# shellcheck shell=bash

function forces {
  export CORALOGIX_LOG_URL="https://ingress.cx498-aws-us-west-2.coralogix.com:443/api/v1/logs" \
    && python3.11 '__argSrcForces__/forces/cli/__init__.py' "$@"
}
