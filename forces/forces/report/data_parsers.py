from datetime import (
    datetime,
)
from decimal import (
    Decimal,
)
from typing import (
    Any,
)
from zoneinfo import (
    ZoneInfo,
)

from forces.model import (
    AcceptanceStatus,
    Finding,
    FindingStatus,
    ForcesConfig,
    Treatment,
    TreatmentStatus,
    Vulnerability,
    VulnerabilityState,
    VulnerabilityTechnique,
    VulnerabilityType,
)
from forces.utils.cvss import (
    get_cvss_fields,
    get_exploitability_from_vector,
)
from forces.utils.strict_mode import (
    get_policy_compliance,
)


def parse_finding(
    config: ForcesConfig,
    finding_dict: dict[str, Any],
) -> Finding:
    severity_field = get_cvss_fields(config.cvss, "finding", "severity")
    vector_field = get_cvss_fields(config.cvss, "finding", "vector")
    return Finding(
        identifier=str(finding_dict["id"]),
        title=str(finding_dict["title"]),
        status=FindingStatus[str(finding_dict["status"]).upper()],
        exploitability=get_exploitability_from_vector(finding_dict[vector_field]),
        severity=Decimal(str(finding_dict[severity_field])),
        url=(
            f"https://app.fluidattacks.com/orgs/{config.organization}/groups/"
            f"{config.group}/vulns/{finding_dict['id']}/locations"
        ),
        vulnerabilities=[],
    )


def parse_location(
    config: ForcesConfig,
    vuln_dict: dict[str, Any],
    exploitability: str,
) -> Vulnerability:
    """
    Parses a vuln mapping into a proper Vulnerability/Location

    Args:
        `config (ForcesConfig)`: The current Forces config
        `vuln_dict (dict[str, Any])`: A vulnerability straight from the
        Integrates API
        `exploitability (str)`: The parent Finding's exploitability

    Returns:
        A parsed location

    """
    severity_field = get_cvss_fields(config.cvss, "vulnerability", "severity")
    severity: Decimal = Decimal(str(vuln_dict[severity_field]))

    return Vulnerability(
        type=(
            VulnerabilityType.STATIC
            if vuln_dict["vulnerabilityType"] == "lines"
            else VulnerabilityType.DYNAMIC
        ),
        technique=VulnerabilityTechnique[
            "PTAAS" if str(vuln_dict["technique"]) == "MPT" else str(vuln_dict["technique"])
        ],
        where=str(vuln_dict["where"]),
        specific=str(vuln_dict["specific"]),
        state=VulnerabilityState[str(vuln_dict["state"])],
        severity=severity,
        report_date=datetime.fromisoformat(str(vuln_dict["reportDate"])).replace(
            tzinfo=ZoneInfo("America/Bogota"),
        ),
        exploitability=exploitability,
        root_nickname=vuln_dict.get("rootNickname"),
        treatment=Treatment(
            status=TreatmentStatus[vuln_dict["treatmentStatus"]]
            if vuln_dict.get("treatmentStatus")
            else None,
            acceptance=AcceptanceStatus[vuln_dict["treatmentAcceptanceStatus"]]
            if vuln_dict.get("treatmentAcceptanceStatus")
            else None,
        ),
        compliance=get_policy_compliance(
            config=config,
            report_date=datetime.fromisoformat(str(vuln_dict["reportDate"])).replace(
                tzinfo=ZoneInfo("America/Bogota"),
            ),
            severity=severity,
            state=VulnerabilityState[str(vuln_dict["state"])],
        ),
    )
