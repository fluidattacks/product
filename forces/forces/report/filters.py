import fnmatch
from collections.abc import ValuesView
from pathlib import (
    Path,
)

from forces.model import (
    Finding,
    ForcesConfig,
    KindEnum,
    Vulnerability,
    VulnerabilityState,
    VulnerabilityType,
)


def filter_kind(
    vuln: Vulnerability,
    kind: KindEnum,
) -> bool:
    return (
        (kind == KindEnum.DYNAMIC and vuln.type == VulnerabilityType.DYNAMIC)
        or (kind == KindEnum.STATIC and vuln.type == VulnerabilityType.STATIC)
        or kind == KindEnum.ALL
    )


def filter_repo(
    vuln: Vulnerability,
    kind: KindEnum,
    repo_name: str | None = None,
) -> bool:
    if repo_name and kind != KindEnum.DYNAMIC and vuln.type == VulnerabilityType.STATIC:
        return fnmatch.fnmatch(vuln.where, f"{repo_name}/*")
    if repo_name and kind != KindEnum.STATIC and vuln.type == VulnerabilityType.DYNAMIC:
        return vuln.root_nickname == repo_name or not vuln.root_nickname
    return True


def filter_findings(
    findings: ValuesView[Finding],
) -> tuple[Finding, ...]:
    """Helper method to filter out findings without vulns and sort the rest"""
    return tuple(
        sorted(
            filter(
                lambda finding: len(finding.vulnerabilities) > 0
                or finding.managed_vulns > 0
                or finding.unmanaged_vulns > 0,
                findings,
            ),
            key=lambda finding: finding.severity,
            reverse=True,
        ),
    )


def filter_repo_paths(vuln: Vulnerability, repo_paths: tuple[str, ...]) -> bool:
    """
    Matches vulns with included repo paths via string comparison and
    globs
    """
    if vuln.type == VulnerabilityType.DYNAMIC or len(repo_paths) == 0:
        return True

    repo_name = Path(vuln.where).parts[0]
    return any(
        repo_path == "."
        or vuln.where.startswith(repo_path)
        or fnmatch.fnmatch(vuln.where, f"{repo_path}")
        or fnmatch.fnmatch(vuln.where, f"{repo_path}/**")
        or fnmatch.fnmatch(vuln.where, f"{repo_name}/{repo_path}")
        or fnmatch.fnmatch(vuln.where, f"{repo_name}/{repo_path}/**")
        for repo_path in repo_paths
    )


def filter_compliance(
    vulnerability: Vulnerability,
    verbose_level: int,
) -> bool:
    """
    Helper method to filter vulns according to their state and
    compliance. A verbose level of 1 only cares about non-compliant vulns
    """
    return vulnerability.state == VulnerabilityState.VULNERABLE and (
        not vulnerability.compliance if verbose_level == 1 else True
    )


def filter_vulnerability(vulnerability: Vulnerability, config: ForcesConfig) -> bool:
    """
    Method to group up all execution filters. `True` means it passes the
    filters, `False` otherwise
    """
    return (
        filter_kind(vulnerability, config.kind)
        and filter_repo(vulnerability, config.kind, config.repository_name)
        and filter_repo_paths(vulnerability, config.repository_paths)
        and filter_compliance(vulnerability, config.verbose_level)
    )
