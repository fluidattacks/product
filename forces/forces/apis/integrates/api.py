"""Fluid Forces integrates api module."""

from collections.abc import (
    Callable,
)
from datetime import (
    UTC,
    datetime,
)
from pathlib import (
    Path,
)
from typing import (
    Any,
    TypeVar,
    cast,
)

import simplejson
from gql import (
    gql,
)

from forces.apis.integrates.client import (
    send,
)
from forces.model import (
    ForcesConfig,
    ForcesData,
    KindEnum,
    Policy,
    StatusCode,
    VulnerabilityState,
)
from forces.utils import (
    env,
)
from forces.utils.cvss import (
    get_exploitability_measure,
)
from forces.utils.errors import (
    ApiError,
)
from forces.utils.function import (
    shield,
)

# Constants
TFun = TypeVar("TFun", bound=Callable[..., object])
SHIELD: Callable[[TFun], TFun] = shield(
    on_error_return=StatusCode.ERROR,
    retries=8 if env.guess_environment() == "production" else 1,
    sleep_between_retries=5,
)


@SHIELD
async def get_findings(group: str, **kwargs: str) -> tuple[dict[str, Any], ...]:
    """
    Gets the findings of a group.

    Args:
        `group (str)`: Group name

    Returns:
        `tuple[dict[str, Any, ...]]`: A tuple with the findings of a group

    """
    # Any change in the following query should be replicated in Checkly
    query = gql(
        """
        query ForcesGetGroupFindings($group_name: String!) {
            group(groupName: $group_name) {
                findings {
                    id
                    currentState
                    title
                    status
                    severityScore
                    severityScoreV4
                    severityVector
                    severityVectorV4
                }
            }
        }
        """,
    )

    result: dict[str, dict[str, list[Any]]] = await send(
        operation=query,
        operation_name="ForcesGetGroupFindings",
        variables={"group_name": group},
        **kwargs,  # type: ignore[arg-type]
    )

    return tuple(
        finding
        for finding in result["group"]["findings"]
        if str(finding["status"]).upper() != "DRAFT"
    )


@SHIELD
async def get_vulnerabilities(config: ForcesConfig, **kwargs: str) -> tuple[dict[str, Any], ...]:
    """
    Gets the vulnerabilities of a group using the new resolver.

    Args:
        `config (ForcesConfig)`: The current Forces config

    Returns:
        `tuple[dict[str, Any, ...]]`: A tuple with the vulns of a group

    """
    vulnerabilities: list[dict[str, Any]] = []
    # Any change in the following query should be replicated in Checkly
    query = gql(
        """
        query ForcesGetGroupLocations(
            $group_name: String!
            $after: String
            $first: Int
            $state: VulnerabilityState
        ) {
            group(groupName: $group_name) {
                forcesVulnerabilities(
                    after: $after
                    first: $first
                    state: $state
                ) {
                    edges {
                        node {
                            findingId
                            state
                            technique
                            treatmentStatus
                            treatmentAcceptanceStatus
                            vulnerabilityType
                            where
                            severityTemporalScore
                            severityThreatScore
                            specific
                            reportDate
                            rootNickname
                            zeroRisk
                        }
                    }
                    pageInfo {
                        hasNextPage
                        endCursor
                    }
                }
            }
        }
    """,
    )
    query_parameters = {
        "first": 200,
        "group_name": config.group,
        "state": "VULNERABLE",
    }
    response: dict = await send(
        operation=query,
        operation_name="ForcesGetGroupLocations",
        variables=query_parameters,
        **kwargs,  # type: ignore[arg-type]
    )
    while True:
        has_next_page: bool = False
        if response:
            vulnerabilities_connection = response["group"]["forcesVulnerabilities"]
            vulnerability_page_info = vulnerabilities_connection["pageInfo"]
            vulnerability_edges = vulnerabilities_connection["edges"]
            has_next_page = vulnerability_page_info["hasNextPage"]
            end_cursor = vulnerability_page_info["endCursor"]
            vulnerabilities.extend([vuln_edge["node"] for vuln_edge in vulnerability_edges])

        if not has_next_page:
            break

        response = await send(
            operation=query,
            operation_name="ForcesGetGroupLocations",
            variables={**query_parameters, "after": end_cursor},
            **kwargs,  # type: ignore[arg-type]
        )

    return tuple(vulnerabilities)


@SHIELD
async def get_vulnerabilities_fallback(
    config: ForcesConfig,
    **kwargs: str,
) -> tuple[dict[str, Any], ...]:
    """
    Gets the vulnerabilities of a group using the Opensearch resolver

    Args:
        `config (ForcesConfig)`: The current Forces config

    Returns:
        `tuple[dict[str, Any, ...]]`: A tuple with the vulns of a group

    """
    vulnerabilities: list[dict[str, Any]] = []
    # Any change in the following query should be replicated in Checkly
    query = gql(
        """
        query ForcesGetGroupLocations(
            $group_name: String!
            $after: String
            $first: Int
            $brk_severity: String
            $state: VulnerabilityState
            $vuln_type: String
        ) {
            group(groupName: $group_name) {
                vulnerabilities(
                    after: $after
                    first: $first
                    minSeverity: $brk_severity
                    state: $state
                    type: $vuln_type
                ) {
                    edges {
                        node {
                            findingId
                            state
                            technique
                            treatmentStatus
                            treatmentAcceptanceStatus
                            vulnerabilityType
                            where
                            severityTemporalScore
                            severityThreatScore
                            specific
                            reportDate
                            rootNickname
                            zeroRisk
                        }
                    }
                    pageInfo {
                        hasNextPage
                        endCursor
                    }
                }
            }
        }
    """,
    )
    query_parameters = {
        "brk_severity": str(int(config.breaking_severity)) if config.verbose_level == 1 else None,
        "first": 200,
        "group_name": config.group,
        "state": "VULNERABLE" if config.verbose_level <= 2 else None,
        "vuln_type": "lines" if config.kind == KindEnum.STATIC else None,
    }

    response: dict = await send(
        operation=query,
        operation_name="ForcesGetGroupLocations",
        variables=query_parameters,
        **kwargs,  # type: ignore[arg-type]
    )
    while True:
        has_next_page: bool = False
        if response:
            vulnerabilities_connection = response["group"]["vulnerabilities"]
            vulnerability_page_info = vulnerabilities_connection["pageInfo"]
            vulnerability_edges = vulnerabilities_connection["edges"]
            has_next_page = vulnerability_page_info["hasNextPage"]
            end_cursor = vulnerability_page_info["endCursor"]
            vulnerabilities.extend([vuln_edge["node"] for vuln_edge in vulnerability_edges])

        if not has_next_page:
            break

        response = await send(
            operation=query,
            operation_name="ForcesGetGroupLocations",
            variables={**query_parameters, "after": end_cursor},
            **kwargs,  # type: ignore[arg-type]
        )

    for index, _ in enumerate(vulnerabilities):
        treatment_approval = vulnerabilities[index].get("treatmentAcceptanceStatus")
        treatment = vulnerabilities[index].get("treatmentStatus")
        zero_risk = vulnerabilities[index].get("zeroRisk")
        if ("ACCEPTED" in str(treatment) and str(treatment_approval).upper() == "APPROVED") or (
            str(zero_risk).upper() == "CONFIRMED"
        ):
            vulnerabilities[index]["state"] = "ACCEPTED"

    return tuple(vulnerabilities)


async def upload_report(
    *,
    config: ForcesConfig,
    execution_id: str,
    git_metadata: dict[str, str],
    log_file: Path,
    report: ForcesData,
    exit_code: str,
    **kwargs: str,
) -> bool:
    """
    Upload report to the Fluid Attacks Platform

    Args:
        `config (ForcesConfig)`: The current Forces config
        `execution_id (str)`: ID of forces execution.
        `git_metadata (dict[str, str])`: Repository metadata.
        `log (str)`: Forces execution log.
        `report (ForcesData)`: Forces execution report.
        `exit_code (str)`: Exit code.

    Returns:
        `bool`: Report upload status

    """
    mutation = gql(
        """
        mutation ForcesUploadReport(
            $group_name: String!
            $execution_id: String!
            $date: DateTime!
            $days_until_it_breaks: Int
            $managed_vulnerabilities: Int
            $un_managed_vulnerabilities: Int
            $exit_code: String!
            $git_branch: String
            $git_commit: String
            $git_origin: String
            $git_repo: String
            $json_log: String!
            $kind: String
            $log: Upload!
            $strictness: String!
            $grace_period: Int!
            $severity_threshold: Float!
            $open: [ExploitResultInput!]
            $closed: [ExploitResultInput!]
            $accepted: [ExploitResultInput!]
        ) {
            addForcesExecution(
                groupName: $group_name
                executionId: $execution_id
                date: $date
                daysUntilItBreaks: $days_until_it_breaks
                managedVulnerabilities: $managed_vulnerabilities
                unManagedVulnerabilities: $un_managed_vulnerabilities
                exitCode: $exit_code
                gitBranch: $git_branch
                gitCommit: $git_commit
                gitOrigin: $git_origin
                gitRepo: $git_repo
                jsonLog: $json_log
                kind: $kind
                log: $log
                strictness: $strictness
                gracePeriod: $grace_period
                severityThreshold: $severity_threshold
                vulnerabilities: {
                    open: $open,
                    accepted: $accepted,
                    closed: $closed,
                }
            ) {
                success
            }
        }
    """,
    )
    vulnerable_vulns: list[dict[str, float | str]] = []
    safe_vulns: list[dict[str, float | str]] = []
    accepted_vulns: list[dict[str, float | str]] = []

    for vuln in [vuln for find in report.findings for vuln in find.vulnerabilities]:
        vuln_state: dict[str, float | str] = {
            "kind": vuln.type.value,
            "who": vuln.specific,
            "where": vuln.where,
            "state": {"safe": "closed", "vulnerable": "open"}.get(
                vuln.state.value,
                vuln.state.value,
            ).upper(),
            "exploitability": get_exploitability_measure(vuln.exploitability),
            "compliance": vuln.compliance,
        }
        if vuln.state == VulnerabilityState.VULNERABLE:
            vulnerable_vulns.append(vuln_state)
        elif vuln.state == VulnerabilityState.SAFE:
            safe_vulns.append(vuln_state)
        elif vuln.state == VulnerabilityState.ACCEPTED:
            accepted_vulns.append(vuln_state)

    with open(log_file, "rb") as forces_log:
        params: dict[str, Any] = {
            "group_name": config.group,
            "execution_id": execution_id,
            "date": datetime.now(UTC).isoformat(),
            "days_until_it_breaks": config.days_until_it_breaks,
            "exit_code": exit_code,
            "git_branch": git_metadata["git_branch"],
            "git_commit": git_metadata["git_commit"],
            "git_origin": git_metadata["git_origin"],
            "git_repo": git_metadata["git_repo"],
            "open": vulnerable_vulns,
            "accepted": accepted_vulns,
            "closed": safe_vulns,
            "log": forces_log,
            "json_log": simplejson.dumps(
                report,
                default=str,
                namedtuple_as_object=True,
                use_decimal=True,
            ),
            "strictness": "strict" if config.strict else "lax",
            "grace_period": config.grace_period,
            "severity_threshold": float(config.breaking_severity),
            "kind": config.kind.value,
            "un_managed_vulnerabilities": sum(find.unmanaged_vulns for find in report.findings),
            "managed_vulnerabilities": sum(find.managed_vulns for find in report.findings),
        }

        response: dict[str, dict[str, bool]] = await send(
            operation=mutation,
            operation_name="ForcesUploadReport",
            variables=params,
            **kwargs,  # type: ignore[arg-type]
        )
    return response.get("addForcesExecution", {}).get("success", False)


async def get_groups_access(api_token: str) -> tuple[dict[str, Any], ...]:
    """
    Gets the relevant user and org data from the token

    Args:
        `api_token (str)`: Forces Integrates API token

    Returns:
        `tuple[dict[str, Any], ...]`: A tuple of dicts with the group data the
        user has access to

    """
    query = gql(
        """
        query ForcesGetMeGroups {
          me {
            organizations {
              groups {
                daysUntilItBreaks
                name
                minBreakingSeverity
                vulnerabilityGracePeriod
                organization
                userRole
              }
            }
          }
        }
    """,
    )
    try:
        response: dict[
            str,
            dict[
                str,
                list[dict[str, list[dict[str, str]] | int | float]],
            ],
        ] = await send(
            operation=query,
            operation_name="ForcesGetMeGroups",
            api_token=api_token,
        )
    except ApiError as exc:
        if "Login required" in str(exc) or "Token format unrecognized" in str(exc):
            return tuple()
        raise exc
    return tuple(
        group
        for organization in response["me"]["organizations"]
        for group in cast(list[dict[str, Any]], organization["groups"])
    )


async def get_git_remotes(group: str) -> tuple[dict[str, str], ...]:
    """
    Gets the git root information of the group

    Args:
        `group (str)`: Group name

    Returns:
        `tuple[dict[str, str]]`: A tuple of dicts with the git root
        information: (`url`, `state` and `nickname`)

    """
    query = gql(
        """
        query ForcesGetGitRoots($group: String!) {
          group(groupName: $group){
            roots {
              ...on GitRoot{
                url
                state
                nickname
              }
            }
          }
        }
    """,
    )
    response: dict[str, dict[str, list[dict[str, str]]]] = await send(
        operation=query,
        operation_name="ForcesGetGitRoots",
        variables={"group": group},
    )

    return tuple(response["group"]["roots"])


async def get_forces_policies(api_token: str) -> Policy | None:
    """
    Gets the relevant forces policies from the group access info

    Args:
        `api_token (str)`: Forces Integrates API token

    Returns:
        `Policy`: A tuple with the relevant forces policies if the token is.
        valid, None otherwise.

    """
    groups = await get_groups_access(api_token)
    for group in groups:
        if group["userRole"] == "service_forces":
            return Policy(
                organization=group["organization"],
                group=group["name"],
                arm_severity_policy=group["minBreakingSeverity"],
                vuln_grace_period=group["vulnerabilityGracePeriod"],
                days_until_it_breaks=group["daysUntilItBreaks"],
            )
    return None
