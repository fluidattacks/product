from typing import (
    NamedTuple,
)

from rich.table import (
    Table,
)

from forces.model.finding import (
    Finding,
)

VulnerabilitiesClassification = dict[str, dict[str, dict[str, int] | int]]


class SummaryItem(NamedTuple):
    dynamic: dict[str, int]
    total_dynamic: int
    static: dict[str, int]
    total_static: int


class ReportSummary(NamedTuple):
    vulnerable: SummaryItem
    overall_compliance: bool
    elapsed_time: str
    total: int


class ForcesData(NamedTuple):
    findings: tuple[Finding, ...]
    summary: ReportSummary


class ForcesReport(NamedTuple):
    findings_report: Table
    summary_report: Table
