import re
import textwrap

from forces.utils.logs import (
    LogInterface,
    log_banner,
    rich_log,
)

USER_PATTERN = r"forces.(?P<group>\w+)@fluidattacks.com"


def is_forces_user(email: str) -> bool:
    """Ensure that is an forces user."""
    return bool(re.match(USER_PATTERN, email))


def get_group_from_email(email: str) -> str:
    return re.match(USER_PATTERN, email).group("group")  # type: ignore


def show_banner() -> None:
    """Show forces banner"""
    name: str = (
        "[default on red]  [/][bold default on red]››[/]"
        "[bold red on white]› [/][italic bold red] Fluid [white]Attacks[/][/]"
    )
    motto: str = "[italic bold white] We [red]hack[/] your software[/]"
    logo: str = f"""
    [default on white]        [/]
    [default on white]  [/]{name}
    [default on white]  [/][default on red]    [/][red on white]  [/]{motto}
    [default on white]        [/]"""
    console_header: str = textwrap.dedent(
        r"""
        [bright_green]      ____            _____           ____
             / __ \___ _   __/ ___/___  _____/ __ \____  _____
            / / / / _ \ | / /\__ \/ _ \/ ___/ / / / __ \/ ___/
           / /_/ /  __/ |/ /___/ /  __/ /__/ /_/ / /_/ (__  )
          /_____/\___/|___//____/\___/\___/\____/ .___/____/
                                               /_/[/]
        """,
    )
    log_header: str = "[bold green]D E V S E C O P S[/]"
    rich_log(logo)
    rich_log(rich_msg=console_header, log_to=LogInterface.CONSOLE)
    log_banner(log_header)
