import logging
import os
import re
import tempfile
from collections.abc import (
    Set,
)
from contextvars import (
    ContextVar,
)
from enum import (
    StrEnum,
)
from pathlib import (
    Path,
)
from typing import (
    IO,
    Any,
)

import bugsnag
from aioextensions import (
    in_thread,
)
from coralogix.handlers import (
    CoralogixLogger,
)
from rich.console import (
    Console,
    RenderableType,
)
from rich.logging import (
    RichHandler,
)
from rich.theme import (
    Theme,
)

from forces.utils.bugs import (
    META as BUGS_META,
)

FORCES_WIDTH = 80


class LogInterface(StrEnum):
    ALL = "ALL"
    CONSOLE = "CONSOLE"
    LOGGER = "LOGGER"


def create_temp_file() -> IO[Any]:
    return tempfile.NamedTemporaryFile(mode="w+t")


# There is no problem in making this key public
# it's intentional so we can monitor Forces' stability in remote users
_CORALOGIX_PRIVATE_KEY = "cxtp_Z1pBhOm2N3hirZQkdk2sKH2Zf4GzKN"
_LOGGER_CORALOGIX_HANDLER = CoralogixLogger(
    _CORALOGIX_PRIVATE_KEY,
    "forces",
    "forces-cli",
)
# Custom themes
CUSTOM_THEME = Theme({"logging.level.warning": "orange1"})
# Private constants, text mode required for Rich logging
LOG_FILE: ContextVar[IO[Any]] = ContextVar("log_file", default=create_temp_file())
INTEGRATES_LOG_FILE: ContextVar[IO[Any]] = ContextVar(
    "integrates_log_file",
    default=create_temp_file(),
)
# Console interface to show logs and special spinner symbols on stdout
CONSOLE_INTERFACE = Console(
    # Omit colors from Azure Pipeline logs
    force_terminal=os.environ.get("System.JobId", None) is None,
    log_path=False,
    log_time=False,
    markup=True,
    theme=CUSTOM_THEME,
    width=FORCES_WIDTH,
)
# Logging interface to get around the Rich library writing limitations
LOGGING_INTERFACE = Console(
    force_terminal=True,
    file=LOG_FILE.get(),
    log_path=False,
    log_time=False,
    markup=True,
    width=FORCES_WIDTH,
)

_FORMAT: str = "%(message)s"
logging.basicConfig(format=_FORMAT)

_LOGGER_FORMATTER: logging.Formatter = logging.Formatter(_FORMAT)

_LOGGER: logging.Logger = logging.getLogger("forces")
_LOGGER.setLevel(logging.INFO)
_LOGGER.propagate = False
_LOGGER.addHandler(_LOGGER_CORALOGIX_HANDLER)


def set_up_handlers(interfaces: Set[Console]) -> None:
    """Configures and sets up logging handlers for the main logger object"""
    for interface in interfaces:
        handler: logging.Handler = RichHandler(
            show_time=False,
            markup=True,
            show_path=False,
            console=interface,
        )
        handler.setFormatter(_LOGGER_FORMATTER)
        _LOGGER.addHandler(handler)


# A rich console can only write to either stdout or the provided file, not both
set_up_handlers({CONSOLE_INTERFACE, LOGGING_INTERFACE})


def blocking_log(level: str, msg: str, *args: object) -> None:
    getattr(_LOGGER, level)(msg, *args)


async def log(level: str, msg: str, *args: object) -> None:
    await in_thread(getattr(_LOGGER, level), msg, *args)


def rich_log(
    rich_msg: RenderableType | str,
    log_to: LogInterface = LogInterface.ALL,
) -> None:
    """
    Writes to the specified console interfaces to have either stdout and log
    output
    """
    if log_to in [LogInterface.ALL, LogInterface.CONSOLE]:
        CONSOLE_INTERFACE.log(rich_msg)
    if log_to in [LogInterface.ALL, LogInterface.LOGGER]:
        LOGGING_INTERFACE.log(rich_msg)


def log_banner(banner: str) -> None:
    """Special method to log the banner to be shown in Integrates logs"""
    LOGGING_INTERFACE.rule(banner)


async def log_to_remote(exception: BaseException, **metadata: str) -> None:  # pragma: no cover
    metadata.update(BUGS_META.get() or {})
    await in_thread(bugsnag.notify, exception, metadata=metadata)


def sanitized_log_file(log_file_path: Path) -> Path:
    """
    Removes OSC 8 sequences from URLs in the log file to ensure
    compatibility with Integrates' ANSI viewer.

    Args:
        log_file_path (Path): Path of the temporary file that captures the log

    Returns:
        `Path`: The path of a sanitized temporary file that Integrates ANSI
        viewer can understand

    """
    temp_file = INTEGRATES_LOG_FILE.get()

    osc_8_pattern = re.compile(r"\x1b\]8;[^;]*;([^\\]+)\\")
    with open(log_file_path, "rb") as file:
        content = file.read().decode("utf-8")
    cleaned_content = osc_8_pattern.sub("", content)

    with open(temp_file.name, "w", encoding="utf-8") as integrates_temp_file:
        integrates_temp_file.write(cleaned_content)

    return Path(integrates_temp_file.name)
