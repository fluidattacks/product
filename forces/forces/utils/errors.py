from typing import (
    Any,
)

from gql.transport.exceptions import (
    TransportError,
    TransportProtocolError,
    TransportQueryError,
    TransportServerError,
)
from graphql import (
    GraphQLError,
)
from httpx import (
    ReadTimeout,
)

from forces.utils import (
    env,
)
from forces.utils.logs import (
    blocking_log,
    log,
    log_to_remote,
)


class ApiError(Exception):
    def __init__(self, *errors: dict[str, Any]) -> None:
        self.messages: list[str] = []
        self.skip_bugsnag: bool = False
        for error in errors:
            if message := error.get("message"):
                self.messages.append(message)
                blocking_log("error", message)
        super().__init__(*errors)


async def handle_client_error(
    operation_name: str,
    exception: GraphQLError | TransportError | TimeoutError | ReadTimeout,
) -> None:
    prod = env.guess_environment() == "production"
    match exception:
        # Expired or invalid token
        case GraphQLError() | TransportQueryError():
            if token_error := "Login required" in str(
                exception,
            ) or "Token format unrecognized" in str(exception):
                await log(
                    "error",
                    (
                        "The token has expired or the token has no "
                        "permissions\n To generate a new forces token go to "
                        "our Platform:\n Login > Group > Scope > Manage token "
                        "> Reveal token > Generate"
                    ),
                )
            api_error = ApiError({"message": str(exception), "operation": operation_name})
            api_error.skip_bugsnag = token_error or not prod
            raise api_error from exception
        # Cloudflare got in the way or the endpoint is slow, should retry
        case TransportProtocolError() | TimeoutError() | ReadTimeout():
            if prod:
                await log_to_remote(exception, operation=operation_name)
        case TransportServerError():
            # 4xx can be caused due to starlette rejecting big log files
            if exception.code is not None and exception.code < 500 and prod:
                await log_to_remote(exception, operation=operation_name)
        case _:
            raise exception
