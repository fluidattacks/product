import os
from collections.abc import Callable
from typing import (
    Literal,
)

# Constants
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
PROD_ENDPOINT = "https://app.fluidattacks.com/api"
ENDPOINT: str = os.environ.get("API_ENDPOINT", PROD_ENDPOINT)
TIMEOUT = 60.0  # seconds


def guess_environment() -> Literal["development", "production"]:
    return "development" if ENDPOINT != PROD_ENDPOINT else "production"


def proxy_manager() -> (
    tuple[
        Callable[[str, bool], None],
        Callable[[], str | None],
        Callable[[], bool],
    ]
):
    http_proxy: str | None = None
    verify_proxy_ssl: bool = True

    def _setup_proxy(proxy: str, verify_ssl: bool) -> None:
        nonlocal http_proxy, verify_proxy_ssl
        if proxy:
            http_proxy = proxy
            verify_proxy_ssl = verify_ssl

    def _get_proxy() -> str | None:
        return http_proxy

    def _get_verify_ssl() -> bool:
        return verify_proxy_ssl

    return _setup_proxy, _get_proxy, _get_verify_ssl


setup_proxy, get_proxy, get_verify_ssl = proxy_manager()
