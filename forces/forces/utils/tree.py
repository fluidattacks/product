from functools import (
    reduce,
)
from pathlib import (
    Path,
)

from rich.padding import (
    Padding,
)
from rich.tree import (
    Tree,
)

from forces.model import (
    ForcesConfig,
    KindEnum,
)
from forces.utils.logs import (
    log,
    log_to_remote,
    rich_log,
)


def normalize_path(path: str, repository_name: str) -> str:
    if not path.startswith(repository_name) and not path.startswith(f"/{repository_name}"):
        return f"{repository_name}/{path}"
    return path


def normalize_paths(paths: tuple[str, ...], repository_name: str) -> tuple[str, ...]:
    return tuple(normalize_path(path, repository_name) for path in paths)


def tuple_to_nested_dict(path_tuple: tuple[str, ...]) -> dict:
    """Turns `(path, to, folder,)` into `{path: {to: {folder: {} } } }`"""
    # Reversing every tuple to build the nested dict inside out
    return reduce(
        lambda path_children, path_segment: {path_segment: path_children},
        reversed(path_tuple),
        {},
    )


def merge_paths(merged_paths: dict[str, dict], path: dict[str, dict]) -> dict[str, dict]:
    """Recursively merge two paths"""
    if merged_paths == {}:
        return path
    for segment in path:
        if segment in merged_paths:
            if isinstance(merged_paths[segment], dict) and isinstance(path[segment], dict):
                merge_paths(merged_paths[segment], path[segment])
        else:
            merged_paths[segment] = path[segment]
    return merged_paths


def build_tree_dict(paths: tuple[str, ...]) -> dict[str, dict]:
    path_tuples: tuple[tuple[str, ...], ...] = tuple(Path(path).parts for path in paths)
    path_dicts = tuple(tuple_to_nested_dict(path_tuple) for path_tuple in path_tuples)
    return reduce(merge_paths, path_dicts, {})


def build_tree(data: dict[str, dict], tree: Tree) -> Tree:
    emojis = {
        ".bat": "🦇",
        ".bin": "📦",
        ".c": "📄",
        ".cpp": "➕",
        ".cs": "🎵",
        ".css": "🎨",
        ".csv": "📑",
        ".dart": "🎯",
        ".exe": "🚀",
        ".go": "🐹",
        ".html": "🌐",
        ".ini": "⚙️",
        ".java": "☕",
        ".js": "✨",
        ".json": "🔧",
        ".kt": "☕",
        ".log": "🪵",
        ".md": "📝",
        ".nix": "❄️",
        ".php": "🐘",
        ".pl": "🐪",
        ".py": "🐍",
        ".r": "📊",
        ".rb": "💎",
        ".rs": "🦀",
        ".sh": "💻",
        ".sql": "🗄️",
        ".swift": "🦅",
        ".ts": "🔷",
        ".xml": "🔖",
        ".yaml": "🔧",
        ".yml": "🔧",
    }
    for key, subtree in data.items():
        if subtree:
            branch = tree.add(f"📁 {key}")
            build_tree(subtree, branch)
        else:
            path = Path(key)
            if path.suffix:
                tree.add(f"{emojis.get(path.suffix,'📄')} {key}")
            else:
                tree.add(f"📁 {key}")
    return tree


def get_tree_from_config(config: ForcesConfig) -> Tree:
    assert config.repository_name is not None
    normalized_paths = normalize_paths(config.repository_paths, config.repository_name)
    tree_dict = build_tree_dict(normalized_paths)
    return build_tree(
        tree_dict[config.repository_name],
        Tree(f"🫚 {config.repository_name}", guide_style="gold1"),
    )


async def show_repo_path_logs(config: ForcesConfig) -> None:
    if config.repository_paths and config.kind == KindEnum.STATIC:
        await log(
            "info",
            (
                "Looking for vulnerabilities matching paths: [bright_yellow]"
                f"{', '.join(path for path in config.repository_paths)}"
                "[/]"
            ),
        )
        if config.repository_name is not None:
            try:
                # Added padding to line up the tree with the other logs
                tree = get_tree_from_config(config)
                rich_log(Padding.indent(tree, 9))
            except (IndexError, ValueError, KeyError) as exc:
                # The tree is a nice touch but the app shouldn't crash
                # because of this, showing the report is paramount
                await log_to_remote(exc)
        await log(
            "warning",
            (
                "The Agent does not validate if the provided paths do "
                "exist in the repository. Make sure to check and update them "
                "regularly"
            ),
        )
