"""Fluid Forces CLI module."""

import sys
from io import (
    TextIOWrapper,
)
from time import (
    sleep,
)

import aioextensions
import click
from aioextensions import (
    run,
)

from forces import (
    entrypoint,
)
from forces.apis.integrates.api import (
    get_forces_policies,
)
from forces.model import (
    CVSS,
    ForcesConfig,
    KindEnum,
    StatusCode,
)
from forces.utils import (
    env,
)
from forces.utils.bugs import (
    configure_bugsnag,
)
from forces.utils.cli import (
    show_banner,
)
from forces.utils.function import (
    shield,
)
from forces.utils.logs import (
    blocking_log,
    log,
)
from forces.utils.strict_mode import (
    set_breaking_severity,
)


def _get_kind(dynamic: bool, static: bool) -> str:
    kind = "all"
    if dynamic:
        kind = "dynamic"
    elif static:
        kind = "static"
    return kind


@click.command(
    name="forces",
    help="Fluid Attacks' DevSecOps CI Agent",
    epilog="""For more information on how to install, configure the Agent to
    your needs, see usage examples, or troubleshoot any issues, please check
    out https://help.fluidattacks.com/portal/en/kb/articles/install-the-ci-agent-to-break-the-build
    or mail your inquiry to help@fluidattacks.com""",
    no_args_is_help=True,
)
@click.option("--token", required=True, help="Your DevSecOps agent token")
@click.option(
    "--breaking",
    required=False,
    default=None,
    help="""(Strict mode only) Vulnerable locations with a CVSS score below
    this threshold won't break the build. Keep in mind that the value set,
    if set, in your Fluid Attacks organization/group's policies will cap the
    one passed to this CLI setting.""",
    type=click.FloatRange(min=0.0, max=10.0),
)
@click.option(
    "--dynamic",
    help="Check for DYNAMIC vulnerabilities only",
    is_flag=True,
    required=False,
)
@click.option(
    "--feature-preview",
    default=False,
    is_flag=True,
    help="Enable the feature preview mode",
    required=False,
)
@click.option(
    "--cvss",
    help="""Which CVSS (Common Vulnerability Scoring System) version to use
    to calculate the severity of the vulnerabilities found. 4.0 being the
    current version and 3.1 being legacy""",
    type=click.Choice(
        (
            "3.1",
            "4.0",
        ),
    ),
    default="4.0",
    required=False,
)
@click.option(
    "--output",
    "-O",
    help="Save output to FILE",
    metavar="FILE",
    required=False,
    type=click.File("w", encoding="utf-8"),
)
@click.option(
    "-p",
    "--proxy",
    default="",
    help="Set up a proxy for routing HTTP requests",
    required=False,
    type=str,
)
@click.option(
    "--repo-name",
    default=None,
    help="Repository nickname",
    required=False,
)
@click.option(
    "--repo-path",
    default=[],
    help="Repository paths to analyze",
    multiple=True,
)
@click.option(
    "--static",
    is_flag=True,
    help="Check for STATIC vulnerabilities only",
    required=False,
)
@click.option(
    "--strict/--lax",
    help="Sets the DevSecOps agent mode (default --lax)",
)
@click.option(
    "-v",
    "--verbose",
    count=True,
    default=2,
    help="The level of detail of the report (default -vv)",
    required=False,
    type=click.IntRange(min=1, max=2, clamp=True),
)
@click.option(
    "--verify-proxy-ssl/--no-verify-proxy-ssl",
    default=True,
    help="""Defines whether to enable or disable SSL certificate validation
    while using an HTTP proxy""",
)
def main(
    breaking: float,
    dynamic: bool,
    cvss: CVSS,
    feature_preview: bool,
    output: TextIOWrapper,
    proxy: str,
    repo_name: str,
    repo_path: tuple[str, ...],
    static: bool,
    strict: bool,
    token: str,
    verbose: int,
    verify_proxy_ssl: bool,
) -> None:
    """Main function"""
    # Use only one worker,
    # some customers are experiencing threads exhaustion
    # and we suspect it could be this
    try:
        assert not aioextensions.PROCESS_POOL.initialized
        assert not aioextensions.THREAD_POOL.initialized
        aioextensions.PROCESS_POOL.initialize(max_workers=1)
        aioextensions.THREAD_POOL.initialize(max_workers=1)

        result: int = 1
        for _ in range(6):
            try:
                result = run(
                    main_wrapped(
                        feature_preview=feature_preview,
                        kind=_get_kind(dynamic, static),
                        cvss=cvss,
                        local_breaking=breaking,
                        output=output,
                        proxy=proxy,
                        repo_name=repo_name,
                        repo_path=repo_path,
                        strict=strict,
                        token=token,
                        verbose=verbose,
                        verify_proxy_ssl=verify_proxy_ssl,
                    ),
                )
                break
            except RuntimeError as err:
                blocking_log("warning", "An error ocurred: %s. Retrying...", err)
                sleep(10.0)

        sys.exit(result)
    finally:
        aioextensions.PROCESS_POOL.shutdown(wait=True)
        aioextensions.THREAD_POOL.shutdown(wait=True)


async def _setup_configuration(
    *,
    feature_preview: bool,
    token: str,
    kind: str,
    cvss: CVSS,
    output: TextIOWrapper,
    repo_name: str,
    repo_path: tuple[str, ...],
    strict: bool,
    verbose: int,
    local_breaking: float,
) -> ForcesConfig | None:
    policies = await get_forces_policies(api_token=token)
    if policies is None:
        return None
    config = ForcesConfig(
        organization=policies.organization,
        group=policies.group,
        cvss=cvss,
        kind=KindEnum[kind.upper()],
        output=output,
        repository_paths=repo_path,
        repository_name=repo_name,
        strict=strict,
        verbose_level=verbose,
        breaking_severity=set_breaking_severity(
            arm_severity_policy=policies.arm_severity_policy,
            cli_severity_policy=local_breaking,
        ),
        grace_period=policies.vuln_grace_period,
        days_until_it_breaks=policies.days_until_it_breaks
        if policies.days_until_it_breaks is not None and policies.days_until_it_breaks > 0
        else None,
        feature_preview=feature_preview,
    )
    return config


@shield(on_error_return=StatusCode.ERROR)
async def main_wrapped(
    *,
    feature_preview: bool,
    kind: str,
    cvss: CVSS,
    local_breaking: float,
    output: TextIOWrapper,
    proxy: str,
    repo_name: str,
    repo_path: tuple[str, ...],
    strict: bool,
    token: str,
    verbose: int,
    verify_proxy_ssl: bool,
) -> int:
    env.setup_proxy(proxy, verify_proxy_ssl)
    config = await _setup_configuration(
        feature_preview=feature_preview,
        token=token,
        cvss=cvss,
        kind=kind,
        output=output,
        repo_name=repo_name,
        repo_path=repo_path,
        strict=strict,
        verbose=verbose,
        local_breaking=local_breaking,
    )
    if config is None:
        return StatusCode.ERROR

    configure_bugsnag(config)
    show_banner()
    if env.guess_environment() == "development":
        await log("debug", "The agent is running in dev mode")
        await log("debug", f"The agent is pointing to {env.ENDPOINT}")

    await log(
        "info",
        f"Running the DevSecOps agent in [bright_yellow]"
        f"{'strict' if strict else 'lax'}[/] mode",
    )
    await log("info", f"Running the DevSecOps agent in [bright_yellow]{kind}[/] kind")

    return await entrypoint(
        token=token,
        config=config,
    )


if __name__ == "__main__":
    main(prog_name="forces")
