{ outputs, ... }:
let
  credentials = {
    token = "DOCKER_HUB_PASS";
    user = "DOCKER_HUB_USER";
  };
  setup = [
    outputs."/secretsForAwsFromGitlab/dev"
    outputs."/secretsForEnvFromSops/forcesContainer"
  ];
  src = outputs."/forces/container";
in {
  deployContainer = {
    forcesAmd64 = {
      image = "docker.io/fluidattacks/forces:amd64";
      inherit credentials setup src;
    };
    forcesArm64 = {
      image = "docker.io/fluidattacks/forces:arm64";
      inherit credentials setup src;
    };
  };
  deployContainerManifest.forces = {
    image = "docker.io/fluidattacks/forces:latest";
    manifests = [
      {
        image = "docker.io/fluidattacks/forces:amd64";
        platform = {
          architecture = "amd64";
          os = "linux";
        };
      }
      {
        image = "docker.io/fluidattacks/forces:arm64";
        platform = {
          architecture = "arm64";
          os = "linux";
        };
      }
    ];
    inherit credentials setup;
  };
  secretsForEnvFromSops.forcesContainer = {
    vars = [ "DOCKER_HUB_PASS" "DOCKER_HUB_USER" ];
    manifest = "/common/secrets/dev.yaml";
  };
}
