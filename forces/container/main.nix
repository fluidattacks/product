{ inputs, makeContainerImage, makeDerivation, outputs, ... }:
makeContainerImage {
  config.WorkingDir = "/src";
  extraCommands = ''
    mkdir -m 1777 tmp
    mkdir -m 1777 .cache
  '';
  layers = [ inputs.nixpkgs.bash inputs.nixpkgs.coreutils outputs."/forces" ];
}
