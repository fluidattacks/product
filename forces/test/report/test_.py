from datetime import datetime
from decimal import Decimal
from typing import Any
from zoneinfo import ZoneInfo

import pytest
from rich.table import Table

from forces.apis.integrates.api import get_findings, get_vulnerabilities
from forces.model import (
    AcceptanceStatus,
    Finding,
    FindingStatus,
    ForcesConfig,
    ForcesData,
    KindEnum,
    Treatment,
    TreatmentStatus,
    Vulnerability,
    VulnerabilityState,
    VulnerabilityTechnique,
    VulnerabilityType,
)
from forces.report.data import compile_raw_report, get_group_findings_info
from forces.report.data_parsers import parse_finding, parse_location
from forces.report.filters import (
    filter_compliance,
    filter_findings,
    filter_kind,
    filter_repo,
    filter_repo_paths,
)
from forces.report.styles import style_report, style_summary, style_treatment
from forces.report.tables import format_finding_table
from forces.utils.cvss import get_exploitability_from_vector
from forces.utils.strict_mode import get_policy_compliance


@pytest.mark.asyncio
async def test_get_group_findings_info(
    test_config: ForcesConfig,
    test_token: str,
) -> None:
    findings_dict_1 = await get_group_findings_info(config=test_config, api_token=test_token)
    for find in findings_dict_1.values():
        assert find.identifier
        assert find.title
        assert find.status
        assert find.url
        assert find.exploitability


@pytest.mark.asyncio
async def test_parse_finding(
    test_config: ForcesConfig,
    test_token: str,
) -> None:
    result: tuple[dict[str, Any], ...] = await get_findings(test_config.group, api_token=test_token)
    for finding_dict in result:
        parsed_finding = parse_finding(
            config=test_config,
            finding_dict=finding_dict,
        )

        assert parsed_finding.identifier == finding_dict["id"]
        assert parsed_finding.title == finding_dict["title"]
        assert parsed_finding.status == FindingStatus[str(finding_dict["status"]).upper()]
        assert parsed_finding.exploitability == get_exploitability_from_vector(
            finding_dict["severityVector"],
        )
        assert parsed_finding.severity == Decimal(str(finding_dict["severityScore"]))
        assert parsed_finding.url == (
            f"https://app.fluidattacks.com/orgs/{test_config.organization}"
            f"/groups/{test_config.group}/vulns/{finding_dict['id']}/locations"
        )
        assert parsed_finding.vulnerabilities == []


@pytest.mark.asyncio
async def test_parse_location(
    test_config: ForcesConfig,
    test_token: str,
) -> None:
    result: tuple[dict[str, Any], ...] = await get_vulnerabilities(
        test_config,
        api_token=test_token,
    )
    dummy_exploitability = "High"
    for vuln_dict in result:
        parsed_location = parse_location(
            vuln_dict=vuln_dict,
            config=test_config,
            exploitability=dummy_exploitability,
        )

        assert parsed_location.type == (
            VulnerabilityType.STATIC
            if vuln_dict["vulnerabilityType"] == "lines"
            else VulnerabilityType.DYNAMIC
        )
        assert (
            parsed_location.technique == "PTAAS"
            if str(vuln_dict["technique"]) == "MPT"
            else str(vuln_dict["technique"])
        )
        assert parsed_location.where == str(vuln_dict["where"])
        assert parsed_location.specific == str(vuln_dict["specific"])
        assert parsed_location.state == VulnerabilityState[str(vuln_dict["state"])]
        assert parsed_location.severity == Decimal(str(vuln_dict["severityTemporalScore"]))
        assert parsed_location.treatment == Treatment(
            status=TreatmentStatus.UNTREATED
            if vuln_dict["treatmentStatus"] == "UNTREATED"
            else TreatmentStatus.IN_PROGRESS,
            acceptance=None,
        )
        assert parsed_location.report_date == datetime.fromisoformat(
            str(vuln_dict["reportDate"]),
        ).replace(tzinfo=ZoneInfo("America/Bogota"))
        assert parsed_location.exploitability == dummy_exploitability
        assert parsed_location.root_nickname == vuln_dict.get("rootNickname")
        assert parsed_location.compliance == get_policy_compliance(
            config=test_config,
            report_date=datetime.fromisoformat(str(vuln_dict["reportDate"])).replace(
                tzinfo=ZoneInfo("America/Bogota"),
            ),
            severity=Decimal(str(vuln_dict["severityTemporalScore"])),
            state=VulnerabilityState[str(vuln_dict["state"])],
        )

        vuln_dict["treatmentStatus"] = "ACCEPTED"
        vuln_dict["treatmentAcceptanceStatus"] = "SUBMITTED"
        parsed_accepted_location = parse_location(
            vuln_dict=vuln_dict,
            config=test_config,
            exploitability=dummy_exploitability,
        )

        assert parsed_accepted_location.treatment == Treatment(
            status=TreatmentStatus.ACCEPTED,
            acceptance=AcceptanceStatus.SUBMITTED,
        )


@pytest.mark.asyncio
async def test_compile_report(
    test_config: ForcesConfig,
    test_token: str,
    test_finding: str,
) -> None:
    report: ForcesData = await compile_raw_report(
        config=test_config,
        api_token=test_token,
    )
    find: Finding = next(find for find in report.findings if find.identifier == test_finding)
    assert len(find.vulnerabilities) == 1

    identifiers: set[str] = {find.identifier for find in report.findings}
    assert len(identifiers) == len(report.findings)

    assert report.summary.overall_compliance is False
    assert (
        report.summary.total
        == sum(len(finding.vulnerabilities) for finding in report.findings)
        == 39
    )
    assert sum(finding.unmanaged_vulns for finding in report.findings) == 39
    assert sum(finding.managed_vulns for finding in report.findings) == 0


@pytest.mark.asyncio
async def test_compile_report_with_preview(
    test_config: ForcesConfig,
    test_token: str,
    test_finding: str,
) -> None:
    report: ForcesData = await compile_raw_report(
        config=test_config._replace(feature_preview=True),
        api_token=test_token,
    )
    find: Finding = next(find for find in report.findings if find.identifier == test_finding)
    assert len(find.vulnerabilities) == 1

    identifiers: set[str] = {find.identifier for find in report.findings}
    assert len(identifiers) == len(report.findings)

    assert report.summary.overall_compliance is False
    assert (
        report.summary.total
        == report.summary.vulnerable.total_dynamic + report.summary.vulnerable.total_static
        == 38
    )


def test_style_summary() -> None:
    assert style_summary(VulnerabilityState.ACCEPTED, 1) == "1"
    assert style_summary(VulnerabilityState.VULNERABLE, 0) == "[green]0[/]"
    assert style_summary(VulnerabilityState.VULNERABLE, 9) == "[yellow3]9[/]"
    assert style_summary(VulnerabilityState.VULNERABLE, 17) == "[orange3]17[/]"
    assert style_summary(VulnerabilityState.VULNERABLE, 25) == "[bright_red]25[/]"
    assert style_summary(VulnerabilityState.SAFE, 15) == "[green]15[/]"


def test_style_report() -> None:
    assert style_report("tittle", "some_value") == "some_value"
    assert style_report("title", "some_value") == "[yellow]some_value[/]"
    assert style_report("state", "vulnerable") == "[bright_red]vulnerable[/]"
    assert style_report("state", "vulnerablee") == "vulnerablee"


def test_style_treatment() -> None:
    assert (
        style_treatment(
            Treatment(
                status=TreatmentStatus.ACCEPTED,
                acceptance=AcceptanceStatus.SUBMITTED,
            ),
        )
        == " [yellow3](pending temp treatment approval)[/]"
    )
    assert (
        style_treatment(
            Treatment(
                status=TreatmentStatus.ACCEPTED_UNDEFINED,
                acceptance=AcceptanceStatus.SUBMITTED,
            ),
        )
        == " [yellow3](pending perm treatment approval)[/]"
    )
    assert (
        style_treatment(
            Treatment(
                status=TreatmentStatus.UNTREATED,
                acceptance=None,
            ),
        )
        == ""
    )
    assert (
        style_treatment(
            Treatment(
                status=TreatmentStatus.IN_PROGRESS,
                acceptance=None,
            ),
        )
        == ""
    )


def test_filter_findings() -> None:
    vuln1 = Vulnerability(
        type=VulnerabilityType.DYNAMIC,
        technique=VulnerabilityTechnique.SCA,
        where="test_location",
        specific="test_specific",
        state=VulnerabilityState.VULNERABLE,
        severity=Decimal("7.5"),
        report_date=datetime.now(),
        root_nickname=None,
        treatment=Treatment(status=TreatmentStatus.UNTREATED),
        exploitability="High",
        compliance=True,
    )
    finding1 = Finding(
        identifier="test_id",
        title="test_title",
        status=FindingStatus.VULNERABLE,
        exploitability="High",
        severity=Decimal("7.5"),
        url="https://test.com",
        vulnerabilities=[vuln1],
    )
    finding2 = Finding(
        identifier="another_test_id",
        title="test_title",
        status=FindingStatus.VULNERABLE,
        exploitability="High",
        severity=Decimal("7.5"),
        url="https://test.com",
        vulnerabilities=[],
    )
    assert filter_findings({"test_id": finding1, "another_test_id": finding2}.values()) == tuple(
        [finding1],
    )


def test_filter_repo() -> None:
    vuln: Vulnerability = Vulnerability(
        type=VulnerabilityType.DYNAMIC,
        technique=VulnerabilityTechnique.SCA,
        where="somewhere",
        specific="port 21",
        state=VulnerabilityState.VULNERABLE,
        severity=Decimal("6.0"),
        report_date=datetime.now(tz=ZoneInfo("America/Bogota")),
        treatment=Treatment(status=TreatmentStatus.UNTREATED),
        exploitability="Proof-of-concept",
        root_nickname=None,
        compliance=True,
    )
    assert filter_repo(
        vuln=vuln,
        repo_name="root_test",
        kind=KindEnum.DYNAMIC,
    )
    assert filter_repo(
        vuln=vuln,
        kind=KindEnum.DYNAMIC,
    )


def test_filter_kind() -> None:
    vuln1 = Vulnerability(
        type=VulnerabilityType.DYNAMIC,
        technique=VulnerabilityTechnique.SCA,
        where="test_location",
        specific="test_specific",
        state=VulnerabilityState.VULNERABLE,
        severity=Decimal("7.5"),
        report_date=datetime.now(),
        root_nickname="myRoot",
        treatment=Treatment(status=TreatmentStatus.UNTREATED),
        exploitability="High",
        compliance=False,
    )
    assert filter_kind(vuln=vuln1, kind=KindEnum.ALL)
    assert not filter_kind(vuln=vuln1, kind=KindEnum.STATIC)
    assert filter_kind(vuln=vuln1, kind=KindEnum.DYNAMIC)
    assert filter_kind(
        vuln=vuln1._replace(type=VulnerabilityType.STATIC),
        kind=KindEnum.ALL,
    )
    assert filter_kind(
        vuln=vuln1._replace(type=VulnerabilityType.STATIC),
        kind=KindEnum.STATIC,
    )
    assert not filter_kind(
        vuln=vuln1._replace(type=VulnerabilityType.STATIC),
        kind=KindEnum.DYNAMIC,
    )


def test_filter_compliance() -> None:
    vuln1 = Vulnerability(
        type=VulnerabilityType.DYNAMIC,
        technique=VulnerabilityTechnique.SCA,
        where="test_location",
        specific="test_specific",
        state=VulnerabilityState.VULNERABLE,
        severity=Decimal("7.5"),
        report_date=datetime.now(),
        root_nickname="myRoot",
        treatment=Treatment(status=TreatmentStatus.UNTREATED),
        exploitability="High",
        compliance=False,
    )
    vuln2 = Vulnerability(
        type=VulnerabilityType.STATIC,
        technique=VulnerabilityTechnique.PTAAS,
        where="test_location2",
        specific="test_specific2",
        state=VulnerabilityState.VULNERABLE,
        severity=Decimal("3.0"),
        report_date=datetime.now(),
        root_nickname="myRoot",
        treatment=Treatment(status=TreatmentStatus.UNTREATED),
        exploitability="Functional",
        compliance=True,
    )

    assert filter_compliance(vulnerability=vuln1, verbose_level=1)
    assert filter_compliance(vulnerability=vuln1, verbose_level=2)
    assert not filter_compliance(vulnerability=vuln2, verbose_level=1)
    assert filter_compliance(vulnerability=vuln2, verbose_level=2)


def test_filter_repo_path() -> None:
    vuln1 = Vulnerability(
        type=VulnerabilityType.STATIC,
        where="WebGoat/src/main/resources/html/SqlInjectionAdvanced.html",
        technique=VulnerabilityTechnique.PTAAS,
        specific="10",
        state=VulnerabilityState.VULNERABLE,
        severity=Decimal("7.5"),
        report_date=datetime.now(),
        root_nickname=None,
        treatment=Treatment(status=TreatmentStatus.UNTREATED),
        exploitability="Functional",
        compliance=False,
    )
    assert filter_repo_paths(vuln=vuln1, repo_paths=tuple()) is True
    assert filter_repo_paths(vuln=vuln1, repo_paths=(".",)) is True
    assert filter_repo_paths(vuln=vuln1, repo_paths=("WebGoat/**",)) is True
    assert filter_repo_paths(vuln=vuln1, repo_paths=("WebGoat/src",)) is True
    assert filter_repo_paths(vuln=vuln1, repo_paths=("WebGoat/src/main/resources/html",)) is True
    assert filter_repo_paths(vuln=vuln1, repo_paths=("src",)) is True
    assert filter_repo_paths(vuln=vuln1, repo_paths=("src/*",)) is True
    assert filter_repo_paths(vuln=vuln1, repo_paths=("src/**",)) is True
    assert (
        filter_repo_paths(
            vuln=vuln1,
            repo_paths=(
                "random/path",
                "src",
            ),
        )
        is True
    )
    assert filter_repo_paths(vuln=vuln1, repo_paths=("src/main/resources/html/**",)) is True
    assert filter_repo_paths(vuln=vuln1, repo_paths=("folder/not/found",)) is False


def test_happy_path_format_finding_table() -> None:
    config = ForcesConfig(organization="test_org", group="test_group")
    vuln1 = Vulnerability(
        type=VulnerabilityType.DYNAMIC,
        where="test_location",
        technique=VulnerabilityTechnique.PTAAS,
        specific="test_specific",
        state=VulnerabilityState.VULNERABLE,
        severity=Decimal("7.5"),
        report_date=datetime.now(),
        root_nickname=None,
        treatment=Treatment(status=TreatmentStatus.UNTREATED),
        exploitability="Functional",
        compliance=True,
    )
    vuln2 = Vulnerability(
        type=VulnerabilityType.STATIC,
        where="test_location2",
        technique=VulnerabilityTechnique.SCA,
        specific="test_specific2",
        state=VulnerabilityState.SAFE,
        severity=Decimal("5.0"),
        report_date=datetime.now(),
        root_nickname=None,
        treatment=Treatment(status=TreatmentStatus.UNTREATED),
        exploitability="Proof-of-concept",
        compliance=True,
    )
    finding = Finding(
        identifier="test_id",
        title="test_title",
        status=FindingStatus.VULNERABLE,
        exploitability="Functional",
        severity=Decimal("7.5"),
        url="https://test.com",
        vulnerabilities=[vuln1, vuln2],
    )
    table = Table()
    result_table = format_finding_table(config, finding, table)
    assert len(result_table.rows) == 6
    cells = list(result_table.columns[0].cells)
    assert cells[0] == "title"
    assert cells[1] == "url"
    assert cells[2] == "state"
    assert cells[3] == "exploit"
    assert cells[4] == "vulnerable"
    assert cells[5] == "locations"
