# shellcheck shell=bash

function main {
  export API_ENDPOINT="https://localhost:8001/api"
  local args_pytest=(
    --cov-branch
    --cov forces
    --cov-fail-under '85'
    --cov-report 'term'
    --cov-report "xml:coverage.xml"
    --disable-pytest-warnings
    --no-cov-on-fail
    --verbose
  )

  aws_login "dev" "3600"
  sops_export_vars __argSecretsFile__ "TEST_FORCES_TOKEN"
  trap "integrates-db stop" EXIT
  DAEMON=true integrates-db
  DAEMON=true integrates-back dev
  sleep 40

  pushd forces || return 1
  source __argForcesRuntime__/template
  pytest "${args_pytest[@]}"
}

main "${@}"
