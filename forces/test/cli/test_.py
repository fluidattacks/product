import json
import os
from typing import (
    Any,
    get_args,
)

import pytest
from click.testing import (
    CliRunner,
)

from forces.cli import (
    main,
)
from forces.model import (
    CVSS,
    StatusCode,
)


def test_cli_invalid_config(test_token: str) -> None:
    runner = CliRunner()
    result = runner.invoke(
        main,
        (
            "--token",
            test_token,
            "--lax",
            "--dynamic",
            "--repo-name",
            "universes",
        ),
    )
    assert result.exit_code == StatusCode.ERROR, result.exception

    result = runner.invoke(
        main,
        ("--token", test_token, "--strict", "--repo-name", "universes"),
    )
    assert result.exit_code == StatusCode.ERROR, result.exception


def test_cli_strict_no_breaking(test_token: str) -> None:
    runner = CliRunner()
    result = runner.invoke(
        main,
        ("--token", test_token, "--strict", "--repo-name", "universe"),
    )
    assert result.exit_code == StatusCode.BREAK_BUILD, result.exception


def test_cli_strict_breaking_low(test_token: str) -> None:
    runner = CliRunner()
    result = runner.invoke(
        main,
        (
            "--token",
            test_token,
            "--strict",
            "--repo-name",
            "universe",
            "--breaking",
            "2",
        ),
    )
    assert result.exit_code == StatusCode.BREAK_BUILD, result.exception


def test_cli_strict_breaking_high(test_token: str) -> None:
    runner = CliRunner()
    result = runner.invoke(
        main,
        (
            "--token",
            test_token,
            "--strict",
            "--repo-name",
            "universe",
            "--breaking",
            "10",
        ),
    )
    assert result.exit_code == StatusCode.SUCCESS, result.exception


def test_cli_repo_path(test_token: str) -> None:
    runner = CliRunner()

    result = runner.invoke(main, ("--token", test_token, "--lax", "--repo-path", "."))
    assert result.exit_code == StatusCode.SUCCESS, result.exception

    result2 = runner.invoke(main, ("--token", test_token, "--strict", "--repo-path", "universe"))
    assert result2.exit_code == StatusCode.BREAK_BUILD, result2.exception

    result3 = runner.invoke(
        main,
        (
            "--token",
            test_token,
            "--strict",
            "--repo-path",
            "universe/path/to/npm",
            "--repo-path",
            "universe/test",
        ),
    )
    assert result3.exit_code == StatusCode.BREAK_BUILD, result3.exception


def test_cli_static(test_token: str) -> None:
    runner = CliRunner()
    result = runner.invoke(
        main,
        (
            "--token",
            test_token,
            "--static",
            "--repo-name",
            "universe",
            "--repo-path",
            "universe/common",
            "--repo-path",
            "skims/**",
        ),
    )
    assert result.exit_code == StatusCode.SUCCESS, result.exception


def test_cli_cvss(test_token: str) -> None:
    runner = CliRunner()
    for valid_cvss in get_args(CVSS):
        result = runner.invoke(
            main,
            (
                "--token",
                test_token,
                "--cvss",
                valid_cvss,
            ),
        )
        assert result.exit_code == StatusCode.SUCCESS, result.exception

    invalid_result = runner.invoke(
        main,
        (
            "--token",
            test_token,
            "--cvss",
            "-1",
        ),
    )
    assert invalid_result.exit_code == StatusCode.INVALID, result.exception


@pytest.mark.skip(reason="Functionality not yet implemented")
def test_cli_repo_args_validation(test_token: str) -> None:
    runner = CliRunner()

    result_repo_name = runner.invoke(
        main,
        (
            "--token",
            test_token,
            "--repo-name",
            "universe",
        ),
    )
    assert result_repo_name.exit_code == StatusCode.SUCCESS, result_repo_name.exception

    result_repo_url = runner.invoke(
        main,
        (
            "--token",
            test_token,
            "--repo-url",
            "https://gitlab.com/fluidattacks/universe.git",
        ),
    )
    assert result_repo_url.exit_code == StatusCode.SUCCESS, result_repo_url.exception

    result_both = runner.invoke(
        main,
        (
            "--token",
            test_token,
            "--repo-name",
            "universe",
            "--repo-url",
            "https://gitlab.com/fluidattacks/universe.git",
        ),
    )
    assert result_both.exit_code == StatusCode.INVALID, result_both.exception

    result_both2 = runner.invoke(
        main,
        (
            "--token",
            test_token,
            "--repo-url",
            "https://gitlab.com/fluidattacks/universe.git",
            "--repo-name",
            "universe",
        ),
    )
    assert result_both2.exit_code == StatusCode.INVALID, result_both2.exception


def test_cli_out_to_file(test_token: str) -> None:
    runner = CliRunner()
    with runner.isolated_filesystem():
        result = runner.invoke(
            main,
            (
                "--token",
                test_token,
                "--strict",
                "--output",
                "test.json",
                "--repo-path",
                ".",
            ),
        )
        assert os.path.exists(f"{os.getcwd()}/test.json"), result.exception
        with open(f"{os.getcwd()}/test.json", encoding="utf-8") as report_file:
            report: dict[str, Any] = json.load(report_file)
        assert "findings" in report
        assert all(
            key in report["findings"][0]
            for key in (
                "identifier",
                "title",
                "status",
                "exploitability",
                "severity",
                "vulnerabilities",
            )
        )
        assert all(
            key in report["findings"][0]["vulnerabilities"][0]
            for key in (
                "type",
                "where",
                "specific",
                "state",
                "severity",
                "report_date",
                "root_nickname",
                "exploitability",
                "compliance",
            )
        )
        assert "summary" in report
        assert all(
            key in report["summary"]
            for key in (
                "vulnerable",
                "overall_compliance",
                "elapsed_time",
                "total",
            )
        )
        assert report["summary"]["overall_compliance"] is False


def test_cli_lax_feature_preview(test_token: str) -> None:
    runner = CliRunner()
    result = runner.invoke(
        main,
        (
            "--token",
            test_token,
            "--lax",
            "--repo-name",
            "universe",
            "--feature-preview",
        ),
    )
    assert result.exit_code == StatusCode.SUCCESS, result.exception


def test_cli_strict_feature_preview(test_token: str) -> None:
    runner = CliRunner()
    result = runner.invoke(
        main,
        (
            "--token",
            test_token,
            "--strict",
            "--repo-name",
            "universe",
            "--feature-preview",
        ),
    )
    assert result.exit_code == StatusCode.BREAK_BUILD, result.exception
