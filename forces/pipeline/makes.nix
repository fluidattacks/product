let
  arch = let
    commit = "7e4ff44f363466e98d0d159d2576a50b67c8a376";
    sha256 = "sha256:0s7kv8sbmr6nsflayvp134vq4yhpn0casjjzjildjb1gx9fi3pyb";
    url =
      "https://gitlab.com/fluidattacks/universe/-/raw/${commit}/common/ci/arch.nix";
    src = builtins.fetchurl { inherit sha256 url; };
  in import src;

  rules = {
    integratesAndForces = {
      default = arch.core.rules.titleRule {
        products = [ "all" "forces" "integrates[^-]" ];
        types = [ ];
      };
      noChore = arch.core.rules.titleRule {
        products = [ "all" "forces" "integrates" ];
        types = [ "feat" "fix" "refac" ];
      };
    };
    onlyForces = let products = [ "all" "forces" ];
    in {
      default = arch.core.rules.titleRule {
        inherit products;
        types = [ ];
      };
      noChore = arch.core.rules.titleRule {
        inherit products;
        types = [ "feat" "fix" "refac" ];
      };
    };
  };
in {
  pipelines = {
    forces = {
      gitlabPath = "/forces/pipeline/default.yaml";
      jobs = [
        {
          output = "/pipelineOnGitlab/forces";
          gitlabExtra = arch.extras.default // {
            rules = arch.rules.dev ++ [ rules.onlyForces.noChore ];
            stage = arch.stages.test;
            tags = [ arch.tags.forces ];
          };
        }
        {
          output = "/deployContainer/forcesAmd64";
          gitlabExtra = arch.extras.default // {
            retry = 2;
            rules = arch.rules.prod ++ [ rules.onlyForces.default ];
            stage = arch.stages.deploy;
            tags = [ arch.tags.common-x86 ];
          };
        }
        {
          output = "/deployContainer/forcesArm64";
          gitlabExtra = arch.extras.default // {
            retry = 2;
            rules = arch.rules.prod ++ [ rules.onlyForces.default ];
            stage = arch.stages.deploy;
            tags = [ arch.tags.forces ];
          };
        }
        {
          output = "/deployContainerManifest/forces";
          gitlabExtra = arch.extras.default // {
            needs =
              [ "/deployContainer/forcesAmd64" "/deployContainer/forcesArm64" ];
            retry = 2;
            rules = arch.rules.prod ++ [ rules.onlyForces.default ];
            stage = arch.stages.deploy;
            tags = [ arch.tags.forces ];
          };
        }
        {
          output = "/forces/test";
          gitlabExtra = arch.extras.default // {
            artifacts = {
              paths = [ "forces/coverage.xml" ];
              expire_in = "1 day";
            };
            rules = arch.rules.dev ++ [ rules.integratesAndForces.noChore ];
            stage = arch.stages.test;
            tags = [ arch.tags.forces ];
          };
        }
        {
          output = "/forces/coverage";
          gitlabExtra = arch.extras.default // {
            needs = [ "/forces/test" ];
            rules = arch.rules.dev ++ [ rules.onlyForces.noChore ];
            stage = arch.stages.deploy;
            tags = [ arch.tags.forces ];
            variables.GIT_DEPTH = 1000;
          };
        }
        {
          output = "/forces/lint";
          gitlabExtra = arch.extras.default // {
            cache = {
              key = "$CI_COMMIT_REF_NAME-forces-lint";
              paths = [ "forces/.ruff_cache" "forces/.mypy_cache" ];
            };
            rules = arch.rules.dev ++ [ rules.onlyForces.noChore ];
            stage = arch.stages.test;
            tags = [ arch.tags.forces ];
          };
        }
      ];
    };
  };
}
