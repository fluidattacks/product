{ makeSearchPaths, outputs, ... }: {
  imports = [ ./container/makes.nix ./coverage/makes.nix ./pipeline/makes.nix ];
  dev.forces.source = [
    outputs."/common/dev/global_deps"
    outputs."/forces/env"
    (makeSearchPaths { pythonPackage = [ "$PWD/forces" ]; })
  ];
}
