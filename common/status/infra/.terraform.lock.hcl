# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/checkly/checkly" {
  version     = "1.8.0"
  constraints = "1.8.0"
  hashes = [
    "h1:4KiFee/jYaAhzr8QLHdA6HDvwaK2uO2JEUP8Hz8HIPQ=",
    "zh:0f9fbe83063b1def4a7580035ebb82c58ea59b6b56ee830bc21caf3447ccd160",
    "zh:2fcf3db85e74000adca2a6f7f51d85afc46488ba21e8af3edfab6aedeb96ce26",
    "zh:4024d7658e5606ac8fe844e2c62383b9566d1c308c22a131e1774be1d9d78683",
    "zh:47cc874ff04b2cac32203dfc9d56ae413425eee423e66b8409cbcb2c72a349ae",
    "zh:49e4a2d11586c84cc23f74026f4c28bd42b3deab69f0d705dd4d2f28429c9970",
    "zh:59609dc56e02fa2f8749671b8cfc337647de04b06d2b747691d603b6a6b472c6",
    "zh:6982e0564dde5f456827f88afe36d03394fb70c547d5b208cee34aee3f0fd396",
    "zh:88c380cb440198618eade6511b58641ea4ededc87be877536bb5f0d9732c323c",
    "zh:9ddddd1a127ba05cc9db7b30751931c8127ba2d311b8f3e166810d5701a05271",
    "zh:a2efe4c2a655f142808301c9c3cbea951c826addf2feee6fbf7985a769e302fa",
    "zh:a773132d7c2ec403c562f9d4d87a18a509056a179b5ee0854b92e741e2fc5c05",
    "zh:af771766d50b45e0bfe609b6d2e7641547afbead32a93fd36af9e6a710cbfc9b",
    "zh:cfd030f63b6901beceb15b6284940f730886221649eaa3d445d99947b90830d9",
    "zh:ee656c6224c13754f7efe2d892798986fc748dab020332e86b44afda0643a24b",
  ]
}

provider "registry.terraform.io/hashicorp/aws" {
  version     = "5.34.0"
  constraints = "5.34.0"
  hashes = [
    "h1:1Y1JgV1z99QqAK06+atyfNqreZxyGZKbm4mZO4VhhT8=",
    "h1:Tbq6dKE+XyXmkup6+7eQj2vH+eCJipk8R3VXhebVYi4=",
    "zh:01bb20ae12b8c66f0cacec4f417a5d6741f018009f3a66077008e67cce127aa4",
    "zh:3b0c9bdbbf846beef2c9573fc27898ceb71b69cf9d2f4b1dd2d0c2b539eab114",
    "zh:5226ecb9c21c2f6fbf1d662ac82459ffcd4ad058a9ea9c6200750a21a80ca009",
    "zh:6021b905d9b3cd3d7892eb04d405c6fa20112718de1d6ef7b9f1db0b0c97721a",
    "zh:9b12af85486a96aedd8d7984b0ff811a4b42e3d88dad1a3fb4c0b580d04fa425",
    "zh:9e61b8e0ccf923979cd2dc1f1140dbcb02f92248578e10c1996f560b6306317c",
    "zh:ad6bf62cdcf531f2f92f6416822918b7ba2af298e4a0065c6baf44991fda982d",
    "zh:b698b041ef38837753bbe5265dddbc70b76e8b8b34c5c10876e6aab0eb5eaf63",
    "zh:bb799843c534f6a3f072a99d93a3b53ff97c58a96742be15518adf8127706784",
    "zh:cebee0d942c37cd3b21e9050457cceb26d0a6ea886b855dab64bb67d78f863d1",
    "zh:e061fdd1cb99e7c81fb4485b41ae000c6792d38f73f9f50aed0d3d5c2ce6dcfb",
    "zh:eeb4943f82734946362696928336357cd1d36164907ae5905da0316a67e275e1",
    "zh:ef09b6ad475efa9300327a30cbbe4373d817261c8e41e5b7391750b16ef4547d",
    "zh:f01aab3881cd90b3f56da7c2a75f83da37fd03cc615fc5600a44056a7e0f9af7",
    "zh:fcd0f724ebc4b56a499eb6c0fc602de609af18a0d578befa2f7a8df155c55550",
  ]
}
