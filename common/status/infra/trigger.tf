resource "checkly_trigger_group" "apis" {
  group_id = checkly_check_group.apis_group.id
}

resource "checkly_trigger_group" "medium_frequency" {
  group_id = checkly_check_group.medium_frequency_group.id
}

resource "checkly_trigger_group" "web" {
  group_id = checkly_check_group.web_group.id
}

resource "checkly_trigger_group" "internal_web" {
  group_id = checkly_check_group.internal_web_group.id
}
