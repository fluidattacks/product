resource "checkly_check" "airs" {
  name                      = "WEB"
  type                      = "BROWSER"
  activated                 = true
  frequency                 = 10
  use_global_alert_settings = false
  runtime_id                = "2024.09"
  group_id                  = checkly_check_group.web_group.id
  group_order               = 1

  script = <<-EOF
    const { expect, test } = require("@playwright/test");

    test("airs", async ({ page }) => {
      await page.goto("https://fluidattacks.com/", {
        waitUntil: "domcontentloaded"
      });
      await expect(page).toHaveTitle("Application security testing solutions | Fluid Attacks");
    });
  EOF
}
