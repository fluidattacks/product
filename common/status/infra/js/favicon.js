function setFavIcons(favImg) {
  let headTitle = document.querySelector("head");
  let setFavicon = document.createElement("link");
  setFavicon.setAttribute("rel", "shortcut icon");
  setFavicon.setAttribute("href", favImg);
  headTitle.appendChild(setFavicon);
}

function setTitleTag() {
  let headTag = document.querySelector("head");
  let setTag = document.createElement("title");
  let title = document.createTextNode("Availability Fluid Attacks");
  setTag.appendChild(title);
  headTag.appendChild(setTag);
}

setFavIcons(
  "https://res.cloudinary.com/fluid-attacks/image/upload/v1677710689/airs/favicon_bkgq3p.png"
);
setTitleTag();
