resource "checkly_check" "platform" {
  name                      = "PLATFORM"
  type                      = "BROWSER"
  activated                 = true
  frequency                 = 1
  use_global_alert_settings = false
  runtime_id                = "2024.09"
  group_id                  = checkly_check_group.medium_frequency_group.id
  group_order               = 1

  script = file("${path.module}/js/integrates_script.js")
}

resource "checkly_check" "integrates_api" {
  name                      = "API"
  type                      = "API"
  activated                 = true
  frequency                 = 0
  frequency_offset          = 10
  use_global_alert_settings = false
  runtime_id                = "2024.09"
  group_id                  = checkly_check_group.apis_group.id
  group_order               = 1

  request {
    url              = "https://app.fluidattacks.com/api"
    follow_redirects = true
    body_type        = "GRAPHQL"
    method           = "POST"

    headers = {
      authorization = "Bearer {{INTEGRATES_API_TOKEN}}"
    }

    assertion {
      source     = "TEXT_BODY"
      property   = "(.*)"
      comparison = "CONTAINS"
      target     = "narrabri"
    }
    assertion {
      source     = "TEXT_BODY"
      property   = "(.*)"
      comparison = "CONTAINS"
      target     = "imamura"
    }

    body = <<-EOF
      query ChecklyApiCheck {
        me {
          organizations {
            name
          groups {
            name
          }
        }
          remember
        }
        organization(organizationId: "ORG#0d6d8f9d-3814-48f8-ba2c-f4fb9f8d4ffa") {
          userRole
          groups {
            name
          }
        }
        group(groupName: "narrabri") {
          permissions
          findings {
            vulnerabilitiesConnection(
              first: 10
              state: VULNERABLE
            ) {
                edges {
                    node {
                        id
                        where
                    }
                }
            }
          }
        }
      }
    EOF
  }
}

resource "checkly_check" "forces_api" {
  name                      = "AGENT"
  type                      = "API"
  activated                 = true
  frequency                 = 0
  frequency_offset          = 10
  use_global_alert_settings = false
  runtime_id                = "2024.09"
  group_id                  = checkly_check_group.apis_group.id
  group_order               = 2

  request {
    url              = "https://app.fluidattacks.com/api"
    follow_redirects = true
    body_type        = "GRAPHQL"
    method           = "POST"

    headers = {
      authorization = "Bearer {{INTEGRATES_API_TOKEN}}"
    }

    assertion {
      source     = "STATUS_CODE"
      comparison = "EQUALS"
      target     = "200"
    }
    assertion {
      source     = "TEXT_BODY"
      property   = "(.*)"
      comparison = "CONTAINS"
      target     = "CREATED"
    }
    assertion {
      source     = "TEXT_BODY"
      property   = "(.*)"
      comparison = "CONTAINS"
      target     = "UNTREATED"
    }
    assertion {
      source     = "TEXT_BODY"
      property   = "(.*)"
      comparison = "CONTAINS"
      target     = "VULNERABLE"
    }
    assertion {
      source     = "TEXT_BODY"
      property   = "(.*)"
      comparison = "CONTAINS"
      target     = "bwapp"
    }

    body = <<-EOF
      query ChecklyForcesCheck {
        group(groupName: "narrabri") {
          findings {
            id
            currentState
            title
            status
            severityScore
            severityVector
          }
          forcesVulnerabilities(first: 10) {
            edges {
              node {
                findingId
                state
                technique
                treatmentStatus
                treatmentAcceptanceStatus
                vulnerabilityType
                where
                severityTemporalScore
                specific
                reportDate
                rootNickname
                zeroRisk
              }
            }
          }
        }
      }
    EOF
  }
}
