{ fetchNixpkgs, makeScript, projectPath, ... }@makes_inputs:
let
  root = projectPath "/common/ai/lead-scoring";
  pkg = import "${root}/entrypoint.nix" makes_inputs;
  check = pkg.check.tests;
  env = pkg.env.dev;
in {
  jobs."/common/ai/lead-scoring/check/tests" = makeScript {
    searchPaths = { bin = [ check env ]; };
    name = "lead-scoring-check-tests";
    entrypoint = ./entrypoint.sh;
  };
}
