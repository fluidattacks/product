from typing import Any

import pandas as pd

import lead_scoring.src.functions as fn
import lead_scoring.src.inference as inf

mql_pipeline_key = "lead-scoring/mql_pipeline_02.joblib"
mql_model_key = "lead-scoring/mql_02.joblib"
sql_pipeline_key = "lead-scoring/sql_pipeline.joblib"
sql_model_key = "lead-scoring/sql.joblib"


mql_pipeline = fn.load_joblib_from_s3(mql_pipeline_key)
mql_model = fn.load_joblib_from_s3(mql_model_key)

sql_pipeline = fn.load_joblib_from_s3(sql_pipeline_key)
sql_model = fn.load_joblib_from_s3(sql_model_key)

output_path = "contact_scoring_inference.csv"
var_id = "contact_id"

data_to_mql_model = [
    var_id,
    "country",
    "segment",
    "lsource",
    "job",
    "department",
    "indusgroup",
]

data_to_sql_model = [
    var_id,
    "country",
    "segment",
    "lsource",
    "job",
    "department",
    "indusgroup",
    "autoridad",
    "git",
]

list_columns_output = ["contact_id", "sql", "mql"]


def contacts_inference(data: pd.DataFrame, con: Any) -> None:
    # heck if the DataFrame is empty
    if data.empty:  # If no results are returned
        print("No results were found for the given query.")  # Log a message
        # Create a DataFrame with the current date and a message
        con.close()
        list_columns = list_columns_output
        inf.inference_empty_df(list_columns, output_path)
        print("Inference process completed")
    else:
        print("Query executed successfully. Data loaded into DataFrame.")
        data.columns = data.columns.str.lower()
        print("Data loaded into DataFrame")
        print(data.shape)
        con.close()
        print("Database connection closed successfully")
        inference_mql_probability, id_column_mql = inf.inference_model(
            var_id, data_to_mql_model, data, mql_pipeline, mql_model, name_model="mql"
        )

        inference_sql_probability, _ = inf.inference_model(
            var_id, data_to_sql_model, data, sql_pipeline, sql_model, name_model="sql"
        )

        inf.output_model_mql_sql(
            var_id,
            id_column_mql,
            inference_sql_probability,
            inference_mql_probability,
            output_path,
        )
