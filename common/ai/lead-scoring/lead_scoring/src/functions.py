import csv
from io import (
    BytesIO,
)
from typing import (
    List,
    Tuple,
)

import boto3
import joblib  # type: ignore [import-untyped]
import numpy as np
import pandas as pd
from sklearn.base import BaseEstimator  # type: ignore [import-untyped]
from sklearn.pipeline import Pipeline  # type: ignore [import-untyped]


def load_joblib_from_s3(s3_key: str) -> str:
    """
    Loads a .joblib file from an S3 bucket into memory and returns the deserialized object.

    Args:
        bucket_name (str): The name of the S3 bucket where the .joblib file is stored.
        s3_key (str): The key (path) to the .joblib file in the S3 bucket.
        s3 (str): set up boto3.client

    Returns:
        Any: The object deserialized from the .joblib file.
    """
    s3 = boto3.client("s3")
    bucket_name = "business-models"
    response = s3.get_object(Bucket=bucket_name, Key=s3_key)  # Fetch the object from S3
    return joblib.load(
        BytesIO(response["Body"].read())
    )  # Load and deserialize the joblib file from memory


def encode_features(df_to_model: pd.DataFrame, pipeline: Pipeline) -> pd.DataFrame:
    """
    Transforms the input features using a pre-loaded preprocessing pipeline.

    Args:
        df_to_model (pd.DataFrame): DataFrame containing features to be encoded.
        pipeline_path (str): Path to the joblib file containing the preprocessor pipeline.

    Returns:
        pd.DataFrame: Transformed features after encoding.
    """
    preprocessor = pipeline.named_steps["preprocessor"]
    categorical_columns = preprocessor.transformers_[0][2]
    df_to_model = df_to_model[categorical_columns]

    df_encoded = pipeline.transform(df_to_model)
    return df_encoded


def output_csv(file_name: str, df: pd.DataFrame) -> None:
    """
    Writes the DataFrame to a CSV file

    Args:
        file_name (str): Path where the CSV file will be saved.
        df (pd.DataFrame): DataFrame to be written to the CSV file.
    """
    with open(file_name, mode="w", newline="", encoding="utf-8") as file:
        writer = csv.writer(file)
        writer.writerow(df.columns)
        for index, row in df.iterrows():
            writer.writerow(row)


def perform_inference(df_encoded: pd.DataFrame, model: BaseEstimator) -> np.ndarray:
    """
    Performs inference using a pre-loaded model.

    Args:
        df_encoded (pd.DataFrame): DataFrame containing encoded features.
        model_path (str): Path to the joblib file containing the trained model.

    Returns:
        np.ndarray: Array of prediction probabilities for each class.
    """

    inference_probability = model.predict_proba(df_encoded)
    return inference_probability


def prepare_output(
    var_id: str,
    id_column: pd.Series,
    inference_sql_proba: np.ndarray,
    inference_mql_proba: np.ndarray,
) -> pd.DataFrame:
    """
    Prepares a DataFrame with conversion probabilities for output.

    Args:
        lead_id (pd.Series): Series containing lead IDs.
        inference_-_proba (np.ndarray): Array of prediction probabilities.

    Returns:
        pd.DataFrame: DataFrame with lead IDs and conversion probabilities.
    """
    sql_result = pd.DataFrame(
        {
            var_id: id_column,
            "non_conversion_prob": inference_sql_proba[:, 0],
            "conversion_prob": inference_sql_proba[:, 1],
        }
    )
    sql_to_crm = sql_result.copy()
    sql_to_crm["sql"] = (sql_to_crm.conversion_prob * 100).round(2)
    sql_to_crm = sql_to_crm[[var_id, "sql"]]

    mql_result = pd.DataFrame(
        {
            var_id: id_column,
            "non_conversion_prob": inference_mql_proba[:, 0],
            "conversion_prob": inference_mql_proba[:, 1],
        }
    )
    mql_to_crm = mql_result.copy()
    mql_to_crm["mql"] = (mql_to_crm.conversion_prob * 100).round(2)
    mql_to_crm = mql_to_crm[[var_id, "mql"]]
    df_to_crm = pd.merge(sql_to_crm, mql_to_crm, on=var_id, how="inner")

    return df_to_crm


def preprocess_features(
    var_id: str, df: pd.DataFrame, columns_to_model: List[str]
) -> Tuple[pd.Series, pd.DataFrame]:
    """
    Extracts and preprocessor features from the DataFrame.

    Args:
        df (pd.DataFrame): DataFrame containing raw data.
        columns_to_model (list of str): List of column names to be used for modeling.

    Returns:
        tuple: A tuple containing:
            - pd.Series: Series with lead IDs.
            - pd.DataFrame: DataFrame with features used for modeling (excluding lead IDs).
    """
    df_feat = df[columns_to_model]
    lead_id = df_feat[var_id]
    df_to_model = df_feat.drop(columns=[var_id])
    return lead_id, df_to_model
