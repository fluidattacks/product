from typing import (
    List,
    Tuple,
)

import numpy as np
import pandas as pd
from sklearn.base import BaseEstimator  # type: ignore [import-untyped]
from sklearn.pipeline import Pipeline  # type: ignore [import-untyped]

import lead_scoring.src.functions as fn


def inference_model(
    var_id: str,
    columns_to_model: List[str],
    data: pd.DataFrame,
    pipeline: Pipeline,
    model: BaseEstimator,
    name_model: str,
) -> Tuple[np.ndarray, pd.Series]:
    # Preprocessor

    if name_model == "mql":
        data["segment"] = data["segment"].replace("none", "growth")

    id_column, df_model = fn.preprocess_features(var_id, data, columns_to_model)
    print("Preprocessing features successfully")

    # Encoded
    df_encoded = fn.encode_features(df_model, pipeline)
    print("Encoding features successfully")

    # Inference
    inference_model_probability = fn.perform_inference(df_encoded, model)
    print(f"{name_model} inference performed successfully")

    return inference_model_probability, id_column


def inference_empty_df(list_columns: list, file_name: str) -> None:
    df_to_crm = pd.DataFrame(columns=list_columns)

    fn.output_csv(file_name, df_to_crm)
    print(f"Inference CSV empty saved in {file_name}: OK")


def output_model_mql_sql(
    var_id: str,
    id_column: pd.Series,
    inference_sql_probability: np.ndarray,
    inference_mql_probability: np.ndarray,
    output_path: str,
) -> None:
    # Set up and save inference
    df_to_crm = fn.prepare_output(
        var_id, id_column, inference_sql_probability, inference_mql_probability
    )
    print("Output prepared successfully")

    df_to_crm.rename(columns={var_id: "id"}, inplace=True)
    fn.output_csv(output_path, df_to_crm)
    print(f"Inference CSV saved in {output_path}: OK")


def output_model_mql(
    id_column: pd.Series,
    var_id: str,
    inference_mql_probability: np.ndarray,
    output_path: str,
) -> None:
    mql_result = pd.DataFrame(
        {
            var_id: id_column,
            "non_conversion_prob": inference_mql_probability[:, 0],
            "conversion_prob": inference_mql_probability[:, 1],
        }
    )

    mql_to_crm = mql_result.copy()
    mql_to_crm["mql"] = (mql_to_crm.conversion_prob * 100).round(2)
    mql_to_crm = mql_to_crm[[var_id, "mql"]]

    fn.output_csv(output_path, mql_to_crm)
    print(f"Inference CSV saved in {output_path}: OK")
