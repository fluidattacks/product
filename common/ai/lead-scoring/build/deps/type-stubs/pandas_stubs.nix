{ lib, python_pkgs, }:
let version = "2.2.2.240909";
in python_pkgs.pandas-stubs.overridePythonAttrs (old: {
  inherit version;
  src = lib.fetchPypi {
    pname = "pandas_stubs";
    inherit version;
    hash = "sha256-PAlRosPkXjR1rtnYC3FHroLxdrnkLp+zIc/evz1BGz0=";
  };
  doCheck = false;

  propagatedBuildInputs = [ python_pkgs.pandas ];
})
