{ inputs, makePythonVscodeSettings, projectPath, ... }@makes_inputs:
let
  root = projectPath "/common/ai/lead-scoring";
  bundle = import "${root}/entrypoint.nix" makes_inputs;
in {
  jobs."/common/ai/lead-scoring/env/dev" = makePythonVscodeSettings {
    env = bundle.env.dev;
    bins = [ ];
    name = "lead-scoring-env-dev";
  };
}
