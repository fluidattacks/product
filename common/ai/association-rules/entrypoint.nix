{ inputs, makePythonPyprojectPackage, projectPath, pythonOverrideUtils, ... }:
let
  raw_nixpkgs = let
    owner = "NixOS";
    repo = "nixpkgs";
    rev = "cb6b16cd3c744f3c893456453a6155382b18bd9f";
    src = builtins.fetchTarball {
      sha256 = "034i1f3xwr7q6za721ajawc2kk6hvw4a9ryiv0b5ysm76ml052c6";
      url = "https://github.com/${owner}/${repo}/archive/${rev}.tar.gz";
    };
  in import src { };
  pynix = let
    commit = "d1f6b952e97fb0c0aca8ef6d357fe128394fe9fa";
    url =
      "https://gitlab.com/dmurciaatfluid/python_nix_builder/-/archive/${commit}/python_nix_builder-${commit}.tar";
    src = builtins.fetchTarball {
      inherit url;
      sha256 = "0i3hp5lya60ry69c02y4qn0xbbddy7xy01d6saiiymbb67clidzj";
    };
  in import "${src}/pynix" {
    inherit nixpkgs;
    pythonVersion = "python311"; # select a python version
  };
  python_version = "python311";
  makes_inputs = {
    inherit (inputs) nix-filter;
    inherit projectPath makePythonPyprojectPackage pythonOverrideUtils;
  };
  nixpkgs = raw_nixpkgs // { inherit (inputs) nix-filter; };
  out = import ./build {
    inherit pynix makes_inputs nixpkgs python_version;
    src = inputs.nix-filter {
      root = ./.;
      include =
        [ "association_rules" "tests" "pyproject.toml" "mypy.ini" "ruff.toml" ];
    };
  };
in out
