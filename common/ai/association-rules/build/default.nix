{ makes_inputs, nixpkgs, pynix, python_version, src, }:
let
  deps = import ./deps { inherit makes_inputs nixpkgs pynix python_version; };
  pkgDeps = {
    runtime_deps = with deps.python_pkgs; [
      mlxtend
      pandas
      pandas-stubs
      snowflake-connection
    ];
    build_deps = with deps.python_pkgs; [ flit-core ];
    test_deps = with deps.python_pkgs; [
      mypy
      pytest
      (pynix.patch.homelessPatch ruff)
    ];
  };
  bundle = pynix.stdBundle { inherit pkgDeps src; };
in bundle
