{ makes_inputs, nixpkgs, pynix, python_pkgs, python_version, }:
let
  raw_src = makes_inputs.projectPath "/sorts/snowflake-connection";
  src = import "${raw_src}/build/filter.nix" nixpkgs.nix-filter raw_src;
  bundle = import "${raw_src}/build" {
    inherit makes_inputs nixpkgs python_version src;
  };
  snowflake_connection =
    python_pkgs.snowflake-connector-python.overridePythonAttrs
    (old: { doCheck = false; });
  pkg = pynix.overrideUtils.deepPkgOverride
    (pynix.patch.homelessPatch snowflake_connection) bundle.pkg;
in pkg
