{ makes_inputs, pynix, nixpkgs, python_version, }:
let
  utils = makes_inputs.pythonOverrideUtils;
  pkgs_overrides = override: python_pkgs:
    builtins.mapAttrs (_: override python_pkgs) python_pkgs;
  layer_1 = python_pkgs:
    python_pkgs // {
      mlxtend = python_pkgs.mlxtend.overridePythonAttrs (old: rec {
        version = "0.19.0";
        src = nixpkgs.fetchFromGitHub {
          owner = "rasbt";
          repo = old.pname;
          rev = "v0.19.0";
          sha256 = "XC1ZYRyDgrNfS3B6gCHTv/Znvu1uAJusCMZ9sj/9XGE=";
        };
        meta = old.meta // { broken = false; };
        doCheck = false;
      });
      pandas-stubs = python_pkgs.pandas-stubs.overridePythonAttrs (_: {
        pythonImportsCheck = [ ];
        doCheck = false;
      });
      snowflake-connection = let
        result = import ./snowflake_connection.nix {
          inherit pynix makes_inputs nixpkgs python_pkgs python_version;
        };
      in result;
    };
  overrides = map pkgs_overrides [ ];

  python_pkgs =
    utils.compose (overrides ++ [ layer_1 ]) pynix.lib.pythonPackages;
in { inherit python_pkgs; }
