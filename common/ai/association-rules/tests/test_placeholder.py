def test_placeholder() -> None:
    temp_var = 1
    assert temp_var + 1 == 2  # noqa: PLR2004
