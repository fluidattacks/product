"""Association rules core module."""

import logging
from collections.abc import (
    Iterable,
)
from pathlib import (
    Path,
)
from typing import (
    Any,
)

import pandas as pd
from mlxtend.frequent_patterns import (  # type: ignore[import-untyped]
    apriori,
    association_rules,
)
from mlxtend.preprocessing import (  # type: ignore[import-untyped]
    TransactionEncoder,
)
from pandas.core.frame import (
    DataFrame,
)

LOG = logging.getLogger(__name__)


def execute_query(cur: Any, query: str) -> list[tuple[str, ...]]:  # type: ignore[misc]
    """Execute a query and return the results."""
    cur.execute(query)  # type: ignore[misc]
    data = cur.fetchall()  # type: ignore[misc]
    cur.close()  # type: ignore[misc]
    return list(data)  # type: ignore[misc]


def generate_dataset(data: Iterable[tuple[str, ...]]) -> list[list[str]]:
    """Generate a dataset from an iterable of tuples."""
    data_dict = {}
    for reg in data:
        index = reg[1] + "-" + reg[5] + "-" + reg[8]
        if index not in data_dict:
            data_dict[index] = {reg[4]}
        else:
            data_dict[index].add(reg[4])
    return [list(x) for x in data_dict.values()]


def generate_sparse_matrix(dataset: list[list[str]]) -> DataFrame:
    """Generate a sparse matrix from raw data."""
    tr_enc = TransactionEncoder()  # type: ignore[misc]
    sparse_dataset = tr_enc.fit(dataset).transform(dataset, sparse=True)  # type: ignore[misc]
    return pd.DataFrame.sparse.from_spmatrix(  # type: ignore[misc, attr-defined, no-any-return]
        sparse_dataset,  # type: ignore[misc]
        columns=tr_enc.columns_,  # type: ignore[misc]
    )


def get_item_sets(
    sparse_df: DataFrame,
    min_support: float = 0.00004,
    use_colnames: bool = True,
    verbose: int = 1,
) -> DataFrame:
    """Get the item sets from the sparse dataframe."""
    return apriori(  # type: ignore[misc, no-any-return]
        sparse_df,
        min_support=min_support,
        use_colnames=use_colnames,
        verbose=verbose,
    )


def get_rules(
    frequent_item_sets: DataFrame,
    metric: str = "confidence",
    min_threshold: float = 0.02,
) -> DataFrame:
    """Get the rules from the frequent item sets."""
    return association_rules(  # type: ignore[misc, no-any-return]
        frequent_item_sets,
        metric=metric,
        min_threshold=min_threshold,
    )


def prepare_to_upload(rules: DataFrame) -> DataFrame:
    """Prepare the rules to upload to the database."""
    to_upload = rules.copy()
    to_upload["antecedents"] = (
        to_upload["antecedents"]  # type: ignore[misc]
        .apply(lambda x: ", ".join(list(x)))  # type: ignore[misc]
        .astype("unicode")
    )
    to_upload["consequents"] = (
        to_upload["consequents"]  # type: ignore[misc]
        .apply(lambda x: ", ".join(list(x)))  # type: ignore[misc]
        .astype("unicode")
    )
    mapper = {
        "antecedent support": "antecedent_support",
        "consequent support": "consequent_support",
    }
    return to_upload.rename(mapper, axis=1)


def prepare_table(conn: Any, schema: str, table: str) -> None:  # type: ignore[misc]
    """
    Check if the table exists.

    - if it does not exist, a new table is generated.
    - if the table exist, then its content is dropped and replaced with new info.
    """
    cur = conn.cursor()  # type: ignore[misc]
    check_query = f"""SELECT EXISTS (
    SELECT * FROM pg_catalog.pg_class c
    JOIN pg_catalog.pg_namespace n ON n.oid = c.relnamespace
    WHERE n.nspname = '{schema}'
    AND c.relname = '{table}'
    );"""  # noqa: S608
    cur.execute(check_query)  # type: ignore[misc]
    response = cur.fetchall()[0][0]  # type: ignore[misc]
    cur.close()  # type: ignore[misc]
    table_full = schema + "." + table
    if response:  # type: ignore[misc]
        cur = conn.cursor()  # type: ignore[misc]
        drop_query = f"DROP TABLE {table_full}"
        cur.execute(drop_query)  # type: ignore[misc]
        conn.commit()  # type: ignore[misc]
        cur.close()  # type: ignore[misc]
    cur = conn.cursor()  # type: ignore[misc]
    create_query = f"""
    CREATE TABLE {table_full} (
    antecedents VARCHAR(3000),
    consequents VARCHAR(3000),
    antecedent_support FLOAT,
    consequent_support FLOAT,
    support FLOAT,
    confidence FLOAT,
    lift FLOAT,
    leverage FLOAT,
    conviction FLOAT)"""
    cur.execute(create_query)  # type: ignore[misc]
    conn.commit()  # type: ignore[misc]
    cur.close()  # type: ignore[misc]


def execute_values(cursor: Any, dataframe: DataFrame, table: str) -> int:  # type: ignore[misc]
    """Insert the dataframe."""
    # Create a list of tuples from the dataframe values
    tuples = [tuple(x) for x in dataframe.to_numpy()]  # type: ignore[misc]
    # Comma-separated dataframe columns
    cols = ",".join(list(dataframe.columns))
    # SQL query to execute
    query = f"INSERT INTO {table}({cols}) VALUES %s"  # noqa: S608
    cursor.execute(query, tuples)  # type: ignore[misc]
    LOG.info("execute_values() done")
    cursor.close()  # type: ignore[misc]
    return 0


def main() -> None:
    """Execute the main procedure."""
    path = Path(__file__).parent / Path("extract_features.sql")
    with path.open(encoding="UTF-8") as file:
        query = file.read()

    con, cur = ("test_connection", "test_cursor")
    LOG.info("connection ok")
    data = execute_query(cur, query)
    LOG.info("data ok")
    dataset = generate_dataset(data)
    LOG.info("dataset ok")
    sparse_m = generate_sparse_matrix(dataset)
    LOG.info("sparse_m ok")
    item_sets = get_item_sets(sparse_m, 0.00004)
    LOG.info("item_sets ok")
    rules = get_rules(item_sets)
    LOG.info("rules ok")
    to_upload = prepare_to_upload(rules)
    LOG.info("to_upload ok")
    prepare_table(con, "sorts", "association_rules")
    LOG.info("prepare_table ok")
    execute_values(con, to_upload, "sorts.association_rules")
    LOG.info("execute_values ok")
