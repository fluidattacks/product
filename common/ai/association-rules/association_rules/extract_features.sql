WITH vulns AS (
    SELECT
        uuid_str,
        where_str AS raw_filename,
        finding_id_str,
        root_id_str,
        finding_title,
        group_name,
        company,
        zr_status,
        SPLIT_PART(where_str, ' ', 1) AS filename_str
    FROM analytics_production.vulnerabilities
    WHERE
        filename_str IS NOT null
        AND vuln_type_str = 'LINES'
        AND (zr_status != 'CONFIRMED' OR zr_status IS null)
        AND company NOT IN ('imamura', 'okada', 'orgtest')
),

files AS (
    SELECT
        filename_str,
        root_id_str
    FROM analytics_production.surface_lines_all
),

final AS (
    SELECT * FROM vulns INNER JOIN files
        USING (filename_str, root_id_str)
)

SELECT
    uuid_str,
    filename_str,
    raw_filename,
    finding_id_str,
    finding_title,
    group_name,
    zr_status,
    company,
    root_id_str
FROM final
