{ makeScript, outputs, projectPath, ... }@makes_inputs:
let
  root = projectPath "/common/ai/fluid-chatbot";
  pkg = import "${root}/entrypoint.nix" makes_inputs;
  env = pkg.env.runtime;
in {
  jobs."/common/ai/fluid-chatbot/bin" = makeScript {
    searchPaths = {
      bin = [ env ];
      source = [ outputs."/common/utils/sops" ];
    };
    name = "fluid-chatbot-bin";
    entrypoint = ./entrypoint.sh;
  };
}
