{ makes_inputs, nixpkgs, python_version, src, }:
let
  deps = import ./deps { inherit makes_inputs nixpkgs python_version; };
  pkgDeps = {
    runtime_deps = with deps.python_pkgs; [ aiohttp openai requests ];
    build_deps = with deps.python_pkgs; [ flit-core ];
    test_deps = with deps.python_pkgs; [
      mypy
      pytest
      pytest-cov
      types-requests
    ];
  };
  packages = makes_inputs.makePythonPyprojectPackage {
    inherit (deps.lib) buildEnv buildPythonPackage;
    inherit pkgDeps src;
  };
in packages
