{ makeScript, projectPath, ... }@makes_inputs:
let
  root = projectPath "/common/ai/fluid-chatbot";
  bundle = import "${root}/entrypoint.nix" makes_inputs;
  check = bundle.check.types;
in {
  jobs."/common/ai/fluid-chatbot/check/tests" = makeScript {
    searchPaths = { bin = [ check ]; };
    name = "fluid-chatbot-check-tests";
    entrypoint = "";
  };
}
