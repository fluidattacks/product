import asyncio
import itertools
import os
import tempfile
import aiohttp
from fluid_chatbot.zoho import (
    get_all_zoho_article_ids,
    get_zoho_access_token,
    get_zoho_article,
    get_zoho_article_count,
    revoke_zoho_access_token,
)
import asyncio
import os
from openai import (
    AsyncOpenAI,
    OpenAI,
    RateLimitError,
    APIError,
)

ASSISTANT_ID = os.environ["OPENAI_ASSISTANT_ID"]
ASYNC_CLIENT = AsyncOpenAI()
CLIENT = OpenAI()


async def _download_zoho_articles(tempdir: str) -> None:
    print("Downloading articles from Zoho Desk")
    access_token = get_zoho_access_token()
    article_count = get_zoho_article_count(access_token)
    print(f"Total article count: {article_count}")
    from_param_list = list(range(1, int(article_count) + 1, 50))
    semaphore_10 = asyncio.Semaphore(10)
    results = []
    async with aiohttp.ClientSession() as session:
        tasks = [
            get_all_zoho_article_ids(
                semaphore_10,
                session,
                access_token,
                str(from_param)
            ) for from_param in from_param_list
        ]
        results =  await asyncio.gather(*tasks)

    all_article_ids = list(itertools.chain.from_iterable(results))
    print(f"There are {len(all_article_ids)} articles to download")
    semaphore_20 = asyncio.Semaphore(20)
    async with aiohttp.ClientSession() as session:
        tasks = [
            get_zoho_article(semaphore_20, session, article_id, access_token)  # type: ignore
            for article_id in all_article_ids
        ]
        zoho_articles = await asyncio.gather(*tasks)

    os.makedirs(f"{tempdir}/articles", exist_ok=True)
    for article in zoho_articles:
        article_id, article_content = article
        if article_id and article_content:
            with open(f"{tempdir}/articles/{article_id}.html", "w") as file:
                file.write(article_content)

    revoke_zoho_access_token(access_token)

async def _upload_file(semaphore: asyncio.Semaphore, file_path: str, vector_store_id: str) -> None:
    async with semaphore:
        try:
            with open(file_path, "rb") as file:
                await ASYNC_CLIENT.beta.vector_stores.files.upload(
                    vector_store_id=vector_store_id,
                    file=file,
                )
        except RateLimitError as exc:
            print(exc)
            await asyncio.sleep(70)
        except APIError as exc:
            if exc.type == "concurrent_modification":
                print(exc)
                await _upload_file(semaphore, file_path, vector_store_id)
            else:
                print(exc)


async def update_fluid_chatbot() -> None:
    print("Removing old vector stores")
    openai_assistant = CLIENT.beta.assistants.retrieve(assistant_id=ASSISTANT_ID)
    if (
        openai_assistant.tool_resources is not None
        and openai_assistant.tool_resources.file_search is not None
        and openai_assistant.tool_resources.file_search.vector_store_ids is not None
    ):
        assistant_vector_store_ids = openai_assistant.tool_resources.file_search.vector_store_ids
    else:
        assistant_vector_store_ids = []
    CLIENT.beta.assistants.update(
        assistant_id=ASSISTANT_ID,
        tool_resources={
            "file_search": {
                "vector_store_ids": [],
            }
        }
    )
    for vector_store_id in assistant_vector_store_ids:
        CLIENT.beta.vector_stores.delete(vector_store_id=vector_store_id)

    print("Creating new vector store")
    new_vector_store = CLIENT.beta.vector_stores.create(name="Fluid Attacks Knowledge Base")
    CLIENT.beta.assistants.update(
        assistant_id=ASSISTANT_ID,
        tool_resources={
            "file_search": {
                "vector_store_ids": [new_vector_store.id],
            }
        }
    )
    with tempfile.TemporaryDirectory() as tmpdir:
        await _download_zoho_articles(tmpdir)
        print("Uploading articles to OpenAI")
        file_paths = [f"{tmpdir}/articles/{filename}" for filename in os.listdir(f"{tmpdir}/articles")]
        semaphore = asyncio.Semaphore(10)
        tasks = [_upload_file(semaphore, file_path, new_vector_store.id) for file_path in file_paths]
        await asyncio.gather(*tasks)
        print("Articles uploaded successfully")


def main() -> None:
    asyncio.run(update_fluid_chatbot())
