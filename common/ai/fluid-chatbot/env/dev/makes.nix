{ makePythonVscodeSettings, projectPath, ... }@makes_inputs:
let
  root = projectPath "/common/ai/fluid-chatbot";
  bundle = import "${root}/entrypoint.nix" makes_inputs;
in {
  jobs."/common/ai/fluid-chatbot/env/dev" = makePythonVscodeSettings {
    env = bundle.env.dev;
    bins = [ ];
    name = "fluid-chatbot-dev";
  };
}
