{
  imports = [
    ./association-rules/makes.nix
    ./fluid-chatbot/makes.nix
    ./lead-scoring/makes.nix
  ];
  secretsForAwsFromGitlab.prodSorts = {
    roleArn = "arn:aws:iam::205810638802:role/prod_sorts";
    duration = 3600;
  };
}
