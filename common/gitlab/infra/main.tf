terraform {
  required_version = "~> 1.0"
  required_providers {
    gitlab = {
      source  = "gitlabhq/gitlab"
      version = "17.0.0"
    }
  }
  backend "s3" {
    bucket         = "fluidattacks-terraform-states-prod"
    key            = "common-gitlab.tfstate"
    region         = "us-east-1"
    encrypt        = true
    dynamodb_table = "terraform_state_lock"
  }
}

provider "gitlab" {
  token = var.fluidattacksGitlabApiToken
}
