locals {
  data = jsondecode(
    <<-EOF
      {
        "apps": ${var.oktaDataApps},
        "groups": ${var.oktaDataGroups},
        "rules": ${var.oktaDataRules},
        "users": ${var.oktaDataUsers},
        "app_groups": ${var.oktaDataAppGroups},
        "aws_group_roles": ${var.oktaDataAwsGroupRoles}
      }
    EOF
  )
}
