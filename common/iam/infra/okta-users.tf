resource "okta_user" "users" {
  for_each   = { for _, user in local.data.users : user.id => user }
  email      = each.value.email
  first_name = each.value.first_name
  last_name  = each.value.last_name
  login      = each.value.email
  status     = each.value.status
}

resource "okta_user_admin_roles" "users" {
  for_each    = { for _, user in local.data.users : user.id => user if length(user.admin_roles) > 0 }
  user_id     = okta_user.users[each.value.id].id
  admin_roles = each.value.admin_roles
}
