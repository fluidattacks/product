data "aws_caller_identity" "current" {}
data "okta_group" "everyone" {
  name = "Everyone"
}
variable "oktaApiToken" {
  type = string
}
variable "oktaDataApps" {
  type = string
}
variable "oktaDataGroups" {
  type = string
}
variable "oktaDataRules" {
  type = string
}
variable "oktaDataUsers" {
  type = string
}
variable "oktaDataAppGroups" {
  type = string
}
variable "oktaDataAwsGroupRoles" {
  type = string
}
