# https://github.com/fluidattacks/makes
{ outputs, ... }: {
  deployTerraform = {
    modules = {
      commonIam = {
        setup = [
          outputs."/secretsForAwsFromGitlab/prodCommon"
          outputs."/common/iam/parse"
          outputs."/secretsForTerraformFromEnv/commonIam"
        ];
        src = "/common/iam/infra";
        version = "1.0";
      };
    };
  };
  lintTerraform = {
    modules = {
      commonIam = {
        setup = [
          outputs."/secretsForAwsFromGitlab/dev"
          outputs."/common/iam/parse"
          outputs."/secretsForTerraformFromEnv/commonIam"
        ];
        src = "/common/iam/infra";
        version = "1.0";
      };
    };
  };
  secretsForTerraformFromEnv = {
    commonIam = {
      oktaApiToken = "OKTA_API_TOKEN";
      oktaDataApps = "OKTA_DATA_APPS";
      oktaDataGroups = "OKTA_DATA_GROUPS";
      oktaDataRules = "OKTA_DATA_RULES";
      oktaDataUsers = "OKTA_DATA_USERS";
      oktaDataAppGroups = "OKTA_DATA_APP_GROUPS";
      oktaDataAwsGroupRoles = "OKTA_DATA_AWS_GROUP_ROLES";
    };
  };
  testTerraform = {
    modules = {
      commonIam = {
        setup = [
          outputs."/secretsForAwsFromGitlab/dev"
          outputs."/common/iam/parse"
          outputs."/secretsForTerraformFromEnv/commonIam"
        ];
        src = "/common/iam/infra";
        version = "1.0";
      };
    };
  };
}
