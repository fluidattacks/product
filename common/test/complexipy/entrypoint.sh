# shellcheck shell=bash

function main {
  complexipy . --details low --output \
    || return 1
}

main "${@}"
