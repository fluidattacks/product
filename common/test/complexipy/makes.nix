{ inputs, makeScript, makePythonEnvironment, outputs, ... }: {
  jobs."/common/test/complexipy" = makeScript {
    name = "common-test-complexipy";
    searchPaths = {
      source = [
        (makePythonEnvironment {
          pythonVersion = "3.11";
          pythonProjectDir = ./.;
        })
      ];
    };
    entrypoint = ./entrypoint.sh;
  };
}
