# shellcheck shell=bash

function main {
  local HEAD_COMMIT_TITLE
  HEAD_COMMIT_TITLE=$(git log -1 --pretty=format:"%s")

  : && common-test-base \
    && common-test-commitlint \
    && common-test-leaks \
    && common-test-secrets-forbidden \
    && common-test-secrets-commit-msg \
    && common-test-spelling \
    && common-test-pipeline \
    && if [[ ${HEAD_COMMIT_TITLE} =~ ^(.*)\\(rotate) ]]; then
      info "Running rotate commit changes lint since commit type is rotate"
      common-test-secrets-commit-changes
    fi
}

main "${@}"
