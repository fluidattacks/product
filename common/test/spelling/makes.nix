{ inputs, makeScript, projectPath, ... }: {
  jobs."/common/test/spelling" = makeScript {
    entrypoint = ./entrypoint.sh;
    name = "common-test-spelling";
    replace = { __argNPMPath__ = projectPath "/common/test/spelling/npm"; };
    searchPaths.bin = [ inputs.nixpkgs.git inputs.nixpkgs.nodejs_20 ];
  };
}
