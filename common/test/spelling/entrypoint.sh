# shellcheck shell=bash

function main {
  local npm_path="common/test/spelling/npm"

  if running_in_ci_cd_provider; then
    npm_path=__argNPMPath__
  fi

  : && pushd "${npm_path}" \
    && npm ci \
    && export PATH="${PWD}/node_modules/.bin:${PATH}" \
    && popd \
    && info "Testing spelling" \
    && cspell-dict-es-es-link \
    && if git diff-tree --no-commit-id --name-only -r HEAD | cspell --file-list stdin -u --no-progress --no-must-find-files; then
      info 'All checked files are ok!'
    else
      info 'There are some errors, search for "Unknown word".' \
        && info 'If you consider that a word is not an error,' \
        && info 'please add it to "/.project-words.txt".' \
        && return 1
    fi
}

main "${@}"
