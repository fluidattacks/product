{ inputs, makeScript, ... }: {
  jobs."/common/test/secrets/forbidden" = makeScript {
    entrypoint = ./entrypoint.sh;
    name = "common-test-secrets-forbidden";
    searchPaths.bin = [ inputs.nixpkgs.git inputs.nixpkgs.gnugrep ];
  };
}
