# shellcheck shell=bash

function main {
  info "Running commit msg secrets tests"
  common-test-secrets-commit-msg
  common-test-secrets-equal-secrets-different-products
  common-test-secrets-equal-secrets-same-product
  common-test-secrets-forbidden
  info "Commit msg secrets tests passed"
}

main "${@}"
