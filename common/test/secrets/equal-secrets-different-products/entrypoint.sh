# shellcheck shell=bash

function main {
  local commit_msg
  local changes
  local file_validate
  local all_files
  local all_files_unmodified
  local changes_content
  local change_specify
  local file_content

  : && info "Checking that equal secrets are rotated at the same commit" \
    && commit_msg="$(git --no-pager log --pretty=format:%B HEAD^..HEAD)" \
    && if echo "${commit_msg}" | grep -qi -- "- no-equal-secret-different-product-test"; then
      return 0
    fi \
    && changes=$(git diff-tree --no-commit-id --name-only -r HEAD) \
    && file_validate='^(common|integrates|observes|sorts|skims|docs)/(secrets|status)/(dev|development|secrets|production|prod)?\.yaml$' \
    && all_files=$(git ls-tree -r --name-only HEAD | grep -E "$file_validate") \
    && mapfile -t changes_array <<< "$changes" \
    && all_files_unmodified="$all_files" \
    && while IFS= read -r change; do
      all_files_unmodified="${all_files_unmodified//$change/}"
    done <<< "$changes" \
    && all_files_unmodified=$(echo "$all_files_unmodified" | grep -v '^$') \
    && mapfile -t all_files_unmodified_array <<< "$all_files_unmodified" \
    && for file in "${changes_array[@]}"; do
      if [[ $file =~ $file_validate ]]; then
        changes_content=$(git show HEAD^..HEAD -- "$file")
        if echo "${changes_content}" | grep -q '^+#ENC\[A'; then
          return 0
        fi
        change_specify=$(echo "${changes_content}" | grep -oE '^[+\-][a-zA-Z]+([[:alnum:]_]+)' \
          | sed 's/-//' | sed 's/+//')
        for secret_name in $change_specify; do
          echo "secret_name: $secret_name"
          for all_file in "${all_files_unmodified_array[@]}"; do
            file_content="$(cat "$all_file")"
            if echo "${file_content}" | grep -q "$secret_name"; then
              error \
                "Secret $secret_name isn't modified in: ${all_file}" \
                "use - no-equal-secret-different-product-test if needed."
            fi
          done
        done
      fi
    done

  info "Equal secrets are rotated at the same commit"
}

main "${@}"
