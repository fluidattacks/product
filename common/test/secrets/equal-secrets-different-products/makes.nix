{ inputs, makeScript, ... }: {
  jobs."/common/test/secrets/equal-secrets-different-products" = makeScript {
    entrypoint = ./entrypoint.sh;
    name = "common-test-secrets-equal-secrets-different-products";
    searchPaths.bin =
      [ inputs.nixpkgs.git inputs.nixpkgs.gnugrep inputs.nixpkgs.gnused ];
  };
}
