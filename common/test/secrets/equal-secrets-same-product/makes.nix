{ inputs, makeScript, ... }: {
  jobs."/common/test/secrets/equal-secrets-same-product" = makeScript {
    entrypoint = ./entrypoint.sh;
    name = "common-test-secrets-equal-secrets-same-product";
    searchPaths.bin =
      [ inputs.nixpkgs.git inputs.nixpkgs.gnugrep inputs.nixpkgs.gnused ];
  };
}
