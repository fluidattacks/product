{ inputs, makeScript, ... }: {
  jobs."/common/test/secrets/commit-changes" = makeScript {
    entrypoint = ./entrypoint.sh;
    name = "common-test-secrets-commit-changes";
    searchPaths.bin = [ inputs.nixpkgs.git ];
  };
}
