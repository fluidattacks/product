{ nixpkgs, pynix, }:
let

  layer_1 = python_pkgs:
    python_pkgs // {
      arch-lint = let
        result = import ./arch_lint.nix { inherit nixpkgs pynix python_pkgs; };
      in result.pkg;
    };
  layer_2 = python_pkgs:
    python_pkgs // {
      fa-purity = let
        result = import ./fa_purity.nix { inherit nixpkgs pynix python_pkgs; };
      in result.pkg;
    };

  python_pkgs =
    pynix.utils.compose [ layer_2 layer_1 ] pynix.lib.pythonPackages;
in { inherit python_pkgs; }
