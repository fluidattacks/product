{ nixpkgs, pynix, src, }:
let
  deps = import ./deps { inherit nixpkgs pynix; };
  pkgDeps = {
    runtime_deps = with deps.python_pkgs; [
      click
      fa-purity
      gitpython
      typing-extensions
    ];
    build_deps = with deps.python_pkgs; [ flit-core ];
    test_deps = with deps.python_pkgs; [ arch-lint mypy pytest ruff ];
  };
  bundle = pynix.stdBundle { inherit pkgDeps src; };
  vs_settings = pynix.vscodeSettingsShell { pythonEnv = bundle.env.dev; };
  coverage = pynix.coverageReport {
    inherit src;
    parentModulePath = "common/test/base";
    pythonDevEnv = bundle.env.dev;
    module = "commit_linter_base";
    testsFolder = "tests";
  };
in bundle // {
  inherit coverage;
  dev_hook = vs_settings.hook.text;
}
