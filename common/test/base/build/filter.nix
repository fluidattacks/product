path_filter: src:
path_filter {
  root = src;
  include =
    [ "commit_linter_base" "tests" "pyproject.toml" "mypy.ini" "ruff.toml" ];
}
