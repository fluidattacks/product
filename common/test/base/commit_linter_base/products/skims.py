from enum import (
    Enum,
)


class SkimsProduct(Enum):
    SKIMS = "skims"
    SKIMS_APK = "skims-apk"
    SKIMS_CSPM = "skims-cspm"
    SKIMS_DAST = "skims-dast"
    SKIMS_SAST = "skims-sast"
    SKIMS_SCA = "skims-sca"
    SKIMS_AI = "skims-ai"
    SKIMS_SBOM = "skims-sbom"
