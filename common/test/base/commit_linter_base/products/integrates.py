from enum import (
    Enum,
)


class IntegratesProduct(Enum):
    INTEGRATES = "integrates"
    INTEGRATES_DESIGN = "integrates-design"
    INTEGRATES_FRONT = "integrates-front"
    INTEGRATES_JIRA = "integrates-jira"
