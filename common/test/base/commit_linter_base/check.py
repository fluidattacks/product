import logging
import sys
from pathlib import (
    Path,
)

import git
from fa_purity import Unsafe
from git.objects import (
    Commit,
)

from commit_linter_base._typing import (
    FrozenSet,
    NoReturn,
)
from commit_linter_base.products.core import OtherProducts
from commit_linter_base.products.skims import SkimsProduct

from .products import (
    Product,
)
from .products.decode import from_commit
from .roots import (
    product_roots,
)

LOG = logging.getLogger(__name__)


def _get_last_modified_files(commit: Commit) -> FrozenSet[Path]:
    return frozenset(Path(str(f)) for f in commit.stats.files)


def _is_child(path: Path, parent: Path) -> bool:
    try:
        path.relative_to(parent)
    except ValueError:
        return False
    else:
        return True


def _is_child_from_allowed_paths(path: Path, allowed_paths: frozenset[Path]) -> bool:
    return any(_is_child(path, parent_path) for parent_path in allowed_paths)


def allow_modification(product: Product, modified: Path) -> bool:
    if any(
        [
            "/" not in modified.as_posix(),  # if is a root file
            modified.as_posix().startswith("."),  # if is a hidden root file/dir
            modified.as_posix().startswith("common/docs/src/content/"),  # if is a docs content file
        ],
    ):
        return True

    return _is_child_from_allowed_paths(modified, product_roots(product))


def get_possible_specific_pipeline(
    modified_paths: FrozenSet[Path],
) -> SkimsProduct | None:
    for pipeline_product in SkimsProduct:
        if pipeline_product == SkimsProduct.SKIMS:
            continue
        allowed_paths = product_roots(Product.skims(pipeline_product))
        if all(
            _is_child_from_allowed_paths(modified_path, allowed_paths)
            for modified_path in modified_paths
        ):
            return pipeline_product
    return None


def modified_directories() -> None | NoReturn:
    repo = git.Repo(".")
    commit = repo.head.commit
    product = from_commit(commit).alt(Unsafe.raise_exception).to_union()
    product_is_all = product.map(
        lambda t: t is OtherProducts.ALL,
        lambda _: False,
        lambda _: False,
        lambda _: False,
    )
    if product_is_all:
        LOG.info(
            "Pipeline ALL, skipping modified files check",
        )
        return None

    modified_files = _get_last_modified_files(commit)
    LOG.info("Testing modified files")
    for m in modified_files:
        if not allow_modification(product, m):
            sys.tracebacklimit = 0
            msg = f"Invalid modification `{m}` for the selected product `{product}`"
            raise ValueError(msg)
    product_pipeline = product.map(
        lambda _: None,
        lambda _: None,
        lambda _: get_possible_specific_pipeline(modified_files),
        lambda _: None,
    )

    product_is_skims = product.map(
        lambda _: False,
        lambda _: False,
        lambda t: t is SkimsProduct.SKIMS,
        lambda _: False,
    )
    if product_is_skims and product_pipeline:
        sys.tracebacklimit = 0
        msg = f"Use `{product_pipeline.value}` specific product for your changes"
        raise ValueError(msg)

    return None
