from pathlib import (
    Path,
)

from commit_linter_base.products.observes import ObservesProduct

from ._utils import multi_path, single_path


def observes_roots(product: ObservesProduct) -> frozenset[Path]:  # noqa: PLR0911, PLR0912, C901
    # Notice
    # Do NOT use a Dict as a refactor of this function.
    # Dict does not ensure that every item of `ObservesProduct`
    # gets assigned a set of Path
    match product:
        case ObservesProduct.OBSERVES:
            return single_path(Path("observes"))
        case ObservesProduct.SDK_GITLAB:
            return single_path(Path("observes/sdk/gitlab"))
        case ObservesProduct.SDK_TIMEDOCTOR:
            return single_path(Path("observes/sdk/timedoctor"))
        case ObservesProduct.TAP_BUGSNAG:
            return single_path(Path("observes/singer/tap-bugsnag"))
        case ObservesProduct.TAP_CHECKLY:
            return single_path(Path("observes/singer/tap-checkly"))
        case ObservesProduct.TAP_CLOUDWATCH:
            return single_path(Path("observes/singer/tap-cloudwatch"))
        case ObservesProduct.TAP_CSV:
            return single_path(Path("observes/singer/tap-csv"))
        case ObservesProduct.TAP_DELIGHTED:
            return single_path(Path("observes/singer/tap-delighted"))
        case ObservesProduct.TAP_DYNAMO:
            return single_path(Path("observes/singer/tap-dynamo"))
        case ObservesProduct.TAP_FLOW:
            return single_path(Path("observes/singer/tap-flow"))
        case ObservesProduct.TAP_GITLAB:
            return single_path(Path("observes/singer/tap-gitlab"))
        case ObservesProduct.TAP_GITLAB_DORA:
            return single_path(Path("observes/singer/tap-gitlab-dora"))
        case ObservesProduct.TAP_GOOGLE_SHEETS:
            return single_path(Path("observes/singer/tap-google-sheets"))
        case ObservesProduct.TAP_JSON:
            return single_path(Path("observes/singer/tap-json"))
        case ObservesProduct.TAP_MIXPANEL:
            return single_path(Path("observes/singer/tap-mixpanel"))
        case ObservesProduct.TAP_TIMEDOCTOR:
            return single_path(Path("observes/singer/tap-timedoctor"))
        case ObservesProduct.TAP_ZOHO_CRM:
            return single_path(Path("observes/singer/tap-zoho-crm"))
        case ObservesProduct.TARGET_S3:
            return single_path(Path("observes/singer/target-s3"))
        case ObservesProduct.TARGET_WAREHOUSE:
            return single_path(Path("observes/singer/target-warehouse"))
        case ObservesProduct.OBSERVES_INFRA:
            return single_path(Path("observes/infra"))
        case ObservesProduct.OBSERVES_INTEGRATES_DAL:
            return single_path(Path("observes/common/integrates-dal"))
        case ObservesProduct.OBSERVES_DEPLOY_IMAGE:
            return single_path(Path("observes/common/deploy-image"))
        case ObservesProduct.OBSERVES_DB_MIGRATION:
            return single_path(Path("observes/service/db-migration"))
        case ObservesProduct.OBSERVES_DB_SNAPSHOT:
            return single_path(Path("observes/service/db-snapshot"))
        case ObservesProduct.SUCCESS_INDICATORS:
            return single_path(Path("observes/service/success-indicators"))
        case ObservesProduct.LAMBDA_BASE_RUNTIME:
            return single_path(Path("observes/lambda/base-runtime"))
        case ObservesProduct.LAMBDA_INTEGRATES_HISTORIC:
            return single_path(Path("observes/lambda/integrates-historic"))
        case ObservesProduct.ETL_ANNOUNCEKIT:
            return single_path(Path("observes/etl/announcekit"))
        case ObservesProduct.ETL_DYNAMO:
            return single_path(Path("observes/etl/dynamo"))
        case ObservesProduct.ETL_CODE:
            return single_path(Path("observes/etl/code"))
        case ObservesProduct.ETL_GITLAB:
            return multi_path(
                Path("observes/etl/gitlab"),
                Path("observes/sdk/gitlab"),
            )
        case ObservesProduct.ETL_GOOGLE_SHEETS:
            return single_path(Path("observes/etl/google-sheets"))
        case ObservesProduct.ETL_INTEGRATES_USAGE:
            return single_path(Path("observes/etl/integrates-usage"))
        case ObservesProduct.ETL_TIMEDOCTOR:
            return multi_path(
                Path("observes/etl/timedoctor"),
                Path("observes/sdk/timedoctor"),
            )
