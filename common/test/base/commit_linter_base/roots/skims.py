from pathlib import (
    Path,
)

from commit_linter_base.products.skims import SkimsProduct

from ._utils import multi_path, single_path


def skims_roots(product: SkimsProduct) -> frozenset[Path]:  # noqa: PLR0911
    # Notice
    # Do NOT use a Dict as a refactor of this function.
    # Dict does not ensure that every item of `SkimsProduct`
    # gets assigned a set of Path
    match product:
        case SkimsProduct.SKIMS:
            return single_path(Path("skims"))
        case SkimsProduct.SKIMS_APK:
            return multi_path(
                Path("skims/skims/lib_apk"),
                Path("skims/test/lib_apk"),
            )
        case SkimsProduct.SKIMS_CSPM:
            return multi_path(
                Path("skims/skims/lib_cspm"),
                Path("skims/test/lib_cspm"),
            )
        case SkimsProduct.SKIMS_DAST:
            return multi_path(
                Path("skims/skims/lib_dast"),
                Path("skims/test/lib_dast"),
            )
        case SkimsProduct.SKIMS_SAST:
            return multi_path(
                Path("skims/skims/lib_sast"),
                Path("skims/test/lib_sast"),
            )
        case SkimsProduct.SKIMS_SCA:
            return multi_path(
                Path("skims/skims/lib_sca"),
                Path("skims/test/lib_sca"),
                Path("skims/static/advisories"),
            )
        case SkimsProduct.SKIMS_AI:
            return single_path(Path("skims/skims-ai"))
        case SkimsProduct.SKIMS_SBOM:
            return single_path(Path("skims/sbom"))
