from pathlib import (
    Path,
)

from commit_linter_base.products.integrates import IntegratesProduct

from ._utils import multi_path, single_path


def integrates_roots(product: IntegratesProduct) -> frozenset[Path]:
    # Notice
    # Do NOT use a Dict as a refactor of this function.
    # Dict does not ensure that every item of `IntegratesProduct`
    # gets assigned a set of Path
    match product:
        case IntegratesProduct.INTEGRATES:
            return single_path(Path("integrates"))
        case IntegratesProduct.INTEGRATES_DESIGN:
            return multi_path(Path("integrates/design"), Path("integrates/front"))
        case IntegratesProduct.INTEGRATES_FRONT:
            return single_path(Path("integrates/front"))
        case IntegratesProduct.INTEGRATES_JIRA:
            return single_path(Path("integrates/jira"))
