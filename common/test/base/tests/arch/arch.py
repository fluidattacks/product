from arch_lint.dag import (
    DagMap,
)
from arch_lint.graph import (
    FullPathModule,
)
from fa_purity import (
    FrozenList,
)

from commit_linter_base._typing import (
    Dict,
    FrozenSet,
    TypeVar,
)

_dag: Dict[str, FrozenList[FrozenList[str] | str]] = {
    "commit_linter_base": (
        "_cli",
        "check",
        "roots",
        "products",
        "_typing",
    ),
    "commit_linter_base.roots": (
        ("integrates", "observes", "others", "skims"),
        "_utils",
    ),
    "commit_linter_base.products": (
        "decode",
        "core",
        ("integrates", "observes", "skims"),
    ),
}
_T = TypeVar("_T")


def raise_or_return(item: Exception | _T) -> _T:
    if isinstance(item, Exception):
        raise item
    return item


def project_dag() -> DagMap:
    return raise_or_return(DagMap.new(_dag))


def forbidden_allowlist() -> Dict[FullPathModule, FrozenSet[FullPathModule]]:
    _raw: Dict[str, FrozenSet[str]] = {}
    return {
        raise_or_return(FullPathModule.from_raw(k)): frozenset(
            raise_or_return(FullPathModule.from_raw(i)) for i in v
        )
        for k, v in _raw.items()
    }
