from pathlib import (
    Path,
)

from commit_linter_base import (
    check,
)
from commit_linter_base.products import (
    Product,
)
from commit_linter_base.products.integrates import IntegratesProduct
from commit_linter_base.products.skims import SkimsProduct


def test_allow_modification() -> None:
    assert not check.allow_modification(
        Product.integrates(IntegratesProduct.INTEGRATES_FRONT),
        Path("integrates/foo.txt"),
    )
    assert check.allow_modification(
        Product.integrates(IntegratesProduct.INTEGRATES_FRONT),
        Path("integrates/front/foo/foo2/foo.txt"),
    )
    assert check.allow_modification(Product.skims(SkimsProduct.SKIMS), Path(".hidden/foo/text.txt"))
    assert check.allow_modification(Product.skims(SkimsProduct.SKIMS), Path(".foo"))
    assert check.allow_modification(Product.skims(SkimsProduct.SKIMS), Path("root_file.txt"))
    assert not check.allow_modification(
        Product.skims(SkimsProduct.SKIMS),
        Path("non_hidden/foo.txt"),
    )
    assert check.allow_modification(
        Product.skims(SkimsProduct.SKIMS),
        Path("common/docs/src/content/docs/foo.mdx"),
    )
