{ inputs, makeScript, ... }@makes_inputs:
let
  bundle = import ./entrypoint.nix makes_inputs;
  job_prefix = "/common/test/base";
  name = "commit-linter-base";
  main_job = makeScript {
    entrypoint = ./job.sh;
    name = "common-test-base";
    searchPaths.bin = [ bundle.env.runtime ]
      ++ (with inputs.nixpkgs; [ gawk git gnugrep gnupg openssh jq curl ]);
  };
in {
  jobs = {
    "${job_prefix}/job" = main_job;
    "${job_prefix}/env/dev" = bundle.env.dev;
    "${job_prefix}/env/runtime" = bundle.env.runtime;
    "${job_prefix}/shell/dev" = let
      run-lint = makes_inputs.inputs.nixpkgs.writeShellApplication {
        name = "run-lint";
        runtimeInputs = [ ];
        text = ''
          ruff format
          ruff check . --fix
          ruff format
          mypy . && pytest ./tests
        '';
      };
    in makes_inputs.makeTemplate {
      name = "${name}-makes-dev-shell";
      searchPaths = { bin = [ run-lint bundle.env.dev ]; };
      template = bundle.dev_hook;
    };
  };
}
