# shellcheck shell=bash

function main {
  local temp_dir
  local file
  local config
  local template=./common/test/machine_sast/template.yaml

  config=$(mktemp).yaml
  cp "$template" "$config"

  : && temp_dir=$(mktemp -d) \
    && git diff-tree --no-commit-id --name-only -r HEAD | while read -r file; do
      if [[ -f $file ]]; then
        mkdir -p "$temp_dir/$(dirname "$file")" \
        && cp "$file" "$temp_dir/$file"
      fi
    done \
    && sed -i "s|{ TEMP_DIR }|$temp_dir|" "$config" \
    && skims scan "$config"
}

main "${@}"
