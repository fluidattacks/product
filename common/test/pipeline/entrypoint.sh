# shellcheck shell=bash

# Script to check pipeline file size and line counts to prevent failures in Gitlab CI

function get_pipeline_files {
  local touched_files
  local existing_files
  local result

  : && result=$(mktemp) \
    && touched_files=$(git diff-tree --no-commit-id --name-status --diff-filter=AM -r HEAD) \
    && existing_files=$(echo "${touched_files}" | awk '{ print $2 }') \
    && for file in ${existing_files}; do
      if [[ $file =~ pipeline/.*\.yaml$ || $file =~ gitlab-ci.yaml || $file =~ .gitlab-ci.yml ]]; then
        echo "${file}" >> "${result}"
      fi
    done \
    && echo "${result}"
}

function main {
  local size_limit=184320 # Set to 180KiB
  local size_limit_in_kb=180
  local pipeline_files=()

  : && info "Checking if there are any touched pipeline files" \
    && mapfile -t pipeline_files < "$(get_pipeline_files)" \
    && for file in "${pipeline_files[@]}"; do
      info "Checking ${file} size" \
        && file_size=$(wc -c < "$file" | awk '{print $1}') \
        && if [[ ${file_size} -gt ${size_limit} ]]; then
          : && file_size_in_kb=$(echo "${file_size}" | awk '{print int($1/1024)}') \
            && warn "Pipelines file size must not exceed a limit above ${size_limit_in_kb} KiB (aprox) so Gitlab can read them" \
            && warn "We suggest splitting the file in parts to reduce its size" \
            && error "File size of ${file} is around ${file_size_in_kb} KiB which exceeds the limit of ${size_limit_in_kb}" \
            || return 1
        fi
    done \
    && info "All pipeline files seems to have acceptable size"
}

main "${@}"
