{ inputs, makeScript, ... }: {
  jobs."/common/test/pipeline" = makeScript {
    name = "common-test-pipeline";
    entrypoint = ./entrypoint.sh;
    searchPaths.bin =
      [ inputs.nixpkgs.coreutils inputs.nixpkgs.git inputs.nixpkgs.gawk ];
  };
}
