# https://github.com/fluidattacks/makes
{ inputs, makeSearchPaths, outputs, ... }:
let searchPaths = makeSearchPaths { bin = [ inputs.nixpkgs.git ]; };
in {
  deployTerraform = {
    modules = {
      commonVpn = {
        setup = [
          searchPaths
          outputs."/secretsForAwsFromGitlab/prodCommon"
          outputs."/secretsForEnvFromSops/commonCloudflare"
          outputs."/secretsForEnvFromSops/commonCloudflareProd"
          outputs."/secretsForEnvFromSops/commonVpnData"
          outputs."/secretsForTerraformFromEnv/commonVpn"
        ];
        src = "/common/vpn/infra";
        version = "1.0";
      };
    };
  };
  lintTerraform = {
    modules = {
      commonVpn = {
        setup = [
          searchPaths
          outputs."/secretsForAwsFromGitlab/dev"
          outputs."/secretsForEnvFromSops/commonCloudflare"
          outputs."/secretsForEnvFromSops/commonCloudflareDev"
          outputs."/secretsForEnvFromSops/commonVpnData"
          outputs."/secretsForTerraformFromEnv/commonVpn"
        ];
        src = "/common/vpn/infra";
        version = "1.0";
      };
    };
  };
  secretsForEnvFromSops = {
    commonVpnData = {
      vars = [ "EGRESS_IPS_DATA_RAW" "VPN_DATA_RAW" "ZTNA_DATA_RAW" ];
      manifest = "/common/vpn/data.yaml";
    };
  };
  secretsForTerraformFromEnv = {
    commonVpn = {
      cloudflare_api_key = "CLOUDFLARE_API_KEY";
      cloudflare_account_id = "CLOUDFLARE_ACCOUNT_ID";
      cloudflare_email = "CLOUDFLARE_EMAIL";
      cloudflare_okta_client_id = "CLOUDFLARE_OKTA_CLIENT_ID";
      cloudflare_okta_client_secret = "CLOUDFLARE_OKTA_CLIENT_SECRET";
      cloudflare_okta_scim_secret = "CLOUDFLARE_OKTA_SCIM_SECRET";
      egress_ips_data_raw = "EGRESS_IPS_DATA_RAW";
      vpn_data_raw = "VPN_DATA_RAW";
      ztna_data_raw = "ZTNA_DATA_RAW";
    };
  };
  testTerraform = {
    modules = {
      commonVpn = {
        setup = [
          searchPaths
          outputs."/secretsForAwsFromGitlab/dev"
          outputs."/secretsForEnvFromSops/commonCloudflare"
          outputs."/secretsForEnvFromSops/commonCloudflareDev"
          outputs."/secretsForEnvFromSops/commonVpnData"
          outputs."/secretsForTerraformFromEnv/commonVpn"
        ];
        src = "/common/vpn/infra";
        version = "1.0";
      };
    };
  };
}
