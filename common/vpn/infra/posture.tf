resource "cloudflare_device_posture_rule" "os_version" {
  account_id  = var.cloudflare_account_id
  name        = "MacOS latest version?"
  type        = "os_version"
  description = "Device posture rule for corporate devices"
  schedule    = "24h"
  expiration  = "24h"

  match {
    platform = "mac"
  }

  input {
    version  = "13.4.1"
    operator = ">="
  }
}

resource "cloudflare_device_posture_rule" "firewall_enabled" {
  account_id  = var.cloudflare_account_id
  name        = "Firewall Enabled?"
  type        = "firewall"
  description = "Device posture rule for corporate devices"
  schedule    = "24h"
  expiration  = "24h"

  match {
    platform = "mac"
  }

  input {
    enabled = true
  }
}

resource "cloudflare_device_posture_rule" "disk_encryption" {
  account_id  = var.cloudflare_account_id
  name        = "MacOS Disk Encryption"
  type        = "disk_encryption"
  description = "Device posture rule for corporate devices"
  schedule    = "24h"
  expiration  = "24h"

  match {
    platform = "mac"
  }

  input {
    require_all = true
  }
}
