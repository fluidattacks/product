resource "cloudflare_access_organization" "default" {
  account_id                         = var.cloudflare_account_id
  name                               = "fluidattacks.cloudflareaccess.com"
  auth_domain                        = "fluidattacks.cloudflareaccess.com"
  user_seat_expiration_inactive_time = "1460h"
  is_ui_read_only                    = true
  auto_redirect_to_identity          = false

  login_design {
    background_color = "#ffffff"
    text_color       = "#000000"
    logo_path        = "https://res.cloudinary.com/fluid-attacks/image/upload/v1669232201/airs/favicon-2022.png"
    header_text      = ""
    footer_text      = "Copyright © 2023 Fluid Attacks. We hack your software. All rights reserved."
  }
}

resource "cloudflare_teams_account" "main" {
  account_id           = var.cloudflare_account_id
  tls_decrypt_enabled  = false
  activity_log_enabled = true

  block_page {
    enabled          = true
    footer_text      = "This website is blocked, contact an admin for any requests."
    header_text      = "This website is blocked"
    mailto_address   = "help@fluidattacks.com"
    mailto_subject   = "Website is blocked"
    logo_path        = "https://res.cloudinary.com/fluid-attacks/image/upload/v1669232201/airs/favicon-2022.png"
    background_color = "#000000"
    name             = "Fluid Attacks"
  }

  fips {
    tls = true
  }

  antivirus {
    enabled_download_phase = true
    enabled_upload_phase   = true
    fail_closed            = false
  }

  logging {
    redact_pii = false

    settings_by_rule_type {
      dns {
        log_all    = true
        log_blocks = true
      }

      http {
        log_all    = true
        log_blocks = true
      }

      l4 {
        log_all    = true
        log_blocks = true
      }
    }
  }

  proxy {
    root_ca = true
    tcp     = true
    udp     = true
  }
}
