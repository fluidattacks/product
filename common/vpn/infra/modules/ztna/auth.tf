locals {
  rule_precedence        = 9
  rule_precedence_factor = 1000
  rule_name_hash         = tonumber(format("%d", regex("[0-9]+", sha256(var.name))))
  calculated_precedence  = local.rule_precedence * local.rule_precedence_factor + (local.rule_name_hash % local.rule_precedence_factor)
}

resource "cloudflare_teams_rule" "allow" {
  account_id  = var.cloudflare.account_id
  name        = "${var.name}.ztna.allow"
  description = "Allow authorization rule for ${var.name}"
  precedence  = local.calculated_precedence
  enabled     = true
  action      = "allow"
  filters     = ["l4"]
  traffic     = "net.vnet_id == \"${cloudflare_tunnel_virtual_network.default.id}\""
  identity = format(
    "identity.email == \"%s\" or any(identity.groups.name[*] in {\"%s\"}) or any(identity.groups.name[*] in {\"%s\"}) or any(identity.groups.name[*] in {\"%s\"}) or any(identity.groups.name[*] in {\"%s\"})",
    var.cloudflare.default_email,
    var.okta.groups.fluid_security_tester,
    var.okta.groups.fluid_security_developer,
    var.okta.groups.fluid_engagement_manager,
    var.okta.groups.fluid_team_it,
  )
  rule_settings {
    check_session {
      enforce  = true
      duration = "168h0m0s"
    }
  }
}
resource "cloudflare_teams_rule" "block" {
  account_id  = var.cloudflare.account_id
  name        = "${var.name}.ztna.block"
  description = "Block authorization rule for ${var.name}"
  precedence  = 10000
  enabled     = true
  action      = "block"
  filters     = ["l4"]
  traffic     = "net.vnet_id == \"${cloudflare_tunnel_virtual_network.default.id}\""
}
