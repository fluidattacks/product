resource "cloudflare_tunnel_route" "default" {
  for_each = toset(var.routes)

  account_id         = var.cloudflare.account_id
  tunnel_id          = cloudflare_tunnel.default.id
  network            = each.key
  virtual_network_id = cloudflare_tunnel_virtual_network.default.id
}
