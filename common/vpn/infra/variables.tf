variable "cloudflare_email" {
  type = string
}
variable "cloudflare_api_key" {
  type = string
}
variable "cloudflare_account_id" {
  type = string
}
variable "cloudflare_okta_client_id" {
  type = string
}
variable "cloudflare_okta_client_secret" {
  type = string
}
variable "cloudflare_okta_scim_secret" {
  type = string
}
variable "egress_ips_data_raw" {
  type = string
}
variable "vpn_data_raw" {
  type = string
}
variable "ztna_data_raw" {
  type = string
}

data "cloudflare_zones" "fluidattacks_com" {
  filter {
    name = "fluidattacks.com"
  }
}
data "aws_vpc" "main" {
  filter {
    name   = "tag:Name"
    values = ["fluid-vpc"]
  }
}
data "aws_subnet" "batch_clone" {
  vpc_id = data.aws_vpc.main.id
  filter {
    name   = "tag:Name"
    values = ["batch_clone"]
  }
}
data "aws_subnet" "common" {
  vpc_id = data.aws_vpc.main.id
  filter {
    name   = "tag:Name"
    values = ["common"]
  }
}

locals {
  egress_ips_data = {
    for client in jsondecode(var.egress_ips_data_raw) : client.id => client
  }
  vpn_data = {
    for client in jsondecode(var.vpn_data_raw) : client.id => client
  }
  ztna_data = {
    for client in jsondecode(var.ztna_data_raw) : client.id => client
  }
  ztna_data_dns = flatten([for _, tunnel in local.ztna_data : tunnel.dns])
}

locals {
  aws = {
    cidr = "192.168.0.0/16"
    private_ips = {
      erp = "192.168.10.37"
    }
  }
  cloudflare_default_email = "non_identity@fluidattacks.cloudflareaccess.com"
  okta = {
    domains = {
      static_assets = "ok7static.oktacdn.com"
      base          = "fluidattacks.okta.com"
    }
    groups = {
      fluid_analytics          = "Fluid Attacks - Analytics Default"
      fluid_design             = "Fluid Attacks - Design"
      fluid_engagement_manager = "Fluid Attacks - Engagement Manager"
      fluid_finance_def        = "Fluid Attacks - Finance Default"
      fluid_marketing          = "Fluid Attacks - Marketing"
      fluid_people             = "Fluid Attacks - People"
      fluid_product            = "Fluid Attacks - Product"
      fluid_prospecting        = "Fluid Attacks - Prospecting"
      fluid_security_developer = "Fluid Attacks - Security Developer"
      fluid_security_tester    = "Fluid Attacks - Security Tester"
      fluid_team_it            = "Fluid Attacks - Team IT"
      fluid_warp               = "Fluid Attacks - Warp"
    }
  }
}
