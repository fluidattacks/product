resource "cloudflare_tunnel_virtual_network" "default" {
  account_id         = var.cloudflare_account_id
  name               = "default"
  comment            = "Default Virtual Network"
  is_default_network = true
}
resource "cloudflare_teams_rule" "disable_http_inspection" {
  account_id  = var.cloudflare_account_id
  name        = "disable_http_inspection"
  description = "Do Not Inspect HTTP traffic"
  precedence  = 1
  enabled     = true
  action      = "off"
  filters     = ["http"]
  traffic     = "not(any(http.conn.dst_ip[*] == 0.0.0.0))"
}

# Clients
resource "cloudflare_teams_rule" "auth" {
  account_id  = var.cloudflare_account_id
  name        = "auth"
  description = "Allow authentication rule for ZTNA tunnels"
  precedence  = 1
  enabled     = true
  action      = "allow"
  filters     = ["l4"]
  traffic     = "any(net.sni.domains[*] == \"${local.okta.domains.static_assets}\") or any(net.sni.domains[*] == \"${local.okta.domains.base}\")"
}

module "tunnels" {
  for_each = local.ztna_data
  source   = "./modules/ztna"

  cloudflare = {
    account_id    = var.cloudflare_account_id
    default_email = local.cloudflare_default_email
  }
  logs = {
    logging = each.value.logging
    bucket  = aws_s3_bucket.logpush.bucket
    region  = aws_s3_bucket.logpush.region
  }
  name = each.key
  okta = local.okta
  routes = distinct(concat(
    each.value.routes,
    # DNS servers for proper name resolution
    [
      for dns in
      flatten([for _, dns in each.value.dns : dns.servers])
      : "${dns}/32"
    ],
  ))
}

# AWS VPC
resource "random_password" "aws" {
  length = 32
}
resource "cloudflare_tunnel" "aws" {
  account_id = var.cloudflare_account_id
  name       = "aws"
  secret     = base64encode(random_password.aws.result)
  config_src = "cloudflare"
}
resource "cloudflare_tunnel_config" "aws" {
  account_id = var.cloudflare_account_id
  tunnel_id  = cloudflare_tunnel.aws.id

  config {
    warp_routing {
      enabled = true
    }
    ingress_rule {
      service = "rdp://${local.aws.private_ips.erp}:3389"
    }
  }
}
resource "cloudflare_tunnel_virtual_network" "aws" {
  account_id         = var.cloudflare_account_id
  name               = "aws"
  is_default_network = false
}
resource "cloudflare_tunnel_route" "aws" {
  account_id         = var.cloudflare_account_id
  tunnel_id          = cloudflare_tunnel.aws.id
  network            = local.aws.cidr
  comment            = "Tunnel to Fluid Attacks AWS VPC"
  virtual_network_id = cloudflare_tunnel_virtual_network.aws.id
}
