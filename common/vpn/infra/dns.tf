resource "aws_security_group" "sg_vpn_dns" {
  name   = "vpn-dns"
  vpc_id = data.aws_vpc.main.id

  ingress {
    // NOFLUID We need to allow port 53 for DNS resolution.
    from_port = 53
    to_port   = 53
    protocol  = "tcp"
    // NOFLUID We need to allow a wide range of IP addresses for DNS resolution.
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    // NOFLUID We need to allow port 53 for DNS resolution.
    from_port = 53
    to_port   = 53
    protocol  = "udp"
    // NOFLUID We need to allow a wide range of IP addresses for DNS resolution.
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port = 0
    to_port   = 0
    // NOFLUID we need to allow all outbound traffic for protocol flexibility.
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name                = "sg_vpn_dns"
    "fluidattacks:line" = "cost"
    "fluidattacks:comp" = "common"
    "NOFLUID"           = "f024_allow_cloning_ports"
  }
}

resource "aws_route53_resolver_endpoint" "main" {
  name      = "vpn"
  direction = "INBOUND"

  security_group_ids = [
    aws_security_group.sg_vpn_dns.id
  ]

  ip_address {
    subnet_id = data.aws_subnet.batch_clone.id
  }

  ip_address {
    subnet_id = data.aws_subnet.common.id
  }

  tags = {
    "Name"              = "vpn"
    "fluidattacks:line" = "cost"
    "fluidattacks:comp" = "common"
  }
}
