{
  association_rules = {
    attempts = 1;
    awsRole = "prod_sorts";
    command = [
      "m"
      "gitlab:fluidattacks/universe@trunk"
      "/common/ai/association-rules/bin"
    ];
    enable = false;
    environment = [ "CACHIX_AUTH_TOKEN" ];
    meta = {
      description = ''
        Update the association rules based in the current available data.
      '';
      requiredBy = [''
        association_execute,
        since these rules are used to compute
        the probability of each vulnerability type
        being present in a determined file.
      ''];
    };
    parallel = 1;
    scheduleExpression = "cron(0 0 1 1-12/3 ? *)";
    size = "sorts_large";
    tags = {
      "Name" = "association_rules";
      "fluidattacks:line" = "cost";
      "fluidattacks:comp" = "sorts";
    };
    timeout = 86400;
  };
  sorts_execute = {
    attempts = 3;
    awsRole = "prod_sorts";
    command = [
      "m"
      "gitlab:fluidattacks/universe@trunk"
      "/sorts/jobs/sorts-execute"
      "5"
    ];
    enable = true;
    environment = [ "CACHIX_AUTH_TOKEN" ];
    meta = {
      description = ''
        Predict the priority for all present files in the ARM scope.
      '';
      requiredBy = [''
        integrates_update_group_toe_priorities
        since these predictions are its input.
      ''];
    };
    parallel = 5;
    scheduleExpression = "cron(0 23 ? * 6 *)";
    size = "sorts";
    tags = {
      "Name" = "sorts_execute";
      "fluidattacks:line" = "cost";
      "fluidattacks:comp" = "sorts";
    };
    timeout = 129600;
  };
  sorts_extract_features = {
    attempts = 3;
    awsRole = "prod_sorts";
    command = [
      "m"
      "gitlab:fluidattacks/universe@trunk"
      "/sorts/jobs/extract-features"
      "20"
    ];
    enable = true;
    environment = [ "CACHIX_AUTH_TOKEN" ];
    meta = {
      description = ''
        Extract the features dataset from all integrates groups
      '';
      requiredBy = [''
        sorts_merge_features since it uses the results from
        this job and merges them into one dataset
      ''];
    };
    parallel = 20;
    scheduleExpression = "cron(0 23 ? * 3 *)";
    size = "sorts";
    tags = {
      "Name" = "sorts_extract_features";
      "fluidattacks:line" = "cost";
      "fluidattacks:comp" = "sorts";
    };
    timeout = 129600;
  };
  lead_scoring = {
    attempts = 3;
    awsRole = "prod_sorts";
    command = [
      "m"
      "gitlab:fluidattacks/universe@trunk"
      "/common/ai/lead-scoring/bin"
    ];
    enable = true;
    environment = [ "CACHIX_AUTH_TOKEN" ];
    meta = {
      description = ''
        Update inference lead scoring (MQL and SQL) based in the current available data.
      '';
      requiredBy = [''
        Marketing and Inside Sales,
        for leads qualification process
      ''];
    };
    parallel = 1;
    scheduleExpression = "cron(0 2 ? * 2-6 *)"; # workdays at 9pm UTC-5
    size = "sorts";
    tags = {
      "Name" = "lead_scoring";
      "fluidattacks:line" = "cost";
      "fluidattacks:comp" = "sorts";
    };
    timeout = 86400;
  };
  sorts_merge_features = {
    attempts = 3;
    awsRole = "prod_sorts";
    command =
      [ "m" "gitlab:fluidattacks/universe@trunk" "/sorts/jobs/merge-features" ];
    enable = true;
    environment = [ "CACHIX_AUTH_TOKEN" ];
    meta = {
      description = ''
        Merge all the feature datasets of all integrates groups
      '';
      requiredBy = [''
        Sorts since this dataset is used for training the model
      ''];
    };
    parallel = 1;
    scheduleExpression = "cron(0 23 ? * 4 *)";
    size = "sorts";
    tags = {
      "Name" = "sorts_merge_features";
      "fluidattacks:line" = "cost";
      "fluidattacks:comp" = "sorts";
    };
    timeout = 129600;
  };
  sorts_training = {
    attempts = 1;
    awsRole = "prod_sorts";
    command = [
      "m"
      "gitlab:fluidattacks/universe@trunk"
      "/sorts/training/bin"
      "train"
    ];
    enable = true;
    environment = [ "CACHIX_AUTH_TOKEN" ];
    meta = {
      description = "Job for training the model used by Sorts.";
      requiredBy = [''
        sorts_execute,
        since this model is used to calculate
        the priority for each file.
      ''];
    };
    parallel = 1;
    scheduleExpression = "cron(0 5 ? * 6 *)";
    size = "sorts";
    tags = {
      "Name" = "sorts_training";
      "fluidattacks:line" = "cost";
      "fluidattacks:comp" = "sorts";
    };
    timeout = 129600;
  };
  sorts_tune = {
    attempts = 1;
    awsRole = "prod_sorts";
    command =
      [ "m" "gitlab:fluidattacks/universe@trunk" "/sorts/training/bin" "tune" ];
    enable = true;
    environment = [ "CACHIX_AUTH_TOKEN" ];
    meta = {
      description = "Job for tuning the model used by Sorts.";
      requiredBy = [''
        sorts_execute,
        since this model is used to calculate
        the priority for each file.
      ''];
    };
    parallel = 1;
    scheduleExpression = "cron(0 5 ? * 7 *)";
    size = "sorts";
    tags = {
      "Name" = "sorts_tune";
      "fluidattacks:line" = "cost";
      "fluidattacks:comp" = "sorts";
    };
    timeout = 129600;
  };
}
