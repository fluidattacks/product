{
  integrates_abandoned_trial_notification = {
    attempts = 1;
    awsRole = "prod_integrates";
    command = [
      "m"
      "gitlab:fluidattacks/universe@trunk"
      "/integrates/utils/scheduler"
      "prod"
      "integrates.schedulers.abandoned_trial_notification.main"
    ];
    enable = true;
    environment = [ "CACHIX_AUTH_TOKEN" ];
    meta = {
      description = ''
        Send email notifications
        to resume or start over trial registration.
      '';
      requiredBy = [''
        ARM in order to prevent losing users
        that start onboarding services for a free trial.
      ''];
    };
    parallel = 1;
    scheduleExpression = "cron(0 */1 * * ? *)";
    size = "integrates";
    tags = {
      "Name" = "abandoned_trial_notification";
      "fluidattacks:line" = "cost";
      "fluidattacks:comp" = "integrates";
    };
    timeout = 3600;
  };
  integrates_nofluid_counter = {
    attempts = 1;
    awsRole = "prod_integrates";
    command = [
      "m"
      "gitlab:fluidattacks/universe@trunk"
      "/integrates/utils/scheduler"
      "prod"
      "integrates.schedulers.nofluid_comments.main"
    ];
    enable = true;
    environment = [ "CACHIX_AUTH_TOKEN" ];
    meta = {
      description = ''
        Count the occurrences of
        NOFLUID comment in active groups.
        Save the results at the root level (individual)
        and at the group level (overall).
      '';
      requiredBy = [''
        Platform in order to generate proper
        analytics.
      ''];
    };
    parallel = 1;
    scheduleExpression = "cron(30 2 ? * 2-6 *)";
    size = "integrates_large";
    tags = {
      "Name" = "integrates_nofluid_counter";
      "fluidattacks:line" = "cost";
      "fluidattacks:comp" = "integrates";
    };
    timeout = 43200;
  };
  integrates_charts_documents = {
    attempts = 2;
    awsRole = "prod_integrates";
    command = [
      "m"
      "gitlab:fluidattacks/universe@trunk"
      "/integrates/charts/documents"
      "prod"
      "15"
      "batch"
    ];
    enable = true;
    environment = [ "CACHIX_AUTH_TOKEN" ];
    meta = {
      description = ''
        Generate charts documents by executing
        the analytics generators and upload them to S3.
      '';
      requiredBy = [ "Analytics view in the ARM." ];
    };
    parallel = 15;
    scheduleExpression = "cron(0 1 ? * MON-FRI *)";
    size = "integrates";
    tags = {
      "Name" = "integrates_charts_documents";
      "fluidattacks:line" = "cost";
      "fluidattacks:comp" = "integrates";
    };
    timeout = 43200;
  };
  integrates_charts_snapshots = {
    attempts = 2;
    awsRole = "prod_integrates";
    command = [
      "m"
      "gitlab:fluidattacks/universe@trunk"
      "/integrates/charts/snapshots"
      "prod"
    ];
    enable = true;
    environment = [ "CACHIX_AUTH_TOKEN" ];
    meta = {
      description = ''
        Generate charts snapshots by executing
        the analytics generators using Selenium and upload them to S3.
      '';
      requiredBy = [ "Analytics view in the ARM." ];
    };
    parallel = 1;
    scheduleExpression = "cron(0 2 ? * MON-FRI *)";
    size = "integrates_large";
    tags = {
      "Name" = "integrates_charts_snapshots";
      "fluidattacks:line" = "cost";
      "fluidattacks:comp" = "integrates";
    };
    timeout = 86400;
  };
  integrates_send_notification_tunnels_down = {
    attempts = 1;
    awsRole = "prod_integrates";
    command = [
      "m"
      "gitlab:fluidattacks/universe@trunk"
      "/integrates/utils/scheduler"
      "prod"
      "integrates.schedulers.send_notification_tunnels_down.main"
    ];
    enable = true;
    environment = [ "CACHIX_AUTH_TOKEN" ];
    meta = {
      description = ''
        Send emails to users in organizations with ZTNA whose status is down.
      '';
      requiredBy = [''
        ARM sends a weekly email to users in organizations with
        roots using ZTNA with an down status.
      ''];
    };
    parallel = 1;
    scheduleExpression = "cron(10 15 ? * WED *)";
    size = "integrates";
    tags = {
      "Name" = "integrates_send_notification_tunnels_down";
      "fluidattacks:line" = "cost";
      "fluidattacks:comp" = "integrates";
    };
    timeout = 3600;
  };
  integrates_update_worker = {
    attempts = 3;
    awsRole = "prod_common";
    command = [
      "m"
      "gitlab:fluidattacks/universe@trunk"
      "/deployTerraform/workerInfra"
    ];
    enable = true;
    environment = [ "CACHIX_AUTH_TOKEN" ];
    meta = {
      description = ''
        Updates the deployment of the task worker, does the deployment
        with the latest commit of universe
      '';
      requiredBy = [ "Integrates tasks worker" ];
    };
    parallel = 1;
    scheduleExpression = "cron(45 10-23/2 * * ? *)";
    size = "integrates";
    tags = {
      "Name" = "integrates_update_worker";
      "fluidattacks:line" = "cost";
      "fluidattacks:comp" = "integrates";
    };
    timeout = 86400;
  };
  integrates_clone_groups_roots = {
    attempts = 3;
    awsRole = "prod_integrates";
    command = [
      "m"
      "gitlab:fluidattacks/universe@trunk"
      "/integrates/utils/scheduler"
      "prod"
      "integrates.schedulers.clone_groups_roots.main"
    ];
    enable = true;
    environment = [ "CACHIX_AUTH_TOKEN" ];
    meta = {
      description = ''
        Clone all the roots in active groups
        that have credentials associated and upload them to S3
      '';
      requiredBy =
        [ "Security testers for analyzing up-to-date repositories." ];
    };
    parallel = 1;
    scheduleExpression = "cron(0 11-23/2 ? * MON-FRI *)";
    size = "integrates";
    tags = {
      "Name" = "integrates_clone_groups_roots";
      "fluidattacks:line" = "cost";
      "fluidattacks:comp" = "integrates";
    };
    timeout = 2 * 60 * 60;
  };
  # This scheduler prevents the S3 lifecycle from deleting all S3 mirrors.
  # Be careful when modifying the frequency between executions
  integrates_clone_roots_that_have_not_cloned = {
    attempts = 3;
    awsRole = "prod_integrates";
    command = [
      "m"
      "gitlab:fluidattacks/universe@trunk"
      "/integrates/utils/scheduler"
      "prod"
      "integrates.schedulers.clone_roots_that_have_not_cloned.main"
    ];
    enable = true;
    environment = [ "CACHIX_AUTH_TOKEN" ];
    meta = {
      description = ''
        Clone all the roots in active groups that have credentials associated
        that not has been cloned in the last three days and upload them to S3
      '';
      requiredBy =
        [ "Security testers for analyzing up-to-date repositories." ];
    };
    parallel = 1;
    scheduleExpression = "cron(0 12 ? * MON,FRI *)";
    size = "integrates";
    tags = {
      "Name" = "integrates_clone_roots_that_have_not_cloned";
      "fluidattacks:line" = "cost";
      "fluidattacks:comp" = "integrates";
    };
    timeout = 4 * 60 * 60;
  };
  integrates_consulting_digest_notification = {
    attempts = 3;
    awsRole = "prod_integrates";
    command = [
      "m"
      "gitlab:fluidattacks/universe@trunk"
      "/integrates/utils/scheduler"
      "prod"
      "integrates.schedulers.consulting_digest_notification.main"
    ];
    enable = true;
    environment = [ "CACHIX_AUTH_TOKEN" ];
    meta = {
      description = ''
        Send emails to users with comments digest.
      '';
      requiredBy = [''
        ARM sending only one mail with all comments summary
        and avoid email flooding to users.
      ''];
    };
    parallel = 1;
    scheduleExpression = "cron(0 11,19 ? * 2-6 *)";
    size = "integrates";
    tags = {
      "Name" = "integrates_consulting_digest_notification";
      "fluidattacks:line" = "cost";
      "fluidattacks:comp" = "integrates";
    };
    timeout = 86400;
  };
  integrates_devsecops_expiration_notification = {
    attempts = 3;
    awsRole = "prod_integrates";
    command = [
      "m"
      "gitlab:fluidattacks/universe@trunk"
      "/integrates/utils/scheduler"
      "prod"
      "integrates.schedulers.devsecops_expiration_notification.main"
    ];
    enable = true;
    environment = [ "CACHIX_AUTH_TOKEN" ];
    meta = {
      description = ''
        Send emails to users manager with devsecops tokens with 7 days to expire.
      '';
      requiredBy = [''
        ARM sending only one mail with all affected groups
        and avoid email flooding to users.
      ''];
    };
    parallel = 1;
    scheduleExpression = "cron(0 14 * * ? *)";
    size = "integrates";
    tags = {
      "Name" = "integrates_devsecops_expiration_notification";
      "fluidattacks:line" = "cost";
      "fluidattacks:comp" = "integrates";
    };
    timeout = 86400;
  };
  integrates_delete_obsolete_groups = {
    attempts = 3;
    awsRole = "prod_integrates";
    command = [
      "m"
      "gitlab:fluidattacks/universe@trunk"
      "/integrates/utils/scheduler"
      "prod"
      "integrates.schedulers.delete_obsolete_groups.main"
    ];
    enable = true;
    environment = [ "CACHIX_AUTH_TOKEN" ];
    meta = {
      description = ''
        Remove groups without users,
        findings nor Fluid Attacks services enabled.
      '';
      requiredBy = [ "ARM cleaning unwanted or obsolete data." ];
    };
    parallel = 1;
    scheduleExpression = "cron(0 2 ? * * *)";
    size = "integrates";
    tags = {
      "Name" = "integrates_delete_obsolete_groups";
      "fluidattacks:line" = "cost";
      "fluidattacks:comp" = "integrates";
    };
    timeout = 86400;
  };
  integrates_delete_obsolete_orgs = {
    attempts = 3;
    awsRole = "prod_integrates";
    command = [
      "m"
      "gitlab:fluidattacks/universe@trunk"
      "/integrates/utils/scheduler"
      "prod"
      "integrates.schedulers.delete_obsolete_orgs.main"
    ];
    enable = true;
    environment = [ "CACHIX_AUTH_TOKEN" ];
    meta = {
      description = ''
        Remove organizations without groups after 60 days.
      '';
      requiredBy = [ "ARM cleaning unwanted or obsolete data." ];
    };
    parallel = 1;
    scheduleExpression = "cron(0 9 ? * * *)";
    size = "integrates";
    tags = {
      "Name" = "integrates_delete_obsolete_orgs";
      "fluidattacks:line" = "cost";
      "fluidattacks:comp" = "integrates";
    };
    timeout = 86400;
  };
  integrates_event_digest_notification = {
    attempts = 3;
    awsRole = "prod_integrates";
    command = [
      "m"
      "gitlab:fluidattacks/universe@trunk"
      "/integrates/utils/scheduler"
      "prod"
      "integrates.schedulers.event_digest_notification.main"
    ];
    enable = true;
    environment = [ "CACHIX_AUTH_TOKEN" ];
    meta = {
      description = ''
        Send emails to users with events digest.
      '';
      requiredBy = [''
        ARM sending a consolidated events summary
        and avoid email flooding to users.
      ''];
    };
    parallel = 1;
    scheduleExpression = "cron(0 11 ? * 2-6 *)";
    size = "integrates";
    tags = {
      "Name" = "integrates_event_digest_notification";
      "fluidattacks:line" = "cost";
      "fluidattacks:comp" = "integrates";
    };
    timeout = 86400;
  };
  integrates_event_report = {
    attempts = 3;
    awsRole = "prod_integrates";
    command = [
      "m"
      "gitlab:fluidattacks/universe@trunk"
      "/integrates/utils/scheduler"
      "prod"
      "integrates.schedulers.event_report.main"
    ];
    enable = true;
    environment = [ "CACHIX_AUTH_TOKEN" ];
    meta = {
      description = ''
        Send emails to users with events summary.
      '';
      requiredBy = [''
        ARM sending a consolidated events summary
        and avoid email flooding to users.
      ''];
    };
    parallel = 1;
    scheduleExpression = "cron(0 14 ? * * *)";
    size = "integrates";
    tags = {
      "Name" = "integrates_event_report";
      "fluidattacks:line" = "cost";
      "fluidattacks:comp" = "integrates";
    };
    timeout = 86400;
  };
  integrates_expire_free_trial = {
    attempts = 1;
    awsRole = "prod_integrates";
    command = [
      "m"
      "gitlab:fluidattacks/universe@trunk"
      "/integrates/utils/scheduler"
      "prod"
      "integrates.schedulers.expire_free_trial.main"
    ];
    enable = true;
    environment = [ "CACHIX_AUTH_TOKEN" ];
    meta = {
      description = ''
        Validate expiration date for trial groups updating their state
        and clean data for already expired groups.
      '';
      requiredBy = [''
        ARM autoenrollment flow as the free trial expiration
                needs to be automatic.''];
    };
    parallel = 1;
    scheduleExpression = "cron(0 6 ? * MON-FRI *)";
    size = "integrates";
    tags = {
      "Name" = "integrates_expire_free_trial";
      "fluidattacks:line" = "cost";
      "fluidattacks:comp" = "integrates";
    };
    timeout = 3600;
  };
  integrates_groups_languages_distribution = {
    attempts = 1;
    awsRole = "prod_integrates";
    command = [
      "m"
      "gitlab:fluidattacks/universe@trunk"
      "/integrates/utils/scheduler"
      "prod"
      "integrates.schedulers.groups_languages_distribution.main"
    ];
    enable = true;
    environment = [ "CACHIX_AUTH_TOKEN" ];
    meta = {
      description = ''
        Download all active group repositories
        and executes tokei over them.
        Save the results at the root level (individual)
        and at the group level (overall).
      '';
      requiredBy =
        [ "ARM collecting information on repositories language distribution." ];
    };
    parallel = 1;
    scheduleExpression = "cron(0 10 ? * MON *)";
    size = "integrates_large";
    tags = {
      "Name" = "integrates_groups_languages_distribution";
      "fluidattacks:line" = "cost";
      "fluidattacks:comp" = "integrates";
    };
    timeout = 86400;
  };
  integrates_machine_queue_sast_sca = {
    attempts = 3;
    awsRole = "prod_integrates";
    command = [
      "m"
      "gitlab:fluidattacks/universe@trunk"
      "/integrates/utils/scheduler"
      "prod"
      "integrates.schedulers.machine_queue_sast_sca.main"
    ];
    enable = true;
    environment = [ "CACHIX_AUTH_TOKEN" ];
    meta = {
      description = ''
        Queue machine SAST and SCA executions for all active groups.
      '';
      requiredBy =
        [ "Machine as it needs to run periodically over every group." ];
    };
    parallel = 1;
    scheduleExpression = "cron(0 13 ? * 7 *)";
    size = "integrates";
    tags = {
      "Name" = "integrates_machine_queue_sast_sca";
      "fluidattacks:line" = "cost";
      "fluidattacks:comp" = "integrates";
    };
    timeout = 86400;
  };
  integrates_machine_queue_cspm = {
    attempts = 3;
    awsRole = "prod_integrates";
    command = [
      "m"
      "gitlab:fluidattacks/universe@trunk"
      "/integrates/utils/scheduler"
      "prod"
      "integrates.schedulers.machine_queue_cspm.main"
    ];
    enable = true;
    environment = [ "CACHIX_AUTH_TOKEN" ];
    meta = {
      description = ''
        Queue machine CSPM executions for all active groups.
      '';
      requiredBy =
        [ "Machine as it needs to run periodically over every group." ];
    };
    parallel = 1;
    scheduleExpression = "cron(0 1 ? * TUE-SAT *)";
    size = "integrates";
    tags = {
      "Name" = "integrates_machine_queue_cspm";
      "fluidattacks:line" = "cost";
      "fluidattacks:comp" = "integrates";
    };
    timeout = 86400;
  };
  sbom_queue_group_images = {
    attempts = 3;
    awsRole = "prod_integrates";
    command = [
      "m"
      "gitlab:fluidattacks/universe@trunk"
      "/integrates/utils/scheduler"
      "prod"
      "integrates.schedulers.sbom_queue_group_images.main"
    ];
    enable = true;
    environment = [ "CACHIX_AUTH_TOKEN" ];
    meta = {
      description = ''
        Queue SBOM executions on the docker images available in the active groups.
      '';
      requiredBy =
        [ "Required by supply chain, it is necessary to run periodically." ];
    };
    parallel = 1;
    scheduleExpression = "cron(0 1 ? * TUE-SAT *)";
    size = "integrates";
    tags = {
      "Name" = "sbom_queue_group_images";
      "fluidattacks:line" = "cost";
      "fluidattacks:comp" = "integrates";
    };
    timeout = 86400;
  };
  integrates_machine_queue_dast = {
    attempts = 3;
    awsRole = "prod_integrates";
    command = [
      "m"
      "gitlab:fluidattacks/universe@trunk"
      "/integrates/utils/scheduler"
      "prod"
      "integrates.schedulers.machine_queue_dast.main"
    ];
    enable = true;
    environment = [ "CACHIX_AUTH_TOKEN" ];
    meta = {
      description = ''
        Queue machine DAST executions for all active groups.
      '';
      requiredBy =
        [ "Machine as it needs to run periodically over every group." ];
    };
    parallel = 1;
    scheduleExpression = "cron(0 3 ? * THU,SUN *)";
    size = "integrates";
    tags = {
      "Name" = "integrates_machine_queue_dast";
      "fluidattacks:line" = "cost";
      "fluidattacks:comp" = "integrates";
    };
    timeout = 86400;
  };
  integrates_machine_queue_re_attacks = {
    attempts = 3;
    awsRole = "prod_integrates";
    command = [
      "m"
      "gitlab:fluidattacks/universe@trunk"
      "/integrates/utils/scheduler"
      "prod"
      "integrates.schedulers.machine_queue_re_attacks.main"
    ];
    enable = true;
    environment = [ "CACHIX_AUTH_TOKEN" ];
    meta = {
      description = ''
        Queue pending reattacks
        on open vulnerabilities with a requested verification.
      '';
      requiredBy =
        [ "Machine in order to ensure that reattacks are verified." ];
    };
    parallel = 1;
    scheduleExpression = "cron(0 12,21 ? * 2-6 *)";
    size = "integrates";
    tags = {
      "Name" = "integrates_machine_queue_re_attacks";
      "fluidattacks:line" = "cost";
      "fluidattacks:comp" = "integrates";
    };
    timeout = 86400;
  };
  integrates_machine_vulnerability_notification = {
    attempts = 1;
    awsRole = "prod_integrates";
    command = [
      "m"
      "gitlab:fluidattacks/universe@trunk"
      "/integrates/utils/scheduler"
      "prod"
      "integrates.schedulers.machine_vulnerability_notification.main"
    ];
    enable = true;
    environment = [ "CACHIX_AUTH_TOKEN" ];
    meta = {
      description = ''
        Send machine vulnerability notification four times a day.
      '';
      requiredBy = [''
        ARM to report vulnerabilities from machine.
      ''];
    };
    parallel = 1;
    scheduleExpression = "cron(0 0,6,12,18 * * ? *)";
    size = "integrates";
    tags = {
      "Name" = "integrates_machine_vulnerability_notification";
      "fluidattacks:line" = "cost";
      "fluidattacks:comp" = "integrates";
    };
    timeout = 86400;
  };
  integrates_metrics_for_streams_accuracy = {
    attempts = 1;
    awsRole = "prod_integrates";
    command = [
      "m"
      "gitlab:fluidattacks/universe@trunk"
      "/integrates/utils/scheduler"
      "prod"
      "integrates.schedulers.metrics_for_streams_accuracy.main"
    ];
    enable = true;
    environment = [ "CACHIX_AUTH_TOKEN" ];
    meta = {
      description = ''
        Streams accuracy is a metric for the percentage of
        Streams-based indicators that are up to date.
        This schedule puts metric data in CloudWatch.
      '';
      requiredBy = [''
        ARM observability.
      ''];
    };
    parallel = 1;
    scheduleExpression = "cron(0 6,21 ? * * *)";
    size = "integrates_large";
    tags = {
      "Name" = "integrates_metrics_for_streams_accuracy";
      "fluidattacks:line" = "cost";
      "fluidattacks:comp" = "integrates";
    };
    timeout = 86400;
  };
  integrates_missing_environment_alert = {
    attempts = 3;
    awsRole = "prod_integrates";
    command = [
      "m"
      "gitlab:fluidattacks/universe@trunk"
      "/integrates/utils/scheduler"
      "prod"
      "integrates.schedulers.missing_environment_alert.main"
    ];
    enable = true;
    environment = [ "CACHIX_AUTH_TOKEN" ];
    meta = {
      description = ''
        Send emails when a group does not have registered environments URLs.
      '';
      requiredBy = [ "ARM to ensure the environments data is given." ];
    };
    parallel = 1;
    scheduleExpression = "cron(0 11 ? * 2-6 *)";
    size = "integrates";
    tags = {
      "Name" = "integrates_missing_environment_alert";
      "fluidattacks:line" = "cost";
      "fluidattacks:comp" = "integrates";
    };
    timeout = 86400;
  };
  integrates_numerator_report_digest = {
    attempts = 2;
    awsRole = "prod_integrates";
    command = [
      "m"
      "gitlab:fluidattacks/universe@trunk"
      "/integrates/utils/scheduler"
      "prod"
      "integrates.schedulers.numerator_report_digest.main"
    ];
    enable = true;
    environment = [ "CACHIX_AUTH_TOKEN" ];
    meta = {
      description = ''
        Automates the enumerator report and send an email notification with it.
      '';
      requiredBy = [ "Hackers as they need to fill out this report daily." ];
    };
    parallel = 1;
    scheduleExpression = "cron(20 5 ? * 2-6 *)";
    size = "integrates_large";
    tags = {
      "Name" = "integrates_numerator_report_digest";
      "fluidattacks:line" = "cost";
      "fluidattacks:comp" = "integrates";
    };
    timeout = 129600;
  };
  integrates_organization_vulnerabilities = {
    attempts = 3;
    awsRole = "prod_integrates";
    command = [
      "m"
      "gitlab:fluidattacks/universe@trunk"
      "/integrates/utils/scheduler"
      "prod"
      "integrates.schedulers.organization_vulnerabilities.main"
    ];
    enable = true;
    environment = [ "CACHIX_AUTH_TOKEN" ];
    meta = {
      description = ''
        Build an artifact with all vulnerabilities
        from an organization and store it in S3.
      '';
      requiredBy =
        [ "ARM to ease downloading all data from a group or an organization." ];
    };
    parallel = 1;
    scheduleExpression = "cron(30 1,7,13,19 ? * 2-6 *)";
    size = "integrates";
    tags = {
      "Name" = "integrates_organization_vulnerabilities";
      "fluidattacks:line" = "cost";
      "fluidattacks:comp" = "integrates";
    };
    timeout = 43200;
  };
  integrates_reminder_notification = {
    attempts = 3;
    awsRole = "prod_integrates";
    command = [
      "m"
      "gitlab:fluidattacks/universe@trunk"
      "/integrates/utils/scheduler"
      "prod"
      "integrates.schedulers.reminder_notification.main"
    ];
    enable = true;
    environment = [ "CACHIX_AUTH_TOKEN" ];
    meta = {
      description = ''
        Send an email to users inactive during three weeks.
      '';
      requiredBy = [ "ARM as some users stop using the platform." ];
    };
    parallel = 1;
    scheduleExpression = "cron(0 19 ? * * *)";
    size = "integrates";
    tags = {
      "Name" = "integrates_reminder_notification";
      "fluidattacks:line" = "cost";
      "fluidattacks:comp" = "integrates";
    };
    timeout = 86400;
  };
  integrates_remove_inactive_stakeholders = {
    attempts = 3;
    awsRole = "prod_integrates";
    command = [
      "m"
      "gitlab:fluidattacks/universe@trunk"
      "/integrates/utils/scheduler"
      "prod"
      "integrates.schedulers.remove_inactive_stakeholders.main"
    ];
    enable = true;
    environment = [ "CACHIX_AUTH_TOKEN" ];
    meta = {
      description = ''
        Remove inactive stakeholders
        according to the related organization policy.
      '';
      requiredBy = [''
        ARM to ensure the removal for users
        that have left the company
        or no longer have interest in the platform.
      ''];
    };
    parallel = 1;
    scheduleExpression = "cron(0 1 ? * * *)";
    size = "integrates";
    tags = {
      "Name" = "integrates_remove_inactive_stakeholders";
      "fluidattacks:line" = "cost";
      "fluidattacks:comp" = "integrates";
    };
    timeout = 86400;
  };
  integrates_requeue_actions = {
    attempts = 3;
    awsRole = "prod_integrates";
    command = [
      "m"
      "gitlab:fluidattacks/universe@trunk"
      "/integrates/utils/scheduler"
      "prod"
      "integrates.schedulers.requeue_actions.main"
    ];
    enable = true;
    environment = [ "CACHIX_AUTH_TOKEN" ];
    meta = {
      description = ''
        Send pending actions/jobs to batch when unsuccessful and update
        their state in DynamoDB table fi_async_processing.
      '';
      requiredBy = [''
        Batch as actions/jobs could fail and they will be resend a certain
        number of attempts.
      ''];
    };
    parallel = 1;
    scheduleExpression = "cron(15 * ? * * *)";
    size = "integrates";
    tags = {
      "Name" = "integrates_requeue_actions";
      "fluidattacks:line" = "cost";
      "fluidattacks:comp" = "integrates";
    };
    timeout = 86400;
  };
  integrates_reset_expired_accepted_findings = {
    attempts = 3;
    awsRole = "prod_integrates";
    command = [
      "m"
      "gitlab:fluidattacks/universe@trunk"
      "/integrates/utils/scheduler"
      "prod"
      "integrates.schedulers.reset_expired_accepted_findings.main"
    ];
    enable = true;
    environment = [ "CACHIX_AUTH_TOKEN" ];
    meta = {
      description = ''
        Update treatment for vulnerabilities where acceptance
        date has expired. This applies for both accepted and
        accepted undefined treatment statuses.
      '';
      requiredBy = [''
        ARM in order to comply with treatment acceptance expiration
        dates and fulfill the accepted treatment flow.
      ''];
    };
    parallel = 1;
    scheduleExpression = "cron(0 0 ? * * *)";
    size = "integrates";
    tags = {
      "Name" = "integrates_reset_expired_accepted_findings";
      "fluidattacks:line" = "cost";
      "fluidattacks:comp" = "integrates";
    };
    timeout = 86400;
  };
  integrates_send_deprecation_notice = {
    attempts = 3;
    awsRole = "prod_integrates";
    command = [
      "m"
      "gitlab:fluidattacks/universe@trunk"
      "/integrates/utils/scheduler"
      "prod"
      "integrates.schedulers.send_deprecation_notice.main"
    ];
    enable = true;
    environment = [ "CACHIX_AUTH_TOKEN" ];
    meta = {
      description = ''
        Send a mail to all users notifying a deprecated field,
        mutation, etc, will be soon removed from the ARM API.
      '';
      requiredBy = [''
        ARM users as API deprecations could go unnoticed
        by the customers, breaking their integrations.
      ''];
    };
    parallel = 1;
    scheduleExpression = "cron(0 12 12 * ? *)";
    size = "integrates";
    tags = {
      "Name" = "integrates_send_deprecation_notice";
      "fluidattacks:line" = "cost";
      "fluidattacks:comp" = "integrates";
    };
    timeout = 86400;
  };
  integrates_reviewer_progress_report = {
    attempts = 3;
    awsRole = "prod_integrates";
    command = [
      "m"
      "gitlab:fluidattacks/universe@trunk"
      "/integrates/utils/scheduler"
      "prod"
      "integrates.schedulers.reviewer_progress_report.main"
    ];
    enable = true;
    environment = [ "CACHIX_AUTH_TOKEN" ];
    meta = {
      description = ''
        Automates the reviewer report and send an email notification with it.
      '';
      requiredBy = [ "Reviewers as they need to fill out this report daily." ];
    };
    parallel = 1;
    scheduleExpression = "cron(0 6 ? * 2-6 *)";
    size = "integrates";
    tags = {
      "Name" = "integrates_reviewer_progress_report";
      "fluidattacks:line" = "cost";
      "fluidattacks:comp" = "integrates";
    };
    timeout = 21600;
  };
  integrates_send_trial_engagement_notification = {
    attempts = 1;
    awsRole = "prod_integrates";
    command = [
      "m"
      "gitlab:fluidattacks/universe@trunk"
      "/integrates/utils/scheduler"
      "prod"
      "integrates.schedulers.send_trial_engagement_notification.main"
    ];
    enable = true;
    environment = [ "CACHIX_AUTH_TOKEN" ];
    meta = {
      description = ''
        Send engagement notification emails to free trial users
        making them aware of the platform features and communication
        channels.
      '';
      requiredBy = [''
        ARM as part of the free trial flow leveraging on the free
        trial to boost user engagement.
      ''];
    };
    parallel = 1;
    scheduleExpression = "cron(0 15 ? * * *)";
    size = "integrates";
    tags = {
      "Name" = "integrates_send_trial_engagement_notification";
      "fluidattacks:line" = "cost";
      "fluidattacks:comp" = "integrates";
    };
    timeout = 3600;
  };
  integrates_treatment_alert_notification = {
    attempts = 3;
    awsRole = "prod_integrates";
    command = [
      "m"
      "gitlab:fluidattacks/universe@trunk"
      "/integrates/utils/scheduler"
      "prod"
      "integrates.schedulers.treatment_alert_notification.main"
    ];
    enable = true;
    environment = [ "CACHIX_AUTH_TOKEN" ];
    meta = {
      description = ''
        Send an alert email with a list of the
        accepted vulnerabilities in a given group
        whose acceptance date is close to expire.
      '';
      requiredBy = [''
        ARM sending only one mail with all related
        accepted treatment summary
        and avoid email flooding to users.
      ''];
    };
    parallel = 1;
    scheduleExpression = "cron(40 18 ? * 2-6 *)";
    size = "integrates";
    tags = {
      "Name" = "integrates_treatment_alert_notification";
      "fluidattacks:line" = "cost";
      "fluidattacks:comp" = "integrates";
    };
    timeout = 86400;
  };
  integrates_update_compliance = {
    attempts = 3;
    awsRole = "prod_integrates";
    command = [
      "m"
      "gitlab:fluidattacks/universe@trunk"
      "/integrates/utils/scheduler"
      "prod"
      "integrates.schedulers.update_compliance.main"
    ];
    enable = true;
    environment = [ "CACHIX_AUTH_TOKEN" ];
    meta = {
      description = ''
        Update all organizations compliance indicators.
      '';
      requiredBy = [''
        ARM in order to update info that will be displayed
        in the organization compliance view.
      ''];
    };
    parallel = 1;
    scheduleExpression = "cron(0 17 ? * * *)";
    size = "integrates";
    tags = {
      "Name" = "integrates_update_compliance";
      "fluidattacks:line" = "cost";
      "fluidattacks:comp" = "integrates";
    };
    timeout = 86400;
  };
  integrates_update_group_toe_sorts = {
    attempts = 3;
    awsRole = "prod_integrates";
    command = [
      "m"
      "gitlab:fluidattacks/universe@trunk"
      "/integrates/utils/scheduler"
      "prod"
      "integrates.schedulers.update_group_toe_sorts.main"
    ];
    enable = true;
    environment = [ "CACHIX_AUTH_TOKEN" ];
    meta = {
      description = ''
        Update all sorts attributes for all active groups'
        toe lines retrieving sorts results from S3.
      '';
      requiredBy = [''
        Sorts as updating this results using the ARM API
        is too slow.
      ''];
    };
    parallel = 1;
    scheduleExpression = "cron(0 5 ? * 2-6 *)";
    size = "integrates";
    tags = {
      "Name" = "integrates_update_group_toe_sorts";
      "fluidattacks:line" = "cost";
      "fluidattacks:comp" = "integrates";
    };
    timeout = 86400;
  };
  integrates_update_group_toe_vulns = {
    attempts = 3;
    awsRole = "prod_integrates";
    command = [
      "m"
      "gitlab:fluidattacks/universe@trunk"
      "/integrates/utils/scheduler"
      "prod"
      "integrates.schedulers.update_group_toe_vulns.main"
    ];
    enable = true;
    environment = [ "CACHIX_AUTH_TOKEN" ];
    meta = {
      description = ''
        Update the has_vulnerabilities attribute in the
        ToE surface
      '';
      requiredBy = [''
        ARM to display updated info in the surface view
        and related flows.
      ''];
    };
    parallel = 1;
    scheduleExpression = "cron(0 10 ? * * *)";
    size = "integrates_large";
    tags = {
      "Name" = "integrates_update_group_toe_vulns";
      "fluidattacks:line" = "cost";
      "fluidattacks:comp" = "integrates";
    };
    timeout = 86400;
  };
  integrates_update_indicators = {
    attempts = 2;
    awsRole = "prod_integrates";
    command = [
      "m"
      "gitlab:fluidattacks/universe@trunk"
      "/integrates/utils/scheduler"
      "prod"
      "integrates.schedulers.update_indicators.main"
    ];
    enable = true;
    environment = [ "CACHIX_AUTH_TOKEN" ];
    meta = {
      description = ''
        Update all active groups unreliable indicators in db.
      '';
      requiredBy = [''
        ARM analytics as these preprocessed data is used
        is generating its charts.
      ''];
    };
    parallel = 1;
    scheduleExpression = "cron(0 19 ? * 2-6 *)";
    size = "integrates_large";
    tags = {
      "Name" = "integrates_update_indicators";
      "fluidattacks:line" = "cost";
      "fluidattacks:comp" = "integrates";
    };
    timeout = 86400;
  };
  integrates_update_organization_overview = {
    attempts = 2;
    awsRole = "prod_integrates";
    command = [
      "m"
      "gitlab:fluidattacks/universe@trunk"
      "/integrates/utils/scheduler"
      "prod"
      "integrates.schedulers.update_organization_overview.main"
    ];
    enable = true;
    environment = [ "CACHIX_AUTH_TOKEN" ];
    meta = {
      description = ''
        Update organizations overview metrics in the
        unreliable indicators in db.
      '';
      requiredBy = [''
        ARM to display the organization overview metrics.
      ''];
    };
    parallel = 1;
    scheduleExpression = "cron(30 20 ? * 6 *)";
    size = "integrates_large";
    tags = {
      "Name" = "integrates_update_organization_overview";
      "fluidattacks:line" = "cost";
      "fluidattacks:comp" = "integrates";
    };
    timeout = 172800;
  };
  integrates_update_organization_repositories = {
    attempts = 2;
    awsRole = "prod_integrates";
    command = [
      "m"
      "gitlab:fluidattacks/universe@trunk"
      "/integrates/utils/scheduler"
      "prod"
      "integrates.schedulers.update_organization_repositories.main"
    ];
    enable = true;
    environment = [ "CACHIX_AUTH_TOKEN" ];
    meta = {
      description = ''
        Update integration repositories data for all organizations.
      '';
      requiredBy = [''
        ARM to enable Azure DevOps integrations and to let the
        customers identify which repositories are not included in
        Fluid Attacks services.
      ''];
    };
    parallel = 1;
    scheduleExpression = "cron(45 0 * * ? *)";
    size = "integrates_large";
    tags = {
      "Name" = "integrates_update_organization_repositories";
      "fluidattacks:line" = "cost";
      "fluidattacks:comp" = "integrates";
    };
    timeout = 86400;
  };
  integrates_update_portfolios = {
    attempts = 3;
    awsRole = "prod_integrates";
    command = [
      "m"
      "gitlab:fluidattacks/universe@trunk"
      "/integrates/utils/scheduler"
      "prod"
      "integrates.schedulers.update_portfolios.main"
    ];
    enable = true;
    environment = [ "CACHIX_AUTH_TOKEN" ];
    meta = {
      description = ''
        Update all portfolios unreliable indicators in db.
      '';
      requiredBy = [''
        ARM analytics as these preprocessed data is used
        is generating its charts.
      ''];
    };
    parallel = 1;
    scheduleExpression = "cron(0 7,14 ? * 2-6 *)";
    size = "integrates";
    tags = {
      "Name" = "integrates_update_portfolios";
      "fluidattacks:line" = "cost";
      "fluidattacks:comp" = "integrates";
    };
    timeout = 86400;
  };
  integrates_update_toe_lines_suggestions = {
    attempts = 3;
    awsRole = "prod_integrates";
    command = [
      "m"
      "gitlab:fluidattacks/universe@trunk"
      "/integrates/utils/scheduler"
      "prod"
      "integrates.schedulers.update_toe_lines_suggestions.main"
    ];
    enable = true;
    environment = [ "CACHIX_AUTH_TOKEN" ];
    meta = {
      description = ''
        Update toe lines vulnerability suggestions attribute
        for all groups
      '';
      requiredBy = [''
        Integrates hackers to receive suggestions about possible
        vulnerabilities in a file
      ''];
    };
    parallel = 1;
    scheduleExpression = "cron(0 23 ? * 7 *)";
    size = "integrates";
    tags = {
      "Name" = "integrates_update_toe_lines_suggestions";
      "fluidattacks:line" = "cost";
      "fluidattacks:comp" = "integrates";
    };
    timeout = 86400;
  };
  integrates_test_machine_flow = {
    attempts = 1;
    awsRole = "prod_integrates";
    command = [
      "m"
      "gitlab:fluidattacks/universe@trunk"
      "/integrates/utils/scheduler"
      "prod"
      "integrates.schedulers.machine_flow.main"
    ];
    enable = true;
    environment = [ "CACHIX_AUTH_TOKEN" ];
    meta = {
      description = ''
        Execute a full machine job in a test repository to
        check the process of reporting vulnerabilities is working
      '';
      requiredBy = [ "Status page" ];
    };
    parallel = 1;
    scheduleExpression = "cron(0 11-23/3 ? * 2-6 *)";
    size = "integrates";
    tags = {
      "Name" = "integrates_test_machine_flow";
      "fluidattacks:line" = "cost";
      "fluidattacks:comp" = "integrates";
    };
    timeout = 7200;
  };
}
