{ inputs, makeTemplate, projectPath, toFileJson, outputs, ... }: {
  jobs."/common/compute/schedule/parse-terraform" = makeTemplate {
    replace = {
      __argParser__ =
        projectPath "/common/compute/schedule/parse-terraform/src/__init__.py";
      __argData__ = toFileJson "data.json" (import ../data.nix);
      __argSizes__ = projectPath "/common/compute/arch/sizes/data.yaml";
    };
    searchPaths = {
      bin = [ inputs.nixpkgs.python39 inputs.nixpkgs.yq ];
      source = [ outputs."/common/utils/aws" outputs."/common/utils/sops" ];
    };

    template = ./template.sh;
    name = "common-compute-schedule-parse-terraform";
  };
}
