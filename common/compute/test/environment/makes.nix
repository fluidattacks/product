{ __system__, inputs, makePythonEnvironment, makeScript, outputs, ... }:
let
  version = "2024.9.346";
  architecture = {
    aarch64-linux = "arm64";
    x86_64-linux = "amd64";
  }."${__system__}";
  cloudflare-warp = if inputs.nixpkgs.stdenv.isLinux then
    inputs.nixpkgs.cloudflare-warp.overrideAttrs (_: {
      inherit version architecture;

      src = inputs.nixpkgs.fetchurl {
        url =
          "https://pkg.cloudflareclient.com/pool/noble/main/c/cloudflare-warp/cloudflare-warp_${version}.0_${architecture}.deb";
        hash = {
          aarch64-linux = "sha256-dgu/OiQPT7bkPnhrDArQg2lDAcOyhzZ5nJrjS2dqpFo=";
          x86_64-linux = "sha256-KwxLF7LWB49M+kZPJ9M4OcDSF1f3MX4S0dTtTkzQVRQ=";
        }."${__system__}";
      };
      buildInputs = [
        inputs.nixpkgs.dbus
        inputs.nixpkgs.gtk3
        inputs.nixpkgs.libpcap
        inputs.nixpkgs.openssl_3_3
        inputs.nixpkgs.nss
        inputs.nixpkgs.stdenv.cc.cc.lib
      ];
    })
  else
    (inputs.makeImpureCmd {
      cmd = "warp-cli";
      path = "/usr/local/bin/warp-cli";
    });
  pythonRequirements = makePythonEnvironment {
    pythonProjectDir = ./.;
    pythonVersion = "3.11";
  };
in {
  jobs."/common/compute/test/environment" = makeScript {
    name = "common-compute-test-environment";
    entrypoint = "python3 __argScript__ $@";
    replace.__argScript__ = ./__init__.py;
    searchPaths = {
      bin = [
        cloudflare-warp
        inputs.nixpkgs.git
        inputs.nixpkgs.python311
        inputs.nixpkgs.iproute2
      ];
      source = [ pythonRequirements ];
    };
  };
}
