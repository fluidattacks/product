import asyncio
import logging
import random
import sys
from typing import (
    Literal,
)
from urllib.parse import urlparse

from fluidattacks_core.git.warp import (
    WarpError,
    is_using_split_tunnel,
    public_ip_ready,
    warp_cli,
)

LOGGER = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO, format="[%(levelname)s] %(message)s")


def _get_domain(url: str) -> str:
    return urlparse(url).hostname


async def log_split_tunnel_usage(urls: list[str]) -> None:
    async def log_usage(url: str) -> None:
        url_domain = _get_domain(url)
        is_split = await is_using_split_tunnel(url_domain)
        LOGGER.info("'%s' is using split tunnel: %s", url, is_split)

    await asyncio.gather(*map(log_usage, urls))


async def _warp_cli_debug(*args: str) -> tuple[str, str]:
    cmd = f"warp-cli --accept-tos {' '.join(args)}"

    LOGGER.info(f"Running '{cmd}'...")
    out = await warp_cli(*args)

    stdout = out[0].decode()
    stderr = out[1].decode()
    LOGGER.info(f"Printing stdout for '{cmd}'...")
    LOGGER.info(stdout)
    LOGGER.info(f"Printing stderr for '{cmd}'...")
    LOGGER.info(stderr)

    return stdout, stderr


def _get_random_vnet() -> str:
    vnets = [
        "20ead430-f4fc-43f3-acec-7b08d416f417",
        "51317059-05c5-4d72-b777-a47196b25ab3",
        "8c20c72f-1697-4792-84d2-7dd32ad2e7a6",
        "d18ac663-4aa9-4619-b794-311f1f1db16a",
        "92d3926f-f86c-498c-b27c-85a2037f0d70",
        "e49535e7-d1fc-4c19-a377-75d47bd428df",
        "21e13d64-04f0-4c4e-a002-815260644506",
        "bfd35d2c-09a8-4e7c-99a8-3c94611c7a91",
        "cf7b7f88-c724-431b-9c19-e1c213dcaecb",
        "d9e10b10-1049-42cd-9977-ad91b73c6c4a",
        "978b6cd1-1905-448e-88f3-20d2746c4e89",
        "4b7da610-0ad5-4e97-af80-d1634d43b0a1",
        "1cfd741c-ba1e-45f9-a4ef-8f0d626480b4",
        "8641f741-8c00-471a-90f8-f6d3a0c0028c",
        "5681ee4c-1cc3-4d4e-9b88-4977a3dab2c7",
        "d33b43b8-c022-411f-834a-864173578402",
        "389f1a48-74a4-4cf8-b58c-6881d484a11a",
        "fc63aa67-a6cf-4a32-8917-bd348e2e7086",
        "e2cb3a06-e33c-4f9f-b3ad-dfbdec3c3788",
        "d9ecae5f-ec94-4196-b924-31fb51799884",
        "d7a8a665-c38c-4106-9100-7d5ce9353026",
        "364d3a79-e245-45ae-908b-6717ad435bb4",
        "ed5c360b-b189-4cf6-be24-d1b075b6235a",
    ]
    return random.choice(vnets)


async def main(target: Literal["default", "warp"]) -> None:
    wait_concurrency = 600  # 10 minutes

    match target:
        case "default":
            LOGGER.info("Testing default environment...")
            await _warp_cli_debug("status")
        case "warp":
            LOGGER.info("Testing warp environment...")
            # Connect to vnet
            try:
                await _warp_cli_debug("connect")
                await _warp_cli_debug("status")
                await _warp_cli_debug("vnet", _get_random_vnet())
                await _warp_cli_debug("vnet", "92d3926f-f86c-498c-b27c-85a2037f0d70")
                await public_ip_ready(
                    expected_ip="104.30.132.78",
                    attempts=10,
                    seconds_per_attempt=5,
                )
                urls = ["https://api.ipify.org?format=text"]
                await log_split_tunnel_usage(urls)
            except WarpError:
                sys.exit(1)
        case _:
            LOGGER.error(
                "Invalid target '%s', only 'default' and 'warp' are supported.",
                target,
            )
            sys.exit(1)

    # Test concurrency
    LOGGER.info(f"Waiting {wait_concurrency} seconds to test concurrency...")
    await asyncio.sleep(wait_concurrency)


if __name__ == "__main__":
    asyncio.run(main(sys.argv[1] if len(sys.argv) > 1 else "none"))
