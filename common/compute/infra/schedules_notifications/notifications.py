import json
import os
from typing import (
    Any,
    NamedTuple,
)

import requests

Item = dict[str, Any]


class SNSEvent(NamedTuple):
    alarm_name: str
    alarm_description: str
    metric_name: str


def _format_sns_event(event_info: Item) -> SNSEvent:
    message = json.loads(str(event_info["Message"]))
    return SNSEvent(
        alarm_name=message["AlarmName"],
        alarm_description=message["AlarmDescription"],
        metric_name=message["Trigger"]["MetricName"],
    )


def lambda_handler(event: Item, _: Any) -> None:
    webhook_url: str = os.environ["GOOGLE_CHAT_WEBHOOK"]
    sns_event = _format_sns_event(event["Records"][0]["Sns"])
    message = f"Please validate that {sns_event.metric_name} is working correctly."
    urls = {
        "metric": "https://us-east-1.console.aws.amazon.com/cloudwatch/home?"
        "region=us-east-1#metricsV2?graph=~(view~'timeSeries~stacked~false"
        "~region~'us-east-1~start~'-PT24H~end~'P0D~metrics~(~(~'failed_"
        f"schedules~'{sns_event.metric_name})))&query=~'*7bfailed_schedules"
        "*7d",
        "logs": "https://us-east-1.console.aws.amazon.com/cloudwatch/home?"
        "region=us-east-1#logsV2:logs-insights$3FqueryDetail$3D~(end~0~start"
        "~-43200~timeType~'RELATIVE~unit~'seconds~editorString~'fields*20*40"
        "timestamp*2c*20*40message*2c*20*40logStream*2c*20*40log*0a*7c*20"
        f"filter*20jobName*3d*3d*22{sns_event.metric_name}*22*0a*7c*20"
        "sort*20*40timestamp*20desc*0a*7c*20limit*2020~queryId~'8d9e7c817f3549"
        "ee-4bea2542-4863516-b1425e4-ed277efd331ec13eca5a3544~source"
        "~(~'batch_jobs_status))",
    }
    anchors = {
        "metric": f'<a href="{urls["metric"]}">Metric</a>',
        "logs": f'<a href="{urls["logs"]}">Logs</a>',
    }

    response = requests.post(
        url=webhook_url,
        json={
            "cardsV2": [
                {
                    "cardId": "SchedulesNotification",
                    "card": {
                        "header": {
                            "title": "Schedules Monitor",
                            "imageUrl": "https://developers.google.com/chat/"
                            "images/quickstart-app-avatar.png",
                            "imageType": "CIRCLE",
                        },
                        "sections": [
                            {
                                "widgets": [
                                    {
                                        "decoratedText": {
                                            "startIcon": {
                                                "knownIcon": "INVITE",
                                            },
                                            "text": message,
                                        }
                                    },
                                    {
                                        "decoratedText": {
                                            "startIcon": {
                                                "knownIcon": "BOOKMARK",
                                            },
                                            "text": anchors["metric"],
                                        }
                                    },
                                    {
                                        "decoratedText": {
                                            "startIcon": {
                                                "knownIcon": "BOOKMARK",
                                            },
                                            "text": anchors["logs"],
                                        }
                                    },
                                ],
                            }
                        ],
                    },
                },
            ],
        },
    )
    if response.status_code != 200:
        print(response.json())
