
data "aws_batch_job_definition" "common_sbom" {
  name = "prod_common"
}
data "aws_batch_job_queue" "common" {
  name = "common"
}
data "aws_batch_job_queue" "common_large" {
  name = "common_large"
}
resource "aws_iam_role" "sbom_step_function_role" {
  name = "sbom-step-function-role"

  assume_role_policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Action = "sts:AssumeRole",
        Effect = "Allow",
        Principal = {
          Service = "states.amazonaws.com"
        }
      }
    ]
  })

  tags = {
    "Name"              = "sbom_step_function_aws_iam_role"
    "fluidattacks:line" = "cost"
    "fluidattacks:comp" = "common"
  }
}

resource "aws_iam_policy" "sbom_step_function_policy" {
  name = "sbom-step-function-policy"

  policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      // NOFLUID This is is not assumed by a aws service
      {
        Action = [
          "batch:SubmitJob",
          "batch:DescribeJobs",
          "states:CreateStateMachine",
          "states:UpdateStateMachine",
          "states:TagResource",
          "states:ListTagsForResource",
          "states:StartExecution",
          "states:DescribeStateMachine",
          "states:DescribeExecution",
          "states:GetExecutionHistory",
          "events:PutRule",
          "events:PutTargets",
          "events:ActivateEventSource",
          "events:CancelReplay",
          "events:CreateApiDestination",
          "events:CreateArchive",
          "events:CreateConnection",
          "events:CreateEndpoint",
          "events:CreateEventBus",
          "events:CreatePartnerEventSource"
        ],
        Effect   = "Allow",
        Resource = "*"
      }
    ]
  })
  tags = {
    "Name"              = "sbom_step_aws_iam_policy_function_policy"
    "fluidattacks:line" = "cost"
    "fluidattacks:comp" = "common"
    "NOFLUID"           = "f325_is_not_assumed_by_a_aws_service"
  }
}

resource "aws_iam_role_policy_attachment" "sbom_step_function_policy_attachment" {
  role       = aws_iam_role.sbom_step_function_role.name
  policy_arn = aws_iam_policy.sbom_step_function_policy.arn
}

resource "aws_sfn_state_machine" "sfn_state_machine" {
  name     = "build-sbom-db-state-machine"
  role_arn = aws_iam_role.sbom_step_function_role.arn

  definition = jsonencode(
    {
      Comment = "Workflow to build SCA database for SBOM",
      StartAt = "Discover Providers",
      States = {
        "Discover Providers" = {
          Type     = "Task",
          Resource = "arn:aws:states:::batch:submitJob.sync",
          Arguments = {
            JobName       = "discover-providers",
            JobDefinition = data.aws_batch_job_definition.common_sbom.arn,
            JobQueue      = data.aws_batch_job_queue.common.arn,
            ContainerOverrides = {
              ResourceRequirements = [
                {
                  Type  = "VCPU",
                  Value = tostring(local.sizes["common"].cpu)
                },
                {
                  Type  = "MEMORY",
                  Value = tostring(local.sizes["common"].memory)
                }
              ],
              Environment = [
                {
                  Name  = "TASK_TOKEN",
                  Value = "{%$states.context.Task.Token%}"
                }
              ],
              Command = [
                "m",
                "gitlab:fluidattacks/universe@trunk",
                "/skims/sbom/schedules/refresh-advisories/list-providers"
              ]
            }
          },
          Next = "Execute Providers",
          Assign = {
            providers = "{%$states.result.providers%}"
          }
        },
        "Execute Providers" = {
          Type = "Map",
          ItemProcessor = {
            ProcessorConfig = {
              Mode = "INLINE"
            },
            StartAt = "Sync Provider",
            States = {
              "Sync Provider" = {
                Type     = "Task",
                Resource = "arn:aws:states:::batch:submitJob.sync",
                Arguments = {
                  JobDefinition = data.aws_batch_job_definition.common_sbom.arn,
                  JobQueue      = data.aws_batch_job_queue.common_large.arn,
                  Timeout       = { AttemptDurationSeconds = 18000 }
                  ContainerOverrides = {
                    ResourceRequirements = [
                      {
                        Type  = "VCPU",
                        Value = tostring(local.sizes["common_large"].cpu)
                      },
                      {
                        Type  = "MEMORY",
                        Value = tostring(local.sizes["common_large"].memory)
                      }
                    ],
                    Command = [
                      "m",
                      "gitlab:fluidattacks/universe@trunk",
                      "/skims/sbom/schedules/refresh-advisories/update-provider",
                      "{% $states.input %}"
                    ]
                  },
                  JobName = "{% $states.input %}"
                },
                Assign = {
                  provider = "{% $states.input %}"
                },
                Catch = [
                  {
                    ErrorEquals = [
                      "States.ALL"
                    ],
                    Next = "HandleFailure"
                  }
                ],
                End = true
              },
              "HandleFailure" = {
                Type = "Pass",
                End  = true
              }
            }
          },
          Items = "{% $providers %}",
          Next  = "Build Database"
        },
        "Build Database" = {
          Type     = "Task",
          Resource = "arn:aws:states:::batch:submitJob.sync",
          Arguments = {
            JobDefinition = data.aws_batch_job_definition.common_sbom.arn,
            JobQueue      = data.aws_batch_job_queue.common_large.arn,
            ContainerOverrides = {
              ResourceRequirements = [
                {
                  Type  = "VCPU",
                  Value = tostring(local.sizes["common_large"].cpu)
                },
                {
                  Type  = "MEMORY",
                  Value = tostring(local.sizes["common_large"].memory)
                }
              ],
              Command = [
                "m",
                "gitlab:fluidattacks/universe@trunk",
                "/skims/sbom/schedules/refresh-advisories/merge-providers"
              ]
            },
            JobName = "build-sbom-database"
          },
          End = true
        }
      },
      QueryLanguage = "JSONata"
    }
  )

  tags = {
    "Name"              = "State Machine build Sbom Database"
    "fluidattacks:line" = "cost"
    "fluidattacks:comp" = "common"
  }
}

resource "aws_cloudwatch_event_rule" "sbom_schedule_rule" {
  name                = "sbom-schedule-rule"
  description         = "Rule to trigger the SBOM Step Function on a schedule"
  schedule_expression = "cron(0 14 ? * MON-FRI *)"
  is_enabled          = true

  tags = {
    "Name"              = "sbom_schedule_rule"
    "fluidattacks:line" = "cost",
    "fluidattacks:comp" = "common"
  }

}

resource "aws_cloudwatch_event_target" "sbom_schedule_target" {
  rule = aws_cloudwatch_event_rule.sbom_schedule_rule.name
  arn  = aws_sfn_state_machine.sfn_state_machine.arn

  role_arn = data.aws_iam_role.main["prod_common"].arn
}
