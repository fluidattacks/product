resource "aws_batch_job_definition" "main" {
  for_each = data.aws_iam_role.main

  name = each.key
  type = "container"
  container_properties = jsonencode(
    {
      image      = "ghcr.io/fluidattacks/makes:24.12"
      jobRoleArn = each.value.arn

      # WARP requires capabilities NET_ADMIN, but that granularity
      # is not supported by AWS Batch containers, so privileged flag is used
      privileged = true

      # Will be overridden on job submission
      resourceRequirements = [
        { type = "VCPU", value = "1" },
        { type = "MEMORY", value = "7600" },
      ]

      # Mount WARP
      volumes = [
        {
          name = "cloudflare-warp-daemon"
          host = { sourcePath = "/run/cloudflare-warp" }
        },
        {
          name = "cloudflare-warp-config"
          host = { sourcePath = "/var/lib/cloudflare-warp" }
        },
      ]
      mountPoints = [
        {
          sourceVolume  = "cloudflare-warp-daemon"
          containerPath = "/run/cloudflare-warp"
          readOnly      = false
        },
        {
          sourceVolume  = "cloudflare-warp-config"
          containerPath = "/var/lib/cloudflare-warp"
          readOnly      = false
        },
      ]
    }
  )

  retry_strategy {
    attempts = 3
    evaluate_on_exit {
      action       = "RETRY"
      on_exit_code = 1
    }
    evaluate_on_exit {
      action    = "EXIT"
      on_reason = "CannotInspectContainerError:*"
    }
    evaluate_on_exit {
      action       = "RETRY"
      on_exit_code = 128
    }
  }

  timeout {
    attempt_duration_seconds = 86400
  }

  tags = {
    "Name"              = each.key
    "fluidattacks:line" = "cost"
    "fluidattacks:comp" = "common"
  }
}
