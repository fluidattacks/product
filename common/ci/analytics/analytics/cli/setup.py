from analytics.cli.option_args import (
    OptionArgs,
)
from analytics.cli.resources import (
    Resources,
)
from analytics.entities.match_type import (
    MatchType,
)
from analytics.entities.timescale import (
    TimeScale,
)

common_args = [
    OptionArgs(
        name="--pages",
        var_type=int,
        default="100",
        help="Number of pages to process (default: 100)",
    ),
    OptionArgs(
        name="--time_scale",
        var_type=str,
        default=TimeScale.DAY.value,
        help="Time scale for grouping (default: day)",
        choices=[
            TimeScale.HOUR.value,
            TimeScale.DAY.value,
            TimeScale.WEEK.value,
            TimeScale.MONTH.value,
        ],
    ),
    OptionArgs(
        name="--cache",
        help=("Used cached data for producing figures instead of fetching new data"),
        action="store_true",
    ),
]

arg_setup_by_workflow = {
    "default": common_args
    + [
        OptionArgs(
            name="--resource",
            var_type=str,
            default=Resources.JOBS.value,
            help="Target resource to fetch (default: jobs)",
            choices=[
                Resources.JOBS.value,
                Resources.PIPELINES.value,
                Resources.PIPELINE_INSIGHTS.value,
                Resources.MERGE_REQUESTS.value,
            ],
        ),
        OptionArgs(
            name="--target_job",
            var_type=str,
            help="Target job to fetch ",
        ),
        OptionArgs(
            name="--product",
            var_type=str,
            help="Target product for pipeline analysis",
        ),
        OptionArgs(
            name="--match_type",
            help=("Match type for target resource filtering"),
            var_type=str,
            choices=[
                MatchType.CONTAINS.value,
                MatchType.STARTS_WITH.value,
                MatchType.ENDS_WITH.value,
                MatchType.EXACT.value,
            ],
            default=MatchType.CONTAINS.value,
        ),
        OptionArgs(
            name="--ref",
            help=("Commit reference to fetch"),
            var_type=str,
        ),
    ],
    "e2e": common_args,
    "functional": common_args
    + [
        OptionArgs(
            name="--limit",
            var_type=int,
            default=10,
            help="Limit of functional tests to display (default: 10)",
        ),
    ],
}


def get_arg_setup_by_workflow(workflow_tag: str) -> list[OptionArgs] | None:
    if workflow_tag not in arg_setup_by_workflow:
        return None
    return arg_setup_by_workflow[workflow_tag]
