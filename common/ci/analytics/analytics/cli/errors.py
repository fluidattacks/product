class MissingSetupInCliWorkflow(Exception):
    def __init__(self, workflow: str) -> None:
        msg = f"Error - Arg configuration not found in setup for workflow: {workflow}"
        super().__init__(msg)
