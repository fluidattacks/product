import os
from logging import (
    getLogger,
)

from pandas import (
    DataFrame,
)

from analytics.entities.timescale import (
    TimeScale,
)
from analytics.parse.errors import (
    DataframeSavingError,
    EmptyDataInParserError,
    ParsingError,
)
from analytics.parse.parser import (
    DataframeParser,
)
from analytics.parse.utils import (
    add_date_cols_to_df,
    convert_cols_to_datetime,
)

LOGGER = getLogger()


CSV_SAVE_DIR = "output/csv"


class MergeRequestDataframeParser(DataframeParser):
    def __create_base_dir(self) -> None:
        if not os.path.exists(CSV_SAVE_DIR):
            os.makedirs(CSV_SAVE_DIR)

    def __init__(
        self,
    ) -> None:
        self.__data_frame: DataFrame | None = None
        self.__create_base_dir()

    def parse(self, raw_data: list[dict]) -> None:
        try:
            data_frame = DataFrame.from_dict(raw_data)
            convert_cols_to_datetime(data_frame, cols=["createdAt", "mergedAt"])

            data_frame["product"] = data_frame["title"].str.split("\\").str[0]

            add_date_cols_to_df(
                data_frame,
                from_field="mergedAt",
                cols=[
                    TimeScale.HOUR,
                    TimeScale.DAY,
                    TimeScale.WEEK,
                    TimeScale.MONTH,
                ],
            )

            self.__data_frame = data_frame

        except Exception as exc:
            LOGGER.error("Error parsing: %s.", exc)
            raise ParsingError("Could not parse incoming data.") from exc

    def pop(self) -> DataFrame:
        if self.__data_frame is None:
            raise EmptyDataInParserError()
        data_frame = self.__data_frame.copy()
        self.__data_frame = None
        return data_frame

    def save_data(self, filename: str = "merge_request_data.csv") -> str:
        file_path = f"{CSV_SAVE_DIR}/{filename}"
        try:
            if self.__data_frame is None:
                raise EmptyDataInParserError()
            self.__data_frame.to_csv(file_path)
        except Exception as exc:
            LOGGER.error("Could not save file to: %s", file_path)
            raise DataframeSavingError from exc

        return file_path
