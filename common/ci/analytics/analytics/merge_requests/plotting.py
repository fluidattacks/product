import asyncio
import os
from pathlib import (
    Path,
)

from pandas import (
    DataFrame,
    unique,
)

from analytics.plot.fig_result import (
    FailureReason,
    FigFailed,
    FigResult,
)
from analytics.plot.plotter import (
    DynamicSize,
    FixedSize,
    Plotter,
    PlotterParams,
)
from analytics.plot.utils import (
    FigureOptions,
    save_countplot_fig,
)

MRS_FIG_SAVE_DIR = "output/figures"
FIG_WIDTH_PER_HUE = 3
FIG_BASE_WIDTH = 12
FIG_HEIGHT = 6


class MergeRequestsPlotter(Plotter):
    def __create_base_dir(self) -> None:
        if not os.path.exists(MRS_FIG_SAVE_DIR):
            os.makedirs(MRS_FIG_SAVE_DIR)

    def __init__(self) -> None:
        self._data: DataFrame | None = None
        self.params = PlotterParams()
        self.__create_base_dir()

    def set_params(self, params: PlotterParams) -> None:
        self.params = params

    async def __gen_merge_requests_by_product(self, data: DataFrame) -> FigResult:
        fig_name = "merge_requests_by_product.png"
        return await save_countplot_fig(
            data=data,
            ax_field="product",
            fig_path=Path(f"{MRS_FIG_SAVE_DIR}/{fig_name}"),
            options=FigureOptions(
                size=FixedSize(14, 7),
                title=(
                    f"Merge requests by product between "
                    f"{data['mergedAt'].min()} and "
                    f"{data['mergedAt'].max()}"
                ),
            ),
        )

    async def __gen_merge_requests_by_product_over_time(self, data: DataFrame) -> FigResult:
        fig_name = "merge_requests_by_product_over_time.png"
        hue_len = len(unique(data[self.params.time_scale.value]))
        return await save_countplot_fig(
            data=data.sort_values(by=self.params.time_scale.value),
            ax_field=self.params.time_scale.value,
            fig_path=Path(f"{MRS_FIG_SAVE_DIR}/{fig_name}"),
            hue_field="product",
            options=FigureOptions(
                size=DynamicSize(hue_len=hue_len),
                legend_move=True,
                title=("Merge requests by product over time"),
            ),
        )

    def set_dataframe(self, data: DataFrame) -> None:
        self._data = data

    async def gen_all(self) -> tuple[FigResult, ...]:
        if self._data is None:
            return (FigFailed(FailureReason("Empty data in plotter")),)
        return await asyncio.gather(
            self.__gen_merge_requests_by_product_over_time(self._data),
            self.__gen_merge_requests_by_product(self._data),
        )
