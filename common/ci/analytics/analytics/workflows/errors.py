class UnavailableWorkflowError(Exception):
    pass


class FactoryFromNoneResourceError(Exception):
    pass


class NoParamsInWorkflowError(Exception):
    pass


class WorkflowPopulationError(Exception):
    def __init__(self, keys: list[str]) -> None:
        msg = f"Error - Could not populate workflow due to missing key(s): {keys}"
        super().__init__(msg)
