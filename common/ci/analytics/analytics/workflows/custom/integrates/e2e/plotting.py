import asyncio
import logging
import os
from pathlib import (
    Path,
)

from pandas import (
    DataFrame,
    unique,
)

from analytics.plot.fig_result import (
    FailureReason,
    FigFailed,
    FigResult,
    FigSkipped,
    SkipReason,
)
from analytics.plot.plotter import (
    FixedSize,
    Plotter,
    PlotterParams,
)
from analytics.plot.utils import (
    FigureOptions,
    save_barplot_fig,
    save_boxplot_fig,
    save_heatmap_fig,
    summarize_data,
)

SAVE_FIG_DIR = "output/figures"
TARGET_PIVOTS = ("name", "status")
LOGGER = logging.getLogger(__name__)


def format_duration(seconds: int) -> str:
    minutes = int(seconds // 60)
    remaining_seconds = int(seconds % 60)
    return f"{minutes}m{remaining_seconds}s"


class E2ePlotter(Plotter):
    def __create_base_dir(self) -> None:
        if not os.path.exists(SAVE_FIG_DIR):
            os.makedirs(SAVE_FIG_DIR)

    def set_params(self, params: PlotterParams) -> None:
        self.params = params

    def __init__(self) -> None:
        self._data: DataFrame | None = None
        self.__create_base_dir()
        self.params = PlotterParams()

    async def _gen_flakiness_plot(self, data: DataFrame) -> FigResult:
        if not self.params.with_flakiness:
            return FigSkipped(
                SkipReason(
                    "No flakiness detected in e2e tests between "
                    f"{data['startedAt'].min()} and {data['startedAt'].max()}",
                ),
            )
        filename = "e2e_flakiness_heatmap.png"
        date_format = "%Y-%m-%d %Hh"
        return await save_heatmap_fig(
            data=data,
            fig_path=Path(f"{SAVE_FIG_DIR}/{filename}"),
            pivots=TARGET_PIVOTS,
            options=FigureOptions(
                size=FixedSize(22, 10),
                title=(
                    "E2E tests flakiness heatmap between "
                    f"{data['startedAt'].min().strftime(date_format)} and "
                    f"{data['startedAt'].max().strftime(date_format)}."
                    f"Total samples: {len(data)}"
                ),
            ),
        )

    async def _gen_flakiness_over_time(self, data: DataFrame) -> FigResult:
        if not self.params.with_flakiness:
            return FigSkipped(
                SkipReason(
                    "No flakiness detected in e2e tests between "
                    f"{data['startedAt'].min()} and {data['startedAt'].max()}",
                ),
            )
        filename = "e2e_flakiness_over_time.png"
        date_format = "%Y-%m-%d %Hh"
        grouped_df = data.groupby(["name", self.params.time_scale.value]).agg(
            {"status": lambda x: (x == "SUCCESS").mean()},
        )
        grouped_df.rename(columns={"status": "success_rate"}, inplace=True)
        grouped_df.reset_index(inplace=True)
        return await save_barplot_fig(
            data=grouped_df,
            x_field=self.params.time_scale.value,
            y_field="success_rate",
            hue_field="name",
            fig_path=Path(f"{SAVE_FIG_DIR}/{filename}"),
            options=FigureOptions(
                size=FixedSize(22, 10),
                title=(
                    "E2E tests success rate between "
                    f"{data['startedAt'].min().strftime(date_format)} and "
                    f"{data['startedAt'].max().strftime(date_format)}."
                    f"Total samples: {len(data)}"
                ),
            ),
        )

    async def _gen_jobs_duration_over_time(self, data: DataFrame) -> FigResult:
        filtered_data = data[data["status"] == "SUCCESS"].dropna()
        filename = "e2e_timings_over_time.png"
        total_time_units = len(unique(data[self.params.time_scale.value]))
        return await save_boxplot_fig(
            data=filtered_data,
            x_field=self.params.time_scale.value,
            y_field="duration",
            hue_field="name",
            fig_path=Path(f"{SAVE_FIG_DIR}/{filename}"),
            options=FigureOptions(
                size=FixedSize(32, 7),
                title=(
                    "Jobs duration over time"
                    f" ({total_time_units} {self.params.time_scale.value}s, "
                    f"{len(data)} records)"
                ),
            ),
        )

    async def _gen_global_jobs_duration(self, data: DataFrame) -> FigResult:
        filtered_df = data[data["status"] == "SUCCESS"].dropna()
        filtered_df["job"] = data["name"].apply(lambda x: int(x.split(" ")[-1].split("/")[0]))
        filtered_df.sort_values(by="job", inplace=True)

        filename = "e2e_global_timings.png"
        init_date = filtered_df["startedAt"].min()
        end_date = filtered_df["startedAt"].max()
        LOGGER.info(
            "Average e2e jobs duration between %s and %s:\n%s",
            init_date,
            end_date,
            summarize_data(
                data=filtered_df,
                group_fields=["job"],
                target_field="duration",
            ),
        )
        return await save_boxplot_fig(
            data=filtered_df,
            x_field="job",
            y_field="duration",
            fig_path=Path(f"{SAVE_FIG_DIR}/{filename}"),
            options=FigureOptions(
                size=FixedSize(32, 7),
                title=(
                    f"Overall jobs duration between {init_date} and {end_date}"
                    f" ({len(filtered_df)} records)"
                ),
            ),
        )

    def set_dataframe(self, data: DataFrame) -> None:
        self._data = data

    async def gen_all(self) -> tuple[FigResult, ...]:
        if self._data is None:
            return (FigFailed(FailureReason("Empty data in plotter.")),)

        if len(self._data.index) == 0:
            return (FigFailed(FailureReason("Not enough data for analysis")),)
        return await asyncio.gather(
            self._gen_global_jobs_duration(data=self._data),
            self._gen_jobs_duration_over_time(data=self._data),
            self._gen_flakiness_plot(data=self._data),
            self._gen_flakiness_over_time(data=self._data),
        )
