import asyncio
import logging
import os
from pathlib import (
    Path,
)

from pandas import (
    DataFrame,
    unique,
)

from analytics.plot.fig_result import (
    FailureReason,
    FigFailed,
    FigResult,
)
from analytics.plot.plotter import (
    FixedSize,
    Plotter,
    PlotterParams,
)
from analytics.plot.utils import (
    FigureOptions,
    save_boxplot_fig,
    save_heatmap_fig,
)

SAVE_FIG_DIR = "output/figures"
TARGET_PIVOTS = ("name", "status")
RENAME_MAP = {"duration": "duration [ms]"}
TARGET_FIELDS = ["duration", "name"]

LOGGER = logging.getLogger(__name__)


def _filter_functional_by_names(data: DataFrame, names: list[str]) -> DataFrame:
    return data[data["name"].isin(names)]


def add_failure_rate_column(data: DataFrame) -> DataFrame:
    data["failure_rate"] = data["failures"] / data["total"]
    return data


class FunctionalTestsPlotter(Plotter):
    def __create_base_dir(self) -> None:
        if not os.path.exists(SAVE_FIG_DIR):
            os.makedirs(SAVE_FIG_DIR)

    def __init__(self) -> None:
        self.__data: DataFrame | None = None
        self.__create_base_dir()
        self.params = PlotterParams()

    def set_params(self, params: PlotterParams, n_flaky: int = 20) -> None:
        self.params = params
        self.n_flaky = n_flaky

    async def gen_jobs_duration_over_time(self, data: DataFrame) -> FigResult:
        filename = "functional_tests_timings_over_time.png"
        total_time_units = len(unique(data[self.params.time_scale.value]))
        return await save_boxplot_fig(
            data=data,
            x_field=self.params.time_scale.value,
            y_field="duration",
            fig_path=Path(f"{SAVE_FIG_DIR}/{filename}"),
            options=FigureOptions(
                size=FixedSize(32, 7),
                title=(
                    "Jobs duration over time"
                    f" ({total_time_units} {self.params.time_scale.value}s, "
                    f"{len(data.index)} records)"
                ),
            ),
        )

    async def gen_global_jobs_duration(self, data: DataFrame) -> FigResult:
        trimmed = (
            data[TARGET_FIELDS]
            .groupby(by="name")
            .mean()
            .sort_values(by="duration")
            .head(self.n_flaky)
            .rename(RENAME_MAP)
        )
        LOGGER.info(
            "\n==================   Top %d slowest tests  ==================\n%s",
            self.n_flaky,
            trimmed,
        )
        filename = "functional_tests_global_timings.png"
        init_date = data["startedAt"].min()
        end_date = data["startedAt"].max()
        return await save_boxplot_fig(
            data=data,
            y_field="duration",
            fig_path=Path(f"{SAVE_FIG_DIR}/{filename}"),
            options=FigureOptions(
                size=FixedSize(32, 7),
                title=(
                    f"Overall jobs duration between {init_date} and {end_date}"
                    f" ({len(data.index)} records)"
                ),
            ),
        )

    async def gen_flakiness_plot(self, data: DataFrame) -> FigResult:
        filename = "functional_tests_flakiness.png"
        date_format = "%Y-%m-%d %Hh"

        flakiest = (
            data.groupby(["name"])
            .agg(
                {"status": lambda x: (x == "SUCCESS").mean()},
            )
            .rename(columns={"status": "success_rate"})
            .sort_values(by="success_rate")
            .head(self.n_flaky)
        )
        LOGGER.info(
            "\n==================   Top %d flakiest tests  ==================\n%s",
            self.n_flaky,
            flakiest,
        )
        filtered_data = _filter_functional_by_names(data=data, names=flakiest.index.tolist())
        filtered_data["name"] = filtered_data["name"].str.split("__").str[-1]
        return await save_heatmap_fig(
            data=filtered_data,
            fig_path=Path(f"{SAVE_FIG_DIR}/{filename}"),
            pivots=TARGET_PIVOTS,
            options=FigureOptions(
                size=FixedSize(32, 10),
                title=(
                    "E2E tests flakiness heatmap between "
                    f"{data['startedAt'].min().strftime(date_format)} and "
                    f"{data['startedAt'].max().strftime(date_format)}."
                    f"Total samples: {len(data)}"
                ),
            ),
        )

    def set_dataframe(self, data: DataFrame) -> None:
        self.__data = data

    async def gen_all(self) -> tuple[FigResult, ...]:
        if self.__data is None:
            return (FigFailed(FailureReason("Empty data in plotter.")),)
        return await asyncio.gather(
            self.gen_global_jobs_duration(data=self.__data),
            self.gen_jobs_duration_over_time(data=self.__data),
            self.gen_flakiness_plot(data=self.__data),
        )
