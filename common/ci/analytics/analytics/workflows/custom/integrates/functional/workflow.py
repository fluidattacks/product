import logging
from dataclasses import (
    dataclass,
)

from pandas import (
    DataFrame,
)

from analytics.entities.jobs import JobStatus
from analytics.entities.timescale import (
    TimeScale,
    parse_time_scale_str,
)
from analytics.fetch.fetcher import (
    FetcherParameters,
)
from analytics.jobs.fetcher import (
    JobsFetcher,
)
from analytics.jobs.parser import (
    JobDataframeParser,
)
from analytics.plot.fig_result import (
    log_fig_results,
)
from analytics.plot.plotter import (
    PlotterParams,
)
from analytics.workflows.custom.integrates.functional.plotting import (
    FunctionalTestsPlotter,
)
from analytics.workflows.errors import (
    NoParamsInWorkflowError,
    WorkflowPopulationError,
)
from analytics.workflows.workflow import (
    WorkFlow,
)

LOGGER = logging.getLogger(__name__)


def filter_functional_tests(data: DataFrame, field_name: str) -> DataFrame:
    return data[data[field_name].str.contains("integrates/back/test/functional")]


@dataclass
class FunctionalTestsParams:
    pages: int
    time_scale: TimeScale
    limit: int = 20
    cache: bool = False


class FunctionalTestsWorkflow(WorkFlow):
    tag = "functional"
    help = "Integrates functional tests analytics_tests"

    def __init__(self) -> None:
        self.params: FunctionalTestsParams | None = None

    def populate(self, workflow_data: dict) -> None:
        time_scale = parse_time_scale_str(workflow_data["time_scale"])
        if time_scale is None:
            raise WorkflowPopulationError(["time_scale"])
        self.params = FunctionalTestsParams(
            pages=workflow_data["pages"],
            time_scale=time_scale,
            limit=workflow_data["limit"],
            cache=workflow_data["cache"],
        )

    async def run(
        self,
        project_id: str,
        access_token: str,
    ) -> None:
        if not self.params:
            raise NoParamsInWorkflowError()
        jobs_fetcher = JobsFetcher()
        jobs_fetcher.set_params(
            FetcherParameters(
                access_token=access_token,
                pages=self.params.pages,
                project_id=project_id,
                statuses=[JobStatus.SUCCESS, JobStatus.FAILED],
            ),
        )
        jobs = await jobs_fetcher.fetch(cache=self.params.cache)
        jobs_parser = JobDataframeParser()
        jobs_parser.parse(jobs)
        data_frame = jobs_parser.pop()
        functional_df = filter_functional_tests(data_frame, field_name="name")

        func_test_plotter = FunctionalTestsPlotter()
        func_test_plotter.set_params(
            PlotterParams(time_scale=self.params.time_scale), n_flaky=self.params.limit
        )
        func_test_plotter.set_dataframe(functional_df)
        fig_results = await func_test_plotter.gen_all()
        log_fig_results(LOGGER, fig_results)


RENAME_MAP = {"duration": "duration [ms]"}
