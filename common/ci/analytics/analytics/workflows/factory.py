from analytics.workflows.errors import UnavailableWorkflowError
from analytics.workflows.workflow import (
    WorkFlow,
)


def parse_workflow_by_tag(tag: str, workflows: list[WorkFlow]) -> WorkFlow:
    for workflow in workflows:
        if tag == workflow.tag:
            return workflow
    raise UnavailableWorkflowError()
