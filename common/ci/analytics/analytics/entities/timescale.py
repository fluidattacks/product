from enum import (
    Enum,
)

from analytics.workflows.errors import WorkflowPopulationError


class TimeScale(Enum):
    HOUR = "hour"
    DAY = "day"
    WEEK = "week"
    MONTH = "month"


def parse_time_scale_str(scale_str: str) -> TimeScale:
    if scale_str not in [
        TimeScale.HOUR.value,
        TimeScale.DAY.value,
        TimeScale.WEEK.value,
        TimeScale.MONTH.value,
    ]:
        raise WorkflowPopulationError(["time_scale"])
    return TimeScale[scale_str.upper()]
