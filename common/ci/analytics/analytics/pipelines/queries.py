PIPELINES_QUERY = """
  {
    projects(ids: [#ID]) {
      nodes {
        pipelines(ref: #REF, after: #AFTER) {
          nodes {
            startedAt
            finishedAt
            duration
            user{
              username
            }
            commit{
              title
            }
            status
            ref
          }
          pageInfo{
            startCursor
            endCursor
            hasPreviousPage
            hasNextPage
          }
        }
      }
    }
  }
  """

PIPELINES_INSIGHTS_QUERY = """
{
  projects(ids: [#ID]) {
    nodes {
      pipelines(ref: #REF, status: SUCCESS, first:1) {
        nodes {
          startedAt
          finishedAt
          duration
          user {
            username
          }
          commit {
            title
          }
          status
          ref
          jobs(after:#AFTER) {
            nodes {
              name
              duration
              startedAt
              finishedAt
              needs {
                edges {
                  node {
                    name
                  }
                }
              }
            }
            pageInfo{
              hasNextPage
              startCursor
              endCursor
            }
          }
        }
      }
    }
  }
}
"""
