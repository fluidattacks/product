import asyncio
import logging
import os
from pathlib import Path

from pandas import DataFrame

from analytics.plot.fig_result import (
    FailureReason,
    FigFailed,
    FigResult,
)
from analytics.plot.plotter import (
    Plotter,
    PlotterParams,
)
from analytics.plot.utils import save_dep_pipelines_analysis

FIG_SAVE_DIR = "output/figures"
LOGGER = logging.getLogger(__name__)


def alias_needs(col: list[str]) -> str:
    if len(col) == 0:
        return "none"
    needs = []
    if "/integrates/back/deploy/dev" in col:
        needs.append("deploy back")
    if any("/integrates/front/deploy" in c for c in col):
        needs.append("deploy front")
    if any("/integrates/back/test" in c for c in col):
        needs.append("back tests")
    if any("/integrates/front/test" in c for c in col):
        needs.append("front tests")
    if any("/integrates/streams/test" in c for c in col):
        needs.append("streams tests")
    if any("/deployContainer/" in c for c in col):
        needs.append("deploy container")
    return ", ".join(needs) if needs else ", ".join(col)


class PipelineInsightsPlotter(Plotter):
    def __create_base_dir(self) -> None:
        if not os.path.exists(FIG_SAVE_DIR):
            os.makedirs(FIG_SAVE_DIR)

    def __init__(self) -> None:
        self._data: DataFrame | None = None
        self.params = PlotterParams()
        self.__create_base_dir()

    def set_params(self, params: PlotterParams) -> None:
        self.params = params

    async def _gen_pipeline_dependencies_analysis(self, data: DataFrame) -> FigResult:
        data["needs"] = data["needs"].apply(alias_needs)
        fig_name = "pipeline_dependencies_analysis.png"

        return await save_dep_pipelines_analysis(
            data, Path(f"{FIG_SAVE_DIR}/{fig_name}"), data["ref"].unique()[0]
        )

    def set_dataframe(self, data: DataFrame) -> None:
        self._data = data

    async def gen_all(self) -> tuple[FigResult, ...]:
        if self._data is None:
            return (FigFailed(FailureReason("Empty data in plotter")),)
        return await asyncio.gather(
            self._gen_pipeline_dependencies_analysis(self._data),
        )
