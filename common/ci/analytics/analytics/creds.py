import os

from analytics.errors.api_errors import (
    GitlabApiTokenNotFound,
)

UNIVERSE_PROJECT_ID = "gid://gitlab/Project/20741933"


def get_gitlab_api_token() -> str:
    value = os.getenv("UNIVERSE_ANALYTICS_TOKEN")
    if value:
        return value
    raise GitlabApiTokenNotFound()
