import json
import os

CACHE_DIR = "output/cache"


class CacheNotFoundError(Exception):
    pass


class CacheReadingError(Exception):
    pass


async def __store_cache(
    data: list[dict],
    start_cursor: str,
    end_cursor: str,
    cache_type: str,
) -> None:
    persistence_dict = {
        "start_cursor": start_cursor,
        "end_cursor": end_cursor,
        "total_items": len(data),
        "data": data,
    }
    if not os.path.exists(CACHE_DIR):
        os.makedirs(CACHE_DIR)
    with open(
        f"{CACHE_DIR}/{cache_type}_{start_cursor}_{end_cursor}.json",
        "w",
        encoding="utf-8",
    ) as cache_file:
        cache_file.write(json.dumps(persistence_dict, indent=4))


async def __clear_cache(cache_type: str) -> None:
    if os.path.exists(CACHE_DIR):
        for file in os.listdir(CACHE_DIR):
            if file.startswith(cache_type):
                os.remove(f"{CACHE_DIR}/{file}")


async def read_cache(cache_type: str) -> list[dict]:
    cache_files = [
        f"{CACHE_DIR}/{file}" for file in os.listdir(CACHE_DIR) if file.startswith(cache_type)
    ]
    if len(cache_files) == 0:
        raise CacheNotFoundError(f"Did not find any {cache_type} cache files in {CACHE_DIR}.")
    cache_file = cache_files[0]
    try:
        with open(cache_file, encoding="utf-8") as file:
            cache_data = json.load(file)
        return cache_data["data"]
    except Exception as exc:
        raise CacheReadingError("Could not read jobs from cache.") from exc


async def renew_cache(
    data: list[dict],
    start_cursor: str,
    end_cursor: str,
    cache_type: str,
) -> None:
    await __clear_cache(cache_type)
    await __store_cache(data, start_cursor, end_cursor, cache_type=cache_type)
