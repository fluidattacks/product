from httpx import (
    AsyncClient,
    Response,
)


class UnknownUrlException(Exception):
    pass


class ApiClient:
    def __init__(self, access_token: str) -> None:
        self.headers = {"Authorization": f"Bearer {access_token}"}
        self.target_url = "https://gitlab.com/api/graphql"
        self.client = AsyncClient()

    async def post(self, req_query: str) -> Response:
        if self.target_url:
            return await self.client.post(
                self.target_url,
                data={"query": req_query},
                headers=self.headers,
                timeout=3000,
            )
        raise UnknownUrlException

    async def close(self) -> None:
        return await self.client.aclose()
