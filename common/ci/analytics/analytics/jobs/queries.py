JOBS_QUERY = """
  {
    projects(ids: [#ID]) {
      nodes {
        id
        jobs(after: #AFTER, statuses: #STATUSES)      {
          nodes{
            name
            pipeline {
              id
            }
            refName
            duration
            detailedStatus{
              name
            }
            startedAt
            finishedAt
            status
            stage {
              name
            }
            failureMessage
            pipeline {
              id
              commit{
                title
              }
            }
          }
          pageInfo{
            startCursor
            endCursor
            hasPreviousPage
            hasNextPage
          }
        }
      }
    }
  }
  """
