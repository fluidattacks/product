import logging
import os

from pandas import (
    DataFrame,
    Series,
    to_datetime,
)

from analytics.entities.jobs import (
    JobStatus,
)
from analytics.entities.timescale import (
    TimeScale,
)
from analytics.parse.errors import (
    DataframeSavingError,
    EmptyDataInParserError,
    ParsingError,
)
from analytics.parse.parser import (
    DataframeParser,
)
from analytics.parse.utils import (
    add_date_cols_to_df,
)

LOGGER = logging.getLogger()

CSV_SAVE_DIR = "output/csv"


class JobDataframeParser(DataframeParser):
    def __create_base_dir(self) -> None:
        if not os.path.exists(CSV_SAVE_DIR):
            os.makedirs(CSV_SAVE_DIR)

    def __init__(self) -> None:
        self.__data_frame: DataFrame | None = None
        self.__create_base_dir()

    def __compute_super_status(self, element: Series) -> str:
        status, failure_message = element["status"], element["failureMessage"]

        if status == JobStatus.SUCCESS.value:
            return status
        return "Script Failure" if not failure_message else "Runner failure"

    def parse(self, raw_data: list[dict]) -> None:
        try:
            data_frame = DataFrame.from_dict(raw_data)
            data_frame["startedAt"] = to_datetime(data_frame["startedAt"])
            data_frame["finishedAt"] = to_datetime(data_frame["finishedAt"])
            data_frame["product"] = data_frame["commitTitle"].str.split("\\").str[0]

            data_frame["status"] = data_frame[["status", "failureMessage"]].apply(
                self.__compute_super_status,
                axis=1,
            )

            add_date_cols_to_df(
                data_frame,
                from_field="finishedAt",
                cols=[
                    TimeScale.HOUR,
                    TimeScale.DAY,
                    TimeScale.WEEK,
                    TimeScale.MONTH,
                ],
            )

            self.__data_frame = self._filter_only_dev(data_frame)

        except Exception as exc:
            LOGGER.error("Error parsing: %s.", exc)
            raise ParsingError("Could not parse incoming data.") from exc

    def _filter_only_dev(self, data: DataFrame) -> tuple[DataFrame, ...]:
        return data[data["refName"] != "trunk"]

    def pop(self) -> DataFrame:
        if self.__data_frame is None:
            raise EmptyDataInParserError()

        data_frame = self.__data_frame.copy()

        self.__data_frame = None

        return data_frame

    def save_data(self, filename: str = "jobs_data.csv") -> str:
        file_path = f"{CSV_SAVE_DIR}/{filename}"
        try:
            if self.__data_frame is None:
                raise EmptyDataInParserError()
            self.__data_frame.to_csv(file_path)
        except Exception as exc:
            LOGGER.error("Could not save file to: %s", file_path)
            raise DataframeSavingError from exc

        return file_path
