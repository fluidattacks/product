import asyncio

from analytics.cli.cli_parser import (
    build_arg_parser,
)
from analytics.creds import (
    UNIVERSE_PROJECT_ID,
    get_gitlab_api_token,
)
from analytics.enabled_workflows import (
    load_workflows,
)
from analytics.workflows.factory import (
    parse_workflow_by_tag,
)

GITLAB_API_TOKEN = get_gitlab_api_token()


async def main() -> None:
    enabled_workflows = load_workflows()
    main_parser = build_arg_parser(enabled_workflows)
    args = main_parser.parse_args()
    workflow = parse_workflow_by_tag(tag=args.workflow, workflows=enabled_workflows)
    workflow.populate(args.__dict__)
    return await workflow.run(
        project_id=UNIVERSE_PROJECT_ID,
        access_token=get_gitlab_api_token(),
    )


def run() -> None:
    asyncio.run(main())
