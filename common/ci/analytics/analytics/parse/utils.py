from pandas import (
    DataFrame,
    to_datetime,
)

from analytics.entities.timescale import (
    TimeScale,
)

DATE_FORMATS = {
    "hour": "%Y-%m-%d %Hh",
    "day": "%Y-%m-%d",
    "week": "%G-%V",
    "month": "%Y-%m",
}


def add_date_cols_to_df(
    data: DataFrame,
    from_field: str,
    cols: list[TimeScale],
) -> None:
    for col in cols:
        data[col.value] = data[from_field].dt.strftime(DATE_FORMATS[col.value])


def convert_cols_to_datetime(data: DataFrame, cols: list[str]) -> None:
    for col in cols:
        data[col] = to_datetime(data[col])
