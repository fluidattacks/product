from abc import (
    ABC,
    abstractmethod,
)

from pandas import (
    DataFrame,
)


class DataframeParser(ABC):
    @abstractmethod
    def parse(self, raw_data: list[dict]) -> None:
        pass

    @abstractmethod
    def pop(self) -> DataFrame:
        pass

    @abstractmethod
    def save_data(self, filename: str = "image.png") -> str:
        pass
