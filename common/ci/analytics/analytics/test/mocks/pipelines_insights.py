from typing import Any

PIPELINE_INSIGHTS_GITLAB_RESPONSE: dict[str, Any] = {
    "data": {
        "projects": {
            "nodes": [
                {
                    "pipelines": {
                        "nodes": [
                            {
                                "startedAt": "2024-12-12T22:39:26Z",
                                "finishedAt": "2024-12-12T22:56:16Z",
                                "duration": 1027,
                                "user": {"username": "jhurtadoatfluid"},
                                "commit": {
                                    "title": "integrates\\test(back): #13631 upload file mutation"
                                },
                                "status": "SUCCESS",
                                "ref": "jhurtadoatfluid",
                                "jobs": {
                                    "nodes": [
                                        {
                                            "name": "/integrates/retrieves/test/e2e run",
                                            "duration": 584,
                                            "startedAt": "2024-12-12T22:41:34Z",
                                            "finishedAt": "2024-12-12T22:51:19Z",
                                            "needs": {
                                                "edges": [
                                                    {
                                                        "node": {
                                                            "name": "/integrates/back/deploy/dev"
                                                        }
                                                    }
                                                ]
                                            },
                                        },
                                        {
                                            "name": "/integrates/web/e2e run 10/10",
                                            "duration": 534,
                                            "startedAt": "2024-12-12T22:45:06Z",
                                            "finishedAt": "2024-12-12T22:54:01Z",
                                            "needs": {
                                                "edges": [
                                                    {
                                                        "node": {
                                                            "name": "/integrates/back/deploy/dev"
                                                        }
                                                    },
                                                    {
                                                        "node": {
                                                            "name": "/integrates/front/deploy/dev"
                                                        }
                                                    },
                                                ]
                                            },
                                        },
                                        {
                                            "name": "/integrates/web/e2e run 9/10",
                                            "duration": 648,
                                            "startedAt": "2024-12-12T22:45:06Z",
                                            "finishedAt": "2024-12-12T22:55:55Z",
                                            "needs": {
                                                "edges": [
                                                    {
                                                        "node": {
                                                            "name": "/integrates/back/deploy/dev"
                                                        }
                                                    },
                                                    {
                                                        "node": {
                                                            "name": "/integrates/front/deploy/dev"
                                                        }
                                                    },
                                                ]
                                            },
                                        },
                                        {
                                            "name": "/integrates/web/e2e run 8/10",
                                            "duration": 444,
                                            "startedAt": "2024-12-12T22:45:06Z",
                                            "finishedAt": "2024-12-12T22:52:30Z",
                                            "needs": {
                                                "edges": [
                                                    {
                                                        "node": {
                                                            "name": "/integrates/back/deploy/dev"
                                                        }
                                                    },
                                                    {
                                                        "node": {
                                                            "name": "/integrates/front/deploy/dev"
                                                        }
                                                    },
                                                ]
                                            },
                                        },
                                        {
                                            "name": "/integrates/web/e2e run 7/10",
                                            "duration": 572,
                                            "startedAt": "2024-12-12T22:45:05Z",
                                            "finishedAt": "2024-12-12T22:54:38Z",
                                            "needs": {
                                                "edges": [
                                                    {
                                                        "node": {
                                                            "name": "/integrates/back/deploy/dev"
                                                        }
                                                    },
                                                    {
                                                        "node": {
                                                            "name": "/integrates/front/deploy/dev"
                                                        }
                                                    },
                                                ]
                                            },
                                        },
                                        {
                                            "name": "/integrates/web/e2e run 6/10",
                                            "duration": 538,
                                            "startedAt": "2024-12-12T22:45:05Z",
                                            "finishedAt": "2024-12-12T22:54:04Z",
                                            "needs": {
                                                "edges": [
                                                    {
                                                        "node": {
                                                            "name": "/integrates/back/deploy/dev"
                                                        }
                                                    },
                                                    {
                                                        "node": {
                                                            "name": "/integrates/front/deploy/dev"
                                                        }
                                                    },
                                                ]
                                            },
                                        },
                                        {
                                            "name": "/integrates/web/e2e run 5/10",
                                            "duration": 535,
                                            "startedAt": "2024-12-12T22:45:04Z",
                                            "finishedAt": "2024-12-12T22:54:00Z",
                                            "needs": {
                                                "edges": [
                                                    {
                                                        "node": {
                                                            "name": "/integrates/back/deploy/dev"
                                                        }
                                                    },
                                                    {
                                                        "node": {
                                                            "name": "/integrates/front/deploy/dev"
                                                        }
                                                    },
                                                ]
                                            },
                                        },
                                        {
                                            "name": "/integrates/web/e2e run 4/10",
                                            "duration": 674,
                                            "startedAt": "2024-12-12T22:45:01Z",
                                            "finishedAt": "2024-12-12T22:56:15Z",
                                            "needs": {
                                                "edges": [
                                                    {
                                                        "node": {
                                                            "name": "/integrates/back/deploy/dev"
                                                        }
                                                    },
                                                    {
                                                        "node": {
                                                            "name": "/integrates/front/deploy/dev"
                                                        }
                                                    },
                                                ]
                                            },
                                        },
                                        {
                                            "name": "/integrates/web/e2e run 3/10",
                                            "duration": 566,
                                            "startedAt": "2024-12-12T22:45:04Z",
                                            "finishedAt": "2024-12-12T22:54:31Z",
                                            "needs": {
                                                "edges": [
                                                    {
                                                        "node": {
                                                            "name": "/integrates/back/deploy/dev"
                                                        }
                                                    },
                                                    {
                                                        "node": {
                                                            "name": "/integrates/front/deploy/dev"
                                                        }
                                                    },
                                                ]
                                            },
                                        },
                                        {
                                            "name": "/integrates/web/e2e run 2/10",
                                            "duration": 544,
                                            "startedAt": "2024-12-12T22:45:03Z",
                                            "finishedAt": "2024-12-12T22:54:08Z",
                                            "needs": {
                                                "edges": [
                                                    {
                                                        "node": {
                                                            "name": "/integrates/back/deploy/dev"
                                                        }
                                                    },
                                                    {
                                                        "node": {
                                                            "name": "/integrates/front/deploy/dev"
                                                        }
                                                    },
                                                ]
                                            },
                                        },
                                        {
                                            "name": "/integrates/web/e2e run 1/10",
                                            "duration": 665,
                                            "startedAt": "2024-12-12T22:45:03Z",
                                            "finishedAt": "2024-12-12T22:56:08Z",
                                            "needs": {
                                                "edges": [
                                                    {
                                                        "node": {
                                                            "name": "/integrates/back/deploy/dev"
                                                        }
                                                    },
                                                    {
                                                        "node": {
                                                            "name": "/integrates/front/deploy/dev"
                                                        }
                                                    },
                                                ]
                                            },
                                        },
                                        {
                                            "name": "/integrates/machine/dast",
                                            "duration": 389,
                                            "startedAt": "2024-12-12T22:45:02Z",
                                            "finishedAt": "2024-12-12T22:51:31Z",
                                            "needs": {
                                                "edges": [
                                                    {
                                                        "node": {
                                                            "name": "/integrates/back/deploy/dev"
                                                        }
                                                    },
                                                    {
                                                        "node": {
                                                            "name": "/integrates/front/deploy/dev"
                                                        }
                                                    },
                                                ]
                                            },
                                        },
                                        {
                                            "name": "/integrates/web/wait",
                                            "duration": 485,
                                            "startedAt": "2024-12-12T22:41:35Z",
                                            "finishedAt": "2024-12-12T22:49:40Z",
                                            "needs": {
                                                "edges": [
                                                    {
                                                        "node": {
                                                            "name": "/integrates/back/deploy/dev"
                                                        }
                                                    }
                                                ]
                                            },
                                        },
                                        {
                                            "name": "/integrates/web/forces",
                                            "duration": 500,
                                            "startedAt": "2024-12-12T22:41:34Z",
                                            "finishedAt": "2024-12-12T22:49:55Z",
                                            "needs": {
                                                "edges": [
                                                    {
                                                        "node": {
                                                            "name": "/integrates/back/deploy/dev"
                                                        }
                                                    }
                                                ]
                                            },
                                        },
                                        {
                                            "name": "/lintWithAjv/integratesStreamsTriggers",
                                            "duration": 47,
                                            "startedAt": "2024-12-12T22:40:02Z",
                                            "finishedAt": "2024-12-12T22:40:49Z",
                                            "needs": {"edges": []},
                                        },
                                        {
                                            "name": "/lintTerraform/integratesInfra",
                                            "duration": 81,
                                            "startedAt": "2024-12-12T22:40:02Z",
                                            "finishedAt": "2024-12-12T22:41:23Z",
                                            "needs": {"edges": []},
                                        },
                                        {
                                            "name": "/integrates/back/deploy/dev",
                                            "duration": 89,
                                            "startedAt": "2024-12-12T22:40:03Z",
                                            "finishedAt": "2024-12-12T22:41:32Z",
                                            "needs": {"edges": []},
                                        },
                                        {
                                            "name": "/integrates/front/deploy/dev",
                                            "duration": 297,
                                            "startedAt": "2024-12-12T22:40:03Z",
                                            "finishedAt": "2024-12-12T22:45:01Z",
                                            "needs": {"edges": []},
                                        },
                                    ],
                                    "pageInfo": {
                                        "hasNextPage": False,
                                        "startCursor": "eyJpZCI6Ijg2Mjk2NjQ4MjYifQ",
                                        "endCursor": "eyJpZCI6Ijg2Mjk2NjQ1ODEifQ",
                                    },
                                },
                            }
                        ]
                    }
                }
            ]
        }
    },
    "correlationId": "fa80cdbc6734f707de6c80882afae04d",
}

FETCHED_PIPELINES_INSIGHTS: list[dict[str, Any]] = [
    {
        "startedAt": "2024-12-12T22:39:26Z",
        "finishedAt": "2024-12-12T22:56:16Z",
        "duration": 1027,
        "user": {"username": "jhurtadoatfluid"},
        "status": "SUCCESS",
        "ref": "jhurtadoatfluid",
        "jobs": [
            {
                "name": "/integrates/retrieves/test/e2e run",
                "duration": 584,
                "startedAt": "2024-12-12T22:41:34Z",
                "finishedAt": "2024-12-12T22:51:19Z",
                "needs": ["/integrates/back/deploy/dev"],
            },
            {
                "name": "/integrates/web/e2e run 10/10",
                "duration": 534,
                "startedAt": "2024-12-12T22:45:06Z",
                "finishedAt": "2024-12-12T22:54:01Z",
                "needs": ["/integrates/back/deploy/dev", "/integrates/front/deploy/dev"],
            },
            {
                "name": "/integrates/web/e2e run 9/10",
                "duration": 648,
                "startedAt": "2024-12-12T22:45:06Z",
                "finishedAt": "2024-12-12T22:55:55Z",
                "needs": ["/integrates/back/deploy/dev", "/integrates/front/deploy/dev"],
            },
            {
                "name": "/integrates/web/e2e run 8/10",
                "duration": 444,
                "startedAt": "2024-12-12T22:45:06Z",
                "finishedAt": "2024-12-12T22:52:30Z",
                "needs": ["/integrates/back/deploy/dev", "/integrates/front/deploy/dev"],
            },
            {
                "name": "/integrates/web/e2e run 7/10",
                "duration": 572,
                "startedAt": "2024-12-12T22:45:05Z",
                "finishedAt": "2024-12-12T22:54:38Z",
                "needs": ["/integrates/back/deploy/dev", "/integrates/front/deploy/dev"],
            },
            {
                "name": "/integrates/web/e2e run 6/10",
                "duration": 538,
                "startedAt": "2024-12-12T22:45:05Z",
                "finishedAt": "2024-12-12T22:54:04Z",
                "needs": ["/integrates/back/deploy/dev", "/integrates/front/deploy/dev"],
            },
            {
                "name": "/integrates/web/e2e run 5/10",
                "duration": 535,
                "startedAt": "2024-12-12T22:45:04Z",
                "finishedAt": "2024-12-12T22:54:00Z",
                "needs": ["/integrates/back/deploy/dev", "/integrates/front/deploy/dev"],
            },
            {
                "name": "/integrates/web/e2e run 4/10",
                "duration": 674,
                "startedAt": "2024-12-12T22:45:01Z",
                "finishedAt": "2024-12-12T22:56:15Z",
                "needs": ["/integrates/back/deploy/dev", "/integrates/front/deploy/dev"],
            },
            {
                "name": "/integrates/web/e2e run 3/10",
                "duration": 566,
                "startedAt": "2024-12-12T22:45:04Z",
                "finishedAt": "2024-12-12T22:54:31Z",
                "needs": ["/integrates/back/deploy/dev", "/integrates/front/deploy/dev"],
            },
            {
                "name": "/integrates/web/e2e run 2/10",
                "duration": 544,
                "startedAt": "2024-12-12T22:45:03Z",
                "finishedAt": "2024-12-12T22:54:08Z",
                "needs": ["/integrates/back/deploy/dev", "/integrates/front/deploy/dev"],
            },
            {
                "name": "/integrates/web/e2e run 1/10",
                "duration": 665,
                "startedAt": "2024-12-12T22:45:03Z",
                "finishedAt": "2024-12-12T22:56:08Z",
                "needs": ["/integrates/back/deploy/dev", "/integrates/front/deploy/dev"],
            },
            {
                "name": "/integrates/machine/dast",
                "duration": 389,
                "startedAt": "2024-12-12T22:45:02Z",
                "finishedAt": "2024-12-12T22:51:31Z",
                "needs": ["/integrates/back/deploy/dev", "/integrates/front/deploy/dev"],
            },
            {
                "name": "/integrates/web/wait",
                "duration": 485,
                "startedAt": "2024-12-12T22:41:35Z",
                "finishedAt": "2024-12-12T22:49:40Z",
                "needs": ["/integrates/back/deploy/dev"],
            },
            {
                "name": "/integrates/web/forces",
                "duration": 500,
                "startedAt": "2024-12-12T22:41:34Z",
                "finishedAt": "2024-12-12T22:49:55Z",
                "needs": ["/integrates/back/deploy/dev"],
            },
            {
                "name": "/lintWithAjv/integratesStreamsTriggers",
                "duration": 47,
                "startedAt": "2024-12-12T22:40:02Z",
                "finishedAt": "2024-12-12T22:40:49Z",
                "needs": [],
            },
            {
                "name": "/lintTerraform/integratesInfra",
                "duration": 81,
                "startedAt": "2024-12-12T22:40:02Z",
                "finishedAt": "2024-12-12T22:41:23Z",
                "needs": [],
            },
            {
                "name": "/integrates/back/deploy/dev",
                "duration": 89,
                "startedAt": "2024-12-12T22:40:03Z",
                "finishedAt": "2024-12-12T22:41:32Z",
                "needs": [],
            },
            {
                "name": "/integrates/front/deploy/dev",
                "duration": 297,
                "startedAt": "2024-12-12T22:40:03Z",
                "finishedAt": "2024-12-12T22:45:01Z",
                "needs": [],
            },
        ],
        "commitTitle": "integrates\\test(back): #13631 upload file mutation",
    }
]
