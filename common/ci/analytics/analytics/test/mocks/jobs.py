PIPELINE_ID = "gid://gitlab/Ci::Pipeline/1209209594"
COMMIT_TITLE = "skims\\refac(back): #11442 support files"


JOBS_GITLAB_RESPONSE = {
    "data": {
        "projects": {
            "nodes": [
                {
                    "id": "gid://gitlab/Project/20741933",
                    "jobs": {
                        "nodes": [
                            {
                                "name": "/testPython/skims@f338",
                                "pipeline": {
                                    "id": PIPELINE_ID,
                                    "commit": {"title": COMMIT_TITLE},
                                },
                                "refName": "ugomezatfluid",
                                "duration": None,
                                "detailedStatus": {"name": "PENDING"},
                                "startedAt": None,
                                "finishedAt": None,
                                "status": "PENDING",
                                "stage": {"name": "test"},
                                "failureMessage": None,
                            },
                            {
                                "name": "/testPython/skims@f335",
                                "pipeline": {
                                    "id": PIPELINE_ID,
                                    "commit": {"title": COMMIT_TITLE},
                                },
                                "refName": "ugomezatfluid",
                                "duration": None,
                                "detailedStatus": {"name": "PENDING"},
                                "startedAt": None,
                                "finishedAt": None,
                                "status": "PENDING",
                                "stage": {"name": "test"},
                                "failureMessage": None,
                            },
                            {
                                "name": "/testPython/skims@f333",
                                "pipeline": {
                                    "id": PIPELINE_ID,
                                    "commit": {"title": COMMIT_TITLE},
                                },
                                "refName": "ugomezatfluid",
                                "duration": None,
                                "detailedStatus": {"name": "PENDING"},
                                "startedAt": None,
                                "finishedAt": None,
                                "status": "PENDING",
                                "stage": {"name": "test"},
                                "failureMessage": None,
                            },
                            {
                                "name": "/testPython/skims@f332",
                                "pipeline": {
                                    "id": PIPELINE_ID,
                                    "commit": {"title": COMMIT_TITLE},
                                },
                                "refName": "ugomezatfluid",
                                "duration": None,
                                "detailedStatus": {"name": "PENDING"},
                                "startedAt": None,
                                "finishedAt": None,
                                "status": "PENDING",
                                "stage": {"name": "test"},
                                "failureMessage": None,
                            },
                            {
                                "name": "/testPython/skims@f325",
                                "pipeline": {
                                    "id": PIPELINE_ID,
                                    "commit": {"title": COMMIT_TITLE},
                                },
                                "refName": "ugomezatfluid",
                                "duration": None,
                                "detailedStatus": {"name": "PENDING"},
                                "startedAt": None,
                                "finishedAt": None,
                                "status": "PENDING",
                                "stage": {"name": "test"},
                                "failureMessage": None,
                            },
                        ],
                        "pageInfo": {
                            "startCursor": "eyJpZCI6IjYzNjcxNDgxMTQifQ",
                            "endCursor": "eyJpZCI6IjYzNjcxMjY0OTQifQ",
                            "hasPreviousPage": False,
                            "hasNextPage": True,
                        },
                    },
                },
            ],
        },
    },
}


FETCHED_JOBS = [
    {
        "name": "/testPython/skims@f338",
        "pipeline": {
            "id": PIPELINE_ID,
            "commit": {"title": COMMIT_TITLE},
        },
        "refName": "ugomezatfluid",
        "duration": None,
        "detailedStatus": "PENDING",
        "startedAt": None,
        "finishedAt": None,
        "status": "PENDING",
        "stage": "test",
        "failureMessage": None,
        "commitTitle": COMMIT_TITLE,
    },
    {
        "name": "/testPython/skims@f335",
        "pipeline": {
            "id": PIPELINE_ID,
            "commit": {"title": COMMIT_TITLE},
        },
        "refName": "ugomezatfluid",
        "duration": None,
        "detailedStatus": "PENDING",
        "startedAt": None,
        "finishedAt": None,
        "status": "PENDING",
        "stage": "test",
        "failureMessage": None,
        "commitTitle": COMMIT_TITLE,
    },
    {
        "name": "/testPython/skims@f333",
        "pipeline": {
            "id": PIPELINE_ID,
            "commit": {"title": COMMIT_TITLE},
        },
        "refName": "ugomezatfluid",
        "duration": None,
        "detailedStatus": "PENDING",
        "startedAt": None,
        "finishedAt": None,
        "status": "PENDING",
        "stage": "test",
        "failureMessage": None,
        "commitTitle": COMMIT_TITLE,
    },
    {
        "name": "/testPython/skims@f332",
        "pipeline": {
            "id": PIPELINE_ID,
            "commit": {"title": COMMIT_TITLE},
        },
        "refName": "ugomezatfluid",
        "duration": None,
        "detailedStatus": "PENDING",
        "startedAt": None,
        "finishedAt": None,
        "status": "PENDING",
        "stage": "test",
        "failureMessage": None,
        "commitTitle": COMMIT_TITLE,
    },
    {
        "name": "/testPython/skims@f325",
        "pipeline": {
            "id": PIPELINE_ID,
            "commit": {"title": COMMIT_TITLE},
        },
        "refName": "ugomezatfluid",
        "duration": None,
        "detailedStatus": "PENDING",
        "startedAt": None,
        "finishedAt": None,
        "status": "PENDING",
        "stage": "test",
        "failureMessage": None,
        "commitTitle": COMMIT_TITLE,
    },
]
