from typing import (
    Any,
)

COMMIT_TITLE1 = "integrates\\feat(back): #11488 issue webhook"
COMMIT_TITLE2 = "airs\\fix(front): #11568 correct some misspellings across project"
COMMIT_TITLE3 = "observes\\refac(build): #11703 rotate high company secrets"
COMMIT_TITLE4 = "integrates\\feat(build): #11720 upgrade cypress"

COMMIT_TITLE5 = "docs\\refac(doc): #11716 docs seo"


PIPELINES_GITLAB_RESPONSE: dict[str, Any] = {
    "data": {
        "projects": {
            "nodes": [
                {
                    "pipelines": {
                        "nodes": [
                            {
                                "startedAt": "2024-03-12T22:02:55Z",
                                "finishedAt": "2024-03-12T22:20:12Z",
                                "duration": 1037,
                                "user": {
                                    "username": (
                                        "project_20741933_bot_34c541660153ede8e1a646ddfe516c98"
                                    ),
                                },
                                "commit": {"title": COMMIT_TITLE1},
                            },
                            {
                                "startedAt": None,
                                "finishedAt": None,
                                "duration": None,
                                "user": {
                                    "username": (
                                        "project_20741933_bot_34c541660153ede8e1a646ddfe516c98"
                                    ),
                                },
                                "commit": {"title": COMMIT_TITLE2},
                            },
                            {
                                "startedAt": None,
                                "finishedAt": None,
                                "duration": None,
                                "user": {
                                    "username": (
                                        "project_20741933_bot_34c541660153ede8e1a646ddfe516c98"
                                    ),
                                },
                                "commit": {"title": COMMIT_TITLE2},
                            },
                            {
                                "startedAt": None,
                                "finishedAt": None,
                                "duration": None,
                                "user": {
                                    "username": (
                                        "project_20741933_bot_34c541660153ede8e1a646ddfe516c98"
                                    ),
                                },
                                "commit": {"title": COMMIT_TITLE3},
                            },
                            {
                                "startedAt": None,
                                "finishedAt": None,
                                "duration": None,
                                "user": {
                                    "username": (
                                        "project_20741933_bot_34c541660153ede8e1a646ddfe516c98"
                                    ),
                                },
                                "commit": {"title": COMMIT_TITLE3},
                            },
                            {
                                "startedAt": None,
                                "finishedAt": None,
                                "duration": None,
                                "user": {
                                    "username": (
                                        "project_20741933_bot_34c541660153ede8e1a646ddfe516c98"
                                    ),
                                },
                                "commit": {"title": COMMIT_TITLE2},
                            },
                            {
                                "startedAt": None,
                                "finishedAt": None,
                                "duration": None,
                                "user": {
                                    "username": (
                                        "project_20741933_bot_34c541660153ede8e1a646ddfe516c98"
                                    ),
                                },
                                "commit": {"title": COMMIT_TITLE4},
                            },
                            {
                                "startedAt": None,
                                "finishedAt": None,
                                "duration": None,
                                "user": {
                                    "username": (
                                        "project_20741933_bot_34c541660153ede8e1a646ddfe516c98"
                                    ),
                                },
                                "commit": {"title": COMMIT_TITLE5},
                            },
                            {
                                "startedAt": None,
                                "finishedAt": None,
                                "duration": None,
                                "user": {
                                    "username": (
                                        "project_20741933_bot_34c541660153ede8e1a646ddfe516c98"
                                    ),
                                },
                                "commit": {"title": COMMIT_TITLE3},
                            },
                            {
                                "startedAt": None,
                                "finishedAt": None,
                                "duration": None,
                                "user": {
                                    "username": (
                                        "project_20741933_bot_34c541660153ede8e1a646ddfe516c98"
                                    ),
                                },
                                "commit": {"title": COMMIT_TITLE5},
                            },
                        ],
                        "pageInfo": {
                            "startCursor": "eyJpZCI6IjEyMTEwODk2MDUifQ",
                            "endCursor": "eyJpZCI6IjEyMTEwNDMxMjkifQ",
                            "hasPreviousPage": False,
                            "hasNextPage": True,
                        },
                    },
                },
            ],
        },
    },
}


FETCHED_PIPELINES: list[dict[str, Any]] = [
    {
        "startedAt": "2024-03-12T22:02:55Z",
        "finishedAt": "2024-03-12T22:20:12Z",
        "duration": 1037,
        "user": {"username": "project_20741933_bot_34c541660153ede8e1a646ddfe516c98"},
        "commit": {"title": COMMIT_TITLE1},
        "commitTitle": COMMIT_TITLE1,
    },
    {
        "startedAt": None,
        "finishedAt": None,
        "duration": None,
        "user": {"username": "project_20741933_bot_34c541660153ede8e1a646ddfe516c98"},
        "commit": {"title": COMMIT_TITLE2},
        "commitTitle": COMMIT_TITLE2,
    },
    {
        "startedAt": None,
        "finishedAt": None,
        "duration": None,
        "user": {"username": "project_20741933_bot_34c541660153ede8e1a646ddfe516c98"},
        "commit": {"title": COMMIT_TITLE2},
        "commitTitle": COMMIT_TITLE2,
    },
    {
        "startedAt": None,
        "finishedAt": None,
        "duration": None,
        "user": {"username": "project_20741933_bot_34c541660153ede8e1a646ddfe516c98"},
        "commit": {"title": COMMIT_TITLE3},
        "commitTitle": COMMIT_TITLE3,
    },
    {
        "startedAt": None,
        "finishedAt": None,
        "duration": None,
        "user": {"username": "project_20741933_bot_34c541660153ede8e1a646ddfe516c98"},
        "commit": {"title": COMMIT_TITLE3},
        "commitTitle": COMMIT_TITLE3,
    },
    {
        "startedAt": None,
        "finishedAt": None,
        "duration": None,
        "user": {"username": "project_20741933_bot_34c541660153ede8e1a646ddfe516c98"},
        "commit": {"title": COMMIT_TITLE2},
        "commitTitle": COMMIT_TITLE2,
    },
    {
        "startedAt": None,
        "finishedAt": None,
        "duration": None,
        "user": {"username": "project_20741933_bot_34c541660153ede8e1a646ddfe516c98"},
        "commit": {"title": COMMIT_TITLE4},
        "commitTitle": COMMIT_TITLE4,
    },
    {
        "startedAt": None,
        "finishedAt": None,
        "duration": None,
        "user": {"username": "project_20741933_bot_34c541660153ede8e1a646ddfe516c98"},
        "commit": {"title": COMMIT_TITLE5},
        "commitTitle": COMMIT_TITLE5,
    },
    {
        "startedAt": None,
        "finishedAt": None,
        "duration": None,
        "user": {"username": "project_20741933_bot_34c541660153ede8e1a646ddfe516c98"},
        "commit": {"title": COMMIT_TITLE3},
        "commitTitle": COMMIT_TITLE3,
    },
    {
        "startedAt": None,
        "finishedAt": None,
        "duration": None,
        "user": {"username": "project_20741933_bot_34c541660153ede8e1a646ddfe516c98"},
        "commit": {"title": COMMIT_TITLE5},
        "commitTitle": COMMIT_TITLE5,
    },
]
