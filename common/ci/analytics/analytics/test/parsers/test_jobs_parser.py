import pytest

from analytics.jobs.parser import (
    JobDataframeParser,
)
from analytics.test.mocks.jobs import (
    FETCHED_JOBS,
)


@pytest.mark.parametrize(
    ("expected_cols"),
    (
        (
            [
                "name",
                "pipeline",
                "refName",
                "duration",
                "detailedStatus",
                "startedAt",
                "finishedAt",
                "status",
                "stage",
                "failureMessage",
                "commitTitle",
                "product",
                "hour",
                "day",
                "week",
                "month",
            ]
        ),
    ),
)
def test_jobs_parser(expected_cols: list[str]) -> None:
    jobs_parser = JobDataframeParser()
    jobs_parser.parse(raw_data=FETCHED_JOBS)
    dataframe = jobs_parser.pop()
    assert sorted(dataframe.columns) == sorted(expected_cols)
    assert len(dataframe) == 5
