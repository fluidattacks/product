import pytest

from analytics.merge_requests.parser import (
    MergeRequestDataframeParser,
)
from analytics.test.mocks.merge_requests import (
    FETCHED_MERGE_REQUESTS,
)


@pytest.mark.parametrize(
    ("expected_cols"),
    (
        (
            [
                "approved",
                "createdAt",
                "mergedAt",
                "state",
                "author",
                "title",
                "product",
                "hour",
                "day",
                "week",
                "month",
            ]
        ),
    ),
)
def test_mrs_parser(expected_cols: list[str]) -> None:
    mrs_parser = MergeRequestDataframeParser()
    mrs_parser.parse(raw_data=FETCHED_MERGE_REQUESTS)
    dataframe = mrs_parser.pop()
    assert sorted(dataframe.columns) == sorted(expected_cols)
    assert len(dataframe) == 7
