from unittest.mock import patch

import pytest
from httpx import Response

from analytics.fetch.fetcher import (
    FetcherParameters,
)
from analytics.pipelines.fetcher_insights import PipelinesInsightsFetcher
from analytics.test.mocks.pipelines_insights import (
    FETCHED_PIPELINES_INSIGHTS,
    PIPELINE_INSIGHTS_GITLAB_RESPONSE,
)

MODULE_AT_TEST = "analytics.pipelines.fetcher_insights."


@pytest.mark.asyncio
@pytest.mark.parametrize(
    ("project_id", "access_token", "gitlab_response", "fetched_data", "ref"),
    (
        (
            "test_project_id",
            "1234",
            PIPELINE_INSIGHTS_GITLAB_RESPONSE,
            FETCHED_PIPELINES_INSIGHTS,
            "jhurtadoatfluid",
        ),
    ),
)
async def test_pipeline_insights_fetching(
    project_id: str,
    access_token: str,
    gitlab_response: dict,
    fetched_data: list[dict],
    ref: str,
) -> None:
    pipelines_fetcher = PipelinesInsightsFetcher()
    pipelines_fetcher.set_params(
        FetcherParameters(project_id=project_id, access_token=access_token, ref=ref),
    )
    with patch(MODULE_AT_TEST + "ApiClient.post") as mocked_method:
        mocked_method.return_value = Response(200, json=gitlab_response)
        pipelines = await pipelines_fetcher.fetch()
        assert pipelines == fetched_data
        mocked_method.assert_awaited_once()
