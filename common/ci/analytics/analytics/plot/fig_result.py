import logging
from collections.abc import Iterable
from dataclasses import (
    dataclass,
)
from pathlib import (
    Path,
)


@dataclass
class SkipReason:
    reason: str

    def __str__(self) -> str:
        return f"Reason: [{self.reason}]"


@dataclass
class FailureReason:
    reason: str

    def __str__(self) -> str:
        return f"Reason: [{self.reason}]"


@dataclass
class FigResult:
    def saved(self) -> Path | None:
        return None

    def skipped(self) -> SkipReason | None:
        return None

    def failed(self) -> FailureReason | None:
        return None


@dataclass
class FigSaved(FigResult):
    fig_path: Path

    def saved(self) -> Path:
        return self.fig_path


@dataclass
class FigSkipped(FigResult):
    reason: SkipReason

    def skipped(self) -> SkipReason:
        return self.reason


@dataclass
class FigFailed(FigResult):
    failure_reason: FailureReason

    def failed(self) -> FailureReason:
        return self.failure_reason


def log_fig_results(logger: logging.Logger, fig_results: Iterable[FigResult]) -> None:
    for fig_result in fig_results:
        match fig_result:
            case FigSaved(path):
                logger.info("Saved figure: %s", path)
            case FigSkipped(reason):
                logger.info("Figure skipped, %s", reason)
            case FigFailed(failure):
                logger.error("Fail saving figure: %s", failure)
