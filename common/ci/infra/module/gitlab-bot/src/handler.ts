import type { LambdaFunctionURLEvent } from "aws-lambda";

import { issueForFailedPipeline } from "./issue-for-failed-pipeline";
import { issueReview } from "./issue-review";
import { mrMerge } from "./mr-merge";
import { mrReview } from "./mr-review";
import { pipelineCancel } from "./pipeline-cancel";
import type { TWebhookEvent } from "./types";

async function handle(event: LambdaFunctionURLEvent): Promise<void> {
  if (event.body === undefined) {
    console.log("Ignoring event as it has an empty body");
    return;
  }

  const body = JSON.parse(event.body) as TWebhookEvent;

  const relevantWorkItemEventTypes = ["issue", "task"];

  switch (body.object_kind) {
    case "issue":
    case "work_item":
      if (!relevantWorkItemEventTypes.includes(body.event_type)) {
        console.log(
          "Not required to process event for work item",
          body.event_type,
        );
        break;
      }

      console.log("Processing event for issue", body.object_attributes.iid);
      await mrReview.handleWorkItemEvent(body);
      await issueReview.handleWorkItemEvent(body);
      break;

    case "merge_request":
      console.log("Processing event for MR", body.object_attributes.iid);
      mrReview.handleMergeRequestEvent(body);
      await mrMerge.handleMergeRequestEvent(body);
      break;

    case "pipeline":
      console.log("Processing event for pipeline", body.object_attributes.id);
      await pipelineCancel.handlePipelineEvent(body);
      await issueForFailedPipeline.handlePipelineEvent(body);
      break;

    default:
      console.error("No handler for event", body);
  }
}

export { handle };
