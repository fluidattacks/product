import { gitlab } from "../gitlab";
import type { IPipelineEvent, TPipelineStatus } from "../types";

async function isMergeRequestPipeline(body: IPipelineEvent): Promise<boolean> {
  const mergeRequests = await gitlab.api.MergeRequests.all({
    authorUsername: body.object_attributes.ref,
    projectId: body.project.id,
    state: "opened",
  });
  const relevantMergeRequest = mergeRequests.at(0);

  if (relevantMergeRequest === undefined) {
    console.log(
      "[pipeline-cancel] No relevant MR found for pipeline",
      body.object_attributes.id,
    );
    return false;
  }

  const pipelines = await gitlab.api.MergeRequests.allPipelines(
    body.project.id,
    relevantMergeRequest.iid,
  );

  return (
    pipelines.toReversed().findIndex((pipeline) => {
      return pipeline.id === body.object_attributes.id;
    }) > 0
  );
}

async function handlePipelineEvent(body: IPipelineEvent): Promise<void> {
  const relevantStatuses: TPipelineStatus[] = [
    "created",
    "pending",
    "preparing",
    "running",
    "waiting_for_resource",
  ];

  if (
    !relevantStatuses.includes(body.object_attributes.status) ||
    body.object_attributes.ref === "trunk" ||
    body.object_attributes.source !== "push"
  ) {
    console.log(
      "[pipeline-cancel] Ignoring event for pipeline",
      body.object_attributes.id,
      "with status",
      body.object_attributes.status,
      "branch",
      body.object_attributes.ref,
      "and source",
      body.object_attributes.source,
    );
    return;
  }

  if (await isMergeRequestPipeline(body)) {
    console.log(
      "[pipeline-cancel] Cancelling MR pipeline",
      body.object_attributes.id,
    );
    await gitlab.api.Pipelines.cancel(
      body.project.id,
      body.object_attributes.id,
    );
    return;
  }

  const userPipelines = await gitlab.api.Pipelines.all(body.project.id, {
    ref: body.object_attributes.ref,
    status: "running",
    username: body.object_attributes.ref,
  });

  await Promise.all(
    userPipelines.slice(1).map(async (pipeline) => {
      console.log(
        "[pipeline-cancel] Cancelling obsolete branch pipeline",
        pipeline.id,
      );
      return gitlab.api.Pipelines.cancel(body.project.id, pipeline.id);
    }),
  );
}

export const pipelineCancel = {
  handlePipelineEvent,
};
