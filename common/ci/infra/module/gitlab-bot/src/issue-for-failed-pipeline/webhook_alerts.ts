import { gitlab } from "../gitlab";

async function getJobsFailedDueToStateLock(
  projectId: number,
  jobIds: number[],
): Promise<number[]> {
  const lockError = "Error acquiring the state lock";

  const failedJobIds = await Promise.all(
    jobIds.map(async (jobId) => {
      const jobLog = await gitlab.api.Jobs.showLog(projectId, jobId);
      return jobLog.includes(lockError) ? jobId : null;
    }),
  );

  const filteredJobIds = failedJobIds.filter(
    (jobId): jobId is number => jobId !== null,
  );

  if (filteredJobIds.length > 0) {
    console.log("Found jobs failed due to state lock", filteredJobIds);
  }

  return filteredJobIds;
}

export { getJobsFailedDueToStateLock };
