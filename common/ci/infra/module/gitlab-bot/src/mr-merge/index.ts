/* eslint-disable camelcase */
import { gitlab } from "../gitlab";
import type {
  IMergeRequestEvent,
  TMergeRequestEventAction,
  TMergeRequestStatus,
} from "../types";

async function rebaseOthers(projectId: number): Promise<void> {
  const mergeRequests = await gitlab.api.MergeRequests.all({
    projectId,
    state: "opened",
  });

  await Promise.all(
    mergeRequests.map(async (mr) => {
      return gitlab.api.MergeRequests.rebase(projectId, mr.iid, {
        skipCI: true,
      });
    }),
  );
}

async function getNextMergeStatus(
  body: IMergeRequestEvent,
  attempt = 1,
): Promise<TMergeRequestStatus> {
  const mr = await gitlab.api.MergeRequests.show(
    body.project.id,
    body.object_attributes.iid,
  );
  const statusChanged =
    mr.detailed_merge_status !== body.object_attributes.detailed_merge_status;

  if (statusChanged) {
    console.log(
      "[mr-merge] MR",
      body.object_attributes.iid,
      "changed status",
      body.object_attributes.detailed_merge_status,
      "->",
      mr.detailed_merge_status,
    );
    return mr.detailed_merge_status;
  }

  if (attempt === 4) {
    console.log(
      "[mr-merge] MR",
      body.object_attributes.iid,
      "didn't change status from",
      body.object_attributes.detailed_merge_status,
    );
    return mr.detailed_merge_status;
  }

  return getNextMergeStatus(body, attempt + 1);
}

async function handleMergeRequestEvent(
  body: IMergeRequestEvent,
): Promise<void> {
  const relevantActions: TMergeRequestEventAction[] = [
    "approval",
    "approved",
    "reopen",
    "update",
  ];

  if (
    !relevantActions.includes(body.object_attributes.action) ||
    body.object_attributes.state !== "opened"
  ) {
    console.log(
      "[mr-merge] Ignoring event for MR",
      body.object_attributes.iid,
      "with action",
      body.object_attributes.action,
      "and state",
      body.object_attributes.state,
    );
    return;
  }

  switch (body.object_attributes.detailed_merge_status) {
    case "checking":
    case "ci_must_pass":
    case "preparing":
    case "unchecked": {
      const nextStatus = await getNextMergeStatus(body);
      if (nextStatus !== body.object_attributes.detailed_merge_status) {
        await handleMergeRequestEvent({
          ...body,
          object_attributes: {
            ...body.object_attributes,
            detailed_merge_status: nextStatus,
          },
        });
      }
      break;
    }

    case "mergeable":
      console.log("[mr-merge] Merging MR", body.object_attributes.iid);
      await gitlab.api.MergeRequests.merge(
        body.project.id,
        body.object_attributes.iid,
        { shouldRemoveSourceBranch: true },
      );
      await rebaseOthers(body.project.id);
      break;

    case "need_rebase":
      console.log("[mr-merge] Rebasing MR", body.object_attributes.iid);
      await gitlab.api.MergeRequests.rebase(
        body.project.id,
        body.object_attributes.iid,
        { skipCI: true },
      );
      break;

    default:
      console.log(
        "[mr-merge] Ignoring event for MR",
        body.object_attributes.iid,
        "with merge status",
        body.object_attributes.detailed_merge_status,
      );
      break;
  }
}

export const mrMerge = {
  handleMergeRequestEvent,
};
