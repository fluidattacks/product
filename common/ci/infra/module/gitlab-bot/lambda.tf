resource "null_resource" "build" {
  provisioner "local-exec" {
    command = <<EOT
      : && pushd module/gitlab-bot \
        && npm clean-install \
        && npx tsc -p tsconfig.json \
        && rm ./dist/src/mr-review/dangerfile.js \
        && cp ./src/mr-review/dangerfile.ts ./dist/src/mr-review \
        && cp -a ./node_modules ./dist/src \
        && popd \
        || return 1
    EOT
  }

  triggers = {
    always_run = timestamp()
  }
}

data "archive_file" "gitlab_bot" {
  depends_on       = [null_resource.build]
  output_file_mode = "0666"
  output_path      = "${path.module}/dist.zip"
  source_dir       = "${path.module}/dist/src"
  type             = "zip"
}

resource "aws_lambda_function" "gitlab_bot" {
  architectures    = ["arm64"]
  depends_on       = [data.archive_file.gitlab_bot]
  filename         = data.archive_file.gitlab_bot.output_path
  function_name    = var.name
  handler          = "handler.handle"
  memory_size      = 1024
  role             = aws_iam_role.gitlab_bot.arn
  runtime          = "nodejs20.x"
  source_code_hash = data.archive_file.gitlab_bot.output_base64sha256
  timeout          = 60

  environment {
    variables = {
      GITLAB_API_TOKEN = var.gitlab_api_token
    }
  }

  tags = merge(
    var.tags,
    {
      NOFLUID = "f165_public_function_url"
    }
  )
}

resource "aws_lambda_function_url" "gitlab_bot" {
  function_name      = aws_lambda_function.gitlab_bot.id
  authorization_type = "NONE"
}
