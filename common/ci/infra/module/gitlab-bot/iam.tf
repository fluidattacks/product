data "aws_iam_policy_document" "gitlab_bot_assume" {
  statement {
    actions = ["sts:AssumeRole"]
    effect  = "Allow"
    sid     = "AssumeRolePolicy"

    principals {
      identifiers = ["lambda.amazonaws.com"]
      type        = "Service"
    }
  }
}

resource "aws_iam_role" "gitlab_bot" {
  assume_role_policy = data.aws_iam_policy_document.gitlab_bot_assume.json
  name               = var.name
  tags               = var.tags
}

data "aws_iam_policy_document" "gitlab_bot_policy" {
  statement {
    actions = [
      "logs:CreateLogGroup",
      "logs:CreateLogStream",
      "logs:PutLogEvents",
    ]
    effect = "Allow"
    resources = [
      "arn:aws:logs:${var.region}:${data.aws_caller_identity.main.account_id}:*"
    ]
    sid = "logPolicy"
  }
}

resource "aws_iam_policy" "gitlab_bot_policy" {
  name   = var.name
  policy = data.aws_iam_policy_document.gitlab_bot_policy.json
  tags   = var.tags
}

resource "aws_iam_role_policy_attachment" "gitlab_bot_policy_attachment" {
  role       = aws_iam_role.gitlab_bot.name
  policy_arn = aws_iam_policy.gitlab_bot_policy.arn
}
