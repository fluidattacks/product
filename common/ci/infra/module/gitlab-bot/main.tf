terraform {
  required_version = "~> 1.0"

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "5.34.0"
    }
  }
}

data "aws_caller_identity" "main" {}

variable "gitlab_api_token" {}
variable "name" {}
variable "region" {}
variable "tags" {}

output "endpoint" {
  value = aws_lambda_function_url.gitlab_bot.function_url
}
