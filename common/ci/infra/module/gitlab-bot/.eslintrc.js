/* eslint-disable functional/immutable-data
  --
  ESLint does not support ESM configuration at this time
  https://eslint.org/docs/latest/use/configure/configuration-files#configuration-file-formats
*/
module.exports = {
  env: { es2023: true, node: true },
  extends: [
    "eslint:all",
    "plugin:@typescript-eslint/all",
    "plugin:eslint-plugin-functional/all",
    "plugin:import/recommended",
    "plugin:import/typescript",
    "plugin:prettier/recommended",
  ],
  parser: "@typescript-eslint/parser",
  parserOptions: {
    ecmaVersion: "es2023",
    project: "./tsconfig.json",
    sourceType: "module",
    tsconfigRootDir: __dirname,
  },
  plugins: ["@typescript-eslint", "functional"],
  rules: {
    "@typescript-eslint/naming-convention": [
      "error",
      {
        format: ["camelCase", "UPPER_CASE"],
        selector: "variable",
      },
      {
        format: ["camelCase", "PascalCase"],
        selector: "variable",
        types: ["function"],
      },
      {
        format: ["PascalCase"],
        prefix: ["I"],
        selector: "interface",
      },
      {
        format: ["PascalCase"],
        prefix: ["T"],
        selector: "typeAlias",
      },
    ],
    "@typescript-eslint/no-magic-numbers": [
      "error",
      { ignore: [-1, 0, 1, 2, 4], ignoreTypeIndexes: true },
    ],
    "@typescript-eslint/prefer-readonly-parameter-types": "off",
    "func-style": ["error", "declaration"],
    "functional/functional-parameters": "off",
    "functional/no-conditional-statements": "off",
    "functional/no-expression-statements": "off",
    "functional/no-mixed-types": "off",
    "functional/no-return-void": "off",
    "functional/prefer-immutable-types": "off",
    "functional/type-declaration-immutability": "off",
    "id-length": [
      "error",
      { exceptions: ["_", "$", "t"], properties: "never" },
    ],
    "import/exports-last": "error",
    "import/first": "error",
    "import/group-exports": "error",
    "import/newline-after-import": "error",
    "import/no-absolute-path": "error",
    "import/no-cycle": "error",
    "import/no-default-export": "error",
    "import/no-deprecated": "error",
    "import/no-duplicates": "error",
    "import/no-extraneous-dependencies": "error",
    "import/no-named-as-default": "error",
    "import/no-named-as-default-member": "off",
    "import/no-named-default": "error",
    "import/no-namespace": "error",
    "import/no-self-import": "error",
    "import/no-unresolved": "error",
    "import/no-useless-path-segments": ["error", { noUselessIndex: true }],
    "import/no-webpack-loader-syntax": "error",
    "import/order": [
      "error",
      {
        alphabetize: { caseInsensitive: true, order: "asc" },
        groups: ["builtin", "external", "sibling"],
        "newlines-between": "always",
      },
    ],
    "max-lines-per-function": "off",
    "no-console": "off",
    "no-duplicate-imports": "off",
    "no-ternary": "off",
    "no-undefined": "off",
    "no-void": ["error", { allowAsStatement: true }],
    "one-var": ["error", "never"],
    "sort-imports": ["error", { ignoreDeclarationSort: true }],
  },
  settings: {
    "import/resolver": { typescript: true },
  },
};
