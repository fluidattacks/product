locals {
  runners = {
    airs = {
      runner = merge(
        local.config.runner,
        { tags = ["airs"] },
      )
      workers = local.config.workers
      tags = merge(
        local.config.tags,
        { "fluidattacks:comp" = "airs" },
      )
    }
    common = {
      runner = merge(
        local.config.runner,
        { tags = ["common"] },
      )
      workers = local.config.workers
      tags = merge(
        local.config.tags,
        { "fluidattacks:comp" = "common" },
      )
    }
    common-large = {
      runner = merge(
        local.config.runner,
        { tags = ["common-large"] },
      )
      workers = merge(
        local.config.workers,
        {
          instances = [
            "r6gd.large",
            "r7gd.large",
            "x2gd.large",
          ]
        },
      )
      tags = merge(
        local.config.tags,
        { "fluidattacks:comp" = "common" },
      )
    }
    common-x86 = {
      runner = merge(
        local.config.runner,
        { tags = ["common-x86"] },
      )
      workers = merge(
        local.config.workers,
        {
          ami       = "ami-014f7ab33242ea43c"
          instances = ["m5d.large"]
        }
      )
      tags = merge(
        local.config.tags,
        { "fluidattacks:comp" = "common" },
      )
    }
    common-x86-large = {
      runner = merge(
        local.config.runner,
        { tags = ["common-x86-large"] },
      )
      workers = merge(
        local.config.workers,
        {
          ami       = "ami-014f7ab33242ea43c"
          instances = ["r6id.large"]
        }
      )
      tags = merge(
        local.config.tags,
        { "fluidattacks:comp" = "common" },
      )
    }
    forces = {
      runner = merge(
        local.config.runner,
        { tags = ["forces"] },
      )
      workers = local.config.workers
      tags = merge(
        local.config.tags,
        { "fluidattacks:comp" = "forces" },
      )
    }
    integrates = {
      runner = merge(
        local.config.runner,
        {
          instance = "r8g.large"
          tags     = ["integrates"]
        },
      )
      workers = local.config.workers
      tags = merge(
        local.config.tags,
        { "fluidattacks:comp" = "integrates" },
      )
    }
    matches = {
      runner = merge(
        local.config.runner,
        { tags = ["matches"] },
      )
      workers = local.config.workers
      tags = merge(
        local.config.tags,
        { "fluidattacks:comp" = "matches" },
      )
    }
    melts = {
      runner = merge(
        local.config.runner,
        { tags = ["melts"] },
      )
      workers = local.config.workers
      tags = merge(
        local.config.tags,
        { "fluidattacks:comp" = "melts" },
      )
    }
    observes = {
      runner = merge(
        local.config.runner,
        { tags = ["observes"] },
      )
      workers = local.config.workers
      tags = merge(
        local.config.tags,
        { "fluidattacks:comp" = "etl" },
      )
    }
    retrieves = {
      runner = merge(
        local.config.runner,
        { tags = ["retrieves"] },
      )
      workers = local.config.workers
      tags = merge(
        local.config.tags,
        { "fluidattacks:comp" = "retrieves" },
      )
    }
    sifts = {
      runner = merge(
        local.config.runner,
        { tags = ["sifts"] },
      )
      workers = local.config.workers
      tags = merge(
        local.config.tags,
        { "fluidattacks:comp" = "sifts" },
      )
    }
    skims = {
      runner = merge(
        local.config.runner,
        { tags = ["skims"] },
      )
      workers = local.config.workers
      tags = merge(
        local.config.tags,
        { "fluidattacks:comp" = "skims" },
      )
    }
    sorts = {
      runner = merge(
        local.config.runner,
        { tags = ["sorts"] },
      )
      workers = local.config.workers
      tags = merge(
        local.config.tags,
        { "fluidattacks:comp" = "sorts" },
      )
    }
  }
  config = {
    runner = {
      instance       = "r8g.medium"
      version        = "16.8.0"
      ami            = "ami-0a9b0f07dd5feed47"
      monitoring     = true
      check-interval = 3
      user-data      = ""

      disk-size      = 15
      disk-type      = "gp3"
      disk-optimized = true

      tags = []
    }
    workers = {
      instances = [
        "r6gd.medium",
        "r7gd.medium",
        "x2gd.medium",
      ]
      ami        = "ami-02ccdccc3ba798e43"
      user-data  = local.user-data.ephemeral-disk
      monitoring = false
      limit      = 1000

      idle-count      = 0
      idle-time       = 60 * 30
      max-growth-rate = 40

      privileged   = false
      network-mode = null // bridge by default
      volumes      = []


      disk-size      = 15
      disk-type      = "gp3"
      disk-optimized = true
    }
    tags = {
      "fluidattacks:line"  = "innovation"
      "fluidattacks:stack" = "ci"
    "NOFLUID" = "f024.f333.f325_external_policies_imposible_modify" }
  }
  user-data = {
    ephemeral-disk = <<-EOT
      Content-Type: multipart/mixed; boundary="==BOUNDARY=="
      MIME-Version: 1.0

      --==BOUNDARY==
      Content-Type: text/cloud-config; charset="us-ascii"
      MIME-Version: 1.0
      Content-Transfer-Encoding: 7bit
      Content-Disposition: attachment; filename="cloud-config.txt"

      disk_setup:
        /dev/nvme1n1:
          table_type: mbr
          layout: true
          overwrite: true

      fs_setup:
        - label: nvme
          filesystem: ext4
          device: /dev/nvme1n1
          partition: auto
          overwrite: true

      mounts:
        - [/dev/nvme1n1, /var/lib/docker]

      --==BOUNDARY==--
    EOT
  }
}

module "runners" {
  source   = "cattle-ops/gitlab-runner/aws"
  version  = "8.0.1"
  for_each = local.runners

  # AWS
  vpc_id                 = data.aws_vpc.main.id
  enable_managed_kms_key = true

  # Runner
  subnet_id = data.aws_subnet.main["ci_1"].id
  runner_install = {
    pre_install_script = each.value.runner.user-data
  }
  runner_role = {
    allow_iam_service_linked_role_creation = true
  }
  runner_cloudwatch = {
    enable = true
  }
  runner_manager = {
    gitlab_check_interval   = each.value.runner.check-interval
    maximum_concurrent_jobs = each.value.workers.limit
  }
  runner_instance = {
    agent_instance_prefix = "ci-runner-${each.key}",
    ssm_access            = true
    type                  = each.value.runner.instance
    monitoring            = each.value.runner.monitoring
    name                  = "ci-${each.key}"
    output_limit          = 8192
    root_device_config = {
      delete_on_termination = true
      encrypted             = true
      volume_type           = each.value.runner.disk-type
      volume_size           = each.value.runner.disk-size
    }
    ebs_optimized = each.value.runner.disk-optimized
  }
  runner_gitlab = {
    url                                           = "https://gitlab.com"
    runner_version                                = each.value.runner.version
    preregistered_runner_token_ssm_parameter_name = aws_ssm_parameter.default[each.key].name
  }
  runner_worker_docker_machine_instance = {
    name_prefix              = "ci-worker-${each.key}"
    start_script             = each.value.workers.user-data
    destroy_after_max_builds = 128
    private_address_only     = false
    subnet_ids               = [for subnet in data.aws_subnet.main : subnet.id]
    idle_time                = each.value.workers.idle-time
    idle_count               = each.value.workers.idle-count
    max_growth_rate          = each.value.workers.max-growth-rate
    monitoring               = each.value.workers.monitoring
    types                    = each.value.workers.instances
    root_size                = each.value.workers.disk-size
    volume_type              = each.value.workers.disk-type
    ebs_optimized            = each.value.workers.disk-optimized
  }
  runner_ami_filter = {
    image-id = [each.value.runner.ami]
  }
  runner_metadata_options = {
    http_endpoint = "enabled",
    http_tokens   = "required",

    http_put_response_hop_limit = 2,
    instance_metadata_tags      = "disabled",
  }

  # Workers
  runner_worker_docker_machine_fleet = {
    enable = true
  }
  runner_worker = {
    ssm_access          = true
    type                = "docker+machine"
    max_jobs            = each.value.workers.limit
    request_concurrency = each.value.workers.max-growth-rate
  }
  runner_worker_docker_machine_instance_spot = {
    max_price = ""
    enable    = true
  }
  runner_worker_docker_options = {
    network_mode = each.value.workers.network-mode
    privileged   = each.value.workers.privileged
    pull_policy  = "always"
    volumes      = each.value.workers.volumes
  }
  runner_worker_docker_machine_ami_filter = {
    image-id = [each.value.workers.ami]
  }
  runner_worker_docker_machine_ec2_metadata_options = {
    http_tokens                 = "required",
    http_put_response_hop_limit = 3,
  }
  runner_worker_cache = {
    shared = true
    create = false
    policy = module.cache.policy_arn
    bucket = module.cache.bucket
  }
  runner_terminate_ec2_timeout_duration = 900

  # Tags
  environment           = "ci-${each.key}"
  security_group_prefix = ""
  iam_object_prefix     = ""
  tags                  = each.value.tags
}

resource "gitlab_user_runner" "default" {
  for_each = local.runners

  runner_type     = "group_type"
  group_id        = var.fluidattacksGitlabGroupId
  maximum_timeout = 3600
  tag_list        = each.value.runner.tags
  untagged        = false
  access_level    = "not_protected"
}


# SSM parameter to store each runner pre-registered token
resource "aws_ssm_parameter" "default" {
  for_each = local.runners

  type  = "String"
  name  = "/gitlab-runner/${each.key}/token"
  value = gitlab_user_runner.default[each.key].token
  tags = {
    "Name"              = "aws_ssm_parameter"
    "fluidattacks:line" = "cost"
    "fluidattacks:comp" = "common"
  }
}
