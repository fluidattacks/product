rec {
  core = {
    rules = {
      branch = branch: {
        "if" = ''$CI_COMMIT_BRANCH != "${branch}"'';
        "when" = "never";
      };
      branchNot = branch: {
        "if" = ''$CI_COMMIT_BRANCH == "${branch}"'';
        "when" = "never";
      };
      notMrs = {
        "if" = ''$CI_PIPELINE_SOURCE == "merge_request_event"'';
        "when" = "never";
      };
      notSchedules = {
        "if" = ''$CI_PIPELINE_SOURCE == "schedule"'';
        "when" = "never";
      };
      notTriggers = {
        "if" = ''$CI_PIPELINE_SOURCE == "trigger"'';
        "when" = "never";
      };
      titleMatching = pattern: { "if" = "$CI_COMMIT_TITLE =~ /${pattern}/"; };
      titleRule = { products, types }:
        let
          concatProducts = builtins.concatStringsSep "|" products;
          concatTypes = builtins.concatStringsSep "|" types;
          regex = if concatTypes == "" then
            "^(${concatProducts})"
          else
            "^(${concatProducts})\\\\(${concatTypes})";
        in core.rules.titleMatching regex;
    };
  };

  extras = {
    default = {
      before_script = [ ];
      id_tokens.CI_JOB_JWT_V2.aud = "https://gitlab.com";
      image = images.makes."24.12";
      retry = {
        max = 2;
        when = "runner_system_failure";
      };
    };
  };

  images = {
    devenv.latest =
      "docker.io/nixpkgs/devenv:latest@sha256:35875c2cf8f0190888ee141b0071188879bfc85e1285ceeb145d0b1ce451f366";
    makes = {
      latest = "ghcr.io/fluidattacks/makes:latest";
      "24.12" = "ghcr.io/fluidattacks/makes:24.12";
      "24.02" = "ghcr.io/fluidattacks/makes:24.02";
    };
  };

  jobs = {
    devenv = { path, script, extras ? { } }: {
      output = "${path} ${script}";
      gitlabExtra = {
        id_tokens.CI_JOB_JWT_V2.aud = "https://gitlab.com";
        image = images.devenv.latest;
        retry = {
          max = 2;
          when = "runner_system_failure";
        };
        script = [ "pushd ${path}" "devenv shell -- ${script}" ];
      } // extras;
    };
  };

  rules = {
    prod = [
      (core.rules.branch "trunk")
      core.rules.notSchedules
      core.rules.notTriggers
    ];
    dev = [
      (core.rules.branchNot "trunk")
      core.rules.notMrs
      core.rules.notSchedules
      core.rules.notTriggers
    ];
  };

  stages = {
    test = "test";
    merge-request = "merge-request";
    deploy = "deploy";
    post-deploy = "post-deploy";
  };

  tags = {
    airs = "airs";
    common = "common";
    common-large = "common-large";
    common-x86 = "common-x86";
    common-x86-large = "common-x86-large";
    forces = "forces";
    integrates = "integrates";
    matches = "matches";
    melts = "melts";
    observes = "observes";
    retrieves = "retrieves";
    skims = "skims";
    sifts = "sifts";
    sorts = "sorts";
  };
}
