---
title: Introduction
description: Information about Forces, which manages the client-side aspect of the DevSecOps agent
slug: components/forces
---

import { Aside, FileTree, Steps } from '@astrojs/starlight/components';

Forces is the product responsible for the client-side part of the
[DevSecOps agent](https://help.fluidattacks.com/portal/en/kb/articles/view-details-of-the-security-of-your-builds).
It uses the [Integrates](/components/integrates/) API to communicate with
the back-end.

## Contributing

Please read the [contributing](/getting-started/contributing) page first.

## Internals

1. Forces is written in Python, with the
   [rich](https://github.com/Textualize/rich) library providing colors, tables
   and formatting. An image is built from the code with
   [makes](https://github.com/fluidattacks/makes) which is then deployed to
   the Fluid Attacks [DockerHub](https://hub.docker.com/r/fluidattacks/forces)
   page.

<Aside type="tip">
  You can try out your own image of Forces by running the following command
  after the `deployContainerManifest/forcesDev` job in the pipeline is done:

  ```sh
  docker pull fluidattacks/forces:<youruseratfluid> &&
  docker run --rm fluidattacks/forces:<youruseratfluid> forces
  ```
</Aside>

<Aside>
  Forces sends some telemetry to [Bugsnag](https://www.bugsnag.com/),
  where developers can use it to improve exception handling
  and enhance the program's logic to prevent unexpected crashes. Remember,
  stability is paramount!
</Aside>

## Product structure

<FileTree>
  - src
    - apis
      - integrates
        - api.py GraphQL operations made to the Integrates API are found here
        - client.py GraphQL client, timeout and retry parameters are defined
          here
    - cli
      - **\_\_init\_\_.py** Defines the CLI flags and help information
    - model/ NamedTuple class definitions for data and the reports
    - report/ Data parsers, validations and report formatting
    - utils/ General utils including the strict mode evaluation
    - **\_\_init\_\_.py** Application entrypoint
  - test/ Unit and functional tests
</FileTree>

## Getting Started

<Steps>
  1. Configure your [Development Environment](/getting-started/environment).

  1. When prompted for an AWS role, choose `dev`, and when prompted for a
     Development Environment, pick `forces`.

  1. Run this command within the `universe` repository:

     ```sh
     m . /forces
     ```

     This will build and run the Forces CLI application, including the
     changes you've made to the source code. Most of the time you'll be
     running Forces this way.
</Steps>

<Aside>
  You will need to pass the Forces token to the CLI, which can be generated
  locally and from ephemeral environments.
</Aside>

### Linting and testing

To lint and format tests and source code,
run the following command with makes:

```sh
m . /forces/lint
```

To run the tests, use:

```sh
m . /forces/test
```

<Aside type="tip">
  After running the test job, you can sometimes end up with an orphaned
  Integrates instance. To stop it, run the following command:

  ```sh
  m . /integrates/utils/kill
  ```
</Aside>

## Building the container locally

Most of the time you'll be running Forces via makes. But in case you want to
manually build and check the container, follow these steps:

<Steps>
  1. Build the container image (a tarball)

     ```sh
     m . /forces/container
     ```

  1. Load the tarball into the local Docker repository

     ```sh
     docker load < ~/.cache/makes/out-forces-container
     ```

  1. Run the container image

     ```sh
     docker run --rm container-image:latest forces
     ```
</Steps>

## Checking the output

Forces can be pointed to your local or ephemeral Integrates instances, to do
so, set the `API_ENDPOINT` variable:

- Local environment:

  ```sh
    export API_ENDPOINT=http://localhost:8001/api
  ```

- Ephemeral environment:

  ```sh
    export API_ENDPOINT=http://<youruseratfluid>.app.fluidattacks.com/api
  ```

The output can be seen in the
[DevSecOps table](https://help.fluidattacks.com/portal/en/kb/articles/view-details-of-the-security-of-your-builds#DevSecOps_table)
of the group in the instance you pointed Forces to.

## See also

- [httpx AsyncClient](https://www.python-httpx.org/api/#asyncclient) used as
as transport by [gql](https://gql.readthedocs.io/en/stable/) to communicate
with the Integrates API.
