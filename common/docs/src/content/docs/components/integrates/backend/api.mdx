---
title: API
description: Overview of Integrates API
---

import { FileTree } from '@astrojs/starlight/components';

Integrates implements a [GraphQL][graphql] API
that allows our products and customer-build external integrations
to query data and perform mutations exposed in its schema.

All queries are directed to a
single endpoint,
which is exposed at [/api](https://app.fluidattacks.com/api).

## Structure

The API is built using a [schema-first][ariadne-schema] approach
enabled by the [Ariadne][ariadne] library,
structured as follows:

<FileTree>
  - api
    - enums
      - \_\_init\_\_.py [Python mappings][ariadne-enums]
      - schema.graphql [Enum types][graphql-enums]
    - explorer
      - \_\_init\_\_.py [Explorer config][ariadne-explorer]
    - inputs
      - schema.graphql [Input types][graphql-input]
    - interfaces
      - schema.graphql [Interface types][graphql-interface]
    - mutations
      - mutation\_name.py [Python implementation][ariadne-mutation]
      - schema.py [Python bindings][ariadne-resolver-bindings]
      - schema.graphql [Mutation type][graphql-mutation]
      - payloads
        - payload\_name.py [Python implementation][ariadne-payload]
        - schema.graphql [Mutation return types][graphql-object]
    - resolvers
      - \_\_init\_\_.py [Python bindings][ariadne-bindings]
      - object\_name
        - field\_name.py [Python implementation][ariadne-resolver]
        - schema.py [Python bindings][ariadne-resolver-bindings]
        - schema.graphql [Object type][graphql-object]
      - query
        - schema.graphql [Query type][graphql-query]
    - scalars
      - \_\_init\_\_.py [Python bindings][ariadne-bindings]
      - scalar\_name.py [Python implementation][ariadne-scalar]
      - scalars.graphql [Scalar types][graphql-scalar]
    - subscriptions
      - subscription\_name.py [Subscription implementation][ariadne-subs-write]
      - schema.py [Python bindings][ariadne-resolver-bindings]
      - schema.graphql [Subscription type][ariadne-subs-define]
    - unions
      - \_\_init\_\_.py [Python bindings][ariadne-bindings]
      - union\_name.py  [Python implementation][ariadne-union]
      - schema.graphql [Union types][graphql-union]
    - validations
      - validation\_name.py [Validation implementation][ariadne-validation]
</FileTree>

## Explorer

The API provides a web-based GUI,
that allows performing queries
and exploring schema definitions
in a graphic and interactive way.
You can access it on:

- [https://app.fluidattacks.com/api](https://app.fluidattacks.com/api),
  which is production.
- `https://<branch>.app.fluidattacks.com/api`,
  which are the ephemeral environments
  (where `<branch>` is the name of your Git branch).
- [https://localhost:8001/api](https://localhost:8001/api),
  which is local (useful for development).

### Types

Integrates GraphQL types can be found on Documentation Explorer section at the
[Fluid Attacks API Playground](https://app.fluidattacks.com/api) or you can
[go to the source code](https://gitlab.com/fluidattacks/universe/-/tree/trunk/integrates/back/integrates/api/resolvers).

There are two approaches
to defining a `GraphQL` schema:

1. Code-first
1. Schema-first

We use the latter,
which implies defining the structure using `GraphQL SDL`
(Schema definition language)
and binding it to python functions.

e.g:

```graphql
# api/resolvers/stakeholder/schema.graphql

type Stakeholder {
  "Stakeholder email"
  email: String!
}
```

```py
# api/resolvers/stakeholder/schema.py

from ariadne import (
  ObjectType,
)

STAKEHOLDER = ObjectType('Stakeholder')
```

:::tip
Further reading:

- [GraphQL docs - Schemas and Types](https://graphql.org/learn/schema/)
- [Mirumee blog - Schema-First GraphQL: The Road Less Travelled](https://blog.mirumee.com/schema-first-graphql-the-road-less-travelled-cf0e50d5ccff)
  :::

### Enums

Integrates GraphQL enums can be found on Documentation Explorer section at the
[Fluid Attacks API Playground](https://app.fluidattacks.com/api) or you can
[go to the source code](https://gitlab.com/fluidattacks/universe/-/tree/trunk/integrates/back/integrates/api/enums).

```graphql
# api/enums/enums.graphql

enum AuthProvider {
  "Bitbucket auth"
  BITBUCKET
  "Google auth"
  GOOGLE
  "Microsoft auth"
  MICROSOFT
}
```

:::note
By default,
enum values passed to resolver functions
will match their name
:::

To map the value to something else,
you can specify it
in the enums binding index,
e.g:

```py
# api/enums/__init__.py
from ariadne import EnumType

ENUMS: Tuple[EnumType, ...] = (
    ...,
    EnumType(
        'AuthProvider',
        {
            'BITBUCKET': 'bitbucket-oauth2',
            'GOOGLE': 'google-oauth2',
            'MICROSOFT': 'azuread-tenant-oauth2'
        }
    ),
    ...
)
```

### Scalars

Integrates GraphQL scalars can be found on Documentation Explorer section at the
[Fluid Attacks API Playground](https://app.fluidattacks.com/api) or you can
[go to the source code](https://gitlab.com/fluidattacks/universe/-/tree/trunk/integrates/back/integrates/api/scalars).

GraphQL provides some primitive scalars,
such as String,
Int and Boolean,
but in some cases,
it is required to define custom ones
that aren't included by default
due to not (yet) being
part of the spec,
like Datetime, JSON and Upload.

Further reading:

- [Ariadne docs - Custom scalars](https://ariadnegraphql.org/docs/scalars)

### Resolvers

Integrates GraphQL resolvers can be found on GraphiQL Explorer section at the
[Fluid Attacks API Playground](https://app.fluidattacks.com/api) or you can
[go to the source code](https://gitlab.com/fluidattacks/universe/-/tree/trunk/integrates/back/integrates/api/resolvers).

A resolver is a function
that receives two arguments:

- **Parent:**
  The value returned by the parent resolver,
  usually a dictionary.
  If it's a root resolver
  this argument will be None
- **Info:**
  An object whose attributes
  provide details about the execution AST
  and the HTTP request.

It will also receive keyword arguments
if the GraphQL field defines any.

```graphql
# api/resolvers/stakeholder/schema.graphql

type Stakeholder {
  ...
  "User email"
  email: String!
  ...
}
```

```py
# api/resolvers/stakeholder/email.py

from graphql.type.definition import (
  GraphQLResolveInfo
)

from typing import (
  TypedDict,
  Unpack,
)

class ResolverArgs(TypedDict):
  email: str

@STAKEHOLDER.field("email")
def resolve(parent: Item, info: GraphQLResolveInfo, **kwargs: Unpack[ResolverArgs]):
   return 'test@fluidattacks.com'
```

The function must return a value
whose structure matches the type
defined in the GraphQL schema

:::caution
Avoid reusing the resolver function.
Other than the binding,
it should never be called
in other parts of the code
:::

:::tip
Further reading:

- [Ariadne docs - resolvers](https://ariadnegraphql.org/docs/resolvers)
  :::

### Mutations

Integrates GraphQL mutations can be found on GraphiQL Explorer section at the
[Fluid Attacks API Playground](https://app.fluidattacks.com/api) or you can
[go to the source code](https://gitlab.com/fluidattacks/universe/-/tree/trunk/integrates/back/integrates/api/mutations).

Mutations are a kind of GraphQL operation
explicitly meant to change data.

:::note
Mutations are also resolvers,
just named differently
for the sake of separating concerns,
and just like a resolver function,
they receive the parent argument
(always None),
the info object and their defined arguments.
:::

Most mutations only return `{'success': bool}`
also known as `SimplePayload`,
but they aren't limited to that.
If you need your mutation to return other data,
just look for it or define a new type in
`api/mutations/payloads/schema.graphql` and use it.

```graphql
# api/mutations/schema.graphql

type Mutation {
  ...
  "Adds a new Stakeholder"
  addStakeholder(
    "Stakeholder email"
    email: String!
    "stakeholder role"
    role: StakeholderRole!
  ): AddStakeholderPayload!
  ...
}
```

```py
# api/mutations/add_stakeholder.py

from graphql.type.definition import (
  GraphQLResolveInfo
)

from typing import (
  TypedDict,
  Unpack,
)

class AddStakeholderArgs(TypedDict):
  email: str
  role: str

@MUTATION.field("addStakeholder")
async def mutate(
  _parent: None,
  info: GraphQLResolveInfo,
  **kwargs: Unpack[AddStakeholderArgs],
):
  user_domain.create(
    kwargs["email"],
    kwargs["role"],
  )
  return AddStakeholderPayload(success=True)
```

### Subscriptions

Integrates GraphQL subscriptions
can be found on GraphiQL Explorer section at the
[Fluid Attacks API Playground](https://app.fluidattacks.com/api) or you can
[go to the source code](https://gitlab.com/fluidattacks/universe/-/tree/trunk/integrates/back/integrates/api/subscriptions).

Subscriptions are long-lasting operations
designed to provide real-time data updates
through bidirectional WebSocket communication.
They are commonly used to query AI models
and suggest solutions for identified risks.

They can maintain an active connection to your GraphQL server.
Typical resolvers are used to start a connection.

:::note
These subscriptions consist of two
key components: the generator,
which progressively provides
values as they become available,
and the resolver,
which processes these values,
potentially returning them unchanged
or with modifications as needed.
:::

```graphql
# api/subscriptions/schema.graphql

type Subscription {
  ...
  "Suggested fix for the vulnerability"
  getCustomFix(
    "The id off the vulnerability"
    vulnerabilityId: String!
    ): String!
  ...
}
```

```py
# api/subscriptions/get_custom_fix.py

from graphql.type.definition import (
  GraphQLResolveInfo
)

from typing import (
  AsyncGenerator,
  cast,
)

@SUBSCRIPTION.source("getCustomFix")
def generator(_parent: None, info: GraphQLResolveInfo, AsyncGenerator[str, None]):
  return "test"

@SUBSCRIPTION.field("getCustomFix")
def resolve(count: str, _info: GraphQLResolveInfo, **_kwargs: str):
  return count
```

:::tip
Further reading:

- [Ariadne docs - subscriptions](https://ariadnegraphql.org/docs/subscriptions)
  :::

### Errors

All exceptions raised,
will be reported
in the "errors" field of the response.

Raising exceptions can be useful
to enforce business rules
and report back to the client
in cases the operation
could not be completed successfully.

Further reading:

- [https://spec.graphql.org/June2018/#sec-Errors](https://spec.graphql.org/June2018/#sec-Errors)

## Authentication

The Integrates API enforces authentication
by checking for the presence
and validity of a JWT
in the request cookies or headers.

For resolvers or mutations
that require authenticated users,
decorate the function with the
`@require_login` from `decorators`.

## Authorization

The Integrates API enforces authorization
implementing an ABAC model with a simple grouping
for defining roles. You can find
[the model here](https://gitlab.com/fluidattacks/universe/-/tree/trunk/integrates/back/integrates/authz).

### Levels and roles

An user can have one role for each one
of the three levels of authorization:

- User
- Organization
- Group

Each role is associated with [a set of permissions](https://gitlab.com/fluidattacks/universe/-/tree/trunk/integrates/back/integrates/authz/model/roles).

Also, Service level exists and it checks
the covered features according to group plan
like [Advanced or Essential](https://fluidattacks.com/plans/).

### Enforcer

An enforcer is an authorization function that
checks if the user can perform
an action on the context.

We define
[enforcers for each authorization level](https://gitlab.com/fluidattacks/universe/-/blob/trunk/integrates/back/integrates/authz/enforcer.py).
Read the description for understanding
how to use them.

### Boundary

The general methods for listing and
getting the user permissions (and
the permissions that user can grant) are
in [boundary](https://gitlab.com/fluidattacks/universe/-/blob/trunk/integrates/back/integrates/authz/boundary.py).

The whole application must use this
methods for implementing controls.

### Policy

The general methods for get user role,
grant permissions or revoke them, are in
[policy](https://gitlab.com/fluidattacks/universe/-/blob/trunk/integrates/back/integrates/authz/policy.py).

### Decorators

For resolvers or mutations
that require authorized users,
decorate the function
with the appropriate decorator
from `decorators`

- @enforce\_user\_level\_auth\_async
- @enforce\_organization\_level\_auth\_async
- @enforce\_group\_level\_auth\_async

## Guides

### Adding new fields or mutations

1. Declare the field or mutation in the schema using SDL
1. Write the resolver or mutation function
1. Bind the resolver or mutation function to the schema

### Editing and removing fields or mutations

When dealing with fields or mutations
that are already in use by clients,
it's crucial to ensure backward compatibility
to prevent breaking changes.
To achieve this,
we implement a deprecation policy,
providing users with advance notice
of any planned edits or removals.

This involves informing API users
about which fields or mutations will be
edited/deleted in the future,
granting them adequate time to adapt
to this changes.

We use field and mutation deprecation for this.
Our current policy mandates removal
**6 months** after marking fields and mutations
as deprecated.

#### Deprecating fields

To mark fields or mutations as deprecated,
use the
[`@deprecated` directive](https://spec.graphql.org/June2018/#sec-Field-Deprecation),
e.g:

```graphql
type ExampleType {
  oldField: String @deprecated(reason: "reason text")
}
```

The reason should follow
something similar to:

```md
This {field|mutation} is deprecated and will be removed after {date}.
```

If it was replaced or there is an alternative,
it should include:

```md
Use the {alternative} {field|mutation} instead.
```

Dates follow the `AAAA/MM/DD` convention.

Additionally,
we offer the option to assume the risk
of using deprecated fields or
mutations by including a flag in the
commit message.
This allows developers to make informed
decision when incorporating changes
that may affect their implementations.

#### Removing fields or mutations

When deprecating fields or mutations for removal,
these are the common steps to follow:

1. Mark the field or mutation as deprecated.
1. Wait six months
   so clients have a considerable window
   to stop using the field or mutation.
1. Delete the field or mutation.

e.g:

Let's remove the `color` field from type `Car`:

1. Mark the `color` field as deprecated:

   ```graphql
   type Car {
     color: String
       @deprecated(
         reason: "This field is deprecated and will be removed after 2022/11/13."
       )
   }
   ```

1. Wait until one day after given deprecation date and Remove the field:

   ```graphql
   type Car {}
   ```

#### Editing fields or mutations

When renaming fields, mutations or already-existing types within the API,
these are the common steps to follow:

1. Mark the field or mutation you want to rename as deprecated.
1. Add a new field or mutation using the new name you want.
1. Wait until one day after given deprecation date.
1. Remove the field or mutation that was marked as deprecated.

e.g:

Let's make the `color` field from type `Car`
to return a `Color` instead of a `String`:

1. create a `newColor` field that returns the `Color` type:

   ```graphql
   type Car {
     color: String
     newColor: Color
   }
   ```

1. Mark the `color` field as deprecated and set `newColor` as the alternative:

   ```graphql
   type Car {
     color: String
       @deprecated(
         reason: "This field is deprecated and will be removed after 2022/11/13. Use the newColor field instead."
       )
     newColor: Color
   }
   ```

1. Wait until one day after given deprecation date and remove the `color` field:

   ```graphql
   type Car {
     newColor: Color
   }
   ```

1. Add a new `color` field that uses the `Color` type:

   ```graphql
   type Car {
     color: Color
     newColor: Color
   }
   ```

1. Mark the `newColor` field as deprecated and set `color` as the alternative:

   ```graphql
   type Car {
     color: Color
     newColor: Color
       @deprecated(
         reason: "This field is deprecated and will be removed after 2022/11/13. Use the color field instead."
       )
   }
   ```

1. Wait until one day
   after given deprecation date
   and remove the `newColor` field:

   ```graphql
   type Car {
     color: Color
   }
   ```

:::note
These steps may change
depending on what you want to do,
just keep in mind that
keeping backwards compatibility
is what really matters.
:::

[ariadne]: https://ariadnegraphql.org/

[ariadne-bindings]: https://ariadnegraphql.org/docs/intro#making-executable-schema

[ariadne-enums]: https://ariadnegraphql.org/docs/enums

[ariadne-explorer]: https://ariadnegraphql.org/docs/explorers

[ariadne-mutation]: https://ariadnegraphql.org/docs/mutations#writing-resolvers

[ariadne-payload]: https://ariadnegraphql.org/docs/mutations#mutation-results

[ariadne-resolver]: https://ariadnegraphql.org/docs/resolvers

[ariadne-resolver-bindings]: https://ariadnegraphql.org/docs/resolvers#binding-resolvers

[ariadne-scalar]: https://ariadnegraphql.org/docs/scalars

[ariadne-schema]: https://mirumee.com/blog/schema-first-graphql-the-road-less-travelled

[ariadne-subs-define]: https://ariadnegraphql.org/docs/subscriptions#defining-subscriptions

[ariadne-subs-write]: https://ariadnegraphql.org/docs/subscriptions#writing-subscriptions

[ariadne-union]: https://ariadnegraphql.org/docs/unions

[ariadne-validation]: https://ariadnegraphql.org/docs/query-validators#implementing-custom-validator

[graphql]: https://graphql.org/

[graphql-enums]: https://graphql.org/learn/schema/#enumeration-types

[graphql-input]: https://graphql.org/learn/schema/#input-types

[graphql-interface]: https://graphql.org/learn/schema/#interfaces

[graphql-mutation]: https://graphql.org/learn/queries/#mutations

[graphql-object]: https://graphql.org/learn/schema/#object-types-and-fields

[graphql-query]: https://graphql.org/learn/schema/#the-query-and-mutation-types

[graphql-scalar]: https://graphql.org/learn/schema/#scalar-types

[graphql-union]: https://graphql.org/learn/schema/#union-types
