---
title: Introduction
description: Introduction to the Backend of Integrates
slug: components/integrates/backend
---

[![codecov](https://codecov.io/gl/fluidattacks/universe/branch/integrates/graph/badge.svg?token=V8SMWFOMG0)](https://codecov.io/gl/fluidattacks/universe)

## Introduction

Integrates' backend is an HTTP web service
written in [Python][py],
served with [Hypercorn][hypercorn]
and built with [Starlette][starlette].

## Principles

- **Functional:**
  The codebase favors functions over classes
  and avoids direct mutations.
- **No need to reinvent the wheel:**
  Leveraging third-party packages when available.

## Getting started

To view changes reflected as you edit the code, you can run:

```bash
  m . /integrates
```

As explained in the [introduction](/components/integrates/#local-environment)
section, this command launches a replica of the platform
using the test data defined in `common/schemas/database-design.json`. Inside
the `schemas/tables` directory you find all the data launched by default.
This is useful as it allows you to modify anything you wish to test,
for example:

```json
{
  enrolled:              true
  is_concurrent_session: false
  is_registered:         true
  role:                  "admin"
  last_name:             "Manager"
  last_login_date:       "2020-12-31T16:50:17+00:00"
  ...
  legal_remember:    true
  registration_date: "2018-02-28T16:54:12+00:00"
  sk:                "USER#__adminEmail__"
  pk_2:              "USER#all"
  pk:                "USER#__adminEmail__"
  first_name:        "Integrates"
  email:             "__adminEmail__"
  sk_2:              "USER#__adminEmail__"
},
```

This portion of the code that is in the `stakeholder_state.cue` file defines
your local user, with this you can modify the role, registration
status, and all metadata information.
Remember, after doing any modifications to the local database
you need to run the following:

```bash
  m . /common/schemas/export
```

And update your local environment so your changes can be reflected.

More on this can be found in the
[database design](/components/integrates/database/database-design) section.

## Linting

The back uses [Prospector][prospector]
and [Mypy][mypy]
to enforce compliance with a defined coding style.

To lint and format your code, run:

```bash
  m . /integrates/back/lint
```

## Testing

The back-end uses [pytest][pytest] as a test runner.

To execute unit tests on your computer, run:

```bash
  m . /integrates/back/test/unit 'changes_db'
```

```bash
  m . /integrates/back/test/unit 'not_changes_db'
```

To execute a functional test on your computer, run:

```bash
  m . /integrates/back/test/functional 'name of the test'
```

## Web server

The back-end is served by [Hypercorn][hypercorn],
a program with two sides:

- 🌐 On one side it speaks the HTTP protocol,
  receiving requests and sending responses.
- 🐍 On the other it speaks Python ([ASGI][asgi]),
  passing request data to Python code for processing.

This lower-level is challenging to build
an application directly on top of, which is where
[Starlette][starlette] comes in handy.

[Starlette][starlette] abstracts requests handling,
and provides utilities for building web applications,
such as declaring routes, middlewares, and managing
sessions and cookies.

## Modules

You can find them in the `back/integrates` directory.

### api

This module implements a [GraphQL][gql] API.
You can find more details about it [here](/components/integrates/backend/api).

### db_model

Declares the structure of each entity used in the application
and interacts with database-specific modules
to read from and write to a data store.

It provides functions to perform [CRUD][crud] operations for each entity,
taking care of batching and caching using [dataloaders][loaders].
Designed to be agnostic for easy swapping of the underlying data store.

This module is also responsible for controlling concurrent writes,
using strategies like [optimistic locking][occ].

[occ]: https://en.wikipedia.org/wiki/Optimistic_concurrency_control

[py]: https://www.python.org/

[hypercorn]: https://hypercorn.readthedocs.io/

[starlette]: https://www.starlette.io/

[prospector]: https://prospector.landscape.io/

[mypy]: https://mypy-lang.org/

[pytest]: https://docs.pytest.org/

[asgi]: https://asgi.readthedocs.io/

[gql]: https://graphql.org/

[crud]: https://en.wikipedia.org/wiki/Create,_read,_update_and_delete

[loaders]: https://github.com/graphql/dataloader
