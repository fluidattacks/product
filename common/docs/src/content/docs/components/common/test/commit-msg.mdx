---
title: Commit Message
description: Commit message syntax within Commitlint has specific rules and guidelines for creating standardized and meaningful commit messages
---

## Syntax

Valid commit messages
have the structure:

```txt
[product/sub-product]\[type]([scope]/): #[issue-number] [title] // This is the commit title
               // This blank line separates the commit title from the commit body
[body]         // This is the commit body. It CAN have multiple lines
```

- `**[variable]**` are **required** variables
  that must be replaced
  in a final commit message
  (`**[]**` symbols must be removed)
- **// Comment** are comments
  that must be removed
  in a final commit message

## Rules

The following rules must be met
for a commit message to be valid:

1. `**[product/sub-product]**` variable depends on the changes in your commit.

   The products are as follows:

   ```md
   all                  // Multiple components. Triggers CI for all products.
   airs                 // Main Fluid Attacks website [https://fluidattacks.com](https://fluidattacks.com). Triggers all jobs related to airs.
   common               // Shared infrastructure resources and utilities across all components. Triggers common-related jobs.
   forces               // Client-side DevSecOps agent. Triggers forces jobs.
   integrates           // Fluid Attacks platform [https://app.fluidattacks.com](https://app.fluidattacks.com). Triggers all integrates jobs.
   integrates-front     // Front-end application. Triggers front-end jobs.
   melts                // CLI for downloading client source code repositories.
   observes             // ETL suites for data centralization and analytics. Triggers all observes jobs.
   observes-code-etl    // Source code extraction and processing. Triggers code-etl jobs.
   observes-gitlab-etl  // GitLab data processing. Triggers gitlab-etl jobs.
   observes-infra       // Infrastructure-related ETL tasks. Triggers infra-etl jobs.
   retrieves            // VSCode plugin. Triggers retrieves jobs.
   skims                // Automatic vulnerability scanner. Triggers all skims jobs.
   skims-cspm           // lib_cspm module and its tests. Triggers cspm jobs.
   skims-sast           // lib_sast module and its tests. Triggers sast jobs.
   skims-dast           // lib_dast module and its tests. Triggers dast jobs.
   skims-sca            // lib_sca module and its tests. Triggers sca jobs.
   skims-apk            // lib_apk module and its tests. Triggers apk jobs.
   skims-ai             // skims-ai CLI. Triggers ai jobs.
   skims-sbom           // SBOM library. Triggers sbom jobs.
   sorts                // Sort files in a Git repository by their probability of containing security vulnerabilities. Triggers sorts jobs.
   ```

   By using a sub-product, you ensure the commit only triggers the pipeline for that specific area.

   If you are working on a file in the root of the repository,
   outside all of these folders, use `common`.

1. `**[type]**` variable has to be
   one of the following:

   ```md
   chore  // Operational and maintenance tasks (including rotate)
   feat   // New feature
   fix    // Bug fix (including reverts)
   refac  // Code improvements without changing functionality (including style)
   ```

1. `**[scope]**` variable has to be
   one of the following:

   ```md
   front  // Front-End change
   back   // Back-End change
   infra  // Infrastructure change
   build  // Build system, CI, compilers, etc (scons, webpack...)
   cross  // Mix of two or more scopes
   doc    // Documentation only changes
   ```

1. A **Commit title**
   must exist.

1. A **Commit title**
   must **not** contain
   the '**:**' character.

1. **Commit title**
   must have 60 characters
   or less.

1. **Commit title**
   must be lower case.

1. **Commit title**
   must not finish
   with a dot '**.**'.

1. **Commit title**
   must reference
   an issue.

1. **Commit title**
   must be meaningful.
   Avoid using things like
   `feat(build)[integrates]: #5 feature`.

1. A **blank line**
   between commit title
   and commit body
   must exist.

1. A **commit body**
   must exist.

1. Lines in **commit body**
   must have 72 characters
   or less.

### Possible combinations

Below is a table explaining
all the possible combinations
between types and scopes
for a commit message
(Types are columns, scopes are rows):

|           |                               **chore**                               |                           **feat**                           |                         **fix**                        |                    **refac**                   |     |
| :-------: | :-------------------------------------------------------------------: | :----------------------------------------------------------: | :----------------------------------------------------: | :--------------------------------------------: | :-: |
| **front** |       Perform operational or maintenance tasks in the front-end       |              Add a new feature to the front-end              |      Fix a bug or revert a change in the front-end     |        Improve or modify front-end code        |     |
|  **back** |        Perform operational or maintenance tasks in the back-end       |               Add a new feature to the back-end              |      Fix a bug or revert a change in the back-end      |         Improve or modify back-end code        |     |
| **infra** |     Perform operational or maintenance tasks in the infrastructure    |            Add a new feature to the infrastructure           |   Fix a bug or revert a change in the infrastructure   | Improve or modify infrastructure configuration |     |
| **build** |       Perform operational or maintenance tasks in building tools      | Add new feature to building tools or add a new building tool |   Fix a bug or revert a change in the building tools   |        Improve or modify building tools        |     |
| **cross** | Perform operational or maintenance tasks across multiple system parts |          Add a new feature affecting several scopes          | Fix a bug or revert a change affecting multiple scopes |  Improve or modify code across multiple scopes |     |
|  **doc**  |     Perform operational or maintenance tasks in the documentation     |                     Add new documentation                    |    Fix a bug or revert a change in the documentation   |       Improve or modify the documentation      |     |

Where:

- **chore:** maintenance tasks.
- **infra:** infrastructure changes.
- **build:** building tools.
- **cross:** multiple scopes.
- **doc:** documentation updates.
- **NA** is not applicable.

## Recommendations

- Try to itemize your commit body:

  ```text
  - Add feature X in file Y
  - Run script Z
  - Remove file A with B purpose
  ```

### ETA

When your Merge Request
is related to one area/issue
that has an enumerable universe,
i.e,
we know with considerable certainty
how many MRs are necessary
to complete it,
then you should use
the following ETA model
as a Merge Request message:

```txt
- Speed: A [parts] / B [time unit] = A/B [parts]/[time unit]
- TODO: C [parts]
- ETA: C / (A/B) = C/(A/B) [time unit]
```

`**[parts]**` should be replaced for
the aspect that allows to
quantify the progress of the area,
which can be a number of issues,
cases, files, tasks, etc.

`**[time unit]**` should be replaced for
an appropriate unit of time
that will be used to estimate an ETA,
for example days or weeks.

`**B**` is the units of time that has passed
since you started addressing
the issues of the area,
`**A**` is the total number of `**[parts]**`
that have been submitted in such `**B**` time
and `**C**` is the total number of `**[parts]**`
that we know will resolve the issues of the area.

ETA Merge Request message example:

```md
- Speed: 4 issues / 2 days = 2 issues/day
- TODO: 10 issues
- ETA: 10 / 2 = 5 days
```

## Example

Here is an example
of a compliant commit message:

```md
integrates\feat(build): #13 add type_check

- Add type_check function
- Remove unnecessary print_output function
```
