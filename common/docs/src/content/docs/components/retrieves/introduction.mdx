---
title: Introduction
description: Information about Retrieves, also known as Fluid Attacks' VSCode plugin
slug: components/retrieves
---

import { FileTree, Steps } from '@astrojs/starlight/components';

Retrieves is the product responsible
for the [VSCode plugin](https://help.fluidattacks.com/portal/en/kb/integrate-with-fluid-attacks/use-ide-extensions/vs-code-extension).
It communicates with the [Integrates](/components/integrates/)
API to fetch live vulnerability data and mark it in users' editors.

## Internals

1. Retrieves is written in Typescript with React used for the Webview section.
   [Webpack](https://webpack.js.org/) bundles the code deployed to
   the marketplace.

   When telemetry is enabled, debugging information is
   reported/logged to [Bugsnag](https://www.bugsnag.com/),
   where developers can use it to improve exception handling
   and enhance the program's logic to prevent crashes.
   It communicates with the
   [Fluid Attacks Platform](/components/integrates/) through the
   API to retrieve Vulnerability information and autofix data
   upon request.

1. The application is divided into two sections:

   - `Extension` (entrypoint: `src/extension.ts`): This is the main part of the
     application, which also consumes the Integrates API and interfaces
     with the `vscode` library to mark vulnerabilities and populate the sidebar.
     [Webpack](https://webpack.js.org/) bundles this section.
   - `Webview` (entrypoint: `src/webview/App.tsx`): This uses React and
     hosts the webviews (`Finding description`, `Suggested autofix`,
     `ToE Lines`, etc...). These webviews and the extension communicate
     through message handlers. For more information, refer to this
     [link](https://code.visualstudio.com/api/extension-guides/webview).
     Webpack also bundles this section.

## Product structure

<FileTree>
  - \_\_mocks\_\_/ Holds VSCode API mocks needed for unit tests
  - src
    - api/ GraphQL operation handlers
    - commands/ Extension commands, can be found in the Command Palette
    - diagnostics/ Code to underline locations
    - mocks/ Mock Service Worker handlers needed for unit tests
    - panels/ ToE Lines table code
    - providers/ Organizes and passes the info needed for the sidebar
    - test/ Functional test stub
    - treeItems/ Sidebar items, including buttons and actions that may call commands
    - utils/ General utils and validations
    - webview React components to show fix guides, git urls, finding descriptions and ToE lines
      - **App.tsx** Webview entrypoint
    - **extension.ts** Extension entrypoint, registers commands and runs validations
    - queries.ts GraphQL operations can be found here
  - webpack/ Webpack config
</FileTree>

### Tooling

Helpful tools for extension development:

- [tsl problem matcher](https://marketplace.visualstudio.com/items?itemName=amodio.tsl-problem-matcher)
- [Jest Runner](https://marketplace.visualstudio.com/items?itemName=firsttris.vscode-jest-runner)
- [ESLint](https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint)
- [Prettier](https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode)

### Getting started

<Steps>
  1. Configure your [Development Environment](/getting-started/environment).
     When prompted for an AWS role, choose `dev`, and when prompted for a
     Development Environment, pick any option or `None`. You will need to have
     [Node v21](https://nodejs.org/en/download/current) installed beforehand.

  1. Open the editor in the base folder `/universe/retrieves` and
     from a terminal in the same folder run:

     ```bash
     npm install
     ```

  1. To view the changes reflected as you edit the code:

     - For `extension`: Launch the Extension Development Host by pressing `F5`;
       this will bundle the code and launch Webpack in watch mode. Despite
       Webpack's hot reloading, some changes may require a manual reload of the
       Extension Host to be reflected.
     - For `webview`: Follow the steps above, and the new changes to the code
       will be immediately reflected thanks to
       [Webpack dev server's](https://webpack.js.org/configuration/dev-server/)
       hot reloading.
</Steps>

### Versioning

To publish your changes to the VSCode marketplace,
you must increment the version number in the `package.json` file
located in the Retrieves base folder.
Otherwise, the change will be merged to trunk, but the publication/deployment
process will be skipped.

For more info, check the semver [documentation](https://semver.org/).

## Linting

Retrieves uses [ESLint](https://eslint.org/) and
[Prettier](https://prettier.io/) to enforce compliance
with a defined coding style.

To view linting issues globally, you can run:

```bash
  npm run lint
```

or

```bash
  m . /retrieves lint
```

## Testing

### Unit tests

Retrieves uses [Jest](https://jestjs.io/) as a test runner and
[MSW](https://mswjs.io/) to mock API responses for unit tests.

To execute the test cases on your computer, you can run:

```bash
  npm run test
```

or

```bash
  m . /retrieves test
```

To execute test cases individually, you can install the
[Jest Runner extension](https://marketplace.visualstudio.com/items?itemName=firsttris.vscode-jest-runner)
and run or debug the tests from the UI.
