---
title: Introduction
description: Overview of Fluid Attacks, a cybersecurity company dedicated to making the world a safer place
slug: getting-started
---

import { Card, CardGrid } from "@astrojs/starlight/components";

## Who we are

We are the engineering team at [Fluid Attacks][fluidattacks-com].

### Beliefs

<CardGrid stagger>
  <Card title="Diversity" icon="heart">
    We are a diverse team of people
    from different backgrounds,
    cultures, and perspectives.
    We believe that diversity
    is the key to innovation.
  </Card>

  {" "}

  <Card title="Horizontal" icon="bars">
    We maintain a horizontal team where all members are developers that solve
    problems. We think this empowers everyone to make decisions and take ownership
    of the work we do.
  </Card>

  {" "}

  <Card title="Open Source" icon="open-book">
    We believe in the power of open source and we are committed to sharing our
    work with the world. We think that open source is the best way to build a more
    secure world.
  </Card>

  {" "}

  <Card title="Asynchronous" icon="random">
    We think in asynchronous work and make sure that everyone can do their work at
    their own pace, with the minimum synchronous dependencies. This allows us to
    be more productive and to have a better work-life balance.
  </Card>

  {" "}

  <Card title="Simplicity" icon="seti:pipeline">
    We believe in simplicity and strive to keep our code and our processes as
    simple as possible. We think that simplicity is the key to maintainability.
  </Card>

  <Card title="Trust" icon="approve-check-circle">
    We are committed to
    building trust with our customers
    and with the community
    at large by publishing our work
    and being transparent about our processes.
  </Card>
</CardGrid>

## What we do

Our main responsibilities are:

- Build and maintain
  all of Fluid Attacks' [components](/components).
- Look for state of the art
  technologies to improve our [stack](https://help.fluidattacks.com/portal/en/kb/stack).
- Optimize the development process
  of the company.
- Help design a seamless experience
  for Fluid Attacks'
  [Continuous Hacking solution](https://fluidattacks.com/services/continuous-hacking/).

Our source code
is versioned in the [Universe repository][universe]
and is divided across many components.
We also have a [GitHub account][github_fluidattacks]
where we publish projects
that are more oriented towards the community
and less coupled to our model of business.

This documentation focuses on the Universe repository.

<CardGrid stagger>
  <Card title="Code Quality" icon="setting">
    ---

    [![Maintainability Rating](https://sonarcloud.io/api/project_badges/measure?project=fluidattacks_universe\&metric=sqale_rating)](https://sonarcloud.io/summary/new_code?id=fluidattacks_universe)

    ---

    [![Technical Debt](https://sonarcloud.io/api/project_badges/measure?project=fluidattacks_universe\&metric=sqale_index)](https://sonarcloud.io/summary/new_code?id=fluidattacks_universe)

    ---

    [![codecov](https://codecov.io/gl/fluidattacks/universe/branch/integrates/graph/badge.svg?token=V8SMWFOMG0)](https://codecov.io/gl/fluidattacks/universe)

    ---

    [![Total lines](https://sloc.xyz/gitlab/fluidattacks/universe/?category=lines)](https://gitlab.com/fluidattacks/universe/)

    ---

    [![Code Style](https://img.shields.io/badge/code%20style-functional%20-mediumpurple)](/getting-started/philosophy/)

    ---

    [![Languages Count Badge](https://img.shields.io/gitlab/languages/count/fluidattacks%2Funiverse)](https://gitlab.com/fluidattacks/universe/-/graphs/trunk/charts)
  </Card>

  <Card title="DevOps" icon="gitlab">
    ---

    [![Last 30 days deployments](https://img.shields.io/badge/dynamic/json?url=https%3A%2F%2Fs3.amazonaws.com%2Ffluidattacks.public.storage%2Fdora_metrics%2Fdora_metrics.json\&query=%24.record.total_deploys\&logo=gitlab\&label=Last%2030%20days%20deployments\&color=%23fc6d26)](https://gitlab.com/fluidattacks/universe)

    ---

    [![Last 24 hours deployments](https://img.shields.io/badge/dynamic/json?url=https%3A%2F%2Fs3.amazonaws.com%2Ffluidattacks.public.storage%2Fdora_metrics%2Fdora_metrics.json\&query=%24.record.last_day_deploys\&logo=gitlab\&label=Last%2024%20hours%20deployments\&color=%23fc6d26)](https://gitlab.com/fluidattacks/universe)

    ---

    [![Daily Deployment Frequency](https://img.shields.io/badge/dynamic/json?url=https%3A%2F%2Fs3.amazonaws.com%2Ffluidattacks.public.storage%2Fdora_metrics%2Fdora_metrics.json\&query=%24.record.daily_deployment_frequency\&style=flat\&logo=gitlab\&label=Daily%20Deployment%20Frequency\&color=%23fc6d26)](https://gitlab.com/fluidattacks/universe)

    ---

    [![Change Failure Rate](https://img.shields.io/badge/dynamic/json?url=https%3A%2F%2Fs3.amazonaws.com%2Ffluidattacks.public.storage%2Fdora_metrics%2Fdora_metrics.json\&query=%24.record.CFR\&style=flat\&logo=gitlab\&label=Change%20Failure%20Rate\&color=%23fc6d26)](https://gitlab.com/fluidattacks/universe)

    ---

    [![Mean Time to Repair](https://img.shields.io/badge/dynamic/json?url=https%3A%2F%2Fs3.amazonaws.com%2Ffluidattacks.public.storage%2Fdora_metrics%2Fdora_metrics.json\&query=%24.record.MTR\&style=flat\&logo=gitlab\&label=Mean%20Time%20to%20Repair\&color=%23fc6d26)](https://gitlab.com/fluidattacks/universe)

    ---

    [![Avg deployment time](https://img.shields.io/badge/dynamic/json?url=https%3A%2F%2Fs3.amazonaws.com%2Ffluidattacks.public.storage%2Fdora_metrics%2Fdora_metrics.json\&query=%24.record.average_deployment_time\&style=flat\&logo=gitlab\&label=Avg%20deployment%20time\&color=fc6d26)](https://gitlab.com/fluidattacks/universe)
  </Card>

  <Card title="Security" icon="seti:ignored">
    ---

    [![OpenSSF Best Practices](https://bestpractices.coreinfrastructure.org/projects/6313/badge)](https://bestpractices.coreinfrastructure.org/projects/6313)

    ---

    [![Security Rating](https://sonarcloud.io/api/project_badges/measure?project=fluidattacks_universe\&metric=security_rating)](https://sonarcloud.io/summary/new_code?id=fluidattacks_universe)

    ---

    [![License: MPL 2.0](https://img.shields.io/badge/License-MPL_2.0-brightgreen.svg)](https://www.mozilla.org/en-US/MPL/2.0/)
  </Card>
</CardGrid>

## Our components

| Component                             | Description                                                                                                                                                                         |                                                  Software Billing Of Materials (SBOM) |
| :------------------------------------ | :---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------: |
| [Airs](/components/airs/)             | Home page, live at [fluidattacks.com][fluidattacks-com]                                                                                                                             |       [Link](https://s3.amazonaws.com/fluidattacks.public.storage/sbom/airs_sbom.zip) |
| [Common](/components/common/)         | Owner of critical, or company-wide infrastructure and resources                                                                                                                     |     [Link](https://s3.amazonaws.com/fluidattacks.public.storage/sbom/common_sbom.zip) |
| [Integrates](/components/integrates/) | The platform that orchestrates Fluid Attacks' core services and its front-ends: [Web][web], [API][api], [VSCode Extension][vscode-extension] and [DevSecOps Agent][devsecops-agent] | [Link](https://s3.amazonaws.com/fluidattacks.public.storage/sbom/integrates_sbom.zip) |
| [Skims](/components/skims/)           | Security Vulnerability Scanner                                                                                                                                                      |      [Link](https://s3.amazonaws.com/fluidattacks.public.storage/sbom/skims_sbom.zip) |
| [Observes](/components/observes/)     | Company-wide data analytics                                                                                                                                                         |   [Link](https://s3.amazonaws.com/fluidattacks.public.storage/sbom/observes_sbom.zip) |

## Our users

Below you will find
our most typical users.

### End users

They don't contribute code,
but instead just interact with our components.

They are usually:

- **Security Analysts of Fluid Attacks**:
  They usually use Skims, Sorts and the platform (Integrates), among others.
- **Customers of Fluid Attacks**:
  They usually use the platform (Integrates),
  the DevSecOps Agent (Sorts),
  read our blog (Airs) and
  [public documentation](https://help.fluidattacks.com/portal/en/kb).
- **Community users**:
  They usually use tools like Skims in its Free and Open Source plan.

### Developers

Fluid Attacks' developers
that contribute source code to [Universe][universe].
They also sometimes contribute
to our [projects on GitHub][github_fluidattacks].

[universe]: https://gitlab.com/fluidattacks/universe

[github_fluidattacks]: https://github.com/fluidattacks

[web]: https://app.fluidattacks.com/

[api]: https://app.fluidattacks.com/api

[vscode-extension]: https://marketplace.visualstudio.com/items?itemName=FluidAttacks.fluidattacks

[devsecops-agent]: /components/forces/

[fluidattacks-com]: https://fluidattacks.com/
