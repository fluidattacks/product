---
title: Set up your environment
description: Guide on how to set up your development environment
---

import { Steps } from '@astrojs/starlight/components';

This guide will help you set up a working development environment
on your work machine.

## Machine

Below you will find all the required steps in order
to configure the most basic machine setup.

<Steps>
   1. Install VSCode, Time Doctor and Google Chrome
      via the Kernel Self Service application.

  1. [Install Nix](https://nixos.org/download/)

     :::note
     Make sure you use the recommended multi-user installation
     :::

     :::caution
     In case you get errors while installing
     and get stuck,
     make sure you follow
     the [uninstalling guide](https://nix.dev/manual/nix/2.18/installation/uninstall#multi-user)
     and then try again.
     :::

  1. Install basic tools for on-editor comfort:

     ```sh
     nix-env -iA \
       nixpkgs.nixfmt \
       nixpkgs.terraform \
       nixpkgs.nil \
       nixpkgs.git \
       nixpkgs.direnv \
       nixpkgs.sops \
       nixpkgs.saml2aws
     ```

  1. [Install Makes](https://makes.fluidattacks.tech/getting-started/#installation).

  1. [Create a GitLab account](https://gitlab.com/users/sign_up).

     :::tip
     The username must follow the syntax usernameatfluid,
     where the username is the one before @fluidattacks in your credentials.
     :::

  1. [Create a SSH key](https://docs.gitlab.com/ee/user/ssh.html).

  1. Add your SSH key to your [GitLab account](https://gitlab.com/-/profile/keys).

  1. Configure Git to [sign commits using SSH](https://docs.gitlab.com/ee/user/project/repository/signed_commits/ssh.html).

     :::tip
     Make sure you run the following command
     to automatically sign your commits:

     ```sh
     git config --global commit.gpgsign true
     ```

     :::

  1. Write an email to [help@fluidattacks.com](mailto:help@fluidattacks.com)
     requesting developer access
     to the [Universe Repository](https://gitlab.com/fluidattacks/universe).
</Steps>

## Environment​

This section will guide you
through setting up a terminal
with AWS credentials and a code editor
in an automated way
every time you enter
your local checkout
of the Universe repository.

After this section you will have:

- A terminal with AWS credentials for the specific product you are developing.
- An editor with:
  - The source code of the Universe repository.
  - Recommended development extensions.
  - Automatic code formatters on save.
  - Auto completion and go to definition.
  - OS libraries required.

### Terminal​

We'll configure the terminal first.
Once the terminal is configured,
all of the applications you open from it
will inherit the development environment and credentials.

At this point you should have Nix and Makes already installed in your system,
so we won't go into those details.

For maximum compatibility,
we suggest you use `ZSH` (macOS default shell)
as the command interpreter of your terminal.

Please follow these steps:

<Steps>
  1. Make sure you have the following tools installed in your system:

     1. Nix
     1. Makes
     1. Git
     1. Direnv
     1. VSCode
     1. If you are missing any of the previous tools,
        refer back to the [machine configuration](#machine)

  1. Clone the Universe repository using SSH:

     ```sh
     git clone git@gitlab.com:fluidattacks/universe.git
     ```

  1. `cd` to the root of the repository
     and configure your Git username and email:

     ```sh
     git config user.name "Your Name"
     git config user.email "username@fluidattacks.com"
     ```

     :::tip

     - The username must only contain
       the first name started in capitals
       and the last name started in capitals.
     - Make sure you use your corporate email.

     Example:

     ```sh
     git config user.name "Aureliano Buendia"
     git config user.email "abuendia@fluidattacks.com"
     ```

     :::

  1. Add the following file in `universe/.envrc.config`:

     ```sh
     # .envrc.config
     export OKTA_EMAIL=<username>@fluidattacks.com
     export OKTA_PASS=<your-password>
     ```

  1. Configure `direnv`
     by adding the following lines
     to your `~/.zshrc` shell configuration file:

     ```sh
     # Configure direnv
     export DIRENV_WARN_TIMEOUT=1h
     source <(direnv hook zsh)
     ```

     After making these changes, apply them with:

     ```sh
     source ~/.zshrc
     ```

  1. `cd` to universe and run the following commands:

     ```sh
     cd universe
     direnv allow
     ```

     This will start the login process on your terminal.
</Steps>

### Editor​

We highly recommend you use Visual Studio Code
because most of the team uses it,
and it works very well for our purpose.

<Steps>
  1. You can open the universe directory,
     either using the `File > Open Folder` option in the menu
     or from a terminal,
     still within the universe repository with `code .`.

  1. Go to the extensions tab `(Ctrl+Shift+X)`,
     type `@recommended`,
     and install the recommended extensions for the workspace.

     ![vscode-recommended-extensions](https://res.cloudinary.com/fluid-attacks/image/upload/v1664733557/docs/development/setup/recommended-extensions.png)
</Steps>

## Your first commit

You have almost finished the onboarding.

Your next goal is making your first commit reach production.

<Steps>
  1. Read the [Contributing section](/getting-started/contributing) carefully.

  1. [Create a new issue](https://gitlab.com/fluidattacks/universe/-/issues/new)
     following the onboarding template and assign it to you.

  1. Create your local branch.

     :::tip

     - Branch name must be equal to GitLab username.
     - All changes you upload must be on your branch.
       :::

  1. Add your name, username and email to the
     [.mailmap](https://gitlab.com/fluidattacks/universe/-/blob/trunk/.mailmap)
     file.

     :::tip
     Make sure your name is in alphabetical order:

     ```txt
     Your Name <username@fluidattacks.com> usernameatfluid <username@fluidattacks.com>
     ```

     :::

  1. Make sure your commit message follows the
     [expected syntax](/components/common/test/commit-msg/).

  1. Check if the commit message is valid using Makes:

     ```sh
     m . /common/test/commitlint
     ```

  1. Push your commit to the repository.
     :::tip
     - If the pipeline fails,
       you can see the logs of the failed jobs
       to get feedback about what went wrong.
     - Once the pipeline succeeds,
       you can then proceed to create a Merge Request.
       :::

  1. Once the Merge Request is approved and merged,
     and every other item in the issue is done,
     you can go ahead and close the issue.
</Steps>

## Troubleshooting

Here you will find typical errors
and how to solve them.

### Nix breaks after updating my mac

Sometimes, after updating your Mac,
Nix does not work properly.

This usually happens because
some macOS updates overwrite `~/.zshrc`.

In order to fix this:

<Steps>
  1. Add the following lines to your `~/.zshrc` file:
     ```sh
     if [ -e '/nix/var/nix/profiles/default/etc/profile.d/nix-daemon.sh' ]; then
       source '/nix/var/nix/profiles/default/etc/profile.d/nix-daemon.sh'
     fi
     ```
  1. Restart your machine
</Steps>
