import { defineConfig } from "astro/config";
import starlight from "@astrojs/starlight";
import starlightImageZoom from "starlight-image-zoom";
import starlightLinksValidator from "starlight-links-validator";

// https://astro.build/config
export default defineConfig({
  site: "https://dev.fluidattacks.com",
  server: {
    headers: {
      "Content-Security-Policy":
        "default-src 'self'; script-src 'self' 'unsafe-inline'; img-src 'self' res.cloudinary.com; style-src 'self' 'unsafe-inline';",
      "X-Content-Type-Options": "nosniff",
    },
  },
  outDir: "public",
  publicDir: "static",
  integrations: [
    starlight({
      title: "Fluid Attacks Development Portal",
      favicon: "/favicon.png",
      components: {
        Footer: "./src/components/Footer.astro",
      },
      customCss: ["./src/styles/custom.css"],
      editLink: {
        baseUrl:
          "https://gitlab.com/fluidattacks/universe/-/tree/trunk/common/docs",
      },
      lastUpdated: true,
      logo: {
        light: "./src/assets/logo-light.png",
        dark: "./src/assets/logo-dark.png",
        replacesTitle: true,
      },
      social: {
        linkedin: "https://www.linkedin.com/company/fluidattacks/",
        github: "https://github.com/fluidattacks",
        gitlab: "https://gitlab.com/fluidattacks",
      },
      // Accepting lowercase nested labels on sidebar due to
      // https://github.com/withastro/starlight/discussions/972
      sidebar: [
        {
          label: "Start Here",
          items: [
            "getting-started",
            "getting-started/philosophy",
            "getting-started/governance",
            "getting-started/environment",
            "getting-started/contributing",
            "getting-started/code-of-conduct",
            "getting-started/faq",
          ],
          collapsed: false,
        },
        {
          label: "Components",
          autogenerate: { directory: "components", collapsed: true },
          collapsed: false,
        },
      ],
      plugins: [
        starlightImageZoom(),
        starlightLinksValidator({
          errorOnRelativeLinks: true,
        }),
      ],
    }),
  ],
});
