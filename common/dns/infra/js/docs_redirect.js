addEventListener("fetch", event => {
  event.respondWith(handleRequest(event.request));
});

async function handleRequest(request) {
  const url = new URL(request.url);

  if (url.pathname.startsWith("/machine/scanner/plans/foss/")) {
    return Response.redirect(
      "https://help.fluidattacks.com/portal/en/kb/articles/validate-casa-tier-2-requirements",
      301
    );
  }

  return Response.redirect("https://fluidattacks.com", 301);
}
