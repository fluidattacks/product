const mode = "testing";
const max_age = 604800; // 1 week
const mx_list = [
  "mx.zoho.com",
  "mx2.zoho.com",
  "mx3.zoho.com",
];

const sts = `version: STSv1
mode: ${mode}
${mx_list.map((i) => "mx: " + i).join("\n")}
max_age: ${max_age}`;

addEventListener("fetch", (evt) => {
  return evt.respondWith(new Response(sts));
});
