module "secondary_domains" {
  source = "./modules/secondary-domain"

  for_each = toset(
    [
      "atfluid.com",
      "fluidattacks.app",
      "fluidattacks.cloud",
      "fluidattacks.co",
      "fluidattacks.com.co",
      "fluidattacks.dev",
      "fluidattacks.email",
      "fluidattacks.info",
      "fluidattacks.io",
      "fluidattacks.net",
      "fluidattacks.website",
      "fluid.com.co",
      "fluid.la",
      "fluidsignal.co",
      "fluidsignal.com",
      "fluidsignal.com.co",
    ]
  )

  cloudflareAccountId = var.cloudflareAccountId
  domain              = each.key
}

resource "aws_s3_bucket" "non_cloudflare" {
  # Domains not hosted on Cloudflare
  for_each = toset([
    "fluidattacks.ai",
  ])

  bucket = each.key

  tags = {
    "Name"              = each.key
    "fluidattacks:line" = "cost"
    "fluidattacks:comp" = "common"
  }
}
resource "aws_s3_bucket_ownership_controls" "non_cloudflare" {
  for_each = aws_s3_bucket.non_cloudflare
  bucket   = each.value.id

  rule {
    object_ownership = "ObjectWriter"
  }
}
resource "aws_s3_bucket_acl" "non_cloudflare" {
  for_each = aws_s3_bucket_ownership_controls.non_cloudflare

  bucket = each.value.id
  acl    = "private"

  depends_on = [
    aws_s3_bucket_ownership_controls.non_cloudflare,
    aws_s3_bucket_public_access_block.non_cloudflare
  ]
}

resource "aws_s3_bucket_versioning" "non_cloudflare" {
  for_each = aws_s3_bucket.non_cloudflare

  bucket = each.value.id
  versioning_configuration {
    status = "Enabled"
  }
}

resource "aws_s3_bucket_logging" "non_cloudflare" {
  for_each = aws_s3_bucket.non_cloudflare

  bucket        = each.value.id
  target_bucket = "common.logging"
  target_prefix = "log/${each.key}"
}

resource "aws_s3_bucket_server_side_encryption_configuration" "non_cloudflare" {
  for_each = aws_s3_bucket.non_cloudflare

  bucket = each.value.id

  rule {
    apply_server_side_encryption_by_default {
      sse_algorithm = "AES256"
    }
  }
}

resource "aws_s3_bucket_public_access_block" "non_cloudflare" {
  for_each = aws_s3_bucket.non_cloudflare

  bucket = each.value.id

  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}
