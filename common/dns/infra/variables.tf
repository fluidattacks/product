data "local_file" "headers" {
  filename = "js/headers.js"
}

data "local_file" "mta_sts" {
  filename = "js/mta-sts.js"
}
data "local_file" "mta_sts_org" {
  filename = "js/mta-sts-org.js"
}
data "local_file" "doc_redirect" {
  filename = "js/docs_redirect.js"
}

data "cloudflare_zone" "fluidattacks_com" {
  name = "fluidattacks.com"
}

variable "cloudflareAccountId" {
  type = string
}
variable "cloudflareApiKey" {
  type = string
}
variable "cloudflareEmail" {
  type = string
}
