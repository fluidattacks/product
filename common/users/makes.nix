{ outputs, ... }: {
  deployTerraform = {
    modules = {
      commonUsers = {
        setup = [
          outputs."/secretsForAwsFromGitlab/prodCommon"
          outputs."/secretsForEnvFromSops/commonCloudflare"
          outputs."/secretsForEnvFromSops/commonCloudflareProd"
          outputs."/secretsForEnvFromSops/commonUsers"
          outputs."/secretsForTerraformFromEnv/commonUsers"
        ];
        src = "/common/users/infra";
        version = "1.0";
      };
    };
  };
  lintTerraform = {
    modules = {
      commonUsers = {
        setup = [
          outputs."/secretsForAwsFromGitlab/dev"
          outputs."/secretsForEnvFromSops/commonCloudflare"
          outputs."/secretsForEnvFromSops/commonCloudflareDev"
        ];
        src = "/common/users/infra";
        version = "1.0";
      };
    };
  };
  secretsForEnvFromSops = {
    commonUsers = {
      manifest = "/common/secrets/dev.yaml";
      vars = [ "CSPM_EXTERNAL_ID" ];
    };
  };
  secretsForTerraformFromEnv = {
    commonUsers = { cspm_external_id = "CSPM_EXTERNAL_ID"; };
  };
  testTerraform = {
    modules = {
      commonUsers = {
        setup = [
          outputs."/secretsForAwsFromGitlab/dev"
          outputs."/secretsForEnvFromSops/commonCloudflare"
          outputs."/secretsForEnvFromSops/commonCloudflareDev"
          outputs."/secretsForEnvFromSops/commonUsers"
          outputs."/secretsForTerraformFromEnv/commonUsers"
        ];
        src = "/common/users/infra";
        version = "1.0";
      };
    };
  };
}
