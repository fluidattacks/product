data "aws_caller_identity" "main" {}
data "cloudflare_api_token_permission_groups" "all" {}

data "aws_s3_bucket" "bucket_logging" {
  bucket = "common-cloudtrail-access-log-bucket"
}

variable "region" {
  default = "us-east-1"
  type    = string
}
variable "inference_midpoint_region" {
  default = "us-east-2"
  type    = string
}
variable "inference_west_region" {
  default = "us-west-2"
  type    = string
}
variable "terraform_state_lock_arn" {
  default = "arn:aws:dynamodb:us-east-1:205810638802:table/terraform_state_lock"
  type    = string
}
variable "accountId" {
  default = "205810638802"
  type    = string
}
variable "cspm_external_id" {
  sensitive = true
  type      = string
}
