locals {
  debug_integrates = {
    policies = {
      aws = {
        DebugPolicy = [
          {
            Sid    = "batchRead"
            Effect = "Allow"
            Action = [
              "batch:DescribeComputeEnvironments",
              "batch:DescribeJobDefinitions",
              "batch:DescribeJobQueues",
              "batch:DescribeJobs",
              "batch:DescribeSchedulingPolicies",
              "batch:ListJobs",
              "batch:ListSchedulingPolicies",
            ]
            Resource = ["*"]
          },
          {
            Sid    = "batchListTagsForResource"
            Effect = "Allow"
            Action = [
              "batch:ListTagsForResource",
            ]
            Resource = [
              "arn:aws:batch:${var.region}:${data.aws_caller_identity.main.account_id}:compute-environment/*",
              "arn:aws:batch:${var.region}:${data.aws_caller_identity.main.account_id}:job/*",
              "arn:aws:batch:${var.region}:${data.aws_caller_identity.main.account_id}:job-definition/*:*",
              "arn:aws:batch:${var.region}:${data.aws_caller_identity.main.account_id}:job-queue/*",
              "arn:aws:batch:${var.region}:${data.aws_caller_identity.main.account_id}:scheduling-policy/*",
            ]
          },
          {
            Sid    = "cloudwatchRead1"
            Effect = "Allow"
            Action = [
              "cloudwatch:DescribeAlarmsForMetric",
              "cloudwatch:DescribeAnomalyDetectors",
              "cloudwatch:DescribeInsightRules",
              "cloudwatch:GetMetricData",
              "cloudwatch:GetMetricStatistics",
              "cloudwatch:GetMetricWidgetImage",
              "cloudwatch:GetTopologyDiscoveryStatus",
              "cloudwatch:GetTopologyMap",
              "cloudwatch:ListDashboards",
              "cloudwatch:ListManagedInsightRules",
              "cloudwatch:ListMetrics",
              "cloudwatch:ListMetricStreams",
              "cloudwatch:ListServiceLevelObjectives",
              "cloudwatch:ListServices",
            ]
            Resource = ["*"]
          },
          {
            Sid    = "cloudwatchRead2"
            Effect = "Allow"
            Action = [
              "cloudwatch:DescribeAlarmHistory",
              "cloudwatch:DescribeAlarms",
              "cloudwatch:GetDashboard",
              "cloudwatch:GetInsightRuleReport",
              "cloudwatch:GetMetricStream",
              "cloudwatch:GetService*",
              "cloudwatch:ListTagsForResource",
            ]
            Resource = [
              "arn:aws:cloudwatch:${var.region}:${data.aws_caller_identity.main.account_id}:*",
              "arn:aws:cloudwatch::${data.aws_caller_identity.main.account_id}:dashboard/*",
            ]
          },
          {
            Sid    = "auroraDsql"
            Effect = "Allow"
            Action = [
              "dsql:*Cluster",
              "dsql:CreateMultiRegionClusters",
              "dsql:Db*",
              "dsql:Delete*",
              "dsql:ListClusters",
              "dsql:*Resource"
            ]
            Resource = ["arn:aws:dsql:*:${data.aws_caller_identity.main.account_id}:cluster/*"]
          },
          {
            Sid    = "dynamoDBList"
            Effect = "Allow"
            Action = [
              "dynamodb:DescribeExport",
              "dynamodb:ListBackups",
              "dynamodb:ListExports",
              "dynamodb:ListTables",
            ]
            Resource = [
              "arn:aws:dynamodb:${var.region}:${data.aws_caller_identity.main.account_id}:table/*",
            ]
          },
          {
            Sid    = "dynamoDBRead"
            Effect = "Allow"
            Action = [
              "dynamodb:BatchGetItem",
              "dynamodb:ConditionCheckItem",
              "dynamodb:DescribeBackup",
              "dynamodb:DescribeTable",
              "dynamodb:ExportTableToPointInTime",
              "dynamodb:GetItem",
              "dynamodb:Query",
              "dynamodb:Scan"
            ]
            Resource = [
              "arn:aws:dynamodb:${var.region}:${data.aws_caller_identity.main.account_id}:table/fi*",
              "arn:aws:dynamodb:${var.region}:${data.aws_caller_identity.main.account_id}:table/integrates*",
              "arn:aws:dynamodb:${var.region}:${data.aws_caller_identity.main.account_id}:table/integrates*/index/*"
            ]
          },
          {
            Sid    = "logsReadAll"
            Effect = "Allow"
            Action = [
              "logs:DescribeAccountPolicies",
              "logs:DescribeDeliveries",
              "logs:DescribeDeliveryDestinations",
              "logs:DescribeDeliverySources",
              "logs:DescribeDestinations",
              "logs:DescribeExportTasks",
              "logs:DescribeLogGroups",
              "logs:DescribeQueries",
              "logs:DescribeQueryDefinitions",
              "logs:DescribeResourcePolicies",
              "logs:ListLogDeliveries",
              "logs:GetLogDelivery",
            ]
            Resource = ["*"]
          },
          {
            Sid    = "logsRead"
            Effect = "Allow"
            Action = [
              "logs:DescribeLogStreams",
              "logs:DescribeMetricFilters",
              "logs:DescribeSubscriptionFilters",
              "logs:ListAnomalies",
              "logs:ListLogAnomalyDetectors",
              "logs:ListTagsForResource",
              "logs:ListTagsLogGroup",
              "logs:FilterLogEvents",
              "logs:Get*",
              "logs:Unmask",
            ]
            Resource = [
              "arn:aws:logs:${var.region}:${var.accountId}:log-group:*",
              "arn:aws:logs:${var.region}:${var.accountId}:delivery:*",
              "arn:aws:logs:${var.region}:${var.accountId}:delivery-destination:*",
              "arn:aws:logs:${var.region}:${var.accountId}:delivery-source:*",
              "arn:aws:logs:${var.region}:${var.accountId}:anomaly-detector:*",
              "arn:aws:logs:${var.region}:${var.accountId}:log-group:*:log-stream:*",
            ]
          },
          {
            Sid    = "opensearchRead1"
            Effect = "Allow"
            Action = [
              "es:DescribeInboundCrossClusterSearchConnections",
              "es:DescribeOutboundCrossClusterSearchConnections",
              "es:DescribeOutboundConnections",
              "es:DescribeVpcEndpoints",
              "es:DescribeInboundConnections",
              "es:DescribeReservedInstances",
              "es:DescribeReservedElasticsearchInstances",
              "es:DescribeReservedElasticsearchInstanceOfferings",
              "es:DescribePackages",
              "es:DescribeElasticsearchInstanceTypeLimits",
              "es:DescribeInstanceTypeLimits",
              "es:DescribeReservedInstanceOfferings",
              "es:GetPackageVersionHistory",
              "es:ListElasticsearchVersions",
              "es:ListInstanceTypeDetails",
              "es:ListDomainNames",
              "es:ListVersions",
              "es:ListVpcEndpoints",
              "es:ListDomainsForPackage",
              "es:ListVpcEndpointAccess",
              "es:ListElasticsearchInstanceTypes",
              "es:ListVpcEndpointsForDomain",
              "es:ListElasticsearchInstanceTypeDetails",
            ]
            Resource = ["*"]
          },
          {
            Sid    = "opensearchRead2"
            Effect = "Allow"
            Action = [
              "es:DescribeDomainChangeProgress",
              "es:DescribeElasticsearchDomainConfig",
              "es:DescribeDomainNodes",
              "es:DescribeElasticsearchDomain",
              "es:DescribeElasticsearchDomains",
              "es:DescribeDomainAutoTunes",
              "es:DescribeDomain",
              "es:DescribeDomainHealth",
              "es:DescribeDomainConfig",
              "es:DescribeDomains",
              "es:DescribeDryRunProgress",
              "es:GetCompatibleElasticsearchVersions",
              "es:GetDataSource",
              "es:GetDomainMaintenanceStatus",
              "es:GetUpgradeHistory",
              "es:GetCompatibleVersions",
              "es:GetUpgradeStatus",
              "es:ListDataSources",
              "es:ListScheduledActions",
              "es:ListPackagesForDomain",
              "es:ListTags",
              "es:ListDomainMaintenances",
            ]
            Resource = ["arn:aws:es:${var.region}:${data.aws_caller_identity.main.account_id}:domain/*"]
          },
          {
            Sid    = "opensearchServerless"
            Effect = "Allow"
            Action = [
              "aoss:BatchGet*",
              "aoss:Get*",
              "aoss:List*",
            ]
            Resource = ["*"]
          },
          {
            Sid    = "opensearchServerlessWrite"
            Effect = "Allow"
            Action = [
              "aoss:APIAccessAll",
              "aoss:DashboardsAccessAll",
            ]
            Resource = [
              "arn:aws:aoss:${var.region}:${data.aws_caller_identity.main.account_id}:collection/*",
              "arn:aws:aoss:${var.region}:${data.aws_caller_identity.main.account_id}:dashboards/default"
            ]
          },
          {
            Sid    = "s3Read"
            Effect = "Allow"
            Action = [
              "s3:Get*",
            ]
            Resource = [
              "arn:aws:s3:::integrates",
              "arn:aws:s3:::integrates/*",
              "arn:aws:s3:::integrates.continuous-repositories",
              "arn:aws:s3:::integrates.continuous-repositories/*",
              "arn:aws:s3:::machine.data",
              "arn:aws:s3:::machine.data/*",
              "arn:aws:s3:::sorts",
              "arn:aws:s3:::sorts/*",
            ]
          },
          {
            Sid    = "sqsList"
            Effect = "Allow"
            Action = [
              "sqs:ListQueues"
            ]
            Resource = ["*"]
          },
          {
            Sid    = "sqsRead"
            Effect = "Allow"
            Action = [
              "sqs:GetQueueUrl",
              "sqs:GetQueueAttributes",
              "sqs:ListQueueTags",
            ]
            Resource = [
              "arn:aws:sqs:${var.region}:${data.aws_caller_identity.main.account_id}:integrates_*",
            ]
          },
        ]
        DebugPolicy2 = [
          {
            Sid    = "kms"
            Effect = "Allow",
            Action = [
              "kms:Decrypt",
              "kms:GenerateDataKey"
            ],
            Resource = [
              "arn:aws:kms:${var.region}:${data.aws_caller_identity.main.account_id}:key/7b7eb12b-6441-4341-89f3-d0c279b18cd6",
            ]
          },
          {
            Sid    = "lambda1"
            Effect = "Allow",
            Action = [
              "lambda:GetAccountSettings",
              "lambda:GetProvisionedConcurrencyConfig",
              "lambda:ListCodeSigningConfigs",
              "lambda:ListEventSourceMappings",
              "lambda:ListFunctions",
              "lambda:ListLayerVersions",
              "lambda:ListLayers",
              "lambda:ListTags",
            ],
            Resource = ["*"]
          },
          {
            Sid    = "lambda2"
            Effect = "Allow",
            Action = [
              "lambda:GetAlias",
              "lambda:GetCodeSigningConfig",
              "lambda:GetEventSourceMapping",
              "lambda:GetFunction*",
              "lambda:GetLayer*",
              "lambda:GetPolicy",
              "lambda:GetRuntimeManagementConfig",
              "lambda:ListAliases",
              "lambda:ListFunction*Configs",
              "lambda:ListProvisionedConcurrencyConfig*",
              "lambda:ListVersionsByFunction"
            ],
            Resource = [
              "arn:aws:lambda:${var.region}:${data.aws_caller_identity.main.account_id}:code-signing-config:*",
              "arn:aws:lambda:${var.region}:${data.aws_caller_identity.main.account_id}:event-source-mapping:*",
              "arn:aws:lambda:${var.region}:${data.aws_caller_identity.main.account_id}:function:*",
              "arn:aws:lambda:${var.region}:${data.aws_caller_identity.main.account_id}:layer:*:*"
            ]
            }, {
            Sid    = "aurora1"
            Effect = "Allow"
            Action = [
              "rds:DescribeAccountAttributes",
              "rds:DescribeBlueGreenDeployments",
              "rds:DescribeCertificates",
              "rds:DescribeDBClusterEndpoints",
              "rds:DescribeDBClusterSnapshots",
              "rds:DescribeDBEngineVersions",
              "rds:DescribeDBInstanceAutomatedBackups",
              "rds:DescribeDBRecommendations",
              "rds:DescribeDBSnapshots",
              "rds:DescribeEngine*",
              "rds:DescribeEventCategories",
              "rds:DescribeEvents",
              "rds:DescribeExportTasks",
              "rds:DescribeOptionGroupOptions",
              "rds:DescribeOrderableDBInstanceOptions",
              "rds:DescribePendingMaintenanceActions",
              "rds:DescribeRecommendation*",
              "rds:DescribeReservedDBInstancesOfferings",
              "rds:DescribeSourceRegions",
            ]
            Resource = ["*"]
          },
          {
            Sid    = "aurora2"
            Effect = "Allow"
            Action = [
              "rds:DescribeDBClusterAutomatedBackups",
              "rds:DescribeDBClusterBacktracks",
              "rds:DescribeDBClusterParameter*",
              "rds:DescribeDBClusterSnapshotAttributes",
              "rds:DescribeDBClusters",
              "rds:DescribeDBInstances",
              "rds:DescribeDBLogFiles",
              "rds:DescribeDBParameter*",
              "rds:DescribeDBProx*",
              "rds:DescribeDBSecurityGroups",
              "rds:DescribeDBShardGroups",
              "rds:DescribeDBSnapshotAttributes",
              "rds:DescribeDBSnapshotTenantDatabases",
              "rds:DescribeDBSubnetGroups",
              "rds:DescribeEventSubscriptions",
              "rds:DescribeGlobalClusters",
              "rds:DescribeIntegrations",
              "rds:DescribeOptionGroups",
              "rds:DescribeReservedDBInstances",
              "rds:DescribeTenantDatabases",
              "rds:DescribeValidDBInstanceModifications",
              "rds:ListTagsForResource",
            ]
            Resource = [
              "arn:aws:rds:${var.region}:*:cluster:*",
              "arn:aws:rds:${var.region}:${var.accountId}:cluster-auto-backup:*",
              "arn:aws:rds:${var.region}:${var.accountId}:cluster-pg:*",
              "arn:aws:rds:${var.region}:${var.accountId}:cluster-snapshot:*",
              "arn:aws:rds:${var.region}:${var.accountId}:db:*",
              "arn:aws:rds:${var.region}:${var.accountId}:es:*",
              "arn:aws:rds::${var.accountId}:global-cluster:*",
              "arn:aws:rds:${var.region}:${var.accountId}:integration:*",
              "arn:aws:rds:${var.region}:${var.accountId}:og:*",
              "arn:aws:rds:${var.region}:${var.accountId}:pg:*",
              "arn:aws:rds:${var.region}:${var.accountId}:db-proxy:*",
              "arn:aws:rds:${var.region}:${var.accountId}:db-proxy-endpoint:*",
              "arn:aws:rds:${var.region}:${var.accountId}:ri:*",
              "arn:aws:rds:${var.region}:${var.accountId}:secgrp:*",
              "arn:aws:rds:${var.region}:${var.accountId}:shard-group:*",
              "arn:aws:rds:${var.region}:${var.accountId}:snapshot:*",
              "arn:aws:rds:${var.region}:${var.accountId}:snapshot-tenant-database:*:*",
              "arn:aws:rds:${var.region}:${var.accountId}:subgrp:*",
              "arn:aws:rds:${var.region}:${var.accountId}:target-group:*",
              "arn:aws:rds:${var.region}:${var.accountId}:tenant-database:*"
            ]
          },
          {
            Sid    = "SecretsManager1"
            Effect = "Allow"
            Action = [
              "secretsmanager:BatchGetSecretValue",
              "secretsmanager:GetRandomPassword",
              "secretsmanager:ListSecrets"
            ],
            Resource = ["*"]
          },
          {
            Sid    = "SecretsManager2"
            Effect = "Allow"
            Action = [
              "secretsmanager:DescribeSecret",
              "secretsmanager:GetSecretValue",
              "secretsmanager:ListSecretVersionIds",
              "secretsmanager:GetResourcePolicy"
            ],
            Resource = ["arn:aws:secretsmanager:${var.region}:${data.aws_caller_identity.main.account_id}:secret:*"]
          },
          {
            Sid    = "sqsQueueMessages"
            Effect = "Allow"
            Action = [
              "sqs:CancelMessageMoveTask",
              "sqs:DeleteMessage",
              "sqs:GetQueueAttributes",
              "sqs:ListDeadLetterSourceQueues",
              "sqs:ListMessageMoveTasks",
              "sqs:ReceiveMessage",
              "sqs:SendMessage",
              "sqs:StartMessageMoveTask",
            ]
            Resource = [
              "arn:aws:sqs:${var.region}:${data.aws_caller_identity.main.account_id}:integrates_audit.fifo",
              "arn:aws:sqs:${var.region}:${data.aws_caller_identity.main.account_id}:integrates_audit_dlq.fifo",
              "arn:aws:sqs:${var.region}:${data.aws_caller_identity.main.account_id}:integrates_dead_queue",
              "arn:aws:sqs:${var.region}:${data.aws_caller_identity.main.account_id}:integrates_tasks_dev",
            ]
          },
          {
            Sid    = "s3List1"
            Effect = "Allow"
            Action = [
              "s3:ListAccessGrantsInstances",
              "s3:ListAccessPoints",
              "s3:ListAccessPointsForObjectLambda",
              "s3:ListAllMyBuckets",
              "s3:ListJobs",
              "s3:ListMultiRegionAccessPoints",
              "s3:ListStorageLensConfigurations",
              "s3:ListStorageLensGroups",
              "s3:ListTagsForResource",
            ]
            Resource = ["*"]
          },
          {
            Sid    = "s3List2"
            Effect = "Allow"
            Action = [
              "s3:ListAccessGrants",
              "s3:ListAccessGrantsLocations",
              "s3:ListBucket",
              "s3:ListBucketMultipartUploads",
              "s3:ListBucketVersions",
              "s3:ListCallerAccessGrants",
              "s3:ListMultipartUploadParts",
            ]
            Resource = [
              "arn:aws:s3:*:*:access-grants/default",
              "arn:aws:s3:::*",
              "arn:aws:s3:::*/*"
            ]
          },
          {
            Sid    = "performanceInsights"
            Effect = "Allow"
            Action = [
              "pi:Create*",
              "pi:Describe*",
              "pi:Get*",
              "pi:List*",
            ]
            Resource = ["*"]
          },
        ]
        CostPolicy = [
          {
            Sid    = "cost"
            Effect = "Allow"
            Action = [
              "bcm-pricing-calculator:Batch*",
              "bcm-pricing-calculator:Create*",
              "bcm-pricing-calculator:Delete*",
              "bcm-pricing-calculator:Get*",
              "bcm-pricing-calculator:List*",
              "bcm-pricing-calculator:Update*",
              "billing:Get*",
              "billing:List*",
              "ce:Describe*",
              "ce:Get*",
              "ce:List*",
              "consolidatedbilling:Get*",
              "consolidatedbilling:List*",
              "cost-optimization-hub:Get*",
              "cost-optimization-hub:List*",
              "cost-optimization-hub:Update*",
              "cur:Describe*",
              "cur:Get*",
              "cur:Validate*",
              "invoicing:Get*",
              "invoicing:List*",
              "payments:Get*",
              "payments:List*",
              "pricing:Describe*",
              "pricing:Get*",
              "rbin:GetRule",
              "rbin:ListTagsForResource",
              "servicecatalog:List*",
              "tax:Get*",
              "tax:List*",
            ]
            Resource = ["*"]
          },
        ]
      }
    }
    keys = {
      debug_integrates = {
        admins = [
          "prod_common",
        ]
        read_users = []
        users = [
          "debug_integrates"
        ]
        tags = {
          "Name"              = "debug_integrates"
          "fluidattacks:line" = "research"
          "fluidattacks:comp" = "common"
        }
      }
    }
  }
}

module "debug_integrates_aws" {
  source = "./modules/aws"

  name     = "debug_integrates"
  policies = local.debug_integrates.policies.aws

  tags = {
    "Name"              = "debug_integrates"
    "fluidattacks:line" = "cost"
    "fluidattacks:comp" = "integrates"
  }
}
