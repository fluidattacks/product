resource "aws_accessanalyzer_analyzer" "access_analyzer" {
  analyzer_name = "iam-analyzer"
  type          = "ACCOUNT"
  tags = {
    "Name"              = "iam_analyzer"
    "fluidattacks:line" = "cost"
    "fluidattacks:comp" = "common"
  }
}
