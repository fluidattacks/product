# Snowflake
locals {
  snowflake_assume_role_policy = {
    Version = "2012-10-17",
    Statement = concat(
      [
        {
          Sid    = "snowflakeAccess",
          Effect = "Allow",
          Principal = {
            AWS = "arn:aws:iam::654654482841:user/j7so0000-s",
          },
          Action = "sts:AssumeRole",
          Condition = {
            StringEquals = {
              # this is not a secret
              # see: https://docs.aws.amazon.com/IAM/latest/UserGuide/id_roles_create_for-user_externalid.html
              "sts:ExternalId" : "HHB24285_SFCRole=2_IiDz1wjOvCskylnxgBuN/SkcJLs="
            },
          },
        },
      ],
    )
  }
}

resource "aws_iam_role" "snowflake_role" {
  name                 = "snowflake_role"
  assume_role_policy   = jsonencode(local.snowflake_assume_role_policy)
  max_session_duration = "3600"
  tags = {
    "Name"              = "snowflake_role"
    "fluidattacks:line" = "cost"
    "fluidattacks:comp" = "etl"
  }
}
data "aws_iam_policy_document" "snowflake_read_policy_doc" {
  statement {
    sid    = "SnowflakeS3ReadAccess"
    effect = "Allow"

    actions = [
      "s3:GetObject",
      "s3:GetObjectVersion",
    ]

    resources = [
      "arn:aws:s3:::observes.etl-data/*",
      "arn:aws:s3:::admin-console205810638802/*"
    ]
  }
  statement {
    sid    = "SnowflakeS3List"
    effect = "Allow"

    actions = [
      "s3:GetBucketLocation",
      "s3:ListBucket",
    ]

    resources = [
      "arn:aws:s3:::observes.etl-data",
      "arn:aws:s3:::admin-console205810638802"
    ]
  }
}
resource "aws_iam_policy" "snowflake_read_policy" {
  name        = "snowflake_read_policy"
  path        = "/"
  description = "Policy for snowflake s3 read access"
  policy      = data.aws_iam_policy_document.snowflake_read_policy_doc.json
  tags = {
    "Name"              = "snowflake_read_policy"
    "fluidattacks:line" = "cost"
    "fluidattacks:comp" = "etl"
  }
}
resource "aws_iam_role_policy_attachment" "snowflake_role_policy" {
  role       = aws_iam_role.snowflake_role.name
  policy_arn = aws_iam_policy.snowflake_read_policy.arn
}
