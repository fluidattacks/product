locals {
  prod_integrates = {
    policies = {
      aws = {
        IntegratesPolicy1 = [
          {
            Sid    = "iamWrite"
            Effect = "Allow"
            Action = [
              "iam:AttachRolePolicy",
              "iam:CreatePolicy",
              "iam:CreatePolicyVersion",
              "iam:CreateRole",
              "iam:CreateServiceLinkedRole",
              "iam:DeletePolicy",
              "iam:DeletePolicyVersion",
              "iam:DetachRolePolicy",
              "iam:DeleteRole",
              "iam:TagPolicy",
              "iam:UntagPolicy",
              "iam:PassRole",
              "iam:TagRole",
              "iam:UntagRole"
            ]
            Resource = [
              "arn:aws:iam::${var.accountId}:role/*",
              "arn:aws:iam::${var.accountId}:policy/*",
            ]
          },
          {
            Sid    = "ecrAll"
            Effect = "Allow"
            Action = [
              "ecr:DescribePullThroughCacheRules",
              "ecr:GetRegistryScanningConfiguration",
            ]
            Resource = ["*"]
          },
          {
            Sid    = "ecr"
            Effect = "Allow"
            Action = [
              "ecr:Describe*",
              "ecr:Get*",
              "ecr:List*",
              "ecr:SetRepositoryPolicy",
            ]
            Resource = [
              "arn:aws:ecr:${var.region}:${var.accountId}:repository/*"
            ]
          },
          {
            Sid    = "elasticacheWrite"
            Effect = "Allow"
            Action = ["*"]
            Resource = [
              "arn:aws:elasticache:${var.region}:${data.aws_caller_identity.main.account_id}:cluster:integrates-*",
              "arn:aws:elasticache:${var.region}:${data.aws_caller_identity.main.account_id}:replicationgroup:integrates-*",
              "arn:aws:elasticache:${var.region}:${data.aws_caller_identity.main.account_id}:subnetgroup:integrates-*",
            ]
          },
          {
            Sid    = "elasticloadbalancingRead"
            Effect = "Allow"
            Action = [
              "elasticloadbalancing:DescribeInstanceHealth",
              "elasticloadbalancing:DescribeLoadBalancerAttributes",
              "elasticloadbalancing:DescribeLoadBalancerPolicies",
              "elasticloadbalancing:DescribeLoadBalancerPolicyTypes",
              "elasticloadbalancing:DescribeLoadBalancers",
              "elasticloadbalancing:DescribeTags",
            ]
            Resource = ["*"]
          },
          {
            Sid    = "inspectorRead"
            Effect = "Allow"
            Action = [
              "inspector2:BatchGetAccountStatus",
              "inspector2:BatchGetFreeTrialInfo",
              "inspector2:Describe*",
              "inspector2:Get*",
              "inspector2:List*",
            ]
            Resource = ["*"]
          },
          {
            Sid    = "s3Create"
            Effect = "Allow"
            Action = [
              "s3:CreateBucket"
            ]
            Resource = ["arn:aws:s3:::*"]
          },
          {
            Sid    = "s3Read"
            Effect = "Allow"
            Action = [
              "s3:Get*",
              "s3:ListBucket",
            ]
            Resource = [
              "arn:aws:s3:::integrates/continuous-data",
              "arn:aws:s3:::integrates/continuous-data/*",
              "arn:aws:s3:::integrates/continuous-repositories",
              "arn:aws:s3:::integrates/continuous-repositories/*",
              "arn:aws:s3:::logpush.fluidattacks.com",
              "arn:aws:s3:::logpush.fluidattacks.com/*",
              "arn:aws:s3:::machine.data",
              "arn:aws:s3:::machine.data/*",
              "arn:aws:s3:::sorts",
              "arn:aws:s3:::sorts/*",
            ]
          },
          {
            Sid    = "s3ReadFluid"
            Effect = "Allow"
            Action = [
              "s3:ListBucket",
            ]
            Resource = [
              "arn:aws:s3:::fluidattacks.com",
            ]
          },
          {
            Sid      = "ListObjectsInTfStatesProd",
            Effect   = "Allow",
            Action   = ["s3:ListBucket"],
            Resource = ["arn:aws:s3:::fluidattacks-terraform-states-prod"]
          },
          {
            Sid    = "s3Write"
            Effect = "Allow"
            Action = ["*"]
            Resource = [
              "arn:aws:s3:::fluidattacks-terraform-states-prod/integrates*",
              "arn:aws:s3:::fluidintegrates*/*",
              "arn:aws:s3:::fluidintegrates*",
              "arn:aws:s3:::fluidattacks.public.storage",
              "arn:aws:s3:::fluidattacks.public.storage/*",
              "arn:aws:s3:::integrates*/*",
              "arn:aws:s3:::integrates*",
              "arn:aws:s3:::continuous*",
              "arn:aws:s3:::continuous*/*",
              "arn:aws:s3:::common.logging/",
              "arn:aws:s3:::common.logging/*",
              "arn:aws:s3:::machine.data",
              "arn:aws:s3:::machine.data/*",

            ]
          },
          {
            Sid    = "tags"
            Effect = "Allow"
            Action = [
              "tag:GetResources",
            ]
            Resource = [
              "*",
            ]
          },
          {
            Sid    = "batchRead"
            Effect = "Allow"
            Action = [
              "batch:DescribeComputeEnvironments",
              "batch:DescribeJobDefinitions",
              "batch:DescribeJobQueues",
              "batch:DescribeJobs",
              "batch:DescribeSchedulingPolicies",
              "batch:ListJobs",
              "batch:ListSchedulingPolicies",
            ]
            Resource = ["*"]
          },
          {
            Sid    = "batchWrite"
            Effect = "Allow"
            Action = [
              "batch:CancelJob",
              "batch:SubmitJob",
              "batch:TagResource",
              "batch:TerminateJob",
              "batch:ListTagsForResource",
              "batch:UntagResource",
            ]
            Resource = [
              "arn:aws:batch:us-east-1:${data.aws_caller_identity.main.account_id}:job-definition/*",
              "arn:aws:batch:us-east-1:${data.aws_caller_identity.main.account_id}:job-queue/*",
              "arn:aws:batch:us-east-1:${data.aws_caller_identity.main.account_id}:job/*",
              "arn:aws:batch:us-east-1:${data.aws_caller_identity.main.account_id}:compute-environment/*",
              "arn:aws:batch:us-east-1:${data.aws_caller_identity.main.account_id}:scheduling-policy/*",
            ]
          },
          {
            Sid    = "logsReadAll"
            Effect = "Allow"
            Action = [
              "logs:DescribeAccountPolicies",
              "logs:DescribeDeliveries",
              "logs:DescribeDeliveryDestinations",
              "logs:DescribeDeliverySources",
              "logs:DescribeDestinations",
              "logs:DescribeExportTasks",
              "logs:DescribeLogGroups",
              "logs:DescribeQueries",
              "logs:DescribeQueryDefinitions",
              "logs:DescribeResourcePolicies",
              "logs:ListLogDeliveries",
              "logs:GetLogDelivery",
            ]
            Resource = ["*"]
          },
          {
            Sid    = "logsRead"
            Effect = "Allow"
            Action = [
              "logs:DescribeLogStreams",
              "logs:DescribeMetricFilters",
              "logs:DescribeSubscriptionFilters",
              "logs:ListAnomalies",
              "logs:ListLogAnomalyDetectors",
              "logs:ListTagsForResource",
              "logs:ListTagsLogGroup",
              "logs:FilterLogEvents",
              "logs:Get*",
              "logs:Unmask",
            ]
            Resource = [
              "arn:aws:logs:${var.region}:${var.accountId}:log-group:*",
              "arn:aws:logs:${var.region}:${var.accountId}:delivery:*",
              "arn:aws:logs:${var.region}:${var.accountId}:delivery-destination:*",
              "arn:aws:logs:${var.region}:${var.accountId}:delivery-source:*",
              "arn:aws:logs:${var.region}:${var.accountId}:anomaly-detector:*",
              "arn:aws:logs:${var.region}:${var.accountId}:log-group:*:log-stream:*",
            ]
          },
          {
            Sid    = "logsWriteMachine"
            Effect = "Allow"
            Action = [
              "logs:Create*",
              "logs:Delete*",
              "logs:Describe*",
              "logs:Get*",
              "logs:Put*",
              "logs:StartQuery",
            ]
            Resource = [
              "arn:aws:logs:us-east-1:${data.aws_caller_identity.main.account_id}:log-group:skims",
              "arn:aws:logs:us-east-1:${data.aws_caller_identity.main.account_id}:log-group:skims:log-stream:*",
            ]
          },
          {
            Sid    = "logsWrite"
            Effect = "Allow"
            Action = [
              "logs:Create*",
              "logs:Delete*",
              "logs:Describe*",
              "logs:Get*",
              "logs:Put*",
              "logs:StartQuery",
            ]
            Resource = [
              "arn:aws:logs:us-east-1:${data.aws_caller_identity.main.account_id}:log-group:integrates*",
              "arn:aws:logs:us-east-1:${data.aws_caller_identity.main.account_id}:log-group:FLUID*",
              "arn:aws:logs:us-east-1:${data.aws_caller_identity.main.account_id}:log-group:opensearch*",
            ]
          },
          {
            Sid    = "logsGlobalWrite"
            Effect = "Allow"
            Action = [
              "logs:CreateLogGroup",
              "logs:CreateLogStream",
              "logs:DeleteLogGroup",
              "logs:PutDataProtectionPolicy",
              "logs:PutResourcePolicy",
              "logs:PutRetentionPolicy",
              "logs:TagLogGroup",
              "logs:UntagLogGroup"
            ]
            Resource = [
              "arn:aws:logs:us-east-1:${data.aws_caller_identity.main.account_id}:*",
            ]
          },
        ]
        IntegratesPolicy2 = [
          {
            Sid    = "ec2Read"
            Effect = "Allow"
            Action = [
              "ec2:Describe*",
              "ec2:Get*",
            ]
            Resource = ["*"]
          },
          {
            Sid    = "ec2WriteAll"
            Effect = "Allow"
            Action = [
              "ec2:*Tags",
            ]
            Resource = ["*"]
          },
          {
            Sid    = "ec2Write"
            Effect = "Allow"
            Action = [
              "ec2:ApplySecurityGroupsToClientVpnTargetNetwork",
              "ec2:AuthorizeSecurityGroupEgress",
              "ec2:AuthorizeSecurityGroupIngress",
              "ec2:CreateSecurityGroup",
              "ec2:CreateVpcEndpoint",
              "ec2:DeleteSecurityGroup",
              "ec2:DeleteVpcEndPoints",
              "ec2:ModifyVpcEndPoint",
              "ec2:RevokeSecurityGroupEgress",
              "ec2:RevokeSecurityGroupIngress",
              "ec2:UpdateSecurityGroupRuleDescriptionsEgress",
              "ec2:UpdateSecurityGroupRuleDescriptionsIngress",
            ]
            Resource = [
              "arn:aws:ec2:${var.region}:${var.accountId}:client-vpn-endpoint/*",
              "arn:aws:ec2:${var.region}:${var.accountId}:security-group/*",
              "arn:aws:ec2:${var.region}:${var.accountId}:subnet/*",
              "arn:aws:ec2:${var.region}:${var.accountId}:vpc/*",
              "arn:aws:ec2:${var.region}:${var.accountId}:vpc-endpoint/*",
            ]
          },
          {
            Sid    = "eksDescribe1"
            Effect = "Allow"
            Action = [
              "eks:DescribeAddonConfiguration",
              "eks:DescribeAddonVersions",
            ]
            Resource = ["*"]
          },
          {
            Sid    = "eksDescribe2"
            Effect = "Allow"
            Action = [
              "eks:DescribeAccessEntry",
              "eks:DescribeAddon",
              "eks:DescribeCluster",
              "eks:DescribeEksAnywhereSubscription",
              "eks:DescribeFargateProfile",
              "eks:DescribeIdentityProviderConfig",
              "eks:DescribeInsight",
              "eks:DescribeNodegroup",
              "eks:DescribePodIdentityAssociation",
              "eks:DescribeUpdate",
            ]
            Resource = ["arn:aws:eks:${var.region}:${data.aws_caller_identity.main.account_id}:*"]
          },
          {
            Sid    = "eksWrite"
            Effect = "Allow"
            Action = ["*"]
            Resource = [
              "arn:aws:eks:${var.region}:${data.aws_caller_identity.main.account_id}:cluster/integrates-*"
            ]
          },
          {
            Sid      = "auroraDsql"
            Effect   = "Allow"
            Action   = ["dsql:*"]
            Resource = ["*"]
          },
          {
            Sid    = "AmazonRDS"
            Effect = "Allow"
            Action = [
              "rds:CreateDBSubnetGroup",
              "rds:AddTagsToResource"
            ]
            Resource = ["*"]
          },
          {
            Sid    = "KeyManagementService"
            Effect = "Allow"
            Action = [
              "kms:DescribeKey",
            ],
            Resource = ["*"]
          },
          {
            Sid    = "SecretsManager"
            Effect = "Allow"
            Action = [
              "secretsmanager:BatchGet*",
              "secretsmanager:CreateSecret",
              "secretsmanager:Describe*",
              "secretsmanager:Get*",
              "secretsmanager:List*",
              "secretsmanager:TagResource"
            ],
            Resource = ["*"]
          },
          {
            Sid      = "dynamoWrite"
            Effect   = "Allow"
            Action   = ["dynamodb:*"]
            Resource = ["*"]
          },
          {
            Sid    = "CloudwatchAlarmWrite"
            Effect = "Allow"
            Action = [
              "cloudwatch:DeleteAlarms",
              "cloudwatch:PutCompositeAlarm",
              "cloudwatch:PutMetricAlarm",
              "cloudwatch:SetAlarmState",
              "cloudwatch:EnableAlarmActions",
              "cloudwatch:DisableAlarmActions",
              "cloudwatch:TagResource",
              "cloudwatch:UntagResource",
            ]
            Resource = [
              "arn:aws:cloudwatch:${var.region}:${data.aws_caller_identity.main.account_id}:alarm:integrates*"
            ]
          },
          {
            Sid    = "cloudwatchWriteWildcard"
            Effect = "Allow"
            Action = [
              "cloudwatch:PutDashboard",
              "cloudwatch:PutMetricData",

            ]
            Resource = ["*"]
          },
          {
            Sid    = "backupWrite"
            Effect = "Allow"
            Action = [
              "backup:*",
            ]
            Resource = ["*"]
          },
          {
            Sid    = "backupStorageWrite"
            Effect = "Allow"
            Action = [
              "backup-storage:CommitBackupJob",
              "backup-storage:DeleteObjects",
              "backup-storage:DescribeBackupJob",
              "backup-storage:GetBaseBackup",
              "backup-storage:GetChunk",
              "backup-storage:GetIncrementalBaseBackup",
              "backup-storage:GetObjectMetadata",
              "backup-storage:ListChunks",
              "backup-storage:ListObjects",
              "backup-storage:MountCapsule",
              "backup-storage:NotifyObjectComplete",
              "backup-storage:PutChunk",
              "backup-storage:PutObject",
              "backup-storage:StartObject",
              "backup-storage:UpdateObjectComplete",
            ]
            Resource = ["*"]
          },
          {
            Sid    = "lambdaRead1"
            Effect = "Allow"
            Action = [
              "lambda:GetAccountSettings",
              "lambda:ListCodeSigningConfigs",
              "lambda:ListEventSourceMappings",
              "lambda:ListFunctions",
              "lambda:ListLayerVersions",
              "lambda:ListLayers",
            ]
            Resource = ["*"]
          },
          {
            Sid    = "lambdaRead2"
            Effect = "Allow"
            Action = [
              "lambda:GetAlias",
              "lambda:Get*Config",
              "lambda:GetEventSourceMapping",
              "lambda:GetFunction*",
              "lambda:GetLayer*",
              "lambda:GetPolicy",
              "lambda:ListAliases",
              "lambda:ListFunction*Config*",
              "lambda:ListProvisionedConcurrencyConfigs",
              "lambda:ListTags",
              "lambda:ListVersionsByFunction",
            ]
            Resource = [
              "*"
            ]
          },
          {
            Sid    = "lambdaWrite"
            Effect = "Allow"
            Action = ["*"]
            Resource = [
              "arn:aws:lambda:${var.region}:${data.aws_caller_identity.main.account_id}:function:integrates*",
              "arn:aws:lambda:${var.region}:${data.aws_caller_identity.main.account_id}:layer:integrates*"
            ]
          },
          {
            Sid    = "lambdaCreateEventSourceMapping"
            Effect = "Allow"
            Action = [
              "lambda:CreateEventSourceMapping",
            ]
            Resource = ["*"]
          },
          {
            Sid    = "lambdaGlobalWrite"
            Effect = "Allow"
            Action = [
              "lambda:DeleteEventSourceMapping",
              "lambda:UpdateEventSourceMapping"
            ]
            Resource = ["arn:aws:lambda:${var.region}:${data.aws_caller_identity.main.account_id}:event-source-mapping:*"]
          },
          {
            Sid    = "SNSAll"
            Effect = "Allow"
            Action = ["*"]
            Resource = [
              "arn:aws:sns:${var.region}:${data.aws_caller_identity.main.account_id}:integrates*",
              "arn:aws:sns:${var.region}:${data.aws_caller_identity.main.account_id}:rds-cpu-utilization-alarms"
            ]
          },
          {
            Sid    = "sqsAll"
            Effect = "Allow"
            Action = [
              "sqs:*",
            ]
            Resource = [
              "arn:aws:sqs:${var.region}:${data.aws_caller_identity.main.account_id}:integrates_*",
            ]
          },
          {
            Sid    = "sqsMarketplace"
            Effect = "Allow"
            Action = [
              "sqs:DeleteMessage",
              "sqs:ReceiveMessage"
            ]
            Resource = [
              "arn:aws:sqs:us-east-1:993047037389:MarketplaceUpdates"
            ]
          },
          {
            Sid    = "XRay"
            Effect = "Allow"
            Action = [
              "xray:PutTelemetryRecords",
              "xray:PutTraceSegments"
            ]
            Resource = ["*"]
          },
          {
            Sid    = "Bedrock"
            Effect = "Allow"
            Action = [
              "bedrock:InvokeModel",
              "bedrock:InvokeModelWithResponseStream"
            ]
            Resource = [
              "arn:aws:bedrock:${var.region}::foundation-model/*",
              "arn:aws:bedrock:${var.inference_midpoint_region}::foundation-model/*",
              "arn:aws:bedrock:${var.inference_west_region}::foundation-model/*",
              "arn:aws:bedrock:${var.region}:${data.aws_caller_identity.main.account_id}:provisioned-model/*",
              "arn:aws:bedrock:${var.region}:${data.aws_caller_identity.main.account_id}:inference-profile/*"
            ]
          },
          {
            Sid    = "legacyPolicy"
            Effect = "Allow"
            Action = [
              "aws-portal:ViewBilling",
              "aws-portal:ViewUsage",
            ]
            Resource = ["*"]
          },
        ]
        IntegratesDbPolicy = [
          {
            Sid    = "opensearchRead1"
            Effect = "Allow"
            Action = [
              "es:DescribeElasticsearchInstanceTypeLimits",
              "es:DescribeInboundConnections",
              "es:DescribeInboundCrossClusterSearchConnections",
              "es:DescribeInstanceTypeLimits",
              "es:DescribeOutboundConnections",
              "es:DescribeOutboundCrossClusterSearchConnections",
              "es:DescribePackages",
              "es:DescribeReservedElasticsearchInstanceOfferings",
              "es:DescribeReservedElasticsearchInstances",
              "es:DescribeReservedInstanceOfferings",
              "es:DescribeReservedInstances",
              "es:DescribeVpcEndpoints",
              "es:GetPackageVersionHistory",
              "es:ListDomainNames",
              "es:ListDomainsForPackage",
              "es:ListElasticsearchInstanceTypeDetails",
              "es:ListElasticsearchInstanceTypes",
              "es:ListElasticsearchVersions",
              "es:ListInstanceTypeDetails",
              "es:ListVersions",
              "es:ListVpcEndpointAccess",
              "es:ListVpcEndpoints",
              "es:ListVpcEndpointsForDomain",
            ]
            Resource = ["*"]
          },
          {
            Sid    = "opensearchRead2"
            Effect = "Allow"
            Action = [
              "es:Describe*Domain*",
              "es:DescribeDryRunProgress",
              "es:GetCompatible*",
              "es:GetDataSource",
              "es:GetDomainMaintenanceStatus",
              "es:GetUpgrade*",
              "es:ListDataSources",
              "es:ListDomainMaintenances",
              "es:ListPackagesForDomain",
              "es:ListScheduledActions",
              "es:ListTags",
            ]
            Resource = ["arn:aws:es:${var.region}:${data.aws_caller_identity.main.account_id}:domain/*"]
          },
          {
            Sid      = "opensearchServerlessWrite"
            Effect   = "Allow"
            Action   = ["aoss:*"]
            Resource = ["*"]
          },
          {
            Sid    = "route53"
            Effect = "Allow"
            Action = [
              "route53:AssociateVPCWithHostedZone",
              "route53:ChangeResourceRecordSets",
              "route53:CreateHostedZone",
              "route53:DeleteHostedZone",
              "route53:GetChange",
              "route53:GetHostedZone",
              "route53:ListHostedZonesByName",
              "route53:ListHostedZonesByVPC",
              "route53:ListResourceRecordSets",
            ]
            Resource = ["*"]
          },
          {
            Sid    = "aurora"
            Effect = "Allow"
            Action = [
              "rds-db:connect",
              "rds:AddTagsToResource",
              "rds:Create*",
              "rds:Delete*",
              "rds:Describe*",
              "rds:ListTagsForResource",
              "rds:Modify*",
              "rds:RemoveTagsFromResource",
            ]
            Resource = ["*"]
          },
          {
            Sid    = "cloudwatch"
            Effect = "Allow"
            Action = [
              "cloudwatch:PutMetricAlarm",
              "cloudwatch:DeleteAlarms",
            ]
            Resource = "arn:aws:cloudwatch:${var.region}:${data.aws_caller_identity.main.account_id}:alarm:*"
          }
        ],
        IntegratesAssumePolicy = [
          {
            Sid    = "CSPMAssumeRole"
            Effect = "Allow"
            Action = [
              "sts:AssumeRole"
            ]
            Resource = [
              "*" # allow to assume any role but only if the assume policy allows it
            ]
          }
        ]
        IntegratesIamReadPolicy = [
          {
            Sid    = "iamRead1"
            Effect = "Allow"
            Action = [
              "iam:GetAccountAuthorizationDetails",
              "iam:GetAccountEmailAddress",
              "iam:GetAccountName",
              "iam:GetAccountPasswordPolicy",
              "iam:GetAccountSummary",
              "iam:GetCloudFrontPublicKey",
              "iam:GetContextKeysForCustomPolicy",
              "iam:GetCredentialReport",
              "iam:GetOrganizationsAccessReport",
              "iam:GetServiceLastAccessedDetails",
              "iam:GetServiceLastAccessedDetailsWithEntities",
              "iam:ListAccountAliases",
              "iam:ListCloudFrontPublicKeys",
              "iam:ListGroups",
              "iam:ListInstanceProfiles",
              "iam:ListOpenIDConnectProviders",
              "iam:ListPolicies",
              "iam:ListRoles",
              "iam:ListSAMLProviders",
              "iam:ListSTSRegionalEndpointsStatus",
              "iam:ListServerCertificates",
              "iam:ListUsers",
              "iam:ListVirtualMFADevices",
            ]
            Resource = ["*"]
          },
          {
            Sid    = "iamRead2"
            Effect = "Allow"
            Action = [
              "iam:GetAccessKeyLastUsed",
              "iam:GetContextKeysForPrincipalPolicy",
              "iam:GetGroup*",
              "iam:Get*Profile",
              "iam:GetMFADevice",
              "iam:GetOpenIDConnectProvider",
              "iam:GetPolicy*",
              "iam:GetRole*",
              "iam:GetSAMLProvider",
              "iam:GetSSHPublicKey",
              "iam:GetServerCertificate",
              "iam:GetServiceLinkedRoleDeletionStatus",
              "iam:GetUser*",
              "iam:ListAccessKeys",
              "iam:ListAttached*",
              "iam:ListEntitiesForPolicy",
              "iam:ListGroupPolicies",
              "iam:ListGroupsForUser",
              "iam:ListInstanceProfileTags",
              "iam:ListInstanceProfilesForRole",
              "iam:ListMFA*",
              "iam:ListOpenIDConnectProviderTags",
              "iam:ListPoliciesGrantingServiceAccess",
              "iam:ListPolicyTags",
              "iam:ListPolicyVersions",
              "iam:ListRolePolicies",
              "iam:ListRoleTags",
              "iam:ListSAMLProviderTags",
              "iam:ListSSHPublicKeys",
              "iam:ListServerCertificateTags",
              "iam:ListServiceSpecificCredentials",
              "iam:ListSigningCertificates",
              "iam:ListUserPolicies",
              "iam:ListUserTags",
            ]
            Resource = [
              "arn:aws:iam::${var.accountId}:group/*",
              "arn:aws:iam::${var.accountId}:instance-profile/*",
              "arn:aws:iam::${var.accountId}:mfa/*",
              "arn:aws:iam::${var.accountId}:oidc-provider/*",
              "arn:aws:iam::${var.accountId}:policy/*",
              "arn:aws:iam::${var.accountId}:role/*",
              "arn:aws:iam::${var.accountId}:saml-provider/*",
              "arn:aws:iam::${var.accountId}:server-certificate/*",
              "arn:aws:iam::${var.accountId}:user/*"
            ]
          },

          {
            Sid    = "elasticacheRead"
            Effect = "Allow"
            Action = [
              "elasticache:DescribeCacheEngineVersions",
              "elasticache:DescribeEngineDefaultParameters",
              "elasticache:DescribeEvents",
              "elasticache:DescribeReservedCacheNodesOfferings",
              "elasticache:DescribeServiceUpdates",
            ]
            Resource = ["*"]
          },
          {
            Sid    = "elasticache"
            Effect = "Allow"
            Action = [
              "elasticache:DescribeCacheClusters",
              "elasticache:DescribeCacheParameterGroups",
              "elasticache:DescribeCacheParameters",
              "elasticache:DescribeCacheSecurityGroups",
              "elasticache:DescribeCacheSubnetGroups",
              "elasticache:DescribeGlobalReplicationGroups",
              "elasticache:DescribeReplicationGroups",
              "elasticache:DescribeReservedCacheNodes",
              "elasticache:DescribeServerlessCacheSnapshots",
              "elasticache:DescribeServerlessCaches",
              "elasticache:DescribeSnapshots",
              "elasticache:DescribeUpdateActions",
              "elasticache:DescribeUserGroups",
              "elasticache:DescribeUsers",
              "elasticache:ListAllowedNodeTypeModifications",
              "elasticache:ListTagsForResource",
              "elasticache:CreateReplicationGroup",
              "elasticache:CreateCacheSecurityGroup",
              "elasticache:CreateCacheSubnetGroup",
              "elasticache:AddTagsToResource",
            ]
            Resource = [
              "arn:aws:elasticache::${var.accountId}:globalreplicationgroup:*",
              "arn:aws:elasticache:${var.region}:${var.accountId}:*",
            ]
          },
          {
            Sid    = "cloudwatchRead1"
            Effect = "Allow"
            Action = [
              "cloudwatch:DescribeAlarmsForMetric",
              "cloudwatch:DescribeAnomalyDetectors",
              "cloudwatch:DescribeInsightRules",
              "cloudwatch:GetMetricData",
              "cloudwatch:GetMetricStatistics",
              "cloudwatch:GetMetricWidgetImage",
              "cloudwatch:GetTopologyDiscoveryStatus",
              "cloudwatch:GetTopologyMap",
              "cloudwatch:ListDashboards",
              "cloudwatch:ListManagedInsightRules",
              "cloudwatch:ListMetricStreams",
              "cloudwatch:ListMetrics",
              "cloudwatch:ListServiceLevelObjectives",
              "cloudwatch:ListServices",
            ]
            Resource = ["*"]
          },
          {
            Sid    = "cloudwatchRead2"
            Effect = "Allow"
            Action = [
              "cloudwatch:DescribeAlarmHistory",
              "cloudwatch:DescribeAlarms",
              "cloudwatch:GetDashboard",
              "cloudwatch:GetInsightRuleReport",
              "cloudwatch:GetMetricStream",
              "cloudwatch:GetService*",
              "cloudwatch:ListTagsForResource",
            ]
            Resource = [
              "arn:aws:cloudwatch:${var.region}:${data.aws_caller_identity.main.account_id}:*",
              "arn:aws:cloudwatch::${data.aws_caller_identity.main.account_id}:dashboard/*",
            ]
          },

        ]
      }

      cloudflare = {
        account = {
          effect = "allow"
          permission_groups = [
            data.cloudflare_api_token_permission_groups.all.account["Argo Tunnel Read"],
          ]
          resources = {
            "com.cloudflare.api.account.*" = "*"
          }
        }
        accountZone = {
          effect = "allow"
          permission_groups = [
            data.cloudflare_api_token_permission_groups.all.zone["Zone Read"],
            data.cloudflare_api_token_permission_groups.all.zone["Cache Purge"],
            data.cloudflare_api_token_permission_groups.all.zone["Page Rules Write"],
            data.cloudflare_api_token_permission_groups.all.zone["Firewall Services Write"],
            data.cloudflare_api_token_permission_groups.all.zone["DNS Write"],
          ]
          resources = {
            "com.cloudflare.api.account.zone.*" = "*"
          }
        }
      }
    }

    keys = {
      prod_integrates = {
        admins = [
          "prod_common",
        ]
        read_users = []
        users = [
          "debug_integrates",
          "prod_integrates"
        ]
        tags = {
          "Name"              = "prod_integrates"
          "fluidattacks:line" = "cost"
          "fluidattacks:comp" = "integrates"
        }
      }
    }
  }
}

module "prod_integrates_aws" {
  source = "./modules/aws"

  name     = "prod_integrates"
  policies = local.prod_integrates.policies.aws

  tags = {
    "Name"              = "prod_integrates"
    "fluidattacks:line" = "cost"
    "fluidattacks:comp" = "integrates"
    "NOFLUID"           = "f325_wildcards_in_actions_are_not_dangerous"
  }
}

module "prod_integrates_keys" {
  source   = "./modules/key"
  for_each = local.prod_integrates.keys

  name       = each.key
  admins     = each.value.admins
  read_users = each.value.read_users
  users      = each.value.users
  tags       = each.value.tags
}

module "prod_integrates_cloudflare" {
  source = "./modules/cloudflare"

  name   = "prod_integrates"
  policy = local.prod_integrates.policies.cloudflare
}

output "prod_integrates_cloudflare_api_token" {
  sensitive = true
  value     = module.prod_integrates_cloudflare.cloudflare_api_token
}
