locals {
  analytics = {
    policies = {
      aws = {
        AnalyticsPolicy = [
          {
            Sid      = "AllowCreateReader"
            Effect   = "Allow"
            Action   = ["quicksight:CreateReader"]
            Resource = ["arn:aws:quicksight:*:${data.aws_caller_identity.main.account_id}:user/$${aws:userid}"]
          },
        ]
      }
    }
  }
}

module "analytics" {
  source = "./modules/aws"

  name     = "analytics"
  policies = local.analytics.policies.aws
  tags = {
    "Name"              = "analytics"
    "fluidattacks:line" = "cost"
    "fluidattacks:comp" = "etl"
  }
}
