locals {
  prod_sorts = {
    policies = {
      aws = {
        SortsPolicy = [
          {
            Sid    = "readBill"
            Effect = "Allow",
            Action = [
              "billing:Get*",
              "billing:List*",
            ]
            Resource = ["*"]
          },
          {
            Sid    = "taxList"
            Effect = "Allow"
            Action = [
              "account:Get*",
              "ce:Describe*",
              "ce:Get*",
              "ce:List*",
              "consolidatedbilling:Get*",
              "consolidatedbilling:List*",
              "cur:Describe*",
              "cur:Get*",
              "cur:Validate*",
              "freetier:Get*",
              "invoicing:Get*",
              "invoicing:List*",
              "payments:Get*",
              "payments:List*",
              "pricing:Describe*",
              "pricing:Get*",
              "tax:Get*",
              "tax:List*",
            ]
            Resource = ["*"]
          },
          {
            Sid    = "dynamoRead"
            Effect = "Allow"
            Action = [
              "dynamodb:BatchGet*",
              "dynamodb:Describe*",
              "dynamodb:Get*",
              "dynamodb:List*",
              "dynamodb:Query*",
              "dynamodb:Scan*",
            ]
            Resource = ["arn:aws:dynamodb:*:205810638802:table/*"]
          },
          {
            Sid    = "batchRead1"
            Effect = "Allow"
            Action = [
              "batch:DescribeComputeEnvironments",
              "batch:DescribeJobDefinitions",
              "batch:DescribeJobQueues",
              "batch:DescribeJobs",
              "batch:DescribeSchedulingPolicies",
              "batch:ListJobs",
              "batch:ListSchedulingPolicies",
            ]
            Resource = ["*"]
          },
          {
            Sid    = "batchRead2"
            Effect = "Allow"
            Action = [
              "batch:ListTagsForResource",
            ]
            Resource = [
              "arn:aws:batch:${var.region}:${data.aws_caller_identity.main.account_id}:compute-environment/*",
              "arn:aws:batch:${var.region}:${data.aws_caller_identity.main.account_id}:job/*",
              "arn:aws:batch:${var.region}:${data.aws_caller_identity.main.account_id}:job-definition/*:*",
              "arn:aws:batch:${var.region}:${data.aws_caller_identity.main.account_id}:job-queue/*",
              "arn:aws:batch:${var.region}:${data.aws_caller_identity.main.account_id}:scheduling-policy/*"
            ]
          },
          {
            Sid    = "batchTags"
            Effect = "Allow"
            Action = [
              "batch:TagResource",
              "batch:UntagResource",
            ]
            Resource = [
              "arn:aws:batch:us-east-1:${data.aws_caller_identity.main.account_id}:job-queue/*",
              "arn:aws:batch:us-east-1:${data.aws_caller_identity.main.account_id}:job-definition/*",
              "arn:aws:batch:us-east-1:${data.aws_caller_identity.main.account_id}:job/*",
            ]
            "Condition" : {
              "StringEquals" : {
                "aws:RequestTag/fluidattacks:comp" : "sorts",
              }
            }
          },
          {
            Sid    = "batchCancel"
            Effect = "Allow"
            Action = [
              "batch:CancelJob",
              "batch:TerminateJob",
            ]
            Resource = [
              "arn:aws:batch:us-east-1:${data.aws_caller_identity.main.account_id}:job/*",
            ]
            # Since for scheduled jobs the tag does not propagate
            # the role cannot terminate them. Uncomment the condition
            # when the tag gets propagated.
            # "Condition" : { "StringEquals" : { "aws:ResourceTag/fluidattacks:comp" : "sorts" } }
          },
          {
            Sid    = "batchSubmit"
            Effect = "Allow"
            Action = [
              "batch:SubmitJob",
            ]
            Resource = [
              "arn:aws:batch:us-east-1:${data.aws_caller_identity.main.account_id}:job-definition/*",
              "arn:aws:batch:us-east-1:${data.aws_caller_identity.main.account_id}:job-queue/*",
            ]
            "Condition" : { "StringEquals" : { "aws:RequestTag/fluidattacks:comp" : "sorts" } }
          },
          {
            Sid    = "cloudwatchRead1"
            Effect = "Allow"
            Action = [
              "cloudwatch:DescribeAlarmsForMetric",
              "cloudwatch:DescribeAnomalyDetectors",
              "cloudwatch:DescribeInsightRules",
              "cloudwatch:GetMetricData",
              "cloudwatch:GetMetricStatistics",
              "cloudwatch:GetMetricWidgetImage",
              "cloudwatch:GetTopologyDiscoveryStatus",
              "cloudwatch:GetTopologyMap",
              "cloudwatch:ListDashboards",
              "cloudwatch:ListManagedInsightRules",
              "cloudwatch:ListMetricStreams",
              "cloudwatch:ListMetrics",
              "cloudwatch:ListServiceLevelObjectives",
              "cloudwatch:ListServices",
            ]
            Resource = ["*"]
          },
          {
            Sid    = "cloudwatchRead2"
            Effect = "Allow"
            Action = [
              "cloudwatch:DescribeAlarmHistory",
              "cloudwatch:DescribeAlarms",
              "cloudwatch:GetDashboard",
              "cloudwatch:GetInsightRuleReport",
              "cloudwatch:GetMetricStream",
              "cloudwatch:GetService*",
              "cloudwatch:ListTagsForResource",
            ]
            Resource = [
              "arn:aws:cloudwatch:${var.region}:${data.aws_caller_identity.main.account_id}:alarm:",
              "arn:aws:cloudwatch:${var.region}:${data.aws_caller_identity.main.account_id}:insight-rule/*",
              "arn:aws:cloudwatch:${var.region}:${data.aws_caller_identity.main.account_id}:metric-stream/*",
              "arn:aws:cloudwatch:${var.region}:${data.aws_caller_identity.main.account_id}:service/*-*",
              "arn:aws:cloudwatch:${var.region}:${data.aws_caller_identity.main.account_id}:slo/*",
              "arn:aws:cloudwatch::${data.aws_caller_identity.main.account_id}:dashboard/*",
            ]
          },
          {
            Sid    = "ec2Read"
            Effect = "Allow"
            Action = [
              "ec2:DescribeVpcEndpoints",
              "ec2:DescribeDhcpOptions",
              "ec2:DescribeVpcs",
              "ec2:DescribeSubnets",
              "ec2:DescribeSecurityGroups",
              "ec2:DescribeNetworkInterfaces",
              "ec2:DeleteNetworkInterfacePermission",
              "ec2:DeleteNetworkInterface",
              "ec2:CreateNetworkInterfacePermission",
              "ec2:CreateNetworkInterface",
            ]
            Resource = ["arn:aws:ec2:*:205810638802:*"]
          },
          {
            Sid    = "logsRead"
            Effect = "Allow"
            Action = [
              "logs:CreateLogGroup",
              "logs:Describe*",
              "logs:Filter*",
              "logs:Get*",
              "logs:List*",
            ]
            Resource = ["arn:aws:logs:*:205810638802:*"]
          },
          {
            Sid    = "logsWrite"
            Effect = "Allow"
            Action = ["logs:*"]
            Resource = [
              "arn:aws:logs:${var.region}:${data.aws_caller_identity.main.account_id}:log-group:/aws/sagemaker/TrainingJobs",
              "arn:aws:logs:${var.region}:${data.aws_caller_identity.main.account_id}:log-group:/aws/sagemaker/TrainingJobs:log-stream:*",
            ]
          },
          {
            Sid      = "ListObjectsInTfStatesProd",
            Effect   = "Allow",
            Action   = ["s3:ListBucket"],
            Resource = ["arn:aws:s3:::fluidattacks-terraform-states-prod"]
          },
          {
            Sid    = "s3ReadRepos"
            Effect = "Allow"
            Action = [
              "s3:ListBucket",
              "s3:ListObjectsV2",
              "s3:Get*",
            ]
            Resource = [
              "arn:aws:s3:::integrates.continuous-repositories",
              "arn:aws:s3:::integrates.continuous-repositories/*",
            ]
          },
          {
            Sid    = "s3Write"
            Effect = "Allow"
            Action = ["s3:*"]
            Resource = [
              "arn:aws:s3:::fluidattacks.public.storage",
              "arn:aws:s3:::fluidattacks.public.storage/*",
              "arn:aws:s3:::fluidattacks-terraform-states-prod/sorts*",
              "arn:aws:s3:::sorts",
              "arn:aws:s3:::sorts/*",
              "arn:aws:s3:::business-models",
              "arn:aws:s3:::business-models/*"
            ]
          },
          {
            Sid    = "legacyPolicy"
            Effect = "Allow"
            Action = [
              "aws-portal:View*",
            ]
            Resource = ["arn:aws:aws-portal:*:205810638802:*"]
          },
          {
            Sid    = "IAMRead1"
            Effect = "Allow"
            Action = [
              "iam:ListRoles",
            ]
            Resource = ["*"]
          },
        ]
        SortsPolicySageMaker = [
          {
            Sid    = "sagemakerRead1"
            Effect = "Allow"
            Action = [
              "sagemaker:ListActions",
              "sagemaker:ListAlgorithms",
              "sagemaker:ListAppImageConfigs",
              "sagemaker:ListApps",
              "sagemaker:ListArtifacts",
              "sagemaker:ListAssociations",
              "sagemaker:ListAutoMLJobs",
              "sagemaker:ListCandidatesForAutoMLJob",
              "sagemaker:ListClusters",
              "sagemaker:ListCodeRepositories",
              "sagemaker:ListCompilationJobs",
              "sagemaker:ListContexts",
              "sagemaker:ListDataQualityJobDefinitions",
              "sagemaker:ListDeviceFleets",
              "sagemaker:ListDevices",
              "sagemaker:ListDomains",
              "sagemaker:ListEdgeDeploymentPlans",
              "sagemaker:ListEdgePackagingJobs",
              "sagemaker:ListEndpointConfigs",
              "sagemaker:ListEndpoints",
              "sagemaker:ListExperiments",
              "sagemaker:ListFeatureGroups",
              "sagemaker:ListFlowDefinitions",
              "sagemaker:ListHubs",
              "sagemaker:ListHumanLoops",
              "sagemaker:ListHumanTaskUis",
              "sagemaker:ListHyperParameterTuningJobs",
              "sagemaker:ListImages",
              "sagemaker:ListInferenceComponents",
              "sagemaker:ListInferenceExperiments",
              "sagemaker:ListInferenceRecommendationsJobSteps",
              "sagemaker:ListInferenceRecommendationsJobs",
              "sagemaker:ListLabelingJobs",
              "sagemaker:ListLineageGroups",
              "sagemaker:ListModelBiasJobDefinitions",
              "sagemaker:ListModelCards",
              "sagemaker:ListModelExplainabilityJobDefinitions",
              "sagemaker:ListModelMetadata",
              "sagemaker:ListModelPackageGroups",
              "sagemaker:ListModelQualityJobDefinitions",
              "sagemaker:ListModels",
              "sagemaker:ListMonitoringAlertHistory",
              "sagemaker:ListMonitoringAlerts",
              "sagemaker:ListMonitoringExecutions",
              "sagemaker:ListMonitoringSchedules",
              "sagemaker:ListNotebookInstanceLifecycleConfigs",
              "sagemaker:ListNotebookInstances",
              "sagemaker:ListPipelines",
              "sagemaker:ListProcessingJobs",
              "sagemaker:ListProjects",
              "sagemaker:ListResourceCatalogs",
              "sagemaker:ListSharedModelEvents",
              "sagemaker:ListSharedModels",
              "sagemaker:ListSpaces",
              "sagemaker:ListStageDevices",
              "sagemaker:ListStudioLifecycleConfigs",
              "sagemaker:ListSubscribedWorkteams",
              "sagemaker:ListTrainingJobs",
              "sagemaker:ListTransformJobs",
              "sagemaker:ListTrialComponents",
              "sagemaker:ListTrials",
              "sagemaker:ListUserProfiles",
              "sagemaker:ListWorkforces",
              "sagemaker:ListWorkteams",
            ]
            Resource = ["*"]
          },
          {
            Sid    = "sagemakerRead2"
            Effect = "Allow"
            Action = [
              "sagemaker:ListAliases",
              "sagemaker:ListClusterNodes",
              "sagemaker:ListHubContentVersions",
              "sagemaker:ListHubContents",
              "sagemaker:ListImageVersions",
              "sagemaker:ListLabelingJobsForWorkteam",
              "sagemaker:ListModelCardExportJobs",
              "sagemaker:ListModelCardVersions",
              "sagemaker:ListModelPackages",
              "sagemaker:ListPipelineExecutionSteps",
              "sagemaker:ListPipelineExecutions",
              "sagemaker:ListPipelineParametersForExecution",
              "sagemaker:ListSharedModelVersions",
              "sagemaker:ListTags",
              "sagemaker:ListTrainingJobsForHyperParameterTuningJob",
            ]
            Resource = ["arn:aws:sagemaker:${var.region}:${data.aws_caller_identity.main.account_id}:*"]
          },
          {
            Sid    = "sagemakerWrite"
            Effect = "Allow"
            Action = ["sagemaker:*"]
            Resource = [
              "arn:aws:sagemaker:${var.region}:${data.aws_caller_identity.main.account_id}:domain/*",
              "arn:aws:sagemaker:${var.region}:${data.aws_caller_identity.main.account_id}:hyper-parameter-tuning-job/*",
              "arn:aws:sagemaker:${var.region}:${data.aws_caller_identity.main.account_id}:notebook-instance/*",
              "arn:aws:sagemaker:${var.region}:${data.aws_caller_identity.main.account_id}:training-job/*",
            ]
          },
          {
            Sid    = "sortsEcrRead"
            Effect = "Allow"
            Action = [
              "ecr:Describe*",
              "ecr:Get*",
              "ecr:List*",
            ]
            Resource = ["*"]
          },
          {
            Sid    = "sortsEcrWrite"
            Effect = "Allow"
            Action = ["ecr:*"]
            Resource = [
              "arn:aws:ecr:${var.region}:${data.aws_caller_identity.main.account_id}:repository/sorts",
            ]
          },
          {
            Sid    = "dynamoWrite"
            Effect = "Allow"
            Action = [
              "dynamodb:DeleteItem",
              "dynamodb:GetItem",
              "dynamodb:PutItem",
            ]
            Resource = [
              var.terraform_state_lock_arn,
            ]
          },

        ]
      }
    }

    keys = {
      prod_sorts = {
        admins = [
          "prod_common",
        ]
        read_users = []
        users = [
          "prod_sorts",
        ]
        tags = {
          "Name"              = "prod_sorts"
          "fluidattacks:line" = "cost"
          "fluidattacks:comp" = "sorts"
        }
      }
    }
  }
}

module "prod_sorts_aws" {
  source = "./modules/aws"

  name     = "prod_sorts"
  policies = local.prod_sorts.policies.aws

  assume_role_policy = [
    {
      Sid    = "SageMakerAssumeRolePolicy",
      Effect = "Allow",
      Principal = {
        Service = "sagemaker.amazonaws.com",
      },
      Action = "sts:AssumeRole",
    },
  ]

  tags = {
    "Name"              = "prod_sorts"
    "fluidattacks:line" = "cost"
    "fluidattacks:comp" = "sorts"
    "NOFLUID"           = "f325_wildcard_in_sagemaker_is_not_dangerous"
  }
}

module "prod_sorts_keys" {
  source   = "./modules/key"
  for_each = local.prod_sorts.keys

  name       = each.key
  admins     = each.value.admins
  read_users = each.value.read_users
  users      = each.value.users
  tags       = each.value.tags
}
