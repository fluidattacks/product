locals {
  dev = {
    policies = {
      aws = {
        DevPolicy = [
          {
            Sid    = "eventsTest"
            Effect = "Allow"
            Action = [
              "events:TestEventPattern",
            ]
            Resource = [
              "arn:aws:events:${var.region}:${var.accountId}:event/*",
            ]
          },
          {
            Sid    = "tags"
            Effect = "Allow"
            Action = [
              "tag:GetResources",
            ]
            Resource = [
              "*",
            ]
          },
          {
            Sid    = "readBillAllResources"
            Effect = "Allow"
            Action = [
              "billing:Get*",
              "billing:List*",
              "ce:Describe*",
              "ce:Get*",
              "ce:List*",
              "consolidatedbilling:Get*",
              "consolidatedbilling:List*",
              "cur:Describe*",
              "cur:Get*",
              "cur:Validate*",
              "invoicing:Get*",
              "invoicing:List*",
              "payments:Get*",
              "payments:List*",
              "pricing:Describe*",
              "pricing:Get*",
              "rbin:GetRule",
              "rbin:ListTagsForResource",
              "tax:Get*",
              "tax:List*",
            ]
            Resource = ["*"]
          },
          {
            Sid    = "decryptCiKms"
            Effect = "Allow"
            Action = [
              "kms:Decrypt",
            ]
            Resource = ["arn:aws:kms:${var.region}:${var.accountId}:key/*"]
            Condition = {
              Null = {
                "kms:ResourceAliases" : "true"
              },
            },
          },
          {
            Sid    = "readBill"
            Effect = "Allow"
            Action = [
              "account:Get*"
            ]
            Resource = [
              "arn:aws:account::${var.accountId}:*",
            ]
          },
          {
            Sid    = "legacyPolicy"
            Effect = "Allow"
            Action = [
              "aws-portal:ViewBilling",
              "aws-portal:ViewUsage",
            ]
            Resource = ["arn:aws:aws-portal:${var.region}:${var.accountId}:*"]
          },
          {
            Sid    = "readAllResources"
            Effect = "Allow"
            Action = [
              "access-analyzer:Get*",
              "access-analyzer:List*",
              "access-analyzer:Validate*",
              "acm:Describe*",
              "acm:Get*",
              "acm:List*",
              "aoss:BatchGet*",
              "aoss:Get*",
              "aoss:List*",
              "application-autoscaling:Describe*",
              "aps:List*",
              "athena:List*",
              "autoscaling:Describe*",
              "autoscaling:Get*",
              "backup:Describe*",
              "backup:Get*",
              "backup:List*",
              "batch:Describe*",
              "batch:List*",
              "cloudformation:Describe*",
              "cloudformation:List*",
              "config:Describe*",
              "config:Get*",
              "config:List*",
              "ec2:Describe*",
              "ec2:Get*",
              "ecr:Describe*",
              "ecr:Get*",
              "ecr-public:GetAuthorizationToken",
              "ecs:Describe*",
              "ecs:List*",
              "eks:Access*",
              "eks:List*",
              "eks:Describe*",
              "elasticache:Describe*",
              "elasticache:List*",
              "elasticloadbalancing:Describe*",
              "es:Describe*",
              "es:Get*",
              "es:List*",
              "events:List*",
              "events:Describe*",
              "firehose:List*",
              "freetier:Get*",
              "fis:Get*",
              "fis:List*",
              "glue:Get*",
              "glue:List*",
              "glue:SearchTables",
              "guardduty:Describe*",
              "guardduty:Get*",
              "guardduty:List*",
              "iam:Get*",
              "iam:List*",
              "kms:Describe*",
              "kms:List*",
              "lambda:Get*",
              "lambda:List*",
              "logs:Describe*",
              "logs:Get*",
              "logs:List*",
              "organizations:DescribeOrganization",
              "organizations:ListDelegatedAdministrators",
              "organizations:ListAWSServiceAccessForOrganization",
              "resiliencehub:Describe*",
              "resiliencehub:List*",
              "route53:Get*",
              "route53:List*",
              "route53-recovery-control-config:List*",
              "route53-recovery-readiness:List*",
              "route53domains:Get*",
              "route53domains:List*",
              "route53resolver:List*",
              "securityhub:Describe*",
              "securityhub:Get*",
              "securityhub:List*",
              "serverlessrepo:List*",
              "sns:Get*",
              "sns:List*",
              "ssm:Describe*",
              "ssm:Get*",
              "ssm:List*",
              "sts:Decode*",
              "sts:Get*",
              "s3:Get*",
              "s3:List*",
              "apigateway:GET",
              "states:Describe*",
              "states:List*",
              "states:Validate*"
            ]
            Resource = ["*"]
          },
          {
            Sid    = "dynamodbLock"
            Effect = "Allow"
            Action = [
              "dynamodb:DeleteItem",
              "dynamodb:GetItem",
              "dynamodb:PutItem",
            ]
            Resource = ["arn:aws:dynamodb:${var.region}:${var.accountId}:table/terraform_state_lock"]
          },
          {
            Sid      = "dynamodbList"
            Effect   = "Allow"
            Action   = ["dynamodb:ListTables"]
            Resource = ["arn:aws:dynamodb:${var.region}:${var.accountId}:table/*"]
          },
          {
            Sid    = "dynamodbReadSkims"
            Effect = "Allow"
            Action = [
              "dynamodb:Get*",
              "dynamodb:ListTagsOfResource",
              "dynamodb:Scan",
              "dynamodb:Query",
            ]
            Resource = ["arn:aws:dynamodb:${var.region}:${var.accountId}:table/skims*"]
          },
          {
            Sid    = "sqsList"
            Effect = "Allow"
            Action = [
              "sqs:ListQueues"
            ]
            Resource = ["arn:aws:sqs:*:${var.accountId}:*"]
          },
          {
            Sid    = "sqsRead"
            Effect = "Allow"
            Action = [
              "sqs:GetQueueUrl",
              "sqs:GetQueueAttributes",
              "sqs:ListQueueTags",
            ]
            Resource = [
              "arn:aws:sqs:${var.region}:${var.accountId}:integrates_*",
            ]
          },
          {
            Sid    = "codeCommitAssumeRol"
            Effect = "Allow"
            Action = [
              "sts:AssumeRole"
            ]
            Resource = [
              "*"
            ]
          },
        ]
        DevPolicy2 = [
          {
            Sid    = "ecrRead"
            Effect = "Allow"
            Action = [
              "ecr-public:Describe*",
              "ecr-public:Get*",
            ]
            Resource = [
              "arn:aws:ecr-public::${var.accountId}:repository/*",
            ]
          },
          {
            Sid    = "cloudTrailRead1"
            Effect = "Allow"
            Action = [
              "cloudtrail:GetImport",
              "cloudtrail:DescribeTrails",
              "cloudtrail:ListEventDataStores",
              "cloudtrail:ListImports",
              "cloudtrail:ListImportFailures",
              "cloudtrail:ListPublicKeys",
              "cloudtrail:ListChannels",
              "cloudtrail:ListServiceLinkedChannels",
              "cloudtrail:ListTrails",
              "cloudtrail:LookupEvents"
            ]
            Resource = [
              "*",
            ]
          },
          {
            Sid    = "cloudTrailRead2"
            Effect = "Allow"
            Action = [
              "cloudtrail:GetEvent*",
              "cloudtrail:GetChannel",
              "cloudtrail:GetInsightSelectors",
              "cloudtrail:GetQueryResults",
              "cloudtrail:GetResourcePolicy",
              "cloudtrail:GetTrail*",
              "cloudtrail:GetServiceLinkedChannel",
              "cloudtrail:DescribeQuery",
              "cloudtrail:ListQueries",
              "cloudtrail:ListTags",
            ]
            Resource = [
              "arn:aws:cloudtrail:${var.region}:${var.accountId}:channel/*",
              "arn:aws:cloudtrail:${var.region}:${var.accountId}:eventdatastore/*",
              "arn:aws:cloudtrail:${var.region}:${var.accountId}:trail/master-cloudtrail",
              "arn:aws:cloudtrail:${var.region}:${var.accountId}:trail/s3-data-events-cloudtrail",
            ]
          },
          {
            Sid    = "ecrRegistry"
            Effect = "Allow"
            Action = [
              "ecr-public:DescribeRegistries",
            ]
            Resource = [
              "arn:aws:ecr-public::${var.accountId}:registry/${var.accountId}",
            ]
          },
          {
            Sid    = "readKinesis"
            Effect = "Allow"
            Action = [
              "kinesis:Describe*",
              "kinesis:List*",
              "kinesis:GetResourcePolicy",
            ]
            Resource = [
              "arn:aws:kinesis:${var.region}:${var.accountId}:stream/*",
            ]
          },
          {
            Sid    = "read"
            Effect = "Allow"
            Action = [
              "aps:Describe*",
              "athena:Get*",
              "cloudformation:Get*",
              "dynamodb:DescribeContinuousBackups",
              "dynamodb:DescribeKinesisStreamingDestination",
              "dynamodb:DescribeTable",
              "dynamodb:DescribeTimeToLive",
              "dynamodb:ListTagsOfResource",
              "ecr:BatchCheckLayerAvailability",
              "ecr:CompleteLayerUpload",
              "ecr:InitiateLayerUpload",
              "ecr:List*",
              "ecr:PutImage",
              "ecr:UploadLayerPart",
              "events:Describe*",
              "firehose:DescribeDeliveryStream",
              "iam:GenerateServiceLastAccessedDetails",
              "kms:Get*",
              "lambda:InvokeFunction",
              "logs:FilterLogEvents",
              "organizations:DescribeOrganizationalUnit",
              "organizations:DescribeAccount",
              "route53-recovery-control-config:Describe*",
              "route53-recovery-readiness:Get*",
              "route53resolver:Get*",
              "serverlessrepo:Get*",
            ]
            Resource = [
              "arn:aws:aps:${var.region}:${var.accountId}:*",
              "arn:aws:athena:${var.region}:${var.accountId}:*",
              "arn:aws:cloudformation:${var.region}:${var.accountId}:*",
              "arn:aws:dynamodb:${var.region}:${var.accountId}:table/*",
              "arn:aws:ecr:${var.region}:${var.accountId}:repository/*",
              "arn:aws:events:${var.region}:${var.accountId}:*",
              "arn:aws:events:${var.region}::event-source/*",
              "arn:aws:firehose:${var.region}:${var.accountId}:deliverystream/*",
              "arn:aws:iam::${var.accountId}:group/*",
              "arn:aws:iam::${var.accountId}:policy/*",
              "arn:aws:iam::${var.accountId}:role/*",
              "arn:aws:kms:${var.region}:${var.accountId}:key/*",
              "arn:aws:lambda:${var.region}:${var.accountId}:function:*",
              "arn:aws:logs:${var.region}:${var.accountId}:log-group:*",
              "arn:aws:organizations::${var.accountId}:*",
              "arn:aws:route53-recovery-control::${var.accountId}:*",
              "arn:aws:route53-recovery-readiness::${var.accountId}:*",
              "arn:aws:route53resolver:${var.region}:${var.accountId}:*",
              "arn:aws:serverlessrepo:${var.region}:${var.accountId}:applications/*",
            ]
          },
        ]
        DevPolicyS3 = [
          {
            Sid    = "s3Write1"
            Effect = "Allow"
            Action = [
              "s3:CreateJob",
              "s3:CreateStorageLensGroup",
              "s3:GetAccessPoint",
              "s3:GetAccountPublicAccessBlock",
              "s3:ListAccessGrantsInstances",
              "s3:ListAccessPoints",
              "s3:ListAccessPointsForObjectLambda",
              "s3:ListAllMyBuckets",
              "s3:ListJobs",
              "s3:ListMultiRegionAccessPoints",
              "s3:ListStorageLensConfigurations",
              "s3:ListStorageLensGroups",
              "s3:PutAccessPointPublicAccessBlock",
              "s3:PutAccountPublicAccessBlock",
              "s3:PutStorageLensConfiguration",
            ]
            Resource = [
              "*",
            ]
          },
          {
            Sid    = "s3Write2"
            Effect = "Allow"
            Action = [
              "s3:AbortMultipartUpload",
              "s3:AssociateAccessGrantsIdentityCenter",
              "s3:BypassGovernanceRetention",
              "s3:CreateAccess*",
              "s3:CreateBucket",
              "s3:CreateMultiRegionAccessPoint",
              "s3:Delete*",
              "s3:Describe*",
              "s3:DissociateAccessGrantsIdentityCenter",
              "s3:GetAccelerateConfiguration",
              "s3:GetAccessGrant*",
              "s3:GetAccessPointConfigurationForObjectLambda",
              "s3:GetAccessPointForObjectLambda",
              "s3:GetAccessPointPolicy*",
              "s3:GetAnalyticsConfiguration",
              "s3:GetBucket*",
              "s3:GetDataAccess",
              "s3:GetEncryptionConfiguration",
              "s3:GetIntelligentTieringConfiguration",
              "s3:GetInventoryConfiguration",
              "s3:GetJobTagging",
              "s3:GetLifecycleConfiguration",
              "s3:GetMetricsConfiguration",
              "s3:GetMultiRegion*",
              "s3:GetObject*",
              "s3:GetReplicationConfiguration",
              "s3:GetStorage*",
              "s3:InitiateReplication",
              "s3:ListAccessGrants",
              "s3:ListAccessGrantsLocations",
              "s3:ListBucket*",
              "s3:ListMultipartUploadParts",
              "s3:ListTagsForResource",
              "s3:ObjectOwnerOverrideToBucketOwner",
              "s3:PutAccelerateConfiguration",
              "s3:PutAccessGrantsInstanceResourcePolicy",
              "s3:PutAccessPointConfigurationForObjectLambda",
              "s3:PutAccessPointPolicy",
              "s3:PutAccessPointPolicyForObjectLambda",
              "s3:PutAnalyticsConfiguration",
              "s3:PutBucket*",
              "s3:PutEncryptionConfiguration",
              "s3:PutIntelligentTieringConfiguration",
              "s3:PutInventoryConfiguration",
              "s3:PutJobTagging",
              "s3:PutLifecycleConfiguration",
              "s3:PutMetricsConfiguration",
              "s3:PutMultiRegionAccessPointPolicy",
              "s3:PutObject*",
              "s3:PutReplicationConfiguration",
              "s3:PutStorageLensConfigurationTagging",
              "s3:Replicate*",
              "s3:RestoreObject",
              "s3:SubmitMultiRegionAccessPointRoutes",
              "s3:TagResource",
              "s3:UntagResource",
              "s3:Update*",
            ]
            Resource = [
              "arn:aws:s3:::fluidattacks-terraform-states-prod/env:/*atfluid/*",
              "arn:aws:s3:::integrates/analytics/*atfluid",
              "arn:aws:s3:::integrates/analytics/*atfluid/*",
              "arn:aws:s3:::integrates/continuous-repositories/continuoustest*/*",
              "arn:aws:s3:::integrates.*atfluid",
              "arn:aws:s3:::integrates.*atfluid/*",
              "arn:aws:s3:::web.eph.fluidattacks.com",
              "arn:aws:s3:::web.eph.fluidattacks.com/*",
              "arn:aws:s3:::integrates.dev",
              "arn:aws:s3:::integrates.dev/*",
              "arn:aws:s3:${var.region}:${var.accountId}:access-grants/default/grant/*",
              "arn:aws:s3:${var.region}:${var.accountId}:access-grants/default",
              "arn:aws:s3:${var.region}:${var.accountId}:access-grants/default/location/*",
              "arn:aws:s3:${var.region}:${var.accountId}:accesspoint/*",
              "arn:aws:s3:${var.region}:${var.accountId}:job/*",
              "arn:aws:s3::${var.accountId}:accesspoint/*",
              "arn:aws:s3:us-west-2:${var.accountId}:async-request/mrap/*/*",
              "arn:aws:s3-object-lambda:${var.region}:${var.accountId}:accesspoint/*",
              "arn:aws:s3:${var.region}:${var.accountId}:storage-lens/*",
              "arn:aws:s3:${var.region}:${var.accountId}:storage-lens-group/*",
            ]
          },
          {
            Sid    = "denyS3Downloads"
            Effect = "Deny"
            Action = "s3:GetObject"
            Resource = [
              "arn:aws:s3:::integrates/continuous-repositories/*",
              "arn:aws:s3:::integrates.continuous-repositories/*",
              "arn:aws:s3:::observes.etl-data"
            ]
          },
          {
            Sid    = "s3Write"
            Effect = "Allow"
            Action = [
              "s3:*Resource",
              "s3:Create*",
              "s3:Delete*",
              "s3:Describe*",
              "s3:Get*",
              "s3:List*",
              "s3:Put*",
              "s3:Update*",
              "s3:Replication*",
              "s3:AbortMultipartUpload",
              "s3:AssociateAccessGrantsIdentityCenter",
              "s3:DissociateAccessGrantsIdentityCenter",
              "s3:RestoreObject",
              "s3:SubmitMultiRegionAccessPointRoutes",
              "s3:BypassGovernanceRetention",
              "s3:ObjectOwnerOverrideToBucketOwner",
            ]
            Resource = [
              "arn:aws:s3:::fluidattacks-terraform-states-prod/env:/*atfluid/*",
              "arn:aws:s3:::integrates/analytics/*atfluid",
              "arn:aws:s3:::integrates/analytics/*atfluid/*",
              "arn:aws:s3:::integrates/continuous-repositories/continuoustest*/*",
              "arn:aws:s3:::integrates.*atfluid",
              "arn:aws:s3:::integrates.*atfluid/*",
              "arn:aws:s3:::web.eph.fluidattacks.com",
              "arn:aws:s3:::web.eph.fluidattacks.com/*",
              "arn:aws:s3:::integrates.dev",
              "arn:aws:s3:::integrates.dev/*",
              "arn:aws:s3:::common-ci-cache-common-ci-cache-gitlab-runner-cache-kbzs8npz",
              "arn:aws:s3:::common-ci-cache-common-ci-cache-gitlab-runner-cache-kbzs8npz/*",
            ]
          },
        ]
        DevPolicy4 = [

          {
            Sid    = "cloudwatchTagPermissions",
            Effect = "Allow",
            Action = [
              "cloudwatch:TagResource",
              "cloudwatch:UntagResource",
            ],
            Resource = [
              "arn:aws:cloudwatch:${var.region}:${var.accountId}:alarm:*",
              "arn:aws:cloudwatch:${var.region}:${var.accountId}:slo/*",
              "arn:aws:cloudwatch:${var.region}:${var.accountId}:insight-rule/*",
            ]
          },
          {
            Sid    = "cloudwatchAlarmWritePermissions"
            Effect = "Allow"
            Action = [
              "cloudwatch:PutMetricAlarm",
              "cloudwatch:PutCompositeAlarm",
              "cloudwatch:SetAlarmState",
            ]
            Resource = [
              "arn:aws:cloudwatch:${var.region}:${var.accountId}:alarm:*",
            ]
          },
          {
            Sid    = "cloudwatchDashboardWritePermissions"
            Effect = "Allow"
            Action = [
              "cloudwatch:PutDashboard",
              "cloudwatch:DeleteDashboards",
            ]
            Resource = [
              "arn:aws:cloudwatch::${var.accountId}:dashboard/*",
            ]
          },
          {
            Sid    = "generalCloudwatchRead1"
            Effect = "Allow"
            Action = [
              "cloudwatch:DescribeAlarmsForMetric",
              "cloudwatch:DescribeAnomalyDetectors",
              "cloudwatch:DescribeInsightRules",
              "cloudwatch:GetMetricData",
              "cloudwatch:GetMetricStatistics",
              "cloudwatch:GetMetricWidgetImage",
              "cloudwatch:GetTopologyMap",
              "cloudwatch:GetTopologyDiscoveryStatus",
              "cloudwatch:ListDashboards",
              "cloudwatch:ListManagedInsightRules",
              "cloudwatch:ListMetrics",
              "cloudwatch:ListMetricStreams",
              "cloudwatch:ListServices",
              "cloudwatch:ListServiceLevelObjectives",
            ]
            Resource = [
              "*"
            ]
          },
          {
            Sid    = "generalCloudwatchRead2"
            Effect = "Allow"
            Action = [
              "cloudwatch:DescribeAlarms",
              "cloudwatch:DescribeAlarmHistory",
              "cloudwatch:GetInsightRuleReport",
              "cloudwatch:GetDashboard",
              "cloudwatch:GetMetricStream",
              "cloudwatch:GetService*",
              "cloudwatch:ListTagsForResource",
            ]
            Resource = [
              "arn:aws:cloudwatch:${var.region}:${var.accountId}:alarm:*",
              "arn:aws:cloudwatch::${var.accountId}:dashboard/*",
              "arn:aws:cloudwatch:${var.region}:${var.accountId}:insight-rule/*",
              "arn:aws:cloudwatch:${var.region}:${var.accountId}:metric-stream/*",
              "arn:aws:cloudwatch:${var.region}:${var.accountId}:service/*-*",
              "arn:aws:cloudwatch:${var.region}:${var.accountId}:slo/*",
            ]
          },
          {
            Sid    = "inspectorRead"
            Effect = "Allow"
            Action = [
              "inspector2:BatchGetAccountStatus",
              "inspector2:BatchGetFreeTrialInfo",
              "inspector2:DescribeOrganizationConfiguration",
              "inspector2:GetCisScanResultDetails",
              "inspector2:GetCisScanReport",
              "inspector2:GetConfiguration",
              "inspector2:GetDelegatedAdminAccount",
              "inspector2:GetEc2DeepInspectionConfiguration",
              "inspector2:GetEncryptionKey",
              "inspector2:GetFindingsReportStatus",
              "inspector2:GetMember",
              "inspector2:GetSbomExport",
              "inspector2:ListAccountPermissions",
              "inspector2:ListCisScanConfigurations",
              "inspector2:ListCisScanResultsAggregatedByChecks",
              "inspector2:ListCisScanResultsAggregatedByTargetResource",
              "inspector2:ListCisScans",
              "inspector2:ListCoverage",
              "inspector2:ListCoverageStatistics",
              "inspector2:ListDelegatedAdminAccounts",
              "inspector2:ListFilters",
              "inspector2:ListFindingAggregations",
              "inspector2:ListFindings",
              "inspector2:ListMembers",
              "inspector2:ListUsageTotals",
              "inspector2:ListTagsForResource",
            ]
            Resource = ["*"]
          },
          {
            Sid    = "aurora1"
            Effect = "Allow"
            Action = [
              "rds:DescribeAccountAttributes",
              "rds:DescribeBlueGreenDeployments",
              "rds:DescribeCertificates",
              "rds:DescribeDBClusterEndpoints",
              "rds:DescribeDBClusterSnapshots",
              "rds:DescribeDBEngineVersions",
              "rds:DescribeDBInstanceAutomatedBackups",
              "rds:DescribeDBRecommendations",
              "rds:DescribeDBSnapshots",
              "rds:DescribeEngine*",
              "rds:DescribeEventCategories",
              "rds:DescribeEvents",
              "rds:DescribeExportTasks",
              "rds:DescribeOptionGroupOptions",
              "rds:DescribeOrderableDBInstanceOptions",
              "rds:DescribePendingMaintenanceActions",
              "rds:DescribeRecommendation*",
              "rds:DescribeReservedDBInstancesOfferings",
              "rds:DescribeSourceRegions",
            ]
            Resource = ["*"]
          },
          {
            Sid    = "aurora2"
            Effect = "Allow"
            Action = [
              "rds:DescribeDBClusterAutomatedBackups",
              "rds:DescribeDBClusterBacktracks",
              "rds:DescribeDBClusterParameter*",
              "rds:DescribeDBClusterSnapshotAttributes",
              "rds:DescribeDBClusters",
              "rds:DescribeDBInstances",
              "rds:DescribeDBLogFiles",
              "rds:DescribeDBParameter*",
              "rds:DescribeDBProx*",
              "rds:DescribeDBSecurityGroups",
              "rds:DescribeDBShardGroups",
              "rds:DescribeDBSnapshotAttributes",
              "rds:DescribeDBSnapshotTenantDatabases",
              "rds:DescribeDBSubnetGroups",
              "rds:DescribeEventSubscriptions",
              "rds:DescribeGlobalClusters",
              "rds:DescribeIntegrations",
              "rds:DescribeOptionGroups",
              "rds:DescribeReservedDBInstances",
              "rds:DescribeTenantDatabases",
              "rds:DescribeValidDBInstanceModifications",
              "rds:ListTagsForResource",
            ]
            Resource = [
              "arn:aws:rds:${var.region}:*:cluster:*",
              "arn:aws:rds:${var.region}:${var.accountId}:cluster-auto-backup:*",
              "arn:aws:rds:${var.region}:${var.accountId}:cluster-pg:*",
              "arn:aws:rds:${var.region}:${var.accountId}:cluster-snapshot:*",
              "arn:aws:rds:${var.region}:${var.accountId}:db:*",
              "arn:aws:rds:${var.region}:${var.accountId}:es:*",
              "arn:aws:rds::${var.accountId}:global-cluster:*",
              "arn:aws:rds:${var.region}:${var.accountId}:integration:*",
              "arn:aws:rds:${var.region}:${var.accountId}:og:*",
              "arn:aws:rds:${var.region}:${var.accountId}:pg:*",
              "arn:aws:rds:${var.region}:${var.accountId}:db-proxy:*",
              "arn:aws:rds:${var.region}:${var.accountId}:db-proxy-endpoint:*",
              "arn:aws:rds:${var.region}:${var.accountId}:ri:*",
              "arn:aws:rds:${var.region}:${var.accountId}:secgrp:*",
              "arn:aws:rds:${var.region}:${var.accountId}:shard-group:*",
              "arn:aws:rds:${var.region}:${var.accountId}:snapshot:*",
              "arn:aws:rds:${var.region}:${var.accountId}:snapshot-tenant-database:*:*",
              "arn:aws:rds:${var.region}:${var.accountId}:subgrp:*",
              "arn:aws:rds:${var.region}:${var.accountId}:target-group:*",
              "arn:aws:rds:${var.region}:${var.accountId}:tenant-database:*"
            ]
          },
        ]
      }

      cloudflare = {
        account = {
          effect = "allow"
          permission_groups = [
            data.cloudflare_api_token_permission_groups.all.account["Workers Scripts Read"],
            data.cloudflare_api_token_permission_groups.all.account["Account Rulesets Read"],
          ]
          resources = {
            "com.cloudflare.api.account.*" = "*"
          }
        }
        accountZone = {
          effect = "allow"
          permission_groups = [
            data.cloudflare_api_token_permission_groups.all.zone["Zone Read"],
            data.cloudflare_api_token_permission_groups.all.zone["DNS Read"],
            data.cloudflare_api_token_permission_groups.all.zone["Workers Routes Read"],
            data.cloudflare_api_token_permission_groups.all.zone["Cache Purge"],
            data.cloudflare_api_token_permission_groups.all.zone["Cache Settings Read"],
            data.cloudflare_api_token_permission_groups.all.zone["Zone Settings Read"],
            data.cloudflare_api_token_permission_groups.all.zone["Config Settings Read"],
            data.cloudflare_api_token_permission_groups.all.zone["Page Rules Read"],
            data.cloudflare_api_token_permission_groups.all.zone["Firewall Services Read"],
          ]
          resources = {
            "com.cloudflare.api.account.zone.*" = "*"
          }
        }
      }
    }

    keys = {
      dev = {
        admins = [
          "prod_common",
        ]
        read_users = []
        users = [
          "dev",
          "prod_airs",
          "prod_integrates",
          "prod_observes",
          "prod_skims",
          "prod_sorts",
          "debug_integrates"
        ]
        tags = {
          "Name"              = "dev"
          "fluidattacks:line" = "research"
          "fluidattacks:comp" = "common"
        }
      }
    }
  }
}

module "dev_aws" {
  source = "./modules/aws"

  name     = "dev"
  policies = local.dev.policies.aws

  assume_role_policy = [
    {
      Sid    = "allowBatchOPerations"
      Effect = "Allow",
      Principal = {
        Service = "batchoperations.s3.amazonaws.com"
      },
      Action = "sts:AssumeRole"
    },
    {
      Sid    = "ciAccessDev",
      Effect = "Allow",
      Principal = {
        Federated = "arn:aws:iam::${var.accountId}:oidc-provider/gitlab.com",
      },
      Action = "sts:AssumeRoleWithWebIdentity",
      Condition = {
        StringLike = {
          "gitlab.com:sub" : "project_path:fluidattacks/universe:ref_type:branch:ref:*"
        },
      },
    },
  ]

  tags = {
    "Name"              = "dev"
    "fluidattacks:line" = "research"
    "fluidattacks:comp" = "common"
  }
}

module "dev_keys" {
  source   = "./modules/key"
  for_each = local.dev.keys

  name       = each.key
  admins     = each.value.admins
  read_users = each.value.read_users
  users      = each.value.users
  tags       = each.value.tags
}

module "dev_cloudflare" {
  source = "./modules/cloudflare"

  name   = "dev"
  policy = local.dev.policies.cloudflare
}

output "dev_cloudflare_api_token" {
  sensitive = true
  value     = module.dev_cloudflare.cloudflare_api_token
}
