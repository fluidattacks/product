locals {
  prod_common = {
    policies = {
      aws = {
        AdminPolicy = [
          {
            Sid    = "allWrite"
            Effect = "Allow"
            # NOFLUID prod_common is root, escalation is expected
            Action = [
              "access-analyzer:*",
              "account:Get*",
              "acm:*",
              "aoss:*",
              "aps:*",
              "apigateway:*",
              "athena:*",
              "autoscaling:*",
              "aws-marketplace:*",
              "backup:*",
              "batch:*",
              "bedrock:*",
              "billing:*",
              "budgets:*",
              "ce:*",
              "cloudformation:*",
              "cloudwatch:*",
              "cloudtrail:*",
              "consolidatedbilling:*",
              "config:*",
              "cur:*",
              "dsql:*",
              "dynamodb:*",
              "ec2:*",
              "ec2-instance-connect:SendSerialConsoleSSHPublicKey",
              "ecr:*",
              "ecr-public:*",
              "ecs:*",
              "eks:*",
              "elasticache:*",
              "elasticloadbalancing:*",
              "es:*",
              "events:*",
              "firehose:*",
              "freetier:*",
              "fis:*",
              "glue:*",
              "grafana:*",
              "guardduty:*",
              "iam:*",
              "inspector2:*",
              "internetmonitor:*",
              "invoicing:*",
              "kinesis:*",
              "kinesisanalytics:*",
              "kms:*",
              "lambda:*",
              "logs:*",
              "organizations:*",
              "osis:*",
              "payments:*",
              "pricing:*",
              "purchase-orders:GetConsoleActionSetEnforced",
              "purchase-orders:UpdateConsoleActionSetEnforced",
              "quicksight:TagResource",
              "quicksight:ListTagsForResource",
              "ram:*",
              "rbin:CreateRule",
              "rbin:UpdateRule",
              "rbin:GetRule",
              "rbin:ListRules",
              "rbin:DeleteRule",
              "rbin:TagResource",
              "rbin:UntagResource",
              "rbin:ListTagsForResource",
              "rbin:LockRule",
              "rbin:UnlockRule",
              "rds:*",
              "redshift:*",
              "redshift-data:*",
              "redshift-serverless:*",
              "resiliencehub:*",
              "route53:*",
              "route53-recovery-control-config:*",
              "route53-recovery-readiness:*",
              "route53domains:*",
              "route53resolver:*",
              "s3:*",
              "sagemaker:*",
              "savingsplans:*",
              "schemas:*",
              "secretsmanager:*",
              "securityhub:*",
              "serverlessrepo:*",
              "servicequotas:*",
              "sns:*",
              "sso:*",
              "sqlworkbench:*",
              "sqs:*",
              "ssm:*Automation*",
              "ssm:*Session",
              "ssm:Add*",
              "ssm:Delete*",
              "ssm:Describe*",
              "ssm:Get*",
              "ssm:List*",
              "ssm:Put*",
              "ssm:Remove*",
              "sts:*",
              "support:*",
              "sustainability:*",
              "tag:*",
              "tax:*",
              "xray:*",
              "resource-groups:SearchResources",
              "states:*"
            ]
            Resource = ["*"]
          },
          {
            Sid    = "legacyPolicy"
            Effect = "Allow"
            Action = [
              "aws-portal:*",
            ]
            Resource = ["*"]
          },
        ]
      }
    }

    keys = {
      common_google = {
        admins = [
          "prod_common",
        ]
        read_users = []
        users = [
          "dev",
        ]
        tags = {
          "Name"              = "common_google"
          "fluidattacks:line" = "cost"
          "fluidattacks:comp" = "common"
        }
      }
      common_okta = {
        admins = [
          "prod_common",
        ]
        read_users = [
          "dev"
        ]
        users = []
        tags = {
          "Name"              = "common_okta"
          "fluidattacks:line" = "cost"
          "fluidattacks:comp" = "common"
        }
      }
      common_status = {
        admins = [
          "prod_common",
        ]
        read_users = []
        users = [
          "dev",
        ]
        tags = {
          "Name"              = "common_status"
          "fluidattacks:line" = "cost"
          "fluidattacks:comp" = "common"
        }
      }
      common_vpn = {
        admins = [
          "prod_common",
        ]
        read_users = []
        users = [
          "dev",
        ]
        tags = {
          "Name"              = "common_vpn"
          "fluidattacks:line" = "cost"
          "fluidattacks:comp" = "common"
        }
      }
      prod_common = {
        admins = [
          "prod_common",
        ]
        read_users = []
        users      = []
        tags = {
          "Name"              = "prod_common"
          "fluidattacks:line" = "cost"
          "fluidattacks:comp" = "common"
        }
      }
    }
  }
}

module "prod_common_aws" {
  source = "./modules/aws"

  name     = "prod_common"
  policies = local.prod_common.policies.aws

  assume_role_policy = [
    {
      Sid    = "AssumeRolePolicy",
      Effect = "Allow",
      Principal = {
        Service = [
          "batch.amazonaws.com",
          "events.amazonaws.com",
          "spotfleet.amazonaws.com",
        ],
      },
      Action = "sts:AssumeRole",
    },
  ]

  tags = {
    "Name"              = "prod_common"
    "fluidattacks:line" = "cost"
    "fluidattacks:comp" = "common"
    "NOFLUID"           = "f005.f325_prod_common_is_root_escalation_is_expected"
  }
}

module "prod_common_keys" {
  source   = "./modules/key"
  for_each = local.prod_common.keys

  name       = each.key
  admins     = each.value.admins
  read_users = each.value.read_users
  users      = each.value.users
  tags       = each.value.tags
}
