locals {
  prod_skims = {
    policies = {
      aws = {
        SkimsPolicy = [
          {
            Sid    = "readBill1"
            Effect = "Allow"
            Action = [
              "billing:Get*",
              "billing:List*",
              "consolidatedbilling:Get*",
              "consolidatedbilling:List*",
              "invoicing:Get*",
              "invoicing:List*",
              "payments:Get*",
              "payments:List*",
              "pricing:Describe*",
              "pricing:Get*",
              "tax:Get*",
              "tax:List*",
            ]
            Resource = ["*"]
          },
          {
            Sid    = "readBill2"
            Effect = "Allow"
            Action = [
              "account:Get*",
              "ce:Describe*",
              "ce:Get*",
              "ce:List*",
              "cur:Describe*",
              "cur:Get*",
              "cur:Validate*",
            ]
            Resource = [
              "arn:aws:account::${var.accountId}:account",
              "arn:aws:ce::${var.accountId}:anomalymonitor/*",
              "arn:aws:cur:${var.region}:${var.accountId}:definition/*"
            ]
          },
          {
            Sid    = "batchRead"
            Effect = "Allow"
            Action = [
              "batch:DescribeComputeEnvironments",
              "batch:DescribeJobDefinitions",
              "batch:DescribeJobQueues",
              "batch:DescribeJobs",
              "batch:ListJobs",
            ]
            Resource = ["*"]
          },
          {
            Sid    = "batchWrite"
            Effect = "Allow"
            Action = [
              "batch:SubmitJob",
            ]
            Resource = [
              "arn:aws:batch:us-east-1:${data.aws_caller_identity.main.account_id}:job-definition/*",
              "arn:aws:batch:us-east-1:${data.aws_caller_identity.main.account_id}:job-queue/*",
            ]
          },
          {
            Sid    = "logsReadAll1"
            Effect = "Allow"
            Action = [
              "logs:DescribeAccountPolicies",
              "logs:DescribeDeliveries",
              "logs:DescribeDeliveryDestinations",
              "logs:DescribeDeliverySources",
              "logs:DescribeDestinations",
              "logs:DescribeExportTasks",
              "logs:DescribeLogGroups",
              "logs:DescribeQueries",
              "logs:DescribeQueryDefinitions",
              "logs:DescribeResourcePolicies",
              "logs:ListLogDeliveries",
            ]
            Resource = ["*"]
          },
          {
            Sid    = "logsReadAll2"
            Effect = "Allow"
            Action = [
              "logs:DescribeLogStreams",
              "logs:DescribeMetricFilters",
              "logs:DescribeSubscriptionFilters",
            ]
            Resource = ["arn:aws:logs:${var.region}:${var.accountId}:log-group:*"]
          },
          {
            Sid    = "RemoveLog"
            Effect = "Allow"
            Action = [
              "logs:DeleteLogGroup"
            ]
            Resource = [
              "arn:aws:logs:${var.region}:${var.accountId}:*"
            ]
          },
          {
            Sid    = "logsRead"
            Effect = "Allow"
            Action = [
              "logs:CreateLogGroup",
              "logs:FilterLogEvents",
              "logs:Get*",
              "logs:ListTagsForResource",
              "logs:ListTagsLogGroup",
            ]
            Resource = [
              "arn:aws:logs:${var.region}:${var.accountId}:*"
            ]
          },
          {
            Sid    = "ec2Read"
            Effect = "Allow"
            Action = [
              "ec2:Describe*",
              "ec2:Get*",
            ]
            Resource = ["*"]
          },
          {
            Sid    = "ec2WriteAll"
            Effect = "Allow"
            Action = [
              "ec2:Create*",
              "ec2:DeleteTags",
            ]
            Resource = ["*"]
          },
          {
            Sid    = "ec2Write"
            Effect = "Allow"
            Action = [
              "ec2:ApplySecurityGroupsToClientVpnTargetNetwork",
              "ec2:AuthorizeSecurityGroupEgress",
              "ec2:AuthorizeSecurityGroupIngress",
              "ec2:DeleteNetworkInterface",
              "ec2:DeleteSecurityGroup",
              "ec2:DeleteSubnet",
              "ec2:ModifySubnetAttribute",
              "ec2:RevokeSecurityGroupEgress",
              "ec2:RevokeSecurityGroupIngress",
              "ec2:UpdateSecurityGroupRuleDescriptionsEgress",
              "ec2:UpdateSecurityGroupRuleDescriptionsIngress",
            ]
            Resource = [
              "arn:aws:ec2:${var.region}:${var.accountId}:client-vpn-endpoint/*",
              "arn:aws:ec2:${var.region}:${var.accountId}:security-group/*",
              "arn:aws:ec2:${var.region}:${var.accountId}:network-interface/*",
              "arn:aws:ec2:${var.region}:${var.accountId}:subnet/*",
              "arn:aws:ec2:${var.region}:${var.accountId}:capacity-reservation/*",
            ]
          },
          {
            Sid    = "dynamoWrite"
            Effect = "Allow"
            Action = [
              "dynamodb:DeleteItem",
              "dynamodb:GetItem",
              "dynamodb:PutItem",
            ]
            Resource = [
              var.terraform_state_lock_arn,
            ]
          },
          {
            Sid    = "dynamoCreate"
            Effect = "Allow"
            Action = [
              "dynamodb:CreateTable"
            ]
            Resource = [
              "arn:aws:dynamodb:${var.region}:${var.accountId}:table/*"
            ]
          },
          {
            Sid    = "dynamoReadJobs"
            Effect = "Allow"
            Action = [
              "dynamodb:GetItem",
              "dynamodb:Query",
              "dynamodb:DeleteItem",
              "dynamodb:UpdateItem",
            ]
            Resource = [
              "arn:aws:dynamodb:us-east-1:205810638802:table/fi_async_processing",
            ]
          },
          {
            Sid    = "sqsWrite"
            Effect = "Allow"
            Action = [
              "sqs:CreateQueue",
              "sqs:DeleteQueue",
              "sqs:GetQueueAttributes",
              "sqs:SetQueueAttributes",
              "sqs:TagQueue",
              "sqs:UntagQueue",
              "sqs:AddPermission",
              "sqs:RemovePermission",
              "sqs:ListQueues",
            ]
            Resource = [
              "arn:aws:sqs:${var.region}:${var.accountId}:*",
            ]
          },
          {
            Sid    = "eksReadAll"
            Effect = "Allow"
            Action = [
              "eks:DescribeAddonConfiguration",
              "eks:DescribeAddonVersions",
            ]
            Resource = ["*"]
          },
          {
            Sid    = "eksRead"
            Effect = "Allow"
            Action = [
              "eks:Describe*",
            ]
            Resource = [
              "arn:aws:eks:${var.region}:${var.accountId}:*"
            ]
          },
          {
            Sid    = "cloudWatchAll"
            Effect = "Allow"
            Action = [
              "cloudwatch:GetMetricData",
            ]
            Resource = ["*"]
          },
          {
            Sid    = "sqsQueueMessages"
            Effect = "Allow"
            Action = [
              "sqs:ChangeMessageVisibility",
              "sqs:GetQueueAttributes",
              "sqs:GetQueueUrl",
              "sqs:SendMessage",
            ]
            Resource = [
              "arn:aws:sqs:us-east-1:205810638802:integrates_report_soon",
            ]
          },
          {
            Sid    = "sqsGetMessages"
            Effect = "Allow"
            Action = [

              "sqs:ReceiveMessage",
              "sqs:DeleteMessage",
              "sqs:DeleteMessageBatch"
            ]
            Resource = [
              "arn:aws:sqs:us-east-1:205810638802:integrates_packages",
            ]
          },
        ]
        SkimsElasticachePolicy = [
          {
            Sid    = "ManageElasticacheSubnetGroup"
            Effect = "Allow"
            Action = [
              "elasticache:CreateCacheSubnetGroup",
              "elasticache:DescribeCacheSubnetGroups",
              "elasticache:DeleteCacheSubnetGroup"
            ]
            Resource = "arn:aws:elasticache:${var.region}:${data.aws_caller_identity.main.account_id}:subnetgroup:sbom-packages-metadata"
          },
          {
            Sid    = "ManageElasticacheClusters"
            Effect = "Allow"
            Action = [
              "elasticache:DescribeCacheClusters"
            ]
            Resource = "arn:aws:elasticache:${var.region}:${data.aws_caller_identity.main.account_id}:cluster:sbom-packages-metadata*"
          },
          {
            Sid    = "ManageElasticacheReplicationGroup"
            Effect = "Allow"
            Action = [
              "elasticache:CreateReplicationGroup",
              "elasticache:DescribeReplicationGroups",
              "elasticache:ModifyReplicationGroup",
              "elasticache:DeleteReplicationGroup"
            ]
            Resource = "arn:aws:elasticache:${var.region}:${data.aws_caller_identity.main.account_id}:replicationgroup:sbom-packages-metadata"
          },
          {
            Sid    = "ManageElasticacheUsers"
            Effect = "Allow"
            Action = [
              "elasticache:CreateUser",
              "elasticache:DescribeUsers",
              "elasticache:ModifyUser",
              "elasticache:DeleteUser"
            ]
            Resource = [
              "arn:aws:elasticache:${var.region}:${data.aws_caller_identity.main.account_id}:user:restricted-default-user",
              "arn:aws:elasticache:${var.region}:${data.aws_caller_identity.main.account_id}:user:readonly",
              "arn:aws:elasticache:${var.region}:${data.aws_caller_identity.main.account_id}:user:readwrite"
            ]
          },
          {
            Sid    = "ManageElasticacheUserGroups"
            Effect = "Allow"
            Action = [
              "elasticache:CreateUserGroup",
              "elasticache:DescribeUserGroups",
              "elasticache:ModifyUserGroup",
              "elasticache:DeleteUserGroup"
            ]
            Resource = "arn:aws:elasticache:${var.region}:${data.aws_caller_identity.main.account_id}:usergroup:sbom-packages-metadata-group"
          },
          {
            Sid    = "ManageTags"
            Effect = "Allow"
            Action = [
              "elasticache:AddTagsToResource",
              "elasticache:ListTagsForResource",
              "elasticache:RemoveTagsFromResource",
            ]
            Resource = "*"
          }
        ]
        S3Policies = [
          {
            Sid    = "write"
            Effect = "Allow"
            Action = [
              "s3:Create*",
              "s3:Delete*",
              "s3:Get*",
              "s3:Put*",
            ]
            Resource = [
              "arn:aws:s3:::fluidattacks.com/resources/doc/skims/*",
              "arn:aws:s3:::fluidattacks-terraform-states-prod/skims*",
              "arn:aws:s3:::skims*",
              "arn:aws:s3:::skims.sca",
              "arn:aws:s3:::skims.sca/*",
              "arn:aws:dynamodb:us-east-1:205810638802:table/skims*",
              "arn:aws:dynamodb:us-east-1:205810638802:table/celery",
              "arn:aws:s3:::fluidattacks.public.storage",
              "arn:aws:s3:::fluidattacks.public.storage/*",
            ]
          },
          {
            Sid    = "s3Read"
            Effect = "Allow"
            Action = [
              "s3:ListBucket",
            ]
            Resource = [
              "arn:aws:s3:::fluidattacks.com",
            ]
          },
          {
            Sid    = "s3ReadExecutions"
            Effect = "Allow"
            Action = [
              "s3:ListBucket",
              "s3:GetObject"
            ]
            Resource = [
              "arn:aws:s3:::machine.data",
              "arn:aws:s3:::machine.data/*",
            ]
          },
          {
            Sid    = "s3ReadRepos"
            Effect = "Allow"
            Action = [
              "s3:ListBucket",
              "s3:ListObjectsV2",
              "s3:GetObject",
            ]
            Resource = [
              "arn:aws:s3:::integrates/continuous-repositories",
              "arn:aws:s3:::integrates/continuous-repositories/*",
              "arn:aws:s3:::integrates.continuous-repositories",
              "arn:aws:s3:::integrates.continuous-repositories/*",
            ]
          },
          {
            Sid      = "ListObjectsInTfStatesProd",
            Effect   = "Allow",
            Action   = ["s3:ListBucket"],
            Resource = ["arn:aws:s3:::fluidattacks-terraform-states-prod"]
          },
        ]
      }
    }

    keys = {
      prod_skims = {
        admins = [
          "prod_common",
        ]
        read_users = []
        users = [
          "prod_skims",
        ]
        tags = {
          "Name"              = "prod_skims"
          "fluidattacks:line" = "cost"
          "fluidattacks:comp" = "skims"
        }
      }
    }
  }
}

module "prod_skims_aws" {
  source = "./modules/aws"

  name     = "prod_skims"
  policies = local.prod_skims.policies.aws

  tags = {
    "Name"              = "prod_skims"
    "fluidattacks:line" = "cost"
    "fluidattacks:comp" = "skims"
    "NOFLUID"           = "f325_no_need_unfold_all_actions"
  }
}

module "prod_skims_keys" {
  source   = "./modules/key"
  for_each = local.prod_skims.keys

  name       = each.key
  admins     = each.value.admins
  read_users = each.value.read_users
  users      = each.value.users
  tags       = each.value.tags
}
