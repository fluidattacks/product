{ outputs, ... }: {
  deployTerraform = {
    modules = {
      commonMarketplace = {
        setup = [
          outputs."/secretsForAwsFromGitlab/marketplaceDeployment"
          outputs."/secretsForEnvFromSops/marketplace"
          outputs."/secretsForTerraformFromEnv/marketplace"
        ];
        src = "/common/marketplace/infra";
        version = "1.0";
      };
    };
  };
  lintTerraform = {
    modules = {
      commonMarketplace = {
        setup = [ outputs."/secretsForAwsFromGitlab/marketplaceDev" ];
        src = "/common/marketplace/infra";
        version = "1.0";
      };
    };
  };
  secretsForEnvFromSops = {
    marketplace = {
      manifest = "/common/marketplace/secrets.yaml";
      vars = [
        "INTEGRATES_EXTERNAL_ID"
        "SNS_ENTITLEMENTS_TOPIC"
        "SNS_SUBSCRIPTIONS_TOPIC"
      ];
    };
  };
  secretsForTerraformFromEnv = {
    marketplace = {
      integrates_external_id = "INTEGRATES_EXTERNAL_ID";
      sns_entitlements_topic = "SNS_ENTITLEMENTS_TOPIC";
      sns_subscriptions_topic = "SNS_SUBSCRIPTIONS_TOPIC";
    };
  };
  testTerraform = {
    modules = {
      commonMarketplace = {
        setup = [
          outputs."/secretsForAwsFromGitlab/marketplaceDev"
          outputs."/secretsForEnvFromSops/marketplace"
          outputs."/secretsForTerraformFromEnv/marketplace"
        ];
        src = "/common/marketplace/infra";
        version = "1.0";
      };
    };
  };
}
