locals {
  sqs_queue_name = "MarketplaceUpdates"
}

data "aws_iam_policy_document" "sqs_policy" {
  // NOFLUID In this context, the wildcard only applies to the queue
  statement {
    sid       = "SNSUpdates"
    actions   = ["sqs:SendMessage"]
    effect    = "Allow"
    resources = ["*"]

    condition {
      test = "ArnEquals"
      values = [
        var.sns_entitlements_topic,
        var.sns_subscriptions_topic
      ]
      variable = "aws:SourceArn"
    }

    principals {
      identifiers = ["sns.amazonaws.com"]
      type        = "Service"
    }
  }

  // NOFLUID In this context, the wildcard only applies to the queue
  statement {
    sid = "IntegratesConsumption"
    actions = [
      "sqs:DeleteMessage",
      "sqs:ReceiveMessage"
    ]
    effect    = "Allow"
    resources = ["*"]

    condition {
      test     = "ArnEquals"
      values   = ["arn:aws:iam::205810638802:role/prod_integrates"]
      variable = "aws:PrincipalArn"
    }

    principals {
      identifiers = ["*"]
      type        = "AWS"
    }
  }
}

resource "aws_sqs_queue" "marketplace" {
  name                    = local.sqs_queue_name
  policy                  = data.aws_iam_policy_document.sqs_policy.json
  sqs_managed_sse_enabled = true
}

resource "aws_sns_topic_subscription" "entitlements_sns" {
  endpoint  = aws_sqs_queue.marketplace.arn
  protocol  = "sqs"
  topic_arn = var.sns_entitlements_topic
}

resource "aws_sns_topic_subscription" "subscriptions_sns" {
  endpoint  = aws_sqs_queue.marketplace.arn
  protocol  = "sqs"
  topic_arn = var.sns_subscriptions_topic
}
