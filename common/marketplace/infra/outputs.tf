output "okta_access_key" {
  sensitive = true
  value     = aws_iam_access_key.okta.id
}

output "okta_idp_arn" {
  value = aws_iam_saml_provider.okta.arn
}

output "okta_secret_key" {
  sensitive = true
  value     = aws_iam_access_key.okta.secret
}
