# Okta
resource "aws_iam_saml_provider" "okta" {
  name                   = "OktaSAMLProvider"
  saml_metadata_document = <<-EOT
  <?xml version="1.0" encoding="UTF-8"?>
  <md:EntityDescriptor xmlns:md="urn:oasis:names:tc:SAML:2.0:metadata" entityID="http://www.okta.com/exkwy51z54JJ6T9yq357">
    <md:IDPSSODescriptor WantAuthnRequestsSigned="false" protocolSupportEnumeration="urn:oasis:names:tc:SAML:2.0:protocol">
      <md:KeyDescriptor use="signing">
        <ds:KeyInfo xmlns:ds="http://www.w3.org/2000/09/xmldsig#">
          <ds:X509Data>
            <ds:X509Certificate>
              MIIDqDCCApCgAwIBAgIGAYs1/+WoMA0GCSqGSIb3DQEBCwUAMIGUMQswCQYDVQQGEwJVUzETMBEG A1UECAwKQ2FsaWZvcm5pYTEWMBQGA1UEBwwNU2FuIEZyYW5jaXNjbzENMAsGA1UECgwET2t0YTEU MBIGA1UECwwLU1NPUHJvdmlkZXIxFTATBgNVBAMMDGZsdWlkYXR0YWNrczEcMBoGCSqGSIb3DQEJ ARYNaW5mb0Bva3RhLmNvbTAeFw0yMzEwMTYwMDU5NDRaFw0zMzEwMTYwMTAwNDRaMIGUMQswCQYD VQQGEwJVUzETMBEGA1UECAwKQ2FsaWZvcm5pYTEWMBQGA1UEBwwNU2FuIEZyYW5jaXNjbzENMAsG A1UECgwET2t0YTEUMBIGA1UECwwLU1NPUHJvdmlkZXIxFTATBgNVBAMMDGZsdWlkYXR0YWNrczEc MBoGCSqGSIb3DQEJARYNaW5mb0Bva3RhLmNvbTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoC ggEBAJIuCb6i0hluSpxuYllAlWiSC5CopP/Ectgm452HVU73iFZZF6jm55g6kE3hdLQ598ipeJkk 2G8UXIOMbLxd/pW/i2tOizIXAeU1/AnZvWAYZtDLg0x7bXHv/bYJzDhusjdozI9g35IhEJuxXTrP fMeu+eXaanhnX/ZqzXuhlz+fUUo4WfHn19zQ8974OjhT/XnCI/ADi/ztfIawuX652W8uu0H5r+k+ l26aCf5dG+DyXkYwBMph1O0TnaxEiHNDXq6z2XPFAvRaE788lnj+ovO6mv17I/aMWIevrHO+FChr cKDFXG+7RzWIi0xb7tyK/3K1C+wAiQj7Sl6q9fvNlFsCAwEAATANBgkqhkiG9w0BAQsFAAOCAQEA EbWXzBtPPTKeZ4IM/KGw9GW65wM4RvzjX/WcIzIScwVICUcF+wT3kKHMtyv80ZWaHB5P6zKePYBm G5xnn9E4g3sIxmH/lqt3MQiWx6eAQToeyED5pcZqXfyzT6qLo0z0TtvFNe1iiFURNBaLQL2EU38/ P/fpvWJxgU48i6X0l05nkPwLnNhTGxc2hpq81t9Y7hAfN2dXkvyGz/hY/hCSbIFazStqjMz/JodM ERRjyGCQqNZY/pb67NEI7T6EpSviZ0fNQlWN0fMhzGYmwWL9hJOa3xFuYW2HZFs+Sn+uI503nkRq EZyQjqtn7r6ZVgkYxhnYpv1VRid1HzJZvDaAVQ==
            </ds:X509Certificate>
          </ds:X509Data>
        </ds:KeyInfo>
      </md:KeyDescriptor>
      <md:NameIDFormat>
        urn:oasis:names:tc:SAML:1.1:nameid-format:emailAddress
      </md:NameIDFormat>
      <md:NameIDFormat>
        urn:oasis:names:tc:SAML:2.0:nameid-format:unspecified
      </md:NameIDFormat>
      <md:SingleSignOnService Binding="urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST" Location="https://fluidattacks.okta.com/app/amazon_aws/exkwy51z54JJ6T9yq357/sso/saml"/>
      <md:SingleSignOnService Binding="urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect" Location="https://fluidattacks.okta.com/app/amazon_aws/exkwy51z54JJ6T9yq357/sso/saml"/>
    </md:IDPSSODescriptor>
  </md:EntityDescriptor>
  EOT
}

data "aws_iam_policy_document" "okta_assume_role" {
  statement {
    sid     = "OktaAssumeRole"
    effect  = "Allow"
    actions = ["sts:AssumeRoleWithSAML"]

    principals {
      type        = "Federated"
      identifiers = [aws_iam_saml_provider.okta.arn]
    }

    condition {
      test     = "StringEquals"
      variable = "SAML:aud"
      values   = ["https://signin.aws.amazon.com/saml"]
    }
  }
}

# Gitlab
data "tls_certificate" "gitlab" {
  url = "https://gitlab.com/.well-known/openid-configuration"
}

resource "aws_iam_openid_connect_provider" "gitlab" {
  url             = "https://gitlab.com"
  client_id_list  = ["https://gitlab.com"]
  thumbprint_list = data.tls_certificate.gitlab.certificates[*].sha1_fingerprint
}

data "aws_iam_policy_document" "gitlab_assume_role_deployment" {
  statement {
    sid     = "GitlabAssumeRoleDeployment"
    effect  = "Allow"
    actions = ["sts:AssumeRoleWithWebIdentity"]

    principals {
      type        = "Federated"
      identifiers = [aws_iam_openid_connect_provider.gitlab.arn]
    }

    condition {
      test     = "StringEquals"
      variable = "gitlab.com:sub"
      values   = ["project_path:fluidattacks/universe:ref_type:branch:ref:trunk"]
    }
  }
}

data "aws_iam_policy_document" "gitlab_assume_role_development" {
  statement {
    sid     = "GitlabAssumeRoleDevelopment"
    effect  = "Allow"
    actions = ["sts:AssumeRoleWithWebIdentity"]

    principals {
      type        = "Federated"
      identifiers = [aws_iam_openid_connect_provider.gitlab.arn]
    }

    condition {
      test     = "StringLike"
      variable = "gitlab.com:sub"
      values   = ["project_path:fluidattacks/universe:ref_type:branch:ref:*atfluid"]
    }
  }
}
