data "aws_iam_policy_document" "sops_kms_keys_policy" {
  // NOFLUID In this context, the wildcard only applies to the associated key
  statement {
    sid = "WritePermissions"
    actions = [
      "kms:CreateKey",
      "kms:DisableKey",
      "kms:DisableKeyRotation",
      "kms:EnableKey",
      "kms:EnableKeyRotation",
      "kms:ScheduleKeyDeletion",
      "kms:UpdateKeyDescription",
      "kms:PutKeyPolicy"
    ]
    effect    = "Allow"
    resources = ["*"]

    principals {
      identifiers = [
        "arn:aws:iam::${data.aws_caller_identity.account.account_id}:root",
        aws_iam_role.admin.arn,
        aws_iam_role.deployment.arn
      ]
      type = "AWS"
    }
  }

  // NOFLUID In this context, the wildcard only applies to the associated key
  statement {
    sid = "ReadPermissions"
    actions = [
      "kms:DescribeKey",
      "kms:GetKeyPolicy",
      "kms:GetKeyRotationStatus",
      "kms:ListKeyPolicies",
      "kms:ListKeyRotations",
      "kms:ListResourceTags"
    ]
    effect    = "Allow"
    resources = ["*"]

    principals {
      identifiers = [
        "arn:aws:iam::${data.aws_caller_identity.account.account_id}:root",
        aws_iam_role.admin.arn,
        aws_iam_role.deployment.arn,
        aws_iam_role.development.arn
      ]
      type = "AWS"
    }
  }

  // NOFLUID In this context, the wildcard only applies to the associated key
  statement {
    sid = "EncryptDecrypt"
    actions = [
      "kms:Decrypt",
      "kms:Encrypt",
      "kms:ReEncryptFrom",
      "kms:ReEncryptTo"
    ]
    effect    = "Allow"
    resources = ["*"]

    principals {
      identifiers = [
        aws_iam_role.admin.arn,
        aws_iam_role.deployment.arn,
        aws_iam_role.development.arn
      ]
      type = "AWS"
    }
  }
}

// NOFLUID Automatic rotation is not compatible with SOPS
resource "aws_kms_key" "sops_kms_keys" {
  deletion_window_in_days = 7
  description             = "Key used to encrypt/decrypt SOPS file"
  key_usage               = "ENCRYPT_DECRYPT"
  policy                  = data.aws_iam_policy_document.sops_kms_keys_policy.json
}

resource "aws_kms_alias" "sops_kms_aliases" {
  name          = "alias/Sops"
  target_key_id = aws_kms_key.sops_kms_keys.key_id
}
