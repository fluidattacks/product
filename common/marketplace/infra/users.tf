data "aws_iam_policy_document" "okta_list_roles" {
  statement {
    sid    = "OktaListRoles"
    effect = "Allow"
    actions = [
      "iam:ListRoles",
      "iam:ListAccountAliases"
    ]
    resources = ["*"]
  }
}

resource "aws_iam_policy" "okta_list_roles" {
  name   = "OktaListRoles"
  policy = data.aws_iam_policy_document.okta_list_roles.json
}

resource "aws_iam_user" "okta" {
  name = "OktaSSOUser"
}

resource "aws_iam_user_policy_attachment" "okta_list_roles" {
  user       = aws_iam_user.okta.name
  policy_arn = aws_iam_policy.okta_list_roles.arn
}

resource "aws_iam_access_key" "okta" {
  status = "Active"
  user   = aws_iam_user.okta.name
}
