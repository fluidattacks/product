locals {
  roles = [
    "Deployment",
    "Development"
  ]
}

resource "aws_iam_role" "marketplace_admin" {
  name               = "MarketplaceAdmin"
  assume_role_policy = data.aws_iam_policy_document.okta_assume_role.json
  managed_policy_arns = [
    "arn:aws:iam::aws:policy/job-function/Billing",
    "arn:aws:iam::aws:policy/AWSMarketplaceSellerFullAccess",
    "arn:aws:iam::aws:policy/AWSVendorInsightsVendorFullAccess"
  ]
  max_session_duration = 32400
}

resource "aws_iam_role" "admin" {
  name               = "Admin"
  assume_role_policy = data.aws_iam_policy_document.okta_assume_role.json

  // NOFLUID This is the admin role used for management
  managed_policy_arns  = ["arn:aws:iam::aws:policy/AdministratorAccess"]
  max_session_duration = 32400
}

# Permissions
data "aws_caller_identity" "account" {}
data "aws_iam_policy_document" "read" {
  statement {
    sid    = "DynamoDBRead"
    effect = "Allow"
    actions = [
      "dynamodb:Describe*",
      "dynamodb:ListTagsOfResource"
    ]
    resources = ["arn:aws:dynamodb:${var.region}:${data.aws_caller_identity.account.account_id}:table/${local.state_db}"]
  }

  statement {
    sid    = "IAMReadPermissions"
    effect = "Allow"
    actions = [
      "iam:GetAccessKeyLastUsed",
      "iam:GetLoginProfile",
      "iam:GetOpenIDConnectProvider",
      "iam:GetPolicy",
      "iam:GetPolicyVersion",
      "iam:GetRole",
      "iam:GetRolePolicy",
      "iam:GetSAMLProvider",
      "iam:GetUser",
      "iam:GetUserPolicy",
      "iam:ListAccessKeys",
      "iam:ListAttachedRolePolicies",
      "iam:ListAttachedUserPolicies",
      "iam:ListOpenIDConnectProviders",
      "iam:ListOpenIDConnectProviderTags",
      "iam:ListPolicies",
      "iam:ListPolicyTags",
      "iam:ListPolicyVersions",
      "iam:ListRoles",
      "iam:ListRoleTags",
      "iam:ListSAMLProviders",
      "iam:ListSAMLProviderTags",
      "iam:ListUserPolicies",
      "iam:ListUsers",
      "iam:ListUserTags",
      "iam:ListRolePolicies"
    ]
    resources = [
      "arn:aws:iam::${data.aws_caller_identity.account.account_id}:oidc-provider/gitlab*",
      "arn:aws:iam::${data.aws_caller_identity.account.account_id}:policy/*",
      "arn:aws:iam::${data.aws_caller_identity.account.account_id}:saml-provider/Okta*",
      "arn:aws:iam::${data.aws_caller_identity.account.account_id}:role/*",
      "arn:aws:iam::${data.aws_caller_identity.account.account_id}:user/*"
    ]
  }

  statement {
    sid    = "KMSRead"
    effect = "Allow"
    actions = [
      "kms:DescribeKey",
      "kms:GetKeyPolicy",
      "kms:GetKeyRotationStatus",
      "kms:ListAliases",
      "kms:ListKeyPolicies",
      "kms:ListKeyRotations",
      "kms:ListKeys",
      "kms:ListResourceTags"
    ]
    resources = ["*"]
  }

  statement {
    sid    = "S3Read"
    effect = "Allow"
    actions = [
      "s3:Get*Configuration",
      "s3:GetBucket*",
      "s3:ListBucket*"
    ]
    resources = [
      "arn:aws:s3:::${local.state_bucket}",
      "arn:aws:s3:::${local.public_storage_bucket}"
    ]
  }

  // NOFLUID SNS subscription ARNs are not predictable
  statement {
    sid = "SNSRead"
    actions = [
      "sns:GetSubscriptionAttributes",
      "sns:ListSubscriptions",
      "sns:ListTagsForResource"
    ]
    effect    = "Allow"
    resources = ["*"]
  }

  statement {
    sid       = "SQSList"
    actions   = ["sqs:ListQueues"]
    effect    = "Allow"
    resources = ["*"]
  }

  statement {
    sid = "SQSRead"
    actions = [
      "sqs:GetQueueAttributes",
      "sqs:GetQueueUrl",
      "sqs:ListQueueTags"
    ]
    effect = "Allow"
    resources = [
      "arn:aws:sqs:${var.region}:${data.aws_caller_identity.account.account_id}:${local.sqs_queue_name}"
    ]
  }

  statement {
    sid       = "STSRead"
    effect    = "Allow"
    actions   = ["sts:GetCallerIdentity"]
    resources = ["*"]
  }

  statement {
    sid    = "TerraformStateRead"
    effect = "Allow"
    actions = [
      "dynamodb:DescribeTable",
      "dynamodb:GetItem",
      "s3:GetObject",
      "s3:ListBucket"
    ]
    resources = [
      "arn:aws:dynamodb:${var.region}:${data.aws_caller_identity.account.account_id}:table/${local.state_db}",
      "arn:aws:s3:::${local.state_bucket}",
      "arn:aws:s3:::${local.state_bucket}/*"
    ]
  }
}

data "aws_iam_policy_document" "write" {
  statement {
    sid    = "DynamoDBWrite"
    effect = "Allow"
    actions = [
      "dynamodb:CreateTable",
      "dynamodb:DeleteTable",
      "dynamodb:UpdateTable"
    ]
    resources = ["arn:aws:dynamodb:${var.region}:${data.aws_caller_identity.account.account_id}:table/${local.state_db}"]
  }

  // NOFLUID This is the admin role which will deploy Terraform resources
  statement {
    sid    = "IAMWritePermissions"
    effect = "Allow"
    actions = [
      "iam:AddClientIDToOpenIDConnectProvider",
      "iam:AttachRolePolicy",
      "iam:AttachUserPolicy",
      "iam:CreateAccessKey",
      "iam:CreateOpenIDConnectProvider",
      "iam:CreatePolicy",
      "iam:CreatePolicyVersion",
      "iam:CreateRole",
      "iam:CreateSAMLProvider",
      "iam:CreateUser",
      "iam:DeleteAccessKey",
      "iam:DeleteOpenIDConnectProvider",
      "iam:DeletePolicy",
      "iam:DeletePolicyVersion",
      "iam:DeleteRole",
      "iam:DeleteRolePolicy",
      "iam:DeleteSAMLProvider",
      "iam:DeleteUser",
      "iam:DeleteUserPolicy",
      "iam:DetachRolePolicy",
      "iam:DetachUserPolicy",
      "iam:PutRolePolicy",
      "iam:PutUserPolicy",
      "iam:RemoveClientIDFromOpenIDConnectProvider",
      "iam:SetDefaultPolicyVersion",
      "iam:UpdateAccessKey",
      "iam:UpdateAssumeRolePolicy",
      "iam:UpdateOpenIDConnectProviderThumbprint",
      "iam:UpdateRole",
      "iam:UpdateRoleDescription",
      "iam:UpdateSAMLProvider",
      "iam:UpdateUser",
      "iam:TagPolicy"
    ]
    resources = [
      "arn:aws:iam::${data.aws_caller_identity.account.account_id}:oidc-provider/gitlab*",
      "arn:aws:iam::${data.aws_caller_identity.account.account_id}:policy/*",
      "arn:aws:iam::${data.aws_caller_identity.account.account_id}:saml-provider/Okta*",
      "arn:aws:iam::${data.aws_caller_identity.account.account_id}:role/*",
      "arn:aws:iam::${data.aws_caller_identity.account.account_id}:user/*"
    ]
  }

  // NOFLUID The ARN of the key cannot be inferred before creation
  statement {
    sid    = "KMSWrite"
    effect = "Allow"
    actions = [
      "kms:CreateAlias",
      "kms:CreateKey",
      "kms:DeleteAlias",
      "kms:DisableKey",
      "kms:DisableKeyRotation",
      "kms:EnableKey",
      "kms:EnableKeyRotation",
      "kms:ScheduleKeyDeletion",
      "kms:UpdateAlias",
      "kms:UpdateKeyDescription",
      "kms:PutKeyPolicy"
    ]
    resources = ["*"]
  }

  statement {
    sid    = "S3Write"
    effect = "Allow"
    actions = [
      "s3:CreateBucket",
      "s3:DeleteBucket*",
      "s3:PutBucket*"
    ]
    resources = [
      "arn:aws:s3:::${local.state_bucket}",
      "arn:aws:s3:::${local.public_storage_bucket}"
    ]
  }

  statement {
    sid = "SNSMarketplaceSubscribe"
    actions = [
      "sns:Subscribe",
      "sns:Unsubscribe"
    ]
    effect = "Allow"
    resources = [
      var.sns_entitlements_topic,
      var.sns_subscriptions_topic
    ]
  }

  // NOFLUID SNS subscriptions ARNs are not predictable
  statement {
    sid = "SNSWrite"
    actions = [
      "sns:SetSubscriptionAttributes",
      "sns:TagResource",
      "sns:UntagResource"
    ]
    effect    = "Allow"
    resources = ["*"]
  }

  statement {
    sid = "SQSWrite"
    actions = [
      "sqs:CreateQueue",
      "sqs:DeleteQueue",
      "sqs:SetQueueAttributes",
      "sqs:TagQueue",
      "sqs:UntagQueue"
    ]
    effect = "Allow"
    resources = [
      "arn:aws:sqs:${var.region}:${data.aws_caller_identity.account.account_id}:${local.sqs_queue_name}"
    ]
  }

  statement {
    sid    = "TerraformStateWrite"
    effect = "Allow"
    actions = [
      "dynamodb:DeleteItem",
      "dynamodb:PutItem",
      "s3:DeleteObject",
      "s3:PutObject"
    ]
    resources = [
      "arn:aws:dynamodb:${var.region}:${data.aws_caller_identity.account.account_id}:table/${local.state_db}",
      "arn:aws:s3:::${local.state_bucket}",
      "arn:aws:s3:::${local.state_bucket}/*"
    ]
  }
}

resource "aws_iam_policy" "read_permissions" {
  name   = "ReadPermissions"
  policy = data.aws_iam_policy_document.read.json
}

resource "aws_iam_policy" "write_permissions" {
  name   = "WritePermissions"
  policy = data.aws_iam_policy_document.write.json
  tags = {
    "NOFLUID" = "f005_This_is_the_admin_role_which_will_deploy_Terraform_resources"
  }
}

# Deployment role
resource "aws_iam_role" "deployment" {
  name               = local.roles[0]
  assume_role_policy = data.aws_iam_policy_document.gitlab_assume_role_deployment.json
}

resource "aws_iam_role_policy_attachment" "deployment_read" {
  role       = aws_iam_role.deployment.name
  policy_arn = aws_iam_policy.read_permissions.arn
}

resource "aws_iam_role_policy_attachment" "deployment_write" {
  role       = aws_iam_role.deployment.name
  policy_arn = aws_iam_policy.write_permissions.arn
}


# Development role
resource "aws_iam_role" "development" {
  name               = local.roles[1]
  assume_role_policy = data.aws_iam_policy_document.gitlab_assume_role_development.json
}

resource "aws_iam_role_policy_attachment" "development_read" {
  role       = aws_iam_role.development.name
  policy_arn = aws_iam_policy.read_permissions.arn
}

# Cross-Account role
data "aws_iam_policy_document" "integrates_cross_account_assume_policy" {
  statement {
    actions = ["sts:AssumeRole"]
    effect  = "Allow"

    condition {
      test     = "ArnEquals"
      values   = ["arn:aws:iam::205810638802:role/prod_integrates"]
      variable = "aws:PrincipalArn"
    }
    condition {
      test     = "StringEquals"
      values   = [var.integrates_external_id]
      variable = "sts:ExternalId"
    }
    principals {
      identifiers = ["*"]
      type        = "AWS"
    }
  }
}

data "aws_iam_policy_document" "integrates_cross_account_policy" {
  statement {
    actions = [
      "aws-marketplace:BatchMeterUsage",
      "aws-marketplace:GetEntitlements",
      "aws-marketplace:MeterUsage",
      "aws-marketplace:ResolveCustomer"
    ]
    effect    = "Allow"
    resources = ["*"]
  }
}

resource "aws_iam_policy" "integrates_cross_account_policy" {
  name   = "IntegratesMarketplaceIntegration"
  policy = data.aws_iam_policy_document.integrates_cross_account_policy.json
}


resource "aws_iam_role" "integrates_cross_account_role" {
  name               = "IntegratesMarketplaceCrossAccount"
  assume_role_policy = data.aws_iam_policy_document.integrates_cross_account_assume_policy.json
}

resource "aws_iam_role_policy_attachment" "integrates_cross_account" {
  role       = aws_iam_role.integrates_cross_account_role.name
  policy_arn = aws_iam_policy.integrates_cross_account_policy.arn
}
