variable "integrates_external_id" {
  default   = "value"
  sensitive = true
  type      = string
}
variable "sns_entitlements_topic" {
  sensitive = true
  type      = string
}
variable "sns_subscriptions_topic" {
  sensitive = true
  type      = string
}
variable "region" {
  default = "us-east-1"
  type    = string
}
