locals {
  public_storage_bucket = "fluidattacks.marketplace"
}

resource "aws_s3_bucket" "public_storage" {
  bucket = local.public_storage_bucket
}

resource "aws_s3_bucket_ownership_controls" "storage_ownership" {
  bucket = aws_s3_bucket.public_storage.id

  rule {
    object_ownership = "BucketOwnerPreferred"
  }
}

resource "aws_s3_bucket_public_access_block" "storage_public_access" {
  bucket = aws_s3_bucket.public_storage.id

  block_public_acls       = false
  block_public_policy     = false
  ignore_public_acls      = false
  restrict_public_buckets = false
}

resource "aws_s3_bucket_acl" "storage_acl" {
  bucket = aws_s3_bucket.public_storage.id
  acl    = "public-read"

  depends_on = [
    aws_s3_bucket_ownership_controls.storage_ownership,
    aws_s3_bucket_public_access_block.storage_public_access
  ]
}
