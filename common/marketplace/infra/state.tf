locals {
  state_bucket = "fa-aws-marketplace-terraform"
  state_db     = "terraform-lock"
}

resource "aws_s3_bucket" "state_bucket" {
  bucket = local.state_bucket
}

resource "aws_s3_bucket_ownership_controls" "state_bucket" {
  bucket = aws_s3_bucket.state_bucket.id
  rule {
    object_ownership = "BucketOwnerPreferred"
  }
}

resource "aws_s3_bucket_server_side_encryption_configuration" "state_bucket" {
  bucket = aws_s3_bucket.state_bucket.id
  rule {
    apply_server_side_encryption_by_default {
      sse_algorithm = "AES256"
    }
  }
}

resource "aws_dynamodb_table" "state_table" {
  name                        = local.state_db
  hash_key                    = "LockID"
  deletion_protection_enabled = true
  billing_mode                = "PROVISIONED"
  read_capacity               = 5
  write_capacity              = 5

  attribute {
    name = "LockID"
    type = "S"
  }

  server_side_encryption {
    enabled = true
  }
}
