from enum import (
    Enum,
)
from typing import (
    TypedDict,
)


class Children(dict):
    name: str
    id: str
    children: list["Children"] | None


class CategoriesResponse(dict):
    children: list["Children"]


class ArticleResponse(TypedDict):
    id: str


class TokenResponse(TypedDict):
    access_token: str
    expires_in: int


class Action(Enum):
    ADD = "add"
    UPDATE = "update"
    DELETE = "delete"
