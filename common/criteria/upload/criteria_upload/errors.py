class ErrorUploadingFile(Exception):
    pass


class ErrorDeletingFiles(Exception):
    pass


class ErrorClearingTrash(Exception):
    pass


class ErrorOpeningFile(Exception):
    pass
