import contextlib
import logging
import os
import time
from collections.abc import AsyncGenerator, Awaitable, Callable
from typing import (
    Any,
)

import aiohttp
from jinja2 import (
    Environment,
    FileSystemLoader,
)

from criteria_upload.custom_types import (
    ArticleResponse,
    TokenResponse,
)
from criteria_upload.errors import (
    ErrorClearingTrash,
    ErrorDeletingFiles,
    ErrorOpeningFile,
    ErrorUploadingFile,
)
from criteria_upload.utils import (
    TEMPLATES_DIR,
    get_subcategories,
)

LOGGER = logging.getLogger()
logging.basicConfig(level=logging.INFO)
FI_ZOHO_CLIENT_ID = os.environ["ZOHO_CLIENT_ID"]
FI_ZOHO_CLIENT_SECRET = os.environ["ZOHO_CLIENT_SECRET"]
FI_ZOHO_ORG_ID = os.environ["ZOHO_ORG_ID"]
FI_ZOHO_REFRESH_TOKEN = os.environ["ZOHO_REFRESH_TOKEN"]
ZOHO_API_BASE_URL = "https://desk.zoho.com/api/v1"
SCOPE = (
    "Desk.articles.READ,Desk.articles.CREATE,"
    "Desk.articles.UPDATE,Desk.articles.DELETE,Desk.settings.READ"
)
DELAY = 1, 3


def create_token_getter() -> Callable[[], Awaitable[str]]:
    current_token = None
    current_expiration = None

    async def _access_token() -> str:
        """
        Returns a valid zoho access token, generating a new one when needed.

        https://desk.zoho.com/support/APIDocument.do#OauthTokens#RefreshingAccessTokens
        """
        nonlocal current_token, current_expiration
        current_time = int(time.time())

        if current_token and current_expiration and current_time < current_expiration:
            return current_token

        async with aiohttp.ClientSession() as session:
            async with session.post(
                "https://accounts.zoho.com/oauth/v2/token",
                params={
                    "scope": SCOPE,
                    "client_id": FI_ZOHO_CLIENT_ID,
                    "client_secret": FI_ZOHO_CLIENT_SECRET,
                    "grant_type": "refresh_token",
                    "refresh_token": FI_ZOHO_REFRESH_TOKEN,
                },
            ) as response:
                content: TokenResponse = await response.json()
                if not response.ok or "access_token" not in content:
                    raise ErrorUploadingFile("Couldn't get access token", response.status, content)
                current_token = content["access_token"]
                current_expiration = current_time + content["expires_in"]
                return current_token

    return _access_token


_get_access_token = create_token_getter()


@contextlib.asynccontextmanager
async def zoho_session() -> AsyncGenerator[aiohttp.ClientSession, None]:
    access_token = await _get_access_token()

    async with aiohttp.ClientSession(
        headers={
            "authorization": f"Zoho-oauthtoken {access_token}",
            "orgId": FI_ZOHO_ORG_ID,
        },
    ) as session:
        yield session


async def get_categories(category_id: int, category_name: str) -> dict[str, int]:
    async with zoho_session() as session:
        response = await session.get(
            f"{ZOHO_API_BASE_URL}/kbRootCategories/{category_id}/categoryTree",
        )
        response.raise_for_status()
        content = await response.json()
        return get_subcategories(content, category_name)


async def get_articles_in_category(
    category_id: int,
    start: int,
    limit: int,
) -> list[dict[str, Any]]:
    async with zoho_session() as session:
        response = await session.get(
            f"{ZOHO_API_BASE_URL}/articles?categoryId={category_id}"
            f"&limit={int(limit)}&from={int(start)}",
        )
        response.raise_for_status()
        content = await response.json()
        return list(content.get("data", []))


async def clear_trash_by_id(articles_ids: list[str]) -> None:
    LOGGER.info("Clearing trash")
    async with zoho_session() as session:
        response = await session.post(
            f"{ZOHO_API_BASE_URL}" + "/articleTranslations/trashedTranslations/delete",
            json={"ids": articles_ids},
        )
        if response.status != 200:
            raise ErrorClearingTrash("Couldn't clear trash", response.status, await response.text())
        LOGGER.info("Successfully cleared trash")


async def delete_articles(articles_ids: list[str]) -> bool:
    LOGGER.info("Deleting  articles")
    try:
        async with zoho_session() as session:
            response = await session.post(
                f"{ZOHO_API_BASE_URL}/articles/moveToTrash",
                json={"ids": articles_ids},
            )
            if response.status == 200:
                LOGGER.info(
                    "Successfully moved %s articles to trash.",
                    len(articles_ids),
                )
                await clear_trash_by_id(articles_ids)
            else:
                content = await response.json()
                LOGGER.error(
                    "Failed to move articles to trash: %s - %s",
                    response.status,
                    content,
                )
                return False
            return True
    except ErrorDeletingFiles as exc:
        LOGGER.error("Error deleting articles. %s", str(exc))
        return False


async def get_article_id(category_id: int, permalink: str) -> str:
    start = 1
    limit = 40
    article_id = ""

    while True:
        articles = await get_articles_in_category(category_id, start, limit)
        if not articles:
            LOGGER.error("Article not found %s in category %s", permalink, category_id)
            break
        articles = [article for article in articles if permalink in article.get("permalink", "")]
        if len(articles) == 1:
            article_id = articles[0]["id"]
            break
        start += limit
    return article_id


def get_article_payload(
    title: str,
    subcategory: int,
    permalink: str,
    html_content: str,
) -> dict[str, Any]:
    return {
        "seoDescription": title,
        "permission": "ALL",
        "title": title,
        "seoTitle": title,
        "seoKeywords": "Help Center, Knowledge Base",
        "isSEOEnabled": True,
        "answer": html_content,
        "categoryId": subcategory,
        "permalink": permalink,
        "status": "Published",
    }


async def update_article(
    title: str,
    subcategory: int,
    permalink: str,
    article_id: str,
    html_content: str,
) -> None:
    if not FI_ZOHO_CLIENT_ID:
        return
    async with zoho_session() as session:
        async with session.patch(
            f"{ZOHO_API_BASE_URL}/articles/{article_id}",
            json=get_article_payload(title, subcategory, permalink, html_content),
        ) as response:
            response_content: ArticleResponse = await response.json()
            if not response.ok:
                raise ErrorUploadingFile(
                    "Couldn't update article",
                    response.status,
                    response_content,
                    title,
                )


async def add_article(
    title: str,
    subcategory: int,
    permalink: str,
    html_content: str,
) -> None:
    if not FI_ZOHO_CLIENT_ID:
        return
    async with zoho_session() as session:
        async with session.post(
            f"{ZOHO_API_BASE_URL}/articles",
            json=get_article_payload(title, subcategory, permalink, html_content),
        ) as response:
            response_content: ArticleResponse = await response.json()
            if not response.ok:
                raise ErrorUploadingFile(
                    "Couldn't create article",
                    response.status,
                    response_content,
                    title,
                )


async def upload_module_intro(
    title: str,
    subcategory: int,
    module: str,
    context: dict[str, Any],
) -> None:
    LOGGER.info("Updating intro")
    permalink = f"criteria-{module}-intro"

    article_id = await get_article_id(subcategory, permalink)
    env = Environment(loader=FileSystemLoader(TEMPLATES_DIR))
    template = env.get_template(f"intros/{module}.html")

    html_content = template.render(context)
    return await update_article(title, subcategory, permalink, article_id, html_content)


async def delete_article(module: str, changes: dict, categories: dict) -> None:
    for vulnerability_id, category in changes["deleted"]:
        subcategory = categories[category]
        permalink = "criteria-" + module + "-" + vulnerability_id

        article_id = await get_article_id(subcategory, permalink)
        await delete_articles([article_id])


def read_html_file(file_path: str) -> str:
    try:
        with open(file_path, encoding="utf-8") as file:
            return file.read()
    except FileNotFoundError as exc:
        raise ErrorOpeningFile("File not found: ", file_path, exc) from exc
    except OSError as exc:
        raise ErrorOpeningFile("Error reading file : ", file_path, exc) from exc


async def update_article_body(
    article_id: str,
) -> bool:
    if not FI_ZOHO_CLIENT_ID:
        return False
    file_path = f"article_{article_id}.html"
    html_content = read_html_file(file_path)
    async with zoho_session() as session:
        async with session.patch(
            f"{ZOHO_API_BASE_URL}/articles/{article_id}",
            json={
                "answer": html_content,
                "status": "Review",
            },
        ) as response:
            response_content: ArticleResponse = await response.json()
            if not response.ok:
                raise ErrorUploadingFile(
                    "Couldn't update article body",
                    response.status,
                    response_content,
                )
            os.remove(file_path)
            return True


async def update_article_props(article: dict[str, Any]) -> bool:
    if not FI_ZOHO_CLIENT_ID:
        return False
    LOGGER.info("updating article %s", article["title"])
    article_id = article["id"]
    # Change the property you need
    json = {
        "status": "Published",
    }

    async with (
        zoho_session() as session,
        session.patch(
            f"{ZOHO_API_BASE_URL}/articles/{article_id}",
            json=json,
        ) as response,
    ):
        response_content: ArticleResponse = await response.json()
        if not response.ok:
            raise ErrorUploadingFile(
                "Couldn't update article body",
                response.status,
                response_content,
            )
        return True


async def get_article_by_id(article_id: int) -> bool:
    LOGGER.info("Getting article %s", article_id)

    async with zoho_session() as session:
        response = await session.get(f"{ZOHO_API_BASE_URL}/articles/{article_id}")
        response.raise_for_status()
        content = await response.json()

        answer = content.get("answer", "")
        with open(f"article_{article_id}.html", "w", encoding="utf-8") as file:
            file.write(answer)
        return True


async def update_category(category: int) -> bool:
    LOGGER.info("Updating articles in category: %s", category)
    start = 1
    per_page = 40
    total_articles = 0
    try:
        while True:
            articles = await get_articles_in_category(category, start, per_page)
            if not articles:
                LOGGER.info("No more articles found in category %s", category)
                break

            for article in articles:
                total_articles += 1
                await update_article_props(article)

            if len(articles) < per_page:
                break

            start += 40

        LOGGER.info(
            "Successfully updated %s articles in category %s.",
            total_articles,
            category,
        )
        return True

    except ErrorUploadingFile as exc:
        LOGGER.error("Error updating articles in category %s: %s", category, str(exc))
        return False
