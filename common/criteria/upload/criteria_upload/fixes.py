import asyncio
import logging
import os
from pathlib import (
    Path,
)
from typing import (
    Any,
)

import aiohttp
from jinja2 import (
    Environment,
    FileSystemLoader,
)

from criteria_upload.custom_types import (
    Action,
)
from criteria_upload.utils import (
    TEMPLATES_DIR,
    get_fixes,
    get_vulnerabilities,
    load_yaml,
    markdown_to_html,
)
from criteria_upload.zoho_desk import (
    DELAY,
    FI_ZOHO_CLIENT_ID,
    add_article,
    delete_articles,
    get_article_id,
    get_categories,
    update_article,
    upload_module_intro,
)

LOGGER = logging.getLogger()
logging.basicConfig(level=logging.INFO)


async def get_context(article: dict[str, Any], vulnerabilities: dict[str, Any]) -> dict[str, Any]:
    vulnerability_id = article["vulnerability_id"]
    return {
        "fix": article,
        "vulnerability_id": vulnerability_id,
        "path": str(Path(article["solution"]["language"]) / Path(vulnerability_id)),
        "vulnerability_name": vulnerabilities[vulnerability_id]["en"]["title"],
        "snippet_language": {"aws": "hcl", "azure": "hcl"}.get(
            article["solution"]["language"],
            article["solution"]["language"],
        ),
    }


async def generate_template(article: dict[str, Any], vulnerabilities: dict[str, Any]) -> str:
    env = Environment(loader=FileSystemLoader(TEMPLATES_DIR))
    env.filters["markdown_to_html"] = markdown_to_html
    template = env.get_template("fixes.html")
    context = await get_context(article, vulnerabilities)
    rendered = template.render(context)
    return rendered


async def upload_zoho_article(
    *,
    article: dict[str, Any],
    category_name: str,
    categories: dict[str, int],
    vulnerabilities: dict[str, Any],
    action: Action,
) -> None:
    fix_title = article["title"]
    LOGGER.info("Uploading file: %s", fix_title)

    language = article["solution"]["language"]
    title = fix_title + " - " + language
    subcategory = categories[language]
    vulnerability_id = article["vulnerability_id"]
    permalink = "criteria-" + category_name + "-" + language + "-" + vulnerability_id
    html_content = await generate_template(article, vulnerabilities)
    if action == Action.ADD:
        LOGGER.info("Adding article: %s", title)
        return await add_article(title, subcategory, permalink, html_content)

    article_id = await get_article_id(subcategory, permalink)

    return await update_article(title, subcategory, permalink, article_id, html_content)


def get_changes(old_file: str, current_file: str) -> dict:
    old_data = load_yaml(old_file)
    new_data = load_yaml(current_file)
    old_vulns_dict = {vuln["vulnerability_id"]: vuln for vuln in old_data}
    new_vulns_dict = {vuln["vulnerability_id"]: vuln for vuln in new_data}

    deleted = []
    modified = []

    for vuln_id, vuln in old_vulns_dict.items():
        if vuln_id not in new_vulns_dict:
            deleted.append((vuln_id, vuln["solution"]["language"]))
        elif old_vulns_dict[vuln_id] != new_vulns_dict[vuln_id]:
            modified.append((vuln_id, Action.UPDATE))

    for vuln_id in new_vulns_dict:
        if vuln_id not in old_vulns_dict:
            modified.append((vuln_id, Action.ADD))

    return {"deleted": deleted, "modified": modified}


def get_all_changes(old_folder: str, current_folder: str) -> dict:
    changes: dict = {"added": [], "deleted": [], "modified": []}
    for file_name in os.listdir(old_folder):
        old_file_path = os.path.join(old_folder, file_name)
        current_file_path = os.path.join(current_folder, file_name)

        file_changes = get_changes(old_file_path, current_file_path)
        changes["added"].extend(file_changes["added"])
        changes["deleted"].extend(file_changes["deleted"])
        changes["modified"].extend(file_changes["modified"])

    return changes


async def upload_fix(
    fix: dict,
    module: str,
    categories: dict,
    vulnerabilities: dict,
    action: Action,
) -> None:
    sent = False
    while not sent:
        try:
            await upload_zoho_article(
                article=fix,
                category_name=module,
                categories=categories,
                vulnerabilities=vulnerabilities,
                action=action,
            )
            sent = True
        except aiohttp.ClientResponseError as exc:
            if exc.status == 429:
                LOGGER.info(
                    "Rate limit exceeded. Retrying in %s seconds.",
                    DELAY,
                )
                await asyncio.sleep(DELAY)  # type: ignore[call-overload]
            else:
                raise exc


def get_intro_context(fixes: dict) -> dict:
    categorized: dict[str, list[str]] = {}
    for fix in fixes:
        categorized.setdefault(fix["solution"]["language"], []).append(fix["vulnerability_id"])
    fixes_language_dict = {fix["vulnerability_id"]: fix for fix in fixes}

    context = {
        "categorized": categorized,
        "fixes": fixes_language_dict,
    }

    return context


async def delete_fixes_article(module: str, changes: dict, categories: dict) -> None:
    for vulnerability_id, category in changes["deleted"]:
        subcategory = categories[category]
        permalink = "criteria-" + module + "-" + category + "-" + vulnerability_id

        article_id = await get_article_id(subcategory, permalink)
        await delete_articles([article_id])


async def process_changes(
    *,
    vuln_changed: dict,
    fixes_language_dict: dict,
    module: str,
    categories: dict,
    vulnerabilities: dict,
    update_intro: bool,
) -> bool:
    for vuln_id, action in vuln_changed["modified"]:
        fix = fixes_language_dict.get(vuln_id)
        if fix:
            await upload_fix(fix, module, categories, vulnerabilities, action)

    await delete_fixes_article(module, vuln_changed, categories)
    added = [item for item in vuln_changed["modified"] if item[1] == Action.ADD]
    if len(added) > 0 or len(vuln_changed["deleted"]) > 0:
        update_intro = True

    return update_intro


async def upload_fixes_articles(module: str) -> bool:
    if not FI_ZOHO_CLIENT_ID:
        return False

    fixes = await get_fixes()
    old_folder = "../src/solutions/old"
    current_folder = "../src/solutions"

    vulnerabilities = await get_vulnerabilities()
    categories = await get_categories(944043000006985001, module.capitalize())

    update_intro = False

    for file_name in os.listdir(old_folder):
        old_file_path = os.path.join(old_folder, file_name)
        current_file_path = os.path.join(current_folder, file_name)

        vuln_changed = get_changes(old_file_path, current_file_path)
        file_root, _ = os.path.splitext(file_name)
        fixes_language_dict = {
            fix["vulnerability_id"]: fix
            for fix in fixes
            if fix["solution"]["language"] == file_root
        }

        update_intro = await process_changes(
            vuln_changed=vuln_changed,
            fixes_language_dict=fixes_language_dict,
            module=module,
            categories=categories,
            vulnerabilities=vulnerabilities,
            update_intro=update_intro,
        )

    if update_intro:
        context = get_intro_context(fixes)
        await upload_module_intro("Introduction", categories["Introduction"], module, context)

    return True
