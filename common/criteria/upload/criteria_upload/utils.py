import logging
import os
import re
from collections.abc import Callable
from html import escape
from typing import (
    Any,
)

import aiofiles
import yaml

from criteria_upload.custom_types import (
    Action,
    CategoriesResponse,
)

LANGUAGE_MAP = {
    "aws": "Aws",
    "azure": "Azure",
    "csharp": "C-Sharp",
    "go": "Go",
    "java": "Java",
    "python": "Python",
    "php": "PHP",
    "scala": "Scala",
    "ruby": "Ruby",
    "typescript": "TypeScript",
    "dart": "Dart",
    "elixir": "Elixir",
    "swift": "Swift",
    "docker": "Docker",
    "terraform": "Terraform",
}
CODE_BLOCK_STYLES = {
    "container": "box-sizing: border-box; background-color: #f6f8fa; padding: 16px; "
    "border-radius: 6px; overflow: auto; margin-top: 0px; margin-bottom: 16px;",
    "pre": "box-sizing: border-box; margin: 0; padding: 0; background-color: inherit;",
    "code": "box-sizing: border-box; background-color: inherit; border: none; font: inherit; "
    "float: left; min-width: 100%;",
}

CATEGORIES_ID = {
    "vulnerabilities": 944043000007045094,
    "requirements": 944043000007045132,
    "compliance": 944043000007045121,
    "fixes": 944043000007045143,
}
TEMPLATES_DIR = "criteria_upload/templates"
LOGGER = logging.getLogger()
logging.basicConfig(level=logging.INFO)


def vulnerabilities_loader() -> Callable:
    vulnerabilities_data = None

    async def load_vulnerabilities() -> dict[str, dict[str, Any]]:
        nonlocal vulnerabilities_data
        if vulnerabilities_data is None:
            async with aiofiles.open(
                os.environ["VULNERABILITIES_FILE"],
                encoding="utf-8",
            ) as reader:
                vulnerabilities_data = yaml.load(await reader.read(), Loader=yaml.FullLoader)

        return vulnerabilities_data

    return load_vulnerabilities


def requirements_loader() -> Callable:
    requirements_data = None

    async def load_requirements() -> dict[str, dict[str, Any]]:
        nonlocal requirements_data
        if requirements_data is None:
            async with aiofiles.open(os.environ["REQUIREMENTS_FILE"], encoding="utf-8") as reader:
                requirements_data = yaml.load(await reader.read(), Loader=yaml.FullLoader)

        return requirements_data

    return load_requirements


def fixes_loader() -> Callable:
    fixes_data: list[dict[str, Any]] | None = None

    async def load_fixes() -> list[dict[str, Any]]:
        nonlocal fixes_data
        if fixes_data is None:
            fixes_data = []
            fixes_dir_path = os.environ["FIXES_DIR"]
            for fixes_file in os.listdir(fixes_dir_path):
                async with aiofiles.open(
                    os.path.join(fixes_dir_path, fixes_file),
                    encoding="utf-8",
                    errors="ignore",
                ) as reader:
                    fixes_data.extend(yaml.load(await reader.read(), Loader=yaml.FullLoader))

        return fixes_data

    return load_fixes


def compliance_loader() -> Callable:
    compliance_data = None

    async def load_compliance() -> dict[str, dict[str, Any]]:
        nonlocal compliance_data
        if compliance_data is None:
            async with aiofiles.open(os.environ["COMPLIANCE_FILE"], encoding="utf-8") as reader:
                compliance_data = yaml.load(await reader.read(), Loader=yaml.FullLoader)

        return compliance_data

    return load_compliance


def get_subcategories(categories: CategoriesResponse, category_name: str) -> dict[str, int]:
    vulnerabilities_subcategories: dict[str, int] = {}

    for child in categories["children"]:
        if child["name"] == category_name and child.get("children"):
            for sub_subcategory in child.get("children", []):
                sub_subcategory_name = sub_subcategory["name"]
                sub_subcategory_id = int(sub_subcategory["id"])
                vulnerabilities_subcategories[sub_subcategory_name] = sub_subcategory_id
    return vulnerabilities_subcategories


async def read_html_file(file_path: str) -> str:
    async with aiofiles.open(file_path) as file:
        content = await file.read()
    return content


def markdown_to_html(text: str) -> str:
    # Remove all occurrences of the pattern
    text = re.sub(r"\s\{\d+(?:,\d+)*\}", "", text)

    # Convert \n\n to <br><br> for paragraph breaks
    text = text.replace("\n\n", "<br><br>")

    # Convert **bold** text to <strong>bold</strong>
    text = re.sub(r"\*\*(.*?)\*\*", r"<strong>\1</strong>", text)

    # Convert code blocks with ```language code```
    text = re.sub(
        r"```(\w+)\s*(.*?)```",
        lambda m: f"""
        <div style="{CODE_BLOCK_STYLES['container']}">
            <pre style="{CODE_BLOCK_STYLES['pre']}">
                <code style="{CODE_BLOCK_STYLES['code']}">{escape(m.group(2))}</code>
            </pre>
        </div>""",
        text,
        flags=re.DOTALL,
    )

    # Handle inline code blocks with `
    text = re.sub(
        r"`([^`]*)`",
        r"""
        <code style="background-color: #f6f8fa; padding: 2px 4px;
        border-radius: 4px;">\1</code>""",
        text,
    )
    # Add a new line before each list item
    text = re.sub(r"(?<!<br>)\s*-\s+", r"<br>- ", text)

    return text


def load_yaml(file_path: str) -> dict:
    with open(file_path, "rb") as file:
        return yaml.safe_load(file)


def get_changes(old_file: str, current_file: str) -> dict[str, list[tuple[str, Any]]]:
    old_data = load_yaml(old_file)
    new_data = load_yaml(current_file)

    deleted = []
    modified = []

    for vuln_id, vuln_data in old_data.items():
        if vuln_id not in new_data:
            deleted.append((vuln_id, vuln_data.get("category", "")))
        elif old_data[vuln_id] != new_data[vuln_id]:
            modified.append((vuln_id, Action.UPDATE))

    for vuln_id, vuln_data in new_data.items():
        if vuln_id not in old_data:
            modified.append((vuln_id, Action.ADD))

    return {"deleted": deleted, "modified": modified}


get_vulnerabilities = vulnerabilities_loader()
get_requirements = requirements_loader()
get_fixes = fixes_loader()
get_compliance = compliance_loader()
