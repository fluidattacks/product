# shellcheck shell=bash

function _run_fixes {
  local current_dir="../src/solutions"
  local changed_files

  mkdir -p "${current_dir}/old"
  changed_files=$(git diff --name-only HEAD^ HEAD -- "${current_dir}")
  if [ -z "${changed_files}" ]; then
    info "No changes detected in fixes. Exiting..."
    return 0
  fi
  info "Changed files: ${changed_files}"
  for file in ${changed_files}; do
    local old_file
    old_file="${current_dir}/old/$(basename "${file}")"
    git show HEAD^:"${file}" > "${old_file}"
  done
  poetry run criteria-upload "fixes"
  rm -r "${current_dir}/old"
}

function _run_others {
  local module="${1}"
  local old_file="../src/${module}/old_data.yaml"
  local current_file="../src/${module}/data.yaml"

  info "Checking criteria ${module}"
  if git diff --quiet HEAD^ HEAD -- "${current_file}"; then
    info "No changes detected in ${module}. Exiting..."
    return 0
  fi
  git show HEAD^:"../src/${module}/data.yaml" > "${old_file}"
  poetry run criteria-upload "${module}"
  rm "${old_file}"
}

function _run {
  local ZOHO_COMMON_SECRETS_LIST=(
    ZOHO_CLIENT_ID
    ZOHO_CLIENT_SECRET
    ZOHO_REFRESH_TOKEN
    ZOHO_ORG_ID
  )
  export COMPLIANCE_FILE="__argCriteriaSrc__/compliance/data.yaml"
  export REQUIREMENTS_FILE="__argCriteriaSrc__/requirements/data.yaml"
  export VULNERABILITIES_FILE="__argCriteriaSrc__/vulnerabilities/data.yaml"
  export FIXES_DIR="__argCriteriaSrc__/solutions"

  aws_login "dev" "3600"
  sops_export_vars __argSecretsDev__ "${ZOHO_COMMON_SECRETS_LIST[@]}"

  case "${1:-}" in
    fixes) _run_fixes "${@}" ;;
    compliance | requirements | vulnerabilities) _run_others "${@}" ;;
    *) error "Invalid argument '${1:-}'. Must provide either 'fixes', 'compliance', 'requirements' or 'vulnerabilities' as the first argument" ;;
  esac
}

function _lint {
  if [[ ${CI:-} == "" ]]; then
    poetry run ruff format --config ruff.toml
    poetry run ruff check --config ruff.toml --fix
  else
    poetry run ruff format --config ruff.toml --diff
    poetry run ruff check --config ruff.toml
  fi

  poetry run mypy --config-file mypy.ini criteria_upload
}

function main {
  local dir="common/criteria/upload"

  pushd "${dir}" || error "${dir} directory not found"
  poetry install

  case "${1:-}" in
    run) _run "${@:2}" ;;
    lint) _lint "${@:2}" ;;
    *) error "Invalid argument '${1:-}'. Must provide either 'run' or 'lint' as the first argument" ;;
  esac
}

main "${@}"
