resource "coralogix_tco_policies_traces" "tco_policies" {
  policies = [
    {
      name     = "GraphQL to Mid"
      priority = "medium"
      actions = {
        rule_type = "includes"
        names     = ["graphql_"]
      }
      archive_retention_id = "afc87366-2fa6-42a8-b34f-fd1b14d52a72"
    },
    {
      name     = "Main HTTP requests"
      priority = "medium"
      actions = {
        rule_type = "is"
        names = [
          "/api",
          "get",
          "head",
          "options",
          "post",
          "put",
          "patch",
          "delete",
        ]
      }
      archive_retention_id = "55afe7ac-aca6-4fa8-bb86-3a003a65032e"
    },
    {
      name     = "Docs and Airs traces"
      priority = "high"
      applications = {
        rule_type = "is"
        names     = ["docs", "airs"]
      }
      archive_retention_id = "55afe7ac-aca6-4fa8-bb86-3a003a65032e"
    },
  ]
}
