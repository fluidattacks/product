resource "coralogix_tco_policies_logs" "tco_policies" {
  policies = [
    {
      name       = "Block non-production logs" # For TCO quota limits
      priority   = "block"
      severities = ["debug", "verbose", "info", "warning", "error", "critical"]
      applications = {
        rule_type = "is"
        names     = ["dev"]
      }
    },
    {
      name       = "Block ephemeral logs" # For TCO quota limits
      priority   = "block"
      severities = ["debug", "verbose", "info", "warning", "error", "critical"]
      subsystems = {
        rule_type = "includes"
        names     = ["atfluid"]
      }
    },
    {
      name                 = "High level logs"
      priority             = "high"
      severities           = ["error", "critical"]
      archive_retention_id = "55afe7ac-aca6-4fa8-bb86-3a003a65032e"
    },
    {
      name                 = "Mid level logs"
      priority             = "medium"
      severities           = ["warning"]
      archive_retention_id = "afc87366-2fa6-42a8-b34f-fd1b14d52a72"
    },
    {
      name                 = "Low level logs"
      priority             = "low"
      severities           = ["info", "debug", "verbose"]
      archive_retention_id = "d9cd5169-7aa2-453c-8cb1-dc4b38debaba"
    }
  ]
}
