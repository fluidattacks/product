locals {
  nat = {
    gateways = [
      {
        name          = "us-east-1a"
        public_subnet = "ci_1"
        tags          = {}
      },
      {
        name          = "us-east-1b"
        public_subnet = "ci_2"
        tags          = {}
      },
    ]
    routes = [
      {
        subnet  = "lambda_1",
        gateway = "us-east-1a",
        tags    = {}
      },
      {
        subnet  = "lambda_2",
        gateway = "us-east-1b",
        tags    = {}
      },
    ]
  }
}


resource "aws_eip" "main" {
  for_each = {
    for gateway in local.nat.gateways : gateway.name => gateway
  }

  tags = merge(
    {
      Name                = "nat_${each.key}"
      "fluidattacks:line" = "cost"
      "fluidattacks:comp" = "common"
    },
    each.value.tags,
  )
}


resource "aws_nat_gateway" "main" {
  for_each = {
    for gateway in local.nat.gateways : gateway.name => gateway
  }

  subnet_id     = aws_subnet.main[each.value.public_subnet].id
  allocation_id = aws_eip.main[each.key].id

  tags = merge(
    {
      Name                = each.key
      "fluidattacks:line" = "cost"
      "fluidattacks:comp" = "common"
    },
    each.value.tags,
  )
}

resource "aws_route_table" "main" {
  for_each = {
    for route in local.nat.routes : route.subnet => route
  }

  vpc_id = aws_vpc.fluid-vpc.id

  # NAT64
  # https://docs.aws.amazon.com/vpc/latest/userguide/nat-gateway-nat64-dns64.html#nat-gateway-nat64-what-is
  route {
    ipv6_cidr_block = "64:ff9b::/96"
    nat_gateway_id  = aws_nat_gateway.main[each.value.gateway].id
  }

  route {
    ipv6_cidr_block = "::/0"
    gateway_id      = aws_internet_gateway.fluid-vpc.id
  }

  route {
    cidr_block     = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.main[each.value.gateway].id
  }

  tags = merge(
    {
      Name                = each.key
      "fluidattacks:line" = "cost"
      "fluidattacks:comp" = "common"
    },
    each.value.tags,
  )
}

resource "aws_route_table_association" "main" {
  for_each = {
    for route in local.nat.routes : route.subnet => route
  }

  subnet_id      = aws_subnet.main[each.value.subnet].id
  route_table_id = aws_route_table.main[each.key].id
}
