# Cloudtrail
resource "aws_cloudtrail" "master_cloudtrail" {
  name                          = "master-cloudtrail"
  s3_bucket_name                = aws_s3_bucket.master_cloudtrail_bucket.id
  s3_key_prefix                 = "cloudtrail"
  include_global_service_events = true
  enable_log_file_validation    = true
  enable_logging                = true
  is_multi_region_trail         = true
  kms_key_id                    = aws_kms_key.cloudtrail_key.arn

  event_selector {
    read_write_type           = "All"
    include_management_events = true
  }
  tags = {
    "Name"              = "cloudtrail"
    "fluidattacks:line" = "cost"
    "fluidattacks:comp" = "common"
  }
}

resource "aws_cloudtrail" "s3_data_events_cloudtrail" {
  name                          = "s3-data-events-cloudtrail"
  s3_bucket_name                = aws_s3_bucket.master_cloudtrail_bucket.id
  s3_key_prefix                 = "cloudtrail-data-events"
  include_global_service_events = true
  enable_log_file_validation    = true
  enable_logging                = true
  is_multi_region_trail         = true
  kms_key_id                    = aws_kms_key.cloudtrail_key.arn

  event_selector {
    read_write_type           = "All"
    include_management_events = false

    data_resource {
      type   = "AWS::S3::Object"
      values = ["arn:aws:s3"]
    }
  }

  tags = {
    "Name"              = "data_events_cloudtrail"
    "fluidattacks:line" = "cost"
    "fluidattacks:comp" = "common"
  }
}


resource "aws_kms_key" "cloudtrail_key" {
  description              = "KMS key for encrypting CloudTrail logs"
  deletion_window_in_days  = 10
  enable_key_rotation      = true
  customer_master_key_spec = "SYMMETRIC_DEFAULT"

  policy = jsonencode(
    {
      "Version" : "2012-10-17",
      "Id" : "auto-sns-1",
      "Statement" : [
        {
          "Sid" : "AllowCloudTrailToUseTheKey",
          "Effect" : "Allow",
          "Principal" : {
            "Service" : "cloudtrail.amazonaws.com"
          },
          "Action" : [
            "kms:Decrypt",
            "kms:GenerateDataKey"
          ],
          "Resource" : "*"
        },
        {
          "Sid" : "AllowAdminsFullAccessToThisKMSKey",
          "Effect" : "Allow",
          "Principal" : {
            "AWS" : [
              "arn:aws:iam::205810638802:root",
              "arn:aws:iam::205810638802:role/prod_common"
            ]
          },
          "Action" : "kms:*",
          "Resource" : "*"
        }
      ]
    }
  )

  tags = {
    "Name"              = "cloudtrail_kms_key"
    "fluidattacks:line" = "cost"
    "fluidattacks:comp" = "common"
  }
}

# S3 Bucket for storing CloudTrail logs access logs
resource "aws_s3_bucket" "cloudtrail_access_log_bucket" {
  bucket        = "common-cloudtrail-access-log-bucket"
  force_destroy = true

  tags = {
    "Name"              = "S3 Cloudtrail Access Logs"
    "fluidattacks:line" = "cost"
    "fluidattacks:comp" = "common"
    "NOFLUID"           = "f031_logging_bucket_needs_LogDelivery_permission"
  }
}

resource "aws_s3_bucket_acl" "cloudtrail_access_log_bucket_acl" {
  bucket = aws_s3_bucket.cloudtrail_access_log_bucket.id
  acl    = "log-delivery-write"
}

resource "aws_s3_bucket_versioning" "cloudtrail_access_log_bucket" {
  bucket = aws_s3_bucket.cloudtrail_access_log_bucket.id

  versioning_configuration {
    status = "Enabled"
  }
}

resource "aws_s3_bucket_public_access_block" "cloudtrail_access_log_bucket" {
  bucket = aws_s3_bucket.cloudtrail_access_log_bucket.id

  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}

# S3 bucket for storing CloudTrail logs
resource "aws_s3_bucket" "master_cloudtrail_bucket" {
  bucket        = "common-master-cloudtrail-bucket"
  force_destroy = true

  tags = {
    "Name"              = "S3 Master Cloudtrail"
    "fluidattacks:line" = "cost"
    "fluidattacks:comp" = "common"
  }
}

resource "aws_s3_bucket_versioning" "master_cloudtrail_bucket_versioning" {
  bucket = aws_s3_bucket.master_cloudtrail_bucket.id
  versioning_configuration {
    status = "Enabled"
  }
}

resource "aws_s3_bucket_logging" "master_cloudtrail_bucket_logging" {
  bucket = aws_s3_bucket.master_cloudtrail_bucket.id

  target_bucket = aws_s3_bucket.cloudtrail_access_log_bucket.id
  target_prefix = "log/"
}

resource "aws_s3_bucket_public_access_block" "master_cloudtrail_bucket" {
  bucket = aws_s3_bucket.master_cloudtrail_bucket.id

  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}

data "aws_iam_policy_document" "cloudtrail_policy" {
  statement {
    sid    = "AWSCloudTrailAclCheck"
    effect = "Allow"

    principals {
      type        = "Service"
      identifiers = ["cloudtrail.amazonaws.com"]
    }

    actions   = ["s3:GetBucketAcl"]
    resources = [aws_s3_bucket.master_cloudtrail_bucket.arn]
  }

  statement {
    sid    = "AWSCloudTrailWrite"
    effect = "Allow"

    principals {
      type        = "Service"
      identifiers = ["cloudtrail.amazonaws.com"]
    }

    actions = ["s3:PutObject"]
    resources = [
      "${aws_s3_bucket.master_cloudtrail_bucket.arn}/cloudtrail/AWSLogs/${data.aws_caller_identity.main.account_id}/*",
      "${aws_s3_bucket.master_cloudtrail_bucket.arn}/cloudtrail-data-events/AWSLogs/${data.aws_caller_identity.main.account_id}/*"
    ]

    condition {
      test     = "StringEquals"
      variable = "s3:x-amz-acl"
      values   = ["bucket-owner-full-control"]
    }
  }
}

resource "aws_s3_bucket_policy" "cloudtrail_policy" {
  bucket = aws_s3_bucket.master_cloudtrail_bucket.id
  policy = data.aws_iam_policy_document.cloudtrail_policy.json
}

# Cost anomaly
resource "aws_ce_anomaly_monitor" "cost_anomaly_alert" {
  name              = "cost_anomaly_alert"
  monitor_type      = "DIMENSIONAL"
  monitor_dimension = "SERVICE"
  tags = {
    "Name"              = "cost_anomaly_alert_monitor"
    "fluidattacks:line" = "cost"
    "fluidattacks:comp" = "common"
  }
}

resource "aws_sns_topic" "cost_anomaly_alert" {
  name              = "cost_anomaly_alert"
  kms_master_key_id = "alias/cost-alert-key-alias"

  policy = jsonencode(
    {
      Version = "2008-10-17"
      Statement = [
        {
          Sid    = "default"
          Effect = "Allow"
          Principal = {
            AWS = "*"
          }
          Action = [
            "sns:GetTopicAttributes",
            "sns:SetTopicAttributes",
            "sns:AddPermission",
            "sns:RemovePermission",
            "sns:DeleteTopic",
            "sns:Subscribe",
            "sns:ListSubscriptionsByTopic",
            "sns:Publish",
          ]
          Resource = ["*"]
          Condition = {
            StringEquals = {
              "AWS:SourceOwner" = "205810638802"
            }
          }
        },
        {
          Sid    = "AWSAnomalyDetectionSNSPublishingPermissions",
          Effect = "Allow",
          Principal = {
            Service = ["costalerts.amazonaws.com"]
          },
          Action   = ["sns:Publish"]
          Resource = ["*"]
        },
      ]
    }
  )

  tags = {
    "Name"              = "cost_anomaly_alert"
    "fluidattacks:line" = "cost"
    "fluidattacks:comp" = "common"
  }
}

resource "aws_sns_topic_subscription" "cost_anomaly_alert_email" {
  protocol  = "email"
  endpoint  = "development@fluidattacks.com"
  topic_arn = aws_sns_topic.cost_anomaly_alert.arn
}

resource "aws_ce_anomaly_subscription" "cost_anomaly_alert" {
  name      = "cost_anomaly_alert"
  frequency = "IMMEDIATE"

  monitor_arn_list = [
    aws_ce_anomaly_monitor.cost_anomaly_alert.arn,
  ]

  subscriber {
    type    = "SNS"
    address = aws_sns_topic.cost_anomaly_alert.arn
  }

  threshold_expression {
    dimension {
      key           = "ANOMALY_TOTAL_IMPACT_PERCENTAGE"
      values        = ["15"]
      match_options = ["GREATER_THAN_OR_EQUAL"]
    }
  }
  tags = {
    "Name"              = "cost_anomaly_alert_subscription"
    "fluidattacks:line" = "cost"
    "fluidattacks:comp" = "common"
  }
}

# Flow logs
resource "aws_flow_log" "flow_log" {
  log_destination = aws_cloudwatch_log_group.flow_log_group.arn
  iam_role_arn    = aws_iam_role.flow_role.arn
  vpc_id          = aws_vpc.fluid-vpc.id
  traffic_type    = "ALL"
  log_format      = "$${version} $${vpc-id} $${subnet-id} $${instance-id} $${interface-id} $${type} $${srcaddr} $${dstaddr} $${srcport} $${dstport} $${pkt-srcaddr} $${pkt-dstaddr} $${protocol} $${bytes} $${packets} $${start} $${end} $${action} $${tcp-flags} $${pkt-src-aws-service} $${pkt-dst-aws-service} $${traffic-path} $${flow-direction} $${log-status}"
  tags = {
    "Name"              = "fluid-vpc"
    "fluidattacks:line" = "cost"
    "fluidattacks:comp" = "common"
  }
}

resource "aws_cloudwatch_log_group" "flow_log_group" {
  name = "flow_log_group"
  tags = {
    "Name"              = "flow_log_group"
    "fluidattacks:line" = "cost"
    "fluidattacks:comp" = "common"
  }
}

resource "aws_iam_role" "flow_role" {
  name = "flow_role"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "",
      "Effect": "Allow",
      "Principal": {
        "Service": "vpc-flow-logs.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
  tags = {
    "Name"              = "flow_role"
    "fluidattacks:line" = "cost"
    "fluidattacks:comp" = "common"
  }
}

resource "aws_iam_role_policy" "flow_policy" {
  name = "flow_policy"
  role = aws_iam_role.flow_role.id

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "logs:CreateLogGroup",
        "logs:CreateLogStream",
        "logs:PutLogEvents",
        "logs:DescribeLogGroups",
        "logs:DescribeLogStreams"
      ],
      "Effect": "Allow",
      "Resource": [
        "${aws_cloudwatch_log_group.flow_log_group.arn}:log-stream:*",
        "${aws_cloudwatch_log_group.flow_log_group.arn}"
      ]
    }
  ]
}
EOF
}

# KMS Keys
resource "aws_kms_key" "sns_key_event_bridge" {
  description              = "KMS event_bridge to SNS"
  deletion_window_in_days  = 10
  enable_key_rotation      = true
  customer_master_key_spec = "SYMMETRIC_DEFAULT"
  policy = jsonencode(
    {
      "Version" : "2012-10-17",
      "Id" : "auto-sns-1",
      "Statement" : [
        {
          "Sid" : "Allow access through SNS for all principals in the account that are authorized to use SNS",
          "Effect" : "Allow",
          "Principal" : {
            "AWS" : "arn:aws:iam::205810638802:root"
          },
          "Action" : [
            "kms:Decrypt",
            "kms:GenerateDataKey*",
            "kms:CreateGrant",
            "kms:ListGrants",
            "kms:DescribeKey"
          ],
          "Resource" : "*",
          "Condition" : {
            "StringEquals" : {
              "kms:CallerAccount" : "205810638802",
              "kms:ViaService" : "sns.us-east-1.amazonaws.com"
            }
          }
        },
        {
          "Sid" : "Key Administrators",
          "Effect" : "Allow",
          "Principal" : {
            "AWS" : [
              "arn:aws:iam::205810638802:root",
              "arn:aws:iam::205810638802:role/prod_common"
            ]
          },
          "Action" : "kms:*",
          "Resource" : "*"
        },
        {
          "Sid" : "Allow EventBridge to use the key",
          "Effect" : "Allow",
          "Principal" : {
            "Service" : "events.amazonaws.com"
          },
          "Action" : [
            "kms:Decrypt",
            "kms:GenerateDataKey"
          ],
          "Resource" : "*"
        }
      ]
    }
  )
  tags = {
    "Name"              = "SNS_key_event_bridge"
    "fluidattacks:line" = "cost"
    "fluidattacks:comp" = "common"
  }
}

resource "aws_kms_alias" "alias" {
  name          = "alias/event-bridge-key-alias"
  target_key_id = aws_kms_key.sns_key_event_bridge.key_id
}

resource "aws_kms_key" "sns_key_cost_alert" {
  description              = "KMS cost_alert to SNS"
  deletion_window_in_days  = 10
  enable_key_rotation      = true
  customer_master_key_spec = "SYMMETRIC_DEFAULT"
  policy = jsonencode(
    {
      "Version" : "2012-10-17",
      "Id" : "auto-sns-1",
      "Statement" : [
        {
          "Sid" : "Allow access through SNS for all principals in the account that are authorized to use SNS",
          "Effect" : "Allow",
          "Principal" : {
            "AWS" : "arn:aws:iam::205810638802:root"
          },
          "Action" : [
            "kms:Decrypt",
            "kms:GenerateDataKey*",
            "kms:CreateGrant",
            "kms:ListGrants",
            "kms:DescribeKey"
          ],
          "Resource" : "*",
          "Condition" : {
            "StringEquals" : {
              "kms:CallerAccount" : "205810638802",
              "kms:ViaService" : "sns.us-east-1.amazonaws.com"
            }
          }
        },
        {
          "Sid" : "Key Administrators",
          "Effect" : "Allow",
          "Principal" : {
            "AWS" : [
              "arn:aws:iam::205810638802:root",
              "arn:aws:iam::205810638802:role/prod_common"
            ]
          },
          "Action" : "kms:*",
          "Resource" : "*"
        },
        {
          "Sid" : "Allow CostAlert to use the key",
          "Effect" : "Allow",
          "Principal" : {
            "Service" : "costalerts.amazonaws.com"
          },
          "Action" : [
            "kms:Decrypt",
            "kms:GenerateDataKey"
          ],
          "Resource" : "*"
        }
      ]
    }
  )
  tags = {
    "Name"              = "SNS_cost_alert_bridge"
    "fluidattacks:line" = "cost"
    "fluidattacks:comp" = "common"
  }
}

resource "aws_kms_alias" "cost-alert-alias" {
  name          = "alias/cost-alert-key-alias"
  target_key_id = aws_kms_key.sns_key_cost_alert.key_id
}

# Guard Duty
resource "aws_guardduty_detector" "us_east_1" {
  enable   = true
  provider = aws.us-east-1

  tags = {
    "Name"              = "guarduty"
    "fluidattacks:line" = "cost"
    "fluidattacks:comp" = "common"
  }

  datasources {
    s3_logs {
      enable = true
    }
    kubernetes {
      audit_logs {
        enable = true
      }
    }
    malware_protection {
      scan_ec2_instance_with_findings {
        ebs_volumes {
          enable = true
        }
      }
    }
  }
}

resource "aws_guardduty_detector" "us_east_2" {
  enable   = true
  provider = aws.us-east-2

  tags = {
    "Name"              = "guarduty"
    "fluidattacks:line" = "cost"
    "fluidattacks:comp" = "common"
  }

  datasources {
    s3_logs {
      enable = true
    }
    kubernetes {
      audit_logs {
        enable = true
      }
    }
  }
}

resource "aws_guardduty_detector" "us_west_1" {
  enable   = true
  provider = aws.us-west-1

  tags = {
    "Name"              = "guarduty"
    "fluidattacks:line" = "cost"
    "fluidattacks:comp" = "common"
  }

  datasources {
    s3_logs {
      enable = true
    }
    kubernetes {
      audit_logs {
        enable = true
      }
    }
  }
}

resource "aws_guardduty_detector" "us_west_2" {
  enable   = true
  provider = aws.us-west-2

  tags = {
    "Name"              = "guarduty"
    "fluidattacks:line" = "cost"
    "fluidattacks:comp" = "common"
  }

  datasources {
    s3_logs {
      enable = true
    }
    kubernetes {
      audit_logs {
        enable = true
      }
    }
  }
}

resource "aws_sns_topic" "guardduty_alert" {
  name              = "guardduty_alert"
  display_name      = "Guardduty Notification"
  kms_master_key_id = aws_kms_key.sns_key_event_bridge.key_id

  policy = jsonencode(
    {
      Version = "2008-10-17"
      Statement = [
        {
          Sid    = "default"
          Effect = "Allow"
          Principal = {
            AWS = "arn:aws:iam::205810638802:root"
          }
          Action = [
            "sns:GetTopicAttributes",
            "sns:SetTopicAttributes",
            "sns:AddPermission",
            "sns:RemovePermission",
            "sns:DeleteTopic",
            "sns:Subscribe",
            "sns:ListSubscriptionsByTopic",
            "sns:Publish",
          ]
          Resource = ["*"]
          Condition = {
            StringEquals = {
              "AWS:SourceOwner" = "205810638802"
            }
          }
        },
        {
          Sid    = "eventsSns",
          Effect = "Allow",
          Principal = {
            Service = ["events.amazonaws.com"]
          },
          Action   = ["sns:Publish"]
          Resource = ["*"]
        },
      ]
    }
  )

  tags = {
    "Name"              = "guardduty_alert"
    "fluidattacks:line" = "cost"
    "fluidattacks:comp" = "common"
  }
}

resource "aws_sns_topic_subscription" "guardduty_alert" {
  protocol  = "email"
  endpoint  = "development@fluidattacks.com"
  topic_arn = aws_sns_topic.guardduty_alert.arn
}

resource "aws_cloudwatch_event_rule" "guardduty_alert" {
  name = "guardduty_alert"

  event_pattern = jsonencode({
    source      = ["aws.guardduty"]
    detail-type = ["GuardDuty Finding"]
    detail = {
      severity = range(4, 9, 0.1),
    }
  })

  tags = {
    "Name"              = "guardduty_alert"
    "fluidattacks:line" = "cost"
    "fluidattacks:comp" = "common"
  }
}

resource "aws_cloudwatch_event_target" "guardduty_alert" {
  rule      = aws_cloudwatch_event_rule.guardduty_alert.name
  target_id = "SendToSNS"
  arn       = aws_sns_topic.guardduty_alert.arn

  input_transformer {
    input_paths = {
      severity            = "$.detail.severity"
      Account_ID          = "$.detail.accountId"
      Finding_ID          = "$.detail.id"
      Finding_Type        = "$.detail.type"
      region              = "$.region"
      Finding_description = "$.detail.description"
    }
    input_template = <<EOF
{
  "region": <region>,
  "FindingUrl": "https://console.aws.amazon.com/guardduty/home?region=<region>#/findings?search=id%3D<Finding_ID>",
  "Account_ID": <Account_ID>,
  "severity": <severity>,
  "Finding_ID": <Finding_ID>,
  "Finding_Type": <Finding_Type>,
  "Finding_description": <Finding_description>
}
EOF
  }
}

resource "aws_sns_topic" "guardduty_alert_us_east_2" {
  name              = "guardduty_alert_us_east_2"
  kms_master_key_id = "alias/aws/sns"
  provider          = aws.us-east-2

  policy = jsonencode(
    {
      Version = "2008-10-17"
      Statement = [
        {
          Sid    = "default"
          Effect = "Allow"
          Principal = {
            AWS = "*"
          }
          Action = [
            "sns:GetTopicAttributes",
            "sns:SetTopicAttributes",
            "sns:AddPermission",
            "sns:RemovePermission",
            "sns:DeleteTopic",
            "sns:Subscribe",
            "sns:ListSubscriptionsByTopic",
            "sns:Publish",
          ]
          Resource = ["*"]
          Condition = {
            StringEquals = {
              "AWS:SourceOwner" = "205810638802"
            }
          }
        },
        {
          Sid    = "eventsSns",
          Effect = "Allow",
          Principal = {
            Service = ["events.amazonaws.com"]
          },
          Action   = ["sns:Publish"]
          Resource = ["*"]
        },
      ]
    }
  )

  tags = {
    "Name"              = "guardduty_alert"
    "fluidattacks:line" = "cost"
    "fluidattacks:comp" = "common"
  }
}

resource "aws_sns_topic_subscription" "guardduty_alert_us_east_2" {
  provider  = aws.us-east-2
  protocol  = "email-json"
  endpoint  = "development@fluidattacks.com"
  topic_arn = aws_sns_topic.guardduty_alert_us_east_2.arn
}

resource "aws_cloudwatch_event_rule" "guardduty_alert_us_east_2" {
  name     = "guardduty_alert_us_east_2"
  provider = aws.us-east-2

  event_pattern = jsonencode({
    source      = ["aws.guardduty"]
    detail-type = ["GuardDuty Finding"]
    detail = {
      severity = range(1, 9, 0.1),
    }
  })

  tags = {
    "Name"              = "guardduty_alert"
    "fluidattacks:line" = "cost"
    "fluidattacks:comp" = "common"
  }
}

resource "aws_cloudwatch_event_target" "guardduty_alert_us_east_2" {
  rule      = aws_cloudwatch_event_rule.guardduty_alert_us_east_2.name
  target_id = "SendToSNS"
  arn       = aws_sns_topic.guardduty_alert_us_east_2.arn
  provider  = aws.us-east-2

  input_transformer {
    input_paths = {
      severity            = "$.detail.severity"
      Account_ID          = "$.detail.accountId"
      Finding_ID          = "$.detail.id"
      Finding_Type        = "$.detail.type"
      region              = "$.region"
      Finding_description = "$.detail.description"
    }
    input_template = <<EOF
{
  "region": <region>,
  "FindingUrl": "https://console.aws.amazon.com/guardduty/home?region=<region>#/findings?search=id%3D<Finding_ID>",
  "Account_ID": <Account_ID>,
  "severity": <severity>,
  "Finding_ID": <Finding_ID>,
  "Finding_Type": <Finding_Type>,
  "Finding_description": <Finding_description>
}
EOF
  }
}

resource "aws_sns_topic" "guardduty_alert_us-west-1" {
  name              = "guardduty_alert_us-west-1"
  kms_master_key_id = "alias/aws/sns"
  provider          = aws.us-west-1

  policy = jsonencode(
    {
      Version = "2008-10-17"
      Statement = [
        {
          Sid    = "default"
          Effect = "Allow"
          Principal = {
            AWS = "*"
          }
          Action = [
            "sns:GetTopicAttributes",
            "sns:SetTopicAttributes",
            "sns:AddPermission",
            "sns:RemovePermission",
            "sns:DeleteTopic",
            "sns:Subscribe",
            "sns:ListSubscriptionsByTopic",
            "sns:Publish",
          ]
          Resource = ["*"]
          Condition = {
            StringEquals = {
              "AWS:SourceOwner" = "205810638802"
            }
          }
        },
        {
          Sid    = "eventsSns",
          Effect = "Allow",
          Principal = {
            Service = ["events.amazonaws.com"]
          },
          Action   = ["sns:Publish"]
          Resource = ["*"]
        },
      ]
    }
  )

  tags = {
    "Name"              = "guardduty_alert"
    "fluidattacks:line" = "cost"
    "fluidattacks:comp" = "common"
  }
}

resource "aws_sns_topic_subscription" "guardduty_alert_us-west-1" {
  provider  = aws.us-west-1
  protocol  = "email-json"
  endpoint  = "development@fluidattacks.com"
  topic_arn = aws_sns_topic.guardduty_alert_us-west-1.arn
}

resource "aws_cloudwatch_event_rule" "guardduty_alert_us-west-1" {
  name     = "guardduty_alert_us-west-1"
  provider = aws.us-west-1

  event_pattern = jsonencode({
    source      = ["aws.guardduty"]
    detail-type = ["GuardDuty Finding"]
    detail = {
      severity = range(1, 9, 0.1),
    }
  })

  tags = {
    "Name"              = "guardduty_alert"
    "fluidattacks:line" = "cost"
    "fluidattacks:comp" = "common"
  }
}

resource "aws_cloudwatch_event_target" "guardduty_alert_us-west-1" {
  rule      = aws_cloudwatch_event_rule.guardduty_alert_us-west-1.name
  target_id = "SendToSNS"
  arn       = aws_sns_topic.guardduty_alert_us-west-1.arn
  provider  = aws.us-west-1

  input_transformer {
    input_paths = {
      severity            = "$.detail.severity"
      Account_ID          = "$.detail.accountId"
      Finding_ID          = "$.detail.id"
      Finding_Type        = "$.detail.type"
      region              = "$.region"
      Finding_description = "$.detail.description"
    }
    input_template = <<EOF
{
  "region": <region>,
  "FindingUrl": "https://console.aws.amazon.com/guardduty/home?region=<region>#/findings?search=id%3D<Finding_ID>",
  "Account_ID": <Account_ID>,
  "severity": <severity>,
  "Finding_ID": <Finding_ID>,
  "Finding_Type": <Finding_Type>,
  "Finding_description": <Finding_description>
}
EOF
  }
}

resource "aws_sns_topic" "guardduty_alert_us-west-2" {
  name              = "guardduty_alert_us-west-2"
  kms_master_key_id = "alias/aws/sns"
  provider          = aws.us-west-2

  policy = jsonencode(
    {
      Version = "2008-10-17"
      Statement = [
        {
          Sid    = "default"
          Effect = "Allow"
          Principal = {
            AWS = "*"
          }
          Action = [
            "sns:GetTopicAttributes",
            "sns:SetTopicAttributes",
            "sns:AddPermission",
            "sns:RemovePermission",
            "sns:DeleteTopic",
            "sns:Subscribe",
            "sns:ListSubscriptionsByTopic",
            "sns:Publish",
          ]
          Resource = ["*"]
          Condition = {
            StringEquals = {
              "AWS:SourceOwner" = "205810638802"
            }
          }
        },
        {
          Sid    = "eventsSns",
          Effect = "Allow",
          Principal = {
            Service = ["events.amazonaws.com"]
          },
          Action   = ["sns:Publish"]
          Resource = ["*"]
        },
      ]
    }
  )

  tags = {
    "Name"              = "guardduty_alert"
    "fluidattacks:line" = "cost"
    "fluidattacks:comp" = "common"
  }
}

resource "aws_sns_topic_subscription" "guardduty_alert_us-west-2" {
  provider  = aws.us-west-2
  protocol  = "email-json"
  endpoint  = "development@fluidattacks.com"
  topic_arn = aws_sns_topic.guardduty_alert_us-west-2.arn
}

resource "aws_cloudwatch_event_rule" "guardduty_alert_us-west-2" {
  name     = "guardduty_alert_us-west-2"
  provider = aws.us-west-2

  event_pattern = jsonencode({
    source      = ["aws.guardduty"]
    detail-type = ["GuardDuty Finding"]
    detail = {
      severity = range(1, 9, 0.1),
    }
  })

  tags = {
    "Name"              = "guardduty_alert"
    "fluidattacks:line" = "cost"
    "fluidattacks:comp" = "common"
  }
}

resource "aws_cloudwatch_event_target" "guardduty_alert_us-west-2" {
  rule      = aws_cloudwatch_event_rule.guardduty_alert_us-west-2.name
  target_id = "SendToSNS"
  arn       = aws_sns_topic.guardduty_alert_us-west-2.arn
  provider  = aws.us-west-2

  input_transformer {
    input_paths = {
      severity            = "$.detail.severity"
      Account_ID          = "$.detail.accountId"
      Finding_ID          = "$.detail.id"
      Finding_Type        = "$.detail.type"
      region              = "$.region"
      Finding_description = "$.detail.description"
    }
    input_template = <<EOF
{
  "region": <region>,
  "FindingUrl": "https://console.aws.amazon.com/guardduty/home?region=<region>#/findings?search=id%3D<Finding_ID>",
  "Account_ID": <Account_ID>,
  "severity": <severity>,
  "Finding_ID": <Finding_ID>,
  "Finding_Type": <Finding_Type>,
  "Finding_description": <Finding_description>
}
EOF
  }
}
resource "aws_guardduty_filter" "CoinHiveFP" {
  name        = "CoinHiveFP"
  action      = "ARCHIVE"
  detector_id = aws_guardduty_detector.us_east_1.id
  description = "This is an article that talks about crypto malware and includes a snippet that triggers this rule"
  rank        = 1

  finding_criteria {
    criterion {
      field  = "service.ebsVolumeScanDetails.scanDetections.threatDetectedByName.threatNames.name"
      equals = ["GT:JS.Application.CoinHive.3.CDE86C70"]
    }
  }
  tags = {
    "Name"              = "CoinHiveFP"
    "fluidattacks:line" = "cost"
    "fluidattacks:comp" = "common"
  }
}
resource "aws_guardduty_filter" "SuspiciousDomain" {
  name        = "SuspiciousDomain"
  action      = "ARCHIVE"
  detector_id = aws_guardduty_detector.us_east_1.id
  rank        = 1

  finding_criteria {
    criterion {
      field  = "type"
      equals = ["Impact:EC2/SuspiciousDomainRequest.Reputation"]
    }
  }
  tags = {
    "Name"              = "SuspiciousDomain"
    "fluidattacks:line" = "cost"
    "fluidattacks:comp" = "common"
  }
}
