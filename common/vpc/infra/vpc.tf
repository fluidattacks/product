resource "aws_vpc" "fluid-vpc" {
  cidr_block                       = "192.168.0.0/16"
  assign_generated_ipv6_cidr_block = true
  enable_dns_support               = true
  enable_dns_hostnames             = true

  tags = {
    "Name"              = "fluid-vpc"
    "fluidattacks:line" = "cost"
    "fluidattacks:comp" = "common"
  }
}
resource "aws_vpc_endpoint" "kinesis" {
  vpc_id              = aws_vpc.fluid-vpc.id
  service_name        = "com.amazonaws.us-east-1.kinesis-streams"
  security_group_ids  = [aws_security_group.kinesis-endpoint.id]
  vpc_endpoint_type   = "Interface"
  private_dns_enabled = true
  subnet_ids          = toset([for subnet in aws_subnet.main : subnet.id if startswith(subnet.tags.Name, "observes")])
  tags = {
    "Name"              = "kinesis-endpoint"
    "fluidattacks:line" = "cost"
    "fluidattacks:comp" = "etl"
  }
}
resource "aws_internet_gateway" "fluid-vpc" {
  vpc_id = aws_vpc.fluid-vpc.id

  tags = {
    "Name"              = "fluid-vpc"
    "fluidattacks:line" = "cost"
    "fluidattacks:comp" = "common"
  }
}

resource "aws_route" "internet-ipv4" {
  route_table_id = aws_vpc.fluid-vpc.default_route_table_id
  # NOFLUID We need to access all the CIDR blocks of our clients.
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.fluid-vpc.id
}

resource "aws_route" "internet-ipv6" {
  route_table_id              = aws_vpc.fluid-vpc.default_route_table_id
  destination_ipv6_cidr_block = "::/0"
  gateway_id                  = aws_internet_gateway.fluid-vpc.id
}
