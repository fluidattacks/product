locals {
  subnets = {
    ipv4 = [
      {
        name                    = "machine_learning_1"
        availability_zone       = "us-east-1a"
        map_public_ip_on_launch = true
        new_bits                = 9
        tags                    = {}
      },
      {
        name                    = "machine_learning_2"
        availability_zone       = "us-east-1b"
        map_public_ip_on_launch = true
        new_bits                = 9
        tags                    = {}
      },
      {
        name                    = "machine_learning_3"
        availability_zone       = "us-east-1d"
        map_public_ip_on_launch = true
        new_bits                = 9
        tags                    = {}
      },
      {
        name                    = "machine_learning_4"
        availability_zone       = "us-east-1e"
        map_public_ip_on_launch = true
        new_bits                = 9
        tags                    = {}
      },
      {
        name                    = "machine_learning_5"
        availability_zone       = "us-east-1f"
        map_public_ip_on_launch = true
        new_bits                = 9
        tags                    = {}
      },
      {
        name                    = "free2"
        availability_zone       = "us-east-1a"
        map_public_ip_on_launch = false
        new_bits                = 9
        tags                    = {}
      },
      {
        name                    = "free4"
        availability_zone       = "us-east-1a"
        map_public_ip_on_launch = false
        new_bits                = 8
        tags                    = {}
      },
      {
        name                    = "observes_1"
        availability_zone       = "us-east-1a"
        map_public_ip_on_launch = false
        new_bits                = 9
        tags                    = {}
      },
      {
        name                    = "observes_2"
        availability_zone       = "us-east-1b"
        map_public_ip_on_launch = false
        new_bits                = 9
        tags                    = {}
      },
      {
        name                    = "observes_3"
        availability_zone       = "us-east-1d"
        map_public_ip_on_launch = false
        new_bits                = 9
        tags                    = {}
      },
      {
        name                    = "observes_4"
        availability_zone       = "us-east-1e"
        map_public_ip_on_launch = false
        new_bits                = 9
        tags                    = {}
      },
      {
        name                    = "free1"
        availability_zone       = "us-east-1a"
        map_public_ip_on_launch = false
        new_bits                = 7
        tags                    = {}
      },
      {
        name                    = "batch_clone"
        availability_zone       = "us-east-1a"
        map_public_ip_on_launch = true
        new_bits                = 8
        tags                    = {}
      },
      {
        name                    = "free3"
        availability_zone       = "us-east-1b"
        map_public_ip_on_launch = true
        new_bits                = 8
        tags                    = {}
      },
      {
        name                    = "common"
        availability_zone       = "us-east-1b"
        map_public_ip_on_launch = false
        new_bits                = 7
        tags                    = {}
      },
      {
        name                    = "k8s_1"
        availability_zone       = "us-east-1b"
        map_public_ip_on_launch = true
        new_bits                = 6
        tags = {
          "kubernetes.io/cluster/common-k8s" = "shared"
          "kubernetes.io/role/elb"           = "1"
        }
      },
      {
        name                    = "k8s_2"
        availability_zone       = "us-east-1a"
        map_public_ip_on_launch = true
        new_bits                = 6
        tags = {
          "kubernetes.io/cluster/common-k8s" = "shared"
          "kubernetes.io/role/elb"           = "1"
        }
      },
      {
        name                    = "k8s_3"
        availability_zone       = "us-east-1d"
        map_public_ip_on_launch = true
        new_bits                = 6
        tags = {
          "kubernetes.io/cluster/common-k8s" = "shared"
          "kubernetes.io/role/elb"           = "1"
        }
      },
      {
        name                    = "k8s_4"
        availability_zone       = "us-east-1f"
        map_public_ip_on_launch = true
        new_bits                = 6
        tags = {
          "kubernetes.io/cluster/common-k8s" = "shared"
          "kubernetes.io/role/elb"           = "1"
        }
      },
      {
        name                    = "k8s_5"
        availability_zone       = "us-east-1e"
        map_public_ip_on_launch = true
        new_bits                = 6
        tags = {
          "kubernetes.io/cluster/common-k8s" = "shared"
          "kubernetes.io/role/elb"           = "1"
        }
      },
      {
        name                    = "k8s_6"
        availability_zone       = "us-east-1b"
        map_public_ip_on_launch = true
        new_bits                = 6
        tags = {
          "kubernetes.io/cluster/common-k8s" = "shared"
          "kubernetes.io/role/elb"           = "1"
        }
      },
      {
        name                    = "k8s_7"
        availability_zone       = "us-east-1a"
        map_public_ip_on_launch = true
        new_bits                = 6
        tags = {
          "kubernetes.io/cluster/common-k8s" = "shared"
          "kubernetes.io/role/elb"           = "1"
        }
      },
      {
        name                    = "k8s_8"
        availability_zone       = "us-east-1d"
        map_public_ip_on_launch = true
        new_bits                = 6
        tags = {
          "kubernetes.io/cluster/common-k8s" = "shared"
          "kubernetes.io/role/elb"           = "1"
        }
      },
      {
        name                    = "k8s_9"
        availability_zone       = "us-east-1f"
        map_public_ip_on_launch = true
        new_bits                = 6
        tags = {
          "kubernetes.io/cluster/common-k8s" = "shared"
          "kubernetes.io/role/elb"           = "1"
        }
      },
      {
        name                    = "k8s_10"
        availability_zone       = "us-east-1e"
        map_public_ip_on_launch = true
        new_bits                = 6
        tags = {
          "kubernetes.io/cluster/common-k8s" = "shared"
          "kubernetes.io/role/elb"           = "1"
        }
      },
      {
        name                    = "ci_1"
        availability_zone       = "us-east-1a"
        map_public_ip_on_launch = false
        new_bits                = 8
        tags                    = {}
      },
      {
        name                    = "ci_2"
        availability_zone       = "us-east-1b"
        map_public_ip_on_launch = false
        new_bits                = 8
        tags                    = {}
      },
      {
        name                    = "ci_3"
        availability_zone       = "us-east-1d"
        map_public_ip_on_launch = false
        new_bits                = 8
        tags                    = {}
      },
      {
        name                    = "ci_4"
        availability_zone       = "us-east-1e"
        map_public_ip_on_launch = false
        new_bits                = 8
        tags                    = {}
      },
      {
        name                    = "ci_5"
        availability_zone       = "us-east-1f"
        map_public_ip_on_launch = false
        new_bits                = 8
        tags                    = {}
      },
      {
        name                    = "batch_main_1"
        availability_zone       = "us-east-1a"
        map_public_ip_on_launch = true
        new_bits                = 8
        tags                    = {}
      },
      {
        name                    = "batch_main_2"
        availability_zone       = "us-east-1b"
        map_public_ip_on_launch = true
        new_bits                = 8
        tags                    = {}
      },
      {
        name                    = "batch_main_3"
        availability_zone       = "us-east-1d"
        map_public_ip_on_launch = true
        new_bits                = 8
        tags                    = {}
      },
      {
        name                    = "batch_main_4"
        availability_zone       = "us-east-1e"
        map_public_ip_on_launch = true
        new_bits                = 8
        tags                    = {}
      },
      {
        name                    = "batch_main_5"
        availability_zone       = "us-east-1f"
        map_public_ip_on_launch = true
        new_bits                = 8
        tags                    = {}
      },
      {
        name                    = "lambda_1"
        availability_zone       = "us-east-1a"
        map_public_ip_on_launch = false
        new_bits                = 8
        tags                    = {}
      },
      {
        name                    = "lambda_2"
        availability_zone       = "us-east-1b"
        map_public_ip_on_launch = false
        new_bits                = 8
        tags                    = {}
      },
    ]
    ipv6 = [
      {
        name              = "us-east-1a"
        availability_zone = "us-east-1a"
        new_bits          = 8
        tags              = {}
      },
      {
        name              = "us-east-1b"
        availability_zone = "us-east-1b"
        new_bits          = 8
        tags              = {}
      },
      {
        name              = "us-east-1d"
        availability_zone = "us-east-1d"
        new_bits          = 8
        tags              = {}
      },
      {
        name              = "us-east-1e"
        availability_zone = "us-east-1e"
        new_bits          = 8
        tags              = {}
      },
      {
        name              = "us-east-1f"
        availability_zone = "us-east-1f"
        new_bits          = 8
        tags              = {}
      },
    ]
  }
}

module "subnet_addrs_ipv4" {
  source  = "hashicorp/subnets/cidr"
  version = "1.0.0"

  base_cidr_block = aws_vpc.fluid-vpc.cidr_block
  networks        = local.subnets.ipv4
}

module "subnet_addrs_ipv6" {
  source  = "hashicorp/subnets/cidr"
  version = "1.0.0"

  base_cidr_block = aws_vpc.fluid-vpc.ipv6_cidr_block
  networks        = local.subnets.ipv6
}

resource "aws_subnet" "main" {
  for_each = {
    for subnet in concat(
      [for subnet in local.subnets.ipv4 : merge(subnet, { type = "ipv4" })],
      [for subnet in local.subnets.ipv6 : merge(subnet, { type = "ipv6" })],
    ) : subnet.name => subnet
  }

  # General
  vpc_id            = aws_vpc.fluid-vpc.id
  availability_zone = each.value.availability_zone

  # IPv4
  cidr_block              = lookup(module.subnet_addrs_ipv4.network_cidr_blocks, each.key, null)
  map_public_ip_on_launch = lookup(each.value, "map_public_ip_on_launch", null)

  # IPv6
  ipv6_cidr_block                                = lookup(module.subnet_addrs_ipv6.network_cidr_blocks, each.key, null)
  ipv6_native                                    = each.value.type == "ipv6" ? true : null
  enable_dns64                                   = each.value.type == "ipv6" ? true : null
  assign_ipv6_address_on_creation                = each.value.type == "ipv6" ? true : null
  enable_resource_name_dns_aaaa_record_on_launch = each.value.type == "ipv6" ? true : null

  tags = merge(
    {
      "Name"              = each.key
      "fluidattacks:line" = "cost"
      "fluidattacks:comp" = "common"
    },
    each.value.tags,
  )
}
