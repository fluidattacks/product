# https://github.com/fluidattacks/makes
{ outputs, ... }: {
  deployTerraform = {
    modules = {
      publicStorage = {
        setup = [ outputs."/secretsForAwsFromGitlab/prodCommon" ];
        src = "/common/public_storage/infra";
        version = "1.0";
      };
    };
  };
  lintTerraform = {
    modules = {
      publicStorage = {
        setup = [ outputs."/secretsForAwsFromGitlab/dev" ];
        src = "/common/public_storage/infra";
        version = "1.0";
      };
    };
  };
  testTerraform = {
    modules = {
      publicStorage = {
        setup = [ outputs."/secretsForAwsFromGitlab/dev" ];
        src = "/common/public_storage/infra";
        version = "1.0";
      };
    };
  };
}
