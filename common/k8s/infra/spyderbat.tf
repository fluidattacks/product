locals {
  orc_url = "https://orc.fa.prod.spyderbat.com"
}

variable "spyderkey" {
  type = string
}

resource "helm_release" "nanoagent" {
  name             = "nanoagent"
  repository       = "https://spyderbat.github.io/nanoagent_helm/"
  chart            = "nanoagent"
  namespace        = "spyderbat"
  create_namespace = true
  version          = "1.0.27"

  set {
    name  = "nanoagent.agentRegistrationCode"
    value = var.spyderkey
  }
  set {
    name  = "nanoagent.orcurl"
    value = local.orc_url
  }
  set {
    name  = "CLUSTER_NAME"
    value = local.cluster_name
  }

}
