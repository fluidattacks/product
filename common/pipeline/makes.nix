let
  arch = let
    commit = "8bcf2517282056e5920bb91b1b50700a814d2d28";
    sha256 = "sha256:14ljl2w10wcsm0g3a3y9v42vb9c0xd2dfvdzqys3fkn90vbmf4pz";
    url =
      "https://gitlab.com/fluidattacks/universe/-/raw/${commit}/common/ci/arch.nix";
    src = builtins.fetchurl { inherit sha256 url; };
  in import src;

  rules = {
    common = {
      default = arch.core.rules.titleRule {
        products = [ "all" "common" ];
        types = [ ];
      };
      noRotate = arch.core.rules.titleRule {
        products = [ "all" "common" ];
        types = [ "feat" "fix" "refac" ];
      };
    };
    root = {
      default = { when = "always"; };
      noRotate = arch.core.rules.titleRule {
        products = [ ".*" ];
        types = [ "feat" "fix" "refac" ];
      };
      onlyRotate = arch.core.rules.titleRule {
        products = [ ".*" ];
        types = [ "chore" ];
      };
    };
  };
in {
  pipelines = {
    common = {
      gitlabPath = "/common/pipeline/default.yaml";
      jobs = [
        {
          output = "/deployTerraform/commonCi";
          gitlabExtra = arch.extras.default // {
            resource_group = "deploy/$CI_JOB_NAME";
            rules = arch.rules.prod ++ [ rules.common.default ];
            stage = arch.stages.deploy;
            tags = [ arch.tags.common ];
          };
        }
        {
          output = "/deployTerraform/commonCompute";
          gitlabExtra = arch.extras.default // {
            resource_group = "deploy/$CI_JOB_NAME";
            rules = arch.rules.prod ++ [ rules.common.default ];
            stage = arch.stages.deploy;
            tags = [ arch.tags.common ];
            retry = {
              max = 2;
              when = [ "runner_system_failure" "script_failure" ];
            };
          };
        }
        {
          output = "/deployTerraform/commonDns";
          gitlabExtra = arch.extras.default // {
            resource_group = "deploy/$CI_JOB_NAME";
            rules = arch.rules.prod ++ [ rules.common.default ];
            stage = arch.stages.deploy;
            tags = [ arch.tags.common ];
          };
        }
        {
          output = "/deployTerraform/commonFoss";
          gitlabExtra = arch.extras.default // {
            resource_group = "deploy/$CI_JOB_NAME";
            rules = arch.rules.prod ++ [ rules.common.default ];
            stage = arch.stages.deploy;
            tags = [ arch.tags.common ];
          };
        }
        {
          output = "/deployTerraform/commonK8s";
          gitlabExtra = arch.extras.default // {
            resource_group = "deploy/$CI_JOB_NAME";
            rules = arch.rules.prod ++ [ rules.common.default ];
            stage = arch.stages.deploy;
            tags = [ arch.tags.common ];
          };
        }
        {
          output = "/deployTerraform/commonMarketplace";
          gitlabExtra = arch.extras.default // {
            resource_group = "deploy/$CI_JOB_NAME";
            rules = arch.rules.prod ++ [ rules.common.default ];
            stage = arch.stages.deploy;
            tags = [ arch.tags.common ];
          };
        }
        {
          output = "/deployTerraform/commonObservability";
          gitlabExtra = arch.extras.default // {
            resource_group = "deploy/$CI_JOB_NAME";
            rules = arch.rules.prod ++ [ rules.common.default ];
            stage = arch.stages.deploy;
            tags = [ arch.tags.common ];
          };
        }
        {
          output = "/deployTerraform/commonIam";
          gitlabExtra = arch.extras.default // {
            resource_group = "deploy/$CI_JOB_NAME";
            rules = arch.rules.prod ++ [ rules.common.default ];
            stage = arch.stages.deploy;
            tags = [ arch.tags.common ];
            retry = {
              max = 2;
              when = [ "runner_system_failure" "script_failure" ];
            };
          };
        }
        {
          output = "/deployTerraform/commonStatus";
          gitlabExtra = arch.extras.default // {
            resource_group = "deploy/$CI_JOB_NAME";
            rules = arch.rules.prod ++ [ rules.common.default ];
            stage = arch.stages.deploy;
            tags = [ arch.tags.common ];
          };
        }
        {
          output = "/deployTerraform/commonUsers";
          gitlabExtra = arch.extras.default // {
            resource_group = "deploy/$CI_JOB_NAME";
            rules = arch.rules.prod ++ [ rules.common.default ];
            stage = arch.stages.deploy;
            tags = [ arch.tags.common ];
          };
        }
        {
          output = "/deployTerraform/commonVpc";
          gitlabExtra = arch.extras.default // {
            resource_group = "deploy/$CI_JOB_NAME";
            rules = arch.rules.prod ++ [ rules.common.default ];
            stage = arch.stages.deploy;
            tags = [ arch.tags.common ];
          };
        }
        {
          output = "/deployTerraform/commonVpn";
          gitlabExtra = arch.extras.default // {
            resource_group = "deploy/$CI_JOB_NAME";
            rules = arch.rules.prod ++ [ rules.common.default ];
            stage = arch.stages.deploy;
            tags = [ arch.tags.common ];
          };
        }
        {
          output = "/deployTerraform/publicStorage";
          gitlabExtra = arch.extras.default // {
            resource_group = "deploy/$CI_JOB_NAME";
            rules = arch.rules.prod ++ [ rules.common.default ];
            stage = arch.stages.deploy;
            tags = [ arch.tags.common ];
          };
        }
        {
          output = "/common/utils/pypi-deploy fluidattacks-core";
          gitlabExtra = arch.extras.default // {
            resource_group = "$CI_JOB_NAME";
            rules = arch.rules.prod ++ [ rules.common.default ];
            stage = arch.stages.deploy;
            tags = [ arch.tags.common ];
          };
        }
        {
          output = "/common/fluidattacks-core";
          gitlabExtra = arch.extras.default // {
            cache = {
              key = "$CI_COMMIT_REF_NAME-common-fluidattacks-core";
              paths = [
                "common/fluidattacks-core/.venv"
                "common/fluidattacks-core/.ruff_cache"
                "common/fluidattacks-core/.import_linter_cache"
                "common/fluidattacks-core/.mypy_cache"
              ];
            };
            rules = arch.rules.dev ++ [ rules.common.noRotate ];
            stage = arch.stages.test;
            tags = [ arch.tags.common ];
          };
        }
        (arch.jobs.devenv {
          path = "common/ci/analytics";
          script = "lint";
          extras = {
            cache = {
              key = "$CI_COMMIT_REF_NAME-common-ci-analytics-lint";
              paths = [
                "common/ci/analytics/.venv"
                "common/ci/analytics/.ruff_cache"
                "common/ci/analytics/.mypy_cache"
              ];
            };
            rules = arch.rules.dev ++ [ rules.common.noRotate ];
            stage = arch.stages.test;
            tags = [ arch.tags.common-large ];
          };
        })
        {
          output = "/common/criteria/upload lint";
          gitlabExtra = arch.extras.default // {
            cache = {
              key = "$CI_COMMIT_REF_NAME-common-criteria-upload-lint";
              paths = [
                "common/criteria/upload/.venv"
                "common/criteria/upload/.ruff_cache"
                "common/criteria/upload/.mypy_cache"
              ];
            };
            rules = arch.rules.dev ++ [ rules.common.noRotate ];
            stage = arch.stages.test;
            tags = [ arch.tags.common ];
          };
        }
        {
          output = "/lintTerraform/commonCi";
          gitlabExtra = arch.extras.default // {
            rules = arch.rules.dev ++ [ rules.common.noRotate ];
            stage = arch.stages.test;
            tags = [ arch.tags.common ];
          };
        }
        {
          output = "/lintTerraform/commonCompute";
          gitlabExtra = arch.extras.default // {
            rules = arch.rules.dev ++ [ rules.common.noRotate ];
            stage = arch.stages.test;
            tags = [ arch.tags.common ];
          };
        }
        {
          output = "/lintTerraform/commonDns";
          gitlabExtra = arch.extras.default // {
            rules = arch.rules.dev ++ [ rules.common.noRotate ];
            stage = arch.stages.test;
            tags = [ arch.tags.common ];
          };
        }
        {
          output = "/lintTerraform/commonFoss";
          gitlabExtra = arch.extras.default // {
            rules = arch.rules.dev ++ [ rules.common.noRotate ];
            stage = arch.stages.test;
            tags = [ arch.tags.common ];
          };
        }
        {
          output = "/lintTerraform/commonK8s";
          gitlabExtra = arch.extras.default // {
            rules = arch.rules.dev ++ [ rules.common.noRotate ];
            stage = arch.stages.test;
            tags = [ arch.tags.common ];
          };
        }
        {
          output = "/lintTerraform/commonMarketplace";
          gitlabExtra = arch.extras.default // {
            allow_failure = true;
            rules = arch.rules.dev ++ [ rules.common.noRotate ];
            stage = arch.stages.test;
            tags = [ arch.tags.common ];
          };
        }
        {
          output = "/lintTerraform/commonIam";
          gitlabExtra = arch.extras.default // {
            rules = arch.rules.dev ++ [ rules.common.noRotate ];
            stage = arch.stages.test;
            tags = [ arch.tags.common ];
          };
        }
        {
          output = "/lintTerraform/commonObservability";
          gitlabExtra = arch.extras.default // {
            rules = arch.rules.dev ++ [ rules.common.noRotate ];
            stage = arch.stages.test;
            tags = [ arch.tags.common ];
          };
        }
        {
          output = "/lintTerraform/commonUsers";
          gitlabExtra = arch.extras.default // {
            rules = arch.rules.dev ++ [ rules.common.noRotate ];
            stage = arch.stages.test;
            tags = [ arch.tags.common ];
          };
        }
        {
          output = "/lintTerraform/commonVpc";
          gitlabExtra = arch.extras.default // {
            rules = arch.rules.dev ++ [ rules.common.noRotate ];
            stage = arch.stages.test;
            tags = [ arch.tags.common ];
          };
        }
        {
          output = "/lintTerraform/commonVpn";
          gitlabExtra = arch.extras.default // {
            rules = arch.rules.dev ++ [ rules.common.noRotate ];
            stage = arch.stages.test;
            tags = [ arch.tags.common ];
          };
        }
        {
          output = "/lintTerraform/publicStorage";
          gitlabExtra = arch.extras.default // {
            rules = arch.rules.dev ++ [ rules.common.noRotate ];
            stage = arch.stages.test;
            tags = [ arch.tags.common ];
          };
        }
        {
          output = "/lintWithAjv/common/compute/arch/sizes";
          gitlabExtra = arch.extras.default // {
            rules = arch.rules.dev ++ [ rules.common.noRotate ];
            stage = arch.stages.test;
            tags = [ arch.tags.common ];
          };
        }
        {
          output = "/lintWithAjv/common/criteria/compliance";
          gitlabExtra = arch.extras.default // {
            rules = arch.rules.dev ++ [ rules.common.noRotate ];
            stage = arch.stages.test;
            tags = [ arch.tags.common ];
          };
        }
        {
          output = "/lintWithAjv/common/criteria/requirements";
          gitlabExtra = arch.extras.default // {
            rules = arch.rules.dev ++ [ rules.common.noRotate ];
            stage = arch.stages.test;
            tags = [ arch.tags.common ];
          };
        }
        {
          output = "/lintWithAjv/common/criteria/vulnerabilities";
          gitlabExtra = arch.extras.default // {
            rules = arch.rules.dev ++ [ rules.common.noRotate ];
            stage = arch.stages.test;
            tags = [ arch.tags.common ];
          };
        }
        {
          output = "/common/test/base/env/dev";
          gitlabExtra = arch.extras.default // {
            rules = arch.rules.dev ++ [ rules.common.noRotate ];
            stage = arch.stages.test;
            tags = [ arch.tags.common ];
          };
        }
        {
          output = "/common/test/base/env/runtime";
          gitlabExtra = arch.extras.default // {
            rules = arch.rules.dev ++ [ rules.common.noRotate ];
            stage = arch.stages.test;
            tags = [ arch.tags.common ];
          };
        }
        {
          output = "/common/ci/infra/module/gitlab-bot/lint";
          gitlabExtra = arch.extras.default // {
            rules = arch.rules.dev ++ [ rules.common.noRotate ];
            stage = arch.stages.test;
            tags = [ arch.tags.common ];
          };
        }
        {
          output = "/common/criteria/test/base";
          gitlabExtra = arch.extras.default // {
            rules = arch.rules.dev ++ [ rules.common.noRotate ];
            stage = arch.stages.test;
            tags = [ arch.tags.common ];
          };
        }
        {
          output = "/common/criteria/test/skims-sync";
          gitlabExtra = arch.extras.default // {
            rules = arch.rules.dev ++ [ rules.common.noRotate ];
            stage = arch.stages.test;
            tags = [ arch.tags.common ];
          };
        }
        {
          output = "/common/criteria/test/unreferenced";
          gitlabExtra = arch.extras.default // {
            rules = arch.rules.dev ++ [ rules.common.noRotate ];
            stage = arch.stages.test;
            tags = [ arch.tags.common ];
          };
        }
        (arch.jobs.devenv {
          path = "common/ci/analytics";
          script = "tests";
          extras = {
            cache = {
              key = "$CI_COMMIT_REF_NAME-common-ci-analytics-tests";
              paths = [
                "common/ci/analytics/.venv"
                "common/ci/analytics/.pytest_cache"
              ];
            };
            rules = arch.rules.dev ++ [ rules.common.noRotate ];
            stage = arch.stages.test;
            tags = [ arch.tags.common-large ];
          };
        })
        {
          output = "/pipelineOnGitlab/common";
          gitlabExtra = arch.extras.default // {
            rules = arch.rules.dev ++ [ rules.common.default ];
            stage = arch.stages.test;
            tags = [ arch.tags.common ];
          };
        }
        {
          output = "/testTerraform/commonCi";
          gitlabExtra = arch.extras.default // {
            rules = arch.rules.dev ++ [ rules.common.noRotate ];
            stage = arch.stages.test;
            tags = [ arch.tags.common ];
          };
        }
        {
          output = "/testTerraform/commonCompute";
          gitlabExtra = arch.extras.default // {
            rules = arch.rules.dev ++ [ rules.common.noRotate ];
            stage = arch.stages.test;
            tags = [ arch.tags.common ];
          };
        }
        {
          output = "/testTerraform/commonDns";
          gitlabExtra = arch.extras.default // {
            rules = arch.rules.dev ++ [ rules.common.noRotate ];
            stage = arch.stages.test;
            tags = [ arch.tags.common ];
          };
        }
        {
          output = "/testTerraform/commonFoss";
          gitlabExtra = arch.extras.default // {
            rules = arch.rules.dev ++ [ rules.common.noRotate ];
            stage = arch.stages.test;
            tags = [ arch.tags.common ];
          };
        }
        {
          output = "/testTerraform/commonK8s";
          gitlabExtra = arch.extras.default // {
            rules = arch.rules.dev ++ [ rules.common.noRotate ];
            stage = arch.stages.test;
            tags = [ arch.tags.common ];
          };
        }
        {
          output = "/testTerraform/commonMarketplace";
          gitlabExtra = arch.extras.default // {
            rules = arch.rules.dev ++ [ rules.common.noRotate ];
            stage = arch.stages.test;
            tags = [ arch.tags.common ];
          };
        }
        {
          output = "/testTerraform/commonObservability";
          gitlabExtra = arch.extras.default // {
            rules = arch.rules.dev ++ [ rules.common.noRotate ];
            stage = arch.stages.test;
            tags = [ arch.tags.common ];
          };
        }
        {
          output = "/testTerraform/commonStatus";
          gitlabExtra = arch.extras.default // {
            rules = arch.rules.dev ++ [ rules.common.noRotate ];
            stage = arch.stages.test;
            tags = [ arch.tags.common ];
          };
        }
        {
          output = "/testTerraform/commonUsers";
          gitlabExtra = arch.extras.default // {
            rules = arch.rules.dev ++ [ rules.common.noRotate ];
            stage = arch.stages.test;
            tags = [ arch.tags.common ];
          };
        }
        {
          output = "/testTerraform/commonVpc";
          gitlabExtra = arch.extras.default // {
            rules = arch.rules.dev ++ [ rules.common.noRotate ];
            stage = arch.stages.test;
            tags = [ arch.tags.common ];
          };
        }
        {
          output = "/testTerraform/commonVpn";
          gitlabExtra = arch.extras.default // {
            rules = arch.rules.dev ++ [ rules.common.noRotate ];
            stage = arch.stages.test;
            tags = [ arch.tags.common ];
          };
        }
        {
          output = "/testTerraform/publicStorage";
          gitlabExtra = arch.extras.default // {
            rules = arch.rules.dev ++ [ rules.common.noRotate ];
            stage = arch.stages.test;
            tags = [ arch.tags.common ];
          };
        }
        {
          output = "/common/machine/sca";
          gitlabExtra = arch.extras.default // {
            rules = arch.rules.dev ++ [ rules.common.noRotate ];
            script = [ "m . /skims scan $PWD/common/sca_config.yaml" ];
            stage = arch.stages.test;
            tags = [ arch.tags.common ];
          };
        }
        {
          output = "/common/ai/association-rules/env/dev";
          gitlabExtra = arch.extras.default // {
            rules = arch.rules.dev ++ [ rules.common.noRotate ];
            stage = arch.stages.test;
            tags = [ arch.tags.common ];
          };
        }
        {
          output = "/common/ai/association-rules/env/runtime";
          gitlabExtra = arch.extras.default // {
            rules = arch.rules.dev ++ [ rules.common.noRotate ];
            stage = arch.stages.test;
            tags = [ arch.tags.common ];
          };
        }
        {
          output = "/common/ai/fluid-chatbot/check/types";
          gitlabExtra = arch.extras.default // {
            rules = arch.rules.dev ++ [ rules.common.noRotate ];
            stage = arch.stages.test;
            tags = [ arch.tags.common ];
          };
        }
        {
          output = "/common/ai/fluid-chatbot/check/tests";
          gitlabExtra = arch.extras.default // {
            artifacts = {
              paths = [ "common/ai/fluid-chatbot/coverage.xml" ];
              expire_in = "1 day";
            };
            rules = arch.rules.dev ++ [ rules.common.noRotate ];
            stage = arch.stages.test;
            tags = [ arch.tags.common ];
          };
        }
        {
          output = "/common/criteria/upload run compliance";
          gitlabExtra = arch.extras.default // {
            cache = {
              key = "$CI_COMMIT_REF_NAME-common-criteria-upload-run-compliance";
              paths = [ "common/criteria/upload/.venv" ];
            };
            resource_group = "$CI_JOB_NAME";
            rules = arch.rules.prod ++ [ rules.common.default ];
            stage = arch.stages.deploy;
            tags = [ arch.tags.common ];
          };
        }
        {
          output = "/common/criteria/upload run requirements";
          gitlabExtra = arch.extras.default // {
            cache = {
              key =
                "$CI_COMMIT_REF_NAME-common-criteria-upload-run-requirements";
              paths = [ "common/criteria/upload/.venv" ];
            };
            resource_group = "$CI_JOB_NAME";
            rules = arch.rules.prod ++ [ rules.common.default ];
            stage = arch.stages.deploy;
            tags = [ arch.tags.common ];
          };
        }
        {
          output = "/common/criteria/upload run vulnerabilities";
          gitlabExtra = arch.extras.default // {
            cache = {
              key =
                "$CI_COMMIT_REF_NAME-common-criteria-upload-run-vulnerabilities";
              paths = [ "common/criteria/upload/.venv" ];
            };
            resource_group = "$CI_JOB_NAME";
            rules = arch.rules.prod ++ [ rules.common.default ];
            stage = arch.stages.deploy;
            tags = [ arch.tags.common ];
          };
        }
        {
          output = "/common/criteria/upload run fixes";
          gitlabExtra = arch.extras.default // {
            cache = {
              key = "$CI_COMMIT_REF_NAME-common-criteria-upload-run-fixes";
              paths = [ "common/criteria/upload/.venv" ];
            };
            resource_group = "$CI_JOB_NAME";
            rules = arch.rules.prod ++ [ rules.common.default ];
            stage = arch.stages.deploy;
            tags = [ arch.tags.common ];
          };
        }
      ];
    };
    root = {
      gitlabPath = "/common/pipeline/root.yaml";
      jobs = [
        {
          output = "/common/docs build";
          gitlabExtra = arch.extras.default // {
            rules = arch.rules.dev ++ [ rules.root.noRotate ];
            stage = arch.stages.test;
            tags = [ arch.tags.common ];
          };
        }
        {
          output = "/common/docs lint";
          gitlabExtra = arch.extras.default // {
            rules = arch.rules.dev ++ [ rules.root.noRotate ];
            stage = arch.stages.test;
            tags = [ arch.tags.common ];
          };
        }
        {
          output = "/pipelineOnGitlab/root";
          gitlabExtra = arch.extras.default // {
            rules = arch.rules.dev ++ [ rules.root.noRotate ];
            stage = arch.stages.test;
            tags = [ arch.tags.common ];
          };
        }
        {
          output = "pages";
          gitlabExtra = arch.extras.default // {
            artifacts.paths = [ "common/docs/public" ];
            publish = "common/docs/public";
            resource_group = "deploy/$CI_JOB_NAME";
            rules = arch.rules.prod ++ [ rules.root.default ];
            script = [ "m . /common/docs build" ];
            stage = arch.stages.deploy;
            tags = [ arch.tags.common ];
            retry = {
              max = 2;
              when = [ "runner_system_failure" "script_failure" ];
            };
            variables.GIT_DEPTH = 0;
          };
        }
        {
          output = "/common/test/pipeline";
          gitlabExtra = arch.extras.default // {
            rules = arch.rules.dev ++ [ rules.root.default ];
            stage = arch.stages.test;
            tags = [ arch.tags.common ];
          };
        }
        {
          output = "/common/test/secrets";
          gitlabExtra = arch.extras.default // {
            rules = arch.rules.dev ++ [ rules.root.default ];
            stage = arch.stages.test;
            tags = [ arch.tags.common ];
          };
        }
        {
          output = "/common/test/machine_sast";
          gitlabExtra = arch.extras.default // {
            rules = arch.rules.dev ++ [ rules.root.default ];
            stage = arch.stages.test;
            tags = [ arch.tags.common ];
          };
        }
        {
          output = "/lintGitMailMap";
          gitlabExtra = arch.extras.default // {
            rules = arch.rules.dev ++ [ rules.root.default ];
            stage = arch.stages.test;
            tags = [ arch.tags.common ];
          };
        }
        {
          output = "/formatYaml";
          gitlabExtra = arch.extras.default // {
            rules = arch.rules.dev ++ [ rules.root.default ];
            stage = arch.stages.test;
            tags = [ arch.tags.common ];
          };
        }
        {
          output = "/common/test/pipeline";
          gitlabExtra = arch.extras.default // {
            rules = arch.rules.dev ++ [ rules.root.default ];
            stage = arch.stages.test;
            tags = [ arch.tags.common ];
          };
        }
        {
          output = "/common/compute/schedule/test";
          gitlabExtra = arch.extras.default // {
            rules = arch.rules.dev ++ [ rules.root.default ];
            stage = arch.stages.test;
            tags = [ arch.tags.common ];
          };
        }
        {
          output = "/common/test/base/job";
          gitlabExtra = arch.extras.default // {
            rules = arch.rules.dev ++ [ rules.root.default ];
            stage = arch.stages.test;
            tags = [ arch.tags.common ];
          };
        }
        {
          output = "/common/test/leaks";
          gitlabExtra = arch.extras.default // {
            rules = arch.rules.dev ++ [ rules.root.default ];
            stage = arch.stages.test;
            tags = [ arch.tags.common ];
          };
        }
        {
          output = "/common/test/spelling";
          gitlabExtra = arch.extras.default // {
            rules = arch.rules.dev ++ [ rules.root.default ];
            stage = arch.stages.test;
            tags = [ arch.tags.common ];
          };
        }
        {
          output = "/common/test/secrets/commit-changes";
          gitlabExtra = arch.extras.default // {
            rules = arch.rules.dev ++ [ rules.root.onlyRotate ];
            stage = arch.stages.test;
            tags = [ arch.tags.common ];
          };
        }
        {
          output = "/common/test/commitlint";
          gitlabExtra = arch.extras.default // {
            rules = arch.rules.dev ++ [ rules.root.default ];
            stage = arch.stages.test;
            tags = [ arch.tags.common ];
            variables.GIT_DEPTH = 0;
          };
        }
        {
          output = "/forces";
          gitlabExtra = arch.extras.default // {
            rules = arch.rules.dev ++ [ rules.root.default ];
            script = [ "m . /forces --token $FORCES_API_TOKEN --strict -v" ];
            stage = arch.stages.test;
            tags = [ arch.tags.common ];
            variables.GIT_DEPTH = 5;
          };
        }
        {
          output = "/formatBash";
          gitlabExtra = arch.extras.default // {
            rules = arch.rules.dev ++ [ rules.root.noRotate ];
            stage = arch.stages.test;
            tags = [ arch.tags.common ];
          };
        }
        {
          output = "/formatNix";
          gitlabExtra = arch.extras.default // {
            rules = arch.rules.dev ++ [ rules.root.noRotate ];
            stage = arch.stages.test;
            tags = [ arch.tags.common ];
          };
        }
        {
          output = "/formatTerraform";
          gitlabExtra = arch.extras.default // {
            rules = arch.rules.dev ++ [ rules.root.noRotate ];
            stage = arch.stages.test;
            tags = [ arch.tags.common ];
          };
        }
        {
          output = "/lintBash";
          gitlabExtra = arch.extras.default // {
            rules = arch.rules.dev ++ [ rules.root.noRotate ];
            stage = arch.stages.test;
            tags = [ arch.tags.common ];
          };
        }
        {
          output = "/lintNix";
          gitlabExtra = arch.extras.default // {
            rules = arch.rules.dev ++ [ rules.root.noRotate ];
            stage = arch.stages.test;
            tags = [ arch.tags.common ];
          };
        }
        {
          output = "/testLicense";
          gitlabExtra = arch.extras.default // {
            rules = arch.rules.dev ++ [ rules.root.noRotate ];
            stage = arch.stages.test;
            tags = [ arch.tags.common ];
          };
        }
        {
          output = "/common/test/complexipy";
          gitlabExtra = arch.extras.default // {
            rules = arch.rules.dev ++ [ rules.root.noRotate ];
            stage = arch.stages.test;
            tags = [ arch.tags.common ];
          };
        }
      ];
    };
  };
}
