# https://github.com/fluidattacks/makes
{ outputs, ... }: {
  deployTerraform = {
    modules = {
      commonFoss = {
        setup = [
          outputs."/secretsForAwsFromGitlab/prodCommon"
          outputs."/secretsForEnvFromSops/commonFoss"
          outputs."/secretsForTerraformFromEnv/commonFoss"
        ];
        src = "/common/foss/infra";
        version = "1.0";
      };
    };
  };
  lintTerraform = {
    modules = {
      commonFoss = {
        setup = [
          outputs."/secretsForAwsFromGitlab/dev"
          outputs."/secretsForEnvFromSops/commonFoss"
          outputs."/secretsForTerraformFromEnv/commonFoss"
        ];
        src = "/common/foss/infra";
        version = "1.0";
      };
    };
  };
  secretsForEnvFromSops = {
    commonFoss = {
      vars = [ "GITHUB_API_TOKEN" ];
      manifest = "/common/secrets/dev.yaml";
    };
  };
  secretsForTerraformFromEnv = {
    commonFoss = { githubToken = "GITHUB_API_TOKEN"; };
  };
  testTerraform = {
    modules = {
      commonFoss = {
        setup = [
          outputs."/secretsForAwsFromGitlab/dev"
          outputs."/secretsForEnvFromSops/commonFoss"
          outputs."/secretsForTerraformFromEnv/commonFoss"
        ];
        src = "/common/foss/infra";
        version = "1.0";
      };
    };
  };
}
