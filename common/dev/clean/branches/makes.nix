{ makeScript, makePythonEnvironment, projectPath, outputs, ... }: {
  jobs."/common/dev/clean/branches" = makeScript {
    replace = {
      __argCleanRepositoryBranches__ =
        projectPath "/common/dev/clean/branches/src/__init__.py";
    };
    name = "common_dev_clean_branches";
    entrypoint = ./entrypoint.sh;
    searchPaths = {
      source = [
        (makePythonEnvironment {
          pythonProjectDir = ./.;
          pythonVersion = "3.11";
        })
        outputs."/common/utils/aws"
        outputs."/common/utils/sops"
      ];
    };
  };
}
