# shellcheck shell=bash

function main {
  aws_login "dev" "3600" \
    && sops_export_vars "common/secrets/dev.yaml" \
      UNIVERSE_API_TOKEN \
    && python __argCleanRepositoryBranches__
}

main
