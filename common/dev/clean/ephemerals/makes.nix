{ inputs, makeScript, outputs, ... }: {
  jobs."/common/dev/clean/ephemerals" = makeScript {
    searchPaths = {
      bin = [
        inputs.nixpkgs.findutils
        inputs.nixpkgs.gawk
        inputs.nixpkgs.kubectl
        outputs."/integrates/jira/clean-ephemerals"
      ];
      kubeConfig = [ ".kubernetes" ];
      source = [ outputs."/common/utils/aws" outputs."/common/utils/sops" ];
    };
    name = "common-dev-clean-ephemerals";
    entrypoint = ./entrypoint.sh;
  };
}
