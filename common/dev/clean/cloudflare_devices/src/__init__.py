import logging
import os
from datetime import datetime, timedelta, timezone
from random import randint
from time import sleep
from typing import Callable, Generator, TypeVar

import cloudflare
import requests
from cloudflare import Cloudflare
from cloudflare.pagination import SyncSinglePage
from cloudflare.types.zero_trust.device import Device

logging.basicConfig(level=logging.INFO)
LOGGER = logging.getLogger()

CLOUDFLARE_EMAIL = os.environ.get("CLOUDFLARE_EMAIL")
CLOUDFLARE_API_KEY = os.environ.get("CLOUDFLARE_API_KEY")
ACCOUNT_ID = os.environ.get("CLOUDFLARE_ACCOUNT_ID")
API_BASE_URL = "https://api.cloudflare.com/client/v4"

HEADERS = {
    "X-Auth-Email": CLOUDFLARE_EMAIL,
    "X-Auth-Key": CLOUDFLARE_API_KEY,
    "Content-Type": "application/json",
}
client = Cloudflare(
    api_email=CLOUDFLARE_EMAIL,
    api_key=CLOUDFLARE_API_KEY,
)


one_month_ago = datetime.now(timezone.utc) - timedelta(days=30)
target_email = "non_identity@fluidattacks.cloudflareaccess.com"

T = TypeVar("T")


def should_retry(response: requests.Response) -> bool:
    try:
        response_json = response.json()
        return not response_json.get("success") and any(
            error.get("code") == 10000 for error in response_json.get("errors", [])
        )
    except ValueError:
        return False


def wait_before_retry(attempt: int, delay: int) -> None:
    wait_time = delay * (2**attempt) + randint(5, 15)
    LOGGER.info(f"Retrying in {wait_time} seconds...")
    sleep(wait_time)


def retry_on_rate_limit(
    retries: int = 5, delay: int = 60
) -> Callable[[Callable[..., requests.Response]], Callable[..., T]]:
    def decorator(
        func: Callable[..., requests.Response],
    ) -> Callable[..., T]:
        def wrapper(*args, **kwargs) -> T:
            for attempt in range(retries):
                try:
                    response = func(*args, **kwargs)

                    if isinstance(response, requests.Response) and should_retry(
                        response
                    ):
                        LOGGER.info("Throttled by Cloudflare (10000).")
                        wait_before_retry(attempt, delay)
                        continue

                    return response
                except cloudflare.RateLimitError:
                    LOGGER.info("Rate limited.")
                    wait_before_retry(attempt, delay)
                except Exception as e:
                    LOGGER.error(f"Error in {func.__name__}: {str(e)}")
                    raise

            raise cloudflare.RateLimitError("Max retries reached.")

        return wrapper

    return decorator


@retry_on_rate_limit(retries=8, delay=60)
def delete_device(device_id: str) -> requests.Response:
    url = f"{API_BASE_URL}/accounts/{ACCOUNT_ID}/devices/registrations/{device_id}"
    response = requests.delete(url, headers=HEADERS)
    if response.status_code == 200:
        LOGGER.info(f"Successfully deleted device: {device_id}")
        sleep(30)
    else:
        LOGGER.error(f"Failed to delete device {device_id}: {response.text}")
    return response


@retry_on_rate_limit(retries=8, delay=60)
def fetch_devices_page(cursor: str | None) -> SyncSinglePage[Device]:
    extra_query = {
        "email": target_email,
        "limit": 100,
        "direction": "asc",
        "order_by": "seen",
        "revoked": "true",
    }
    if cursor:
        extra_query["cursor"] = cursor

    devices_page = client.zero_trust.devices.list(
        account_id=ACCOUNT_ID,
        extra_query=extra_query,
    )
    return devices_page


@retry_on_rate_limit(retries=5, delay=60)
def revoke_devices(device_ids: list[str]) -> None:
    client.zero_trust.devices.revoke.create(
        account_id=ACCOUNT_ID,
        body=device_ids,
    )
    LOGGER.info(f"Successfully revoked devices: {device_ids}")


def paginate_devices(
    cursor: str | None,
) -> Generator[list[dict], None, None]:
    next_cursor = cursor

    devices_page = fetch_devices_page(next_cursor)
    yield devices_page.result

    next_cursor = devices_page.result_info.get("cursor")

    while next_cursor is not None:
        devices_page = fetch_devices_page(next_cursor)
        yield devices_page.result
        next_cursor = devices_page.result_info.get("cursor")


def fetch_matching_devices() -> None:
    try:
        for devices in paginate_devices(None):
            filtered_devices = [
                device
                for device in devices
                if device.user
                and device.user.email == target_email
                and device.last_seen
                and device.last_seen < one_month_ago
            ]
            if filtered_devices:
                device_info = [
                    f"ID: {device.id}, Name: {device.name}, Last Seen: {device.last_seen}"
                    for device in filtered_devices
                ]
                LOGGER.info("Devices matching the criteria:")
                LOGGER.info("\n".join(device_info))
            else:
                LOGGER.info("No devices found matching the criteria.")
            device_ids_to_delete = [device.id for device in filtered_devices]

            if device_ids_to_delete:
                for device_id in device_ids_to_delete:
                    delete_device(device_id)

    except Exception as e:
        LOGGER.error("Error processing devices: %s", str(e))


if __name__ == "__main__":
    fetch_matching_devices()
