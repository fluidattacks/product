{ makeScript, outputs, projectPath, makePythonEnvironment, ... }: {
  jobs."/common/dev/clean/cloudflare_devices" = makeScript {
    replace = {
      __argCleanCloudflareDevices__ =
        projectPath "/common/dev/clean/cloudflare_devices/src/__init__.py";
    };
    searchPaths = {
      source = [
        (makePythonEnvironment {
          pythonProjectDir = ./.;
          pythonVersion = "3.11";
        })
        outputs."/common/utils/sops"
      ];
    };
    name = "common_dev_clean_cloudflare_devices";
    entrypoint = ./entrypoint.sh;
  };
}
