{ inputs, ... }:
let
  src = let
    platformMappings = {
      x86_64-linux = {
        platform = "linux_amd64";
        sha256 = "sha256-Z8iPbDvfiEMBeUw+yR+ebj9mDn3jt+EM0p+70pG6rFA=";
      };
      aarch64-linux = {
        platform = "linux_arm64";
        sha256 = "sha256-O5DEn6rzNnM4vZXbC/duyGv0yn0XXUKiIeJ73I0mUlY=";
      };
      aarch64-darwin = {
        platform = "darwin_arm64";
        sha256 = "sha256-DwGQmTfH8bW1jVXlOn1TZvO0+PxI3DHjkd1VNA6hrpU=";
      };
    };
    currentPlatform = builtins.currentSystem;
    platformInfo = builtins.getAttr currentPlatform platformMappings;
    version = "v0.9.2";
  in inputs.nixpkgs.fetchurl {
    url =
      "https://github.com/cue-lang/cue/releases/download/${version}/cue_${version}_${platformInfo.platform}.tar.gz";
    inherit (platformInfo) sha256;
  };
in inputs.nixpkgs.stdenv.mkDerivation {
  unpackPhase = ''
    mkdir src
    tar -xf $src -C src
  '';
  installPhase = ''
    mkdir -p $out/bin
    chmod +x src/cue
    mv src/cue $out/bin
  '';
  name = "common-utils-cue";
  inherit src;
}
