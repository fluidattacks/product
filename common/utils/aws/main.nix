{ inputs, makeTemplate, ... }:
makeTemplate {
  searchPaths = {
    bin = [
      # https://github.com/NixOS/nixpkgs/issues/47900
      (inputs.nixpkgs.awscli.overrideAttrs (old: {
        makeWrapperArgs = (old.makeWrapperArgs or [ ])
          ++ [ "--unset" "PYTHONPATH" ];
      }))
      inputs.nixpkgs.jq
    ];
  };
  name = "utils-bash-lib-aws";
  template = ./template.sh;
}
