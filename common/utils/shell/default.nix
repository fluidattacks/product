{ pkgs }: rec {
  core = {
    scriptStrict = script:
      ''
        set -o errexit
        set -o pipefail
        set -o nounset
        set -o functrace
        set -o errtrace
        set -o monitor
        shopt -s dotglob
      ''
      + (if builtins.isString script then script else builtins.readFile script);
    writeShellApplicationStrict = { name, runtimeInputs ? [ ], script }:
      pkgs.writeShellApplication {
        inherit name;
        inherit runtimeInputs;
        text = core.scriptStrict script;
        bashOptions = [ ];
      };
  };

  strict = core.scriptStrict;

  aws = core.writeShellApplicationStrict {
    name = "utils-aws";
    runtimeInputs = [ pkgs.awscli pkgs.jq pkgs.saml2aws ];
    script = ./aws.sh;
  };
  sops = core.writeShellApplicationStrict {
    name = "utils-sops";
    runtimeInputs = [ pkgs.gnugrep pkgs.jq pkgs.sops ];
    script = ./sops.sh;
  };
}
