{ pkgs }: {
  cachix = import ./cachix { inherit pkgs; };
  shell = import ./shell { inherit pkgs; };
  ssl-cert = import ./ssl-cert { inherit pkgs; };
}
