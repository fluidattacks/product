# shellcheck shell=bash

function main {
  : && info "Launching Jaeger" \
    && info "Jaeger UI on http://localhost:16686" \
    && __argJaeger__/bin/jaeger-all-in-one
}

main "${@}"
