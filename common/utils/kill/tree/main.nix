{ inputs, makeScript, ... }:
makeScript {
  name = "common-kill-tree";
  searchPaths = { bin = [ inputs.nixpkgs.gawk inputs.nixpkgs.procps ]; };
  entrypoint = ./entrypoint.sh;
}
