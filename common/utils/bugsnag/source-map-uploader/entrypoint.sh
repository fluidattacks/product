# shellcheck shell=bash

function main {
  local api_key="${1}"
  local base_url="${2}"
  local directory="${3}"
  local npm_path="common/utils/bugsnag/source-map-uploader/npm"

  if running_in_ci_cd_provider; then
    npm_path=__argNPMPath__
  fi

  : && pushd "${npm_path}" \
    && npm ci \
    && export PATH="${PWD}/node_modules/.bin:${PATH}" \
    && popd \
    && bugsnag-source-maps upload-browser \
      --api-key "${api_key}" \
      --app-version "${CI_COMMIT_SHORT_SHA}" \
      --base-url "${base_url}" \
      --directory "${directory}" \
      --overwrite

}

main "${@}"
