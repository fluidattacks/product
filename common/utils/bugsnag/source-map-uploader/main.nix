{ inputs, makeScript, projectPath, ... }:
makeScript {
  name = "bugsnag-source-map-uploader";
  replace = {
    __argNPMPath__ =
      projectPath "/common/utils/bugsnag/source-map-uploader/npm";
  };
  searchPaths.bin = [ inputs.nixpkgs.nodejs_20 ];
  entrypoint = ./entrypoint.sh;
}
