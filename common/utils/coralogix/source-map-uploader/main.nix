{ inputs, isDarwin, makeTemplate, projectPath, ... }:
let
  libcPackage = if isDarwin then inputs.nixpkgs.clang else inputs.nixpkgs.musl;
in makeTemplate {
  replace = {
    __argNpmDir__ =
      projectPath "/common/utils/coralogix/source-map-uploader/npm";
  };
  name = "coralogix-source-map-uploader";
  searchPaths = {
    rpath = [ libcPackage ];
    bin = [ inputs.nixpkgs.bash inputs.nixpkgs.nodejs_20 ];
  };
  template = ./template.sh;
}
