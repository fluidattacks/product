# shellcheck shell=bash

function coralogix_source_map_uploader {
  local api_key="${1}"
  local project="${2}"
  local directory="${3}"

  pushd "__argNpmDir__" \
    && npm install \
    && ./node_modules/.bin/coralogix-rum-cli upload-source-maps -k "${api_key}" \
      -a "${project}" \
      -e "US2" \
      -v "${CI_COMMIT_SHORT_SHA}" \
      -f "${directory}" \
    && popd \
    || return 1
}
