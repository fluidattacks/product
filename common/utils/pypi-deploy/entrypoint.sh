# shellcheck shell=bash

function _process_api_token_name {
  local lib="${1}"
  local to_uppercase
  local dashes_to_underscores
  local result

  : && to_uppercase="${lib^^}" \
    && dashes_to_underscores="${to_uppercase//-/_}" \
    && result="PYPI_API_TOKEN_${dashes_to_underscores}" \
    && echo "${result}"
}

function main {
  local lib="${1}"
  local project_dir="${2:-"common/${lib}"}"
  local pyproject_toml="${3:-"${project_dir}/pyproject.toml"}"
  local api_token_name

  if ! git diff HEAD~1 HEAD -- "${pyproject_toml}" | grep -q '^[-+]version'; then
    : && info "${pyproject_toml} version has not changed. Skipping deployment." \
      && return 0
  fi

  : && api_token_name="$(_process_api_token_name "${lib}")" \
    && aws_login "prod_common" "3600" \
    && sops_export_vars "common/secrets/prod.yaml" \
      "${api_token_name}" \
    && info "Publishing new version for ${project_dir}" \
    && pushd "${project_dir}" \
    && rm -rf "dist" \
    && poetry publish \
      --build \
      --no-interaction \
      --username "__token__" \
      --password "${!api_token_name}" \
    && popd \
    || return 1
}

main "${@}"
