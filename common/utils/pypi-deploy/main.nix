{ inputs, makeScript, outputs, ... }:
makeScript {
  entrypoint = ./entrypoint.sh;
  name = "common-utils-pypi-deploy";
  searchPaths = {
    bin = [ inputs.nixpkgs.git inputs.nixpkgs.poetry ];
    source = [ outputs."/common/utils/aws" outputs."/common/utils/sops" ];
  };
}
