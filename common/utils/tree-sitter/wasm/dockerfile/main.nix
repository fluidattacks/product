{ inputs, outputs, ... }:
let
  build = import ../. { inherit inputs outputs; };
  name = builtins.baseNameOf ./.;
  src = builtins.fetchTarball {
    sha256 = "sha256:0nxw8q0sxmmpw6v0vw10075rkp5mfw1v2hx1hwkmyq8qlfml6d5q";
    url =
      "https://github.com/camdencheek/tree-sitter-dockerfile/archive/087daa20438a6cc01fa5e6fe6906d77c869d19fe.tar.gz";
  };
in build { inherit name src; }
