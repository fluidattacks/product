{ inputs, outputs, ... }:
inputs.nixpkgs.stdenv.mkDerivation {
  buildPhase = ''
    export HOME=.
    mkdir -p $out
    cp $wasm $out/tree-sitter-java.wasm
  '';
  name = "tree-sitter-java";
  dontUnpack = true;
  sourceRoot = ".";
  wasm = builtins.fetchurl {
    sha256 = "sha256:13g694cpj0qpj5jwljdjqr38dx64xkhmlnsh6clrdq5gv0p4gm2r";
    url =
      "https://github.com/tree-sitter/tree-sitter-java/releases/download/v0.23.4/tree-sitter-java.wasm";
  };
}
