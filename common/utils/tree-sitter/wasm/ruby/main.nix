{ inputs, outputs, ... }:
inputs.nixpkgs.stdenv.mkDerivation {
  buildPhase = ''
    export HOME=.
    mkdir -p $out
    cp $wasm $out/tree-sitter-ruby.wasm
  '';
  name = "tree-sitter-ruby";
  dontUnpack = true;
  sourceRoot = ".";
  wasm = builtins.fetchurl {
    sha256 = "sha256:1x4nzxcvvijw4g02bh59ss8lmz134a0xj327xl9hcby7swkn9a89";
    url =
      "https://github.com/tree-sitter/tree-sitter-ruby/releases/download/v0.23.1/tree-sitter-ruby.wasm";
  };
}
