{ inputs, outputs, ... }:
inputs.nixpkgs.stdenv.mkDerivation {
  buildPhase = ''
    export HOME=.
    mkdir -p $out
    cp $wasm $out/tree-sitter-toml.wasm
  '';
  name = "tree-sitter-toml";
  dontUnpack = true;
  sourceRoot = ".";
  wasm = builtins.fetchurl {
    sha256 = "sha256:17z0wmv1y17kdhd97cvdxgsbxg7ng06f09c3gy2nhnn34qwaiihs";
    url =
      "https://github.com/tree-sitter-grammars/tree-sitter-toml/releases/download/v0.7.0/tree-sitter-toml.wasm";
  };
}
