{ inputs, outputs, }:
{ name ? null, overrides ? { }, src ? null, }:
inputs.nixpkgs.stdenv.mkDerivation ({
  buildInputs = [ outputs."/common/utils/tree-sitter" ];
  buildPhase = ''
    export HOME=.
    mkdir -p $out
    tree-sitter build --wasm --output tree-sitter-${name}.wasm .
    mv tree-sitter-${name}.wasm $out
  '';
  name = "tree-sitter-${name}";
  inherit src;
} // overrides)
