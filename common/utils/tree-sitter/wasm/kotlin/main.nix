{ inputs, outputs, ... }:
inputs.nixpkgs.stdenv.mkDerivation {
  buildPhase = ''
    export HOME=.
    mkdir -p $out
    cp $wasm $out/tree-sitter-kotlin.wasm
  '';
  name = "tree-sitter-kotlin";
  dontUnpack = true;
  sourceRoot = ".";
  wasm = builtins.fetchurl {
    sha256 = "sha256:0bb0nwabi3bcdbckxgjyal94qmk763kp85nqqnnjh71p7d2ff966";
    url =
      "https://github.com/fwcd/tree-sitter-kotlin/releases/download/0.3.8/tree-sitter-kotlin.wasm";
  };
}
