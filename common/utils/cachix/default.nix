{ pkgs }: {
  config.default = if pkgs.stdenv.isLinux then {
    enable = true;
    pull = [ "devenv" "fluidattacks" ];
    push = "fluidattacks";
  } else {
    enable = false;
  };
}
