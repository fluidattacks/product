import json
import logging
from typing import (
    Any,
)

import boto3
import botocore.exceptions
import click

logging.basicConfig(
    level=logging.INFO,
    format="[%(levelname)s] %(message)s",
)
PROCESS_SBOM_LOGGER = logging.getLogger("process_sbom")

SQS_QUEUE_URLS = {
    "integrates_sbom": ("https://sqs.us-east-1.amazonaws.com/205810638802/integrates_sbom"),
    "integrates_mail_sbom": (
        "https://sqs.us-east-1.amazonaws.com/205810638802/integrates_mail_sbom"
    ),
}


def send_sqs_message(queue_url: str, message_body: str) -> None:
    client = boto3.client("sqs")
    try:
        client.send_message(QueueUrl=queue_url, MessageBody=message_body)
        PROCESS_SBOM_LOGGER.info(
            "Message sent to queue: %s",
            queue_url,
            extra={"extra": {"message_body": message_body}},
        )
    except botocore.exceptions.ClientError as err:
        error_message = f"Error occurred while sending message to SQS: {err}"
        raise RuntimeError(error_message) from err


@click.group(invoke_without_command=True)
@click.pass_context
def main(_ctx: Any) -> None:
    """
    This function is intentionally empty because it serves as a base command
    for the command-line interface. It doesn't need to do anything itself,
    its purpose is to hold subcommands.
    """


@main.command()
@click.option("--sbom-format", required=True)
def get_file_extension(sbom_format: str) -> None:
    error_message = "Invalid SBOM format, unable to determine file extension"
    try:
        if "-" in sbom_format:
            file_extension = sbom_format.split("-")[-1]
            PROCESS_SBOM_LOGGER.info(
                "%s file extension extracted from %s format",
                file_extension,
                sbom_format,
            )
            click.echo(file_extension)
        else:
            raise ValueError(error_message)
    except ValueError:
        PROCESS_SBOM_LOGGER.exception(error_message)
        raise


@main.command()
@click.option("--execution-id", required=True)
@click.option("--image-ref", required=False)
def submit_task(execution_id: str, image_ref: str | None) -> None:
    message_body = json.dumps(
        {
            "id": execution_id,
            "task": "sbom",
            "args": [
                execution_id,
                image_ref,
            ],
        },
    )
    error_message = "Failed to submit task to the 'integrates_sbom' SQS queue"
    try:
        send_sqs_message(SQS_QUEUE_URLS["integrates_sbom"], message_body)
    except RuntimeError:
        PROCESS_SBOM_LOGGER.exception(
            error_message,
            extra={
                "queue_url": SQS_QUEUE_URLS["integrates_sbom"],
                "message_body": message_body,
                "execution_id": execution_id,
            },
        )
        raise


@main.command()
@click.option("--execution-id", required=True)
@click.option("--requester-email", required=True)
@click.option("--sbom-format", required=True)
@click.option("--notification-id", required=False)
@click.option("--image-ref", required=False)
def submit_mail_task(
    execution_id: str,
    requester_email: str,
    sbom_format: str,
    notification_id: str | None,
    image_ref: str | None,
) -> None:
    message_body = json.dumps(
        {
            "id": execution_id,
            "task": "mail_sbom",
            "args": [
                execution_id,
                requester_email,
                sbom_format,
                notification_id,
                image_ref,
            ],
        },
    )

    try:
        send_sqs_message(SQS_QUEUE_URLS["integrates_mail_sbom"], message_body)
    except RuntimeError:
        error_message = "Failed to submit task to the 'integrates_mail_sbom' SQS queue"
        PROCESS_SBOM_LOGGER.exception(
            error_message,
            extra={
                "queue_url": SQS_QUEUE_URLS["integrates_mail_sbom"],
                "message_body": message_body,
                "execution_id": execution_id,
            },
        )
        raise
