{ inputs, outputs, projectPath, makeScript, ... }:
makeScript {
  name = "integrates-execute-sbom";
  searchPaths = {
    pythonPackage = [ (projectPath "/integrates/jobs/execute_sbom") ];
    bin = [
      outputs."/melts"
      outputs."/skims/sbom"
      inputs.nixpkgs.python311
      inputs.nixpkgs.jq
      inputs.nixpkgs.findutils
    ];
    source = [
      outputs."/integrates/jobs/execute_sbom/env"
      outputs."/common/utils/aws"
      outputs."/common/utils/sops"
      outputs."/secretsForEnvFromSops/sbom"
    ];
  };
  entrypoint = ./entrypoint.sh;
}
