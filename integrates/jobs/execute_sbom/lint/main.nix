{ makeScript, outputs, ... }:
makeScript {
  name = "integrates-streams-lint";
  entrypoint = ''
    pushd integrates/jobs/execute_sbom

    if test -n "''${CI:-}"; then
      ruff format --config ruff.toml --diff
      ruff check --config ruff.toml
    else
      ruff format --config ruff.toml
      ruff check --config ruff.toml --fix
    fi

    mypy --config-file mypy.ini $PWD
  '';
  searchPaths.source = [ outputs."/integrates/jobs/execute_sbom/env" ];
}
