# shellcheck shell=bash

function update_dynamodb_item() {
  local dynamo_key="${1}"
  local root="${2}"
  local additional_info="${3}"

  updated_info=$(python3 __argScript__ remove-root-info \
    --additional-info "${additional_info}" \
    --root-nickname "${root}")

  aws dynamodb update-item \
    --table-name "fi_async_processing" \
    --key "${dynamo_key}" \
    --update-expression "SET additional_info = :value" \
    --expression-attribute-values '{":value": {"S": "'"${updated_info}"'"}}' \
    || true

  return 0
}

function download_apk_files() {
  local group_name="${1}"
  local root="${2}"
  local file_config="${3}"

  if grep -q "apk:" "$file_config"; then
    info "Downloading root apks"
    melts --init pull-root-apks --group "${group_name}" --root "${root}"
    if [ ! -d "groups/${group_name}/${root}/fa_apks_to_analyze" ]; then
      error "Failed apk downloads for the root ${root}"
      return 1
    fi
  fi
}

function connect_to_warp() {
  local network_name="${1}"

  info "WARP: Connecting to ${network_name}"
  network_info="$(warp-cli --accept-tos vnet | grep -B 1 "${network_name}")"
  if [ -z "${network_info}" ]; then
    error "WARP: Network not found"
    return
  fi
  : && warp-cli --accept-tos connect \
    && sleep 10 \
    && network_id="$(echo "${network_info}" | head --lines 1 | awk '{print $2}')" \
    && warp-cli --accept-tos vnet "${network_id}" \
    && sleep 10 \
    && info "WARP: Connected to network ID: ${network_id}"
}

function config_and_execute_machine() {
  local dynamo_item="${1}"
  local group_name="${2}"
  local dynamo_key="${3}"

  additional_info="$(echo "${dynamo_item}" | jq '.Item.additional_info.S' -r)"
  user_email="$(echo "${dynamo_item}" | jq '.Item.subject.S' -r)"

  network_name="$(echo "${additional_info}" | jq -c -r '.network_name // ""')"
  if [ -n "${network_name}" ]; then
    connect_to_warp "${network_name}"
  fi

  roots_array=()
  roots="$(echo "${additional_info}" | jq -c -r '.roots[]')"
  while IFS= read -r root; do
    roots_array+=("$root")
  done <<< "$roots"

  config_files=()
  roots_config_files="$(echo "${additional_info}" | jq -c -r '.roots_config_files[]')"
  while IFS= read -r config; do
    config_files+=("$config")
  done <<< "$roots_config_files"

  for i in "${!roots_array[@]}"; do
    info "Processing root $((i + 1)) out of ${#roots_array[@]}" \
      && root="${roots_array[i]}" \
      && if ! melts --init pull-repos \
        --group "$(echo "${dynamo_item}" | jq -c -r '.Item.entity.S')" \
        --root "${root}"; then
        error "Failed repository cloning"
        continue
      fi \
      && if [ ! -d "groups/${group_name}/${root}/" ]; then
        error "Failed repository cloning"
        continue
      fi \
      && aws s3 cp \
        "s3://machine.data/configs/${config_files[i]}" \
        "groups/${group_name}/${root}/execution_configs/" \
      && file_config=$(find "groups/${group_name}/${root}/execution_configs/" -name "*.yaml") \
      && if [ ! -e "$file_config" ]; then
        error "No machine configuration found for the execution"
        continue
      fi \
      && download_apk_files "${group_name}" "${root}" "${file_config}" \
      && pushd "groups/${group_name}/${root}" \
      && filename="$(basename "${file_config}")" \
      && mkdir -p "execution_results" \
      && head_commit_sha="$(git log -1 --format=%H)" \
      && head_commit_date="$(git log -1 --format=%cI)" \
      && skims scan "execution_configs/$filename" \
      && execution_id="${filename%.*}" \
      && execution_result="execution_results/${execution_id}.sarif" \
      && if test -f "${execution_result}"; then
        aws s3 cp "${execution_result}" s3://machine.data/results/ \
          && python3 __argScript__ submit-task \
            --execution-id "${execution_id}" \
            --commit "${head_commit_sha}" \
            --commit-date "${head_commit_date}" \
            --user-email "${user_email}" \
          && update_dynamodb_item "${dynamo_key}" "${root}" "${additional_info}"
      else
        error "No execution result"
      fi \
      && { popd || continue; }
  done
}

function main() {
  local dynamo_item
  local group_name
  local dynamo_key

  aws_login "dev" "3600" \
    && sops_export_vars "common/secrets/dev.yaml" \
      CACHIX_AUTH_TOKEN \
      INTEGRATES_API_TOKEN \
    && aws_login "prod_integrates" "3600" \
    && dynamo_key="{\"pk\":{\"S\": \"${2}\"}}" \
    && dynamo_item="$(aws dynamodb get-item \
      --table-name "fi_async_processing" \
      --key "${dynamo_key}")" \
    && group_name="$(echo "${dynamo_item}" \
      | jq '.Item.entity.S' -r)" \
    && chown -R "${USER}" "$(pwd)" \
    && git config --global --add safe.directory '*' \
    && config_and_execute_machine "${dynamo_item}" "${group_name}" "${dynamo_key}" \
    && aws dynamodb delete-item \
      --table-name "fi_async_processing" \
      --key "${dynamo_key}"
}

main "${@}"
