import json
import logging
import sys
from typing import (
    Any,
)

import boto3
import click

logging.basicConfig(
    level=logging.INFO,
    format="[%(levelname)s] %(message)s",
)
LOGGER = logging.getLogger(__name__)


@click.group(invoke_without_command=True)
@click.pass_context
def cli(_ctx: Any) -> None:
    """
    This function is intentionally empty because it serves as a base command
    for the command-line interface. It doesn't need to do anything itself,
    its purpose is to hold subcommands.
    """


@cli.command()
@click.option("--additional-info", required=True)
@click.option("--root-nickname", required=True)
def remove_root_info(
    additional_info: str,
    root_nickname: str,
) -> None:
    db_item_info: dict[str, list[str]] = json.loads(additional_info)
    root_nicknames: list[str] = db_item_info.get("roots", [])
    root_config_files: list[str] = db_item_info.get("roots_config_files", [])

    if root_nicknames and root_config_files and root_nickname in root_nicknames:
        index = root_nicknames.index(root_nickname)
        if index < len(root_config_files):
            updated_configs = root_config_files[index + 1 :]
            db_item_info["roots_config_files"] = updated_configs
            updated_roots = root_nicknames[index + 1 :]
            db_item_info["roots"] = updated_roots

    LOGGER.info("Updated additional info to remove root: %s", root_nickname)
    LOGGER.info("New additional info:\n %s", db_item_info)
    click.echo(json.dumps(db_item_info).replace('"', '\\"'))


@cli.command()
@click.option("--execution-id", required=True)
@click.option("--commit", required=True)
@click.option("--commit-date", required=True)
@click.option("--user-email", required=True)
def submit_task(
    execution_id: str,
    commit: str,
    commit_date: str,
    user_email: str,
) -> None:
    client = boto3.client("sqs")
    client.send_message(
        QueueUrl=("https://sqs.us-east-1.amazonaws.com/205810638802/integrates_report"),
        MessageBody=json.dumps(
            {
                "id": execution_id,
                "task": "report",
                "args": [
                    execution_id,
                    commit,
                    commit_date,
                    user_email,
                ],
            },
        ),
    )
    LOGGER.info(
        "Task submitted to SQS queue for execution id %s",
        execution_id,
    )

    sys.exit(0)


if __name__ == "__main__":
    cli()  # pylint: disable=no-value-for-parameter
