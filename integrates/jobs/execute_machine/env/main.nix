{ inputs, makePythonEnvironment, makeSearchPaths, projectPath, ... }:
let
  src = projectPath "/integrates/jobs/execute_machine";
  pythonDeps = makePythonEnvironment {
    pythonProjectDir = src;
    pythonVersion = "3.11";
  };
in makeSearchPaths {
  bin = [ inputs.nixpkgs.git inputs.nixpkgs.jq inputs.nixpkgs.findutils ];
  pythonPackage = [ src ];
  source = [ pythonDeps ];
}
