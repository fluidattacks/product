{ inputs, outputs, makeScript, ... }:
makeScript {
  name = "integrates-execute-machine";
  replace = { __argScript__ = ./src/__init__.py; };
  searchPaths = {
    bin = [
      outputs."/skims"
      outputs."/melts"
      inputs.nixpkgs.gawk
      inputs.nixpkgs.gnugrep
      inputs.nixpkgs.python311
    ] ++ inputs.nixpkgs.lib.optionals inputs.nixpkgs.stdenv.isLinux
      [ inputs.nixpkgs.cloudflare-warp ];
    source = [
      outputs."/integrates/jobs/execute_machine/env"
      outputs."/common/utils/aws"
      outputs."/common/utils/sops"
    ];
  };
  entrypoint = ./entrypoint.sh;
}
