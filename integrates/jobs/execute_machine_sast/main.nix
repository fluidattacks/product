{ inputs, outputs, makeScript, ... }:
makeScript {
  name = "integrates-execute-machine-sast";
  replace = { __argScript__ = ./scan/__init__.py; };
  searchPaths = {
    bin = [
      outputs."/skims"
      outputs."/melts"
      outputs."/skims/sbom"
      inputs.nixpkgs.gawk
      inputs.nixpkgs.gnugrep
      inputs.nixpkgs.python311
    ] ++ inputs.nixpkgs.lib.optionals inputs.nixpkgs.stdenv.isLinux
      [ inputs.nixpkgs.cloudflare-warp ];
    source = [
      outputs."/integrates/jobs/execute_machine_sast/env"
      outputs."/common/utils/aws"
      outputs."/common/utils/sops"
      outputs."/integrates/jobs/execute_sbom/env"
      outputs."/secretsForEnvFromSops/sbom"
    ];
  };
  entrypoint = ./entrypoint.sh;
}
