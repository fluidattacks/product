import boto3
import click
import json
import logging
import sys
from typing import (
    Any,
)
import botocore.exceptions

logging.basicConfig(
    level=logging.INFO,
    format="[%(levelname)s] %(message)s",
)
LOGGER = logging.getLogger(__name__)

def send_sqs_message(queue_url: str, message_body: str) -> None:
    client = boto3.client("sqs")
    try:
        client.send_message(QueueUrl=queue_url, MessageBody=message_body)
        LOGGER.info(
            "Message sent to queue: %s",
            queue_url,
            extra={"extra": {"message_body": message_body}},
        )
        LOGGER.info("Message body: %s", message_body)
    except botocore.exceptions.ClientError as err:
        error_message = f"Error occurred while sending message to SQS: {err}"
        raise RuntimeError(error_message) from err

@click.group(invoke_without_command=True)
@click.pass_context
def cli(_ctx: Any) -> None:
    """
    This function is intentionally empty because it serves as a base command
    for the command-line interface. It doesn't need to do anything itself,
    its purpose is to hold subcommands.
    """


@cli.command()
@click.option("--additional-info", required=True)
@click.option("--root-nickname", required=True)
def remove_root_info(
    additional_info: str,
    root_nickname: str,
) -> None:
    db_item_info: dict[str, list[str]] = json.loads(additional_info)
    root_nicknames: list[str] = db_item_info.get("roots", [])
    root_config_files: list[str] = db_item_info.get("roots_config_files", [])

    if (
        root_nicknames
        and root_config_files
        and root_nickname in root_nicknames
    ):
        index = root_nicknames.index(root_nickname)
        if index < len(root_config_files):
            updated_configs = root_config_files[index + 1 :]
            db_item_info["roots_config_files"] = updated_configs
            updated_roots = root_nicknames[index + 1 :]
            db_item_info["roots"] = updated_roots

    LOGGER.info("Updated additional info to remove root: %s", root_nickname)
    LOGGER.info("New additional info:\n %s", db_item_info)
    click.echo(json.dumps(db_item_info).replace('"', '\\"'))


@cli.command()
@click.option("--execution-id", required=True)
@click.option("--commit", required=True)
@click.option("--commit-date", required=True)
@click.option("--user-email", required=True)
@click.option("--sbom-id", required=True)
def submit_task(
    execution_id: str,
    commit: str,
    commit_date: str,
    user_email: str,
    sbom_id: str,
) -> None:
    message_body = json.dumps(
        {
            "id": execution_id,
            "task": "report_sbom_and_machine",
            "args": [
                execution_id,
                sbom_id,
                commit,
                commit_date,
                user_email,
            ],
        }
    )

    error_message = "Failed to submit task to the 'integrates_report' SQS queue"
    try:
        send_sqs_message(
            "https://sqs.us-east-1.amazonaws.com/205810638802/integrates_report",
            message_body
        )
    except RuntimeError:
        LOGGER.exception(
            error_message,
            extra={
                "message_body": message_body,
                "execution_id": execution_id,
            },
        )
        raise

    sys.exit(0)


if __name__ == "__main__":
    cli()  # pylint: disable=no-value-for-parameter
