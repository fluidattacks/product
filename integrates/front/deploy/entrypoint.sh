# shellcheck shell=bash

function _replace {
  local src="${1}"
  local from="${2}"
  local to="${3}"

  find "${src}" -type f -exec sed -i "s|${from}|${to}|gI" {} +
}

function _build {
  local branch="${1}"
  local dir="integrates/front"

  pushd "${dir}" || error "${dir} not found"
  npm ci
  export PATH="${PWD}/node_modules/.bin:${PATH}"
  export NODE_PATH="${PWD}/node_modules:${NODE_PATH}"
  info "Building for ${branch}"
  if [[ ${branch} == "trunk" ]]; then
    info "Building for production environment"
    npm run build
  else
    info "Building for ephemeral environment"
    npm run build-eph
  fi
  popd || error "root not found"
  copy integrates/back/integrates/app/templates/static integrates/app/static
}

function main {
  export NODE_OPTIONS="--max-old-space-size=4096"
  export CI_COMMIT_REF_NAME
  export CI_COMMIT_SHA
  export CI_COMMIT_SHORT_SHA

  local env="${1}"
  local bugsnag_key="99a64555a50340cfa856f6623c6bf35d"
  local bucket="integrates.front.fluidattacks.com"
  local base_url
  local aws_role

  if test -z "${CI_COMMIT_REF_NAME-}"; then
    CI_COMMIT_REF_NAME="$(git rev-parse --abbrev-ref HEAD)"
  fi
  if test -z "${CI_COMMIT_SHA-}"; then
    CI_COMMIT_SHA="$(git rev-parse HEAD)"
  fi
  if test -z "${CI_COMMIT_SHORT_SHA-}"; then
    CI_COMMIT_SHORT_SHA="${CI_COMMIT_SHA:0:8}"
  fi

  case "${env}" in
    dev) aws_role="dev" ;;
    prod) aws_role="prod_integrates" ;;
    *) error "First argument must be one of: dev, prod" ;;
  esac
  base_url="${bucket}/${CI_COMMIT_REF_NAME}"

  _build "${CI_COMMIT_REF_NAME}"

  pushd integrates || error "integrates not found"
  aws_login "${aws_role}" "3600"
  sops_export_vars "secrets/${env}.yaml" \
    CLOUDFLARE_API_TOKEN \
    CLOUDFLARE_API_TOKEN_INTEGRATES
  _replace app "__CI_COMMIT_REF_NAME__" "${CI_COMMIT_REF_NAME}"
  _replace app "__CI_COMMIT_SHA__" "${CI_COMMIT_SHA}"
  _replace app "__CI_COMMIT_SHORT_SHA__" "${CI_COMMIT_SHORT_SHA}"
  _replace app "__INTEGRATES_BUCKET_NAME__" "${bucket}"
  aws_s3_sync app "s3://${base_url}/"
  bugsnag-source-map-uploader "${bugsnag_key}" "https://${base_url}" "./app/static/dashboard" --overwrite
  bugsnag-announce "${bugsnag_key}" "${env}"

  if [[ ${env} == "prod" ]]; then
    coralogix_source_map_uploader "ff40b039-9ac1-4ed0-bde5-023996b931d5" "integrates-prod" "${PWD}/app/static/dashboard/"
  fi
}

main "${@}"
