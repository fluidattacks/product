{ inputs, makeScript, outputs, ... }: {
  jobs."/integrates/front/deploy" = makeScript {
    name = "integrates-front-deploy";
    searchPaths = {
      bin = [
        inputs.nixpkgs.findutils
        inputs.nixpkgs.git
        inputs.nixpkgs.gnused
        inputs.nixpkgs.nodejs_20
        outputs."/common/utils/bugsnag/announce"
        outputs."/common/utils/bugsnag/source-map-uploader"
      ];
      source = [
        outputs."/common/utils/aws"
        outputs."/common/utils/sops"
        outputs."/common/utils/coralogix/source-map-uploader"
      ];
    };
    entrypoint = ./entrypoint.sh;
  };
}
