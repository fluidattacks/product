{ inputs, makeScript, ... }: {
  jobs."/integrates/front/test" = makeScript {
    entrypoint = ./entrypoint.sh;
    name = "integrates-front-test";
    searchPaths.bin = [ inputs.nixpkgs.bash inputs.nixpkgs.nodejs_22 ];
  };
}
