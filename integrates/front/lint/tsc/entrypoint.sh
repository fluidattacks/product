# shellcheck shell=bash

function main {
  : \
    && pushd integrates/front \
    && if [ ! -d node_modules ]; then
      info "Installing node modules"
      npm ci
    fi \
    && export PATH="${PWD}/node_modules/.bin:${PATH}" \
    && export NODE_PATH="${PWD}/node_modules:${NODE_PATH}" \
    && info "Running typescript compiler" \
    && tsc -p tsconfig.json \
    || return 1
}

main "$@"
