{ inputs, makeScript, outputs, ... }:
makeScript {
  name = "integrates-front-lint";
  searchPaths.bin = [
    inputs.nixpkgs.bash
    inputs.nixpkgs.nodejs_20
    outputs."/integrates/front/lint/eslint"
    outputs."/integrates/front/lint/stylelint"
    outputs."/integrates/front/lint/tsc"
  ];
  entrypoint = ./entrypoint.sh;
}
