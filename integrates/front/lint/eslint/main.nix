{ inputs, makeScript, ... }:
makeScript {
  entrypoint = ./entrypoint.sh;
  name = "integrates-front-lint-eslint";
  searchPaths = { bin = [ inputs.nixpkgs.nodejs_20 inputs.nixpkgs.bash ]; };
}
