# shellcheck shell=bash

function main {
  local eslint_args=(
    --config eslint.config.mjs
    --print-config
  )
  local target_path="${1:-}"
  if [ -z "${target_path}" ]; then
    error "First argument (target path) is required"
    return 1
  fi
  local STORAGE_PATH="eslint_rules.json"
  export ESLINT_USE_FLAT_CONFIG=true
  pushd integrates/front || exit
  if [ ! -d node_modules ]; then
    info "Installing node modules"
    npm ci
  fi
  export PATH="${PWD}/node_modules/.bin:${PATH}"
  export NODE_PATH="${PWD}/node_modules:${NODE_PATH}"
  eslint "${eslint_args[@]}" "${target_path}" > "${STORAGE_PATH}"
  info "Config printed to $(pwd)/${STORAGE_PATH} successfully!"
  popd || exit
}

main "$@"
