# shellcheck shell=bash

function main {
  local eslint_args=(
    --report-unused-disable-directives
    --config eslint.config.mjs
    .
  )
  if ! test -n "${CI-}"; then
    eslint_args+=(
      --fix
    )
  fi

  export NODE_OPTIONS=--max-old-space-size=4096
  : && export ESLINT_USE_FLAT_CONFIG=true \
    && pushd integrates/front \
    && if [ ! -d node_modules ]; then
      info "Installing node modules"
      npm ci
    fi \
    && export PATH="${PWD}/node_modules/.bin:${PATH}" \
    && export NODE_PATH="${PWD}/node_modules:${NODE_PATH}" \
    && eslint "${eslint_args[@]}" \
    && info 'eslint ok!' \
    && popd || return 1

}

main "$@"
