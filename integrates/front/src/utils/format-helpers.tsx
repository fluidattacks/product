import type { TTagVariant } from "@fluidattacks/design";
import {
  Container,
  SeverityBadge,
  SeverityOverview,
  Tag,
  Text,
} from "@fluidattacks/design";
import { Fragment, isValidElement } from "react";

import { getTagVariant } from "./colors";

import type { ISeverityBadgeProps } from "components/severity-badge";
import type { IPhoneAttr } from "features/verify-dialog/types";
import type { HookEventType } from "gql/graphql";
import { HOOK_EVENT_TO_LABEL } from "pages/integrations/webhooks/add-hook-modal/types";
import { translate } from "utils/translations/translate";

const castEventType = (field: string): string => {
  const eventType: Record<string, string> = {
    AUTHORIZATION_SPECIAL_ATTACK:
      "group.events.type.authorizationSpecialAttack",
    CLIENT_CANCELS_PROJECT_MILESTONE:
      "group.events.type.clientCancelsProjectMilestone",
    CLIENT_EXPLICITLY_SUSPENDS_PROJECT:
      "group.events.type.clientSuspendsProject",
    CLONING_ISSUES: "group.events.type.cloningIssues",
    CREDENTIAL_ISSUES: "group.events.type.credentialsIssues",
    DATA_UPDATE_REQUIRED: "group.events.type.dataUpdateRequired",
    ENVIRONMENT_ISSUES: "group.events.type.environmentIssues",
    INSTALLER_ISSUES: "group.events.type.installerIssues",
    MISSING_SUPPLIES: "group.events.type.missingSupplies",
    MOVED_TO_ANOTHER_GROUP: "group.events.type.movedToAnotherGroup",
    NETWORK_ACCESS_ISSUES: "group.events.type.networkAccessIssues",
    OTHER: "group.events.type.other",
    REMOTE_ACCESS_ISSUES: "group.events.type.remoteAccessIssues",
    TOE_DIFFERS_APPROVED: "group.events.type.toeDiffersApproved",
    VPN_ISSUES: "group.events.type.vpnIssues",
  };

  return eventType[field];
};

const castEventStatus = (
  field: string,
): {
  status: string;
  tagVariant: TTagVariant;
} => {
  const eventStatus: Record<
    string,
    {
      status: string;
      tagVariant: TTagVariant;
    }
  > = {
    CREATED: {
      status: "searchFindings.tabEvents.statusValues.unsolved",
      tagVariant: "error",
    },
    OPEN: {
      status: "searchFindings.tabEvents.statusValues.unsolved",
      tagVariant: "error",
    },
    SOLVED: {
      status: "searchFindings.tabEvents.statusValues.solve",
      tagVariant: "success",
    },
    VERIFICATION_REQUESTED: {
      status: "searchFindings.tabEvents.statusValues.pendingVerification",
      tagVariant: "warning",
    },
  };

  return eventStatus[field];
};

const formatDropdownField = (field: string): string => {
  const translationParameters: Record<string, string> = {
    ACCEPTED: "searchFindings.tabDescription.treatment.accepted",
    ACCEPTED_UNDEFINED:
      "searchFindings.tabDescription.treatment.acceptedUndefined",
    APPLICATIONS: "searchFindings.tabDescription.ambit.applications",
    CLOSED: "searchFindings.tabDescription.treatment.closed",
    DATABASES: "searchFindings.tabDescription.ambit.databases",
    INFRASTRUCTURE: "searchFindings.tabDescription.ambit.infra",
    IN_PROGRESS: "searchFindings.tabDescription.treatment.inProgress",
    NEW: "searchFindings.tabDescription.treatment.new",
    REJECTED: "searchFindings.tabDescription.treatment.rejected",
    SOURCE_CODE: "searchFindings.tabDescription.ambit.sourcecode",
    UNTREATED: "searchFindings.tabDescription.treatment.new",
  };

  return translationParameters[field];
};

const formatVulnerabilityState = (status: string): string => {
  return (
    {
      DELETED: "Deleted",
      MASKED: "Masked",
      REJECTED: translate.t("searchFindings.tabVuln.rejected"),
      SAFE: translate.t("searchFindings.tabVuln.safe"),
      SUBMITTED: translate.t("searchFindings.tabVuln.submitted"),
      VULNERABLE: translate.t("searchFindings.tabVuln.open"),
    }[status] ?? status
  );
};

const formatReattack = (reattack: string): string => {
  return (
    {
      NotRequested: translate.t("searchFindings.tabVuln.notRequested"),
      // eslint-disable-next-line camelcase
      On_hold: translate.t("searchFindings.tabVuln.onHold"),
      Requested: translate.t("searchFindings.tabVuln.requested"),
      Verified: translate.t("searchFindings.tabVuln.verified"),
    }[reattack] ?? reattack
  );
};

const formatTechnique = (technique: string): string => {
  return translate.t(
    `searchFindings.tabVuln.technique.${technique.toLowerCase()}`,
  );
};

const formatTreatment = (treatment: string): string => {
  return translate.t(formatDropdownField(treatment));
};

const formatPhone = (value: string): IPhoneAttr => {
  const [dialCode, ...phone] = value.split(" ");

  return {
    callingCountryCode: dialCode.replace("+", ""),
    // eslint-disable-next-line require-unicode-regexp
    nationalNumber: phone.join("").replaceAll(/[^0-9]/g, ""),
  };
};

const formatTreatmentColumn = (
  treatment: string,
  acceptanceStatus: string,
): string => {
  if (acceptanceStatus === "SUBMITTED") {
    return `${formatTreatment(treatment)} (Pending approval)`;
  }

  return formatTreatment(treatment);
};

const formatDuration = (value: number): string => {
  if (value < 0) {
    return "-";
  }
  const secondsInMiliseconds = 1000;
  const factor = 60;

  const seconds = Math.trunc(value / secondsInMiliseconds);
  const minutes = Math.trunc(seconds / factor);
  const ss = seconds % factor;
  const hh = Math.trunc(minutes / factor);
  const mm = minutes % factor;
  const hhStr = hh.toString().length < 2 ? `0${hh}` : hh.toString();
  const mmStr = mm.toString().length < 2 ? `0${mm}` : mm.toString();
  const ssStr = ss.toString().length < 2 ? `0${ss}` : ss.toString();

  return `${hhStr}:${mmStr}:${ssStr}`;
};

const formatPercentage = (value: number, decimal = false): string => {
  const oneHundred = 100;
  const minDecimal = 0.001;
  const [before, after = "00"] = (value * oneHundred).toString().split(".");

  if (decimal)
    return value < minDecimal && value > 0
      ? "0.001%"
      : `${before}.${after.slice(0, 2)}%`;

  const percentageValue = Math.floor(value * oneHundred);

  return `${percentageValue}%`;
};

const flattenObj = (input: object): object => {
  const traverseKeys = (
    prevKey: string,
    entries: [string, unknown][],
  ): [string, unknown][] => {
    const ent = entries.reduce(
      (
        current: [string, unknown][],
        [key, value]: [string, unknown],
      ): [string, unknown][] => {
        if (isValidElement(value) || key === "__typename")
          return current.concat([["", ""]]);
        if (typeof value === "object" && value) {
          const objEntries: [string, unknown][] = Object.entries(value);

          return current.concat(traverseKeys(key, objEntries));
        }
        const path = prevKey === "" ? "" : `${prevKey}.`;

        return current.concat([[path + key, value]]);
      },
      [],
    );

    return ent;
  };
  const entries: [string, unknown][] = Object.entries(input);
  const flatEntries = traverseKeys("", entries).filter(
    ([key, _value]: [string, unknown]): boolean => key !== "",
  );

  return Object.fromEntries(flatEntries);
};

const flattenData = (dataset: object[]): object[] => {
  const flatArray: object[] = dataset.map(flattenObj);

  return flatArray;
};

const includeTagsFormatter = ({
  dataPrivate = false,
  text,
  customTag,
  excludeTag = false,
  failedTag = false,
  newTag = false,
  reviewTag = false,
}: {
  dataPrivate?: boolean;
  text: JSX.Element | string | undefined;
  customTag?: JSX.Element;
  excludeTag?: boolean;
  newTag?: boolean;
  reviewTag?: boolean;
  failedTag?: boolean;
  decor?: string;
}): JSX.Element => {
  return (
    <Container
      {...(dataPrivate && { "data-private": true })}
      display={"inline-block"}
    >
      <Container alignItems={"center"} display={"flex"} gap={0.25}>
        {customTag}
        {text === undefined ? undefined : (
          <Text color={"#212a36"} display={"inline-block"} size={"sm"}>
            {text}
          </Text>
        )}
        {failedTag ? (
          <Tag
            priority={"low"}
            tagLabel={translate.t("formatters.includeTags.failed")}
            variant={"error"}
          />
        ) : undefined}
        {newTag ? (
          <Tag
            priority={"medium"}
            tagLabel={translate.t("formatters.includeTags.new")}
            variant={"error"}
          />
        ) : undefined}
        {reviewTag ? (
          <Tag
            priority={"medium"}
            tagLabel={translate.t("formatters.includeTags.review")}
            variant={"warning"}
          />
        ) : undefined}
        {excludeTag ? (
          <Tag
            icon={"ban"}
            priority={"low"}
            tagLabel={translate.t("formatters.includeTags.exclude")}
            variant={"warning"}
          />
        ) : undefined}
      </Container>
    </Container>
  );
};

const listTagsFormatter = (list: HookEventType[]): JSX.Element => {
  return (
    <Fragment>
      {list.map(
        (el): JSX.Element => (
          <Tag key={el} tagLabel={HOOK_EVENT_TO_LABEL[el]} />
        ),
      )}
    </Fragment>
  );
};

const getVariant = (severity: number): ISeverityBadgeProps["variant"] => {
  const midSeverity = 7;
  const maxSeverity = 9;
  if (severity === 0) {
    return "disable";
  }

  if (severity > 0 && severity < 4) {
    return "low";
  } else if (severity >= 4 && severity < midSeverity) {
    return "medium";
  } else if (severity >= midSeverity && severity < maxSeverity) {
    return "high";
  }

  return "critical";
};

const severityFormatter = (severity?: number): JSX.Element => {
  const score = severity === undefined ? "0.0" : severity.toFixed(1);
  const variant = getVariant(severity ?? 0);
  const capitalizedVariant =
    variant === "disable"
      ? "None"
      : variant.charAt(0).toUpperCase() + variant.slice(1);

  return (
    <SeverityBadge textL={score} textR={capitalizedVariant} variant={variant} />
  );
};

const severityOverviewFormatter = (
  critical: number,
  high: number,
  medium: number,
  low: number,
): JSX.Element => {
  return (
    <SeverityOverview
      critical={critical}
      high={high}
      low={low}
      medium={medium}
    />
  );
};

const tagsFormatter = (tags: unknown): JSX.Element => {
  if (typeof tags !== "string") return <div>{"-"}</div>;

  const tagsArray = [...new Set(tags.split(",").filter(Boolean))];

  return (
    <Container display={"flex"} gap={0.25}>
      {tagsArray.map(
        (tag): JSX.Element => (
          <Tag key={tag} tagLabel={tag} />
        ),
      )}
    </Container>
  );
};

const formatBoolean = (value: boolean): string =>
  value ? translate.t("yes") : translate.t("no");

const formatStatus = (vulnerable: boolean): string =>
  vulnerable ? translate.t("status.vulnerable") : translate.t("status.safe");

const jsxStatus = (status: string): JSX.Element => {
  const variant = getTagVariant(status);

  return (
    <Tag
      priority={variant === "inactive" ? "default" : "low"}
      tagLabel={status}
      variant={variant}
    />
  );
};

const onlineStatusBadge = (isOnline: boolean): JSX.Element =>
  isOnline ? (
    <Tag
      priority={"low"}
      tagLabel={translate.t("components.sideBar.help.chat.status.online")}
      variant={"success"}
    />
  ) : (
    <Tag
      tagLabel={translate.t("components.sideBar.help.chat.status.offline")}
      variant={"inactive"}
    />
  );

export {
  castEventType,
  castEventStatus,
  formatBoolean,
  flattenData,
  flattenObj,
  formatDropdownField,
  formatDuration,
  formatPercentage,
  formatPhone,
  formatStatus,
  formatReattack,
  formatTechnique,
  formatTreatment,
  formatTreatmentColumn,
  formatVulnerabilityState,
  jsxStatus,
  includeTagsFormatter,
  listTagsFormatter,
  onlineStatusBadge,
  severityFormatter,
  severityOverviewFormatter,
  tagsFormatter,
};
