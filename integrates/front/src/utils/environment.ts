import isUndefined from "lodash/isUndefined";

type TEnvironment = "development" | "ephemeral" | "production";

export const getEnvironment = (): TEnvironment => {
  if (isUndefined(window)) {
    return "development";
  }
  const currentUrl = window.location.hostname;
  const ephemeralDomainRegex = /[a-z]+atfluid.app.fluidattacks.com/gu;

  if (currentUrl === "localhost") {
    return "development";
  } else if (ephemeralDomainRegex.test(currentUrl)) {
    return "ephemeral";
  }

  return "production";
};
