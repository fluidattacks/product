import type { FetchResult } from "@apollo/client";

import type { SignPostUrlMutationMutation } from "gql/graphql";
import type { IAddFiles } from "pages/group/scope/files/types";
import { Logger } from "utils/logger";
import { msgError, msgWarning } from "utils/notifications";
import { translate } from "utils/translations/translate";

const badRequest = 400;
const forbidden = 403;
const internalServer = 500;

const logError = (
  event: ProgressEvent<XMLHttpRequestEventTarget>,
  groupName: string,
  request: XMLHttpRequest,
): void => {
  Logger.error("Error while uploading a file", {
    eventTime: event.timeStamp,
    eventType: event.type,
    groupName,
    response: request.response,
    status: request.status,
    statusText: request.statusText,
    totalLoaded: event.loaded,
    totalSize: event.total,
  });
};

const uploadGroupFileToS3 = async (
  data: FetchResult<SignPostUrlMutationMutation>,
  file: File,
  groupName: string,
  onError: VoidFunction,
  setProgress: React.Dispatch<number>,
): Promise<number> => {
  if (!data.data) return forbidden;

  const {
    signPostUrl: {
      url: { url, fields },
    },
  }: IAddFiles = data.data;

  const { algorithm, credential, date, key, policy, securitytoken, signature } =
    fields;

  const formData = new FormData();
  formData.append("acl", "private");
  formData.append("key", key);
  formData.append("X-Amz-Algorithm", algorithm);
  formData.append("X-Amz-Credential", credential);
  formData.append("X-Amz-Date", date);
  formData.append("policy", policy);
  formData.append("X-Amz-Signature", signature);
  formData.append("X-Amz-Security-Token", securitytoken);
  formData.append("file", file, file.name);

  const notifiedIfNonAuthorized = (
    request: XMLHttpRequest,
    event: ProgressEvent<XMLHttpRequestEventTarget>,
    errorMessage: string,
  ): boolean => {
    if (request.status >= badRequest && request.status < internalServer) {
      logError(event, groupName, request);
      msgError(errorMessage);
      onError();

      return false;
    }

    return true;
  };

  const updateUploadProgress = (
    event: ProgressEvent<XMLHttpRequestEventTarget>,
  ): void => {
    const oneHundred = 100;
    if (event.lengthComputable) {
      const percentComplete = Math.ceil(
        (event.loaded / event.total) * oneHundred,
      );
      setProgress(percentComplete);
    }
  };

  const retryOnError = async (remainedTrials: number): Promise<number> => {
    if (remainedTrials <= 0) {
      Logger.error("Error while uploading a file", {
        file: file.name,
        groupName,
      });
      msgError(translate.t("groupAlerts.errorTryAgain"));
      onError();

      return forbidden;
    }

    return new Promise((resolve): void => {
      const xhr = new XMLHttpRequest();

      xhr.upload.addEventListener("progress", updateUploadProgress);

      const handleErrorOrTimeOut = (
        event: ProgressEvent<XMLHttpRequestEventTarget>,
      ): void => {
        logError(event, groupName, xhr);
        const isAuthorized = notifiedIfNonAuthorized(
          xhr,
          event,
          translate.t("groupAlerts.accessDenied"),
        );

        if (isAuthorized && remainedTrials > 0) {
          msgWarning(translate.t("groupAlerts.retryingFileUpload"));
          setProgress(0);

          resolve(retryOnError(remainedTrials - 1));
        }

        resolve(xhr.status);
      };

      xhr.addEventListener("error", handleErrorOrTimeOut);
      xhr.addEventListener("timeout", handleErrorOrTimeOut);
      xhr.addEventListener("abort", (event): void => {
        notifiedIfNonAuthorized(
          xhr,
          event,
          translate.t("groupAlerts.fileUploadAborted"),
        );
        const resolveNumber = 499;
        resolve(resolveNumber);
      });

      xhr.addEventListener("load", (event): void => {
        notifiedIfNonAuthorized(
          xhr,
          event,
          translate.t("groupAlerts.accessDenied"),
        );

        resolve(xhr.status);
      });

      xhr.open("POST", url, true);
      xhr.send(formData);
    });
  };
  const retry = 3;

  return retryOnError(retry);
};

export { uploadGroupFileToS3 };
