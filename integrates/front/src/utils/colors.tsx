import type { ITagProps } from "@fluidattacks/design";

const statusGreenColor: string[] = [
  "Active",
  "Closed",
  "Confirmed",
  "Enabled",
  "Low",
  "Ok",
  "Registered",
  "Safe",
  "Secure",
  "Solved",
  "Submitted",
  "Success",
  "Verified (closed)",
];
const statusOrangeColor: string[] = [
  "Accepted",
  "Cloning",
  "Created",
  "Draft",
  "In progress",
  "Masked",
  "Medium",
  "New",
  "On_hold",
  "Pending",
  "Pending verification",
  "Partially closed",
  "Permanently accepted",
  "Requested",
  "Temporarily accepted",
  "Untreated",
];
const statusRedColor: string[] = [
  "Critical",
  "Disabled",
  "Failed",
  "High",
  "Inactive",
  "Open",
  "Rejected",
  "Unregistered",
  "Unsolved",
  "Verified (open)",
  "Vulnerable",
  "Vulnerable",
];

const getTagVariant = (value: string): ITagProps["variant"] => {
  if (statusGreenColor.includes(value)) return "success";
  if (statusOrangeColor.includes(value)) return "warning";
  if (statusRedColor.includes(value)) return "error";

  return "inactive";
};

export { getTagVariant };
