import dayjs from "dayjs";

type TDateComparisonResult = "equal" | "higher" | "invalid" | "lower";
/*
 * Compares two dates and returns a string indicating the result of
 * Comparing the first vs the second.
 */
const compareDates = (
  date1: Date | number | string,
  date2: Date | number | string,
): TDateComparisonResult => {
  if (!dayjs(date1).isValid() || !dayjs(date2).isValid()) return "invalid";

  const isAfter = dayjs(date1).isAfter(dayjs(date2));
  if (isAfter) return "higher";

  const isBefore = dayjs(date1).isBefore(dayjs(date2));
  if (isBefore) return "lower";

  return "equal";
};

export type { TDateComparisonResult };
export { compareDates };
