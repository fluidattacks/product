/* eslint-disable testing-library/no-node-access */
import { screen } from "@testing-library/react";
import { userEvent } from "@testing-library/user-event";

const getByValue = (container: HTMLElement, value: string): HTMLElement => {
  const element = container.querySelector(`[value="${value}"]`);

  expect(element).not.toBeNull();

  return element as HTMLElement;
};

const selectOption = async (
  inputName: string,
  optionValue: string,
  isInputSelect = true,
): Promise<void> => {
  const testIdSufix = isInputSelect
    ? "select-selected-option"
    : "selected-option";

  const testIdSufixDropdown = isInputSelect
    ? "select-dropdown-options"
    : "dropdown-options";
  const currentOption = screen.getByTestId(`${inputName}-${testIdSufix}`);
  // Open dropdown
  await userEvent.click(currentOption);
  const optionsContainer = screen.getByTestId(
    `${inputName}-${testIdSufixDropdown}`,
  );
  await userEvent.click(getByValue(optionsContainer, optionValue));
};

const expectOptionHaveValue = (name: string, value: string): void => {
  const currentOption = document.querySelector(`#${name}-select`)?.firstChild;

  expect(currentOption).not.toBeNull();
  expect(currentOption).toHaveAttribute("value", value);
};

const getInputByName = (name: string): HTMLElement =>
  screen.getByTestId(`${name}-input`);

const queryByInputName = (name: string): HTMLElement | null =>
  screen.queryByTestId(`${name}-input`);

export {
  expectOptionHaveValue,
  getByValue,
  getInputByName,
  queryByInputName,
  selectOption,
};
