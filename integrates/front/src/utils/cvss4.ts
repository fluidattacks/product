/* eslint-disable sort-keys -- Key order for metric values is altered for correct tooltip rendering*/
import _ from "lodash";

const CVSS4_CALCULATOR_BASE_URL = "https://www.first.org/cvss/calculator/4.0";
const CVSS_V4_DEFAULT =
  "CVSS:4.0/AV:N/AC:L/AT:N/PR:N/UI:N/VC:N/VI:N/VA:N/SC:N/SI:N/SA:N";

interface ICVSS4BaseMetrics {
  // Exploitability
  attackComplexity: string;
  attackRequirements: string;
  attackVector: string;
  userInteraction: string;
  privilegesRequired: string;
  // Impact
  availabilityVulnerableImpact: string;
  confidentialityVulnerableImpact: string;
  integrityVulnerableImpact: string;
  availabilitySubsequentImpact: string;
  confidentialitySubsequentImpact: string;
  integritySubsequentImpact: string;
}
interface ICVSS4ThreatMetrics extends ICVSS4BaseMetrics {
  exploitability: string;
}
interface ICVSS4EnvironmentalMetrics extends ICVSS4ThreatMetrics {
  // Environmental (Security Requirements)
  availabilityRequirement: string;
  confidentialityRequirement: string;
  integrityRequirement: string;
  /*
   * Environmental (Modified Base Metrics)
   * Exploitability
   */
  modifiedAttackComplexity: string;
  modifiedAttackVector: string;
  modifiedAttackRequirements: string;
  modifiedUserInteraction: string;
  modifiedPrivilegesRequired: string;
  // Impact
  modifiedAvailabilityVulnerableImpact: string;
  modifiedConfidentialityVulnerableImpact: string;
  modifiedIntegrityVulnerableImpact: string;
  modifiedAvailabilitySubsequentImpact: string;
  modifiedConfidentialitySubsequentImpact: string;
  modifiedIntegritySubsequentImpact: string;
}

/**
 * CVSS 4.0 base score metrics.
 * Values were taken from:
 * @see https://www.first.org/cvss/v4.0/specification-document
 */
const attackVectorValues: Record<string, string> = {
  N: "searchFindings.tabSeverity.attackVector.options.network.label",
  A: "searchFindings.tabSeverity.attackVector.options.adjacent.label",
  L: "searchFindings.tabSeverity.attackVector.options.local.label",
  P: "searchFindings.tabSeverity.attackVector.options.physical.label",
};

const attackComplexityValues: Record<string, string> = {
  L: "searchFindings.tabSeverity.attackComplexity.options.low.label",
  H: "searchFindings.tabSeverity.attackComplexity.options.high.label",
};

const privilegesRequiredValues: Record<string, string> = {
  N: "searchFindings.tabSeverity.privilegesRequired.options.none.label",
  L: "searchFindings.tabSeverity.privilegesRequired.options.low.label",
  H: "searchFindings.tabSeverity.privilegesRequired.options.high.label",
};

const attackRequirementsValues: Record<string, string> = {
  N: "searchFindings.tabSeverity.attackRequirements.options.none.label",
  P: "searchFindings.tabSeverity.attackRequirements.options.present.label",
};

const userInteractionValues: Record<string, string> = {
  N: "searchFindings.tabSeverity.userInteraction.options.none.label",
  P: "searchFindings.tabSeverity.userInteraction.options.passive.label",
  A: "searchFindings.tabSeverity.userInteraction.options.active.label",
};

// Impact Metrics

const confidentialitySubsequentImpactValues: Record<string, string> = {
  H: "searchFindings.tabSeverity.confidentialitySubsequentImpact.options.high.label",
  L: "searchFindings.tabSeverity.confidentialitySubsequentImpact.options.low.label",
  N: "searchFindings.tabSeverity.confidentialitySubsequentImpact.options.none.label",
};

const integritySubsequentImpactValues: Record<string, string> = {
  H: "searchFindings.tabSeverity.integritySubsequentImpact.options.high.label",
  L: "searchFindings.tabSeverity.integritySubsequentImpact.options.low.label",
  N: "searchFindings.tabSeverity.integritySubsequentImpact.options.none.label",
};

const availabilitySubsequentImpactValues: Record<string, string> = {
  H: "searchFindings.tabSeverity.availabilitySubsequentImpact.options.high.label",
  L: "searchFindings.tabSeverity.availabilitySubsequentImpact.options.low.label",
  N: "searchFindings.tabSeverity.availabilitySubsequentImpact.options.none.label",
};

const confidentialityVulnerableImpactValues: Record<string, string> = {
  H: "searchFindings.tabSeverity.confidentialityVulnerableImpact.options.high.label",
  L: "searchFindings.tabSeverity.confidentialityVulnerableImpact.options.low.label",
  N: "searchFindings.tabSeverity.confidentialityVulnerableImpact.options.none.label",
};

const integrityVulnerableImpactValues: Record<string, string> = {
  H: "searchFindings.tabSeverity.integrityVulnerableImpact.options.high.label",
  L: "searchFindings.tabSeverity.integrityVulnerableImpact.options.low.label",
  N: "searchFindings.tabSeverity.integrityVulnerableImpact.options.none.label",
};

const availabilityVulnerableImpactValues: Record<string, string> = {
  H: "searchFindings.tabSeverity.availabilityVulnerableImpact.options.high.label",
  L: "searchFindings.tabSeverity.availabilityVulnerableImpact.options.low.label",
  N: "searchFindings.tabSeverity.availabilityVulnerableImpact.options.none.label",
};

/**
 * CVSS 4.0 threat score metrics.
 */

const exploitabilityValues: Record<string, string> = {
  A: "searchFindings.tabSeverity.exploitability.options.attacked.label",
  P: "searchFindings.tabSeverity.exploitability.options.proofOfConcept.label",
  U: "searchFindings.tabSeverity.exploitability.options.unreported.label",
  X: "searchFindings.tabSeverity.exploitability.options.notDefined.label",
};

/**
 * CVSS 4.0 environmental score metrics.
 */

const confidentialityRequirementValues: Record<string, string> = {
  H: "searchFindings.tabSeverity.confidentialityRequirement.options.high.label",
  M: "searchFindings.tabSeverity.confidentialityRequirement.options.medium.label",
  L: "searchFindings.tabSeverity.confidentialityRequirement.options.low.label",
  X: "searchFindings.tabSeverity.confidentialityRequirement.options.notDefined.label",
};

const integrityRequirementValues: Record<string, string> = {
  H: "searchFindings.tabSeverity.integrityRequirement.options.high.label",
  M: "searchFindings.tabSeverity.integrityRequirement.options.medium.label",
  L: "searchFindings.tabSeverity.integrityRequirement.options.low.label",
  X: "searchFindings.tabSeverity.integrityRequirement.options.notDefined.label",
};

const availabilityRequirementValues: Record<string, string> = {
  H: "searchFindings.tabSeverity.availabilityRequirement.options.high.label",
  M: "searchFindings.tabSeverity.availabilityRequirement.options.medium.label",
  L: "searchFindings.tabSeverity.availabilityRequirement.options.low.label",
  X: "searchFindings.tabSeverity.availabilityRequirement.options.notDefined.label",
};

const getCVSS40Values = (vector: string): ICVSS4EnvironmentalMetrics => {
  const notDefined = `X`;
  const metrics = _.reduce(
    vector.split("/"),
    (previous, item): Record<string, string> => ({
      ...previous,
      [item.split(":")[0]]: item.split(":")[1],
    }),
    {},
  );
  const baseMetrics: ICVSS4BaseMetrics = {
    attackComplexity: _.get(metrics, ["AC"]),
    attackVector: _.get(metrics, ["AV"]),
    attackRequirements: _.get(metrics, ["AT"]),
    userInteraction: _.get(metrics, ["UI"]),
    privilegesRequired: _.get(metrics, ["PR"]),
    availabilityVulnerableImpact: _.get(metrics, ["VA"]),
    confidentialityVulnerableImpact: _.get(metrics, ["VC"]),
    integrityVulnerableImpact: _.get(metrics, ["VI"]),
    availabilitySubsequentImpact: _.get(metrics, ["SA"]),
    confidentialitySubsequentImpact: _.get(metrics, ["SC"]),
    integritySubsequentImpact: _.get(metrics, ["SI"]),
  };
  const temporalMetrics: ICVSS4ThreatMetrics = {
    ...baseMetrics,
    exploitability: _.get(metrics, ["E"], notDefined),
  };
  const environmentalMetrics: ICVSS4EnvironmentalMetrics = {
    ...temporalMetrics,
    // Security Requirements
    confidentialityRequirement: _.get(metrics, ["CR"], notDefined),
    integrityRequirement: _.get(metrics, ["IR"], notDefined),
    availabilityRequirement: _.get(metrics, ["AR"], notDefined),
    // Modified Base Metrics
    modifiedAttackVector: _.get(metrics, ["MAV"], notDefined),
    modifiedAttackComplexity: _.get(metrics, ["MAC"], notDefined),
    modifiedAttackRequirements: _.get(metrics, ["MAT"], notDefined),
    modifiedPrivilegesRequired: _.get(metrics, ["MPR"], notDefined),
    modifiedUserInteraction: _.get(metrics, ["MUI"], notDefined),
    // Modified Impact Metric
    modifiedConfidentialityVulnerableImpact: _.get(
      metrics,
      ["MVC"],
      notDefined,
    ),
    modifiedIntegrityVulnerableImpact: _.get(metrics, ["MVI"], notDefined),
    modifiedAvailabilityVulnerableImpact: _.get(metrics, ["MVA"], notDefined),
    modifiedConfidentialitySubsequentImpact: _.get(
      metrics,
      ["MSC"],
      notDefined,
    ),
    modifiedIntegritySubsequentImpact: _.get(metrics, ["MSI"], notDefined),
    modifiedAvailabilitySubsequentImpact: _.get(metrics, ["MSA"], notDefined),
  };

  return environmentalMetrics;
};

const getCVSS4VectorString = (values: ICVSS4EnvironmentalMetrics): string => {
  const cvssVersion = `CVSS:4.0`;
  const notDefined = `X`;

  return (
    /*
     * Base score metrics: mandatory
     * Exploitability score metrics
     */
    `${cvssVersion}/AV:${values.attackVector}` +
    `/AC:${values.attackComplexity}` +
    `/AT:${values.attackRequirements}` +
    `/PR:${values.privilegesRequired}` +
    `/UI:${values.userInteraction}` +
    // Impact score metrics
    `/VC:${values.confidentialityVulnerableImpact}` +
    `/VI:${values.integrityVulnerableImpact}` +
    `/VA:${values.availabilityVulnerableImpact}` +
    `/SC:${values.confidentialitySubsequentImpact}` +
    `/SI:${values.integritySubsequentImpact}` +
    `/SA:${values.availabilitySubsequentImpact}` +
    // Threat score metrics: optional
    `/E:${values.exploitability || notDefined}` +
    /*
     * Environmental score metrics: optional
     * Security Requirements
     */
    `/CR:${values.confidentialityRequirement || notDefined}` +
    `/IR:${values.integrityRequirement || notDefined}` +
    `/AR:${values.availabilityRequirement || notDefined}` +
    // Modified Base Metrics
    `/MAV:${values.modifiedAttackVector || notDefined}` +
    `/MAC:${values.modifiedAttackComplexity || notDefined}` +
    `/MAT:${values.modifiedAttackRequirements || notDefined}` +
    `/MPR:${values.modifiedPrivilegesRequired || notDefined}` +
    `/MUI:${values.modifiedUserInteraction || notDefined}` +
    // Modified Impact Metric
    `/MVC:${values.modifiedConfidentialityVulnerableImpact || notDefined}` +
    `/MVI:${values.modifiedIntegrityVulnerableImpact || notDefined}` +
    `/MVA:${values.modifiedAvailabilityVulnerableImpact || notDefined}` +
    `/MSC:${values.modifiedConfidentialitySubsequentImpact || notDefined}` +
    `/MSI:${values.modifiedIntegritySubsequentImpact || notDefined}` +
    `/MSA:${values.modifiedAvailabilitySubsequentImpact || notDefined}`
  );
};

export {
  attackComplexityValues,
  attackVectorValues,
  availabilityVulnerableImpactValues,
  availabilitySubsequentImpactValues,
  availabilityRequirementValues,
  confidentialityVulnerableImpactValues,
  confidentialitySubsequentImpactValues,
  getCVSS40Values,
  confidentialityRequirementValues,
  exploitabilityValues,
  integrityVulnerableImpactValues,
  integritySubsequentImpactValues,
  integrityRequirementValues,
  privilegesRequiredValues,
  userInteractionValues,
  attackRequirementsValues,
  CVSS_V4_DEFAULT,
  getCVSS4VectorString,
  CVSS4_CALCULATOR_BASE_URL,
};

export type {
  ICVSS4BaseMetrics,
  ICVSS4EnvironmentalMetrics,
  ICVSS4ThreatMetrics,
};
