interface IOrganizationGroups {
  __typename: "Organization";
  groups: IGroups[];
  name: string;
}

interface IGroups {
  __typename: "Group";
  name: string;
  permissions: string[];
  serviceAttributes: string[];
}

interface IGetUserOrganizationsGroups {
  me: {
    __typename: "Me";
    organizations: IOrganizationGroups[];
    userEmail: string;
  };
}

export type { IGetUserOrganizationsGroups, IGroups, IOrganizationGroups };
