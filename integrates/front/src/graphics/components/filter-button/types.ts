import type { IDocumentValues } from "graphics/ctx";

interface ITimeFilterButton {
  shouldDisplayAll: boolean | undefined;
  subjectName: string;
  subject: string;
  timeFilter: boolean;
  changeToThirtyDays: () => void;
  changeToOneHundredEighty: (() => void) | undefined;
  changeToSixtyDays: (() => void) | undefined;
  changeToNinety: () => void;
  changeToAll: () => void;
}

interface ITypeFilterButton {
  documentName: string;
  currentDocumentName: string;
  documentNameFilter: boolean;
  changeToAlternative: (index: number) => void;
  changeToDefault: () => void;
}

interface IGButton {
  alternative: IDocumentValues;
  changeToAlternative: (index: number) => void;
  currentDocumentName: string;
  index: number;
}

export type { ITimeFilterButton, ITypeFilterButton, IGButton };
