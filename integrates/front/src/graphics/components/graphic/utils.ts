import includes from "lodash/includes";

import { allowedDocumentNames, allowedDocumentTypes } from "../../ctx";
import type { IComponentSizeProps, IReadonlyGraphicProps } from "../types";

function hasIFrameError(
  iFrameRef: React.MutableRefObject<HTMLIFrameElement | null>,
): boolean {
  return Boolean(
    iFrameRef.current?.contentDocument?.title.toLowerCase().includes("error"),
  );
}

const pixelsSensitivity = 5;

function buildUrl(
  props: IReadonlyGraphicProps,
  size: IComponentSizeProps,
  subjectName: string,
  documentName: string,
): string {
  const roundedHeight: number =
    pixelsSensitivity * Math.floor(size.height / pixelsSensitivity);
  const roundedWidth: number =
    pixelsSensitivity * Math.floor(size.width / pixelsSensitivity);

  const url: URL = new URL("/graphic", window.location.origin);
  url.searchParams.set("documentName", documentName);
  url.searchParams.set("documentType", props.documentType);
  url.searchParams.set("entity", props.entity);
  url.searchParams.set("generatorName", props.generatorName);
  url.searchParams.set("generatorType", props.generatorType);
  url.searchParams.set("height", roundedHeight.toString());
  url.searchParams.set("subject", subjectName);
  url.searchParams.set("width", roundedWidth.toString());

  return roundedWidth.toString() === "0" && roundedHeight.toString() === "0"
    ? ""
    : url.toString();
}

function buildCsvUrl(
  props: IReadonlyGraphicProps,
  subjectName: string,
  documentName: string,
): string {
  const url: URL = new URL("/graphic-csv", window.location.origin);
  url.searchParams.set("documentName", documentName);
  url.searchParams.set("documentType", props.documentType);
  url.searchParams.set("entity", props.entity);
  url.searchParams.set("generatorName", props.generatorName);
  url.searchParams.set("generatorType", props.generatorType);
  url.searchParams.set("subject", subjectName);

  return url.toString();
}

function isDocumentAllowed(name: string, type: string): boolean {
  return (
    includes(allowedDocumentNames, name) && includes(allowedDocumentTypes, type)
  );
}

export { hasIFrameError, buildUrl, buildCsvUrl, isDocumentAllowed };
