import { IconButton, Link } from "@fluidattacks/design";
import _ from "lodash";
import { useCallback, useMemo } from "react";
import { useTranslation } from "react-i18next";

import type { IDocumentValues } from "../../ctx";
import { mergedDocuments } from "../../ctx";

const ChartInfo = ({
  infoLink,
  currentDocumentName,
  documentName,
  documentType,
  isDocumentMerged,
}: Readonly<{
  infoLink: string | undefined;
  currentDocumentName: string;
  documentName: string;
  documentType: string;
  isDocumentMerged: (name: string, type: string) => boolean;
}>): JSX.Element | undefined => {
  const { t } = useTranslation();

  const getUrl = useCallback(
    (alternatives: IDocumentValues[]): string => {
      return alternatives.reduce(
        (url: string, alternative: IDocumentValues): string =>
          alternative.documentName === currentDocumentName
            ? alternative.url
            : url,
        "",
      );
    },
    [currentDocumentName],
  );

  const getAdditionalInfoLink = useCallback(
    (name: string, type: string): string => {
      if (isDocumentMerged(name, type)) {
        return mergedDocuments[name].default.documentName ===
          currentDocumentName
          ? mergedDocuments[name].default.url
          : getUrl(mergedDocuments[name].alt);
      }

      return "";
    },
    [currentDocumentName, getUrl, isDocumentMerged],
  );

  const shouldDisplayUrl: boolean = useMemo(
    (): boolean =>
      isDocumentMerged(documentName, documentType)
        ? !_.isEmpty(getAdditionalInfoLink(documentName, documentType))
        : true,
    [documentName, documentType, getAdditionalInfoLink, isDocumentMerged],
  );

  if (_.isUndefined(infoLink) && !shouldDisplayUrl) return undefined;

  return (
    <Link
      href={infoLink + getAdditionalInfoLink(documentName, documentType)}
      iconPosition={"hidden"}
    >
      <IconButton
        icon={"info-circle"}
        iconSize={"xxs"}
        iconType={"fa-light"}
        id={"information-button"}
        tooltip={t("analytics.buttonToolbar.information.tooltip")}
        tooltipPlace={"top"}
        variant={"ghost"}
      />
    </Link>
  );
};

export { ChartInfo };
