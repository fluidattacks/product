interface IComponentSizeProps {
  height: number;
  width: number;
}

interface IReadonlyGraphicProps {
  documentName: string;
  documentType: string;
  entity: string;
  generatorName: string;
  generatorType: string;
  subject: string;
}

type TGraphicState = "error" | "loading" | "ready";

export type { IComponentSizeProps, IReadonlyGraphicProps, TGraphicState };
