import { createElement } from "react";
import { createRoot } from "react-dom/client";
import { BrowserRouter, Route, Routes } from "react-router-dom";

import { CustomThemeProvider } from "components/colors";
import { GroupAnalytics } from "pages/group/analytics";
import { GlobalStyle } from "styles";
import { ApolloProvider } from "utils/apollo";
import { secureStore, secureStoreContext } from "utils/secure-store";
// eslint-disable-next-line import/no-unresolved
import "vite/modulepreload-polyfill";

const App = (): JSX.Element => (
  <CustomThemeProvider>
    <GlobalStyle />
    <BrowserRouter basename={"/graphics-for-group"}>
      <ApolloProvider>
        <secureStoreContext.Provider value={secureStore}>
          <Routes>
            <Route element={<GroupAnalytics />} path={"/"} />
          </Routes>
        </secureStoreContext.Provider>
      </ApolloProvider>
    </BrowserRouter>
  </CustomThemeProvider>
);

const rootElement = document.getElementById("root");
if (rootElement) {
  createRoot(rootElement).render(createElement(App));
}
