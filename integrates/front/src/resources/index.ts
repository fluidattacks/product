// Images resources are loaded from the cloud
import lotCircleXMark from "./circleXMark.json";
import radar from "./radar.json";
import redLoader from "./redLoader.json";
import whiteLoader from "./whiteLoader.json";

export { lotCircleXMark, radar, redLoader, whiteLoader };
