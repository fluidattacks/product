/* eslint-disable functional/immutable-data */
import isUndefined from "lodash/isUndefined";
import type { FC } from "react";
import { useCallback, useContext, useRef, useState } from "react";
import { useParams } from "react-router-dom";

import { OauthErrorsPopUp } from "./oauth-errors-pop-up";
import type { IOauthError } from "./oauth-errors-pop-up/types";
import { OauthForm } from "./oauth-form";
import type { IIntegrationRepository, IOauthRootFormProps } from "./types";
import { useRootSubmit, validationSchema } from "./utils";

import { Form } from "components/form";
import { authzGroupContext } from "context/authz/config";

const AddOauthRootForm: FC<IOauthRootFormProps> = ({
  closeModal,
  initialValues = {
    allChecked: false,
    branches: [],
    cloningStatus: {
      message: "",
      status: "UNKNOWN",
    },
    credentials: {
      id: "",
      isPat: false,
      isToken: false,
      name: "",
      oauthType: "",
      type: "OAUTH",
    },
    environmentUrls: [],
    environments: [],
    gitEnvironmentUrls: [],
    gitignore: [{ paths: [] }],
    hasExclusions: "",
    healthCheckConfirm: [],
    id: "",
    includesHealthCheck: "",
    reposByName: "",
    secrets: [],
    state: "ACTIVE",
    urls: [],
  },
  trialGroupName,
  trialOrgId,
  onUpdate,
  provider,
  setIsCredentialSelected,
  setProgress,
}): JSX.Element => {
  const { groupName } = useParams() as { groupName: string };
  const attributes = useContext(authzGroupContext);
  const hasAdvanced = attributes.can("has_advanced");

  const [repos, setRepos] = useState<Record<string, IIntegrationRepository>>(
    {},
  );
  const oauthErrors = useRef<IOauthError[]>([]);
  const [showErrors, setShowErrors] = useState<boolean>(false);

  const fillErrors = useCallback((error: IOauthError): void => {
    const newErrors = [...oauthErrors.current, error];
    oauthErrors.current = newErrors;
  }, []);

  const confirmErrors = useCallback((): void => {
    oauthErrors.current = [];
    setShowErrors(false);
    closeModal?.();
    onUpdate?.();
  }, [closeModal, onUpdate]);

  const finalActions = useCallback((): void => {
    if (oauthErrors.current.length > 0) {
      setShowErrors(true);
    } else {
      closeModal?.();
      onUpdate?.();
    }
  }, [closeModal, oauthErrors, onUpdate]);

  const handleSubmit = useRootSubmit(
    isUndefined(provider) ? (trialGroupName ?? groupName) : groupName,
    repos,
    fillErrors,
    finalActions,
  );

  if (showErrors) {
    return (
      <OauthErrorsPopUp
        confirmClick={confirmErrors}
        errors={oauthErrors.current}
      />
    );
  }

  return (
    <Form
      initialValues={initialValues}
      name={"gitAddOauthRoot"}
      onSubmit={handleSubmit}
      validationSchema={validationSchema(hasAdvanced, provider)}
    >
      <OauthForm
        handleClose={closeModal}
        provider={provider}
        setIsCredentialSelected={setIsCredentialSelected}
        setProgress={setProgress}
        setRepos={setRepos}
        trialOrgId={trialOrgId}
      />
    </Form>
  );
};

export { AddOauthRootForm };
