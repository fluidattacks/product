/* eslint-disable functional/immutable-data */
import { Container, Heading, Icon, Text } from "@fluidattacks/design";
import type { FieldArrayRenderProps } from "formik";
import { FieldArray, useFormikContext } from "formik";
import type { FC } from "react";
import { useCallback } from "react";
import * as React from "react";
import { useTranslation } from "react-i18next";

import {
  StyledAlert,
  StyledCheckbox,
  StyledIconBox,
  StyledInputsWrapper,
  StyledRadioGroup,
  StyledRepoContainer,
} from "./styles";
import type { IExclusionsProps } from "./types";

import type { IFormValues } from "../../types";
import { InputArray } from "components/input";
import { RadioButton } from "components/radio-button";
import { Have } from "context/authz/have";
import { openUrl } from "utils/resource-helpers";

const ExclusionsSection: FC<IExclusionsProps> = ({
  exclusionsArrayHelpersRef,
  repositories,
}): JSX.Element => {
  const { t } = useTranslation();
  const { isSubmitting, setFieldValue, values } =
    useFormikContext<IFormValues>();

  const onChangeHealthCheck = useCallback(
    (event: React.MouseEvent<HTMLInputElement>): void => {
      const hasHealthCheck = (event.target as HTMLInputElement).value;

      void setFieldValue("includesHealthCheck", String(hasHealthCheck));
      void setFieldValue("healthCheckConfirm", []);
    },
    [setFieldValue],
  );

  const goToDocumentation = useCallback((): void => {
    openUrl(
      "https://mirrors.edge.kernel.org/pub/software/scm/git/docs/gitignore.html#_pattern_format",
    );
  }, []);

  const onChangeHasExclusions = useCallback(
    (event: React.MouseEvent<HTMLInputElement>): void => {
      const hasExclusions = (event.target as HTMLInputElement).value;

      void setFieldValue("hasExclusions", String(hasExclusions));
      void setFieldValue("gitignore", []);
      values.urls.map((_): void => {
        return exclusionsArrayHelpersRef.current?.push({
          paths: hasExclusions === "yes" ? [""] : [],
        });
      });
    },
    [exclusionsArrayHelpersRef, setFieldValue, values.urls],
  );

  const handleRender = useCallback(
    (arrayHelpers: FieldArrayRenderProps): JSX.Element => {
      exclusionsArrayHelpersRef.current = arrayHelpers;

      return (
        <Container scroll={"none"}>
          {values.hasExclusions === "yes" ? (
            <React.Fragment>
              <Text fontWeight={"bold"} mt={1} size={"sm"}>
                {t("components.oauthRootForm.steps.s3.exclusions.exclude")}
                <StyledIconBox>
                  <Icon
                    clickable={true}
                    icon={"circle-info"}
                    iconSize={"xs"}
                    iconType={"fa-light"}
                    ml={0.25}
                    onClick={goToDocumentation}
                  />
                </StyledIconBox>
              </Text>
              <StyledAlert>
                {t("components.oauthRootForm.steps.s3.exclusions.warning")}
              </StyledAlert>
              {values.urls.map((url, urlIndex): JSX.Element => {
                return (
                  <StyledInputsWrapper key={url}>
                    <StyledRepoContainer>
                      <Text
                        fontWeight={"bold"}
                        size={"xs"}
                        whiteSpace={"nowrap"}
                      >
                        {`${repositories[url].name}/`}
                        <br />
                        {`${values.branches[urlIndex]}/`}
                      </Text>
                    </StyledRepoContainer>
                    <Container>
                      <InputArray name={`gitignore.${urlIndex}.paths`} />
                    </Container>
                  </StyledInputsWrapper>
                );
              })}
            </React.Fragment>
          ) : undefined}
        </Container>
      );
    },
    [
      exclusionsArrayHelpersRef,
      goToDocumentation,
      repositories,
      values.branches,
      values.urls,
      t,
      values.hasExclusions,
    ],
  );

  return (
    <Container
      bgColor={"#ffffff"}
      border={"1px solid #F4F4F6"}
      borderRadius={"4px"}
      mt={0.625}
      px={1.5}
      py={1.5}
      scroll={"none"}
    >
      <Heading fontWeight={"bold"} size={"xs"}>
        {t("components.oauthRootForm.steps.s3.exclusions.label")}
      </Heading>
      <StyledRadioGroup>
        <RadioButton
          defaultChecked={values.hasExclusions === "yes"}
          disabled={isSubmitting}
          label={t(
            "components.oauthRootForm.steps.s3.exclusions.radio.yes.label",
          )}
          name={"hasExclusions"}
          onClick={onChangeHasExclusions}
          required={true}
          value={t(
            "components.oauthRootForm.steps.s3.exclusions.radio.yes.value",
          )}
          variant={"formikField"}
        />
        <RadioButton
          defaultChecked={values.hasExclusions === "no"}
          disabled={isSubmitting}
          label={t(
            "components.oauthRootForm.steps.s3.exclusions.radio.no.label",
          )}
          name={"hasExclusions"}
          onClick={onChangeHasExclusions}
          required={true}
          value={t(
            "components.oauthRootForm.steps.s3.exclusions.radio.no.value",
          )}
          variant={"formikField"}
        />
      </StyledRadioGroup>
      <FieldArray name={"gitignore"} render={handleRender} />

      <Container my={2} />

      <Have I={"has_advanced"}>
        <Heading fontWeight={"bold"} size={"xs"}>
          {t("components.oauthRootForm.steps.s3.healthCheck.confirm")}
        </Heading>
        <StyledRadioGroup>
          <RadioButton
            defaultChecked={values.includesHealthCheck === "yes"}
            disabled={isSubmitting}
            label={t(
              "components.oauthRootForm.steps.s3.exclusions.radio.yes.label",
            )}
            name={"includesHealthCheck"}
            onClick={onChangeHealthCheck}
            required={true}
            value={t(
              "components.oauthRootForm.steps.s3.exclusions.radio.yes.value",
            )}
            variant={"formikField"}
          />
          <RadioButton
            defaultChecked={values.hasExclusions === "no"}
            disabled={isSubmitting}
            label={t(
              "components.oauthRootForm.steps.s3.exclusions.radio.no.label",
            )}
            name={"includesHealthCheck"}
            onClick={onChangeHealthCheck}
            required={true}
            value={t(
              "components.oauthRootForm.steps.s3.exclusions.radio.no.value",
            )}
            variant={"formikField"}
          />
        </StyledRadioGroup>
        {values.includesHealthCheck === "yes" ? (
          <StyledAlert>
            <StyledCheckbox
              disabled={isSubmitting}
              label={t("components.oauthRootForm.steps.s3.healthCheck.accept")}
              name={"healthCheckConfirm"}
              required={true}
              value={"includeA"}
              variant={"formikField"}
            />
          </StyledAlert>
        ) : undefined}
        {values.includesHealthCheck === "no" ? (
          <StyledAlert>
            <StyledCheckbox
              disabled={isSubmitting}
              label={t("components.oauthRootForm.steps.s3.healthCheck.rejectA")}
              name={"healthCheckConfirm"}
              required={true}
              value={"rejectA"}
              variant={"formikField"}
            />
            <StyledCheckbox
              disabled={isSubmitting}
              label={t("components.oauthRootForm.steps.s3.healthCheck.rejectB")}
              name={"healthCheckConfirm"}
              required={true}
              value={"rejectB"}
              variant={"formikField"}
            />
            <StyledCheckbox
              disabled={isSubmitting}
              label={t("components.oauthRootForm.steps.s3.healthCheck.rejectC")}
              name={"healthCheckConfirm"}
              required={true}
              value={"rejectC"}
              variant={"formikField"}
            />
          </StyledAlert>
        ) : undefined}
      </Have>
    </Container>
  );
};

export { ExclusionsSection };
