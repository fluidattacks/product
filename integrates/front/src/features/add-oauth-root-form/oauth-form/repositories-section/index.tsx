import { Container, Icon } from "@fluidattacks/design";
import { useFormikContext } from "formik";
import type { FC } from "react";
import { useCallback, useState } from "react";
import * as React from "react";
import { useTranslation } from "react-i18next";

import {
  StyledBox,
  StyledGrid,
  StyledSearchBox,
  StyledSearchInput,
  StyledTitle,
} from "./styles";
import type { IRepositoriesSectionProps } from "./types";

import type { IFormValues, IIntegrationRepository } from "../../types";
import { Checkbox } from "components/checkbox";

const RepositoriesSection: FC<IRepositoriesSectionProps> = ({
  branchesArrayHelpersRef,
  credentialName,
  envsArrayHelpersRef,
  exclusionsArrayHelpersRef,
  repositories,
}): JSX.Element => {
  const { t } = useTranslation();
  const { setFieldValue, values } = useFormikContext<IFormValues>();

  const cleanEnvironments = useCallback(
    (event: React.ChangeEvent<HTMLInputElement>): void => {
      const repoToRemove = values.urls.findIndex(
        (url): boolean => event.target.value === url,
      );
      if (repoToRemove >= 0) {
        envsArrayHelpersRef.current?.remove(repoToRemove);
        branchesArrayHelpersRef.current?.remove(repoToRemove);
        exclusionsArrayHelpersRef.current?.remove(repoToRemove);
      } else {
        exclusionsArrayHelpersRef.current?.push({ paths: [""] });
        envsArrayHelpersRef.current?.push("");
      }

      if (values.urls.length === 0) {
        void setFieldValue("hasExclusions", "");
        void setFieldValue("includesHealthCheck", "");
      }
    },
    [
      branchesArrayHelpersRef,
      envsArrayHelpersRef,
      exclusionsArrayHelpersRef,
      setFieldValue,
      values,
    ],
  );

  const [filteredRepos, setFilteredRepos] = useState<IIntegrationRepository[]>(
    Object.values(repositories),
  );

  const handleFilter = useCallback(
    (search: string): void => {
      setFilteredRepos(
        Object.values(repositories).filter((repo): boolean => {
          return (
            repo.name === "" ||
            repo.name.toLowerCase().includes(search.toLowerCase())
          );
        }),
      );
    },
    [repositories],
  );

  const onSearchChange = useCallback(
    (event: React.ChangeEvent<HTMLInputElement>): void => {
      const searchValue = event.target.value;
      handleFilter(searchValue);
    },
    [handleFilter],
  );

  const selectAll = useCallback(
    (event: React.ChangeEvent<HTMLInputElement>): void => {
      if (event.target.checked) {
        void setFieldValue("allChecked", true);
        filteredRepos.forEach((repository, index): void => {
          void setFieldValue(`urls.${index}`, repository.url);
        });
      } else {
        void setFieldValue("allChecked", false);
        void setFieldValue("urls", []);
        void setFieldValue("environments", []);
        void setFieldValue("branches", []);
        void setFieldValue("gitignore", [{ paths: [] }]);
      }
    },
    [filteredRepos, setFieldValue],
  );

  return (
    <React.Fragment>
      <StyledSearchBox>
        <Icon icon={"magnifying-glass"} iconSize={"sm"} />
        <StyledSearchInput
          aria-label={"reposByName"}
          name={"reposByName"}
          onChange={onSearchChange}
          placeholder={t(
            "components.oauthRootForm.steps.s1.filter.placeHolder",
          )}
        />
      </StyledSearchBox>
      <StyledBox>
        <StyledTitle>
          <Checkbox
            defaultChecked={values.urls.length === filteredRepos.length}
            label={credentialName}
            name={"allChecked"}
            onChange={selectAll}
          />
        </StyledTitle>
        <StyledGrid>
          {filteredRepos.map(
            (repos): JSX.Element => (
              <Container key={repos.name} minHeight={"24px"} scroll={"none"}>
                <Checkbox
                  label={`/${repos.name}`}
                  name={"urls"}
                  onChange={cleanEnvironments}
                  value={repos.url}
                  variant={"formikField"}
                />
              </Container>
            ),
          )}
        </StyledGrid>
      </StyledBox>
    </React.Fragment>
  );
};

export { RepositoriesSection };
