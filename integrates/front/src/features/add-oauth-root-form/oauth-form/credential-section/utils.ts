import type { ICredentialsAttr } from "../../types";

const getCredentials = (
  credentials: ICredentialsAttr[],
  provider: string,
): ICredentialsAttr[] => {
  return credentials.filter(
    (credential): boolean =>
      credential.type === "OAUTH" && credential.oauthType === provider,
  );
};

export { getCredentials };
