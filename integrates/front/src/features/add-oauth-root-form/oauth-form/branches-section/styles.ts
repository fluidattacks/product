import { styled } from "styled-components";

const StyledGrid = styled.div`
  display: grid;
  grid-template-columns: 60% 40%;
  gap: 8px;
  justify-content: space-between;
  align-items: center;
`;

const StyledSelectorWrapper = styled.div`
  & ul li {
    min-width: 0 !important;
  }

  & ul li p {
    overflow: hidden;
  }
`;

export { StyledGrid, StyledSelectorWrapper };
