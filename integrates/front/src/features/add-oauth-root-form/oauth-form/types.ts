import type { IIntegrationRepository, TProvider } from "../types";

interface IOauthFormProps {
  handleClose?: () => void;
  trialOrgId?: string;
  provider?: TProvider;
  setIsCredentialSelected?: React.Dispatch<React.SetStateAction<boolean>>;
  setProgress?: React.Dispatch<React.SetStateAction<number>>;
  setRepos: React.Dispatch<
    React.SetStateAction<Record<string, IIntegrationRepository>>
  >;
}

export type { IOauthFormProps };
