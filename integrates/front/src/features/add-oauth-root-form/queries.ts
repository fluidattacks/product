import { graphql } from "gql";

const GET_ORGANIZATION_CREDENTIALS = graphql(`
  query GetOrganizationCredentials($organizationId: String!) {
    organization(organizationId: $organizationId) {
      __typename
      name
      credentials {
        id
        __typename
        azureOrganization
        isPat
        isToken
        name
        oauthType
        owner
        type
      }
    }
  }
`);

const GET_INTEGRATION_REPOSITORIES = graphql(`
  query GetIntegrationRepositories($credId: String!, $organizationId: String!) {
    organization(organizationId: $organizationId) {
      __typename
      name
      credential(id: $credId) {
        integrationRepositories {
          __typename
          branches
          name
          url
        }
      }
    }
  }
`);

const ADD_GIT_ROOT = graphql(`
  mutation AddGitRootOauth(
    $branch: String!
    $credentials: RootCredentialsInput
    $gitignore: [String!]
    $groupName: String!
    $includesHealthCheck: Boolean!
    $nickname: String!
    $url: String!
    $useEgress: Boolean!
    $useVpn: Boolean!
    $useZtna: Boolean!
  ) {
    addGitRoot(
      branch: $branch
      credentials: $credentials
      gitignore: $gitignore
      groupName: $groupName
      includesHealthCheck: $includesHealthCheck
      nickname: $nickname
      url: $url
      useEgress: $useEgress
      useVpn: $useVpn
      useZtna: $useZtna
    ) {
      success
    }
  }
`);

export {
  ADD_GIT_ROOT,
  GET_INTEGRATION_REPOSITORIES,
  GET_ORGANIZATION_CREDENTIALS,
};
