import { useEffect, useState } from "react";
import * as React from "react";
import { useTranslation } from "react-i18next";

import { Filters } from "components/filter";
import type { IFilter } from "components/filter/types";
import { useDebouncedCallback } from "hooks/use-debounced-callback";
import type { IGitRootData } from "pages/group/scope/types";
import { getFiltersToSearch } from "utils/helpers";
import { setFiltersUtil } from "utils/set-filters";

const GitRootsFilters = ({
  roots,
  setFilteredRoots,
  setFiltersToSearch,
}: Readonly<{
  roots: IGitRootData[];
  setFilteredRoots: React.Dispatch<React.SetStateAction<IGitRootData[]>>;
  setFiltersToSearch: React.Dispatch<Record<string, unknown>>;
}>): JSX.Element => {
  const { t } = useTranslation();

  const [filters, setFilters] = useState<IFilter<IGitRootData>[]>([
    {
      id: "nickname",
      key: "nickname",
      label: t("group.scope.git.repo.nickname"),
      type: "text",
    },
    {
      id: "branch",
      key: "branch",
      label: t("group.scope.git.repo.branch.header"),
      type: "text",
    },
    {
      filterFn: "caseInsensitive",
      id: "state",
      key: "state",
      label: t("group.scope.common.state"),
      selectOptions: [
        { header: "Active", value: "ACTIVE" },
        { header: "Inactive", value: "INACTIVE" },
      ],
      type: "select",
    },
    {
      filterFn: "caseInsensitive",
      id: "cloningStatus",
      key: (arg0, value): boolean => {
        if (value === "") return true;

        return value?.includes(arg0.cloningStatus.status) ?? true;
      },
      label: t("group.scope.git.repo.cloning.status"),
      selectOptions: [
        { header: "Cloning", value: "CLONING" },
        { header: "Failed", value: "FAILED" },
        { header: "Cloned", value: "OK" },
        { header: "Queued", value: "QUEUED" },
        { header: "Unknown", value: "UNKNOWN" },
      ],
      type: "select",
    },
    {
      id: "includesHealthCheck",
      key: "includesHealthCheck",
      label: t("group.scope.git.healthCheck.tableHeader"),
      selectOptions: [
        { header: t("group.scope.git.healthCheck.yes"), value: "true" },
        { header: t("group.scope.git.healthCheck.no"), value: "false" },
      ],
      type: "select",
    },
  ]);
  const DEBOUNCE_FILTER_DELAY_MS = 800;
  const handleSetFiltersToSearch = useDebouncedCallback(
    setFiltersToSearch,
    DEBOUNCE_FILTER_DELAY_MS,
  );
  useEffect((): void => {
    setFilteredRoots(setFiltersUtil(roots, filters));
    handleSetFiltersToSearch(
      getFiltersToSearch(filters, {
        branch: "branch",
        cloningStatus: "cloningStatus",
        includesHealthCheck: "includesHealthCheck",
        nickname: "nickname",
        state: "state",
      }),
    );
  }, [
    filters,
    roots,
    setFiltersToSearch,
    setFilteredRoots,
    handleSetFiltersToSearch,
  ]);

  return <Filters filters={filters} setFilters={setFilters} />;
};

export { GitRootsFilters };
