import { SeverityBadge } from "@fluidattacks/design";
import capitalize from "lodash/capitalize";
import * as React from "react";

import type { TSeverityBadgeVariant } from "components/severity-badge";

interface IPriority {
  readonly priority: string;
}

const Status: React.FC<IPriority> = ({ priority }: IPriority): JSX.Element => {
  const lowerPriority = priority.toLowerCase();
  const severityVariant =
    lowerPriority === "none"
      ? "disable"
      : (lowerPriority as TSeverityBadgeVariant);
  const formatedStatus: string = capitalize(priority);

  return <SeverityBadge textR={formatedStatus} variant={severityVariant} />;
};

const priorityFormatter = (value: string): JSX.Element => {
  return <Status priority={value} />;
};

export type { IPriority };
export { priorityFormatter, Status };
