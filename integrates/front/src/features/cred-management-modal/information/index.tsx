import { Link } from "@fluidattacks/design";
import { useMemo } from "react";
import { useTranslation } from "react-i18next";

import type { IInformationProps } from "../types";

const Information = ({ data }: Readonly<IInformationProps>): JSX.Element => {
  const { t } = useTranslation();

  const text = useMemo((): string => {
    if (data.isUsed)
      return t("organization.tabs.users.removeButton.credentialsUsedBy", {
        groupName: data.groupName,
      });

    return t("organization.tabs.users.removeButton.credentialsUnused");
  }, [data, t]);

  const linkText = (): JSX.Element | null => {
    if (data.isUsed) {
      return (
        <Link
          href={
            data.organizationName === ""
              ? `groups/${data.groupName}/scope`
              : `/orgs/${data.organizationName}/groups/${
                  data.groupName.split(" ")[0]
                }/scope`
          }
          target={"_blank"}
        >
          {t("organization.tabs.users.removeButton.credentialsRemoveLink")}
        </Link>
      );
    }
    if (!data.type.includes("OAUTH")) {
      return (
        <Link
          href={
            data.organizationName === ""
              ? "credentials"
              : `/orgs/${data.organizationName}/credentials`
          }
          target={"_blank"}
        >
          {t("organization.tabs.users.removeButton.credentialsEditLink")}
        </Link>
      );
    }

    return null;
  };

  return (
    <div style={{ whiteSpace: "pre-line" }}>
      {text}
      {"\n"}
      {linkText()}
    </div>
  );
};

export { Information };
