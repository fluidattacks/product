import { PureAbility } from "@casl/ability";
import { fireEvent, screen, waitFor } from "@testing-library/react";
import { userEvent } from "@testing-library/user-event";
import { GraphQLError } from "graphql";
import { HttpResponse, type StrictResponse, graphql } from "msw";

import { GET_ORGANIZATION_BILLING } from "./confirm-invitation/queries";
import { GET_STAKEHOLDER } from "./queries";
import type { IAddStakeholderModalProps } from "./types";

import { AddUserModal } from ".";
import { authzPermissionsContext } from "context/authz/config";
import type {
  GetOrganizationBillingAtInvitationQuery as GetOrganizationBilling,
  GetStakeholderQuery,
} from "gql/graphql";
import { StakeholderEntity } from "gql/graphql";
import { render } from "mocks";
import { LINK } from "mocks/constants";
import type { IErrorMessage } from "mocks/types";
import { msgError } from "utils/notifications";

jest.mock("utils/notifications", (): Record<string, unknown> => {
  const mockedNotifications: Record<string, () => Record<string, unknown>> =
    jest.requireActual("utils/notifications");
  jest.spyOn(mockedNotifications, "msgError").mockImplementation();

  return mockedNotifications;
});

const functionMock = jest.fn();

describe("add user modal", (): void => {
  const mockPropsAdd: IAddStakeholderModalProps = {
    action: "add",
    domainSuggestions: [],
    editTitle: "",
    groupName: "unittesting",
    onClose: functionMock,
    onSubmit: functionMock,
    open: true,
    suggestions: [],
    title: "",
    type: "group",
  };

  const mockPropsEdit: IAddStakeholderModalProps = {
    action: "edit",
    domainSuggestions: [],
    editTitle: "edit title",
    groupName: "unittesting",
    initialValues: {
      email: "user@test.com",
      role: "USER",
    },
    onClose: functionMock,
    onSubmit: functionMock,
    open: true,
    suggestions: [],
    title: "",
    type: "group",
  };

  const graphqlMocked = graphql.link(LINK);
  const mocks = [
    graphqlMocked.query(
      GET_STAKEHOLDER,
      ({
        variables,
      }): StrictResponse<IErrorMessage | { data: GetStakeholderQuery }> => {
        const { entity, groupName, organizationId, userEmail } = variables;
        if (
          entity === StakeholderEntity.Group &&
          groupName === "TEST" &&
          organizationId === "-" &&
          userEmail === "user@test.com"
        ) {
          return HttpResponse.json({
            data: {
              stakeholder: {
                __typename: "Stakeholder",
                email: "user@test.com",
                responsibility: "tester",
              },
            },
          });
        }
        if (
          entity === StakeholderEntity.Group &&
          groupName === "TEST" &&
          organizationId === "-" &&
          userEmail === "unittest@test.com"
        ) {
          return HttpResponse.json({
            data: {
              stakeholder: {
                __typename: "Stakeholder",
                email: "unittest@test.com",
                responsibility: "edited",
              },
            },
          });
        }

        return HttpResponse.json({
          errors: [new GraphQLError("Error getting a stakeholder")],
        });
      },
    ),
  ];

  const mockError = [
    graphqlMocked.query(GET_STAKEHOLDER, (): StrictResponse<IErrorMessage> => {
      return HttpResponse.json({
        errors: [new GraphQLError("Error getting a stakeholder")],
      });
    }),
  ];

  it("should handle errors when auto fill data", async (): Promise<void> => {
    expect.hasAssertions();

    render(
      <AddUserModal {...mockPropsEdit} groupName={"TEST"} type={"group"} />,
      { mocks: mockError },
    );
    await waitFor((): void => {
      expect(
        screen.getByPlaceholderText("userModal.emailPlaceholder"),
      ).toHaveValue("user@test.com");
    });

    expect(screen.queryByText("edit title")).toBeInTheDocument();

    fireEvent.blur(screen.getByPlaceholderText("userModal.emailPlaceholder"));
    await waitFor((): void => {
      expect(msgError).toHaveBeenCalledWith("groupAlerts.errorTextsad");
    });

    jest.clearAllMocks();
  });

  it("should render an add component group", async (): Promise<void> => {
    expect.hasAssertions();

    const mockedPermissions = new PureAbility<string>([
      { action: "grant_group_level_role:user" },
    ]);

    render(
      <authzPermissionsContext.Provider value={mockedPermissions}>
        <AddUserModal {...mockPropsAdd} groupName={"TEST"} type={"group"} />
      </authzPermissionsContext.Provider>,
      { mocks: mockError },
    );
    await waitFor((): void => {
      expect(
        screen.getByPlaceholderText("userModal.emailPlaceholder"),
      ).toHaveValue("");
    });

    fireEvent.change(screen.getByRole("combobox", { name: "email" }), {
      target: { value: "user@test.com" },
    });
    fireEvent.blur(screen.getByRole("combobox", { name: "email" }));

    await waitFor((): void => {
      expect(msgError).toHaveBeenCalledTimes(0);
    });

    expect(screen.queryByText("Required field")).not.toBeInTheDocument();

    await userEvent.click(screen.getByText("userModal.role"));
    await userEvent.click(screen.getByText("userModal.roles.user"));
    await userEvent.click(screen.getByText("Confirm"));

    expect(screen.queryByText("Required field")).toBeInTheDocument();

    expect(
      screen.queryByText("userModal.confirm.title"),
    ).not.toBeInTheDocument();

    await userEvent.type(
      screen.getByRole("textbox", { name: "responsibility" }),
      "Group Tester",
    );

    expect(screen.queryByText("Required field")).not.toBeInTheDocument();

    await userEvent.click(screen.getByText("Confirm"));
    await waitFor((): void => {
      expect(screen.queryByText("userModal.confirm.title")).toBeInTheDocument();
    });
    jest.clearAllMocks();
  });

  it("should render an add component organization", async (): Promise<void> => {
    expect.hasAssertions();

    const mockedPermissions = new PureAbility<string>([
      { action: "grant_organization_level_role:user" },
    ]);

    render(
      <authzPermissionsContext.Provider value={mockedPermissions}>
        <AddUserModal
          {...mockPropsAdd}
          organizationId={"ORG#f0c74b3e-bce4-4946-ba63-cb7e113ee817"}
          type={"organization"}
        />
      </authzPermissionsContext.Provider>,
      {
        mocks: [
          graphqlMocked.query(
            GET_ORGANIZATION_BILLING,
            ({
              variables,
            }): StrictResponse<
              IErrorMessage | { data: GetOrganizationBilling }
            > => {
              const { organizationId } = variables;
              if (
                organizationId === "ORG#f0c74b3e-bce4-4946-ba63-cb7e113ee817"
              ) {
                return HttpResponse.json({
                  data: {
                    organization: {
                      __typename: "Organization",
                      billing: { authors: [] },
                      name: "okada",
                    },
                  },
                });
              }

              return HttpResponse.json({
                errors: [new GraphQLError("Error getting group billing")],
              });
            },
          ),
        ],
      },
    );
    await waitFor((): void => {
      expect(
        screen.getByPlaceholderText("userModal.emailPlaceholder"),
      ).toHaveValue("");
    });

    fireEvent.change(screen.getByRole("combobox", { name: "email" }), {
      target: { value: "user@test.com" },
    });
    fireEvent.blur(screen.getByRole("combobox", { name: "email" }));

    expect(screen.queryByText("Required field")).not.toBeInTheDocument();

    await userEvent.click(screen.getByText("userModal.role"));
    await userEvent.click(screen.getByText("userModal.roles.user"));
    await userEvent.click(screen.getByText("Confirm"));

    await waitFor((): void => {
      expect(screen.queryByText("userModal.confirm.title")).toBeInTheDocument();
    });
    jest.clearAllMocks();
  });

  it("should render an edit component", async (): Promise<void> => {
    expect.hasAssertions();

    render(<AddUserModal {...mockPropsEdit} />, {
      mocks,
    });
    await waitFor((): void => {
      expect(
        screen.getByPlaceholderText("userModal.emailPlaceholder"),
      ).toHaveValue("user@test.com");
    });
    jest.clearAllMocks();
  });

  it("should auto fill data on inputs", async (): Promise<void> => {
    expect.hasAssertions();

    render(
      <AddUserModal {...mockPropsAdd} groupName={"TEST"} type={"group"} />,
      { mocks },
    );
    await waitFor((): void => {
      expect(
        screen.getByPlaceholderText("userModal.emailPlaceholder"),
      ).toHaveValue("");
    });
    fireEvent.change(screen.getByRole("combobox", { name: "email" }), {
      target: { value: "unittest@test.com" },
    });
    fireEvent.blur(screen.getByRole("combobox", { name: "email" }));
    await waitFor((): void => {
      expect(
        screen.getByRole("textbox", { name: "responsibility" }),
      ).toHaveValue("edited");
    });
    jest.clearAllMocks();
  });
});
