import { graphql } from "gql";

interface IStakeholder {
  stakeholder: {
    email: string;
    responsibility: string | undefined;
  };
}

const GET_STAKEHOLDER = graphql(`
  query GetStakeholder(
    $entity: StakeholderEntity!
    $groupName: String
    $organizationId: String
    $userEmail: String!
  ) {
    stakeholder(
      entity: $entity
      groupName: $groupName
      organizationId: $organizationId
      userEmail: $userEmail
    ) {
      email
      responsibility
    }
  }
`);

export { GET_STAKEHOLDER };
export type { IStakeholder };
