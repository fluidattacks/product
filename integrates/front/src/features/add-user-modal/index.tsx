import { Form, InnerForm, Modal, useModal } from "@fluidattacks/design";
import { Fragment, useCallback } from "react";

import { useConfirmInvitation } from "./confirm-invitation";
import { AddUserForm } from "./form";
import type {
  IAddStakeholderModalProps,
  IStakeholderFormValues,
} from "./types";
import { getInitialValues, getSuggestions } from "./utils";
import { validationSchema } from "./validations";

const AddUserModal = ({
  action,
  editTitle,
  initialValues,
  onClose,
  onSubmit,
  open,
  organizationId,
  groupName,
  domainSuggestions,
  suggestions,
  title,
  type,
}: IAddStakeholderModalProps): JSX.Element => {
  const modalProps = useModal("add-user-modal");
  const modalTitle = action === "add" ? title : editTitle;

  const formInitialValues = getInitialValues(initialValues, action);
  const shouldConfirm = open && action === "add";
  const { confirmInvitation, ConfirmInvitationDialog } = useConfirmInvitation(
    shouldConfirm,
    type,
    groupName,
    organizationId,
  );

  const confirmAndSubmit = useCallback(
    async (values: IStakeholderFormValues): Promise<void> => {
      if (await confirmInvitation(values.email)) {
        await onSubmit(values);
      }
    },
    [confirmInvitation, onSubmit],
  );

  return (
    <Fragment>
      <Modal
        id={"addUser"}
        modalRef={{ ...modalProps, close: onClose, isOpen: open }}
        size={"sm"}
        title={modalTitle}
      >
        <Form
          cancelButton={{ onClick: onClose }}
          defaultValues={{ ...formInitialValues, ...{ type } }}
          onSubmit={confirmAndSubmit}
          yupSchema={validationSchema}
        >
          <InnerForm<IStakeholderFormValues>>
            {(methods): JSX.Element => {
              const email = methods.watch("email");
              const allSuggestions = getSuggestions(
                email,
                suggestions,
                domainSuggestions,
              );

              return (
                <AddUserForm
                  action={action}
                  allSuggestions={allSuggestions}
                  groupName={groupName}
                  organizationId={organizationId}
                  type={type}
                  {...methods}
                />
              );
            }}
          </InnerForm>
        </Form>
      </Modal>
      <ConfirmInvitationDialog />
    </Fragment>
  );
};

export { AddUserModal };
