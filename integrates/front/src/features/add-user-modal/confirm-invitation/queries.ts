import { graphql } from "gql";

const GET_GROUP_BILLING = graphql(`
  query GetGroupBilling($groupName: String!) {
    group(groupName: $groupName) {
      name
      billing {
        authors {
          actor
        }
      }
    }
  }
`);

const GET_ORGANIZATION_BILLING = graphql(`
  query GetOrganizationBillingAtInvitation($organizationId: String!) {
    organization(organizationId: $organizationId) {
      name
      billing {
        authors {
          actor
        }
      }
    }
  }
`);

export { GET_GROUP_BILLING, GET_ORGANIZATION_BILLING };
