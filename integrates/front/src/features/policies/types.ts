import type { IUseModal } from "@fluidattacks/design";

import type { GetPoliciesAtOrganizationQuery } from "gql/graphql";

interface IFormikPoliciesValues
  extends Omit<
    GetPoliciesAtOrganizationQuery["organization"]["groups"][0],
    "__typename" | "name"
  > {
  inactivityPeriod?: number;
  groups?: string[];
  __typename: "Group" | "Organization";
}

interface ISelectedPolicies
  extends Omit<
    GetPoliciesAtOrganizationQuery["organization"],
    "__typename" | "groups" | "inactivityPeriod"
  > {
  __typename: "Group" | "Organization";
}

interface IUpdateModalProps extends ISelectedPolicies {
  inactivityPeriod?: number;
  title: string;
  handleSubmit: (values: IFormikPoliciesValues) => void;
  handleCancel: VoidFunction;
  modalRef: IUseModal;
}

interface IPolicies {
  data: GetPoliciesAtOrganizationQuery["organization"];
  handleSubmit: (values: IFormikPoliciesValues) => void;
  handleResetToOrgPolicies: (selectedGroups: string[]) => Promise<void>;
}

export type {
  IFormikPoliciesValues,
  IUpdateModalProps,
  IPolicies,
  ISelectedPolicies,
};
