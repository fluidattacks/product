import type { TFormMethods } from "@fluidattacks/design";
import {
  Alert,
  Container,
  Divider,
  GridContainer,
  Heading,
  InputNumber,
  Link,
  Text,
} from "@fluidattacks/design";
import { Fragment } from "react";
import { useTranslation } from "react-i18next";
import { useTheme } from "styled-components";

import type { IFormikPoliciesValues } from "../../types";
import { returnDashIfNil } from "features/policies/utils";

const POLICIES_URL =
  "https://help.fluidattacks.com/portal/en/kb/articles/manage-general-policies";

const UpdateModalInputs = ({
  form,
}: {
  form: Readonly<TFormMethods<IFormikPoliciesValues>>;
}): JSX.Element => {
  const { t } = useTranslation();
  const theme = useTheme();
  const { formState, watch, register, setValue } = form;

  const maxNumberAcceptances = watch("maxNumberAcceptances");
  const maxAcceptanceDays = watch("maxAcceptanceDays");

  const showWarning =
    maxNumberAcceptances !== formState.defaultValues?.maxNumberAcceptances ||
    maxAcceptanceDays !== formState.defaultValues.maxAcceptanceDays;

  return (
    <Fragment>
      <Container
        alignItems={"center"}
        display={"inline-flex"}
        gap={0.5}
        width={"fit-content"}
      >
        <Heading my={0} size={"xs"}>
          {t("policies.temporaryAcceptance")}
        </Heading>
        <Link
          href={`${
            POLICIES_URL
          }#Maximum_number_of_times_a_finding_can_be_accepted`}
          target={"_blank"}
        />
      </Container>
      <Divider mb={0.25} mt={0} />
      <Text color={"black"} fontWeight={"bold"} size={"sm"}>
        {t("policies.duration")}
      </Text>
      <GridContainer lg={2} md={1} sm={1} xl={2}>
        <InputNumber
          error={formState.errors.minAcceptanceSeverity?.message}
          helpLink={`${POLICIES_URL}#Maximum_number_of_calendar_days_a_finding_can_be_temporarily_accepted`}
          label={t("policies.maxAcceptanceDays")}
          linkPosition={"up"}
          max={999}
          min={0}
          name={"maxAcceptanceDays"}
          register={register}
          setValue={setValue}
          watch={watch}
        />
        <InputNumber
          error={formState.errors.maxAcceptanceSeverity?.message}
          helpLink={`${POLICIES_URL}#Maximum_number_of_times_a_finding_can_be_accepted`}
          label={t("policies.maxNumberAcceptances")}
          linkPosition={"up"}
          max={Infinity}
          min={0}
          name={"maxNumberAcceptances"}
          register={register}
          setValue={setValue}
          watch={watch}
        />
      </GridContainer>
      {showWarning && (
        <Alert variant={"warning"}>
          {t("policies.update.warning", {
            days: returnDashIfNil(maxAcceptanceDays),
            times: returnDashIfNil(maxNumberAcceptances),
          })}
        </Alert>
      )}
      <Text color={"black"} fontWeight={"bold"} size={"sm"}>
        {t("policies.severity")}
      </Text>
      <GridContainer lg={2} md={1} sm={1} xl={2}>
        <InputNumber
          decimalPlaces={1}
          error={formState.errors.minAcceptanceSeverity?.message}
          helpLink={`${POLICIES_URL}#Minimum_CVSS_score_allowed_for_temporary_acceptance`}
          label={t("policies.minAcceptanceSeverity")}
          linkPosition={"up"}
          max={10}
          min={0}
          name={"minAcceptanceSeverity"}
          register={register}
          setValue={setValue}
          watch={watch}
        />
        <InputNumber
          decimalPlaces={1}
          error={formState.errors.maxAcceptanceSeverity?.message}
          helpLink={`${POLICIES_URL}#Maximum_CVSS_score_allowed_for_temporary_acceptance`}
          label={t("policies.maxAcceptanceSeverity")}
          linkPosition={"up"}
          max={10}
          min={0}
          name={"maxAcceptanceSeverity"}
          register={register}
          setValue={setValue}
          watch={watch}
        />
      </GridContainer>
      <Container
        alignItems={"center"}
        display={"inline-flex"}
        gap={0.5}
        mt={1}
        width={"fit-content"}
      >
        <Heading size={"xs"}>{t("policies.devSecOps")}</Heading>
        <Link
          href={`${
            POLICIES_URL
          }#Minimum_CVSS_score_of_an_open_vulnerability_to_break_the_build`}
          target={"_blank"}
        />
      </Container>
      <Divider mb={0.25} mt={0} />
      <Text
        color={theme.palette.gray[800]}
        fontWeight={"bold"}
        mb={0.25}
        size={"sm"}
      >
        {t("policies.duration")}
      </Text>
      <GridContainer lg={2} md={1} sm={1} xl={2}>
        <InputNumber
          error={formState.errors.vulnerabilityGracePeriod?.message}
          helpLink={`${POLICIES_URL}#Grace_period_where_newly_reported_vulnerabilities_will_not_break_the_build`}
          helpText={t("policies.gracePeriodHelpText")}
          label={t("policies.vulnerabilityGracePeriod")}
          linkPosition={"up"}
          max={Infinity}
          min={0}
          name={"vulnerabilityGracePeriod"}
          register={register}
          setValue={setValue}
          watch={watch}
        />
        <InputNumber
          error={formState.errors.daysUntilItBreaks?.message}
          helpLink={`${POLICIES_URL}#Number_of_days_until_vulnerabilities_are_considered_technical_debt_and_do_not_break_the_build`}
          helpText={t("policies.daysUntilItBreaksHelpText")}
          label={t("policies.daysUntilItBreaks")}
          linkPosition={"up"}
          max={Infinity}
          min={0}
          name={"daysUntilItBreaks"}
          register={register}
          setValue={setValue}
          watch={watch}
        />
      </GridContainer>
      <Text
        color={theme.palette.gray[800]}
        fontWeight={"bold"}
        mb={0.25}
        size={"sm"}
      >
        {t("policies.severity")}
      </Text>
      <GridContainer lg={1} md={1} sm={1} xl={1}>
        <InputNumber
          decimalPlaces={1}
          error={formState.errors.minBreakingSeverity?.message}
          helpLink={`${POLICIES_URL}#Minimum_CVSS_score_of_an_open_vulnerability_to_break_the_build`}
          helpText={t("policies.breakingTheBuildHelpText")}
          label={t("policies.minBreakingSeverity")}
          linkPosition={"up"}
          max={10}
          min={0}
          name={"minBreakingSeverity"}
          register={register}
          setValue={setValue}
          watch={watch}
        />
      </GridContainer>
      {formState.defaultValues?.__typename === "Group" ? undefined : (
        <Fragment>
          <Container
            alignItems={"center"}
            display={"inline-flex"}
            gap={0.5}
            mt={1}
            width={"fit-content"}
          >
            <Heading size={"xs"}>{t("policies.inactivityPolicy")}</Heading>
            <Link
              href={`${
                POLICIES_URL
              }#Number_of_days_after_which_a_member_is_removed_due_to_inactivity`}
              target={"_blank"}
            />
          </Container>
          <Divider mb={0.25} mt={0} />
          <GridContainer lg={1} md={1} sm={1} xl={1}>
            <InputNumber
              error={formState.errors.inactivityPeriod?.message}
              helpLink={`${POLICIES_URL}#Number_of_days_after_which_a_member_is_removed_due_to_inactivity`}
              label={t("policies.inactivityPeriod")}
              linkPosition={"up"}
              max={Infinity}
              min={0}
              name={"inactivityPeriod"}
              register={register}
              setValue={setValue}
              watch={watch}
            />
          </GridContainer>
        </Fragment>
      )}
    </Fragment>
  );
};

export { UpdateModalInputs };
