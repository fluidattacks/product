import {
  Container,
  Form,
  InnerForm,
  Input,
  Loading,
} from "@fluidattacks/design";
import type { ReactNode } from "react";
import * as React from "react";
import { useTranslation } from "react-i18next";
import { object, string } from "yup";

import type { IVerifyFormValues } from "../types";

interface IVerify {
  readonly disable: boolean;
  readonly handleProceed: (values: IVerifyFormValues) => void;
  readonly isChannelOpen: boolean;
  readonly message: ReactNode;
}

const Verify: React.FC<IVerify> = ({
  disable,
  handleProceed,
  isChannelOpen,
  message,
}): JSX.Element | null => {
  const { t } = useTranslation();
  const validationSchema = object().shape({
    verificationCode: string().required(t("validations.required")),
  });

  return (
    <Container display={isChannelOpen ? "none" : "block"} width={"100%"}>
      {message}
      <Form
        confirmButton={{ disabled: disable, label: t("verifyDialog.verify") }}
        defaultValues={{ verificationCode: "" }}
        id={"verify"}
        maxButtonWidth={"190px"}
        onSubmit={handleProceed}
        yupSchema={validationSchema}
      >
        <InnerForm>
          {({ formState, register }): JSX.Element => {
            return (
              <React.Fragment>
                <Input
                  error={formState.errors.verificationCode?.message?.toString()}
                  isTouched={"verificationCode" in formState.touchedFields}
                  isValid={!("verificationCode" in formState.errors)}
                  label={t("verifyDialog.fields.verificationCode")}
                  name={"verificationCode"}
                  register={register}
                />
                {formState.isSubmitting ? (
                  <Loading label={"Verifying..."} size={"sm"} />
                ) : undefined}
              </React.Fragment>
            );
          }}
        </InnerForm>
      </Form>
    </Container>
  );
};

export { Verify };
