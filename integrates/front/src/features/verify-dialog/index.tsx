import { useMutation, useQuery } from "@apollo/client";
import { Modal, useModal } from "@fluidattacks/design";
import isNil from "lodash/isNil";
import { useCallback, useState } from "react";
import * as React from "react";
import { useTranslation } from "react-i18next";

import { Channels } from "./channels";
import { GET_STAKEHOLDER_PHONE, VERIFY_STAKEHOLDER_MUTATION } from "./queries";
import type { IVerifyDialogProps, IVerifyFormValues, TVerifyFn } from "./types";
import { Verify } from "./verify";
import { VerifyTour } from "./verify-tour";

import type { VerificationChannel } from "gql/graphql";
import { Logger } from "utils/logger";
import { msgError, msgSuccess } from "utils/notifications";

const VerifyDialog: React.FC<IVerifyDialogProps> = ({
  disable = false,
  isOpen,
  callbacks,
  children,
  message,
}): JSX.Element => {
  const { t } = useTranslation();
  const modalProps = useModal("verify-dialog");
  const [verifyCallback, setVerifyCallback] = useState<
    (verificationCode: string) => Promise<void>
  >(
    callbacks
      ? (): ((verificationCode: string) => Promise<void>) =>
          callbacks.verifyCallback
      : (): ((verificationCode: string) => Promise<void>) =>
          async (_verificationCode: string): Promise<void> =>
            Promise.resolve(),
  );
  const [cancelCallback, setCancelCallback] = useState<() => void>(
    callbacks
      ? (): (() => void) => callbacks.cancelCallback
      : (): (() => void) => (): void => undefined,
  );
  const [isChannelOpen, setIsChannelOpen] = useState(true);

  const [handleVerifyStakeholder] = useMutation(VERIFY_STAKEHOLDER_MUTATION, {
    onCompleted: (data): void => {
      if (data.verifyStakeholder.success) {
        msgSuccess(
          t("verifyDialog.alerts.sendMobileVerificationSuccess"),
          t("groupAlerts.titleSuccess"),
        );
      }
    },
    onError: (errors): void => {
      errors.graphQLErrors.forEach((error): void => {
        switch (error.message) {
          case "Exception - Stakeholder verification could not be started":
            msgError(t("verifyDialog.alerts.nonSentVerificationCode"));
            break;
          case "Exception - Too many requests":
            msgError(t("groupAlerts.tooManyRequests"));
            break;
          default:
            msgError(t("groupAlerts.errorTextsad"));
            Logger.warning(
              "An error occurred sending a verification code",
              error,
            );
        }
        setIsChannelOpen(true);
        cancelCallback();
      });
    },
  });

  const { data } = useQuery(GET_STAKEHOLDER_PHONE, {
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error): void => {
        Logger.error("Couldn't load stakeholder's phone", error);
      });
    },
  });

  const handleClose = useCallback((): void => {
    setIsChannelOpen(true);
    cancelCallback();
  }, [cancelCallback]);

  const handleProceed = useCallback(
    (values: IVerifyFormValues): void => {
      void verifyCallback(values.verificationCode);
    },
    [verifyCallback],
  );

  const { phone } = data?.me ?? {};
  const handleChannelClick = useCallback(
    (event: React.MouseEvent<HTMLButtonElement>): void => {
      if (!isNil(phone)) {
        const channel = event.currentTarget.value as VerificationChannel;
        void handleVerifyStakeholder({ variables: { channel } });
      }
      setIsChannelOpen(false);
    },
    [phone, handleVerifyStakeholder, setIsChannelOpen],
  );

  if (isNil(data)) {
    return <div />;
  }

  const setVerifyCallbacks: TVerifyFn = (
    verifyFn: (verificationCode: string) => Promise<void>,
    cancelFn: () => void,
  ): void => {
    setVerifyCallback(
      (): ((verificationCode: string) => Promise<void>) => verifyFn,
    );
    setCancelCallback((): (() => void) => cancelFn);
  };

  return (
    <React.Fragment>
      <VerifyTour handleClose={handleClose} isOpen={isOpen} phone={phone} />
      <Modal
        modalRef={{ ...modalProps, close: handleClose, isOpen }}
        size={"sm"}
        title={t("verifyDialog.title")}
      >
        <Channels
          isChannelOpen={isChannelOpen}
          onClick={handleChannelClick}
          phone={phone?.nationalNumber}
        />
        <Verify
          disable={disable}
          handleProceed={handleProceed}
          isChannelOpen={isChannelOpen}
          message={message}
        />
      </Modal>
      {callbacks ? undefined : children?.(setVerifyCallbacks)}
    </React.Fragment>
  );
};

export { VerifyDialog };
