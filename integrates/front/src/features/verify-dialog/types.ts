interface IPhoneAttr {
  callingCountryCode: string;
  nationalNumber: string;
}

interface IGetStakeholderPhoneAttr {
  me: {
    __typename: "Me";
    phone: IPhoneAttr | null;
    userEmail: string;
  };
}

type TVerifyFn = (
  verifyCallback: (verificationCode: string) => Promise<void>,
  cancelCallback: () => void,
) => void;

interface IVerifyFormValues {
  verificationCode: string;
}

interface ICallBacks {
  verifyCallback: (verificationCode: string) => Promise<void>;
  cancelCallback: () => void;
}

interface IVerifyDialogProps {
  disable?: boolean;
  isOpen: boolean;
  message?: React.ReactNode;
  children?: (verify: TVerifyFn) => React.ReactNode;
  callbacks?: ICallBacks;
}

export type {
  IPhoneAttr,
  IGetStakeholderPhoneAttr,
  IVerifyDialogProps,
  IVerifyFormValues,
  TVerifyFn,
};
