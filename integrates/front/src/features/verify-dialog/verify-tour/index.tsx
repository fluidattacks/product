import { Tour, baseStep } from "@fluidattacks/design";
import isNil from "lodash/isNil";
import type { FC } from "react";
import { useTranslation } from "react-i18next";

interface IVerifyTour {
  readonly isOpen: boolean;
  readonly handleClose: () => void;
  readonly phone:
    | {
        __typename?: "Phone";
        callingCountryCode: string;
        countryCode: string;
        nationalNumber: string;
      }
    | null
    | undefined;
}

const VerifyTour: FC<IVerifyTour> = ({
  handleClose,
  isOpen,
  phone,
}): JSX.Element | null => {
  const { t } = useTranslation();

  if (isOpen && isNil(phone)) {
    return (
      <Tour
        onFinish={handleClose}
        run={true}
        steps={[
          {
            ...baseStep,
            content: t("verifyDialog.tour.addMobile.profile"),
            disableBeacon: true,
            hideFooter: true,
            target: "#navbar-user-profile",
          },
        ]}
      />
    );
  }

  return null;
};

export { VerifyTour };
