import { fireEvent, screen } from "@testing-library/react";

import { Channels } from ".";
import { render } from "mocks";

describe("channels", (): void => {
  const onClick = jest.fn();

  it("should render channels", (): void => {
    expect.hasAssertions();

    render(
      <Channels isChannelOpen={true} onClick={onClick} phone={undefined} />,
    );

    expect(
      screen.getByText("components.channels.smsButton.text"),
    ).toBeInTheDocument();
    expect(
      screen.getByText("components.channels.whatsappButton.text"),
    ).toBeInTheDocument();
  });

  it("should display channel icon", (): void => {
    expect.hasAssertions();

    const { container } = render(
      <Channels isChannelOpen={true} onClick={onClick} phone={undefined} />,
    );

    expect(container.querySelectorAll(".fa-message-sms")).toHaveLength(1);
    expect(container.querySelectorAll(".fa-whatsapp")).toHaveLength(1);
  });

  it("should display channel description", (): void => {
    expect.hasAssertions();

    render(
      <Channels isChannelOpen={true} onClick={onClick} phone={"1234567890"} />,
    );

    expect(
      screen.getAllByText("components.channels.description 7890"),
    ).toHaveLength(2);
  });

  it("test channel button click", (): void => {
    expect.hasAssertions();

    render(
      <Channels isChannelOpen={true} onClick={onClick} phone={undefined} />,
    );

    fireEvent.click(screen.getByText("components.channels.smsButton.text"));

    expect(onClick).toHaveBeenCalledTimes(1);
  });

  it("test button values", (): void => {
    expect.hasAssertions();

    render(
      <Channels isChannelOpen={true} onClick={onClick} phone={undefined} />,
    );

    const buttonSMS = screen.getByRole("button", {
      name: "components.channels.smsButton.text",
    });
    const buttonWhatsApp = screen.getByRole("button", {
      name: "components.channels.whatsappButton.text",
    });

    expect(buttonSMS).toHaveAttribute("value", "SMS");
    expect(buttonWhatsApp).toHaveAttribute("value", "WHATSAPP");
  });
});
