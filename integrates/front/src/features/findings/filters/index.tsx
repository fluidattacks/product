import _ from "lodash";
import { useCallback, useContext, useEffect, useState } from "react";
import { useTranslation } from "react-i18next";

import type { IFindingsFilters } from "./types";

import { Filters } from "components/filter";
import type {
  IFilter,
  IPermanentData,
  ISelectedOptions,
} from "components/filter/types";
import { meetingModeContext } from "context/meeting-mode";
import type { Technique } from "gql/graphql";
import { useDebouncedCallback } from "hooks/use-debounced-callback";
import { useStoredState } from "hooks/use-stored-state";
import { useFiltersStore } from "pages/finding/vulnerabilities/hooks/use-filters-store";
import type { IFindingAttr } from "pages/group/findings/types";
import { setFiltersUtil } from "utils/set-filters";

const DEBOUNCE = 500;

const FindingsFilters = ({
  findings,
  groupName,
  cvssVersion,
  permissions,
  refetch,
  setFilteredFindings,
}: Readonly<IFindingsFilters>): JSX.Element => {
  const { t } = useTranslation();
  const { meetingMode } = useContext(meetingModeContext);
  const { setSearch, setTechnique } = useFiltersStore(
    `findingFilters-${groupName}`,
  );
  const onFilterSeverity = useCallback(
    (
      finding: IFindingAttr,
      _value?: string,
      _rangeValues?: [string, string],
      numberRangeValues?: [number | undefined, number | undefined],
    ): boolean => {
      if (
        numberRangeValues === undefined ||
        _.every(numberRangeValues, _.isNil)
      )
        return true;

      const currentNumber = _.toNumber(finding[cvssVersion]);
      const isHigher = _.isNil(numberRangeValues[0])
        ? true
        : currentNumber >= _.toNumber(numberRangeValues[0]);
      const isLower = _.isNil(numberRangeValues[1])
        ? true
        : currentNumber <= _.toNumber(numberRangeValues[1]);

      return isLower && isHigher;
    },
    [cvssVersion],
  );
  const [filters, setFilters] = useState<IFilter<IFindingAttr>[]>([
    {
      id: "root",
      isBackFilter: true,
      key: "root",
      label: "Location",
      type: "text",
    },
    {
      id: "lastVulnerability",
      key: "lastVulnerability",
      label: t("group.findings.lastReport"),
      type: "number",
    },
    {
      id: "title",
      key: "title",
      label: t("group.findings.type"),
      selectOptions: (findingsList): ISelectedOptions[] =>
        findingsList.map(
          (finding): ISelectedOptions => ({
            header: finding.title,
            value: finding.title,
          }),
        ),
      type: "select",
    },
    {
      id: "state",
      key: "status",
      label: t("group.findings.status"),
      selectOptions: [
        ...(permissions.can("see_draft_status")
          ? [
              {
                header: t("searchFindings.header.status.draft.label"),
                value: "DRAFT",
              },
            ]
          : []),
        {
          header: t("searchFindings.header.status.open.label"),
          value: "VULNERABLE",
        },
        {
          header: t("searchFindings.header.status.closed.label"),
          value: "SAFE",
        },
      ],
      type: "select",
    },
    {
      id: "technique",
      isBackFilter: true,
      key: "technique",
      label: t("searchFindings.tabVuln.vulnTable.technique"),
      selectOptions: [
        { header: t("searchFindings.tabVuln.technique.cspm"), value: "CSPM" },
        { header: t("searchFindings.tabVuln.technique.dast"), value: "DAST" },
        { header: t("searchFindings.tabVuln.technique.ptaas"), value: "PTAAS" },
        { header: t("searchFindings.tabVuln.technique.re"), value: "RE" },
        { header: t("searchFindings.tabVuln.technique.sast"), value: "SAST" },
        { header: t("searchFindings.tabVuln.technique.sca"), value: "SCA" },
        { header: t("searchFindings.tabVuln.technique.scr"), value: "SCR" },
      ],
      type: "select",
    },
    {
      id: "treatment",
      key: (finding, value): boolean => {
        if (value === "" || value === undefined) return true;

        return (
          finding.treatmentSummary[
            value as keyof typeof finding.treatmentSummary
          ] > 0
        );
      },
      label: t("group.findings.treatment"),
      selectOptions: [
        {
          header: t("searchFindings.tabDescription.treatment.new"),
          value: "untreated",
        },
        {
          header: t("searchFindings.tabDescription.treatment.inProgress"),
          value: "inProgress",
        },
        {
          header: t("searchFindings.tabDescription.treatment.accepted"),
          value: "accepted",
        },
        {
          header: t(
            "searchFindings.tabDescription.treatment.acceptedUndefined",
          ),
          value: "acceptedUndefined",
        },
      ],
      type: "select",
    },
    {
      id: "maxOpenSeverityScore",
      key: onFilterSeverity,
      label: t("group.findings.severity"),
      type: "numberRange",
    },
    {
      id: "age",
      key: "age",
      label: t("group.findings.age"),
      type: "number",
    },
    {
      id: "reattack",
      key: "reattack",
      label: t("group.findings.reattack"),
      selectOptions: [
        {
          header: "Pending",
          value: "pending",
        },
        {
          header: "-",
          value: "-",
        },
      ],
      type: "select",
    },
    {
      id: "releaseDate",
      key: "releaseDate",
      label: t("group.findings.releaseDate"),
      type: "dateRange",
    },
    ...(permissions.can("see_review_filter")
      ? ([
          {
            id: "review",
            key: (finding, value): boolean => {
              switch (value) {
                case "YES":
                  return (
                    (_.isNumber(finding.rejectedVulnerabilities) &&
                      finding.rejectedVulnerabilities > 0) ||
                    (_.isNumber(finding.submittedVulnerabilities) &&
                      finding.submittedVulnerabilities > 0)
                  );
                case "NO":
                  return (
                    (!_.isNumber(finding.rejectedVulnerabilities) ||
                      finding.rejectedVulnerabilities === 0) &&
                    (!_.isNumber(finding.submittedVulnerabilities) ||
                      finding.submittedVulnerabilities === 0)
                  );
                case undefined:
                default:
                  return true;
              }
            },
            label: t("group.findings.review"),
            selectOptions: [
              {
                header: t("group.findings.boolean.True"),
                value: "YES",
              },
              {
                header: t("group.findings.boolean.False"),
                value: "NO",
              },
            ],
            type: "select",
          },
        ] as IFilter<IFindingAttr>[])
      : []),
  ]);

  const [filterVal, setFilterVal] = useStoredState<IPermanentData[]>(
    `tblFindFilters-${groupName}`,
    [
      { id: "lastVulnerability", value: "" },
      { id: "title", value: "" },
      { id: "state", value: "" },
      { id: "technique", value: "" },
      { id: "treatment", value: "" },
      {
        id: "maxOpenSeverityScore",
        numberRangeValues: [undefined, undefined],
        valueType: "numberRangeValues",
      },
      {
        id: "severityScore",
        numberRangeValues: [undefined, undefined],
        valueType: "numberRangeValues",
      },
      { id: "age", value: "" },
      { id: "locationsInfo", value: "" },
      { id: "reattack", value: "" },
      { id: "releaseDate", rangeValues: ["", ""], valueType: "rangeValues" },
      { id: "root", value: "" },
      { id: "review", value: "" },
    ],
    localStorage,
  );

  const onSearch = useCallback((value: string): void => {
    setSearch?.(value);
    // Zustand store already bring memoized data
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const onTechnique = useCallback((value: string): void => {
    setTechnique?.(value ? [value as unknown as Technique] : undefined);
    // Zustand store already bring memoized data
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const debouncedSearch = useDebouncedCallback(
    (root?: string, technique?: string): void => {
      void refetch({
        root: _.isEmpty(root) ? null : root,
        technique: _.isEmpty(technique) ? null : technique,
      });
    },
    DEBOUNCE,
  );

  useEffect((): VoidFunction => {
    const currentRootFilter = filters.find(
      (filter): boolean => filter.id === "root",
    )?.value;
    const currentTechniqueFilter = filters.find(
      (filter): boolean => filter.id === "technique",
    )?.value;
    if (currentRootFilter !== undefined && !_.isEmpty(currentRootFilter))
      onSearch(currentRootFilter);
    if (currentTechniqueFilter !== undefined)
      onTechnique(currentTechniqueFilter);

    debouncedSearch(currentRootFilter, currentTechniqueFilter);

    return (): void => {
      debouncedSearch.cancel();
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [filters, onSearch, onTechnique]);

  useEffect((): void => {
    const orderedFindings = _.orderBy(
      findings,
      ["status", "totalOpenPriority"],
      ["desc", "desc"],
    );

    const filteredDrafts =
      !permissions.can("see_draft_status") || meetingMode
        ? orderedFindings.filter(
            (finding): boolean => finding.status !== "DRAFT",
          )
        : orderedFindings;
    const filteredFindings = setFiltersUtil(filteredDrafts, filters);

    setFilteredFindings(filteredFindings);
  }, [
    cvssVersion,
    findings,
    filters,
    permissions,
    meetingMode,
    setFilteredFindings,
  ]);

  useEffect((): void => {
    setFilters((currentFilter): IFilter<IFindingAttr>[] => {
      return currentFilter.map((actualValue): IFilter<IFindingAttr> => {
        if (actualValue.id === "maxOpenSeverityScore") {
          return { ...actualValue, key: onFilterSeverity };
        }

        return actualValue;
      });
    });
  }, [cvssVersion, onFilterSeverity]);

  return (
    <Filters
      dataset={findings}
      filters={filters}
      permaset={[filterVal, setFilterVal]}
      setFilters={setFilters}
    />
  );
};

export { FindingsFilters };
