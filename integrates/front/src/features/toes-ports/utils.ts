import type { IFilter, ISelectedOptions } from "components/filter/types";
import type { GetToePortsQueryVariables } from "gql/graphql";
import type { IToePortData } from "pages/group/surface/ports/types";
import { formatBoolean, formatStatus } from "utils/format-helpers";
import { translate } from "utils/translations/translate";

const VALUES_FOR_16_BIT = 65536;

function filterColumns<T extends { id?: string }>(
  data: T[],
  isInternal: boolean,
  getToePortsVariables: GetToePortsQueryVariables,
): T[] {
  const {
    canGetAttackedAt,
    canGetAttackedBy,
    canGetFirstAttackAt,
    canGetSeenFirstTimeBy,
    canGetBePresentUntil,
  } = getToePortsVariables;

  return data.filter((filter): boolean => {
    switch (filter.id) {
      case "attackedAt":
        return isInternal && canGetAttackedAt;
      case "attackedBy":
        return isInternal && canGetAttackedBy;
      case "firstAttackAt":
        return isInternal && canGetFirstAttackAt;
      case "seenFirstTimeBy":
        return isInternal && canGetSeenFirstTimeBy;
      case "bePresentUntil":
        return isInternal && canGetBePresentUntil;
      case undefined:
      default:
        return true;
    }
  });
}

const baseFilters: IFilter<IToePortData>[] = [
  {
    id: "rootNickname",
    isBackFilter: true,
    key: "rootNickname",
    label: translate.t("group.toe.ports.root"),
    selectOptions: (ports): ISelectedOptions[] =>
      ports.map(
        (datapoint): ISelectedOptions => ({
          header: datapoint.rootNickname,
          value: datapoint.rootId,
        }),
      ),
    type: "select",
  },
  {
    id: "address",
    key: "address",
    label: translate.t("group.toe.ports.address"),
    selectOptions: (ports): ISelectedOptions[] =>
      ports.map(
        (datapoint): ISelectedOptions => ({ value: datapoint.address }),
      ),
    type: "select",
  },
  {
    id: "port",
    key: "port",
    label: translate.t("group.toe.ports.port"),
    minMaxRangeValues: [0, VALUES_FOR_16_BIT],
    type: "number",
  },
  {
    id: "hasVulnerabilities",
    key: "hasVulnerabilities",
    label: translate.t("group.toe.ports.status"),
    selectOptions: [
      { header: formatStatus(true), value: "true" },
      { header: formatStatus(false), value: "false" },
    ],
    type: "select",
  },
  {
    id: "seenAt",
    key: "seenAt",
    label: translate.t("group.toe.ports.seenAt"),
    type: "dateRange",
  },
  {
    id: "bePresent",
    isBackFilter: true,
    key: "bePresent",
    label: translate.t("group.toe.ports.bePresent"),
    selectOptions: [
      { header: formatBoolean(true), value: "true" },
      { header: formatBoolean(false), value: "false" },
    ],
    type: "select",
    value: "true",
  },
  {
    id: "attackedAt",
    key: "attackedAt",
    label: translate.t("group.toe.ports.attackedAt"),
    type: "dateRange",
  },
  {
    id: "attackedBy",
    key: "attackedBy",
    label: translate.t("group.toe.ports.attackedBy"),
    type: "text",
  },
  {
    id: "firstAttackAt",
    key: "firstAttackAt",
    label: translate.t("group.toe.ports.firstAttackAt"),
    type: "dateRange",
  },
  {
    id: "seenFirstTimeBy",
    key: "seenFirstTimeBy",
    label: translate.t("group.toe.ports.seenFirstTimeBy"),
    type: "text",
  },
  {
    id: "bePresentUntil",
    key: "bePresentUntil",
    label: translate.t("group.toe.ports.bePresentUntil"),
    type: "dateRange",
  },
];

export { filterColumns, baseFilters };
