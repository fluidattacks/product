import type { FormikHelpers } from "formik";
import { Formik } from "formik";
import { useCallback } from "react";
import * as React from "react";

import { InnerForm } from "./inner-form";
import type { ICommentEditorProps, ICommentForm } from "./types";
import { validatePreSubmit, validationSchema } from "./validations";

const CommentEditor: React.FC<Readonly<ICommentEditorProps>> = ({
  onPost,
}): JSX.Element => {
  const onSubmit = useCallback(
    (values: ICommentForm, helpers: FormikHelpers<ICommentForm>): void => {
      const { data, error } = validatePreSubmit(values, helpers);

      if (error) {
        return;
      }

      onPost(data["comment-editor"]);
      helpers.resetForm();
    },
    [onPost],
  );

  return (
    <Formik
      initialValues={{ "comment-editor": "" }}
      name={"addConsult"}
      onSubmit={onSubmit}
      validationSchema={validationSchema}
    >
      {(props): JSX.Element => <InnerForm {...props} />}
    </Formik>
  );
};

export { CommentEditor };
