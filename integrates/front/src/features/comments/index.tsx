import { Container, EmptyState, Text } from "@fluidattacks/design";
import dayjs from "dayjs";
import _ from "lodash";
import { useCallback, useContext, useMemo, useState } from "react";
import { useTranslation } from "react-i18next";

import { Comment } from "./comment";
import { CommentEditor } from "./comment-editor";
import { commentContext } from "./context";
import type {
  ICommentContext,
  ICommentStructure,
  ICommentsProps,
} from "./types";

import { Dropdown } from "components/dropdown";
import type { IDropDownOption } from "components/dropdown/types";
import { authContext } from "context/auth";
import type { IAuthContext } from "context/auth";
import { Have } from "context/authz/have";
import { useCalendly } from "hooks/use-calendly";

enum OrderBy {
  NEWEST = "newest",
  OLDEST = "oldest",
}

export const Comments = ({
  comments,
  emptyComponentsProps,
  onPostComment,
  isFindingComment = false,
  isObservation = false,
}: Readonly<ICommentsProps>): JSX.Element => {
  const { t } = useTranslation();
  const { userEmail, userName }: IAuthContext = useContext(authContext);
  const [replying, setReplying] = useState<number>(0);
  const [orderBy, setOrderBy] = useState<OrderBy>(OrderBy.NEWEST);
  const [showCommentEditor, setShowCommentEditor] = useState<boolean>(false);
  const { isAdvancedActive } = useCalendly();

  const postHandler = useCallback(
    (editorText: string): void => {
      onPostComment({
        content: editorText,
        created: dayjs().toString(),
        createdByCurrentUser: true,
        email: userEmail,
        fullName: userName,
        id: 0,
        modified: dayjs().toString(),
        parentComment: replying,
      });
      setReplying(0);
    },
    [onPostComment, replying, userEmail, userName],
  );

  const onOrderChange = useCallback((selection: IDropDownOption): void => {
    setOrderBy(selection.value as OrderBy);
  }, []);

  const rootComments: ICommentStructure[] = _.filter(comments, [
    "parentComment",
    0,
  ]);

  const orderComments = (
    unordered: ICommentStructure[],
    order: OrderBy,
  ): ICommentStructure[] => {
    return order === OrderBy.OLDEST
      ? _.orderBy(unordered, ["created"], ["asc"])
      : _.orderBy(unordered, ["created"], ["desc"]);
  };

  const value = useMemo(
    (): ICommentContext => ({ replying, setReplying }),
    [replying],
  );

  if (rootComments.length === 0 && !showCommentEditor) {
    const {
      confirmButton = {
        text: t("comments.add"),
      },
      description = t("comments.noComments.description"),
      title = t("comments.noComments.title"),
    } = emptyComponentsProps ?? {};

    return (
      <EmptyState
        confirmButton={
          isAdvancedActive || !isFindingComment
            ? {
                onClick: (): void => {
                  setShowCommentEditor(true);
                },
                text: confirmButton.text,
              }
            : undefined
        }
        description={description}
        imageSrc={"integrates/empty/consulting"}
        padding={0}
        title={title}
      />
    );
  }

  return (
    <Container>
      <commentContext.Provider value={value}>
        <Have I={"has_advanced"} passThrough={isObservation}>
          <CommentEditor onPost={postHandler} />
        </Have>
        {comments.length > 1 && (
          <Container alignItems={"center"} display={"inline-flex"} gap={0.5}>
            <Text size={"sm"}>{t("comments.orderBy.label")}</Text>
            <Dropdown
              customSelectionHandler={onOrderChange}
              items={[
                {
                  header: t("comments.orderBy.newest"),
                  value: OrderBy.NEWEST,
                },
                {
                  header: t("comments.orderBy.oldest"),
                  value: OrderBy.OLDEST,
                },
              ]}
            />
          </Container>
        )}
        <Container mt={1.25}>
          {orderComments(rootComments, orderBy).map(
            (comment: ICommentStructure): JSX.Element => (
              <Container key={comment.id} mb={1.25}>
                <Comment
                  backgroundEnabled={false}
                  comments={comments}
                  id={comment.id}
                  isObservation={isObservation}
                  key={comment.id}
                  onPost={postHandler}
                  orderBy={orderBy}
                />
              </Container>
            ),
          )}
        </Container>
      </commentContext.Provider>
    </Container>
  );
};
