import {
  CloudImage,
  Container,
  Modal,
  PremiumFeature,
  Span,
  Text,
} from "@fluidattacks/design";
import * as React from "react";
import { Trans, useTranslation } from "react-i18next";

import { useModal } from "hooks/use-modal";

interface IUpgradeGroupsModalProps {
  readonly onClose: () => void;
  img: string;
  title: string;
  description: string;
}

const UpgradeFreeTrialModal: React.FC<IUpgradeGroupsModalProps> = ({
  onClose,
  img,
  description,
  title,
}): JSX.Element => {
  const { t } = useTranslation();
  const modalRef = useModal("upgrade-groups-modal");

  const toggleModal = React.useCallback((): void => {
    onClose();
  }, [onClose]);

  return (
    <Modal
      cancelButton={{ onClick: onClose, text: t("buttons.cancel") }}
      id={"upgrade-groups-modal"}
      modalRef={{
        ...modalRef,
        close: onClose,
        isOpen: true,
        toggle: toggleModal,
      }}
      size={"sm"}
      title={
        <Container
          alignItems={"center"}
          display={"flex"}
          justify={"space-between"}
          width={"100%"}
        >
          <Text color={"black"} fontWeight={"bold"} size={"md"}>
            {title}
          </Text>
          <PremiumFeature margin={1} text={"Upgrade"} />
        </Container>
      }
    >
      <Container style={{ marginLeft: "-25px", marginRight: "-25px" }}>
        <CloudImage alt={img} publicId={img} width={"100%"} />
      </Container>
      <Container mt={1}>
        <Trans
          components={{
            bold: (
              <Span fontWeight={"bold"} size={"sm"}>
                {undefined}
              </Span>
            ),
          }}
          i18nKey={description}
          t={t}
        />
      </Container>
    </Modal>
  );
};

export { UpgradeFreeTrialModal };
