import { Alert, Form, InnerForm, Input, Modal } from "@fluidattacks/design";
import { Fragment } from "react";
import { useTranslation } from "react-i18next";

import type { ISetMailmapEntryAsMailmapSubentryModalProps } from "../types";

const SetMailmapEntryAsMailmapSubentryModal: React.FC<
  ISetMailmapEntryAsMailmapSubentryModalProps
> = ({
  onClose,
  onSubmit,
  entryEmail,
  modalRef,
}: ISetMailmapEntryAsMailmapSubentryModalProps): JSX.Element => {
  const { t } = useTranslation();

  return (
    <Modal
      modalRef={modalRef}
      size={"sm"}
      title={t("mailmap.setMailmapEntryAsMailmapSubentry")}
    >
      <Form
        cancelButton={{ onClick: onClose }}
        confirmButton={{ label: t("mailmap.setEntryAsSubentry") }}
        defaultDisabled={false}
        defaultValues={{
          entryEmail,
          targetEntryEmail: "",
        }}
        onSubmit={onSubmit}
      >
        <InnerForm>
          {({ formState, getFieldState, register }): JSX.Element => (
            <Fragment>
              <Input
                disabled={true}
                label={t("mailmap.entryEmail")}
                name={"entryEmail"}
                placeholder={t("mailmap.entryEmail")}
                register={register}
              />
              <Input
                error={formState.errors.targetEntryEmail?.message?.toString()}
                isTouched={getFieldState("targetEntryEmail").isTouched}
                isValid={!getFieldState("targetEntryEmail").invalid}
                label={t("mailmap.targetEntryEmail")}
                name={"targetEntryEmail"}
                placeholder={t("mailmap.targetEntryEmail")}
                register={register}
              />
              <Alert>
                {t("mailmap.alertSetMailmapEntryAsMailmapSubentry")}
              </Alert>
            </Fragment>
          )}
        </InnerForm>
      </Form>
    </Modal>
  );
};

export { SetMailmapEntryAsMailmapSubentryModal };
