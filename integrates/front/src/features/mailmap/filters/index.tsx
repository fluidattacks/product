import { useEffect, useState } from "react";
import * as React from "react";
import { useTranslation } from "react-i18next";

import type { IMailmapRecord } from "../types";
import { Filters } from "components/filter";
import type { IFilter, IPermanentData } from "components/filter/types";
import { useStoredState } from "hooks/use-stored-state";
import { setFiltersUtil } from "utils/set-filters";

const MailmapFilters = ({
  dataset,
  setFilteredDataset,
}: Readonly<{
  dataset: IMailmapRecord[];
  setFilteredDataset: React.Dispatch<React.SetStateAction<IMailmapRecord[]>>;
}>): JSX.Element => {
  const { t } = useTranslation();

  const [filters, setFilters] = useState<IFilter<IMailmapRecord>[]>([
    {
      id: "entryName",
      key: "mailmapEntryName",
      label: t("mailmap.entryName"),
      type: "text",
    },
    {
      id: "entryEmail",
      key: "mailmapEntryEmail",
      label: t("mailmap.entryEmail"),
      type: "text",
    },
    {
      id: "subentryName",
      key: "mailmapSubentryName",
      label: t("mailmap.subentryName"),
      type: "text",
    },
    {
      id: "subentryEmail",
      key: "mailmapSubentryEmail",
      label: t("mailmap.subentryEmail"),
      type: "text",
    },
  ]);

  const [filtersPermaset, setFiltersPermaset] = useStoredState<
    IPermanentData[]
  >(
    "tblMailmapFilters",
    [
      { id: "entryName", value: "" },
      { id: "entryEmail", value: "" },
      { id: "subentryName", value: "" },
      { id: "subentryEmail", value: "" },
    ],
    localStorage,
  );

  useEffect((): void => {
    setFilteredDataset(setFiltersUtil(dataset, filters));
  }, [setFilteredDataset, dataset, filters]);

  return (
    <Filters
      dataset={dataset}
      filters={filters}
      permaset={[filtersPermaset, setFiltersPermaset]}
      setFilters={setFilters}
    />
  );
};

export { MailmapFilters };
