import { Alert, Form, InnerForm, Input, Modal } from "@fluidattacks/design";
import { Fragment } from "react";
import { useTranslation } from "react-i18next";

import type { IMoveMailmapEntryWithSubentriesModalProps } from "../types";

const MoveMailmapEntryWithSubentriesModal: React.FC<
  IMoveMailmapEntryWithSubentriesModalProps
> = ({
  onClose,
  onSubmit,
  entryEmail,
  modalRef,
}: IMoveMailmapEntryWithSubentriesModalProps): JSX.Element => {
  const { t } = useTranslation();

  return (
    <Modal
      modalRef={modalRef}
      size={"sm"}
      title={t("mailmap.moveMailmapEntryWithSubentries")}
    >
      <Form
        cancelButton={{ onClick: onClose }}
        confirmButton={{ label: t("mailmap.moveEntryWithSubentries") }}
        defaultDisabled={false}
        defaultValues={{
          entryEmail,
          newOrganizationId: "",
        }}
        onSubmit={onSubmit}
      >
        <InnerForm>
          {({ formState, getFieldState, register }): JSX.Element => (
            <Fragment>
              <Input
                disabled={true}
                label={t("mailmap.entryEmail")}
                name={"entryEmail"}
                placeholder={t("mailmap.entryEmail")}
                register={register}
              />
              <Alert variant={"info"}>
                {t("mailmap.infoMoveMailmapEntryWithSubentries")}
              </Alert>
              <Input
                error={formState.errors.newOrganizationId?.message?.toString()}
                isTouched={getFieldState("newOrganizationId").isTouched}
                isValid={!getFieldState("newOrganizationId").invalid}
                label={t("mailmap.newOrganizationId")}
                name={"newOrganizationId"}
                placeholder={t("mailmap.newOrganizationId")}
                register={register}
              />
            </Fragment>
          )}
        </InnerForm>
      </Form>
    </Modal>
  );
};

export { MoveMailmapEntryWithSubentriesModal };
