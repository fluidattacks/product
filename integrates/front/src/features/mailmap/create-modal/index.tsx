import { Form, InnerForm, Input, Modal } from "@fluidattacks/design";
import type { FC } from "react";
import { Fragment } from "react";
import { useTranslation } from "react-i18next";
import { object, string } from "yup";

import type { ICreateMailmapEntryModalProps } from "../types";
import {
  domainPattern,
  subdomainPattern,
} from "pages/group/surface/lines/addition-modal/validations";

const AUTHOR_NAME_REGEX =
  /[^a-zA-Z0-9ñáéíóúäëïöüÑÁÉÍÓÚÄËÏÖÜ\s(){}[\],./:;@&_$%"'#*¿?¡!+-]/u;
const MAX_LENGTH = 200;
const localPartPattern = /^[a-zA-Z0-9!#$%&'“"*+/=?^_`{|}[\]~-]+/u;
const tldPattern = /[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9”"])?$/u;
const emailRegex = new RegExp(
  localPartPattern.source +
    subdomainPattern.source +
    domainPattern.source +
    tldPattern.source,
  "u",
);

const CreateMailmapEntryModal: FC<ICreateMailmapEntryModalProps> = ({
  modalRef,
  onClose,
  onSubmit,
}): JSX.Element => {
  const { t } = useTranslation();

  return (
    <Modal
      id={"createMailmapEntry"}
      modalRef={modalRef}
      size={"sm"}
      title={t("mailmap.createMailmapEntry")}
    >
      <Form
        cancelButton={{ onClick: onClose }}
        confirmButton={{ label: t("mailmap.createEntry") }}
        defaultValues={{
          entryEmail: "",
          entryName: "",
        }}
        onSubmit={onSubmit}
        yupSchema={object().shape({
          entryEmail: string()
            .matches(emailRegex, t("validations.email"))
            .max(MAX_LENGTH, t("validations.maxLength", { count: MAX_LENGTH }))
            .required(),
          entryName: string()
            .max(MAX_LENGTH, t("validations.maxLength", { count: MAX_LENGTH }))
            .isValidTextField(undefined, AUTHOR_NAME_REGEX)
            .required(),
        })}
      >
        <InnerForm>
          {({ formState, getFieldState, register }): JSX.Element => (
            <Fragment>
              <Input
                error={formState.errors.entryName?.message?.toString()}
                isTouched={getFieldState("entryName").isTouched}
                isValid={!getFieldState("entryName").invalid}
                label={t("mailmap.entryName")}
                name={"entryName"}
                placeholder={t("mailmap.entryName")}
                register={register}
              />
              <Input
                error={formState.errors.entryEmail?.message?.toString()}
                isTouched={getFieldState("entryEmail").isTouched}
                isValid={!getFieldState("entryEmail").invalid}
                label={t("mailmap.entryEmail")}
                name={"entryEmail"}
                placeholder={t("mailmap.entryEmail")}
                register={register}
              />
            </Fragment>
          )}
        </InnerForm>
      </Form>
    </Modal>
  );
};

export { CreateMailmapEntryModal, emailRegex, MAX_LENGTH, AUTHOR_NAME_REGEX };
