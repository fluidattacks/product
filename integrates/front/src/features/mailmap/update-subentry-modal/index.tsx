import { Form, InnerForm, Input, Modal } from "@fluidattacks/design";
import * as React from "react";
import { Fragment } from "react";
import { useTranslation } from "react-i18next";
import { object, string } from "yup";

import { AUTHOR_NAME_REGEX, MAX_LENGTH, emailRegex } from "../create-modal";
import type { IUpdateMailmapSubentryModalProps } from "../types";

const UpdateMailmapSubentryModal: React.FC<
  IUpdateMailmapSubentryModalProps
> = ({
  onClose,
  onSubmit,
  entryEmail,
  subentryEmail,
  subentryName,
  modalRef,
}: IUpdateMailmapSubentryModalProps): JSX.Element => {
  const { t } = useTranslation();

  return (
    <Modal
      modalRef={modalRef}
      size={"sm"}
      title={t("mailmap.updateMailmapSubentry")}
    >
      <Form
        cancelButton={{ onClick: onClose }}
        confirmButton={{ label: t("mailmap.updateSubentry") }}
        defaultDisabled={false}
        defaultValues={{
          entryEmail,
          newSubentryEmail: subentryEmail,
          newSubentryName: subentryName,
          oldSubentryEmail: subentryEmail,
          oldSubentryName: subentryName,
        }}
        onSubmit={onSubmit}
        yupSchema={object().shape({
          newSubentryEmail: string()
            .matches(emailRegex, t("validations.email"))
            .max(MAX_LENGTH, t("validations.maxLength", { count: MAX_LENGTH }))
            .required(),
          newSubentryName: string()
            .max(MAX_LENGTH, t("validations.maxLength", { count: MAX_LENGTH }))
            .isValidTextField(undefined, AUTHOR_NAME_REGEX)
            .required(),
        })}
      >
        <InnerForm>
          {({ register, formState, getFieldState }): JSX.Element => (
            <Fragment>
              <Input
                disabled={true}
                label={t("mailmap.entryEmail")}
                name={"entryEmail"}
                placeholder={t("mailmap.entryEmail")}
                register={register}
              />
              <Input
                disabled={true}
                label={t("mailmap.currentSubentryName")}
                name={"oldSubentryName"}
                placeholder={t("mailmap.subentryName")}
                register={register}
              />
              <Input
                disabled={true}
                label={t("mailmap.currentSubentryEmail")}
                name={"oldSubentryEmail"}
                placeholder={t("mailmap.subentryEmail")}
                register={register}
              />
              <Input
                error={formState.errors.newSubentryName?.message?.toString()}
                isTouched={getFieldState("newSubentryName").isTouched}
                isValid={!getFieldState("newSubentryName").invalid}
                label={t("mailmap.newSubentryName")}
                name={"newSubentryName"}
                placeholder={t("mailmap.subentryName")}
                register={register}
              />
              <Input
                error={formState.errors.newSubentryEmail?.message?.toString()}
                isTouched={getFieldState("newSubentryEmail").isTouched}
                isValid={!getFieldState("newSubentryEmail").invalid}
                label={t("mailmap.newSubentryEmail")}
                name={"newSubentryEmail"}
                placeholder={t("mailmap.subentryEmail")}
                register={register}
              />
            </Fragment>
          )}
        </InnerForm>
      </Form>
    </Modal>
  );
};

export { UpdateMailmapSubentryModal };
