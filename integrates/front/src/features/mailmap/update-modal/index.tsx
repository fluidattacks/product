import { Form, InnerForm, Input, Modal } from "@fluidattacks/design";
import { Fragment } from "react";
import { useTranslation } from "react-i18next";
import { object, string } from "yup";

import { AUTHOR_NAME_REGEX, MAX_LENGTH, emailRegex } from "../create-modal";
import type { IUpdateMailmapEntryModalProps } from "../types";

const UpdateMailmapEntryModal: React.FC<IUpdateMailmapEntryModalProps> = ({
  onClose,
  onSubmit,
  entryEmail,
  entryName,
  modalRef,
}: IUpdateMailmapEntryModalProps): JSX.Element => {
  const { t } = useTranslation();

  return (
    <Modal
      modalRef={{ ...modalRef, close: onClose }}
      size={"sm"}
      title={t("mailmap.updateMailmapEntry")}
    >
      <Form
        cancelButton={{ onClick: onClose }}
        confirmButton={{ label: t("mailmap.updateEntry") }}
        defaultDisabled={false}
        defaultValues={{
          newEntryEmail: entryEmail,
          newEntryName: entryName,
          oldEntryEmail: entryEmail,
        }}
        onSubmit={onSubmit}
        yupSchema={object().shape({
          newEntryEmail: string()
            .matches(emailRegex, t("validations.email"))
            .max(MAX_LENGTH, t("validations.maxLength", { count: MAX_LENGTH }))
            .required(),
          newEntryName: string()
            .max(MAX_LENGTH, t("validations.maxLength", { count: MAX_LENGTH }))
            .isValidTextField(undefined, AUTHOR_NAME_REGEX)
            .required(),
        })}
      >
        <InnerForm>
          {({ formState, getFieldState, register }): JSX.Element => (
            <Fragment>
              <Input
                disabled={true}
                label={t("mailmap.currentEntryEmail")}
                name={"oldEntryEmail"}
                placeholder={t("mailmap.entryEmail")}
                register={register}
              />
              <Input
                error={formState.errors.newEntryName?.message?.toString()}
                isTouched={getFieldState("newEntryName").isTouched}
                isValid={!getFieldState("newEntryName").invalid}
                label={t("mailmap.newEntryName")}
                name={"newEntryName"}
                placeholder={t("mailmap.entryName")}
                register={register}
              />
              <Input
                error={formState.errors.newEntryEmail?.message?.toString()}
                isTouched={getFieldState("newEntryEmail").isTouched}
                isValid={!getFieldState("newEntryEmail").invalid}
                label={t("mailmap.newEntryEmail")}
                name={"newEntryEmail"}
                placeholder={t("mailmap.entryEmail")}
                register={register}
              />
            </Fragment>
          )}
        </InnerForm>
      </Form>
    </Modal>
  );
};

export { UpdateMailmapEntryModal };
