import { useQuery } from "@apollo/client";
import { Alert, Form, InnerForm, Input, Modal } from "@fluidattacks/design";
import type { ColumnDef } from "@tanstack/react-table";
import type { GraphQLError } from "graphql";
import { useMemo, useState } from "react";
import * as React from "react";
import { Fragment } from "react";
import { useTranslation } from "react-i18next";

import type {
  IConvertMailmapSubentriesToNewEntryModalProps,
  IMailmapSubentry,
} from "../types";
import { Table } from "components/table";
import { useTable } from "hooks";
import { GET_MAILMAP_ENTRY_SUBENTRIES_QUERY } from "pages/organization/mailmap/queries";
import { Logger } from "utils/logger";
import { msgError } from "utils/notifications";

const ConvertMailmapSubentriesToNewEntryModal: React.FC<
  IConvertMailmapSubentriesToNewEntryModalProps
> = ({
  onClose,
  onSubmit,
  entryEmail,
  subentryEmail,
  subentryName,
  organizationId,
  modalRef,
}: IConvertMailmapSubentriesToNewEntryModalProps): JSX.Element => {
  const { t } = useTranslation();

  const tableRef = useTable(`tblMailmapConvertSubentriesModal-${entryEmail}`);

  const [selectedSubentries, setSelectedSubentries] = useState<
    IMailmapSubentry[]
  >([]);

  const columns = useMemo(
    (): ColumnDef<IMailmapSubentry>[] => [
      {
        accessorKey: "mailmapSubentryName",
        header: t("mailmap.subentryName"),
      },
      {
        accessorKey: "mailmapSubentryEmail",
        header: t("mailmap.subentryEmail"),
      },
    ],
    [t],
  );

  const { data } = useQuery(GET_MAILMAP_ENTRY_SUBENTRIES_QUERY, {
    fetchPolicy: "cache-first",
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error: GraphQLError): void => {
        if (error.message.startsWith("Exception - Mailmap entry not found:")) {
          msgError(
            t("mailmap.errorMailmapEntryNotFound"),
            t("mailmap.errorTitle"),
          );
          Logger.error("Exception - Mailmap entry not found:", error);
        } else {
          msgError(
            t("mailmap.errorGetMailmapSubentries"),
            t("mailmap.errorTitle"),
          );
          Logger.error(
            `Error loading mailmap subentries for ${entryEmail}:`,
            error,
          );
        }
      });
    },
    variables: {
      entryEmail,
      organizationId,
    },
  });

  const subentries: IMailmapSubentry[] =
    data?.mailmapEntrySubentries?.map(
      (subentry: IMailmapSubentry): IMailmapSubentry => ({
        mailmapSubentryEmail: subentry.mailmapSubentryEmail,
        mailmapSubentryName: subentry.mailmapSubentryName,
      }),
    ) ?? [];

  // Filter out selected subentry (main subentry)
  const filteredSubentries = subentries.filter(
    (subentry: IMailmapSubentry): boolean =>
      !(
        subentry.mailmapSubentryEmail === subentryEmail &&
        subentry.mailmapSubentryName === subentryName
      ),
  );

  const table = (
    <Table
      columns={columns}
      data={filteredSubentries}
      options={{ enableSearchBar: false }}
      rowSelectionSetter={setSelectedSubentries}
      rowSelectionState={selectedSubentries}
      tableRef={tableRef}
    />
  );

  const noAvailableSubentries = filteredSubentries.length === 0;

  return (
    <Modal
      modalRef={modalRef}
      size={"sm"}
      title={t("mailmap.convertMailmapSubentriesToNewEntry")}
    >
      <Form
        cancelButton={{ onClick: onClose }}
        confirmButton={{ label: t("mailmap.convertSubentriesToNewEntry") }}
        defaultDisabled={noAvailableSubentries}
        defaultValues={{
          entryEmail,
          mainSubentry: {
            mailmapSubentryEmail: subentryEmail,
            mailmapSubentryName: subentryName,
          },
          otherSubentries: [],
        }}
        onSubmit={onSubmit}
      >
        <InnerForm>
          {({ register }): JSX.Element => (
            <Fragment>
              <Input
                disabled={true}
                label={t("mailmap.convertNewEntryName")}
                name={"mainSubentry.mailmapSubentryName"}
                placeholder={t("mailmap.subentryName")}
                register={register}
              />
              <Input
                disabled={true}
                label={t("mailmap.convertNewEntryEmail")}
                name={"mainSubentry.mailmapSubentryEmail"}
                placeholder={t("mailmap.subentryEmail")}
                register={register}
              />
              {noAvailableSubentries ? (
                <Alert>
                  {t("mailmap.alertNoAvailableSubentriesToConvert")}
                </Alert>
              ) : (
                <Alert variant={"info"}>
                  {t("mailmap.infoConvertSubentriesToNewEntry2")}
                </Alert>
              )}
              {table}
              {noAvailableSubentries ? undefined : (
                <Alert variant={"warning"}>
                  {t("mailmap.alertConvertSubentriesToNewEntry")}
                </Alert>
              )}
            </Fragment>
          )}
        </InnerForm>
      </Form>
    </Modal>
  );
};

export { ConvertMailmapSubentriesToNewEntryModal };
