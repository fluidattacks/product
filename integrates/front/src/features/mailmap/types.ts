import type { ApolloQueryResult } from "@apollo/client";
import type { IUseModal } from "@fluidattacks/design";

import type { Exact, MailmapRecordsQuery } from "gql/graphql";

// Mailmap Objects Component Props Type Interfaces

interface IMailmapEntry {
  mailmapEntryEmail: string;
  mailmapEntryName: string;
}

interface IMailmapSubentry {
  mailmapSubentryEmail: string;
  mailmapSubentryName: string;
}

interface IMailmapRecord {
  mailmapEntryEmail: string;
  mailmapEntryName: string;
  mailmapSubentryEmail: string;
  mailmapSubentryName: string;
}

// Mailmap Subentries Component Props Type Interface

interface IMailmapSubentriesProps {
  entriesEmails: string[];
  entryEmail: string;
  organizationId: string;
  refetchEntries: (
    variables?: Partial<Exact<Record<string, never>>>,
  ) => Promise<ApolloQueryResult<MailmapRecordsQuery>>;
}

// Form Values Type Interfaces

// Mailmap Entry Operations Form Values

interface ICreateMailmapEntryFormValues {
  entryEmail: string;
  entryName: string;
}

interface IDeleteMailmapEntryFormValues {
  entryEmail: string;
}

interface IUpdateMailmapEntryFormValues {
  oldEntryEmail: string;
  newEntryEmail: string;
  newEntryName: string;
}

interface ISetMailmapEntryAsMailmapSubentryFormValues {
  entryEmail: string;
  targetEntryEmail: string;
}

interface IMoveMailmapEntryWithSubentriesFormValues {
  entryEmail: string;
  newOrganizationId: string;
}

// Mailmap Import Operations Form Values

interface IImportMailmapFormValues {
  file: FileList;
}

// Mailmap Subentry Operations Form Values

interface ICreateMailmapSubentryFormValues {
  subentryEmail: string;
  subentryName: string;
}

interface IDeleteMailmapSubentryFormValues {
  subentryEmail: string;
  subentryName: string;
}

interface IUpdateMailmapSubentryFormValues {
  oldSubentryEmail: string;
  oldSubentryName: string;
  newSubentryEmail: string;
  newSubentryName: string;
}

interface ISetMailmapSubentryAsMailmapEntryFormValues {
  subentryEmail: string;
  subentryName: string;
}

interface IConvertMailmapSubentryToNewEntryFormValues {
  entryEmail: string;
  mainSubentry: IMailmapSubentry;
  otherSubentries: IMailmapSubentry[];
}

interface ICreateMailmapEntryModalProps {
  readonly modalRef: IUseModal;
  readonly onClose: () => void;
  readonly onSubmit: (values: ICreateMailmapEntryFormValues) => Promise<void>;
}

// Import Mailmap Form Props Type Interface

interface IImportMailmapFormProps {
  readonly onClose: () => void;
  readonly submitting: boolean;
}

// Mailmap Entry Operations Modal Props

interface IDeleteMailmapEntryModalProps {
  readonly onClose: () => void;
  readonly onSubmit: (values: IDeleteMailmapEntryFormValues) => Promise<void>;
  entryEmail: string;
  readonly modalRef: IUseModal;
}

interface IUpdateMailmapEntryModalProps {
  readonly onClose: () => void;
  readonly onSubmit: (values: IUpdateMailmapEntryFormValues) => Promise<void>;
  entryEmail: string;
  entryName: string;
  readonly modalRef: IUseModal;
}

interface ISetMailmapEntryAsMailmapSubentryModalProps {
  readonly onClose: () => void;
  readonly onSubmit: (
    values: ISetMailmapEntryAsMailmapSubentryFormValues,
  ) => Promise<void>;
  entryEmail: string;
  readonly modalRef: IUseModal;
}

interface IMoveMailmapEntryWithSubentriesModalProps {
  readonly onClose: () => void;
  readonly onSubmit: (
    values: IMoveMailmapEntryWithSubentriesFormValues,
  ) => Promise<void>;
  entryEmail: string;
  readonly modalRef: IUseModal;
}

// Import Mailmap Operations Modal Props

interface IImportMailmapModalProps {
  readonly onClose: () => void;
  readonly onSubmit: (values: IImportMailmapFormValues) => Promise<void>;
  readonly modalRef: IUseModal;
  readonly submitting: boolean;
}

// Mailmap Subentry Operations Modal Props

interface ICreateMailmapSubentryModalProps {
  readonly onClose: () => void;
  readonly onSubmit: (
    values: ICreateMailmapSubentryFormValues,
  ) => Promise<void>;
  entryEmail: string;
  readonly modalRef: IUseModal;
}

interface IDeleteMailmapSubentryModalProps {
  readonly onClose: () => void;
  readonly onSubmit: (
    values: IDeleteMailmapSubentryFormValues,
  ) => Promise<void>;
  entryEmail: string;
  subentryEmail: string;
  subentryName: string;
  readonly modalRef: IUseModal;
}

interface IUpdateMailmapSubentryModalProps {
  readonly onClose: () => void;
  readonly onSubmit: (
    values: IUpdateMailmapSubentryFormValues,
  ) => Promise<void>;
  entryEmail: string;
  subentryEmail: string;
  subentryName: string;
  readonly modalRef: IUseModal;
}

interface ISetMailmapSubentryAsMailmapEntryModalProps {
  readonly onClose: () => void;
  readonly onSubmit: (
    values: ISetMailmapSubentryAsMailmapEntryFormValues,
  ) => Promise<void>;
  entryEmail: string;
  subentryEmail: string;
  subentryName: string;
  readonly modalRef: IUseModal;
}

interface IConvertMailmapSubentriesToNewEntryModalProps {
  readonly onClose: () => void;
  readonly onSubmit: (
    values: IConvertMailmapSubentryToNewEntryFormValues,
  ) => Promise<void>;
  entryEmail: string;
  organizationId: string;
  subentryEmail: string;
  subentryName: string;
  readonly modalRef: IUseModal;
}

export type {
  IMailmapEntry,
  IMailmapSubentry,
  IMailmapRecord,
  IMailmapSubentriesProps,
  ICreateMailmapEntryFormValues,
  IDeleteMailmapEntryFormValues,
  IUpdateMailmapEntryFormValues,
  ISetMailmapEntryAsMailmapSubentryFormValues,
  IMoveMailmapEntryWithSubentriesFormValues,
  IImportMailmapFormValues,
  ICreateMailmapSubentryFormValues,
  IDeleteMailmapSubentryFormValues,
  IUpdateMailmapSubentryFormValues,
  ISetMailmapSubentryAsMailmapEntryFormValues,
  IConvertMailmapSubentryToNewEntryFormValues,
  IImportMailmapFormProps,
  ICreateMailmapEntryModalProps,
  IDeleteMailmapEntryModalProps,
  IUpdateMailmapEntryModalProps,
  ISetMailmapEntryAsMailmapSubentryModalProps,
  IMoveMailmapEntryWithSubentriesModalProps,
  ICreateMailmapSubentryModalProps,
  IDeleteMailmapSubentryModalProps,
  IUpdateMailmapSubentryModalProps,
  ISetMailmapSubentryAsMailmapEntryModalProps,
  IImportMailmapModalProps,
  IConvertMailmapSubentriesToNewEntryModalProps,
};
