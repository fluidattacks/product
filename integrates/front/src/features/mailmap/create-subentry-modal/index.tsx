import { Alert, Form, InnerForm, Input, Modal } from "@fluidattacks/design";
import * as React from "react";
import { Fragment } from "react";
import { useTranslation } from "react-i18next";
import { object, string } from "yup";

import { AUTHOR_NAME_REGEX, MAX_LENGTH, emailRegex } from "../create-modal";
import type {
  ICreateMailmapSubentryFormValues,
  ICreateMailmapSubentryModalProps,
} from "../types";
import { getDomain } from "pages/organization/mailmap/utils";

const CreateMailmapSubentryModal: React.FC<
  ICreateMailmapSubentryModalProps
> = ({ modalRef, onClose, onSubmit, entryEmail }): JSX.Element => {
  const { t } = useTranslation();

  return (
    <Modal
      modalRef={modalRef}
      size={"sm"}
      title={t("mailmap.createMailmapSubentry")}
    >
      <Form
        cancelButton={{ onClick: onClose }}
        confirmButton={{ label: t("mailmap.createSubentry") }}
        defaultValues={{ entryEmail, subentryEmail: "", subentryName: "" }}
        onSubmit={onSubmit}
        yupSchema={object().shape({
          subentryEmail: string()
            .matches(emailRegex, t("validations.email"))
            .max(MAX_LENGTH, t("validations.maxLength", { count: MAX_LENGTH }))
            .required(),
          subentryName: string()
            .max(MAX_LENGTH, t("validations.maxLength", { count: MAX_LENGTH }))
            .isValidTextField(undefined, AUTHOR_NAME_REGEX)
            .required(),
        })}
      >
        <InnerForm<ICreateMailmapSubentryFormValues>>
          {({ register, watch, formState, getFieldState }): JSX.Element => {
            const subentryEmail = watch("subentryEmail");

            return (
              <Fragment>
                <Input
                  disabled={true}
                  label={t("mailmap.entryEmail")}
                  name={"entryEmail"}
                  placeholder={t("mailmap.entryEmail")}
                  register={register}
                  watch={watch}
                />
                <Input
                  error={formState.errors.subentryName?.message?.toString()}
                  isTouched={getFieldState("subentryName").isTouched}
                  isValid={!getFieldState("subentryName").invalid}
                  label={t("mailmap.subentryName")}
                  name={"subentryName"}
                  placeholder={t("mailmap.subentryName")}
                  register={register}
                  watch={watch}
                />
                <Input
                  error={formState.errors.subentryEmail?.message?.toString()}
                  isTouched={getFieldState("subentryEmail").isTouched}
                  isValid={!getFieldState("subentryEmail").invalid}
                  label={t("mailmap.subentryEmail")}
                  name={"subentryEmail"}
                  placeholder={t("mailmap.subentryEmail")}
                  register={register}
                  watch={watch}
                />
                {subentryEmail &&
                entryEmail &&
                getDomain(entryEmail) !== getDomain(subentryEmail) ? (
                  <Alert variant={"warning"}>
                    {t("mailmap.alertDomainMismatch")}
                  </Alert>
                ) : undefined}
              </Fragment>
            );
          }}
        </InnerForm>
      </Form>
    </Modal>
  );
};

export { CreateMailmapSubentryModal };
