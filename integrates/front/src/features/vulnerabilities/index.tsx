import { useAbility } from "@casl/react";
import type { Row } from "@tanstack/react-table";
import _ from "lodash";
import mixpanel from "mixpanel-browser";
import {
  useCallback,
  useContext,
  useEffect,
  useMemo,
  useRef,
  useState,
} from "react";
import React from "react";
import type { FormEvent } from "react";
import { useLocation, useNavigate, useParams } from "react-router-dom";

import type {
  IVulnComponentProps,
  IVulnRowAttr,
  IVulnStateProps,
} from "./types";
import {
  filterOutVulnerabilities,
  formatVulnerabilities,
  getNonSelectableVulnerabilitiesOnApproveSeverityUpdate,
  getNonSelectableVulnerabilitiesOnCloseIds,
  getNonSelectableVulnerabilitiesOnDeleteIds,
  getNonSelectableVulnerabilitiesOnReattackIds,
  getNonSelectableVulnerabilitiesOnResubmitIds,
  getNonSelectableVulnerabilitiesOnVerifyIds,
} from "./utils";
import { VulnerabilityModal } from "./vulnerability-modal";

import { Table } from "components/table";
import type { ICsvConfig, ITableOptions } from "components/table/types";
import { allGroupContext } from "context/authz/all-group-permissions-provider";
import { authzPermissionsContext } from "context/authz/config";
import { useTable, useVulnerabilityStore } from "hooks";

function usePreviousProps(value: boolean): boolean {
  const ref = useRef(false);
  useEffect((): void => {
    // eslint-disable-next-line functional/immutable-data
    ref.current = value;
  });

  return ref.current;
}

export const VulnComponent: React.FC<IVulnComponentProps<IVulnRowAttr>> = ({
  columns,
  csvConfig = {} as ICsvConfig,
  cvssVersion,
  extraButtons = undefined,
  extraBelowButtons = undefined,
  filters = undefined,
  shouldHideRelease,
  nzrFetchMoreData = undefined,
  refetchData,
  tableOptions = {} as ITableOptions<IVulnRowAttr>,
  tableRefProps,
  nonValidOnReattackVulns = undefined,
  vulnerabilities,
  onNextPage = undefined,
  vulnStateProps = {} as IVulnStateProps,
  onSearch = (): void => undefined,
  onVulnSelect = (): void => undefined,
  loading = false,
}: IVulnComponentProps<IVulnRowAttr>): JSX.Element => {
  const { vulnerabilityId: vulnId } = useParams() as {
    vulnerabilityId: string | undefined;
  };
  const { pathname } = useLocation();
  const { changePermissions } = useContext(allGroupContext);
  const url = pathname.split("/").slice(0, -1).join("/");
  const navigate = useNavigate();
  const permissions = useAbility(authzPermissionsContext);

  const {
    isApprovingSeverityUpdate,
    isClosing,
    isDeleting,
    isEditing,
    isReattacking,
    isResubmitting,
    isVerifying,
    isUpdatingSeverity,
  } = useVulnerabilityStore();

  const { findingState = "VULNERABLE", isFindingReleased = true } =
    vulnStateProps;

  const canResubmitVulnerabilities = permissions.can(
    "integrates_api_mutations_resubmit_vulnerabilities_mutate",
  );

  const tableRef = useTable(
    `vulnerabilitiesTable-${tableRefProps.id}`,
    tableRefProps.columnVisibility,
    tableRefProps.columnOrder,
    tableRefProps.columnPinning,
  );

  const [selectedVulnerabilities, setSelectedVulnerabilities] = useState<
    IVulnRowAttr[]
  >([]);
  const [vulnerabilityId, setVulnerabilityId] = useState(vulnId ?? "");
  const [isVulnerabilityModalOpen, setIsVulnerabilityModalOpen] =
    useState(false);
  const [currentRow, setCurrentRow] = useState<IVulnRowAttr>();

  const previousStates = {
    isApprovingSeverityUpdate: usePreviousProps(isApprovingSeverityUpdate),
    isClosing: usePreviousProps(isClosing),
    isDeleting: usePreviousProps(isDeleting),
    isEditing: usePreviousProps(isEditing),
    isReattacking: usePreviousProps(isReattacking),
    isResubmitting: usePreviousProps(isResubmitting),
    isUpdatingSeverity: usePreviousProps(isUpdatingSeverity),
    isVerifying: usePreviousProps(isVerifying),
  };

  const openAdditionalInfoModal = useCallback(
    (rowInfo: Row<IVulnRowAttr>): ((event: FormEvent) => void) => {
      return (event: FormEvent): void => {
        if (!_.isUndefined(changePermissions)) {
          changePermissions(rowInfo.original.groupName);
        }
        navigate(`${pathname}/${rowInfo.original.id}`, { replace: true });
        setVulnerabilityId(rowInfo.original.id);
        setCurrentRow(rowInfo.original);
        mixpanel.track("ViewVulnerability", {
          groupName: rowInfo.original.groupName,
        });
        setIsVulnerabilityModalOpen(true);
        event.stopPropagation();
      };
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [pathname, changePermissions],
  );

  const closeAdditionalInfoModal = useCallback((): void => {
    navigate(url, { replace: true });
    setVulnerabilityId("");
    setIsVulnerabilityModalOpen(false);
  }, [navigate, url]);

  const clearSelectedVulns = useCallback((): void => {
    setSelectedVulnerabilities([]);
  }, []);

  const validateState = (): boolean => {
    return (
      (previousStates.isApprovingSeverityUpdate &&
        !isApprovingSeverityUpdate) ||
      (previousStates.isReattacking && !isReattacking) ||
      (previousStates.isVerifying && !isVerifying) ||
      (previousStates.isEditing && !isEditing) ||
      (previousStates.isUpdatingSeverity && !isUpdatingSeverity) ||
      (previousStates.isDeleting && !isDeleting) ||
      (previousStates.isResubmitting && !isResubmitting) ||
      (previousStates.isClosing && !isClosing)
    );
  };

  function onVulnSelection(): void {
    function applyFiltersUpdater(
      currentVulnerabilities: IVulnRowAttr[],
      filterFunction: (
        currentVulnerabilities: IVulnRowAttr[],
      ) => IVulnRowAttr[],
    ): IVulnRowAttr[] {
      return filterFunction(currentVulnerabilities);
    }

    function selectVulnerabilities(newVulnerabilities: IVulnRowAttr[]): void {
      setSelectedVulnerabilities(newVulnerabilities);
      onVulnSelect(newVulnerabilities, clearSelectedVulns);
    }
    if (validateState()) {
      selectVulnerabilities([]);
    }

    if (
      !previousStates.isApprovingSeverityUpdate &&
      isApprovingSeverityUpdate
    ) {
      const filteredVulnerabilities = applyFiltersUpdater(
        selectedVulnerabilities,
        (currVuln: IVulnRowAttr[]): IVulnRowAttr[] => {
          return filterOutVulnerabilities(
            currVuln,
            vulnerabilities,
            getNonSelectableVulnerabilitiesOnApproveSeverityUpdate,
          );
        },
      );

      selectVulnerabilities(filteredVulnerabilities);
    }

    if (!previousStates.isReattacking && isReattacking) {
      const nonValidIds: string[] =
        nonValidOnReattackVulns?.map((vuln): string => vuln.id) ?? [];
      const filteredVulnerabilities = applyFiltersUpdater(
        selectedVulnerabilities,
        (currVuln: IVulnRowAttr[]): IVulnRowAttr[] => {
          return filterOutVulnerabilities(
            currVuln,
            vulnerabilities,
            getNonSelectableVulnerabilitiesOnReattackIds,
          ).filter(
            (vuln: IVulnRowAttr): boolean => !nonValidIds.includes(vuln.id),
          );
        },
      );

      selectVulnerabilities(filteredVulnerabilities);
    }

    if (!previousStates.isResubmitting && isResubmitting) {
      const filteredVulnerabilities = applyFiltersUpdater(
        selectedVulnerabilities,
        (currVuln: IVulnRowAttr[]): IVulnRowAttr[] => {
          return filterOutVulnerabilities(
            currVuln,
            vulnerabilities,
            getNonSelectableVulnerabilitiesOnResubmitIds,
          );
        },
      );

      selectVulnerabilities(filteredVulnerabilities);
    }

    if (!previousStates.isClosing && isClosing) {
      const filteredVulnerabilities = applyFiltersUpdater(
        selectedVulnerabilities,
        (currVuln: IVulnRowAttr[]): IVulnRowAttr[] => {
          return filterOutVulnerabilities(
            currVuln,
            vulnerabilities,
            getNonSelectableVulnerabilitiesOnCloseIds,
          );
        },
      );

      selectVulnerabilities(filteredVulnerabilities);
    }
    if (!previousStates.isDeleting && isDeleting) {
      const filteredVulnerabilities = applyFiltersUpdater(
        selectedVulnerabilities,
        (currVuln: IVulnRowAttr[]): IVulnRowAttr[] => {
          return filterOutVulnerabilities(
            currVuln,
            vulnerabilities,
            getNonSelectableVulnerabilitiesOnDeleteIds,
            true,
            shouldHideRelease,
          );
        },
      );

      selectVulnerabilities(filteredVulnerabilities);
    }

    if (!previousStates.isVerifying && isVerifying) {
      const filteredVulnerabilities = applyFiltersUpdater(
        selectedVulnerabilities,
        (currVuln: IVulnRowAttr[]): IVulnRowAttr[] => {
          return filterOutVulnerabilities(
            currVuln,
            vulnerabilities,
            getNonSelectableVulnerabilitiesOnVerifyIds,
          );
        },
      );

      selectVulnerabilities(filteredVulnerabilities);
    }
    onVulnSelect(selectedVulnerabilities, clearSelectedVulns);
  }
  // Annotation needed as adding the dependencies creates a memory leak
  // eslint-disable-next-line react-hooks/exhaustive-deps
  useEffect(onVulnSelection, [
    selectedVulnerabilities,
    onVulnSelect,
    isApprovingSeverityUpdate,
    isClosing,
    isDeleting,
    isEditing,
    isReattacking,
    isResubmitting,
    isVerifying,
    isUpdatingSeverity,
  ]);

  const findingId: string = useMemo(
    (): string => (currentRow === undefined ? "" : currentRow.findingId),
    [currentRow],
  );
  const groupName: string = useMemo(
    (): string => (currentRow === undefined ? "" : currentRow.groupName),
    [currentRow],
  );

  const orderedVulns: IVulnRowAttr[] = _.orderBy(
    formatVulnerabilities({
      vulnerabilities,
    }),
    ["state", "priority"],
    ["desc", "desc"],
  );

  useEffect((): void => {
    if (vulnerabilityId !== "" && orderedVulns.length > 0) {
      const rowInfo: IVulnRowAttr | undefined = orderedVulns.find(
        (item): boolean => item.id === vulnerabilityId,
      );
      if (_.isUndefined(rowInfo)) {
        if (!_.isUndefined(nzrFetchMoreData)) nzrFetchMoreData();
      } else {
        if (!_.isUndefined(changePermissions)) {
          changePermissions(rowInfo.groupName);
        }
        setCurrentRow(rowInfo);
        mixpanel.track("ViewVulnerability", {
          groupName: rowInfo.groupName,
        });
        setIsVulnerabilityModalOpen(true);
      }
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [vulnerabilityId, orderedVulns.length]);

  return (
    <React.StrictMode>
      <Table
        columns={columns}
        csvConfig={csvConfig}
        data={orderedVulns}
        dataPrivate={false}
        extraBelowButtons={extraBelowButtons}
        extraButtons={extraButtons}
        filters={filters}
        loadingData={loading}
        onNextPage={onNextPage}
        onRowClick={openAdditionalInfoModal}
        onSearch={onSearch}
        options={tableOptions}
        rowSelectionSetter={
          (isFindingReleased && findingState !== "SAFE") ||
          canResubmitVulnerabilities
            ? setSelectedVulnerabilities
            : undefined
        }
        rowSelectionState={selectedVulnerabilities}
        tableRef={tableRef}
      />
      {vulnerabilityId !== "" && currentRow ? (
        <VulnerabilityModal
          clearSelectedVulns={clearSelectedVulns}
          closeModal={closeAdditionalInfoModal}
          currentRow={currentRow}
          cvssVersion={cvssVersion}
          findingId={findingId}
          groupName={groupName}
          isFindingReleased={isFindingReleased}
          isModalOpen={isVulnerabilityModalOpen}
          refetchData={refetchData}
        />
      ) : null}
    </React.StrictMode>
  );
};
