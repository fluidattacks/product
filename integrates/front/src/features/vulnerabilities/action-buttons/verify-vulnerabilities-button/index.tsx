import { Button } from "@fluidattacks/design";
import type { IconName } from "@fortawesome/free-solid-svg-icons";
import { Fragment, useCallback } from "react";
import * as React from "react";
import { useTranslation } from "react-i18next";

import { Authorize } from "components/@core/authorize";
import { useVulnerabilityStore } from "hooks/use-vulnerability-store";

interface IVerifyVulnerabilitiesButtonProps {
  readonly areVulnsSelected: boolean;
  readonly full?: boolean;
  readonly shouldRender: boolean;
  readonly onVerify: () => void;
}

const VerifyVulnerabilitiesButton: React.FC<
  IVerifyVulnerabilitiesButtonProps
> = ({ areVulnsSelected, full, shouldRender, onVerify }): JSX.Element => {
  const { t } = useTranslation();
  const { isVerifying, setIsVerifyModalOpen, setIsVerifying } =
    useVulnerabilityStore();

  const verifyText = t("searchFindings.tabDescription.markVerified.text");
  const cancelText = t("searchFindings.tabDescription.cancelVerified");

  const toggleModal = useCallback((): void => {
    setIsVerifyModalOpen(true);
  }, [setIsVerifyModalOpen]);

  const onVerifyVulnerabilities = useCallback((): void => {
    setIsVerifying(!isVerifying);
    if (!isVerifying) {
      onVerify();
    }
  }, [isVerifying, onVerify, setIsVerifying]);

  const buttonAttributes = useCallback((): {
    buttonIcon: IconName;
    buttonId: string;
    buttonText: string;
  } => {
    if (isVerifying) {
      return {
        buttonIcon: "times",
        buttonId: "verify-cancel",
        buttonText: cancelText,
      };
    }

    return {
      buttonIcon: "check",
      buttonId: "verify",
      buttonText: verifyText,
    };
  }, [cancelText, isVerifying, verifyText]);

  const { buttonIcon, buttonId, buttonText } = buttonAttributes();

  return (
    <Authorize
      can={"integrates_api_mutations_verify_vulnerabilities_request_mutate"}
      have={"can_report_vulnerabilities"}
    >
      <Fragment>
        {isVerifying ? (
          <Button
            disabled={!areVulnsSelected}
            icon={"check"}
            id={"verify"}
            mr={0.5}
            onClick={toggleModal}
            tooltip={t("searchFindings.tabDescription.markVerified.tooltip")}
            variant={"ghost"}
          >
            {verifyText}
          </Button>
        ) : undefined}
        {shouldRender ? (
          <Button
            icon={buttonIcon}
            id={buttonId}
            mr={0.5}
            onClick={onVerifyVulnerabilities}
            tooltip={
              isVerifying
                ? undefined
                : t("searchFindings.tabDescription.markVerified.tooltip")
            }
            variant={"ghost"}
            width={full === true ? "100%" : undefined}
          >
            {buttonText}
          </Button>
        ) : undefined}
      </Fragment>
    </Authorize>
  );
};

export { VerifyVulnerabilitiesButton };
