import { Button } from "@fluidattacks/design";
import type { IconName } from "@fortawesome/free-solid-svg-icons";
import { Fragment, useCallback } from "react";
import * as React from "react";
import { useTranslation } from "react-i18next";

import { Authorize } from "components/@core/authorize";
import { useVulnerabilityStore } from "hooks/use-vulnerability-store";

interface IResubmitVulnerabilitiesButtonProps {
  full?: boolean;
  shouldRender: boolean;
  areVulnsSelected: boolean;
  onResubmit: () => void;
}

export const ResubmitVulnerabilitiesButton: React.FC<
  IResubmitVulnerabilitiesButtonProps
> = ({
  areVulnsSelected,
  full,
  shouldRender,
  onResubmit,
}: Readonly<IResubmitVulnerabilitiesButtonProps>): JSX.Element => {
  const { t } = useTranslation();
  const { isResubmitting, setIsResubmitting } = useVulnerabilityStore();

  const onResubmitVulnerabilities = useCallback((): void => {
    setIsResubmitting(!isResubmitting);
    if (!isResubmitting) {
      onResubmit();
    }
  }, [isResubmitting, onResubmit, setIsResubmitting]);

  const buttonAttributes = useCallback((): {
    buttonIcon: IconName;
    buttonId: string;
    buttonText: string;
  } => {
    if (isResubmitting) {
      return {
        buttonIcon: "times",
        buttonId: "resubmit-cancel",
        buttonText: t("searchFindings.tabVuln.buttons.cancel"),
      };
    }

    return {
      buttonIcon: "check",
      buttonId: "resubmit",
      buttonText: t("searchFindings.tabVuln.buttons.resubmit"),
    };
  }, [isResubmitting, t]);

  const { buttonIcon, buttonId, buttonText } = buttonAttributes();

  return (
    <Authorize
      can={"integrates_api_mutations_resubmit_vulnerabilities_mutate"}
      have={"can_report_vulnerabilities"}
    >
      <Fragment>
        {isResubmitting ? (
          <Button
            disabled={!areVulnsSelected}
            icon={"check"}
            id={"resubmit"}
            mr={0.5}
            onClick={onResubmit}
            tooltip={t("searchFindings.tabVuln.buttonsTooltip.resubmit")}
            variant={"ghost"}
          >
            {t("searchFindings.tabVuln.buttons.resubmit")}
          </Button>
        ) : undefined}
        {shouldRender ? (
          <Button
            icon={buttonIcon}
            id={buttonId}
            mr={0.5}
            onClick={onResubmitVulnerabilities}
            tooltip={
              isResubmitting
                ? undefined
                : t("searchFindings.tabVuln.buttonsTooltip.resubmit")
            }
            variant={"ghost"}
            width={full === true ? "100%" : undefined}
          >
            {buttonText}
          </Button>
        ) : undefined}
      </Fragment>
    </Authorize>
  );
};
