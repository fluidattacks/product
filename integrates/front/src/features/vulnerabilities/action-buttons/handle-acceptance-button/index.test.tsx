import { PureAbility } from "@casl/ability";
import { act, screen } from "@testing-library/react";
import { userEvent } from "@testing-library/user-event";

import { HandleAcceptanceButton } from ".";
import { authzPermissionsContext } from "context/authz/config";
import { useVulnerabilityStore } from "hooks/use-vulnerability-store";
import { render } from "mocks";

describe("handleAcceptanceButtons", (): void => {
  it("should render a component", async (): Promise<void> => {
    expect.hasAssertions();

    const handleAcceptanceButtonText =
      "searchFindings.tabVuln.buttons.handleAcceptance";
    const mockedPermissions = new PureAbility<string>([
      {
        action:
          "integrates_api_mutations_handle_vulnerabilities_acceptance_mutate",
      },
    ]);

    const { rerender } = render(
      <authzPermissionsContext.Provider value={mockedPermissions}>
        <HandleAcceptanceButton shouldRender={false} />
      </authzPermissionsContext.Provider>,
    );

    expect(screen.queryAllByText(handleAcceptanceButtonText)).toHaveLength(0);

    rerender(
      <authzPermissionsContext.Provider value={mockedPermissions}>
        <HandleAcceptanceButton shouldRender={true} />
      </authzPermissionsContext.Provider>,
    );

    act((): void => {
      useVulnerabilityStore.setState({ isAcceptanceModalOpen: false });
    });

    expect(screen.getByText(handleAcceptanceButtonText)).toBeInTheDocument();

    await userEvent.click(screen.getByText(handleAcceptanceButtonText));

    act((): void => {
      useVulnerabilityStore.setState({ isAcceptanceModalOpen: true });
    });
  });
});
