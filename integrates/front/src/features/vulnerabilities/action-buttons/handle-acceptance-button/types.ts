export interface IHandleAcceptanceButtonProps {
  areVulnsPendingOfAcceptance: boolean;
  areRequestedZeroRiskVulns: boolean;
  areSubmittedVulns: boolean;
}
