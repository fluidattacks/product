import { Button } from "@fluidattacks/design";
import { StrictMode, useCallback } from "react";
import { useTranslation } from "react-i18next";

import { useVulnerabilityStore } from "hooks/use-vulnerability-store";

const NotifyButton = ({
  shouldRender,
}: Readonly<{ shouldRender: boolean }>): JSX.Element | null => {
  const { t } = useTranslation();
  const { isNotify, setIsNotify } = useVulnerabilityStore();

  const onNotify = useCallback((): void => {
    setIsNotify(!isNotify);
  }, [isNotify, setIsNotify]);

  return shouldRender ? (
    <StrictMode>
      <Button
        icon={"paper-plane"}
        id={"vulnerabilities-notify"}
        mr={0.5}
        onClick={onNotify}
        tooltip={t("searchFindings.tabDescription.notify.tooltip")}
        variant={"ghost"}
      >
        {t("searchFindings.tabDescription.notify.text")}
      </Button>
    </StrictMode>
  ) : null;
};

export { NotifyButton };
