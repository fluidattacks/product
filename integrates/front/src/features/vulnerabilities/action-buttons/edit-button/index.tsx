import { Button } from "@fluidattacks/design";
import { StrictMode, useCallback } from "react";
import { useTranslation } from "react-i18next";

import { useVulnerabilityStore } from "hooks/use-vulnerability-store";

interface IEditButtonProps {
  isDisabled: boolean;
  shouldRender: boolean;
}

const EditButton = ({
  isDisabled,
  shouldRender,
}: Readonly<IEditButtonProps>): JSX.Element | null => {
  const { t } = useTranslation();
  const { isEditing, isReattacking, isVerifying, setIsEditing } =
    useVulnerabilityStore();

  const onEdit = useCallback((): void => {
    setIsEditing(!isEditing);
  }, [isEditing, setIsEditing]);

  return shouldRender ? (
    <StrictMode>
      <Button
        disabled={isReattacking || isVerifying || isDisabled}
        icon={"edit"}
        id={"vulnerabilities-edit"}
        onClick={onEdit}
        variant={"ghost"}
      >
        {isEditing
          ? t("searchFindings.tabDescription.save.text")
          : t("searchFindings.tabVuln.buttons.edit")}
      </Button>
    </StrictMode>
  ) : null;
};

export type { IEditButtonProps };
export { EditButton };
