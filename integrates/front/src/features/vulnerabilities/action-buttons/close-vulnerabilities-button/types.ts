import type { VulnerabilityStateReason } from "gql/graphql";

interface ICloseVulnerabilitiesButtonProps {
  shouldRender: boolean;
  areVulnsSelected: boolean;
  onClosing: (justification: VulnerabilityStateReason) => void;
}

export type { ICloseVulnerabilitiesButtonProps };
