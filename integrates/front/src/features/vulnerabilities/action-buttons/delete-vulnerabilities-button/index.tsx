import { Button } from "@fluidattacks/design";
import type { IconName } from "@fortawesome/free-solid-svg-icons";
import { Fragment, useCallback } from "react";
import * as React from "react";
import { useTranslation } from "react-i18next";

import type { IDeleteVulnerabilitiesButtonProps } from "./types";

import { Authorize } from "components/@core/authorize";
import { useVulnerabilityStore } from "hooks/use-vulnerability-store";

export const DeleteVulnerabilitiesButton: React.FC<
  IDeleteVulnerabilitiesButtonProps
> = ({ shouldRender, areVulnsSelected, full, onDeleting }): JSX.Element => {
  const { t } = useTranslation();
  const { isDeleting, setIsDeleteModalOpen, setIsDeleting } =
    useVulnerabilityStore();

  const toggleModal = useCallback((): void => {
    setIsDeleteModalOpen(true);
  }, [setIsDeleteModalOpen]);

  const onDeleteVulnerabilities = useCallback((): void => {
    setIsDeleting(!isDeleting);
    if (!isDeleting) {
      onDeleting();
    }
  }, [isDeleting, onDeleting, setIsDeleting]);

  const buttonAttributes = useCallback((): {
    buttonIcon: IconName;
    buttonId: string;
    buttonText: string;
    buttonTooltip?: string;
  } => {
    if (isDeleting) {
      return {
        buttonIcon: "times",
        buttonId: "delete-cancel",
        buttonText: t("searchFindings.tabVuln.buttons.cancel"),
      };
    }

    return {
      buttonIcon: "trash-alt",
      buttonId: "delete",
      buttonText: t("searchFindings.tabVuln.buttons.delete.text"),
      buttonTooltip: t("searchFindings.tabVuln.buttonsTooltip.delete"),
    };
  }, [isDeleting, t]);

  const { buttonIcon, buttonId, buttonText, buttonTooltip } =
    buttonAttributes();

  return (
    <Authorize
      can={"integrates_api_mutations_remove_vulnerability_mutate"}
      have={"can_report_vulnerabilities"}
    >
      <Fragment>
        {isDeleting ? (
          <Button
            disabled={!areVulnsSelected}
            icon={"trash-alt"}
            id={"delete"}
            mr={0.5}
            onClick={toggleModal}
            tooltip={t("searchFindings.tabVuln.buttons.delete.text")}
            variant={"ghost"}
          >
            {t("searchFindings.tabVuln.buttons.delete.text")}
          </Button>
        ) : undefined}
        {shouldRender ? (
          <Button
            icon={buttonIcon}
            id={buttonId}
            mr={0.5}
            onClick={onDeleteVulnerabilities}
            tooltip={buttonTooltip}
            variant={"ghost"}
            width={full === true ? "100%" : undefined}
          >
            {buttonText}
          </Button>
        ) : undefined}
      </Fragment>
    </Authorize>
  );
};
