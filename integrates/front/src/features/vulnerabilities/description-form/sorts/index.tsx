import { useTranslation } from "react-i18next";

import { Select } from "components/input";
import { Can } from "context/authz/can";

interface ISorts {
  isEditing: boolean;
}

const Sorts = ({ isEditing }: Readonly<ISorts>): JSX.Element | null => {
  const { t } = useTranslation();

  const sortItems = [
    {
      header: t("group.findings.boolean.True"),
      value: "YES",
    },
    {
      header: t("group.findings.boolean.False"),
      value: "NO",
    },
  ];

  return (
    <Can do={"integrates_api_mutations_update_finding_description_mutate"}>
      {isEditing ? (
        <Select
          items={sortItems}
          label={t("searchFindings.tabDescription.sorts.text")}
          name={"sorts"}
          tooltip={t("searchFindings.tabDescription.sorts.tooltip")}
          weight={"bold"}
        />
      ) : undefined}
    </Can>
  );
};

export { Sorts };
