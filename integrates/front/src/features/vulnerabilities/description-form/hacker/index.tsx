import { Container, Text } from "@fluidattacks/design";
import { useTranslation } from "react-i18next";
import { useTheme } from "styled-components";

import { Can } from "context/authz/can";

const Hacker = ({
  hacker,
}: Readonly<{ hacker?: string }>): JSX.Element | null => {
  const theme = useTheme();
  const { t } = useTranslation();

  if (hacker !== undefined) {
    return (
      <Can do={"integrates_api_resolvers_finding_hacker_resolve"}>
        <Container>
          <Text
            color={theme.palette.gray["800"]}
            fontWeight={"bold"}
            size={"sm"}
          >
            {t("searchFindings.tabDescription.hacker")}
          </Text>
          <Text color={theme.palette.gray["600"]} size={"md"}>
            {hacker}
          </Text>
        </Container>
      </Can>
    );
  }

  return null;
};

export { Hacker };
