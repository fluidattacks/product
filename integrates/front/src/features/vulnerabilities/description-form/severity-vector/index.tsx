import { Editable, Input } from "components/input";
import { Can } from "context/authz/can";
import { CVSS3_CALCULATOR_BASE_URL } from "utils/cvss";
import { CVSS4_CALCULATOR_BASE_URL } from "utils/cvss4";

interface ISeverityVector {
  isEditing: boolean;
  label: string;
  severityVector?: string;
  tooltip: string;
  variant: "v3" | "v4";
}

const SeverityVector = ({
  isEditing,
  label,
  severityVector,
  tooltip,
  variant,
}: Readonly<ISeverityVector>): JSX.Element | null => {
  const BASE_URL =
    variant === "v3" ? CVSS3_CALCULATOR_BASE_URL : CVSS4_CALCULATOR_BASE_URL;
  const inputName = variant === "v3" ? "severityVector" : "severityVectorV4";

  if (variant === "v4" && severityVector === undefined && !isEditing) {
    return null;
  }

  return (
    <Can
      do={"integrates_api_mutations_update_severity_mutate"}
      passThrough={true}
    >
      {(canEdit: boolean): JSX.Element => (
        <Editable
          currentValue={severityVector}
          externalLink={`${BASE_URL}#${severityVector}`}
          isEditing={isEditing ? canEdit : false}
          label={`${label}\n`}
        >
          <Input
            label={label}
            name={inputName}
            tooltip={tooltip}
            weight={"bold"}
          />
        </Editable>
      )}
    </Can>
  );
};

export { SeverityVector };
