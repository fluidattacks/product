import { Container, Link } from "@fluidattacks/design";
import defaultTo from "lodash/defaultTo";
import type { ReactElement } from "react";
import { Fragment } from "react";
import { useTranslation } from "react-i18next";

import { Checkbox } from "components/checkbox";
import { Label } from "components/input";
import { Can } from "context/authz/can";
import type { IUnfulfilledRequirement } from "pages/finding/description/types";

interface IRequirements {
  baseCriteriaUrl: string;
  criteriaBaseRequirements: IUnfulfilledRequirement[] | undefined;
  isEditing: boolean;
  track: (url: string) => () => void;
  unfulfilledRequirement: IUnfulfilledRequirement[];
}

const Requirements = ({
  baseCriteriaUrl,
  criteriaBaseRequirements,
  isEditing,
  track,
  unfulfilledRequirement,
}: Readonly<IRequirements>): JSX.Element | null => {
  const { t } = useTranslation();

  return (
    <Container mb={1}>
      <Label
        htmlFor={"requirementIds"}
        tooltip={
          isEditing
            ? t("searchFindings.tabDescription.requirements.tooltip")
            : undefined
        }
        weight={"bold"}
      >
        {t("searchFindings.tabDescription.requirements.text")}
      </Label>
      <Can
        do={"integrates_api_mutations_update_finding_description_mutate"}
        passThrough={true}
      >
        {(canEdit: boolean): JSX.Element =>
          isEditing && canEdit ? (
            <Fragment>
              {defaultTo(criteriaBaseRequirements, []).map(
                (requirement: IUnfulfilledRequirement): ReactElement => (
                  <Checkbox
                    id={requirement.id}
                    key={`requirementIds.${requirement.id}`}
                    label={t(`${requirement.id}. ${requirement.summary}`)}
                    name={"requirementIds"}
                    value={requirement.id}
                    variant={"formikField"}
                  />
                ),
              )}
            </Fragment>
          ) : (
            <Container>
              {unfulfilledRequirement.map(
                (requirement: IUnfulfilledRequirement): ReactElement => {
                  return (
                    <Container key={requirement.id}>
                      <Link
                        href={`${baseCriteriaUrl}-requirements-${requirement.id}`}
                        iconPosition={"hidden"}
                        onClick={track(
                          `${baseCriteriaUrl}-requirements-${requirement.id}`,
                        )}
                      >
                        {`${requirement.id}. ${requirement.summary}`}
                      </Link>
                    </Container>
                  );
                },
              )}
            </Container>
          )
        }
      </Can>
    </Container>
  );
};

export { Requirements };
