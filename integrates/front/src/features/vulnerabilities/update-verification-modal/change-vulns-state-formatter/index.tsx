import { Toggle } from "@fluidattacks/design";
import { useCallback } from "react";
import * as React from "react";

interface IChangeVulnStateFormatterProps {
  readonly row: Readonly<Record<string, string>>;
  readonly changeFunction: (arg1: Record<string, string>) => void;
}

const ChangeVulnStateFormatter: React.FC<IChangeVulnStateFormatterProps> = ({
  row,
  changeFunction,
}: IChangeVulnStateFormatterProps): JSX.Element => {
  const handleOnChange = useCallback((): void => {
    changeFunction(row);
  }, [changeFunction, row]);

  return (
    <Toggle
      defaultChecked={!("state" in row) || row.state !== "SAFE"}
      name={"vuln-state"}
      onChange={handleOnChange}
    />
  );
};

export const changeVulnStateFormatter = (
  row: Readonly<Record<string, string>>,
  changeFunction: (arg1: Record<string, string>) => void,
): JSX.Element => {
  return <ChangeVulnStateFormatter changeFunction={changeFunction} row={row} />;
};
