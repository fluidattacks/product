import { Container, Toggle, Tooltip } from "@fluidattacks/design";
import type { ColumnDef } from "@tanstack/react-table";
import capitalize from "lodash/capitalize";
import { Fragment, StrictMode } from "react";
import * as React from "react";
import { useTranslation } from "react-i18next";

import type { IVulnerabilitiesToVerifyProps } from "./types";

import { changeVulnStateFormatter } from "../change-vulns-state-formatter";
import type { IVulnData } from "../types";
import { Table } from "components/table";

const VulnerabilitiesToVerify: React.FC<IVulnerabilitiesToVerifyProps> = ({
  handleOnChange,
  isOpen,
  setVulnerabilitiesList,
  tableRef,
  vulnerabilitiesList,
}): JSX.Element => {
  const { t } = useTranslation();
  const handleUpdateRepo = (vulnInfo: Record<string, string>): void => {
    const newVulnList = vulnerabilitiesList.map(
      (vuln): IVulnData =>
        vuln.id === vulnInfo.id
          ? {
              ...vuln,
              state: vuln.state === "VULNERABLE" ? "SAFE" : "VULNERABLE",
            }
          : vuln,
    );
    setVulnerabilitiesList([...newVulnList]);
  };

  const columns: ColumnDef<IVulnData>[] = [
    {
      accessorKey: "where",
      header: "Where",
    },
    {
      accessorKey: "specific",
      header: "Specific",
    },
    {
      accessorKey: "state",
      cell: (cell): string => capitalize(cell.row.original.state),
      header: "State",
    },
    {
      accessorKey: "toggleState",
      cell: (cell): JSX.Element =>
        changeVulnStateFormatter(
          cell.row.original as unknown as Record<string, string>,
          handleUpdateRepo,
        ),
      header: "Toggle State",
      sortingFn: (rowA, rowB): number => {
        const stateA = rowA.original.state !== "SAFE";
        const stateB = rowB.original.state !== "SAFE";

        if (stateA === stateB) {
          return 0;
        } else if (stateA) {
          return 1;
        }

        return -1;
      },
    },
  ];

  return (
    <StrictMode>
      <Fragment>
        <Tooltip
          id={"toogleToolTip"}
          place={"top"}
          tip={t(
            "searchFindings.tabDescription.remediationModal.globalSwitch.tooltip",
          )}
        >
          <Container display={"flex"} flexDirection={"row-reverse"}>
            <Toggle
              defaultChecked={isOpen}
              leftDescription={`${t(
                "searchFindings.tabDescription.remediationModal.globalSwitch.text",
              )} to Safe`}
              name={"verification-switch"}
              onChange={handleOnChange}
              rightDescription={"Vulnerable"}
            />
          </Container>
        </Tooltip>
        <Table
          columns={columns}
          data={vulnerabilitiesList}
          options={{ enableSearchBar: true }}
          tableRef={tableRef}
        />
      </Fragment>
    </StrictMode>
  );
};

export { VulnerabilitiesToVerify };
