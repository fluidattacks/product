import type { IVulnRowAttr } from "../types";

interface IAdditionalInfoProps {
  canRetrieveHacker: boolean;
  canSeeSource: boolean;
  refetchData: () => void;
  vulnerability: IVulnRowAttr;
}

interface IFormValues {
  commitHash: string | null;
  source: string;
  type: string;
}

interface IActionButtonsProps {
  isEditing: boolean;
  isPristine: boolean;
  onEdit: () => void;
  onUpdate: () => void;
}

export type { IActionButtonsProps, IAdditionalInfoProps, IFormValues };
