import { Col, Row } from "@fluidattacks/design";
import _ from "lodash";
import { useTranslation } from "react-i18next";

import { Detail } from "../detail";
import { Value } from "../value";
import type { Vulnerability } from "gql/graphql";

const Advisories = ({
  advisories,
}: Readonly<{
  advisories: Vulnerability["advisories"] | undefined;
}>): JSX.Element | undefined => {
  const { t } = useTranslation();

  if (!advisories) return undefined;

  return (
    <Row>
      <Col>
        <h4>
          {t("searchFindings.tabVuln.vulnTable.advisories.packageDetails")}
        </h4>
        <Detail
          field={<Value value={advisories.package} />}
          label={t("searchFindings.tabVuln.vulnTable.advisories.name")}
        />
        <Detail
          field={<Value value={advisories.vulnerableVersion} />}
          label={t(
            "searchFindings.tabVuln.vulnTable.advisories.vulnerableVersion",
          )}
        />
        <Detail
          field={<Value value={advisories.cve?.join(",\n")} />}
          label={t("searchFindings.tabVuln.vulnTable.advisories.cve")}
        />
        {!_.isNil(advisories.epss) &&
        _.isInteger(advisories.epss) &&
        advisories.epss > 0 ? (
          <Detail
            field={<Value value={`${advisories.epss}%`} />}
            label={t("searchFindings.tabVuln.vulnTable.advisories.epss")}
          />
        ) : null}
      </Col>
    </Row>
  );
};

export { Advisories };
