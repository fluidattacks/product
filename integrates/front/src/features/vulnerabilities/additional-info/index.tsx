import { useMutation, useQuery } from "@apollo/client";
import _ from "lodash";
import { Fragment, useCallback, useContext, useMemo, useState } from "react";
import * as React from "react";
import { useTranslation } from "react-i18next";
import { useTheme } from "styled-components";
import type { Schema } from "yup";
import { object, string } from "yup";

import { ActionButtons } from "./action-buttons";
import { Advisories } from "./advisories";
import { CommitHash } from "./commit-hash";
import { Detail } from "./detail";
import { MethodDescription } from "./method-description";
import {
  GET_VULN_ADDITIONAL_INFO,
  UPDATE_VULNERABILITY_DESCRIPTION,
} from "./queries";
import { Source } from "./source";
import type { IAdditionalInfoProps, IFormValues } from "./types";
import { Value } from "./value";
import { Values } from "./values";

import { Status } from "../formatters/status";
import type { IErrorInfoAttr } from "../upload-file/types";
import {
  formatTreatment,
  formatVulnerabilitiesReason,
  getTimeToDetect,
} from "../utils";
import { Form } from "components/form";
import { authContext } from "context/auth";
import { Can } from "context/authz/can";
import type { VulnerabilitySource } from "gql/graphql";
import { VulnerabilityTreatment } from "gql/graphql";
import { useAudit } from "hooks/use-audit";
import { Logger } from "utils/logger";
import { msgError, msgSuccess } from "utils/notifications";

const AdditionalInfo: React.FC<IAdditionalInfoProps> = ({
  canRetrieveHacker,
  canSeeSource,
  refetchData,
  vulnerability,
}: IAdditionalInfoProps): JSX.Element => {
  const theme = useTheme();
  const { t } = useTranslation();
  const { userEmail } = useContext(authContext);
  const [isEditing, setIsEditing] = useState(false);
  const regexExp = /^[A-Fa-f0-9]{40}$|^[A-Fa-f0-9]{64}$/u;

  const formatError = useCallback(
    (errorName: string, errorValue: string): string =>
      ` ${t(errorName)} "${errorValue}" ${t("groupAlerts.invalid")}. `,
    [t],
  );

  // Graphql operations
  const { addAuditEvent } = useAudit();
  const { data } = useQuery(GET_VULN_ADDITIONAL_INFO, {
    onCompleted: (): void => {
      addAuditEvent("Vulnerability", vulnerability.id);
    },
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error): void => {
        msgError(t("groupAlerts.errorTextsad"));
        Logger.warning(
          "An error occurred loading the vulnerability info",
          error,
        );
      });
    },
    variables: {
      canRetrieveHacker,
      vulnId: vulnerability.id,
    },
  });
  const [updateVulnerabilityDescription] = useMutation(
    UPDATE_VULNERABILITY_DESCRIPTION,
    {
      onCompleted: (mtResult): void => {
        if (mtResult.updateVulnerabilityDescription.success) {
          msgSuccess(
            t("searchFindings.tabVuln.additionalInfo.alerts.updatedDetails"),
            t("groupAlerts.updatedTitle"),
          );
          setIsEditing(false);
          refetchData();
        }
      },
      onError: (error): void => {
        error.graphQLErrors.forEach(({ message }): void => {
          if (message.includes("Exception - Error in path value")) {
            const errorObject: IErrorInfoAttr = JSON.parse(message);
            msgError(`${t("groupAlerts.portValue")}
            ${formatError("groupAlerts.value", errorObject.values)}`);
          } else if (message === "Invalid, vulnerability already exists") {
            msgError(t("validations.vulnerabilityAlreadyExists"));
          } else if (message === "Exception - Unsanitized input found") {
            msgError(t("validations.unsanitizedInputFound"));
          } else if (
            message.includes(
              "Exception - The vulnerability path does not exist in the toe lines",
            )
          ) {
            msgError(
              t(
                "searchFindings.tabVuln.alerts.uploadFile.linesPathDoesNotExist",
                {
                  path: t("searchFindings.tabVuln.vulnTable.location"),
                },
              ),
            );
          } else if (
            message.includes(
              "Exception -  The vulnerability URL and field do not exist in the toe inputs",
            )
          ) {
            msgError(
              t(
                "searchFindings.tabVuln.alerts.uploadFile.inputUrlAndFieldDoNotExist",
                {
                  path: t("searchFindings.tabVuln.vulnTable.location"),
                },
              ),
            );
          } else if (
            message.includes(
              "Exception -  The line does not exist in the range of 0 and lines of code",
            )
          ) {
            const destructMsg: { msg: string; path: string } =
              JSON.parse(message);
            msgError(
              t(
                "searchFindings.tabVuln.alerts.uploadFile.lineDoesNotExistInLoc",
                {
                  line: destructMsg.msg.split("code: ")[1],
                  path: t("searchFindings.tabVuln.vulnTable.location"),
                },
              ),
            );
          } else {
            msgError(t("groupAlerts.errorTextsad"));
            Logger.warning(
              "An error occurred updating the vulnerability description",
              error,
            );
          }
        });
      },
      refetchQueries: [
        {
          query: GET_VULN_ADDITIONAL_INFO,
          variables: {
            canRetrieveHacker,
            vulnId: vulnerability.id,
          },
        },
      ],
    },
  );

  const closingDate = useMemo((): string | undefined => {
    if (data === undefined) {
      return "";
    }

    return (
      data.vulnerability.closingDate ?? data.vulnerability.lastStateDate
    )?.split(" ")[0];
  }, [data]);

  const treatmentLabel = useMemo((): string => {
    if (data === undefined) {
      return "";
    }

    return formatTreatment(
      data.vulnerability.treatmentStatus ?? "",
      vulnerability.state,
      data.vulnerability.treatmentAcceptanceStatus,
    );
  }, [data, vulnerability]);

  // Handle action
  const toggleEdit = useCallback((): void => {
    setIsEditing((currentValue: boolean): boolean => !currentValue);
  }, []);

  const onSubmit = useCallback(
    async (values: IFormValues): Promise<void> => {
      await updateVulnerabilityDescription({
        variables: {
          commit: values.type === "lines" ? values.commitHash : undefined,
          source:
            values.source === "ASM"
              ? undefined
              : (values.source as VulnerabilitySource),
          vulnerabilityId: vulnerability.id,
        },
      });
    },
    [vulnerability, updateVulnerabilityDescription],
  );

  const isVulnOpen = vulnerability.state === "VULNERABLE";
  const vulnerabilityType = useMemo((): string => {
    if (data === undefined) {
      return t("searchFindings.tabVuln.vulnTable.vulnerabilityType.inputs");
    }

    return t(
      `searchFindings.tabVuln.vulnTable.vulnerabilityType.${data.vulnerability.vulnerabilityType}`,
    );
  }, [t, data]);

  const currentExpiration =
    isVulnOpen &&
    data?.vulnerability.treatmentStatus === VulnerabilityTreatment.Accepted &&
    !_.isNull(data.vulnerability.treatmentAcceptanceDate)
      ? data.vulnerability.treatmentAcceptanceDate.split(" ")[0]
      : "";

  const currentJustification =
    !isVulnOpen ||
    _.isUndefined(vulnerability.treatmentJustification) ||
    _.isNull(vulnerability.treatmentJustification)
      ? ""
      : vulnerability.treatmentJustification;
  const currentAssigned = isVulnOpen
    ? (vulnerability.treatmentAssigned as string)
    : "";
  const treatmentDate = isVulnOpen
    ? vulnerability.lastTreatmentDate.split(" ")[0]
    : "";
  const treatmentChanges = data?.vulnerability.treatmentChanges ?? 0;
  const canSeeAuthor =
    userEmail.endsWith("@fluidattacks.com") &&
    !_.isNil(data?.vulnerability.author) &&
    data.vulnerability.vulnerabilityType === "lines";
  const timeToDetect = canSeeAuthor
    ? getTimeToDetect(
        data.vulnerability.reportDate,
        data.vulnerability.author?.commitDate,
      )
    : undefined;
  const vulnAuthor = canSeeAuthor ? data.vulnerability.author : undefined;

  return (
    <React.StrictMode>
      <Form
        enableReinitialize={true}
        initialValues={{
          commitHash: data?.vulnerability.commitHash ?? "",
          source: data?.vulnerability.source.toUpperCase() ?? "",
          type: data?.vulnerability.vulnerabilityType ?? "",
        }}
        name={"editVulnerability"}
        onSubmit={onSubmit}
        validationSchema={object().shape({
          commitHash: string().when("type", ([type]: string[]): Schema => {
            return type === "lines"
              ? string()
                  .required(t("validations.required"))
                  .matches(regexExp, t("validations.commitHash"))
                  .nullable()
              : string().nullable();
          }),
          source: string().required(t("validations.required")),
        })}
      >
        {({ dirty, submitForm, values }): React.ReactNode => {
          return (
            <Fragment>
              <Can
                do={
                  "integrates_api_mutations_update_vulnerability_description_mutate"
                }
              >
                <ActionButtons
                  isEditing={isEditing}
                  isPristine={!dirty}
                  onEdit={toggleEdit}
                  onUpdate={submitForm}
                />
              </Can>
              <div
                style={{
                  display: "grid",
                  gap: "1rem",
                  gridTemplateColumns: "50% 50%",
                }}
              >
                <main style={{ textWrap: "wrap" }}>
                  <h4>{t("searchFindings.tabVuln.vulnTable.location")}</h4>
                  <p
                    style={{
                      margin: "0",
                      padding: "0.25rem 0",
                      wordBreak: "break-word",
                    }}
                  >
                    {_.unescape(vulnerability.where)}
                  </p>
                  {_.isEmpty(vulnerability.stream) ? undefined : (
                    <Detail field={vulnerability.stream} />
                  )}
                  <Detail
                    field={<Value value={vulnerability.specific} />}
                    label={t(
                      `searchFindings.tabVuln.vulnTable.specificType.${vulnerabilityType}`,
                    )}
                  />
                  <h4>{t("searchFindings.tabVuln.vulnTable.info")}</h4>
                  <Detail
                    field={
                      <Value value={vulnerability.reportDate?.split(" ")[0]} />
                    }
                    label={t("searchFindings.tabVuln.vulnTable.reportDate")}
                  />
                  <Detail
                    field={
                      <Value
                        value={
                          vulnerability.state === "SAFE" ? closingDate : ""
                        }
                      />
                    }
                    label={t("searchFindings.tabVuln.vulnTable.closingDate")}
                  />
                  <Detail
                    field={
                      <Values
                        values={formatVulnerabilitiesReason(
                          vulnerability.stateReasons,
                        )}
                      />
                    }
                    label={t("searchFindings.tabDescription.closingReason")}
                  />
                  <Source
                    canSeeSource={canSeeSource}
                    isEditing={isEditing}
                    vulnerabilitySource={data?.vulnerability.source}
                  />
                  <CommitHash
                    commitHash={values.commitHash}
                    isEditing={isEditing}
                    vulnerability={data?.vulnerability}
                  />
                  <MethodDescription
                    methodDescription={data?.vulnerability.methodDescription}
                  />
                  <Detail
                    field={<Value value={vulnerability.tag} />}
                    label={t("searchFindings.tabDescription.tag")}
                  />
                  <Detail
                    field={
                      _.isEmpty(vulnerability.zeroRisk) ? (
                        <Value value={undefined} />
                      ) : (
                        <Status status={vulnerability.zeroRisk as string} />
                      )
                    }
                    label={t("searchFindings.tabDescription.zeroRisk")}
                  />
                  {canRetrieveHacker ? (
                    <Detail
                      field={<Value value={data?.vulnerability.hacker} />}
                      label={t("searchFindings.tabDescription.hacker")}
                    />
                  ) : undefined}
                  {_.isNil(vulnAuthor) ? undefined : (
                    <Detail
                      color={theme.palette.white}
                      field={
                        <Value
                          color={theme.palette.white}
                          value={vulnAuthor.authorEmail}
                        />
                      }
                      label={t("searchFindings.tabDescription.author.email")}
                    />
                  )}
                  {_.isNil(timeToDetect) ? undefined : (
                    <Detail
                      color={theme.palette.white}
                      field={
                        <Value
                          color={theme.palette.white}
                          value={timeToDetect.toString()}
                        />
                      }
                      label={t(
                        "searchFindings.tabDescription.author.timeToDetect",
                      )}
                    />
                  )}
                  <h4>{t("searchFindings.tabVuln.vulnTable.reattacks")}</h4>
                  <Detail
                    field={
                      <Value
                        value={
                          data?.vulnerability.lastRequestedReattackDate?.split(
                            " ",
                          )[0]
                        }
                      />
                    }
                    label={t(
                      "searchFindings.tabVuln.vulnTable.lastRequestedReattackDate",
                    )}
                  />
                  <Detail
                    field={
                      <Value
                        value={data?.vulnerability.lastReattackRequester}
                      />
                    }
                    label={t("searchFindings.tabVuln.vulnTable.requester")}
                  />
                  <Detail
                    field={<Value value={data?.vulnerability.cycles} />}
                    label={t("searchFindings.tabVuln.vulnTable.cycles")}
                  />
                  <Detail
                    field={<Value value={data?.vulnerability.efficacy} />}
                    label={t("searchFindings.tabVuln.vulnTable.efficacy")}
                  />
                </main>
                <aside>
                  <h4>{t("searchFindings.tabVuln.vulnTable.treatments")}</h4>
                  <Detail
                    field={<Value value={treatmentLabel} />}
                    label={t(
                      "searchFindings.tabVuln.vulnTable.currentTreatment",
                    )}
                  />
                  <Detail
                    field={<Value value={currentAssigned} />}
                    label={t("searchFindings.tabVuln.vulnTable.assigned")}
                  />
                  <Detail
                    field={<Value value={treatmentDate} />}
                    label={t("searchFindings.tabVuln.vulnTable.treatmentDate")}
                  />
                  <Detail
                    field={<Value value={currentExpiration} />}
                    label={t(
                      "searchFindings.tabVuln.vulnTable.treatmentExpiration",
                    )}
                  />
                  <Detail
                    field={<Value value={currentJustification} />}
                    label={t(
                      "searchFindings.tabVuln.vulnTable.treatmentJustification",
                    )}
                  />
                  <Detail
                    field={<Value value={treatmentChanges} />}
                    label={t(
                      "searchFindings.tabVuln.vulnTable.treatmentChanges",
                    )}
                  />
                  <Advisories advisories={data?.vulnerability.advisories} />
                </aside>
              </div>
            </Fragment>
          );
        }}
      </Form>
    </React.StrictMode>
  );
};

export { AdditionalInfo };
