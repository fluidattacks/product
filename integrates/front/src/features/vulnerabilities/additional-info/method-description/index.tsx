import isNil from "lodash/isNil";
import { useTranslation } from "react-i18next";

import { Detail } from "../detail";
import { Value } from "../value";

const MethodDescription = ({
  methodDescription,
}: Readonly<{
  methodDescription: string | null | undefined;
}>): JSX.Element | undefined => {
  const { t } = useTranslation();

  if (isNil(methodDescription)) {
    return undefined;
  }

  return (
    <Detail
      field={<Value value={methodDescription} />}
      label={t("searchFindings.tabVuln.vulnTable.methodDescription.text")}
    />
  );
};

export { MethodDescription };
