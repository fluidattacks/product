export interface IDetailProps {
  dataPrivate?: boolean;
  editableField?: JSX.Element;
  isEditing?: boolean;
  label?: JSX.Element | string;
  field: JSX.Element | string | null;
  color?: string;
}
