import { graphql } from "gql";

const GET_VULN_ADDITIONAL_INFO = graphql(`
  query GetVulnAdditionalInfo($canRetrieveHacker: Boolean!, $vulnId: String!) {
    vulnerability(uuid: $vulnId) {
      id
      closingDate
      commitHash
      customSeverity
      cycles
      efficacy
      findingId
      groupName
      hacker @include(if: $canRetrieveHacker)
      isAutoFixable
      lastReattackRequester
      lastRequestedReattackDate
      lastStateDate
      lastTreatmentDate
      methodDescription
      reportDate
      rootNickname
      severityTemporalScore
      severityThreatScore

      severityVector
      severityVectorV4

      source
      specific
      state
      stateReasons
      stream
      tag
      technique
      treatmentAcceptanceDate
      treatmentAcceptanceStatus
      treatmentAssigned
      treatmentChanges
      treatmentJustification
      treatmentStatus
      vulnerabilityType
      where
      advisories {
        cve
        epss
        package
        vulnerableVersion
      }
      author {
        authorEmail
        commit
        commitDate
      }
      snippet {
        content
        offset
      }
    }
  }
`);

const UPDATE_VULNERABILITY_DESCRIPTION = graphql(`
  mutation UpdateVulnerabilityDescription(
    $commit: String
    $source: VulnerabilitySource
    $vulnerabilityId: ID!
  ) {
    updateVulnerabilityDescription(
      commit: $commit
      source: $source
      vulnerabilityId: $vulnerabilityId
    ) {
      success
    }
  }
`);

export { GET_VULN_ADDITIONAL_INFO, UPDATE_VULNERABILITY_DESCRIPTION };
