import { Text } from "@fluidattacks/design";
import _ from "lodash";
import { StrictMode } from "react";
import { useTranslation } from "react-i18next";

export const Value = ({
  value,
  color,
}: Readonly<{
  value: number | string | null | undefined;
  color?: string;
}>): JSX.Element => {
  const { t } = useTranslation();
  const isEmpty = _.isNumber(value)
    ? value === 0
    : _.isEmpty(value) || value === "-";

  return (
    <StrictMode>
      <Text
        color={color}
        size={"sm"}
        wordBreak={"break-word"}
        wordWrap={"normal"}
      >
        {isEmpty ? t("searchFindings.tabVuln.notApplicable") : value}
      </Text>
    </StrictMode>
  );
};
