import isEmpty from "lodash/isEmpty";
import { useTranslation } from "react-i18next";

import { Detail } from "../detail";
import { Value } from "../value";
import { Input } from "components/input";
import type { GetVulnAdditionalInfoQuery } from "gql/graphql";

const commitFormatter = (value: string | null): string => {
  if (value === null) {
    return "";
  }

  const COMMIT_LENGTH = 7;

  return value.slice(0, COMMIT_LENGTH);
};

const CommitHash = ({
  commitHash,
  isEditing,
  vulnerability,
}: Readonly<{
  commitHash: string;
  isEditing: boolean;
  vulnerability: GetVulnAdditionalInfoQuery["vulnerability"] | undefined;
}>): JSX.Element | undefined => {
  const { t } = useTranslation();

  if (
    (isEmpty(commitHash) && !isEditing) ||
    vulnerability?.vulnerabilityType !== "lines"
  )
    return undefined;

  return (
    <Detail
      editableField={<Input name={"commitHash"} />}
      field={<Value value={commitFormatter(vulnerability.commitHash)} />}
      isEditing={isEditing}
      label={t("searchFindings.tabVuln.commitHash")}
    />
  );
};

export { CommitHash };
