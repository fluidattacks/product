import { PureAbility } from "@casl/ability";
import { screen, waitFor, within } from "@testing-library/react";
import { userEvent } from "@testing-library/user-event";
import dayjs from "dayjs";
import { GraphQLError } from "graphql";
import { HttpResponse, type StrictResponse, graphql } from "msw";

import {
  GET_VULN_ADDITIONAL_INFO,
  UPDATE_VULNERABILITY_DESCRIPTION,
} from "./queries";

import { AdditionalInfo } from ".";
import type { IVulnRowAttr } from "../types";
import { formatVulnerabilities } from "../utils";
import { authzPermissionsContext } from "context/authz/config";
import type {
  GetVulnAdditionalInfoQuery as GetVulnAdditionalInfo,
  UpdateVulnerabilityDescriptionMutation as UpdateVulnerabilityDescription,
} from "gql/graphql";
import {
  RootCriticality,
  Technique,
  VulnerabilitySource,
  VulnerabilityState,
  VulnerabilityTreatment,
} from "gql/graphql";
import { render } from "mocks";
import { LINK } from "mocks/constants";
import type { IErrorMessage } from "mocks/types";
import { msgSuccess } from "utils/notifications";
import { translate } from "utils/translations/translate";

jest.mock("utils/notifications", (): Record<string, unknown> => {
  const mockedNotifications: Record<string, () => Record<string, unknown>> =
    jest.requireActual("utils/notifications");
  jest.spyOn(mockedNotifications, "msgError").mockImplementation();
  jest.spyOn(mockedNotifications, "msgSuccess").mockImplementation();

  return mockedNotifications;
});

describe("additionalInfo", (): void => {
  const graphqlMocked = graphql.link(LINK);
  const numberOfDays = 5;
  const memoryRouter = {
    initialEntries: [
      "/TEST/vulns/438679960/locations/af7a48b8-d8fc-41da-9282-d424fff563f0",
    ],
  };
  const mockVuln: IVulnRowAttr = {
    advisories: {
      cve: ["GHSA-jxqq-cqm6-pfq9", "CVE-2017-16117"],
      epss: 20,
      package: "slug",
      vulnerableVersion: "0.9.0",
    },
    assigned: "assigned-user-4",
    closingDate: null,
    customSeverity: 1,
    externalBugTrackingSystem: null,
    findingId: "438679960",
    groupName: "test",
    historicTreatment: [
      {
        acceptanceDate: "",
        acceptanceStatus: "",
        assigned: "assigned-user-4",
        date: "2019-07-05 09:56:40",
        justification: "test progress justification",
        treatment: "IN PROGRESS",
        user: "usertreatment@test.test",
      },
    ],
    id: "af7a48b8-d8fc-41da-9282-d424fff563f0",
    lastEditedBy: "usertreatment@test.test",
    lastStateDate: "2019-07-05 09:56:40",
    lastTreatmentDate: "2019-07-05 09:56:40",
    lastVerificationDate: dayjs()
      .subtract(numberOfDays, "days")
      .format("YYYY-MM-DD hh:mm:ss A"),
    organizationName: undefined,
    priority: 0,
    proposedSeverityAuthor: null,
    proposedSeverityTemporalScore: null,
    proposedSeverityThreatScore: null,
    proposedSeverityVector: null,
    proposedSeverityVectorV4: null,
    remediated: false,
    reportDate: "",
    root: {
      __typename: "GitRoot",
      branch: "master",
      criticality: RootCriticality.Low,
    },
    rootNickname: "https:",
    severityTemporalScore: 1.0,
    severityThreatScore: 0.8,
    severityVector: "AV:A/AC:H/PR:L/UI:R/S:C/C:L/I:L/A:L/E:F/RL:O/RC:U",
    severityVectorV4:
      "CVSS:4.0/AV:A/AC:H/PR:L/UI:P/VC:L/VI:L/VA:L/SC:L/SI:L/SA:L/E:A",
    source: "asm",
    specific: "specific-3",
    state: "VULNERABLE",
    stateReasons: null,
    stream: null,
    tag: "tag-7, tag-8",
    technique: "SCR",
    treatmentAcceptanceDate: "",
    treatmentAcceptanceStatus: "",
    treatmentAssigned: "assigned-user-4",
    treatmentDate: "2019-07-05 09:56:40",
    treatmentJustification: "test progress justification",
    treatmentStatus: "IN PROGRESS",
    treatmentUser: "usertreatment@test.test",
    verification: "Verified",
    verificationJustification: "Solved now",
    vulnerabilityType: "lines",
    where: "https://example.com/lines",
    zeroRisk: null,
  };

  const mockQueryVulnAdditionalInfo = [
    graphqlMocked.query(
      GET_VULN_ADDITIONAL_INFO,
      (): StrictResponse<{ data: GetVulnAdditionalInfo }> => {
        return HttpResponse.json({
          data: {
            vulnerability: {
              __typename: "Vulnerability",
              advisories: {
                cve: ["GHSA-jxqq-cqm6-pfq9", "CVE-2017-16117"],
                epss: 20,
                package: "slug",
                vulnerableVersion: "0.9.0",
              },
              author: {
                authorEmail: "author@test.test",
                commit: "ff9afd8d7499d0debf6abdf15a8b980a6cc167e2",
                commitDate: "2020-01-01 00:00:00",
              },
              closingDate: "2020-09-05 03:23:23",

              commitHash: null,
              customSeverity: 1,
              cycles: 1,
              efficacy: 0,
              findingId: "438679960",
              groupName: "test",
              id: "af7a48b8-d8fc-41da-9282-d424fff563f0",
              isAutoFixable: false,
              lastReattackRequester: "",
              lastRequestedReattackDate: null,
              lastStateDate: "2020-09-05 03:23:23",
              lastTreatmentDate: "2019-07-05 09:56:40",
              methodDescription: "This is the description for skims method",
              reportDate: "",
              rootNickname: "",
              severityTemporalScore: 3.0,
              severityThreatScore: 3.0,
              severityVector:
                "AV:A/AC:H/PR:L/UI:R/S:C/C:L/I:L/A:L/E:F/RL:O/RC:U",
              severityVectorV4:
                "CVSS:4.0/AV:A/AC:H/PR:L/UI:P/VC:L/VI:L/VA:L/SC:L/SI:L/SA:L/E:A",
              snippet: null,
              source: "escape",
              specific: "specific-3",
              state: VulnerabilityState.Vulnerable,
              stateReasons: null,
              stream: null,
              tag: "",
              technique: Technique.Ptaas,
              treatmentAcceptanceDate: "",
              treatmentAcceptanceStatus: null,
              treatmentAssigned: "assigned-user-4",
              treatmentChanges: 1,
              treatmentJustification: "test progress justification",
              treatmentStatus: VulnerabilityTreatment.InProgress,
              vulnerabilityType: "lines",
              where: "https://example.com/lines",
            },
          },
        });
      },
    ),
  ];

  it("should render in vulnerabilities", async (): Promise<void> => {
    expect.hasAssertions();

    render(
      <AdditionalInfo
        canRetrieveHacker={false}
        canSeeSource={true}
        refetchData={jest.fn()}
        vulnerability={
          formatVulnerabilities({
            vulnerabilities: [{ ...mockVuln, state: "SAFE" }],
          })[0]
        }
      />,
      {
        memoryRouter,
        mocks: [...mockQueryVulnAdditionalInfo],
      },
    );

    await waitFor((): void => {
      expect(
        screen.getByText("searchFindings.tabVuln.vulnTable.closingDate"),
      ).toBeInTheDocument();
    });

    await waitFor((): void => {
      expect(screen.getByText("2020-09-05")).toBeInTheDocument();
    });

    await waitFor((): void => {
      expect(
        screen.getByText("searchFindings.tabVuln.vulnTable.advisories.epss"),
      ).toBeInTheDocument();
    });
  });

  it("should update vulnerability details %s", async (): Promise<void> => {
    expect.hasAssertions();

    const mockedMutations = [
      graphqlMocked.mutation(
        UPDATE_VULNERABILITY_DESCRIPTION,
        ({
          variables,
        }): StrictResponse<
          IErrorMessage | { data: UpdateVulnerabilityDescription }
        > => {
          const { commit, source, vulnerabilityId } = variables;
          if (
            commit === "ea871eee64cfd5ce293411efaf4d3b446d04eb4a" &&
            source === VulnerabilitySource.Deterministic &&
            vulnerabilityId === "af7a48b8-d8fc-41da-9282-d424fff563f0"
          ) {
            return HttpResponse.json({
              data: {
                updateVulnerabilityDescription: {
                  success: true,
                },
              },
            });
          }

          return HttpResponse.json({
            errors: [new GraphQLError("Error updating vulnerability")],
          });
        },
      ),
    ];
    const mockedPermissions = new PureAbility<string>([
      {
        action:
          "integrates_api_mutations_update_vulnerability_description_mutate",
      },
    ]);

    render(
      <authzPermissionsContext.Provider value={mockedPermissions}>
        <AdditionalInfo
          canRetrieveHacker={false}
          canSeeSource={true}
          refetchData={jest.fn()}
          vulnerability={
            formatVulnerabilities({ vulnerabilities: [mockVuln] })[0]
          }
        />
      </authzPermissionsContext.Provider>,
      {
        memoryRouter,
        mocks: [...mockQueryVulnAdditionalInfo, ...mockedMutations],
      },
    );
    await waitFor((): void => {
      expect(
        screen.queryByText(
          translate.t("searchFindings.tabDescription.treatment.inProgress"),
        ),
      ).toBeInTheDocument();
    });

    await userEvent.click(
      screen.getByText("searchFindings.tabVuln.additionalInfo.buttons.edit"),
    );

    expect(
      screen.getByRole("button", {
        name: "searchFindings.tabVuln.additionalInfo.buttons.save",
      }),
    ).toBeDisabled();

    await userEvent.click(
      within(screen.getByRole("combobox", { name: "source" })).getByTestId(
        "source-select-selected-option",
      ),
    );
    await userEvent.click(
      screen.getByText(
        "searchFindings.tabVuln.vulnTable.vulnerabilitySource.DETERMINISTIC",
      ),
    );

    await userEvent.clear(
      screen.getByRole("combobox", { name: /commithash/iu }),
    );
    await userEvent.type(
      screen.getByRole("combobox", { name: /commithash/iu }),
      "ea871eee64cfd5ce293411efaf4d3b446d04eb4a",
    );
    await waitFor((): void => {
      expect(
        screen.getByRole("button", {
          name: "searchFindings.tabVuln.additionalInfo.buttons.save",
        }),
      ).not.toBeDisabled();
    });
    await userEvent.click(
      screen.getByRole("button", {
        name: "searchFindings.tabVuln.additionalInfo.buttons.save",
      }),
    );

    await waitFor((): void => {
      expect(msgSuccess).toHaveBeenCalledWith(
        "searchFindings.tabVuln.additionalInfo.alerts.updatedDetails",
        "groupAlerts.updatedTitle",
      );
    });
  });
});
