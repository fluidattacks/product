import { graphql } from "gql";

const GET_VULN_TREATMENT = graphql(`
  query GetVulnTreatment($after: String, $vulnId: String!) {
    vulnerability(uuid: $vulnId) {
      id
      historicTreatmentConnection(after: $after) {
        edges {
          node {
            acceptanceDate
            acceptanceStatus
            assigned
            date
            justification
            treatment
            user
          }
        }
        pageInfo {
          endCursor
          hasNextPage
        }
      }
    }
  }
`);

export { GET_VULN_TREATMENT };
