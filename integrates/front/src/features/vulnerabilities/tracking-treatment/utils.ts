import type { IVulnRowAttr } from "../types";
import { VulnerabilityStateReason } from "gql/graphql";
import type { IHistoricTreatment } from "pages/finding/description/types";
import { formatDateTime } from "utils/date";

function getJustification(vuln: IVulnRowAttr): string {
  if (vuln.verification !== null) {
    if (vuln.verificationJustification !== null) {
      return vuln.verificationJustification;
    }

    return "Remediated";
  }
  if (
    vuln.stateReasons?.includes(VulnerabilityStateReason.Exclusion) ??
    false
  ) {
    return "Out of the current active scope";
  }
  if (vuln.lastEditedBy.startsWith("machine@")) {
    return "Closed by Machine";
  }
  if (vuln.lastEditedBy.startsWith("rebase@")) {
    return "In a recent Machine run, the vulnerability was not found.";
  }

  return "";
}

function getClosedTreatment(vuln: IVulnRowAttr): IHistoricTreatment[] {
  if (vuln.state !== "SAFE") {
    return [];
  }
  const closingDate = vuln.closingDate ?? vuln.lastStateDate;

  return [
    {
      date: formatDateTime(closingDate),
      justification: getJustification(vuln),
      treatment: "CLOSED",
      user: vuln.hacker ?? "",
    },
  ];
}

function isNextTreatmentDifferent(
  treatment: IHistoricTreatment,
  array: IHistoricTreatment[],
  index: number,
): boolean {
  return Boolean(treatment.treatment !== array[index + 1].treatment);
}

function isFirstTreatmentOrNextDifferent(
  treatment: IHistoricTreatment,
  array: IHistoricTreatment[],
  index: number,
): boolean {
  return (
    index === 0 ||
    (index < array.length - 1 &&
      isNextTreatmentDifferent(treatment, array, index))
  );
}

function isLastTreatmentOrNextDifferent(
  treatment: IHistoricTreatment,
  array: IHistoricTreatment[],
  index: number,
): boolean {
  return (
    index === array.length - 1 ||
    isNextTreatmentDifferent(treatment, array, index)
  );
}

export {
  getClosedTreatment,
  isFirstTreatmentOrNextDifferent,
  isLastTreatmentOrNextDifferent,
};
