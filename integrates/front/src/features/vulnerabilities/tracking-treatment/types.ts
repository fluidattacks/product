import type { IVulnRowAttr } from "../types";
import type { IHistoricTreatment } from "pages/finding/description/types";

interface IHistoricTreatmentEdge {
  node: IHistoricTreatment;
}

interface IVulnHistoricTreatmentConnection {
  edges: IHistoricTreatmentEdge[];
  pageInfo: {
    hasNextPage: boolean;
    endCursor: string;
  };
}

interface IVulnTreatmentAttr {
  __typename: string;
  historicTreatmentConnection: IVulnHistoricTreatmentConnection;
  id: string;
}

interface IGetVulnTreatmentAttr {
  vulnerability: IVulnTreatmentAttr;
}

interface ITreatmentTrackingAttr {
  readonly vuln: IVulnRowAttr;
}

export type { IGetVulnTreatmentAttr, ITreatmentTrackingAttr };
