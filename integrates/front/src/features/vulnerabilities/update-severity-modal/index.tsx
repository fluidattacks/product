import { useMutation, useQuery } from "@apollo/client";
import { Form, InnerForm, Input, Modal, useModal } from "@fluidattacks/design";
import { type FC, Fragment, StrictMode, useCallback } from "react";
import { useTranslation } from "react-i18next";
import { object, string } from "yup";

import type { IUpdateSeverityProps } from "./types";

import {
  GET_VULN_SEVERITY_INFO,
  REQUEST_VULNERABILITIES_SEVERITY_UPDATE,
  UPDATE_VULNERABILITIES_SEVERITY,
} from "../severity-info/queries";
import { VulnerabilityState } from "gql/graphql";
import { GET_FINDING_HEADER } from "pages/finding/queries";
import { CVSS3_CALCULATOR_BASE_URL } from "utils/cvss";
import { CVSS4_CALCULATOR_BASE_URL } from "utils/cvss4";
import { Logger } from "utils/logger";
import { msgError, msgSuccess } from "utils/notifications";

export const UpdateSeverityModal: FC<IUpdateSeverityProps> = ({
  findingId,
  isOpen,
  vulnerabilities,
  handleCloseModal,
  refetchData,
}: IUpdateSeverityProps): JSX.Element => {
  const { t } = useTranslation();
  const modalProps = useModal("update-severity-modal");
  const vulnerabilityIds = vulnerabilities.map(({ id }): string => id);
  const alertsPrefix = "searchFindings.tabVuln.severityInfo.alerts.";
  const prefix = "searchFindings.tabDescription.";

  // Graphql operations
  const { data } = useQuery(GET_VULN_SEVERITY_INFO, {
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error): void => {
        msgError(t("groupAlerts.errorTextsad"));
        Logger.warning("An error occurred loading the severity info", error);
      });
    },
    variables: {
      vulnId: vulnerabilityIds[0],
    },
  });

  const invalidCvssV3 =
    "Exception - Error invalid severity CVSS v3.1 vector string";
  const invalidCvssV4 =
    "Exception - Error invalid severity CVSS v4 vector string";
  const invalidVulnStatus =
    "Exception - Direct vulnerability update not available for current status";
  const severityUpdateAlreadyRequested =
    "Exception - Severity score update already requested for vuln";

  const [updateVulnerabilitiesSeverity] = useMutation(
    UPDATE_VULNERABILITIES_SEVERITY,
    {
      onCompleted: (result): void => {
        if (result.updateVulnerabilitiesSeverity.success) {
          msgSuccess(
            t(`${alertsPrefix}updatedSeverity`),
            t("groupAlerts.updatedTitle"),
          );
          handleCloseModal();
          refetchData();
        }
      },
      onError: ({ graphQLErrors }): void => {
        graphQLErrors.forEach((error): void => {
          if (error.message === invalidCvssV3) {
            msgError(t(`${alertsPrefix}invalidSeverityVector`));
          } else if (error.message === invalidCvssV4) {
            msgError(t(`${alertsPrefix}invalidSeverityVectorV4`));
          } else if (error.message === invalidVulnStatus) {
            msgError(t(`${alertsPrefix}invalidVulnStatus`));
          } else {
            msgError(t("groupAlerts.errorTextsad"));
            Logger.warning("An error occurred updating severity", error);
          }
        });
      },
      refetchQueries: [
        {
          query: GET_FINDING_HEADER,
          variables: {
            findingId,
          },
        },
      ],
    },
  );

  const [requestVulnerabilitiesSeverityUpdate] = useMutation(
    REQUEST_VULNERABILITIES_SEVERITY_UPDATE,
    {
      onCompleted: (result): void => {
        if (result.requestVulnerabilitiesSeverityUpdate.success) {
          msgSuccess(
            t(`${alertsPrefix}severityUpdateRequested`),
            t("groupAlerts.updatedTitle"),
          );
          handleCloseModal();
          refetchData();
        }
      },
      onError: ({ graphQLErrors }): void => {
        graphQLErrors.forEach((error): void => {
          if (error.message === invalidCvssV3) {
            msgError(t(`${alertsPrefix}invalidSeverityVector`));
          } else if (error.message === invalidCvssV4) {
            msgError(t(`${alertsPrefix}invalidSeverityVectorV4`));
          } else if (error.message === severityUpdateAlreadyRequested) {
            msgError(t(`${alertsPrefix}severityUpdateAlreadyRequested`));
          } else {
            msgError(t("groupAlerts.errorTextsad"));
            Logger.warning(
              "An error occurred requesting severity update",
              error,
            );
          }
        });
      },
      refetchQueries: [
        {
          query: GET_FINDING_HEADER,
          variables: {
            findingId,
          },
        },
      ],
    },
  );

  const onSubmit = useCallback(
    async (values: {
      severityVector: string;
      severityVectorV4: string;
    }): Promise<void> => {
      if (data?.vulnerability.state === VulnerabilityState.Vulnerable) {
        await requestVulnerabilitiesSeverityUpdate({
          variables: {
            cvss4Vector: values.severityVectorV4,
            cvssVector: values.severityVector,
            findingId,
            vulnerabilityIds,
          },
        });
      } else if (
        data?.vulnerability.state === VulnerabilityState.Submitted ||
        data?.vulnerability.state === VulnerabilityState.Rejected
      ) {
        await updateVulnerabilitiesSeverity({
          variables: {
            cvss4Vector: values.severityVectorV4,
            cvssVector: values.severityVector,
            findingId,
            vulnerabilityIds,
          },
        });
      }
    },
    [
      data,
      findingId,
      requestVulnerabilitiesSeverityUpdate,
      updateVulnerabilitiesSeverity,
      vulnerabilityIds,
    ],
  );

  return (
    <StrictMode>
      <Modal
        id={"updateVulnerabilitiesSeverity"}
        modalRef={{ ...modalProps, close: handleCloseModal, isOpen }}
        size={"sm"}
        title={t(`${prefix}updateVulnSeverity`)}
      >
        {data ? (
          <Form
            cancelButton={{
              label: t("group.findings.report.modalClose"),
              onClick: handleCloseModal,
            }}
            defaultValues={{
              severityVector: data.vulnerability.severityVector,
              severityVectorV4: data.vulnerability.severityVectorV4,
            }}
            onSubmit={onSubmit}
            yupSchema={object().shape({
              severityVector: string().required(t("validations.required")),
              severityVectorV4: string().required(t("validations.required")),
            })}
          >
            <InnerForm>
              {({ formState, register, watch }): JSX.Element => {
                const [severityVector, severityVectorV4] = watch([
                  "severityVector",
                  "severityVectorV4",
                ]);

                return (
                  <Fragment>
                    <Input
                      error={formState.errors.severityVector?.message?.toString()}
                      helpLink={`${CVSS3_CALCULATOR_BASE_URL}#${severityVector}`}
                      helpLinkText={t(`${prefix}updateVulnSeverityHelp`)}
                      isTouched={"severityVector" in formState.touchedFields}
                      isValid={!("severityVector" in formState.errors)}
                      label={t(`${prefix}updateVulnerabilitiesSeverityLabel`)}
                      name={"severityVector"}
                      placeholder={t(
                        `${prefix}updateVulnerabilitiesSeverityPlaceholder`,
                      )}
                      register={register}
                      weight={"bold"}
                    />
                    <br />
                    <Input
                      error={formState.errors.severityVectorV4?.message?.toString()}
                      helpLink={`${CVSS4_CALCULATOR_BASE_URL}#${severityVectorV4}`}
                      helpLinkText={t(`${prefix}updateVulnSeverityHelp`)}
                      isTouched={"severityVectorV4" in formState.touchedFields}
                      isValid={!("severityVectorV4" in formState.errors)}
                      label={t(`${prefix}updateVulnerabilitiesSeverityLabelV4`)}
                      name={"severityVectorV4"}
                      placeholder={t(
                        `${prefix}updateVulnerabilitiesSeverityPlaceholderV4`,
                      )}
                      register={register}
                      weight={"bold"}
                    />
                    {data.vulnerability.state ===
                    VulnerabilityState.Vulnerable ? (
                      <p style={{ color: "#9D9D9D", marginTop: "16px" }}>
                        {t(
                          `${prefix}updateVulnerabilitiesSeverityIsVulnerable`,
                        )}
                      </p>
                    ) : undefined}
                  </Fragment>
                );
              }}
            </InnerForm>
          </Form>
        ) : undefined}
      </Modal>
    </StrictMode>
  );
};
