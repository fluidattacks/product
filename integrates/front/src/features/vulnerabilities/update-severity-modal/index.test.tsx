import { screen, waitFor } from "@testing-library/react";
import { userEvent } from "@testing-library/user-event";
import { GraphQLError } from "graphql";
import isEqual from "lodash/isEqual";
import { HttpResponse, type StrictResponse, graphql } from "msw";

import { UpdateSeverityModal } from ".";
import { REQUEST_VULNERABILITIES_SEVERITY_UPDATE } from "../severity-info/queries";
import type { IVulnDataTypeAttr } from "../types";
import type { RequestVulnerabilitiesSeverityUpdateMutation as RequestVulnerabilitiesSeverityUpdate } from "gql/graphql";
import { render } from "mocks";
import { LINK } from "mocks/constants";
import type { IErrorMessage } from "mocks/types";
import { CVSS3_CALCULATOR_BASE_URL } from "utils/cvss";
import { CVSS4_CALCULATOR_BASE_URL } from "utils/cvss4";
import { msgSuccess } from "utils/notifications";

jest.mock("utils/notifications", (): Record<string, unknown> => {
  const mockedNotifications: Record<string, () => Record<string, unknown>> =
    jest.requireActual("utils/notifications");
  jest.spyOn(mockedNotifications, "msgSuccess").mockImplementation();

  return mockedNotifications;
});

describe("updateSeverityModal", (): void => {
  const vulnId = "af7a48b8-d8fc-41da-9282-d424fff563f0";
  const findingId = "438679960";
  const memoryRouter = {
    initialEntries: ["/orgs/okada"],
  };
  const vulnerabilitiesData: IVulnDataTypeAttr[] = [
    {
      assigned: "",
      customSeverity: 2,
      externalBugTrackingSystem: null,
      findingId,
      groupName: "testgroupname",
      historicTreatment: [
        {
          date: "",
          justification: "test justification",
          treatment: "UNTREATED",
          user: "",
        },
      ],
      id: vulnId,
      source: "asm",
      specific: "",
      state: "VULNERABLE",
      tag: "one",
      where: "",
    },
  ];

  it("should render a component", async (): Promise<void> => {
    expect.hasAssertions();

    jest.clearAllMocks();
    jest.spyOn(console, "error").mockImplementation();
    render(
      <UpdateSeverityModal
        findingId={findingId}
        handleCloseModal={jest.fn()}
        isOpen={true}
        refetchData={jest.fn()}
        vulnerabilities={vulnerabilitiesData}
      />,
      {
        memoryRouter,
      },
    );

    await waitFor((): void => {
      expect(
        screen.queryByRole("textbox", { name: "severityVector" }),
      ).toBeInTheDocument();
    });

    expect(
      screen.getByText(
        "searchFindings.tabDescription.updateVulnerabilitiesSeverityLabel",
      ),
    ).toBeInTheDocument();

    const confirmButton = screen.getByRole("button", { name: "Confirm" });

    expect(confirmButton).toBeInTheDocument();
    expect(confirmButton).toBeDisabled();

    const cancelButton = screen.getByRole("button", {
      name: "group.findings.report.modalClose",
    });

    expect(cancelButton).toBeInTheDocument();
    expect(cancelButton).toBeEnabled();
  });

  it("should request vulnerabilities severity update", async (): Promise<void> => {
    expect.hasAssertions();

    jest.clearAllMocks();
    jest.spyOn(console, "warn").mockImplementation();

    const modifiedCvssVector = "CVSS:3.1/AV:N/AC:H/PR:H/UI:N/S:C/C:H/I:H/A:H";
    const modifiedCvss4Vector =
      "CVSS:4.0/AV:N/AC:H/AT:N/PR:H/UI:N/VC:H/VI:H/VA:H/SC:L/SI:L/SA:L";

    const mockedMutation = graphql
      .link(LINK)
      .mutation(
        REQUEST_VULNERABILITIES_SEVERITY_UPDATE,
        ({
          variables,
        }): StrictResponse<
          IErrorMessage | { data: RequestVulnerabilitiesSeverityUpdate }
        > => {
          const {
            cvssVector,
            cvss4Vector,
            findingId: id,
            vulnerabilityIds,
          } = variables;
          if (
            cvssVector === modifiedCvssVector &&
            cvss4Vector === modifiedCvss4Vector &&
            findingId === id &&
            isEqual(vulnerabilityIds, [vulnId])
          ) {
            return HttpResponse.json({
              data: {
                requestVulnerabilitiesSeverityUpdate: {
                  success: true,
                },
              },
            });
          }

          return HttpResponse.json({
            errors: [new GraphQLError("Error updating severity")],
          });
        },
      );

    render(
      <UpdateSeverityModal
        findingId={findingId}
        handleCloseModal={jest.fn()}
        isOpen={true}
        refetchData={jest.fn()}
        vulnerabilities={vulnerabilitiesData}
      />,
      { memoryRouter, mocks: [mockedMutation] },
    );

    await waitFor((): void => {
      expect(
        screen.queryByRole("textbox", { name: "severityVector" }),
      ).toBeInTheDocument();
    });

    await waitFor((): void => {
      expect(
        screen.getByRole("textbox", { name: "severityVector" }),
      ).toHaveValue(
        "CVSS:3.1/AV:N/AC:L/PR:L/UI:N/S:U/C:N/I:L/A:N/E:U/RL:O/RC:C",
      );
    });

    await userEvent.clear(
      screen.getByRole("textbox", { name: "severityVector" }),
    );
    await userEvent.type(
      screen.getByRole("textbox", { name: "severityVector" }),
      modifiedCvssVector,
    );
    await userEvent.clear(
      screen.getByRole("textbox", { name: "severityVectorV4" }),
    );
    await userEvent.type(
      screen.getByRole("textbox", { name: "severityVectorV4" }),
      modifiedCvss4Vector,
    );

    const links = screen.getAllByRole("link");

    expect(links[0]).toHaveAttribute(
      "href",
      `${CVSS3_CALCULATOR_BASE_URL}#${modifiedCvssVector}`,
    );
    expect(links[1]).toHaveAttribute(
      "href",
      `${CVSS4_CALCULATOR_BASE_URL}#${modifiedCvss4Vector}`,
    );

    await userEvent.click(screen.getByRole("button", { name: "Confirm" }));

    await waitFor((): void => {
      expect(msgSuccess).toHaveBeenCalledWith(
        "searchFindings.tabVuln.severityInfo.alerts.severityUpdateRequested",
        "groupAlerts.updatedTitle",
      );
    });
  });
});
