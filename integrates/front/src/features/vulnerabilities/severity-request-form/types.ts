import type { IUseModal } from "@fluidattacks/design";

import type { IVulnRowAttr } from "../types";

interface IHandleSeverityRequestFormProps {
  findingId: string;
  modalRef: IUseModal;
  vulnerabilities: IVulnRowAttr[];
  clearSelected: () => void;
  onCancel: () => void;
  refetchData: () => void;
}

interface IFormValues {
  justification: string;
}

export type { IHandleSeverityRequestFormProps, IFormValues };
