import { graphql } from "gql";

const APPROVE_VULNS_SEVERITY_REQUEST = graphql(`
  mutation ApproveVulnerabilitiesSeverity(
    $findingId: ID!
    $vulnerabilityIds: [ID!]!
  ) {
    approveVulnerabilitiesSeverityUpdate(
      findingId: $findingId
      vulnerabilityIds: $vulnerabilityIds
    ) {
      success
    }
  }
`);

const REJECT_VULNS_SEVERITY_REQUEST = graphql(`
  mutation RejectVulnerabilitiesSeverity(
    $findingId: String!
    $justification: String!
    $vulnerabilityIds: [String!]!
  ) {
    rejectVulnerabilitiesSeverityUpdate(
      findingId: $findingId
      justification: $justification
      vulnerabilityIds: $vulnerabilityIds
    ) {
      success
    }
  }
`);

export { APPROVE_VULNS_SEVERITY_REQUEST, REJECT_VULNS_SEVERITY_REQUEST };
