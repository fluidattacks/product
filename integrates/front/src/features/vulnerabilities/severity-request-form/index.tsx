import { useMutation } from "@apollo/client";
import {
  Container,
  Form,
  InnerForm,
  Modal,
  TextArea,
} from "@fluidattacks/design";
import type { GraphQLError } from "graphql";
import { useCallback, useEffect, useState } from "react";
import * as React from "react";
import { Fragment } from "react";
import { useTranslation } from "react-i18next";
import type { Schema } from "yup";
import { array, object, string } from "yup";

import {
  APPROVE_VULNS_SEVERITY_REQUEST,
  REJECT_VULNS_SEVERITY_REQUEST,
} from "./queries";
import { SeverityRequestsTable } from "./severity-requests-table";
import type { IFormValues, IHandleSeverityRequestFormProps } from "./types";
import { getSeverityRequestVulns } from "./utils";

import type { IVulnRowAttr } from "../types";
import { Logger } from "utils/logger";
import { msgError, msgSuccess } from "utils/notifications";

const HandleSeverityRequestsForm: React.FC<IHandleSeverityRequestFormProps> = ({
  findingId,
  modalRef,
  vulnerabilities,
  onCancel,
  refetchData,
  clearSelected,
}: IHandleSeverityRequestFormProps): JSX.Element => {
  const { t } = useTranslation();

  const [vulnsToHandle, setVulnsToHandle] = useState<IVulnRowAttr[]>(
    getSeverityRequestVulns(vulnerabilities),
  );
  const [vulnsToApprove, setVulnsToApprove] = useState<IVulnRowAttr[]>([]);
  const [vulnsToReject, setVulnsToReject] = useState<IVulnRowAttr[]>([]);

  useEffect((): void => {
    setVulnsToHandle(vulnerabilities);
  }, [vulnerabilities]);

  const handleError = (error: GraphQLError): void => {
    if (
      error.message ===
      "Exception - Severity score update has not been requested yet"
    ) {
      msgError(
        t("severityUpdateRequests.handleRequestApproval.alerts.notRequested"),
      );
    } else if (
      error.message ===
      "Exception - Vulnerability severity update not available for current status"
    ) {
      msgError(
        t(
          "severityUpdateRequests.handleRequestApproval.alerts.invalidStateForUpdate",
        ),
      );
    } else {
      msgError(t("groupAlerts.errorTextsad"));
      Logger.error("Couldn't handle vuln severity requests", error);
    }
  };

  const [approveVulnSeverities, { loading: approvingSeverities }] = useMutation(
    APPROVE_VULNS_SEVERITY_REQUEST,
    {
      onCompleted: (result): void => {
        if (result.approveVulnerabilitiesSeverityUpdate.success) {
          msgSuccess(
            t("severityUpdateRequests.handleRequestApproval.success.approved"),
          );
          onCancel();
          refetchData();
          clearSelected();
        }
      },
      onError: ({ graphQLErrors }): void => {
        graphQLErrors.forEach((error): void => {
          handleError(error);
        });
      },
    },
  );

  const [rejectVulnSeverities, { loading: rejectingSeverities }] = useMutation(
    REJECT_VULNS_SEVERITY_REQUEST,
    {
      onCompleted: (result): void => {
        if (result.rejectVulnerabilitiesSeverityUpdate.success) {
          msgSuccess(
            t("severityUpdateRequests.handleRequestApproval.success.rejected"),
          );
          onCancel();
          refetchData();
          clearSelected();
        }
      },
      onError: ({ graphQLErrors }): void => {
        graphQLErrors.forEach((error): void => {
          handleError(error);
        });
      },
    },
  );

  const handleSubmit = useCallback(
    ({ justification }: IFormValues): void => {
      if (vulnsToApprove.length > 0)
        approveVulnSeverities({
          variables: {
            findingId,
            vulnerabilityIds: vulnsToApprove.map((vuln): string => vuln.id),
          },
        }).catch((): void => {
          Logger.error(
            "An error occurred approving vulnerabilities severity requests",
          );
        });

      if (vulnsToReject.length > 0)
        rejectVulnSeverities({
          variables: {
            findingId,
            justification,
            vulnerabilityIds: vulnsToReject.map((vuln): string => vuln.id),
          },
        }).catch((): void => {
          Logger.error(
            "An error occurred rejecting vulnerabilities severity requests",
          );
        });
    },
    [
      approveVulnSeverities,
      rejectVulnSeverities,
      findingId,
      vulnsToApprove,
      vulnsToReject,
    ],
  );

  const validationSchema = object().shape({
    justification: array().when([], (): Schema => {
      return vulnsToReject.length === 0
        ? string().notRequired()
        : string().required(t("validations.required"));
    }),
  });

  useEffect((): void => {
    setVulnsToApprove(
      vulnsToHandle.reduce(
        (acc: IVulnRowAttr[], vulnerability: IVulnRowAttr): IVulnRowAttr[] =>
          vulnerability.severityAcceptance === "APPROVED"
            ? [...acc, vulnerability]
            : acc,
        [],
      ),
    );
    setVulnsToReject(
      vulnsToHandle.reduce(
        (acc: IVulnRowAttr[], vulnerability: IVulnRowAttr): IVulnRowAttr[] =>
          vulnerability.severityAcceptance === "REJECTED"
            ? [...acc, vulnerability]
            : acc,
        [],
      ),
    );
  }, [vulnsToHandle]);

  return (
    <Modal
      id={"handleSeverityUpdateForm"}
      modalRef={{ ...modalRef, close: onCancel }}
      size={"lg"}
      title={t("severityUpdateRequests.handleRequestApproval.title")}
    >
      <Form
        cancelButton={{ onClick: onCancel }}
        defaultValues={{ justification: "" }}
        disabled={
          (vulnsToReject.length === 0 && vulnsToApprove.length === 0) ||
          approvingSeverities ||
          rejectingSeverities
        }
        onSubmit={handleSubmit}
        yupSchema={validationSchema}
      >
        <InnerForm>
          {({ formState, watch, register }): JSX.Element => (
            <Fragment>
              <p>
                {t("severityUpdateRequests.handleRequestApproval.description")}
              </p>
              <SeverityRequestsTable
                refetchData={refetchData}
                setVulnsToHandle={setVulnsToHandle}
                vulnsToHandle={vulnsToHandle}
              />
              {vulnsToReject.length === 0 ? undefined : (
                <Container>
                  <TextArea
                    error={formState.errors.justification?.message?.toString()}
                    label={t(
                      "severityUpdateRequests.handleRequestApproval.observations",
                    )}
                    maxLength={20000}
                    name={"justification"}
                    register={register}
                    required={true}
                    watch={watch}
                  />
                </Container>
              )}
            </Fragment>
          )}
        </InnerForm>
      </Form>
    </Modal>
  );
};

export { HandleSeverityRequestsForm };
