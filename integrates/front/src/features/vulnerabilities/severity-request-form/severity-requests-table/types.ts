import type { IVulnRowAttr } from "features/vulnerabilities/types";

interface ISeverityRequestsTableProps {
  vulnsToHandle: IVulnRowAttr[];
  setVulnsToHandle: (vulns: IVulnRowAttr[]) => void;
  refetchData: () => void;
}

export type { ISeverityRequestsTableProps };
