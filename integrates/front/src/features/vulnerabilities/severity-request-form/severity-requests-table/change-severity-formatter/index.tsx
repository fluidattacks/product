import { ApproveSeverityCheckBox } from "../approve-vuln-severity-check-box";
import type { IVulnRowAttr } from "features/vulnerabilities/types";

export const severityApprovalCheckboxButtons = (
  row: IVulnRowAttr,
  approveFunction: (arg1?: IVulnRowAttr) => void,
  deleteFunction: (arg1?: IVulnRowAttr) => void,
): JSX.Element => {
  return (
    <ApproveSeverityCheckBox
      approveFunction={approveFunction}
      rejectFunction={deleteFunction}
      vulnerabilityRow={row}
    />
  );
};
