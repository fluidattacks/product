import { Button } from "@fluidattacks/design";
import type { ColumnDef, Row as TableRow } from "@tanstack/react-table";
import type { FormEvent } from "react";
import { Fragment, StrictMode, useCallback, useState } from "react";
import { useTranslation } from "react-i18next";

import { severityApprovalCheckboxButtons } from "./change-severity-formatter";
import type { ISeverityRequestsTableProps } from "./types";

import { Table } from "components/table";
import type { ICellHelper } from "components/table/types";
import type { IVulnRowAttr } from "features/vulnerabilities/types";
import { VulnerabilityModal } from "features/vulnerabilities/vulnerability-modal";
import { useTable } from "hooks";
import { severityFormatter } from "utils/format-helpers";

const SeverityRequestsTable = ({
  vulnsToHandle,
  setVulnsToHandle,
  refetchData,
}: ISeverityRequestsTableProps): JSX.Element => {
  const { t } = useTranslation();
  const tableRef = useTable("handleSeverityChangesTable");
  const [selectedVulns, setSelectedVulns] = useState<IVulnRowAttr[]>([]);
  const [currentRow, setCurrentRow] = useState<IVulnRowAttr>();

  const openAdditionalInfoModal = useCallback(
    (rowInfo: TableRow<IVulnRowAttr>): ((event: FormEvent) => void) => {
      return (event: FormEvent): void => {
        setCurrentRow(rowInfo.original);
        event.stopPropagation();
      };
    },
    [],
  );

  const closeAdditionalInfoModal = useCallback((): void => {
    setCurrentRow(undefined);
  }, []);

  const handleReject = (vulnInfo?: IVulnRowAttr): void => {
    if (vulnInfo) {
      const newVulnList: IVulnRowAttr[] = vulnsToHandle.map(
        (vuln: IVulnRowAttr): IVulnRowAttr =>
          vuln.id === vulnInfo.id
            ? {
                ...vuln,
                severityAcceptance:
                  vuln.severityAcceptance === "REJECTED" ? "" : "REJECTED",
              }
            : vuln,
      );
      setVulnsToHandle([...newVulnList]);
      setSelectedVulns([
        ...newVulnList.filter(
          (vuln): boolean => vuln.severityAcceptance === "REJECTED",
        ),
        ...selectedVulns,
      ]);
    }
  };

  const handleApprove = (vulnInfo?: IVulnRowAttr): void => {
    if (vulnInfo) {
      const newVulnList: IVulnRowAttr[] = vulnsToHandle.map(
        (vuln: IVulnRowAttr): IVulnRowAttr =>
          vuln.id === vulnInfo.id
            ? {
                ...vuln,
                severityAcceptance:
                  vuln.severityAcceptance === "APPROVED" ? "" : "APPROVED",
              }
            : vuln,
      );
      setVulnsToHandle([...newVulnList]);
      setSelectedVulns([
        ...newVulnList.filter(
          (vuln): boolean => vuln.severityAcceptance === "APPROVED",
        ),
        ...selectedVulns,
      ]);
    }
  };

  const columns = [
    {
      accessorKey: "groupName",
      header: t(
        "severityUpdateRequests.handleRequestApproval.vulnTable.groupName",
      ),
    },
    {
      accessorKey: "where",
      header: t("severityUpdateRequests.handleRequestApproval.vulnTable.where"),
    },
    {
      accessorKey: "specific",
      header: t(
        "severityUpdateRequests.handleRequestApproval.vulnTable.specific",
      ),
    },
    {
      accessorKey: "severityThreatScore",
      cell: (cell: ICellHelper<IVulnRowAttr>): JSX.Element =>
        severityFormatter(cell.getValue()),
      header: t(
        "severityUpdateRequests.handleRequestApproval.vulnTable.severity",
      ),
    },
    {
      accessorKey: "proposedSeverityThreatScore",
      cell: (cell: ICellHelper<IVulnRowAttr>): JSX.Element =>
        severityFormatter(cell.getValue()),
      header: t(
        "severityUpdateRequests.handleRequestApproval.vulnTable.proposedSeverity",
      ),
    },
    {
      accessorKey: "severityAcceptance",
      cell: (cell: ICellHelper<IVulnRowAttr>): JSX.Element =>
        severityApprovalCheckboxButtons(
          cell.row.original,
          handleApprove,
          handleReject,
        ),
      header: t(
        "severityUpdateRequests.handleRequestApproval.vulnTable.severityAcceptance",
      ),
    },
  ];

  const handleOnChange = useCallback(
    (newValue: "APPROVED" | "REJECTED"): (() => void) => {
      return (): void => {
        const newVulnList: IVulnRowAttr[] = vulnsToHandle.map(
          (vuln: IVulnRowAttr): IVulnRowAttr => ({
            ...vuln,
            severityAcceptance: newValue,
          }),
        );
        setVulnsToHandle([...newVulnList]);
        setSelectedVulns([...newVulnList]);
      };
    },
    [vulnsToHandle, setVulnsToHandle],
  );

  return (
    <StrictMode>
      <Table
        columns={columns as ColumnDef<IVulnRowAttr>[]}
        data={vulnsToHandle}
        extraButtons={
          <Fragment>
            <Button
              icon={"check"}
              onClick={handleOnChange("APPROVED")}
              variant={"ghost"}
            >
              {t(
                "severityUpdateRequests.handleRequestApproval.vulnTable.approveAll",
              )}
            </Button>
            <Button
              icon={"xmark"}
              onClick={handleOnChange("REJECTED")}
              variant={"ghost"}
            >
              {t(
                "severityUpdateRequests.handleRequestApproval.vulnTable.rejectAll",
              )}
            </Button>
          </Fragment>
        }
        onRowClick={openAdditionalInfoModal}
        options={{ enableRowSelection: true }}
        rowSelectionSetter={setSelectedVulns}
        rowSelectionState={selectedVulns}
        tableRef={tableRef}
      />
      {currentRow ? (
        <VulnerabilityModal
          clearSelectedVulns={undefined}
          closeModal={closeAdditionalInfoModal}
          currentRow={currentRow}
          findingId={currentRow.findingId}
          groupName={currentRow.groupName}
          isFindingReleased={false}
          isModalOpen={true}
          refetchData={refetchData}
          severityUpdateRequest={true}
        />
      ) : undefined}
    </StrictMode>
  );
};

export { SeverityRequestsTable };
