import type { IVulnRowAttr } from "../types";

const getSeverityRequestVulns = (
  vulnerabilities: IVulnRowAttr[],
): IVulnRowAttr[] =>
  vulnerabilities.reduce(
    (
      severityRequestsVulns: IVulnRowAttr[],
      vuln: IVulnRowAttr,
    ): IVulnRowAttr[] => {
      return [...severityRequestsVulns, { severityAcceptance: "", ...vuln }];
    },
    [],
  );

export { getSeverityRequestVulns };
