/* eslint-disable max-lines */
import dayjs from "dayjs";
import _ from "lodash";
import type { UseTranslationResponse } from "react-i18next";

import { vulnerabilityStates } from "./types";
import type {
  IFormatVulns,
  IFormatVulnsTreatment,
  IReqFormat,
  IVulnRowAttr,
  TVulnerabilityStatesStrings,
} from "./types";
import type { IErrorInfoAttr } from "./upload-file/types";

import type { IOrganizationGroups } from "@types";
import type {
  RemoveVulnerabilityMutationMutation,
  VulnerabilityStateReason,
} from "gql/graphql";
import type { IHistoricTreatment } from "pages/finding/description/types";
import { getRequirementsText } from "pages/finding/description/utils";
import { isWithInAWeek } from "utils/date";
import { formatDropdownField } from "utils/format-helpers";
import { Logger } from "utils/logger";
import { msgError, msgErrorStick, msgSuccess } from "utils/notifications";
import { translate } from "utils/translations/translate";

const CRITERIA_ID_SLICE = 3;
const EMPTY_STRING = "";
const DASH_STRING = "-";

const requirementsTitle = ({
  findingTitle,
  requirementsData,
  vulnsData,
}: IReqFormat): string[] => {
  const findingNumber =
    !_.isNil(findingTitle) && findingTitle
      ? findingTitle.slice(0, CRITERIA_ID_SLICE)
      : EMPTY_STRING;
  if (
    !_.isNil(vulnsData) &&
    !_.isNil(findingNumber) &&
    findingNumber in vulnsData
  ) {
    const { requirements } = vulnsData[findingNumber];

    return getRequirementsText(requirements, requirementsData);
  }

  return [];
};

const formatTreatment = (
  treatmentStatus: string,
  state: TVulnerabilityStatesStrings,
  treatmentAcceptanceStatus: string | null,
): string => {
  const isPendingToApproval =
    (treatmentStatus === "ACCEPTED_UNDEFINED" ||
      treatmentStatus === "ACCEPTED") &&
    treatmentAcceptanceStatus !== "APPROVED";
  const isVulnOpen = state === vulnerabilityStates.VULNERABLE;
  const pendingApproval = isPendingToApproval
    ? translate.t("searchFindings.tabDescription.treatment.pendingApproval")
    : EMPTY_STRING;
  const treatmentLabel =
    translate.t(formatDropdownField(treatmentStatus)) + pendingApproval;

  return isVulnOpen ? treatmentLabel : DASH_STRING;
};

const formatVulnerabilities: ({
  requirementsData,
  vulnerabilities,
  vulnsData,
}: IFormatVulns) => IVulnRowAttr[] = ({
  requirementsData,
  vulnerabilities,
  vulnsData,
}): IVulnRowAttr[] =>
  vulnerabilities.map((vulnerability): IVulnRowAttr => {
    const isVulnOpen = vulnerability.state === vulnerabilityStates.VULNERABLE;
    const verification =
      vulnerability.verification === "Verified"
        ? `${vulnerability.verification} (${vulnerability.state.toLowerCase()})`
        : (vulnerability.verification as string);
    const isLastVerificationWithInAWeek = Boolean(
      isWithInAWeek(
        dayjs(vulnerability.lastVerificationDate, "YYYY-MM-DD hh:mm:ss A"),
      ),
    );
    const isVulnVerified =
      !_.isEmpty(vulnerability.lastVerificationDate) &&
      vulnerability.verification === "Verified";
    const shouldDisplayVerification = isVulnVerified
      ? isLastVerificationWithInAWeek
      : true;
    const requirements = requirementsTitle({
      findingTitle: vulnerability.finding?.title,
      requirementsData,
      vulnsData,
    });
    const formatDate = (date: string | null | undefined): string => {
      return _.isNil(date) ? DASH_STRING : date.split(" ")[0];
    };

    return {
      ...vulnerability,
      assigned: isVulnOpen
        ? (vulnerability.treatmentAssigned as string)
        : DASH_STRING,
      lastStateDate: formatDate(vulnerability.lastStateDate),
      lastTreatmentDate: isVulnOpen
        ? formatDate(vulnerability.lastTreatmentDate)
        : DASH_STRING,
      reportDate: _.isNull(vulnerability.reportDate)
        ? DASH_STRING
        : formatDate(vulnerability.reportDate),
      requirements,
      treatmentAssigned: isVulnOpen
        ? (vulnerability.treatmentAssigned as string)
        : DASH_STRING,
      treatmentDate: isVulnOpen
        ? formatDate(vulnerability.lastTreatmentDate)
        : DASH_STRING,
      treatmentStatus: formatTreatment(
        vulnerability.treatmentStatus,
        vulnerability.state,
        vulnerability.treatmentAcceptanceStatus,
      ),
      treatmentUser: isVulnOpen
        ? (vulnerability.treatmentUser as string)
        : DASH_STRING,
      verification: shouldDisplayVerification ? verification : EMPTY_STRING,
      vulnerabilityType: translate.t(
        `searchFindings.tabVuln.vulnTable.vulnerabilityType.${vulnerability.vulnerabilityType}`,
      ),
    };
  });

const formatHistoricTreatment = (
  vulnerability: IVulnRowAttr,
): IHistoricTreatment => {
  return {
    acceptanceDate: _.isNull(vulnerability.treatmentAcceptanceDate)
      ? undefined
      : vulnerability.treatmentAcceptanceDate,
    acceptanceStatus: _.isNull(vulnerability.treatmentAcceptanceStatus)
      ? undefined
      : vulnerability.treatmentAcceptanceStatus,
    assigned: _.isNull(vulnerability.treatmentAssigned)
      ? undefined
      : vulnerability.treatmentAssigned,
    date: vulnerability.lastTreatmentDate,
    justification: _.isNull(vulnerability.treatmentJustification)
      ? undefined
      : vulnerability.treatmentJustification,
    treatment: vulnerability.treatmentStatus,
    user: _.isNull(vulnerability.treatmentUser)
      ? EMPTY_STRING
      : vulnerability.treatmentUser,
  };
};

const getOrganizationGroups = (
  organizationsGroups: IOrganizationGroups[] | undefined,
  groupName: string,
): IOrganizationGroups | undefined => {
  const organizationGroups = organizationsGroups?.find(
    (orgGroup): boolean =>
      orgGroup.groups.find((group): boolean => group.name === groupName)
        ?.name === groupName,
  );

  return organizationGroups;
};

const formatVulnerabilitiesTreatment = ({
  organizationsGroups,
  vulnerabilities,
}: IFormatVulnsTreatment): IVulnRowAttr[] =>
  vulnerabilities.map((vulnerability): IVulnRowAttr => {
    const lastTreatment = formatHistoricTreatment(vulnerability);
    const organizationGroups = getOrganizationGroups(
      organizationsGroups,
      vulnerability.groupName,
    );

    return {
      ...vulnerability,
      historicTreatment: [lastTreatment],
      organizationName:
        organizationGroups === undefined
          ? EMPTY_STRING
          : organizationGroups.name,
    };
  });

function filterZeroRisk(vulnerabilities: IVulnRowAttr[]): IVulnRowAttr[] {
  return vulnerabilities.filter(
    (vuln): boolean => vuln.zeroRisk !== "Confirmed",
  );
}

function filterSafeVulns(vulnerabilities: IVulnRowAttr[]): IVulnRowAttr[] {
  return vulnerabilities.filter((vuln): boolean => vuln.state !== "SAFE");
}

function getNonSelectableVulnerabilitiesOnApproveSeverityUpdate(
  vulnerabilities: IVulnRowAttr[],
): string[] {
  return vulnerabilities.reduce(
    (
      nonSelectableVulnerabilities: string[],
      vulnerability: IVulnRowAttr,
    ): string[] => {
      const isVulnNonSelectable =
        vulnerability.remediated ||
        vulnerability.state === vulnerabilityStates.SAFE;

      return isVulnNonSelectable
        ? [...nonSelectableVulnerabilities, vulnerability.id]
        : nonSelectableVulnerabilities;
    },
    [],
  );
}

function getNonSelectableVulnerabilitiesOnReattackIds(
  vulnerabilities: IVulnRowAttr[],
  hasAdvanced = true,
): string[] {
  return vulnerabilities.reduce(
    (
      nonSelectableVulnerabilities: string[],
      vulnerability: IVulnRowAttr,
    ): string[] => {
      const isVulnNonSelectable =
        vulnerability.remediated ||
        vulnerability.state === vulnerabilityStates.REJECTED ||
        vulnerability.state === vulnerabilityStates.SAFE ||
        vulnerability.state === vulnerabilityStates.SUBMITTED ||
        vulnerability.verification?.toLowerCase() === "on_hold" ||
        (!hasAdvanced && vulnerability.source !== "machine");

      return isVulnNonSelectable
        ? [...nonSelectableVulnerabilities, vulnerability.id]
        : nonSelectableVulnerabilities;
    },
    [],
  );
}

function getNonSelectableVulnerabilitiesOnVerifyIds(
  vulnerabilities: IVulnRowAttr[],
): string[] {
  return vulnerabilities.reduce(
    (
      nonSelectableVulnerabilities: string[],
      vulnerability: IVulnRowAttr,
    ): string[] =>
      vulnerability.remediated &&
      vulnerability.state === vulnerabilityStates.VULNERABLE
        ? nonSelectableVulnerabilities
        : [...nonSelectableVulnerabilities, vulnerability.id],
    [],
  );
}

function getNonSelectableVulnerabilitiesOnDeleteIds(
  vulnerabilities: IVulnRowAttr[],
): string[] {
  return vulnerabilities.reduce(
    (
      nonSelectableVulnerabilities: string[],
      vulnerability: IVulnRowAttr,
    ): string[] =>
      vulnerability.state === vulnerabilityStates.SAFE
        ? [...nonSelectableVulnerabilities, vulnerability.id]
        : nonSelectableVulnerabilities,
    [],
  );
}

function getNonSelectableVulnerabilitiesOnCloseIds(
  vulnerabilities: IVulnRowAttr[],
): string[] {
  return vulnerabilities.reduce<string[]>(
    (nonSelectableVulnerabilities, vulnerability): string[] =>
      vulnerability.state === vulnerabilityStates.VULNERABLE
        ? nonSelectableVulnerabilities
        : [...nonSelectableVulnerabilities, vulnerability.id],
    [],
  );
}

function getNonSelectableVulnerabilitiesOnResubmitIds(
  vulnerabilities: IVulnRowAttr[],
): string[] {
  return vulnerabilities.reduce(
    (
      nonSelectableVulnerabilities: string[],
      vulnerability: IVulnRowAttr,
    ): string[] =>
      vulnerability.state === vulnerabilityStates.REJECTED
        ? nonSelectableVulnerabilities
        : [...nonSelectableVulnerabilities, vulnerability.id],
    [],
  );
}

function filterByExclude(
  all: IVulnRowAttr[],
  ignoredIds: string[],
): IVulnRowAttr[] {
  return all.filter(
    (vulnerability): boolean => !ignoredIds.includes(vulnerability.id),
  );
}

function filterOutVulnerabilities(
  selectedVulnerabilities: IVulnRowAttr[],
  allVulnerabilities: IVulnRowAttr[],
  filter: (vulnerabilities: IVulnRowAttr[], hasAdvanced?: boolean) => string[],
  hasAdvanced = true,
  shouldHideRelease = false,
): IVulnRowAttr[] {
  const ignoredIds = filter(allVulnerabilities, hasAdvanced);

  const nonSelectableReleased = shouldHideRelease
    ? allVulnerabilities
        .filter((vuln): boolean => vuln.state === "VULNERABLE")
        .map((vuln): string => vuln.id)
    : [];

  return filterByExclude(selectedVulnerabilities, [
    ...ignoredIds,
    ...nonSelectableReleased,
  ]);
}

const formatError = (errorName: string, errorValue: string): string => {
  return ` ${translate.t(errorName)} "${errorValue}" ${translate.t(
    "groupAlerts.invalid",
  )}. `;
};

const includedMessage = (message: string): void => {
  if (message.includes("Exception - Error in path value")) {
    const errorObject: IErrorInfoAttr = JSON.parse(message);
    msgErrorStick(`${translate.t("groupAlerts.pathValue")}
    ${formatError("groupAlerts.value", errorObject.values)}`);
  } else if (
    message.includes("Exception - The commit hash format is invalid")
  ) {
    const errorObject: IErrorInfoAttr = JSON.parse(message);
    msgErrorStick(`${translate.t("groupAlerts.commitHashFormat")}
    ${formatError("groupAlerts.value", errorObject.values)}`);
  } else if (message.includes("Exception - Error in port value")) {
    const errorObject: IErrorInfoAttr = JSON.parse(message);
    msgErrorStick(`${translate.t("groupAlerts.portValue")}
    ${formatError("groupAlerts.value", errorObject.values)}`);
  } else if (
    message.includes(
      "Exception - Invalid stream should start 'home' or 'query'",
    )
  ) {
    const destructMsg: { msg: string; path: string } = JSON.parse(message);
    msgError(
      translate.t("searchFindings.tabVuln.alerts.uploadFile.invalidStream", {
        path: destructMsg.path,
      }),
    );
  } else if (
    message.includes(
      "Exception - New vulnerabilities require the submitted status",
    )
  ) {
    const { where, specific }: { where: string; specific: string } =
      JSON.parse(message);
    msgError(
      translate.t(
        "searchFindings.tabVuln.alerts.uploadFile.submittedRequired",
        {
          specific,
          where,
        },
      ),
    );
  } else if (
    message.includes("Exception - Uploaded vulnerability is already open")
  ) {
    const { where, specific }: { where: string; specific: string } =
      JSON.parse(message);
    msgError(
      translate.t("searchFindings.tabVuln.alerts.uploadFile.alreadyOpen", {
        specific,
        where,
      }),
    );
  } else if (
    message.includes(
      "Exception - Uploaded vulnerability can not change the status",
    )
  ) {
    const {
      specific,
      status,
      where,
    }: { where: string; specific: string; status: string } =
      JSON.parse(message);
    msgError(
      translate.t(
        "searchFindings.tabVuln.alerts.uploadFile.canNotChangeStatus",
        {
          specific,
          status,
          where,
        },
      ),
    );
  } else {
    msgError(translate.t("groupAlerts.invalidSpecific"));
    Logger.error("An error occurred uploading vulnerabilities file", message);
  }
};

const errorMessageHelper = (message: string): void => {
  switch (message) {
    case "Exception - Access denied or root not found":
      msgError(
        translate.t("searchFindings.tabVuln.alerts.uploadFile.invalidRoot"),
      );
      break;
    case "Exception - Error in specific value":
      msgError(translate.t("groupAlerts.invalidSpecific"));
      break;
    case "Exception - Error Uploading File to S3":
      msgError(translate.t("groupAlerts.errorTextsad"));
      break;
    case "Exception - Invalid characters":
      msgError(translate.t("validations.invalidChar"));
      break;
    case "Exception - Invalid file size":
      msgError(translate.t("validations.fileSize", { count: 1 }));
      break;
    case "Exception - Invalid file type":
      msgError(translate.t("groupAlerts.fileTypeYaml"));
      break;
    case "Exception - The commit hash is invalid":
      msgError(translate.t("groupAlerts.invalidCommitHash"));
      break;
    case "Exception - You can upload a maximum of 100 vulnerabilities per file":
      msgError(translate.t("groupAlerts.invalidNOfVulns"));
      break;
    case "Expected path to start with the repo nickname":
      msgError(translate.t("groupAlerts.expectedPathToStartWithRepo"));
      break;
    case "Expected vulnerability to have repo_nickname":
      msgError(translate.t("groupAlerts.expectedVulnToHaveNickname"));
      break;
    case "Invalid, only New vulnerabilities with Open state are allowed":
      msgError(translate.t("groupAlerts.onlyNewVulnerabilitiesOpenState"));
      break;
    case "Invalid, only New vulnerabilities with submitted state are allowed":
      msgError(translate.t("groupAlerts.onlyNewVulnerabilitiesSubmittedState"));
      break;
    case "Invalid, you cannot change the nickname while closing":
      msgError(
        translate.t("groupAlerts.invalidCannotModifyNicknameWhenClosing"),
      );
      break;
    default:
      includedMessage(message);
      break;
  }
};

/**
 * The following helper displays a message depending on the
 * `removeVulnerabilityResult` success value
 */
const onRemoveVulnerabilityResultHelper = (
  removeVulnResult: RemoveVulnerabilityMutationMutation,
  t: UseTranslationResponse<"translation", unknown>["t"],
): void => {
  if (removeVulnResult.removeVulnerability.success) {
    msgSuccess(
      t("searchFindings.tabDescription.vulnDeleted"),
      t("groupAlerts.titleSuccess"),
    );
  } else {
    msgError(t("deleteVulns.notSuccess"));
  }
};

const formatVulnerabilitiesReason = (
  reasons: VulnerabilityStateReason[] | null,
): string[] | null => {
  if (_.isNil(reasons)) return null;

  return reasons.map((reason: VulnerabilityStateReason): string =>
    _.capitalize(reason.valueOf()),
  );
};

const getTimeToDetect = (
  reportDateStr: string | null | undefined,
  commitDateStr: string | null | undefined,
): number | undefined => {
  if (_.isNil(reportDateStr) || _.isEmpty(reportDateStr)) return undefined;
  if (_.isNil(commitDateStr) || _.isEmpty(commitDateStr)) return undefined;

  const reportDate = new Date(reportDateStr);
  if (isNaN(reportDate.getTime())) return undefined;
  const commitDate = new Date(commitDateStr);
  if (isNaN(commitDate.getTime())) return undefined;

  if (reportDate < commitDate) return undefined;

  return dayjs(reportDate).diff(dayjs(commitDate), "day");
};

export {
  errorMessageHelper,
  filterOutVulnerabilities,
  filterSafeVulns,
  filterZeroRisk,
  formatHistoricTreatment,
  formatTreatment,
  formatVulnerabilities,
  formatVulnerabilitiesReason,
  formatVulnerabilitiesTreatment,
  getNonSelectableVulnerabilitiesOnApproveSeverityUpdate,
  getNonSelectableVulnerabilitiesOnCloseIds,
  getNonSelectableVulnerabilitiesOnDeleteIds,
  getNonSelectableVulnerabilitiesOnReattackIds,
  getNonSelectableVulnerabilitiesOnResubmitIds,
  getNonSelectableVulnerabilitiesOnVerifyIds,
  getOrganizationGroups,
  getTimeToDetect,
  onRemoveVulnerabilityResultHelper,
};
