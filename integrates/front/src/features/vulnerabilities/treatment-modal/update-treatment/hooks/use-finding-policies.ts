import { useQuery } from "@apollo/client";
import isEmpty from "lodash/isEmpty";

import {
  GET_FINDING_TITLE_AND_ORG,
  GET_ORGANIZATION_POLICIES,
} from "../queries";
import type { IVulnerabilityPoliciesData } from "pages/organization/policies/acceptance/vulnerability-policies/types";
import { Logger } from "utils/logger";

const useFindingTitleAndPolicies = (
  findingId?: string,
): {
  findingTitle: string | undefined;
  policies: IVulnerabilityPoliciesData[] | undefined;
} => {
  const { data } = useQuery(GET_FINDING_TITLE_AND_ORG, {
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error): void => {
        Logger.warning("An error occurred fetching finding", error);
      });
    },
    skip: findingId === undefined || isEmpty(findingId),
    variables: { findingId: findingId ?? "" },
  });

  const findingTitle = data?.finding.title;
  const organizationName = data?.finding.organizationName;

  const { data: policiesData } = useQuery(GET_ORGANIZATION_POLICIES, {
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error): void => {
        Logger.warning(
          "An error occurred fetching organization policies",
          error,
        );
      });
    },
    skip: organizationName === undefined,
    variables: { organizationName: organizationName ?? "" },
  });

  const policies = policiesData?.organizationId
    .findingPolicies as IVulnerabilityPoliciesData[];

  return { findingTitle, policies };
};

export { useFindingTitleAndPolicies };
