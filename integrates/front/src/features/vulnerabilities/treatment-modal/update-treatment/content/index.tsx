import {
  Col,
  ComboBox,
  Container,
  Input,
  InputDate,
  InputTags,
  Row,
  Slider,
  TextArea,
  useWatch,
} from "@fluidattacks/design";
import { getLocalTimeZone, today } from "@internationalized/date";
import some from "lodash/some";
import { useCallback, useEffect, useState } from "react";
import { useTranslation } from "react-i18next";

import type { ITreatmentContent } from "../types";
import { getTreatmentPristine } from "../utils";

const ALLOWED_TREATMENTS = ["IN_PROGRESS", "ACCEPTED", "ACCEPTED_UNDEFINED"];
const TREATMENTS = ["UNTREATED", "REQUEST_ZERO_RISK"].concat(
  ALLOWED_TREATMENTS,
);
const PREFIX = "searchFindings.tabDescription.";

const hasTreatment = (treatment: string, iterable?: string[]): boolean =>
  some(
    iterable ?? ALLOWED_TREATMENTS,
    (value: string): boolean => value === treatment,
  );

const TreatmentContent = ({
  canDeleteVulnsTags,
  canRequestZeroRiskVuln,
  canUpdateVulnsTreatment,
  handleDeletion,
  handleMessage,
  items,
  isActiveFindingPolicy,
  isPristine,
  lastTreatment,
  loadingUsers,
  maxDays,
  setIsPristine,
  setUpdateTreatment,
  showSource,
  sourceOptions,
  userEmails,
  // React hook form props
  control,
  formState: { defaultValues: defaults, errors, isValid },
  getFieldState,
  getValues,
  register,
  setValue,
  watch,
}: ITreatmentContent): JSX.Element => {
  const { t } = useTranslation();
  const minDate = today(getLocalTimeZone()).add({ days: 1 });
  const maxDate = today(getLocalTimeZone()).add({ days: maxDays });

  const { isTouched: touchedJust } = getFieldState("justification");
  const { isTouched: touchedBug } = getFieldState("externalBugTrackingSystem");
  const acceptanceDate = useWatch({ control, name: "acceptanceDate" });
  const [treatmentValue, setTreatmentValue] = useState(
    defaults?.treatment ?? "",
  );
  const [showDate, setShowDate] = useState(treatmentValue === "ACCEPTED");
  const [showField, setShowField] = useState(hasTreatment(treatmentValue));
  const showUser = lastTreatment.acceptanceStatus === "APPROVED" && showField;

  const handleTreatmentChange = useCallback((value: string): void => {
    if (value === "") {
      setShowDate(false);
      setShowField(false);
    }
    if (hasTreatment(value, TREATMENTS)) {
      setTreatmentValue(value);
      setShowDate(value === "ACCEPTED");
      setShowField(hasTreatment(value));
    }
  }, []);

  useEffect((): void => {
    const treatmentState = getTreatmentPristine({
      initialValues: defaults ?? {},
      values: getValues(),
    });
    if (isPristine !== treatmentState) setIsPristine(treatmentState);
    if (setUpdateTreatment)
      setUpdateTreatment(
        showField ||
          treatmentValue === "REQUEST_ZERO_RISK" ||
          !(canRequestZeroRiskVuln || canUpdateVulnsTreatment),
      );

    if (treatmentValue === "ACCEPTED_UNDEFINED") {
      handleMessage(t(`${PREFIX}approvalMessage`, { treatment: "permanent" }));
    } else if (treatmentValue === "ACCEPTED") {
      handleMessage(
        t(`${PREFIX}temporaryApprovalMessage`, {
          date: acceptanceDate,
          note: isActiveFindingPolicy
            ? t(`${PREFIX}approvalMessage`, { treatment: "temporary" })
            : "",
        }),
      );
    }
  }, [
    acceptanceDate,
    canRequestZeroRiskVuln,
    canUpdateVulnsTreatment,
    defaults,
    getValues,
    isActiveFindingPolicy,
    isPristine,
    handleMessage,
    showField,
    setUpdateTreatment,
    setIsPristine,
    t,
    treatmentValue,
  ]);

  return (
    <Container display={"flex"} flexDirection={"column"} gap={1.25}>
      <Container display={"flex"} flexDirection={"column"} gap={1}>
        <Row>
          <Col lg={50} md={50} sm={50}>
            <ComboBox
              control={control}
              disabled={!(canRequestZeroRiskVuln || canUpdateVulnsTreatment)}
              items={items}
              label={t(`${PREFIX}treatment.title`)}
              name={"treatment"}
              onChange={handleTreatmentChange}
            />
          </Col>
          {showField ? (
            <Col lg={50} md={50} sm={50}>
              <ComboBox
                control={control}
                disabled={!canUpdateVulnsTreatment}
                items={userEmails.map(
                  (email): { name: string; value: string } => ({
                    name: email,
                    value: email,
                  }),
                )}
                label={t(`${PREFIX}assigned.text`)}
                loadingItems={loadingUsers}
                name={"assigned"}
                placeholder={t(`${PREFIX}assigned.placeholder`)}
                required={true}
              />
            </Col>
          ) : undefined}
        </Row>
        {showUser ? (
          <Input
            disabled={true}
            label={t(`${PREFIX}acceptanceUser`)}
            name={"acceptanceUser"}
            register={register}
          />
        ) : undefined}
        <TextArea
          disabled={!(canUpdateVulnsTreatment || canRequestZeroRiskVuln)}
          error={errors.justification?.message?.toString()}
          isTouched={touchedJust}
          isValid={isValid}
          label={t(`${PREFIX}treatmentJust`)}
          maxLength={10000}
          name={"justification"}
          register={register}
          required={true}
          watch={watch}
        />
      </Container>
      {showDate ? (
        <InputDate
          error={errors.acceptanceDate?.message?.toString()}
          helpLink={
            "https://help.fluidattacks.com/portal/en/kb/articles/manage-general-policies#Maximum_number_of_calendar_days_a_finding_can_be_temporarily_accepted"
          }
          helpLinkText={"See here"}
          helpText={`Date must be after ${minDate.toString()} and before ${maxDate.toString()}`}
          label={t(`${PREFIX}acceptanceDate`)}
          maxValue={maxDate}
          minValue={minDate}
          name={"acceptanceDate"}
          register={register}
          required={true}
          setValue={setValue}
          watch={watch}
        />
      ) : undefined}
      {showField ? (
        <Input
          disabled={!canUpdateVulnsTreatment}
          error={errors.externalBugTrackingSystem?.message?.toString()}
          helpLink={
            "https://help.fluidattacks.com/portal/en/kb/integrate-with-fluid-attacks"
          }
          helpLinkText={"here"}
          helpText={"Learn about our integrations"}
          isTouched={touchedBug}
          isValid={isValid}
          label={t(`${PREFIX}bts`)}
          name={"externalBugTrackingSystem"}
          placeholder={t(`${PREFIX}btsPlaceholder`)}
          register={register}
        />
      ) : undefined}
      <InputTags
        control={control}
        disabled={!(canUpdateVulnsTreatment && canDeleteVulnsTags)}
        error={errors.tag?.message?.toString()}
        helpText={
          "Remember to add tags to your vulnerabilities to ease their management"
        }
        label={t(`${PREFIX}tag`)}
        name={"tag"}
        onRemove={handleDeletion}
      />
      {showField ? (
        <Slider
          isDisabled={!canUpdateVulnsTreatment}
          label={t(`${PREFIX}businessPriority`)}
          maxValue={1000}
          minValue={-1000}
          name={"customSeverity"}
          setValue={setValue}
          watch={watch}
        />
      ) : undefined}
      {showSource ? (
        <Col lg={50} md={50} sm={50}>
          <ComboBox
            control={control}
            items={sourceOptions}
            label={t("searchFindings.tabVuln.vulnTable.source")}
            name={"source"}
          />
        </Col>
      ) : undefined}
    </Container>
  );
};

export { TreatmentContent };
