import { PureAbility } from "@casl/ability";
import { fireEvent, screen, waitFor } from "@testing-library/react";
import { userEvent } from "@testing-library/user-event";
import { GraphQLError } from "graphql";
import { HttpResponse, type StrictResponse, graphql } from "msw";
import * as React from "react";

import {
  REQUEST_VULNS_ZERO_RISK,
  UPDATE_VULNERABILITY_MUTATION,
} from "./queries";
import type { IUpdateVulnerabilityResultAttr } from "./types";
import { getLastTreatment, groupLastHistoricTreatment } from "./utils";

import { TreatmentForm } from ".";
import type { IVulnDataTypeAttr } from "../../types";
import {
  authzGroupContext,
  authzPermissionsContext,
} from "context/authz/config";
import type {
  GetFindingVulnsQueriesQuery as GetFindingVulnsQueries,
  RequestVulnerabilitiesZeroRiskMutation as RequestVulnerabilitiesZeroRisk,
  UpdateVulnerabilityMutationMutation as UpdateVulnerability,
} from "gql/graphql";
import {
  UpdateClientDescriptionTreatment,
  VulnerabilitySource,
  VulnerabilityState,
} from "gql/graphql";
import { render } from "mocks";
import { LINK } from "mocks/constants";
import type { IErrorMessage, IMessage } from "mocks/types";
import type { IHistoricTreatment } from "pages/finding/description/types";
import { GET_FINDING_VULNS_QUERIES } from "pages/finding/vulnerabilities/queries";
import { msgError, msgSuccess } from "utils/notifications";
import { translate } from "utils/translations/translate";

jest.mock("utils/notifications", (): Record<string, unknown> => {
  const mockedNotifications: Record<string, () => Record<string, unknown>> =
    jest.requireActual("utils/notifications");
  jest.spyOn(mockedNotifications, "msgError").mockImplementation();
  jest.spyOn(mockedNotifications, "msgSuccess").mockImplementation();

  return mockedNotifications;
});

const WrapperComponent = ({
  children,
  permissions,
}: Readonly<{
  children: React.ReactNode;
  permissions: PureAbility<string>;
}>): JSX.Element => {
  return (
    <authzPermissionsContext.Provider value={permissions}>
      {children}
    </authzPermissionsContext.Provider>
  );
};

describe("treatment Form component", (): void => {
  const btnConfirm = "Confirm";
  const graphqlMocked = graphql.link(LINK);

  const memoryRouter = {
    initialEntries: ["/orgs/okada"],
  };

  const vulns: IVulnDataTypeAttr[] = [
    {
      assigned: "",
      customSeverity: 2,
      externalBugTrackingSystem: null,
      findingId: "422286126",
      groupName: "testgroupname",
      historicTreatment: [
        {
          date: "",
          justification: "test justification",
          treatment: "UNTREATED",
          user: "",
        },
      ],
      id: "ab25380d-dfe1-4cde-aefd-acca6990d6aa",
      source: "asm",
      specific: "",
      state: "VULNERABLE",
      tag: "one",
      where: "",
    },
  ];

  const mocksVulns = [
    graphqlMocked.query(
      GET_FINDING_VULNS_QUERIES,
      ({ variables }): StrictResponse<{ data: GetFindingVulnsQueries }> => {
        const { findingId } = variables;

        return HttpResponse.json({
          data: {
            __typename: "Query",
            ...{
              finding: {
                __typename: "Finding",
                draftsConnection: undefined,
                id: findingId,
                vulnerabilitiesConnection: {
                  edges: [
                    {
                      node: {
                        __typename: "Vulnerability",
                        advisories: null,
                        customSeverity: null,
                        externalBugTrackingSystem: null,
                        findingId: "422286126",
                        id: "",
                        lastStateDate: null,
                        lastTreatmentDate: null,
                        lastVerificationDate: null,
                        remediated: false,
                        reportDate: "",
                        rootNickname: null,
                        severityTemporalScore: 0,
                        severityThreatScore: 0,
                        source: "",
                        specific: "",
                        state: VulnerabilityState.Vulnerable,
                        stream: null,
                        tag: "",
                        technique: null,
                        treatmentAcceptanceDate: null,
                        treatmentAcceptanceStatus: null,
                        treatmentAssigned: null,
                        treatmentJustification: null,
                        treatmentStatus: null,
                        treatmentUser: null,
                        verification: null,
                        vulnerabilityType: "",
                        where: "",
                        zeroRisk: null,
                      },
                    },
                  ],
                  pageInfo: {
                    endCursor: "test-cursor=",
                    hasNextPage: false,
                  },
                },
                zeroRiskConnection: undefined,
              },
            },
          },
        });
      },
    ),
  ];

  const mockedPermissions = new PureAbility<string>([
    { action: "integrates_api_mutations_remove_vulnerability_tags_mutate" },
    {
      action:
        "integrates_api_mutations_request_vulnerabilities_zero_risk_mutate",
    },
    {
      action: "integrates_api_mutations_update_vulnerability_treatment_mutate",
    },
    {
      action:
        "integrates_api_mutations_update_vulnerabilities_treatment_mutate",
    },
    { action: "integrates_api_resolvers_group_stakeholders_resolve" },
  ]);

  it("should group last treatment", (): void => {
    expect.hasAssertions();

    const treatment: IHistoricTreatment = {
      date: "",
      justification: "test justification",
      treatment: "IN PROGRESS",
      user: "",
    };

    const vulnerabilities: IVulnDataTypeAttr[] = [
      {
        assigned: "",
        customSeverity: null,
        externalBugTrackingSystem: null,
        findingId: "422286126",
        groupName: "",
        historicTreatment: [treatment],
        id: "test_one",
        source: "asm",
        specific: "",
        state: "VULNERABLE",
        tag: "one",
        where: "",
      },
      {
        assigned: "",
        customSeverity: null,
        externalBugTrackingSystem: null,
        findingId: "422286126",
        groupName: "",
        historicTreatment: [treatment],
        id: "test_two",
        source: "asm",
        specific: "",
        state: "VULNERABLE",
        tag: "one",
        where: "",
      },
    ];

    const lastTreatment: IHistoricTreatment =
      groupLastHistoricTreatment(vulnerabilities);

    expect(lastTreatment).toStrictEqual(getLastTreatment([treatment]));
  });

  it("list editable fields", async (): Promise<void> => {
    expect.hasAssertions();

    jest.clearAllMocks();

    const handleOnClose: jest.Mock = jest.fn();
    const handleClearSelected: jest.Mock = jest.fn();
    const handleRefetchData: jest.Mock = jest.fn();
    render(
      <authzPermissionsContext.Provider value={mockedPermissions}>
        <TreatmentForm
          close={handleOnClose}
          findingId={"422286126"}
          groupName={"testgroupname"}
          handleClearSelected={handleClearSelected}
          refetchData={handleRefetchData}
          vulnerabilities={vulns}
        />
      </authzPermissionsContext.Provider>,
    );

    await waitFor((): void => {
      expect(
        screen.queryAllByRole("combobox", { name: "treatment" }),
      ).toHaveLength(1);
    });

    expect(screen.queryAllByRole("textbox")).toHaveLength(2);

    await userEvent.click(
      screen.getByText("searchFindings.tabDescription.treatment.title"),
    );
    await screen.findByText(
      "searchFindings.tabDescription.treatment.inProgress",
    );
    await userEvent.click(
      screen.getByText("searchFindings.tabDescription.treatment.inProgress"),
    );
    const numberOfEditableFields = 3;

    await waitFor((): void => {
      expect(screen.queryAllByRole("textbox")).toHaveLength(
        numberOfEditableFields,
      );
    });
    ["justification", "externalBugTrackingSystem", "tag"].forEach(
      (inputName): void => {
        expect(
          screen.queryByRole("textbox", { name: inputName }),
        ).toBeInTheDocument();
      },
    );

    expect(screen.getByRole("slider", { name: "customSeverity" })).toHaveValue(
      String(vulns[0].customSeverity),
    );
  });

  it("should handle request zero risk", async (): Promise<void> => {
    expect.hasAssertions();

    jest.clearAllMocks();

    const handleOnClose: jest.Mock = jest.fn();
    const handleClearSelected: jest.Mock = jest.fn();
    const handleRefetchData: jest.Mock = jest.fn();

    const mocksMutation = [
      graphqlMocked.mutation(
        REQUEST_VULNS_ZERO_RISK,
        ({
          variables,
        }): StrictResponse<
          IErrorMessage | { data: RequestVulnerabilitiesZeroRisk }
        > => {
          const { findingId, justification } = variables;
          if (
            findingId === "422286126" &&
            justification ===
              "This is a commenting test of a request zero risk in vulns"
          ) {
            return HttpResponse.json({
              data: {
                requestVulnerabilitiesZeroRisk: { success: true },
              },
            });
          }

          return HttpResponse.json({
            errors: [new GraphQLError("Error requesting zero risk")],
          });
        },
      ),
    ];
    render(
      <WrapperComponent permissions={mockedPermissions}>
        <authzGroupContext.Provider
          value={
            new PureAbility([
              { action: "can_report_vulnerabilities" },
              { action: "can_request_zero_risk" },
            ])
          }
        >
          <TreatmentForm
            close={handleOnClose}
            findingId={"422286126"}
            groupName={"testgroupname"}
            handleClearSelected={handleClearSelected}
            refetchData={handleRefetchData}
            vulnerabilities={vulns}
          />
        </authzGroupContext.Provider>
      </WrapperComponent>,
      {
        memoryRouter,
        mocks: [...mocksVulns, ...mocksMutation],
      },
    );

    await waitFor((): void => {
      expect(
        screen.queryAllByRole("combobox", { name: "treatment" }),
      ).toHaveLength(1);
    });

    expect(screen.getByText(btnConfirm)).toBeDisabled();

    await userEvent.click(
      screen.getByText("searchFindings.tabDescription.treatment.title"),
    );
    await userEvent.click(
      screen.getByText(
        "searchFindings.tabDescription.treatment.requestZeroRisk",
      ),
    );

    await userEvent.type(
      screen.getByRole("textbox", { name: "justification" }),
      "This is a commenting test of a request zero risk in vulns",
    );
    await waitFor((): void => {
      expect(screen.getByText(btnConfirm)).not.toBeDisabled();
    });

    await userEvent.click(screen.getByText(btnConfirm));

    await waitFor((): void => {
      expect(msgSuccess).toHaveBeenCalledWith(
        "Zero risk vulnerability has been requested",
        "Correct!",
      );
    });

    expect(handleClearSelected).toHaveBeenCalledWith();
    expect(handleOnClose).toHaveBeenCalledWith();
    expect(handleRefetchData).toHaveBeenCalledTimes(1);
  });

  it("should handle request zero risk error", async (): Promise<void> => {
    expect.hasAssertions();

    jest.clearAllMocks();

    const handleOnClose: jest.Mock = jest.fn();
    const handleClearSelected: jest.Mock = jest.fn();
    const handleRefetchData: jest.Mock = jest.fn();

    const mocksMutation = [
      graphqlMocked.mutation(
        REQUEST_VULNS_ZERO_RISK,
        ({
          variables,
        }): StrictResponse<
          | { data: RequestVulnerabilitiesZeroRisk }
          | { errors: [IMessage, IMessage] }
        > => {
          const { findingId, justification } = variables;
          if (
            findingId === "422286126" &&
            justification ===
              "This is a commenting test of a request zero risk in vulns"
          ) {
            return HttpResponse.json({
              errors: [
                new GraphQLError(
                  "Exception - Zero risk vulnerability is already requested",
                ),
                new GraphQLError(
                  "Exception - Justification must have a maximum of 5000 characters",
                ),
              ],
            });
          }

          return HttpResponse.json({
            data: {
              requestVulnerabilitiesZeroRisk: { success: true },
            },
          });
        },
      ),
    ];
    render(
      <WrapperComponent permissions={mockedPermissions}>
        <authzGroupContext.Provider
          value={
            new PureAbility([
              { action: "can_report_vulnerabilities" },
              { action: "can_request_zero_risk" },
            ])
          }
        >
          <TreatmentForm
            close={handleOnClose}
            findingId={"422286126"}
            groupName={"testgroupname"}
            handleClearSelected={handleClearSelected}
            refetchData={handleRefetchData}
            vulnerabilities={vulns}
          />
        </authzGroupContext.Provider>
      </WrapperComponent>,
      {
        memoryRouter,
        mocks: [...mocksVulns, ...mocksMutation],
      },
    );

    await waitFor((): void => {
      expect(
        screen.queryAllByRole("combobox", { name: "treatment" }),
      ).toHaveLength(1);
    });

    expect(screen.getByText(btnConfirm)).toBeDisabled();

    await userEvent.click(
      screen.getByText("searchFindings.tabDescription.treatment.title"),
    );
    await userEvent.click(
      screen.getByText(
        "searchFindings.tabDescription.treatment.requestZeroRisk",
      ),
    );

    await userEvent.type(
      screen.getByRole("textbox", { name: "justification" }),
      "This is a commenting test of a request zero risk in vulns",
    );
    await waitFor((): void => {
      expect(screen.getByText(btnConfirm)).not.toBeDisabled();
    });

    await userEvent.click(screen.getByText(btnConfirm));

    await waitFor((): void => {
      expect(msgError).toHaveBeenNthCalledWith(
        1,
        translate.t("groupAlerts.zeroRiskAlreadyRequested"),
      );
    });

    expect(handleClearSelected).not.toHaveBeenCalled();
    expect(handleOnClose).not.toHaveBeenCalled();
    expect(handleRefetchData).not.toHaveBeenCalled();
    expect(msgError).toHaveBeenNthCalledWith(
      2,
      translate.t("validations.invalidFieldLength"),
    );
  });

  it("should render update treatment", async (): Promise<void> => {
    expect.hasAssertions();

    jest.clearAllMocks();

    const handleClearSelected: jest.Mock = jest.fn();
    const handleOnClose: jest.Mock = jest.fn();
    const handleRefetchData: jest.Mock = jest.fn();
    const updateTreatment: IUpdateVulnerabilityResultAttr = {
      updateVulnerabilitiesTreatment: { success: true },
      updateVulnerabilityTreatment: { success: true },
    };
    const mutationVariables: Record<
      string,
      boolean | number | string | undefined
    > = {
      acceptanceDate: "",
      assigned: "manager_test@test.test",
      customSeverity: 2,
      externalBugTrackingSystem: "http://test.t",
      findingId: "422286126",
      isVulnDescriptionChanged: false,
      isVulnTreatmentChanged: true,
      isVulnTreatmentDescriptionChanged: true,
      justification: "test justification to treatment",
      source: undefined,
      tag: "one",
      treatment: "IN_PROGRESS",
    };
    const mocksMutation = [
      graphqlMocked.mutation(
        UPDATE_VULNERABILITY_MUTATION,
        ({
          variables,
        }): StrictResponse<IErrorMessage | { data: UpdateVulnerability }> => {
          if (
            Object.keys(mutationVariables).every(
              (key): boolean =>
                mutationVariables[key] ===
                variables[key as keyof typeof variables],
            )
          ) {
            return HttpResponse.json({
              data: {
                ...updateTreatment,
              },
            });
          }

          return HttpResponse.json({
            errors: [new GraphQLError("Failed to update vulnerability")],
          });
        },
      ),
    ];
    const vulnsToUpdate: IVulnDataTypeAttr[] = [
      {
        assigned: "",
        customSeverity: null,
        externalBugTrackingSystem: null,
        findingId: "422286126",
        groupName: "testgroupname",
        historicTreatment: [],
        id: "test1",
        severityTemporalScore: 2,
        severityThreatScore: 1.9,
        source: "asm",
        specific: "",
        state: "VULNERABLE",
        tag: "one",
        where: "",
      },
      {
        assigned: "",
        customSeverity: null,
        externalBugTrackingSystem: null,
        findingId: "422286126",
        groupName: "testgroupname",
        historicTreatment: [],
        id: "test2",
        severityTemporalScore: 2,
        severityThreatScore: 1.9,
        source: "asm",
        specific: "",
        state: "VULNERABLE",
        tag: "one",
        where: "",
      },
    ];
    render(
      <WrapperComponent permissions={mockedPermissions}>
        <authzGroupContext.Provider
          value={new PureAbility([{ action: "can_report_vulnerabilities" }])}
        >
          <TreatmentForm
            close={handleOnClose}
            findingId={"422286126"}
            groupName={"testgroupname"}
            handleClearSelected={handleClearSelected}
            refetchData={handleRefetchData}
            vulnerabilities={vulnsToUpdate}
          />
        </authzGroupContext.Provider>
      </WrapperComponent>,
      {
        memoryRouter,
        mocks: [...mocksVulns, ...mocksMutation],
      },
    );
    await waitFor((): void => {
      expect(
        screen.queryAllByRole("combobox", { name: "treatment" }),
      ).toHaveLength(1);
    });
    await screen.findByText("searchFindings.tabDescription.treatment.title");
    await userEvent.click(
      screen.getByText("searchFindings.tabDescription.treatment.title"),
    );
    await userEvent.click(
      screen.getByText("searchFindings.tabDescription.treatment.inProgress"),
    );

    await waitFor((): void => {
      expect(
        screen.queryByRole("combobox", { name: "assigned" }),
      ).toBeInTheDocument();
    });

    await userEvent.type(
      screen.getByRole("textbox", { name: "justification" }),
      "test justification to treatment",
    );
    await userEvent.type(
      screen.getByRole("textbox", { name: "externalBugTrackingSystem" }),
      "http://test.t",
    );
    fireEvent.change(screen.getByRole("slider", { name: "customSeverity" }), {
      target: { value: "2" },
    });

    await userEvent.click(
      screen.getByText("searchFindings.tabDescription.assigned.text"),
    );
    await userEvent.click(screen.getByText("manager_test@test.test"));

    await waitFor((): void => {
      expect(screen.getByText(btnConfirm)).not.toBeDisabled();
    });

    await userEvent.click(screen.getByText(btnConfirm));

    await waitFor((): void => {
      expect(msgSuccess).toHaveBeenCalledTimes(2);
    });

    expect(handleOnClose).toHaveBeenCalledTimes(1);
  });

  it("should render error update treatment", async (): Promise<void> => {
    expect.hasAssertions();

    jest.clearAllMocks();

    const handleClearSelected: jest.Mock = jest.fn();
    const handleOnClose: jest.Mock = jest.fn();
    const handleRefetchData: jest.Mock = jest.fn();

    const mocksError = [
      graphqlMocked.mutation(
        UPDATE_VULNERABILITY_MUTATION,
        (): StrictResponse<IErrorMessage> => {
          return HttpResponse.json({
            errors: [
              new GraphQLError(
                "Vulnerability has been accepted the maximum number of times allowed by the defined policy",
              ),
            ],
          });
        },
      ),
    ];
    const vulnsToUpdate: IVulnDataTypeAttr[] = [
      {
        assigned: "manager_test@test.test",
        customSeverity: null,
        externalBugTrackingSystem: null,
        findingId: "422286126",
        groupName: "testgroupname",
        historicTreatment: [],
        id: "test",
        source: "asm",
        specific: "",
        state: "VULNERABLE",
        tag: "one",
        where: "",
      },
    ];
    render(
      <WrapperComponent permissions={mockedPermissions}>
        <authzGroupContext.Provider
          value={new PureAbility([{ action: "can_report_vulnerabilities" }])}
        >
          <TreatmentForm
            close={handleOnClose}
            findingId={"422286126"}
            groupName={"testgroupname"}
            handleClearSelected={handleClearSelected}
            refetchData={handleRefetchData}
            vulnerabilities={vulnsToUpdate}
          />
        </authzGroupContext.Provider>
      </WrapperComponent>,
      {
        mocks: mocksError,
      },
    );

    await waitFor((): void => {
      expect(
        screen.queryAllByRole("combobox", { name: "treatment" }),
      ).toHaveLength(1);
    });

    expect(
      screen.queryByText("searchFindings.tabDescription.approvalTitle"),
    ).not.toBeInTheDocument();

    await userEvent.click(
      screen.getByText("searchFindings.tabDescription.treatment.title"),
    );
    await userEvent.click(
      screen.getByText(
        "searchFindings.tabDescription.treatment.acceptedUndefined",
      ),
    );

    await userEvent.click(
      screen.getByText("searchFindings.tabDescription.assigned.text"),
    );
    await userEvent.click(screen.getByText("manager_test@test.test"));

    await userEvent.type(
      screen.getByRole("textbox", { name: "justification" }),
      "test justification to treatment",
    );

    await waitFor((): void => {
      expect(screen.getByText(btnConfirm)).not.toBeDisabled();
    });

    await userEvent.click(screen.getByText(btnConfirm));

    await waitFor((): void => {
      expect(
        screen.queryByText("searchFindings.tabDescription.approvalTitle"),
      ).toBeInTheDocument();
    });
    await userEvent.click(
      screen.getAllByRole("button", { name: btnConfirm })[1],
    );
    await waitFor((): void => {
      expect(msgError).toHaveBeenCalledWith(
        translate.t("searchFindings.tabVuln.alerts.maximumNumberOfAcceptances"),
      );
    });

    expect(handleOnClose).not.toHaveBeenCalled();
    expect(handleRefetchData).not.toHaveBeenCalled();

    expect(screen.getByRole("combobox", { name: "assigned" })).toHaveValue(
      "manager_test@test.test",
    );
  });

  it("should handle edit source", async (): Promise<void> => {
    expect.hasAssertions();

    jest.clearAllMocks();

    const handleOnClose: jest.Mock = jest.fn();
    const handleClearSelected: jest.Mock = jest.fn();
    const handleRefetchData: jest.Mock = jest.fn();

    const mocksMutation = [
      graphqlMocked.mutation(
        UPDATE_VULNERABILITY_MUTATION,
        ({
          variables,
        }): StrictResponse<IErrorMessage | { data: UpdateVulnerability }> => {
          const {
            acceptanceDate,
            customSeverity,
            externalBugTrackingSystem,
            findingId,
            isVulnDescriptionChanged,
            isVulnTreatmentChanged,
            isVulnTreatmentDescriptionChanged,
            justification,
            source,
            tag,
            treatment,
            vulnerabilityId,
          } = variables;
          if (
            acceptanceDate === "" &&
            externalBugTrackingSystem === "" &&
            findingId === "422286126" &&
            isVulnDescriptionChanged &&
            !isVulnTreatmentChanged &&
            !isVulnTreatmentDescriptionChanged &&
            justification === "" &&
            customSeverity === 2 &&
            source === VulnerabilitySource.Analyst &&
            tag === "one" &&
            treatment === UpdateClientDescriptionTreatment.InProgress &&
            vulnerabilityId === "ab25380d-dfe1-4cde-aefd-acca6990d6aa"
          ) {
            return HttpResponse.json({
              data: {
                updateVulnerabilityDescription: { success: true },
              },
            });
          }

          return HttpResponse.json({
            errors: [new GraphQLError("Failed to update vulnerability")],
          });
        },
      ),
    ];
    const mockedSourcePermissions = new PureAbility<string>([
      { action: "see_vulnerability_source" },
      {
        action:
          "integrates_api_mutations_update_vulnerability_description_mutate",
      },
    ]);
    render(
      <WrapperComponent permissions={mockedSourcePermissions}>
        <TreatmentForm
          close={handleOnClose}
          findingId={"422286126"}
          groupName={"testgroupname"}
          handleClearSelected={handleClearSelected}
          refetchData={handleRefetchData}
          vulnerabilities={vulns}
        />
      </WrapperComponent>,
      {
        memoryRouter,
        mocks: [...mocksVulns, ...mocksMutation],
      },
    );
    await waitFor((): void => {
      expect(
        screen.queryAllByRole("combobox", { name: "source" }),
      ).toHaveLength(1);
    });

    expect(screen.getByText(btnConfirm)).toBeDisabled();

    await userEvent.click(screen.getByRole("combobox", { name: "source" }));
    await userEvent.click(
      screen.getByText(
        "searchFindings.tabVuln.vulnTable.vulnerabilitySource.ANALYST",
      ),
    );

    await waitFor((): void => {
      expect(screen.getByText(btnConfirm)).not.toBeDisabled();
    });
    await userEvent.click(screen.getByText(btnConfirm));
    await waitFor((): void => {
      expect(msgSuccess).toHaveBeenNthCalledWith(
        1,
        "Update vulnerabilities",
        "Success!",
      );
    });
  });
});
