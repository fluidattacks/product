import { type IUseModal, IconButton, Modal } from "@fluidattacks/design";
import isUndefined from "lodash/isUndefined";
import { useCallback } from "react";
import { useTranslation } from "react-i18next";
import { useTheme } from "styled-components";

import { TreatmentForm } from "./update-treatment";

import type { IVulnDataTypeAttr } from "../types";
import { msgError, msgSuccess } from "utils/notifications";

const TreatmentModal = ({
  findingId,
  groupName,
  handleClearSelected,
  modalProps,
  refetchData,
  vulnerabilities,
}: Readonly<{
  findingId?: string;
  groupName: string;
  handleClearSelected?: () => void;
  modalProps: IUseModal;
  refetchData: () => void;
  vulnerabilities: IVulnDataTypeAttr[];
}>): JSX.Element => {
  const theme = useTheme();
  const { t } = useTranslation();

  const onCopy = useCallback(async (): Promise<void> => {
    const { clipboard } = navigator;

    if (isUndefined(clipboard)) {
      msgError(t("searchFindings.copyUrl.failed"));
    } else {
      await clipboard.writeText(window.location.href);
      msgSuccess(
        t("searchFindings.copyUrl.success"),
        t("searchFindings.copyUrl.successTitle"),
      );
    }
  }, [t]);

  return (
    <Modal
      modalRef={modalProps}
      otherActions={
        <IconButton
          icon={"copy"}
          iconColor={theme.palette.gray[700]}
          iconSize={"xs"}
          iconType={"fa-light"}
          onClick={onCopy}
          px={0.25}
          py={0.25}
          variant={"ghost"}
        />
      }
      size={"md"}
      title={t("searchFindings.tabDescription.editVuln")}
    >
      <TreatmentForm
        close={modalProps.close}
        findingId={findingId}
        groupName={groupName}
        handleClearSelected={handleClearSelected}
        refetchData={refetchData}
        vulnerabilities={vulnerabilities}
      />
    </Modal>
  );
};

export { TreatmentModal };
