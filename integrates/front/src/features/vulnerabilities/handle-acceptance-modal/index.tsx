import { useAbility } from "@casl/react";
import { Modal, useModal } from "@fluidattacks/design";
import { StrictMode, useCallback, useContext, useState } from "react";
import { useTranslation } from "react-i18next";

import { SubmittedForm } from "./submitted-form";
import { TreatmentAcceptanceForm } from "./treatment-acceptance-form";
import type { IHandleVulnerabilitiesAcceptanceModalProps } from "./types";
import { ZeroRiskForm } from "./zero-risk-form";

import { Dropdown } from "components/dropdown";
import type { IDropDownOption } from "components/dropdown/types";
import {
  authzGroupContext,
  authzPermissionsContext,
} from "context/authz/config";
import { useVulnerabilityStore } from "hooks/use-vulnerability-store";

const HandleAcceptanceModal = ({
  findingId = undefined,
  vulns,
  handleCloseModal,
  refetchData,
}: Readonly<IHandleVulnerabilitiesAcceptanceModalProps>): JSX.Element => {
  const { t } = useTranslation();
  const attributes = useContext(authzGroupContext);
  const permissions = useAbility(authzPermissionsContext);
  const mainModalRef = useModal("main-modal");
  const treatmentAcceptanceModalRef = useModal("treatment-acceptance-modal");
  const zeroRiskModalRef = useModal("zero-risk-modal");
  const submittedModalRef = useModal("submitted-form");
  const { isAcceptanceModalOpen } = useVulnerabilityStore();
  const canHandleVulnerabilitiesAcceptance = permissions.can(
    "integrates_api_mutations_handle_vulnerabilities_acceptance_mutate",
  );
  const canConfirmVulnerabilities = permissions.can(
    "integrates_api_mutations_confirm_vulnerabilities_mutate",
  );
  const canRejectVulnerabilities = permissions.can(
    "integrates_api_mutations_reject_vulnerabilities_mutate",
  );
  const canConfirmZeroRiskVulnerabilities =
    permissions.can(
      "integrates_api_mutations_confirm_vulnerabilities_zero_risk_mutate",
    ) && attributes.can("can_request_zero_risk");
  const canRejectZeroRiskVulnerabilities =
    permissions.can(
      "integrates_api_mutations_reject_vulnerabilities_zero_risk_mutate",
    ) && attributes.can("can_request_zero_risk");
  const canUpdateVulns = attributes.can("can_report_vulnerabilities");

  const [treatment, setTreatment] = useState<string | undefined>(undefined);

  const setTreatmentHelper = useCallback(({ value }: IDropDownOption): void => {
    setTreatment(value);
  }, []);

  const handleInternalModal = useCallback((): void => {
    setTreatment("");
    handleCloseModal();
  }, [handleCloseModal]);

  const treatmentOptions = [
    {
      value: t("searchFindings.tabDescription.treatment.placeholder"),
    },
    {
      extraCondition: canHandleVulnerabilitiesAcceptance,
      header: t("searchFindings.tabDescription.treatment.acceptedUndefined"),
      value: "ACCEPTED_UNDEFINED",
    },
    {
      extraCondition: canHandleVulnerabilitiesAcceptance,
      header: t("searchFindings.tabDescription.treatment.accepted"),
      value: "ACCEPTED",
    },
    {
      extraCondition:
        canConfirmVulnerabilities && canRejectVulnerabilities && canUpdateVulns,
      header: t(
        "searchFindings.tabDescription.treatment.confirmRejectVulnerability",
      ),
      value: "CONFIRM_REJECT_VULNERABILITY",
    },
    {
      extraCondition:
        canConfirmZeroRiskVulnerabilities &&
        canRejectZeroRiskVulnerabilities &&
        canUpdateVulns,
      header: t(
        "searchFindings.tabDescription.treatment.confirmRejectZeroRisk",
      ),
      value: "CONFIRM_REJECT_ZERO_RISK",
    },
  ];

  return (
    <StrictMode>
      <Modal
        modalRef={{
          ...mainModalRef,
          close: handleCloseModal,
          isOpen: isAcceptanceModalOpen,
        }}
        size={"sm"}
        title={t("searchFindings.tabDescription.handleAcceptanceModal.title")}
      >
        <Dropdown
          customSelectionHandler={setTreatmentHelper}
          id={"treatment-type"}
          items={treatmentOptions}
          placeholder={t("searchFindings.tabDescription.treatment.placeholder")}
          width={"100%"}
        />
        <TreatmentAcceptanceForm
          modalRef={{
            ...treatmentAcceptanceModalRef,
            close: handleInternalModal,
            isOpen: treatment === "ACCEPTED_UNDEFINED",
            toggle: handleInternalModal,
          }}
          onCancel={handleInternalModal}
          refetchData={refetchData}
          title={t("searchFindings.tabDescription.treatment.acceptedUndefined")}
          treatmentType={"ACCEPTED_UNDEFINED"}
          vulnerabilities={vulns}
        />
        <TreatmentAcceptanceForm
          modalRef={{
            ...treatmentAcceptanceModalRef,
            close: handleInternalModal,
            isOpen: treatment === "ACCEPTED",
            toggle: handleInternalModal,
          }}
          onCancel={handleInternalModal}
          refetchData={refetchData}
          title={t("searchFindings.tabDescription.treatment.accepted")}
          treatmentType={"ACCEPTED"}
          vulnerabilities={vulns}
        />
        <ZeroRiskForm
          findingId={findingId}
          modalRef={{
            ...zeroRiskModalRef,
            close: handleInternalModal,
            isOpen: treatment === "CONFIRM_REJECT_ZERO_RISK",
          }}
          onCancel={handleInternalModal}
          refetchData={refetchData}
          title={t(
            "searchFindings.tabDescription.treatment.confirmRejectZeroRisk",
          )}
          vulnerabilities={vulns}
        />
        <SubmittedForm
          findingId={findingId}
          modalRef={{
            ...submittedModalRef,
            close: handleInternalModal,
            isOpen: treatment === "CONFIRM_REJECT_VULNERABILITY",
          }}
          onCancel={handleInternalModal}
          refetchData={refetchData}
          title={t(
            "searchFindings.tabDescription.treatment.confirmRejectVulnerability",
          )}
          vulnerabilities={vulns}
        />
      </Modal>
    </StrictMode>
  );
};

export { HandleAcceptanceModal };
