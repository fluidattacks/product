import { useMutation } from "@apollo/client";
import { useAbility } from "@casl/react";
import type { IUseModal } from "@fluidattacks/design";
import {
  Form,
  InnerForm,
  InputTags,
  Modal,
  TextArea,
} from "@fluidattacks/design";
import _ from "lodash";
import { Fragment, useCallback, useMemo, useState } from "react";
import { useTranslation } from "react-i18next";
import { object, string } from "yup";

import { TreatmentAcceptanceTable } from "./treatment-acceptance-table";
import type { IFormValues } from "./types";
import {
  getAllNotifications,
  handleSubmitError,
  mapAndReduceUpdateVulnResults,
} from "./utils";

import { HANDLE_VULNS_ACCEPTANCE } from "../queries";
import { isAcceptedUndefinedSelectedHelper } from "../utils";
import { authzPermissionsContext } from "context/authz/config";
import {
  REMOVE_TAGS_MUTATION,
  SEND_ASSIGNED_NOTIFICATION,
  UPDATE_VULNERABILITY_MUTATION,
} from "features/vulnerabilities/treatment-modal/update-treatment/queries";
import type { IRemoveTagResultAttr } from "features/vulnerabilities/treatment-modal/update-treatment/types";
import {
  deleteTagVulnHelper,
  getAreAllNotificationValid,
  sortTags,
} from "features/vulnerabilities/treatment-modal/update-treatment/utils";
import type { IVulnRowAttr } from "features/vulnerabilities/types";
import { getVulnsPendingOfAcceptance } from "pages/finding/vulnerabilities/utils";
import { Logger } from "utils/logger";
import { msgError, msgSuccess } from "utils/notifications";

interface IAcceptanceModalProps {
  onCancel: () => void;
  refetchData: () => void;
  treatmentType: "ACCEPTED_UNDEFINED" | "ACCEPTED";
  vulnerabilities: IVulnRowAttr[];
  modalRef: IUseModal;
  title?: string;
}

const TreatmentAcceptanceForm = ({
  modalRef,
  onCancel,
  refetchData,
  title,
  treatmentType,
  vulnerabilities,
}: IAcceptanceModalProps): JSX.Element => {
  const { t } = useTranslation();
  const permissions = useAbility(authzPermissionsContext);
  const canUpdateVulnsTreatment = permissions.can(
    "integrates_api_mutations_update_vulnerabilities_treatment_mutate",
  );
  const canDeleteVulnsTags = permissions.can(
    "integrates_api_mutations_remove_vulnerability_tags_mutate",
  );

  // State
  const [acceptanceVulnerabilities, setAcceptanceVulnerabilities] = useState(
    getVulnsPendingOfAcceptance(vulnerabilities, treatmentType),
  );

  // GraphQL operations
  const [handleAcceptance, { loading }] = useMutation(HANDLE_VULNS_ACCEPTANCE);

  const [sendNotification] = useMutation(SEND_ASSIGNED_NOTIFICATION, {
    onError: (updateError): void => {
      updateError.graphQLErrors.forEach((error): void => {
        msgError(
          t(
            "searchFindings.tabDescription.notification.emailNotificationError",
          ),
        );
        Logger.warning("An error occurred sending the notification", error);
      });
    },
  });

  const handleNotification = useCallback(async (): Promise<void> => {
    const accepted = acceptanceVulnerabilities.filter(
      (vuln): boolean => vuln.acceptance === "APPROVED",
    );
    if (accepted.length > 0) {
      const notificationsResults = await getAllNotifications(
        sendNotification,
        accepted,
      );
      const areAllNotificationValid =
        getAreAllNotificationValid(notificationsResults);
      if (areAllNotificationValid.every(Boolean)) {
        msgSuccess(
          t(
            "searchFindings.tabDescription.notification.altEmailNotificationText",
          ),
          t(
            "searchFindings.tabDescription.notification.emailNotificationTitle",
          ),
        );
      }
    }
  }, [acceptanceVulnerabilities, sendNotification, t]);

  const vulnsTags = useMemo(
    (): string[][] =>
      acceptanceVulnerabilities.map((vuln: IVulnRowAttr): string[] =>
        sortTags(vuln.tag),
      ),
    [acceptanceVulnerabilities],
  );

  const initialValues = useMemo((): IFormValues & Record<string, string> => {
    return {
      justification: "",
      tag: _.join(_.intersection(...vulnsTags), ","),
    };
  }, [vulnsTags]);

  const [, { loading: updatingVulnerability }] = useMutation(
    UPDATE_VULNERABILITY_MUTATION,
  );

  // Handle actions
  const handleSubmit = useCallback(
    async (formValues: IFormValues): Promise<void> => {
      try {
        const accepted = acceptanceVulnerabilities.filter(
          (vuln): boolean => vuln.acceptance === "APPROVED",
        );
        const rejected = acceptanceVulnerabilities.filter(
          (vuln): boolean => vuln.acceptance === "REJECTED",
        );
        await isAcceptedUndefinedSelectedHelper(
          handleAcceptance,
          accepted,
          { justification: formValues.justification },
          rejected,
        ).then(async (allValues): Promise<void> => {
          const areAllValid = mapAndReduceUpdateVulnResults(allValues);

          if (areAllValid.every(Boolean)) {
            msgSuccess(
              t("searchFindings.tabVuln.alerts.acceptanceSuccess"),
              t("groupAlerts.updatedTitle"),
            );
          }
          onCancel();
          refetchData();
          await handleNotification();
        });
      } catch (requestError: unknown) {
        handleSubmitError(requestError);
      }
    },
    [
      acceptanceVulnerabilities,
      handleAcceptance,
      handleNotification,
      onCancel,
      refetchData,
      t,
    ],
  );

  const [deleteTagVuln, { loading: deletingTag }] = useMutation(
    REMOVE_TAGS_MUTATION,
    {
      onCompleted: (result: IRemoveTagResultAttr): void => {
        deleteTagVulnHelper(result);
        refetchData();
      },
      onError: (updateError): void => {
        updateError.graphQLErrors.forEach((error): void => {
          msgError(t("groupAlerts.errorTextsad"));
          Logger.warning(
            "An error occurred deleting vulnerabilities tags",
            error,
          );
        });
      },
    },
  );

  const handleDeletion = useCallback(
    async (tag: string): Promise<void> => {
      await deleteTagVuln({
        variables: {
          findingId: acceptanceVulnerabilities[0].findingId,
          tag,
          vulnerabilities: acceptanceVulnerabilities.map(
            (vuln: IVulnRowAttr): string => vuln.id,
          ),
        },
      });
    },
    [deleteTagVuln, acceptanceVulnerabilities],
  );

  return (
    <Modal
      id={"treatmentAcceptanceForm"}
      modalRef={modalRef}
      size={"md"}
      title={title}
    >
      <Form
        cancelButton={{ onClick: onCancel }}
        confirmButton={{
          disabled:
            loading ||
            deletingTag ||
            updatingVulnerability ||
            acceptanceVulnerabilities.length === 0,
        }}
        defaultValues={initialValues}
        onSubmit={handleSubmit}
        yupSchema={object().shape({
          justification: string().required(t("validations.required")),
        })}
      >
        <InnerForm>
          {({ control, formState, register, watch }): JSX.Element => {
            return (
              <Fragment>
                <TreatmentAcceptanceTable
                  setVulns={setAcceptanceVulnerabilities}
                  vulns={acceptanceVulnerabilities}
                />
                <TextArea
                  error={formState.errors.justification?.message?.toString()}
                  isTouched={"justification" in formState.touchedFields}
                  isValid={!("justification" in formState.errors)}
                  label={t(
                    "searchFindings.tabDescription.remediationModal.observations",
                  )}
                  maxLength={10000}
                  name={"justification"}
                  register={register}
                  required={true}
                  watch={watch}
                />
                <InputTags
                  control={control}
                  disabled={!(canUpdateVulnsTreatment && canDeleteVulnsTags)}
                  label={t("searchFindings.tabDescription.tag")}
                  name={"tag"}
                  onRemove={handleDeletion}
                />
              </Fragment>
            );
          }}
        </InnerForm>
      </Form>
    </Modal>
  );
};

export { TreatmentAcceptanceForm };
