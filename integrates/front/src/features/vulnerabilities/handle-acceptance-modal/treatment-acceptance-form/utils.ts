import _ from "lodash";

import type { IHandleTagModificationParameters } from "./types";

import type { TNotificationResult } from "../../treatment-modal/update-treatment/types";
import type { TVulnUpdateResult } from "../types";
import {
  getAllResults,
  getAreAllChunkedMutationValid,
  handleUpdateVulnTreatmentError,
  validMutationsHelper,
} from "features/vulnerabilities/treatment-modal/update-treatment/utils";
import type {
  IUpdateVulnerabilityForm,
  IVulnDataTypeAttr,
  IVulnRowAttr,
} from "features/vulnerabilities/types";
import { Logger } from "utils/logger";
import { msgError } from "utils/notifications";
import { translate } from "utils/translations/translate";

const getAllNotifications = async (
  sendNotification: (
    variables: Record<string, unknown>,
  ) => Promise<TNotificationResult>,
  vulnerabilities: IVulnRowAttr[],
): Promise<TNotificationResult[]> => {
  const vulnerabilitiesByFinding = _.groupBy(
    vulnerabilities,
    (vuln): string => vuln.findingId,
  );
  const requestedChunks = Object.entries(vulnerabilitiesByFinding).map(
    ([findingId, chunkedVulnerabilities]: [
      string,
      IVulnRowAttr[],
    ]): (() => Promise<TNotificationResult[]>) =>
      async (): Promise<TNotificationResult[]> => {
        return Promise.all([
          await sendNotification({
            variables: {
              findingId,
              vulnerabilities: chunkedVulnerabilities.map(
                ({ id }): string => id,
              ),
            },
          }),
        ]);
      },
  );

  return requestedChunks.reduce(
    async (previousValue, currentValue): Promise<TNotificationResult[]> => [
      ...(await previousValue),
      ...(await currentValue()),
    ],
    Promise.resolve<TNotificationResult[]>([]),
  );
};

const handleSubmitError = (requestError: unknown): void => {
  switch (String(requestError).replace(/^ApolloError: /u, "")) {
    case "Exception - It cant handle acceptance without being requested":
      msgError(
        translate.t("searchFindings.tabVuln.alerts.acceptanceNotRequested"),
      );
      break;
    case "Exception - Vulnerability not found":
      msgError(translate.t("groupAlerts.noFound"));
      break;
    case "Exception - Invalid characters":
      msgError(translate.t("validations.invalidChar"));
      break;
    default:
      msgError(translate.t("groupAlerts.errorTextsad"));
      Logger.warning("An error occurred handling acceptance", requestError);
  }
};

const handleTagModification = async ({
  acceptanceVulnerabilities,
  formValues,
  treatmentType,
  vulnerabilities,
  wasTagModified,
  updateVulnerability,
  onCancel,
}: Readonly<IHandleTagModificationParameters>): Promise<boolean> => {
  if (!wasTagModified) {
    return false;
  }

  try {
    const acceptanceVulns = acceptanceVulnerabilities
      .map((acceptanceVuln): IVulnRowAttr | undefined => {
        const result = vulnerabilities.find(
          (vuln): boolean => vuln.id === acceptanceVuln.id,
        );

        return result;
      })
      .filter((vuln): boolean => vuln !== undefined)
      .map((acceptanceVuln): IVulnRowAttr => {
        return acceptanceVuln as IVulnRowAttr;
      });

    const vulns = acceptanceVulns.map((vuln): IVulnDataTypeAttr => {
      const result: IVulnDataTypeAttr = {
        assigned: vuln.assigned,
        customSeverity: vuln.customSeverity,
        externalBugTrackingSystem: vuln.externalBugTrackingSystem,
        findingId: vuln.findingId,
        groupName: vuln.groupName,
        historicTreatment: vuln.historicTreatment,
        id: vuln.id,
        source: vuln.source,
        specific: vuln.specific,
        state: vuln.state,
        tag: vuln.tag,
        where: vuln.where,
      };

      return result;
    });
    const values: IUpdateVulnerabilityForm = {
      acceptanceDate: undefined,
      assigned: undefined,
      customSeverity: null,
      externalBugTrackingSystem: null,
      justification: "",
      source: undefined,
      tag: formValues.tag,
      treatment: treatmentType,
    };
    const results = await getAllResults(
      updateVulnerability,
      vulns,
      values,
      true,
      false,
      true,
    );
    const areAllMutationValid = getAreAllChunkedMutationValid(results);
    validMutationsHelper(onCancel, areAllMutationValid, values, vulns, true);

    return true;
  } catch (updateError: unknown) {
    handleUpdateVulnTreatmentError(updateError);

    return false;
  }
};

const mapAndReduceUpdateVulnResults = (
  allValues: TVulnUpdateResult[][],
): boolean[] => {
  return allValues
    .map((values): boolean[] => {
      return values.map((result): boolean => {
        if (!_.isNil(result.data)) {
          return _.isUndefined(result.data.handleVulnerabilitiesAcceptance)
            ? true
            : result.data.handleVulnerabilitiesAcceptance.success;
        }

        return false;
      });
    })
    .reduce(
      (previous: boolean[], current: boolean[]): boolean[] => [
        ...previous,
        ...current,
      ],
      [],
    );
};

export {
  getAllNotifications,
  handleSubmitError,
  handleTagModification,
  mapAndReduceUpdateVulnResults,
};
