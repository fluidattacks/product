import { useMutation } from "@apollo/client";
import { useAbility } from "@casl/react";
import {
  ComboBox,
  Form,
  InnerForm,
  Modal,
  TextArea,
} from "@fluidattacks/design";
import { Fragment, useCallback, useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import { object, string } from "yup";

import type { IFormValues, IZeroRiskFormProps } from "./types";
import { ZeroRiskTable } from "./zero-risk-table";

import {
  CONFIRM_VULNERABILITIES_ZERO_RISK,
  REJECT_VULNERABILITIES_ZERO_RISK,
} from "../queries";
import type { IVulnDataAttr } from "../types";
import {
  confirmZeroRiskProps,
  isConfirmZeroRiskSelectedHelper,
  isRejectZeroRiskSelectedHelper,
  rejectZeroRiskProps,
} from "../utils";
import { authzPermissionsContext } from "context/authz/config";
import { getRequestedZeroRiskVulns } from "pages/finding/vulnerabilities/utils";

const ZeroRiskForm = ({
  findingId,
  modalRef,
  onCancel,
  refetchData,
  title,
  vulnerabilities,
}: IZeroRiskFormProps): JSX.Element => {
  const { t } = useTranslation();
  const permissions = useAbility(authzPermissionsContext);
  const canSeeDropDownToConfirmZeroRisk = permissions.can(
    "see_dropdown_to_confirm_zero_risk",
  );
  const prefix =
    "searchFindings.tabDescription.handleAcceptanceModal.zeroRiskJustification.";

  // State
  const [acceptanceVulnerabilities, setAcceptanceVulnerabilities] = useState<
    IVulnDataAttr[]
  >(getRequestedZeroRiskVulns(vulnerabilities));
  const [confirmedVulnerabilities, setConfirmedVulnerabilities] = useState<
    IVulnDataAttr[]
  >([]);
  const [rejectedVulnerabilities, setRejectedVulnerabilities] = useState<
    IVulnDataAttr[]
  >([]);

  // GraphQL operations
  const [confirmZeroRisk, { loading: confirmingZeroRisk }] = useMutation(
    CONFIRM_VULNERABILITIES_ZERO_RISK,
    confirmZeroRiskProps(refetchData, onCancel, findingId),
  );
  const [rejectZeroRisk, { loading: rejectingZeroRisk }] = useMutation(
    REJECT_VULNERABILITIES_ZERO_RISK,
    rejectZeroRiskProps(refetchData, onCancel, findingId),
  );

  // Handle actions
  const handleSubmit = useCallback(
    (formValues: IFormValues): void => {
      isConfirmZeroRiskSelectedHelper(
        true,
        confirmZeroRisk,
        confirmedVulnerabilities,
        formValues,
      );
      isRejectZeroRiskSelectedHelper(
        true,
        rejectZeroRisk,
        formValues,
        rejectedVulnerabilities,
      );
    },
    [
      confirmZeroRisk,
      confirmedVulnerabilities,
      rejectZeroRisk,
      rejectedVulnerabilities,
    ],
  );

  // Side effects
  useEffect((): void => {
    setConfirmedVulnerabilities(
      acceptanceVulnerabilities.reduce(
        (
          acc: IVulnDataAttr[],
          vulnerability: IVulnDataAttr,
        ): IVulnDataAttr[] =>
          vulnerability.acceptance === "APPROVED"
            ? [...acc, vulnerability]
            : acc,
        [],
      ),
    );
    setRejectedVulnerabilities(
      acceptanceVulnerabilities.reduce(
        (
          acc: IVulnDataAttr[],
          vulnerability: IVulnDataAttr,
        ): IVulnDataAttr[] =>
          vulnerability.acceptance === "REJECTED"
            ? [...acc, vulnerability]
            : acc,
        [],
      ),
    );
  }, [acceptanceVulnerabilities]);

  const selectOptions = [
    ...(confirmedVulnerabilities.length > 0
      ? [
          {
            name: t(`${prefix}confirmation.fpReport`),
            value: "FP - Wrong report",
          },
          {
            name: t(`${prefix}confirmation.fpContext`),
            value: "FP - Does not apply to the context",
          },
          {
            name: t(`${prefix}confirmation.outOfTheScope`),
            value: "Out of the scope",
          },
        ]
      : []),
    ...(rejectedVulnerabilities.length > 0
      ? [
          {
            name: t(`${prefix}rejection.fn`),
            value: "FN",
          },
          {
            name: t(`${prefix}rejection.complementaryControl`),
            value: "Complementary control",
          },
        ]
      : []),
  ];

  return (
    <Modal id={"zeroRiskForm"} modalRef={modalRef} size={"md"} title={title}>
      <Form
        cancelButton={{ onClick: onCancel }}
        confirmButton={{
          disabled:
            (confirmedVulnerabilities.length === 0 &&
              rejectedVulnerabilities.length === 0) ||
            confirmingZeroRisk ||
            rejectingZeroRisk,
        }}
        defaultDisabled={false}
        defaultValues={{ justification: "" }}
        onSubmit={handleSubmit}
        yupSchema={object().shape({
          justification: string().required(t("validations.required")),
        })}
      >
        <InnerForm>
          {({ control, formState, register, watch }): JSX.Element => (
            <Fragment>
              <ZeroRiskTable
                acceptanceVulns={acceptanceVulnerabilities}
                isConfirmRejectZeroRiskSelected={true}
                setAcceptanceVulns={setAcceptanceVulnerabilities}
              />
              {canSeeDropDownToConfirmZeroRisk ? (
                <ComboBox
                  control={control}
                  items={selectOptions}
                  label={t(
                    "searchFindings.tabDescription.remediationModal.observations",
                  )}
                  name={"justification"}
                  required={true}
                />
              ) : (
                <TextArea
                  error={formState.errors.justification?.message?.toString()}
                  isTouched={"justification" in formState.touchedFields}
                  isValid={!("justification" in formState.errors)}
                  label={t(
                    "searchFindings.tabDescription.remediationModal.observations",
                  )}
                  maxLength={10000}
                  name={"justification"}
                  register={register}
                  required={true}
                  watch={watch}
                />
              )}
            </Fragment>
          )}
        </InnerForm>
      </Form>
    </Modal>
  );
};

export { ZeroRiskForm };
