import { Button } from "@fluidattacks/design";
import type { ColumnDef } from "@tanstack/react-table";
import { Fragment, useCallback, useState } from "react";
import * as React from "react";
import { useTranslation } from "react-i18next";

import { changeZeroRiskFormatter } from "./change-zero-risk-formatter";
import type { IZeroRiskTableProps } from "./types";

import type { IVulnDataAttr } from "../../types";
import { Table } from "components/table";
import { useTable } from "hooks";
import { severityFormatter } from "utils/format-helpers";

const ZeroRiskTable: React.FC<IZeroRiskTableProps> = ({
  acceptanceVulns,
  isConfirmRejectZeroRiskSelected,
  setAcceptanceVulns,
}: IZeroRiskTableProps): JSX.Element => {
  const { t } = useTranslation();
  const tableRef = useTable("vulnsToHandleAcceptance");
  const [selectedVulnerabilities, setSelectedVulnerabilities] = useState<
    IVulnDataAttr[]
  >([]);

  const handleRejectZeroRisk = (vulnInfo?: IVulnDataAttr): void => {
    if (vulnInfo) {
      const newVulnList: IVulnDataAttr[] = acceptanceVulns.map(
        (vuln: IVulnDataAttr): IVulnDataAttr =>
          vuln.id === vulnInfo.id
            ? {
                ...vuln,
                acceptance: vuln.acceptance === "REJECTED" ? "" : "REJECTED",
              }
            : vuln,
      );
      setAcceptanceVulns([...newVulnList]);
      setSelectedVulnerabilities(
        newVulnList.filter((vuln): boolean => vuln.acceptance === "REJECTED"),
      );
    }
  };

  const handleConfirmZeroRisk = (vulnInfo?: IVulnDataAttr): void => {
    if (vulnInfo) {
      const newVulnList: IVulnDataAttr[] = acceptanceVulns.map(
        (vuln: IVulnDataAttr): IVulnDataAttr =>
          vuln.id === vulnInfo.id
            ? {
                ...vuln,
                acceptance: vuln.acceptance === "APPROVED" ? "" : "APPROVED",
              }
            : vuln,
      );
      setAcceptanceVulns([...newVulnList]);
      setSelectedVulnerabilities(
        newVulnList.filter((vuln): boolean => vuln.acceptance === "APPROVED"),
      );
    }
  };

  const columns: ColumnDef<IVulnDataAttr>[] = [
    {
      accessorKey: "where",
      header: "Where",
    },
    {
      accessorKey: "specific",
      header: "Specific",
    },
    {
      accessorKey: "hacker",
      header: t(
        "searchFindings.tabVuln.handleAcceptanceModal.submittedForm.submittedTable.hacker",
      ),
    },
    {
      accessorKey: "severityThreatScore",
      cell: (cell): JSX.Element => severityFormatter(Number(cell.getValue())),
      header: "Severity",
    },
    {
      accessorKey: "acceptance",
      cell: (cell): JSX.Element =>
        changeZeroRiskFormatter(
          cell.row.original,
          handleConfirmZeroRisk,
          handleRejectZeroRisk,
        ),
      header: "Acceptance",
    },
  ];

  const handleOnChange = useCallback(
    (newValue: "APPROVED" | "REJECTED"): (() => void) => {
      return (): void => {
        const newVulnList: IVulnDataAttr[] = acceptanceVulns.map(
          (vuln: IVulnDataAttr): IVulnDataAttr => ({
            ...vuln,
            acceptance: newValue,
          }),
        );
        setAcceptanceVulns([...newVulnList]);
        setSelectedVulnerabilities([...newVulnList]);
      };
    },
    [acceptanceVulns, setAcceptanceVulns],
  );

  return (
    <React.StrictMode>
      {isConfirmRejectZeroRiskSelected ? (
        <Table
          columns={columns}
          data={acceptanceVulns}
          extraButtons={
            <Fragment>
              <Button
                icon={"check"}
                onClick={handleOnChange("APPROVED")}
                variant={"ghost"}
              >
                {t(
                  "searchFindings.tabDescription.handleAcceptanceModal.zeroRisk.globalSwitch.confirmAll",
                )}
              </Button>
              <Button
                icon={"xmark"}
                onClick={handleOnChange("REJECTED")}
                variant={"ghost"}
              >
                {t(
                  "searchFindings.tabDescription.handleAcceptanceModal.zeroRisk.globalSwitch.rejectAll",
                )}
              </Button>
            </Fragment>
          }
          options={{ enableRowSelection: true }}
          rowSelectionSetter={setSelectedVulnerabilities}
          rowSelectionState={selectedVulnerabilities}
          tableRef={tableRef}
        />
      ) : undefined}
    </React.StrictMode>
  );
};

export { ZeroRiskTable };
