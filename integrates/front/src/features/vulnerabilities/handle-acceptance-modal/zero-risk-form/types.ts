import type { IUseModal } from "@fluidattacks/design";

import type { IVulnRowAttr } from "features/vulnerabilities/types";

interface IZeroRiskFormProps {
  findingId?: string;
  onCancel: () => void;
  refetchData: () => void;
  vulnerabilities: IVulnRowAttr[];
  title?: string;
  modalRef: IUseModal;
}

interface IFormValues {
  justification: string;
}

export type { IFormValues, IZeroRiskFormProps };
