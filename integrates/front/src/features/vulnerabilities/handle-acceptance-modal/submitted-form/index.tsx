import { useMutation } from "@apollo/client";
import {
  Checkbox,
  Container,
  Form,
  GridContainer,
  InnerForm,
  Modal,
  OutlineContainer,
  TextArea,
} from "@fluidattacks/design";
import { Fragment, useCallback, useEffect, useState } from "react";
import type { FC } from "react";
import { useTranslation } from "react-i18next";
import type { Schema } from "yup";
import { array, object, string } from "yup";

import { SubmittedTable } from "./submitted-table";
import type { IFormValues, ISubmittedFormProps } from "./types";

import { CONFIRM_VULNERABILITIES, REJECT_VULNERABILITIES } from "../queries";
import {
  confirmVulnerabilityHelper,
  confirmVulnerabilityProps,
  rejectVulnerabilityHelper,
  rejectVulnerabilityProps,
} from "../utils";
import type { IVulnRowAttr } from "features/vulnerabilities/types";
import { getSubmittedVulns } from "pages/finding/vulnerabilities/utils";

const SubmittedForm: FC<ISubmittedFormProps> = ({
  findingId = undefined,
  displayGlobalColumns = false,
  modalRef,
  onCancel,
  refetchData,
  title,
  vulnerabilities,
}: ISubmittedFormProps): JSX.Element => {
  const { t } = useTranslation();

  const rejectionReasons: string[] = [
    "CONSISTENCY",
    "DUPLICATED",
    "EVIDENCE",
    "FALSE_POSITIVE",
    "NAMING",
    "OMISSION",
    "SCORING",
    "WRITING",
    "OTHER",
  ];

  // State
  const [acceptanceVulnerabilities, setAcceptanceVulnerabilities] = useState<
    IVulnRowAttr[]
  >(getSubmittedVulns(vulnerabilities));
  const [confirmedVulnerabilities, setConfirmedVulnerabilities] = useState<
    IVulnRowAttr[]
  >([]);
  const [rejectedVulnerabilities, setRejectedVulnerabilities] = useState<
    IVulnRowAttr[]
  >([]);

  // GraphQL operations
  const [confirmVulnerability, { loading: confirmingVulnerability }] =
    useMutation(
      CONFIRM_VULNERABILITIES,
      confirmVulnerabilityProps(refetchData, onCancel, findingId),
    );
  const [rejectVulnerability, { loading: rejectingVulnerability }] =
    useMutation(
      REJECT_VULNERABILITIES,
      rejectVulnerabilityProps(refetchData, onCancel, findingId),
    );

  // Handle actions
  const handleSubmit = useCallback(
    (formValues: IFormValues): void => {
      confirmVulnerabilityHelper(
        true,
        confirmVulnerability,
        confirmedVulnerabilities,
      );
      rejectVulnerabilityHelper(
        true,
        rejectVulnerability,
        {
          otherReason: formValues.rejectionReasons.includes("OTHER")
            ? formValues.otherRejectionReason
            : undefined,
          reasons: formValues.rejectionReasons,
        },
        rejectedVulnerabilities,
      );
    },
    [
      confirmVulnerability,
      confirmedVulnerabilities,
      rejectVulnerability,
      rejectedVulnerabilities,
    ],
  );

  const validationSchema = object().shape({
    otherRejectionReason: string().when(
      "rejectionReasons",
      ([reasons]: string[]): Schema => {
        return reasons.includes("OTHER")
          ? string().required(t("validations.required"))
          : string();
      },
    ),
    rejectionReasons: array().when([], (): Schema => {
      return rejectedVulnerabilities.length === 0
        ? array().notRequired()
        : array()
            .min(1, t("validations.someRequired"))
            .of(string().required(t("validations.required")));
    }),
  });

  // Side effects
  useEffect((): void => {
    setConfirmedVulnerabilities(
      acceptanceVulnerabilities.reduce(
        (acc: IVulnRowAttr[], vulnerability: IVulnRowAttr): IVulnRowAttr[] =>
          vulnerability.acceptance === "APPROVED"
            ? [...acc, vulnerability]
            : acc,
        [],
      ),
    );
    setRejectedVulnerabilities(
      acceptanceVulnerabilities.reduce(
        (acc: IVulnRowAttr[], vulnerability: IVulnRowAttr): IVulnRowAttr[] =>
          vulnerability.acceptance === "REJECTED"
            ? [...acc, vulnerability]
            : acc,
        [],
      ),
    );
  }, [acceptanceVulnerabilities]);

  return (
    <Modal
      id={"submittedForm"}
      modalRef={{ ...modalRef, close: onCancel }}
      size={"md"}
      title={
        title ?? t("searchFindings.tabDescription.handleAcceptanceModal.title")
      }
    >
      <Form
        cancelButton={{ onClick: onCancel }}
        confirmButton={{
          disabled:
            (confirmedVulnerabilities.length === 0 &&
              rejectedVulnerabilities.length === 0) ||
            confirmingVulnerability ||
            rejectingVulnerability,
        }}
        defaultDisabled={false}
        defaultValues={{
          justification: undefined,
          otherRejectionReason: undefined,
          rejectionReasons: [] as string[],
        }}
        onSubmit={handleSubmit}
        yupSchema={validationSchema}
      >
        <InnerForm>
          {({ formState, register, watch }): JSX.Element => {
            const rejectionReasonsValue: string[] = watch("rejectionReasons");

            return (
              <Fragment>
                <SubmittedTable
                  acceptanceVulns={acceptanceVulnerabilities}
                  displayGlobalColumns={displayGlobalColumns}
                  isConfirmRejectVulnerabilitySelected={true}
                  refetchData={refetchData}
                  setAcceptanceVulns={setAcceptanceVulnerabilities}
                />
                {rejectedVulnerabilities.length === 0 ? undefined : (
                  <Container>
                    <OutlineContainer
                      error={formState.errors.rejectionReasons?.message?.toString()}
                      htmlFor={"rejectionReasons"}
                      required={true}
                    >
                      {t(
                        "searchFindings.tabVuln.handleAcceptanceModal.submittedForm.reject.reasonForRejection",
                      )}
                      <GridContainer
                        gap={0.5}
                        lg={1}
                        mb={1}
                        md={1}
                        sm={1}
                        xl={1}
                      >
                        {rejectionReasons.map(
                          (reason): JSX.Element => (
                            <Checkbox
                              {...register("rejectionReasons")}
                              key={`rejectionReasons.${reason}`}
                              label={t(
                                `searchFindings.tabVuln.handleAcceptanceModal.submittedForm.reject.${reason.toLowerCase()}.text`,
                              )}
                              name={"rejectionReasons"}
                              tooltip={t(
                                `searchFindings.tabVuln.handleAcceptanceModal.submittedForm.reject.${reason.toLowerCase()}.info`,
                              )}
                              value={reason}
                            />
                          ),
                        )}
                      </GridContainer>
                    </OutlineContainer>
                    {rejectionReasonsValue.includes("OTHER") ? (
                      <TextArea
                        error={formState.errors.otherRejectionReason?.message?.toString()}
                        id={"reject-draft-other-reason"}
                        isTouched={
                          "otherRejectionReason" in formState.touchedFields
                        }
                        isValid={!("otherRejectionReason" in formState.errors)}
                        label={t(
                          "searchFindings.tabVuln.handleAcceptanceModal.submittedForm.reject.why",
                        )}
                        maxLength={10000}
                        name={"otherRejectionReason"}
                        register={register}
                        required={true}
                        watch={watch}
                      />
                    ) : undefined}
                    <TextArea
                      label={t(
                        "searchFindings.tabDescription.remediationModal.observations",
                      )}
                      maxLength={20000}
                      name={"justification"}
                      register={register}
                      watch={watch}
                    />
                  </Container>
                )}
              </Fragment>
            );
          }}
        </InnerForm>
      </Form>
    </Modal>
  );
};

export { SubmittedForm };
