import { Button } from "@fluidattacks/design";
import type { ColumnDef, Row as TableRow } from "@tanstack/react-table";
import mixpanel from "mixpanel-browser";
import type { FormEvent } from "react";
import { Fragment, StrictMode, useCallback, useContext, useState } from "react";
import { useTranslation } from "react-i18next";

import { changeSubmittedFormatter } from "./change-submitted-formatter";
import type { ISubmittedTableProps } from "./types";

import { Table } from "components/table";
import type { ICellHelper } from "components/table/types";
import { allGroupContext } from "context/authz/all-group-permissions-provider";
import type { IVulnRowAttr } from "features/vulnerabilities/types";
import { VulnerabilityModal } from "features/vulnerabilities/vulnerability-modal";
import { useTable } from "hooks";
import { severityFormatter } from "utils/format-helpers";

const SubmittedTable = (props: ISubmittedTableProps): JSX.Element => {
  const {
    acceptanceVulns,
    displayGlobalColumns = false,
    isConfirmRejectVulnerabilitySelected,
    setAcceptanceVulns,
    refetchData,
  } = props;

  const { changePermissions } = useContext(allGroupContext);
  const { t } = useTranslation();
  const tableRef = useTable("submittedTable");
  const [currentRow, setCurrentRow] = useState<IVulnRowAttr>();
  const [selectedVulnerabilities, setSelectedVulnerabilities] = useState<
    IVulnRowAttr[]
  >([]);

  const openAdditionalInfoModal = useCallback(
    (rowInfo: TableRow<IVulnRowAttr>): ((event: FormEvent) => void) => {
      return (event: FormEvent): void => {
        if (changePermissions !== undefined) {
          changePermissions(rowInfo.original.groupName);
        }
        setCurrentRow(rowInfo.original);
        mixpanel.track("ViewDraftVulnerability", {
          groupName: rowInfo.original.groupName,
        });
        event.stopPropagation();
      };
    },
    [changePermissions],
  );
  const closeAdditionalInfoModal = useCallback((): void => {
    setCurrentRow(undefined);
  }, []);

  const handleRejectSubmitted = (vulnInfo?: IVulnRowAttr): void => {
    if (vulnInfo) {
      const newVulnList: IVulnRowAttr[] = acceptanceVulns.map(
        (vuln: IVulnRowAttr): IVulnRowAttr =>
          vuln.id === vulnInfo.id
            ? {
                ...vuln,
                acceptance: vuln.acceptance === "REJECTED" ? "" : "REJECTED",
              }
            : vuln,
      );
      setAcceptanceVulns([...newVulnList]);
      setSelectedVulnerabilities(
        newVulnList.filter((vuln): boolean => vuln.acceptance === "REJECTED"),
      );
    }
  };

  const handleConfirmSubmitted = (vulnInfo?: IVulnRowAttr): void => {
    if (vulnInfo) {
      const newVulnList: IVulnRowAttr[] = acceptanceVulns.map(
        (vuln: IVulnRowAttr): IVulnRowAttr =>
          vuln.id === vulnInfo.id
            ? {
                ...vuln,
                acceptance: vuln.acceptance === "APPROVED" ? "" : "APPROVED",
              }
            : vuln,
      );
      setAcceptanceVulns([...newVulnList]);
      setSelectedVulnerabilities(
        newVulnList.filter((vuln): boolean => vuln.acceptance === "APPROVED"),
      );
    }
  };

  const columns = [
    ...(displayGlobalColumns
      ? ([
          {
            accessorKey: "groupName",
            header: t(
              "searchFindings.tabVuln.handleAcceptanceModal.submittedForm.submittedTable.groupName",
            ),
          },
          {
            accessorFn: (row): string | undefined => row.finding?.title,
            accessorKey: "finding",
            header: t(
              "searchFindings.tabVuln.handleAcceptanceModal.submittedForm.submittedTable.type",
            ),
          },
        ] as ColumnDef<IVulnRowAttr>[])
      : []),
    {
      accessorKey: "where",
      header: t(
        "searchFindings.tabVuln.handleAcceptanceModal.submittedForm.submittedTable.where",
      ),
    },
    {
      accessorKey: "specific",
      header: t(
        "searchFindings.tabVuln.handleAcceptanceModal.submittedForm.submittedTable.specific",
      ),
    },
    {
      accessorKey: "severityThreatScore",
      cell: (cell: ICellHelper<IVulnRowAttr>): JSX.Element =>
        severityFormatter(cell.getValue()),
      header: t(
        "searchFindings.tabVuln.handleAcceptanceModal.submittedForm.submittedTable.severity",
      ),
    },
    {
      accessorKey: "acceptance",
      cell: (cell: ICellHelper<IVulnRowAttr>): JSX.Element =>
        changeSubmittedFormatter(
          cell.row.original,
          handleConfirmSubmitted,
          handleRejectSubmitted,
        ),
      header: t(
        "searchFindings.tabVuln.handleAcceptanceModal.submittedForm.submittedTable.acceptance",
      ),
    },
  ];

  const handleOnChange = useCallback(
    (newValue: "APPROVED" | "REJECTED"): (() => void) => {
      return (): void => {
        const newVulnList: IVulnRowAttr[] = acceptanceVulns.map(
          (vuln: IVulnRowAttr): IVulnRowAttr => ({
            ...vuln,
            acceptance: newValue,
          }),
        );
        setAcceptanceVulns([...newVulnList]);
        setSelectedVulnerabilities([...newVulnList]);
      };
    },
    [acceptanceVulns, setAcceptanceVulns],
  );

  return (
    <StrictMode>
      {isConfirmRejectVulnerabilitySelected ? (
        <Fragment>
          <Table
            columns={columns as ColumnDef<IVulnRowAttr>[]}
            data={acceptanceVulns}
            extraButtons={
              <Fragment>
                <Button
                  icon={"check"}
                  onClick={handleOnChange("APPROVED")}
                  variant={"ghost"}
                >
                  {t(
                    "searchFindings.tabDescription.handleAcceptanceModal.zeroRisk.globalSwitch.confirmAll",
                  )}
                </Button>
                <Button
                  icon={"xmark"}
                  onClick={handleOnChange("REJECTED")}
                  variant={"ghost"}
                >
                  {t(
                    "searchFindings.tabDescription.handleAcceptanceModal.zeroRisk.globalSwitch.rejectAll",
                  )}
                </Button>
              </Fragment>
            }
            onRowClick={openAdditionalInfoModal}
            options={{ enableRowSelection: true }}
            rowSelectionSetter={setSelectedVulnerabilities}
            rowSelectionState={selectedVulnerabilities}
            tableRef={tableRef}
          />
          {currentRow ? (
            <VulnerabilityModal
              clearSelectedVulns={undefined}
              closeModal={closeAdditionalInfoModal}
              currentRow={currentRow}
              findingId={currentRow.findingId}
              groupName={currentRow.groupName}
              isFindingReleased={false}
              isModalOpen={true}
              refetchData={refetchData}
            />
          ) : undefined}
        </Fragment>
      ) : undefined}
    </StrictMode>
  );
};

export { SubmittedTable };
