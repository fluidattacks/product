import { CodeSnippet } from "@fluidattacks/design";
import * as React from "react";
import type { ExtraProps } from "react-markdown";

export const CodeComponent = (
  props: ExtraProps & React.HTMLAttributes<HTMLElement>,
): JSX.Element => {
  const { children: snippet, className } = props;
  const match = /^language-(?<language>\w+)$/u.exec(className ?? "");

  return match ? (
    <CodeSnippet snippet={String(snippet).replace(/\n$/u, "")} />
  ) : (
    <code className={className}>{snippet}</code>
  );
};
