import React, { useEffect, useRef } from "react";
import { useTranslation } from "react-i18next";
import Markdown from "react-markdown";

import { CodeComponent } from "./code";
import { useCustomFix } from "./hooks";

interface ICustomFixProps {
  vulnId: string;
}

export const CustomFix: React.FC<ICustomFixProps> = ({
  vulnId,
}): JSX.Element => {
  const { t } = useTranslation();

  const { customFixMarkdown, loading } = useCustomFix(vulnId);

  const endRef = useRef<HTMLDivElement>(null);
  useEffect((): void => {
    endRef.current?.scrollIntoView({ behavior: "smooth" });
  }, [customFixMarkdown]);

  if (loading) {
    return (
      <div>{t("searchFindings.tabVuln.contentTab.customFix.loading")}</div>
    );
  }

  return (
    <React.Fragment>
      <Markdown components={{ code: CodeComponent }}>
        {customFixMarkdown}
      </Markdown>
      <div ref={endRef} />
    </React.Fragment>
  );
};
