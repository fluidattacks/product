import { graphql } from "gql";

const GET_VULN_SEVERITY_INFO = graphql(`
  query GetVulnSeverityInfo($vulnId: String!) {
    vulnerability(uuid: $vulnId) {
      id

      severityTemporalScore
      severityThreatScore

      severityVector
      severityVectorV4
      state
    }
  }
`);

const UPDATE_VULNERABILITIES_SEVERITY = graphql(`
  mutation UpdateVulnerabilitiesSeverity(
    $cvss4Vector: String!
    $cvssVector: String
    $findingId: ID!
    $vulnerabilityIds: [ID!]!
  ) {
    updateVulnerabilitiesSeverity(
      cvss4Vector: $cvss4Vector
      cvssVector: $cvssVector
      findingId: $findingId
      vulnerabilityIds: $vulnerabilityIds
    ) {
      success
    }
  }
`);

const REQUEST_VULNERABILITIES_SEVERITY_UPDATE = graphql(`
  mutation RequestVulnerabilitiesSeverityUpdate(
    $cvss4Vector: String!
    $cvssVector: String
    $findingId: ID!
    $vulnerabilityIds: [ID!]!
  ) {
    requestVulnerabilitiesSeverityUpdate(
      cvss4Vector: $cvss4Vector
      cvssVector: $cvssVector
      findingId: $findingId
      vulnerabilityIds: $vulnerabilityIds
    ) {
      success
    }
  }
`);

export {
  GET_VULN_SEVERITY_INFO,
  UPDATE_VULNERABILITIES_SEVERITY,
  REQUEST_VULNERABILITIES_SEVERITY_UPDATE,
};
