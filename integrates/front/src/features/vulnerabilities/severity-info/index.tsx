import { useMutation, useQuery } from "@apollo/client";
import { Col, Link, Row } from "@fluidattacks/design";
import _ from "lodash";
import { Fragment, useCallback, useState } from "react";
import React from "react";
import { useTranslation } from "react-i18next";
import { object, string } from "yup";

import {
  GET_VULN_SEVERITY_INFO,
  UPDATE_VULNERABILITIES_SEVERITY,
} from "./queries";
import type { ISeverityInfoProps } from "./types";

import { ActionButtons } from "../additional-info/action-buttons";
import { Detail } from "../additional-info/detail";
import { Form } from "components/form";
import { Input } from "components/input";
import { Can } from "context/authz/can";
import { VulnerabilityState } from "gql/graphql";
import { useAudit } from "hooks/use-audit";
import { GET_FINDING_HEADER } from "pages/finding/queries";
import { SeverityContent } from "pages/finding/severity/content";
import { SeverityContent as SeverityContentV4 } from "pages/finding/severity/content-v4";
import { CVSS3_CALCULATOR_BASE_URL, getCVSS31Values } from "utils/cvss";
import { CVSS4_CALCULATOR_BASE_URL, getCVSS40Values } from "utils/cvss4";
import { severityFormatter } from "utils/format-helpers";
import { Logger } from "utils/logger";
import { msgError, msgSuccess } from "utils/notifications";

export const SeverityInfo: React.FC<ISeverityInfoProps> = ({
  findingId,
  refetchData,
  cvssVersion,
  vulnerabilityId,
}: ISeverityInfoProps): JSX.Element => {
  const { t } = useTranslation();
  const [isEditing, setIsEditing] = useState(false);
  const prefix = "searchFindings.tabVuln.severityInfo.";

  const { addAuditEvent } = useAudit();
  const { data } = useQuery(GET_VULN_SEVERITY_INFO, {
    onCompleted: (): void => {
      addAuditEvent("Vulnerability.Severity", vulnerabilityId);
    },
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error): void => {
        msgError(t("groupAlerts.errorTextsad"));
        Logger.warning("An error occurred loading the severity info", error);
      });
    },
    variables: {
      vulnId: vulnerabilityId,
    },
  });
  const [updateVulnerabilitiesSeverity] = useMutation(
    UPDATE_VULNERABILITIES_SEVERITY,
    {
      onCompleted: (result): void => {
        if (result.updateVulnerabilitiesSeverity.success) {
          msgSuccess(
            t(`${prefix}alerts.updatedSeverity`),
            t("groupAlerts.updatedTitle"),
          );
          setIsEditing(false);
          refetchData();
        }
      },
      onError: ({ graphQLErrors }): void => {
        graphQLErrors.forEach((error): void => {
          if (
            error.message ===
            "Exception - Error invalid severity CVSS v3.1 vector string"
          ) {
            msgError(t(`${prefix}alerts.invalidSeverityVector`));
          } else if (
            error.message ===
            "Exception - Error invalid severity CVSS v4 vector string"
          ) {
            msgError(t(`${prefix}alerts.invalidSeverityVectorV4`));
          } else {
            msgError(t("groupAlerts.errorTextsad"));
            Logger.warning("An error occurred updating severity", error);
          }
        });
      },
      refetchQueries: [
        {
          query: GET_FINDING_HEADER,
          variables: {
            findingId,
          },
        },
        {
          query: GET_VULN_SEVERITY_INFO,
          variables: {
            vulnId: vulnerabilityId,
          },
        },
      ],
    },
  );

  const toggleEdit = useCallback((): void => {
    setIsEditing((currentValue: boolean): boolean => !currentValue);
  }, []);

  const onSubmit = useCallback(
    async (values: {
      severityVector: string;
      severityVectorV4: string | null;
    }): Promise<void> => {
      await updateVulnerabilitiesSeverity({
        variables: {
          cvss4Vector: values.severityVectorV4 ?? "",
          cvssVector: values.severityVector,
          findingId,
          vulnerabilityIds: [vulnerabilityId],
        },
      });
    },
    [findingId, vulnerabilityId, updateVulnerabilitiesSeverity],
  );

  if (_.isUndefined(data) || _.isEmpty(data)) {
    return <div />;
  }
  if (_.isNil(data.vulnerability) || _.isEmpty(data.vulnerability)) {
    return <div />;
  }

  const cvss31Values = getCVSS31Values(data.vulnerability.severityVector);
  const cvss40Values = getCVSS40Values(data.vulnerability.severityVectorV4);

  return (
    <React.StrictMode>
      <Row>
        <Form
          enableReinitialize={true}
          initialValues={{
            severityVector: data.vulnerability.severityVector,
            severityVectorV4: data.vulnerability.severityVectorV4,
          }}
          name={"editVulnerabilitySeverity"}
          onSubmit={onSubmit}
          validationSchema={object().shape({
            severityVector: string().required(t("validations.required")),
            severityVectorV4: string().required(t("validations.required")),
          })}
        >
          {({ dirty, submitForm }): React.ReactNode => {
            return (
              <Fragment>
                <Can
                  do={
                    "integrates_api_mutations_update_vulnerabilities_severity_mutate"
                  }
                >
                  {data.vulnerability.state ===
                  VulnerabilityState.Safe ? undefined : (
                    <ActionButtons
                      isEditing={isEditing}
                      isPristine={!dirty}
                      onEdit={toggleEdit}
                      onUpdate={submitForm}
                    />
                  )}
                </Can>
                <Row>
                  <Col>
                    <Detail
                      dataPrivate={false}
                      editableField={<Input name={"severityVector"} />}
                      field={
                        <Link
                          href={`${CVSS3_CALCULATOR_BASE_URL}#${data.vulnerability.severityVector}`}
                          iconPosition={"hidden"}
                        >
                          {data.vulnerability.severityVector}
                        </Link>
                      }
                      isEditing={isEditing}
                      label={<b>{t(`${prefix}severityVectorTitle`)}</b>}
                    />
                  </Col>
                </Row>
                <Row>
                  <Col>
                    <Detail
                      dataPrivate={false}
                      editableField={<Input name={"severityVectorV4"} />}
                      field={
                        <Link
                          href={`${CVSS4_CALCULATOR_BASE_URL}#${data.vulnerability.severityVectorV4}`}
                          iconPosition={"hidden"}
                        >
                          {data.vulnerability.severityVectorV4}
                        </Link>
                      }
                      isEditing={isEditing}
                      label={<b>{t(`${prefix}severityVectorTitleV4`)}</b>}
                    />
                  </Col>
                </Row>
              </Fragment>
            );
          }}
        </Form>
      </Row>
      <br />
      {isEditing ? undefined : (
        <Row>
          <Row>
            <Col lg={30}>
              <Detail
                dataPrivate={false}
                field={severityFormatter(
                  data.vulnerability.severityTemporalScore,
                )}
                isEditing={false}
                label={<b>{t(`${prefix}severityTemporalScore`)}</b>}
              />
            </Col>
            <Col lg={30}>
              <Detail
                dataPrivate={false}
                field={severityFormatter(
                  data.vulnerability.severityThreatScore,
                )}
                isEditing={false}
                label={<b>{t(`${prefix}severityThreatScore`)}</b>}
              />
            </Col>
          </Row>
          {cvssVersion === "severityThreatScore" ? (
            <SeverityContentV4 {...cvss40Values} />
          ) : (
            <SeverityContent {...cvss31Values} />
          )}
        </Row>
      )}
    </React.StrictMode>
  );
};
