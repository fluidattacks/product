/* eslint-disable @typescript-eslint/strict-boolean-expressions */
import { SlideOutMenu } from "@fluidattacks/design";
import { getLocalTimeZone, today } from "@internationalized/date";
import { Form, Formik } from "formik";
import { useCallback } from "react";
import * as React from "react";
import { object, string } from "yup";

import { Input, InputDateRange, Select } from "components/input";
import type { Technique } from "gql/graphql";
import {
  type VulnerabilityFiltersInput,
  type VulnerabilityState,
  VulnerabilityTreatment,
} from "gql/graphql";
import { useAuthz } from "hooks/use-authz";
import type { IUseModal } from "hooks/use-modal";
import type { IFindingInfoResponse } from "pages/finding/vulnerabilities/hooks/types";
import { useFiltersStore } from "pages/finding/vulnerabilities/hooks/use-filters-store";
import { translate } from "utils/translations/translate";
import type { TFormFilters } from "utils/zustand";

interface IVulnerabilitiesFilterProps {
  readonly modalRef: IUseModal;
  readonly finding: IFindingInfoResponse["data"]["finding"];
  readonly groupName: string;
}

const TECHNIQUES = ["CSPM", "DAST", "PTAAS", "RE", "SAST", "SCA", "SCR"];

const TREATMENTS = [...Object.values(VulnerabilityTreatment)];

const VulnerabilitiesFilters: React.FC<IVulnerabilitiesFilterProps> = ({
  modalRef,
  finding,
  groupName,
}): JSX.Element => {
  const { close, isOpen } = modalRef;
  const { can } = useAuthz();
  const {
    state,
    setPageIndex,
    setAssignees,
    setReattack,
    setReportedAfter,
    setReportedBefore,
    setState,
    setTreatment,
    formatTreatment,
    formatTechnique,
    setTechnique,
    setTags,
    setSearch,
    reset,
  } = useFiltersStore(`vulnerabilitiesFilters-${groupName}`);

  const canSeeDraft = can("see_draft_status");

  const handleFilters = useCallback(
    (filters: { [K in keyof VulnerabilityFiltersInput]: string }): void => {
      setAssignees?.(filters.assignees ? [filters.assignees] : undefined);
      setReattack?.(filters.reattack);
      setReportedAfter?.(filters.reportedAfter);
      setReportedBefore?.(filters.reportedBefore);

      setState?.(
        filters.state
          ? [filters.state as unknown as VulnerabilityState]
          : undefined,
      );

      setTags?.(filters.tags ? [filters.tags] : undefined);
      setTechnique?.(
        filters.technique
          ? [filters.technique as unknown as Technique]
          : undefined,
      );
      setTreatment?.(
        filters.treatment
          ? [filters.treatment as unknown as VulnerabilityTreatment]
          : undefined,
      );
      setSearch?.(filters.where);

      setPageIndex?.(0);
      close();
    },
    [
      close,
      setState,
      setTechnique,
      setAssignees,
      setReattack,
      setReportedAfter,
      setReportedBefore,
      setTreatment,
      setPageIndex,
      setSearch,
      setTags,
    ],
  );

  const clear = useCallback(
    (resetForm: () => void): VoidFunction => {
      return (): void => {
        resetForm();
        reset?.();

        setPageIndex?.(0);
      };
    },
    [reset, setPageIndex],
  );

  return (
    <Formik<TFormFilters<VulnerabilityFiltersInput>>
      initialValues={{
        assignees: "",
        reattack: "",
        reportedAfter: "",
        reportedBefore: "",
        state: (state?.length ?? 0) > 1 ? "" : (state?.[0] ?? ""),
        tags: "",
        technique: "",
        treatment: "",
        where: "",
      }}
      onSubmit={handleFilters}
      validationSchema={object({
        assignees: string().email(),
        state: string(),
        tags: string(),
        treatment: string(),
        where: string(),
      })}
    >
      {({ resetForm }): JSX.Element => (
        <Form>
          <SlideOutMenu
            closeIconId={"close-filters"}
            isOpen={isOpen}
            onClose={close}
            primaryButtonText={translate.t("filter.confirm")}
            secondaryButtonText={"Clear filters"}
            secondaryOnClick={clear(resetForm)}
            title={translate.t("filter.title")}
          >
            <Input label={"Location"} name={"where"} />
            <Select
              items={[
                { header: "All", value: "" },
                { header: "Vulnerable", value: "VULNERABLE" },
                { header: "Safe", value: "SAFE" },
                ...(canSeeDraft
                  ? [
                      { header: "Submitted", value: "SUBMITTED" },
                      { header: "Rejected", value: "REJECTED" },
                    ]
                  : []),
              ]}
              label={"Status"}
              name={"state"}
            />
            <Select
              items={[
                { header: "All", value: "" },
                ...TECHNIQUES.map(
                  (technique): { header: string; value: string } => ({
                    header: formatTechnique?.(technique) ?? technique,
                    value: technique,
                  }),
                ),
              ]}
              label={"Technique"}
              name={"technique"}
            />
            <InputDateRange
              label={"Reported date"}
              maxName={"reportedBefore"}
              maxValue={today(getLocalTimeZone())}
              minName={"reportedAfter"}
              name={"reported"}
            />
            <Select
              items={[
                { header: "All", value: "" },
                { header: "Not requested", value: "NotRequested" },
                { header: "On hold", value: "On_hold" },
                { header: "Requested", value: "Requested" },
                { header: "Verified", value: "Verified" },
              ]}
              label={"Reattack"}
              name={"reattack"}
            />
            <Select
              items={[
                { header: "All", value: "" },
                ...TREATMENTS.map(
                  (treatment): { header: string; value: string } => ({
                    header: formatTreatment?.(treatment) ?? treatment,
                    value: treatment.toUpperCase(),
                  }),
                ),
              ]}
              label={"Treatment"}
              name={"treatment"}
            />
            <Input label={"Tags"} name={"tags"} />
            <Select
              items={[
                { header: "All", value: "" },
                ...(finding ?? { assignees: [] }).assignees.map(
                  (assignee): { header: string; value: string } => ({
                    header: assignee.replace("_", " "),
                    value: assignee,
                  }),
                ),
              ]}
              label={"Assignees"}
              name={"assignees"}
            />
          </SlideOutMenu>
        </Form>
      )}
    </Formik>
  );
};

export { VulnerabilitiesFilters };
