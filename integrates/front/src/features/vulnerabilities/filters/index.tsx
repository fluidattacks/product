import { useContext, useEffect, useState } from "react";

import type { IVulnerabilitiesFilterProps } from "./types";
import { getFilters } from "./utils";

import type { IVulnRowAttr } from "../types";
import { Filters } from "components/filter";
import type { IFilter, IPermanentData } from "components/filter/types";
import { meetingModeContext } from "context/meeting-mode";
import { useStoredState } from "hooks/use-stored-state";
import { isVulnerabilityState } from "pages/finding/vulnerabilities/utils";
import { setFiltersUtil } from "utils/set-filters";

const VulnerabilitiesFilters = ({
  vulnerabilities,
  setFilteredVulns,
  setVulnerabilitiesState,
  groupName,
}: IVulnerabilitiesFilterProps): JSX.Element => {
  const { meetingMode } = useContext(meetingModeContext);

  const [filters, setFilters] = useState<IFilter<IVulnRowAttr>[]>(
    getFilters(groupName),
  );

  const [filterVal, setFilterVal] = useStoredState<IPermanentData[]>(
    `vulnerabilitiesTableFilters-${groupName}`,
    filters.filter((filter): boolean => filter.id !== "where"),
    localStorage,
  );

  useEffect((): void => {
    const filteredVulnerabilities = setFiltersUtil(
      meetingMode
        ? vulnerabilities.filter((vulnerability): boolean =>
            ["VULNERABLE", "SAFE"].includes(vulnerability.state),
          )
        : vulnerabilities,
      filters,
    );
    const newState = filters.find(
      (filter): boolean => filter.id === "currentState",
    )?.value;

    setVulnerabilitiesState(
      isVulnerabilityState(newState) ? newState : undefined,
    );

    setFilteredVulns(filteredVulnerabilities);
  }, [
    filters,
    meetingMode,
    setFilteredVulns,
    setVulnerabilitiesState,
    vulnerabilities,
  ]);

  return (
    <Filters
      dataset={vulnerabilities}
      filters={filters}
      permaset={[filterVal, setFilterVal]}
      setFilters={setFilters}
    />
  );
};

export { VulnerabilitiesFilters };
