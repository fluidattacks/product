import DiffMatchPatch, { DIFF_DELETE, DIFF_INSERT } from "diff-match-patch";
import { Fragment } from "react";

import type { IHighlightDiffProps } from "./types";

const highlightDifferences = (
  oldString: string,
  newString: string,
): JSX.Element => {
  const dmp = new DiffMatchPatch();
  const diffs = dmp.diff_main(oldString, newString) as [number, string][];
  dmp.diff_cleanupSemantic(diffs);

  return (
    <Fragment>
      {diffs.map(([op, text]): JSX.Element => {
        if (op === DIFF_INSERT) {
          return (
            <span key={text} style={{ backgroundColor: "lightgreen" }}>
              {text}
            </span>
          );
        } else if (op === DIFF_DELETE) {
          return (
            <span
              key={text}
              style={{
                backgroundColor: "salmon",
                textDecoration: "line-through",
              }}
            >
              {text}
            </span>
          );
        }

        return <span key={text}>{text}</span>;
      })}
    </Fragment>
  );
};

const HighlightDiff = ({
  oldString,
  newString,
}: Readonly<IHighlightDiffProps>): JSX.Element => {
  return (
    <div>
      <div>{highlightDifferences(oldString, newString)}</div>
    </div>
  );
};

export { HighlightDiff };
