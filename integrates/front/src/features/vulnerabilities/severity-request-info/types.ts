interface ISeverityRequestInfoProps {
  vulnerabilityId: string;
}

export type { ISeverityRequestInfoProps };
