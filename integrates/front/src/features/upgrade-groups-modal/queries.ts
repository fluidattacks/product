import { graphql } from "gql";

interface IGroup {
  __typename: "Group";
  name: string;
  permissions: string[];
  serviceAttributes: string[];
}

const GET_USER_ORGANIZATIONS_GROUPS = graphql(`
  query GetUserOrganizationsGroupsAtGroupModals {
    me {
      userEmail
      organizations {
        name
        groups {
          name
          permissions
          serviceAttributes
        }
      }
    }
  }
`);

const REQUEST_GROUPS_UPGRADE_MUTATION = graphql(`
  mutation RequestGroupsUpgrade($groupNames: [String!]!) {
    requestGroupsUpgrade(groupNames: $groupNames) {
      success
    }
  }
`);

export type { IGroup };
export { GET_USER_ORGANIZATIONS_GROUPS, REQUEST_GROUPS_UPGRADE_MUTATION };
