import {
  type IFilterOptions,
  type IOptionsProps,
  type IUseFilterProps,
  useFilters,
} from "@fluidattacks/design";
import { useCallback, useMemo } from "react";
import { useTranslation } from "react-i18next";

import type { IToElineFilters } from "./types";

import {
  getCheckedFilter,
  getDateRangeFilter,
  getNewFilter,
  getNumberRangeFilter,
} from "components/filter/utils";
import type { IToeLinesData } from "pages/group/surface/lines/types";
import { formatBoolean, formatStatus } from "utils/format-helpers";

const useToELinesFilters = ({
  groupName,
  setBackFilters,
}: IToElineFilters): IUseFilterProps<IToeLinesData> => {
  const { t } = useTranslation();

  const options: IOptionsProps<IToeLinesData>[] = useMemo(
    (): IOptionsProps<IToeLinesData>[] => [
      {
        filterOptions: [
          {
            key: "filename",
            label: t("group.toe.lines.filename"),
            type: "text",
          },
        ],
        label: t("group.toe.lines.filename"),
      },
      {
        filterOptions: [
          {
            key: "loc",
            label: t("group.toe.lines.loc"),
            type: "numberRange",
          },
        ],
        label: t("group.toe.lines.loc"),
      },
      {
        filterOptions: [
          {
            key: "hasVulnerabilities",
            label: t("group.toe.lines.status"),
            options: [
              { label: formatStatus(true), value: "true" },
              { label: formatStatus(false), value: "false" },
            ],
            type: "radioButton",
          },
        ],
        label: t("group.toe.lines.status"),
      },
      {
        filterOptions: [
          {
            key: "modifiedDate",
            label: t("group.toe.lines.modifiedDate"),
            type: "dateRange",
          },
        ],
        label: t("group.toe.lines.modifiedDate"),
      },
      {
        filterOptions: [
          {
            key: "lastCommit",
            label: t("group.toe.lines.lastCommit"),
            type: "text",
          },
        ],
        label: t("group.toe.lines.lastCommit"),
      },
      {
        filterOptions: [
          {
            key: "lastAuthor",
            label: t("group.toe.lines.lastAuthor"),
            type: "text",
          },
        ],
        label: t("group.toe.lines.lastAuthor"),
      },
      {
        filterOptions: [
          {
            key: "seenAt",
            label: t("group.toe.lines.seenAt"),
            type: "dateRange",
          },
        ],
        label: t("group.toe.lines.seenAt"),
      },
      {
        filterOptions: [
          {
            key: "sortsPriorityFactor",
            label: t("group.toe.lines.sortsPriorityFactor"),
            type: "numberRange",
          },
        ],
        label: t("group.toe.lines.sortsPriorityFactor"),
      },
      {
        filterOptions: [
          {
            key: "bePresent",
            label: t("group.toe.lines.bePresent"),
            options: [
              { label: formatBoolean(true), value: "true" },
              { label: formatBoolean(false), value: "false" },
            ],
            type: "radioButton",
          },
        ],
        label: t("group.toe.lines.bePresent"),
      },
    ],
    [t],
  );

  const handleFilterChange = useCallback(
    (newFilters: readonly IFilterOptions<IToeLinesData>[]): void => {
      const newFiltersArray = newFilters as IFilterOptions<IToeLinesData>[];
      const filename = getNewFilter(newFiltersArray, "filename")?.value;
      const { min: minLoc, max: maxLoc } = getNumberRangeFilter(
        newFiltersArray,
        "loc",
      );
      const hasVulnerabilities = getCheckedFilter(
        newFiltersArray,
        "hasVulnerabilities",
      )?.value;
      const { from: fromModifiedDate, to: toModifiedDate } = getDateRangeFilter(
        newFiltersArray,
        "modifiedDate",
      );
      const lastCommit = getNewFilter(newFiltersArray, "lastCommit")?.value;
      const lastAuthor = getNewFilter(newFiltersArray, "lastAuthor")?.value;
      const { from: fromSeenAt, to: toSeenAt } = getDateRangeFilter(
        newFiltersArray,
        "seenAt",
      );
      const { min: minSortsPriorityFactor, max: maxSortsPriorityFactor } =
        getNumberRangeFilter(newFiltersArray, "sortsPriorityFactor");
      const bePresent = getCheckedFilter(newFiltersArray, "bePresent")?.value;
      const toeLinesBackFilters = {
        bePresent: bePresent === undefined ? undefined : bePresent === "true",
        filename: filename ?? undefined,
        fromModifiedDate,
        fromSeenAt,
        hasVulnerabilities:
          hasVulnerabilities === undefined
            ? undefined
            : hasVulnerabilities === "true",
        lastAuthor: lastAuthor ?? undefined,
        lastCommit: lastCommit ?? undefined,
        maxLoc,
        maxSortsPriorityFactor,
        minLoc,
        minSortsPriorityFactor,
        toModifiedDate,
        toSeenAt,
      };

      setBackFilters(toeLinesBackFilters);
    },
    [setBackFilters],
  );

  return useFilters({
    localStorageKey: `toeLinesTableFilters-${groupName}`,
    onFiltersChange: handleFilterChange,
    options,
  });
};

export { useToELinesFilters };
