import { graphql } from "gql";

export const GET_GROUP_ROOTS = graphql(`
  query GetGroupRoots($groupName: String!) {
    group(groupName: $groupName) {
      closedVulnerabilities
      description
      hasAdvanced
      hasEssential
      managed
      name
      openFindings
      openVulnerabilities
      service
      subscription
      userRole
      roots {
        ... on GitRoot {
          cloningStatus {
            status
          }
          machineStatus {
            modifiedDate
            status
          }
        }
      }
    }
  }
`);
