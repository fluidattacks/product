interface IHeader {
  title: string;
  subtitle: string;
  description?: string;
  icon: string;
}

export type { IHeader };
