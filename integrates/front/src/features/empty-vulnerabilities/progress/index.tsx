import { useTranslation } from "react-i18next";

import { ProgressIndicator, Step } from "components/progress-indicator";
import type { IStepProps } from "components/progress-indicator/types";

interface IProgressProps {
  readonly currentStepNumber: number;
  readonly itemsProps: IStepProps[];
}

const Progress = ({
  currentStepNumber,
  itemsProps,
}: IProgressProps): JSX.Element => {
  const { t } = useTranslation();

  return (
    <ProgressIndicator>
      {itemsProps.map((props: IStepProps, index): JSX.Element => {
        const { state, stepNumber, title } = props;

        return (
          <Step
            key={`icon-${index + 1}`}
            state={stepNumber <= currentStepNumber ? state : "disabled"}
            stepNumber={stepNumber}
            title={t(title)}
          />
        );
      })}
    </ProgressIndicator>
  );
};

export { Progress };
