import { useQuery } from "@apollo/client";
import { Container } from "@fluidattacks/design";
import _ from "lodash";
import { useMemo } from "react";
import { useParams } from "react-router-dom";

import { Header } from "./header";
import { HelperButtons } from "./helper-buttons";
import { Progress } from "./progress";
import { GET_GROUP_ROOTS } from "./queries";
import type { IStepItemsFormat } from "./types";
import { isOnlyEssential } from "./utils";

import type { TStepState } from "components/progress-indicator/types";
import type {
  IGitRootAttr,
  IGroupData,
  IGroupDataResult,
  TRoot,
} from "pages/organization/groups/types";
import {
  hasFailedGitRootCloning,
  isGitRootCloning,
  noRootFound,
  noVulnerabilitiesFound,
} from "pages/organization/groups/utils";
import { Logger } from "utils/logger";

const EmptyVulnerabilities = (): JSX.Element => {
  const { groupName } = useParams() as { groupName: string };

  const { data, loading } = useQuery(GET_GROUP_ROOTS, {
    fetchPolicy: "cache-first",
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error): void => {
        Logger.error("Couldn't load roots", error);
      });
    },
    variables: { groupName },
  });

  const hasRootCloned = (
    roots: TRoot[],
    gitRoots: IGitRootAttr[],
  ): TStepState => {
    if (roots.length !== gitRoots.length) return "completed";
    if (isGitRootCloning(gitRoots)) return "in progress";
    if (hasFailedGitRootCloning(gitRoots)) return "error";

    return "completed";
  };

  const stepItemsFormat: IStepItemsFormat[] =
    useMemo((): IStepItemsFormat[] => {
      if (_.isNil(data)) return [];
      const { roots } = data.group;
      const gitRoots = roots?.filter(
        (root): boolean => root.__typename === "GitRoot",
      ) as IGitRootAttr[];
      const hasOnlyEssential = isOnlyEssential(data.group as IGroupData);
      const onlyEssentialSubtitle = hasOnlyEssential
        ? "components.empty.subtitle.essential"
        : "components.empty.subtitle.advanced";

      const stepItems: IStepItemsFormat[] = [
        {
          header: {
            icon: "integrates/empty/addRoot",
            subtitle: "components.empty.steps.addRoot.subtitle",
            title: "components.empty.steps.addRoot.title",
          },
          state: noRootFound(gitRoots) ? "error" : "completed",
          stepNumber: 1,
          title: "components.empty.steps.addRoot.stepTitle",
        },
        {
          header:
            hasRootCloned(roots as TRoot[], gitRoots) === "error"
              ? {
                  icon: "integrates/empty/cloningError",
                  subtitle: "components.empty.steps.cloning.subtitle",
                  title: "components.empty.steps.cloning.title",
                }
              : {
                  description: hasOnlyEssential
                    ? "components.empty.description"
                    : undefined,
                  icon: "integrates/empty/cloning",
                  subtitle: onlyEssentialSubtitle,
                  title: "components.empty.title",
                },
          state: hasRootCloned(roots as TRoot[], gitRoots),
          stepNumber: 2,
          title: "components.empty.steps.cloning.stepTitle",
        },
        {
          header: {
            description: hasOnlyEssential
              ? "components.empty.description"
              : undefined,
            icon: "integrates/empty/testing",
            subtitle: onlyEssentialSubtitle,
            title: "components.empty.title",
          },
          state: noVulnerabilitiesFound(
            gitRoots,
            data.group as IGroupDataResult,
          )
            ? "completed"
            : "in progress",
          stepNumber: 3,
          title: "components.empty.steps.testing",
        },
        {
          header: {
            icon: "integrates/empty/noVulnerabilities",
            subtitle: "components.empty.steps.findings.subtitle",
            title: "components.empty.steps.findings.title",
          },
          state: "error",
          stepNumber: 4,
          title: "components.empty.steps.findings.stepTitle",
        },
      ];

      return stepItems;
    }, [data]);

  const lastNonCompletedStep = _.find(
    stepItemsFormat,
    (item): boolean => item.state !== "completed",
  );

  if (loading || !lastNonCompletedStep || !data) return <div />;

  const { stepNumber, header, state } = lastNonCompletedStep;

  return (
    <Container
      alignItems={"center"}
      display={"flex"}
      flexDirection={"column"}
      gap={2}
      justify={"center"}
      mb={2.5}
      mt={2.5}
    >
      <Header headerProps={header} />
      <Progress currentStepNumber={stepNumber} itemsProps={stepItemsFormat} />
      <HelperButtons state={state} stepNumber={stepNumber} />
    </Container>
  );
};

export { EmptyVulnerabilities };
