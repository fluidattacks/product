import { graphql } from "gql";

const GET_USER_ROLE = graphql(`
  query GetUserRole(
    $groupLevel: Boolean!
    $groupName: String!
    $organizationLevel: Boolean!
    $organizationName: String!
    $userLevel: Boolean!
  ) {
    group(groupName: $groupName) @include(if: $groupLevel) {
      name
      userRole
    }
    me @include(if: $userLevel) {
      role
      userEmail
    }
    organizationId(organizationName: $organizationName)
      @include(if: $organizationLevel) {
      name
      userRole
    }
  }
`);

export { GET_USER_ROLE };
