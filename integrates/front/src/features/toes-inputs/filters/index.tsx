import { useQuery } from "@apollo/client";
import { useAbility } from "@casl/react";
import { useEffect, useMemo, useState } from "react";
import * as React from "react";
import { useTranslation } from "react-i18next";

import { getBaseFilters, getFiltersToResearch, getTableFilters } from "./utils";

import { Filters } from "components/filter";
import type { IPermanentData } from "components/filter/types";
import { authzPermissionsContext } from "context/authz/config";
import type {
  GetRootsInfoAtInputsQuery,
  GetToeInputsQueryVariables,
} from "gql/graphql";
import { useStoredState } from "hooks/use-stored-state";
import { GET_TOE_INPUTS_COMPONENTS } from "pages/group/surface/inputs/queries";
import type { IToeInputData } from "pages/group/surface/inputs/types";
import { Logger } from "utils/logger";
import { msgError } from "utils/notifications";

const ToEInputsFilters = ({
  groupName,
  rootsData,
  isInternal,
  setFiltersToSearch,
  toeInputs,
}: Readonly<{
  groupName: string;
  isInternal: boolean;
  rootsData: GetRootsInfoAtInputsQuery | undefined;
  setFiltersToSearch: React.Dispatch<
    React.SetStateAction<Partial<GetToeInputsQueryVariables>>
  >;
  toeInputs: IToeInputData[];
}>): JSX.Element => {
  const permissions = useAbility(authzPermissionsContext);
  const { t } = useTranslation();

  const { data: componentData } = useQuery(GET_TOE_INPUTS_COMPONENTS, {
    fetchPolicy: "no-cache",
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error): void => {
        msgError(t("groupAlerts.errorTextsad"));
        Logger.warning("An error occurred loading toe input components", error);
      });
    },
    variables: { groupName },
  });

  const [filtersPermaset, setFiltersPermaset] = useStoredState<
    IPermanentData[]
  >(
    "tblToeInputsFilters",
    getBaseFilters([], [], []).map((filter): IPermanentData => {
      if (filter.type === "dateRange") {
        return {
          id: filter.id,
          rangeValues: filter.rangeValues,
          valueType: "rangeValues",
        };
      }

      return { id: filter.id, value: filter.value, valueType: "value" };
    }),
    localStorage,
  );

  const [filters, setFilters] = useState(
    getTableFilters(isInternal, permissions, [], [], filtersPermaset),
  );

  const uniqueComponents = useMemo((): string[] => {
    const components = componentData?.group.toeInputs.edges.map(
      (edge): string => edge.node.component,
    );

    return [
      ...new Set(components?.map((datapoint): string => datapoint)),
    ].filter(Boolean);
  }, [componentData]);

  const rootsNicknames = useMemo((): string[] => {
    const groupRoots = rootsData?.group.roots;

    return groupRoots ? groupRoots.map((item): string => item.nickname) : [];
  }, [rootsData]);

  useEffect((): void => {
    const newFilters = getFiltersToResearch(filters);
    setFiltersToSearch(newFilters);
  }, [filters, setFiltersToSearch]);

  useEffect((): void => {
    setFilters(
      getTableFilters(
        isInternal,
        permissions,
        rootsNicknames,
        uniqueComponents,
        filtersPermaset,
      ),
    );
  }, [
    isInternal,
    permissions,
    uniqueComponents,
    rootsNicknames,
    filtersPermaset,
  ]);

  return (
    <Filters
      dataset={toeInputs}
      filters={filters}
      permaset={[filtersPermaset, setFiltersPermaset]}
      setFilters={setFilters}
    />
  );
};

export { ToEInputsFilters };
