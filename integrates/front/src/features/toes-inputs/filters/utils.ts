import type { PureAbility } from "@casl/ability";
import isEmpty from "lodash/isEmpty";

import type { IFilter, IPermanentData } from "components/filter/types";
import type { GetToeInputsQueryVariables } from "gql/graphql";
import type { IToeInputData } from "pages/group/surface/inputs/types";
import { formatBoolean, formatStatus } from "utils/format-helpers";
import { translate } from "utils/translations/translate";

const getBaseFilters = (
  groupRoots: string[],
  components: string[],
  filtersPermaset: IPermanentData[],
): IFilter<IToeInputData>[] => {
  return [
    {
      id: "rootNickname",
      key: "rootNickname",
      label: translate.t("group.toe.inputs.root"),
      selectOptions: groupRoots,
      type: "select",
      value:
        filtersPermaset.find((filter): boolean => filter.id === "rootNickname")
          ?.value ?? "",
    },
    {
      id: "component",
      key: "component",
      label: translate.t("group.toe.inputs.component"),
      selectOptions: components,
      type: "select",
      value:
        filtersPermaset.find((filter): boolean => filter.id === "component")
          ?.value ?? "",
    },
    {
      id: "entryPoint",
      key: "entryPoint",
      label: translate.t("group.toe.inputs.entryPoint"),
      type: "text",
      value:
        filtersPermaset.find((filter): boolean => filter.id === "entryPoint")
          ?.value ?? "",
    },
    {
      id: "hasVulnerabilities",
      isBackFilter: true,
      key: "hasVulnerabilities",
      label: translate.t("group.toe.inputs.status"),
      selectOptions: [
        { header: formatStatus(true), value: "true" },
        { header: formatStatus(false), value: "false" },
      ],
      type: "select",
      value:
        filtersPermaset.find(
          (filter): boolean => filter.id === "hasVulnerabilities",
        )?.value ?? "true",
    },
    {
      id: "seenAt",
      key: "seenAt",
      label: translate.t("group.toe.inputs.seenAt"),
      rangeValues: filtersPermaset.find(
        (filter): boolean => filter.id === "seenAt",
      )?.rangeValues,
      type: "dateRange",
    },
    {
      id: "bePresent",
      isBackFilter: true,
      key: "bePresent",
      label: translate.t("group.toe.inputs.bePresent"),
      selectOptions: [
        { header: formatBoolean(true), value: "true" },
        { header: formatBoolean(false), value: "false" },
      ],
      type: "select",
      value:
        filtersPermaset.find((filter): boolean => filter.id === "bePresent")
          ?.value ?? "true",
    },
    {
      id: "attackedAt",
      key: "attackedAt",
      label: translate.t("group.toe.inputs.attackedAt"),
      rangeValues: filtersPermaset.find(
        (filter): boolean => filter.id === "attackedAt",
      )?.rangeValues,
      type: "dateRange",
    },
    {
      id: "attackedBy",
      key: "attackedBy",
      label: translate.t("group.toe.inputs.attackedBy"),
      type: "text",
      value:
        filtersPermaset.find((filter): boolean => filter.id === "attackedBy")
          ?.value ?? "",
    },
    {
      id: "firstAttackAt",
      key: "firstAttackAt",
      label: translate.t("group.toe.inputs.firstAttackAt"),
      rangeValues: filtersPermaset.find(
        (filter): boolean => filter.id === "firstAttackAt",
      )?.rangeValues,
      type: "dateRange",
    },
    {
      id: "seenFirstTimeBy",
      key: "seenFirstTimeBy",
      label: translate.t("group.toe.inputs.seenFirstTimeBy"),
      type: "text",
      value:
        filtersPermaset.find(
          (filter): boolean => filter.id === "seenFirstTimeBy",
        )?.value ?? "",
    },
    {
      id: "bePresentUntil",
      key: "bePresentUntil",
      label: translate.t("group.toe.inputs.bePresentUntil"),
      rangeValues: filtersPermaset.find(
        (filter): boolean => filter.id === "bePresentUntil",
      )?.rangeValues,
      type: "dateRange",
    },
  ];
};

const getTableFilters = (
  isInternal: boolean,
  permissions: PureAbility<string>,
  groupRoots: string[],
  components: string[],
  filtersPermaset: IPermanentData[],
): IFilter<IToeInputData>[] => {
  const canGetAttackedAt = permissions.can(
    "integrates_api_resolvers_toe_input_attacked_at_resolve",
  );
  const canGetAttackedBy = permissions.can(
    "integrates_api_resolvers_toe_input_attacked_by_resolve",
  );
  const canGetBePresentUntil = permissions.can(
    "integrates_api_resolvers_toe_input_be_present_until_resolve",
  );
  const canGetFirstAttackAt = permissions.can(
    "integrates_api_resolvers_toe_input_first_attack_at_resolve",
  );
  const canGetSeenFirstTimeBy = permissions.can(
    "integrates_api_resolvers_toe_input_seen_first_time_by_resolve",
  );

  const filters = getBaseFilters(groupRoots, components, filtersPermaset);

  return filters.filter((filter): boolean => {
    switch (filter.id) {
      case "attackedAt":
        return isInternal && canGetAttackedAt;
      case "attackedBy":
        return isInternal && canGetAttackedBy;
      case "firstAttackAt":
        return isInternal && canGetFirstAttackAt;
      case "seenFirstTimeBy":
        return isInternal && canGetSeenFirstTimeBy;
      case "bePresentUntil":
        return isInternal && canGetBePresentUntil;
      default:
        return true;
    }
  });
};

const getFiltersToResearch = (
  currentFilters: IFilter<IToeInputData>[],
): Partial<GetToeInputsQueryVariables> => {
  const parseBoolean = (value: string | undefined): boolean | undefined => {
    if (isEmpty(value)) {
      return undefined;
    }

    return value === "true";
  };

  return {
    attackedBy: currentFilters.find(
      (filter): boolean => filter.id === "attackedBy",
    )?.value,
    bePresent: parseBoolean(
      currentFilters.find((filter): boolean => filter.id === "bePresent")
        ?.value,
    ),
    component: currentFilters.find(
      (filter): boolean => filter.id === "component",
    )?.value,
    entryPoint: currentFilters.find(
      (filter): boolean => filter.id === "entryPoint",
    )?.value,
    fromAttackedAt: currentFilters.find(
      (filter): boolean => filter.id === "attackedAt",
    )?.rangeValues?.[0],
    fromBePresentUntil: currentFilters.find(
      (filter): boolean => filter.id === "bePresentUntil",
    )?.rangeValues?.[0],
    fromFirstAttackAt: currentFilters.find(
      (filter): boolean => filter.id === "firstAttackAt",
    )?.rangeValues?.[0],
    fromSeenAt: currentFilters.find((filter): boolean => filter.id === "seenAt")
      ?.rangeValues?.[0],
    hasVulnerabilities: parseBoolean(
      currentFilters.find(
        (filter): boolean => filter.id === "hasVulnerabilities",
      )?.value,
    ),
    rootNickname: currentFilters.find(
      (filter): boolean => filter.id === "rootNickname",
    )?.value,
    seenFirstTimeBy: currentFilters.find(
      (filter): boolean => filter.id === "seenFirstTimeBy",
    )?.value,
    toAttackedAt: currentFilters.find(
      (filter): boolean => filter.id === "attackedAt",
    )?.rangeValues?.[1],
    toBePresentUntil: currentFilters.find(
      (filter): boolean => filter.id === "bePresentUntil",
    )?.rangeValues?.[1],
    toFirstAttackAt: currentFilters.find(
      (filter): boolean => filter.id === "firstAttackAt",
    )?.rangeValues?.[1],
    toSeenAt: currentFilters.find((filter): boolean => filter.id === "seenAt")
      ?.rangeValues?.[1],
  };
};

export { getBaseFilters, getTableFilters, getFiltersToResearch };
