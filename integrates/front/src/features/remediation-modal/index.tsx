import type { IUseModal } from "@fluidattacks/design";
import {
  Alert,
  ComboBox,
  Form,
  InnerForm,
  Modal,
  Text,
  TextArea,
} from "@fluidattacks/design";
import { Fragment, useMemo } from "react";
import * as React from "react";
import { useTranslation } from "react-i18next";
import { Link, useLocation } from "react-router-dom";
import { useTheme } from "styled-components";

import type { IRemediationValues } from "features/remediation-modal/types";
import {
  MAX_TREATMENT_JUSTIFICATION_LENGTH,
  validationSchema,
} from "features/remediation-modal/validations";
import { VulnerabilityStateReason } from "gql/graphql";
import { useVulnerabilityStore } from "hooks/use-vulnerability-store";

interface IAddRemediationProps {
  readonly allHackersAreMachine?: boolean;
  readonly additionalInfo?: string;
  readonly alertMessage?: string;
  readonly children?: React.ReactNode;
  readonly isLoading?: boolean;
  readonly message: string;
  readonly modalRef: IUseModal;
  readonly title: string;
  readonly onSubmit: (values: IRemediationValues) => void;
  readonly subtitle?: string;
  readonly values: IRemediationValues;
  readonly remediationType: "verifyEvent" | "verifyVulnerability";
}

const RemediationModal = ({
  remediationType,
  additionalInfo,
  alertMessage,
  children,
  isLoading,
  message,
  modalRef,
  title,
  onSubmit,
  subtitle,
  values,
  allHackersAreMachine = false,
}: IAddRemediationProps): JSX.Element => {
  const theme = useTheme();
  const location = useLocation();
  const { t } = useTranslation();
  const { close } = modalRef;
  const { isReattacking } = useVulnerabilityStore();

  function goToScope(): string {
    const url = location.pathname;
    const vulnsIndex = url.indexOf("vulns");
    if (vulnsIndex !== -1) {
      const modifiedUrl = `${url.substring(0, vulnsIndex)}scope`;

      return modifiedUrl;
    }

    return url;
  }

  const translations = useMemo((): Record<string, string> => {
    return {
      [VulnerabilityStateReason.VerifiedAsSafe]: t(
        "vulnerability.stateReasons.verifiedAsSafe",
      ),
      [VulnerabilityStateReason.RootOrEnvironmentDeactivated]: t(
        "vulnerability.stateReasons.rootOrEnvironmentDeactivated",
      ),
      [VulnerabilityStateReason.Exclusion]: t(
        "vulnerability.stateReasons.exclusion",
      ),
      [VulnerabilityStateReason.NoJustification]: t(
        "vulnerability.stateReasons.noJustification",
      ),
      [VulnerabilityStateReason.FunctionalityNoLongerExists]: t(
        "vulnerability.stateReasons.functionalityNoLongerExists",
      ),
      [VulnerabilityStateReason.VerifiedAsVulnerable]: t(
        "vulnerability.stateReasons.verifiedAsVulnerable",
      ),
      [VulnerabilityStateReason.RootMovedToAnotherGroup]: t(
        "vulnerability.stateReasons.rootMovedToAnotherGroup",
      ),
    };
  }, [t]);

  return (
    <Modal description={subtitle} modalRef={modalRef} size={"md"} title={title}>
      <Form
        cancelButton={{ onClick: close }}
        confirmButton={{ disabled: isLoading }}
        defaultDisabled={false}
        defaultValues={values}
        onSubmit={onSubmit}
        yupSchema={validationSchema}
      >
        <InnerForm>
          {({
            formState: { errors },
            getFieldState,
            register,
            watch,
            control,
          }): JSX.Element => {
            const { invalid, isTouched } = getFieldState(
              "treatmentJustification",
            );

            return (
              <Fragment>
                {children}
                {allHackersAreMachine && isReattacking ? undefined : (
                  <Fragment>
                    <TextArea
                      error={errors.treatmentJustification?.message?.toString()}
                      isTouched={isTouched}
                      isValid={!invalid}
                      label={message}
                      maxLength={MAX_TREATMENT_JUSTIFICATION_LENGTH}
                      name={"treatmentJustification"}
                      register={register}
                      required={true}
                      rows={6}
                      watch={watch}
                    />
                    {isReattacking ||
                    remediationType === "verifyEvent" ? undefined : (
                      <ComboBox
                        control={control}
                        items={[
                          {
                            name: translations[
                              VulnerabilityStateReason.VerifiedAsSafe
                            ],
                            value: VulnerabilityStateReason.VerifiedAsSafe,
                          },
                          {
                            name: translations[
                              VulnerabilityStateReason.VerifiedAsVulnerable
                            ],
                            value:
                              VulnerabilityStateReason.VerifiedAsVulnerable,
                          },
                          {
                            name: translations[
                              VulnerabilityStateReason
                                .FunctionalityNoLongerExists
                            ],
                            value:
                              VulnerabilityStateReason.FunctionalityNoLongerExists,
                          },
                          {
                            name: translations[
                              VulnerabilityStateReason.Exclusion
                            ],
                            value: VulnerabilityStateReason.Exclusion,
                          },
                          {
                            name: translations[
                              VulnerabilityStateReason.RootMovedToAnotherGroup
                            ],
                            value:
                              VulnerabilityStateReason.RootMovedToAnotherGroup,
                          },
                          {
                            name: translations[
                              VulnerabilityStateReason
                                .RootOrEnvironmentDeactivated
                            ],
                            value:
                              VulnerabilityStateReason.RootOrEnvironmentDeactivated,
                          },
                        ]}
                        label={"Verification Reason"}
                        name={"verificationReason"}
                        required={true}
                      />
                    )}
                  </Fragment>
                )}
                <Text color={theme.palette.gray[800]} size={"sm"}>
                  {additionalInfo}
                </Text>
                {alertMessage === undefined ? undefined : (
                  <Alert variant={"info"}>
                    {alertMessage}
                    {alertMessage.includes("reported") ? (
                      <Link to={goToScope()}>{t("app.link")}</Link>
                    ) : undefined}
                  </Alert>
                )}
              </Fragment>
            );
          }}
        </InnerForm>
      </Form>
    </Modal>
  );
};

export { RemediationModal };
