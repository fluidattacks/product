import type { VulnerabilityStateReason } from "gql/graphql";

interface IRemediationValues {
  treatmentJustification: string;
  verificationReason?: VulnerabilityStateReason;
}

export type { IRemediationValues };
