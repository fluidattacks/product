import { useAbility } from "@casl/react";
import { Button } from "@fluidattacks/design";
import { StrictMode } from "react";
import { useTranslation } from "react-i18next";

import { authzPermissionsContext } from "context/authz/config";

interface IEditButtonProps {
  isAllSafe?: boolean;
  isDeleting?: boolean;
  isDisabled: boolean;
  isEditing: boolean;
  isFindingReleased?: boolean;
  isRequestingReattack: boolean;
  isResubmitting?: boolean;
  isVerifying?: boolean;
  onEdit: () => void;
}

const EditButton = ({
  isDeleting = false,
  isDisabled,
  isEditing,
  isFindingReleased = true,
  isRequestingReattack,
  isResubmitting = false,
  isVerifying = false,
  onEdit,
}: Readonly<IEditButtonProps>): JSX.Element => {
  const { t } = useTranslation();

  const permissions = useAbility(authzPermissionsContext);
  const canRequestZeroRiskVuln = permissions.can(
    "integrates_api_mutations_request_vulnerabilities_zero_risk_mutate",
  );
  const canUpdateVulnsTreatment = permissions.can(
    "integrates_api_mutations_update_vulnerabilities_treatment_mutate",
  );
  const shouldRenderEditBtn: boolean =
    isFindingReleased &&
    !(isRequestingReattack || isVerifying || isResubmitting || isDeleting) &&
    (canRequestZeroRiskVuln || canUpdateVulnsTreatment);

  return (
    <StrictMode>
      {shouldRenderEditBtn ? (
        <Button
          disabled={isRequestingReattack || isVerifying || isDisabled}
          icon={"edit"}
          id={"vulnerabilities-edit"}
          onClick={onEdit}
          variant={"ghost"}
        >
          {isEditing
            ? t("searchFindings.tabDescription.save.text")
            : t("searchFindings.tabVuln.buttons.edit")}
        </Button>
      ) : undefined}
    </StrictMode>
  );
};

export type { IEditButtonProps };
export { EditButton };
