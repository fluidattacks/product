import { Button } from "@fluidattacks/design";
import type { IconName } from "@fortawesome/free-solid-svg-icons";
import { Fragment, useCallback } from "react";
import * as React from "react";
import { useTranslation } from "react-i18next";

import type { IDeleteVulnerabilitiesButtonProps } from "./types";

import { Authorize } from "components/@core/authorize";

export const DeleteVulnerabilitiesButton: React.FC<
  IDeleteVulnerabilitiesButtonProps
> = ({
  areDeletableLocations,
  areVulnsSelected,
  isDeleting,
  isEditing,
  isRequestingReattack,
  isResubmitting = false,
  isVerifying = false,
  onCancel,
  onDeleting,
}): JSX.Element => {
  const { t } = useTranslation();

  const onDeleteVulnerabilities = useCallback((): void => {
    onDeleting?.();
  }, [onDeleting]);

  const shouldRenderBtn =
    areDeletableLocations &&
    !(isEditing || isRequestingReattack || isVerifying || isResubmitting);

  const buttonAttributes = useCallback((): {
    buttonIcon: IconName;
    buttonId: string;
    buttonText: string;
    buttonTooltip?: string;
  } => {
    if (isDeleting) {
      return {
        buttonIcon: "times",
        buttonId: "delete-cancel",
        buttonText: t("searchFindings.tabVuln.buttons.cancel"),
      };
    }

    return {
      buttonIcon: "trash-alt",
      buttonId: "delete",
      buttonText: t("searchFindings.tabVuln.buttons.delete.text"),
      buttonTooltip: t("searchFindings.tabVuln.buttonsTooltip.delete"),
    };
  }, [isDeleting, t]);

  const { buttonIcon, buttonId, buttonText, buttonTooltip } =
    buttonAttributes();

  return (
    <Authorize
      can={"integrates_api_mutations_remove_vulnerability_mutate"}
      have={"can_report_vulnerabilities"}
    >
      <Fragment>
        {isDeleting ? (
          <Button
            icon={"trash-alt"}
            id={"delete"}
            mr={0.5}
            onClick={onCancel}
            tooltip={t("searchFindings.tabVuln.buttons.delete.text")}
            variant={"ghost"}
          >
            {t("searchFindings.tabVuln.buttons.delete.text")}
          </Button>
        ) : undefined}
        {shouldRenderBtn ? (
          <Button
            disabled={!areVulnsSelected}
            icon={buttonIcon}
            id={buttonId}
            mr={0.5}
            onClick={onDeleteVulnerabilities}
            tooltip={buttonTooltip}
            variant={"ghost"}
          >
            {buttonText}
          </Button>
        ) : undefined}
      </Fragment>
    </Authorize>
  );
};
