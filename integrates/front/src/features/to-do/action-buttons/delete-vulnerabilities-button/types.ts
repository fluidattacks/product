export interface IDeleteVulnerabilitiesButtonProps {
  areDeletableLocations: boolean;
  areVulnsSelected: boolean;
  isDeleting: boolean;
  isEditing: boolean;
  isRequestingReattack: boolean;
  isResubmitting?: boolean;
  isVerifying?: boolean;
  onCancel?: () => void;
  onDeleting?: () => void;
}
