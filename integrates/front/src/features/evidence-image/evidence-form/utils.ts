import type { IFilePreviewProps } from "@fluidattacks/design";
import _ from "lodash";

const getFileExtension = (filename: string | undefined): string => {
  if (_.isNil(filename)) return "";
  const splittedName = filename.split(".");
  const extension =
    splittedName.length > 1 ? (_.last(splittedName) as string) : "";

  return extension.toLowerCase();
};

const hasExtension = (
  allowedExtensions: string[] | string,
  filename?: string,
): boolean => {
  if (!_.isUndefined(filename)) {
    return _.includes(allowedExtensions, getFileExtension(filename));
  }

  return false;
};

const filesTypeMapper: Record<string, IFilePreviewProps["fileType"]> = {
  jpg: "image",
  png: "image",
  webm: "video",
};

export { filesTypeMapper, getFileExtension, hasExtension };
