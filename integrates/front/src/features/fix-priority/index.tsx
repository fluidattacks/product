import { useQuery } from "@apollo/client";
import { useAbility } from "@casl/react";
import { Button, useModal } from "@fluidattacks/design";
import isEmpty from "lodash/isEmpty";
import { StrictMode, useCallback, useEffect, useMemo, useState } from "react";
import { useParams } from "react-router-dom";

import { PriorityFilters } from "./filters";
import { useAdditionalInfo } from "./hooks";
import { GET_VULNS_PRIORITY_RANKING } from "./queries";
import type {
  IPriorityRanking,
  IPriorityVulnerabilities,
  IVulnerabilityAttr,
} from "./types";

import { Table } from "components/table";
import { authzPermissionsContext } from "context/authz/config";
import { TreatmentModal } from "features/vulnerabilities/treatment-modal";
import type { IVulnRowAttr } from "features/vulnerabilities/types";
import { formatVulnerabilitiesTreatment } from "features/vulnerabilities/utils";
import { VulnerabilityModal } from "features/vulnerabilities/vulnerability-modal";
import { useTable } from "hooks";
import { useAudit } from "hooks/use-audit";
import { Logger } from "utils/logger";

const PriorityRanking = ({
  columns,
  groupName,
}: IPriorityRanking): JSX.Element => {
  const { addAuditEvent } = useAudit();
  const [treatment, setTreatment] = useState("");
  const [selectedVulns, setSelectedVulns] = useState<IVulnerabilityAttr[]>([]);
  const { vulnerabilityId: vulnId } = useParams() as {
    vulnerabilityId: string | undefined;
  };
  const [
    currentRow,
    setCurrentRow,
    openAdditionalInfoModal,
    closeAdditionalInfoModal,
  ] = useAdditionalInfo();
  const tableRef = useTable("tblVulnerabilitiesPriority");
  const permissions = useAbility(authzPermissionsContext);
  const modalProps = useModal("treatment-modal");

  const { data, loading, refetch } = useQuery<IPriorityVulnerabilities>(
    GET_VULNS_PRIORITY_RANKING,
    {
      fetchPolicy: "network-only",
      onCompleted: (): void => {
        if (groupName === undefined) {
          addAuditEvent("ToDo.Priority", "unknown");
        } else {
          addAuditEvent("Group.Priority", groupName);
        }
      },
      onError: ({ graphQLErrors }): void => {
        graphQLErrors.forEach((error): void => {
          Logger.error(
            "An error occurred loading vulnerabilities priority top 10",
            error,
          );
        });
      },
      variables: {
        groupName: groupName ?? "",
        hasGroupName: groupName !== undefined,
        treatment: isEmpty(treatment) ? null : treatment,
      },
    },
  );

  const { edges } =
    data === undefined ? { edges: [] } : data.me.vulnerabilitiesPriorityRanking;
  const totalOpenPriority = data?.group?.totalOpenPriority ?? 0;
  const unformattedVulns = useMemo((): IVulnerabilityAttr[] => {
    return [
      ...edges.map((edge: { node: IVulnerabilityAttr }): IVulnerabilityAttr => {
        return { ...edge.node, totalOpenPriority };
      }),
    ];
  }, [edges, totalOpenPriority]);
  const vulnerabilities = useMemo((): IVulnRowAttr[] => {
    return formatVulnerabilitiesTreatment({
      organizationsGroups: undefined,
      vulnerabilities: unformattedVulns,
    });
  }, [unformattedVulns]);

  const handleTreatment = useCallback((): void => {
    modalProps.open();
  }, [modalProps]);

  const handleClearSelected = useCallback((): void => {
    setSelectedVulns([]);
  }, []);

  useEffect((): void => {
    if (vulnId !== undefined) {
      setCurrentRow(
        vulnerabilities.find((vuln): boolean => vuln.id === vulnId),
      );
    }
  }, [setCurrentRow, vulnerabilities, vulnId]);

  return (
    <StrictMode>
      <Table
        columns={columns}
        data={unformattedVulns}
        extraButtons={
          <Button
            disabled={selectedVulns.length === 0}
            icon={"wrench"}
            onClick={handleTreatment}
            variant={"primary"}
          >
            {"Add treatment"}
          </Button>
        }
        filters={<PriorityFilters setTreatment={setTreatment} />}
        loadingData={loading}
        onRowClick={openAdditionalInfoModal}
        options={{ enableSearchBar: true }}
        rowSelectionSetter={
          permissions.can(
            "integrates_api_mutations_update_vulnerabilities_treatment_mutate",
          ) && Boolean(groupName)
            ? setSelectedVulns
            : undefined
        }
        rowSelectionState={selectedVulns}
        selectionMode={"radio"}
        tableRef={tableRef}
      />
      {currentRow ? (
        <VulnerabilityModal
          closeModal={closeAdditionalInfoModal}
          currentRow={currentRow}
          findingId={currentRow.findingId}
          groupName={currentRow.groupName}
          isFindingReleased={true}
          isModalOpen={true}
          refetchData={refetch}
        />
      ) : null}
      {selectedVulns.length > 0 ? (
        <TreatmentModal
          groupName={selectedVulns[0].groupName}
          handleClearSelected={handleClearSelected}
          modalProps={modalProps}
          refetchData={refetch}
          vulnerabilities={selectedVulns}
        />
      ) : undefined}
    </StrictMode>
  );
};

export { PriorityRanking };
