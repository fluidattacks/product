import { graphql } from "gql";

const GET_VULNS_PRIORITY_RANKING = graphql(`
  query GetVulnerabilitiesPriorityRanking(
    $canRetrieveHacker: Boolean! = false
    $groupName: String!
    $hasGroupName: Boolean!
    $treatment: VulnerabilityTreatment
  ) {
    group(groupName: $groupName) @include(if: $hasGroupName) {
      __typename
      name
      totalOpenPriority
    }
    me {
      __typename
      userEmail
      vulnerabilitiesPriorityRanking(
        groupName: $groupName
        treatment: $treatment
      ) {
        total
        edges {
          node {
            ...vulnFields
            finding {
              id
              organizationName
              title
            }
          }
        }
        pageInfo {
          endCursor
          hasNextPage
        }
      }
    }
  }
`);

export { GET_VULNS_PRIORITY_RANKING };
