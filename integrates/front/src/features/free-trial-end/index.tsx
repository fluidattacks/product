import { PopUp } from "@fluidattacks/design";
import { useTranslation } from "react-i18next";
import { useTheme } from "styled-components";

import type { IFreeTrialEnd } from "./types";

import { openUrl } from "utils/resource-helpers";

const FreeTrialEnd = ({ isOpen }: IFreeTrialEnd): JSX.Element | null => {
  const theme = useTheme();
  const { t } = useTranslation();

  if (isOpen) {
    return (
      <PopUp
        cancelButton={{
          key: "trialEndCancelBtn",
          onClick: (): void => {
            openUrl("mailto:help@fluidattacks.com");
          },
          text: t("autoenrollment.freeTrialEnd.secondButton"),
          variant: "primary",
        }}
        confirmButton={{
          key: "trialEndConfirmBtn",
          onClick: (): void => {
            openUrl("https://fluidattacks.com/plans/");
          },
          text: t("autoenrollment.freeTrialEnd.firstButton"),
          variant: "tertiary",
        }}
        container={null}
        darkBackground={true}
        description={t("autoenrollment.freeTrialEnd.description")}
        highlightDescription={[t("autoenrollment.freeTrialEnd.highlight")]}
        image={{
          alt: "freeTrialError",
          height: "110px",
          src: "integrates/resources/freeTrialEnd",
          width: "110px",
        }}
        maxWidth={"738px"}
        title={t("autoenrollment.freeTrialEnd.title")}
        titleColor={theme.palette.gray["800"]}
      />
    );
  }

  return null;
};

export { FreeTrialEnd };
