interface IFreeTrialEnd {
  isOpen: boolean;
}

export type { IFreeTrialEnd };
