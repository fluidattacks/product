import type { IGroupData } from "pages/organization/groups/types";

interface IGroupFilters {
  dataset: IGroupData[];
  setFilteredDataSet: React.Dispatch<React.SetStateAction<IGroupData[]>>;
}

export type { IGroupFilters };
