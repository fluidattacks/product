import type { DefaultTheme } from "styled-components";

type TSpacing = keyof DefaultTheme["spacing"];

interface IEvent {
  __typename: "Event";
  eventDate: string;
  eventStatus: string;
  groupName: string;
  id: string;
}

interface IEventBarProps {
  bgColor?: string;
  organizationName: string;
  pt?: TSpacing;
  pr?: TSpacing;
  pb?: TSpacing;
  pl?: TSpacing;
}

export type { IEvent, IEventBarProps };
