import { graphql } from "gql";

const GET_ORG_EVENTS = graphql(`
  query GetOrganizationEvents($organizationName: String!) {
    organizationId(organizationName: $organizationName) {
      name
      groups {
        name
        events {
          id
          eventDate
          eventStatus
          groupName
        }
      }
    }
  }
`);

export { GET_ORG_EVENTS };
