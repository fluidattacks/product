import { PureAbility } from "@casl/ability";
import { screen, waitFor } from "@testing-library/react";
import { userEvent } from "@testing-library/user-event";

import { Charts } from ".";
import { authzPermissionsContext } from "context/authz/config";
import { buildUrl } from "graphics/components/graphic/utils";
import { render } from "mocks";

describe("charts", (): void => {
  it("buildUrl", (): void => {
    expect.hasAssertions();

    expect(
      buildUrl(
        {
          documentName: "riskOverTimeCvssf",
          documentType: "stackedBarChart",
          entity: "group",
          generatorName: "generic",
          generatorType: "stackedBarChart",
          subject: "test",
        },
        {
          height: 680,
          width: 240,
        },
        "test",
        "riskOverTimeCvssf",
      ),
    ).toBe(
      [
        "http://localhost/graphic?documentName=riskOverTimeCvssf&documentType=",
        "stackedBarChart&entity=group&generatorName=generic&generatorType=",
        "stackedBarChart&height=680&subject=test&width=240",
      ].join(""),
    );
  });

  it("should render a component and number of graphics of entity", async (): Promise<void> => {
    expect.hasAssertions();

    const groupGraphics = 38;
    const organizationAndPortfolioGraphics = 40;

    const { container, rerender } = render(
      <authzPermissionsContext.Provider
        value={
          new PureAbility<string>([
            {
              action:
                "integrates_api_resolvers_organization_vulnerabilities_url_resolve",
            },
          ])
        }
      >
        <Charts
          bgChange={false}
          entity={"organization"}
          reportMode={false}
          subject={"subject"}
        />
      </authzPermissionsContext.Provider>,
    );

    await waitFor((): void => {
      expect(container.getElementsByClassName("frame")).toHaveLength(
        organizationAndPortfolioGraphics,
      );
    });

    expect(screen.queryByText("verifyDialog.title")).not.toBeInTheDocument();

    await waitFor((): void => {
      expect(
        screen.queryByText("analytics.sections.extras.vulnerabilitiesUrl.text"),
      ).toBeInTheDocument();
    });

    await userEvent.click(
      screen.getByText("analytics.sections.extras.vulnerabilitiesUrl.text"),
    );
    await waitFor((): void => {
      expect(screen.queryByText("verifyDialog.title")).toBeInTheDocument();
    });

    rerender(
      <Charts
        bgChange={false}
        entity={"group"}
        reportMode={false}
        subject={"subject"}
      />,
    );

    await waitFor((): void => {
      expect(container.getElementsByClassName("frame")).toHaveLength(
        groupGraphics,
      );
    });

    rerender(
      <Charts
        bgChange={true}
        entity={"portfolio"}
        reportMode={true}
        subject={"subject"}
      />,
    );
    await waitFor((): void => {
      expect(container.getElementsByClassName("frame")).toHaveLength(
        organizationAndPortfolioGraphics,
      );
    });
  });
});
