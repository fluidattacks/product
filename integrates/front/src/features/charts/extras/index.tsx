/* eslint-disable functional/immutable-data */
import { useLazyQuery } from "@apollo/client";
import { Button, Container } from "@fluidattacks/design";
import { Fragment, useCallback, useMemo, useState } from "react";
import * as React from "react";
import { useTranslation } from "react-i18next";
import { useTheme } from "styled-components";

import { GET_VULNERABILITIES_URL } from "../queries";
import type { IChartsProps } from "../types";
import { Authorize } from "components/@core/authorize";
import { SectionHeader } from "components/section-header";
import { VerifyDialog } from "features/verify-dialog";
import type { GetOrgVulnerabilitiesUrlQuery as TOrgVulnerabilitiesUrl } from "gql/graphql";
import { GET_NOTIFICATIONS } from "pages/dashboard/navbar/downloads/queries";
import { Logger } from "utils/logger";
import { msgError } from "utils/notifications";
import { openUrl } from "utils/resource-helpers";

const ChartsExtras: React.FC<IChartsProps> = ({
  entity,
  subject,
}): JSX.Element => {
  const { t } = useTranslation();
  const entityName = entity;
  const theme = useTheme();
  const [isVerifyDialogOpen, setIsVerifyDialogOpen] = useState(false);
  const downloadPngUrl = useMemo(
    (): URL => new URL("/graphics-report", window.location.origin),
    [],
  );
  downloadPngUrl.searchParams.set("entity", entity);
  downloadPngUrl.searchParams.set(entityName, subject);

  const [getUrl, { client }] = useLazyQuery(GET_VULNERABILITIES_URL, {
    onCompleted: (data: TOrgVulnerabilitiesUrl): void => {
      setIsVerifyDialogOpen(false);
      openUrl(data.organization.vulnerabilitiesUrl as string);
      void client.refetchQueries({ include: [GET_NOTIFICATIONS] });
    },
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error): void => {
        switch (error.message) {
          case "Exception - Stakeholder could not be verified":
            msgError(t("verifyDialog.alerts.nonSentVerificationCode"));
            break;
          case "Exception - The verification code is invalid":
            msgError(t("group.findings.report.alerts.invalidVerificationCode"));
            break;
          case "Exception - Document not found":
            msgError(t("analytics.sections.extras.vulnerabilitiesUrl.error"));
            break;
          case "Exception - The verification code is required":
            msgError(t("profile.mobileModal.alerts.requiredVerificationCode"));
            break;
          default:
            msgError(t("groupAlerts.errorTextsad"));
            Logger.error(
              "An error occurred getting vulnerabilities url for organization",
              error.message,
            );
        }
      });
    },
  });

  const getVulnerabilitiesUrl = useCallback(
    async (verificationCode: string): Promise<void> => {
      await getUrl({
        variables: {
          identifier: subject,
          verificationCode,
        },
      });
    },
    [getUrl, subject],
  );

  const openVerifyDialog = useCallback((): void => {
    setIsVerifyDialogOpen(true);
  }, []);

  const handleDownloadClick = useCallback((): void => {
    const downloadLink = document.createElement("a");
    downloadLink.href = downloadPngUrl.toString();
    downloadLink.download = `charts-${entity}-${subject}.png`;
    downloadLink.click();
  }, [downloadPngUrl, entity, subject]);

  return (
    <React.StrictMode>
      {entity === "organization" ? (
        <Fragment>
          <SectionHeader header={t("analytics.header")} />
          <Container
            alignItems={"center"}
            display={"flex"}
            gap={0.5}
            justify={"end"}
            px={1.25}
            py={1.25}
          >
            <Button icon={"file-arrow-down"} onClick={handleDownloadClick}>
              {t("analytics.sections.extras.download")}
            </Button>
            <Authorize
              can={
                "integrates_api_resolvers_organization_vulnerabilities_url_resolve"
              }
            >
              <Button
                icon={"file-csv"}
                onClick={openVerifyDialog}
                tooltip={t(
                  "analytics.sections.extras.vulnerabilitiesUrl.tooltip",
                )}
                variant={"secondary"}
              >
                {t("analytics.sections.extras.vulnerabilitiesUrl.text")}
              </Button>
            </Authorize>
          </Container>
          <VerifyDialog
            callbacks={{
              cancelCallback: (): void => {
                setIsVerifyDialogOpen(false);
              },
              verifyCallback: async (
                verificationCode: string,
              ): Promise<void> => {
                await getVulnerabilitiesUrl(verificationCode);
              },
            }}
            isOpen={isVerifyDialogOpen}
          />
        </Fragment>
      ) : (
        <Container
          bgColor={theme.palette.gray[50]}
          display={"flex"}
          justify={"end"}
          pb={1.5}
          width={"100%"}
          zIndex={10}
        >
          <Button icon={"file-download"} onClick={handleDownloadClick}>
            {t("analytics.sections.extras.download")}
          </Button>
        </Container>
      )}
    </React.StrictMode>
  );
};

export { ChartsExtras };
