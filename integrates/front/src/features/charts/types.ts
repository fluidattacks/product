type TEntity = "group" | "organization" | "portfolio";

interface IChartsProps {
  bgChange: boolean;
  entity: TEntity;
  reportMode: boolean;
  subject: string;
}

export type { TEntity, IChartsProps };
