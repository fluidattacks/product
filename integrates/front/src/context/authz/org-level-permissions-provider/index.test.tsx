import { screen, waitFor } from "@testing-library/react";
import { HttpResponse, type StrictResponse, graphql } from "msw";
import { Route, Routes } from "react-router-dom";

import { OrgLevelPermissionsProvider } from ".";
import type { GetOrganizationDataQuery } from "gql/graphql";
import { render } from "mocks";
import { LINK } from "mocks/constants";
import { GET_ORGANIZATION_DATA } from "pages/organization/queries";

describe("orgLevelPermissionsProvider", (): void => {
  const graphqlMocked = graphql.link(LINK);

  const baseQuery = {
    me: {
      tags: [
        {
          groups: [
            {
              name: "group1",
            },
            {
              name: "group2",
            },
          ],
          name: "test",
        },
      ],
      userEmail: "fserna@fluidattacks.com",
    },
    organizationId: {
      __typename: "Organization" as const,
      hasZtnaRoots: true,
      id: "ORG#test",
      name: "okada",
      permissions: [
        "integrates_api_resolvers_query_ztna_logs_report_url_resolve",
        "integrates_api_resolvers_organization_ztna_network_logs_resolve",
        "integrates_api_resolvers_organization_analytics_resolve",
        "integrates_api_mutations_add_organization_finding_policy_mutate",
        "integrates_api_resolvers_organization_ztna_http_logs_resolve",
        "integrates_api_resolvers_organization_ztna_session_logs_resolve",
        "see_ztna_logs",
        "integrates_api_mutations_submit_organization_finding_policy_mutate",
      ],
    },
  };
  const dataQuery = graphqlMocked.query(
    GET_ORGANIZATION_DATA,
    (): StrictResponse<{ data: GetOrganizationDataQuery }> => {
      return HttpResponse.json({ data: baseQuery });
    },
  );

  it("should render a component", async (): Promise<void> => {
    expect.hasAssertions();

    const aText = "This is a text of test";

    render(
      <Routes>
        <Route
          element={
            <OrgLevelPermissionsProvider>
              <p>{aText}</p>
            </OrgLevelPermissionsProvider>
          }
          path={"/orgs/:organizationName"}
        />
      </Routes>,
      {
        memoryRouter: {
          initialEntries: ["/orgs/okada"],
        },
        mocks: [dataQuery],
      },
    );

    await waitFor((): void => {
      expect(screen.getByText(aText)).toBeInTheDocument();
    });
  });
});
