import { useQuery } from "@apollo/client";
import isUndefined from "lodash/isUndefined";
import { useEffect } from "react";
import * as React from "react";
import { useLocation, useNavigate, useParams } from "react-router-dom";

import {
  authzPermissionsContext,
  organizationLevelPermissions,
} from "../config";
import { GET_ORGANIZATION_DATA } from "pages/organization/queries";
import { Logger } from "utils/logger";

const OrgLevelPermissionsProvider: React.FC<
  Readonly<{
    children: JSX.Element;
  }>
> = ({ children }): JSX.Element => {
  const { organizationName } = useParams() as { organizationName: string };
  const { pathname } = useLocation();
  const navigate = useNavigate();
  useEffect((): void => {
    if (
      !isUndefined(organizationName) &&
      organizationName !== organizationName.toLocaleLowerCase()
    ) {
      navigate(
        pathname.replace(
          organizationName,
          organizationName.toLocaleLowerCase(),
        ),
        { replace: true },
      );
    }
  }, [navigate, organizationName, pathname]);

  const { data } = useQuery(GET_ORGANIZATION_DATA, {
    fetchPolicy: "cache-first",
    onCompleted: ({ organizationId }): void => {
      if (organizationId.permissions) {
        organizationLevelPermissions.update(
          organizationId.permissions.map((permission): { action: string } => ({
            action: permission ?? "",
          })),
        );
      }
    },
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error): void => {
        Logger.error("Couldn't load organization data", error);
      });
    },
    variables: { organizationName: organizationName.toLowerCase() },
  });

  useEffect((): void => {
    if (organizationName !== data?.organizationId.name) {
      organizationLevelPermissions.update([]);
    }
  }, [organizationName, data?.organizationId.name]);

  if (data === undefined) {
    return <div />;
  }

  return (
    <authzPermissionsContext.Provider value={organizationLevelPermissions}>
      {children}
    </authzPermissionsContext.Provider>
  );
};

export { OrgLevelPermissionsProvider };
