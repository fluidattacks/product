import { screen } from "@testing-library/react";

import { Can } from "context/authz/can";
import {
  authzGroupContext,
  authzPermissionsContext,
  groupAttributes,
  userLevelPermissions,
} from "context/authz/config";
import { Have } from "context/authz/have";
import { render } from "mocks";

describe("authorization", (): void => {
  it("should render", (): void => {
    expect.hasAssertions();

    userLevelPermissions.update([{ action: "resolve_hacker" }]);
    render(
      <authzPermissionsContext.Provider value={userLevelPermissions}>
        <Can do={"resolve_hacker"}>
          <p>{"someone@fluidattacks.com"}</p>
        </Can>
      </authzPermissionsContext.Provider>,
    );

    expect(screen.queryByText("someone@fluidattacks.com")).toBeInTheDocument();
  });

  it("should render Have", (): void => {
    expect.hasAssertions();

    groupAttributes.update([{ action: "has_asm" }]);

    render(
      <authzGroupContext.Provider value={groupAttributes}>
        <Have I={"has_asm"}>
          <p>{"I have ASM"}</p>
        </Have>
      </authzGroupContext.Provider>,
    );

    expect(screen.queryByText("I have ASM")).toBeInTheDocument();
  });

  it("should not render", (): void => {
    expect.hasAssertions();

    userLevelPermissions.update([]);
    render(
      <authzPermissionsContext.Provider value={userLevelPermissions}>
        <Can do={"resolve_hacker"}>
          <p>{"someone@fluidattacks.com"}</p>
        </Can>
      </authzPermissionsContext.Provider>,
    );

    expect(
      screen.queryByText("someone@fluidattacks.com"),
    ).not.toBeInTheDocument();
  });

  it("should not render Have", (): void => {
    expect.hasAssertions();

    groupAttributes.update([]);

    render(
      <authzGroupContext.Provider value={groupAttributes}>
        <Have I={"has_asm"}>
          <p>{"I have ASM"}</p>
        </Have>
      </authzGroupContext.Provider>,
    );

    expect(screen.queryByText("I have ASM")).not.toBeInTheDocument();
  });
});
