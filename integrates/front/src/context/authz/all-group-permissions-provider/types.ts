import type { IGetUserOrganizationsGroups } from "@types";

interface IAllGroupContext {
  changePermissions?: (groupName: string) => void;
  onGroupChange?: (vulnerabilitiesGroupName: string[]) => void;
  userData?: IGetUserOrganizationsGroups;
}

export type { IAllGroupContext };
