import { screen, waitFor } from "@testing-library/react";
import { HttpResponse, type StrictResponse, graphql } from "msw";
import { Route, Routes } from "react-router-dom";

import { GroupLevelPermissionsProvider } from ".";
import { ManagedType } from "gql/graphql";
import type { GetGroupDataQueryQuery as GetGroupData } from "gql/graphql";
import { render } from "mocks";
import { LINK } from "mocks/constants";
import { GET_GROUP_DATA } from "pages/group/queries";

describe("groupLevelPermissionsProvider", (): void => {
  const graphqlMocked = graphql.link(LINK);

  const baseQuery = {
    group: {
      __typename: "Group" as const,
      managed: ManagedType.NotManaged,
      name: "test",
      organization: "okada",
      permissions: ["integrates_api_resolvers_group_consulting_resolve"],
      serviceAttributes: ["has_asm"],
    },
    organizationId: {
      __typename: "Organization" as const,
      id: "ORG#test",
      name: "okada",
    },
  };
  const dataQuery = graphqlMocked.query(
    GET_GROUP_DATA,
    (): StrictResponse<{ data: GetGroupData }> => {
      return HttpResponse.json({ data: baseQuery });
    },
  );

  it("should render a component", async (): Promise<void> => {
    expect.hasAssertions();

    const aText = "This is a text of test";

    render(
      <Routes>
        <Route
          element={
            <GroupLevelPermissionsProvider>
              <p>{aText}</p>
            </GroupLevelPermissionsProvider>
          }
          path={"/orgs/:organizationName/groups/:groupName/*"}
        />
      </Routes>,
      {
        memoryRouter: {
          initialEntries: ["/orgs/okada/groups/test/consulting"],
        },
        mocks: [dataQuery],
      },
    );

    await waitFor((): void => {
      expect(screen.getByText(aText)).toBeInTheDocument();
    });
  });
});
