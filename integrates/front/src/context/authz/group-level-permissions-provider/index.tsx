import { useQuery } from "@apollo/client";
import { isUndefined } from "lodash";
import { useEffect } from "react";
import * as React from "react";
import { useTranslation } from "react-i18next";
import {
  Navigate,
  useLocation,
  useNavigate,
  useParams,
} from "react-router-dom";

import {
  authzGroupContext,
  authzPermissionsContext,
  groupAttributes,
  groupLevelPermissions,
} from "../config";
import { GET_GROUP_DATA } from "pages/group/queries";
import { Logger } from "utils/logger";
import { msgError } from "utils/notifications";

const GroupLevelPermissionsProvider: React.FC<
  Readonly<{
    children: JSX.Element;
  }>
> = ({ children }): JSX.Element => {
  const { organizationName, groupName } = useParams() as {
    organizationName: string;
    groupName: string;
  };
  const { t } = useTranslation();
  const { pathname } = useLocation();
  const navigate = useNavigate();
  useEffect((): void => {
    if (
      !isUndefined(groupName) &&
      groupName !== groupName.toLocaleLowerCase()
    ) {
      navigate(pathname.replace(groupName, groupName.toLocaleLowerCase()), {
        replace: true,
      });
    }
  }, [navigate, groupName, pathname]);

  useEffect((): void => {
    groupAttributes.update([]);
    groupLevelPermissions.update([]);
  }, [groupName]);

  const { data, error } = useQuery(GET_GROUP_DATA, {
    onCompleted: ({ group }): void => {
      groupLevelPermissions.update(
        group.permissions.map((permission): { action: string } => ({
          action: permission,
        })),
      );
      groupAttributes.update(
        group.serviceAttributes.map((attribute): { action: string } => ({
          action: attribute,
        })),
      );
    },
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((groupError): void => {
        msgError(t("groupAlerts.errorTextsad"));
        Logger.warning("An error occurred group data", groupError);
      });
    },
    variables: { groupName, organizationName },
  });

  if (error !== undefined) {
    return <Navigate to={"/home"} />;
  }

  if (data === undefined) {
    return <div />;
  }

  if (organizationName !== data.group.organization) {
    return <Navigate to={"/home"} />;
  }

  return (
    <authzGroupContext.Provider value={groupAttributes}>
      <authzPermissionsContext.Provider value={groupLevelPermissions}>
        {children}
      </authzPermissionsContext.Provider>
    </authzGroupContext.Provider>
  );
};

export { GroupLevelPermissionsProvider };
