import { Container } from "@fluidattacks/design";
import { screen, waitFor } from "@testing-library/react";
import { userEvent } from "@testing-library/user-event";

import { RadioButton } from ".";
import { CustomThemeProvider } from "components/colors";
import { render } from "mocks";

describe("radioButton components", (): void => {
  it("should render", (): void => {
    expect.hasAssertions();

    const { container } = render(
      <CustomThemeProvider>
        <Container>
          {[0, 1, 2].map(
            (index): JSX.Element => (
              <RadioButton
                defaultChecked={index === 2}
                disabled={index === 0}
                key={`radio-button-${index}`}
                label={`radio-button-${index}`}
                name={`radio-button-${index}`}
                value={`radio-button-${index}`}
              />
            ),
          )}
        </Container>
      </CustomThemeProvider>,
    );

    expect(screen.getByText("radio-button-0")).toBeInTheDocument();
    expect(screen.getByText("radio-button-1")).toBeInTheDocument();
    expect(screen.getByText("radio-button-2")).toBeInTheDocument();
    expect(
      container.querySelector('input[name="radio-button-2"]'),
    ).toBeChecked();
    expect(
      container.querySelector('input[name="radio-button-0"]'),
    ).not.toBeChecked();
  });

  it("should be clickable", async (): Promise<void> => {
    expect.hasAssertions();

    const onChangeCallback: jest.Mock = jest.fn();
    const onClickCallback: jest.Mock = jest.fn();

    const { container } = render(
      <CustomThemeProvider>
        <Container>
          {[0, 1, 2].map(
            (index): JSX.Element => (
              <RadioButton
                defaultChecked={false}
                disabled={index === 0}
                key={`radio-button-${index}`}
                name={`radio-button-${index}`}
                onChange={onChangeCallback}
                onClick={onClickCallback}
                value={`radio-button-${index}`}
              />
            ),
          )}
        </Container>
      </CustomThemeProvider>,
    );

    const radioButton0 = container.querySelector(
      'input[name="radio-button-0"]',
    );
    const radioButton1 = container.querySelector(
      'input[name="radio-button-1"]',
    );
    const radioButton2 = container.querySelector(
      'input[name="radio-button-2"]',
    );

    if (radioButton0) await userEvent.click(radioButton0);
    if (radioButton1) await userEvent.click(radioButton1);
    if (radioButton2) await userEvent.click(radioButton2);

    expect(radioButton2).toBeChecked();
    expect(radioButton0).not.toBeChecked();

    await waitFor((): void => {
      expect(onChangeCallback).toHaveBeenCalledTimes(2);
    });
    await waitFor((): void => {
      expect(onClickCallback).toHaveBeenCalledTimes(2);
    });
  });
});
