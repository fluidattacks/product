import { Icon } from "@fluidattacks/design";
import type { RowData, Table } from "@tanstack/react-table";
import _ from "lodash";
import { useCallback, useMemo } from "react";

import { TableTags } from "../styles";
import { theme } from "components/colors";

interface ITagsProps<TData extends RowData> {
  readonly table: Table<TData>;
}

const Tags = <TData extends RowData>({
  table,
}: ITagsProps<TData>): JSX.Element | null => {
  const capitalize = (value: string): string => {
    return value.charAt(0).toUpperCase() + value.slice(1);
  };

  const setMethod = useCallback(
    (filters: Record<string, unknown>, key: string): VoidFunction => {
      const setter = `set${capitalize(key)}`;

      return (): void => {
        if (filters[setter] instanceof Function) {
          table.setPageIndex(0);
          // eslint-disable-next-line @typescript-eslint/no-unsafe-call
          filters[setter]();
        }
      };
    },
    [table],
  );

  const formatMethod = useCallback(
    (
      filters: Record<string, unknown>,
      key: string,
    ): ((value: string) => string) => {
      const getter = `format${capitalize(key)}`;

      return (value: string): string => {
        // eslint-disable-next-line @typescript-eslint/no-unsafe-return
        return filters[getter] instanceof Function
          ? // eslint-disable-next-line @typescript-eslint/no-unsafe-call
            filters[getter](value)
          : String(value);
      };
    },
    [],
  );

  const activeFilters = useMemo((): [
    string,
    string,
    (value: string) => string,
    VoidFunction,
  ][] => {
    const { filters } = table.options.meta ?? {};
    if (filters !== undefined) {
      const filtered = Object.entries(filters).filter(
        ([key, value]): boolean =>
          !_.isEmpty(value) &&
          !key.startsWith("set") &&
          !key.startsWith("reset") &&
          !key.startsWith("pagination") &&
          !key.startsWith("sort"),
      );

      const sanitized = filtered.map(
        ([key, value]): [
          string,
          string,
          (value: string) => string,
          VoidFunction,
        ] => [
          capitalize(key),
          Array.isArray(value) ? value.join(", ") : value,
          formatMethod(filters, key),
          setMethod(filters, key),
        ],
      );

      return sanitized;
    }

    return [];
  }, [table.options.meta, formatMethod, setMethod]);

  if (activeFilters.length === 0) {
    return null;
  }

  return (
    <TableTags>
      {activeFilters.map(
        ([key, value, format, clear]): JSX.Element => (
          <div key={key}>
            <Icon
              icon={"file-exclamation"}
              iconColor={theme.palette.gray[800]}
              iconSize={"xs"}
              iconType={"fa-light"}
              onClick={clear}
            />
            <span>
              {key} <b>{format(value)}</b>
            </span>
            <Icon
              icon={"xmark"}
              iconColor={theme.palette.gray[800]}
              iconSize={"xs"}
              iconType={"fa-light"}
              onClick={clear}
            />
          </div>
        ),
      )}
    </TableTags>
  );
};

export { Tags };
