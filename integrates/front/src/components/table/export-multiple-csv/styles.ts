import { keyframes, styled } from "styled-components";

const spin = keyframes`
  from {
    transform: rotate(0deg);
  }
  to {
    transform: rotate(360deg);
  }
`;

const ExportAnimation = styled.span`
  display: flex;
  justify-content: center;
  align-items: center;
  height: 20px;
  position: relative;
  width: 20px;

  ::before {
    content: "";
    position: absolute;
    width: 100%;
    height: 100%;
    top: 0;
    left: 0;
    border: 2px solid ${({ theme }): string => theme.palette.gray[800]};
    border-bottom: 2px solid transparent;
    border-radius: 50%;
    animation: ${spin} 2s linear infinite;
  }
`;

export { ExportAnimation };
