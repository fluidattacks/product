import { useLazyQuery } from "@apollo/client";
import type { RowData } from "@tanstack/react-table";
import { useCallback, useState } from "react";
import * as React from "react";
import { useTranslation } from "react-i18next";
import { useParams } from "react-router-dom";

import { REQUEST_GROUP_CSV_REPORT } from "./queries";

import { ExportMultipleCsv } from "components/table/export-multiple-csv";
import { Can } from "context/authz/can";
import { VerifyDialog } from "features/verify-dialog";
import type { TVerifyFn } from "features/verify-dialog/types";
import type { ReportType } from "gql/graphql";
import { GET_NOTIFICATIONS } from "pages/dashboard/navbar/downloads/queries";
import { Logger } from "utils/logger";
import { msgError, msgSuccess } from "utils/notifications";

interface IExportButtonProps<TData extends RowData> {
  data: TData[];
  fetchReportData?: () => Promise<void>;
  reportType: ReportType;
  size?: number;
}

const ExportButton = <TData extends RowData>({
  data,
  fetchReportData,
  reportType,
  size,
}: Readonly<IExportButtonProps<TData>>): JSX.Element => {
  const { groupName } = useParams() as { groupName: string };
  const { t } = useTranslation();
  const [isVerifyDialogOpen, setIsVerifyDialogOpen] = useState(false);

  const handleClose = useCallback((): void => {
    setIsVerifyDialogOpen(false);
  }, [setIsVerifyDialogOpen]);

  const [requestGroupReport, { client }] = useLazyQuery(
    REQUEST_GROUP_CSV_REPORT,
    {
      onCompleted: (): void => {
        handleClose();
        msgSuccess(
          t("groupAlerts.reportRequested"),
          t("groupAlerts.titleSuccess"),
        );
        void client.refetchQueries({ include: [GET_NOTIFICATIONS] });
      },
      onError: (errors): void => {
        errors.graphQLErrors.forEach((error): void => {
          switch (error.message) {
            case "Exception - The user already has a requested report for the same group":
              msgError(t("groupAlerts.reportAlreadyRequested"));
              break;
            case "Exception - Stakeholder could not be verified":
              msgError(
                t("group.findings.report.alerts.nonVerifiedStakeholder"),
              );
              break;
            case "Exception - The verification code is invalid":
              msgError(
                t("group.findings.report.alerts.invalidVerificationCode"),
              );
              break;
            default:
              msgError(t("groupAlerts.errorTextsad"));
              Logger.warning(
                "An error occurred requesting toe lines report",
                error,
              );
          }
        });
      },
    },
  );
  const onRequestReport = useCallback(
    (
      setVerifyCallbacks: TVerifyFn,
    ): ((event: React.MouseEvent<HTMLLIElement>) => void) => {
      return (event: React.MouseEvent<HTMLLIElement>): void => {
        event.stopPropagation();
        setVerifyCallbacks(
          async (verificationCode: string): Promise<void> => {
            await requestGroupReport({
              variables: { groupName, reportType, verificationCode },
            });
          },
          (): void => {
            setIsVerifyDialogOpen(false);
          },
        );
        setIsVerifyDialogOpen(true);
      };
    },
    [groupName, reportType, requestGroupReport, setIsVerifyDialogOpen],
  );

  return (
    <React.StrictMode>
      <Can do={"integrates_api_resolvers_query_csv_report_resolve"}>
        <VerifyDialog isOpen={isVerifyDialogOpen}>
          {(setVerifyCallbacks: TVerifyFn): JSX.Element => {
            return (
              <ExportMultipleCsv
                data={data}
                fetchReportData={fetchReportData}
                onBatchDownload={onRequestReport(setVerifyCallbacks)}
                size={size}
              />
            );
          }}
        </VerifyDialog>
      </Can>
    </React.StrictMode>
  );
};

export { ExportButton };
