import type { RowData } from "@tanstack/react-table";

interface IExportOptionsProps<TData extends RowData> {
  data: TData[];
  filteredData?: TData[];
  onBatchDownload?: (event: React.MouseEvent<HTMLLIElement>) => void;
  size?: number;
}

export type { IExportOptionsProps };
