import { Icon } from "@fluidattacks/design";
import * as React from "react";
import { styled } from "styled-components";

const DeleteFormatter = styled.button.attrs(
  ({
    className,
    type,
  }): Partial<React.ButtonHTMLAttributes<HTMLButtonElement>> => ({
    className: `b-sb bg-sb ${className ?? ""}`,
    type: type ?? "button",
  }),
)``;

export const deleteFormatter = <T extends object>(
  row: T,
  deleteFunction: (arg?: T) => void,
): JSX.Element => {
  function handleDeleteFormatter(
    event: React.FormEvent<HTMLButtonElement>,
  ): void {
    event.stopPropagation();
    deleteFunction(row);
  }

  return (
    <DeleteFormatter
      aria-label={"remove-row"}
      onClick={handleDeleteFormatter}
      type={"button"}
    >
      <Icon icon={"trash-alt"} iconSize={"xs"} iconType={"fa-light"} />
    </DeleteFormatter>
  );
};
