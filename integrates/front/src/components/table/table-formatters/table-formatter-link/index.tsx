/* eslint-disable react/jsx-no-bind */
import { Container, Icon, Link, Tooltip } from "@fluidattacks/design";
import _ from "lodash";
import * as React from "react";

export function formatLinkHandler(
  link: string,
  text: string,
  showInfo = false,
  tip?: string,
  showError = false,
  onClick?: () => void,
): JSX.Element {
  const handleClick = (event: React.MouseEvent<HTMLDivElement>): void => {
    if (onClick) {
      onClick();
    }
    event.stopPropagation();
  };

  const linkComponent = (
    // eslint-disable-next-line jsx-a11y/click-events-have-key-events
    <div onClick={handleClick} role={"button"} tabIndex={0}>
      <Link href={link}>{_.capitalize(text)}</Link>
    </div>
  );
  if (showInfo || showError) {
    return (
      <Container
        display={"flex"}
        flexDirection={showInfo ? "row" : "row-reverse"}
      >
        {linkComponent}
        &nbsp;
        <Tooltip
          display={"inline"}
          id={`${_.camelCase(text)}Tooltip`}
          place={"top"}
          tip={tip}
        >
          {showInfo ? (
            <sup>
              <Icon
                icon={"info-circle"}
                iconColor={"#5c5c70"}
                iconSize={"xs"}
              />
            </sup>
          ) : (
            <Icon
              icon={"circle-exclamation"}
              iconColor={"#f04438"}
              iconSize={"xs"}
              iconType={"fa-light"}
            />
          )}
        </Tooltip>
      </Container>
    );
  }

  return linkComponent;
}
