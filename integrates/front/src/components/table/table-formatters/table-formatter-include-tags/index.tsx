import { Container, LittleFlag, Text } from "@fluidattacks/design";

import { translate } from "utils/translations/translate";

export function includeTagsFormatter({
  dataPrivate = false,
  text,
  failedTag = false,
  newTag = false,
  reviewTag = false,
  decor = "none",
}: {
  dataPrivate?: boolean;
  text: string;
  newTag?: boolean;
  reviewTag?: boolean;
  failedTag?: boolean;
  decor?: string;
}): JSX.Element {
  return (
    <Container
      {...(dataPrivate && { "data-private": true })}
      display={"inline-block"}
    >
      <Container alignItems={"center"} display={"flex"}>
        <Text display={"inline-block"} size={"sm"}>
          {text}
        </Text>
        {failedTag ? (
          <LittleFlag bgColor={"#bf0b1a"} txtDecoration={decor}>
            {translate.t("table.formatters.includeTags.failed")}
          </LittleFlag>
        ) : undefined}
        {newTag ? (
          <LittleFlag>
            {translate.t("table.formatters.includeTags.new")}
          </LittleFlag>
        ) : undefined}
        {reviewTag ? (
          <LittleFlag bgColor={"#d88218"}>
            {translate.t("table.formatters.includeTags.review")}
          </LittleFlag>
        ) : undefined}
      </Container>
    </Container>
  );
}
