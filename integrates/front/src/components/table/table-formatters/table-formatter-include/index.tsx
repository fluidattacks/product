import { Alert } from "@fluidattacks/design";
import { useConfirmDialog } from "@fluidattacks/design";
import { Toggle } from "@fluidattacks/design";
import { Fragment, useCallback } from "react";
import { useTranslation } from "react-i18next";

import { Logger } from "utils/logger";
import { translate } from "utils/translations/translate";

interface IIncludeSwitchFormatterProps<T> {
  confirmAction: boolean;
  row: T;
  changeFunction: (row: T) => void;
}

function formatIncludeText(value: boolean): string {
  return value
    ? translate.t("table.formatters.include.include")
    : translate.t("table.formatters.include.exclude");
}

const IncludeSwitchFormatter = <T extends { include: boolean }>({
  row,
  changeFunction,
}: Readonly<IIncludeSwitchFormatterProps<T>>): JSX.Element => {
  const { t } = useTranslation();
  const { confirm, ConfirmDialog } = useConfirmDialog();

  const handleOnChange = useCallback((): void => {
    void confirm({
      message: row.include ? (
        <Alert>{t("table.formatters.include.confirm.message")}</Alert>
      ) : undefined,
      title: row.include
        ? t("table.formatters.include.confirm.exclusion")
        : t("table.formatters.include.confirm.inclusion"),
    })
      .then((result: boolean): void => {
        if (result) {
          changeFunction(row);
        }
      })
      .catch((): void => {
        Logger.error("An error occurred formatting");
      });
  }, [changeFunction, confirm, row, t]);

  return (
    <Fragment>
      <Toggle
        defaultChecked={row.include}
        leftDescription={{
          off: t("table.formatters.include.exclude"),
          on: t("table.formatters.include.include"),
        }}
        name={"includeToggle"}
        onChange={handleOnChange}
      />
      <ConfirmDialog />
    </Fragment>
  );
};

export const includeFormatter = <T extends { include: boolean }>(
  row: T,
  changeFunction: (row: T) => void,
  canEdit = false,
  confirmAction = false,
): JSX.Element | string =>
  canEdit ? (
    <IncludeSwitchFormatter<T>
      changeFunction={changeFunction}
      confirmAction={confirmAction}
      row={row}
    />
  ) : (
    formatIncludeText(row.include)
  );
