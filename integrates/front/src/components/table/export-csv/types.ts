import type { RowData } from "@tanstack/react-table";

import type { ICsvConfig } from "../types";

interface IHeader {
  key: string;
  label: string;
}

interface IExportCsvProps<TData extends RowData> {
  csvConfig?: ICsvConfig;
  data: TData[];
  children: React.ReactNode;
}

export type { IExportCsvProps, IHeader };
