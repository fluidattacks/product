/* eslint-disable new-cap */

import type { ApolloError } from "@apollo/client";
import type {
  ColumnDef,
  ColumnOrderState,
  ColumnPinningState,
  OnChangeFn,
  PaginationState,
  Row,
  RowSelectionState,
  SortingState,
  Table,
  VisibilityState,
} from "@tanstack/react-table";
import {
  createColumnHelper,
  getCoreRowModel,
  useReactTable,
} from "@tanstack/react-table";
import { useCallback, useMemo, useState } from "react";

import { Checkbox } from "components/checkbox";
import { useStoredState } from "hooks";
import type { TStore } from "utils/zustand";

type TQueryHook<T, V> = (variables: V) => {
  data: T[];
  total: number;
  loading: boolean;
  fetchMore: () => Promise<void>;
  error?: ApolloError;
};

interface ITableOptions<T> {
  /** The mode to use the table. */
  mode: "client-side" | "server-side";
  /** The variant of the table. */
  variant: "default" | "minimal";
  /** Additional methods and properties for actions in the table. */
  actionsOptions: Partial<{
    onSearch: (search: string) => void;
    openFiltersHandler: () => void;
  }>;
  /** Additional methods and properties for rows in the table. */
  rowOptions: Partial<{
    /** Enable multiple selection. */
    enableMultiSelection: boolean;
    /** Helper method to get the id. Useful for selection issues. */
    getId: (item: T) => string;
    /** Validator to enable row selection. */
    validator: (row: Row<T>) => boolean;
    /** Handler to execute when the first cell in the row is clicked. */
    onClick: (row: Row<T>) => void;
  }>;
  /** Additional methods and properties for columns in the table. */
  columnOptions: Partial<{
    /** The default order of the columns. */
    order: ColumnOrderState;
    /** The default pinning of the columns. */
    pinning: ColumnPinningState;
    /** The default visibility of the columns. */
    visibility: VisibilityState;
  }>;
}

interface IUseTable<T, F extends object, S extends string> {
  /** Unique table identifier */
  id: string;
  /**
   * The columns to show in the table. You can use a column helper to define
   * columns easily.
   * @example
   * const columns = [
   *   columnHelper.accessor("where", { header: "Location" }),
   *   columnHelper.accessor("specific", { header: "Specific" }),
   *   columnHelper.accessor("reportDate", { header: "Report Date", cell: ... }),
   * ];
   *
   * const { table } = useTable({ columns, ... });
   */
  columns: ColumnDef<T, string>[];
  /** Data to show in the table */
  data: T[];
  /** The total number of items found in the server */
  total: number;
  /**
   * The store hook to manage the filters.
   * @example
   * const useVulnFiltersStore = createFilterStore<Filter, Sort>(
   *   { state: ["VULNERABLE", "SAFE"], ... },
   *   (set): Partial<Store> => ({
   *     setState: (state): void => {
   *       set({ state });
   *     },
   *   }),
   * );
   *
   * const filters = useVulnFiltersStore();
   *
   * const { table } = useTable({ filters, ... });
   */
  filters: TStore<F, S>;
  /** The filters component from our design library */
  filtersComponent?: JSX.Element;
  /** The applied filters component from our design library */
  appliedFilters?: JSX.Element;
  /** The method to ask for more items. */
  fetchMore: () => Promise<void>;
  /** Additional options to customize the table hook */
  options: Partial<ITableOptions<T>>;
}

interface IUseTableResult<T> {
  table: Table<T>;
}

/**
 * ### useTable
 *
 * Hook to create a server-side table with pagination, sorting, and filtering.
 *
 * @template T - The type of the data to show in the table
 * @template F - The filters type
 * @template S - The sort enum
 */
const useTable = <T, F extends object, S extends string>({
  data,
  columns,
  id,
  total,
  appliedFilters,
  filters,
  filtersComponent,
  fetchMore,
  options,
}: IUseTable<T, F, S>): IUseTableResult<T> => {
  const {
    variant = "default",
    actionsOptions = {},
    rowOptions = {},
    columnOptions = {},
  } = options;

  const { onSearch, openFiltersHandler } = actionsOptions;

  const {
    getId = (item: T): string => (item as unknown as { id: string }).id,
    enableMultiSelection = false,
  } = rowOptions;

  const mappedData = useMemo(
    (): Map<string, T> =>
      new Map(data.map((item): [string, T] => [getId(item), item])),
    [data, getId],
  );

  const [rowSelection, setRowSelection] = useState<RowSelectionState>({});
  const [sorting, setSorting] = useState<SortingState>([]);
  const [, setColumnOrder] = useStoredState<ColumnOrderState>(
    `${id}-orderState`,
    columnOptions.order ?? [],
    localStorage,
  );
  const [, setColumnVisibility] = useStoredState<VisibilityState>(
    `${id}-visibilityState`,
    columnOptions.visibility ?? {},
    localStorage,
  );
  const storedOrder = localStorage.getItem(`${id}-orderState`);
  const storedVisibility = localStorage.getItem(`${id}-visibilityState`);

  const columnOrder =
    storedOrder === null
      ? columnOptions.order
      : (JSON.parse(storedOrder) as ColumnOrderState);
  const columnPinning = columnOptions.pinning ?? { left: [], right: [] };
  const columnVisibility =
    storedVisibility === null
      ? columnOptions.visibility
      : (JSON.parse(storedVisibility) as VisibilityState);

  const handlePagination = useCallback(
    (updater: (old: PaginationState) => PaginationState): void => {
      const movePage = async (): Promise<void> => {
        const pagination = {
          pageIndex: filters.pagination?.pageIndex as number,
          pageSize: filters.pagination?.pageSize as number,
        };
        const newValue = updater(pagination);
        const pageSizeChanged = pagination.pageSize !== newValue.pageSize;

        if (
          mappedData.size !== total &&
          mappedData.size < (newValue.pageIndex + 1) * newValue.pageSize &&
          !pageSizeChanged
        ) {
          await fetchMore();
        }

        if (pageSizeChanged) {
          filters.setPageIndex?.(0);
          filters.setPageSize?.(newValue.pageSize);
        } else {
          filters.setPageIndex?.(newValue.pageIndex);
          filters.setPageSize?.(newValue.pageSize);
        }
      };

      void movePage();
    },
    [filters, fetchMore, mappedData.size, total],
  );

  const handleSorting = useCallback(
    (updater: (old: SortingState) => SortingState): void => {
      const newSorting = updater(sorting);
      if (newSorting.length > 0) {
        const field = newSorting[0].id.toUpperCase() as `${S}`;
        const order = newSorting[0].desc ? "DESC" : "ASC";
        filters.setSortBy?.(field, order);
      } else {
        filters.resetSortBy?.();
      }
      setSorting(newSorting);
    },
    [sorting, setSorting, filters],
  );

  const addExtraColumns = useCallback(
    (cols: ColumnDef<T, string>[]): ColumnDef<T, string>[] => {
      const selectionColumn = createColumnHelper<T>().display({
        cell: ({ row }): React.ReactNode =>
          Checkbox({
            defaultChecked: row.getIsSelected(),
            disabled: !row.getCanSelect(),
            name: `select-${row.id}`,
            onChange: row.getToggleSelectedHandler(),
            variant: "inputOnly",
          }),
        enableResizing: false,
        enableSorting: false,
        header: ({ table }): React.ReactNode =>
          Checkbox({
            defaultChecked: table.getIsAllRowsSelected(),
            name: "select-all",
            onChange: table.getToggleAllPageRowsSelectedHandler(),
            variant: "inputOnly",
          }),
        id: "selection",
        maxSize: 18,
        minSize: 18,
        size: 18,
      });

      return [...(enableMultiSelection ? [selectionColumn] : []), ...cols];
    },
    [enableMultiSelection],
  );

  const paginate = useCallback(
    (fullData: Map<string, T>, fullPagination: PaginationState): T[] => {
      const { pageIndex: index, pageSize: items } = fullPagination;

      return [...fullData.values()].slice(index * items, (index + 1) * items);
    },
    [],
  );

  const table = useReactTable<T>({
    autoResetAll: false,
    columnResizeMode: "onChange",
    columns: addExtraColumns(columns),
    data: paginate(mappedData, {
      pageIndex: filters.pagination?.pageIndex as number,
      pageSize: filters.pagination?.pageSize as number,
    }),
    defaultColumn: {
      maxSize: 1000,
      minSize: 100,
    },
    enableColumnResizing: true,
    enableMultiRowSelection: enableMultiSelection,
    enableRowSelection: rowOptions.validator,
    enableSorting: true,
    getCoreRowModel: getCoreRowModel(),
    getRowId: getId,
    initialState: { columnOrder: columnOptions.order },
    manualFiltering: true,
    manualPagination: true,
    manualSorting: true,
    meta: {
      appliedFilters,
      data: mappedData,
      defaultColumnOrder: columnOptions.order,
      filters: filters as unknown as Record<string, string>,
      filtersComponent,
      onSearch,
      openFiltersHandler,
      variant,
    },
    onColumnOrderChange: setColumnOrder,
    onColumnVisibilityChange: setColumnVisibility,
    onPaginationChange: handlePagination as OnChangeFn<PaginationState>,
    onRowClick: rowOptions.onClick,
    onRowSelectionChange: setRowSelection,
    onSortingChange: handleSorting as OnChangeFn<SortingState>,
    rowCount: total,
    state: {
      columnOrder,
      columnPinning,
      columnVisibility,
      pagination: {
        pageIndex: filters.pagination?.pageIndex as number,
        pageSize: filters.pagination?.pageSize as number,
      },
      rowSelection,
      sorting,
    },
  });

  return { table };
};

export { useTable };
export type { TQueryHook };
