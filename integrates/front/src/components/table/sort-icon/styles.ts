import { styled } from "styled-components";

const StyledSortIcon = styled.span<{ $variant: "asc" | "desc" | false }>`
  transition: all 150ms;
  width: 5px;
  height: 10px;
  padding: ${({ theme }): string => theme.spacing[0.625]};
  display: flex;
  flex-direction: column;
  justify-content: center;
  gap: 0.125rem;
  align-items: center;

  svg {
    margin-top: -4px;
    margin-bottom: -2px;
  }

  span:first-child {
    transition: opacity 150ms;
    opacity: ${({ $variant }): number => ($variant === "desc" ? 0 : 1)};
  }

  span:last-child {
    transition: opacity 150ms;
    opacity: ${({ $variant }): number => ($variant === "asc" ? 0 : 1)};
  }
`;

export { StyledSortIcon };
