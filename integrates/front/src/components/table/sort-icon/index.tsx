import { Icon } from "@fluidattacks/design";

import { StyledSortIcon } from "./styles";

interface ISortIconProps {
  /** The variant of the sort icon. `false` for no sorting */
  variant: "asc" | "desc" | false;
}

/** Shows the sort icon arrows according its variants */
const SortIcon = (props: ISortIconProps): JSX.Element => {
  const { variant } = props;

  return (
    <StyledSortIcon $variant={variant}>
      <Icon
        icon={"caret-up"}
        iconColor={"#2f394b"}
        iconSize={"xxss"}
        iconType={"fa-solid"}
      />
      <Icon
        icon={"caret-down"}
        iconColor={"#2f394b"}
        iconSize={"xxss"}
        iconType={"fa-solid"}
      />
    </StyledSortIcon>
  );
};

export { SortIcon };
