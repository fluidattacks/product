import { Container, Icon } from "@fluidattacks/design";
import type { IconName } from "@fortawesome/free-solid-svg-icons";
import type { RowData, Table } from "@tanstack/react-table";
import { flexRender } from "@tanstack/react-table";
import isNil from "lodash/isNil";
import type { FormEvent } from "react";
import { Fragment, useCallback } from "react";

import { Checkbox } from "components/checkbox";
import type { ITableProps } from "components/table/types";

interface IHeadProps<TData extends RowData>
  extends Pick<
    ITableProps<TData>,
    "expandedRow" | "rowSelectionSetter" | "selectionMode"
  > {
  readonly table: Table<TData>;
}

const Head = <TData extends RowData>({
  expandedRow,
  rowSelectionSetter,
  selectionMode,
  table,
}: IHeadProps<TData>): JSX.Element => {
  const [headerGroup, hasSubHeaders] = table.getHeaderGroups();
  const group = isNil(hasSubHeaders) ? headerGroup : hasSubHeaders;

  function allRowExpansionHandler(): (event: FormEvent) => void {
    return (event: FormEvent): void => {
      event.stopPropagation();
      table.toggleAllRowsExpanded();
    };
  }

  const handleClick = useCallback((event: FormEvent): void => {
    event.stopPropagation();
  }, []);

  function allRowSelectionHandler(): (event: FormEvent) => void {
    return (event: FormEvent): void => {
      event.stopPropagation();
      if (table.getIsAllRowsSelected()) {
        table.resetRowSelection();
      } else {
        table.toggleAllRowsSelected();
      }
    };
  }

  return (
    <thead>
      <tr key={group.id}>
        {rowSelectionSetter !== undefined && (
          <th>
            {selectionMode === "checkbox" && (
              <Checkbox
                defaultChecked={table.getIsAllRowsSelected()}
                name={"table-head-row-checkbox"}
                onChange={allRowSelectionHandler()}
                onClick={handleClick}
                value={""}
                variant={"tableCheckbox"}
              />
            )}
          </th>
        )}
        {group.headers.map((header): JSX.Element => {
          return (
            <Fragment key={header.id}>
              <th
                className={
                  header.column.getCanSort() ? "pointer select-none" : ""
                }
                id={header.id}
              >
                <Container alignItems={"center"} display={"flex"}>
                  {header === group.headers[0] && expandedRow !== undefined && (
                    <div
                      onClick={allRowExpansionHandler()}
                      onKeyPress={allRowExpansionHandler()}
                      role={"button"}
                      tabIndex={0}
                    >
                      <Icon
                        icon={
                          table.getIsAllRowsExpanded()
                            ? "angle-up"
                            : "angle-down"
                        }
                        iconSize={"sm"}
                      />
                    </div>
                  )}
                  <Container
                    alignItems={"center"}
                    display={"flex"}
                    gap={0.25}
                    onClick={header.column.getToggleSortingHandler()}
                    pr={1}
                    whiteSpace={"nowrap"}
                  >
                    {flexRender(
                      header.column.columnDef.header,
                      header.getContext(),
                    )}
                    {header.column.getCanSort() ? (
                      <Icon
                        icon={
                          {
                            asc: "sort-up" as IconName,
                            desc: "sort-down" as IconName,
                          }[header.column.getIsSorted() as string] ??
                          ("sort" as IconName)
                        }
                        iconColor={"#b0b0bf"}
                        iconSize={"xs"}
                        iconType={"fa-solid"}
                      />
                    ) : undefined}
                  </Container>
                </Container>
              </th>
            </Fragment>
          );
        })}
      </tr>
    </thead>
  );
};

export { Head };
