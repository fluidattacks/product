import { screen } from "@testing-library/react";

import { TableControls } from ".";
import { render } from "mocks";

describe("tableControls", (): void => {
  it("renders export CSV button with correct translation", (): void => {
    expect.hasAssertions();

    const mockData = [{ id: 1, name: "Test" }];
    const mockCsvConfig = {
      export: true,
      filename: "test-export",
    };

    const { container } = render(
      <TableControls
        csvConfig={mockCsvConfig}
        data={mockData}
        extraButtons={<div data-testid={"extra-buttons"} />}
      />,
    );

    expect(
      screen.getByText("group.findings.exportCsv.text"),
    ).toBeInTheDocument();
    expect(screen.getByRole("button")).toHaveAttribute("id", "CSVExportBtn");
    expect(container).toBeInTheDocument();
  });

  it("renders extra buttons passed as props", (): void => {
    expect.hasAssertions();

    const mockData = [{ id: 1, name: "Test" }];
    const mockCsvConfig = {
      export: true,
      filename: "test-export",
    };
    const extraButton = <button data-testid={"extra-button"}>{"Extra"}</button>;

    render(
      <TableControls
        csvConfig={mockCsvConfig}
        data={mockData}
        extraButtons={extraButton}
      />,
    );

    expect(screen.getByTestId("extra-button")).toBeInTheDocument();
  });

  it("renders with empty data array", (): void => {
    expect.hasAssertions();

    const mockCsvConfig = {
      export: true,
      filename: "test-export",
    };

    const { container } = render(
      <TableControls csvConfig={mockCsvConfig} data={[]} extraButtons={null} />,
    );

    expect(container).toBeInTheDocument();
    expect(screen.getByRole("button")).toBeInTheDocument();
  });

  it("renders with null extraButtons", (): void => {
    expect.hasAssertions();

    const mockData = [{ id: 1, name: "Test" }];
    const mockCsvConfig = {
      export: true,
      filename: "test-export",
    };

    render(
      <TableControls
        csvConfig={mockCsvConfig}
        data={mockData}
        extraButtons={null}
      />,
    );

    expect(screen.queryByTestId("extra-buttons")).not.toBeInTheDocument();
    expect(screen.getByRole("button")).toBeInTheDocument();
  });
});
