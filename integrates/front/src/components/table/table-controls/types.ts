import type { RowData } from "@tanstack/react-table";

import type { ICsvConfig } from "../types";

interface ITableControlsProps<TData extends RowData> {
  csvConfig?: ICsvConfig;
  data: TData[];
  extraButtons: React.ReactNode;
}

export type { ITableControlsProps };
