import { Container, Text } from "@fluidattacks/design";
import type { RowData, Table } from "@tanstack/react-table";
import { useCallback, useEffect } from "react";
import { useTranslation } from "react-i18next";
import { useTheme } from "styled-components";

import { PaginationControl } from "./pagination-control";
import { PaginationDropdown } from "./pagination-dropdown";
import type { IPaginationDropdownItem } from "./pagination-dropdown/types";
import { PaginationSize } from "./pagination-size";

interface IPagination<TData extends RowData> {
  size: number;
  onNextPage: (() => Promise<void>) | undefined;
  hasNextPage: boolean;
  table: Table<TData>;
}

const Pagination = <TData extends RowData>({
  hasNextPage,
  size,
  onNextPage,
  table,
}: Readonly<IPagination<TData>>): JSX.Element | null => {
  const theme = useTheme();
  const { t } = useTranslation();

  const {
    pagination: { pageSize },
  } = table.getState();

  const SIZE_SMALL = 10;
  const SIZE_MEDIUM = 20;
  const SIZE_LARGE = 50;
  const SIZE_XLARGE = 100;

  const lastPage = Math.min(SIZE_XLARGE, size);

  const listSize = [SIZE_SMALL, SIZE_MEDIUM, SIZE_LARGE, lastPage].filter(
    (viewSize): boolean => viewSize <= size,
  );

  const handleClickSize = useCallback(
    (viewSize: number): (() => void) =>
      (): void => {
        table.setPageSize(viewSize);
      },
    [table],
  );

  const sizes = listSize.map((tableSize): IPaginationDropdownItem => {
    return {
      onClick: handleClickSize(tableSize),
      value: tableSize,
    };
  });

  useEffect((): void => {
    table.setPageIndex(0);
  }, [table]);

  return size > SIZE_SMALL ? (
    <Container
      alignItems={"center"}
      display={"flex"}
      justify={"space-between"}
      pb={3}
      pl={0}
      pr={0}
      pt={0.75}
    >
      <PaginationSize onNextPage={onNextPage} size={size} table={table} />
      <Container alignItems={"center"} display={"flex"} gap={0.25}>
        <Container alignItems={"center"} display={"flex"} gap={0.25}>
          <Text
            color={theme.palette.gray[600]}
            display={"inline-block"}
            size={"sm"}
          >
            {t("components.table.dropdownLabel")}
          </Text>
          <PaginationDropdown initial={pageSize} items={sizes} />
        </Container>
        <PaginationControl
          hasNextPage={hasNextPage}
          onNextPage={onNextPage}
          table={table}
        />
      </Container>
    </Container>
  ) : null;
};

export { Pagination };
