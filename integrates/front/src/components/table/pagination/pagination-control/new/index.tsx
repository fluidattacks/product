import { Container } from "@fluidattacks/design";
import type { RowData } from "@tanstack/react-table";
import { useCallback } from "react";

import type { IPaginationProps } from "../../../types";
import { ArrowButton } from "../styles";

const PaginationControl = <TData extends RowData>({
  table,
}: Readonly<IPaginationProps<TData>>): JSX.Element => {
  const {
    pagination: { pageIndex },
  } = table.getState();
  const pageCount = table.getPageCount();

  const goToNext = useCallback((): void => {
    table.setPageIndex(pageIndex + 1);
  }, [pageIndex, table]);

  const handlePreviousPage = useCallback((): void => {
    table.previousPage();
  }, [table]);

  return (
    <Container alignItems={"center"} display={"flex"} gap={0.5}>
      <ArrowButton
        $padding={`4px 6px`}
        disabled={!table.getCanPreviousPage()}
        icon={"chevron-left"}
        iconSize={"xxs"}
        id={"go-previous"}
        onClick={handlePreviousPage}
        variant={"ghost"}
      />
      <ArrowButton
        $padding={`4px 6px`}
        disabled={pageIndex > pageCount - 2}
        icon={"chevron-right"}
        iconSize={"xxs"}
        id={"go-next"}
        onClick={goToNext}
        variant={"ghost"}
      />
    </Container>
  );
};

export { PaginationControl };
