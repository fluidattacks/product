interface IPaginationDropdownItem {
  value: number;
  onClick: () => void;
}

interface IPaginationDropdown {
  items: IPaginationDropdownItem[];
  initial: number;
}

export type { IPaginationDropdown, IPaginationDropdownItem };
