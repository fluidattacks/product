import { Text } from "@fluidattacks/design";
import type { RowData } from "@tanstack/react-table";
import { useCallback } from "react";
import { useTheme } from "styled-components";

import type { IPaginationProps } from "../../../types";

const PaginationSize = <TData extends RowData>({
  table,
}: Readonly<IPaginationProps<TData>>): JSX.Element => {
  const theme = useTheme();
  const {
    pagination: { pageIndex, pageSize },
  } = table.getState();
  const size = table.getRowCount();

  const getCurrentRange = useCallback(
    (currentPage: number, limit: number, totalSize: number): string => {
      const fromRange = limit * currentPage + 1;
      const toRange = Math.min(limit * (currentPage + 1), totalSize);
      const currentRange = `${fromRange}-${toRange} of ${totalSize}`;

      return currentRange;
    },
    [],
  );

  return (
    <Text
      color={theme.palette.gray[600]}
      display={"inline-block"}
      fontWeight={"bold"}
      size={"sm"}
    >
      {getCurrentRange(pageIndex, pageSize, size)}
    </Text>
  );
};

export { PaginationSize };
