import { Container } from "@fluidattacks/design";
import type {
  Table as ReactTable,
  RowData,
  TableMeta,
} from "@tanstack/react-table";
import { useCallback, useEffect, useMemo } from "react";
import React, { useRef } from "react";

import { PaginationControl } from "../pagination/pagination-control/new";
import { PaginationDropdown } from "../pagination/pagination-dropdown/new";
import { PaginationSize } from "../pagination/pagination-size/new";
import { TableOptions } from "../table-options";
import { Tags } from "../table-tags";
import {
  NewTableContainer,
  TableFooter,
  TableStatus,
} from "components/table/styles";
import { Body } from "components/table/table-body/new";
import { Head } from "components/table/table-head/new";

interface ITableProps<TData extends RowData> {
  id: string;
  table: ReactTable<TData>;
  loading?: boolean;
  highlightedContent?: React.ReactNode;
  actionsContent?: React.ReactNode;
  footerContent?: React.ReactNode;
  groupHeaders?: boolean;
}

const STATUS_OFFSET = 160;

const Table = <T extends RowData>({
  id,
  loading = false,
  table,
  actionsContent = undefined,
  highlightedContent = undefined,
  footerContent = undefined,
  groupHeaders = false,
}: Readonly<ITableProps<T>>): React.ReactNode => {
  const { appliedFilters } = useMemo(
    (): TableMeta<T> => table.options.meta ?? {},
    [table.options.meta],
  );
  /** Ref to get current table size */
  const tableContainerRef = useRef<HTMLDivElement>(null);

  const totalSelected = Object.keys(table.getState().rowSelection).length;
  const elementName = totalSelected === 1 ? "item" : "items";
  const hidden = totalSelected === 0;

  const tableSize = Math.max(
    tableContainerRef.current?.clientWidth ?? 0,
    table.getTotalSize() + STATUS_OFFSET,
  );

  const handleUnselectAll = useCallback((): void => {
    table.resetRowSelection();
  }, [table]);

  const handleKeyDown = useCallback(
    (event: React.KeyboardEvent): void => {
      if (event.key === "Enter") {
        handleUnselectAll();
      }
    },
    [handleUnselectAll],
  );

  useEffect((): void => {
    table.setPageIndex(0);
  }, [table]);

  return (
    <NewTableContainer id={id}>
      <Container display={"flex"} flexDirection={"column"} gap={1} mb={1}>
        <TableOptions
          actionsContent={actionsContent}
          highlightedContent={highlightedContent}
          table={table}
        />
        {appliedFilters ?? <Tags table={table} />}
      </Container>
      <div ref={tableContainerRef}>
        {hidden ? undefined : (
          <TableStatus $width={tableSize}>
            <div>
              <b>{`${totalSelected} ${elementName} `}</b>
              {"have been selected. "}
              <span
                onClick={handleUnselectAll}
                onKeyDown={handleKeyDown}
                role={"link"}
                tabIndex={0}
              >
                {"Unselect all"}
              </span>
            </div>
          </TableStatus>
        )}
        <table>
          <Head
            containerRef={tableContainerRef}
            groupHeaders={groupHeaders}
            table={table}
          />
          <Body loading={loading} table={table} />
        </table>
      </div>
      <TableFooter>
        <div className={"pagination-container"}>
          <PaginationSize table={table} />
          <PaginationDropdown table={table} />
          <PaginationControl table={table} />
        </div>
        {footerContent === undefined ? undefined : (
          <div className={"footer-container"}>{footerContent}</div>
        )}
      </TableFooter>
    </NewTableContainer>
  );
};

export { Table };
