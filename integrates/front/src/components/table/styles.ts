import { styled } from "styled-components";

interface ITableContainerProps {
  $clickable: boolean;
  $rowSelection: boolean;
}

interface ISizeable {
  $width?: number;
}

const TableContainer = styled.div<ITableContainerProps>`
  background-color: ${({ theme }): string => theme.palette.white};
  border: solid 1px ${({ theme }): string => theme.palette.gray[200]};
  border-radius: 4px;
  color: ${({ theme }): string => theme.palette.gray[800]};
  font-family: Roboto, sans-serif;
  font-size: 14px;
  text-align: left;
  overflow: auto hidden;

  table {
    border-collapse: collapse;
    table-layout: auto;
    width: 100%;
  }

  label {
    display: flex;
    align-items: center;
    margin: unset;
  }

  td {
    height: 40px;
    white-space: pre-line;
    padding-left: 10px;
  }

  tr {
    border-bottom: solid 1px ${({ theme }): string => theme.palette.gray[200]};
  }

  td.clickable * {
    cursor: pointer;

    &:hover:not(span) {
      color: rgb(12 17 29 / 60%) !important;
      transition: color 100ms;
    }
  }

  td > label {
    cursor: ${({ $clickable }): string => ($clickable ? "pointer" : "unset")};
  }

  table tbody tr:not(.no-data-row):hover {
    background-color: ${({ theme }): string => theme.palette.gray[50]};
  }

  th svg {
    font-size: 12px;
  }

  th svg[class*="angle"] {
    font-size: 12px;
    padding-right: 10px;
  }

  th {
    background-color: ${({ theme }): string => theme.palette.gray[200]};
    font-weight: 700;
    color: ${({ theme }): string => theme.palette.gray[600]};
    padding: unset;
    height: 40px;
    white-space: pre-line;
    padding-left: 10px;
  }

  table th:first-child {
    border-right: 1px solid ${({ theme }): string => theme.palette.gray[25]};
    width: ${({ $rowSelection }): string => ($rowSelection ? "40px" : "auto")};
    min-width: ${({ $rowSelection }): string =>
      $rowSelection ? "40px" : "auto"};
    max-width: ${({ $rowSelection }): string =>
      $rowSelection ? "40px" : "auto"};
  }

  table td:first-child {
    width: ${({ $rowSelection }): string => ($rowSelection ? "40px" : "auto")};
    min-width: ${({ $rowSelection }): string =>
      $rowSelection ? "40px" : "auto"};
    max-width: ${({ $rowSelection }): string =>
      $rowSelection ? "40px" : "auto"};
  }

  table tr td:first-child {
    border-right: 1px solid ${({ theme }): string => theme.palette.gray[200]};
  }

  &::-webkit-scrollbar {
    width: 8px;
    height: 8px;
  }

  &::-webkit-scrollbar-track {
    background: ${({ theme }): string => theme.palette.gray[100]} !important;
    border-radius: 50px;
  }

  &::-webkit-scrollbar-thumb {
    background: ${({ theme }): string => theme.palette.gray[600]} !important;
    border-radius: 50px;
  }
`;

const NewTableContainer = styled.div`
  position: relative;

  div:has(table) {
    scrollbar-color: ${({ theme }): string => theme.palette.gray[600]}
      transparent;
    scrollbar-width: thin;
    overflow-x: auto;
    margin-left: -3px;
    padding-left: 3px;
    padding-bottom: 5px;
  }

  table {
    display: block;
    overflow-x: hidden;
    width: max-content;
    white-space: nowrap;
    margin: 0;
    border-radius: 8px;
    border: 1px solid ${({ theme }): string => theme.palette.gray[200]};
    border-collapse: collapse;
  }

  table thead {
    background-color: ${({ theme }): string => theme.palette.gray[50]};
    position: sticky;
    z-index: 1;
    top: 0;
  }

  table tbody {
    background-color: ${({ theme }): string => theme.palette.white};
  }

  table tr {
    height: ${({ theme }): string => theme.spacing[3]};
    border-bottom: solid 1px ${({ theme }): string => theme.palette.gray[200]};
  }

  table thead tr {
    background-color: transparent;
    border-bottom: none;
  }

  table tr:hover {
    background-color: ${({ theme }): string => theme.palette.gray[50]};
  }

  table thead tr:hover {
    background-color: transparent;
  }

  table tbody tr {
    &:last-child {
      border-bottom: none;
    }

    &:has(input[disabled]) {
      background-color: ${({ theme }): string => theme.palette.gray[100]};
    }

    &:has(input[disabled]):hover {
      background-color: ${({ theme }): string => theme.palette.gray[100]};
    }
  }

  table tbody tr.row-no-data {
    &:hover {
      background-color: ${({ theme }): string => theme.palette.white};
    }
  }

  thead th {
    position: relative;
    font-weight: 700;
    color: ${({ theme }): string => theme.palette.gray[800]};
    padding: unset;
    height: ${({ theme }): string => theme.spacing[3]};
    white-space: pre-line;

    & div:first-child {
      background-color: ${({ theme }): string => theme.palette.gray[200]};
    }

    &:hover div:first-child {
      background-color: ${({ theme }): string => theme.palette.gray[300]};
    }

    &:first-child {
      border-right: 1px solid ${({ theme }): string => theme.palette.gray[25]};

      svg {
        margin-right: unset;
      }

      &:has(input) {
        padding: unset;
      }

      &:has(input) + th:nth-child(2) {
        border-right: 1px solid ${({ theme }): string => theme.palette.gray[25]};
      }

      &:hover div:first-child {
        background-color: ${({ theme }): string => theme.palette.gray[200]};
      }
    }
  }

  tbody tr:not(.row-no-data) td {
    white-space: pre-line;
    padding-left: 10px;

    &:first-child {
      cursor: pointer;
      border-right: 1px solid ${({ theme }): string => theme.palette.gray[200]};

      svg {
        margin-right: unset;
      }

      &:hover p {
        text-decoration: underline;
        color: ${({ theme }): string => theme.palette.gray[400]};
      }

      &:has(input) {
        padding: ${({ theme }): string => theme.spacing[0.5]};

        & + td {
          cursor: pointer;
          border-right: 1px solid
            ${({ theme }): string => theme.palette.gray[200]};

          &:hover p {
            text-decoration: underline;
            color: ${({ theme }): string => theme.palette.gray[400]};
          }
        }
      }
    }
  }
`;

const TableTags = styled.div`
  display: flex;
  flex-wrap: wrap;
  gap: ${({ theme }): string => theme.spacing[0.625]};

  div {
    display: flex;
    align-items: center;
    gap: ${({ theme }): string => theme.spacing[0.5]};
    border-radius: 4px;
    font-size: ${({ theme }): string => theme.typography.text.xs};
    padding: ${({ theme }): string =>
      `${theme.spacing[0.125]} ${theme.spacing[0.5]}`};
    color: ${({ theme }): string => theme.palette.gray[600]};
    background-color: ${({ theme }): string => theme.palette.gray[200]};
  }
`;

const TableStatus = styled.div<ISizeable>`
  display: flex;
  flex-direction: column;
  align-items: stretch;
  flex: 1 0 auto;
  padding-right: 5px;
  width: ${({ $width }): string => `${$width}px`};

  div {
    position: relative;
    font-weight: 400;
    font-size: ${({ theme }): string => theme.typography.text.sm};
    text-align: left;

    color: ${({ theme }): string => theme.palette.gray[600]};
    background-color: ${({ theme }): string => theme.palette.gray[200]};
    border-radius: 4px;
    padding: ${({ theme }): string => theme.spacing[0.625]};
    margin-top: ${({ theme }): string => theme.spacing[0.5]};
    margin-bottom: ${({ theme }): string => theme.spacing[0.5]};
    height: ${({ theme }): string => theme.spacing[2.25]};

    span {
      text-decoration: underline;
      cursor: pointer;
    }
  }
`;

const TableResizer = styled.div`
  transition: all 150ms;
  content: "";
  z-index: 2;
  position: absolute;
  bottom: 0;
  right: -1px;
  width: 30px;
  height: 100%;
  margin-right: 0;
  cursor: col-resize;
  user-select: none;
  touch-action: none;
  border-right: 1px solid transparent;

  &:hover {
    transition: all 150ms;
    border-right: 1px solid ${({ theme }): string => theme.palette.black};
  }
`;

const SearchContainer = styled.div.attrs({ className: "search-container" })`
  display: flex;
  flex-direction: column;
  justify-content: center;
  flex: 1 0 320px;

  > div {
    align-items: center;
    background-color: ${({ theme }): string => theme.palette.white};
    border: 1px solid;
    border-color: ${({ theme }): string => theme.palette.gray[300]};
    border-radius: ${({ theme }): string => theme.spacing[0.5]};
    display: flex;
    height: 40px;
    padding: ${({ theme }): string => theme.spacing[0.5]};
    max-width: 100%;
    width: 100%;

    &:hover {
      border-color: ${({ theme }): string => theme.palette.gray[600]};
    }

    &:focus-within {
      border: 2px solid;
      border-color: ${({ theme }): string => theme.palette.black};
    }

    span {
      margin-top: -${({ theme }): string => theme.spacing[0.125]};
      margin-right: ${({ theme }): string => theme.spacing[0.5]};
    }

    input {
      background: none;
      border: none !important;
      box-shadow: none;
      box-sizing: border-box;
      color: ${({ theme }): string => theme.palette.gray[800]};
      font-family: ${({ theme }): string => theme.typography.type.primary};
      font-size: ${({ theme }): string => theme.typography.text.sm};
      outline: none;
      width: 100%;
      line-height: ${({ theme }): string => theme.spacing[0.75]};
    }
  }
`;

const ControlsContainer = styled.div.attrs({ className: "controls-container" })`
  display: flex;
  flex: 1 0 50%;

  .table-controls-container {
    display: flex;
    align-items: center;
    gap: ${({ theme }): string => theme.spacing[0.125]};

    button {
      height: 40px;
      padding-inline: ${({ theme }): string => theme.spacing[0.5]};

      p {
        background-color: ${({ theme }): string => theme.palette.gray[200]};
        padding-inline: ${({ theme }): string => theme.spacing[0.25]};
        font-size: ${({ theme }): string => theme.typography.text.xs};
        border-radius: 4px;
      }
    }

    &::after {
      display: block;
      content: "";
      height: ${({ theme }): string => theme.typography.text.md};
      border-left: 1px solid ${({ theme }): string => theme.palette.gray[300]};
      margin: 0 ${({ theme }): string => theme.spacing[0.125]};
    }
  }

  .actions-container {
    display: flex;
    align-items: center;
    flex: 1 0 auto;
  }
`;

const CellContainer = styled.div<{ $width: string }>`
  align-items: center;
  display: flex;
  overflow-wrap: anywhere;
  width: ${({ $width }): string => $width}px;
`;

const TableFooter = styled.div`
  align-items: center;
  background-color: ${({ theme }): string => theme.palette.gray[50]};
  display: flex;
  gap: ${({ theme }): string => theme.spacing[1]};
  padding: ${({ theme }): string =>
    `${theme.spacing[0.5]} ${theme.spacing[1]} ${theme.spacing[1.5]} ${theme.spacing[0]}`};

  .pagination-container {
    display: flex;
    flex-direction: row;
    align-items: center;
    gap: ${({ theme }): string => theme.spacing[1]};
  }

  .footer-container {
    display: flex;
    align-items: center;
    flex: 1 0 auto;
    height: 100%;

    &::before {
      display: block;
      content: "";
      height: ${({ theme }): string => theme.typography.text.md};
      border-left: 1px solid ${({ theme }): string => theme.palette.gray[300]};
      margin: 0;
      margin-right: ${({ theme }): string => theme.spacing[0.5]};
    }
  }
`;

const TableOptionsContainer = styled.div`
  display: flex;
  flex-wrap: wrap;
  width: 100%;
  justify-content: space-between;
  gap: ${({ theme }): string => theme.spacing[1]};

  .highlight-container {
    flex: 1 0 auto;
  }

  .highlight-container ~ .controls-container {
    flex: 1 0 100%;
  }
`;

export {
  TableContainer,
  TableFooter,
  NewTableContainer,
  TableTags,
  TableStatus,
  TableResizer,
  SearchContainer,
  CellContainer,
  ControlsContainer,
  TableOptionsContainer,
};
