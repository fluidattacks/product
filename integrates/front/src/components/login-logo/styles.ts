import { styled } from "styled-components";

const LogoWrapper = styled.div`
  width: 230px;
`;

export { LogoWrapper };
