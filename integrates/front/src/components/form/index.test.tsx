import { screen, waitFor } from "@testing-library/react";
import { userEvent } from "@testing-library/user-event";
import { mixed, object, string } from "yup";

import { Form, InnerForm } from ".";
import { Checkbox } from "components/checkbox";
import { CustomThemeProvider } from "components/colors";
import {
  Input,
  InputFile,
  InputNumber,
  Select,
  TextArea,
} from "components/input";
import { RadioButton } from "components/radio-button";
import { render } from "mocks";

describe("form Component", (): void => {
  it("should not submit when validations failed", async (): Promise<void> => {
    expect.hasAssertions();

    const onSubmitMock = jest.fn();
    const onClickCancel = jest.fn();

    render(
      <CustomThemeProvider>
        <Form
          initialValues={{
            date: "",
            file: "",
            lastReport: "2",
            location: "Initial Location",
          }}
          onSubmit={onSubmitMock}
          subtitle={"Subtitle"}
          title={"Test Form"}
          validationSchema={object({
            file: mixed().required("A file is required"),
            location: string().required("Location Required"),
          })}
          width={"400px"}
        >
          {(props): JSX.Element => (
            <InnerForm {...props} onCancel={onClickCancel}>
              <Input
                label={"Location"}
                name={"location"}
                placeholder={"Location file"}
                suggestions={["test", "suggestion"]}
                tooltip={"Location"}
              />
              <InputNumber
                disabled={true}
                label={"Last Report (days)"}
                max={10}
                min={0}
                name={"lastReport"}
                required={true}
              />
              <InputFile
                accept={"image/*"}
                id={"Evidence"}
                label={"File upload label"}
                name={"file"}
                tooltip={"Add File"}
              />
            </InnerForm>
          )}
        </Form>
      </CustomThemeProvider>,
    );

    const user = userEvent.setup();
    await user.type(
      screen.getByPlaceholderText("Location file"),
      "Location test",
    );

    await user.click(screen.getByText("buttons.confirm"));

    expect(screen.getByText("A file is required")).toBeInTheDocument();
    expect(screen.queryByText("Location Required")).not.toBeInTheDocument();

    await waitFor((): void => {
      expect(onSubmitMock).not.toHaveBeenCalled();
    });

    await waitFor((): void => {
      expect(onClickCancel).not.toHaveBeenCalled();
    });
    jest.clearAllMocks();
  });

  it("call cancel callback when cancel button clicked", async (): Promise<void> => {
    expect.hasAssertions();

    const onSubmitMock = jest.fn();
    const onClickCancel = jest.fn();

    render(
      <CustomThemeProvider>
        <Form
          initialValues={{ location: "" }}
          onSubmit={onSubmitMock}
          subtitle={"Subtitle"}
          title={"Test Form"}
          validationSchema={object({
            location: string().required("Location Required"),
          })}
          width={"400px"}
        >
          {(props): JSX.Element => (
            <InnerForm {...props} onCancel={onClickCancel}>
              <Input
                label={"Location"}
                name={"location"}
                placeholder={"Location file"}
                suggestions={["test", "suggestion"]}
                tooltip={"Location"}
              />
            </InnerForm>
          )}
        </Form>
      </CustomThemeProvider>,
    );

    const user = userEvent.setup();

    const locationInput = screen.getByPlaceholderText("Location file");
    await userEvent.type(locationInput, "A Location");

    expect(locationInput).toHaveValue("A Location");

    await user.click(screen.getByText("buttons.cancel"));

    expect(locationInput).toHaveValue("");

    await waitFor((): void => {
      expect(onClickCancel).toHaveBeenCalledTimes(1);
    });
    await waitFor((): void => {
      expect(onSubmitMock).not.toHaveBeenCalled();
    });
    jest.clearAllMocks();
  });

  it("should submit and set values properly", async (): Promise<void> => {
    expect.hasAssertions();

    const onSubmitMock = jest.fn();
    const onClickCancel = jest.fn();

    render(
      <CustomThemeProvider>
        <Form
          initialValues={{
            checkbox: "",
            inputNumber: "",
            lastName: "Doe",
            name: "",
            radioSelection: "",
            selection: "",
            textarea: "",
          }}
          onSubmit={onSubmitMock}
          subtitle={"Subtitle"}
          title={"Test Form"}
          width={"400px"}
        >
          {(props): JSX.Element => (
            <InnerForm {...props} onCancel={onClickCancel}>
              <Input label={"Name"} name={"name"} placeholder={"Name"} />
              <Input
                label={"Last Name"}
                name={"lastName"}
                placeholder={"Last Name"}
              />
              <InputNumber
                label={"Input Number"}
                max={10}
                min={0}
                name={"inputNumber"}
                placeholder={"Number"}
              />
              <Select
                items={[
                  { value: "Option 1" },
                  { value: "Option 2" },
                  { value: "Option 3" },
                ]}
                label={"Select text label"}
                name={"selection"}
                required={true}
              />
              <RadioButton
                label={"Yes"}
                name={"radioSelection"}
                value={"Yes"}
                variant={"formikField"}
              />
              <Checkbox
                name={"checkbox"}
                value={"Accept"}
                variant={"formikField"}
              />
              <TextArea
                maxLength={1000}
                name={"textarea"}
                placeholder={"Enter a description"}
              />
            </InnerForm>
          )}
        </Form>
      </CustomThemeProvider>,
    );

    const user = userEvent.setup();

    await userEvent.type(screen.getByPlaceholderText("Name"), "John");
    await userEvent.type(screen.getByPlaceholderText("Number"), "3");

    await user.click(screen.getByText("Option 1"));
    await user.click(screen.getByText("Option 2"));

    await user.click(screen.getByLabelText("Yes"));
    await user.click(screen.getByLabelText("Accept"));
    await user.type(
      screen.getByPlaceholderText("Enter a description"),
      "This is a short description to test if value is set properly",
    );
    await user.click(screen.getByText("buttons.confirm"));

    await waitFor((): void => {
      expect(onClickCancel).not.toHaveBeenCalled();
    });
    await waitFor((): void => {
      expect(onSubmitMock).toHaveBeenCalledTimes(1);
    });

    expect(onSubmitMock).toHaveBeenCalledWith(
      {
        checkbox: ["Accept"],
        inputNumber: 3,
        lastName: "Doe",
        name: "John",
        radioSelection: "Yes",
        selection: "Option 2",
        textarea:
          "This is a short description to test if value is set properly",
      },
      expect.anything(),
    );

    jest.clearAllMocks();
  });
});
