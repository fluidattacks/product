import type { FormikConfig, FormikProps, FormikValues } from "formik";

interface IInnerFormProps {
  cancelButtonLabel?: string;
  id?: string;
  onCancel: () => void;
  onConfirm?: "submit" | (() => void);
  submitButtonLabel?: string;
  submitDisabled?: boolean;
}

interface IFormProps {
  maxWidth?: string;
  minWidth?: string;
  name?: string;
  subtitle?: string;
  onCloseModal?: () => void;
  title?: string;
  width?: string;
}

type TFormProps<Values extends FormikValues> = FormikConfig<Values> &
  IFormProps;

type TInnerFormProps<Values extends FormikValues> = FormikProps<Values> &
  IInnerFormProps;

export type { IFormProps, IInnerFormProps, TFormProps, TInnerFormProps };
