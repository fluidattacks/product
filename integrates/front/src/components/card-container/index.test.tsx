import { screen } from "@testing-library/react";

import { CardContainer } from "components/card-container";
import { render } from "mocks";

describe("cardContainer", (): void => {
  it("shoul return a function", (): void => {
    expect.hasAssertions();

    expect(CardContainer).toBeInstanceOf(Function);
  });

  it("should render children", (): void => {
    expect.hasAssertions();

    render(
      <CardContainer>
        <div>{"test"}</div>
      </CardContainer>,
    );

    expect(screen.getByText("test")).toBeInTheDocument();
  });
});
