import type { TModifiable } from "@fluidattacks/design";
import type { IContainerProps } from "@fluidattacks/design/dist/components/container/types";

interface ICardContainerProps extends TModifiable {
  alignItems?: IContainerProps["alignItems"];
  children: React.ReactNode;
  bgColor?: IContainerProps["bgColor"];
  border?: IContainerProps["border"];
  gap?: IContainerProps["gap"];
  justify?: IContainerProps["justify"];
  minHeight?: IContainerProps["minHeight"];
  minWidth?: IContainerProps["minWidth"];
}

export type { ICardContainerProps };
