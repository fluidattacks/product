import { css, styled } from "styled-components";

import type { IIconWrapProps, TSize } from "./types";

import type { TCssString } from "components/@core/variants/utils";

interface IStyledIconWrapProps {
  $clickable?: IIconWrapProps["clickable"];
  $color?: IIconWrapProps["color"];
  $hoverColor?: IIconWrapProps["hoverColor"];
  $mt?: IIconWrapProps["mt"];
  $mr?: IIconWrapProps["mr"];
  $mb?: IIconWrapProps["mb"];
  $ml?: IIconWrapProps["ml"];
  $disabled?: IIconWrapProps["disabled"];
  $rotation?: IIconWrapProps["rotation"];
  $size: IIconWrapProps["size"];
  $transition?: IIconWrapProps["transition"];
}

const sizes: Record<TSize, string> = {
  lg: "16px",
  md: "14px",
  sm: "12px",
  xl: "20px",
  xs: "10px",
};

const IconWrap = styled.span<IStyledIconWrapProps>`
  color: ${({ $color }): string => $color ?? "inherit"};
  cursor: inherit;
  font-size: ${({ $size }): string => sizes[$size]};
  line-height: 0;
  margin: ${({ theme, $mt = 0, $mr = 0, $mb = 0, $ml = 0 }): string =>
    `${theme.spacing[$mt]} ${theme.spacing[$mr]} ${theme.spacing[$mb]} ${theme.spacing[$ml]}`};
  vertical-align: middle;
  pointer-events: inherit;
  opacity: ${({ $disabled = false }): string => ($disabled ? "0.5" : "1")};

  > svg {
    transform: ${({ $rotation }): string =>
      $rotation === undefined ? "" : `rotate(${$rotation}deg)`};
    transition: ${({ $transition }): string => $transition ?? ""};
  }

  ${({ $clickable, $hoverColor }): TCssString => {
    const hoverEffect =
      $hoverColor === undefined
        ? `filter: brightness(50%);`
        : `color: ${$hoverColor};`;

    return $clickable === true
      ? css`
          cursor: pointer;

          &:hover {
            ${hoverEffect}
          }
        `
      : css``;
  }};
`;

const IconInGalleryWrap = styled.div`
  cursor: pointer;
  transition: all 0.2s ease-in-out;

  &:hover:not([disabled]) {
    transform: scale(1.2);
  }
`;

export { IconWrap, IconInGalleryWrap };
