import type { IconName } from "@fortawesome/free-solid-svg-icons";
import type { MouseEventHandler } from "react";
import type { DefaultTheme } from "styled-components";

type TSize = "lg" | "md" | "sm" | "xl" | "xs";
type TIconType = "fa-brands" | "fa-light" | "fa-regular" | "fa-solid";
type TSpacing = keyof DefaultTheme["spacing"];

interface IIconWrapProps {
  clickable?: boolean;
  color?: string;
  hoverColor?: string;
  mt?: TSpacing;
  mr?: TSpacing;
  mb?: TSpacing;
  ml?: TSpacing;
  onClick?: MouseEventHandler<HTMLSpanElement>;
  disabled?: boolean;
  rotation?: number;
  size: TSize;
  transition?: string;
}

interface IIconProps extends IIconWrapProps {
  iconClass?: string;
  icon: IconName;
  iconType?: TIconType;
  addStyle?: React.CSSProperties;
}

export type { IIconProps, IIconWrapProps, TSize, TIconType };
