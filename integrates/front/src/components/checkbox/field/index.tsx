import { Container, Icon } from "@fluidattacks/design";
import { Field, useField } from "formik";
import { useCallback } from "react";
import * as React from "react";

import { AsteriskSpan, InputContainer } from "../styles";
import type { ICheckboxProps } from "../types";
import { ErrorMessage } from "components/input/styles";

const CheckboxField = ({
  alert,
  disabled = false,
  label,
  name,
  onChange,
  required = false,
  value,
}: Readonly<ICheckboxProps>): JSX.Element => {
  const [field, { error, touched }] = useField(name);
  const errorMessage = touched ? error : undefined;
  const handleChange = useCallback(
    (event: React.FormEvent<HTMLInputElement>): void => {
      if (onChange) {
        onChange(event);
      }
      field.onChange(event);
    },
    [field, onChange],
  );

  return (
    <Container>
      <InputContainer
        $alert={alert}
        $disabled={disabled}
        $error={errorMessage !== undefined}
      >
        <Field
          aria-label={name}
          disabled={disabled}
          name={name}
          onChange={handleChange}
          type={"checkbox"}
          value={value}
        />
        <Icon disabled={disabled} icon={"check"} iconSize={"xs"} />

        <span className={"label-container"}>
          {label ?? value}
          {required ? <AsteriskSpan>{"*"}</AsteriskSpan> : undefined}
        </span>
      </InputContainer>
      {errorMessage === undefined ? undefined : (
        <ErrorMessage $show={true}>
          <Icon
            clickable={false}
            icon={"circle-exclamation"}
            iconSize={"xs"}
            iconType={"fa-light"}
          />
          {errorMessage}
        </ErrorMessage>
      )}
    </Container>
  );
};

export { CheckboxField };
