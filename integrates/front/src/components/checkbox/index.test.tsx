import { Container } from "@fluidattacks/design";
import { screen, waitFor } from "@testing-library/react";
import { userEvent } from "@testing-library/user-event";
import { Formik } from "formik";

import { Checkbox } from ".";
import { CustomThemeProvider } from "components/colors";
import { render } from "mocks";

describe("checkbox components", (): void => {
  it("should render", (): void => {
    expect.hasAssertions();

    const { container } = render(
      <CustomThemeProvider>
        <Container>
          {[0, 1, 2].map(
            (index): JSX.Element => (
              <Checkbox
                defaultChecked={index === 2}
                disabled={index === 0}
                key={`checkbox-${index}`}
                name={`checkbox-${index}`}
                onChange={jest.fn()}
                value={`checkbox-${index}`}
              />
            ),
          )}
        </Container>
      </CustomThemeProvider>,
    );

    expect(screen.getByText("checkbox-0")).toBeInTheDocument();
    expect(screen.getByText("checkbox-1")).toBeInTheDocument();
    expect(screen.getByText("checkbox-2")).toBeInTheDocument();
    expect(container.querySelector('input[name="checkbox-2"]')).toBeChecked();
    expect(
      container.querySelector('input[name="checkbox-0"]'),
    ).not.toBeChecked();
  });

  it("should be clickable", async (): Promise<void> => {
    expect.hasAssertions();

    const clickCallback: jest.Mock = jest.fn();
    const onChangeCallback: jest.Mock = jest.fn();

    const { container } = render(
      <CustomThemeProvider>
        <Container>
          {[0, 1, 2].map(
            (index): JSX.Element => (
              <Checkbox
                defaultChecked={false}
                disabled={index === 0}
                key={`checkbox-${index}`}
                name={`checkbox-${index}`}
                onChange={onChangeCallback}
                onClick={clickCallback}
                value={`checkbox-${index}`}
              />
            ),
          )}
        </Container>
      </CustomThemeProvider>,
    );

    const checkbox0 = container.querySelector('input[name="checkbox-0"]');
    const checkbox1 = container.querySelector('input[name="checkbox-1"]');
    const checkbox2 = container.querySelector('input[name="checkbox-2"]');
    if (checkbox0) await userEvent.click(checkbox0);
    if (checkbox1) await userEvent.click(checkbox1);
    if (checkbox2) await userEvent.click(checkbox2);

    await waitFor((): void => {
      expect(clickCallback).toHaveBeenCalledTimes(2);
    });
    await waitFor((): void => {
      expect(onChangeCallback).toHaveBeenCalledTimes(2);
    });
  });

  it("formik variant should be clickable", async (): Promise<void> => {
    expect.hasAssertions();

    const onChangeCallback: jest.Mock = jest.fn();

    const { container } = render(
      <CustomThemeProvider>
        <Formik initialValues={{ checkbox0: false }} onSubmit={jest.fn()}>
          <Checkbox
            defaultChecked={false}
            key={`checkbox-0`}
            name={`checkbox-0`}
            onChange={onChangeCallback}
            value={`checkbox-0`}
            variant={"formikField"}
          />
        </Formik>
      </CustomThemeProvider>,
    );

    const checkbox0 = container.querySelector('input[name="checkbox-0"]');

    expect(checkbox0).not.toBeChecked();

    if (checkbox0) await userEvent.click(checkbox0);

    expect(checkbox0).toBeChecked();

    await waitFor((): void => {
      expect(onChangeCallback).toHaveBeenCalledTimes(1);
    });
  });
});
