import { Icon, Tooltip } from "@fluidattacks/design";
import { Fragment } from "react";

import { CheckboxField } from "./field";
import { InputContainer } from "./styles";
import type { ICheckboxProps } from "./types";

const Checkbox = ({
  alert,
  defaultChecked = false,
  disabled = false,
  label,
  name,
  onChange,
  onClick,
  onFocus,
  onKeyDown,
  required = false,
  tooltip,
  value,
  variant = "input",
}: Readonly<ICheckboxProps>): JSX.Element => {
  const checkbox = (): JSX.Element => {
    switch (variant) {
      case "input": {
        return (
          <InputContainer $disabled={disabled}>
            <input
              aria-label={name}
              checked={defaultChecked}
              disabled={disabled}
              name={name}
              onChange={onChange}
              onClick={onClick}
              onFocus={onFocus}
              onKeyDown={onKeyDown}
              type={"checkbox"}
              value={value}
            />
            <Icon disabled={disabled} icon={"check"} iconSize={"xs"} />
            {label ?? value}
          </InputContainer>
        );
      }
      case "formikField": {
        return (
          <CheckboxField
            alert={alert}
            disabled={disabled}
            label={label}
            name={name}
            onChange={onChange}
            required={required}
            value={value}
          />
        );
      }
      case "inputOnly":
      case "tableCheckbox":
      default: {
        return (
          <InputContainer $disabled={disabled}>
            <input
              aria-label={label}
              checked={defaultChecked}
              disabled={disabled}
              name={name}
              onChange={onChange}
              onClick={onClick}
              onFocus={onFocus}
              onKeyDown={onKeyDown}
              type={"checkbox"}
              value={value}
            />
            <Icon disabled={disabled} icon={"check"} iconSize={"xs"} />
          </InputContainer>
        );
      }
    }
  };

  return (
    <div className={"inline-flex"}>
      {checkbox()}
      {variant === "inputOnly" ? undefined : <Fragment>&nbsp;</Fragment>}
      {tooltip === undefined ? undefined : (
        <Tooltip
          icon={"circle-info"}
          id={tooltip}
          place={"right"}
          tip={tooltip}
        />
      )}
    </div>
  );
};

export { Checkbox };
