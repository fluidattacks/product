import type { HTMLAttributes } from "react";

import type { TFieldVariant } from "components/radio-button/types";

interface ICheckboxProps extends HTMLAttributes<HTMLInputElement> {
  alert?: boolean;
  disabled?: boolean;
  label?: string;
  name: string;
  required?: boolean;
  tooltip?: string;
  value?: string;
  variant?: TFieldVariant;
}

export type { ICheckboxProps };
