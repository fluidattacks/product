import { Icon } from "@fluidattacks/design";
import { useField } from "formik";
import isEmpty from "lodash/isEmpty";
import { useCallback } from "react";
import * as React from "react";
import { useTheme } from "styled-components";

import { InputContainer } from "../../input-container";
import { OutlineContainer, OutlineInput, StyledTextArea } from "../../styles";
import type { TTextAreaProps } from "../../types";

const TextArea: React.FC<TTextAreaProps> = (props): JSX.Element => {
  const {
    disabled = false,
    helpLink,
    helpText,
    label,
    name = "input-text",
    id,
    maskValue = false,
    maxLength,
    onBlur,
    placeholder,
    required,
    rows,
    tooltip,
    weight,
  } = props;
  const theme = useTheme();
  const [field, { error, touched }] = useField(name);
  const { value } = field;

  const alert = touched && !disabled ? error : undefined;
  const hasError = alert !== undefined;
  const showSuccess = !hasError && touched && !disabled && !isEmpty(value);

  const handleBlur = useCallback(
    (event: React.FocusEvent<HTMLTextAreaElement>): void => {
      if (onBlur) {
        onBlur(event);
      }
      if (!isEmpty(event.target.value.trim())) {
        field.onBlur({
          ...event,
          target: { ...event.target, name, value: "" },
        });
      }
    },
    [onBlur, field, name],
  );

  return (
    <InputContainer
      alert={alert}
      disabled={disabled}
      helpLink={helpLink}
      helpText={helpText}
      id={id}
      label={label}
      maxLength={maxLength}
      name={name}
      required={required}
      tooltip={tooltip}
      weight={weight}
    >
      <OutlineContainer
        $disabled={disabled}
        $show={hasError}
        $showSuccess={showSuccess}
      >
        <OutlineInput>
          <StyledTextArea
            {...field}
            $maskValue={maskValue}
            aria-hidden={false}
            aria-label={name}
            data-testid={`${name}-input`}
            disabled={disabled}
            id={id ?? name}
            maxLength={maxLength}
            name={name}
            onBlur={handleBlur}
            placeholder={placeholder}
            rows={rows}
            value={isEmpty(value) ? "" : value}
          />
          <div>
            {hasError ? (
              <Icon
                icon={"exclamation-circle"}
                iconClass={"error-icon"}
                iconColor={theme.palette.error[500]}
                iconSize={"xs"}
              />
            ) : undefined}
            <Icon
              icon={"check-circle"}
              iconClass={"success-icon"}
              iconColor={theme.palette.success[500]}
              iconSize={"xs"}
            />
          </div>
        </OutlineInput>
      </OutlineContainer>
    </InputContainer>
  );
};

export { TextArea };
