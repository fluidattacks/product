import { useFormikContext } from "formik";
import { useCallback } from "react";

import { InputContainer } from "../../input-container";
import type { TSelectProps } from "../../types";
import { Dropdown } from "components/dropdown";
import type { IDropDownOption } from "components/dropdown/types";

const Select = ({
  id,
  label,
  name,
  handleOnChange,
  items,
  disabled,
  placeholder,
  required = false,
  tooltip,
  weight,
}: Readonly<TSelectProps>): JSX.Element => {
  const { getFieldMeta, getFieldHelpers } = useFormikContext();
  const { error, touched, value } = getFieldMeta(name);
  const { setValue } = getFieldHelpers(name);

  const handleClickItem = useCallback(
    (selection: IDropDownOption): void => {
      if (handleOnChange) {
        handleOnChange(selection);
      }
      void setValue(selection.value);
    },
    [handleOnChange, setValue],
  );

  const placeholderItem =
    placeholder === undefined ? undefined : { header: placeholder, value: "" };

  const initialSelection = items.find(
    (option): boolean => option.value === value,
  );
  const alert = touched ? error : undefined;
  const isDisabled =
    disabled === true || (items.length === 1 && undefined === placeholder);

  return (
    <InputContainer
      alert={alert}
      aria-label={name}
      disabled={isDisabled}
      id={id}
      label={label}
      name={name}
      required={required}
      role={"combobox"}
      tooltip={tooltip}
      weight={weight}
      width={"100%"}
    >
      <Dropdown
        customSelectionHandler={handleClickItem}
        disabled={isDisabled}
        error={alert}
        id={`${id ?? name}-select`}
        initialSelection={initialSelection ?? placeholderItem}
        isSelectInput={true}
        items={items}
        open={false}
        placeholder={placeholder}
        width={"100%"}
      />
    </InputContainer>
  );
};

export { Select };
