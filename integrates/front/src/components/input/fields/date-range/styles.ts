import { styled } from "styled-components";

const DateSelectorOutline = styled.div<{
  $disabled?: boolean;
  $show?: boolean;
}>`
  border-color: ${({ theme, $show = false }): string =>
    $show ? theme.palette.error[500] : theme.palette.gray[300]};
  display: flex;
  gap: ${({ theme }): string => theme.spacing[0.25]};
  width: 100%;

  ~ div {
    background: ${({ theme, $disabled = false }): string =>
      $disabled ? theme.palette.gray[200] : theme.palette.white};
  }
`;

export { DateSelectorOutline };
