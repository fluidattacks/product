import { screen } from "@testing-library/react";
import { userEvent } from "@testing-library/user-event";
import dayjs from "dayjs";
import { Form, Formik } from "formik";
import { first, last } from "lodash";
import { object, string } from "yup";

import { InputDateRange } from ".";
import { CustomThemeProvider } from "components/colors";
import { render } from "mocks";

const validations = object().shape({
  minDateRangeTest: string().isValidFunction((value?: string): boolean => {
    if (value === undefined) {
      return false;
    }

    return dayjs(value).diff(dayjs()) > 0;
  }, "This is an error message"),
});

describe("inputDateRange component", (): void => {
  const date = new Date();
  const month = date.toLocaleString("en", { month: "long" });
  const year = date.getFullYear();

  it("should render component", (): void => {
    expect.hasAssertions();

    render(
      <Formik initialValues={{}} onSubmit={jest.fn()}>
        <Form name={"testForm"}>
          <CustomThemeProvider>
            <InputDateRange label={"Label test date"} name={"inputDateRange"} />
          </CustomThemeProvider>
        </Form>
      </Formik>,
    );

    expect(screen.getByText("Label test date")).toBeInTheDocument();
    expect(screen.getAllByRole("button", { name: "Calendar" })).toHaveLength(2);

    [/month, /u, /day, /u, /year, /u].forEach((name: RegExp): void => {
      expect(screen.getAllByRole("spinbutton", { name })).toHaveLength(2);
    });
  });

  it("should not allow edition when disabled", (): void => {
    expect.hasAssertions();

    render(
      <Formik initialValues={{}} onSubmit={jest.fn()}>
        <Form name={"testForm"}>
          <CustomThemeProvider>
            <InputDateRange
              disabled={true}
              label={"Label test date"}
              name={"inputDateRange"}
            />
          </CustomThemeProvider>
        </Form>
      </Formik>,
    );

    expect(screen.getByText("Label test date")).toBeInTheDocument();

    screen.getAllByRole("button", { name: "Calendar" }).forEach((btn): void => {
      expect(btn).toBeDisabled();
    });

    [/month, /u, /day, /u, /year, /u].forEach((name: RegExp): void => {
      screen.getAllByRole("spinbutton", { name }).forEach((spin): void => {
        expect(spin).toHaveAttribute("contenteditable", "false");
      });
    });
  });

  it("should render and get value from dropdown Calendar", async (): Promise<void> => {
    expect.hasAssertions();

    render(
      <Formik initialValues={{}} onSubmit={jest.fn()}>
        <Form name={"testForm"}>
          <CustomThemeProvider>
            <InputDateRange label={"Label test date"} name={"inputDateRange"} />
          </CustomThemeProvider>
        </Form>
      </Formik>,
    );

    expect(screen.getByText("Label test date")).toBeInTheDocument();

    await userEvent.click(
      screen.getAllByRole("button", { name: "Calendar" })[0],
    );

    expect(screen.getByText(`${month} ${year}`)).toBeInTheDocument();

    await userEvent.click(first(screen.getAllByText("2")) as HTMLElement);
    await userEvent.click(last(screen.getAllByText("27")) as HTMLElement);

    expect(
      screen.getByText(`Selected Range: ${month} 2 to 27, ${year}`),
    ).toBeInTheDocument();
  });

  it("should render and select range in different months and years", async (): Promise<void> => {
    expect.hasAssertions();

    render(
      <Formik initialValues={{}} onSubmit={jest.fn()}>
        <Form name={"testForm"}>
          <CustomThemeProvider>
            <InputDateRange label={"Label test date"} name={"inputDate"} />
          </CustomThemeProvider>
        </Form>
      </Formik>,
    );
    const newDate = new Date();
    newDate.setDate(1);
    const actualMonth = newDate.toLocaleString("en", { month: "long" });
    const actualYear = newDate.getFullYear();
    const nextMonth = newDate.getMonth() + 1;
    const dateName = `${actualMonth} ${actualYear}`;

    expect(screen.getByText("Label test date")).toBeInTheDocument();

    await userEvent.click(
      screen.getAllByRole("button", { name: "Calendar" })[0],
    );

    // Different month range
    await userEvent.click(screen.getByText("22"));
    await userEvent.click(screen.getByRole("button", { name: "Next" }));
    newDate.setDate(1);
    newDate.setMonth(nextMonth);
    await userEvent.click(screen.getByText("16"));

    const newMonth = newDate.toLocaleString("en", { month: "long" });
    const newYear =
      // eslint-disable-next-line no-negated-condition
      actualYear !== newDate.getFullYear() ? `, ${actualYear}` : "";

    expect(
      screen.getByText(
        `Selected Range: ${actualMonth} 22${newYear} to ${newMonth} 16, ${newDate.getFullYear()}`,
      ),
    ).toBeInTheDocument();

    // Different year range
    await userEvent.click(
      screen.getAllByRole("button", { name: "Calendar" })[1],
    );
    await userEvent.hover(screen.getByText(dateName));
    await userEvent.click(screen.getByRole("button", { name: "bottom" }));

    await userEvent.click(screen.getByText("20"));
    await userEvent.dblClick(screen.getByRole("button", { name: "top" }));
    await userEvent.click(screen.getByText("18"));

    expect(
      screen.getByText(
        `Selected Range: ${actualMonth} 20, ${year - 1} to ${actualMonth} 18, ${year + 1}`,
      ),
    ).toBeInTheDocument();
  });

  it("should select date range when typing", async (): Promise<void> => {
    expect.hasAssertions();

    render(
      <Formik initialValues={{}} onSubmit={jest.fn()}>
        <Form name={"testForm"}>
          <CustomThemeProvider>
            <InputDateRange label={"Label test date"} name={"inputDate"} />
          </CustomThemeProvider>
        </Form>
      </Formik>,
    );

    expect(screen.getByText("Label test date")).toBeInTheDocument();

    // Date from
    await userEvent.type(
      screen.getByRole("spinbutton", { name: /month, Start Date/u }),
      "12",
    );
    await userEvent.type(
      screen.getByRole("spinbutton", { name: /day, Start Date/u }),
      "25",
    );
    await userEvent.type(
      screen.getByRole("spinbutton", { name: /year, Start Date/u }),
      "2023",
    );

    // Date to
    await userEvent.type(
      screen.getByRole("spinbutton", { name: /month, End Date/u }),
      "10",
    );
    await userEvent.type(
      screen.getByRole("spinbutton", { name: /day, End Date/u }),
      "02",
    );
    await userEvent.type(
      screen.getByRole("spinbutton", { name: /year, End Date/u }),
      "2024",
    );

    expect(
      screen.getByText("Selected Range: December 25, 2023 to October 2, 2024"),
    ).toBeInTheDocument();
  });

  it("should show error with form validations", async (): Promise<void> => {
    expect.hasAssertions();

    render(
      <Formik
        initialValues={{}}
        onSubmit={jest.fn()}
        validationSchema={validations}
      >
        <Form name={"testForm"}>
          <CustomThemeProvider>
            <InputDateRange label={"Label test date"} name={"DateRangeTest"} />
          </CustomThemeProvider>
        </Form>
      </Formik>,
    );

    expect(screen.getByText("Label test date")).toBeInTheDocument();

    await userEvent.click(
      screen.getAllByRole("button", { name: "Calendar" })[1],
    );
    await userEvent.hover(screen.getByText(`${month} ${year}`));
    await userEvent.click(screen.getByRole("button", { name: "bottom" }));

    await userEvent.click(screen.getByText("10"));
    await userEvent.click(screen.getByText("21"));
    await userEvent.click(screen.getByText("Label test date"));

    expect(screen.getByText("This is an error message")).toBeInTheDocument();
  });
});
