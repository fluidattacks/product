import { useDateRangePicker } from "@react-aria/datepicker";
import type { DateValue } from "@react-aria/datepicker";
import { useDateRangePickerState } from "@react-stately/datepicker";
import type { DateRangePickerStateOptions } from "@react-stately/datepicker";
import type { RangeValue, ValidationError } from "@react-types/shared";
import { useField } from "formik";
import { useCallback, useRef } from "react";

import { Calendar } from "./calendar";
import { DateSelectorOutline } from "./styles";

import { InputContainer } from "../../input-container";
import { handleValueChange } from "../../utils";
import { DateSelector } from "../../utils/date-selector";
import { Dialog } from "../../utils/dialog";
import { Popover } from "../../utils/popover";
import type { IDateProps } from "../date/types";
import { formatDate } from "utils/date";

type TInputDateRangeProps = IDateProps &
  Omit<DateRangePickerStateOptions, "label">;

const InputDateRange = (props: TInputDateRangeProps): JSX.Element => {
  const datePickerRef = useRef(null);
  const state = useDateRangePickerState(props);
  const { close, isOpen } = state;
  const {
    disabled = false,
    minName = undefined,
    maxName = undefined,
    label,
    name,
    required,
    tooltip,
    handleOnMaxChange,
    handleOnMinChange,
    handleRangeChange,
  } = props;
  const dateMin = minName ?? `min${name}`;
  const dateMax = maxName ?? `max${name}`;
  const [, metaMin, fieldMin] = useField(dateMin);
  const [, metaMax, fieldMax] = useField(dateMax);

  const {
    groupProps,
    startFieldProps,
    endFieldProps,
    buttonProps,
    dialogProps,
    calendarProps,
    validationErrors,
  } = useDateRangePicker(props, state, datePickerRef);

  const error = metaMin.error ?? metaMax.error;
  const errorMsg = validationErrors.length > 0 ? validationErrors[0] : error;

  const handleCalendarChange = useCallback(
    (selectedValue: RangeValue<DateValue> | null): void => {
      if (selectedValue) {
        if (calendarProps.onChange) {
          calendarProps.onChange(selectedValue);
        }
        if (handleRangeChange) {
          handleRangeChange(selectedValue);
        }
        handleValueChange({
          selectedValue: formatDate(String(selectedValue.start)),
          setValue: fieldMin.setValue,
        });
        handleValueChange({
          selectedValue: formatDate(String(selectedValue.end)),
          setValue: fieldMax.setValue,
        });
      }
    },
    [calendarProps, fieldMax, fieldMin, handleRangeChange],
  );

  return (
    <InputContainer
      alert={errorMsg}
      disabled={disabled}
      label={label}
      name={name}
      required={required}
      tooltip={tooltip}
    >
      <DateSelectorOutline
        $disabled={disabled}
        $show={errorMsg !== undefined}
        ref={datePickerRef}
      >
        <DateSelector
          alert={errorMsg}
          buttonProps={buttonProps}
          datePickerRef={datePickerRef}
          disabled={disabled}
          fieldProps={{
            ...startFieldProps,
            "aria-label": `${startFieldProps["aria-label"]} ${name}`,
            validate: (date): ValidationError | null =>
              endFieldProps.value && date > endFieldProps.value
                ? "Start date must be less than or equal to end date"
                : null,
          }}
          groupProps={groupProps}
          handleOnChange={handleOnMinChange}
          isOpen={isOpen}
          name={dateMin}
          variant={"range"}
        />
        <DateSelector
          alert={errorMsg}
          buttonProps={buttonProps}
          datePickerRef={datePickerRef}
          disabled={disabled}
          fieldProps={{
            ...endFieldProps,
            "aria-label": `${endFieldProps["aria-label"]} ${name}`,
            validate: (date): ValidationError | null =>
              startFieldProps.value && date < startFieldProps.value
                ? "End date must be greater than or equal to start date"
                : null,
          }}
          groupProps={groupProps}
          handleOnChange={handleOnMaxChange}
          isOpen={isOpen}
          name={dateMax}
          variant={"range"}
        />
      </DateSelectorOutline>
      {state.isOpen && !disabled ? (
        <Popover
          isFilter={handleRangeChange !== undefined}
          offset={8}
          placement={"bottom start"}
          state={state}
          triggerRef={datePickerRef}
        >
          <Dialog {...dialogProps}>
            <Calendar
              onClose={close}
              props={{ ...calendarProps, onChange: handleCalendarChange }}
            />
          </Dialog>
        </Popover>
      ) : null}
    </InputContainer>
  );
};

export type { TInputDateRangeProps };
export { InputDateRange };
