import { Button } from "@fluidattacks/design";
import { createCalendar } from "@internationalized/date";
import { useRangeCalendar } from "@react-aria/calendar";
import { useLocale } from "@react-aria/i18n";
import type { RangeCalendarStateOptions } from "@react-stately/calendar";
import { useRangeCalendarState } from "@react-stately/calendar";
import { useRef } from "react";
import { useTranslation } from "react-i18next";

import { CalendarGrid } from "../../date/calendar/grid";
import { Header } from "../../date/calendar/header";
import { FooterContainer } from "../../date/calendar/styles";

const Calendar = ({
  onClose,
  props,
}: Readonly<{
  onClose: () => void;
  props: Omit<RangeCalendarStateOptions, "createCalendar" | "locale">;
}>): JSX.Element => {
  const { t } = useTranslation();
  const { locale } = useLocale();
  const calendarRef = useRef(null);
  const state = useRangeCalendarState({ ...props, createCalendar, locale });
  const { prevButtonProps, nextButtonProps, title } = useRangeCalendar(
    props,
    state,
    calendarRef,
  );

  return (
    <div className={"calendar-range"} ref={calendarRef}>
      <Header
        nextButtonProps={nextButtonProps}
        prevButtonProps={prevButtonProps}
        state={state}
        title={title}
      />
      <CalendarGrid state={state} />
      <FooterContainer>
        <Button onClick={onClose} variant={"ghost"}>
          {t("calendar.cancel")}
        </Button>
      </FooterContainer>
    </div>
  );
};

export { Calendar };
