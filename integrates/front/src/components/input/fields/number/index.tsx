import { useFormikContext } from "formik";

import { InputContainer } from "../../input-container";
import type { IInputNumberProps } from "../../types";
import { NumberField } from "../../utils/number-field";

const InputNumber = ({
  decimalPlaces,
  disabled = false,
  helpLink,
  helpText,
  id,
  label,
  maxLength,
  max,
  min,
  name = "input-number",
  onChange,
  placeholder,
  required,
  tooltip,
  updateStoredData,
  weight,
  width,
}: IInputNumberProps): JSX.Element => {
  const { getFieldMeta } = useFormikContext();
  const { error, touched } = getFieldMeta(name);

  const alert = touched && !disabled ? error : undefined;

  return (
    <InputContainer
      alert={alert}
      helpLink={helpLink}
      helpText={helpText}
      id={id}
      label={label}
      maxLength={maxLength}
      name={name}
      required={required}
      tooltip={tooltip}
      weight={weight}
      width={width}
    >
      <NumberField
        alert={alert}
        decimalPlaces={decimalPlaces}
        disabled={disabled}
        id={id}
        max={max}
        min={min}
        name={name}
        onChange={onChange}
        placeholder={placeholder}
        updateStoredData={updateStoredData}
      />
    </InputContainer>
  );
};

export { InputNumber };
