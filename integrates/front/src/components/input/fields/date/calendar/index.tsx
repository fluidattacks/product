import { Button } from "@fluidattacks/design";
import { createCalendar } from "@internationalized/date";
import { useCalendar } from "@react-aria/calendar";
import { useLocale } from "@react-aria/i18n";
import type { CalendarStateOptions } from "@react-stately/calendar";
import { useCalendarState } from "@react-stately/calendar";
import { useTranslation } from "react-i18next";

import { CalendarGrid } from "./grid";
import { Header } from "./header";
import { FooterContainer } from "./styles";

const Calendar = ({
  onClose,
  props,
}: Readonly<{
  onClose: () => void;
  props: Omit<CalendarStateOptions, "createCalendar" | "locale">;
}>): JSX.Element => {
  const { t } = useTranslation();
  const { locale } = useLocale();
  const state = useCalendarState({ ...props, createCalendar, locale });
  const { prevButtonProps, nextButtonProps, title } = useCalendar(props, state);

  return (
    <div className={"calendar"}>
      <Header
        nextButtonProps={nextButtonProps}
        prevButtonProps={prevButtonProps}
        state={state}
        title={title}
      />
      <CalendarGrid state={state} />
      <FooterContainer>
        <Button onClick={onClose} variant={"ghost"}>
          {t("calendar.cancel")}
        </Button>
      </FooterContainer>
    </div>
  );
};

export { Calendar };
