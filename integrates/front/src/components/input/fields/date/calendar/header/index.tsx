import { Icon } from "@fluidattacks/design";
import { useCallback } from "react";

import type { THeaderProps } from "../../types";
import { CaretButton, TitleContainer, YearSwitch } from "../styles";
import { Button } from "components/input/utils/calendar-button";

const Header = ({
  state,
  prevButtonProps,
  nextButtonProps,
  title,
}: THeaderProps): JSX.Element => {
  const date = state.focusedDate;

  const handleFutureClick = useCallback((): void => {
    state.setFocusedDate(date.add({ years: 1 }));
  }, [date, state]);

  const handlePastClick = useCallback((): void => {
    state.setFocusedDate(date.subtract({ years: 1 }));
  }, [date, state]);

  return (
    <div
      className={"header"}
      style={{ alignItems: "flex-start", display: "flex" }}
    >
      <Button icon={"chevron-left"} props={prevButtonProps} />
      <TitleContainer>
        {title}
        <YearSwitch>
          <CaretButton aria-label={"top"} onClick={handleFutureClick}>
            <Icon icon={"caret-up"} iconClass={"fa-sharp"} iconSize={"xxss"} />
          </CaretButton>
          <CaretButton aria-label={"bottom"} onClick={handlePastClick}>
            <Icon
              icon={"caret-down"}
              iconClass={"fa-sharp"}
              iconSize={"xxss"}
            />
          </CaretButton>
        </YearSwitch>
      </TitleContainer>
      <Button icon={"chevron-right"} props={nextButtonProps} />
    </div>
  );
};

export { Header };
