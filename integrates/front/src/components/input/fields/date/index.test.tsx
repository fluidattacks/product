import { screen, waitFor } from "@testing-library/react";
import { userEvent } from "@testing-library/user-event";
import { Form, Formik } from "formik";

import { InputDate } from ".";
import { CustomThemeProvider } from "components/colors";
import { render } from "mocks";

describe("inputDate component", (): void => {
  it("should render component", (): void => {
    expect.hasAssertions();

    render(
      <Formik initialValues={{}} onSubmit={jest.fn()}>
        <Form name={"testForm"}>
          <CustomThemeProvider>
            <InputDate label={"Label test date"} name={"inputDate"} />
          </CustomThemeProvider>
        </Form>
      </Formik>,
    );

    expect(screen.getByText("Label test date")).toBeInTheDocument();
    expect(
      screen.getByRole("button", { name: "Calendar" }),
    ).toBeInTheDocument();

    [/^month,/u, /^day,/u, /^year,/u].forEach((name: RegExp): void => {
      expect(screen.getByRole("spinbutton", { name })).toBeInTheDocument();
    });
  });

  it("should render and get value from dropdown Calendar", async (): Promise<void> => {
    expect.hasAssertions();

    render(
      <Formik initialValues={{}} onSubmit={jest.fn()}>
        <Form name={"testForm"}>
          <CustomThemeProvider>
            <InputDate label={"Date selector test label"} name={"inputDate"} />
          </CustomThemeProvider>
        </Form>
      </Formik>,
    );

    const date = new Date();
    const month = date.toLocaleString("en", { month: "long" });
    const year = date.getFullYear();

    expect(screen.getByText("Date selector test label")).toBeInTheDocument();
    expect(
      screen.getByRole("button", { name: "Calendar" }),
    ).toBeInTheDocument();

    await userEvent.click(screen.getByRole("button", { name: "Calendar" }));

    expect(screen.getByText(`${month} ${year}`)).toBeInTheDocument();

    await userEvent.click(screen.getByText("15"));

    expect(
      screen.getByText(`Selected Date: ${month} 15, ${year}`),
    ).toBeInTheDocument();
  });

  it("should render and change year and month with action buttons", async (): Promise<void> => {
    expect.hasAssertions();

    render(
      <Formik initialValues={{}} onSubmit={jest.fn()}>
        <Form name={"testForm"}>
          <CustomThemeProvider>
            <InputDate label={"Label test date"} name={"inputDate"} />
          </CustomThemeProvider>
        </Form>
      </Formik>,
    );
    const newDate = new Date();
    newDate.setDate(1);
    const actualMonth = newDate.toLocaleString("en", { month: "long" });
    const actualYear = newDate.getFullYear();
    const nextMonth = newDate.getMonth() + 1;

    const dateName = `${actualMonth} ${actualYear}`;

    expect(screen.getByText("Label test date")).toBeInTheDocument();
    expect(
      screen.getByRole("button", { name: "Calendar" }),
    ).toBeInTheDocument();

    await userEvent.click(screen.getByRole("button", { name: "Calendar" }));

    expect(screen.getByText(dateName)).toBeInTheDocument();

    // Change calendar month
    await userEvent.click(screen.getByRole("button", { name: "Next" }));
    newDate.setDate(1);
    newDate.setMonth(nextMonth);
    const newMonth = newDate.toLocaleString("en", { month: "long" });
    const nextDateName = `${newMonth} ${newDate.getFullYear()}`;

    await waitFor((): void => {
      expect(screen.queryByText(nextDateName)).toBeInTheDocument();
    });

    // Change calendar year
    await userEvent.hover(screen.getByText(nextDateName));
    await userEvent.click(screen.getByRole("button", { name: "bottom" }));

    expect(
      screen.getByText(`${newMonth} ${newDate.getFullYear() - 1}`),
    ).toBeInTheDocument();

    await userEvent.dblClick(screen.getByRole("button", { name: "top" }));

    expect(
      screen.getByText(`${newMonth} ${newDate.getFullYear() + 1}`),
    ).toBeInTheDocument();
  });
});
