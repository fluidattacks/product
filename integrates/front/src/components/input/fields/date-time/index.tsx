import type { DateValue, TimeValue } from "@react-aria/datepicker";
import { useDatePicker } from "@react-aria/datepicker";
import { useDatePickerState } from "@react-stately/datepicker";
import { useField } from "formik";
import { useCallback, useEffect, useRef } from "react";

import { Calendar } from "./calendar";

import { InputContainer } from "../../input-container";
import { handleValueChange } from "../../utils";
import { DateSelector } from "../../utils/date-selector";
import { Dialog } from "../../utils/dialog";
import { Popover } from "../../utils/popover";
import type { TInputDateProps } from "../date/types";
import { formatDateTimeInInput } from "utils/date";

const InputDateTime = (props: TInputDateProps): JSX.Element => {
  const datePickerRef = useRef(null);
  const state = useDatePickerState({
    ...props,
    granularity: "minute",
    hourCycle: 12,
    shouldCloseOnSelect: false,
  });
  const { close } = state;
  const { disabled = false, required, label, name, tooltip } = props;
  const [, { error, touched }, { setValue }] = useField(name);
  const alert = touched ? error : undefined;
  const { groupProps, fieldProps, buttonProps, dialogProps, calendarProps } =
    useDatePicker(props, state, datePickerRef);

  const handleCalendarChange = useCallback(
    (selectedValue: DateValue): void => {
      if (calendarProps.onChange) {
        calendarProps.onChange(selectedValue);
      }
      handleValueChange({
        selectedValue: formatDateTimeInInput(
          String(selectedValue).replace(/-/gu, "/"),
        ),
        setValue,
      });
    },
    [calendarProps, setValue],
  );

  const handleTimeChange = useCallback(
    (selectedValue: TimeValue | null): void => {
      if (selectedValue) state.setTimeValue(selectedValue);
    },
    [state],
  );

  useEffect((): void => {
    if (state.value)
      handleValueChange({
        selectedValue: formatDateTimeInInput(
          String(state.value).replace(/-/gu, "/"),
        ),
        setValue,
      });
  }, [state.value, setValue]);

  return (
    <InputContainer
      alert={alert}
      disabled={disabled}
      label={label}
      name={name}
      required={required}
      tooltip={tooltip}
    >
      <DateSelector
        alert={alert}
        buttonProps={buttonProps}
        datePickerRef={datePickerRef}
        disabled={disabled}
        fieldProps={fieldProps}
        granularity={"minute"}
        groupProps={groupProps}
        isOpen={state.isOpen}
        name={name}
      />
      {state.isOpen && !disabled ? (
        <Popover
          offset={8}
          placement={"bottom start"}
          state={state}
          triggerRef={datePickerRef}
        >
          <Dialog {...dialogProps}>
            <Calendar
              handleTimeChange={handleTimeChange}
              onClose={close}
              props={{ ...calendarProps, onChange: handleCalendarChange }}
              timeState={state}
            />
          </Dialog>
        </Popover>
      ) : null}
    </InputContainer>
  );
};

export { InputDateTime };
