import { Text } from "@fluidattacks/design";
import { styled } from "styled-components";

const StyledInputText = styled(Text)`
  white-space: nowrap;
  margin-right: ${({ theme }): string => theme.spacing[0.25]};
  text-overflow: ellipsis;
  overflow: hidden;
  max-width: 80%;
`;

export { StyledInputText };
