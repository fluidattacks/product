import { Button, Container, Icon, IconButton } from "@fluidattacks/design";
import { useField } from "formik";
import isEmpty from "lodash/isEmpty";
import { useCallback, useRef } from "react";
import * as React from "react";
import { useTranslation } from "react-i18next";
import { useTheme } from "styled-components";

import { StyledInputText } from "./styles";

import { InputContainer } from "../../input-container";
import { FileInput, OutlineContainer, OutlineInput } from "../../styles";
import type { TSharedProps as TInputFileProps } from "../../types";
import { createEvent, getFileName } from "../../utils";

const InputFile = (props: TInputFileProps): JSX.Element => {
  const {
    accept,
    disabled = false,
    label,
    helpLink,
    helpText,
    id,
    multiple,
    name = "input-file",
    onChange: onChangeProp,
    placeholder,
    required,
    tooltip,
    weight,
  } = props;
  const theme = useTheme();
  const { t } = useTranslation();
  const [field, { error, touched }, { setValue }] = useField(name);
  const { onChange, value } = field;
  const inputRef = useRef<HTMLInputElement>(null);
  const fileName = getFileName(value);
  const alert = touched ? error : undefined;

  const handleRemoveFile = useCallback((): void => {
    if (inputRef.current) {
      const changeEvent = createEvent("change", name, undefined);
      onChange(changeEvent);
      // eslint-disable-next-line functional/immutable-data
      inputRef.current.value = "";
      void setValue("");
    }
  }, [name, onChange, setValue]);
  const handleInputChange = useCallback(
    (event: React.ChangeEvent<HTMLInputElement>): void => {
      const { files } = event.currentTarget;
      const changeEvent = createEvent(
        "change",
        name,
        files && files.length > 0 ? files : undefined,
      );
      onChangeProp?.(
        changeEvent as unknown as React.ChangeEvent<HTMLInputElement>,
      );
      onChange(changeEvent);
    },
    [name, onChange, onChangeProp],
  );
  const handleButtonClick = useCallback(
    (event: React.MouseEvent<HTMLButtonElement>): void => {
      if (disabled) {
        event.preventDefault();
      } else if (inputRef.current) {
        inputRef.current.click();
      }
    },
    [disabled],
  );

  return (
    <InputContainer
      alert={alert}
      helpLink={helpLink}
      helpText={helpText}
      label={label}
      name={name}
      placeholder={placeholder}
      required={required}
      tooltip={tooltip}
      weight={weight}
    >
      <OutlineContainer
        $disabled={disabled}
        $show={alert !== undefined}
        style={{
          borderRadius: "8px 12px 12px 8px",
          borderRightColor: "transparent",
          justifyContent: "space-between",
        }}
      >
        <OutlineInput $width={"calc(100% - 106px)"} style={{ paddingRight: 0 }}>
          <StyledInputText size={"xs"}>{fileName}</StyledInputText>
          <Container display={"flex"} gap={0.5}>
            {isEmpty(fileName) ? undefined : (
              <IconButton
                color={theme.palette.gray[400]}
                height={"20px"}
                icon={"xmark"}
                iconSize={"xs"}
                iconType={"fa-light"}
                onClick={handleRemoveFile}
                variant={"ghost"}
                width={"20px"}
              />
            )}
            {alert === undefined ? undefined : (
              <Icon
                icon={"exclamation-circle"}
                iconClass={"error-icon"}
                iconColor={theme.palette.error[500]}
                iconSize={"xxs"}
              />
            )}
          </Container>
        </OutlineInput>
        <Button
          border={
            disabled ? `1px solid ${theme.palette.gray[300]} !important` : ""
          }
          borderRadius={"0px 8px 8px 0px"}
          disabled={disabled}
          height={"40px"}
          onClick={handleButtonClick}
          variant={"primary"}
          width={"96px"}
        >
          <Icon icon={"search"} iconSize={"xs"} mr={0.5} />
          {t("inputFile.button")}
        </Button>
      </OutlineContainer>
      <FileInput
        {...field}
        $maskValue={false}
        accept={accept}
        aria-label={name}
        data-testid={name}
        disabled={disabled}
        id={id}
        multiple={multiple}
        name={name}
        onChange={handleInputChange}
        onClick={handleRemoveFile}
        ref={inputRef}
        required={undefined}
        type={"file"}
        value={undefined}
      />
    </InputContainer>
  );
};

export { InputFile };
