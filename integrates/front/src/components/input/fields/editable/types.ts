import type { ReactNode } from "react";

interface IEditableProps {
  children?: ReactNode;
  currentValue?: string;
  externalLink?: string;
  isEditing: boolean;
  label: string;
  tooltip?: string;
}

export type { IEditableProps };
