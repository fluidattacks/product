import { screen } from "@testing-library/react";

import { Editable } from ".";
import { render } from "mocks";

describe("editable", (): void => {
  it("should render children when isEditing is true", (): void => {
    expect.hasAssertions();

    render(
      <Editable isEditing={true} label={"label"}>
        <div>{"children"}</div>
      </Editable>,
    );

    expect(screen.getByText("children")).toBeInTheDocument();
  });

  it("should render label and value when isEditing is false", (): void => {
    expect.hasAssertions();

    render(
      <Editable currentValue={"value"} isEditing={false} label={"label"}>
        <div>{"children"}</div>
      </Editable>,
    );

    expect(screen.getByText("label")).toBeInTheDocument();
    expect(screen.getByText("value")).toBeInTheDocument();
    expect(screen.queryByText("children")).not.toBeInTheDocument();
  });
});
