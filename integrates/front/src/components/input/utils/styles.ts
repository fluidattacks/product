import { styled } from "styled-components";

import type { IBorderModifiable } from "components/@core/types";
import { getStyledConfig, setBorder } from "components/@core/utils";

const OutlineContainer = styled.div.attrs({ className: "outline-input" })<{
  $disabled?: boolean;
  $show?: boolean;
  $range: boolean;
}>`
  align-items: center;
  background: ${({ theme, $disabled = false, $range }): string => {
    const rangeVariant = $range ? "inherit" : theme.palette.white;

    return $disabled ? theme.palette.gray[100] : rangeVariant;
  }};
  border: 1px solid;
  border-color: ${({ theme, $range, $show = false }): string => {
    const rangeVariant = $range ? "inherit" : theme.palette.gray[300];

    return $show ? theme.palette.error[500] : rangeVariant;
  }};
  border-radius: ${({ theme }): string => theme.spacing[0.5]};
  display: flex;
  font-family: ${({ theme }): string => theme.typography.type.primary};
  font-size: ${({ theme }): string => theme.typography.text.sm};
  font-weight: ${({ theme }): string => theme.typography.weight.regular};
  height: 40px;
  line-height: ${({ theme }): string => theme.spacing[1.25]};
  width: 100%;

  input {
    display: none;
    width: 0;
  }

  &:hover {
    border-color: ${({ theme, $disabled = false, $show = false }): string => {
      const isDisabled = theme.palette.gray[$disabled ? 300 : 600];

      return $show ? theme.palette.error[500] : isDisabled;
    }};
  }

  &:is(:focus-within, #focused) {
    ${({ theme, $disabled = false, $show = false }): string => {
      const isDisabled = $disabled
        ? theme.palette.gray[300]
        : theme.palette.black;
      const borderColor = $show ? theme.palette.error[500] : isDisabled;
      const borderWidth = $disabled || $show ? "1px solid" : "2px solid";

      return `
          border: ${borderWidth};
          border-color: ${borderColor};

          button {
            height: 34px;
          }
        `;
    }};
  }
`;

const InputDateBox = styled.div`
  ${({ theme }): string => `
  align-items: center;
  background: transparent;
  border-radius: ${theme.spacing[0.5]} 0 0 ${theme.spacing[0.5]};
  border-right: inherit;
  border-color: inherit;
  display: flex;
  height: 100%;
  justify-content: space-between;
  padding: ${theme.spacing[0.75]};
  width: calc(100% - 40px);
  `}
`;

const DialogContainer = styled.div`
  ${({ theme }): string => `
  align-items: flex-start;
  background: ${theme.palette.white};
  border: 1px solid ${theme.palette.gray[200]};
  border-radius: ${theme.spacing[0.5]};
  box-shadow: ${theme.shadows.md};
  display: inline-flex;
  flex-direction: column;
  gap: ${theme.spacing[0.25]};
  height: auto;
  padding: ${theme.spacing[0.25]};
  width: 292px;
  `}
`;

const CalendarButtonAction = styled.button<{
  $disabled: boolean;
  $focus: boolean;
  $header: boolean;
}>`
  all: unset;
  align-items: center;
  border-radius: ${({ theme, $header = false }): string =>
    $header ? `0 ${theme.spacing[0.5]} ${theme.spacing[0.5]} 0` : "4px"};
  border: 1px solid transparent;
  display: flex;
  height: ${({ $focus, $header }): string => {
    const isHeader = $header ? "36px" : "40px";

    return $focus ? "34px" : isHeader;
  }};
  justify-content: center;
  width: ${({ $header = false }): string => ($header ? "38px" : "40px")};

  &:hover {
    background-color: ${({ theme, $disabled }): string =>
      theme.palette.gray[$disabled ? 500 : 100]};
  }
`;

const StyledButtonAction = styled.button
  .withConfig(getStyledConfig())
  .attrs({ className: "action-btn" })<IBorderModifiable>`
    all: unset;
    align-items: center;
    background-color: inherit;
    border-color: inherit;
    display: flex;
    height: 38px;
    justify-content: center;
    width: 40px;

    &:hover:not(:disabled) {
      background-color: ${({ theme }): string => theme.palette.gray[100]};
      cursor: pointer;
    }
    ${setBorder}
`;

const TimeOutlineContainer = styled.div`
  align-items: center;
  display: flex;
  justify-content: flex-end;
  gap: ${({ theme }): string => theme.spacing[0.5]};
  flex: 1 0 0;
`;

const TimePickerContainer = styled.div`
  ${({ theme }): string => `
  align-items: center;
  align-self: stretch;
  background-color: ${theme.palette.gray[200]};
  border-radius: ${theme.spacing[0.25]};
  display: flex;
  font-family: ${theme.typography.type.primary};
  gap: ${theme.spacing[0.25]};
  line-height: ${theme.spacing[1.5]};
  padding: 2px ${theme.spacing[0.25]};
  `}
`;

const DayPeriodButton = styled.button`
  ${({ theme }): string => `
  align-items: center;
  background-color: inherit;
  border: 0.5px solid ${theme.palette.gray[200]};
  border-radius: 7px;
  color: ${theme.palette.gray[800]};
  cursor: pointer;
  display: flex;
  font-weight: ${theme.typography.weight.regular};
  justify-content: center;
  gap: 10px;
  padding: 2px 10px;

  &#active {
    background-color: ${theme.palette.white};
    box-shadow: ${theme.shadows.md};
    font-weight: ${theme.typography.weight.bold};
  }
  `}
`;

export {
  CalendarButtonAction,
  DayPeriodButton,
  DialogContainer,
  InputDateBox,
  OutlineContainer,
  StyledButtonAction,
  TimeOutlineContainer,
  TimePickerContainer,
};
