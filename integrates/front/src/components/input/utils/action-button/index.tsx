import { Icon } from "@fluidattacks/design";
import type { Ref } from "react";
import { forwardRef } from "react";

import { StyledButtonAction } from "../styles";
import type { IActionButtonProps } from "../types";

const ActionButton = forwardRef(
  (
    { disabled, icon, onClick, ...props }: IActionButtonProps,
    ref: Ref<HTMLButtonElement>,
  ): JSX.Element => {
    return (
      <StyledButtonAction
        aria-label={icon}
        disabled={disabled}
        onClick={onClick}
        ref={ref}
        type={"button"}
        {...props}
      >
        <Icon icon={icon} iconSize={"xxs"} iconType={"fa-light"} />
      </StyledButtonAction>
    );
  },
);

export { ActionButton };
