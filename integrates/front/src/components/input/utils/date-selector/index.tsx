/* eslint-disable react/jsx-props-no-spreading */
import type { DateValue } from "@react-aria/datepicker";
import { useField } from "formik";
import { useCallback } from "react";

import { handleValueChange } from "../../utils";
import { Button } from "../calendar-button";
import { DateField } from "../date-time-field";
import { OutlineContainer } from "../styles";
import type { IDateSelectorProps } from "../types";
import { formatDate, formatDateTime } from "utils/date";

const DateSelector = ({
  alert,
  buttonProps,
  datePickerRef,
  disabled = false,
  granularity = "day",
  handleOnChange,
  isOpen,
  fieldProps,
  groupProps,
  name,
  variant,
}: IDateSelectorProps): JSX.Element => {
  const [field, , { setValue }] = useField(name);

  const handleChange = useCallback(
    (selectedValue: DateValue | null): void => {
      if (fieldProps.onChange) {
        fieldProps.onChange(selectedValue);
      }
      if (handleOnChange && selectedValue) {
        handleOnChange(selectedValue);
      }
      if (granularity === "day") {
        handleValueChange({
          selectedValue: formatDate(String(selectedValue)),
          setValue,
        });
      } else {
        handleValueChange({
          selectedValue: formatDateTime(String(selectedValue)),
          setValue,
        });
      }
    },
    [fieldProps, granularity, handleOnChange, setValue],
  );

  return (
    <OutlineContainer
      {...groupProps}
      $disabled={disabled}
      $range={variant === "range"}
      $show={alert !== undefined}
      id={isOpen ? "focused" : ""}
      ref={datePickerRef}
    >
      <DateField
        disabled={disabled}
        error={alert !== undefined}
        props={{
          ...fieldProps,
          autoFocus: false,
          granularity,
          onChange: handleChange,
          shouldForceLeadingZeros: true,
        }}
      />
      <Button
        disabled={disabled}
        icon={granularity === "minute" ? "calendar-clock" : "calendar"}
        props={buttonProps}
      />
      <input
        {...field}
        aria-label={name}
        disabled={disabled}
        name={name}
        type={"text"}
        value={""}
      />
    </OutlineContainer>
  );
};

export { DateSelector };
