import { Icon } from "@fluidattacks/design";
import { useField } from "formik";
import _ from "lodash";
import { useCallback, useMemo } from "react";
import * as React from "react";
import { useTheme } from "styled-components";

import { OutlineContainer, OutlineInput, StyledInput } from "../../styles";
import type { IInputNumberProps } from "../../types";
import { ActionButton } from "../action-button";

const PRECISION = 10;
const NumberField: React.FC<IInputNumberProps> = (props): JSX.Element => {
  const {
    alert,
    decimalPlaces = 0,
    disabled = false,
    max = Infinity,
    min = 0,
    name = "input-number",
    onChange,
    placeholder,
    updateStoredData,
  } = props;
  const theme = useTheme();
  const [field, , { setValue }] = useField(name);
  const { value } = field;

  const decimals = useMemo(
    (): number => (decimalPlaces < 0 ? 0 : decimalPlaces),
    [decimalPlaces],
  );
  const getCurrentValue = useCallback((): number => {
    return value === undefined ? 0 : _.toNumber(value);
  }, [value]);
  const changeValue = useCallback(
    (targetValue: number | undefined): void => {
      void setValue(targetValue);
      if (updateStoredData) {
        updateStoredData(targetValue);
      }
    },
    [setValue, updateStoredData],
  );

  const handleClickMinus = useCallback((): void => {
    const newValue = (getCurrentValue() - PRECISION ** -decimals).toFixed(
      decimals,
    );
    changeValue(Math.max(Number(min), Number(newValue)));
  }, [changeValue, decimals, getCurrentValue, min]);
  const handleClickPlus = useCallback((): void => {
    const newValue = (getCurrentValue() + PRECISION ** -decimals).toFixed(
      decimals,
    );
    changeValue(
      Math.min(
        Number(max),
        Number(newValue) > Number(min) ? Number(newValue) : Number(min),
      ),
    );
  }, [changeValue, decimals, getCurrentValue, max, min]);
  const handleChange = useCallback(
    (event: React.ChangeEvent<HTMLInputElement>): void => {
      const { value: targetValue } = event.target;
      const isNumber = /\d+/u.test(targetValue) && !_.isEmpty(targetValue);
      if (updateStoredData) {
        updateStoredData(isNumber ? Number(targetValue) : undefined);
      }
      if (!isNumber) {
        void setValue(undefined);
      } else if (onChange) {
        onChange(event);
      } else {
        field.onChange(event);
      }
    },
    [field, onChange, setValue, updateStoredData],
  );

  return (
    <OutlineContainer $disabled={disabled} $show={alert !== undefined}>
      <OutlineInput
        $borderRadius={`${theme.spacing["0.5"]} 0 0 ${theme.spacing["0.5"]}`}
        $width={"calc(100% - 80px)"}
      >
        <StyledInput
          {...field}
          $maskValue={false}
          aria-hidden={false}
          aria-label={name}
          autoComplete={"off"}
          disabled={disabled}
          max={max}
          min={min}
          onChange={handleChange}
          placeholder={placeholder}
          required={undefined}
          step={PRECISION ** -decimals}
          type={"number"}
          value={/^-?\d+/u.test(value) ? value : ""}
        />
        {alert === undefined ? undefined : (
          <Icon
            icon={"exclamation-circle"}
            iconClass={"error-icon"}
            iconColor={theme.palette.error[500]}
            iconSize={"xs"}
          />
        )}
      </OutlineInput>
      <ActionButton
        borderRight={"inherit"}
        disabled={disabled}
        icon={"minus"}
        onClick={handleClickMinus}
      />
      <ActionButton
        borderRadius={`0 ${theme.spacing["0.5"]} ${theme.spacing["0.5"]} 0`}
        disabled={disabled}
        icon={"plus"}
        onClick={handleClickPlus}
      />
    </OutlineContainer>
  );
};

export { NumberField };
