/* eslint-disable react/jsx-props-no-spreading */
import { Icon } from "@fluidattacks/design";
import { createCalendar } from "@internationalized/date";
import {
  useDateField,
  useDateSegment,
  useTimeField,
} from "@react-aria/datepicker";
import {
  useDateFieldState,
  useTimeFieldState,
} from "@react-stately/datepicker";
import isNil from "lodash/isNil";
import { useCallback, useRef, useState } from "react";
import * as React from "react";
import { useTheme } from "styled-components";

import {
  DayPeriodButton,
  InputDateBox,
  TimeOutlineContainer,
  TimePickerContainer,
} from "../styles";
import type {
  IDateSegmentProps,
  TDateFieldProps,
  TTimeFieldProps,
} from "../types";

const GRAY_400 = 400;
const GRAY_500 = 500;
const GRAY_800 = 800;
const MIDDAY_HOUR = 12;
const EXCLUDED_SEGMENTS_COUNT = 2;

const DateSegment = ({ segment, state }: IDateSegmentProps): JSX.Element => {
  const theme = useTheme();
  const segmentRef = useRef(null);
  const placeholders = {
    day: "dd",
    dayPeriod: "(AM/PM)",
    era: "",
    hour: "hh",
    literal: "",
    minute: "mm",
    month: "mm",
    second: "",
    timeZoneName: "",
    year: "yyyy",
  };
  const segmentProp = { ...segment, placeholder: placeholders[segment.type] };
  const { segmentProps } = useDateSegment(segmentProp, state, segmentRef);
  const segmentStyle =
    theme.palette.gray[segmentProp.isEditable ? GRAY_800 : GRAY_500];

  return (
    <div
      {...(segmentProp.type !== "literal" && {
        ...segmentProps,
        className: "segment-cell",
      })}
      ref={segmentRef}
      style={
        segmentProp.type === "literal"
          ? undefined
          : {
              color: segmentProp.isPlaceholder
                ? theme.palette.gray[400]
                : segmentStyle,
            }
      }
    >
      {segmentProp.isPlaceholder ? segmentProp.placeholder : segmentProp.text}
    </div>
  );
};

const DateField = ({
  disabled,
  error = false,
  props,
}: TDateFieldProps): JSX.Element => {
  const theme = useTheme();
  const dateFieldRef = useRef(null);

  const state = useDateFieldState({
    ...props,
    createCalendar,
    locale: "en-US",
  });

  const { fieldProps } = useDateField(props, state, dateFieldRef);
  const preventOutClick = useCallback(
    (event: React.MouseEvent<HTMLDivElement>): void => {
      const cells = document.getElementsByClassName("segment-cell");
      const currentElement = event.currentTarget as HTMLElement;
      if (
        !Object.values(cells).some((cell): boolean =>
          cell.contains(currentElement),
        )
      ) {
        event.preventDefault();
        event.stopPropagation();
      }
    },
    [],
  );

  return (
    <InputDateBox {...fieldProps} onClick={preventOutClick}>
      <div
        className={"flex flex-row"}
        style={{
          color: theme.palette.gray[isNil(state.value) ? GRAY_400 : GRAY_800],
        }}
        tabIndex={-1}
      >
        {state.segments.map((segment, index): JSX.Element => {
          const segIndex = `key_${index}`;

          return (
            <DateSegment
              key={segIndex}
              segment={{ ...segment, isEditable: !disabled }}
              state={state}
            />
          );
        })}
      </div>
      {error ? (
        <Icon
          icon={"exclamation-circle"}
          iconColor={theme.palette.error[500]}
          iconSize={"xs"}
        />
      ) : undefined}
    </InputDateBox>
  );
};

const TimeField = (props: TTimeFieldProps): JSX.Element => {
  const timeFieldRef = useRef(null);
  const state = useTimeFieldState({
    ...props,
    locale: "en-US",
    shouldForceLeadingZeros: true,
  });
  const { timeValue } = state;
  const initDayPeriod =
    !isNil(timeValue) && timeValue.hour < MIDDAY_HOUR ? "AM" : "PM";
  const [dayPeriod, setDayPeriod] = useState(
    isNil(timeValue) ? "AM" : initDayPeriod,
  );
  const { fieldProps } = useTimeField(props, state, timeFieldRef);

  const handleDayPeriod = useCallback(
    (event: React.MouseEvent<HTMLButtonElement>): void => {
      const fieldName = event.currentTarget.name;
      if (fieldName !== dayPeriod) {
        if (!isNil(timeValue)) {
          if (fieldName === "AM") {
            state.setSegment("dayPeriod", 0);
          } else if (fieldName === "PM") {
            state.setSegment("dayPeriod", MIDDAY_HOUR);
          }
        }
      }
      setDayPeriod(fieldName);
    },
    [dayPeriod, state, timeValue],
  );

  return (
    <TimeOutlineContainer>
      <TimePickerContainer {...fieldProps} ref={timeFieldRef}>
        {state.segments
          .slice(0, -EXCLUDED_SEGMENTS_COUNT)
          .map((segment, index): JSX.Element => {
            const segIndex = `key_${index}`;

            return (
              <DateSegment key={segIndex} segment={segment} state={state} />
            );
          })}
      </TimePickerContainer>
      <TimePickerContainer>
        <DayPeriodButton
          id={dayPeriod === "AM" ? "active" : ""}
          name={"AM"}
          onClick={handleDayPeriod}
          type={"button"}
        >
          {"AM"}
        </DayPeriodButton>
        <DayPeriodButton
          id={dayPeriod === "PM" ? "active" : ""}
          name={"PM"}
          onClick={handleDayPeriod}
          type={"button"}
        >
          {"PM"}
        </DayPeriodButton>
      </TimePickerContainer>
    </TimeOutlineContainer>
  );
};

export { DateField, TimeField };
