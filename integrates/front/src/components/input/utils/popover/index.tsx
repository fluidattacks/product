import { Overlay, usePopover } from "@react-aria/overlays";
import { useCallback, useRef } from "react";
import * as React from "react";

import type { TPopOverProps } from "../types";

const Popover = ({
  children,
  isFilter = false,
  state,
  ...props
}: TPopOverProps): JSX.Element => {
  const popoverRef = useRef(null);
  const { popoverProps } = usePopover({ ...props, popoverRef }, state);

  const handlePointerEvents = useCallback(
    (event: React.PointerEvent<HTMLDivElement>): void => {
      event.preventDefault();
    },
    [],
  );

  return (
    <Overlay>
      <div
        onPointerDown={isFilter ? handlePointerEvents : undefined}
        ref={popoverRef}
        style={popoverProps.style}
      >
        {children}
      </div>
    </Overlay>
  );
};

export { Popover };
