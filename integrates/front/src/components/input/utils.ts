/* eslint-disable functional/immutable-data */
import type { FormikErrors, FormikValues } from "formik";

interface IChangeValue {
  selectedValue: string;
  setValue: (
    value: string,
    shouldValidate?: boolean,
    // eslint-disable-next-line @typescript-eslint/no-invalid-void-type
  ) => Promise<FormikErrors<FormikValues> | void>;
}

const createEvent = (type: string, name: string, value: unknown): Event => {
  const event = new Event(type);
  Object.defineProperty(event, "target", {
    value: { name, value },
  });

  return event;
};

const getFileName = (value: unknown): string => {
  const files = value as FileList | undefined;

  if (files) {
    return Array.from(files)
      .map((file): string => file.name)
      .join(", ");
  }

  return "";
};

const handleValueChange = ({ selectedValue, setValue }: IChangeValue): void => {
  void setValue(selectedValue);
};

export { createEvent, getFileName, handleValueChange };
