import type { DefaultTheme } from "styled-components";
import { styled } from "styled-components";

const OutlineContainer = styled.div<{
  $disabled: boolean;
  $show: boolean;
  $showSuccess?: boolean;
}>`
  align-items: center;
  background: ${({ theme, $disabled }): string =>
    $disabled ? theme.palette.gray[100] : theme.palette.white};
  border: 1px solid;
  border-color: ${({ theme, $show }): string =>
    $show ? theme.palette.error[500] : theme.palette.gray[300]};
  border-radius: ${({ theme }): string => theme.spacing[0.5]};
  color: ${({ theme, $disabled }): string =>
    theme.palette.gray[$disabled ? 500 : 800]};
  display: flex;
  font-family: ${({ theme }): string => theme.typography.type.primary};
  font-size: ${({ theme }): string => theme.typography.text.sm};
  font-weight: ${({ theme }): string => theme.typography.weight.regular};
  height: 40px;
  line-height: ${({ theme }): string => theme.spacing[1.25]};
  width: 100%;

  *::placeholder {
    color: ${({ theme }): string => theme.palette.gray[400]};
  }

  span {
    ${({ theme, $disabled }): string =>
      $disabled ? `color: ${theme.palette.gray[400]}` : ""}
  }

  .success-icon {
    display: none;
  }

  &:has(textarea) {
    align-items: start;
    height: auto;
  }

  ${({ theme, $disabled, $show, $showSuccess = false }): string => {
    const successState = $showSuccess
      ? `&:not(:focus-within) { border-color: ${theme.palette.success[500]}; .success-icon { display: block; } }`
      : "";
    const allowHover = $disabled
      ? "input, textarea { cursor: not-allowed }"
      : `&:hover { border-color: ${theme.palette.gray[600]}; }`;

    return $show
      ? ""
      : `
          ${allowHover}
          ${successState}

          &:focus-within {
            border: 2px solid;
            border-color: ${theme.palette.black};

            button.action-btn {
              height: 36px;
              width: 40px;
            }
          }`;
  }}
`;

const OutlineInput = styled.div<{
  $borderRadius?: string;
  $maxWidth?: string;
  $overflowX?: string;
  $width?: string;
}>`
  align-items: center;
  border: transparent;
  border-color: inherit;
  border-right: ${({ $width }): string =>
    $width === undefined ? "" : "inherit"};
  border-radius: ${({ $borderRadius }): string => $borderRadius ?? ""};
  color: inherit;
  display: flex;
  height: 100%;
  gap: 10px;
  justify-content: space-between;
  max-width: ${({ $maxWidth }): string => $maxWidth ?? "unset"};
  overflow-x: ${({ $overflowX }): string => $overflowX ?? "unset"};
  padding: ${({ theme, $overflowX }): string =>
    $overflowX === undefined
      ? `10px ${theme.spacing[0.75]}`
      : `4px ${theme.spacing[0.75]}`};
  width: ${({ $width }): string => $width ?? "100%"};
  white-space: nowrap;

  &:has(textarea) {
    align-items: start;
  }

  ul {
    display: inline-block;
    margin: unset;
    padding-right: unset;
    padding-left: unset;
  }

  .parent-option {
    background-color: inherit;
    display: flex;
    height: 100%;
    margin: unset;
    padding-right: unset;
    padding-left: unset;
    width: 100%;

    li {
      background-color: inherit;
      padding: 0;

      &:hover {
        background-color: ${({ theme }): string => theme.palette.white};
      }
    }
  }
`;

const InputBox = styled.div<{
  $disabled?: boolean;
  $width: string;
  $type?: React.AriaRole;
}>`
  /* stylelint-disable no-duplicate-selectors */
  align-items: flex-start;
  cursor: ${({ $disabled = false }): string =>
    $disabled ? "not-allowed" : "auto"};
  display: flex;
  flex-direction: column;
  gap: 4px;
  position: ${({ $type }): string =>
    $type === "combobox" ? "static" : "relative"};
  margin: 0;
  width: ${({ $width = "100%" }): string => $width};

  li {
    width: 100%;
    z-index: 20;
  }

  .dropdown li {
    padding-top: 4px;
    padding-bottom: 4px;
    height: auto;
    min-height: auto;
  }

  .parent-option,
  ul.parent-option > li {
    height: 36px;
    min-height: 36px;
  }

  ${({ theme, $disabled = false }): string =>
    $disabled
      ? `ul, li{
    background-color: ${theme.palette.gray["100"]};
  }`
      : ""}
`;

const ErrorMessage = styled.div<{ $show: boolean }>`
  align-items: center;
  align-self: stretch;
  color: ${({ theme }): string => theme.palette.error["500"]};
  display: ${({ $show }): string => ($show ? "flex" : "none")};
  font-family: ${({ theme }): string => theme.typography.type.primary};
  font-size: ${({ theme }): string => theme.typography.text.xs};
  font-weight: ${({ theme }): string => theme.typography.weight.regular};
  gap: 4px;
  line-height: 18px;
  text-align: left;
`;

const OutlineInputText = styled.div<{ $hasAction: boolean; $hasIcon: boolean }>`
  align-items: center;
  border: transparent;
  border-right: ${({ $hasAction }): string => ($hasAction ? "inherit" : "")};
  border-right-color: ${({ $hasAction }): string =>
    $hasAction ? "inherit" : "transparent"};
  border-radius: ${({ theme, $hasAction }): string =>
    $hasAction
      ? `${theme.spacing[0.5]} 0 0 ${theme.spacing[0.5]}`
      : theme.spacing[0.5]};
  display: flex;
  height: 100%;
  gap: 10px;
  justify-content: space-between;
  padding: 10px ${({ theme }): string => theme.spacing[0.75]};
  width: ${({ $hasAction }): string =>
    $hasAction ? "calc(100% - 38px)" : "100%"};
`;

const StyledInput = styled.input.attrs<{ $maskValue: boolean }>(
  ({ $maskValue }): object => ({ className: $maskValue ? "sr-block" : "" }),
)`
  align-items: center;
  background-color: inherit;
  border: transparent;
  color: inherit;
  display: flex;
  width: 100%;

  &:focus-visible {
    outline: none;
  }

  &::-webkit-calendar-picker-indicator {
    display: none !important;
  }

  &::-webkit-outer-spin-button,
  &::-webkit-inner-spin-button {
    appearance: none;
    margin: 0;
  }

  &[type="number"] {
    appearance: textfield;
  }
`;

const StyledTextArea = styled.textarea.attrs<{ $maskValue: boolean }>(
  ({ $maskValue }): object => ({ className: $maskValue ? "sr-block" : "" }),
)`
  background-color: inherit;
  border: none;
  color: inherit;
  height: auto;
  min-height: ${({ theme }): string => theme.spacing[6]};
  outline: none;
  padding: 0;
  resize: none;
  width: 100%;
`;

const FileInput = styled(StyledInput)`
  opacity: 0;
  position: absolute;
  visibility: hidden;
  width: 0;
`;

const TagInput = styled(StyledInput)`
  border: none;
  background: transparent;
  min-width: 15rem;
  padding: 0;
  flex: 1;
`;

const AsteriskSpan = styled.span`
  color: ${({ theme }): string => theme.palette.error["500"]};
  margin-left: 0.25rem;
  font-size: 1rem;
`;

const InputLabel = styled.label<{
  $weight: keyof DefaultTheme["typography"]["weight"];
}>`
  color: ${({ theme }): string => theme.palette.gray[800]};
  font-weight: ${({ theme, $weight }): string =>
    theme.typography.weight[$weight]};
  display: inline;
  font-size: 14px;

  div,
  span {
    vertical-align: top;
  }
`;

export {
  ErrorMessage,
  InputBox,
  FileInput,
  OutlineContainer,
  OutlineInput,
  OutlineInputText,
  StyledInput,
  StyledTextArea,
  TagInput,
  AsteriskSpan,
  InputLabel,
};
