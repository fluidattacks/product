import { IconButton, Search } from "@fluidattacks/design";
import { useField, useFormikContext } from "formik";
import { Fragment, useCallback, useState } from "react";
import * as React from "react";
import { useTheme } from "styled-components";

import type { TInputProps } from "../types";

const SearchBar = ({
  name,
  handleOnKeyPressed,
  handleOnSubmit,
  placeHolder,
  smallSearch = false,
}: TInputProps): JSX.Element => {
  const theme = useTheme();
  const [field] = useField(name);
  const [isOpen, setIsOpen] = useState(false);
  const { setFieldValue, values } = useFormikContext<{ search: string }>();

  const handleSearch = useCallback((): void => {
    handleOnSubmit?.(values.search);
  }, [handleOnSubmit, values.search]);

  const handleSearchByKey = useCallback(
    (event: React.KeyboardEvent<HTMLInputElement>): void => {
      if (event.key === "Enter") {
        handleSearch();
      }
      handleOnKeyPressed?.(values.search);
    },
    [handleOnKeyPressed, handleSearch, values.search],
  );

  const clearField = useCallback((): void => {
    void setFieldValue("search", "");
    handleOnKeyPressed?.("");
  }, [handleOnKeyPressed, setFieldValue]);

  const handleOnPaste = useCallback(
    (event: React.ClipboardEvent<HTMLInputElement>): void => {
      event.preventDefault();
      const inputText = event.clipboardData.getData("text").trim();
      void setFieldValue("search", inputText);
    },
    [setFieldValue],
  );

  const openSearchBar = useCallback((): void => {
    setIsOpen((previous): boolean => !previous);
  }, []);

  return smallSearch ? (
    <Fragment>
      <IconButton
        border={`1px solid ${theme.palette.gray[300]} !important`}
        icon={"magnifying-glass"}
        iconSize={"xxs"}
        iconType={"fa-light"}
        onClick={openSearchBar}
        variant={"ghost"}
      />
      {isOpen ? (
        <Search
          {...field}
          name={name}
          onClear={clearField}
          onKeyDown={handleSearchByKey}
          onKeyUp={handleSearchByKey}
          onPaste={handleOnPaste}
          onSubmit={handleSearch}
          placeholder={placeHolder}
        />
      ) : undefined}
    </Fragment>
  ) : (
    <Search
      {...field}
      name={name}
      onClear={clearField}
      onKeyDown={handleSearchByKey}
      onKeyUp={handleSearchByKey}
      onPaste={handleOnPaste}
      onSubmit={handleSearch}
      placeholder={placeHolder}
    />
  );
};

export { SearchBar };
