import { useState } from "react";

import { useDebouncedCallback } from "hooks";

interface IUseSearchParams {
  readonly debounceWait: number;
}

const useSearch = (
  params?: IUseSearchParams,
): [string | undefined, (value: string) => void] => {
  const [search, setSearch] = useState<string>();
  const DEBOUNCE_WAIT_MS = 300;
  const debouncedSetSearch = useDebouncedCallback((value: string): void => {
    setSearch(value);
  }, params?.debounceWait ?? DEBOUNCE_WAIT_MS);

  return [search, debouncedSetSearch];
};

export { useSearch };
