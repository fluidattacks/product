import { screen } from "@testing-library/react";

import { SeverityBadge } from ".";
import { CustomThemeProvider } from "components/colors";
import { render } from "mocks";

describe("severityBadge", (): void => {
  it("should render textL and textR with correct values", (): void => {
    expect.hasAssertions();

    render(
      <CustomThemeProvider>
        <SeverityBadge textL={"textL"} textR={"textR"} variant={"critical"} />
      </CustomThemeProvider>,
    );

    expect(screen.getByText("textL").textContent).toBe("textL");
    expect(screen.getByText("textR").textContent).toBe("textR");
  });
});
