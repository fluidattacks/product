import type { IFilterOptions } from "@fluidattacks/design";
import type { IOptions } from "@fluidattacks/design/dist/components/modal/filters-modal/types";
import type { FormikErrors } from "formik";
import { isNil, isUndefined } from "lodash";
import _ from "lodash";

import type {
  IFilter,
  IPermanentData,
  ISelectedOptions,
  ISwitchOptions,
  TValueType,
} from "components/filter/types";

const getInitialValues = (
  storedValues: IPermanentData[] | undefined,
): Record<string, string> => {
  if (isUndefined(storedValues)) return {};

  return storedValues.reduce((acc, permadata): object => {
    if (isNil(permadata)) return acc;

    if (!isUndefined(permadata.valueType)) {
      if (["rangeValues", "numberRangeValues"].includes(permadata.valueType)) {
        return {
          ...acc,
          [`${permadata.id}-min`]: permadata[permadata.valueType]?.[0],
          [`${permadata.id}-max`]: permadata[permadata.valueType]?.[1],
        };
      }
    }

    return { ...acc, [permadata.id]: permadata.value };
  }, {});
};

function getMappedOptions(
  filter: IFilter<object>,
  dataset?: object[],
): ISelectedOptions[] {
  const options =
    typeof filter.selectOptions === "function"
      ? filter.selectOptions(dataset ?? [])
      : filter.selectOptions;

  const mappedOptions = options?.map(
    (value): ISelectedOptions =>
      typeof value === "string" ? { value } : value,
  );

  const sortedMappedOptions = mappedOptions?.reduce<ISelectedOptions[]>(
    (acc: ISelectedOptions[], curr: ISelectedOptions): ISelectedOptions[] => {
      if (acc.some((item): boolean => item.value === curr.value)) {
        return acc;
      }

      return acc.length === 0
        ? [curr]
        : [
            ...acc.filter((value): boolean => curr.value >= value.value),
            curr,
            ...acc.filter((value): boolean => curr.value < value.value),
          ];
    },
    [],
  );

  return [
    { header: "All", value: "" },
    ...(sortedMappedOptions?.filter(Boolean) ?? []),
  ];
}

const resetStoredData = (permadata: IPermanentData): IPermanentData => {
  const switchValues = permadata.switchValues?.map(
    (checkedValue): ISwitchOptions => {
      return { ...checkedValue, checked: false };
    },
  );
  switch (permadata.valueType) {
    case "checkValues":
    case "value":
      return {
        id: permadata.id,
        value: "",
        valueType: permadata.valueType,
      };
    case "numberRangeValues":
      return {
        id: permadata.id,
        numberRangeValues: [undefined, undefined],
        valueType: "numberRangeValues",
      };
    case "rangeValues":
      return {
        id: permadata.id,
        rangeValues: ["", ""],
        valueType: "rangeValues",
      };
    case "tags":
      return {
        id: permadata.id,
        tags: [],
        valueType: "tags",
      };
    case undefined:
    case "switchValues":
    default:
      return {
        checkValues: [],
        id: permadata.id,
        numberRangeValues: [undefined, undefined],
        rangeValues: ["", ""],
        switchValues,
        value: "",
        valueType: permadata.valueType,
      };
  }
};

function mapFilterTypeToValueType<T extends object>(
  filter: IFilter<T>,
): TValueType {
  switch (filter.type) {
    case "checkboxes":
      return "checkValues";
    case "dateRange":
      return "rangeValues";
    case "numberRange":
      return "numberRangeValues";
    case "switch":
      return "switchValues";
    case "tags":
      return "tags";
    case undefined:
    case "number":
    case "select":
    case "text":
    default:
      return "value";
  }
}

function resetFilters<IData extends object>(
  filter: IFilter<IData>,
  filterToReset: IFilter<IData>,
  setFieldValue: (
    field: string,
    value: unknown,
    shouldValidate?: boolean,
  ) => Promise<FormikErrors<Record<string, string>>> | Promise<void>,
  shouldResetField = true,
): IFilter<IData> {
  const switchValues = filter.switchValues?.map(
    (checkedValue): ISwitchOptions => {
      return { ...checkedValue, checked: false };
    },
  );

  const resetField = (newValue: boolean | string | undefined): void => {
    if (shouldResetField) {
      void setFieldValue(filterToReset.id, newValue);
    }
  };

  switch (filterToReset.type) {
    case "dateRange":
      if (shouldResetField) {
        void setFieldValue(`from${filterToReset.id}`, "");
        void setFieldValue(`to${filterToReset.id}`, "");
      }

      return {
        id: filter.id,
        isBackFilter: filter.isBackFilter,
        key: filter.key,
        label: filter.label,
        minMaxRangeValues: filter.minMaxRangeValues,
        rangeValues: ["", ""],
        type: filter.type,
        valueType: "rangeValues",
      };
    case "checkboxes":
      resetField(false);

      return {
        checkValues: [],
        id: filter.id,
        isBackFilter: filter.isBackFilter,
        key: filter.key,
        label: filter.label,
        type: filter.type,
        valueType: "checkValues",
      };
    case "number":
    case "numberRange":
      if (shouldResetField) {
        void setFieldValue(`${filterToReset.id}-max`, undefined);
        void setFieldValue(`${filterToReset.id}-min`, undefined);
      }

      return {
        id: filter.id,
        isBackFilter: filter.isBackFilter,
        key: filter.key,
        label: filter.label,
        minMaxRangeValues: filter.minMaxRangeValues,
        numberRangeValues: [undefined, undefined],
        type: filter.type,
        valueType: "numberRangeValues",
      };
    case "switch":
      resetField(false);

      return {
        id: filter.id,
        key: filter.key,
        label: filter.label,
        switchValues,
        type: filter.type,
        valueType: "switchValues",
      };
    case "tags":
      resetField("");

      return {
        id: filter.id,
        isBackFilter: filter.isBackFilter,
        key: filter.key,
        label: filter.label,
        tags: [],
        type: filter.type,
        valueType: "tags",
      };
    case "select":
    case "text":
      resetField("");

      return {
        filterFn: filter.filterFn,
        id: filter.id,
        isBackFilter: filter.isBackFilter,
        key: filter.key,
        label: filter.label,
        selectOptions: filter.selectOptions,
        type: filter.type,
        value: "",
        valueType: "value",
      };

    case undefined:
    default:
      return {
        ...filter,
        checkValues: [],
        numberRangeValues: [undefined, undefined],
        rangeValues: ["", ""],
        switchValues,
        tags: [],
        value: "",
        valueType: mapFilterTypeToValueType<IData>(filter),
      };
  }
}

function areLegacyFilters<IData extends object>(
  filters: IFilter<IData>[],
): boolean {
  return filters.some((filter): boolean => filter.isLegacyFilter === true);
}

function getValueBySource<IData extends keyof IPermanentData>(
  legacyValue: IPermanentData[IData],
  storageValue: IPermanentData[IData],
  source: "legacy" | "storage",
  _key: IData,
): IPermanentData[IData] {
  return source === "legacy" ? legacyValue : storageValue;
}

function updateFilterData<IData extends object>(
  filter: IFilter<IData>,
  source: "legacy" | "storage",
  storedValues?: IPermanentData[],
): IFilter<IData> {
  const {
    legacyCheckValues,
    legacyNumberRangeValues,
    legacyRangeValues,
    legacySwitchValues,
    legacyTags,
    legacyValue,
  } = filter;
  const valueInLocalStorage = storedValues?.find(
    (permadata): boolean => permadata.id === filter.id,
  );
  const tags =
    valueInLocalStorage?.tags && valueInLocalStorage.tags.length > 0
      ? valueInLocalStorage.tags
      : valueInLocalStorage?.value?.split(",");
  switch (filter.type) {
    case "text":
    case "number":
    case "select":
      return {
        ...filter,
        value: getValueBySource(
          legacyValue,
          valueInLocalStorage?.value,
          source,
          "value",
        ),
        valueType: "value",
      };
    case "numberRange":
      return {
        ...filter,
        numberRangeValues: getValueBySource(
          legacyNumberRangeValues,
          valueInLocalStorage?.numberRangeValues,
          source,
          "numberRangeValues",
        ),
        valueType: "numberRangeValues",
      };
    case "dateRange":
      return {
        ...filter,
        rangeValues: getValueBySource(
          legacyRangeValues,
          valueInLocalStorage?.rangeValues,
          source,
          "rangeValues",
        ),
        valueType: "rangeValues",
      };
    case "checkboxes":
      return {
        ...filter,
        checkValues: getValueBySource(
          legacyCheckValues,
          valueInLocalStorage?.checkValues,
          source,
          "checkValues",
        ),
        valueType: "checkValues",
      };
    case "switch":
      return {
        ...filter,
        switchValues: getValueBySource(
          legacySwitchValues,
          valueInLocalStorage?.switchValues,
          source,
          "switchValues",
        ),
        valueType: "switchValues",
      };
    case "tags":
      return {
        ...filter,
        tags: getValueBySource(legacyTags, tags, source, "tags"),
        value: getValueBySource(
          legacyTags?.join(","),
          tags?.join(","),
          source,
          "value",
        ),
        valueType: "tags",
      };
    case undefined:
    default:
      return {
        ...filter,
        checkValues: valueInLocalStorage?.checkValues ?? filter.checkValues,
        numberRangeValues:
          valueInLocalStorage?.numberRangeValues ?? filter.numberRangeValues,
        rangeValues: valueInLocalStorage?.rangeValues ?? filter.rangeValues,
        switchValues: valueInLocalStorage?.switchValues ?? filter.switchValues,
        tags: valueInLocalStorage?.tags ?? filter.tags,
        value: valueInLocalStorage?.value ?? filter.value,
        valueType: valueInLocalStorage?.valueType ?? filter.valueType,
      };
  }
}

const getFilter = <T extends object>(
  filters: IFilter<T>[],
  key: keyof T,
): IFilter<T> | undefined => {
  const relevantFilter = filters.find((filter): boolean => filter.key === key);

  return relevantFilter;
};

const getNewFilter = <T extends object>(
  filters: IFilterOptions<T>[],
  key: keyof T,
): IFilterOptions<T> | undefined => {
  const relevantFilter = filters.find((filter): boolean => filter.key === key);

  return relevantFilter;
};

const getCheckedFilter = <T extends object>(
  filters: IFilterOptions<T>[],
  key: keyof T,
): IOptions | undefined => {
  const relevantFilter = getNewFilter(filters, key)?.options?.find(
    ({ checked }): boolean => checked ?? false,
  );

  return relevantFilter;
};

const getCheckedFilters = <T extends object>(
  filters: IFilterOptions<T>[],
  key: keyof T,
): string[] | undefined => {
  const relevantFilters = getNewFilter(filters, key)
    ?.options?.filter(({ checked }): boolean => checked ?? false)
    .map(({ value }): string => value);

  return relevantFilters;
};

const getDateRangeFilter = <T extends object>(
  filters: IFilterOptions<T>[],
  key: keyof T,
): { from: string | undefined; to: string | undefined } => {
  const filter = getNewFilter(filters, key);

  return {
    from:
      _.isEmpty(filter?.minValue) && _.isNil(filter?.minValue)
        ? undefined
        : String(filter?.minValue),
    to:
      _.isEmpty(filter?.maxValue) && _.isNil(filter?.maxValue)
        ? undefined
        : String(filter?.maxValue),
  };
};

const getNumberRangeFilter = <T extends object>(
  filters: IFilterOptions<T>[],
  key: keyof T,
): { max: number | undefined; min: number | undefined } => {
  const filter = getNewFilter(filters, key);

  return {
    max:
      _.isEmpty(filter?.maxValue) && typeof filter?.maxValue !== "number"
        ? undefined
        : Number(filter?.maxValue),
    min:
      _.isEmpty(filter?.minValue) && typeof filter?.minValue !== "number"
        ? undefined
        : Number(filter?.minValue),
  };
};

export {
  areLegacyFilters,
  getDateRangeFilter,
  getMappedOptions,
  getNumberRangeFilter,
  getInitialValues,
  getFilter,
  getNewFilter,
  getCheckedFilter,
  getCheckedFilters,
  resetFilters,
  resetStoredData,
  updateFilterData,
};
