import isNaN from "lodash/isNaN";
import isUndefined from "lodash/isUndefined";
import { useCallback } from "react";

import type { IPermanentData } from "../../../types";
import { InputNumberRange } from "components/input";
import type { IInputNumberRangeProps } from "components/input/types";

const NumberRangeFilter = ({
  id,
  label,
  propsMax,
  propsMin,
  setPermanentData,
}: IInputNumberRangeProps &
  Readonly<{
    setPermanentData: (data: IPermanentData) => void;
  }>): JSX.Element => {
  const updateStoredData = useCallback(
    (range: "max" | "min"): ((newValue?: number) => void) =>
      (newValue?: number): void => {
        const value =
          isUndefined(newValue) || newValue === 0 ? undefined : newValue;
        const validMaxValue = isNaN(propsMax.value)
          ? undefined
          : Number(propsMax.value);
        const validMinValue = isNaN(propsMin.value)
          ? undefined
          : Number(propsMin.value);
        const maxValue = isUndefined(propsMax.value)
          ? undefined
          : validMaxValue;
        const minValue = isUndefined(propsMin.value)
          ? undefined
          : validMinValue;

        if (range === "min") {
          setPermanentData({
            id: id ?? "min-value",
            numberRangeValues: [value, maxValue],
          });

          return;
        }

        setPermanentData({
          id: id ?? "max-value",
          numberRangeValues: [minValue, value],
        });
      },
    [id, setPermanentData, propsMin.value, propsMax.value],
  );

  return (
    <InputNumberRange
      id={id}
      label={label}
      propsMax={{ ...propsMax, updateStoredData: updateStoredData("max") }}
      propsMin={{ ...propsMin, updateStoredData: updateStoredData("min") }}
    />
  );
};

export { NumberRangeFilter };
