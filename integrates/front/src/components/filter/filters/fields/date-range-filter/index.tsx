import type { DateValue } from "@react-aria/calendar";
import type { RangeValue } from "@react-types/shared";
import { useCallback, useState } from "react";

import type { IPermanentData } from "../../../types";
import { InputDateRange } from "components/input";
import type { TInputDateRangeProps } from "components/input/types";
import { formatDate } from "utils/date";

/**
 * Disclaimer!
 * The range dropdown calendar needs to select a start and end date to work
 * properly, if only one of those are needed please write the dates manually
 */

const DateRangeFilter = ({
  id,
  label,
  name,
  setPermanentData,
}: Readonly<{
  setPermanentData: (data: IPermanentData) => void;
}> &
  TInputDateRangeProps): JSX.Element => {
  const [minValue, setMinValue] = useState("");
  const handleMinChange = useCallback(
    (value: DateValue): void => {
      setMinValue(formatDate(String(value)));
      setPermanentData({
        id: id ?? "min-value",
        rangeValues: [formatDate(String(value)), ""],
      });
    },
    [id, setPermanentData],
  );
  const handleMaxChange = useCallback(
    (value: DateValue): void => {
      setPermanentData({
        id: id ?? "max-value",
        rangeValues: [minValue, formatDate(String(value))],
      });
    },
    [id, minValue, setPermanentData],
  );
  const handleRangeChange = useCallback(
    (value: RangeValue<DateValue>): void => {
      setPermanentData({
        id: id ?? "range-value",
        rangeValues: [
          formatDate(String(value.start)),
          formatDate(String(value.end)),
        ],
      });
    },
    [id, setPermanentData],
  );

  return (
    <InputDateRange
      handleOnMaxChange={handleMaxChange}
      handleOnMinChange={handleMinChange}
      handleRangeChange={handleRangeChange}
      id={id}
      label={label}
      name={name}
    />
  );
};

export { DateRangeFilter };
