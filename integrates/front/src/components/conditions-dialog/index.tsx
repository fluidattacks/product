import { useCallback, useRef, useState } from "react";
import * as React from "react";

import { Checkbox } from "components/checkbox";
import { Modal, ModalConfirm } from "components/modal";
import { useModal } from "hooks/index";

interface IConditions {
  /** Checked or not */
  state: boolean;
  /** Label for checkbox */
  label: string;
}

interface IConditionsModalProps {
  readonly children?: React.ReactNode;
}

type TConditionsMap = Record<string, IConditions>;
type TConfirmAction = () => Promise<boolean>;
type TModal = React.FC<IConditionsModalProps>;

const useConditionsDialog = (
  /** Id of modal component for testing */
  id: string,
  /** Title of the modal */
  title: string,
  /** Conditions to be displayed */
  conditions: TConditionsMap,
): [TConfirmAction, TModal] => {
  const modal = useModal("confirm-dialog-modal");

  const resolveConfirm = useRef<((result: boolean) => void) | null>(null);
  const confirm = useCallback(async (): Promise<boolean> => {
    modal.open();

    return new Promise<boolean>((resolve): void => {
      // eslint-disable-next-line functional/immutable-data
      resolveConfirm.current = resolve;
    });
  }, [modal]);

  const handleConfirm = useCallback((): void => {
    modal.close();
    resolveConfirm.current?.(true);
  }, [modal]);

  const handleCancel = useCallback((): void => {
    modal.close();
    resolveConfirm.current?.(false);
  }, [modal]);

  const ConditionsDialog: TModal = ({
    children,
  }: IConditionsModalProps): React.ReactNode => {
    const [checks, setChecks] = useState<TConditionsMap>(conditions);
    const resetChecks = useCallback((): void => {
      setChecks({ ...conditions });
    }, []);

    const handleChecks = useCallback((name: string): VoidFunction => {
      return (): void => {
        setChecks(
          (prev): TConditionsMap => ({
            ...prev,
            [name]: { ...prev[name], state: !prev[name].state },
          }),
        );
      };
    }, []);

    const validSubmit = !Object.values(checks).every(
      ({ state }): boolean => state,
    );

    const checkboxes = Object.entries(checks).map(
      ([name, { state, label }]): React.ReactNode => (
        <Checkbox
          defaultChecked={state}
          key={name}
          label={label}
          name={name}
          onChange={handleChecks(name)}
        />
      ),
    );

    return (
      <Modal
        data-testid={id}
        id={id}
        modalRef={{ ...modal, close: handleCancel }}
        onClose={resetChecks}
        size={"sm"}
        title={title}
      >
        {children}
        {checkboxes}
        <ModalConfirm
          disabled={validSubmit}
          id={id}
          onConfirm={handleConfirm}
        />
      </Modal>
    );
  };

  return [confirm, ConditionsDialog];
};

export { useConditionsDialog };
