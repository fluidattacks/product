import { Container, Divider } from "@fluidattacks/design";
import { isEmpty, isUndefined } from "lodash";
import { Fragment } from "react";

import { TabItems } from "../tab-items";
import type { INestedTab } from "../types";

const renderNestedTabs = (
  color: string,
  pathname: string,
  tabs?: Record<string, INestedTab[]>,
): JSX.Element | null => {
  const getNestedPaths = (): INestedTab[] | undefined => {
    if (tabs?.[pathname]) {
      return tabs[pathname];
    }

    const nestedPathsProps = Object.entries(tabs ?? {}).filter(
      ([, nestedTab]): boolean =>
        nestedTab.some(
          ({ link, nestedTabs }): boolean =>
            link === pathname ||
            Object.values(nestedTabs ?? []).some((subTabs): boolean =>
              subTabs.some((subTab): boolean => subTab.link === pathname),
            ),
        ),
    );

    if (nestedPathsProps.length === 0) {
      return [];
    }

    return tabs?.[nestedPathsProps[0][0]];
  };

  const nestedTabsItems = getNestedPaths();

  if (isEmpty(nestedTabsItems) || isUndefined(nestedTabsItems)) return null;

  return (
    <Fragment>
      <Container pl={1.25} pr={1.25} pt={0.25}>
        <TabItems
          borders={false}
          items={nestedTabsItems}
          variant={"secondary"}
        />
      </Container>
      <Divider color={color} mb={0} mt={0} />
      {nestedTabsItems.map((tab): JSX.Element | null =>
        tab.nestedTabs
          ? renderNestedTabs(color, pathname, tab.nestedTabs)
          : null,
      )}
    </Fragment>
  );
};

export { renderNestedTabs };
