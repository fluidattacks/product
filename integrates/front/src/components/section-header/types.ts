import type { ITabProps } from "@fluidattacks/design/dist/components/tabs/types";
import type { PropsWithChildren } from "react";

interface INestedTab extends PropsWithChildren<ITabProps> {
  nestedTabs?: Record<string, INestedTab[]>;
}

interface ISectionHeaderProps {
  header: string;
  goBackTo?: string;
  tag?: {
    label: string;
    title: string;
  };
  onClickSelector?: React.MouseEventHandler<HTMLButtonElement>;
  tabs?: PropsWithChildren<ITabProps>[];
  nestedTabs?: Record<string, INestedTab[]>;
}

export type { INestedTab, ISectionHeaderProps };
