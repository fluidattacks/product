import {
  Container,
  Divider,
  Heading,
  IconButton,
  Tag,
} from "@fluidattacks/design";
import { useCallback, useRef, useState } from "react";
import { useLocation, useNavigate } from "react-router-dom";
import { useTheme } from "styled-components";

import { HeaderContainer } from "./styles";
import { TabItems } from "./tab-items";
import { renderNestedTabs } from "./tabs";
import type { ISectionHeaderProps } from "./types";

import { useScrollUpDown } from "hooks/use-scroll-up-down";

const SectionHeader = ({
  header,
  goBackTo,
  onClickSelector,
  tabs,
  tag,
  nestedTabs,
}: Readonly<ISectionHeaderProps>): JSX.Element | null => {
  const theme = useTheme();
  const [isVisible, setIsVisible] = useState(true);
  const ref = useRef<HTMLDivElement | null>(null);

  const { height } = ref.current?.getBoundingClientRect() ?? { height: 80 };
  const { pathname } = useLocation();

  useScrollUpDown({
    downThreshold: height,
    scrollDownCallback: (): void => {
      setIsVisible(false);
    },
    scrollUpCallback: (): void => {
      setIsVisible(true);
    },
  });

  const navigate = useNavigate();

  const handleGoBack = useCallback((): void => {
    if (goBackTo === undefined) return;

    navigate(goBackTo);
  }, [goBackTo, navigate]);

  const SPACING = 0.75;

  return (
    <HeaderContainer
      $isVisible={isVisible}
      alignItems={"flex-start"}
      as={"header"}
      bgColor={theme.palette.white}
      display={"flex"}
      flexDirection={"column"}
      pb={0}
      pt={0.75}
      ref={ref}
      width={"100%"}
      zIndex={999}
    >
      <Container
        alignItems={tabs ? "flex-start" : "center"}
        display={"flex"}
        justify={"space-between"}
        width={"100%"}
      >
        <Container
          alignItems={"flex-start"}
          display={"inline-flex"}
          flexDirection={"column"}
          width={"100%"}
        >
          <Container
            alignItems={"center"}
            display={"flex"}
            gap={0.5}
            pl={1.25}
            pr={1.25}
          >
            {goBackTo === undefined ? null : (
              <IconButton
                borderRadius={"2px"}
                icon={"arrow-left"}
                iconColor={theme.palette.gray[400]}
                iconSize={"xs"}
                iconType={"fa-light"}
                onClick={handleGoBack}
                variant={"ghost"}
              />
            )}
            <Heading fontWeight={"bold"} size={"sm"}>
              {header}
            </Heading>
            {tag ? (
              <Tag
                fontSize={"14px"}
                icon={"clock"}
                iconType={"fa-light"}
                tagLabel={tag.label}
                tagTitle={tag.title}
                variant={"remediation"}
              />
            ) : null}
            {onClickSelector === undefined ? null : (
              <IconButton
                borderRadius={"2px"}
                icon={"sort"}
                iconColor={theme.palette.gray[400]}
                iconSize={"xs"}
                iconType={"fa-solid"}
                id={"section-header-selector"}
                onClick={onClickSelector}
                variant={"ghost"}
              />
            )}
          </Container>
          <Container pl={1.25} pr={1.25} pt={0.25}>
            <TabItems borders={false} items={tabs} />
          </Container>
          <Divider
            color={theme.palette.gray[300]}
            mb={0}
            mt={tabs === undefined ? SPACING : 0}
          />
          {renderNestedTabs(theme.palette.gray[300], pathname, nestedTabs)}
        </Container>
      </Container>
    </HeaderContainer>
  );
};

export { SectionHeader };
