import { Tabs } from "@fluidattacks/design";
import type { ITabProps } from "@fluidattacks/design/dist/components/tabs/types";

interface ITabItemsProps {
  readonly items?: ITabProps[];
  readonly borders?: boolean;
  readonly variant?: ITabProps["variant"];
}

const TabItems = ({
  borders,
  items,
  variant,
}: Readonly<ITabItemsProps>): JSX.Element | null => {
  return items ? (
    <Tabs borders={borders} items={items} variant={variant} />
  ) : null;
};

export type { ITabItemsProps };
export { TabItems };
