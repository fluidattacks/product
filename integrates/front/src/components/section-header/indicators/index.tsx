import type { IIndicatorCardProps } from "@fluidattacks/design";
import {
  Alert,
  Container,
  GridContainer,
  type IAlertProps,
  IndicatorCard,
} from "@fluidattacks/design";
import type { PropsWithChildren } from "react";

interface IIndicatorsProps {
  cards: PropsWithChildren<IIndicatorCardProps>[];
  alertProps?: IAlertProps;
}

const Indicators = ({
  alertProps,
  cards,
}: Readonly<IIndicatorsProps>): JSX.Element => {
  const alert = alertProps ? (
    <Container pb={1}>
      <Alert {...alertProps} />
    </Container>
  ) : undefined;

  return (
    <Container>
      {alert}
      <GridContainer lg={cards.length} md={3} sm={2} xl={cards.length}>
        {cards.map((properties, index): JSX.Element => {
          const keyProp = `${properties.title}#${index}`;

          return (
            <IndicatorCard
              description={properties.description}
              height={"100%"}
              key={keyProp}
              leftIconName={properties.leftIconName}
              rightIconName={properties.rightIconName}
              title={properties.title}
              tooltipId={properties.tooltipId}
              tooltipTip={properties.tooltipTip}
              variant={properties.variant}
              width={"auto"}
            >
              {properties.children}
            </IndicatorCard>
          );
        })}
      </GridContainer>
    </Container>
  );
};

export type { IIndicatorsProps };
export { Indicators };
