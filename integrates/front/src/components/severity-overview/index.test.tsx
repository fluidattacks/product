import { screen } from "@testing-library/react";

import { SeverityOverview } from ".";
import { CustomThemeProvider } from "components/colors";
import { render } from "mocks";

describe("severityOverview", (): void => {
  it("should render variant with correct values", (): void => {
    expect.hasAssertions();

    render(
      <CustomThemeProvider>
        <SeverityOverview critical={1} high={2} low={0} medium={4} />
      </CustomThemeProvider>,
    );

    expect(screen.getByText("C")).toBeInTheDocument();
    expect(screen.getByText("H")).toBeInTheDocument();
    expect(screen.getByText("M")).toBeInTheDocument();
    expect(screen.getByText("L")).toBeInTheDocument();
    expect(screen.getByText("1")).toBeInTheDocument();
    expect(screen.getByText("2")).toBeInTheDocument();
    expect(screen.getByText("0")).toBeInTheDocument();
    expect(screen.getByText("4")).toBeInTheDocument();
  });
});
