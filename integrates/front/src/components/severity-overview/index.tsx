import { Container } from "@fluidattacks/design";

import { SeverityOverviewBadge } from "./badge";

interface ISeverityOverviewProps {
  readonly critical: number;
  readonly high: number;
  readonly medium: number;
  readonly low: number;
}

const SeverityOverview = ({
  critical,
  high,
  medium,
  low,
}: ISeverityOverviewProps): JSX.Element => {
  return (
    <Container
      alignItems={"center"}
      display={"inline-flex"}
      flex-direction={"row"}
      gap={1}
    >
      <SeverityOverviewBadge value={critical} variant={"critical"} />
      <SeverityOverviewBadge value={high} variant={"high"} />
      <SeverityOverviewBadge value={medium} variant={"medium"} />
      <SeverityOverviewBadge value={low} variant={"low"} />
    </Container>
  );
};

export type { ISeverityOverviewProps };
export { SeverityOverview };
