import { styled } from "styled-components";

const HelpContainer = styled.div`
  span {
    font-size: 20px;
  }
`;

export { HelpContainer };
