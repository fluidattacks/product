import { Container, SlideOutMenu } from "@fluidattacks/design";
import mixpanel from "mixpanel-browser";
import { useCallback, useState } from "react";
import { useTranslation } from "react-i18next";
import { useTheme } from "styled-components";

import { SideBarButton } from "./side-bar-button";
import { HelpContainer } from "./styles";
import type { ISideBarProps } from "./types";

import { useStoredState } from "hooks";
import { useAudit } from "hooks/use-audit";

const SideBar = ({
  helpItems,
  sideBarItems,
}: Readonly<ISideBarProps>): JSX.Element => {
  const theme = useTheme();
  const { t } = useTranslation();
  const [isSideBarCollapsed, setIsSideBarCollapsed] = useStoredState(
    "isSessionSideBarCollapsed",
    false,
  );
  const [isOpenHelpMenu, setIsOpenHelpMenu] = useState(false);

  const handleCollapsed = useCallback((): void => {
    setIsSideBarCollapsed(!isSideBarCollapsed);
  }, [isSideBarCollapsed, setIsSideBarCollapsed]);

  const { addAuditEvent } = useAudit();
  const handleHelpMenu = useCallback((): void => {
    if (!isOpenHelpMenu) {
      mixpanel.track("OpenHelpDeskMenu");
      addAuditEvent("HelpDeskMenu", "unknown");
    }
    setIsOpenHelpMenu(!isOpenHelpMenu);
  }, [addAuditEvent, isOpenHelpMenu]);

  return (
    <Container as={"aside"} height={"100%"}>
      <Container
        bgColor={theme.palette.white}
        borderRight={`1px solid ${theme.palette.gray[300]}`}
        display={"flex"}
        flexDirection={"column"}
        height={"100%"}
        justify={"space-between"}
        pb={1}
        position={"relative"}
        pt={0.5}
        width={isSideBarCollapsed ? "65px" : "200px"}
      >
        <Container>
          {sideBarItems.map(
            ({
              canDo,
              disabled,
              hide = false,
              id,
              icon,
              onClick,
              text,
              to,
            }): JSX.Element | null =>
              hide ? null : (
                <SideBarButton
                  canDo={canDo}
                  collapsed={isSideBarCollapsed}
                  disabled={disabled}
                  icon={icon}
                  id={id}
                  key={id}
                  marginBottom={1}
                  marginTop={1}
                  onClick={onClick}
                  text={text}
                  to={to}
                />
              ),
          )}
        </Container>
        <Container width={"100%"}>
          <SideBarButton
            collapsed={isSideBarCollapsed}
            icon={"plug"}
            id={"collapseBtn"}
            text={"Integrations"}
            to={"/integrations"}
          />
          {helpItems ? (
            <HelpContainer>
              <SideBarButton
                collapsed={isSideBarCollapsed}
                icon={"circle-question"}
                iconColor={theme.palette.primary[500]}
                iconType={"fa-solid"}
                id={"helpBtn"}
                marginBottom={0.5}
                marginTop={1}
                onClick={handleHelpMenu}
                text={t("components.sideBar.help.title")}
              />
            </HelpContainer>
          ) : undefined}
          <Container pl={1} pr={1} width={"100%"}>
            <Container
              bgColor={theme.palette.gray[300]}
              height={"1px"}
              width={"100%"}
            />
          </Container>
          <SideBarButton
            collapsed={isSideBarCollapsed}
            icon={
              isSideBarCollapsed ? "arrow-right-to-line" : "arrow-left-to-line"
            }
            id={"collapseBtn"}
            marginBottom={0.5}
            marginTop={0.5}
            onClick={handleCollapsed}
            text={t("components.sideBar.collapse")}
          />
          <Container pl={1} pr={1} width={"100%"}>
            <Container
              bgColor={theme.palette.gray[300]}
              height={"1px"}
              width={"100%"}
            />
          </Container>
        </Container>
      </Container>
      {helpItems ? (
        <SlideOutMenu
          isOpen={isOpenHelpMenu}
          items={helpItems}
          onClose={handleHelpMenu}
          title={t("components.sideBar.help.title")}
        />
      ) : undefined}
    </Container>
  );
};

export { SideBar };
