import type { ColorPalette, DefaultTheme } from "styled-components";

import type { TIconType } from "components/icon/types";

type TWeight = keyof DefaultTheme["typography"]["weight"];

const getBgColor = (
  theme: DefaultTheme,
  color: ColorPalette,
  disabled: boolean,
  selected: boolean,
): string => {
  if (disabled) {
    return theme.palette.white;
  }

  if (selected) {
    return theme.palette.primary[color];
  }

  return theme.palette.white;
};

const getIconColor = (
  theme: DefaultTheme,
  disabled: boolean,
  selected: boolean,
): string => {
  if (disabled) {
    return theme.palette.gray[200];
  }

  if (selected) {
    return theme.palette.primary[800];
  }

  return theme.palette.gray[800];
};

const getIconType = (disabled: boolean, selected: boolean): TIconType => {
  if (disabled) {
    return "fa-light";
  }

  if (selected) {
    return "fa-solid";
  }

  return "fa-light";
};

const getTextWeight = (disabled: boolean, selected: boolean): TWeight => {
  if (disabled) {
    return "regular";
  }

  if (selected) {
    return "bold";
  }

  return "regular";
};

export { getBgColor, getIconColor, getIconType, getTextWeight };
