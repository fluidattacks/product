import type { IMenuItemProps } from "@fluidattacks/design";
import type { IconName } from "@fortawesome/free-solid-svg-icons";

interface ISideBarProps {
  sideBarItems: ISideBarButton[];
  helpItems?: IMenuItemProps[];
}

interface ISideBarButton {
  canDo?: string;
  disabled?: boolean;
  hide?: boolean;
  icon: IconName;
  id: string;
  onClick?: () => void;
  text: string;
  to: string;
}

export type { ISideBarButton, ISideBarProps };
