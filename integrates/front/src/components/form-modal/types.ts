import type { FormikValues } from "formik";

import type { TFormProps } from "components/form/types";
import type { TSize } from "components/modal/types";
import type { IUseModal } from "hooks/use-modal";

interface IFormModalProps<Values extends FormikValues>
  extends Omit<TFormProps<Values>, "onCloseModal"> {
  modalRef: IUseModal;
  size: TSize;
}

export type { IFormModalProps };
