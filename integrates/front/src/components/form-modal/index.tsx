import type { FormikValues } from "formik";
import { useEffect } from "react";
import { createPortal } from "react-dom";

import { ModalContainer } from "./styles";
import type { IFormModalProps } from "./types";

import { Form } from "components/form";
import { ModalBackground } from "components/modal/styles";

const FormModal = <Values extends FormikValues>({
  children,
  enableReinitialize,
  initialValues,
  modalRef,
  name,
  onSubmit,
  subtitle,
  title,
  size,
  validationSchema,
}: Readonly<IFormModalProps<Values>>): JSX.Element | null => {
  const { close, isOpen } = modalRef;

  useEffect((): (() => void) => {
    const handleKeydown = (event: KeyboardEvent): void => {
      if (event.key === "Escape") {
        close();
      }
    };
    window.addEventListener("keydown", handleKeydown);

    return (): void => {
      window.removeEventListener("keydown", handleKeydown);
    };
  }, [close]);

  if (isOpen) {
    const formModal = (
      <ModalBackground>
        <ModalContainer $size={size} aria-labelledby={"header"} role={"dialog"}>
          <Form
            enableReinitialize={enableReinitialize}
            initialValues={initialValues}
            name={name}
            onCloseModal={close}
            onSubmit={onSubmit}
            subtitle={subtitle}
            title={title}
            validationSchema={validationSchema}
            width={"100%"}
          >
            {children}
          </Form>
        </ModalContainer>
      </ModalBackground>
    );

    return createPortal(
      formModal,
      document.getElementById("portals") ?? document.body,
    );
  }

  return null;
};

export { FormModal };
