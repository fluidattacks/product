import type { IconPrefix } from "@fortawesome/free-solid-svg-icons";
import type { DefaultTheme, Interpolation } from "styled-components";

type TCssString = Interpolation<object>[];

/**
 * Object with the icons to be printed on Iconography section.
 * The key is the name of the icon and the value is an array of icon prefixes.
 *
 * Possible prefixes are:
 * - `fas`: for solid icons
 * - `far`: for regular icons
 *
 * Example:
 * ```ts
 * const icons: IconsList = {
 *   "user": ["far", "fas"],
 *   "search": ["fas"],
 *   ...
 * };
 * ```
 */
type TIconsList = Record<string, [IconPrefix, IconPrefix] | [IconPrefix]>;

interface IComponentVariant<T extends string> {
  /**
   * Get the variant css string. It can be added to `styled` template string.
   *
   * @param theme Theme to use
   * @param variant Variant to get
   * @returns Css string for the variant
   */
  getVariant: (theme: DefaultTheme, variant: T) => TCssString;
}

/**
 * Builder function to create a function for getting component variants
 * injecting the theme.
 *
 * Example:
 * ```ts
 * const { getVariant } = variantBuilder((theme) => ({
 *  primary: css`
 *    background: ${theme.colors.primary};
 *    border: 2px solid ${theme.colors.primary};
 *    color: ${theme.colors.white};
 *  `,
 *  secondary: css`
 *    background: ${theme.colors.secondary};
 *    border: 2px solid ${theme.colors.secondary};
 *    color: ${theme.colors.white};
 *  `,
 * }));
 *
 * getVariant(theme, "primary"); // returns the primary variant
 * ```
 *
 * @param variants A function that returns the variants of a component as
 * an object with variants as keys and values as css strings.
 * @returns An object with a function to get the variant.
 */
const variantBuilder = <T extends string>(
  variants: (theme: DefaultTheme) => Record<T, TCssString>,
): IComponentVariant<T> => ({
  getVariant: (theme: DefaultTheme, variant: T): TCssString =>
    variants(theme)[variant],
});

export type { TCssString, IComponentVariant, TIconsList };
export { variantBuilder };
