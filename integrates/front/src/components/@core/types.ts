import type { Property } from "csstype";
import type { DefaultTheme } from "styled-components";

type TSpacing = keyof DefaultTheme["spacing"];
type TShadows = keyof DefaultTheme["shadows"];
type TOrientation = "horizontal" | "vertical";
type TPlace = "bottom" | "left" | "right" | "top";

interface IPaddingModifiable {
  padding?: [TSpacing, TSpacing, TSpacing, TSpacing];
  px?: TSpacing;
  py?: TSpacing;
  pl?: TSpacing;
  pr?: TSpacing;
  pt?: TSpacing;
  pb?: TSpacing;
}

interface IMarginModifiable {
  margin?: [TSpacing, TSpacing, TSpacing, TSpacing];
  mx?: TSpacing;
  my?: TSpacing;
  ml?: TSpacing;
  mr?: TSpacing;
  mt?: TSpacing;
  mb?: TSpacing;
}

interface IPositionModifiable {
  zIndex?: number;
  position?: Property.Position;
  top?: string;
  right?: string;
  bottom?: string;
  left?: string;
}

interface IBorderModifiable {
  border?: string;
  borderTop?: string;
  borderRight?: string;
  borderBottom?: string;
  borderLeft?: string;
  borderColor?: string;
  borderRadius?: string;
}

interface IDisplayModifiable {
  scroll?: "none" | "x" | "xy" | "y";
  visibility?: Property.Visibility;
  display?: Property.Display;
  height?: string;
  width?: string;
  maxHeight?: string;
  maxWidth?: string;
  minHeight?: string;
  minWidth?: string;
  shadow?: TShadows;
  bgColor?: string;
  bgGradient?: string;
  gap?: TSpacing;
  flexDirection?: Property.FlexDirection;
  flexGrow?: Property.FlexGrow;
  justify?: Property.JustifyContent;
  justifySelf?: Property.JustifyContent;
  alignItems?: Property.AlignItems;
  alignSelf?: Property.AlignItems;
  wrap?: Property.FlexWrap;
}

interface ITextModifiable {
  color?: string;
  fontSize?: Property.FontSize<number | string>;
  fontWeight?: Property.FontWeight;
  lineSpacing?: Property.LineHeight<number | string>;
  textAlign?: Property.TextAlign;
  textOverflow?: Property.Overflow;
  whiteSpace?: Property.WhiteSpace;
  wordBreak?: Property.WordBreak;
}

interface IInteractionModifiable {
  cursor?: Property.Cursor;
  transition?: string;
  borderColorHover?: string;
  bgColorHover?: string;
  shadowHover?: TShadows;
}

type TModifiable = IBorderModifiable &
  IDisplayModifiable &
  IInteractionModifiable &
  IMarginModifiable &
  IPaddingModifiable &
  IPositionModifiable &
  ITextModifiable;

export type {
  TOrientation,
  TPlace,
  TSpacing,
  IPaddingModifiable,
  IMarginModifiable,
  IDisplayModifiable,
  ITextModifiable,
  IInteractionModifiable,
  IPositionModifiable,
  IBorderModifiable,
  TModifiable,
};
