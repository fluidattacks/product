import { Can } from "context/authz/can";
import { Have } from "context/authz/have";

interface IAuthorizeProps {
  children: JSX.Element;
  /**
   * The API permissions that user must have to render the children component.
   */
  can?: string;
  /**
   * The user and group permissions that user must have
   * to render the children component.
   */
  have?: string;
  /** Any other condition for preventing the component render */
  extraCondition?: boolean;
}

/**
 * This component uses the Can and Have components to check if the user has
 * the required permissions to render the children component.
 */
const Authorize = ({
  children,
  can,
  have,
  extraCondition = true,
}: Readonly<IAuthorizeProps>): JSX.Element | undefined => {
  if (!extraCondition) {
    return undefined;
  }

  if (have !== undefined) {
    return (
      <Have I={have}>
        {can === undefined ? children : <Can do={can}>{children}</Can>}
      </Have>
    );
  }

  return can === undefined ? children : <Can do={can}>{children}</Can>;
};

export { Authorize };
