const hiddenProps = [
  // IPaddingModifiable
  "padding",
  "px",
  "py",
  "pl",
  "pr",
  "pt",
  "pb",

  // IMarginModifiable
  "margin",
  "mx",
  "my",
  "ml",
  "mr",
  "mt",
  "mb",

  // IPositionModifiable
  "zIndex",
  "position",
  "top",
  "right",
  "bottom",
  "left",

  // IBorderModifiable
  "border",
  "borderTop",
  "borderRight",
  "borderBottom",
  "borderLeft",
  "borderColor",
  "borderRadius",

  // IDisplayModifiable
  "scroll",
  "visibility",
  "display",
  "height",
  "width",
  "maxHeight",
  "maxWidth",
  "minHeight",
  "minWidth",
  "shadow",
  "bgColor",
  "bgGradient",
  "gap",
  "flexDirection",
  "flexGrow",
  "justify",
  "alignItems",
  "wrap",

  // ITextModifiable
  "color",
  "fontSize",
  "fontWeight",
  "textAlign",
  "whiteSpace",
  "lineSpacing",
  "textOverflow",

  // IInteractionModifiable
  "cursor",
  "transition",
  "borderColorHover",
  "bgColorHover",
  "shadowHover",
];

export { hiddenProps };
