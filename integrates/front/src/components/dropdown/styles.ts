import { ListItem } from "@fluidattacks/design";
import { styled } from "styled-components";

import type { IDropDownMap, TVariant } from "./types";

const DropdownContainer = styled.ul<{
  $variant: TVariant;
  $parentMap: IDropDownMap;
}>`
  ${({ theme, $variant, $parentMap }): string => `
  background-color: ${theme.palette.white};
  box-sizing: border-box;
  border: ${
    $variant === "nested-list-item"
      ? "none"
      : `1px solid ${theme.palette.gray[200]}`
  };
  gap: 10px;
  margin-top: ${$variant === "nested-list-item" ? "unset" : "4px !important"};
  max-height: 300px;
  overflow: hidden auto;
  position: ${$variant === "nested-list-item" ? "relative" : "absolute"};
  top: ${$variant === "nested-list-item" ? "0px" : ""};
  width: ${$variant === "nested-list-item" ? "100%" : `${$parentMap.width}`}px;
  border-radius: 4px;
  box-shadow: ${$variant === "nested-list-item" ? "none" : theme.shadows.md};
  z-index: 9999;

  & > li {
    margin: unset;
    padding: ${$variant === "nested-list-item" ? "0px 32px" : "10px 16px"};
  }

  li:first-child {
    border-top-left-radius: 4px;
    border-top-right-radius: 4px;
  }

  li:last-child {
    border-bottom-left-radius: 4px;
    border-bottom-right-radius: 4px;
  }
  `}
`;

const Container = styled.div<{
  $variant: TVariant;
  $width?: string;
  $error: boolean;
  $open: boolean;
}>`
  ${({ theme, $error, $open, $variant, $width = "240px" }): string => `
  display: ${$variant === "nested-list-item" ? "block" : "flex"};
  flex-direction: column;
  position: static;
  width: ${$width};
  z-index: ${$open ? "50" : "unset"};

  [aria-disabled="true"] {
    pointer-events: none;
    cursor: not-allowed;
  }

  .nested-list-item {
    border: none;
    margin: unset;
  }

  ul {
    display: inline-block;
    margin: unset;
    padding-right: unset;
    padding-left: unset;
  }

  .nested-list-item ul {
    padding: unset;
    margin: unset;
  }

  .parent-option li {
    border: ${
      $variant === "nested-list-item"
        ? "none"
        : `1px solid ${theme.palette.gray["200"]}`
    };
    display: flex;
    border-radius: ${$variant === "nested-list-item" ? "none" : "4px"};
  }

  .parent-option {
    border: ${$error ? `1px solid ${theme.palette.error["500"]}` : "none"};
    border-radius: 4px;
    height: auto;
  }
  `}
`;

const DropDownSelector = styled(ListItem)<{
  disabled: boolean;
  $isInput: boolean;
  $width?: string;
}>`
  overflow: hidden;
  min-width: ${({ $width }): string =>
    $width === undefined ? "" : `${$width} !important`};

  & > div:first-child {
    width: 95%;

    & > p {
      color: ${({ disabled, $isInput }): string =>
        disabled && $isInput ? "inherit" : ""};
      height: 20px;
      overflow: hidden;
      white-space: nowrap;
      text-overflow: ellipsis;
    }
  }
`;

const ChildrenContainer = styled.div`
  ul,
  ol {
    padding-top: 6px;
    padding-bottom: 6px;
    background-color: #fff;
    position: absolute;
    top: 40px;
    right: 1px;
    z-index: 1;
  }

  .no-hover,
  .user-info {
    text-decoration: none;
    display: flex;
    justify-content: space-between;
    height: auto;
  }

  .user-info,
  .commit-info {
    padding-left: 16px;
    padding-right: 16px;
  }

  .user-info {
    padding-top: 10px;
    padding-bottom: 1px;
  }

  .commit-info {
    padding-top: 1px;
    padding-bottom: 10px;
  }

  li {
    list-style-type: none;
  }

  span {
    white-space: nowrap;
  }

  a {
    color: ${({ theme }): string => theme.palette.gray[800]};
    text-decoration: none;
    padding: unset;
  }

  a:hover {
    text-decoration: none;
    color: ${({ theme }): string => theme.palette.gray[800]};
  }

  #menu-profile-container ul {
    min-width: unset;
    width: auto;
    z-index: 10;
  }

  #menu-profile-container li div {
    min-width: unset;
  }
`;

export { ChildrenContainer, Container, DropDownSelector, DropdownContainer };
