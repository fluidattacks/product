import { Icon } from "@fluidattacks/design";
import type { DefaultTheme } from "styled-components";
import { styled } from "styled-components";

import type { TStepState } from "./types";

import type { TOrientation } from "components/@core/types";

const colors = (state: TStepState, theme: DefaultTheme): string => {
  switch (state) {
    case "completed": {
      return theme.palette.success[500];
    }
    case "disabled": {
      return theme.palette.gray[300];
    }
    case "error": {
      return theme.palette.error[500];
    }
    case "in progress": {
      return theme.palette.gray[800];
    }
    case "not completed":
    default: {
      return theme.palette.gray[800];
    }
  }
};

const StyledIcon = styled(Icon)<{ $variant: TStepState }>`
  box-sizing: border-box;

  border-right: ${({ theme, $variant }): string =>
    $variant === "in progress"
      ? `1px dashed ${colors("in progress", theme)}`
      : "unset"};
  border-radius: 50%;
`;

const ResponsiveContainer = styled.div<{
  $minWidth: string;
  $orientation: TOrientation;
}>`
  ${({ $minWidth, $orientation }): string => `
    display: flex;
    flex-direction: ${$orientation === "horizontal" ? "row" : "column"};

    @media (width <= ${$minWidth}) {
      display: flex;
      flex-flow: row wrap;
    }
  `}
`;

export { colors, StyledIcon, ResponsiveContainer };
