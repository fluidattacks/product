import type { MouseEventHandler, ReactElement } from "react";

import type { TOrientation } from "components/@core/types";

type TStepState =
  | "completed"
  | "disabled"
  | "error"
  | "in progress"
  | "not completed";

interface IStepProps {
  title: string;
  label?: string;
  minWidth?: number;
  state: TStepState;
  stepNumber: number;
  onClick?: MouseEventHandler<HTMLSpanElement>;
  orientation?: TOrientation;
}

interface ISVGProps {
  onClick?: MouseEventHandler<HTMLOrSVGElement>;
}

interface IProgressIndicator {
  onError?: () => void;
  orientation?: TOrientation;
  minWidth?: number;
  children: ReactElement<IStepProps> | ReactElement<IStepProps>[];
}

export type { IProgressIndicator, IStepProps, ISVGProps, TStepState };
