import { IconButton } from "@fluidattacks/design";
import { useCallback, useState } from "react";
import * as React from "react";

import { Logger } from "utils/logger";

interface ICopyButtonProps {
  readonly content: string;
}
const TIME = 2000;
const CopyButton: React.FC<ICopyButtonProps> = ({
  content,
}): React.JSX.Element => {
  const [tooltip, setTooltip] = useState("Copy");

  const handleCopy = useCallback((): void => {
    navigator.clipboard
      .writeText(content)
      .then((): void => {
        setTooltip("Copied");
        setTimeout((): void => {
          setTooltip("Copy");
        }, TIME);
      })
      .catch((error: unknown): void => {
        setTooltip("Failed to copy");
        Logger.error("Failed to copy", error);
      });
  }, [content]);

  return (
    <IconButton
      icon={"copy"}
      iconSize={"xs"}
      iconType={"fa-light"}
      onClick={handleCopy}
      tooltip={tooltip}
      variant={"ghost"}
    />
  );
};

export type { ICopyButtonProps };
export { CopyButton };
