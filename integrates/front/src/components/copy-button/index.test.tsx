import { fireEvent, screen } from "@testing-library/react";

import { CopyButton } from ".";
import { render } from "mocks/index";

describe("copyButton", (): void => {
  it("should render copy button", (): void => {
    expect.hasAssertions();

    render(<CopyButton content={"Hello world"} />);

    expect(screen.getByRole("button")).toBeInTheDocument();
    expect(screen.getByTestId("copy-icon")).toBeInTheDocument();
  });

  it("should copy successfully", (): void => {
    expect.hasAssertions();

    const mockClipboard = {
      writeText: jest.fn().mockResolvedValueOnce(undefined),
    };
    // eslint-disable-next-line functional/immutable-data
    Object.assign(navigator, {
      clipboard: mockClipboard,
    });

    jest.spyOn(console, "error").mockImplementation();

    render(<CopyButton content={"Hello world"} />);

    expect(screen.getByRole("button")).toBeInTheDocument();

    fireEvent.click(screen.getByRole("button"));

    expect(navigator.clipboard.writeText).toHaveBeenCalledWith("Hello world");

    jest.clearAllMocks();
  });

  it("should fail to copy'", (): void => {
    expect.hasAssertions();

    jest.spyOn(console, "error").mockImplementation();

    const mockClipboardError = {
      writeText: jest.fn().mockRejectedValueOnce(new Error("Clipboard error")),
    };
    // eslint-disable-next-line functional/immutable-data
    Object.assign(navigator, {
      clipboard: mockClipboardError,
    });

    render(<CopyButton content={"Hello world"} />);

    expect(screen.getByRole("button")).toBeInTheDocument();

    fireEvent.click(screen.getByRole("button"));

    expect(mockClipboardError.writeText).toHaveBeenCalledTimes(1);
  });
});
