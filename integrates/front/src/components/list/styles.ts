import { styled } from "styled-components";

const StyledContainer = styled.div`
  ${({ theme }): string => `
  color: ${theme.palette.gray[700]};
  flex-shrink: 0;
  font-family: ${theme.typography.type.primary};
  font-style: normal;
  font-weight: ${theme.typography.weight.regular};
  line-height: ${theme.spacing[1.25]};

  ol,
  ul {
    padding-inline-start: ${theme.spacing[1.25]};
  }

  ol li {
    padding-left: ${theme.spacing[0.25]};
  }

  ul li {
    list-style-type: disc;
  }

  ol li ol li {
    list-style-type: lower-alpha;
  }
  `}
`;

export { StyledContainer };
