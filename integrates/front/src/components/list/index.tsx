import { Fragment, isValidElement } from "react";

import { StyledContainer } from "./styles";
import type { IListProps } from "./types";

const List = ({ items, level = 0, variant }: IListProps): JSX.Element => {
  const content =
    items === undefined
      ? null
      : items.map((item, index): JSX.Element => {
          const key = `${index}-${level}`;

          return (
            <li key={key}>
              {typeof item === "string" || isValidElement(item) ? (
                item
              ) : (
                <Fragment>
                  {Object.keys(item)[0]}
                  <List
                    items={Object.values(item)[0]}
                    level={level + 1}
                    variant={variant}
                  />
                </Fragment>
              )}
            </li>
          );
        });

  return (
    <StyledContainer>
      {variant === "ordered" ? <ol>{content}</ol> : <ul>{content}</ul>}
    </StyledContainer>
  );
};

export { List };
