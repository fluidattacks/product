import { screen } from "@testing-library/react";

import { List } from ".";
import { CustomThemeProvider } from "components/colors";
import { render } from "mocks";

describe("list", (): void => {
  it("should render a list", (): void => {
    expect.hasAssertions();

    render(
      <CustomThemeProvider>
        <List
          items={[
            "element level 1",
            "element level 1",
            {
              "element level 1": [
                "nested element level 2",
                "nested element level 2",
              ],
            },
          ]}
        />
      </CustomThemeProvider>,
    );

    expect(screen.queryAllByText("element level 1")).toHaveLength(3);
    expect(screen.queryAllByText("nested element level 2")).toHaveLength(2);
  });

  it("should render a list variant", (): void => {
    expect.hasAssertions();

    render(
      <CustomThemeProvider>
        <List
          items={[
            "element level 1",
            "element level 1",
            {
              "element level 1": [
                "nested element level 2",
                "nested element level 2",
              ],
            },
          ]}
          variant={"ordered"}
        />
      </CustomThemeProvider>,
    );

    expect(screen.queryAllByText("element level 1")).toHaveLength(3);
    expect(screen.queryAllByText("nested element level 2")).toHaveLength(2);
  });

  it("should render if the list is indefined", (): void => {
    expect.hasAssertions();

    render(
      <CustomThemeProvider>
        <List items={undefined} />
      </CustomThemeProvider>,
    );

    expect(document.getElementsByTagName("li")).toHaveLength(0);
  });
});
