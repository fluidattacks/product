import type { Property } from "csstype";
import type { ReactNode } from "react";

import type { TSpacing } from "components/@core/types";

interface ICardHeader {
  headerChildren?: ReactNode;
  textAlign?: Property.TextAlign;
  title?: string;
  description?: string;
  date?: string;
  authorEmail?: string;
  tooltip?: string;
  id?: string;
  titleColor?: string;
  descriptionColor?: string;
  textSpacing?: TSpacing;
}

export type { ICardHeader };
