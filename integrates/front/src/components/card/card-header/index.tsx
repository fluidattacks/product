import { Container, Text, Tooltip } from "@fluidattacks/design";
import { t } from "i18next";
import _ from "lodash";
import { useTheme } from "styled-components";

import type { ICardHeader } from "./types";

const SPACING = 0.5;
const CardHeader = ({
  authorEmail,
  date,
  description,
  descriptionColor,
  headerChildren,
  id,
  title,
  titleColor,
  textAlign,
  textSpacing = SPACING,
  tooltip,
}: Readonly<ICardHeader>): JSX.Element => {
  const theme = useTheme();

  return (
    <Container width={"100%"}>
      {title !== undefined && description !== undefined ? (
        <Text
          color={titleColor ?? theme.palette.gray[800]}
          fontWeight={"bold"}
          mb={textSpacing}
          size={"sm"}
          textAlign={textAlign}
          wordBreak={"break-word"}
        >
          {title}
        </Text>
      ) : undefined}
      <Container
        alignItems={"start"}
        display={"flex"}
        flexDirection={"column"}
        gap={0.25}
      >
        <Text
          color={descriptionColor ?? theme.palette.gray[800]}
          fontWeight={description === undefined ? "bold" : "regular"}
          size={"sm"}
          textAlign={textAlign}
          wordBreak={"break-word"}
        >
          {description ?? title}
        </Text>
        {_.isUndefined(date) ? undefined : (
          <Text
            color={theme.palette.gray[800]}
            fontWeight={"bold"}
            size={"sm"}
            textAlign={textAlign}
            wordBreak={"break-word"}
          >
            {`${t("searchFindings.tabEvidence.date")} ${date.split(" ")[0]}`}
          </Text>
        )}
        {_.isUndefined(authorEmail) ? undefined : (
          <Text
            color={theme.palette.gray[800]}
            fontWeight={"bold"}
            size={"sm"}
            textAlign={textAlign}
            wordBreak={"break-word"}
          >
            {`${t("searchFindings.tabEvidence.authorEmail")} ${authorEmail}`}
          </Text>
        )}

        {tooltip === undefined ? undefined : (
          <Tooltip icon={"circle-info"} id={id ?? tooltip} tip={tooltip} />
        )}
        {headerChildren}
      </Container>
    </Container>
  );
};

export { CardHeader };
