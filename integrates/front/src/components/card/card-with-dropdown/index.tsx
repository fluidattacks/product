import { useTheme } from "styled-components";

import { SelectContainer } from "./styles";
import type { ICardWithDropdown } from "./types";

import { CardHeader } from "../card-header";
import { CardContainer } from "components/card-container";
import { Dropdown } from "components/dropdown";
import { Select } from "components/input";

const CardWithDropdown = ({
  headerChildren,
  disabled,
  description,
  textAlign = "start",
  minWidth,
  minHeight,
  name = "select",
  items,
  required,
  textSpacing,
  title,
  tooltip,
  customSelectionHandler,
  variant = "dropdown",
}: ICardWithDropdown): JSX.Element => {
  const theme = useTheme();

  if (variant === "dropdown")
    return (
      <CardContainer
        alignItems={"center"}
        border={`1px solid ${theme.palette.gray[200]}`}
        borderRadius={"4px"}
        justify={"center"}
        minHeight={minHeight}
        minWidth={minWidth}
        px={1.25}
        py={1.25}
      >
        <CardHeader
          description={description}
          headerChildren={headerChildren}
          textAlign={textAlign}
          textSpacing={textSpacing}
          title={title}
          tooltip={tooltip}
        />
        <Dropdown
          customSelectionHandler={customSelectionHandler}
          disabled={disabled}
          items={items}
          role={"button"}
        />
      </CardContainer>
    );

  return (
    <SelectContainer
      minHeight={minHeight}
      minWidth={minWidth}
      px={1.25}
      py={1.25}
    >
      <CardHeader
        description={description}
        headerChildren={headerChildren}
        textAlign={textAlign}
        textSpacing={textSpacing}
        title={title}
        tooltip={tooltip}
      />
      <Select
        disabled={disabled}
        handleOnChange={customSelectionHandler}
        items={items}
        name={name}
        required={required}
      />
    </SelectContainer>
  );
};

export { CardWithDropdown };
