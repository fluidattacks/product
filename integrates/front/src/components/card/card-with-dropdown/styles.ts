import { BaseComponent } from "@fluidattacks/design";
import { styled } from "styled-components";

import type { IContainerProps } from "./types";

const SelectContainer = styled(BaseComponent)<IContainerProps>`
  background-color: ${({ theme }): string => theme.palette.white};
  align-items: start;
  border: ${({ theme }): string => `1px solid ${theme.palette.gray[200]}`};
  border-radius: ${({ theme }): string => theme.spacing[0.25]};
  display: flex;
  flex-direction: column;
  justify-content: center;
  min-height: ${({ minHeight = "auto" }): string => minHeight};
  min-width: ${({ minWidth = "auto" }): string => minWidth};
  cursor: pointer;
  gap: ${({ theme }): string => theme.spacing[0.5]};

  & > div {
    width: 100%;
  }

  label {
    color: ${({ theme }): string => theme.palette.gray[800]};
    font-size: ${({ theme }): string => theme.typography.text.sm};
  }

  .dropdown {
    margin-top: ${({ theme }): string => theme.spacing[0.5]};
  }
`;

export { SelectContainer };
