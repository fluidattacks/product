import { screen } from "@testing-library/react";
import { userEvent } from "@testing-library/user-event";

import { CardWithDropdown } from ".";
import { Form, InnerForm } from "components/form";
import { render } from "mocks";

describe("cardWithDropdown", (): void => {
  const items = [
    { header: "Item 1", value: "item1" },
    { header: "Item 2", value: "item2" },
    { header: "Item 3", value: "item3" },
  ];

  it("should render dropdown", async (): Promise<void> => {
    expect.hasAssertions();

    const handleSelection = jest.fn();

    render(
      <CardWithDropdown
        customSelectionHandler={handleSelection}
        items={items}
        title={"Title"}
      />,
    );

    expect(screen.getByText("Title")).toBeInTheDocument();
    expect(screen.getByText("Item 1")).toBeInTheDocument();
    expect(screen.queryByText("Item 2")).not.toBeInTheDocument();

    await userEvent.click(screen.getByRole("listitem"));

    expect(screen.getByText("Item 2")).toBeInTheDocument();

    await userEvent.click(screen.getByText("Item 2"));

    expect(handleSelection).toHaveBeenCalledTimes(1);
    expect(handleSelection).toHaveBeenCalledWith(items[1]);
  });

  it("should render card with select", async (): Promise<void> => {
    expect.hasAssertions();

    const handleSubmit = jest.fn();

    render(
      <Form initialValues={{ selectItem: "item1" }} onSubmit={handleSubmit}>
        {(props): JSX.Element => (
          <InnerForm {...props}>
            <CardWithDropdown
              customSelectionHandler={jest.fn()}
              items={items}
              name={"selectItem"}
              title={"Title"}
              variant={"select"}
            />
          </InnerForm>
        )}
      </Form>,
    );

    expect(screen.getByText("Title")).toBeInTheDocument();
    expect(screen.getAllByText("Item 1")[0]).toBeInTheDocument();

    await userEvent.click(screen.getAllByText("Item 1")[0]);
    await userEvent.click(screen.getByText("Item 2"));
    await userEvent.click(screen.getByText("buttons.confirm"));

    expect(handleSubmit).toHaveBeenCalledTimes(1);
    expect(handleSubmit).toHaveBeenCalledWith(
      { selectItem: "item2" },
      expect.anything(),
    );
  });
});
