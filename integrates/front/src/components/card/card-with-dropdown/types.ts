import type { ICardHeader } from "../card-header/types";
import type { ICardContainerProps } from "components/card-container/types";
import type { IDropdownProps } from "components/dropdown/types";

interface IContainerProps {
  minHeight?: ICardContainerProps["minHeight"];
  minWidth?: ICardContainerProps["minWidth"];
}
interface ICardWithDropdown
  extends Omit<IDropdownProps, "variant">,
    IContainerProps,
    ICardHeader {
  variant?: "dropdown" | "select";
  name?: string;
  required?: boolean;
}

export type { ICardWithDropdown, IContainerProps };
