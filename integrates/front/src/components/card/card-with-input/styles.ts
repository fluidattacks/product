import { styled } from "styled-components";

import type { ICardInputContainerProps } from "./types";

const CardInputContainer = styled.div<ICardInputContainerProps>`
  ${({
    $inputAlign = "center",
    $minWidth = "auto",
    $minHeight = "auto",
    theme,
  }): string => `
  min-width: ${$minWidth};
  min-height: ${$minHeight};
  padding: 16px;
  background-color: ${theme.palette.white};
  border-radius: ${theme.spacing[0.25]};
  border: 1px solid ${theme.palette.gray[200]};
  display: flex;
  flex-direction: column;
  justify-content: center;
  gap: 8px;

  input {
    background-color: inherit;
    outline: none;
    border-radius: ${theme.spacing[0.25]};
    font-family: inherit;
    font-size: ${theme.typography.text.sm};
    color: ${theme.palette.gray[800]};
    font-style: normal;
    font-weight: ${theme.typography.weight.regular};
    line-height: ${theme.spacing[1.5]};
    height: 45px;
    width: 100%;
    flex-direction: column;
    justify-content: center;
    padding: ${theme.spacing[1.5]} 0;
    text-align: ${$inputAlign};
  }

  input:focus {
    outline: none;
  }

  input:disabled {
    color: ${theme.palette.gray[300]};
  }

  [aria-disabled="true"] {
    background-color: ${theme.palette.gray[200]};
  }

  .input-date {
    bottom: 17px;
    top: unset;
  }
`}
`;

export { CardInputContainer };
