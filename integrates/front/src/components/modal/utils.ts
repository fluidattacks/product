import type { DefaultTheme } from "styled-components";

import type { TSize } from "./types";

const getVariant = (theme: DefaultTheme, size: TSize): string => {
  const variants: Record<TSize, string> = {
    fixed: `
      width: 300px;
    `,
    lg: `
      width: 80%;

      @media screen and (min-width: ${theme.breakpoints.sm}) {
        width: 72%;
      }
    `,
    md: `
      width: 60%;

      @media screen and (min-width: ${theme.breakpoints.sm}) {
        width: 48%;
      }
    `,
    sm: `
      width: 40%;

      @media screen and (min-width: ${theme.breakpoints.sm}) {
        width: 32%;
      }
    `,
  };

  return variants[size];
};

export { getVariant };
