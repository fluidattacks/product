import type { HTMLAttributes } from "react";

import type { TSpacing } from "components/@core/types";
import type { IUseModal } from "hooks/use-modal";

interface IStyledImageProps extends HTMLAttributes<HTMLImageElement> {
  $framed?: boolean;
}

interface IImageContainerProps extends HTMLAttributes<HTMLImageElement> {
  framed?: boolean;
}

interface IStyledFooterProps extends HTMLAttributes<HTMLDivElement> {
  $fullInfo?: boolean;
  $gap?: TSpacing;
  $justifyContent?: "flex-start" | "space-between";
}

interface IFooterProps extends HTMLAttributes<HTMLDivElement> {
  fullInfo?: boolean;
  gap?: string;
  justifyContent?: "flex-start" | "space-between";
}

// Interface with button info.
interface IButtonProps {
  onClick: () => void;
  text: string;
}

interface IModalContent {
  // Image source to use.
  imageSrc?: string;

  // It frames the image with a border.
  imageFramed?: boolean;

  // Caption below the image.
  imageText?: string;
}

type TSize = "fixed" | "lg" | "md" | "sm";

interface IModalProps {
  // Prop for storybook rendering
  _portal?: boolean;

  /**
   * Data for the cancel button.
   *
   * Example:
   * ```
   * {
   *   onClick: () => {},
   *   text: 'Cancel',
   * }
   * ```
   */
  cancelButton?: IButtonProps;

  children?: React.ReactNode;

  id?: string;

  /**
   * Data for the confirm button.
   *
   * Example:
   * ```
   * {
   *   onClick: () => {},
   *   text: 'Confirm',
   * }
   * ```
   */
  confirmButton?: IButtonProps;

  /**
   * Image data for the modal.
   *
   * Example:
   * ```
   * {
   *   imageSrc: 'https://example.com/150',
   *   imageFramed: true,
   *   imageText: 'Image text',
   * }
   * ```
   */
  content?: IModalContent;

  // Description text for modal.
  description?: React.ReactNode | string;

  // Boolean that includes a functional "don't show again" checkbox.
  dontShowCheckbox?: boolean;

  /**
   * Value from useModal hook.
   *
   * Example:
   * ```
   * const modalRef = useModal("test-modal");
   *
   * <Modal modalRef={modalRef} ... />
   * ```
   */
  modalRef: IUseModal;

  // Title text for modal.
  title?: React.ReactNode | string;

  // Additional actions needed in modal header.
  otherActions?: React.ReactNode;

  // Hook for doing actions when modal is closed.
  onClose?: () => void;

  // Size of modal according to content and window resolution.
  size: TSize;
}

interface IModalConfirmProps {
  disabled?: boolean;
  id?: string;
  onCancel?: () => void;
  onConfirm?: "submit" | (() => void);
  txtCancel?: string;
  txtConfirm?: React.ReactNode | string;
  variant?: "large";
}

export type {
  IModalProps,
  IStyledImageProps,
  IFooterProps,
  IStyledFooterProps,
  IModalContent,
  IImageContainerProps,
  IModalConfirmProps,
  TSize,
};
