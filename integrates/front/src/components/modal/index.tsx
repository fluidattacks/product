import { CloudImage, Text } from "@fluidattacks/design";
import _ from "lodash";
import type { PropsWithChildren } from "react";
import { createPortal } from "react-dom";

import { ModalConfirm } from "./modal-confirm";
import { ModalFooter } from "./modal-footer";
import { ModalHeader } from "./modal-header";
import { ImageContainer, ModalBackground, ModalContainer } from "./styles";
import type { IModalProps } from "./types";

const Modal = ({
  _portal = true,
  cancelButton = undefined,
  children = undefined,
  confirmButton = undefined,
  content = undefined,
  description = "",
  modalRef,
  otherActions = undefined,
  onClose = undefined,
  title = "",
  size,
  id = "modal-container",
}: Readonly<PropsWithChildren<IModalProps>>): JSX.Element | null => {
  const modal = (): JSX.Element => {
    return (
      <ModalBackground>
        <ModalContainer
          $size={size}
          aria-label={modalRef.name}
          aria-modal={"true"}
          data-testid={id}
          id={id}
        >
          {!_.isEmpty(title) && (
            <ModalHeader
              description={description}
              modalRef={modalRef}
              onClose={onClose}
              otherActions={otherActions}
              title={title}
            />
          )}
          {children}
          {_.isObject(content) && _.isString(content.imageSrc) && (
            <ImageContainer $framed={content.imageFramed}>
              <CloudImage alt={"modal-img"} publicId={content.imageSrc} />
            </ImageContainer>
          )}
          {_.isObject(content) && _.isString(content.imageText) && (
            <Text mb={1.5} size={"sm"}>
              {content.imageText}
            </Text>
          )}
          <ModalFooter
            cancelButton={cancelButton}
            confirmButton={confirmButton}
            modalRef={modalRef}
          />
        </ModalContainer>
      </ModalBackground>
    );
  };

  if (_portal && modalRef.isOpen) {
    return createPortal(
      modal(),
      document.getElementById("portals") ?? document.body,
    );
  }
  if (modalRef.isOpen) {
    return modal();
  }

  return null;
};

export { Modal, ModalConfirm };
