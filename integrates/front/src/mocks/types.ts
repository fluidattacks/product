interface IMessage {
  message: string;
}

interface IErrorMessage {
  errors: [IMessage];
}

export type { IErrorMessage, IMessage };
