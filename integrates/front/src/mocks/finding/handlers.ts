import { GraphQLError } from "graphql";
import includes from "lodash/includes";
import { HttpResponse, type StrictResponse, graphql } from "msw";

import { GET_FINDING_TITLE_AND_ORG } from "features/vulnerabilities/treatment-modal/update-treatment/queries";
import type {
  GetFindingConsultingQuery as GetFindingConsulting,
  GetFindingHeaderQuery as GetFindingHeader,
  GetFindingInfoQuery as GetFindingInfo,
  GetFindingObservationsQuery as GetFindingObservations,
  GetFindingTitleAndOrgQuery as GetFindingTitle,
  GetFindingTrackingQuery as GetFindingTracking,
} from "gql/graphql";
import { FindingStateStatus } from "gql/graphql";
import { LINK } from "mocks/constants";
import type { IErrorMessage } from "mocks/types";
import {
  GET_FINDING_CONSULTING,
  GET_FINDING_OBSERVATIONS,
} from "pages/finding/consulting/queries";
import { GET_FINDING_HEADER } from "pages/finding/queries";
import { GET_FINDING_TRACKING } from "pages/finding/tracking/queries";
import { GET_FINDING_INFO } from "pages/finding/vulnerabilities/queries";

const graphqlMocked = graphql.link(LINK);
export const findingHandlers = [
  graphqlMocked.query(
    GET_FINDING_TRACKING,
    (): StrictResponse<{ data: GetFindingTracking }> => {
      const testJustification = "test justification accepted treatment";

      return HttpResponse.json({
        data: {
          finding: {
            __typename: "Finding",
            id: "422286126",
            tracking: [
              {
                accepted: 0,
                acceptedUndefined: 0,
                assigned: null,
                cycle: 0,
                date: "2018-09-28",
                justification: null,
                safe: 0,
                vulnerable: 1,
              },
              {
                accepted: 1,
                acceptedUndefined: 0,
                assigned: "test@test.test",
                cycle: 1,
                date: "2019-01-08",
                justification: testJustification,
                safe: 0,
                vulnerable: 0,
              },
            ],
          },
        },
      });
    },
  ),
  graphqlMocked.query(
    GET_FINDING_CONSULTING,
    ({
      variables,
    }): StrictResponse<IErrorMessage | { data: GetFindingConsulting }> => {
      const { findingId } = variables;
      if (findingId === "422286126") {
        return HttpResponse.json({
          data: {
            finding: {
              __typename: "Finding",
              consulting: [
                {
                  __typename: "Consult",
                  ...{
                    content: "Consult comment",
                    created: "2019/12/04 08:13:53",
                    email: "unittest@fluidattacks.com",
                    fullName: "Test User",
                    id: "1337260012345",
                    modified: "2019/12/04 08:13:53",
                    parentComment: "0",
                  },
                },
                {
                  __typename: "Consult",
                  ...{
                    content: "Consult comment two",
                    created: "2019/12/04 08:14:53",
                    email: "unittest@fluidattacks.com",
                    fullName: "Test User",
                    id: "1337260012346",
                    modified: "2019/12/04 08:14:53",
                    parentComment: "0",
                  },
                },
              ],
              id: "422286126",
            },
          },
        });
      }
      if (findingId === "413372600") {
        return HttpResponse.json({
          data: {
            finding: {
              __typename: "Finding",
              consulting: [],
              id: "413372600",
            },
          },
        });
      }

      return HttpResponse.json({
        errors: [new GraphQLError("Error getting finding consult")],
      });
    },
  ),
  graphqlMocked.query(
    GET_FINDING_OBSERVATIONS,
    ({
      variables,
    }): StrictResponse<IErrorMessage | { data: GetFindingObservations }> => {
      const { findingId } = variables;
      if (findingId === "422286126") {
        return HttpResponse.json({
          data: {
            finding: {
              __typename: "Finding",
              id: "413372600",
              observations: [
                {
                  __typename: "Consult",
                  ...{
                    content: "Observation comment",
                    created: "2019/12/04 08:13:53",
                    email: "unittest@fluidattacks.com",
                    fullName: "Test User",
                    id: "1337260012345",
                    modified: "2019/12/04 08:13:53",
                    parentComment: "0",
                  },
                },
              ],
            },
          },
        });
      }

      return HttpResponse.json({
        errors: [new GraphQLError("Error getting finding observation")],
      });
    },
  ),
  graphqlMocked.query(
    GET_FINDING_TITLE_AND_ORG,
    ({
      variables,
    }): StrictResponse<IErrorMessage | { data: GetFindingTitle }> => {
      const { findingId } = variables;
      if (findingId === "F3F42d73-c1bf-47c5-954e-FFFFFFFFFFFF") {
        return HttpResponse.json({
          data: {
            finding: {
              __typename: "Finding",
              id: findingId,
              organizationName: "org-test",
              title: "001. Test draft title",
            },
          },
        });
      }

      if (findingId === "436992569") {
        return HttpResponse.json({
          data: {
            finding: {
              __typename: "Finding",
              id: findingId,
              organizationName: "org-test",
              title: "001. Test finding title",
            },
          },
        });
      }

      if (includes(["480857698", "422286126"], findingId)) {
        return HttpResponse.json({
          data: {
            finding: {
              __typename: "Finding",
              id: findingId,
              organizationName: "org-test",
              title: "038. Business information leak",
            },
          },
        });
      }

      return HttpResponse.json({
        errors: [new GraphQLError("Error getting finding title")],
      });
    },
  ),
  graphqlMocked.query(
    GET_FINDING_INFO,
    ({
      variables,
    }): StrictResponse<IErrorMessage | { data: GetFindingInfo }> => {
      const findings = ["438679960", "422286126", "1", ""];
      const { findingId } = variables;
      if (findings.includes(findingId)) {
        return HttpResponse.json({
          data: {
            finding: {
              __typename: "Finding",
              assignees: [],
              id: findingId,
              locations: [],
              releaseDate: "2019-05-08",
              remediated: false,
              status: "VULNERABLE",
              verified: false,
            },
          },
        });
      }

      return HttpResponse.json({
        errors: [new GraphQLError("Error getting finding info")],
      });
    },
  ),
  graphqlMocked.query(
    GET_FINDING_HEADER,
    ({
      variables,
    }): StrictResponse<IErrorMessage | { data: GetFindingHeader }> => {
      const { canRetrieveHacker, findingId } = variables;
      if (findingId === "422286126" && canRetrieveHacker === false) {
        return HttpResponse.json({
          data: {
            finding: {
              __typename: "Finding",
              currentState: FindingStateStatus.Created,
              id: "ab25380d-dfe1-4cde-aefd-acca6990d6aa",
              maxOpenSeverityScore: 0.0,
              maxOpenSeverityScoreV4: 0.0,
              minTimeToRemediate: 10,
              releaseDate: null,
              status: "VULNERABLE",
              title: "",
              vulnerabilitiesSummary: {
                closed: 0,
                open: 3,
                openCritical: 1,
                openHigh: 1,
                openLow: 1,
                openMedium: 1,
              },
            },
          },
        });
      }
      if (findingId === "438679960" && canRetrieveHacker === true) {
        return HttpResponse.json({
          data: {
            finding: {
              __typename: "Finding",
              currentState: FindingStateStatus.Created,
              hacker: "",
              id: "438679960",
              maxOpenSeverityScore: 2.9,
              maxOpenSeverityScoreV4: 2.7,
              minTimeToRemediate: 60,
              releaseDate: null,
              status: "VULNERABLE",
              title: "050. Guessed weak credentials",
              vulnerabilitiesSummary: {
                closed: 0,
                open: 3,
                openCritical: 1,
                openHigh: 1,
                openLow: 1,
                openMedium: 1,
              },
            },
          },
        });
      }

      if (findingId === "438679960" && canRetrieveHacker === false) {
        return HttpResponse.json({
          data: {
            finding: {
              __typename: "Finding",
              currentState: FindingStateStatus.Created,
              id: "438679960",
              maxOpenSeverityScore: 3.0,
              maxOpenSeverityScoreV4: 2.8,
              minTimeToRemediate: 60,
              releaseDate: "2018-12-04 09:04:13",
              status: "VULNERABLE",
              title: "050. Guessed weak credentials",
              vulnerabilitiesSummary: {
                closed: 0,
                open: 3,
                openCritical: 1,
                openHigh: 1,
                openLow: 1,
                openMedium: 1,
              },
            },
          },
        });
      }

      return HttpResponse.json({
        errors: [new GraphQLError("Error getting finding header")],
      });
    },
  ),
];
