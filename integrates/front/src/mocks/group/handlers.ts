/* eslint-disable max-lines */
import { GraphQLError } from "graphql";
import _ from "lodash";
import { HttpResponse, type StrictResponse, graphql } from "msw";

import { GET_GROUP_BILLING } from "features/add-user-modal/confirm-invitation/queries";
import { GET_GROUP_ROOTS } from "features/empty-vulnerabilities/queries";
import { GET_GROUP_USERS } from "features/vulnerabilities/queries";
import { GET_POLICIES } from "features/vulnerabilities/treatment-modal/update-treatment/queries";
import type {
  GetAuthorsQuery as GetAuthors,
  GetEventsQuery as GetEvents,
  GetFilesQueryQuery as GetFiles,
  GetFindingsQueryQuery as GetFindings,
  GetGitRootDockerImagesQuery as GetGitRootDockerImages,
  GetGroupAccessInfoQuery as GetGroupAccessInfo,
  GetGroupBillingQuery as GetGroupBilling,
  GetGroupDataQuery as GetGroupData,
  GetGroupDataQueryQuery as GetGroupDataRoute,
  GetGroupDockerImagesQuery as GetGroupDockerImages,
  GetGroupEventStatusQuery as GetGroupEventStatus,
  GetGroupFindingsPriorityQuery,
  GetGroupPoliciesQuery,
  GetGroupRootsQuery as GetGroupRoots,
  GetGroupServicesQuery as GetGroupServices,
  GetGroupUnfulfilledStandardsQuery as GetGroupUnfulfilledStandards,
  GetGroupUsersQuery as GetGroupUsers,
  GetHooksQueryQuery as GetHooks,
  GetLanguageQueryQuery as GetLanguage,
  GetPoliciesQuery,
  GetRootNicknamesQuery as GetRoots,
  GetStakeholdersQuery as GetStakeholders,
  GetTagsQueryQuery as GetTags,
  RequestGroupReportAtComplianceStandardsQuery as GetUnfulfilledStandardReportURL,
  GetUrlRootExcludedSubPathsQuery as GetUrlRootExcludedSubPaths,
  UnsubscribeFromGroupMutationMutation as UnsubscribeFromGroup,
} from "gql/graphql";
import {
  HookEventType,
  InvitationState,
  Language,
  ManagedType,
  ServiceType,
} from "gql/graphql";
import { GET_GROUP_SERVICES } from "hooks/queries";
import { LINK } from "mocks/constants";
import type { IErrorMessage } from "mocks/types";
import { GET_LANGUAGE } from "pages/finding/description/queries";
import { GET_GROUP_FINDINGS_PRIORITY } from "pages/finding/vulnerabilities/queries";
import { GET_GROUP_POLICIES } from "pages/group/dev-sec-ops/queries";
import { GET_EVENTS } from "pages/group/events/queries";
import { GET_FINDINGS } from "pages/group/findings/queries";
import { GET_AUTHORS } from "pages/group/queries";
import { GET_STAKEHOLDERS } from "pages/group/queries";
import {
  GET_GROUP_DATA as GET_GROUP_DATA_ROUTE,
  GET_GROUP_EVENT_STATUS,
} from "pages/group/queries";
import {
  GET_FILES,
  GET_GIT_ROOT_DOCKER_IMAGES,
  GET_GROUP_ACCESS_INFO,
  GET_GROUP_DATA,
  GET_GROUP_DOCKER_IMAGES,
  GET_HOOKS,
  GET_ROOTS,
  GET_TAGS,
  GET_URL_ROOT_EXCLUDED_SUB_PATHS,
} from "pages/group/scope/queries";
import { UNSUBSCRIBE_FROM_GROUP_MUTATION } from "pages/group/scope/unsubscribe/modal/queries";
import {
  GET_GROUP_UNFULFILLED_STANDARDS,
  GET_UNFULFILLED_STANDARD_REPORT_URL,
} from "pages/organization/compliance/unfilled-standards/queries";

const graphqlMocked = graphql.link(LINK);
export const groupHandlers = [
  graphqlMocked.query(
    GET_UNFULFILLED_STANDARD_REPORT_URL,
    (): StrictResponse<{ data: GetUnfulfilledStandardReportURL }> => {
      return HttpResponse.json({
        data: {
          unfulfilledStandardReportUrl: "test",
        },
      });
    },
  ),
  graphqlMocked.query(
    GET_GROUP_FINDINGS_PRIORITY,
    (): StrictResponse<{ data: GetGroupFindingsPriorityQuery }> => {
      return HttpResponse.json({
        data: {
          group: {
            __typename: "Group",
            name: "testgroup",
            totalOpenPriority: 0,
          },
        },
      });
    },
  ),
  graphqlMocked.query(
    GET_GROUP_UNFULFILLED_STANDARDS,
    (): StrictResponse<{ data: GetGroupUnfulfilledStandards }> => {
      return HttpResponse.json({
        data: {
          group: {
            __typename: "Group",
            compliance: {
              __typename: "GroupCompliance",
              unfulfilledStandards: [
                {
                  __typename: "UnfulfilledStandard",
                  standardId: "standardId1",
                  title: "standardname1",
                  unfulfilledRequirements: [
                    {
                      __typename: "Requirement",
                      id: "001",
                      title: "requirement1",
                    },
                    {
                      __typename: "Requirement",
                      id: "002",
                      title: "requirement2",
                    },
                  ],
                },
                {
                  __typename: "UnfulfilledStandard",
                  standardId: "standardId2",
                  title: "standardname2",
                  unfulfilledRequirements: [
                    {
                      __typename: "Requirement",
                      id: "001",
                      title: "requirement1",
                    },
                    {
                      __typename: "Requirement",
                      id: "003",
                      title: "requirement3",
                    },
                  ],
                },
              ],
            },
            name: "group1",
          },
        },
      });
    },
  ),
  graphqlMocked.query(
    GET_GROUP_POLICIES,
    ({
      variables,
    }): StrictResponse<IErrorMessage | { data: GetGroupPoliciesQuery }> => {
      const groups = ["unittesting", "TEST"];
      const { groupName } = variables;

      if (groups.includes(groupName)) {
        return HttpResponse.json({
          data: {
            group: {
              __typename: "Group",
              daysUntilItBreaks: null,
              maxAcceptanceDays: null,
              maxAcceptanceSeverity: 10,
              maxNumberAcceptances: null,
              minAcceptanceSeverity: 0,
              minBreakingSeverity: 0,
              name: groupName,
              vulnerabilityGracePeriod: 1,
            },
          },
        });
      }

      return HttpResponse.json({
        errors: [new GraphQLError("An error occurred")],
      });
    },
  ),
  graphqlMocked.query(
    GET_AUTHORS,
    ({ variables }): StrictResponse<IErrorMessage | { data: GetAuthors }> => {
      const { groupName } = variables;

      if (groupName === "unittesting") {
        return HttpResponse.json({
          data: {
            group: {
              __typename: "Group",
              billing: {
                authors: [
                  {
                    actor: "test",
                    commit: "123",
                    groups: ["test, test2"],
                    organization: "testorg",
                    repository: "test_repository",
                  },
                ],
              },
              name: "unittesting",
            },
          },
        });
      }

      return HttpResponse.json({
        errors: [new GraphQLError("Error getting authors")],
      });
    },
  ),
  graphqlMocked.query(
    GET_STAKEHOLDERS,
    ({
      variables,
    }): StrictResponse<IErrorMessage | { data: GetStakeholders }> => {
      const { groupName } = variables;

      if (groupName === "unittesting") {
        return HttpResponse.json({
          data: {
            group: {
              __typename: "Group",
              name: "unittesting",
              stakeholders: [
                {
                  email: "user@gmail.com",
                  firstLogin: "2017-09-05 15:00:00",
                  invitationState: InvitationState.Registered,
                  lastLogin: "2017-10-29 13:40:37",
                  responsibility: "Rest responsibility",
                  role: "user",
                },
              ],
            },
          },
        });
      }
      if (groupName === "TEST") {
        return HttpResponse.json({
          data: {
            group: {
              __typename: "Group",
              name: "TEST",
              stakeholders: [
                {
                  __typename: "Stakeholder",
                  email: "user@gmail.com",
                  firstLogin: "2017-09-05 15:00:00",
                  invitationState: InvitationState.Registered,
                  lastLogin: "2017-10-29 13:40:37",
                  responsibility: "Test responsibility",
                  role: "user",
                },
                {
                  __typename: "Stakeholder",
                  email: "user2@gmail.com",
                  firstLogin: "2018-09-05 15:00:00",
                  invitationState: InvitationState.Registered,
                  lastLogin: "2018-11-29 13:40:37",
                  responsibility: "To be a great",
                  role: "user manager",
                },
              ],
            },
          },
        });
      }

      return HttpResponse.json({
        errors: [new GraphQLError("Error getting stakeholders")],
      });
    },
  ),
  graphqlMocked.query(
    GET_HOOKS,
    ({ variables }): StrictResponse<IErrorMessage | { data: GetHooks }> => {
      const { groupName } = variables;

      if (groupName === "TEST") {
        return HttpResponse.json({
          data: {
            group: {
              __typename: "Group",
              hook: [
                {
                  entryPoint: "www.client1.com",
                  hookEvents: [HookEventType.VulnerabilityCreated],
                  id: "id1",
                  name: "Name1",
                  token: "tokenClient1",
                  tokenHeader: "Basic",
                },
                {
                  entryPoint: "www.client2.com",
                  hookEvents: [HookEventType.VulnerabilityCreated],
                  id: "id2",
                  name: "Name1",
                  token: "tokenClient2",
                  tokenHeader: "Basic",
                },
              ],
              name: "TEST",
            },
          },
        });
      }

      return HttpResponse.json({
        errors: [new GraphQLError("Error getting hooks")],
      });
    },
  ),
  graphqlMocked.query(
    GET_GROUP_ACCESS_INFO,
    ({
      variables,
    }): StrictResponse<IErrorMessage | { data: GetGroupAccessInfo }> => {
      const { groupName } = variables;

      if (groupName === "TEST") {
        return HttpResponse.json({
          data: {
            group: {
              __typename: "Group",
              disambiguation: "-",
              groupContext: "-",
              name: "TEST",
            },
          },
        });
      }

      return HttpResponse.json({
        errors: [new GraphQLError("Error group access info")],
      });
    },
  ),
  graphqlMocked.mutation(
    UNSUBSCRIBE_FROM_GROUP_MUTATION,
    ({
      variables,
    }): StrictResponse<IErrorMessage | { data: UnsubscribeFromGroup }> => {
      const { groupName } = variables;

      if (groupName === "TEST") {
        return HttpResponse.json({
          data: {
            unsubscribeFromGroup: {
              success: true,
            },
          },
        });
      }

      return HttpResponse.json({
        errors: [new GraphQLError("Error updating stakeholder access")],
      });
    },
  ),
  graphqlMocked.query(
    GET_GROUP_SERVICES,
    ({
      variables,
    }): StrictResponse<IErrorMessage | { data: GetGroupServices }> => {
      const groups = ["TEST", "unittesting", "testgroup"];
      const { groupName } = variables;

      if (groups.includes(groupName)) {
        return HttpResponse.json({
          data: {
            group: {
              __typename: "Group",
              name: groupName,
              serviceAttributes: [],
            },
          },
        });
      }

      return HttpResponse.json({
        errors: [new GraphQLError("Error getting group services")],
      });
    },
  ),
  graphqlMocked.query(
    GET_GROUP_DATA,
    ({ variables }): StrictResponse<IErrorMessage | { data: GetGroupData }> => {
      const groups = ["test", "unittesting"];
      const { groupName } = variables;

      if (groups.includes(groupName)) {
        return HttpResponse.json({
          data: {
            group: {
              __typename: "Group",
              businessId: "",
              businessName: "",
              description: "Integrates unit test project",
              hasAdvanced: true,
              hasEssential: true,
              language: Language.En,
              managed: ManagedType.Managed,
              name: groupName,
              service: ServiceType.White,
              sprintDuration: 1,
              sprintStartDate: "",
              subscription: "CoNtInUoUs",
            },
          },
        });
      }
      if (groupName === "oneshottest") {
        return HttpResponse.json({
          data: {
            group: {
              __typename: "Group",
              businessId: "",
              businessName: "",
              description: "",
              hasAdvanced: false,
              hasEssential: false,
              language: Language.En,
              managed: ManagedType.Managed,
              name: "oneshottest",
              service: ServiceType.Black,
              sprintDuration: 1,
              sprintStartDate: "2022-06-06T00:00:00",
              subscription: "OnEsHoT",
            },
          },
        });
      }
      if (groupName === "not-exists") {
        return HttpResponse.json({
          data: {
            group: {
              __typename: "Group",
              businessId: "",
              businessName: "",
              description: "",
              hasAdvanced: false,
              hasEssential: false,
              language: Language.En,
              managed: ManagedType.Managed,
              name: "not-exists",
              service: ServiceType.Black,
              sprintDuration: 1,
              sprintStartDate: "2022-06-06T00:00:00",
              subscription: "OnEsHoT",
            },
          },
        });
      }
      if (groupName === "testgroup") {
        return HttpResponse.json({
          data: {
            group: {
              __typename: "Group",
              businessId: "",
              businessName: "",
              description: "Integrates unit test project",
              hasAdvanced: true,
              hasEssential: true,
              language: Language.En,
              managed: ManagedType.UnderReview,
              name: groupName,
              service: ServiceType.White,
              sprintDuration: 1,
              sprintStartDate: "",
              subscription: "CoNtInUoUs",
            },
          },
        });
      }

      return HttpResponse.json({
        errors: [new GraphQLError("Error getting group events")],
      });
    },
  ),
  graphqlMocked.query(
    GET_GROUP_EVENT_STATUS,
    ({
      variables,
    }): StrictResponse<IErrorMessage | { data: GetGroupEventStatus }> => {
      const groups = ["test", "testgroup"];
      const { groupName } = variables;

      if (groups.includes(groupName)) {
        return HttpResponse.json({
          data: {
            group: {
              __typename: "Group",
              events: [],
              name: groupName,
            },
          },
        });
      }

      return HttpResponse.json({
        errors: [new GraphQLError("Error getting group events")],
      });
    },
  ),
  graphqlMocked.query(
    GET_GROUP_DATA_ROUTE,
    ({
      variables,
    }): StrictResponse<IErrorMessage | { data: GetGroupDataRoute }> => {
      const groups = ["test", "unittesting"];
      const { groupName, organizationName } = variables;

      if (groups.includes(groupName)) {
        return HttpResponse.json({
          data: {
            group: {
              __typename: "Group",
              managed: ManagedType.NotManaged,
              name: groupName,
              organization: "test",
              permissions: [],
              serviceAttributes: ["has_asm"],
            },
            organizationId: {
              __typename: "Organization",
              id: "ORG#test",
              name: organizationName,
            },
          },
        });
      }

      return HttpResponse.json({
        errors: [new GraphQLError("Error getting group services")],
      });
    },
  ),
  graphqlMocked.query(
    GET_GROUP_USERS,
    ({
      variables,
    }): StrictResponse<IErrorMessage | { data: GetGroupUsers }> => {
      const { groupName } = variables;

      if (groupName === "testgroupname") {
        return HttpResponse.json({
          data: {
            group: {
              __typename: "Group",
              name: "testgroupname",
              stakeholders: [
                {
                  email: "manager_test@test.test",
                  invitationState: InvitationState.Registered,
                  role: "user",
                },
              ],
            },
          },
        });
      }

      return HttpResponse.json({
        errors: [new GraphQLError("Error getting group stakeholders")],
      });
    },
  ),
  graphqlMocked.query(
    GET_LANGUAGE,
    ({ variables }): StrictResponse<IErrorMessage | { data: GetLanguage }> => {
      const groups = ["TEST", "testgroup"];
      const { groupName } = variables;

      if (_.includes(groups, groupName)) {
        return HttpResponse.json({
          data: {
            group: {
              __typename: "Group",
              language: Language.En,
              name: groupName,
            },
          },
        });
      }

      return HttpResponse.json({
        errors: [new GraphQLError("Error getting group languages")],
      });
    },
  ),
  graphqlMocked.query(
    GET_FINDINGS,
    ({ variables }): StrictResponse<IErrorMessage | { data: GetFindings }> => {
      const groups = ["TEST", "testgroup"];
      const {
        canGetRejectedVulnerabilities,
        canGetSubmittedVulnerabilities,
        root,
        groupName,
      } = variables;

      if (
        (!canGetRejectedVulnerabilities &&
          !canGetSubmittedVulnerabilities &&
          _.includes(groups, groupName)) ||
        _.isEqual(root, "") ||
        _.isEqual(root, undefined)
      ) {
        return HttpResponse.json({
          data: {
            ...{
              criteriaConnection: {
                edges: [],
                pageInfo: {
                  endCursor: "001",
                  hasNextPage: false,
                },
                total: 1,
              },
            },
            group: {
              __typename: "Group" as const,
              businessId: "id",
              businessName: "name",
              description: "description",
              findings: [],
              hasEssential: false,
              language: Language.En,
              managed: ManagedType.Managed,
              name: groupName,
              totalOpenPriority: 0,
              userRole: "user-role",
            },
          },
        });
      }

      return HttpResponse.json({
        errors: [new GraphQLError("Error getting group findings")],
      });
    },
  ),
  graphqlMocked.query(
    GET_GROUP_ROOTS,
    ({
      variables,
    }): StrictResponse<IErrorMessage | { data: GetGroupRoots }> => {
      const groups = ["unittesting", "testgroup"];
      const { groupName } = variables;

      if (_.includes(groups, groupName)) {
        return HttpResponse.json({
          data: {
            group: {
              __typename: "Group",
              closedVulnerabilities: 0,
              description: "Test group",
              hasAdvanced: false,
              hasEssential: true,
              managed: ManagedType.Managed,
              name: groupName,
              openFindings: 0,
              openVulnerabilities: 0,
              roots: [],
              service: ServiceType.White,
              subscription: "Essential",
              userRole: "testRole",
            },
          },
        });
      }

      return HttpResponse.json({
        errors: [new GraphQLError("Error getting group roots")],
      });
    },
  ),
  graphqlMocked.query(
    GET_GROUP_BILLING,
    ({
      variables,
    }): StrictResponse<IErrorMessage | { data: GetGroupBilling }> => {
      const { groupName } = variables;
      if (groupName === "TEST") {
        return HttpResponse.json({
          data: {
            group: {
              __typename: "Group",
              billing: {
                __typename: "GroupBilling",
                authors: [{ actor: "Test <test@test.com>" }],
              },
              name: "TEST",
            },
          },
        });
      }

      return HttpResponse.json({
        errors: [new GraphQLError("Error getting group billing")],
      });
    },
  ),
  graphqlMocked.query(GET_FILES, (): StrictResponse<{ data: GetFiles }> => {
    return HttpResponse.json({
      data: {
        resources: {
          __typename: "Resource",
          files: [],
        },
      },
    });
  }),
  graphqlMocked.query(
    GET_TAGS,
    ({ variables }): StrictResponse<IErrorMessage | { data: GetTags }> => {
      const { groupName } = variables;
      if (groupName === "TEST") {
        return HttpResponse.json({
          data: {
            group: {
              __typename: "Group",
              name: "TEST",
              tags: ["test"],
            },
          },
        });
      }

      return HttpResponse.json({
        errors: [new GraphQLError("Error getting group tags")],
      });
    },
  ),
  graphqlMocked.query(
    GET_URL_ROOT_EXCLUDED_SUB_PATHS,
    ({
      variables,
    }): StrictResponse<
      IErrorMessage | { data: GetUrlRootExcludedSubPaths }
    > => {
      const { groupName, rootId } = variables;
      if (
        groupName === "group" &&
        rootId === "c7118ab2-9345-4398-895f-11ce6e3cc1a6"
      ) {
        return HttpResponse.json({
          data: {
            root: {
              __typename: "URLRoot",
              excludedSubPaths: ["subPath/1"],
              id: "c7118ab2-9345-4398-895f-11ce6e3cc1a6",
            },
          },
        });
      }

      return HttpResponse.json({
        errors: [new GraphQLError("Error getting url roots data")],
      });
    },
  ),
  graphqlMocked.query(
    GET_ROOTS,
    ({ variables }): StrictResponse<IErrorMessage | { data: GetRoots }> => {
      const { groupName } = variables;
      if (groupName === "TEST") {
        return HttpResponse.json({
          data: {
            group: {
              __typename: "Group",
              name: groupName,
              roots: [],
            },
          },
        });
      }

      return HttpResponse.json({
        errors: [new GraphQLError("Error getting roots")],
      });
    },
  ),
  graphqlMocked.query(
    GET_POLICIES,
    (): StrictResponse<{ data: GetPoliciesQuery }> => {
      return HttpResponse.json({
        data: {
          __typename: "Query",
          group: {
            __typename: "Group",
            maxAcceptanceDays: 60,
            maxAcceptanceSeverity: 10,
            maxNumberAcceptances: 3,
            minAcceptanceSeverity: 0,
            name: "testgroupname",
          },
        },
      });
    },
  ),
  graphqlMocked.query(GET_EVENTS, (): StrictResponse<{ data: GetEvents }> => {
    return HttpResponse.json({
      data: {
        group: {
          __typename: "Group",
          events: [],
          name: "unittesting",
        },
      },
    });
  }),
  graphqlMocked.query(
    GET_GROUP_DOCKER_IMAGES,
    ({
      variables,
    }): StrictResponse<IErrorMessage | { data: GetGroupDockerImages }> => {
      const { groupName } = variables;

      return HttpResponse.json({
        data: {
          group: {
            __typename: "Group",
            dockerImages: [],
            name: groupName,
          },
        },
      });
    },
  ),
  graphqlMocked.query(
    GET_GIT_ROOT_DOCKER_IMAGES,
    ({
      variables,
    }): StrictResponse<IErrorMessage | { data: GetGitRootDockerImages }> => {
      const { rootId } = variables;

      return HttpResponse.json({
        data: {
          root: {
            __typename: "GitRoot",
            dockerImages: [],
            id: rootId,
          },
        },
      });
    },
  ),
];
