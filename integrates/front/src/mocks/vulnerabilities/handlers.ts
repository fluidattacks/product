import { GraphQLError } from "graphql";
import { HttpResponse, type StrictResponse, graphql } from "msw";

import { GET_VULN_SEVERITY_INFO } from "features/vulnerabilities/severity-info/queries";
import { SEND_ASSIGNED_NOTIFICATION } from "features/vulnerabilities/treatment-modal/update-treatment/queries";
import {
  type GetVulnSeverityInfoQuery as GetVulnSeverityInfo,
  type SendAssignedNotificationMutation as SendAssignedNotification,
  VulnerabilityState,
} from "gql/graphql";
import { LINK } from "mocks/constants";
import type { IErrorMessage } from "mocks/types";

const graphqlMocked = graphql.link(LINK);
export const vulnerabilitiesHandlers = [
  graphqlMocked.query(
    GET_VULN_SEVERITY_INFO,
    ({
      variables,
    }): StrictResponse<IErrorMessage | { data: GetVulnSeverityInfo }> => {
      const { vulnId } = variables;

      if (vulnId === "af7a48b8-d8fc-41da-9282-d424fff563f0") {
        return HttpResponse.json({
          data: {
            vulnerability: {
              __typename: "Vulnerability",
              id: vulnId,
              severityTemporalScore: 3.8,
              severityThreatScore: 3.9,
              severityVector:
                "CVSS:3.1/AV:N/AC:L/PR:L/UI:N/S:U/C:N/I:L/A:N/E:U/RL:O/RC:C",
              severityVectorV4:
                "CVSS:4.0/AV:N/AC:L/AT:N/PR:L/UI:N/VC:N/VI:L/VA:N/SC:N/SI:N/SA:N/E:U",
              state: VulnerabilityState.Vulnerable,
            },
          },
        });
      }

      return HttpResponse.json({
        errors: [
          new GraphQLError("An error occurred fetching vulnerability severity"),
        ],
      });
    },
  ),
  graphqlMocked.mutation(
    SEND_ASSIGNED_NOTIFICATION,
    (): StrictResponse<{ data: SendAssignedNotification }> => {
      return HttpResponse.json({
        data: {
          sendAssignedNotification: { success: true },
        },
      });
    },
  ),
];
