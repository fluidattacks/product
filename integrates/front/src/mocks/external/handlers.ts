import { HttpResponse, http } from "msw";

import { COUNTRIES_URL } from "utils/countries";

export const externalHandlers = [
  http.get(COUNTRIES_URL, (): HttpResponse => {
    return HttpResponse.json(
      [
        {
          id: "CO",
          name: "Colombia",
        },
      ],
      { status: 200 },
    );
  }),
];
