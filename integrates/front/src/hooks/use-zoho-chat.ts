import { useEffect, useState } from "react";

interface ISalesIQ {
  floatwindow: { visible: (arg: "hide" | "show") => void };
  values?: { embedHideDetails?: { outsideBusinessHours: boolean } };
}

interface IZoho {
  $zoho?: {
    salesiq?: ISalesIQ;
  };
}

interface IUseZohoChat {
  openChat: () => void;
  isChatOnline: boolean;
}
const SECONDS_IN_MIN = 60;
const MS_PROPORTIONALITY = 1000;
const availabilityInterval = SECONDS_IN_MIN * MS_PROPORTIONALITY;

const useZohoChat = (): IUseZohoChat => {
  const [isChatOnline, setIsChatOnline] = useState<boolean>(false);
  const { $zoho } = window as IZoho & typeof window;

  const openChat = (): void => {
    if ($zoho?.salesiq) {
      $zoho.salesiq.floatwindow.visible("show");
    }
  };

  useEffect((): (() => void) => {
    const checkChatAvailability = (): void => {
      if ($zoho?.salesiq?.values?.embedHideDetails) {
        const { embedHideDetails } = $zoho.salesiq.values;
        const { outsideBusinessHours } = embedHideDetails;
        setIsChatOnline(!outsideBusinessHours);
      } else {
        setIsChatOnline(false);
      }
    };

    checkChatAvailability();

    const intervalId = setInterval(checkChatAvailability, availabilityInterval);

    return (): void => {
      clearInterval(intervalId);
    };
  }, [$zoho]);

  return { isChatOnline, openChat };
};

export { useZohoChat };
