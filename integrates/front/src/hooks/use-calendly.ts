import { useQuery } from "@apollo/client";
import { useCallback, useContext, useState } from "react";
import { useTranslation } from "react-i18next";
import { useMatch } from "react-router-dom";

import { GET_GROUP_SERVICES } from "./queries";

import { authContext } from "context/auth";
import { Logger } from "utils/logger";
import { msgError } from "utils/notifications";

interface IWidgetProps {
  prefill: {
    customAnswers: { a1: string };
    email: string;
    name: string;
  };
  url: string;
}

const useCalendly = (): {
  closeUpgradeModal: () => void;
  isAdvancedActive: boolean;
  isAvailable: boolean;
  isUpgradeOpen: boolean;
  openUpgradeModal: () => void;
  widgetProps: IWidgetProps;
} => {
  const routeMatch = useMatch("/orgs/:orgName/groups/:groupName/*");
  const groupName = routeMatch?.params.groupName ?? "";

  const { userEmail, userName } = useContext(authContext);
  const [isUpgradeOpen, setIsUpgradeOpen] = useState(false);
  const { t } = useTranslation();

  const { data } = useQuery(GET_GROUP_SERVICES, {
    fetchPolicy: "cache-first",
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error): void => {
        if (error.message === "Service unavailable, please retry") {
          msgError(t("groupAlerts.retryInAMoment"));
        } else {
          Logger.error("An error occurred fetching group services", error);
        }
      });
    },
    skip: routeMatch === null,
    variables: { groupName },
  });

  const isAvailable = routeMatch !== null && data !== undefined;

  const isAdvancedActive = isAvailable
    ? data.group.serviceAttributes.includes("has_advanced") &&
      data.group.serviceAttributes.includes("is_continuous")
    : false;

  const openUpgradeModal = useCallback((): void => {
    setIsUpgradeOpen(true);
  }, []);

  const closeUpgradeModal = useCallback((): void => {
    setIsUpgradeOpen(false);
  }, []);

  const widgetProps = {
    prefill: {
      customAnswers: { a1: groupName },
      email: userEmail,
      name: userName,
    },
    url: "https://calendly.com/fluidattacks/talk-to-a-hacker",
  };

  return {
    closeUpgradeModal,
    isAdvancedActive,
    isAvailable,
    isUpgradeOpen,
    openUpgradeModal,
    widgetProps,
  };
};

export { useCalendly };
