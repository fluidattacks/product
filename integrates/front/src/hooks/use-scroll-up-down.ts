import { useEffect, useState } from "react";

interface IScrollUpDown {
  scrollUpCallback: VoidFunction;
  scrollDownCallback: VoidFunction;
  upThreshold?: number;
  downThreshold?: number;
}

/**
 * Custom hook to handle scroll up and scroll down events.
 *
 * @param {Object} params - The parameters object.
 * @param {Function} params.scrollUpCallback
 *  - Callback function to be executed when scrolling up.
 * @param {Function} params.scrollDownCallback
 *  - Callback function to be executed when scrolling down.
 * @param {number} [params.upThreshold=0]
 * - The threshold point from the top of the container to trigger the scroll up
 * callback. It could be a percentage or a number of pixels. F
 * For percentage use values between 0 and 1.
 * @param {number} [params.downThreshold=0]
 * - The threshold point from the top of the container to trigger the scroll
 * down callback. It could be a percentage or a number of pixels.
 * For percentage use values between 0 and 1.
 *
 * @returns {void}
 */
const useScrollUpDown = ({
  scrollUpCallback,
  scrollDownCallback,
  upThreshold = 0,
  downThreshold = 0,
}: IScrollUpDown): void => {
  const [lastScrollPosition, setLastScrollPosition] = useState(0);

  useEffect((): VoidFunction => {
    const container =
      document.getElementById("dashboard") ?? document.documentElement;

    const containerHeight = container.clientHeight;

    const handleScroll = (): void => {
      const currentScrollPosition =
        document.getElementById("dashboard")?.scrollTop ?? window.scrollY;

      const upThresholdAsNumberOfPixels =
        upThreshold <= 1 ? containerHeight * upThreshold : upThreshold;
      const downThresholdANumberOfPixels =
        downThreshold <= 1 ? containerHeight * downThreshold : downThreshold;

      const shouldExecuteScrollUpCallback =
        currentScrollPosition < lastScrollPosition &&
        currentScrollPosition >= upThresholdAsNumberOfPixels;

      const shouldExecuteScrollDownCallback =
        currentScrollPosition > lastScrollPosition &&
        currentScrollPosition >= downThresholdANumberOfPixels;

      if (shouldExecuteScrollUpCallback) {
        scrollUpCallback();
      } else if (shouldExecuteScrollDownCallback) scrollDownCallback();

      setLastScrollPosition(currentScrollPosition);
    };

    container.addEventListener("scroll", handleScroll);

    return (): void => {
      container.removeEventListener("scroll", handleScroll);
    };
  }, [
    lastScrollPosition,
    scrollUpCallback,
    scrollDownCallback,
    downThreshold,
    upThreshold,
  ]);
};

export { useScrollUpDown };
