import { useMutation } from "@apollo/client";
import capitalize from "lodash/capitalize";
import type { RefObject } from "react";
import { useCallback, useEffect, useRef } from "react";

import { ADD_AUDIT_EVENT } from "./queries";

import type { IEventEvidenceAttr } from "pages/event/evidence/types";
import type { IEvidenceItem } from "pages/finding/evidence/types";

interface IUseEvidenceAudit {
  addEvidenceAudit: (
    evidences: Record<string, IEvidenceItem | IEventEvidenceAttr>,
    fileName: string,
    instance: {
      id: string;
      name: "Event" | "Finding";
    },
    videoViewStatus?: "start" | "half" | "end",
  ) => void;
}

interface IUseAudit {
  addAuditEvent: (object: string, objectId: string) => void;
}

interface IUseObserverAudit {
  ref: RefObject<HTMLDivElement>;
}

const OBSERVER_DELAY = 1000;

const useAudit = (): IUseAudit => {
  const [addAuditEventMutation] = useMutation(ADD_AUDIT_EVENT, {
    fetchPolicy: "no-cache",
  });

  const addAuditEvent = useCallback(
    (object: string, objectId: string): void => {
      void addAuditEventMutation({ variables: { object, objectId } });
    },
    [addAuditEventMutation],
  );

  return { addAuditEvent };
};

const useEvidenceAudit = (): IUseEvidenceAudit => {
  const { addAuditEvent } = useAudit();
  const addEvidenceAudit = useCallback(
    (
      evidences: Record<string, IEvidenceItem | IEventEvidenceAttr>,
      fileName: string,
      instance: {
        id: string;
        name: "Event" | "Finding";
      },
      videoViewStatus?: "start" | "half" | "end",
    ): void => {
      const filteredEvidenceEntry = Object.entries(evidences).find(
        ([_, evidence]: [
          string,
          IEvidenceItem | IEventEvidenceAttr,
        ]): boolean => {
          if ("url" in evidence) {
            return evidence.url === fileName;
          }
          if ("fileName" in evidence) {
            return evidence.fileName === fileName;
          }

          return false;
        },
      );

      if (filteredEvidenceEntry) {
        const [key] = filteredEvidenceEntry;
        const getObjectName = (): string => {
          const baseName = `${instance.name}.Evidence.${capitalize(key)}`;
          if (videoViewStatus) {
            return `${baseName}.${capitalize(videoViewStatus)}`;
          }

          return baseName;
        };
        addAuditEvent(getObjectName(), instance.id);
      }
    },
    [addAuditEvent],
  );

  return { addEvidenceAudit };
};

const useObserverAudit = (
  object: string,
  objectId: string,
): IUseObserverAudit => {
  const ref = useRef<HTMLDivElement>(null);
  const { addAuditEvent } = useAudit();

  useEffect((): (() => void) => {
    const currentRef = ref.current;
    const startObserver = (): IntersectionObserver => {
      const observer = new IntersectionObserver(
        ([entry]): void => {
          if (entry.isIntersecting) {
            addAuditEvent(object, objectId);
          }
        },
        {
          threshold: 0.9,
        },
      );

      if (currentRef) {
        observer.observe(currentRef);
      }

      return observer;
    };

    const timeoutId = setTimeout(startObserver, OBSERVER_DELAY);

    return (): void => {
      clearTimeout(timeoutId);
    };
  }, [addAuditEvent, object, objectId]);

  return { ref };
};

export { useAudit, useEvidenceAudit, useObserverAudit };
