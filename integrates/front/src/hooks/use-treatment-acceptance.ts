import type { IconName } from "@fortawesome/free-solid-svg-icons";

import type { IVulnRowAttr } from "features/vulnerabilities/types";
import type { VulnerabilityFiltersInput } from "gql/graphql";
import {
  VulnerabilityState,
  VulnerabilityTreatment,
  VulnerabilityZeroRiskStatus,
} from "gql/graphql";

type TTreatmentAcceptanceAction =
  | "ACCEPTED_UNDEFINED"
  | "ACCEPTED"
  | "none"
  | "CONFIRM_REJECT_ZERO_RISK"
  | "CONFIRM_REJECT_VULNERABILITY";

interface ITreatmentAcceptanceOptions {
  extraCondition: boolean;
  disabled: boolean;
  header: string;
  icon: IconName;
  value: string;
}

interface ITreatmentAcceptanceActionsDisabled {
  acceptedUndefined: boolean;
  accepted: boolean;
  confirmRejectZeroRisk: boolean;
  confirmRejectVulnerability: boolean;
}

const getTreatmentDisabled = (
  vulns: IVulnRowAttr[],
): ITreatmentAcceptanceActionsDisabled => {
  return vulns.reduce<ITreatmentAcceptanceActionsDisabled>(
    (
      result: ITreatmentAcceptanceActionsDisabled,
      row: IVulnRowAttr,
    ): ITreatmentAcceptanceActionsDisabled => {
      if (row.treatmentAcceptanceStatus === VulnerabilityState.Submitted) {
        switch (row.treatmentStatus.toUpperCase()) {
          case "ACCEPTED":
            return { ...result, accepted: false };

          case "ACCEPTED_UNDEFINED":
            return { ...result, acceptedUndefined: false };

          default:
            break;
        }
      }
      if (row.zeroRisk?.toUpperCase() === "REQUESTED") {
        return { ...result, confirmRejectZeroRisk: false };
      }

      if (row.state.toUpperCase() === "SUBMITTED") {
        return { ...result, confirmRejectVulnerability: false };
      }

      return result;
    },
    {
      accepted: true,
      acceptedUndefined: true,
      confirmRejectVulnerability: true,
      confirmRejectZeroRisk: true,
    },
  );
};

const getFilter = (value: string): VulnerabilityFiltersInput => {
  switch (value) {
    case "ACCEPTED":
      return {
        treatment: [VulnerabilityTreatment.Accepted],
      };
    case "ACCEPTED_UNDEFINED":
      return {
        treatment: [VulnerabilityTreatment.AcceptedUndefined],
      };
    case "CONFIRM_REJECT_ZERO_RISK":
      return {
        zeroRisk: [VulnerabilityZeroRiskStatus.Requested],
      };
    case "CONFIRM_REJECT_VULNERABILITY":
      return {
        state: [VulnerabilityState.Submitted],
      };
    default:
      return {};
  }
};

export { getFilter, getTreatmentDisabled };
export type {
  TTreatmentAcceptanceAction,
  ITreatmentAcceptanceOptions,
  ITreatmentAcceptanceActionsDisabled,
};
