import { create } from "zustand";

interface IHelpState {
  isLearningOpen: boolean;
  isTalkToHackerOpen: boolean;
}
interface IHelpAction {
  setIsLearningOpen: (
    isLearningModalOpen: IHelpState["isLearningOpen"],
  ) => void;
  setIsTalkToHackerOpen: (
    isTalkToHackerOpen: IHelpState["isTalkToHackerOpen"],
  ) => void;
}
type THelpButtonsState = IHelpAction & IHelpState;

const useHelpState = create<THelpButtonsState>()(
  (set): THelpButtonsState => ({
    isLearningOpen: false,
    isTalkToHackerOpen: false,
    setIsLearningOpen: (isLearningOpen): void => {
      set((): Partial<IHelpState> => ({ isLearningOpen }));
    },
    setIsTalkToHackerOpen: (isTalkToHackerOpen): void => {
      set((): Partial<IHelpState> => ({ isTalkToHackerOpen }));
    },
  }),
);

export type { THelpButtonsState, IHelpState };
export { useHelpState };
