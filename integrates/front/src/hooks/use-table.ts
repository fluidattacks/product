import type {
  ColumnOrderState,
  ColumnPinningState,
  PaginationState,
  SortingState,
  VisibilityState,
} from "@tanstack/react-table";

import { useStoredState } from "./use-stored-state";

interface IUseTable {
  columnOrder?: ColumnOrderState;
  columnPinning: ColumnPinningState;
  columnVisibility: VisibilityState;
  defaultColumnOrder?: ColumnOrderState;
  id: string;
  pagination: PaginationState;
  setColumnOrder: React.Dispatch<React.SetStateAction<ColumnOrderState>>;
  setColumnVisibility: React.Dispatch<React.SetStateAction<VisibilityState>>;
  setPagination: React.Dispatch<React.SetStateAction<PaginationState>>;
  setSorting: React.Dispatch<React.SetStateAction<SortingState>>;
  sorting: SortingState;
}

const useTable = (
  id: string,
  defaultColumnVisibility?: VisibilityState,
  defaultColumnOrder?: ColumnOrderState,
  defaultColumnPinning?: ColumnPinningState,
  defaultPagination?: PaginationState,
): IUseTable => {
  const [, setColumnVisibility] = useStoredState<VisibilityState>(
    `${id}-visibilityState`,
    defaultColumnVisibility ?? {},
    localStorage,
  );
  const [, setColumnOrder] = useStoredState<ColumnOrderState>(
    `${id}-orderState`,
    defaultColumnOrder ?? [],
    localStorage,
  );

  const [sorting, setSorting] = useStoredState<SortingState>(
    `${id}-sortingState`,
    [],
  );

  const [pagination, setPagination] = useStoredState<PaginationState>(
    `${id}-paginationState`,
    defaultPagination ?? { pageIndex: 0, pageSize: 10 },
  );

  const storedPinning = sessionStorage.getItem(`${id}-columnPinningState`);
  const storedOrder = localStorage.getItem(`${id}-orderState`);
  const storedVisibility = localStorage.getItem(`${id}-visibilityState`);

  const columnPinning =
    storedPinning === null
      ? defaultColumnPinning
      : (JSON.parse(storedPinning) as ColumnPinningState);
  const columnOrder =
    storedOrder === null
      ? defaultColumnOrder
      : (JSON.parse(storedOrder) as ColumnOrderState);
  const columnVisibility =
    storedVisibility === null
      ? defaultColumnVisibility
      : (JSON.parse(storedVisibility) as VisibilityState);

  return {
    columnOrder,
    columnPinning: columnPinning ?? { left: [], right: [] },
    columnVisibility: columnVisibility ?? {},
    defaultColumnOrder,
    id,
    pagination,
    setColumnOrder,
    setColumnVisibility,
    setPagination,
    setSorting,
    sorting,
  };
};

export type { IUseTable };
export { useTable };
