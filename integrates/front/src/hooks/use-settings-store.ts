import { create } from "zustand";

type TCVSSVersion = "v3.1" | "v4.0";
interface ICVSSVersionObject {
  header: string;
  maxOpenSeverityKey: "maxOpenSeverityScore" | "maxOpenSeverityScoreV4";
  scoreKey: "severityTemporalScore" | "severityThreatScore";
  summaryKey: "vulnerabilitiesSummary" | "vulnerabilitiesSummaryV4";
  value: TCVSSVersion;
}

interface ISettingsState {
  cvssVersion: ICVSSVersionObject;
}

interface ISettingsSetters {
  setCvssVersion: (value: TCVSSVersion) => void;
}

const initialState: ISettingsState = {
  cvssVersion: {
    header: "Severity (v4.0)",
    maxOpenSeverityKey: "maxOpenSeverityScoreV4",
    scoreKey: "severityThreatScore",
    summaryKey: "vulnerabilitiesSummaryV4",
    value: "v4.0",
  },
};

const useSettingsStore = create<ISettingsState & ISettingsSetters>()(
  (set): ISettingsState & ISettingsSetters => ({
    ...initialState,
    setCvssVersion: (value: TCVSSVersion): void => {
      set(
        (): Partial<ISettingsState> => ({
          cvssVersion:
            value === "v3.1"
              ? {
                  header: "Severity (v3.1)",
                  maxOpenSeverityKey: "maxOpenSeverityScore",
                  scoreKey: "severityTemporalScore",
                  summaryKey: "vulnerabilitiesSummary",
                  value: "v3.1",
                }
              : {
                  header: "Severity (v4.0)",
                  maxOpenSeverityKey: "maxOpenSeverityScoreV4",
                  scoreKey: "severityThreatScore",
                  summaryKey: "vulnerabilitiesSummaryV4",
                  value: "v4.0",
                },
        }),
      );
    },
  }),
);

export type { ICVSSVersionObject, TCVSSVersion };
export { useSettingsStore };
