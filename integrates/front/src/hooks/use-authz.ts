import { useAbility } from "@casl/react";
import { useCallback } from "react";

import { authzPermissionsContext } from "context/authz/config";
import { authzGroupContext } from "context/authz/config";

interface IAuthzResponse {
  /**
   * Verify if current user is allowed to do a mutation.
   *
   * You can find permissions with the prefix ``integrates_api_mutations_``.
   */
  readonly canMutate: (permission: string) => boolean;
  /**
   * Verify if current user is allowed to check a resolver.
   *
   * You can find permissions with the prefix ``integrates_api_resolvers_``.
   */
  readonly canResolve: (permission: string) => boolean;
  /**
   * Verify if current user is allowed to do something. If you want to check
   * a mutation or resolver permission, use `canMutate` or `canResolve` instead.
   */
  readonly can: (permission: string) => boolean;
  /**
   * Verify if current group is allowed to do something. If you want to check
   * a mutation or resolver permission, use `canMutate` or `canResolve` instead.
   */
  readonly hasAttribute: (permission: string) => boolean;
}

const useAuthz = (): IAuthzResponse => {
  const permissions = useAbility(authzPermissionsContext);
  const groupPermissions = useAbility(authzGroupContext);

  const canMutate = useCallback(
    (permission: string): boolean =>
      permissions.can(`integrates_api_mutations_${permission}_mutate`),
    [permissions],
  );

  const canResolve = useCallback(
    (permission: string): boolean =>
      permissions.can(`integrates_api_resolvers_${permission}_resolve`),
    [permissions],
  );

  const can = useCallback(
    (permission: string): boolean => permissions.can(permission),
    [permissions],
  );

  const hasAttribute = useCallback(
    (permission: string): boolean => groupPermissions.can(permission),
    [groupPermissions],
  );

  return {
    can,
    canMutate,
    canResolve,
    hasAttribute,
  };
};

export { useAuthz };
