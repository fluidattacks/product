/* eslint-disable @typescript-eslint/no-empty-object-type */
import type { ColumnOrderState } from "@tanstack/react-table";
import { ColumnMeta } from "@tanstack/react-table";

declare module "@tanstack/table-core" {
  interface TableMeta<TData extends RowData> {
    appliedFilters?: JSX.Element;
    data?: Map<string, TData>;
    defaultColumnOrder?: ColumnOrderState;
    filters?: Record<string, string>;
    filtersComponent?: JSX.Element;
    variant?: "default" | "minimal";
    onSearch?: (search: string) => void;
    openFiltersHandler?: () => void;
  }
}

declare module "@tanstack/react-table" {
  interface ColumnMeta {
    filterType: "dateRange" | "number" | "numberRange" | "select" | "text";
  }

  interface CustomOptions<TData extends RowData> {
    onRowClick?: (row: Row<TData>) => void;
  }

  interface TableOptionsResolved<TData extends RowData>
    extends CustomOptions<TData> {}
}
