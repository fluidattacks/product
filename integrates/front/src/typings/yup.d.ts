// Needed for compatibility with yup validations

import type { AnyObject, Flags, Maybe, Schema, TestContext } from "yup";

// eslint-disable-next-line @typescript-eslint/no-empty-object-type
interface AnyPresentValue {}

export type TDateValidationType =
  | "datetime"
  | "greaterDate"
  | "greaterThanDate"
  | "lowerDate"
  | "validDateToken";

declare module "yup" {
  interface StringSchema<
    TType extends Maybe<string> = string | undefined,
    TContext = AnyObject,
    TDefault = undefined,
    TFlags extends Flags = "",
  > extends Schema<TType, TContext, TDefault, TFlags> {
    isValidArnFormat: (message?: string) => this;
    isValidDraftTitle: (message?: string) => this;
    isValidExcludeFormat: (repoUrl?: string, message?: string) => this;
    isValidFindingTypology: (
      titleSuggestions: string[],
      message?: string,
    ) => this;
    isValidFunction: (
      fn: (
        value: string | undefined,
        options: TestContext<Maybe<AnyObject>>,
      ) => boolean,
      message: string,
    ) => this;
    isValidTextBeginning: (message?: string, regex?: RegExp) => this;
    isValidTextField: (message?: string, regex?: RegExp) => this;
    isValidTextPattern: (message?: string, regex?: RegExp) => this;
    isValidValue: (message?: string, regex?: RegExp) => this;
    isValidUrlProtocolOrSSHFormat: (message?: string) => this;
    isValidSSHFormat: (
      regex: RegExp,
      formItems: AnyObject,
      message?: string,
    ) => this;
    isValidDate: (type: TDateValidationType) => this;
    isValidLength: (min: number, max: number, message?: string) => this;
    isValidBoolean: (booleanValue: boolean, message: string) => this;
    isValidCustomRegexValidation: (
      regex: RegExp,
      message: string,
      i18nKey: string,
    ) => this;
    isValidDynamicValidation: (
      validationFn: (
        value: string | undefined,
        context: TestContext<Maybe<AnyObject>>,
      ) => ValidationError | boolean,
    ) => this;
  }

  interface MixedSchema<
    TType extends Maybe<AnyPresentValue> = AnyPresentValue | undefined,
    TContext = AnyObject,
    TDefault = undefined,
    TFlags extends Flags = "",
  > extends Schema<TType, TContext, TDefault, TFlags> {
    isValidFileName: (
      groupName?: string,
      organizationName?: string,
      message?: string,
    ) => this;
    isValidFileSize: (message?: string, maxFileSize?: number) => this;
    isValidFileType: (extensions: string[], message?: string) => this;
    isValidFunction: (
      fn: (
        value: AnyPresentValue | undefined,
        options: TestContext<Maybe<AnyObject>>,
      ) => boolean,
      message: string,
    ) => this;
    isValidPhoneNumber: (message?: string) => this;
    isValidMaxNumberFiles: (maxNumberFiles: number, message?: string) => this;
    isValidDate: (
      type: TDateValidationType,
      maxWeeksCreatedDate?: number,
    ) => this;
  }
  interface NumberSchema<
    TType extends Maybe<number> = number | undefined,
    TContext = AnyObject,
    TDefault = undefined,
    TFlags extends Flags = "",
  > extends Schema<TType, TContext, TDefault, TFlags> {
    isValidFunction: (
      fn: (
        value: number | undefined,
        options: TestContext<Maybe<AnyObject>>,
      ) => boolean,
      message: string,
    ) => this;
  }

  interface ArraySchema<
    TType extends Maybe<AnyObject[] | AnyPresentValue[]> =
      | AnyObject[]
      | undefined,
    TContext = AnyObject,
    TDefault = undefined,
    TFlags extends Flags = "",
  > extends Schema<TType, TContext, TDefault, TFlags> {
    isValidFunction: (
      fn: (
        value: AnyObject[] | AnyPresentValue[] | undefined,
        options: TestContext<AnyObject>,
      ) => boolean,
      message: string,
    ) => this;
  }
}

export type { AnyPresentValue };
