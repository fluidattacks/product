import { graphql } from "gql";

const GET_N_GROUPS = graphql(`
  query GetNGroups($organizationId: String!) {
    organization(organizationId: $organizationId) {
      name
      nGroups
    }
  }
`);

const REMOVE_STAKEHOLDER_MUTATION = graphql(`
  mutation RemoveStakeholderAccessMutation(
    $groupName: String!
    $userEmail: String!
  ) {
    removeStakeholderAccess(groupName: $groupName, userEmail: $userEmail) {
      removedEmail
      success
    }
  }
`);

const ADD_STAKEHOLDER_MUTATION = graphql(`
  mutation GrantStakeholderMutation(
    $email: String!
    $groupName: String!
    $responsibility: String
    $role: StakeholderRole!
  ) {
    grantStakeholderAccess(
      email: $email
      groupName: $groupName
      responsibility: $responsibility
      role: $role
    ) {
      success
      grantedStakeholder {
        email
        firstLogin
        lastLogin
        responsibility
        role
      }
    }
  }
`);

const UPDATE_GROUP_STAKEHOLDER_MUTATION = graphql(`
  mutation UpdateGroupStakeholderMutation(
    $email: String!
    $groupName: String!
    $responsibility: String!
    $role: StakeholderRole!
  ) {
    updateGroupStakeholder(
      email: $email
      groupName: $groupName
      responsibility: $responsibility
      role: $role
    ) {
      success
    }
  }
`);

export {
  GET_N_GROUPS,
  REMOVE_STAKEHOLDER_MUTATION,
  ADD_STAKEHOLDER_MUTATION,
  UPDATE_GROUP_STAKEHOLDER_MUTATION,
};
