import dayjs from "dayjs";
import _ from "lodash";
import { useCallback, useContext, useEffect, useMemo, useState } from "react";
import { useTranslation } from "react-i18next";

import { authContext } from "context/auth";
import { AddUserModal } from "features/add-user-modal";
import type { IStakeholderFormValues } from "features/add-user-modal/types";
import type { GetStakeholdersQuery } from "gql/graphql";
import { useAuthors } from "pages/group/hooks";
import type { IStakeholderDataSet } from "pages/organization/members/types";

const DATE_RANGE = 12;

interface IMembersAlertProps {
  closeUserModal: () => void;
  currentRow: IStakeholderDataSet[];
  data: GetStakeholdersQuery;
  groupName: string;
  handleSubmit: (values: IStakeholderFormValues) => Promise<void>;
  isUserModalOpen: boolean;
  userModalAction: "add" | "edit";
}

export const AddModal = ({
  closeUserModal,
  currentRow,
  data,
  groupName,
  handleSubmit,
  isUserModalOpen,
  userModalAction,
}: Readonly<IMembersAlertProps>): JSX.Element => {
  const { t } = useTranslation();
  const { userEmail } = useContext(authContext);

  const dateRange = useMemo((): string[] => {
    return _.range(0, DATE_RANGE).map((month): string =>
      dayjs().subtract(month, "month").toISOString(),
    );
  }, []);

  const [emailSuggestion, setEmailSuggestion] = useState<string[]>([]);
  const [domainSuggestion, setDomainSuggestion] = useState<string[]>([]);
  const [authorsDate, setAuthorsDate] = useState(dateRange[0]);
  const { data: dataAuthor } = useAuthors(groupName, authorsDate, true, false);

  const authorDate = useCallback((): void => {
    if (dataAuthor !== undefined && dateRange.length > 1) {
      if (
        dataAuthor.group.billing.authors.length === 0 &&
        authorsDate !== dateRange[1]
      ) {
        setAuthorsDate(dateRange[1]);
      }
    }
  }, [authorsDate, dataAuthor, dateRange]);
  useEffect((): void => {
    const emailStakeholder = data.group.stakeholders.map(
      (stakeholder): string => stakeholder.email ?? "",
    );
    const authorsEmail = (dataAuthor?.group.billing.authors ?? []).map(
      ({ actor }): string => {
        const place = actor.lastIndexOf("<");

        return place >= 0
          ? actor.substring(place + 1, actor.length - 1)
          : actor;
      },
    );

    const authorsNotStakeholder = authorsEmail.filter(
      (value): boolean => !emailStakeholder.includes(value),
    );
    const domains = Array.from(
      new Set(
        [...emailStakeholder, ...authorsEmail].map((email): string => {
          const [, emailDomain] = email.split("@");
          if (userEmail.endsWith("@fluidattacks.com")) {
            return emailDomain;
          }

          return emailDomain === "fluidattacks.com" ? "" : emailDomain;
        }),
      ),
    );
    setEmailSuggestion(Array.from(new Set(authorsNotStakeholder)));
    setDomainSuggestion(domains.filter((domain): boolean => domain !== ""));

    authorDate();
  }, [authorDate, data, dateRange, dataAuthor, userEmail]);

  return (
    <AddUserModal
      action={userModalAction}
      domainSuggestions={userModalAction === "edit" ? [] : domainSuggestion}
      editTitle={t("searchFindings.tabUsers.editStakeholderTitle")}
      groupName={groupName}
      initialValues={userModalAction === "edit" ? currentRow[0] : undefined}
      onClose={closeUserModal}
      onSubmit={handleSubmit}
      open={isUserModalOpen}
      suggestions={userModalAction === "edit" ? [] : emailSuggestion}
      title={t("searchFindings.tabUsers.title")}
      type={"group"}
    />
  );
};
