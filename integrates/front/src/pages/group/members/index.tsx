/* eslint-disable max-lines */
import { useMutation, useQuery } from "@apollo/client";
import {
  AppliedFilters,
  Button,
  Container,
  EmptyState,
  Link,
  useConfirmDialog,
} from "@fluidattacks/design";
import type { ColumnDef } from "@tanstack/react-table";
import type { ExecutionResult } from "graphql";
import _ from "lodash";
import mixpanel from "mixpanel-browser";
import { Fragment, useCallback, useContext, useMemo, useState } from "react";
import * as React from "react";
import { useTranslation } from "react-i18next";
import { useParams } from "react-router-dom";
import { useTheme } from "styled-components";

import { ActionsButtons } from "./action-buttons";
import { AddModal } from "./add-modal";
import { MembersAlert } from "./alert";
import { useMembersFilters } from "./filters";
import {
  ADD_STAKEHOLDER_MUTATION,
  GET_N_GROUPS,
  REMOVE_STAKEHOLDER_MUTATION,
  UPDATE_GROUP_STAKEHOLDER_MUTATION,
} from "./queries";
import type {
  IRemoveStakeholderAttr,
  IStakeholderAttrs,
  IStakeholderDataSet,
} from "./types";
import {
  getAreAllMutationValid,
  handleEditError,
  handleGrantError,
  handleRemoveError,
  removeMultipleAccess,
} from "./utils";

import { useStakeholders } from "../hooks";
import { Table } from "components/table";
import { timeFromNow } from "components/table/table-formatters";
import { authContext } from "context/auth";
import { Can } from "context/authz/can";
import { AddUserModal } from "features/add-user-modal";
import type { IStakeholderFormValues } from "features/add-user-modal/types";
import { statusFormatter } from "features/vulnerabilities/formatters/status";
import type { StakeholderRole } from "gql/graphql";
import { InvitationState } from "gql/graphql";
import { useDailyAlert, useStoredState, useTable } from "hooks";
import { organizationDataContext } from "pages/organization/context";
import { formatDate } from "utils/date";
import { Logger } from "utils/logger";
import { msgError, msgSuccess } from "utils/notifications";

const PADDING_A = 0.25;
const PADDING_B = 1.25;

const GroupMembers: React.FC = (): JSX.Element => {
  const { t } = useTranslation();
  const { organizationId } = useContext(organizationDataContext);
  const theme = useTheme();

  const tableRef = useTable("tblUsers");
  const { confirm, ConfirmDialog } = useConfirmDialog();
  const { groupName } = useParams() as { groupName: string };
  const baseRolesUrl =
    "https://help.fluidattacks.com/portal/en/kb/articles/understand-roles";
  const { userEmail } = useContext(authContext);

  // State management
  const useAlert = useDailyAlert("members", groupName);
  const [currentRow, setCurrentRow] = useState<IStakeholderDataSet[]>([]);
  const [isUserModalOpen, setIsUserModalOpen] = useState(false);
  const [userModalAction, setUserModalAction] = useState<"add" | "edit">("add");
  const [filteredData, setFilteredData] = useState<IStakeholderDataSet[]>([]);
  const openAddUserModal = useCallback((): void => {
    setUserModalAction("add");
    setIsUserModalOpen(true);
  }, []);
  const openEditUserModal = useCallback((): void => {
    setUserModalAction("edit");
    setIsUserModalOpen(true);
  }, []);
  const closeUserModal = useCallback((): void => {
    setIsUserModalOpen(false);
  }, []);

  const roleToUrl = (role: string, anchor: string): JSX.Element => {
    return (
      <Link href={`${baseRolesUrl}${anchor}`} iconPosition={"hidden"}>
        {t(`userModal.roles.${_.camelCase(role)}`, {
          defaultValue: "-",
        })}
      </Link>
    );
  };

  const tableColumns: ColumnDef<IStakeholderDataSet>[] = [
    {
      accessorKey: "email",
      header: t("searchFindings.usersTable.usermail"),
    },
    {
      accessorKey: "role",
      cell: (cell): JSX.Element | string => {
        const mappedRole = {
          // eslint-disable-next-line camelcase
          group_manager: roleToUrl(
            String(cell.getValue()),
            "#Group_Manager_role",
          ),
          user: roleToUrl(String(cell.getValue()), "#User_role"),
          // eslint-disable-next-line camelcase
          vulnerability_manager: roleToUrl(
            String(cell.getValue()),
            "#Vulnerability_Manager_role",
          ),
        }[String(cell.getValue())];

        if (!_.isUndefined(mappedRole)) {
          return mappedRole;
        }

        return t(`userModal.roles.${_.camelCase(String(cell.getValue()))}`, {
          defaultValue: "-",
        });
      },
      header: t("searchFindings.usersTable.userRole"),
    },
    {
      accessorKey: "responsibility",
      header: t("searchFindings.usersTable.userResponsibility"),
    },
    {
      accessorKey: "firstLogin",
      cell: (cell): string => formatDate(String(cell.getValue() ?? "")),
      header: t("searchFindings.usersTable.firstlogin"),
    },
    {
      accessorKey: "lastLogin",
      cell: (cell): string => timeFromNow(String(cell.getValue())),
      header: t("searchFindings.usersTable.lastlogin"),
    },
    {
      accessorKey: "invitationState",
      cell: (cell): JSX.Element => statusFormatter(String(cell.getValue())),
      header: t("searchFindings.usersTable.invitationState"),
    },
    {
      accessorKey: "invitationResend",
      cell: (cell): JSX.Element => cell.getValue<JSX.Element>(),
      header: t("searchFindings.usersTable.invitation"),
    },
  ];

  const [lastOrganization, setLastOrganization] = useStoredState(
    "organizationV2",
    { deleted: false, name: "" },
    localStorage,
  );

  // GraphQL operations
  const {
    data,
    refetch,
    loading: loadingStakeholders,
  } = useStakeholders(groupName);
  const { data: nGroupsResult } = useQuery(GET_N_GROUPS, {
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error): void => {
        msgError(t("groupAlerts.errorTextsad"));
        Logger.error("An error occurred loading organization groups", error);
      });
    },
    variables: { organizationId },
  });
  const {
    organization: { nGroups },
  } = nGroupsResult ?? { organization: { nGroups: -1 } };
  const [grantStakeholderAccess, { loading: gratingAccess }] = useMutation(
    ADD_STAKEHOLDER_MUTATION,
    {
      onCompleted: (mtResult): void => {
        if (mtResult.grantStakeholderAccess.success) {
          refetch().catch((): void => {
            Logger.error("An error occurred adding stakeholder");
          });
          mixpanel.track("AddUserAccess");
          const { email } =
            mtResult.grantStakeholderAccess.grantedStakeholder ?? {};
          msgSuccess(
            `${t("searchFindings.tabUsers.success")} ${email}`,
            t("searchFindings.tabUsers.titleSuccess"),
          );
          setCurrentRow([]);
        }
      },
      onError: (grantError): void => {
        handleGrantError(grantError);
      },
    },
  );

  const [updateGroupStakeholder] = useMutation(
    UPDATE_GROUP_STAKEHOLDER_MUTATION,
    {
      onCompleted: (mtResult): void => {
        if (mtResult.updateGroupStakeholder.success) {
          setUserModalAction("add");
          refetch().catch((): void => {
            Logger.error("An error occurred updating stakeholder group");
          });

          mixpanel.track("EditUserAccess");
          msgSuccess(
            t("searchFindings.tabUsers.successAdmin"),
            t("searchFindings.tabUsers.titleSuccess"),
          );
          setCurrentRow([]);
        }
      },
      onError: (editError): void => {
        handleEditError(editError, refetch);
      },
    },
  );

  const [removeStakeholderAccess, { loading: removing }] = useMutation(
    REMOVE_STAKEHOLDER_MUTATION,
    {
      onError: (error): void => {
        handleRemoveError(error);
      },
    },
  );

  const handleSubmit = useCallback(
    async (values: IStakeholderFormValues): Promise<void> => {
      closeUserModal();
      if (userModalAction === "add") {
        await grantStakeholderAccess({
          variables: {
            email: values.email,
            groupName,
            responsibility: values.responsibility,
            role: values.role as StakeholderRole,
          },
        });
      } else {
        await updateGroupStakeholder({
          variables: {
            email: values.email,
            groupName,
            responsibility: values.responsibility ?? "",
            role: values.role as StakeholderRole,
          },
        });
      }
    },
    [
      closeUserModal,
      updateGroupStakeholder,
      grantStakeholderAccess,
      groupName,
      userModalAction,
    ],
  );

  const validMutationsHelper = useCallback(
    (areAllMutationValid: boolean[]): void => {
      if (areAllMutationValid.every(Boolean)) {
        mixpanel.track("RemoveUserAccess");

        if (areAllMutationValid.length === 1) {
          msgSuccess(
            `${currentRow[0]?.email} ${t(
              "searchFindings.tabUsers.successDelete",
            )}`,
            t("searchFindings.tabUsers.titleSuccess"),
          );
        } else {
          msgSuccess(
            t("searchFindings.tabUsers.successDeletePlural"),
            t("searchFindings.tabUsers.titleSuccess"),
          );
        }
      }
    },
    [currentRow, t],
  );

  const handleRemoveUser = useCallback(async (): Promise<void> => {
    const results = await removeMultipleAccess(
      removeStakeholderAccess as (
        variables: Record<string, unknown>,
      ) => Promise<ExecutionResult<IRemoveStakeholderAttr>>,
      currentRow,
      groupName,
    );
    validMutationsHelper(getAreAllMutationValid(results));
    if (
      nGroups === 1 &&
      currentRow.findIndex(
        (stakeholder): boolean => stakeholder.email === userEmail,
      ) > -1
    ) {
      setLastOrganization({ ...lastOrganization, deleted: true });
    }
    setCurrentRow([]);
    await refetch();
    setUserModalAction("add");
  }, [
    currentRow,
    nGroups,
    groupName,
    refetch,
    removeStakeholderAccess,
    validMutationsHelper,
    lastOrganization,
    setLastOrganization,
    userEmail,
  ]);

  const handleClick = useCallback(async (): Promise<void> => {
    const users = currentRow.map((user): string => user.email).join(", ");
    const confirmResult = await confirm({
      message: `${users} ${t(
        "searchFindings.tabUsers.removeUserButton.confirmMessage",
      )}`,
      title: t("searchFindings.tabUsers.removeUserButton.confirmTitle"),
    });
    if (confirmResult) {
      await handleRemoveUser();
    }
  }, [confirm, currentRow, handleRemoveUser, t]);

  const resendHandler = useCallback(
    (
      stakeholder: IStakeholderAttrs,
    ): ((event: React.MouseEvent<HTMLButtonElement>) => void) => {
      return (event: React.MouseEvent<HTMLButtonElement>): void => {
        event.preventDefault();

        grantStakeholderAccess({
          variables: {
            email: stakeholder.email,
            groupName,
            responsibility: stakeholder.responsibility,
            role: stakeholder.role.toUpperCase() as StakeholderRole,
          },
        }).catch((): void => {
          Logger.error("An error occurred in logout");
        });
      };
    },
    [grantStakeholderAccess, groupName],
  );

  const stakeholdersList = useMemo(
    (): IStakeholderDataSet[] =>
      (data?.group.stakeholders ?? []).map(
        (stakeholder): IStakeholderDataSet => {
          const isPending =
            stakeholder.invitationState === InvitationState.Pending;

          return {
            ...(stakeholder as IStakeholderDataSet),
            invitationResend: (
              <Button
                disabled={!isPending || loadingStakeholders || gratingAccess}
                onClick={resendHandler(stakeholder as IStakeholderAttrs)}
                variant={"secondary"}
              >
                {t("searchFindings.usersTable.resendEmail")}
              </Button>
            ),
          };
        },
      ),
    [
      data?.group.stakeholders,
      loadingStakeholders,
      gratingAccess,
      resendHandler,
      t,
    ],
  );

  const filteredStakeholders = stakeholdersList.filter(
    (stakeholder): boolean => {
      return (
        stakeholder.email !== userEmail &&
        (userEmail.endsWith("@fluidattacks.com") ||
          !stakeholder.email.endsWith("@fluidattacks.com"))
      );
    },
  );

  const { Filters, options, removeFilter } = useMembersFilters({
    dataset: stakeholdersList,
    groupName,
    setFilteredDataset: setFilteredData,
  });

  const modal =
    data && userModalAction === "add" && isUserModalOpen ? (
      <AddModal
        closeUserModal={closeUserModal}
        currentRow={currentRow}
        data={data}
        groupName={groupName}
        handleSubmit={handleSubmit}
        isUserModalOpen={isUserModalOpen}
        userModalAction={userModalAction}
      />
    ) : (
      <AddUserModal
        action={userModalAction}
        domainSuggestions={[]}
        editTitle={t("searchFindings.tabUsers.editStakeholderTitle")}
        groupName={groupName}
        initialValues={userModalAction === "edit" ? currentRow[0] : undefined}
        onClose={closeUserModal}
        onSubmit={handleSubmit}
        open={isUserModalOpen}
        suggestions={[]}
        title={t("searchFindings.tabUsers.title")}
        type={"group"}
      />
    );

  if (filteredStakeholders.length <= 0 && !loadingStakeholders) {
    return (
      <React.Fragment>
        <EmptyState
          confirmButton={{
            onClick: openAddUserModal,
            text: t("organization.tabs.users.emptyComponent.addButton.text"),
          }}
          description={t("organization.tabs.users.emptyComponent.description")}
          imageSrc={"integrates/resources/memberIcon"}
          title={t("organization.tabs.users.emptyComponent.title")}
        />
        {modal}
      </React.Fragment>
    );
  }

  return (
    <Fragment>
      <Container
        bgColor={theme.palette.gray[50]}
        height={"100hv"}
        id={"users"}
        padding={[1, PADDING_B, 1, PADDING_B]}
        px={1.25}
        py={1.25}
      >
        {useAlert ? (
          <MembersAlert organizationId={organizationId} />
        ) : undefined}
        <Container
          bgColor={theme.palette.gray[50]}
          padding={[PADDING_A, 0, 0, 0]}
        >
          <Table
            columns={tableColumns}
            csvConfig={{ export: true }}
            data={filteredData}
            extraButtons={
              <ActionsButtons
                areButtonsDisabled={
                  _.isEmpty(currentRow) || removing || loadingStakeholders
                }
                areThereMultipleSelections={currentRow.length > 1}
                handleRemoveUser={handleClick}
                openEditUserModal={openEditUserModal}
              />
            }
            filters={<Filters />}
            filtersApplied={
              <AppliedFilters onClose={removeFilter} options={options} />
            }
            loadingData={loadingStakeholders}
            options={{
              searchPlaceholder: t("organization.members.searchPlaceholder"),
            }}
            rightSideComponents={
              <Can
                do={"integrates_api_mutations_grant_stakeholder_access_mutate"}
              >
                <Button
                  id={"addUser"}
                  onClick={openAddUserModal}
                  tooltip={t("searchFindings.tabUsers.addButton.tooltip")}
                  variant={"primary"}
                >
                  {t("organization.members.addUserButton.text")}
                </Button>
              </Can>
            }
            rowSelectionSetter={setCurrentRow}
            rowSelectionState={currentRow}
            selectionMode={"checkbox"}
            tableRef={tableRef}
          />
        </Container>
        {modal}
      </Container>
      <ConfirmDialog />
    </Fragment>
  );
};

export { GroupMembers };
