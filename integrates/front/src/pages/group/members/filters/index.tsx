import { type IUseFilterProps, useFilters } from "@fluidattacks/design";
import type { IOptionsProps } from "@fluidattacks/design";
import type React from "react";
import { useMemo } from "react";
import { useTranslation } from "react-i18next";

import type { IStakeholderDataSet } from "../types";

const useMembersFilters = ({
  dataset,
  groupName,
  setFilteredDataset,
}: Readonly<{
  dataset: IStakeholderDataSet[];
  groupName: string;
  setFilteredDataset: React.Dispatch<
    React.SetStateAction<IStakeholderDataSet[]>
  >;
}>): IUseFilterProps<IStakeholderDataSet> => {
  const { t } = useTranslation();

  const options: IOptionsProps<IStakeholderDataSet>[] = useMemo(
    (): IOptionsProps<IStakeholderDataSet>[] => [
      {
        filterOptions: [
          {
            key: "role",
            label: t("searchFindings.usersTable.userRole"),
            options: [
              { label: "Admin", value: "admin" },
              { label: "Customer Manager", value: "customer_manager" },
              { label: "Hacker", value: "hacker" },
              { label: "Reattacker", value: "reattacker" },
              { label: "Resourcer", value: "resourcer" },
              { label: "Reviewer", value: "reviewer" },
              { label: "User", value: "user" },
              { label: "Group Manager", value: "group_manager" },
              {
                label: "Vulnerability Manager",
                value: "vulnerability_manager",
              },
            ],
            type: "checkboxes",
          },
        ],
        label: t("searchFindings.usersTable.userRole"),
      },
      {
        filterOptions: [
          {
            key: "invitationState",
            label: t("searchFindings.usersTable.invitationState"),
            options: [
              { label: "Pending", value: "PENDING" },
              { label: "Registered", value: "REGISTERED" },
              { label: "Unregistered", value: "UNREGISTERED" },
            ],
            type: "checkboxes",
          },
        ],
        label: t("searchFindings.usersTable.invitationState"),
      },
    ],
    [t],
  );

  return useFilters({
    dataset,
    localStorageKey: `tblUsersFilters-${groupName}`,
    options,
    setFilteredDataset,
  });
};

export { useMembersFilters };
