import type { TVerifyFn } from "features/verify-dialog/types";
import type { VulnerabilitiesSummary, ZeroRiskSummary } from "gql/graphql";
import type {
  ICVSS3EnvironmentalMetrics,
  ICVSS3TemporalMetrics,
} from "utils/cvss";
import type {
  ICVSS4EnvironmentalMetrics,
  ICVSS4ThreatMetrics,
} from "utils/cvss4";

type TStatus = "DRAFT" | "SAFE" | "VULNERABLE";

interface ITreatmentSummaryAttr {
  accepted: number;
  acceptedUndefined: number;
  inProgress: number;
  untreated: number;
}

interface IVerificationSummaryAttr {
  __typename: "VerificationSummary";
  onHold: number;
  requested: number;
  verified: number;
}

interface ILocationsInfoAttr {
  findingId: string;
  locations: string | undefined;
  treatmentAssignmentEmails: Set<string>;
  vulnerabilitiesSummary: VulnerabilitiesSummary;
}

interface IFindingAttr {
  __typename: "Finding";
  age: number;
  closingPercentage: number;
  description: string;
  hacker: string | null;
  id: string;
  includeTechnique: boolean;
  isExploitable: boolean;
  lastVulnerability: number;
  locationsInfo: ILocationsInfoAttr;
  maxOpenSeverityScore: number;
  maxOpenSeverityScoreV4: number;
  minTimeToRemediate: number | null;
  name: string;
  openAge: number;
  reattack: string;
  rejectedVulnerabilities: number | undefined;
  releaseDate: string | null;
  root: string | null;
  status: TStatus;
  submittedVulnerabilities: number | undefined;
  technique: string | null;
  title: string;
  totalOpenPriority: number;
  treatment: string;
  treatmentSummary: ITreatmentSummaryAttr;
  verificationSummary: IVerificationSummaryAttr;
  verified: boolean;
  vulnerabilitiesSummary: VulnerabilitiesSummary;
  vulnerabilitiesSummaryV4: VulnerabilitiesSummary;
  zeroRiskSummary: ZeroRiskSummary | null;
}

interface IVulnerability {
  findingId: string;
  id: string;
  state: string;
  treatmentAssigned: string | null;
  where: string;
}

interface IGroupVulnerabilities {
  group: {
    name: string;
    vulnerabilities: {
      edges: { node: IVulnerability }[];
      pageInfo: {
        endCursor: string;
        hasNextPage: boolean;
      };
    };
  };
}

interface IVulnerabilitiesResume {
  treatmentAssignmentEmails: Set<string>;
  wheres: string;
}

interface IRoot {
  nickname: string;
  state: "ACTIVE" | "INACTIVE";
}

interface IFindingSuggestionData {
  attackVectorDescription: string;
  code: string;
  description: string;
  recommendation: string;
  minTimeToRemediate: number | null;
  score: ICVSS3TemporalMetrics;
  scoreV4: ICVSS4ThreatMetrics;
  threat: string;
  title: string;
  unfulfilledRequirements: string[];
}

interface IAddFindingFormValues {
  score: ICVSS3EnvironmentalMetrics;
  scoreV4: ICVSS4EnvironmentalMetrics;
  description: string;
  threat: string;
  title: string;
}

interface IDescriptionProps {
  description: string;
  isExploitable: boolean;
  lastVulnerability: number;
  openAge: number;
  status: "DRAFT" | "SAFE" | "VULNERABLE";
  treatment: string;
  verificationSummary: IVerificationSummaryAttr;
}

interface IReportParams {
  age?: number;
  closingDate?: string;
  findingTitle?: string;
  lastReport?: number;
  location?: string;
  maxReleaseDate?: string;
  maxSeverity?: number;
  minReleaseDate?: string;
  minSeverity?: number;
  startClosingDate?: string;
  states: string[];
  treatments?: string[];
  verifications: string[];
  verificationCode: string;
}

interface IFilterFormProps {
  onClose?: () => void;
  requestGroupReport: (reportParams: IReportParams) => Promise<void>;
  setIsVerifyDialogOpen: React.Dispatch<React.SetStateAction<boolean>>;
  setVerifyCallbacks: TVerifyFn;
  typesOptions: string[];
}

interface IFormValues {
  age: number | undefined;
  closingDate: string;
  findingTitle: string;
  lastReport: number | undefined;
  location: string;
  maxReleaseDate: string;
  maxSeverity: number | undefined;
  minReleaseDate: string;
  minSeverity: number | undefined;
  startClosingDate?: string;
  states: string[];
  treatments: string[];
  verifications: string[];
}

interface IDeactivationModalProps {
  isOpen: boolean;
  onClose: () => void;
  typesOptions: string[];
}

interface IVulnerabilityCriteriaLanguage {
  description: string;
  impact: string;
  recommendation: string;
  threat: string;
  title: string;
}

interface IVulnerabilityCriteriaScore {
  base: {
    attackVector: string;
    attackComplexity: string;
    privilegesRequired: string;
    userInteraction: string;
    scope: string;
    confidentiality: string;
    integrity: string;
    availability: string;
  };
  temporal: {
    exploitCodeMaturity: string;
    remediationLevel: string;
    reportConfidence: string;
  };
}

interface IVulnerabilityCriteriaScoreV4 {
  base: {
    // Exploitability
    attackComplexity: string;
    attackRequirements: string;
    attackVector: string;
    userInteraction: string;
    privilegesRequired: string;
    // Impact
    availabilityVa: string;
    confidentialityVc: string;
    integrityVi: string;
    availabilitySa: string;
    confidentialitySc: string;
    integritySi: string;
  };
  threat: {
    exploitMaturity: string;
  };
}

interface IVulnerabilityCriteriaData {
  en: IVulnerabilityCriteriaLanguage;
  es: IVulnerabilityCriteriaLanguage;
  findingNumber: string;
  score: IVulnerabilityCriteriaScore;
  scoreV4: IVulnerabilityCriteriaScoreV4;
  remediationTime: string;
  requirements: string[];
}

export type {
  IAddFindingFormValues,
  IVulnerabilityCriteriaData,
  IFindingAttr,
  IFindingSuggestionData,
  IGroupVulnerabilities,
  ILocationsInfoAttr,
  IRoot,
  ITreatmentSummaryAttr,
  IVerificationSummaryAttr,
  IVulnerabilitiesResume,
  IVulnerability,
  IDescriptionProps,
  IFilterFormProps,
  IReportParams,
  IVulnerabilityCriteriaScoreV4,
  IFormValues,
  IDeactivationModalProps,
  TStatus,
};
