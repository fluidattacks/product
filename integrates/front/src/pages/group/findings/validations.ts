import type { InferType, Schema } from "yup";
import { array, date, lazy, mixed, number, object, ref, string } from "yup";

import type { IPhoneAttr } from "features/verify-dialog/types";
import { formatPhone } from "utils/format-helpers";
import { translate } from "utils/translations/translate";

const MAX_LENGTH = 100;
const MAX_SEVERITY = 10;
const today: Date = new Date();

const validationSchema = object().shape({
  closingDate: date()
    .nullable()
    .transform((value, currentValue): Date | undefined =>
      currentValue === "" ? undefined : value,
    )
    .max(today, translate.t("validations.greaterDate")),
  location: string()
    .max(
      MAX_LENGTH,
      translate.t("validations.maxLength", { count: MAX_LENGTH }),
    )
    .isValidTextBeginning()
    .isValidTextField(),
  maxReleaseDate: date()
    .nullable()
    .transform((value, currentValue): Date | undefined =>
      currentValue === "" ? undefined : value,
    )
    .max(new Date(), translate.t("validations.greaterDate")),
  maxSeverity: number()
    .nullable()
    .transform((value, currentValue): number | undefined =>
      Number.isNaN(currentValue) ? undefined : value,
    )
    .max(MAX_SEVERITY)
    .min(ref("minSeverity"), "Must be greater than or equal to Min severity"),
  minReleaseDate: date()
    .nullable()
    .transform((value, currentValue): Date | undefined =>
      currentValue === "" ? undefined : value,
    )
    .max(new Date(), translate.t("validations.greaterDate")),
  minSeverity: number()
    .nullable()
    .transform((value, currentValue): number | undefined =>
      Number.isNaN(currentValue) ? undefined : value,
    )
    .min(0)
    .max(ref("maxSeverity"), "Must be less than or equal to Max severity"),
  states: array().min(1, translate.t("validations.someRequired")),
  treatments: array().when(
    ["startClosingDate", "closingDate"],
    ([startClosingDate, closingDate]: (string | undefined)[]): Schema =>
      (closingDate === "" || closingDate === undefined) &&
      (startClosingDate === "" || startClosingDate === undefined)
        ? array().min(1, translate.t("validations.someRequired"))
        : array().notRequired(),
  ),
});

const addFindingValidations = (titleSuggestions: string[]): InferType<Schema> =>
  lazy(
    (): Schema =>
      object().shape({
        description: string()
          .required(translate.t("validations.required"))
          .isValidTextBeginning()
          .isValidTextField(),
        score: object().shape({
          attackComplexity: string().required(
            translate.t("validations.required"),
          ),
          attackVector: string().required(translate.t("validations.required")),
          availabilityImpact: string().required(
            translate.t("validations.required"),
          ),
          confidentialityImpact: string().required(
            translate.t("validations.required"),
          ),
          exploitability: string().required(
            translate.t("validations.required"),
          ),
          integrityImpact: string().required(
            translate.t("validations.required"),
          ),
          privilegesRequired: string().required(
            translate.t("validations.required"),
          ),
          remediationLevel: string().required(
            translate.t("validations.required"),
          ),
          reportConfidence: string().required(
            translate.t("validations.required"),
          ),
          severityScope: string().required(translate.t("validations.required")),
          userInteraction: string().required(
            translate.t("validations.required"),
          ),
        }),
        scoreV4: object().shape({
          attackComplexity: string().required(
            translate.t("validations.required"),
          ),
          attackRequirements: string().required(
            translate.t("validations.required"),
          ),
          attackVector: string().required(translate.t("validations.required")),
          availabilitySubsequentImpact: string().required(
            translate.t("validations.required"),
          ),
          availabilityVulnerableImpact: string().required(
            translate.t("validations.required"),
          ),
          confidentialitySubsequentImpact: string().required(
            translate.t("validations.required"),
          ),
          confidentialityVulnerableImpact: string().required(
            translate.t("validations.required"),
          ),
          exploitability: string().required(
            translate.t("validations.required"),
          ),
          integritySubsequentImpact: string().required(
            translate.t("validations.required"),
          ),
          integrityVulnerableImpact: string().required(
            translate.t("validations.required"),
          ),
          privilegesRequired: string().required(
            translate.t("validations.required"),
          ),
          userInteraction: string().required(
            translate.t("validations.required"),
          ),
        }),
        threat: string()
          .required(translate.t("validations.required"))
          .isValidTextBeginning()
          .isValidTextField(),
        title: string()
          .required(translate.t("validations.required"))
          .matches(/^\d{3}\. .+/gu, translate.t("validations.draftTitle"))
          .oneOf(titleSuggestions, translate.t("validations.draftTypology")),
      }),
  );

const welcomeValidationSchema = object().shape({
  phone: mixed<IPhoneAttr>()
    .transform(
      (value): IPhoneAttr => (value === "" ? value : formatPhone(value)),
    )
    .required(translate.t("validations.required"))
    .isValidPhoneNumber(),
  reason: string().required(translate.t("validations.required")),
});

export { addFindingValidations, validationSchema, welcomeValidationSchema };
