import { useApolloClient, useLazyQuery } from "@apollo/client";
import { Button, useModal } from "@fluidattacks/design";
import React, { useCallback, useState } from "react";
import { useTranslation } from "react-i18next";

import { AddFindingModal } from "../add-finding-modal";
import { DeleteModal } from "../delete-modal";
import { GET_CRITERIA, GET_FINDINGS } from "../queries";
import { ReportsDropDown } from "../reports";
import type { IReportOptionsProps } from "../reports/types";
import type {
  IFindingAttr,
  IFindingSuggestionData,
  IVulnerabilityCriteriaData,
} from "../types";
import { getFindingSuggestions } from "../utils";
import { Can } from "context/authz/can";
import type { GetFindingsCriteriaFragment } from "gql/graphql";
import { useAuthz } from "hooks/use-authz";
import { useHandledTimeout } from "hooks/use-handled-timeout";

interface IExtraButtons extends IReportOptionsProps {
  language: "en" | "es";
  groupName: string;
  selectedFindings: IFindingAttr[];
}

const ExtraButtons = ({
  language,
  groupName,
  selectedFindings,
  enableCerts,
  typesOptions,
}: Readonly<IExtraButtons>): JSX.Element | undefined => {
  const { t } = useTranslation();
  const addModalRef = useModal("add-finding");
  const deleteModalRef = useModal("delete-finding");
  const { open: openDeleteModal } = deleteModalRef;
  const [suggestions, setSuggestions] = useState<IFindingSuggestionData[]>([]);
  const client = useApolloClient();
  const { hasAttribute, canResolve, canMutate } = useAuthz();

  const canReportVulns = hasAttribute("can_report_vulnerabilities");
  const canAddFinding = canMutate("add_finding");
  const canRemoveFinding = canMutate("remove_finding");
  const canGetReport = canResolve("query_report");

  const [getCriteria, { loading }] = useLazyQuery(GET_CRITERIA, {
    onCompleted: (results): void => {
      const formattedVulns = Object.fromEntries(
        (
          results as unknown as GetFindingsCriteriaFragment
        ).criteriaConnection.edges.map(
          (vuln): [string, IVulnerabilityCriteriaData] => [
            vuln.node.findingNumber,
            vuln.node as IVulnerabilityCriteriaData,
          ],
        ),
      );

      const findingSuggestions = getFindingSuggestions(
        formattedVulns,
        language,
      );
      setSuggestions(findingSuggestions);
    },
  });

  const { addTimeout } = useHandledTimeout();

  const onAction = useCallback(async (): Promise<void> => {
    await new Promise((resolve): void => {
      const delayRefetch = 1500;
      addTimeout((): void => {
        void client.refetchQueries({ include: [GET_FINDINGS] });
      }, delayRefetch);

      resolve(undefined);
    });
  }, [client, addTimeout]);

  const openAddFindingModal = useCallback((): void => {
    addModalRef.open();
    void getCriteria({ variables: { after: "" } });
  }, [addModalRef, getCriteria]);

  return (
    <React.Fragment>
      {canReportVulns && canAddFinding && (
        <React.Fragment>
          <Can I={"integrates_api_mutations_add_finding_mutate"}>
            <Button
              icon={"plus"}
              id={"addFinding"}
              onClick={openAddFindingModal}
              tooltip={t("group.findings.buttons.add.tooltip")}
              variant={"ghost"}
            >
              {t("group.findings.buttons.add.text")}
            </Button>
          </Can>
          <AddFindingModal
            groupName={groupName}
            loading={loading}
            modalRef={addModalRef}
            refetch={onAction}
            suggestions={suggestions}
          />
        </React.Fragment>
      )}
      {canRemoveFinding && (
        <React.Fragment>
          <Button
            disabled={selectedFindings.length === 0}
            icon={"trash-alt"}
            onClick={openDeleteModal}
            tooltip={t("group.findings.buttons.delete.tooltip")}
            variant={"ghost"}
          >
            {t("group.findings.buttons.delete.text")}
          </Button>
          <DeleteModal
            modalRef={deleteModalRef}
            refetch={onAction}
            selectedFindings={selectedFindings}
          />
        </React.Fragment>
      )}
      {canGetReport && (
        <ReportsDropDown
          enableCerts={enableCerts}
          typesOptions={typesOptions}
        />
      )}
    </React.Fragment>
  );
};

export { ExtraButtons };
