import type { ApolloError } from "@apollo/client";

import type { IAddFindingFormValues } from "../types";
import { Logger } from "utils/logger";
import { msgError } from "utils/notifications";
import { translate } from "utils/translations/translate";

const defaultAddFindingInitialValues: IAddFindingFormValues = {
  description: "",
  score: {
    attackComplexity: "",
    attackVector: "",
    availabilityImpact: "",
    availabilityRequirement: "",
    confidentialityImpact: "",
    confidentialityRequirement: "",
    exploitability: "",
    integrityImpact: "",
    integrityRequirement: "",
    modifiedAttackComplexity: "",
    modifiedAttackVector: "",
    modifiedAvailabilityImpact: "",
    modifiedConfidentialityImpact: "",
    modifiedIntegrityImpact: "",
    modifiedPrivilegesRequired: "",
    modifiedSeverityScope: "",
    modifiedUserInteraction: "",
    privilegesRequired: "",
    remediationLevel: "",
    reportConfidence: "",
    severityScope: "",
    userInteraction: "",
  },
  scoreV4: {
    attackComplexity: "",
    attackRequirements: "",
    attackVector: "",
    availabilityRequirement: "",
    availabilitySubsequentImpact: "",
    availabilityVulnerableImpact: "",
    confidentialityRequirement: "",
    confidentialitySubsequentImpact: "",
    confidentialityVulnerableImpact: "",
    exploitability: "",
    integrityRequirement: "",
    integritySubsequentImpact: "",
    integrityVulnerableImpact: "",
    modifiedAttackComplexity: "",
    modifiedAttackRequirements: "",
    modifiedAttackVector: "",
    modifiedAvailabilitySubsequentImpact: "",
    modifiedAvailabilityVulnerableImpact: "",
    modifiedConfidentialitySubsequentImpact: "",
    modifiedConfidentialityVulnerableImpact: "",
    modifiedIntegritySubsequentImpact: "",
    modifiedIntegrityVulnerableImpact: "",
    modifiedPrivilegesRequired: "",
    modifiedUserInteraction: "",
    privilegesRequired: "",
    userInteraction: "",
  },
  threat: "",
  title: "",
};

const handleAddFindingError = (errors: ApolloError): void => {
  errors.graphQLErrors.forEach((error): void => {
    switch (error.message) {
      case "Exception - The inserted Draft/Finding title is invalid":
        msgError(translate.t("validations.addFindingModal.invalidTitle"));
        break;
      case "Exception - Finding with the same threat already exists":
        msgError(translate.t("validations.addFindingModal.duplicatedThreat"));
        break;
      case "Exception - Finding with the same description already exists":
        msgError(
          translate.t("validations.addFindingModal.duplicatedDescription"),
        );
        break;
      case "Exception - Error invalid severity CVSS v4 vector string":
        msgError(
          translate.t(
            "searchFindings.tabVuln.severityInfo.alerts.invalidSeverityVectorV4",
          ),
        );
        break;
      case "Exception - Finding with the same description, threat and severity already exists":
        msgError(
          translate.t(
            "validations.addFindingModal.duplicatedMachineDescription",
          ),
        );
        break;
      case "Exception - Severity score is invalid":
        msgError(
          translate.t("validations.addFindingModal.invalidSeverityScore"),
        );
        break;
      case "Exception - Invalid characters":
        msgError(translate.t("validations.invalidChar"));
        break;
      default:
        msgError(translate.t("groupAlerts.errorTextsad"));
        Logger.warning("An error occurred adding finding", error);
    }
  });
};

export { defaultAddFindingInitialValues, handleAddFindingError };
