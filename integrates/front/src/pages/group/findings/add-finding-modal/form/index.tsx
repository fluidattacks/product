import type { IItem } from "@fluidattacks/design";
import {
  Col,
  ComboBox,
  Input,
  Loading,
  Row,
  type TFormMethods,
  Text,
  TextArea,
} from "@fluidattacks/design";
import _ from "lodash";
import { type ChangeEvent, Fragment, useEffect, useState } from "react";
import { useTranslation } from "react-i18next";

import type { IAddFindingFormValues } from "../../types";
import { castFieldsCVSS4 } from "pages/finding/severity/cvss4/utils";
import {
  attackComplexityValues,
  attackVectorValues,
  availabilityImpactValues,
  availabilityRequirementValues,
  confidentialityImpactValues,
  confidentialityRequirementValues,
  exploitabilityValues,
  integrityImpactValues,
  integrityRequirementValues,
  privilegesRequiredValues,
  remediationLevelValues,
  reportConfidenceValues,
  severityScopeValues,
  userInteractionValues,
} from "utils/cvss";

const MAX_DESCRIPTION_LENGTH = 500;
const MAX_THREAT_LENGTH = 300;
const prefix = "searchFindings.tabSeverity.";

export const AddFindingForm = ({
  addFindingInitialValues,
  control,
  defaultValues,
  handleOnChange,
  formState: { errors },
  loading,
  register,
  reset,
  titleSuggestions,
  watch,
}: {
  addFindingInitialValues?: IAddFindingFormValues;
  defaultValues: IAddFindingFormValues;
  handleOnChange: ({ target }: ChangeEvent<HTMLInputElement>) => void;
  loading: boolean;
  titleSuggestions: string[];
} & TFormMethods<IAddFindingFormValues>): JSX.Element => {
  const { t } = useTranslation();
  const [isResetting, setIsResetting] = useState(false);

  const selectOptions = (listOptions: Record<string, string>): IItem[] =>
    Object.entries(listOptions).map(
      ([value, label]): IItem => ({
        name: t(label),
        value,
      }),
    );

  useEffect((): void => {
    if (addFindingInitialValues) {
      setIsResetting(true);
      reset(addFindingInitialValues, { keepDefaultValues: true });
      setIsResetting(false);
    } else {
      reset(defaultValues, { keepDefaultValues: true });
    }
  }, [addFindingInitialValues, defaultValues, reset]);

  return (
    <Fragment>
      <Row>
        {loading && titleSuggestions.length === 0 ? (
          <Loading label={"Loading finding suggestions"} />
        ) : (
          <Input
            error={errors.title?.message}
            label={t("group.findings.addModal.fields.title.label")}
            name={"title"}
            onChange={handleOnChange}
            register={register}
            required={true}
            suggestions={titleSuggestions}
            tooltip={t("group.findings.addModal.fields.title.tooltip")}
          />
        )}
      </Row>
      <Row>
        <TextArea
          error={errors.description?.message}
          label={t("group.findings.addModal.fields.description.label")}
          maxLength={MAX_DESCRIPTION_LENGTH}
          name={"description"}
          register={register}
          required={true}
          tooltip={t("group.findings.addModal.fields.description.tooltip")}
          watch={watch}
        />
      </Row>
      <Row>
        <TextArea
          error={errors.threat?.message}
          label={t("group.findings.addModal.fields.threat.label")}
          maxLength={MAX_THREAT_LENGTH}
          name={"threat"}
          register={register}
          required={true}
          tooltip={t("group.findings.addModal.fields.threat.tooltip")}
          watch={watch}
        />
      </Row>
      <Text fontWeight={"bold"} size={"sm"}>
        {`${t("searchFindings.tabSeverity.cvssVersion")}: 3.1`}
      </Text>
      <Row>
        <Col lg={33} md={50} sm={100}>
          <ComboBox
            control={control}
            items={selectOptions(attackComplexityValues)}
            label={t(`${prefix}attackComplexity.label`)}
            loadingItems={isResetting}
            name={"score.attackComplexity"}
            required={true}
          />
        </Col>
        <Col lg={33} md={50} sm={100}>
          <ComboBox
            control={control}
            items={selectOptions(attackVectorValues)}
            label={t(`${prefix}attackVector.label`)}
            loadingItems={isResetting}
            name={"score.attackVector"}
            required={true}
          />
        </Col>
        <Col lg={33} md={50} sm={100}>
          <ComboBox
            control={control}
            items={selectOptions(availabilityImpactValues)}
            label={t(`${prefix}availabilityImpact.label`)}
            loadingItems={isResetting}
            name={"score.availabilityImpact"}
            required={true}
          />
        </Col>
      </Row>
      <Row>
        <Col lg={33} md={50} sm={100}>
          <ComboBox
            control={control}
            items={selectOptions(availabilityRequirementValues)}
            label={t(`${prefix}availabilityRequirement.label`)}
            loadingItems={isResetting}
            name={"score.availabilityRequirement"}
          />
        </Col>
        <Col lg={33} md={50} sm={100}>
          <ComboBox
            control={control}
            items={selectOptions(confidentialityImpactValues)}
            label={t(`${prefix}confidentialityImpact.label`)}
            loadingItems={isResetting}
            name={"score.confidentialityImpact"}
            required={true}
          />
        </Col>
        <Col lg={33} md={50} sm={100}>
          <ComboBox
            control={control}
            items={selectOptions(confidentialityRequirementValues)}
            label={t(`${prefix}confidentialityRequirement.label`)}
            loadingItems={isResetting}
            name={"score.confidentialityRequirement"}
          />
        </Col>
      </Row>
      <Row>
        <Col lg={33} md={50} sm={100}>
          <ComboBox
            control={control}
            items={selectOptions(exploitabilityValues)}
            label={t(`${prefix}exploitability.label`)}
            loadingItems={isResetting}
            name={"score.exploitability"}
            required={true}
          />
        </Col>
        <Col lg={33} md={50} sm={100}>
          <ComboBox
            control={control}
            items={selectOptions(integrityImpactValues)}
            label={t(`${prefix}integrityImpact.label`)}
            loadingItems={isResetting}
            name={"score.integrityImpact"}
            required={true}
          />
        </Col>
        <Col lg={33} md={50} sm={100}>
          <ComboBox
            control={control}
            items={selectOptions(integrityRequirementValues)}
            label={t(`${prefix}integrityRequirement.label`)}
            loadingItems={isResetting}
            name={"score.integrityRequirement"}
          />
        </Col>
      </Row>
      <Row>
        <Col lg={33} md={50} sm={100}>
          <ComboBox
            control={control}
            items={selectOptions(attackComplexityValues)}
            label={t(`${prefix}modifiedAttackComplexity`)}
            loadingItems={isResetting}
            name={"score.modifiedAttackComplexity"}
          />
        </Col>
        <Col lg={33} md={50} sm={100}>
          <ComboBox
            control={control}
            items={selectOptions(attackVectorValues)}
            label={t(`${prefix}modifiedAttackVector`)}
            loadingItems={isResetting}
            name={"score.modifiedAttackVector"}
          />
        </Col>
        <Col lg={33} md={50} sm={100}>
          <ComboBox
            control={control}
            items={selectOptions(availabilityImpactValues)}
            label={t("modifiedAvailabilityImpact")}
            loadingItems={isResetting}
            name={"score.modifiedAvailabilityImpact"}
          />
        </Col>
      </Row>
      <Row>
        <Col lg={33} md={50} sm={100}>
          <ComboBox
            control={control}
            items={selectOptions(confidentialityImpactValues)}
            label={t(`${prefix}modifiedConfidentialityImpact`)}
            loadingItems={isResetting}
            name={"score.modifiedConfidentialityImpact"}
          />
        </Col>
        <Col lg={33} md={50} sm={100}>
          <ComboBox
            control={control}
            items={selectOptions(integrityImpactValues)}
            label={t(`${prefix}modifiedIntegrityImpact`)}
            loadingItems={isResetting}
            name={"score.modifiedIntegrityImpact"}
          />
        </Col>
        <Col lg={33} md={50} sm={100}>
          <ComboBox
            control={control}
            items={selectOptions(privilegesRequiredValues)}
            label={t(`${prefix}modifiedPrivilegesRequired`)}
            loadingItems={isResetting}
            name={"score.modifiedPrivilegesRequired"}
          />
        </Col>
      </Row>
      <Row>
        <Col lg={33} md={50} sm={100}>
          <ComboBox
            control={control}
            items={selectOptions(userInteractionValues)}
            label={t(`${prefix}modifiedUserInteraction`)}
            loadingItems={isResetting}
            name={"score.modifiedUserInteraction"}
          />
        </Col>
        <Col lg={33} md={50} sm={100}>
          <ComboBox
            control={control}
            items={selectOptions(severityScopeValues)}
            label={t(`${prefix}modifiedSeverityScope`)}
            loadingItems={isResetting}
            name={"score.modifiedSeverityScope"}
          />
        </Col>
        <Col lg={33} md={50} sm={100}>
          <ComboBox
            control={control}
            items={selectOptions(privilegesRequiredValues)}
            label={t(`${prefix}privilegesRequired.label`)}
            loadingItems={isResetting}
            name={"score.privilegesRequired"}
            required={true}
          />
        </Col>
      </Row>
      <Row>
        <Col lg={33} md={50} sm={100}>
          <ComboBox
            control={control}
            items={selectOptions(remediationLevelValues)}
            label={t(`${prefix}remediationLevel.label`)}
            loadingItems={isResetting}
            name={"score.remediationLevel"}
            required={true}
          />
        </Col>
        <Col lg={33} md={50} sm={100}>
          <ComboBox
            control={control}
            items={selectOptions(reportConfidenceValues)}
            label={t(`${prefix}reportConfidence.label`)}
            loadingItems={isResetting}
            name={"score.reportConfidence"}
            required={true}
          />
        </Col>
        <Col lg={33} md={50} sm={100}>
          <ComboBox
            control={control}
            items={selectOptions(severityScopeValues)}
            label={t(`${prefix}severityScope.label`)}
            loadingItems={isResetting}
            name={"score.severityScope"}
            required={true}
          />
        </Col>
      </Row>
      <Row>
        <Col lg={33} md={50} sm={100}>
          <ComboBox
            control={control}
            items={selectOptions(userInteractionValues)}
            label={t(`${prefix}userInteraction.label`)}
            loadingItems={isResetting}
            name={"score.userInteraction"}
            required={true}
          />
        </Col>
      </Row>
      <Text fontWeight={"bold"} size={"sm"}>
        {`${t("searchFindings.tabSeverity.cvssVersion")}: 4.0`}
      </Text>
      <Row>
        {castFieldsCVSS4(defaultValues.scoreV4).map(
          (field, index): JSX.Element => {
            return (
              <Col key={`scoreV4${field.name}`} lg={33} md={50} sm={100}>
                <ComboBox
                  control={control}
                  id={`scoreV4Row${index}`}
                  items={_.map(
                    field.options,
                    (text, value): IItem => ({
                      name: t(text),
                      value,
                    }),
                  )}
                  label={field.title}
                  loadingItems={isResetting}
                  name={`scoreV4.${field.name}`}
                  required={field.required === true}
                />
              </Col>
            );
          },
        )}
      </Row>
    </Fragment>
  );
};
