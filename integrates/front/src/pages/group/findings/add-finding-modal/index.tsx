import { useMutation } from "@apollo/client";
import { Form, InnerForm, Modal } from "@fluidattacks/design";
import _ from "lodash";
import { useCallback, useMemo, useState } from "react";
import type { ChangeEvent } from "react";
import { useTranslation } from "react-i18next";

import { AddFindingForm } from "./form";
import { defaultAddFindingInitialValues, handleAddFindingError } from "./hooks";
import type { IAddVulnerabilityModalProps } from "./types";

import { ADD_FINDING_MUTATION } from "../queries";
import type { IAddFindingFormValues, IFindingSuggestionData } from "../types";
import { addFindingValidations } from "../validations";
import { getCVSS31VectorString } from "utils/cvss";
import { getCVSS4VectorString } from "utils/cvss4";
import { msgSuccess } from "utils/notifications";

const AddFindingModal = ({
  groupName,
  modalRef,
  refetch,
  suggestions,
  loading = false,
}: IAddVulnerabilityModalProps): JSX.Element => {
  const { t } = useTranslation();
  const { close } = modalRef;
  const [addFindingInitialValues, setAddFindingInitialValues] = useState<
    IAddFindingFormValues | undefined
  >(undefined);

  const titleSuggestions = useMemo(
    (): string[] =>
      _.isUndefined(suggestions)
        ? []
        : _.sortBy(suggestions.map((suggestion): string => suggestion.title)),
    [suggestions],
  );

  const [addFinding, { loading: addingFinding }] = useMutation(
    ADD_FINDING_MUTATION,
    {
      onCompleted: (result): void => {
        if (result.addFinding.success) {
          msgSuccess(
            t("group.findings.addModal.alerts.addedFinding"),
            t("groupAlerts.titleSuccess"),
          );
          void refetch();
          close();
        }
      },
      onError: handleAddFindingError,
    },
  );

  const getFindingMatchingSuggestion = useCallback(
    (findingName: string): IFindingSuggestionData | undefined => {
      const [matchingSuggestion] = _.isUndefined(suggestions)
        ? []
        : suggestions.filter(
            (suggestion): boolean => suggestion.title === findingName,
          );

      return matchingSuggestion;
    },
    [suggestions],
  );
  const handleAddFindingTitleChange = useCallback(
    ({ target }: ChangeEvent<HTMLInputElement>): void => {
      const matchingSuggestion = getFindingMatchingSuggestion(target.value);
      if (matchingSuggestion !== undefined) {
        setAddFindingInitialValues({
          ...addFindingInitialValues,
          description: matchingSuggestion.description,
          score: {
            ...defaultAddFindingInitialValues.score,
            ...matchingSuggestion.score,
          },
          scoreV4: {
            ...defaultAddFindingInitialValues.scoreV4,
            ...matchingSuggestion.scoreV4,
          },
          threat: matchingSuggestion.threat,
          title: target.value,
        });
      }
    },
    [addFindingInitialValues, getFindingMatchingSuggestion],
  );

  const handleAddFinding = useCallback(
    async (values: IAddFindingFormValues): Promise<void> => {
      const [matchingSuggestion] = _.isUndefined(suggestions)
        ? []
        : suggestions.filter(
            (suggestion): boolean => suggestion.title === values.title,
          );
      const suggestionData = _.omit(matchingSuggestion, ["code"]);
      await addFinding({
        variables: {
          attackVectorDescription: suggestionData.attackVectorDescription,
          cvss4Vector: getCVSS4VectorString(values.scoreV4),
          cvssVector: getCVSS31VectorString(values.score),
          description: values.description,
          groupName,
          minTimeToRemediate: suggestionData.minTimeToRemediate,
          recommendation: suggestionData.recommendation,
          threat: values.threat,
          title: values.title,
          unfulfilledRequirements: suggestionData.unfulfilledRequirements,
        },
      });
      setAddFindingInitialValues(undefined);
    },
    [addFinding, groupName, suggestions],
  );
  const closeAddFindingModal = useCallback((): void => {
    setAddFindingInitialValues(undefined);
    close();
  }, [close]);

  return (
    <Modal
      id={"addFinding"}
      modalRef={modalRef}
      size={"lg"}
      title={t("group.findings.addModal.title")}
    >
      <Form
        cancelButton={{ onClick: closeAddFindingModal }}
        confirmButton={{ disabled: addingFinding }}
        defaultValues={defaultAddFindingInitialValues}
        onSubmit={handleAddFinding}
        yupSchema={addFindingValidations(titleSuggestions)}
      >
        <InnerForm<IAddFindingFormValues>>
          {(methods): JSX.Element => (
            <AddFindingForm
              {...methods}
              addFindingInitialValues={addFindingInitialValues}
              defaultValues={defaultAddFindingInitialValues}
              handleOnChange={handleAddFindingTitleChange}
              loading={loading}
              titleSuggestions={titleSuggestions}
            />
          )}
        </InnerForm>
      </Form>
    </Modal>
  );
};

export { AddFindingModal };
