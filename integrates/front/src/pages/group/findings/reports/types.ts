import type React from "react";

import type { TVerifyFn } from "features/verify-dialog/types";

interface IReportOptionsProps {
  enableCerts: boolean;
  setVerifyCallbacks?: TVerifyFn;
  typesOptions: string[];
  setIsVerifyDialogOpen?: React.Dispatch<React.SetStateAction<boolean>>;
}
export type { IReportOptionsProps };
