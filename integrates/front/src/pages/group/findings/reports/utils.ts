import isEmpty from "lodash/isEmpty";

import type { IFormValues } from "../types";

function setReportType(icon: SVGElement): string {
  if (
    (icon.attributes.getNamedItem("data-icon")?.value as string).includes("pdf")
  ) {
    return "PDF";
  } else if (
    (icon.attributes.getNamedItem("data-icon")?.value as string).includes(
      "contract",
    )
  ) {
    return "CERT";
  }

  return (icon.attributes.getNamedItem("data-icon")?.value as string).includes(
    "excel",
  )
    ? "XLS"
    : "DATA";
}

function setReport(button: HTMLElement): string | undefined {
  if (button.id === "report-pdf") {
    return "PDF";
  } else if (button.id === "report-cert") {
    return "CERT";
  } else if (button.id === "report-excel") {
    return "XLS";
  } else if (button.id === "report-zip") {
    return "DATA";
  }

  return undefined;
}

const findingTitle = (values: IFormValues): string | undefined =>
  isEmpty(values.findingTitle) ? undefined : values.findingTitle;

const lastReport = (values: IFormValues): number | undefined =>
  values.lastReport === undefined ||
  isEmpty(String(values.lastReport)) ||
  Number.isNaN(values.lastReport)
    ? undefined
    : Number(values.lastReport);

const getMaxSeverity = (values: IFormValues): number | undefined =>
  values.maxSeverity === undefined ||
  isEmpty(String(values.maxSeverity)) ||
  Number.isNaN(values.maxSeverity)
    ? undefined
    : Number(values.maxSeverity);

const getMinSeverity = (values: IFormValues): number | undefined =>
  values.minSeverity === undefined ||
  isEmpty(String(values.minSeverity)) ||
  Number.isNaN(values.minSeverity)
    ? undefined
    : Number(values.minSeverity);

const getAge = (values: IFormValues): number | undefined =>
  values.age === undefined ||
  isEmpty(String(values.age)) ||
  Number.isNaN(values.age)
    ? undefined
    : Number(values.age);

const location = (values: IFormValues): string | undefined =>
  isEmpty(values.location) ? undefined : values.location;

export {
  setReportType,
  setReport,
  findingTitle,
  getAge,
  getMaxSeverity,
  getMinSeverity,
  lastReport,
  location,
};
