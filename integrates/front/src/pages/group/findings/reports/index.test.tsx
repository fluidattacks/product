import { PureAbility } from "@casl/ability";
import { screen, waitFor } from "@testing-library/react";
import { userEvent } from "@testing-library/user-event";
import { GraphQLError } from "graphql";
import isEqual from "lodash/isEqual";
import { HttpResponse, type StrictResponse, graphql } from "msw";
import { Route, Routes } from "react-router-dom";

import { ReportsDropDown } from ".";
import { GET_FINDINGS, REQUEST_GROUP_REPORT } from "../queries";
import { authzPermissionsContext } from "context/authz/config";
import type {
  GetFindingsQueryQuery as GetFindings,
  GetRootNicknamesQuery as GetRoots,
  RequestGroupReportAtFindingsQuery as RequestGroupReport,
} from "gql/graphql";
import { Language, ManagedType, ReportType } from "gql/graphql";
import { render } from "mocks";
import { LINK } from "mocks/constants";
import type { IErrorMessage } from "mocks/types";
import { GET_ROOTS } from "pages/group/scope/queries";
import { msgError, msgSuccess } from "utils/notifications";

jest.mock("utils/notifications", (): Record<string, unknown> => {
  const mockedNotifications: Record<string, () => Record<string, unknown>> =
    jest.requireActual("utils/notifications");

  jest.spyOn(mockedNotifications, "msgError").mockImplementation();
  jest.spyOn(mockedNotifications, "msgSuccess").mockImplementation();

  return mockedNotifications;
});

const ReportComponent = ({
  permission = "see_group_certificate",
}: Readonly<{ permission?: string }>): JSX.Element => {
  const mockedPermissions = new PureAbility<string>([{ action: permission }]);

  return (
    <authzPermissionsContext.Provider value={mockedPermissions}>
      <Routes>
        <Route
          element={<ReportsDropDown enableCerts={true} typesOptions={[]} />}
          path={"/orgs/:organizationName/groups/:groupName/vulns"}
        />
      </Routes>
    </authzPermissionsContext.Provider>
  );
};

const findings: GetFindings["group"]["findings"] = [
  {
    __typename: "Finding",
    age: 252,
    description: "This is a test description",
    hacker: "anyhacker@fluidattacks.com",
    id: "438679960",
    isExploitable: true,
    lastVulnerability: 33,
    maxOpenSeverityScore: 2.9,
    maxOpenSeverityScoreV4: 2.9,
    minTimeToRemediate: 60,
    openAge: 60,
    releaseDate: "2020-02-05 09:56:40",
    status: "VULNERABLE",
    title: "038. Business information leak",
    totalOpenPriority: 0,
    treatmentSummary: {
      accepted: 0,
      acceptedUndefined: 0,
      inProgress: 0,
      untreated: 1,
    },
    verificationSummary: {
      onHold: 1,
      requested: 2,
      verified: 3,
    },
    verified: false,
    vulnerabilitiesSummary: {
      closed: 6,
      open: 6,
      openCritical: 2,
      openHigh: 2,
      openLow: 0,
      openMedium: 2,
    },
    vulnerabilitiesSummaryV4: {
      closed: 6,
      open: 6,
      openCritical: 0,
      openHigh: 2,
      openLow: 2,
      openMedium: 2,
    },
  },
  {
    __typename: "Finding",
    age: 240,
    description: "I just have updated the description",
    hacker: "anyhacker@fluidattacks.com",
    id: "697510163",
    isExploitable: false,
    lastVulnerability: 22,
    maxOpenSeverityScore: 3.6,
    maxOpenSeverityScoreV4: 4.6,
    minTimeToRemediate: 60,
    openAge: 60,
    releaseDate: "2020-07-05 09:56:40",
    status: "VULNERABLE",
    title: "001. SQL injection - C Sharp SQL API",
    totalOpenPriority: 0,
    treatmentSummary: {
      accepted: 0,
      acceptedUndefined: 0,
      inProgress: 6,
      untreated: 0,
    },
    verificationSummary: {
      onHold: 0,
      requested: 0,
      verified: 0,
    },
    verified: false,
    vulnerabilitiesSummary: {
      closed: 6,
      open: 6,
      openCritical: 2,
      openHigh: 2,
      openLow: 0,
      openMedium: 2,
    },
    vulnerabilitiesSummaryV4: {
      closed: 6,
      open: 6,
      openCritical: 0,
      openHigh: 2,
      openLow: 2,
      openMedium: 2,
    },
  },
  {
    __typename: "Finding",
    age: 23,
    description: "I just have updated the description",
    hacker: "anyhacker@fluidattacks.com",
    id: "268510163",
    isExploitable: false,
    lastVulnerability: 22,
    maxOpenSeverityScore: 0,
    maxOpenSeverityScoreV4: 0,
    minTimeToRemediate: 0,
    openAge: 23,
    releaseDate: "2020-04-22 09:56:40",
    status: "SAFE",
    title: "071. Insecure or unset HTTP headers - Referrer-Policy",
    totalOpenPriority: 0,
    treatmentSummary: {
      accepted: 0,
      acceptedUndefined: 0,
      inProgress: 0,
      untreated: 0,
    },
    verificationSummary: {
      onHold: 0,
      requested: 0,
      verified: 0,
    },
    verified: false,
    vulnerabilitiesSummary: {
      closed: 6,
      open: 6,
      openCritical: 2,
      openHigh: 2,
      openLow: 0,
      openMedium: 2,
    },
    vulnerabilitiesSummaryV4: {
      closed: 6,
      open: 6,
      openCritical: 0,
      openHigh: 2,
      openLow: 2,
      openMedium: 2,
    },
  },
];

describe("reports dropdown", (): void => {
  const graphqlMocked = graphql.link(LINK);
  const apolloDataFinding = {
    group: {
      __typename: "Group" as const,
      businessId: "id",
      businessName: "name",
      description: "description",
      findings,
      hasEssential: false,
      language: Language.En,
      managed: ManagedType.Managed,
      name: "TEST",
      totalOpenPriority: 0,
      userRole: "user-role",
    },
  };
  const filterDataFinding = {
    group: {
      __typename: "Group" as const,
      businessId: "id",
      businessName: "name",
      description: "description",
      findings: [
        {
          __typename: "Finding" as const,
          age: 252,
          description: "This is a test description",
          hacker: "anyhacker@fluidattacks.com",
          id: "413372600",
          isExploitable: true,
          lastVulnerability: 33,
          maxOpenSeverityScore: 2.9,
          maxOpenSeverityScoreV4: 2.4,
          minTimeToRemediate: 60,
          openAge: 60,
          releaseDate: "2020-02-05 09:56:40",
          status: "VULNERABLE",
          title: "004. Remote command execution",
          totalOpenPriority: 0,
          treatmentSummary: {
            accepted: 0,
            acceptedUndefined: 0,
            inProgress: 0,
            untreated: 1,
          },
          verificationSummary: {
            onHold: 1,
            requested: 2,
            verified: 3,
          },
          verified: false,
          vulnerabilitiesSummary: {
            closed: 6,
            open: 6,
            openCritical: 2,
            openHigh: 2,
            openLow: 0,
            openMedium: 2,
          },
          vulnerabilitiesSummaryV4: {
            closed: 6,
            open: 6,
            openCritical: 0,
            openHigh: 2,
            openLow: 2,
            openMedium: 2,
          },
        },
      ],
      hasEssential: false,
      language: Language.En,
      managed: ManagedType.Managed,
      name: "TEST",
      totalOpenPriority: 0,
      userRole: "user-role",
    },
  };

  const apolloDataMock = [
    graphqlMocked.query(
      GET_FINDINGS,
      ({
        variables,
      }): StrictResponse<IErrorMessage | { data: GetFindings }> => {
        const {
          canGetRejectedVulnerabilities,
          canGetSubmittedVulnerabilities,
          groupName,
          root,
        } = variables;

        if (
          !canGetRejectedVulnerabilities &&
          !canGetSubmittedVulnerabilities &&
          isEqual(root, "back/src/model/user/index.js") &&
          groupName === "TEST"
        ) {
          return HttpResponse.json({ data: filterDataFinding });
        }

        if (
          !canGetRejectedVulnerabilities &&
          !canGetSubmittedVulnerabilities &&
          groupName === "TEST"
        ) {
          return HttpResponse.json({ data: apolloDataFinding });
        }

        return HttpResponse.json({
          errors: [new GraphQLError("Error getting group findings")],
        });
      },
    ),
    graphqlMocked.query(
      GET_FINDINGS,
      ({
        variables,
      }): StrictResponse<IErrorMessage | { data: GetFindings }> => {
        const {
          canGetRejectedVulnerabilities,
          canGetSubmittedVulnerabilities,
          groupName,
          root,
        } = variables;
        if (
          !canGetRejectedVulnerabilities &&
          !canGetSubmittedVulnerabilities &&
          isEqual(root, "") &&
          groupName === "TEST"
        ) {
          return HttpResponse.json({ data: apolloDataFinding });
        }

        return HttpResponse.json({
          errors: [new GraphQLError("Error getting group findings")],
        });
      },
    ),
    graphqlMocked.query(
      GET_FINDINGS,
      ({
        variables,
      }): StrictResponse<IErrorMessage | { data: GetFindings }> => {
        const {
          canGetRejectedVulnerabilities,
          canGetSubmittedVulnerabilities,
          groupName,
          root,
        } = variables;
        if (
          !canGetRejectedVulnerabilities &&
          !canGetSubmittedVulnerabilities &&
          isEqual(root, undefined) &&
          groupName === "TEST"
        ) {
          return HttpResponse.json({ data: apolloDataFinding });
        }

        return HttpResponse.json({
          errors: [new GraphQLError("Error getting group findings")],
        });
      },
    ),
    graphqlMocked.query(
      GET_ROOTS,
      ({ variables }): StrictResponse<IErrorMessage | { data: GetRoots }> => {
        const { groupName } = variables;
        if (groupName === "TEST") {
          return HttpResponse.json({
            data: {
              group: {
                __typename: "Group",
                name: "TEST",
                roots: [],
              },
            },
          });
        }

        return HttpResponse.json({
          errors: [new GraphQLError("Error getting group roots")],
        });
      },
    ),
  ];

  const mockReportError = graphqlMocked.query(
    REQUEST_GROUP_REPORT,
    ({
      variables,
    }): StrictResponse<IErrorMessage | { data: RequestGroupReport }> => {
      const { verificationCode, reportType, groupName } = variables;
      if (
        reportType === ReportType.Pdf &&
        verificationCode === "1234" &&
        groupName === "testgroup"
      ) {
        return HttpResponse.json({
          errors: [
            new GraphQLError(
              "Exception - The user already has a requested report for the same group",
            ),
          ],
        });
      }

      return HttpResponse.json({
        data: {
          report: {
            __typename: "Report",
            success: true,
          },
        },
      });
    },
  );

  it("should render report dropdown and send data report link", async (): Promise<void> => {
    expect.hasAssertions();

    localStorage.clear();
    sessionStorage.clear();
    jest.clearAllMocks();
    jest.spyOn(console, "warn").mockImplementation();

    render(<ReportComponent />, {
      memoryRouter: {
        initialEntries: ["/orgs/testorg/groups/testgroup/vulns"],
      },
      mocks: [
        ...apolloDataMock,
        graphqlMocked.query(
          REQUEST_GROUP_REPORT,
          (): StrictResponse<IErrorMessage | { data: RequestGroupReport }> => {
            return HttpResponse.json({
              data: {
                report: {
                  __typename: "Report",
                  success: true,
                },
              },
            });
          },
        ),
      ],
    });
    await waitFor((): void => {
      expect(
        screen.getByText("group.findings.report.data.text"),
      ).toBeInTheDocument();
    });
    await userEvent.click(screen.getByText("group.findings.report.data.text"));
    await userEvent.click(
      screen.getByText("components.channels.smsButton.text"),
    );
    await userEvent.type(
      screen.getByRole("textbox", { name: "verificationCode" }),
      "1234",
    );
    await userEvent.click(screen.getByText("verifyDialog.verify"));
    await waitFor((): void => {
      expect(msgSuccess).toHaveBeenCalledWith(
        "verifyDialog.alerts.sendMobileVerificationSuccess",
        "groupAlerts.titleSuccess",
      );
    });
    await waitFor((): void => {
      expect(msgSuccess).toHaveBeenCalledWith(
        "groupAlerts.reportRequested",
        "groupAlerts.titleSuccess",
      );
    });
  });

  it("should render report modal and mock request error", async (): Promise<void> => {
    expect.hasAssertions();

    jest.clearAllMocks();

    render(<ReportComponent />, {
      memoryRouter: {
        initialEntries: ["/orgs/testorg/groups/testgroup/vulns"],
      },
      mocks: [...apolloDataMock, mockReportError],
    });
    await waitFor((): void => {
      expect(
        screen.getByText("group.findings.report.pdf.text"),
      ).toBeInTheDocument();
    });

    // Find buttons
    const buttons: HTMLElement[] = screen.getAllByRole("button", {
      hidden: true,
    });

    ["fa-file-import"].forEach((expectedClass, idx): void => {
      expect(buttons[idx].querySelector(".fa-light")).toHaveClass(
        expectedClass,
      );
    });

    expect(
      screen.getByText("group.findings.report.cert.text"),
    ).toBeInTheDocument();
    expect(
      screen.getByText("group.findings.report.data.text"),
    ).toBeInTheDocument();
    expect(
      screen.getByText("group.findings.report.pdf.text"),
    ).toBeInTheDocument();
    expect(
      screen.getByText("group.findings.report.xls.text"),
    ).toBeInTheDocument();

    await userEvent.click(screen.getByText("group.findings.report.pdf.text"));

    expect(
      screen.getByText("components.channels.smsButton.text"),
    ).toBeInTheDocument();

    await userEvent.click(
      screen.getByText("components.channels.smsButton.text"),
    );
    await userEvent.type(
      screen.getByRole("textbox", { name: "verificationCode" }),
      "1234",
    );
    await userEvent.click(screen.getByText("verifyDialog.verify"));
    await waitFor((): void => {
      expect(msgError).toHaveBeenCalledWith(
        "groupAlerts.reportAlreadyRequested",
      );
    });
  });

  it("should not render certificate button on dropdown", async (): Promise<void> => {
    expect.hasAssertions();

    jest.clearAllMocks();

    render(<ReportComponent permission={""} />, {
      memoryRouter: {
        initialEntries: ["/orgs/testorg/groups/testgroup/vulns"],
      },
      mocks: [...apolloDataMock, mockReportError],
    });
    await waitFor((): void => {
      expect(
        screen.getByText("group.findings.report.pdf.text"),
      ).toBeInTheDocument();
    });

    expect(
      screen.queryByText("group.findings.report.cert.text"),
    ).not.toBeInTheDocument();
    expect(
      screen.getByText("group.findings.report.data.text"),
    ).toBeInTheDocument();
    expect(
      screen.getByText("group.findings.report.pdf.text"),
    ).toBeInTheDocument();
    expect(
      screen.getByText("group.findings.report.xls.text"),
    ).toBeInTheDocument();
  });

  it("should render report modal, check validation and mock request error", async (): Promise<void> => {
    expect.hasAssertions();

    jest.clearAllMocks();

    render(<ReportComponent />, {
      memoryRouter: {
        initialEntries: ["/orgs/testorg/groups/testgroup/vulns"],
      },
      mocks: [
        ...apolloDataMock,
        graphqlMocked.query(
          REQUEST_GROUP_REPORT,
          ({
            variables,
          }): StrictResponse<IErrorMessage | { data: RequestGroupReport }> => {
            const {
              verificationCode,
              reportType,
              groupName,
              closingDate,
              states,
            } = variables;

            if (
              reportType === ReportType.Xls &&
              verificationCode === "1234" &&
              groupName === "testgroup" &&
              closingDate === "2023-12-09" &&
              isEqual(states, ["SAFE"])
            ) {
              return HttpResponse.json({
                errors: [
                  new GraphQLError(
                    "Exception - The user already has a requested report for the same group",
                  ),
                ],
              });
            }

            return HttpResponse.json({
              data: {
                report: {
                  __typename: "Report",
                  success: true,
                },
              },
            });
          },
        ),
      ],
    });
    await waitFor((): void => {
      expect(
        screen.getByText("group.findings.report.pdf.text"),
      ).toBeInTheDocument();
    });

    expect(
      screen.queryByText("group.findings.report.filterReportDescription"),
    ).not.toBeInTheDocument();

    await userEvent.click(
      screen.getByText("group.findings.buttons.report.text"),
    );
    await userEvent.click(screen.getByTestId("customize-report"));

    await waitFor((): void => {
      expect(
        screen.queryByText("group.findings.report.filterReportDescription"),
      ).toBeInTheDocument();
    });

    expect(
      screen.queryAllByRole("checkbox", { name: /treatments/u }),
    ).toHaveLength(4);

    expect(
      screen.queryAllByRole("checkbox", { name: /verifications/u }),
    ).toHaveLength(4);

    expect(
      screen.getByText("group.findings.report.reattack.not_requested"),
    ).toBeInTheDocument();

    const closingDateName = "group.findings.report.closingDate.text";
    await userEvent.type(
      screen.getByRole("spinbutton", { name: `month, ${closingDateName}` }),
      "12",
    );
    await userEvent.type(
      screen.getByRole("spinbutton", { name: `day, ${closingDateName}` }),
      "09",
    );
    await userEvent.type(
      screen.getByRole("spinbutton", { name: `year, ${closingDateName}` }),
      "2023",
    );
    await waitFor((): void => {
      expect(
        screen.queryAllByRole("checkbox", { name: /treatments/u }),
      ).toHaveLength(0);
    });

    await userEvent.click(screen.getByRole("checkbox", { name: "states" }));

    await userEvent.click(
      screen.getByText("group.findings.report.generateXls"),
    );

    expect(
      screen.getByText("components.channels.smsButton.text"),
    ).toBeInTheDocument();

    await userEvent.click(
      screen.getByText("components.channels.smsButton.text"),
    );
    await userEvent.type(
      screen.getByRole("textbox", { name: "verificationCode" }),
      "1234",
    );
    await userEvent.click(screen.getByText("verifyDialog.verify"));
    await waitFor((): void => {
      expect(msgError).toHaveBeenCalledWith(
        "groupAlerts.reportAlreadyRequested",
      );
    });
  });
});
