import { useLazyQuery } from "@apollo/client";
import { Modal, useModal } from "@fluidattacks/design";
import mixpanel from "mixpanel-browser";
import { StrictMode, useCallback, useState } from "react";
import { useTranslation } from "react-i18next";
import { useParams } from "react-router-dom";

import { FilterForm } from "./filter-form";

import type { IDeactivationModalProps, IReportParams } from "../../types";
import { VerifyDialog } from "features/verify-dialog";
import type {
  ReportType,
  VulnerabilityState,
  VulnerabilityTreatment,
  VulnerabilityVerification,
} from "gql/graphql";
import { useAudit } from "hooks/use-audit";
import { GET_NOTIFICATIONS } from "pages/dashboard/navbar/downloads/queries";
import { REQUEST_GROUP_REPORT } from "pages/group/findings/queries";
import { Logger } from "utils/logger";
import { msgError, msgSuccess } from "utils/notifications";

const FilterReportModal = ({
  isOpen,
  onClose,
  typesOptions,
}: IDeactivationModalProps): JSX.Element => {
  const { groupName } = useParams() as { groupName: string };
  const { t } = useTranslation();
  const modalProps = useModal("filter-report");

  const [isVerifyDialogOpen, setIsVerifyDialogOpen] = useState(false);

  const handleClose = useCallback((): void => {
    onClose();
    setIsVerifyDialogOpen(false);
  }, [onClose, setIsVerifyDialogOpen]);

  const [requestGroupReport, { client }] = useLazyQuery(REQUEST_GROUP_REPORT, {
    onCompleted: (): void => {
      handleClose();
      setIsVerifyDialogOpen(false);
      msgSuccess(
        t("groupAlerts.reportRequested"),
        t("groupAlerts.titleSuccess"),
      );
      void client.refetchQueries({ include: [GET_NOTIFICATIONS] });
    },
    onError: (errors): void => {
      errors.graphQLErrors.forEach((error): void => {
        switch (error.message) {
          case "Exception - The user already has a requested report for the same group":
            msgError(t("groupAlerts.reportAlreadyRequested"));
            break;
          case "Exception - Stakeholder could not be verified":
            msgError(t("group.findings.report.alerts.nonVerifiedStakeholder"));
            break;
          case "Exception - The verification code is invalid":
            msgError(t("group.findings.report.alerts.invalidVerificationCode"));
            break;
          default:
            msgError(t("groupAlerts.errorTextsad"));
            Logger.warning("An error occurred requesting group report", error);
        }
      });
    },
  });

  const { addAuditEvent } = useAudit();
  const handleRequestGroupReport = useCallback(
    async (reportParams: IReportParams): Promise<void> => {
      const reportType = "XLS" as ReportType;
      mixpanel.track("GroupReportRequest", { reportType });
      addAuditEvent("GroupReportRequest", reportType);

      await requestGroupReport({
        variables: {
          groupName,
          reportType,
          ...reportParams,
          states: reportParams.states as VulnerabilityState[],
          treatments: reportParams.treatments as VulnerabilityTreatment[],
          verifications:
            reportParams.verifications as VulnerabilityVerification[],
        },
      });
    },
    [addAuditEvent, groupName, requestGroupReport],
  );

  return (
    <StrictMode>
      <Modal
        description={t("group.findings.report.filterReportDescription")}
        modalRef={{ ...modalProps, close: handleClose, isOpen }}
        size={"md"}
        title={t("group.findings.report.modalTitle")}
      >
        <VerifyDialog isOpen={isVerifyDialogOpen}>
          {(setVerifyCallbacks): JSX.Element => {
            return (
              <FilterForm
                onClose={handleClose}
                requestGroupReport={handleRequestGroupReport}
                setIsVerifyDialogOpen={setIsVerifyDialogOpen}
                setVerifyCallbacks={setVerifyCallbacks}
                typesOptions={typesOptions}
              />
            );
          }}
        </VerifyDialog>
      </Modal>
    </StrictMode>
  );
};

export { FilterReportModal };
