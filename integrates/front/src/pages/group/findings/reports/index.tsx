import { Button, Container, PremiumFeature } from "@fluidattacks/design";
import { StrictMode, useCallback, useRef, useState } from "react";
import { useTranslation } from "react-i18next";

import { ReportOptions } from "./report-options";
import type { IReportOptionsProps } from "./types";

import { UpgradeFreeTrialModal } from "features/upgrade-free-trial-users";
import { VerifyDialog } from "features/verify-dialog";
import { ManagedType } from "gql/graphql";
import { useCalendly } from "hooks/use-calendly";
import { useClickOutside } from "hooks/use-click-outside";
import { useTrial } from "hooks/use-trial";

const generateReportImg = "integrates/plans/generate-report";

const ReportsDropDown = ({
  enableCerts,
  typesOptions,
}: IReportOptionsProps): JSX.Element => {
  const { t } = useTranslation();
  const dropdownRef = useRef<HTMLDivElement>(null);
  const trial = useTrial();
  const { closeUpgradeModal, openUpgradeModal, isUpgradeOpen } = useCalendly();

  const [open, setOpen] = useState(false);
  const [isVerifyDialogOpen, setIsVerifyDialogOpen] = useState(false);
  const isInTrial = trial?.trial.state === ManagedType.Trial;

  const openDropdown = useCallback((): void => {
    if (isInTrial) {
      openUpgradeModal();
    } else {
      setOpen((previousState): boolean => !previousState);
    }
  }, [isInTrial, openUpgradeModal]);

  useClickOutside(
    dropdownRef.current,
    (): void => {
      setOpen(false);
    },
    true,
  );

  return (
    <StrictMode>
      <Container position={"relative"} ref={dropdownRef}>
        <Button
          icon={"file-import"}
          id={"reports"}
          onClick={openDropdown}
          tooltip={t("group.findings.buttons.report.tooltip")}
          variant={"primary"}
        >
          {t("group.findings.buttons.report.text")}
          {isInTrial && <PremiumFeature />}
        </Button>

        <VerifyDialog isOpen={isVerifyDialogOpen}>
          {(setVerifyCallbacks): JSX.Element => {
            const options = (
              <ReportOptions
                enableCerts={enableCerts}
                setIsVerifyDialogOpen={setIsVerifyDialogOpen}
                setVerifyCallbacks={setVerifyCallbacks}
                typesOptions={typesOptions}
              />
            );

            return (
              <Container
                display={open ? "block" : "none"}
                mt={0.25}
                position={"absolute"}
                right={"1px"}
                zIndex={2}
              >
                {options}
              </Container>
            );
          }}
        </VerifyDialog>
      </Container>
      {isUpgradeOpen && isInTrial && (
        <UpgradeFreeTrialModal
          description={t("group.findings.buttons.report.upgrade.description")}
          img={generateReportImg}
          onClose={closeUpgradeModal}
          title={t("group.findings.buttons.report.upgrade.title")}
        />
      )}
    </StrictMode>
  );
};

export { ReportsDropDown };
