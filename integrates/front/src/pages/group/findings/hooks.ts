import type { ApolloQueryResult } from "@apollo/client";
import { useQuery } from "@apollo/client";
import { useMemo } from "react";
import { useTranslation } from "react-i18next";

import { GET_FINDINGS } from "./queries";
import type { IFindingAttr, IVulnerabilitiesResume } from "./types";
import { formatFindings, isGroupInfoFilled } from "./utils";

import type {
  GetFindingsQueryQuery,
  GetFindingsQueryQueryVariables,
} from "gql/graphql";
import { useAudit } from "hooks/use-audit";
import { useAuthz } from "hooks/use-authz";
import { Logger } from "utils/logger";
import { msgError } from "utils/notifications";

type TGroup = GetFindingsQueryQuery["group"];

interface IFindingsQuery {
  filledGroupInfo: boolean;
  findings: IFindingAttr[];
  group: TGroup | undefined;
  hasEssential: boolean;
  loading: boolean;
  refetch: (
    variables?: Partial<GetFindingsQueryQueryVariables>,
  ) => Promise<ApolloQueryResult<GetFindingsQueryQuery>>;
}

const useFindingsQuery = (groupName: string): IFindingsQuery => {
  const { addAuditEvent } = useAudit();
  const { canResolve } = useAuthz();
  const { t } = useTranslation();

  const canGetHacker = canResolve("finding_hacker");
  const canGetRejectedVulnerabilities = canResolve(
    "finding_rejected_vulnerabilities",
  );
  const canGetSubmittedVulnerabilities = canResolve(
    "finding_submitted_vulnerabilities",
  );
  const canGetZRSummary = canResolve("finding_zero_risk_summary");

  const { data, loading, refetch } = useQuery(GET_FINDINGS, {
    fetchPolicy: "network-only",
    onCompleted: (): void => {
      addAuditEvent("Group.Findings", groupName);
    },
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error): void => {
        msgError(t("groupAlerts.errorTextsad"));
        Logger.warning("An error occurred loading group data", error);
      });
    },
    variables: {
      canGetHacker,
      canGetRejectedVulnerabilities,
      canGetSubmittedVulnerabilities,
      canGetZRSummary,
      groupName,
    },
  });

  const findings = useMemo(
    (): IFindingAttr[] =>
      formatFindings(
        (data?.group.findings ?? []) as IFindingAttr[],
        {} as Record<string, IVulnerabilitiesResume>,
      ),
    [data],
  );

  return {
    filledGroupInfo: isGroupInfoFilled(data),
    findings,
    group: data?.group,
    hasEssential: data?.group.hasEssential ?? false,
    loading,
    refetch,
  };
};

export { useFindingsQuery };
