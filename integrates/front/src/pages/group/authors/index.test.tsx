import { PureAbility } from "@casl/ability";
import { act, screen, waitFor, within } from "@testing-library/react";
import { HttpResponse, type StrictResponse, graphql } from "msw";
import { Route, Routes } from "react-router-dom";

import { GroupAuthors } from ".";
import { authzPermissionsContext } from "context/authz/config";
import type { GetAuthorsQuery, GetStakeholdersQuery } from "gql/graphql";
import { render } from "mocks";
import { LINK } from "mocks/constants";
import type { IErrorMessage } from "mocks/types";
import { GET_AUTHORS } from "pages/group/queries";
import { GET_STAKEHOLDERS } from "pages/group/queries";

const Wrapper = ({
  mockedPermissions = new PureAbility([]),
}: Readonly<{
  mockedPermissions?: PureAbility<string>;
}>): JSX.Element => (
  <authzPermissionsContext.Provider value={mockedPermissions}>
    <Routes>
      <Route
        element={<GroupAuthors />}
        path={"/orgs/:organizationName/groups/:groupName/authors"}
      />
    </Routes>
  </authzPermissionsContext.Provider>
);

describe("groupAuthors", (): void => {
  const TEST_DATE = 2020;
  const date: Date = new Date(TEST_DATE, 0);

  const graphqlMocked = graphql.link(LINK);

  const mocks = [
    graphqlMocked.query(
      GET_AUTHORS,
      ({
        variables,
      }): StrictResponse<IErrorMessage | { data: GetAuthorsQuery }> => {
        const { date: dateRange } = variables;

        if (dateRange === "10/2019") {
          return HttpResponse.json({
            data: {
              group: {
                __typename: "Group",
                billing: {
                  authors: [
                    {
                      actor: "october",
                      commit: "234567732",
                      groups: ["test, test2"],
                      organization: "testorg",
                      repository: "test_repository",
                    },
                  ],
                },
                name: "oneshottest",
              },
            },
          });
        }

        return HttpResponse.json({
          data: {
            group: {
              __typename: "Group",
              billing: {
                authors: [
                  {
                    actor: "january",
                    commit: "123",
                    groups: ["test, test2"],
                    organization: "okada",
                    repository: "test_repository",
                  },
                ],
              },
              name: "oneshottest",
            },
          },
        });
      },
    ),
    graphqlMocked.query(
      GET_STAKEHOLDERS,
      (): StrictResponse<IErrorMessage | { data: GetStakeholdersQuery }> => {
        return HttpResponse.json({
          data: {
            group: {
              __typename: "Group",
              name: "oneshottest",
              stakeholders: [],
            },
          },
        });
      },
    ),
  ];

  it("should render a component", async (): Promise<void> => {
    expect.hasAssertions();

    jest.useFakeTimers().setSystemTime(date);

    render(<Wrapper />, {
      memoryRouter: {
        initialEntries: ["/orgs/okada/groups/oneshottest/authors"],
      },
    });

    await waitFor((): void => {
      expect(screen.queryByRole("table")).toBeInTheDocument();
    });

    expect(screen.getAllByRole("columnheader")).toHaveLength(4);
    expect(
      screen.getAllByRole("row")[0].textContent?.replace(/[^a-zA-Z ]/gu, ""),
    ).toStrictEqual(
      [
        "group.authors.actor",
        "group.authors.groupsContributed",
        "group.authors.commit",
        "group.authors.repository",
      ]
        .join("")
        .replace(/[^a-zA-Z ]/gu, ""),
    );

    jest.clearAllMocks();
    jest.useRealTimers();
  });

  it("should render table", async (): Promise<void> => {
    expect.hasAssertions();

    jest.useFakeTimers().setSystemTime(date);

    const mockedPermissions = new PureAbility<string>([
      { action: "integrates_api_mutations_grant_stakeholder_access_mutate" },
      {
        action: "integrates_api_resolvers_query_stakeholder__resolve_for_group",
      },
    ]);

    render(<Wrapper mockedPermissions={mockedPermissions} />, {
      memoryRouter: {
        initialEntries: ["/orgs/okada/groups/unittesting/authors"],
      },
    });

    const TEST_COLUMN_LENGTH = 4;
    await waitFor((): void => {
      expect(screen.queryAllByRole("columnheader")).toHaveLength(
        TEST_COLUMN_LENGTH,
      );
    });

    expect(screen.getByRole("table")).toBeInTheDocument();

    await waitFor((): void => {
      expect(
        screen.getAllByRole("row")[0].textContent?.replace(/[^a-zA-Z ]/gu, ""),
      ).toStrictEqual(
        [
          "group.authors.actor",
          "group.authors.groupsContributed",
          "group.authors.commit",
          "group.authors.repository",
          "searchFindings.usersTable.invitationState",
        ]
          .join("")
          .replace(/[^a-zA-Z ]/gu, ""),
      );
    });

    expect(screen.getAllByRole("cell")[0].textContent).toBe("test");
    expect(screen.getAllByRole("cell")[1].textContent).toBe("test, test2");
    expect(screen.getAllByRole("cell")[2].textContent).toBe("123");
    expect(
      screen.getAllByRole("cell")[TEST_COLUMN_LENGTH - 1].textContent,
    ).toBe("test_repository");
    expect(
      within(screen.getAllByRole("cell")[TEST_COLUMN_LENGTH]).getByText(
        "group.authors.sendInvitation",
      ),
    ).toBeInTheDocument();
  });

  it("should render information accordlyn to selected period", async (): Promise<void> => {
    expect.hasAssertions();

    jest.useFakeTimers().setSystemTime(date);

    render(<Wrapper />, {
      memoryRouter: {
        initialEntries: ["/orgs/okada/groups/oneshottest/authors"],
      },
      mocks,
    });

    await waitFor((): void => {
      expect(screen.getByRole("table")).toBeInTheDocument();
    });
    await waitFor((): void => {
      expect(screen.getByText("january")).toBeInTheDocument();
    });

    expect(screen.getByText("01/2020")).toBeInTheDocument();

    act((): void => {
      screen.getByText("01/2020").click();
    });

    expect(screen.getByText("10/2019")).toBeInTheDocument();

    act((): void => {
      screen.getByText("10/2019").click();
    });
    await waitFor((): void => {
      expect(screen.getByText("october")).toBeInTheDocument();
    });

    expect(screen.queryByText("january")).not.toBeInTheDocument();

    jest.clearAllMocks();
    jest.useRealTimers();
  });

  jest.clearAllMocks();
  jest.useRealTimers();
});
