import { useMutation, useQuery } from "@apollo/client";
import { useAbility } from "@casl/react";
import type { IOptionsProps } from "@fluidattacks/design";
import {
  AppliedFilters,
  Button,
  Container,
  Text,
  useFilters,
} from "@fluidattacks/design";
import type { IOptions } from "@fluidattacks/design/dist/components/modal/filters-modal/types";
import type { ColumnDef } from "@tanstack/react-table";
import dayjs from "dayjs";
import range from "lodash/range";
import mixpanel from "mixpanel-browser";
import type { ReactElement } from "react";
import { useCallback, useEffect, useMemo, useState } from "react";
import * as React from "react";
import { useTranslation } from "react-i18next";
import { useParams } from "react-router-dom";
import { useTheme } from "styled-components";

import type { IAuthors } from "./types";

import { useAuthors, useStakeholders } from "../hooks";
import { Dropdown } from "components/dropdown";
import type { IDropDownOption } from "components/dropdown/types";
import { Table } from "components/table";
import { Can } from "context/authz/can";
import { authzPermissionsContext } from "context/authz/config";
import { statusFormatter } from "features/vulnerabilities/formatters/status";
import type { StakeholderRole } from "gql/graphql";
import { useTable } from "hooks";
import { ADD_STAKEHOLDER_MUTATION } from "pages/group/members/queries";
import { handleGrantError } from "pages/group/members/utils";
import { GET_AUTHORS } from "pages/group/queries";
import { Logger } from "utils/logger";
import { msgError, msgSuccess } from "utils/notifications";

const DATE_RANGE = 12;

function commitFormatter(value: string): string {
  const COMMIT_LENGTH = 7;

  return value.slice(0, COMMIT_LENGTH);
}

const formatText = (value: string): ReactElement<Text> => (
  <p className={"word-wrap"}>{value}</p>
);

const formatCommit = (value: string): ReactElement<Text> => (
  <p className={"word-wrap"}>{commitFormatter(value)}</p>
);

function groupFormatter(value: string[]): string {
  const result = value.map((group): string => group).join(", ");

  return result;
}

const formatGroup = (value: string[]): ReactElement<Text> => (
  <p className={"word-wrap"}>{groupFormatter(value)}</p>
);

type TAuthorData = IAuthors & { invitationState: string };

const GroupAuthors: React.FC = (): JSX.Element => {
  const { groupName } = useParams() as { groupName: string };
  const { t } = useTranslation();
  const theme = useTheme();
  const permissions = useAbility(authzPermissionsContext);
  const [filteredData, setFilteredData] = useState<TAuthorData[]>([]);

  const dateRange = useMemo(
    (): string[] =>
      range(0, DATE_RANGE).map((month): string =>
        dayjs().subtract(month, "month").format("MM/YYYY"),
      ),
    [],
  );
  const [billingDate, setBillingDate] = useState(dateRange[0]);

  const handleSelection = useCallback(
    (selection: IDropDownOption): void => {
      setBillingDate(selection.value ?? dateRange[0]);
    },
    [dateRange],
  );

  const {
    data: stakeholderData,
    loading: loadingStakeholders,
    refetch,
  } = useStakeholders(groupName, true);

  const { data } = useAuthors(groupName, billingDate);

  useQuery(GET_AUTHORS, {
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error): void => {
        msgError(t("groupAlerts.errorTextsad"));
        Logger.warning("An error occurred getting billing data", error);
      });
    },
    variables: { date: billingDate, groupName },
  });

  const [grantAccess, { loading }] = useMutation(ADD_STAKEHOLDER_MUTATION, {
    onCompleted: (mtResult): void => {
      if (mtResult.grantStakeholderAccess.success) {
        void refetch();
        mixpanel.track("AddUserAccess");
        const { email } =
          mtResult.grantStakeholderAccess.grantedStakeholder ?? {};
        msgSuccess(
          `${t("searchFindings.tabUsers.success")} ${email}`,
          t("searchFindings.tabUsers.titleSuccess"),
        );
      }
    },
    onError: handleGrantError,
  });

  const formatInvitation = useCallback(
    (actorEmail: string): string => {
      const invitationState =
        stakeholderData === undefined
          ? ""
          : stakeholderData.group.stakeholders.reduce(
              (previousValue, stakeholder): string =>
                stakeholder.email?.toLocaleLowerCase() ===
                actorEmail.toLocaleLowerCase()
                  ? (stakeholder.invitationState ?? "")
                  : previousValue,
              "",
            );
      if (invitationState === "REGISTERED") {
        return t("group.authors.invitationState.confirmed");
      }
      if (invitationState === "PENDING") {
        return t("group.authors.invitationState.pending");
      }

      return t("group.authors.invitationState.unregistered");
    },
    [stakeholderData, t],
  );

  const hasInvitationPermissions = useMemo(
    (): boolean =>
      stakeholderData !== undefined &&
      permissions.can(
        "integrates_api_resolvers_query_stakeholder__resolve_for_group",
      ) &&
      permissions.can(
        "integrates_api_mutations_grant_stakeholder_access_mutate",
      ),
    [permissions, stakeholderData],
  );

  const tableRef = useTable("tblAuthorsList", {
    invitation: hasInvitationPermissions,
  });

  const columns = useMemo(
    (): ColumnDef<TAuthorData>[] => [
      {
        accessorKey: "actor",
        cell: (cell): JSX.Element => formatText(String(cell.getValue())),
        header: t("group.authors.actor"),
      },
      {
        accessorKey: "groups",
        cell: (cell): JSX.Element => formatGroup(cell.getValue<string[]>()),
        header: t("group.authors.groupsContributed"),
      },
      {
        accessorKey: "commit",
        cell: (cell): JSX.Element => formatCommit(String(cell.getValue())),
        header: t("group.authors.commit"),
      },
      {
        accessorKey: "repository",
        cell: (cell): JSX.Element => formatText(String(cell.getValue())),
        header: t("group.authors.repository"),
      },
      {
        accessorKey: "invitation",
        cell: (cell): JSX.Element => cell.getValue<JSX.Element>(),
        header: t("searchFindings.usersTable.invitationState"),
      },
    ],
    [t],
  );

  useEffect((): void => {
    tableRef.setColumnVisibility({
      invitation: hasInvitationPermissions,
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [hasInvitationPermissions]);

  const stakeholdersEmail = useMemo(
    (): string[] =>
      stakeholderData === undefined
        ? []
        : stakeholderData.group.stakeholders
            .map(
              (stakeholder): string =>
                stakeholder.email?.toLocaleLowerCase() ?? "",
            )
            .filter(Boolean),
    [stakeholderData],
  );

  const handleSendInvitation = useCallback(
    (
      actorEmail: string,
    ): ((event: React.MouseEvent<HTMLButtonElement>) => void) => {
      return (event: React.MouseEvent<HTMLButtonElement>): void => {
        event.stopPropagation();

        const resendStakeholder = {
          email: actorEmail.toLocaleLowerCase(),
          groupName,
          responsibility: "",
          role: "USER" as StakeholderRole,
        };
        void grantAccess({
          variables: {
            ...resendStakeholder,
          },
        });
      };
    },
    [grantAccess, groupName],
  );

  const dataset = useMemo(
    (): IAuthors[] =>
      data === undefined
        ? []
        : data.group.billing.authors.map((value): IAuthors => {
            const { actor } = value;
            const place = actor.lastIndexOf("<");
            const actorEmail =
              place >= 0 ? actor.substring(place + 1, actor.length - 1) : actor;

            if (stakeholderData === undefined) {
              return {
                ...(value as IAuthors),
                invitation: <React.StrictMode />,
              };
            }

            if (stakeholdersEmail.includes(actorEmail.toLowerCase())) {
              return {
                ...(value as IAuthors),
                invitation: (
                  <React.StrictMode>
                    {statusFormatter(
                      formatInvitation(actorEmail.toLowerCase()),
                    )}
                  </React.StrictMode>
                ),
              };
            }

            return {
              ...(value as IAuthors),
              invitation: (
                <Can
                  do={
                    "integrates_api_mutations_grant_stakeholder_access_mutate"
                  }
                >
                  <Button
                    disabled={loading || loadingStakeholders}
                    onClick={handleSendInvitation(actorEmail)}
                    tooltip={t("group.authors.tooltip.text")}
                    variant={"secondary"}
                  >
                    {t("group.authors.sendInvitation")}
                  </Button>
                </Can>
              ),
            };
          }),

    [
      data,
      formatInvitation,
      handleSendInvitation,
      loading,
      loadingStakeholders,
      stakeholderData,
      stakeholdersEmail,
      t,
    ],
  );

  const filterOptions: IOptionsProps<TAuthorData>[] = useMemo(
    (): IOptionsProps<TAuthorData>[] => [
      {
        filterOptions: [
          {
            key: "actor",
            label: t("group.authors.actor"),
            options: dataset.map((value): IOptions => {
              return {
                label: value.actor,
                value: value.actor,
              };
            }),
            type: "checkboxes",
          },
        ],
        label: t("group.authors.actor"),
      },
      {
        filterOptions: [
          {
            filterFn: "includesInsensitive",
            key: "groups",
            label: t("group.authors.groupsContributed"),
            type: "text",
          },
        ],
        label: t("group.authors.groupsContributed"),
      },
      {
        filterOptions: [
          {
            key: "repository",
            label: `${t("group.authors.repository")} is`,
            type: "text",
          },
          {
            filterFn: "includesInsensitive",
            key: "repository",
            label: `or ${t("group.authors.repository")} contains`,
            type: "text",
          },
        ],
        label: t("group.authors.repository"),
      },
    ],
    [dataset, t],
  );

  const datasetText = useMemo(
    (): TAuthorData[] =>
      dataset.map((value): TAuthorData => {
        const { actor } = value;
        const place = actor.lastIndexOf("<");
        const actorEmail =
          place >= 0 ? actor.substring(place + 1, actor.length - 1) : actor;

        return {
          ...value,
          invitationState: formatInvitation(actorEmail.toLowerCase()),
        };
      }),
    [dataset, formatInvitation],
  );

  const dropdownItems = useMemo(
    (): IDropDownOption[] =>
      dateRange.map((date): IDropDownOption => {
        return { value: date };
      }),
    [dateRange],
  );

  const { Filters, options, removeFilter } = useFilters({
    dataset: datasetText,
    localStorageKey: `tblAuthorsList-${groupName}`,
    options: filterOptions,
    setFilteredDataset: setFilteredData,
  });

  return (
    <Container
      bgColor={theme.palette.gray[50]}
      display={"flex"}
      flexDirection={"column"}
      gap={1.25}
      height={"100hv"}
      id={"users"}
      px={1.25}
      py={1.25}
    >
      <Text color={theme.palette.gray[800]} size={"sm"}>
        {t("group.authors.tableAdvice")}
      </Text>
      <Dropdown
        customSelectionHandler={handleSelection}
        items={dropdownItems}
      />
      <Table
        columns={columns}
        csvConfig={{ export: true }}
        data={filteredData}
        filters={<Filters />}
        filtersApplied={
          <AppliedFilters onClose={removeFilter} options={options} />
        }
        loadingData={loadingStakeholders}
        tableRef={tableRef}
      />
    </Container>
  );
};

export { GroupAuthors };
