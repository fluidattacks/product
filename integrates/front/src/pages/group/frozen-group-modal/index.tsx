import { PopUp } from "@fluidattacks/design";
import * as React from "react";
import { useTranslation } from "react-i18next";
import { useNavigate, useParams } from "react-router-dom";

import type { IFrozenGroupModalProps } from "./types";

import { openUrl } from "utils/resource-helpers";

const FrozenGroupModal: React.FC<IFrozenGroupModalProps> = ({
  isOpen,
}): JSX.Element | null => {
  const { t } = useTranslation();
  const navigate = useNavigate();
  const { organizationName, groupName } = useParams() as {
    organizationName: string;
    groupName: string;
  };

  if (isOpen) {
    return (
      <PopUp
        _portal={true}
        cancelButton={{
          key: "frozenCancelButton",
          onClick: (): void => {
            openUrl("mailto:help@fluidattacks.com");
          },
          text: t("group.frozen.secondButton"),
          variant: "primary",
        }}
        confirmButton={{
          key: "frozenConfirmButton",
          onClick: (): void => {
            navigate(`/orgs/${organizationName}/billing`);
          },
          text: t("group.frozen.firstButton"),
          variant: "tertiary",
        }}
        container={null}
        darkBackground={true}
        description={t("group.frozen.description")}
        highlightDescription={[t("group.frozen.highlight")]}
        image={{
          alt: "frozenGroupImg",
          height: "110px",
          src: "integrates/resources/frozenGroup",
          width: "110px",
        }}
        maxWidth={"738px"}
        title={t("group.frozen.title", { groupName })}
      />
    );
  }

  return null;
};

export { FrozenGroupModal };
