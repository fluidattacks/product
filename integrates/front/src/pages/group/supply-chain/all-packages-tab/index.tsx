import { useCallback } from "react";
import * as React from "react";

import { useFilters, usePackagesQuery } from "../hooks";
import { PackagesTable } from "../packages-table";
import { RequestSbomModal } from "../request-sbom-modal";
import { useSearch } from "components/search/hooks";
import { useModal } from "hooks/use-modal";

interface IAllPackagesTabProps {
  readonly groupName: string;
}

const AllPackagesTab: React.FC<IAllPackagesTabProps> = ({
  groupName,
}): React.JSX.Element => {
  const [search, setSearch] = useSearch();
  const [filters, setFilters, options] = useFilters();
  const packagesQuery = usePackagesQuery(groupName, search, filters);

  const requestSbomModal = useModal("request-sbom-modal");
  const openSbomModal = useCallback((): void => {
    requestSbomModal.open();
  }, [requestSbomModal]);

  return (
    <div>
      <PackagesTable
        localStorageKey={`tblToeAllPackagesFilters-${groupName}`}
        onFilter={setFilters}
        onRequestSbom={openSbomModal}
        onSearch={setSearch}
        options={options}
        packagesQuery={packagesQuery}
      />
      {requestSbomModal.isOpen ? (
        <RequestSbomModal groupName={groupName} modalRef={requestSbomModal} />
      ) : undefined}
    </div>
  );
};

export { AllPackagesTab };
