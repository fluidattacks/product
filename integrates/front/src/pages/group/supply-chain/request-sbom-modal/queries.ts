import { graphql } from "gql/gql";

const GET_ACTIVE_RESOURCES = graphql(`
  query GetActiveResources(
    $after: String
    $first: Int!
    $groupName: String!
    $search: String
  ) {
    group(groupName: $groupName) {
      name
      dockerImages(include: true) {
        uri
        credentials {
          id
        }
      }
      gitRoots(after: $after, first: $first, search: $search, state: ACTIVE) {
        total
        edges {
          node {
            id
            nickname
          }
        }
        pageInfo {
          endCursor
          hasNextPage
        }
      }
    }
  }
`);

const REQUEST_SBOM_FILE_MUTATION = graphql(`
  mutation RequestSbomFileMutation(
    $fileFormat: String!
    $groupName: String!
    $rootNicknames: [String!]
    $sbomFormat: String!
    $urisWithCredentials: [UriWithCredentialsInput!]
  ) {
    requestSbomFile(
      fileFormat: $fileFormat
      groupName: $groupName
      rootNicknames: $rootNicknames
      sbomFormat: $sbomFormat
      urisWithCredentials: $urisWithCredentials
    ) {
      success
    }
  }
`);

export { GET_ACTIVE_RESOURCES, REQUEST_SBOM_FILE_MUTATION };
