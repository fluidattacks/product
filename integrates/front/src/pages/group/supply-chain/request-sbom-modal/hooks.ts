import type { FetchResult } from "@apollo/client";
import { useMutation, useQuery } from "@apollo/client";
import mixpanel from "mixpanel-browser";

import { GET_ACTIVE_RESOURCES, REQUEST_SBOM_FILE_MUTATION } from "./queries";

import type {
  GetActiveResourcesQuery,
  RequestSbomFileMutationMutation,
  RequestSbomFileMutationMutationVariables,
} from "gql/graphql";
import { GET_NOTIFICATIONS } from "pages/dashboard/navbar/downloads/queries";
import { Logger } from "utils/logger";
import { msgError } from "utils/notifications";
import { translate } from "utils/translations/translate";

type TRoot = GetActiveResourcesQuery["group"]["gitRoots"]["edges"][0]["node"];
type TDockerImage = GetActiveResourcesQuery["group"]["dockerImages"][0];

interface IUseActiveRootsQuery {
  fetchNextPage: () => Promise<void>;
  hasNextPage: boolean;
  loading: boolean;
  roots: TRoot[];
  images: TDockerImage[];
  total: number;
}

const useActiveRootsQuery = (
  groupName: string,
  search: string | undefined,
): IUseActiveRootsQuery => {
  const { data, fetchMore, loading } = useQuery(GET_ACTIVE_RESOURCES, {
    fetchPolicy: "network-only",
    nextFetchPolicy: "cache-first",
    variables: {
      first: 20,
      groupName,
      search: search === "" ? undefined : search,
    },
  });

  const edges = data?.group.gitRoots.edges ?? [];
  const pageInfo = data?.group.gitRoots.pageInfo ?? {
    endCursor: [],
    hasNextPage: false,
  };
  const filteredImages =
    search?.trim() === ""
      ? data?.group.dockerImages
      : data?.group.dockerImages.filter((image): boolean =>
          image.uri.toLowerCase().includes(search?.toLowerCase() ?? ""),
        );

  return {
    fetchNextPage: async (): Promise<void> => {
      await fetchMore({ variables: { after: pageInfo.endCursor } });
    },
    hasNextPage: pageInfo.hasNextPage,
    images: filteredImages ?? [],
    loading,
    roots: edges.map((edge): TRoot => edge.node),
    total: (data?.group.gitRoots.total ?? 0) + (filteredImages?.length ?? 0),
  };
};

interface IUseRequestSbomFileMutation {
  requestSbomFile: (
    values: RequestSbomFileMutationMutationVariables,
  ) => Promise<FetchResult<RequestSbomFileMutationMutation>>;
}

const useRequestSbomFileMutation = (): IUseRequestSbomFileMutation => {
  const [requestSbomFile] = useMutation(REQUEST_SBOM_FILE_MUTATION, {
    onCompleted: (): void => {
      mixpanel.track("RequestSbomFile");
    },
    onError: (error): void => {
      error.graphQLErrors.forEach(({ message }): void => {
        switch (message) {
          case "Exception - Sbom file request was rejected ":
            msgError(translate.t("group.toe.packages.requestError"));
            break;
          case "Exception - Invalid format: ":
            msgError("Invalid format");
            break;
          default:
            msgError(translate.t("groupAlerts.accessDenied"));
            Logger.warning("Access denied", error);
        }
      });
    },
    refetchQueries: [GET_NOTIFICATIONS],
  });

  return {
    requestSbomFile: async (
      variables: RequestSbomFileMutationMutationVariables,
    ): Promise<FetchResult<RequestSbomFileMutationMutation>> => {
      return requestSbomFile({ variables });
    },
  };
};

export { useActiveRootsQuery, useRequestSbomFileMutation };
