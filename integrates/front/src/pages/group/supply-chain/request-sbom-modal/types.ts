import type { IUseModal } from "hooks/use-modal";

interface IFormValues {
  sbomFormat: string;
  fileFormat: string;
  rootNicknames: string[];
  uris: string[];
}

interface IRequestSbomModalProps {
  groupName: string;
  modalRef: IUseModal;
}

export type { IRequestSbomModalProps, IFormValues };
