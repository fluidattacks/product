import { graphql } from "gql/gql";

const GET_ROOTS = graphql(`
  query GetRoots(
    $after: String
    $first: Int
    $groupName: String!
    $search: String
  ) {
    group(groupName: $groupName) {
      name
      gitRoots(after: $after, first: $first, search: $search, state: ACTIVE) {
        total
        edges {
          node {
            id
            branch
            nickname
            url
            toePackages(first: 0) {
              total
            }
          }
        }
        pageInfo {
          endCursor
          hasNextPage
        }
      }
    }
  }
`);

export { GET_ROOTS };
