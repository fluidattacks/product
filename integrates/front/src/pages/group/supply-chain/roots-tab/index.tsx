import type { ColumnDef } from "@tanstack/react-table";
import * as React from "react";

import { formatPackagesColumn } from "./formatters/format-details-column";
import type { TRoot } from "./hooks";
import { useRootsQuery } from "./hooks";

import { useSearch } from "components/search/hooks";
import { Table } from "components/table";
import { useTable } from "hooks";

interface IRootsTab {
  readonly groupName: string;
}

const RootsTab: React.FC<IRootsTab> = ({ groupName }): React.JSX.Element => {
  const [search, setSearch] = useSearch();
  const { fetchNextPage, hasNextPage, loading, roots, total } = useRootsQuery(
    groupName,
    search,
  );

  const tableRef = useTable("supply-chain-roots-table");
  const columns: ColumnDef<TRoot>[] = [
    { accessorKey: "url", header: "URL" },
    { accessorKey: "branch", header: "Branch" },
    { accessorKey: "nickname", header: "Nickname" },
    { cell: formatPackagesColumn, header: "Packages" },
  ];

  return (
    <div>
      <Table
        columns={columns}
        data={roots}
        loadingData={loading}
        onNextPage={fetchNextPage}
        onSearch={setSearch}
        options={{ enableSorting: false, hasNextPage, size: total }}
        tableRef={tableRef}
      />
    </div>
  );
};

export { RootsTab };
