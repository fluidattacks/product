import { useQuery } from "@apollo/client";

import { GET_ROOTS } from "./queries";

import type { GetRootsQuery } from "gql/graphql";

type TRoot = GetRootsQuery["group"]["gitRoots"]["edges"][0]["node"];

interface IUsePackagesQuery {
  fetchNextPage: () => Promise<void>;
  hasNextPage: boolean;
  loading: boolean;
  roots: TRoot[];
  total: number;
}

const useRootsQuery = (
  groupName: string,
  search: string | undefined,
): IUsePackagesQuery => {
  const { data, fetchMore, loading } = useQuery(GET_ROOTS, {
    fetchPolicy: "cache-and-network",
    nextFetchPolicy: "cache-first",
    variables: {
      first: 50,
      groupName,
      search: search === "" ? undefined : search,
    },
  });

  const edges = data?.group.gitRoots.edges ?? [];
  const pageInfo = data?.group.gitRoots.pageInfo ?? {
    endCursor: "",
    hasNextPage: false,
  };

  return {
    fetchNextPage: async (): Promise<void> => {
      await fetchMore({ variables: { after: pageInfo.endCursor } });
    },
    hasNextPage: pageInfo.hasNextPage,
    loading,
    roots: edges.map((edge): TRoot => edge.node),
    total: data?.group.gitRoots.total ?? 0,
  };
};

export type { TRoot };
export { useRootsQuery };
