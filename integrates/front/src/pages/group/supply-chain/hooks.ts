import { useQuery } from "@apollo/client";
import type { IFilterOptions, IOptionsProps } from "@fluidattacks/design";
import _ from "lodash";
import { useMemo, useState } from "react";
import { useTranslation } from "react-i18next";
import { useLocation } from "react-router-dom";

import { GET_PACKAGES, GET_TOTALS } from "./queries";

import { getCheckedFilter, getCheckedFilters } from "components/filter/utils";
import type { GetPackagesQuery } from "gql/graphql";
import { useAudit } from "hooks/use-audit";

type TPackageFilters = IFilterOptions<TPackage>;

const useFilters = (): [
  TPackageFilters[],
  (value: TPackageFilters[]) => void,
  options: IOptionsProps<TPackage>[],
] => {
  const { t } = useTranslation();

  const filterOptions: IOptionsProps<TPackage>[] = useMemo(
    (): IOptionsProps<TPackage>[] => [
      {
        filterOptions: [
          {
            key: "platform",
            label: t("group.tabs.toe.packages.allPackages.manager"),
            options: [
              { label: "Cabal", value: "CABAL" },
              { label: "Cargo", value: "CARGO" },
              { label: "Composer", value: "COMPOSER" },
              { label: "Conan", value: "CONAN" },
              { label: "CRAN", value: "CRAN" },
              { label: "Erlang", value: "ERLANG" },
              { label: "Go", value: "GO" },
              { label: "Maven", value: "MAVEN" },
              { label: "npm", value: "NPM" },
              { label: "NuGet", value: "NUGET" },
              { label: "pip", value: "PIP" },
              { label: "Pub", value: "PUB" },
              { label: "RubyGems", value: "GEM" },
              { label: "SwiftPM", value: "SWIFT" },
            ],
            type: "checkboxes",
          },
        ],
        label: t("group.tabs.toe.packages.allPackages.manager"),
      },
      {
        filterOptions: [
          {
            key: "vulnerable",
            label: t("group.tabs.toe.packages.allPackages.vulnerable"),
            options: [
              { label: "Yes", value: "YES" },
              { label: "No", value: "NO" },
            ],
            type: "radioButton",
          },
        ],
        label: t("group.tabs.toe.packages.allPackages.vulnerable"),
      },
      {
        filterOptions: [
          {
            key: "outdated",
            label: t("group.tabs.toe.packages.allPackages.outdated"),
            options: [
              { label: "Yes", value: "YES" },
              { label: "No", value: "NO" },
            ],
            type: "radioButton",
          },
        ],
        label: t("group.tabs.toe.packages.allPackages.outdated"),
      },
    ],
    [t],
  );

  const [filters, setFilters] = useState<TPackageFilters[]>([]);

  return [filters, setFilters, filterOptions];
};

type TPackage = GetPackagesQuery["group"]["toePackages"]["edges"][0]["node"];

interface IUsePackagesQuery {
  fetchNextPage: () => Promise<void>;
  hasNextPage: boolean;
  loading: boolean;
  packages: TPackage[];
  total: number;
}

const usePackagesQuery = (
  groupName: string,
  search: string | undefined,
  filters: TPackageFilters[],
): IUsePackagesQuery => {
  const platforms = getCheckedFilters(filters, "platform");
  const vulnerable = getCheckedFilter(filters, "vulnerable")?.value;
  const outdated = getCheckedFilter(filters, "outdated")?.value;

  const { addAuditEvent } = useAudit();
  const { data, fetchMore, loading } = useQuery(GET_PACKAGES, {
    fetchPolicy: "cache-and-network",
    nextFetchPolicy: "cache-first",
    onCompleted: (): void => {
      addAuditEvent("Group.ToePackages", groupName);
    },
    variables: {
      first: 101,
      groupName,
      outdated: { NO: false, YES: true }[outdated ?? ""],
      platforms:
        _.isEmpty(platforms) || _.isNil(platforms) ? undefined : platforms,
      search: search === "" ? undefined : search,
      vulnerable: { NO: false, YES: true }[vulnerable ?? ""],
    },
  });

  const edges = data?.group.toePackages.edges ?? [];
  const pageInfo = data?.group.toePackages.pageInfo ?? {
    endCursor: "",
    hasNextPage: false,
  };

  return {
    fetchNextPage: async (): Promise<void> => {
      await fetchMore({ variables: { after: pageInfo.endCursor } });
    },
    hasNextPage: pageInfo.hasNextPage,
    loading,
    packages: edges.map((edge): TPackage => edge.node),
    total: data?.group.toePackages.total ?? 0,
  };
};

interface IUseTotalsQuery {
  dockerImagesTotal?: number;
  packagesTotal?: number;
  rootsTotal?: number;
}

const useTotalsQuery = (groupName: string): IUseTotalsQuery => {
  const { pathname } = useLocation();
  const { data } = useQuery(GET_TOTALS, {
    fetchPolicy: "no-cache",
    skip: !pathname.includes("packages"),
    variables: { groupName },
  });

  return {
    dockerImagesTotal: data?.group.dockerImages.length,
    packagesTotal: data?.group.toePackages.total,
    rootsTotal: data?.group.gitRoots.total,
  };
};

export type { IUsePackagesQuery, TPackage, TPackageFilters };
export { useFilters, usePackagesQuery, useTotalsQuery };
