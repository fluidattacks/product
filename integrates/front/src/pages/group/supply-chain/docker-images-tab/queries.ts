import { graphql } from "gql/gql";

const GET_DOCKER_IMAGES = graphql(`
  query GetDockerImages($groupName: String!) {
    group(groupName: $groupName) {
      name
      dockerImages(include: true) {
        uri
        root {
          id
          nickname
        }
        toePackagesConnection(first: 0) {
          total
        }
      }
    }
  }
`);

export { GET_DOCKER_IMAGES };
