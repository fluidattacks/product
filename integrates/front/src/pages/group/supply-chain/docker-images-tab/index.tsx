import type { ColumnDef } from "@tanstack/react-table";
import * as React from "react";

import { formatPackagesColumn } from "./formatters/format-details-column";
import { type TDockerImage, useDockerImagesQuery } from "./hooks";

import { Table } from "components/table";
import { useTable } from "hooks";

interface IDockerImagesTab {
  readonly groupName: string;
}

const DockerImagesTab: React.FC<IDockerImagesTab> = ({
  groupName,
}): React.JSX.Element => {
  const { dockerImages, loading } = useDockerImagesQuery(groupName);

  const tableRef = useTable("supply-chain-docker-images-table");
  const columns: ColumnDef<TDockerImage>[] = [
    { accessorKey: "uri", header: "URI" },
    { accessorKey: "root.nickname", header: "Root nickname" },
    { cell: formatPackagesColumn, header: "Packages" },
  ];

  return (
    <div>
      <Table
        columns={columns}
        data={dockerImages}
        loadingData={loading}
        tableRef={tableRef}
      />
    </div>
  );
};

export { DockerImagesTab };
