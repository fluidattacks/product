import { Link } from "@fluidattacks/design";
import type { CellContext } from "@tanstack/react-table";
import * as React from "react";

import type { TDockerImage } from "../../hooks";

interface IPackagesColumnProps {
  readonly details: TDockerImage;
}

const PackagesColumn: React.FC<IPackagesColumnProps> = ({
  details,
}): React.JSX.Element => {
  const dockerImageId = encodeURIComponent(`${details.uri}@${details.root.id}`);

  return (
    <Link href={`./${dockerImageId}`}>
      {`View packages (${details.toePackagesConnection.total})`}
    </Link>
  );
};

const formatPackagesColumn = (
  cell: CellContext<TDockerImage, unknown>,
): React.JSX.Element => {
  const details = cell.row.original;

  return <PackagesColumn details={details} />;
};

export { formatPackagesColumn };
