import { Container, Link, Text } from "@fluidattacks/design";
import type { Row } from "@tanstack/react-table";
import * as React from "react";
import { useTheme } from "styled-components";

import type { IAdvisoryOrCoordinates } from "../../types";
import { List } from "components/list";

interface IExpandedRowProps {
  readonly details: IAdvisoryOrCoordinates;
}

const ExpandedRow: React.FC<IExpandedRowProps> = ({
  details,
}): React.JSX.Element => {
  const theme = useTheme();

  return (
    <Container display={"flex"} gap={1.25} px={1.25} py={1.25}>
      <Container
        display={"flex"}
        flexDirection={"column"}
        flexGrow={1}
        gap={0.5}
      >
        <Text color={theme.palette.gray[800]} fontWeight={"bold"} size={"sm"}>
          {"Description"}
        </Text>
        <Text size={"sm"}>{details.description}</Text>
      </Container>
      <Container flexGrow={1}>
        <Text color={theme.palette.gray[800]} fontWeight={"bold"} size={"sm"}>
          {`URLs (${details.urls === undefined ? 0 : details.urls.length})`}
        </Text>
        <List
          items={
            details.urls === undefined
              ? []
              : details.urls.map((url): JSX.Element => {
                  return (
                    <Link href={url} key={url}>
                      {url}
                    </Link>
                  );
                })
          }
        />
      </Container>
    </Container>
  );
};

export const renderExpandedRow = (
  row: Row<IAdvisoryOrCoordinates>,
): JSX.Element => {
  const details = row.original;

  return <ExpandedRow details={details} />;
};
