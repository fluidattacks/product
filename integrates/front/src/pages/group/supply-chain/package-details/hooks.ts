import { useQuery } from "@apollo/client";

import { GET_PACKAGE } from "./queries";

import type { GetPackageQuery } from "gql/graphql";

type TPackage = NonNullable<GetPackageQuery["group"]["package"]>;
type TAdvisory = NonNullable<TPackage["advisories"]>[0];

interface IUsePackageQuery {
  loading: boolean;
  pkg: TPackage | undefined;
}

const usePackageQuery = (
  groupName: string,
  name: string,
  rootId: string,
  version: string,
): IUsePackageQuery => {
  const { data, loading } = useQuery(GET_PACKAGE, {
    fetchPolicy: "cache-first",
    variables: { groupName, name, rootId, version },
  });

  return { loading, pkg: data?.group.package ?? undefined };
};

export type { TAdvisory, TPackage };
export { usePackageQuery };
