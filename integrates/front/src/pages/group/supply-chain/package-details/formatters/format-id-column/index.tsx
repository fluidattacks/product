import { Container, Icon } from "@fluidattacks/design";
import type { CellContext } from "@tanstack/react-table";
import * as React from "react";
import type { DefaultTheme } from "styled-components";

import type { TPackage } from "../../hooks";
import type { IAdvisoryOrCoordinates } from "pages/group/supply-chain/types";

const formatIdColumn = (
  theme: DefaultTheme,
  cell: CellContext<IAdvisoryOrCoordinates, unknown>,
  vulnInfo?: TPackage["vulnerabilityInfo"],
): React.JSX.Element => {
  const details = cell.row.original;

  const isCveReachable = vulnInfo
    ? vulnInfo.some((vuln): boolean => vuln.cve.includes(details.id))
    : false;

  const hasMalwareAdvisory = details.id.startsWith("MAL-");

  return (
    <Container alignItems={"center"} display={"flex"} gap={0.25}>
      {isCveReachable ? (
        <Icon
          icon={"triangle-exclamation"}
          iconColor={theme.palette.error["700"]}
          iconSize={"xxs"}
          iconType={"fa-light"}
        />
      ) : null}
      {hasMalwareAdvisory ? (
        <Icon
          icon={"bug"}
          iconColor={theme.palette.error["700"]}
          iconSize={"xxs"}
          iconType={"fa-light"}
        />
      ) : null}

      {details.id}
    </Container>
  );
};

export { formatIdColumn };
