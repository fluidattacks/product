import { graphql } from "gql/gql";

const GET_DOCKER_IMAGE_PACKAGES = graphql(`
  query GetDockerImagePackages(
    $after: String
    $first: Int!
    $groupName: String!
    $outdated: Boolean
    $platform: String
    $platforms: [String]
    $rootId: ID!
    $search: String
    $uri: String
    $vulnerable: Boolean
  ) {
    root(groupName: $groupName, rootId: $rootId) {
      ... on GitRoot {
        id
        dockerImage(uri: $uri) {
          toePackagesConnection(
            after: $after
            first: $first
            outdated: $outdated
            platform: $platform
            platforms: $platforms
            search: $search
            vulnerable: $vulnerable
          ) {
            total
            edges {
              node {
                id
                name
                outdated
                platform
                version
                vulnerable
                advisories {
                  id
                  epss
                }
                healthMetadata {
                  authors
                  latestVersion
                  latestVersionCreatedAt
                }
                locations {
                  dependencyType
                  imageRef
                  layer
                  line
                  path
                  scope
                }
                root {
                  id
                  nickname
                }
                vulnerabilityInfo {
                  id
                  cve
                  severityScore
                }
              }
            }
            pageInfo {
              endCursor
              hasNextPage
            }
          }
        }
      }
    }
  }
`);

export { GET_DOCKER_IMAGE_PACKAGES };
