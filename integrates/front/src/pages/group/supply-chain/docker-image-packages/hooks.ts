import { useQuery } from "@apollo/client";
import _ from "lodash";

import { GET_DOCKER_IMAGE_PACKAGES } from "./queries";

import type { TPackageFilters } from "../hooks";
import { getCheckedFilter, getCheckedFilters } from "components/filter/utils";
import type { GetDockerImagePackagesQuery } from "gql/graphql";

type TGitRoot = Extract<
  GetDockerImagePackagesQuery["root"],
  { __typename?: "GitRoot" }
>;
type TDockerImage = NonNullable<TGitRoot["dockerImage"]>;
type TPackage = TDockerImage["toePackagesConnection"]["edges"][0]["node"];

interface IUsePackagesQuery {
  fetchNextPage: () => Promise<void>;
  hasNextPage: boolean;
  loading: boolean;
  packages: TPackage[];
  total: number;
}

const usePackagesQuery = (
  groupName: string,
  search: string | undefined,
  filters: TPackageFilters[],
  rootId: string,
  uri: string,
): IUsePackagesQuery => {
  const platforms = getCheckedFilters(filters, "platform");
  const vulnerable = getCheckedFilter(filters, "vulnerable")?.value;
  const outdated = getCheckedFilter(filters, "outdated")?.value;

  const { data, fetchMore, loading } = useQuery(GET_DOCKER_IMAGE_PACKAGES, {
    fetchPolicy: "cache-and-network",
    nextFetchPolicy: "cache-first",
    variables: {
      first: 101,
      groupName,
      outdated: { NO: false, YES: true }[outdated ?? ""],
      platforms:
        _.isEmpty(platforms) || _.isNil(platforms) ? undefined : platforms,
      rootId,
      search: search === "" ? undefined : search,
      uri,
      vulnerable: { NO: false, YES: true }[vulnerable ?? ""],
    },
  });

  const root = data?.root.__typename === "GitRoot" ? data.root : undefined;
  const edges = root?.dockerImage?.toePackagesConnection.edges ?? [];
  const pageInfo = root?.dockerImage?.toePackagesConnection.pageInfo ?? {
    endCursor: "",
    hasNextPage: false,
  };

  return {
    fetchNextPage: async (): Promise<void> => {
      await fetchMore({ variables: { after: pageInfo.endCursor } });
    },
    hasNextPage: pageInfo.hasNextPage,
    loading,
    packages: edges.map((edge): TPackage => edge.node),
    total: root?.dockerImage?.toePackagesConnection.total ?? 0,
  };
};

export type { IUsePackagesQuery };
export { usePackagesQuery };
