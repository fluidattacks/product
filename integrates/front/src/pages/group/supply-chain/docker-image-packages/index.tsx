import { Container } from "@fluidattacks/design";
import * as React from "react";
import { useParams } from "react-router-dom";

import { usePackagesQuery } from "./hooks";

import { useFilters } from "../hooks";
import { PackagesTable } from "../packages-table";
import { useSearch } from "components/search/hooks";
import { SectionHeader } from "components/section-header";

const parseId = (dockerImageId: string): { rootId: string; uri: string } => {
  const [uri, rootId] = decodeURIComponent(dockerImageId).split("@");

  return { rootId, uri };
};

const DockerImagePackages: React.FC = (): React.JSX.Element => {
  const { groupName, organizationName, dockerImageId } = useParams() as Record<
    string,
    string
  >;
  const { rootId, uri } = parseId(dockerImageId);

  const [search, setSearch] = useSearch();
  const [filters, setFilters, options] = useFilters();
  const packagesQuery = usePackagesQuery(
    groupName,
    search,
    filters,
    rootId,
    uri,
  );

  return (
    <React.Fragment>
      <SectionHeader
        goBackTo={`/orgs/${organizationName}/groups/${groupName}/surface/packages/docker-images`}
        header={`Docker image | ${uri}`}
      />
      <Container px={1.25} py={1.25} scroll={"y"}>
        <PackagesTable
          localStorageKey={`tblToePackagesDockerImagesFilters-${groupName}`}
          onFilter={setFilters}
          onSearch={setSearch}
          options={options}
          packagesQuery={packagesQuery}
        />
      </Container>
    </React.Fragment>
  );
};

export { DockerImagePackages };
