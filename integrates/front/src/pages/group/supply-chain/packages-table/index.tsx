import type { IOptionsProps } from "@fluidattacks/design";
import { AppliedFilters, Button, useFilters } from "@fluidattacks/design";
import type { ColumnDef } from "@tanstack/react-table";
import * as React from "react";
import { useTranslation } from "react-i18next";

import { renderExpandedRow } from "./expanded-row";
import { formatDependencyColumn } from "./formatters/format-dependency-column";
import { formatDetailsColumn } from "./formatters/format-details-column";
import { formatEPSSColumn } from "./formatters/format-epss-column";
import { formatRootColumn } from "./formatters/format-root-column";
import { formatStatusColumn } from "./formatters/format-status-column";

import type { IUsePackagesQuery, TPackage, TPackageFilters } from "../hooks";
import { Authorize } from "components/@core/authorize";
import { Table } from "components/table";
import { timeFromNow } from "components/table/table-formatters";
import { useTable } from "hooks";

interface IPackagesTableProps {
  readonly onFilter: (filters: TPackageFilters[]) => void;
  readonly onRequestSbom?: () => void;
  readonly onSearch: (value: string) => void;
  readonly packagesQuery: IUsePackagesQuery;
  readonly options: IOptionsProps<TPackage>[];
  readonly localStorageKey: string;
}

const PackagesTable: React.FC<IPackagesTableProps> = ({
  onFilter,
  onRequestSbom,
  onSearch,
  packagesQuery,
  options: filterOptions,
  localStorageKey,
}): React.JSX.Element => {
  const { fetchNextPage, hasNextPage, loading, total, packages } =
    packagesQuery;
  const { t } = useTranslation();
  const tableRef = useTable(
    "tblAllPackages",
    {},
    [
      "name",
      "rootNickname",
      "version",
      "vulnerable",
      "epss",
      "metadataLatestVersion",
      "metadataLatestVersionCreatedAt",
      "details",
    ],
    { left: ["name"] },
  );
  const columns: ColumnDef<TPackage>[] = [
    {
      accessorKey: "name",
      cell: formatDependencyColumn,
      header: "Dependency",
    },
    {
      accessorKey: "root.nickname",
      cell: formatRootColumn,
      header: "Root",
      id: "rootNickname",
    },
    {
      accessorKey: "version",
      enableSorting: false,
      header: t("group.toe.packages.currentVersion"),
    },
    {
      accessorKey: "vulnerable",
      cell: formatStatusColumn,
      enableSorting: false,
      header: t("group.toe.packages.status"),
    },
    {
      accessorKey: "epss",
      cell: formatEPSSColumn,
      header: "% EPSS",
    },
    {
      accessorFn: (row): string | null =>
        row.healthMetadata?.latestVersion ?? null,
      enableSorting: false,
      header: t("group.toe.packages.latestVersion"),
      id: "metadataLatestVersion",
    },
    {
      accessorFn: (row): string | null =>
        row.healthMetadata?.latestVersionCreatedAt ?? null,
      cell: (cell): string => timeFromNow(cell.getValue<string>()),
      header: t("group.toe.packages.lastPublished"),
      id: "metadataLatestVersionCreatedAt",
    },
    {
      cell: formatDetailsColumn,
      header: "Details",
      id: "details",
    },
  ];

  function handleOnChange(filters: readonly TPackageFilters[]): void {
    onFilter(filters as TPackageFilters[]);
  }

  const { Filters, options, removeFilter } = useFilters({
    localStorageKey,
    onFiltersChange: handleOnChange,
    options: filterOptions,
  });

  return (
    <React.StrictMode>
      <Table
        columns={columns}
        data={packages}
        expandedRow={renderExpandedRow}
        filters={<Filters />}
        filtersApplied={
          <AppliedFilters onClose={removeFilter} options={options} />
        }
        loadingData={loading}
        onNextPage={fetchNextPage}
        onSearch={onSearch}
        options={{
          columnToggle: true,
          enableSorting: false,
          hasNextPage,
          size: total,
        }}
        rightSideComponents={
          onRequestSbom ? (
            <Authorize
              can={"integrates_api_mutations_request_sbom_file_mutate"}
            >
              <Button
                id={t("group.toe.packages.btn.id")}
                onClick={onRequestSbom}
                tooltip={t("group.toe.packages.btn.tooltip")}
                variant={"primary"}
              >
                {t("group.toe.packages.btn.text")}
              </Button>
            </Authorize>
          ) : undefined
        }
        tableRef={tableRef}
      />
    </React.StrictMode>
  );
};

export { PackagesTable };
