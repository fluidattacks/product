import { PureAbility } from "@casl/ability";
import { screen, waitFor, within } from "@testing-library/react";
import { userEvent } from "@testing-library/user-event";
import { GraphQLError } from "graphql";
import { HttpResponse, type StrictResponse, graphql } from "msw";
import { Route, Routes } from "react-router-dom";

import { ADD_TOE_PORT, GET_ROOTS } from "./addition-modal/queries";
import { GET_TOE_PORTS, UPDATE_TOE_PORT } from "./queries";

import { GroupToePorts } from ".";
import { authzPermissionsContext } from "context/authz/config";
import { ResourceState } from "gql/graphql";
import type {
  AddToePortMutation as AddToePort,
  GetRootsInfoAtPortsQuery as GetRoots,
  GetToePortsQuery as GetToePorts,
  UpdateToePortMutation as UpdateToePort,
} from "gql/graphql";
import { render } from "mocks";
import { LINK } from "mocks/constants";
import type { IErrorMessage, IMessage } from "mocks/types";
import { msgError, msgSuccess } from "utils/notifications";

jest.mock("utils/notifications", (): Record<string, unknown> => {
  const mockedNotifications: Record<string, () => Record<string, unknown>> =
    jest.requireActual("utils/notifications");
  jest.spyOn(mockedNotifications, "msgError").mockImplementation();
  jest.spyOn(mockedNotifications, "msgSuccess").mockImplementation();

  return mockedNotifications;
});

describe("groupToePorts", (): void => {
  const graphqlMocked = graphql.link(LINK);
  const memoryRouter = {
    initialEntries: ["/unittesting/surface/ports"],
  };
  const mockedToePortsData = graphqlMocked.query(
    GET_TOE_PORTS,
    ({ variables }): StrictResponse<IErrorMessage | { data: GetToePorts }> => {
      const {
        canGetAttackedAt,
        canGetAttackedBy,
        canGetBePresentUntil,
        canGetFirstAttackAt,
        canGetSeenFirstTimeBy,
        first,
        groupName,
      } = variables;
      if (
        canGetAttackedAt &&
        canGetAttackedBy &&
        canGetBePresentUntil &&
        canGetFirstAttackAt &&
        canGetSeenFirstTimeBy &&
        first === 150 &&
        groupName === "unittesting"
      ) {
        return HttpResponse.json({
          data: {
            group: {
              __typename: "Group",
              name: "unittesting",
              toePorts: {
                __typename: "ToePortsConnection",
                edges: [
                  {
                    node: {
                      __typename: "ToePort",
                      ...{
                        address: "127.0.0.1",
                        attackedAt: "2020-01-02T00:00:00-05:00",
                        attackedBy: "hacker@test.com",
                        bePresent: true,
                        bePresentUntil: null,
                        firstAttackAt: "2020-02-19T15:41:04+00:00",
                        hasVulnerabilities: false,
                        port: 8080,
                        root: {
                          __typename: "IPRoot",
                          id: "1a32cab8-7b4c-4761-a0a5-85cb8b64ce68",
                          nickname: "test_nickname",
                        },
                        seenAt: "2000-01-01T05:00:00+00:00",
                        seenFirstTimeBy: "",
                      },
                    },
                  },
                  {
                    node: {
                      __typename: "ToePort",
                      ...{
                        address: "172.16.0.0",
                        attackedAt: "2021-02-02T00:00:00-05:00",
                        attackedBy: "hacker@test.com",
                        bePresent: true,
                        bePresentUntil: null,
                        firstAttackAt: "2021-02-02T00:00:00-05:00",
                        hasVulnerabilities: true,
                        port: 8081,
                        root: {
                          __typename: "IPRoot",
                          id: "1a32cab8-7b4c-4761-a0a5-85cb8b64ce68",
                          nickname: "test_nickname",
                        },
                        seenAt: "2020-03-14T00:00:00-05:00",
                        seenFirstTimeBy: "test@test.com",
                      },
                    },
                  },
                  {
                    node: {
                      __typename: "ToePort",
                      ...{
                        address: "172.31.255.255",
                        attackedAt: "2021-02-11T00:00:00-05:00",
                        attackedBy: "hacker@test.com",
                        bePresent: false,
                        bePresentUntil: "2021-03-11T00:00:00-05:00",
                        firstAttackAt: "2021-02-11T00:00:00-05:00",
                        hasVulnerabilities: true,
                        port: 80,
                        root: null,
                        seenAt: "2020-01-11T00:00:00-05:00",
                        seenFirstTimeBy: "test2@test.com",
                      },
                    },
                  },
                ],
                pageInfo: {
                  endCursor: "bnVsbA==",
                  hasNextPage: false,
                },
              },
            },
          },
        });
      }

      return HttpResponse.json({
        errors: [new GraphQLError("Error getting port surface")],
      });
    },
  );

  const mockRootsQuery = graphqlMocked.query(
    GET_ROOTS,
    (): StrictResponse<{ data: GetRoots }> => {
      return HttpResponse.json({
        data: {
          group: {
            __typename: "Group",
            name: "unittesting",
            roots: [
              {
                __typename: "IPRoot",
                address: "192.168.50.111",
                id: "80596eb9-8177-42bb-9f67-bea2101fe7c5",
                nickname: "testing_ip_root",
                state: ResourceState.Active,
              },
              {
                __typename: "IPRoot",
                address: "127.0.0.1",
                id: "d312f0b9-da49-4d2b-a881-bed438875e99",
                nickname: "ip_root_1",
                state: ResourceState.Active,
              },
              {
                __typename: "GitRoot",
              },
            ],
          },
        },
      });
    },
  );

  it("should display group toe ports", async (): Promise<void> => {
    expect.hasAssertions();

    const mockedPermissions = new PureAbility<string>([
      { action: "integrates_api_resolvers_toe_port_attacked_at_resolve" },
      { action: "integrates_api_resolvers_toe_port_attacked_by_resolve" },
      { action: "integrates_api_resolvers_toe_port_be_present_until_resolve" },
      { action: "integrates_api_resolvers_toe_port_first_attack_at_resolve" },
      {
        action: "integrates_api_resolvers_toe_port_seen_first_time_by_resolve",
      },
    ]);
    render(
      <authzPermissionsContext.Provider value={mockedPermissions}>
        <Routes>
          <Route
            element={<GroupToePorts isInternal={true} />}
            path={"/:groupName/surface/ports"}
          />
        </Routes>
      </authzPermissionsContext.Provider>,
      { memoryRouter, mocks: [mockedToePortsData, mockRootsQuery] },
    );

    await waitFor((): void => {
      expect(screen.queryAllByText("test_nickname")).toHaveLength(2);
    });

    expect(
      screen.getAllByRole("row")[0].textContent?.replace(/[^a-zA-Z ]/gu, ""),
    ).toStrictEqual(
      [
        "group.toe.ports.root",
        "group.toe.ports.port",
        "group.toe.ports.status",
        "group.toe.ports.seenAt",
        "group.toe.ports.attackedAt",
        "group.toe.ports.seenFirstTimeBy",
      ]
        .join("")
        .replace(/[^a-zA-Z ]/gu, ""),
    );
    expect(
      screen.getAllByRole("row")[1].textContent?.replace(/[^a-zA-Z ]/gu, ""),
    ).toStrictEqual(
      ["test_nickname", "8080", "Safe", "2000-01-01", "2020-01-02", ""]
        .join("")
        .replace(/[^a-zA-Z ]/gu, ""),
    );
    expect(
      screen.getAllByRole("row")[2].textContent?.replace(/[^a-zA-Z ]/gu, ""),
    ).toStrictEqual(
      [
        "test_nickname",
        "8081",
        "Vulnerable",
        "2020-03-14",
        "2021-02-02",
        "test@test.com",
      ]
        .join("")
        .replace(/[^a-zA-Z ]/gu, ""),
    );

    expect(
      screen.getAllByRole("row")[3].textContent?.replace(/[^a-zA-Z ]/gu, ""),
    ).toStrictEqual(
      ["", "80", "Vulnerable", "2020-01-11", "2021-02-11", "test2@test.com"]
        .join("")
        .replace(/[^a-zA-Z ]/gu, ""),
    );

    jest.clearAllMocks();
  });

  it("should edit be present on cell", async (): Promise<void> => {
    expect.hasAssertions();

    const mocksMutation = [
      graphqlMocked.mutation(
        UPDATE_TOE_PORT,
        ({
          variables,
        }): StrictResponse<IErrorMessage | { data: UpdateToePort }> => {
          const {
            address,
            bePresent,
            canGetAttackedAt,
            canGetAttackedBy,
            canGetBePresentUntil,
            canGetFirstAttackAt,
            canGetSeenFirstTimeBy,
            groupName,
            hasRecentAttack,
            port,
            rootId,
            shouldGetNewToePort,
          } = variables;
          if (
            address === "127.0.0.1" &&
            !bePresent &&
            canGetAttackedAt &&
            canGetAttackedBy &&
            canGetBePresentUntil &&
            canGetFirstAttackAt &&
            canGetSeenFirstTimeBy &&
            groupName === "unittesting" &&
            hasRecentAttack === undefined &&
            port === 8080 &&
            rootId === "1a32cab8-7b4c-4761-a0a5-85cb8b64ce68" &&
            shouldGetNewToePort
          ) {
            return HttpResponse.json({
              data: {
                updateToePort: {
                  success: true,
                  toePort: {
                    __typename: "ToePort",
                    ...{
                      address: "127.0.0.1",
                      attackedAt: "2020-01-02T00:00:00-05:00",
                      attackedBy: "hacker@test.com",
                      bePresent: false,
                      bePresentUntil: "2022-01-02T00:00:00-05:00",
                      firstAttackAt: "2020-02-19T15:41:04+00:00",
                      hasVulnerabilities: false,
                      port: 8080,
                      root: {
                        __typename: "IPRoot",
                        id: "1a32cab8-7b4c-4761-a0a5-85cb8b64ce68",
                        nickname: "test_nickname",
                      },
                      seenAt: "2000-01-01T05:00:00+00:00",
                      seenFirstTimeBy: "",
                    },
                  },
                },
              },
            });
          }

          return HttpResponse.json({
            errors: [new GraphQLError("Error updating port surface")],
          });
        },
      ),
    ];
    const mockedToePorts = graphqlMocked.query(
      GET_TOE_PORTS,
      ({
        variables,
      }): StrictResponse<IErrorMessage | { data: GetToePorts }> => {
        const {
          canGetAttackedAt,
          canGetAttackedBy,
          canGetBePresentUntil,
          canGetFirstAttackAt,
          canGetSeenFirstTimeBy,
          first,
          groupName,
        } = variables;
        if (
          canGetAttackedAt &&
          canGetAttackedBy &&
          canGetBePresentUntil &&
          canGetFirstAttackAt &&
          canGetSeenFirstTimeBy &&
          first === 150 &&
          groupName === "unittesting"
        ) {
          return HttpResponse.json({
            data: {
              group: {
                __typename: "Group",
                name: "unittesting",
                toePorts: {
                  __typename: "ToePortsConnection",
                  edges: [
                    {
                      node: {
                        __typename: "ToePort",
                        ...{
                          address: "127.0.0.1",
                          attackedAt: "2020-01-02T00:00:00-05:00",
                          attackedBy: "hacker@test.com",
                          bePresent: true,
                          bePresentUntil: null,
                          firstAttackAt: "2020-02-19T15:41:04+00:00",
                          hasVulnerabilities: false,
                          port: 8080,
                          root: {
                            __typename: "IPRoot",
                            id: "1a32cab8-7b4c-4761-a0a5-85cb8b64ce68",
                            nickname: "test_nickname",
                          },
                          seenAt: "2000-01-01T05:00:00+00:00",
                          seenFirstTimeBy: "",
                        },
                      },
                    },
                  ],
                  pageInfo: {
                    endCursor: "bnVsbA==",
                    hasNextPage: false,
                  },
                },
              },
            },
          });
        }

        return HttpResponse.json({
          errors: [new GraphQLError("Error getting port surface")],
        });
      },
    );
    const mockedPermissions = new PureAbility<string>([
      { action: "integrates_api_resolvers_toe_port_attacked_at_resolve" },
      { action: "integrates_api_resolvers_toe_port_attacked_by_resolve" },
      { action: "integrates_api_resolvers_toe_port_be_present_until_resolve" },
      { action: "integrates_api_resolvers_toe_port_first_attack_at_resolve" },
      {
        action: "integrates_api_resolvers_toe_port_seen_first_time_by_resolve",
      },
      { action: "integrates_api_mutations_update_toe_port_mutate" },
    ]);
    render(
      <authzPermissionsContext.Provider value={mockedPermissions}>
        <Routes>
          <Route
            element={<GroupToePorts isInternal={true} />}
            path={"/:groupName/surface/ports"}
          />
        </Routes>
      </authzPermissionsContext.Provider>,
      {
        memoryRouter,
        mocks: [mockedToePorts, mockRootsQuery, ...mocksMutation],
      },
    );

    expect(
      screen.getByRole("button", {
        name: "group.toe.ports.actionButtons.attackedButton.text",
      }),
    ).toBeInTheDocument();

    await waitFor((): void => {
      expect(screen.queryByRole("table")).toBeInTheDocument();
    });

    await userEvent.click(
      screen.getByRole("button", {
        name: /group\.findings\.tableSet\.btn\.text .*/u,
      }),
    );

    await userEvent.click(
      screen.getByRole("checkbox", {
        checked: false,
        name: "group.toe.ports.bePresent",
      }),
    );
    await userEvent.click(
      screen.getByRole("checkbox", {
        checked: false,
        name: "group.toe.ports.bePresentUntil",
      }),
    );

    expect(
      screen.getAllByRole("row")[0].textContent?.replace(/[^a-zA-Z ]/gu, ""),
    ).toStrictEqual(
      [
        "group.toe.ports.root",
        "group.toe.ports.port",
        "group.toe.ports.status",
        "group.toe.ports.seenAt",
        "group.toe.ports.bePresent",
        "group.toe.ports.attackedAt",
        "group.toe.ports.seenFirstTimeBy",
        "group.toe.ports.bePresentUntil",
      ]
        .join("")
        .replace(/[^a-zA-Z ]/gu, ""),
    );

    const row = screen.getByRole("row", {
      name: "test_nickname 8080 Safe 2000-01-01 yes bePresentSwitch Toggle Switch 2020-01-02 -",
    });

    await userEvent.click(
      within(row).getByRole("checkbox", { name: "bePresentSwitch" }),
    );

    await waitFor((): void => {
      expect(msgSuccess).toHaveBeenCalledWith(
        "group.toe.ports.alerts.updatePort",
        "groupAlerts.updatedTitle",
      );
    });
  });

  it("should mark port as attacked", async (): Promise<void> => {
    expect.hasAssertions();

    const mocksMutation = [
      graphqlMocked.mutation(
        UPDATE_TOE_PORT,
        ({
          variables,
        }): StrictResponse<IErrorMessage | { data: UpdateToePort }> => {
          const {
            address,
            bePresent,
            canGetAttackedAt,
            canGetAttackedBy,
            canGetBePresentUntil,
            canGetFirstAttackAt,
            canGetSeenFirstTimeBy,
            groupName,
            hasRecentAttack,
            port,
            rootId,
            shouldGetNewToePort,
          } = variables;
          if (
            address === "127.0.0.1" &&
            bePresent &&
            canGetAttackedAt &&
            canGetAttackedBy &&
            canGetBePresentUntil &&
            canGetFirstAttackAt &&
            canGetSeenFirstTimeBy &&
            groupName === "unittesting" &&
            hasRecentAttack === true &&
            port === 8080 &&
            rootId === "1a32cab8-7b4c-4761-a0a5-85cb8b64ce68" &&
            !shouldGetNewToePort
          ) {
            return HttpResponse.json({
              data: {
                updateToePort: {
                  success: true,
                  toePort: {
                    __typename: "ToePort",
                    ...{
                      address: "127.0.0.1",
                      attackedAt: "2020-01-02T00:00:00-05:00",
                      attackedBy: "hacker@test.com",
                      bePresent: false,
                      bePresentUntil: "2022-01-02T00:00:00-05:00",
                      firstAttackAt: "2020-02-19T15:41:04+00:00",
                      hasVulnerabilities: false,
                      port: 8080,
                      root: {
                        __typename: "IPRoot",
                        id: "1a32cab8-7b4c-4761-a0a5-85cb8b64ce68",
                        nickname: "test_nickname",
                      },
                      seenAt: "2000-01-01T05:00:00+00:00",
                      seenFirstTimeBy: "",
                    },
                  },
                },
              },
            });
          }

          return HttpResponse.json({
            errors: [new GraphQLError("Error updating port surface")],
          });
        },
      ),
    ];
    const mockedToePorts = graphqlMocked.query(
      GET_TOE_PORTS,
      ({
        variables,
      }): StrictResponse<IErrorMessage | { data: GetToePorts }> => {
        const {
          canGetAttackedAt,
          canGetAttackedBy,
          canGetBePresentUntil,
          canGetFirstAttackAt,
          canGetSeenFirstTimeBy,
          first,
          groupName,
        } = variables;
        if (
          canGetAttackedAt &&
          canGetAttackedBy &&
          canGetBePresentUntil &&
          canGetFirstAttackAt &&
          canGetSeenFirstTimeBy &&
          first === 150 &&
          groupName === "unittesting"
        ) {
          return HttpResponse.json({
            data: {
              group: {
                __typename: "Group",
                name: "unittesting",
                toePorts: {
                  __typename: "ToePortsConnection",
                  edges: [
                    {
                      node: {
                        __typename: "ToePort",
                        ...{
                          address: "127.0.0.1",
                          attackedAt: "2020-01-02T00:00:00-05:00",
                          attackedBy: "hacker@test.com",
                          bePresent: true,
                          bePresentUntil: null,
                          firstAttackAt: "2020-02-19T15:41:04+00:00",
                          hasVulnerabilities: false,
                          port: 8080,
                          root: {
                            __typename: "IPRoot",
                            id: "1a32cab8-7b4c-4761-a0a5-85cb8b64ce68",
                            nickname: "test_nickname",
                          },
                          seenAt: "2000-01-01T05:00:00+00:00",
                          seenFirstTimeBy: "",
                        },
                      },
                    },
                  ],
                  pageInfo: {
                    endCursor: "bnVsbA==",
                    hasNextPage: false,
                  },
                },
              },
            },
          });
        }

        return HttpResponse.json({
          errors: [new GraphQLError("Error getting port surface")],
        });
      },
    );
    const mockedPermissions = new PureAbility<string>([
      { action: "integrates_api_resolvers_toe_port_attacked_at_resolve" },
      { action: "integrates_api_resolvers_toe_port_attacked_by_resolve" },
      { action: "integrates_api_resolvers_toe_port_be_present_until_resolve" },
      { action: "integrates_api_resolvers_toe_port_first_attack_at_resolve" },
      {
        action: "integrates_api_resolvers_toe_port_seen_first_time_by_resolve",
      },
      { action: "integrates_api_mutations_update_toe_port_mutate" },
    ]);
    render(
      <authzPermissionsContext.Provider value={mockedPermissions}>
        <Routes>
          <Route
            element={<GroupToePorts isInternal={true} />}
            path={"/:groupName/surface/ports"}
          />
        </Routes>
      </authzPermissionsContext.Provider>,
      {
        memoryRouter,
        mocks: [mockedToePorts, mockRootsQuery, ...mocksMutation],
      },
    );
    await waitFor((): void => {
      expect(screen.queryByRole("table")).toBeInTheDocument();
    });

    await userEvent.click(screen.getAllByRole("checkbox", { name: "" })[0]);
    await userEvent.click(
      screen.getByRole("button", {
        name: "group.toe.ports.actionButtons.attackedButton.text",
      }),
    );

    await waitFor((): void => {
      expect(msgSuccess).toHaveBeenCalledWith(
        "group.toe.ports.alerts.markAsAttacked.success",
        "groupAlerts.updatedTitle",
      );
    });

    jest.clearAllMocks();
  });

  it("should render a error updating toe port", async (): Promise<void> => {
    expect.hasAssertions();

    jest.clearAllMocks();

    const mockError = graphqlMocked.mutation(
      UPDATE_TOE_PORT,
      ({
        variables,
      }): StrictResponse<
        Record<"errors", IMessage[]> | { data: UpdateToePort }
      > => {
        const { groupName } = variables;
        if (groupName === "unittesting") {
          return HttpResponse.json({
            errors: [
              new GraphQLError(
                "Exception - The toe port has been updated by another operation",
              ),
              new GraphQLError("Exception - The toe port is not present"),
              new GraphQLError("An error occurred updating the toe port"),
            ],
          });
        }

        return HttpResponse.json({
          data: {
            updateToePort: { success: true },
          },
        });
      },
    );

    const mockedPermissions = new PureAbility<string>([
      { action: "integrates_api_resolvers_toe_port_attacked_at_resolve" },
      { action: "integrates_api_resolvers_toe_port_attacked_by_resolve" },
      { action: "integrates_api_resolvers_toe_port_be_present_until_resolve" },
      { action: "integrates_api_resolvers_toe_port_first_attack_at_resolve" },
      {
        action: "integrates_api_resolvers_toe_port_seen_first_time_by_resolve",
      },
      { action: "integrates_api_mutations_update_toe_port_mutate" },
    ]);

    render(
      <authzPermissionsContext.Provider value={mockedPermissions}>
        <Routes>
          <Route
            element={<GroupToePorts isInternal={true} />}
            path={"/:groupName/surface/ports"}
          />
        </Routes>
      </authzPermissionsContext.Provider>,
      {
        memoryRouter,
        mocks: [mockedToePortsData, mockRootsQuery, mockError],
      },
    );

    await waitFor((): void => {
      expect(screen.queryByRole("table")).toBeInTheDocument();
    });

    await userEvent.click(screen.getAllByRole("checkbox", { name: "" })[0]);
    jest.clearAllMocks();
    await userEvent.click(
      screen.getByRole("button", {
        name: "group.toe.ports.actionButtons.attackedButton.text",
      }),
    );

    await waitFor((): void => {
      expect(msgError).toHaveBeenCalledWith(
        "Something modified the port during the edition.",
      );
    });

    expect(msgError).toHaveBeenCalledWith("The port is not present.");
    expect(msgError).toHaveBeenCalledWith("There is an error :(");

    expect(msgError).toHaveBeenCalledTimes(6);

    jest.clearAllMocks();
  });

  it("should display add group toe ports", async (): Promise<void> => {
    expect.hasAssertions();

    jest.spyOn(console, "warn").mockImplementation();
    const mockAddMutation = graphqlMocked.mutation(
      ADD_TOE_PORT,
      ({ variables }): StrictResponse<IErrorMessage | { data: AddToePort }> => {
        const { address, groupName, port } = variables;
        if (
          address === "192.168.50.111" &&
          groupName === "unittesting" &&
          port === 8080
        ) {
          return HttpResponse.json({
            data: {
              addToePort: { success: true },
            },
          });
        }

        return HttpResponse.json({
          errors: [new GraphQLError("Error adding port surface")],
        });
      },
    );

    const mockedPermissions = new PureAbility<string>([
      { action: "integrates_api_resolvers_toe_port_attacked_at_resolve" },
      { action: "integrates_api_resolvers_toe_port_attacked_by_resolve" },
      { action: "integrates_api_resolvers_toe_port_be_present_until_resolve" },
      { action: "integrates_api_resolvers_toe_port_first_attack_at_resolve" },
      {
        action: "integrates_api_resolvers_toe_port_seen_first_time_by_resolve",
      },
      { action: "integrates_api_mutations_add_toe_port_mutate" },
    ]);
    render(
      <authzPermissionsContext.Provider value={mockedPermissions}>
        <Routes>
          <Route
            element={<GroupToePorts isInternal={true} />}
            path={"/:groupName/surface/ports"}
          />
        </Routes>
      </authzPermissionsContext.Provider>,
      {
        memoryRouter,
        mocks: [mockedToePortsData, mockAddMutation, mockRootsQuery],
      },
    );

    expect(
      screen.getByRole("button", {
        name: "group.toe.ports.actionButtons.addButton.text",
      }),
    ).toBeInTheDocument();

    await userEvent.click(
      screen.getByRole("button", {
        name: "group.toe.ports.actionButtons.addButton.text",
      }),
    );

    expect(
      screen.getByText("group.toe.ports.addModal.title"),
    ).toBeInTheDocument();

    expect(
      within(screen.getByRole("combobox", { name: "rootId" })).getByText(
        "testing_ip_root - 192.168.50.111",
      ),
    ).toBeInTheDocument();

    await userEvent.type(
      screen.getByRole("spinbutton", { name: "port" }),
      "8080",
    );

    await userEvent.click(
      screen.getByRole("button", { name: "buttons.confirm" }),
    );

    expect(msgSuccess).toHaveBeenCalledWith(
      "group.toe.ports.addModal.alerts.success",
      "groupAlerts.titleSuccess",
    );
  });

  it("should handle error when add group toe ports", async (): Promise<void> => {
    expect.hasAssertions();

    const mockErrorMutation = graphqlMocked.mutation(
      ADD_TOE_PORT,
      ({
        variables,
      }): StrictResponse<
        Record<"errors", IMessage[]> | { data: AddToePort }
      > => {
        const { address, groupName, port } = variables;
        if (
          address === "192.168.50.111" &&
          groupName === "unittesting" &&
          port === 8080
        ) {
          return HttpResponse.json({
            errors: [
              new GraphQLError("Exception - Toe port already exists"),
              new GraphQLError("groupAlerts.errorTextsad"),
            ],
          });
        }

        return HttpResponse.json({
          data: {
            addToePort: { success: true },
          },
        });
      },
    );

    const mockedPermissions = new PureAbility<string>([
      { action: "integrates_api_resolvers_toe_port_attacked_at_resolve" },
      { action: "integrates_api_resolvers_toe_port_attacked_by_resolve" },
      { action: "integrates_api_resolvers_toe_port_be_present_until_resolve" },
      { action: "integrates_api_resolvers_toe_port_first_attack_at_resolve" },
      {
        action: "integrates_api_resolvers_toe_port_seen_first_time_by_resolve",
      },
      { action: "integrates_api_mutations_add_toe_port_mutate" },
    ]);
    render(
      <authzPermissionsContext.Provider value={mockedPermissions}>
        <Routes>
          <Route
            element={<GroupToePorts isInternal={true} />}
            path={"/:groupName/surface/ports"}
          />
        </Routes>
      </authzPermissionsContext.Provider>,
      {
        memoryRouter,
        mocks: [mockedToePortsData, mockErrorMutation, mockRootsQuery],
      },
    );

    expect(
      screen.getByRole("button", {
        name: "group.toe.ports.actionButtons.addButton.text",
      }),
    ).toBeInTheDocument();

    await userEvent.click(
      screen.getByRole("button", {
        name: "group.toe.ports.actionButtons.addButton.text",
      }),
    );

    expect(
      screen.getByText("group.toe.ports.addModal.title"),
    ).toBeInTheDocument();

    await userEvent.type(
      screen.getByRole("spinbutton", { name: "port" }),
      "808000",
    );

    await userEvent.click(
      screen.getByRole("button", { name: "buttons.confirm" }),
    );

    expect(screen.getByText("validations.portRange")).toBeInTheDocument();

    await userEvent.clear(screen.getByRole("spinbutton"));
    await userEvent.type(
      screen.getByRole("spinbutton", { name: "port" }),
      "8080",
    );
    await userEvent.click(
      screen.getByRole("button", { name: "buttons.confirm" }),
    );

    await waitFor((): void => {
      expect(msgError).toHaveBeenCalledWith(
        "group.toe.ports.addModal.alerts.alreadyExists",
      );
    });

    expect(msgError).toHaveBeenCalledTimes(2);

    jest.clearAllMocks();
  });

  it("shouldn't display action buttons", async (): Promise<void> => {
    expect.hasAssertions();

    const mockedPermissions = new PureAbility<string>([
      { action: "integrates_api_resolvers_toe_port_attacked_at_resolve" },
      { action: "integrates_api_resolvers_toe_port_attacked_by_resolve" },
      { action: "integrates_api_resolvers_toe_port_be_present_until_resolve" },
      { action: "integrates_api_resolvers_toe_port_first_attack_at_resolve" },
      {
        action: "integrates_api_resolvers_toe_port_seen_first_time_by_resolve",
      },
    ]);
    render(
      <authzPermissionsContext.Provider value={mockedPermissions}>
        <Routes>
          <Route
            element={<GroupToePorts isInternal={false} />}
            path={"/:groupName/surface/ports"}
          />
        </Routes>
      </authzPermissionsContext.Provider>,
      { memoryRouter, mocks: [mockedToePortsData, mockRootsQuery] },
    );

    expect(
      screen.queryByRole("button", {
        name: "group.toe.ports.actionButtons.addButton.text",
      }),
    ).not.toBeInTheDocument();
    expect(
      screen.queryByRole("button", {
        name: "group.toe.ports.actionButtons.attackedButton.text",
      }),
    ).not.toBeInTheDocument();

    await waitFor((): void => {
      expect(screen.queryAllByText("test_nickname")[0]).toBeInTheDocument();
    });

    expect(
      screen.getAllByRole("row")[0].textContent?.replace(/[^a-zA-Z ]/gu, ""),
    ).toStrictEqual(
      [
        "group.toe.ports.root",
        "group.toe.ports.port",
        "group.toe.ports.status",
        "group.toe.ports.seenAt",
      ]
        .join("")
        .replace(/[^a-zA-Z ]/gu, ""),
    );

    jest.clearAllMocks();
  });
});
