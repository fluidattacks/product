import type { ApolloError } from "@apollo/client";
import isNil from "lodash/isNil";

import type { IIPRootAttr, IToePortAttr, IToePortData } from "./types";

import { markSeenFirstTimeBy } from "../utils";
import { Logger } from "utils/logger";
import { msgError } from "utils/notifications";
import { translate } from "utils/translations/translate";

const isEqualRootId = (root: IIPRootAttr | null, rootId: string): boolean => {
  if (isNil(root)) {
    return rootId === "";
  }

  return root.id === rootId;
};

const formatToePortData = (toePortAttr: IToePortAttr): IToePortData => ({
  ...toePortAttr,
  attackedAt: toePortAttr.attackedAt ?? "-",
  bePresentUntil: toePortAttr.bePresentUntil ?? "-",
  firstAttackAt: toePortAttr.firstAttackAt ?? "-",
  markedSeenFirstTimeBy: markSeenFirstTimeBy(toePortAttr.seenFirstTimeBy),
  rootId: isNil(toePortAttr.root) ? "" : toePortAttr.root.id,
  rootNickname: isNil(toePortAttr.root) ? "" : toePortAttr.root.nickname,
  seenAt: toePortAttr.seenAt ?? "-",
});

const handleUpdateToePortError = (errors: ApolloError): void => {
  errors.graphQLErrors.forEach((error): void => {
    switch (error.message) {
      case "Exception - The toe port is not present":
        msgError(translate.t("group.toe.ports.alerts.nonPresent"));
        break;
      case "Exception - The toe port has been updated by another operation":
        msgError(translate.t("group.toe.ports.alerts.alreadyUpdate"));
        break;
      default:
        msgError(translate.t("groupAlerts.errorTextsad"));
        Logger.warning("An error occurred updating the toe port", error);
    }
  });
};

export { isEqualRootId, formatToePortData, handleUpdateToePortError };
