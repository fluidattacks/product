import { Button } from "@fluidattacks/design";
import * as React from "react";
import { useTranslation } from "react-i18next";

import type { IAddButtonProps } from "./types";

import { Authorize } from "components/@core/authorize";

const AddButton: React.FC<IAddButtonProps> = ({
  isDisabled,
  onAdd,
}: IAddButtonProps): JSX.Element => {
  const { t } = useTranslation();

  return (
    <React.StrictMode>
      <Authorize can={"integrates_api_mutations_add_toe_input_mutate"}>
        <Button
          disabled={isDisabled}
          icon={"add"}
          id={"addToeInput"}
          onClick={onAdd}
          tooltip={t("group.toe.inputs.actionButtons.addButton.tooltip")}
          variant={"primary"}
        >
          {t("group.toe.inputs.actionButtons.addButton.text")}
        </Button>
      </Authorize>
    </React.StrictMode>
  );
};

export { AddButton };
