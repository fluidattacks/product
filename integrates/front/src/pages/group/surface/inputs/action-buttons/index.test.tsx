import { PureAbility } from "@casl/ability";
import { screen } from "@testing-library/react";

import { ActionButtons } from ".";
import { authzPermissionsContext } from "context/authz/config";
import { render } from "mocks";

describe("toeInputsActionButtons", (): void => {
  it("should not display the edition button without permissions", (): void => {
    expect.hasAssertions();

    render(
      <authzPermissionsContext.Provider value={new PureAbility([])}>
        <ActionButtons
          areInputsSelected={true}
          data={[]}
          isAdding={false}
          isInternal={true}
          isMarkingAsAttacked={false}
          onAdd={jest.fn()}
          onMarkAsAttacked={jest.fn()}
        />
      </authzPermissionsContext.Provider>,
    );

    expect(screen.queryByRole("button")).not.toBeInTheDocument();
  });

  it("should hide the addition button for the external view", (): void => {
    expect.hasAssertions();

    const mockedPermissions = new PureAbility<string>([
      { action: "integrates_api_mutations_add_toe_input_mutate" },
    ]);
    render(
      <authzPermissionsContext.Provider value={mockedPermissions}>
        <ActionButtons
          areInputsSelected={true}
          data={[]}
          isAdding={false}
          isInternal={false}
          isMarkingAsAttacked={false}
          onAdd={jest.fn()}
          onMarkAsAttacked={jest.fn()}
        />
      </authzPermissionsContext.Provider>,
    );

    expect(screen.queryByRole("button")).not.toBeInTheDocument();
  });

  it("should display the addition button", (): void => {
    expect.hasAssertions();

    const mockedPermissions = new PureAbility<string>([
      { action: "integrates_api_mutations_add_toe_input_mutate" },
    ]);
    render(
      <authzPermissionsContext.Provider value={mockedPermissions}>
        <ActionButtons
          areInputsSelected={true}
          data={[]}
          isAdding={false}
          isInternal={true}
          isMarkingAsAttacked={false}
          onAdd={jest.fn()}
          onMarkAsAttacked={jest.fn()}
        />
      </authzPermissionsContext.Provider>,
    );

    expect(screen.queryByRole("button")).toBeInTheDocument();
    expect(
      screen.queryByText("group.toe.inputs.actionButtons.addButton.text"),
    ).toBeInTheDocument();
  });

  it("should display the attacked button", (): void => {
    expect.hasAssertions();

    const mockedPermissions = new PureAbility<string>([
      { action: "integrates_api_mutations_update_toe_input_mutate" },
    ]);
    render(
      <authzPermissionsContext.Provider value={mockedPermissions}>
        <ActionButtons
          areInputsSelected={true}
          data={[]}
          isAdding={false}
          isInternal={true}
          isMarkingAsAttacked={false}
          onAdd={jest.fn()}
          onMarkAsAttacked={jest.fn()}
        />
      </authzPermissionsContext.Provider>,
    );

    expect(
      screen.getByRole("button", {
        name: "group.toe.inputs.actionButtons.attackedButton.text",
      }),
    ).toBeInTheDocument();
  });
});
