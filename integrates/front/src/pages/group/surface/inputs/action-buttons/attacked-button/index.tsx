import { Button } from "@fluidattacks/design";
import { StrictMode } from "react";
import { useTranslation } from "react-i18next";

import { Authorize } from "components/@core/authorize";

interface IAttackedButtonProps {
  readonly isDisabled: boolean;
  readonly onAttacked: () => void;
}

const AttackedButton = ({
  isDisabled,
  onAttacked,
}: IAttackedButtonProps): JSX.Element => {
  const { t } = useTranslation();

  return (
    <StrictMode>
      <Authorize can={"integrates_api_mutations_update_toe_input_mutate"}>
        <Button
          disabled={isDisabled}
          icon={"check"}
          id={"attackedToeInputs"}
          onClick={onAttacked}
          tooltip={t("group.toe.inputs.actionButtons.attackedButton.tooltip")}
          variant={"secondary"}
        >
          {t("group.toe.inputs.actionButtons.attackedButton.text")}
        </Button>
      </Authorize>
    </StrictMode>
  );
};

export type { IAttackedButtonProps };
export { AttackedButton };
