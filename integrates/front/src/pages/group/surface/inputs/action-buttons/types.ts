import type { IToeInputData } from "../types";

interface IActionButtonsProps {
  data: IToeInputData[];
  fetchReportData?: () => Promise<void>;
  areInputsSelected: boolean;
  isAdding: boolean;
  isMarkingAsAttacked: boolean;
  isInternal: boolean;
  onAdd: () => void;
  onMarkAsAttacked: () => void;
  size?: number;
}

export type { IActionButtonsProps };
