import { graphql } from "gql";

const TOE_INPUTS_FRAGMENT = graphql(`
  fragment toeInputFields on ToeInput {
    __typename
    attackedAt @include(if: $canGetAttackedAt)
    attackedBy @include(if: $canGetAttackedBy)
    bePresent
    bePresentUntil @include(if: $canGetBePresentUntil)
    component
    entryPoint
    firstAttackAt @include(if: $canGetFirstAttackAt)
    hasVulnerabilities
    seenAt
    seenFirstTimeBy @include(if: $canGetSeenFirstTimeBy)
    root {
      ... on GitRoot {
        id
        __typename
        nickname
      }
      ... on IPRoot {
        id
        __typename
        nickname
      }
      ... on URLRoot {
        id
        __typename
        nickname
      }
    }
  }
`);

const GET_TOE_INPUTS_COMPONENTS = graphql(`
  query GetToeInputComponents($groupName: String!) {
    group(groupName: $groupName) {
      __typename
      name
      toeInputs {
        edges {
          node {
            component
          }
        }
      }
    }
  }
`);

const GET_TOE_INPUTS = graphql(`
  query GetToeInputs(
    $after: String
    $attackedBy: String
    $bePresent: Boolean
    $canGetAttackedAt: Boolean!
    $canGetAttackedBy: Boolean!
    $canGetBePresentUntil: Boolean!
    $canGetFirstAttackAt: Boolean!
    $canGetSeenFirstTimeBy: Boolean!
    $component: String
    $entryPoint: String
    $first: Int
    $fromAttackedAt: DateTime
    $fromBePresentUntil: DateTime
    $fromFirstAttackAt: DateTime
    $fromSeenAt: DateTime
    $groupName: String!
    $hasVulnerabilities: Boolean
    $rootId: ID
    $rootNickname: String
    $search: String
    $seenFirstTimeBy: String
    $toAttackedAt: DateTime
    $toBePresentUntil: DateTime
    $toFirstAttackAt: DateTime
    $toSeenAt: DateTime
  ) {
    group(groupName: $groupName) {
      __typename
      name
      toeInputs(
        after: $after
        attackedBy: $attackedBy
        bePresent: $bePresent
        component: $component
        entryPoint: $entryPoint
        first: $first
        fromAttackedAt: $fromAttackedAt
        fromBePresentUntil: $fromBePresentUntil
        fromFirstAttackAt: $fromFirstAttackAt
        fromSeenAt: $fromSeenAt
        hasVulnerabilities: $hasVulnerabilities
        rootId: $rootId
        rootNickname: $rootNickname
        search: $search
        seenFirstTimeBy: $seenFirstTimeBy
        toAttackedAt: $toAttackedAt
        toBePresentUntil: $toBePresentUntil
        toFirstAttackAt: $toFirstAttackAt
        toSeenAt: $toSeenAt
      ) {
        __typename
        total
        edges {
          node {
            ...toeInputFields
          }
        }
        pageInfo {
          endCursor
          hasNextPage
        }
      }
    }
  }
`);

const UPDATE_TOE_INPUT = graphql(`
  mutation UpdateToeInput(
    $bePresent: Boolean!
    $canGetAttackedAt: Boolean!
    $canGetAttackedBy: Boolean!
    $canGetBePresentUntil: Boolean!
    $canGetFirstAttackAt: Boolean!
    $canGetSeenFirstTimeBy: Boolean!
    $component: String!
    $entryPoint: String!
    $groupName: String!
    $hasRecentAttack: Boolean
    $rootId: String!
    $shouldGetNewToeInput: Boolean!
  ) {
    updateToeInput(
      bePresent: $bePresent
      component: $component
      entryPoint: $entryPoint
      groupName: $groupName
      hasRecentAttack: $hasRecentAttack
      rootId: $rootId
    ) {
      success
      toeInput @include(if: $shouldGetNewToeInput) {
        ...toeInputFields
      }
    }
  }
`);

export {
  GET_TOE_INPUTS,
  GET_TOE_INPUTS_COMPONENTS,
  TOE_INPUTS_FRAGMENT,
  UPDATE_TOE_INPUT,
};
