interface IGroupToeInputsProps {
  isInternal: boolean;
}

interface IToeInputEdge {
  node: IToeInputAttr;
}

interface IToeInputAttr {
  __typename: "ToeInput";
  attackedAt: string | null;
  attackedBy: string;
  bePresent: boolean;
  bePresentUntil: string | null;
  component: string;
  entryPoint: string;
  firstAttackAt: string | null;
  hasVulnerabilities: boolean;
  root: IGitRootAttr | null;
  seenAt: string | null;
  seenFirstTimeBy: string;
}

interface IGitRootAttr {
  __typename: "GitRoot" | "IPRoot" | "URLRoot";
  id: string;
  nickname: string;
}

interface IToeInputData {
  attackedAt: string;
  attackedBy: string;
  bePresent: boolean;
  bePresentUntil: string;
  component: string;
  entryPoint: string;
  firstAttackAt: string;
  hasVulnerabilities: boolean;
  markedSeenFirstTimeBy: string;
  root: IGitRootAttr | null;
  rootId: string;
  rootNickname: string;
  seenAt: string;
  seenFirstTimeBy: string;
}

export type {
  IGitRootAttr,
  IGroupToeInputsProps,
  IToeInputAttr,
  IToeInputEdge,
  IToeInputData,
};
