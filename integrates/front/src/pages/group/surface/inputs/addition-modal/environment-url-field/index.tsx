import type { FormikValues } from "formik";
import _ from "lodash";
import { StrictMode, useEffect } from "react";
import { useTranslation } from "react-i18next";

import type { TEnvironmentUrlFieldProps } from "./types";

import { isGitRoot } from "../utils";
import { Select } from "components/input";

const EnvironmentUrlField = <Values extends FormikValues>({
  getEnvUrlByRoot,
  selectedRoot,
  setFieldValue,
  values,
}: TEnvironmentUrlFieldProps<Values>): JSX.Element => {
  const { t } = useTranslation();
  const gitEnvironmentUrls = getEnvUrlByRoot(selectedRoot?.id);
  const environmentOptions = [{ header: "", value: "" }].concat(
    gitEnvironmentUrls.map((envUrl): { header: string; value: string } => ({
      header: envUrl.url,
      value: envUrl.url,
    })),
  );

  useEffect((): void => {
    if (
      !_.isEmpty(values.environmentUrl) &&
      (_.isUndefined(selectedRoot) ||
        !isGitRoot(selectedRoot) ||
        (!_.isUndefined(selectedRoot) &&
          isGitRoot(selectedRoot) &&
          _.isEmpty(gitEnvironmentUrls)))
    ) {
      void setFieldValue("environmentUrl", "");
    }
    if (
      _.isEmpty(values.environmentUrl) &&
      !_.isUndefined(selectedRoot) &&
      isGitRoot(selectedRoot) &&
      !_.isEmpty(gitEnvironmentUrls)
    ) {
      void setFieldValue("environmentUrl", gitEnvironmentUrls[0].url);
    }
  }, [setFieldValue, selectedRoot, values, gitEnvironmentUrls]);

  return (
    <StrictMode>
      {!_.isUndefined(selectedRoot) && isGitRoot(selectedRoot) ? (
        <Select
          items={environmentOptions}
          label={t("group.toe.inputs.addModal.fields.environmentUrl")}
          name={"environmentUrl"}
        />
      ) : undefined}
    </StrictMode>
  );
};

export { EnvironmentUrlField };
