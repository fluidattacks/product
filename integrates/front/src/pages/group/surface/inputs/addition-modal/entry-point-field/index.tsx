import { useEffect } from "react";
import { useTranslation } from "react-i18next";

import type { TRoot } from "../types";
import { getNewHost } from "../utils";
import { Input } from "components/input";

interface IEntryPointFieldProps {
  readonly environmentUrl?: string;
  readonly selectedRoot?: TRoot;
  readonly setHost: (host: string | undefined) => void;
}

const EntryPointField = ({
  environmentUrl,
  selectedRoot,
  setHost,
}: IEntryPointFieldProps): JSX.Element => {
  const { t } = useTranslation();

  useEffect((): void => {
    const newHost = getNewHost(environmentUrl, selectedRoot);
    setHost(newHost);
  }, [environmentUrl, selectedRoot, setHost]);

  return (
    <Input
      label={t("group.toe.inputs.addModal.fields.entryPoint")}
      name={"entryPoint"}
    />
  );
};

export { EntryPointField };
