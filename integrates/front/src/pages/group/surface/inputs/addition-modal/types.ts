import type { IUseModal } from "hooks/use-modal";

interface IFormValues {
  entryPoint: string;
  environmentUrl: string;
  path: string;
  rootId: string | undefined;
  rootNickname: string | undefined;
}

interface IAdditionModalFormProps {
  roots: TRoot[];
  host: string | undefined;
  getEnvUrlByRoot: (rootId: string | undefined) => IGitEnvironmentURLAttr[];
  handleCloseModal: () => void;
  setHost: (host: string | undefined) => void;
}

interface IAddToeInputResultAttr {
  addToeInput: {
    success: boolean;
  };
}

interface IRootsData {
  group: { roots: TRoot[]; gitEnvironmentUrls: IGitEnvironmentURLAttr[] };
}

interface IAdditionModalProps {
  groupName: string;
  modalRef: IUseModal;
  rootsData: IRootsData | undefined;
  refetchData: () => void;
}

interface IGitEnvironmentURLAttr {
  url: string;
  id: string;
  rootId: string;
  urlType: string;
}

interface IGitRootAttr {
  __typename: "GitRoot";
  gitEnvironmentUrls: IGitEnvironmentURLAttr[];
  id: string;
  nickname: string;
  state: "ACTIVE" | "INACTIVE";
}

interface IIPRootAttr {
  __typename: "IPRoot";
  id: string;
  nickname: string;
}

interface IURLRootAttr {
  __typename: "URLRoot";
  host: string;
  id: string;
  nickname: string;
  path: string;
  port: number;
  protocol: "HTTP" | "HTTPS";
  query: string | null;
  state: "ACTIVE" | "INACTIVE";
}

type TRoot = IGitRootAttr | IIPRootAttr | IURLRootAttr;

export type {
  IFormValues,
  IAdditionModalProps,
  IAdditionModalFormProps,
  IAddToeInputResultAttr,
  TRoot,
  IRootsData,
  IGitEnvironmentURLAttr,
  IGitRootAttr,
  IIPRootAttr,
  IURLRootAttr,
};
