import _ from "lodash";
import type {
  AnyObject,
  InferType,
  Maybe,
  ObjectSchema,
  Schema,
  TestContext,
  ValidationError,
} from "yup";
import { object, string } from "yup";

import type { IGitRootAttr, IURLRootAttr } from "./types";
import { isGitRoot } from "./utils";

import { translate } from "utils/translations/translate";

const validatePath =
  (
    host: string | undefined,
  ): ((
    value: string | undefined,
    thisContext: TestContext<Maybe<AnyObject>>,
  ) => ValidationError | boolean) =>
  (
    value: string | undefined,
    thisContext: TestContext<Maybe<AnyObject>>,
  ): ValidationError | boolean => {
    if (value === undefined) {
      return true;
    }
    const formattedHost = host
      ?.replace("https://", "")
      .replace("http://", "")
      .slice(0, -1);

    if (
      (!_.isUndefined(formattedHost) && value.includes(formattedHost)) ||
      value.includes("https://") ||
      value.includes("http://")
    ) {
      return thisContext.createError({
        message: translate.t("validations.excludePathHost"),
      });
    }

    if (!_.isEmpty(value) && value.startsWith("/")) {
      return thisContext.createError({
        message: translate.t("validations.invalidTextBeginning", {
          chars: "/",
        }),
      });
    }

    return true;
  };

export const validationSchema = (
  roots: (IGitRootAttr | IURLRootAttr)[],
  host: string | undefined,
): ObjectSchema<InferType<Schema>> =>
  object().shape({
    entryPoint: string().isValidTextBeginning().isValidTextField(),
    environmentUrl: string().isValidFunction((value, thisContext): boolean => {
      const { rootNickname } = thisContext.parent;
      const selectedRoots = roots.filter(
        (root): boolean => root.nickname === rootNickname,
      );
      if (selectedRoots.length === 1) {
        if (isGitRoot(selectedRoots[0])) {
          return !_.isEmpty(value);
        }

        return true;
      }

      return false;
    }, translate.t("validations.required")),
    path: string()
      .isValidTextBeginning()
      .isValidTextField()
      .isValidDynamicValidation(validatePath(host)),
    rootId: string().required(translate.t("validations.required")),
    rootNickname: string()
      .oneOf(
        roots.map((root): string => root.nickname),
        translate.t("validations.oneOf"),
      )
      .when("reason", ([reason]: string[]): Schema => {
        return reason === "MISSING_SUPPLIES"
          ? string().notRequired()
          : string().required();
      }),
  });
