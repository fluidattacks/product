import { fireEvent, screen, waitFor, within } from "@testing-library/react";
import { userEvent } from "@testing-library/user-event";
import { GraphQLError } from "graphql";
import { HttpResponse, type StrictResponse, graphql } from "msw";

import { ADD_TOE_INPUT } from "./queries";
import type { IRootsData } from "./types";

import { AdditionModal } from ".";
import type {
  AddToeInputMutation as AddToeInput,
  GetRootsInfoAtInputsQuery as GetRootsInfoAtInputs,
} from "gql/graphql";
import { ResourceState, UrlProtocol } from "gql/graphql";
import { render } from "mocks";
import { LINK } from "mocks/constants";
import type { IErrorMessage, IMessage } from "mocks/types";
import { msgError, msgSuccess } from "utils/notifications";

jest.mock("utils/notifications", (): Record<string, unknown> => {
  const mockedNotifications: Record<string, () => Record<string, unknown>> =
    jest.requireActual("utils/notifications");
  jest.spyOn(mockedNotifications, "msgSuccess").mockImplementation();
  jest.spyOn(mockedNotifications, "msgError").mockImplementation();

  return mockedNotifications;
});

describe("handle addition modal", (): void => {
  const btnConfirm = "buttons.confirm";
  const graphqlMocked = graphql.link(LINK);
  const refetchDataFn: jest.Mock = jest.fn();
  const modalRef = {
    close: jest.fn(),
    isOpen: true,
    name: "test-edition-modal",
    open: jest.fn(),
    setIsOpen: jest.fn(),
  };
  const mocksMutation = graphqlMocked.mutation(
    ADD_TOE_INPUT,
    ({ variables }): StrictResponse<IErrorMessage | { data: AddToeInput }> => {
      const { component, entryPoint, groupName, rootId } = variables;

      if (
        component === "https://app.fluidattacks.com/test/test" &&
        entryPoint === "test" &&
        groupName === "groupname" &&
        rootId === "4039d098-ffc5-4984-8ed3-eb17bca98e19"
      ) {
        return HttpResponse.json({
          data: {
            addToeInput: { success: true },
          },
        });
      }

      return HttpResponse.json({
        errors: [new GraphQLError("Get group user error")],
      });
    },
  );

  const queryMock: GetRootsInfoAtInputs = {
    group: {
      __typename: "Group",
      gitEnvironmentUrls: [
        {
          __typename: "GitEnvironmentUrl",
          id: "00fbe3579a253b43239954a545dc0536e6c83094",
          rootId: "4039d098-ffc5-4984-8ed3-eb17bca98e19",
          url: "https://app.fluidattacks.com/test/",
          urlType: "URL",
        },
      ],
      name: "test",
      roots: [
        {
          __typename: "GitRoot",
          id: "4039d098-ffc5-4984-8ed3-eb17bca98e19",
          nickname: "universe",
          state: ResourceState.Active,
        },
      ],
    },
  };

  it("should render modal", async (): Promise<void> => {
    expect.hasAssertions();

    jest.clearAllMocks();

    render(
      <AdditionModal
        groupName={"groupname"}
        modalRef={modalRef}
        refetchData={refetchDataFn}
        rootsData={queryMock as IRootsData}
      />,
    );

    await waitFor((): void => {
      expect(
        screen.getByText("group.toe.inputs.addModal.title"),
      ).toBeInTheDocument();
    });

    const environmentUrl = within(
      screen.getByRole("combobox", { name: "environmentUrl" }),
    ).getByText("https://app.fluidattacks.com/test/");

    await waitFor((): void => {
      expect(environmentUrl).toBeInTheDocument();
    });

    expect(
      screen.getByText("group.toe.inputs.addModal.fields.root"),
    ).toBeInTheDocument();
    expect(
      screen.getByText("group.toe.inputs.addModal.fields.environmentUrl"),
    ).toBeInTheDocument();
    expect(
      screen.getByText("group.toe.inputs.addModal.fields.component"),
    ).toBeInTheDocument();
    expect(
      screen.getByText("group.toe.inputs.addModal.fields.entryPoint"),
    ).toBeInTheDocument();
  });

  it("should submit form", async (): Promise<void> => {
    expect.hasAssertions();

    const role = "combobox";
    jest.clearAllMocks();

    render(
      <AdditionModal
        groupName={"groupname"}
        modalRef={modalRef}
        refetchData={refetchDataFn}
        rootsData={queryMock as IRootsData}
      />,
      { mocks: [mocksMutation] },
    );

    await screen.findByText("group.toe.inputs.addModal.title");
    await waitFor((): void => {
      expect(
        screen.getByRole("combobox", { name: "rootNickname" }),
      ).toHaveValue("universe");
    });

    expect(
      screen.queryByRole("combobox", { name: "environmentUrl" }),
    ).toBeInTheDocument();

    const rootInput = screen.getByRole("combobox", { name: "rootNickname" });
    const environmentInput = screen.getByRole("combobox", {
      name: "environmentUrl",
    });
    const componentInput = screen.getByRole(role, { name: "path" });
    const entryPointInput = screen.getByRole(role, { name: "entryPoint" });

    fireEvent.change(componentInput, { target: { value: "test" } });
    fireEvent.change(entryPointInput, { target: { value: "test" } });

    expect(rootInput).toHaveValue("universe");

    expect(
      within(environmentInput).getByText("https://app.fluidattacks.com/test/"),
    ).toBeInTheDocument();

    expect(componentInput).toHaveValue("test");
    expect(entryPointInput).toHaveValue("test");

    await userEvent.click(screen.getByText(btnConfirm));

    await waitFor((): void => {
      expect(msgSuccess).toHaveBeenCalledWith(
        "group.toe.inputs.addModal.alerts.success",
        "groupAlerts.titleSuccess",
      );
    });

    expect(modalRef.close).toHaveBeenCalledTimes(1);
    expect(refetchDataFn).toHaveBeenCalledTimes(1);
  });

  it("should show error submit form changing environment", async (): Promise<void> => {
    expect.hasAssertions();

    jest.clearAllMocks();

    const queryMockEnv: GetRootsInfoAtInputs = {
      group: {
        __typename: "Group",
        gitEnvironmentUrls: [
          {
            __typename: "GitEnvironmentUrl",
            id: "00fbe3579a253b43239954a545dc0536e6c83094",
            rootId: "4039d098-ffc5-4984-8ed3-eb17bca98e19",
            url: "https://app.fluidattacks.com/test/",
            urlType: "URL",
          },
          {
            __typename: "GitEnvironmentUrl",
            id: "00d424fff563f0af7a48b804d8fc41da92821209",
            rootId: "4039d098-ffc5-4984-8ed3-eb17bca98e19",
            url: "https://test.com/test/",
            urlType: "URL",
          },
        ],
        name: "test",
        roots: [
          {
            __typename: "GitRoot",
            id: "4039d098-ffc5-4984-8ed3-eb17bca98e19",
            nickname: "universe",
            state: ResourceState.Active,
          },
        ],
      },
    };
    render(
      <AdditionModal
        groupName={"groupname"}
        modalRef={modalRef}
        refetchData={refetchDataFn}
        rootsData={queryMockEnv as IRootsData}
      />,
      { mocks: [mocksMutation] },
    );

    await screen.findByText("group.toe.inputs.addModal.title");
    await waitFor((): void => {
      expect(
        screen.getByRole("combobox", { name: "rootNickname" }),
      ).toHaveValue("universe");
    });

    await userEvent.click(
      screen.getByTestId("environmentUrl-select-selected-option"),
    );

    const rootInput = screen.getByRole("combobox", { name: "rootNickname" });
    const environmentInput = screen.getByRole("combobox", {
      name: "environmentUrl",
    });
    await userEvent.click(screen.getByText("https://test.com/test/"));

    expect(rootInput).toHaveValue("universe");

    expect(
      within(environmentInput).getByText("https://test.com/test/"),
    ).toBeInTheDocument();

    await userEvent.click(screen.getByText(btnConfirm));

    await waitFor((): void => {
      expect(msgError).toHaveBeenCalledWith("groupAlerts.errorTextsad");
    });
  });

  it("should submit form non gitroot", async (): Promise<void> => {
    expect.hasAssertions();

    const role = "combobox";

    jest.clearAllMocks();
    const mocksNonGitRootMutation = graphqlMocked.mutation(
      ADD_TOE_INPUT,
      ({
        variables,
      }): StrictResponse<IErrorMessage | { data: AddToeInput }> => {
        const { component, entryPoint, groupName, rootId } = variables;

        if (
          component === "https://app.test.test:443/test" &&
          entryPoint === "test" &&
          groupName === "groupname" &&
          rootId === "8493c82f-2860-4902-86fa-75b0fef76034"
        ) {
          return HttpResponse.json({
            data: {
              addToeInput: { success: true },
            },
          });
        }

        return HttpResponse.json({
          errors: [new GraphQLError("Get group user error")],
        });
      },
    );

    const queryNonGitRootMock: GetRootsInfoAtInputs = {
      group: {
        __typename: "Group",
        gitEnvironmentUrls: [],
        name: "test",
        roots: [
          {
            __typename: "URLRoot",
            host: "app.test.test",
            id: "8493c82f-2860-4902-86fa-75b0fef76034",
            nickname: "url_root_1",
            path: "/",
            port: 443,
            protocol: UrlProtocol.Https,
            query: null,
            state: ResourceState.Active,
          },
          {
            __typename: "URLRoot",
            host: "app.test.test.test",
            id: "8493c82f-2860-4902-86fa-75b0fef76035",
            nickname: "url_root_2",
            path: "/",
            port: 808,
            protocol: UrlProtocol.Https,
            query: "=1234&page=1&start=56789",
            state: ResourceState.Active,
          },
          {
            __typename: "IPRoot",
            id: "d312f0b9-da49-4d2b-a881-bed438875e99",
            nickname: "ip_root_1",
            state: ResourceState.Active,
          },
        ],
      },
    };

    render(
      <AdditionModal
        groupName={"groupname"}
        modalRef={modalRef}
        refetchData={refetchDataFn}
        rootsData={queryNonGitRootMock as IRootsData}
      />,
      { mocks: [mocksNonGitRootMutation] },
    );

    await screen.findByText("group.toe.inputs.addModal.title");
    await waitFor((): void => {
      expect(
        screen.getByRole("combobox", { name: "rootNickname" }),
      ).toHaveValue("url_root_1");
    });

    expect(
      screen.queryByRole("combobox", { name: "environmentUrl" }),
    ).not.toBeInTheDocument();

    const rootInput = screen.getByRole("combobox", { name: "rootNickname" });
    const componentInput = screen.getByRole(role, { name: "path" });
    const entryPointInput = screen.getByRole(role, { name: "entryPoint" });

    fireEvent.change(componentInput, { target: { value: "test" } });
    fireEvent.change(entryPointInput, { target: { value: "test" } });

    expect(rootInput).toHaveValue("url_root_1");
    expect(componentInput).toHaveValue("test");
    expect(entryPointInput).toHaveValue("test");

    await userEvent.click(screen.getByText(btnConfirm));

    await waitFor((): void => {
      expect(msgSuccess).toHaveBeenCalledWith(
        "group.toe.inputs.addModal.alerts.success",
        "groupAlerts.titleSuccess",
      );
    });

    expect(modalRef.close).toHaveBeenCalledTimes(1);
    expect(refetchDataFn).toHaveBeenCalledTimes(1);

    fireEvent.change(screen.getByRole("combobox", { name: "rootNickname" }), {
      target: { value: "url_root_2" },
    });
    await waitFor((): void => {
      expect(
        screen.getByRole("combobox", { name: "rootNickname" }),
      ).toHaveValue("url_root_2");
    });

    fireEvent.change(screen.getByRole(role, { name: "entryPoint" }), {
      target: { value: "test" },
    });

    expect(screen.getByRole("combobox", { name: "rootNickname" })).toHaveValue(
      "url_root_2",
    );
    expect(screen.getByRole(role, { name: "entryPoint" })).toHaveValue("test");

    await userEvent.click(screen.getByText(btnConfirm));

    await waitFor((): void => {
      expect(msgSuccess).toHaveBeenCalledWith(
        "group.toe.inputs.addModal.alerts.success",
        "groupAlerts.titleSuccess",
      );
    });
  });

  it("should handle error adding input", async (): Promise<void> => {
    expect.hasAssertions();

    const role = "combobox";

    jest.clearAllMocks();
    const mocksError = graphqlMocked.mutation(
      ADD_TOE_INPUT,
      ({
        variables,
      }): StrictResponse<
        Record<"errors", IMessage[]> | { data: AddToeInput }
      > => {
        const { groupName, rootId } = variables;

        if (
          groupName === "groupname" &&
          rootId === "4039d098-ffc5-4984-8ed3-eb17bca98e19"
        ) {
          return HttpResponse.json({
            errors: [
              new GraphQLError("Exception - Toe input already exists"),
              new GraphQLError("An error occurred adding toe input"),
            ],
          });
        }

        return HttpResponse.json({
          data: {
            addToeInput: { success: true },
          },
        });
      },
    );

    render(
      <AdditionModal
        groupName={"groupname"}
        modalRef={modalRef}
        refetchData={refetchDataFn}
        rootsData={queryMock as IRootsData}
      />,
      { mocks: [mocksError] },
    );

    await screen.findByText("group.toe.inputs.addModal.title");
    await waitFor((): void => {
      expect(
        screen.getByRole("combobox", { name: "rootNickname" }),
      ).toHaveValue("universe");
    });

    expect(
      screen.queryByRole("combobox", { name: "environmentUrl" }),
    ).toBeInTheDocument();

    const rootInput = screen.getByRole("combobox", { name: "rootNickname" });
    const environmentInput = screen.getByRole("combobox", {
      name: "environmentUrl",
    });
    const componentInput = screen.getByRole(role, { name: "path" });
    const entryPointInput = screen.getByRole(role, { name: "entryPoint" });

    fireEvent.change(componentInput, { target: { value: "test" } });
    fireEvent.change(entryPointInput, { target: { value: "test" } });

    expect(rootInput).toHaveValue("universe");

    expect(
      within(environmentInput).getByText("https://app.fluidattacks.com/test/"),
    ).toBeInTheDocument();

    expect(componentInput).toHaveValue("test");
    expect(entryPointInput).toHaveValue("test");

    await userEvent.click(screen.getByText(btnConfirm));

    await waitFor((): void => {
      expect(msgError).toHaveBeenCalledWith(
        "group.toe.inputs.addModal.alerts.alreadyExists",
      );
    });
  });
});
