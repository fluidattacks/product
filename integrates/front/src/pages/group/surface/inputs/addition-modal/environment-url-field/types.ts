import type { FormikProps, FormikValues } from "formik";

import type { IGitEnvironmentURLAttr, TRoot } from "../types";

interface IEnvironmentUrlFieldProps {
  selectedRoot: TRoot | undefined;
  getEnvUrlByRoot: (rootId: string | undefined) => IGitEnvironmentURLAttr[];
}

type TEnvironmentUrlFieldProps<Values extends FormikValues> =
  IEnvironmentUrlFieldProps &
    Pick<FormikProps<Values>, "setFieldValue" | "values">;

export type { IEnvironmentUrlFieldProps, TEnvironmentUrlFieldProps };
