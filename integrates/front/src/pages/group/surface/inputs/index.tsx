/* eslint-disable max-lines */
import { useApolloClient, useMutation, useQuery } from "@apollo/client";
import type { FetchResult } from "@apollo/client";
import { useAbility } from "@casl/react";
import type { ColumnDef, Row } from "@tanstack/react-table";
import _ from "lodash";
import { useCallback, useEffect, useMemo, useState } from "react";
import * as React from "react";
import { useTranslation } from "react-i18next";
import { useParams } from "react-router-dom";

import { ActionButtons } from "./action-buttons";
import { AdditionModal } from "./addition-modal";
import { GET_ROOTS } from "./addition-modal/queries";
import type { IRootsData } from "./addition-modal/types";
import {
  GET_TOE_INPUTS,
  TOE_INPUTS_FRAGMENT,
  UPDATE_TOE_INPUT,
} from "./queries";
import type {
  IGroupToeInputsProps,
  IToeInputAttr,
  IToeInputData,
  IToeInputEdge,
} from "./types";
import { formatToeInputData, isEqualRootId } from "./utils";

import { editableBePresentFormatter } from "../formatters/editable-be-present";
import { Table } from "components/table";
import { PROCESSING_ROWS_LIMIT } from "components/table/export-multiple-csv/export-options";
import { filterDate } from "components/table/utils";
import { authzPermissionsContext } from "context/authz/config";
import { ToEInputsFilters } from "features/toes-inputs/filters";
import { getFiltersToResearch } from "features/toes-inputs/filters/utils";
import { getFragmentData } from "gql/fragment-masking";
import type {
  GetToeInputsQueryVariables,
  UpdateToeInputMutation,
} from "gql/graphql";
import { useDebouncedCallback, useModal, useTable } from "hooks";
import { useAudit } from "hooks/use-audit";
import { formatDate } from "utils/date";
import { formatBoolean, formatStatus, jsxStatus } from "utils/format-helpers";
import { getErrors } from "utils/helpers";
import { Logger } from "utils/logger";
import { msgError, msgSuccess } from "utils/notifications";

const DEBOUNCE_DELAY = 500;
const GroupToeInputs: React.FC<IGroupToeInputsProps> = ({
  isInternal,
}): JSX.Element => {
  const { t } = useTranslation();
  const client = useApolloClient();

  const permissions = useAbility(authzPermissionsContext);
  const canGetAttackedAt = permissions.can(
    "integrates_api_resolvers_toe_input_attacked_at_resolve",
  );
  const canGetAttackedBy = permissions.can(
    "integrates_api_resolvers_toe_input_attacked_by_resolve",
  );
  const canGetBePresentUntil = permissions.can(
    "integrates_api_resolvers_toe_input_be_present_until_resolve",
  );
  const canGetFirstAttackAt = permissions.can(
    "integrates_api_resolvers_toe_input_first_attack_at_resolve",
  );
  const canGetSeenFirstTimeBy = permissions.can(
    "integrates_api_resolvers_toe_input_seen_first_time_by_resolve",
  );
  const canUpdateToeInput = permissions.can(
    "integrates_api_mutations_update_toe_input_mutate",
  );

  const { groupName } = useParams() as { groupName: string };
  const additionModalProps = useModal("addition-modal");
  const [isMarkingAsAttacked, setIsMarkingAsAttacked] = useState(false);
  const [filtersToSearch, setFiltersToSearch] = useState<
    Partial<GetToeInputsQueryVariables>
  >(getFiltersToResearch([]));
  const [selectedToeInputData, setSelectedToeInputData] = useState<
    IToeInputData[]
  >([]);

  const tableRef = useTable(
    "tblToeInputs",
    {
      attackedAt: true,
      attackedBy: false,
      bePresent: false,
      bePresentUntil: false,
      component: false,
      entryPoint: true,
      firstAttackAt: false,
      hasVulnerabilities: true,
      rootNickname: true,
      seenAt: true,
      seenFirstTimeBy: true,
    },
    ["rootNickname", "entryPoint", "hasVulnerabilities", "seenAt"],
    { left: ["rootNickname"] },
  );

  const [handleUpdateToeInput] = useMutation(UPDATE_TOE_INPUT, {
    onError: (errors): void => {
      errors.graphQLErrors.forEach((error): void => {
        switch (error.message) {
          case "Exception - The toe input is not present":
            msgError(t("group.toe.inputs.alerts.nonPresent"));
            break;
          case "Exception - The attack time must be between the previous attack and the current time":
            msgError(t("group.toe.inputs.alerts.invalidAttackedAt"));
            break;
          case "Exception - The toe input has been updated by another operation":
            msgError(t("group.toe.inputs.alerts.alreadyUpdate"));
            break;
          case "Exception - The environment has been excluded":
            msgError(t("group.toe.inputs.alerts.excludedUrlFromScope"));
            break;
          default:
            msgError(t("groupAlerts.errorTextsad"));
            Logger.warning("An error occurred updating the toe input", error);
        }
      });
    },
  });

  const getToeInputsVariables = useMemo(
    (): GetToeInputsQueryVariables => ({
      canGetAttackedAt,
      canGetAttackedBy,
      canGetBePresentUntil,
      canGetFirstAttackAt,
      canGetSeenFirstTimeBy,
      groupName,
    }),
    [
      canGetAttackedAt,
      canGetAttackedBy,
      canGetBePresentUntil,
      canGetFirstAttackAt,
      canGetSeenFirstTimeBy,
      groupName,
    ],
  );

  const { data: rootsData } = useQuery(GET_ROOTS, {
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error): void => {
        Logger.error("Couldn't load roots", error);
      });
    },
    variables: { groupName },
  });

  const { addAuditEvent } = useAudit();
  const { data, fetchMore, loading, refetch } = useQuery(GET_TOE_INPUTS, {
    fetchPolicy: "network-only",
    nextFetchPolicy: "cache-first",
    onCompleted: (): void => {
      addAuditEvent("Group.ToeInputs", groupName);
    },
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error): void => {
        Logger.error("Couldn't load toe inputs", error);
      });
    },
    variables: {
      ...getToeInputsVariables,
      ...filtersToSearch,
      first: 150,
    },
  });
  const toeInputsEdges = data === undefined ? [] : data.group.toeInputs.edges;
  const toeInputs = toeInputsEdges.map(
    (edge): IToeInputData => formatToeInputData(edge.node as IToeInputAttr),
  );

  const handleSearch = useDebouncedCallback((search: string): void => {
    void refetch({ search });
  }, DEBOUNCE_DELAY);

  const handleNextPage = useCallback(async (): Promise<void> => {
    const pageInfo =
      data === undefined
        ? { endCursor: [], hasNextPage: false }
        : {
            endCursor: data.group.toeInputs.pageInfo.endCursor,
            hasNextPage: data.group.toeInputs.pageInfo.hasNextPage,
          };
    if (pageInfo.hasNextPage) {
      await fetchMore({ variables: { after: pageInfo.endCursor } });
    }
  }, [data, fetchMore]);

  const fetchReportData = useCallback(async (): Promise<void> => {
    const fetchMoreData = async (
      endCursor: string,
      count: number,
    ): Promise<void> => {
      const result = await fetchMore({
        variables: { after: endCursor },
      });
      const { endCursor: newEndCursor, hasNextPage } =
        result.data.group.toeInputs.pageInfo;
      if (hasNextPage && count < PROCESSING_ROWS_LIMIT) {
        await fetchMoreData(
          newEndCursor,
          count + result.data.group.toeInputs.edges.length,
        );
      }
    };
    if (data !== undefined) {
      await fetchMoreData(
        data.group.toeInputs.pageInfo.endCursor,
        data.group.toeInputs.edges.length,
      );
    }
  }, [data, fetchMore]);

  const storeUpdatedInput = (
    rootId: string,
    component: string,
    entryPoint: string,
    updatedToeInput: IToeInputAttr,
  ): void => {
    if (data) {
      client.writeQuery({
        data: {
          ...data,
          group: {
            ...data.group,
            toeInputs: {
              ...data.group.toeInputs,
              edges: data.group.toeInputs.edges.map((value): IToeInputEdge => {
                const node = getFragmentData(TOE_INPUTS_FRAGMENT, value.node);

                if (
                  node.component === component &&
                  node.entryPoint === entryPoint &&
                  isEqualRootId(node.root, rootId)
                ) {
                  return { node: updatedToeInput };
                }

                return { node: node as IToeInputAttr };
              }),
            },
          },
        },
        query: GET_TOE_INPUTS,
        variables: {
          ...getToeInputsVariables,
          ...filtersToSearch,
          first: 150,
        },
      });
    }
  };

  const handleUpdateToeInputBePresent = async (
    row: IToeInputData,
  ): Promise<void> => {
    const { rootId, component, entryPoint, bePresent } = row;
    const result = await handleUpdateToeInput({
      variables: {
        ...getToeInputsVariables,
        bePresent: !bePresent,
        component,
        entryPoint,
        groupName,
        hasRecentAttack: undefined,
        rootId,
        shouldGetNewToeInput: true,
      },
    });

    if (!_.isNil(result.data) && result.data.updateToeInput.success) {
      const updatedToeInput = result.data.updateToeInput
        .toeInput as IToeInputAttr;
      if (!_.isUndefined(updatedToeInput)) {
        setSelectedToeInputData(
          selectedToeInputData
            .map(
              (toeInputData): IToeInputData =>
                toeInputData.component === component &&
                toeInputData.entryPoint === entryPoint &&
                toeInputData.rootId === rootId
                  ? formatToeInputData(updatedToeInput)
                  : toeInputData,
            )
            .filter((toeInputData): boolean => toeInputData.bePresent),
        );
        storeUpdatedInput(rootId, component, entryPoint, updatedToeInput);
      }
      msgSuccess(
        t("group.toe.inputs.alerts.updateInput"),
        t("groupAlerts.updatedTitle"),
      );
    }
  };

  const columns: ColumnDef<IToeInputData>[] = [
    {
      accessorKey: "rootNickname",
      header: t("group.toe.inputs.root"),
      meta: { filterType: "select" },
    },
    {
      accessorKey: "component",
      header: t("group.toe.inputs.component"),
      meta: { filterType: "select" },
    },
    {
      accessorKey: "entryPoint",
      header: t("group.toe.inputs.entryPoint"),
    },
    {
      accessorFn: (row: IToeInputData): string =>
        formatStatus(row.hasVulnerabilities),

      cell: (cell): JSX.Element => jsxStatus(String(cell.getValue())),
      header: String(t("group.toe.inputs.status")),
      id: "hasVulnerabilities",
      meta: { filterType: "select" },
    },
    {
      accessorKey: "seenAt",
      cell: (cell): string => formatDate(cell.getValue<string>()),
      filterFn: filterDate,
      header: t("group.toe.inputs.seenAt"),
      meta: { filterType: "dateRange" },
    },
    {
      accessorFn: (row: IToeInputData): string => {
        return formatBoolean(row.bePresent);
      },
      cell: (cell): JSX.Element | string =>
        editableBePresentFormatter(
          canUpdateToeInput && isInternal,
          handleUpdateToeInputBePresent,
          cell.row.original,
        ),
      header: t("group.toe.inputs.bePresent"),
      id: "bePresent",
      meta: { filterType: "select" },
    },
    {
      accessorKey: "attackedAt",
      cell: (cell): string => formatDate(cell.getValue<string>()),
      filterFn: filterDate,
      header: t("group.toe.inputs.attackedAt"),
      id: "attackedAt",
      meta: { filterType: "dateRange" },
    },
    {
      accessorKey: "attackedBy",
      header: t("group.toe.inputs.attackedBy"),
      id: "attackedBy",
    },
    {
      accessorKey: "firstAttackAt",
      cell: (cell): string => formatDate(cell.getValue<string>()),
      filterFn: filterDate,
      header: t("group.toe.inputs.firstAttackAt"),
      id: "firstAttackAt",
      meta: { filterType: "dateRange" },
    },
    {
      accessorKey: "seenFirstTimeBy",
      header: t("group.toe.inputs.seenFirstTimeBy"),
      id: "seenFirstTimeBy",
    },
    {
      accessorKey: "bePresentUntil",
      cell: (cell): string => formatDate(cell.getValue<string>()),
      filterFn: filterDate,
      header: t("group.toe.inputs.bePresentUntil"),
      id: "bePresentUntil",
      meta: { filterType: "dateRange" },
    },
  ];

  const tableColumns = columns.filter((column): boolean => {
    switch (column.id) {
      case "attackedAt":
        return isInternal && canGetAttackedAt;
      case "attackedBy":
        return isInternal && canGetAttackedBy;
      case "firstAttackAt":
        return isInternal && canGetFirstAttackAt;
      case "seenFirstTimeBy":
        return isInternal && canGetSeenFirstTimeBy;
      case "bePresentUntil":
        return isInternal && canGetBePresentUntil;
      case undefined:
      default:
        return true;
    }
  });

  useEffect((): void => {
    setSelectedToeInputData([]);
    void refetch();
  }, [refetch]);

  const toggleAdd = useCallback((): void => {
    additionModalProps.open();
  }, [additionModalProps]);

  const handleOnMarkAsAttackedCompleted = useCallback(
    (result: FetchResult<UpdateToeInputMutation>): void => {
      if (!_.isNil(result.data) && result.data.updateToeInput.success) {
        msgSuccess(
          t("group.toe.inputs.alerts.markAsAttacked.success"),
          t("groupAlerts.updatedTitle"),
        );
        void refetch();
        setSelectedToeInputData([]);
      }
    },
    [refetch, t],
  );

  const handleMarkAsAttacked = useCallback(async (): Promise<void> => {
    const presentSelectedToeInputData = selectedToeInputData.filter(
      (toeInputData): boolean => toeInputData.bePresent,
    );
    setIsMarkingAsAttacked(true);
    const results = await Promise.all(
      presentSelectedToeInputData.map(
        async (toeInputData): Promise<FetchResult<UpdateToeInputMutation>> =>
          handleUpdateToeInput({
            variables: {
              ...getToeInputsVariables,
              bePresent: toeInputData.bePresent,
              component: toeInputData.component,
              entryPoint: toeInputData.entryPoint,
              groupName,
              hasRecentAttack: true,
              rootId: toeInputData.rootId,
              shouldGetNewToeInput: false,
            },
          }),
      ),
    );

    const errors = getErrors<UpdateToeInputMutation>(results);

    if (!_.isEmpty(results) && _.isEmpty(errors)) {
      handleOnMarkAsAttackedCompleted(results[0]);
    } else {
      void refetch();
    }
    setIsMarkingAsAttacked(false);
  }, [
    getToeInputsVariables,
    groupName,
    handleOnMarkAsAttackedCompleted,
    handleUpdateToeInput,
    refetch,
    selectedToeInputData,
  ]);

  const enabledRows = useCallback((row: Row<IToeInputData>): boolean => {
    return row.original.bePresent;
  }, []);

  return (
    <React.StrictMode>
      <Table
        columns={tableColumns}
        data={toeInputs}
        extraButtons={
          <ActionButtons
            areInputsSelected={selectedToeInputData.length > 0}
            data={toeInputs}
            fetchReportData={fetchReportData}
            isAdding={additionModalProps.isOpen}
            isInternal={isInternal}
            isMarkingAsAttacked={isMarkingAsAttacked}
            onAdd={toggleAdd}
            onMarkAsAttacked={handleMarkAsAttacked}
            size={data?.group.toeInputs.total ?? 0}
          />
        }
        filters={
          <ToEInputsFilters
            groupName={groupName}
            isInternal={isInternal}
            rootsData={rootsData}
            setFiltersToSearch={setFiltersToSearch}
            toeInputs={toeInputs}
          />
        }
        loadingData={loading}
        onNextPage={handleNextPage}
        onSearch={handleSearch}
        options={{
          columnToggle: true,
          enableRowSelection: enabledRows,
          size: data?.group.toeInputs.total ?? 0,
        }}
        rowSelectionSetter={
          !isInternal || !canUpdateToeInput
            ? undefined
            : setSelectedToeInputData
        }
        rowSelectionState={selectedToeInputData}
        tableRef={tableRef}
      />
      <AdditionModal
        groupName={groupName}
        modalRef={additionModalProps}
        refetchData={refetch}
        rootsData={
          rootsData === undefined ? undefined : (rootsData as IRootsData)
        }
      />
    </React.StrictMode>
  );
};

export { GroupToeInputs };
