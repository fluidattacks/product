import { Container } from "@fluidattacks/design";
import * as React from "react";
import { Outlet } from "react-router-dom";
import { useTheme } from "styled-components";

const GroupSurface: React.FC = (): JSX.Element => {
  const theme = useTheme();

  return (
    <Container
      bgColor={theme.palette.gray[50]}
      height={"100hv"}
      id={"users"}
      px={1.25}
      py={1.25}
    >
      <Outlet />
    </Container>
  );
};

export { GroupSurface };
