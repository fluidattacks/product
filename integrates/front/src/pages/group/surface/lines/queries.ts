import { graphql } from "gql";

const TOE_LINES_FRAGMENT = graphql(`
  fragment toeLinesFields on ToeLines {
    __typename
    attackedAt @include(if: $canGetAttackedAt)
    attackedBy @include(if: $canGetAttackedBy)
    attackedLines @include(if: $canGetAttackedLines)
    bePresent
    bePresentUntil @include(if: $canGetBePresentUntil)
    comments @include(if: $canGetComments)
    filename
    firstAttackAt @include(if: $canGetFirstAttackAt)
    hasVulnerabilities
    lastAuthor
    lastCommit
    loc
    modifiedDate
    seenAt
    sortsPriorityFactor
    root {
      id
      nickname
    }
    sortsSuggestions {
      findingTitle
      probability
    }
  }
`);

const GET_TOE_LINES = graphql(`
  query GetToeLines(
    $after: [String]
    $attackedBy: String
    $bePresent: Boolean
    $canGetAttackedAt: Boolean!
    $canGetAttackedBy: Boolean!
    $canGetAttackedLines: Boolean!
    $canGetBePresentUntil: Boolean!
    $canGetComments: Boolean!
    $canGetFirstAttackAt: Boolean!
    $comments: String
    $filename: String
    $first: Int
    $fromAttackedAt: DateTime
    $fromBePresentUntil: DateTime
    $fromFirstAttackAt: DateTime
    $fromModifiedDate: DateTime
    $fromSeenAt: DateTime
    $groupName: String!
    $hasVulnerabilities: Boolean
    $lastAuthor: String
    $lastCommit: String
    $maxAttackedLines: Int
    $maxCoverage: Int
    $maxLoc: Int
    $maxSortsPriorityFactor: Int
    $minAttackedLines: Int
    $minCoverage: Int
    $minLoc: Int = 1
    $minSortsPriorityFactor: Int
    $rootId: ID
    $sort: LinesSortInput
    $toAttackedAt: DateTime
    $toBePresentUntil: DateTime
    $toFirstAttackAt: DateTime
    $toModifiedDate: DateTime
    $toSeenAt: DateTime
  ) {
    group(groupName: $groupName) {
      __typename
      name
      toeLinesConnection(
        after: $after
        attackedBy: $attackedBy
        bePresent: $bePresent
        comments: $comments
        filename: $filename
        first: $first
        fromAttackedAt: $fromAttackedAt
        fromBePresentUntil: $fromBePresentUntil
        fromFirstAttackAt: $fromFirstAttackAt
        fromModifiedDate: $fromModifiedDate
        fromSeenAt: $fromSeenAt
        hasVulnerabilities: $hasVulnerabilities
        lastAuthor: $lastAuthor
        lastCommit: $lastCommit
        maxAttackedLines: $maxAttackedLines
        maxCoverage: $maxCoverage
        maxLoc: $maxLoc
        maxSortsPriorityFactor: $maxSortsPriorityFactor
        minAttackedLines: $minAttackedLines
        minCoverage: $minCoverage
        minLoc: $minLoc
        minSortsPriorityFactor: $minSortsPriorityFactor
        rootId: $rootId
        sort: $sort
        toAttackedAt: $toAttackedAt
        toBePresentUntil: $toBePresentUntil
        toFirstAttackAt: $toFirstAttackAt
        toModifiedDate: $toModifiedDate
        toSeenAt: $toSeenAt
      ) {
        __typename
        total
        edges {
          node {
            ...toeLinesFields
          }
        }
        pageInfo {
          endCursor
          hasNextPage
        }
      }
    }
  }
`);

const VERIFY_TOE_LINES = graphql(`
  mutation VerifyToeLines(
    $attackedLines: Int
    $canGetAttackedAt: Boolean!
    $canGetAttackedBy: Boolean!
    $canGetAttackedLines: Boolean!
    $canGetBePresentUntil: Boolean!
    $canGetComments: Boolean!
    $canGetFirstAttackAt: Boolean!
    $comments: String!
    $filename: String!
    $groupName: String!
    $rootId: String!
    $shouldGetNewToeLines: Boolean!
  ) {
    updateToeLinesAttackedLines(
      attackedLines: $attackedLines
      comments: $comments
      filename: $filename
      groupName: $groupName
      rootId: $rootId
    ) {
      success
      toeLines @include(if: $shouldGetNewToeLines) {
        ...toeLinesFields
      }
    }
  }
`);

export { GET_TOE_LINES, TOE_LINES_FRAGMENT, VERIFY_TOE_LINES };
