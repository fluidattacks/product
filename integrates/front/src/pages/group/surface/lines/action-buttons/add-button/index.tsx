import { Button } from "@fluidattacks/design";
import * as React from "react";
import { useTranslation } from "react-i18next";

import type { IAddButtonProps } from "./types";

import { Authorize } from "components/@core/authorize";

const AddButton: React.FC<IAddButtonProps> = ({
  isDisabled,
  onAdd,
}: IAddButtonProps): JSX.Element => {
  const { t } = useTranslation();

  return (
    <Authorize can={"integrates_api_mutations_add_toe_lines_mutate"}>
      <Button
        disabled={isDisabled}
        icon={"add"}
        id={"addToeInput"}
        onClick={onAdd}
        tooltip={t("group.toe.lines.actionButtons.addButton.tooltip")}
        variant={"primary"}
      >
        {t("group.toe.lines.actionButtons.addButton.text")}
      </Button>
    </Authorize>
  );
};

export { AddButton };
