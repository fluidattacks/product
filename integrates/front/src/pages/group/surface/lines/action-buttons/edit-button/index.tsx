import { Button } from "@fluidattacks/design";
import { useTranslation } from "react-i18next";

import { Authorize } from "components/@core/authorize";

interface IEditButtonProps {
  isDisabled: boolean;
  onEdit: () => void;
}

const EditButton = ({
  isDisabled,
  onEdit,
}: Readonly<IEditButtonProps>): JSX.Element => {
  const { t } = useTranslation();

  return (
    <Authorize
      can={"integrates_api_mutations_update_toe_lines_attacked_lines_mutate"}
    >
      <Button
        disabled={isDisabled}
        icon={"edit"}
        id={"editToeLines"}
        onClick={onEdit}
        tooltip={t("group.toe.lines.actionButtons.editButton.tooltip")}
        variant={"secondary"}
      >
        {t("buttons.edit")}
      </Button>
    </Authorize>
  );
};

export type { IEditButtonProps };
export { EditButton };
