import { Fragment, StrictMode, useCallback } from "react";

import { AddButton } from "./add-button";
import { EditButton } from "./edit-button";
import { VerifyButton } from "./verify-button";

import type { IToeLinesData } from "../types";
import { ExportButton } from "components/table/export-multiple-csv/export-button";
import { ReportType } from "gql/graphql";

interface IActionButtonsProps {
  data: IToeLinesData[];
  fetchReportData?: () => Promise<void>;
  isAdding: boolean;
  isInternal: boolean;
  isEditing: boolean;
  isVerifying: boolean;
  onAdd: () => void;
  onEdit: () => void;
  onVerify: () => void;
  selectedToeLinesData: IToeLinesData[];
  size?: number;
}

const ActionButtons = ({
  data,
  fetchReportData,
  isAdding,
  isInternal,
  isEditing,
  isVerifying,
  onAdd,
  onEdit,
  onVerify,
  selectedToeLinesData,
  size,
}: Readonly<IActionButtonsProps>): JSX.Element | null => {
  const isActiveAction = isAdding || isVerifying || isEditing;

  const Internal = useCallback(
    (): JSX.Element | null =>
      isInternal ? (
        <StrictMode>
          <AddButton isDisabled={isActiveAction} onAdd={onAdd} />
          <VerifyButton
            isDisabled={isActiveAction || selectedToeLinesData.length === 0}
            onVerify={onVerify}
          />
          <EditButton
            isDisabled={
              isActiveAction ||
              selectedToeLinesData.length > 1 ||
              selectedToeLinesData.length === 0
            }
            onEdit={onEdit}
          />
        </StrictMode>
      ) : null,
    [isActiveAction, isInternal, onAdd, onEdit, onVerify, selectedToeLinesData],
  );

  return (
    <Fragment>
      <Internal />
      <ExportButton
        data={data}
        fetchReportData={fetchReportData}
        reportType={ReportType.ToeLines}
        size={size}
      />
    </Fragment>
  );
};

export type { IActionButtonsProps };
export { ActionButtons };
