import { PureAbility } from "@casl/ability";
import { screen } from "@testing-library/react";
import { Route, Routes } from "react-router-dom";

import { ActionButtons } from ".";
import type { IToeLinesData } from "../types";
import { authzPermissionsContext } from "context/authz/config";
import { render } from "mocks";

describe("toelinesActionButtons", (): void => {
  const memoryRouter = {
    initialEntries: ["/unittesting/surface/lines"],
  };
  const mokedToeLines: IToeLinesData[] = [
    {
      attackedAt: "2021-02-20T05:00:00+00:00",
      attackedBy: "test2@test.com",
      attackedLines: 4,
      bePresent: true,
      bePresentUntil: "",
      comments: "comment 1",
      coverage: 0.1,
      daysToAttack: 4,
      extension: "config",
      filename: "test/test#.config",
      firstAttackAt: "2020-02-19T15:41:04+00:00",
      hasVulnerabilities: true,
      lastAuthor: "user@gmail.com",
      lastCommit: "f9e4beba70c4f34d6117c3b0c23ebe6b2bff66c2",
      loc: 8,
      modifiedDate: "2020-11-15T15:41:04+00:00",
      root: {
        id: "63298a73-9dff-46cf-b42d-9b2f01a56690",
        nickname: "universe",
      },
      rootId: "63298a73-9dff-46cf-b42d-9b2f01a56690",
      rootNickname: "universe",
      seenAt: "2020-02-01T15:41:04+00:00",
      sortsPriorityFactor: 70,
      sortsSuggestions: null,
    },
  ];

  it("should not display the edition button without permissions", (): void => {
    expect.hasAssertions();

    render(
      <authzPermissionsContext.Provider value={new PureAbility([])}>
        <Routes>
          <Route
            element={
              <ActionButtons
                data={[]}
                isAdding={false}
                isEditing={false}
                isInternal={true}
                isVerifying={false}
                onAdd={jest.fn()}
                onEdit={jest.fn()}
                onVerify={jest.fn()}
                selectedToeLinesData={mokedToeLines}
              />
            }
            path={"/:groupName/surface/lines"}
          />
        </Routes>
      </authzPermissionsContext.Provider>,
      {
        memoryRouter,
      },
    );

    expect(screen.queryByRole("button")).not.toBeInTheDocument();
  });

  it("should hide the edition button for the external view", (): void => {
    expect.hasAssertions();

    const mockedPermissions = new PureAbility<string>([
      {
        action:
          "integrates_api_mutations_update_toe_lines_attacked_lines_mutate",
      },
    ]);
    render(
      <authzPermissionsContext.Provider value={mockedPermissions}>
        <Routes>
          <Route
            element={
              <ActionButtons
                data={[]}
                isAdding={false}
                isEditing={false}
                isInternal={false}
                isVerifying={false}
                onAdd={jest.fn()}
                onEdit={jest.fn()}
                onVerify={jest.fn()}
                selectedToeLinesData={mokedToeLines}
              />
            }
            path={"/:groupName/surface/lines"}
          />
        </Routes>
      </authzPermissionsContext.Provider>,
      {
        memoryRouter,
      },
    );

    expect(screen.queryByRole("button")).not.toBeInTheDocument();
  });

  it("should display the edition button", (): void => {
    expect.hasAssertions();

    const mockedPermissions = new PureAbility<string>([
      {
        action:
          "integrates_api_mutations_update_toe_lines_attacked_lines_mutate",
      },
    ]);

    render(
      <authzPermissionsContext.Provider value={mockedPermissions}>
        <Routes>
          <Route
            element={
              <ActionButtons
                data={[]}
                isAdding={false}
                isEditing={false}
                isInternal={true}
                isVerifying={false}
                onAdd={jest.fn()}
                onEdit={jest.fn()}
                onVerify={jest.fn()}
                selectedToeLinesData={mokedToeLines}
              />
            }
            path={"/:groupName/surface/lines"}
          />
        </Routes>
      </authzPermissionsContext.Provider>,
      {
        memoryRouter,
      },
    );

    expect(screen.queryByText("buttons.edit")).toBeInTheDocument();
  });

  it("should display the addition button", (): void => {
    expect.hasAssertions();

    const mockedPermissions = new PureAbility<string>([
      { action: "integrates_api_mutations_add_toe_lines_mutate" },
    ]);
    render(
      <authzPermissionsContext.Provider value={mockedPermissions}>
        <Routes>
          <Route
            element={
              <ActionButtons
                data={[]}
                isAdding={false}
                isEditing={false}
                isInternal={true}
                isVerifying={false}
                onAdd={jest.fn()}
                onEdit={jest.fn()}
                onVerify={jest.fn()}
                selectedToeLinesData={mokedToeLines}
              />
            }
            path={"/:groupName/surface/lines"}
          />
        </Routes>
      </authzPermissionsContext.Provider>,
      {
        memoryRouter,
      },
    );

    expect(screen.queryByRole("button")).toBeInTheDocument();
    expect(
      screen.queryByText("group.toe.lines.actionButtons.addButton.text"),
    ).toBeInTheDocument();
  });

  it("should display the verify button", (): void => {
    expect.hasAssertions();

    const mockedPermissions = new PureAbility<string>([
      {
        action:
          "integrates_api_mutations_update_toe_lines_attacked_lines_mutate",
      },
    ]);

    render(
      <authzPermissionsContext.Provider value={mockedPermissions}>
        <Routes>
          <Route
            element={
              <ActionButtons
                data={[]}
                isAdding={false}
                isEditing={false}
                isInternal={true}
                isVerifying={false}
                onAdd={jest.fn()}
                onEdit={jest.fn()}
                onVerify={jest.fn()}
                selectedToeLinesData={mokedToeLines}
              />
            }
            path={"/:groupName/surface/lines"}
          />
        </Routes>
      </authzPermissionsContext.Provider>,
      {
        memoryRouter,
      },
    );

    expect(screen.queryByText("buttons.verify")).toBeInTheDocument();
  });
});
