import { useApolloClient, useMutation, useQuery } from "@apollo/client";
import type { FetchResult } from "@apollo/client";
import { useAbility } from "@casl/react";
import { AppliedFilters, useModal } from "@fluidattacks/design";
import _ from "lodash";
import { useCallback, useMemo, useState } from "react";
import * as React from "react";
import { useTranslation } from "react-i18next";
import { useParams } from "react-router-dom";

import { ActionButtons } from "./action-buttons";
import { AdditionModal } from "./addition-modal";
import { EditionModal } from "./edition-modal";
import { LocModal } from "./loc-modal";
import { GET_TOE_LINES, VERIFY_TOE_LINES } from "./queries";
import { SortsSuggestionsModal } from "./sorts-suggestions-modal";
import type {
  IGroupToeLinesProps,
  ISortsSuggestionAttr,
  IToeLinesAttr,
  IToeLinesConnection,
  IToeLinesData,
  IToeLinesEdge,
  IVerifyToeLinesResultAttr,
} from "./types";
import {
  formatToeLines,
  getUpdatedEdges,
  handleVerifyTOELinesError,
} from "./utils";

import { Table } from "components/table";
import { PROCESSING_ROWS_LIMIT } from "components/table/export-multiple-csv/export-options";
import { authzPermissionsContext } from "context/authz/config";
import { useToELinesFilters } from "features/toes-lines/filters";
import { getColumns } from "features/toes-lines/table/utils";
import type {
  GetToeLinesQueryVariables,
  VerifyToeLinesMutation,
} from "gql/graphql";
import { useTable } from "hooks";
import { useAudit } from "hooks/use-audit";
import { getErrors } from "utils/helpers";
import { Logger } from "utils/logger";
import { msgSuccess } from "utils/notifications";

const GroupToeLines: React.FC<IGroupToeLinesProps> = ({
  isInternal,
}): JSX.Element => {
  const { t } = useTranslation();
  const client = useApolloClient();
  const permissions = useAbility(authzPermissionsContext);
  const canUpdateAttackedLines = permissions.can(
    "integrates_api_mutations_update_toe_lines_attacked_lines_mutate",
  );
  const canGetAttackedAt = permissions.can(
    "integrates_api_resolvers_toe_lines_attacked_at_resolve",
  );
  const canGetAttackedBy = permissions.can(
    "integrates_api_resolvers_toe_lines_attacked_by_resolve",
  );
  const canGetAttackedLines = permissions.can(
    "integrates_api_resolvers_toe_lines_attacked_lines_resolve",
  );
  const canGetBePresentUntil = permissions.can(
    "integrates_api_resolvers_toe_lines_be_present_until_resolve",
  );
  const canGetComments = permissions.can(
    "integrates_api_resolvers_toe_lines_comments_resolve",
  );
  const canGetFirstAttackAt = permissions.can(
    "integrates_api_resolvers_toe_lines_first_attack_at_resolve",
  );
  const canSeeCoverage = permissions.can("see_toe_lines_coverage");
  const canSeeDaysToAttack = permissions.can("see_toe_lines_days_to_attack");
  const { groupName } = useParams() as { groupName: string };
  const [isVerifying, setIsVerifying] = useState(false);
  const additionModalProps = useModal("addition-modal");
  const editionModalProps = useModal("edition-modal");
  const sortsSuggestionModalProps = useModal("sorts-suggestion-modal");
  const [toeLinesBackFilters, setToeLinesBackFilters] = useState<
    Partial<GetToeLinesQueryVariables>
  >({
    bePresent: true,
    hasVulnerabilities: true,
  });
  const [selectedToeLinesData, setSelectedToeLinesData] = useState<
    IToeLinesData[]
  >([]);
  const [isLocModalOpen, setIsLocModalOpen] = useState(false);
  const [
    selectedToeLinesSortsSuggestions,
    setSelectedToeLinesSortsSuggestions,
  ] = useState<ISortsSuggestionAttr[]>();

  const { Filters, options, removeFilter } = useToELinesFilters({
    groupName,
    setBackFilters: setToeLinesBackFilters,
  });
  const tableRef = useTable(
    "tblToeLines",
    {
      attackedBy: false,
      bePresent: false,
      bePresentUntil: false,
      daysToAttack: false,
      filename: false,
      firstAttackAt: false,
      lastAuthor: false,
      seenAt: false,
      sortsSuggestions: false,
    },
    [
      "rootNickname",
      "loc",
      "hasVulnerabilities",
      "modifiedDate",
      "lastCommit",
      "sortsPriorityFactor",
    ],
    { left: ["rootNickname"] },
  );
  // GraphQL operations
  const [handleVerifyToeLines] = useMutation(VERIFY_TOE_LINES, {
    onError: handleVerifyTOELinesError,
  });
  const TOE_LINES_THRESHOLD = 10;
  const toeLinesPermissions = useMemo((): GetToeLinesQueryVariables => {
    return {
      canGetAttackedAt,
      canGetAttackedBy,
      canGetAttackedLines,
      canGetBePresentUntil,
      canGetComments,
      canGetFirstAttackAt,
      groupName,
    };
  }, [
    canGetAttackedAt,
    canGetAttackedBy,
    canGetAttackedLines,
    canGetBePresentUntil,
    canGetComments,
    canGetFirstAttackAt,
    groupName,
  ]);
  const { addAuditEvent } = useAudit();
  const { data, fetchMore, refetch } = useQuery<{
    group: {
      __typename: "Group";
      name: string;
      toeLinesConnection: IToeLinesConnection;
    };
  }>(GET_TOE_LINES, {
    fetchPolicy: "network-only",
    nextFetchPolicy: "cache-first",
    onCompleted: (): void => {
      addAuditEvent("Group.ToeLines", groupName);
    },
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error): void => {
        Logger.error("Couldn't load group toe lines", error);
      });
    },
    variables: {
      ...toeLinesBackFilters,
      ...toeLinesPermissions,
      first: 150,
    },
  });
  const size = data?.group.toeLinesConnection.total;
  const toeLines = useMemo((): IToeLinesData[] => {
    const toeLinesEdges =
      data === undefined ? [] : data.group.toeLinesConnection.edges;

    return formatToeLines(toeLinesEdges);
  }, [data]);
  const handleUpdateAttackedLines = async (
    rootId: string,
    filename: string,
    comments: string,
    attackedLines: number,
  ): Promise<void> => {
    const result = await handleVerifyToeLines({
      variables: {
        ...toeLinesPermissions,
        attackedLines,
        comments,
        filename,
        groupName,
        rootId,
        shouldGetNewToeLines: true,
      },
    });
    if (
      !_.isNil(result.data) &&
      result.data.updateToeLinesAttackedLines.success
    ) {
      msgSuccess(
        t("group.toe.lines.alerts.verifyToeLines.success"),
        t("groupAlerts.updatedTitle"),
      );
      const updatedToeLines = result.data.updateToeLinesAttackedLines
        .toeLines as IToeLinesAttr;

      if (!_.isUndefined(updatedToeLines)) {
        if (data?.group) {
          client.writeQuery({
            data: {
              ...data,
              group: {
                ...data.group,
                toeLinesConnection: {
                  ...data.group.toeLinesConnection,
                  edges: data.group.toeLinesConnection.edges.map(
                    (value): IToeLinesEdge =>
                      value.node.root.id === rootId &&
                      value.node.filename === filename
                        ? {
                            node: updatedToeLines,
                          }
                        : { node: value.node },
                  ),
                  total: data.group.toeLinesConnection.total as number | null,
                },
              },
            },
            query: GET_TOE_LINES,
            variables: {
              ...toeLinesBackFilters,
              ...toeLinesPermissions,
              first: 150,
            },
          });
        }
      }
    }
  };
  const handleNextPage = useCallback(async (): Promise<void> => {
    const pageInfo =
      data === undefined
        ? { endCursor: [], hasNextPage: false }
        : {
            endCursor: JSON.parse(
              data.group.toeLinesConnection.pageInfo.endCursor,
            ) as unknown[],
            hasNextPage: data.group.toeLinesConnection.pageInfo.hasNextPage,
          };

    if (pageInfo.hasNextPage) {
      await fetchMore({ variables: { after: pageInfo.endCursor.map(String) } });
    }
  }, [data, fetchMore]);
  const fetchReportData = useCallback(async (): Promise<void> => {
    const fetchMoreData = async (
      endCursor: string[],
      count: number,
    ): Promise<void> => {
      const result = await fetchMore({
        variables: { after: endCursor.map(String) },
      });
      const newEndCursor = JSON.parse(
        result.data.group.toeLinesConnection.pageInfo.endCursor,
      );
      const { hasNextPage } = result.data.group.toeLinesConnection.pageInfo;

      if (hasNextPage && count < PROCESSING_ROWS_LIMIT) {
        await fetchMoreData(
          newEndCursor,
          count + result.data.group.toeLinesConnection.edges.length,
        );
      }
    };
    const pageInfo =
      data === undefined
        ? { count: 0, endCursor: [], hasNextPage: false }
        : {
            count: data.group.toeLinesConnection.edges.length,
            endCursor: JSON.parse(
              data.group.toeLinesConnection.pageInfo.endCursor,
            ) as string[],
            hasNextPage: data.group.toeLinesConnection.pageInfo.hasNextPage,
          };

    if (pageInfo.hasNextPage) {
      await fetchMoreData(pageInfo.endCursor, pageInfo.count);
    }
  }, [data, fetchMore]);

  const toggleAdd = useCallback((): void => {
    additionModalProps.open();
  }, [additionModalProps]);
  const toggleEdit = useCallback((): void => {
    editionModalProps.open();
  }, [editionModalProps]);
  const toggleLocModal = useCallback((): void => {
    setIsLocModalOpen(!isLocModalOpen);
  }, [isLocModalOpen]);
  const updateQuery = useCallback(
    (
      updatedEdges: IToeLinesEdge[],
      currentData:
        | {
            group: {
              __typename: "Group";
              name: string;
              toeLinesConnection: IToeLinesConnection;
            };
          }
        | undefined,
    ): void => {
      if (currentData) {
        client.writeQuery({
          data: {
            ...currentData,
            group: {
              ...currentData.group,
              toeLinesConnection: {
                ...currentData.group.toeLinesConnection,
                edges: updatedEdges,
                total: currentData.group.toeLinesConnection.total ?? null,
              },
            },
          },
          query: GET_TOE_LINES,
          variables: {
            ...toeLinesBackFilters,
            ...toeLinesPermissions,
            first: 150,
          },
        });
      }
    },
    [client, toeLinesBackFilters, toeLinesPermissions],
  );
  const handleOnVerifyCompleted = useCallback(
    async (
      results: FetchResult<IVerifyToeLinesResultAttr>[],
    ): Promise<void> => {
      const currentEdges = data?.group.toeLinesConnection.edges ?? [];
      const updatedEdges = results.reduce(
        (
          previousValue: IToeLinesEdge[],
          currentValue: FetchResult<IVerifyToeLinesResultAttr>,
        ): IToeLinesEdge[] => {
          const currentUpdated = selectedToeLinesData.map(
            (toeLine): IToeLinesEdge[] | undefined =>
              getUpdatedEdges(previousValue, currentValue, toeLine),
          );
          const filteredUpdated = currentUpdated.filter(
            (value): boolean => value !== undefined,
          );

          return _.last(filteredUpdated) ?? previousValue;
        },
        currentEdges,
      );
      updateQuery(
        updatedEdges.length === currentEdges.length
          ? updatedEdges
          : currentEdges,
        data,
      );
      if (
        !_.isNil(results[0].data) &&
        results[0].data.updateToeLinesAttackedLines.success
      ) {
        msgSuccess(
          t("group.toe.lines.alerts.verifyToeLines.success"),
          t("groupAlerts.updatedTitle"),
        );
        if (selectedToeLinesData.length >= TOE_LINES_THRESHOLD) {
          await refetch();
        }
        setSelectedToeLinesData([]);
      }
    },
    [data, refetch, selectedToeLinesData, t, updateQuery],
  );
  const handleVerify = useCallback((): void => {
    setIsVerifying(true);
    void Promise.all(
      selectedToeLinesData.map(
        async (toeInputData): Promise<FetchResult<VerifyToeLinesMutation>> =>
          handleVerifyToeLines({
            variables: {
              ...toeLinesPermissions,
              attackedLines: toeInputData.loc,
              comments: toeInputData.comments,
              filename: toeInputData.filename,
              groupName,
              rootId: toeInputData.rootId,
              shouldGetNewToeLines:
                selectedToeLinesData.length < TOE_LINES_THRESHOLD,
            },
          }),
      ),
    ).then((results: FetchResult<VerifyToeLinesMutation>[]): void => {
      const errors = getErrors<VerifyToeLinesMutation>(results);
      if (!_.isEmpty(results) && _.isEmpty(errors)) {
        void handleOnVerifyCompleted(
          results as FetchResult<IVerifyToeLinesResultAttr>[],
        );
      } else {
        refetch().catch((): void => {
          Logger.error("An error occurred formatting");
        });
      }
    });
    setIsVerifying(false);
  }, [
    selectedToeLinesData,
    handleVerifyToeLines,
    toeLinesPermissions,
    groupName,
    handleOnVerifyCompleted,
    refetch,
  ]);
  const tableColumns = getColumns({
    canSeeCoverage,
    canSeeDaysToAttack,
    canUpdateAttackedLines,
    handleUpdateAttackedLines,
    isInternal,
    permissions,
    setIsSortsSuggestionsModalOpen: sortsSuggestionModalProps.setIsOpen,
    setSelectedToeLinesData,
    setSelectedToeLinesSortsSuggestions,
    toggleLocModal,
  });

  return (
    <React.StrictMode>
      <Table
        columns={tableColumns}
        data={toeLines}
        extraButtons={
          <ActionButtons
            data={toeLines}
            fetchReportData={fetchReportData}
            isAdding={additionModalProps.isOpen}
            isEditing={editionModalProps.isOpen}
            isInternal={isInternal}
            isVerifying={isVerifying}
            onAdd={toggleAdd}
            onEdit={toggleEdit}
            onVerify={handleVerify}
            selectedToeLinesData={selectedToeLinesData}
            size={size}
          />
        }
        filters={<Filters />}
        filtersApplied={
          <AppliedFilters onClose={removeFilter} options={options} />
        }
        loadingData={_.isEmpty(toeLines) && size !== 0}
        onNextPage={handleNextPage}
        options={{ columnToggle: true, size }}
        rowSelectionSetter={
          isInternal && canUpdateAttackedLines
            ? setSelectedToeLinesData
            : undefined
        }
        rowSelectionState={selectedToeLinesData}
        tableRef={tableRef}
      />
      <AdditionModal
        groupName={groupName}
        modalRef={additionModalProps}
        refetchData={refetch}
      />
      <EditionModal
        groupName={groupName}
        modalRef={editionModalProps}
        refetchData={refetch}
        selectedToeLinesData={selectedToeLinesData}
        setSelectedToeLinesData={setSelectedToeLinesData}
      />
      {isLocModalOpen ? (
        <LocModal
          groupName={groupName}
          handleCloseModal={toggleLocModal}
          refetchData={refetch}
          selectedToeLineData={selectedToeLinesData[0]}
          setSelectedToeLinesData={setSelectedToeLinesData}
        />
      ) : undefined}
      <SortsSuggestionsModal
        modalRef={sortsSuggestionModalProps}
        selectedSortsSuggestions={selectedToeLinesSortsSuggestions}
      />
    </React.StrictMode>
  );
};
export { GroupToeLines };
