import type { FetchResult } from "@apollo/client";
import { useMutation } from "@apollo/client";
import {
  Form,
  InnerForm,
  InputNumber,
  Modal,
  TextArea,
  useModal,
} from "@fluidattacks/design";
import { isEmpty, isNil } from "lodash";
import { useCallback } from "react";
import * as React from "react";
import { Fragment } from "react";
import { useTranslation } from "react-i18next";

import { UPDATE_TOE_LINES_LOC } from "./queries";
import type {
  IFormValues,
  ILocModalProps,
  IUpdateToeLinesLocResultAttr,
} from "./types";
import { validationSchema } from "./validations";

import { Logger } from "utils/logger";
import { msgError, msgSuccess } from "utils/notifications";

const LocModal: React.FC<ILocModalProps> = ({
  groupName,
  selectedToeLineData,
  handleCloseModal,
  refetchData,
  setSelectedToeLinesData,
}): JSX.Element => {
  const { t } = useTranslation();
  const modalProps = useModal("loc-modal");

  // GraphQL operations
  const [handleUpdateToeLinesLoc] = useMutation(UPDATE_TOE_LINES_LOC, {
    onError: (errors): void => {
      errors.graphQLErrors.forEach((error): void => {
        switch (error.message) {
          case "Exception - Value must be between 1 (inclusive) and 1000000 (inclusive)":
            msgError(t("group.toe.lines.editModal.alerts.invalidLocBetween"));
            break;
          case "Exception - The toe lines has been updated by another operation":
            msgError(t("group.toe.lines.editModal.alerts.alreadyUpdate"));
            break;
          default:
            msgError(t("groupAlerts.errorTextsad"));
            Logger.warning(
              "An error occurred updating the toe lines loc",
              error,
            );
        }
      });
    },
  });

  const handleOnCompleted = useCallback(
    (result: FetchResult<IUpdateToeLinesLocResultAttr>): void => {
      if (!isNil(result.data) && result.data.updateToeLinesLoc.success) {
        msgSuccess(
          t("group.toe.lines.editModal.alerts.success"),
          t("groupAlerts.updatedTitle"),
        );
        refetchData();
        setSelectedToeLinesData([]);
        handleCloseModal();
      }
    },
    [handleCloseModal, refetchData, setSelectedToeLinesData, t],
  );

  const handleSubmit = useCallback(
    async (values: IFormValues): Promise<void> => {
      const results = await handleUpdateToeLinesLoc({
        variables: {
          comments: values.comments,
          filename: selectedToeLineData.filename,
          groupName,
          loc: parseInt(values.loc.toString(), 10) || 0,
          rootId: selectedToeLineData.rootId,
        },
      });
      const errors = "errors" in results ? results.errors : undefined;

      if (!isEmpty(results) && isEmpty(errors)) {
        handleOnCompleted(results);
      } else {
        refetchData();
      }
    },
    [
      groupName,
      handleOnCompleted,
      handleUpdateToeLinesLoc,
      refetchData,
      selectedToeLineData,
    ],
  );

  return (
    <React.StrictMode>
      <Modal
        id={"update-toe-lines-loc"}
        modalRef={{ ...modalProps, close: handleCloseModal, isOpen: true }}
        size={"sm"}
        title={t("group.toe.lines.editModal.title.loc")}
      >
        <Form
          cancelButton={{ onClick: handleCloseModal }}
          defaultValues={{
            comments: "",
            loc: selectedToeLineData.loc,
          }}
          onSubmit={handleSubmit}
          yupSchema={validationSchema}
        >
          <InnerForm>
            {({
              register,
              watch,
              setValue,
              formState,
              getFieldState,
            }): JSX.Element => (
              <Fragment>
                <InputNumber
                  error={formState.errors.loc?.message?.toString()}
                  label={t("group.toe.lines.editModal.fields.loc")}
                  max={1000000}
                  min={0}
                  name={"loc"}
                  register={register}
                  setValue={setValue}
                  watch={watch}
                  weight={"bold"}
                />
                <TextArea
                  error={getFieldState("comments").error?.message?.toString()}
                  isTouched={getFieldState("comments").isTouched}
                  isValid={!getFieldState("comments").invalid}
                  label={t("group.toe.lines.editModal.fields.comments")}
                  maxLength={200}
                  name={"comments"}
                  register={register}
                  watch={watch}
                />
              </Fragment>
            )}
          </InnerForm>
        </Form>
      </Modal>
    </React.StrictMode>
  );
};

export { LocModal };
