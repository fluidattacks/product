import { graphql } from "gql";

const UPDATE_TOE_LINES_LOC = graphql(`
  mutation UpdateToeLinesLoc(
    $comments: String!
    $filename: String!
    $groupName: String!
    $loc: Int!
    $rootId: String!
  ) {
    updateToeLinesLoc(
      comments: $comments
      filename: $filename
      groupName: $groupName
      loc: $loc
      rootId: $rootId
    ) {
      success
    }
  }
`);

export { UPDATE_TOE_LINES_LOC };
