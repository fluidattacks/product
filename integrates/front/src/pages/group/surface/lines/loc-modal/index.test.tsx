import { screen, waitFor } from "@testing-library/react";
import { userEvent } from "@testing-library/user-event";
import { GraphQLError } from "graphql";
import { HttpResponse, type StrictResponse, graphql } from "msw";
import { Route, Routes } from "react-router-dom";
import {
  type AnyObject,
  type InferType,
  type Schema,
  ValidationError,
} from "yup";

import { UPDATE_TOE_LINES_LOC } from "./queries";
import { locRange, validationSchema } from "./validations";

import { LocModal } from ".";
import type { IToeLinesData } from "../types";
import type { UpdateToeLinesLocMutation as UpdateToeLinesLoc } from "gql/graphql";
import { render } from "mocks";
import { LINK } from "mocks/constants";
import type { IErrorMessage, IMessage } from "mocks/types";
import { msgError, msgSuccess } from "utils/notifications";
import { translate } from "utils/translations/translate";

jest.mock("utils/notifications", (): Record<string, unknown> => {
  const mockedNotifications: Record<string, () => Record<string, unknown>> =
    jest.requireActual("utils/notifications");
  jest.spyOn(mockedNotifications, "msgError").mockImplementation();
  jest.spyOn(mockedNotifications, "msgSuccess").mockImplementation();

  return mockedNotifications;
});

describe("handle toe lines loc modal", (): void => {
  const testValidation = (obj: AnyObject): InferType<Schema> => {
    return validationSchema.validateSync(obj);
  };
  const memoryRouter = {
    initialEntries: ["/unittesting/surface/lines"],
  };

  it("should handle toe lines loc edition", async (): Promise<void> => {
    expect.hasAssertions();

    jest.clearAllMocks();

    const handleRefetchData: jest.Mock = jest.fn();
    const handleCloseModal: jest.Mock = jest.fn();
    const mocksMutation = [
      graphql
        .link(LINK)
        .mutation(
          UPDATE_TOE_LINES_LOC,
          ({
            variables,
          }): StrictResponse<IErrorMessage | { data: UpdateToeLinesLoc }> => {
            const { loc, comments, filename, groupName, rootId } = variables;

            if (
              loc === 5 &&
              comments === "This is a test of updating toe lines loc" &&
              filename === "test/test#.config" &&
              groupName === "groupname" &&
              rootId === "63298a73-9dff-46cf-b42d-9b2f01a56690"
            ) {
              return HttpResponse.json({
                data: {
                  updateToeLinesLoc: { success: true },
                },
              });
            }

            return HttpResponse.json({
              errors: [new GraphQLError("Error updating toe lines")],
            });
          },
        ),
    ];
    const mokedToeLines: IToeLinesData = {
      attackedAt: "2021-02-20T05:00:00+00:00",
      attackedBy: "test2@test.com",
      attackedLines: 4,
      bePresent: true,
      bePresentUntil: "",
      comments: "comment 1",
      coverage: 0.1,
      daysToAttack: 4,
      extension: "config",
      filename: "test/test#.config",
      firstAttackAt: "2020-02-19T15:41:04+00:00",
      hasVulnerabilities: true,
      lastAuthor: "user@gmail.com",
      lastCommit: "f9e4beba70c4f34d6117c3b0c23ebe6b2bff66c2",
      loc: 0,
      modifiedDate: "2020-11-15T15:41:04+00:00",
      root: {
        id: "63298a73-9dff-46cf-b42d-9b2f01a56690",
        nickname: "universe",
      },
      rootId: "63298a73-9dff-46cf-b42d-9b2f01a56690",
      rootNickname: "universe",
      seenAt: "2020-02-01T15:41:04+00:00",
      sortsPriorityFactor: 70,
      sortsSuggestions: null,
    };
    render(
      <Routes>
        <Route
          element={
            <LocModal
              groupName={"groupname"}
              handleCloseModal={handleCloseModal}
              refetchData={handleRefetchData}
              selectedToeLineData={mokedToeLines}
              setSelectedToeLinesData={jest.fn()}
            />
          }
          path={"/:groupName/surface/lines"}
        />
      </Routes>,
      { memoryRouter, mocks: mocksMutation },
    );

    await userEvent.clear(screen.getByRole("spinbutton"));
    await userEvent.type(screen.getByRole("spinbutton"), "5");
    await userEvent.type(
      screen.getByRole("textbox"),
      "This is a test of updating toe lines loc",
    );

    await userEvent.click(screen.getByText("Confirm"));

    await waitFor((): void => {
      expect(handleRefetchData).toHaveBeenCalledTimes(1);
    });

    expect(handleCloseModal).toHaveBeenCalledTimes(1);
    expect(msgSuccess).toHaveBeenCalledWith(
      "group.toe.lines.editModal.alerts.success",
      "groupAlerts.updatedTitle",
    );
  });

  it("should handle error toe lines loc edition", async (): Promise<void> => {
    expect.hasAssertions();

    jest.clearAllMocks();

    const handleRefetchData: jest.Mock = jest.fn();
    const handleCloseModal: jest.Mock = jest.fn();
    const mocksQuery = [
      graphql
        .link(LINK)
        .mutation(
          UPDATE_TOE_LINES_LOC,
          ({
            variables,
          }): StrictResponse<IErrorMessage | { data: UpdateToeLinesLoc }> => {
            const { loc, comments, filename, groupName, rootId } = variables;

            if (
              loc === 1 &&
              comments === "This is a test error in updating toe lines" &&
              filename === "test/test#.config" &&
              groupName === "groupname" &&
              rootId === "63298a73-9dff-46cf-b42d-9b2f01a56690"
            ) {
              return HttpResponse.json({
                errors: [
                  new GraphQLError(
                    "Exception - Value must be between 1 (inclusive) and " +
                      "1000000 (inclusive)",
                  ),
                ],
              });
            }

            return HttpResponse.json({
              data: {
                updateToeLinesLoc: { success: true },
              },
            });
          },
        ),
    ];

    const mokedToeLines: IToeLinesData = {
      attackedAt: "2021-02-20T05:00:00+00:00",
      attackedBy: "test2@test.com",
      attackedLines: 4,
      bePresent: true,
      bePresentUntil: "",
      comments: "comment 1",
      coverage: 0.1,
      daysToAttack: 4,
      extension: "config",
      filename: "test/test#.config",
      firstAttackAt: "2020-02-19T15:41:04+00:00",
      hasVulnerabilities: true,
      lastAuthor: "user@gmail.com",
      lastCommit: "f9e4beba70c4f34d6117c3b0c23ebe6b2bff66c2",
      loc: 8,
      modifiedDate: "2020-11-15T15:41:04+00:00",
      root: {
        id: "63298a73-9dff-46cf-b42d-9b2f01a56690",
        nickname: "universe",
      },
      rootId: "63298a73-9dff-46cf-b42d-9b2f01a56690",
      rootNickname: "universe",
      seenAt: "2020-02-01T15:41:04+00:00",
      sortsPriorityFactor: 70,
      sortsSuggestions: null,
    };
    render(
      <Routes>
        <Route
          element={
            <LocModal
              groupName={"groupname"}
              handleCloseModal={handleCloseModal}
              refetchData={handleRefetchData}
              selectedToeLineData={mokedToeLines}
              setSelectedToeLinesData={jest.fn()}
            />
          }
          path={"/:groupName/surface/lines"}
        />
      </Routes>,
      { memoryRouter, mocks: mocksQuery },
    );

    await userEvent.clear(screen.getByRole("spinbutton"));
    await userEvent.type(screen.getByRole("spinbutton"), "1");

    await userEvent.type(
      screen.getByRole("textbox"),
      "This is a test error in updating toe lines",
    );

    await userEvent.click(screen.getByText("Confirm"));

    await waitFor((): void => {
      expect(msgError).toHaveBeenCalledWith(
        "group.toe.lines.editModal.alerts.invalidLocBetween",
      );
    });
  });

  it("should handle another error in toe lines loc edition", async (): Promise<void> => {
    expect.hasAssertions();

    jest.clearAllMocks();

    const handleRefetchData: jest.Mock = jest.fn();
    const handleCloseModal: jest.Mock = jest.fn();
    const mocksQuery = [
      graphql
        .link(LINK)
        .mutation(
          UPDATE_TOE_LINES_LOC,
          ({
            variables,
          }): StrictResponse<
            Record<"errors", IMessage[]> | { data: UpdateToeLinesLoc }
          > => {
            const { loc, comments, filename, groupName, rootId } = variables;

            if (
              loc === 6 &&
              comments === "This is a test error in updating toe lines loc" &&
              filename === "test/test#.config" &&
              groupName === "groupname" &&
              rootId === "63298a73-9dff-46cf-b42d-9b2f01a56690"
            ) {
              return HttpResponse.json({
                errors: [
                  new GraphQLError(
                    "Exception - The toe lines has been updated by another operation",
                  ),
                  new GraphQLError(
                    "An error occurred updating the toe lines attacked lines error",
                  ),
                ],
              });
            }

            return HttpResponse.json({
              data: {
                updateToeLinesLoc: { success: true },
              },
            });
          },
        ),
    ];

    const mokedToeLines: IToeLinesData = {
      attackedAt: "2021-02-20T05:00:00+00:00",
      attackedBy: "test2@test.com",
      attackedLines: 0,
      bePresent: true,
      bePresentUntil: "",
      comments: "comment 1",
      coverage: 0.1,
      daysToAttack: 4,
      extension: "config",
      filename: "test/test#.config",
      firstAttackAt: "2020-02-19T15:41:04+00:00",
      hasVulnerabilities: true,
      lastAuthor: "user@gmail.com",
      lastCommit: "f9e4beba70c4f34d6117c3b0c23ebe6b2bff66c2",
      loc: 8,
      modifiedDate: "2020-11-15T15:41:04+00:00",
      root: {
        id: "63298a73-9dff-46cf-b42d-9b2f01a56690",
        nickname: "universe",
      },
      rootId: "63298a73-9dff-46cf-b42d-9b2f01a56690",
      rootNickname: "universe",
      seenAt: "2020-02-01T15:41:04+00:00",
      sortsPriorityFactor: 70,
      sortsSuggestions: null,
    };
    render(
      <Routes>
        <Route
          element={
            <LocModal
              groupName={"groupname"}
              handleCloseModal={handleCloseModal}
              refetchData={handleRefetchData}
              selectedToeLineData={mokedToeLines}
              setSelectedToeLinesData={jest.fn()}
            />
          }
          path={"/:groupName/surface/lines"}
        />
      </Routes>,
      { memoryRouter, mocks: mocksQuery },
    );

    await userEvent.clear(screen.getByRole("spinbutton"));
    await userEvent.type(screen.getByRole("spinbutton"), "6");

    await userEvent.type(
      screen.getByRole("textbox"),
      "This is a test error in updating toe lines loc",
    );

    await userEvent.click(screen.getByText("Confirm"));

    await waitFor((): void => {
      expect(msgError).toHaveBeenCalledWith(
        "group.toe.lines.editModal.alerts.alreadyUpdate",
      );
    });

    expect(msgError).toHaveBeenLastCalledWith("groupAlerts.errorTextsad");
  });

  it("should display loc field errors of validation schema", (): void => {
    expect.hasAssertions();

    const testRequiredField = (): void => {
      testValidation({});
    };
    const testMinLoc = (): void => {
      testValidation({ loc: 0 });
    };
    const testMaxLoc = (): void => {
      testValidation({ loc: 1000001 });
    };
    const testIntegerLoc = (): void => {
      testValidation({ loc: 1.3 });
    };

    expect(testRequiredField).toThrow(
      new ValidationError(translate.t("validations.required")),
    );
    expect(testMinLoc).toThrow(
      new ValidationError(translate.t("validations.between", locRange)),
    );
    expect(testMaxLoc).toThrow(
      new ValidationError(translate.t("validations.between", locRange)),
    );
    expect(testIntegerLoc).toThrow(
      new ValidationError(translate.t("validations.integer")),
    );
  });

  it("should display comment editor validation errors", (): void => {
    expect.hasAssertions();

    const testInvalidTextBeginning = (): void => {
      testValidation({ comments: "=", loc: 1 });
    };
    const testInvalidTextField = (): void => {
      testValidation({
        comments: "Test ^ invalid character",
        loc: 1,
      });
    };

    expect(testInvalidTextBeginning).toThrow(
      new ValidationError(
        translate.t("validations.invalidTextBeginning", {
          chars: "'='",
        }),
      ),
    );
    expect(testInvalidTextField).toThrow(
      new ValidationError(
        translate.t("validations.invalidTextField", {
          chars: "'^'",
        }),
      ),
    );
  });
});
