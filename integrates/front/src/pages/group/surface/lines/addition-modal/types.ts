import type { IUseModal } from "@fluidattacks/design";

interface IFormValues {
  filename: string;
  lastAuthor: string;
  lastCommit: string;
  loc: number;
  modifiedDate: string | undefined;
  rootId: string;
}

interface IAdditionModalFormProps {
  roots: IGitRootAttr[];
  handleCloseModal: () => void;
}

interface IAddToeInputResultAttr {
  addToeLines: {
    success: boolean;
  };
}

interface IAdditionModalProps {
  groupName: string;
  modalRef: IUseModal;
  refetchData: () => void;
}

interface IGitRootAttr {
  __typename: "GitRoot";
  id: string;
  nickname: string;
  state: "ACTIVE" | "INACTIVE";
}

export type {
  IFormValues,
  IAdditionModalProps,
  IAdditionModalFormProps,
  IAddToeInputResultAttr,
  IGitRootAttr,
};
