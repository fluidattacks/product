import { graphql } from "gql";

const ADD_TOE_LINES = graphql(`
  mutation AddToeLines(
    $filename: String!
    $groupName: String!
    $lastAuthor: String!
    $lastCommit: String!
    $loc: Int!
    $modifiedDate: DateTime!
    $rootId: String!
  ) {
    addToeLines(
      filename: $filename
      groupName: $groupName
      lastAuthor: $lastAuthor
      lastCommit: $lastCommit
      loc: $loc
      modifiedDate: $modifiedDate
      rootId: $rootId
    ) {
      success
    }
  }
`);

const GET_GIT_ROOTS = graphql(`
  query GetGitRootsInfo($groupName: String!) {
    group(groupName: $groupName) {
      name
      roots {
        ... on GitRoot {
          id
          __typename
          nickname
          state
        }
      }
    }
  }
`);

export { ADD_TOE_LINES, GET_GIT_ROOTS };
