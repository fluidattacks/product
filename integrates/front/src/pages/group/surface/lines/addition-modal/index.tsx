import { useMutation, useQuery } from "@apollo/client";
import dayjs, { extend } from "dayjs";
import utc from "dayjs/plugin/utc";
import isEmpty from "lodash/isEmpty";
import { StrictMode, useCallback } from "react";
import { useTranslation } from "react-i18next";

import { ADD_TOE_LINES, GET_GIT_ROOTS } from "./queries";
import type {
  IAddToeInputResultAttr,
  IAdditionModalProps,
  IFormValues,
} from "./types";
import { validationSchema } from "./validations";

import { InnerForm } from "components/form";
import { FormModal } from "components/form-modal";
import {
  Input,
  InputDateTime,
  InputNumber,
  Select,
  TextArea,
} from "components/input";
import { ResourceState } from "gql/graphql";
import { Logger } from "utils/logger";
import { msgError, msgSuccess } from "utils/notifications";

const AdditionModal = ({
  groupName,
  modalRef,
  refetchData,
}: IAdditionModalProps): JSX.Element => {
  const { t } = useTranslation();
  const { close, isOpen } = modalRef;

  // GraphQL operations
  const { data: rootsData } = useQuery(GET_GIT_ROOTS, {
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error): void => {
        Logger.error("Couldn't load roots", error);
      });
    },
    skip: !isOpen,
    variables: { groupName },
  });
  const [handleAddToeLines] = useMutation(ADD_TOE_LINES, {
    onCompleted: (data: IAddToeInputResultAttr): void => {
      if (data.addToeLines.success) {
        msgSuccess(
          t("group.toe.lines.addModal.alerts.success"),
          t("groupAlerts.titleSuccess"),
        );
        refetchData();
        close();
      }
    },
    onError: (errors): void => {
      errors.graphQLErrors.forEach((error): void => {
        switch (error.message) {
          case "Exception - Toe lines already exists":
            msgError(t("group.toe.lines.addModal.alerts.alreadyExists"));
            break;
          case "Exception - File is excluded":
            msgError(t("group.toe.lines.addModal.alerts.fileExcluded"));
            break;
          case "Exception - Invalid file name":
            msgError(t("group.toe.lines.addModal.alerts.invalidFileName"));
            break;
          default:
            msgError(t("groupAlerts.errorTextsad"));
            Logger.warning("An error occurred adding toe input", error);
        }
      });
    },
  });

  // Data management
  const roots = rootsData?.group.roots
    ? rootsData.group.roots.filter(
        (
          root,
        ): root is {
          __typename: "GitRoot";
          id: string;
          nickname: string;
          state: ResourceState;
        } =>
          root.__typename === "GitRoot" && root.state === ResourceState.Active,
      )
    : [];
  const selectRootOptions = roots.map(
    (root): { header: string; value: string } => {
      return { header: root.nickname, value: root.id };
    },
  );

  const handleSubmit = useCallback(
    (values: IFormValues): void => {
      extend(utc);
      void handleAddToeLines({
        variables: {
          filename: values.filename,
          groupName,
          lastAuthor: values.lastAuthor,
          lastCommit: values.lastCommit,
          loc: values.loc,
          modifiedDate: dayjs(values.modifiedDate ?? "")
            .utc(false)
            .format(),
          rootId: values.rootId,
        },
      });
    },
    [groupName, handleAddToeLines],
  );

  return (
    <StrictMode>
      {rootsData === undefined ? undefined : (
        <FormModal
          initialValues={{
            filename: "",
            lastAuthor: "",
            lastCommit: "",
            loc: "" as unknown as number,
            modifiedDate: undefined,
            rootId: isEmpty(roots) ? "" : roots[0].id,
          }}
          modalRef={modalRef}
          name={"addToeLines"}
          onSubmit={handleSubmit}
          size={"sm"}
          title={t("group.toe.lines.addModal.title")}
          validationSchema={validationSchema}
        >
          <InnerForm onCancel={close}>
            <Select
              items={selectRootOptions}
              label={t("group.toe.lines.addModal.fields.root")}
              name={"rootId"}
            />
            <TextArea
              label={t("group.toe.lines.addModal.fields.filename")}
              maxLength={20000}
              name={"filename"}
            />
            <InputNumber
              label={t("group.toe.lines.addModal.fields.loc")}
              min={0}
              name={"loc"}
            />
            <Input
              label={t("group.toe.lines.addModal.fields.lastAuthor")}
              name={"lastAuthor"}
            />
            <Input
              label={t("group.toe.lines.addModal.fields.lastCommit")}
              name={"lastCommit"}
            />
            <InputDateTime
              label={t("group.toe.lines.addModal.fields.modifiedDate")}
              name={"modifiedDate"}
            />
          </InnerForm>
        </FormModal>
      )}
    </StrictMode>
  );
};

export { AdditionModal };
