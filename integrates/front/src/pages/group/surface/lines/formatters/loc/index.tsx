import { Button } from "@fluidattacks/design";
import * as React from "react";

import type { IToeLinesData } from "../../types";

const handleOnClick = (
  row: IToeLinesData,
  setSelectedToeLinesData: React.Dispatch<
    React.SetStateAction<IToeLinesData[]>
  >,
  toggleLocModal: () => void,
): (() => void) => {
  return (): void => {
    setSelectedToeLinesData([row]);
    toggleLocModal();
  };
};

const formatLoc = (
  row: IToeLinesData,
  setSelectedToeLinesData: React.Dispatch<
    React.SetStateAction<IToeLinesData[]>
  >,
  toggleLocModal: () => void,
  isInternal: boolean,
): React.JSX.Element | string => {
  const onClick = handleOnClick(row, setSelectedToeLinesData, toggleLocModal);

  if (!isInternal || row.loc.valueOf() > 0) {
    return row.loc.toString();
  }

  return (
    <Button id={`locModal`} onClick={onClick} variant={"ghost"}>
      {row.loc.toString()}
    </Button>
  );
};

export { formatLoc };
