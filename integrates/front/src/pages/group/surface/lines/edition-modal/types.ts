import type { IUseModal } from "@fluidattacks/design";

import type { IToeLinesData } from "../types";

interface IFormValues {
  attackedLines: number | string;
  comments: string;
}

interface IEditionModalFormProps {
  selectedToeLinesData: IToeLinesData[];
  handleCloseModal: () => void;
}

interface IUpdateToeLinesAttackedLinesResultAttr {
  updateToeLinesAttackedLines: {
    success: boolean;
  };
}

interface IEditionModalProps {
  groupName: string;
  modalRef: IUseModal;
  selectedToeLinesData: IToeLinesData[];
  refetchData: () => void;
  setSelectedToeLinesData: (selectedToeLinesData: IToeLinesData[]) => void;
}

export type {
  IFormValues,
  IEditionModalProps,
  IEditionModalFormProps,
  IUpdateToeLinesAttackedLinesResultAttr,
};
