import { screen, waitFor } from "@testing-library/react";
import { userEvent } from "@testing-library/user-event";
import { GraphQLError } from "graphql";
import { HttpResponse, type StrictResponse, graphql } from "msw";
import { Route, Routes } from "react-router-dom";

import { UPDATE_TOE_LINES_ATTACKED_LINES } from "./queries";

import { EditionModal } from ".";
import type { IToeLinesData } from "../types";
import type { UpdateToeLinesAttackedLinesMutation as UpdateToeLinesAttackedLines } from "gql/graphql";
import { render } from "mocks";
import { LINK } from "mocks/constants";
import type { IErrorMessage, IMessage } from "mocks/types";
import { msgError, msgSuccess } from "utils/notifications";

jest.mock("utils/notifications", (): Record<string, unknown> => {
  const mockedNotifications: Record<string, () => Record<string, unknown>> =
    jest.requireActual("utils/notifications");
  jest.spyOn(mockedNotifications, "msgError").mockImplementation();
  jest.spyOn(mockedNotifications, "msgSuccess").mockImplementation();

  return mockedNotifications;
});

describe("handle toe lines edition modal", (): void => {
  const memoryRouter = {
    initialEntries: ["/unittesting/surface/lines"],
  };
  const btnConfirm = "Confirm";
  const handleRefetchData = jest.fn();
  const modalRef = {
    close: jest.fn(),
    isOpen: true,
    name: "test-edition-modal",
    open: jest.fn(),
    setIsOpen: jest.fn(),
    toggle: jest.fn(),
  };

  it("should handle attacked lines edition", async (): Promise<void> => {
    expect.hasAssertions();

    jest.clearAllMocks();

    const mocksMutation = [
      graphql
        .link(LINK)
        .mutation(
          UPDATE_TOE_LINES_ATTACKED_LINES,
          ({
            variables,
          }): StrictResponse<
            IErrorMessage | { data: UpdateToeLinesAttackedLines }
          > => {
            const { attackedLines, comments, filename, groupName, rootId } =
              variables;

            if (
              attackedLines === 5 &&
              comments === "This is a test of updating toe lines" &&
              filename === "test/test#.config" &&
              groupName === "groupname" &&
              rootId === "63298a73-9dff-46cf-b42d-9b2f01a56690"
            ) {
              return HttpResponse.json({
                data: {
                  updateToeLinesAttackedLines: { success: true },
                },
              });
            }

            return HttpResponse.json({
              errors: [new GraphQLError("Error updating toe lines")],
            });
          },
        ),
    ];
    const mokedToeLines: IToeLinesData[] = [
      {
        attackedAt: "2021-02-20T05:00:00+00:00",
        attackedBy: "test2@test.com",
        attackedLines: 4,
        bePresent: true,
        bePresentUntil: "",
        comments: "comment 1",
        coverage: 0.1,
        daysToAttack: 4,
        extension: "config",
        filename: "test/test#.config",
        firstAttackAt: "2020-02-19T15:41:04+00:00",
        hasVulnerabilities: true,
        lastAuthor: "user@gmail.com",
        lastCommit: "f9e4beba70c4f34d6117c3b0c23ebe6b2bff66c2",
        loc: 8,
        modifiedDate: "2020-11-15T15:41:04+00:00",
        root: {
          id: "63298a73-9dff-46cf-b42d-9b2f01a56690",
          nickname: "universe",
        },
        rootId: "63298a73-9dff-46cf-b42d-9b2f01a56690",
        rootNickname: "universe",
        seenAt: "2020-02-01T15:41:04+00:00",
        sortsPriorityFactor: 70,
        sortsSuggestions: null,
      },
    ];
    render(
      <Routes>
        <Route
          element={
            <EditionModal
              groupName={"groupname"}
              modalRef={modalRef}
              refetchData={handleRefetchData}
              selectedToeLinesData={mokedToeLines}
              setSelectedToeLinesData={jest.fn()}
            />
          }
          path={"/:groupName/surface/lines"}
        />
      </Routes>,
      { memoryRouter, mocks: mocksMutation },
    );

    await userEvent.clear(screen.getByRole("spinbutton"));
    await userEvent.type(screen.getByRole("spinbutton"), "5");
    await userEvent.type(
      screen.getByRole("textbox"),
      "This is a test of updating toe lines",
    );

    await userEvent.click(screen.getByText(btnConfirm));

    await waitFor((): void => {
      expect(handleRefetchData).toHaveBeenCalledTimes(1);
    });

    expect(modalRef.close).toHaveBeenCalledTimes(1);
    expect(msgSuccess).toHaveBeenCalledWith(
      "group.toe.lines.editModal.alerts.success",
      "groupAlerts.updatedTitle",
    );
  });

  it("should handle error attacked lines edition", async (): Promise<void> => {
    expect.hasAssertions();

    jest.clearAllMocks();

    const mocksQuery = [
      graphql
        .link(LINK)
        .mutation(
          UPDATE_TOE_LINES_ATTACKED_LINES,
          ({
            variables,
          }): StrictResponse<
            IErrorMessage | { data: UpdateToeLinesAttackedLines }
          > => {
            const { attackedLines, comments, filename, groupName, rootId } =
              variables;

            if (
              attackedLines === 6 &&
              comments === "This is a test of error in updating toe lines" &&
              filename === "test/test#.config" &&
              groupName === "groupname" &&
              rootId === "63298a73-9dff-46cf-b42d-9b2f01a56690"
            ) {
              return HttpResponse.json({
                errors: [
                  new GraphQLError(
                    "Exception - The attacked lines must be between 0 and the loc (lines of code)",
                  ),
                ],
              });
            }
            if (
              attackedLines === 9 &&
              comments === "This is a test of error in updating toe lines" &&
              filename === "test/test#.config" &&
              groupName === "groupname" &&
              rootId === "63298a73-9dff-46cf-b42d-9b2f01a56690"
            ) {
              return HttpResponse.json({
                errors: [
                  new GraphQLError(
                    "Exception - The attacked lines must be between 0 and the loc (lines of code)",
                  ),
                ],
              });
            }

            return HttpResponse.json({
              data: {
                updateToeLinesAttackedLines: { success: true },
              },
            });
          },
        ),
    ];

    const mokedToeLines: IToeLinesData[] = [
      {
        attackedAt: "2021-02-20T05:00:00+00:00",
        attackedBy: "test2@test.com",
        attackedLines: 4,
        bePresent: true,
        bePresentUntil: "",
        comments: "comment 1",
        coverage: 0.1,
        daysToAttack: 4,
        extension: "config",
        filename: "test/test#.config",
        firstAttackAt: "2020-02-19T15:41:04+00:00",
        hasVulnerabilities: true,
        lastAuthor: "user@gmail.com",
        lastCommit: "f9e4beba70c4f34d6117c3b0c23ebe6b2bff66c2",
        loc: 8,
        modifiedDate: "2020-11-15T15:41:04+00:00",
        root: {
          id: "63298a73-9dff-46cf-b42d-9b2f01a56690",
          nickname: "universe",
        },
        rootId: "63298a73-9dff-46cf-b42d-9b2f01a56690",
        rootNickname: "universe",
        seenAt: "2020-02-01T15:41:04+00:00",
        sortsPriorityFactor: 70,
        sortsSuggestions: null,
      },
    ];
    const { rerender } = render(
      <Routes>
        <Route
          element={
            <EditionModal
              groupName={"groupname"}
              modalRef={modalRef}
              refetchData={handleRefetchData}
              selectedToeLinesData={mokedToeLines}
              setSelectedToeLinesData={jest.fn()}
            />
          }
          path={"/:groupName/surface/lines"}
        />
      </Routes>,
      { memoryRouter, mocks: mocksQuery },
    );

    await userEvent.clear(screen.getByRole("spinbutton"));
    await userEvent.type(screen.getByRole("spinbutton"), "6");

    await userEvent.type(
      screen.getByRole("textbox"),
      "This is a test of error in updating toe lines",
    );

    await userEvent.click(screen.getByText(btnConfirm));

    await waitFor((): void => {
      expect(msgError).toHaveBeenCalledWith(
        "group.toe.lines.editModal.alerts.invalidAttackedLines",
      );
    });

    rerender(
      <Routes>
        <Route
          element={
            <EditionModal
              groupName={"groupname"}
              modalRef={modalRef}
              refetchData={handleRefetchData}
              selectedToeLinesData={mokedToeLines}
              setSelectedToeLinesData={jest.fn()}
            />
          }
          path={"/:groupName/surface/lines"}
        />
      </Routes>,
    );

    await userEvent.clear(screen.getByRole("spinbutton"));
    await userEvent.type(screen.getByRole("spinbutton"), "9");

    await userEvent.clear(screen.getByRole("textbox"));
    await userEvent.type(
      screen.getByRole("textbox"),
      "This is a second test of error in updating toe lines",
    );

    await userEvent.click(screen.getByText(btnConfirm));

    expect(
      screen.getByText("This value must be between 0 and 8"),
    ).toBeInTheDocument();
  });

  it("should handle other error attacked lines edition", async (): Promise<void> => {
    expect.hasAssertions();

    jest.clearAllMocks();

    const mocksQuery = [
      graphql
        .link(LINK)
        .mutation(
          UPDATE_TOE_LINES_ATTACKED_LINES,
          ({
            variables,
          }): StrictResponse<
            Record<"errors", IMessage[]> | { data: UpdateToeLinesAttackedLines }
          > => {
            const { attackedLines, comments, filename, groupName, rootId } =
              variables;

            if (
              attackedLines === 6 &&
              comments === "This is a test of error in updating toe lines" &&
              filename === "test/test#.config" &&
              groupName === "groupname" &&
              rootId === "63298a73-9dff-46cf-b42d-9b2f01a56690"
            ) {
              return HttpResponse.json({
                errors: [
                  new GraphQLError(
                    "Exception - The toe lines has been updated by another operation",
                  ),
                  new GraphQLError(
                    "An error occurred updating the toe lines attacked lines error",
                  ),
                ],
              });
            }

            return HttpResponse.json({
              data: {
                updateToeLinesAttackedLines: { success: true },
              },
            });
          },
        ),
    ];

    const mokedToeLines: IToeLinesData[] = [
      {
        attackedAt: "2021-02-20T05:00:00+00:00",
        attackedBy: "test2@test.com",
        attackedLines: 0,
        bePresent: true,
        bePresentUntil: "",
        comments: "comment 1",
        coverage: 0.1,
        daysToAttack: 4,
        extension: "config",
        filename: "test/test#.config",
        firstAttackAt: "2020-02-19T15:41:04+00:00",
        hasVulnerabilities: true,
        lastAuthor: "user@gmail.com",
        lastCommit: "f9e4beba70c4f34d6117c3b0c23ebe6b2bff66c2",
        loc: 8,
        modifiedDate: "2020-11-15T15:41:04+00:00",
        root: {
          id: "63298a73-9dff-46cf-b42d-9b2f01a56690",
          nickname: "universe",
        },
        rootId: "63298a73-9dff-46cf-b42d-9b2f01a56690",
        rootNickname: "universe",
        seenAt: "2020-02-01T15:41:04+00:00",
        sortsPriorityFactor: 70,
        sortsSuggestions: null,
      },
    ];
    render(
      <Routes>
        <Route
          element={
            <EditionModal
              groupName={"groupname"}
              modalRef={modalRef}
              refetchData={handleRefetchData}
              selectedToeLinesData={mokedToeLines}
              setSelectedToeLinesData={jest.fn()}
            />
          }
          path={"/:groupName/surface/lines"}
        />
      </Routes>,
      { memoryRouter, mocks: mocksQuery },
    );

    await userEvent.clear(screen.getByRole("spinbutton"));
    await userEvent.type(screen.getByRole("spinbutton"), "6");

    await userEvent.type(
      screen.getByRole("textbox"),
      "This is a test of error in updating toe lines",
    );

    await userEvent.click(screen.getByText(btnConfirm));

    await waitFor((): void => {
      expect(msgError).toHaveBeenCalledWith(
        "group.toe.lines.editModal.alerts.alreadyUpdate",
      );
    });

    expect(msgError).toHaveBeenLastCalledWith("groupAlerts.errorTextsad");
  });
});
