import _ from "lodash";
import type { InferType, ObjectSchema, Schema } from "yup";
import { number, object, string } from "yup";

import type { IToeLinesData } from "../types";
import { translate } from "utils/translations/translate";

const contentTextMatch = /["',;](?:[-=+@\t\r])/u;

const validations = (
  selectedToeLinesData: IToeLinesData[],
): ObjectSchema<InferType<Schema>> => {
  const maxSelectedLoc =
    _.min(
      selectedToeLinesData.map((toeLinesData): number => toeLinesData.loc),
    ) ?? 1;

  return object().shape({
    attackedLines: number()
      .isValidFunction((value): boolean => {
        return _.isInteger(value) || (_.isString(value) && _.isEmpty(value));
      }, translate.t("validations.integer"))
      .isValidFunction(
        (value): boolean => {
          return _.isNumber(value) && value >= 0 && value <= maxSelectedLoc;
        },
        translate.t("validations.between", { max: maxSelectedLoc, min: 0 }),
      ),
    comments: string()
      .isValidTextBeginning()
      .isValidTextField(
        translate.t("validations.invalidTextPattern"),
        contentTextMatch,
      )
      .isValidTextField(),
  });
};

export { validations };
