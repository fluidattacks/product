interface IToeLinesEdge {
  node: IToeLinesAttr;
}

interface IToeLinesConnection {
  __typename: "ToeLinesConnection";
  edges: IToeLinesEdge[];
  pageInfo: {
    hasNextPage: boolean;
    endCursor: string;
  };
  total: number | undefined;
}

interface IToeLinesAttr {
  __typename: "ToeLines";
  attackedAt: string | null;
  attackedBy: string;
  attackedLines: number;
  bePresent: boolean;
  bePresentUntil: string | null;
  comments: string;
  filename: string;
  firstAttackAt: string | null;
  hasVulnerabilities: boolean;
  lastAuthor: string;
  lastCommit: string;
  loc: number;
  modifiedDate: string;
  root: IGitRootAttr;
  seenAt: string;
  sortsPriorityFactor: number;
  sortsSuggestions: ISortsSuggestionAttr[] | null;
}

interface IGitRootAttr {
  id: string;
  nickname: string;
}

interface ISortsSuggestionAttr {
  findingTitle: string;
  probability: number;
}

interface IToeLinesData {
  attackedAt: string;
  attackedBy: string;
  attackedLines: number;
  bePresent: boolean;
  bePresentUntil: string;
  comments: string;
  coverage: number;
  daysToAttack: number;
  extension: string;
  filename: string;
  firstAttackAt: string;
  hasVulnerabilities: boolean;
  lastAuthor: string;
  lastCommit: string;
  loc: number;
  modifiedDate: string;
  root: IGitRootAttr;
  rootNickname: string;
  rootId: string;
  seenAt: string;
  sortsPriorityFactor: number;
  sortsSuggestions: ISortsSuggestionAttr[] | null;
}

interface IGroupToeLinesProps {
  isInternal: boolean;
}

interface IVerifyToeLinesResultAttr {
  updateToeLinesAttackedLines: {
    success: boolean;
    toeLines: IToeLinesAttr | undefined;
  };
}

export type {
  IGitRootAttr,
  IGroupToeLinesProps,
  IToeLinesAttr,
  IToeLinesConnection,
  IToeLinesData,
  IToeLinesEdge,
  ISortsSuggestionAttr,
  IVerifyToeLinesResultAttr,
};
