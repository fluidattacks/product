import type { ApolloError, FetchResult } from "@apollo/client";
import dayjs, { extend } from "dayjs";
import utc from "dayjs/plugin/utc";
import _ from "lodash";

import type {
  IToeLinesAttr,
  IToeLinesData,
  IToeLinesEdge,
  IVerifyToeLinesResultAttr,
} from "./types";

import type { IFilter } from "components/filter/types";
import { Logger } from "utils/logger";
import { msgError } from "utils/notifications";
import { translate } from "utils/translations/translate";

const NOEXTENSION = ".no.extension.";
const COMMIT_LENGTH = 7;
extend(utc);

const commitFormatter = (value: string): string =>
  value.slice(0, COMMIT_LENGTH);

const getCoverage = (toeLinesAttr: IToeLinesAttr): number =>
  toeLinesAttr.loc === 0 ? 1 : toeLinesAttr.attackedLines / toeLinesAttr.loc;

const getDaysToAttack = (toeLinesAttr: IToeLinesAttr): number => {
  if (
    _.isNull(toeLinesAttr.attackedAt) ||
    _.isEmpty(toeLinesAttr.attackedAt) ||
    dayjs(toeLinesAttr.modifiedDate).diff(dayjs(toeLinesAttr.attackedAt)) > 0
  ) {
    if (toeLinesAttr.bePresent) {
      return dayjs().diff(toeLinesAttr.modifiedDate, "days");
    }

    return dayjs(toeLinesAttr.bePresentUntil ?? "").diff(
      toeLinesAttr.modifiedDate,
      "days",
    );
  }

  return dayjs(toeLinesAttr.attackedAt).diff(toeLinesAttr.modifiedDate, "days");
};

const getExtension = (toeLinesAttr: IToeLinesAttr): string => {
  const lastPointindex = toeLinesAttr.filename.lastIndexOf(".");
  const lastSlashIndex = toeLinesAttr.filename.lastIndexOf("/");
  if (lastPointindex === -1 || lastSlashIndex > lastPointindex) {
    return NOEXTENSION;
  }

  return toeLinesAttr.filename.slice(lastPointindex + 1);
};

const formatToeLines = (toeLinesEdges: IToeLinesEdge[]): IToeLinesData[] =>
  toeLinesEdges.map(
    ({ node }): IToeLinesData => ({
      ...node,
      attackedAt: node.attackedAt ?? "-",
      bePresentUntil: node.bePresentUntil ?? "-",
      coverage: getCoverage(node),
      daysToAttack: getDaysToAttack(node),
      extension: getExtension(node),
      firstAttackAt: node.firstAttackAt ?? "-",
      lastCommit: commitFormatter(node.lastCommit),
      modifiedDate: node.modifiedDate,
      rootId: node.root.id,
      rootNickname: node.root.nickname,
      seenAt: node.seenAt,
    }),
  );

const formatLinesFilter = (state: string): string[] | string => {
  const linesParameters: Record<string, string[] | string> = {
    attackedAt: ["fromAttackedAt", "toAttackedAt"],
    attackedBy: "attackedBy",
    attackedLines: ["minAttackedLines", "maxAttackedLines"],
    bePresent: "bePresent",
    bePresentUntil: ["fromBePresentUntil", "toBePresentUntil"],
    comments: "comments",
    coverage: ["minCoverage", "maxCoverage"],
    filename: "filename",
    firstAttackAt: ["fromFirstAttackAt", "toFirstAttackAt"],
    hasVulnerabilities: "hasVulnerabilities",
    lastAuthor: "lastAuthor",
    lastCommit: "lastCommit",
    loc: ["minLoc", "maxLoc"],
    modifiedDate: ["fromModifiedDate", "toModifiedDate"],
    seenAt: ["fromSeenAt", "toSeenAt"],
    sortsPriorityFactor: ["minSortsPriorityFactor", "maxSortsPriorityFactor"],
  };

  return linesParameters[state];
};

const unformatFilterValues: (
  value: IFilter<IToeLinesData>,
) => Record<string, unknown> = (
  value: IFilter<IToeLinesData>,
): Record<string, unknown> => {
  const unformat = (): Record<string, unknown> => {
    const titleFormat = formatLinesFilter(value.id);

    if (typeof titleFormat === "string")
      return _.isNil(value.value) || _.isEmpty(value.value)
        ? { [titleFormat]: undefined }
        : {
            [titleFormat]: ["true", "false"].includes(value.value)
              ? JSON.parse(value.value)
              : value.value,
          };

    if (titleFormat[0].startsWith("min"))
      return {
        [titleFormat[0]]: value.numberRangeValues?.[0],
        [titleFormat[1]]: value.numberRangeValues?.[1],
      };

    return {
      [titleFormat[0]]: _.isEmpty(value.rangeValues?.[0])
        ? value.rangeValues?.[0]
        : dayjs.utc(value.rangeValues?.[0]).toISOString(),
      [titleFormat[1]]: _.isEmpty(value.rangeValues?.[1])
        ? value.rangeValues?.[1]
        : dayjs.utc(value.rangeValues?.[1]).toISOString(),
    };
  };

  return unformat();
};

const handleVerifyTOELinesError = (errors: ApolloError): void => {
  errors.graphQLErrors.forEach((error): void => {
    switch (error.message) {
      case "Exception - The toe lines has been updated by another operation":
        msgError(translate.t("group.toe.lines.editModal.alerts.alreadyUpdate"));
        break;
      case "Exception - The attacked lines must be between 0 and the loc (lines of code)":
        msgError(
          translate.t(
            "group.toe.lines.editModal.alerts.invalidAttackedLinesBetween",
          ),
        );
        break;
      default:
        msgError(translate.t("groupAlerts.errorTextsad"));
        Logger.warning("An error occurred verifying a toe lines", error);
    }
  });
};

const getUpdatedEdges = (
  edges: IToeLinesEdge[],
  result: FetchResult<IVerifyToeLinesResultAttr>,
  toeLine: IToeLinesData,
): IToeLinesEdge[] | undefined => {
  if (
    !_.isNil(result.data) &&
    result.data.updateToeLinesAttackedLines.success
  ) {
    const updatedToeLine = result.data.updateToeLinesAttackedLines.toeLines;
    if (_.isUndefined(updatedToeLine)) {
      return undefined;
    }
    const isChanged = edges.reduce(
      (previousValue: boolean, value: IToeLinesEdge): boolean =>
        value.node.root.id === toeLine.rootId &&
        value.node.filename === toeLine.filename
          ? true
          : previousValue,
      false,
    );

    if (
      updatedToeLine.root.id !== toeLine.rootId ||
      updatedToeLine.filename !== toeLine.filename ||
      !isChanged
    ) {
      return undefined;
    }

    return edges.map(
      (value): IToeLinesEdge =>
        value.node.root.id === toeLine.rootId &&
        value.node.filename === toeLine.filename
          ? {
              node: updatedToeLine,
            }
          : { node: value.node },
    );
  }

  return undefined;
};

export {
  formatToeLines,
  getUpdatedEdges,
  handleVerifyTOELinesError,
  unformatFilterValues,
};
