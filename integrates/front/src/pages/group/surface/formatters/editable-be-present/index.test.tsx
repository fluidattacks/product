import { screen } from "@testing-library/react";
import { userEvent } from "@testing-library/user-event";

import { EditableBePresentFormatter, editableBePresentFormatter } from ".";
import { render } from "mocks";

describe("editableBePresentFormatter", (): void => {
  const row = {
    bePresent: true,
  };
  const handleUpdate = jest.fn();

  it("should render a toggle", (): void => {
    expect.hasAssertions();

    render(
      <EditableBePresentFormatter
        canEdit={true}
        handleUpdate={handleUpdate}
        row={row}
      />,
    );

    expect(screen.getByRole("checkbox")).toBeInTheDocument();
  });

  it("should call handleUpdate when switch is clicked", async (): Promise<void> => {
    expect.hasAssertions();

    jest.clearAllMocks();
    render(
      <EditableBePresentFormatter
        canEdit={true}
        handleUpdate={handleUpdate}
        row={row}
      />,
    );

    await userEvent.click(screen.getByRole("checkbox"));

    expect(handleUpdate).toHaveBeenCalledTimes(1);
  });

  it("should render a tag when canEdit is false", (): void => {
    expect.hasAssertions();

    render(
      <EditableBePresentFormatter
        canEdit={false}
        handleUpdate={handleUpdate}
        row={row}
      />,
    );

    expect(screen.getByText("Yes")).toBeInTheDocument();
  });

  it("should return a component", (): void => {
    expect.hasAssertions();

    const result = editableBePresentFormatter(true, handleUpdate, row);

    expect(result).not.toBeNull();
  });
});
