import { useQuery } from "@apollo/client";
import { Text } from "@fluidattacks/design";
import { Fragment } from "react";
import * as React from "react";
import { useTranslation } from "react-i18next";
import { useParams } from "react-router-dom";
import { useTheme } from "styled-components";

import { GET_TOE_LANGUAGES } from "./queries";
import type { ICodeLanguage } from "./types";

import { Table } from "components/table";
import { useTable } from "hooks";
import { useAudit } from "hooks/use-audit";
import { formatPercentage } from "utils/format-helpers";
import { Logger } from "utils/logger";

export const GroupToeLanguages: React.FC = (): JSX.Element => {
  const { groupName } = useParams() as { groupName: string };
  const { t } = useTranslation();
  const theme = useTheme();
  const tableRef = useTable("tblCodeLanguages");

  const { addAuditEvent } = useAudit();
  const { data, loading } = useQuery(GET_TOE_LANGUAGES, {
    onCompleted: (): void => {
      addAuditEvent("Group.ToeLanguages", groupName);
    },
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error): void => {
        Logger.error("Couldn't load group toe languages", error);
      });
    },
    variables: {
      groupName,
    },
  });

  const languages = data?.group.codeLanguages ?? [];

  const totalLoc = languages.reduce(
    (total, language): number => total + language.loc,
    0,
  );
  const completeData: ICodeLanguage[] = languages.map(
    (lang): ICodeLanguage => ({
      language: lang.language,
      loc: lang.loc,
      percentage: lang.loc / totalLoc,
    }),
  );

  return (
    <Fragment>
      <Text
        color={theme.palette.gray[800]}
        fontWeight={"bold"}
        mb={1.25}
        size={"xl"}
      >
        {t("group.toe.codeLanguages.title")}
      </Text>
      <Table
        columns={[
          {
            accessorKey: "language",
            header: String(t("group.toe.codeLanguages.lang")),
          },
          {
            accessorKey: "loc",
            header: String(t("group.toe.codeLanguages.loc")),
          },
          {
            accessorKey: "percentage",
            cell: (cell): string =>
              formatPercentage(Number(cell.getValue()), true),
            header: String(t("group.toe.codeLanguages.percent")),
          },
        ]}
        csvConfig={{ export: true }}
        data={completeData}
        loadingData={loading}
        tableRef={tableRef}
      />
    </Fragment>
  );
};
