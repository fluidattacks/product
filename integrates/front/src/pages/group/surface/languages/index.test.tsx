import { screen, waitFor } from "@testing-library/react";
import { HttpResponse, type StrictResponse, graphql } from "msw";
import { Route, Routes } from "react-router-dom";

import { GET_TOE_LANGUAGES } from "./queries";

import { GroupToeLanguages } from ".";
import type { GetToeLanguagesQuery as GetToeLanguages } from "gql/graphql";
import { render } from "mocks";
import { LINK } from "mocks/constants";

describe("groupToeLanguages", (): void => {
  const graphqlMocked = graphql.link(LINK);
  const memoryRouter = {
    initialEntries: ["/unittesting/surface/languages"],
  };

  it("should display group toe languages", async (): Promise<void> => {
    expect.hasAssertions();

    render(
      <Routes>
        <Route
          element={<GroupToeLanguages />}
          path={"/:groupName/surface/languages"}
        />
      </Routes>,
      {
        memoryRouter,
        mocks: [
          graphqlMocked.query(
            GET_TOE_LANGUAGES,
            (): StrictResponse<{ data: GetToeLanguages }> => {
              return HttpResponse.json({
                data: {
                  group: {
                    __typename: "Group",
                    codeLanguages: [
                      {
                        __typename: "CodeLanguages",

                        language: "Python",
                        loc: 15,
                      },
                      {
                        __typename: "CodeLanguages",
                        language: "Ruby",
                        loc: 27,
                      },
                    ],
                    name: "unittesting",
                  },
                },
              });
            },
          ),
        ],
      },
    );

    const numberOfRows = 3;
    await waitFor((): void => {
      expect(screen.queryAllByRole("row")).toHaveLength(numberOfRows);
    });

    expect(
      screen.getAllByRole("row")[0].textContent?.replace(/[^a-zA-Z ]/gu, ""),
    ).toStrictEqual(
      [
        "group.toe.codeLanguages.lang",
        "group.toe.codeLanguages.loc",
        "group.toe.codeLanguages.percent",
      ]
        .join("")
        .replace(/[^a-zA-Z ]/gu, ""),
    );
    expect(screen.getAllByRole("row")[1].textContent).toStrictEqual(
      ["Python", "15", "35.71%"].join(""),
    );
    expect(screen.getAllByRole("row")[2].textContent).toStrictEqual(
      ["Ruby", "27", "64.28%"].join(""),
    );
  });

  it("should display empty table", async (): Promise<void> => {
    expect.hasAssertions();

    render(
      <Routes>
        <Route
          element={<GroupToeLanguages />}
          path={"/:groupName/surface/languages"}
        />
      </Routes>,
      {
        memoryRouter,
        mocks: [
          graphqlMocked.query(
            GET_TOE_LANGUAGES,
            (): StrictResponse<{ data: GetToeLanguages }> => {
              return HttpResponse.json({
                data: {
                  group: {
                    __typename: "Group",
                    codeLanguages: null,
                    name: "unittesting",
                  },
                },
              });
            },
          ),
        ],
      },
    );

    const numberOfRows = 2;
    await waitFor((): void => {
      expect(screen.queryAllByRole("row")).toHaveLength(numberOfRows);
    });

    await waitFor((): void => {
      expect(screen.getByText("table.noDataIndication")).toBeInTheDocument();
    });
  });
});
