import { object, string } from "yup";

import { translate } from "utils/translations/translate";

const MAX_GROUP_CONTEXT_LENGTH = 20000;
const MAX_DISAMBIGUATION_INFO_LENGTH = 10000;

const contextValidations = object().shape({
  disambiguation: string().max(
    MAX_GROUP_CONTEXT_LENGTH,
    translate.t("validations.maxLength", {
      count: MAX_GROUP_CONTEXT_LENGTH,
    }),
  ),
});

const disambiguationValidations = object().shape({
  disambiguation: string().max(
    MAX_DISAMBIGUATION_INFO_LENGTH,
    translate.t("validations.maxLength", {
      count: MAX_DISAMBIGUATION_INFO_LENGTH,
    }),
  ),
});

export { contextValidations, disambiguationValidations };
