import { Alert } from "@fluidattacks/design";
import MDEditor from "@uiw/react-md-editor/nohighlight";
import { ErrorMessage } from "formik";
import { useCallback } from "react";
import * as React from "react";
import { useTranslation } from "react-i18next";
import type { PluggableList } from "react-markdown/lib";
import rehypeRemoveImages from "rehype-remove-images";
import rehypeSanitize from "rehype-sanitize";

import type { IFieldProps } from "../../context";

export const UpdateDisambiguation: React.FC<IFieldProps> = ({
  field,
  form: { values, setFieldValue },
}): JSX.Element => {
  const { t } = useTranslation();

  const handleMDChange = useCallback(
    (value: string | undefined): void => {
      setFieldValue("disambiguation", value);
    },
    [setFieldValue],
  );

  return (
    <React.Fragment>
      <MDEditor
        height={200}
        onChange={handleMDChange}
        previewOptions={{
          rehypePlugins: [rehypeRemoveImages, rehypeSanitize] as PluggableList,
        }}
        value={values.disambiguation}
      />
      <ErrorMessage name={field.name} />
      <Alert variant={"info"}>
        {t("searchFindings.groupAccessInfoSection.markdownAlert")}
      </Alert>
    </React.Fragment>
  );
};
