import { Container, Text } from "@fluidattacks/design";
import MDEditor from "@uiw/react-md-editor/nohighlight";
import { Field, Form, useFormikContext } from "formik";
import _ from "lodash";
import { useCallback } from "react";
import * as React from "react";
import { useTranslation } from "react-i18next";
import type { PluggableList } from "react-markdown/lib";
import rehypeRemoveImages from "rehype-remove-images";
import rehypeSanitize from "rehype-sanitize";

import { UpdateDisambiguation } from "./update-form";

import type { IGroupAccessInfo } from "..";
import { ActionButtons } from "../action-buttons";
import type { IFieldProps } from "../context";

interface IDisambiguationForm {
  readonly data: IGroupAccessInfo | undefined;
  readonly isEditing: boolean;
  readonly setEditing: React.Dispatch<React.SetStateAction<boolean>>;
}

const DisambiguationForm: React.FC<IDisambiguationForm> = ({
  data,
  isEditing,
  setEditing,
}: IDisambiguationForm): JSX.Element => {
  const { dirty, resetForm, submitForm } = useFormikContext();
  const isDisambiguationInfoPristine = !dirty;
  const { t } = useTranslation();

  const toggleEdit = useCallback((): void => {
    if (!isDisambiguationInfoPristine) {
      resetForm();
    }
    setEditing(!isEditing);
  }, [isDisambiguationInfoPristine, isEditing, resetForm, setEditing]);

  const handleSubmit = useCallback((): void => {
    if (!isDisambiguationInfoPristine) {
      void submitForm();
    }
  }, [isDisambiguationInfoPristine, submitForm]);

  if (_.isUndefined(data) || _.isEmpty(data)) {
    return <div />;
  }

  const dataset = data.group;

  if (isEditing) {
    return (
      <React.StrictMode>
        <Form data-private={true} id={"editDisambiguationInfo"}>
          <Container
            display={"flex"}
            flexDirection={"column"}
            gap={1}
            height={"100%"}
            justify={"space-between"}
            wrap={"wrap"}
          >
            <Field name={"disambiguation"}>
              {({ field, form }: IFieldProps): JSX.Element => {
                return <UpdateDisambiguation field={field} form={form} />;
              }}
            </Field>
            <ActionButtons
              editTooltip={t(
                "searchFindings.groupAccessInfoSection.tooltips.editDisambiguationInfo",
              )}
              isEditing={isEditing}
              isPristine={isDisambiguationInfoPristine}
              onEdit={toggleEdit}
              onUpdate={handleSubmit}
              permission={
                "integrates_api_mutations_update_group_disambiguation_mutate"
              }
            />
          </Container>
        </Form>
      </React.StrictMode>
    );
  }

  return (
    <React.StrictMode>
      <Form data-private={true} id={"editDisambiguationInfo"}>
        <Container
          display={"flex"}
          flexDirection={"column"}
          gap={1}
          height={"100%"}
          justify={"space-between"}
          wrap={"wrap"}
        >
          {dataset.disambiguation !== null && dataset.disambiguation !== "" ? (
            <div data-color-mode={"light"}>
              <MDEditor.Markdown
                rehypePlugins={
                  [rehypeRemoveImages, rehypeSanitize] as PluggableList
                }
                source={dataset.disambiguation}
                style={{
                  backgroundColor: "inherit",
                  display: "table",
                  fontSize: "14px",
                  tableLayout: "fixed",
                  width: "100%",

                  wordBreak: "break-word",
                }}
              />
            </div>
          ) : (
            <Text size={"md"}>
              {t("searchFindings.groupAccessInfoSection.noDisambiguation")}
            </Text>
          )}
          <ActionButtons
            editTooltip={t(
              "searchFindings.groupAccessInfoSection.tooltips.editDisambiguationInfo",
            )}
            isEditing={isEditing}
            isPristine={isDisambiguationInfoPristine}
            onEdit={toggleEdit}
            onUpdate={handleSubmit}
            permission={
              "integrates_api_mutations_update_group_disambiguation_mutate"
            }
          />
        </Container>
      </Form>
    </React.StrictMode>
  );
};

export { DisambiguationForm };
