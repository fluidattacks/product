import type { FieldInputProps } from "formik";

import type { IGroupAccessInfo } from "..";

interface IGroupContextForm {
  data: IGroupAccessInfo | undefined;
  isEditing: boolean;
  setEditing: React.Dispatch<React.SetStateAction<boolean>>;
}

interface IFieldProps {
  field: FieldInputProps<string>;
  form: {
    values: {
      disambiguation: string;
      groupContext: string;
    };
    setFieldValue: (
      field: string,
      value: string | undefined,
      shouldValidate?: boolean,
    ) => void;
  };
}

export type { IGroupContextForm, IFieldProps };
