import { Alert } from "@fluidattacks/design";
import MDEditor from "@uiw/react-md-editor/nohighlight";
import { ErrorMessage } from "formik";
import { useCallback } from "react";
import * as React from "react";
import { useTranslation } from "react-i18next";
import type { PluggableList } from "react-markdown/lib";
import rehypeRemoveImages from "rehype-remove-images";
import rehypeSanitize from "rehype-sanitize";

import type { IFieldProps } from "..";

export const UpdateGroupContext: React.FC<IFieldProps> = ({
  field,
  form: { values, setFieldValue },
}: IFieldProps): JSX.Element => {
  const { t } = useTranslation();

  const handleMDChange = useCallback(
    (value: string | undefined): void => {
      setFieldValue("groupContext", value);
    },
    [setFieldValue],
  );

  return (
    <React.Fragment>
      <MDEditor
        height={200}
        onChange={handleMDChange}
        previewOptions={{
          rehypePlugins: [rehypeRemoveImages, rehypeSanitize] as PluggableList,
        }}
        value={values.groupContext}
      />
      <ErrorMessage name={field.name} />
      <Alert variant={"info"}>
        {t("searchFindings.groupAccessInfoSection.markdownAlert")}
      </Alert>
    </React.Fragment>
  );
};
