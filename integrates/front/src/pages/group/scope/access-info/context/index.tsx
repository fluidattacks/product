import { Container, Text } from "@fluidattacks/design";
import MDEditor from "@uiw/react-md-editor/nohighlight";
import { Field, Form, useFormikContext } from "formik";
import _ from "lodash";
import { StrictMode, useCallback } from "react";
import { useTranslation } from "react-i18next";
import type { PluggableList } from "react-markdown/lib";
import rehypeRemoveImages from "rehype-remove-images";
import rehypeSanitize from "rehype-sanitize";

import type { IFieldProps, IGroupContextForm } from "./types";
import { UpdateGroupContext } from "./update-form";

import { ActionButtons } from "../action-buttons";

const GroupContextForm = ({
  data,
  isEditing,
  setEditing,
}: Readonly<IGroupContextForm>): JSX.Element => {
  const { dirty, resetForm, submitForm } = useFormikContext();
  const isGroupAccessInfoPristine = !dirty;
  const { t } = useTranslation();

  const toggleEdit = useCallback((): void => {
    if (!isGroupAccessInfoPristine) {
      resetForm();
    }
    setEditing(!isEditing);
  }, [isGroupAccessInfoPristine, isEditing, resetForm, setEditing]);

  const handleSubmit = useCallback((): void => {
    if (!isGroupAccessInfoPristine) {
      void submitForm();
    }
  }, [isGroupAccessInfoPristine, submitForm]);

  if (_.isUndefined(data) || _.isEmpty(data)) {
    return <div />;
  }

  const dataset = data.group;

  if (isEditing) {
    return (
      <StrictMode>
        <Form data-private={true} id={"editGroupAccessInfo"}>
          <Container
            display={"flex"}
            flexDirection={"column"}
            gap={1}
            height={"100%"}
            justify={"space-between"}
            wrap={"wrap"}
          >
            <Field name={"groupContext"}>
              {({ field, form }: IFieldProps): JSX.Element => {
                return <UpdateGroupContext field={field} form={form} />;
              }}
            </Field>
            <ActionButtons
              editTooltip={t(
                "searchFindings.groupAccessInfoSection.tooltips.editGroupContext",
              )}
              isEditing={isEditing}
              isPristine={isGroupAccessInfoPristine}
              onEdit={toggleEdit}
              onUpdate={handleSubmit}
              permission={
                "integrates_api_mutations_update_group_access_info_mutate"
              }
            />
          </Container>
        </Form>
      </StrictMode>
    );
  }

  return (
    <StrictMode>
      <Form data-private={true} id={"editGroupAccessInfo"}>
        <Container
          display={"flex"}
          flexDirection={"column"}
          gap={1}
          height={"100%"}
          justify={"space-between"}
          wrap={"wrap"}
        >
          {dataset.groupContext !== null && dataset.groupContext !== "" ? (
            <div data-color-mode={"light"}>
              <MDEditor.Markdown
                rehypePlugins={
                  [rehypeRemoveImages, rehypeSanitize] as PluggableList
                }
                source={dataset.groupContext}
                style={{
                  backgroundColor: "inherit",
                  display: "table",
                  fontSize: "14px",
                  tableLayout: "fixed",
                  width: "100%",
                  wordBreak: "break-word",
                }}
              />
            </div>
          ) : (
            <Text mb={2} size={"md"}>
              {t("searchFindings.groupAccessInfoSection.noGroupContext")}
            </Text>
          )}
          <ActionButtons
            editTooltip={t(
              "searchFindings.groupAccessInfoSection.tooltips.editGroupContext",
            )}
            isEditing={isEditing}
            isPristine={isGroupAccessInfoPristine}
            onEdit={toggleEdit}
            onUpdate={handleSubmit}
            permission={
              "integrates_api_mutations_update_group_access_info_mutate"
            }
          />
        </Container>
      </Form>
    </StrictMode>
  );
};

export type { IFieldProps };
export { GroupContextForm };
