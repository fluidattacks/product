import { PureAbility } from "@casl/ability";
import { screen, waitFor, within } from "@testing-library/react";
import { userEvent } from "@testing-library/user-event";
import { GraphQLError } from "graphql";
import { HttpResponse, type StrictResponse, graphql } from "msw";
import {
  type AnyObject,
  type InferType,
  type Schema,
  ValidationError,
} from "yup";

import { validationSchema } from "./validations";

import { Services } from ".";
import { GET_GROUP_DATA, UPDATE_GROUP_DATA } from "../queries";
import { authzPermissionsContext } from "context/authz/config";
import type {
  GetGroupDataQuery as GetGroupData,
  UpdateGroupDataMutation as UpdateGroupData,
} from "gql/graphql";
import { Language, ManagedType, ServiceType } from "gql/graphql";
import { render } from "mocks";
import { LINK } from "mocks/constants";
import type { IErrorMessage } from "mocks/types";
import { msgError, msgSuccess } from "utils/notifications";

jest.mock("utils/notifications", (): Record<string, unknown> => {
  const mockedNotifications: Record<string, () => Record<string, unknown>> =
    jest.requireActual("utils/notifications");
  jest.spyOn(mockedNotifications, "msgError").mockImplementation();
  jest.spyOn(mockedNotifications, "msgSuccess").mockImplementation();

  return mockedNotifications;
});

describe("services", (): void => {
  const btnConfirm = "components.modal.confirm";
  const graphqlMocked = graphql.link(LINK);
  const testValidation = (
    groupName: string,
    obj: AnyObject,
  ): InferType<Schema> => {
    return validationSchema(groupName).validateSync(obj);
  };

  const testGroup = [
    { group: "unittesting", texts: 10 },
    { group: "oneshottest", texts: 10 },
    { group: "not-exists", texts: 0 },
  ];

  it.each(testGroup)(
    `should render services for: %s`,
    async (test): Promise<void> => {
      expect.hasAssertions();

      jest.clearAllMocks();

      const mockedPermissions = new PureAbility<string>([
        { action: "integrates_api_mutations_update_group_mutate" },
      ]);

      render(
        <authzPermissionsContext.Provider value={mockedPermissions}>
          <Services groupName={test.group} organizationName={"okada"} />
        </authzPermissionsContext.Provider>,
        {
          memoryRouter: {
            initialEntries: ["/home"],
          },
        },
      );

      await waitFor((): void => {
        expect(
          screen.queryAllByText("searchFindings.servicesTable.", {
            exact: false,
          }),
        ).toHaveLength(test.texts);
      });

      await waitFor((): void => {
        expect(screen.getByLabelText("type")).not.toBeDisabled();
      });

      expect(screen.getByLabelText("service")).not.toBeDisabled();
      expect(
        screen.getByRole("checkbox", { name: "essential" }),
      ).not.toBeDisabled();
      expect(
        screen.getByRole("checkbox", { name: "advanced" }),
      ).not.toBeDisabled();
    },
  );

  it("should show erros", async (): Promise<void> => {
    expect.hasAssertions();

    jest.clearAllMocks();

    const mockQuery = [
      graphqlMocked.query(GET_GROUP_DATA, (): StrictResponse<IErrorMessage> => {
        return HttpResponse.json({
          errors: [new GraphQLError("Error getting group data")],
        });
      }),
    ];

    render(<Services groupName={"unittesting"} organizationName={"okada"} />, {
      memoryRouter: {
        initialEntries: ["/home"],
      },
      mocks: [...mockQuery],
    });

    await waitFor((): void => {
      expect(msgError).toHaveBeenCalledWith("groupAlerts.errorTextsad");
    });
  });

  it("should toggle buttons properly", async (): Promise<void> => {
    expect.hasAssertions();

    jest.clearAllMocks();

    const group: GetGroupData["group"] = {
      __typename: "Group",
      businessId: "",
      businessName: "",
      description: "Integrates unit test project",
      hasAdvanced: true,
      hasEssential: true,
      language: Language.En,
      managed: ManagedType.Managed,
      name: "unittesting",
      service: ServiceType.White,
      sprintDuration: 1,
      sprintStartDate: "2021-06-06T00:00:00",
      subscription: "CONTINUOUS",
    };

    const mockQuery = [
      graphqlMocked.query(
        GET_GROUP_DATA,
        (): StrictResponse<{ data: GetGroupData }> => {
          return HttpResponse.json({
            data: {
              group,
            },
          });
        },
        { once: true },
      ),
      graphqlMocked.query(
        GET_GROUP_DATA,
        (): StrictResponse<{ data: GetGroupData }> => {
          return HttpResponse.json({
            data: {
              group,
            },
          });
        },
        { once: true },
      ),
      graphqlMocked.query(
        GET_GROUP_DATA,
        (): StrictResponse<{ data: GetGroupData }> => {
          return HttpResponse.json({
            data: {
              group,
            },
          });
        },
        { once: true },
      ),
    ];
    const mockMutations = [
      graphqlMocked.query(
        GET_GROUP_DATA,
        (): StrictResponse<{ data: GetGroupData }> => {
          return HttpResponse.json({
            data: {
              group: {
                ...group,
                hasAdvanced: false,
              },
            },
          });
        },
      ),
      graphqlMocked.mutation(
        UPDATE_GROUP_DATA,
        ({
          variables,
        }): StrictResponse<IErrorMessage | { data: UpdateGroupData }> => {
          const { hasASM, hasEssential, hasAdvanced } = variables;

          if (hasASM && hasEssential && hasAdvanced) {
            return HttpResponse.json({
              data: {
                updateGroup: {
                  success: true,
                },
              },
            });
          }
          if (hasASM && !hasEssential && !hasAdvanced) {
            return HttpResponse.json({
              data: {
                updateGroup: {
                  success: true,
                },
              },
            });
          }
          if (hasASM && hasEssential && !hasAdvanced) {
            return HttpResponse.json({
              data: {
                updateGroup: {
                  success: true,
                },
              },
            });
          }

          return HttpResponse.json({
            errors: [new GraphQLError("Error updating group data")],
          });
        },
      ),
    ];

    const mockedPermissions = new PureAbility<string>([
      { action: "integrates_api_mutations_update_group_mutate" },
    ]);

    render(
      <authzPermissionsContext.Provider value={mockedPermissions}>
        <Services groupName={"unittesting"} organizationName={"okada"} />
      </authzPermissionsContext.Provider>,
      {
        memoryRouter: {
          initialEntries: ["/home"],
        },
        mocks: [...mockQuery, ...mockMutations],
      },
    );

    await waitFor((): void => {
      expect(
        screen.queryAllByText("searchFindings.servicesTable.", {
          exact: false,
        }),
      ).toHaveLength(10);
    });

    await userEvent.click(screen.getByRole("checkbox", { name: "essential" }));

    await waitFor((): void => {
      expect(
        screen.getByText("searchFindings.servicesTable.modal.continue"),
      ).toBeInTheDocument();
    });
    await userEvent.click(
      screen.getByText("searchFindings.servicesTable.modal.continue"),
    );
    await waitFor((): void => {
      expect(screen.getByText(btnConfirm)).toBeInTheDocument();
    });
    await waitFor((): void => {
      expect(screen.getByText(btnConfirm)).toBeDisabled();
    });
    await userEvent.type(
      screen.getByRole("textbox", { name: "comments" }),
      "Testing comment",
    );
    await userEvent.type(
      screen.getByPlaceholderText("unittesting"),
      "unittesting",
    );
    await waitFor((): void => {
      expect(screen.getByText(btnConfirm)).not.toBeDisabled();
    });
    await userEvent.click(screen.getByText(btnConfirm));

    await waitFor((): void => {
      expect(msgSuccess).toHaveBeenCalledWith(
        "searchFindings.servicesTable.success",
        "searchFindings.servicesTable.successTitle",
      );
    });

    await userEvent.click(screen.getByRole("checkbox", { name: "advanced" }));
    await waitFor((): void => {
      expect(
        screen.getByText("searchFindings.servicesTable.modal.continue"),
      ).toBeInTheDocument();
    });
    await userEvent.click(
      screen.getByText("searchFindings.servicesTable.modal.continue"),
    );
    await waitFor((): void => {
      expect(screen.getByText(btnConfirm)).toBeInTheDocument();
    });
    await userEvent.clear(screen.getByPlaceholderText("unittesting"));
    await userEvent.type(
      screen.getByPlaceholderText("unittesting"),
      "unittesting",
    );
    await waitFor((): void => {
      expect(screen.getByText(btnConfirm)).not.toBeDisabled();
    });
    await userEvent.click(screen.getByText(btnConfirm));

    await waitFor((): void => {
      expect(msgSuccess).toHaveBeenCalledWith(
        "searchFindings.servicesTable.success",
        "searchFindings.servicesTable.successTitle",
      );
    });

    await userEvent.click(screen.getByRole("checkbox", { name: "advanced" }));
    await waitFor((): void => {
      expect(
        screen.getByText("searchFindings.servicesTable.modal.continue"),
      ).toBeInTheDocument();
    });
    await userEvent.click(
      screen.getByText("searchFindings.servicesTable.modal.continue"),
    );
    await waitFor((): void => {
      expect(screen.getByText(btnConfirm)).toBeInTheDocument();
    });
    await userEvent.clear(screen.getByPlaceholderText("unittesting"));
    await userEvent.type(
      screen.getByPlaceholderText("unittesting"),
      "unittesting",
    );
    await waitFor((): void => {
      expect(screen.getByText(btnConfirm)).not.toBeDisabled();
    });
    await userEvent.click(screen.getByText(btnConfirm));

    await waitFor((): void => {
      expect(msgSuccess).toHaveBeenCalledWith(
        "searchFindings.servicesTable.success",
        "searchFindings.servicesTable.successTitle",
      );
    });

    await waitFor((): void => {
      expect(screen.queryByRole("button")).not.toBeInTheDocument();
    });
    await waitFor((): void => {
      expect(
        screen.getByText("searchFindings.servicesTable.continuous"),
      ).toBeInTheDocument();
    });

    expect(
      screen.getByText("searchFindings.servicesTable.white"),
    ).toBeInTheDocument();
    expect(
      screen.getAllByText("searchFindings.servicesTable.inactive", {
        exact: false,
      }),
    ).toHaveLength(2);
    expect(
      screen.getAllByText("searchFindings.servicesTable.active", {
        exact: false,
      }),
    ).toHaveLength(2);
  });

  it("should display services form as read-only", async (): Promise<void> => {
    expect.hasAssertions();

    jest.clearAllMocks();

    const mockedPermissions = new PureAbility<string>([
      { action: "see_group_services_info" },
    ]);
    render(
      <authzPermissionsContext.Provider value={mockedPermissions}>
        <Services groupName={"unittesting"} organizationName={"okada"} />
      </authzPermissionsContext.Provider>,
      {
        memoryRouter: {
          initialEntries: ["/home"],
        },
      },
    );

    await waitFor((): void => {
      expect(
        screen.queryAllByText("searchFindings.servicesTable.", {
          exact: false,
        }),
      ).toHaveLength(10);
    });

    expect(
      within(screen.getByRole("combobox", { name: "type" })).getAllByTestId(
        "type-select",
      )[0],
    ).toHaveAttribute("aria-disabled", "true");
    expect(
      within(screen.getByRole("combobox", { name: "service" })).getAllByTestId(
        "service-select",
      )[0],
    ).toHaveAttribute("aria-disabled", "true");
    expect(screen.getByRole("checkbox", { name: "essential" })).toBeDisabled();
    expect(screen.getByRole("checkbox", { name: "advanced" })).toBeDisabled();
  });

  it("should show erros when update service trial group", async (): Promise<void> => {
    expect.hasAssertions();

    jest.clearAllMocks();

    const group: GetGroupData["group"] = {
      __typename: "Group",
      businessId: "",
      businessName: "",
      description: "Integrates unit test project",
      hasAdvanced: false,
      hasEssential: true,
      language: Language.En,
      managed: ManagedType.Trial,
      name: "testtrial",
      service: ServiceType.White,
      sprintDuration: 1,
      sprintStartDate: "2021-06-06T00:00:00",
      subscription: "CONTINUOUS",
    };

    const mockQuery = [
      graphqlMocked.query(
        GET_GROUP_DATA,
        (): StrictResponse<{ data: GetGroupData }> => {
          return HttpResponse.json({
            data: {
              group,
            },
          });
        },
        { once: true },
      ),
    ];
    const mockMutations = [
      graphqlMocked.mutation(
        UPDATE_GROUP_DATA,
        ({
          variables,
        }): StrictResponse<IErrorMessage | { data: UpdateGroupData }> => {
          const { groupName, hasAdvanced } = variables;

          if (groupName === "testtrial" && hasAdvanced) {
            return HttpResponse.json({
              errors: [
                new GraphQLError(
                  "Exception - The action is not allowed during the free trial",
                ),
              ],
            });
          }

          return HttpResponse.json({
            data: {
              updateGroup: {
                success: true,
              },
            },
          });
        },
      ),
    ];

    const mockedPermissions = new PureAbility<string>([
      { action: "integrates_api_mutations_update_group_mutate" },
    ]);

    render(
      <authzPermissionsContext.Provider value={mockedPermissions}>
        <Services groupName={"testtrial"} organizationName={"okada"} />
      </authzPermissionsContext.Provider>,
      {
        memoryRouter: {
          initialEntries: ["/home"],
        },
        mocks: [...mockQuery, ...mockMutations],
      },
    );

    await waitFor((): void => {
      expect(
        screen.queryAllByText("searchFindings.servicesTable.", {
          exact: false,
        }),
      ).toHaveLength(10);
    });

    await userEvent.click(screen.getByRole("checkbox", { name: "advanced" }));

    await waitFor((): void => {
      expect(
        screen.getByText("searchFindings.servicesTable.modal.continue"),
      ).toBeInTheDocument();
    });
    await userEvent.click(
      screen.getByText("searchFindings.servicesTable.modal.continue"),
    );
    await waitFor((): void => {
      expect(screen.getByText(btnConfirm)).toBeInTheDocument();
    });
    await waitFor((): void => {
      expect(screen.getByText(btnConfirm)).toBeDisabled();
    });
    await userEvent.type(
      screen.getByRole("textbox", { name: "comments" }),
      "Testing comment",
    );
    await userEvent.type(screen.getByPlaceholderText("testtrial"), "testtrial");
    await waitFor((): void => {
      expect(screen.getByText(btnConfirm)).not.toBeDisabled();
    });
    await userEvent.click(screen.getByText(btnConfirm));

    await waitFor((): void => {
      expect(msgError).toHaveBeenCalledWith(
        "You can only have the essential service during the free trial",
      );
    });
  });

  it("should display required field errors of validation schema", (): void => {
    expect.hasAssertions();

    const groupName = "unittesting";
    const errorString = "Required field";

    const testRequiredField = (): void => {
      testValidation(groupName, {});
    };
    const testCommentsRequiredField = (): void => {
      testValidation(groupName, { confirmation: groupName });
    };
    const testConfirmationRequiredField = (): void => {
      testValidation(groupName, { comments: "Testing group services" });
    };

    expect(testRequiredField).toThrow(new ValidationError(errorString));
    expect(testCommentsRequiredField).toThrow(new ValidationError(errorString));
    expect(testConfirmationRequiredField).toThrow(
      new ValidationError(errorString),
    );
  });

  it("should display comments field validation errors", (): void => {
    expect.hasAssertions();

    const groupName = "unittesting";

    const testInvalidTextBeginning = (): void => {
      testValidation(groupName, { comments: "=", confirmation: groupName });
    };
    const testInvalidTextField = (): void => {
      testValidation(groupName, {
        comments: "Test ^ invalid character",
        confirmation: groupName,
      });
    };

    expect(testInvalidTextBeginning).toThrow(
      new ValidationError(
        "Field cannot begin with the following character: '='",
      ),
    );
    expect(testInvalidTextField).toThrow(
      new ValidationError("Field cannot contain the following characters: '^'"),
    );
  });

  it("should display confirmation field validation errors", (): void => {
    expect.hasAssertions();

    const groupName = "unittesting";

    const testMatchGroupName = (): void => {
      testValidation(groupName, {
        comments: "Testing group services",
        confirmation: "test",
      });
    };

    expect(testMatchGroupName).toThrow(
      new ValidationError(`Expected: ${groupName}`),
    );
  });
});
