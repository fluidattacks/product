import { useMutation, useQuery } from "@apollo/client";
import { useModal } from "@fluidattacks/design";
import _ from "lodash";
import mixpanel from "mixpanel-browser";
import { useCallback } from "react";
import * as React from "react";
import { useTranslation } from "react-i18next";

import { ServicesForm } from "./form";
import type { IFormData, IServicesProps } from "./types";
import { handleEditGroupDataError } from "./utils";
import { validationSchema } from "./validations";

import { GET_GROUP_DATA, UPDATE_GROUP_DATA } from "../queries";
import { Form } from "components/form";
import type {
  ServiceType,
  SubscriptionType,
  UpdateGroupReason,
} from "gql/graphql";
import { GET_GROUP_DATA as GET_GROUP_SERVICES } from "pages/group/queries";
import { Logger } from "utils/logger";
import { msgError, msgSuccess } from "utils/notifications";

const Services: React.FC<IServicesProps> = ({
  groupName,
  organizationName,
}): JSX.Element => {
  const { t } = useTranslation();
  const servicesModal = useModal("services-modal-open");

  // Graphql operations
  const {
    data,
    loading: loadingGroupData,
    refetch: refetchGroupData,
  } = useQuery(GET_GROUP_DATA, {
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error): void => {
        msgError(t("groupAlerts.errorTextsad"));
        Logger.warning("An error occurred getting group data", error);
      });
    },
    variables: { groupName },
  });
  const [editGroupData, { loading: submittingGroupData }] = useMutation(
    UPDATE_GROUP_DATA,
    {
      onCompleted: (): void => {
        mixpanel.track("EditGroupData");
        msgSuccess(
          t("searchFindings.servicesTable.success"),
          t("searchFindings.servicesTable.successTitle"),
        );
        void refetchGroupData({ groupName });
      },
      onError: (error): void => {
        handleEditGroupDataError(error);
      },
      refetchQueries: [
        {
          query: GET_GROUP_SERVICES,
          variables: {
            groupName,
            organizationName: organizationName.toLowerCase(),
          },
        },
      ],
    },
  );

  // Data management
  const handleFormSubmit = useCallback(
    async (values: IFormData): Promise<void> => {
      await editGroupData({
        variables: {
          comments: values.comments,
          description: values.description,
          groupName,
          hasASM: values.asm,
          hasAdvanced: values.advanced,
          hasEssential: values.essential,
          language: values.language,
          reason: values.reason as UpdateGroupReason,
          service: values.service as ServiceType,
          subscription: values.type as SubscriptionType,
        },
      });
      servicesModal.close();
    },
    [editGroupData, groupName, servicesModal],
  );

  if (_.isUndefined(data) || _.isEmpty(data)) {
    return <div />;
  }

  return (
    <React.StrictMode>
      <Form
        enableReinitialize={true}
        initialValues={{
          advanced: data.group.hasAdvanced,
          asm: true,
          comments: "",
          confirmation: "",
          description: data.group.description ?? "",
          essential: data.group.hasEssential,
          language: data.group.language,
          reason: "NONE",
          service: data.group.service,
          type: data.group.subscription.toUpperCase(),
        }}
        name={"editGroup"}
        onSubmit={handleFormSubmit}
        validationSchema={validationSchema(groupName.toLocaleLowerCase())}
      >
        {(props): JSX.Element => (
          <ServicesForm
            data={{
              ...data,
              group: {
                ...data.group,
                __typename: "Group" as const,
                hasForces: false,
                organization: { name: "" },
              },
            }}
            groupName={groupName}
            loadingGroupData={loadingGroupData}
            modalRef={servicesModal}
            submittingGroupData={submittingGroupData}
            {...props}
          />
        )}
      </Form>
    </React.StrictMode>
  );
};

export { Services };
