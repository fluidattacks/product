import {
  Alert,
  Button,
  Container,
  GridContainer,
  Modal,
  Text,
} from "@fluidattacks/design";
import { Form } from "formik";
import _ from "lodash";
import { Fragment, useCallback } from "react";
import * as React from "react";
import { useTranslation } from "react-i18next";
import { useTheme } from "styled-components";

import {
  computeConfirmationMessage,
  downgradeReasons,
  isDowngrading,
  isDowngradingServices,
} from "./utils";

import { Cards } from "../cards";
import type { IServicesFormProps } from "../types";
import { Input, Select, TextArea } from "components/input";
import { List } from "components/list";
import { ModalConfirm } from "components/modal";

const ServicesForm = ({
  data,
  dirty,
  isValid,
  groupName,
  loadingGroupData,
  modalRef,
  setFieldValue,
  submitForm,
  submittingGroupData,
  values,
}: IServicesFormProps): JSX.Element => {
  const { t } = useTranslation();
  const theme = useTheme();
  const { close, open } = modalRef;

  const handleEssentialBtnChange = useCallback(
    (event: React.ChangeEvent<HTMLInputElement>): void => {
      const withEssential = event.target.checked;
      void setFieldValue("essential", withEssential);
      if (!withEssential) {
        void setFieldValue("advanced", false);
      }
    },
    [setFieldValue],
  );
  const handleAdvancedBtnChange = useCallback(
    (event: React.ChangeEvent<HTMLInputElement>): void => {
      const withAdvanced = event.target.checked;
      void setFieldValue("advanced", withAdvanced);
      if (withAdvanced) {
        void setFieldValue("essential", true);
      }
    },
    [setFieldValue],
  );

  const selectOptions = downgradeReasons.map(
    (reason): { header: string; value: string } => ({
      header: t(
        `searchFindings.servicesTable.modal.${_.camelCase(
          reason.toLowerCase(),
        )}`,
      ),
      value: reason,
    }),
  );

  if (_.isUndefined(data) || _.isEmpty(data)) {
    return <div />;
  }

  return (
    <Form id={"editGroup"}>
      <GridContainer lg={4} mb={1.25} md={3} sm={2} xl={4}>
        <Cards
          handleAdvancedBtnChange={handleAdvancedBtnChange}
          handleEssentialBtnChange={handleEssentialBtnChange}
          values={values}
        />
      </GridContainer>
      {!dirty || loadingGroupData || submittingGroupData ? undefined : (
        <Button onClick={open} variant={"primary"}>
          {t("searchFindings.servicesTable.modal.continue")}
        </Button>
      )}
      <Modal
        modalRef={modalRef}
        size={"md"}
        title={t("searchFindings.servicesTable.modal.title")}
      >
        <Container mb={1.25}>
          <Text color={theme.palette.gray[800]} size={"sm"}>
            {t("searchFindings.servicesTable.modal.changesToApply")}
          </Text>
          <List
            items={computeConfirmationMessage(data, values).map(
              (line: string): JSX.Element => (
                <Text key={line} size={"sm"}>
                  {line}
                </Text>
              ),
            )}
          />
        </Container>
        <Container display={"flex"} flexDirection={"column"} gap={0.5}>
          <TextArea
            label={t("searchFindings.servicesTable.modal.observations")}
            maxLength={250}
            name={"comments"}
            placeholder={t(
              "searchFindings.servicesTable.modal.observationsPlaceholder",
            )}
            required={true}
          />
          {isDowngradingServices(data, values) ? (
            <Select
              items={selectOptions}
              label={t("searchFindings.servicesTable.modal.downgrading")}
              name={"reason"}
            />
          ) : undefined}
          {isDowngrading(true, values.asm) ? (
            <Fragment>
              <Text mb={2} size={"sm"}>
                {t("searchFindings.servicesTable.modal.warning")}
              </Text>
              <Alert>
                {t("searchFindings.servicesTable.modal.warningDowngrade")}
              </Alert>
            </Fragment>
          ) : undefined}
          <Input
            label={t("searchFindings.servicesTable.modal.typeGroupName")}
            name={"confirmation"}
            placeholder={groupName.toLowerCase()}
            required={true}
            type={"text"}
          />
          <Alert>
            {`* ${t("organization.tabs.groups.newGroup.extraChargesMayApply")}`}
          </Alert>
        </Container>
        <ModalConfirm
          disabled={!isValid}
          onCancel={close}
          onConfirm={submitForm}
        />
      </Modal>
    </Form>
  );
};

export { ServicesForm };
