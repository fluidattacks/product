import { useAbility } from "@casl/react";
import { CardWithSwitch } from "@fluidattacks/design";
import { Fragment } from "react";
import { useTranslation } from "react-i18next";

import type { IInfoCardProps } from "./types";

import { CardWithDropdown } from "components/card/card-with-dropdown";
import type { TSelectProps } from "components/input/types";
import { authzPermissionsContext } from "context/authz/config";

const Cards = ({
  handleAdvancedBtnChange,
  handleEssentialBtnChange,
  values,
}: Readonly<IInfoCardProps>): JSX.Element => {
  const { t } = useTranslation();
  const permissions = useAbility(authzPermissionsContext);
  const canUpdateGroupServices = permissions.can(
    "integrates_api_mutations_update_group_mutate",
  );

  const offeredService: TSelectProps["items"] = [
    {
      header: t("searchFindings.servicesTable.continuous"),
      value: "CONTINUOUS",
    },
  ];

  const selectCardsProps: TSelectProps[] = [
    {
      disabled: !canUpdateGroupServices,
      items:
        values.type === "ONESHOT"
          ? [
              ...offeredService,
              {
                header: t("searchFindings.servicesTable.oneShot"),
                value: "ONESHOT",
              },
            ]
          : offeredService,
      name: "type",
      title: t("searchFindings.servicesTable.type"),
    },
    {
      disabled: !canUpdateGroupServices,
      items: [
        { header: t("searchFindings.servicesTable.black"), value: "BLACK" },
        { header: t("searchFindings.servicesTable.white"), value: "WHITE" },
      ],
      name: "service",
      title: t("searchFindings.servicesTable.service"),
    },
  ];

  const toogleCardsProps = [
    {
      title: t("searchFindings.servicesTable.essential"),
      toggles: [
        {
          defaultChecked: values.essential,
          disabled: !canUpdateGroupServices,
          id: "essential-plan",
          leftDescription: t("searchFindings.servicesTable.inactive"),
          name: "essential",
          onChange: handleEssentialBtnChange,
          rightDescription: t("searchFindings.servicesTable.active"),
        },
      ],
    },
    {
      title: t("searchFindings.servicesTable.advanced"),
      toggles: [
        {
          defaultChecked: values.advanced,
          disabled: !canUpdateGroupServices,
          id: "advanced-plan",
          leftDescription: t("searchFindings.servicesTable.inactive"),
          name: "advanced",
          onChange: handleAdvancedBtnChange,
          rightDescription: t("searchFindings.servicesTable.active"),
        },
      ],
    },
  ];

  return (
    <Fragment>
      {selectCardsProps.map(
        ({ disabled, id, title, name, items }): JSX.Element => (
          <CardWithDropdown
            disabled={disabled}
            id={id}
            items={items}
            key={name}
            minHeight={"auto"}
            minWidth={"auto"}
            name={name}
            required={true}
            title={title ?? ""}
            variant={"select"}
          />
        ),
      )}
      {toogleCardsProps.map(
        ({ title, toggles }): JSX.Element => (
          <CardWithSwitch
            id={title}
            key={title}
            title={title}
            toggles={toggles}
          />
        ),
      )}
    </Fragment>
  );
};

export { Cards };
