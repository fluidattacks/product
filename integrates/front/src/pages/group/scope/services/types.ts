import type { IUseModal } from "@fluidattacks/design";
import type { FormikProps } from "formik";

interface IFormData {
  advanced: boolean;
  asm: boolean;
  comments: string;
  confirmation: string;
  description: string;
  forces?: boolean;
  language: string;
  essential: boolean;
  reason: string;
  organization?: string;
  service: string;
  type: string;
}

interface IGroupData {
  group: {
    __typename: "Group";
    businessId: string | null;
    businessName: string | null;
    description: string | null;
    hasAdvanced: boolean;
    hasForces: boolean;
    hasEssential: boolean;
    language: string;
    managed: string;
    name: string;
    organization: {
      name: string;
    };
    service: string;
    sprintDuration: number;
    sprintStartDate: string;
    subscription: string;
  };
}

interface IServicesProps {
  groupName: string;
  organizationName: string;
}

interface IServicesFormProps
  extends Pick<
    FormikProps<IFormData>,
    "dirty" | "isValid" | "setFieldValue" | "submitForm" | "values"
  > {
  data: IGroupData | undefined;
  groupName: string;
  modalRef: IUseModal;
  loadingGroupData: boolean;
  submittingGroupData: boolean;
}

interface IServicesDataSet {
  id: string;
  onChange?: (checked: boolean) => void;
  service: string;
}

export type {
  IFormData,
  IGroupData,
  IServicesDataSet,
  IServicesFormProps,
  IServicesProps,
};
