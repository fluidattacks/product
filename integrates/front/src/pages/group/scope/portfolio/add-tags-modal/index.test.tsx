import { useModal } from "@fluidattacks/design";
import { screen, waitFor } from "@testing-library/react";
import { userEvent } from "@testing-library/user-event";

import { AddTagsModal } from ".";
import { render } from "mocks";

const functionMock = (): void => undefined;
const AddTags = (): JSX.Element => {
  const modalRef = useModal("tags-test-modal");

  return (
    <AddTagsModal
      modalRef={{
        ...modalRef,
        close: functionMock,
        isOpen: true,
      }}
      onSubmit={functionMock}
    />
  );
};

describe("add Tags modal", (): void => {
  it("should render", (): void => {
    expect.hasAssertions();

    render(<AddTags />);

    expect(
      screen.queryByText("searchFindings.tabIndicators.tags.modalTitle"),
    ).toBeInTheDocument();
  });

  it("should render input field and add button", (): void => {
    expect.hasAssertions();

    render(<AddTags />);

    expect(screen.queryByRole("textbox")).toBeInTheDocument();
    expect(screen.queryAllByRole("button")).toHaveLength(4);
  });

  it("should add and remove a input field", async (): Promise<void> => {
    expect.hasAssertions();

    const { container } = render(<AddTags />);
    const role = "textbox";

    expect(screen.queryAllByRole(role)).toHaveLength(1);
    expect(screen.queryAllByRole("button")).toHaveLength(4);
    expect(container.querySelector(".fa-trash-can")).not.toBeInTheDocument();

    await userEvent.click(
      screen.getByRole("button", {
        name: "searchFindings.tabIndicators.tags.inputArrayBtn",
      }),
    );
    await waitFor((): void => {
      expect(screen.queryAllByRole(role)).toHaveLength(2);
    });

    expect(screen.queryAllByRole("button")).toHaveLength(5);

    await userEvent.click(screen.getAllByRole("button")[1]);
    await waitFor((): void => {
      expect(screen.queryAllByRole(role)).toHaveLength(1);
    });
  });
});
