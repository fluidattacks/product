import {
  Form,
  InnerForm,
  InputArray,
  Modal,
  type TFormMethods,
  useFieldArray,
} from "@fluidattacks/design";
import { StrictMode } from "react";
import { useTranslation } from "react-i18next";
import { array, object, string } from "yup";

import type { IAddTagsModalProps } from "../types";

interface ITagsForm {
  tags: { tag: string }[];
}

const AddTagsModal = ({
  modalRef,
  onSubmit,
}: IAddTagsModalProps): JSX.Element => {
  const { t } = useTranslation();
  const { close } = modalRef;
  const pattern = /^[a-z0-9]+(?:-[a-z0-9]+)*$/u;

  const validationSchema = object().shape({
    tags: array().of(
      string()
        .required(t("validations.required"))
        .isValidValue(t("validations.tags"), pattern),
    ),
  });

  const ArrayField = ({
    control,
  }: {
    control: TFormMethods<ITagsForm>["control"];
  }): JSX.Element => {
    const { fields, append, remove } = useFieldArray({
      control,
      name: "tags",
    });

    return (
      <InputArray
        addText={t("searchFindings.tabIndicators.tags.inputArrayBtn")}
        append={append}
        control={control}
        fields={fields}
        name={"tags"}
        remove={remove}
      />
    );
  };

  return (
    <StrictMode>
      <Modal
        id={"addTags"}
        modalRef={modalRef}
        size={"sm"}
        title={t("searchFindings.tabIndicators.tags.modalTitle")}
      >
        <Form
          cancelButton={{ onClick: close }}
          defaultValues={{ tags: [{ tag: "" } as unknown as string] }}
          id={"portfolio-add-confirm"}
          onSubmit={onSubmit}
          yupSchema={validationSchema}
        >
          <InnerForm<ITagsForm>>
            {({ control }): JSX.Element => <ArrayField control={control} />}
          </InnerForm>
        </Form>
      </Modal>
    </StrictMode>
  );
};

export type { IAddTagsModalProps };
export { AddTagsModal };
