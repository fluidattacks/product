import type { IURLRootAttr } from "../../../types";

interface IExcludedSubPathsProps {
  groupName: string;
  initialValues: IURLRootAttr;
  onClose: () => void;
}

interface ISubPathData {
  subPath: string;
}

export type { IExcludedSubPathsProps, ISubPathData };
