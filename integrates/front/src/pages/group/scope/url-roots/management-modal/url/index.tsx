import { Form, InnerForm, Input } from "@fluidattacks/design";
import * as React from "react";
import { Fragment } from "react";
import { useTranslation } from "react-i18next";
import { object, string } from "yup";

import { useAudit } from "hooks/use-audit";
import type { IURLRootAttr } from "pages/group/surface/inputs/addition-modal/types";

interface IUrlProps {
  readonly initialValues: IURLRootAttr;
  readonly isEditing: boolean;
  readonly onClose: () => void;
  readonly onSubmit: (values: {
    id: string;
    nickname: string;
    url: string;
  }) => Promise<void>;
}

const validationSchema = object().shape({
  nickname: string()
    .required()
    .matches(/^[a-zA-Z_0-9-]{1,128}$/u),
  url: string().required(),
});

const Url: React.FC<IUrlProps> = ({
  initialValues,
  isEditing,
  onClose,
  onSubmit,
}): JSX.Element => {
  const { t } = useTranslation();

  const { addAuditEvent } = useAudit();
  React.useEffect((): void => {
    if (initialValues.id) addAuditEvent("Root", initialValues.id);
  }, [addAuditEvent, initialValues.id]);

  return (
    <React.StrictMode>
      <Form
        cancelButton={{ onClick: onClose }}
        defaultValues={{
          id: initialValues.id,
          nickname: initialValues.nickname,
          url: initialValues.host,
        }}
        id={"ipRoot"}
        onSubmit={onSubmit}
        yupSchema={validationSchema}
      >
        <InnerForm>
          {({ formState, getFieldState, register }): JSX.Element => {
            const urlState = getFieldState("url");
            const nicknameState = getFieldState("nickname");

            return (
              <Fragment>
                <Input
                  disabled={isEditing}
                  error={formState.errors.url?.message?.toString()}
                  isTouched={urlState.isTouched}
                  isValid={!urlState.invalid}
                  label={t("group.scope.ip.address")}
                  name={"url"}
                  register={register}
                />
                <Input
                  error={formState.errors.nickname?.message?.toString()}
                  isTouched={nicknameState.isTouched}
                  isValid={!nicknameState.invalid}
                  label={t("group.scope.ip.nickname")}
                  name={"nickname"}
                  register={register}
                />
              </Fragment>
            );
          }}
        </InnerForm>
      </Form>
    </React.StrictMode>
  );
};

export { Url };
