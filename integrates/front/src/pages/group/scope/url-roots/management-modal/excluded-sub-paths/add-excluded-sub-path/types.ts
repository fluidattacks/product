import type { IUseModal } from "@fluidattacks/design";

interface IAddExcludedSubPathProps {
  groupName: string;
  excludedSubPaths: string[];
  modalRef: IUseModal;
  rootId: string;
  onClose: () => void;
}

interface IFormProps {
  subPath: string;
}

export type { IAddExcludedSubPathProps, IFormProps };
