import { PureAbility } from "@casl/ability";
import { screen, waitFor, within } from "@testing-library/react";
import { userEvent } from "@testing-library/user-event";
import { GraphQLError } from "graphql";
import isEqual from "lodash/isEqual";
import { HttpResponse, type StrictResponse, graphql } from "msw";
import { Route, Routes } from "react-router-dom";

import { ExcludedSubPaths } from "./management-modal/excluded-sub-paths";

import { URLRoots } from ".";
import {
  ACTIVATE_ROOT,
  ADD_URL_ROOT,
  DEACTIVATE_ROOT,
  GET_URL_ROOTS,
  UPDATE_URL_ROOT,
  UPDATE_URL_ROOT_EXCLUDED_SUB_PATHS,
} from "../queries";
import { authzPermissionsContext } from "context/authz/config";
import {
  type ActivateRootMutation,
  type AddUrlRootMutation as AddUrlRoot,
  type DeactivateRootMutation as DeactivateRoot,
  type GetUrlRootsQuery as GetUrlRoots,
  RootDeactivationReason,
  type UpdateUrlRootExcludedSubPathsMutation as UpdateUrlRootExcludedSubPaths,
  type UpdateUrlRootMutation,
} from "gql/graphql";
import { render } from "mocks";
import { LINK } from "mocks/constants";
import type { IErrorMessage } from "mocks/types";
import { msgError, msgSuccess } from "utils/notifications";

jest.mock("utils/notifications", (): Record<string, unknown> => {
  const mockedNotifications: Record<string, () => Record<string, unknown>> =
    jest.requireActual("utils/notifications");
  jest.spyOn(mockedNotifications, "msgError").mockImplementation();
  jest.spyOn(mockedNotifications, "msgSuccess").mockImplementation();

  return mockedNotifications;
});

describe("uRLRoots", (): void => {
  // eslint-disable-next-line jest/no-hooks
  beforeEach((): void => {
    jest.spyOn(console, "warn").mockImplementation();
  });

  const graphqlMocked = graphql.link(LINK);
  const initialUrlRootsMockGlobal = graphqlMocked.query(
    GET_URL_ROOTS,
    (): StrictResponse<{ data: GetUrlRoots }> => {
      return HttpResponse.json({
        data: {
          __typename: "Query",
          ...{
            group: {
              __typename: "Group",
              name: "unittesting",
              urlRoots: {
                __typename: "URLRootsConnection",
                edges: [
                  {
                    __typename: "URLRootEdge",
                    node: {
                      __typename: "URLRoot",
                      host: "app.fluidattacks.com",
                      id: "8493c82f-2860-4902-86fa-75b0fef76034",
                      nickname: "url_root_1",
                      path: "/",
                      port: 443,
                      protocol: "HTTPS",
                      query: null,
                      state: "ACTIVE",
                    },
                  },
                  {
                    __typename: "URLRootEdge",
                    node: {
                      __typename: "URLRoot",
                      host: "app.testingurl.com",
                      id: "6493c82f-2860-4902-86fa-75b0fef76034",
                      nickname: "url_root_3",
                      path: "/",
                      port: 443,
                      protocol: "HTTPS",
                      query: null,
                      state: "INACTIVE",
                    },
                  },
                ],
                pageInfo: {
                  endCursor:
                    "ROOT#8493c82f-2860-4902-86fa-75b0fef76034#GROUP#unittesting",
                  hasNextPage: false,
                },
                total: 1,
              },
            },
          },
        },
      });
    },
  );

  it("should render url roots", async (): Promise<void> => {
    expect.hasAssertions();

    render(
      <authzPermissionsContext.Provider value={new PureAbility([])}>
        <URLRoots groupName={"unittesting"} />
      </authzPermissionsContext.Provider>,
      { mocks: [initialUrlRootsMockGlobal] },
    );

    expect(screen.queryByRole("table")).toBeInTheDocument();

    await waitFor((): void => {
      expect(
        screen
          .queryAllByRole("row")[0]
          .textContent?.replace(/[^a-zA-Z ]/gu, ""),
      ).toStrictEqual(
        [
          "group.scope.url.host",
          "group.scope.url.path",
          "group.scope.url.port",
          "group.scope.url.protocol",
          "group.scope.url.query",
          "group.scope.ip.nickname",
          "group.scope.common.state",
        ]
          .join("")
          .replace(/[^a-zA-Z ]/gu, ""),
      );
    });
    await waitFor((): void => {
      expect(
        screen
          .queryAllByRole("row")[1]
          .textContent?.replace(/[^a-zA-Z ]/gu, ""),
      ).toStrictEqual(
        [
          "app.fluidattacks.com",
          "/",
          "443",
          "HTTPS",
          "",
          "url_root_1",
          "Active",
        ]
          .join("")
          .replace(/[^a-zA-Z ]/gu, ""),
      );
    });

    jest.clearAllMocks();
  });

  it("should add url roots", async (): Promise<void> => {
    expect.hasAssertions();

    const mutationMock = graphqlMocked.mutation(
      ADD_URL_ROOT,
      ({ variables }): StrictResponse<IErrorMessage | { data: AddUrlRoot }> => {
        const { groupName, nickname, url } = variables;

        if (
          groupName === "unittesting" &&
          nickname === "url_root_2" &&
          url === "https://app.test.com/test"
        ) {
          return HttpResponse.json({
            data: {
              addUrlRoot: { __typename: "AddRootPayload", success: true },
            },
          });
        }

        return HttpResponse.json({
          errors: [new GraphQLError("Error adding roots")],
        });
      },
    );
    render(
      <authzPermissionsContext.Provider
        value={
          new PureAbility([
            { action: "integrates_api_mutations_add_url_root_mutate" },
            { action: "integrates_api_mutations_update_url_root_mutate" },
          ])
        }
      >
        <Routes>
          <Route
            element={<URLRoots groupName={"unittesting"} />}
            path={"/orgs/:organizationName/groups/:groupName/scope"}
          />
        </Routes>
      </authzPermissionsContext.Provider>,
      {
        memoryRouter: {
          initialEntries: ["/orgs/okada/groups/unittesting/scope"],
        },
        mocks: [initialUrlRootsMockGlobal, mutationMock],
      },
    );

    expect(screen.queryByRole("table")).toBeInTheDocument();

    await waitFor((): void => {
      expect(
        screen.getByRole("button", { name: "group.scope.common.add" }),
      ).toBeInTheDocument();
    });

    await userEvent.click(
      screen.getByRole("button", { name: "group.scope.common.add" }),
    );

    expect(screen.getByRole("button", { name: "Confirm" })).toBeDisabled();

    await userEvent.type(
      screen.getByRole("textbox", { name: "url" }),
      "https://app.test.com/test",
    );

    await userEvent.type(
      screen.getByRole("textbox", { name: "nickname" }),
      "url_root_2",
    );

    await waitFor((): void => {
      expect(
        screen.getByRole("button", { name: "Confirm" }),
      ).not.toBeDisabled();
    });
    await userEvent.click(screen.getByText("Confirm"));

    jest.clearAllMocks();
  });

  it("should handle error when adding url roots", async (): Promise<void> => {
    expect.hasAssertions();

    const mutationMock = graphqlMocked.mutation(
      ADD_URL_ROOT,
      ({ variables }): StrictResponse<IErrorMessage | { data: AddUrlRoot }> => {
        const { groupName, nickname, url } = variables;
        if (
          groupName === "unittesting" &&
          nickname === "url_root_2" &&
          url === "https://app.fluidattacks.com"
        ) {
          return HttpResponse.json({
            errors: [
              new GraphQLError(
                "Exception - Root with the same URL/branch already exists",
              ),
            ],
          });
        }

        if (
          groupName === "unittesting" &&
          nickname === "url_root_1" &&
          url === "https://app.testing.com"
        ) {
          return HttpResponse.json({
            errors: [
              new GraphQLError(
                "Exception - Root with the same nickname already exists",
              ),
            ],
          });
        }

        if (
          groupName === "unittesting" &&
          nickname === "url_root_2" &&
          url === "error_Url"
        ) {
          return HttpResponse.json({
            errors: [new GraphQLError("Exception - Error value is not valid")],
          });
        }
        if (
          groupName === "unittesting" &&
          nickname === "url_root_2" &&
          url === "https://app.testing.com|test"
        ) {
          return HttpResponse.json({
            errors: [new GraphQLError("Exception - Invalid characters")],
          });
        }

        return HttpResponse.json({
          data: {
            addUrlRoot: { __typename: "AddRootPayload", success: true },
          },
        });
      },
    );
    render(
      <authzPermissionsContext.Provider
        value={
          new PureAbility([
            { action: "integrates_api_mutations_add_url_root_mutate" },
            { action: "integrates_api_mutations_update_url_root_mutate" },
          ])
        }
      >
        <Routes>
          <Route
            element={<URLRoots groupName={"unittesting"} />}
            path={"/orgs/:organizationName/groups/:groupName/scope"}
          />
        </Routes>
      </authzPermissionsContext.Provider>,
      {
        memoryRouter: {
          initialEntries: ["/orgs/okada/groups/unittesting/scope"],
        },
        mocks: [initialUrlRootsMockGlobal, mutationMock],
      },
    );

    expect(screen.queryByRole("table")).toBeInTheDocument();

    await waitFor((): void => {
      expect(
        screen.getByRole("button", { name: "group.scope.common.add" }),
      ).toBeInTheDocument();
    });

    await userEvent.click(
      screen.getByRole("button", { name: "group.scope.common.add" }),
    );

    expect(screen.getByRole("button", { name: "Confirm" })).toBeDisabled();

    // Exception - Root with the same nickname already exists
    await userEvent.type(
      screen.getByRole("textbox", { name: "url" }),
      "https://app.testing.com",
    );
    await userEvent.type(
      screen.getByRole("textbox", { name: "nickname" }),
      "url_root_1",
    );
    await userEvent.click(screen.getByText("Confirm"));

    await waitFor((): void => {
      expect(msgError).toHaveBeenCalledWith(
        "group.scope.common.errors.duplicateNickname",
      );
    });

    // Exception - Root with the same URL/branch already exists
    await userEvent.clear(screen.getByRole("textbox", { name: "url" }));
    await userEvent.type(
      screen.getByRole("textbox", { name: "url" }),
      "https://app.fluidattacks.com",
    );
    await userEvent.clear(screen.getByRole("textbox", { name: "nickname" }));
    await userEvent.type(
      screen.getByRole("textbox", { name: "nickname" }),
      "url_root_2",
    );
    await userEvent.click(screen.getByText("Confirm"));

    await waitFor((): void => {
      expect(msgError).toHaveBeenCalledWith(
        "group.scope.common.errors.duplicateUrl",
      );
    });

    // Exception - Error value is not valid
    await userEvent.clear(screen.getByRole("textbox", { name: "url" }));
    await userEvent.type(
      screen.getByRole("textbox", { name: "url" }),
      "error_Url",
    );
    await userEvent.click(screen.getByText("Confirm"));

    await waitFor((): void => {
      expect(msgError).toHaveBeenCalledWith("group.scope.url.errors.invalid");
    });

    // Exception - Invalid characters
    await userEvent.clear(screen.getByRole("textbox", { name: "url" }));
    await userEvent.type(
      screen.getByRole("textbox", { name: "url" }),
      "https://app.testing.com|test",
    );

    await userEvent.click(screen.getByText("Confirm"));

    await waitFor((): void => {
      expect(msgError).toHaveBeenCalledWith(
        "group.scope.url.errors.invalidCharacters",
      );
    });

    jest.clearAllMocks();
  });

  it("should update url roots", async (): Promise<void> => {
    expect.hasAssertions();

    const mutationMock = graphqlMocked.mutation(
      UPDATE_URL_ROOT,
      ({
        variables,
      }): StrictResponse<IErrorMessage | { data: UpdateUrlRootMutation }> => {
        const { groupName, nickname, rootId } = variables;

        if (
          groupName === "unittesting" &&
          nickname === "url_root_2_test" &&
          rootId === "8493c82f-2860-4902-86fa-75b0fef76034"
        ) {
          return HttpResponse.json({
            data: {
              updateUrlRoot: { success: true },
            },
          });
        }

        return HttpResponse.json({
          errors: [new GraphQLError("Error updating roots")],
        });
      },
    );
    render(
      <authzPermissionsContext.Provider
        value={
          new PureAbility([
            { action: "integrates_api_mutations_add_url_root_mutate" },
            { action: "integrates_api_mutations_update_url_root_mutate" },
          ])
        }
      >
        <Routes>
          <Route
            element={<URLRoots groupName={"unittesting"} />}
            path={"/orgs/:organizationName/groups/:groupName/scope"}
          />
        </Routes>
      </authzPermissionsContext.Provider>,
      {
        memoryRouter: {
          initialEntries: ["/orgs/okada/groups/unittesting/scope"],
        },
        mocks: [initialUrlRootsMockGlobal, mutationMock],
      },
    );

    expect(screen.queryByRole("table")).toBeInTheDocument();

    await waitFor((): void => {
      expect(
        screen.getByRole("cell", { name: "app.fluidattacks.com" }),
      ).toBeInTheDocument();
    });
    await userEvent.click(
      screen.getByRole("cell", {
        name: "app.fluidattacks.com",
      }),
    );

    expect(screen.queryAllByRole("link")).toHaveLength(2);

    expect(screen.getByRole("button", { name: "Confirm" })).toBeDisabled();

    await userEvent.clear(screen.getByRole("textbox", { name: "nickname" }));

    await userEvent.type(
      screen.getByRole("textbox", { name: "nickname" }),
      "url_root_2_test",
    );

    await waitFor((): void => {
      expect(
        screen.getByRole("button", { name: "Confirm" }),
      ).not.toBeDisabled();
    });
    await userEvent.click(screen.getByText("Confirm"));

    expect(msgError).toHaveBeenCalledTimes(0);

    jest.clearAllMocks();
  });

  it("should handle error when updating url roots", async (): Promise<void> => {
    expect.hasAssertions();

    const mutationMock = graphqlMocked.mutation(
      UPDATE_URL_ROOT,
      ({
        variables,
      }): StrictResponse<IErrorMessage | { data: UpdateUrlRootMutation }> => {
        const { groupName, nickname, rootId } = variables;

        if (
          groupName === "unittesting" &&
          nickname === "url_root_3" &&
          rootId === "8493c82f-2860-4902-86fa-75b0fef76034"
        ) {
          return HttpResponse.json({
            errors: [
              new GraphQLError(
                "Exception - Root with the same nickname already exists",
              ),
            ],
          });
        }

        return HttpResponse.json({
          data: {
            updateUrlRoot: { success: true },
          },
        });
      },
    );
    render(
      <authzPermissionsContext.Provider
        value={
          new PureAbility([
            { action: "integrates_api_mutations_add_url_root_mutate" },
            { action: "integrates_api_mutations_update_url_root_mutate" },
          ])
        }
      >
        <Routes>
          <Route
            element={<URLRoots groupName={"unittesting"} />}
            path={"/orgs/:organizationName/groups/:groupName/scope"}
          />
        </Routes>
      </authzPermissionsContext.Provider>,
      {
        memoryRouter: {
          initialEntries: ["/orgs/okada/groups/unittesting/scope"],
        },
        mocks: [initialUrlRootsMockGlobal, mutationMock],
      },
    );

    expect(screen.queryByRole("table")).toBeInTheDocument();

    await waitFor((): void => {
      expect(
        screen.getByRole("cell", { name: "app.fluidattacks.com" }),
      ).toBeInTheDocument();
    });
    await userEvent.click(
      screen.getByRole("cell", {
        name: "app.fluidattacks.com",
      }),
    );

    expect(screen.queryAllByRole("link")).toHaveLength(2);

    expect(screen.getByRole("button", { name: "Confirm" })).toBeDisabled();

    await userEvent.clear(screen.getByRole("textbox", { name: "nickname" }));

    await userEvent.type(
      screen.getByRole("textbox", { name: "nickname" }),
      "url_root_3",
    );

    await waitFor((): void => {
      expect(
        screen.getByRole("button", { name: "Confirm" }),
      ).not.toBeDisabled();
    });
    await userEvent.click(screen.getByText("Confirm"));

    await waitFor((): void => {
      expect(msgError).toHaveBeenCalledWith(
        "group.scope.common.errors.duplicateNickname",
      );
    });

    jest.clearAllMocks();
  });

  it("should activate url roots", async (): Promise<void> => {
    expect.hasAssertions();

    const mutationMock = graphqlMocked.mutation(
      ACTIVATE_ROOT,
      ({
        variables,
      }): StrictResponse<IErrorMessage | { data: ActivateRootMutation }> => {
        const { groupName, id } = variables;

        if (
          groupName === "unittesting" &&
          id === "6493c82f-2860-4902-86fa-75b0fef76034"
        ) {
          return HttpResponse.json({
            data: {
              activateRoot: { success: true },
            },
          });
        }

        return HttpResponse.json({
          errors: [new GraphQLError("Error activating roots")],
        });
      },
    );
    render(
      <authzPermissionsContext.Provider
        value={
          new PureAbility([
            { action: "integrates_api_mutations_add_url_root_mutate" },
            { action: "integrates_api_mutations_update_url_root_mutate" },
            { action: "integrates_api_mutations_activate_root_mutate" },
          ])
        }
      >
        <Routes>
          <Route
            element={<URLRoots groupName={"unittesting"} />}
            path={"/orgs/:organizationName/groups/:groupName/scope"}
          />
        </Routes>
      </authzPermissionsContext.Provider>,
      {
        memoryRouter: {
          initialEntries: ["/orgs/okada/groups/unittesting/scope"],
        },
        mocks: [initialUrlRootsMockGlobal, mutationMock],
      },
    );

    expect(screen.queryByRole("table")).toBeInTheDocument();

    await waitFor((): void => {
      expect(
        within(
          screen.queryAllByRole("table")[0],
        ).getAllByRole<HTMLInputElement>("checkbox")[1].checked,
      ).toBe(false);
    });
    await userEvent.click(
      within(screen.queryAllByRole("table")[0]).getAllByRole("checkbox")[1],
    );

    await waitFor((): void => {
      expect(
        screen.getAllByText(/group.scope.common.confirm/iu)[1],
      ).toBeInTheDocument();
    });

    await userEvent.click(
      screen.getAllByRole("button", { name: "Confirm" })[1],
    );

    jest.clearAllMocks();
  });

  it("should activating url roots", async (): Promise<void> => {
    expect.hasAssertions();

    const mutationMock = graphqlMocked.mutation(
      ACTIVATE_ROOT,
      (): StrictResponse<IErrorMessage> => {
        return HttpResponse.json({
          errors: [new GraphQLError("Error activating roots")],
        });
      },
    );
    render(
      <authzPermissionsContext.Provider
        value={
          new PureAbility([
            { action: "integrates_api_mutations_add_url_root_mutate" },
            { action: "integrates_api_mutations_update_url_root_mutate" },
            { action: "integrates_api_mutations_activate_root_mutate" },
          ])
        }
      >
        <Routes>
          <Route
            element={<URLRoots groupName={"unittesting"} />}
            path={"/orgs/:organizationName/groups/:groupName/scope"}
          />
        </Routes>
      </authzPermissionsContext.Provider>,
      {
        memoryRouter: {
          initialEntries: ["/orgs/okada/groups/unittesting/scope"],
        },
        mocks: [initialUrlRootsMockGlobal, mutationMock],
      },
    );

    expect(screen.queryByRole("table")).toBeInTheDocument();

    await waitFor((): void => {
      expect(
        within(
          screen.queryAllByRole("table")[0],
        ).getAllByRole<HTMLInputElement>("checkbox")[1].checked,
      ).toBe(false);
    });
    await userEvent.click(
      within(screen.queryAllByRole("table")[0]).getAllByRole("checkbox")[1],
    );

    await waitFor((): void => {
      expect(
        screen.getAllByText(/group.scope.common.confirm/iu)[1],
      ).toBeInTheDocument();
    });

    await userEvent.click(
      screen.getAllByRole("button", { name: "Confirm" })[1],
    );
    await waitFor((): void => {
      expect(msgError).toHaveBeenCalledWith("groupAlerts.errorTextsad");
    });
    jest.clearAllMocks();
  });

  it("should deactivate url roots", async (): Promise<void> => {
    expect.hasAssertions();

    const mutationMock = graphqlMocked.mutation(
      DEACTIVATE_ROOT,
      ({
        variables,
      }): StrictResponse<IErrorMessage | { data: DeactivateRoot }> => {
        const { groupName, id, other, reason: reasonFromVariables } = variables;
        if (
          groupName === "unittesting" &&
          id === "8493c82f-2860-4902-86fa-75b0fef76034" &&
          other === "" &&
          reasonFromVariables === RootDeactivationReason.RegisteredByMistake
        ) {
          return HttpResponse.json({
            data: {
              deactivateRoot: { __typename: "SimplePayload", success: true },
            },
          });
        }

        return HttpResponse.json({
          errors: [new GraphQLError("Error deactivating roots")],
        });
      },
    );
    render(
      <authzPermissionsContext.Provider
        value={
          new PureAbility([
            { action: "integrates_api_mutations_add_url_root_mutate" },
            { action: "integrates_api_mutations_update_url_root_mutate" },
            { action: "integrates_api_mutations_activate_root_mutate" },
          ])
        }
      >
        <Routes>
          <Route
            element={<URLRoots groupName={"unittesting"} />}
            path={"/orgs/:organizationName/groups/:groupName/scope"}
          />
        </Routes>
      </authzPermissionsContext.Provider>,
      {
        memoryRouter: {
          initialEntries: ["/orgs/okada/groups/unittesting/scope"],
        },
        mocks: [initialUrlRootsMockGlobal, mutationMock],
      },
    );

    expect(screen.queryByRole("table")).toBeInTheDocument();

    await waitFor((): void => {
      expect(
        within(
          screen.queryAllByRole("table")[0],
        ).getAllByRole<HTMLInputElement>("checkbox")[0].checked,
      ).toBe(true);
    });
    await userEvent.click(
      within(screen.queryAllByRole("table")[0]).getAllByRole("checkbox")[0],
    );

    await waitFor((): void => {
      expect(
        screen.getByText(/group.scope.common.deactivation.title/iu),
      ).toBeInTheDocument();
    });

    expect(screen.getByText("Confirm")).toBeDisabled();

    await userEvent.click(
      screen.getByText("group.scope.common.deactivation.reason.label"),
    );
    await userEvent.click(
      screen.getByText("group.scope.common.deactivation.reason.mistake"),
    );
    await waitFor((): void => {
      expect(screen.getByText("Confirm")).not.toBeDisabled();
    });
    await userEvent.click(screen.getByRole("button", { name: "Confirm" }));
    await waitFor((): void => {
      expect(
        screen.queryByText("group.scope.common.confirm"),
      ).toBeInTheDocument();
    });
    await userEvent.click(
      screen.getAllByRole("button", { name: "Confirm" })[1],
    );

    jest.clearAllMocks();
  });

  it("should list excluded sub-paths", async (): Promise<void> => {
    expect.hasAssertions();

    const handleClose: jest.Mock = jest.fn();

    render(
      <authzPermissionsContext.Provider value={new PureAbility([])}>
        <ExcludedSubPaths
          groupName={"group"}
          initialValues={{
            __typename: "URLRoot",
            host: "",
            id: "c7118ab2-9345-4398-895f-11ce6e3cc1a6",
            nickname: "",
            path: "",
            port: 0,
            protocol: "HTTPS",
            query: "",
            state: "ACTIVE",
          }}
          onClose={handleClose}
        />
      </authzPermissionsContext.Provider>,
    );

    await waitFor((): void => {
      expect(screen.getByText(/subPath\/1/iu)).toBeInTheDocument();
    });

    jest.clearAllMocks();
  });

  it("should remove excluded sub-path", async (): Promise<void> => {
    expect.hasAssertions();

    const handleClose: jest.Mock = jest.fn();
    const mockedMutation = graphqlMocked.mutation(
      UPDATE_URL_ROOT_EXCLUDED_SUB_PATHS,
      ({
        variables,
      }): StrictResponse<
        IErrorMessage | { data: UpdateUrlRootExcludedSubPaths }
      > => {
        const { excludedSubPaths, groupName, rootId } = variables;
        if (
          isEqual(excludedSubPaths, []) &&
          groupName === "group" &&
          rootId === "c7118ab2-9345-4398-895f-11ce6e3cc1a6"
        ) {
          return HttpResponse.json({
            data: {
              updateUrlRoot: { success: true },
            },
          });
        }

        return HttpResponse.json({
          errors: [new GraphQLError("Error updating url roots data")],
        });
      },
    );

    render(
      <authzPermissionsContext.Provider
        value={
          new PureAbility([
            { action: "integrates_api_mutations_update_url_root_mutate" },
          ])
        }
      >
        <ExcludedSubPaths
          groupName={"group"}
          initialValues={{
            __typename: "URLRoot",
            host: "",
            id: "c7118ab2-9345-4398-895f-11ce6e3cc1a6",
            nickname: "",
            path: "",
            port: 0,
            protocol: "HTTPS",
            query: "",
            state: "ACTIVE",
          }}
          onClose={handleClose}
        />
      </authzPermissionsContext.Provider>,
      { mocks: [mockedMutation] },
    );

    await waitFor((): void => {
      expect(screen.getByText(/subPath\/1/iu)).toBeInTheDocument();
    });
    await userEvent.click(screen.getByRole("button", { name: "remove-row" }));
    await waitFor((): void => {
      expect(msgSuccess).toHaveBeenCalledWith(
        "group.scope.url.modal.excludedSubPaths.alerts.remove.success",
        "groupAlerts.titleSuccess",
      );
    });

    jest.clearAllMocks();
  });

  it("should add excluded sub-path", async (): Promise<void> => {
    expect.hasAssertions();

    const handleClose: jest.Mock = jest.fn();
    const mockedMutation = graphqlMocked.mutation(
      UPDATE_URL_ROOT_EXCLUDED_SUB_PATHS,
      ({
        variables,
      }): StrictResponse<
        IErrorMessage | { data: UpdateUrlRootExcludedSubPaths }
      > => {
        const { excludedSubPaths, groupName, rootId } = variables;
        if (
          isEqual(excludedSubPaths, ["subPath/1", "subPath/2"]) &&
          groupName === "group" &&
          rootId === "c7118ab2-9345-4398-895f-11ce6e3cc1a6"
        ) {
          return HttpResponse.json({
            data: {
              updateUrlRoot: { success: true },
            },
          });
        }

        return HttpResponse.json({
          errors: [new GraphQLError("Error updating url roots data")],
        });
      },
    );

    render(
      <authzPermissionsContext.Provider
        value={
          new PureAbility([
            { action: "integrates_api_mutations_update_url_root_mutate" },
          ])
        }
      >
        <ExcludedSubPaths
          groupName={"group"}
          initialValues={{
            __typename: "URLRoot",
            host: "",
            id: "c7118ab2-9345-4398-895f-11ce6e3cc1a6",
            nickname: "",
            path: "",
            port: 0,
            protocol: "HTTPS",
            query: "",
            state: "ACTIVE",
          }}
          onClose={handleClose}
        />
      </authzPermissionsContext.Provider>,
      { mocks: [mockedMutation] },
    );

    await waitFor((): void => {
      expect(screen.getByText(/subPath\/1/iu)).toBeInTheDocument();
    });
    await userEvent.click(
      screen.getByRole("button", {
        name: /group\.scope\.url\.modal\.excludedsubpaths\.buttons\.addsubpath/iu,
      }),
    );
    const title = screen.getByText(
      "group.scope.url.modal.excludedSubPaths.addModal.title",
    );

    expect(title).toBeInTheDocument();

    await userEvent.type(
      screen.getByRole("textbox", { name: /subpath/iu }),
      "subPath/2",
    );
    await userEvent.click(screen.getByRole("button", { name: /Confirm/u }));
    await waitFor((): void => {
      expect(msgSuccess).toHaveBeenCalledWith(
        "group.scope.url.modal.excludedSubPaths.alerts.add.success",
        "groupAlerts.titleSuccess",
      );
    });

    expect(title).not.toBeInTheDocument();

    jest.clearAllMocks();
  });

  it("should handle error when adding excluded sub-path", async (): Promise<void> => {
    expect.hasAssertions();

    const handleClose: jest.Mock = jest.fn();

    const mockedMutation = graphqlMocked.mutation(
      UPDATE_URL_ROOT_EXCLUDED_SUB_PATHS,
      ({
        variables,
      }): StrictResponse<
        IErrorMessage | { data: UpdateUrlRootExcludedSubPaths }
      > => {
        const { excludedSubPaths, groupName, rootId } = variables;
        if (
          isEqual(excludedSubPaths, ["subPath/1", "subPath/2"]) &&
          groupName === "group" &&
          rootId === "c7118ab2-9345-4398-895f-11ce6e3cc1a6"
        ) {
          return HttpResponse.json({
            errors: [new GraphQLError("Error updating url roots data")],
          });
        }

        return HttpResponse.json({
          data: {
            updateUrlRoot: { success: true },
          },
        });
      },
    );

    render(
      <authzPermissionsContext.Provider
        value={
          new PureAbility([
            { action: "integrates_api_mutations_update_url_root_mutate" },
          ])
        }
      >
        <ExcludedSubPaths
          groupName={"group"}
          initialValues={{
            __typename: "URLRoot",
            host: "",
            id: "c7118ab2-9345-4398-895f-11ce6e3cc1a6",
            nickname: "",
            path: "",
            port: 0,
            protocol: "HTTPS",
            query: "",
            state: "ACTIVE",
          }}
          onClose={handleClose}
        />
      </authzPermissionsContext.Provider>,
      { mocks: [mockedMutation] },
    );

    await waitFor((): void => {
      expect(screen.getByText(/subPath\/1/iu)).toBeInTheDocument();
    });
    await userEvent.click(
      screen.getByRole("button", {
        name: /group\.scope\.url\.modal\.excludedsubpaths\.buttons\.addsubpath/iu,
      }),
    );
    await userEvent.type(
      screen.getByRole("textbox", { name: /subpath/iu }),
      "subPath/2",
    );
    await userEvent.click(screen.getByRole("button", { name: /Confirm/iu }));
    await waitFor((): void => {
      expect(msgError).toHaveBeenCalledWith("groupAlerts.errorTextsad");
    });

    jest.clearAllMocks();
  });

  it("should handle invalid sub-path error", async (): Promise<void> => {
    expect.hasAssertions();

    const handleClose: jest.Mock = jest.fn();
    const mockedMutation = graphqlMocked.mutation(
      UPDATE_URL_ROOT_EXCLUDED_SUB_PATHS,
      ({
        variables,
      }): StrictResponse<
        IErrorMessage | { data: UpdateUrlRootExcludedSubPaths }
      > => {
        const { excludedSubPaths, groupName, rootId } = variables;
        if (
          isEqual(excludedSubPaths, ["subPath/1", "subPath/3"]) &&
          groupName === "group" &&
          rootId === "c7118ab2-9345-4398-895f-11ce6e3cc1a6"
        ) {
          return HttpResponse.json({
            errors: [new GraphQLError("Exception - The sub-path is not valid")],
          });
        }

        return HttpResponse.json({
          data: {
            updateUrlRoot: { success: true },
          },
        });
      },
    );

    render(
      <authzPermissionsContext.Provider
        value={
          new PureAbility([
            { action: "integrates_api_mutations_update_url_root_mutate" },
          ])
        }
      >
        <ExcludedSubPaths
          groupName={"group"}
          initialValues={{
            __typename: "URLRoot",
            host: "",
            id: "c7118ab2-9345-4398-895f-11ce6e3cc1a6",
            nickname: "",
            path: "",
            port: 0,
            protocol: "HTTPS",
            query: "",
            state: "ACTIVE",
          }}
          onClose={handleClose}
        />
      </authzPermissionsContext.Provider>,
      { mocks: [mockedMutation] },
    );

    await waitFor((): void => {
      expect(screen.getByText(/subPath\/1/iu)).toBeInTheDocument();
    });
    await userEvent.click(
      screen.getByRole("button", {
        name: /group\.scope\.url\.modal\.excludedsubpaths\.buttons\.addsubpath/iu,
      }),
    );
    await userEvent.type(
      screen.getByRole("textbox", { name: /subpath/iu }),
      "subPath/3",
    );
    await userEvent.click(screen.getByRole("button", { name: /Confirm/iu }));
    await waitFor((): void => {
      expect(msgError).toHaveBeenCalledWith(
        "group.scope.url.modal.excludedSubPaths.alerts.add.invalidSubPath",
      );
    });

    jest.clearAllMocks();
  });

  it("should not start with slash", async (): Promise<void> => {
    expect.hasAssertions();

    const handleClose: jest.Mock = jest.fn();

    render(
      <authzPermissionsContext.Provider
        value={
          new PureAbility([
            { action: "integrates_api_mutations_update_url_root_mutate" },
          ])
        }
      >
        <ExcludedSubPaths
          groupName={"group"}
          initialValues={{
            __typename: "URLRoot",
            host: "",
            id: "c7118ab2-9345-4398-895f-11ce6e3cc1a6",
            nickname: "",
            path: "",
            port: 0,
            protocol: "HTTPS",
            query: "",
            state: "ACTIVE",
          }}
          onClose={handleClose}
        />
      </authzPermissionsContext.Provider>,
    );

    await waitFor((): void => {
      expect(screen.getByText(/subPath\/1/iu)).toBeInTheDocument();
    });
    await userEvent.click(
      screen.getByRole("button", {
        name: /group\.scope\.url\.modal\.excludedsubpaths\.buttons\.addsubpath/iu,
      }),
    );
    await userEvent.type(
      screen.getByRole("textbox", { name: /subpath/iu }),
      "/subPath/2",
    );

    expect(
      screen.getByText("validations.invalidStartsWithSlash"),
    ).toBeInTheDocument();

    jest.clearAllMocks();
  });

  it("should not end with slash", async (): Promise<void> => {
    expect.hasAssertions();

    const handleClose: jest.Mock = jest.fn();

    render(
      <authzPermissionsContext.Provider
        value={
          new PureAbility([
            { action: "integrates_api_mutations_update_url_root_mutate" },
          ])
        }
      >
        <ExcludedSubPaths
          groupName={"group"}
          initialValues={{
            __typename: "URLRoot",
            host: "",
            id: "c7118ab2-9345-4398-895f-11ce6e3cc1a6",
            nickname: "",
            path: "",
            port: 0,
            protocol: "HTTPS",
            query: "",
            state: "ACTIVE",
          }}
          onClose={handleClose}
        />
      </authzPermissionsContext.Provider>,
    );

    await waitFor((): void => {
      expect(screen.getByText(/subPath\/1/iu)).toBeInTheDocument();
    });
    await userEvent.click(
      screen.getByRole("button", {
        name: /group\.scope\.url\.modal\.excludedsubpaths\.buttons\.addsubpath/iu,
      }),
    );
    await userEvent.type(
      screen.getByRole("textbox", { name: /subpath/iu }),
      "subPath/2/",
    );

    expect(
      screen.getByText("validations.invalidEndsWithSlash"),
    ).toBeInTheDocument();

    jest.clearAllMocks();
  });
});
