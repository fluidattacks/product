import { useMutation, useQuery } from "@apollo/client";
import { useAbility } from "@casl/react";
import { Button, useConfirmDialog, useModal } from "@fluidattacks/design";
import type { Row } from "@tanstack/react-table";
import isNil from "lodash/isNil";
import { useCallback, useState } from "react";
import * as React from "react";
import { useTranslation } from "react-i18next";

import { ManagementModal } from "./management-modal";

import { DeactivateRootModal } from "../deactivate-root-modal";
import { changeFormatter } from "../formatters";
import { InternalSurfaceButton } from "../internal-surface-button";
import {
  ACTIVATE_ROOT,
  ADD_URL_ROOT,
  GET_URL_ROOTS,
  GET_URL_ROOTS_FRAGMENT,
  UPDATE_URL_ROOT,
} from "../queries";
import type { IURLRootAttr } from "../types";
import { Table } from "components/table";
import { Can } from "context/authz/can";
import { authzPermissionsContext } from "context/authz/config";
import { statusFormatter } from "features/vulnerabilities/formatters/status";
import { getFragmentData } from "gql/fragment-masking";
import { useDebouncedCallback, useTable } from "hooks";
import { useAudit } from "hooks/use-audit";
import { Logger } from "utils/logger";
import { msgError } from "utils/notifications";

interface IURLRootsProps {
  readonly groupName: string;
}
const TIMEOUT_DURATION = 1000;
const DEBOUNCE_DELAY = 800;
export const URLRoots: React.FC<IURLRootsProps> = ({
  groupName,
}): JSX.Element => {
  const permissions = useAbility(authzPermissionsContext);
  const { t } = useTranslation();

  const tableRef = useTable("tblURLRoots");
  const { confirm, ConfirmDialog } = useConfirmDialog();

  const [isManagingRoot, setIsManagingRoot] = useState<
    false | { mode: "ADD" | "EDIT" }
  >(false);
  const [currentRow, setCurrentRow] = useState<IURLRootAttr | undefined>(
    undefined,
  );
  const openAddModal = useCallback((): void => {
    setIsManagingRoot({ mode: "ADD" });
  }, []);
  const closeModal = useCallback((): void => {
    setIsManagingRoot(false);
    setCurrentRow(undefined);
  }, []);

  const [currentRootId, setCurrentRootId] = useState("");
  const deactivateRootModal = useModal("deactivation-root-modal");
  const closeDeactivationModal = useCallback((): void => {
    deactivateRootModal.close();
    setCurrentRootId("");
  }, [deactivateRootModal]);

  const { addAuditEvent } = useAudit();
  // GraphQL operations
  const { data, refetch, fetchMore } = useQuery(GET_URL_ROOTS, {
    fetchPolicy: "network-only",
    nextFetchPolicy: "cache-first",
    onCompleted: (): void => {
      addAuditEvent("Group.Roots", groupName);
    },
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error): void => {
        Logger.error("Couldn't load url roots", error);
      });
    },
    variables: {
      first: 150,
      groupName,
    },
  });
  const onUpdate = useCallback((): void => {
    setTimeout((): void => {
      void refetch();
    }, TIMEOUT_DURATION);
  }, [refetch]);
  const rootsResult = getFragmentData(GET_URL_ROOTS_FRAGMENT, data);
  const [addUrlRoot] = useMutation(ADD_URL_ROOT, {
    onCompleted: (): void => {
      onUpdate();
      closeModal();
    },
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error): void => {
        switch (error.message) {
          case "Exception - Error value is not valid":
            msgError(t("group.scope.url.errors.invalid"));
            break;
          case "Exception - Root with the same URL/branch already exists":
            msgError(t("group.scope.common.errors.duplicateUrl"));
            break;
          case "Exception - Invalid characters":
            msgError(t("group.scope.url.errors.invalidCharacters"));
            break;
          case "Exception - Root with the same nickname already exists":
            msgError(t("group.scope.common.errors.duplicateNickname"));
            break;
          case "Exception - The file has not been found":
            msgError(t("group.scope.common.errors.fileNotFound"));
            break;
          default:
            msgError(t("groupAlerts.errorTextsad"));
            Logger.error("Couldn't add url roots", error);
        }
      });
    },
  });

  const [updateUrlRoot] = useMutation(UPDATE_URL_ROOT, {
    onCompleted: (): void => {
      onUpdate();
      closeModal();
    },
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error): void => {
        if (
          error.message ===
          "Exception - Root with the same nickname already exists"
        ) {
          msgError(t("group.scope.common.errors.duplicateNickname"));
        } else {
          msgError(t("groupAlerts.errorTextsad"));
          Logger.error("Couldn't update url roots", error);
        }
      });
    },
  });

  const handleUrlSubmit = useCallback(
    async ({
      id,
      nickname,
      url,
    }: {
      id: string;
      nickname: string;
      url: string;
    }): Promise<void> => {
      if (isManagingRoot !== false) {
        if (isManagingRoot.mode === "ADD") {
          await addUrlRoot({
            variables: { groupName, nickname, url: url.trim() },
          });
        } else {
          await updateUrlRoot({
            variables: { groupName, nickname, rootId: id },
          });
        }
      }
    },
    [addUrlRoot, groupName, isManagingRoot, updateUrlRoot],
  );

  function handleRowClick(
    rowInfo: Row<IURLRootAttr>,
  ): (event: React.FormEvent) => void {
    return (event: React.FormEvent): void => {
      if (rowInfo.original.state === "ACTIVE") {
        setCurrentRow(rowInfo.original);
        setIsManagingRoot({ mode: "EDIT" });
      }
      event.preventDefault();
    };
  }

  const [activateRoot] = useMutation(ACTIVATE_ROOT, {
    onCompleted: (): void => {
      onUpdate();
    },
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error): void => {
        if (
          error.message ===
          "Exception - Root with the same URL/branch already exists"
        ) {
          msgError(t("group.scope.url.errors.invalid"));
        } else {
          msgError(t("groupAlerts.errorTextsad"));
          Logger.error("Couldn't activate url root", error);
        }
      });
    },
  });

  const canUpdateRootState = permissions.can(
    "integrates_api_mutations_activate_root_mutate",
  );
  const handleStateUpdate = (row: Record<string, string>): void => {
    if (row.state === "ACTIVE") {
      deactivateRootModal.open();
      setCurrentRootId(row.id);
    } else {
      void confirm({
        title: t("group.scope.common.confirm"),
      }).then((confirmModal: boolean): void => {
        if (confirmModal) {
          void activateRoot({ variables: { groupName, id: row.id } });
        }
      });
    }
  };
  const handleSearch = useDebouncedCallback((search: string): void => {
    void refetch({ search });
  }, DEBOUNCE_DELAY);
  const handleNextPage = useCallback(async (): Promise<void> => {
    const rootsInfo = rootsResult?.group;
    const pageInfo = isNil(rootsInfo)
      ? { endCursor: [], hasNextPage: false }
      : {
          endCursor: rootsInfo.urlRoots.pageInfo.endCursor,
          hasNextPage: rootsInfo.urlRoots.pageInfo.hasNextPage,
        };
    if (pageInfo.hasNextPage) {
      await fetchMore({ variables: { after: pageInfo.endCursor } });
    }
  }, [fetchMore, rootsResult]);

  const { total: size, edges } = rootsResult?.group.urlRoots ?? {
    edges: [],
    total: 0,
  };

  return (
    <React.Fragment>
      <Table
        columns={[
          {
            accessorKey: "host",
            header: String(t("group.scope.url.host")),
          },
          {
            accessorKey: "path",
            header: String(t("group.scope.url.path")),
          },
          {
            accessorKey: "port",
            header: String(t("group.scope.url.port")),
          },
          {
            accessorKey: "protocol",
            header: String(t("group.scope.url.protocol")),
          },
          {
            accessorKey: "query",
            header: String(t("group.scope.url.query")),
          },
          {
            accessorKey: "nickname",
            header: String(t("group.scope.ip.nickname")),
          },
          {
            accessorKey: "state",
            cell: (cell): JSX.Element =>
              canUpdateRootState
                ? changeFormatter(
                    "url-root-state",
                    cell.row.original as unknown as Record<string, string>,
                    handleStateUpdate,
                  )
                : statusFormatter(String(cell.getValue())),
            header: String(t("group.scope.common.state")),
          },
        ]}
        data={edges.map((edge): IURLRootAttr => edge.node as IURLRootAttr)}
        onNextPage={handleNextPage}
        onRowClick={
          permissions.can("integrates_api_mutations_update_url_root_mutate")
            ? handleRowClick
            : undefined
        }
        onSearch={handleSearch}
        options={{ enableSorting: true, size }}
        rightSideComponents={
          <React.Fragment>
            <Can do={"integrates_api_mutations_add_url_root_mutate"}>
              <Button icon={"plus"} onClick={openAddModal} variant={"primary"}>
                {t("group.scope.common.add")}
              </Button>
            </Can>
            <InternalSurfaceButton />
            <ConfirmDialog />
          </React.Fragment>
        }
        tableRef={tableRef}
      />
      {isManagingRoot === false ? undefined : (
        <ManagementModal
          groupName={groupName}
          initialValues={
            isManagingRoot.mode === "EDIT" ? currentRow : undefined
          }
          onClose={closeModal}
          onSubmit={handleUrlSubmit}
        />
      )}
      <DeactivateRootModal
        groupName={groupName}
        modalRef={{
          ...deactivateRootModal,
          close: closeDeactivationModal,
        }}
        onUpdate={onUpdate}
        rootId={currentRootId}
      />
      <ConfirmDialog />
    </React.Fragment>
  );
};
