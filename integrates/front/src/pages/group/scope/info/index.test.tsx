import { PureAbility } from "@casl/ability";
import { screen, waitFor } from "@testing-library/react";
import type { UserEvent } from "@testing-library/user-event";
import { userEvent } from "@testing-library/user-event";
import dayjs from "dayjs";
import { GraphQLError } from "graphql";
import isEqual from "lodash/isEqual";
import { HttpResponse, type StrictResponse, graphql } from "msw";
import { Route, Routes } from "react-router-dom";

import { UPDATE_GROUP_INFO } from "../queries";
import { authzPermissionsContext } from "context/authz/config";
import type { UpdateGroupInfoMutation } from "gql/graphql";
import { render } from "mocks";
import { LINK } from "mocks/constants";
import type { IErrorMessage } from "mocks/types";
import { GroupInformation } from "pages/group/scope/info";
import { msgError, msgSuccess } from "utils/notifications";

jest.mock("utils/notifications", (): Record<string, unknown> => {
  const mockedNotifications: Record<string, () => Record<string, unknown>> =
    jest.requireActual("utils/notifications");
  jest.spyOn(mockedNotifications, "msgSuccess").mockImplementation();
  jest.spyOn(mockedNotifications, "msgError").mockImplementation();

  return mockedNotifications;
});

describe("info", (): void => {
  const labels = [
    "organization.tabs.groups.newGroup.businessId.text",
    "organization.tabs.groups.newGroup.businessName.text",
    "organization.tabs.groups.newGroup.description.text",
    "organization.tabs.groups.newGroup.language.text",
    "organization.tabs.groups.newGroup.sprintDuration.text",
    "organization.tabs.groups.editGroup.sprintStartDate.text",
    "organization.tabs.groups.newGroup.managed.text",
  ];

  const textInputsNames = [
    "businessId",
    "businessName",
    "description",
    "sprintDuration",
  ];

  const commonDate = dayjs().subtract(2, "days");
  const date = commonDate.format("YYYY-MM-DD");

  async function fillInputs(
    elements: Element[],
    user: UserEvent,
    inputValue: string,
  ): Promise<void> {
    if (elements.length === 0) return;

    const [firstInput, ...rest] = elements;

    await user.clear(firstInput);
    await userEvent.type(firstInput, inputValue);

    await fillInputs(rest, user, inputValue);
  }

  const graphqlMocked = graphql.link(LINK);

  const mocks = [
    graphqlMocked.mutation(
      UPDATE_GROUP_INFO,
      ({
        variables,
      }): StrictResponse<IErrorMessage | { data: UpdateGroupInfoMutation }> => {
        if (
          isEqual(variables, {
            businessId: "10",
            businessName: "10",
            comments: "",
            description: "10",
            groupName: "test",
            isManagedChanged: false,
            language: "EN",
            managed: "MANAGED",
            sprintDuration: Number("10"),
            sprintStartDate: dayjs(date).toISOString(),
          })
        ) {
          return HttpResponse.json({
            data: {
              updateGroupInfo: { success: true },
            },
          });
        }

        return HttpResponse.json({
          errors: [
            new GraphQLError(
              "Exception - Incorrect change in managed parameter. Please review the payment conditions",
            ),
          ],
        });
      },
      { once: true },
    ),
  ];

  it("should show group info", async (): Promise<void> => {
    expect.hasAssertions();

    render(
      <Routes>
        <Route
          element={<GroupInformation />}
          path={"/orgs/:organizationName/groups/:groupName/scope"}
        />
      </Routes>,
      {
        memoryRouter: {
          initialEntries: ["/orgs/okada/groups/test/scope"],
        },
      },
    );

    await waitFor((): void => {
      labels.forEach((label): void => {
        expect(screen.queryByText(label)).toBeInTheDocument();
      });
    });
    // Has no permission to edit
    document.querySelectorAll("input").forEach((input): void => {
      expect(input).toBeDisabled();
    });
  });

  it("should show group info with permission to edit", async (): Promise<void> => {
    expect.hasAssertions();

    jest.clearAllMocks();

    const mockedPermissions = new PureAbility<string>([
      { action: "integrates_api_mutations_update_group_stakeholder_mutate" },
    ]);

    render(
      <authzPermissionsContext.Provider value={mockedPermissions}>
        <Routes>
          <Route
            element={<GroupInformation />}
            path={"/orgs/:organizationName/groups/:groupName/scope"}
          />
        </Routes>
      </authzPermissionsContext.Provider>,
      {
        memoryRouter: {
          initialEntries: ["/orgs/okada/groups/test/scope"],
        },
      },
    );

    await waitFor((): void => {
      expect(screen.queryByText(labels[0])).toBeInTheDocument();
    });

    document.querySelectorAll("input").forEach((input): void => {
      expect(input).not.toBeDisabled();
    });
  });

  it("should edit group info", async (): Promise<void> => {
    expect.hasAssertions();

    jest.clearAllMocks();

    const mockedPermissions = new PureAbility<string>([
      { action: "integrates_api_mutations_update_group_stakeholder_mutate" },
    ]);

    render(
      <authzPermissionsContext.Provider value={mockedPermissions}>
        <Routes>
          <Route
            element={<GroupInformation />}
            path={"/orgs/:organizationName/groups/:groupName/scope"}
          />
        </Routes>
      </authzPermissionsContext.Provider>,
      {
        memoryRouter: {
          initialEntries: ["/orgs/okada/groups/test/scope"],
        },
        mocks,
      },
    );

    await waitFor((): void => {
      expect(screen.queryByText(labels[0])).toBeInTheDocument();
    });

    const inputs = textInputsNames.map(
      (name): Element => document.querySelectorAll(`input[name="${name}"]`)[0],
    );

    const user = userEvent.setup();
    await fillInputs(inputs, user, "10");

    const [dateInput] = Array.from(
      document.querySelectorAll("input[name='sprintStartDate']"),
    );
    await user.clear(dateInput);

    await userEvent.type(
      screen.getByRole("spinbutton", { name: /^month,/u }),
      date.slice(5, 7),
    );
    await userEvent.type(
      screen.getByRole("spinbutton", { name: /^day,/u }),
      date.slice(8),
    );
    await userEvent.type(
      screen.getByRole("spinbutton", { name: /^year,/u }),
      date.slice(0, 4),
    );

    expect(
      screen.getByText("searchFindings.servicesTable.modal.continue"),
    ).not.toBeDisabled();

    await user.click(
      screen.getByText("searchFindings.servicesTable.modal.continue"),
    );

    expect(msgError).not.toHaveBeenCalled();
    expect(msgSuccess).toHaveBeenCalledTimes(1);
    expect(msgSuccess).toHaveBeenCalledWith(
      "groupAlerts.groupInfoUpdated",
      "groupAlerts.titleSuccess",
    );
  });

  it("should show errors while editing", async (): Promise<void> => {
    expect.hasAssertions();

    jest.clearAllMocks();

    const mockedPermissions = new PureAbility<string>([
      { action: "integrates_api_mutations_update_group_stakeholder_mutate" },
    ]);

    render(
      <authzPermissionsContext.Provider value={mockedPermissions}>
        <Routes>
          <Route
            element={<GroupInformation />}
            path={"/orgs/:organizationName/groups/:groupName/scope"}
          />
        </Routes>
      </authzPermissionsContext.Provider>,
      {
        memoryRouter: {
          initialEntries: ["/orgs/okada/groups/test/scope"],
        },
        mocks,
      },
    );

    await waitFor((): void => {
      expect(screen.queryByText(labels[0])).toBeInTheDocument();
    });

    const inputs = textInputsNames.map(
      (name): Element => document.querySelectorAll(`input[name="${name}"]`)[0],
    );

    const user = userEvent.setup();
    await fillInputs(inputs, user, "0");

    const [dateInput] = Array.from(
      document.querySelectorAll("input[name='sprintStartDate']"),
    );
    await user.clear(dateInput);
    await user.type(dateInput, dayjs().format("MM-DD-YYYY"));

    expect(
      screen.getByText("searchFindings.servicesTable.modal.continue"),
    ).not.toBeDisabled();

    await user.click(
      screen.getByText("searchFindings.servicesTable.modal.continue"),
    );

    expect(
      screen.getByText("This value must be between 1 and 10"),
    ).toBeInTheDocument();

    expect(msgSuccess).not.toHaveBeenCalled();
  });
});
