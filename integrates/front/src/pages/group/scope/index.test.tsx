import { PureAbility } from "@casl/ability";
import { fireEvent, screen, waitFor, within } from "@testing-library/react";
import { userEvent } from "@testing-library/user-event";
import { GraphQLError } from "graphql";
import _ from "lodash";
import { HttpResponse, type StrictResponse, graphql } from "msw";
import { Route, Routes } from "react-router-dom";

import {
  ACTIVATE_ROOT,
  ADD_GIT_ROOT,
  DEACTIVATE_ROOT,
  GET_FILES,
  GET_GIT_ROOTS,
  GET_GIT_ROOTS_QUERIES,
  GET_GIT_ROOT_MANAGEMENT_DETAILS,
  GET_GROUPS,
  GET_GROUP_ACCESS_INFO,
  GET_IP_ROOTS,
  GET_ROOT_VULNS,
  GET_TAGS,
  GET_URL_ROOTS,
  UPDATE_GIT_ROOT,
} from "./queries";

import { GroupScope } from ".";
import {
  authzGroupContext,
  authzPermissionsContext,
} from "context/authz/config";
import type {
  ActivateRootMutation as ActivateRoot,
  AddGitRootMutation as AddGitRoot,
  DeactivateRootMutation as DeactivateRoot,
  GetFilesQueryQuery as GetFiles,
  GetGitRootsQuery,
  GetGitRootsViewQueriesQuery,
  GetGroupsQuery as GetGroups,
  GetIpRootsQuery,
  GetRootVulnsQuery as GetRootVulns,
  GetUrlRootsQuery,
  GetGitRootManagementDetailsQuery as RootManagementDetails,
  UpdateGitRootMutation as UpdateGitRoot,
  UpdateToursMutation as UpdateTours,
} from "gql/graphql";
import {
  CloningStatus,
  ResourceState,
  RootCriticality,
  RootDeactivationReason,
  ServiceType,
  Technique,
  VulnerabilityState,
} from "gql/graphql";
import { UPDATE_TOURS } from "hooks/queries";
import { render as mswRender } from "mocks";
import { LINK } from "mocks/constants";
import type { IErrorMessage, IMessage } from "mocks/types";
import { organizationDataContext } from "pages/organization/context";
import { msgError } from "utils/notifications";

jest.mock("utils/notifications", (): Record<string, unknown> => {
  const mockedNotifications: Record<string, () => Record<string, unknown>> =
    jest.requireActual("utils/notifications");
  jest.spyOn(mockedNotifications, "msgError").mockImplementation();

  return mockedNotifications;
});

describe("groupScope", (): void => {
  // eslint-disable-next-line jest/no-hooks
  beforeEach((): void => {
    jest.spyOn(console, "warn").mockImplementation();
  });

  const btnConfirm = "buttons.confirm";
  const organizationData = {
    hasZtnaRoots: false,
    organizationId: "f0c74b3e-bce4-4946-ba63-cb7e113ee817",
    portfolios: [],
  };
  const scopeQueriesDataMock = {
    group: {
      __typename: "Group",
      gitEnvironmentUrls: [],
      name: "unittesting",
      roots: [
        {
          __typename: "GitRoot" as const,
          branch: "master",
          cloningStatus: {
            __typename: "GitRootCloningStatus" as const,
            message: "root created",
            status: CloningStatus.Cloning,
          },
          createdAt: "2022-02-10T14:58:10+00:00",
          createdBy: "testuser1@test.test",
          credentials: {
            __typename: "Credentials" as const,
            id: "",
            isToken: false,
            name: "",
            type: "",
          },
          criticality: RootCriticality.High,
          gitEnvironmentUrls: [],
          gitignore: [],
          id: "ROOT#4039d098-ffc5-4984-8ed3-eb17bca98e19",
          includesHealthCheck: false,
          lastEditedAt: "2022-10-21T15:58:31+00:00",
          lastEditedBy: "testuser2@test.test",
          nickname: "universe",
          state: ResourceState.Active,
          url: "https://gitlab.com/fluidattacks/universe",
          useEgress: false,
          useVpn: false,
          useZtna: false,
        },
      ],
    },
    organizationId: {
      __typename: "Organization",
      awsExternalId: "6e52c11c-abf7-4ca3-b7d0-635e394f41c1",
      name: "ort-test",
    },
  };
  const memoryUnittesting = {
    initialEntries: ["/orgs/okada/groups/unittesting/scope"],
  };

  const memoryTest = {
    initialEntries: ["/orgs/okada/groups/TEST/scope"],
  };
  const graphqlMocked = graphql.link(LINK);
  const gitRootsDataMock = {
    __typename: "Query" as const,
    group: {
      __typename: "Group" as const,
      gitRoots: {
        __typename: "GitRootsConnection" as const,
        edges: [
          {
            __typename: "GitRootEdge" as const,
            node: {
              __typename: "GitRoot" as const,
              branch: "master",
              cloningStatus: {
                message: "root created",
                status: CloningStatus.Cloning,
              },
              credentials: {
                name: "reattacker (GitLab OAuth)",
              },
              criticality: "HIGH",
              id: "4039d098-ffc5-4984-8ed3-eb17bca98e19",
              includesHealthCheck: false,
              nickname: "universe",
              state: "ACTIVE",
              url: "https://gitlab.com/fluidattacks/universe",
            },
          },
        ],
        pageInfo: {
          endCursor:
            "ROOT#8493c82f-2860-4902-86fa-75b0fef76034#GROUP#unittesting",
          hasNextPage: false,
        },
        total: 1,
      },
      name: "unittesting",
    },
  };

  const gitRootsQueriesDataMock = {
    group: {
      __typename: "Group" as const,
      gitEnvironmentUrls: [
        {
          __typename: "GitEnvironmentUrl" as const,
          cloudName: null,
          createdAt: null,
          createdBy: null,
          hasSecrets: true,
          id: "00fbe3579a253b43239954a545dc0536e6c83094",
          include: false,
          root: {
            id: "4039d098-ffc5-4984-8ed3-eb17bca98e19",
            state: "ACTIVE",
            url: "https://gitlab.com/fluidattacks/universe.git",
          },
          rootId: "4039d098-ffc5-4984-8ed3-eb17bca98e19",
          url: "https://app.fluidattacks.com/test",
          urlType: "URL",
          useEgress: false,
          useVpn: false,
          useZtna: false,
        },
      ],
      name: "unittesting",
    },
    organizationId: {
      __typename: "Organization" as const,
      awsExternalId: "6e52c11c-abf7-4ca3-b7d0-635e394f41c1",
      name: "ort-test",
    },
  };
  const initialUrlRootsMockGlobal = graphqlMocked.query(
    GET_URL_ROOTS,
    (): StrictResponse<{ data: GetUrlRootsQuery }> => {
      return HttpResponse.json({
        data: {
          __typename: "Query",
          ...{
            group: {
              __typename: "Group",
              name: "unittesting",
              urlRoots: {
                __typename: "URLRootsConnection",
                edges: [
                  {
                    __typename: "URLRootEdge",
                    node: {
                      __typename: "URLRoot",
                      host: "app.fluidattacks.com",
                      id: "8493c82f-2860-4902-86fa-75b0fef76034",
                      nickname: "url_root_1",
                      path: "/",
                      port: 443,
                      protocol: "HTTPS",
                      query: null,
                      state: "ACTIVE",
                    },
                  },
                ],
                pageInfo: {
                  endCursor:
                    "ROOT#8493c82f-2860-4902-86fa-75b0fef76034#GROUP#unittesting",
                  hasNextPage: false,
                },
                total: 1,
              },
            },
          },
        },
      });
    },
    { once: true },
  );
  const initialIPRootsMockGlobal = graphqlMocked.query(
    GET_IP_ROOTS,
    (): StrictResponse<{ data: GetIpRootsQuery }> => {
      return HttpResponse.json({
        data: {
          __typename: "Query",
          ...{
            group: {
              __typename: "Group",
              ipRoots: {
                __typename: "IPRootsConnection",
                edges: [
                  {
                    __typename: "IPRootEdge",
                    node: {
                      __typename: "IPRoot",
                      address: "127.0.0.1",
                      host: "app.fluidattacks.com",
                      id: "d312f0b9-da49-4d2b-a881-bed438875e99",
                      nickname: "ip_root_1",
                      state: "ACTIVE",
                    },
                  },
                ],
                pageInfo: {
                  endCursor:
                    "ROOT#2c0dfd9a-486d-41e5-bc5a-64be1bcfb704#GROUP#unittesting",
                  hasNextPage: false,
                },
                total: 1,
              },
              name: "unittesting",
            },
          },
        },
      });
    },
    { once: true },
  );
  const initialGitRootsMockGlobal = graphqlMocked.query(
    GET_GIT_ROOTS,
    (): StrictResponse<{ data: GetGitRootsQuery }> => {
      return HttpResponse.json({
        data: {
          __typename: "Query",
          ...{
            group: {
              ...gitRootsDataMock.group,
              gitRoots: {
                ...gitRootsDataMock.group.gitRoots,
                edges: [],
              },
            },
          },
        },
      });
    },
    { once: true },
  );
  const finalGitRootsMockGlobal = graphqlMocked.query(
    GET_GIT_ROOTS,
    (): StrictResponse<{ data: GetGitRootsQuery }> => {
      return HttpResponse.json({
        data: {
          ...gitRootsDataMock,
        },
      });
    },
    { once: true },
  );
  const initialGitRootsQueriesMockGlobal = graphqlMocked.query(
    GET_GIT_ROOTS_QUERIES,
    (): StrictResponse<{ data: GetGitRootsViewQueriesQuery }> => {
      return HttpResponse.json({
        data: {
          __typename: "Query",
          ...{
            group: {
              __typename: "Group",
              gitEnvironmentUrls: [],
              name: "unittesting",
            },
          },
          ...{ organizationId: gitRootsQueriesDataMock.organizationId },
        },
      });
    },
    { once: true },
  );
  const finalGitRootsQueriesMockGlobal = graphqlMocked.query(
    GET_GIT_ROOTS_QUERIES,
    (): StrictResponse<{ data: GetGitRootsViewQueriesQuery }> => {
      return HttpResponse.json({
        data: {
          __typename: "Query",
          ...{ group: gitRootsQueriesDataMock.group },
          ...{ organizationId: gitRootsQueriesDataMock.organizationId },
        },
      });
    },
    { once: true },
  );
  const mocks = [
    graphqlMocked.mutation(
      UPDATE_TOURS,
      ({
        variables,
      }): StrictResponse<IErrorMessage | { data: UpdateTours }> => {
        const { newGroup, newRoot, welcome } = variables;
        if (newGroup && newRoot && welcome) {
          return HttpResponse.json({
            data: {
              updateTours: { success: true },
            },
          });
        }

        return HttpResponse.json({
          errors: [new GraphQLError("Error updating tours")],
        });
      },
    ),
    initialGitRootsQueriesMockGlobal,
    initialGitRootsMockGlobal,
  ];

  it("should render git roots", async (): Promise<void> => {
    expect.hasAssertions();

    const finalRootsQueryMock = graphqlMocked.query(
      GET_GIT_ROOTS,
      (): StrictResponse<{ data: GetGitRootsQuery }> => {
        return HttpResponse.json({
          data: {
            __typename: "Query",
            ...{
              group: {
                ...gitRootsDataMock.group,
                gitRoots: {
                  ...gitRootsDataMock.group.gitRoots,
                  edges: [
                    {
                      node: {
                        ...gitRootsDataMock.group.gitRoots.edges[0].node,
                        includesHealthCheck: true,
                      },
                    },
                  ],
                },
              },
            },
          },
        });
      },
      { once: true },
    );

    mswRender(
      <authzGroupContext.Provider
        value={new PureAbility([{ action: "has_service_white" }])}
      >
        <organizationDataContext.Provider value={organizationData}>
          <Routes>
            <Route
              element={<GroupScope />}
              path={"/orgs/:organizationName/groups/:groupName/scope"}
            />
          </Routes>
        </organizationDataContext.Provider>
      </authzGroupContext.Provider>,
      {
        memoryRouter: memoryUnittesting,
        mocks: [finalGitRootsQueriesMockGlobal, finalRootsQueryMock],
      },
    );

    await waitFor((): void => {
      expect(
        within(screen.queryAllByRole("table")[0]).queryByText(
          "table.noDataIndication",
        ),
      ).not.toBeInTheDocument();
    });

    expect(
      within(screen.queryAllByRole("table")[0]).queryAllByRole("row"),
    ).toHaveLength(2);

    await waitFor((): void => {
      expect(
        within(screen.queryAllByRole("table")[0]).queryAllByRole("row")[1]
          .textContent,
      ).toStrictEqual(
        [
          // Url
          "https://gitlab.com/fluidattacks/universe",
          // Branch
          "master",
          // State
          "Active",
          // Criticality
          "High",
          // Cloning status
          "Cloning",
          // Root nickname
          "universe",
          // HealthCheck
          "group.scope.git.healthCheck.yes",
        ].join(""),
      );
    });
  });

  it("should render URL roots", async (): Promise<void> => {
    expect.hasAssertions();

    mswRender(
      <authzGroupContext.Provider
        value={new PureAbility([{ action: "has_service_black" }])}
      >
        <organizationDataContext.Provider value={organizationData}>
          <Routes>
            <Route
              element={<GroupScope />}
              path={"/orgs/:organizationName/groups/:groupName/scope"}
            />
          </Routes>
        </organizationDataContext.Provider>
      </authzGroupContext.Provider>,
      {
        memoryRouter: memoryUnittesting,
        mocks: [
          finalGitRootsQueriesMockGlobal,
          initialIPRootsMockGlobal,
          initialUrlRootsMockGlobal,
        ],
      },
    );

    await waitFor((): void => {
      expect(
        within(screen.queryAllByRole("table")[1]).queryByText(
          "table.noDataIndication",
        ),
      ).toBeInTheDocument();
    });

    expect(
      within(screen.queryAllByRole("table")[1]).queryAllByRole("row"),
    ).toHaveLength(2);

    await waitFor((): void => {
      expect(
        within(screen.queryAllByRole("table")[1]).queryAllByRole("row")[1]
          .textContent,
      ).toStrictEqual(
        [
          // Host
          "app.fluidattacks.com/",
          // Port
          "443",
          // Protocol
          "HTTPS",
          // Root nickname
          "url_root_1",
          // State
          "Active",
        ].join(""),
      );
    });
  });

  it("should add git roots with user credentials", async (): Promise<void> => {
    expect.hasAssertions();

    const mutationMock = graphqlMocked.mutation(
      ADD_GIT_ROOT,
      ({ variables }): StrictResponse<IErrorMessage | { data: AddGitRoot }> => {
        const {
          branch,
          credentials,
          gitignore,
          groupName,
          includesHealthCheck,
          nickname,
          url,
          useEgress,
          useVpn,
          useZtna,
        } = variables;

        if (
          branch === "master" &&
          _.isEqual(credentials, {
            arn: "",
            isPat: false,
            name: "credential name",
            password: "password-test",
            token: "",
            type: "HTTPS",
            user: "user-test",
          }) &&
          _.isEqual(gitignore, []) &&
          groupName === "unittesting" &&
          !includesHealthCheck &&
          nickname === "" &&
          url === "https://gitlab.com/fluidattacks/universe" &&
          !useZtna &&
          !useVpn &&
          !useEgress
        ) {
          return HttpResponse.json({
            data: {
              addGitRoot: { __typename: "AddRootPayload", success: true },
            },
          });
        }

        return HttpResponse.json({
          errors: [new GraphQLError("Error adding roots")],
        });
      },
    );
    jest.clearAllMocks();

    mswRender(
      <authzGroupContext.Provider
        value={
          new PureAbility([
            { action: "has_service_white" },
            { action: "has_advanced" },
          ])
        }
      >
        <authzPermissionsContext.Provider
          value={
            new PureAbility([
              { action: "integrates_api_mutations_add_git_root_mutate" },
              { action: "integrates_api_mutations_add_secret_mutate" },
              { action: "integrates_api_mutations_update_git_root_mutate" },
            ])
          }
        >
          <organizationDataContext.Provider value={organizationData}>
            <Routes>
              <Route
                element={<GroupScope />}
                path={"/orgs/:organizationName/groups/:groupName/scope"}
              />
            </Routes>
          </organizationDataContext.Provider>
        </authzPermissionsContext.Provider>
      </authzGroupContext.Provider>,
      {
        memoryRouter: memoryUnittesting,
        mocks: [
          initialGitRootsQueriesMockGlobal,
          initialGitRootsMockGlobal,
          mutationMock,
          finalGitRootsQueriesMockGlobal,
          finalGitRootsMockGlobal,
        ],
      },
    );
    const role = "combobox";

    await waitFor((): void => {
      expect(
        screen.queryAllByRole("button", { name: "group.scope.common.add" }),
      ).toHaveLength(1);
    });

    await userEvent.click(
      screen.queryAllByRole("button", { name: "group.scope.common.add" })[0],
    );

    await userEvent.click(
      screen.getByText("components.repositoriesDropdown.manual.text"),
    );

    expect(screen.queryByRole(role, { name: "url" })).toBeInTheDocument();
    expect(screen.getByText(btnConfirm)).toBeDisabled();

    await userEvent.type(
      screen.getByRole(role, { name: "url" }),
      "https://gitlab.com/fluidattacks/universe",
    );
    await userEvent.type(screen.getByRole(role, { name: "branch" }), "master");

    expect(
      screen.queryByRole("textbox", { name: "credentials.key" }),
    ).not.toBeInTheDocument();
    expect(
      screen.queryByRole("textbox", { name: "credentials.token" }),
    ).not.toBeInTheDocument();
    expect(
      screen.queryByRole("textbox", {
        name: "credentials.azureOrganization",
      }),
    ).not.toBeInTheDocument();

    expect(
      screen.getByText("group.scope.git.repo.credentials.userHttps"),
    ).toBeInTheDocument();

    expect(
      screen.queryByRole(role, { name: "credentials.user" }),
    ).toBeInTheDocument();
    expect(
      screen.queryByRole(role, { name: "credentials.password" }),
    ).toBeInTheDocument();

    await userEvent.type(
      screen.getByRole(role, { name: "credentials.name" }),
      "credential name",
    );

    await userEvent.type(
      screen.getByRole(role, { name: "credentials.user" }),
      "user-test",
    );

    await userEvent.type(
      screen.getByRole(role, { name: "credentials.password" }),
      "password-test",
    );

    await userEvent.click(screen.getByRole("radio", { name: "No" }));
    const healthCheckCheckboxes = 3;
    const secureConnectionCheckboxes = 2;

    expect(screen.queryAllByRole("checkbox", { checked: false })).toHaveLength(
      healthCheckCheckboxes + secureConnectionCheckboxes,
    );

    await userEvent.click(screen.getByDisplayValue("rejectA"));
    await userEvent.click(screen.getByDisplayValue("rejectB"));
    await userEvent.click(screen.getByDisplayValue("rejectC"));

    expect(screen.queryAllByRole("checkbox", { checked: true })).toHaveLength(
      healthCheckCheckboxes,
    );

    await waitFor((): void => {
      expect(screen.getByText(btnConfirm)).not.toBeDisabled();
    });
    await userEvent.click(screen.getByText(btnConfirm));
    await waitFor(
      (): void => {
        expect(
          within(screen.queryAllByRole("table")[0]).queryByText(
            "table.noDataIndication",
          ),
        ).not.toBeInTheDocument();
      },
      { timeout: 6000 },
    );

    await waitFor((): void => {
      expect(screen.queryAllByRole("row")[1].textContent).toStrictEqual(
        [
          // Url
          "https://gitlab.com/fluidattacks/universe",
          // Branch
          "master",
          // State
          "Active",
          // Criticality
          "High",
          // Cloning status
          "Cloning",
          // Root nickname
          "universe",
          // HealthCheck
          "group.scope.git.healthCheck.no",
        ].join(""),
      );
    });
  }, 60000);

  it("should add git roots with ssh credentials", async (): Promise<void> => {
    expect.hasAssertions();

    const mutationMock = graphqlMocked.mutation(
      ADD_GIT_ROOT,
      ({ variables }): StrictResponse<IErrorMessage | { data: AddGitRoot }> => {
        const {
          branch,
          credentials,
          gitignore,
          groupName,
          includesHealthCheck,
          nickname,
          url,
          useEgress,
          useVpn,
          useZtna,
        } = variables;

        if (
          branch === "master" &&
          _.isEqual(credentials, {
            arn: "",
            isPat: false,
            key: "LS0tLS1CRUdJTiBPUEVOU1NIIFBSSVZBVEUgS0VZLS0tLS0KdGVzdAotLS0tLUVORCBPUEVOU1NIIFBSSVZBVEUgS0VZLS0tLS0=",
            name: "credential name",
            password: "",
            token: "",
            type: "SSH",
            user: "",
          }) &&
          _.isEqual(gitignore, []) &&
          groupName === "unittesting" &&
          !includesHealthCheck &&
          nickname === "" &&
          url === "ssh://git@gitlab.com:fluidattacks/universe.git" &&
          !useZtna &&
          !useVpn &&
          !useEgress
        ) {
          return HttpResponse.json({
            data: {
              addGitRoot: { __typename: "AddRootPayload", success: true },
            },
          });
        }

        return HttpResponse.json({
          errors: [new GraphQLError("Error adding roots")],
        });
      },
    );
    const finalGitRootsQueryMock = graphqlMocked.query(
      GET_GIT_ROOTS,
      (): StrictResponse<{ data: GetGitRootsQuery }> => {
        return HttpResponse.json({
          data: {
            __typename: "Query",
            ...{
              group: {
                ...gitRootsDataMock.group,
                gitRoots: {
                  ...gitRootsDataMock.group.gitRoots,
                  edges: [
                    {
                      node: {
                        ...gitRootsDataMock.group.gitRoots.edges[0].node,
                        url: "ssh://git@gitlab.com:fluidattacks/universe.git",
                      },
                    },
                  ],
                },
              },
            },
          },
        });
      },
      { once: true },
    );

    jest.clearAllMocks();

    mswRender(
      <authzGroupContext.Provider
        value={new PureAbility([{ action: "has_service_white" }])}
      >
        <authzPermissionsContext.Provider
          value={
            new PureAbility([
              { action: "integrates_api_mutations_add_git_root_mutate" },
              { action: "integrates_api_mutations_add_secret_mutate" },
              { action: "integrates_api_mutations_update_git_root_mutate" },
            ])
          }
        >
          <organizationDataContext.Provider value={organizationData}>
            <Routes>
              <Route
                element={<GroupScope />}
                path={"/orgs/:organizationName/groups/:groupName/scope"}
              />
            </Routes>
          </organizationDataContext.Provider>
        </authzPermissionsContext.Provider>
      </authzGroupContext.Provider>,
      {
        memoryRouter: memoryUnittesting,
        mocks: [
          initialGitRootsQueriesMockGlobal,
          initialGitRootsMockGlobal,
          mutationMock,
          finalGitRootsQueryMock,
          finalGitRootsQueriesMockGlobal,
        ],
      },
    );
    const role = "combobox";

    await waitFor((): void => {
      expect(
        screen.queryAllByRole("button", { name: "group.scope.common.add" }),
      ).toHaveLength(1);
    });

    await userEvent.click(
      screen.queryAllByRole("button", { name: "group.scope.common.add" })[0],
    );

    await userEvent.click(
      screen.getByText("components.repositoriesDropdown.manual.text"),
    );

    expect(screen.queryByRole(role, { name: "url" })).toBeInTheDocument();
    expect(screen.getByText(btnConfirm)).toBeDisabled();

    await userEvent.type(
      screen.getByRole(role, { name: "url" }),
      "git@gitlab.com:fluidattacks/universe.git",
    );
    await userEvent.type(screen.getByRole(role, { name: "branch" }), "master");

    expect(
      screen.getByText("group.scope.git.repo.credentials.ssh"),
    ).toBeInTheDocument();

    expect(
      screen.queryByRole("textbox", { name: "credentials.key" }),
    ).toBeInTheDocument();
    expect(
      screen.queryByRole(role, { name: "credentials.password" }),
    ).not.toBeInTheDocument();
    expect(
      screen.queryByRole(role, { name: "credentials.user" }),
    ).not.toBeInTheDocument();
    expect(
      screen.queryByRole("textbox", { name: "credentials.token" }),
    ).not.toBeInTheDocument();
    expect(
      screen.queryByRole("textbox", {
        name: "credentials.azureOrganization",
      }),
    ).not.toBeInTheDocument();

    await userEvent.clear(screen.getByRole(role, { name: "credentials.name" }));
    await userEvent.type(
      screen.getByRole(role, { name: "credentials.name" }),
      "credential name",
    );
    await userEvent.clear(
      screen.getByRole("textbox", { name: "credentials.key" }),
    );
    await userEvent.type(
      screen.getByRole("textbox", { name: "credentials.key" }),
      "-----BEGIN OPENSSH PRIVATE KEY-----\ntest\n-----END OPENSSH PRIVATE KEY-----",
    );

    await waitFor((): void => {
      expect(screen.getByText(btnConfirm)).not.toBeDisabled();
    });
    await userEvent.click(screen.getByText(btnConfirm));

    await waitFor(
      (): void => {
        expect(
          within(screen.queryAllByRole("table")[0]).queryByText(
            "table.noDataIndication",
          ),
        ).not.toBeInTheDocument();
      },
      { timeout: 6000 },
    );

    await waitFor((): void => {
      expect(screen.queryAllByRole("row")[1].textContent).toStrictEqual(
        [
          // Url
          "ssh://git@gitlab.com:fluidattacks/universe.git",
          // Branch
          "master",
          // State
          "Active",
          // Criticality
          "High",
          // Cloning status
          "Cloning",
          // Root nickname
          "universe",
          // HealthCheck
          "group.scope.git.healthCheck.no",
        ].join(""),
      );
    });
  }, 60000);

  it("should update git roots", async (): Promise<void> => {
    expect.hasAssertions();

    const initialQueryMocks = [
      initialGitRootsQueriesMockGlobal,
      graphqlMocked.query(
        GET_GIT_ROOTS,
        (): StrictResponse<{ data: GetGitRootsQuery }> => {
          return HttpResponse.json({
            data: {
              __typename: "Query",
              ...{
                group: {
                  ...gitRootsDataMock.group,
                  gitRoots: {
                    ...gitRootsDataMock.group.gitRoots,
                    edges: [
                      {
                        node: {
                          ...gitRootsDataMock.group.gitRoots.edges[0].node,
                          gitignore: ["bower_components/*"],
                        },
                      },
                    ],
                  },
                },
              },
            },
          });
        },
        { once: true },
      ),
      graphqlMocked.query(
        GET_GIT_ROOT_MANAGEMENT_DETAILS,
        (): StrictResponse<{
          data: RootManagementDetails;
        }> => {
          return HttpResponse.json({
            data: {
              __typename: "Query",
              root: {
                ...scopeQueriesDataMock.group.roots[0],
                gitignore: ["bower_components/*"],
              },
            },
          });
        },
      ),
    ];
    const mutationMock = graphqlMocked.mutation(
      UPDATE_GIT_ROOT,
      ({
        variables,
      }): StrictResponse<IErrorMessage | { data: UpdateGitRoot }> => {
        const {
          branch,
          credentials,
          gitignore,
          id,
          groupName,
          includesHealthCheck,
          nickname,
          url,
          useEgress,
          useVpn,
          useZtna,
        } = variables;
        if (
          branch === "master" &&
          credentials === undefined &&
          _.isEqual(gitignore, ["node_modules/*"]) &&
          groupName === "unittesting" &&
          id === "ROOT#4039d098-ffc5-4984-8ed3-eb17bca98e19" &&
          includesHealthCheck &&
          nickname === "universe" &&
          url === "https://gitlab.com/fluidattacks/universe" &&
          !useZtna &&
          !useVpn &&
          !useEgress
        ) {
          return HttpResponse.json({
            data: {
              updateGitRoot: { __typename: "SimplePayload", success: true },
            },
          });
        }

        return HttpResponse.json({
          errors: [new GraphQLError("Error adding roots")],
        });
      },
    );

    mswRender(
      <authzGroupContext.Provider
        value={
          new PureAbility([
            { action: "has_service_white" },
            { action: "has_advanced" },
          ])
        }
      >
        <authzPermissionsContext.Provider
          value={
            new PureAbility([
              { action: "integrates_api_mutations_add_git_root_mutate" },
              { action: "integrates_api_mutations_update_git_root_mutate" },
              { action: "update_git_root_filter" },
            ])
          }
        >
          <organizationDataContext.Provider value={organizationData}>
            <Routes>
              <Route
                element={<GroupScope />}
                path={"/orgs/:organizationName/groups/:groupName/scope"}
              />
            </Routes>
          </organizationDataContext.Provider>
        </authzPermissionsContext.Provider>
      </authzGroupContext.Provider>,
      {
        memoryRouter: memoryUnittesting,
        mocks: [
          ...initialQueryMocks,
          mutationMock,
          finalGitRootsQueriesMockGlobal,
          finalGitRootsMockGlobal,
        ],
      },
    );

    await waitFor((): void => {
      expect(
        within(screen.queryAllByRole("table")[0]).queryByText(
          "table.noDataIndication",
        ),
      ).not.toBeInTheDocument();
    });

    expect(
      within(screen.queryAllByRole("table")[0]).queryAllByRole("row"),
    ).toHaveLength(2);

    await userEvent.click(
      screen.getByRole("cell", {
        name: "https://gitlab.com/fluidattacks/universe",
      }),
    );

    await waitFor((): void => {
      expect(screen.getByText("group.scope.common.edit")).toBeInTheDocument();
    });

    await waitFor((): void => {
      expect(screen.queryByText(btnConfirm)).toBeInTheDocument();
    });

    expect(screen.getByText(btnConfirm)).toBeDisabled();

    await waitFor((): void => {
      expect(
        screen.queryAllByRole("radio", { checked: true, name: "Yes" }),
      ).toHaveLength(1);
    });
    await userEvent.clear(
      screen.getByRole("combobox", { name: "gitignore.0" }),
    );
    await userEvent.type(
      screen.getByRole("combobox", { name: "gitignore.0" }),
      "node_modules/*",
    );
    await waitFor((): void => {
      expect(
        screen.queryAllByRole("radio", { checked: true, name: "No" }),
      ).toHaveLength(1);
    });
    await userEvent.click(screen.getAllByRole("radio", { name: "Yes" })[1]);
    await waitFor((): void => {
      expect(
        screen.queryAllByRole("radio", { checked: true, name: "Yes" }),
      ).toHaveLength(2);
    });
    await waitFor((): void => {
      expect(
        screen.queryAllByRole("radio", { checked: true, name: "No" }),
      ).toHaveLength(0);
    });
    await waitFor((): void => {
      expect(
        screen.queryAllByRole("checkbox", { checked: false }),
      ).toHaveLength(3);
    });
    await userEvent.click(screen.getByDisplayValue("includeA"));
    await waitFor((): void => {
      expect(screen.queryAllByRole("checkbox", { checked: true })).toHaveLength(
        1,
      );
    });
    await waitFor((): void => {
      expect(screen.getByText(btnConfirm)).not.toBeDisabled();
    });
    await userEvent.click(screen.getByText(btnConfirm));
    await waitFor((): void => {
      expect(screen.queryAllByRole("row")[1].textContent).toStrictEqual(
        [
          // Url
          "https://gitlab.com/fluidattacks/universe",
          // Branch
          "master",
          // State
          "Active",
          // Criticality
          "High",
          // Cloning status
          "Cloning",
          // Root nickname
          "universe",
          // HeathCheck
          "group.scope.git.healthCheck.no",
        ].join(""),
      );
    });
  });

  it("should activate root", async (): Promise<void> => {
    expect.hasAssertions();

    const initialGitRootsQueryMock = graphqlMocked.query(
      GET_GIT_ROOTS,
      (): StrictResponse<{ data: GetGitRootsQuery }> => {
        return HttpResponse.json({
          data: {
            __typename: "Query",
            ...{
              group: {
                ...gitRootsDataMock.group,
                gitRoots: {
                  ...gitRootsDataMock.group.gitRoots,
                  edges: [
                    {
                      node: {
                        ...gitRootsDataMock.group.gitRoots.edges[0].node,
                        state: ResourceState.Inactive,
                      },
                    },
                  ],
                },
              },
            },
          },
        });
      },
      { once: true },
    );
    const mutationMock = graphqlMocked.mutation(
      ACTIVATE_ROOT,
      ({
        variables,
      }): StrictResponse<IErrorMessage | { data: ActivateRoot }> => {
        const { groupName, id } = variables;
        if (
          groupName === "unittesting" &&
          id === "4039d098-ffc5-4984-8ed3-eb17bca98e19"
        ) {
          return HttpResponse.json({
            data: {
              activateRoot: { __typename: "SimplePayload", success: true },
            },
          });
        }

        return HttpResponse.json({
          errors: [new GraphQLError("Error adding roots")],
        });
      },
    );

    mswRender(
      <authzGroupContext.Provider
        value={new PureAbility([{ action: "has_service_white" }])}
      >
        <authzPermissionsContext.Provider
          value={
            new PureAbility<string>([
              { action: "integrates_api_mutations_activate_root_mutate" },
            ])
          }
        >
          <organizationDataContext.Provider value={organizationData}>
            <Routes>
              <Route
                element={<GroupScope />}
                path={"/orgs/:organizationName/groups/:groupName/scope"}
              />
            </Routes>
          </organizationDataContext.Provider>
        </authzPermissionsContext.Provider>
      </authzGroupContext.Provider>,
      {
        memoryRouter: memoryUnittesting,
        mocks: [
          initialGitRootsQueriesMockGlobal,
          initialGitRootsQueryMock,
          mutationMock,
          finalGitRootsQueriesMockGlobal,
          finalGitRootsMockGlobal,
        ],
      },
    );

    await waitFor(
      (): void => {
        expect(
          within(screen.queryAllByRole("table")[0]).queryByText(
            "table.noDataIndication",
          ),
        ).not.toBeInTheDocument();
      },
      {
        timeout: 5000,
      },
    );

    expect(
      within(screen.queryAllByRole("table")[0]).queryAllByRole("row"),
    ).toHaveLength(2);
    expect(
      screen.queryByText("group.scope.common.confirm"),
    ).not.toBeInTheDocument();

    expect(
      within(screen.queryAllByRole("table")[0]).getByRole<HTMLInputElement>(
        "checkbox",
      ).checked,
    ).toBe(false);

    fireEvent.click(
      within(screen.queryAllByRole("table")[0]).getByRole("checkbox"),
    );
    await waitFor((): void => {
      expect(
        screen.getByText(/group.scope.common.confirm/iu),
      ).toBeInTheDocument();
    });
    await waitFor((): void => {
      expect(
        screen.getByRole("button", { name: /confirm/iu }),
      ).toBeInTheDocument();
    });
    fireEvent.click(screen.getByText("Confirm"));
    await waitFor((): void => {
      expect(
        within(screen.queryAllByRole("table")[0]).getByRole("checkbox"),
      ).toBeChecked();
    });
  });

  it("should handle error when activate root", async (): Promise<void> => {
    expect.hasAssertions();

    const initialGitRootsQueryMock = graphqlMocked.query(
      GET_GIT_ROOTS,
      (): StrictResponse<{ data: GetGitRootsQuery }> => {
        return HttpResponse.json({
          data: {
            __typename: "Query",
            ...{
              group: {
                ...gitRootsDataMock.group,
                gitRoots: {
                  ...gitRootsDataMock.group.gitRoots,
                  edges: [
                    {
                      node: {
                        ...gitRootsDataMock.group.gitRoots.edges[0].node,
                        state: ResourceState.Inactive,
                      },
                    },
                  ],
                },
              },
            },
          },
        });
      },
      { once: true },
    );
    const mutationMock = graphqlMocked.mutation(
      ACTIVATE_ROOT,
      ({
        variables,
      }): StrictResponse<IErrorMessage | { data: ActivateRoot }> => {
        const { groupName, id } = variables;
        if (
          groupName === "unittesting" &&
          id === "4039d098-ffc5-4984-8ed3-eb17bca98e19"
        ) {
          return HttpResponse.json({
            errors: [new GraphQLError("Couldn't activate root")],
          });
        }

        return HttpResponse.json({
          data: {
            activateRoot: { __typename: "SimplePayload", success: true },
          },
        });
      },
    );

    mswRender(
      <authzGroupContext.Provider
        value={new PureAbility([{ action: "has_service_white" }])}
      >
        <authzPermissionsContext.Provider
          value={
            new PureAbility<string>([
              { action: "integrates_api_mutations_activate_root_mutate" },
            ])
          }
        >
          <organizationDataContext.Provider value={organizationData}>
            <Routes>
              <Route
                element={<GroupScope />}
                path={"/orgs/:organizationName/groups/:groupName/scope"}
              />
            </Routes>
          </organizationDataContext.Provider>
        </authzPermissionsContext.Provider>
      </authzGroupContext.Provider>,
      {
        memoryRouter: memoryUnittesting,
        mocks: [
          initialGitRootsQueriesMockGlobal,
          initialGitRootsQueryMock,
          mutationMock,
          finalGitRootsQueriesMockGlobal,
          finalGitRootsMockGlobal,
        ],
      },
    );

    await waitFor(
      (): void => {
        expect(
          within(screen.queryAllByRole("table")[0]).queryByText(
            "table.noDataIndication",
          ),
        ).not.toBeInTheDocument();
      },
      {
        timeout: 5000,
      },
    );

    expect(
      within(screen.queryAllByRole("table")[0]).queryAllByRole("row"),
    ).toHaveLength(2);
    expect(
      screen.queryByText("group.scope.common.confirm"),
    ).not.toBeInTheDocument();

    expect(
      within(screen.queryAllByRole("table")[0]).getByRole<HTMLInputElement>(
        "checkbox",
      ).checked,
    ).toBe(false);

    fireEvent.click(
      within(screen.queryAllByRole("table")[0]).getByRole("checkbox"),
    );
    await waitFor((): void => {
      expect(
        screen.getByText(/group.scope.common.confirm/iu),
      ).toBeInTheDocument();
    });
    await waitFor((): void => {
      expect(
        screen.getByRole("button", { name: /confirm/iu }),
      ).toBeInTheDocument();
    });
    fireEvent.click(screen.getByText("Confirm"));
    await waitFor((): void => {
      expect(msgError).toHaveBeenCalledWith("groupAlerts.errorTextsad");
    });

    await waitFor((): void => {
      expect(msgError).toHaveBeenCalledTimes(3);
    });
  });

  it("should deactivate root with reason", async (): Promise<void> => {
    expect.hasAssertions();

    const mutationMock = graphqlMocked.mutation(
      DEACTIVATE_ROOT,
      ({
        variables,
      }): StrictResponse<IErrorMessage | { data: DeactivateRoot }> => {
        const { groupName, id, other, reason: reasonFromVariables } = variables;
        if (
          groupName === "unittesting" &&
          id === "4039d098-ffc5-4984-8ed3-eb17bca98e19" &&
          other === "" &&
          reasonFromVariables === RootDeactivationReason.RegisteredByMistake
        ) {
          return HttpResponse.json({
            data: {
              deactivateRoot: { __typename: "SimplePayload", success: true },
            },
          });
        }

        return HttpResponse.json({
          errors: [new GraphQLError("Error deactivating roots")],
        });
      },
    );
    const finalGitRootsQueryMock = graphqlMocked.query(
      GET_GIT_ROOTS,
      (): StrictResponse<{ data: GetGitRootsQuery }> => {
        return HttpResponse.json({
          data: {
            __typename: "Query",
            ...{
              group: {
                ...gitRootsDataMock.group,
                gitRoots: {
                  ...gitRootsDataMock.group.gitRoots,
                  edges: [
                    {
                      node: {
                        ...gitRootsDataMock.group.gitRoots.edges[0].node,
                        state: ResourceState.Inactive,
                      },
                    },
                  ],
                },
              },
            },
          },
        });
      },
      { once: true },
    );
    const meGoupAndRootVulnsQueryMock = [
      graphqlMocked.query(
        GET_GROUPS,
        (): StrictResponse<{ data: GetGroups }> => {
          return HttpResponse.json({
            data: {
              me: {
                __typename: "Me",
                organizations: [
                  {
                    __typename: "Organization",
                    groups: [
                      {
                        __typename: "Group",
                        name: "unittesting",
                        organization: "okada",
                        service: ServiceType.White,
                      },
                    ],
                    name: "okada",
                  },
                ],
                userEmail: "",
              },
            },
          });
        },
      ),
      graphqlMocked.query(
        GET_ROOT_VULNS,
        (): StrictResponse<{ data: GetRootVulns }> => {
          return HttpResponse.json({
            data: {
              root: {
                __typename: "GitRoot",
                id: "4039d098-ffc5-4984-8ed3-eb17bca98e19",
                vulnerabilities: [
                  {
                    id: "test",
                    state: VulnerabilityState.Vulnerable,
                    technique: Technique.Cloud,
                    vulnerabilityType: "lines",
                  },
                ],
              },
            },
          });
        },
      ),
    ];

    mswRender(
      <authzGroupContext.Provider
        value={
          new PureAbility([
            { action: "has_service_white" },
            { action: "has_advanced" },
          ])
        }
      >
        <authzPermissionsContext.Provider
          value={
            new PureAbility([
              { action: "integrates_api_mutations_activate_root_mutate" },
            ])
          }
        >
          <organizationDataContext.Provider value={organizationData}>
            <Routes>
              <Route
                element={<GroupScope />}
                path={"/orgs/:organizationName/groups/:groupName/scope"}
              />
            </Routes>
          </organizationDataContext.Provider>
        </authzPermissionsContext.Provider>
      </authzGroupContext.Provider>,
      {
        memoryRouter: memoryUnittesting,
        mocks: [
          finalGitRootsQueriesMockGlobal,
          finalGitRootsMockGlobal,
          ...meGoupAndRootVulnsQueryMock,
          mutationMock,
          finalGitRootsQueryMock,
          finalGitRootsQueriesMockGlobal,
        ],
      },
    );

    await waitFor((): void => {
      expect(
        within(screen.queryAllByRole("table")[0]).queryByText(
          "table.noDataIndication",
        ),
      ).not.toBeInTheDocument();
    });

    expect(
      within(screen.queryAllByRole("table")[0]).queryAllByRole("row"),
    ).toHaveLength(2);
    expect(
      screen.queryByText("group.scope.common.confirm"),
    ).not.toBeInTheDocument();
    expect(
      within(screen.queryAllByRole("table")[0]).getByRole<HTMLInputElement>(
        "checkbox",
      ).checked,
    ).toBe(true);

    fireEvent.click(
      within(screen.queryAllByRole("table")[0]).getByRole("checkbox"),
    );
    await waitFor((): void => {
      expect(
        screen.queryAllByText("group.scope.common.deactivation.title"),
      ).toHaveLength(1);
    });

    expect(screen.getByRole("button", { name: "Confirm" })).toBeDisabled();

    await userEvent.click(
      screen.getByText("group.scope.common.deactivation.reason.label"),
    );
    await userEvent.click(
      screen.getByText("group.scope.common.deactivation.reason.mistake"),
    );

    await waitFor((): void => {
      expect(
        screen.getByRole("button", { name: "Confirm" }),
      ).not.toBeDisabled();
    });

    expect(
      screen.queryByText("group.scope.common.confirm"),
    ).not.toBeInTheDocument();

    await userEvent.click(screen.getByRole("button", { name: "Confirm" }));
    await waitFor((): void => {
      expect(
        screen.queryByText("group.scope.common.confirm"),
      ).toBeInTheDocument();
    });
    await userEvent.click(
      screen.getAllByRole("button", { name: "Confirm" })[1],
    );

    await waitFor((): void => {
      expect(
        within(screen.queryAllByRole("table")[0]).getByRole<HTMLInputElement>(
          "checkbox",
        ).checked,
      ).toBe(false);
    });
  });

  const mockError = [
    graphql
      .link(LINK)
      .query(GET_GROUP_ACCESS_INFO, (): StrictResponse<IErrorMessage> => {
        return HttpResponse.json({
          errors: [new GraphQLError("Error getting group access info")],
        });
      }),
    graphql.link(LINK).query(GET_TAGS, (): StrictResponse<IErrorMessage> => {
      return HttpResponse.json({
        errors: [new GraphQLError("Error getting group tags")],
      });
    }),
  ];

  it("should render tags component", async (): Promise<void> => {
    expect.hasAssertions();

    jest.clearAllMocks();

    const { container } = mswRender(
      <Routes>
        <Route
          element={<GroupScope />}
          path={"/orgs/:organizationName/groups/:groupName/scope"}
        />
      </Routes>,
      {
        memoryRouter: memoryTest,
        mocks: [...mocks],
      },
    );

    await waitFor((): void => {
      expect(screen.queryAllByRole("table")).toHaveLength(2);
    });

    await waitFor((): void => {
      expect(screen.queryAllByRole("row")).toHaveLength(4);
    });

    expect(container.querySelector("#resources")).toBeInTheDocument();
  });

  it("should render a error in component", async (): Promise<void> => {
    expect.hasAssertions();

    jest.clearAllMocks();

    mswRender(
      <Routes>
        <Route
          element={<GroupScope />}
          path={"/orgs/:organizationName/groups/:groupName/scope"}
        />
      </Routes>,
      { memoryRouter: memoryTest, mocks: [...mocks, ...mockError] },
    );

    await waitFor((): void => {
      expect(msgError).toHaveBeenCalledWith("groupAlerts.errorTextsad");
    });

    await waitFor((): void => {
      expect(msgError).toHaveBeenCalledTimes(3);
    });

    await waitFor((): void => {
      expect(screen.queryAllByRole("table")).toHaveLength(1);
    });
    await waitFor((): void => {
      expect(screen.queryAllByRole("row")).toHaveLength(2);
    });
  });

  it("should render files component", async (): Promise<void> => {
    expect.hasAssertions();

    jest.clearAllMocks();

    const mockFiles = graphql
      .link(LINK)
      .query(GET_FILES, (): StrictResponse<{ data: GetFiles }> => {
        return HttpResponse.json({
          data: {
            resources: {
              __typename: "Resource",
              files: [
                {
                  description: "shell",
                  fileName: "shell.exe",
                  uploadDate: "2019-04-24 14:56",
                  uploader: "unittest@fluidattacks.com",
                },
                {
                  description: "Test",
                  fileName: "test.zip",
                  uploadDate: "2019-03-01 15:21",
                  uploader: "unittest@fluidattacks.com",
                },
              ],
            },
          },
        });
      });

    mswRender(
      <Routes>
        <Route
          element={<GroupScope />}
          path={"/orgs/:organizationName/groups/:groupName/scope"}
        />
      </Routes>,
      { memoryRouter: memoryTest, mocks: [...mocks, mockFiles] },
    );
    await waitFor((): void => {
      expect(screen.queryAllByRole("table")).toHaveLength(2);
    });
    await waitFor((): void => {
      expect(screen.queryAllByRole("row")).toHaveLength(5);
    });
  });

  it("should render IP roots", async (): Promise<void> => {
    expect.hasAssertions();

    mswRender(
      <authzGroupContext.Provider
        value={new PureAbility([{ action: "has_service_black" }])}
      >
        <organizationDataContext.Provider value={organizationData}>
          <Routes>
            <Route
              element={<GroupScope />}
              path={"/orgs/:organizationName/groups/:groupName/scope"}
            />
          </Routes>
        </organizationDataContext.Provider>
      </authzGroupContext.Provider>,
      {
        memoryRouter: memoryUnittesting,
        mocks: [
          finalGitRootsQueriesMockGlobal,
          initialIPRootsMockGlobal,
          initialUrlRootsMockGlobal,
        ],
      },
    );

    await waitFor((): void => {
      expect(
        within(screen.queryAllByRole("table")[0]).queryByText(
          "table.noDataIndication",
        ),
      ).toBeInTheDocument();
    });

    expect(
      within(screen.queryAllByRole("table")[0]).queryAllByRole("row"),
    ).toHaveLength(2);

    await waitFor((): void => {
      expect(
        within(screen.queryAllByRole("table")[0]).queryAllByRole("row")[1]
          .textContent,
      ).toStrictEqual(
        [
          // Address
          "127.0.0.1",
          // Nickname
          "ip_root_1",
          // State
          "Active",
        ].join(""),
      );
    });
  });

  it("should render button internal surface", async (): Promise<void> => {
    expect.hasAssertions();

    const finalRootsQueryMock = graphqlMocked.query(
      GET_GIT_ROOTS,
      (): StrictResponse<{ data: GetGitRootsQuery }> => {
        return HttpResponse.json({
          data: {
            __typename: "Query",
            ...{
              group: {
                ...gitRootsDataMock.group,
                gitRoots: {
                  ...gitRootsDataMock.group.gitRoots,
                  edges: [
                    {
                      node: {
                        ...gitRootsDataMock.group.gitRoots.edges[0].node,
                        includesHealthCheck: true,
                      },
                    },
                  ],
                },
              },
            },
          },
        });
      },
      { once: true },
    );

    const mockedPermissions = new PureAbility<string>([
      { action: "integrates_api_resolvers_group_toe_lines_connection_resolve" },
      { action: "integrates_api_resolvers_group_toe_inputs_resolve" },
      { action: "integrates_api_resolvers_group_toe_ports_resolve" },
      { action: "see_internal_toe" },
    ]);
    mswRender(
      <authzGroupContext.Provider
        value={new PureAbility([{ action: "has_service_white" }])}
      >
        <organizationDataContext.Provider value={organizationData}>
          <authzPermissionsContext.Provider value={mockedPermissions}>
            <Routes>
              <Route
                element={<GroupScope />}
                path={"/orgs/:organizationName/groups/:groupName/scope"}
              />
            </Routes>
          </authzPermissionsContext.Provider>
        </organizationDataContext.Provider>
      </authzGroupContext.Provider>,
      {
        memoryRouter: memoryUnittesting,
        mocks: [finalGitRootsQueriesMockGlobal, finalRootsQueryMock],
      },
    );
    await waitFor((): void => {
      expect(
        screen.getByRole("button", { name: "group.tabs.toe.text" }),
      ).toBeInTheDocument();
    });
    await waitFor((): void => {
      expect(
        within(screen.queryAllByRole("table")[0]).queryByText(
          "table.noDataIndication",
        ),
      ).not.toBeInTheDocument();
    });

    expect(
      within(screen.queryAllByRole("table")[0]).queryAllByRole("row"),
    ).toHaveLength(2);

    await userEvent.click(
      screen.getByRole("button", { name: "group.tabs.toe.text" }),
    );

    await waitFor((): void => {
      expect(
        screen.getByText("group.scope.internalSurface.confirmDialog.title"),
      ).toBeInTheDocument();
    });

    await userEvent.click(screen.getByRole("button", { name: "Confirm" }));
  });

  it("should render error when add git root", async (): Promise<void> => {
    expect.hasAssertions();

    const mutationMock = graphqlMocked.mutation(
      ADD_GIT_ROOT,
      ({
        variables,
      }): StrictResponse<
        Record<"errors", IMessage[]> | { data: AddGitRoot }
      > => {
        const {
          credentials,
          gitignore,
          groupName,
          includesHealthCheck,
          nickname,
          url,
        } = variables;

        if (
          _.isEqual(credentials, {
            arn: "",
            isPat: false,
            name: "credential name",
            password: "password-test",
            token: "",
            type: "HTTPS",
            user: "user-test",
          }) &&
          _.isEqual(gitignore, []) &&
          groupName === "unittesting" &&
          !includesHealthCheck &&
          nickname === "" &&
          url === "https://gitlab.com/fluidattacks/universe"
        ) {
          return HttpResponse.json({
            errors: [
              new GraphQLError(
                "Exception - Root with the same nickname already exists",
              ),
              new GraphQLError(
                "Exception - Root with the same URL/branch already exists",
              ),
              new GraphQLError(
                "Exception - A credential exists with the same name",
              ),
            ],
          });
        }

        return HttpResponse.json({
          data: {
            addGitRoot: { __typename: "AddRootPayload", success: true },
          },
        });
      },
    );
    jest.clearAllMocks();

    mswRender(
      <authzGroupContext.Provider
        value={
          new PureAbility([
            { action: "has_service_white" },
            { action: "has_advanced" },
          ])
        }
      >
        <authzPermissionsContext.Provider
          value={
            new PureAbility([
              { action: "integrates_api_mutations_add_git_root_mutate" },
              { action: "integrates_api_mutations_add_secret_mutate" },
              { action: "integrates_api_mutations_update_git_root_mutate" },
            ])
          }
        >
          <organizationDataContext.Provider value={organizationData}>
            <Routes>
              <Route
                element={<GroupScope />}
                path={"/orgs/:organizationName/groups/:groupName/scope"}
              />
            </Routes>
          </organizationDataContext.Provider>
        </authzPermissionsContext.Provider>
      </authzGroupContext.Provider>,
      {
        memoryRouter: memoryUnittesting,
        mocks: [
          initialGitRootsQueriesMockGlobal,
          initialGitRootsMockGlobal,
          mutationMock,
          finalGitRootsQueriesMockGlobal,
          finalGitRootsMockGlobal,
        ],
      },
    );
    const role = "combobox";

    await waitFor((): void => {
      expect(
        screen.queryAllByRole("button", { name: "group.scope.common.add" }),
      ).toHaveLength(1);
    });

    await userEvent.click(
      screen.queryAllByRole("button", { name: "group.scope.common.add" })[0],
    );

    await userEvent.click(
      screen.getByText("components.repositoriesDropdown.manual.text"),
    );

    expect(screen.queryByRole(role, { name: "url" })).toBeInTheDocument();
    expect(screen.getByText(btnConfirm)).toBeDisabled();

    await userEvent.type(
      screen.getByRole(role, { name: "url" }),
      "https://gitlab.com/fluidattacks/universe",
    );
    await userEvent.type(screen.getByRole(role, { name: "branch" }), "master");

    expect(
      screen.queryByRole("textbox", { name: "credentials.key" }),
    ).not.toBeInTheDocument();
    expect(
      screen.queryByRole("textbox", { name: "credentials.token" }),
    ).not.toBeInTheDocument();
    expect(
      screen.queryByRole("textbox", {
        name: "credentials.azureOrganization",
      }),
    ).not.toBeInTheDocument();

    expect(
      screen.getByText("group.scope.git.repo.credentials.userHttps"),
    ).toBeInTheDocument();

    expect(
      screen.queryByRole(role, { name: "credentials.user" }),
    ).toBeInTheDocument();
    expect(
      screen.queryByRole(role, { name: "credentials.password" }),
    ).toBeInTheDocument();

    await userEvent.type(
      screen.getByRole(role, { name: "credentials.name" }),
      "credential name",
    );

    await userEvent.type(
      screen.getByRole(role, { name: "credentials.user" }),
      "user-test",
    );

    await userEvent.type(
      screen.getByRole(role, { name: "credentials.password" }),
      "password-test",
    );

    await userEvent.click(screen.getByRole("radio", { name: "No" }));
    const healthCheckCheckboxes = 3;
    const secureConnectionCheckboxes = 2;

    expect(screen.queryAllByRole("checkbox", { checked: false })).toHaveLength(
      healthCheckCheckboxes + secureConnectionCheckboxes,
    );

    await userEvent.click(screen.getByDisplayValue("rejectA"));
    await userEvent.click(screen.getByDisplayValue("rejectB"));
    await userEvent.click(screen.getByDisplayValue("rejectC"));

    expect(screen.queryAllByRole("checkbox", { checked: true })).toHaveLength(
      healthCheckCheckboxes,
    );

    await waitFor((): void => {
      expect(screen.getByText(btnConfirm)).not.toBeDisabled();
    });
    await userEvent.click(screen.getByText(btnConfirm));
    await waitFor((): void => {
      expect(msgError).toHaveBeenCalledWith("groupAlerts.errorTextsad");
    });

    expect(msgError).toHaveBeenCalledTimes(2);
  });
});
