import { useQuery } from "@apollo/client";
import { Col, Row } from "@fluidattacks/design";
import isNil from "lodash/isNil";
import { useTranslation } from "react-i18next";

import { GET_GIT_ROOT_DETAILS } from "../../queries";
import type { IDescriptionProps } from "../types";
import { formatIsoDate } from "utils/date";
import { includeTagsFormatter } from "utils/format-helpers";
import { Logger } from "utils/logger";

const Description = ({
  gitEnvironmentUrls,
  groupName,
  id,
  nickname,
}: IDescriptionProps & Readonly<{ groupName: string }>): JSX.Element => {
  const { t } = useTranslation();

  // GraphQL operations
  const { data } = useQuery(GET_GIT_ROOT_DETAILS, {
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error): void => {
        Logger.error("Couldn't load root details", error);
      });
    },
    variables: { groupName, rootId: id },
  });
  if (isNil(data)) {
    return <div />;
  }

  const { gitignore } =
    data.root.__typename === "GitRoot" ? data.root : { gitignore: [] };
  const { lastStateStatusUpdate } =
    data.root.__typename === "GitRoot"
      ? data.root
      : { lastStateStatusUpdate: "" };
  const { modifiedDate } =
    data.root.__typename === "GitRoot"
      ? data.root.cloningStatus
      : { modifiedDate: "" };
  const { message } =
    data.root.__typename === "GitRoot"
      ? data.root.cloningStatus
      : { message: "" };

  return (
    <div>
      <h3>{t("group.findings.description.title")}</h3>
      <Row>
        <Col lg={50} md={50} sm={50}>
          {t("group.scope.git.repo.nickname")}
          {":"}&nbsp;{nickname}
        </Col>
      </Row>
      <hr />
      <Row>
        <Col lg={50} md={50} sm={50}>
          {t("group.scope.git.envUrls")}
          {":"}
          <ul>
            {gitEnvironmentUrls.map(
              (envUrl): JSX.Element => (
                <li key={envUrl.id}>
                  {includeTagsFormatter({
                    excludeTag: !envUrl.include,
                    text: (
                      <a href={envUrl.url} rel={"noreferrer"} target={"_blank"}>
                        {envUrl.url}
                      </a>
                    ),
                  })}
                </li>
              ),
            )}
          </ul>
        </Col>
        <Col lg={50} md={50} sm={50}>
          {t("group.scope.git.filter.exclude")}
          {":"}
          <ul>
            {gitignore.map(
              (pattern): JSX.Element => (
                <li key={pattern}>{pattern}</li>
              ),
            )}
          </ul>
        </Col>
      </Row>
      <hr />
      <Row>
        <Col lg={50} md={50} sm={50}>
          {t("group.scope.common.lastStateStatusUpdate")}
          {":"}&nbsp;{formatIsoDate(lastStateStatusUpdate)}
        </Col>
        <Col lg={50} md={50} sm={50}>
          {t("group.scope.common.lastCloningStatusUpdate")}
          {":"}&nbsp;{formatIsoDate(modifiedDate)}
        </Col>
      </Row>
      <hr />
      <Row>
        <Col lg={50} md={50} sm={50}>
          {t("group.scope.git.repo.cloning.message")}
          {":"}&nbsp;{message}
        </Col>
      </Row>
    </div>
  );
};

export const renderDescriptionComponent = (
  props: IDescriptionProps,
  groupName: string,
): JSX.Element => (
  <Description
    gitEnvironmentUrls={props.gitEnvironmentUrls}
    groupName={groupName}
    id={props.id}
    nickname={props.nickname}
  />
);
