/* eslint-disable max-lines */
import type { FetchResult } from "@apollo/client";
import type { GraphQLError } from "graphql";
import _ from "lodash";
import mixpanel from "mixpanel-browser";
import type React from "react";
import { useCallback } from "react";

import { checkSSHFormat } from "./management-modal/repository/utils";
import { formInitialValues } from "./management-modal/utils";

import type {
  ICredentials,
  IDockerImageAttr,
  IEnvironmentUrlAttr,
  IFormValues,
  IGitRootAttr,
  IGitRootData,
  IGitRootManagementDetailsAttr,
} from "../types";
import {
  formatBooleanHealthCheck,
  getAddGitRootCredentials,
  getUpdateGitRootCredentials,
} from "../utils";
import type {
  GetGitRootsViewFragmentFragment,
  GetRootsEnvironmentUrlsFragment,
} from "gql/graphql";
import { formatTypeCredentials } from "utils/credentials";
import { Logger } from "utils/logger";
import { msgError } from "utils/notifications";
import { translate } from "utils/translations/translate";

// GitModal helpers

type TModalMessages = React.Dispatch<
  React.SetStateAction<{
    message: string;
    type: string;
  }>
>;

// Index helpers

const handleCreationError = (
  graphQLErrors: readonly GraphQLError[],
  setModalMessages: TModalMessages,
): void => {
  graphQLErrors.forEach((error): void => {
    switch (error.message) {
      case "Exception - Error empty value is not valid":
        setModalMessages({
          message: translate.t("group.scope.git.errors.invalid"),
          type: "error",
        });
        break;
      case "Exception - Root with the same nickname already exists":
        setModalMessages({
          message: translate.t("group.scope.common.errors.duplicateNickname"),
          type: "error",
        });
        break;
      case "Exception - Root with the same URL/branch already exists":
        setModalMessages({
          message: translate.t("group.scope.common.errors.duplicateUrlBranch"),
          type: "error",
        });
        break;
      case "Exception - Root name should not be included in the exception pattern":
        setModalMessages({
          message: translate.t("group.scope.git.errors.rootInGitignore"),
          type: "error",
        });
        break;
      case "Exception - Invalid characters":
        setModalMessages({
          message: translate.t("validations.invalidChar"),
          type: "error",
        });
        break;
      case "Exception - Unsanitized input found":
        setModalMessages({
          message: translate.t("validations.unsanitizedInputFound"),
          type: "error",
        });
        break;
      case "Exception - A credential exists with the same name":
        setModalMessages({
          message: translate.t("validations.invalidCredentialName"),
          type: "error",
        });
        break;
      case "Exception - Git repository was not accessible with given credentials":
        setModalMessages({
          message: translate.t("group.scope.git.errors.invalidGitCredentials"),
          type: "error",
        });
        break;
      case "Exception - Branch not found":
        setModalMessages({
          message: translate.t("group.scope.git.errors.invalidBranch"),
          type: "error",
        });
        break;
      case "Exception - Field cannot fill with blank characters":
        setModalMessages({
          message: translate.t("validations.invalidSpaceField"),
          type: "error",
        });
        break;
      case "Exception - Protocol http is invalid":
        setModalMessages({
          message: translate.t("validations.invalidHttpProtocol"),
          type: "error",
        });
        break;
      case "Exception - The action is not allowed during the free trial":
        msgError(translate.t("group.scope.git.errors.trial"));
        break;
      default:
        setModalMessages({
          message: translate.t("groupAlerts.errorTextsad"),
          type: "error",
        });
        Logger.error("Couldn't add git roots", error);
    }
  });
};

const handleUpdateError = (
  graphQLErrors: readonly GraphQLError[],
  setModalMessages: TModalMessages,
  scope: "envs" | "root" | "tours",
): void => {
  graphQLErrors.forEach((error): void => {
    const showMessage = (translation: string): void => {
      if (scope === "root" || scope === "envs") {
        setModalMessages({
          message: translate.t(translation),
          type: "error",
        });
      } else {
        msgError(translate.t(translation));
      }
    };
    if (error.message.includes("Exception - Invalid expression -")) {
      const [, message] = error.message.split(
        "Exception - Invalid expression -",
      );
      setModalMessages({
        message: `${translate.t("group.scope.git.errors.invalidGitignore")} - ${message}`,
        type: "error",
      });

      return;
    }
    if (error.message.includes("Exception - Invalid nickname -")) {
      const [, message] = error.message.split("Exception - Invalid nickname -");
      setModalMessages({
        message,
        type: "error",
      });

      return;
    }
    switch (error.message) {
      case "Exception - Error empty value is not valid":
        showMessage("group.scope.git.errors.invalid");
        break;
      case "Exception - A root with reported vulns can't be updated":
        showMessage("group.scope.common.errors.hasVulns");
        break;
      case "Exception - Root with the same URL/branch already exists":
        showMessage("group.scope.common.errors.duplicateUrlBranch");
        break;
      case "Exception - Root name should not be included in the exception pattern":
        setModalMessages({
          message: translate.t("group.scope.git.errors.rootInGitignore"),
          type: "error",
        });
        break;
      case "Exception - Root with the same nickname already exists":
        showMessage("group.scope.common.errors.duplicateNickname");
        break;
      case "Exception - Invalid characters":
        showMessage("validations.invalidChar");
        break;
      case "Exception - Git repository was not accessible with given credentials":
        showMessage("group.scope.git.errors.invalidGitCredentials");
        break;
      case "Exception - A credential exists with the same name":
        showMessage("validations.invalidCredentialName");
        break;
      case "Exception - Unsanitized input found":
        setModalMessages({
          message: translate.t("validations.unsanitizedInputFound"),
          type: "error",
        });
        break;
      case "Exception - Protocol http is invalid":
        setModalMessages({
          message: translate.t("validations.invalidHttpProtocol"),
          type: "error",
        });
        break;
      case "Exception - Error permission denied - Edit URL":
        setModalMessages({
          message: translate.t("group.scope.git.errors.permissions.editUrl"),
          type: "error",
        });
        break;
      default:
        showMessage("groupAlerts.errorTextsad");
        Logger.error(`Couldn't update git ${scope}`, error);
    }
  });
};

const handleActivationError = (
  graphQLErrors: readonly GraphQLError[],
): void => {
  graphQLErrors.forEach((error): void => {
    if (
      error.message ===
      "Exception - Root with the same URL/branch already exists"
    ) {
      msgError(translate.t("group.scope.common.errors.duplicateUrlBranch"));
    } else {
      msgError(translate.t("groupAlerts.errorTextsad"));
      Logger.error("Couldn't activate root", error);
    }
  });
};

const handleSyncError = (graphQLErrors: readonly GraphQLError[]): void => {
  graphQLErrors.forEach((error): void => {
    switch (error.message) {
      case "Exception - Access denied or credential not found":
        msgError(translate.t("group.scope.git.sync.noCredentials"));
        break;
      case "Exception - The root already has an active cloning process":
        msgError(translate.t("group.scope.git.sync.alreadyCloning"));
        break;
      case "Exception - Cloning job or task could not be queued":
        msgError(translate.t("group.scope.git.sync.couldNotBeQueued"));
        break;
      default:
        msgError(translate.t("groupAlerts.errorTextsad"));
        Logger.error("Couldn't queue root cloning", error);
    }
  });
};

const hasCheckedItem = (
  checkedItems: Record<string, boolean>,
  columnName: string,
): boolean => {
  return (
    Object.values(checkedItems).filter((val: boolean): boolean => val)
      .length === 1 && checkedItems[columnName]
  );
};

function useGitSubmit(
  addGitRoot: (
    variables: Record<string, unknown>,
  ) => Promise<FetchResult<unknown>>,
  groupName: string,
  isManagingRoot: false | { mode: "ADD" | "EDIT" },
  setModalMessages: TModalMessages,
  updateGitRoot: (
    variables: Record<string, unknown>,
  ) => Promise<FetchResult<unknown>>,
): ({
  branch,
  credentials,
  gitignore,
  id,
  includesHealthCheck,
  nickname,
  priority,
  url,
  useEgress,
  useVpn,
  useZtna,
}: IFormValues) => Promise<void> {
  return useCallback(
    async ({
      branch,
      credentials,
      gitignore,
      id,
      includesHealthCheck,
      nickname,
      priority,
      url,
      useEgress,
      useVpn,
      useZtna,
    }: IFormValues): Promise<void> => {
      setModalMessages({ message: "", type: "success" });
      if (isManagingRoot !== false) {
        const newUrl = checkSSHFormat(url.trim())
          ? `ssh://${url.trim()}`
          : url.trim();
        if (isManagingRoot.mode === "ADD") {
          mixpanel.track("AddGitRoot");
          await addGitRoot({
            variables: {
              branch: branch.trim(),
              credentials: getAddGitRootCredentials(credentials),
              criticality: _.isEmpty(priority) ? null : priority,
              gitignore,
              groupName,
              includesHealthCheck:
                formatBooleanHealthCheck(includesHealthCheck) ?? false,
              nickname,
              url: newUrl,
              useEgress,
              useVpn,
              useZtna,
            },
          });
        } else {
          mixpanel.track("EditGitRoot");
          await updateGitRoot({
            variables: {
              branch,
              credentials: getUpdateGitRootCredentials(credentials),
              criticality: _.isEmpty(priority) ? null : priority,
              gitignore,
              groupName,
              id,
              includesHealthCheck:
                formatBooleanHealthCheck(includesHealthCheck) ?? false,
              nickname,
              url: newUrl,
              useEgress,
              useVpn,
              useZtna,
            },
          });
        }
      }
    },
    [addGitRoot, groupName, isManagingRoot, setModalMessages, updateGitRoot],
  );
}

function filterSelectStatus(
  rows: IGitRootAttr[],
  currentValue: string,
): IGitRootAttr[] {
  return rows.filter((row: IGitRootAttr): boolean =>
    _.isEmpty(currentValue) ? true : row.cloningStatus.status === currentValue,
  );
}

function filterSelectIncludesHealthCheck(
  rows: IGitRootAttr[],
  currentValue: string,
): IGitRootAttr[] {
  const isHealthCheckIncluded = currentValue === "true";

  return rows.filter((row: IGitRootAttr): boolean =>
    _.isEmpty(currentValue)
      ? true
      : row.includesHealthCheck === isHealthCheckIncluded,
  );
}

const getCredentials = (
  currentRow: IGitRootManagementDetailsAttr,
): ICredentials => {
  return _.isNull(currentRow.credentials)
    ? {
        arn: "",
        auth: "",
        azureOrganization: "",
        id: "",
        isPat: false,
        isToken: false,
        key: "",
        name: "",
        password: "",
        token: "",
        type: "",
        typeCredential: "",
        user: "",
      }
    : {
        ...currentRow.credentials,
        typeCredential: formatTypeCredentials(currentRow.credentials),
      };
};

const getEnvsFromResult = (
  envUrlsResult: GetRootsEnvironmentUrlsFragment | null | undefined,
): IEnvironmentUrlAttr[] =>
  _.isNil(envUrlsResult)
    ? ([] as IEnvironmentUrlAttr[])
    : envUrlsResult.group.gitEnvironmentUrls;

const getGitRootsFromResult = (
  rootsResult: GetGitRootsViewFragmentFragment | null | undefined,
): IGitRootAttr[] =>
  _.isNil(rootsResult)
    ? ([] as IGitRootAttr[])
    : rootsResult.group.gitRoots.edges.map(
        (edge): IGitRootAttr => edge.node as IGitRootAttr,
      );

const getInitialValues = (
  currentRow: IGitRootManagementDetailsAttr | undefined,
): IFormValues => {
  if (_.isUndefined(currentRow)) {
    return formInitialValues;
  }

  return {
    branch: currentRow.branch,
    cloningStatus: currentRow.cloningStatus,
    credentials: getCredentials(currentRow),
    gitEnvironmentUrls: currentRow.gitEnvironmentUrls,
    gitignore: currentRow.gitignore,
    hasExclusions: _.isEmpty(currentRow.gitignore) ? "no" : "yes",
    healthCheckConfirm: undefined,
    id: currentRow.id,
    includesHealthCheck: currentRow.includesHealthCheck,
    nickname: currentRow.nickname,
    priority: currentRow.criticality,
    state: currentRow.state,
    url: currentRow.url,
    useEgress: currentRow.useEgress,
    useVpn: currentRow.useVpn,
    useZtna: currentRow.useZtna,
  };
};

const formatRootsData = (
  rootsAttr: IGitRootAttr[],
  environmentUrls: IEnvironmentUrlAttr[],
): IGitRootData[] => {
  const envUrlsByRoot = environmentUrls.reduce<
    Record<string, IEnvironmentUrlAttr[]>
  >((previous, currentValue): Record<string, IEnvironmentUrlAttr[]> => {
    return {
      ...previous,
      [currentValue.rootId]: [
        ..._.get(previous, currentValue.rootId, [] as IEnvironmentUrlAttr[]),
        currentValue,
      ],
    };
  }, {});

  return rootsAttr.map((root): IGitRootData => {
    const gitEnvironmentUrls = _.get(
      envUrlsByRoot,
      root.id,
      [] as IEnvironmentUrlAttr[],
    );

    return {
      ...root,
      gitEnvironmentUrls,
    };
  });
};

const isAwsRole = (values: IFormValues): boolean => {
  if (
    values.credentials.typeCredential === "AWSROLE" &&
    values.credentials.arn === ""
  ) {
    return true;
  }

  return false;
};

const isSshRole = (values: IFormValues): boolean => {
  if (
    values.credentials.typeCredential === "SSH" &&
    values.credentials.key === ""
  ) {
    return true;
  }

  return false;
};

const getRepositoryTourStepsText = (
  values: IFormValues,
): string | undefined => {
  if (values.credentials.id === "") {
    if (
      values.credentials.typeCredential === "USER" &&
      (values.credentials.user === "" || values.credentials.password === "")
    ) {
      return translate.t("tours.addGitRoot.rootCredentials.user");
    }
    if (
      values.credentials.typeCredential === "TOKEN" &&
      (values.credentials.token === "" ||
        values.credentials.azureOrganization === "")
    ) {
      return translate.t("tours.addGitRoot.rootCredentials.token");
    }
    if (isSshRole(values)) {
      return translate.t("tours.addGitRoot.rootCredentials.key");
    }
    if (isAwsRole(values)) {
      return translate.t("tours.addGitRoot.rootCredentials.arn");
    }
    if (values.credentials.typeCredential === "") {
      return translate.t("tours.addGitRoot.rootCredentials.type");
    }
  }

  return undefined;
};

const shouldHideAwsRole = (values: IFormValues): boolean => {
  if (
    values.credentials.typeCredential === "AWSROLE" &&
    values.credentials.arn.length === 0
  ) {
    return true;
  }

  return false;
};

const shouldHideToken = (values: IFormValues): boolean => {
  if (
    values.credentials.typeCredential === "TOKEN" &&
    (values.credentials.token.length === 0 ||
      values.credentials.azureOrganization.length === 0)
  ) {
    return true;
  }

  return false;
};

const shouldHideRepositoryTourFooter = (values: IFormValues): boolean => {
  if (values.credentials.id === "") {
    if (
      values.credentials.typeCredential === "" ||
      values.credentials.name.length === 0
    ) {
      return true;
    }
    if (
      values.credentials.typeCredential === "SSH" &&
      values.credentials.key.length === 0
    ) {
      return true;
    }
    if (shouldHideAwsRole(values)) {
      return true;
    }
    if (shouldHideToken(values)) {
      return true;
    }
    if (
      values.credentials.typeCredential === "USER" &&
      (values.credentials.user.length === 0 ||
        values.credentials.password.length === 0)
    ) {
      return true;
    }
  }

  return false;
};

const fromDockerImagesToEnvironmentUrl = (
  dockerImages: IDockerImageAttr[],
): IEnvironmentUrlAttr[] => {
  return dockerImages.map(
    (dockerImage): IEnvironmentUrlAttr => ({
      __typename: "GitEnvironmentUrl" as const,
      cloudName: null,
      countSecrets: 0,
      createdAt: dockerImage.createdAt,
      createdBy: dockerImage.createdBy,
      id: dockerImage.rootId,
      include: dockerImage.include,
      root: {
        id: dockerImage.root.id,
        state: dockerImage.root.state,
        url: dockerImage.root.url,
      },
      rootId: dockerImage.rootId,
      url: dockerImage.uri,
      urlType: "Docker Image",
      useEgress: false,
      useVpn: false,
      useZtna: false,
    }),
  );
};

export {
  filterSelectStatus,
  filterSelectIncludesHealthCheck,
  formatRootsData,
  getCredentials,
  getEnvsFromResult,
  getGitRootsFromResult,
  getInitialValues,
  getRepositoryTourStepsText,
  handleCreationError,
  handleUpdateError,
  handleActivationError,
  handleSyncError,
  hasCheckedItem,
  shouldHideRepositoryTourFooter,
  useGitSubmit,
  fromDockerImagesToEnvironmentUrl,
};
