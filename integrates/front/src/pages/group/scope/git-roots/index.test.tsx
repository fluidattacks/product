import { PureAbility } from "@casl/ability";
import { screen, waitFor, within } from "@testing-library/react";
import { userEvent } from "@testing-library/user-event";
import { GraphQLError } from "graphql";
import { newServer } from "mock-xmlhttprequest";
import { HttpResponse, type StrictResponse, graphql } from "msw";
import * as React from "react";
import { Route, Routes } from "react-router-dom";

import { EXAMPLE_CSV } from "./add-git-roots-file-button/utils";
import { ManagementModal } from "./management-modal";
import { formInitialValues } from "./management-modal/utils";

import { GitRoots } from ".";
import {
  DOWNLOAD_GIT_ROOTS_FILE_MUTATION,
  GET_FILES,
  GET_GIT_ROOTS,
  GET_GIT_ROOTS_QUERIES,
  GET_GIT_ROOT_DETAILS,
  GET_GIT_ROOT_MANAGEMENT_DETAILS,
  GET_ROOT_ENVIRONMENT_URLS,
  GET_ROOT_ENVIRONMENT_URLS_SECRETS,
  GET_ROOT_SECRETS,
  REMOVE_ENVIRONMENT_URL,
  SIGN_POST_URL_MUTATION,
  SYNC_GIT_ROOT,
  UPDATE_ENVIRONMENT_URL,
  UPDATE_GIT_ROOT_ENVIRONMENT_FILE,
  UPLOAD_GIT_ROOT_FILE,
} from "../queries";
import type { IFormValues } from "../types";
import {
  authzGroupContext,
  authzPermissionsContext,
} from "context/authz/config";
import {
  GET_INTEGRATION_REPOSITORIES,
  GET_ORGANIZATION_CREDENTIALS,
} from "features/add-oauth-root-form/queries";
import {
  CloningStatus,
  type DownloadGitRootsFileMutation as DownloadFile,
  type GetFilesQueryQuery,
  type GetGitRootDetailsQuery,
  type GetGitRootsQuery,
  type GetGitRootsViewQueriesQuery,
  type GetIntegrationRepositoriesQuery,
  type GetOrganizationCredentialsQuery,
  type GetRootEnvironmentUrlsQuery as GetRootEnvironmentUrls,
  type GetRootEnvironmentUrlsSecretsQuery as GetRootEnvironmentUrlsSecrets,
  type GetRootQuery,
  type RemoveEnvironmentUrlMutation,
  ResourceState,
  RootCriticality,
  type GetGitRootManagementDetailsQuery as RootManagementDetails,
  type SignPostUrlMutationMutation,
  type SyncGitRootMutation as SyncGitRoot,
  type UpdateGitRootEnvironmentFileMutation,
  type UpdateGitRootEnvironmentUrlMutation as UpdateGitRootEnvironmentUrl,
  type UploadGitRootFileMutation as UploadGitRootFile,
} from "gql/graphql";
import { render } from "mocks";
import { LINK } from "mocks/constants";
import type { IErrorMessage, IMessage } from "mocks/types";
import { msgError, msgSuccess } from "utils/notifications";
import { getByValue } from "utils/test-utils";

jest.mock("utils/notifications", (): Record<string, unknown> => {
  const mockedNotifications: Record<string, () => Record<string, unknown>> =
    jest.requireActual("utils/notifications");
  jest.spyOn(mockedNotifications, "msgError").mockImplementation();
  jest.spyOn(mockedNotifications, "msgSuccess").mockImplementation();

  return mockedNotifications;
});
jest.mock("utils/upload-file-to-s3", (): Record<string, unknown> => {
  const mockedUpload: Record<string, () => Record<string, unknown>> =
    jest.requireActual("utils/upload-file-to-s3");

  return {
    ...mockedUpload,
    uploadGroupFileToS3: jest.fn((): number => 200),
  };
});
const graphqlMocked = graphql.link(LINK);

const gitRootsDataMock = {
  __typename: "Query" as const,
  group: {
    __typename: "Group" as const,
    gitRoots: {
      __typename: "GitRootsConnection" as const,
      edges: [
        {
          __typename: "GitRootEdge" as const,
          node: {
            __typename: "GitRoot" as const,
            branch: "master",
            cloningStatus: {
              message: "Cloning message test",
              status: "UNKNOWN",
            },
            credentials: {
              name: "",
            },
            criticality: RootCriticality.Medium,
            id: "4039d098-ffc5-4984-8ed3-eb17bca98e19",
            includesHealthCheck: false,
            nickname: "universe",
            state: "ACTIVE",
            url: "https://gitlab.com/fluidattacks/universe.git",
          },
        },
        {
          __typename: "GitRootEdge",
          node: {
            __typename: "GitRoot",
            branch: "main",
            cloningStatus: {
              __typename: "GitRootCloningStatus",
              message: "Invalid credentials",
              status: "FAILED",
            },
            credentials: {
              __typename: "Credentials",
              name: "reattacker (GitLab OAuth)",
            },
            criticality: RootCriticality.Low,
            id: "f8771794-0428-4018-90c3-ce20525cee02",
            includesHealthCheck: false,
            nickname: "integrates_2",
            state: "ACTIVE",
            url: "https://github.com/fluidattacks/integrates",
          },
        },
        {
          __typename: "GitRootEdge",
          node: {
            __typename: "GitRoot",
            branch: "develop",
            cloningStatus: {
              __typename: "GitRootCloningStatus",

              message: "changes",
              status: "QUEUED",
            },
            credentials: {
              __typename: "Credentials",
              name: "reattacker (GitLab OAuth)",
            },
            criticality: null,
            id: "9eccb9ed-e835-4dbc-b28e-8cecd0296e27",
            includesHealthCheck: false,
            nickname: "integrates_test_root",
            state: "ACTIVE",
            url: "https://gitlab.com/fluidattacks/machine",
          },
        },
        {
          __typename: "GitRootEdge",
          node: {
            __typename: "GitRoot",
            branch: "main",
            cloningStatus: {
              __typename: "GitRootCloningStatus",
              message: "Invalid credentials",
              status: "FAILED",
            },
            credentials: {
              __typename: "Credentials",
              name: "reattacker (GitLab OAuth)",
            },
            criticality: RootCriticality.Low,
            id: "f8771794-0428-4018-90c3-ce20525cee01",
            includesHealthCheck: false,
            nickname: "integrates",
            state: "INACTIVE",
            url: "https://gitlab.com/fluidattacks/integrates",
          },
        },
      ],
      pageInfo: {
        endCursor:
          "ROOT#8493c82f-2860-4902-86fa-75b0fef76034#GROUP#unittesting",
        hasNextPage: false,
      },
      total: 2,
    },
    name: "unittesting",
  },
};

const gitRootsQueriesDataMock = {
  group: {
    __typename: "Group" as const,
    gitEnvironmentUrls: [
      {
        __typename: "GitEnvironmentUrl" as const,
        cloudName: null,
        createdAt: new Date("2022-04-27T17:30:07.230355"),
        createdBy: null,
        id: "3f6eb6274ec7dc2855451c0fbb4ff9485360be5b",
        include: true,
        root: {
          id: "4039d098-ffc5-4984-8ed3-eb17bca98e19",
          state: "ACTIVE",
          url: "https://gitlab.com/fluidattacks/universe",
        },
        rootId: "4039d098-ffc5-4984-8ed3-eb17bca98e19",
        url: "https://app.fluidattacks.com",
        urlType: "URL",
        useEgress: false,
        useVpn: false,
        useZtna: false,
      },
      {
        __typename: "GitEnvironmentUrl" as const,
        cloudName: null,
        createdAt: new Date("2022-04-27T17:30:07.230355"),
        createdBy: null,
        id: "4e6eb6274ec7dc2855451c0fbb4ff9485360be5b",
        include: true,
        root: {
          id: "f8771794-0428-4018-90c3-ce20525cee01",
          state: "INACTIVE",
          url: "https://gitlab.com/fluidattacks/integrates",
        },
        rootId: "f8771794-0428-4018-90c3-ce20525cee01",
        url: "https://app.fluidattacks.com/test",
        urlType: "URL",
        useEgress: false,
        useVpn: false,
        useZtna: false,
      },
    ],
    name: "unittesting",
  },
  organizationId: {
    __typename: "Organization" as const,
    awsExternalId: "d89de098-0b63-4a5d-a9b3-df30438bfa86",
    name: "ort-test",
  },
};
const initialGitRootsMockGlobal = graphqlMocked.query(
  GET_GIT_ROOTS,
  (): StrictResponse<{ data: GetGitRootsQuery }> => {
    return HttpResponse.json({
      data: {
        __typename: "Query",
        ...{
          group: {
            ...gitRootsDataMock.group,
            gitRoots: {
              ...gitRootsDataMock.group.gitRoots,
              edges: [],
            },
          },
        },
      },
    });
  },
  { once: true },
);
const finalGitRootsMockGlobal = graphqlMocked.query(
  GET_GIT_ROOTS,
  (): StrictResponse<{ data: GetGitRootsQuery }> => {
    return HttpResponse.json({
      data: {
        ...gitRootsDataMock,
      },
    });
  },
  { once: true },
);
const initialGitRootsQueriesMockGlobal = graphqlMocked.query(
  GET_GIT_ROOTS_QUERIES,
  (): StrictResponse<{ data: GetGitRootsViewQueriesQuery }> => {
    return HttpResponse.json({
      data: {
        __typename: "Query",
        ...{
          group: {
            __typename: "Group",
            gitEnvironmentUrls: [],
            name: "unittesting",
          },
          organizationId: gitRootsQueriesDataMock.organizationId,
        },
      },
    });
  },
  { once: true },
);
const finalGitRootsQueriesMockGlobal = graphqlMocked.query(
  GET_GIT_ROOTS_QUERIES,
  (): StrictResponse<{ data: GetGitRootsViewQueriesQuery }> => {
    return HttpResponse.json({
      data: {
        __typename: "Query",
        ...{ group: gitRootsQueriesDataMock.group },
        ...{ organizationId: gitRootsQueriesDataMock.organizationId },
      },
    });
  },
  { once: true },
);
const mockedEnvironments = graphqlMocked.query(
  GET_ROOT_ENVIRONMENT_URLS,
  ({
    variables,
  }): StrictResponse<IErrorMessage | { data: GetRootEnvironmentUrls }> => {
    const { groupName, rootId } = variables;
    if (
      groupName === "test-group" &&
      rootId === "c7118ab2-9345-4398-895f-11ce6e3cc1a6"
    ) {
      return HttpResponse.json({
        data: {
          root: {
            __typename: "GitRoot",
            gitEnvironmentUrls: [
              {
                id: "adc83b19e793491b1c6ea0fd8b46cd9f32e592fc",
                include: true,
                rootId: "c7118ab2-9345-4398-895f-11ce6e3cc1a6",
                url: "https://app.fluidattacks.com/test",
                urlType: "URL",
                useEgress: false,
                useVpn: false,
                useZtna: false,
              },
            ],
            id: "c7118ab2-9345-4398-895f-11ce6e3cc1a6",
          },
        },
      });
    }

    return HttpResponse.json({
      errors: [new GraphQLError("Error getting root environment urls")],
    });
  },
);

const mockedOauthData = [
  graphqlMocked.query(
    GET_ORGANIZATION_CREDENTIALS,
    (): StrictResponse<{ data: GetOrganizationCredentialsQuery }> => {
      return HttpResponse.json({
        data: {
          organization: {
            __typename: "Organization",
            credentials: [
              {
                __typename: "Credentials",
                azureOrganization: null,
                id: "6e52c11c-abf7-4ca3-b7d0-635e394f41c2",
                isPat: false,
                isToken: false,
                name: "GitLab",
                oauthType: "GITLAB",
                owner: "owner@test.com",
                type: "OAUTH",
              },
            ],
            name: "okada",
          },
        },
      });
    },
  ),
  graphqlMocked.query(
    GET_INTEGRATION_REPOSITORIES,
    ({
      variables,
    }): StrictResponse<
      IErrorMessage | { data: GetIntegrationRepositoriesQuery }
    > => {
      const { credId } = variables;
      if (credId === "6e52c11c-abf7-4ca3-b7d0-635e394f41c2") {
        return HttpResponse.json({
          data: {
            organization: {
              __typename: "Organization",
              credential: {
                __typename: "Credentials",
                integrationRepositories: [
                  {
                    __typename: "OrganizationIntegrationRepositories",
                    branches: ["prod", "main"],
                    name: "nonexistingorg/abcdfg/mobile",
                    url: "https://gitlab.com/nonexistingorg/abcdfg/mobile.git",
                  },
                  {
                    __typename: "OrganizationIntegrationRepositories",
                    branches: ["prod", "main"],
                    name: "nonexistingorg/abcdfg/backend",
                    url: "https://gitlab.com/nonexistingorg/abcdfg/backend.git",
                  },
                  {
                    __typename: "OrganizationIntegrationRepositories",
                    branches: ["main"],
                    name: "nonexistingorg/abcdfg/frontend",
                    url: "https://gitlab.com/nonexistingorg/abcdfg/frontend.git",
                  },
                  {
                    __typename: "OrganizationIntegrationRepositories",
                    branches: ["main"],
                    name: "nonexistingorg/mirror-abcdfg",
                    url: "https://gitlab.com/nonexistingorg/mirror-abcdfg.git",
                  },
                  {
                    __typename: "OrganizationIntegrationRepositories",
                    branches: ["main"],
                    name: "nonexistingorg/demo",
                    url: "https://gitlab.com/nonexistingorg/demo.git",
                  },
                  {
                    __typename: "OrganizationIntegrationRepositories",
                    branches: ["main"],
                    name: "nonexistingorg/data",
                    url: "https://gitlab.com/nonexistingorg/data.git",
                  },
                  {
                    __typename: "OrganizationIntegrationRepositories",
                    branches: ["main"],
                    name: "nonexistingorg/target_data",
                    url: "https://gitlab.com/nonexistingorg/target_data.git",
                  },
                  {
                    __typename: "OrganizationIntegrationRepositories",
                    branches: ["main"],
                    name: "nonexistingorg/time",
                    url: "https://gitlab.com/nonexistingorg/time.git",
                  },
                  {
                    __typename: "OrganizationIntegrationRepositories",
                    branches: ["master"],
                    name: "nonexistingorg/test",
                    url: "https://gitlab.com/nonexistingorg/test.git",
                  },
                  {
                    __typename: "OrganizationIntegrationRepositories",
                    branches: ["master"],
                    name: "nonexistingorg/testb",
                    url: "https://gitlab.com/nonexistingorg/testb.git",
                  },
                ],
              },
              name: "okada",
            },
          },
        });
      }

      return HttpResponse.json({
        errors: [new GraphQLError("Error getting roots")],
      });
    },
  ),
];

const Wrapper = ({
  children,
  mockedPermissions,
}: Readonly<{
  children: React.ReactNode;
  mockedPermissions: PureAbility<string>;
}>): JSX.Element => (
  <authzPermissionsContext.Provider value={mockedPermissions}>
    <Routes>
      <Route
        element={children}
        path={"/orgs/:organizationName/groups/:groupName/roots"}
      />
    </Routes>
  </authzPermissionsContext.Provider>
);

describe("gitRoots", (): void => {
  // eslint-disable-next-line jest/no-hooks
  beforeEach((): void => {
    jest.spyOn(console, "warn").mockImplementation();
  });

  it("should render tables", async (): Promise<void> => {
    expect.hasAssertions();

    render(
      <GitRoots groupName={"unittesting"} organizationName={"ort-test"} />,
      {
        memoryRouter: { initialEntries: ["/TEST"] },
        mocks: [finalGitRootsQueriesMockGlobal, finalGitRootsMockGlobal],
      },
    );

    await waitFor((): void => {
      expect(screen.queryAllByRole("table")).toHaveLength(2);
    });
  });

  it("should render action buttons", async (): Promise<void> => {
    expect.hasAssertions();

    render(
      <authzPermissionsContext.Provider
        value={
          new PureAbility([
            { action: "integrates_api_mutations_add_git_root_mutate" },
            { action: "integrates_api_mutations_update_git_root_mutate" },
            {
              action: "integrates_api_mutations_download_git_roots_file_mutate",
            },
            { action: "integrates_api_mutations_upload_git_root_file_mutate" },
          ])
        }
      >
        <GitRoots groupName={"unittesting"} organizationName={"ort-test"} />
      </authzPermissionsContext.Provider>,
      {
        memoryRouter: {
          initialEntries: ["/TEST"],
        },
        mocks: [initialGitRootsQueriesMockGlobal, initialGitRootsMockGlobal],
      },
    );

    await waitFor((): void => {
      expect(screen.queryAllByRole("button")).toHaveLength(6);
    });

    await userEvent.click(
      screen.getByRole("button", { name: "group.scope.common.add" }),
    );
    await waitFor((): void => {
      expect(
        screen.queryByText("components.repositoriesDropdown.manual.text"),
      ).toBeInTheDocument();
    });
    jest.clearAllMocks();
  });

  it("should render add file modal", async (): Promise<void> => {
    expect.hasAssertions();

    render(
      <authzPermissionsContext.Provider
        value={
          new PureAbility([
            { action: "integrates_api_mutations_add_git_root_mutate" },
            { action: "integrates_api_mutations_update_git_root_mutate" },
            {
              action: "integrates_api_mutations_download_git_roots_file_mutate",
            },
            { action: "integrates_api_mutations_upload_git_root_file_mutate" },
          ])
        }
      >
        <GitRoots groupName={"unittesting"} organizationName={"ort-test"} />
      </authzPermissionsContext.Provider>,
      {
        memoryRouter: {
          initialEntries: ["/TEST"],
        },
        mocks: [initialGitRootsQueriesMockGlobal, initialGitRootsMockGlobal],
      },
    );

    expect(
      screen.queryByText("group.scope.git.addGitRootFile.button"),
    ).toBeInTheDocument();

    await userEvent.click(
      screen.getByRole("button", {
        name: "group.scope.git.addGitRootFile.button",
      }),
    );
    await waitFor((): void => {
      expect(
        screen.queryByText("group.scope.git.addGitRootFile.modal.title"),
      ).toBeInTheDocument();
    });
    jest.clearAllMocks();
  });

  it("should add a git file modal", async (): Promise<void> => {
    expect.hasAssertions();

    const user = userEvent.setup();
    const text = async (): Promise<string> =>
      Promise.resolve(EXAMPLE_CSV.join("\n"));
    jest.spyOn(global, "File").mockImplementation(
      (_, fileName): File => ({
        ...global.File.prototype,
        name: fileName,
        text,
      }),
    );
    const file = new File(
      [new Blob([EXAMPLE_CSV.join("\n")])],
      "unittesting_git-roots_0134b6789.csv",
      { type: "text/csv" },
    );

    const mockUploadGitRootFile = graphqlMocked.mutation(
      UPLOAD_GIT_ROOT_FILE,
      (): StrictResponse<{ data: UploadGitRootFile }> => {
        return HttpResponse.json({
          data: {
            uploadGitRootFile: {
              errorLines: [],
              message: "Roots created: 1",
              success: true,
              totalErrors: 0,
              totalSuccess: 1,
            },
          },
        });
      },
    );
    render(
      <authzPermissionsContext.Provider
        value={
          new PureAbility([
            { action: "integrates_api_mutations_add_git_root_mutate" },
            { action: "integrates_api_mutations_upload_git_root_file_mutate" },
          ])
        }
      >
        <GitRoots groupName={"unittesting"} organizationName={"ort-test"} />
      </authzPermissionsContext.Provider>,
      {
        memoryRouter: {
          initialEntries: ["/unittesting"],
        },
        mocks: [
          initialGitRootsQueriesMockGlobal,
          initialGitRootsMockGlobal,
          mockUploadGitRootFile,
        ],
      },
    );

    expect(
      screen.queryByText("group.scope.git.addGitRootFile.button"),
    ).toBeInTheDocument();

    await user.click(
      screen.getByRole("button", {
        name: "group.scope.git.addGitRootFile.button",
      }),
    );
    await waitFor((): void => {
      expect(
        screen.queryByText("group.scope.git.addGitRootFile.modal.title"),
      ).toBeInTheDocument();
    });

    expect(
      screen.getByRole("button", {
        name: "group.scope.git.addGitRootFile.modal.button",
      }),
    ).toBeDisabled();

    await user.upload(screen.getByTestId("file"), file);

    expect(
      screen.getByText("unittesting_git-roots_0134b6789.csv"),
    ).toBeInTheDocument();
    expect(
      screen.getByRole("button", {
        name: "group.scope.git.addGitRootFile.modal.button",
      }),
    ).not.toBeDisabled();

    await user.click(
      screen.getByRole("button", {
        name: "group.scope.git.addGitRootFile.modal.button",
      }),
    );

    await waitFor((): void => {
      expect(screen.queryByText("Uploading...")).toBeInTheDocument();
    });

    jest.clearAllMocks();
  });

  it("should render action buttons and modal", async (): Promise<void> => {
    expect.hasAssertions();

    render(
      <authzPermissionsContext.Provider
        value={
          new PureAbility([
            { action: "integrates_api_mutations_add_git_root_mutate" },
            { action: "integrates_api_mutations_update_git_root_mutate" },
            {
              action: "integrates_api_mutations_download_git_roots_file_mutate",
            },
          ])
        }
      >
        <GitRoots groupName={"unittesting"} organizationName={"okada"} />
      </authzPermissionsContext.Provider>,
      {
        memoryRouter: { initialEntries: ["/TEST"] },
        mocks: [
          initialGitRootsQueriesMockGlobal,
          initialGitRootsMockGlobal,
          ...mockedOauthData,
        ],
      },
    );

    await waitFor((): void => {
      expect(screen.queryAllByRole("button")).toHaveLength(5);
    });

    await userEvent.click(
      screen.getByRole("button", { name: "group.scope.common.add" }),
    );
    await waitFor((): void => {
      expect(
        screen.queryByText("components.repositoriesDropdown.manual.text"),
      ).toBeInTheDocument();
    });

    expect(screen.getByText("GitLab")).toBeInTheDocument();
    expect(
      screen.queryByText("group.scope.git.oauthModal.title1"),
    ).not.toBeInTheDocument();

    await userEvent.click(screen.getByText("GitLab"));

    await waitFor((): void => {
      expect(
        screen.queryByText("group.scope.git.oauthModal.title1"),
      ).toBeInTheDocument();
    });

    expect(
      screen.getByRole("button", {
        name: "components.oauthRootForm.credentialSection.continue",
      }),
    ).toBeDisabled();

    await userEvent.click(
      screen.getByText(
        "components.oauthRootForm.credentialSection.placeholder",
      ),
    );
    await userEvent.click(
      screen.getByRole("listitem", {
        name: "6e52c11c-abf7-4ca3-b7d0-635e394f41c2",
      }),
    );

    await waitFor((): void => {
      expect(
        screen.getByRole("button", {
          name: "components.oauthRootForm.credentialSection.continue",
        }),
      ).not.toBeDisabled();
    });

    await userEvent.click(
      screen.getByRole("button", {
        name: "components.oauthRootForm.credentialSection.continue",
      }),
    );

    await waitFor(
      (): void => {
        expect(screen.getAllByRole("checkbox", { hidden: false })).toHaveLength(
          11,
        );
      },
      { timeout: 10000 },
    );

    await userEvent.type(
      screen.getByRole("textbox", { name: "reposByName" }),
      "abc",
    );
    await waitFor((): void => {
      expect(screen.getByRole("textbox", { name: "reposByName" })).toHaveValue(
        "abc",
      );
    });
    await waitFor(
      (): void => {
        expect(screen.getAllByRole("checkbox", { hidden: false })).toHaveLength(
          5,
        );
      },
      { timeout: 10000 },
    );

    await userEvent.clear(screen.getByRole("textbox", { name: "reposByName" }));
    await waitFor((): void => {
      expect(screen.getByRole("textbox", { name: "reposByName" })).toHaveValue(
        "",
      );
    });

    await userEvent.type(
      screen.getByRole("textbox", { name: "reposByName" }),
      "empty",
    );
    await waitFor((): void => {
      expect(screen.getByRole("textbox", { name: "reposByName" })).toHaveValue(
        "empty",
      );
    });
    await waitFor(
      (): void => {
        expect(screen.getAllByRole("checkbox", { hidden: false })).toHaveLength(
          1,
        );
      },
      { timeout: 10000 },
    );

    await userEvent.clear(screen.getByRole("textbox", { name: "reposByName" }));
    await waitFor((): void => {
      expect(screen.getByRole("textbox", { name: "reposByName" })).toHaveValue(
        "",
      );
    });
    await waitFor(
      (): void => {
        expect(screen.getAllByRole("checkbox")).toHaveLength(11);
      },
      { timeout: 10000 },
    );
    jest.clearAllMocks();
  }, 60000);

  it("should render git modal", async (): Promise<void> => {
    expect.hasAssertions();

    const handleClose: jest.Mock = jest.fn();
    const handleSubmit: jest.Mock = jest.fn();
    render(
      <authzGroupContext.Provider
        value={new PureAbility([{ action: "has_advanced" }])}
      >
        <authzPermissionsContext.Provider
          value={
            new PureAbility([
              { action: "update_git_root_filter" },
              { action: "integrates_api_mutations_add_secret_mutate" },
              { action: "integrates_api_mutations_update_git_root_mutate" },
            ])
          }
        >
          <ManagementModal
            externalId={"id"}
            groupName={""}
            isEditing={false}
            manyRows={false}
            modalMessages={{ message: "", type: "success" }}
            onClose={handleClose}
            onSubmitRepo={handleSubmit}
            organizationName={""}
          />
        </authzPermissionsContext.Provider>
      </authzGroupContext.Provider>,
    );

    // Repository fields
    await waitFor((): void => {
      expect(screen.getByRole("combobox", { name: "url" })).toBeInTheDocument();
    });

    expect(
      screen.getByRole("combobox", { name: "branch" }),
    ).toBeInTheDocument();

    // Health Check
    expect(
      screen.queryAllByRole("checkbox", { name: "healthCheckConfirm" }),
    ).toHaveLength(0);

    await userEvent.clear(screen.getByRole("combobox", { name: "url" }));
    await userEvent.type(
      screen.getByRole("combobox", { name: "url" }),
      "https://gitlab.com/fluidattacks/universe",
    );

    await userEvent.click(screen.getAllByRole("radio", { name: "Yes" })[1]);

    await waitFor((): void => {
      expect(
        screen.queryAllByRole("checkbox", { name: "healthCheckConfirm" }),
      ).toHaveLength(1);
    });

    await userEvent.click(screen.getAllByRole("radio", { name: "Yes" })[0]);

    // Filters
    await waitFor((): void => {
      expect(
        screen.getByRole("combobox", { name: "gitignore.0" }),
      ).toBeInTheDocument();
    });

    jest.clearAllMocks();
  });

  it("should render envs modal", async (): Promise<void> => {
    expect.hasAssertions();

    const handleClose: jest.Mock = jest.fn();
    const handleSubmit: jest.Mock = jest.fn();
    const initialValues: IFormValues = {
      ...formInitialValues,
      gitEnvironmentUrls: [
        {
          id: "adc83b19e793491b1c6ea0fd8b46cd9f32e592fc",
          rootId: "",
          secrets: [],
          url: "",
          urlType: "URL",
        },
      ],
      includesHealthCheck: false,
      url: "https://gitlab.com/fluidattacks/universe",
    };

    render(
      <authzPermissionsContext.Provider
        value={
          new PureAbility([
            { action: "integrates_api_mutations_add_secret_mutate" },
            { action: "integrates_api_mutations_update_git_root_mutate" },
            { action: "integrates_api_resolvers_git_root_secrets_resolve" },
            {
              action:
                "integrates_api_mutations_update_git_root_environment_url_mutate",
            },
          ])
        }
      >
        <ManagementModal
          externalId={"id"}
          groupName={""}
          initialValues={initialValues}
          isEditing={true}
          manyRows={false}
          modalMessages={{ message: "", type: "success" }}
          onClose={handleClose}
          onSubmitRepo={handleSubmit}
          organizationName={""}
        />
      </authzPermissionsContext.Provider>,
      {
        mocks: [
          graphqlMocked.query(
            GET_ROOT_ENVIRONMENT_URLS,
            (): StrictResponse<
              IErrorMessage | { data: GetRootEnvironmentUrls }
            > => {
              return HttpResponse.json({
                data: {
                  root: {
                    __typename: "GitRoot",
                    gitEnvironmentUrls: [
                      {
                        id: "adc83b19e793491b1c6ea0fd8b46cd9f32e592fc",
                        include: false,
                        rootId: "c7118ab2-9345-4398-895f-11ce6e3cc1a6",
                        url: "https://app.fluidattacks.com/test",
                        urlType: "URL",
                        useEgress: false,
                        useVpn: false,
                        useZtna: false,
                      },
                    ],
                    id: "c7118ab2-9345-4398-895f-11ce6e3cc1a6",
                  },
                },
              });
            },
          ),
        ],
      },
    );

    // Repository fields
    await waitFor((): void => {
      expect(screen.getByRole("combobox", { name: "url" })).toBeInTheDocument();
    });

    expect(screen.getByRole("combobox", { name: "url" })).toHaveValue(
      "https://gitlab.com/fluidattacks/universe",
    );

    await waitFor((): void => {
      expect(screen.queryAllByRole("link")).toHaveLength(3);
    });

    await userEvent.click(
      screen.getByRole("link", { name: "group.scope.git.envUrls" }),
    );

    expect(screen.queryByText("group.scope.git.envUrl")).toBeInTheDocument();
    expect(
      screen.queryByText("group.scope.git.envUrlType"),
    ).toBeInTheDocument();
    expect(
      screen.queryByText("group.scope.git.exclusionStatus"),
    ).toBeInTheDocument();

    await userEvent.click(screen.getByRole("checkbox"));
    await waitFor((): void => {
      expect(
        screen.queryByRole("button", { name: "Confirm" }),
      ).toBeInTheDocument();
    });

    expect(
      screen.queryByText("table.formatters.include.confirm.message"),
    ).not.toBeInTheDocument();

    jest.clearAllMocks();
  });

  it("should render add secrets modal", async (): Promise<void> => {
    expect.hasAssertions();

    const handleClose: jest.Mock = jest.fn();
    const handleSubmit: jest.Mock = jest.fn();
    const initialValues: IFormValues = {
      ...formInitialValues,
      url: "https://gitlab.com/fluidattacks/universe",
    };

    const mockedRoot = graphqlMocked.query(
      GET_ROOT_SECRETS,
      ({
        variables,
      }): StrictResponse<IErrorMessage | { data: GetRootQuery }> => {
        const { groupName, rootId } = variables;
        if (
          groupName === "test-group" &&
          rootId === "c7118ab2-9345-4398-895f-11ce6e3cc1a6"
        ) {
          return HttpResponse.json({
            data: {
              root: {
                __typename: "GitRoot",
                id: "",
                secrets: [
                  {
                    description: "",
                    key: "",
                    owner: "",
                  },
                ],
              },
            },
          });
        }

        return HttpResponse.json({
          errors: [new GraphQLError("Error getting root environment urls")],
        });
      },
    );

    render(
      <authzPermissionsContext.Provider
        value={
          new PureAbility([
            { action: "integrates_api_mutations_add_secret_mutate" },
            { action: "integrates_api_mutations_update_secret_mutate" },
            { action: "integrates_api_mutations_update_git_root_mutate" },
            { action: "integrates_api_resolvers_git_root_secrets_resolve" },
          ])
        }
      >
        <ManagementModal
          externalId={"adc83b19e793491b1c6ea0fd8b46cd9f32e592fc"}
          groupName={"test-group"}
          initialValues={initialValues}
          isEditing={true}
          manyRows={false}
          modalMessages={{ message: "", type: "success" }}
          onClose={handleClose}
          onSubmitRepo={handleSubmit}
          organizationName={""}
        />
      </authzPermissionsContext.Provider>,
      {
        mocks: [mockedRoot],
      },
    );

    const btnConfirm = "Confirm";

    await userEvent.click(
      screen.getByRole("link", { name: "group.scope.git.repo.secrets" }),
    );

    expect(
      screen.queryByText("group.scope.git.repo.credentials.secrets.key"),
    ).toBeInTheDocument();
    expect(
      screen.queryByText(
        "group.scope.git.repo.credentials.secrets.description",
      ),
    ).toBeInTheDocument();

    await userEvent.click(
      screen.getByRole("button", {
        name: "group.scope.git.repo.credentials.secrets.add",
      }),
    );

    await waitFor((): void => {
      expect(
        screen.queryByText("group.scope.git.repo.credentials.secrets.title"),
      ).toBeInTheDocument();
    });

    expect(screen.getByRole("textbox", { name: "key" })).toBeInTheDocument();
    expect(screen.getByRole("textbox", { name: "value" })).toBeInTheDocument();
    expect(
      screen.getByRole("textbox", { name: "description" }),
    ).toBeInTheDocument();

    expect(screen.getByRole("button", { name: btnConfirm })).toBeDisabled();

    await userEvent.type(
      screen.getByRole("textbox", { name: "key" }),
      "test add secret key",
    );

    await userEvent.type(
      screen.getByRole("textbox", { name: "value" }),
      "testing add secret",
    );

    await userEvent.type(
      screen.getByRole("textbox", { name: "description" }),
      "test description",
    );

    await waitFor((): void => {
      expect(
        screen.getByRole("button", { name: btnConfirm }),
      ).not.toBeDisabled();
    });

    jest.clearAllMocks();
  });

  it("should update environment exclusion", async (): Promise<void> => {
    expect.hasAssertions();

    const handleClose: jest.Mock = jest.fn();
    const handleSubmit: jest.Mock = jest.fn();
    const initialValues: IFormValues = {
      ...formInitialValues,
      gitEnvironmentUrls: [
        {
          id: "adc83b19e793491b1c6ea0fd8b46cd9f32e592fc",
          rootId: "",
          secrets: [],
          url: "",
          urlType: "URL",
        },
      ],
      id: "c7118ab2-9345-4398-895f-11ce6e3cc1a6",
      includesHealthCheck: false,
      url: "https://gitlab.com/fluidattacks/universe",
    };

    const mockedMutation = graphqlMocked.mutation(
      UPDATE_ENVIRONMENT_URL,
      ({
        variables,
      }): StrictResponse<
        IErrorMessage | { data: UpdateGitRootEnvironmentUrl }
      > => {
        const { groupName, include, rootId, urlId } = variables;
        if (
          groupName === "test-group" &&
          include === false &&
          rootId === "c7118ab2-9345-4398-895f-11ce6e3cc1a6" &&
          urlId === "adc83b19e793491b1c6ea0fd8b46cd9f32e592fc"
        ) {
          return HttpResponse.json({
            data: {
              updateGitRootEnvironmentUrl: { success: true },
            },
          });
        }

        return HttpResponse.json({
          errors: [
            new GraphQLError(
              "An error occurred updating root environment urls",
            ),
          ],
        });
      },
    );
    const mockedRoot1 = graphqlMocked.query(
      GET_ROOT_ENVIRONMENT_URLS_SECRETS,
      ({
        variables,
      }): StrictResponse<
        IErrorMessage | { data: GetRootEnvironmentUrlsSecrets }
      > => {
        const { groupName, rootId } = variables;
        if (
          groupName === "test-group" &&
          rootId === "c7118ab2-9345-4398-895f-11ce6e3cc1a6"
        ) {
          return HttpResponse.json({
            data: {
              root: {
                __typename: "GitRoot",
                gitEnvironmentUrls: [
                  {
                    __typename: "GitEnvironmentUrl",
                    id: "",
                    rootId: "",
                    secrets: [],
                    url: "",
                    urlType: "URL",
                  },
                ],
              },
            },
          });
        }

        return HttpResponse.json({
          errors: [new GraphQLError("Error getting root environment urls")],
        });
      },
    );

    render(
      <authzPermissionsContext.Provider
        value={
          new PureAbility([
            { action: "integrates_api_mutations_add_secret_mutate" },
            { action: "integrates_api_mutations_update_git_root_mutate" },
            {
              action:
                "integrates_api_mutations_update_git_root_environment_url_mutate",
            },
          ])
        }
      >
        <ManagementModal
          externalId={"id"}
          groupName={"test-group"}
          initialValues={initialValues}
          isEditing={true}
          manyRows={false}
          modalMessages={{ message: "", type: "success" }}
          onClose={handleClose}
          onSubmitRepo={handleSubmit}
          organizationName={""}
        />
      </authzPermissionsContext.Provider>,
      {
        memoryRouter: {
          initialEntries: ["/environments"],
        },
        mocks: [mockedRoot1, mockedEnvironments, mockedMutation],
      },
    );

    await waitFor((): void => {
      expect(
        screen.getByRole("link", { name: /group\.scope\.git\.envurls/iu }),
      ).toBeInTheDocument();
    });
    await userEvent.click(
      screen.getByRole("link", { name: /group\.scope\.git\.envurls/iu }),
    );
    await waitFor((): void => {
      expect(
        screen.getByText(/https:\/\/app\.fluidattacks\.com\/test/iu),
      ).toBeInTheDocument();
    });
    await userEvent.click(screen.getByRole("checkbox"));
    await waitFor((): void => {
      expect(
        screen.queryByText("table.formatters.include.confirm.message"),
      ).toBeInTheDocument();
    });
    await userEvent.click(screen.getByRole("button", { name: "Confirm" }));
    await waitFor((): void => {
      expect(msgSuccess).toHaveBeenCalledWith(
        "group.scope.git.updateEnvironment.success",
        "group.scope.git.updateEnvironment.successTitle",
      );
    });

    jest.clearAllMocks();
  });

  it("should display environment file", async (): Promise<void> => {
    expect.hasAssertions();

    const handleClose: jest.Mock = jest.fn();
    const handleSubmit: jest.Mock = jest.fn();
    const file: File = new File([""], "test_2_1.zip", {
      type: "application/zip",
    });
    const initialValues: IFormValues = {
      ...formInitialValues,
      gitEnvironmentUrls: [
        {
          id: "adc83b19e793491b1c6ea0fd8b46cd9f32e592fc",
          rootId: "",
          secrets: [],
          url: "",
          urlType: "APK",
        },
      ],
      id: "c7118ab2-9345-4398-895f-11ce6e3cc1a6",
      includesHealthCheck: false,
      url: "test.apk",
    };
    const server = newServer({
      post: [
        "https://mocked.test",
        {
          status: 200,
        },
      ],
    });

    server.install();

    const mockedRoot1 = graphqlMocked.query(
      GET_ROOT_ENVIRONMENT_URLS_SECRETS,
      ({
        variables,
      }): StrictResponse<
        IErrorMessage | { data: GetRootEnvironmentUrlsSecrets }
      > => {
        const { groupName, rootId } = variables;
        if (
          groupName === "test-group" &&
          rootId === "c7118ab2-9345-4398-895f-11ce6e3cc1a6"
        ) {
          return HttpResponse.json({
            data: {
              root: {
                __typename: "GitRoot",
                gitEnvironmentUrls: [
                  {
                    __typename: "GitEnvironmentUrl",
                    id: "",
                    rootId: "",
                    secrets: [],
                    url: "test.apk",
                    urlType: "APK",
                  },
                ],
              },
            },
          });
        }

        return HttpResponse.json({
          errors: [new GraphQLError("Error getting root environment urls")],
        });
      },
    );

    render(
      <authzPermissionsContext.Provider
        value={
          new PureAbility([
            { action: "integrates_api_mutations_add_secret_mutate" },
            {
              action:
                "integrates_api_mutations_update_git_root_environment_file_mutate",
            },
            { action: "integrates_api_mutations_update_git_root_mutate" },
            {
              action:
                "integrates_api_mutations_update_git_root_environment_url_mutate",
            },
          ])
        }
      >
        <ManagementModal
          externalId={"id"}
          groupName={"test-group"}
          initialValues={initialValues}
          isEditing={true}
          manyRows={false}
          modalMessages={{ message: "", type: "success" }}
          onClose={handleClose}
          onSubmitRepo={handleSubmit}
          organizationName={""}
        />
      </authzPermissionsContext.Provider>,
      {
        memoryRouter: {
          initialEntries: ["/environments"],
        },
        mocks: [
          mockedRoot1,
          graphqlMocked.query(
            GET_ROOT_ENVIRONMENT_URLS,
            ({
              variables,
            }): StrictResponse<
              IErrorMessage | { data: GetRootEnvironmentUrls }
            > => {
              const { groupName, rootId } = variables;
              if (
                groupName === "test-group" &&
                rootId === "c7118ab2-9345-4398-895f-11ce6e3cc1a6"
              ) {
                return HttpResponse.json({
                  data: {
                    root: {
                      __typename: "GitRoot",
                      gitEnvironmentUrls: [
                        {
                          id: "adc83b19e793491b1c6ea0fd8b46cd9f32e592fc",
                          include: true,
                          rootId: "c7118ab2-9345-4398-895f-11ce6e3cc1a6",
                          url: "test.apk",
                          urlType: "APK",
                          useEgress: false,
                          useVpn: false,
                          useZtna: false,
                        },
                      ],
                      id: "c7118ab2-9345-4398-895f-11ce6e3cc1a6",
                    },
                  },
                });
              }

              return HttpResponse.json({
                errors: [
                  new GraphQLError("Error getting root environment urls"),
                ],
              });
            },
            { once: true },
          ),
          graphqlMocked.mutation(
            SIGN_POST_URL_MUTATION,
            ({
              variables,
            }): StrictResponse<
              IErrorMessage | { data: SignPostUrlMutationMutation }
            > => {
              const { filesDataInput, groupName } = variables;
              if (
                Array.isArray(filesDataInput) &&
                filesDataInput[0].fileName === "test_2_1.zip" &&
                groupName === "test-group"
              ) {
                return HttpResponse.json({
                  data: {
                    signPostUrl: {
                      success: true,
                      url: {
                        fields: {
                          algorithm: "",
                          credential: "",
                          date: "",
                          key: "",
                          policy: "",
                          securitytoken: "",
                          signature: "",
                        },
                        url: "https://mocked.test",
                      },
                    },
                  },
                });
              }

              return HttpResponse.json({
                errors: [new GraphQLError("Error signing urls")],
              });
            },
          ),
          graphqlMocked.mutation(
            UPDATE_GIT_ROOT_ENVIRONMENT_FILE,
            (): StrictResponse<
              IErrorMessage | { data: UpdateGitRootEnvironmentFileMutation }
            > => {
              return HttpResponse.json({
                data: {
                  updateGitRootEnvironmentFile: {
                    success: true,
                  },
                },
              });
            },
          ),
          graphqlMocked.query(
            GET_FILES,
            (): StrictResponse<{ data: GetFilesQueryQuery }> => {
              return HttpResponse.json({
                data: {
                  resources: {
                    files: [
                      {
                        description: "",
                        fileName: "test_1_1.zip",
                        uploadDate: "2019-03-01 15:21",
                        uploader: "unittest@fluidattacks.com",
                      },
                    ],
                  },
                },
              });
            },
          ),
          graphqlMocked.query(
            GET_GIT_ROOTS_QUERIES,
            (): StrictResponse<{ data: GetGitRootsViewQueriesQuery }> => {
              return HttpResponse.json({
                data: {
                  __typename: "Query",
                  ...{
                    group: {
                      __typename: "Group" as const,
                      gitEnvironmentUrls: [
                        {
                          __typename: "GitEnvironmentUrl" as const,
                          cloudName: null,
                          createdAt: new Date("2022-04-27T17:30:07.230355"),
                          createdBy: null,
                          id: "3f6eb6274ec7dc2855451c0fbb4ff9485360be5b",
                          include: true,
                          root: {
                            id: "c7118ab2-9345-4398-895f-11ce6e3cc1a6",
                            state: "ACTIVE",
                            url: "https://gitlab.com/fluidattacks/universe",
                          },
                          rootId: "c7118ab2-9345-4398-895f-11ce6e3cc1a6",
                          url: "test_1_1.zip",
                          urlType: "APK",
                          useEgress: false,
                          useVpn: false,
                          useZtna: false,
                        },
                      ],
                      name: "unittesting",
                    },
                    organizationId: gitRootsQueriesDataMock.organizationId,
                  },
                },
              });
            },
          ),
          graphqlMocked.query(
            GET_ROOT_ENVIRONMENT_URLS,
            ({
              variables,
            }): StrictResponse<
              IErrorMessage | { data: GetRootEnvironmentUrls }
            > => {
              const { groupName, rootId } = variables;
              if (
                groupName === "test-group" &&
                rootId === "c7118ab2-9345-4398-895f-11ce6e3cc1a6"
              ) {
                return HttpResponse.json({
                  data: {
                    root: {
                      __typename: "GitRoot",
                      gitEnvironmentUrls: [
                        {
                          id: "adc83b19e793491b1c6ea0fd8b46cd9f32e592fc",
                          include: true,
                          rootId: "c7118ab2-9345-4398-895f-11ce6e3cc1a6",
                          url: "test_1_1.zip",
                          urlType: "APK",
                          useEgress: false,
                          useVpn: false,
                          useZtna: false,
                        },
                      ],
                      id: "c7118ab2-9345-4398-895f-11ce6e3cc1a6",
                    },
                  },
                });
              }

              return HttpResponse.json({
                errors: [
                  new GraphQLError("Error getting root environment urls"),
                ],
              });
            },
          ),
        ],
      },
    );

    await waitFor((): void => {
      expect(
        screen.getByRole("link", { name: /group\.scope\.git\.envurls/iu }),
      ).toBeInTheDocument();
    });
    await userEvent.click(
      screen.getByRole("link", { name: /group\.scope\.git\.envurls/iu }),
    );

    await waitFor((): void => {
      expect(screen.getByText("test.apk")).toBeInTheDocument();
    });

    expect(
      screen.queryByText("searchFindings.tabResources.files.form.button"),
    ).not.toBeInTheDocument();

    await userEvent.click(
      screen.getByRole("button", { name: "git-root-edit-environment-url" }),
    );
    await waitFor((): void => {
      expect(
        screen.queryByText("searchFindings.tabResources.files.form.button"),
      ).toBeInTheDocument();
    });

    expect(
      screen.queryByRole("button", {
        name: "searchFindings.tabResources.files.form.button",
      }),
    ).not.toBeInTheDocument();

    await userEvent.upload(screen.getByTestId("file"), file);

    await waitFor((): void => {
      expect(
        screen.queryByRole("button", {
          name: "searchFindings.tabResources.files.form.button",
        }),
      ).toBeInTheDocument();
    });

    expect(
      screen.queryByText("group.scope.git.updateEnvironment.warning"),
    ).not.toBeInTheDocument();

    await userEvent.click(
      screen.getByRole("button", {
        name: "searchFindings.tabResources.files.form.button",
      }),
    );
    await waitFor((): void => {
      expect(
        screen.queryByText("group.scope.git.updateEnvironment.warning"),
      ).toBeInTheDocument();
    });

    expect(
      within(screen.getByRole("table")).queryByText("test.apk"),
    ).toBeInTheDocument();

    await userEvent.click(screen.getByRole("button", { name: "Confirm" }));
    await waitFor((): void => {
      expect(msgSuccess).toHaveBeenCalledWith(
        "searchFindings.tabResources.files.form.success",
        "searchFindings.tabUsers.titleSuccess",
      );
    });
    await waitFor((): void => {
      expect(
        within(screen.getByRole("table")).queryByText("test.apk"),
      ).not.toBeInTheDocument();
    });

    expect(
      within(screen.getByRole("table")).queryByText("test_1_1.zip"),
    ).toBeInTheDocument();

    server.remove();
    jest.clearAllMocks();
  });

  it("should download the git roots file", async (): Promise<void> => {
    expect.hasAssertions();

    const open: jest.Mock = jest.fn();
    open.mockReturnValue({ opener: "" });
    // eslint-disable-next-line functional/immutable-data
    window.open = open;
    const mocksMutation = graphqlMocked.mutation(
      DOWNLOAD_GIT_ROOTS_FILE_MUTATION,
      ({
        variables,
      }): StrictResponse<IErrorMessage | { data: DownloadFile }> => {
        const { groupName } = variables;
        if (groupName === "TEST") {
          return HttpResponse.json({
            data: {
              downloadGitRootsFile: {
                success: true,
                url: "https://test.com/file",
              },
            },
          });
        }

        return HttpResponse.json({
          errors: [new GraphQLError("Unable to upload file to S3 service")],
        });
      },
    );
    const mockedPermissions = new PureAbility<string>([
      { action: "integrates_api_mutations_download_git_roots_file_mutate" },
    ]);

    render(
      <Wrapper mockedPermissions={mockedPermissions}>
        <GitRoots groupName={"TEST"} organizationName={"ort-test"} />
      </Wrapper>,
      {
        memoryRouter: {
          initialEntries: ["/orgs/okada/groups/TEST/roots"],
        },
        mocks: [
          initialGitRootsMockGlobal,
          initialGitRootsQueriesMockGlobal,
          mocksMutation,
        ],
      },
    );
    await waitFor((): void => {
      expect(screen.getByText(/buttons\.export/iu)).toBeInTheDocument();
    });
    await userEvent.click(screen.getByText(/buttons\.export/iu));
    await waitFor((): void => {
      expect(open).toHaveBeenCalledWith(
        "https://test.com/file",
        undefined,
        "noopener,noreferrer,",
      );
    });
  });

  it("should render error when download the git roots file", async (): Promise<void> => {
    expect.hasAssertions();

    const mocksMutation = graphqlMocked.mutation(
      DOWNLOAD_GIT_ROOTS_FILE_MUTATION,
      ({
        variables,
      }): StrictResponse<
        Record<"errors", IMessage[]> | { data: DownloadFile }
      > => {
        const { groupName } = variables;
        if (groupName === "TEST") {
          return HttpResponse.json({
            errors: [
              new GraphQLError("Unable to upload file to S3 service"),
              new GraphQLError("Unable to download the requested file"),
              new GraphQLError("Error dowloading file"),
            ],
          });
        }

        return HttpResponse.json({
          data: {
            downloadGitRootsFile: {
              success: true,
              url: "https://test.com/file",
            },
          },
        });
      },
    );
    const mockedPermissions = new PureAbility<string>([
      { action: "integrates_api_mutations_download_git_roots_file_mutate" },
    ]);

    render(
      <Wrapper mockedPermissions={mockedPermissions}>
        <GitRoots groupName={"TEST"} organizationName={"ort-test"} />
      </Wrapper>,
      {
        memoryRouter: {
          initialEntries: ["/orgs/okada/groups/TEST/roots"],
        },
        mocks: [
          initialGitRootsMockGlobal,
          initialGitRootsQueriesMockGlobal,
          mocksMutation,
        ],
      },
    );
    await waitFor((): void => {
      expect(screen.getByText(/buttons\.export/iu)).toBeInTheDocument();
    });
    await userEvent.click(screen.getByText(/buttons\.export/iu));
    await waitFor((): void => {
      expect(msgError).toHaveBeenCalledWith("groupAlerts.errorTextsad");
    });

    await waitFor((): void => {
      expect(msgError).toHaveBeenCalledTimes(3);
    });
  });

  it("should display description", async (): Promise<void> => {
    expect.hasAssertions();

    const mockedPermissions = new PureAbility<string>([]);
    const mockedRootDetails = graphqlMocked.query(
      GET_GIT_ROOT_DETAILS,
      (): StrictResponse<{ data: GetGitRootDetailsQuery }> => {
        return HttpResponse.json({
          data: {
            root: {
              __typename: "GitRoot",
              cloningStatus: {
                message: "Cloning message test",
                modifiedDate: "2022-02-10T14:58:10+00:00",
                status: CloningStatus.Unknown,
              },
              gitignore: ["ignore"],
              id: "4039d098-ffc5-4984-8ed3-eb17bca98e19",
              lastStateStatusUpdate: "2022-02-09T14:58:10+00:00",
            },
          },
        });
      },
    );

    render(
      <Wrapper mockedPermissions={mockedPermissions}>
        <GitRoots groupName={"unittesting"} organizationName={"ort-test"} />
      </Wrapper>,
      {
        memoryRouter: {
          initialEntries: ["/orgs/okada/groups/TEST/roots"],
        },
        mocks: [
          finalGitRootsMockGlobal,
          finalGitRootsQueriesMockGlobal,
          mockedRootDetails,
        ],
      },
    );
    await waitFor((): void => {
      expect(
        screen.getByText(
          /https:\/\/gitlab\.com\/fluidattacks\/universe\.git/iu,
        ),
      ).toBeInTheDocument();
    });
    const cell = screen.getByRole("cell", {
      name: /https:\/\/gitlab\.com\/fluidattacks\/universe\.git/iu,
    });

    const arrow = within(cell).getByTestId("angle-down-icon");
    await userEvent.click(arrow);

    expect(
      screen.getByText(
        /group\.scope\.git\.repo\.cloning\.message: cloning message test/iu,
      ),
    ).toBeInTheDocument();
    expect(screen.getByText(/ignore/iu)).toBeInTheDocument();
    expect(
      screen.getAllByText(/https:\/\/app\.fluidattacks\.com/iu)[0],
    ).toBeInTheDocument();
    expect(
      screen.getByText(
        /group\.scope\.common\.lastcloningstatusupdate: 2022-02-10 02:58:10 pm/iu,
      ),
    ).toBeInTheDocument();
    expect(
      screen.getByText(
        /group\.scope\.common\.laststatestatusupdate: 2022-02-09 02:58:10 pm/iu,
      ),
    ).toBeInTheDocument();
  });

  it("should open edit root modal", async (): Promise<void> => {
    expect.hasAssertions();

    const mockedPermissions = new PureAbility<string>([
      { action: "integrates_api_mutations_update_git_root_mutate" },
      { action: "integrates_api_resolvers_git_root_secrets_resolve" },
    ]);
    const mockedRootManagement = graphqlMocked.query(
      GET_GIT_ROOT_MANAGEMENT_DETAILS,
      (): StrictResponse<{
        data: RootManagementDetails;
      }> => {
        return HttpResponse.json({
          data: {
            __typename: "Query",
            root: {
              __typename: "GitRoot" as const,
              branch: "master",
              cloningStatus: {
                __typename: "GitRootCloningStatus" as const,
                message: "Cloning message test",
                status: CloningStatus.Unknown,
              },
              credentials: {
                __typename: "Credentials" as const,
                id: "",
                isToken: false,
                name: "",
                type: "",
              },
              criticality: RootCriticality.Medium,
              gitEnvironmentUrls: [],
              gitignore: [],
              id: "4039d098-ffc5-4984-8ed3-eb17bca98e19",
              includesHealthCheck: false,
              nickname: "universe",
              state: ResourceState.Active,
              url: "https://gitlab.com/fluidattacks/universe.git",
              useEgress: false,
              useVpn: false,
              useZtna: false,
            },
          },
        });
      },
    );

    const mockedEnvironmentRoot = graphqlMocked.query(
      GET_ROOT_ENVIRONMENT_URLS,
      ({
        variables,
      }): StrictResponse<IErrorMessage | { data: GetRootEnvironmentUrls }> => {
        const { groupName, rootId } = variables;
        if (
          groupName === "test-group" &&
          rootId === "4039d098-ffc5-4984-8ed3-eb17bca98e19"
        ) {
          return HttpResponse.json({
            data: {
              root: {
                __typename: "GitRoot",
                gitEnvironmentUrls: [
                  {
                    id: "adc83b19e793491b1c6ea0fd8b46cd9f32e592fc",
                    include: true,
                    rootId: "4039d098-ffc5-4984-8ed3-eb17bca98e19",
                    url: "https://app.fluidattacks.com/test",
                    urlType: "URL",
                    useEgress: false,
                    useVpn: false,
                    useZtna: false,
                  },
                ],
                id: "4039d098-ffc5-4984-8ed3-eb17bca98e19",
              },
            },
          });
        }

        return HttpResponse.json({
          errors: [new GraphQLError("Error getting root environment urls")],
        });
      },
    );
    render(
      <Wrapper mockedPermissions={mockedPermissions}>
        <GitRoots groupName={"unittesting"} organizationName={"ort-test"} />
      </Wrapper>,
      {
        memoryRouter: {
          initialEntries: ["/orgs/okada/groups/TEST/roots"],
        },
        mocks: [
          finalGitRootsMockGlobal,
          finalGitRootsQueriesMockGlobal,
          mockedRootManagement,
          mockedEnvironmentRoot,
        ],
      },
    );
    await waitFor((): void => {
      expect(
        screen.getByText(
          /https:\/\/gitlab\.com\/fluidattacks\/universe\.git/iu,
        ),
      ).toBeInTheDocument();
    });
    const cell = screen.getByRole("cell", {
      name: /https:\/\/gitlab\.com\/fluidattacks\/universe\.git/iu,
    });

    await userEvent.click(cell);

    expect(
      screen.getByText(/group\.scope\.common\.edit/iu),
    ).toBeInTheDocument();
    expect(
      screen.getByRole("link", { name: "group.scope.git.repo.title" }),
    ).toBeInTheDocument();
    expect(
      screen.getByRole("link", { name: "group.scope.git.envUrls" }),
    ).toBeInTheDocument();
    expect(
      screen.getByRole("link", { name: "group.scope.git.repo.secrets" }),
    ).toBeInTheDocument();

    await userEvent.click(
      screen.getByRole("link", { name: "group.scope.git.envUrls" }),
    );

    await waitFor((): void => {
      expect(screen.queryAllByRole("table")).toHaveLength(3);
    });
  });

  it("should render sync root button", async (): Promise<void> => {
    expect.hasAssertions();

    const mockedSyncMutation = graphqlMocked.mutation(
      SYNC_GIT_ROOT,
      ({
        variables,
      }): StrictResponse<IErrorMessage | { data: SyncGitRoot }> => {
        const { groupName, rootId } = variables;

        if (
          groupName === "unittesting" &&
          rootId !== "4039d098-ffc5-4984-8ed3-eb17bca98e19"
        ) {
          return HttpResponse.json({
            data: {
              syncGitRoot: { success: true },
            },
          });
        }

        return HttpResponse.json({
          errors: [new GraphQLError("Couldn't queue root cloning")],
        });
      },
    );
    const mockedPermissions = new PureAbility<string>([
      { action: "integrates_api_mutations_sync_git_root_mutate" },
      { action: "integrates_api_mutations_activate_root_mutate" },
    ]);

    render(
      <Wrapper mockedPermissions={mockedPermissions}>
        <GitRoots groupName={"unittesting"} organizationName={"ort-test"} />
      </Wrapper>,
      {
        memoryRouter: {
          initialEntries: ["/orgs/ort-test/groups/TEST/roots"],
        },
        mocks: [
          finalGitRootsMockGlobal,
          finalGitRootsQueriesMockGlobal,
          mockedSyncMutation,
        ],
      },
    );
    await waitFor((): void => {
      expect(
        screen.getByText(/https:\/\/github\.com\/fluidattacks\/integrates/iu),
      ).toBeInTheDocument();
    });

    const syncButton = document.querySelectorAll("#gitRootSync");

    expect(syncButton[0]).toBeDisabled();
    expect(syncButton[1]).not.toBeDisabled();

    await userEvent.click(syncButton[1]);

    await waitFor((): void => {
      expect(msgSuccess).toHaveBeenCalledWith(
        "group.scope.git.sync.success",
        "group.scope.git.sync.successTitle",
      );
    });
    jest.clearAllMocks();
  }, 120000);

  it("should handle error sync root", async (): Promise<void> => {
    expect.hasAssertions();

    const mockedSyncMutation = graphqlMocked.mutation(
      SYNC_GIT_ROOT,
      ({
        variables,
      }): StrictResponse<IErrorMessage | { data: SyncGitRoot }> => {
        const { groupName, rootId } = variables;

        if (
          groupName === "unittesting" &&
          rootId === "9eccb9ed-e835-4dbc-b28e-8cecd0296e27"
        ) {
          return HttpResponse.json({
            errors: [
              new GraphQLError(
                "Exception - The root already has an active cloning process",
              ),
            ],
          });
        }

        return HttpResponse.json({
          data: {
            syncGitRoot: { success: true },
          },
        });
      },
    );
    const mockedPermissions = new PureAbility<string>([
      { action: "integrates_api_mutations_sync_git_root_mutate" },
      { action: "integrates_api_mutations_activate_root_mutate" },
    ]);

    render(
      <Wrapper mockedPermissions={mockedPermissions}>
        <GitRoots groupName={"unittesting"} organizationName={"ort-test"} />
      </Wrapper>,
      {
        memoryRouter: {
          initialEntries: ["/orgs/okada/groups/TEST/roots"],
        },
        mocks: [
          finalGitRootsMockGlobal,
          finalGitRootsQueriesMockGlobal,
          mockedSyncMutation,
        ],
      },
    );
    await waitFor((): void => {
      expect(
        screen.getByText(/https:\/\/github\.com\/fluidattacks\/integrates/iu),
      ).toBeInTheDocument();
    });

    const syncButton = document.querySelectorAll("#gitRootSync");

    expect(syncButton[0]).toBeDisabled();
    expect(syncButton[1]).not.toBeDisabled();
    expect(syncButton[2]).not.toBeDisabled();

    const cell = screen.getByText("https://gitlab.com/fluidattacks/machine");
    const row = cell.closest("tr");
    const button = within(row as HTMLElement).getAllByRole("button");

    await userEvent.click(button[1]);

    await waitFor((): void => {
      expect(msgError).toHaveBeenCalledWith("Git root is already cloning");
    });
    jest.clearAllMocks();
  });

  it("should render state button on cell", async (): Promise<void> => {
    expect.hasAssertions();

    const mockedPermissions = new PureAbility<string>([
      { action: "integrates_api_mutations_sync_git_root_mutate" },
      { action: "integrates_api_mutations_activate_root_mutate" },
    ]);

    render(
      <Wrapper mockedPermissions={mockedPermissions}>
        <GitRoots groupName={"unittesting"} organizationName={"ort-test"} />
      </Wrapper>,
      {
        memoryRouter: {
          initialEntries: ["/orgs/okada/groups/TEST/roots"],
        },
        mocks: [finalGitRootsMockGlobal, finalGitRootsQueriesMockGlobal],
      },
    );
    await waitFor((): void => {
      expect(
        screen.getByText(/https:\/\/github\.com\/fluidattacks\/integrates/iu),
      ).toBeInTheDocument();
    });
    const rowName =
      "https://github.com/fluidattacks/integrates main Active root-status Toggle Switch Low Failed integrates_2 buttons.update group.scope.git.healthCheck.no";
    const row = screen.getByRole("row", {
      name: rowName,
    });

    expect(within(row).getByRole("checkbox")).toBeChecked();

    await userEvent.click(screen.getAllByRole("checkbox")[1]);

    jest.clearAllMocks();
  });

  it("should render steps to add git root", async (): Promise<void> => {
    expect.hasAssertions();

    render(
      <authzPermissionsContext.Provider
        value={
          new PureAbility([
            { action: "integrates_api_mutations_add_git_root_mutate" },
            { action: "integrates_api_mutations_update_git_root_mutate" },
            {
              action: "integrates_api_mutations_download_git_roots_file_mutate",
            },
          ])
        }
      >
        <GitRoots groupName={"unittesting"} organizationName={"okada"} />
      </authzPermissionsContext.Provider>,
      {
        memoryRouter: {
          initialEntries: ["/TEST"],
        },
        mocks: [
          finalGitRootsQueriesMockGlobal,
          finalGitRootsMockGlobal,
          ...mockedOauthData,
        ],
      },
    );
    await waitFor((): void => {
      expect(screen.queryAllByRole("button")).toHaveLength(5);
    });

    await userEvent.click(
      screen.getByRole("button", { name: "group.scope.common.add" }),
    );
    await waitFor((): void => {
      expect(
        screen.queryByText("components.repositoriesDropdown.manual.text"),
      ).toBeInTheDocument();
    });

    expect(screen.getByText("GitLab")).toBeInTheDocument();
    expect(
      screen.queryByText("group.scope.git.oauthModal.title1"),
    ).not.toBeInTheDocument();

    await userEvent.click(screen.getByText("GitLab"));

    await waitFor((): void => {
      expect(
        screen.queryByText("group.scope.git.oauthModal.title1"),
      ).toBeInTheDocument();
    });

    expect(
      screen.getByRole("button", {
        name: "components.oauthRootForm.credentialSection.continue",
      }),
    ).toBeDisabled();

    await userEvent.click(
      screen.getByText(
        "components.oauthRootForm.credentialSection.placeholder",
      ),
    );
    await userEvent.click(
      screen.getByRole("listitem", {
        name: "6e52c11c-abf7-4ca3-b7d0-635e394f41c2",
      }),
    );

    await waitFor((): void => {
      expect(
        screen.getByRole("button", {
          name: "components.oauthRootForm.credentialSection.continue",
        }),
      ).not.toBeDisabled();
    });

    await userEvent.click(
      screen.getByRole("button", {
        name: "components.oauthRootForm.credentialSection.continue",
      }),
    );

    await waitFor(
      (): void => {
        expect(screen.getAllByRole("checkbox", { hidden: false })).toHaveLength(
          11,
        );
      },
      { timeout: 10000 },
    );

    await waitFor((): void => {
      expect(screen.getByRole("button", { name: "Next step" })).toBeDisabled();
    });

    await userEvent.click(screen.getAllByRole("checkbox", { name: "urls" })[2]);

    expect(
      screen.getByRole("button", { name: "Next step" }),
    ).not.toBeDisabled();

    await userEvent.click(screen.getByRole("button", { name: "Next step" }));
    const currentOption = screen.getByTestId(
      `branches.0-select-selected-option`,
    );

    // Open dropdown
    await userEvent.click(currentOption);
    const optionsContainer = screen.getByTestId(
      `branches.0-select-dropdown-options`,
    );
    await userEvent.click(getByValue(optionsContainer, "main"));

    expect(
      screen.getByRole("button", { name: "Next step" }),
    ).not.toBeDisabled();

    await userEvent.click(screen.getByRole("button", { name: "Next step" }));

    await waitFor((): void => {
      expect(
        screen.getByRole("button", {
          name: "components.oauthRootForm.onSubmit1",
        }),
      ).toBeDisabled();
    });

    await userEvent.click(
      screen.getByRole("radio", {
        name: "components.oauthRootForm.steps.s3.exclusions.radio.no.label",
      }),
    );

    expect(
      screen.getByRole("button", {
        name: "components.oauthRootForm.onSubmit1",
      }),
    ).not.toBeDisabled();

    await userEvent.click(
      screen.getByRole("button", {
        name: "components.oauthRootForm.onSubmit1",
      }),
    );
    await waitFor((): void => {
      expect(
        screen.queryByText("group.scope.git.oauthModal.title1"),
      ).not.toBeInTheDocument();
    });

    jest.clearAllMocks();
  });

  it("should remove environment url", async (): Promise<void> => {
    expect.hasAssertions();

    const handleClose: jest.Mock = jest.fn();
    const handleSubmit: jest.Mock = jest.fn();
    const initialValues: IFormValues = {
      ...formInitialValues,
      gitEnvironmentUrls: [
        {
          id: "adc83b19e793491b1c6ea0fd8b46cd9f32e592fc",
          rootId: "",
          secrets: [],
          url: "",
          urlType: "URL",
        },
      ],
      id: "c7118ab2-9345-4398-895f-11ce6e3cc1a6",
      includesHealthCheck: false,
      url: "https://gitlab.com/fluidattacks/universe",
    };

    const mockedMutation = graphqlMocked.mutation(
      REMOVE_ENVIRONMENT_URL,
      ({
        variables,
      }): StrictResponse<
        IErrorMessage | { data: RemoveEnvironmentUrlMutation }
      > => {
        const { groupName, rootId, urlId } = variables;
        if (
          groupName === "test-group" &&
          rootId === "c7118ab2-9345-4398-895f-11ce6e3cc1a6" &&
          urlId === "adc83b19e793491b1c6ea0fd8b46cd9f32e592fc"
        ) {
          return HttpResponse.json({
            data: {
              removeEnvironmentUrl: { success: true },
            },
          });
        }

        return HttpResponse.json({
          errors: [
            new GraphQLError(
              "An error occurred removing root environment urls",
            ),
          ],
        });
      },
    );
    const mockedRoot1 = graphqlMocked.query(
      GET_ROOT_ENVIRONMENT_URLS_SECRETS,
      ({
        variables,
      }): StrictResponse<
        IErrorMessage | { data: GetRootEnvironmentUrlsSecrets }
      > => {
        const { groupName, rootId } = variables;
        if (
          groupName === "test-group" &&
          rootId === "c7118ab2-9345-4398-895f-11ce6e3cc1a6"
        ) {
          return HttpResponse.json({
            data: {
              root: {
                __typename: "GitRoot",
                gitEnvironmentUrls: [
                  {
                    __typename: "GitEnvironmentUrl",
                    id: "",
                    rootId: "",
                    secrets: [],
                    url: "",
                    urlType: "URL",
                  },
                ],
              },
            },
          });
        }

        return HttpResponse.json({
          errors: [new GraphQLError("Error getting root environment urls")],
        });
      },
    );

    render(
      <authzPermissionsContext.Provider
        value={
          new PureAbility([
            { action: "integrates_api_mutations_add_secret_mutate" },
            { action: "integrates_api_mutations_update_git_root_mutate" },
            {
              action:
                "integrates_api_mutations_update_git_root_environment_url_mutate",
            },
            {
              action: "integrates_api_mutations_remove_environment_url_mutate",
            },
          ])
        }
      >
        <ManagementModal
          externalId={"id"}
          groupName={"test-group"}
          initialValues={initialValues}
          isEditing={true}
          manyRows={false}
          modalMessages={{ message: "", type: "success" }}
          onClose={handleClose}
          onSubmitRepo={handleSubmit}
          organizationName={""}
        />
      </authzPermissionsContext.Provider>,
      {
        memoryRouter: {
          initialEntries: ["/environments"],
        },
        mocks: [mockedRoot1, mockedEnvironments, mockedMutation],
      },
    );

    await waitFor((): void => {
      expect(
        screen.getByRole("link", { name: /group\.scope\.git\.envurls/iu }),
      ).toBeInTheDocument();
    });
    await userEvent.click(
      screen.getByRole("link", { name: /group\.scope\.git\.envurls/iu }),
    );

    await waitFor((): void => {
      expect(
        screen.getByText(/https:\/\/app\.fluidattacks\.com\/test/iu),
      ).toBeInTheDocument();
    });
    await userEvent.click(
      screen.getByRole("button", { name: "git-root-remove-environment-url" }),
    );
    await waitFor((): void => {
      expect(
        screen.queryByText("group.scope.git.removeEnvironment.title"),
      ).toBeInTheDocument();
    });
    await userEvent.click(screen.getByRole("button", { name: "Confirm" }));
    await waitFor((): void => {
      expect(msgSuccess).toHaveBeenCalledWith(
        "group.scope.git.removeEnvironment.success",
        "group.scope.git.removeEnvironment.successTitle",
      );
    });

    jest.clearAllMocks();
  });
});
