import { useAbility } from "@casl/react";
import { Text } from "@fluidattacks/design";
import type { CellContext, ColumnDef, Row } from "@tanstack/react-table";
import { Fragment, useCallback, useEffect, useState } from "react";
import * as React from "react";
import { useTranslation } from "react-i18next";
import { useTheme } from "styled-components";

import { renderEnvDescription } from "./description";
import { formatEnvironmentStatusColumn } from "./formatters/format-environment-status-column";
import { formatEnvironmentTypeColumn } from "./formatters/format-environment-type-column";
import type { ICombinedEnvUrlsData, IEnvironmentsProps } from "./types";

import { LinkSpan } from "../../../../organization/billing/groups/styles";
import { Secrets } from "../../secrets";
import { Table } from "components/table";
import { Can } from "context/authz/can";
import { authzPermissionsContext } from "context/authz/config";
import { useAudit } from "hooks/use-audit";

export const Environments = ({
  envUrlsDataSet,
  eventsDataSet,
  groupName,
  tableRef,
}: IEnvironmentsProps): JSX.Element => {
  const combinedData = envUrlsDataSet.map((envData): ICombinedEnvUrlsData => {
    const matchedEvent = eventsDataSet.find(
      (event): boolean =>
        event.environment === envData.url && event.eventStatus === "Unsolved",
    );

    return {
      ...envData,
      eventEnvironment: matchedEvent?.environment ?? undefined,
      eventId: matchedEvent?.id ?? undefined,
      eventStatus: matchedEvent?.eventStatus ?? undefined,
      groupName,
    };
  });

  const permissions = useAbility(authzPermissionsContext);
  const canGetSecrets = permissions.can(
    "integrates_api_resolvers_git_environment_url_secrets_resolve",
  );
  const { t } = useTranslation();
  const theme = useTheme();
  const docketImageType = t("group.scope.git.dockerImageType");
  const [isEnvironmentModalOpen, setIsEnvironmentModalOpen] = useState(false);
  const [currentRowUrl, setCurrentRowUrl] = useState<
    ICombinedEnvUrlsData | undefined
  >(undefined);
  function handleRowUrlClick(
    rowInfo: Row<ICombinedEnvUrlsData>,
  ): (event: React.FormEvent) => void {
    return (event: React.FormEvent): void => {
      if (rowInfo.original.urlType !== String(docketImageType)) {
        setCurrentRowUrl(rowInfo.original);
        setIsEnvironmentModalOpen(true);
        event.preventDefault();
      }
    };
  }

  const closeEnvironmentModal = useCallback((): void => {
    setIsEnvironmentModalOpen(false);
  }, []);

  function onClick(
    details: ICombinedEnvUrlsData,
  ): (event: React.FormEvent) => void {
    return (event: React.FormEvent): void => {
      setCurrentRowUrl(details);
      setIsEnvironmentModalOpen(true);
      event.preventDefault();
    };
  }
  const handleUrlRowExpand = useCallback(
    (row: Row<ICombinedEnvUrlsData>): JSX.Element => {
      return renderEnvDescription(row.original);
    },
    [],
  );

  const handleSecretsLink = (
    cell: CellContext<ICombinedEnvUrlsData, unknown>,
  ): React.JSX.Element => {
    return cell.row.original.urlType === String(docketImageType) ? (
      <Text color={theme.palette.gray[300]} size={"sm"}>
        {"None"}
      </Text>
    ) : (
      <LinkSpan
        $isNone={false}
        onClick={canGetSecrets ? onClick(cell.row.original) : undefined}
      >
        <Text color={theme.palette.gray[800]} size={"sm"}>
          {Number(cell.row.original.countSecrets) === 0
            ? "None"
            : `${cell.row.original.countSecrets} secret(s)`}
        </Text>
      </LinkSpan>
    );
  };

  const getConnectionType = (row: ICombinedEnvUrlsData): string => {
    if (row.useEgress) return "Egress";
    if (row.useVpn) return "Legacy";
    if (row.useZtna) return "Connector";

    return "N/A";
  };

  const columns: ColumnDef<ICombinedEnvUrlsData>[] = [
    {
      accessorKey: "url",
      cell: (cell): string => cell.row.original.url,
      header: String(t("group.scope.git.repo.url.header")),
      id: "url",
    },
    {
      accessorKey: "urlType",
      cell: formatEnvironmentTypeColumn,
      header: String(t("group.scope.git.repo.type.header")),
      id: "urlType",
    },
    {
      accessorFn: (row): string => {
        if (row.eventId !== undefined) {
          return t("group.scope.git.repo.status.value.events");
        }

        const isExcluded = row.repositoryUrls.every(
          (envRootData): boolean => !envRootData.include,
        );

        return isExcluded
          ? t("group.scope.git.repo.status.value.excluded")
          : t("group.scope.git.repo.status.value.included");
      },
      accessorKey: "eventStatus",
      cell: formatEnvironmentStatusColumn,
      header: String(t("group.scope.git.repo.status.header")),
      id: "eventStatus",
    },
    {
      accessorKey: "countSecrets",
      cell: handleSecretsLink,
      header: String(t("group.scope.git.repo.hasSecrets.header")),
      id: "hasSecrets",
    },
    {
      accessorFn: getConnectionType,
      header: String(t("group.scope.git.repo.connectionType")),
      id: "connectionType",
    },
  ];

  const { addAuditEvent } = useAudit();

  useEffect((): void => {
    addAuditEvent("Group.Environments", groupName);
  }, [addAuditEvent, groupName]);

  return (
    <Fragment>
      <Text
        color={theme.palette.gray[800]}
        fontWeight={"bold"}
        mb={1.25}
        mt={1.25}
        size={"xl"}
      >
        {t("group.scope.git.envUrls")}
      </Text>
      <Table
        columns={columns.filter((col): boolean =>
          canGetSecrets ? true : col.id !== "hasSecrets",
        )}
        data={combinedData}
        expandedRow={handleUrlRowExpand}
        onRowClick={canGetSecrets ? handleRowUrlClick : undefined}
        tableRef={tableRef}
      />
      <Can do={"integrates_api_resolvers_query_environment_url_resolve"}>
        <div>
          {currentRowUrl === undefined ? undefined : (
            <Secrets
              environmentSecret={true}
              groupName={groupName}
              isOpen={isEnvironmentModalOpen}
              onCloseModal={closeEnvironmentModal}
              resourceId={String(currentRowUrl.id)}
            />
          )}
        </div>
      </Can>
    </Fragment>
  );
};
