import { mixed, object } from "yup";

import { translate } from "utils/translations/translate";

const MAX_FILE_SIZE = 10;

const addGitRootFileModalSchema = object().shape({
  file: mixed()
    .required(translate.t("validations.required"))
    .isValidFileName()
    .isValidFileSize(undefined, MAX_FILE_SIZE),
});

export { addGitRootFileModalSchema };
