/* eslint-disable functional/immutable-data */
const EXAMPLE_CSV = [
  "url,branch,credential_id,connection,priority,includes_health_check,nickname,git_ignore",
  "https://github.com/user/repo,main,0a4542f6-9808-4db2-8982-8aeb5dab3c63,,LOW,true,custom_repo_1,",
  "https://github.com/user/repo,main,0a4542f6-9808-4db2-8982-8aeb5dab3c63,ztna,HIGH,true,custom_repo_2,",
];

const GIT_ROOT_FILE_LINK =
  "https://help.fluidattacks.com/portal/en/kb/articles/manage-roots#Prepare_the_CSV_file";

const handleDownloadSample = (): void => {
  const element = document.createElement("a");
  const file = new Blob([EXAMPLE_CSV.join("\n")], { type: "text/csv" });
  const downloadLink = window.URL.createObjectURL(file);
  element.href = URL.createObjectURL(file);
  element.download = "example.csv";
  element.href = downloadLink;

  document.body.appendChild(element);
  element.click();
  document.body.removeChild(element);
};

export { EXAMPLE_CSV, GIT_ROOT_FILE_LINK, handleDownloadSample };
