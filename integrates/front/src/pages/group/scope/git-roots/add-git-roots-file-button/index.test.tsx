import { PureAbility } from "@casl/ability";
import { screen, waitFor } from "@testing-library/react";
import { userEvent } from "@testing-library/user-event";
import { HttpResponse, type StrictResponse, graphql } from "msw";

import { EXAMPLE_CSV } from "./utils";

import { GitRootAddFileButton } from ".";
import { GitRoots } from "..";
import {
  GET_GIT_ROOTS,
  GET_GIT_ROOTS_QUERIES,
  UPLOAD_GIT_ROOT_FILE,
} from "../../queries";
import { authzPermissionsContext } from "context/authz/config";
import type {
  GetGitRootsQuery,
  GetGitRootsViewQueriesQuery,
  UploadGitRootFileMutation,
} from "gql/graphql";
import { render } from "mocks";
import { LINK } from "mocks/constants";

describe("add git root action", (): void => {
  const graphqlMocked = graphql.link(LINK);

  const mocks = graphqlMocked.mutation(
    UPLOAD_GIT_ROOT_FILE,
    (): StrictResponse<{ data: UploadGitRootFileMutation }> => {
      return HttpResponse.json({
        data: {
          uploadGitRootFile: {
            errorLines: [],
            message: "Roots created: 1",
            success: true,
            totalErrors: 0,
            totalSuccess: 1,
          },
        },
      });
    },
  );
  const queryMock = graphql
    .link(LINK)
    .query(GET_GIT_ROOTS, (): StrictResponse<{ data: GetGitRootsQuery }> => {
      return HttpResponse.json({
        data: {
          __typename: "Query",
          ...{
            group: {
              __typename: "Group",
              gitRoots: {
                __typename: "GitRootsConnection",
                edges: [],
                pageInfo: {
                  endCursor: "",
                  hasNextPage: false,
                },
                total: 0,
              },
              name: "test",
            },
          },
        },
      });
    });
  const secondQuery = graphqlMocked.query(
    GET_GIT_ROOTS_QUERIES,
    (): StrictResponse<{ data: GetGitRootsViewQueriesQuery }> => {
      return HttpResponse.json({
        data: {
          __typename: "Query",
          ...{
            group: {
              __typename: "Group" as const,
              gitEnvironmentUrls: [],
              name: "unittesting",
            },
            organizationId: {
              __typename: "Organization" as const,
              awsExternalId: "d89de098-0b63-4a5d-a9b3-df30438bfa86",
              name: "ort-test",
            },
          },
        },
      });
    },
  );

  it("should show the modal when button is clicked", async (): Promise<void> => {
    expect.hasAssertions();

    render(<GitRootAddFileButton />, { mocks: [mocks] });

    expect(screen.getByRole("button")).toBeVisible();

    await userEvent.click(screen.getByRole("button"));

    expect(
      screen.getByText("group.scope.git.addGitRootFile.modal.title"),
    ).toBeVisible();
    expect(
      screen.getByRole("link", { name: /See more information/u }),
    ).toBeVisible();
    expect(
      screen.getByRole("link", { name: /example CSV import file/u }),
    ).toBeVisible();
    expect(screen.getByTestId("file")).toBeInTheDocument();
    expect(
      screen.getByRole("button", { name: /addGitRootFile.modal.button/u }),
    ).not.toBeEnabled();
  });

  it("should submit a file", async (): Promise<void> => {
    expect.hasAssertions();

    const user = userEvent.setup();
    const text = async (): Promise<string> =>
      Promise.resolve(EXAMPLE_CSV.join("\n"));
    jest.spyOn(global, "File").mockImplementation(
      (_, fileName): File => ({
        ...global.File.prototype,
        name: fileName,
        text,
        type: "text/csv",
      }),
    );
    const file = new File(
      [new Blob([EXAMPLE_CSV.join("\n")])],
      "okata-test.csv",
      { type: "text/csv" },
    );

    render(
      <authzPermissionsContext.Provider
        value={
          new PureAbility([
            { action: "integrates_api_mutations_add_git_root_mutate" },
            { action: "integrates_api_mutations_upload_git_root_file_mutate" },
          ])
        }
      >
        <GitRoots groupName={"unittesting"} organizationName={"ort-test"} />
      </authzPermissionsContext.Provider>,
      {
        memoryRouter: {
          initialEntries: ["/unittesting"],
        },
        mocks: [mocks, queryMock, secondQuery],
      },
    );

    expect(
      screen.queryByText("group.scope.git.addGitRootFile.button"),
    ).toBeInTheDocument();

    await user.click(
      screen.getByRole("button", {
        name: "group.scope.git.addGitRootFile.button",
      }),
    );
    await waitFor((): void => {
      expect(
        screen.queryByText("group.scope.git.addGitRootFile.modal.title"),
      ).toBeInTheDocument();
    });

    await user.upload(screen.getByTestId("file"), file);

    expect(screen.getByText("okata-test.csv")).toBeInTheDocument();
    expect(
      screen.getByRole("button", { name: /addGitRootFile.modal.button/u }),
    ).toBeEnabled();

    await user.click(
      screen.getByRole("button", {
        name: "group.scope.git.addGitRootFile.modal.button",
      }),
    );

    await waitFor((): void => {
      expect(screen.queryByText("Uploading...")).toBeInTheDocument();
    });
  });
});
