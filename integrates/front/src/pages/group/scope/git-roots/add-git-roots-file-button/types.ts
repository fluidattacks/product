interface IErrorInfoAttr {
  message: string;
  row: string;
}

interface ILineError {
  line: string;
  lineIndex: number;
  field: string;
  fieldIndex: number;
  message: string;
}

export type { ILineError, IErrorInfoAttr };
