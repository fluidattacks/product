import { Text } from "@fluidattacks/design";
import { Fragment, useMemo } from "react";

import { getColumns } from "./utils";

import type { ILineError } from "../types";
import { Table } from "components/table";
import { useTable } from "hooks";

interface IInvalidRootsTableProps {
  readonly data: ILineError[];
  readonly total: number;
}

const InvalidRootsTable = ({
  data,
  total,
}: IInvalidRootsTableProps): JSX.Element => {
  const tableRef = useTable("invalid-roots");
  const columns = useMemo(getColumns, []);

  return (
    <Fragment>
      <Text size={"sm"}>
        {`In ${total} lines processed, we have detected `}
        <b>{`${data.length} errors:`}</b>
      </Text>
      <Table
        columns={columns}
        data={data}
        options={{ enableSearchBar: false }}
        tableRef={tableRef}
      />
      <Text fontWeight={"bold"} size={"sm"}>
        {"Please correct these repositories and upload again"}
      </Text>
    </Fragment>
  );
};

export { InvalidRootsTable };
