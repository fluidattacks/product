import { useMutation } from "@apollo/client";
import {
  Container,
  Form,
  type IUseModal,
  InnerForm,
  InputFile,
  Link,
  Loading,
  Modal,
  Text,
} from "@fluidattacks/design";
import mixpanel from "mixpanel-browser";
import { Fragment, useCallback, useState } from "react";
import { useTranslation } from "react-i18next";
import { useParams } from "react-router-dom";

import type { IAddFileValues } from "./types";
import { addGitRootFileModalSchema } from "./validations";

import { GET_GIT_ROOTS, UPLOAD_GIT_ROOT_FILE } from "../../../queries";
import { InvalidRootsTable } from "../invalid-roots-table";
import type { ILineError } from "../types";
import { GIT_ROOT_FILE_LINK, handleDownloadSample } from "../utils";
import { sleep } from "utils/helpers";
import { Logger } from "utils/logger";
import { showNotification } from "utils/notifications";
import { sanitizeFileName } from "utils/validations";

interface IAddFilesModalProps {
  modalRef: IUseModal;
}

const AddGitRootFileModal = ({
  modalRef,
}: Readonly<IAddFilesModalProps>): JSX.Element => {
  const { groupName } = useParams() as { groupName: string };
  const { t } = useTranslation();
  const [invalidRoots, setInvalidRoots] = useState<ILineError[]>([]);
  const [totalRoots, setTotalRoots] = useState(0);

  const [uploadFile, { loading: submitting }] = useMutation(
    UPLOAD_GIT_ROOT_FILE,
    {
      onCompleted: (mtResult): void => {
        mixpanel.track("UploadGitRootFile");
        const { message, success, errorLines, totalErrors, totalSuccess } =
          mtResult.uploadGitRootFile;

        setInvalidRoots(
          errorLines.map(
            (line): ILineError => ({
              field: line.split("|")[2],
              fieldIndex: Number(line.split("|")[1]),
              line: line.split("|")[3],
              lineIndex: Number(line.split("|")[0]),
              message: line.split("|")[4],
            }),
          ),
        );

        if (errorLines.length > 0) {
          setTotalRoots(totalErrors + totalSuccess);
          const newMessage =
            totalSuccess === 0
              ? "No roots were added, please check format errors"
              : message;
          showNotification("warning", newMessage, "Some roots updated");
        } else if (success) {
          showNotification("success", message, t("groupAlerts.updatedTitle"));
          modalRef.close();
        } else {
          showNotification("error", message, "Oops!");
        }
      },
      onError: ({ graphQLErrors }): void => {
        showNotification("error", t("groupAlerts.errorTextsad"), "Oops!");
        graphQLErrors.forEach((error): void => {
          Logger.error("Error uploading git root file", error);
        });
      },
      onQueryUpdated: async (observableQuery): Promise<void> => {
        const SLEEP_DURATION_MS = 1000;
        await sleep(SLEEP_DURATION_MS);
        await observableQuery.refetch();
      },
      refetchQueries: [GET_GIT_ROOTS],
    },
  );

  const handleUploadFileClick = useCallback(
    async (values: File): Promise<void> => {
      const sanitizedFileName = sanitizeFileName(values.name);
      const newFile = new File([values], sanitizedFileName, {
        type: values.type,
      });
      await uploadFile({
        variables: {
          file: newFile,
          groupName,
        },
      });
    },
    [groupName, uploadFile],
  );

  const onClose = useCallback((): void => {
    modalRef.close();
  }, [modalRef]);

  const onSubmit = useCallback(
    async (values: IAddFileValues): Promise<void> => {
      await handleUploadFileClick(values.file[0]);
    },
    [handleUploadFileClick],
  );

  return (
    <Modal
      modalRef={modalRef}
      size={"md"}
      title={t("group.scope.git.addGitRootFile.modal.title")}
    >
      <Form
        cancelButton={{ onClick: onClose }}
        confirmButton={{
          disabled: submitting,
          label: t("group.scope.git.addGitRootFile.modal.button"),
        }}
        defaultValues={{ file: undefined as unknown as FileList }}
        onSubmit={onSubmit}
        yupSchema={addGitRootFileModalSchema}
      >
        <InnerForm>
          {({ formState, register, setValue, watch }): JSX.Element => {
            return (
              <Fragment>
                <Text mb={1.25} size={"sm"}>
                  {
                    "Are you unsure about how the CSV file should be organized? "
                  }
                  <Link href={GIT_ROOT_FILE_LINK} iconPosition={"hidden"}>
                    <b>{"See more information"}</b>
                  </Link>
                  {" or obtain an "}
                  <Link href={""} onClick={handleDownloadSample}>
                    <b>{"example CSV import file."}</b>
                  </Link>
                </Text>
                <InputFile
                  accept={".csv"}
                  error={formState.errors.file?.message?.toString()}
                  id={"file"}
                  name={"file"}
                  register={register}
                  required={true}
                  setValue={setValue}
                  watch={watch}
                />
                {submitting ? (
                  <Container
                    alignItems={"center"}
                    display={"flex"}
                    height={"100%"}
                    justify={"center"}
                  >
                    <Loading color={"red"} label={"Uploading..."} size={"lg"} />
                  </Container>
                ) : undefined}
                {invalidRoots.length > 0 && (
                  <InvalidRootsTable data={invalidRoots} total={totalRoots} />
                )}
              </Fragment>
            );
          }}
        </InnerForm>
      </Form>
    </Modal>
  );
};

export { AddGitRootFileModal };
