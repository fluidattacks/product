import type { ColumnDef } from "@tanstack/react-table";

import { LineCell } from "./line-cell";

import type { ILineError } from "../types";

const getColumns = (): ColumnDef<ILineError>[] => [
  { accessorKey: "lineIndex", header: "Line" },
  { accessorKey: "fieldIndex", header: "Column" },
  { accessorKey: "message", header: "Error" },
  { accessorKey: "line", cell: LineCell, header: "Row" },
];

export { getColumns };
