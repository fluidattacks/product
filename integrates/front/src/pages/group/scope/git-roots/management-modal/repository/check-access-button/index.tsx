import { Buffer } from "buffer";

import { useMutation } from "@apollo/client";
import { Button, Col } from "@fluidattacks/design";
import { useFormikContext } from "formik";
import { useCallback } from "react";
import { useTranslation } from "react-i18next";

import type { ICheckAccessProps } from "./types";

import { VALIDATE_GIT_ACCESS } from "../../../../queries";
import type { IFormValues } from "../../../../types";
import { submittableCredentials } from "../utils";
import type { CredentialType } from "gql/graphql";
import { Logger } from "utils/logger";
import { validateSSHFormat } from "utils/validations";

const CheckAccessButton = ({
  credExists,
  setIsGitAccessible,
  setShowGitAlert,
  setValidateGitMsg,
}: ICheckAccessProps): JSX.Element | null => {
  const { t } = useTranslation();
  const { validateField, values } = useFormikContext<IFormValues>();

  const [validateGitAccess] = useMutation(VALIDATE_GIT_ACCESS, {
    onCompleted: (): void => {
      setShowGitAlert(false);
      setIsGitAccessible(true);
      setValidateGitMsg({
        message: t("group.scope.git.repo.credentials.checkAccess.success"),
        type: "success",
      });
      void validateField("credentials.token");
    },
    onError: ({ graphQLErrors }): void => {
      setShowGitAlert(false);
      graphQLErrors.forEach((error): void => {
        switch (error.message) {
          case "Exception - Git repository was not accessible with given credentials":
            setValidateGitMsg({
              message: t("group.scope.git.errors.invalidGitCredentials"),
              type: "error",
            });
            break;
          case "Exception - Branch not found":
            setValidateGitMsg({
              message: t("group.scope.git.errors.invalidBranch"),
              type: "error",
            });
            break;
          case "Exception - Protocol http is invalid":
            setValidateGitMsg({
              message: t("validations.invalidHttpProtocol"),
              type: "error",
            });
            break;
          default:
            setValidateGitMsg({
              message: t("groupAlerts.errorTextsad"),
              type: "error",
            });
            Logger.error("Couldn't activate root", error);
        }
      });
      setIsGitAccessible(false);
    },
  });

  const handleCheckAccessClick = useCallback((): void => {
    const formattedUrl = validateSSHFormat(values.url.trim())
      ? `ssh://${values.url.trim()}`
      : values.url.trim();

    void validateGitAccess({
      variables: {
        branch: values.branch,
        credentials: {
          arn: values.credentials.arn,
          isPat: values.credentials.isPat,
          key: values.credentials.key
            ? Buffer.from(values.credentials.key).toString("base64")
            : undefined,
          name: values.credentials.name,
          password: values.credentials.password,
          token: values.credentials.token,
          type: values.credentials.type as CredentialType,
          user: values.credentials.user,
        },
        url: formattedUrl,
      },
    });
  }, [validateGitAccess, values]);

  if (!submittableCredentials(credExists, values)) {
    return (
      <Col lg={100} md={100} sm={100}>
        <Button
          id={"checkAccessBtn"}
          onClick={handleCheckAccessClick}
          variant={"secondary"}
        >
          {t("group.scope.git.repo.credentials.checkAccess.text")}
        </Button>
      </Col>
    );
  }

  return null;
};

export { CheckAccessButton };
