import { useLazyQuery, useMutation } from "@apollo/client";
import { useAbility } from "@casl/react";
import {
  Alert,
  Checkbox,
  ComboBox,
  Container,
  Form,
  InnerForm,
  Link,
  Modal,
  OutlineContainer,
  RadioButton,
  Row,
  Text,
  useConfirmDialog,
} from "@fluidattacks/design";
import type { IUseModal } from "@fluidattacks/design";
import isEqual from "lodash/isEqual";
import mixpanel from "mixpanel-browser";
import { Fragment, useCallback } from "react";
import { useTranslation } from "react-i18next";

import { EnvironmentUrl } from "./environment-url";
import type {
  IAddEnvironmentProps,
  IEnvironmentNoSuccess,
  IEnvironmentSubmit,
  IVerifyEnvironmentIntegrity,
} from "./types";
import { MOBILE_EXTENSIONS } from "./utils";
import { validationSchema } from "./validations";

import {
  ADD_ENVIRONMENT_CSPM,
  ADD_ENVIRONMENT_URL,
  GET_ROOT_ENVIRONMENT_URLS,
  VERIFY_ENVIRONMENT_INTEGRITY,
} from "../../../../queries";
import { authzPermissionsContext } from "context/authz/config";
import type { IErrorInfoAttr } from "features/vulnerabilities/upload-file/types";
import type { GitEnvironmentCloud } from "gql/graphql";
import { Logger } from "utils/logger";
import { msgError, msgErrorStick, msgSuccess } from "utils/notifications";

const AddEnvironment = ({
  groupName,
  modalRef,
  rootAwsExternalId,
  rootId,
  onUpdate,
}: IAddEnvironmentProps & Readonly<{ modalRef: IUseModal }>): JSX.Element => {
  const { t } = useTranslation();
  const { confirm, ConfirmDialog } = useConfirmDialog();
  const { close } = modalRef;
  const permissions = useAbility(authzPermissionsContext);
  const canAddSecret = permissions.can(
    "integrates_api_mutations_add_secret_mutate",
  );
  const canVerifyIntegrity = permissions.can(
    "integrates_api_resolvers_query_verify_environment_integrity_resolve",
  );
  const documentation =
    "https://help.fluidattacks.com/portal/en/kb/articles/set-up-an-aws-integration";

  const formatError = (errorName: string, errorValue: string): string => {
    return ` ${t(errorName)} "${errorValue}" ${t("groupAlerts.invalid")}. `;
  };

  // Graphql operations
  const [addEnvironmentUrl] = useMutation(ADD_ENVIRONMENT_URL, {
    onCompleted: (result): void => {
      if (result.addGitEnvironmentUrl.success) {
        mixpanel.track("AddEnvironmentUrl");
        msgSuccess(
          t("group.scope.git.addEnvironment.success"),
          t("group.scope.git.addEnvironment.successTittle"),
        );
        onUpdate?.();
      } else {
        msgError(t("validations.environmentUrlExists"));
      }
    },
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error): void => {
        if (error.message.includes("Exception - Error in port value")) {
          const errorObject: IErrorInfoAttr = JSON.parse(error.message);
          msgErrorStick(`${t("groupAlerts.portValue")}
          ${formatError("groupAlerts.value", errorObject.values)}`);
        } else if (error.message === "Exception - The URL is not valid") {
          msgError(t("group.toe.inputs.addModal.alerts.invalidUrl"));
        } else if (
          error.message === "Exception - Cannot assume cross account IAM role"
        ) {
          msgError(t("validations.cannotAssumeAWSRole"));
        } else if (error.message === "Exception - Invalid characters") {
          msgError(t("group.scope.git.addEnvironment.invalidCharacter"));
        } else if (error.message === "Exception - Invalid file name") {
          msgError(
            t("validations.invalidFileNameEnvironment", {
              extensions: MOBILE_EXTENSIONS.join(", "),
            }),
          );
        } else if (error.message.includes("Exception - Protocol")) {
          msgError(t(`group.scope.hooks.errorMessage.invalidProtocol`));
        } else if (
          error.message ===
          "Exception - Root environment with the same url already exists"
        ) {
          msgError(t("group.scope.git.addEnvironment.repeatedEnvironment"));
        } else {
          msgError(t("groupAlerts.errorTextsad"));
          Logger.error("Couldn't add environment url", error);
        }
      });
    },
    refetchQueries: [
      { query: GET_ROOT_ENVIRONMENT_URLS, variables: { groupName, rootId } },
    ],
  });
  const [addEnvironmentCspm] = useMutation(ADD_ENVIRONMENT_CSPM, {
    onCompleted: (result): void => {
      if (result.addGitEnvironmentCspm.success) {
        mixpanel.track("AddEnvironmentCspm");
        msgSuccess(
          t("group.scope.git.addEnvironment.success"),
          t("group.scope.git.addEnvironment.successTittle"),
        );
        onUpdate?.();
      } else {
        msgError(t("validations.environmentUrlExists"));
      }
    },
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error): void => {
        if (
          error.message === "Exception - Cannot assume cross account IAM role"
        ) {
          msgError(t("validations.cannotAssumeAWSRole"));
        } else if (error.message === "Exception - Invalid characters") {
          msgError(t("group.scope.git.addEnvironment.invalidCharacter"));
        } else {
          msgError(t("groupAlerts.errorTextsad"));
          Logger.error("Couldn't add CSPM environment to the root", error);
        }
      });
    },
    refetchQueries: [
      { query: GET_ROOT_ENVIRONMENT_URLS, variables: { groupName, rootId } },
    ],
  });

  const [verifyEnvironmentIntegrity] =
    useLazyQuery<IVerifyEnvironmentIntegrity>(VERIFY_ENVIRONMENT_INTEGRITY, {
      onError: ({ graphQLErrors }): void => {
        graphQLErrors.forEach((error): void => {
          Logger.error("Credentials are broken", error);
        });
      },
    });
  const getUrl = useCallback((url: string): string => {
    try {
      return new URL(url).toString();
    } catch {
      return url;
    }
  }, []);

  // Data management
  const noSuccessStatus = useCallback(
    async ({
      url,
      urlType,
      useEgress,
      useVpn,
      useZtna,
      isProduction,
    }: IEnvironmentNoSuccess): Promise<void> => {
      const confirmResult = await confirm({
        message: (
          <Fragment>
            <Text size={"sm"}>
              {t("group.scope.git.addEnvironment.status")}
            </Text>
            <Alert>{t("groupAlerts.noSuccessStatus")}</Alert>
          </Fragment>
        ),
        title: t("group.scope.git.addEnvironment.checkingStatus"),
      });

      if (confirmResult) {
        await addEnvironmentUrl({
          variables: {
            groupName,
            isProduction: isProduction === "yes",
            rootId,
            url: getUrl(url),
            urlType: urlType as GitEnvironmentCloud,
            useEgress,
            useVpn,
            useZtna,
          },
        });
        close();
      }
    },
    [confirm, t, close, getUrl, groupName, rootId, addEnvironmentUrl],
  );

  const handleFormSubmit = useCallback(
    async ({
      cloudName,
      gcpPrivateKey,
      azureClientId,
      azureClientSecret,
      azureTenantId,
      url,
      urlType,
      useEgress,
      useVpn,
      useZtna,
      isProduction,
    }: IEnvironmentSubmit): Promise<void> => {
      const isProductionBoolean = isProduction === "yes";
      const { data: environmentIntegrity } = await verifyEnvironmentIntegrity({
        variables: {
          azureClientId,
          azureClientSecret,
          azureTenantId,
          cloudName,
          gcpPrivateKey,
          groupName,
          url,
          urlType,
        },
      });
      const environmentIntegrityStatus = environmentIntegrity
        ? environmentIntegrity.verifyEnvironmentIntegrity
        : false;
      if (urlType === "URL" && !useVpn && !useZtna) {
        if (environmentIntegrityStatus) {
          await addEnvironmentUrl({
            variables: {
              groupName,
              isProduction: isProductionBoolean,
              rootId,
              url: getUrl(url),
              urlType: urlType as GitEnvironmentCloud,
              useEgress,
              useVpn,
              useZtna,
            },
          });
          close();
        } else {
          await noSuccessStatus({
            isProduction,
            url,
            urlType,
            useEgress,
            useVpn,
            useZtna,
          });
        }
      } else if (urlType === "CSPM") {
        if (
          canVerifyIntegrity &&
          environmentIntegrityStatus &&
          cloudName !== undefined &&
          ["GCP", "AZURE", "AWS"].includes(cloudName)
        ) {
          const azureSubscriptionId = url;
          await addEnvironmentCspm({
            variables: {
              azureClientId,
              azureClientSecret,
              azureSubscriptionId,
              azureTenantId,
              cloudName,
              gcpPrivateKey,
              groupName,
              rootId,
              url,
              useEgress,
              useVpn,
              useZtna,
            },
          });
          close();
        } else {
          msgError(t("groupAlerts.noSuccessEnvironmentIntegrity"));
        }
      } else {
        await addEnvironmentUrl({
          variables: {
            groupName,
            isProduction: isProductionBoolean,
            rootId,
            url: urlType === "URL" ? getUrl(url) : url,
            urlType: urlType as GitEnvironmentCloud,
            useEgress,
            useVpn,
            useZtna,
          },
        });
        close();
      }
    },
    [
      noSuccessStatus,
      addEnvironmentCspm,
      addEnvironmentUrl,
      groupName,
      getUrl,
      rootId,
      close,
      verifyEnvironmentIntegrity,
      canVerifyIntegrity,
      t,
    ],
  );

  return (
    <Modal
      id={"addGitEnv"}
      modalRef={modalRef}
      size={"sm"}
      title={t("group.scope.git.addEnvUrl")}
    >
      <Form
        cancelButton={{ onClick: close }}
        confirmButton={{ disabled: !canAddSecret }}
        defaultValues={{
          azureClientId: "",
          azureClientSecret: "",
          azureTenantId: "",
          cloudName: undefined,
          gcpPrivateKey: "",
          groupName,
          isProduction: undefined,
          rootId,
          type: "",
          url: "",
          urlType: "",
          useEgress: false,
          useVpn: false,
          useZtna: false,
        }}
        id={"add-env-url-confirm"}
        onSubmit={handleFormSubmit}
        yupSchema={validationSchema}
      >
        <InnerForm<IEnvironmentSubmit>>
          {({
            control,
            formState,
            getValues,
            register,
            watch,
          }): JSX.Element => {
            const [
              cloudName,
              urlType,
              useVpn,
              useEgress,
              useZtna,
              isProduction,
            ] = watch([
              "cloudName",
              "urlType",
              "useVpn",
              "useEgress",
              "useZtna",
              "isProduction",
            ]);

            return (
              <Fragment>
                <ComboBox
                  control={control}
                  items={[
                    { name: "CSPM", value: "CSPM" },
                    { name: "Mobile App", value: "APK" },
                    { name: "URL", value: "URL" },
                  ]}
                  label={t("group.scope.git.addEnvironment.type")}
                  name={"urlType"}
                  required={true}
                />
                {urlType === "CSPM" ? (
                  <ComboBox
                    control={control}
                    items={[
                      { name: "AWS", value: "AWS" },
                      { name: "Google Cloud Platform", value: "GCP" },
                      { name: "Azure", value: "AZURE" },
                    ]}
                    label={t("group.scope.git.addEnvironment.cloud")}
                    name={"cloudName"}
                    required={true}
                  />
                ) : undefined}
                {urlType === "CSPM" && isEqual(cloudName, "AWS") ? (
                  <Alert variant={"info"}>
                    <Text size={"sm"}>
                      {t("group.scope.git.addEnvironment.awsExternalId", {
                        externalId: rootAwsExternalId,
                      })}
                    </Text>
                    <Text display={"inline"} size={"sm"}>
                      {t("group.scope.git.addEnvironment.awsSetupDocs")}
                      &nbsp;
                      <Link href={documentation}>{t("app.link")}</Link>
                    </Text>
                  </Alert>
                ) : undefined}
                <EnvironmentUrl
                  control={control}
                  formState={formState}
                  getValues={getValues}
                  groupName={groupName}
                  register={register}
                />
                {urlType === "APK" || urlType === "CSPM" ? undefined : (
                  <Row>
                    &nbsp;
                    <Checkbox
                      {...register("useZtna")}
                      disabled={Boolean(useVpn) || Boolean(useEgress)}
                      label={t("group.scope.git.repo.useZtna.label")}
                      name={"useZtna"}
                      tooltip={t("group.scope.git.repo.useZtna.toolTip")}
                    />
                    <Checkbox
                      {...register("useEgress")}
                      disabled={Boolean(useZtna) || Boolean(useVpn)}
                      label={t("group.scope.git.repo.useEgress.label")}
                      name={"useEgress"}
                      tooltip={t("group.scope.git.repo.useEgress.toolTip")}
                    />
                  </Row>
                )}
                {urlType !== "CSPM" && (
                  <Container mt={1}>
                    <OutlineContainer
                      error={formState.errors.isProduction?.message?.toString()}
                      htmlFor={"isProduction"}
                      label={t("group.scope.git.addEnvironment.isProduction")}
                      weight={"bold"}
                    >
                      <Container
                        alignSelf={"start"}
                        display={"flex"}
                        gap={1}
                        width={"fit-content"}
                      >
                        <RadioButton
                          {...register("isProduction")}
                          defaultChecked={isProduction === "yes"}
                          label={t(
                            "group.scope.git.addEnvironment.yesIsProduction",
                          )}
                          name={"isProduction"}
                          value={"yes"}
                        />
                        <RadioButton
                          {...register("isProduction")}
                          defaultChecked={isProduction === "no"}
                          label={t(
                            "group.scope.git.addEnvironment.noIsProduction",
                          )}
                          name={"isProduction"}
                          value={"no"}
                        />
                      </Container>
                    </OutlineContainer>
                  </Container>
                )}
                <ConfirmDialog />
              </Fragment>
            );
          }}
        </InnerForm>
      </Form>
    </Modal>
  );
};

export { AddEnvironment };
