import { PureAbility } from "@casl/ability";
import { fireEvent, screen, waitFor } from "@testing-library/react";
import { Form, Formik } from "formik";

import { Exclusions } from ".";
import { authzPermissionsContext } from "context/authz/config";
import { render } from "mocks";

describe("exclusions", (): void => {
  it("should render correctly when manyRows is false", (): void => {
    expect.hasAssertions();

    render(
      <authzPermissionsContext.Provider
        value={new PureAbility([{ action: "update_git_root_filter" }])}
      >
        <Formik
          initialValues={{ gitignore: [], hasExclusions: "no" }}
          onSubmit={jest.fn()}
        >
          <Form name={"gitRoot"}>
            <Exclusions isEditing={true} manyRows={false} />
          </Form>
        </Formik>
      </authzPermissionsContext.Provider>,
    );

    expect(screen.getByLabelText("Yes")).toBeInTheDocument();
    expect(screen.getByLabelText("No")).toBeInTheDocument();
  });

  it("should return null when manyRows is true", (): void => {
    expect.hasAssertions();

    const { container } = render(
      <authzPermissionsContext.Provider
        value={new PureAbility([{ action: "update_git_root_filter" }])}
      >
        <Formik
          initialValues={{ gitignore: [], hasExclusions: "no" }}
          onSubmit={jest.fn()}
        >
          <Exclusions isEditing={true} manyRows={true} />
        </Formik>
      </authzPermissionsContext.Provider>,
    );

    expect(container.firstChild).toBeNull();
  });

  it('should show alert and input array when "Yes" is selected', async (): Promise<void> => {
    expect.hasAssertions();

    render(
      <authzPermissionsContext.Provider
        value={new PureAbility([{ action: "update_git_root_filter" }])}
      >
        <Formik
          initialValues={{ gitignore: [], hasExclusions: "no" }}
          onSubmit={jest.fn()}
        >
          <Form name={"gitRoot"}>
            <Exclusions isEditing={true} manyRows={false} />
          </Form>
        </Formik>
      </authzPermissionsContext.Provider>,
    );

    fireEvent.click(screen.getByLabelText("Yes"));

    await waitFor((): void => {
      expect(
        screen.getByText("group.scope.git.filter.warning"),
      ).toBeInTheDocument();
    });

    expect(
      screen.getByText("group.scope.git.filter.addExclusion"),
    ).toBeInTheDocument();
  });
});
