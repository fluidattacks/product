import type { ICredentials } from "pages/group/scope/types";

const formatAuthentication = (value: string): boolean => value === "yes";

const formatInitialAuthentication = (
  isEditing: boolean,
  credentials?: ICredentials,
): string => {
  if (isEditing) {
    return credentials ? "yes" : "no";
  }

  return "";
};

const baseCredentials: ICredentials = {
  arn: "",
  auth: "",
  azureOrganization: "",
  id: "",
  isPat: false,
  isToken: false,
  key: "",
  name: "",
  password: "",
  token: "",
  type: "",
  typeCredential: "",
  user: "",
};

export { formatAuthentication, baseCredentials, formatInitialAuthentication };
