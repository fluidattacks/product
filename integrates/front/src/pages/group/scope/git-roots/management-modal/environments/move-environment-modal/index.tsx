import { Form, Icon, InnerForm, Modal } from "@fluidattacks/design";
import type { TFormMethods } from "@fluidattacks/design";
import { Fragment, useCallback } from "react";
import { useTranslation } from "react-i18next";

import type { IModalProps, IValues } from "./form";
import { FormContent } from "./form";
import { validationSchema } from "./validations";

import { useMoveEnvironmentMutation } from "../../hooks/use-move-environment-mutation";
import { useConditionsDialog } from "components/conditions-dialog";

const MoveEnvironmentModal = (modalProps: IModalProps): JSX.Element | null => {
  const { groupName, rootId, environment, modalRef } = modalProps;
  const { t } = useTranslation();
  const [conditionsConfirm, ConditionsDialog] = useConditionsDialog(
    "move-root-conditions",
    t("group.scope.git.moveEnvironment.title"),
    {
      closeVulns: {
        label: t("group.scope.git.moveEnvironment.conditions.closeVulns"),
        state: false,
      },
      moveSecrets: {
        label: t("group.scope.git.moveEnvironment.conditions.moveSecrets"),
        state: false,
      },
    },
  );

  const [moveEnvironment] = useMoveEnvironmentMutation();

  const onSubmit = useCallback(
    async (values: IValues): Promise<void> => {
      const result = await conditionsConfirm();
      if (result) {
        await moveEnvironment({
          variables: {
            groupName: values.groupName,
            rootId: values.rootId,
            targetGroupName: values.targetGroupName,
            targetRootId: values.targetRootId,
            urlId: environment.id,
          },
        });
        modalRef.close();
      }
    },
    [modalRef, environment.id, moveEnvironment, conditionsConfirm],
  );

  if (!modalRef.isOpen) {
    return null;
  }

  return (
    <Fragment>
      <Modal
        id={"move-environment"}
        modalRef={modalRef}
        size={"sm"}
        title={t("group.scope.git.moveEnvironment.title")}
      >
        <Form
          cancelButton={{ onClick: modalRef.close }}
          defaultValues={{
            groupName,
            rootId,
            targetGroupName: "",
            targetRootId: "",
          }}
          id={"move-environment-confirm"}
          onSubmit={onSubmit}
          yupSchema={validationSchema}
        >
          <InnerForm>
            {(props: TFormMethods<IValues>): JSX.Element => (
              <FormContent {...props} {...modalProps} />
            )}
          </InnerForm>
        </Form>
      </Modal>
      <ConditionsDialog>
        <div style={{ margin: "0 auto 2.5rem" }}>
          <Icon icon={"code-pull-request"} iconSize={"lg"} mb={2} />
        </div>
      </ConditionsDialog>
    </Fragment>
  );
};

export { MoveEnvironmentModal };
