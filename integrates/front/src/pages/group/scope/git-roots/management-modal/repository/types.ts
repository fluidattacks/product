import type { IFormValues } from "pages/group/scope/types";

interface IRepositoryProps {
  initialValues?: IFormValues;
  isEditing: boolean;
  manyRows: boolean | undefined;
  modalMessages: { message: string; type: string };
  onClose: () => void;
  onSubmit: (values: IFormValues) => Promise<void>;
}

export type { IRepositoryProps };
