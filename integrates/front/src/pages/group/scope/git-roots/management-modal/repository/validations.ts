import _ from "lodash";
import type {
  AnyObject,
  InferType,
  Maybe,
  Schema,
  TestContext,
  ValidationError,
} from "yup";
import { array, lazy, object, string } from "yup";

import type { IFormValues } from "../../../types";
import { formatBooleanHealthCheck } from "../../../utils";
import { translate } from "utils/translations/translate";

const REGEX_TEST = {
  INVALID_SPACES: /^\S*$/u,
  SSH_FORMAT:
    /^-{5}BEGIN OPENSSH PRIVATE KEY-{5}\n(?:[a-zA-Z0-9+/=]+\n)+-{5}END OPENSSH PRIVATE KEY-{5}\n?$/u,
};

const checkAwsRole = (type: string, credExists: boolean): Schema => {
  if (type === (credExists ? "" : "AWSROLE")) {
    return string().required(translate.t("validations.required"));
  }

  return string();
};

const validCredentialType = (
  value: string | undefined,
  values: IFormValues,
): boolean => {
  if (
    _.isUndefined(value) ||
    _.isUndefined(values.url) ||
    !_.includes(["OAUTH"], value) ||
    _.isEmpty(values.url)
  ) {
    return true;
  }
  try {
    const { protocol } = new URL(values.url);

    return _.includes(["OAUTH"], value) && protocol.toLowerCase() === "https:";
  } catch {
    return true;
  }
};

const pathFormat = (value: string | undefined): boolean => {
  if (value === undefined) {
    return false;
  }

  return !value.includes("\\");
};

const validateHealthCheck = (
  gitRootsPermissions: {
    canAddExclusions: boolean;
    hasAdvanced: boolean;
  },
  isCheckedHealthCheck: boolean,
  values: IFormValues,
): boolean => {
  const { healthCheckConfirm, includesHealthCheck } = values;
  if (!gitRootsPermissions.hasAdvanced || isCheckedHealthCheck) {
    return true;
  }
  if (
    formatBooleanHealthCheck(includesHealthCheck) !== null &&
    formatBooleanHealthCheck(includesHealthCheck) === false &&
    healthCheckConfirm === undefined
  ) {
    return true;
  }
  if (healthCheckConfirm === undefined) {
    return false;
  }

  return (
    ((formatBooleanHealthCheck(includesHealthCheck) ?? false) &&
      healthCheckConfirm.includes("includeA")) ||
    (!(formatBooleanHealthCheck(includesHealthCheck) ?? true) &&
      healthCheckConfirm.includes("rejectA") &&
      healthCheckConfirm.includes("rejectB") &&
      healthCheckConfirm.includes("rejectC"))
  );
};

const validateAzureOrg = (
  credExists: boolean,
  [type, isPat]: unknown[],
  values: IFormValues,
): Schema => {
  const typeProtocol: string =
    values.credentials.typeCredential === "TOKEN" ? "HTTPS" : "";

  return !credExists && String(type) === typeProtocol && Boolean(isPat)
    ? string()
        .required(translate.t("validations.required"))
        .isValidValue(
          translate.t("validations.invalidSpaceInField"),
          REGEX_TEST.INVALID_SPACES,
        )
    : string();
};

const isRequired = (condition: boolean): Schema => {
  return condition
    ? string().required(translate.t("validations.required"))
    : string();
};

const getProtocol = (credential: boolean): string => {
  return credential ? "HTTPS" : "";
};

const validateUrlProtocol = (
  value: string | undefined,
  thisContext: TestContext<Maybe<AnyObject>>,
): ValidationError | boolean => {
  if (value === undefined || value.startsWith("codecommit")) {
    return true;
  }
  const contextParent = thisContext.parent as IFormValues;
  const urlProtocol = value.startsWith("http") ? "HTTP" : "SSH";

  return (urlProtocol === "SSH") === (contextParent.credentials.type === "SSH")
    ? true
    : thisContext.createError({
        message: translate.t("validations.urlMismatchProtocolCredential"),
      });
};

const validateSelectedCredential =
  (
    values: IFormValues,
  ): ((
    value: string | undefined,
    thisContext: TestContext<Maybe<AnyObject>>,
  ) => ValidationError | boolean) =>
  (
    value: string | undefined,
    thisContext: TestContext<Maybe<AnyObject>>,
  ): ValidationError | boolean => {
    if (value === undefined) {
      return true;
    }

    const url = values.url as string | undefined;

    if (
      url === undefined ||
      (url.startsWith("codecommit") && value === "AWSROLE")
    ) {
      return true;
    }

    const urlProtocol = url.startsWith("http") ? "HTTP" : "SSH";

    return (urlProtocol === "SSH") === (value === "SSH")
      ? true
      : thisContext.createError({
          message: translate.t("validations.protocolMismatchURLCredential"),
        });
  };

const validationSchema = (
  isEditing: boolean,
  credExists: boolean,
  gitRootsPermissions: {
    canAddExclusions: boolean;
    hasAdvanced: boolean;
  },
  isCheckedHealthCheck: boolean,
  isGitAccessible: boolean,
): InferType<Schema> =>
  lazy(
    (values: IFormValues): Schema =>
      object().shape({
        branch: string().required(translate.t("validations.required")),
        credentials: object({
          arn: string()
            .when("type", ([type]: string[]): Schema => {
              return checkAwsRole(type, credExists);
            })
            .isValidArnFormat(),
          azureOrganization: string().when(
            ["type", "isPat"],
            ([type, isPat]: unknown[]): Schema => {
              return validateAzureOrg(credExists, [type, isPat], values);
            },
          ),
          id: string(),
          key: string()
            .when("type", ([type]: string[]): Schema => {
              const keyType: string = credExists ? "" : "SSH";

              return isRequired(type === keyType);
            })
            .isValidBoolean(
              isGitAccessible,
              translate.t(
                "group.scope.git.repo.credentials.checkAccess.noAccess",
              ),
            )
            .isValidSSHFormat(REGEX_TEST.SSH_FORMAT, values),
          name: isEditing
            ? string().when("type", ([type]: unknown[]): Schema => {
                return type === undefined
                  ? string()
                  : string().required(translate.t("validations.required"));
              })
            : string()
                .required(translate.t("validations.required"))
                .isValidValue(),
          password: string()
            .when("type", ([type]: string[]): Schema => {
              const userCred =
                !credExists && values.credentials.typeCredential === "USER";
              const protocol: string = getProtocol(userCred);

              return isRequired(type === protocol);
            })
            .isValidBoolean(
              isGitAccessible,
              translate.t(
                "group.scope.git.repo.credentials.checkAccess.noAccess",
              ),
            )
            .isValidValue(),
          token: string()
            .when("type", ([type]: string[]): Schema => {
              const tokenCred =
                !credExists && values.credentials.typeCredential === "TOKEN";
              const tokenProtocol: string = getProtocol(tokenCred);

              return isRequired(type === tokenProtocol);
            })
            .isValidBoolean(
              isGitAccessible,
              translate.t(
                "group.scope.git.repo.credentials.checkAccess.noAccess",
              ),
            )
            .isValidValue(),
          type: string().when("$isEditing", (): Schema => {
            return isEditing
              ? string()
              : string().required(translate.t("validations.required"));
          }),
          typeCredential: string().when("$isEditing", (): Schema => {
            return isEditing
              ? string().isValidDynamicValidation(
                  validateSelectedCredential(values),
                )
              : string()
                  .required(translate.t("validations.required"))
                  .isValidFunction((value): boolean => {
                    return validCredentialType(value, values);
                  }, translate.t("validations.invalidCredentialType"))
                  .isValidDynamicValidation(validateSelectedCredential(values));
          }),
          user: string()
            .when("type", ([type]: string[]): Schema => {
              const userCred: boolean =
                !credExists && values.credentials.typeCredential === "USER";
              const protocol: string = getProtocol(userCred);

              return isRequired(type === protocol);
            })
            .isValidBoolean(
              isGitAccessible,
              translate.t(
                "group.scope.git.repo.credentials.checkAccess.noAccess",
              ),
            )
            .isValidValue(),
        }),
        gitignore: array().of(
          string()
            .required(translate.t("validations.required"))
            .isValidExcludeFormat(values.url)
            .isValidFunction(pathFormat, "Use '/' for directories"),
        ),
        hasExclusions: string().when("$canAddExclusions", (): Schema => {
          return gitRootsPermissions.canAddExclusions
            ? string().required(translate.t("validations.required"))
            : string();
        }),
        healthCheckConfirm: array().of(
          string().isValidFunction((): boolean => {
            return validateHealthCheck(
              gitRootsPermissions,
              isCheckedHealthCheck,
              values,
            );
          }, translate.t("validations.required")),
        ),
        includesHealthCheck: string().when("$hasAdvanced", (): Schema => {
          const { hasAdvanced } = gitRootsPermissions;

          return isRequired(hasAdvanced);
        }),
        url: string()
          .required(translate.t("validations.required"))
          .isValidTextBeginning()
          .isValidTextField()
          .isValidDynamicValidation(validateUrlProtocol)
          .isValidUrlProtocolOrSSHFormat(),
      }),
  );

export { validationSchema };
