import { useMutation, useQuery } from "@apollo/client";
import { useAbility } from "@casl/react";
import {
  Alert,
  Button,
  Col,
  Container,
  ProgressBar,
  Row,
  Span,
  useConfirmDialog,
} from "@fluidattacks/design";
import { Form } from "formik";
import { isUndefined } from "lodash";
import isEmpty from "lodash/isEmpty";
import { useCallback, useMemo, useState } from "react";
import * as React from "react";
import { Trans, useTranslation } from "react-i18next";

import type { IEditModalProps } from "./types";

import { FormModal } from "components/form-modal";
import { InputFile } from "components/input";
import { Can } from "context/authz/can";
import { authzPermissionsContext } from "context/authz/config";
import type { GroupFile } from "gql/graphql";
import { validationSchema } from "pages/group/scope/files/file-options-modal/modal/validations";
import type { IFile } from "pages/group/scope/files/types";
import { MOBILE_EXTENSIONS } from "pages/group/scope/git-roots/management-modal/environments/add-environment/utils";
import {
  GET_FILES,
  GET_GIT_ROOTS_QUERIES,
  GET_ROOT_ENVIRONMENT_URLS,
  SIGN_POST_URL_MUTATION,
  UPDATE_GIT_ROOT_ENVIRONMENT_FILE,
} from "pages/group/scope/queries";
import { Logger } from "utils/logger";
import { msgError, msgSuccess } from "utils/notifications";
import { uploadGroupFileToS3 } from "utils/upload-file-to-s3";
import { sanitizeFileName } from "utils/validations";

const MIN_SUCCESS_STATUS_CODE = 200;
const MAX_SUCCESS_STATUS_CODE = 300;
const EditModal: React.FC<IEditModalProps> = ({
  gitEnvironment,
  modalRef,
  groupName,
  organizationName,
  rootId,
}): JSX.Element => {
  const { t } = useTranslation();
  const permissions = useAbility(authzPermissionsContext);
  const [isButtonEnabled, setIsButtonEnabled] = useState(false);
  const [progressPercentage, setProgressPercentage] = useState(0);
  const [isUpdatingFileInEnvironment, setIsUpdatingFileInEnvironment] =
    useState(false);
  const { confirm, ConfirmDialog } = useConfirmDialog();

  const { data } = useQuery(GET_FILES, {
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error): void => {
        msgError(t("groupAlerts.errorTextsad"));
        Logger.error("An error occurred loading group files", error);
      });
    },
    variables: { groupName },
  });
  const [uploadFile] = useMutation(SIGN_POST_URL_MUTATION, {
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error): void => {
        if (error.message === "Exception - Invalid characters in filename") {
          msgError(t("searchFindings.tabResources.invalidChars"));
        } else {
          msgError(t("groupAlerts.errorTextsad"));
          Logger.error("An error occurred uploading group files", error);
        }
      });
    },
  });
  const [updateGitRootEnvironmentFile] = useMutation(
    UPDATE_GIT_ROOT_ENVIRONMENT_FILE,
    {
      onError: ({ graphQLErrors }): void => {
        graphQLErrors.forEach((error): void => {
          if (error.message === "Exception - Invalid file name") {
            msgError(
              t("validations.invalidFileNameEnvironment", {
                extensions: MOBILE_EXTENSIONS.join(", "),
              }),
            );
          } else if (
            error.message === "File name already exists in group files"
          ) {
            msgError(t("validations.invalidFileName"));
          } else {
            msgError(t("groupAlerts.errorTextsad"));
            Logger.error("An error occurred update environment file", error);
          }
        });
      },
    },
  );

  const resourcesFiles = useMemo((): GroupFile[] => {
    if (!data?.resources) {
      return [];
    }

    return data.resources.files ?? [];
  }, [data?.resources]);

  const disableButton = useCallback((): void => {
    setIsButtonEnabled(true);
  }, []);

  const enableButton = useCallback((): void => {
    setIsButtonEnabled(false);
  }, []);
  const onSubmit = useCallback(
    async (formikValues: { file?: FileList }): Promise<void> => {
      if (formikValues.file === undefined) return;

      const sanitizedFileName = sanitizeFileName(formikValues.file[0].name);
      const newFile = new File([formikValues.file[0]], sanitizedFileName, {
        type: formikValues.file[0].type,
      });

      const repeatedFiles = resourcesFiles.filter(
        (file): boolean => file.fileName === sanitizedFileName,
      );
      if (repeatedFiles.length > 0) {
        msgError(t("searchFindings.tabResources.repeatedItem"));

        return;
      }

      const confirmResult = await confirm({
        message: (
          <Alert>
            <Trans
              components={{
                bold: (
                  <Span fontWeight={"bold"} size={"sm"}>
                    {undefined}
                  </Span>
                ),
              }}
              i18nKey={"group.scope.git.updateEnvironment.warning"}
              t={t}
              values={{
                newFileName: sanitizedFileName,
                oldFileName: gitEnvironment.url,
              }}
            />
          </Alert>
        ),
        title: t("searchFindings.tabResources.files.form.button"),
      });
      if (!confirmResult) return;
      disableButton();

      const firstFile: IFile = {
        description: "",
        fileName: sanitizedFileName,
        uploadDate: new Date().toISOString(),
      };

      const results = await uploadFile({
        variables: {
          filesDataInput: [firstFile],
          groupName,
        },
      });

      if (!results.data) return;

      const onError = (): void => {
        enableButton();
        modalRef.close();
      };

      try {
        const status = await uploadGroupFileToS3(
          results,
          newFile,
          groupName,
          onError,
          setProgressPercentage,
        );

        if (
          status < MIN_SUCCESS_STATUS_CODE ||
          status >= MAX_SUCCESS_STATUS_CODE
        ) {
          msgError(t("groupAlerts.errorTextsad"));

          return;
        }
        setIsUpdatingFileInEnvironment(true);
        await updateGitRootEnvironmentFile({
          onCompleted: (resultData): void => {
            const { updateGitRootEnvironmentFile: mutationResults } =
              resultData;
            const { success } = mutationResults;

            if (!success) {
              msgError(t("groupAlerts.errorTextsad"));
              Logger.error("An error occurred adding group files to the db");
              setIsUpdatingFileInEnvironment(false);
              enableButton();
              modalRef.close();

              return;
            }
            msgSuccess(
              t("searchFindings.tabResources.files.form.success"),
              t("searchFindings.tabUsers.titleSuccess"),
            );
            modalRef.close();
          },
          refetchQueries: [
            {
              query: GET_GIT_ROOTS_QUERIES,
              variables: {
                canGetSecrets: permissions.can(
                  "integrates_api_resolvers_git_environment_url_secrets_resolve",
                ),
                groupName,
                organizationName,
              },
            },
            {
              query: GET_ROOT_ENVIRONMENT_URLS,
              variables: { groupName, rootId },
            },

            { query: GET_FILES, variables: { groupName } },
          ],
          variables: {
            fileName: firstFile.fileName,
            groupName,
            oldFileName: gitEnvironment.url,
            rootId,
            urlId: gitEnvironment.id,
          },
        });
      } catch (err) {
        Logger.error("Error uploading file to s3", err);
      }
      enableButton();
    },
    [
      confirm,
      gitEnvironment,
      groupName,
      disableButton,
      enableButton,
      modalRef,
      organizationName,
      permissions,
      resourcesFiles,
      rootId,
      updateGitRootEnvironmentFile,
      t,
      uploadFile,
    ],
  );

  return (
    <React.Fragment>
      <FormModal
        enableReinitialize={true}
        initialValues={{
          file: undefined as FileList | undefined,
        }}
        modalRef={modalRef}
        onSubmit={onSubmit}
        size={"sm"}
        title={t("searchFindings.tabResources.files.form.button")}
        validationSchema={validationSchema(gitEnvironment.url)}
      >
        {({ values }): JSX.Element => (
          <Form>
            <Container
              display={"flex"}
              flexDirection={"column"}
              gap={0.5}
              mb={0.5}
            >
              <Can
                do={
                  "integrates_api_mutations_update_git_root_environment_file_mutate"
                }
              >
                {gitEnvironment.urlType === "APK" ? (
                  <React.Fragment>
                    <Row>
                      <Col>
                        <InputFile
                          accept={MOBILE_EXTENSIONS.join(",")}
                          id={"file"}
                          name={"file"}
                        />
                      </Col>
                    </Row>
                    {isButtonEnabled ? (
                      <Container
                        display={"flex"}
                        flexDirection={"column"}
                        gap={1}
                      >
                        <Alert variant={"info"}>
                          {t("searchFindings.tabResources.uploadingProgress")}
                        </Alert>
                        <ProgressBar
                          minWidth={246}
                          percentage={progressPercentage}
                          showPercentage={true}
                        />
                        {isUpdatingFileInEnvironment ? (
                          <Alert variant={"info"}>
                            {t("groupAlerts.updatingFileInEnvironment")}
                          </Alert>
                        ) : undefined}
                      </Container>
                    ) : undefined}
                    {values.file === undefined ||
                    isEmpty(values.file) ? undefined : (
                      <Button
                        disabled={
                          isUndefined(values.file) || isEmpty(values.file)
                        }
                        justify={"center"}
                        maxWidth={"100px"}
                        type={"submit"}
                      >
                        {t("searchFindings.tabResources.files.form.button")}
                      </Button>
                    )}
                  </React.Fragment>
                ) : undefined}
              </Can>
            </Container>
          </Form>
        )}
      </FormModal>
      <ConfirmDialog />
    </React.Fragment>
  );
};

export { EditModal };
