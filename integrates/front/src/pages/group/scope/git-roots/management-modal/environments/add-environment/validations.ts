import isNull from "lodash/isNull";
import type {
  AnyObject,
  Maybe,
  Schema,
  StringSchema,
  TestContext,
  ValidationError,
} from "yup";
import { object, string } from "yup";

import type { IFormProps } from "./types";

import { translate } from "utils/translations/translate";

const REGEX_TEST = {
  FORBIDDEN_LINKS:
    /^(?!.*(?:https:\/\/play\.google\.com\/store\/apps|https:\/\/apps\.apple\.com\/co\/app\/)).*$/u,
  INVALID_URL:
    /^(?:https?:\/\/)?(?:[\w-]+(?:\.[\w-]+)*)?(?::\d+)?(?:\/[^\s]*)?$/u,
};

const validateCloudName = (
  value: string | undefined,
  thisContext: TestContext<Maybe<AnyObject>>,
): ValidationError | boolean => {
  if (value === undefined) {
    return true;
  }

  const contextParent = thisContext.parent as IFormProps;
  if (contextParent.cloudName === "AWS") {
    const awsRegex = /^arn:aws:iam::\d{12}:role\/[\w+=,.@-]+$/u;
    const awsTextMatch: RegExpMatchArray | null = awsRegex.exec(value);

    return isNull(awsTextMatch)
      ? thisContext.createError({
          message: translate.t("validations.matchRegex", { regex: awsRegex }),
        })
      : true;
  }
  if (contextParent.cloudName === "AZURE") {
    const azureRegex = /^.{36}$/u;
    const azureTextMatch: RegExpMatchArray | null = azureRegex.exec(value);

    return isNull(azureTextMatch)
      ? thisContext.createError({
          message: translate.t("validations.matchRegex", { regex: azureRegex }),
        })
      : true;
  }

  return true;
};

export const validationSchema = object().shape({
  azureClientId: string().when("cloudName", ([cloudName]: string[]): Schema => {
    return cloudName === "AZURE"
      ? string()
          .required(translate.t("validations.required"))
          .matches(/^.{36}$/u)
      : string();
  }),
  azureClientSecret: string().when(
    "cloudName",
    ([cloudName]: string[]): Schema => {
      return cloudName === "AZURE"
        ? string()
            .required(translate.t("validations.required"))
            .matches(/^.{40}$/u)
        : string();
    },
  ),
  azureTenantId: string().when("cloudName", ([cloudName]: string[]): Schema => {
    return cloudName === "AZURE"
      ? string()
          .required(translate.t("validations.required"))
          .matches(/^.{36}$/u)
      : string();
  }),
  cloudName: string().oneOf(["AZURE", "AWS", "GCP"]).nullable(),
  gcpPrivateKey: string().when("cloudName", ([cloudName]: string[]): Schema => {
    return cloudName === "GCP"
      ? string().required(translate.t("validations.required"))
      : string();
  }),
  isProduction: string().when("urlType", ([urlType]: string[]): Schema => {
    return urlType === "CSPM"
      ? string().nullable()
      : string().required(translate.t("validations.required"));
  }),
  url: string().when(
    "urlType",
    ([urlType]: string[], schema: StringSchema): Schema => {
      return urlType === "URL"
        ? schema
            .required(translate.t("validations.required"))
            .isValidValue(
              translate.t("validations.invalidUrl"),
              REGEX_TEST.INVALID_URL,
            )
            .matches(
              REGEX_TEST.FORBIDDEN_LINKS,
              translate.t("validations.invalidAppStoreUrl"),
            )
        : string()
            .required(translate.t("validations.required"))
            .isValidDynamicValidation(validateCloudName);
    },
  ),
  urlType: string()
    .oneOf(["URL", "APK", "CSPM"])
    .defined(translate.t("validations.invalidUrlType")),
});
