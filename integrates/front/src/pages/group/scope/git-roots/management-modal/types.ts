import type { IFormValues } from "../../types";

interface IManagementModalProps {
  externalId: string;
  groupName: string;
  initialValues?: IFormValues;
  isEditing: boolean;
  manyRows?: boolean;
  modalMessages: { message: string; type: string };
  organizationName: string;
  onClose: () => void;
  onUpdate?: () => void;
  onSubmitRepo: (values: IFormValues) => Promise<void>;
}

type TManagementTabs =
  | "dockerImages"
  | "environments"
  | "repository"
  | "secrets";

export type { TManagementTabs, IManagementModalProps };
