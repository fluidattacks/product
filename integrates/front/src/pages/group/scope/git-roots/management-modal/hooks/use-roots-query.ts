import { useQuery } from "@apollo/client";
import { useEffect, useMemo } from "react";

import type { IRootsResponse } from "./types";

import { GET_ACTIVE_ROOTS } from "pages/group/scope/queries";

interface IRootVars {
  groupName: string;
  search: string;
}

/** Get active git roots given a group name */
const useRootsQuery = ({ groupName, search }: IRootVars): IRootsResponse => {
  const { data, error, loading, fetchMore } = useQuery(GET_ACTIVE_ROOTS, {
    context: { skipGlobalErrorHandler: true },
    fetchPolicy: "network-only",
    variables: { first: 150, groupName, search },
  });

  useEffect((): void => {
    const { hasNextPage, endCursor } = data?.group.gitRoots.pageInfo ?? {};
    if (hasNextPage ?? false) {
      void fetchMore({ variables: { after: endCursor } });
    }
  }, [data, fetchMore]);

  const roots = useMemo(
    (): IRootsResponse["data"] =>
      data?.group.gitRoots.edges.map(
        (edge): IRootsResponse["data"] extends (infer U)[] ? U : never =>
          edge.node,
      ) ?? [],
    [data],
  );

  return {
    data: roots,
    error,
    loading,
  };
};

export { useRootsQuery };
