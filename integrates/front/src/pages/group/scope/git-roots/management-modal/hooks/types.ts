import type { ApolloError } from "@apollo/client";

import type { GetActiveRootsQuery, GetGroupsQuery } from "gql/graphql";

type TError = ApolloError | undefined;

interface IRoot {
  id: string;
  nickname: string;
  state: string;
}

interface IGroupsResponse {
  data: GetGroupsQuery["me"]["organizations"][number]["groups"];
  error: TError;
  loading: boolean;
}

interface IRootsResponse {
  data: GetActiveRootsQuery["group"]["gitRoots"]["edges"][number]["node"][];
  error: TError;
  loading: boolean;
}

export type { IRoot, IGroupsResponse, IRootsResponse };
