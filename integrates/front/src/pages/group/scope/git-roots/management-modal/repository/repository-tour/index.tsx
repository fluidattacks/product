import { Tour, baseStep } from "@fluidattacks/design";
import type { FormikErrors } from "formik";
import type { FC } from "react";
import { Fragment } from "react";
import { useTranslation } from "react-i18next";
import type { Step } from "react-joyride";

import type { IFormValues } from "../../../../types";
import { formatBooleanHealthCheck } from "../../../../utils";
import {
  getRepositoryTourStepsText,
  shouldHideRepositoryTourFooter,
} from "../../../utils";

interface IRepositoryTourProps {
  errors: FormikErrors<IFormValues>;
  onFinish: () => void;
  dirty: boolean;
  isGitAccessible: boolean;
  run: boolean;
  values: IFormValues;
}

const RepositoryTour: FC<IRepositoryTourProps> = ({
  errors,
  onFinish,
  dirty,
  isGitAccessible,
  run,
  values,
}: Readonly<IRepositoryTourProps>): JSX.Element => {
  const { t } = useTranslation();

  const steps: Step[] = [
    {
      ...baseStep,
      content: t("tours.addGitRoot.intro"),
      placement: "center",
      target: "#git-root-add-repo-url",
      title: t("group.scope.common.add"),
    },
    {
      ...baseStep,
      content: t("tours.addGitRoot.rootUrl"),
      hideBackButton: true,
      hideFooter: values.url.length === 0,
      target: "#git-root-add-repo-url",
    },
    {
      ...baseStep,
      content: t("tours.addGitRoot.rootBranch"),
      hideFooter: values.branch.length === 0,
      target: "#git-root-add-repo-branch",
    },
    {
      ...baseStep,
      content: t("tours.addGitRoot.customConnection"),
      target: "#git-root-add-use-custom-connection",
    },
    {
      ...baseStep,
      content: t("tours.addGitRoot.nickname"),
      hideFooter: values.nickname.length === 0,
      target: "#git-root-add-nickname",
    },
    {
      ...baseStep,
      content: (
        <Fragment>
          {t("tours.addGitRoot.rootCredentials.content")}
          <ul>
            <li>{getRepositoryTourStepsText(values)}</li>
            {values.credentials.name === "" ? (
              <li>{t("tours.addGitRoot.rootCredentials.name")}</li>
            ) : undefined}
            {isGitAccessible ? undefined : (
              <li>{t("tours.addGitRoot.rootCredentials.invalid")}</li>
            )}
          </ul>
        </Fragment>
      ),
      hideFooter: shouldHideRepositoryTourFooter(values),
      placement: "left",
      target: "#git-root-add-credentials",
    },
    {
      ...baseStep,
      content: (
        <Fragment>
          {t("tours.addGitRoot.rootHasHealthCheck")}
          {formatBooleanHealthCheck(values.includesHealthCheck) !== null &&
          errors.healthCheckConfirm !== undefined ? (
            <ul>
              <li>{t("tours.addGitRoot.healthCheckConditions")}</li>
            </ul>
          ) : undefined}
        </Fragment>
      ),
      hideFooter:
        formatBooleanHealthCheck(values.includesHealthCheck) === null ||
        errors.healthCheckConfirm !== undefined,
      placement: "left",
      target: "#git-root-add-health-check",
    },
    {
      ...baseStep,
      content:
        !isGitAccessible || !dirty
          ? t("tours.addGitRoot.proceedButton.invalidForm")
          : t("tours.addGitRoot.proceedButton.validForm"),
      target: "#git-root-add-confirm",
    },
  ];

  return <Tour onFinish={onFinish} run={run} steps={steps} />;
};

export type { IRepositoryTourProps };
export { RepositoryTour };
