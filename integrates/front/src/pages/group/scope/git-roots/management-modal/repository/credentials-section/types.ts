import type { IFormValues } from "../../../../types";

interface ICredentialsType {
  credExists: boolean;
  disabledCredsEdit: boolean;
  isEditing: boolean;
  manyRows: boolean;
  setCredExists: React.Dispatch<React.SetStateAction<boolean>>;
  setDisabledCredsEdit: React.Dispatch<React.SetStateAction<boolean>>;
  setShowGitAlert: React.Dispatch<React.SetStateAction<boolean>>;
  showGitAlert: boolean;
  validateGitMsg: {
    message: string;
    type: string;
  };
}

interface ICredentialsTypeProps {
  credExists: boolean;
  values: IFormValues;
}

export type { ICredentialsType, ICredentialsTypeProps };
