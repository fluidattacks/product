import { useQuery } from "@apollo/client";
import type { IAlertProps } from "@fluidattacks/design";
import { Alert, Col, Text } from "@fluidattacks/design";
import { useFormikContext } from "formik";
import isEmpty from "lodash/isEmpty";
import { Fragment, useCallback, useContext, useMemo } from "react";
import { useTranslation } from "react-i18next";
import { useTheme } from "styled-components";

import { CredentialsType } from "./credentials-type";
import type { ICredentialsType } from "./types";

import { GET_ORGANIZATION_CREDENTIALS } from "../../../../queries";
import type { ICredentialsAttr, IFormValues } from "../../../../types";
import { chooseCredentialType } from "../utils";
import type { IDropDownOption } from "components/dropdown/types";
import { Input, Select } from "components/input";
import { organizationDataContext } from "pages/organization/context";
import { formatTypeCredentials } from "utils/credentials";
import { Logger } from "utils/logger";

const CredentialsSection = ({
  credExists,
  disabledCredsEdit,
  isEditing,
  manyRows,
  setCredExists,
  setDisabledCredsEdit,
  setShowGitAlert,
  showGitAlert,
  validateGitMsg,
}: ICredentialsType): JSX.Element => {
  const { t } = useTranslation();
  const theme = useTheme();
  const { organizationId } = useContext(organizationDataContext);
  const { setFieldValue, values } = useFormikContext<IFormValues>();

  // GraphQL operations
  const {
    data: { organization: { credentials: rawCredentials } } = {
      organization: {
        credentials: [],
      },
    },
  } = useQuery(GET_ORGANIZATION_CREDENTIALS, {
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error): void => {
        Logger.error("Couldn't load organization credentials", error);
      });
    },
    variables: {
      organizationId,
    },
  });

  // Data management
  const formatCredentials = (
    creds: ICredentialsAttr[],
  ): Record<string, ICredentialsAttr> => {
    return Object.fromEntries(
      creds.map((cred): [string, ICredentialsAttr] => [cred.id, cred]),
    );
  };
  const credentials = useMemo(
    (): Record<string, ICredentialsAttr> =>
      rawCredentials && rawCredentials.length > 0
        ? formatCredentials(rawCredentials)
        : {},
    [rawCredentials],
  );
  const selectOptions = [{ header: "", value: "" }].concat(
    Object.values(credentials).map(
      (cred): { header: string; value: string } => ({
        header: cred.name,
        value: cred.id,
      }),
    ),
  );

  const onChangeExists = useCallback(
    (selection: IDropDownOption): void => {
      if (selection.value === "" || selection.value === undefined) {
        setCredExists(manyRows || false);
        setDisabledCredsEdit(manyRows || false);
        chooseCredentialType(values.url, false, setCredExists, setFieldValue);
      } else {
        const currentCred = credentials[selection.value];

        void setFieldValue(
          "credentials.typeCredential",
          formatTypeCredentials(currentCred),
        );
        void setFieldValue("credentials.type", currentCred.type);
        void setFieldValue("credentials.name", currentCred.name);
        void setFieldValue("credentials.id", currentCred.id);
        setCredExists(true);
        setDisabledCredsEdit(true);
      }
    },
    [
      credentials,
      manyRows,
      setCredExists,
      setFieldValue,
      setDisabledCredsEdit,
      values.url,
    ],
  );
  const onTypeChange = useCallback(
    (selection: IDropDownOption): void => {
      if (selection.value === "SSH") {
        void setFieldValue("credentials.type", "SSH");
        void setFieldValue("credentials.auth", "");
        void setFieldValue("credentials.isPat", false);
      }
      if (selection.value === "") {
        void setFieldValue("type", "");
        void setFieldValue("auth", "");
        void setFieldValue("isPat", false);
      }
      if (selection.value === "USER") {
        void setFieldValue("credentials.type", "HTTPS");
        void setFieldValue("credentials.auth", "USER");
        void setFieldValue("credentials.isPat", false);
      }
      if (selection.value === "TOKEN") {
        void setFieldValue("credentials.type", "HTTPS");
        void setFieldValue("credentials.auth", "TOKEN");
        void setFieldValue("credentials.isPat", true);
      }
    },
    [setFieldValue],
  );

  return (
    <Fragment>
      <Text
        color={theme.palette.gray[800]}
        fontWeight={"bold"}
        lineSpacing={3.5}
        size={"xl"}
      >
        {t("group.scope.git.repo.credentials.title")}
      </Text>
      {isEmpty(credentials) ? null : (
        <Col lg={100} md={100} sm={100}>
          <Select
            handleOnChange={onChangeExists}
            id={"credentials-id"}
            items={selectOptions}
            label={t("group.scope.git.repo.credentials.existing")}
            name={"credentials.id"}
          />
        </Col>
      )}
      <Col lg={50} md={50} sm={50}>
        <Select
          disabled={true}
          handleOnChange={onTypeChange}
          id={"credentials-type"}
          items={[
            { header: "", value: "" },
            {
              header: t("group.scope.git.repo.credentials.awsRole"),
              value: "AWSROLE",
            },
            {
              header: t("group.scope.git.repo.credentials.ssh"),
              value: "SSH",
            },
            {
              header: t("group.scope.git.repo.credentials.userHttps"),
              value: "USER",
            },
            {
              header: t("group.scope.git.repo.credentials.azureToken"),
              value: "TOKEN",
            },
            disabledCredsEdit
              ? {
                  header: t("group.scope.git.repo.credentials.oauth"),
                  value: "OAUTH",
                }
              : {},
          ]}
          label={t("group.scope.git.repo.credentials.type.text")}
          name={"credentials.typeCredential"}
          required={!isEditing}
          tooltip={t("group.scope.git.repo.credentials.type.toolTip")}
        />
      </Col>
      <Col lg={50} md={50} sm={50}>
        <Input
          disabled={disabledCredsEdit}
          label={t("group.scope.git.repo.credentials.name.text")}
          name={"credentials.name"}
          required={!isEditing}
          tooltip={t("group.scope.git.repo.credentials.name.toolTip")}
        />
      </Col>
      {isEmpty(values.url) &&
      isEmpty(values.credentials.typeCredential) ? undefined : (
        <CredentialsType credExists={credExists} values={values} />
      )}
      {!showGitAlert && validateGitMsg.message !== "" ? (
        <Alert
          onTimeOut={setShowGitAlert}
          variant={validateGitMsg.type as IAlertProps["variant"]}
        >
          {validateGitMsg.message}
        </Alert>
      ) : undefined}
    </Fragment>
  );
};

export { CredentialsSection };
