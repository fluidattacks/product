import { object, string } from "yup";

import { translate } from "utils/translations/translate";

export const validationSchema = object().shape({
  groupName: string().required(translate.t("validations.required")),
  rootId: string().required(translate.t("validations.required")),
  targetGroupName: string().required(translate.t("validations.required")),
  targetRootId: string().required(translate.t("validations.required")),
});
