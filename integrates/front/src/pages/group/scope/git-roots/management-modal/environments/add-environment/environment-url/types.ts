import type { TFormMethods } from "@fluidattacks/design";

import type { IEnvironmentSubmit } from "../types";

interface IFile {
  description: string;
  fileName: string;
  uploadDate: string;
}

interface IEnvironmentUrlProps
  extends Pick<
    TFormMethods<IEnvironmentSubmit>,
    "control" | "formState" | "getValues" | "register"
  > {
  groupName: string;
}

export type { IFile, IEnvironmentUrlProps };
