import { styled } from "styled-components";

const StyledExpandedCell = styled.div`
  grid-column: 1 / -1;
`;

export { StyledExpandedCell };
