import type { IFormValues } from "../../types";

const formInitialValues: IFormValues = {
  branch: "",
  cloningStatus: {
    message: "",
    status: "UNKNOWN",
  },
  credentials: {
    arn: "",
    auth: "",
    azureOrganization: "",
    id: "",
    isPat: false,
    isToken: false,
    key: "",
    name: "",
    password: "",
    token: "",
    type: "",
    typeCredential: "",
    user: "",
  },
  gitEnvironmentUrls: [],
  gitignore: [],
  hasExclusions: "",
  healthCheckConfirm: [],
  id: "",
  includesHealthCheck: "",
  nickname: "",
  priority: null,
  state: "ACTIVE",
  url: "",
  useEgress: false,
  useVpn: false,
  useZtna: false,
};

export { formInitialValues };
