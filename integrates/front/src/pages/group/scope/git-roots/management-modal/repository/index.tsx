/* eslint-disable functional/no-promise-reject */
import { useAbility } from "@casl/react";
import type { IAlertProps } from "@fluidattacks/design";
import { Alert, Col, Row, useConfirmDialog } from "@fluidattacks/design";
import type { FormikHelpers, FormikProps } from "formik";
import _, { isEqual } from "lodash";
import isNull from "lodash/isNull";
import mixpanel from "mixpanel-browser";
import {
  Fragment,
  useCallback,
  useContext,
  useEffect,
  useRef,
  useState,
} from "react";
import { useTranslation } from "react-i18next";

import { CheckAccessButton } from "./check-access-button";
import { CredentialsSection } from "./credentials-section";
import { Exclusions } from "./exclusions";
import { HealthCheckSection } from "./health-check-section";
import { RepositoryTour } from "./repository-tour";
import type { IRepositoryProps } from "./types";
import { UrlSection } from "./url-section";
import { formatInitialValues } from "./utils";
import { validationSchema } from "./validations";

import type { IFormValues } from "../../../types";
import { formInitialValues } from "../utils";
import { Form, InnerForm } from "components/form";
import { Select } from "components/input";
import {
  authzGroupContext,
  authzPermissionsContext,
} from "context/authz/config";
import { useAudit } from "hooks/use-audit";
import { useTour } from "hooks/use-tour";

const Repository = ({
  initialValues = formInitialValues,
  isEditing,
  manyRows = false,
  modalMessages,
  onClose,
  onSubmit,
}: Readonly<IRepositoryProps>): JSX.Element => {
  const { t } = useTranslation();
  const { tours, setCompleted } = useTour();
  const { confirm: gitignoreConfirm, ConfirmDialog: GitignoreConfirmDialog } =
    useConfirmDialog();
  const { confirm, ConfirmDialog } = useConfirmDialog();
  const prefix = "group.scope.git.repo.";
  const formRef = useRef<FormikProps<IFormValues>>(null);
  const initialCreds =
    !isNull(initialValues.credentials) && initialValues.credentials.id !== "";

  const { addAuditEvent } = useAudit();
  useEffect((): void => {
    if (initialValues.id) addAuditEvent("Root", initialValues.id);
  }, [addAuditEvent, initialValues.id]);

  const [showGitAlert, setShowGitAlert] = useState(false);
  const [showSubmitAlert, setShowSubmitAlert] = useState(false);
  const [isCheckedHealthCheck, setIsCheckedHealthCheck] = useState(isEditing);
  const [isGitAccessible, setIsGitAccessible] = useState(true);
  const [credExists, setCredExists] = useState(initialCreds || manyRows);
  const [disabledCredsEdit, setDisabledCredsEdit] = useState(
    initialCreds || manyRows,
  );
  const [validateGitMsg, setValidateGitMsg] = useState({
    message: "",
    type: "success",
  });

  const attributes = useContext(authzGroupContext);
  const permissions = useAbility(authzPermissionsContext);
  const hasAdvanced = attributes.can("has_advanced");
  const canAddExclusions = permissions.can("update_git_root_filter");
  const canUpdateUrl = permissions.can("update_git_root_edit_url");

  // Data management
  const formattedInitialValues = formatInitialValues(initialValues);
  const showTour = !isEditing && !tours.newRoot;
  const gitRootsPermissions = {
    canAddExclusions,
    hasAdvanced,
  };

  const isValidGitignoreModified = useCallback(
    async (values: IFormValues): Promise<boolean> => {
      if (!_.isEqual(values.gitignore, initialValues.gitignore)) {
        return gitignoreConfirm({
          id: "exclusions-confirm",
          message: t("group.scope.git.warnings.gitignoreModified.description"),
          title: t("group.scope.git.warnings.gitignoreModified.title"),
        });
      }

      return true;
    },
    [gitignoreConfirm, initialValues.gitignore, t],
  );

  const isValidBranchModified = useCallback(
    async (
      values: IFormValues,
      formikHelpers: FormikHelpers<IFormValues>,
    ): Promise<boolean> => {
      const prevUrl = formattedInitialValues.url;
      const prevBranch = formattedInitialValues.branch;

      if (!canUpdateUrl && !_.isEqual(values.url, prevUrl)) {
        formikHelpers.setFieldError(
          "url",
          "Insufficient permissions to edit the URL.",
        );

        return false;
      }

      if (!canUpdateUrl && !_.isEqual(values.branch, prevBranch)) {
        formikHelpers.setFieldError(
          "branch",
          "Insufficient permissions to edit the branch.",
        );

        return false;
      }

      if (
        !_.isEqual(values.url, prevUrl) ||
        !_.isEqual(values.branch, prevBranch)
      ) {
        mixpanel.track("URLorBranchChanged");

        return confirm({
          title: t("group.scope.git.warnings.urlModified.title"),
        });
      }

      return true;
    },
    [
      canUpdateUrl,
      confirm,
      formattedInitialValues.branch,
      formattedInitialValues.url,
      t,
    ],
  );

  // Form Management
  const finishTour = useCallback((): void => {
    setCompleted("newRoot");
  }, [setCompleted]);

  const confirmAndSubmit = useCallback(
    async (
      values: IFormValues,
      formikHelpers: FormikHelpers<IFormValues>,
    ): Promise<void> => {
      setShowSubmitAlert(false);

      if (isEditing) {
        const gitignoreValid = await isValidGitignoreModified(values);
        if (!gitignoreValid) {
          return Promise.reject(new Error());
        }

        const branchValid = await isValidBranchModified(values, formikHelpers);
        if (!branchValid) {
          return Promise.reject(new Error());
        }
      }
      if (
        !isEqual(initialValues.useEgress, values.useEgress) ||
        !isEqual(initialValues.useVpn, values.useVpn) ||
        !isEqual(initialValues.useZtna, values.useZtna)
      ) {
        mixpanel.track("UpdateRootExposure");
      }
      if (!isEqual(initialValues.gitignore, values.gitignore)) {
        mixpanel.track("UpdateRootExclusions");
      }

      return onSubmit(values);
    },
    [
      isValidBranchModified,
      isValidGitignoreModified,
      isEditing,
      onSubmit,
      initialValues,
    ],
  );

  return (
    <Form
      initialValues={formattedInitialValues}
      innerRef={formRef}
      name={"gitRoot"}
      onSubmit={confirmAndSubmit}
      validationSchema={validationSchema(
        isEditing,
        credExists,
        gitRootsPermissions,
        isCheckedHealthCheck,
        isGitAccessible,
      )}
    >
      {({ dirty, errors, isSubmitting, values }): JSX.Element => (
        <Fragment>
          <InnerForm
            onCancel={onClose}
            submitDisabled={
              (!isGitAccessible &&
                !values.useVpn &&
                !values.useZtna &&
                !values.useEgress) ||
              !dirty ||
              isSubmitting
            }
          >
            <UrlSection
              credExists={credExists}
              form={formRef}
              isEditing={isEditing}
              manyRows={manyRows}
              setCredExists={setCredExists}
              setIsSameHealthCheck={setIsCheckedHealthCheck}
            />
            <Row>
              <CredentialsSection
                credExists={credExists}
                disabledCredsEdit={disabledCredsEdit}
                isEditing={isEditing}
                manyRows={manyRows}
                setCredExists={setCredExists}
                setDisabledCredsEdit={setDisabledCredsEdit}
                setShowGitAlert={setShowGitAlert}
                showGitAlert={showGitAlert}
                validateGitMsg={validateGitMsg}
              />
              <CheckAccessButton
                credExists={credExists}
                setIsGitAccessible={setIsGitAccessible}
                setShowGitAlert={setShowGitAlert}
                setValidateGitMsg={setValidateGitMsg}
              />
              <Col lg={50} md={50} sm={50}>
                <Select
                  items={[
                    { header: t(`${prefix}priority.level.none`), value: "" },
                    {
                      header: t(`${prefix}priority.level.low`),
                      value: "LOW",
                    },
                    {
                      header: t(`${prefix}priority.level.medium`),
                      value: "MEDIUM",
                    },
                    {
                      header: t(`${prefix}priority.level.high`),
                      value: "HIGH",
                    },
                    {
                      header: t(`${prefix}priority.level.critical`),
                      value: "CRITICAL",
                    },
                  ]}
                  label={t(`${prefix}priority.header`)}
                  name={"priority"}
                  tooltip={t(`${prefix}priority.toolTip`)}
                />
              </Col>
            </Row>
            <Exclusions isEditing={isEditing} manyRows={manyRows} />
            <HealthCheckSection
              form={formRef}
              isEditing={isEditing}
              isSameHealthCheck={isCheckedHealthCheck}
              setIsSameHealthCheck={setIsCheckedHealthCheck}
            />
            {!showSubmitAlert && modalMessages.message !== "" ? (
              <Alert
                onTimeOut={setShowSubmitAlert}
                variant={modalMessages.type as IAlertProps["variant"]}
              >
                {modalMessages.message}
              </Alert>
            ) : undefined}
          </InnerForm>
          {showTour ? (
            <RepositoryTour
              dirty={dirty}
              errors={errors}
              isGitAccessible={isGitAccessible}
              onFinish={finishTour}
              run={showTour}
              values={values}
            />
          ) : undefined}
          <GitignoreConfirmDialog id={"gitignore-changed-modal"}>
            <i
              className={`fa-solid fa-code-pull-request-closed`}
              style={{ fontSize: "3.5rem", margin: "0 auto 2.5rem" }}
            />
          </GitignoreConfirmDialog>
          <ConfirmDialog id={"branch-changed-modal"}>
            <i
              className={`fa-solid fa-code-branch`}
              style={{ fontSize: "3.5rem", margin: "0 auto 2.5rem" }}
            />
            <Alert variant={"warning"}>
              {t("group.scope.git.repo.url.changeWarning")}
            </Alert>
          </ConfirmDialog>
        </Fragment>
      )}
    </Form>
  );
};

export { Repository };
