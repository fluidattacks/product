import { useQuery } from "@apollo/client";
import { useMemo } from "react";

import type { IGroupsResponse } from "./types";

import { GET_GROUPS } from "pages/group/scope/queries";

interface IGroupsVars {
  organizationName: string;
}

/** Get accesible groups given a organization name */
const useGroupsQuery = ({ organizationName }: IGroupsVars): IGroupsResponse => {
  const { data, error, loading } = useQuery(GET_GROUPS, {
    context: { skipGlobalErrorHandler: true },
    fetchPolicy: "cache-first",
  });

  const groups = useMemo(
    (): IGroupsResponse["data"] =>
      data?.me.organizations.find(
        (org): boolean => org.name === organizationName,
      )?.groups ?? [],
    [data, organizationName],
  );

  return {
    data: groups,
    error,
    loading,
  };
};

export { useGroupsQuery };
