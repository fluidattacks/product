import type { FetchResult } from "@apollo/client";

import type { IAddEnvironmentURL } from "./types";

import { ResourceType } from "gql/graphql";

const MOBILE_EXTENSIONS = [".zip", ".aab", ".ipa", ".apk"];
const addGcpCredentialsFlow = async (
  gcpPrivateKey: string,
  groupName: string,
  urlId: string,
  addSecret: (
    variables: Record<string, unknown>,
  ) => Promise<FetchResult<unknown>>,
): Promise<void> => {
  const keySecretName = "GCP_PRIVATE_KEY";
  const description = "";
  const addGcpCredentials = async (): Promise<void> => {
    await Promise.all([
      addSecret({
        variables: {
          description,
          groupName,
          key: keySecretName,
          resourceId: urlId,
          resourceType: ResourceType.Url,
          value: gcpPrivateKey,
        },
      }),
    ]);
  };
  await addGcpCredentials();
};

const addAzureCredentialsFlow = async (
  azureClientId: string,
  azureClientSecret: string,
  azureTenantId: string,
  groupName: string,
  url: string,
  urlId: string,
  addSecret: (
    variables: Record<string, unknown>,
  ) => Promise<FetchResult<unknown>>,
): Promise<void> => {
  const clientId = "AZURE_CLIENT_ID";
  const clientSecret = "AZURE_CLIENT_SECRET";
  const tenantId = "AZURE_TENANT_ID";
  const subscriptionId = "AZURE_SUBSCRIPTION_ID";
  const description = "";
  const addAzureCredentials = async (): Promise<void> => {
    await Promise.all([
      addSecret({
        variables: {
          description,
          groupName,
          key: clientId,
          resourceId: urlId,
          resourceType: ResourceType.Url,
          value: azureClientId,
        },
      }),
      addSecret({
        variables: {
          description,
          groupName,
          key: clientSecret,
          resourceId: urlId,
          resourceType: ResourceType.Url,
          value: azureClientSecret,
        },
      }),
      addSecret({
        variables: {
          description,
          groupName,
          key: tenantId,
          resourceId: urlId,
          resourceType: ResourceType.Url,
          value: azureTenantId,
        },
      }),
      addSecret({
        variables: {
          description,
          groupName,
          key: subscriptionId,
          resourceId: urlId,
          resourceType: ResourceType.Url,
          value: url,
        },
      }),
    ]);
  };
  await addAzureCredentials();
};

const addCredentials = async ({
  addSecret,
  azureClientId,
  azureClientSecret,
  azureTenantId,
  cloudName,
  data,
  gcpPrivateKey,
  groupName,
  url,
  urlType,
}: {
  addSecret: (
    variables: Record<string, unknown>,
  ) => Promise<FetchResult<unknown>>;
  azureClientId: string;
  azureClientSecret: string;
  azureTenantId: string;
  cloudName: string | undefined;
  data: IAddEnvironmentURL | null | undefined;
  gcpPrivateKey: string;
  groupName: string;
  url: string;
  urlType: string;
}): Promise<void> => {
  if (data) {
    const { urlId } = data.addGitEnvironmentUrl;
    const isCloudGcp = cloudName === "GCP" && urlType === "CSPM";
    const isCloudAzure = cloudName === "AZURE" && urlType === "CSPM";

    if (isCloudGcp) {
      await addGcpCredentialsFlow(gcpPrivateKey, groupName, urlId, addSecret);
    }

    if (isCloudAzure) {
      await addAzureCredentialsFlow(
        azureClientId,
        azureClientSecret,
        azureTenantId,
        groupName,
        url,
        urlId,
        addSecret,
      );
    }
  }
};

export {
  addCredentials,
  addGcpCredentialsFlow,
  addAzureCredentialsFlow,
  MOBILE_EXTENSIONS,
};
