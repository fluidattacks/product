/* eslint-disable max-lines */
import { useLazyQuery, useMutation, useQuery } from "@apollo/client";
import { useAbility } from "@casl/react";
import {
  Divider,
  Tag,
  Text,
  Tour,
  baseStep,
  useConfirmDialog,
  useModal,
} from "@fluidattacks/design";
import type { ColumnDef, Row } from "@tanstack/react-table";
import _ from "lodash";
import mixpanel from "mixpanel-browser";
import { Fragment, useCallback, useEffect, useMemo, useState } from "react";
import * as React from "react";
import { useTranslation } from "react-i18next";
import { useTheme } from "styled-components";

import { GitRootAddFileButton } from "./add-git-roots-file-button";
import { AddRootButton } from "./add-root-button";
import { Environments } from "./environments";
import { ExportButton } from "./export-button";
import { ManagementModal } from "./management-modal";
import { OauthRootModal } from "./oauth-root-modal";
import { renderDescriptionComponent } from "./root-description";
import {
  formatRootsData,
  fromDockerImagesToEnvironmentUrl,
  getEnvsFromResult,
  getGitRootsFromResult,
  getInitialValues,
  handleActivationError,
  handleCreationError,
  handleSyncError,
  handleUpdateError,
  useGitSubmit,
} from "./utils";

import { DeactivateRootModal } from "../deactivate-root-modal";
import { changeFormatter, syncButtonFormatter } from "../formatters";
import { InternalSurfaceButton } from "../internal-surface-button";
import {
  ACTIVATE_ROOT,
  ADD_ENVIRONMENT_INTEGRITY_EVENT,
  ADD_GIT_ROOT,
  GET_GIT_ROOTS,
  GET_GIT_ROOTS_FRAGMENT,
  GET_GIT_ROOTS_QUERIES,
  GET_GIT_ROOT_MANAGEMENT_DETAILS,
  GET_GROUP_DOCKER_IMAGES,
  GET_GROUP_ENV_URLS_FRAGMENT,
  GET_ORGANIZATION_EXTERNAL_ID,
  SYNC_GIT_ROOT,
  UPDATE_GIT_ROOT,
} from "../queries";
import type {
  IEnvironmentUrlAttr,
  IEnvironmentUrlData,
  IGitRootAttr,
  IGitRootData,
  IGitRootEventsData,
  IGitRootManagementDetailsAttr,
  TProvider,
} from "../types";
import { Table } from "components/table";
import { Can } from "context/authz/can";
import { authzPermissionsContext } from "context/authz/config";
import { GitRootsFilters } from "features/git-roots/filters";
import { priorityFormatter } from "features/git-roots/formatters/priority";
import { statusFormatter } from "features/vulnerabilities/formatters/status";
import { getFragmentData } from "gql/fragment-masking";
import { useDebouncedCallback, useTable, useTour } from "hooks";
import { useAudit } from "hooks/use-audit";
import { GET_EVENTS } from "pages/group/events/queries";
import type { IEventAttr, IEventData } from "pages/group/events/types";
import { formatEvents } from "pages/group/events/utils";
import { includeTagsFormatter } from "utils/format-helpers";
import { Logger } from "utils/logger";
import { msgSuccess } from "utils/notifications";

interface IGitRootsProps {
  readonly groupName: string;
  readonly organizationName: string;
}
const TIMEOUT_DURATION = 1500;
const DEBOUNCE_DELAY = 800;
const SCOPE_PATH_LENGTH = 6;
export const GitRoots: React.FC<IGitRootsProps> = ({
  groupName,
  organizationName,
}: IGitRootsProps): JSX.Element => {
  const permissions = useAbility(authzPermissionsContext);
  const { t } = useTranslation();
  const theme = useTheme();
  const [filteredRoots, setFilteredRoots] = useState<IGitRootData[]>([]);
  const [rootFiltersToSearch, setRootFiltersToSearch] = useState<
    Record<string, unknown>
  >({});
  const tableRef2 = useTable("tblGitRootEnvs");
  const { confirm, ConfirmDialog } = useConfirmDialog();
  const [rootModalMessages, setRootModalMessages] = useState({
    message: "",
    type: "success",
  });
  const canSyncGitRoot = permissions.can(
    "integrates_api_mutations_sync_git_root_mutate",
  );
  const canUpdateRootState = permissions.can(
    "integrates_api_mutations_activate_root_mutate",
  );
  const canShowModal =
    permissions.can("integrates_api_resolvers_git_root_secrets_resolve") ||
    permissions.can("integrates_api_mutations_update_git_root_mutate");

  // State management
  const [isManagingRoot, setIsManagingRoot] = useState<
    false | { mode: "ADD" | "EDIT" }
  >(false);
  const { tours, setCompleted } = useTour();
  const enableTour = !tours.newRoot;
  const [oauthProvider, setOauthProvider] = useState<TProvider>("");
  const [currentRootId, setCurrentRootId] = useState("");
  const addOauthRootModal = useModal("add-oauth-root-modal");
  const deactivateRootModal = useModal("deactivation-root-modal");
  const closeDeactivationModal = useCallback((): void => {
    deactivateRootModal.close();
    setCurrentRootId("");
  }, [deactivateRootModal]);
  const openAddModal = useCallback((): void => {
    if (enableTour) {
      setCompleted("newRoot");
    }
    setIsManagingRoot({ mode: "ADD" });
  }, [enableTour, setCompleted]);
  const openOauthRootModal = useCallback(
    (provider: TProvider): void => {
      setOauthProvider(provider);
      addOauthRootModal.open();
    },
    [addOauthRootModal],
  );

  const closeModal = useCallback((): void => {
    if (enableTour) {
      setCompleted("newRoot");
    }
    setIsManagingRoot(false);
    setRootModalMessages({ message: "", type: "success" });
  }, [enableTour, setCompleted, setRootModalMessages]);
  const [currentRow, setCurrentRow] = useState<
    IGitRootManagementDetailsAttr | undefined
  >(undefined);
  const tableRef1 = useTable(
    "tblGitRoots",
    {
      HCK: false,
    },
    ["url", "branch", "state", "priority", "cloningStatus", "nickname", "sync"],
    { left: ["url"] },
  );

  // GraphQL operations

  const {
    data,
    refetch,
    loading: dataLoading,
  } = useQuery(GET_GIT_ROOTS_QUERIES, {
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error): void => {
        Logger.error("Couldn't load Git roots queries", error);
      });
    },
    variables: {
      canGetSecrets: permissions.can(
        "integrates_api_resolvers_git_environment_url_secrets_resolve",
      ),
      groupName,
      organizationName,
    },
  });

  const { addAuditEvent } = useAudit();
  const {
    data: rootsData,
    refetch: rootsRefetch,
    fetchMore: rootsFetchMore,
  } = useQuery(GET_GIT_ROOTS, {
    fetchPolicy: "network-only",
    nextFetchPolicy: "cache-first",
    onCompleted: (): void => {
      addAuditEvent("Group.Roots", groupName);
    },
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error): void => {
        Logger.error("Couldn't load Git roots", error);
      });
    },
    variables: {
      first: 150,
      groupName,
      ...rootFiltersToSearch,
    },
  });
  const { data: eventsData } = useQuery(GET_EVENTS, {
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error): void => {
        Logger.error("Couldn't load group events", error);
      });
    },
    variables: { groupName },
  });

  const eventsDataSet = useMemo(
    (): IEventData[] =>
      formatEvents(
        eventsData === undefined
          ? []
          : (eventsData.group.events as IEventAttr[]),
      ),
    [eventsData],
  );

  const { data: dockerImagesResult } = useQuery(GET_GROUP_DOCKER_IMAGES, {
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error): void => {
        Logger.error("Couldn't load group events", error);
      });
    },
    variables: { groupName },
  });

  const envUrlsResult = getFragmentData(GET_GROUP_ENV_URLS_FRAGMENT, data);
  const externalIdResult = getFragmentData(GET_ORGANIZATION_EXTERNAL_ID, data);

  const rootsResult = getFragmentData(GET_GIT_ROOTS_FRAGMENT, rootsData);
  const [requestRootInfo] = useLazyQuery<{
    root: IGitRootManagementDetailsAttr;
  }>(GET_GIT_ROOT_MANAGEMENT_DETAILS, {
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error): void => {
        Logger.error("Couldn't load root management details", error);
      });
    },
  });

  const onUpdate = useCallback((): void => {
    setTimeout((): void => {
      void refetch();
      void rootsRefetch();
    }, TIMEOUT_DURATION);
  }, [refetch, rootsRefetch]);

  const [addGitRoot] = useMutation(ADD_GIT_ROOT, {
    onCompleted: (): void => {
      onUpdate();
      closeModal();
    },
    onError: ({ graphQLErrors }): void => {
      handleCreationError(graphQLErrors, setRootModalMessages);
    },
  });

  const [updateGitRoot] = useMutation(UPDATE_GIT_ROOT, {
    onCompleted: (): void => {
      onUpdate();
      closeModal();
      setCurrentRow(undefined);
    },
    onError: ({ graphQLErrors }): void => {
      handleUpdateError(graphQLErrors, setRootModalMessages, "root");
    },
  });

  const [activateRoot] = useMutation(ACTIVATE_ROOT, {
    onCompleted: (): void => {
      onUpdate();
      setCurrentRow(undefined);
    },
    onError: ({ graphQLErrors }): void => {
      handleActivationError(graphQLErrors);
    },
  });

  const [syncGitRoot, { loading }] = useMutation(SYNC_GIT_ROOT, {
    onCompleted: (): void => {
      mixpanel.track("SyncGitRoot");
      onUpdate();
      msgSuccess(
        t("group.scope.git.sync.success"),
        t("group.scope.git.sync.successTitle"),
      );
      setCurrentRow(undefined);
    },
    onError: ({ graphQLErrors }): void => {
      handleSyncError(graphQLErrors);
    },
  });

  // Event handlers
  const handleSyncClick = useCallback(
    (row: IGitRootData): void => {
      void syncGitRoot({ variables: { groupName, rootId: row.id } });
    },
    [groupName, syncGitRoot],
  );

  const handleRowClick = useCallback(
    (
      rowInfo: Row<IGitRootData>,
    ): ((event: React.FormEvent) => Promise<void>) => {
      return async (event: React.FormEvent): Promise<void> => {
        if (rowInfo.original.state === "ACTIVE") {
          const result = await requestRootInfo({
            variables: {
              groupName: groupName.toLocaleLowerCase(),
              rootId: rowInfo.original.id,
            },
          });
          setCurrentRow(result.data?.root);
          setIsManagingRoot({ mode: "EDIT" });
        }
        event.preventDefault();
      };
    },
    [groupName, requestRootInfo],
  );

  const handleGitSubmit = useGitSubmit(
    addGitRoot,
    groupName,
    isManagingRoot,
    setRootModalMessages,
    updateGitRoot,
  );

  const formatBoolean = useCallback(
    (value: boolean | string): string =>
      value === true || value === "yes"
        ? t("group.scope.git.healthCheck.yes")
        : t("group.scope.git.healthCheck.no"),
    [t],
  );

  const gitRoots = useMemo(
    (): IGitRootAttr[] => getGitRootsFromResult(rootsResult),
    [rootsResult],
  );

  const gitEnvironmentUrls = useMemo(
    (): IEnvironmentUrlAttr[] => getEnvsFromResult(envUrlsResult),
    [envUrlsResult],
  );

  const [checkEvents] = useMutation(ADD_ENVIRONMENT_INTEGRITY_EVENT);

  useEffect((): void => {
    gitEnvironmentUrls.forEach((envData): void => {
      if (envData.urlType === "CSPM") {
        void checkEvents({
          variables: {
            groupName,
            urlId: envData.id,
          },
        });
      }
    });
  }, [gitEnvironmentUrls, checkEvents, groupName]);

  const gitEnvironmentDockerEnvs = useMemo((): IEnvironmentUrlAttr[] => {
    const existingUrls = gitEnvironmentUrls;
    const dockerImages = dockerImagesResult?.group.dockerImages ?? [];

    const transformedDockerImages =
      fromDockerImagesToEnvironmentUrl(dockerImages);

    return [...existingUrls, ...transformedDockerImages];
  }, [dockerImagesResult, gitEnvironmentUrls]);

  const roots = useMemo(
    (): IGitRootData[] => formatRootsData(gitRoots, gitEnvironmentUrls),
    [gitEnvironmentUrls, gitRoots],
  );
  const handleSearch = useDebouncedCallback((search: string): void => {
    void rootsRefetch({ search });
  }, DEBOUNCE_DELAY);
  const handleNextPage = useCallback(async (): Promise<void> => {
    const rootsInfo = rootsResult?.group;
    const pageInfo = _.isNil(rootsInfo)
      ? { endCursor: [], hasNextPage: false }
      : {
          endCursor: rootsInfo.gitRoots.pageInfo.endCursor,
          hasNextPage: rootsInfo.gitRoots.pageInfo.hasNextPage,
        };
    if (pageInfo.hasNextPage) {
      await rootsFetchMore({ variables: { after: pageInfo.endCursor } });
    }
  }, [rootsFetchMore, rootsResult]);

  const envUrlsDataSet = useMemo((): IEnvironmentUrlData[] => {
    const envUrlsDataAt = gitEnvironmentDockerEnvs.reduce<
      Record<string, IEnvironmentUrlData>
    >((previousValue, currentValue): Record<string, IEnvironmentUrlData> => {
      const { url, include, root } = currentValue;

      if (root.state === "INACTIVE") {
        return previousValue;
      }

      return {
        ...previousValue,
        [url]: {
          ...currentValue,
          repositoryUrls: [
            ...(url in previousValue ? previousValue[url].repositoryUrls : []),
            { include, root },
          ],
        },
      };
    }, {});

    return Object.values(envUrlsDataAt);
  }, [gitEnvironmentDockerEnvs]);

  const combinedData = filteredRoots.map((rootData): IGitRootEventsData => {
    const matchedEvent = eventsDataSet.find(
      (event): boolean =>
        event.root?.id === rootData.id &&
        event.eventStatus === "Unsolved" &&
        event.environment === null,
    );

    return {
      ...rootData,
      cloningStatus: {
        ...rootData.cloningStatus,
        status:
          rootData.state === "ACTIVE"
            ? rootData.cloningStatus.status
            : "INACTIVE",
      },
      eventId: matchedEvent?.id ?? undefined,
      eventStatus: matchedEvent?.eventStatus ?? undefined,
    };
  });

  const statusTag = (status: string): JSX.Element => {
    switch (status) {
      case "OK":
        return (
          <Tag
            icon={"circle-check"}
            priority={"low"}
            tagLabel={"Cloned"}
            variant={"success"}
          />
        );
      case "FAILED":
        return (
          <Tag
            icon={"circle-xmark"}
            priority={"low"}
            tagLabel={"Failed"}
            variant={"error"}
          />
        );
      case "UNKNOWN":
        return (
          <Tag
            icon={"circle-dashed"}
            tagLabel={"Unknown"}
            variant={"inactive"}
          />
        );
      case "CLONING":
      default:
        return (
          <Tag
            icon={"spinner-third"}
            priority={"low"}
            tagLabel={"Cloning"}
            variant={"warning"}
          />
        );
    }
  };

  const handleRowExpand = useCallback(
    (row: Row<IGitRootData>): JSX.Element => {
      return renderDescriptionComponent(row.original, groupName);
    },
    [groupName],
  );

  const handleStateUpdate = useCallback(
    (row: Record<string, string>): void => {
      if (row.state === "ACTIVE") {
        deactivateRootModal.open();
        setCurrentRootId(row.id);
      } else {
        confirm({
          title: t("group.scope.common.confirm"),
        })
          .then((confirmResult: boolean): void => {
            if (confirmResult) {
              void activateRoot({ variables: { groupName, id: row.id } });
            }
          })
          .catch((): void => {
            Logger.error("An error occurred confirming state");
          });
      }
    },
    [activateRoot, confirm, deactivateRootModal, groupName, t],
  );

  const externalId = externalIdResult?.organizationId.awsExternalId;
  const { total: size } = rootsResult?.group.gitRoots ?? { total: 0 };

  const baseColumns: ColumnDef<IGitRootEventsData>[] = [
    {
      accessorKey: "url",
      enableColumnFilter: false,
      header: String(t("group.scope.git.repo.url.header")),
    },
    {
      accessorKey: "branch",
      header: String(t("group.scope.git.repo.branch.header")),
    },
    {
      accessorFn: (row): string => {
        return _.capitalize(row.state);
      },
      accessorKey: "state",
      cell: (cell): JSX.Element =>
        canUpdateRootState
          ? changeFormatter(
              "root-status",
              cell.row.original as unknown as Record<string, string>,
              handleStateUpdate,
            )
          : statusFormatter(cell.getValue() as string, false),
      header: String(t("group.scope.common.state")),
    },
    {
      accessorKey: "priority",
      cell: (cell): JSX.Element => {
        const value = cell.row.original.criticality;

        if (value === null) {
          return priorityFormatter("NONE");
        }

        return priorityFormatter(value);
      },
      header: String(t("group.scope.git.repo.priority.header")),
    },
    {
      accessorFn: (row: IGitRootEventsData): string => row.cloningStatus.status,
      cell: (cell): JSX.Element => {
        const customTag =
          cell.row.original.state === "INACTIVE" ? (
            <Tag
              icon={"power-off"}
              tagLabel={"Inactive"}
              variant={"inactive"}
            />
          ) : (
            statusTag(cell.getValue() as string)
          );

        const currentUrl: string = window.location.href;

        const modifiedUrl: string = currentUrl.endsWith("/scope")
          ? currentUrl.slice(0, -SCOPE_PATH_LENGTH)
          : currentUrl;

        const eventTag =
          cell.row.original.eventId === undefined ? undefined : (
            <Tag
              href={`${modifiedUrl}/events/${cell.row.original.eventId}/description`}
              icon={"hexagon-exclamation"}
              priority={"medium"}
              tagLabel={t("group.scope.git.repo.status.value.events")}
              variant={"error"}
            />
          );

        return includeTagsFormatter({
          customTag,
          text: eventTag,
        });
      },
      header: String(t("group.scope.git.repo.cloning.status")),
      id: "cloningStatus",
    },
    {
      accessorFn: (row: IGitRootEventsData): string | undefined =>
        formatBoolean(row.includesHealthCheck),
      header: String(t("group.scope.git.healthCheck.tableHeader")),
      id: "healthCheck",
    },
    {
      accessorKey: "nickname",
      header: String(t("group.scope.git.repo.nickname")),
    },
  ];

  const canSyncColumn: ColumnDef<IGitRootEventsData>[] = canSyncGitRoot
    ? [
        {
          accessorFn: (): string => "sync",
          cell: (cell): JSX.Element =>
            syncButtonFormatter(cell.row.original, handleSyncClick, loading),
          header: String(t("group.scope.git.repo.cloning.sync")),
          id: "sync",
        },
      ]
    : [];

  const gitRootsColumns = [...baseColumns, ...canSyncColumn];

  return (
    <Fragment>
      <Fragment>
        <Text
          color={theme.palette.gray[800]}
          fontWeight={"bold"}
          mb={1.25}
          size={"xl"}
        >
          {t("group.scope.git.title")}
        </Text>
        <Table
          columns={gitRootsColumns}
          data={combinedData}
          expandedRow={handleRowExpand}
          extraButtons={
            <Fragment>
              <ExportButton />
              <Can do={"integrates_api_mutations_add_git_root_mutate"}>
                <Can
                  do={"integrates_api_mutations_upload_git_root_file_mutate"}
                >
                  <GitRootAddFileButton />
                </Can>
              </Can>
            </Fragment>
          }
          filters={
            <GitRootsFilters
              roots={roots}
              setFilteredRoots={setFilteredRoots}
              setFiltersToSearch={setRootFiltersToSearch}
            />
          }
          onNextPage={handleNextPage}
          onRowClick={canShowModal ? handleRowClick : undefined}
          onSearch={handleSearch}
          options={{ columnToggle: true, enableSorting: true, size }}
          rightSideComponents={
            <Fragment>
              <Can do={"integrates_api_mutations_add_git_root_mutate"}>
                <AddRootButton
                  manualClick={openAddModal}
                  providersClick={openOauthRootModal}
                />
                {enableTour ? (
                  <Tour
                    run={false}
                    steps={[
                      {
                        ...baseStep,
                        content: t("tours.addGitRoot.addButton"),
                        disableBeacon: true,
                        hideFooter: true,
                        target: "#git-root-add",
                      },
                    ]}
                  />
                ) : undefined}
              </Can>
              <InternalSurfaceButton />
            </Fragment>
          }
          tableRef={tableRef1}
        />
      </Fragment>
      <ConfirmDialog />
      {dataLoading || envUrlsDataSet.length === 0 ? undefined : (
        <Fragment>
          <Divider color={theme.palette.gray[200]} mb={1.25} mt={1.25} />
          <Environments
            envUrlsDataSet={envUrlsDataSet}
            eventsDataSet={eventsDataSet}
            groupName={groupName}
            tableRef={tableRef2}
          />
        </Fragment>
      )}
      {isManagingRoot === false || externalId === undefined ? undefined : (
        <ManagementModal
          externalId={externalId}
          groupName={groupName}
          initialValues={
            isManagingRoot.mode === "EDIT"
              ? getInitialValues(currentRow)
              : undefined
          }
          isEditing={isManagingRoot.mode === "EDIT"}
          manyRows={false}
          modalMessages={rootModalMessages}
          onClose={closeModal}
          onSubmitRepo={handleGitSubmit}
          onUpdate={onUpdate}
          organizationName={organizationName}
        />
      )}
      <DeactivateRootModal
        groupName={groupName}
        modalRef={{
          ...deactivateRootModal,
          close: closeDeactivationModal,
        }}
        onUpdate={onUpdate}
        rootId={currentRootId}
      />
      <OauthRootModal
        addOauthRootModal={addOauthRootModal}
        oauthProvider={oauthProvider}
        onUpdate={onUpdate}
      />
    </Fragment>
  );
};
