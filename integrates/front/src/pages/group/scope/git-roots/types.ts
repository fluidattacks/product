import type { IEnvironmentUrlAttr, TProvider } from "../types";
import type { IUseModal } from "hooks/use-modal";

interface IOauthRootModalProps {
  addOauthRootModal: IUseModal;
  onUpdate: () => void;
  oauthProvider: TProvider;
}

interface IDescriptionProps {
  gitEnvironmentUrls: IEnvironmentUrlAttr[];
  id: string;
  nickname: string;
}

export type { IOauthRootModalProps, IDescriptionProps };
