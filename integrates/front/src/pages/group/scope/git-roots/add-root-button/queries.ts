import { graphql } from "gql";

const GET_ORGANIZATION_CREDENTIALS = graphql(`
  query GetOrganizationCredentials($organizationId: String!) {
    organization(organizationId: $organizationId) {
      __typename
      name
      credentials {
        id
        __typename
        azureOrganization
        isPat
        isToken
        name
        oauthType
        owner
        type
      }
    }
  }
`);

export { GET_ORGANIZATION_CREDENTIALS };
