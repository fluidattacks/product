import { useQuery } from "@apollo/client";
import { OAuthSelector, OptionContainer } from "@fluidattacks/design";
import { useCallback, useContext } from "react";
import type { FC, MouseEventHandler } from "react";
import { useTranslation } from "react-i18next";
import { useNavigate, useParams } from "react-router-dom";

import { GET_ORGANIZATION_CREDENTIALS } from "./queries";
import type { IAddRootProps, ICredentialsAttr } from "./types";
import { type TOAuthProvider, showProviders } from "./utils";

import { organizationDataContext } from "pages/organization/context";
import { Logger } from "utils/logger";

const AddRootButton: FC<IAddRootProps> = ({
  manualClick,
  providersClick,
}): JSX.Element => {
  const { t } = useTranslation();
  const { organizationId } = useContext(organizationDataContext);

  const navigate = useNavigate();
  const { organizationName } = useParams() as { organizationName: string };

  const onMoveToCredentials = useCallback((): void => {
    navigate(`/orgs/${organizationName}/credentials`);
  }, [organizationName, navigate]);

  const { data } = useQuery(GET_ORGANIZATION_CREDENTIALS, {
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error): void => {
        Logger.error("Couldn't load organization credentials", error);
      });
    },
    variables: {
      organizationId,
    },
  });

  const credentials: ICredentialsAttr[] =
    data === undefined
      ? []
      : (data.organization.credentials as ICredentialsAttr[]);

  const availableProviders = showProviders(credentials);

  const azureClick = useCallback((): void => {
    providersClick("AZURE");
  }, [providersClick]);

  const bitbucketClick = useCallback((): void => {
    providersClick("BITBUCKET");
  }, [providersClick]);

  const gitHubClick = useCallback((): void => {
    providersClick("GITHUB");
  }, [providersClick]);

  const gitLabClick = useCallback((): void => {
    providersClick("GITLAB");
  }, [providersClick]);

  const providersHandlers: Partial<
    Record<TOAuthProvider, { onClick: MouseEventHandler<HTMLOrSVGElement> }>
  > = {
    Azure: { onClick: azureClick },
    Bitbucket: { onClick: bitbucketClick },
    GitHub: { onClick: gitHubClick },
    GitLab: { onClick: gitLabClick },
  };

  const providers = availableProviders.reduce(
    (
      result,
      currentProvider,
    ): Partial<
      Record<TOAuthProvider, { onClick: MouseEventHandler<HTMLOrSVGElement> }>
    > => {
      return {
        ...result,
        [currentProvider]: providersHandlers[currentProvider],
      };
    },
    {},
  );

  return (
    <OAuthSelector
      align={"end"}
      buttonLabel={t("group.scope.common.add")}
      id={"repositories-providers"}
      manualOption={{
        label: t("components.repositoriesDropdown.manual.text"),
        onClick: manualClick,
      }}
      providers={providers}
    >
      <OptionContainer
        label={t("components.repositoriesDropdown.otherButton.text")}
        onClick={onMoveToCredentials}
        onlyLabel={true}
      />
    </OAuthSelector>
  );
};

export { AddRootButton };
