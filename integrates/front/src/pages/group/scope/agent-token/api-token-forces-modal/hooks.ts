import type {
  ApolloError,
  MutationFunction,
  MutationResult,
} from "@apollo/client";
import { useMutation } from "@apollo/client";
import { useTranslation } from "react-i18next";

import { GET_FORCES_TOKEN, UPDATE_FORCES_TOKEN_MUTATION } from "./queries";

import type { UpdateForcesAccessTokenMutationMutation } from "gql/graphql";
import { Logger } from "utils/logger";
import { msgError, msgSuccess } from "utils/notifications";

const useUpdateAPIToken = (
  groupName: string,
): readonly [
  MutationFunction,
  MutationResult<UpdateForcesAccessTokenMutationMutation>,
] => {
  const { t } = useTranslation();

  // Handle mutation results
  const handleOnSuccess = (
    mtResult: UpdateForcesAccessTokenMutationMutation,
  ): void => {
    if (mtResult.updateForcesAccessToken.success) {
      msgSuccess(
        t("updateForcesToken.successfully"),
        t("updateForcesToken.success"),
      );
    }
  };
  const handleOnError = ({ graphQLErrors }: ApolloError): void => {
    graphQLErrors.forEach((error): void => {
      Logger.warning("An error occurred adding access token", error);
      msgError(t("groupAlerts.errorTextsad"));
    });
  };

  const [updateAPIToken, mtResponse] = useMutation(
    UPDATE_FORCES_TOKEN_MUTATION,
    {
      onCompleted: handleOnSuccess,
      onError: handleOnError,
      refetchQueries: [{ query: GET_FORCES_TOKEN, variables: { groupName } }],
    },
  );

  return [updateAPIToken as MutationFunction, mtResponse];
};

export { useUpdateAPIToken };
