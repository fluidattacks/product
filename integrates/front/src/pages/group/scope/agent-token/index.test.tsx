import { screen, waitFor } from "@testing-library/react";
import { userEvent } from "@testing-library/user-event";
import { GraphQLError } from "graphql";
import { HttpResponse, type StrictResponse, graphql } from "msw";

import {
  GET_FORCES_TOKEN,
  UPDATE_FORCES_TOKEN_MUTATION,
} from "./api-token-forces-modal/queries";

import { AgentToken } from ".";
import type {
  GetForcesTokenQuery as GetForcesToken,
  UpdateForcesAccessTokenMutationMutation as UpdateForcesAccessToken,
} from "gql/graphql";
import { render } from "mocks";
import { LINK } from "mocks/constants";
import type { IErrorMessage } from "mocks/types";
import { msgError } from "utils/notifications";

jest.mock("utils/notifications", (): Record<string, unknown> => {
  const mockedNotifications: Record<string, () => Record<string, unknown>> =
    jest.requireActual("utils/notifications");
  jest.spyOn(mockedNotifications, "msgError").mockImplementation();
  jest.spyOn(mockedNotifications, "msgSuccess").mockImplementation();

  return mockedNotifications;
});

describe("manage devsecops access token", (): void => {
  const graphqlMocked = graphql.link(LINK);
  const openButtonModal = "searchFindings.agentTokenSection.generate";
  const revealButtonText = "updateForcesToken.revealToken";
  const generateButtonText = "updateForcesToken.generate";
  const copyButtonText = "updateForcesToken.copy.copy";

  it("should render with token set", async (): Promise<void> => {
    expect.hasAssertions();

    const mockQuery = [
      graphqlMocked.query(
        GET_FORCES_TOKEN,
        (): StrictResponse<{ data: GetForcesToken }> => {
          return HttpResponse.json({
            data: {
              group: {
                __typename: "Group",
                forcesExpDate: "date",
                forcesToken: "token",
                name: "unittesting",
              },
            },
          });
        },
        { once: true },
      ),
    ];

    render(<AgentToken groupName={"test"} />, { mocks: mockQuery });

    expect(
      screen.queryByRole("button", { name: openButtonModal }),
    ).toBeInTheDocument();

    await userEvent.click(screen.getByText(openButtonModal));
    await waitFor((): void => {
      expect(
        screen.getByText("updateForcesToken.revealToken"),
      ).not.toBeDisabled();
    });
  });

  it("should render with token unsettled", async (): Promise<void> => {
    expect.hasAssertions();

    const mockQuery = [
      graphqlMocked.query(
        GET_FORCES_TOKEN,
        (): StrictResponse<{ data: GetForcesToken }> => {
          return HttpResponse.json({
            data: {
              group: {
                __typename: "Group",
                forcesExpDate: null,
                forcesToken: null,
                name: "unittesting",
              },
            },
          });
        },
        { once: true },
      ),
      graphqlMocked.mutation(
        UPDATE_FORCES_TOKEN_MUTATION,
        (): StrictResponse<{ data: UpdateForcesAccessToken }> => {
          return HttpResponse.json({
            data: {
              updateForcesAccessToken: {
                sessionJwt: "token",
                success: true,
              },
            },
          });
        },
        { once: true },
      ),
      graphqlMocked.query(
        GET_FORCES_TOKEN,
        (): StrictResponse<{ data: GetForcesToken }> => {
          return HttpResponse.json({
            data: {
              group: {
                __typename: "Group",
                forcesExpDate: "token",
                forcesToken: "token",
                name: "unittesting",
              },
            },
          });
        },
        { once: true },
      ),
    ];

    render(<AgentToken groupName={"test"} />, { mocks: mockQuery });

    expect(
      screen.queryByRole("button", { name: openButtonModal }),
    ).toBeInTheDocument();

    await userEvent.click(screen.getByText(openButtonModal));
    await waitFor((): void => {
      expect(screen.getByText("updateForcesToken.revealToken")).toBeDisabled();
    });
    await waitFor((): void => {
      expect(screen.getByText("updateForcesToken.generate")).not.toBeDisabled();
    });
    await userEvent.click(screen.getByText(generateButtonText));
    await waitFor((): void => {
      expect(
        screen.getAllByRole("textbox", { name: "sessionJwt" })[0],
      ).toHaveValue("token");
    });
  });

  it("should render an unknown error", async (): Promise<void> => {
    expect.hasAssertions();

    const mockQueryFull = [
      graphqlMocked.query(
        GET_FORCES_TOKEN,
        (): StrictResponse<IErrorMessage> => {
          return HttpResponse.json({
            errors: [
              new GraphQLError("An error occurred getting forces token"),
            ],
          });
        },
      ),
    ];
    render(<AgentToken groupName={"unnittesting"} />, { mocks: mockQueryFull });
    await userEvent.click(screen.getByText(openButtonModal));

    expect(screen.getByText(revealButtonText)).not.toBeDisabled();
    expect(screen.getByText(copyButtonText)).toBeDisabled();

    await userEvent.click(screen.getByText(revealButtonText));

    expect(msgError).toHaveBeenNthCalledWith(1, "groupAlerts.errorTextsad");

    jest.clearAllMocks();
  });

  it("should render InvalidAuthorization error", async (): Promise<void> => {
    expect.hasAssertions();

    const mockQueryFull = [
      graphqlMocked.query(
        GET_FORCES_TOKEN,
        (): StrictResponse<IErrorMessage> => {
          return HttpResponse.json({
            errors: [new GraphQLError("Exception - Invalid Authorization")],
          });
        },
      ),
    ];
    render(<AgentToken groupName={"unnittesting"} />, { mocks: mockQueryFull });
    await userEvent.click(screen.getByText(openButtonModal));

    expect(screen.getByText(revealButtonText)).not.toBeDisabled();

    await userEvent.click(screen.getByText(revealButtonText));

    expect(msgError).toHaveBeenNthCalledWith(
      1,
      "groupAlerts.invalidAuthorization",
    );

    jest.clearAllMocks();
  });

  it("should render", (): void => {
    expect.hasAssertions();

    render(<AgentToken groupName={"unittesting"} />);

    expect(
      screen.queryByText("searchFindings.agentTokenSection.about"),
    ).toBeInTheDocument();
    expect(
      screen.queryByRole("button", {
        name: "searchFindings.agentTokenSection.install",
      }),
    ).toBeInTheDocument();

    expect(
      screen.queryByRole("button", { name: openButtonModal }),
    ).toBeInTheDocument();
  });
});
