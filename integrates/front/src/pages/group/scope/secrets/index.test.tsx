import { PureAbility } from "@casl/ability";
import { fireEvent, screen, waitFor } from "@testing-library/react";
import { userEvent } from "@testing-library/user-event";
import { GraphQLError } from "graphql";
import { HttpResponse, type StrictResponse, graphql } from "msw";
import { Route, Routes } from "react-router-dom";

import { Secrets } from ".";
import {
  ADD_SECRET,
  GET_ENVIRONMENT_URL,
  GET_ROOT_SECRETS,
  GET_SECRET_BY_KEY,
  REMOVE_SECRET,
  UPDATE_SECRET,
} from "../queries";
import { type IAuthContext, authContext } from "context/auth";
import { authzPermissionsContext } from "context/authz/config";
import type {
  AddSecretMutation,
  GetEnvironmentUrlQuery,
  GetRootQuery,
  GetSecretByKeyQuery,
  RemoveSecretMutation,
  UpdateSecretMutation,
} from "gql/graphql";
import { render } from "mocks";
import { LINK } from "mocks/constants";
import type { IErrorMessage } from "mocks/types";
import { msgError, msgSuccess } from "utils/notifications";

jest.mock("utils/notifications", (): Record<string, unknown> => {
  const mockedNotifications: Record<string, () => Record<string, unknown>> =
    jest.requireActual("utils/notifications");
  jest.spyOn(mockedNotifications, "msgError").mockImplementation();
  jest.spyOn(mockedNotifications, "msgSuccess").mockImplementation();

  return mockedNotifications;
});

const context: IAuthContext = {
  awsSubscription: null,
  tours: {
    newGroup: false,
    newRoot: false,
    welcome: false,
  },
  userEmail: "jonhDoe@fluidattacks.com",
  userName: "Jhon Doe",
};

describe("group secrets", (): void => {
  const graphqlMocked = graphql.link(LINK);
  const rootSecretsMock = graphqlMocked.query(
    GET_ROOT_SECRETS,
    (): StrictResponse<{ data: GetRootQuery }> => {
      return HttpResponse.json({
        data: {
          root: {
            __typename: "GitRoot",
            id: "4039d098-ffc5-4984-8ed3-eb17bca98e19",
            secrets: [
              {
                __typename: "Secret",
                description: "User name for user management",
                key: "username",
                owner: "jonhDoe@fluidattacks.com",
              },
            ],
          },
        },
      });
    },
  );

  it("should render empty secrets", async (): Promise<void> => {
    expect.hasAssertions();

    jest.spyOn(console, "warn").mockImplementation();

    const onClose = jest.fn();
    const secretsMockEmpty = graphqlMocked.query(
      GET_ROOT_SECRETS,
      (): StrictResponse<{ data: GetRootQuery }> => {
        return HttpResponse.json({
          data: {
            root: {
              __typename: "GitRoot",
              id: "f8771794-0428-4018-90c3-ce20525cee02",
              secrets: [],
            },
          },
        });
      },
    );
    render(
      <Routes>
        <Route
          element={
            <Secrets
              groupName={"unittesting"}
              onCloseModal={onClose}
              resourceId={"f8771794-0428-4018-90c3-ce20525cee02"}
            />
          }
          path={"/orgs/:organizationName/groups/:groupName/scope"}
        />
      </Routes>,
      {
        memoryRouter: {
          initialEntries: ["/orgs/okada/groups/unittesting/scope"],
        },
        mocks: [secretsMockEmpty],
      },
    );

    expect(screen.queryByRole("table")).toBeInTheDocument();

    await waitFor((): void => {
      expect(
        screen
          .queryAllByRole("row")[0]
          .textContent?.replace(/[^a-zA-Z ]/gu, ""),
      ).toStrictEqual(
        [
          "group.scope.git.repo.credentials.secrets.key",
          "group.scope.git.repo.credentials.secrets.description",
          "group.scope.git.repo.credentials.secrets.owner",
          "",
        ]
          .join("")
          .replace(/[^a-zA-Z ]/gu, ""),
      );
    });
    await waitFor((): void => {
      expect(screen.getByText("table.noDataIndication")).toBeInTheDocument();
    });
  });

  it("should render secrets", async (): Promise<void> => {
    expect.hasAssertions();

    jest.spyOn(console, "warn").mockImplementation();

    const onClose = jest.fn();

    render(
      <Routes>
        <Route
          element={
            <Secrets
              groupName={"unittesting"}
              onCloseModal={onClose}
              resourceId={"4039d098-ffc5-4984-8ed3-eb17bca98e19"}
            />
          }
          path={"/orgs/:organizationName/groups/:groupName/scope"}
        />
      </Routes>,
      {
        memoryRouter: {
          initialEntries: ["/orgs/okada/groups/unittesting/scope"],
        },
        mocks: [rootSecretsMock],
      },
    );

    expect(screen.queryByRole("table")).toBeInTheDocument();

    await waitFor((): void => {
      expect(
        screen
          .queryAllByRole("row")[0]
          .textContent?.replace(/[^a-zA-Z ]/gu, ""),
      ).toStrictEqual(
        [
          "group.scope.git.repo.credentials.secrets.key",
          "group.scope.git.repo.credentials.secrets.description",
          "group.scope.git.repo.credentials.secrets.owner",
          "",
        ]
          .join("")
          .replace(/[^a-zA-Z ]/gu, ""),
      );
    });
    await waitFor((): void => {
      expect(
        screen
          .queryAllByRole("row")[1]
          .textContent?.replace(/[^a-zA-Z ]/gu, ""),
      ).toStrictEqual(
        [
          "username",
          "User name for user management",
          "jonhDoe@fluidattacks.com",
        ]
          .join("")
          .replace(/[^a-zA-Z ]/gu, ""),
      );
    });
    await waitFor((): void => {
      expect(screen.queryAllByRole("button")).toHaveLength(2);
    });

    expect(
      screen.getByRole("button", {
        name: "group.scope.git.repo.credentials.secrets.add",
      }),
    ).toBeDisabled();
  });

  it("should render secrets with action buttons", async (): Promise<void> => {
    expect.hasAssertions();

    jest.spyOn(console, "warn").mockImplementation();

    const onClose = jest.fn();
    const mockedPermissions = new PureAbility<string>([
      { action: "integrates_api_mutations_add_secret_mutate" },
      { action: "integrates_api_resolvers_query_secret_resolve" },
      { action: "integrates_api_mutations_update_secret_mutate" },
      { action: "integrates_api_mutations_remove_secret_mutate" },
    ]);

    render(
      <authContext.Provider value={context}>
        <authzPermissionsContext.Provider value={mockedPermissions}>
          <Routes>
            <Route
              element={
                <Secrets
                  groupName={"unittesting"}
                  onCloseModal={onClose}
                  resourceId={"4039d098-ffc5-4984-8ed3-eb17bca98e19"}
                />
              }
              path={"/orgs/:organizationName/groups/:groupName/scope"}
            />
          </Routes>
        </authzPermissionsContext.Provider>
      </authContext.Provider>,

      {
        memoryRouter: {
          initialEntries: ["/orgs/okada/groups/unittesting/scope"],
        },
        mocks: [rootSecretsMock],
      },
    );

    expect(screen.queryByRole("table")).toBeInTheDocument();

    await waitFor((): void => {
      expect(
        screen
          .queryAllByRole("row")[0]
          .textContent?.replace(/[^a-zA-Z ]/gu, ""),
      ).toStrictEqual(
        [
          "group.scope.git.repo.credentials.secrets.key",
          "group.scope.git.repo.credentials.secrets.description",
          "group.scope.git.repo.credentials.secrets.owner",
          "",
        ]
          .join("")
          .replace(/[^a-zA-Z ]/gu, ""),
      );
    });
    await waitFor((): void => {
      expect(
        screen
          .queryAllByRole("row")[1]
          .textContent?.replace(/[^a-zA-Z ]/gu, ""),
      ).toStrictEqual(
        [
          "username",
          "User name for user management",
          "jonhDoe@fluidattacks.com",
        ]
          .join("")
          .replace(/[^a-zA-Z ]/gu, ""),
      );
    });
    await waitFor((): void => {
      expect(screen.queryAllByRole("button")).toHaveLength(5);
    });

    await waitFor((): void => {
      expect(document.querySelector("#get-secret")).toBeInTheDocument();
    });

    expect(document.querySelector("#edit-secret")).toBeInTheDocument();
    expect(
      document.querySelector("#git-root-remove-secret"),
    ).toBeInTheDocument();

    expect(
      screen.getByRole("button", {
        name: "group.scope.git.repo.credentials.secrets.add",
      }),
    ).not.toBeDisabled();
  });

  it("should edit secret", async (): Promise<void> => {
    expect.hasAssertions();

    jest.spyOn(console, "warn").mockImplementation();

    const onClose = jest.fn();
    const mockedPermissions = new PureAbility<string>([
      { action: "integrates_api_mutations_add_secret_mutate" },
      { action: "integrates_api_resolvers_query_secret_resolve" },
      { action: "integrates_api_mutations_update_secret_mutate" },
      { action: "integrates_api_mutations_remove_secret_mutate" },
    ]);
    const rootSecretValueMock = graphqlMocked.query(
      GET_SECRET_BY_KEY,
      (): StrictResponse<{ data: GetSecretByKeyQuery }> => {
        return HttpResponse.json({
          data: {
            secret: {
              __typename: "Secret",
              description: "User name for user management",
              key: "username",
              owner: "jonhDoe@fluidattacks.com",
              value: "jane_doe",
            },
          },
        });
      },
    );
    const mockedMutation = graphqlMocked.mutation(
      UPDATE_SECRET,
      ({
        variables,
      }): StrictResponse<IErrorMessage | { data: UpdateSecretMutation }> => {
        const { groupName, description, resourceId } = variables;
        if (
          groupName === "unittesting" &&
          description === "test description" &&
          resourceId === "4039d098-ffc5-4984-8ed3-eb17bca98e19"
        ) {
          return HttpResponse.json({
            data: {
              updateSecret: { success: true },
            },
          });
        }

        return HttpResponse.json({
          errors: [new GraphQLError("An error occurred updating secret")],
        });
      },
    );
    render(
      <authContext.Provider value={context}>
        <authzPermissionsContext.Provider value={mockedPermissions}>
          <Routes>
            <Route
              element={
                <Secrets
                  groupName={"unittesting"}
                  onCloseModal={onClose}
                  resourceId={"4039d098-ffc5-4984-8ed3-eb17bca98e19"}
                />
              }
              path={"/orgs/:organizationName/groups/:groupName/scope"}
            />
          </Routes>
        </authzPermissionsContext.Provider>
      </authContext.Provider>,

      {
        memoryRouter: {
          initialEntries: ["/orgs/okada/groups/unittesting/scope"],
        },
        mocks: [rootSecretsMock, rootSecretValueMock, mockedMutation],
      },
    );

    const btnConfirm = "Confirm";

    expect(screen.queryByRole("table")).toBeInTheDocument();

    await waitFor((): void => {
      expect(
        screen
          .queryAllByRole("row")[1]
          .textContent?.replace(/[^a-zA-Z ]/gu, ""),
      ).toStrictEqual(
        [
          "username",
          "User name for user management",
          "jonhDoe@fluidattacks.com",
        ]
          .join("")
          .replace(/[^a-zA-Z ]/gu, ""),
      );
    });
    await waitFor((): void => {
      expect(screen.queryAllByRole("button")).toHaveLength(5);
    });

    await userEvent.click(
      document.querySelectorAll("#edit-secret")[0].firstChild as Element,
    );

    await waitFor((): void => {
      expect(
        screen.queryByText("group.scope.git.repo.credentials.secrets.title"),
      ).toBeInTheDocument();
    });

    expect(screen.getByRole("textbox", { name: "key" })).toBeDisabled();
    expect(screen.getByRole("textbox", { name: "value" })).toBeInTheDocument();
    expect(
      screen.getByRole("textbox", { name: "description" }),
    ).toBeInTheDocument();

    expect(screen.getByRole("button", { name: btnConfirm })).toBeDisabled();

    await userEvent.clear(screen.getByRole("textbox", { name: "description" }));
    await userEvent.type(
      screen.getByRole("textbox", { name: "description" }),
      "test description",
    );

    await waitFor((): void => {
      expect(
        screen.getByRole("button", { name: btnConfirm }),
      ).not.toBeDisabled();
    });

    fireEvent.click(screen.getByRole("button", { name: btnConfirm }));

    await waitFor((): void => {
      expect(msgSuccess).toHaveBeenCalledWith(
        "group.scope.git.repo.credentials.secrets.successUpdate",
        "group.scope.git.repo.credentials.secrets.successTitle",
      );
    });
  });

  it("should handle error editing secret", async (): Promise<void> => {
    expect.hasAssertions();

    jest.spyOn(console, "warn").mockImplementation();

    const onClose = jest.fn();
    const mockedPermissions = new PureAbility<string>([
      { action: "integrates_api_mutations_add_secret_mutate" },
      { action: "integrates_api_resolvers_query_secret_resolve" },
      { action: "integrates_api_mutations_update_secret_mutate" },
      { action: "integrates_api_mutations_remove_secret_mutate" },
    ]);
    const rootSecretValueMock = graphqlMocked.query(
      GET_SECRET_BY_KEY,
      (): StrictResponse<{ data: GetSecretByKeyQuery }> => {
        return HttpResponse.json({
          data: {
            secret: {
              __typename: "Secret",
              description: "User name for user management",
              key: "username",
              owner: "jonhDoe@fluidattacks.com",
              value: "jane_doe",
            },
          },
        });
      },
    );
    const mockedMutation = graphqlMocked.mutation(
      UPDATE_SECRET,
      (): StrictResponse<IErrorMessage> => {
        return HttpResponse.json({
          errors: [new GraphQLError("An error occurred updating secret")],
        });
      },
    );
    render(
      <authContext.Provider value={context}>
        <authzPermissionsContext.Provider value={mockedPermissions}>
          <Routes>
            <Route
              element={
                <Secrets
                  groupName={"unittesting"}
                  onCloseModal={onClose}
                  resourceId={"4039d098-ffc5-4984-8ed3-eb17bca98e19"}
                />
              }
              path={"/orgs/:organizationName/groups/:groupName/scope"}
            />
          </Routes>
        </authzPermissionsContext.Provider>
      </authContext.Provider>,

      {
        memoryRouter: {
          initialEntries: ["/orgs/okada/groups/unittesting/scope"],
        },
        mocks: [rootSecretsMock, rootSecretValueMock, mockedMutation],
      },
    );

    const btnConfirm = "Confirm";

    expect(screen.queryByRole("table")).toBeInTheDocument();

    await waitFor((): void => {
      expect(screen.queryAllByRole("button")).toHaveLength(5);
    });

    await userEvent.click(
      document.querySelectorAll("#edit-secret")[0].firstChild as Element,
    );

    await waitFor((): void => {
      expect(
        screen.queryByText("group.scope.git.repo.credentials.secrets.title"),
      ).toBeInTheDocument();
    });

    expect(screen.getByRole("textbox", { name: "key" })).toBeDisabled();
    expect(screen.getByRole("textbox", { name: "value" })).toBeInTheDocument();
    expect(
      screen.getByRole("textbox", { name: "description" }),
    ).toBeInTheDocument();

    expect(screen.getByRole("button", { name: btnConfirm })).toBeDisabled();

    await userEvent.clear(screen.getByRole("textbox", { name: "description" }));
    await userEvent.type(
      screen.getByRole("textbox", { name: "description" }),
      "test error",
    );

    await waitFor((): void => {
      expect(
        screen.getByRole("button", { name: btnConfirm }),
      ).not.toBeDisabled();
    });

    fireEvent.click(screen.getByRole("button", { name: btnConfirm }));

    await waitFor((): void => {
      expect(msgError).toHaveBeenCalledWith("groupAlerts.errorTextsad");
    });
  });

  it("should render secret value and copy it", async (): Promise<void> => {
    expect.hasAssertions();

    const mockClipboard = { writeText: jest.fn() };
    // eslint-disable-next-line functional/immutable-data
    Object.assign(navigator, {
      clipboard: mockClipboard,
    });

    jest.spyOn(console, "warn").mockImplementation();

    const onClose = jest.fn();
    const mockedPermissions = new PureAbility<string>([
      { action: "integrates_api_mutations_add_secret_mutate" },
      { action: "integrates_api_resolvers_query_secret_resolve" },
      { action: "integrates_api_mutations_update_secret_mutate" },
      { action: "integrates_api_mutations_remove_secret_mutate" },
    ]);
    const rootSecretValueMock = graphqlMocked.query(
      GET_SECRET_BY_KEY,
      (): StrictResponse<{ data: GetSecretByKeyQuery }> => {
        return HttpResponse.json({
          data: {
            secret: {
              __typename: "Secret",
              description: "User name for user management",
              key: "username",
              owner: "jonhDoe@fluidattacks.com",
              value: "jane_doe",
            },
          },
        });
      },
    );

    render(
      <authContext.Provider value={context}>
        <authzPermissionsContext.Provider value={mockedPermissions}>
          <Routes>
            <Route
              element={
                <Secrets
                  groupName={"unittesting"}
                  onCloseModal={onClose}
                  resourceId={"4039d098-ffc5-4984-8ed3-eb17bca98e19"}
                />
              }
              path={"/orgs/:organizationName/groups/:groupName/scope"}
            />
          </Routes>
        </authzPermissionsContext.Provider>
      </authContext.Provider>,

      {
        memoryRouter: {
          initialEntries: ["/orgs/okada/groups/unittesting/scope"],
        },
        mocks: [rootSecretsMock, rootSecretValueMock],
      },
    );

    expect(screen.queryByRole("table")).toBeInTheDocument();

    await waitFor((): void => {
      expect(
        screen
          .queryAllByRole("row")[1]
          .textContent?.replace(/[^a-zA-Z ]/gu, ""),
      ).toStrictEqual(
        [
          "username",
          "User name for user management",
          "jonhDoe@fluidattacks.com",
        ]
          .join("")
          .replace(/[^a-zA-Z ]/gu, ""),
      );
    });
    await waitFor((): void => {
      expect(screen.queryAllByRole("button")).toHaveLength(5);
    });

    await userEvent.click(
      document.querySelectorAll("#get-secret")[0].firstChild as Element,
    );

    expect(screen.queryAllByText("username")).toHaveLength(2);

    expect(screen.getByRole("textbox", { name: "secretValue" })).toHaveValue(
      "jane_doe",
    );

    await userEvent.click(
      screen.getByRole("button", {
        name: "group.scope.git.repo.credentials.secrets.copy.copy",
      }),
    );

    expect(mockClipboard.writeText).toHaveBeenCalledWith("jane_doe");

    await waitFor((): void => {
      expect(msgSuccess).toHaveBeenCalledWith(
        "group.scope.git.repo.credentials.secrets.copy.successfully",
        "group.scope.git.repo.credentials.secrets.copy.success",
      );
    });
  });

  it("should remove secret", async (): Promise<void> => {
    expect.hasAssertions();

    jest.spyOn(console, "warn").mockImplementation();

    const onClose = jest.fn();
    const mockedPermissions = new PureAbility<string>([
      { action: "integrates_api_mutations_add_secret_mutate" },
      { action: "integrates_api_resolvers_query_secret_resolve" },
      { action: "integrates_api_mutations_update_secret_mutate" },
      { action: "integrates_api_mutations_remove_secret_mutate" },
    ]);
    const rootSecretValueMock = graphqlMocked.query(
      GET_SECRET_BY_KEY,
      (): StrictResponse<{ data: GetSecretByKeyQuery }> => {
        return HttpResponse.json({
          data: {
            secret: {
              __typename: "Secret",
              description: "User name for user management",
              key: "username",
              owner: "jonhDoe@fluidattacks.com",
              value: "jane_doe",
            },
          },
        });
      },
    );
    const mockedMutation = graphqlMocked.mutation(
      REMOVE_SECRET,
      ({
        variables,
      }): StrictResponse<IErrorMessage | { data: RemoveSecretMutation }> => {
        const { groupName, key, resourceId } = variables;
        if (
          groupName === "unittesting" &&
          key === "username" &&
          resourceId === "4039d098-ffc5-4984-8ed3-eb17bca98e19"
        ) {
          return HttpResponse.json({
            data: {
              removeSecret: { success: true },
            },
          });
        }

        return HttpResponse.json({
          errors: [new GraphQLError("An error occurred removing secret")],
        });
      },
    );
    render(
      <authContext.Provider value={context}>
        <authzPermissionsContext.Provider value={mockedPermissions}>
          <Routes>
            <Route
              element={
                <Secrets
                  groupName={"unittesting"}
                  onCloseModal={onClose}
                  resourceId={"4039d098-ffc5-4984-8ed3-eb17bca98e19"}
                />
              }
              path={"/orgs/:organizationName/groups/:groupName/scope"}
            />
          </Routes>
        </authzPermissionsContext.Provider>
      </authContext.Provider>,

      {
        memoryRouter: {
          initialEntries: ["/orgs/okada/groups/unittesting/scope"],
        },
        mocks: [rootSecretsMock, rootSecretValueMock, mockedMutation],
      },
    );

    expect(screen.queryByRole("table")).toBeInTheDocument();

    await waitFor((): void => {
      expect(
        screen
          .queryAllByRole("row")[1]
          .textContent?.replace(/[^a-zA-Z ]/gu, ""),
      ).toStrictEqual(
        [
          "username",
          "User name for user management",
          "jonhDoe@fluidattacks.com",
        ]
          .join("")
          .replace(/[^a-zA-Z ]/gu, ""),
      );
    });
    await waitFor((): void => {
      expect(screen.queryAllByRole("button")).toHaveLength(5);
    });

    await userEvent.click(
      document.querySelectorAll("#git-root-remove-secret")[0]
        .firstChild as Element,
    );
    await waitFor((): void => {
      expect(
        screen.getByText("group.scope.git.repo.credentials.secrets.remove"),
      ).toBeInTheDocument();
    });

    await userEvent.click(screen.getByRole("button", { name: "Confirm" }));

    await waitFor((): void => {
      expect(msgSuccess).toHaveBeenCalledWith(
        "group.scope.git.repo.credentials.secrets.removed",
        "group.scope.git.repo.credentials.secrets.successTitle",
      );
    });
  });

  it("should render environments secrets", async (): Promise<void> => {
    expect.hasAssertions();

    jest.spyOn(console, "warn").mockImplementation();

    const onClose = jest.fn();
    const mockedPermissions = new PureAbility<string>([
      { action: "integrates_api_mutations_add_secret_mutate" },
      { action: "integrates_api_resolvers_query_secret_resolve" },
      { action: "integrates_api_mutations_update_secret_mutate" },
      { action: "integrates_api_mutations_remove_secret_mutate" },
    ]);
    const contextEnv: IAuthContext = {
      ...context,
      userEmail: "jdoe@fluidattacks.com",
    };
    const mockedEnvUrl = graphqlMocked.query(
      GET_ENVIRONMENT_URL,
      ({
        variables,
      }): StrictResponse<IErrorMessage | { data: GetEnvironmentUrlQuery }> => {
        const { groupName, urlId } = variables;
        if (
          groupName === "unittesting" &&
          urlId === "00fbe3579a253b43239954a545dc0536e6c83094"
        ) {
          return HttpResponse.json({
            data: {
              environmentUrl: {
                __typename: "GitEnvironmentUrl",
                createdAt: null,
                id: "00fbe3579a253b43239954a545dc0536e6c83094",
                rootId: "4039d098-ffc5-4984-8ed3-eb17bca98e19",
                secrets: [
                  {
                    __typename: "Secret",
                    description: "Environment secret description",
                    key: "Key value",
                    owner: "jdoe@fluidattacks.com",
                  },
                ],
                url: "https://app.fluidattacks.com/test",
              },
            },
          });
        }

        return HttpResponse.json({
          errors: [new GraphQLError("Error getting root environment urls")],
        });
      },
    );
    render(
      <authContext.Provider value={contextEnv}>
        <authzPermissionsContext.Provider value={mockedPermissions}>
          <Routes>
            <Route
              element={
                <Secrets
                  environmentSecret={true}
                  groupName={"unittesting"}
                  isOpen={true}
                  onCloseModal={onClose}
                  resourceId={"00fbe3579a253b43239954a545dc0536e6c83094"}
                />
              }
              path={"/orgs/:organizationName/groups/:groupName/scope"}
            />
          </Routes>
        </authzPermissionsContext.Provider>
      </authContext.Provider>,

      {
        memoryRouter: {
          initialEntries: ["/orgs/okada/groups/unittesting/scope"],
        },
        mocks: [mockedEnvUrl],
      },
    );

    expect(screen.queryByRole("table")).toBeInTheDocument();

    await waitFor((): void => {
      expect(
        screen
          .queryAllByRole("row")[0]
          .textContent?.replace(/[^a-zA-Z ]/gu, ""),
      ).toStrictEqual(
        [
          "group.scope.git.repo.credentials.secrets.key",
          "group.scope.git.repo.credentials.secrets.description",
          "group.scope.git.repo.credentials.secrets.owner",
          "",
        ]
          .join("")
          .replace(/[^a-zA-Z ]/gu, ""),
      );
    });
    await waitFor((): void => {
      expect(
        screen
          .queryAllByRole("row")[1]
          .textContent?.replace(/[^a-zA-Z ]/gu, ""),
      ).toStrictEqual(
        ["Key value", "Environment secret description", "jdoe@fluidattacks.com"]
          .join("")
          .replace(/[^a-zA-Z ]/gu, ""),
      );
    });
    await waitFor((): void => {
      expect(screen.queryAllByRole("button")).toHaveLength(6);
    });
    await waitFor((): void => {
      expect(document.querySelector("#get-secret")).toBeInTheDocument();
    });

    expect(document.querySelector("#edit-secret")).toBeInTheDocument();
    expect(
      document.querySelector("#git-root-remove-secret"),
    ).toBeInTheDocument();

    expect(
      screen.getByRole("button", {
        name: "group.scope.git.repo.credentials.secrets.add",
      }),
    ).not.toBeDisabled();
  });

  it("should add git secrets", async (): Promise<void> => {
    expect.hasAssertions();

    jest.spyOn(console, "warn").mockImplementation();

    const onClose = jest.fn();
    const mockedPermissions = new PureAbility<string>([
      { action: "integrates_api_mutations_add_secret_mutate" },
      { action: "integrates_api_resolvers_query_secret_resolve" },
      { action: "integrates_api_mutations_update_secret_mutate" },
      { action: "integrates_api_mutations_remove_secret_mutate" },
    ]);
    const mockedMutation = graphqlMocked.mutation(
      ADD_SECRET,
      ({
        variables,
      }): StrictResponse<IErrorMessage | { data: AddSecretMutation }> => {
        const { groupName, key, description, value, resourceId } = variables;
        if (
          groupName === "unittesting" &&
          key === "test key" &&
          description === "test description" &&
          value === "test value" &&
          resourceId === "4039d098-ffc5-4984-8ed3-eb17bca98e19"
        ) {
          return HttpResponse.json({
            data: {
              addSecret: { success: true },
            },
          });
        }

        return HttpResponse.json({
          errors: [new GraphQLError("An error occurred adding secret")],
        });
      },
    );
    render(
      <authContext.Provider value={context}>
        <authzPermissionsContext.Provider value={mockedPermissions}>
          <Routes>
            <Route
              element={
                <Secrets
                  groupName={"unittesting"}
                  onCloseModal={onClose}
                  resourceId={"4039d098-ffc5-4984-8ed3-eb17bca98e19"}
                />
              }
              path={"/orgs/:organizationName/groups/:groupName/scope"}
            />
          </Routes>
        </authzPermissionsContext.Provider>
      </authContext.Provider>,

      {
        memoryRouter: {
          initialEntries: ["/orgs/okada/groups/unittesting/scope"],
        },
        mocks: [rootSecretsMock, mockedMutation],
      },
    );

    expect(screen.queryByRole("table")).toBeInTheDocument();

    const btnConfirm = "Confirm";

    await waitFor((): void => {
      expect(
        screen
          .queryAllByRole("row")[0]
          .textContent?.replace(/[^a-zA-Z ]/gu, ""),
      ).toStrictEqual(
        [
          "group.scope.git.repo.credentials.secrets.key",
          "group.scope.git.repo.credentials.secrets.description",
          "group.scope.git.repo.credentials.secrets.owner",
          "",
        ]
          .join("")
          .replace(/[^a-zA-Z ]/gu, ""),
      );
    });
    await waitFor((): void => {
      expect(
        screen
          .queryAllByRole("row")[1]
          .textContent?.replace(/[^a-zA-Z ]/gu, ""),
      ).toStrictEqual(
        [
          "username",
          "User name for user management",
          "jonhDoe@fluidattacks.com",
        ]
          .join("")
          .replace(/[^a-zA-Z ]/gu, ""),
      );
    });

    expect(
      screen.getByRole("button", {
        name: "group.scope.git.repo.credentials.secrets.add",
      }),
    ).not.toBeDisabled();

    await userEvent.click(
      screen.getByRole("button", {
        name: "group.scope.git.repo.credentials.secrets.add",
      }),
    );

    expect(
      screen.getByText("group.scope.git.repo.credentials.secrets.title"),
    ).toBeInTheDocument();

    expect(screen.getByRole("button", { name: btnConfirm })).toBeDisabled();

    await userEvent.type(
      screen.getByRole("textbox", { name: "key" }),
      "test key",
    );
    await userEvent.type(
      screen.getByRole("textbox", { name: "value" }),
      "test value",
    );
    await userEvent.type(
      screen.getByRole("textbox", { name: "description" }),
      "test description",
    );

    expect(screen.getByRole("button", { name: btnConfirm })).not.toBeDisabled();

    fireEvent.click(screen.getByRole("button", { name: btnConfirm }));

    await waitFor((): void => {
      expect(msgSuccess).toHaveBeenCalledWith(
        "group.scope.git.repo.credentials.secrets.successAdd",
        "group.scope.git.repo.credentials.secrets.successTitle",
      );
    });
  });

  it("should handle error adding git secrets", async (): Promise<void> => {
    expect.hasAssertions();

    jest.spyOn(console, "warn").mockImplementation();

    const onClose = jest.fn();
    const mockedPermissions = new PureAbility<string>([
      { action: "integrates_api_mutations_add_secret_mutate" },
      { action: "integrates_api_resolvers_query_secret_resolve" },
      { action: "integrates_api_mutations_update_secret_mutate" },
      { action: "integrates_api_mutations_remove_secret_mutate" },
    ]);
    const mockedMutation = graphqlMocked.mutation(
      ADD_SECRET,
      (): StrictResponse<IErrorMessage> => {
        return HttpResponse.json({
          errors: [new GraphQLError("An error occurred adding secret")],
        });
      },
    );
    render(
      <authContext.Provider value={context}>
        <authzPermissionsContext.Provider value={mockedPermissions}>
          <Routes>
            <Route
              element={
                <Secrets
                  groupName={"unittesting"}
                  onCloseModal={onClose}
                  resourceId={"4039d098-ffc5-4984-8ed3-eb17bca98e19"}
                />
              }
              path={"/orgs/:organizationName/groups/:groupName/scope"}
            />
          </Routes>
        </authzPermissionsContext.Provider>
      </authContext.Provider>,

      {
        memoryRouter: {
          initialEntries: ["/orgs/okada/groups/unittesting/scope"],
        },
        mocks: [rootSecretsMock, mockedMutation],
      },
    );

    expect(screen.queryByRole("table")).toBeInTheDocument();

    const btnConfirm = "Confirm";

    expect(
      screen.getByRole("button", {
        name: "group.scope.git.repo.credentials.secrets.add",
      }),
    ).not.toBeDisabled();

    await userEvent.click(
      screen.getByRole("button", {
        name: "group.scope.git.repo.credentials.secrets.add",
      }),
    );

    expect(
      screen.getByText("group.scope.git.repo.credentials.secrets.title"),
    ).toBeInTheDocument();

    expect(screen.getByRole("button", { name: btnConfirm })).toBeDisabled();

    await userEvent.type(
      screen.getByRole("textbox", { name: "key" }),
      "test key",
    );
    await userEvent.type(
      screen.getByRole("textbox", { name: "value" }),
      "test value",
    );
    await userEvent.type(
      screen.getByRole("textbox", { name: "description" }),
      "test error",
    );

    expect(screen.getByRole("button", { name: btnConfirm })).not.toBeDisabled();

    fireEvent.click(screen.getByRole("button", { name: btnConfirm }));

    await waitFor((): void => {
      expect(msgError).toHaveBeenCalledWith("groupAlerts.errorTextsad");
    });
  });
});
