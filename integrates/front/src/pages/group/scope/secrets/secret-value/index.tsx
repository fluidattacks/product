import { useMutation, useQuery } from "@apollo/client";
import {
  Button,
  Container,
  Gap,
  IconButton,
  Input,
  Modal,
  useConfirmDialog,
  useModal,
} from "@fluidattacks/design";
import { Fragment, useCallback, useState } from "react";
import { useTranslation } from "react-i18next";
import { useTheme } from "styled-components";

import {
  GET_ENVIRONMENT_URL,
  GET_ROOT_SECRETS,
  GET_SECRET_BY_KEY,
  REMOVE_SECRET,
} from "../../queries";
import type { ISecretValueProps } from "../types";
import { Can } from "context/authz/can";
import { ResourceType } from "gql/graphql";
import { Logger } from "utils/logger";
import { msgError, msgSuccess } from "utils/notifications";

const SecretValue = ({
  groupName,
  resourceId,
  resourceType,
  secretDescription,
  secretKey,
  enableCondition,
  onEdit,
}: ISecretValueProps): JSX.Element => {
  const { t } = useTranslation();
  const { confirm, ConfirmDialog } = useConfirmDialog();
  const [isCopying, setIsCopying] = useState(false);
  const modalProps = useModal("copy-secret-modal");
  const theme = useTheme();

  const refetchQueriesByResourceType = {
    [ResourceType.Root]: [
      {
        query: GET_ROOT_SECRETS,
        variables: {
          groupName,
          rootId: resourceId,
        },
      },
    ],
    [ResourceType.Url]: [
      {
        query: GET_ENVIRONMENT_URL,
        variables: {
          groupName,
          urlId: resourceId,
        },
      },
    ],
  };

  const { data } = useQuery(GET_SECRET_BY_KEY, {
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error): void => {
        Logger.error("Couldn't load secrets", error);
      });
    },
    skip: !isCopying,
    variables: {
      groupName,
      resourceId,
      resourceType,
      secretKey,
    },
  });

  const handleSecretModal = useCallback((): void => {
    setIsCopying(!isCopying);
  }, [isCopying]);

  const handleOnEdit = useCallback((): void => {
    onEdit(secretKey, secretDescription);
  }, [onEdit, secretDescription, secretKey]);

  const handleCopy = useCallback((): void => {
    try {
      const { clipboard } = navigator;
      void clipboard.writeText(data?.secret?.value ?? "");
      msgSuccess(
        t("group.scope.git.repo.credentials.secrets.copy.successfully"),
        t("group.scope.git.repo.credentials.secrets.copy.success"),
      );
      handleSecretModal();
    } catch {
      msgError(t("group.scope.git.repo.credentials.secrets.copy.failed"));
    }
  }, [t, data?.secret?.value, handleSecretModal]);

  const [removeSecret] = useMutation(REMOVE_SECRET, {
    onCompleted: (): void => {
      msgSuccess(
        t("group.scope.git.repo.credentials.secrets.removed"),
        t("group.scope.git.repo.credentials.secrets.successTitle"),
      );
    },
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error): void => {
        msgError(t("groupAlerts.errorTextsad"));
        Logger.error("Couldn't remove secret", error);
      });
    },
    refetchQueries: refetchQueriesByResourceType[resourceType],
  });

  const handleRemoveClick = useCallback((): void => {
    void removeSecret({
      variables: {
        groupName,
        key: secretKey,
        resourceId,
        resourceType,
      },
    });
  }, [groupName, removeSecret, resourceId, secretKey, resourceType]);

  const onConfirmDelete = useCallback((): void => {
    confirm({
      title: t("group.scope.git.repo.credentials.secrets.remove"),
    })
      .then((result: boolean): void => {
        if (result) {
          handleRemoveClick();
        }
      })
      .catch((): void => {
        Logger.error("An error occurred during the filtering process");
      });
  }, [confirm, handleRemoveClick, t]);

  return (
    <Fragment>
      {enableCondition ? (
        <Gap>
          <Can do={"integrates_api_resolvers_query_secret_resolve"}>
            <IconButton
              border={`1px solid ${theme.palette.gray[100]} !important`}
              icon={"eye"}
              iconSize={"xxs"}
              iconType={"fa-light"}
              id={"get-secret"}
              onClick={handleSecretModal}
              variant={"ghost"}
            />
          </Can>
          <Can do={"integrates_api_mutations_update_secret_mutate"}>
            <IconButton
              border={`1px solid ${theme.palette.gray[100]} !important`}
              icon={"pencil"}
              iconSize={"xxs"}
              iconType={"fa-light"}
              id={"edit-secret"}
              onClick={handleOnEdit}
              variant={"ghost"}
            />
          </Can>
          <Can do={"integrates_api_mutations_remove_secret_mutate"}>
            <IconButton
              border={`1px solid ${theme.palette.gray[100]} !important`}
              icon={"trash"}
              iconSize={"xxs"}
              iconType={"fa-light"}
              id={"git-root-remove-secret"}
              onClick={onConfirmDelete}
              variant={"ghost"}
            />
          </Can>
          <Modal
            modalRef={{
              ...modalProps,
              close: handleSecretModal,
              isOpen: isCopying,
            }}
            size={"sm"}
            title={secretKey}
          >
            <Container
              alignItems={"center"}
              display={"flex"}
              gap={1}
              justify={"center"}
            >
              <Input
                disabled={true}
                maskValue={true}
                name={"secretValue"}
                value={data?.secret?.value ?? ""}
              />
              <Button icon={"copy"} onClick={handleCopy} variant={"ghost"}>
                {t("group.scope.git.repo.credentials.secrets.copy.copy")}
              </Button>
            </Container>
          </Modal>
        </Gap>
      ) : (
        <div />
      )}
      <ConfirmDialog />
    </Fragment>
  );
};

export { SecretValue };
