import { Buffer } from "buffer";

import _ from "lodash";

import type {
  ICredentials,
  IGitRootAttr,
  IIPRootAttr,
  IURLRootAttr,
  TRoot,
} from "./types";

const isGitRoot = (root: TRoot): root is IGitRootAttr =>
  root.__typename === "GitRoot";

const isIPRoot = (root: TRoot): root is IIPRootAttr =>
  root.__typename === "IPRoot";

const isURLRoot = (root: TRoot): root is IURLRootAttr =>
  root.__typename === "URLRoot";

const getAddGitRootCredentials = (
  credentials: ICredentials,
): Record<string, boolean | string | undefined> | undefined => {
  if (_.isEmpty(credentials.id)) {
    if (
      _.isEmpty(credentials.key) &&
      _.isEmpty(credentials.user) &&
      _.isEmpty(credentials.password) &&
      _.isEmpty(credentials.token) &&
      _.isEmpty(credentials.arn)
    ) {
      return undefined;
    }

    return {
      arn: credentials.arn,
      azureOrganization:
        _.isUndefined(credentials.azureOrganization) ||
        _.isUndefined(credentials.isPat) ||
        !credentials.isPat
          ? undefined
          : credentials.azureOrganization,
      isPat: _.isUndefined(credentials.isPat) ? false : credentials.isPat,
      key: _.isEmpty(credentials.key)
        ? undefined
        : Buffer.from(credentials.key).toString("base64"),
      name: credentials.name,
      password: credentials.password,
      token: credentials.token,
      type: credentials.type,
      user: credentials.user,
    };
  }

  return {
    id: credentials.id,
  };
};

const getUpdateGitRootCredentials = (
  credentials: ICredentials,
): Record<string, boolean | string | undefined> | undefined => {
  if (_.isEmpty(credentials.id)) {
    if (
      _.isEmpty(credentials.key) &&
      _.isEmpty(credentials.user) &&
      _.isEmpty(credentials.password) &&
      _.isEmpty(credentials.token)
    ) {
      return undefined;
    }

    return {
      key: _.isEmpty(credentials.key)
        ? undefined
        : Buffer.from(credentials.key).toString("base64"),
      name: credentials.name,
      password: credentials.password,
      token: credentials.token,
      type: credentials.type,
      user: credentials.user,
    };
  }

  return {
    id: credentials.id,
  };
};

const formatBooleanHealthCheck = (
  includesHealthCheck: boolean | string,
): boolean | null => {
  if (includesHealthCheck === true || includesHealthCheck === "yes") {
    return true;
  }

  if (includesHealthCheck === false || includesHealthCheck === "no") {
    return false;
  }

  return null;
};

const formatStringHealthCheck = (
  includesHealthCheck: boolean | string,
): string => {
  if (includesHealthCheck === true || includesHealthCheck === "yes") {
    return "yes";
  }

  if (includesHealthCheck === false || includesHealthCheck === "no") {
    return "no";
  }

  return "";
};

export {
  isGitRoot,
  isIPRoot,
  isURLRoot,
  formatBooleanHealthCheck,
  formatStringHealthCheck,
  getAddGitRootCredentials,
  getUpdateGitRootCredentials,
};
