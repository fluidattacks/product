import { Form, InnerForm, Input, Modal } from "@fluidattacks/design";
import type { IUseModal } from "@fluidattacks/design";
import { type FC, Fragment, StrictMode, useEffect } from "react";
import { useTranslation } from "react-i18next";
import { object, string } from "yup";

import type { IIPRootAttr } from "../../types";
import { useAudit } from "hooks/use-audit";

interface IManagementModalProps {
  readonly initialValues: IIPRootAttr | undefined;
  readonly modalRef: IUseModal;
  readonly onClose: () => void;
  readonly onSubmit: (values: {
    address: string;
    id: string;
    nickname: string;
  }) => Promise<void>;
}

const validationSchema = object().shape({
  address: string().required(),
  nickname: string()
    .required()
    .matches(/^[a-zA-Z_0-9-]{1,128}$/u),
});

const ManagementModal: FC<IManagementModalProps> = ({
  initialValues = {
    __typename: "IPRoot",
    address: "",
    id: "",
    nickname: "",
    state: "ACTIVE",
  },
  modalRef,
  onClose,
  onSubmit,
}): JSX.Element => {
  const { t } = useTranslation();
  const isEditing = initialValues.address !== "";

  const { addAuditEvent } = useAudit();
  useEffect((): void => {
    if (initialValues.id) addAuditEvent("Root", initialValues.id);
  }, [addAuditEvent, initialValues.id]);

  return (
    <StrictMode>
      <Modal
        id={"ipRoot"}
        modalRef={{ ...modalRef, close: onClose }}
        size={"sm"}
        title={t(`group.scope.common.${isEditing ? "edit" : "add"}`)}
      >
        <Form
          cancelButton={{ onClick: onClose }}
          defaultValues={{
            address: initialValues.address,
            id: initialValues.id,
            nickname: initialValues.nickname,
          }}
          onSubmit={onSubmit}
          yupSchema={validationSchema}
        >
          <InnerForm>
            {({ formState, getFieldState, register }): JSX.Element => {
              const addressState = getFieldState("address");
              const nicknameState = getFieldState("nickname");

              return (
                <Fragment>
                  <Input
                    disabled={isEditing}
                    error={formState.errors.address?.message?.toString()}
                    isTouched={addressState.isTouched}
                    isValid={!addressState.invalid}
                    label={t("group.scope.ip.address")}
                    name={"address"}
                    register={register}
                  />
                  <Input
                    error={formState.errors.nickname?.message?.toString()}
                    isTouched={nicknameState.isTouched}
                    isValid={!nicknameState.invalid}
                    label={t("group.scope.ip.nickname")}
                    name={"nickname"}
                    register={register}
                  />
                </Fragment>
              );
            }}
          </InnerForm>
        </Form>
      </Modal>
    </StrictMode>
  );
};

export { ManagementModal };
