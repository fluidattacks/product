import { TableButton } from "@fluidattacks/design";
import isNull from "lodash/isNull";
import { useCallback } from "react";
import * as React from "react";
import { useTranslation } from "react-i18next";

import type { IGitRootData } from "../../types";

interface ISyncButtonFormatterProps {
  readonly row: IGitRootData;
  readonly changeFunction: (arg: IGitRootData) => void;
  readonly loading: boolean;
}

const SyncButtonFormatter = ({
  row,
  changeFunction,
  loading,
}: ISyncButtonFormatterProps): JSX.Element => {
  const { t } = useTranslation();

  const handleOnChange = useCallback(
    (ev: React.SyntheticEvent): void => {
      ev.stopPropagation();
      changeFunction(row);
    },
    [changeFunction, row],
  );

  return (
    <TableButton
      disabled={
        row.state !== "ACTIVE" ||
        isNull(row.credentials) ||
        (!isNull(row.credentials) && row.credentials.name === "") ||
        loading
      }
      icon={"sync"}
      id={"gitRootSync"}
      label={t("buttons.update")}
      onClick={handleOnChange}
      variant={"submit"}
    />
  );
};

export const syncButtonFormatter = (
  row: IGitRootData,
  changeFunction: (arg: IGitRootData) => void,
  loading: boolean,
): JSX.Element => {
  return (
    <SyncButtonFormatter
      changeFunction={changeFunction}
      loading={loading}
      row={row}
    />
  );
};
