import {
  Alert,
  ComboBox,
  Form,
  type IItem,
  InnerForm,
  Input,
  Modal,
  TextArea,
  useModal,
} from "@fluidattacks/design";
import { capitalize } from "lodash";
import { StrictMode } from "react";
import { Fragment } from "react";
import { useTranslation } from "react-i18next";

import type { IDeleteGroupModalProps } from "./types";
import { validationSchema } from "./validations";

const DeleteGroupModal = ({
  groupName,
  isOpen,
  onClose,
  onSubmit,
  values,
}: Readonly<IDeleteGroupModalProps>): JSX.Element => {
  const { t } = useTranslation();
  const modalProps = useModal("delete-group");

  const selectOptions: IItem[] = [
    {
      name: t("searchFindings.servicesTable.deleteGroup.reason.noSystem"),
      value: "NO_SYSTEM",
    },
    {
      name: t("searchFindings.servicesTable.deleteGroup.reason.noSectst"),
      value: "NO_SECTST",
    },
    {
      name: t("searchFindings.servicesTable.deleteGroup.reason.diffSectst"),
      value: "DIFF_SECTST",
    },
    {
      name: t("searchFindings.servicesTable.deleteGroup.reason.rename"),
      value: "RENAME",
    },
    {
      name: t("searchFindings.servicesTable.deleteGroup.reason.migration"),
      value: "MIGRATION",
    },
    {
      name: t("searchFindings.servicesTable.deleteGroup.reason.pocOver"),
      value: "POC_OVER",
    },
    {
      name: t("searchFindings.servicesTable.deleteGroup.reason.trCancelled"),
      value: "TR_CANCELLED",
    },
    {
      name: t("searchFindings.servicesTable.deleteGroup.reason.mistake"),
      value: "MISTAKE",
    },
    {
      name: t("searchFindings.servicesTable.deleteGroup.reason.other"),
      value: "OTHER",
    },
  ];

  return (
    <StrictMode>
      <Modal
        modalRef={{ ...modalProps, close: onClose, isOpen }}
        size={"md"}
        title={t("searchFindings.servicesTable.deleteGroup.deleteGroup")}
      >
        <Form
          cancelButton={{ onClick: onClose }}
          defaultValues={values}
          onSubmit={onSubmit}
          yupSchema={validationSchema(capitalize(groupName))}
        >
          <InnerForm>
            {({ control, getFieldState, register, watch }): JSX.Element => {
              const confirmationState = getFieldState("confirmation");
              const commentsState = getFieldState("comments");

              return (
                <Fragment>
                  <Alert>
                    {t("searchFindings.servicesTable.deleteGroup.warningBody")}
                  </Alert>
                  <Input
                    error={confirmationState.error?.message}
                    isTouched={confirmationState.isTouched}
                    isValid={!confirmationState.invalid}
                    label={t(
                      "searchFindings.servicesTable.deleteGroup.typeGroupName",
                    )}
                    name={"confirmation"}
                    placeholder={capitalize(groupName)}
                    register={register}
                    type={"text"}
                  />
                  <ComboBox
                    control={control}
                    items={selectOptions}
                    label={t(
                      "searchFindings.servicesTable.deleteGroup.reason.title",
                    )}
                    name={"reason"}
                    placeholder={t(
                      "searchFindings.servicesTable.deleteGroup.reason.default",
                    )}
                    tooltip={t(
                      "searchFindings.servicesTable.deleteGroup.reason.tooltip",
                    )}
                  />
                  <TextArea
                    error={commentsState.error?.message}
                    isTouched={commentsState.isTouched}
                    isValid={!commentsState.invalid}
                    label={t("searchFindings.servicesTable.modal.observations")}
                    maxLength={250}
                    name={"comments"}
                    placeholder={t(
                      "searchFindings.servicesTable.modal.observationsPlaceholder",
                    )}
                    register={register}
                    watch={watch}
                  />
                </Fragment>
              );
            }}
          </InnerForm>
        </Form>
      </Modal>
    </StrictMode>
  );
};

export { DeleteGroupModal };
