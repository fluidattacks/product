import { graphql } from "gql";

const REMOVE_GROUP_MUTATION = graphql(`
  mutation RemoveGroupMutation(
    $comments: String
    $groupName: String!
    $reason: RemoveGroupReason!
  ) {
    removeGroup(comments: $comments, groupName: $groupName, reason: $reason) {
      success
    }
  }
`);

export { REMOVE_GROUP_MUTATION };
