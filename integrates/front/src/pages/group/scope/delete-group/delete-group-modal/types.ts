interface IDeleteGroupValues {
  comments: string;
  confirmation: string;
  reason: string;
}

interface IDeleteGroupModalProps {
  groupName: string;
  isOpen: boolean;
  onClose: () => void;
  onSubmit: (values: IDeleteGroupValues) => void;
  values: IDeleteGroupValues;
}

export type { IDeleteGroupModalProps, IDeleteGroupValues };
