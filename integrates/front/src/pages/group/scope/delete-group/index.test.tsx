import { screen, waitFor } from "@testing-library/react";
import { userEvent } from "@testing-library/user-event";
import { GraphQLError } from "graphql";
import { HttpResponse, type StrictResponse, graphql } from "msw";
import { Route, Routes } from "react-router-dom";

import { REMOVE_GROUP_MUTATION } from "./delete-group-modal/queries";

import { GET_GROUP_DATA } from "../queries";
import type {
  GetGroupDataQuery as GetGroupData,
  RemoveGroupMutationMutation as RemoveGroup,
} from "gql/graphql";
import {
  Language,
  ManagedType,
  RemoveGroupReason,
  ServiceType,
} from "gql/graphql";
import { render } from "mocks";
import { LINK } from "mocks/constants";
import type { IErrorMessage, IMessage } from "mocks/types";
import { DeleteGroup } from "pages/group/scope/delete-group";
import { msgError, msgSuccess } from "utils/notifications";

jest.mock("utils/notifications", (): Record<string, unknown> => {
  const mockedNotifications: Record<string, () => Record<string, unknown>> =
    jest.requireActual("utils/notifications");
  jest.spyOn(mockedNotifications, "msgError").mockImplementation();
  jest.spyOn(mockedNotifications, "msgSuccess").mockImplementation();

  return mockedNotifications;
});

describe("deleteGroup", (): void => {
  const graphqlMocked = graphql.link(LINK);

  it("should render", async (): Promise<void> => {
    expect.hasAssertions();

    render(
      <Routes>
        <Route element={<DeleteGroup />} path={"/:groupName"} />
      </Routes>,
      {
        memoryRouter: {
          initialEntries: ["/TEST"],
        },
      },
    );

    expect(
      screen.queryByRole("button", {
        name: "searchFindings.servicesTable.deleteGroup.deleteGroup",
      }),
    ).toBeInTheDocument();
    expect(
      screen.queryByText(
        "searchFindings.servicesTable.deleteGroup.warningBody",
      ),
    ).not.toBeInTheDocument();

    await userEvent.click(screen.getByRole("button"));
    await waitFor((): void => {
      expect(
        screen.queryByText(
          "searchFindings.servicesTable.deleteGroup.warningBody",
        ),
      ).toBeInTheDocument();
    });
  });

  it("should delete a group", async (): Promise<void> => {
    expect.hasAssertions();

    const mocksMutation = [
      graphqlMocked.mutation(
        REMOVE_GROUP_MUTATION,
        ({
          variables,
        }): StrictResponse<IErrorMessage | { data: RemoveGroup }> => {
          const { groupName, comments, reason } = variables;

          if (
            groupName === "TEST" &&
            comments === "This is a test for the group's removal" &&
            reason === RemoveGroupReason.Mistake
          ) {
            return HttpResponse.json({
              data: {
                removeGroup: { success: true },
              },
            });
          }

          return HttpResponse.json({
            errors: [new GraphQLError("Error removing group")],
          });
        },
      ),
    ];
    render(
      <Routes>
        <Route element={<DeleteGroup />} path={"/:groupName"} />
      </Routes>,
      {
        memoryRouter: {
          initialEntries: ["/TEST"],
        },
        mocks: mocksMutation,
      },
    );

    expect(
      screen.queryByRole("button", {
        name: "searchFindings.servicesTable.deleteGroup.deleteGroup",
      }),
    ).toBeInTheDocument();
    expect(
      screen.queryByText(
        "searchFindings.servicesTable.deleteGroup.warningBody",
      ),
    ).not.toBeInTheDocument();

    await userEvent.click(screen.getByRole("button"));

    await waitFor((): void => {
      expect(
        screen.queryByText(
          "searchFindings.servicesTable.deleteGroup.warningBody",
        ),
      ).toBeInTheDocument();
    });

    expect(screen.queryByRole("button", { name: "Confirm" })).toBeDisabled();

    await userEvent.type(
      screen.getByRole("textbox", { name: "confirmation" }),
      "Test",
    );

    await userEvent.click(
      screen.getByText("searchFindings.servicesTable.deleteGroup.reason.title"),
    );
    await userEvent.click(
      screen.getByText(
        "searchFindings.servicesTable.deleteGroup.reason.mistake",
      ),
    );

    await userEvent.type(
      screen.getByRole("textbox", { name: "comments" }),
      "This is a test for the group's removal",
    );

    expect(
      screen.queryByRole("button", { name: "Confirm" }),
    ).not.toBeDisabled();

    await userEvent.click(screen.getByRole("button", { name: "Confirm" }));

    await waitFor((): void => {
      expect(msgSuccess).toHaveBeenCalledWith(
        "searchFindings.servicesTable.success",
        "searchFindings.servicesTable.successTitle",
      );
    });
  });

  it("should show error when delete a trial group", async (): Promise<void> => {
    expect.hasAssertions();

    const dataQuery = graphqlMocked.query(
      GET_GROUP_DATA,
      (): StrictResponse<{ data: GetGroupData }> => {
        return HttpResponse.json({
          data: {
            group: {
              __typename: "Group",
              businessId: "",
              businessName: "",
              description: "Integrates unit test project",
              hasAdvanced: false,
              hasEssential: true,
              language: Language.En,
              managed: ManagedType.Trial,
              name: "testtrial",
              service: ServiceType.White,
              sprintDuration: 1,
              sprintStartDate: "2021-06-06T00:00:00",
              subscription: "CONTINUOUS",
            },
          },
        });
      },
      { once: true },
    );
    const mocksMutation = [
      graphqlMocked.mutation(
        REMOVE_GROUP_MUTATION,
        ({
          variables,
        }): StrictResponse<IErrorMessage | { data: RemoveGroup }> => {
          const { groupName, comments, reason } = variables;

          if (
            groupName === "testtrial" &&
            comments === "This is a test for the group's removal" &&
            reason === RemoveGroupReason.Other
          ) {
            return HttpResponse.json({
              errors: [
                new GraphQLError(
                  "Exception - The action is not allowed during the free trial",
                ),
              ],
            });
          }

          return HttpResponse.json({
            data: {
              removeGroup: { success: true },
            },
          });
        },
      ),
    ];
    render(
      <Routes>
        <Route element={<DeleteGroup />} path={"/:groupName"} />
      </Routes>,
      {
        memoryRouter: {
          initialEntries: ["/testtrial"],
        },
        mocks: [...mocksMutation, dataQuery],
      },
    );

    expect(
      screen.queryByRole("button", {
        name: "searchFindings.servicesTable.deleteGroup.deleteGroup",
      }),
    ).toBeInTheDocument();
    expect(
      screen.queryByText(
        "searchFindings.servicesTable.deleteGroup.warningBody",
      ),
    ).not.toBeInTheDocument();

    await userEvent.click(screen.getByRole("button"));

    await waitFor((): void => {
      expect(
        screen.queryByText(
          "searchFindings.servicesTable.deleteGroup.warningBody",
        ),
      ).toBeInTheDocument();
    });

    expect(screen.queryByRole("button", { name: "Confirm" })).toBeDisabled();

    await userEvent.type(
      screen.getByRole("textbox", { name: "confirmation" }),
      "Testtrial",
    );

    await userEvent.click(
      screen.getByText("searchFindings.servicesTable.deleteGroup.reason.title"),
    );
    await userEvent.click(
      screen.getByText("searchFindings.servicesTable.deleteGroup.reason.other"),
    );

    await userEvent.type(
      screen.getByRole("textbox", { name: "comments" }),
      "This is a test for the group's removal",
    );

    expect(
      screen.queryByRole("button", { name: "Confirm" }),
    ).not.toBeDisabled();

    await userEvent.click(screen.getByRole("button", { name: "Confirm" }));

    await waitFor((): void => {
      expect(msgError).toHaveBeenCalledWith(
        "searchFindings.servicesTable.deleteGroup.alerts.trialRestrictionError",
      );
    });
  });

  it("should show error when delete group", async (): Promise<void> => {
    expect.hasAssertions();

    const mocksMutation = [
      graphqlMocked.mutation(
        REMOVE_GROUP_MUTATION,
        ({
          variables,
        }): StrictResponse<
          { data: RemoveGroup } | { errors: [IMessage, IMessage] }
        > => {
          const { groupName, comments, reason } = variables;

          if (
            groupName === "TEST" &&
            comments === "This is a test for the group's removal" &&
            reason === RemoveGroupReason.Mistake
          ) {
            return HttpResponse.json({
              errors: [
                new GraphQLError("Exception - The group has pending actions"),
                new GraphQLError("Error removing group"),
              ],
            });
          }

          return HttpResponse.json({
            data: {
              removeGroup: { success: true },
            },
          });
        },
      ),
    ];
    jest.spyOn(console, "warn").mockImplementation();
    render(
      <Routes>
        <Route element={<DeleteGroup />} path={"/:groupName"} />
      </Routes>,
      {
        memoryRouter: {
          initialEntries: ["/TEST"],
        },
        mocks: mocksMutation,
      },
    );

    expect(
      screen.queryByRole("button", {
        name: "searchFindings.servicesTable.deleteGroup.deleteGroup",
      }),
    ).toBeInTheDocument();
    expect(
      screen.queryByText(
        "searchFindings.servicesTable.deleteGroup.warningBody",
      ),
    ).not.toBeInTheDocument();

    await userEvent.click(screen.getByRole("button"));

    await waitFor((): void => {
      expect(
        screen.queryByText(
          "searchFindings.servicesTable.deleteGroup.warningBody",
        ),
      ).toBeInTheDocument();
    });

    expect(screen.queryByRole("button", { name: "Confirm" })).toBeDisabled();

    await userEvent.type(
      screen.getByRole("textbox", { name: "confirmation" }),
      "Test",
    );

    await userEvent.click(
      screen.getByText("searchFindings.servicesTable.deleteGroup.reason.title"),
    );
    await userEvent.click(
      screen.getByText(
        "searchFindings.servicesTable.deleteGroup.reason.mistake",
      ),
    );

    await userEvent.type(
      screen.getByRole("textbox", { name: "comments" }),
      "This is a test for the group's removal",
    );

    expect(
      screen.queryByRole("button", { name: "Confirm" }),
    ).not.toBeDisabled();

    await userEvent.click(screen.getByRole("button", { name: "Confirm" }));

    await waitFor((): void => {
      expect(msgError).toHaveBeenNthCalledWith(
        1,
        "searchFindings.servicesTable.deleteGroup.alerts.pendingActionsError",
      );
    });
  });
});
