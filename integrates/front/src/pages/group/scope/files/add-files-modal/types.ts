interface IAddFileValues {
  description: string;
  file: FileList;
}

interface IAddFilesModalProps {
  isAddingFileToDB: boolean;
  isOpen: boolean;
  isUploading: boolean;
  progressPercentage: number;
  onClose: () => void;
  onSubmit: (values: IAddFileValues) => void;
  values: IAddFileValues;
}

export type { IAddFilesModalProps, IAddFileValues };
