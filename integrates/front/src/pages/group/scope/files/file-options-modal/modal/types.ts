import type { IFileOptionsModalProps } from "../types";

interface IModalProps extends IFileOptionsModalProps {
  onConfirmDelete: () => void;
  groupName: string;
  organizationName: string;
  ConfirmDialog: React.FC;
}

export type { IModalProps };
