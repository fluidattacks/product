import { useMutation } from "@apollo/client";
import { useAbility } from "@casl/react";
import {
  Alert,
  Button,
  Col,
  Container,
  ProgressBar,
  Row,
  Span,
  Text,
  useConfirmDialog,
} from "@fluidattacks/design";
import { Form } from "formik";
import { isUndefined } from "lodash";
import isEmpty from "lodash/isEmpty";
import mixpanel from "mixpanel-browser";
import { Fragment, useCallback, useState } from "react";
import * as React from "react";
import { Trans, useTranslation } from "react-i18next";

import type { IModalProps } from "./types";
import { validationSchema } from "./validations";

import type { IFile } from "../../types";
import { FormModal } from "components/form-modal";
import { InputFile } from "components/input";
import { Can } from "context/authz/can";
import { authzPermissionsContext } from "context/authz/config";
import { MOBILE_EXTENSIONS } from "pages/group/scope/git-roots/management-modal/environments/add-environment/utils";
import {
  GET_FILES,
  GET_GIT_ROOTS_QUERIES,
  SIGN_POST_URL_MUTATION,
  UPDATE_GIT_ROOT_ENVIRONMENT_FILE,
} from "pages/group/scope/queries";
import { Logger } from "utils/logger";
import { msgError, msgSuccess } from "utils/notifications";
import { uploadGroupFileToS3 } from "utils/upload-file-to-s3";
import { sanitizeFileName } from "utils/validations";

const MIN_SUCCESS_STATUS_CODE = 200;
const MAX_SUCCESS_STATUS_CODE = 300;
const Modal: React.FC<IModalProps> = ({
  apkEnvironments,
  canRemove,
  ConfirmDialog,
  fileName,
  filesDataSet,
  onClose,
  onConfirmDelete,
  onDownload,
  modalRef,
  groupName,
  organizationName,
}): JSX.Element => {
  const { t } = useTranslation();
  const permissions = useAbility(authzPermissionsContext);
  const [isButtonEnabled, setIsButtonEnabled] = useState(false);
  const [progressPercentage, setProgressPercentage] = useState(0);
  const [isUpdatingFileInEnvironment, setIsUpdatingFileInEnvironment] =
    useState(false);
  const { confirm, ConfirmDialog: ConfirmDialogReplace } = useConfirmDialog();

  const [uploadFile] = useMutation(SIGN_POST_URL_MUTATION, {
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error): void => {
        if (error.message === "Exception - Invalid characters in filename") {
          msgError(t("searchFindings.tabResources.invalidChars"));
        } else {
          msgError(t("groupAlerts.errorTextsad"));
          Logger.error("An error occurred uploading group files", error);
        }
      });
    },
  });
  const [updateGitRootEnvironmentFile] = useMutation(
    UPDATE_GIT_ROOT_ENVIRONMENT_FILE,
    {
      onError: ({ graphQLErrors }): void => {
        graphQLErrors.forEach((error): void => {
          if (error.message === "Exception - Invalid file name") {
            msgError(
              t("validations.invalidFileNameEnvironment", {
                extensions: MOBILE_EXTENSIONS.join(", "),
              }),
            );
          } else if (
            error.message === "File name already exists in group files"
          ) {
            msgError(t("validations.invalidFileName"));
          } else {
            msgError(t("groupAlerts.errorTextsad"));
            Logger.error("An error occurred update environment file", error);
          }
        });
      },
    },
  );

  const disableButton = useCallback((): void => {
    setIsButtonEnabled(true);
  }, []);

  const enableButton = useCallback((): void => {
    setIsButtonEnabled(false);
  }, []);
  const onSubmit = useCallback(
    async (formikValues: { file?: FileList }): Promise<void> => {
      if (formikValues.file === undefined) return;

      const sanitizedFileName = sanitizeFileName(formikValues.file[0].name);
      const newFile = new File([formikValues.file[0]], sanitizedFileName, {
        type: formikValues.file[0].type,
      });

      const repeatedFiles = filesDataSet.filter(
        (file): boolean => file.fileName === sanitizedFileName,
      );
      if (repeatedFiles.length > 0) {
        msgError(t("searchFindings.tabResources.repeatedItem"));

        return;
      }

      const confirmResult = await confirm({
        message: (
          <Alert>
            <Trans
              components={{
                bold: (
                  <Span fontWeight={"bold"} size={"sm"}>
                    {undefined}
                  </Span>
                ),
              }}
              i18nKey={"searchFindings.tabResources.files.form.warning"}
              t={t}
              values={{
                newFileName: sanitizedFileName,
                oldFileName: fileName,
              }}
            />
          </Alert>
        ),
        title: t("searchFindings.tabResources.files.form.button"),
      });
      if (!confirmResult) return;
      disableButton();

      const firstFile: IFile = {
        description: "",
        fileName: sanitizedFileName,
        uploadDate: new Date().toISOString(),
      };

      const results = await uploadFile({
        variables: {
          filesDataInput: [firstFile],
          groupName,
        },
      });

      if (!results.data) return;

      const onError = (): void => {
        enableButton();
        onClose();
      };

      try {
        const status = await uploadGroupFileToS3(
          results,
          newFile,
          groupName,
          onError,
          setProgressPercentage,
        );

        if (
          status < MIN_SUCCESS_STATUS_CODE ||
          status >= MAX_SUCCESS_STATUS_CODE
        ) {
          msgError(t("groupAlerts.errorTextsad"));

          return;
        }
        setIsUpdatingFileInEnvironment(true);
        await updateGitRootEnvironmentFile({
          onCompleted: (resultData): void => {
            const { updateGitRootEnvironmentFile: mutationResults } =
              resultData;
            const { success } = mutationResults;

            if (!success) {
              msgError(t("groupAlerts.errorTextsad"));
              Logger.error("An error occurred adding group files to the db");
              setIsUpdatingFileInEnvironment(false);
              enableButton();
              onClose();

              return;
            }
            mixpanel.track("UpdateGitRootEnvironmentFile");
            msgSuccess(
              t("searchFindings.tabResources.files.form.success"),
              t("searchFindings.tabUsers.titleSuccess"),
            );
            onClose();
          },
          refetchQueries: [
            {
              query: GET_GIT_ROOTS_QUERIES,
              variables: {
                canGetSecrets: permissions.can(
                  "integrates_api_resolvers_git_environment_url_secrets_resolve",
                ),
                groupName,
                organizationName,
              },
            },
            { query: GET_FILES, variables: { groupName } },
          ],
          variables: {
            fileName: firstFile.fileName,
            groupName,
            oldFileName: fileName,
            rootId: apkEnvironments[fileName].root.id,
            urlId: apkEnvironments[fileName].id,
          },
        });
      } catch (err) {
        Logger.error("Error uploading file to s3", err);
      }
      enableButton();
    },
    [
      apkEnvironments,
      confirm,
      fileName,
      filesDataSet,
      groupName,
      disableButton,
      enableButton,
      onClose,
      organizationName,
      permissions,
      updateGitRootEnvironmentFile,
      t,
      uploadFile,
    ],
  );

  const description = (
    <Fragment>
      {t("searchFindings.tabResources.modalOptionsContent")}
      <b>{fileName}</b>
      {"?"}
    </Fragment>
  );

  return (
    <React.Fragment>
      <FormModal
        enableReinitialize={true}
        initialValues={{
          file: undefined as FileList | undefined,
        }}
        modalRef={modalRef}
        onSubmit={onSubmit}
        size={"sm"}
        title={t("searchFindings.tabResources.modalOptionsTitle")}
        validationSchema={validationSchema(fileName)}
      >
        {({ values }): JSX.Element => (
          <Form>
            <Container>
              <Container
                display={"flex"}
                flexDirection={"column"}
                gap={0.5}
                mb={0.5}
              >
                <Text mb={1.5} mt={0.5} size={"sm"}>
                  {description}
                </Text>
                <Container display={"flex"} justify={"space-between"}>
                  {canRemove ? (
                    <Button
                      icon={"trash"}
                      onClick={onConfirmDelete}
                      variant={"tertiary"}
                    >
                      {t("searchFindings.tabResources.removeRepository")}
                    </Button>
                  ) : undefined}

                  <Button
                    icon={"arrow-down-to-line"}
                    onClick={onDownload}
                    variant={"primary"}
                  >
                    {t("searchFindings.tabResources.download")}
                  </Button>
                </Container>
                <Can
                  do={
                    "integrates_api_mutations_update_git_root_environment_file_mutate"
                  }
                >
                  {fileName in apkEnvironments &&
                  apkEnvironments[fileName].root.state === "ACTIVE" ? (
                    <React.Fragment>
                      <Row>
                        <Col>
                          <InputFile
                            accept={MOBILE_EXTENSIONS.join(",")}
                            id={"file"}
                            label={t(
                              "searchFindings.tabResources.files.form.button",
                            )}
                            name={"file"}
                          />
                        </Col>
                      </Row>
                      {isButtonEnabled ? (
                        <Container
                          display={"flex"}
                          flexDirection={"column"}
                          gap={1}
                        >
                          <Alert variant={"info"}>
                            {t("searchFindings.tabResources.uploadingProgress")}
                          </Alert>
                          <ProgressBar
                            minWidth={246}
                            percentage={progressPercentage}
                            showPercentage={true}
                          />
                          {isUpdatingFileInEnvironment ? (
                            <Alert variant={"info"}>
                              {t("groupAlerts.updatingFileInEnvironment")}
                            </Alert>
                          ) : undefined}
                        </Container>
                      ) : undefined}
                      {values.file === undefined ||
                      isEmpty(values.file) ? undefined : (
                        <Button
                          disabled={
                            isUndefined(values.file) || isEmpty(values.file)
                          }
                          justify={"center"}
                          maxWidth={"100px"}
                          type={"submit"}
                        >
                          {t("searchFindings.tabResources.files.form.button")}
                        </Button>
                      )}
                    </React.Fragment>
                  ) : undefined}
                </Can>
              </Container>
            </Container>
          </Form>
        )}
      </FormModal>
      <ConfirmDialog />
      <ConfirmDialogReplace />
    </React.Fragment>
  );
};

export { Modal };
