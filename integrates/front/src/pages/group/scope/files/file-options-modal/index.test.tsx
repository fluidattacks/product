import { PureAbility } from "@casl/ability";
import { screen, waitFor } from "@testing-library/react";
import { userEvent } from "@testing-library/user-event";
import { useEffect } from "react";

import type { IFile } from "../types";
import { authzPermissionsContext } from "context/authz/config";
import { useModal } from "hooks/use-modal";
import { render } from "mocks/index";
import { FileOptionsModal } from "pages/group/scope/files/file-options-modal";
import { msgError } from "utils/notifications";

jest.mock("utils/notifications", (): Record<string, unknown> => {
  const mockedNotifications: Record<string, () => Record<string, unknown>> =
    jest.requireActual("utils/notifications");
  jest.spyOn(mockedNotifications, "msgError").mockImplementation();

  return mockedNotifications;
});

const functionMock = (): void => undefined;

const ModalComponent = ({
  canRemove,
  filesDataset,
}: Readonly<{
  canRemove: boolean;
  filesDataset: IFile[];
}>): JSX.Element => {
  const modal = useModal("file-test");

  useEffect((): void => {
    modal.open();
  }, [modal]);

  return (
    <FileOptionsModal
      apkEnvironments={{
        "test.zip": {
          cloudName: null,
          countSecrets: 0,
          createdAt: "2024-07-30T16:36:32.226096+00:00",
          createdBy: "slizcano@fluidattacks.com",
          id: "b53ad821dec7f86dbf74aebd6a2fc54fddbdd4fd",
          include: true,
          root: {
            id: "4039d098-ffc5-4984-8ed3-eb17bca98e19",
            state: "ACTIVE",
            url: "https://gitlab.com/fluidattacks/universe.git",
          },
          rootId: "4039d098-ffc5-4984-8ed3-eb17bca98e19",
          url: "test.zip",
          urlType: "APK",
          useEgress: false,
          useVpn: false,
          useZtna: false,
        },
      }}
      canRemove={canRemove}
      fileName={"test.zip"}
      filesDataSet={filesDataset}
      groupName={"TEST"}
      modalRef={modal}
      onClose={functionMock}
      onDelete={functionMock}
      onDownload={functionMock}
      organizationName={"test"}
    />
  );
};

describe("add resources modal", (): void => {
  it("should render", async (): Promise<void> => {
    expect.hasAssertions();

    const mockedPermissions = new PureAbility<string>([
      {
        action:
          "integrates_api_mutations_update_git_root_environment_file_mutate",
      },
    ]);
    const file: File = new File([""], "test_1_1.zip", {
      type: "application/zip",
    });

    render(
      <authzPermissionsContext.Provider value={mockedPermissions}>
        <ModalComponent canRemove={true} filesDataset={[]} />
      </authzPermissionsContext.Provider>,
    );

    expect(
      screen.queryByText("searchFindings.tabResources.modalOptionsTitle"),
    ).toBeInTheDocument();
    expect(
      screen.queryByText("searchFindings.tabResources.files.confirm.title"),
    ).not.toBeInTheDocument();

    await userEvent.click(
      screen.getByRole("button", {
        name: "searchFindings.tabResources.removeRepository",
      }),
    );
    await waitFor((): void => {
      expect(
        screen.queryByText("searchFindings.tabResources.files.confirm.title"),
      ).toBeInTheDocument();
    });

    expect(screen.queryByTestId("file")).toBeInTheDocument();
    expect(
      screen.queryByRole("button", {
        name: "searchFindings.tabResources.files.form.button",
      }),
    ).not.toBeInTheDocument();

    await userEvent.upload(screen.getByTestId("file"), file);

    await waitFor((): void => {
      expect(
        screen.queryByRole("button", {
          name: "searchFindings.tabResources.files.form.button",
        }),
      ).toBeInTheDocument();
    });

    expect(
      screen.queryByText("searchFindings.tabResources.files.form.warning"),
    ).not.toBeInTheDocument();

    await userEvent.click(
      screen.getByRole("button", {
        name: "searchFindings.tabResources.files.form.button",
      }),
    );
    await waitFor((): void => {
      expect(
        screen.queryByText("searchFindings.tabResources.files.form.warning"),
      ).toBeInTheDocument();
    });
  });

  it("should render repeated error", async (): Promise<void> => {
    expect.hasAssertions();

    const mockedPermissions = new PureAbility<string>([
      {
        action:
          "integrates_api_mutations_update_git_root_environment_file_mutate",
      },
    ]);
    const file: File = new File([""], "test_1_1.zip", {
      type: "application/zip",
    });

    render(
      <authzPermissionsContext.Provider value={mockedPermissions}>
        <ModalComponent
          canRemove={true}
          filesDataset={[
            {
              description: "",
              fileName: "test_1_1.zip",
            },
          ]}
        />
      </authzPermissionsContext.Provider>,
    );

    expect(
      screen.queryByText("searchFindings.tabResources.modalOptionsTitle"),
    ).toBeInTheDocument();
    expect(
      screen.queryByText("searchFindings.tabResources.files.confirm.title"),
    ).not.toBeInTheDocument();

    await userEvent.click(
      screen.getByRole("button", {
        name: "searchFindings.tabResources.removeRepository",
      }),
    );
    await waitFor((): void => {
      expect(
        screen.queryByText("searchFindings.tabResources.files.confirm.title"),
      ).toBeInTheDocument();
    });

    expect(screen.queryByTestId("file")).toBeInTheDocument();
    expect(
      screen.queryByRole("button", {
        name: "searchFindings.tabResources.files.form.button",
      }),
    ).not.toBeInTheDocument();

    await userEvent.upload(screen.getByTestId("file"), file);

    await waitFor((): void => {
      expect(
        screen.queryByRole("button", {
          name: "searchFindings.tabResources.files.form.button",
        }),
      ).toBeInTheDocument();
    });

    expect(
      screen.queryByText("searchFindings.tabResources.files.form.warning"),
    ).not.toBeInTheDocument();

    await userEvent.click(
      screen.getByRole("button", {
        name: "searchFindings.tabResources.files.form.button",
      }),
    );

    await waitFor((): void => {
      expect(msgError).toHaveBeenCalledWith(
        "searchFindings.tabResources.repeatedItem",
      );
    });

    expect(
      screen.queryByText("searchFindings.tabResources.files.form.warning"),
    ).not.toBeInTheDocument();
  });

  it("should render without remove option", async (): Promise<void> => {
    expect.hasAssertions();

    render(<ModalComponent canRemove={false} filesDataset={[]} />);

    expect(
      screen.queryByText("searchFindings.tabResources.modalOptionsTitle"),
    ).toBeInTheDocument();

    await waitFor((): void => {
      expect(
        screen.queryByText("searchFindings.tabResources.removeRepository"),
      ).not.toBeInTheDocument();
    });

    expect(
      screen.queryByText("searchFindings.tabResources.download"),
    ).toBeInTheDocument();
    expect(screen.queryByTestId("file")).not.toBeInTheDocument();
  });
});
