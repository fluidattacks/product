interface IFile {
  description: string;
  fileName: string;
  uploadDate?: string;
}

interface IAddFiles {
  signPostUrl: {
    url: {
      url: string;
      fields: {
        algorithm: string;
        credential: string;
        date: string;
        key: string;
        policy: string;
        securitytoken: string;
        signature: string;
      };
    };
  };
}

interface IAddFilesToDbResults {
  addFilesToDb: {
    success: boolean;
  };
}

interface IFilesProps {
  groupName: string;
  organizationName: string;
}

export type { IAddFiles, IAddFilesToDbResults, IFile, IFilesProps };
