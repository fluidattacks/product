import { screen, waitFor } from "@testing-library/react";
import { userEvent } from "@testing-library/user-event";
import { Route, Routes } from "react-router-dom";
import {
  type AnyObject,
  type InferType,
  type Schema,
  ValidationError,
} from "yup";

import { validationSchema } from "./validations";

import { CustomThemeProvider } from "components/colors";
import { render } from "mocks";
import { Navbar } from "pages/dashboard/navbar";
import { Unsubscribe } from "pages/group/scope/unsubscribe";
import { msgError, msgSuccess } from "utils/notifications";

jest.mock("utils/notifications", (): Record<string, unknown> => {
  const mockedNotifications: Record<string, () => Record<string, unknown>> =
    jest.requireActual("utils/notifications");

  jest.spyOn(mockedNotifications, "msgError").mockImplementation();
  jest.spyOn(mockedNotifications, "msgSuccess").mockImplementation();

  return mockedNotifications;
});

describe("unsubscribe from group", (): void => {
  const testValidation = (
    groupName: string,
    obj: AnyObject,
  ): InferType<Schema> => {
    return validationSchema(groupName).validateSync(obj);
  };

  it("should unsubscribe from a group", async (): Promise<void> => {
    expect.hasAssertions();

    jest.spyOn(console, "warn").mockImplementation();

    render(
      <Routes>
        <Route
          element={
            <CustomThemeProvider>
              <Navbar initialOrganization={"okada"} />
              <Unsubscribe />
            </CustomThemeProvider>
          }
          path={"/:groupName"}
        />
      </Routes>,
      {
        memoryRouter: {
          initialEntries: ["/TEST"],
        },
      },
    );
    const btnConfirm = "Confirm";

    expect(
      screen.queryByText("searchFindings.servicesTable.unsubscribe.button"),
    ).toBeInTheDocument();

    await userEvent.click(
      screen.getByText("searchFindings.servicesTable.unsubscribe.button"),
    );
    await waitFor((): void => {
      expect(
        screen.getByRole("textbox", { name: "confirmation" }),
      ).toBeInTheDocument();
    });

    expect(screen.getByText(btnConfirm)).toBeDisabled();

    await userEvent.type(
      screen.getByRole("textbox", { name: "confirmation" }),
      "test",
    );
    await waitFor((): void => {
      expect(screen.getByText(btnConfirm)).not.toBeDisabled();
    });
    await userEvent.click(screen.getByText(btnConfirm));
    await waitFor((): void => {
      expect(msgSuccess).toHaveBeenCalledWith(
        "searchFindings.servicesTable.unsubscribe.success",
        "searchFindings.servicesTable.unsubscribe.successTitle",
      );
    });
  });

  it("shouldn't unsubscribe from a group", async (): Promise<void> => {
    expect.hasAssertions();

    render(
      <Routes>
        <Route
          element={
            <CustomThemeProvider>
              <Unsubscribe />
            </CustomThemeProvider>
          }
          path={"/:groupName"}
        />
      </Routes>,
      {
        memoryRouter: {
          initialEntries: ["/test"],
        },
      },
    );
    const btnConfirm = "Confirm";

    expect(
      screen.queryByText("searchFindings.servicesTable.unsubscribe.button"),
    ).toBeInTheDocument();

    await userEvent.click(
      screen.getByText("searchFindings.servicesTable.unsubscribe.button"),
    );
    await waitFor((): void => {
      expect(
        screen.getByRole("textbox", { name: "confirmation" }),
      ).toBeInTheDocument();
    });

    expect(screen.getByText(btnConfirm)).toBeDisabled();

    await userEvent.type(
      screen.getByRole("textbox", { name: "confirmation" }),
      "test",
    );
    await userEvent.click(screen.getByText(btnConfirm));
    await waitFor((): void => {
      expect(msgError).toHaveBeenCalledWith("groupAlerts.errorTextsad");
    });
  });

  it("should display required field errors of validation schema", (): void => {
    expect.hasAssertions();

    const groupName = "unittesting";
    const errorString = "Required field";

    const testRequiredField = (): void => {
      testValidation(groupName, {});
    };

    expect(testRequiredField).toThrow(new ValidationError(errorString));
  });

  it("should display confirmation field validation errors", (): void => {
    expect.hasAssertions();

    const groupName = "unittesting";

    const testMatchGroupName = (): void => {
      testValidation(groupName, {
        confirmation: "test",
      });
    };

    expect(testMatchGroupName).toThrow(
      new ValidationError(`Expected: ${groupName}`),
    );
  });
});
