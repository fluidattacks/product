import { Alert, Form, InnerForm, Input, Modal } from "@fluidattacks/design";
import * as React from "react";
import { useTranslation } from "react-i18next";

import type { IUnsubscribeModalProps } from "../types";
import { validationSchema } from "../validations";

const UnsubscribeModal: React.FC<IUnsubscribeModalProps> = ({
  groupName,
  modalRef,
  onSubmit,
}): JSX.Element => {
  const { t } = useTranslation();
  const { close } = modalRef;

  return (
    <React.StrictMode>
      <Modal
        id={"unsubscribeFromGroup"}
        modalRef={modalRef}
        size={"md"}
        title={t("searchFindings.servicesTable.unsubscribe.title")}
      >
        <Form
          cancelButton={{ onClick: close }}
          defaultValues={{ confirmation: "" }}
          onSubmit={onSubmit}
          yupSchema={validationSchema(groupName.toLowerCase())}
        >
          <InnerForm>
            {({ formState, getFieldState, register }): JSX.Element => {
              const { invalid, isTouched } = getFieldState("confirmation");

              return (
                <React.Fragment>
                  <Alert>
                    {t("searchFindings.servicesTable.unsubscribe.warningBody")}
                  </Alert>
                  <Input
                    error={formState.errors.confirmation?.message?.toString()}
                    isTouched={isTouched}
                    isValid={!invalid}
                    label={t(
                      "searchFindings.servicesTable.unsubscribe.typeGroupName",
                    )}
                    name={"confirmation"}
                    placeholder={groupName.toLowerCase()}
                    register={register}
                  />
                </React.Fragment>
              );
            }}
          </InnerForm>
        </Form>
      </Modal>
    </React.StrictMode>
  );
};

export type { IUnsubscribeModalProps };
export { UnsubscribeModal };
