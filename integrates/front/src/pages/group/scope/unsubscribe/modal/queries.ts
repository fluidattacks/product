import { graphql } from "gql";

const UNSUBSCRIBE_FROM_GROUP_MUTATION = graphql(`
  mutation UnsubscribeFromGroupMutation($groupName: String!) {
    unsubscribeFromGroup(groupName: $groupName) {
      success
    }
  }
`);

export { UNSUBSCRIBE_FROM_GROUP_MUTATION };
