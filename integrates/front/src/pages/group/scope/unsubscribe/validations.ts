import type { InferType, ObjectSchema, Schema } from "yup";
import { object, string } from "yup";

import { translate } from "utils/translations/translate";

const validationSchema = (groupName: string): ObjectSchema<InferType<Schema>> =>
  object().shape({
    confirmation: string()
      .required(translate.t("validations.required"))
      .matches(new RegExp(`^${groupName}$`, "u"), {
        message: translate.t(
          "searchFindings.servicesTable.errors.expectedGroupName",
          {
            groupName,
          },
        ),
      }),
  });

export { validationSchema };
