import { createContext } from "react";

import type { IVulnerabilitiesContext } from "./types";

const vulnerabilitiesContext: React.Context<IVulnerabilitiesContext> =
  createContext({
    openVulnerabilities: 0,
  });

export { vulnerabilitiesContext };
