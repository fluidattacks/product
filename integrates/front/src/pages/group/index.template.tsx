import { useQuery } from "@apollo/client";
import type { ITabProps } from "@fluidattacks/design/dist/components/tabs/types";
import { capitalize } from "lodash";
import { useContext, useMemo, useState } from "react";
import * as React from "react";
import { useTranslation } from "react-i18next";
import { useParams } from "react-router-dom";

import { vulnerabilitiesContext } from "./context";
import { FrozenGroupModal } from "./frozen-group-modal";
import { GroupSelector } from "./group-selector";
import { useGroupUrl } from "./hooks";
import { GET_GROUP_DATA, GET_GROUP_EVENT_STATUS } from "./queries";
import { useTotalsQuery } from "./supply-chain/hooks";
import type { IVulnerabilitiesContext } from "./types";

import { SectionHeader } from "components/section-header";
import { authContext } from "context/auth";
import { ManagedType } from "gql/graphql";
import { useAuthz } from "hooks/use-authz";
import { useModal } from "hooks/use-modal";
import { useTabTracking } from "hooks/use-tab-tracking";
import { Logger } from "utils/logger";
import { msgError } from "utils/notifications";

const GroupTemplate: React.FC<Readonly<{ children: JSX.Element }>> = ({
  children,
}): JSX.Element => {
  const { open, isOpen, close } = useModal("group-selector");

  const { url: groupUrl } = useGroupUrl();
  const { organizationName, groupName } = useParams() as {
    organizationName: string;
    groupName: string;
  };
  const { userEmail } = useContext(authContext);
  const { t } = useTranslation();

  const [openVulnerabilities, setOpenVulnerabilities] = useState<number>(0);
  const [denyAccess, setDenyAccess] = useState(false);
  const { dockerImagesTotal, packagesTotal, rootsTotal } =
    useTotalsQuery(groupName);

  const { can, canResolve, hasAttribute } = useAuthz();
  const canGetToeLines = canResolve("group_toe_lines_connection");
  const canGetToeInputs = canResolve("group_toe_inputs");
  const canRenderAuthors = canResolve("group_billing");
  const canRenderInputs = canResolve("group_toe_inputs");
  const canRenderPorts = canResolve("group_toe_ports");
  const canRenderMembers = can(
    "integrates_api_resolvers_query_stakeholder__resolve_for_group",
  );
  const hasWhite = hasAttribute("has_service_white");

  useTabTracking("Group");
  const { data } = useQuery(GET_GROUP_DATA, {
    fetchPolicy: "cache-first",
    onCompleted: ({ group }): void => {
      setDenyAccess(
        group.managed === ManagedType.UnderReview &&
          !userEmail.endsWith("@fluidattacks.com"),
      );
    },
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((groupError): void => {
        msgError(t("groupAlerts.errorTextsad"));
        Logger.warning("An error occurred group data", groupError);
      });
    },
    variables: { groupName, organizationName },
  });

  const { data: eventData } = useQuery(GET_GROUP_EVENT_STATUS, {
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((eventError): void => {
        Logger.warning("An error occurred loading event bar data", eventError);
        msgError(t("groupAlerts.errorTextsad"));
      });
    },
    variables: { groupName },
  });

  const openEvents = useMemo(
    (): number =>
      (eventData?.group.events ?? []).filter(
        (event): boolean => event.eventStatus === "CREATED",
      ).length,
    [eventData],
  );
  const requestEvents = useMemo(
    (): number =>
      (eventData?.group.events ?? []).filter(
        (event): boolean => event.eventStatus === "VERIFICATION_REQUESTED",
      ).length,
    [eventData],
  );

  const vulnValue = useMemo(
    (): IVulnerabilitiesContext => ({
      openVulnerabilities,
      setOpenVulnerabilities,
    }),
    [openVulnerabilities],
  );

  const tabsItems: ITabProps[] = [
    {
      id: "findingsTab",
      label: t("group.tabs.findings.text"),
      link: `${groupUrl}/vulns`,
    },
    {
      id: "analyticsTab",
      label: t("group.tabs.analytics.text"),
      link: `${groupUrl}/analytics`,
      tooltip: t("group.tabs.indicators.tooltip"),
    },
    {
      id: "forcesTab",
      label: t("group.tabs.forces.text"),
      link: `${groupUrl}/devsecops`,
      tooltip: t("group.tabs.forces.tooltip"),
    },
    {
      id: "eventsTab",
      label: t("group.tabs.events.text"),
      link: `${groupUrl}/events`,
      notificationSign: {
        left: "100%",
        numberIndicator: openEvents === 0 ? requestEvents : openEvents,
        show: requestEvents > 0 || openEvents > 0,
        top: "15%",
        variant: openEvents > 0 ? "error" : "warning",
      },
      tooltip: t("group.tabs.events.tooltip"),
    },
    ...(canRenderMembers
      ? [
          {
            id: "usersTab",
            label: t("group.tabs.users.text"),
            link: `${groupUrl}/members`,
            tooltip: t("group.tabs.users.tooltip"),
          },
        ]
      : []),
    ...(canRenderAuthors && hasWhite
      ? [
          {
            id: "authorsTab",
            label: t("group.tabs.authors.text"),
            link: `${groupUrl}/authors`,
            tooltip: t("group.tabs.authors.tooltip"),
          },
        ]
      : []),
    ...(canGetToeInputs && canGetToeLines
      ? [
          {
            id: "toeTab",
            label: t("group.tabs.toe.text"),
            link: `${groupUrl}/surface`,
            tooltip: t("group.tabs.toe.tooltip"),
          },
        ]
      : []),
    {
      id: "resourcesTab",
      label: t("group.tabs.resources.text"),
      link: `${groupUrl}/scope`,
      tooltip: t("group.tabs.resources.tooltip"),
    },
  ];

  if (data === undefined) {
    return <div />;
  }

  return (
    <vulnerabilitiesContext.Provider value={vulnValue}>
      <SectionHeader
        goBackTo={`/orgs/${organizationName}/groups`}
        header={capitalize(groupName)}
        nestedTabs={{
          [`${groupUrl}/surface`]: [
            ...(canGetToeLines
              ? [
                  {
                    id: "toe-lines-tab",
                    label: t("group.toe.tabs.lines.text"),
                    link: `${groupUrl}/surface/lines`,
                    tooltip: t("group.toe.tabs.lines.tooltip"),
                  },
                ]
              : []),
            ...(canRenderInputs
              ? [
                  {
                    id: "toe-inputs-tab",
                    label: t("group.toe.tabs.inputs.text"),
                    link: `${groupUrl}/surface/inputs`,
                    tooltip: t("group.toe.tabs.inputs.tooltip"),
                  },
                ]
              : []),
            {
              id: "packagesTab",
              label: "Packages",
              link: `${groupUrl}/surface/packages`,
              nestedTabs: {
                [`${groupUrl}/surface/packages`]: [
                  {
                    end: true,
                    id: "all-packages",
                    label: "All packages",
                    link: `${groupUrl}/surface/packages/all-packages`,
                    tag: packagesTotal,
                  },
                  {
                    id: "roots",
                    label: "Roots",
                    link: `${groupUrl}/surface/packages/roots`,
                    tag: rootsTotal,
                  },
                  {
                    id: "docker-images",
                    label: "Docker images",
                    link: `${groupUrl}/surface/packages/docker-images`,
                    tag: dockerImagesTotal,
                  },
                ],
              },
            },
            ...(canRenderPorts
              ? [
                  {
                    id: "toe-ports-tab",
                    label: t("group.toe.tabs.ports.text"),
                    link: `${groupUrl}/surface/ports`,
                    tooltip: t("group.toe.tabs.ports.tooltip"),
                  },
                ]
              : []),
            {
              id: "toe-languages-tab",
              label: t("group.toe.tabs.languages.text"),
              link: `${groupUrl}/surface/languages`,
              tooltip: t("group.toe.tabs.languages.text"),
            },
          ],
          [`${groupUrl}/internal/surface`]: [
            ...(canGetToeLines
              ? [
                  {
                    id: "toe-lines-tab",
                    label: t("group.toe.tabs.lines.text"),
                    link: `${groupUrl}/internal/surface/lines`,
                    tooltip: t("group.toe.tabs.lines.tooltip"),
                  },
                ]
              : []),
            ...(canGetToeInputs
              ? [
                  {
                    id: "toe-inputs-tab",
                    label: t("group.toe.tabs.inputs.text"),
                    link: `${groupUrl}/internal/surface/inputs`,
                    tooltip: t("group.toe.tabs.inputs.tooltip"),
                  },
                ]
              : []),
            {
              id: "packagesTab",
              label: "Packages",
              link: `${groupUrl}/internal/surface/packages`,
              nestedTabs: {
                [`${groupUrl}/internal/surface/packages`]: [
                  {
                    end: true,
                    id: "all-packages",
                    label: "All packages",
                    link: `${groupUrl}/internal/surface/packages/all-packages`,
                    tag: packagesTotal,
                  },
                  {
                    id: "roots",
                    label: "Roots",
                    link: `${groupUrl}/internal/surface/packages/roots`,
                    tag: rootsTotal,
                  },
                  {
                    id: "docker-images",
                    label: "Docker images",
                    link: `${groupUrl}/internal/surface/packages/docker-images`,
                    tag: dockerImagesTotal,
                  },
                ],
              },
            },
            ...(canRenderPorts
              ? [
                  {
                    id: "toe-ports-tab",
                    label: t("group.toe.tabs.ports.text"),
                    link: `${groupUrl}/internal/surface/ports`,
                    tooltip: t("group.toe.tabs.ports.tooltip"),
                  },
                ]
              : []),
            {
              id: "toe-languages-tab",
              label: t("group.toe.tabs.languages.text"),
              link: `${groupUrl}/internal/surface/languages`,
              tooltip: t("group.toe.tabs.languages.text"),
            },
          ],
        }}
        onClickSelector={open}
        tabs={tabsItems}
      />
      {children}
      <FrozenGroupModal isOpen={Boolean(denyAccess)} />
      <GroupSelector close={close} isOpen={isOpen} />
    </vulnerabilitiesContext.Provider>
  );
};

export { GroupTemplate };
