import type { IEventData } from "../types";

interface IActionsButtonsProps {
  openAddModal: () => void;
  openUpdateAffectedModal: () => void;
  closeOpenMode: () => void;
  data: IEventData[];
  filteredData: IEventData[];
  isOpenMode: boolean;
  isThereEventsThatAffectsReattacks: boolean;
  selectedUnsolvedEvents: IEventData[];
  onClickRequestEventVerification: () => void;
}

export type { IActionsButtonsProps };
