import { castEventType } from "utils/format-helpers";
import { translate } from "utils/translations/translate";

const selectOptionType = [
  {
    label: translate.t(castEventType("AUTHORIZATION_SPECIAL_ATTACK")),
    value: translate.t(castEventType("AUTHORIZATION_SPECIAL_ATTACK")),
  },
  {
    label: translate.t(castEventType("CLIENT_CANCELS_PROJECT_MILESTONE")),
    value: translate.t(castEventType("CLIENT_CANCELS_PROJECT_MILESTONE")),
  },
  {
    label: translate.t(castEventType("CLIENT_EXPLICITLY_SUSPENDS_PROJECT")),
    value: translate.t(castEventType("CLIENT_EXPLICITLY_SUSPENDS_PROJECT")),
  },
  {
    label: translate.t(castEventType("CLONING_ISSUES")),
    value: translate.t(castEventType("CLONING_ISSUES")),
  },
  {
    label: translate.t(castEventType("CREDENTIAL_ISSUES")),
    value: translate.t(castEventType("CREDENTIAL_ISSUES")),
  },
  {
    label: translate.t(castEventType("DATA_UPDATE_REQUIRED")),
    value: translate.t(castEventType("DATA_UPDATE_REQUIRED")),
  },
  {
    label: translate.t(castEventType("ENVIRONMENT_ISSUES")),
    value: translate.t(castEventType("ENVIRONMENT_ISSUES")),
  },
  {
    label: translate.t(castEventType("INSTALLER_ISSUES")),
    value: translate.t(castEventType("INSTALLER_ISSUES")),
  },
  {
    label: translate.t(castEventType("MISSING_SUPPLIES")),
    value: translate.t(castEventType("MISSING_SUPPLIES")),
  },
  {
    label: translate.t(castEventType("NETWORK_ACCESS_ISSUES")),
    value: translate.t(castEventType("NETWORK_ACCESS_ISSUES")),
  },
  {
    label: translate.t(castEventType("OTHER")),
    value: translate.t(castEventType("OTHER")),
  },
  {
    label: translate.t(castEventType("REMOTE_ACCESS_ISSUES")),
    value: translate.t(castEventType("REMOTE_ACCESS_ISSUES")),
  },
  {
    label: translate.t(castEventType("TOE_DIFFERS_APPROVED")),
    value: translate.t(castEventType("TOE_DIFFERS_APPROVED")),
  },
  {
    label: translate.t(castEventType("VPN_ISSUES")),
    value: translate.t(castEventType("VPN_ISSUES")),
  },
];

export { selectOptionType };
