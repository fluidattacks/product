/* eslint-disable @typescript-eslint/no-invalid-void-type */
import { useQuery } from "@apollo/client";
import { Col, Row, Text, Toggle } from "@fluidattacks/design";
import type { FormikErrors } from "formik";
import { useCallback, useContext, useMemo, useState } from "react";
import * as React from "react";
import { useTranslation } from "react-i18next";

import type {
  IAddModalProps,
  IFormValues,
  IListItemProps,
  IRootEnvironmentUrlsData,
} from "./types";
import { eventTypes } from "./utils";
import { validations } from "./validations";

import { AffectedReattackAccordion } from "../affected-reattack-accordion";
import { GET_VERIFIED_FINDING_INFO } from "../affected-reattack-accordion/queries";
import type { IFinding } from "../affected-reattack-accordion/types";
import { InnerForm } from "components/form";
import { FormModal } from "components/form-modal";
import {
  Input,
  InputDateTime,
  InputFile,
  Select,
  TextArea,
} from "components/input";
import { authzGroupContext } from "context/authz/config";
import { ResourceState } from "gql/graphql";
import {
  GET_ROOTS,
  GET_ROOT_ENVIRONMENT_URLS,
} from "pages/group/scope/queries";
import { castEventType } from "utils/format-helpers";
import { Logger } from "utils/logger";

const MAX_EVENT_DETAILS_LENGTH = 1000;

const AddModal = ({
  organizationName,
  groupName,
  modalRef,
  onSubmit,
}: IAddModalProps): JSX.Element => {
  const { t } = useTranslation();
  const attributes = useContext(authzGroupContext);
  const { close, isOpen } = modalRef;
  const [rootId, setRootId] = useState("");

  const { data: rootEnvironmentUrls } = useQuery<
    IRootEnvironmentUrlsData | undefined
  >(GET_ROOT_ENVIRONMENT_URLS, {
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error): void => {
        Logger.error("Couldn't load roots", error);
      });
    },
    skip: !rootId || !isOpen,
    variables: { groupName, rootId },
  });

  const getSuggestions = (): IListItemProps[] => {
    if (!rootEnvironmentUrls?.root) {
      return [];
    }

    const suggestions = rootEnvironmentUrls.root.gitEnvironmentUrls.map(
      (urlObj): IListItemProps => ({
        label: urlObj.url,
        value: urlObj.url,
      }),
    );

    return suggestions.length > 0
      ? [
          { label: "Select an option", value: "No related environment" },
          ...suggestions,
        ]
      : suggestions;
  };

  const { data } = useQuery(GET_ROOTS, {
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error): void => {
        Logger.error("Couldn't load roots", error);
      });
    },
    skip: !isOpen,
    variables: { groupName },
  });
  const { data: findingsData } = useQuery(GET_VERIFIED_FINDING_INFO, {
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error): void => {
        Logger.error("Couldn't load reattack vulns", error);
      });
    },
    skip: !isOpen,
    variables: { groupName },
  });

  const findings =
    findingsData === undefined ? [] : (findingsData.group.findings ?? []);
  const canOnHold = attributes.can("can_report_vulnerabilities");
  const hasReattacks = findings.some(
    (finding): boolean => finding !== null && !finding.verified,
  );
  const roots = useMemo(
    // eslint-disable-next-line @typescript-eslint/explicit-function-return-type
    () =>
      data === undefined
        ? []
        : (data.group.roots?.filter(
            (root): boolean => root.state === ResourceState.Active,
          ) ?? []),
    [data],
  );
  const nicknames = roots.map((root): string => root.nickname);
  const selectOptions = [{ header: "", value: "" }].concat(
    eventTypes.map((type): { header: string; value: string } => ({
      header: t(castEventType(type)),
      value: type,
    })),
  );

  function handleAffectsReattacksBtnChange(
    setFieldValue: (
      field: string,
      value: boolean,
      shouldValidate?: boolean,
    ) => Promise<FormikErrors<IFormValues> | void>,
  ): (event: React.ChangeEvent<HTMLInputElement>) => void {
    return (event: React.ChangeEvent<HTMLInputElement>): void => {
      const switchValue = event.target.checked;
      void setFieldValue("affectsReattacks", switchValue);
    };
  }
  const handleSubmit = useCallback(
    async (values: IFormValues): Promise<void> => {
      return onSubmit({
        ...values,
        rootId: values.rootNickname
          ? roots[nicknames.indexOf(values.rootNickname)].id
          : "",
      });
    },
    [nicknames, onSubmit, roots],
  );

  function handleRootNicknameChange(
    setFieldValue: (
      field: string,
      value: string,
    ) => Promise<FormikErrors<IFormValues> | void>,
  ): (event: React.ChangeEvent<HTMLInputElement>) => void {
    return (event: React.ChangeEvent<HTMLInputElement>): void => {
      const { value } = event.target;
      if (nicknames.includes(value)) {
        const currentRootId = roots[nicknames.indexOf(value)].id;
        setRootId(currentRootId);
        void setFieldValue("rootNickname", value);
      } else {
        setRootId("");
      }
    };
  }

  return (
    <FormModal
      initialValues={{
        affectedReattacks: [],
        affectsReattacks: false,
        detail: "",
        environmentUrl: "",
        eventDate: "",
        eventType: "",
        files: undefined,
        images: undefined,
        rootId: "",
        rootNickname: "",
      }}
      modalRef={modalRef}
      name={"newEvent"}
      onSubmit={handleSubmit}
      size={"md"}
      title={t("group.events.new")}
      validationSchema={validations(groupName, nicknames, organizationName)}
    >
      {({ dirty, isSubmitting, values, setFieldValue }): JSX.Element => {
        const allSuggestions = nicknames.includes(values.rootNickname)
          ? getSuggestions()
          : [];

        return (
          <InnerForm onCancel={close} submitDisabled={!dirty || isSubmitting}>
            <Row>
              <Col lg={50} md={50} sm={100}>
                <InputDateTime
                  label={t("group.events.form.date")}
                  name={"eventDate"}
                />
              </Col>
              <Col lg={50} md={50} sm={100}>
                <Select
                  items={selectOptions}
                  label={t("group.events.form.type")}
                  name={"eventType"}
                />
              </Col>
            </Row>
            <Row>
              <Input
                label={t("group.events.form.root")}
                name={"rootNickname"}
                onChange={handleRootNicknameChange(setFieldValue)}
                placeholder={t("group.events.form.rootPlaceholder")}
                suggestions={nicknames}
              />
            </Row>
            {values.rootNickname && allSuggestions.length > 0 ? (
              <Row>
                <Select
                  items={allSuggestions}
                  label={t("group.events.form.environment")}
                  name={"environmentUrl"}
                  placeholder={t("group.events.form.environmentPlaceholder")}
                />
              </Row>
            ) : undefined}
            <Row>
              <TextArea
                label={t("group.events.form.details")}
                maxLength={MAX_EVENT_DETAILS_LENGTH}
                name={"detail"}
              />
            </Row>
            <Row>
              <Col>
                <InputFile
                  accept={"image/png,video/webm"}
                  id={"images"}
                  label={t("group.events.form.evidence")}
                  multiple={true}
                  name={"images"}
                />
              </Col>
              <Col>
                <InputFile
                  accept={"application/pdf,application/zip,text/csv,text/plain"}
                  id={"files"}
                  label={t("group.events.form.evidenceFile")}
                  name={"files"}
                />
              </Col>
            </Row>
            {hasReattacks && canOnHold ? (
              <React.Fragment>
                <Text size={"sm"}>
                  {t("group.events.form.affectedReattacks.sectionTitle")}
                </Text>
                <Toggle
                  defaultChecked={values.affectsReattacks}
                  name={"affectsReattacks"}
                  onChange={handleAffectsReattacksBtnChange(setFieldValue)}
                />
                {values.affectsReattacks ? (
                  <React.Fragment>
                    <Text size={"sm"}>
                      {t("group.events.form.affectedReattacks.selection")}
                    </Text>
                    <Row>
                      <AffectedReattackAccordion
                        findings={findings as IFinding[]}
                      />
                    </Row>
                  </React.Fragment>
                ) : undefined}
              </React.Fragment>
            ) : undefined}
          </InnerForm>
        );
      }}
    </FormModal>
  );
};

export type { IFormValues };
export { AddModal };
