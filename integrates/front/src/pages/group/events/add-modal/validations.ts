import type {
  ArraySchema,
  InferType,
  ObjectSchema,
  Schema,
  StringSchema,
} from "yup";
import { array, mixed, object, string } from "yup";

import { translate } from "utils/translations/translate";

const MAX_FILE_SIZE = 20;
const MAX_AMOUNT_OF_FILES = 6;
const MAX_WEEKS_CREATED_DATE = 104;

export const validations = (
  groupName: string,
  nicknames: string[],
  organizationName: string,
): ObjectSchema<InferType<Schema>> =>
  object().shape({
    affectedReattacks: array().when(
      "affectsReattacks",
      (
        [affectsReattacks]: boolean[],
        schema: ArraySchema<boolean[] | undefined, unknown>,
      ): Schema => {
        return affectsReattacks
          ? schema.min(1, translate.t("validations.someRequired"))
          : schema.notRequired();
      },
    ),
    detail: string()
      .required(translate.t("validations.required"))
      .isValidTextBeginning()
      .isValidTextField(),
    eventDate: mixed()
      .required(translate.t("validations.required"))
      .isValidDate("datetime")
      .isValidDate("greaterDate")
      .isValidDate("greaterThanDate", MAX_WEEKS_CREATED_DATE),
    eventType: string().required(translate.t("validations.required")),
    files: mixed<FileList>()
      .isValidFileSize(undefined, MAX_FILE_SIZE)
      .isValidFileName(groupName, organizationName)
      .isValidFileType(["pdf", "zip", "csv", "txt"]),
    images: mixed<FileList>()
      .isValidFileSize(undefined, MAX_FILE_SIZE)
      .isValidMaxNumberFiles(MAX_AMOUNT_OF_FILES)
      .isValidFileName(groupName, organizationName)
      .isValidFileType(
        ["png", "webm"],
        translate.t("group.events.form.wrongImageType"),
      ),
    rootNickname: string()
      .oneOf(nicknames, translate.t("validations.oneOf"))
      .when(
        "eventType",
        ([eventType]: string[], schema: StringSchema): Schema => {
          return eventType === "MISSING_SUPPLIES"
            ? schema.notRequired()
            : schema.required();
        },
      ),
  });
