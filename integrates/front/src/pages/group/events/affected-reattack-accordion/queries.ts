import { graphql } from "gql";

const GET_VERIFIED_FINDING_INFO = graphql(`
  query GetVerifiedFindingInfo($groupName: String!) {
    group(groupName: $groupName) {
      name
      findings {
        id
        title
        verified
      }
    }
  }
`);

export { GET_VERIFIED_FINDING_INFO };
