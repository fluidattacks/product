import type { IFinding } from "../types";

interface IVulnerabilitiesToReattackTableProps {
  finding: IFinding;
}

interface IVulnerabilityAttr {
  findingId: string;
  id: string;
  where: string;
  specific: string;
}

interface IVulnerabilityEdge {
  node: IVulnerabilityAttr;
}

export type {
  IVulnerabilityEdge,
  IVulnerabilityAttr,
  IVulnerabilitiesToReattackTableProps,
};
