import { graphql } from "gql";

const GET_FINDING_VULNS_TO_REATTACK = graphql(`
  query GetFindingVulnsToReattack(
    $after: String
    $findingId: String!
    $first: Int
  ) {
    finding(identifier: $findingId) {
      id
      __typename
      vulnerabilitiesToReattackConnection(after: $after, first: $first) {
        edges {
          node {
            id
            findingId
            specific
            where
          }
        }
        pageInfo {
          endCursor
          hasNextPage
        }
      }
    }
  }
`);

export { GET_FINDING_VULNS_TO_REATTACK };
