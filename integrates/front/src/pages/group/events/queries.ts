import { graphql } from "gql";

const GET_EVENTS = graphql(`
  query GetEvents($groupName: String!) {
    group(groupName: $groupName) {
      name
      events {
        id
        closingDate
        detail
        environment
        eventDate
        eventStatus
        eventType
        groupName
        root {
          ... on GitRoot {
            id
            nickname
          }
          ... on URLRoot {
            id
            nickname
          }
          ... on IPRoot {
            id
            nickname
          }
        }
      }
    }
  }
`);

const ADD_EVENT_MUTATION = graphql(`
  mutation AddEventMutation(
    $detail: String!
    $environmentUrl: String
    $eventDate: DateTime!
    $eventType: EventType!
    $groupName: String!
    $rootId: ID
  ) {
    addEvent(
      detail: $detail
      environmentUrl: $environmentUrl
      eventDate: $eventDate
      eventType: $eventType
      groupName: $groupName
      rootId: $rootId
    ) {
      eventId
      success
    }
  }
`);

const REQUEST_EVENT_VERIFICATION_MUTATION = graphql(`
  mutation RequestEventVerification(
    $comments: String!
    $eventId: String!
    $groupName: String!
  ) {
    requestEventVerification(
      comments: $comments
      eventId: $eventId
      groupName: $groupName
    ) {
      success
    }
  }
`);

const REQUEST_VULNS_HOLD_MUTATION = graphql(`
  mutation RequestVulnerabilitiesHold(
    $eventId: String!
    $findingId: String!
    $groupName: String!
    $vulnerabilities: [String!]!
  ) {
    requestVulnerabilitiesHold(
      eventId: $eventId
      findingId: $findingId
      groupName: $groupName
      vulnerabilities: $vulnerabilities
    ) {
      success
    }
  }
`);

export {
  ADD_EVENT_MUTATION,
  GET_EVENTS,
  REQUEST_EVENT_VERIFICATION_MUTATION,
  REQUEST_VULNS_HOLD_MUTATION,
};
