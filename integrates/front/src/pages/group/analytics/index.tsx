import { Container, CustomThemeProvider } from "@fluidattacks/design";
import isNull from "lodash/isNull";
import * as React from "react";
import { useLocation, useParams } from "react-router-dom";
import { useTheme } from "styled-components";

import { Charts } from "features/charts";

const GroupAnalytics: React.FC = (): JSX.Element => {
  const params = useParams() as { groupName: string };
  const searchParams = new URLSearchParams(useLocation().search);
  const theme = useTheme();

  const subjectFromSearchParams = searchParams.get("group");
  const subject = isNull(subjectFromSearchParams)
    ? params.groupName
    : subjectFromSearchParams;

  return (
    <CustomThemeProvider>
      <Container
        bgColor={theme.palette.gray[50]}
        height={"100hv"}
        px={1.25}
        py={1.25}
      >
        <Charts
          bgChange={searchParams.get("bgChange") === "true"}
          entity={"group"}
          reportMode={searchParams.get("reportMode") === "true"}
          subject={subject}
        />
      </Container>
    </CustomThemeProvider>
  );
};

export { GroupAnalytics };
