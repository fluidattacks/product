import type {
  IFilterOptions,
  IOptionsProps,
  IUseFilterProps,
} from "@fluidattacks/design";
import { useFilters } from "@fluidattacks/design";
import { useMemo } from "react";
import { useTranslation } from "react-i18next";

import type { IExecution } from "../types";

const useDevSecOpsFilters = ({
  groupName,
  setFilters,
}: {
  groupName: string;
  setFilters: React.Dispatch<
    React.SetStateAction<IFilterOptions<IExecution>[]>
  >;
}): IUseFilterProps<IExecution> => {
  const { t } = useTranslation();

  const filterOptions: IOptionsProps<IExecution>[] = useMemo(
    (): IOptionsProps<IExecution>[] => [
      {
        filterOptions: [
          {
            key: "status",
            label: t("group.forces.status.title"),
            options: [
              { label: "Secure", value: "Secure" },
              { label: "Vulnerable", value: "Vulnerable" },
            ],
            type: "radioButton",
          },
        ],
        label: t("group.forces.status.title"),
      },
      {
        filterOptions: [
          {
            key: "strictness",
            label: t("group.forces.strictness.title"),
            options: [
              { label: "Strict", value: "Strict" },
              { label: "Tolerant", value: "Tolerant" },
            ],
            type: "radioButton",
          },
        ],
        label: t("group.forces.strictness.title"),
      },
      {
        filterOptions: [
          {
            key: "kind",
            label: t("group.forces.kind.title"),
            options: [
              { label: "Dynamic", value: "DYNAMIC" },
              { label: "Static", value: "STATIC" },
            ],
            type: "radioButton",
          },
        ],
        label: t("group.forces.kind.title"),
      },
      {
        filterOptions: [
          {
            key: "breakBuild",
            label: t("group.forces.exitCode.title"),
            options: [
              { label: "Yes", value: "Yes" },
              { label: "No", value: "No" },
            ],
            type: "radioButton",
          },
        ],
        label: t("group.forces.exitCode.title"),
      },
      {
        filterOptions: [
          {
            key: "gitRepo",
            label: `${t("group.forces.gitRepo")} is`,
            type: "text",
          },
          {
            filterFn: "includesInsensitive",
            key: "gitRepo",
            label: `or ${t("group.forces.gitRepo")} contains`,
            type: "text",
          },
        ],
        label: t("group.forces.gitRepo"),
      },
      {
        filterOptions: [
          {
            key: "date",
            label: t("group.forces.date"),
            type: "dateRange",
          },
        ],
        label: t("group.forces.date"),
      },
    ],
    [t],
  );

  function handleFilterChange(
    newFilters: readonly IFilterOptions<IExecution>[],
  ): void {
    setFilters(newFilters as IFilterOptions<IExecution>[]);
  }

  return useFilters({
    localStorageKey: `forcesTable-columnFilters-${groupName}`,
    onFiltersChange: handleFilterChange,
    options: filterOptions,
  });
};

export { useDevSecOpsFilters };
