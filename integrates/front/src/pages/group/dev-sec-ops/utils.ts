import type { IFilterOptions } from "@fluidattacks/design";
import _ from "lodash";

import type {
  IExecution,
  IFoundVulnerabilities,
  TForcesExecutionsEdges,
} from "./types";

import type { IFilter } from "components/filter/types";
import { formatDateTime } from "utils/date";
import { translate } from "utils/translations/translate";

const toTitleCase = (str: string): string =>
  str
    .split(" ")
    .map((item): string => item[0].toUpperCase() + item.slice(1).toLowerCase())
    .join(" ");

const formatBreakBuild = (exitCode: string): string => {
  if (exitCode === "66") {
    return "Yes";
  } else if (exitCode === "0") {
    return "No";
  }

  return "Error";
};
const formatFoundVulnerabilities = (
  vulnerabilities: IExecution["vulnerabilities"],
): IFoundVulnerabilities => {
  if (vulnerabilities === null) {
    return {
      accepted: 0,
      closed: 0,
      open: 0,
      total: 0,
    };
  }

  return {
    accepted: vulnerabilities.numOfAcceptedVulnerabilities ?? 0,
    closed: vulnerabilities.numOfClosedVulnerabilities ?? 0,
    open: vulnerabilities.numOfOpenVulnerabilities ?? 0,
    total:
      (vulnerabilities.numOfAcceptedVulnerabilities ?? 0) +
      (vulnerabilities.numOfOpenVulnerabilities ?? 0) +
      (vulnerabilities.numOfClosedVulnerabilities ?? 0),
  };
};

const formatExecutions = (executions: TForcesExecutionsEdges): IExecution[] => {
  return executions.map((execution): IExecution => {
    const date = formatDateTime(execution.node.date);
    const kind = translate.t(
      `group.forces.kind.${(execution.node.kind ?? "").toLowerCase()}`,
    );
    const strictness = toTitleCase(
      translate.t(
        execution.node.strictness === "lax"
          ? "group.forces.strictness.tolerant"
          : "group.forces.strictness.strict",
      ),
    );
    const { vulnerabilities } = execution.node;
    const foundVulnerabilities: IFoundVulnerabilities = _.isNull(
      vulnerabilities,
    )
      ? {
          accepted: 0,
          closed: 0,
          open: 0,
          total: 0,
        }
      : formatFoundVulnerabilities(vulnerabilities);
    const status = translate.t(
      foundVulnerabilities.open === 0
        ? "group.forces.status.secure"
        : "group.forces.status.vulnerable",
    );
    const breakBuild = formatBreakBuild(execution.node.exitCode ?? "66");

    return {
      ...execution.node,
      breakBuild,
      date,
      foundVulnerabilities,
      kind,
      status,
      strictness,
    };
  });
};

const formatExecutionFilters = (state: string): string => {
  const execFormat: Record<string, string> = {
    breakBuild: "exitCode",
    gitRepo: "gitRepo",
    kind: "type",
    status: "status",
    strictness: "strictness",
  };

  return execFormat[state];
};

const unformatBreakBuild = (breakBuild: string): string => {
  const unformat: Record<string, string> = {
    error: "1",
    no: "0",
    yes: "66",
  };

  return unformat[breakBuild.toLowerCase()];
};

const unformatKind = (kind: string): string => {
  const unformat: Record<string, string> = {
    dynamic: "dynamic",
    static: "static",
  };

  return unformat[kind.toLowerCase()];
};

const unformatStrictness = (strictness: string): string => {
  const unformat: Record<string, string> = {
    strict: "strict",
    tolerant: "lax",
  };

  return unformat[strictness.toLowerCase()];
};

const unformatFilterValues = (
  value: IFilter<IExecution>,
): Record<string, unknown> => {
  const unformat = (): Record<string, unknown> => {
    if (value.id === "date")
      return {
        fromDate: value.rangeValues?.[0],
        toDate: value.rangeValues?.[1],
      };

    const titleFormat = formatExecutionFilters(value.id);
    if (_.isUndefined(value.value)) return { [titleFormat]: undefined };

    if (value.id === "breakBuild")
      return { [titleFormat]: unformatBreakBuild(value.value) };

    if (value.id === "kind")
      return { [titleFormat]: unformatKind(value.value) };

    return value.id === "strictness"
      ? { [titleFormat]: unformatStrictness(value.value) }
      : {
          [titleFormat]: value.value,
        };
  };

  return unformat();
};

const getVulnerabilitySummaries = (
  vulnerabilities: IExecution["vulnerabilities"],
): string => {
  const acceptedTrans = translate.t(
    "group.forces.foundVulnerabilitiesNew.accepted",
  );
  const unManagedTrans = translate.t(
    "group.forces.foundVulnerabilitiesNew.unManaged",
  );
  const totalTrans = translate.t("group.forces.foundVulnerabilitiesNew.total");
  const numOfManagedVulnerabilities =
    vulnerabilities === null
      ? 0
      : (vulnerabilities.numOfManagedVulnerabilities ?? 0);
  const numOfUnManagedVulnerabilities =
    vulnerabilities === null
      ? 0
      : (vulnerabilities.numOfUnManagedVulnerabilities ?? 0);

  const managedStr = `${numOfManagedVulnerabilities} ${acceptedTrans}`;
  const unManagedStr = `${numOfUnManagedVulnerabilities} ${unManagedTrans}`;
  const totalStr = `${numOfManagedVulnerabilities + numOfUnManagedVulnerabilities} ${totalTrans}`;

  return `${managedStr}, ${unManagedStr}, ${totalStr}`;
};

const emptyFilters = (filters: IFilterOptions<IExecution>[]): boolean => {
  const empty = filters.every((filter): boolean => {
    return (
      _.isEmpty(filter.value) &&
      _.isUndefined(filter.minValue) &&
      _.isUndefined(filter.maxValue) &&
      (filter.options?.every(
        ({ checked }): boolean => _.isUndefined(checked) || !checked,
      ) ??
        true)
    );
  });

  return empty;
};

export {
  emptyFilters,
  formatExecutionFilters,
  formatExecutions,
  formatFoundVulnerabilities,
  getVulnerabilitySummaries,
  toTitleCase,
  unformatBreakBuild,
  unformatFilterValues,
  unformatKind,
  unformatStrictness,
};
