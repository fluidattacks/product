import type { ColumnDef } from "@tanstack/react-table";

import type { IExploitResult } from "../types";
import { statusFormatter } from "features/vulnerabilities/formatters/status";
import { translate } from "utils/translations/translate";

const columns: ColumnDef<IExploitResult>[] = [
  {
    accessorFn: (row): number =>
      row.exploitability === null ? 0 : parseFloat(row.exploitability),
    header: translate.t("group.forces.compromisedToe.exploitability"),
    id: "exploitability",
  },
  {
    accessorFn: (row): string => row.state,
    cell: (cell): JSX.Element => statusFormatter(String(cell.getValue())),
    header: translate.t("group.forces.compromisedToe.status"),
    id: "state",
  },
  {
    accessorFn: (row): string => row.kind ?? "",
    header: translate.t("group.forces.compromisedToe.type"),
    id: "kind",
  },
  {
    accessorFn: (row): string => row.who ?? "",
    header: translate.t("group.forces.compromisedToe.specific"),
    id: "who",
  },
  {
    accessorFn: (row): string => row.where ?? "",
    header: translate.t("group.forces.compromisedToe.where"),
    id: "where",
  },
];

const getStatus = (status: string): string => {
  switch (status) {
    case "OPEN":
      return translate.t("group.forces.status.vulnerable");
    case "CLOSED":
      return translate.t("group.forces.status.secure");
    case "ACCEPTED":
      return translate.t("group.forces.status.accepted");
    default:
      return "";
  }
};

export { columns, getStatus };
