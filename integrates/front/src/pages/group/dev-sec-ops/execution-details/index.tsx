import { Container, Heading, Text } from "@fluidattacks/design";
import { useTranslation } from "react-i18next";

import type { IExecutionDetailsProps } from "../types";
import { getVulnerabilitySummaries } from "../utils";

const ExecutionDetails = ({
  execution,
}: IExecutionDetailsProps): JSX.Element => {
  const { t } = useTranslation();

  return (
    <Container display={"flex"}>
      <Container
        display={"flex"}
        flexDirection={"column"}
        gap={1.25}
        width={"50%"}
      >
        <Heading fontWeight={"bold"} size={"xs"}>
          {t("group.forces.date")}
        </Heading>
        <Heading fontWeight={"bold"} size={"xs"}>
          {t("group.forces.status.title")}
        </Heading>
        <Heading fontWeight={"bold"} size={"xs"}>
          {t("group.forces.strictness.title")}
        </Heading>
        <Heading fontWeight={"bold"} size={"xs"}>
          {t("group.forces.severityThreshold.title")}
        </Heading>
        <Heading fontWeight={"bold"} size={"xs"}>
          {t("group.forces.gracePeriod.title")}
        </Heading>
        <Heading fontWeight={"bold"} size={"xs"}>
          {t("group.forces.daysUntilItBreaks.title")}
        </Heading>
        <Heading fontWeight={"bold"} size={"xs"}>
          {t("group.forces.kind.title")}
        </Heading>
        <Heading fontWeight={"bold"} size={"xs"}>
          {t("group.forces.gitRepo")}
        </Heading>
        <Heading fontWeight={"bold"} size={"xs"}>
          {t("group.forces.identifier")}
        </Heading>
        <Heading fontWeight={"bold"} size={"xs"}>
          {t("group.forces.foundVulnerabilities.title")}
        </Heading>
      </Container>
      <Container
        display={"flex"}
        flexDirection={"column"}
        gap={1.25}
        width={"50%"}
      >
        <Text size={"md"}>{execution.date}</Text>
        <Text size={"md"}>{execution.status}</Text>
        <Text size={"md"}>{execution.strictness}</Text>
        <Text size={"md"}>{execution.severityThreshold}</Text>
        <Text size={"md"}>{execution.gracePeriod}</Text>
        <Text size={"md"}>{execution.daysUntilItBreaks ?? "-"}</Text>
        <Text size={"md"}>{execution.kind}</Text>
        <Text size={"md"}>{execution.gitRepo}</Text>
        <Text size={"md"}>{execution.executionId}</Text>
        <Text size={"md"}>
          {getVulnerabilitySummaries(execution.vulnerabilities ?? null)}
        </Text>
      </Container>
    </Container>
  );
};

export { ExecutionDetails };
