import { useLazyQuery, useQuery } from "@apollo/client";
import type { IFilterOptions } from "@fluidattacks/design";
import _ from "lodash";
import { useCallback } from "react";
import { useTranslation } from "react-i18next";

import {
  GET_FORCES_EXECUTIONS,
  GET_GROUP_POLICIES,
  SEND_FORCES_EXECUTIONS_FILE,
} from "./queries";
import type { IExecution } from "./types";
import {
  formatExecutions,
  unformatBreakBuild,
  unformatKind,
  unformatStrictness,
} from "./utils";

import { getCheckedFilter, getNewFilter } from "components/filter/utils";
import type { GetGroupPoliciesQuery } from "gql/graphql";
import { useAudit } from "hooks/use-audit";
import { GET_NOTIFICATIONS } from "pages/dashboard/navbar/downloads/queries";
import { Logger } from "utils/logger";
import { msgError, msgSuccess } from "utils/notifications";

interface IUseExecutionsQuery {
  fetchNextPage: () => Promise<void>;
  hasNextPage: boolean;
  loading: boolean;
  executions: IExecution[];
  total: number;
}

const useExecutionsQuery = (
  groupName: string,
  search: string | undefined,
  filters: IFilterOptions<IExecution>[],
): IUseExecutionsQuery => {
  const { t } = useTranslation();
  const { addAuditEvent } = useAudit();

  const status = getCheckedFilter(filters, "status")?.value;
  const strictness = getCheckedFilter(filters, "strictness")?.value;
  const kind = getCheckedFilter(filters, "kind")?.value;
  const breakBuild = getCheckedFilter(filters, "breakBuild")?.value;
  const gitRepoFilters = filters.filter(
    (filter): boolean => filter.key === "gitRepo",
  );
  const gitRepo = gitRepoFilters.find(
    (filter): boolean => filter.filterFn === "includesInsensitive",
  )?.value;
  const gitRepoExactFilter = gitRepoFilters.find(
    (filter): boolean => filter.filterFn !== "includesInsensitive",
  )?.value;
  const date = getNewFilter(filters, "date");
  const minValue =
    _.isEmpty(date?.minValue) && _.isNil(date?.minValue)
      ? undefined
      : String(date?.minValue);
  const maxValue =
    _.isEmpty(date?.maxValue) && _.isNil(date?.maxValue)
      ? undefined
      : String(date?.maxValue);

  const { data, fetchMore, loading } = useQuery(GET_FORCES_EXECUTIONS, {
    fetchPolicy: "cache-and-network",
    nextFetchPolicy: "cache-first",
    onCompleted: (): void => {
      addAuditEvent("Group.ForcesExecutions", groupName);
    },
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error): void => {
        msgError(t("groupAlerts.errorTextsad"));
        Logger.warning("An error occurred getting executions", error);
      });
    },
    variables: {
      exitCode:
        breakBuild === undefined ? undefined : unformatBreakBuild(breakBuild),
      first: 150,
      fromDate: minValue,
      gitRepo: gitRepo === "" ? undefined : gitRepo,
      gitRepoExactFilter:
        gitRepoExactFilter === "" ? undefined : gitRepoExactFilter,
      groupName,
      search: search === "" ? undefined : search,
      status,
      strictness:
        strictness === undefined ? undefined : unformatStrictness(strictness),
      toDate: maxValue,
      type: kind === undefined ? undefined : unformatKind(kind),
    },
  });

  const edges = data?.group.forcesExecutionsConnection.edges ?? [];
  const pageInfo = data?.group.forcesExecutionsConnection.pageInfo ?? {
    endCursor: "[]",
    hasNextPage: false,
  };

  return {
    executions: formatExecutions(edges),
    fetchNextPage: async (): Promise<void> => {
      await fetchMore({
        variables: {
          after: (JSON.parse(pageInfo.endCursor) as number[]).map(String),
        },
      });
    },
    hasNextPage: pageInfo.hasNextPage,
    loading,
    total: data?.group.forcesExecutionsConnection.total ?? 0,
  };
};

interface IUseReportQuery {
  requestReport: () => void;
}

const useReportQuery = (groupName: string): IUseReportQuery => {
  const { t } = useTranslation();

  const [requestGroupReport, { client }] = useLazyQuery(
    SEND_FORCES_EXECUTIONS_FILE,
    {
      onCompleted: (): void => {
        msgSuccess(
          t("groupAlerts.reportRequested"),
          t("groupAlerts.titleSuccess"),
        );
        void client.refetchQueries({ include: [GET_NOTIFICATIONS] });
      },
      onError: (errors): void => {
        errors.graphQLErrors.forEach((error): void => {
          switch (error.message) {
            case "Exception - The user already has a requested report for the same group":
              msgError(t("groupAlerts.reportAlreadyRequested"));
              break;
            case "Exception - Stakeholder could not be verified":
              msgError(
                t("group.findings.report.alerts.nonVerifiedStakeholder"),
              );
              break;
            default:
              msgError(t("groupAlerts.errorTextsad"));
              Logger.warning(
                "An error occurred requesting group report",
                error,
              );
          }
        });
      },
    },
  );

  const requestReport = useCallback((): void => {
    void requestGroupReport({ variables: { groupName } });
  }, [groupName, requestGroupReport]);

  return { requestReport };
};

interface IUsePoliciesQuery {
  groupPolicies: GetGroupPoliciesQuery | undefined;
  policiesLoading: boolean;
}

const usePoliciesQuery = (groupName: string): IUsePoliciesQuery => {
  const { t } = useTranslation();
  const { data, loading } = useQuery(GET_GROUP_POLICIES, {
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error): void => {
        msgError(t("groupAlerts.errorTextsad"));
        Logger.warning("An error occurred fetching group policies", error);
      });
    },
    variables: { groupName },
  });

  return {
    groupPolicies: data,
    policiesLoading: loading,
  };
};

export { useExecutionsQuery, useReportQuery, usePoliciesQuery };
