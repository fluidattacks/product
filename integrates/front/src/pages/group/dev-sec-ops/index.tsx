import {
  Alert,
  AppliedFilters,
  Button,
  Container,
  type IFilterOptions,
  Text,
} from "@fluidattacks/design";
import type { ColumnDef, Row } from "@tanstack/react-table";
import { Fragment, useCallback, useMemo, useState } from "react";
import * as React from "react";
import { useTranslation } from "react-i18next";
import { useParams } from "react-router-dom";
import { useTheme } from "styled-components";

import { EmptyState } from "./empty-state";
import { ExecutionModal } from "./execution";
import { useDevSecOpsFilters } from "./filters";
import { useExecutionsQuery, usePoliciesQuery, useReportQuery } from "./hooks";
import type { IExecution } from "./types";
import { emptyFilters } from "./utils";

import { useSearch } from "components/search/hooks";
import { Table } from "components/table";
import { filterDate } from "components/table/utils";
import { statusFormatter } from "features/vulnerabilities/formatters/status";
import { useDailyAlert, useModal, useTable } from "hooks";

const DEFAULT_POLICY = 0;
const PADDING = 1.25;

const GroupDevSecOps: React.FC = (): JSX.Element => {
  const { t } = useTranslation();
  const { groupName } = useParams() as { groupName: string };
  const theme = useTheme();
  const executionsModalRef = useModal("executions-modal");

  const [search, setSearch] = useSearch();
  const [currentRow, setCurrentRow] = useState<IExecution>();
  const [filters, setFilters] = useState<IFilterOptions<IExecution>[]>([]);
  const tableRef = useTable("tblForcesExecutionsSorting");
  const { Filters, options, removeFilter } = useDevSecOpsFilters({
    groupName,
    setFilters,
  });

  const headersExecutionTable = useMemo(
    (): ColumnDef<IExecution>[] => [
      {
        accessorKey: "date",
        filterFn: filterDate,
        header: t("group.forces.date"),
      },
      {
        accessorKey: "status",
        cell: (cell): JSX.Element => statusFormatter(String(cell.getValue())),
        header: t("group.forces.status.title"),
      },
      {
        accessorKey: "breakBuild",
        header: t("group.forces.exitCode.title"),
      },
      {
        accessorFn: (row): number => {
          return row.vulnerabilities?.numOfManagedVulnerabilities ?? 0;
        },
        header: t("group.forces.status.managedVulnerabilities"),
      },
      {
        accessorFn: (row): number => {
          return row.vulnerabilities?.numOfUnManagedVulnerabilities ?? 0;
        },
        header: t("group.forces.status.unManagedVulnerabilities"),
      },
      {
        accessorKey: "strictness",
        header: t("group.forces.strictness.title"),
      },
      {
        accessorKey: "kind",
        header: t("group.forces.kind.title"),
      },
      {
        accessorFn: (row): string => {
          if (row.gitRepo === "unable to retrieve") {
            return "all roots";
          }

          return row.gitRepo ?? "";
        },
        header: t("group.forces.gitRepo"),
        id: "gitRepo",
      },
      {
        accessorFn: (row): string => {
          const EXECUTION_ID_LENGTH = 8;

          return `${String(row.executionId).slice(0, EXECUTION_ID_LENGTH)}...`;
        },
        header: t("group.forces.identifier"),
      },
    ],
    [t],
  );

  const openSeeExecutionDetailsModal = useCallback(
    (rowInfo: Row<IExecution>): VoidFunction => {
      return (): void => {
        setCurrentRow(rowInfo.original);
        executionsModalRef.open();
      };
    },
    [executionsModalRef],
  );

  const { executions, loading, fetchNextPage, hasNextPage, total } =
    useExecutionsQuery(groupName, search, filters);

  const { requestReport } = useReportQuery(groupName);

  const { groupPolicies, policiesLoading } = usePoliciesQuery(groupName);

  const useAlert = useDailyAlert("group-policies", groupName);

  if (
    !loading &&
    !policiesLoading &&
    executions.length === 0 &&
    (search ?? "").length === 0 &&
    emptyFilters(filters)
  ) {
    return <EmptyState />;
  }

  return (
    <Fragment>
      <Container
        bgColor={theme.palette.gray[50]}
        gap={1.25}
        padding={[1, PADDING, 0, PADDING]}
        px={1.25}
        py={1.25}
      >
        <Text
          color={theme.palette.gray[800]}
          fontWeight={"bold"}
          lineSpacing={1.5}
          size={"xl"}
        >
          {t("group.forces.title")}
        </Text>
        <Text color={theme.palette.gray[800]} pb={0.5} pt={0.5} size={"xs"}>
          {t("group.forces.tableAdvice")}
        </Text>
        <Alert closable={true} show={useAlert} variant={"info"}>
          <Text color={theme.palette.info["700"]} size={"xs"}>
            <strong>
              {t("group.forces.policiesAlert.temporaryAcceptance.title")}
            </strong>
            {t("group.forces.policiesAlert.temporaryAcceptance.text1")}
            <strong>
              {t("group.forces.policiesAlert.temporaryAcceptance.text2", {
                maxDays:
                  groupPolicies?.group.maxAcceptanceDays ?? DEFAULT_POLICY,
                maxNumber:
                  groupPolicies?.group.maxNumberAcceptances ?? DEFAULT_POLICY,
              })}
            </strong>
            {t("group.forces.policiesAlert.temporaryAcceptance.text3")}
            <strong>
              {t("group.forces.policiesAlert.temporaryAcceptance.text4", {
                maxSeverity:
                  groupPolicies?.group.maxAcceptanceSeverity ?? DEFAULT_POLICY,
                minSeverity:
                  groupPolicies?.group.minAcceptanceSeverity ?? DEFAULT_POLICY,
              })}
            </strong>
          </Text>
          <Text color={theme.palette.info["700"]} size={"xs"}>
            <strong>{t("group.forces.policiesAlert.devSecOps.title")}</strong>
            {t("group.forces.policiesAlert.devSecOps.text1")}
            <strong>
              {t("group.forces.policiesAlert.devSecOps.text2", {
                gracePeriod:
                  groupPolicies?.group.vulnerabilityGracePeriod ??
                  DEFAULT_POLICY,
              })}
            </strong>
            {t("group.forces.policiesAlert.devSecOps.text3")}
            <strong>
              {t("group.forces.policiesAlert.devSecOps.text4", {
                minSeverity:
                  groupPolicies?.group.minBreakingSeverity ?? DEFAULT_POLICY,
              })}
            </strong>
          </Text>
        </Alert>
      </Container>
      <Container
        bgColor={theme.palette.gray[50]}
        height={"100hv"}
        padding={[0, 1, 0, 1]}
        px={1.25}
        py={1.25}
      >
        <Table
          columns={headersExecutionTable}
          data={executions}
          filters={<Filters />}
          filtersApplied={
            <AppliedFilters onClose={removeFilter} options={options} />
          }
          loadingData={loading}
          onNextPage={fetchNextPage}
          onRowClick={openSeeExecutionDetailsModal}
          onSearch={setSearch}
          options={{ enableSorting: false, hasNextPage, size: total }}
          rightSideComponents={
            <Button id={"reports"} onClick={requestReport} showArrow={true}>
              {t("group.findings.buttons.report.text")}
            </Button>
          }
          tableRef={tableRef}
        />
        {currentRow ? (
          <ExecutionModal
            execution={currentRow}
            modalRef={executionsModalRef}
          />
        ) : undefined}
      </Container>
    </Fragment>
  );
};

export { GroupDevSecOps };
