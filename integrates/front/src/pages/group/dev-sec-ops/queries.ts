import { graphql } from "gql";

const GET_FORCES_EXECUTIONS = graphql(`
  query GetForcesExecutions(
    $after: [String]
    $exitCode: String
    $first: Int
    $fromDate: DateTime
    $gitRepo: String
    $gitRepoExactFilter: String
    $groupName: String!
    $search: String
    $status: String
    $strictness: String
    $toDate: DateTime
    $type: String
  ) {
    group(groupName: $groupName) {
      name
      forcesExecutionsConnection(
        after: $after
        exitCode: $exitCode
        first: $first
        fromDate: $fromDate
        gitRepo: $gitRepo
        gitRepoExactFilter: $gitRepoExactFilter
        search: $search
        status: $status
        strictness: $strictness
        toDate: $toDate
        type: $type
      ) {
        total
        edges {
          node {
            date
            daysUntilItBreaks
            executionId
            exitCode
            gitRepo
            gracePeriod
            groupName
            kind
            severityThreshold
            strictness
            vulnerabilities {
              numOfAcceptedVulnerabilities
              numOfClosedVulnerabilities
              numOfManagedVulnerabilities
              numOfOpenVulnerabilities
              numOfUnManagedVulnerabilities
            }
          }
        }
        pageInfo {
          endCursor
          hasNextPage
        }
      }
    }
  }
`);

const GET_FORCES_EXECUTION = graphql(`
  query GetForcesExecution($executionId: String!, $groupName: String!) {
    forcesExecution(executionId: $executionId, groupName: $groupName) {
      groupName
      log
      vulnerabilities {
        numOfAcceptedVulnerabilities
        numOfClosedVulnerabilities
        numOfManagedVulnerabilities
        numOfOpenVulnerabilities
        numOfUnManagedVulnerabilities
        accepted {
          exploitability
          kind
          state
          where
          who
        }
        closed {
          exploitability
          kind
          state
          where
          who
        }
        open {
          exploitability
          kind
          state
          where
          who
        }
      }
    }
  }
`);

const SEND_FORCES_EXECUTIONS_FILE = graphql(`
  query SendForcesExecutionsFile($groupName: String!) {
    sendExportedFile(groupName: $groupName) {
      success
    }
  }
`);

const GET_GROUP_POLICIES = graphql(`
  query GetGroupPolicies($groupName: String!) {
    group(groupName: $groupName) {
      daysUntilItBreaks
      maxAcceptanceDays
      maxAcceptanceSeverity
      maxNumberAcceptances
      minAcceptanceSeverity
      minBreakingSeverity
      name
      vulnerabilityGracePeriod
    }
  }
`);

export {
  GET_FORCES_EXECUTION,
  GET_FORCES_EXECUTIONS,
  SEND_FORCES_EXECUTIONS_FILE,
  GET_GROUP_POLICIES,
};
