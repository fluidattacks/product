const loginLogo =
  "https://res.cloudinary.com/fluid-attacks/image/upload/v1707416779/integrates/login/loginLogo.webp";
const loginBitBucketLogo =
  "https://res.cloudinary.com/fluid-attacks/image/upload/v1707499340/integrates/login/loginBitBucketLogo.webp";
const loginGoogleLogo =
  "https://res.cloudinary.com/fluid-attacks/image/upload/v1707499363/integrates/login/loginGoogleLogo.webp";
const loginMicrosoftLogo =
  "https://res.cloudinary.com/fluid-attacks/image/upload/v1707499386/integrates/login/loginMicrosoftLogo.webp";
const loginImage =
  "https://res.cloudinary.com/fluid-attacks/image/upload/v1707323245/integrates/login/fluidExtension.webp";

export {
  loginLogo,
  loginBitBucketLogo,
  loginGoogleLogo,
  loginMicrosoftLogo,
  loginImage,
};
