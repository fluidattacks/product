interface IProvider {
  imageAlt: string;
  imageId: string;
  name: string;
  provider: string;
  redirectUrl: string;
}

export type { IProvider };
