import { Container } from "@fluidattacks/design";
import type { ITabProps } from "@fluidattacks/design/dist/components/tabs/types";
import { capitalize } from "lodash";
import * as React from "react";
import { useTranslation } from "react-i18next";
import { useParams } from "react-router-dom";

import { SectionHeader } from "components/section-header";
import { useOrgUrl } from "pages/organization/hooks";

const PortfolioTemplate: React.FC<Readonly<{ children: JSX.Element }>> = ({
  children,
}): JSX.Element => {
  const { t } = useTranslation();
  const { url } = useOrgUrl("/portfolios/*");
  const { tagName } = useParams() as {
    tagName: string;
  };

  const tabs: ITabProps[] = [
    {
      id: "tag-indicators-tab",
      label: t("organization.tabs.portfolios.tabs.indicators.text"),
      link: `${url}/${tagName}/analytics`,
      tooltip: t("organization.tabs.portfolios.tabs.indicators.tooltip"),
    },
    {
      id: "tag-groups-tab",
      label: t("organization.tabs.portfolios.tabs.group.text"),
      link: `${url}/${tagName}/groups`,
      tooltip: t("organization.tabs.portfolios.tabs.group.tooltip"),
    },
  ];

  return (
    <div>
      <SectionHeader
        goBackTo={url}
        header={`${capitalize(tagName)} ${t("organization.tabs.portfolios.portfolio.text")}`}
        tabs={tabs}
      />
      <Container py={1.25}>{children}</Container>
    </div>
  );
};

export { PortfolioTemplate };
