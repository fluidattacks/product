import { graphql } from "gql";

export const PORTFOLIO_GROUPS_QUERY = graphql(`
  query GetPortfolioGroups($organizationId: String, $tag: String!) {
    tag(organizationId: $organizationId, tag: $tag) {
      name
      groups {
        description
        name
      }
    }
  }
`);
