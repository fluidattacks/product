import { screen, waitFor } from "@testing-library/react";
import dayjs from "dayjs";
import { newMockXhr } from "mock-xmlhttprequest";
import { HttpResponse, type StrictResponse, graphql } from "msw";

import { GET_CURRENT_USER } from "./queries";

import Home from ".";
import { authContext } from "context/auth";
import type { GetCurrentUserAtHomeQuery as GetCurrentUser } from "gql/graphql";
import { render } from "mocks";
import { LINK } from "mocks/constants";

jest.spyOn(window, "alert").mockImplementation();

describe("home", (): void => {
  const globalXMLHttpRequest = global.XMLHttpRequest;

  // eslint-disable-next-line jest/no-hooks
  beforeEach((): void => {
    const mockXhr = newMockXhr();
    // eslint-disable-next-line functional/immutable-data
    global.XMLHttpRequest = mockXhr;
  });

  // eslint-disable-next-line jest/no-hooks
  afterEach((): void => {
    // eslint-disable-next-line functional/immutable-data
    global.XMLHttpRequest = globalXMLHttpRequest;
  });

  it("should render dashboard", async (): Promise<void> => {
    expect.hasAssertions();

    render(
      <authContext.Provider
        value={{
          awsSubscription: null,
          setUser: jest.fn(),
          tours: {
            newGroup: true,
            newRoot: true,
            welcome: true,
          },
          userEmail: "test@fluidattacks.com",
          userName: "John Doe",
        }}
      >
        <Home />
      </authContext.Provider>,
      {
        memoryRouter: {
          initialEntries: ["/"],
        },
      },
    );

    await expect(
      screen.findByRole("main", undefined, {
        timeout: 5000,
      }),
    ).resolves.toBeInTheDocument();
  });

  it("should render autoenrollment component", async (): Promise<void> => {
    expect.hasAssertions();

    render(
      <authContext.Provider
        value={{
          awsSubscription: null,
          setUser: jest.fn(),
          tours: {
            newGroup: true,
            newRoot: true,
            welcome: true,
          },
          userEmail: "test@fluidattacks.com",
          userName: "John Doe",
        }}
      >
        <Home />
      </authContext.Provider>,
      {
        memoryRouter: {
          initialEntries: ["/"],
        },
        mocks: [
          graphql
            .link(LINK)
            .query(
              GET_CURRENT_USER,
              (): StrictResponse<{ data: GetCurrentUser }> => {
                return HttpResponse.json({
                  data: {
                    me: {
                      __typename: "Me",
                      awsSubscription: null,
                      enrolled: false,
                      permissions: [],
                      phone: null,
                      sessionExpiration: dayjs().add(1, "day").toISOString(),
                      tours: {
                        newGroup: true,
                        newRoot: true,
                        welcome: true,
                      },
                      trial: {
                        completed: true,
                        groupRole: null,
                      },
                      userEmail: "test@fluidattacks.com",
                      userName: "John Doe",
                    },
                  },
                });
              },
            ),
        ],
      },
    );

    await waitFor((): void => {
      expect(screen.queryByText("Hi John!")).toBeInTheDocument();
    });

    await waitFor((): void => {
      expect(
        screen.getByText("autoenrollment.fastTrackDesktop.title"),
      ).toBeInTheDocument();
    });

    await waitFor((): void => {
      expect(
        screen.getByText("autoenrollment.languages.checkLanguages"),
      ).toBeInTheDocument();
    });

    await waitFor((): void => {
      expect(
        screen.getByText("components.repositoriesDropdown.gitLabButton.text"),
      ).toBeInTheDocument();
    });
    await waitFor((): void => {
      expect(
        screen.getByText("components.repositoriesDropdown.gitHubButton.text"),
      ).toBeInTheDocument();
    });
    await waitFor((): void => {
      expect(
        screen.getByText("components.repositoriesDropdown.azureButton.text"),
      ).toBeInTheDocument();
    });
    await waitFor((): void => {
      expect(
        screen.getByText(
          "components.repositoriesDropdown.bitbucketButton.text",
        ),
      ).toBeInTheDocument();
    });
    await waitFor((): void => {
      expect(
        screen.getByText("autoenrollment.fastTrack.manual.button"),
      ).toBeInTheDocument();
    });

    await waitFor((): void => {
      expect(
        screen.getByRole("button", { name: "Authenticate" }),
      ).toBeInTheDocument();
    });

    expect(
      screen.queryByText("autoenrollment.pendingInvitation.title"),
    ).not.toBeInTheDocument();
  });
});
