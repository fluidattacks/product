import { graphql } from "gql";

// Keep this query light weight for a smooth post-login experience
const GET_CURRENT_USER = graphql(`
  query GetCurrentUserAtHome {
    me {
      enrolled
      permissions
      sessionExpiration
      userEmail
      userName
      awsSubscription {
        contractedGroups
        status
        usedGroups
      }
      phone {
        callingCountryCode
        nationalNumber
      }
      tours {
        newGroup
        newRoot
        welcome
      }
      trial {
        completed
        groupRole
      }
    }
  }
`);

const GET_USER_INVITATION = graphql(`
  query GetUserInvitationAtHome {
    me {
      hasAllInvitationsPending
      userEmail
      userName
    }
  }
`);

const GET_ROOTS = graphql(`
  query GetRootsAtHome($groupName: String!) {
    group(groupName: $groupName) {
      name
      roots {
        ... on GitRoot {
          id
          nickname
        }
        ... on IPRoot {
          id
          nickname
        }
        ... on URLRoot {
          id
          nickname
        }
      }
    }
  }
`);

export { GET_CURRENT_USER, GET_USER_INVITATION, GET_ROOTS };
