import { graphql } from "gql";

const GET_CURRENT_USER = graphql(`
  query GetCurrentUserAtRoot {
    me {
      userEmail
      userName
      organizations {
        id
        name
        groups {
          name
        }
      }
    }
  }
`);

export { GET_CURRENT_USER };
