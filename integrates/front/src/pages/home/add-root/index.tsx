import { useQuery } from "@apollo/client";
import isEmpty from "lodash/isEmpty";
import { useState } from "react";
import type { FC } from "react";

import { GET_CURRENT_USER } from "./queries";

import { GET_ROOTS } from "../queries";
import type { IRoot } from "../types";
import { Autoenrollment } from "pages/auto-enrollment";
import { Dashboard } from "pages/dashboard";
import { Logger } from "utils/logger";

const AddRoot: FC = (): JSX.Element => {
  const [groupName, setGroupName] = useState("");

  const { data: rootsData, loading } = useQuery(GET_ROOTS, {
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error): void => {
        Logger.error("Couldn't load roots", error);
      });
    },
    skip: isEmpty(groupName),
    variables: { groupName },
  });

  const { data: userData } = useQuery(GET_CURRENT_USER, {
    onCompleted: (data): void => {
      const group = data.me.organizations[0]?.groups?.[0] ?? { name: "" };
      setGroupName(group.name);
    },
    onError: (error): void => {
      error.graphQLErrors.forEach(({ message }): void => {
        Logger.error("Couldn't load current user", message);
      });
    },
  });

  const orgId = userData?.me.organizations[0]?.id;
  const orgName = userData?.me.organizations[0]?.name;

  const roots: IRoot[] = rootsData?.group.roots ?? [];

  if (roots.length === 0 && !loading) {
    return (
      <Autoenrollment
        initialPage={"oauthRepoForm"}
        trialGroupName={groupName}
        trialOrgId={orgId}
        trialOrgName={orgName}
      />
    );
  }

  return <Dashboard />;
};

export { AddRoot };
