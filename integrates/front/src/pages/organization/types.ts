import type { IPortfolios } from "./portfolios/types";

interface IOrganizationPermission {
  organization: {
    permissions: string[];
  };
}

interface IGetUserPortfolios {
  me: {
    tags: {
      name: string;
      groups: {
        name: string;
      }[];
    }[];
    userEmail: string;
  };
}

interface IOrganizationDataContext {
  hasZtnaRoots: boolean;
  organizationId: string;
  portfolios: IPortfolios[];
}

export type {
  IGetUserPortfolios,
  IOrganizationPermission,
  IOrganizationDataContext,
};
