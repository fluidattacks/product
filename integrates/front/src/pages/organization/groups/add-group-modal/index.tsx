import { useMutation } from "@apollo/client";
import mixpanel from "mixpanel-browser";
import { useCallback, useContext, useState } from "react";
import { useTranslation } from "react-i18next";
import { useNavigate } from "react-router-dom";

import { AddGroupModal as GroupModal } from "./add-group-modal";
import { handleCreateError } from "./utils";

import { authContext } from "context/auth";
import type { Language, ServiceType, SubscriptionType } from "gql/graphql";
import { useTour } from "hooks/use-tour";
import { ADD_GROUP_MUTATION } from "pages/organization/groups/add-group-modal/queries";
import type { IAddGroupModalProps } from "pages/organization/groups/add-group-modal/types";
import { msgSuccess } from "utils/notifications";

type TInitialValues = Record<string, FileList | boolean | string>;

const AddGroupModal = ({
  isOpen,
  onClose,
  onClickOpen,
  organization,
  runTour,
}: IAddGroupModalProps): JSX.Element => {
  const { awsSubscription, refetch: refetchCurrentUser } =
    useContext(authContext);
  const { t } = useTranslation();

  const navigate = useNavigate();
  const { setCompleted } = useTour();
  const [values, setValues] = useState<TInitialValues>({
    advanced: true,
    description: "",
    language: "EN",
    name: "",
    organization: organization.toUpperCase(),
    service: "WHITE",
    type: "CONTINUOUS-ESSENTIAL",
  });

  const handleMutationResult = (result: {
    addGroup: { success: boolean };
  }): void => {
    if (result.addGroup.success) {
      onClose();
      msgSuccess(
        t("organization.tabs.groups.newGroup.success"),
        t("organization.tabs.groups.newGroup.titleSuccess"),
      );
    }
  };

  const [addGroup, { loading: submitting }] = useMutation(ADD_GROUP_MUTATION, {
    onCompleted: handleMutationResult,
    onError: handleCreateError,
  });

  const finishTour = useCallback((): void => {
    setCompleted("newGroup");
    onClose();
  }, [onClose, setCompleted]);

  const handleSubmit = useCallback(
    async (formikValues: TInitialValues): Promise<void> => {
      mixpanel.track("AddGroup");
      setValues(formikValues);
      await addGroup({
        variables: {
          description: formikValues.description as string,
          groupName: (formikValues.name as string).toUpperCase(),
          hasAdvanced: formikValues.type === "CONTINUOUS-ADVANCED",
          hasEssential:
            formikValues.type === "CONTINUOUS-ESSENTIAL" ||
            formikValues.type === "CONTINUOUS-ADVANCED",
          language: formikValues.language as Language,
          organizationName: formikValues.organization as string,
          service: formikValues.service as ServiceType,
          subscription: [
            "CONTINUOUS-ESSENTIAL",
            "CONTINUOUS-ADVANCED",
          ].includes(formikValues.type as string)
            ? ("CONTINUOUS" as SubscriptionType)
            : ("ONESHOT" as SubscriptionType),
        },
      });
      if (refetchCurrentUser !== undefined) {
        await refetchCurrentUser();
      }
      if (runTour) {
        finishTour();
        navigate(
          `/orgs/${organization.toLocaleLowerCase()}/groups/${(formikValues.name as string).toLocaleLowerCase()}/scope`,
        );
      }
    },
    [
      addGroup,
      organization,
      navigate,
      runTour,
      finishTour,
      refetchCurrentUser,
      setValues,
    ],
  );

  const essentialOption = [
    {
      text: "Continuous Hacking - Essential plan",
      tip: t("organization.tabs.groups.newGroup.essential.tooltip"),
      value: "CONTINUOUS-ESSENTIAL",
    },
  ];
  const advancedOptions = [
    {
      text: "Continuous Hacking - Advanced plan",
      tip: t("organization.tabs.groups.newGroup.advanced.tooltip"),
      value: "CONTINUOUS-ADVANCED",
    },
  ];

  const radioBtnOptions =
    awsSubscription === null
      ? essentialOption.concat(advancedOptions)
      : essentialOption;

  return (
    <GroupModal
      handleSubmit={handleSubmit}
      isOpen={isOpen}
      onClickOpen={onClickOpen}
      onClose={onClose}
      radioBtnOptions={radioBtnOptions}
      submitting={submitting}
      values={values}
    />
  );
};

export { AddGroupModal };
