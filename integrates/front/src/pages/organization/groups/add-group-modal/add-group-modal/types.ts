type TInitialValues = Record<string, FileList | boolean | string>;

interface IRadioButtonOptions {
  text: string;
  tip: string;
  value: string;
}

interface IModalProps {
  handleSubmit: (formikValues: TInitialValues) => Promise<void>;
  isOpen?: boolean;
  onClose: () => void;
  onClickOpen: () => void;
  radioBtnOptions: IRadioButtonOptions[];
  submitting: boolean;
  values: TInitialValues;
}

export type { IModalProps, TInitialValues };
