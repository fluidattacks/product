import {
  ComboBox,
  Container,
  Form,
  InnerForm,
  Input,
  Link,
  Modal,
  OutlineContainer,
  RadioButton,
  type TFormMethods,
  Tooltip,
  useModal,
} from "@fluidattacks/design";
import { type FC, Fragment } from "react";
import { useTranslation } from "react-i18next";
import LoadingOverlay from "react-loading-overlay-ts";
import FadeLoader from "react-spinners/FadeLoader";

import type { IModalProps, TInitialValues } from "./types";

import { validationSchema } from "../validations";

const AddGroupModal: FC<IModalProps> = ({
  handleSubmit,
  isOpen = true,
  onClose,
  radioBtnOptions,
  submitting,
  values,
}): JSX.Element => {
  const { t } = useTranslation();
  const modalProps = useModal("add-group");

  const renderRadioGroup = (
    register: TFormMethods<TInitialValues>["register"],
  ): JSX.Element[] =>
    radioBtnOptions.map((option): JSX.Element => {
      return (
        <Container
          alignItems={"end"}
          display={"inline-flex"}
          gap={0.25}
          key={`type_${option.value}`}
        >
          <RadioButton
            {...register("type")}
            label={option.text}
            name={"type"}
            value={option.value}
          />
          <Tooltip
            icon={"circle-info"}
            id={option.text}
            place={"right"}
            tip={option.tip}
          />
        </Container>
      );
    });

  return (
    <Fragment>
      <LoadingOverlay active={submitting} spinner={<FadeLoader />} />
      <Modal
        modalRef={{ ...modalProps, close: onClose, isOpen }}
        size={"sm"}
        title={t("organization.tabs.groups.newGroup.new.group")}
      >
        <Form
          cancelButton={{ onClick: onClose }}
          defaultValues={values}
          onSubmit={handleSubmit}
          yupSchema={validationSchema}
        >
          <InnerForm>
            {({
              control,
              formState: { errors, touchedFields },
              register,
            }): JSX.Element => (
              <Fragment>
                <Input
                  disabled={true}
                  label={t(
                    "organization.tabs.groups.newGroup.organization.text",
                  )}
                  name={"organization"}
                  register={register}
                  tooltip={t(
                    "organization.tabs.groups.newGroup.organization.tooltip",
                  )}
                />
                <Input
                  error={errors.name?.message?.toString()}
                  id={"add-group-name"}
                  isTouched={"name" in touchedFields}
                  isValid={!("name" in errors)}
                  label={t("organization.tabs.groups.newGroup.name.text")}
                  name={"name"}
                  register={register}
                  tooltip={t("organization.tabs.groups.newGroup.name.tooltip")}
                />
                <Input
                  error={errors.description?.message?.toString()}
                  id={"add-group-description"}
                  isTouched={"description" in touchedFields}
                  isValid={!("description" in errors)}
                  label={t(
                    "organization.tabs.groups.newGroup.description.text",
                  )}
                  name={"description"}
                  register={register}
                  tooltip={t(
                    "organization.tabs.groups.newGroup.description.tooltip",
                  )}
                />
                <OutlineContainer
                  htmlFor={"type"}
                  label={t("organization.tabs.groups.newGroup.type.title")}
                  required={true}
                >
                  <Container
                    display={"inline-flex"}
                    flexDirection={"column"}
                    gap={0.5}
                    mb={0.25}
                  >
                    {renderRadioGroup(register)}
                  </Container>
                </OutlineContainer>
                <Link href={"https://fluidattacks.com/plans/"}>
                  {"Learn more about Continuous Hacking plans"}
                </Link>
                <ComboBox
                  control={control}
                  id={"add-group-testing-type"}
                  items={[
                    {
                      name: t(
                        "organization.tabs.groups.newGroup.service.black",
                      ),
                      value: "BLACK",
                    },
                    {
                      name: t(
                        "organization.tabs.groups.newGroup.service.white",
                      ),
                      value: "WHITE",
                    },
                  ]}
                  label={t("organization.tabs.groups.newGroup.service.title")}
                  name={"service"}
                  placeholder={t("organization.tabs.groups.newGroup.select")}
                  required={true}
                />
                <ComboBox
                  control={control}
                  id={"add-group-report-language"}
                  items={[
                    {
                      name: t("organization.tabs.groups.newGroup.language.EN"),
                      value: "EN",
                    },
                    {
                      name: t("organization.tabs.groups.newGroup.language.ES"),
                      value: "ES",
                    },
                  ]}
                  label={t("organization.tabs.groups.newGroup.language.text")}
                  name={"language"}
                  placeholder={t("organization.tabs.groups.newGroup.select")}
                  required={true}
                  tooltip={t(
                    "organization.tabs.groups.newGroup.language.tooltip",
                  )}
                />
              </Fragment>
            )}
          </InnerForm>
        </Form>
      </Modal>
    </Fragment>
  );
};

export { AddGroupModal };
