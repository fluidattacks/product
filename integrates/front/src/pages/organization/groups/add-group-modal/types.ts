interface IAddGroupModalProps {
  isOpen: boolean;
  organization: string;
  onClickOpen: () => void;
  onClose: () => void;
  runTour: boolean;
}

export type { IAddGroupModalProps };
