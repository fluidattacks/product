import { graphql } from "gql";

const ADD_GROUP_MUTATION = graphql(`
  mutation AddGroupMutation(
    $description: String!
    $groupName: String!
    $hasAdvanced: Boolean!
    $hasEssential: Boolean!
    $language: Language!
    $organizationName: String!
    $service: ServiceType!
    $subscription: SubscriptionType!
  ) {
    addGroup(
      description: $description
      groupName: $groupName
      hasAdvanced: $hasAdvanced
      hasEssential: $hasEssential
      language: $language
      organizationName: $organizationName
      service: $service
      subscription: $subscription
    ) {
      success
    }
  }
`);

export { ADD_GROUP_MUTATION };
