import { PureAbility } from "@casl/ability";
import { screen, waitFor } from "@testing-library/react";
import { userEvent } from "@testing-library/user-event";
import { GraphQLError } from "graphql/error";
import { HttpResponse, type StrictResponse, graphql } from "msw";
import { Route, Routes } from "react-router-dom";

import { ADD_GROUP_MUTATION } from "./add-group-modal/queries";
import {
  GET_ORGANIZATION_EVENTS,
  GET_ORGANIZATION_GROUPS,
  GET_ORGANIZATION_ROOTS,
} from "./queries";
import { VulnerabilityStatus } from "./types";
import type { IGroupData, IOrganizationGroupsProps } from "./types";
import { formatVulnState } from "./utils";

import { OrganizationGroups } from ".";
import { authContext } from "context/auth";
import { authzPermissionsContext } from "context/authz/config";
import type {
  AddGroupMutationMutation as AddGroup,
  GetOrgEventsQuery,
  GetOrganizationGroupsQuery as GetOrganizationGroups,
  GetOrganizationRootsQuery,
} from "gql/graphql";
import { ManagedType, ServiceType, TrialState } from "gql/graphql";
import { render } from "mocks";
import { LINK } from "mocks/constants";
import type { IErrorMessage } from "mocks/types";
import { msgError } from "utils/notifications";
import { translate } from "utils/translations/translate";

jest.mock("utils/notifications", (): Record<string, unknown> => {
  const mockedNotifications: Record<string, () => Record<string, unknown>> =
    jest.requireActual("utils/notifications");
  jest.spyOn(mockedNotifications, "msgError").mockImplementation();
  jest.spyOn(mockedNotifications, "msgSuccess").mockImplementation();

  return mockedNotifications;
});

describe("organization groups view", (): void => {
  const graphqlMocked = graphql.link(LINK);
  const memoryRouter = {
    initialEntries: ["/orgs/okada/groups"],
  };
  const mockProps: IOrganizationGroupsProps = {
    organizationId: "ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3",
  };

  const mockGroupData = {
    advanced: "",
    description: "",
    essential: "",
    eventFormat: "",
    events: [],
    hasAdvanced: false,
    hasEssential: true,
    name: "",
    plan: "",
    service: "",
    status: "",
    subscription: "",
    userRole: "",
    vulnerabilities: "",
  };

  it("should render a component", async (): Promise<void> => {
    expect.hasAssertions();

    const mockedPermissions = new PureAbility<string>([
      { action: "integrates_api_mutations_add_group_mutate" },
    ]);
    render(
      <Routes>
        <Route
          element={
            <authzPermissionsContext.Provider value={mockedPermissions}>
              <OrganizationGroups organizationId={mockProps.organizationId} />
            </authzPermissionsContext.Provider>
          }
          path={"/orgs/:organizationName/groups"}
        />
      </Routes>,
      {
        memoryRouter,
      },
    );

    await waitFor((): void => {
      expect(screen.queryAllByRole("table")).toHaveLength(1);
    });

    expect(screen.getAllByRole("row")).toHaveLength(4);
    expect(
      screen.queryByText("organization.tabs.groups.overview.title.text"),
    ).toBeInTheDocument();

    expect(
      screen.queryByText(
        "organization.tabs.groups.overview.coveredAuthors.title",
      ),
    ).toBeInTheDocument();
    expect(
      screen.queryByText(
        "organization.tabs.groups.overview.coveredRepositories.title",
      ),
    ).toBeInTheDocument();
    expect(
      screen.queryByText(
        "organization.tabs.groups.overview.missedAuthors.title",
      ),
    ).toBeInTheDocument();
    expect(
      screen.queryByText(
        "organization.tabs.groups.overview.missedRepositories.title",
      ),
    ).toBeInTheDocument();

    const UNIT_TESTING_ROW_AT = 1;

    expect(
      screen.getByRole("button", {
        name: /organization.tabs.groups.newGroup.new.text/u,
      }),
    ).toBeInTheDocument();
    expect(screen.getAllByRole("row")[2].textContent).toContain("Oneshottest");
    expect(screen.getAllByRole("row")[2].textContent).toContain(
      "OneshottestSubscribedOneshotLoading...One-shot type test groupuserModal.roles.groupManagerLoading...",
    );
    expect(screen.getAllByRole("row")[2].textContent).toContain("Oneshot");
    expect(screen.getAllByRole("row")[2].textContent).toContain(
      "OneshottestSubscribedOneshotLoading...One-shot type test groupuserModal.roles.groupManagerLoading...",
    );
    expect(screen.getAllByRole("row")[2].textContent).toContain(
      "One-shot type test group",
    );
    expect(screen.getAllByRole("row")[2].textContent).toContain(
      "userModal.roles.groupManager",
    );

    expect(screen.getAllByRole("row")[3].textContent).toContain("Pendinggroup");
    expect(screen.getAllByRole("row")[3].textContent).toContain(
      "PendinggroupSubscribedEssentialLoading...Continuous group for deletionuserModal.roles.customerManagerLoading...",
    );
    expect(screen.getAllByRole("row")[3].textContent).toContain("Essential");
    expect(screen.getAllByRole("row")[3].textContent).toContain(
      "PendinggroupSubscribedEssentialLoading...Continuous group for deletionuserModal.roles.customerManagerLoading...",
    );
    expect(screen.getAllByRole("row")[3].textContent).toContain(
      "Continuous group for deletion",
    );
    expect(screen.getAllByRole("row")[3].textContent).toContain(
      "userModal.roles.customerManager",
    );

    expect(
      screen.getAllByRole("row")[UNIT_TESTING_ROW_AT].textContent,
    ).toContain("Unittesting");
    expect(
      screen.getAllByRole("row")[UNIT_TESTING_ROW_AT].textContent,
    ).toContain("Advanced");
    expect(
      screen.getAllByRole("row")[UNIT_TESTING_ROW_AT].textContent,
    ).toContain("userModal.roles.user");

    const links = screen.getAllByRole("link");

    expect(links).toHaveLength(13);
    expect(links[0]).toHaveAttribute("href", "/orgs/okada/outside");

    expect(links[3]).toHaveAttribute(
      "href",
      "/orgs/okada/groups/unittesting/vulns",
    );
    expect(links[7]).toHaveAttribute(
      "href",
      "/orgs/okada/groups/oneshottest/vulns",
    );
    expect(links[11]).toHaveAttribute(
      "href",
      "/orgs/okada/groups/pendingGroup/vulns",
    );

    await userEvent.click(screen.getByRole("cell", { name: "Unittesting" }));
    jest.clearAllMocks();
  });

  it("should render a component error", async (): Promise<void> => {
    expect.hasAssertions();

    const mockedPermissions = new PureAbility<string>([
      { action: "api_mutations_add_group_mutate" },
    ]);
    render(
      <Routes>
        <Route
          element={
            <authzPermissionsContext.Provider value={mockedPermissions}>
              <OrganizationGroups
                organizationId={"ORG#5cc323c5-87de-4025-865d-a1a9f3dad1be"}
              />
            </authzPermissionsContext.Provider>
          }
          path={"/orgs/:organizationName/groups"}
        />
      </Routes>,
      {
        memoryRouter: {
          initialEntries: ["/orgs/nonokada/groups"],
        },
        mocks: [
          graphqlMocked.query(
            GET_ORGANIZATION_GROUPS,
            (): StrictResponse<{ data: GetOrganizationGroups }> => {
              return HttpResponse.json({
                data: {
                  organization: {
                    coveredAuthors: 2,
                    coveredRepositories: 1,
                    groups: [
                      {
                        closedVulnerabilities: 0,
                        description: "Continuous type test group",
                        hasAdvanced: true,
                        hasEssential: true,
                        managed: ManagedType.UnderReview,
                        name: "unittesting",
                        openFindings: 2,
                        openVulnerabilities: 0,
                        service: ServiceType.White,
                        subscription: "continuous",
                        userRole: "user",
                      },
                      {
                        closedVulnerabilities: 0,
                        description: "One-shot type test group",
                        hasAdvanced: true,
                        hasEssential: true,
                        managed: ManagedType.Managed,
                        name: "oneshottest",
                        openFindings: 1,
                        openVulnerabilities: 0,
                        service: ServiceType.White,
                        subscription: "oneshot",
                        userRole: "group_manager",
                      },
                    ],
                    missedAuthors: 3,
                    missedRepositories: 1,
                    name: "nonokada",
                    trial: null,
                  },
                },
              });
            },
          ),
          graphqlMocked.query(
            GET_ORGANIZATION_ROOTS,
            (): StrictResponse<
              IErrorMessage | { data: GetOrganizationRootsQuery }
            > => {
              return HttpResponse.json({
                data: {
                  organization: {
                    groups: [
                      {
                        name: "unittesting",
                        roots: null,
                      },
                      {
                        name: "oneshottest",
                        roots: [],
                      },
                    ],
                    name: "nonokada",
                  },
                },
                errors: [new GraphQLError("is_under_review")],
              });
            },
          ),
          graphqlMocked.query(
            GET_ORGANIZATION_EVENTS,
            (): StrictResponse<IErrorMessage | { data: GetOrgEventsQuery }> => {
              return HttpResponse.json({
                data: {
                  organization: {
                    groups: [
                      {
                        events: null,
                        name: "unittesting",
                      },
                      {
                        events: [],
                        name: "oneshottest",
                      },
                    ],
                    name: "nonokada",
                  },
                },
                errors: [new GraphQLError("is_under_review")],
              });
            },
          ),
        ],
      },
    );

    await waitFor((): void => {
      expect(screen.getAllByRole("row")).toHaveLength(3);
    });

    expect(screen.getAllByRole("row")[2].textContent).toContain("Oneshottest");
    expect(screen.getAllByRole("row")[2].textContent).toContain(
      "OneshottestSubscribedOneshotLoading...One-shot type test groupuserModal.roles.groupManagerLoading...",
    );
    expect(screen.getAllByRole("row")[2].textContent).toContain("Oneshot");
    expect(screen.getAllByRole("row")[2].textContent).toContain(
      "OneshottestSubscribedOneshotLoading...One-shot type test groupuserModal.roles.groupManagerLoading...",
    );
    expect(screen.getAllByRole("row")[2].textContent).toContain(
      "One-shot type test group",
    );
    expect(screen.getAllByRole("row")[2].textContent).toContain(
      "userModal.roles.groupManager",
    );

    expect(screen.getAllByRole("row")[1].textContent).toContain("Unittesting");
    expect(screen.getAllByRole("row")[1].textContent).toContain("Suspended");
    expect(screen.getAllByRole("row")[1].textContent).toContain(
      "userModal.roles.user",
    );

    await waitFor((): void => {
      expect(screen.getAllByRole("link")).toHaveLength(6);
    });
    const links = screen.getAllByRole("link");

    expect(links[0]).toHaveAttribute("href", "/orgs/nonokada/outside");
    expect(links[1]).toHaveAttribute("href", "/orgs/nonokada/billing");
    expect(links[2]).toHaveAttribute(
      "href",
      "/orgs/nonokada/groups/oneshottest",
    );
    expect(links[3]).toHaveAttribute(
      "href",
      "/orgs/nonokada/groups/oneshottest/scope",
    );
    expect(links[4]).toHaveAttribute(
      "href",
      "/orgs/nonokada/groups/oneshottest/vulns",
    );
    expect(links[5]).toHaveAttribute(
      "href",
      "/orgs/nonokada/groups/oneshottest/events",
    );

    await userEvent.click(screen.getByRole("cell", { name: "Oneshottest" }));
    jest.clearAllMocks();
  });

  it("should show an error", async (): Promise<void> => {
    expect.hasAssertions();

    render(
      <Routes>
        <Route
          element={
            <OrganizationGroups
              organizationId={"ORG#5cc323c5-87de-4025-865d-a1a9f3dad1be"}
            />
          }
          path={"/orgs/:organizationName/groups"}
        />
      </Routes>,
      {
        memoryRouter,
      },
    );

    await waitFor((): void => {
      expect(msgError).toHaveBeenCalledWith("groupAlerts.errorTextsad");
    });

    expect(
      screen.getByText("organization.tabs.groups.empty.title"),
    ).toBeInTheDocument();

    jest.clearAllMocks();
  });

  it("should add a new group", async (): Promise<void> => {
    expect.hasAssertions();

    const mockedPermissions = new PureAbility<string>([
      { action: "integrates_api_mutations_add_group_mutate" },
    ]);

    render(
      <authzPermissionsContext.Provider value={mockedPermissions}>
        <authContext.Provider
          value={{
            awsSubscription: null,
            setUser: jest.fn(),
            tours: {
              newGroup: true,
              newRoot: true,
              welcome: true,
            },
            userEmail: "",
            userName: "",
          }}
        >
          <Routes>
            <Route
              element={
                <OrganizationGroups organizationId={mockProps.organizationId} />
              }
              path={"/orgs/:organizationName/groups"}
            />
          </Routes>
        </authContext.Provider>
      </authzPermissionsContext.Provider>,
      {
        memoryRouter,
        mocks: [
          graphqlMocked.query(
            GET_ORGANIZATION_GROUPS,
            (): StrictResponse<{ data: GetOrganizationGroups }> => {
              return HttpResponse.json({
                data: {
                  organization: {
                    coveredAuthors: 2,
                    coveredRepositories: 1,
                    groups: [
                      {
                        closedVulnerabilities: 0,
                        description: "Continuous type test group",
                        hasAdvanced: true,
                        hasEssential: true,
                        managed: ManagedType.Managed,
                        name: "unittesting",
                        openFindings: 2,
                        openVulnerabilities: 0,
                        service: ServiceType.White,
                        subscription: "continuous",
                        userRole: "user",
                      },
                      {
                        closedVulnerabilities: 0,
                        description: "One-shot type test group",
                        hasAdvanced: true,
                        hasEssential: true,
                        managed: ManagedType.Managed,
                        name: "oneshottest",
                        openFindings: 1,
                        openVulnerabilities: 0,
                        service: ServiceType.White,
                        subscription: "oneshot",
                        userRole: "group_manager",
                      },
                    ],
                    missedAuthors: 3,
                    missedRepositories: 1,
                    name: "okada",
                    trial: null,
                  },
                },
              });
            },
            { once: true },
          ),
          graphqlMocked.query(
            GET_ORGANIZATION_ROOTS,
            (): StrictResponse<{ data: GetOrganizationRootsQuery }> => {
              return HttpResponse.json({
                data: {
                  organization: {
                    groups: [
                      {
                        name: "unittesting",
                        roots: [],
                      },
                      {
                        name: "oneshottest",
                        roots: [],
                      },
                    ],
                    name: "okada",
                  },
                },
              });
            },
            { once: true },
          ),
          graphqlMocked.query(
            GET_ORGANIZATION_EVENTS,
            (): StrictResponse<{ data: GetOrgEventsQuery }> => {
              return HttpResponse.json({
                data: {
                  organization: {
                    groups: [
                      {
                        events: [],
                        name: "unittesting",
                      },
                      {
                        events: [],
                        name: "oneshottest",
                      },
                    ],
                    name: "okada",
                  },
                },
              });
            },
            { once: true },
          ),
          graphqlMocked.mutation(
            ADD_GROUP_MUTATION,
            (): StrictResponse<{ data: AddGroup }> => {
              return HttpResponse.json({
                data: {
                  addGroup: {
                    success: true,
                  },
                },
              });
            },
            { once: true },
          ),
        ],
      },
    );
    const confirmBtn = "Confirm";

    const numberOfRows = 3;
    await waitFor((): void => {
      expect(screen.queryAllByRole("table")).toHaveLength(1);
    });

    expect(screen.getAllByRole("row")).toHaveLength(numberOfRows);

    await userEvent.click(
      screen.getByText("organization.tabs.groups.newGroup.new.text"),
    );

    await waitFor((): void => {
      expect(
        screen.getByText("organization.tabs.groups.newGroup.new.group"),
      ).toBeInTheDocument();
    });

    await userEvent.type(
      screen.getByRole("textbox", { name: "name" }),
      "AKAME",
    );
    await userEvent.type(
      screen.getByRole("textbox", { name: "description" }),
      "Test group",
    );

    await waitFor((): void => {
      expect(screen.getByText(confirmBtn)).toBeEnabled();
    });

    await userEvent.click(screen.getByText(confirmBtn));

    await waitFor(
      (): void => {
        expect(screen.queryAllByRole("row")).toHaveLength(4);
      },
      { timeout: 2000 },
    );

    jest.clearAllMocks();
  });

  it("should show upgrade modal when free trial user tries to create a second group", async (): Promise<void> => {
    expect.hasAssertions();

    const mockedPermissions = new PureAbility<string>([
      { action: "integrates_api_mutations_add_group_mutate" },
    ]);

    render(
      <authzPermissionsContext.Provider value={mockedPermissions}>
        <authContext.Provider
          value={{
            awsSubscription: null,
            setUser: jest.fn(),
            tours: {
              newGroup: true,
              newRoot: true,
              welcome: true,
            },
            userEmail: "",
            userName: "",
          }}
        >
          <Routes>
            <Route
              element={
                <OrganizationGroups organizationId={mockProps.organizationId} />
              }
              path={"/orgs/:organizationName/groups"}
            />
          </Routes>
        </authContext.Provider>
      </authzPermissionsContext.Provider>,
      {
        memoryRouter,
        mocks: [
          graphqlMocked.query(
            GET_ORGANIZATION_GROUPS,
            (): StrictResponse<{ data: GetOrganizationGroups }> => {
              return HttpResponse.json({
                data: {
                  organization: {
                    coveredAuthors: 0,
                    coveredRepositories: 0,
                    groups: [
                      {
                        closedVulnerabilities: 0,
                        description: "Trial group",
                        hasAdvanced: false,
                        hasEssential: true,
                        managed: ManagedType.Trial,
                        name: "trialgroup",
                        openFindings: 2,
                        openVulnerabilities: 0,
                        service: ServiceType.White,
                        subscription: "continuous",
                        userRole: "group_manager",
                      },
                    ],
                    missedAuthors: 0,
                    missedRepositories: 0,
                    name: "okada",
                    trial: {
                      completed: false,
                      extensionDate: "",
                      extensionDays: 0,
                      startDate: "2025-02-21 15:50:30.904072+00:00",
                      state: TrialState.Trial,
                    },
                  },
                },
              });
            },
            { once: true },
          ),
          graphqlMocked.query(
            GET_ORGANIZATION_ROOTS,
            (): StrictResponse<{ data: GetOrganizationRootsQuery }> => {
              return HttpResponse.json({
                data: {
                  organization: {
                    groups: [
                      {
                        name: "trialgroup",
                        roots: [],
                      },
                    ],
                    name: "okada",
                  },
                },
              });
            },
            { once: true },
          ),
          graphqlMocked.query(
            GET_ORGANIZATION_EVENTS,
            (): StrictResponse<{ data: GetOrgEventsQuery }> => {
              return HttpResponse.json({
                data: {
                  organization: {
                    groups: [
                      {
                        events: [],
                        name: "trialgroup",
                      },
                    ],
                    name: "okada",
                  },
                },
              });
            },
            { once: true },
          ),
        ],
      },
    );

    const numberOfRows = 2;
    await waitFor((): void => {
      expect(screen.queryAllByRole("table")).toHaveLength(1);
    });

    expect(screen.getAllByRole("row")).toHaveLength(numberOfRows);

    await userEvent.click(
      screen.getByText("organization.tabs.groups.newGroup.new.text"),
    );

    await waitFor((): void => {
      expect(
        screen.getByText("organization.tabs.groups.newGroup.trial.title"),
      ).toBeInTheDocument();
    });

    jest.clearAllMocks();
  });

  it("should add a new group from empty page", async (): Promise<void> => {
    expect.hasAssertions();

    const mockedPermissions = new PureAbility<string>([
      { action: "integrates_api_mutations_add_group_mutate" },
    ]);

    render(
      <authzPermissionsContext.Provider value={mockedPermissions}>
        <authContext.Provider
          value={{
            awsSubscription: null,
            setUser: jest.fn(),
            tours: {
              newGroup: true,
              newRoot: true,
              welcome: true,
            },
            userEmail: "",
            userName: "",
          }}
        >
          <Routes>
            <Route
              element={
                <OrganizationGroups organizationId={mockProps.organizationId} />
              }
              path={"/orgs/:organizationName/groups"}
            />
          </Routes>
        </authContext.Provider>
      </authzPermissionsContext.Provider>,
      {
        memoryRouter,
        mocks: [
          graphqlMocked.query(
            GET_ORGANIZATION_GROUPS,
            (): StrictResponse<{ data: GetOrganizationGroups }> => {
              return HttpResponse.json({
                data: {
                  organization: {
                    coveredAuthors: 2,
                    coveredRepositories: 1,
                    groups: [],
                    missedAuthors: 3,
                    missedRepositories: 1,
                    name: "okada",
                    trial: null,
                  },
                },
              });
            },
            { once: true },
          ),
          graphqlMocked.query(
            GET_ORGANIZATION_ROOTS,
            (): StrictResponse<{ data: GetOrganizationRootsQuery }> => {
              return HttpResponse.json({
                data: {
                  organization: {
                    groups: [],
                    name: "okada",
                  },
                },
              });
            },
            { once: true },
          ),
          graphqlMocked.query(
            GET_ORGANIZATION_EVENTS,
            (): StrictResponse<{ data: GetOrgEventsQuery }> => {
              return HttpResponse.json({
                data: {
                  organization: {
                    groups: [],
                    name: "okada",
                  },
                },
              });
            },
            { once: true },
          ),
          graphqlMocked.mutation(
            ADD_GROUP_MUTATION,
            (): StrictResponse<{ data: AddGroup }> => {
              return HttpResponse.json({
                data: {
                  addGroup: {
                    success: true,
                  },
                },
              });
            },
            { once: true },
          ),
        ],
      },
    );
    const confirmBtn = "Confirm";

    await waitFor((): void => {
      expect(screen.queryAllByRole("table")).toHaveLength(0);
    });

    await waitFor((): void => {
      expect(
        screen.queryByText("organization.tabs.groups.empty.button"),
      ).toBeInTheDocument();
    });

    await userEvent.click(
      screen.getByText("organization.tabs.groups.empty.button"),
    );

    await waitFor((): void => {
      expect(
        screen.getByText("organization.tabs.groups.newGroup.new.group"),
      ).toBeInTheDocument();
    });

    await userEvent.type(
      screen.getByRole("textbox", { name: "name" }),
      "AKAME",
    );
    await userEvent.type(
      screen.getByRole("textbox", { name: "description" }),
      "Test group",
    );

    await waitFor((): void => {
      expect(screen.getByText(confirmBtn)).toBeEnabled();
    });

    await userEvent.click(screen.getByText(confirmBtn));

    await waitFor(
      (): void => {
        expect(screen.queryAllByRole("row")).toHaveLength(4);
      },
      { timeout: 2000 },
    );

    jest.clearAllMocks();
  });

  it("should return CLONING when all Git roots are cloning", (): void => {
    expect.hasAssertions();

    const group: IGroupData = {
      ...mockGroupData,
      closedVulnerabilities: 0,
      managed: "MANAGED",
      openFindings: 0,
      openVulnerabilities: 0,
      roots: [
        {
          __typename: "GitRoot",
          cloningStatus: { status: "CLONING" },
          machineStatus: null,
        },
        {
          __typename: "GitRoot",
          cloningStatus: { status: "CLONING" },
          machineStatus: null,
        },
      ],
    };

    const result = formatVulnState(group);

    expect(result).toBe(VulnerabilityStatus.CLONING);
  });

  it("should return NO_ROOT when no roots are found", (): void => {
    expect.hasAssertions();

    const group: IGroupData = {
      ...mockGroupData,
      closedVulnerabilities: 0,
      managed: "MANAGED",
      openFindings: 0,
      openVulnerabilities: 0,
      roots: [],
    };

    const result = formatVulnState(group);

    expect(result).toBe(VulnerabilityStatus.NO_ROOT);
  });

  it("should return NO_VULNERABILITIES", (): void => {
    expect.hasAssertions();

    const group: IGroupData = {
      ...mockGroupData,
      closedVulnerabilities: 0,
      managed: "MANAGED",
      openFindings: 0,
      openVulnerabilities: 0,
      roots: [
        {
          __typename: "GitRoot",
          cloningStatus: { status: "OK" },
          machineStatus: {
            modifiedDate: "2023-08-08T00:00:00+00:00",
            status: "SUCCESS",
          },
        },
      ],
    };

    const result = formatVulnState(group);

    expect(result).toBe(VulnerabilityStatus.NO_VULNERABILITIES);

    jest.restoreAllMocks();
  });

  it("should return IN_PROCESS", (): void => {
    expect.hasAssertions();

    const group: IGroupData = {
      ...mockGroupData,
      closedVulnerabilities: 0,
      managed: "MANAGED",
      openFindings: 0,
      openVulnerabilities: 0,
      roots: [
        {
          __typename: "GitRoot",
          cloningStatus: { status: "OK" },
          machineStatus: null,
        },
      ],
    };

    const result = formatVulnState(group);

    expect(result).toBe(VulnerabilityStatus.IN_PROCESS);
  });

  it("should return CLONING_ERROR", (): void => {
    expect.hasAssertions();

    const group: IGroupData = {
      ...mockGroupData,
      closedVulnerabilities: 0,
      managed: "MANAGED",
      openFindings: 0,
      openVulnerabilities: 0,
      roots: [
        {
          __typename: "GitRoot",
          cloningStatus: { status: "FAILED" },
          machineStatus: null,
        },
      ],
    };

    const result = formatVulnState(group);

    expect(result).toBe(VulnerabilityStatus.CLONING_ERROR);
  });

  it("should return vulnerability count when open findings exist", (): void => {
    expect.hasAssertions();

    const group: IGroupData = {
      ...mockGroupData,
      closedVulnerabilities: 0,
      managed: "MANAGED",
      openFindings: 5,
      openVulnerabilities: 0,
      roots: [
        {
          __typename: "GitRoot",
          cloningStatus: { status: "OK" },
          machineStatus: null,
        },
      ],
    };

    const result = formatVulnState(group);

    expect(result).toBe(
      translate.t("organization.tabs.groups.vulnerabilities.open", {
        openFindings: group.openFindings,
      }),
    );
  });

  it("should return IN_PROCESS when is black and have git roots", (): void => {
    expect.hasAssertions();

    const group: IGroupData = {
      ...mockGroupData,
      closedVulnerabilities: 0,
      managed: "MANAGED",
      openFindings: 0,
      openVulnerabilities: 0,
      roots: [
        {
          __typename: "GitRoot",
          cloningStatus: { status: "CLONING" },
          machineStatus: null,
        },
      ],
      service: "BLACK",
    };

    const result = formatVulnState(group);

    expect(result).toBe(VulnerabilityStatus.IN_PROCESS);
  });

  it("should filter by user input", async (): Promise<void> => {
    expect.hasAssertions();

    render(
      <Routes>
        <Route
          element={
            <OrganizationGroups organizationId={mockProps.organizationId} />
          }
          path={"/orgs/:organizationName/groups"}
        />
      </Routes>,
      { memoryRouter },
    );

    const numberOfRows = 4;
    await waitFor((): void => {
      expect(screen.queryAllByRole("table")).toHaveLength(1);
    });

    expect(screen.getAllByRole("row")).toHaveLength(numberOfRows);

    const filter = document.querySelector("#filterBtn");

    const groupName = "unittesting";

    expect(filter).toBeInTheDocument();

    if (filter) await userEvent.click(filter);
    await userEvent.click(
      screen.getByTestId(
        "li-filter-option-organization.tabs.groups.newGroup.name.text",
      ),
    );
    const textarea = screen.getByRole("textbox", {
      name: "text.name-caseInsensitive",
    });
    await userEvent.type(textarea, groupName);
    await userEvent.click(screen.getByText("Apply"));
    await userEvent.click(screen.getByRole("button", { name: "modal-close" }));

    const numberOfRowAfterFiltering = 2;

    expect(screen.getAllByRole("row")).toHaveLength(numberOfRowAfterFiltering);
    expect(screen.getByRole("link", { name: "Unittesting" })).toHaveAttribute(
      "href",
      "/orgs/okada/groups/unittesting",
    );

    // Clear filter by closing filter tag
    const xMark = screen.getByTestId("xmark-icon");
    await userEvent.click(xMark.parentElement ?? xMark);

    await waitFor((): void => {
      expect(screen.getAllByRole("row")).toHaveLength(numberOfRows);
    });
  });
});
