import dayjs from "dayjs";
import timezone from "dayjs/plugin/timezone";
import utc from "dayjs/plugin/utc";
import _ from "lodash";
import { create } from "zustand";

import type {
  IAddGroupModalStore,
  IGitRootAttr,
  IGroupData,
  IGroupDataResult,
  IGroupEventsDataResult,
  IGroupRootsDataResult,
  ITrialData,
  TRoot,
} from "./types";
import { VulnerabilityStatus } from "./types";

import {
  formatInProcessHandler,
  formatLinkHandler,
} from "components/table/table-formatters";
import { getTrialRemainingDays } from "utils/get-trial-remaining-days";
import { translate } from "utils/translations/translate";

dayjs.extend(utc);
dayjs.extend(timezone);

const getTrialTip = (trial: ITrialData | null | undefined): string => {
  const trialDays = trial ? getTrialRemainingDays(trial) : 0;

  if (trialDays > 0) {
    return translate.t(`organization.tabs.groups.status.trialDaysTip`, {
      remainingDays: trialDays,
    });
  }

  return translate.t(`organization.tabs.groups.status.trialTip`);
};

const getPlan = (group: IGroupDataResult): string => {
  const subscription = _.capitalize(group.subscription);

  if (subscription === "Oneshot") {
    return subscription;
  } else if (group.hasAdvanced) {
    return "Advanced";
  }

  return "Essential";
};

const getEventFormat = (group: IGroupDataResult): string => {
  if (_.isUndefined(group.events)) {
    return "None";
  }
  const eventsCreated = group.events.filter((event): boolean =>
    event.eventStatus.includes("CREATED"),
  );

  return eventsCreated.length > 0
    ? `${eventsCreated.length} need(s) attention`
    : "None";
};

type TCloningType = IGitRootAttr[] | null | undefined;

const isGitRootCloning = (gitRoots: TCloningType): boolean => {
  if (_.isNil(gitRoots) || _.isEmpty(gitRoots)) return false;

  return gitRoots.every(
    (root): boolean => root.cloningStatus.status === "CLONING",
  );
};

const hasFailedGitRootCloning = (
  gitRoots: IGitRootAttr[] | null | undefined,
): boolean => {
  if (_.isNil(gitRoots) || _.isEmpty(gitRoots)) return false;

  return gitRoots.every(
    (root): boolean => root.cloningStatus.status === "FAILED",
  );
};

const noVulnerabilitiesFound = (
  gitRoots: IGitRootAttr[] | null | undefined,
  group: IGroupDataResult,
): boolean => {
  if (
    _.isNil(gitRoots) ||
    _.isEmpty(gitRoots) ||
    getPlan(group) !== "Essential" ||
    Boolean(group.openVulnerabilities ?? false) ||
    Boolean(group.closedVulnerabilities ?? false)
  )
    return false;

  if (group.managed === "UNDER_REVIEW" && !group.openFindings) return true;

  if (!group.hasEssential || group.openFindings) return false;

  const [{ machineStatus }] = gitRoots;
  if (machineStatus?.status === "SUCCESS") return true;

  const machineExecutionDate = machineStatus?.modifiedDate ?? dayjs().format();
  const limitTime = dayjs(machineExecutionDate).add(2, "hour");

  return dayjs().isAfter(limitTime);
};

const noRootFound = (roots: TRoot[] | null | undefined): boolean => {
  return _.isNil(roots) || _.isEmpty(roots);
};

const formatVulnState = (group: IGroupDataResult): string => {
  const roots =
    group.service === "BLACK"
      ? []
      : group.roots.filter(
          (root): root is IGitRootAttr => root.__typename === "GitRoot",
        );

  if (isGitRootCloning(roots)) return VulnerabilityStatus.CLONING;

  if (group.openFindings)
    return translate.t("organization.tabs.groups.vulnerabilities.open", {
      openFindings: group.openFindings,
    });

  if (noRootFound(group.roots)) return VulnerabilityStatus.NO_ROOT;

  if (hasFailedGitRootCloning(roots)) return VulnerabilityStatus.CLONING_ERROR;

  if (noVulnerabilitiesFound(roots, group))
    return VulnerabilityStatus.NO_VULNERABILITIES;

  return VulnerabilityStatus.IN_PROCESS;
};

const formatGroupData = (
  groupData: IGroupDataResult[],
  rootsData: IGroupRootsDataResult[] | undefined,
  eventsData: IGroupEventsDataResult[] | undefined,
): IGroupData[] => {
  return groupData.map((group): IGroupData => {
    const description = _.capitalize(group.description);
    const plan = getPlan(group);
    const vulnerabilities = formatVulnState({
      ...group,
      roots:
        rootsData?.find((value): boolean => value.name === group.name)?.roots ??
        [],
    });
    const eventFormat = getEventFormat({
      ...group,
      events:
        eventsData?.find((value): boolean => value.name === group.name)
          ?.events ?? [],
    });
    const status = translate.t(
      `organization.tabs.groups.status.${_.camelCase(group.managed)}`,
    );

    return {
      ...group,
      description,
      eventFormat,
      plan,
      status,
      vulnerabilities,
    };
  });
};

const formatVulnerabilityCell = (
  text: string,
  linkBody: string,
): JSX.Element => {
  switch (text) {
    case VulnerabilityStatus.CLONING.toString():
      return formatInProcessHandler(
        translate.t("organization.tabs.groups.vulnerabilities.cloning"),
      );
    case VulnerabilityStatus.IN_PROCESS.toString():
      return formatInProcessHandler(
        translate.t("organization.tabs.groups.vulnerabilities.inProcess"),
      );
    case VulnerabilityStatus.NO_ROOT.toString():
      return formatLinkHandler(
        `${linkBody}/scope`,
        translate.t("organization.tabs.groups.vulnerabilities.noRoot"),
        false,
        "",
        true,
      );
    case VulnerabilityStatus.CLONING_ERROR.toString():
      return formatLinkHandler(
        `${linkBody}/scope`,
        translate.t("organization.tabs.groups.vulnerabilities.cloningError"),
        false,
        "",
        true,
      );
    case VulnerabilityStatus.NO_VULNERABILITIES.toString():
      return formatLinkHandler(
        `${linkBody}/scope`,
        translate.t(
          "organization.tabs.groups.vulnerabilities.noVulnerabilities",
        ),
        false,
        "",
        true,
      );
    default:
      return formatLinkHandler(`${linkBody}/vulns`, text);
  }
};

const useAddGroupModalStore = create<IAddGroupModalStore>()(
  (set): IAddGroupModalStore => ({
    handleAddGroupModal: (): void => {
      set(
        (state): Partial<IAddGroupModalStore> => ({
          isAddGroupModalOpen: !state.isAddGroupModalOpen,
        }),
      );
    },
    handleAddGroupTour: (): void => {
      set(
        (state): Partial<IAddGroupModalStore> => ({
          isAddGroupTourOpen: !state.isAddGroupTourOpen,
        }),
      );
    },
    isAddGroupModalOpen: false,
    isAddGroupTourOpen: false,
  }),
);

export {
  formatGroupData,
  formatVulnState,
  getTrialTip,
  getPlan,
  isGitRootCloning,
  hasFailedGitRootCloning,
  noVulnerabilitiesFound,
  formatVulnerabilityCell,
  noRootFound,
  useAddGroupModalStore,
};
