import { useAbility } from "@casl/react";
import {
  AppliedFilters,
  Button,
  Container,
  EmptyState,
  Loading,
  PremiumFeature,
  Tour,
  baseStep,
} from "@fluidattacks/design";
import type { IButtonProps } from "@fluidattacks/design/dist/components/empty-state/types";
import type { ColumnDef } from "@tanstack/react-table";
import { camelCase, capitalize } from "lodash";
import { useCallback, useState } from "react";
import * as React from "react";
import { useTranslation } from "react-i18next";
import { useParams } from "react-router-dom";
import { useTheme } from "styled-components";

import { AddGroupModal } from "./add-group-modal";
import { useOrganizationGroupsQuery } from "./hooks";
import { OrganizationGroupOverview } from "./overview";
import {
  formatVulnerabilityCell,
  getTrialTip,
  useAddGroupModalStore,
} from "./utils";

import { useOrgUrl } from "../hooks";
import { SectionHeader } from "components/section-header";
import { Table } from "components/table";
import { formatLinkHandler } from "components/table/table-formatters";
import { Can } from "context/authz/can";
import { authzPermissionsContext } from "context/authz/config";
import { EventBar } from "features/event-bar";
import { useGroupsFilters } from "features/groups/filters-new";
import { UpgradeFreeTrialModal } from "features/upgrade-free-trial-users";
import { ManagedType } from "gql/graphql";
import { useCalendly, useTable, useTour } from "hooks";
import type {
  IGroupData,
  IOrganizationGroupsProps,
  ITrialData,
} from "pages/organization/groups/types";

const OrganizationGroups: React.FC<IOrganizationGroupsProps> = ({
  organizationId,
}): JSX.Element => {
  const theme = useTheme();
  const { organizationName } = useParams() as { organizationName: string };
  const { t } = useTranslation();
  const { url: orgUrl } = useOrgUrl();
  const tableRef = useTable("tblGroups");
  const { tours, setCompleted } = useTour();
  const enableTour = !tours.newGroup;
  const [filteredDataset, setFilteredDataset] = useState<IGroupData[]>([]);
  const {
    handleAddGroupModal,
    handleAddGroupTour,
    isAddGroupModalOpen,
    isAddGroupTourOpen,
  } = useAddGroupModalStore();
  const { closeUpgradeModal, isUpgradeOpen, openUpgradeModal } = useCalendly();

  const permissions = useAbility(authzPermissionsContext);
  const canAddGroup = permissions.can(
    "integrates_api_mutations_add_group_mutate",
  );
  const newGroupFreeTrial = "integrates/plans/group-paid-accounts";
  const {
    eventsLoading,
    groups,
    loading,
    organization,
    refetchGroups,
    rootsLoading,
  } = useOrganizationGroupsQuery(organizationId);
  const openNewGroupModal = useCallback((): void => {
    if (enableTour) {
      handleAddGroupTour();
    }
    if (groups[0]?.managed === ManagedType.Trial) {
      openUpgradeModal();
    } else {
      handleAddGroupModal();
    }
  }, [
    enableTour,
    handleAddGroupModal,
    handleAddGroupTour,
    openUpgradeModal,
    groups,
  ]);

  const closeNewGroupModal = useCallback((): void => {
    if (enableTour) {
      setCompleted("newGroup");
      handleAddGroupTour();
    }
    handleAddGroupModal();
    void refetchGroups();
  }, [
    enableTour,
    handleAddGroupModal,
    handleAddGroupTour,
    refetchGroups,
    setCompleted,
  ]);

  const { Filters, options, removeFilter } = useGroupsFilters({
    dataset: groups,
    setFilteredDataset,
  });

  const tableHeaders: ColumnDef<IGroupData>[] = [
    {
      accessorKey: "name",
      cell: (cell): JSX.Element | string => {
        const groupName = String(cell.getValue()).toLowerCase();
        const isSuspended =
          cell.row.original.managed === ManagedType.UnderReview;
        const link = `${orgUrl}/groups/${groupName}`;
        const text = cell.getValue<string>();

        return isSuspended ? capitalize(text) : formatLinkHandler(link, text);
      },
      header: t("organization.tabs.groups.newGroup.name.text"),
    },
    {
      accessorKey: "status",
      cell: (cell): JSX.Element => {
        const text = cell.getValue<string>();
        const showTrialTip =
          text === t(`organization.tabs.groups.status.trial`);
        const showSuspendedTip =
          cell.row.original.managed === ManagedType.UnderReview;
        const link = showSuspendedTip
          ? `/orgs/${organizationName}/billing`
          : `${orgUrl}/groups/${String(cell.row.getValue("name"))}/scope`;

        const infoTip = showTrialTip
          ? getTrialTip(organization?.trial as ITrialData | undefined)
          : t(`organization.tabs.groups.status.underReviewTip`);

        return formatLinkHandler(
          link,
          text,
          showTrialTip || showSuspendedTip,
          infoTip,
        );
      },
      header: t("organization.tabs.groups.status.header"),
    },
    {
      accessorKey: "plan",
      header: t("organization.tabs.groups.plan"),
    },
    {
      accessorKey: "vulnerabilities",
      cell: (cell): JSX.Element | string => {
        const isSuspended =
          cell.row.original.managed === ManagedType.UnderReview;
        const link = `${orgUrl}/groups/${String(cell.row.getValue("name"))}`;
        const text = rootsLoading ? "Loading..." : cell.getValue<string>();

        return isSuspended ? text : formatVulnerabilityCell(text, link);
      },
      header: t("organization.tabs.groups.vulnerabilities.header"),
    },
    {
      accessorKey: "description",
      header: t("organization.tabs.groups.newGroup.description.text"),
    },
    {
      accessorKey: "userRole",
      cell: (cell): string => {
        return t(`userModal.roles.${camelCase(String(cell.getValue()))}`, {
          defaultValue: "-",
        });
      },
      header: t("organization.tabs.groups.role"),
    },
    {
      accessorKey: "eventFormat",
      cell: (cell): JSX.Element | string => {
        const isSuspended =
          cell.row.original.managed === ManagedType.UnderReview;
        const link = `${orgUrl}/groups/${String(cell.row.getValue("name"))}/events`;

        const text = eventsLoading ? "Loading..." : cell.getValue<string>();

        return isSuspended ? text : formatLinkHandler(link, text);
      },
      header: t("organization.tabs.groups.newGroup.events.text"),
    },
  ];

  const getAddGroupButton = useCallback((): IButtonProps | undefined => {
    if (canAddGroup) {
      return {
        onClick: openNewGroupModal,
        text: t("organization.tabs.groups.empty.button"),
      };
    }

    return undefined;
  }, [canAddGroup, openNewGroupModal, t]);

  if (loading) {
    return (
      <Container
        alignItems={"center"}
        display={"flex"}
        height={"100%"}
        justify={"center"}
      >
        <Loading color={"red"} label={"Loading..."} size={"lg"} />
      </Container>
    );
  }

  return (
    <React.Fragment>
      <SectionHeader header={t("organization.tabs.groups.header")} />
      {groups.length === 0 ? (
        <EmptyState
          confirmButton={getAddGroupButton()}
          description={t("organization.tabs.groups.empty.description")}
          imageSrc={"integrates/empty/groupIcon"}
          title={t("organization.tabs.groups.empty.title")}
        />
      ) : (
        <React.Fragment>
          {organization ? (
            <OrganizationGroupOverview
              coveredAuthors={organization.coveredAuthors}
              coveredRepositories={organization.coveredRepositories}
              missedAuthors={organization.missedAuthors}
              missedRepositories={organization.missedRepositories}
              organizationName={organization.name}
            />
          ) : undefined}
          <Container
            bgColor={theme.palette.gray[50]}
            height={"100%"}
            id={"users"}
            px={1.25}
            py={1.25}
          >
            <EventBar organizationName={organizationName} pb={1.25} />
            <Table
              columns={tableHeaders}
              data={filteredDataset}
              dataPrivate={false}
              filters={<Filters />}
              filtersApplied={
                <AppliedFilters onClose={removeFilter} options={options} />
              }
              loadingData={loading}
              rightSideComponents={
                <Can do={"integrates_api_mutations_add_group_mutate"}>
                  <Button
                    icon={"plus"}
                    id={t("organization.tabs.groups.newGroup.new.id")}
                    onClick={openNewGroupModal}
                    tooltip={t("organization.tabs.groups.newGroup.new.tooltip")}
                  >
                    {t("organization.tabs.groups.newGroup.new.text")}
                    {groups[0]?.managed === ManagedType.Trial && (
                      <PremiumFeature />
                    )}
                  </Button>
                </Can>
              }
              tableRef={tableRef}
            />
          </Container>
        </React.Fragment>
      )}
      {isAddGroupModalOpen ? (
        <AddGroupModal
          isOpen={isAddGroupModalOpen}
          onClickOpen={handleAddGroupModal}
          onClose={closeNewGroupModal}
          organization={organizationName}
          runTour={enableTour}
        />
      ) : undefined}
      {isAddGroupTourOpen ? (
        <Tour
          run={false}
          steps={[
            {
              ...baseStep,
              content: t("tours.addGroup.addButton"),
              disableBeacon: true,
              hideFooter: true,
              target: "#add-group",
            },
          ]}
        />
      ) : undefined}
      {isUpgradeOpen && (
        <UpgradeFreeTrialModal
          description={t("organization.tabs.groups.newGroup.trial.description")}
          img={newGroupFreeTrial}
          onClose={closeUpgradeModal}
          title={t("organization.tabs.groups.newGroup.trial.title")}
        />
      )}
    </React.Fragment>
  );
};

export { OrganizationGroups };
