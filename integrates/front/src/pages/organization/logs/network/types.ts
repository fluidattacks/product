interface IZtnaNetworkLogData {
  date: string;
  email: string;
}

export type { IZtnaNetworkLogData };
