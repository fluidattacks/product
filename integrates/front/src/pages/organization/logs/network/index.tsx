import { Container } from "@fluidattacks/design";
import type { ColumnDef } from "@tanstack/react-table";
import dayjs from "dayjs";
import { useEffect, useState } from "react";
import * as React from "react";
import { useTranslation } from "react-i18next";
import { useTheme } from "styled-components";

import { useNetworkLogsQuery } from "./hooks";
import type { IZtnaNetworkLogData } from "./types";

import { Buttons } from "../action-buttons";
import type { IZtnaLogsProps } from "../types";
import { Filters } from "components/filter";
import type { IFilter, IPermanentData } from "components/filter/types";
import { Table } from "components/table";
import { Can } from "context/authz/can";
import { ZtnaLogType } from "gql/graphql";
import { setFiltersUtil, useStoredState, useTable } from "hooks";

const OrganizationZtnaNetworkLogs: React.FC<IZtnaLogsProps> = ({
  organizationId,
}): JSX.Element => {
  const { t } = useTranslation();
  const theme = useTheme();
  const tableRef = useTable("tblZtnaNetworkLogs");
  const [filteredDataset, setFilteredDataset] = useState<IZtnaNetworkLogData[]>(
    [],
  );
  const [filters, setFilters] = useState<IFilter<IZtnaNetworkLogData>[]>([
    {
      id: "date",
      key: "date",
      label: t("organization.tabs.ztna.tabs.network.date"),
      rangeValues: [
        dayjs().subtract(1, "days").format("YYYY-MM-DDTHH:mmZ"),
        "",
      ],
      type: "dateRange",
    },
    {
      filterFn: "includesInsensitive",
      id: "email",
      key: "email",
      label: t("organization.tabs.ztna.tabs.network.email"),
      type: "text",
    },
  ]);
  const [filtersPermaset, setFiltersPermaset] = useStoredState<
    IPermanentData[]
  >(
    "tblZtnaLogsNetworkFilters",
    filters.map((filter): IPermanentData => {
      switch (filter.type) {
        case "dateRange":
          return {
            id: filter.id,
            rangeValues: filter.rangeValues,
            valueType: "rangeValues",
          };
        case "numberRange":
          return {
            id: filter.id,
            numberRangeValues: filter.numberRangeValues,
            valueType: "numberRangeValues",
          };
        case undefined:
        case "number":
        case "checkboxes":
        case "select":
        case "switch":
        case "tags":
        case "text":
        default:
          return { id: filter.id, value: filter.value, valueType: "value" };
      }
    }),
    localStorage,
  );

  const tableHeaders: ColumnDef<IZtnaNetworkLogData>[] = [
    {
      accessorKey: "date",
      header: t("organization.tabs.ztna.tabs.network.date"),
    },
    {
      accessorKey: "email",
      header: t("organization.tabs.ztna.tabs.network.email"),
    },
  ];

  const startDate =
    filtersPermaset[0]?.rangeValues?.[0] === ""
      ? dayjs().subtract(1, "days").format("YYYY-MM-DDTZ")
      : dayjs(filtersPermaset[0]?.rangeValues?.[0]).format("YYYY-MM-DDTHHZ");
  const endDate =
    filtersPermaset[0]?.rangeValues?.[1] === ""
      ? undefined
      : dayjs(filtersPermaset[0]?.rangeValues?.[1]).format("YYYY-MM-DDTHHZ");
  const { networkLogs } = useNetworkLogsQuery(
    organizationId,
    startDate,
    endDate,
  );

  useEffect((): void => {
    setFilteredDataset(setFiltersUtil(networkLogs, filters));
  }, [filters, networkLogs, setFilteredDataset]);

  return (
    <Container
      bgColor={theme.palette.gray[50]}
      height={"100hv"}
      px={1.25}
      py={1.25}
    >
      <Table
        columns={tableHeaders}
        data={filteredDataset}
        extraButtons={
          <Can
            do={"integrates_api_resolvers_query_ztna_logs_report_url_resolve"}
          >
            <Buttons
              logType={ZtnaLogType.Network}
              organizationId={organizationId}
            />
          </Can>
        }
        filters={
          <Filters
            filters={filters}
            permaset={[filtersPermaset, setFiltersPermaset]}
            setFilters={setFilters}
          />
        }
        tableRef={tableRef}
      />
    </Container>
  );
};

export { OrganizationZtnaNetworkLogs };
