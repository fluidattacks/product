import { useQuery } from "@apollo/client";
import { useEffect, useMemo } from "react";
import { useTranslation } from "react-i18next";

import { GET_ORGANIZATION_ZTNA_HTTP_LOGS } from "./queries";
import type { IZtnaHttpLogData } from "./types";

import { useAudit } from "hooks/use-audit";
import { Logger } from "utils/logger";
import { msgError } from "utils/notifications";

interface IUseHttpLogsQuery {
  httpLogs: IZtnaHttpLogData[];
}

const useHttpLogsQuery = (
  organizationId: string,
  startDate: string,
  endDate: string | undefined,
): IUseHttpLogsQuery => {
  const { addAuditEvent } = useAudit();
  const { t } = useTranslation();

  const { data, fetchMore } = useQuery(GET_ORGANIZATION_ZTNA_HTTP_LOGS, {
    onCompleted: (): void => {
      addAuditEvent("Organization.HttpLogs", organizationId);
    },
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error): void => {
        msgError(t("groupAlerts.errorTextsad"));
        Logger.error(
          "An error occurred loading organization ztna http logs",
          error,
        );
      });
    },
    variables: { after: "", endDate, organizationId, startDate },
  });

  // REFAC NEEDED: Implement proper server-side pagination with the table
  useEffect((): void => {
    if (data !== undefined) {
      if (data.organization.ztnaHttpLogs?.pageInfo.hasNextPage === true) {
        void fetchMore({
          variables: {
            after: data.organization.ztnaHttpLogs.pageInfo.endCursor,
          },
        });
      }
    }
  }, [data, fetchMore]);

  const httpLogs = useMemo(
    (): IZtnaHttpLogData[] =>
      (data?.organization.ztnaHttpLogs?.edges ?? []).map(
        (edge): IZtnaHttpLogData => ({
          date: edge?.node.date ?? "",
          destinationIP: edge?.node.destinationIP ?? "",
          email: edge?.node.email ?? "",
          sourceIP: edge?.node.sourceIP ?? "",
        }),
      ),
    [data],
  );

  return { httpLogs };
};

export { useHttpLogsQuery };
