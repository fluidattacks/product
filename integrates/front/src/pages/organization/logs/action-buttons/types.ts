import type { ZtnaLogType } from "gql/graphql";

interface IButtonsProps {
  logType: ZtnaLogType;
  organizationId: string;
}

export type { IButtonsProps };
