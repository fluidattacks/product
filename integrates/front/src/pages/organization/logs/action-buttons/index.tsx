import { Button } from "@fluidattacks/design";
import * as React from "react";
import { useTranslation } from "react-i18next";

import type { IButtonsProps } from "./types";

import { ReportModal } from "../report-modal";
import { useModal } from "hooks/use-modal";

const Buttons: React.FC<IButtonsProps> = ({
  logType,
  organizationId,
}: IButtonsProps): JSX.Element => {
  const { t } = useTranslation();
  const modalProps = useModal("report-ztna-logs");
  const { open } = modalProps;

  return (
    <React.Fragment>
      <Button icon={"file-export"} onClick={open} variant={"ghost"}>
        {t("group.findings.exportCsv.text")}
      </Button>
      <ReportModal
        logType={logType}
        modalRef={modalProps}
        organizationId={organizationId}
      />
    </React.Fragment>
  );
};

export { Buttons };
