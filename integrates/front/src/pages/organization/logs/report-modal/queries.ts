import { graphql } from "gql";

const ZTNA_LOGS_REPORT_URL = graphql(`
  query GetZtnaLogsReportUrl(
    $date: DateTime!
    $logType: ZtnaLogType!
    $organizationId: String!
    $verificationCode: String!
  ) {
    ztnaLogsReportUrl(
      date: $date
      logType: $logType
      organizationId: $organizationId
      verificationCode: $verificationCode
    )
  }
`);

export { ZTNA_LOGS_REPORT_URL };
