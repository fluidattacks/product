import { useLazyQuery } from "@apollo/client";
import { Button, Col, Row, Text } from "@fluidattacks/design";
import dayjs from "dayjs";
import range from "lodash/range";
import { Fragment, useCallback, useState } from "react";
import { useTranslation } from "react-i18next";

import { ZTNA_LOGS_REPORT_URL } from "./queries";
import type { IReportProps } from "./types";

import type { IDropDownOption } from "components/dropdown/types";
import { FormModal } from "components/form-modal";
import { Select } from "components/input";
import { VerifyDialog } from "features/verify-dialog";
import type { TVerifyFn } from "features/verify-dialog/types";
import { GET_NOTIFICATIONS } from "pages/dashboard/navbar/downloads/queries";
import { Logger } from "utils/logger";
import { msgSuccess } from "utils/notifications";
import { openUrl } from "utils/resource-helpers";

const DATE_RANGE = 12;

const ReportModal = ({
  logType,
  modalRef,
  organizationId,
}: IReportProps): JSX.Element => {
  const { t } = useTranslation();
  const dateRange = range(0, DATE_RANGE).map((month): string =>
    dayjs().subtract(month, "month").format("MM/YYYY"),
  );
  const [date, setDate] = useState(dateRange[0]);
  const [isVerifyDialogOpen, setIsVerifyDialogOpen] = useState(false);

  const [getZtnaLogsReportUrl, { client }] = useLazyQuery(
    ZTNA_LOGS_REPORT_URL,
    {
      onCompleted: (data): void => {
        msgSuccess(
          t("groupAlerts.reportRequested"),
          t("groupAlerts.titleSuccess"),
        );
        void client.refetchQueries({ include: [GET_NOTIFICATIONS] });
        setIsVerifyDialogOpen(false);
        openUrl(data.ztnaLogsReportUrl);
        modalRef.close();
      },
      onError: (errors): void => {
        errors.graphQLErrors.forEach((error): void => {
          Logger.warning("An error occurred requesting report", error);
        });
      },
    },
  );

  const handleZtnaLogsReportUrl = useCallback(
    async (verificationCode: string): Promise<void> => {
      await getZtnaLogsReportUrl({
        variables: {
          date,
          logType,
          organizationId,
          verificationCode,
        },
      });
    },
    [date, organizationId, logType, getZtnaLogsReportUrl],
  );

  const handleReportUrl = useCallback(
    (setVerifyCallbacks: TVerifyFn): (() => void) => {
      return (): void => {
        setVerifyCallbacks(
          async (verificationCode): Promise<void> => {
            await handleZtnaLogsReportUrl(verificationCode);
          },
          (): void => {
            setIsVerifyDialogOpen(false);
          },
        );
        setIsVerifyDialogOpen(true);
      };
    },
    [handleZtnaLogsReportUrl, setIsVerifyDialogOpen],
  );

  const handleClose = useCallback((): void => {
    modalRef.close();
    setIsVerifyDialogOpen(false);
  }, [modalRef, setIsVerifyDialogOpen]);
  const handleDateChange = useCallback((selection: IDropDownOption): void => {
    setDate(selection.value ?? "");
  }, []);
  const selectOptions = dateRange.map(
    (dateValue): IDropDownOption => ({
      header: dateValue,
      value: dateValue,
    }),
  );

  return (
    <FormModal
      initialValues={{ date }}
      modalRef={{ ...modalRef, close: handleClose }}
      onSubmit={handleClose}
      size={"sm"}
      title={t("group.findings.report.modalTitle")}
    >
      <VerifyDialog isOpen={isVerifyDialogOpen}>
        {(setVerifyCallbacks): JSX.Element => {
          return (
            <Fragment>
              <Text mb={1} size={"sm"}>
                {t("organization.tabs.ztna.reports.advice")}
              </Text>
              <Row>
                <Col lg={70} md={70}>
                  <Select
                    handleOnChange={handleDateChange}
                    items={selectOptions}
                    name={"date"}
                    required={true}
                  />
                </Col>
              </Row>
              <div className={"mt3"}>
                <Button
                  icon={"file-archive"}
                  onClick={handleReportUrl(setVerifyCallbacks)}
                  variant={"secondary"}
                >
                  {t("group.findings.report.data.text")}
                </Button>
              </div>
            </Fragment>
          );
        }}
      </VerifyDialog>
    </FormModal>
  );
};

export { ReportModal };
