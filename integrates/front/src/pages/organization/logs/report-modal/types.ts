import type { ZtnaLogType } from "gql/graphql";
import type { IUseModal } from "hooks/use-modal";

interface IReportOptionsProps {
  isOpen: boolean;
  logType: ZtnaLogType;
  organizationId: string;
  onClose: () => void;
}

interface IReportProps {
  logType: ZtnaLogType;
  modalRef: IUseModal;
  organizationId: string;
}

export type { IReportOptionsProps, IReportProps };
