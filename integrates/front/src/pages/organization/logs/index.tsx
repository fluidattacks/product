import * as React from "react";
import { Navigate, Route, Routes } from "react-router-dom";

import { OrganizationZtnaHttpLogs } from "./http";
import { ZtnaLogsTemplate } from "./index.template";
import { OrganizationZtnaNetworkLogs } from "./network";
import { OrganizationZtnaSessionLogs } from "./session";
import type { IZtnaLogsProps } from "./types";

import { useOrgUrl } from "pages/organization/hooks";

const OrganizationLogs: React.FC<IZtnaLogsProps> = ({
  organizationId,
}): JSX.Element => {
  const { url } = useOrgUrl("/logs/*");

  return (
    <ZtnaLogsTemplate url={url}>
      <Routes>
        <Route
          element={<OrganizationZtnaHttpLogs organizationId={organizationId} />}
          path={`/http`}
        />
        <Route
          element={
            <OrganizationZtnaNetworkLogs organizationId={organizationId} />
          }
          path={`/network`}
        />
        <Route
          element={
            <OrganizationZtnaSessionLogs organizationId={organizationId} />
          }
          path={`/session`}
        />
        <Route
          element={
            <Navigate replace={true} to={`${url}/http`.replace("//", "/")} />
          }
          path={"*"}
        />
      </Routes>
    </ZtnaLogsTemplate>
  );
};

export { OrganizationLogs };
