import { graphql } from "gql";

export const GET_ORGANIZATION_ZTNA_SESSION_LOGS = graphql(`
  query GetOrganizationZtnaSessionLogs(
    $after: String
    $endDate: DateTime
    $organizationId: String!
    $startDate: DateTime!
  ) {
    organization(organizationId: $organizationId) {
      id
      name
      ztnaSessionLogs(after: $after, endDate: $endDate, startDate: $startDate) {
        edges {
          node {
            email
            endSessionDate
            startSessionDate
          }
        }
        pageInfo {
          endCursor
          hasNextPage
        }
      }
    }
  }
`);
