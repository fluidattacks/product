interface IZtnaSessionLogData {
  email: string;
  endSessionDate: string;
  startSessionDate: string;
}

export type { IZtnaSessionLogData };
