import { useQuery } from "@apollo/client";
import { useAbility } from "@casl/react";
import { Button, Container, EmptyState, Loading } from "@fluidattacks/design";
import type { IButtonProps } from "@fluidattacks/design/dist/components/empty-state/types";
import type { ColumnDef } from "@tanstack/react-table";
import _ from "lodash";
import {
  Fragment,
  useCallback,
  useContext,
  useEffect,
  useMemo,
  useState,
} from "react";
import * as React from "react";
import { useTranslation } from "react-i18next";
import { useNavigate, useParams } from "react-router-dom";

import { plusFormatterRender } from "./plus-formatter/utils";
import { PlusModal } from "./plus-modal";
import {
  GET_ORGANIZATION_GROUPS,
  GET_ORGANIZATION_INTEGRATION_REPOSITORIES,
} from "./queries";
import type {
  IIntegrationRepositoriesAttr,
  IOrganizationGroups,
  IOrganizationIntegrationRepositoriesAttr,
  IOrganizationOutsideProps,
} from "./types";
import { getInfoRepositories } from "./utils";

import { Authorize } from "components/@core/authorize";
import { SectionHeader } from "components/section-header";
import { Table } from "components/table";
import {
  authzGroupContext,
  authzPermissionsContext,
} from "context/authz/config";
import { useModal, useTable } from "hooks";
import { useAudit } from "hooks/use-audit";
import { GET_ORGANIZATION_CREDENTIALS } from "pages/organization/credentials/queries";
import type { ICredentialsAttr } from "pages/organization/credentials/types";
import type {
  IAction,
  IGroupAction,
} from "pages/to-do/assigned-vulnerabilities/types";
import { Logger } from "utils/logger";

export const OrganizationOutside: React.FC<IOrganizationOutsideProps> = ({
  organizationId,
}): JSX.Element => {
  const { t } = useTranslation();
  const permissions = useAbility(authzPermissionsContext);
  const canAddCredentials = permissions.can(
    "integrates_api_mutations_add_credentials_mutate",
  );
  const tableRef = useTable("tblOrganizationCredentials");
  const navigate = useNavigate();
  const { organizationName } = useParams() as { organizationName: string };

  const attributesContext = useContext(authzGroupContext);
  const permissionsContext = useContext(authzPermissionsContext);

  const [selectedRow, setSelectedRow] =
    useState<IIntegrationRepositoriesAttr>();
  const [selectedRepositories, setSelectedRepositories] = useState<
    IIntegrationRepositoriesAttr[]
  >([]);
  const modalRef = useModal("plus-modal");

  // GraphQl queries
  const { addAuditEvent } = useAudit();
  const {
    data: repositoriesData,
    fetchMore,
    loading,
    refetch: refetchRepositories,
  } = useQuery(GET_ORGANIZATION_INTEGRATION_REPOSITORIES, {
    fetchPolicy: "network-only",
    nextFetchPolicy: "cache-first",
    onCompleted: (): void => {
      addAuditEvent("Organization.Outside", organizationId);
    },
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error): void => {
        Logger.error(
          "Couldn't load organization integration repositories",
          error,
        );
      });
    },
    variables: {
      first: 150,
      organizationId,
    },
  });

  const { data: groupsData } = useQuery<{ organization: IOrganizationGroups }>(
    GET_ORGANIZATION_GROUPS,
    {
      fetchPolicy: "cache-first",
      onError: ({ graphQLErrors }): void => {
        graphQLErrors.forEach((error): void => {
          Logger.warning(
            "An error occurred fetching organization groups",
            error,
          );
        });
      },
      variables: {
        organizationId,
      },
    },
  );

  const { data: credentialsData } = useQuery(GET_ORGANIZATION_CREDENTIALS, {
    onError: (errors): void => {
      errors.graphQLErrors.forEach((error): void => {
        Logger.error(
          "Couldn't load organization credentials from outside",
          error,
        );
      });
    },
    variables: {
      organizationId,
    },
  });

  const credentialsAttrs = useMemo(
    (): ICredentialsAttr[] =>
      _.isUndefined(credentialsData)
        ? []
        : (credentialsData.organization.credentials ?? []).filter(
            (credential): boolean =>
              credential.isPat || credential.type === "OAUTH",
          ),
    [credentialsData],
  );

  const groupNames = useMemo(
    (): string[] =>
      _.isUndefined(groupsData)
        ? []
        : groupsData.organization.groups.reduce(
            (previousValue: string[], group): string[] => {
              return group.permissions.includes(
                "integrates_api_mutations_add_git_root_mutate",
              ) && group.serviceAttributes.includes("has_service_white")
                ? [...previousValue, group.name]
                : previousValue;
            },
            [],
          ),
    [groupsData],
  );

  const { integrationRepositories, pageInfo } = getInfoRepositories(
    repositoriesData as IOrganizationIntegrationRepositoriesAttr,
  );

  // Table config
  const tableColumns: ColumnDef<IIntegrationRepositoriesAttr>[] = [
    {
      accessorKey: "url",
      header: t("organization.tabs.weakest.table.url"),
    },
    {
      accessorKey: "lastCommitDate",
      header: t("organization.tabs.weakest.table.lastCommitDate"),
    },
  ];
  const handlePlusRoot = useCallback(
    (repository: IIntegrationRepositoriesAttr | undefined): void => {
      if (repository !== undefined) {
        modalRef.setIsOpen(true);
        setSelectedRow(repository);
      }
    },
    [modalRef],
  );
  const handlePlusManyRoots = useCallback((): void => {
    if (selectedRepositories.length > 0) {
      modalRef.setIsOpen(true);
    }
  }, [selectedRepositories.length, modalRef]);

  const plusColumn: ColumnDef<IIntegrationRepositoriesAttr>[] = [
    {
      accessorKey: "defaultBranch",
      cell: (cell): JSX.Element =>
        plusFormatterRender(cell.row.original, handlePlusRoot),
      header: t("organization.tabs.weakest.table.action"),
    },
  ];
  const onGroupChange = (): void => {
    if (groupsData !== undefined) {
      attributesContext.update([]);
      permissionsContext.update([]);
      const groupsServicesAttributes = groupsData.organization.groups.reduce(
        (
          previousValue: IOrganizationGroups["groups"],
          currentValue,
        ): IOrganizationGroups["groups"] => [
          ...previousValue,
          ...(currentValue.serviceAttributes.includes("has_service_white")
            ? [currentValue]
            : []),
        ],
        [],
      );

      const currentAttributes = Array.from(
        new Set(
          groupsServicesAttributes.reduce(
            (previous: string[], current): string[] => [
              ...previous,
              ...current.serviceAttributes,
            ],
            [],
          ),
        ),
      );
      if (currentAttributes.length > 0) {
        attributesContext.update(
          currentAttributes.map((action): IAction => ({ action })),
        );
      }
      permissionsContext.update(
        groupsData.organization.permissions.map(
          (action): IAction => ({ action }),
        ),
      );
    }
  };

  const changeOrganizationPermissions = useCallback((): void => {
    if (groupsData !== undefined) {
      permissionsContext.update(
        groupsData.organization.permissions.map(
          (action): IAction => ({ action }),
        ),
      );
    }
  }, [groupsData, permissionsContext]);

  const changeGroupPermissions = useCallback(
    (groupName: string): void => {
      permissionsContext.update([]);
      attributesContext.update([]);
      if (groupsData !== undefined) {
        const recordPermissions = groupsData.organization.groups.map(
          (group): IGroupAction => ({
            actions: group.permissions.map((action): IAction => ({ action })),
            groupName: group.name,
          }),
        );
        const filteredPermissions = recordPermissions.filter(
          (recordPermission): boolean =>
            recordPermission.groupName.toLowerCase() ===
            groupName.toLowerCase(),
        );
        if (filteredPermissions.length > 0) {
          permissionsContext.update(filteredPermissions[0].actions);
        }

        const recordServiceAttributes = groupsData.organization.groups.map(
          (group): IGroupAction => ({
            actions: group.serviceAttributes.map(
              (action): IAction => ({ action }),
            ),
            groupName: group.name,
          }),
        );
        const filteredServiceAttributes: IGroupAction[] =
          recordServiceAttributes.filter(
            (record): boolean =>
              record.groupName.toLowerCase() === groupName.toLowerCase(),
          );
        if (filteredServiceAttributes.length > 0) {
          attributesContext.update(filteredServiceAttributes[0].actions);
        }
      }
    },
    [attributesContext, permissionsContext, groupsData],
  );

  useEffect((): void => {
    if (!_.isUndefined(pageInfo)) {
      if (pageInfo.hasNextPage) {
        void fetchMore({
          variables: { after: pageInfo.endCursor, first: 1200 },
        });
      }
    }
  }, [pageInfo, fetchMore]);

  useEffect(onGroupChange, [
    attributesContext,
    permissionsContext,
    groupsData,
    repositoriesData,
  ]);

  const isThereRepositories = useMemo(
    (): boolean => groupNames.length > 0 && integrationRepositories.length > 0,
    [groupNames.length, integrationRepositories.length],
  );

  const onMoveToCredentials = useCallback((): void => {
    navigate(`/orgs/${organizationName}/credentials`);
  }, [organizationName, navigate]);

  const getAddCredentialsButton = useCallback((): IButtonProps | undefined => {
    if (canAddCredentials) {
      return {
        onClick: onMoveToCredentials,
        text: t("organization.tabs.weakest.empty.button"),
      };
    }

    return undefined;
  }, [canAddCredentials, onMoveToCredentials, t]);

  const isEmpty = useCallback((): boolean => {
    return integrationRepositories.length === 0;
  }, [integrationRepositories.length]);

  const header = (
    <SectionHeader header={t("organization.tabs.weakest.empty.header.text")} />
  );

  if (loading) {
    return (
      <Container
        alignItems={"center"}
        display={"flex"}
        height={"100%"}
        justify={"center"}
      >
        <Loading color={"red"} label={"Loading..."} size={"lg"} />
      </Container>
    );
  }

  if (isEmpty()) {
    return (
      <Fragment>
        {header}
        <EmptyState
          confirmButton={getAddCredentialsButton()}
          description={t("organization.tabs.weakest.empty.description")}
          imageSrc={"integrates/empty/outsideThumbIcon"}
          title={t("organization.tabs.weakest.empty.title")}
        />
      </Fragment>
    );
  }

  return (
    <React.StrictMode>
      {header}
      <Container px={1.25} py={1.25}>
        {credentialsAttrs.length === 0 && credentialsData !== undefined ? (
          <Authorize can={"integrates_api_mutations_add_credentials_mutate"}>
            <Button
              icon={"plug"}
              id={"moveToCredentials"}
              onClick={onMoveToCredentials}
              tooltip={t("organization.tabs.weakest.buttons.add.tooltip")}
              variant={"secondary"}
            >
              {t("organization.tabs.weakest.buttons.add.text")}
            </Button>
          </Authorize>
        ) : undefined}
        <Table
          columns={[
            ...tableColumns,
            ...(groupNames.length > 0 ? plusColumn : []),
          ]}
          data={integrationRepositories}
          rightSideComponents={
            isThereRepositories ? (
              <Button
                disabled={selectedRepositories.length === 0}
                icon={"plus"}
                id={"add-many-repositories"}
                onClick={handlePlusManyRoots}
                tooltip={t(
                  "organization.tabs.weakest.buttons.addRepositories.tooltip",
                )}
              >
                {t("organization.tabs.weakest.buttons.addRepositories.text")}
              </Button>
            ) : undefined
          }
          rowSelectionSetter={
            isThereRepositories ? setSelectedRepositories : undefined
          }
          rowSelectionState={selectedRepositories}
          tableRef={tableRef}
        />
        {selectedRow !== undefined || selectedRepositories.length > 0 ? (
          <PlusModal
            changeGroupPermissions={changeGroupPermissions}
            changeOrganizationPermissions={changeOrganizationPermissions}
            groupNames={groupNames}
            modalRef={modalRef}
            refetchRepositories={refetchRepositories}
            repositories={
              selectedRow === undefined ? selectedRepositories : [selectedRow]
            }
            setSelectedRepositories={setSelectedRepositories}
            setSelectedRow={setSelectedRow}
          />
        ) : undefined}
      </Container>
    </React.StrictMode>
  );
};
