import { Buffer } from "buffer";

import _ from "lodash";

import type {
  IIntegrationRepositoriesAttr,
  IIntegrationRepositoriesEdge,
  IOrganizationIntegrationRepositoriesAttr,
  TAddGitRootResult,
} from "./types";

import type { ICredentials } from "pages/group/scope/types";
import { formatDate } from "utils/date";
import { Logger } from "utils/logger";
import { msgError } from "utils/notifications";
import { translate } from "utils/translations/translate";

type TModalMessages = React.Dispatch<
  React.SetStateAction<{
    message: string;
    type: string;
  }>
>;

const getAreAllMutationValid = (results: TAddGitRootResult[]): boolean[] => {
  return results.map((result): boolean => {
    if (!_.isUndefined(result.data) && !_.isNull(result.data)) {
      const addGitRootSuccess = _.isUndefined(result.data.addGitRoot)
        ? true
        : result.data.addGitRoot.success;

      return addGitRootSuccess;
    }

    return false;
  });
};

const handleAddedError = (
  error: unknown,
  setModalMessages: TModalMessages,
): void => {
  switch (String(error).replace(/^ApolloError: /u, "")) {
    case "Exception - Root with the same nickname already exists":
      setModalMessages({
        message: translate.t("group.scope.common.errors.duplicateNickname"),
        type: "error",
      });
      break;
    case "Exception - Root with the same URL/branch already exists":
      setModalMessages({
        message: translate.t("group.scope.common.errors.duplicateUrlBranch"),
        type: "error",
      });
      break;
    case "Exception - Root name should not be included in the exception pattern":
      setModalMessages({
        message: translate.t("group.scope.git.errors.rootInGitignore"),
        type: "error",
      });
      break;
    case "Exception - Invalid characters":
      setModalMessages({
        message: translate.t("validations.invalidChar"),
        type: "error",
      });
      break;
    case "Exception - Unsanitized input found":
      setModalMessages({
        message: translate.t("validations.unsanitizedInputFound"),
        type: "error",
      });
      break;
    case "Exception - A credential exists with the same name":
      setModalMessages({
        message: translate.t("validations.invalidCredentialName"),
        type: "error",
      });
      break;
    case "Exception - Git repository was not accessible with given credentials":
      setModalMessages({
        message: translate.t("group.scope.git.errors.invalidGitCredentials"),
        type: "error",
      });
      break;
    case "Exception - Branch not found":
      setModalMessages({
        message: translate.t("group.scope.git.errors.invalidBranch"),
        type: "error",
      });
      break;
    case "Exception - Field cannot fill with blank characters":
      setModalMessages({
        message: translate.t("validations.invalidSpaceField"),
        type: "error",
      });
      break;
    case "Exception - Protocol http is invalid":
      setModalMessages({
        message: translate.t("validations.invalidHttpProtocol"),
        type: "error",
      });
      break;
    case "Exception - The action is not allowed during the free trial":
      msgError(translate.t("group.scope.git.errors.trial"));
      break;
    default:
      setModalMessages({
        message: translate.t("groupAlerts.errorTextsad"),
        type: "error",
      });
      Logger.error("Couldn't add git roots", error);
  }
};

const getAddGitRootCredentialsVariables = (
  credentials: ICredentials,
): Record<string, boolean | string | undefined> | undefined => {
  if (_.isEmpty(credentials.id)) {
    if (
      _.isEmpty(credentials.key) &&
      _.isEmpty(credentials.user) &&
      _.isEmpty(credentials.password) &&
      _.isEmpty(credentials.token)
    ) {
      return undefined;
    }

    return {
      azureOrganization:
        _.isUndefined(credentials.azureOrganization) ||
        _.isUndefined(credentials.isPat) ||
        !credentials.isPat
          ? undefined
          : credentials.azureOrganization,
      isPat: _.isUndefined(credentials.isPat) ? false : credentials.isPat,
      key: _.isEmpty(credentials.key)
        ? undefined
        : Buffer.from(credentials.key).toString("base64"),
      name: credentials.name,
      password: credentials.password,
      token: credentials.token,
      type: credentials.type,
      user: credentials.user,
    };
  }

  return {
    id: credentials.id,
  };
};

const getInfoRepositories = (
  repositoriesData: IOrganizationIntegrationRepositoriesAttr | undefined,
): {
  integrationRepositories: IIntegrationRepositoriesAttr[];
  pageInfo: { hasNextPage: boolean; endCursor: string } | undefined;
} => {
  if (_.isUndefined(repositoriesData)) {
    return { integrationRepositories: [], pageInfo: undefined };
  }

  return {
    integrationRepositories: _.orderBy(
      repositoriesData.organization.integrationRepositoriesConnection.edges.map(
        ({
          node,
        }: IIntegrationRepositoriesEdge): IIntegrationRepositoriesAttr => ({
          ...node,
          lastCommitDate: formatDate(node.lastCommitDate ?? ""),
        }),
      ),
      "lastCommitDate",
      "desc",
    ),
    pageInfo:
      repositoriesData.organization.integrationRepositoriesConnection.pageInfo,
  };
};

export {
  getAreAllMutationValid,
  handleAddedError,
  getAddGitRootCredentialsVariables,
  getInfoRepositories,
};
