import { Container, IconButton } from "@fluidattacks/design";
import { useCallback } from "react";
import * as React from "react";
import { useTheme } from "styled-components";

import type { IPlusFormatterProps } from "./types";

import { translate } from "utils/translations/translate";

export const PlusFormatter = <T extends object>({
  row,
  plusFunction,
}: IPlusFormatterProps<T>): JSX.Element => {
  const theme = useTheme();

  const handlePlusFormatter = useCallback(
    (event: React.FormEvent<HTMLButtonElement>): void => {
      event.stopPropagation();
      plusFunction(row);
    },
    [row, plusFunction],
  );

  return (
    <Container alignItems={"center"} display={"flex"} gap={0.5}>
      <IconButton
        border={`1px solid ${theme.palette.gray[100]} !important`}
        icon={"plus"}
        iconSize={"xxs"}
        iconType={"fa-light"}
        onClick={handlePlusFormatter}
        variant={"ghost"}
      />
      {translate.t("organization.tabs.weakest.formatter.plus.button")}
    </Container>
  );
};
