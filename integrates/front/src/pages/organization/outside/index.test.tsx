import { PureAbility } from "@casl/ability";
import { fireEvent, screen, waitFor, within } from "@testing-library/react";
import { userEvent } from "@testing-library/user-event";
import { HttpResponse, type StrictResponse, graphql } from "msw";
import { Route, Routes } from "react-router-dom";

import { GET_ORGANIZATION_INTEGRATION_REPOSITORIES } from "./queries";

import { OrganizationOutside } from ".";
import type { IAuthContext } from "context/auth";
import { authContext } from "context/auth";
import {
  authzGroupContext,
  authzPermissionsContext,
} from "context/authz/config";
import type {
  GetOrgCredentialsQuery as GetOrganizationCredentials,
  GetOrganizationIntegrationRepositoriesQuery as GetOrganizationIntegrationRepositories,
} from "gql/graphql";
import { render } from "mocks";
import { LINK } from "mocks/constants";
import { GET_ORGANIZATION_CREDENTIALS } from "pages/organization/credentials/queries";

const mockNavigatePush: jest.Mock = jest.fn();
jest.mock(
  "react-router-dom",
  (): Record<string, unknown> => ({
    ...jest.requireActual("react-router-dom"),
    useNavigate: (): jest.Mock => mockNavigatePush,
  }),
);

describe("organizationOutside", (): void => {
  const memoryRouter = {
    initialEntries: ["/orgs/orgtest/outside"],
  };
  const mockAuthContext: IAuthContext = {
    awsSubscription: null,
    tours: {
      newGroup: false,
      newRoot: true,
      welcome: false,
    },
    userEmail: "",
    userName: "",
  };

  it("should handle select group to add", async (): Promise<void> => {
    expect.hasAssertions();

    jest.clearAllMocks();

    const { container } = render(
      <authContext.Provider value={mockAuthContext}>
        <authzPermissionsContext.Provider value={new PureAbility<string>([])}>
          <authzGroupContext.Provider value={new PureAbility<string>([])}>
            <Routes>
              <Route
                element={
                  <OrganizationOutside
                    organizationId={"ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3"}
                  />
                }
                path={"/orgs/:organizationName/outside"}
              />
            </Routes>
          </authzGroupContext.Provider>
        </authzPermissionsContext.Provider>
      </authContext.Provider>,
      {
        memoryRouter,
      },
    );

    await waitFor((): void => {
      expect(
        screen.queryByRole("cell", {
          name: "https://testrepo.com/testorg1/testproject1/_git/testrepo",
        }),
      ).toBeInTheDocument();
    });

    expect(container.querySelector(".fa-plus")).toBeInTheDocument();
    expect(
      screen.queryByRole("combobox", { name: "groupName" }),
    ).not.toBeInTheDocument();

    fireEvent.click(screen.getAllByRole("button")[1]);

    await waitFor((): void => {
      expect(
        screen.getByRole("combobox", { name: "groupName" }),
      ).toBeInTheDocument();
    });

    expect(
      screen.queryByText("group.scope.common.add"),
    ).not.toBeInTheDocument();

    await userEvent.click(screen.getByRole("listitem"));
    await userEvent.click(screen.getByRole("listitem", { name: "group1" }));

    await userEvent.click(screen.getByText("buttons.confirm"));
    await waitFor((): void => {
      expect(screen.getByRole("combobox", { name: "url" })).toHaveValue(
        "https://testrepo.com/testorg1/testproject1/_git/testrepo",
      );
    });

    expect(screen.getByRole("combobox", { name: "branch" })).toHaveValue(
      "main",
    );
    expect(screen.getAllByRole("combobox")).toHaveLength(5);
    expect(screen.getAllByRole("button")).toHaveLength(6);
    expect(
      within(
        screen.getByRole("combobox", { name: "credentials.typeCredential" }),
      ).getAllByRole("listitem")[0],
    ).toHaveAttribute("aria-disabled", "true");
  });

  it("should handle select group to add many", async (): Promise<void> => {
    expect.hasAssertions();

    jest.clearAllMocks();

    render(
      <authContext.Provider value={mockAuthContext}>
        <authzPermissionsContext.Provider value={new PureAbility<string>([])}>
          <authzGroupContext.Provider value={new PureAbility<string>([])}>
            <Routes>
              <Route
                element={
                  <OrganizationOutside
                    organizationId={"ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3"}
                  />
                }
                path={"/orgs/:organizationName/outside"}
              />
            </Routes>
          </authzGroupContext.Provider>
        </authzPermissionsContext.Provider>
      </authContext.Provider>,
      {
        memoryRouter,
      },
    );

    await waitFor((): void => {
      expect(
        screen.queryByRole("cell", {
          name: "https://testrepo.com/testorg1/testproject1/_git/testrepo",
        }),
      ).toBeInTheDocument();
    });

    expect(
      screen.queryByRole("combobox", { name: "groupName" }),
    ).not.toBeInTheDocument();
    expect(screen.getAllByRole("button")[0]).toBeDisabled();

    await userEvent.click(screen.getAllByRole("checkbox")[0]);

    await waitFor((): void => {
      expect(screen.getAllByRole("button")[0]).not.toBeDisabled();
    });

    await userEvent.click(screen.getAllByRole("button")[0]);

    await waitFor((): void => {
      expect(
        screen.getByRole("combobox", { name: "groupName" }),
      ).toBeInTheDocument();
    });

    expect(
      screen.queryByText("group.scope.common.add"),
    ).not.toBeInTheDocument();

    await userEvent.click(screen.getByRole("listitem"));
    await userEvent.click(screen.getByRole("listitem", { name: "group1" }));
    await userEvent.click(screen.getByText("buttons.confirm"));

    await waitFor((): void => {
      expect(screen.getByRole("combobox", { name: "branch" })).toHaveValue(
        "main",
      );
    });

    expect(
      screen.queryByRole("combobox", { name: "url" }),
    ).not.toBeInTheDocument();
    expect(screen.getAllByRole("combobox")).toHaveLength(4);
    expect(screen.getAllByRole("button")).toHaveLength(6);
    expect(
      within(
        screen.getByRole("combobox", { name: "credentials.typeCredential" }),
      ).getAllByRole("listitem")[0],
    ).toHaveAttribute("aria-disabled", "true");
  });

  it("should handle empty", async (): Promise<void> => {
    expect.hasAssertions();

    jest.clearAllMocks();

    render(
      <authzPermissionsContext.Provider value={new PureAbility<string>([])}>
        <authzGroupContext.Provider value={new PureAbility<string>([])}>
          <Routes>
            <Route
              element={
                <OrganizationOutside
                  organizationId={"ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3"}
                />
              }
              path={"/orgs/:organizationName/outside"}
            />
          </Routes>
        </authzGroupContext.Provider>
      </authzPermissionsContext.Provider>,
      {
        memoryRouter,
        mocks: [
          graphql
            .link(LINK)
            .query(
              GET_ORGANIZATION_CREDENTIALS,
              (): StrictResponse<{ data: GetOrganizationCredentials }> => {
                return HttpResponse.json({
                  data: {
                    organization: {
                      __typename: "Organization",
                      credentials: [],
                      groups: [],
                      name: "orgtest",
                    },
                  },
                });
              },
            ),
          graphql.link(LINK).query(
            GET_ORGANIZATION_INTEGRATION_REPOSITORIES,
            (): StrictResponse<{
              data: GetOrganizationIntegrationRepositories;
            }> => {
              return HttpResponse.json({
                data: {
                  organization: {
                    __typename: "Organization",
                    integrationRepositoriesConnection: {
                      __typename: "IntegrationRepositoriesConnection",
                      edges: [],
                      pageInfo: {
                        endCursor: "bnVsbA==",
                        hasNextPage: false,
                      },
                    },
                    name: "orgtest",
                  },
                },
              });
            },
          ),
        ],
      },
    );

    await waitFor((): void => {
      expect(screen.queryByRole("button")).toBeInTheDocument();
    });

    await userEvent.click(screen.getByRole("button"));

    await waitFor((): void => {
      expect(mockNavigatePush).toHaveBeenCalledTimes(1);
    });

    expect(mockNavigatePush).toHaveBeenCalledWith("/orgs/orgtest/credentials");
  });
});
