import type { IItem, TFormMethods } from "@fluidattacks/design";
import type { SetStateAction } from "react";

import type { IAddOtherMethodProps } from "../types";
import type { ICountry, IState } from "utils/countries";

interface IChangeProps {
  countries: ICountry[];
  setValue: TFormMethods<IAddOtherMethodProps>["setValue"];
  setCities: (value: SetStateAction<string[]>) => void;
  values: TFormMethods<IAddOtherMethodProps>["watch"];
}

type TChangeCountryProps = IChangeProps & {
  setStates: (value: SetStateAction<string[]>) => void;
};
type TChangeStateProps = IChangeProps;

const findCountry = (
  countries: ICountry[],
  countryName: string,
): ICountry | undefined =>
  countries.find((country): boolean => country.name === countryName);

const findState = (country: ICountry, stateName: string): IState | undefined =>
  country.states.find((state): boolean => state.name === stateName);

const changeCountry = ({
  countries,
  setValue,
  setCities,
  setStates,
  values,
}: TChangeCountryProps): void => {
  setValue("state", "");
  setValue("city", "");
  setCities([]);
  const selectedCountry = values("country");
  if (selectedCountry !== "") {
    const foundCountry = findCountry(countries, selectedCountry);
    if (foundCountry)
      setStates(foundCountry.states.map((state): string => state.name));
  }
};

const changeState = ({
  countries,
  setValue,
  setCities,
  values,
}: TChangeStateProps): void => {
  setValue("city", "");
  const selectedState = values("state");
  if (values("country") !== "" && selectedState !== "") {
    const foundCountry = findCountry(countries, values("country"));
    if (foundCountry) {
      const foundState = findState(foundCountry, selectedState);
      if (foundState)
        setCities(foundState.cities.map((city): string => city.name));
    }
  }
};

const getCountryOptions = (countries: ICountry[]): IItem[] => {
  return countries.map(
    (country): IItem => ({ name: country.name, value: country.name }),
  );
};
const getStateOptions = (states: string[]): IItem[] => {
  return states.map((state): IItem => ({ name: state, value: state }));
};
const getCityOptions = (cities: string[]): IItem[] => {
  return cities.map((city): IItem => ({ name: city, value: city }));
};

const handleCountryChange = (props: TChangeCountryProps): (() => void) => {
  return (): void => {
    changeCountry({
      ...props,
    });
  };
};

const handleStateChange = (props: TChangeStateProps): (() => void) => {
  return (): void => {
    changeState({
      ...props,
    });
  };
};

export type { TChangeCountryProps, TChangeStateProps };
export {
  changeCountry,
  changeState,
  findCountry,
  getCountryOptions,
  getStateOptions,
  getCityOptions,
  handleCountryChange,
  handleStateChange,
};
