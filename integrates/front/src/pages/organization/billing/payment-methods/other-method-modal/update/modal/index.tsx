import {
  ComboBox,
  Form,
  InnerForm,
  Input,
  InputFile,
} from "@fluidattacks/design";
import { Fragment, useEffect, useState } from "react";
import { useTranslation } from "react-i18next";

import type {
  IAddOtherMethodProps,
  IOldOtherMethodModalProps,
} from "../../../types";
import {
  getCityOptions,
  getCountryOptions,
  getStateOptions,
  handleCountryChange,
  handleStateChange,
} from "../../utils";
import { validationSchema } from "../../validations";
import { getCountries } from "utils/countries";
import type { ICountry } from "utils/countries";

export const UpdateOtherPaymentMethod = ({
  onClose,
  onSubmit,
  initialValues,
}: IOldOtherMethodModalProps): JSX.Element => {
  const { t } = useTranslation();
  const acceptedTypes =
    "application/pdf,application/zip,image/gif,image/jpg,image/png";
  const prefix = "organization.tabs.billing.paymentMethods.add.otherMethods.";

  const [countries, setCountries] = useState<ICountry[]>([]);
  const [states, setStates] = useState<string[]>([]);
  const [cities, setCities] = useState<string[]>([]);

  // Select options
  const countryOptions = getCountryOptions(countries);
  const stateOptions = getStateOptions(states);
  const cityOptions = getCityOptions(cities);

  // Handle actions

  useEffect((): void => {
    const loadCountries = async (): Promise<void> => {
      setCountries(await getCountries());
    };
    void loadCountries();
  }, []);

  return (
    <Form
      cancelButton={{ onClick: onClose }}
      defaultValues={
        initialValues ?? {
          businessName: "",
          city: "",
          country: "",
          email: "",
          rutList: undefined,
          state: "",
          taxIdList: undefined,
        }
      }
      id={"add-other-methods"}
      onSubmit={onSubmit}
      yupSchema={validationSchema(cities, states)}
    >
      <InnerForm<IAddOtherMethodProps>>
        {({ control, register, getValues, setValue, watch }): JSX.Element => {
          const country = getValues("country");

          return (
            <Fragment>
              <Input
                label={t(`${prefix}businessName`)}
                name={"businessName"}
                register={register}
                required={true}
              />
              <ComboBox
                control={control}
                items={countryOptions}
                label={t(`${prefix}country`)}
                loadingItems={countries.length === 0}
                name={"country"}
                onChange={handleCountryChange({
                  countries,
                  setCities,
                  setStates,
                  setValue,
                  values: getValues,
                })}
                required={true}
              />
              {states.length > 0 ? (
                <ComboBox
                  control={control}
                  items={stateOptions}
                  label={t(`${prefix}state`)}
                  loadingItems={states.length === 0}
                  name={"state"}
                  onChange={handleStateChange({
                    countries,
                    setCities,
                    setValue,
                    values: watch,
                  })}
                  required={true}
                />
              ) : undefined}
              {cities.length > 0 ? (
                <ComboBox
                  control={control}
                  items={cityOptions}
                  label={t(`${prefix}city`)}
                  loadingItems={cities.length === 0}
                  name={"city"}
                  required={true}
                />
              ) : undefined}
              {country === "Colombia" ? (
                <Fragment>
                  <Input
                    label={t(`${prefix}email`)}
                    name={"email"}
                    register={register}
                    required={true}
                  />
                  <InputFile
                    accept={acceptedTypes}
                    id={"rut"}
                    label={t(`${prefix}rut`)}
                    name={"rutList"}
                    register={register}
                    required={true}
                    setValue={setValue}
                    watch={watch}
                  />
                </Fragment>
              ) : (
                <InputFile
                  accept={acceptedTypes}
                  id={"taxId"}
                  label={t(`${prefix}taxId`)}
                  name={"taxIdList"}
                  register={register}
                  required={true}
                  setValue={setValue}
                  watch={watch}
                />
              )}
            </Fragment>
          );
        }}
      </InnerForm>
    </Form>
  );
};
