/* eslint-disable react/jsx-no-bind */
import {
  ComboBox,
  Form,
  InnerForm,
  Input,
  InputFile,
} from "@fluidattacks/design";
import { Fragment, useEffect, useState } from "react";
import { useTranslation } from "react-i18next";

import {
  findCountry,
  getCityOptions,
  getStateOptions,
  handleStateChange,
} from "./utils";
import { validationSchema } from "./validations";

import type { IAddOtherMethodProps, IOtherMethodModalProps } from "../types";
import { getCountries } from "utils/countries";
import type { ICountry } from "utils/countries";

export const OtherMethodModal = ({
  country,
  onClose,
  onSubmit,
  initialValues,
}: IOtherMethodModalProps): JSX.Element => {
  const { t } = useTranslation();
  const acceptedTypes =
    "application/pdf,application/zip,image/gif,image/jpg,image/png";
  const prefix = "organization.tabs.billing.paymentMethods.add.otherMethods.";

  const [countries, setCountries] = useState<ICountry[]>([]);
  const [states, setStates] = useState<string[]>([]);
  const [cities, setCities] = useState<string[]>([]);

  // Select options
  const stateOptions = getStateOptions(states);
  const cityOptions = getCityOptions(cities);

  useEffect((): void => {
    const loadCountries = async (): Promise<void> => {
      setCountries(await getCountries());
    };
    void loadCountries();
  }, [setCountries]);

  useEffect((): void => {
    const foundCountry = findCountry(countries, country.value ?? "");
    if (foundCountry)
      setStates(foundCountry.states.map((state): string => state.name));
  }, [countries, country]);

  return (
    <Form
      cancelButton={{ onClick: onClose }}
      defaultValues={
        initialValues ?? {
          businessName: "",
          city: "",
          country: country.value ?? "",
          email: "",
          rutList: undefined,
          state: "",
          taxIdList: undefined,
        }
      }
      id={"add-other-methods"}
      onSubmit={onSubmit}
      yupSchema={validationSchema(cities, states)}
    >
      <InnerForm<IAddOtherMethodProps>>
        {({ register, control, setValue, getValues, watch }): JSX.Element => (
          <Fragment>
            <Input
              label={t(`${prefix}businessName`)}
              name={"businessName"}
              register={register}
              required={true}
            />
            {states.length > 0 ? (
              <ComboBox
                control={control}
                items={stateOptions}
                label={t(`${prefix}state`)}
                loadingItems={states.length === 0}
                name={"state"}
                onChange={(): void => {
                  handleStateChange({
                    countries,
                    setCities,
                    setValue,
                    values: getValues,
                  });
                }}
                required={true}
              />
            ) : undefined}
            {cities.length > 0 ? (
              <ComboBox
                control={control}
                items={cityOptions}
                label={t(`${prefix}city`)}
                loadingItems={cities.length === 0}
                name={"city"}
                required={true}
              />
            ) : undefined}
            {country.value === "Colombia" ? (
              <Fragment>
                <Input
                  label={t(`${prefix}email`)}
                  name={"email"}
                  register={register}
                  required={true}
                />
                <InputFile
                  accept={acceptedTypes}
                  id={"rut"}
                  label={t(`${prefix}rut`)}
                  name={"rutList"}
                  register={register}
                  required={true}
                  setValue={setValue}
                  watch={watch}
                />
              </Fragment>
            ) : (
              <InputFile
                accept={acceptedTypes}
                id={"taxId"}
                label={t(`${prefix}taxId`)}
                name={"taxIdList"}
                register={register}
                required={true}
                setValue={setValue}
                watch={watch}
              />
            )}
          </Fragment>
        )}
      </InnerForm>
    </Form>
  );
};
