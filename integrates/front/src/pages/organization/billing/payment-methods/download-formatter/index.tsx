import { useMutation } from "@apollo/client";
import { IconButton } from "@fluidattacks/design";
import { useCallback } from "react";
import { useTranslation } from "react-i18next";
import { useTheme } from "styled-components";

import type { IDownloadFormatterProps } from "./types";

import { DOWNLOAD_FILE_MUTATION } from "../../queries";
import { Logger } from "utils/logger";
import { msgError } from "utils/notifications";
import { openUrl } from "utils/resource-helpers";

export const DownloadFormatter = ({
  organizationId,
  paymentMethod,
  paymentMethodId,
}: IDownloadFormatterProps): JSX.Element => {
  const { t } = useTranslation();
  const theme = useTheme();

  // Download legal document file
  const [downloadFile] = useMutation(DOWNLOAD_FILE_MUTATION, {
    onCompleted: (downloadData): void => {
      openUrl(downloadData.downloadBillingFile.url);
    },
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error): void => {
        msgError(t("groupAlerts.errorTextsad"));
        Logger.warning("An error occurred downloading billing file", error);
      });
    },
  });

  const handleDownload = useCallback(async (): Promise<void> => {
    if (paymentMethod?.rut) {
      const { fileName } = paymentMethod.rut;
      await downloadFile({
        variables: { fileName, organizationId, paymentMethodId },
      });
    } else if (paymentMethod?.taxId) {
      const { fileName } = paymentMethod.taxId;
      await downloadFile({
        variables: { fileName, organizationId, paymentMethodId },
      });
    } else {
      msgError("Not found a downloadable file");
    }
  }, [downloadFile, organizationId, paymentMethod, paymentMethodId]);

  return (
    <IconButton
      border={`1px solid ${theme.palette.gray[100]} !important`}
      icon={"download"}
      iconSize={"xs"}
      iconType={"fa-light"}
      id={`download-${paymentMethodId}`}
      onClick={handleDownload}
      variant={"ghost"}
    />
  );
};
