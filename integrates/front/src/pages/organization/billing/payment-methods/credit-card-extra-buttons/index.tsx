import { Button, Icon } from "@fluidattacks/design";
import isEmpty from "lodash/isEmpty";
import { Fragment } from "react";
import { useTranslation } from "react-i18next";

import type { ICreditCardExtraButtonsProps } from "./types";

import { Can } from "context/authz/can";

export const CreditCardExtraButtons = ({
  currentCreditCardRow,
  handleRemoveCreditCardPaymentMethod,
  openUpdateCreditCardModal,
  removing,
  updatingCreditCard,
}: ICreditCardExtraButtonsProps): JSX.Element => {
  const { t } = useTranslation();
  const prefix = "organization.tabs.billing.paymentMethods.";

  const disabled =
    isEmpty(currentCreditCardRow) || removing || updatingCreditCard;

  return (
    <Fragment>
      <Can
        do={"integrates_api_mutations_update_credit_card_payment_method_mutate"}
      >
        <Button
          disabled={disabled}
          id={"updateCreditCard"}
          onClick={openUpdateCreditCardModal}
          variant={"ghost"}
        >
          <Icon
            clickable={false}
            icon={"arrow-rotate-right"}
            iconSize={"xs"}
            iconType={"fa-light"}
          />
          &nbsp;
          {t(`${prefix}update.button`)}
        </Button>
      </Can>
      <Can do={"integrates_api_mutations_remove_payment_method_mutate"}>
        <Button
          disabled={disabled}
          id={"removeCreditCard"}
          onClick={handleRemoveCreditCardPaymentMethod}
          variant={"ghost"}
        >
          <Icon
            clickable={false}
            icon={"trash"}
            iconSize={"xs"}
            iconType={"fa-light"}
          />
          &nbsp;
          {t(`${prefix}remove.button`)}
        </Button>
      </Can>
    </Fragment>
  );
};
