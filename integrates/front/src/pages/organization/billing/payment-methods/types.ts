import type { IPaymentMethodAttr } from "../types";
import type { IDropDownOption } from "components/dropdown/types";

interface IAddOtherMethodProps {
  businessName: string;
  city: string;
  country: string;
  email: string;
  rutList: FileList | undefined;
  state: string;
  taxIdList: FileList | undefined;
}

interface IAddMethodPaymentProps {
  country: string | null;
  organizationId: string;
  onClose: () => void;
  onUpdate: () => void;
}

interface IOtherMethodModalProps {
  country: IDropDownOption;
  onClose: () => void;
  onSubmit: (values: IAddOtherMethodProps) => Promise<void>;
  initialValues?: IAddOtherMethodProps;
}
interface IOldOtherMethodModalProps {
  onClose: () => void;
  onSubmit: (values: IAddOtherMethodProps) => Promise<void>;
  initialValues?: IAddOtherMethodProps;
}

interface IAddCreditCardModalProps {
  organizationId: string;
  onClose: () => void;
  country: string;
  groupsInfo: Record<string, Record<string, string | null>>;
}

interface ICardProps {
  cardExpirationMonth?: number;
  cardExpirationYear?: number;
  makeDefault: boolean;
}

interface IUpdateCreditCardModalProps {
  onClose: () => void;
  onSubmit: (values: ICardProps) => Promise<void>;
  country: string;
  isDefault: boolean;
}

interface IOrganizationPaymentMethodsProps {
  country: string | null;
  organizationId: string;
  paymentMethods: IPaymentMethodAttr[];
  onUpdate: () => void;
}

export type {
  ICardProps,
  IAddMethodPaymentProps,
  IAddCreditCardModalProps,
  IOtherMethodModalProps,
  IAddOtherMethodProps,
  IUpdateCreditCardModalProps,
  IOrganizationPaymentMethodsProps,
  IOldOtherMethodModalProps,
};
