import type { Schema } from "yup";
import { array, number, object, string } from "yup";

import { translate } from "utils/translations/translate";

const MIN_BUSINESS_ID_LENGTH = 8;
const MAX_BUSINESS_ID_LENGTH = 13;

const MIN_BUSINESS_NAME_LENGTH = 4;
const MAX_BUSINESS_NAME_LENGTH = 49;

const MIN_CARD_NUMBER_LENGTH = 14;
const MAX_CARD_NUMBER_LENGTH = 19;

const validationSchema: Schema = object().shape({
  billedGroups: array().of(string().required()),
  businessId: string()
    .matches(/^\d+/u, translate.t("organization.billing.businessIdInvalid"))
    .isValidLength(
      MIN_BUSINESS_ID_LENGTH,
      MAX_BUSINESS_ID_LENGTH,
      translate.t("organization.billing.businessIdInvalidLength"),
    ),
  businessName: string()
    .required(
      translate.t("validations.requiredField", { field: "Business Name" }),
    )
    .isValidLength(
      MIN_BUSINESS_NAME_LENGTH,
      MAX_BUSINESS_NAME_LENGTH,
      translate.t("organization.billing.businessNameInvalidLength"),
    ),
  cardNumber: string()
    .required(translate.t("organization.billing.cardNumberRequired"))
    .isValidLength(
      MIN_CARD_NUMBER_LENGTH,
      MAX_CARD_NUMBER_LENGTH,
      translate.t("organization.billing.cardNumberInvalid"),
    ),
  cvc: string()
    .required(translate.t("organization.billing.CVCRequired"))
    .matches(/^\d+/u, translate.t("organization.billing.CVCInvalid"))
    .isValidLength(3, 4, translate.t("organization.billing.CVCInvalidLength")),
  email: string()
    .required(translate.t("validations.requiredEmail"))
    .email(translate.t("validations.email")),
  expMonth: string()
    .required(translate.t("organization.billing.monthRequired"))
    .matches(
      /(?<month>0[1-9]|1[0-2])/u,
      translate.t("organization.billing.creditCardExpMonthInvalid"),
    ),
  expYear: number()
    .required(translate.t("organization.billing.yearRequired"))
    .isValidFunction((value): boolean => {
      if (value === undefined || isNaN(value)) return false;
      const currentYear = new Date().getFullYear();

      return value >= currentYear;
    }, translate.t("organization.billing.creditCardExpYearInvalid")),
});

export { validationSchema };
