import { screen, waitFor } from "@testing-library/react";
import { userEvent } from "@testing-library/user-event";
import { isEmpty, isEqual } from "lodash";
import { HttpResponse, type StrictResponse, graphql } from "msw";

import type { ICreditCardValues } from "./types";
import { createCardToken } from "./utils";

import { RegisterCreditCard } from ".";
import { ADD_CREDIT_CARD_PAYMENT_METHOD } from "../queries";
import type { AddCreditCardPaymentMethodMutation } from "gql/graphql";
import { render } from "mocks";
import { LINK } from "mocks/constants";
import type { IErrorMessage } from "mocks/types";
import { msgError, msgSuccess } from "utils/notifications";

jest.mock("./utils", (): { createCardToken: jest.Mock } => ({
  createCardToken: jest.fn(
    (
      cardData: ICreditCardValues,
    ): {
      token: string | null;
      error: string | null;
    } => {
      const { cardHolderName, cardNumber, cvc, email, expMonth, expYear } =
        cardData;

      if (
        cvc === "123" &&
        email === "billing@fluidtest.com" &&
        expMonth === "12" &&
        expYear === "2030" &&
        cardHolderName === "test" &&
        cardNumber === "4575623182290326"
      )
        return { error: null, token: "this_is_the_token" };

      return { error: "error", token: null };
    },
  ),
}));

jest.mock("utils/notifications", (): Record<string, unknown> => {
  const mockedNotifications: Record<string, () => Record<string, unknown>> =
    jest.requireActual("utils/notifications");
  jest.spyOn(mockedNotifications, "msgError").mockImplementation();
  jest.spyOn(mockedNotifications, "msgSuccess").mockImplementation();

  return mockedNotifications;
});

describe("registerCreditCard", (): void => {
  const graphqlMocked = graphql.link(LINK);

  const mocks = graphqlMocked.mutation(
    ADD_CREDIT_CARD_PAYMENT_METHOD,
    ({
      variables,
    }): StrictResponse<
      IErrorMessage | { data: AddCreditCardPaymentMethodMutation }
    > => {
      const {
        billedGroups,
        billingEmail,
        paymentMethodId,
        makeDefault,
        country,
        organizationId,
        businessId,
        businessName,
      } = variables;

      if (
        billingEmail !== "billing@fluidtest.com" ||
        paymentMethodId !== "this_is_the_token" ||
        makeDefault ||
        country !== "Colombia" ||
        organizationId !== "organizationId"
      ) {
        return HttpResponse.json({
          errors: [{ message: "error" }],
        });
      }

      if (
        isEmpty(billedGroups) &&
        businessId === "323.322.122-7" &&
        businessName === "business1"
      ) {
        return HttpResponse.json({
          data: {
            addCreditCardPaymentMethod: {
              success: true,
            },
          },
        });
      }

      if (
        isEqual(billedGroups, ["group2"]) &&
        businessId === "1321121223" &&
        businessName === "business2"
      ) {
        return HttpResponse.json({
          data: {
            addCreditCardPaymentMethod: {
              success: true,
            },
          },
        });
      }

      if (
        isEqual(billedGroups, ["group1", "group2"]) &&
        businessId === "12323433334-4" &&
        businessName === "business1"
      ) {
        return HttpResponse.json({
          data: {
            addCreditCardPaymentMethod: {
              success: true,
            },
          },
        });
      }

      return HttpResponse.json({
        errors: [{ message: "error" }],
      });
    },
    { once: true },
  );

  it("should render errors", async (): Promise<void> => {
    expect.hasAssertions();

    render(
      <RegisterCreditCard
        country={"Colombia"}
        groupsInfo={{}}
        onClose={jest.fn()}
        organizationId={"1"}
      />,
    );

    await userEvent.type(screen.getByPlaceholderText("Card Number"), "1234");

    expect(
      screen.getByText(/Business Name field is required/iu, { exact: false })
        .parentElement,
    ).toMatchSnapshot();

    expect(screen.getByText("buttons.confirm")).toBeDisabled();
  });

  it("should show confirm modal", async (): Promise<void> => {
    expect.hasAssertions();

    render(
      <RegisterCreditCard
        country={"Colombia"}
        groupsInfo={{
          group1: {
            businessId: "12323433334-4",
            businessName: "business1",
          },
          group2: {
            businessId: "12323433334-4",
            businessName: "business1",
          },
        }}
        onClose={jest.fn()}
        organizationId={"organizationId"}
      />,
      { mocks: [mocks] },
    );

    await userEvent.type(
      screen.getByPlaceholderText("Card Number"),
      "4575623182290326",
    );
    await userEvent.type(screen.getByPlaceholderText("MM"), "12");
    await userEvent.type(screen.getByPlaceholderText("YYYY"), "2030");
    await userEvent.type(screen.getByPlaceholderText("CVC"), "123");
    await userEvent.type(
      screen.getByTestId("email-input"),
      "biling@fluidtest.com",
    );

    await userEvent.click(
      screen.getByTestId("billedGroups-dropdown-selected-option"),
    );
    await userEvent.click(screen.getByText("organization.billing.selectAll"));

    expect(screen.getByText("buttons.confirm")).not.toBeDisabled();

    await userEvent.click(screen.getByText("buttons.confirm"));

    await waitFor((): void => {
      expect(
        screen.queryByText("organization.billing.confirm.title"),
      ).toBeInTheDocument();
    });

    expect(document.getElementsByTagName("table")[0]).toMatchSnapshot();
  });

  it("should submit", async (): Promise<void> => {
    expect.hasAssertions();

    render(
      <RegisterCreditCard
        country={"Colombia"}
        groupsInfo={{
          group1: {
            businessId: "1",
            businessName: "business1",
          },
          group2: {
            businessId: "1",
            businessName: "business1",
          },
        }}
        onClose={jest.fn()}
        organizationId={"organizationId"}
      />,
      { mocks: [mocks] },
    );

    await userEvent.type(
      screen.getByPlaceholderText("Card Number"),
      "4575623182290326",
    );
    await userEvent.type(screen.getByPlaceholderText("MM"), "12");
    await userEvent.type(screen.getByPlaceholderText("YYYY"), "2030");
    await userEvent.type(screen.getByPlaceholderText("CVC"), "123");
    await userEvent.type(
      screen.getByTestId("email-input"),
      "billing@fluidtest.com",
    );

    await userEvent.type(
      screen.getByTestId("businessId-input"),
      "323.322.122-7",
    );
    await userEvent.type(screen.getByTestId("businessName-input"), "business1");

    await waitFor((): void => {
      expect(screen.getByText("buttons.confirm")).not.toBeDisabled();
    });

    await userEvent.click(screen.getByText("buttons.confirm"));
    await userEvent.click(screen.getByText("Confirm"));

    expect(createCardToken).toHaveBeenCalledWith({
      cardHolderName: "test",
      cardNumber: "4575623182290326",
      cvc: "123",
      email: "billing@fluidtest.com",
      expMonth: "12",
      expYear: "2030",
    });

    expect(msgError).not.toHaveBeenCalled();

    expect(msgSuccess).toHaveBeenCalledWith(
      "organization.tabs.billing.paymentMethods.add.success.body",
      "organization.tabs.billing.paymentMethods.add.success.title",
    );
  });

  it("submit only selected groups", async (): Promise<void> => {
    expect.hasAssertions();

    render(
      <RegisterCreditCard
        country={"Colombia"}
        groupsInfo={{
          group1: {
            businessId: "121.212.232-3",
            businessName: "business1",
          },
          group2: {
            businessId: "1321121223",
            businessName: "business2",
          },
          group3: {
            businessId: "121.321.445.3",
            businessName: "business3",
          },
        }}
        onClose={jest.fn()}
        organizationId={"organizationId"}
      />,
      { mocks: [mocks] },
    );

    await userEvent.type(
      screen.getByPlaceholderText("Card Number"),
      "4575623182290326",
    );
    await userEvent.type(screen.getByPlaceholderText("MM"), "12");
    await userEvent.type(screen.getByPlaceholderText("YYYY"), "2030");
    await userEvent.type(screen.getByPlaceholderText("CVC"), "123");
    await userEvent.type(
      screen.getByTestId("email-input"),
      "billing@fluidtest.com",
    );
    await userEvent.click(
      screen.getByTestId("billedGroups-dropdown-selected-option"),
    );
    await userEvent.click(screen.getByText("group2"));

    await waitFor((): void => {
      expect(screen.getByText("buttons.confirm")).not.toBeDisabled();
    });
    await userEvent.click(screen.getByText("buttons.confirm"));

    await userEvent.click(screen.getByText("Confirm"));

    expect(msgError).not.toHaveBeenCalled();

    expect(msgSuccess).toHaveBeenCalledWith(
      "organization.tabs.billing.paymentMethods.add.success.body",
      "organization.tabs.billing.paymentMethods.add.success.title",
    );
  });
});
