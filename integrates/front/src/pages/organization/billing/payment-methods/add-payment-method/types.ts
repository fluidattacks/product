interface IAddMethodPaymentProps {
  organizationId: string;
  onClose: () => void;
  onUpdate: () => void;
}

interface IAddCreditCardModalProps {
  organizationId: string;
  onClose: () => void;
}

export type { IAddMethodPaymentProps, IAddCreditCardModalProps };
