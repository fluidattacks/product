import { Elements } from "@stripe/react-stripe-js";
import { loadStripe } from "@stripe/stripe-js";
import { screen, waitFor } from "@testing-library/react";
import { userEvent } from "@testing-library/user-event";
import { isEqual } from "lodash";
import { HttpResponse, type StrictResponse, graphql } from "msw";
import { useCallback } from "react";

import { AddCreditCardModal } from ".";
import { ADD_CREDIT_CARD_PAYMENT_METHOD } from "../queries";
import type { AddCreditCardPaymentMethodMutation } from "gql/graphql";
import { render } from "mocks";
import { LINK } from "mocks/constants";
import type { IErrorMessage } from "mocks/types";
import { msgError, msgSuccess } from "utils/notifications";

jest.mock("utils/notifications", (): Record<string, unknown> => {
  const mockedNotifications: Record<string, () => Record<string, unknown>> =
    jest.requireActual("utils/notifications");
  jest.spyOn(mockedNotifications, "msgError").mockImplementation();
  jest.spyOn(mockedNotifications, "msgSuccess").mockImplementation();

  return mockedNotifications;
});

jest.mock(
  "@stripe/react-stripe-js",
  (): Record<string, unknown> => {
    return {
      ...jest.requireActual("@stripe/react-stripe-js"),
      CardElement: ({
        onChange,
      }: Readonly<{
        onChange: (event: object) => void;
      }>): JSX.Element => {
        const handleChange = useCallback((): void => {
          onChange({ complete: true });
        }, [onChange]);

        return (
          <div>
            <label>{"CardElement"}</label>
            <input onChange={handleChange} placeholder={"CardMock"} />
          </div>
        );
      },
      useElements: jest.fn((): object => ({})),
      useStripe: jest.fn((): object => ({
        createPaymentMethod: jest.fn((): object => ({
          paymentMethod: { id: "this_is_the_token" },
        })),
      })),
    };
  },
  { virtual: true },
);

const Wrapper = ({
  groupsInfo,
}: Readonly<{
  groupsInfo: Record<string, Record<string, string | null>>;
}>): JSX.Element => {
  const stripePromise = loadStripe("pk_test_hfBDuDH6n4MevubIYN0NrAAA");

  return (
    <Elements stripe={stripePromise}>
      <AddCreditCardModal
        country={"Colombia"}
        groupsInfo={groupsInfo}
        onClose={jest.fn()}
        organizationId={"organizationId"}
      />
    </Elements>
  );
};

describe("registerCreditCard", (): void => {
  const graphqlMocked = graphql.link(LINK);

  const mocks = graphqlMocked.mutation(
    ADD_CREDIT_CARD_PAYMENT_METHOD,
    ({
      variables,
    }): StrictResponse<
      IErrorMessage | { data: AddCreditCardPaymentMethodMutation }
    > => {
      const {
        billedGroups,
        billingEmail,
        paymentMethodId,
        makeDefault,
        country,
        organizationId,
        businessId,
        businessName,
      } = variables;

      if (
        isEqual(billedGroups, []) &&
        billingEmail === "biling@fluidtest.com" &&
        paymentMethodId === "this_is_the_token" &&
        !makeDefault &&
        country === "Colombia" &&
        organizationId === "organizationId"
      ) {
        return HttpResponse.json({
          data: {
            addCreditCardPaymentMethod: {
              success: true,
            },
          },
        });
      }

      if (
        isEqual(billedGroups, ["group2", "group1"]) &&
        billingEmail === "biling@fluidtest.com" &&
        paymentMethodId === "this_is_the_token" &&
        !makeDefault &&
        country === "Colombia" &&
        organizationId === "organizationId" &&
        businessId === "323.322.122-7" &&
        businessName === "business1"
      ) {
        return HttpResponse.json({
          data: {
            addCreditCardPaymentMethod: {
              success: true,
            },
          },
        });
      }

      return HttpResponse.json({
        errors: [{ message: "error" }],
      });
    },
    { once: true },
  );

  it("should submit", async (): Promise<void> => {
    expect.hasAssertions();

    render(
      <Wrapper
        groupsInfo={{
          group1: {
            businessId: "132.132.322-3",
            businessName: "business1",
          },
          group2: {
            businessId: "132.132.322-3",
            businessName: "business1",
          },
        }}
      />,
      {
        mocks: [mocks],
      },
    );

    const cardElement = await screen.findByText("CardElement");

    expect(cardElement).toBeInTheDocument();

    await userEvent.type(screen.getByPlaceholderText("CardMock"), "change");
    await userEvent.type(
      screen.getByTestId("email-input"),
      "biling@fluidtest.com",
    );

    expect(screen.getByText("buttons.confirm")).not.toBeDisabled();

    await userEvent.click(screen.getByText("buttons.confirm"));

    expect(msgError).not.toHaveBeenCalled();

    expect(msgSuccess).toHaveBeenCalledWith(
      "organization.tabs.billing.paymentMethods.add.success.body",
      "organization.tabs.billing.paymentMethods.add.success.title",
    );
  });

  it("submit only selected groups", async (): Promise<void> => {
    expect.hasAssertions();

    render(
      <Wrapper
        groupsInfo={{
          group1: {
            businessId: "323.322.122-7",
            businessName: "business1",
          },
          group2: {
            businessId: "323.322.122-7",
            businessName: "business1",
          },
          group3: {
            businessId: "323.322.121-7",
            businessName: "business3",
          },
        }}
      />,
      {
        mocks: [mocks],
      },
    );

    await waitFor((): void => {
      expect(screen.queryByText("CardElement")).toBeInTheDocument();
    });
    await userEvent.type(screen.getByPlaceholderText("CardMock"), "change");

    await userEvent.type(
      screen.getByTestId("email-input"),
      "biling@fluidtest.com",
    );
    await userEvent.click(
      screen.getByTestId("billedGroups-dropdown-selected-option"),
    );
    await userEvent.click(screen.getByText("group2"));
    await userEvent.click(screen.getByText("group1"));

    expect(screen.getByText("buttons.confirm")).not.toBeDisabled();

    await userEvent.click(screen.getByText("buttons.confirm"));

    expect(msgError).not.toHaveBeenCalled();

    expect(msgSuccess).toHaveBeenCalledWith(
      "organization.tabs.billing.paymentMethods.add.success.body",
      "organization.tabs.billing.paymentMethods.add.success.title",
    );
  });
});
