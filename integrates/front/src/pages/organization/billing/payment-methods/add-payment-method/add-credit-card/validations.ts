import type { Schema } from "yup";
import { array, object, string } from "yup";

import { translate } from "utils/translations/translate";

const MIN_BUSINESS_ID_LENGTH = 8;
const MAX_BUSINESS_ID_LENGTH = 13;
const MIN_BUSINESS_NAME_LENGTH = 4;
const MAX_BUSINESS_NAME_LENGTH = 49;

const validationSchema: Schema = object().shape({
  billedGroups: array().of(string().required()),
  businessId: string()
    .matches(/^\d+/u, translate.t("organization.billing.businessIdInvalid"))
    .isValidLength(
      MIN_BUSINESS_ID_LENGTH,
      MAX_BUSINESS_ID_LENGTH,
      translate.t("organization.billing.businessIdInvalidLength"),
    ),
  businessName: string().isValidLength(
    MIN_BUSINESS_NAME_LENGTH,
    MAX_BUSINESS_NAME_LENGTH,
    translate.t("organization.billing.businessNameInvalidLength"),
  ),
  email: string()
    .required(translate.t("validations.requiredEmail"))
    .email(translate.t("validations.email")),
});

export { validationSchema };
