import { isEmpty } from "lodash";

const findBusinessInfo = (
  selectedGroups: string[],
  groupsInfo: Record<string, Record<string, string | null>>,
): {
  businessId: string | undefined;
  businessName: string | undefined;
} => {
  const groupWhitBusinessInfo = selectedGroups.find((groupName): boolean => {
    const info = groupsInfo[groupName];

    return (
      !isEmpty(info) && !isEmpty(info.businessId) && !isEmpty(info.businessName)
    );
  });

  if (groupWhitBusinessInfo === undefined) {
    return {
      businessId: undefined,
      businessName: undefined,
    };
  }

  return {
    businessId: groupsInfo[groupWhitBusinessInfo].businessId as string,
    businessName: groupsInfo[groupWhitBusinessInfo].businessName as string,
  };
};

export { findBusinessInfo };
