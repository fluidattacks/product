import type { ColumnDef } from "@tanstack/react-table";
import { useTranslation } from "react-i18next";

import type { IFormValues } from "./types";

import { Table } from "components/table";
import { useTable } from "hooks";

const LAST_TWO_DIGITS = 2;
const ConfirmContent = ({
  values,
}: Readonly<{ values: IFormValues }>): JSX.Element => {
  const { t } = useTranslation();

  const tableRef = useTable("confirm-credit-card-addition");

  const columns: ColumnDef<{ key: string; value: string }>[] = [
    {
      accessorKey: "key",
      header: t("key"),
    },
    {
      accessorKey: "value",
      header: t("Value"),
    },
  ];

  const {
    businessId,
    businessName,
    cardNumber,
    email,
    expMonth,
    expYear,
    billedGroups,
    makeDefault,
    cvc,
  } = values;

  const data: { key: string; value: string }[] = [
    {
      key: t("organization.billing.businessId"),
      value: businessId,
    },
    {
      key: t("organization.billing.businessName"),
      value: businessName,
    },
    {
      key: t("organization.billing.cardInformation.cardNumber"),
      value: cardNumber,
    },
    { key: t("organization.billing.email"), value: email },
    {
      key: t("organization.billing.cardInformation.expirationDate"),
      value: `${expMonth}/${expYear.slice(-LAST_TWO_DIGITS)}`,
    },
    {
      key: t("organization.billing.billedGroups"),
      value: billedGroups.join(", "),
    },
    {
      key: t("organization.billing.cardInformation.default"),
      value: makeDefault.toString(),
    },
    { key: t("organization.billing.cardInformation.securityCode"), value: cvc },
  ];

  return (
    <Table
      columns={columns}
      data={data}
      dataPrivate={true}
      options={{ enableSearchBar: false }}
      tableRef={tableRef}
    />
  );
};

export { ConfirmContent };
