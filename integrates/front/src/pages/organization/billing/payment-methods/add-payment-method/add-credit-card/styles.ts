import { styled } from "styled-components";

const CardContainer = styled.div.attrs({ className: "sr-block" })`
  ${({ theme }): string => `
  /* stylelint-disable selector-class-pattern */
  .StripeElement {
    align-items: center;
    background-color: ${theme.palette.white};
    border: 1px solid ${theme.palette.gray[200]};
    border-radius: 4px;
    color: ${theme.palette.gray[800]};
    line-height: ${theme.spacing[1.25]};
    margin-top: -${theme.spacing[0.5]};
    margin-bottom: ${theme.spacing[0.25]};
    padding: 10px 12px;
    transition: all 0.3s ease;

    &::placeholder {
      color: ${theme.palette.gray[400]};
    }
  }

  label {
    padding-bottom: ${theme.spacing[1]};
  }

  .StripeElement--invalid {
    border-color: ${theme.palette.error[500]};
  }

  .StripeElement--webkit-autofill {
    background-color: ${theme.palette.gray[100]} !important;
  }

  margin-bottom: ${theme.spacing[1.5]};
  `}
`;

export { CardContainer };
