import { Field } from "formik";
import { styled } from "styled-components";

const StyledInput = styled(Field).attrs({ className: "sr-block" })`
  align-items: center;
  background-color: inherit;
  border: transparent;
  border-radius: inherit;
  color: inherit;
  display: flex;
  width: 100%;

  &:focus-visible {
    outline: none;
  }

  &::-webkit-calendar-picker-indicator {
    display: none !important;
  }

  &::-webkit-outer-spin-button,
  &::-webkit-inner-spin-button {
    appearance: none;
    margin: 0;
  }

  &[type="number"] {
    appearance: textfield;
  }
`;

export { StyledInput };
