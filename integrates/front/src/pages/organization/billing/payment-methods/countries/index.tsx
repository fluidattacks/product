import { useEffect, useState } from "react";

import type { IDropdownProps } from "components/dropdown/types";
import { Select } from "components/input";
import type { ICountry } from "utils/countries";
import { getCountries } from "utils/countries";

interface ISelect {
  header: string;
  value: string;
}

const CountriesSelect = ({
  handleChange,
}: Readonly<{
  handleChange: IDropdownProps["customSelectionHandler"];
}>): JSX.Element => {
  const [countries, setCountries] = useState<ICountry[]>([]);
  const countryOptions = countries.map(
    (country): ISelect => ({ header: country.name, value: country.name }),
  );

  useEffect((): void => {
    const loadCountries = async (): Promise<void> => {
      setCountries(await getCountries());
    };
    void loadCountries();
  }, [setCountries]);

  return (
    <Select
      handleOnChange={handleChange}
      items={countryOptions}
      label={"Country where the organization is based"}
      name={"country"}
      required={true}
    />
  );
};

export { CountriesSelect };
