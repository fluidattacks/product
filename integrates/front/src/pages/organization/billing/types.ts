import type { ReactNode } from "react";

import type { ManagedType, ServiceType, TierType } from "gql/graphql";

interface IGroupBilling {
  costsAuthors: number;
  costsBase: number;
  costsTotal: number;
  numberAuthors: number;
}

interface IOrganizationActiveGroupAttr {
  name: string;
  tier: string;
}

interface IOrganizationAuthorAttr {
  actor: string;
  activeGroups: IOrganizationActiveGroupAttr[];
}

interface IPaymentMethodAttr {
  __typename: "PaymentMethod";
  id: string;
  brand: string;
  default: boolean;
  lastFourDigits: string;
  expirationMonth: string;
  expirationYear: string;
  businessName: string;
  download: string;
  email: string;
  country: string;
  state: string;
  city: string;
  rut: IFileMetadata | undefined;
  taxId: IFileMetadata | undefined;
}

interface IOrganizationActorAttr {
  name: string;
  email: string;
}

interface IOrganizationAuthorsTable {
  actorName: string;
  actorEmail: string | undefined;
  groupsAuthors: string;
}

interface IGroupAttr {
  billing: IGroupBilling;
  forces: string;
  hasForces: boolean;
  hasEssential: boolean;
  hasAdvanced: boolean;
  essential: string;
  managed: ManagedType;
  name: string;
  paymentId: string;
  permissions: string[];
  service: ServiceType;
  advanced: string;
  tier: TierType;
}

interface IFileMetadata {
  fileName: string;
  modifiedDate: string;
}

interface IOrganizationBillingProps {
  organizationId: string;
}

interface IBillingTemplateProps {
  url: string;
  children: ReactNode;
  costsTotal: number;
  numberAuthorsEssential: number;
  numberAuthorsAdvanced: number;
  numberGroupsEssential: number;
  numberGroupsAdvanced: number;
}

export type {
  IBillingTemplateProps,
  IFileMetadata,
  IGroupAttr,
  IPaymentMethodAttr,
  IOrganizationActorAttr,
  IOrganizationAuthorAttr,
  IOrganizationAuthorsTable,
  IOrganizationActiveGroupAttr,
  IOrganizationBillingProps,
};
