import { useQuery } from "@apollo/client";
import { Col, Row } from "@fluidattacks/design";
import dayjs from "dayjs";
import range from "lodash/range";
import { Fragment, useCallback, useState } from "react";
import * as React from "react";
import { useTranslation } from "react-i18next";
import { Navigate, Route, Routes } from "react-router-dom";

import { BillingTemplate } from "./index.template";

import { useOrgUrl } from "../hooks";
import { Dropdown } from "components/dropdown";
import type { IDropDownOption } from "components/dropdown/types";
import { useAudit } from "hooks/use-audit";
import { OrganizationAuthors } from "pages/organization/billing/authors";
import { OrganizationGroups } from "pages/organization/billing/groups";
import { OrganizationPaymentMethods } from "pages/organization/billing/payment-methods";
import {
  GET_ORGANIZATION_BILLING,
  GET_ORGANIZATION_BILLING_BY_DATE,
} from "pages/organization/billing/queries";
import type {
  IGroupAttr,
  IOrganizationAuthorAttr,
  IOrganizationBillingProps,
  IPaymentMethodAttr,
} from "pages/organization/billing/types";
import { Logger } from "utils/logger";
import { msgError } from "utils/notifications";

const DATE_RANGE = 12;

export const OrganizationBilling: React.FC<IOrganizationBillingProps> = ({
  organizationId,
}): JSX.Element => {
  const { t } = useTranslation();
  const { url } = useOrgUrl("/billing/*");
  const dateRange = range(0, DATE_RANGE).map((month): string =>
    dayjs().subtract(month, "month").format("MM/YYYY"),
  );
  const [billingDate, setBillingDate] = useState(dateRange[0]);
  const customSelectionHandler = useCallback(
    (selection: IDropDownOption): void => {
      if (selection.value !== undefined) {
        setBillingDate(selection.value);
      }
    },
    [],
  );

  // GraphQL operations
  const { addAuditEvent } = useAudit();
  const { data, refetch } = useQuery(GET_ORGANIZATION_BILLING, {
    context: { skipGlobalErrorHandler: true },
    fetchPolicy: "network-only",
    onCompleted: (): void => {
      addAuditEvent("Organization.Billing", organizationId);
    },
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error): void => {
        if (error.message !== "Access denied") {
          msgError(t("groupAlerts.errorTextsad"));
          Logger.warning(
            "An error occurred loading organization groups",
            error,
          );
        }
      });
    },
    variables: { organizationId },
  });

  const { data: dataByDate } = useQuery(GET_ORGANIZATION_BILLING_BY_DATE, {
    context: { skipGlobalErrorHandler: true },
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error): void => {
        if (error.message !== "Access denied") {
          msgError(t("groupAlerts.errorTextsad"));
          Logger.warning("An error occurred getting billing data", error);
        }
      });
    },
    variables: { date: billingDate, organizationId },
  });
  const authors =
    dataByDate === undefined ? [] : dataByDate.organization.billing?.authors;
  const costsTotal = data?.organization.billing?.costsTotal ?? 0;
  const groups = dataByDate?.organization.groups ?? [];
  const numberAuthorsEssential =
    data?.organization.billing?.numberAuthorsEssential ?? 0;
  const numberAuthorsAdvanced =
    data?.organization.billing?.numberAuthorsAdvanced ?? 0;
  const numberGroupsEssential =
    data?.organization.billing?.numberGroupsEssential ?? 0;
  const numberGroupsAdvanced =
    data?.organization.billing?.numberGroupsAdvanced ?? 0;
  const paymentMethods =
    data === undefined ? [] : (data.organization.billing?.paymentMethods ?? []);

  const billingHistory = (
    <Fragment>
      <Row>
        <Row>{t("organization.tabs.billing.authors.label")}</Row>
        <Col lg={20} md={20}>
          <Dropdown
            customSelectionHandler={customSelectionHandler}
            initialSelection={{ value: billingDate }}
            items={[
              ...dateRange.map((date): { value: string } => ({
                value: date,
              })),
            ]}
          />
        </Col>
      </Row>
      <OrganizationGroups groups={groups as IGroupAttr[]} />
      <OrganizationAuthors authors={authors as IOrganizationAuthorAttr[]} />
    </Fragment>
  );

  const paymentMethodsComponent = (
    <OrganizationPaymentMethods
      country={data === undefined ? null : data.organization.country}
      onUpdate={refetch}
      organizationId={organizationId}
      paymentMethods={paymentMethods as IPaymentMethodAttr[]}
    />
  );

  return (
    <BillingTemplate
      costsTotal={costsTotal}
      numberAuthorsAdvanced={numberAuthorsAdvanced}
      numberAuthorsEssential={numberAuthorsEssential}
      numberGroupsAdvanced={numberGroupsAdvanced}
      numberGroupsEssential={numberGroupsEssential}
      url={url}
    >
      <Routes>
        <Route element={billingHistory} path={"/history"} />
        <Route element={paymentMethodsComponent} path={`/payment`} />
        <Route element={<Navigate to={`${url}/history`} />} path={"*"} />
      </Routes>
    </BillingTemplate>
  );
};
