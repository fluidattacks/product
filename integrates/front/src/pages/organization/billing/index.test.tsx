import { screen, waitFor } from "@testing-library/react";
import { Route, Routes } from "react-router-dom";

import { OrganizationBilling } from ".";
import { render } from "mocks";

describe("billing", (): void => {
  it("should render a component", async (): Promise<void> => {
    expect.hasAssertions();

    jest.spyOn(console, "error").mockImplementation();
    jest.spyOn(console, "warn").mockImplementation();
    render(
      <Routes>
        <Route
          element={
            <OrganizationBilling
              organizationId={"ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3"}
            />
          }
          path={"/orgs/:organizationName/billing/history"}
        />
      </Routes>,
      {
        memoryRouter: {
          initialEntries: ["/orgs/okada/billing/history"],
        },
      },
    );

    await waitFor((): void => {
      expect(
        screen.getByText(/organization\.tabs.billing.header/u),
      ).toBeInTheDocument();
    });
  });
});
