import { OAuthSelector } from "@fluidattacks/design";
import type { MouseEventHandler } from "react";
import { useCallback, useMemo } from "react";
import { useTranslation } from "react-i18next";

import type { ICredentialSelector } from "./types";

import { Can } from "context/authz/can";
import type { TOAuthProvider } from "pages/group/scope/git-roots/add-root-button/utils";
import { openUrl } from "utils/resource-helpers";

const CredentialSelector = ({
  onAdd,
  organizationId,
}: Readonly<ICredentialSelector>): JSX.Element => {
  const { t } = useTranslation();

  const providers: TOAuthProvider[] = [
    "GitHub",
    "GitLab",
    "Bitbucket",
    "Azure",
  ];

  const labUrl = useMemo((): string => {
    const oauthUrl = new URL("/dgitlab", window.location.origin);
    oauthUrl.searchParams.set("subject", organizationId);

    return oauthUrl.toString();
  }, [organizationId]);

  const hubUrl = useMemo((): string => {
    const oauthUrl = new URL("/dgithub", window.location.origin);
    oauthUrl.searchParams.set("subject", organizationId);

    return oauthUrl.toString();
  }, [organizationId]);

  const ketUrl = useMemo((): string => {
    const oauthUrl = new URL("/dbitbucket", window.location.origin);
    oauthUrl.searchParams.set("subject", organizationId);

    return oauthUrl.toString();
  }, [organizationId]);

  const azureUrl = useMemo((): string => {
    const oauthUrl = new URL("/dazure", window.location.origin);
    oauthUrl.searchParams.set("subject", organizationId);

    return oauthUrl.toString();
  }, [organizationId]);

  const openLabUrl = useCallback((): void => {
    openUrl(labUrl, false);
  }, [labUrl]);

  const openHubUrl = useCallback((): void => {
    openUrl(hubUrl, false);
  }, [hubUrl]);

  const openKetUrl = useCallback((): void => {
    openUrl(ketUrl, false);
  }, [ketUrl]);

  const openAzureUrl = useCallback((): void => {
    openUrl(azureUrl, false);
  }, [azureUrl]);

  const providersHandlers: Partial<
    Record<TOAuthProvider, { onClick: MouseEventHandler<HTMLOrSVGElement> }>
  > = {
    Azure: {
      onClick: openAzureUrl,
    },
    Bitbucket: {
      onClick: openKetUrl,
    },
    GitHub: {
      onClick: openHubUrl,
    },
    GitLab: {
      onClick: openLabUrl,
    },
  };

  const visibleProviders = providers.reduce(
    (
      result,
      currentProvider,
    ): Partial<
      Record<TOAuthProvider, { onClick: MouseEventHandler<HTMLOrSVGElement> }>
    > => {
      return {
        ...result,
        [currentProvider]: providersHandlers[currentProvider],
      };
    },
    {},
  );

  return (
    <Can do={"integrates_api_mutations_add_credentials_mutate"}>
      <OAuthSelector
        buttonLabel={t(
          "organization.tabs.credentials.actionButtons.addButton.text",
        )}
        id={"repositories-providers"}
        manualOption={{
          label: t("components.repositoriesDropdown.manual.text"),
          onClick: onAdd,
        }}
        providers={visibleProviders}
      />
    </Can>
  );
};

export { CredentialSelector };
