interface ISecretsHttpsCredentials {
  password: string | undefined;
  type: string;
  user: string | undefined;
}

interface ISecretsAwsRole {
  arn: string;
  type: string;
}

interface ISecretsTokenCredentials {
  azureOrganization: string | undefined;
  isPat: boolean;
  token: string | undefined;
  type: string;
}

interface ISecretsSshCredentials {
  key: string;
  type: string;
}
type TSecretsCredentials =
  | ISecretsAwsRole
  | ISecretsHttpsCredentials
  | ISecretsSshCredentials
  | ISecretsTokenCredentials;
interface IAddCredentialsVariables {
  organizationId: string;
  credentials: TSecretsCredentials;
}

interface ICredentialsAttr {
  arn?: string;
  azureOrganization: string | null;
  id: string;
  isPat: boolean;
  isToken: boolean;
  name: string;
  oauthType: string;
  owner: string;
  type: string;
}

interface ICredentialsData {
  usedBy: (string | undefined)[];
  formattedType: string;
  __typename: "Credentials";
  arn?: string | null;
  azureOrganization: string | null;
  id: string;
  isPat: boolean | null;
  isToken: boolean | null;
  name: string;
  oauthType: string;
  owner: string;
  type: string;
}

interface IOrganizationAttr {
  id: string;
  name: string;
}

interface IOrganizationCredentialsProps {
  organizationId: string;
}

interface IUpdateCredentialsVariables {
  organizationId: string;
  credentials: TSecretsCredentials;
  credentialsId: string;
}

export type {
  IUpdateCredentialsVariables,
  IAddCredentialsVariables,
  ICredentialsAttr,
  ICredentialsData,
  IOrganizationAttr,
  IOrganizationCredentialsProps,
  TSecretsCredentials,
};
