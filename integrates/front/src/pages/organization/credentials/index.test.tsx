import { PureAbility } from "@casl/ability";
import { screen, waitFor } from "@testing-library/react";
import { userEvent } from "@testing-library/user-event";
import { GraphQLError } from "graphql";
import { HttpResponse, type StrictResponse, graphql } from "msw";

import {
  ADD_CREDENTIALS,
  GET_ORGANIZATION_CREDENTIALS_AND_USAGE,
  GET_ORGANIZATION_EXTERNAL_ID_BY_ID,
  REMOVE_CREDENTIALS,
  UPDATE_CREDENTIALS,
} from "./queries";

import { OrganizationCredentials } from ".";
import { authContext } from "context/auth";
import { authzPermissionsContext } from "context/authz/config";
import type {
  AddCredentialsMutationMutation as AddCredentials,
  GetOrgCredentialsAndUsageQuery as GetOrganizationCredentials,
  GetOrganizationExternalIdByIdQuery as GetOrganizationExternalId,
  RemoveCredentialsMutationMutation as RemoveCredentials,
  UpdateCredentialsMutationMutation as UpdateCredentials,
} from "gql/graphql";
import { render } from "mocks";
import { LINK } from "mocks/constants";
import type { IErrorMessage, IMessage } from "mocks/types";
import { msgError, msgSuccess } from "utils/notifications";
import {
  expectOptionHaveValue,
  getInputByName,
  queryByInputName,
  selectOption,
} from "utils/test-utils";

jest.mock("utils/notifications", (): Record<string, unknown> => {
  const mockedNotifications: Record<string, () => Record<string, unknown>> =
    jest.requireActual("utils/notifications");
  jest.spyOn(mockedNotifications, "msgError").mockImplementation();
  jest.spyOn(mockedNotifications, "msgSuccess").mockImplementation();

  return mockedNotifications;
});

describe("organization credentials view", (): void => {
  const graphqlMocked = graphql.link(LINK);

  const getOrgExternalIdHandler = [
    graphqlMocked.query(
      GET_ORGANIZATION_EXTERNAL_ID_BY_ID,
      (): StrictResponse<{
        data: GetOrganizationExternalId;
      }> =>
        HttpResponse.json({
          data: {
            organization: {
              __typename: "Organization",
              awsExternalId: "6e52c11c-abf7-4ca3-b7d0-635e394f41c1",
              name: "org-test",
            },
          },
        }),
      { once: true },
    ),
  ];

  const mockGetCredentialsQuery = [
    graphqlMocked.query(
      GET_ORGANIZATION_CREDENTIALS_AND_USAGE,
      (): StrictResponse<
        IErrorMessage | { data: GetOrganizationCredentials }
      > => {
        return HttpResponse.json({
          data: {
            organization: {
              __typename: "Organization",
              credentials: [
                {
                  __typename: "Credentials",
                  azureOrganization: null,
                  id: "6e52c11c-abf7-4ca3-b7d0-635e394f41c1",
                  isPat: false,
                  isToken: false,
                  name: "Credentials test",
                  oauthType: "",
                  owner: "owner@test.com",
                  type: "HTTPS",
                },
              ],
              groups: [
                {
                  __typename: "Group",
                  dockerImages: [],
                  gitRoots: {
                    __typename: "GitRootsConnection",
                    edges: [
                      {
                        __typename: "GitRootEdge",
                        node: {
                          __typename: "GitRoot",
                          credentials: {
                            __typename: "Credentials",
                            id: "6e52c11c-abf7-4ca3-b7d0-635e394f41c1",
                            name: "Credentials test",
                          },
                          nickname: "repo_1",
                        },
                      },
                    ],
                  },
                  name: "group-test",
                },
              ],
              name: "org-test",
            },
          },
        });
      },
    ),
  ];

  it("should list organization's credentials", async (): Promise<void> => {
    expect.hasAssertions();

    jest.clearAllMocks();

    const mockedPermissions = new PureAbility<string>([
      { action: "integrates_api_mutations_add_credentials_mutate" },
    ]);

    const { container } = render(
      <authzPermissionsContext.Provider value={mockedPermissions}>
        <OrganizationCredentials
          organizationId={"ORG#15eebe68-e9ce-4611-96f5-13d6562687e1"}
        />
      </authzPermissionsContext.Provider>,
      { mocks: [...getOrgExternalIdHandler, ...mockGetCredentialsQuery] },
    );
    await waitFor((): void => {
      expect(screen.getByText("Credentials test")).toBeInTheDocument();
    });

    expect(screen.getByText("owner@test.com")).toBeInTheDocument();

    await waitFor((): void => {
      expect(screen.getByText("Used by repo_1")).toBeInTheDocument();
    });

    expect(
      screen.queryByText("profile.credentialsModal.title"),
    ).not.toBeInTheDocument();

    await userEvent.click(
      screen.getByText(
        "organization.tabs.credentials.actionButtons.addButton.text",
      ),
    );

    const repoDropdown = container.querySelectorAll(
      "#repositories-providers",
    )[0].children;

    expect(repoDropdown).toHaveLength(5);

    await userEvent.click(repoDropdown[4]);

    await waitFor((): void => {
      expect(
        screen.queryByText("profile.credentialsModal.title"),
      ).toBeInTheDocument();
    });

    expectOptionHaveValue("typeCredential", "SSH");

    expect(getInputByName("key")).toBeInTheDocument();

    expect(queryByInputName("password")).not.toBeInTheDocument();
    expect(queryByInputName("azureOrganization")).not.toBeInTheDocument();

    await selectOption("typeCredential", "TOKEN");
    await waitFor((): void => {
      expect(getInputByName("azureOrganization")).toBeInTheDocument();
    });

    expect(queryByInputName("key")).not.toBeInTheDocument();
    expect(queryByInputName("password")).not.toBeInTheDocument();

    await selectOption("typeCredential", "USER");
    await waitFor((): void => {
      expect(getInputByName("password")).toBeInTheDocument();
    });

    expect(queryByInputName("key")).not.toBeInTheDocument();
    expect(queryByInputName("azureOrganization")).not.toBeInTheDocument();
  });

  it("should render errors", async (): Promise<void> => {
    expect.hasAssertions();

    jest.clearAllMocks();

    const mockOrgExternalIdHandlerError = [
      graphqlMocked.query(
        GET_ORGANIZATION_EXTERNAL_ID_BY_ID,
        (): StrictResponse<Record<"errors", IMessage[]>> => {
          return HttpResponse.json({
            errors: [
              new GraphQLError("Couldn't load organization external id"),
            ],
          });
        },
      ),
    ];

    const mockError = [
      graphqlMocked.mutation(
        ADD_CREDENTIALS,
        (): StrictResponse<Record<"errors", IMessage[]>> => {
          return HttpResponse.json({
            errors: [
              new GraphQLError(
                "Exception - A credential exists with the same name",
              ),
              new GraphQLError(
                "Exception - Field cannot fill with blank characters",
              ),
              new GraphQLError(
                "Exception - Password should start with a letter",
              ),
              new GraphQLError(
                "Exception - Password should include at least one number",
              ),
              new GraphQLError(
                "Exception - Password should include lowercase characters",
              ),
              new GraphQLError(
                "Exception - Password should include uppercase characters",
              ),
              new GraphQLError(
                "Exception - Password should not include sequentials characters",
              ),
              new GraphQLError(
                "Exception - Cannot assume cross account IAM role",
              ),
              new GraphQLError("Couldn't add the credential"),
            ],
          });
        },
      ),
    ];

    const mockedPermissions = new PureAbility<string>([
      { action: "integrates_api_mutations_add_credentials_mutate" },
    ]);
    const mockedAuth = {
      awsSubscription: null,
      tours: {
        newGroup: false,
        newRoot: false,
        welcome: true,
      },
      userEmail: "owner@test.com",
      userName: "owner",
    };

    const { container } = render(
      <authzPermissionsContext.Provider value={mockedPermissions}>
        <authContext.Provider value={mockedAuth}>
          <OrganizationCredentials
            organizationId={"ORG#12c44fe4-4171-4f49-9cba-ef8b58790cbd"}
          />
        </authContext.Provider>
      </authzPermissionsContext.Provider>,
      {
        mocks: [
          ...mockOrgExternalIdHandlerError,
          ...mockGetCredentialsQuery,
          ...mockError,
        ],
      },
    );
    await waitFor((): void => {
      expect(screen.queryByRole("table")).toBeInTheDocument();
    });

    await userEvent.click(
      screen.getByText(
        "organization.tabs.credentials.actionButtons.addButton.text",
      ),
    );

    const repoDropdown = container.querySelectorAll(
      "#repositories-providers",
    )[0].children;

    expect(repoDropdown).toHaveLength(5);

    await userEvent.click(
      screen.getByText("components.repositoriesDropdown.manual.text"),
    );

    expect(msgError).toHaveBeenCalledWith("groupAlerts.errorTextsad");

    await userEvent.type(getInputByName("name"), "New name");
    await selectOption("typeCredential", "TOKEN");
    await userEvent.type(getInputByName("token"), "New token");
    await userEvent.type(getInputByName("azureOrganization"), "testorg1");
    await userEvent.click(
      screen.getByRole("button", {
        name: "organization.tabs.credentials.credentialsModal.form.add",
      }),
    );

    await waitFor((): void => {
      expect(msgError).toHaveBeenCalledTimes(10);
    });
  });

  it("should add credentials", async (): Promise<void> => {
    expect.hasAssertions();

    jest.clearAllMocks();

    const mockMutationAddCredentials = [
      graphqlMocked.mutation(
        ADD_CREDENTIALS,
        (): StrictResponse<{ data: AddCredentials }> => {
          return HttpResponse.json({
            data: {
              addCredentials: { success: true },
            },
          });
        },
      ),
    ];

    const mockedPermissions = new PureAbility<string>([
      { action: "integrates_api_mutations_add_credentials_mutate" },
    ]);
    const mockedAuth = {
      awsSubscription: null,
      tours: {
        newGroup: false,
        newRoot: false,
        welcome: true,
      },
      userEmail: "owner@test.com",
      userName: "owner",
    };
    const { container } = render(
      <authzPermissionsContext.Provider value={mockedPermissions}>
        <authContext.Provider value={mockedAuth}>
          <OrganizationCredentials
            organizationId={"ORG#15eebe68-e9ce-4611-96f5-13d6562687e1"}
          />
        </authContext.Provider>
      </authzPermissionsContext.Provider>,
      {
        mocks: [
          ...mockGetCredentialsQuery,
          ...getOrgExternalIdHandler,
          ...mockMutationAddCredentials,
        ],
      },
    );
    await waitFor((): void => {
      expect(screen.getByText("owner@test.com")).toBeInTheDocument();
    });

    await userEvent.click(
      screen.getByText(
        "organization.tabs.credentials.actionButtons.addButton.text",
      ),
    );

    const providersDropdown = container.querySelectorAll(
      "#repositories-providers",
    )[0].children;

    expect(providersDropdown).toHaveLength(5);

    await userEvent.click(providersDropdown[4]);
    await userEvent.type(getInputByName("name"), "New name");
    await selectOption("typeCredential", "TOKEN");
    await userEvent.type(getInputByName("token"), "New token");
    await userEvent.type(getInputByName("azureOrganization"), "testorg1");
    await userEvent.click(
      screen.getByRole("button", {
        name: "organization.tabs.credentials.credentialsModal.form.add",
      }),
    );
    await waitFor((): void => {
      expect(msgSuccess).toHaveBeenLastCalledWith(
        "organization.tabs.credentials.alerts.addSuccess",
        "groupAlerts.titleSuccess",
      );
    });
  });

  it("should remove credentials", async (): Promise<void> => {
    expect.hasAssertions();

    jest.clearAllMocks();

    const mockMutationRemoveCredentials = [
      graphqlMocked.mutation(
        REMOVE_CREDENTIALS,
        (): StrictResponse<{ data: RemoveCredentials }> => {
          return HttpResponse.json({
            data: {
              removeCredentials: { success: true },
            },
          });
        },
      ),
    ];

    const mockedPermissions = new PureAbility<string>([
      { action: "integrates_api_mutations_remove_credentials_mutate" },
    ]);
    const mockedAuth = {
      awsSubscription: null,
      tours: {
        newGroup: false,
        newRoot: false,
        welcome: true,
      },
      userEmail: "owner@test.com",
      userName: "owner",
    };
    render(
      <authzPermissionsContext.Provider value={mockedPermissions}>
        <authContext.Provider value={mockedAuth}>
          <OrganizationCredentials
            organizationId={"ORG#15eebe68-e9ce-4611-96f5-13d6562687e1"}
          />
        </authContext.Provider>
      </authzPermissionsContext.Provider>,
      {
        mocks: [
          ...getOrgExternalIdHandler,
          ...[
            graphqlMocked.query(
              GET_ORGANIZATION_CREDENTIALS_AND_USAGE,
              (): StrictResponse<
                IErrorMessage | { data: GetOrganizationCredentials }
              > => {
                return HttpResponse.json({
                  data: {
                    organization: {
                      __typename: "Organization",
                      credentials: [
                        {
                          __typename: "Credentials",
                          azureOrganization: null,
                          id: "6e52c11c-abf7-4ca3-b7d0-635e394f41c1",
                          isPat: false,
                          isToken: false,
                          name: "Credentials test",
                          oauthType: "",
                          owner: "owner@test.com",
                          type: "HTTPS",
                        },
                      ],
                      groups: [
                        {
                          __typename: "Group",
                          dockerImages: [],
                          gitRoots: {
                            __typename: "GitRootsConnection",
                            edges: [],
                          },
                          name: "group-test",
                        },
                      ],
                      name: "org-test",
                    },
                  },
                });
              },
            ),
          ],
          ...mockMutationRemoveCredentials,
        ],
      },
    );
    await waitFor((): void => {
      expect(screen.queryByText("owner@test.com")).toBeInTheDocument();
    });
    await userEvent.click(screen.getByRole("radio"));
    await userEvent.click(
      screen.getByRole("button", {
        name: "organization.tabs.credentials.actionButtons.removeButton.text",
      }),
    );
    await userEvent.click(
      screen.getByRole("button", {
        name: "buttons.confirm",
      }),
    );
    await waitFor((): void => {
      expect(msgSuccess).toHaveBeenLastCalledWith(
        "organization.tabs.credentials.alerts.removeSuccess",
        "groupAlerts.titleSuccess",
      );
    });
  });

  it("should display warning msg", async (): Promise<void> => {
    expect.hasAssertions();

    jest.clearAllMocks();

    const mockedPermissions = new PureAbility<string>([
      { action: "integrates_api_mutations_remove_credentials_mutate" },
    ]);
    const mockedAuth = {
      awsSubscription: null,
      tours: {
        newGroup: false,
        newRiskExposure: true,
        newRoot: false,
        welcome: true,
      },
      userEmail: "owner@test.com",
      userName: "owner",
    };
    render(
      <authzPermissionsContext.Provider value={mockedPermissions}>
        <authContext.Provider value={mockedAuth}>
          <OrganizationCredentials
            organizationId={"ORG#15eebe68-e9ce-4611-96f5-13d6562687e1"}
          />
        </authContext.Provider>
      </authzPermissionsContext.Provider>,
      {
        mocks: [...getOrgExternalIdHandler, ...mockGetCredentialsQuery],
      },
    );
    await waitFor((): void => {
      expect(screen.queryByText("owner@test.com")).toBeInTheDocument();
    });
    await userEvent.click(screen.getByRole("radio"));
    await userEvent.click(
      screen.getByRole("button", {
        name: "organization.tabs.credentials.actionButtons.removeButton.text",
      }),
    );
    await waitFor((): void => {
      expect(
        screen.queryByText(
          "organization.tabs.credentials.actionButtons.removeButton.errorWarning",
        ),
      ).toBeInTheDocument();
    });
  });

  it("should edit credentials user and password", async (): Promise<void> => {
    expect.hasAssertions();

    jest.clearAllMocks();

    const mockMutationUpdateCredentials = [
      graphqlMocked.mutation(
        UPDATE_CREDENTIALS,
        (): StrictResponse<{ data: UpdateCredentials }> => {
          return HttpResponse.json({
            data: {
              updateCredentials: { success: true },
            },
          });
        },
      ),
    ];

    const mockedPermissions = new PureAbility<string>([
      { action: "integrates_api_mutations_update_credentials_mutate" },
    ]);
    const mockedAuth = {
      awsSubscription: null,
      tours: {
        newGroup: false,
        newRoot: false,
        welcome: true,
      },
      userEmail: "owner@test.com",
      userName: "owner",
    };
    render(
      <authzPermissionsContext.Provider value={mockedPermissions}>
        <authContext.Provider value={mockedAuth}>
          <OrganizationCredentials
            organizationId={"ORG#15eebe68-e9ce-4611-96f5-13d6562687e1"}
          />
        </authContext.Provider>
      </authzPermissionsContext.Provider>,
      {
        mocks: [
          ...getOrgExternalIdHandler,
          ...mockGetCredentialsQuery,
          ...mockMutationUpdateCredentials,
        ],
      },
    );
    await waitFor((): void => {
      expect(screen.queryByText("owner@test.com")).toBeInTheDocument();
    });
    await userEvent.click(screen.getByRole("radio"));
    await userEvent.click(
      screen.getByRole("button", {
        name: "organization.tabs.credentials.actionButtons.editButton.text",
      }),
    );
    await userEvent.click(screen.getByRole("checkbox", { name: "newSecrets" }));
    await selectOption("typeCredential", "USER");
    const userInput = getInputByName("user");
    await userEvent.clear(userInput);
    await userEvent.type(userInput, "User test");
    const passwordInput = getInputByName("password");

    await userEvent.clear(passwordInput);
    await userEvent.type(
      passwordInput,
      "lorem.ipsum,Dolor.sit:am3t;consectetur@adipiscing$elit",
    );
    await userEvent.click(
      screen.getByRole("button", {
        name: "organization.tabs.credentials.credentialsModal.form.edit",
      }),
    );
    await waitFor((): void => {
      expect(msgSuccess).toHaveBeenLastCalledWith(
        "organization.tabs.credentials.alerts.editSuccess",
        "groupAlerts.titleSuccess",
      );
    });
  });
});
