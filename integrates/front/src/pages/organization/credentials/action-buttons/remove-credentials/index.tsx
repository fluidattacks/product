import { Alert, Button, Modal, useModal } from "@fluidattacks/design";
import { isEmpty, isUndefined } from "lodash";
import { Fragment, useCallback, useMemo } from "react";
import { useTranslation } from "react-i18next";

import type { IActionButtonsProps } from "../types";
import { Authorize } from "components/@core/authorize";

const RemoveButton = ({
  isAdding,
  isEditing,
  isRemoving,
  onRemove,
  selectedCredentials,
}: Readonly<IActionButtonsProps>): JSX.Element => {
  const prefix = "organization.tabs.credentials.actionButtons.";
  const { t } = useTranslation();
  const disabled = isAdding || isEditing || isRemoving;
  const modalRef = useModal("remove-credential-modal");
  const usedBy = useMemo((): string[] => {
    if (
      selectedCredentials === undefined ||
      isEmpty(selectedCredentials.usedBy)
    ) {
      return [];
    }

    return selectedCredentials.usedBy
      .filter((group): boolean => group !== undefined)
      .map(String);
  }, [selectedCredentials]);
  const canConfirm = useMemo((): boolean => usedBy.length === 0, [usedBy]);

  const confirmButton = t("buttons.confirm");
  const cancelButton = t("organization.tabs.users.removeButton.cancel");

  const onClose = useCallback((): void => {
    modalRef.close();
  }, [modalRef]);

  const handleClick = useCallback((): void => {
    modalRef.open();
  }, [modalRef]);

  return (
    <Fragment>
      <Modal
        cancelButton={{
          onClick: onClose,
          text: cancelButton,
        }}
        confirmButton={
          canConfirm
            ? {
                onClick: onRemove,
                text: confirmButton,
              }
            : undefined
        }
        description={
          canConfirm ? (
            t(`${prefix}removeButton.confirmMessage`, {
              credentialName: selectedCredentials?.name,
            })
          ) : (
            <Fragment>
              <Alert variant={"warning"}>
                {t(`${prefix}removeButton.errorWarning`)}
              </Alert>
              <br />
              {t(`${prefix}removeButton.error`, { roots: usedBy.join(", ") })}
            </Fragment>
          )
        }
        modalRef={modalRef}
        size={"md"}
        title={t(`${prefix}removeButton.confirmTitle`)}
      />
      <Authorize can={"integrates_api_mutations_remove_credentials_mutate"}>
        <Button
          disabled={disabled || isUndefined(selectedCredentials)}
          icon={"trash-alt"}
          id={"removeCredentials"}
          onClick={handleClick}
          tooltip={t(`${prefix}removeButton.tooltip`)}
          variant={"ghost"}
        >
          {t(`${prefix}removeButton.text`)}
        </Button>
      </Authorize>
    </Fragment>
  );
};

export { RemoveButton };
