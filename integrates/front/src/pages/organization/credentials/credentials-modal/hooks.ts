import type { ApolloError } from "@apollo/client";

import { Logger } from "utils/logger";
import { msgError } from "utils/notifications";
import { translate } from "utils/translations/translate";

const handleAddCredentialsError = (errors: ApolloError): void => {
  errors.graphQLErrors.forEach((error): void => {
    switch (error.message) {
      case "Exception - A credential exists with the same name":
        msgError(translate.t("validations.invalidCredentialName"));
        break;
      case "Exception - Field cannot fill with blank characters":
        msgError(translate.t("validations.invalidSpaceField"));
        break;
      case "Exception - Password should start with a letter":
        msgError(translate.t("validations.credentialsModal.startWithLetter"));
        break;
      case "Exception - Password should include at least one number":
        msgError(translate.t("validations.credentialsModal.includeNumber"));
        break;
      case "Exception - Password should include lowercase characters":
        msgError(translate.t("validations.credentialsModal.includeLowercase"));
        break;
      case "Exception - Password should include uppercase characters":
        msgError(translate.t("validations.credentialsModal.includeUppercase"));
        break;
      case "Exception - Password should include symbols characters":
        msgError(translate.t("validations.credentialsModal.includeSymbols"));
        break;
      case "Exception - Password should not include sequentials characters":
        msgError(
          translate.t("validations.credentialsModal.sequentialsCharacters"),
        );
        break;
      case "Exception - Cannot assume cross account IAM role":
        msgError(translate.t("validations.invalidAwsRole"));
        break;
      default:
        msgError(translate.t("groupAlerts.errorTextsad"));
        Logger.warning("An error occurred adding credential", error);
    }
  });
};

const handleUpdateCredentialsError = (errors: ApolloError): void => {
  errors.graphQLErrors.forEach((error): void => {
    switch (error.message) {
      case "Exception - A credential exists with the same name":
        msgError(translate.t("validations.invalidCredentialName"));
        break;
      case "Exception - Field cannot fill with blank characters":
        msgError(translate.t("validations.invalidSpaceField"));
        break;
      default:
        msgError(translate.t("groupAlerts.errorTextsad"));
        Logger.warning("An error occurred editing credentials", error);
    }
  });
};

export { handleAddCredentialsError, handleUpdateCredentialsError };
