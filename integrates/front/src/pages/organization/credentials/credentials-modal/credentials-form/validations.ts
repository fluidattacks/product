import type { InferType, Schema } from "yup";
import { lazy, object, string } from "yup";

import type { IFormValues } from "./types";

import { translate } from "utils/translations/translate";

const MAX_FIELD_LENGTH = 1800;
const MIN_FIELD_LENGTH = 20;
const REGEX_VALIDATIONS = {
  AZURE_ORGANIZATION_NAME: /^[a-zA-Z0-9][a-zA-Z0-9-]{0,49}$/u,
  CREDENTIAL_NAME: /^(?:(?!oauth).)*$/gimu,
  SSH_FORMAT:
    /^-{5}BEGIN OPENSSH PRIVATE KEY-{5}\n(?:[a-zA-Z0-9+/=]+\n)+-{5}END OPENSSH PRIVATE KEY-{5}\n?$/u,
  VALIDATE_INCLUDE_NUMBER: /\d/u,
  VALIDATE_INCLUDE_SYMBOLS: /['~:;%@_$#!,.*\-?"[\]|()/{}>^<=&+`]/u,
  VALIDATE_LOWER_CASE: /[a-z]/u,
  VALIDATE_START_VALUE: /^[a-zA-Z]/u,
  VALIDATE_UPPER_CASE: /[A-Z]/u,
};

const validateArnFormat = ([newSecrets, type]: unknown[]): Schema => {
  return Boolean(newSecrets) && String(type) === "AWSROLE"
    ? string()
        .required(translate.t("validations.required"))
        .isValidValue()
        .isValidArnFormat()
    : string();
};

const validateAzureOrgName = (
  [newSecrets, type, isPat]: unknown[],
  values: IFormValues,
): Schema => {
  const typeProtocol: string = values.auth === "TOKEN" ? "HTTPS" : "";

  return Boolean(newSecrets) && String(type) === typeProtocol && Boolean(isPat)
    ? string()
        .required(translate.t("validations.required"))
        .isValidValue(
          translate.t("validations.invalidAzureOrganizationName"),
          REGEX_VALIDATIONS.AZURE_ORGANIZATION_NAME,
        )
    : string();
};

const isRequired = (
  [newSecrets, type]: unknown[],
  typeProtocol: string,
): Schema => {
  return Boolean(newSecrets) && type === typeProtocol
    ? string().required(translate.t("validations.required")).isValidValue()
    : string();
};

const validationSchema = (): InferType<Schema> =>
  lazy(
    (values: IFormValues): Schema =>
      object({
        arn: string().when(
          ["newSecrets", "type"],
          ([newSecrets, type]: unknown[]): Schema => {
            return validateArnFormat([newSecrets, type]);
          },
        ),
        auth: string(),
        azureOrganization: string().when(
          ["newSecrets", "type", "isPat"],
          ([newSecrets, type, isPat]: unknown[]): Schema => {
            return validateAzureOrgName([newSecrets, type, isPat], values);
          },
        ),
        key: string().when(
          ["newSecrets", "type"],
          ([newSecrets, type]: unknown[]): Schema => {
            return Boolean(newSecrets) && String(type) === "SSH"
              ? string()
                  .required(translate.t("validations.required"))
                  .isValidSSHFormat(REGEX_VALIDATIONS.SSH_FORMAT, values)
              : string();
          },
        ),
        name: string()
          .when("type", ([type]: unknown[]): Schema => {
            return type === undefined
              ? string()
              : string()
                  .required(translate.t("validations.required"))
                  .isValidValue();
          })
          .isValidValue(
            translate.t("validations.invalidCredentialNameValue"),
            REGEX_VALIDATIONS.CREDENTIAL_NAME,
          ),
        password: string().when(
          ["newSecrets", "type"],
          ([newSecrets, type]: unknown[]): Schema => {
            const typeProtocol: string = values.auth === "USER" ? "HTTPS" : "";

            return Boolean(newSecrets) && String(type) === typeProtocol
              ? string()
                  .required(translate.t("validations.required"))
                  .isValidValue()
                  .isValidValue(
                    translate.t("validations.credentialsModal.startWithLetter"),
                    REGEX_VALIDATIONS.VALIDATE_START_VALUE,
                  )
                  .isValidValue(
                    translate.t("validations.credentialsModal.includeNumber"),
                    REGEX_VALIDATIONS.VALIDATE_INCLUDE_NUMBER,
                  )
                  .isValidValue(
                    translate.t(
                      "validations.credentialsModal.includeLowercase",
                    ),
                    REGEX_VALIDATIONS.VALIDATE_LOWER_CASE,
                  )
                  .isValidValue(
                    translate.t(
                      "validations.credentialsModal.includeUppercase",
                    ),
                    REGEX_VALIDATIONS.VALIDATE_UPPER_CASE,
                  )
                  .isValidValue(
                    translate.t("validations.credentialsModal.includeSymbols"),
                    REGEX_VALIDATIONS.VALIDATE_INCLUDE_SYMBOLS,
                  )
                  .min(
                    MIN_FIELD_LENGTH,
                    translate.t("validations.minLength", {
                      count: MIN_FIELD_LENGTH,
                    }),
                  )
                  .max(
                    MAX_FIELD_LENGTH,
                    translate.t("validations.maxLength", {
                      count: MAX_FIELD_LENGTH,
                    }),
                  )
              : string();
          },
        ),
        token: string().when(
          ["newSecrets", "type"],
          ([newSecrets, type]: unknown[]): Schema => {
            const typeProtocol: string = values.auth === "TOKEN" ? "HTTPS" : "";

            return isRequired([newSecrets, String(type)], typeProtocol);
          },
        ),
        type: string().required(translate.t("validations.required")),
        user: string().when(
          ["newSecrets", "type"],
          ([newSecrets, type]: unknown[]): Schema => {
            const typeProtocol: string = values.auth === "USER" ? "HTTPS" : "";

            return isRequired([newSecrets, type], typeProtocol);
          },
        ),
      }),
  );

export { validationSchema };
