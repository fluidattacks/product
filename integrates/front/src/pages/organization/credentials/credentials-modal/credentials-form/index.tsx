import { useQuery } from "@apollo/client";
import { Formik } from "formik";
import isUndefined from "lodash/isUndefined";
import { useMemo } from "react";
import * as React from "react";
import { useTranslation } from "react-i18next";

import type { ICredentialsFormProps, IFormValues } from "./types";
import { UpdateCredentials } from "./update-credentials";
import { validationSchema } from "./validations";

import { GET_ORGANIZATION_EXTERNAL_ID_BY_ID } from "../../queries";
import { Logger } from "utils/logger";
import { msgError } from "utils/notifications";

const CredentialsForm: React.FC<ICredentialsFormProps> = ({
  initialValues,
  isAdding,
  isEditing,
  onCancel,
  onSubmit,
  organizationId,
}): JSX.Element => {
  const { t } = useTranslation();

  // GraphQl queries
  const { data } = useQuery(GET_ORGANIZATION_EXTERNAL_ID_BY_ID, {
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error): void => {
        msgError(t("groupAlerts.errorTextsad"));
        Logger.error("Couldn't load organization external ID", error);
      });
    },
    variables: {
      organizationId,
    },
  });
  const externalId = useMemo(
    (): string => (isUndefined(data) ? "" : data.organization.awsExternalId),
    [data],
  );
  const defaultInitialValues: IFormValues = {
    arn: "",
    auth: "TOKEN",
    azureOrganization: "",
    isPat: false,
    key: "",
    name: "",
    newSecrets: true,
    password: "",
    token: "",
    type: "SSH",
    typeCredential: "SSH",
    user: "",
  };

  return (
    <Formik
      enableReinitialize={true}
      initialValues={
        isUndefined(initialValues) ? defaultInitialValues : initialValues
      }
      name={"credentials"}
      onSubmit={onSubmit}
      validationSchema={validationSchema()}
    >
      {({ values, isSubmitting, dirty, setFieldValue }): JSX.Element => {
        return (
          <UpdateCredentials
            dirty={dirty}
            externalId={externalId}
            isAdding={isAdding}
            isEditing={isEditing}
            isSubmitting={isSubmitting}
            onCancel={onCancel}
            setFieldValue={setFieldValue}
            values={values}
          />
        );
      }}
    </Formik>
  );
};

export { CredentialsForm };
