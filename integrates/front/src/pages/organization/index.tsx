import { useContext } from "react";
import * as React from "react";
import { Navigate, Route, Routes } from "react-router-dom";

import { OrganizationAnalytics } from "./analytics";
import { OrganizationBilling } from "./billing";
import { OrganizationCompliance } from "./compliance";
import { organizationDataContext } from "./context";
import { OrganizationCredentials } from "./credentials";
import { OrganizationGroups } from "./groups";
import { useOrgUrl } from "./hooks";
import { OrganizationLogs } from "./logs";
import { OrganizationMembers } from "./members";
import { OrganizationOutside } from "./outside";
import { OrganizationPolicies } from "./policies";
import { OrganizationPortfolios } from "./portfolios";

import { useTabTracking } from "hooks";
import { Mailmap } from "pages/organization/mailmap";

const Organization: React.FC = (): JSX.Element => {
  const { organizationId, portfolios } = useContext(organizationDataContext);
  const { url } = useOrgUrl();
  useTabTracking("Organization");

  return (
    <Routes>
      <Route
        element={<OrganizationAnalytics organizationId={organizationId} />}
        path={`/analytics`}
      />
      <Route
        element={<OrganizationGroups organizationId={organizationId} />}
        path={`/groups`}
      />
      <Route
        element={
          <OrganizationPortfolios
            organizationId={organizationId}
            portfolios={portfolios}
          />
        }
        path={`/portfolios`}
      />
      <Route
        element={<OrganizationMembers organizationId={organizationId} />}
        path={`/members`}
      />
      <Route
        element={<OrganizationPolicies organizationId={organizationId} />}
        path={`/policies/*`}
      />
      <Route
        element={<Mailmap organizationId={organizationId} />}
        path={`/mailmap/*`}
      />
      <Route
        element={<OrganizationBilling organizationId={organizationId} />}
        path={`/billing/*`}
      />
      <Route
        element={<OrganizationOutside organizationId={organizationId} />}
        path={`/outside`}
      />
      <Route
        element={<OrganizationCredentials organizationId={organizationId} />}
        path={`/credentials`}
      />
      <Route
        element={<OrganizationCompliance organizationId={organizationId} />}
        path={`/compliance/*`}
      />
      <Route
        element={<OrganizationLogs organizationId={organizationId} />}
        path={`/logs/*`}
      />
      <Route
        element={<Navigate to={`${url}/members`} />}
        path={`${url}/stakeholders`}
      />
      <Route element={<Navigate to={`${url}/groups`} />} path={"*"} />
    </Routes>
  );
};

export { Organization };
