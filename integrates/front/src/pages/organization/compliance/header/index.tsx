import type { ITabProps } from "@fluidattacks/design/dist/components/tabs/types";
import { useTranslation } from "react-i18next";

import type { IComplianceHeaderProps } from "../types";
import { SectionHeader } from "components/section-header";

const ComplianceHeader = ({
  url,
}: Readonly<IComplianceHeaderProps>): JSX.Element => {
  const { t } = useTranslation();

  const tabs: ITabProps[] = [
    {
      id: "overview-tab",
      label: t("organization.tabs.compliance.header.tabs.overview.text"),
      link: `${url}/overview`,
      tooltip: t("organization.tabs.compliance.tabs.overview.tooltip"),
    },
    {
      id: "standards-tab",
      label: t("organization.tabs.compliance.header.tabs.standards.text"),
      link: `${url}/standards`,
      tooltip: t("organization.tabs.compliance.tabs.standards.tooltip"),
    },
  ];

  return (
    <SectionHeader
      header={t("organization.tabs.compliance.header.title")}
      tabs={tabs}
    />
  );
};

export { ComplianceHeader };
