import { graphql } from "gql";

const GET_ORGANIZATION_COMPLIANCE = graphql(`
  query GetOrganizationCompliance($organizationId: String!) {
    organization(organizationId: $organizationId) {
      __typename
      name
      compliance {
        __typename
        complianceLevel
        complianceWeeklyTrend
        estimatedDaysToFullCompliance
        standards {
          __typename
          avgOrganizationComplianceLevel
          bestOrganizationComplianceLevel
          complianceLevel
          standardTitle
          worstOrganizationComplianceLevel
        }
      }
    }
  }
`);

export { GET_ORGANIZATION_COMPLIANCE };
