import { Container, Loading } from "@fluidattacks/design";
import { Navigate, Route, Routes } from "react-router-dom";
import { useTheme } from "styled-components";

import { Benchmark } from "./benchmark";
import { ComplianceHeader } from "./header";
import { useOrganizationComplianceQuery } from "./hooks";
import { ComplianceIndicators } from "./indicators";
import type { IStandardComplianceAttr } from "./types";
import { UnfilledStandards } from "./unfilled-standards";

import { useOrgUrl } from "pages/organization/hooks";

interface IOrganizationComplianceProps {
  organizationId: string;
}

const OrganizationCompliance = ({
  organizationId,
}: Readonly<IOrganizationComplianceProps>): JSX.Element => {
  const { url } = useOrgUrl("/compliance/*");
  const theme = useTheme();

  const { loading, organization } =
    useOrganizationComplianceQuery(organizationId);

  if (loading || organization === undefined)
    return (
      <Container
        alignItems={"center"}
        display={"flex"}
        justify={"center"}
        left={"50%"}
        position={"absolute"}
        top={"50%"}
      >
        <Loading size={"lg"} />
      </Container>
    );

  const standards = organization.compliance.standards.map(
    (standard): IStandardComplianceAttr => ({
      avgOrganizationComplianceLevel: standard.avgOrganizationComplianceLevel,
      bestOrganizationComplianceLevel: standard.bestOrganizationComplianceLevel,
      complianceLevel: standard.complianceLevel,
      standardTitle: standard.standardTitle,
      worstOrganizationComplianceLevel:
        standard.worstOrganizationComplianceLevel,
    }),
  );

  return (
    <Container width={"100%"}>
      <ComplianceHeader
        complianceLevel={organization.compliance.complianceLevel}
        estimatedDaysToFullCompliance={
          organization.compliance.estimatedDaysToFullCompliance
        }
        organizationName={organization.name}
        standards={standards}
        url={url}
      />
      <ComplianceIndicators
        complianceLevel={organization.compliance.complianceLevel}
        estimatedDaysToFullCompliance={
          organization.compliance.estimatedDaysToFullCompliance
        }
        organizationName={organization.name}
        standards={standards}
        url={url}
      />
      <Container
        bgColor={theme.palette.gray[50]}
        height={"100%"}
        pb={1.25}
        pl={1.25}
        pr={1.25}
        pt={1.25}
      >
        <Routes>
          <Route
            element={<Benchmark standards={standards} />}
            path={`/overview`}
          />
          <Route
            element={<UnfilledStandards organizationId={organizationId} />}
            path={`/standards`}
          />
          <Route
            element={<Navigate to={`${url}/overview`.replace("//", "/")} />}
            path={"*"}
          />
        </Routes>
      </Container>
    </Container>
  );
};

export { OrganizationCompliance };
