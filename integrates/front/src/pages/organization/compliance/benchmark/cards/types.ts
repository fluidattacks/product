interface IBenchmarkCardProps {
  avgOrg: number;
  bestOrg: number;
  myOrg: number;
  standard: string;
  worstOrg: number;
}

export type { IBenchmarkCardProps };
