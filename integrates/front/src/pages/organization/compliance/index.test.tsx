import { screen, waitFor } from "@testing-library/react";
import { userEvent } from "@testing-library/user-event";
import { GraphQLError } from "graphql";
import { HttpResponse, type StrictResponse, graphql } from "msw";
import { Route, Routes } from "react-router-dom";

import { GET_UNFULFILLED_STANDARD_REPORT_URL } from "./unfilled-standards/queries";

import { OrganizationCompliance } from ".";
import {
  ReportType,
  type RequestGroupReportAtComplianceStandardsQuery as RequestGroupReportAtComplianceStandards,
} from "gql/graphql";
import { render } from "mocks";
import { LINK } from "mocks/constants";
import type { IErrorMessage } from "mocks/types";
import { msgError, msgSuccess } from "utils/notifications";

jest.mock("utils/notifications", (): Record<string, unknown> => {
  const mockedNotifications: Record<string, () => Record<string, unknown>> =
    jest.requireActual("utils/notifications");

  jest.spyOn(mockedNotifications, "msgError").mockImplementation();
  jest.spyOn(mockedNotifications, "msgSuccess").mockImplementation();

  return mockedNotifications;
});

const Component = (): JSX.Element => (
  <Routes>
    <Route
      element={
        <OrganizationCompliance
          organizationId={"ORG#15eebe68-e9ce-4611-96f5-13d6562687e1"}
        />
      }
      path={"orgs/org-test/compliance/*"}
    />
  </Routes>
);

describe("organization Compliance view", (): void => {
  const graphqlMocked = graphql.link(LINK);

  const memoryRouter = {
    initialEntries: ["/orgs/org-test/compliance/overview"],
  };

  it("should display the organization compliance header (cards)", async (): Promise<void> => {
    expect.hasAssertions();

    render(<Component />, { memoryRouter });
    await waitFor((): void => {
      expect(
        screen.getByText("organization.tabs.compliance.header.title"),
      ).toBeInTheDocument();
    });

    expect(
      screen.getByText(
        "organization.tabs.compliance.header.cards.complianceLevel.description",
      ),
    ).toBeInTheDocument();
    expect(screen.getByText("30%")).toBeInTheDocument();
    expect(
      screen.getByText(
        "organization.tabs.compliance.header.cards.complianceWeeklyTrend.description",
      ),
    ).toBeInTheDocument();
    expect(
      screen.getByText(
        "organization.tabs.compliance.header.cards.etToFullCompliance.description",
      ),
    ).toBeInTheDocument();
    expect(
      screen.getByText(
        "organization.tabs.compliance.header.cards.standardWithLowestCompliance.percentage.description",
      ),
    ).toBeInTheDocument();
  });

  it("should display the organization compliance benchmark", async (): Promise<void> => {
    expect.hasAssertions();

    render(<Component />, { memoryRouter });
    await waitFor((): void => {
      expect(
        screen.getByText(
          "organization.tabs.compliance.content.benchmark.title",
        ),
      ).toBeInTheDocument();
    });

    expect(screen.getAllByText(/standardname1/iu)).toHaveLength(2);
    expect(screen.getAllByText("75%")).toHaveLength(2);
    expect(screen.getByText("99%")).toBeInTheDocument();
    expect(screen.getByText("50%")).toBeInTheDocument();
    expect(screen.getByText("20%")).toBeInTheDocument();
    expect(screen.getByText(/standardname2/iu)).toBeInTheDocument();
    expect(screen.getByText("88%")).toBeInTheDocument();
    expect(screen.getByText("80%")).toBeInTheDocument();
    expect(screen.getByText("60%")).toBeInTheDocument();
    expect(screen.getByText("0%")).toBeInTheDocument();
  });

  it("should display the organization compliance unfilled standards by group", async (): Promise<void> => {
    expect.hasAssertions();

    render(<Component />, { memoryRouter });

    await waitFor((): void => {
      expect(
        screen.getByText(
          "organization.tabs.compliance.header.tabs.standards.text",
        ),
      ).toBeInTheDocument();
    });
    await userEvent.click(
      screen.getByText(
        "organization.tabs.compliance.header.tabs.standards.text",
      ),
    );
    await waitFor((): void => {
      expect(
        screen.getAllByText(
          "organization.tabs.compliance.content.unfulfilledStandards.defaultOption",
        ),
      ).toHaveLength(2);
    });
    const dropdownsRef = screen.getAllByText(
      "organization.tabs.compliance.content.unfulfilledStandards.defaultOption",
    );

    expect(screen.getByText("standardname1")).toBeInTheDocument();
    expect(screen.getByText("standardname2")).toBeInTheDocument();
    // Each standard have tow unfulfilled requirements
    expect(
      screen.getAllByText(
        "organization.tabs.compliance.content.unfulfilledStandards.subtitle (2)",
      ),
    ).toHaveLength(2);
    expect(screen.queryAllByText(/requirement1/iu)).toHaveLength(0);
    expect(screen.queryByText(/requirement2/iu)).not.toBeInTheDocument();
    expect(screen.queryByText(/requirement3/iu)).not.toBeInTheDocument();

    await userEvent.click(dropdownsRef[0]);

    expect(screen.getAllByText(/requirement1/iu)).toHaveLength(1);
    expect(screen.getByText(/requirement2/iu)).toBeInTheDocument();

    await userEvent.click(dropdownsRef[1]);

    expect(screen.getByText(/requirement3/iu)).toBeInTheDocument();

    expect(screen.queryByText(/group2/iu)).not.toBeInTheDocument();

    expect(
      screen.queryByText(
        "organization.tabs.compliance.content.unfulfilledStandards.header.noGroups",
      ),
    ).not.toBeInTheDocument();

    const groupsDropdownRef = screen.getByText(/group1/iu);

    expect(groupsDropdownRef).toBeInTheDocument();

    await userEvent.click(groupsDropdownRef);
    await waitFor((): void => {
      expect(screen.getByText(/group2/iu)).toBeInTheDocument();
    });
  });

  it("should render generate report modal and send data report link", async (): Promise<void> => {
    expect.hasAssertions();

    jest.spyOn(console, "error").mockImplementation();
    jest.spyOn(console, "warn").mockImplementation();

    render(<Component />, {
      memoryRouter,
      mocks: [
        graphqlMocked.query(
          GET_UNFULFILLED_STANDARD_REPORT_URL,
          (): StrictResponse<
            IErrorMessage | { data: RequestGroupReportAtComplianceStandards }
          > => {
            return HttpResponse.json({
              data: {
                unfulfilledStandardReportUrl: "https://test.com/",
              },
            });
          },
        ),
      ],
    });

    await waitFor((): void => {
      expect(
        screen.getByText(
          "organization.tabs.compliance.header.tabs.standards.text",
        ),
      ).toBeInTheDocument();
    });
    await userEvent.click(
      screen.getByText(
        "organization.tabs.compliance.header.tabs.standards.text",
      ),
    );

    await waitFor((): void => {
      expect(
        screen.queryByText(
          "organization.tabs.compliance.content.unfulfilledStandards.bottom.text",
        ),
      ).toBeInTheDocument();
    });
    await userEvent.click(
      screen.getByText(
        "organization.tabs.compliance.content.unfulfilledStandards.bottom.text",
      ),
    );

    await waitFor((): void => {
      expect(
        screen.getByText(
          "organization.tabs.compliance.tabs.standards.generateReportModal.title",
        ),
      ).toBeInTheDocument();
    });

    expect(
      screen.getByText(
        "organization.tabs.compliance.tabs.standards.generateReportModal.buttons.generateReport.text",
      ),
    ).toBeInTheDocument();

    await userEvent.click(
      screen.getByRole("checkbox", {
        name: "AllStandards",
      }),
    );

    expect(
      screen.getByRole("button", {
        name: "organization.tabs.compliance.tabs.standards.generateReportModal.buttons.generateReport.text",
      }),
    ).toBeDisabled();

    await userEvent.click(
      screen.getByRole("checkbox", {
        name: "STANDARDNAME2",
      }),
    );

    expect(
      screen.getByRole("button", {
        name: "organization.tabs.compliance.tabs.standards.generateReportModal.buttons.generateReport.text",
      }),
    ).not.toBeDisabled();

    await userEvent.click(
      screen.getByRole("button", {
        name: "organization.tabs.compliance.tabs.standards.generateReportModal.buttons.generateReport.text",
      }),
    );

    expect(screen.getByText("verifyDialog.title")).toBeInTheDocument();

    await userEvent.click(
      screen.getByText("components.channels.smsButton.text"),
    );
    await userEvent.type(
      screen.getByRole("textbox", { name: "verificationCode" }),
      "1234",
    );
    await userEvent.click(screen.getByText("verifyDialog.verify"));

    await waitFor((): void => {
      expect(msgSuccess).toHaveBeenCalledWith(
        "verifyDialog.alerts.sendMobileVerificationSuccess",
        "groupAlerts.titleSuccess",
      );
    });
    await waitFor((): void => {
      expect(msgSuccess).toHaveBeenCalledWith(
        "organization.tabs.compliance.tabs.standards.alerts.generatedReport",
        "groupAlerts.titleSuccess",
      );
    });
  });

  it("should render error generate report modal", async (): Promise<void> => {
    expect.hasAssertions();

    jest.spyOn(console, "error").mockImplementation();

    render(<Component />, {
      memoryRouter,
      mocks: [
        graphqlMocked.query(
          GET_UNFULFILLED_STANDARD_REPORT_URL,
          ({
            variables,
          }): StrictResponse<
            IErrorMessage | { data: RequestGroupReportAtComplianceStandards }
          > => {
            const { verificationCode, reportType } = variables;

            if (reportType === ReportType.Csv && verificationCode === "1234") {
              return HttpResponse.json({
                errors: [
                  new GraphQLError(
                    "Exception - The verification code is invalid",
                  ),
                ],
              });
            }

            return HttpResponse.json({
              data: {
                unfulfilledStandardReportUrl: "https://test.com/",
              },
            });
          },
        ),
      ],
    });

    await waitFor((): void => {
      expect(
        screen.getByText(
          "organization.tabs.compliance.header.tabs.standards.text",
        ),
      ).toBeInTheDocument();
    });
    await userEvent.click(
      screen.getByText(
        "organization.tabs.compliance.header.tabs.standards.text",
      ),
    );

    await waitFor((): void => {
      expect(
        screen.queryByText(
          "organization.tabs.compliance.content.unfulfilledStandards.bottom.text",
        ),
      ).toBeInTheDocument();
    });
    await userEvent.click(
      screen.getByText(
        "organization.tabs.compliance.content.unfulfilledStandards.bottom.text",
      ),
    );

    await waitFor((): void => {
      expect(
        screen.getByText(
          "organization.tabs.compliance.tabs.standards.generateReportModal.title",
        ),
      ).toBeInTheDocument();
    });

    expect(
      screen.getByText(
        "organization.tabs.compliance.tabs.standards.generateReportModal.buttons.generateReport.text",
      ),
    ).toBeInTheDocument();

    await userEvent.click(screen.getByRole("radio", { name: "CSV" }));

    expect(
      screen.getByRole("button", {
        name: "organization.tabs.compliance.tabs.standards.generateReportModal.buttons.generateReport.text",
      }),
    ).not.toBeDisabled();

    await userEvent.click(
      screen.getByRole("button", {
        name: "organization.tabs.compliance.tabs.standards.generateReportModal.buttons.generateReport.text",
      }),
    );

    expect(screen.getByText("verifyDialog.title")).toBeInTheDocument();

    await userEvent.click(
      screen.getByText("components.channels.smsButton.text"),
    );
    await userEvent.type(
      screen.getByRole("textbox", { name: "verificationCode" }),
      "1234",
    );
    await userEvent.click(screen.getByText("verifyDialog.verify"));

    await waitFor((): void => {
      expect(msgSuccess).toHaveBeenCalledWith(
        "verifyDialog.alerts.sendMobileVerificationSuccess",
        "groupAlerts.titleSuccess",
      );
    });
    await waitFor((): void => {
      expect(msgError).toHaveBeenCalledWith(
        "group.findings.report.alerts.invalidVerificationCode",
      );
    });
  });
});
