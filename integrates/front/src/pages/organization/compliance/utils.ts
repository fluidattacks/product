import isNull from "lodash/isNull";

const PERCENTAGE_BASE = 100;

const handleCompliancePercentageValue = (value: number | null): number =>
  isNull(value) ? 0 : Math.round(value * PERCENTAGE_BASE);

const handleComplianceValue = (value: number | null): number =>
  isNull(value) ? 0.0 : value;

export { handleCompliancePercentageValue, handleComplianceValue };
