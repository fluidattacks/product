import { useLazyQuery, useQuery } from "@apollo/client";
import {
  Alert,
  Button,
  Container,
  Modal,
  RadioButton,
  Toggle,
  useModal,
} from "@fluidattacks/design";
import type { ColumnDef } from "@tanstack/react-table";
import _ from "lodash";
import mixpanel from "mixpanel-browser";
import { useCallback, useEffect, useState } from "react";
import * as React from "react";
import { useTranslation } from "react-i18next";

import { includeFormatter } from "./include-formatter";
import { downloadReport } from "./utils";

import type {
  IGenerateReportModalProps,
  IUnfulfilledStandardData,
} from "../../types";
import { GET_UNFULFILLED_STANDARD_REPORT_URL } from "../queries";
import { Table } from "components/table";
import { VerifyDialog } from "features/verify-dialog";
import { GET_STAKEHOLDER_PHONE } from "features/verify-dialog/queries";
import type { TVerifyFn } from "features/verify-dialog/types";
import { ReportType } from "gql/graphql";
import { useTable } from "hooks";
import { GET_NOTIFICATIONS } from "pages/dashboard/navbar/downloads/queries";
import { Logger } from "utils/logger";
import { msgError, msgSuccess } from "utils/notifications";

const GenerateReportModal = ({
  isOpen,
  onClose,
  groupName,
  unfulfilledStandards,
}: IGenerateReportModalProps): JSX.Element => {
  const { t } = useTranslation();

  const tableRef = useTable("standardsToGenerateReport");
  const modalProps = useModal("generate-report-modal");
  const getFormattedUnfulfilledStandards = useCallback(
    (): IUnfulfilledStandardData[] =>
      _.sortBy(
        unfulfilledStandards?.group.compliance.unfulfilledStandards.map(
          (unfulfilledStandard): IUnfulfilledStandardData => ({
            ...unfulfilledStandard,
            include: !_.isNil(unfulfilledStandard?.title),
            title: unfulfilledStandard?.title?.toUpperCase(),
          }),
        ),
        (unfulfilledStandard): string => unfulfilledStandard.title ?? "",
      ),
    [unfulfilledStandards],
  );

  const [reportType, setReportType] = useState<ReportType>(ReportType.Pdf);
  const [isVerifyDialogOpen, setIsVerifyDialogOpen] = useState(false);
  const [disableVerify, setDisableVerify] = useState(false);
  const [includeAllStandards, setIncludeAllStandards] = useState(true);
  const [unfulfilledStandardsData, setUnfulfilledStandardsData] = useState(
    getFormattedUnfulfilledStandards(),
  );

  const includedUnfulfilledStandards = unfulfilledStandardsData
    .filter(
      (unfulfilledStandard): boolean =>
        unfulfilledStandard.include && !_.isNil(unfulfilledStandard.standardId),
    )
    .map((unfulfilledStandard): string => unfulfilledStandard.standardId ?? "");

  const [requestUnfulfilledStandardReport, { client }] = useLazyQuery(
    GET_UNFULFILLED_STANDARD_REPORT_URL,
    {
      onCompleted: (data): void => {
        setDisableVerify(false);
        downloadReport(data.unfulfilledStandardReportUrl);
        msgSuccess(
          t(
            "organization.tabs.compliance.tabs.standards.alerts.generatedReport",
          ),
          t("groupAlerts.titleSuccess"),
        );
        setIsVerifyDialogOpen(false);
        onClose();
        void client.refetchQueries({ include: [GET_NOTIFICATIONS] });
      },
      onError: (errors): void => {
        setDisableVerify(false);
        errors.graphQLErrors.forEach((error): void => {
          switch (error.message) {
            case "Exception - Stakeholder could not be verified":
              msgError(
                t("group.findings.report.alerts.nonVerifiedStakeholder"),
              );
              break;
            case "Exception - The verification code is invalid":
              msgError(
                t("group.findings.report.alerts.invalidVerificationCode"),
              );
              break;
            default:
              msgError(t("groupAlerts.errorTextsad"));
              Logger.warning(
                "An error occurred requesting group report",
                error,
              );
          }
        });
      },
    },
  );

  const { data: phoneData } = useQuery(GET_STAKEHOLDER_PHONE, {
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error): void => {
        Logger.error("Couldn't load stakeholder's phone", error);
      });
    },
  });

  const { phone } = phoneData?.me ?? {};

  const handleReportType = useCallback(
    (event: React.MouseEvent<HTMLInputElement>): void => {
      setReportType(
        (event.target as HTMLInputElement).value === "PDF"
          ? ReportType.Pdf
          : ReportType.Csv,
      );
    },
    [],
  );

  const handleRequestUnfulfilledStandardReport = useCallback(
    async (verificationCode: string): Promise<void> => {
      setDisableVerify(true);
      mixpanel.track("UnfulfilledStandardReportRequest");
      await requestUnfulfilledStandardReport({
        variables: {
          groupName,
          reportType,
          unfulfilledStandards: includedUnfulfilledStandards,
          verificationCode,
        },
      }).then((): void => {
        setReportType(ReportType.Pdf);
      });
    },
    [
      requestUnfulfilledStandardReport,
      groupName,
      includedUnfulfilledStandards,
      reportType,
    ],
  );
  const handleRequestReport = useCallback(
    (setVerifyCallbacks: TVerifyFn): (() => void) =>
      (): void => {
        setVerifyCallbacks(
          async (verificationCode): Promise<void> => {
            await handleRequestUnfulfilledStandardReport(verificationCode);
          },
          (): void => {
            setIsVerifyDialogOpen(false);
          },
        );
        setIsVerifyDialogOpen(true);
      },
    [handleRequestUnfulfilledStandardReport],
  );
  const handleIncludeStandard = (row: IUnfulfilledStandardData): void => {
    setUnfulfilledStandardsData(
      unfulfilledStandardsData.map(
        (unfulfilledStandard): IUnfulfilledStandardData =>
          row.standardId === unfulfilledStandard.standardId
            ? {
                ...unfulfilledStandard,
                include: !unfulfilledStandard.include,
              }
            : unfulfilledStandard,
      ),
    );
  };
  const handleIncludeAllStandards = useCallback((): void => {
    setUnfulfilledStandardsData(
      unfulfilledStandardsData.map(
        (unfulfilledStandard): IUnfulfilledStandardData => ({
          ...unfulfilledStandard,
          include: !includeAllStandards,
        }),
      ),
    );
    setIncludeAllStandards(!includeAllStandards);
  }, [includeAllStandards, unfulfilledStandardsData]);
  const handleClose = useCallback((): void => {
    onClose();
    setIsVerifyDialogOpen(false);
    setReportType(ReportType.Pdf);
  }, [onClose, setIsVerifyDialogOpen]);

  // Side effects
  useEffect((): void => {
    setUnfulfilledStandardsData(getFormattedUnfulfilledStandards());
  }, [unfulfilledStandards, getFormattedUnfulfilledStandards]);

  // Table
  const columns: ColumnDef<IUnfulfilledStandardData>[] = [
    {
      accessorKey: "title",
      header: t(
        "organization.tabs.compliance.tabs.standards.generateReportModal.unfulfilledStandard",
      ),
    },
    {
      accessorKey: "include",
      cell: (cell): JSX.Element =>
        includeFormatter(cell.row.original, handleIncludeStandard),
      header: t(
        "organization.tabs.compliance.tabs.standards.generateReportModal.action",
      ),
    },
  ];

  return (
    <React.StrictMode>
      <Modal
        modalRef={{ ...modalProps, close: handleClose, isOpen }}
        size={"md"}
        title={t(
          "organization.tabs.compliance.tabs.standards.generateReportModal.title",
        )}
      >
        <Alert show={_.isNil(phone)} variant={"error"}>
          {t(
            "organization.tabs.compliance.tabs.standards.generateReportModal.2FA",
          )}
        </Alert>
        <VerifyDialog disable={disableVerify} isOpen={isVerifyDialogOpen}>
          {(setVerifyCallbacks): JSX.Element => {
            return (
              <Table
                columns={columns}
                data={unfulfilledStandardsData}
                extraButtons={
                  <Container>
                    <Container display={"flex"} gap={0.5} justify={"left"}>
                      <RadioButton
                        defaultChecked={reportType === ReportType.Pdf}
                        label={"PDF"}
                        name={"reportType"}
                        onClick={handleReportType}
                        value={ReportType.Pdf}
                      />
                      <RadioButton
                        label={"CSV"}
                        name={"reportType"}
                        onClick={handleReportType}
                        value={ReportType.Csv}
                      />
                    </Container>
                    <Toggle
                      defaultChecked={includeAllStandards}
                      leftDescription={t(
                        "organization.tabs.compliance.tabs.standards.generateReportModal.excludeAll",
                      )}
                      name={"AllStandards"}
                      onChange={handleIncludeAllStandards}
                      rightDescription={t(
                        "organization.tabs.compliance.tabs.standards.generateReportModal.includeAll",
                      )}
                    />
                  </Container>
                }
                rightSideComponents={
                  <Button
                    disabled={
                      includedUnfulfilledStandards.length === 0 ||
                      _.isNil(phone)
                    }
                    id={"standard-report"}
                    mt={0.25}
                    onClick={handleRequestReport(setVerifyCallbacks)}
                    tooltip={t(
                      "organization.tabs.compliance.tabs.standards.generateReportModal.buttons.generateReport.tooltip",
                    )}
                    type={"submit"}
                    variant={"primary"}
                  >
                    {t(
                      "organization.tabs.compliance.tabs.standards.generateReportModal.buttons.generateReport.text",
                    )}
                  </Button>
                }
                tableRef={tableRef}
              />
            );
          }}
        </VerifyDialog>
      </Modal>
    </React.StrictMode>
  );
};

export { GenerateReportModal };
