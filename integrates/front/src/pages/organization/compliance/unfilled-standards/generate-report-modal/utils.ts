/* eslint-disable functional/immutable-data */
const downloadReport = (url: string): void => {
  const downloadLink = document.createElement("a");
  downloadLink.href = url;
  downloadLink.rel = "noopener noreferrer";
  downloadLink.click();
};

export { downloadReport };
