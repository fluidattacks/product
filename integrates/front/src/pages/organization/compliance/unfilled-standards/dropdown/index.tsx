import { useQuery } from "@apollo/client";
import { Container, Loading } from "@fluidattacks/design";
import isNil from "lodash/isNil";
import { useEffect } from "react";
import { useTranslation } from "react-i18next";

import type { IGroupsData } from "./types";

import { Dropdown } from "components/dropdown";
import type { IDropDownOption } from "components/dropdown/types";
import { GET_ORGANIZATION_GROUP_NAMES } from "pages/group/group-selector/queries";
import { Logger } from "utils/logger";
import { msgError } from "utils/notifications";

const GroupsDropdown = ({
  organizationId,
  selectionHandler,
}: Readonly<IGroupsData>): JSX.Element => {
  const { t } = useTranslation();

  // Unfilled Standards view queries
  const { data, loading } = useQuery(GET_ORGANIZATION_GROUP_NAMES, {
    fetchPolicy: "cache-first",
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error): void => {
        msgError(t("groupAlerts.errorTextsad"));
        Logger.warning(
          "An error occurred loading organization group names",
          error,
        );
      });
    },
    variables: {
      organizationId,
    },
  });

  useEffect((): void => {
    if (data?.organization.groups[0] && selectionHandler !== undefined) {
      selectionHandler({ value: data.organization.groups[0].name });
    }
  }, [data, selectionHandler]);

  if (loading)
    return (
      <Container
        alignItems={"center"}
        display={"flex"}
        justify={"center"}
        left={"50%"}
        position={"absolute"}
        top={"70%"}
      >
        <Loading size={"lg"} />
      </Container>
    );

  const groups: IDropDownOption[] | undefined =
    isNil(data) || isNil(data.organization.groups)
      ? []
      : data.organization.groups
          .filter((group): boolean => !isNil(group) && !isNil(group.name))
          .map((group): IDropDownOption => ({ value: group.name }));

  if (groups.length === 0)
    return (
      <Dropdown
        disabled={true}
        items={[
          {
            header: t(
              "organization.tabs.compliance.content.unfulfilledStandards.header.noGroups",
            ),
          },
        ]}
      />
    );

  return <Dropdown customSelectionHandler={selectionHandler} items={groups} />;
};

export { GroupsDropdown };
