import type { IDropdownProps } from "components/dropdown/types";

interface IGroupsData {
  organizationId: string;
  selectionHandler: IDropdownProps["customSelectionHandler"];
}

export type { IGroupsData };
