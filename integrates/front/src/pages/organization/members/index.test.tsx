import { PureAbility } from "@casl/ability";
import { fireEvent, screen, waitFor, within } from "@testing-library/react";
import { userEvent } from "@testing-library/user-event";
import dayjs from "dayjs";
import { HttpResponse, type StrictResponse, graphql } from "msw";
import { Route, Routes } from "react-router-dom";

import {
  GET_ORGANIZATION_STAKEHOLDERS,
  UPDATE_STAKEHOLDER_MUTATION,
} from "./queries";
import type { IOrganizationMembers } from "./types";

import { OrganizationMembers } from ".";
import { GET_ORGANIZATION_POLICIES } from "../policies/queries";
import { authContext } from "context/auth";
import { authzPermissionsContext } from "context/authz/config";
import type {
  GetCredentialsAndRootsQuery as GetCredentialsAndRoots,
  GetOrganizationPoliciesAtOrganizationQuery,
  GetOrganizationStakeholdersQuery as GetOrganizationStakeholders,
  UpdateOrganizationStakeholderMutationMutation,
} from "gql/graphql";
import { InvitationState } from "gql/graphql";
import { render } from "mocks";
import { LINK } from "mocks/constants";
import { GET_CREDENTIALS_AND_ROOTS } from "pages/organization/members/remove-stakeholder-modal/queries";
import { msgError, msgSuccess } from "utils/notifications";
import { translate } from "utils/translations/translate";

jest.mock("utils/notifications", (): Record<string, unknown> => {
  const mockedNotifications: Record<string, () => Record<string, unknown>> =
    jest.requireActual("utils/notifications");
  jest.spyOn(mockedNotifications, "msgError").mockImplementation();
  jest.spyOn(mockedNotifications, "msgSuccess").mockImplementation();

  return mockedNotifications;
});

const mockProps: IOrganizationMembers = {
  organizationId: "ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3",
};
const mockedPermissions = new PureAbility<string>([
  {
    action:
      "integrates_api_mutations_grant_stakeholder_organization_access_mutate",
  },
  { action: "integrates_api_mutations_update_organization_stakeholder_mutate" },
  {
    action:
      "integrates_api_mutations_remove_stakeholder_organization_access_mutate",
  },
  { action: "grant_organization_level_role:user" },
  { action: "grant_organization_level_role:organization_manager" },
  { action: "grant_organization_level_role:customer_manager" },
]);

const Wrapper = ({
  mockPermissions = mockedPermissions,
  orgId = mockProps.organizationId,
}: Readonly<{
  mockPermissions?: PureAbility<string>;
  orgId?: string;
}>): JSX.Element => (
  <authzPermissionsContext.Provider value={mockPermissions}>
    <Routes>
      <Route
        element={<OrganizationMembers organizationId={orgId} />}
        path={"/orgs/:organizationName/members"}
      />
    </Routes>
  </authzPermissionsContext.Provider>
);

describe("organizationMembers", (): void => {
  const graphqlMocked = graphql.link(LINK);
  const memoryRouter = {
    initialEntries: ["/orgs/okada/members"],
  };
  const mockCredentials = graphqlMocked.query(
    GET_CREDENTIALS_AND_ROOTS,
    (): StrictResponse<{ data: GetCredentialsAndRoots }> => {
      return HttpResponse.json({
        data: {
          organization: {
            __typename: "Organization",
            credentials: [],
            groups: [],
            name: "",
          },
        },
      });
    },
  );
  const mockOrganizationPolicies = graphqlMocked.query(
    GET_ORGANIZATION_POLICIES,
    (): StrictResponse<{
      data: GetOrganizationPoliciesAtOrganizationQuery;
    }> => {
      return HttpResponse.json({
        data: {
          organization: {
            __typename: "Organization",
            daysUntilItBreaks: null,
            inactivityPeriod: 90,
            maxAcceptanceDays: 60,
            maxAcceptanceSeverity: 10.0,
            maxNumberAcceptances: 2,
            minAcceptanceSeverity: 0.0,
            minBreakingSeverity: 0.0,
            name: "okada",
            userRole: "organization_manager",
            vulnerabilityGracePeriod: 0,
          },
        },
      });
    },
    { once: true },
  );

  it("should render component", async (): Promise<void> => {
    expect.hasAssertions();

    jest.clearAllMocks();

    render(<Wrapper />, {
      memoryRouter,
      mocks: [mockCredentials, mockOrganizationPolicies],
    });

    const RENDER_TEST_LENGTH = 3;
    await waitFor((): void => {
      expect(screen.queryAllByRole("row")).toHaveLength(RENDER_TEST_LENGTH);
    });

    await waitFor((): void => {
      expect(
        screen.queryByText("organization.members.policiesAlert"),
      ).toBeInTheDocument();
    });

    const addButtonText = "organization.members.addUserButton.text";

    expect(screen.getByText(addButtonText)).not.toBeDisabled();
    expect(document.getElementById("editUser")).toBeDisabled();
    expect(document.getElementById("removeUser")).toBeDisabled();

    expect(screen.getByText("testuser1@gmail.com")).toBeInTheDocument();
    expect(screen.getByText("Customer Manager")).toBeInTheDocument();

    expect(screen.getByText("2020-06-01")).toBeInTheDocument();
    expect(
      screen.getByText(dayjs("2020-09-01", "YYYY-MM-DD hh:mm:ss A").fromNow()),
    ).toBeInTheDocument();

    expect(screen.getByText("testuser2@gmail.com")).toBeInTheDocument();
    expect(screen.getByText("Organization Manager")).toBeInTheDocument();
    expect(screen.getByText("2020-08-01")).toBeInTheDocument();
    expect(
      within(screen.queryAllByRole("row")[2]).getByRole("cell", {
        name: "-",
      }),
    ).toBeInTheDocument();

    await waitFor((): void => {
      expect(
        within(screen.getByRole("table")).queryByText("testuser1@gmail.com"),
      ).toBeInTheDocument();
    });

    const cell = screen.getByText("testuser1@gmail.com");
    const row = cell.closest("tr");
    const checkbox = within(row as HTMLElement).getByRole("checkbox");

    await userEvent.click(checkbox);

    await waitFor((): void => {
      expect(
        screen.getByText("organization.tabs.users.editButton.text"),
      ).not.toBeDisabled();
    });

    expect(
      screen.getByText("organization.tabs.users.removeButton.text"),
    ).not.toBeDisabled();
  });

  it("should add a user", async (): Promise<void> => {
    expect.hasAssertions();

    jest.clearAllMocks();

    render(
      <authContext.Provider
        value={{
          awsSubscription: null,
          setUser: jest.fn(),
          tours: {
            newGroup: true,
            newRoot: true,
            welcome: true,
          },
          userEmail: "john@gmail.com",
          userName: "John Doe",
        }}
      >
        <Wrapper />
      </authContext.Provider>,
      {
        memoryRouter,
        mocks: [
          mockCredentials,
          graphqlMocked.query(
            GET_ORGANIZATION_STAKEHOLDERS,
            (): StrictResponse<{ data: GetOrganizationStakeholders }> => {
              return HttpResponse.json({
                data: {
                  organization: {
                    __typename: "Organization",
                    name: "okada",
                    stakeholders: [
                      {
                        __typename: "Stakeholder",
                        email: "testuser1@gmail.com",
                        firstLogin: "2020-06-01",
                        invitationState: InvitationState.Registered,
                        lastLogin: "2020-09-01",
                        role: "customer_manager",
                      },
                    ],
                  },
                },
              });
            },
            { once: true },
          ),
          mockOrganizationPolicies,
        ],
      },
    );

    const addButtonText = "organization.members.addUserButton.text";

    await waitFor((): void => {
      expect(screen.getByText(addButtonText)).not.toBeDisabled();
    });

    expect(
      screen.queryByText("organization.tabs.users.modalAddTitle"),
    ).not.toBeInTheDocument();

    await userEvent.click(screen.getByText(addButtonText));
    await waitFor((): void => {
      expect(screen.getByText("Confirm")).toBeInTheDocument();
    });
    fireEvent.change(screen.getByRole("combobox", { name: "email" }), {
      target: { value: "testuser2@gmail.com" },
    });

    await userEvent.click(screen.getByRole("combobox", { name: "role" }));
    await userEvent.click(screen.getByText("userModal.roles.user"));

    await waitFor((): void => {
      expect(screen.getByText("Confirm")).not.toBeDisabled();
    });

    await userEvent.click(screen.getByText("Confirm"));
    await waitFor((): void => {
      expect(msgSuccess).toHaveBeenCalledWith(
        "searchFindings.tabUsers.success testuser2@gmail.com",
        "organization.tabs.users.successTitle",
      );
    });
    const TEST_LENGTH = 3;

    expect(screen.getAllByRole("row")).toHaveLength(TEST_LENGTH);
    expect(
      screen.queryByText("organization.tabs.users.modalAddTitle"),
    ).not.toBeInTheDocument();
  });

  it("should edit a user", async (): Promise<void> => {
    expect.hasAssertions();

    jest.clearAllMocks();

    render(<Wrapper />, {
      memoryRouter,
      mocks: [
        mockCredentials,
        mockOrganizationPolicies,
        graphqlMocked.mutation(
          UPDATE_STAKEHOLDER_MUTATION,
          ({
            variables,
          }): StrictResponse<{
            data: UpdateOrganizationStakeholderMutationMutation;
          }> => {
            const { email } = variables;

            return HttpResponse.json({
              data: {
                updateOrganizationStakeholder: {
                  modifiedStakeholder: {
                    email,
                  },
                  success: true,
                },
              },
            });
          },
        ),
      ],
    });
    await waitFor((): void => {
      expect(document.getElementById("editUser")).toBeDisabled();
    });

    await waitFor((): void => {
      expect(
        within(screen.getByRole("table")).queryByText("testuser1@gmail.com"),
      ).toBeInTheDocument();
    });

    const cell = screen.getByText("testuser1@gmail.com");
    const row = cell.closest("tr");
    const checkbox = within(row as HTMLElement).getByRole("checkbox");

    await userEvent.click(checkbox);

    await waitFor((): void => {
      expect(document.getElementById("editUser")).not.toBeDisabled();
    });
    await userEvent.click(document.querySelectorAll("#editUser")[0]);

    await waitFor((): void => {
      expect(screen.getByRole("combobox", { name: "email" })).toHaveValue(
        "testuser1@gmail.com",
      );
    });

    expect(screen.getByRole("combobox", { name: "email" })).toBeDisabled();
    expect(screen.getByRole("combobox", { name: "role" })).toHaveValue(
      "userModal.roles.customerManager",
    );

    await userEvent.click(screen.getByRole("combobox", { name: "role" }));
    await userEvent.click(
      screen.getByText("userModal.roles.organizationManager"),
    );

    await userEvent.click(screen.getByText("Confirm"));
    await waitFor((): void => {
      expect(msgSuccess).toHaveBeenCalledWith(
        "testuser1@gmail.com organization.tabs.users.editButton.success",
        "organization.tabs.users.successTitle",
      );
    });

    expect(
      screen.queryByText("organization.tabs.users.modalEditTitle"),
    ).not.toBeInTheDocument();
    expect(screen.getAllByRole("row")).toHaveLength(3);
  });

  it("should remove a user", async (): Promise<void> => {
    expect.hasAssertions();

    jest.clearAllMocks();

    render(<Wrapper />, {
      memoryRouter,
      mocks: [
        mockCredentials,
        mockOrganizationPolicies,
        graphqlMocked.query(
          GET_ORGANIZATION_STAKEHOLDERS,
          (): StrictResponse<{ data: GetOrganizationStakeholders }> => {
            return HttpResponse.json({
              data: {
                organization: {
                  __typename: "Organization",
                  name: "okada",
                  stakeholders: [
                    {
                      __typename: "Stakeholder",
                      email: "testuser1@gmail.com",
                      firstLogin: "2020-06-01",
                      invitationState: InvitationState.Registered,
                      lastLogin: "2020-09-01",
                      role: "customer_manager",
                    },
                    {
                      __typename: "Stakeholder",
                      email: "testuser2@gmail.com",
                      firstLogin: "2020-08-01",
                      invitationState: InvitationState.Registered,
                      lastLogin: "-",
                      role: "organization_manager",
                    },
                  ],
                },
              },
            });
          },
          { once: true },
        ),
      ],
    });
    const TEST_LENGTH = 3;
    await waitFor((): void => {
      expect(screen.queryAllByRole("row")).toHaveLength(TEST_LENGTH);
    });

    expect(screen.queryAllByRole("checkbox")[1]).not.toBeChecked();
    expect(screen.queryAllByRole("checkbox")[2]).not.toBeChecked();
    expect(document.getElementById("removeUser")).toBeDisabled();

    await userEvent.click(screen.queryAllByRole("checkbox")[2]);

    await waitFor((): void => {
      expect(document.getElementById("removeUser")).not.toBeDisabled();
    });

    expect(screen.queryAllByRole("checkbox")[2]).toBeChecked();

    await userEvent.click(document.querySelectorAll("#removeUser")[0]);
    await waitFor((): void => {
      expect(
        screen.queryByText("organization.tabs.users.removeButton.confirmTitle"),
      ).toBeInTheDocument();
    });
  });

  it("should show empty state", async (): Promise<void> => {
    expect.hasAssertions();

    jest.clearAllMocks();

    render(
      <authContext.Provider
        value={{
          awsSubscription: null,
          setUser: jest.fn(),
          tours: {
            newGroup: true,
            newRoot: true,
            welcome: true,
          },
          userEmail: "john@gmail.com",
          userName: "John Doe",
        }}
      >
        <Wrapper />
      </authContext.Provider>,
      {
        memoryRouter,
        mocks: [
          mockCredentials,
          mockOrganizationPolicies,
          graphqlMocked.query(
            GET_ORGANIZATION_STAKEHOLDERS,
            (): StrictResponse<{ data: GetOrganizationStakeholders }> => {
              return HttpResponse.json({
                data: {
                  organization: {
                    __typename: "Organization",
                    name: "okada",
                    stakeholders: [],
                  },
                },
              });
            },
            { once: true },
          ),
        ],
      },
    );

    await waitFor((): void => {
      expect(
        screen.queryByText("organization.tabs.users.emptyComponent.header"),
      ).toBeInTheDocument();
    });

    expect(screen.queryAllByRole("row")).toHaveLength(0);

    await userEvent.click(
      screen.getByText("organization.tabs.users.emptyComponent.addButton.text"),
    );

    await waitFor((): void => {
      expect(
        screen.queryByText("organization.tabs.users.modalAddTitle"),
      ).toBeInTheDocument();
    });
  });

  it("should handle query errors", async (): Promise<void> => {
    expect.hasAssertions();

    jest.clearAllMocks();

    render(<Wrapper orgId={"ORG#602e9f9e-f2b9-4aad-afe1-782de31cd3a9"} />, {
      memoryRouter,
      mocks: [mockCredentials],
    });

    await waitFor((): void => {
      expect(msgError).toHaveBeenCalledWith("groupAlerts.errorTextsad");
    });

    expect(
      screen.getByText("organization.tabs.users.emptyComponent.description"),
    ).toBeInTheDocument();
    expect(
      screen.getByText("organization.tabs.users.emptyComponent.title"),
    ).toBeInTheDocument();
    expect(
      screen.getByText("organization.tabs.users.emptyComponent.addButton.text"),
    ).toBeInTheDocument();
  });

  it("should handle mutation errors", async (): Promise<void> => {
    expect.hasAssertions();

    render(<Wrapper />, {
      memoryRouter,
      mocks: [mockCredentials],
    });

    await waitFor((): void => {
      expect(screen.queryAllByRole("row")).toHaveLength(3);
    });

    expect(document.getElementById("editUser")).toBeDisabled();

    await waitFor((): void => {
      expect(
        within(screen.getByRole("table")).queryByText("testuser1@gmail.com"),
      ).toBeInTheDocument();
    });

    const cell = screen.getByText("testuser1@gmail.com");
    const row = cell.closest("tr");
    const checkbox = within(row as HTMLElement).getByRole("checkbox");

    await userEvent.click(checkbox);

    await waitFor((): void => {
      expect(document.getElementById("editUser")).not.toBeDisabled();
    });

    expect(
      screen.queryByText("organization.tabs.users.modalEditTitle"),
    ).not.toBeInTheDocument();

    const openModal = async (): Promise<void> => {
      await userEvent.click(document.querySelectorAll("#editUser")[0]);
      await waitFor((): void => {
        expect(
          screen.queryByText("organization.tabs.users.modalEditTitle"),
        ).toBeInTheDocument();
      });
    };
    const editStakeholder = async (): Promise<void> => {
      await openModal();
      await userEvent.click(screen.getByRole("combobox", { name: "role" }));
      await userEvent.click(
        screen.getByText("userModal.roles.organizationManager"),
      );

      await userEvent.click(screen.getByText("Confirm"));
    };
    await editStakeholder();
    await waitFor((): void => {
      expect(msgError).toHaveBeenCalledWith(translate.t("validations.email"));
    });

    await editStakeholder();
    await waitFor((): void => {
      expect(msgError).toHaveBeenCalledWith(
        translate.t("validations.invalidValueInField"),
      );
    });

    await editStakeholder();
    await waitFor((): void => {
      expect(msgError).toHaveBeenCalledWith(
        translate.t("validations.invalidChar"),
      );
    });

    await editStakeholder();
    await waitFor((): void => {
      expect(msgError).toHaveBeenCalledWith(
        translate.t("validations.invalidEmailInField"),
      );
    });
    await editStakeholder();
    await waitFor((): void => {
      expect(msgError).toHaveBeenCalledWith(
        translate.t("groupAlerts.errorTextsad"),
      );
    });
  });

  it("should render add user modal to a each role", async (): Promise<void> => {
    expect.hasAssertions();

    jest.clearAllMocks();
    const mockedPermissionsorganizationManager = new PureAbility<string>([
      {
        action:
          "integrates_api_mutations_grant_stakeholder_organization_access_mutate",
      },
      { action: "grant_organization_level_role:user" },
      { action: "grant_organization_level_role:organization_manager" },
      { action: "grant_organization_level_role:customer_manager" },
      { action: "grant_organization_level_role:resourcer" },
    ]);
    const { rerender } = render(
      <authContext.Provider
        value={{
          awsSubscription: null,
          setUser: jest.fn(),
          tours: {
            newGroup: true,
            newRoot: true,
            welcome: true,
          },
          userEmail: "john@gmail.com",
          userName: "John Doe",
        }}
      >
        <Wrapper mockPermissions={mockedPermissionsorganizationManager} />
      </authContext.Provider>,
      {
        memoryRouter,
        mocks: [mockCredentials, mockOrganizationPolicies],
      },
    );

    const addButtonText = "organization.members.addUserButton.text";

    await waitFor((): void => {
      expect(screen.getByText(addButtonText)).not.toBeDisabled();
    });

    expect(
      screen.queryByText("organization.tabs.users.modalAddTitle"),
    ).not.toBeInTheDocument();

    await userEvent.click(screen.getByText(addButtonText));
    await waitFor((): void => {
      expect(screen.getByText("Confirm")).toBeInTheDocument();
    });
    fireEvent.change(screen.getByRole("combobox", { name: "email" }), {
      target: { value: "testuser2@gmail.com" },
    });

    await userEvent.click(screen.getByRole("combobox", { name: "role" }));

    expect(screen.queryByText("userModal.roles.user")).toBeInTheDocument();
    expect(
      screen.queryByText("userModal.roles.organizationManager"),
    ).toBeInTheDocument();
    expect(
      screen.queryByText("userModal.roles.customerManager"),
    ).toBeInTheDocument();
    expect(screen.queryByText("userModal.roles.resourcer")).toBeInTheDocument();

    const mockedPermissionsCusomerManager = new PureAbility<string>([
      {
        action:
          "integrates_api_mutations_grant_stakeholder_organization_access_mutate",
      },
      { action: "grant_organization_level_role:user" },
      { action: "grant_organization_level_role:organization_manager" },
      { action: "grant_organization_level_role:customer_manager" },
    ]);
    rerender(
      <authContext.Provider
        value={{
          awsSubscription: null,
          setUser: jest.fn(),
          tours: {
            newGroup: true,
            newRoot: true,
            welcome: true,
          },
          userEmail: "john@gmail.com",
          userName: "John Doe",
        }}
      >
        <Wrapper mockPermissions={mockedPermissionsCusomerManager} />
      </authContext.Provider>,
    );

    await userEvent.click(screen.getByText(addButtonText));
    await waitFor((): void => {
      expect(screen.getByText("Confirm")).toBeInTheDocument();
    });
    fireEvent.change(screen.getByRole("combobox", { name: "email" }), {
      target: { value: "testuser2@gmail.com" },
    });

    await userEvent.click(screen.getByRole("combobox", { name: "role" }));

    expect(screen.queryByText("userModal.roles.user")).toBeInTheDocument();
    expect(
      screen.queryByText("userModal.roles.organizationManager"),
    ).toBeInTheDocument();
    expect(
      screen.queryByText("userModal.roles.customerManager"),
    ).toBeInTheDocument();
    expect(
      screen.queryByText("userModal.roles.resourcer"),
    ).not.toBeInTheDocument();
  });
});
