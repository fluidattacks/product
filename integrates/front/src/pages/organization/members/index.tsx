import { useMutation } from "@apollo/client";
import {
  Alert,
  Button,
  Container,
  EmptyState,
  Text,
  useConfirmDialog,
} from "@fluidattacks/design";
import type { ColumnDef } from "@tanstack/react-table";
import _ from "lodash";
import mixpanel from "mixpanel-browser";
import { Fragment, useCallback, useContext, useMemo, useState } from "react";
import * as React from "react";
import { useTranslation } from "react-i18next";
import { useParams } from "react-router-dom";
import { useTheme } from "styled-components";

import { ActionsButtons } from "./actions-buttons";
import { useOrganizationStakeholdersQuery } from "./hooks";
import {
  ADD_STAKEHOLDER_MUTATION,
  REMOVE_STAKEHOLDER_MUTATION,
  UPDATE_STAKEHOLDER_MUTATION,
} from "./queries";
import { RemoveStakeholderModal } from "./remove-stakeholder-modal";
import type { IOrganizationMembers, IStakeholderDataSet } from "./types";
import {
  getAreAllMutationValid,
  handleMtError,
  handleRemoveError,
  removeMultipleAccess,
} from "./utils";

import { SectionHeader } from "components/section-header";
import { Table } from "components/table";
import { timeFromNow } from "components/table/table-formatters";
import { authContext } from "context/auth";
import { Can } from "context/authz/can";
import { AddUserModal } from "features/add-user-modal";
import type { IStakeholderFormValues } from "features/add-user-modal/types";
import { statusFormatter } from "features/vulnerabilities/formatters/status";
import type { OrganizationRole } from "gql/graphql";
import { InvitationState } from "gql/graphql";
import {
  useDailyAlert,
  useModal,
  usePolicies,
  useStoredState,
  useTable,
} from "hooks";
import { handleGrantError } from "pages/group/members/utils";
import { formatDate } from "utils/date";
import { Logger } from "utils/logger";
import { msgSuccess } from "utils/notifications";
import { translate } from "utils/translations/translate";

const PADDING_A = 0.25;
const PADDING_B = 1.25;

const tableColumns: ColumnDef<IStakeholderDataSet>[] = [
  {
    accessorKey: "email",
    header: translate.t("searchFindings.usersTable.usermail"),
  },
  {
    accessorKey: "role",
    cell: (cell): string =>
      translate.t(`userModal.roles.${_.camelCase(String(cell.getValue()))}`, {
        defaultValue: "-",
      }),
    header: translate.t("searchFindings.usersTable.userRole"),
  },
  {
    accessorKey: "firstLogin",
    cell: (cell): string => formatDate(String(cell.getValue() ?? "")),
    header: translate.t("searchFindings.usersTable.firstlogin"),
  },
  {
    accessorKey: "lastLogin",
    cell: (cell): string => timeFromNow(String(cell.getValue())),
    header: translate.t("searchFindings.usersTable.lastlogin"),
  },
  {
    accessorKey: "invitationState",
    cell: (cell): JSX.Element => statusFormatter(String(cell.getValue())),
    header: translate.t("searchFindings.usersTable.invitationState"),
  },
  {
    accessorKey: "invitationResend",
    cell: (cell): JSX.Element => cell.getValue<JSX.Element>(),
    header: translate.t("searchFindings.usersTable.invitation"),
  },
];

const OrganizationMembers: React.FC<IOrganizationMembers> = ({
  organizationId,
}): JSX.Element => {
  const { t } = useTranslation();
  const theme = useTheme();

  const [lastOrganization, setLastOrganization] = useStoredState(
    "organizationV2",
    { deleted: false, name: "" },
    localStorage,
  );
  const tableRef = useTable("tblUsers");
  const { ConfirmDialog } = useConfirmDialog();
  const { organizationName } = useParams() as { organizationName: string };
  const { userEmail } = useContext(authContext);
  const credModal = useModal("credManagementModal");
  const useAlert = useDailyAlert("org-members", organizationId);

  const [currentRow, setCurrentRow] = useState<IStakeholderDataSet[]>([]);
  const [isStakeholderModalOpen, setIsStakeholderModalOpen] = useState(false);
  const [stakeholderModalAction, setStakeholderModalAction] = useState<
    "add" | "edit"
  >("add");

  const openAddStakeholderModal = useCallback((): void => {
    setStakeholderModalAction("add");
    setIsStakeholderModalOpen(true);
  }, []);
  const openEditStakeholderModal = useCallback((): void => {
    setStakeholderModalAction("edit");
    setIsStakeholderModalOpen(true);
  }, []);
  const closeStakeholderModal = useCallback((): void => {
    setIsStakeholderModalOpen(false);
  }, []);

  // GraphQL Operations
  const { loadingStakeholders, refetchStakeholders, stakeholders } =
    useOrganizationStakeholdersQuery(organizationId);

  const { orgPolicies, loadingPolicies } = usePolicies(organizationId);

  const [grantStakeholderAccess] = useMutation(ADD_STAKEHOLDER_MUTATION, {
    onCompleted: (mtResult): void => {
      if (mtResult.grantStakeholderOrganizationAccess.success) {
        refetchStakeholders().catch((): void => {
          Logger.error("An error occurred adding stakeholder");
        });
        mixpanel.track("AddUserOrganizationAccess", {
          Organization: organizationName,
        });
        const { email } =
          mtResult.grantStakeholderOrganizationAccess.grantedStakeholder ?? {};
        msgSuccess(
          `${t("searchFindings.tabUsers.success")} ${email}`,
          t("organization.tabs.users.successTitle"),
        );
        setCurrentRow([]);
      }
    },
    onError: (grantError): void => {
      handleGrantError(grantError);
    },
  });

  const [updateStakeholder] = useMutation(UPDATE_STAKEHOLDER_MUTATION, {
    onCompleted: (mtResult): void => {
      if (mtResult.updateOrganizationStakeholder.success) {
        setStakeholderModalAction("add");
        const { email } =
          mtResult.updateOrganizationStakeholder.modifiedStakeholder ?? {};
        refetchStakeholders().catch((): void => {
          Logger.error("An error occurred updating stakeholder");
        });

        mixpanel.track("EditUserOrganizationAccess", {
          Organization: organizationName,
        });
        msgSuccess(
          `${email} ${t("organization.tabs.users.editButton.success")}`,
          t("organization.tabs.users.successTitle"),
        );
        setCurrentRow([]);
      }
    },
    onError: handleMtError,
  });

  const [removeStakeholderAccess, { loading: removing }] = useMutation(
    REMOVE_STAKEHOLDER_MUTATION,
    {
      onError: handleRemoveError,
    },
  );

  // Auxiliary elements
  const handleSubmit = useCallback(
    async (values: IStakeholderFormValues): Promise<void> => {
      closeStakeholderModal();
      if (stakeholderModalAction === "add") {
        await grantStakeholderAccess({
          variables: {
            email: values.email,
            organizationId,
            role: values.role as OrganizationRole,
          },
        });
      } else {
        await updateStakeholder({
          variables: {
            email: values.email,
            organizationId,
            role: values.role as OrganizationRole,
          },
        });
      }
    },
    [
      closeStakeholderModal,
      updateStakeholder,
      grantStakeholderAccess,
      organizationId,
      stakeholderModalAction,
    ],
  );

  const validMutationsHelper = useCallback(
    (areAllMutationValid: boolean[]): void => {
      if (areAllMutationValid.every(Boolean)) {
        mixpanel.track("RemoveUserOrganizationAccess", {
          Organization: organizationName,
        });
        if (areAllMutationValid.length === 1) {
          msgSuccess(
            `${currentRow[0]?.email} ${t(
              "organization.tabs.users.removeButton.success",
            )}`,
            t("organization.tabs.users.successTitle"),
          );
        } else {
          msgSuccess(
            t("organization.tabs.users.removeButton.successPlural"),
            t("organization.tabs.users.successTitle"),
          );
        }
      }
    },
    [currentRow, organizationName, t],
  );

  const handleRemoveStakeholder = useCallback(async (): Promise<void> => {
    const results = await removeMultipleAccess(
      removeStakeholderAccess,
      currentRow,
      organizationId,
    );

    validMutationsHelper(getAreAllMutationValid(results));
    if (
      currentRow.findIndex(
        (stakeholder): boolean => stakeholder.email === userEmail,
      ) > -1
    ) {
      setLastOrganization({ ...lastOrganization, deleted: true });
    }
    setCurrentRow([]);
    await refetchStakeholders();
    setStakeholderModalAction("add");
  }, [
    currentRow,
    organizationId,
    refetchStakeholders,
    removeStakeholderAccess,
    validMutationsHelper,
    userEmail,
    setLastOrganization,
    lastOrganization,
  ]);

  const domainSuggestions = useMemo((): string[] => {
    const domains = Array.from(
      new Set(
        stakeholders
          .filter(({ email }): boolean => email !== null)
          .map(({ email }): string => {
            const [, emailDomain] = (email as string).split("@");

            return emailDomain;
          }),
      ),
    );

    if (userEmail.endsWith("@fluidattacks.com")) {
      return domains;
    }

    return domains.filter((domain): boolean => domain !== "fluidattacks.com");
  }, [stakeholders, userEmail]);

  const stakeholdersList = stakeholders.map(
    (stakeholder): IStakeholderDataSet => {
      function handleResendEmail(
        event: React.MouseEvent<HTMLButtonElement>,
      ): void {
        event.stopPropagation();

        const resendStakeholder = {
          ...stakeholder,
          role: stakeholder.role?.toUpperCase(),
        };
        setStakeholderModalAction("add");
        void handleSubmit(resendStakeholder as IStakeholderFormValues);
      }
      const isPending = stakeholder.invitationState === InvitationState.Pending;

      return {
        ...(stakeholder as IStakeholderDataSet),
        invitationResend: (
          <Button
            disabled={!isPending}
            onClick={handleResendEmail}
            variant={"secondary"}
          >
            {t("organization.members.resendInvitation")}
          </Button>
        ),
      };
    },
  );

  const handleOpenCredModal = useCallback((): void => {
    credModal.open();
  }, [credModal]);
  const filteredStakeholders = stakeholdersList.filter(
    (stakeholder): boolean => {
      return (
        stakeholder.email !== userEmail &&
        (userEmail.endsWith("@fluidattacks.com") ||
          !stakeholder.email.endsWith("@fluidattacks.com"))
      );
    },
  );

  const addUsersModal = (
    <AddUserModal
      action={stakeholderModalAction}
      domainSuggestions={domainSuggestions}
      editTitle={t("organization.tabs.users.modalEditTitle")}
      initialValues={
        stakeholderModalAction === "edit" ? currentRow[0] : undefined
      }
      onClose={closeStakeholderModal}
      onSubmit={handleSubmit}
      open={isStakeholderModalOpen}
      organizationId={organizationId}
      suggestions={[]}
      title={t("organization.tabs.users.modalAddTitle")}
      type={"organization"}
    />
  );

  if (
    filteredStakeholders.length <= 0 &&
    !loadingStakeholders &&
    !loadingPolicies
  ) {
    return (
      <React.Fragment>
        <SectionHeader
          header={t("organization.tabs.users.emptyComponent.header")}
        />
        <EmptyState
          confirmButton={{
            onClick: openAddStakeholderModal,
            text: t("organization.tabs.users.emptyComponent.addButton.text"),
          }}
          description={t("organization.tabs.users.emptyComponent.description")}
          imageSrc={"integrates/resources/memberIcon"}
          title={t("organization.tabs.users.emptyComponent.title")}
        />
        {addUsersModal}
      </React.Fragment>
    );
  }

  return (
    <Fragment>
      <SectionHeader header={t("organization.members.title")} />
      <Container
        bgColor={theme.palette.gray[50]}
        height={"100hv"}
        id={"users"}
        padding={[1, PADDING_B, 1, PADDING_B]}
        px={1.25}
        py={1.25}
      >
        <Alert closable={true} show={useAlert} variant={"info"}>
          <Text
            color={theme.palette.info["700"]}
            fontWeight={"bold"}
            size={"xs"}
          >
            {t("organization.members.policiesAlert", {
              inactivityPeriod: orgPolicies?.organization.inactivityPeriod ?? 0,
            })}
          </Text>
        </Alert>
        <Container
          bgColor={theme.palette.gray[50]}
          padding={[PADDING_A, 0, 0, 0]}
        >
          <Table
            columns={tableColumns}
            csvConfig={{ export: true }}
            data={stakeholdersList}
            extraButtons={
              <ActionsButtons
                areButtonsDisabled={
                  _.isEmpty(currentRow) || removing || loadingStakeholders
                }
                areThereMultipleSelections={currentRow.length > 1}
                handleOpenCredModal={handleOpenCredModal}
                openEditStakeholderModal={openEditStakeholderModal}
              />
            }
            loadingData={loadingStakeholders}
            options={{
              searchPlaceholder: t("organization.members.searchPlaceholder"),
            }}
            rightSideComponents={
              <Can
                do={
                  "integrates_api_mutations_grant_stakeholder_organization_access_mutate"
                }
              >
                <Button
                  id={"add-user"}
                  onClick={openAddStakeholderModal}
                  tooltip={t("organization.members.addUserButton.tooltip")}
                >
                  {t("organization.members.addUserButton.text")}
                </Button>
              </Can>
            }
            rowSelectionSetter={setCurrentRow}
            rowSelectionState={currentRow}
            selectionMode={"checkbox"}
            tableRef={tableRef}
          />
        </Container>
        <RemoveStakeholderModal
          modalRef={credModal}
          onRemove={handleRemoveStakeholder}
          organizationId={organizationId}
          selectedStakeholders={currentRow}
        />
        {addUsersModal}
        <ConfirmDialog />
      </Container>
    </Fragment>
  );
};

export { OrganizationMembers };
