interface IOrganizationMembers {
  organizationId: string;
}

interface IStakeholderDataSet {
  email: string;
  firstLogin: string;
  invitationResend: JSX.Element;
  invitationState: string;
  lastLogin: string;
  role: string;
}

export type { IOrganizationMembers, IStakeholderDataSet };
