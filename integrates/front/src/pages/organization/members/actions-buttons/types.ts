interface IActionsButtonsProps {
  areButtonsDisabled: boolean;
  areThereMultipleSelections: boolean;
  handleOpenCredModal: VoidFunction;
  openEditStakeholderModal: VoidFunction;
}

export type { IActionsButtonsProps };
