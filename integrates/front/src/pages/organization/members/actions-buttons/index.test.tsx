import { PureAbility } from "@casl/ability";
import { screen } from "@testing-library/react";

import { ActionsButtons } from ".";
import { authzPermissionsContext } from "context/authz/config";
import { render } from "mocks";

const mockedPermissions = new PureAbility<string>([
  {
    action:
      "integrates_api_mutations_grant_stakeholder_organization_access_mutate",
  },
  { action: "integrates_api_mutations_update_organization_stakeholder_mutate" },
  {
    action:
      "integrates_api_mutations_remove_stakeholder_organization_access_mutate",
  },
]);

const Wrapper = ({
  areButtonsDisabled,
  areThereMultipleSelections,
  handleOpenCredModal,
  openEditStakeholderModal,
}: Readonly<{
  areButtonsDisabled: boolean;
  areThereMultipleSelections: boolean;
  handleOpenCredModal?: jest.Mock;
  openEditStakeholderModal?: jest.Mock;
}>): JSX.Element => (
  <authzPermissionsContext.Provider value={mockedPermissions}>
    <ActionsButtons
      areButtonsDisabled={areButtonsDisabled}
      areThereMultipleSelections={areThereMultipleSelections}
      handleOpenCredModal={handleOpenCredModal ?? jest.fn()}
      openEditStakeholderModal={openEditStakeholderModal ?? jest.fn()}
    />
  </authzPermissionsContext.Provider>
);

describe("actionsButtons", (): void => {
  const numberOfButtons = 2;

  it("should return a function", (): void => {
    expect.hasAssertions();
    expect(ActionsButtons).toBeInstanceOf(Function);
  });

  it("renders buttons", (): void => {
    expect.hasAssertions();

    render(
      <Wrapper areButtonsDisabled={false} areThereMultipleSelections={false} />,
    );

    const buttons = screen.getAllByRole("button");

    expect(buttons).toHaveLength(numberOfButtons);

    buttons.forEach((button): void => {
      expect(button).not.toBeDisabled();
    });
  });

  it("renders buttons as disabled", (): void => {
    expect.hasAssertions();

    render(
      <Wrapper areButtonsDisabled={true} areThereMultipleSelections={false} />,
    );

    const buttons = screen.getAllByRole("button");

    expect(buttons).toHaveLength(numberOfButtons);

    buttons.forEach((button): void => {
      if (button.id === "addUser") {
        return;
      }

      expect(button).toBeDisabled();
    });
  });

  it("calls callbacks when buttons not disabled", (): void => {
    expect.hasAssertions();

    const handleOpenCredModal = jest.fn();
    const openEditStakeholderModal = jest.fn();

    render(
      <Wrapper
        areButtonsDisabled={false}
        areThereMultipleSelections={false}
        handleOpenCredModal={handleOpenCredModal}
        openEditStakeholderModal={openEditStakeholderModal}
      />,
    );

    const buttons = screen.getAllByRole("button");
    buttons.forEach((button): void => {
      expect(button).not.toBeDisabled();

      button.click();
    });

    expect(handleOpenCredModal).toHaveBeenCalledTimes(1);
    expect(openEditStakeholderModal).toHaveBeenCalledTimes(1);
  });
});
