import { useMutation, useQuery } from "@apollo/client";
import { useAbility } from "@casl/react";
import {
  Alert,
  Button,
  Container,
  EmptyState,
  IconButton,
  useConfirmDialog,
} from "@fluidattacks/design";
import type { FormikHelpers } from "formik";
import { Formik } from "formik";
import _ from "lodash";
import mixpanel from "mixpanel-browser";
import { Fragment, useCallback, useMemo } from "react";
import { useTranslation } from "react-i18next";

import { AddCriteriaModal } from "./add-criteria-modal";
import type {
  IPolicy,
  IPolicyCriteria,
  IPolicyItem,
  IVulnerabilitiesPriority,
} from "./types";
import { validationSchema } from "./validations";

import {
  GET_ORGANIZATION_PRIORITY_POLICIES,
  REMOVE_ORGANIZATION_PRIORITY_POLICY,
  UPDATE_ORGANIZATION_PRIORITY_POLICIES,
} from "../queries";
import { Table } from "components/table";
import { authzPermissionsContext } from "context/authz/config";
import { useModal, useTable } from "hooks";
import { useAudit } from "hooks/use-audit";
import { Logger } from "utils/logger";
import { msgError, msgSuccess } from "utils/notifications";

const VulnerabilitiesPriority = ({
  organizationId,
}: IVulnerabilitiesPriority): JSX.Element => {
  const { t } = useTranslation();
  const { confirm, ConfirmDialog } = useConfirmDialog();
  const permissions = useAbility(authzPermissionsContext);
  const { addAuditEvent } = useAudit();
  const modalProps = useModal("priority-modal");
  const tableRef = useTable("tblPriorityPolicies");
  const { open, close } = modalProps;
  const prefix = "organization.tabs.policies.priority.";
  const modifyError =
    "Exception - The user already modify the policies for the same organization";
  const allowUpdate = permissions.can(
    "integrates_api_mutations_update_organization_priority_policies_mutate",
  );

  // GraphQL Operations
  const { data, loading } = useQuery(GET_ORGANIZATION_PRIORITY_POLICIES, {
    fetchPolicy: "cache-and-network",
    onCompleted: (): void => {
      addAuditEvent("OrgPriorityPolicy", organizationId);
      mixpanel.track("ViewOrgPriorityPolicy");
    },
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error): void => {
        msgError(t("groupAlerts.errorTextsad"));
        Logger.warning(
          "An error occurred fetching organization priority policies",
          error,
        );
      });
    },
    variables: { organizationId },
  });
  const [removePriorityPolicy, { loading: removeLoading }] = useMutation(
    REMOVE_ORGANIZATION_PRIORITY_POLICY,
    {
      context: { skipGlobalErrorHandler: true },
      onCompleted: (): void => {
        msgSuccess(t(`${prefix}removeSuccess`));
        mixpanel.track("DeleteOrgPriorityPolicy");
      },
      onError: ({ graphQLErrors }): void => {
        graphQLErrors.forEach((error): void => {
          if (error.message === modifyError) {
            msgError(t(`${prefix}error`));
          } else {
            msgError(t("groupAlerts.errorTextsad"));
            Logger.warning(
              "An error occurred removing organization priority policy",
              error,
            );
          }
        });
      },
      refetchQueries: [GET_ORGANIZATION_PRIORITY_POLICIES],
    },
  );
  const [updatePriorityPolicies, { loading: updateLoading }] = useMutation(
    UPDATE_ORGANIZATION_PRIORITY_POLICIES,
    {
      context: { skipGlobalErrorHandler: true },
      onCompleted: (): void => {
        msgSuccess(t(`${prefix}success`));
        mixpanel.track("UpdateOrgPriorityPolicy");
      },
      onError: ({ graphQLErrors }): void => {
        graphQLErrors.forEach((error): void => {
          if (error.message === modifyError) {
            msgError(t(`${prefix}error`));
          } else {
            msgError(t("groupAlerts.errorTextsad"));
            Logger.warning(
              "An error occurred updating organization priority policies",
              error,
            );
          }
        });
      },
      refetchQueries: [GET_ORGANIZATION_PRIORITY_POLICIES],
    },
  );

  // Data management
  const loadingState = updateLoading || removeLoading;
  const criteria = useMemo((): IPolicy[] => {
    return (data?.organization.priorityPolicies ?? []).map(
      ({ policy, value }): IPolicy => ({ policy, value }),
    );
  }, [data]);

  const onConfirmDelete = useCallback(
    (priorityPolicy: IPolicy): (() => Promise<void>) => {
      return async (): Promise<void> => {
        const confirmResult = await confirm({
          message: t(`${prefix}confirmModal.description`, {
            criteria: priorityPolicy.policy.split("(")[0],
          }),
          title: t(`${prefix}confirmModal.title`),
        });
        if (confirmResult) {
          await Promise.all([
            removePriorityPolicy({
              variables: {
                organizationId,
                priorityPolicy: priorityPolicy.policy,
              },
            }),
          ]);
        }
      };
    },
    [confirm, organizationId, removePriorityPolicy, t],
  );
  const tableData = useMemo((): IPolicyItem[] => {
    return _.orderBy(
      criteria.map(
        (item): IPolicyItem => ({
          element: (
            <IconButton
              disabled={permissions.cannot(
                "integrates_api_mutations_remove_organization_priority_policy_mutate",
              )}
              icon={"trash"}
              iconSize={"xxs"}
              iconType={"fa-light"}
              id={"remove-priority"}
              onClick={onConfirmDelete(item)}
              variant={"ghost"}
            />
          ),
          policy: item.policy.split("(")[0],
          value: item.value,
        }),
      ),
      ["value"],
      ["desc"],
    );
  }, [criteria, onConfirmDelete, permissions]);

  const handleSubmit = useCallback(
    async (
      values: IPolicyCriteria,
      formikHelpers: FormikHelpers<IPolicyCriteria>,
    ): Promise<void> => {
      const uniquePolicies = criteria.filter(
        ({ policy: storedPolicy }): boolean =>
          !values.criteria
            .map(({ policy }): string => policy)
            .includes(storedPolicy),
      );
      const policiesToUpdate = uniquePolicies.concat(
        values.criteria
          .filter(({ policy }): boolean => !_.isEmpty(policy))
          .map(({ policy, value }): { policy: string; value: number } => ({
            policy,
            value,
          })),
      );
      const confirmResult = await confirm({
        id: "confirm-update",
        message: (
          <Alert variant={"info"}>
            {t(`${prefix}confirmUpdateModal.description`)}
          </Alert>
        ),
        title: t(`${prefix}confirmUpdateModal.title`),
      });

      if (confirmResult) {
        formikHelpers.resetForm();
        close();

        await Promise.all([
          updatePriorityPolicies({
            variables: { organizationId, priorityPolicies: policiesToUpdate },
          }),
        ]);
      }
    },
    [close, confirm, criteria, updatePriorityPolicies, organizationId, t],
  );

  return (
    <Container mx={1.25} my={1.25}>
      <Formik
        initialValues={{
          criteria: [{ policy: "", value: 0 }],
          optionsCriteria: "",
        }}
        onSubmit={handleSubmit}
        validationSchema={validationSchema}
      >
        <Fragment>
          {tableData.length === 0 && !loading ? (
            <EmptyState
              confirmButton={
                allowUpdate
                  ? { onClick: open, text: t(`${prefix}empty.button`) }
                  : undefined
              }
              description={t(`${prefix}empty.description`)}
              imageSrc={"integrates/empty/addRoot"}
              title={t(`${prefix}empty.title`)}
            />
          ) : (
            <Table
              columns={[
                {
                  accessorKey: "policy",
                  cell: (cell): string => String(cell.getValue()),
                  header: "Policy",
                },
                {
                  accessorKey: "value",
                  header: "Priority score",
                  sortDescFirst: true,
                  sortUndefined: "last",
                },
                {
                  accessorKey: "element",
                  cell: (cell): JSX.Element => cell.getValue() as JSX.Element,
                  enableSorting: false,
                  header: "",
                },
              ]}
              data={tableData}
              dataPrivate={false}
              loadingData={loading || loadingState}
              rightSideComponents={
                <Button
                  disabled={!allowUpdate}
                  icon={"plus"}
                  id={"add-priority"}
                  onClick={open}
                >
                  {t(`${prefix}button`)}
                </Button>
              }
              tableRef={tableRef}
            />
          )}
          <ConfirmDialog />
          <AddCriteriaModal loading={updateLoading} modalRef={modalProps} />
        </Fragment>
      </Formik>
    </Container>
  );
};

export { VulnerabilitiesPriority };
