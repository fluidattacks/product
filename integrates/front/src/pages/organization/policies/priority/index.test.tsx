import { PureAbility } from "@casl/ability";
import { screen, waitFor } from "@testing-library/react";
import { userEvent } from "@testing-library/user-event";
import { GraphQLError } from "graphql";
import isEqual from "lodash/isEqual";
import { HttpResponse, type StrictResponse, graphql } from "msw";

import { VulnerabilitiesPriority } from ".";
import { authContext } from "context/auth";
import { authzPermissionsContext } from "context/authz/config";
import type {
  GetOrganizationPriorityPoliciesQuery as GetOrganizationPriorityPolicies,
  RemoveOrganizationPriorityPolicyMutation,
  UpdateOrganizationPriorityPoliciesMutation as UpdateOrganizationPriorityPolicies,
} from "gql/graphql";
import { render } from "mocks";
import { LINK } from "mocks/constants";
import type { IErrorMessage } from "mocks/types";
import {
  GET_ORGANIZATION_PRIORITY_POLICIES,
  REMOVE_ORGANIZATION_PRIORITY_POLICY,
  UPDATE_ORGANIZATION_PRIORITY_POLICIES,
} from "pages/organization/policies/queries";
import { msgSuccess } from "utils/notifications";

const graphqlMocked = graphql.link(LINK);

jest.mock("utils/notifications", (): Record<string, unknown> => {
  const mockedNotifications: Record<string, () => Record<string, unknown>> =
    jest.requireActual("utils/notifications");

  jest.spyOn(mockedNotifications, "msgError").mockImplementation();
  jest.spyOn(mockedNotifications, "msgSuccess").mockImplementation();

  return mockedNotifications;
});

describe("priority", (): void => {
  it("should show and update the priority policies", async (): Promise<void> => {
    expect.hasAssertions();

    const mockedPermissions = new PureAbility<string>([
      {
        action:
          "integrates_api_mutations_update_organization_priority_policies_mutate",
      },
      {
        action:
          "integrates_api_mutations_remove_organization_priority_policy_mutate",
      },
    ]);
    render(
      <authContext.Provider
        value={{
          awsSubscription: null,
          tours: {
            newGroup: true,
            newRoot: true,
            welcome: true,
          },
          userEmail: "test@fluidattacks.com",
          userName: "test",
        }}
      >
        <authzPermissionsContext.Provider value={mockedPermissions}>
          <VulnerabilitiesPriority
            organizationId={"ORG#f8a7ebe0-01a6-b3ff-2e1e-2960d7f7c9fa"}
          />
        </authzPermissionsContext.Provider>
      </authContext.Provider>,
      {
        mocks: [
          graphqlMocked.query(
            GET_ORGANIZATION_PRIORITY_POLICIES,
            (): StrictResponse<{ data: GetOrganizationPriorityPolicies }> => {
              return HttpResponse.json({
                data: {
                  organization: {
                    __typename: "Organization",
                    name: "testPriority",
                    priorityPolicies: [
                      {
                        __typename: "PriorityPolicy",
                        policy: "CSPM",
                        value: -25,
                      },
                    ],
                  },
                },
              });
            },
            { once: true },
          ),
          graphqlMocked.mutation(
            UPDATE_ORGANIZATION_PRIORITY_POLICIES,
            ({
              variables,
            }): StrictResponse<
              | IErrorMessage
              | {
                  data: UpdateOrganizationPriorityPolicies;
                }
            > => {
              const { organizationId, priorityPolicies } = variables;
              if (
                organizationId === "ORG#f8a7ebe0-01a6-b3ff-2e1e-2960d7f7c9fa" &&
                isEqual(priorityPolicies, [
                  { policy: "CSPM", value: -25 },
                  { policy: "PTAAS", value: 25 },
                ])
              ) {
                return HttpResponse.json({
                  data: {
                    updateOrganizationPriorityPolicies: {
                      success: true,
                    },
                  },
                });
              }

              return HttpResponse.json({
                errors: [
                  new GraphQLError(
                    "Exception - An error occurred updating organization priority policies",
                  ),
                ],
              });
            },
            { once: true },
          ),
          graphqlMocked.query(
            GET_ORGANIZATION_PRIORITY_POLICIES,
            (): StrictResponse<{ data: GetOrganizationPriorityPolicies }> => {
              return HttpResponse.json({
                data: {
                  organization: {
                    __typename: "Organization",
                    name: "testPriority",
                    priorityPolicies: [
                      {
                        __typename: "PriorityPolicy",
                        policy: "CSPM",
                        value: -25,
                      },
                      {
                        __typename: "PriorityPolicy",
                        policy: "PTAAS",
                        value: 25,
                      },
                    ],
                  },
                },
              });
            },
            { once: true },
          ),
        ],
      },
    );

    await userEvent.click(
      screen.getByRole("button", {
        name: "organization.tabs.policies.priority.button",
      }),
    );

    expect(
      screen.getByText("organization.tabs.policies.priority.modal.title"),
    ).toBeInTheDocument();

    await userEvent.click(
      screen.getByText(
        "organization.tabs.policies.priority.criteria.placeholder",
      ),
    );
    await userEvent.click(
      screen.getByText(
        "organization.tabs.policies.priority.criteria.options.technique",
      ),
    );
    await userEvent.click(
      screen.getByText("organization.tabs.policies.priority.modal.placeholder"),
    );
    await userEvent.click(screen.getByRole("listitem", { name: "PTAAS" }));
    await userEvent.type(screen.getByRole("spinbutton"), "25");
    await userEvent.click(
      screen.getByRole("button", { name: "buttons.confirm" }),
    );
    await userEvent.click(screen.getByRole("button", { name: "Confirm" }));

    await waitFor((): void => {
      expect(msgSuccess).toHaveBeenCalledWith(
        "organization.tabs.policies.priority.success",
      );
    });
    await waitFor((): void => {
      expect(screen.getAllByRole("row")[1].textContent).toBe("PTAAS25");
    });
    jest.clearAllMocks();
  });

  it("should remove the priority policies", async (): Promise<void> => {
    expect.hasAssertions();

    const mockedPermissions = new PureAbility<string>([
      {
        action:
          "integrates_api_mutations_update_organization_priority_policies_mutate",
      },
      {
        action:
          "integrates_api_mutations_remove_organization_priority_policy_mutate",
      },
    ]);
    render(
      <authContext.Provider
        value={{
          awsSubscription: null,
          tours: {
            newGroup: true,
            newRoot: true,
            welcome: true,
          },
          userEmail: "test@fluidattacks.com",
          userName: "test",
        }}
      >
        <authzPermissionsContext.Provider value={mockedPermissions}>
          <VulnerabilitiesPriority
            organizationId={"ORG#f8a7ebe0-01a6-b3ff-2e1e-2960d7f7c9fa"}
          />
        </authzPermissionsContext.Provider>
      </authContext.Provider>,
      {
        mocks: [
          graphqlMocked.query(
            GET_ORGANIZATION_PRIORITY_POLICIES,
            (): StrictResponse<{ data: GetOrganizationPriorityPolicies }> => {
              return HttpResponse.json({
                data: {
                  organization: {
                    __typename: "Organization",
                    name: "testPriority",
                    priorityPolicies: [
                      {
                        __typename: "PriorityPolicy",
                        policy: "PTAAS",
                        value: 100,
                      },
                      {
                        __typename: "PriorityPolicy",
                        policy: "CSPM",
                        value: 40,
                      },
                      {
                        __typename: "PriorityPolicy",
                        policy: "Attacked (E:A)",
                        value: 100,
                      },
                      {
                        __typename: "PriorityPolicy",
                        policy: "Network (AV:N)",
                        value: 100,
                      },
                    ],
                  },
                },
              });
            },
            { once: true },
          ),
          graphqlMocked.mutation(
            REMOVE_ORGANIZATION_PRIORITY_POLICY,
            ({
              variables,
            }): StrictResponse<
              | IErrorMessage
              | {
                  data: RemoveOrganizationPriorityPolicyMutation;
                }
            > => {
              const { organizationId, priorityPolicy } = variables;
              if (
                organizationId === "ORG#f8a7ebe0-01a6-b3ff-2e1e-2960d7f7c9fa" &&
                priorityPolicy === "CSPM"
              ) {
                return HttpResponse.json({
                  data: {
                    removeOrganizationPriorityPolicy: {
                      success: true,
                    },
                  },
                });
              }

              return HttpResponse.json({
                errors: [
                  new GraphQLError(
                    "Exception - An error occurred removing organization priority policies",
                  ),
                ],
              });
            },
            { once: true },
          ),
          graphqlMocked.query(
            GET_ORGANIZATION_PRIORITY_POLICIES,
            (): StrictResponse<{ data: GetOrganizationPriorityPolicies }> => {
              return HttpResponse.json({
                data: {
                  organization: {
                    __typename: "Organization",
                    name: "testPriority",
                    priorityPolicies: [
                      {
                        __typename: "PriorityPolicy",
                        policy: "PTAAS",
                        value: 100,
                      },
                      {
                        __typename: "PriorityPolicy",
                        policy: "Attacked (E:A)",
                        value: 100,
                      },
                      {
                        __typename: "PriorityPolicy",
                        policy: "Network (AV:N)",
                        value: 100,
                      },
                    ],
                  },
                },
              });
            },
            { once: true },
          ),
        ],
      },
    );

    await waitFor((): void => {
      expect(screen.getAllByRole("row")[4].textContent).toBe("CSPM40");
    });

    expect(screen.getAllByRole("row")).toHaveLength(5);

    await userEvent.click(
      screen.getAllByRole("button", {
        name: "remove-priority",
      })[3],
    );

    expect(
      screen.getByText(
        "organization.tabs.policies.priority.confirmModal.title",
      ),
    ).toBeInTheDocument();

    expect(
      screen.getByText(
        "organization.tabs.policies.priority.confirmModal.description",
      ),
    ).toBeInTheDocument();

    await userEvent.click(screen.getByRole("button", { name: "Confirm" }));
  });
});
