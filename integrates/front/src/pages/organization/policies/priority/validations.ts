import { array, number, object, string } from "yup";

import type { IPolicy } from "./types";

import { translate } from "utils/translations/translate";

const MAX_PRIORITY_VALUE = 1000;
const MIN_PRIORITY_VALUE = -1000;

const validationSchema = object().shape({
  criteria: array().of(
    object().shape({
      policy: string()
        .required(translate.t("validations.required"))
        .isValidFunction((value, options): boolean => {
          const polices =
            (
              options.from?.[1]?.value as { criteria: IPolicy[] } | undefined
            )?.criteria.map((item: IPolicy): string => item.policy) ?? [];
          const isDuplicate =
            polices.filter((item): boolean => item === value).length > 1;

          return !isDuplicate;
        }, translate.t("validations.uniqueCriteria")),
      value: number()
        .required(translate.t("validations.required"))
        .min(
          MIN_PRIORITY_VALUE,
          translate.t("validations.between", {
            max: MAX_PRIORITY_VALUE,
            min: MIN_PRIORITY_VALUE,
          }),
        )
        .max(
          MAX_PRIORITY_VALUE,
          translate.t("validations.between", {
            max: MAX_PRIORITY_VALUE,
            min: MIN_PRIORITY_VALUE,
          }),
        )
        .isValidFunction(
          (value): boolean => value !== 0,
          translate.t("validations.noZero"),
        ),
    }),
  ),
});

export { validationSchema };
