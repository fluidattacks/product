import type { IUseModal } from "hooks/use-modal";

interface ICriteriaProps {
  loading: boolean;
  modalRef: IUseModal;
}

interface IVulnerabilitiesPriority {
  organizationId: string;
}

interface IPolicy {
  policy: string;
  value: number;
}

interface IPolicyItem {
  element: JSX.Element;
  policy: string;
  value: number;
}

interface IPolicyCriteria {
  criteria: IPolicy[];
  optionsCriteria: string;
}

export type {
  ICriteriaProps,
  IPolicy,
  IPolicyCriteria,
  IPolicyItem,
  IVulnerabilitiesPriority,
};
