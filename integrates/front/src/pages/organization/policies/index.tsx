import * as React from "react";
import { Navigate, Route, Routes } from "react-router-dom";

import { Acceptance } from "./acceptance";
import { PoliciesTemplate } from "./index.template";
import { PoliciesSettings } from "./policies-settings";
import { VulnerabilitiesPriority } from "./priority";
import type { IOrganizationPolicies } from "./types";

import { useOrgUrl } from "pages/organization/hooks";

const OrganizationPolicies: React.FC<IOrganizationPolicies> = ({
  organizationId,
}): JSX.Element => {
  const { url } = useOrgUrl("/policies/*");

  const policiesSettings = <PoliciesSettings organizationId={organizationId} />;
  const acceptance = <Acceptance organizationId={organizationId} />;
  const priority = <VulnerabilitiesPriority organizationId={organizationId} />;

  return (
    <PoliciesTemplate url={url}>
      <Routes>
        <Route element={policiesSettings} path={"/settings"} />
        <Route element={acceptance} path={"/acceptance"} />
        <Route element={priority} path={"/priority"} />
        <Route element={<Navigate to={`${url}/settings`} />} path={"*"} />
      </Routes>
    </PoliciesTemplate>
  );
};

export { OrganizationPolicies };
