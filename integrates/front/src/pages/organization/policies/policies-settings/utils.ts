import { Logger } from "utils/logger";
import { msgError } from "utils/notifications";
import { translate } from "utils/translations/translate";

const handleError = (message: string): void => {
  const tPath = "organization.tabs.policies.";
  switch (message) {
    case "Exception - Days until it breaks must be a number greater than 0":
      msgError(translate.t(`${tPath}errors.daysUntilItBreaks`));
      break;
    case "Exception - Grace period value should not be higher than Days until it breaks value":
      msgError(translate.t(`${tPath}errors.daysUntilItBreaksRange`));
      break;
    case "Exception - Inactivity period should be greater than the provided value":
      msgError(translate.t(`${tPath}errors.inactivityPeriod`));
      break;
    case "Exception - Vulnerability grace period value should be a positive integer":
      msgError(translate.t(`${tPath}errors.vulnerabilityGracePeriod`));
      break;
    case "Exception - Acceptance days should be a positive integer":
      msgError(translate.t(`${tPath}errors.maxAcceptanceDays`));
      break;
    case "Exception - Severity value must be a positive floating number between 0.0 and 10.0":
      msgError(translate.t(`${tPath}errors.acceptanceSeverity`));
      break;
    case "Exception - Severity value must be between 0.0 and 10.0":
      msgError(translate.t(`${tPath}errors.invalidBreakableSeverity`));
      break;
    case "Exception - Min acceptance severity value should not be higher than the max value":
      msgError(translate.t(`${tPath}errors.acceptanceSeverityRange`));
      break;
    case "Exception - Number of acceptances should be zero or positive":
      msgError(translate.t(`${tPath}errors.maxNumberAcceptances`));
      break;
    default:
      msgError(translate.t("groupAlerts.errorTextsad"));
      Logger.warning("An error occurred updating the policies", message);
  }
};

export { handleError };
