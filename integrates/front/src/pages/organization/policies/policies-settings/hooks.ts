import { useMutation, useQuery } from "@apollo/client";
import { capitalize } from "lodash";
import mixpanel from "mixpanel-browser";
import { useCallback } from "react";
import { useTranslation } from "react-i18next";

import { handleError } from "./utils";

import {
  GET_POLICIES,
  UPDATE_GROUPS_POLICIES,
  UPDATE_ORGANIZATION_POLICIES,
} from "../queries";
import type { IFormikPoliciesValues } from "features/policies/types";
import type { GetPoliciesAtOrganizationQuery } from "gql/graphql";
import { useAudit } from "hooks/use-audit";
import { Logger } from "utils/logger";
import { msgError, msgSuccess } from "utils/notifications";

const tPath = "organization.tabs.policies.";
const defaultInactivityPeriod = 90;

interface IUsePolicies {
  data: GetPoliciesAtOrganizationQuery | undefined;
  loading: boolean;
  handlePoliciesUpdate: (values: IFormikPoliciesValues) => Promise<void>;
  handleResetToOrgPolicies: (selectedGroups: string[]) => Promise<void>;
}

const usePolicies = (organizationId: string): IUsePolicies => {
  const { addAuditEvent } = useAudit();
  const { t } = useTranslation();

  const { data, loading } = useQuery(GET_POLICIES, {
    onCompleted: (): void => {
      addAuditEvent("Organization.Policy", organizationId);
    },
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error): void => {
        msgError(t("groupAlerts.errorTextsad"));
        Logger.warning(
          "An error occurred fetching organization policies",
          error,
        );
      });
    },
    variables: { organizationId },
  });

  const [updateOrgPolicies] = useMutation(UPDATE_ORGANIZATION_POLICIES, {
    onCompleted: (): void => {
      mixpanel.track("UpdateOrganizationPolicies");
      msgSuccess(t(`${tPath}success`), t(`${tPath}successTitle`));
    },
    onError: (error): void => {
      error.graphQLErrors.forEach(({ message }): void => {
        handleError(message);
      });
    },
    refetchQueries: [{ query: GET_POLICIES, variables: { organizationId } }],
  });

  const [updateGroupPolicies] = useMutation(UPDATE_GROUPS_POLICIES, {
    onError: (error): void => {
      error.graphQLErrors.forEach(({ message }): void => {
        handleError(message);
      });
    },
    refetchQueries: [{ query: GET_POLICIES, variables: { organizationId } }],
  });

  const handlePoliciesUpdate = useCallback(
    async (values: IFormikPoliciesValues): Promise<void> => {
      const {
        vulnerabilityGracePeriod,
        maxNumberAcceptances,
        daysUntilItBreaks,
        inactivityPeriod,
        maxAcceptanceDays,
        maxAcceptanceSeverity,
        minAcceptanceSeverity,
        minBreakingSeverity,
      } = values;

      if (
        values.__typename === "Organization" &&
        data?.organization.name !== undefined
      )
        await updateOrgPolicies({
          variables: {
            daysUntilItBreaks,
            inactivityPeriod: inactivityPeriod ?? defaultInactivityPeriod,
            maxAcceptanceDays,
            maxAcceptanceSeverity,
            maxNumberAcceptances,
            minAcceptanceSeverity,
            minBreakingSeverity,
            organizationId,
            organizationName: data.organization.name.toLowerCase(),
            vulnerabilityGracePeriod,
          },
        });
      else if (values.groups === undefined) {
        msgError(t("groupAlerts.errorTextsad"));
      } else {
        await updateGroupPolicies({
          onCompleted: (): void => {
            mixpanel.track("UpdateGroupPolicies");
            msgSuccess(
              t("policies.updatedSuccessfully"),
              t("policies.groupUpdated", {
                group: capitalize(values.groups?.[0]),
              }),
            );
          },
          variables: {
            daysUntilItBreaks,
            groups: values.groups,
            maxAcceptanceDays,
            maxAcceptanceSeverity,
            maxNumberAcceptances,
            minAcceptanceSeverity,
            minBreakingSeverity,
            organizationId,
            vulnerabilityGracePeriod,
          },
        });
      }
    },
    [
      data?.organization.name,
      organizationId,
      updateOrgPolicies,
      updateGroupPolicies,
      t,
    ],
  );

  const handleResetToOrgPolicies = useCallback(
    async (selectedGroups: string[]): Promise<void> => {
      await updateGroupPolicies({
        onCompleted: (): void => {
          mixpanel.track("UpdateGroupPolicies");
          msgSuccess(
            t("policies.reset.successDescription"),
            t("policies.reset.successTitle"),
          );
        },
        variables: {
          daysUntilItBreaks: data?.organization.daysUntilItBreaks,
          groups: selectedGroups,
          maxAcceptanceDays: data?.organization.maxAcceptanceDays,
          maxAcceptanceSeverity: data?.organization.maxAcceptanceSeverity,
          maxNumberAcceptances: data?.organization.maxNumberAcceptances,
          minAcceptanceSeverity: data?.organization.minAcceptanceSeverity,
          minBreakingSeverity: data?.organization.minBreakingSeverity,
          organizationId,
          vulnerabilityGracePeriod: data?.organization.vulnerabilityGracePeriod,
        },
      });
    },
    [organizationId, data, updateGroupPolicies, t],
  );

  return {
    data,
    handlePoliciesUpdate,
    handleResetToOrgPolicies,
    loading,
  };
};

export { usePolicies };
