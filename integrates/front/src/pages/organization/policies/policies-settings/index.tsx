import { Container, Loading } from "@fluidattacks/design";
import * as React from "react";

import { usePolicies } from "./hooks";
import type { IPoliciesSettings } from "./types";

import { Policies } from "features/policies";

const PoliciesSettings: React.FC<IPoliciesSettings> = ({
  organizationId,
}: Readonly<IPoliciesSettings>): JSX.Element => {
  const { data, loading, handlePoliciesUpdate, handleResetToOrgPolicies } =
    usePolicies(organizationId);

  if (loading) {
    return (
      <Container
        alignItems={"center"}
        display={"flex"}
        justify={"center"}
        left={"50%"}
        position={"absolute"}
        top={"50%"}
      >
        <Loading size={"lg"} />
      </Container>
    );
  }

  if (data === undefined) return <div />;

  return (
    <Policies
      data={data.organization}
      handleResetToOrgPolicies={handleResetToOrgPolicies}
      handleSubmit={handlePoliciesUpdate}
    />
  );
};

export { PoliciesSettings };
