interface IOrganizationPolicies {
  organizationId: string;
}

interface IPoliciesTemplateProps {
  url: string;
  children: JSX.Element;
}

export type { IOrganizationPolicies, IPoliciesTemplateProps };
