import { Container } from "@fluidattacks/design";
import type { ITabProps } from "@fluidattacks/design/dist/components/tabs/types";
import * as React from "react";
import { useTranslation } from "react-i18next";

import type { IPoliciesTemplateProps } from "./types";

import { SectionHeader } from "components/section-header";

const PoliciesTemplate: React.FC<IPoliciesTemplateProps> = ({
  url,
  children,
}): JSX.Element => {
  const { t } = useTranslation();

  const tabsItems: ITabProps[] = [
    {
      id: "policies-tab",
      label: t("organization.tabs.policies.text"),
      link: `${url}/settings`,
    },
    {
      id: "acceptance-tab",
      label: t("organization.tabs.policies.acceptance.text"),
      link: `${url}/acceptance`,
    },
    {
      id: "priority-tab",
      label: t("organization.tabs.policies.priority.text"),
      link: `${url}/priority`,
    },
  ];

  return (
    <React.StrictMode>
      <SectionHeader
        header={t("organization.tabs.policies.header.title")}
        tabs={tabsItems}
      />
      <Container mt={0.25}>{children}</Container>
    </React.StrictMode>
  );
};

export { PoliciesTemplate };
