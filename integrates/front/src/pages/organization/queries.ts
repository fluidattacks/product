import { graphql } from "gql";

const GET_ORGANIZATION_ID = graphql(`
  query GetOrganizationId($organizationName: String!) {
    organizationId(organizationName: $organizationName) {
      id
      name
    }
  }
`);

const GET_ORGANIZATION_DATA = graphql(`
  query GetOrganizationData($organizationName: String!) {
    me {
      userEmail
      tags(organizationName: $organizationName) {
        name
        groups {
          name
        }
      }
    }
    organizationId(organizationName: $organizationName) {
      id
      hasZtnaRoots
      name
      permissions
    }
  }
`);

export { GET_ORGANIZATION_DATA, GET_ORGANIZATION_ID };
