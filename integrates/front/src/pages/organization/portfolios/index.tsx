import { Container, EmptyState } from "@fluidattacks/design";
import type { ColumnDef, Row as tanRow } from "@tanstack/react-table";
import { capitalize } from "lodash";
import isEmpty from "lodash/isEmpty";
import type { FormEvent } from "react";
import { StrictMode, useCallback, useEffect } from "react";
import React from "react";
import { useTranslation } from "react-i18next";
import { useLocation, useNavigate } from "react-router-dom";

import { SectionHeader } from "components/section-header";
import { Table } from "components/table";
import { useTable } from "hooks";
import { useAudit } from "hooks/use-audit";
import type {
  IOrganizationPortfoliosProps,
  IPortfolios,
  IPortfoliosTable,
} from "pages/organization/portfolios/types";
import { openUrl } from "utils/resource-helpers";

const OrganizationPortfolios: React.FC<IOrganizationPortfoliosProps> = ({
  organizationId,
  portfolios,
}): JSX.Element => {
  const { pathname } = useLocation();
  const { t } = useTranslation();
  const navigate = useNavigate();

  const { addAuditEvent } = useAudit();
  useEffect((): void => {
    addAuditEvent("Organization.Portfolios", organizationId);
  }, [addAuditEvent, organizationId]);

  const tableRef = useTable("tblGroups");
  const formatPortfolioDescription = (groups: { name: string }[]): string => {
    const MAX_NUMBER_OF_GROUPS = 6;
    const mainDescription = groups
      .map((group): string => capitalize(group.name))
      .slice(0, MAX_NUMBER_OF_GROUPS)
      .join(", ");
    const remaining = groups.length - MAX_NUMBER_OF_GROUPS;
    const remainingDescription =
      remaining > 0
        ? t("organization.tabs.portfolios.remainingDescription", { remaining })
        : "";

    return mainDescription + remainingDescription;
  };

  const goToDocs = useCallback((): void => {
    openUrl(
      "https://help.fluidattacks.com/portal/en/kb/articles/sort-groups-into-portfolios",
    );
  }, []);

  const formatPortfolioTableData = (
    portfoliosList: IPortfolios[],
  ): IPortfoliosTable[] =>
    portfoliosList.map(
      (portfolio): IPortfoliosTable => ({
        groups: formatPortfolioDescription(portfolio.groups),
        nGroups: portfolio.groups.length,
        portfolio: portfolio.name,
      }),
    );

  const goToPortfolio = useCallback(
    (portfolioName: string): void => {
      navigate(`${pathname}/${portfolioName.toLowerCase()}/analytics`);
    },
    [navigate, pathname],
  );

  const handleRowClick = useCallback(
    (rowInfo: tanRow<IPortfoliosTable>): ((event: FormEvent) => void) => {
      return (event: FormEvent): void => {
        goToPortfolio(rowInfo.getValue("portfolio"));
        event.preventDefault();
      };
    },
    [goToPortfolio],
  );

  const tableHeaders: ColumnDef<IPortfoliosTable>[] = [
    {
      accessorKey: "portfolio",
      header: t("organization.tabs.portfolios.table.portfolio"),
    },
    {
      accessorKey: "nGroups",
      header: t("organization.tabs.portfolios.table.nGroups"),
    },
    {
      accessorKey: "groups",
      header: t("organization.tabs.portfolios.table.groups"),
    },
  ];

  const sectionHeader = (
    <SectionHeader header={t("organization.tabs.portfolios.title")} />
  );

  if (isEmpty(portfolios)) {
    return (
      <React.Fragment>
        {sectionHeader}
        <div className={"flex flex-column items-center"}>
          <EmptyState
            cancelButton={{
              onClick: goToDocs,
              text: t("app.learnMore"),
            }}
            description={t("organization.tabs.portfolios.empty.description")}
            imageSrc={"integrates/empty/portfoliosIcon"}
            title={t("organization.tabs.portfolios.empty.title")}
          />
        </div>
      </React.Fragment>
    );
  }

  return (
    <StrictMode>
      {sectionHeader}
      <Container px={1.25} py={1.25}>
        {isEmpty(portfolios) ? undefined : (
          <Table
            columns={tableHeaders}
            data={formatPortfolioTableData(portfolios)}
            dataPrivate={false}
            onRowClick={handleRowClick}
            options={{
              searchPlaceholder: t("organization.portfolios.searchPlaceholder"),
            }}
            tableRef={tableRef}
          />
        )}
      </Container>
    </StrictMode>
  );
};

export { OrganizationPortfolios };
