import { screen, waitFor } from "@testing-library/react";
import { userEvent } from "@testing-library/user-event";
import { Route, Routes } from "react-router-dom";

import { render } from "mocks";
import { OrganizationPortfolios } from "pages/organization/portfolios";
import type { IPortfolios } from "pages/organization/portfolios/types";

describe("organizationPortfolios", (): void => {
  const memoryRouterPortfolio = {
    initialEntries: ["/portfolios"],
  };

  const portfoliosInfo: IPortfolios[] = [
    {
      groups: [{ name: "continuoustesting" }, { name: "oneshottest" }],
      name: "front testing",
    },
    {
      groups: [
        { name: "continuoustesting" },
        { name: "unittesting" },
        { name: "oneshottest" },
      ],
      name: "test-groups",
    },
  ];

  it("should render table columns", async (): Promise<void> => {
    expect.hasAssertions();

    render(
      <Routes>
        <Route
          element={
            <OrganizationPortfolios
              organizationId={"test"}
              portfolios={portfoliosInfo}
            />
          }
          path={"/portfolios"}
        />
      </Routes>,
      { memoryRouter: memoryRouterPortfolio },
    );

    expect(screen.queryAllByRole("table")).toHaveLength(1);
    expect(
      screen.getByText("organization.tabs.portfolios.table.portfolio"),
    ).toBeInTheDocument();
    expect(
      screen.getByText("organization.tabs.portfolios.table.nGroups"),
    ).toBeInTheDocument();
    expect(
      screen.getByText("organization.tabs.portfolios.table.groups"),
    ).toBeInTheDocument();

    await waitFor((): void => {
      expect(screen.queryByText("front testing")).toBeInTheDocument();
    });

    expect(screen.getByText("2")).toBeInTheDocument();
    expect(
      screen.getByText("Continuoustesting, Oneshottest"),
    ).toBeInTheDocument();
    expect(screen.getByText("test-groups")).toBeInTheDocument();
    expect(screen.getByText("3")).toBeInTheDocument();
    expect(
      screen.getByText("Continuoustesting, Unittesting, Oneshottest"),
    ).toBeInTheDocument();
  });

  it("should filter by search", async (): Promise<void> => {
    expect.hasAssertions();

    render(
      <Routes>
        <Route
          element={
            <OrganizationPortfolios
              organizationId={"test"}
              portfolios={portfoliosInfo}
            />
          }
          path={"/portfolios"}
        />
      </Routes>,
      { memoryRouter: memoryRouterPortfolio },
    );

    expect(screen.queryAllByRole("table")).toHaveLength(1);

    const searchField = screen.getByPlaceholderText(
      "organization.portfolios.searchPlaceholder",
    );

    await userEvent.type(searchField, "front");

    await waitFor((): void => {
      expect(screen.queryByText("front testing")).toBeInTheDocument();
    });

    await waitFor((): void => {
      expect(screen.queryByText("test-groups")).not.toBeInTheDocument();
    });
  });
});
