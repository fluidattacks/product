interface IMailmapProps {
  organizationId: string;
}

interface IFormattedMailmapEntryEmailProps {
  entryEmail: string;
  domainsCheck: boolean;
}

export type { IMailmapProps, IFormattedMailmapEntryEmailProps };
