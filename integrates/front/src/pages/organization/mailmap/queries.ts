import { graphql } from "gql";

const CREATE_MAILMAP_ENTRY_MUTATION = graphql(`
  mutation createMailmapEntry(
    $entry: MailmapEntryInput!
    $organizationId: String!
  ) {
    createMailmapEntry(entry: $entry, organizationId: $organizationId) {
      success
      entryWithSubentries {
        entry {
          mailmapEntryCreatedAt
          mailmapEntryEmail
          mailmapEntryName
          mailmapEntryUpdatedAt
        }
        subentries {
          mailmapSubentryCreatedAt
          mailmapSubentryEmail
          mailmapSubentryName
          mailmapSubentryUpdatedAt
        }
      }
    }
  }
`);

const CREATE_MAILMAP_SUBENTRY_MUTATION = graphql(`
  mutation createMailmapSubentry(
    $entryEmail: String!
    $organizationId: String!
    $subentry: MailmapSubentryInput!
  ) {
    createMailmapSubentry(
      entryEmail: $entryEmail
      organizationId: $organizationId
      subentry: $subentry
    ) {
      success
    }
  }
`);

const DELETE_MAILMAP_ENTRY_MUTATION = graphql(`
  mutation deleteMailmapEntry($entryEmail: String!, $organizationId: String!) {
    deleteMailmapEntry(
      entryEmail: $entryEmail
      organizationId: $organizationId
    ) {
      success
    }
  }
`);

const DELETE_MAILMAP_SUBENTRY_MUTATION = graphql(`
  mutation deleteMailmapSubentry(
    $entryEmail: String!
    $organizationId: String!
    $subentryEmail: String!
    $subentryName: String!
  ) {
    deleteMailmapSubentry(
      entryEmail: $entryEmail
      organizationId: $organizationId
      subentryEmail: $subentryEmail
      subentryName: $subentryName
    ) {
      success
    }
  }
`);

const DELETE_MAILMAP_ENTRY_WITH_SUBENTRIES_MUTATION = graphql(`
  mutation deleteMailmapEntryWithSubentries(
    $entryEmail: String!
    $organizationId: String!
  ) {
    deleteMailmapEntryWithSubentries(
      entryEmail: $entryEmail
      organizationId: $organizationId
    ) {
      success
    }
  }
`);

const GET_MAILMAP_ENTRIES_WITH_SUBENTRIES_QUERY = graphql(`
  query mailmapEntriesWithSubentries($organizationId: String!) {
    mailmapEntriesWithSubentries(organizationId: $organizationId) {
      entry {
        mailmapEntryCreatedAt
        mailmapEntryEmail
        mailmapEntryName
        mailmapEntryUpdatedAt
      }
      subentries {
        mailmapSubentryCreatedAt
        mailmapSubentryEmail
        mailmapSubentryName
        mailmapSubentryUpdatedAt
      }
    }
  }
`);

const GET_MAILMAP_RECORDS_QUERY = graphql(`
  query mailmapRecords($organizationId: String!) {
    mailmapRecords(organizationId: $organizationId) {
      mailmapEntryCreatedAt
      mailmapEntryEmail
      mailmapEntryName
      mailmapEntryUpdatedAt
      mailmapSubentryCreatedAt
      mailmapSubentryEmail
      mailmapSubentryName
      mailmapSubentryUpdatedAt
    }
  }
`);

const GET_MAILMAP_ENTRY_SUBENTRIES_QUERY = graphql(`
  query mailmapEntrySubentries($entryEmail: String!, $organizationId: String!) {
    mailmapEntrySubentries(
      entryEmail: $entryEmail
      organizationId: $organizationId
    ) {
      mailmapSubentryCreatedAt
      mailmapSubentryEmail
      mailmapSubentryName
      mailmapSubentryUpdatedAt
    }
  }
`);

const UPDATE_MAILMAP_ENTRY_MUTATION = graphql(`
  mutation updateMailmapEntry(
    $entry: MailmapEntryInput!
    $entryEmail: String!
    $organizationId: String!
  ) {
    updateMailmapEntry(
      entry: $entry
      entryEmail: $entryEmail
      organizationId: $organizationId
    ) {
      success
    }
  }
`);

const UPDATE_MAILMAP_SUBENTRY_MUTATION = graphql(`
  mutation updateMailmapSubentry(
    $entryEmail: String!
    $organizationId: String!
    $subentry: MailmapSubentryInput!
    $subentryEmail: String!
    $subentryName: String!
  ) {
    updateMailmapSubentry(
      entryEmail: $entryEmail
      organizationId: $organizationId
      subentry: $subentry
      subentryEmail: $subentryEmail
      subentryName: $subentryName
    ) {
      success
    }
  }
`);

const SET_MAILMAP_SUBENTRY_AS_MAILMAP_ENTRY_MUTATION = graphql(`
  mutation setMailmapSubentryAsMailmapEntry(
    $entryEmail: String!
    $organizationId: String!
    $subentryEmail: String!
    $subentryName: String!
  ) {
    setMailmapSubentryAsMailmapEntry(
      entryEmail: $entryEmail
      organizationId: $organizationId
      subentryEmail: $subentryEmail
      subentryName: $subentryName
    ) {
      success
      entry {
        mailmapEntryCreatedAt
        mailmapEntryEmail
        mailmapEntryName
        mailmapEntryUpdatedAt
      }
    }
  }
`);

const SET_MAILMAP_ENTRY_AS_MAILMAP_SUBENTRY_MUTATION = graphql(`
  mutation setMailmapEntryAsMailmapSubentry(
    $entryEmail: String!
    $organizationId: String!
    $targetEntryEmail: String!
  ) {
    setMailmapEntryAsMailmapSubentry(
      entryEmail: $entryEmail
      organizationId: $organizationId
      targetEntryEmail: $targetEntryEmail
    ) {
      success
    }
  }
`);

const IMPORT_MAILMAP_MUTATION = graphql(`
  mutation ImportMailmap($file: Upload!, $organizationId: String!) {
    importMailmap(file: $file, organizationId: $organizationId) {
      exceptionsMessages
      success
      successCount
    }
  }
`);

const MOVE_MAILMAP_ENTRIES_WITH_SUBENTRIES_MUTATION = graphql(`
  mutation MoveMailmapEntryWithSubentries(
    $entryEmail: String!
    $newOrganizationId: String!
    $organizationId: String!
  ) {
    moveMailmapEntryWithSubentries(
      entryEmail: $entryEmail
      newOrganizationId: $newOrganizationId
      organizationId: $organizationId
    ) {
      success
    }
  }
`);

const CONVERT_MAILMAP_SUBENTRIES_TO_NEW_ENTRY_MUTATION = graphql(`
  mutation ConvertMailmapSubentriesToNewEntry(
    $entryEmail: String!
    $mainSubentry: MailmapSubentryInput!
    $organizationId: String!
    $otherSubentries: [MailmapSubentryInput!]!
  ) {
    convertMailmapSubentriesToNewEntry(
      entryEmail: $entryEmail
      mainSubentry: $mainSubentry
      organizationId: $organizationId
      otherSubentries: $otherSubentries
    ) {
      success
    }
  }
`);

export {
  CREATE_MAILMAP_ENTRY_MUTATION,
  CREATE_MAILMAP_SUBENTRY_MUTATION,
  DELETE_MAILMAP_ENTRY_MUTATION,
  DELETE_MAILMAP_SUBENTRY_MUTATION,
  DELETE_MAILMAP_ENTRY_WITH_SUBENTRIES_MUTATION,
  GET_MAILMAP_ENTRIES_WITH_SUBENTRIES_QUERY,
  GET_MAILMAP_RECORDS_QUERY,
  GET_MAILMAP_ENTRY_SUBENTRIES_QUERY,
  UPDATE_MAILMAP_ENTRY_MUTATION,
  UPDATE_MAILMAP_SUBENTRY_MUTATION,
  SET_MAILMAP_SUBENTRY_AS_MAILMAP_ENTRY_MUTATION,
  SET_MAILMAP_ENTRY_AS_MAILMAP_SUBENTRY_MUTATION,
  IMPORT_MAILMAP_MUTATION,
  MOVE_MAILMAP_ENTRIES_WITH_SUBENTRIES_MUTATION,
  CONVERT_MAILMAP_SUBENTRIES_TO_NEW_ENTRY_MUTATION,
};
