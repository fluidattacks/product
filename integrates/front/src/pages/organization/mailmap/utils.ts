import type { IMailmapRecord } from "features/mailmap/types";

function getConflicts(message: string): string {
  const regex = /\[(?:.*?)\]/u;
  const match = regex.exec(message);

  return match ? match[0].trim() : "No conflicting mappings found.";
}

/*
 * We may have two mailmap records with the same entry name and email
 * This function is to filter them out
 */
function removeDuplicateEntryRecords(
  records: IMailmapRecord[],
): IMailmapRecord[] {
  const seen = new Map<string, IMailmapRecord>();

  records.forEach((record): void => {
    const key = `${record.mailmapEntryName}:${record.mailmapEntryEmail}`;
    if (!seen.has(key)) {
      seen.set(key, record);
    }
  });

  return Array.from(seen.values());
}

const getDomain = (email: string): string => {
  return email.split("@")[1];
};

const allDomainsAreEqual = (
  entryEmail: string,
  records: IMailmapRecord[],
): boolean => {
  const filteredRecords = records.filter(
    (record): boolean => record.mailmapEntryEmail === entryEmail,
  );

  if (filteredRecords.length === 0) {
    return false;
  }

  const entryDomain = getDomain(entryEmail);
  const subentryDomains = filteredRecords.map((record): string =>
    getDomain(record.mailmapSubentryEmail),
  );

  return subentryDomains.every((domain): boolean => domain === entryDomain);
};

export {
  getConflicts,
  removeDuplicateEntryRecords,
  getDomain,
  allDomainsAreEqual,
};
