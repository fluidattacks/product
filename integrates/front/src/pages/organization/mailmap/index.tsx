/* eslint-disable max-lines */
import { useMutation, useQuery } from "@apollo/client";
import { useAbility } from "@casl/react";
import {
  Button,
  Container,
  Divider,
  Icon,
  ListItem,
  ListItemsWrapper,
  useModal,
} from "@fluidattacks/design";
import type { ColumnDef, Row } from "@tanstack/react-table";
import type { GraphQLError } from "graphql";
import { useCallback, useMemo, useRef, useState } from "react";
import * as React from "react";
import { useTranslation } from "react-i18next";
import { useTheme } from "styled-components";

import {
  CREATE_MAILMAP_ENTRY_MUTATION,
  DELETE_MAILMAP_ENTRY_WITH_SUBENTRIES_MUTATION,
  GET_MAILMAP_RECORDS_QUERY,
  IMPORT_MAILMAP_MUTATION,
  MOVE_MAILMAP_ENTRIES_WITH_SUBENTRIES_MUTATION,
  SET_MAILMAP_ENTRY_AS_MAILMAP_SUBENTRY_MUTATION,
  UPDATE_MAILMAP_ENTRY_MUTATION,
} from "./queries";
import type { IMailmapProps } from "./types";
import {
  allDomainsAreEqual,
  getConflicts,
  removeDuplicateEntryRecords,
} from "./utils";

import { Authorize } from "components/@core/authorize";
import { ChildrenContainer } from "components/dropdown/styles";
import { SectionHeader } from "components/section-header";
import { Table } from "components/table";
import { authzPermissionsContext } from "context/authz/config";
import { CreateMailmapEntryModal } from "features/mailmap/create-modal";
import { DeleteMailmapEntryModal } from "features/mailmap/delete-modal";
import { MailmapFilters } from "features/mailmap/filters";
import { formatMailmapEntryEmail } from "features/mailmap/formatted-entry-email";
import { ImportMailmapModal } from "features/mailmap/import-mailmap-modal";
import { MoveMailmapEntryWithSubentriesModal } from "features/mailmap/move-entry-with-subentries-modal";
import { SetMailmapEntryAsMailmapSubentryModal } from "features/mailmap/set-entry-as-subentry-modal";
import { renderMailmapSubentries } from "features/mailmap/subentries";
import type {
  ICreateMailmapEntryFormValues,
  IDeleteMailmapEntryFormValues,
  IImportMailmapFormValues,
  IMailmapEntry,
  IMailmapRecord,
  IMoveMailmapEntryWithSubentriesFormValues,
  ISetMailmapEntryAsMailmapSubentryFormValues,
  IUpdateMailmapEntryFormValues,
} from "features/mailmap/types";
import { UpdateMailmapEntryModal } from "features/mailmap/update-modal";
import { useClickOutside, useTable } from "hooks";
import { useAudit } from "hooks/use-audit";
import { sleep } from "utils/helpers";
import { Logger } from "utils/logger";
import { msgError, msgSuccess } from "utils/notifications";

const Mailmap: React.FC<IMailmapProps> = ({
  organizationId,
}: IMailmapProps): JSX.Element => {
  const theme = useTheme();
  const { t } = useTranslation();

  const permissions = useAbility(authzPermissionsContext);
  // This particular mutation below was chosen arbitrarily
  const mutation = "integrates_api_mutations_create_mailmap_entry_mutate";
  const canMutate = permissions.can(mutation);

  const [filteredDataset, setFilteredDataset] = useState<IMailmapRecord[]>([]);

  const createEntryModal = useModal("create-entry-modal");
  const onCloseCreateEntryModal = createEntryModal.close;
  const onOpenCreateEntryModal = createEntryModal.open;

  const deleteEntryModal = useModal("delete-entry-modal");
  const onCloseDeleteEntryModal = deleteEntryModal.close;
  const onOpenDeleteEntryModal = deleteEntryModal.open;

  const updateEntryModal = useModal("update-entry-modal");
  const onCloseUpdateEntryModal = updateEntryModal.close;
  const onOpenUpdateEntryModal = updateEntryModal.open;

  const setEntryAsSubentryModal = useModal("set-entry-as-subentry-modal");
  const onCloseSetEntryAsSubentryModal = setEntryAsSubentryModal.close;
  const onOpenSetEntryAsSubentryModal = setEntryAsSubentryModal.open;

  const moveEntryWithSubentriesModal = useModal(
    "move-entry-with-subentries-modal",
  );
  const onCloseMoveEntryWithSubentriesModal =
    moveEntryWithSubentriesModal.close;
  const onOpenMoveEntryWithSubentriesModal = moveEntryWithSubentriesModal.open;

  const importMailmapModal = useModal("import-mailmap-modal");
  const onCloseImportMailmapModal = importMailmapModal.close;
  const onOpenImportMailmapModal = importMailmapModal.open;

  const [selectedEntries, setSelectedEntries] = useState<IMailmapEntry[]>([]);

  const [isEntryConfigMenuVisible, setIsEntryConfigMenuVisible] =
    useState<boolean>(false);

  const tableRef = useTable("tblMailmap");

  /*
   * The purpose of using mailmap records instead of mailmap entries with
   * subentries is because it's easier to apply filters to mailmap records.
   * We get records with duplicate entry name and entry email, and that0s why
   * we need `removeDuplicateEntryRecords` function later.
   */
  const { addAuditEvent } = useAudit();
  const { data, refetch } = useQuery(GET_MAILMAP_RECORDS_QUERY, {
    fetchPolicy: "cache-first",
    onCompleted: (): void => {
      addAuditEvent("Organization.Mailmap", organizationId);
    },
    onError: ({ graphQLErrors }): void => {
      msgError(t("mailmap.errorGetMailmapEntries"), t("mailmap.errorTitle"));
      graphQLErrors.forEach((error: GraphQLError): void => {
        Logger.error("Error loading mailmap records:", error);
      });
    },
    variables: {
      organizationId,
    },
  });

  const records = useMemo((): IMailmapRecord[] => {
    return (
      data?.mailmapRecords.map(
        (record: IMailmapRecord): IMailmapRecord => ({
          mailmapEntryEmail: record.mailmapEntryEmail,
          mailmapEntryName: record.mailmapEntryName,
          mailmapSubentryEmail: record.mailmapSubentryEmail,
          mailmapSubentryName: record.mailmapSubentryName,
        }),
      ) ?? []
    );
  }, [data]);

  const columns = useMemo(
    (): ColumnDef<IMailmapEntry>[] => [
      {
        accessorKey: "mailmapEntryName",
        header: t("mailmap.entryName"),
      },
      {
        accessorKey: "mailmapEntryEmail",
        cell: (cell): JSX.Element => {
          const entryEmail = cell.getValue() as string;
          const domainsCheck = allDomainsAreEqual(entryEmail, records);

          return formatMailmapEntryEmail({ domainsCheck, entryEmail });
        },
        header: t("mailmap.entryEmail"),
      },
    ],
    [t, records],
  );

  const [createMailmapEntryMutation] = useMutation(
    CREATE_MAILMAP_ENTRY_MUTATION,
    {
      onCompleted: (): void => {
        msgSuccess(t("mailmap.successCreateEntry"), t("mailmap.successTitle"));
      },
      onError: ({ graphQLErrors }): void => {
        graphQLErrors.forEach((error: GraphQLError): void => {
          if (
            error.message.startsWith(
              "Exception - Mailmap entry already exists:",
            )
          ) {
            msgError(
              t("mailmap.errorMailmapEntryAlreadyExists"),
              t("mailmap.errorTitle"),
            );
            Logger.error("Exception - Mailmap entry already exists:", error);
          } else {
            msgError(
              t("mailmap.errorCreateMailmapEntry"),
              t("mailmap.errorTitle"),
            );
            Logger.error("Error creating mailmap entry:", error);
          }
        });
      },
      refetchQueries: [GET_MAILMAP_RECORDS_QUERY],
    },
  );

  const [deleteMailmapEntryWithSubentriesMutation] = useMutation(
    DELETE_MAILMAP_ENTRY_WITH_SUBENTRIES_MUTATION,
    {
      onCompleted: (): void => {
        msgSuccess(t("mailmap.successDeleteEntry"), t("mailmap.successTitle"));
      },
      onError: ({ graphQLErrors }): void => {
        graphQLErrors.forEach((error: GraphQLError): void => {
          if (
            error.message.startsWith("Exception - Mailmap entry not found:")
          ) {
            msgError(
              t("mailmap.errorDeleteMailmapEntryNotFound"),
              t("mailmap.errorTitle"),
            );
            Logger.error("Exception - Mailmap entry not found:", error);
          } else {
            msgError(
              t("mailmap.errorDeleteMailmapEntry"),
              t("mailmap.errorTitle"),
            );
            Logger.error("Error deleting mailmap entry:", error);
          }
        });
      },
      refetchQueries: [GET_MAILMAP_RECORDS_QUERY],
    },
  );

  const [updateMailmapEntryMutation] = useMutation(
    UPDATE_MAILMAP_ENTRY_MUTATION,
    {
      onCompleted: (): void => {
        msgSuccess(t("mailmap.successUpdateEntry"), t("mailmap.successTitle"));
      },
      onError: ({ graphQLErrors }): void => {
        graphQLErrors.forEach((error: GraphQLError): void => {
          if (
            error.message.startsWith("Exception - Mailmap entry not found:")
          ) {
            msgError(
              t("mailmap.errorUpdateMailmapEntryNotFound"),
              t("mailmap.errorTitle"),
            );
            Logger.error("Exception - Mailmap entry not found:", error);
          } else {
            msgError(
              t("mailmap.errorUpdateMailmapEntry"),
              t("mailmap.errorTitle"),
            );
            Logger.error("Error updating mailmap entry:", error);
          }
        });
      },
      refetchQueries: [GET_MAILMAP_RECORDS_QUERY],
    },
  );

  const [setMailmapEntryAsMailmapSubentryMutation] = useMutation(
    SET_MAILMAP_ENTRY_AS_MAILMAP_SUBENTRY_MUTATION,
    {
      onCompleted: (): void => {
        msgSuccess(
          t("mailmap.successSetEntryAsSubentry"),
          t("mailmap.successTitle"),
        );
      },
      onError: ({ graphQLErrors }): void => {
        graphQLErrors.forEach((error: GraphQLError): void => {
          if (
            error.message.startsWith("Exception - Mailmap entry not found:")
          ) {
            msgError(
              t("mailmap.errorTargetMailmapEntryNotFound"),
              t("mailmap.errorTitle"),
            );
            Logger.error("Exception - Mailmap entry not found:", error);
          } else {
            msgError(
              t("mailmap.errorSetMailmapEntryAsMailmapSubentry"),
              t("mailmap.errorTitle"),
            );
            Logger.error(
              "Error setting mailmap entry as mailmap subentry",
              error,
            );
          }
        });
      },
      refetchQueries: [GET_MAILMAP_RECORDS_QUERY],
    },
  );

  const [moveMailmapEntryWithSubentriesMutation] = useMutation(
    MOVE_MAILMAP_ENTRIES_WITH_SUBENTRIES_MUTATION,
    {
      onCompleted: (): void => {
        msgSuccess(
          t("mailmap.successMoveEntryWithSubentries"),
          t("mailmap.successTitle"),
        );
      },
      onError: ({ graphQLErrors }): void => {
        graphQLErrors.forEach((error: GraphQLError): void => {
          Logger.error("Error moving mailmap entry with subentries", error);
        });
      },
      refetchQueries: [GET_MAILMAP_RECORDS_QUERY],
    },
  );

  const [importMailmapMutation, { loading: importMailmapSubmitting }] =
    useMutation(IMPORT_MAILMAP_MUTATION, {
      onCompleted: (importMailmapResult): void => {
        const { successCount } = importMailmapResult.importMailmap;
        if (successCount === 0) {
          msgError(
            t("mailmap.errorImportExistentMailmap"),
            t("mailmap.errorTitle"),
          );

          return;
        }
        msgSuccess(
          `${t("mailmap.successImportMailmap")}\n${t("mailmap.successfullyImportedLines")}: ${successCount}`,
          t("mailmap.successTitle"),
        );
        const messages = importMailmapResult.importMailmap.exceptionsMessages;
        if (messages.length > 0) {
          Logger.warning("Mailmap import exceptions:", messages);
        }
      },
      onError: ({ graphQLErrors }): void => {
        graphQLErrors.forEach((error: GraphQLError): void => {
          if (
            error.message.startsWith(
              "Exception - Conflicting mappings detected in mailmap file",
            )
          ) {
            const conflicts = getConflicts(error.message);
            msgError(
              `${t("mailmap.errorConflictingMailmapMappingError")}: ${conflicts}`,
              t("mailmap.errorTitle"),
            );
            Logger.error(
              "Exception - Conflicting mappings detected in mailmap file",
              error,
            );
          } else {
            msgError(t("mailmap.errorImportMailmap"), t("mailmap.errorTitle"));
            Logger.error("Error importing mailmap file:", error);
          }
        });
      },
      onQueryUpdated: async (observableQuery): Promise<void> => {
        const SLEEP_DURATION = 1000;
        await sleep(SLEEP_DURATION);
        await observableQuery.refetch();
      },
      refetchQueries: [GET_MAILMAP_RECORDS_QUERY],
    });

  const createMailmapEntry = useCallback(
    async (values: ICreateMailmapEntryFormValues): Promise<void> => {
      const { entryEmail, entryName } = values;
      await createMailmapEntryMutation({
        variables: {
          entry: {
            mailmapEntryEmail: entryEmail,
            mailmapEntryName: entryName,
          },
          organizationId,
        },
      });
      onCloseCreateEntryModal();
    },
    [createMailmapEntryMutation, onCloseCreateEntryModal, organizationId],
  );

  const deleteMailmapEntry = useCallback(
    async (values: IDeleteMailmapEntryFormValues): Promise<void> => {
      const { entryEmail } = values;
      await deleteMailmapEntryWithSubentriesMutation({
        variables: {
          entryEmail,
          organizationId,
        },
      });
      onCloseDeleteEntryModal();
      setSelectedEntries([]);
    },
    [
      deleteMailmapEntryWithSubentriesMutation,
      onCloseDeleteEntryModal,
      organizationId,
    ],
  );

  const updateMailmapEntry = useCallback(
    async (values: IUpdateMailmapEntryFormValues): Promise<void> => {
      const { oldEntryEmail, newEntryEmail, newEntryName } = values;
      await updateMailmapEntryMutation({
        variables: {
          entry: {
            mailmapEntryEmail: newEntryEmail,
            mailmapEntryName: newEntryName,
          },
          entryEmail: oldEntryEmail,
          organizationId,
        },
      });
      onCloseUpdateEntryModal();
      setSelectedEntries([]);
    },
    [updateMailmapEntryMutation, onCloseUpdateEntryModal, organizationId],
  );

  const setMailmapEntryAsMailmapSubentry = useCallback(
    async (
      values: ISetMailmapEntryAsMailmapSubentryFormValues,
    ): Promise<void> => {
      const { entryEmail, targetEntryEmail } = values;
      await setMailmapEntryAsMailmapSubentryMutation({
        variables: {
          entryEmail,
          organizationId,
          targetEntryEmail,
        },
      });
      onCloseSetEntryAsSubentryModal();
      setSelectedEntries([]);
    },
    [
      setMailmapEntryAsMailmapSubentryMutation,
      onCloseSetEntryAsSubentryModal,
      organizationId,
    ],
  );

  const moveMailmapEntryWithSubentries = useCallback(
    async (
      values: IMoveMailmapEntryWithSubentriesFormValues,
    ): Promise<void> => {
      const { entryEmail, newOrganizationId } = values;
      await moveMailmapEntryWithSubentriesMutation({
        variables: {
          entryEmail,
          newOrganizationId,
          organizationId,
        },
      });
      onCloseMoveEntryWithSubentriesModal();
      setSelectedEntries([]);
    },
    [
      moveMailmapEntryWithSubentriesMutation,
      onCloseMoveEntryWithSubentriesModal,
      organizationId,
    ],
  );

  const importMailmap = useCallback(
    async (values: IImportMailmapFormValues): Promise<void> => {
      await importMailmapMutation({
        variables: {
          file: values.file[0],
          organizationId,
        },
      });
      onCloseImportMailmapModal();
    },
    [importMailmapMutation, onCloseImportMailmapModal, organizationId],
  );

  const entriesEmails = useMemo(
    (): string[] =>
      records.map((record: IMailmapRecord): string => record.mailmapEntryEmail),
    [records],
  );

  const rowExpansion = useCallback(
    (row: Row<IMailmapEntry>): JSX.Element => {
      const entryEmail = row.original.mailmapEntryEmail;

      return renderMailmapSubentries({
        entriesEmails,
        entryEmail,
        organizationId,
        refetchEntries: refetch,
      });
    },
    [refetch, organizationId, entriesEmails],
  );

  const getSelectedEntryEmail = (): string =>
    selectedEntries.length === 1 ? selectedEntries[0].mailmapEntryEmail : "";

  const getSelectedEntryName = (): string =>
    selectedEntries.length === 1 ? selectedEntries[0].mailmapEntryName : "";

  // Components Declaration

  const modals = (
    <React.Fragment>
      <CreateMailmapEntryModal
        modalRef={createEntryModal}
        onClose={onCloseCreateEntryModal}
        onSubmit={createMailmapEntry}
      />
      <DeleteMailmapEntryModal
        entryEmail={getSelectedEntryEmail()}
        modalRef={deleteEntryModal}
        onClose={onCloseDeleteEntryModal}
        onSubmit={deleteMailmapEntry}
      />
      <UpdateMailmapEntryModal
        entryEmail={getSelectedEntryEmail()}
        entryName={getSelectedEntryName()}
        modalRef={updateEntryModal}
        onClose={onCloseUpdateEntryModal}
        onSubmit={updateMailmapEntry}
      />
      <SetMailmapEntryAsMailmapSubentryModal
        entryEmail={getSelectedEntryEmail()}
        modalRef={setEntryAsSubentryModal}
        onClose={onCloseSetEntryAsSubentryModal}
        onSubmit={setMailmapEntryAsMailmapSubentry}
      />
      <MoveMailmapEntryWithSubentriesModal
        entryEmail={getSelectedEntryEmail()}
        modalRef={moveEntryWithSubentriesModal}
        onClose={onCloseMoveEntryWithSubentriesModal}
        onSubmit={moveMailmapEntryWithSubentries}
      />
      <ImportMailmapModal
        modalRef={importMailmapModal}
        onClose={onCloseImportMailmapModal}
        onSubmit={importMailmap}
        submitting={importMailmapSubmitting}
      />
    </React.Fragment>
  );

  const header = (
    <SectionHeader header={t("organization.tabs.mailmap.header")} />
  );

  const toggleEntryConfigMenu = useCallback((): void => {
    setIsEntryConfigMenuVisible((previous): boolean => !previous);
  }, [setIsEntryConfigMenuVisible]);

  const entryConfigMenu = ((): JSX.Element | undefined => {
    if (!isEntryConfigMenuVisible) {
      return undefined;
    }

    return (
      <ListItemsWrapper>
        <ListItem
          icon={"pencil"}
          iconType={"fa-light"}
          onClick={onOpenUpdateEntryModal}
        >
          {t("mailmap.updateEntry")}
        </ListItem>

        <Divider />
        <ListItem
          icon={"money-check-pen"}
          iconType={"fa-light"}
          onClick={onOpenSetEntryAsSubentryModal}
        >
          {t("mailmap.setEntryAsSubentry")}
        </ListItem>
        <Divider />
        <ListItem
          icon={"trash"}
          iconType={"fa-light"}
          onClick={onOpenDeleteEntryModal}
        >
          {t("mailmap.deleteEntry")}
        </ListItem>
        <Divider />
        <ListItem
          icon={"share-from-square"}
          iconType={"fa-light"}
          onClick={onOpenMoveEntryWithSubentriesModal}
        >
          {t("mailmap.moveEntryWithSubentries")}{" "}
        </ListItem>
      </ListItemsWrapper>
    );
  })();

  const entryConfigButtonRef = useRef<HTMLButtonElement>(null);

  const entryConfigButton = (
    <Button
      disabled={selectedEntries.length !== 1}
      onClick={toggleEntryConfigMenu}
      ref={entryConfigButtonRef}
      variant={"ghost"}
    >
      <Icon
        clickable={false}
        icon={"gear"}
        iconSize={"xs"}
        iconType={"fa-light"}
      />
      {t("mailmap.entryConfig")}
      <Container zIndex={2}>
        <ChildrenContainer>{entryConfigMenu}</ChildrenContainer>
      </Container>
    </Button>
  );

  const importMailmapButton = (
    <Button
      icon={"file-import"}
      onClick={onOpenImportMailmapModal}
      tooltip={t("mailmap.tooltipImportMailmapButton")}
      tooltipPlace={"top"}
      variant={"ghost"}
    >
      {t("group.scope.git.addGitRootFile.button")}
    </Button>
  );

  useClickOutside(entryConfigButtonRef.current, (): void => {
    setIsEntryConfigMenuVisible(false);
  });

  const buttons = (
    <React.Fragment>
      {importMailmapButton}
      {entryConfigButton}
      <Button icon={"plus"} onClick={onOpenCreateEntryModal}>
        {t("mailmap.createEntry")}
      </Button>
    </React.Fragment>
  );

  const filters = (
    <MailmapFilters dataset={records} setFilteredDataset={setFilteredDataset} />
  );

  const table = (
    <Container
      bgColor={theme.palette.gray[50]}
      height={"100hv"}
      pb={1.25}
      pl={1.25}
      pr={1.25}
      pt={1.25}
    >
      <Table
        columns={columns}
        data={removeDuplicateEntryRecords(filteredDataset)}
        expandedRow={rowExpansion}
        extraButtons={<Authorize can={mutation}>{buttons}</Authorize>}
        filters={filters}
        rowSelectionSetter={canMutate ? setSelectedEntries : undefined}
        rowSelectionState={canMutate ? selectedEntries : undefined}
        tableRef={tableRef}
      />
    </Container>
  );

  return (
    <React.Fragment>
      {modals}
      {header}
      {table}
    </React.Fragment>
  );
};

export { Mailmap };
