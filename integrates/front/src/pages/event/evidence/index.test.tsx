import { PureAbility } from "@casl/ability";
import { screen, waitFor } from "@testing-library/react";
import { userEvent } from "@testing-library/user-event";
import { HttpResponse, type StrictResponse, graphql } from "msw";
import { Route, Routes } from "react-router-dom";

import { UPDATE_EVIDENCE_MUTATION } from "./queries";

import { authzPermissionsContext } from "context/authz/config";
import type { UpdateEventEvidenceMutationMutation as UpdateEventEvidence } from "gql/graphql";
import { render } from "mocks";
import { LINK } from "mocks/constants";
import { EventEvidence } from "pages/event/evidence";

describe("eventEvidence", (): void => {
  const handlePreview: jest.Mock = jest.fn();
  // eslint-disable-next-line functional/immutable-data -- Mutation needed for the test
  window.URL.createObjectURL = handlePreview;

  it("should render empty UI", async (): Promise<void> => {
    expect.hasAssertions();

    const mockedPermissions = new PureAbility<string>([
      { action: "integrates_api_mutations_update_event_evidence_mutate" },
    ]);

    render(
      <authzPermissionsContext.Provider value={mockedPermissions}>
        <Routes>
          <Route
            element={<EventEvidence />}
            path={"/:groupName/events/:eventId/evidence"}
          />
        </Routes>
      </authzPermissionsContext.Provider>,
      {
        memoryRouter: {
          initialEntries: ["/TEST/events/413372600/evidence"],
        },
      },
    );
    await waitFor((): void => {
      expect(
        screen.queryByText("group.events.evidence.empty.title"),
      ).toBeInTheDocument();
    });

    expect(
      screen.queryByText("group.events.evidence.edit"),
    ).not.toBeInTheDocument();

    const newEvidenceBtn = screen.getByRole("button", {
      name: "group.events.evidence.empty.button",
    });

    expect(newEvidenceBtn).toBeInTheDocument();

    await userEvent.click(newEvidenceBtn);

    expect(
      screen.queryByText("group.events.evidence.edit"),
    ).toBeInTheDocument();

    jest.clearAllMocks();
  });

  it("should render image and file", async (): Promise<void> => {
    expect.hasAssertions();

    render(
      <Routes>
        <Route
          element={<EventEvidence />}
          path={"/:groupName/events/:eventId/evidence"}
        />
      </Routes>,
      {
        memoryRouter: {
          initialEntries: ["/TEST/events/413372602/evidence"],
        },
      },
    );
    await waitFor((): void => {
      expect(screen.queryByRole("img")).toBeInTheDocument();
    });

    expect(
      screen.getByText("searchFindings.tabEvidence.downloadFile"),
    ).toBeInTheDocument();

    jest.clearAllMocks();
  });

  it("should render image viewer", async (): Promise<void> => {
    expect.hasAssertions();

    render(
      <Routes>
        <Route
          element={<EventEvidence />}
          path={"/:groupName/events/:eventId/evidence"}
        />
      </Routes>,
      {
        memoryRouter: {
          initialEntries: ["/TEST/events/413372602/evidence"],
        },
      },
    );
    await waitFor((): void => {
      expect(screen.queryAllByRole("img")).toHaveLength(1);
    });

    expect(screen.queryAllByRole("span", { hidden: true })).toHaveLength(0);

    await userEvent.click(screen.getAllByRole("img")[0]);
    await userEvent.hover(
      screen.getByRole("dialog", {
        hidden: true,
        name: "ImageViewer",
      }),
    );

    const reactImageViewerButtons = 2;
    await waitFor((): void => {
      expect(screen.queryAllByRole("img", { hidden: true })).toHaveLength(
        reactImageViewerButtons,
      );
    });

    jest.clearAllMocks();
  });

  it("should open file link", async (): Promise<void> => {
    expect.hasAssertions();

    const onOpenLink: jest.Mock = jest
      .fn()
      .mockReturnValue({ opener: undefined });
    // eslint-disable-next-line functional/immutable-data -- Mutation needed for the test
    (
      window as typeof window & {
        open: (url: string) => { opener: undefined };
      }
    ).open = onOpenLink;
    render(
      <Routes>
        <Route
          element={<EventEvidence />}
          path={"/:groupName/events/:eventId/evidence"}
        />
      </Routes>,
      {
        memoryRouter: {
          initialEntries: ["/TEST/events/413372603/evidence"],
        },
      },
    );
    await waitFor((): void => {
      expect(
        screen.getAllByText("searchFindings.tabEvidence.downloadFile"),
      ).toHaveLength(1);
    });
    await userEvent.click(
      screen.getAllByText("searchFindings.tabEvidence.downloadFile")[0],
    );
    await waitFor((): void => {
      expect(onOpenLink).toHaveBeenCalledWith(
        "https://localhost:9000/some_file.pdf",
        undefined,
        "noopener,noreferrer,",
      );
    });
    jest.clearAllMocks();
  });

  it("should edit evidences", async (): Promise<void> => {
    expect.hasAssertions();

    // [object FormData]
    jest.spyOn(console, "warn").mockImplementation();
    const image1 = new File(
      ["okada-test-0123456789.png"],
      "okada-test-0123456789.png",
      { type: "image/png" },
    );
    const file = new File(
      ["okada-test-0987654321.txt"],
      "okada-test-0987654321.txt",
      { type: "text/plain" },
    );

    const mockedPermissions = new PureAbility<string>([
      { action: "integrates_api_mutations_update_event_evidence_mutate" },
    ]);
    render(
      <authzPermissionsContext.Provider value={mockedPermissions}>
        <Routes>
          <Route
            element={<EventEvidence />}
            path={
              "/orgs/:organizationName/groups/:groupName/events/:eventId/evidence"
            }
          />
        </Routes>
      </authzPermissionsContext.Provider>,
      {
        memoryRouter: {
          initialEntries: ["/orgs/okada/groups/TEST/events/413372602/evidence"],
        },
        mocks: [
          graphql
            .link(LINK)
            .mutation(
              UPDATE_EVIDENCE_MUTATION,
              (): StrictResponse<{ data: UpdateEventEvidence }> => {
                return HttpResponse.json({
                  data: {
                    updateEventEvidence: {
                      success: true,
                    },
                  },
                });
              },
            ),
        ],
      },
    );
    await waitFor((): void => {
      expect(
        screen.queryByText("group.events.evidence.edit"),
      ).not.toBeDisabled();
    });
    await userEvent.click(screen.getByText("group.events.evidence.edit"));
    await waitFor((): void => {
      expect(
        screen.getByRole("button", {
          name: /searchfindings\.tabevidence\.update/iu,
        }),
      ).toBeDisabled();
    });
    await userEvent.upload(screen.getByTestId("image1.file"), image1);
    await userEvent.upload(screen.getByTestId("file1.file"), file);
    await waitFor((): void => {
      expect(
        screen.getByRole("button", {
          name: /searchfindings\.tabevidence\.update/iu,
        }),
      ).not.toBeDisabled();
    });
    await userEvent.click(
      screen.getByRole("button", {
        name: /searchfindings\.tabevidence\.update/iu,
      }),
    );

    await waitFor((): void => {
      expect(screen.queryAllByRole("img")).toHaveLength(1);
    });
    jest.clearAllMocks();
  });
});
