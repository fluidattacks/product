import type { InferType, Schema } from "yup";
import { mixed, object } from "yup";

import type { IEventEvidenceAttr } from "./types";

import { translate } from "utils/translations/translate";

const MAX_FILE_SIZE = 20;

const eventEvidenceSchema = (
  organizationName: string,
  groupName: string,
  evidenceImages: Record<string, IEventEvidenceAttr>,
): InferType<Schema> => {
  const imageSchema = Object.keys(evidenceImages).reduce(
    (obj, key): Record<string, Record<string, Schema>> => {
      const fileSchema: Schema = mixed<FileList>()
        .isValidFileSize(undefined, MAX_FILE_SIZE)
        .isValidFileName(groupName, organizationName)
        .isValidFileType(
          key === "file1" ? ["pdf", "zip", "csv", "txt"] : ["png", "webm"],
          translate.t(
            `group.events.form.${key === "file1" ? "wrongFileType" : "wrongImageType"}`,
          ),
        );

      return { ...obj, [key]: object().shape({ file: fileSchema }) };
    },
    {},
  );

  return object().shape(imageSchema);
};

export { eventEvidenceSchema };
