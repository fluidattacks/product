import { screen, waitFor } from "@testing-library/react";
import { Route, Routes } from "react-router-dom";

import { render } from "mocks";
import { Event } from "pages/event";
import { msgError } from "utils/notifications";

jest.mock("utils/notifications", (): Record<string, unknown> => {
  const mockedNotifications: Record<string, () => Record<string, unknown>> =
    jest.requireActual("utils/notifications");
  jest.spyOn(mockedNotifications, "msgError").mockImplementation();

  return mockedNotifications;
});

describe("event", (): void => {
  it("should render a component", async (): Promise<void> => {
    expect.hasAssertions();

    jest.clearAllMocks();

    render(
      <Routes>
        <Route
          element={<Event />}
          path={"/orgs/:organizationName/groups/:groupName/events/:eventId/*"}
        />
      </Routes>,
      {
        memoryRouter: {
          initialEntries: [
            "/orgs/okada/groups/TEST/events/413372600/description",
          ],
        },
      },
    );

    await waitFor((): void => {
      expect(
        screen.queryByText("Test | group.events.type.other"),
      ).toBeInTheDocument();
    });
    await waitFor((): void => {
      expect(screen.queryByText("Something happened")).toBeInTheDocument();
    });
  });

  it("should render error in component", async (): Promise<void> => {
    expect.hasAssertions();

    jest.clearAllMocks();

    const { container } = render(
      <Routes>
        <Route
          element={<Event />}
          path={"/orgs/:organizationName/groups/:groupName/events/:eventId/*"}
        />
      </Routes>,
      {
        memoryRouter: {
          initialEntries: [
            "/orgs/okada/groups/TEST/events/413372601/description",
          ],
        },
      },
    );

    await waitFor((): void => {
      expect(msgError).toHaveBeenCalledWith("groupAlerts.errorTextsad");
    });
    await waitFor((): void => {
      expect(container.textContent).toBe("");
    });
  });

  it("should render header component", async (): Promise<void> => {
    expect.hasAssertions();

    jest.clearAllMocks();

    render(
      <Routes>
        <Route
          element={<Event />}
          path={"/orgs/:organizationName/groups/:groupName/events/:eventId/*"}
        />
      </Routes>,
      {
        memoryRouter: {
          initialEntries: [
            "/orgs/okada/groups/TEST/events/413372600/description",
          ],
        },
      },
    );

    await waitFor((): void => {
      expect(
        screen.queryByText("searchFindings.tabEvents...."),
      ).toBeInTheDocument();
    });

    await waitFor((): void => {
      expect(
        screen.queryByText("searchFindings.tabEvents.rootNickname"),
      ).toBeInTheDocument();
    });
  });

  it("should not display findings in tab", async (): Promise<void> => {
    expect.hasAssertions();

    jest.clearAllMocks();

    render(
      <Routes>
        <Route
          element={<Event />}
          path={"/orgs/:organizationName/groups/:groupName/events/:eventId/*"}
        />
      </Routes>,
      {
        memoryRouter: {
          initialEntries: [
            "/orgs/okada/groups/TEST/events/413372600/description",
          ],
        },
      },
    );

    await waitFor((): void => {
      expect(
        screen.queryByText("group.tabs.comments.findings"),
      ).not.toBeInTheDocument();
    });
  });
});
