import * as React from "react";
import { Navigate, Route, Routes } from "react-router-dom";

import { EventConsulting } from "./consulting";
import { EventDescription } from "./description";
import { EventEvidence } from "./evidence";
import { EventTemplate } from "./index.template";

import { useGroupUrl } from "pages/group/hooks";

const Event: React.FC = (): JSX.Element => {
  const { url } = useGroupUrl("/events/:eventId/*");

  return (
    <EventTemplate>
      <Routes>
        <Route element={<EventDescription />} path={`/description`} />
        <Route element={<EventEvidence />} path={`/evidence`} />
        <Route element={<EventConsulting />} path={`/consulting`} />
        <Route
          element={<Navigate to={`${url}/consulting`.replace("//", "/")} />}
          path={`/comments`}
        />
        <Route
          element={<Navigate to={`${url}/description`.replace("//", "/")} />}
          path={"*"}
        />
      </Routes>
    </EventTemplate>
  );
};

export { Event };
