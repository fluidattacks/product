import { useMutation } from "@apollo/client";
import mixpanel from "mixpanel-browser";
import { useCallback } from "react";
import * as React from "react";
import { useTranslation } from "react-i18next";
import { useParams } from "react-router-dom";

import { useConsultingQuery } from "./hooks";
import { ADD_EVENT_CONSULT, GET_EVENT_CONSULTING } from "./queries";

import { Comments } from "features/comments";
import type { ICommentStructure } from "features/comments/types";
import { Logger } from "utils/logger";
import { msgError } from "utils/notifications";

const EventConsulting: React.FC = (): JSX.Element => {
  const { eventId, groupName } = useParams() as {
    eventId: string;
    groupName: string;
  };
  const { t } = useTranslation();

  const { comments, loading } = useConsultingQuery(eventId, groupName);

  const [addComment] = useMutation(ADD_EVENT_CONSULT, {
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach(({ message }): void => {
        switch (message) {
          case "Exception - Invalid field length in form":
            msgError(t("validations.invalidFieldLength"));
            break;
          case "Exception - Comment parent is invalid":
            msgError(t("validations.invalidCommentParent", { count: 1 }));
            break;
          case "Exception - Invalid characters":
            msgError(t("validations.invalidChar"));
            break;
          default:
            msgError(t("groupAlerts.errorTextsad"));
            Logger.error("An error occurred posting event comment", message);
        }
      });
    },
    refetchQueries: [GET_EVENT_CONSULTING],
  });

  const handlePost = useCallback(
    async (comment: ICommentStructure): Promise<void> => {
      mixpanel.track("AddEventComment", { eventId });
      await addComment({
        variables: {
          content: comment.content,
          eventId,
          groupName,
          parentComment: comment.parentComment,
        },
      });
    },
    [addComment, eventId, groupName],
  );

  if (comments === undefined || loading) {
    return <div />;
  }

  return (
    <React.StrictMode>
      <Comments
        comments={comments}
        emptyComponentsProps={{
          confirmButton: {
            text: t("group.consulting.empty.button"),
          },
          description: t("group.events.consulting.empty.description"),
          title: t("group.events.consulting.empty.title"),
        }}
        onPostComment={handlePost}
      />
    </React.StrictMode>
  );
};

export { EventConsulting };
