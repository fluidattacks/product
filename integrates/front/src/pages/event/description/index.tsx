/* eslint-disable max-lines */
import { useMutation, useQuery } from "@apollo/client";
import type { ApolloError } from "@apollo/client";
import { useAbility } from "@casl/react";
import { Container, GridContainer, useModal } from "@fluidattacks/design";
import { Form, Formik } from "formik";
import _ from "lodash";
import { Fragment, useCallback, useState } from "react";
import * as React from "react";
import { useTranslation } from "react-i18next";
import { useParams } from "react-router-dom";
import type { Schema } from "yup";
import { object, string } from "yup";

import { ActionButtons } from "./action-buttons";
import { handleSolveEventError } from "./hooks";
import {
  GET_EVENT_DESCRIPTION,
  REJECT_EVENT_SOLUTION_MUTATION,
  SOLVE_EVENT_MUTATION,
  UPDATE_EVENT_MUTATION,
} from "./queries";
import { SolveEventModal } from "./solve-event-modal";
import type {
  IDescriptionFormValues,
  IEventDescriptionData,
  IRejectEventSolutionResultAttr,
  IUpdateEventAttr,
} from "./types";
import { UpdateSolvingReason } from "./update-solving-reason";

import { GET_EVENT_HEADER } from "../queries";
import { Editable, Input, Select } from "components/input";
import { Can } from "context/authz/can";
import { authzPermissionsContext } from "context/authz/config";
import { RemediationModal } from "features/remediation-modal";
import type { IRemediationValues } from "features/remediation-modal/types";
import { EventType, SolveEventReason } from "gql/graphql";
import { useAudit } from "hooks/use-audit";
import { formatDateTime } from "utils/date";
import { castEventType } from "utils/format-helpers";
import { Logger } from "utils/logger";
import { msgError, msgSuccess } from "utils/notifications";

const solutionReasonByEventType: Record<EventType, SolveEventReason[]> = {
  AUTHORIZATION_SPECIAL_ATTACK: [
    SolveEventReason.PermissionDenied,
    SolveEventReason.PermissionGranted,
  ],
  CLIENT_CANCELS_PROJECT_MILESTONE: [SolveEventReason.Other],
  CLIENT_EXPLICITLY_SUSPENDS_PROJECT: [
    SolveEventReason.IsOkToResume,
    SolveEventReason.AffectedResourceRemovedFromScope,
    SolveEventReason.Other,
  ],
  CLONING_ISSUES: [
    SolveEventReason.AffectedResourceRemovedFromScope,
    SolveEventReason.ClonedSuccessfully,
  ],
  CREDENTIAL_ISSUES: [
    SolveEventReason.AffectedResourceRemovedFromScope,
    SolveEventReason.CredentialsAreWorkingNow,
    SolveEventReason.NewCredentialsProvided,
  ],
  DATA_UPDATE_REQUIRED: [
    SolveEventReason.AffectedResourceRemovedFromScope,
    SolveEventReason.DataUpdated,
    SolveEventReason.Other,
  ],
  ENVIRONMENT_ISSUES: [
    SolveEventReason.AffectedResourceRemovedFromScope,
    SolveEventReason.EnvironmentIsWorkingNow,
    SolveEventReason.NewEnvironmentProvided,
  ],
  INSTALLER_ISSUES: [
    SolveEventReason.AffectedResourceRemovedFromScope,
    SolveEventReason.InstallerIsWorkingNow,
    SolveEventReason.Other,
  ],
  MISSING_SUPPLIES: [
    SolveEventReason.AffectedResourceRemovedFromScope,
    SolveEventReason.SuppliesWereGiven,
    SolveEventReason.Other,
  ],
  NETWORK_ACCESS_ISSUES: [
    SolveEventReason.PermissionGranted,
    SolveEventReason.AffectedResourceRemovedFromScope,
    SolveEventReason.Other,
  ],
  OTHER: [SolveEventReason.Other],
  REMOTE_ACCESS_ISSUES: [
    SolveEventReason.AccessGranted,
    SolveEventReason.AffectedResourceRemovedFromScope,
    SolveEventReason.Other,
  ],
  TOE_DIFFERS_APPROVED: [
    SolveEventReason.ToeChangeApproved,
    SolveEventReason.ToeWillRemainUnchanged,
  ],
  VPN_ISSUES: [
    SolveEventReason.AccessGranted,
    SolveEventReason.AffectedResourceRemovedFromScope,
    SolveEventReason.Other,
  ],
};

const EventDescription: React.FC = (): JSX.Element => {
  const { t } = useTranslation();
  const { eventId, groupName } = useParams() as {
    eventId: string;
    groupName: string;
  };
  const permissions = useAbility(authzPermissionsContext);
  const canUpdateEvent = permissions.can(
    "integrates_api_mutations_update_event_mutate",
  );

  const solvingReasons: Record<
    Exclude<
      SolveEventReason,
      SolveEventReason.MovedToAnotherGroup | SolveEventReason.ProblemSolved
    >,
    string
  > = {
    ACCESS_GRANTED: t(
      "searchFindings.tabSeverity.common.deactivation.reason.accessGranted",
    ),
    AFFECTED_RESOURCE_REMOVED_FROM_SCOPE: t(
      "searchFindings.tabSeverity.common.deactivation.reason.removedFromScope",
    ),
    CLONED_SUCCESSFULLY: t(
      "searchFindings.tabSeverity.common.deactivation.reason.clonedSuccessfully",
    ),
    CREDENTIALS_ARE_WORKING_NOW: t(
      "searchFindings.tabSeverity.common.deactivation.reason.credentialsAreWorkingNow",
    ),
    DATA_UPDATED: t(
      "searchFindings.tabSeverity.common.deactivation.reason.dataUpdated",
    ),
    ENVIRONMENT_IS_WORKING_NOW: t(
      "searchFindings.tabSeverity.common.deactivation.reason.environmentIsWorkingNow",
    ),
    INSTALLER_IS_WORKING_NOW: t(
      "searchFindings.tabSeverity.common.deactivation.reason.installerIsWorkingNow",
    ),
    IS_OK_TO_RESUME: t(
      "searchFindings.tabSeverity.common.deactivation.reason.isOkToResume",
    ),
    NEW_CREDENTIALS_PROVIDED: t(
      "searchFindings.tabSeverity.common.deactivation.reason.newCredentialsProvided",
    ),
    NEW_ENVIRONMENT_PROVIDED: t(
      "searchFindings.tabSeverity.common.deactivation.reason.newEnvironmentProvided",
    ),
    OTHER: t("searchFindings.tabSeverity.common.deactivation.reason.other"),
    PERMISSION_DENIED: t(
      "searchFindings.tabSeverity.common.deactivation.reason.permissionDenied",
    ),
    PERMISSION_GRANTED: t(
      "searchFindings.tabSeverity.common.deactivation.reason.permissionGranted",
    ),
    SUPPLIES_WERE_GIVEN: t(
      "searchFindings.tabSeverity.common.deactivation.reason.suppliesWereGiven",
    ),
    TOE_CHANGE_APPROVED: t(
      "searchFindings.tabSeverity.common.deactivation.reason.toeApproved",
    ),
    TOE_WILL_REMAIN_UNCHANGED: t(
      "searchFindings.tabSeverity.common.deactivation.reason.toeUnchanged",
    ),
  };
  const allSolvingReasons: Record<string, string> = {
    ...solvingReasons,
    PROBLEM_SOLVED: t(
      "searchFindings.tabSeverity.common.deactivation.reason.problemSolved",
    ),
  };

  // State management
  const remediationModal = useModal("remediation-modal");
  const [isSolvingModalOpen, setIsSolvingModalOpen] = useState(false);
  const [isEditing, setIsEditing] = useState(false);
  const [remediationValues, setRemediationValues] =
    useState<IRemediationValues>({
      treatmentJustification: "",
    });
  const openSolvingModal = useCallback((): void => {
    setIsSolvingModalOpen(true);
  }, []);
  const closeSolvingModal = useCallback((): void => {
    setIsSolvingModalOpen(false);
  }, []);
  const toggleEdit = useCallback((): void => {
    setIsEditing(!isEditing);
  }, [isEditing]);

  const handleErrors = useCallback(
    ({ graphQLErrors }: ApolloError): void => {
      graphQLErrors.forEach((error): void => {
        msgError(t("groupAlerts.errorTextsad"));
        Logger.warning("An error occurred loading event description", error);
      });
    },
    [t],
  );

  const { addAuditEvent } = useAudit();
  const { data, refetch } = useQuery(GET_EVENT_DESCRIPTION, {
    onCompleted: (): void => {
      addAuditEvent("Event.Description", eventId);
    },
    onError: handleErrors,
    variables: {
      canRetrieveHacker: permissions.can(
        "integrates_api_resolvers_event_hacker_resolve",
      ),
      eventId,
      groupName,
    },
  });

  const handleUpdateResult = (): void => {
    void refetch();
  };

  const [solveEvent, { loading: submitting }] = useMutation(
    SOLVE_EVENT_MUTATION,
    {
      onCompleted: handleUpdateResult,
      onError: ({ graphQLErrors }): void => {
        graphQLErrors.forEach((error): void => {
          handleSolveEventError(error);
        });
      },
      refetchQueries: [
        { query: GET_EVENT_HEADER, variables: { eventId, groupName } },
        {
          query: GET_EVENT_DESCRIPTION,
          variables: {
            canRetrieveHacker: permissions.can(
              "integrates_api_resolvers_event_hacker_resolve",
            ),
            eventId,
            groupName,
          },
        },
      ],
    },
  );

  const [rejectSolution] = useMutation(REJECT_EVENT_SOLUTION_MUTATION, {
    onCompleted: (mtResult: IRejectEventSolutionResultAttr): void => {
      if (mtResult.rejectEventSolution.success) {
        msgSuccess(
          t("group.events.description.alerts.rejectSolution.success"),
          t("groupAlerts.updatedTitle"),
        );
        remediationModal.close();
      }
    },
    onError: (error): void => {
      error.graphQLErrors.forEach(({ message }): void => {
        switch (message) {
          case "Exception - Event not found":
            msgError(
              t(
                `group.events.description.alerts.editSolvingReason.eventNotFound`,
              ),
            );
            break;
          case "Exception - The event verification has not been requested":
            msgError(
              t(
                "group.events.description.alerts.rejectSolution.nonRequestedVerification",
              ),
            );
            break;
          case "Exception - The event has already been closed":
            msgError(t("group.events.alreadyClosed"));
            break;
          default:
            msgError(t("groupAlerts.errorTextsad"));
            Logger.warning(
              "An error occurred rejecting the event solution",
              error,
            );
        }
      });
    },
    refetchQueries: [
      { query: GET_EVENT_HEADER, variables: { eventId, groupName } },
      {
        query: GET_EVENT_DESCRIPTION,
        variables: {
          canRetrieveHacker: permissions.can(
            "integrates_api_resolvers_event_hacker_resolve",
          ),
          eventId,
          groupName,
        },
      },
    ],
  });

  const [updateEvent] = useMutation(UPDATE_EVENT_MUTATION, {
    onCompleted: (mtResult: IUpdateEventAttr): void => {
      if (mtResult.updateEvent.success) {
        msgSuccess(
          t("group.events.description.alerts.editEvent.success"),
          t("groupAlerts.updatedTitle"),
        );
        setIsEditing(false);
      }
    },
    onError: (error): void => {
      error.graphQLErrors.forEach(({ message }): void => {
        switch (message) {
          case "Exception - Event not found":
            msgError(
              t(`group.events.description.alerts.editEvent.eventNotFound`),
            );
            break;
          case "Exception - The event has not been solved":
            msgError(
              t(`group.events.description.alerts.editEvent.nonSolvedEvent`),
            );
            break;
          default:
            msgError(t("groupAlerts.errorTextsad"));
            Logger.warning("An error occurred updating the event", error);
        }
      });
    },
    refetchQueries: [
      {
        query: GET_EVENT_DESCRIPTION,
        variables: {
          canRetrieveHacker: permissions.can(
            "integrates_api_resolvers_event_hacker_resolve",
          ),
          eventId,
          groupName,
        },
      },
    ],
  });

  const handleRejectSolution = useCallback(
    async (values: IRemediationValues): Promise<void> => {
      setRemediationValues(values);
      await rejectSolution({
        variables: {
          comments: values.treatmentJustification,
          eventId,
          groupName,
        },
      });
      remediationModal.close();
    },
    [rejectSolution, eventId, groupName, remediationModal],
  );

  const handleSubmit = useCallback(
    (values: Record<string, unknown>): void => {
      const otherReason = values.reason === "OTHER" ? values.other : undefined;
      void solveEvent({
        variables: {
          eventId,
          groupName,
          other: otherReason as string,
          reason: values.reason as SolveEventReason,
        },
      });
      closeSolvingModal();
    },
    [solveEvent, eventId, groupName, closeSolvingModal],
  );

  const handleDescriptionSubmit = useCallback(
    (values: IDescriptionFormValues): void => {
      const otherSolvingReason =
        values.solvingReason === "OTHER"
          ? values.otherSolvingReason
          : undefined;

      if (!_.isUndefined(data)) {
        void updateEvent({
          variables: {
            eventDescription: values.detail,
            eventId,
            eventType: values.eventType as EventType,
            groupName,
            otherSolvingReason,
            solvingReason: values.solvingReason as SolveEventReason,
          },
        });
      }
    },
    [data, eventId, groupName, updateEvent],
  );

  if (_.isUndefined(data) || _.isEmpty(data)) {
    return <div />;
  }

  const validationSchema = object().shape({
    eventType: string().required(t("validations.required")),
    otherSolvingReason: string().when(
      "solvingReason",
      ([reason]: string[]): Schema => {
        return reason === "OTHER"
          ? string().nullable().required(t("validations.required"))
          : string().nullable();
      },
    ),
    solvingReason: string().when("eventStatus", ([event]: string[]): Schema => {
      return event === "SOLVED"
        ? string().nullable().required(t("validations.required"))
        : string().nullable();
    }),
  });

  return (
    <Fragment>
      <RemediationModal
        isLoading={false}
        message={t(
          "group.events.description.rejectSolution.modal.observations",
        )}
        modalRef={remediationModal}
        onSubmit={handleRejectSolution}
        remediationType={"verifyEvent"}
        title={t("group.events.description.rejectSolution.modal.title")}
        values={remediationValues}
      />
      <SolveEventModal
        eventType={data.event.eventType}
        isOpen={isSolvingModalOpen}
        numberOfAffectedReattacks={data.event.affectedReattacks.length}
        onClose={closeSolvingModal}
        onSubmit={handleSubmit}
        solutionReasonByEventType={solutionReasonByEventType}
        solvingReasons={solvingReasons}
        submitting={submitting}
      />
      <Formik
        enableReinitialize={true}
        initialValues={data.event as IEventDescriptionData["event"]}
        name={"editEvent"}
        onSubmit={handleDescriptionSubmit}
        validationSchema={validationSchema}
      >
        {({ values, dirty, setFieldValue }): React.ReactNode => {
          if (
            !_.isNull(values.solvingReason) &&
            !_.isEmpty(values.solvingReason) &&
            !_.isEmpty(values.eventType) &&
            !solutionReasonByEventType[values.eventType as EventType].includes(
              values.solvingReason as SolveEventReason,
            )
          ) {
            void setFieldValue("solvingReason", "");
          }

          return (
            <Form id={"editEvent"}>
              <ActionButtons
                eventStatus={values.eventStatus}
                isDirtyForm={dirty}
                isEditing={isEditing}
                onEdit={toggleEdit}
                openRejectSolutionModal={remediationModal.open}
                openSolvingModal={openSolvingModal}
              />
              <GridContainer lg={2} md={2} sm={1} xl={2}>
                <Container display={"flex"} flexDirection={"column"} gap={1.25}>
                  {isEditing && canUpdateEvent ? (
                    <Select
                      items={[{ header: "", value: "" }].concat(
                        Object.values(EventType).map(
                          (value): { header: string; value: string } => ({
                            header: t(castEventType(value)),
                            value,
                          }),
                        ),
                      )}
                      label={t("searchFindings.tabEvents.type")}
                      name={"eventType"}
                    />
                  ) : undefined}
                  <Editable
                    currentValue={data.event.detail}
                    isEditing={Boolean(isEditing && canUpdateEvent)}
                    label={t("searchFindings.tabEvents.description")}
                  >
                    <Input
                      label={t("searchFindings.tabEvents.description")}
                      name={"detail"}
                    />
                  </Editable>
                  <Can do={"integrates_api_resolvers_event_hacker_resolve"}>
                    <Editable
                      currentValue={data.event.hacker}
                      isEditing={false}
                      label={t("searchFindings.tabEvents.hacker")}
                    >
                      <Input
                        label={t("searchFindings.tabEvents.hacker")}
                        name={"hacker"}
                      />
                    </Editable>
                  </Can>
                  {data.event.eventStatus === "SOLVED" ? (
                    <UpdateSolvingReason
                      allSolvingReasons={allSolvingReasons}
                      canUpdateEvent={canUpdateEvent}
                      data={data as IEventDescriptionData}
                      isEditing={isEditing}
                      solutionReasonByEventType={solutionReasonByEventType}
                      solvingReasons={solvingReasons}
                      values={values}
                    />
                  ) : undefined}
                </Container>
                <Container display={"flex"} flexDirection={"column"} gap={1.25}>
                  <Editable
                    currentValue={
                      data.event.root ? data.event.root.nickname : "-"
                    }
                    isEditing={false}
                    label={t("searchFindings.tabEvents.rootNickname")}
                  >
                    <Input
                      label={t("searchFindings.tabEvents.rootNickname")}
                      name={"rootNickname"}
                    />
                  </Editable>
                  <Editable
                    currentValue={
                      data.event.environment !== null && data.event.environment
                        ? data.event.environment
                        : "-"
                    }
                    isEditing={false}
                    label={t("searchFindings.tabEvents.environmentUrl")}
                  >
                    <Input
                      label={t("searchFindings.tabEvents.environmentUrl")}
                      name={"environmentUrl"}
                    />
                  </Editable>
                  <Editable
                    currentValue={
                      _.isEmpty(data.event.affectedReattacks)
                        ? "0"
                        : String(data.event.affectedReattacks.length)
                    }
                    isEditing={false}
                    label={t("searchFindings.tabEvents.affectedReattacks")}
                  >
                    <Input
                      label={t("searchFindings.tabEvents.affectedReattacks")}
                      name={"affectedReattacks"}
                    />
                  </Editable>
                  {data.event.eventStatus === "SOLVED" ? (
                    <Editable
                      currentValue={
                        _.isNil(data.event.closingDate)
                          ? "-"
                          : formatDateTime(data.event.closingDate)
                      }
                      isEditing={false}
                      label={t("searchFindings.tabEvents.dateClosed")}
                    >
                      <Input
                        label={t("searchFindings.tabEvents.dateClosed")}
                        name={"dateClosed"}
                      />
                    </Editable>
                  ) : undefined}
                </Container>
              </GridContainer>
            </Form>
          );
        }}
      </Formik>
    </Fragment>
  );
};

export { EventDescription, solutionReasonByEventType };
