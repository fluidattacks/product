import type { GraphQLError } from "graphql";

import { Logger } from "utils/logger";
import { msgError } from "utils/notifications";
import { translate } from "utils/translations/translate";

const handleSolveEventError = (error: GraphQLError): void => {
  switch (error.message) {
    case "Exception - The git repository is outdated":
      msgError(translate.t("groupAlerts.outdatedRepository"), "Oops!");
      break;
    case "Exception - The event has already been closed":
      msgError(translate.t("group.events.alreadyClosed"));
      break;
    case "Exception - The solving reason is not valid for the event type":
      msgError(
        translate.t(`group.events.description.alerts.solveEvent.invalidReason`),
      );
      break;
    default:
      msgError(translate.t("groupAlerts.errorTextsad"));
      Logger.warning("An error occurred updating event", error);
  }
};

export { handleSolveEventError };
