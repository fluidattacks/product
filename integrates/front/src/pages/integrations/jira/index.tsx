import * as React from "react";
import { useTranslation } from "react-i18next";

import { IntegrationCard } from "../integration-card";

const Jira: React.FC = (): React.JSX.Element => {
  const { t } = useTranslation();

  return (
    <IntegrationCard
      description={t("integrations.jira.description")}
      docsLink={
        "https://help.fluidattacks.com/portal/en/kb/articles/install-the-fluid-attacks-app-for-jira-cloud"
      }
      iconPath={"integrates/integrations/icon-jira"}
      name={"Jira Cloud"}
    />
  );
};

export { Jira };
