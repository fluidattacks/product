import { CloudImage, Modal, Span, Text } from "@fluidattacks/design";
import type { IUseModal } from "@fluidattacks/design";
import * as React from "react";
import { Trans, useTranslation } from "react-i18next";

interface IIntegrationOAuthConfirmProps {
  readonly groupName: string;
  readonly iconPath: `integrates/integrations/${string}`;
  readonly modalRef: IUseModal;
  readonly name: string;
  readonly onCancel: () => void;
  readonly onConfirm: () => void;
}

const IntegrationOAuthConfirm: React.FC<IIntegrationOAuthConfirmProps> = ({
  groupName,
  iconPath,
  modalRef,
  name,
  onCancel,
  onConfirm,
}): React.JSX.Element => {
  const { t } = useTranslation();

  return (
    <Modal
      cancelButton={{
        onClick: onCancel,
        text: t("integrations.oauthConfirm.cancel"),
      }}
      confirmButton={{
        onClick: onConfirm,
        text: t("integrations.oauthConfirm.confirm"),
      }}
      modalRef={modalRef}
      size={"md"}
      title={t("integrations.oauthConfirm.title", { name })}
    >
      <div style={{ marginLeft: "-25px", marginRight: "-25px" }}>
        <CloudImage publicId={iconPath} />
      </div>
      <Text mt={1.5} size={"sm"}>
        <Trans
          components={{
            bold: (
              <Span fontWeight={"bold"} size={"sm"}>
                {undefined}
              </Span>
            ),
          }}
          i18nKey={"integrations.oauthConfirm.prompt"}
          t={t}
          values={{ groupName, name }}
        />
      </Text>
    </Modal>
  );
};

export { IntegrationOAuthConfirm };
