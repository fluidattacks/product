import {
  Button,
  CloudImage,
  Container,
  Heading,
  IconButton,
  Link,
  Tag,
  Text,
} from "@fluidattacks/design";
import * as React from "react";
import { useTranslation } from "react-i18next";
import { useTheme } from "styled-components";

import { CardContainer } from "components/card-container";

interface IIntegrationCardProps {
  readonly description: string;
  readonly docsLink: string;
  readonly groupsSummary?: { connected: number; total: number };
  readonly iconPath: `integrates/integrations/${string}`;
  readonly name: string;
  readonly onClickConfig?: () => void;
}

const IntegrationCard: React.FC<IIntegrationCardProps> = ({
  description,
  docsLink,
  groupsSummary = { connected: 0, total: 0 },
  iconPath,
  name,
  onClickConfig,
}): React.JSX.Element => {
  const theme = useTheme();
  const { t } = useTranslation();

  return (
    <CardContainer
      display={"flex"}
      flexDirection={"column"}
      gap={0.5}
      width={"386.67px"}
    >
      <Container display={"flex"} justify={"space-between"}>
        <CloudImage height={"56px"} publicId={iconPath} width={"54px"} />
        {groupsSummary.connected > 0 ? (
          <Container alignItems={"flex-start"} display={"flex"}>
            <IconButton
              icon={"gear"}
              iconSize={"xxs"}
              iconType={"fa-light"}
              onClick={onClickConfig}
              variant={"ghost"}
            />
          </Container>
        ) : undefined}
      </Container>
      <Container display={"flex"} flexDirection={"column"} gap={0.5}>
        <Heading fontWeight={"bold"} size={"xs"}>
          {name}
        </Heading>
        <Text size={"sm"}>{description}</Text>
        {groupsSummary.connected > 0 ? (
          <Text color={theme.palette.gray[400]} fontWeight={"bold"} size={"sm"}>
            {t("integrations.card.summary", {
              connected: groupsSummary.connected,
              count: groupsSummary.total,
            })}
          </Text>
        ) : undefined}
      </Container>
      <div style={{ flexGrow: 1 }} />
      <Container
        alignItems={"center"}
        display={"flex"}
        justify={"space-between"}
        wrap={"wrap-reverse"}
      >
        <Link href={docsLink}>{t("integrations.card.docs")}</Link>
        {groupsSummary.connected > 0 ? (
          <Tag
            priority={"low"}
            tagLabel={t("integrations.card.active")}
            variant={"success"}
          />
        ) : undefined}
        {groupsSummary.connected === 0 && onClickConfig ? (
          <Button onClick={onClickConfig} variant={"secondary"}>
            {t("integrations.card.config")}
          </Button>
        ) : undefined}
      </Container>
    </CardContainer>
  );
};

export { IntegrationCard };
