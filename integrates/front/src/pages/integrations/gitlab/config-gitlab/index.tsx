import { useMutation } from "@apollo/client";
import { Container, Loading } from "@fluidattacks/design";
import type { IUseModal } from "@fluidattacks/design";
import mixpanel from "mixpanel-browser";
import { useCallback, useEffect, useState } from "react";
import * as React from "react";
import { useTranslation } from "react-i18next";
import { object, string } from "yup";

import { useProjectMembers, useProjectNames } from "./hooks";
import { ProjectSelect } from "./project-select";
import { UPDATE_GITLAB_ISSUES_INTEGRATION } from "./queries";

import { IntegrationOAuthConfirm } from "../../integration-oauth-confirm";
import { IntegrationProjectsEmpty } from "../../integration-projects-empty";
import { GET_INTEGRATIONS } from "../../queries";
import type { TGroup } from "../queries";
import { Checkbox } from "components/checkbox";
import type { IDropDownOption } from "components/dropdown/types";
import { InnerForm } from "components/form";
import { FormModal } from "components/form-modal";
import { InputTags, Select } from "components/input";
import { msgSuccess } from "utils/notifications";
import { openUrl } from "utils/resource-helpers";

interface IConfigGitLabProps {
  readonly modalRef: IUseModal;
  readonly selectedGroup: TGroup;
}

interface IFormValues {
  assigneeId: string;
  gitlabProject: string;
  issueAutomationEnabled: boolean;
  labels: string;
}

const ConfigGitLab: React.FC<IConfigGitLabProps> = ({
  modalRef,
  selectedGroup,
}): JSX.Element => {
  const { t } = useTranslation();
  const { close, open } = modalRef;
  const { name: groupName, gitlabIssuesIntegration } = selectedGroup;

  useEffect((): void => {
    const comesFromGitLab = Boolean(localStorage.getItem("comesFromGitLab"));
    if (comesFromGitLab) {
      localStorage.removeItem("comesFromGitLab");
      open();
    }
  }, [open]);
  const goToAuthorize = useCallback((): void => {
    localStorage.setItem("comesFromGitLab", String(true));
    openUrl(`/begin_gitlab_issues_oauth?groupName=${groupName}`, false);
  }, [groupName]);

  const projectNames = useProjectNames(groupName);
  const [selectedProject, setSelectedProject] = useState(
    gitlabIssuesIntegration?.gitlabProject ?? undefined,
  );
  const handleProjectChange = useCallback((projectName: string): void => {
    setSelectedProject(projectName);
  }, []);
  const projectMembers = useProjectMembers(selectedProject, groupName);

  const [update] = useMutation(UPDATE_GITLAB_ISSUES_INTEGRATION);
  const handleSubmit = useCallback(
    async (values: IFormValues): Promise<void> => {
      await update({
        refetchQueries: [GET_INTEGRATIONS],
        variables: {
          assigneeIds: values.assigneeId ? [Number(values.assigneeId)] : [],
          gitlabProjectName: values.gitlabProject,
          groupName,
          issueAutomationEnabled: values.issueAutomationEnabled,
          labels: values.labels ? values.labels.split(",") : [],
        },
      });
      mixpanel.track("UpdateGitlabIntegration");
      msgSuccess(t("integrations.update.success"));
    },
    [groupName, t, update],
  );

  if (gitlabIssuesIntegration === null) {
    return (
      <IntegrationOAuthConfirm
        groupName={groupName}
        iconPath={"integrates/integrations/authorize-gitlab"}
        modalRef={modalRef}
        name={"GitLab"}
        onCancel={close}
        onConfirm={goToAuthorize}
      />
    );
  }

  return (
    <FormModal
      initialValues={{
        assigneeId: gitlabIssuesIntegration.assigneeIds?.[0].toString() ?? "",
        gitlabProject: gitlabIssuesIntegration.gitlabProject ?? "",
        issueAutomationEnabled:
          gitlabIssuesIntegration.issueAutomationEnabled ?? true,
        labels: gitlabIssuesIntegration.labels?.join(",") ?? "",
      }}
      modalRef={modalRef}
      onSubmit={handleSubmit}
      size={"md"}
      title={t("integrations.gitlab.config.title")}
      validationSchema={object().shape({
        gitlabProject: string().required(),
      })}
    >
      {({ isSubmitting, isValid }): JSX.Element => {
        if (projectNames === undefined || projectMembers === undefined) {
          return (
            <Container
              alignItems={"center"}
              display={"flex"}
              flexDirection={"column"}
            >
              <Loading />
            </Container>
          );
        }

        const isEmpty = projectNames.length === 0;

        return (
          <InnerForm
            onCancel={close}
            submitButtonLabel={t("integrations.gitlab.config.submit")}
            submitDisabled={isSubmitting || !isValid || isEmpty}
          >
            {isEmpty ? (
              <IntegrationProjectsEmpty name={"GitLab"} />
            ) : (
              <React.Fragment>
                <ProjectSelect
                  onChange={handleProjectChange}
                  projectNames={projectNames}
                />
                <Select
                  items={[
                    {
                      header: t("integrations.gitlab.config.assignees.default"),
                      value: "",
                    },
                    ...projectMembers.map((member): IDropDownOption => {
                      return {
                        header: `${member.name} (${member.username})`,
                        value: member.id.toString(),
                      };
                    }),
                  ]}
                  label={t("integrations.gitlab.config.assignees.label")}
                  name={"assigneeId"}
                  placeholder={t(
                    "integrations.gitlab.config.assignees.placeholder",
                  )}
                />
                <InputTags
                  label={t("integrations.gitlab.config.labels.label")}
                  name={"labels"}
                  placeholder={t(
                    "integrations.gitlab.config.labels.placeholder",
                  )}
                />
                <Checkbox
                  label={t("integrations.gitlab.config.issues.label")}
                  name={"issueAutomationEnabled"}
                  variant={"formikField"}
                />
              </React.Fragment>
            )}
          </InnerForm>
        );
      }}
    </FormModal>
  );
};

export { ConfigGitLab };
