import { useMutation } from "@apollo/client";
import { useConfirmDialog, useModal } from "@fluidattacks/design";
import mixpanel from "mixpanel-browser";
import { useCallback } from "react";
import * as React from "react";
import { useTranslation } from "react-i18next";

import { ConfigGitLab } from "./config-gitlab";
import type { TGroup } from "./queries";
import { GET_GITLAB_INTEGRATION } from "./queries";

import { IntegrationCard } from "../integration-card";
import { IntegrationGroupSelection } from "../integration-group-selection";
import { IntegrationInfo } from "../integration-info";
import { GET_INTEGRATIONS, REMOVE_GROUP_INTEGRATION } from "../queries";
import type { IGroup } from "../types";
import type { FragmentType } from "gql/fragment-masking";
import { getFragmentData } from "gql/fragment-masking";
import { GroupIntegration } from "gql/graphql";
import { useStoredState } from "hooks/use-stored-state";

interface IGitLabProps {
  readonly data: FragmentType<typeof GET_GITLAB_INTEGRATION>;
}

const GitLab: React.FC<IGitLabProps> = ({ data }): React.JSX.Element => {
  const { t } = useTranslation();

  const fragmentData = getFragmentData(GET_GITLAB_INTEGRATION, data);
  const { organizations } = fragmentData.me;
  const groups = organizations.flatMap((org): TGroup[] => org.groups);
  const connectedGroups = groups.filter(
    (group): boolean => group.gitlabIssuesIntegration !== null,
  );

  const [selectedGroupName, setSelectedGroupName] = useStoredState(
    "gitlabConfigGroup",
    "",
    localStorage,
  );
  const selectedGroup = groups.find(
    (group): boolean => group.name === selectedGroupName,
  );

  const groupSelectionModal = useModal("gitlab-group-selection-modal");
  const openGroupSelectionModal = useCallback((): void => {
    groupSelectionModal.open();
  }, [groupSelectionModal]);

  const configModal = useModal("gitlab-config-modal");
  const openConfigModal = useCallback(
    (groupName: string): void => {
      setSelectedGroupName(groupName);
      configModal.open();
    },
    [configModal, setSelectedGroupName],
  );

  const infoModal = useModal("gitlab-info-modal");
  const openInfoModal = useCallback(
    (groupName: string): void => {
      setSelectedGroupName(groupName);
      infoModal.open();
    },
    [infoModal, setSelectedGroupName],
  );

  const { confirm, ConfirmDialog } = useConfirmDialog();
  const [removeGroupIntegration] = useMutation(REMOVE_GROUP_INTEGRATION);
  const disconnectIntegration = useCallback(
    async (groupName: string): Promise<void> => {
      const confirmResult = await confirm({
        title: t("integrations.disconnect.confirm"),
      });

      if (confirmResult) {
        await removeGroupIntegration({
          refetchQueries: [GET_INTEGRATIONS],
          variables: { groupName, integration: GroupIntegration.Gitlab },
        });
        mixpanel.track("RemoveGitlabIntegration");
      }
    },
    [confirm, removeGroupIntegration, t],
  );

  return (
    <React.Fragment>
      <IntegrationCard
        description={t("integrations.gitlab.description")}
        docsLink={
          "https://help.fluidattacks.com/portal/en/kb/articles/set-up-the-gitlab-integration"
        }
        groupsSummary={{
          connected: connectedGroups.length,
          total: groups.length,
        }}
        iconPath={"integrates/integrations/icon-gitlab"}
        name={"GitLab"}
        onClickConfig={openGroupSelectionModal}
      />
      {selectedGroup ? (
        <ConfigGitLab modalRef={configModal} selectedGroup={selectedGroup} />
      ) : undefined}
      <ConfirmDialog />
      <IntegrationGroupSelection
        groups={groups.map((group): IGroup => {
          return {
            connected: group.gitlabIssuesIntegration !== null,
            description: group.description ?? "",
            name: group.name,
          };
        })}
        isFirstTime={connectedGroups.length === 0}
        modalRef={groupSelectionModal}
        name={"GitLab"}
        onClickConnect={openConfigModal}
        onClickDisconnect={disconnectIntegration}
        onClickEdit={openConfigModal}
        onClickInfo={openInfoModal}
      />
      <IntegrationInfo
        connectionDate={selectedGroup?.gitlabIssuesIntegration?.connectionDate}
        iconPath={"integrates/integrations/icon-gitlab"}
        modalRef={infoModal}
        name={"GitLab"}
        permissions={[
          {
            description: t("integrations.gitlab.permissions.api.description"),
            title: t("integrations.gitlab.permissions.api.title"),
          },
        ]}
      />
    </React.Fragment>
  );
};

export { GitLab };
