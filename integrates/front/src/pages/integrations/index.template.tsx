import * as React from "react";
import { useTranslation } from "react-i18next";

import { SectionHeader } from "components/section-header";

interface IIntegrationsTemplateProps {
  readonly children: React.ReactNode;
}

const IntegrationsTemplate: React.FC<IIntegrationsTemplateProps> = ({
  children,
}): React.JSX.Element => {
  const { t } = useTranslation();

  return (
    <React.Fragment>
      <SectionHeader header={t("integrations.title")} />
      {children}
    </React.Fragment>
  );
};

export { IntegrationsTemplate };
