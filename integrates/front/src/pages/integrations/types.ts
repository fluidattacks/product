import type React from "react";

import type { GetIntegrationsQuery } from "gql/graphql";

interface IGroup {
  connected: boolean;
  description: string;
  name: string;
}

type TCategory = "BTS" | "IDE" | "OTHERS";

interface IIntegration {
  category: TCategory;
  component: (data: GetIntegrationsQuery) => React.JSX.Element;
  name: string;
}

export type { IGroup, IIntegration, TCategory };
