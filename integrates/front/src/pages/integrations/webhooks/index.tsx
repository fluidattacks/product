import { useModal } from "@fluidattacks/design";
import { useCallback, useState } from "react";
import * as React from "react";
import { useTranslation } from "react-i18next";

import { ConfigWebhooks } from "./config-webhooks";
import { GET_WEBHOOKS_INTEGRATION } from "./queries";

import { IntegrationCard } from "../integration-card";
import { IntegrationGroupSelection } from "../integration-group-selection";
import type { IGroup } from "../types";
import type { FragmentType } from "gql/fragment-masking";
import { getFragmentData } from "gql/fragment-masking";
import type { GetWebhooksIntegrationFragment } from "gql/graphql";

type TGroups =
  GetWebhooksIntegrationFragment["me"]["organizations"][0]["groups"];

interface IWebhooksProps {
  readonly data: FragmentType<typeof GET_WEBHOOKS_INTEGRATION>;
}

const Webhooks: React.FC<IWebhooksProps> = ({ data }): React.JSX.Element => {
  const { t } = useTranslation();

  const fragmentData = getFragmentData(GET_WEBHOOKS_INTEGRATION, data);
  const { organizations } = fragmentData.me;
  const groups = organizations.flatMap((org): TGroups => org.groups);
  const connectedGroups = groups.filter(
    (group): boolean => (group.hooks ?? []).length > 0,
  );

  const [selectedGroupName, setSelectedGroupName] = useState("");
  const selectedGroup = groups.find(
    (group): boolean => group.name === selectedGroupName,
  );

  const groupSelectionModal = useModal("webhooks-group-selection-modal");
  const openGroupSelectionModal = useCallback((): void => {
    groupSelectionModal.open();
  }, [groupSelectionModal]);

  const configModal = useModal("webhooks-config-modal");
  const openConfigModal = useCallback(
    (groupName: string): void => {
      setSelectedGroupName(groupName);
      configModal.open();
    },
    [configModal, setSelectedGroupName],
  );

  return (
    <React.Fragment>
      <IntegrationCard
        description={t("integrations.webhooks.description")}
        docsLink={
          "https://help.fluidattacks.com/portal/en/kb/articles/get-notified-with-webhooks"
        }
        groupsSummary={{
          connected: connectedGroups.length,
          total: groups.length,
        }}
        iconPath={"integrates/integrations/icon-webhooks"}
        name={"Webhooks"}
        onClickConfig={openGroupSelectionModal}
      />
      <ConfigWebhooks
        groupName={selectedGroupName}
        groupPermissions={selectedGroup?.permissions ?? []}
        modalRef={configModal}
      />
      <IntegrationGroupSelection
        groups={groups.map((group): IGroup => {
          return {
            connected: (group.hooks ?? []).length > 0,
            description: group.description ?? "",
            name: group.name,
          };
        })}
        isFirstTime={connectedGroups.length === 0}
        modalRef={groupSelectionModal}
        name={"Webhooks"}
        onClickConnect={openConfigModal}
        onClickEdit={openConfigModal}
      />
    </React.Fragment>
  );
};

export { Webhooks };
