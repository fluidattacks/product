import { PureAbility } from "@casl/ability";
import { Modal } from "@fluidattacks/design";
import type { IUseModal } from "@fluidattacks/design";
import React, { useMemo } from "react";
import { useTranslation } from "react-i18next";

import { Hooks } from "../hook";
import { authzPermissionsContext } from "context/authz/config";

interface IConfigWebhooksProps {
  readonly groupName: string;
  readonly groupPermissions: string[];
  readonly modalRef: IUseModal;
}

const ConfigWebhooks: React.FC<IConfigWebhooksProps> = ({
  groupName,
  groupPermissions,
  modalRef,
}): JSX.Element => {
  const { t } = useTranslation();

  const groupLevelPermissions = useMemo((): PureAbility<string> => {
    return new PureAbility(
      groupPermissions.map((action): { action: string } => ({ action })),
    );
  }, [groupPermissions]);

  return (
    <Modal
      modalRef={modalRef}
      size={"md"}
      title={t("integrations.webhooks.config.title")}
    >
      <authzPermissionsContext.Provider value={groupLevelPermissions}>
        <Hooks groupName={groupName} />
      </authzPermissionsContext.Provider>
    </Modal>
  );
};

export { ConfigWebhooks };
