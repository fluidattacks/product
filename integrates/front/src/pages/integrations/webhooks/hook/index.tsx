import { useMutation, useQuery } from "@apollo/client";
import { Button, useConfirmDialog } from "@fluidattacks/design";
import type { ColumnDef } from "@tanstack/react-table";
import type { GraphQLError } from "graphql";
import _ from "lodash";
import mixpanel from "mixpanel-browser";
import { Fragment, useCallback, useMemo, useState } from "react";
import * as React from "react";
import { useTranslation } from "react-i18next";

import type { IHookAttr } from "./types";

import {
  ADD_GROUP_HOOKS_MUTATION,
  GET_HOOKS,
  REMOVE_GROUP_HOOK_MUTATION,
  UPDATE_GROUP_HOOKS_MUTATION,
} from "../../../group/scope/queries";
import { AddHookModal } from "../add-hook-modal";
import { Table } from "components/table";
import { Can } from "context/authz/can";
import type { HookEventType, HookInput } from "gql/graphql";
import { useModal, useTable } from "hooks";
import { listTagsFormatter } from "utils/format-helpers";
import { Logger } from "utils/logger";
import { msgError, msgSuccess } from "utils/notifications";

interface IHooksProps {
  readonly groupName: string;
}

const Hooks: React.FC<IHooksProps> = ({ groupName }): JSX.Element => {
  const { t } = useTranslation();
  const tableRef = useTable("tblHooks");
  const { confirm, ConfirmDialog } = useConfirmDialog();

  // State management
  const initialHook = useMemo(
    (): HookInput => ({
      entryPoint: "",
      hookEvents: [],
      name: "",
      token: "",
      tokenHeader: "",
    }),
    [],
  );

  const [selectedHooks, setSelectedHooks] = useState<IHookAttr[]>([]);
  const [isEditing, setIsEditing] = useState(false);
  const addHookModal = useModal("add-hook-modal");
  const { open } = addHookModal;

  const openUpdateModal = useCallback((): void => {
    setIsEditing(true);
    open();
  }, [open]);

  const closeAddModal = useCallback((): void => {
    addHookModal.close();
    setIsEditing(false);
  }, [addHookModal]);

  const { data, refetch } = useQuery(GET_HOOKS, {
    onError: (error): void => {
      msgError(t("groupAlerts.errorTextsad"));
      Logger.warning("An error occurred loading group hooks", error);
    },
    variables: { groupName },
  });

  const handleGroupHookError = (
    graphQLErrors: readonly GraphQLError[],
  ): void => {
    graphQLErrors.forEach((error): void => {
      switch (error.message) {
        case "Hook data is invalid":
          msgError(t(`group.scope.hooks.errorMessage.invalidData`));
          break;
        case "Entry point already exists":
          msgError(t(`group.scope.hooks.errorMessage.duplicated`));
          break;
        case "Access denied or hook not found":
          msgError(t(`group.scope.hooks.errorMessage.notFound`));
          break;
        case "Can not find host":
          msgError(t(`group.scope.hooks.errorMessage.unreachedHost`));
          break;
        case "Exception - Protocol http is invalid":
          msgError(t(`group.scope.hooks.errorMessage.invalidHttpProtocol`));
          break;
        default:
          msgError(t("groupAlerts.errorTextsad"));
          Logger.warning("An error occurred updating the hook", error);
      }
    });
  };

  const [removeGroupHook] = useMutation(REMOVE_GROUP_HOOK_MUTATION, {
    onCompleted: (): void => {
      void refetch();
      setSelectedHooks([]);
      mixpanel.track("RemoveGroupHook");
      msgSuccess(
        t("searchFindings.tabResources.successRemove"),
        t("searchFindings.tabUsers.titleSuccess"),
      );
    },
    onError: (error): void => {
      msgError(t("groupAlerts.errorTextsad"));
      Logger.warning("An error occurred removing hook", error);
    },
  });

  const [updateGroupHook] = useMutation(UPDATE_GROUP_HOOKS_MUTATION, {
    onCompleted: (): void => {
      void refetch();
      setSelectedHooks([]);
      mixpanel.track("UpdateGroupHook");
      msgSuccess(
        t("group.scope.hooks.successMessage.edit"),
        t("searchFindings.tabUsers.titleSuccess"),
      );
    },
    onError: (error): void => {
      handleGroupHookError(error.graphQLErrors);
    },
  });

  const [addGroupHook] = useMutation(ADD_GROUP_HOOKS_MUTATION, {
    onCompleted: (): void => {
      void refetch();
      mixpanel.track("AddGroupHook");
      msgSuccess(
        t("group.scope.hooks.successMessage.add"),
        t("searchFindings.tabUsers.titleSuccess"),
      );
    },
    onError: (error): void => {
      handleGroupHookError(error.graphQLErrors);
    },
  });

  const resourcesFiles =
    _.isUndefined(data) || _.isEmpty(data) || _.isNull(data.group.hook)
      ? []
      : data.group.hook;

  const hooksDataset = resourcesFiles as IHook[];

  const handleHookAdd = useCallback(
    async (hook: HookInput): Promise<void> => {
      closeAddModal();
      if (isEditing) {
        await updateGroupHook({
          variables: {
            groupName,
            hook,
            hookId: selectedHooks[0].id,
          },
        });
      } else {
        await addGroupHook({
          variables: {
            groupName,
            hook,
          },
        });
      }
    },
    [
      addGroupHook,
      closeAddModal,
      groupName,
      updateGroupHook,
      isEditing,
      selectedHooks,
    ],
  );

  const handleClick = useCallback(async (): Promise<void> => {
    const confirmResult = await confirm({
      message: t("group.scope.hooks.actionButtons.removeButton.confirmMessage"),
      title: t("group.scope.hooks.actionButtons.removeButton.confirmTitle"),
    });
    if (confirmResult) {
      await removeGroupHook({
        variables: {
          groupName,
          hookId: selectedHooks[0].id,
        },
      });
    }
  }, [confirm, t, selectedHooks, groupName, removeGroupHook]);

  interface IHook {
    id: string;
    entryPoint: string;
    name: string;
    token: string;
    tokenHeader: string;
    hookEvents: HookEventType[];
  }

  const columns: ColumnDef<IHook>[] = [
    {
      accessorKey: "name",
      header: t("group.scope.hooks.table.name"),
    },
    {
      accessorKey: "entryPoint",
      header: t("group.scope.hooks.table.entryPoint"),
    },
    {
      accessorKey: "tokenHeader",
      header: t("group.scope.hooks.table.tokenHeader"),
    },
    {
      accessorKey: "hookEvents",
      cell: (cell): JSX.Element =>
        listTagsFormatter(cell.getValue() as HookEventType[]),
      header: t("group.scope.hooks.table.hookEvents"),
    },
  ];

  return (
    <React.Fragment>
      <Table
        columns={columns}
        data={hooksDataset}
        rightSideComponents={
          <Fragment>
            <Can do={"integrates_api_mutations_add_hook_mutate"}>
              <Button
                icon={"add"}
                id={"hook-add"}
                onClick={open}
                tooltip={t("group.scope.hooks.addTooltip")}
                variant={"primary"}
              >
                {t("group.scope.hooks.actionButtons.addButton.text")}
              </Button>
            </Can>
            <Can do={"integrates_api_mutations_update_hook_mutate"}>
              <Button
                disabled={selectedHooks.length !== 1}
                icon={"edit"}
                id={"hook-edit"}
                onClick={openUpdateModal}
                tooltip={t(
                  "group.scope.hooks.actionButtons.editButton.tooltip",
                )}
              >
                {t("group.scope.hooks.actionButtons.editButton.text")}
              </Button>
            </Can>
            <Can do={"integrates_api_mutations_remove_hook_mutate"}>
              <Button
                disabled={selectedHooks.length !== 1}
                icon={"remove"}
                id={"hook-remove"}
                onClick={handleClick}
                tooltip={t(
                  "group.scope.hooks.actionButtons.removeButton.tooltip",
                )}
                variant={"secondary"}
              >
                {t("group.scope.hooks.actionButtons.removeButton.text")}
              </Button>
            </Can>
          </Fragment>
        }
        rowSelectionSetter={setSelectedHooks}
        rowSelectionState={selectedHooks}
        selectionMode={"radio"}
        tableRef={tableRef}
      />
      <AddHookModal
        hook={isEditing ? selectedHooks[0] : initialHook}
        isEditing={isEditing}
        modalRef={{
          ...addHookModal,
          close: closeAddModal,
        }}
        onSubmit={handleHookAdd}
      />
      <ConfirmDialog />
    </React.Fragment>
  );
};

export type { IHooksProps };
export { Hooks };
