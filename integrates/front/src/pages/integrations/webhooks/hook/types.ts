import type { HookEventType } from "gql/graphql";

interface IHookAttr {
  id: string;
  entryPoint: string;
  name: string;
  token: string;
  tokenHeader: string;
  hookEvents: HookEventType[];
}

export type { IHookAttr };
