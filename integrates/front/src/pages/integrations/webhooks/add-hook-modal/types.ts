import { HookEventType, type HookInput } from "gql/graphql";
import type { IUseModal } from "hooks/use-modal";

interface IAddHookModalProps {
  modalRef: IUseModal;
  onSubmit: (hook: HookInput) => void;
  hook: HookInput;
  isEditing: boolean;
}

export const HOOK_EVENT_TO_LABEL: Record<HookEventType, string> = {
  [HookEventType.EventClosed]: "Event closed",
  [HookEventType.EventCreated]: "New group eventuality",
  [HookEventType.RootCreated]: "Root added",
  [HookEventType.RootDisabled]: "Root disablement",
  [HookEventType.VulnerabilityClosed]: "Vulnerability closed",
  [HookEventType.VulnerabilityCreated]: "Vulnerability created",
  [HookEventType.VulnerabilityDeleted]: "Vulnerability deleted",
  [HookEventType.AgentTokenExpiration]: "Agent token expiration",
  [HookEventType.EnvironmentRemoved]: "Environment removed",
  [HookEventType.VulnerabilityAssigned]: "Assigned vulnerability",
  [HookEventType.VulnerabilityVerified]: "Response to reattacks",
  [HookEventType.VulnerabilitySeverityChanged]: "Severity Changed",
};

export type { IAddHookModalProps };
