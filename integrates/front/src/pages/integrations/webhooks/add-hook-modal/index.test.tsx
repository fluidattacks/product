import { screen, waitFor } from "@testing-library/react";
import { userEvent } from "@testing-library/user-event";

import { AddHookModal } from ".";
import { CustomThemeProvider } from "components/colors";
import type { HookInput } from "gql/graphql";
import { HookEventType } from "gql/graphql";
import { useModal } from "hooks/use-modal";
import { render } from "mocks";

const functionMock = (): void => undefined;
const AddHookModalComp = ({
  hook,
}: Readonly<{
  hook: HookInput;
}>): JSX.Element => {
  const modalProps = useModal("test-modal");
  const handleOnClose = jest.fn();

  return (
    <CustomThemeProvider>
      <AddHookModal
        hook={hook}
        isEditing={false}
        modalRef={{
          ...modalProps,
          close: handleOnClose,
          isOpen: true,
        }}
        onSubmit={functionMock}
      />
    </CustomThemeProvider>
  );
};

describe("add Hook modal", (): void => {
  const initialHook: HookInput = {
    entryPoint: "",
    hookEvents: [],
    name: "",
    token: "",
    tokenHeader: "",
  };

  it("should render", (): void => {
    expect.hasAssertions();

    render(<AddHookModalComp hook={initialHook} />);

    expect(
      screen.queryByText("group.scope.hooks.modal.title"),
    ).toBeInTheDocument();
  });

  it("should validate required fields", async (): Promise<void> => {
    expect.hasAssertions();

    jest.clearAllMocks();

    render(<AddHookModalComp hook={initialHook} />);
    const btnConfirm = "buttons.confirm";

    await userEvent.click(screen.getByText(btnConfirm));

    expect(screen.getAllByText("Required")).toHaveLength(3);

    await userEvent.type(
      screen.getByRole("combobox", { name: "entryPoint" }),
      "entryPoint",
    );
    await userEvent.type(
      screen.getByRole("combobox", { name: "token" }),
      "token",
    );
    await userEvent.click(screen.getByText(btnConfirm));

    expect(screen.queryAllByText("validations.required")).toHaveLength(0);
  });

  it("should set initial options", async (): Promise<void> => {
    expect.hasAssertions();

    jest.clearAllMocks();

    render(
      <AddHookModalComp
        hook={{
          entryPoint: "entryPoint",
          hookEvents: [HookEventType.VulnerabilityCreated],
          name: "hook1",
          token: "token",
          tokenHeader: "tokenHeader",
        }}
      />,
    );

    await waitFor((): void => {
      expect(
        document.querySelectorAll("input[name='VULNERABILITY_CREATED']")[0],
      ).toBeChecked();
    });
  });
});
