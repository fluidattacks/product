import { Col, Container, Gap, Row, Text } from "@fluidattacks/design";
import { StrictMode, useCallback, useEffect, useState } from "react";
import * as React from "react";
import { useTranslation } from "react-i18next";
import { object, string } from "yup";

import { HOOK_EVENT_TO_LABEL, type IAddHookModalProps } from "./types";

import { Checkbox } from "components/checkbox";
import { InnerForm } from "components/form";
import { FormModal } from "components/form-modal";
import { Input } from "components/input";
import { HookEventType } from "gql/graphql";

const AddHookModal = ({
  modalRef,
  onSubmit,
  hook,
  isEditing,
}: IAddHookModalProps): JSX.Element => {
  const { t } = useTranslation();
  const { close } = modalRef;
  const options = Object.values(HookEventType);
  const [selectedOptions, setSelectedOptions] = useState<HookEventType[]>([]);

  const handleCheckboxChange = useCallback(
    (event: React.ChangeEvent<HTMLInputElement>): void => {
      const { name, checked } = event.target;
      const nameParsed: HookEventType = name as HookEventType;
      const updatedSelectedOptions = checked
        ? [...selectedOptions, nameParsed]
        : selectedOptions.filter((option): boolean => option !== nameParsed);

      setSelectedOptions(updatedSelectedOptions);
    },
    [selectedOptions, setSelectedOptions],
  );

  const handleSubmit = useCallback(
    (values: {
      entryPoint: string;
      name: string;
      token: string;
      tokenHeader: string;
    }): void => {
      if (selectedOptions.length === 0) {
        return;
      }
      const newHook = {
        entryPoint: values.entryPoint,
        hookEvents: selectedOptions,
        name: values.name,
        token: values.token,
        tokenHeader: values.tokenHeader,
      };
      setSelectedOptions([]);
      onSubmit(newHook);
    },
    [onSubmit, selectedOptions],
  );

  useEffect((): void => {
    setSelectedOptions(hook.hookEvents ?? []);
  }, [hook.hookEvents]);

  return (
    <StrictMode>
      <FormModal
        initialValues={hook}
        modalRef={modalRef}
        name={"addHook"}
        onSubmit={handleSubmit}
        size={"md"}
        title={
          isEditing
            ? t("group.scope.hooks.modal.editTittle")
            : t("group.scope.hooks.modal.title")
        }
        validationSchema={object({
          entryPoint: string().required("Required"),
          name: string().required("Required"),
          token: string().required("Required"),
        })}
      >
        <InnerForm onCancel={close}>
          <Gap disp={"block"} mh={0} mv={12}>
            <Input
              label={t("group.scope.hooks.table.entryPoint")}
              name={"entryPoint"}
              required={true}
            />
            <Input
              label={t("group.scope.hooks.table.name")}
              name={"name"}
              required={true}
            />
            <Row>
              <Col lg={30} md={30} sm={30}>
                <Input
                  label={t("group.scope.hooks.table.tokenHeader")}
                  name={"tokenHeader"}
                  placeholder={"x-api-key"}
                />
              </Col>
              <Col lg={70} md={70} sm={70}>
                <Input
                  label={t("group.scope.hooks.table.token")}
                  name={"token"}
                  required={true}
                />
              </Col>
            </Row>
            <Container display={"flex"} flexDirection={"column"} gap={0.5}>
              <Text mb={0.75} size={"sm"}>
                {t("group.scope.hooks.hookEvents")}
              </Text>
              <Container
                display={"flex"}
                flexDirection={"column"}
                gap={0.25}
                maxHeight={"120px"}
                wrap={"wrap"}
              >
                {options.map((option): JSX.Element => {
                  return (
                    <Checkbox
                      defaultChecked={selectedOptions.includes(option)}
                      key={option}
                      label={HOOK_EVENT_TO_LABEL[option]}
                      name={option}
                      onChange={handleCheckboxChange}
                      value={option}
                    />
                  );
                })}
              </Container>
            </Container>
          </Gap>
        </InnerForm>
      </FormModal>
    </StrictMode>
  );
};

export type { IAddHookModalProps };
export { AddHookModal };
