import { graphql } from "gql/gql";

const GET_WEBHOOKS_INTEGRATION = graphql(`
  fragment GetWebhooksIntegration on Query {
    me {
      userEmail
      organizations {
        name
        groups {
          description
          name
          permissions
          hooks: hook {
            id
            entryPoint
            hookEvents
            name
            token
            tokenHeader
          }
        }
      }
    }
  }
`);

export { GET_WEBHOOKS_INTEGRATION };
