import * as React from "react";
import { useTranslation } from "react-i18next";

import { IntegrationCard } from "../integration-card";

const Api: React.FC = (): React.JSX.Element => {
  const { t } = useTranslation();

  return (
    <IntegrationCard
      description={t("integrations.api.description")}
      docsLink={
        "https://help.fluidattacks.com/portal/en/kb/integrate-with-fluid-attacks/use-the-api"
      }
      iconPath={"integrates/integrations/icon-api"}
      name={"API"}
    />
  );
};

export { Api };
