import { useQuery } from "@apollo/client";

import {
  GET_AZURE_ORGANIZATION_NAMES,
  GET_AZURE_PROJECT_MEMBERS,
  GET_AZURE_PROJECT_NAMES,
} from "./queries";

import type { AzureProjectMember } from "gql/graphql";

const useOrganizationNames = (groupName: string): string[] | undefined => {
  const { data } = useQuery(GET_AZURE_ORGANIZATION_NAMES, {
    fetchPolicy: "no-cache",
    variables: { groupName },
  });

  return data?.group.azureIssuesIntegration?.organizationNames;
};

const useProjectMembers = (
  azureOrganization: string | undefined,
  azureProject: string | undefined,
  groupName: string,
): AzureProjectMember[] | undefined => {
  const { data, loading } = useQuery(GET_AZURE_PROJECT_MEMBERS, {
    fetchPolicy: "no-cache",
    skip: azureOrganization === undefined || azureProject === undefined,
    variables: {
      azureOrganization: azureOrganization ?? "",
      azureProject: azureProject ?? "",
      groupName,
    },
  });

  if (loading) {
    return undefined;
  }

  return data?.group.azureIssuesIntegration?.projectMembers ?? [];
};

const useProjectNames = (
  azureOrganization: string | undefined,
  groupName: string,
): string[] | undefined => {
  const { data, loading } = useQuery(GET_AZURE_PROJECT_NAMES, {
    fetchPolicy: "no-cache",
    skip: azureOrganization === undefined,
    variables: { azureOrganization: azureOrganization ?? "", groupName },
  });

  if (loading) {
    return undefined;
  }

  return data?.group.azureIssuesIntegration?.projectNames ?? [];
};

export { useOrganizationNames, useProjectMembers, useProjectNames };
