import { useFormikContext } from "formik";
import { useCallback } from "react";
import * as React from "react";
import { useTranslation } from "react-i18next";

import type { IDropDownOption } from "components/dropdown/types";
import { Select } from "components/input";

interface IOrganizationSelectProps {
  readonly onChange: (organizationName: string) => void;
  readonly organizationNames: string[];
}

const OrganizationSelect: React.FC<IOrganizationSelectProps> = ({
  onChange,
  organizationNames,
}): React.JSX.Element => {
  const { t } = useTranslation();
  const formikHelpers = useFormikContext();

  const handleChange = useCallback(
    async (selection: IDropDownOption): Promise<void> => {
      await formikHelpers.setFieldValue("azureProject", undefined);
      await formikHelpers.setFieldTouched("azureProject");
      await formikHelpers.setFieldValue("assignedTo", undefined);
      await formikHelpers.setFieldTouched("assignedTo");
      onChange(selection.value ?? "");
    },
    [formikHelpers, onChange],
  );

  return (
    <Select
      handleOnChange={handleChange}
      items={organizationNames.map((orgName): IDropDownOption => {
        return { value: orgName };
      })}
      label={t("integrations.azure.config.organization.label")}
      name={"azureOrganization"}
      placeholder={t("integrations.azure.config.organization.placeholder")}
      required={true}
    />
  );
};

export { OrganizationSelect };
