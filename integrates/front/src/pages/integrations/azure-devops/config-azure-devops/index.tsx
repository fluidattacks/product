import { useMutation } from "@apollo/client";
import { Container, Loading } from "@fluidattacks/design";
import type { IUseModal } from "@fluidattacks/design";
import mixpanel from "mixpanel-browser";
import { useCallback, useEffect, useState } from "react";
import * as React from "react";
import { useTranslation } from "react-i18next";
import { object, string } from "yup";

import {
  useOrganizationNames,
  useProjectMembers,
  useProjectNames,
} from "./hooks";
import { OrganizationSelect } from "./organization-select";
import { ProjectSelect } from "./project-select";
import { UPDATE_AZURE_ISSUES_INTEGRATION } from "./queries";

import { IntegrationOAuthConfirm } from "../../integration-oauth-confirm";
import { IntegrationProjectsEmpty } from "../../integration-projects-empty";
import { GET_INTEGRATIONS } from "../../queries";
import type { TGroup } from "../queries";
import { Checkbox } from "components/checkbox";
import type { IDropDownOption } from "components/dropdown/types";
import { InnerForm } from "components/form";
import { FormModal } from "components/form-modal";
import { InputTags, Select } from "components/input";
import { msgSuccess } from "utils/notifications";
import { openUrl } from "utils/resource-helpers";

interface IConfigAzureDevOpsProps {
  readonly modalRef: IUseModal;
  readonly selectedGroup: TGroup;
}

interface IFormValues {
  assignedTo: string;
  azureOrganization: string;
  azureProject: string;
  issueAutomationEnabled: boolean;
  tags: string;
}

const ConfigAzureDevOps: React.FC<IConfigAzureDevOpsProps> = ({
  modalRef,
  selectedGroup,
}): JSX.Element => {
  const { t } = useTranslation();
  const { close, open } = modalRef;
  const { name: groupName, azureIssuesIntegration } = selectedGroup;

  useEffect((): void => {
    const comesFromAzure = Boolean(localStorage.getItem("comesFromAzure"));
    if (comesFromAzure) {
      localStorage.removeItem("comesFromAzure");
      open();
    }
  }, [open]);
  const goToAuthorize = useCallback((): void => {
    localStorage.setItem("comesFromAzure", String(true));
    openUrl(`/begin_azure_issues_oauth?groupName=${groupName}`, false);
  }, [groupName]);

  const organizationNames = useOrganizationNames(groupName);
  const [selectedOrganization, setSelectedOrganization] = useState(
    azureIssuesIntegration?.azureOrganization ?? undefined,
  );
  const handleOrganizationChange = useCallback(
    (organizationName: string): void => {
      setSelectedOrganization(organizationName);
    },
    [],
  );
  const projectNames = useProjectNames(selectedOrganization, groupName);
  const [selectedProject, setSelectedProject] = useState(
    azureIssuesIntegration?.azureProject ?? undefined,
  );
  const handleProjectChange = useCallback((projectName: string): void => {
    setSelectedProject(projectName);
  }, []);
  const projectMembers = useProjectMembers(
    selectedOrganization,
    selectedProject,
    groupName,
  );

  const [update] = useMutation(UPDATE_AZURE_ISSUES_INTEGRATION);
  const handleSubmit = useCallback(
    async (values: IFormValues): Promise<void> => {
      await update({
        refetchQueries: [GET_INTEGRATIONS],
        variables: {
          assignedTo: values.assignedTo,
          azureOrganizationName: values.azureOrganization,
          azureProjectName: values.azureProject,
          groupName,
          issueAutomationEnabled: values.issueAutomationEnabled,
          tags: values.tags ? values.tags.split(",") : [],
        },
      });
      mixpanel.track("UpdateAzureIntegration");
      msgSuccess(t("integrations.update.success"));
    },
    [groupName, t, update],
  );

  if (azureIssuesIntegration === null) {
    return (
      <IntegrationOAuthConfirm
        groupName={groupName}
        iconPath={"integrates/integrations/authorize-azure"}
        modalRef={modalRef}
        name={"Azure DevOps"}
        onCancel={close}
        onConfirm={goToAuthorize}
      />
    );
  }

  return (
    <FormModal
      initialValues={{
        assignedTo: azureIssuesIntegration.assignedTo ?? "",
        azureOrganization: azureIssuesIntegration.azureOrganization ?? "",
        azureProject: azureIssuesIntegration.azureProject ?? "",
        issueAutomationEnabled:
          azureIssuesIntegration.issueAutomationEnabled ?? true,
        tags: azureIssuesIntegration.tags?.join(",") ?? "",
      }}
      modalRef={modalRef}
      onSubmit={handleSubmit}
      size={"md"}
      title={t("integrations.azure.config.title")}
      validationSchema={object().shape({
        azureOrganization: string().required(),
        azureProject: string().required(),
      })}
    >
      {({ isSubmitting, isValid }): JSX.Element => {
        if (
          organizationNames === undefined ||
          projectNames === undefined ||
          projectMembers === undefined
        ) {
          return (
            <Container
              alignItems={"center"}
              display={"flex"}
              flexDirection={"column"}
            >
              <Loading />
            </Container>
          );
        }

        const isEmpty =
          organizationNames.length === 0 || projectNames.length === 0;

        return (
          <InnerForm
            onCancel={close}
            submitButtonLabel={t("integrations.azure.config.submit")}
            submitDisabled={isSubmitting || !isValid || isEmpty}
          >
            {organizationNames.length === 0 ? (
              <IntegrationProjectsEmpty name={"Azure DevOps"} />
            ) : (
              <React.Fragment>
                <OrganizationSelect
                  onChange={handleOrganizationChange}
                  organizationNames={organizationNames}
                />
                {projectNames.length === 0 ? (
                  <IntegrationProjectsEmpty name={"Azure DevOps"} />
                ) : (
                  <React.Fragment>
                    <ProjectSelect
                      onChange={handleProjectChange}
                      projectNames={projectNames}
                    />
                    <Select
                      items={[
                        {
                          header: t(
                            "integrations.azure.config.assignedTo.default",
                          ),
                          value: "",
                        },
                        ...projectMembers.map((member): IDropDownOption => {
                          return {
                            header: `${member.displayName} (${member.uniqueName})`,
                            value: member.uniqueName,
                          };
                        }),
                      ]}
                      label={t("integrations.azure.config.assignedTo.label")}
                      name={"assignedTo"}
                      placeholder={t(
                        "integrations.azure.config.assignedTo.placeholder",
                      )}
                    />
                    <InputTags
                      label={t("integrations.azure.config.tags.label")}
                      name={"tags"}
                      placeholder={t(
                        "integrations.azure.config.tags.placeholder",
                      )}
                    />
                    <Checkbox
                      label={t("integrations.azure.config.issues.label")}
                      name={"issueAutomationEnabled"}
                      variant={"formikField"}
                    />
                  </React.Fragment>
                )}
              </React.Fragment>
            )}
          </InnerForm>
        );
      }}
    </FormModal>
  );
};

export { ConfigAzureDevOps };
