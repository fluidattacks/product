import { graphql } from "gql/gql";

const GET_AZURE_ORGANIZATION_NAMES = graphql(`
  query GetAzureOrganizationNames($groupName: String!) {
    group(groupName: $groupName) {
      azureIssuesIntegration {
        organizationNames
      }
    }
  }
`);

const GET_AZURE_PROJECT_MEMBERS = graphql(`
  query GetAzureProjectMembers(
    $azureOrganization: String!
    $azureProject: String!
    $groupName: String!
  ) {
    group(groupName: $groupName) {
      azureIssuesIntegration {
        projectMembers(
          azureOrganization: $azureOrganization
          azureProject: $azureProject
        ) {
          id
          displayName
          uniqueName
        }
      }
    }
  }
`);

const GET_AZURE_PROJECT_NAMES = graphql(`
  query GetAzureProjectNames($azureOrganization: String!, $groupName: String!) {
    group(groupName: $groupName) {
      azureIssuesIntegration {
        projectNames(azureOrganization: $azureOrganization)
      }
    }
  }
`);

const UPDATE_AZURE_ISSUES_INTEGRATION = graphql(`
  mutation UpdateAzureIssuesIntegration(
    $assignedTo: String
    $azureOrganizationName: String!
    $azureProjectName: String!
    $groupName: String!
    $issueAutomationEnabled: Boolean
    $tags: [String!]
  ) {
    updateAzureIssuesIntegration(
      assignedTo: $assignedTo
      azureOrganizationName: $azureOrganizationName
      azureProjectName: $azureProjectName
      groupName: $groupName
      issueAutomationEnabled: $issueAutomationEnabled
      tags: $tags
    ) {
      success
    }
  }
`);

export {
  GET_AZURE_ORGANIZATION_NAMES,
  GET_AZURE_PROJECT_MEMBERS,
  GET_AZURE_PROJECT_NAMES,
  UPDATE_AZURE_ISSUES_INTEGRATION,
};
