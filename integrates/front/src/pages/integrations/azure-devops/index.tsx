import { useMutation } from "@apollo/client";
import { useConfirmDialog, useModal } from "@fluidattacks/design";
import mixpanel from "mixpanel-browser";
import { useCallback } from "react";
import * as React from "react";
import { useTranslation } from "react-i18next";

import { ConfigAzureDevOps } from "./config-azure-devops";
import { GET_AZURE_INTEGRATION } from "./queries";

import { IntegrationCard } from "../integration-card";
import { IntegrationGroupSelection } from "../integration-group-selection";
import { IntegrationInfo } from "../integration-info";
import { GET_INTEGRATIONS, REMOVE_GROUP_INTEGRATION } from "../queries";
import type { IGroup } from "../types";
import type { FragmentType } from "gql/fragment-masking";
import { getFragmentData } from "gql/fragment-masking";
import type { GetAzureIntegrationFragment } from "gql/graphql";
import { GroupIntegration } from "gql/graphql";
import { useStoredState } from "hooks/use-stored-state";

type TGroups = GetAzureIntegrationFragment["me"]["organizations"][0]["groups"];

interface IAzureDevOpsProps {
  readonly data: FragmentType<typeof GET_AZURE_INTEGRATION>;
}

const AzureDevOps: React.FC<IAzureDevOpsProps> = ({
  data,
}): React.JSX.Element => {
  const { t } = useTranslation();

  const fragmentData = getFragmentData(GET_AZURE_INTEGRATION, data);
  const { organizations } = fragmentData.me;
  const groups = organizations.flatMap((org): TGroups => org.groups);
  const connectedGroups = groups.filter(
    (group): boolean => group.azureIssuesIntegration !== null,
  );

  const [selectedGroupName, setSelectedGroupName] = useStoredState(
    "azureConfigGroup",
    "",
    localStorage,
  );
  const selectedGroup = groups.find(
    (group): boolean => group.name === selectedGroupName,
  );

  const groupSelectionModal = useModal("azure-group-selection-modal");
  const openGroupSelectionModal = useCallback((): void => {
    groupSelectionModal.open();
  }, [groupSelectionModal]);

  const configModal = useModal("azure-config-modal");
  const openConfigModal = useCallback(
    (groupName: string): void => {
      setSelectedGroupName(groupName);
      configModal.open();
    },
    [configModal, setSelectedGroupName],
  );

  const infoModal = useModal("azure-info-modal");
  const openInfoModal = useCallback(
    (groupName: string): void => {
      setSelectedGroupName(groupName);
      infoModal.open();
    },
    [infoModal, setSelectedGroupName],
  );

  const { confirm, ConfirmDialog } = useConfirmDialog();
  const [removeGroupIntegration] = useMutation(REMOVE_GROUP_INTEGRATION);
  const disconnectIntegration = useCallback(
    async (groupName: string): Promise<void> => {
      const confirmResult = await confirm({
        title: t("integrations.disconnect.confirm"),
      });

      if (confirmResult) {
        await removeGroupIntegration({
          refetchQueries: [GET_INTEGRATIONS],
          variables: { groupName, integration: GroupIntegration.AzureDevops },
        });
        mixpanel.track("RemoveAzureIntegration");
      }
    },
    [confirm, removeGroupIntegration, t],
  );

  return (
    <React.Fragment>
      <IntegrationCard
        description={t("integrations.azure.description")}
        docsLink={
          "https://help.fluidattacks.com/portal/en/kb/articles/set-up-the-azure-devops-integration"
        }
        groupsSummary={{
          connected: connectedGroups.length,
          total: groups.length,
        }}
        iconPath={"integrates/integrations/icon-azure"}
        name={"Azure DevOps"}
        onClickConfig={openGroupSelectionModal}
      />
      {selectedGroup ? (
        <ConfigAzureDevOps
          modalRef={configModal}
          selectedGroup={selectedGroup}
        />
      ) : undefined}
      <ConfirmDialog />
      <IntegrationGroupSelection
        groups={groups.map((group): IGroup => {
          return {
            connected: group.azureIssuesIntegration !== null,
            description: group.description ?? "",
            name: group.name,
          };
        })}
        isFirstTime={connectedGroups.length === 0}
        modalRef={groupSelectionModal}
        name={"Azure DevOps"}
        onClickConnect={openConfigModal}
        onClickDisconnect={disconnectIntegration}
        onClickEdit={openConfigModal}
        onClickInfo={openInfoModal}
      />
      <IntegrationInfo
        connectionDate={selectedGroup?.azureIssuesIntegration?.connectionDate}
        iconPath={"integrates/integrations/icon-azure"}
        modalRef={infoModal}
        name={"Azure DevOps"}
        permissions={[
          {
            description: t("integrations.azure.permissions.wItems.description"),
            title: t("integrations.azure.permissions.wItems.title"),
          },
        ]}
      />
    </React.Fragment>
  );
};

export { AzureDevOps };
