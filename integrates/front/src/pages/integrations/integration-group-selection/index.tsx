import {
  Button,
  Container,
  Heading,
  IconButton,
  Modal,
  Text,
} from "@fluidattacks/design";
import type { IUseModal } from "@fluidattacks/design";
import { useCallback, useState } from "react";
import * as React from "react";
import { useTranslation } from "react-i18next";
import { styled, useTheme } from "styled-components";

import type { IGroup } from "../types";
import { Search } from "components/search";

const GroupsContainer = styled(Container)`
  > div:not(:last-child) {
    border-bottom: 1px solid;
    border-color: ${({ theme }): string => theme.palette.gray[200]};
  }
`;

interface IIntegrationGroupSelection {
  readonly groups: IGroup[];
  readonly isFirstTime: boolean;
  readonly modalRef: IUseModal;
  readonly name: string;
  readonly onClickConnect: (groupName: string) => void;
  readonly onClickDisconnect?: (groupName: string) => void;
  readonly onClickEdit: (groupName: string) => void;
  readonly onClickInfo?: (groupName: string) => void;
}

const IntegrationGroupSelection: React.FC<IIntegrationGroupSelection> = ({
  groups,
  isFirstTime,
  modalRef,
  name,
  onClickConnect,
  onClickDisconnect,
  onClickEdit,
  onClickInfo,
}): React.JSX.Element => {
  const theme = useTheme();
  const { t } = useTranslation();

  const [search, setSearch] = useState("");
  const sortedGroups = groups.toSorted((groupA, groupB): number => {
    if (groupA.connected === groupB.connected) {
      return 0;
    }
    if (groupA.connected) {
      return -1;
    }

    return 1;
  });
  const filteredGroups = sortedGroups.filter((group): boolean => {
    return (
      search.trim() === "" ||
      group.name.toLowerCase().includes(search.toLowerCase())
    );
  });

  const handleClick = useCallback(
    (
      callback: (groupName: string) => void,
      groupName: string,
    ): React.MouseEventHandler<HTMLButtonElement> => {
      return (): void => {
        callback(groupName);
        modalRef.close();
      };
    },
    [modalRef],
  );

  return (
    <Modal
      description={
        isFirstTime
          ? t("integrations.groupSelection.description", { name })
          : undefined
      }
      modalRef={modalRef}
      size={"md"}
      title={t(
        `integrations.groupSelection.title.${isFirstTime ? "use" : "manage"}`,
        { name },
      )}
    >
      <Container pb={1.25}>
        <Search
          handleOnKeyPressed={setSearch}
          placeHolder={t("integrations.search")}
        />
      </Container>
      <GroupsContainer
        border={"1px solid"}
        borderColor={theme.palette.gray[200]}
        borderRadius={"8px"}
        display={"flex"}
        flexDirection={"column"}
        scroll={"y"}
      >
        {filteredGroups.map((group): JSX.Element => {
          return (
            <Container
              alignSelf={"stretch"}
              display={"flex"}
              gap={0.75}
              justify={"space-between"}
              key={group.name}
              px={1}
              py={0.5}
              wrap={"wrap"}
            >
              <Container
                display={"flex"}
                flexDirection={"column"}
                justify={"center"}
              >
                <Heading fontWeight={"bold"} size={"xs"}>
                  {group.name}
                </Heading>
                <Text size={"sm"}>{group.description}</Text>
              </Container>
              {group.connected ? (
                <Container display={"flex"} gap={0.5}>
                  {onClickInfo ? (
                    <IconButton
                      icon={"info-circle"}
                      iconSize={"xxs"}
                      iconType={"fa-light"}
                      onClick={handleClick(onClickInfo, group.name)}
                      variant={"ghost"}
                    />
                  ) : undefined}
                  <Button
                    icon={"pen-to-square"}
                    onClick={handleClick(onClickEdit, group.name)}
                    variant={"ghost"}
                  >
                    {t("integrations.groupSelection.edit")}
                  </Button>
                  {onClickDisconnect ? (
                    <Button
                      icon={"link-simple-slash"}
                      onClick={handleClick(onClickDisconnect, group.name)}
                      variant={"ghost"}
                    >
                      {t("integrations.groupSelection.disconnect")}
                    </Button>
                  ) : undefined}
                </Container>
              ) : (
                <Button
                  icon={"link-simple"}
                  onClick={handleClick(onClickConnect, group.name)}
                  variant={"secondary"}
                >
                  {t("integrations.groupSelection.connect")}
                </Button>
              )}
            </Container>
          );
        })}
      </GroupsContainer>
    </Modal>
  );
};

export { IntegrationGroupSelection };
