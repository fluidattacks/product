interface ITrustedDevice {
  browser: string;
  device: string;
  firstLoginDate: string | null;
  ipAddress: string;
  lastAttempt: string | null;
  lastLoginDate: string | null;
  location: string;
  otpTokenJti: string;
}

export type { ITrustedDevice };
