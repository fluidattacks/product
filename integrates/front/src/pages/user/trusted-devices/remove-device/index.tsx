import { Button } from "@fluidattacks/design";
import { useCallback } from "react";

import type { ITrustedDevice } from "../types";

const RenderDeleteButton = (
  handleDelete: (trustedDevice: ITrustedDevice) => Promise<void>,
  trustedDevice: ITrustedDevice,
): JSX.Element => {
  const handleClick = useCallback(async (): Promise<void> => {
    await handleDelete(trustedDevice);
  }, [handleDelete, trustedDevice]);

  return <Button icon={"trash"} onClick={handleClick} variant={"ghost"} />;
};

export { RenderDeleteButton };
