import { graphql } from "gql";

const GET_TRUSTED_DEVICES = graphql(`
  query GetTrustedDevices {
    me {
      userEmail
      trustedDevices {
        browser
        device
        firstLoginDate
        ipAddress
        lastAttempt
        lastLoginDate
        location
        otpTokenJti
      }
    }
  }
`);

const REMOVE_TRUSTED_DEVICES = graphql(`
  mutation RemoveTrustedDevice(
    $browser: String!
    $device: String!
    $otpTokenJti: String
  ) {
    removeTrustedDevice(
      trustedDevice: {
        device: $device
        browser: $browser
        otpTokenJti: $otpTokenJti
      }
    ) {
      success
    }
  }
`);

export { GET_TRUSTED_DEVICES, REMOVE_TRUSTED_DEVICES };
