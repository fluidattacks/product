import { screen, waitFor } from "@testing-library/react";
import { userEvent } from "@testing-library/user-event";
import { GraphQLError } from "graphql";
import type { StrictResponse } from "msw";
import { HttpResponse, graphql } from "msw";
import { Route, Routes } from "react-router-dom";

import { GET_TRUSTED_DEVICES, REMOVE_TRUSTED_DEVICES } from "./queries";

import { TrustedDevices } from ".";
import type {
  GetTrustedDevicesQuery as GetTrustedDevices,
  RemoveTrustedDeviceMutation as RemoveTrustedDevice,
} from "gql/graphql";
import { render } from "mocks";
import { LINK } from "mocks/constants";
import type { IErrorMessage } from "mocks/types";
import { msgSuccess } from "utils/notifications";

jest.mock("utils/notifications", (): Record<string, unknown> => {
  const mockedNotifications: Record<string, () => Record<string, unknown>> =
    jest.requireActual("utils/notifications");
  jest.spyOn(mockedNotifications, "msgSuccess").mockImplementation();

  return mockedNotifications;
});

describe("trustedDevices", (): void => {
  const graphqlMocked = graphql.link(LINK);
  const mocksTrustedDevices = graphqlMocked.query(
    GET_TRUSTED_DEVICES,
    (): StrictResponse<{ data: GetTrustedDevices }> => {
      return HttpResponse.json({
        data: {
          me: {
            trustedDevices: [
              {
                browser: "chrome",
                device: "ubuntu",
                firstLoginDate: "2024-02-22 17:39:59.247393+00:00",
                ipAddress: "192.168.1.1",
                lastAttempt: "2024-02-22 17:39:59.247393+00:00",
                lastLoginDate: "2024-02-22 17:39:59.247393+00:00",
                location: "Toronto",
                otpTokenJti: "xvc123",
              },
              {
                browser: "firefox",
                device: "mac",
                firstLoginDate: "2024-02-21 17:39:59.247393+00:00",
                ipAddress: "192.168.1.1",
                lastAttempt: "2024-02-21 17:39:59.247393+00:00",
                lastLoginDate: "2024-02-21 17:39:59.247393+00:00",
                location: "Toronto",
                otpTokenJti: "xvc123",
              },
            ],
            userEmail: "testing@test.com",
          },
        },
      });
    },
  );
  const removeTrustedDeviceMutation = graphqlMocked.mutation(
    REMOVE_TRUSTED_DEVICES,
    ({
      variables,
    }): StrictResponse<IErrorMessage | { data: RemoveTrustedDevice }> => {
      const { browser, device, otpTokenJti } = variables;
      if (
        browser === "chrome" &&
        device === "ubuntu" &&
        otpTokenJti === "xvc123"
      ) {
        return HttpResponse.json({
          data: {
            removeTrustedDevice: { success: true },
          },
        });
      }

      return HttpResponse.json({
        errors: [
          new GraphQLError(
            "Exception - Remove trusted device is not requested",
          ),
        ],
      });
    },
  );

  it("renders without error", async (): Promise<void> => {
    expect.hasAssertions();

    render(
      <Routes>
        <Route element={<TrustedDevices />} path={"/user/trusted-devices"} />
      </Routes>,
      {
        memoryRouter: {
          initialEntries: ["/user/trusted-devices"],
        },
        mocks: [mocksTrustedDevices],
      },
    );

    expect(screen.queryAllByRole("table")).toHaveLength(1);

    expect(screen.getByText("Device")).toBeInTheDocument();
    expect(screen.getByText("Location")).toBeInTheDocument();
    expect(screen.getByText("Browser")).toBeInTheDocument();
    expect(screen.getByText("First sign-in")).toBeInTheDocument();
    expect(screen.getByText("Recent activity")).toBeInTheDocument();

    await waitFor((): void => {
      expect(screen.getByText("ubuntu")).toBeInTheDocument();
    });

    expect(screen.getByText("chrome")).toBeInTheDocument();
    expect(screen.getByText("firefox")).toBeInTheDocument();
    expect(screen.getByText("mac")).toBeInTheDocument();
    expect(screen.getAllByText("Toronto")[0]).toBeInTheDocument();
  });

  it("should remove device", async (): Promise<void> => {
    expect.hasAssertions();

    jest.clearAllMocks();

    render(
      <Routes>
        <Route element={<TrustedDevices />} path={"/user/trusted-devices"} />
      </Routes>,
      {
        memoryRouter: {
          initialEntries: ["/user/trusted-devices"],
        },
        mocks: [mocksTrustedDevices, removeTrustedDeviceMutation],
      },
    );

    await waitFor((): void => {
      expect(screen.getByText("ubuntu")).toBeInTheDocument();
    });

    expect(screen.queryAllByRole("row")).toHaveLength(3);

    await userEvent.click(screen.getAllByRole("button")[0]);
    await userEvent.click(screen.getByRole("button", { name: "Confirm" }));

    await waitFor((): void => {
      expect(msgSuccess).toHaveBeenCalledTimes(1);
    });
  });
});
