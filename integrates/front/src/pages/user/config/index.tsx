import { useMutation, useQuery } from "@apollo/client";
import {
  CardWithSwitch,
  Col,
  Container,
  Row,
  Tooltip,
} from "@fluidattacks/design";
import { Formik } from "formik";
import _ from "lodash";
import mixpanel from "mixpanel-browser";
import { useCallback } from "react";
import * as React from "react";
import { useTranslation } from "react-i18next";
import { useTheme } from "styled-components";

import { CardWithInput } from "components/card/card-with-input";
import { SectionHeader } from "components/section-header";
import type { NotificationsName } from "gql/graphql";
import {
  GET_SUBSCRIPTIONS,
  UPDATE_NOTIFICATIONS_PREFERENCES,
} from "pages/user/config/queries";
import type {
  IFormData,
  INotificationsPreferences,
  ISubscriptionName,
} from "pages/user/config/types";
import { Logger } from "utils/logger";
import { msgError } from "utils/notifications";

const UserConfig: React.FC = (): JSX.Element => {
  const { t } = useTranslation();
  const theme = useTheme();
  const { data: dataEnum, refetch } = useQuery(GET_SUBSCRIPTIONS, {
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error): void => {
        msgError(t("configuration.errorText"));
        Logger.warning(
          "An error occurred loading the subscriptions info",
          error,
        );
      });
    },
  });

  const [updateSubscription] = useMutation(UPDATE_NOTIFICATIONS_PREFERENCES, {
    onCompleted: (): void => {
      mixpanel.track("UpdateNotificationsPreferences");
      void refetch();
    },
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach(({ message }): void => {
        msgError(t("configuration.errorText"));
        Logger.warning(
          "An error occurred changing the subscriptions info",
          message,
        );
      });
    },
  });

  const handleSubmit = useCallback(
    (email: NotificationsName[], severity: number): void => {
      void updateSubscription({
        variables: {
          email,
          severity,
        },
      });
    },
    [updateSubscription],
  );

  const onChangeSeverity = useCallback(
    (
      listSubscription: INotificationsPreferences,
    ): ((newValue: number | undefined) => void) => {
      return (newValue: number | undefined): void => {
        if (!_.isUndefined(newValue)) {
          handleSubmit(listSubscription.email as NotificationsName[], newValue);
        }
      };
    },
    [handleSubmit],
  );

  // eslint-disable-next-line @typescript-eslint/no-empty-function
  const onSubmit = useCallback((__: IFormData): void => {}, []);

  const subscriptions =
    _.isUndefined(dataEnum) || _.isEmpty(dataEnum)
      ? []
      : dataEnum.Notifications?.enumValues?.map(
          (subscription): ISubscriptionName => {
            const listSubscription = dataEnum.me.notificationsPreferences;
            const severity =
              dataEnum.me.notificationsPreferences.parameters?.minSeverity ?? 0;

            const isSubscribe = (sub: string[] | string): boolean =>
              sub.includes(subscription.name);

            const newListSubs = (listSub: string[]): string[] =>
              isSubscribe(listSub)
                ? listSub.filter((sub): boolean => !isSubscribe(sub))
                : [subscription.name, ...listSub];

            const onChangeMail = (listSub: string[]): VoidFunction => {
              return (): void => {
                const newListEmailSubs = newListSubs(listSub);
                handleSubmit(newListEmailSubs as NotificationsName[], severity);
              };
            };

            return {
              minSeverity: (
                <Formik
                  initialValues={{ minimumSeverity: severity }}
                  name={"minimumSeverityForm"}
                  onSubmit={onSubmit}
                >
                  <CardWithInput
                    inputAlign={"start"}
                    inputNumberProps={{
                      decimalPlaces: 1,
                      max: 10,
                      min: 0,
                      updateStoredData: onChangeSeverity(
                        listSubscription as INotificationsPreferences,
                      ),
                    }}
                    inputType={"number"}
                    minHeight={"100%"}
                    name={"minimumSeverity"}
                    placeholder={"Min"}
                    title={t(
                      "searchFindings.notificationTable.parameters.minimumSeverity.name",
                    )}
                  />
                </Formik>
              ),
              name: t(`searchFindings.enumValues.${subscription.name}.name`),
              toggles: [
                {
                  defaultChecked: isSubscribe(listSubscription.email),
                  justify: "space-between",
                  leftDescription: t("searchFindings.notificationTable.email"),
                  name: `${t(
                    `searchFindings.enumValues.${subscription.name}.name`,
                  )}_EMAIL`,
                  onChange: onChangeMail(listSubscription.email),
                },
              ],
              tooltip: t(
                `searchFindings.enumValues.${subscription.name}.tooltip`,
              ),
            };
          },
        );

  const defaultExceptions = ["ACCESS_GRANTED", "GROUP_REPORT"];

  const exceptions =
    _.isUndefined(dataEnum) || _.isEmpty(dataEnum)
      ? defaultExceptions
      : [
          ...defaultExceptions,
          dataEnum.me.userEmail.endsWith("@fluidattacks.com")
            ? undefined
            : "Draft updates",
        ];

  const filterByName = (subscription: ISubscriptionName): boolean =>
    !exceptions.includes(subscription.name);

  const subscriptionsFiltered = subscriptions?.filter(filterByName);

  return (
    <React.StrictMode>
      <SectionHeader header={t("searchFindings.notificationTable.title")} />
      <Container
        bgColor={theme.palette.gray[50]}
        height={"100%"}
        id={"notifications"}
        px={1.25}
        py={1.25}
      >
        <Row colsPadding={3}>
          {subscriptionsFiltered?.map(
            (item): JSX.Element => (
              <React.Fragment key={item.name}>
                <React.Fragment>
                  <Col lg={33} md={50} sm={100} xl={25}>
                    <Tooltip
                      id={`${item.name.toLowerCase().replace(" ", "_")}_tooltip`}
                      place={"bottom"}
                      tip={item.tooltip}
                      width={"100%"}
                    >
                      <CardWithSwitch
                        height={"100%"}
                        id={item.name}
                        title={item.name}
                        toggles={item.toggles}
                      />
                    </Tooltip>
                  </Col>
                  {item.name ===
                    t(
                      "searchFindings.enumValues.VULNERABILITY_REPORT.name",
                    ) && (
                    <Col key={item.name} lg={33} md={50} sm={100} xl={25}>
                      <Tooltip
                        height={"100%"}
                        id={`${t(
                          "searchFindings.notificationTable.parameters.minimumSeverity.name",
                        )
                          .toLowerCase()
                          .replace(" ", "_")}_tooltip`}
                        tip={t(
                          "searchFindings.notificationTable.parameters.minimumSeverity.tooltip",
                        )}
                        width={"100%"}
                      >
                        {item.minSeverity}
                      </Tooltip>
                    </Col>
                  )}
                </React.Fragment>
              </React.Fragment>
            ),
          )}
        </Row>
      </Container>
    </React.StrictMode>
  );
};

export { UserConfig };
