import { screen, waitFor } from "@testing-library/react";
import { HttpResponse, type StrictResponse, graphql } from "msw";
import { Route, Routes } from "react-router-dom";

import { UserConfig } from ".";
import { NotificationsName } from "gql/graphql";
import type {
  GetSubscriptionsQuery as GetSubscriptions,
  UpdateNotificationsPreferencesMutation as UpdateNotificationsPreferences,
} from "gql/graphql";
import { render } from "mocks";
import { LINK } from "mocks/constants";
import {
  GET_SUBSCRIPTIONS,
  UPDATE_NOTIFICATIONS_PREFERENCES,
} from "pages/user/config/queries";

describe("userConfig", (): void => {
  const graphqlMocked = graphql.link(LINK);

  it("should render notification matrix showing active and inactive subscriptions", async (): Promise<void> => {
    expect.hasAssertions();

    render(
      <Routes>
        <Route element={<UserConfig />} path={"/user/config"} />
      </Routes>,
      {
        memoryRouter: {
          initialEntries: ["/user/config"],
        },
        mocks: [
          graphqlMocked.mutation(
            UPDATE_NOTIFICATIONS_PREFERENCES,
            (): StrictResponse<{ data: UpdateNotificationsPreferences }> => {
              return HttpResponse.json({
                data: {
                  updateNotificationsPreferences: { success: true },
                },
              });
            },
          ),
          graphqlMocked.query(
            GET_SUBSCRIPTIONS,
            (): StrictResponse<{ data: GetSubscriptions }> => {
              return HttpResponse.json({
                data: {
                  Notifications: {
                    enumValues: [
                      {
                        name: "ACCESS_GRANTED",
                      },
                      {
                        name: "AGENT_TOKEN",
                      },
                      {
                        name: "EVENT_DIGEST",
                      },
                      {
                        name: "EVENT_REPORT",
                      },
                      {
                        name: "FILE_UPDATE",
                      },
                      {
                        name: "GROUP_INFORMATION",
                      },
                      {
                        name: "GROUP_REPORT",
                      },
                      {
                        name: "NEWSLETTER",
                      },
                      {
                        name: "NEW_COMMENT",
                      },
                      {
                        name: "NEW_DRAFT",
                      },
                      {
                        name: "PORTFOLIO_UPDATE",
                      },
                      {
                        name: "REMEDIATE_FINDING",
                      },
                      {
                        name: "REMINDER_NOTIFICATION",
                      },
                      {
                        name: "ROOT_UPDATE",
                      },
                      {
                        name: "SERVICE_UPDATE",
                      },
                      {
                        name: "UNSUBSCRIPTION_ALERT",
                      },
                      {
                        name: "UPDATED_TREATMENT",
                      },
                      {
                        name: "VULNERABILITY_ASSIGNED",
                      },
                      {
                        name: "VULNERABILITY_REPORT",
                      },
                    ],
                  },
                  me: {
                    __typename: "Me",
                    notificationsPreferences: {
                      email: [
                        NotificationsName.NewComment,
                        NotificationsName.ServiceUpdate,
                        NotificationsName.RootUpdate,
                        NotificationsName.UpdatedTreatment,
                        NotificationsName.VulnerabilityReport,
                      ],
                      parameters: {
                        minSeverity: 3,
                      },
                    },
                    userEmail: "unitesting@fluidattacks.com",
                  },
                },
              });
            },
          ),
        ],
      },
    );

    await waitFor((): void => {
      expect(
        screen.queryByText("searchFindings.enumValues.AGENT_TOKEN.name"),
      ).toBeInTheDocument();
    });

    await waitFor((): void => {
      const checkedCheckboxes = screen.getAllByRole("checkbox", {
        checked: true,
      });

      expect(checkedCheckboxes).toHaveLength(5);
    });

    await waitFor((): void => {
      expect(
        screen.queryByText(
          "searchFindings.notificationTable.parameters.minimumSeverity.name",
        ),
      ).toBeInTheDocument();
    });

    await waitFor((): void => {
      expect(
        screen.getAllByText("searchFindings.notificationTable.email"),
      ).toHaveLength(19);
    });
  });
});
