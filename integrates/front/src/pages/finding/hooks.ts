import { useQuery } from "@apollo/client";
import { useAbility } from "@casl/react";
import { useContext } from "react";
import { useTranslation } from "react-i18next";
import { useParams } from "react-router-dom";

import { GET_FINDING_HEADER } from "./queries";

import { authContext } from "context/auth";
import { authzPermissionsContext } from "context/authz/config";
import type { GetFindingHeaderQuery } from "gql/graphql";
import { ManagedType } from "gql/graphql";
import { useAudit } from "hooks/use-audit";
import { GET_GROUP_DATA } from "pages/group/queries";
import { Logger } from "utils/logger";
import { msgError } from "utils/notifications";

const useFindingHeaderQuery = (
  findingId: string,
): GetFindingHeaderQuery | undefined => {
  const permissions = useAbility(authzPermissionsContext);
  const { addAuditEvent } = useAudit();
  const { t } = useTranslation();

  const { data } = useQuery(GET_FINDING_HEADER, {
    onCompleted: (): void => {
      addAuditEvent("Finding", findingId);
    },
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error): void => {
        msgError(t("groupAlerts.errorTextsad"));
        Logger.warning("An error occurred loading finding header", error);
      });
    },
    variables: {
      canGetZRSummary: permissions.can(
        "integrates_api_resolvers_finding_zero_risk_summary_resolve",
      ),
      canRetrieveHacker: permissions.can(
        "integrates_api_resolvers_finding_hacker_resolve",
      ),
      findingId,
    },
  });

  return data;
};

const useIsGroupUnderReview = (): boolean | undefined => {
  const { t } = useTranslation();
  const { userEmail } = useContext(authContext);
  const { groupName, organizationName } = useParams() as {
    groupName: string;
    organizationName: string;
  };

  const { data } = useQuery(GET_GROUP_DATA, {
    fetchPolicy: "cache-first",
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((groupError): void => {
        msgError(t("groupAlerts.errorTextsad"));
        Logger.warning("An error occurred group data", groupError);
      });
    },
    variables: { groupName, organizationName },
  });

  if (data === undefined) {
    return undefined;
  }

  return (
    data.group.managed === ManagedType.UnderReview &&
    !userEmail.endsWith("@fluidattacks.com")
  );
};

export { useFindingHeaderQuery, useIsGroupUnderReview };
