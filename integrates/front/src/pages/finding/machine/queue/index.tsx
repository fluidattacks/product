/* eslint-disable react/jsx-no-bind */
import { Checkbox, Form, InnerForm } from "@fluidattacks/design";
import { Fragment, useCallback, useState } from "react";
import * as React from "react";
import { useTranslation } from "react-i18next";

import type { IQueue } from "../types";

interface IFormValues {
  checkAll: boolean;
  selectedRoots: string[];
}

const Queue: React.FC<IQueue> = (props: Readonly<IQueue>): JSX.Element => {
  const { t } = useTranslation();
  const { rootNicknames, onClose, onSubmit } = props;

  const [isJobSubmitted, setIsJobSubmitted] = useState(false);

  const initialValues: IFormValues = {
    checkAll: false,
    selectedRoots: [],
  };

  const handleSubmit = useCallback(
    async (values: IFormValues): Promise<void> => {
      setIsJobSubmitted(true);
      await onSubmit(values.selectedRoots);
      onClose();
    },
    [onClose, onSubmit],
  );

  return (
    <Form
      cancelButton={{ onClick: onClose }}
      confirmButton={{
        disabled: isJobSubmitted,
      }}
      defaultValues={initialValues}
      id={"machine-jobs"}
      onSubmit={handleSubmit}
    >
      <InnerForm<IFormValues>>
        {({ register, setValue, watch }): JSX.Element => {
          const selectedRoots = watch("selectedRoots", []);
          const checkAll = watch("checkAll", false);

          const handleCheckAll = (): void => {
            const newSelectedRoots = checkAll ? [] : [...rootNicknames];
            setValue("selectedRoots", newSelectedRoots, { shouldDirty: true });
            setValue("checkAll", !checkAll, { shouldDirty: true });
          };

          const onCheck = (root: string): void => {
            const newSelectedRoots = selectedRoots.includes(root)
              ? selectedRoots.filter(
                  (selectedRoot): boolean => selectedRoot !== root,
                )
              : [...selectedRoots, root];

            setValue("selectedRoots", newSelectedRoots, { shouldDirty: true });
            if (newSelectedRoots.length !== rootNicknames.length) {
              setValue("checkAll", false, { shouldDirty: true });
            }
          };

          return (
            <Fragment>
              <Checkbox
                label={t("searchFindings.tabMachine.checkAll")}
                {...register("checkAll")}
                onChange={handleCheckAll}
              />
              <br />
              {rootNicknames.map((root): JSX.Element => {
                return (
                  <Checkbox
                    checked={selectedRoots.includes(root)}
                    key={root}
                    label={root}
                    name={`selectedRoots.${root}`}
                    onChange={(): void => {
                      onCheck(root);
                    }}
                  />
                );
              })}
            </Fragment>
          );
        }}
      </InnerForm>
    </Form>
  );
};

export { Queue };
