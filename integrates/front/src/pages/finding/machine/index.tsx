import { NetworkStatus, useMutation, useQuery } from "@apollo/client";
import { Button, ButtonToolbarCenter } from "@fluidattacks/design";
import _ from "lodash";
import { Fragment, StrictMode, useCallback, useEffect } from "react";
import React from "react";
import { useTranslation } from "react-i18next";
import { useParams } from "react-router-dom";

import { GET_ROOTS, SUBMIT_MACHINE_JOB } from "./queries";
import { Queue } from "./queue";

import { Modal } from "components/modal";
import { ResourceState } from "gql/graphql";
import { useAudit } from "hooks/use-audit";
import { useModal } from "hooks/use-modal";
import { Logger } from "utils/logger";
import { msgError, msgSuccess } from "utils/notifications";

const FindingMachine: React.FC = (): JSX.Element => {
  const { t } = useTranslation();
  const { findingId, groupName } = useParams() as {
    findingId: string;
    groupName: string;
  };
  const modalProps = useModal("finding-machine");
  const { close, open } = modalProps;

  const { addAuditEvent } = useAudit();
  useEffect((): void => {
    addAuditEvent("Finding.Machine", findingId);
  }, [addAuditEvent, findingId]);

  const { data: dataRoots, networkStatus: dataNS } = useQuery(GET_ROOTS, {
    fetchPolicy: "no-cache",
    notifyOnNetworkStatusChange: true,
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error): void => {
        msgError(t("groupAlerts.errorTextsad"));
        Logger.warning("An error occurred loading roots", error);
      });
    },
    variables: { groupName },
  });

  const [submitMachineJob, { loading: submittingMachineJob }] = useMutation(
    SUBMIT_MACHINE_JOB,
    {
      onCompleted: (result): void => {
        if (!_.isUndefined(result)) {
          if (result.submitMachineJob.success) {
            msgSuccess(
              t("searchFindings.tabMachine.submitJobSuccess"),
              t("searchFindings.tabMachine.success"),
            );
          } else {
            msgError(
              t(
                result.submitMachineJob.message ??
                  "searchFindings.tabMachine.errorNoCheck",
              ),
            );
          }
        }
      },
      onError: ({ graphQLErrors }): void => {
        graphQLErrors.forEach((error): void => {
          switch (error.message) {
            case "Exception - Access denied or credential not found":
              msgError(t("group.scope.git.sync.noCredentials"));
              break;
            case "Exception - There is already a Machine execution queued with the same parameters":
              msgError(t("group.machine.alreadyQueued"));
              break;
            default:
              Logger.warning("An error occurred submitting job", error);
              msgError(t("groupAlerts.errorTextsad"));
          }
        });
      },
    },
  );

  const isLoading = submittingMachineJob || dataNS === NetworkStatus.refetch;
  const submitJobOnClick = useCallback(
    async (roots: string[]): Promise<void> => {
      await submitMachineJob({
        variables: { findingId, rootNicknames: roots },
      });
    },
    [findingId, submitMachineJob],
  );

  if (_.isUndefined(dataRoots) || _.isEmpty(dataRoots)) {
    return <div />;
  }

  const rootNicknamesSorted = _.isUndefined(dataRoots)
    ? []
    : _.sortBy(dataRoots.group.roots, [
        (root): string => (root.nickname || "").toLowerCase(),
      ]);
  const rootNicknames = rootNicknamesSorted
    .filter((root): boolean => root.state === ResourceState.Active)
    .map((root): string => root.nickname);

  return (
    <StrictMode>
      {_.isUndefined(dataRoots) || _.isEmpty(dataRoots) ? (
        <div />
      ) : (
        <Fragment>
          <ButtonToolbarCenter>
            <Button
              disabled={isLoading}
              icon={isLoading ? "clock" : "rocket"}
              id={"submitJob"}
              justify={"center"}
              onClick={open}
              width={"15%"}
            >
              {t("searchFindings.tabMachine.submitJob")}
            </Button>
          </ButtonToolbarCenter>
          <Modal
            description={t("searchFindings.tabMachine.subtitle")}
            modalRef={modalProps}
            size={"sm"}
            title={t("searchFindings.tabMachine.title")}
          >
            <Queue
              onClose={close}
              onSubmit={submitJobOnClick}
              rootNicknames={rootNicknames}
            />
          </Modal>
        </Fragment>
      )}
    </StrictMode>
  );
};

export { FindingMachine };
