import { screen, waitFor } from "@testing-library/react";
import { Route, Routes } from "react-router-dom";

import { render } from "mocks";
import { FindingTracking } from "pages/finding/tracking";

describe("findingTracking", (): void => {
  it("should render", async (): Promise<void> => {
    expect.hasAssertions();

    render(
      <Routes>
        <Route
          element={<FindingTracking />}
          path={
            "/orgs/:organizationName/groups/:groupName/vulns/:findingId/tracking"
          }
        />
      </Routes>,
      {
        memoryRouter: {
          initialEntries: ["/orgs/aorg/groups/agroup/vulns/422286126/tracking"],
        },
      },
    );

    await waitFor((): void => {
      expect(screen.queryByText("Cycle: 1")).toBeInTheDocument();
    });

    expect(screen.queryByText("2019-01-08")).toBeInTheDocument();

    expect(
      screen.queryByText("test justification accepted treatment"),
    ).toBeInTheDocument();
    expect(screen.queryByText("Assigned: test@test.test")).toBeInTheDocument();
    expect(screen.queryByText("Found")).toBeInTheDocument();
    expect(screen.queryByText("Vulnerabilities found: 1")).toBeInTheDocument();
  });
});
