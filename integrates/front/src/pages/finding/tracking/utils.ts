import isEmpty from "lodash/isEmpty";

import type { ITracking } from "./types";

import { translate } from "utils/translations/translate";

const getTiTle = (cycle: number): string =>
  ({
    false: translate.t("searchFindings.tabTracking.found"),
    true: translate.t("searchFindings.tabTracking.cycle", { cycle }),
  })[String(cycle > 0)] as string;

const getVulnerabilitiesFound = (tracking: ITracking): string | undefined => {
  if (tracking.vulnerable > 0)
    return translate.t("searchFindings.tabTracking.vulnerabilitiesFound", {
      count: tracking.vulnerable,
    });

  return undefined;
};

const getSafeVulnerabilities = (tracking: ITracking): string | undefined => {
  if (tracking.safe > 0)
    return translate.t("searchFindings.tabTracking.vulnerabilitiesClosed", {
      count: tracking.safe,
    });

  return undefined;
};

const getJustification = (tracking: ITracking): string | undefined => {
  if (!isEmpty(tracking.justification))
    return translate.t("searchFindings.tabTracking.justification", {
      justification: tracking.justification,
    });

  return undefined;
};

const getAssigned = (tracking: ITracking): string | undefined => {
  if (!isEmpty(tracking.assigned))
    return translate.t("searchFindings.tabTracking.assigned", {
      assigned: tracking.assigned,
    });

  return undefined;
};

const getAcceptedTreatment = (tracking: ITracking): string | undefined => {
  if (tracking.accepted > 0)
    return translate.t(
      "searchFindings.tabTracking.vulnerabilitiesAcceptedTreatment",
      {
        count: tracking.accepted,
      },
    );

  return undefined;
};

const getAcceptedUndefinedTreatment = (
  tracking: ITracking,
): string | undefined => {
  if (
    tracking.acceptedUndefined !== undefined &&
    tracking.acceptedUndefined > 0
  )
    return translate.t(
      "searchFindings.tabTracking.vulnerabilitiesAcceptedUndefinedTreatment",
      {
        count: tracking.acceptedUndefined,
      },
    );

  return undefined;
};

const getTrackingInfo = (tracking: ITracking): string[] => {
  const vulnerableAndSafe = [
    getVulnerabilitiesFound(tracking),
    getSafeVulnerabilities(tracking),
  ];
  if (tracking.cycle === 0) {
    return vulnerableAndSafe.filter(
      (info): boolean => info !== undefined,
    ) as string[];
  }

  if (tracking.accepted > 0) {
    return [
      ...vulnerableAndSafe,
      getAcceptedTreatment(tracking),
      getJustification(tracking),
      getAssigned(tracking),
    ].filter((info): boolean => info !== undefined) as string[];
  } else if (
    tracking.acceptedUndefined !== undefined &&
    tracking.acceptedUndefined > 0
  ) {
    return [
      ...vulnerableAndSafe,
      getAcceptedUndefinedTreatment(tracking),
      getJustification(tracking),
      getAssigned(tracking),
    ].filter((info): boolean => info !== undefined) as string[];
  }

  return vulnerableAndSafe.filter(
    (info): boolean => info !== undefined,
  ) as string[];
};

export { getTrackingInfo, getTiTle };
