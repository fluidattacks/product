interface ITracking {
  accepted: number;
  acceptedUndefined?: number;
  assigned?: string | null;
  safe: number;
  cycle: number;
  date: string;
  justification?: string | null;
  vulnerable: number;
}

export type { ITracking };
