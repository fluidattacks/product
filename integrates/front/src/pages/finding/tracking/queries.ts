import { graphql } from "gql";

export const GET_FINDING_TRACKING = graphql(`
  query GetFindingTracking($findingId: String!) {
    finding(identifier: $findingId) {
      id
      tracking {
        accepted
        acceptedUndefined
        assigned
        cycle
        date
        justification
        safe
        vulnerable
      }
    }
  }
`);
