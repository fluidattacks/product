import { graphql } from "gql";

const GET_FINDING_HEADER = graphql(`
  query GetFindingHeader(
    $canGetZRSummary: Boolean! = false
    $canRetrieveHacker: Boolean! = false
    $findingId: String!
  ) {
    finding(identifier: $findingId) {
      id
      currentState
      hacker @include(if: $canRetrieveHacker)

      maxOpenSeverityScore
      maxOpenSeverityScoreV4
      minTimeToRemediate
      releaseDate
      status
      title

      vulnerabilitiesSummary {
        closed
        open
        openCritical
        openHigh
        openLow
        openMedium
      }
      zeroRiskSummary @include(if: $canGetZRSummary) {
        confirmed
        requested
      }
    }
  }
`);

const REMOVE_FINDING_MUTATION = graphql(`
  mutation RemoveFindingMutation(
    $findingId: String!
    $justification: RemoveFindingJustification!
  ) {
    removeFinding(findingId: $findingId, justification: $justification) {
      success
    }
  }
`);

export { GET_FINDING_HEADER, REMOVE_FINDING_MUTATION };
