import type { FetchResult } from "@apollo/client";
import _ from "lodash";
import type { InferType, Schema } from "yup";
import { mixed, object } from "yup";

import type { IEvidenceItem, IGetFindingEvidences } from "./types";

import { translate } from "utils/translations/translate";

const formatEvidenceImages = (
  evidences: IGetFindingEvidences["finding"]["evidence"],
): Record<string, IEvidenceItem> => {
  const formatted = Object.entries(evidences)
    .map(([key, value]): [string, IEvidenceItem] => {
      return [
        key,
        {
          authorEmail: value.authorEmail ?? "",
          date: value.date ?? "",
          description: value.description ?? "",
          isDraft: value.isDraft ?? false,
          url: value.url ?? "",
        },
      ];
    })
    .filter(([key]): boolean => key !== "__typename");

  return Object.fromEntries(formatted);
};

const formatEvidenceList = (
  evidenceImages: Record<string, IEvidenceItem>,
  isEditing: boolean,
  meetingMode: boolean,
): string[] => {
  const keys = _.uniq([
    "animation",
    "exploitation",
    ...Object.keys(evidenceImages),
  ]);

  return keys.filter((name): boolean => {
    const evidence = evidenceImages[name];

    if (evidence.isDraft && meetingMode) {
      return false;
    }

    if (_.isEmpty(evidence.url)) {
      return isEditing;
    }

    return true;
  });
};

const setPrefix = (name: string): string => {
  if (name === "animation") {
    return translate.t("searchFindings.tabEvidence.animationExploit");
  }

  return name === "exploitation"
    ? translate.t("searchFindings.tabEvidence.evidenceExploit")
    : "";
};

const setAltDescription = (prefix: string, evidence: IEvidenceItem): string => {
  return prefix !== "" && evidence.description !== ""
    ? `${prefix}: ${evidence.description}`
    : `${prefix}${evidence.description}`;
};

const updateChangesHelper = async (
  updateEvidence: (
    variables: Record<string, unknown>,
  ) => Promise<FetchResult<unknown>>,
  updateDescription: (
    variables: Record<string, unknown>,
  ) => Promise<FetchResult<unknown>>,
  file: FileList | undefined,
  key: string,
  description: string,
  findingId: string,
  descriptionChanged: boolean,
): Promise<void> => {
  if (file !== undefined) {
    const mtResult = await updateEvidence({
      variables: {
        evidenceId: key.toUpperCase(),
        file: file[0],
        findingId,
      },
    });
    const { success } = (
      mtResult as {
        data: { updateEvidence: { success: boolean } };
      }
    ).data.updateEvidence;

    if (success && descriptionChanged) {
      await updateDescription({
        variables: {
          description,
          evidenceId: key.toUpperCase(),
          findingId,
        },
      });
    }
  } else if (descriptionChanged) {
    await updateDescription({
      variables: {
        description,
        evidenceId: key.toUpperCase(),
        findingId,
      },
    });
  }
};

const MAX_FILE_SIZE = 40;
const findingEvidenceSchema = (
  organizationName: string,
  groupName: string,
  evidenceImages: Record<string, IEvidenceItem>,
): InferType<Schema> => {
  const imageSchema = Object.keys(evidenceImages).reduce(
    (obj, key): Record<string, Record<string, Schema>> => {
      const fileSchema: Schema = mixed<FileList>()
        .isValidFileSize(undefined, MAX_FILE_SIZE)
        .isValidFileName(groupName, organizationName)
        .isValidFileType(
          ["png", "webm"],
          translate.t("group.events.form.wrongImageType"),
        );

      return { ...obj, [key]: object().shape({ file: fileSchema }) };
    },
    {},
  );

  return object().shape(imageSchema);
};

export {
  formatEvidenceImages,
  findingEvidenceSchema,
  formatEvidenceList,
  setAltDescription,
  setPrefix,
  updateChangesHelper,
};
