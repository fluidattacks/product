import { graphql } from "gql";

const GET_FINDING_EVIDENCES = graphql(`
  query GetFindingEvidences($findingId: String!) {
    finding(identifier: $findingId) {
      id
      evidence {
        animation {
          authorEmail
          date
          description
          isDraft
          url
        }
        evidence1 {
          authorEmail
          date
          description
          isDraft
          url
        }
        evidence2 {
          authorEmail
          date
          description
          isDraft
          url
        }
        evidence3 {
          authorEmail
          date
          description
          isDraft
          url
        }
        evidence4 {
          authorEmail
          date
          description
          isDraft
          url
        }
        evidence5 {
          authorEmail
          date
          description
          isDraft
          url
        }
        exploitation {
          authorEmail
          date
          description
          isDraft
          url
        }
      }
    }
  }
`);

const UPDATE_EVIDENCE_MUTATION = graphql(`
  mutation UpdateEvidenceMutation(
    $evidenceId: EvidenceType!
    $file: Upload!
    $findingId: String!
  ) {
    updateEvidence(
      evidenceId: $evidenceId
      file: $file
      findingId: $findingId
    ) {
      success
    }
  }
`);

const APPROVE_EVIDENCE_MUTATION = graphql(`
  mutation ApproveEvidenceMutation(
    $evidenceId: EvidenceDescriptionType!
    $findingId: String!
  ) {
    approveEvidence(evidenceId: $evidenceId, findingId: $findingId) {
      success
    }
  }
`);

const REJECT_EVIDENCE_MUTATION = graphql(`
  mutation RejectEvidenceMutation(
    $evidenceId: EvidenceDescriptionType!
    $findingId: String!
    $justification: String!
  ) {
    rejectEvidence(
      evidenceId: $evidenceId
      findingId: $findingId
      justification: $justification
    ) {
      success
    }
  }
`);

const UPDATE_DESCRIPTION_MUTATION = graphql(`
  mutation UpdateDescriptionMutation(
    $description: String!
    $evidenceId: EvidenceDescriptionType!
    $findingId: String!
  ) {
    updateEvidenceDescription(
      description: $description
      evidenceId: $evidenceId
      findingId: $findingId
    ) {
      success
    }
  }
`);

const REMOVE_EVIDENCE_MUTATION = graphql(`
  mutation RemoveEvidenceMutation(
    $evidenceId: EvidenceType!
    $findingId: String!
  ) {
    removeEvidence(evidenceId: $evidenceId, findingId: $findingId) {
      success
    }
  }
`);

export {
  APPROVE_EVIDENCE_MUTATION,
  GET_FINDING_EVIDENCES,
  UPDATE_EVIDENCE_MUTATION,
  UPDATE_DESCRIPTION_MUTATION,
  REJECT_EVIDENCE_MUTATION,
  REMOVE_EVIDENCE_MUTATION,
};
