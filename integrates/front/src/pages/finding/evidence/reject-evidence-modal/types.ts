import type { ApolloQueryResult, OperationVariables } from "@apollo/client";

import type { GetFindingEvidencesQuery } from "gql/graphql";

interface IRejectEvidenceModalProps {
  evidenceId: string;
  findingId: string;
  refetch: (
    variables?: Partial<OperationVariables>,
  ) => Promise<ApolloQueryResult<GetFindingEvidencesQuery>>;
  setEditing: React.Dispatch<React.SetStateAction<boolean>>;
  onClose: () => void;
  isOpen: boolean;
}

export type { IRejectEvidenceModalProps };
