import { useMutation } from "@apollo/client";
import {
  Form,
  InnerForm,
  Modal,
  TextArea,
  useModal,
} from "@fluidattacks/design";
import mixpanel from "mixpanel-browser";
import { StrictMode, useCallback } from "react";
import { useTranslation } from "react-i18next";
import { object, string } from "yup";

import type { IRejectEvidenceModalProps } from "./types";

import { REJECT_EVIDENCE_MUTATION } from "../queries";
import type { EvidenceDescriptionType } from "gql/graphql";
import { Logger } from "utils/logger";
import { msgError, msgSuccess } from "utils/notifications";

export const RejectEvidenceModal = ({
  findingId,
  evidenceId,
  refetch,
  setEditing,
  isOpen,
  onClose,
}: IRejectEvidenceModalProps): JSX.Element => {
  const { t } = useTranslation();

  const modalProps = useModal("reject-evidence-modal");

  const [rejectEvidence] = useMutation(REJECT_EVIDENCE_MUTATION, {
    onCompleted: (result: { rejectEvidence: { success: boolean } }): void => {
      if (result.rejectEvidence.success) {
        msgSuccess(
          t("searchFindings.tabEvidence.rejectModal.message"),
          t("searchFindings.tabEvidence.rejectModal.success"),
        );
      }
      refetch({ findingId }).catch((): void => {
        Logger.error("An error occurred rejecting evidence");
      });
    },
    onError: ({ graphQLErrors }): void => {
      refetch({ findingId }).catch((): void => {
        Logger.error("An error occurred finding evidence");
      });
      graphQLErrors.forEach((error): void => {
        msgError(t("groupAlerts.errorTextsad"));
        Logger.warning("An error occurred rejecting finding evidences", error);
      });
    },
  });

  const validationSchema = object().shape({
    justification: string()
      .required(t("validations.required"))
      .isValidTextBeginning()
      .isValidTextField(),
  });

  const rejectImage = useCallback(
    async (values: Record<string, string>): Promise<void> => {
      mixpanel.track("RejectEvidence");
      setEditing(false);
      await rejectEvidence({
        variables: {
          evidenceId: evidenceId.toUpperCase() as EvidenceDescriptionType,
          findingId,
          justification: values.justification,
        },
      });
    },
    [evidenceId, findingId, rejectEvidence, setEditing],
  );

  return (
    <StrictMode>
      <Modal
        id={"removeFinding"}
        modalRef={{ ...modalProps, close: onClose, isOpen }}
        size={"sm"}
        title={t("searchFindings.tabEvidence.rejectModal.title")}
      >
        <Form
          defaultValues={{ description: "" }}
          onSubmit={rejectImage}
          yupSchema={validationSchema}
        >
          <InnerForm>
            {({ formState, watch, register }): JSX.Element => (
              <TextArea
                error={formState.errors.justification?.message?.toString()}
                isTouched={"justification" in formState.touchedFields}
                isValid={!("justification" in formState.errors)}
                label={t(
                  "searchFindings.tabEvidence.rejectModal.justification",
                )}
                maxLength={10000}
                name={"justification"}
                register={register}
                required={true}
                watch={watch}
              />
            )}
          </InnerForm>
        </Form>
      </Modal>
    </StrictMode>
  );
};
