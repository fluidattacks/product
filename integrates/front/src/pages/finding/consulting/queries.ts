import { graphql } from "gql";

const CONSULT_FIELDS_FRAGMENT = graphql(`
  fragment consultFields on Consult {
    id
    content
    created
    email
    fullName
    modified
    parentComment
  }
`);

const GET_FINDING_CONSULTING = graphql(`
  query GetFindingConsulting($findingId: String!) {
    finding(identifier: $findingId) {
      id
      consulting {
        ...consultFields
      }
    }
  }
`);

const GET_FINDING_OBSERVATIONS = graphql(`
  query GetFindingObservations($findingId: String!) {
    finding(identifier: $findingId) {
      id
      observations {
        ...consultFields
      }
    }
  }
`);

const ADD_FINDING_CONSULT = graphql(`
  mutation AddFindingConsult(
    $content: String!
    $findingId: String!
    $parentComment: GenericScalar!
    $type: FindingConsultType!
  ) {
    addFindingConsult(
      content: $content
      findingId: $findingId
      parentComment: $parentComment
      type: $type
    ) {
      commentId
      success
    }
  }
`);

export {
  CONSULT_FIELDS_FRAGMENT,
  GET_FINDING_CONSULTING,
  GET_FINDING_OBSERVATIONS,
  ADD_FINDING_CONSULT,
};
