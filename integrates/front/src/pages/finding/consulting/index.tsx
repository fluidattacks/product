import { useMutation } from "@apollo/client";
import { Container, Loading } from "@fluidattacks/design";
import _ from "lodash";
import mixpanel from "mixpanel-browser";
import { useCallback, useMemo } from "react";
import * as React from "react";
import { useTranslation } from "react-i18next";
import { useParams } from "react-router-dom";

import { handleAddCommentErrorHelper, useConsultingQuery } from "./hooks";
import {
  ADD_FINDING_CONSULT,
  GET_FINDING_CONSULTING,
  GET_FINDING_OBSERVATIONS,
} from "./queries";

import { Comments } from "features/comments";
import type { ICommentStructure } from "features/comments/types";
import type { FindingConsultType } from "gql/graphql";

interface IFindingConsultingProps {
  type: "consult" | "observation";
}

const FindingConsulting: React.FC<IFindingConsultingProps> = ({
  type,
}): JSX.Element => {
  const { findingId } = useParams() as { findingId: string };

  const isObservation = useMemo((): boolean => type === "observation", [type]);
  const { t } = useTranslation();

  const { comments, loading } = useConsultingQuery(findingId, type);

  const [addComment] = useMutation(ADD_FINDING_CONSULT, {
    onError: (addCommentError): void => {
      handleAddCommentErrorHelper(addCommentError, type);
    },
    refetchQueries: [
      type === "consult" ? GET_FINDING_CONSULTING : GET_FINDING_OBSERVATIONS,
    ],
  });

  const handlePost = useCallback(
    async (comment: ICommentStructure): Promise<void> => {
      mixpanel.track(`Add${_.capitalize(type)}`, {
        findingId,
      });
      await addComment({
        variables: {
          content: comment.content,
          findingId,
          parentComment: comment.parentComment,
          type: type.toUpperCase() as FindingConsultType,
        },
      });
    },
    [addComment, findingId, type],
  );

  if (comments === undefined || loading) {
    return (
      <Container
        alignItems={"center"}
        display={"flex"}
        height={"100%"}
        justify={"center"}
      >
        <Loading color={"red"} label={"Loading..."} size={"lg"} />
      </Container>
    );
  }

  return (
    <React.StrictMode>
      <div data-private={true}>
        <Comments
          comments={comments}
          emptyComponentsProps={{
            confirmButton: {
              text: isObservation
                ? t("vulnerability.observations.add")
                : t("group.consulting.empty.button"),
            },
            description: isObservation
              ? t("vulnerability.observations.description")
              : t("vulnerability.consulting.description"),
          }}
          isFindingComment={true}
          isObservation={isObservation}
          onPostComment={handlePost}
        />
      </div>
    </React.StrictMode>
  );
};

export { FindingConsulting };
