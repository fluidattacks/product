import { type ApolloError, useQuery } from "@apollo/client";
import { useContext, useMemo } from "react";

import {
  CONSULT_FIELDS_FRAGMENT,
  GET_FINDING_CONSULTING,
  GET_FINDING_OBSERVATIONS,
} from "./queries";

import { authContext } from "context/auth";
import type { ICommentStructure } from "features/comments/types";
import { getFragmentData } from "gql/fragment-masking";
import { useAudit } from "hooks/use-audit";
import { Logger } from "utils/logger";
import { msgError } from "utils/notifications";
import { translate } from "utils/translations/translate";

interface IUseConsultingQuery {
  comments: ICommentStructure[] | undefined;
  loading: boolean;
}

const useConsultingQuery = (
  findingId: string,
  type: "consult" | "observation",
): IUseConsultingQuery => {
  const { addAuditEvent } = useAudit();
  const { userEmail } = useContext(authContext);

  const consultingQuery = useQuery(GET_FINDING_CONSULTING, {
    onCompleted: (): void => {
      addAuditEvent("Finding.Consulting", findingId);
    },
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error): void => {
        msgError(translate.t("groupAlerts.errorTextsad"));
        Logger.warning("An error occurred loading finding consulting", error);
      });
    },
    skip: type !== "consult",
    variables: { findingId },
  });

  const observationsQuery = useQuery(GET_FINDING_OBSERVATIONS, {
    onCompleted: (): void => {
      addAuditEvent("Finding.Observations", findingId);
    },
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error): void => {
        msgError(translate.t("groupAlerts.errorTextsad"));
        Logger.warning("An error occurred loading finding observations", error);
      });
    },
    skip: type !== "observation",
    variables: { findingId },
  });

  const commentsDataset =
    type === "consult"
      ? consultingQuery.data?.finding.consulting
      : observationsQuery.data?.finding.observations;

  const comments = useMemo(
    (): ICommentStructure[] | undefined =>
      commentsDataset?.map((commentData): ICommentStructure => {
        // REFAC NEEDED: Adjust API schema nullity on these fields
        // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
        const comment = getFragmentData(CONSULT_FIELDS_FRAGMENT, commentData)!;

        return {
          ...comment,
          createdByCurrentUser: comment.email === userEmail,
          id: Number(comment.id),
          parentComment: Number(comment.parentComment),
        };
      }),
    [commentsDataset, userEmail],
  );

  return {
    comments,
    loading: consultingQuery.loading || observationsQuery.loading,
  };
};

const handleAddCommentErrorHelper = (
  addCommentError: ApolloError,
  type: string,
): void => {
  addCommentError.graphQLErrors.forEach(({ message }): void => {
    switch (message) {
      case "Exception - Invalid field length in form":
        msgError(translate.t("validations.invalidFieldLength"));
        break;
      case "Exception - Comment parent is invalid":
        msgError(
          translate.t("validations.invalidCommentParent", {
            count: 1,
          }),
        );
        break;
      case "Exception - Consults are not allowed in Drafts":
        msgError(translate.t("validations.invalidConsult"));
        break;
      default:
        msgError(translate.t("groupAlerts.errorTextsad"));
        Logger.warning(`An error occurred posting ${type}`, addCommentError);
    }
  });
};

export { handleAddCommentErrorHelper, useConsultingQuery };
