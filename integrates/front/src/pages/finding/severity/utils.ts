/* eslint-disable sort-keys -- Key order for metric values is altered for correct tooltip rendering*/
import { theme } from "components/colors";
import type { ISeverityField } from "pages/finding/severity/types";
import type {
  ICVSS3EnvironmentalMetrics,
  ICVSS3TemporalMetrics,
} from "utils/cvss";
import {
  attackComplexityValues,
  attackVectorValues,
  availabilityImpactValues,
  availabilityRequirementValues,
  confidentialityImpactValues,
  confidentialityRequirementValues,
  exploitabilityValues,
  integrityImpactValues,
  integrityRequirementValues,
  privilegesRequiredValues,
  remediationLevelValues,
  reportConfidenceValues,
  severityScopeValues,
  userInteractionValues,
} from "utils/cvss";
import type { ICVSS4ThreatMetrics } from "utils/cvss4";
import { translate } from "utils/translations/translate";

const attackVectorBgColors: Record<string, string> = {
  N: theme.palette.error[500],
  A: theme.palette.error[200],
  L: theme.palette.warning[600],
  P: theme.palette.warning[500],
};

const attackComplexityBgColors: Record<string, string> = {
  L: theme.palette.error[500],
  H: theme.palette.warning[500],
};

const attackRequirementsBgColors: Record<string, string> = {
  P: theme.palette.warning[500],
  N: theme.palette.error[500],
};

const privilegesRequiredBgColors: Record<string, string> = {
  N: theme.palette.error[500],
  L: theme.palette.warning[600],
  H: theme.palette.warning[500],
};

const userInteractionBgColors: Record<string, string> = {
  N: theme.palette.error[500],
  P: theme.palette.warning[600],
  R: theme.palette.warning[500],
  A: theme.palette.warning[500],
};

const confidentialityVulnerableImpactBgColors: Record<string, string> = {
  H: theme.palette.error[500],
  L: theme.palette.warning[500],
  N: theme.palette.success[200],
};

const integrityVulnerableImpactBgColors: Record<string, string> = {
  H: theme.palette.error[500],
  L: theme.palette.warning[500],
  N: theme.palette.success[200],
};

const availabilityVulnerableImpactBgColors: Record<string, string> = {
  H: theme.palette.error[500],
  L: theme.palette.warning[500],
  N: theme.palette.success[200],
};

const severityScopeBgColors: Record<string, string> = {
  C: theme.palette.error[500],
  U: theme.palette.warning[500],
};

const remediationLevelBgColors: Record<string, string> = {
  U: theme.palette.error[500],
  W: theme.palette.warning[600],
  T: theme.palette.warning[500],
  O: theme.palette.success[200],
  X: theme.palette.gray[400],
};

const confidentialityImpactBgColors: Record<string, string> = {
  H: theme.palette.error[500],
  L: theme.palette.warning[500],
  N: theme.palette.success[200],
};

const reportConfidenceBgColors: Record<string, string> = {
  C: theme.palette.error[500],
  R: theme.palette.warning[600],
  U: theme.palette.warning[500],
  X: theme.palette.gray[400],
};

const integrityImpactBgColors: Record<string, string> = {
  H: theme.palette.error[500],
  L: theme.palette.warning[500],
  N: theme.palette.success[200],
};

const availabilityImpactBgColors: Record<string, string> = {
  H: theme.palette.error[500],
  L: theme.palette.warning[500],
  N: theme.palette.success[200],
};

const exploitabilityBgColors: Record<string, string> = {
  A: theme.palette.error[500],
  H: theme.palette.error[500],
  F: theme.palette.error[200],
  P: theme.palette.warning[600],
  U: theme.palette.warning[500],
  X: theme.palette.gray[400],
};

const castFieldsCVSS3: (
  dataset: ICVSS3EnvironmentalMetrics,
) => ISeverityField[] = (
  dataset: ICVSS3EnvironmentalMetrics,
): ISeverityField[] => {
  const fields: ISeverityField[] = [
    {
      currentValue: dataset.attackVector,
      name: "attackVector",
      options: attackVectorValues,
      title: translate.t("searchFindings.tabSeverity.attackVector.label"),
    },
    {
      currentValue: dataset.attackComplexity,
      name: "attackComplexity",
      options: attackComplexityValues,
      title: translate.t("searchFindings.tabSeverity.attackComplexity.label"),
    },
    {
      currentValue: dataset.userInteraction,
      name: "userInteraction",
      options: userInteractionValues,
      title: translate.t("searchFindings.tabSeverity.userInteraction.label"),
    },
    {
      currentValue: dataset.severityScope,
      name: "severityScope",
      options: severityScopeValues,
      title: translate.t("searchFindings.tabSeverity.severityScope.label"),
    },
    {
      currentValue: dataset.confidentialityImpact,
      name: "confidentialityImpact",
      options: confidentialityImpactValues,
      title: translate.t(
        "searchFindings.tabSeverity.confidentialityImpact.label",
      ),
    },
    {
      currentValue: dataset.integrityImpact,
      name: "integrityImpact",
      options: integrityImpactValues,
      title: translate.t("searchFindings.tabSeverity.integrityImpact.label"),
    },
    {
      currentValue: dataset.availabilityImpact,
      name: "availabilityImpact",
      options: availabilityImpactValues,
      title: translate.t("searchFindings.tabSeverity.availabilityImpact.label"),
    },
    {
      currentValue: dataset.exploitability,
      name: "exploitability",
      options: exploitabilityValues,
      title: translate.t("searchFindings.tabSeverity.exploitability.label"),
    },
    {
      currentValue: dataset.remediationLevel,
      name: "remediationLevel",
      options: remediationLevelValues,
      title: translate.t("searchFindings.tabSeverity.remediationLevel.label"),
    },
    {
      currentValue: dataset.reportConfidence,
      name: "reportConfidence",
      options: reportConfidenceValues,
      title: translate.t("searchFindings.tabSeverity.reportConfidence.label"),
    },
    {
      currentValue: dataset.privilegesRequired,
      name: "privilegesRequired",
      options: privilegesRequiredValues,
      title: translate.t("searchFindings.tabSeverity.privilegesRequired.label"),
    },
  ];

  const environmentFields: ISeverityField[] = [
    {
      currentValue: dataset.confidentialityRequirement,
      name: "confidentialityRequirement",
      options: confidentialityRequirementValues,
      title: translate.t(
        "searchFindings.tabSeverity.confidentialityRequirement.label",
      ),
    },
    {
      currentValue: dataset.integrityRequirement,
      name: "integrityRequirement",
      options: integrityRequirementValues,
      title: translate.t(
        "searchFindings.tabSeverity.integrityRequirement.label",
      ),
    },
    {
      currentValue: dataset.availabilityRequirement,
      name: "availabilityRequirement",
      options: availabilityRequirementValues,
      title: translate.t(
        "searchFindings.tabSeverity.availabilityRequirement.label",
      ),
    },
    {
      currentValue: dataset.modifiedAttackVector,
      name: "modifiedAttackVector",
      options: attackVectorValues,
      title: translate.t("searchFindings.tabSeverity.modifiedAttackVector"),
    },
    {
      currentValue: dataset.modifiedAttackComplexity,
      name: "modifiedAttackComplexity",
      options: attackComplexityValues,
      title: translate.t("searchFindings.tabSeverity.modifiedAttackComplexity"),
    },
    {
      currentValue: dataset.modifiedUserInteraction,
      name: "modifiedUserInteraction",
      options: userInteractionValues,
      title: translate.t("searchFindings.tabSeverity.modifiedUserInteraction"),
    },
    {
      currentValue: dataset.modifiedSeverityScope,
      name: "modifiedSeverityScope",
      options: severityScopeValues,
      title: translate.t("searchFindings.tabSeverity.modifiedSeverityScope"),
    },
    {
      currentValue: dataset.modifiedConfidentialityImpact,
      name: "modifiedConfidentialityImpact",
      options: confidentialityImpactValues,
      title: translate.t(
        "searchFindings.tabSeverity.modifiedConfidentialityImpact",
      ),
    },
    {
      currentValue: dataset.modifiedIntegrityImpact,
      name: "modifiedIntegrityImpact",
      options: integrityImpactValues,
      title: translate.t("searchFindings.tabSeverity.modifiedIntegrityImpact"),
    },
    {
      currentValue: dataset.modifiedAvailabilityImpact,
      name: "modifiedAvailabilityImpact",
      options: availabilityImpactValues,
      title: translate.t(
        "searchFindings.tabSeverity.modifiedAvailabilityImpact",
      ),
    },
    {
      currentValue: dataset.modifiedPrivilegesRequired,
      name: "modifiedPrivilegesRequired",
      options: privilegesRequiredValues,
      title: translate.t(
        "searchFindings.tabSeverity.modifiedPrivilegesRequired",
      ),
    },
  ];

  return [...fields, ...environmentFields];
};

const cloudinaryUrl = "https://res.cloudinary.com/fluid-attacks/image/upload";
const version = [
  "/v1706127922/",
  "/v1706127923/",
  "/v1706127924/",
  "/v1706127925/",
  "/v1706127926/",
  "/v1706127927/",
  "/v1706127950/",
  "/v1706127983/",
  "/v1706128042/",
  "/v1706128044/",
  "/v1706128045/",
  "/v1706128046/",
  "/v1715894816/",
  "/v1715894797/",
  "/v1715894785/",
  "/v1715894764/",
  "/v1715894724/",
];
const severityImagesRoot = "integrates/severity/";
const severityImagesRootV4 = "integrates/severity-v4/";

const severityImages: Record<string, string> = {
  attackComplexityHigh: `${version[0]}${severityImagesRoot}attackComplexityHigh.webp`,
  attackComplexityLow: `${version[0]}${severityImagesRoot}attackComplexityLow.webp`,
  attackRequirementsNone: `${version[11]}${severityImagesRootV4}attackRequirementsNone.webp`,
  attackRequirementsPresent: `${version[12]}${severityImagesRootV4}attackRequirementsPresent.webp`,
  attackVectorAdjacent: `${version[0]}${severityImagesRoot}attackVectorAdjacent.webp`,
  attackVectorLocal: `${version[0]}${severityImagesRoot}attackVectorLocal.webp`,
  attackVectorNetwork: `${version[0]}${severityImagesRoot}attackVectorNetwork.webp`,
  attackVectorPhysical: `${version[0]}${severityImagesRoot}attackVectorPhysical.webp`,
  availabilitySubsequentImpactHigh: `${version[1]}${severityImagesRoot}availabilityImpactHigh.webp`,
  availabilitySubsequentImpactLow: `${version[1]}${severityImagesRoot}availabilityImpactLow.webp`,
  availabilitySubsequentImpactNone: `${version[1]}${severityImagesRoot}availabilityImpactNone.webp`,
  availabilityVulnerableImpactHigh: `${version[1]}${severityImagesRoot}availabilityImpactHigh.webp`,
  availabilityVulnerableImpactLow: `${version[1]}${severityImagesRoot}availabilityImpactLow.webp`,
  availabilityVulnerableImpactNone: `${version[1]}${severityImagesRoot}availabilityImpactNone.webp`,
  confidentialitySubsequentImpactHigh: `${version[1]}${severityImagesRoot}confidentialityImpactHigh.webp`,
  confidentialitySubsequentImpactLow: `${version[2]}${severityImagesRoot}confidentialityImpactLow.webp`,
  confidentialitySubsequentImpactNone: `${version[2]}${severityImagesRoot}confidentialityImpactNone.webp`,
  confidentialityVulnerableImpactHigh: `${version[1]}${severityImagesRoot}confidentialityImpactHigh.webp`,
  confidentialityVulnerableImpactLow: `${version[2]}${severityImagesRoot}confidentialityImpactLow.webp`,
  confidentialityVulnerableImpactNone: `${version[2]}${severityImagesRoot}confidentialityImpactNone.webp`,
  exploitabilityAttacked: `${version[13]}${severityImagesRootV4}exploitabilityAttacked.webp`,
  exploitabilityNot: `${version[3]}${severityImagesRoot}exploitabilityNot.webp`,
  exploitabilityProof: `${version[3]}${severityImagesRoot}exploitabilityProof.webp`,
  exploitabilityUnproven: `${version[3]}${severityImagesRoot}exploitabilityUnproven.webp`,
  exploitabilityUnreported: `${version[3]}${severityImagesRoot}exploitabilityUnproven.webp`,
  integritySubsequentImpactHigh: `${version[4]}${severityImagesRoot}integrityImpactHigh.webp`,
  integritySubsequentImpactLow: `${version[5]}${severityImagesRoot}integrityImpactLow.webp`,
  integritySubsequentImpactNone: `${version[5]}${severityImagesRoot}integrityImpactNone.webp`,
  integrityVulnerableImpactHigh: `${version[4]}${severityImagesRoot}integrityImpactHigh.webp`,
  integrityVulnerableImpactLow: `${version[5]}${severityImagesRoot}integrityImpactLow.webp`,
  integrityVulnerableImpactNone: `${version[5]}${severityImagesRoot}integrityImpactNone.webp`,
  privilegesRequiredHigh: `${version[6]}${severityImagesRoot}privilegesRequiredHigh.webp`,
  privilegesRequiredLow: `${version[7]}${severityImagesRoot}privilegesRequiredLow.webp`,
  privilegesRequiredNone: `${version[8]}${severityImagesRoot}privilegesRequiredNone.webp`,
  userInteractionActive: `${version[14]}${severityImagesRootV4}userInteractionActive.webp`,
  userInteractionNone: `${version[10]}${severityImagesRoot}userInteractionNone.webp`,
  userInteractionPassive: `${version[15]}${severityImagesRootV4}userInteractionPassive.webp`,
};

const mappedColors: Record<
  keyof ICVSS4ThreatMetrics,
  Record<string, string>
> = {
  availabilitySubsequentImpact: availabilityVulnerableImpactBgColors,
  confidentialitySubsequentImpact: confidentialityVulnerableImpactBgColors,
  integritySubsequentImpact: integrityVulnerableImpactBgColors,
  attackComplexity: attackComplexityBgColors,
  attackVector: attackVectorBgColors,
  availabilityVulnerableImpact: availabilityVulnerableImpactBgColors,
  confidentialityVulnerableImpact: confidentialityVulnerableImpactBgColors,
  exploitability: exploitabilityBgColors,
  integrityVulnerableImpact: integrityVulnerableImpactBgColors,
  privilegesRequired: privilegesRequiredBgColors,
  userInteraction: userInteractionBgColors,
  attackRequirements: attackRequirementsBgColors,
};

const mappedColorsV3: Record<
  keyof ICVSS3TemporalMetrics,
  Record<string, string>
> = {
  availabilityImpact: availabilityImpactBgColors,
  confidentialityImpact: confidentialityImpactBgColors,
  integrityImpact: integrityImpactBgColors,
  attackComplexity: attackComplexityBgColors,
  attackVector: attackVectorBgColors,
  exploitability: exploitabilityBgColors,
  privilegesRequired: privilegesRequiredBgColors,
  remediationLevel: remediationLevelBgColors,
  reportConfidence: reportConfidenceBgColors,
  severityScope: severityScopeBgColors,
  userInteraction: userInteractionBgColors,
};

export {
  attackComplexityBgColors,
  attackVectorBgColors,
  availabilityImpactBgColors,
  castFieldsCVSS3,
  confidentialityImpactBgColors,
  cloudinaryUrl,
  mappedColors,
  mappedColorsV3,
  exploitabilityBgColors,
  integrityImpactBgColors,
  privilegesRequiredBgColors,
  severityScopeBgColors,
  severityImages,
  severityImagesRoot,
  severityImagesRootV4,
  userInteractionBgColors,
  remediationLevelBgColors,
  reportConfidenceBgColors,
  version,
};
