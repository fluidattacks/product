import { CloudImage, Container, Text, Tooltip } from "@fluidattacks/design";
import first from "lodash/first";
import * as React from "react";
import { useTranslation } from "react-i18next";

import { TooltipContent } from "../content-v4/tooltip-content";
import {
  mappedColors,
  severityImagesRoot,
  severityImagesRootV4,
} from "../utils";
import type { ICVSS4ThreatMetrics } from "utils/cvss4";
import { translate } from "utils/translations/translate";

interface ISeverityTile {
  color: string;
  metricOptions: Record<string, string>;
  metricValue: string;
  name: keyof ICVSS4ThreatMetrics;
}

const SeverityTile: React.FC<Readonly<ISeverityTile>> = ({
  color,
  name,
  metricOptions,
  metricValue,
}): JSX.Element => {
  const { t } = useTranslation();

  const valueText = t(metricOptions[metricValue]);

  const usesV4 = [
    "attackRequirementsNone",
    "attackRequirementsPresent",
    "exploitabilityAttacked",
    "userInteractionActive",
    "userInteractionPassive",
  ];
  const imageName = (
    first(`${name}${translate.t(valueText)}`.split(" ")) as string
  )
    .replace(/\W/u, "")
    .replace("Subsequent", "")
    .replace("Vulnerable", "")
    .replace("Unreported", "Unproven");

  const rootImage = usesV4.includes(imageName)
    ? severityImagesRootV4
    : severityImagesRoot;

  return (
    <div className={"pa1 w-100 mb3-l mb2-m mb1-ns"}>
      <Tooltip
        effect={"float"}
        id={name}
        nodeTip={
          <TooltipContent
            bgColor={mappedColors[name]}
            metricOptions={metricOptions}
            metricValue={metricValue}
          />
        }
      >
        <Container
          alignItems={"center"}
          display={"flex"}
          justify={"center"}
          mb={1}
        >
          <Container>
            <CloudImage
              alt={"severityImg"}
              publicId={`${rootImage}${imageName}`}
              width={"60px"}
            />
          </Container>
          <Container ml={1}>
            <Text fontWeight={"bold"} size={"sm"}>
              {t(`searchFindings.tabSeverity.${name}.label`)}
            </Text>
            <Container
              alignItems={"center"}
              display={"flex"}
              justify={"center"}
            >
              <Container mr={0.25}>
                <span className={`dib br-100 pa1 ${color}`} />
              </Container>
              <Text size={"xs"}>{valueText}</Text>
            </Container>
          </Container>
        </Container>
      </Tooltip>
    </div>
  );
};

export type { ISeverityTile };
export { SeverityTile };
