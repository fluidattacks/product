import { PureAbility } from "@casl/ability";
import { screen, waitFor } from "@testing-library/react";
import { userEvent } from "@testing-library/user-event";
import { GraphQLError } from "graphql";
import { HttpResponse, type StrictResponse, graphql } from "msw";
import { Route, Routes } from "react-router-dom";

import { authzPermissionsContext } from "context/authz/config";
import type {
  GetFindingSeverityQueryQuery as GetFindingSeverity,
  UpdateFindingSeverityMutationMutation,
} from "gql/graphql";
import { render } from "mocks";
import { LINK } from "mocks/constants";
import type { IErrorMessage } from "mocks/types";
import { FindingSeverity } from "pages/finding/severity";
import {
  GET_FINDING_SEVERITY,
  UPDATE_FINDING_SEVERITY_MUTATION,
} from "pages/finding/severity/queries";
import { msgError, msgSuccess } from "utils/notifications";
import { selectOption } from "utils/test-utils";

jest.mock("utils/notifications", (): Record<string, unknown> => {
  const mockedNotifications: Record<string, () => Record<string, unknown>> =
    jest.requireActual("utils/notifications");
  jest.spyOn(mockedNotifications, "msgSuccess").mockImplementation();
  jest.spyOn(mockedNotifications, "msgError").mockImplementation();

  return mockedNotifications;
});

describe("findingSeverity", (): void => {
  const mocks = [
    graphql
      .link(LINK)
      .query(
        GET_FINDING_SEVERITY,
        ({
          variables,
        }): StrictResponse<IErrorMessage | { data: GetFindingSeverity }> => {
          const { identifier } = variables;

          if (identifier === "438679960") {
            return HttpResponse.json({
              data: {
                finding: {
                  __typename: "Finding",
                  id: identifier,
                  severityVector:
                    "CVSS:3.1/AV:N/AC:L/PR:L/UI:N/S:U/C:N/I:L/A:N/E:U/RL:O/RC:R",
                  severityVectorV4:
                    "CVSS:4.0/AV:N/AT:N/AC:L/PR:L/UI:N/VC:N/VI:L/VA:N/SC:N/SI:N/SA:N/E:U",
                },
              },
            });
          }

          return HttpResponse.json({
            errors: [new GraphQLError("Error getting finding severity")],
          });
        },
      ),
  ];

  it("should render a component", async (): Promise<void> => {
    expect.hasAssertions();

    const mockedPermissions = new PureAbility<string>([
      { action: "integrates_api_mutations_update_severity_mutate" },
    ]);
    render(
      <authzPermissionsContext.Provider value={mockedPermissions}>
        <Routes>
          <Route
            element={<FindingSeverity cvssVersion={"severityTemporalScore"} />}
            path={"/:groupName/vulns/:findingId/severity"}
          />
        </Routes>
      </authzPermissionsContext.Provider>,
      {
        memoryRouter: {
          initialEntries: ["/TEST/vulns/438679960/severity"],
        },
        mocks: [
          ...mocks,
          graphql.link(LINK).mutation(
            UPDATE_FINDING_SEVERITY_MUTATION,
            ({
              variables,
            }): StrictResponse<
              IErrorMessage | { data: UpdateFindingSeverityMutationMutation }
            > => {
              const { cvssVector, cvss4Vector, findingId } = variables;

              if (
                cvss4Vector ===
                  "CVSS:4.0/AV:N/AC:L/AT:N/PR:L/UI:N/VC:H/VI:L/VA:N/SC:N/SI:N/SA:H/E:U/CR:L/IR:X/AR:X/MAV:X/MAC:X/MAT:X/MPR:X/MUI:X/MVC:X/MVI:X/MVA:X/MSC:X/MSI:X/MSA:X" &&
                cvssVector ===
                  "CVSS:3.1/AV:N/AC:L/PR:L/UI:N/S:U/C:H/I:L/A:N/E:U/RL:O/RC:R/CR:X/IR:X/AR:X/MAV:X/MAC:X/MPR:X/MUI:X/MS:X/MC:X/MI:X/MA:X" &&
                findingId === "438679960"
              ) {
                return HttpResponse.json({
                  data: { updateSeverity: { success: true } },
                });
              }

              return HttpResponse.json({
                errors: [new GraphQLError("Error updating severity")],
              });
            },
            { once: true },
          ),
        ],
      },
    );
    await waitFor((): void => {
      expect(
        screen.queryByText("searchFindings.tabSeverity.editable.label"),
      ).toBeInTheDocument();
    });

    expect(
      screen.queryByText("searchFindings.tabSeverity.privilegesRequired.label"),
    ).toBeInTheDocument();
    expect(
      screen.queryByText(
        "searchFindings.tabSeverity.exploitability.options.proofOfConcept.label",
      ),
    ).not.toBeInTheDocument();
    expect(
      screen.queryByText("searchFindings.tabSeverity.editable.label"),
    ).toBeInTheDocument();

    await userEvent.click(
      screen.getByText("searchFindings.tabSeverity.editable.label"),
    );

    await waitFor((): void => {
      expect(
        screen.getByText("searchFindings.tabSeverity.cvssVersion: 3.1"),
      ).toBeInTheDocument();
    });
    await waitFor((): void => {
      expect(
        screen.getByText("searchFindings.tabSeverity.cvssVersion: 4.0"),
      ).toBeInTheDocument();
    });

    await waitFor((): void => {
      expect(
        screen.queryAllByText(
          "searchFindings.tabSeverity.exploitability.options.unproven.label",
        ),
      ).toHaveLength(1);
    });

    expect(
      screen.queryAllByText(
        "searchFindings.tabSeverity.exploitability.options.unreported.label",
      ),
    ).toHaveLength(1);

    await selectOption("scoreV4confidentialityRequirement", "L");
    await selectOption("scoreV4availabilitySubsequentImpact", "H");
    await selectOption("scoreV4confidentialityVulnerableImpact", "H");
    await selectOption("confidentialityImpact", "H");

    await userEvent.click(
      screen.getByText("searchFindings.tabSeverity.update.text"),
    );

    await waitFor((): void => {
      expect(msgSuccess).toHaveBeenCalledWith(
        "groupAlerts.updated",
        "groupAlerts.updatedTitle",
      );
    });

    jest.clearAllMocks();
  });

  it("should render an error in component", async (): Promise<void> => {
    expect.hasAssertions();

    render(
      <Routes>
        <Route
          element={<FindingSeverity cvssVersion={"severityTemporalScore"} />}
          path={"/:groupName/vulns/:findingId/severity"}
        />
      </Routes>,
      {
        memoryRouter: {
          initialEntries: ["/TEST/vulns/438679961/severity"],
        },
        mocks,
      },
    );
    await waitFor((): void => {
      expect(msgError).toHaveBeenCalledWith("groupAlerts.errorTextsad");
    });

    jest.clearAllMocks();
  });

  it("should render as editable", async (): Promise<void> => {
    expect.hasAssertions();

    const mockedPermissions = new PureAbility<string>([
      { action: "integrates_api_mutations_update_severity_mutate" },
    ]);
    render(
      <authzPermissionsContext.Provider value={mockedPermissions}>
        <Routes>
          <Route
            element={<FindingSeverity cvssVersion={"severityTemporalScore"} />}
            path={"/:groupName/vulns/:findingId/severity"}
          />
        </Routes>
      </authzPermissionsContext.Provider>,
      {
        memoryRouter: {
          initialEntries: ["/TEST/vulns/438679960/severity"],
        },
        mocks,
      },
    );
    await waitFor((): void => {
      expect(
        screen.queryByText("searchFindings.tabSeverity.editable.label"),
      ).toBeInTheDocument();
    });
    await userEvent.click(
      screen.getByText("searchFindings.tabSeverity.editable.label"),
    );
    await waitFor((): void => {
      expect(
        screen.queryByText("searchFindings.tabSeverity.editable.label"),
      ).not.toBeInTheDocument();
    });

    expect(
      screen.queryByText("searchFindings.tabSeverity.update.text"),
    ).toBeDisabled();

    expect(
      screen.queryByText("searchFindings.tabSeverity.editable.cancel"),
    ).toBeInTheDocument();

    await userEvent.click(
      screen.getByText("searchFindings.tabSeverity.editable.cancel"),
    );
    await waitFor((): void => {
      expect(
        screen.queryByText("searchFindings.tabSeverity.editable.label"),
      ).toBeInTheDocument();
    });
  });

  it("should render as readonly 3.1", async (): Promise<void> => {
    expect.hasAssertions();

    render(
      <Routes>
        <Route
          element={<FindingSeverity cvssVersion={"severityTemporalScore"} />}
          path={"/:groupName/vulns/:findingId/severity"}
        />
      </Routes>,
      {
        memoryRouter: {
          initialEntries: ["/TEST/vulns/438679960/severity"],
        },
        mocks,
      },
    );
    await waitFor((): void => {
      expect(
        screen.queryByText("searchFindings.tabSeverity.editable.label"),
      ).not.toBeInTheDocument();
    });

    await waitFor((): void => {
      expect(
        screen.queryByText("searchFindings.tabSeverity.reportConfidence.label"),
      ).toBeInTheDocument();
    });

    expect(
      screen.queryByText(
        "searchFindings.tabSeverity.confidentialityVulnerableImpact.label",
      ),
    ).not.toBeInTheDocument();
  });

  it("should render as readonly 4.0", async (): Promise<void> => {
    expect.hasAssertions();

    render(
      <Routes>
        <Route
          element={<FindingSeverity cvssVersion={"severityThreatScore"} />}
          path={"/:groupName/vulns/:findingId/severity"}
        />
      </Routes>,
      {
        memoryRouter: {
          initialEntries: ["/TEST/vulns/438679960/severity"],
        },
        mocks,
      },
    );
    await waitFor((): void => {
      expect(
        screen.queryByText("searchFindings.tabSeverity.editable.label"),
      ).not.toBeInTheDocument();
    });

    await waitFor((): void => {
      expect(
        screen.queryByText(
          "searchFindings.tabSeverity.confidentialityVulnerableImpact.label",
        ),
      ).toBeInTheDocument();
    });

    expect(
      screen.queryByText("searchFindings.tabSeverity.reportConfidence.label"),
    ).not.toBeInTheDocument();
  });
});
