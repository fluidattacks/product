import { CloudImage, Container, Text, Tooltip } from "@fluidattacks/design";
import first from "lodash/first";
import * as React from "react";
import { useTranslation } from "react-i18next";
import { styled } from "styled-components";

import { TooltipContent } from "../content/tooltip-content";
import { mappedColorsV3, severityImagesRoot } from "../utils";
import type { ICVSS3TemporalMetrics } from "utils/cvss";
import { translate } from "utils/translations/translate";

interface ISeverityTile {
  color: string;
  metricOptions: Record<string, string>;
  metricValue: string;
  name: keyof ICVSS3TemporalMetrics;
}

const Col = styled.div.attrs({
  className: "pa1 w-100 mb3-l mb2-m mb1-ns",
})``;

const SeverityTile: React.FC<Readonly<ISeverityTile>> = ({
  color,
  name,
  metricOptions,
  metricValue,
}): JSX.Element => {
  const { t } = useTranslation();
  const valueText = t(metricOptions[metricValue]);
  const imageName = (
    first(`${name}${translate.t(valueText)}`.split(" ")) as string
  )
    .replace(/\W/u, "")
    .replace("Subsequent", "")
    .replace("Vulnerable", "");

  const tip = (
    <TooltipContent
      bgColor={mappedColorsV3[name]}
      metricOptions={metricOptions}
      metricValue={metricValue}
    />
  );

  return (
    <Col>
      <Tooltip effect={"float"} id={imageName} nodeTip={tip}>
        <Container
          alignItems={"center"}
          display={"flex"}
          justify={"center"}
          mb={1}
        >
          <Container>
            <CloudImage
              alt={"severityImg"}
              publicId={`${severityImagesRoot}${imageName}`}
              width={"60px"}
            />
          </Container>
          <Container ml={1}>
            <Text fontWeight={"bold"} size={"sm"}>
              {t(`searchFindings.tabSeverity.${name}.label`)}
            </Text>
            <Container
              alignItems={"center"}
              display={"flex"}
              justify={"center"}
            >
              <Container mr={0.25}>
                <span className={`dib br-100 pa1 ${color}`} />
              </Container>
              <Text size={"xs"}>{valueText}</Text>
            </Container>
          </Container>
        </Container>
      </Tooltip>
    </Col>
  );
};

export type { ISeverityTile };
export { SeverityTile };
