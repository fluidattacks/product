import { screen } from "@testing-library/react";

import { SeverityContent } from ".";
import { render } from "mocks";
import type { ICVSS4ThreatMetrics } from "utils/cvss4";
import { CVSS_V4_DEFAULT, getCVSS40Values } from "utils/cvss4";

describe("severityContent", (): void => {
  it("should render content", (): void => {
    expect.hasAssertions();

    render(<SeverityContent {...getCVSS40Values(CVSS_V4_DEFAULT)} />);

    const metrics: (keyof ICVSS4ThreatMetrics)[] = [
      "attackComplexity",
      "attackVector",
      "exploitability",
      "privilegesRequired",
      "userInteraction",
      "availabilityVulnerableImpact",
      "confidentialityVulnerableImpact",
      "integrityVulnerableImpact",
      "availabilitySubsequentImpact",
      "confidentialitySubsequentImpact",
      "integritySubsequentImpact",
      "attackRequirements",
    ];

    expect(screen.getAllByRole("img")).toHaveLength(metrics.length);

    metrics.forEach((metric): void => {
      expect(
        screen.getByText(`searchFindings.tabSeverity.${metric}.label`),
      ).toBeInTheDocument();
    });
  });
});
