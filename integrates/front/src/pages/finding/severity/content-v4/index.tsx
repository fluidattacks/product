import { GridContainer } from "@fluidattacks/design";
import * as React from "react";
import { styled } from "styled-components";

import {
  attackComplexityBgColors,
  attackRequirementsBgColors,
  attackVectorBgColors,
  availabilityVulnerableImpactBgColors,
  confidentialityVulnerableImpactBgColors,
  exploitabilityBgColors,
  integrityVulnerableImpactBgColors,
  privilegesRequiredBgColors,
  userInteractionBgColors,
} from "../cvss4/utils";
import { SeverityTile } from "../tile-v4";
import {
  attackComplexityValues,
  attackRequirementsValues,
  attackVectorValues,
  availabilitySubsequentImpactValues,
  availabilityVulnerableImpactValues,
  confidentialitySubsequentImpactValues,
  confidentialityVulnerableImpactValues,
  exploitabilityValues,
  integritySubsequentImpactValues,
  integrityVulnerableImpactValues,
  privilegesRequiredValues,
  userInteractionValues,
} from "utils/cvss4";
import type { ICVSS4ThreatMetrics } from "utils/cvss4";

const FlexCol = styled.div.attrs({
  className: "flex flex-column items-center",
})``;

export const SeverityContent: React.FC<ICVSS4ThreatMetrics> = ({
  attackComplexity,
  attackVector,
  exploitability,
  privilegesRequired,
  userInteraction,
  availabilityVulnerableImpact,
  confidentialityVulnerableImpact,
  integrityVulnerableImpact,
  availabilitySubsequentImpact,
  confidentialitySubsequentImpact,
  integritySubsequentImpact,
  attackRequirements,
}): JSX.Element => {
  return (
    <GridContainer lg={4} md={4} sm={3} xl={4}>
      <FlexCol>
        <SeverityTile
          color={userInteractionBgColors[userInteraction]}
          metricOptions={userInteractionValues}
          metricValue={userInteraction}
          name={"userInteraction"}
        />
        <SeverityTile
          color={privilegesRequiredBgColors[privilegesRequired]}
          metricOptions={privilegesRequiredValues}
          metricValue={privilegesRequired}
          name={"privilegesRequired"}
        />
        <SeverityTile
          color={attackRequirementsBgColors[attackRequirements]}
          metricOptions={attackRequirementsValues}
          metricValue={attackRequirements}
          name={"attackRequirements"}
        />
      </FlexCol>
      <FlexCol>
        <SeverityTile
          color={attackVectorBgColors[attackVector]}
          metricOptions={attackVectorValues}
          metricValue={attackVector}
          name={"attackVector"}
        />
        <SeverityTile
          color={attackComplexityBgColors[attackComplexity]}
          metricOptions={attackComplexityValues}
          metricValue={attackComplexity}
          name={"attackComplexity"}
        />
        <SeverityTile
          color={exploitabilityBgColors[exploitability]}
          metricOptions={exploitabilityValues}
          metricValue={exploitability}
          name={"exploitability"}
        />
        <SeverityTile
          color={
            availabilityVulnerableImpactBgColors[availabilityVulnerableImpact]
          }
          metricOptions={availabilityVulnerableImpactValues}
          metricValue={availabilityVulnerableImpact}
          name={"availabilityVulnerableImpact"}
        />
      </FlexCol>
      <FlexCol>
        <SeverityTile
          color={
            confidentialityVulnerableImpactBgColors[
              confidentialityVulnerableImpact
            ]
          }
          metricOptions={confidentialityVulnerableImpactValues}
          metricValue={confidentialityVulnerableImpact}
          name={"confidentialityVulnerableImpact"}
        />
        <SeverityTile
          color={integrityVulnerableImpactBgColors[integrityVulnerableImpact]}
          metricOptions={integrityVulnerableImpactValues}
          metricValue={integrityVulnerableImpact}
          name={"integrityVulnerableImpact"}
        />
        <SeverityTile
          color={
            confidentialityVulnerableImpactBgColors[
              confidentialitySubsequentImpact
            ]
          }
          metricOptions={confidentialitySubsequentImpactValues}
          metricValue={confidentialitySubsequentImpact}
          name={"confidentialitySubsequentImpact"}
        />
      </FlexCol>
      <FlexCol>
        <SeverityTile
          color={integrityVulnerableImpactBgColors[integritySubsequentImpact]}
          metricOptions={integritySubsequentImpactValues}
          metricValue={integritySubsequentImpact}
          name={"integritySubsequentImpact"}
        />
        <SeverityTile
          color={
            availabilityVulnerableImpactBgColors[availabilitySubsequentImpact]
          }
          metricOptions={availabilitySubsequentImpactValues}
          metricValue={availabilitySubsequentImpact}
          name={"availabilitySubsequentImpact"}
        />
      </FlexCol>
    </GridContainer>
  );
};
