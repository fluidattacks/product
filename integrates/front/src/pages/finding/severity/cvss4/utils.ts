/* eslint-disable sort-keys -- Key order for metric values is altered for correct tooltip rendering*/
import type { ISeverityField } from "../types";
import type { ICVSS4EnvironmentalMetrics } from "utils/cvss4";
import {
  attackComplexityValues,
  attackRequirementsValues,
  attackVectorValues,
  availabilityRequirementValues,
  availabilitySubsequentImpactValues,
  availabilityVulnerableImpactValues,
  confidentialityRequirementValues,
  confidentialitySubsequentImpactValues,
  confidentialityVulnerableImpactValues,
  exploitabilityValues,
  integrityRequirementValues,
  integritySubsequentImpactValues,
  integrityVulnerableImpactValues,
  privilegesRequiredValues,
  userInteractionValues,
} from "utils/cvss4";
import { translate } from "utils/translations/translate";

const attackVectorBgColors: Record<string, string> = {
  N: "bg-dark-red",
  A: "bg-red",
  L: "bg-orange",
  P: "bg-lbl-yellow",
};

const attackComplexityBgColors: Record<string, string> = {
  L: "bg-dark-red",
  H: "bg-lbl-yellow",
};

const attackRequirementsBgColors: Record<string, string> = {
  P: "bg-lbl-yellow",
  N: "bg-dark-red",
};

const privilegesRequiredBgColors: Record<string, string> = {
  N: "bg-dark-red",
  L: "bg-orange",
  H: "bg-lbl-yellow",
};

const userInteractionBgColors: Record<string, string> = {
  N: "bg-dark-red",
  P: "bg-orange",
  R: "bg-lbl-yellow",
  A: "bg-lbl-yellow",
};

const confidentialityVulnerableImpactBgColors: Record<string, string> = {
  H: "bg-dark-red",
  L: "bg-lbl-yellow",
  N: "bg-lbl-green",
};

const integrityVulnerableImpactBgColors: Record<string, string> = {
  H: "bg-dark-red",
  L: "bg-lbl-yellow",
  N: "bg-lbl-green",
};

const remediationLevelBgColors: Record<string, string> = {
  U: "bg-dark-red",
  W: "bg-orange",
  T: "bg-lbl-yellow",
  O: "bg-lbl-green",
  X: "bg-gray",
};

const severityScopeBgColors: Record<string, string> = {
  C: "bg-dark-red",
  U: "bg-lbl-yellow",
};

const reportConfidenceBgColors: Record<string, string> = {
  C: "bg-dark-red",
  R: "bg-orange",
  U: "bg-lbl-yellow",
  X: "bg-gray",
};

const availabilityVulnerableImpactBgColors: Record<string, string> = {
  H: "bg-dark-red",
  L: "bg-lbl-yellow",
  N: "bg-lbl-green",
};

const exploitabilityBgColors: Record<string, string> = {
  A: "bg-red",
  H: "bg-dark-red",
  F: "bg-red",
  P: "bg-orange",
  U: "bg-lbl-yellow",
  X: "bg-gray",
};

const castFieldsCVSS4: (
  dataset: ICVSS4EnvironmentalMetrics,
) => ISeverityField[] = (
  dataset: ICVSS4EnvironmentalMetrics,
): ISeverityField[] => {
  const fields: ISeverityField[] = [
    // Exploitability
    {
      currentValue: dataset.attackVector,
      name: "attackVector",
      options: attackVectorValues,
      required: true,
      title: translate.t("searchFindings.tabSeverity.attackVector.label"),
    },
    {
      currentValue: dataset.attackComplexity,
      name: "attackComplexity",
      options: attackComplexityValues,
      required: true,
      title: translate.t("searchFindings.tabSeverity.attackComplexity.label"),
    },
    {
      currentValue: dataset.attackRequirements,
      name: "attackRequirements",
      options: attackRequirementsValues,
      required: true,
      title: translate.t("searchFindings.tabSeverity.attackRequirements.label"),
    },
    {
      currentValue: dataset.userInteraction,
      name: "userInteraction",
      options: userInteractionValues,
      required: true,
      title: translate.t("searchFindings.tabSeverity.userInteraction.label"),
    },
    {
      currentValue: dataset.privilegesRequired,
      name: "privilegesRequired",
      options: privilegesRequiredValues,
      required: true,
      title: translate.t("searchFindings.tabSeverity.privilegesRequired.label"),
    },
    // Impact
    {
      currentValue: dataset.confidentialityVulnerableImpact,
      name: "confidentialityVulnerableImpact",
      options: confidentialityVulnerableImpactValues,
      required: true,
      title: translate.t(
        "searchFindings.tabSeverity.confidentialityVulnerableImpact.label",
      ),
    },
    {
      currentValue: dataset.integrityVulnerableImpact,
      name: "integrityVulnerableImpact",
      options: integrityVulnerableImpactValues,
      required: true,
      title: translate.t(
        "searchFindings.tabSeverity.integrityVulnerableImpact.label",
      ),
    },
    {
      currentValue: dataset.availabilityVulnerableImpact,
      name: "availabilityVulnerableImpact",
      options: availabilityVulnerableImpactValues,
      required: true,
      title: translate.t(
        "searchFindings.tabSeverity.availabilityVulnerableImpact.label",
      ),
    },
    {
      currentValue: dataset.confidentialitySubsequentImpact,
      name: "confidentialitySubsequentImpact",
      options: confidentialitySubsequentImpactValues,
      required: true,
      title: translate.t(
        "searchFindings.tabSeverity.confidentialitySubsequentImpact.label",
      ),
    },
    {
      currentValue: dataset.integritySubsequentImpact,
      name: "integritySubsequentImpact",
      options: integritySubsequentImpactValues,
      required: true,
      title: translate.t(
        "searchFindings.tabSeverity.integritySubsequentImpact.label",
      ),
    },
    {
      currentValue: dataset.availabilitySubsequentImpact,
      name: "availabilitySubsequentImpact",
      options: availabilitySubsequentImpactValues,
      required: true,
      title: translate.t(
        "searchFindings.tabSeverity.availabilitySubsequentImpact.label",
      ),
    },
    // Threat
    {
      currentValue: dataset.exploitability,
      name: "exploitability",
      options: exploitabilityValues,
      required: true,
      title: translate.t("searchFindings.tabSeverity.exploitability.label"),
    },
  ];

  const environmentFields: ISeverityField[] = [
    // Security Requirements
    {
      currentValue: dataset.confidentialityRequirement,
      name: "confidentialityRequirement",
      options: confidentialityRequirementValues,
      title: translate.t(
        "searchFindings.tabSeverity.confidentialityRequirement.label",
      ),
    },
    {
      currentValue: dataset.integrityRequirement,
      name: "integrityRequirement",
      options: integrityRequirementValues,
      title: translate.t(
        "searchFindings.tabSeverity.integrityRequirement.label",
      ),
    },
    {
      currentValue: dataset.availabilityRequirement,
      name: "availabilityRequirement",
      options: availabilityRequirementValues,
      title: translate.t(
        "searchFindings.tabSeverity.availabilityRequirement.label",
      ),
    },
    // Exploitability
    {
      currentValue: dataset.modifiedAttackVector,
      name: "modifiedAttackVector",
      options: attackVectorValues,
      title: translate.t("searchFindings.tabSeverity.modifiedAttackVector"),
    },
    {
      currentValue: dataset.modifiedAttackRequirements,
      name: "modifiedAttackRequirements",
      options: attackRequirementsValues,
      title: translate.t(
        "searchFindings.tabSeverity.modifiedAttackRequirements",
      ),
    },
    {
      currentValue: dataset.modifiedAttackComplexity,
      name: "modifiedAttackComplexity",
      options: attackComplexityValues,
      title: translate.t("searchFindings.tabSeverity.modifiedAttackComplexity"),
    },
    {
      currentValue: dataset.modifiedUserInteraction,
      name: "modifiedUserInteraction",
      options: userInteractionValues,
      title: translate.t("searchFindings.tabSeverity.modifiedUserInteraction"),
    },
    {
      currentValue: dataset.modifiedPrivilegesRequired,
      name: "modifiedPrivilegesRequired",
      options: privilegesRequiredValues,
      title: translate.t(
        "searchFindings.tabSeverity.modifiedPrivilegesRequired",
      ),
    },
    // Impact
    {
      currentValue: dataset.modifiedConfidentialityVulnerableImpact,
      name: "modifiedConfidentialityVulnerableImpact",
      options: confidentialityVulnerableImpactValues,
      title: translate.t(
        "searchFindings.tabSeverity.modifiedConfidentialityVulnerableImpact",
      ),
    },
    {
      currentValue: dataset.modifiedIntegrityVulnerableImpact,
      name: "modifiedIntegrityVulnerableImpact",
      options: integrityVulnerableImpactValues,
      title: translate.t(
        "searchFindings.tabSeverity.modifiedIntegrityVulnerableImpact",
      ),
    },
    {
      currentValue: dataset.modifiedAvailabilityVulnerableImpact,
      name: "modifiedAvailabilityVulnerableImpact",
      options: availabilityVulnerableImpactValues,
      title: translate.t(
        "searchFindings.tabSeverity.modifiedAvailabilityVulnerableImpact",
      ),
    },
    {
      currentValue: dataset.modifiedConfidentialitySubsequentImpact,
      name: "modifiedConfidentialitySubsequentImpact",
      options: confidentialitySubsequentImpactValues,
      title: translate.t(
        "searchFindings.tabSeverity.modifiedConfidentialitySubsequentImpact",
      ),
    },
    {
      currentValue: dataset.modifiedIntegritySubsequentImpact,
      name: "modifiedIntegritySubsequentImpact",
      options: integritySubsequentImpactValues,
      title: translate.t(
        "searchFindings.tabSeverity.modifiedIntegritySubsequentImpact",
      ),
    },
    {
      currentValue: dataset.modifiedAvailabilitySubsequentImpact,
      name: "modifiedAvailabilitySubsequentImpact",
      options: availabilitySubsequentImpactValues,
      title: translate.t(
        "searchFindings.tabSeverity.modifiedAvailabilitySubsequentImpact",
      ),
    },
  ];

  return [...fields, ...environmentFields];
};

export {
  attackComplexityBgColors,
  attackVectorBgColors,
  availabilityVulnerableImpactBgColors,
  castFieldsCVSS4,
  confidentialityVulnerableImpactBgColors,
  exploitabilityBgColors,
  integrityVulnerableImpactBgColors,
  privilegesRequiredBgColors,
  userInteractionBgColors,
  attackRequirementsBgColors,
  remediationLevelBgColors,
  severityScopeBgColors,
  reportConfidenceBgColors,
};
