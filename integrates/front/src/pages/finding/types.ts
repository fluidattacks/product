import type { QueryResult } from "@apollo/client";

import type {
  GetFindingHeaderQuery,
  GetFindingHeaderQueryVariables,
} from "gql/graphql";

interface IFindingDataContext {
  findingHeaderResponse?: QueryResult<
    GetFindingHeaderQuery,
    GetFindingHeaderQueryVariables
  >;
}

interface IRemoveFindingResultAttr {
  removeFinding?: {
    success: boolean;
  };
}

export type { IFindingDataContext, IRemoveFindingResultAttr };
