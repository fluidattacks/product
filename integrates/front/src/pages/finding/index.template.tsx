import { Container } from "@fluidattacks/design";
import _ from "lodash";
import { useCallback, useState } from "react";
import * as React from "react";
import { useParams } from "react-router-dom";

import { FindingHeader } from "./finding-header";
import { useFindingHeaderQuery, useIsGroupUnderReview } from "./hooks";
import { DeleteFindingModal } from "./vulnerabilities/delete-modal";

import { useTabTracking } from "hooks";
import { FrozenGroupModal } from "pages/group/frozen-group-modal";

const FindingTemplate: React.FC<
  Readonly<{ cvssVersion: string; children: JSX.Element }>
> = ({ children }): JSX.Element => {
  const { findingId } = useParams() as {
    findingId: string;
    groupName: string;
    organizationName: string;
  };

  useTabTracking("Finding");

  const denyAccess = useIsGroupUnderReview();
  const [isDeleteModalOpen, setIsDeleteModalOpen] = useState(false);
  const openDeleteModal = useCallback((): void => {
    setIsDeleteModalOpen(true);
  }, []);
  const closeDeleteModal = useCallback((): void => {
    setIsDeleteModalOpen(false);
  }, []);

  const headerData = useFindingHeaderQuery(findingId);

  if (_.isUndefined(headerData) || _.isEmpty(headerData)) {
    return <div />;
  }

  const isDraft =
    _.isEmpty(headerData.finding.releaseDate) &&
    (headerData.finding.zeroRiskSummary?.requested ?? 0) === 0;

  const calculateEstRemediationTime = (): string => {
    if (_.isNil(headerData.finding.minTimeToRemediate)) {
      return "Unknown";
    }
    const minutesInAnHour = 60;
    const rawHours =
      (headerData.finding.minTimeToRemediate *
        headerData.finding.vulnerabilitiesSummary.open) /
      minutesInAnHour;

    if (rawHours === 0) {
      return "None";
    } else if (Number.isInteger(rawHours)) {
      return `${rawHours.toFixed(0)} hours`;
    }

    return `${rawHours.toFixed(1)} hours`;
  };

  const { title: findingTitle } = headerData.finding;
  const findingStatus = headerData.finding.status as
    | "DRAFT"
    | "SAFE"
    | "VULNERABLE";

  const estRemediationTime = calculateEstRemediationTime();

  return (
    <React.StrictMode>
      <FindingHeader
        estRemediationTime={estRemediationTime}
        findingStatus={findingStatus}
        findingTitle={findingTitle}
        hacker={headerData.finding.hacker ?? undefined}
        isDraft={isDraft}
        openDeleteModal={openDeleteModal}
      />
      <Container mt={0.25} px={1.25} py={1.25}>
        {children}
      </Container>
      <DeleteFindingModal
        isOpen={isDeleteModalOpen}
        onClose={closeDeleteModal}
      />
      <FrozenGroupModal isOpen={Boolean(denyAccess)} />
    </React.StrictMode>
  );
};

export { FindingTemplate };
