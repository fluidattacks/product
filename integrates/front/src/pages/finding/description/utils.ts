import _ from "lodash";

import type {
  IUnfulfilledRequirement,
  IVulnerabilityCriteriaRequirement,
} from "./types";

import { translate } from "utils/translations/translate";

const formatFindingType = (type: string): string =>
  _.isEmpty(type)
    ? "-"
    : translate.t(`searchFindings.tabDescription.type.${type.toLowerCase()}`);

function formatRequirements(
  requirements: string[],
  criteriaData: Record<string, IVulnerabilityCriteriaRequirement> | undefined,
): IUnfulfilledRequirement[] {
  if (criteriaData === undefined || _.isEmpty(requirements)) {
    return [];
  }
  const requirementsData: IUnfulfilledRequirement[] = requirements.map(
    (key: string): IUnfulfilledRequirement => {
      return { id: key, summary: criteriaData[key].en.summary };
    },
  );

  return requirementsData;
}

function getRequirementsText(
  requirements: string[],
  criteriaData: Record<string, IVulnerabilityCriteriaRequirement> | undefined,
  language?: string,
): string[] {
  if (criteriaData === undefined || _.isEmpty(requirements)) {
    return requirements;
  }
  const requirementsSummaries: string[] = requirements.map(
    (key: string): string => {
      const summary =
        language === "ES"
          ? criteriaData[key].es.summary
          : criteriaData[key].en.summary;

      return `${key}. ${summary}`;
    },
  );

  return requirementsSummaries;
}

function validateNotEmpty(field: string | undefined): string {
  if (!_.isNil(field) && field !== "__empty__") {
    return field;
  }

  return "";
}

export {
  formatFindingType,
  formatRequirements,
  getRequirementsText,
  validateNotEmpty,
};
