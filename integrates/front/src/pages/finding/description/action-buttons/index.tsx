import { Button, Container } from "@fluidattacks/design";
import type { IconName } from "@fortawesome/free-solid-svg-icons";
import * as React from "react";
import { useTranslation } from "react-i18next";

import { Authorize } from "components/@core/authorize";

interface IActionButtonsProps {
  readonly canEdit: boolean;
  readonly isEditing: boolean;
  readonly isPristine: boolean;
  readonly onEdit: () => void;
  readonly onUpdate: () => void;
}

const ActionButtons: React.FC<IActionButtonsProps> = ({
  canEdit,
  isEditing,
  isPristine,
  onEdit,
  onUpdate,
}: IActionButtonsProps): JSX.Element => {
  const { t } = useTranslation();

  const buttonAttributes = (): {
    text: string;
    icon: IconName;
    tip: string | undefined;
  } => {
    if (isEditing) {
      return {
        icon: "times",
        text: t("searchFindings.tabDescription.editable.cancel"),
        tip: undefined,
      };
    }

    return {
      icon: "pen",
      text: t("searchFindings.tabDescription.editable.text"),
      tip: t("searchFindings.tabDescription.editable.editableTooltip"),
    };
  };

  const { icon, text, tip } = buttonAttributes();

  return (
    <Container display={"flex"} justify={"end"}>
      <Authorize
        can={"integrates_api_mutations_update_finding_description_mutate"}
        extraCondition={isEditing}
      >
        <Button
          disabled={isPristine}
          icon={"rotate-right"}
          mr={0.5}
          onClick={onUpdate}
          tooltip={t("searchFindings.tabDescription.save.tooltip")}
          variant={"primary"}
        >
          {t("searchFindings.tabDescription.save.text")}
        </Button>
      </Authorize>
      <Authorize
        can={"integrates_api_mutations_update_finding_description_mutate"}
      >
        <Button
          disabled={canEdit}
          icon={icon}
          onClick={onEdit}
          tooltip={tip}
          variant={"secondary"}
        >
          {text}
        </Button>
      </Authorize>
    </Container>
  );
};

export type { IActionButtonsProps };
export { ActionButtons };
