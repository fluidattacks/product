import { PureAbility } from "@casl/ability";
import { screen } from "@testing-library/react";

import { CustomThemeProvider } from "components/colors";
import { authzPermissionsContext } from "context/authz/config";
import { render } from "mocks";
import { ActionButtons } from "pages/finding/description/action-buttons";
import type { IActionButtonsProps } from "pages/finding/description/action-buttons";

describe("actionButtons", (): void => {
  const baseMockedProps: IActionButtonsProps = {
    canEdit: true,
    isEditing: false,
    isPristine: false,
    onEdit: jest.fn(),
    onUpdate: jest.fn(),
  };

  it("should render a component", (): void => {
    expect.hasAssertions();

    const mockedPermissions = new PureAbility<string>([
      { action: "integrates_api_mutations_update_finding_description_mutate" },
    ]);
    const { canEdit, isEditing, isPristine, onEdit, onUpdate } =
      baseMockedProps;
    render(
      <authzPermissionsContext.Provider value={mockedPermissions}>
        <CustomThemeProvider>
          <ActionButtons
            canEdit={canEdit}
            isEditing={isEditing}
            isPristine={isPristine}
            onEdit={onEdit}
            onUpdate={onUpdate}
          />
        </CustomThemeProvider>
      </authzPermissionsContext.Provider>,
    );

    expect(screen.queryByRole("button")).toBeInTheDocument();
    expect(
      screen.queryByText("searchFindings.tabDescription.editable.text"),
    ).toBeInTheDocument();
  });
});
