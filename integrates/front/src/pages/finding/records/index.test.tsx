import { PureAbility } from "@casl/ability";
import { screen, waitFor } from "@testing-library/react";
import { userEvent } from "@testing-library/user-event";
import { HttpResponse, type StrictResponse, graphql } from "msw";
import { Route, Routes } from "react-router-dom";
import type { AnyObject, InferType, Schema } from "yup";
import { ValidationError } from "yup";

import { validationSchema } from "./validations";

import { authzPermissionsContext } from "context/authz/config";
import type { GetFindingRecordsQuery as GetFindingRecords } from "gql/graphql";
import { render } from "mocks";
import { LINK } from "mocks/constants";
import { FindingRecords } from "pages/finding/records";
import { GET_FINDING_RECORDS } from "pages/finding/records/queries";

const Wrapper = ({
  mockedPermissions,
}: Readonly<{
  mockedPermissions: PureAbility<string>;
}>): JSX.Element => (
  <authzPermissionsContext.Provider value={mockedPermissions}>
    <Routes>
      <Route
        element={<FindingRecords />}
        path={
          "/orgs/:organizationName/groups/:groupName/vulns/:findingId/records"
        }
      />
    </Routes>
  </authzPermissionsContext.Provider>
);

describe("findingRecords", (): void => {
  const memoryRouter = {
    initialEntries: ["/orgs/okada/groups/TEST/vulns/422286126/records"],
  };
  const mocks = [
    graphql
      .link(LINK)
      .query(
        GET_FINDING_RECORDS,
        (): StrictResponse<{ data: GetFindingRecords }> => {
          return HttpResponse.json({
            data: {
              finding: {
                __typename: "Finding",
                id: "422286126",
                records: JSON.stringify([
                  {
                    Character: "Cobra Commander",
                    Genre: "action",
                    Release: "2013",
                    Title: "G.I. Joe: Retaliation",
                  },
                  {
                    Character: "Tony Stark",
                    Genre: "action",
                    Release: "2008",
                    Title: "Iron Man",
                  },
                ]),
              },
            },
          });
        },
      ),
  ];

  const testValidation = (
    groupName: string,
    organizationName: string,
    obj: AnyObject,
  ): InferType<Schema> => {
    return validationSchema(groupName, organizationName).validateSync(obj);
  };
  const groupName = "unittesting";
  const organizationName = "okada";

  it("should render a component", async (): Promise<void> => {
    expect.hasAssertions();

    render(<Wrapper mockedPermissions={new PureAbility<string>()} />, {
      memoryRouter,
      mocks,
    });
    await waitFor((): void => {
      expect(screen.queryAllByRole("columnheader")).toHaveLength(4);
    });
  });

  it("should render as editable", async (): Promise<void> => {
    expect.hasAssertions();

    const mockedPermissions = new PureAbility<string>([
      { action: "integrates_api_mutations_update_evidence_mutate" },
    ]);
    render(<Wrapper mockedPermissions={mockedPermissions} />, {
      memoryRouter,
      mocks,
    });
    await waitFor((): void => {
      expect(screen.queryByText("buttons.edit")).toBeInTheDocument();
    });
    await userEvent.click(screen.getByText("buttons.edit"));
    await waitFor((): void => {
      expect(screen.queryByText("buttons.update")).toBeInTheDocument();
    });

    expect(screen.queryByText("buttons.delete")).toBeInTheDocument();
  });

  it("should render as readonly", async (): Promise<void> => {
    expect.hasAssertions();

    render(<Wrapper mockedPermissions={new PureAbility<string>()} />, {
      memoryRouter,
      mocks,
    });
    await waitFor((): void => {
      expect(screen.queryAllByRole("row")).toHaveLength(3);
    });

    expect(screen.queryByText("buttons.edit")).not.toBeInTheDocument();
  });

  it("should render empty UI", async (): Promise<void> => {
    expect.hasAssertions();

    const mockedPermissions = new PureAbility<string>([
      { action: "integrates_api_mutations_update_evidence_mutate" },
    ]);
    render(<Wrapper mockedPermissions={mockedPermissions} />, {
      memoryRouter,
      mocks: [
        graphql
          .link(LINK)
          .query(
            GET_FINDING_RECORDS,
            (): StrictResponse<{ data: GetFindingRecords }> => {
              return HttpResponse.json({
                data: {
                  finding: {
                    __typename: "Finding",
                    id: "422286126",
                    records: "[]",
                  },
                },
              });
            },
          ),
      ],
    });
    await waitFor((): void => {
      expect(
        screen.queryByText("group.findings.records.empty.title"),
      ).toBeInTheDocument();
    });
    await userEvent.click(screen.getByText("group.findings.records.add"));
    await waitFor((): void => {
      expect(screen.queryByText("buttons.update")).toBeInTheDocument();
    });

    expect(screen.queryByText("buttons.cancel")).toBeInTheDocument();
  });

  it("should not render add record if empty and not permission to add", async (): Promise<void> => {
    expect.hasAssertions();

    const mockedPermissions = new PureAbility<string>([]);
    render(<Wrapper mockedPermissions={mockedPermissions} />, {
      memoryRouter,
      mocks: [
        graphql
          .link(LINK)
          .query(
            GET_FINDING_RECORDS,
            (): StrictResponse<{ data: GetFindingRecords }> => {
              return HttpResponse.json({
                data: {
                  finding: {
                    __typename: "Finding",
                    id: "422286126",
                    records: "[]",
                  },
                },
              });
            },
          ),
      ],
    });
    await waitFor((): void => {
      expect(
        screen.queryByText("group.findings.records.empty.title"),
      ).toBeInTheDocument();
    });

    expect(
      screen.queryByText("group.findings.records.empty.description"),
    ).toBeInTheDocument();
    expect(
      screen.queryByText("group.findings.records.add"),
    ).not.toBeInTheDocument();
  });

  it("should display empty file error", (): void => {
    expect.hasAssertions();

    const testEmptyFilename = (): void => {
      testValidation(groupName, organizationName, {});
    };

    expect(testEmptyFilename).toThrow(new ValidationError("Required field"));
  });

  it("should display not a csv file error", (): void => {
    expect.hasAssertions();

    const testNotCSVFile = (): void => {
      testValidation(groupName, organizationName, {
        filename: [
          new File([""], "testing.png", {
            lastModified: 1708631811207,
            type: "image/png",
          }),
        ],
      });
    };

    expect(testNotCSVFile).toThrow(
      new ValidationError("The file must have .csv extension"),
    );
  });

  it("should display wrong filename error", (): void => {
    expect.hasAssertions();

    const testWrongFilename = (): void => {
      testValidation(groupName, organizationName, {
        filename: [
          new File([""], "testing.csv", {
            lastModified: 1708631811207,
            type: "text/csv",
          }),
        ],
      });
    };

    expect(testWrongFilename).toThrow(
      new ValidationError(
        "Evidence name must have the following format " +
          "organizationName-groupName-10 alphanumeric chars.extension",
      ),
    );
  });

  it("should accept a valid csv file", (): void => {
    expect.hasAssertions();

    const testObject = {
      filename: [
        new File([""], `${organizationName}-${groupName}-1234567890.csv`, {
          lastModified: 1708631811207,
          type: "text/csv",
        }),
      ],
    };

    expect(testValidation(groupName, organizationName, testObject)).toBe(
      testObject,
    );
  });
});
