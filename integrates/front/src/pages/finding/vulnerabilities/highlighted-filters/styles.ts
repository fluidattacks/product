import { styled } from "styled-components";

const StyledWrapper = styled.div`
  display: flex;
  flex-direction: row;
  gap: ${({ theme }): string => theme.spacing[1]};

  > div {
    background-color: ${({ theme }): string => theme.palette.white};
  }
`;

export { StyledWrapper };
