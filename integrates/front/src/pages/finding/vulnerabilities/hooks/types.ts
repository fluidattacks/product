import type { ApolloError, ObservableQueryFields } from "@apollo/client";

import type { IVulnRowAttr } from "features/vulnerabilities/types";
import type {
  GetFindingHeaderQuery,
  GetFindingHeaderQueryVariables,
  GetFindingInfoQuery,
  GetFindingInfoQueryVariables,
  GetFindingVulnsQueriesQuery,
  GetFindingVulnsQueriesQueryVariables,
  GetGroupFindingsPriorityQuery,
  GetGroupFindingsPriorityQueryVariables,
} from "gql/graphql";

type TError = ApolloError | undefined;

type TFindingInfo = GetFindingInfoQuery;
type TFindingInfoVars = GetFindingInfoQueryVariables;

interface IFindingInfoResponse {
  data: {
    finding?: {
      assignees: string[];
      id: string;
      locations: string[];
      releaseDate: string;
      remediated: boolean;
      status: "SAFE" | "VULNERABLE";
      verified: boolean;
    };
  };
  error: TError;
  loading: boolean;
  refetch: ObservableQueryFields<TFindingInfo, TFindingInfoVars>["refetch"];
}

type TFindingVulns = GetFindingVulnsQueriesQuery;
type TFindingVulnsVars = GetFindingVulnsQueriesQueryVariables;

interface IFindingVulnsResponse {
  data: {
    vulnerabilities: IVulnRowAttr[];
  };
  error: TError;
  loading: boolean;
  fetchMore: ObservableQueryFields<
    TFindingVulns,
    TFindingVulnsVars
  >["fetchMore"];
  refetch: ObservableQueryFields<TFindingVulns, TFindingVulnsVars>["refetch"];
}

type TFindingHeader = GetFindingHeaderQuery;
type TFindingHeaderVars = GetFindingHeaderQueryVariables;

interface IFindingHeaderResponse {
  data: {
    finding?: TFindingHeader["finding"];
  };
  error: TError;
  loading: boolean;
  refetch: ObservableQueryFields<TFindingHeader, TFindingHeaderVars>["refetch"];
}

type TFindingsPriority = GetGroupFindingsPriorityQuery;
type TFindingsPriorityVars = GetGroupFindingsPriorityQueryVariables;

interface IFindingsPriorityResponse {
  data: {
    group?: TFindingsPriority["group"];
  };
}

export type {
  TFindingInfo,
  TFindingInfoVars,
  IFindingInfoResponse,
  TFindingVulns,
  TFindingVulnsVars,
  IFindingVulnsResponse,
  TFindingHeader,
  TFindingHeaderVars,
  IFindingHeaderResponse,
  IFindingsPriorityResponse,
  TFindingsPriorityVars,
};
