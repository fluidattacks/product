import { useQuery } from "@apollo/client";
import { useCallback, useEffect, useMemo, useState } from "react";

import type { IFindingVulnsResponse, TFindingVulnsVars } from "./types";
import type { IVulnsFragment } from "./use-vulnerability-info";
import { useVulnerabilityInfo } from "./use-vulnerability-info";

import { GET_FINDING_VULNS_QUERIES } from "../queries";
import type { IVulnRowAttr } from "features/vulnerabilities/types";
import { formatVulnerabilitiesTreatment } from "features/vulnerabilities/utils";
import { Logger } from "utils/logger";
import { msgError } from "utils/notifications";
import { translate } from "utils/translations/translate";

const FETCH_AMOUNT = 800;

const useFindingVulnsQuery = ({
  canRetrieveDrafts,
  canRetrieveHacker,
  canRetrieveZeroRisk,
  findingId,
  first,
  state,
}: TFindingVulnsVars): IFindingVulnsResponse => {
  const [isLoading, setIsLoading] = useState(false);
  const { data, error, loading, fetchMore, refetch } = useQuery(
    GET_FINDING_VULNS_QUERIES,
    {
      fetchPolicy: "network-only",
      nextFetchPolicy: "cache-first",
      onError: ({ graphQLErrors }): void => {
        graphQLErrors.forEach((err): void => {
          msgError(translate.t("groupAlerts.errorTextsad"));
          Logger.warning(
            "An error occurred loading finding vulnerability drafts",
            err,
          );
        });
      },
      variables: {
        canRetrieveDrafts,
        canRetrieveHacker,
        canRetrieveZeroRisk,
        findingId,
        first,
        state,
      },
    },
  );

  const vulnsNzr = useVulnerabilityInfo(
    data as IVulnsFragment,
    "vulnerabilitiesConnection",
  );

  const vulnsDrafts = useVulnerabilityInfo(
    data as IVulnsFragment,
    "draftsConnection",
  );

  const vulnsZr = useVulnerabilityInfo(
    data as IVulnsFragment,
    "zeroRiskConnection",
  );

  const updateLoading = useCallback((): void => {
    setIsLoading(
      loading ||
        vulnsNzr.pageInfo.hasNextPage ||
        vulnsDrafts.pageInfo.hasNextPage ||
        vulnsZr.pageInfo.hasNextPage,
    );
  }, [
    setIsLoading,
    loading,
    vulnsNzr.pageInfo,
    vulnsDrafts.pageInfo,
    vulnsZr.pageInfo,
  ]);

  const unformattedVulns = useMemo(
    (): IVulnRowAttr[] =>
      [...vulnsZr.edges, ...vulnsNzr.edges, ...vulnsDrafts.edges].map(
        (vulnerabilityEdge): IVulnRowAttr => ({
          ...vulnerabilityEdge.node,
          groupName: vulnerabilityEdge.node.groupName,
          where:
            vulnerabilityEdge.node.vulnerabilityType === "lines" &&
            vulnerabilityEdge.node.rootNickname !== null &&
            vulnerabilityEdge.node.rootNickname !== "" &&
            !vulnerabilityEdge.node.where.startsWith(
              `${vulnerabilityEdge.node.rootNickname}/`,
            )
              ? `${vulnerabilityEdge.node.rootNickname}/${vulnerabilityEdge.node.where}`
              : vulnerabilityEdge.node.where,
        }),
      ),
    [vulnsNzr.edges, vulnsDrafts.edges, vulnsZr.edges],
  );

  useEffect((): void => {
    if (vulnsDrafts.pageInfo.hasNextPage) {
      void fetchMore({
        variables: {
          afterDrafts: vulnsDrafts.pageInfo.endCursor,
          first: FETCH_AMOUNT,
        },
      });
    }
    updateLoading();
  }, [vulnsDrafts.pageInfo, fetchMore, updateLoading]);

  useEffect((): void => {
    if (vulnsZr.pageInfo.hasNextPage) {
      void fetchMore({
        variables: {
          afterZR: vulnsZr.pageInfo.endCursor,
          first: FETCH_AMOUNT,
        },
      });
    }
    updateLoading();
  }, [vulnsZr.pageInfo, fetchMore, updateLoading]);

  useEffect((): void => {
    if (vulnsNzr.pageInfo.hasNextPage) {
      void fetchMore({
        variables: {
          afterNZR: vulnsNzr.pageInfo.endCursor,
          first: FETCH_AMOUNT,
        },
      });
    }
    updateLoading();
  }, [vulnsNzr.pageInfo, fetchMore, updateLoading]);

  const vulnerabilities = useMemo((): IVulnRowAttr[] => {
    return formatVulnerabilitiesTreatment({
      organizationsGroups: undefined,
      vulnerabilities: unformattedVulns.map(
        (vulnerability): IVulnRowAttr => ({
          ...vulnerability,
          where:
            vulnerability.vulnerabilityType === "lines" &&
            vulnerability.rootNickname !== null &&
            vulnerability.rootNickname !== "" &&
            !vulnerability.where.startsWith(`${vulnerability.rootNickname}/`)
              ? `${vulnerability.rootNickname}/${vulnerability.where}`
              : vulnerability.where,
        }),
      ),
    });
  }, [unformattedVulns]);

  const newData: IFindingVulnsResponse["data"] = {
    vulnerabilities,
  };

  return {
    data: newData,
    error,
    fetchMore,
    loading: isLoading,
    refetch,
  };
};

export { useFindingVulnsQuery };
