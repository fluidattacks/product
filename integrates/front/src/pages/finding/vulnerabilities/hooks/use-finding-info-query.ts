import { useQuery } from "@apollo/client";

import type { IFindingInfoResponse, TFindingInfoVars } from "./types";

import { GET_FINDING_INFO } from "../queries";
import { Logger } from "utils/logger";
import { msgError } from "utils/notifications";
import { translate } from "utils/translations/translate";

const useFindingInfoQuery = ({
  findingId,
}: TFindingInfoVars): IFindingInfoResponse => {
  const { data, error, loading, refetch } = useQuery(GET_FINDING_INFO, {
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((err): void => {
        msgError(translate.t("groupAlerts.errorTextsad"));
        Logger.warning("An error occurred loading finding", err);
      });
    },
    variables: { findingId },
  });

  const newData: IFindingInfoResponse["data"] = {
    finding: data?.finding
      ? {
          assignees: data.finding.assignees,
          id: data.finding.id,
          locations: data.finding.locations,
          releaseDate: data.finding.releaseDate as string,
          remediated: data.finding.remediated ?? false,
          status: data.finding.status as "SAFE" | "VULNERABLE",
          verified: data.finding.verified,
        }
      : undefined,
  };

  return {
    data: newData,
    error,
    loading,
    refetch,
  };
};

export { useFindingInfoQuery };
