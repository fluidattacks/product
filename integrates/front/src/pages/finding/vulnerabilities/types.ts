import type { IVulnRowAttr } from "features/vulnerabilities/types";
import type { IHistoricTreatment } from "pages/finding/description/types";

interface IVulnerabilitiesAttr {
  findingId: string;
  historicTreatment: IHistoricTreatment[];
  id: string;
  specific: string;
  state: "REJECTED" | "SAFE" | "SUBMITTED" | "VULNERABLE";
  where: string;
  zeroRisk: string | null;
}

interface IModalConfig {
  selectedVulnerabilities: IVulnRowAttr[];
  clearSelected: () => void;
}

interface IResubmitVulnerabilitiesResultAttr {
  resubmitVulnerabilities: {
    success: boolean;
  };
}

interface IVulnerabilityEdge {
  node: IVulnRowAttr;
}

interface IVulnerabilitiesConnection {
  edges: IVulnerabilityEdge[];
  pageInfo: {
    hasNextPage: boolean;
    endCursor: string;
  };
}

interface ISendNotificationResultAttr {
  sendVulnerabilityNotification: {
    success: boolean;
  };
}

interface ICloseVulnerabilitiesResultAttr {
  closeVulnerabilities: {
    success: boolean;
  };
}

export type {
  ICloseVulnerabilitiesResultAttr,
  IModalConfig,
  IResubmitVulnerabilitiesResultAttr,
  ISendNotificationResultAttr,
  IVulnerabilitiesAttr,
  IVulnerabilityEdge,
  IVulnerabilitiesConnection,
};
