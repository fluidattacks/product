/* eslint-disable max-lines */
import type { ApolloError } from "@apollo/client";
import { useMutation } from "@apollo/client";
import { useAbility } from "@casl/react";
import { Col, Modal, useModal } from "@fluidattacks/design";
import type { ColumnDef, Row } from "@tanstack/react-table";
import { get, isEmpty, isUndefined } from "lodash";
import { useCallback, useContext, useEffect, useMemo, useState } from "react";
import * as React from "react";
import { useTranslation } from "react-i18next";
import { useParams } from "react-router-dom";

import { useFindingHeaderQuery } from "./hooks/use-finding-header-query";
import { useFindingInfoQuery } from "./hooks/use-finding-info-query";
import { useFindingVulnsQuery } from "./hooks/use-finding-vulns-query";
import { useFindingsPriority } from "./hooks/use-findings-priority";
import {
  CLOSE_VULNERABILITIES,
  RESUBMIT_VULNERABILITIES,
  SEND_VULNERABILITY_NOTIFICATION,
} from "./queries";
import type { IModalConfig, ISendNotificationResultAttr } from "./types";
import {
  allVulnsWithEqualStatus,
  allVulnsWithUpdatableSeverity,
  areAllSuccessful,
  getColumns,
  getRejectedVulns,
  getRequestedZeroRiskVulns,
  getSafeVulnsCount,
  getSubmittedVulns,
  getVulnsPendingOfAcceptance,
  getVunerableLocations,
  handleCloseVulnerabilities,
  isHackerRemoveReleased,
  isNotRejected,
  isNotVulnerable,
  isRejectedOrSafeOrSubmitted,
  isSafe,
  isVerificationNotRequested,
  isVerificationRequestedOrOnHold,
  isVulnerabilityState,
  resubmitVulnerabilityProps,
} from "./utils";

import type { IPermanentData } from "components/filter/types";
import { ModalConfirm } from "components/modal";
import { Can } from "context/authz/can";
import {
  authzGroupContext,
  authzPermissionsContext,
} from "context/authz/config";
import { Have } from "context/authz/have";
import { ExpertButton } from "features/expert-button";
import { Role as role } from "features/user-role";
import { VulnComponent } from "features/vulnerabilities";
import { ActionButtons } from "features/vulnerabilities/action-buttons";
import { VulnerabilitiesFilters } from "features/vulnerabilities/filters";
import { HandleAcceptanceModal } from "features/vulnerabilities/handle-acceptance-modal";
import { RemoveVulnerabilityModal } from "features/vulnerabilities/remove-vulnerability-modal";
import { TreatmentModal } from "features/vulnerabilities/treatment-modal";
import type { IVulnRowAttr } from "features/vulnerabilities/types";
import { UpdateSeverityModal } from "features/vulnerabilities/update-severity-modal";
import { UpdateVerificationModal } from "features/vulnerabilities/update-verification-modal";
import { UploadVulnerabilities } from "features/vulnerabilities/upload-file";
import {
  filterOutVulnerabilities,
  filterSafeVulns,
  filterZeroRisk,
  getNonSelectableVulnerabilitiesOnCloseIds,
  getNonSelectableVulnerabilitiesOnDeleteIds,
  getNonSelectableVulnerabilitiesOnReattackIds,
  getNonSelectableVulnerabilitiesOnResubmitIds,
  getNonSelectableVulnerabilitiesOnVerifyIds,
  onRemoveVulnerabilityResultHelper,
} from "features/vulnerabilities/utils";
import type {
  RemoveVulnerabilityMutationMutation,
  VulnerabilityStateReason,
} from "gql/graphql";
import { VulnerabilityState } from "gql/graphql";
import { useStoredState } from "hooks/use-stored-state";
import { useVulnerabilityStore } from "hooks/use-vulnerability-store";
import { sleep } from "utils/helpers";
import { Logger } from "utils/logger";
import { msgError, msgSuccess } from "utils/notifications";

interface IFindingVulnerabilities {
  readonly cvssVersion: {
    accessorKey: "severityTemporalScore" | "severityThreatScore";
    header: string;
  };
}

const FindingVulnerabilities: React.FC<IFindingVulnerabilities> = ({
  cvssVersion,
}: IFindingVulnerabilities): React.ReactNode => {
  const { findingId, groupName } = useParams() as {
    findingId: string;
    groupName: string;
  };
  const { t } = useTranslation();
  const permissions = useAbility(authzPermissionsContext);
  const attributes = useContext(authzGroupContext);
  const hasAdvanced = useMemo(
    (): boolean => attributes.can("has_advanced"),
    [attributes],
  );
  const shouldHideRelease = role().toLowerCase() === "hacker";
  const canRetrieveHacker = permissions.can(
    "integrates_api_resolvers_vulnerability_hacker_resolve",
  );
  const canSeeExpertButton = permissions.can("see_expert_button");

  const {
    isAcceptanceModalOpen,
    isClosing,
    isDeleteModalOpen,
    isDeleting,
    isEditing,
    isNotify,
    isReattacking,
    isResubmitting,
    isUpdatingSeverity,
    isVerifyModalOpen,
    isVerifying,
    resetVulnsStore,
    setIsAcceptanceModalOpen,
    setIsClosing,
    setIsDeleteModalOpen,
    setIsDeleting,
    setIsEditing,
    setIsNotify,
    setIsReattacking,
    setIsResubmitting,
    setIsUpdatingSeverity,
    setIsVerifyModalOpen,
    setIsVerifying,
  } = useVulnerabilityStore();
  const [storedValues] = useStoredState<IPermanentData[]>(
    "vulnerabilitiesTableFilters",
    [{ id: "currentState", value: "VULNERABLE" }],
    localStorage,
  );
  const notifyModalProps = useModal("notify-modal");
  const editionModalProps = useModal("edition-modal");

  const currentStatusFilter = storedValues.find(
    (filter): boolean => filter.id === "currentState",
  )?.value;

  const [vulnerabilitiesState, setVulnerabilitiesState] = useState<
    VulnerabilityState | undefined
  >(
    isVulnerabilityState(currentStatusFilter)
      ? currentStatusFilter
      : VulnerabilityState.Vulnerable,
  );

  const toggleHandleAcceptanceModal = useCallback((): void => {
    setIsAcceptanceModalOpen(!isAcceptanceModalOpen);
  }, [isAcceptanceModalOpen, setIsAcceptanceModalOpen]);
  const [remediationModal, setRemediationModal] = useState<IModalConfig>({
    clearSelected: (): void => undefined,
    selectedVulnerabilities: [],
  });
  const openRemediationModal = useCallback(
    (
      selectedVulnerabilities: IVulnRowAttr[],
      clearSelected: () => void,
    ): void => {
      setRemediationModal({ clearSelected, selectedVulnerabilities });
    },
    [],
  );
  const closeRemediationModal = useCallback((): void => {
    setIsVerifyModalOpen(false);
  }, [setIsVerifyModalOpen]);
  const handleCloseUpdateModal = useCallback((): void => {
    setIsEditing(false);
  }, [setIsEditing]);
  const handleCloseNotifyModal = useCallback((): void => {
    setIsNotify(false);
  }, [setIsNotify]);
  const [filteredVulnerabilities, setFilteredVulnerabilities] = useState<
    IVulnRowAttr[]
  >([]);
  const closeSeverityModal = useCallback((): void => {
    setIsUpdatingSeverity(false);
    setRemediationModal({
      clearSelected: (): void => undefined,
      selectedVulnerabilities: [],
    });
  }, [setIsUpdatingSeverity]);

  const { refetch: refetchFindingHeader } = useFindingHeaderQuery({
    canRetrieveHacker: permissions.can(
      "integrates_api_resolvers_finding_hacker_resolve",
    ),
    findingId,
  });

  const {
    data: { finding },
    refetch: refetchInfo,
  } = useFindingInfoQuery({
    findingId,
  });

  const {
    data: { group },
  } = useFindingsPriority({ groupName });

  const columns = useMemo(
    (): ColumnDef<IVulnRowAttr>[] => getColumns(group, cvssVersion),
    [group, cvssVersion],
  );

  const {
    data: { vulnerabilities },
    loading: vulnsLoading,
    refetch: vulnRefetch,
  } = useFindingVulnsQuery({
    canRetrieveDrafts: permissions.can(
      "integrates_api_resolvers_finding_drafts_connection_resolve",
    ),
    canRetrieveHacker,
    canRetrieveZeroRisk: permissions.can(
      "integrates_api_resolvers_finding_zero_risk_connection_resolve",
    ),
    findingId,
    first: 200,
    state: vulnerabilitiesState,
  });

  const [sendNotification, { loading }] = useMutation(
    SEND_VULNERABILITY_NOTIFICATION,
    {
      onCompleted: (result: ISendNotificationResultAttr): void => {
        if (result.sendVulnerabilityNotification.success) {
          msgSuccess(
            t("searchFindings.tabDescription.notify.emailNotificationText"),
            t("searchFindings.tabDescription.notify.emailNotificationTitle"),
          );
        }
      },
      onError: (updateError): void => {
        updateError.graphQLErrors.forEach((error): void => {
          msgError(
            t("searchFindings.tabDescription.notify.emailNotificationError"),
          );
          Logger.warning("An error occurred sending the notification", error);
        });
      },
    },
  );
  const handleSendNotification = useCallback((): void => {
    sendNotification({
      variables: {
        findingId,
      },
    }).catch((): void => {
      Logger.error("An error occurred sending the notification");
    });
    setIsNotify(false);
  }, [findingId, setIsNotify, sendNotification]);

  const toggleDelete = useCallback((): void => {
    const { selectedVulnerabilities } = remediationModal;
    const newVulnerabilities: IVulnRowAttr[] = filterOutVulnerabilities(
      selectedVulnerabilities,
      filterZeroRisk(vulnerabilities),
      getNonSelectableVulnerabilitiesOnDeleteIds,
      true,
      shouldHideRelease,
    );
    if (selectedVulnerabilities.length > newVulnerabilities.length) {
      setIsDeleting(!isDeleting);
      if (newVulnerabilities.length === 0) {
        msgError(t("searchFindings.tabVuln.errors.selectedVulnerabilities"));
      } else {
        setRemediationModal(
          (currentRemediation): IModalConfig => ({
            clearSelected: currentRemediation.clearSelected,
            selectedVulnerabilities: newVulnerabilities,
          }),
        );
        setIsDeleteModalOpen(true);
      }
    } else if (selectedVulnerabilities.length > 0) {
      setIsDeleteModalOpen(true);
      setIsDeleting(!isDeleting);
    } else {
      setIsDeleting(!isDeleting);
    }
  }, [
    isDeleting,
    remediationModal,
    setIsDeleteModalOpen,
    setIsDeleting,
    shouldHideRelease,
    t,
    vulnerabilities,
  ]);

  const onRequestReattack = useCallback((): void => {
    const { selectedVulnerabilities } = remediationModal;
    const newVulnerabilities: IVulnRowAttr[] = filterOutVulnerabilities(
      selectedVulnerabilities,
      filterZeroRisk(vulnerabilities),
      getNonSelectableVulnerabilitiesOnReattackIds,
      hasAdvanced,
    );

    if (selectedVulnerabilities.length > newVulnerabilities.length) {
      setIsReattacking(!isReattacking);
      if (newVulnerabilities.length === 0) {
        msgError(t("searchFindings.tabVuln.errors.selectedVulnerabilities"));
      } else {
        setRemediationModal(
          (currentRemediation): IModalConfig => ({
            clearSelected: currentRemediation.clearSelected,
            selectedVulnerabilities: newVulnerabilities,
          }),
        );
        setIsVerifyModalOpen(
          hasAdvanced
            ? true
            : newVulnerabilities.some(
                (vuln): boolean => vuln.source === "machine",
              ),
        );
      }
    } else if (selectedVulnerabilities.length > 0) {
      setIsVerifyModalOpen(true);
      setIsReattacking(!isReattacking);
    } else {
      setIsReattacking(!isReattacking);
    }
  }, [
    hasAdvanced,
    isReattacking,
    remediationModal,
    setIsReattacking,
    setIsVerifyModalOpen,
    t,
    vulnerabilities,
  ]);

  const toggleVerify = useCallback((): void => {
    const { selectedVulnerabilities } = remediationModal;
    const newVulnerabilities: IVulnRowAttr[] = filterOutVulnerabilities(
      selectedVulnerabilities,
      filterZeroRisk(vulnerabilities),
      getNonSelectableVulnerabilitiesOnVerifyIds,
    );
    if (selectedVulnerabilities.length > newVulnerabilities.length) {
      setIsVerifying(!isVerifying);
      if (newVulnerabilities.length === 0) {
        msgError(t("searchFindings.tabVuln.errors.selectedVulnerabilities"));
      } else {
        setRemediationModal(
          (currentRemediation): IModalConfig => ({
            clearSelected: currentRemediation.clearSelected,
            selectedVulnerabilities: newVulnerabilities,
          }),
        );
        setIsVerifyModalOpen(true);
      }
    } else if (selectedVulnerabilities.length > 0) {
      setIsVerifyModalOpen(true);
      setIsVerifying(!isVerifying);
    } else {
      setIsVerifying(!isVerifying);
    }
  }, [
    isVerifying,
    remediationModal,
    setIsVerifying,
    setIsVerifyModalOpen,
    t,
    vulnerabilities,
  ]);

  const refetchVulnsData = useCallback((): void => {
    const SLEEP = 1000;
    sleep(SLEEP)
      .then(async (): Promise<void> => {
        await vulnRefetch();
      })
      .catch((): void => {
        Logger.error("An error occurred refetching vulns");
      });
  }, [vulnRefetch]);

  const [resubmitVulnerability] = useMutation(
    RESUBMIT_VULNERABILITIES,
    resubmitVulnerabilityProps(findingId, refetchVulnsData),
  );
  const [closeVulnerabilities] = useMutation(CLOSE_VULNERABILITIES);
  const handleResubmit = useCallback((): void => {
    const { selectedVulnerabilities } = remediationModal;
    const newVulnerabilities: IVulnRowAttr[] = filterOutVulnerabilities(
      selectedVulnerabilities,
      filterZeroRisk(vulnerabilities),
      getNonSelectableVulnerabilitiesOnResubmitIds,
    );
    if (selectedVulnerabilities.length !== newVulnerabilities.length) {
      msgError(t("searchFindings.tabVuln.errors.selectedVulnerabilities"));
    }
    if (
      selectedVulnerabilities.length > 0 &&
      selectedVulnerabilities.length === newVulnerabilities.length
    ) {
      const selectedVulnerabilitiesIds = newVulnerabilities.map(
        (vuln): string => vuln.id,
      );
      resubmitVulnerability({
        variables: {
          findingId,
          vulnerabilities: selectedVulnerabilitiesIds,
        },
      }).catch((): void => {
        Logger.error("An error occurred resubmitting the vulnerability");
      });
      setIsResubmitting(false);
    } else {
      setIsResubmitting(true);
      setRemediationModal(
        (currentRemediation): IModalConfig => ({
          clearSelected: currentRemediation.clearSelected,
          selectedVulnerabilities: newVulnerabilities,
        }),
      );
    }
  }, [
    findingId,
    remediationModal,
    resubmitVulnerability,
    setIsResubmitting,
    t,
    vulnerabilities,
  ]);

  const handleCancelAction = useCallback((): void => {
    setIsResubmitting(false);
    setIsClosing(false);
    setIsDeleteModalOpen(false);
  }, [setIsClosing, setIsDeleteModalOpen, setIsResubmitting]);

  const onDeleteVulnerabilityResult = useCallback(
    (removeVulnerabilityResult: RemoveVulnerabilityMutationMutation): void => {
      refetchVulnsData();
      void refetchFindingHeader();
      onRemoveVulnerabilityResultHelper(removeVulnerabilityResult, t);
      setIsDeleteModalOpen(false);
      setIsDeleting(false);
      remediationModal.clearSelected();
    },
    [
      refetchFindingHeader,
      refetchVulnsData,
      remediationModal,
      setIsDeleteModalOpen,
      setIsDeleting,
      t,
    ],
  );

  const onCloseVulnerabilitiesAux = useCallback(
    async (
      newVulnerabilities: IVulnRowAttr[],
      justification: VulnerabilityStateReason,
    ): Promise<void> => {
      try {
        const results = await handleCloseVulnerabilities(
          closeVulnerabilities,
          justification,
          newVulnerabilities,
        );
        const areAllMutationValid = areAllSuccessful(results);
        if (areAllMutationValid.every(Boolean)) {
          void refetchFindingHeader();
          refetchVulnsData();
          msgSuccess(
            t("groupAlerts.closedVulnerabilitySuccess"),
            t("groupAlerts.updatedTitle"),
          );
          remediationModal.clearSelected();
          await refetchInfo();
        }
      } catch (requestError: unknown) {
        (requestError as ApolloError).graphQLErrors.forEach((error): void => {
          if (
            error.message ===
            "Exception - The vulnerability has already been closed"
          ) {
            refetchVulnsData();
            msgError(t("groupAlerts.vulnClosed"));
            remediationModal.clearSelected();
          } else {
            msgError(t("groupAlerts.errorTextsad"));
            Logger.warning("An error occurred closing vulnerabilities", error);
          }
        });
      }
    },
    [
      closeVulnerabilities,
      refetchFindingHeader,
      refetchInfo,
      refetchVulnsData,
      remediationModal,
      t,
    ],
  );

  const onCloseVulnerabilities = useCallback(
    (justification: VulnerabilityStateReason): void => {
      const { selectedVulnerabilities } = remediationModal;
      const newVulnerabilities: IVulnRowAttr[] = filterOutVulnerabilities(
        selectedVulnerabilities,
        filterZeroRisk(vulnerabilities),
        getNonSelectableVulnerabilitiesOnCloseIds,
      );
      if (selectedVulnerabilities.length !== newVulnerabilities.length) {
        msgError(t("searchFindings.tabVuln.errors.selectedVulnerabilities"));
      }
      if (
        selectedVulnerabilities.length > 0 &&
        selectedVulnerabilities.length === newVulnerabilities.length
      ) {
        void onCloseVulnerabilitiesAux(newVulnerabilities, justification);
        setIsClosing(false);
      } else {
        setIsClosing(true);
        setRemediationModal(
          (currentRemediation): IModalConfig => ({
            clearSelected: currentRemediation.clearSelected,
            selectedVulnerabilities: newVulnerabilities,
          }),
        );
      }
    },
    [
      onCloseVulnerabilitiesAux,
      remediationModal,
      setIsClosing,
      t,
      vulnerabilities,
    ],
  );

  const enabledRows = useCallback(
    (row: Row<IVulnRowAttr>): boolean => {
      if ((isVerifying || isReattacking) && isRejectedOrSafeOrSubmitted(row)) {
        return false;
      }

      if (isReattacking && isVerificationRequestedOrOnHold(row)) {
        return false;
      }

      if (isReattacking && !hasAdvanced && row.original.source !== "machine") {
        return false;
      }

      if (isVerifying && isVerificationNotRequested(row)) {
        return false;
      }

      if (isResubmitting && isNotRejected(row)) {
        return false;
      }

      if (isClosing && isNotVulnerable(row)) {
        return false;
      }

      if (isDeleting && isSafe(row)) {
        return false;
      }

      if (isHackerRemoveReleased(row, isDeleting, shouldHideRelease)) {
        return false;
      }

      return true;
    },
    [
      hasAdvanced,
      isClosing,
      isDeleting,
      isReattacking,
      isResubmitting,
      isVerifying,
      shouldHideRelease,
    ],
  );

  useEffect((): VoidFunction => {
    return (): void => {
      resetVulnsStore();
    };
  }, [resetVulnsStore]);

  if (isUndefined(finding)) {
    return null;
  }

  const notifyModal = (
    <Modal
      modalRef={{
        ...notifyModalProps,
        close: handleCloseNotifyModal,
        isOpen: isNotify,
      }}
      size={"sm"}
      title={t("searchFindings.notifyModal.body")}
    >
      <ModalConfirm
        disabled={loading}
        onCancel={handleCloseNotifyModal}
        onConfirm={handleSendNotification}
        txtCancel={t("searchFindings.notifyModal.cancel")}
        txtConfirm={t("searchFindings.notifyModal.notify")}
      />
    </Modal>
  );
  const editionModal = (
    <TreatmentModal
      findingId={findingId}
      groupName={groupName}
      handleClearSelected={get(remediationModal, "clearSelected")}
      modalProps={{
        ...editionModalProps,
        close: handleCloseUpdateModal,
        isOpen: isEditing,
      }}
      refetchData={refetchVulnsData}
      vulnerabilities={filterSafeVulns(
        remediationModal.selectedVulnerabilities,
      )}
    />
  );

  const areSelectedVulnsManual = remediationModal.selectedVulnerabilities.some(
    (vuln): boolean => vuln.source !== "machine",
  );
  const isFindingReleased =
    vulnerabilities.filter((vuln): boolean => vuln.state === "VULNERABLE")
      .length > 0 || !isEmpty(finding.releaseDate);
  const findingState =
    vulnerabilities.filter((vuln): boolean => vuln.state === "VULNERABLE")
      .length > 0
      ? "VULNERABLE"
      : finding.status;

  return (
    <React.Fragment>
      <VulnComponent
        columns={columns}
        cvssVersion={cvssVersion.accessorKey}
        extraButtons={
          <ActionButtons
            allSelectedVulnsWithUpdatableSeverity={
              remediationModal.selectedVulnerabilities.length > 0 &&
              allVulnsWithUpdatableSeverity(
                remediationModal.selectedVulnerabilities,
              ) &&
              allVulnsWithEqualStatus(remediationModal.selectedVulnerabilities)
            }
            allVulnsSelectedAreSafe={
              remediationModal.selectedVulnerabilities.length > 0 &&
              remediationModal.selectedVulnerabilities.length ===
                getSafeVulnsCount(remediationModal.selectedVulnerabilities)
            }
            areRejectedVulns={getRejectedVulns(vulnerabilities).length > 0}
            areRequestedZeroRiskVulns={
              getRequestedZeroRiskVulns(vulnerabilities).length > 0
            }
            areSelectedVulnsManual={areSelectedVulnsManual}
            areSubmittedVulns={getSubmittedVulns(vulnerabilities).length > 0}
            areVulnerableLocations={
              getVunerableLocations(
                vulnerabilities.filter(
                  (vuln): boolean =>
                    ["SAFE", "VULNERABLE"].includes(vuln.state) &&
                    vuln.zeroRisk !== "Requested",
                ),
              ).length > 0
            }
            areVulnsPendingOfAcceptance={
              getVulnsPendingOfAcceptance(vulnerabilities, "ALL_ACCEPTED")
                .length > 0
            }
            areVulnsSelected={
              remediationModal.selectedVulnerabilities.length > 0
            }
            isFindingReleased={isFindingReleased}
            isVerified={finding.verified}
            onCancel={handleCancelAction}
            onClosing={onCloseVulnerabilities}
            onDeleting={toggleDelete}
            onRequestReattack={onRequestReattack}
            onResubmit={handleResubmit}
            onVerify={toggleVerify}
            status={findingState}
          />
        }
        filters={
          <VulnerabilitiesFilters
            groupName={groupName}
            setFilteredVulns={setFilteredVulnerabilities}
            setVulnerabilitiesState={setVulnerabilitiesState}
            vulnerabilities={vulnerabilities}
          />
        }
        loading={vulnsLoading}
        nonValidOnReattackVulns={Array.from(
          new Set(
            vulnerabilities.filter(
              (vulnerability: IVulnRowAttr): boolean =>
                !hasAdvanced && vulnerability.source !== "machine",
            ),
          ),
        )}
        onVulnSelect={openRemediationModal}
        refetchData={refetchVulnsData}
        shouldHideRelease={shouldHideRelease}
        tableOptions={{ columnToggle: true, enableRowSelection: enabledRows }}
        tableRefProps={{
          columnOrder: [
            "where",
            "specific",
            "state",
            "CVSSF_SCORE",
            "CVSSF_V4_SCORE",
            "priority_score",
            "technique",
            "TREATMENT_STATUS",
            "REPORT_DATE",
          ],
          columnPinning: { left: ["where", "specific", "state"] },
          columnVisibility: {
            TREATMENT_ASSIGNED: false,
            tag: false,
            treatmentAcceptanceStatus: false,
            verification: false,
            zeroRisk: false,
          },
          id: "vulnsView",
        }}
        vulnStateProps={{
          findingState,
          isFindingReleased,
        }}
        vulnerabilities={filterZeroRisk(filteredVulnerabilities)}
      />
      <br />
      <Col>
        <Have I={"can_report_vulnerabilities"}>
          <Can do={"integrates_api_mutations_upload_file_mutate"}>
            <UploadVulnerabilities
              findingId={findingId}
              refetchData={refetchVulnsData}
            />
          </Can>
        </Have>
      </Col>
      {isVerifyModalOpen ? (
        <UpdateVerificationModal
          clearSelected={get(remediationModal, "clearSelected")}
          handleCloseModal={closeRemediationModal}
          refetchData={refetchVulnsData}
          refetchFindingAndGroup={refetchInfo}
          refetchFindingHeader={refetchFindingHeader}
          setRequestState={onRequestReattack}
          setVerifyState={toggleVerify}
          vulns={remediationModal.selectedVulnerabilities}
        />
      ) : null}
      <HandleAcceptanceModal
        findingId={findingId}
        handleCloseModal={toggleHandleAcceptanceModal}
        refetchData={refetchVulnsData}
        vulns={vulnerabilities}
      />
      {isEditing ? editionModal : null}
      {isNotify ? notifyModal : null}
      {isUpdatingSeverity ? (
        <UpdateSeverityModal
          findingId={findingId}
          handleCloseModal={closeSeverityModal}
          isOpen={isUpdatingSeverity}
          refetchData={refetchVulnsData}
          vulnerabilities={remediationModal.selectedVulnerabilities}
        />
      ) : undefined}
      {canSeeExpertButton ? (
        <ExpertButton text={t("searchFindings.helpButton")} />
      ) : null}
      <RemoveVulnerabilityModal
        onClose={handleCancelAction}
        onRemoveVulnRes={onDeleteVulnerabilityResult}
        open={isDeleteModalOpen}
        vulnerabilities={remediationModal.selectedVulnerabilities}
      />
    </React.Fragment>
  );
};

export { FindingVulnerabilities };
