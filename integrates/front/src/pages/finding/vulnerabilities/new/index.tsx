import { useApolloClient } from "@apollo/client";
import { Container } from "@fluidattacks/design";
import type { Row } from "@tanstack/table-core";
import { useCallback, useEffect } from "react";
import * as React from "react";
import { useTranslation } from "react-i18next";
import { useLocation, useNavigate, useParams } from "react-router-dom";

import { HighlightedFilters } from "../highlighted-filters";
import { useFiltersStore } from "../hooks/use-filters-store";
import { useFindingInfoQuery } from "../hooks/use-finding-info-query";
import { useFindingsPriority } from "../hooks/use-findings-priority";
import { useGetVulnsQuery } from "../hooks/use-get-vulns-query";
import { GET_VULNS } from "../queries";
import { TableActions } from "../table-actions";
import { DownloadVulnsAction } from "../table-actions/download-vulns-action";
import { UploadVulnsAction } from "../table-actions/upload-vulns-action";
import {
  disableRowForClose,
  disableRowForResubmit,
  disableRowForSeverity,
  disableRowForVerify,
} from "../table-actions/utils";
import { getColumns } from "../utils";
import { useTable } from "components/table/hooks/use-table";
import { Table } from "components/table/new";
import { ExpertButton } from "features/expert-button";
import { Role as role } from "features/user-role";
import { VulnerabilitiesFilters } from "features/vulnerabilities/filters/new";
import type { IVulnRowAttr } from "features/vulnerabilities/types";
import { VulnerabilityModal } from "features/vulnerabilities/vulnerability-modal/new";
import { useAuthz } from "hooks/use-authz";
import { useModal } from "hooks/use-modal";
import { getTreatmentDisabled } from "hooks/use-treatment-acceptance";
import { useVulnerabilityStore } from "hooks/use-vulnerability-store";
import { sleep } from "utils/helpers";

const pageSize = 10;

interface IFindingVulnerabilitiesProps {
  readonly cvssVersion: {
    accessorKey: "severityTemporalScore" | "severityThreatScore";
    header: string;
  };
  readonly onChangeCvss: () => void;
}

const FindingVulnerabilities: React.FC<IFindingVulnerabilitiesProps> = ({
  cvssVersion,
  onChangeCvss,
}): JSX.Element => {
  const { findingId, groupName, vulnerabilityId } = useParams() as {
    findingId: string;
    groupName: string;
    vulnerabilityId?: string;
  };

  const userRole = role();

  const { t } = useTranslation();
  const { pathname } = useLocation();
  const navigate = useNavigate();
  const { vulnAction, resetVulnsStore, setVulnAction } =
    useVulnerabilityStore();
  const { canMutate, hasAttribute } = useAuthz();
  const hasAdvanced = hasAttribute("has_advanced");
  const canUploadVulns = canMutate("upload_file");
  const canReportVulns = hasAttribute("can_report_vulnerabilities");
  const filtersModal = useModal("filter-slide-menu");
  const client = useApolloClient();
  const {
    data: { group },
  } = useFindingsPriority({ groupName });

  const filtersStore = useFiltersStore(`vulnerabilitiesFilters-${groupName}`);

  const {
    data: { finding },
  } = useFindingInfoQuery({
    findingId,
  });

  const { data, total, loading, fetchMore } = useGetVulnsQuery({
    filters: {
      assignees: filtersStore.assignees,
      reattack: filtersStore.reattack,
      reportedAfter: filtersStore.reportedAfter,
      reportedBefore: filtersStore.reportedBefore,
      search: filtersStore.search,
      state: filtersStore.state,
      tags: filtersStore.tags,
      technique: filtersStore.technique,
      treatment: filtersStore.treatment,
      where: filtersStore.where,
    },
    findingId,
    first: filtersStore.pagination?.pageSize ?? pageSize,
    sortBy: filtersStore.sortBy,
  });

  const treatmentsDisabled = getTreatmentDisabled(data);

  const onRowClick = useCallback(
    (row: Row<IVulnRowAttr>): void => {
      navigate(`${pathname}/${row.original.id}`, { replace: true });
    },
    [navigate, pathname],
  );

  const resetFilters = useCallback((): void => {
    filtersStore.reset?.();
    // Zustand store already bring memoized data
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const enabledRows = useCallback(
    (row: Row<IVulnRowAttr>): boolean => {
      const drafts = ["SUBMITTED", "REJECTED"];
      const status = row.original.state;
      const verification = (row.original.verification ?? "").toUpperCase();

      if (vulnAction === "reattack") {
        return (
          ["VULNERABLE"].includes(status) &&
          !["REQUESTED", "ON_HOLD"].includes(verification) &&
          (hasAdvanced || row.original.source === "machine")
        );
      }

      if (disableRowForSeverity(row, vulnAction)) {
        return false;
      }

      if (disableRowForVerify(row, vulnAction)) {
        return false;
      }

      if (vulnAction === "delete") {
        const isHacker = userRole.toLowerCase() === "hacker";
        const isAdmin = ["admin", "architect"].includes(userRole.toLowerCase());

        return (
          (isHacker && drafts.includes(status)) ||
          (isAdmin && ["VULNERABLE", ...drafts].includes(status))
        );
      }

      if (disableRowForClose(row, vulnAction)) {
        return false;
      }

      if (disableRowForResubmit(row, vulnAction)) {
        return false;
      }

      return true;
    },
    [hasAdvanced, vulnAction, userRole],
  );

  const { table } = useTable({
    columns: getColumns(group, cvssVersion),
    data,
    fetchMore,
    filters: filtersStore,
    id: "vulns",
    options: {
      actionsOptions: {
        onSearch: filtersStore.setSearch,
        openFiltersHandler: filtersModal.open,
      },
      columnOptions: {
        order: [
          "where",
          "specific",
          "state",
          "CVSSF_SCORE",
          "CVSSF_V4_SCORE",
          "priority_score",
          "technique",
          "TREATMENT_STATUS",
          "REPORT_DATE",
        ],
        pinning: { left: ["selection", "where", "specific", "state"] },
        visibility: {
          TREATMENT_ASSIGNED: false,
          tag: false,
          treatmentAcceptanceStatus: false,
          verification: false,
          zeroRisk: false,
        },
      },
      rowOptions: {
        enableMultiSelection: true,
        onClick: onRowClick,
        validator: enabledRows,
      },
    },
    total,
  });

  const onAction = useCallback(async (): Promise<void> => {
    setVulnAction("none");
    table.setRowSelection({});
    table.setPageIndex(0);
    resetFilters();
    const delayRefetch = 1500;
    await sleep(delayRefetch);
    await client.refetchQueries({ include: [GET_VULNS] });
  }, [client, table, resetFilters, setVulnAction]);

  useEffect((): VoidFunction => {
    return (): void => {
      resetVulnsStore();
    };
  }, [resetVulnsStore]);

  return (
    <React.Fragment>
      <Table
        actionsContent={
          <TableActions
            cvssVersion={cvssVersion}
            findingId={findingId}
            groupName={groupName}
            onChangeCvss={onChangeCvss}
            resetFilters={resetFilters}
            table={table}
            treatmentsDisabled={treatmentsDisabled}
          />
        }
        footerContent={
          <Container
            alignItems={"center"}
            display={"flex"}
            gap={0.5}
            justify={"space-between"}
            width={"100%"}
          >
            <Container>
              <ExpertButton
                display={"flex"}
                featurePreview={true}
                text={t("searchFindings.tabDescription.helpButton")}
              />
            </Container>
            <Container display={"flex"} gap={0.5}>
              {canUploadVulns && canReportVulns ? (
                <Container>
                  <DownloadVulnsAction
                    findingId={findingId}
                    groupName={groupName}
                  />
                </Container>
              ) : null}
              {canUploadVulns ? (
                <Container>
                  <UploadVulnsAction
                    findingId={findingId}
                    onAction={onAction}
                  />
                </Container>
              ) : undefined}
            </Container>
          </Container>
        }
        highlightedContent={
          <HighlightedFilters
            finding={finding}
            groupName={groupName}
            loading={loading}
            table={table}
          />
        }
        id={"vulns"}
        loading={loading}
        table={table}
      />
      <VulnerabilitiesFilters
        finding={finding}
        groupName={groupName}
        modalRef={filtersModal}
      />
      {vulnerabilityId !== undefined && vulnerabilityId !== "" && (
        <VulnerabilityModal
          cvssVersion={cvssVersion.accessorKey}
          findingId={findingId}
          groupName={groupName}
          onActionCompleted={onAction}
          vulnerabilityId={vulnerabilityId}
        />
      )}
    </React.Fragment>
  );
};

export { FindingVulnerabilities };
