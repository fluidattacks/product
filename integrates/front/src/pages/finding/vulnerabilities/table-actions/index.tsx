import { useApolloClient } from "@apollo/client";
import {
  Button,
  Container,
  ListItemsWrapper,
  Toggle,
} from "@fluidattacks/design";
import type { Table } from "@tanstack/react-table";
import { useCallback, useRef, useState } from "react";
import * as React from "react";
import { useTranslation } from "react-i18next";

import { ActionSwitch } from "./action-switch";
import type { IActionSwitchProps } from "./action-switch/types";
import { CloseVulnsAction } from "./close-vulns-action";
import { DeleteVulnsAction } from "./delete-vulns-action";
import { EditAction } from "./edit-action";
import { HandleAcceptanceAction } from "./handle-acceptance-action";
import { NotifyAction } from "./notify-action";
import { ReattackVulnsAction } from "./reattack-vulns-action";
import { ResubmitVulnsAction } from "./resubmit-vulns-action";
import { UpdateSeverityAction } from "./update-severity-action";
import { VerifyVulnsAction } from "./verify-vulns-action";

import { GET_VULNS } from "../queries";
import type { IVulnRowAttr } from "features/vulnerabilities/types";
import { useAuthz } from "hooks/use-authz";
import { useClickOutside } from "hooks/use-click-outside";
import type { ITreatmentAcceptanceActionsDisabled } from "hooks/use-treatment-acceptance";
import { useVulnerabilityStore } from "hooks/use-vulnerability-store";
import type { TVulnAction } from "hooks/use-vulnerability-store";
import { sleep } from "utils/helpers";

interface ITableActionsProps {
  readonly table: Table<IVulnRowAttr>;
  readonly findingId: string;
  readonly groupName: string;
  readonly cvssVersion: {
    accessorKey: "severityTemporalScore" | "severityThreatScore";
    header: string;
  };
  readonly resetFilters: () => void;
  readonly onChangeCvss: () => void;
  readonly treatmentsDisabled: ITreatmentAcceptanceActionsDisabled;
}

const TableActions: React.FC<ITableActionsProps> = ({
  table,
  findingId,
  groupName,
  resetFilters,
  cvssVersion,
  onChangeCvss,
  treatmentsDisabled,
}): JSX.Element => {
  const { t } = useTranslation();
  const client = useApolloClient();

  const { rowSelection } = table.getState();
  const { data } = table.options.meta ?? {};
  const selectedVulns = Object.keys(rowSelection).map(
    (key): IVulnRowAttr => data?.get(key) ?? ({} as IVulnRowAttr),
  );

  /** Permissions */
  const { canMutate, hasAttribute } = useAuthz();
  const canHandleAcceptance = canMutate("handle_vulnerabilities_acceptance");
  const canConfirmOrRejectZeroRisk =
    (canMutate("confirm_vulnerabilities_zero_risk") ||
      canMutate("reject_vulnerabilities_zero_risk")) &&
    hasAttribute("can_request_zero_risk");
  const canConfirmOrRejectVulns =
    canMutate("confirm_vulnerabilities") || canMutate("reject_vulnerabilities");
  const canAcceptance =
    canHandleAcceptance ||
    canConfirmOrRejectZeroRisk ||
    canConfirmOrRejectVulns;

  const canUpdateSeverity = canMutate("update_vulnerabilities_severity");
  const canReportVulns = hasAttribute("can_report_vulnerabilities");
  const canReattack =
    canMutate("request_vulnerabilities_verification") && canReportVulns;
  const canVerify =
    canMutate("verify_vulnerabilities_request") && canReportVulns;
  const canDelete = canMutate("remove_vulnerability") && canReportVulns;
  const canClose = canMutate("close_vulnerabilities") && canReportVulns;
  const canResubmit = canMutate("resubmit_vulnerabilities") && canReportVulns;

  /** Vulnerability action state */
  const { vulnAction, setVulnAction } = useVulnerabilityStore();

  /** Actions */
  const onAction = useCallback(async (): Promise<void> => {
    setVulnAction("none");
    table.setRowSelection({});
    table.setPageIndex(0);
    resetFilters();
    const delayRefetch = 1500;
    await sleep(delayRefetch);
    await client.refetchQueries({ include: [GET_VULNS] });
  }, [client, table, resetFilters, setVulnAction]);

  const onCancel = useCallback((): void => {
    setVulnAction("none");
    table.setRowSelection({});
    table.setPageIndex(0);
    resetFilters();
  }, [table, resetFilters, setVulnAction]);

  /** Actions menu */
  const additionalRef = useRef<HTMLDivElement>(null);
  const [open, setOpen] = useState(false);
  const handleDropdown = useCallback((): void => {
    setOpen((previousState): boolean => !previousState);
  }, []);
  const closeDropdown = useCallback((): void => {
    setOpen(false);
  }, []);

  useClickOutside(
    additionalRef.current,
    (): void => {
      setOpen(false);
    },
    true,
  );

  const actionSwitches: Record<TVulnAction, IActionSwitchProps | undefined> = {
    close: {
      icon: "do-not-enter",
      onCLick: closeDropdown,
      shouldDisable: selectedVulns.length > 0,
      shouldRender: canClose,
      switchAction: "close",
      text: t("searchFindings.tabVuln.buttons.close.text"),
      tooltip: t("searchFindings.tabVuln.buttonsTooltip.close"),
    },
    delete: {
      icon: "trash-alt",
      onCLick: handleDropdown,
      shouldRender: canDelete,
      switchAction: "delete",
      text: t("searchFindings.tabVuln.buttons.delete.text"),
    },
    none: undefined,
    reattack: {
      icon: "rotate",
      onCLick: closeDropdown,
      shouldDisable: selectedVulns.length > 0,
      shouldRender: canReattack,
      switchAction: "reattack",
      text: t("searchFindings.tabVuln.buttons.reattack"),
      tooltip: t("searchFindings.tabDescription.requestVerify.tooltip"),
    },
    resubmit: {
      icon: "share-from-square",
      onCLick: handleDropdown,
      shouldRender: canResubmit,
      switchAction: "resubmit",
      text: t("searchFindings.tabVuln.buttons.resubmit"),
      tooltip: t("searchFindings.tabVuln.buttonsTooltip.resubmit"),
    },
    severity: {
      icon: "ruler",
      onCLick: handleDropdown,
      shouldRender: canUpdateSeverity,
      switchAction: "severity",
      text: t("searchFindings.tabDescription.updateVulnSeverityButton"),
      tooltip: t("searchFindings.tabDescription.updateVulnSeverityTooltip"),
    },
    verify: {
      icon: "check",
      onCLick: handleDropdown,
      shouldRender: canVerify,
      switchAction: "verify",
      text: t("searchFindings.tabDescription.markVerified.text"),
    },
  };

  const dropdownSwitches: Partial<IActionSwitchProps[]> = [
    actionSwitches.severity,
    actionSwitches.verify,
    actionSwitches.resubmit,
    actionSwitches.delete,
  ];

  const outsideSwitches: Partial<IActionSwitchProps[]> = [
    actionSwitches.reattack,
    actionSwitches.close,
  ];

  const currentInterface: Record<TVulnAction, JSX.Element | undefined> = {
    close: (
      <CloseVulnsAction
        onAction={onAction}
        onCancel={onCancel}
        selectedVulns={selectedVulns}
      />
    ),
    delete: (
      <DeleteVulnsAction
        onAction={onAction}
        onCancel={onCancel}
        selectedVulns={selectedVulns}
      />
    ),
    none: dropdownSwitches.some((props): boolean =>
      // eslint-disable-next-line react/prop-types
      Boolean(props?.shouldRender),
    ) ? (
      <Button
        disabled={selectedVulns.length > 0}
        icon={"ellipsis-vertical"}
        id={"manage-vulns"}
        onClick={handleDropdown}
        variant={"ghost"}
      >
        {"Vuln. Management"}
      </Button>
    ) : undefined,
    reattack: (
      <ReattackVulnsAction
        findingId={findingId}
        onAction={onAction}
        onCancel={onCancel}
        selectedVulns={selectedVulns}
      />
    ),
    resubmit: (
      <ResubmitVulnsAction
        findingId={findingId}
        onAction={onAction}
        onCancel={onCancel}
        selectedVulns={selectedVulns}
      />
    ),
    severity: (
      <UpdateSeverityAction
        findingId={findingId}
        onAction={onAction}
        onCancel={onCancel}
        selectedVulns={selectedVulns}
      />
    ),
    verify: (
      <VerifyVulnsAction
        onAction={onAction}
        onCancel={onCancel}
        selectedVulns={selectedVulns}
      />
    ),
  };

  const numButtons = Object.values(actionSwitches).filter(
    (action): boolean => action?.shouldRender ?? false,
  );

  return (
    <Container
      alignItems={"center"}
      display={"flex"}
      flexGrow={1}
      justify={"space-between"}
    >
      <NotifyAction findingId={findingId} />
      <Container
        alignItems={"center"}
        display={"flex"}
        gap={1}
        position={"relative"}
        ref={additionalRef}
      >
        <Container px={1} py={0.5}>
          <Toggle
            defaultChecked={cvssVersion.accessorKey === "severityThreatScore"}
            leftDescription={"CVSS 4.0"}
            name={"cvssToggle"}
            onChange={onChangeCvss}
          />
        </Container>
        {currentInterface[vulnAction]}
        {numButtons.length <= 4 ? (
          <Container>
            {Object.values(actionSwitches).map(
              (props): JSX.Element | undefined => {
                if (
                  props &&
                  vulnAction !== "close" &&
                  vulnAction !== "reattack"
                ) {
                  // eslint-disable-next-line react/prop-types
                  return <ActionSwitch key={props.text} {...props} />;
                }

                return undefined;
              },
            )}
          </Container>
        ) : (
          <React.Fragment>
            <Container
              display={open ? "block" : "none"}
              left={"70px"}
              maxWidth={"100px"}
              mt={0.25}
              position={"absolute"}
              top={"35px"}
              zIndex={2}
            >
              <ListItemsWrapper
                aria-labelledby={"manage-vulns"}
                role={"menu"}
                tabIndex={-1}
              >
                {dropdownSwitches.map((props): JSX.Element | undefined => {
                  if (props) {
                    // eslint-disable-next-line react/prop-types
                    return <ActionSwitch key={props.text} {...props} />;
                  }

                  return undefined;
                })}
              </ListItemsWrapper>
            </Container>
            {outsideSwitches.map((props): JSX.Element | undefined => {
              if (props && vulnAction === "none") {
                // eslint-disable-next-line react/prop-types
                return <ActionSwitch key={props.text} {...props} />;
              }

              return undefined;
            })}
          </React.Fragment>
        )}

        {canAcceptance && vulnAction === "none" ? (
          <HandleAcceptanceAction
            additionalRef={additionalRef}
            disabled={selectedVulns.length > 0}
            findingId={findingId}
            onAction={onAction}
            treatmentsDisabled={treatmentsDisabled}
          />
        ) : null}
        {vulnAction === "none" ? (
          <EditAction
            findingId={findingId}
            groupName={groupName}
            onAction={onAction}
            selectedVulns={selectedVulns}
          />
        ) : undefined}
      </Container>
    </Container>
  );
};

export { TableActions };
