import type { ApolloError } from "@apollo/client";
import { useMutation } from "@apollo/client";
import { Button, Container } from "@fluidattacks/design";
import { useCallback } from "react";
import { useTranslation } from "react-i18next";

import { GET_FINDING_INFO, RESUBMIT_VULNERABILITIES } from "../../queries";
import type { IResubmitVulnerabilitiesResultAttr } from "../../types";
import { Authorize } from "components/@core/authorize";
import type { IVulnRowAttr } from "features/vulnerabilities/types";
import { GET_FINDING_HEADER } from "pages/finding/queries";
import { Logger } from "utils/logger";
import { showNotification } from "utils/notifications";

interface IResubmitVulnsActionProps {
  readonly findingId: string;
  readonly selectedVulns: IVulnRowAttr[];
  readonly onAction: () => Promise<void>;
  readonly onCancel: () => void;
}

const ResubmitVulnsAction = ({
  findingId,
  selectedVulns,
  onAction,
  onCancel,
}: IResubmitVulnsActionProps): JSX.Element => {
  const { t } = useTranslation();

  const [resubmit] = useMutation(RESUBMIT_VULNERABILITIES, {
    onCompleted: (data: IResubmitVulnerabilitiesResultAttr): void => {
      if (data.resubmitVulnerabilities.success) {
        showNotification(
          "success",
          t("groupAlerts.submittedVulnerabilitySuccess"),
          t("groupAlerts.updatedTitle"),
        );
      }
    },
    onError: ({ graphQLErrors }: ApolloError): void => {
      graphQLErrors.forEach((error): void => {
        showNotification("error", t("groupAlerts.errorTextsad"));
        Logger.warning("An error occurred resubmitting vulnerability", error);
      });
    },
    refetchQueries: [
      {
        query: GET_FINDING_INFO,
        variables: {
          findingId,
        },
      },
      {
        query: GET_FINDING_HEADER,
        variables: {
          findingId,
        },
      },
    ],
  });

  const onResubmit = useCallback(async (): Promise<void> => {
    await resubmit({
      variables: {
        findingId,
        vulnerabilities: selectedVulns.map((vuln): string => vuln.id),
      },
    });
    await onAction();
  }, [resubmit, onAction, findingId, selectedVulns]);

  return (
    <Container display={"flex"} gap={0.75}>
      <Authorize
        can={"integrates_api_mutations_resubmit_vulnerabilities_mutate"}
        have={"can_report_vulnerabilities"}
      >
        <Button
          disabled={selectedVulns.length === 0}
          icon={"share-from-square"}
          id={"resubmit"}
          onClick={onResubmit}
          variant={"primary"}
          width={"100%"}
        >
          {t("searchFindings.tabVuln.buttons.resubmit")}
        </Button>
      </Authorize>
      <Button
        id={"cancel-severity-update"}
        onClick={onCancel}
        variant={"tertiary"}
        width={"100%"}
      >
        {t("searchFindings.tabVuln.buttons.cancel")}
      </Button>
    </Container>
  );
};

export { ResubmitVulnsAction };
