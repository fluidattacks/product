import { Button, Container, useModal } from "@fluidattacks/design";
import { useCallback, useEffect } from "react";
import { useTranslation } from "react-i18next";

import type { IVulnRowAttr } from "features/vulnerabilities/types";
import { UpdateVerificationModal } from "features/vulnerabilities/update-verification-modal";
import { useVulnerabilityStore } from "hooks/use-vulnerability-store";
import { showNotification } from "utils/notifications";

interface IVerifyVulnsActionProps {
  readonly selectedVulns: IVulnRowAttr[];
  readonly onAction: () => Promise<void>;
  readonly onCancel: () => void;
}

const VerifyVulnsAction = ({
  selectedVulns,
  onAction,
  onCancel,
}: IVerifyVulnsActionProps): JSX.Element => {
  const { t } = useTranslation();
  const { open, isOpen, close } = useModal("update-verification-modal");

  const { setIsReattacking, setIsVerifying } = useVulnerabilityStore();

  useEffect((): void => {
    setIsVerifying(true);
    setIsReattacking(false);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect((): void => {
    if (isOpen && selectedVulns.length === 0) {
      showNotification(
        "warning",
        "You must select open vulnerabilities on reattack",
        "Select valid vulnerabilities",
      );
      close();
    }
  }, [isOpen, selectedVulns, close]);

  const handleNothing = useCallback((): void => undefined, []);

  return (
    <Container display={"flex"} gap={0.75}>
      <Button
        disabled={selectedVulns.length === 0}
        icon={"check"}
        id={"verify"}
        onClick={open}
        variant={"primary"}
        width={"100%"}
      >
        {t("searchFindings.tabDescription.markVerified.text")}
      </Button>
      <Button
        id={"cancel-severity-update"}
        onClick={onCancel}
        variant={"tertiary"}
        width={"100%"}
      >
        {t("searchFindings.tabVuln.buttons.cancel")}
      </Button>
      {isOpen && selectedVulns.length > 0 ? (
        <UpdateVerificationModal
          clearSelected={onAction}
          handleCloseModal={close}
          refetchData={close}
          refetchFindingAndGroup={close}
          refetchFindingHeader={close}
          setRequestState={handleNothing}
          setVerifyState={handleNothing}
          vulns={selectedVulns}
        />
      ) : null}
    </Container>
  );
};

export { VerifyVulnsAction };
