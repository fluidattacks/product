import { Button, Container } from "@fluidattacks/design";
import { useTranslation } from "react-i18next";

import type { IVulnRowAttr } from "features/vulnerabilities/types";
import { UpdateSeverityModal } from "features/vulnerabilities/update-severity-modal";
import { useModal } from "hooks/use-modal";

interface IUpdateSeverityActionProps {
  readonly findingId: string;
  readonly selectedVulns: IVulnRowAttr[];
  readonly onAction: () => Promise<void>;
  readonly onCancel: () => void;
}

const UpdateSeverityAction = ({
  findingId,
  selectedVulns,
  onAction,
  onCancel,
}: IUpdateSeverityActionProps): JSX.Element => {
  const { t } = useTranslation();
  const { open, close, isOpen } = useModal("update-severity-modal");

  return (
    <Container display={"flex"} gap={0.75}>
      <Button
        disabled={selectedVulns.length === 0}
        icon={"ruler"}
        id={"vulnerabilities-update-severity"}
        mr={0.5}
        onClick={open}
        variant={"primary"}
        width={"100%"}
      >
        {t("searchFindings.tabDescription.updateVulnSeverityButton")}
      </Button>
      <Button
        id={"cancel-severity-update"}
        onClick={onCancel}
        variant={"tertiary"}
        width={"100%"}
      >
        {t("searchFindings.tabVuln.buttons.cancel")}
      </Button>
      {isOpen ? (
        <UpdateSeverityModal
          findingId={findingId}
          handleCloseModal={close}
          isOpen={isOpen}
          refetchData={onAction}
          vulnerabilities={selectedVulns}
        />
      ) : undefined}
    </Container>
  );
};

export { UpdateSeverityAction };
