import { Button, useModal } from "@fluidattacks/design";
import { Fragment } from "react";
import { useTranslation } from "react-i18next";

import { TreatmentModal } from "features/vulnerabilities/treatment-modal";
import type { IVulnRowAttr } from "features/vulnerabilities/types";
import { filterSafeVulns } from "features/vulnerabilities/utils";
import { useVulnerabilityStore } from "hooks/use-vulnerability-store";

interface IEditActionProps {
  readonly findingId: string;
  readonly groupName: string;
  readonly selectedVulns: IVulnRowAttr[];
  readonly onAction: () => Promise<void>;
}

const EditAction = ({
  findingId,
  groupName,
  selectedVulns,
  onAction,
}: IEditActionProps): JSX.Element => {
  const { t } = useTranslation();
  const modalRef = useModal("edit-treatment-modal");
  const { open, isOpen } = modalRef;
  const { vulnAction } = useVulnerabilityStore();

  const invalidSelection = selectedVulns.some(
    (vuln): boolean => vuln.state !== "VULNERABLE",
  );

  return (
    <Fragment>
      <Button
        disabled={
          selectedVulns.length === 0 ||
          invalidSelection ||
          vulnAction !== "none"
        }
        icon={"wrench"}
        id={"vulnerabilities-edit-treatment"}
        onClick={open}
        tooltip={t("searchFindings.tabDescription.notify.tooltip")}
        variant={"primary"}
      >
        {"Edit treatment"}
      </Button>
      {isOpen ? (
        <TreatmentModal
          findingId={findingId}
          groupName={groupName}
          modalProps={modalRef}
          refetchData={onAction}
          vulnerabilities={filterSafeVulns(selectedVulns)}
        />
      ) : undefined}
    </Fragment>
  );
};

export { EditAction };
