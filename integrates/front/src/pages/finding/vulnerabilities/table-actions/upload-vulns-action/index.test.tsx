import type { MockedResponse } from "@apollo/client/testing";
import { MockedProvider } from "@apollo/client/testing";
import type { RenderResult } from "@testing-library/react";
import { fireEvent, screen, waitFor } from "@testing-library/react";
import { Route, Routes } from "react-router-dom";

import { UploadVulnsAction } from ".";
import { UPLOAD_VULNERABILITIES } from "features/vulnerabilities/queries";
import { render } from "mocks";
import { GET_FINDING_HEADER } from "pages/finding/queries";
import { GET_FINDING_INFO } from "pages/finding/vulnerabilities/queries";
import { showNotification } from "utils/notifications";

jest.mock("utils/notifications", (): Record<string, unknown> => {
  const mockedNotifications: Record<string, () => Record<string, unknown>> =
    jest.requireActual("utils/notifications");
  jest.spyOn(mockedNotifications, "showNotification").mockImplementation();

  return mockedNotifications;
});

const mockRefetchData = jest.fn();

describe("uploadFile", (): void => {
  const defaultProps = {
    findingId: "test-finding-id",
    onAction: mockRefetchData,
  };

  const renderComponent = (
    mocks: MockedResponse[] = [],
    props = defaultProps,
  ): RenderResult => {
    return render(
      <MockedProvider addTypename={false} mocks={mocks}>
        <Routes>
          <Route
            element={<UploadVulnsAction {...props} />}
            path={"/group/:groupName"}
          />
        </Routes>
      </MockedProvider>,
      { memoryRouter: { initialEntries: ["/group/test-group"] } },
    );
  };

  it("should render", (): void => {
    expect.hasAssertions();

    renderComponent();

    expect(screen.getByText(/updateVulnerabilities/iu)).toBeInTheDocument();

    jest.clearAllMocks();
  });

  it("should handle successful file upload", async (): Promise<void> => {
    expect.hasAssertions();

    const file = new File(["test content"], "test.yml", { type: "text/yaml" });
    const uploadMock = {
      request: {
        query: UPLOAD_VULNERABILITIES,
        variables: { file, findingId: "test-finding-id" },
      },
      result: {
        data: {
          uploadFile: {
            message: "",
            success: true,
          },
        },
      },
    };
    const findingMock = {
      request: {
        query: GET_FINDING_INFO,
        variables: { findingId: "test-finding-id" },
      },
      result: {
        data: {
          finding: {
            __typename: "Finding",
            assignees: [],
            id: "test-finding-id",
            locations: [],
            releaseDate: "2019-05-08",
            remediated: false,
            status: "VULNERABLE",
            verified: false,
          },
        },
      },
    };
    const findingHeaderMock = {
      request: {
        query: GET_FINDING_HEADER,
        variables: {
          canGetZRSummary: false,
          canRetrieveHacker: false,
          findingId: "test-finding-id",
        },
      },
      result: {
        data: {
          finding: {
            __typename: "Finding",
            currentState: "CREATED",
            hacker: "integratesmanager@gmail.com",
            id: "test-finding-id",
            maxOpenSeverityScore: 2.7,
            maxOpenSeverityScoreV4: 6.9,
            minTimeToRemediate: 18,
            releaseDate: "2019-08-30 09:30:13",
            status: "VULNERABLE",
            title: "038. Business information leak",
            vulnerabilitiesSummary: {
              __typename: "VulnerabilitiesSummary",
              closed: 4,
              open: 25,
              openCritical: 0,
              openHigh: 0,
              openLow: 25,
              openMedium: 0,
            },
          },
        },
      },
    };

    renderComponent([uploadMock, findingMock, findingHeaderMock]);

    fireEvent.click(screen.getByText(/updateVulnerabilities/iu));

    expect(
      screen.getByText(/searchfindings\.tabresources\.modalfiletitle/iu),
    ).toBeInTheDocument();

    expect(screen.getByRole("button", { name: /Cancel/iu })).toBeEnabled();
    expect(screen.getByRole("button", { name: /Confirm/iu })).toBeDisabled();

    const fileInput = screen.getByLabelText(/filename/iu);
    fireEvent.change(fileInput, { target: { files: [file] } });

    expect(screen.getByRole("button", { name: /Confirm/iu })).toBeEnabled();

    fireEvent.click(screen.getByRole("button", { name: /Confirm/iu }));

    await waitFor((): void => {
      expect(showNotification).toHaveBeenLastCalledWith(
        "success",
        "groupAlerts.fileUpdated",
        "groupAlerts.titleSuccess",
      );
    });
    await waitFor((): void => {
      expect(mockRefetchData).toHaveBeenCalledTimes(1);
    });
    jest.clearAllMocks();
  });
});
