import type { IconName } from "@fortawesome/free-solid-svg-icons";

import type { TVulnAction } from "hooks/use-vulnerability-store";

interface IActionSwitchProps {
  readonly onCLick?: () => void;
  readonly shouldRender: boolean;
  readonly shouldDisable?: boolean;
  readonly tooltip?: string;
  readonly text: string;
  readonly icon: IconName;
  readonly switchAction: TVulnAction;
}

export type { IActionSwitchProps };
