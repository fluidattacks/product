import { Button, Container, Icon } from "@fluidattacks/design";
import { useCallback } from "react";

import type { IActionSwitchProps } from "./types";

import { useVulnerabilityStore } from "hooks/use-vulnerability-store";

const ActionSwitch = ({
  onCLick,
  shouldRender,
  tooltip,
  text,
  icon,
  switchAction,
  shouldDisable = false,
}: IActionSwitchProps): JSX.Element | null => {
  const { setVulnAction } = useVulnerabilityStore();

  const activateActions = useCallback((): void => {
    onCLick?.();
    setVulnAction(switchAction);
  }, [onCLick, setVulnAction, switchAction]);

  if (shouldRender) {
    return (
      <Button
        disabled={shouldDisable}
        id={`${switchAction}-switch`}
        justify={"flex-start"}
        onClick={activateActions}
        tooltip={tooltip}
        variant={"ghost"}
        width={"100%"}
      >
        <Container pl={0.5}>
          <Icon
            icon={icon}
            iconSize={"xs"}
            iconType={"fa-light"}
            mr={0.25}
          ></Icon>
          {text}
        </Container>
      </Button>
    );
  }

  return null;
};

export { ActionSwitch };
