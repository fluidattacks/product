import { useMutation } from "@apollo/client";
import { Button, useConfirmDialog } from "@fluidattacks/design";
import { Fragment, useCallback } from "react";
import { useTranslation } from "react-i18next";

import { SEND_VULNERABILITY_NOTIFICATION } from "../../queries";
import type { ISendNotificationResultAttr } from "../../types";
import { Logger } from "utils/logger";
import { showNotification } from "utils/notifications";

interface INotifyActionProps {
  readonly findingId: string;
}

const NotifyAction = ({ findingId }: INotifyActionProps): JSX.Element => {
  const { t } = useTranslation();
  const { confirm, ConfirmDialog } = useConfirmDialog();

  const [sendNotification] = useMutation(SEND_VULNERABILITY_NOTIFICATION, {
    onCompleted: (result: ISendNotificationResultAttr): void => {
      if (result.sendVulnerabilityNotification.success) {
        showNotification(
          "success",
          t("searchFindings.tabDescription.notify.emailNotificationText"),
          t("searchFindings.tabDescription.notify.emailNotificationTitle"),
        );
      }
    },
    onError: (updateError): void => {
      updateError.graphQLErrors.forEach((error): void => {
        showNotification(
          "error",
          t("searchFindings.tabDescription.notify.emailNotificationError"),
        );
        Logger.warning("An error occurred sending the notification", error);
      });
    },
  });

  const handleClick = useCallback(async (): Promise<void> => {
    const confirmResult = await confirm({
      title: t("searchFindings.notifyModal.body"),
    });
    if (confirmResult) {
      await sendNotification({
        variables: {
          findingId,
        },
      });
    }
  }, [confirm, findingId, sendNotification, t]);

  return (
    <Fragment>
      <Button
        height={"40px"}
        icon={"paper-plane"}
        id={"vulnerabilities-notify"}
        onClick={handleClick}
        tooltip={t("searchFindings.tabDescription.notify.tooltip")}
        variant={"ghost"}
      />
      <ConfirmDialog />
    </Fragment>
  );
};

export { NotifyAction };
