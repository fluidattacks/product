import { useMutation } from "@apollo/client";
import {
  Button,
  ComboBox,
  Container,
  Form,
  type IItem,
  InnerForm,
  Modal,
  useModal,
} from "@fluidattacks/design";
import { useCallback, useMemo } from "react";
import { useTranslation } from "react-i18next";

import { CLOSE_VULNERABILITIES } from "../../queries";
import { handleCloseVulnerabilities } from "../../utils";
import type { IVulnRowAttr } from "features/vulnerabilities/types";
import { VulnerabilityStateReason } from "gql/graphql";
import { Logger } from "utils/logger";
import { showNotification } from "utils/notifications";

interface ICloseVulnsActionProps {
  readonly selectedVulns: IVulnRowAttr[];
  readonly onAction: () => Promise<void>;
  readonly onCancel: () => void;
}

const CloseVulnsAction = ({
  selectedVulns,
  onAction,
  onCancel,
}: ICloseVulnsActionProps): JSX.Element => {
  const { t } = useTranslation();
  const modalRef = useModal("close-vulnerabilities-modal");
  const { open, close } = modalRef;

  const [closeVulnerabilities] = useMutation(CLOSE_VULNERABILITIES, {
    onCompleted: (): void => {
      showNotification(
        "success",
        t("groupAlerts.closedVulnerabilitySuccess"),
        t("groupAlerts.updatedTitle"),
      );
    },
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error): void => {
        if (
          error.message ===
          "Exception - The vulnerability has already been closed"
        ) {
          showNotification("error", t("groupAlerts.vulnClosed"));
        } else {
          showNotification("error", t("groupAlerts.errorTextsad"));
          Logger.warning("An error occurred closing vulnerabilities", error);
        }
      });
    },
  });

  const onClose = useCallback(
    async ({
      closingReason,
    }: {
      closingReason: VulnerabilityStateReason;
    }): Promise<void> => {
      await handleCloseVulnerabilities(
        closeVulnerabilities,
        closingReason,
        selectedVulns,
      );
      await onAction();
    },
    [closeVulnerabilities, selectedVulns, onAction],
  );

  const translations = useMemo((): Record<string, string> => {
    return {
      [VulnerabilityStateReason.VerifiedAsSafe]: t(
        "vulnerability.stateReasons.verifiedAsSafe",
      ),
      [VulnerabilityStateReason.RootOrEnvironmentDeactivated]: t(
        "vulnerability.stateReasons.rootOrEnvironmentDeactivated",
      ),
      [VulnerabilityStateReason.Exclusion]: t(
        "vulnerability.stateReasons.exclusion",
      ),
      [VulnerabilityStateReason.NoJustification]: t(
        "vulnerability.stateReasons.noJustification",
      ),
      [VulnerabilityStateReason.FunctionalityNoLongerExists]: t(
        "vulnerability.stateReasons.functionalityNoLongerExists",
      ),
      [VulnerabilityStateReason.RootMovedToAnotherGroup]: t(
        "vulnerability.stateReasons.rootMovedToAnotherGroup",
      ),
    };
  }, [t]);

  const justificationOptions = useMemo(
    (): IItem[] => [
      {
        name: translations[VulnerabilityStateReason.VerifiedAsSafe],
        value: VulnerabilityStateReason.VerifiedAsSafe,
      },
      {
        name: translations[
          VulnerabilityStateReason.RootOrEnvironmentDeactivated
        ],
        value: VulnerabilityStateReason.RootOrEnvironmentDeactivated,
      },
      {
        name: translations[VulnerabilityStateReason.Exclusion],
        value: VulnerabilityStateReason.Exclusion,
      },
      {
        name: translations[
          VulnerabilityStateReason.FunctionalityNoLongerExists
        ],
        value: VulnerabilityStateReason.FunctionalityNoLongerExists,
      },
    ],
    [translations],
  );

  return (
    <Container display={"flex"} gap={0.75}>
      <Button
        disabled={selectedVulns.length === 0}
        icon={"do-not-enter"}
        id={"close"}
        onClick={open}
        variant={"primary"}
        width={"100%"}
      >
        {t("searchFindings.tabVuln.buttons.close.text")}
      </Button>
      <Button
        id={"cancel-severity-update"}
        onClick={onCancel}
        variant={"tertiary"}
        width={"100%"}
      >
        {t("searchFindings.tabVuln.buttons.cancel")}
      </Button>
      <Modal
        id={"close-vulnerabilities-modal"}
        modalRef={modalRef}
        size={"sm"}
        title={t("searchFindings.tabVuln.buttons.close.text")}
      >
        <Form
          cancelButton={{ onClick: close }}
          defaultValues={{
            closingReason: VulnerabilityStateReason.NoJustification,
          }}
          onSubmit={onClose}
        >
          <InnerForm>
            {({ control }): JSX.Element => (
              <ComboBox
                control={control}
                items={justificationOptions}
                label={"Closing reason"}
                name={"closingReason"}
                required={true}
              />
            )}
          </InnerForm>
        </Form>
      </Modal>
    </Container>
  );
};

export { CloseVulnsAction };
