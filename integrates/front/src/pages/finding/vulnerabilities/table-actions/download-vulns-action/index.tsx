import { Button } from "@fluidattacks/design";
import { useTranslation } from "react-i18next";

import { useDownloadVulnMutation } from "./hooks";

interface IDownloadVulnsActionProps {
  readonly findingId: string;
  readonly groupName: string;
}

const DownloadVulnsAction = ({
  findingId,
  groupName,
}: IDownloadVulnsActionProps): JSX.Element => {
  const { t } = useTranslation();

  const { downloadVulnerability } = useDownloadVulnMutation(
    findingId,
    groupName,
  );

  return (
    <Button
      icon={"download"}
      mb={0}
      ml={0}
      mr={0.5}
      mt={0}
      onClick={downloadVulnerability}
      variant={"primary"}
      width={"100%"}
    >
      {t("searchFindings.tabDescription.downloadVulnerabilities")}
    </Button>
  );
};

export { DownloadVulnsAction };
