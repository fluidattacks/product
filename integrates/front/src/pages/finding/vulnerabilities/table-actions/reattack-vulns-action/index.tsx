import type { ApolloError, FetchResult } from "@apollo/client";
import { useMutation } from "@apollo/client";
import { Button, Container, useModal } from "@fluidattacks/design";
import _ from "lodash";
import { useCallback, useContext, useEffect, useMemo, useState } from "react";
import { useTranslation } from "react-i18next";

import { Authorize } from "components/@core/authorize";
import { authzGroupContext } from "context/authz/config";
import { RemediationModal } from "features/remediation-modal";
import type { IRemediationValues } from "features/remediation-modal/types";
import type { IVulnRowAttr } from "features/vulnerabilities/types";
import { REQUEST_VULNERABILITIES_VERIFICATION } from "features/vulnerabilities/update-verification-modal/queries";
import { ReportedBy } from "features/vulnerabilities/update-verification-modal/reported-by";
import { handleRequestVerificationError } from "features/vulnerabilities/update-verification-modal/utils";
import type { RequestVulnerabilitiesVerificationMutation } from "gql/graphql";
import { VulnerabilityStateReason } from "gql/graphql";
import { useVulnerabilityStore } from "hooks/use-vulnerability-store";
import { showNotification } from "utils/notifications";
import { translate } from "utils/translations/translate";

interface IReattackVulnsActionsProps {
  readonly findingId: string;
  readonly selectedVulns: IVulnRowAttr[];
  readonly onAction: () => Promise<void>;
  readonly onCancel: () => void;
}

const ReattackVulnsAction = ({
  findingId,
  selectedVulns,
  onAction,
  onCancel,
}: IReattackVulnsActionsProps): JSX.Element => {
  const { t } = useTranslation();
  const modalRef = useModal("reattack-modal");
  const { open: openModal, close: closeModal } = modalRef;
  const attributes = useContext(authzGroupContext);

  const { setIsReattacking, setIsVerifying } = useVulnerabilityStore();
  const [processing, setProcessing] = useState(false);

  useEffect((): void => {
    setIsReattacking(true);
    setIsVerifying(false);
  }, [setIsReattacking, setIsVerifying]);

  const allHackersAreMachine = useMemo(
    (): boolean =>
      selectedVulns.every((vuln): boolean => vuln.source === "machine"),
    [selectedVulns],
  );

  const isContinuous = attributes.can("is_continuous");

  const currentValues = useMemo(
    (): IRemediationValues => ({
      treatmentJustification: allHackersAreMachine
        ? t(
            "searchFindings.tabDescription.remediationModal.machineJustification",
          )
        : "",
      verificationReason: VulnerabilityStateReason.VerifiedAsSafe,
    }),
    [allHackersAreMachine, t],
  );

  const [requestVerification] = useMutation(
    REQUEST_VULNERABILITIES_VERIFICATION,
  );

  const handleSubmit = useCallback(
    async (values: IRemediationValues): Promise<void> => {
      try {
        setProcessing(true);
        const CHUNK_SIZE = 80;
        const vulnsIds: string[][] = _.chunk(
          selectedVulns.map((vuln): string => vuln.id),
          CHUNK_SIZE,
        );
        const responses = vulnsIds.map(
          async (
            items,
          ): Promise<FetchResult<RequestVulnerabilitiesVerificationMutation>> =>
            requestVerification({
              variables: {
                findingId,
                justification: values.treatmentJustification,
                vulnerabilities: items,
              },
            }),
        );

        const results = await Promise.allSettled(responses);
        const errors = results.some(
          (response): boolean =>
            response.status === "rejected" ||
            (response.value.errors?.length ?? 0) > 0,
        );

        if (errors) {
          showNotification(
            "error",
            t("groupAlerts.requestedReattackFailed"),
            t("groupAlerts.updatedTitle"),
          );
          onCancel();
        } else {
          showNotification(
            "success",
            t("groupAlerts.requestedReattackSuccess"),
            t("groupAlerts.updatedTitle"),
          );
          await onAction();
        }
      } catch (requestError: unknown) {
        (requestError as ApolloError).graphQLErrors.forEach((error): void => {
          handleRequestVerificationError(error);
        });
        onCancel();
      } finally {
        closeModal();
        setProcessing(false);
      }
    },
    [
      selectedVulns,
      closeModal,
      onAction,
      onCancel,
      requestVerification,
      findingId,
      t,
    ],
  );

  const getAlertMessage = useCallback(
    (vulns: IVulnRowAttr[]): string | undefined => {
      if (vulns.length === 0) {
        return undefined;
      }

      const [head] = vulns;

      const vulnsAreCode = vulns.every(
        (vuln): boolean => vuln.vulnerabilityType === "lines",
      );

      const vulnsInSameRoot: boolean = vulns.every(
        (vuln): boolean =>
          vuln.rootNickname === head.rootNickname &&
          vuln.root?.branch === head.root?.branch,
      );

      if (vulnsAreCode && vulnsInSameRoot) {
        return translate.t(
          "searchFindings.tabDescription.remediationModal.alertMessageOne",
          {
            branch: head.root?.branch,
            repository: head.rootNickname,
          },
        );
      }

      if (vulnsAreCode) {
        return translate.t(
          "searchFindings.tabDescription.remediationModal.alertMessageMultiple",
        );
      }

      return undefined;
    },
    [],
  );

  return (
    <Container display={"flex"} gap={0.75}>
      <Authorize
        can={
          "integrates_api_mutations_request_vulnerabilities_verification_mutate"
        }
        extraCondition={isContinuous}
      >
        <Button
          disabled={selectedVulns.length === 0}
          icon={"rotate"}
          id={"confirm-reattack"}
          onClick={openModal}
          variant={"primary"}
          width={"100%"}
        >
          {t("searchFindings.tabVuln.buttons.reattack")}
        </Button>
      </Authorize>
      <Button
        id={"cancel-reattack"}
        onClick={onCancel}
        variant={"tertiary"}
        width={"100%"}
      >
        {t("searchFindings.tabVuln.buttons.cancel")}
      </Button>
      {modalRef.isOpen && selectedVulns.length > 0 ? (
        <RemediationModal
          alertMessage={getAlertMessage(selectedVulns)}
          allHackersAreMachine={allHackersAreMachine}
          isLoading={processing}
          message={t(
            "searchFindings.tabDescription.remediationModal.justification",
          )}
          modalRef={{
            ...modalRef,
            close: closeModal,
          }}
          onSubmit={handleSubmit}
          remediationType={"verifyVulnerability"}
          title={t(
            "searchFindings.tabDescription.remediationModal.titleRequest",
          )}
          values={currentValues}
        >
          {allHackersAreMachine ? <ReportedBy /> : undefined}
        </RemediationModal>
      ) : undefined}
    </Container>
  );
};

export { ReattackVulnsAction };
