import { Buffer } from "buffer";

import type { GraphQLError } from "graphql";
import _ from "lodash";

import type { ICredential, TAlertMessages } from "./types";

import { Logger } from "utils/logger";
import { translate } from "utils/translations/translate";

const getAddGitRootCredentials = (
  credentials: ICredential,
): Record<string, boolean | string | undefined> | null => {
  if (
    !(
      credentials.arn === "" &&
      credentials.key === "" &&
      credentials.user === "" &&
      credentials.password === "" &&
      credentials.token === ""
    )
  ) {
    return {
      arn: credentials.arn,
      azureOrganization:
        _.isUndefined(credentials.azureOrganization) ||
        _.isUndefined(credentials.isPat) ||
        !credentials.isPat
          ? undefined
          : credentials.azureOrganization,
      isPat: _.isUndefined(credentials.isPat) ? false : credentials.isPat,
      key:
        credentials.key === ""
          ? undefined
          : Buffer.from(credentials.key).toString("base64"),
      name: credentials.name,
      password: credentials.password,
      token: credentials.token,
      type: credentials.type,
      user: credentials.user,
    };
  }

  return null;
};

const { t } = translate;

const handleGroupCreateError = (
  graphQLErrors: readonly GraphQLError[],
  setMessages: TAlertMessages,
): void => {
  graphQLErrors.forEach((error): void => {
    switch (error.message) {
      case "Exception - Error invalid group name":
        setMessages({
          message: t("organization.tabs.groups.newGroup.invalidName"),
          type: "error",
        });
        break;
      case "Exception - User is not a member of the target organization":
        setMessages({
          message: t("organization.tabs.groups.newGroup.userNotInOrganization"),
          type: "error",
        });
        break;
      default:
        setMessages({
          message: t("groupAlerts.errorTextsad"),
          type: "error",
        });
        Logger.warning("An error occurred adding a group", error);
    }
  });
};

const handleRootCreateError = (
  graphQLErrors: readonly GraphQLError[],
  setMessages: TAlertMessages,
): void => {
  graphQLErrors.forEach((error): void => {
    switch (error.message) {
      case "Exception - Error empty value is not valid":
        setMessages({
          message: t("group.scope.git.errors.invalid"),
          type: "error",
        });
        break;
      case "Exception - Root with the same nickname already exists":
        setMessages({
          message: t("group.scope.common.errors.duplicateNickname"),
          type: "error",
        });
        break;
      case "Exception - Root with the same URL/branch already exists":
        setMessages({
          message: t("group.scope.common.errors.duplicateUrlBranch"),
          type: "error",
        });
        break;
      case "Exception - Root name should not be included in the exception pattern":
        setMessages({
          message: t("group.scope.git.errors.rootInGitignore"),
          type: "error",
        });
        break;
      case "Exception - Invalid characters":
        setMessages({
          message: t("validations.invalidChar"),
          type: "error",
        });
        break;
      default:
        setMessages({
          message: t("groupAlerts.errorTextsad"),
          type: "error",
        });
        Logger.error("Couldn't add git roots", error);
    }
  });
};

const isRepeatedNickname = (graphQLErrors: string): boolean => {
  return graphQLErrors.includes("Root with the same nickname already exists");
};

const handleEnrollmentCreateError = (
  graphQLErrors: readonly GraphQLError[],
  setMessages: TAlertMessages,
): void => {
  graphQLErrors.forEach((error): void => {
    if (error.message === "Enrollment user already exists") {
      setMessages({
        message: t("autoenrollment.messages.error.enrollmentUser"),
        type: "error",
      });
    } else {
      setMessages({
        message: t("autoenrollment.messages.error.enrollment"),
        type: "error",
      });
      Logger.error("Couldn't add enrollment user data", error);
    }
  });
};

const handleValidationError = (
  graphQLErrors: readonly GraphQLError[],
  setMessages: TAlertMessages,
): void => {
  graphQLErrors.forEach((error): void => {
    switch (error.message) {
      case "Exception - Git repository was not accessible with given credentials":
        setMessages({
          message: t("group.scope.git.errors.invalidGitCredentials"),
          type: "error",
        });
        break;
      case "Exception - Branch not found":
        setMessages({
          message: t("group.scope.git.errors.invalidBranch"),
          type: "error",
        });
        break;
      case "Exception - The URL is not valid":
        setMessages({
          message: t("group.scope.git.errors.invalid"),
          type: "error",
        });
        break;
      default:
        setMessages({
          message: t("groupAlerts.errorTextsad"),
          type: "error",
        });
        Logger.error("Couldn't validate git access", error);
    }
  });
};

const checkCodeCommitFormat = (url: string | undefined): boolean => {
  if (url === undefined) return true;

  const region = /[a-z0-9-]+/u;
  const profile = /[\w-]+/u;
  const repo = /[\w-]+/u;

  const codeCommitRegex = new RegExp(
    `^codecommit::(${region.source})://(${profile.source}@)?${repo.source}$` +
      `|^codecommit://(${profile.source}@)?${repo.source}$`,
    "u",
  );

  return codeCommitRegex.test(url);
};

export {
  checkCodeCommitFormat,
  getAddGitRootCredentials,
  handleEnrollmentCreateError,
  handleGroupCreateError,
  handleRootCreateError,
  handleValidationError,
  isRepeatedNickname,
};
