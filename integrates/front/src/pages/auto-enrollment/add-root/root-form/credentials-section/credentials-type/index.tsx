import { Alert, Link, Text } from "@fluidattacks/design";
import type { FC } from "react";
import { Fragment } from "react";
import { useTranslation } from "react-i18next";

import type { ICredentialsTypeProps } from "../types";
import { Input, TextArea } from "components/input";
import { StyledExpandedCell } from "pages/auto-enrollment/add-root/styles";

export const CredentialsType: FC<ICredentialsTypeProps> = ({
  accessChecked,
  externalId,
  values,
}): JSX.Element | null => {
  const { t } = useTranslation();

  const documentation =
    "https://help.fluidattacks.com/portal/en/kb/articles/set-up-an-aws-integration";

  if (values.credentials.type === "SSH") {
    return (
      <StyledExpandedCell>
        <TextArea
          disabled={accessChecked}
          label={t("autoenrollment.credentials.sshKey.label")}
          maxLength={20000}
          name={"credentials.key"}
          placeholder={t("autoenrollment.credentials.sshKey.hint")}
          required={true}
          tooltip={t("autoenrollment.credentials.sshKey.toolTip")}
        />
      </StyledExpandedCell>
    );
  } else if (values.credentials.type === "AWSROLE") {
    return (
      <Fragment>
        <Alert variant={"info"}>
          <Text size={"sm"}>
            {t("group.scope.git.addEnvironment.awsExternalId", {
              externalId,
            })}
          </Text>
          <Text size={"sm"}>
            {t("group.scope.git.addEnvironment.awsSetupDocs")}
            <Link href={documentation}>{t("app.link")} </Link>
          </Text>
        </Alert>
        <Input
          label={t("organization.tabs.credentials.credentialsModal.form.arn")}
          name={"credentials.arn"}
          required={true}
        />
      </Fragment>
    );
  } else if (values.credentials.type === "HTTPS") {
    return (
      <Fragment>
        {values.credentials.auth === "USER" ? (
          <Fragment>
            <Input
              disabled={accessChecked}
              label={t("autoenrollment.credentials.user.label")}
              name={"credentials.user"}
              required={true}
              tooltip={t("autoenrollment.credentials.user.toolTip")}
            />
            <Input
              disabled={accessChecked}
              label={t("autoenrollment.credentials.password.label")}
              name={"credentials.password"}
              required={true}
              tooltip={t("autoenrollment.credentials.password.toolTip")}
              type={"password"}
            />
          </Fragment>
        ) : undefined}
        {values.credentials.auth === "TOKEN" ? (
          <Fragment>
            <Input
              disabled={accessChecked}
              label={t("autoenrollment.credentials.token.label")}
              name={"credentials.token"}
              required={true}
              tooltip={t("autoenrollment.credentials.token.toolTip")}
            />
            {values.credentials.isPat ? (
              <Input
                disabled={accessChecked}
                label={t("autoenrollment.credentials.azureOrganization.label")}
                name={"credentials.azureOrganization"}
                required={true}
                tooltip={t(
                  "autoenrollment.credentials.azureOrganization.toolTip",
                )}
              />
            ) : undefined}
          </Fragment>
        ) : undefined}
      </Fragment>
    );
  }

  return null;
};
