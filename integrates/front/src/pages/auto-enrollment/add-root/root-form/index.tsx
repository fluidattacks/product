import { Buffer } from "buffer";

import { useMutation } from "@apollo/client";
import type { IAlertProps } from "@fluidattacks/design";
import { Button, Container, GridContainer } from "@fluidattacks/design";
import { Form, useFormikContext } from "formik";
import isEmpty from "lodash/isEmpty";
import type { ChangeEvent } from "react";
import { useCallback, useState } from "react";
import * as React from "react";
import { useTranslation } from "react-i18next";

import { CredentialsSection } from "./credentials-section";
import { Exclusions } from "./exclusions";
import { StyledAlert } from "./exclusions/styles";

import { VALIDATE_GIT_ACCESS } from "../../queries";
import type { IRootAttr, TEnrollPages } from "../../types";
import { handleValidationError } from "../../utils";
import { StyledExpandedCell } from "../styles";
import { chooseCredentialType, submittableCredentials } from "../utils";
import { Input } from "components/input";
import type { CredentialType } from "gql/graphql";
import { msgSuccess } from "utils/notifications";
import { validateSSHFormat } from "utils/validations";

interface IAddRootFormProps {
  readonly externalId: string;
  readonly isSubmitting: boolean;
  readonly orgId: string;
  readonly rootMessages: {
    message: string;
    type: string;
  };
  readonly setPage: React.Dispatch<React.SetStateAction<TEnrollPages>>;
  readonly setProgress: React.Dispatch<React.SetStateAction<number>>;
  readonly setRootMessages: React.Dispatch<
    React.SetStateAction<{
      message: string;
      type: string;
    }>
  >;
  readonly setShowSubmitAlert: React.Dispatch<React.SetStateAction<boolean>>;
  readonly showSubmitAlert: boolean;
}

export const RootForm: React.FC<IAddRootFormProps> = ({
  externalId,
  isSubmitting,
  orgId,
  rootMessages,
  setRootMessages,
  setPage,
  setProgress,
  setShowSubmitAlert,
  showSubmitAlert,
}: IAddRootFormProps): JSX.Element => {
  const { t } = useTranslation();
  const { dirty, values, setFieldValue } = useFormikContext<IRootAttr>();

  const [validateGitAccess, { loading }] = useMutation(VALIDATE_GIT_ACCESS);
  const [repoUrl, setRepoUrl] = useState("");
  const [accessChecked, setAccessChecked] = useState(false);

  const handleCheckAccessClick = useCallback((): void => {
    const formattedUrl = validateSSHFormat(values.url.trim())
      ? `ssh://${values.url.trim()}`
      : values.url.trim();
    const progress = 100;
    void validateGitAccess({
      onCompleted: (result): void => {
        if (result.validateGitAccess.success) {
          setProgress(progress);
          setAccessChecked(true);
          msgSuccess(
            t("autoenrollment.messages.accessChecked.body"),
            t("autoenrollment.messages.accessChecked.title"),
          );
        }
      },
      onError: (error): void => {
        setShowSubmitAlert(false);
        const { graphQLErrors } = error;
        handleValidationError(graphQLErrors, setRootMessages);
      },
      variables: {
        branch: values.branch,
        credentials: {
          arn: values.credentials.arn,
          isPat: values.credentials.isPat,
          key: values.credentials.key
            ? Buffer.from(values.credentials.key).toString("base64")
            : undefined,
          name: values.credentials.name,
          password: values.credentials.password,
          token: values.credentials.token,
          type: values.credentials.type as CredentialType,
          user: values.credentials.user,
        },
        organizationId: orgId,
        url: formattedUrl,
      },
    });
  }, [
    orgId,
    setProgress,
    setRootMessages,
    setShowSubmitAlert,
    t,
    validateGitAccess,
    values,
  ]);

  const onChangeUrl = useCallback(
    (event: ChangeEvent<HTMLInputElement>): void => {
      const urlValue = event.target.value;
      setRepoUrl(urlValue);
      chooseCredentialType(urlValue, setFieldValue);
    },
    [setFieldValue, setRepoUrl],
  );

  const goBack = useCallback((): void => {
    setPage("fastTrack");
  }, [setPage]);

  return (
    <Form>
      <GridContainer lg={2} md={2} sm={1} xl={2}>
        <Input
          disabled={accessChecked}
          id={"git-root-add-repo-url"}
          label={t("autoenrollment.url.label")}
          name={"url"}
          onBlur={onChangeUrl}
          placeholder={t("autoenrollment.url.placeHolder")}
          required={true}
          tooltip={t("autoenrollment.url.toolTip")}
        />
        <Input
          disabled={accessChecked}
          id={"branch"}
          label={t("autoenrollment.branch.label")}
          name={"branch"}
          required={true}
          tooltip={t("autoenrollment.branch.toolTip")}
        />
        <CredentialsSection
          accessChecked={accessChecked}
          externalId={externalId}
          repoUrl={repoUrl}
        />
        <StyledExpandedCell>
          <Exclusions setRootMessages={setRootMessages} />
          {!showSubmitAlert && rootMessages.message !== "" && (
            <StyledAlert variant={rootMessages.type as IAlertProps["variant"]}>
              {rootMessages.message}
            </StyledAlert>
          )}
        </StyledExpandedCell>
        <Container mb={0} ml={0} mr={0} mt={0.5}>
          {accessChecked ? (
            <Button disabled={isSubmitting} type={"submit"} variant={"primary"}>
              {t("autoenrollment.submit")}
            </Button>
          ) : (
            <Container display={"flex"}>
              <Button mr={1} onClick={goBack} variant={"primary"}>
                {t("autoenrollment.goBack")}
              </Button>
              <Button
                disabled={
                  loading ||
                  submittableCredentials(values) ||
                  !dirty ||
                  isEmpty(values.url)
                }
                onClick={handleCheckAccessClick}
                variant={"secondary"}
              >
                {t("autoenrollment.checkAccess")}
              </Button>
            </Container>
          )}
        </Container>
      </GridContainer>
    </Form>
  );
};
