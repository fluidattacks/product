import { useMutation } from "@apollo/client";
import type { ApolloError } from "@apollo/client";
import { Formik } from "formik";
import mixpanel from "mixpanel-browser";
import { useCallback, useState } from "react";
import * as React from "react";
import { useTranslation } from "react-i18next";

import { RootForm } from "./root-form";

import { ADD_ENROLLMENT, ADD_GIT_ROOT, ADD_GROUP } from "../queries";
import type { IAddRootProps, IRootAttr } from "../types";
import {
  getAddGitRootCredentials,
  handleEnrollmentCreateError,
  handleGroupCreateError,
  handleRootCreateError,
  handleValidationError,
  isRepeatedNickname,
} from "../utils";
import { validationSchema } from "../validations";
import type { Language, ServiceType, SubscriptionType } from "gql/graphql";
import { msgSuccess } from "utils/notifications";
import { pagesenseFreeTrial } from "utils/page-sense-events";
import { validateSSHFormat } from "utils/validations";

const AddRoot: React.FC<IAddRootProps> = ({
  externalId,
  initialValues,
  mutationsState,
  organizationValues,
  orgId,
  orgName,
  rootMessages,
  setPage,
  setProgress,
  setRepositoryValues,
  setRootMessages,
}: IAddRootProps): JSX.Element => {
  const { t } = useTranslation();
  const [showSubmitAlert, setShowSubmitAlert] = useState(false);
  const [isSubmitting, setIsSubmitting] = useState(false);
  const [successMutation, setSuccessMutation] = useState(mutationsState);

  const [addGroup] = useMutation(ADD_GROUP, {
    onCompleted: (result): void => {
      if (result.addGroup.success) {
        msgSuccess(
          t("organization.tabs.groups.newGroup.success"),
          t("organization.tabs.groups.newGroup.titleSuccess"),
        );
      }
    },
    onError: (error): void => {
      handleGroupCreateError(error.graphQLErrors, setRootMessages);
    },
  });

  const [addGitRoot] = useMutation(ADD_GIT_ROOT, {
    onCompleted: (result): void => {
      if (result.addGitRoot.success) {
        msgSuccess(
          t("autoenrollment.messages.success.body"),
          t("autoenrollment.messages.success.title"),
        );
      }
    },
    onError: (error): void => {
      handleRootCreateError(error.graphQLErrors, setRootMessages);
      setShowSubmitAlert(false);
    },
  });

  const [addEnrollment] = useMutation(ADD_ENROLLMENT, {
    onError: (error): void => {
      handleEnrollmentCreateError(error.graphQLErrors, setRootMessages);
    },
  });

  const addNewGroup = useCallback(
    async (groupName: string): Promise<boolean> => {
      try {
        mixpanel.track("AddGroup");
        const response = await addGroup({
          variables: {
            description: "Trial group",
            groupName: groupName.toUpperCase(),
            hasAdvanced: false,
            hasEssential: true,
            language: "EN" as Language,
            organizationName: orgName.toUpperCase(),
            service: "WHITE" as ServiceType,
            subscription: "CONTINUOUS" as SubscriptionType,
          },
        });
        const groupResult = response.data;

        return groupResult ? groupResult.addGroup.success : false;
      } catch {
        return false;
      }
    },
    [addGroup, orgName],
  );

  const addNewRoot = useCallback(
    async (groupName: string, values: IRootAttr): Promise<boolean> => {
      try {
        mixpanel.track("AddGitRoot");
        const formattedUrl = validateSSHFormat(values.url.trim())
          ? `ssh://${values.url.trim()}`
          : values.url.trim();

        const response = await addGitRoot({
          variables: {
            branch: values.branch.trim(),
            credentials: getAddGitRootCredentials(values.credentials),
            gitignore: values.exclusions,
            groupName: groupName.toUpperCase(),
            includesHealthCheck: false,
            nickname: "",
            url: formattedUrl,
            useEgress: false,
            useVpn: false,
            useZtna: false,
          },
        });
        const rootResult = response.data;
        const rootErrors = response.errors;
        const repeatedNickname = rootErrors
          ? isRepeatedNickname(rootErrors.toString())
          : false;

        return (
          (rootResult ? rootResult.addGitRoot.success : false) ||
          repeatedNickname
        );
      } catch {
        return false;
      }
    },
    [addGitRoot],
  );

  const validateAndSubmit = useCallback(
    async (values: IRootAttr): Promise<void> => {
      setIsSubmitting(true);
      pagesenseFreeTrial();
      const groupName = successMutation.group
        ? organizationValues.groupName
        : orgName;

      async function createRoot(): Promise<void> {
        if (await addNewRoot(groupName, values)) {
          localStorage.clear();
          sessionStorage.clear();
          await addEnrollment();
          location.replace(
            `/orgs/${orgName.toLowerCase()}/groups/${groupName.toLowerCase()}/vulns`,
          );
          setPage("standBy");
        } else {
          setPage("repository");
          setRootMessages({
            message: t("autoenrollment.messages.error.repository"),
            type: "error",
          });
        }
      }

      async function createGroup(): Promise<void> {
        if (successMutation.group ? true : await addNewGroup(groupName)) {
          setSuccessMutation({ ...successMutation, group: true });
          await createRoot();
        } else {
          setRootMessages({
            message: t("autoenrollment.messages.error.group"),
            type: "error",
          });
        }
      }

      try {
        setRepositoryValues(values);
        if (successMutation.organization) {
          await createGroup();
        } else {
          setRootMessages({
            message: t("autoenrollment.messages.error.organization"),
            type: "error",
          });
          mixpanel.track("AutoenrollSubmit", {
            addGroup: false,
            addOrg: false,
            addRoot: false,
            group: groupName.toLowerCase(),
            organization: orgName.toLowerCase(),
            url: values.url.trim(),
          });
        }
      } catch (error) {
        setIsSubmitting(false);
        setShowSubmitAlert(false);
        const { graphQLErrors } = error as ApolloError;
        handleValidationError(graphQLErrors, setRootMessages);
      }
    },
    [
      successMutation,
      organizationValues.groupName,
      orgName,
      addNewRoot,
      addEnrollment,
      setPage,
      setRootMessages,
      t,
      addNewGroup,
      setRepositoryValues,
    ],
  );

  return (
    <Formik
      enableReinitialize={true}
      initialValues={initialValues}
      name={"newRoot"}
      onSubmit={validateAndSubmit}
      validationSchema={validationSchema}
    >
      {(): JSX.Element => {
        return (
          <RootForm
            externalId={externalId}
            isSubmitting={isSubmitting}
            orgId={orgId}
            rootMessages={rootMessages}
            setPage={setPage}
            setProgress={setProgress}
            setRootMessages={setRootMessages}
            setShowSubmitAlert={setShowSubmitAlert}
            showSubmitAlert={showSubmitAlert}
          />
        );
      }}
    </Formik>
  );
};

export { AddRoot };
