interface IProgressTopBarProps {
  userName: string;
  progress: number;
}

export type { IProgressTopBarProps };
