import type { ApolloError, ApolloQueryResult } from "@apollo/client";

import type { GetStakeholderGroupsQuery } from "gql/graphql";

interface IGetStakeholderGroupsQueryResult {
  data: {
    me: {
      isPersonalEmail: boolean;
      userEmail: string;
      userName: string;
      group: {
        description: string;
        name: string;
      };
      organization: {
        awsExternalId: string;
        country: string;
        id: string;
        name: string;
      };
      trial: {
        completed: boolean;
        startDate: string;
      } | null;
      organizationValues: {
        groupName: string;
        organizationCountry: string;
        organizationName: string;
      };
    };
  };
  error: ApolloError | undefined;
  loading: boolean;
  refetch: () => Promise<ApolloQueryResult<GetStakeholderGroupsQuery>>;
}

export type { IGetStakeholderGroupsQueryResult };
