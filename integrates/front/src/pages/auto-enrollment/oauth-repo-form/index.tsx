import { Container, Heading } from "@fluidattacks/design";
import * as React from "react";
import { useTranslation } from "react-i18next";

import { AddOauthRootForm } from "features/add-oauth-root-form";

interface IOauthRepoFormProps {
  onAddRoot: () => void;
  setProgress: React.Dispatch<React.SetStateAction<number>>;
  trialGroupName: string | undefined;
  trialOrgId: string | undefined;
}

const OauthRepoForm: React.FC<Readonly<IOauthRepoFormProps>> = ({
  onAddRoot,
  setProgress,
  trialGroupName,
  trialOrgId,
}): JSX.Element => {
  const { t } = useTranslation();

  return (
    <Container
      alignItems={"center"}
      center={true}
      display={"flex"}
      flexDirection={"column"}
      justify={"center"}
      maxWidth={"700px"}
      py={2}
      scroll={"none"}
    >
      <Heading
        fontWeight={"bold"}
        mb={2}
        mt={2}
        size={"lg"}
        textAlign={"center"}
      >
        {t("autoenrollment.oauthFormTitle")}
      </Heading>
      <Container
        bgColor={"#ffffff"}
        border={"1px solid #e9e9ed"}
        borderRadius={"8px"}
        maxHeight={"610px"}
        maxWidth={"770px"}
        px={1}
        py={1.5}
        scroll={"y"}
        shadow={"md"}
        width={"770px"}
      >
        <AddOauthRootForm
          onUpdate={onAddRoot}
          setProgress={setProgress}
          trialGroupName={trialGroupName}
          trialOrgId={trialOrgId}
        />
      </Container>
    </Container>
  );
};

export { OauthRepoForm };
