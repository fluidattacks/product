import type { ApolloQueryResult } from "@apollo/client";

import type { TEnrollPages } from "../types";
import type { GetStakeholderGroupsQuery } from "gql/graphql";

interface IFastTrack {
  refetch: () => Promise<ApolloQueryResult<GetStakeholderGroupsQuery>>;
  setPage: React.Dispatch<React.SetStateAction<TEnrollPages>>;
}

export type { IFastTrack };
