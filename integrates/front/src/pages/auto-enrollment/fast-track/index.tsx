import {
  Button,
  CloudImage,
  Container,
  Heading,
  Text,
} from "@fluidattacks/design";
import mixpanel from "mixpanel-browser";
import { useCallback, useEffect } from "react";
import * as React from "react";
import { useTranslation } from "react-i18next";

import { useAddOrganizationNewUser } from "./hooks";
import { RepoButton, StyledLink, StyledRadio, StyledRedLink } from "./styles";
import type { IFastTrack } from "./types";

import { LanguageSidebar } from "../language-sidebar";
import { useAudit } from "hooks/use-audit";
import { useModal } from "hooks/use-modal";

const FastTrack: React.FC<IFastTrack> = ({ refetch, setPage }): JSX.Element => {
  const { t } = useTranslation();
  const [selectedProvider, setSelectedProvider] = React.useState("");
  const languageModalRef = useModal("language-modal");
  const { addNewOrganization, notLoading } = useAddOrganizationNewUser(refetch);

  const { addAuditEvent } = useAudit();
  useEffect((): void => {
    mixpanel.track("FastTrack");
    addAuditEvent("Autoenroll.FastTrack", "unknown");
  }, [addAuditEvent]);

  const repositories = [
    {
      id: "gitlab",
      text: t("components.repositoriesDropdown.gitLabButton.text"),
    },
    {
      id: "github",
      text: t("components.repositoriesDropdown.gitHubButton.text"),
    },
    {
      id: "azure",
      text: t("components.repositoriesDropdown.azureButton.text"),
    },
    {
      id: "bitbucket",
      text: t("components.repositoriesDropdown.bitbucketButton.text"),
    },
  ];

  const handleClick = useCallback((id: string): (() => void) => {
    return (): void => {
      setSelectedProvider(id);
    };
  }, []);

  const onManualClick = useCallback(async (): Promise<void> => {
    if (notLoading && (await addNewOrganization())) {
      setPage("repository");
    }
  }, [addNewOrganization, notLoading, setPage]);

  const handleOpenModal = useCallback((): void => {
    languageModalRef.open();
  }, [languageModalRef]);

  const onSubmit = useCallback((): void => {
    mixpanel.track("FastTrackOAuth", { provider: selectedProvider });
    addAuditEvent("Autoenroll.FastTrackOAuth", selectedProvider);
    window.location.assign(`/d${selectedProvider}?fast_track=true`);
  }, [addAuditEvent, selectedProvider]);

  return (
    <Container
      alignItems={"center"}
      center={true}
      display={"flex"}
      flexDirection={"column"}
      justify={"center"}
      maxWidth={"700px"}
      mb={0.75}
      mt={1.5}
      pb={3.5}
      pl={2}
      pr={2}
      pt={3.5}
      scroll={"none"}
    >
      <Button
        disabled={languageModalRef.isOpen}
        icon={"circle-info"}
        onClick={handleOpenModal}
        variant={"ghost"}
      >
        {t("autoenrollment.languages.checkLanguages")}
      </Button>
      <LanguageSidebar modalRef={languageModalRef} />
      <Heading
        fontWeight={"bold"}
        lineSpacing={2}
        mb={1.25}
        mt={2}
        size={"lg"}
        textAlign={"center"}
      >
        {t("autoenrollment.fastTrackDesktop.title")}
      </Heading>
      <Text mb={0.75} size={"sm"} textAlign={"center"}>
        {t("autoenrollment.fastTrack.subtitle")}
        <StyledLink
          href={"https://help.fluidattacks.com/portal/en/kb/compliance"}
        >
          {t("autoenrollment.fastTrack.subtitleEnd")}
        </StyledLink>
      </Text>
      <Container
        display={"flex"}
        justify={"center"}
        maxWidth={"655px"}
        width={"100%"}
        wrap={"wrap"}
      >
        {repositories.map(({ id, text }): JSX.Element | undefined => {
          return (
            <RepoButton key={id} onClick={handleClick(id)}>
              <StyledRadio
                defaultChecked={selectedProvider === id}
                id={id}
                name={"provider"}
                type={"radio"}
              />
              <CloudImage
                alt={`${id}-logo`}
                height={44}
                publicId={`integrates/login/external/${id}Icon`}
                width={44}
              />
              <Text
                color={"#000"}
                display={"inline"}
                fontWeight={"bold"}
                ml={0.5}
                size={"xl"}
              >
                {text}
              </Text>
            </RepoButton>
          );
        })}
      </Container>
      <Text mb={2.25} mt={0.75} size={"sm"} textAlign={"center"}>
        {t("autoenrollment.fastTrack.manual.text")}
        <StyledRedLink onClick={onManualClick}>
          {t("autoenrollment.fastTrack.manual.button")}
        </StyledRedLink>
      </Text>
      <Button
        disabled={!selectedProvider}
        onClick={onSubmit}
        type={"submit"}
        variant={"primary"}
      >
        {`Authenticate ${selectedProvider.charAt(0).toUpperCase() + selectedProvider.slice(1)}`}
      </Button>
    </Container>
  );
};

export { FastTrack };
