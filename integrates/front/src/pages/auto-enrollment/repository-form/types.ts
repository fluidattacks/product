import type { IOrgAttr, IRootAttr, TEnrollPages } from "../types";

export interface IRepositoryFormProps {
  externalId: string;
  initialValues: IRootAttr;
  mutationsState: {
    group: boolean;
    organization: boolean;
  };
  orgId: string;
  orgName: string;
  organizationValues: IOrgAttr;
  rootMessages: {
    message: string;
    type: string;
  };
  setPage: React.Dispatch<React.SetStateAction<TEnrollPages>>;
  setProgress: React.Dispatch<React.SetStateAction<number>>;
  setRepositoryValues: React.Dispatch<React.SetStateAction<IRootAttr>>;
  setRootMessages: React.Dispatch<
    React.SetStateAction<{
      message: string;
      type: string;
    }>
  >;
}
