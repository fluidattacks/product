import { useEffect } from "react";
import { useNavigate } from "react-router-dom";

/** Applies the start url set by the backend */
const useStartUrl = (): void => {
  const navigate = useNavigate();

  useEffect((): void => {
    const startUrl = localStorage.getItem("start_url");

    if (startUrl !== null) {
      localStorage.removeItem("start_url");
      navigate(startUrl);
    }
  }, [navigate]);
};

export { useStartUrl };
