import { useMutation } from "@apollo/client";
import type { ApolloError } from "@apollo/client";
import { Form, InnerForm, Input, Modal } from "@fluidattacks/design";
import mixpanel from "mixpanel-browser";
import { StrictMode, useCallback } from "react";
import { useTranslation } from "react-i18next";
import { useNavigate } from "react-router-dom";
import { object, string } from "yup";

import { ADD_NEW_ORGANIZATION } from "./queries";
import type { IAddOrganizationModalProps } from "./types";

import type { AddOrganizationMutation as TOrganization } from "gql/graphql";
import { Logger } from "utils/logger";
import { msgError, msgSuccess } from "utils/notifications";

const MAX_ORG_LENGTH = 10;
const MIN_ORG_LENGTH = 4;
const prefix = "sidebar.newOrganization.modal.";

const AddOrganizationModal = ({
  modalProps,
}: IAddOrganizationModalProps): JSX.Element => {
  const { t } = useTranslation();
  const navigate = useNavigate();
  const { close } = modalProps;

  const [addOrganization, { loading: submitting }] = useMutation(
    ADD_NEW_ORGANIZATION,
    {
      onCompleted: (result: TOrganization): void => {
        if (
          result.addOrganization.success &&
          result.addOrganization.organization
        ) {
          close();
          const { name } = result.addOrganization.organization;
          msgSuccess(
            t(`${prefix}success`, { name }),
            t(`${prefix}successTitle`),
          );
          navigate(`/orgs/${name.toLowerCase()}/`);
        }
      },
      onError: (error: ApolloError): void => {
        error.graphQLErrors.forEach(({ message }): void => {
          if (message.includes("organization name is invalid")) {
            msgError(t(`${prefix}invalidName`));
          } else if (message.includes("organization name is already taken")) {
            msgError(t(`${prefix}nameTaken`));
          } else if (
            message ===
            "Exception - The action is not allowed during the free trial"
          ) {
            msgError(t(`${prefix}trial`));
          } else {
            msgError(t("groupAlerts.errorTextsad"));
            Logger.warning(
              "An error occurred creating new organization",
              message,
            );
          }
        });
      },
    },
  );

  const handleSubmit = useCallback(
    (values: { name: string }): void => {
      mixpanel.track("AddOrganization");
      void addOrganization({
        variables: { name: values.name.toUpperCase() },
      });
    },
    [addOrganization],
  );

  const validationSchema = object().shape({
    name: string()
      .required(t("validations.required"))
      .min(
        MIN_ORG_LENGTH,
        t("validations.minLength", { count: MIN_ORG_LENGTH }),
      )
      .max(
        MAX_ORG_LENGTH,
        t("validations.maxLength", { count: MAX_ORG_LENGTH }),
      )
      .matches(/^[a-zA-Z]+$/u, t("validations.alphabetic")),
  });

  return (
    <StrictMode>
      <Modal modalRef={modalProps} size={"sm"} title={t(`${prefix}title`)}>
        <Form
          cancelButton={{ onClick: close }}
          confirmButton={{ disabled: submitting }}
          defaultValues={{ name: "" }}
          onSubmit={handleSubmit}
          yupSchema={validationSchema}
        >
          <InnerForm>
            {({ formState, getFieldState, register }): JSX.Element => {
              const { invalid, isTouched } = getFieldState("name");

              return (
                <Input
                  error={formState.errors.name?.message?.toString()}
                  isTouched={isTouched}
                  isValid={!invalid}
                  label={t(`${prefix}name`)}
                  name={"name"}
                  register={register}
                />
              );
            }}
          </InnerForm>
        </Form>
      </Modal>
    </StrictMode>
  );
};

export { AddOrganizationModal };
