import { graphql } from "gql";

const ADD_NEW_ORGANIZATION = graphql(`
  mutation AddOrganization($name: String!) {
    addOrganization(name: $name) {
      success
      organization {
        id
        awsExternalId
        name
      }
    }
  }
`);

export { ADD_NEW_ORGANIZATION };
