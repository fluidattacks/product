import { PureAbility } from "@casl/ability";
import { screen, waitFor } from "@testing-library/react";
import { userEvent } from "@testing-library/user-event";
import { Route, Routes } from "react-router-dom";

import { Navbar } from ".";
import { authContext } from "context/auth";
import { authzPermissionsContext } from "context/authz/config";
import { render } from "mocks";

const mockNavigatePush: jest.Mock = jest.fn();
jest.mock(
  "react-router-dom",
  (): Record<string, unknown> => ({
    ...jest.requireActual("react-router-dom"),
    useNavigate: (): jest.Mock => mockNavigatePush,
  }),
);

describe("navbar", (): void => {
  it("should render", async (): Promise<void> => {
    expect.hasAssertions();

    jest.clearAllMocks();

    const mockedPermissions = new PureAbility<string>([]);
    localStorage.setItem("organizationV2", JSON.stringify({ name: "okada" }));

    render(
      <authzPermissionsContext.Provider value={mockedPermissions}>
        <authContext.Provider
          value={{
            awsSubscription: null,
            tours: {
              newGroup: true,
              newRoot: true,
              welcome: true,
            },
            userEmail: "test@fluidattacks.com",
            userName: "test",
          }}
        >
          <Routes>
            <Route
              element={<Navbar initialOrganization={"okada"} />}
              path={"/orgs/:organizationName/groups"}
            />
          </Routes>
        </authContext.Provider>
      </authzPermissionsContext.Provider>,
      {
        memoryRouter: {
          initialEntries: ["/orgs/okada/groups"],
        },
      },
    );

    await waitFor((): void => {
      expect(screen.getByPlaceholderText("Search")).toBeInTheDocument();
    });

    expect(screen.getByRole("listitem")).toBeInTheDocument();
    expect(screen.queryByText("bulat")).not.toBeInTheDocument();

    await userEvent.click(screen.getByRole("listitem"));
    await waitFor((): void => {
      expect(screen.queryByText("Bulat")).toBeInTheDocument();
    });
    await userEvent.click(screen.getByRole("listitem"));
    await waitFor((): void => {
      expect(screen.queryByText("Bulat")).not.toBeInTheDocument();
    });
    await userEvent.click(screen.getByRole("listitem"));
    await waitFor((): void => {
      expect(screen.queryByText("Bulat")).toBeInTheDocument();
    });
    await userEvent.click(screen.getByText("Bulat"));
    await waitFor((): void => {
      expect(mockNavigatePush).toHaveBeenCalledTimes(1);
    });

    expect(mockNavigatePush).toHaveBeenCalledWith("/orgs/bulat/groups");

    localStorage.clear();
    jest.clearAllMocks();
  });

  it("should open user profile", async (): Promise<void> => {
    expect.hasAssertions();

    jest.clearAllMocks();

    localStorage.setItem("organizationV2", JSON.stringify({ name: "okada" }));
    const mockedPermissions = new PureAbility<string>([]);

    render(
      <authzPermissionsContext.Provider value={mockedPermissions}>
        <authContext.Provider
          value={{
            awsSubscription: null,
            tours: {
              newGroup: true,
              newRoot: true,
              welcome: true,
            },
            userEmail: "test@fluidattacks.com",
            userName: "test",
          }}
        >
          <Routes>
            <Route
              element={<Navbar initialOrganization={"okada"} />}
              path={"/orgs/:organizationName/groups/:groupName/vulns"}
            />
          </Routes>
        </authContext.Provider>
      </authzPermissionsContext.Provider>,
      {
        memoryRouter: {
          initialEntries: ["/orgs/okada/groups/unittesting/vulns"],
        },
      },
    );

    await waitFor((): void => {
      expect(screen.getByText("test")).toBeInTheDocument();
    });
    await userEvent.click(screen.getByText("test"));

    expect(screen.getByText("navbar.token")).toBeInTheDocument();
    expect(screen.getByText("navbar.notification")).toBeInTheDocument();
    expect(screen.getByText("navbar.trustedDevices")).toBeInTheDocument();
    expect(screen.getByText("navbar.mobile")).toBeInTheDocument();
    expect(screen.getByText("navbar.speakup")).toBeInTheDocument();
    expect(screen.getByText("navbar.deleteAccount.text")).toBeInTheDocument();
    expect(screen.getByText("navbar.logout")).toBeInTheDocument();

    localStorage.clear();
    jest.clearAllMocks();
  });
});
