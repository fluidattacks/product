import { IconButton } from "@fluidattacks/design";
import { styled } from "styled-components";

const DownloadButton = styled(IconButton)`
  &:hover:not([disabled]) {
    background-color: ${({ theme }): string => theme.palette.gray[300]};
  }
`;

const CloseButton = styled(IconButton)`
  &:hover:not([disabled]) {
    background-color: ${({ theme }): string => theme.palette.gray[300]};
  }
`;

export { DownloadButton, CloseButton };
