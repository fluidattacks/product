import { useLazyQuery } from "@apollo/client";
import { useMutation } from "@apollo/client";
import mixpanel from "mixpanel-browser";
import { useCallback } from "react";

import { GET_NOTIFICATION } from "./queries";

import { UPDATE_NOTIFICATIONS } from "../queries";
import { useAudit } from "hooks/use-audit";
import { Logger } from "utils/logger";
import { openUrl } from "utils/resource-helpers";

interface IUseUpdateNotifications {
  dismissNotification: () => Promise<void>;
}

const useUpdateNotifications = (
  notificationId: string,
  size: number | null,
  s3FilePath: string | null,
): IUseUpdateNotifications => {
  const [updateNotification] = useMutation(UPDATE_NOTIFICATIONS, {
    onCompleted: (): void => {
      mixpanel.track("DownloadsMenuDismiss");
    },
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach(({ message }): void => {
        Logger.error("Failed to update notifications", message);
      });
    },
  });
  const dismissNotification = useCallback(async (): Promise<void> => {
    await updateNotification({
      variables: { notificationId, s3FilePath, size },
    });
  }, [updateNotification, notificationId, s3FilePath, size]);

  return { dismissNotification };
};

interface IUseDownload {
  download: () => Promise<void>;
  downloading: boolean;
}

const useDownload = (notificationId: string): IUseDownload => {
  const [getNotification, { loading }] = useLazyQuery(GET_NOTIFICATION, {
    fetchPolicy: "network-only",
    variables: { notificationId },
  });

  const { addAuditEvent } = useAudit();
  const download = useCallback(async (): Promise<void> => {
    const { data } = await getNotification();
    const downloadURL = data?.me.notification?.downloadURL ?? null;
    if (downloadURL === null) {
      return;
    }

    mixpanel.track("DownloadsMenuDownload");
    addAuditEvent("Notification.Download", notificationId);
    openUrl(downloadURL);
  }, [addAuditEvent, getNotification, notificationId]);

  return { download, downloading: loading };
};

export { useDownload, useUpdateNotifications };
