import { useQuery } from "@apollo/client";
import mixpanel from "mixpanel-browser";
import type { RefObject } from "react";
import { useCallback, useRef, useState } from "react";

import { GET_NOTIFICATIONS } from "./queries";

import type { GetNotificationsQuery } from "gql/graphql";
import { useAudit } from "hooks/use-audit";
import { useClickOutside } from "hooks/use-click-outside";
import { useStoredState } from "hooks/use-stored-state";

const useMenu = (): {
  closeMenu: () => void;
  isOpen: boolean;
  menuRef: RefObject<HTMLDivElement>;
  toggleMenu: () => void;
} => {
  const { addAuditEvent } = useAudit();
  const [isOpen, setIsOpen] = useState(false);

  const toggleMenu = useCallback((): void => {
    setIsOpen((current): boolean => {
      if (current) {
        addAuditEvent("Notifications.Close", "unknown");
      } else {
        addAuditEvent("Notifications", "unknown");
      }
      mixpanel.track(`DownloadsMenu${current ? "Close" : "Open"}`);

      return !current;
    });
  }, [addAuditEvent]);
  const closeMenu = useCallback((): void => {
    setIsOpen(false);
  }, []);

  const menuRef = useRef<HTMLDivElement>(null);
  useClickOutside(menuRef.current, closeMenu);

  return { closeMenu, isOpen, menuRef, toggleMenu };
};

type TNotification = GetNotificationsQuery["me"]["notifications"][0];

interface IUseExecutionsQuery {
  loading: boolean;
  notifications: TNotification[];
}

const useNotificationsQuery = (): IUseExecutionsQuery => {
  const { data, loading } = useQuery(GET_NOTIFICATIONS, {
    fetchPolicy: "network-only",
    pollInterval: 5000,
  });
  const notifications = data?.me.notifications ?? [];

  return { loading, notifications };
};

interface IUsePendingDownloads {
  hasPendingDownloads: boolean;
  markAsDownloaded: (notificationId: string) => void;
}

const usePendingDownloads = (
  activeReports: TNotification[],
): IUsePendingDownloads => {
  const [downloaded, setDownloaded] = useStoredState<string[]>(
    "downloaded",
    [],
    localStorage,
  );

  const markAsDownloaded = useCallback(
    (notificationId: string): void => {
      setDownloaded((current): string[] => {
        if (current.includes(notificationId)) {
          return current;
        }

        // Use active as base to clear stale ids from storage
        const activeIds = activeReports.map(({ id }): string => id);
        const relevantIds = current.filter((id): boolean =>
          activeIds.includes(id),
        );

        return [...relevantIds, notificationId];
      });
    },
    [activeReports, setDownloaded],
  );

  const hasPendingDownloads = activeReports.some(
    (report): boolean => !downloaded.includes(report.id),
  );

  return { hasPendingDownloads, markAsDownloaded };
};

export type { TNotification };
export { useMenu, useNotificationsQuery, usePendingDownloads };
