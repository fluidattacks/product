import { graphql } from "gql";

const REMOVE_STAKEHOLDER_MUTATION = graphql(`
  mutation RemoveStakeholderMutation {
    removeStakeholder {
      success
    }
  }
`);

const GET_USER_CREDENTIALS_AND_ROOTS = graphql(`
  query GetUserCredentialsAndRoots {
    me {
      userEmail
      organizations {
        name
        credentials {
          id
          __typename
          name
          oauthType
          owner
          type
        }
        groups {
          __typename
          name
          roots {
            ... on GitRoot {
              __typename
              nickname
              url
              credentials {
                id
                __typename
                owner
              }
            }
          }
        }
      }
    }
  }
`);

export { GET_USER_CREDENTIALS_AND_ROOTS, REMOVE_STAKEHOLDER_MUTATION };
