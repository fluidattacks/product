import {
  Button,
  Modal,
  Text,
  useConfirmDialog,
  useModal,
} from "@fluidattacks/design";
import type { ColumnDef } from "@tanstack/react-table";
import dayjs, { extend } from "dayjs";
import relativeTime from "dayjs/plugin/relativeTime";
import timezone from "dayjs/plugin/timezone";
import utc from "dayjs/plugin/utc";
import { useCallback, useState } from "react";
import * as React from "react";
import { useTranslation } from "react-i18next";

import { AddTokenModal } from "./add-token-modal";
import { useGetAPIToken, useInvalidateAPIToken } from "./hooks";
import type { IAccessTokens, ITokensModalProps } from "./types";

import { Table } from "components/table";
import { useTable } from "hooks";

const AccessTokenModal: React.FC<ITokensModalProps> = ({
  open,
  onClose,
}): JSX.Element => {
  const { t } = useTranslation();
  const tableRef = useTable("tblAccessTokens");
  const modalProps = useModal("api-token-modal");
  const [isOpen, setIsOpen] = useState(false);
  const handleOpen = useCallback((): void => {
    setIsOpen((current): boolean => !current);
  }, []);

  const [data, refetch] = useGetAPIToken();
  const { confirm, ConfirmDialog } = useConfirmDialog();

  const lastAccessTokenUseFromNow = useCallback(
    (lastTokenUse: string | null): string => {
      if (lastTokenUse === null) {
        return "Never";
      }
      extend(relativeTime);
      extend(utc);
      extend(timezone);

      return dayjs(lastTokenUse).tz(dayjs.tz.guess()).fromNow();
    },
    [],
  );

  const invalidateToken = useInvalidateAPIToken(refetch);

  const revokeToken = useCallback(
    (token: IAccessTokens): (() => void) =>
      async (): Promise<void> => {
        const confirmResult = await confirm({
          message:
            token.lastUse === null ? (
              t("updateAccessToken.warningConfirm")
            ) : (
              <React.Fragment>
                <Text fontWeight={"bold"} size={"sm"}>
                  {t("updateAccessToken.warning")}
                </Text>
                <div>
                  <Text
                    display={"inline-block"}
                    fontWeight={"bold"}
                    size={"sm"}
                  >
                    {t("updateAccessToken.tokenLastUsed")}
                  </Text>
                  &nbsp;
                  {lastAccessTokenUseFromNow(token.lastUse)}
                </div>
              </React.Fragment>
            ),
          title: t("updateAccessToken.invalidate"),
        });
        if (confirmResult) {
          await invalidateToken({ variables: { id: token.id } });
        }
      },
    [confirm, invalidateToken, lastAccessTokenUseFromNow, t],
  );

  const handleRevoke = useCallback(
    (token: IAccessTokens): JSX.Element => (
      <Button icon={"xmark"} onClick={revokeToken(token)} variant={"ghost"}>
        {t("updateAccessToken.buttons.revoke")}
      </Button>
    ),
    [revokeToken, t],
  );
  const SECONDS_IN_MS = 1000;
  const tableColumns: ColumnDef<IAccessTokens>[] = [
    {
      accessorKey: "name",
      header: t("updateAccessToken.header.name"),
    },
    {
      accessorKey: "issuedAt",
      cell: (cell): string =>
        dayjs(cell.row.original.issuedAt * SECONDS_IN_MS).format("YYYY-MM-DD"),
      header: t("updateAccessToken.header.issuedAt"),
    },
    {
      accessorKey: "lastUse",
      cell: (cell): string =>
        lastAccessTokenUseFromNow(cell.row.original.lastUse),
      header: t("updateAccessToken.header.lastUse"),
    },
    {
      accessorKey: "action",
      cell: (cell): JSX.Element => handleRevoke(cell.row.original),
      enableSorting: false,
      header: "",
    },
  ];

  return (
    <React.Fragment>
      <Modal
        modalRef={{ ...modalProps, close: onClose, isOpen: open }}
        size={"sm"}
        title={t("updateAccessToken.title")}
      >
        <Table
          columns={tableColumns}
          data={data?.me.accessTokens ?? []}
          options={{ enableSearchBar: false }}
          rightSideComponents={
            (data?.me.accessTokens.length ?? 2) < 2 ? (
              <Button icon={"plus"} id={"add-api-token"} onClick={handleOpen}>
                {t("updateAccessToken.buttons.add")}
              </Button>
            ) : undefined
          }
          tableRef={tableRef}
        />
        <ConfirmDialog />
      </Modal>
      {isOpen ? (
        <AddTokenModal onClose={handleOpen} open={isOpen} refetch={refetch} />
      ) : undefined}
    </React.Fragment>
  );
};

export { AccessTokenModal };
