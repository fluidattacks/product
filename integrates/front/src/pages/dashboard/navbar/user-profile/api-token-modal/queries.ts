import { graphql } from "gql";

const GET_ACCESS_TOKEN = graphql(`
  query GetAccessTokenQuery {
    me {
      userEmail
      accessTokens {
        id
        __typename
        issuedAt
        lastUse
        name
      }
    }
  }
`);

const INVALIDATE_ACCESS_TOKEN_MUTATION = graphql(`
  mutation InvalidateAccessTokenMutation($id: ID) {
    invalidateAccessToken(id: $id) {
      success
    }
  }
`);

const ADD_ACCESS_TOKEN = graphql(`
  mutation AddAccessToken($expirationTime: Int!, $name: String!) {
    addAccessToken(expirationTime: $expirationTime, name: $name) {
      sessionJwt
      success
    }
  }
`);

export { ADD_ACCESS_TOKEN, GET_ACCESS_TOKEN, INVALIDATE_ACCESS_TOKEN_MUTATION };
