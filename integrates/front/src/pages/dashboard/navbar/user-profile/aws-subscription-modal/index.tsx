import type { TTagVariant } from "@fluidattacks/design";
import {
  Alert,
  Col,
  Container,
  IndicatorCard,
  ProgressBar,
  Row,
  Tag,
  Text,
} from "@fluidattacks/design";
import { Fragment, useContext } from "react";
import * as React from "react";
import { useTranslation } from "react-i18next";

import type { IAWSSubscriptionModalProps } from "./types";

import { Modal } from "components/modal";
import { authContext } from "context/auth";
import { useModal } from "hooks/use-modal";

const AWSSubscriptionModal: React.FC<IAWSSubscriptionModalProps> = ({
  onClose,
}): JSX.Element => {
  const { awsSubscription } = useContext(authContext);
  const { t } = useTranslation();
  const modalProps = useModal("aws-subscription-modal");

  const tagVariants: Record<string, TTagVariant> = {
    ACTIVE: "success",
    EXPIRED: "inactive",
    FAILED: "error",
    PENDING: "warning",
  };

  if (awsSubscription === null) {
    return <div />;
  }

  const showUsageAlert =
    awsSubscription.contractedGroups === null
      ? false
      : awsSubscription.usedGroups > awsSubscription.contractedGroups;

  const PERCENT_MULTIPLIER = 100;
  const usagePercentage =
    awsSubscription.contractedGroups === null
      ? 0
      : Math.round(
          (awsSubscription.usedGroups * PERCENT_MULTIPLIER) /
            awsSubscription.contractedGroups,
        );
  const tagVariant = tagVariants[awsSubscription.status];

  return (
    <Modal
      modalRef={{ ...modalProps, close: onClose, isOpen: true }}
      size={"sm"}
      title={t("navbar.awsSubscription.modal.title")}
    >
      {awsSubscription.status === "ACTIVE" ? (
        <Fragment>
          <Row cols={2}>
            <Container display={"flex"} textAlign={"center"}>
              <Col>
                <IndicatorCard
                  description={t(
                    "navbar.awsSubscription.modal.indicators.usedGroups",
                  )}
                  title={awsSubscription.usedGroups.toString()}
                />
              </Col>
              <Col>
                <IndicatorCard
                  description={t(
                    "navbar.awsSubscription.modal.indicators.contractedGroups",
                  )}
                  title={awsSubscription.contractedGroups?.toString()}
                />
              </Col>
            </Container>
          </Row>
          <ProgressBar minWidth={720} percentage={usagePercentage} />
        </Fragment>
      ) : (
        <Row align={"center"} cols={2}>
          <Col>
            <Text size={"sm"}>{t("navbar.awsSubscription.modal.status")}</Text>
          </Col>
          <Col>
            <Tag
              priority={tagVariant === "inactive" ? "default" : "low"}
              tagLabel={awsSubscription.status}
              variant={tagVariant}
            />
          </Col>
        </Row>
      )}
      <br />
      {showUsageAlert ? (
        <Alert>{t("navbar.awsSubscription.modal.usageAlert")}</Alert>
      ) : undefined}
    </Modal>
  );
};

export { AWSSubscriptionModal };
