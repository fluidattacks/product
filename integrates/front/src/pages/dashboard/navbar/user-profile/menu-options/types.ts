interface IMenuOptionsProps {
  deleteAccount: VoidFunction;
  isMenuVisible: boolean;
  logout: VoidFunction;
  meetingMode: boolean;
  openAWSSubscriptionModal: VoidFunction | undefined;
  openTokenModal: VoidFunction;
  openMobileModal: VoidFunction;
  setIsMenuVisible: React.Dispatch<React.SetStateAction<boolean>>;
  toggleMeetingMode: VoidFunction;
  toggleShowMenu: VoidFunction;
}

export type { IMenuOptionsProps };
