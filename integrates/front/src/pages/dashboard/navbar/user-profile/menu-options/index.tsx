import {
  Avatar,
  Divider,
  Link as LinkComp,
  ListItem,
  Menu,
  Toggle,
} from "@fluidattacks/design";
import { useCallback, useContext, useRef } from "react";
import { useTranslation } from "react-i18next";
import { Link } from "react-router-dom";

import type { IMenuOptionsProps } from "./types";

import { authContext } from "context/auth";
import { Can } from "context/authz/can";
import { featurePreviewContext } from "context/feature-preview";
import { Role as role } from "features/user-role";
import { CI_COMMIT_SHA, CI_COMMIT_SHORT_SHA } from "utils/ctx";

const MenuOptions = ({
  deleteAccount,
  logout,
  meetingMode,
  openAWSSubscriptionModal,
  openMobileModal,
  openTokenModal,
  isMenuVisible,
  setIsMenuVisible,
  toggleMeetingMode,
  toggleShowMenu,
}: Readonly<IMenuOptionsProps>): JSX.Element => {
  const { featurePreview, setFeaturePreview } = useContext(
    featurePreviewContext,
  );
  const { userEmail, userName, userIntPhone } = useContext(authContext);
  const { t } = useTranslation();
  const avatarRef = useRef<HTMLDivElement>(null);
  const userRole = role();

  const togglePreview = useCallback((): void => {
    setFeaturePreview?.((value): boolean => !value);
  }, [setFeaturePreview]);

  const menu = ((): JSX.Element | undefined => {
    if (!isMenuVisible) {
      return undefined;
    }
    const userInfo = {
      email: userEmail,
      phone: userIntPhone,
      userName,
      userRole,
    };

    return (
      <Menu
        commitSha={CI_COMMIT_SHA}
        commitShortSha={CI_COMMIT_SHORT_SHA}
        parentElement={avatarRef.current}
        setVisibility={setIsMenuVisible}
        userInfo={userInfo}
      >
        <Can do={"front_can_enable_meeting_mode"}>
          <li className={"no-hover"} style={{ padding: "1px 16px 2px" }}>
            {t("navbar.meeting")}
            <Toggle
              defaultChecked={meetingMode}
              name={"meetingMode"}
              onChange={toggleMeetingMode}
            />
          </li>
          <li className={"no-hover"} style={{ padding: "1px 16px 2px" }}>
            {t("navbar.featurePreview")}
            <Toggle
              defaultChecked={featurePreview}
              name={"featurePreview"}
              onChange={togglePreview}
            />
          </li>
        </Can>
        <Divider />
        {openAWSSubscriptionModal === undefined ? undefined : (
          <ListItem
            icon={"aws"}
            iconType={"fa-brands"}
            onClick={openAWSSubscriptionModal}
          >
            {t("navbar.awsSubscription.text")}
          </ListItem>
        )}
        <ListItem
          icon={"shield-check"}
          iconType={"fa-light"}
          onClick={openTokenModal}
        >
          {t("navbar.token")}
        </ListItem>
        <Link to={"/user/config"}>
          <ListItem
            icon={"bell"}
            iconType={"fa-light"}
            onClick={toggleShowMenu}
          >
            {t("navbar.notification")}
          </ListItem>
        </Link>
        <Link to={"/user/trusted-devices"}>
          <ListItem
            icon={"link"}
            iconType={"fa-light"}
            onClick={toggleShowMenu}
          >
            {t("navbar.trustedDevices")}
          </ListItem>
        </Link>
        <ListItem
          icon={"mobile"}
          iconType={"fa-light"}
          onClick={openMobileModal}
        >
          {t("navbar.mobile")}
        </ListItem>
        <LinkComp
          href={"https://r.easyllama.com/ed68576b-c349-4f40-926b-4905c82c53bb"}
          iconPosition={"hidden"}
        >
          <ListItem icon={"message-exclamation"} iconType={"fa-light"}>
            {t("navbar.speakup")}
          </ListItem>
        </LinkComp>
        <ListItem icon={"trash"} iconType={"fa-light"} onClick={deleteAccount}>
          {t("navbar.deleteAccount.text")}
        </ListItem>
        <Divider />
        <ListItem icon={"sign-out"} iconType={"fa-light"} onClick={logout}>
          {t("navbar.logout")}
        </ListItem>
      </Menu>
    );
  })();

  return (
    <Avatar
      onClick={toggleShowMenu}
      ref={avatarRef}
      showIcon={true}
      showUsername={true}
      userName={userName}
    >
      {menu}
    </Avatar>
  );
};

export { MenuOptions };
