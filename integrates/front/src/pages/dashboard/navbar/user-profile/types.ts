interface IRoot {
  nickname: string;
  url: string;
  credentials?: ICredentialRow;
}

interface ICredential {
  id: string;
  owner: string;
  name: string;
  oauthType?: string;
  type: string;
}

interface ICredentialWithOrganization extends ICredential {
  organizationName: string;
}

interface ICredentialRow extends ICredential {
  isUsed: boolean;
  groupName: string;
}

interface IUserCredAndRootsAttr {
  me: {
    __typename: "Me";
    organizations: {
      __typename: "Organization";
      name: string;
      credentials: ICredential[];
      groups?: {
        __typename: "Group";
        name: string;
        roots: IRoot[];
      }[];
    }[];
    userEmail: string;
  };
}

export type {
  ICredential,
  ICredentialRow,
  IUserCredAndRootsAttr,
  ICredentialWithOrganization,
};
