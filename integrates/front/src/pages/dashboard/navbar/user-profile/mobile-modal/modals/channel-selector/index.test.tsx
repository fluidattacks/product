import { screen } from "@testing-library/react";

import { ChannelSelector } from ".";
import { render } from "mocks";

describe("select channel modal", (): void => {
  it("should render a Modal component when isOpen is true", (): void => {
    expect.hasAssertions();

    const onClose = jest.fn();
    const isOpen = true;
    const handleChannelClick = jest.fn();

    render(
      <ChannelSelector
        handleChannelClick={handleChannelClick}
        isOpen={isOpen}
        onClose={onClose}
      />,
    );

    expect(
      screen.getByRole("button", {
        name: "components.channels.smsButton.text",
      }),
    ).toBeInTheDocument();

    expect(
      screen.getByRole("button", {
        name: "components.channels.whatsappButton.text",
      }),
    ).toBeInTheDocument();
  });

  it("should not render the component when isOpen is false", (): void => {
    expect.hasAssertions();

    const onClose = jest.fn();
    const isOpen = false;
    const handleChannelClick = jest.fn();

    render(
      <ChannelSelector
        handleChannelClick={handleChannelClick}
        isOpen={isOpen}
        onClose={onClose}
      />,
    );

    expect(screen.queryByText("verifyDialog.title")).not.toBeInTheDocument();
  });
});
