import { PureAbility } from "@casl/ability";
import { screen } from "@testing-library/react";

import { AddPhoneForm } from ".";
import { authzPermissionsContext } from "context/authz/config";
import { render } from "mocks";

describe("add phone modal", (): void => {
  it("should render a form", (): void => {
    expect.hasAssertions();

    const isOpen = true;
    const onSubmit = jest.fn();
    const onCancel = jest.fn();
    const mockedPermissions = new PureAbility<string>([
      { action: "integrates_api_mutations_update_stakeholder_phone_mutate" },
    ]);

    jest.spyOn(console, "error").mockImplementation();
    render(
      <authzPermissionsContext.Provider value={mockedPermissions}>
        <AddPhoneForm isOpen={isOpen} onCancel={onCancel} onSubmit={onSubmit} />
      </authzPermissionsContext.Provider>,
    );

    const inputPhoneElement = document.querySelector("input[name='phone']");

    expect(inputPhoneElement).toBeInTheDocument();
    expect(inputPhoneElement).toBeEnabled();
    expect(screen.getByRole("button", { name: "Cancel" })).toBeInTheDocument();
    expect(
      screen.getByRole("button", { name: "profile.mobileModal.add" }),
    ).toBeInTheDocument();
  });

  it("should not render anything when isOpen is false", (): void => {
    expect.hasAssertions();

    const isOpen = false;
    const onSubmit = jest.fn();
    const onCancel = jest.fn();
    const mockedPermissions = new PureAbility<string>([
      { action: "integrates_api_mutations_update_stakeholder_phone_mutate" },
    ]);

    render(
      <authzPermissionsContext.Provider value={mockedPermissions}>
        <AddPhoneForm isOpen={isOpen} onCancel={onCancel} onSubmit={onSubmit} />
      </authzPermissionsContext.Provider>,
    );

    expect(
      document.querySelector("input[name='phone']"),
    ).not.toBeInTheDocument();
    expect(
      screen.queryByRole("button", { name: "Cancel" }),
    ).not.toBeInTheDocument();
    expect(
      screen.queryByRole("button", { name: "profile.mobileModal.add" }),
    ).not.toBeInTheDocument();
  });
});
