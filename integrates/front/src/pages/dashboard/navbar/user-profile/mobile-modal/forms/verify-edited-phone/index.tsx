import { Form, InnerForm, Input, PhoneInput } from "@fluidattacks/design";
import isNil from "lodash/isNil";
import { type FC, Fragment } from "react";
import { useTranslation } from "react-i18next";

import type { IVerifyEditedPhoneForm } from "./types";

import { verifyCodeSchema } from "../../validations";

const VerifyEditedPhoneForm: FC<IVerifyEditedPhoneForm> = ({
  isOpen,
  onSubmit,
  onCancel,
  phone,
  phoneToEdit,
}: IVerifyEditedPhoneForm): JSX.Element | null => {
  const { t } = useTranslation();

  if (!isOpen || isNil(phone) || isNil(phoneToEdit)) return null;

  return (
    <Form
      cancelButton={{ onClick: onCancel }}
      confirmButton={{ label: t("profile.mobileModal.verify") }}
      defaultValues={{
        newPhone: phoneToEdit.callingCountryCode + phoneToEdit.nationalNumber,
        newVerificationCode: "",
        phone: phone.callingCountryCode + phone.nationalNumber,
        verificationCode: "",
      }}
      id={"verifyEditionCode"}
      onSubmit={onSubmit}
      yupSchema={verifyCodeSchema}
    >
      <InnerForm>
        {({ formState, register, watch }): JSX.Element => (
          <Fragment>
            <PhoneInput
              disabled={true}
              label={t("profile.mobileModal.fields.phoneNumber")}
              name={"phone"}
              register={register}
              watch={watch}
            />
            <PhoneInput
              disabled={true}
              label={t("profile.mobileModal.fields.newPhoneNumber")}
              name={"newPhone"}
              register={register}
              watch={watch}
            />
            <Input
              error={formState.errors.newVerificationCode?.message?.toString()}
              isTouched={"newVerificationCode" in formState.touchedFields}
              isValid={!("newVerificationCode" in formState.errors)}
              label={t("profile.mobileModal.fields.verificationCode")}
              name={"newVerificationCode"}
              register={register}
            />
          </Fragment>
        )}
      </InnerForm>
    </Form>
  );
};

export { VerifyEditedPhoneForm };
