import type React from "react";

interface IUpdateStakeholderPhone {
  isAdding: boolean;
  isEditing: boolean;
  setIsAdding: React.Dispatch<React.SetStateAction<boolean>>;
  setIsOpenEdit: React.Dispatch<React.SetStateAction<boolean>>;
  setIsEditing: React.Dispatch<React.SetStateAction<boolean>>;
  setIsCodeInCurrentMobile: React.Dispatch<React.SetStateAction<boolean>>;
  setIsCodeInNewMobile: React.Dispatch<React.SetStateAction<boolean>>;
  setPhoneToVerify: React.Dispatch<
    React.SetStateAction<IPhoneAttr | undefined>
  >;
}

interface IVerifyStakeholder {
  isAdding: boolean;
  isEditing: boolean;
  isOpenEdit: boolean;
  setIsCodeInCurrentMobile: React.Dispatch<React.SetStateAction<boolean>>;
  setIsCodeInNewMobile: React.Dispatch<React.SetStateAction<boolean>>;
}

interface IUpdateStakeholderPhoneResultAttr {
  updateStakeholderPhone: {
    success: boolean;
  };
}

interface IPhoneAttr {
  callingCountryCode: string;
  nationalNumber: string;
}

interface IAdditionFormValues {
  phone: string;
}

interface IVerifyAdditionCodeFormValues {
  phone: string;
  newVerificationCode: string;
}

interface IEditionFormValues {
  phone: string;
  newPhone: string;
  verificationCode: string;
}

interface IVerifyEditionFormValues {
  phone: string;
  newPhone: string;
  newVerificationCode: string;
  verificationCode: string;
}

interface IHandleAdditionModalFormProps {
  handleCloseModal: () => void;
}

interface IPhoneNumberFieldProps {
  disable: boolean;
}

interface IMobileModalProps {
  onClose: () => void;
}

export type {
  IAdditionFormValues,
  IHandleAdditionModalFormProps,
  IPhoneAttr,
  IPhoneNumberFieldProps,
  IUpdateStakeholderPhoneResultAttr,
  IMobileModalProps,
  IVerifyAdditionCodeFormValues,
  IEditionFormValues,
  IVerifyEditionFormValues,
  IUpdateStakeholderPhone,
  IVerifyStakeholder,
};
