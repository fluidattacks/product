import { Modal, useModal } from "@fluidattacks/design";
import isNil from "lodash/isNil";
import { useCallback, useMemo, useState } from "react";
import * as React from "react";
import { useTranslation } from "react-i18next";

import { AddPhoneForm } from "./forms/add-phone";
import { EditPhoneForm } from "./forms/edit-phone";
import { VerifyEditedPhoneForm } from "./forms/verify-edited-phone";
import { VerifyPhoneForm } from "./forms/verify-phone";
import {
  useStakeholderPhone,
  useUpdateStakeholderPhone,
  useVerifyStakeholder,
} from "./hooks";
import { ChannelSelector } from "./modals/channel-selector";
import type {
  IAdditionFormValues,
  IEditionFormValues,
  IMobileModalProps,
  IPhoneAttr,
  IVerifyAdditionCodeFormValues,
  IVerifyEditionFormValues,
} from "./types";

import type { VerificationChannel } from "gql/graphql";

const MobileModal: React.FC<IMobileModalProps> = ({ onClose }): JSX.Element => {
  const { t } = useTranslation();
  const modalProps = useModal("mobile-modal");
  const [isAdding, setIsAdding] = useState(false);
  const [isEditing, setIsEditing] = useState(false);
  const [isOpenEdit, setIsOpenEdit] = useState(false);
  const [isChannelOpen, setIsChannelOpen] = useState(false);
  const [isCodeInCurrentMobile, setIsCodeInCurrentMobile] = useState(false);
  const [isCodeInNewMobile, setIsCodeInNewMobile] = useState(false);
  const [phoneToVerify, setPhoneToVerify] = useState<IPhoneAttr | undefined>(
    undefined,
  );
  const [verificationCode, setVerificationCode] = useState("");

  const [handleUpdateStakeholderPhone] = useUpdateStakeholderPhone({
    isAdding,
    isEditing,
    setIsAdding,
    setIsCodeInCurrentMobile,
    setIsCodeInNewMobile,
    setIsEditing,
    setIsOpenEdit,
    setPhoneToVerify,
  });
  const [handleVerifyStakeholder] = useVerifyStakeholder({
    isAdding,
    isEditing,
    isOpenEdit,
    setIsCodeInCurrentMobile,
    setIsCodeInNewMobile,
  });
  const [data] = useStakeholderPhone();
  const handleAdd = useCallback((values: IAdditionFormValues): void => {
    setPhoneToVerify(values.phone as unknown as IPhoneAttr);
    setIsAdding(true);
    setIsCodeInNewMobile(false);
    setIsChannelOpen(true);
  }, []);

  const handleVerifyAdditionCode = useCallback(
    (values: IVerifyAdditionCodeFormValues): void => {
      void handleUpdateStakeholderPhone({
        variables: {
          newPhone: {
            callingCountryCode: phoneToVerify?.callingCountryCode,
            nationalNumber: phoneToVerify?.nationalNumber,
          },
          verificationCode: values.newVerificationCode,
        },
      });
    },
    [handleUpdateStakeholderPhone, phoneToVerify],
  );

  const handleOpenEdit = useCallback((): void => {
    setIsOpenEdit(true);
    setIsCodeInCurrentMobile(false);
    setIsChannelOpen(true);
  }, []);

  const handleEdit = useCallback((values: IEditionFormValues): void => {
    setPhoneToVerify(values.newPhone as unknown as IPhoneAttr);
    setIsEditing(true);
    setIsCodeInNewMobile(false);
    setVerificationCode(values.verificationCode);
    setIsChannelOpen(true);
  }, []);

  const handleChannelClick = useCallback(
    (event: React.MouseEvent<HTMLButtonElement>): void => {
      const channel = event.currentTarget.value as VerificationChannel;
      if (isNil(phoneToVerify) || typeof phoneToVerify !== "object") {
        void handleVerifyStakeholder({ variables: { channel } });
      } else {
        void handleVerifyStakeholder({
          variables: {
            channel,
            newPhone: {
              callingCountryCode: phoneToVerify.callingCountryCode,
              nationalNumber: phoneToVerify.nationalNumber,
            },
            ...(verificationCode && { verificationCode }),
          },
        });
      }
      setIsChannelOpen(false);
    },
    [
      phoneToVerify,
      handleVerifyStakeholder,
      setIsChannelOpen,
      verificationCode,
    ],
  );

  const handleVerifyEditionCode = useCallback(
    (values: IVerifyEditionFormValues): void => {
      void handleUpdateStakeholderPhone({
        variables: {
          newPhone: {
            callingCountryCode: phoneToVerify?.callingCountryCode,
            nationalNumber: phoneToVerify?.nationalNumber,
          },
          verificationCode: values.newVerificationCode,
        },
      });
    },
    [handleUpdateStakeholderPhone, phoneToVerify],
  );

  const phone = useMemo(
    (): IPhoneAttr | null => (isNil(data) ? null : data.me.phone),
    [data],
  );

  return (
    <React.Fragment>
      <Modal
        modalRef={{ ...modalProps, close: onClose, isOpen: true }}
        size={"sm"}
        title={t("profile.mobileModal.title")}
      >
        <AddPhoneForm
          isOpen={!(isAdding && isCodeInNewMobile) && isNil(phone)}
          onCancel={onClose}
          onSubmit={handleAdd}
        />
        <VerifyPhoneForm
          isOpen={isAdding ? isCodeInNewMobile : false}
          onCancel={onClose}
          onSubmit={handleVerifyAdditionCode}
          phoneToAdd={phoneToVerify}
        />
        <EditPhoneForm
          handleOpenEdit={handleOpenEdit}
          isEditingMobileCode={isOpenEdit ? isCodeInCurrentMobile : false}
          isOpen={!isCodeInNewMobile}
          onCancel={onClose}
          onSubmit={handleEdit}
          phone={phone}
        />
        <VerifyEditedPhoneForm
          isOpen={isEditing ? isCodeInNewMobile : false}
          onCancel={onClose}
          onSubmit={handleVerifyEditionCode}
          phone={phone}
          phoneToEdit={phoneToVerify}
        />
      </Modal>
      <ChannelSelector
        handleChannelClick={handleChannelClick}
        isOpen={isChannelOpen}
        onClose={onClose}
      />
    </React.Fragment>
  );
};

export { MobileModal };
