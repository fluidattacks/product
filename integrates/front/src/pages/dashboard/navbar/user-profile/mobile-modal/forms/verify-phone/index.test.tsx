import { screen } from "@testing-library/react";

import { VerifyPhoneForm } from ".";
import type { IPhoneAttr } from "../../types";
import { render } from "mocks";

describe("verify phone modal", (): void => {
  it("should render a form", (): void => {
    expect.hasAssertions();

    const isOpen = true;

    const phoneToAdd: IPhoneAttr = {
      callingCountryCode: "",
      nationalNumber: "",
    };
    const onSubmit = jest.fn();
    const onCancel = jest.fn();

    jest.spyOn(console, "error").mockImplementation();
    render(
      <VerifyPhoneForm
        isOpen={isOpen}
        onCancel={onCancel}
        onSubmit={onSubmit}
        phoneToAdd={phoneToAdd}
      />,
    );

    const inputVerificationCode = document.querySelector(
      "input[name='newVerificationCode']",
    );

    expect(
      screen.getByText("profile.mobileModal.fields.phoneNumber"),
    ).toBeInTheDocument();
    expect(inputVerificationCode).toBeInTheDocument();
    expect(inputVerificationCode).toBeEnabled();
  });

  it("should not render the form when isOpen is false", (): void => {
    expect.hasAssertions();

    const phoneToAdd: IPhoneAttr = {
      callingCountryCode: "",
      nationalNumber: "",
    };

    const onSubmit = jest.fn();
    const onCancel = jest.fn();
    const isOpen = false;

    render(
      <VerifyPhoneForm
        isOpen={isOpen}
        onCancel={onCancel}
        onSubmit={onSubmit}
        phoneToAdd={phoneToAdd}
      />,
    );

    expect(
      screen.queryByLabelText("profile.mobileModal.fields.phoneNumber"),
    ).not.toBeInTheDocument();
    expect(
      document.querySelector("input[name='newVerificationCode']"),
    ).not.toBeInTheDocument();
  });
});
