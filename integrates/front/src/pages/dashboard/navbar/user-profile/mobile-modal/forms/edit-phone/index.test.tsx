import { PureAbility } from "@casl/ability";
import { fireEvent, screen, waitFor } from "@testing-library/react";

import { EditPhoneForm } from ".";
import type { IPhoneAttr } from "../../types";
import { authzPermissionsContext } from "context/authz/config";
import { render } from "mocks";

describe("edit phone modal", (): void => {
  it("should render a form", (): void => {
    expect.hasAssertions();

    const isOpen = true;
    const phone: IPhoneAttr = {
      callingCountryCode: "+1",
      nationalNumber: "123456789",
    };
    const onSubmit = jest.fn();
    const onCancel = jest.fn();
    const handleOpenEdit = jest.fn();
    const mockedPermissions = new PureAbility<string>([
      { action: "integrates_api_mutations_update_stakeholder_phone_mutate" },
    ]);

    jest.spyOn(console, "error").mockImplementation();
    render(
      <authzPermissionsContext.Provider value={mockedPermissions}>
        <EditPhoneForm
          handleOpenEdit={handleOpenEdit}
          isEditingMobileCode={true}
          isOpen={isOpen}
          onCancel={onCancel}
          onSubmit={onSubmit}
          phone={phone}
        />
      </authzPermissionsContext.Provider>,
    );

    const inputPhoneElement = document.querySelector("input[name='phone']");

    expect(inputPhoneElement).toBeInTheDocument();
    expect(inputPhoneElement).toHaveValue("+1 (123) 456-789");
    expect(inputPhoneElement).toBeDisabled();
    expect(
      document.querySelector("input[name='verificationCode']"),
    ).toBeInTheDocument();

    const inputNewPhoneElement = document.querySelector(
      "input[name='newPhone']",
    );

    expect(inputNewPhoneElement).toBeInTheDocument();
    expect(inputNewPhoneElement).toHaveValue("+1 ");
    expect(inputNewPhoneElement).toBeEnabled();
  });

  it("should display an error message when the phone input field is not valid", async (): Promise<void> => {
    expect.hasAssertions();

    const phone: IPhoneAttr = {
      callingCountryCode: "+57",
      nationalNumber: "",
    };

    const onSubmit = jest.fn();
    const onCancel = jest.fn();
    const handleOpenEdit = jest.fn();

    const isEditingMobileCode = true;
    const isOpen = true;
    const mockedPermissions = new PureAbility<string>([
      { action: "integrates_api_mutations_update_stakeholder_phone_mutate" },
    ]);

    render(
      <authzPermissionsContext.Provider value={mockedPermissions}>
        <EditPhoneForm
          handleOpenEdit={handleOpenEdit}
          isEditingMobileCode={isEditingMobileCode}
          isOpen={isOpen}
          onCancel={onCancel}
          onSubmit={onSubmit}
          phone={phone}
        />
      </authzPermissionsContext.Provider>,
    );

    const phoneInput = document.querySelector("input[name='newPhone']");
    if (phoneInput) {
      fireEvent.change(phoneInput, { target: { value: "" } });
      fireEvent.blur(phoneInput);
    }
    await waitFor((): void => {
      expect(
        screen.queryByText("The phone number is invalid"),
      ).toBeInTheDocument();
    });
  });
});
