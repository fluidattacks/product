import type { IPhoneAttr, IVerifyEditionFormValues } from "../../types";
import type { IPhoneForm } from "../types";

interface IVerifyEditedPhoneForm extends IPhoneForm<IVerifyEditionFormValues> {
  phone: IPhoneAttr | null;
  phoneToEdit: IPhoneAttr | undefined;
}

export type { IVerifyEditedPhoneForm };
