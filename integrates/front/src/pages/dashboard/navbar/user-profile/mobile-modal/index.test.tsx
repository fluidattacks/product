import { PureAbility } from "@casl/ability";
import { screen, waitFor } from "@testing-library/react";
import { userEvent } from "@testing-library/user-event";
import dayjs from "dayjs";
import { GraphQLError } from "graphql";
import _ from "lodash";
import { HttpResponse, type StrictResponse, graphql } from "msw";

import {
  GET_STAKEHOLDER_PHONE,
  GET_USER,
  UPDATE_STAKEHOLDER_PHONE_MUTATION,
  VERIFY_STAKEHOLDER_MUTATION,
} from "./queries";

import { MobileModal } from ".";
import { authzPermissionsContext } from "context/authz/config";
import type {
  GetStakeholderPhoneQueryQuery as GetStakeholderPhone,
  GetUserQuery as GetUser,
  UpdateStakeholderPhoneMutationMutation as UpdateStakeholderPhone,
  VerificationChannel,
  VerifyStakeholderMutationAtMobileModalMutation as VerifyStakeholder,
} from "gql/graphql";
import { render } from "mocks";
import { LINK } from "mocks/constants";
import type { IErrorMessage } from "mocks/types";
import { msgSuccess } from "utils/notifications";

jest.mock("utils/notifications", (): Record<string, unknown> => {
  const mockedNotifications: Record<string, () => Record<string, unknown>> =
    jest.requireActual("utils/notifications");
  jest.spyOn(mockedNotifications, "msgSuccess").mockImplementation();

  return mockedNotifications;
});

describe("mobile modal", (): void => {
  const graphqlMocked = graphql.link(LINK);
  const btnCancel = "Cancel";
  const mocks = [
    graphqlMocked.query(GET_USER, (): StrictResponse<{ data: GetUser }> => {
      return HttpResponse.json({
        data: {
          me: {
            __typename: "Me",
            isConcurrentSession: false,
            remember: false,
            role: "",
            sessionExpiration: dayjs().add(1, "day").toISOString(),
            userEmail: "test@fluidattacks.com",
            userName: "",
          },
        },
      });
    }),
  ];

  it("should close the mobile modal", async (): Promise<void> => {
    expect.hasAssertions();

    const handleOnClose: jest.Mock = jest.fn();
    const mockQuery = [
      graphqlMocked.query(
        GET_STAKEHOLDER_PHONE,
        (): StrictResponse<{ data: GetStakeholderPhone }> => {
          return HttpResponse.json({
            data: {
              me: {
                __typename: "Me",
                phone: {
                  callingCountryCode: "1",
                  countryCode: "US",
                  nationalNumber: "1234545",
                },
                userEmail: "test@fluidattacks.com",
              },
            },
          });
        },
      ),
    ];

    render(<MobileModal onClose={handleOnClose} />, {
      mocks: [...mocks, ...mockQuery],
    });
    await waitFor((): void => {
      expect(screen.getByText("profile.mobileModal.title")).toBeInTheDocument();
    });

    expect(
      screen.queryByRole("button", { name: btnCancel }),
    ).toBeInTheDocument();

    await userEvent.click(screen.getByRole("button", { name: btnCancel }));

    expect(handleOnClose).toHaveBeenCalledTimes(1);
  });

  it("should display the stakeholder's mobile without edit permission", async (): Promise<void> => {
    expect.hasAssertions();

    const handleOnClose: jest.Mock = jest.fn();
    const mockedPermissions = new PureAbility<string>([
      { action: "integrates_api_mutations_update_stakeholder_phone_mutate" },
    ]);
    const mockQuery = [
      graphqlMocked.query(
        GET_STAKEHOLDER_PHONE,
        (): StrictResponse<{ data: GetStakeholderPhone }> => {
          return HttpResponse.json({
            data: {
              me: {
                __typename: "Me",
                phone: {
                  callingCountryCode: "1",
                  countryCode: "US",
                  nationalNumber: "1234545",
                },
                userEmail: "test@fluidattacks.com",
              },
            },
          });
        },
      ),
    ];

    render(
      <authzPermissionsContext.Provider value={mockedPermissions}>
        <MobileModal onClose={handleOnClose} />
      </authzPermissionsContext.Provider>,
      { mocks: [...mocks, ...mockQuery] },
    );
    await waitFor((): void => {
      expect(screen.getByDisplayValue("+1 (123) 454-5")).toBeInTheDocument();
    });

    expect(handleOnClose).toHaveBeenCalledTimes(0);
  });

  it("should add a new mobile", async (): Promise<void> => {
    expect.hasAssertions();

    const handleOnClose: jest.Mock = jest.fn();

    const mockedPermissions = new PureAbility<string>([
      { action: "integrates_api_mutations_update_stakeholder_phone_mutate" },
    ]);
    const mockQuery = [
      graphqlMocked.query(
        GET_STAKEHOLDER_PHONE,
        (): StrictResponse<{ data: GetStakeholderPhone }> => {
          return HttpResponse.json({
            data: {
              me: {
                __typename: "Me",
                phone: null,
                userEmail: "test@fluidattacks.com",
              },
            },
          });
        },
        { once: true },
      ),
      graphqlMocked.query(
        GET_STAKEHOLDER_PHONE,
        (): StrictResponse<{ data: GetStakeholderPhone }> => {
          return HttpResponse.json({
            data: {
              me: {
                __typename: "Me",
                phone: {
                  callingCountryCode: "57",
                  countryCode: "CO",
                  nationalNumber: "123456789",
                },
                userEmail: "test@fluidattacks.com",
              },
            },
          });
        },
        { once: true },
      ),
      graphqlMocked.mutation(
        VERIFY_STAKEHOLDER_MUTATION,
        ({
          variables,
        }): StrictResponse<IErrorMessage | { data: VerifyStakeholder }> => {
          const { newPhone, channel } = variables;
          if (
            _.get(newPhone, "callingCountryCode") === "57" &&
            _.get(newPhone, "nationalNumber") === "123456789" &&
            channel === ("SMS" as VerificationChannel)
          ) {
            return HttpResponse.json({
              data: {
                verifyStakeholder: { success: true },
              },
            });
          }

          return HttpResponse.json({
            errors: [new GraphQLError("Error verifying stakeholder")],
          });
        },
        { once: true },
      ),
      graphqlMocked.mutation(
        UPDATE_STAKEHOLDER_PHONE_MUTATION,
        ({
          variables,
        }): StrictResponse<
          IErrorMessage | { data: UpdateStakeholderPhone }
        > => {
          const { newPhone, verificationCode } = variables;
          if (
            _.get(newPhone, "callingCountryCode") === "57" &&
            _.get(newPhone, "nationalNumber") === "123456789" &&
            verificationCode === "1234"
          ) {
            return HttpResponse.json({
              data: {
                updateStakeholderPhone: { success: true },
              },
            });
          }

          return HttpResponse.json({
            errors: [new GraphQLError("Error updating stakeholder")],
          });
        },
        { once: true },
      ),
    ];

    render(
      <authzPermissionsContext.Provider value={mockedPermissions}>
        <MobileModal onClose={handleOnClose} />
      </authzPermissionsContext.Provider>,
      { mocks: [...mocks, ...mockQuery] },
    );

    await userEvent.type(screen.getByRole("textbox"), "123456789");
    await userEvent.click(
      screen.getByRole("button", { name: "profile.mobileModal.add" }),
    );
    await userEvent.click(
      screen.getByRole("button", {
        name: "components.channels.smsButton.text",
      }),
    );
    await waitFor((): void => {
      expect(
        screen.getByRole("button", { name: "profile.mobileModal.verify" }),
      ).toBeInTheDocument();
    });

    expect(msgSuccess).toHaveBeenCalledTimes(1);
    expect(msgSuccess).toHaveBeenLastCalledWith(
      "profile.mobileModal.alerts.sendNewMobileVerificationSuccess",
      "groupAlerts.titleSuccess",
    );

    await userEvent.type(
      screen.getByRole("textbox", { name: "newVerificationCode" }),
      "1234",
    );
    await userEvent.click(
      screen.getByRole("button", { name: "profile.mobileModal.verify" }),
    );
    await waitFor((): void => {
      expect(msgSuccess).toHaveBeenCalledTimes(2);
    });

    expect(msgSuccess).toHaveBeenLastCalledWith(
      "profile.mobileModal.alerts.additionSuccess",
      "groupAlerts.titleSuccess",
    );

    expect(screen.getByRole("button", { name: btnCancel })).toBeInTheDocument();
    expect(handleOnClose).toHaveBeenCalledTimes(0);

    jest.clearAllMocks();
  });

  it("should edit mobile", async (): Promise<void> => {
    expect.hasAssertions();

    const handleOnClose: jest.Mock = jest.fn();

    const mockedPermissions = new PureAbility<string>([
      { action: "integrates_api_mutations_update_stakeholder_phone_mutate" },
    ]);
    const mockQuery = [
      graphqlMocked.query(
        GET_STAKEHOLDER_PHONE,
        (): StrictResponse<{ data: GetStakeholderPhone }> => {
          return HttpResponse.json({
            data: {
              me: {
                __typename: "Me",
                phone: {
                  callingCountryCode: "57",
                  countryCode: "CO",
                  nationalNumber: "123456789",
                },
                userEmail: "test@fluidattacks.com",
              },
            },
          });
        },
        { once: true },
      ),
      graphqlMocked.query(
        GET_STAKEHOLDER_PHONE,
        (): StrictResponse<{ data: GetStakeholderPhone }> => {
          return HttpResponse.json({
            data: {
              me: {
                __typename: "Me",
                phone: {
                  callingCountryCode: "57",
                  countryCode: "CO",
                  nationalNumber: "987654321",
                },
                userEmail: "test@fluidattacks.com",
              },
            },
          });
        },
        { once: true },
      ),
      graphqlMocked.mutation(
        VERIFY_STAKEHOLDER_MUTATION,
        ({
          variables,
        }): StrictResponse<IErrorMessage | { data: VerifyStakeholder }> => {
          const { channel } = variables;
          if (channel === ("SMS" as VerificationChannel)) {
            return HttpResponse.json({
              data: {
                verifyStakeholder: { success: true },
              },
            });
          }

          return HttpResponse.json({
            errors: [new GraphQLError("Error verifying stakeholder")],
          });
        },
        { once: true },
      ),
      graphqlMocked.mutation(
        VERIFY_STAKEHOLDER_MUTATION,
        ({
          variables,
        }): StrictResponse<IErrorMessage | { data: VerifyStakeholder }> => {
          const { channel, newPhone, verificationCode } = variables;
          if (
            _.get(newPhone, "callingCountryCode") === "57" &&
            _.get(newPhone, "nationalNumber") === "987654321" &&
            channel === ("SMS" as VerificationChannel) &&
            verificationCode === "1234"
          ) {
            return HttpResponse.json({
              data: {
                verifyStakeholder: { success: true },
              },
            });
          }

          return HttpResponse.json({
            errors: [new GraphQLError("Error verifying stakeholder")],
          });
        },
        { once: true },
      ),
      graphqlMocked.mutation(
        UPDATE_STAKEHOLDER_PHONE_MUTATION,
        ({
          variables,
        }): StrictResponse<
          IErrorMessage | { data: UpdateStakeholderPhone }
        > => {
          const { newPhone, verificationCode } = variables;
          if (
            _.get(newPhone, "callingCountryCode") === "57" &&
            _.get(newPhone, "nationalNumber") === "987654321" &&
            verificationCode === "1234"
          ) {
            return HttpResponse.json({
              data: {
                updateStakeholderPhone: { success: true },
              },
            });
          }

          return HttpResponse.json({
            errors: [new GraphQLError("Error updating stakeholder")],
          });
        },
        { once: true },
      ),
    ];

    render(
      <authzPermissionsContext.Provider value={mockedPermissions}>
        <MobileModal onClose={handleOnClose} />
      </authzPermissionsContext.Provider>,
      { mocks: [...mocks, ...mockQuery] },
    );
    await waitFor((): void => {
      expect(screen.getByDisplayValue("+57 123 456 789")).toBeInTheDocument();
    });

    await userEvent.click(
      screen.getByRole("button", { name: "profile.mobileModal.edit" }),
    );
    await userEvent.click(
      screen.getByRole("button", {
        name: "components.channels.smsButton.text",
      }),
    );
    await waitFor((): void => {
      expect(msgSuccess).toHaveBeenLastCalledWith(
        "profile.mobileModal.alerts.sendCurrentMobileVerificationSuccess",
        "groupAlerts.titleSuccess",
      );
    });

    expect(msgSuccess).toHaveBeenCalledTimes(1);

    await userEvent.type(
      screen.getByRole("textbox", { name: "verificationCode" }),
      "1234",
    );
    await userEvent.type(screen.getByDisplayValue("+57"), "987654321");
    await userEvent.click(
      screen.getByRole("button", { name: "profile.mobileModal.edit" }),
    );
    await userEvent.click(
      screen.getByRole("button", {
        name: "components.channels.smsButton.text",
      }),
    );
    await waitFor((): void => {
      expect(msgSuccess).toHaveBeenCalledTimes(2);
    });

    expect(msgSuccess).toHaveBeenLastCalledWith(
      "profile.mobileModal.alerts.sendNewMobileVerificationSuccess",
      "groupAlerts.titleSuccess",
    );

    await userEvent.type(
      screen.getByRole("textbox", { name: "newVerificationCode" }),
      "1234",
    );
    await userEvent.click(
      screen.getByRole("button", { name: "profile.mobileModal.verify" }),
    );
    await waitFor((): void => {
      expect(msgSuccess).toHaveBeenLastCalledWith(
        "profile.mobileModal.alerts.editionSuccess",
        "groupAlerts.titleSuccess",
      );
    });

    expect(screen.getByRole("button", { name: btnCancel })).toBeInTheDocument();
    expect(handleOnClose).toHaveBeenCalledTimes(0);

    jest.clearAllMocks();
  });
});
