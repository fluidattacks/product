import type { ApolloError, MutationFunction } from "@apollo/client";
import { useMutation, useQuery } from "@apollo/client";
import { useTranslation } from "react-i18next";

import {
  GET_STAKEHOLDER_PHONE,
  GET_USER,
  UPDATE_STAKEHOLDER_PHONE_MUTATION,
  VERIFY_STAKEHOLDER_MUTATION,
} from "./queries";
import type {
  IUpdateStakeholderPhone,
  IUpdateStakeholderPhoneResultAttr,
  IVerifyStakeholder,
} from "./types";

import type { GetStakeholderPhoneQueryQuery as IGetStakeholderPhone } from "gql/graphql";
import { Logger } from "utils/logger";
import { msgError, msgSuccess } from "utils/notifications";

const useUpdateStakeholderPhone = ({
  isAdding,
  isEditing,
  setIsAdding,
  setIsEditing,
  setIsCodeInCurrentMobile,
  setIsCodeInNewMobile,
  setIsOpenEdit,
  setPhoneToVerify,
}: IUpdateStakeholderPhone): readonly [MutationFunction] => {
  const { t } = useTranslation();
  const handleError = ({ graphQLErrors }: ApolloError): void => {
    graphQLErrors.forEach((error): void => {
      switch (error.message) {
        case "Exception - A mobile number is required with the international format":
          msgError(t("profile.mobileModal.alerts.requiredMobile"));
          break;
        case "Exception - Stakeholder could not be verified":
          msgError(t("profile.mobileModal.alerts.nonVerifiedStakeholder"));
          break;
        case "Exception - The verification code is invalid":
          msgError(t("profile.mobileModal.alerts.invalidVerificationCode"));
          break;
        default:
          msgError(t("groupAlerts.errorTextsad"));
          Logger.warning("An error occurred updating stakeholder phone", error);
      }
    });
  };

  const [handleUpdateStakeholderPhone] =
    useMutation<IUpdateStakeholderPhoneResultAttr>(
      UPDATE_STAKEHOLDER_PHONE_MUTATION,
      {
        onCompleted: (data): void => {
          if (data.updateStakeholderPhone.success && isAdding) {
            msgSuccess(
              t("profile.mobileModal.alerts.additionSuccess"),
              t("groupAlerts.titleSuccess"),
            );
            setIsAdding(false);
          }
          if (data.updateStakeholderPhone.success && isEditing) {
            msgSuccess(
              t("profile.mobileModal.alerts.editionSuccess"),
              t("groupAlerts.titleSuccess"),
            );
            setIsOpenEdit(false);
            setIsEditing(false);
            setIsCodeInCurrentMobile(false);
          }
          setIsCodeInNewMobile(false);
          setPhoneToVerify(undefined);
        },
        onError: handleError,
        refetchQueries: [{ query: GET_STAKEHOLDER_PHONE }, { query: GET_USER }],
      },
    );

  return [handleUpdateStakeholderPhone] as const;
};

const useVerifyStakeholder = ({
  isAdding,
  isEditing,
  isOpenEdit,
  setIsCodeInNewMobile,
  setIsCodeInCurrentMobile,
}: IVerifyStakeholder): readonly [MutationFunction] => {
  const { t } = useTranslation();
  const handleError = ({ graphQLErrors }: ApolloError): void => {
    graphQLErrors.forEach((error): void => {
      switch (error.message) {
        case "Exception - A mobile number is required with the international format":
        case "Exception - A new phone number is required":
          msgError(t("profile.mobileModal.alerts.requiredMobile"));
          break;
        case "Exception - Stakeholder could not be verified":
          msgError(t("profile.mobileModal.alerts.nonVerifiedStakeholder"));
          break;
        case "Exception - The verification code is invalid":
          msgError(t("profile.mobileModal.alerts.invalidVerificationCode"));
          break;
        case "Exception - The verification code is required":
          msgError(t("profile.mobileModal.alerts.requiredVerificationCode"));
          break;
        case "Exception - Stakeholder verification could not be started":
          msgError(t("profile.mobileModal.alerts.nonSentVerificationCode"));
          break;
        case "Exception - The new phone number is the current phone number":
          msgError(t("profile.mobileModal.alerts.sameMobile"));
          break;
        default:
          msgError(t("groupAlerts.errorTextsad"));
          Logger.warning("An error occurred updating stakeholder phone", error);
      }
    });
  };

  const [handleVerifyStakeholder] = useMutation(VERIFY_STAKEHOLDER_MUTATION, {
    onCompleted: (data): void => {
      if (data.verifyStakeholder.success && isAdding) {
        msgSuccess(
          t("profile.mobileModal.alerts.sendNewMobileVerificationSuccess"),
          t("groupAlerts.titleSuccess"),
        );
        setIsCodeInNewMobile(true);
      }
      if (data.verifyStakeholder.success && isOpenEdit && !isEditing) {
        msgSuccess(
          t("profile.mobileModal.alerts.sendCurrentMobileVerificationSuccess"),
          t("groupAlerts.titleSuccess"),
        );
        setIsCodeInCurrentMobile(true);
      }
      if (data.verifyStakeholder.success && isEditing) {
        msgSuccess(
          t("profile.mobileModal.alerts.sendNewMobileVerificationSuccess"),
          t("groupAlerts.titleSuccess"),
        );
        setIsCodeInNewMobile(true);
      }
    },
    onError: handleError,
  });

  return [handleVerifyStakeholder] as const;
};

const useStakeholderPhone = (): readonly [IGetStakeholderPhone | undefined] => {
  const { data } = useQuery(GET_STAKEHOLDER_PHONE, {
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error): void => {
        Logger.error("Couldn't load stakeholder's phone", error);
      });
    },
  });

  return [data] as const;
};

export { useUpdateStakeholderPhone, useVerifyStakeholder, useStakeholderPhone };
