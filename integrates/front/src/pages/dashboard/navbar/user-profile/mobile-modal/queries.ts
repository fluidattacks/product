import { graphql } from "gql";

const GET_STAKEHOLDER_PHONE = graphql(`
  query GetStakeholderPhoneQuery {
    me {
      __typename
      userEmail
      phone {
        callingCountryCode
        countryCode
        nationalNumber
      }
    }
  }
`);

const UPDATE_STAKEHOLDER_PHONE_MUTATION = graphql(`
  mutation UpdateStakeholderPhoneMutation(
    $newPhone: PhoneInput!
    $verificationCode: String!
  ) {
    updateStakeholderPhone(
      phone: $newPhone
      verificationCode: $verificationCode
    ) {
      success
    }
  }
`);

const VERIFY_STAKEHOLDER_MUTATION = graphql(`
  mutation VerifyStakeholderMutationAtMobileModal(
    $channel: VerificationChannel
    $newPhone: PhoneInput
    $verificationCode: String
  ) {
    verifyStakeholder(
      channel: $channel
      newPhone: $newPhone
      verificationCode: $verificationCode
    ) {
      success
    }
  }
`);

const GET_USER = graphql(`
  query GetUser {
    me {
      isConcurrentSession
      remember
      role
      sessionExpiration
      userEmail
      userName
    }
  }
`);

export {
  GET_STAKEHOLDER_PHONE,
  GET_USER,
  UPDATE_STAKEHOLDER_PHONE_MUTATION,
  VERIFY_STAKEHOLDER_MUTATION,
};
