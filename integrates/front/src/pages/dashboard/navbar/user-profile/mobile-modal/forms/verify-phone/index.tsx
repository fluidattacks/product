import { Form, InnerForm, Input, PhoneInput } from "@fluidattacks/design";
import isNil from "lodash/isNil";
import { type FC, Fragment } from "react";
import { useTranslation } from "react-i18next";

import type { IVerifyPhoneForm } from "./types";

import { verifyCodeSchema } from "../../validations";

const VerifyPhoneForm: FC<IVerifyPhoneForm> = ({
  isOpen,
  onCancel,
  onSubmit,
  phoneToAdd,
}: IVerifyPhoneForm): JSX.Element | null => {
  const { t } = useTranslation();

  if (!isOpen || isNil(phoneToAdd)) return null;

  return (
    <Form
      cancelButton={{ onClick: onCancel }}
      confirmButton={{ label: t("profile.mobileModal.verify") }}
      defaultValues={{
        newVerificationCode: "",
        phone: phoneToAdd.callingCountryCode + phoneToAdd.nationalNumber,
      }}
      id={"verifyAdditionCode"}
      onSubmit={onSubmit}
      yupSchema={verifyCodeSchema}
    >
      <InnerForm>
        {({ formState, register, watch }): JSX.Element => (
          <Fragment>
            <PhoneInput
              disabled={true}
              label={t("profile.mobileModal.fields.phoneNumber")}
              name={"phone"}
              register={register}
              watch={watch}
            />
            <Input
              error={formState.errors.newVerificationCode?.message?.toString()}
              isTouched={"newVerificationCode" in formState.touchedFields}
              isValid={!("newVerificationCode" in formState.errors)}
              label={t("profile.mobileModal.fields.verificationCode")}
              name={"newVerificationCode"}
              register={register}
            />
          </Fragment>
        )}
      </InnerForm>
    </Form>
  );
};

export { VerifyPhoneForm };
