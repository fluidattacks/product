interface IChannelSelectorProps {
  onClose: () => void;
  isOpen: boolean;
  handleChannelClick: (event: React.MouseEvent<HTMLButtonElement>) => void;
}

export type { IChannelSelectorProps };
