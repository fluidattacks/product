import { useQuery } from "@apollo/client";
import { Container } from "@fluidattacks/design";
import _ from "lodash";
import mixpanel from "mixpanel-browser";
import { useCallback, useContext, useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import { useMatch, useNavigate } from "react-router-dom";
import { useTheme } from "styled-components";

import { Downloads } from "./downloads";
import { News } from "./news";
import { OrganizationDropdown } from "./organization-dropdown";
import { GET_GROUP_INFO, GET_USER_ORGANIZATIONS } from "./queries";
import { ToDo } from "./to-do";
import { UserProfile } from "./user-profile";

import { InlineLogo } from "components/inline-logo";
import { Search } from "components/search";
import { authContext } from "context/auth";
import { useStoredState } from "hooks";
import { useAudit } from "hooks/use-audit";
import { Logger } from "utils/logger";
import { msgError } from "utils/notifications";

const Navbar = ({
  initialOrganization,
}: Readonly<{
  initialOrganization: string;
}>): JSX.Element => {
  const theme = useTheme();
  const { t } = useTranslation();
  const navigate = useNavigate();
  const orgMatch = useMatch("/orgs/:organizationName/*");
  const { userEmail } = useContext(authContext);
  const [searchedGroup, setSearchedGroup] = useState("");
  const [currentOrganization, setCurrentOrganization] =
    useState(initialOrganization);
  const [, setLastOrganization] = useStoredState(
    "organizationV2",
    { deleted: false, name: "" },
    localStorage,
  );
  const [, setLastGroup] = useStoredState("group", { name: "" }, localStorage);

  const handleOrgChange = useCallback(
    (orgName: string): void => {
      if (orgName !== currentOrganization) {
        setLastOrganization({ deleted: false, name: orgName });
        setCurrentOrganization(orgName);
        navigate(`/orgs/${orgName.toLowerCase()}/groups`);
      }
    },
    [currentOrganization, navigate, setLastOrganization],
  );

  const { data } = useQuery(GET_USER_ORGANIZATIONS, {
    fetchPolicy: "network-only",
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error): void => {
        msgError(t("groupAlerts.errorTextsad"));
        Logger.warning(
          "An error occurred fetching organizations for the top bar",
          error,
        );
      });
    },
  });

  const { addAuditEvent } = useAudit();
  useQuery(GET_GROUP_INFO, {
    onCompleted: ({ group }): void => {
      const { name, organization } = group;
      addAuditEvent("SearchGroup", name);
      setLastGroup({ name });
      setLastOrganization({ deleted: false, name: organization });
      setCurrentOrganization(organization);
      setSearchedGroup("");
      navigate(`orgs/${organization}/groups/${name}/vulns`);
    },
    skip: searchedGroup === "",
    variables: { groupName: searchedGroup },
  });

  const handleSubmit = useCallback(
    (value: string): void => {
      const groupName = value.toLowerCase().trim();
      if (groupName !== "") {
        mixpanel.track("SearchGroup", { group: groupName });
        setSearchedGroup(groupName);
      }
    },
    [setSearchedGroup],
  );

  useEffect((): void => {
    const { organizationName } = orgMatch?.params ?? {};
    if (!_.isUndefined(organizationName)) {
      setCurrentOrganization(organizationName);
    }
  }, [orgMatch]);

  const organizationList = _.sortBy(data?.me.organizations ?? [], ["name"]);

  return (
    <Container
      alignItems={"center"}
      as={"header"}
      bgColor={theme.palette.white}
      borderBottom={`1px solid ${theme.palette.gray[300]}`}
      display={"flex"}
      justify={"space-between"}
      px={0.75}
      py={0.75}
    >
      <Container alignItems={"center"} display={"flex"}>
        <InlineLogo />
        <OrganizationDropdown
          handleOrganizationChange={handleOrgChange}
          initialOrg={currentOrganization}
          organizationList={organizationList}
        />
      </Container>
      <Container alignItems={"center"} display={"flex"}>
        {userEmail.endsWith("@fluidattacks.com") ? (
          <Container mr={0.5}>
            <Search handleOnSubmit={handleSubmit} placeHolder={"Search"} />
          </Container>
        ) : undefined}
        <Container mr={0.5} position={"relative"}>
          <ToDo />
        </Container>
        <Container mr={0.5} position={"relative"}>
          <Downloads />
        </Container>
        <Container mr={0.5} position={"relative"}>
          <News />
        </Container>
        <UserProfile />
      </Container>
    </Container>
  );
};

export { Navbar };
