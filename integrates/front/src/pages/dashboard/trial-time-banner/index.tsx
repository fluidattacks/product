import { MessageBanner } from "@fluidattacks/design";
import { useCallback } from "react";
import * as React from "react";
import { useTranslation } from "react-i18next";

import { useTrial } from "hooks/use-trial";
import { openUrl } from "utils/resource-helpers";

const TrialTimeBanner: React.FC = (): JSX.Element | null => {
  const { t } = useTranslation();
  const trialData = useTrial();
  const handleButtonClick = useCallback((): void => {
    openUrl(
      "https://res.cloudinary.com/fluid-attacks/image/upload/fl_attachment:Fluid-Attacks-Plans/v1704916448/integrates/plans/fluid-attacks-plans.pdf",
    );
  }, []);

  if (trialData === null) {
    return null;
  }

  const { trial, remainingDays } = trialData;

  if (trial.completed || remainingDays < 0) {
    return null;
  }

  return (
    <MessageBanner
      buttonText={t("dashboard.freeTrial.button")}
      message={t("dashboard.freeTrial.message", { remainingDays })}
      onClickButton={handleButtonClick}
    />
  );
};

export { TrialTimeBanner };
