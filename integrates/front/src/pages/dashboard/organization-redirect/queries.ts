import { graphql } from "gql";

const GET_GROUP_ORGANIZATION = graphql(`
  query GetGroupOrganization($groupName: String!) {
    group(groupName: $groupName) {
      name
      organization
    }
  }
`);

const GET_PORTFOLIO_ORGANIZATION = graphql(`
  query GetPortfolioOrganization($tagName: String!) {
    tag(tag: $tagName) {
      name
      organization
    }
  }
`);

export { GET_GROUP_ORGANIZATION, GET_PORTFOLIO_ORGANIZATION };
