import { screen, waitFor } from "@testing-library/react";
import { userEvent } from "@testing-library/user-event";
import { HttpResponse, type StrictResponse, graphql } from "msw";
import { Fragment } from "react";
import { useLocation } from "react-router-dom";

import {
  ACCEPT_LEGAL_MUTATION,
  GET_USER_REMEMBER,
} from "./legal-notice/queries";

import { Dashboard } from ".";
import type {
  AcceptLegalMutationMutation as AcceptLegal,
  GetUserOrganizationsQuery,
  GetUserRememberQuery as GetUserRemember,
} from "gql/graphql";
import { GET_USER_ORGANIZATIONS } from "hooks/queries";
import { render } from "mocks";
import { LINK } from "mocks/constants";

const mockNavigatePush: jest.Mock = jest.fn();
jest.mock(
  "react-router-dom",
  (): Record<string, unknown> => ({
    ...jest.requireActual("react-router-dom"),
    useNavigate: (): jest.Mock => mockNavigatePush,
  }),
);

jest.spyOn(Object.getPrototypeOf(localStorage), "setItem");

const LocationTracker = ({ path }: Readonly<{ path: string }>): null => {
  const location = useLocation();
  if (location.pathname === path) {
    localStorage.setItem("redirect", location.pathname);
  }

  return null;
};

describe("dashboard", (): void => {
  const graphqlMocked = graphql.link(LINK);

  it("should apply start url", async (): Promise<void> => {
    expect.hasAssertions();

    localStorage.setItem("start_url", "/orgs/test/nonexistent-tab");

    render(<Dashboard />);

    await expect(screen.findByRole("main")).resolves.toBeInTheDocument();

    expect(mockNavigatePush).toHaveBeenCalledWith("/orgs/test/nonexistent-tab");
    expect(localStorage.getItem("start_url")).toBeNull();
  });

  it("should determine initial organization from user", async (): Promise<void> => {
    expect.hasAssertions();

    render(
      <Fragment>
        <LocationTracker path={"/orgs/okada"} />
        <Dashboard />
      </Fragment>,
    );

    await expect(screen.findByRole("main")).resolves.toBeInTheDocument();

    await waitFor((): void => {
      expect(localStorage.getItem("redirect")).toBe("/orgs/okada");
    });
  });

  it("should determine initial organization from storage", async (): Promise<void> => {
    expect.hasAssertions();

    localStorage.setItem(
      "organizationV2",
      JSON.stringify({ deleted: false, name: "test" }),
    );

    render(
      <Fragment>
        <LocationTracker path={"/orgs/test"} />
        <Dashboard />
      </Fragment>,
    );

    await expect(screen.findByRole("main")).resolves.toBeInTheDocument();

    await waitFor((): void => {
      expect(localStorage.getItem("redirect")).toBe("/orgs/test");
    });

    expect(localStorage.getItem("organizationV2")).toBe(
      JSON.stringify({ deleted: false, name: "test" }),
    );
  });

  it("should determine initial organization from url", async (): Promise<void> => {
    expect.hasAssertions();

    render(<Dashboard />, {
      memoryRouter: { initialEntries: ["/orgs/bulat"] },
    });

    await expect(screen.findByRole("main")).resolves.toBeInTheDocument();
    expect(localStorage.getItem("organizationV2")).toBe(
      JSON.stringify({ deleted: false, name: "bulat" }),
    );
  });

  it("should determine the initial organization from storage and change it if it is not in the user organization names", async (): Promise<void> => {
    expect.hasAssertions();

    localStorage.setItem(
      "organizationV2",
      JSON.stringify({ deleted: false, name: "stored" }),
    );

    render(
      <Fragment>
        <LocationTracker path={"/orgs/stored"} />
        <Dashboard />
      </Fragment>,
      {
        mocks: [
          graphql
            .link(LINK)
            .query(
              GET_USER_ORGANIZATIONS,
              (): StrictResponse<{ data: GetUserOrganizationsQuery }> => {
                return HttpResponse.json({
                  data: {
                    me: {
                      __typename: "Me",
                      organizations: [
                        { __typename: "Organization", name: "okada" },
                      ],
                      userEmail: "test2@test.test",
                    },
                  },
                });
              },
            ),
        ],
      },
    );

    await expect(screen.findByRole("main")).resolves.toBeInTheDocument();

    await waitFor((): void => {
      expect(localStorage.getItem("organizationV2")).toBe(
        JSON.stringify({ deleted: false, name: "okada" }),
      );
    });
  });

  it("should open legal notice", async (): Promise<void> => {
    expect.hasAssertions();

    jest
      .spyOn(document, "referrer", "get")
      .mockReturnValue("https://accounts.google.com/");

    render(<Dashboard />, {
      mocks: [
        graphqlMocked.query(
          GET_USER_REMEMBER,
          (): StrictResponse<{ data: GetUserRemember }> => {
            return HttpResponse.json({
              data: {
                me: {
                  __typename: "Me",
                  remember: false,
                  userEmail: "test@test.rest",
                },
              },
            });
          },
        ),
        graphqlMocked.mutation(
          ACCEPT_LEGAL_MUTATION,
          (): StrictResponse<{ data: AcceptLegal }> => {
            return HttpResponse.json({
              data: {
                acceptLegal: { success: true },
              },
            });
          },
        ),
      ],
    });

    expect(screen.queryByRole("main")).not.toBeInTheDocument();
    await expect(
      screen.findByText("legalNotice.title"),
    ).resolves.toBeInTheDocument();

    const acceptButton = await screen.findByText("legalNotice.accept");
    await userEvent.click(acceptButton);

    await expect(screen.findByRole("main")).resolves.toBeInTheDocument();
    expect(screen.queryByRole("legalNotice.title")).not.toBeInTheDocument();
  });
});
