import uniq from "lodash/uniq";

import type { IEvidenceItem } from "pages/finding/evidence/types";

const hasEvidencesDrafts = (
  evidenceImages: Record<string, IEvidenceItem>,
): boolean => {
  const keys = uniq([
    "animation",
    "exploitation",
    ...Object.keys(evidenceImages),
  ]);

  return (
    keys.filter((name): boolean => {
      const evidence = evidenceImages[name];

      return evidence.isDraft;
    }).length > 0
  );
};

export { hasEvidencesDrafts };
