import { graphql } from "gql";

graphql(`
  fragment findingFields on Finding {
    id
    groupName
    rejectedVulnerabilities
    submittedVulnerabilities
    title
    evidence {
      animation {
        date
        description
        isDraft
        url
      }
      evidence1 {
        date
        description
        isDraft
        url
      }
      evidence2 {
        date
        description
        isDraft
        url
      }
      evidence3 {
        date
        description
        isDraft
        url
      }
      evidence4 {
        date
        description
        isDraft
        url
      }
      evidence5 {
        date
        description
        isDraft
        url
      }
      exploitation {
        date
        description
        isDraft
        url
      }
    }

    vulnerabilitiesSummary {
      closed
      open
      openCritical
      openHigh
      openLow
      openMedium
    }
  }
`);

const GET_FINDING_EVIDENCE_DRAFTS = graphql(`
  query GetFindingEvidenceDrafts($after: String, $first: Int) {
    me {
      __typename
      userEmail
      findingEvidenceDrafts(after: $after, first: $first) {
        total
        edges {
          node {
            ...findingFields
          }
        }
        pageInfo {
          endCursor
          hasNextPage
        }
      }
    }
  }
`);

const GET_FINDING_EMPTY_EVIDENCE = graphql(`
  query GetFindingEmptyEvidence($after: String, $first: Int) {
    me {
      __typename
      userEmail
      findingEmptyEvidence(after: $after, first: $first) {
        total
        edges {
          node {
            ...findingFields
          }
        }
        pageInfo {
          endCursor
          hasNextPage
        }
      }
    }
  }
`);

export { GET_FINDING_EVIDENCE_DRAFTS, GET_FINDING_EMPTY_EVIDENCE };
