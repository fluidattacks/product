import { useQuery } from "@apollo/client";
import { Button, Container } from "@fluidattacks/design";
import type { ColumnDef } from "@tanstack/react-table";
import dayjs, { extend } from "dayjs";
import timezone from "dayjs/plugin/timezone";
import _ from "lodash";
import { useCallback, useEffect, useState } from "react";
import * as React from "react";
import { useTranslation } from "react-i18next";

import {
  GET_FINDING_EMPTY_EVIDENCE,
  GET_FINDING_EVIDENCE_DRAFTS,
} from "./queries";
import type {
  IEvidenceDrafts,
  IFindingAttr,
  IFindingEmptyEvidence,
  IFindingResponse,
  IMeFindingVariable,
} from "./types";
import { hasEvidencesDrafts } from "./utils";

import { GET_USER_ORGANIZATIONS_GROUPS } from "../queries";
import type { IGroups, IOrganizationGroups } from "@types";
import { Filters } from "components/filter";
import type { IFilter, ISelectedOptions } from "components/filter/types";
import { Table } from "components/table";
import { formatLinkHandler } from "components/table/table-formatters";
import { getOrganizationGroups } from "features/vulnerabilities/utils";
import { setFiltersUtil, useTable } from "hooks";
import { useAudit } from "hooks/use-audit";
import {
  formatEvidenceImages,
  formatEvidenceList,
} from "pages/finding/evidence/utils";
import { Logger } from "utils/logger";
import { translate } from "utils/translations/translate";

const columns: ColumnDef<IFindingAttr>[] = [
  {
    accessorKey: "groupName",
    cell: (cell): JSX.Element => {
      const { groupName } = cell.row.original;
      const link = `/groups/${groupName}`;
      const text = cell.getValue<string>();

      return formatLinkHandler(link, text);
    },
    header: "Group name",
  },
  {
    accessorKey: "title",
    cell: (cell): JSX.Element => {
      const { groupName, id, organizationName } = cell.row.original;
      const orgPath =
        organizationName === null ? "" : `/orgs/${organizationName}`;
      const link = `${orgPath}/groups/${groupName}/vulns/${id}/evidence`;
      const text = cell.getValue<string>();

      return formatLinkHandler(link, text);
    },
    header: "Type",
  },
  {
    accessorKey: "hasEvidenceDraft",
    cell: (cell): string => {
      const { hasEvidenceDraft } = cell.row.original;

      return translate.t(
        `todoList.tabs.evidenceDrafts.table.hasLocationDrafts.${
          hasEvidenceDraft ? "yes" : "no"
        }`,
      );
    },
    header: translate.t(
      "todoList.tabs.evidenceDrafts.table.headers.hasEvidenceDraft",
    ),
  },
  {
    accessorKey: "hasLocationDrafts",
    cell: (cell): string => {
      const { hasLocationDrafts } = cell.row.original;

      return translate.t(
        `todoList.tabs.evidenceDrafts.table.hasLocationDrafts.${
          hasLocationDrafts ? "yes" : "no"
        }`,
      );
    },
    header: translate.t(
      "todoList.tabs.evidenceDrafts.table.headers.hasLocationDrafts",
    ),
  },
  {
    accessorKey: "hasApprovedEvidence",
    cell: (cell): string => {
      const { hasApprovedEvidence } = cell.row.original;

      return translate.t(
        `todoList.tabs.evidenceDrafts.table.hasLocationDrafts.${
          hasApprovedEvidence ? "yes" : "no"
        }`,
      );
    },
    header: translate.t(
      "todoList.tabs.evidenceDrafts.table.headers.hasApprovedEvidence",
    ),
  },
  {
    accessorKey: "hasLocationApproved",
    cell: (cell): string => {
      const { hasLocationApproved } = cell.row.original;

      return translate.t(
        `todoList.tabs.evidenceDrafts.table.hasLocationDrafts.${
          hasLocationApproved ? "yes" : "no"
        }`,
      );
    },
    header: translate.t(
      "todoList.tabs.evidenceDrafts.table.headers.hasLocationApproved",
    ),
  },
];

const EvidenceDrafts: React.FC = (): JSX.Element => {
  const { t } = useTranslation();
  const tableRef = useTable("findingEvidenceDraftsTable");

  const [filters, setFilters] = useState<IFilter<IFindingAttr>[]>([
    {
      id: "groupName",
      key: "groupName",
      label: t("organization.tabs.groups.newGroup.name.text"),
      selectOptions: (findings): ISelectedOptions[] =>
        findings.map(
          (finding): ISelectedOptions => ({ value: finding.groupName }),
        ),
      type: "select",
    },
    {
      id: "findingTitle",
      key: "title",
      label: t("group.findings.type"),
      selectOptions: (findings): ISelectedOptions[] =>
        findings.map(
          (finding): ISelectedOptions => ({ value: finding.title ?? "" }),
        ),
      type: "select",
    },
    {
      id: "hasEvidenceDraft",
      key: (finding, value): boolean => {
        if (value === "" || value === undefined) return true;

        return (
          value ===
          t(
            `todoList.tabs.evidenceDrafts.table.hasLocationDrafts.${
              finding.hasEvidenceDraft ? "yes" : "no"
            }`,
          )
        );
      },
      label: translate.t(
        "todoList.tabs.evidenceDrafts.table.headers.hasEvidenceDraft",
      ),
      selectOptions: [
        t("todoList.tabs.evidenceDrafts.table.hasLocationDrafts.yes"),
        t("todoList.tabs.evidenceDrafts.table.hasLocationDrafts.no"),
      ],
      type: "select",
    },
    {
      id: "hasLocationDrafts",
      key: (finding, value): boolean => {
        if (value === "" || value === undefined) return true;

        return (
          value ===
          t(
            `todoList.tabs.evidenceDrafts.table.hasLocationDrafts.${
              finding.hasLocationDrafts ? "yes" : "no"
            }`,
          )
        );
      },
      label: translate.t(
        "todoList.tabs.evidenceDrafts.table.headers.hasLocationDrafts",
      ),
      selectOptions: [
        t("todoList.tabs.evidenceDrafts.table.hasLocationDrafts.yes"),
        t("todoList.tabs.evidenceDrafts.table.hasLocationDrafts.no"),
      ],
      type: "select",
    },
    {
      id: "hasApprovedEvidence",
      key: (finding, value): boolean => {
        if (value === "" || value === undefined) return true;

        return (
          value ===
          t(
            `todoList.tabs.evidenceDrafts.table.hasLocationDrafts.${
              finding.hasApprovedEvidence ? "yes" : "no"
            }`,
          )
        );
      },
      label: translate.t(
        "todoList.tabs.evidenceDrafts.table.headers.hasApprovedEvidence",
      ),
      selectOptions: [
        t("todoList.tabs.evidenceDrafts.table.hasLocationDrafts.yes"),
        t("todoList.tabs.evidenceDrafts.table.hasLocationDrafts.no"),
      ],
      type: "select",
    },
    {
      id: "hasLocationApproved",
      key: (finding, value): boolean => {
        if (value === "" || value === undefined) return true;

        return (
          value ===
          t(
            `todoList.tabs.evidenceDrafts.table.hasLocationDrafts.${
              finding.hasLocationApproved ? "yes" : "no"
            }`,
          )
        );
      },
      label: translate.t(
        "todoList.tabs.evidenceDrafts.table.headers.hasLocationApproved",
      ),
      selectOptions: [
        t("todoList.tabs.evidenceDrafts.table.hasLocationDrafts.yes"),
        t("todoList.tabs.evidenceDrafts.table.hasLocationDrafts.no"),
      ],
      type: "select",
    },
  ]);

  const { addAuditEvent } = useAudit();
  const { data, fetchMore, refetch } = useQuery<
    IEvidenceDrafts,
    IMeFindingVariable
  >(GET_FINDING_EVIDENCE_DRAFTS, {
    fetchPolicy: "cache-first",
    onCompleted: (): void => {
      addAuditEvent("ToDo.EvidenceDrafts", "unknown");
    },
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error): void => {
        Logger.error(
          "An error occurred loading finding evidence drafts",
          error,
        );
      });
    },
    pollInterval: 60000,
    variables: { first: 100 },
  });

  const {
    data: dataEmpty,
    fetchMore: fetchMoreEmpty,
    refetch: refetchEmpty,
  } = useQuery<IFindingEmptyEvidence, IMeFindingVariable>(
    GET_FINDING_EMPTY_EVIDENCE,
    {
      fetchPolicy: "cache-first",
      onError: ({ graphQLErrors }): void => {
        graphQLErrors.forEach((error): void => {
          Logger.error(
            "An error occurred loading finding empty evidence",
            error,
          );
        });
      },
      pollInterval: 60000,
      variables: { first: 100 },
    },
  );

  const { data: userData } = useQuery(GET_USER_ORGANIZATIONS_GROUPS, {
    fetchPolicy: "cache-first",
    onError: ({ graphQLErrors }): void => {
      graphQLErrors.forEach((error): void => {
        Logger.warning("An error occurred fetching user groups", error);
      });
    },
  });

  const refreshDrafts = useCallback((): void => {
    refetch().catch((): void => {
      Logger.error("An error occurred in refetch");
    });
    refetchEmpty().catch((): void => {
      Logger.error("An error occurred loading finding empty evidence");
    });
  }, [refetch, refetchEmpty]);

  const formatEdges = useCallback(
    (edge: { node: IFindingResponse }): IFindingAttr => {
      const evidenceImages = formatEvidenceImages(edge.node.evidence);
      const evidenceList = formatEvidenceList(evidenceImages, false, true);
      const organizationGroup = getOrganizationGroups(
        (userData?.me.organizations ?? []).map(
          (org): IOrganizationGroups => ({
            __typename: "Organization",
            groups: org.groups.map(
              (group): IGroups => ({
                __typename: "Group",
                name: group.name,
                permissions: group.permissions,
                serviceAttributes: group.serviceAttributes,
              }),
            ),
            name: org.name,
          }),
        ),
        edge.node.groupName,
      );

      return {
        ...edge.node,
        hasApprovedEvidence: !_.isEmpty(evidenceList),
        hasEvidenceDraft: hasEvidencesDrafts(evidenceImages),
        hasLocationApproved:
          edge.node.vulnerabilitiesSummary.closed +
            edge.node.vulnerabilitiesSummary.open >
          0,
        hasLocationDrafts:
          edge.node.rejectedVulnerabilities +
            edge.node.submittedVulnerabilities >
          0,
        organizationName:
          organizationGroup === undefined ? null : organizationGroup.name,
      };
    },
    [userData],
  );
  const findingEvidenceDraftsPageInfo =
    data === undefined ? undefined : data.me.findingEvidenceDrafts.pageInfo;
  const findingEmptyEvidencePageInfo =
    dataEmpty === undefined
      ? undefined
      : dataEmpty.me.findingEmptyEvidence.pageInfo;

  useEffect((): void => {
    if (!_.isUndefined(findingEvidenceDraftsPageInfo)) {
      if (findingEvidenceDraftsPageInfo.hasNextPage) {
        void fetchMore({
          variables: {
            after: findingEvidenceDraftsPageInfo.endCursor,
            first: 1200,
          },
        });
      }
    }
  }, [findingEvidenceDraftsPageInfo, fetchMore]);

  useEffect((): void => {
    if (!_.isUndefined(findingEmptyEvidencePageInfo)) {
      if (findingEmptyEvidencePageInfo.hasNextPage) {
        void fetchMoreEmpty({
          variables: {
            after: findingEmptyEvidencePageInfo.endCursor,
            first: 1200,
          },
        });
      }
    }
  }, [findingEmptyEvidencePageInfo, fetchMoreEmpty]);

  const { edges } =
    data === undefined
      ? {
          edges: [],
        }
      : data.me.findingEvidenceDrafts;
  const { edges: edgesEmpty } =
    dataEmpty === undefined
      ? {
          edges: [],
        }
      : dataEmpty.me.findingEmptyEvidence;
  const findings = [...edges.map(formatEdges), ...edgesEmpty.map(formatEdges)];
  const emptyTotal =
    !_.isUndefined(dataEmpty) &&
    !_.isUndefined(dataEmpty.me.findingEmptyEvidence) &&
    !_.isUndefined(dataEmpty.me.findingEmptyEvidence.total)
      ? dataEmpty.me.findingEmptyEvidence.total
      : 0;
  const draftTotal =
    !_.isUndefined(data) &&
    !_.isUndefined(data.me.findingEvidenceDrafts) &&
    !_.isUndefined(data.me.findingEvidenceDrafts.total)
      ? data.me.findingEvidenceDrafts.total
      : 0;
  const size = draftTotal + emptyTotal;
  const filteredData = setFiltersUtil(findings, filters);
  extend(timezone);

  return (
    <Table
      columns={columns}
      csvConfig={{
        columns: [
          "groupName",
          "title",
          "id",
          "hasEvidenceDraft",
          "hasLocationDrafts",
          "hasApprovedEvidence",
          "hasLocationApproved",
        ],
        export: true,
        headers: {
          groupName: "Group name",
          hasApprovedEvidence: t(
            "todoList.tabs.evidenceDrafts.table.headers.hasApprovedEvidence",
          ),
          hasEvidenceDraft: t(
            "todoList.tabs.evidenceDrafts.table.headers.hasEvidenceDraft",
          ),
          hasLocationApproved: t(
            "todoList.tabs.evidenceDrafts.table.headers.hasLocationApproved",
          ),
          hasLocationDrafts: t(
            "todoList.tabs.evidenceDrafts.table.headers.hasLocationDrafts",
          ),
          id: "Id",
          title: "Type",
        },
        name: `evidence-drafts-${dayjs()
          .tz(dayjs.tz.guess())
          .format("YYYY-MM-DD")}`,
      }}
      data={filteredData}
      extraButtons={
        <Container display={"flex"} gap={0.5}>
          <Button icon={"refresh"} onClick={refreshDrafts} variant={"ghost"}>
            {t("todoList.assignedVulnerabilities.refresh")}
          </Button>
        </Container>
      }
      filters={
        <Filters dataset={findings} filters={filters} setFilters={setFilters} />
      }
      options={{
        size:
          filters.filter(
            (filter): boolean =>
              filter.value === "" || filter.value === undefined,
          ).length > 0
            ? undefined
            : size,
      }}
      tableRef={tableRef}
    />
  );
};

export { EvidenceDrafts };
