import type { VulnerabilitiesSummary } from "gql/graphql";
import type { IGetFindingEvidences } from "pages/finding/evidence/types";

interface IFindingResponse {
  __typename: "Finding";
  evidence: IGetFindingEvidences["finding"]["evidence"];
  groupName: string;
  id: string;
  rejectedVulnerabilities: number;
  submittedVulnerabilities: number;
  title?: string;
  vulnerabilitiesSummary: VulnerabilitiesSummary;
}
interface IFindingAttr extends IFindingResponse {
  hasApprovedEvidence: boolean;
  hasEvidenceDraft: boolean;
  hasLocationApproved: boolean;
  hasLocationDrafts: boolean;
  organizationName: string | null;
}

interface IEvidenceDrafts {
  me: {
    __typename: "Me";
    findingEvidenceDrafts: {
      __typename: "FindingsConnection";
      edges: { node: IFindingResponse }[];
      pageInfo: {
        __typename: "PageInfo";
        endCursor: string;
        hasNextPage: boolean;
      };
      total: number | undefined;
    };
    userEmail: string;
  };
}

interface IFindingEmptyEvidence {
  me: {
    __typename: "Me";
    findingEmptyEvidence: IEvidenceDrafts["me"]["findingEvidenceDrafts"];
    userEmail: string;
  };
}
interface IMeFindingVariable {
  after?: string;
  first: number;
}

export type {
  IFindingAttr,
  IFindingEmptyEvidence,
  IEvidenceDrafts,
  IFindingResponse,
  IMeFindingVariable,
};
