import { graphql } from "gql";

const GET_USER_ORGANIZATIONS_GROUPS = graphql(`
  query GetUserOrganizationsGroupsAtDashboard {
    me {
      __typename
      userEmail
      organizations {
        name
        groups {
          name
          permissions
          serviceAttributes
        }
      }
    }
  }
`);

export { GET_USER_ORGANIZATIONS_GROUPS };
