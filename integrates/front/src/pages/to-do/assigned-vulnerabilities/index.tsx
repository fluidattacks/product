/* eslint-disable max-lines */
import { Container, Toggle } from "@fluidattacks/design";
import type { ColumnDef, Row } from "@tanstack/react-table";
import _ from "lodash";
import mixpanel from "mixpanel-browser";
import { useCallback, useContext, useEffect, useMemo, useState } from "react";
import * as React from "react";
import { useTranslation } from "react-i18next";

import { ActionButtons } from "./action-buttons";
import { useVulnsAssignedQuery } from "./hooks";
import {
  filteredContinuousVulnerabilitiesOnReattackIds,
  getSeverity,
  getUserGroups,
  isRejectedOrSafeOrSubmitted,
  isValidVulnerability,
  isVerificationRequestedOrOnHold,
} from "./utils";

import type { IOrganizationGroups } from "@types";
import { formatLinkHandler } from "components/table/table-formatters";
import { allGroupContext } from "context/authz/all-group-permissions-provider";
import {
  authzGroupContext,
  authzPermissionsContext,
} from "context/authz/config";
import { AssignedVulnerabilitiesFilter } from "features/assigned-vulnerabilities-filter";
import { DeleteVulnerabilitiesButton } from "features/to-do/action-buttons/delete-vulnerabilities-button";
import { EditButton } from "features/to-do/action-buttons/edit-button";
import { VulnComponent } from "features/vulnerabilities";
import { RemoveVulnerabilityModal } from "features/vulnerabilities/remove-vulnerability-modal";
import { TreatmentModal } from "features/vulnerabilities/treatment-modal";
import type { IVulnRowAttr } from "features/vulnerabilities/types";
import { UpdateVerificationModal } from "features/vulnerabilities/update-verification-modal";
import {
  filterOutVulnerabilities,
  getNonSelectableVulnerabilitiesOnDeleteIds,
  onRemoveVulnerabilityResultHelper,
} from "features/vulnerabilities/utils";
import type { RemoveVulnerabilityMutationMutation } from "gql/graphql";
import { useDebouncedCallback, useTabTracking } from "hooks";
import { useAudit } from "hooks/use-audit";
import { useVulnerabilityStore } from "hooks/use-vulnerability-store";
import type { IModalConfig } from "pages/finding/vulnerabilities/types";
import { isSafe } from "pages/finding/vulnerabilities/utils";
import { severityFormatter, tagsFormatter } from "utils/format-helpers";
import { Logger } from "utils/logger";
import { msgError, msgSuccess } from "utils/notifications";

const AssignedVulnerabilities: React.FC = (): JSX.Element => {
  const { t } = useTranslation();
  const { isReattacking, resetVulnsStore, setIsReattacking } =
    useVulnerabilityStore();

  const [isEditing, setIsEditing] = useState(false);
  const [iscurrentOpen, setIscurrentOpen] = useState<boolean[]>([]);
  const [isOpen, setIsOpen] = useState(false);
  const [isDeleting, setIsDeleting] = useState(false);
  const [isDeleteOpen, setIsDeleteOpen] = useState(false);
  const attributesContext = useContext(authzGroupContext);
  const permissionsContext = useContext(authzPermissionsContext);
  const [filteredData, setFilteredData] = useState<IVulnRowAttr[]>([]);

  useTabTracking("Todos");

  const { onGroupChange, userData } = useContext(allGroupContext);
  const groups = useMemo(
    (): IOrganizationGroups["groups"] => getUserGroups(userData),
    [userData],
  );

  const { refetchVulnerabilitiesAssigned, vulnerabilities } =
    useVulnsAssignedQuery(userData);

  const vulnerabilitiesGroupName = useMemo(
    (): string[] =>
      Array.from(
        new Set(
          vulnerabilities.map((vulnerability): string =>
            vulnerability.groupName.toLowerCase(),
          ),
        ),
      ),
    [vulnerabilities],
  );

  useEffect((): void => {
    if (!_.isUndefined(onGroupChange)) {
      onGroupChange(vulnerabilitiesGroupName);
    }
  }, [
    attributesContext,
    onGroupChange,
    permissionsContext,
    userData,
    vulnerabilitiesGroupName,
  ]);

  const [modalConfig, setModalConfig] = useState<IModalConfig>({
    clearSelected: (): void => undefined,
    selectedVulnerabilities: [],
  });

  const openRemediationModal = useCallback(
    (vulns: IVulnRowAttr[], clearSelected: () => void): void => {
      setModalConfig({
        clearSelected,
        selectedVulnerabilities: vulns,
      });
    },
    [],
  );

  const refreshAssigned = useCallback((): void => {
    refetchVulnerabilitiesAssigned().catch((): void => {
      Logger.error("An error occurred assigning vulnerabilities");
    });
  }, [refetchVulnerabilitiesAssigned]);

  const closeDeleteModal = useCallback((): void => {
    setIsDeleting(false);
    setIsDeleteOpen(false);
  }, []);
  const [cvssVersion, setCvssVersion] = useState({
    accessorKey: "severityThreatScore",
    header: "Severity (v4.0)",
  });

  const { addAuditEvent } = useAudit();
  const onChangeCvssVersion = useCallback((): void => {
    mixpanel.track("TouchCvssToggle");
    addAuditEvent("CVSSToggle", "unknown");
    setCvssVersion(
      (
        value,
      ): {
        accessorKey: string;
        header: string;
      } => {
        if (value.accessorKey === "severityThreatScore") {
          return {
            accessorKey: "severityTemporalScore",
            header: t("Severity (v3.1)"),
          };
        }

        return {
          accessorKey: "severityThreatScore",
          header: t("Severity (v4.0)"),
        };
      },
    );
  }, [addAuditEvent, t]);

  const columns: ColumnDef<IVulnRowAttr>[] = [
    {
      accessorKey: "organizationName",
      cell: (cell): JSX.Element => {
        const orgName = cell.row.original.organizationName ?? "";
        const link = `/orgs/${orgName}/groups/${cell.row.original.groupName}`;
        const text = cell.getValue<string>();

        return formatLinkHandler(link, text);
      },
      header: "Organization",
      meta: { filterType: "select" },
    },
    {
      accessorKey: "groupName",
      cell: (cell): JSX.Element => {
        const orgName = cell.row.original.organizationName ?? "";
        const link = `/orgs/${orgName}/groups/${cell.row.original.groupName}`;
        const text = cell.getValue<string>();

        return formatLinkHandler(link, text);
      },
      header: t("organization.tabs.groups.newGroup.name.text"),
      meta: { filterType: "select" },
    },
    {
      accessorFn: (row): string | undefined => row.finding?.title,
      cell: (cell): JSX.Element => {
        const orgName = cell.row.original.organizationName ?? "";
        const findingId =
          cell.row.original.finding === undefined
            ? ""
            : cell.row.original.finding.id;
        const link =
          `/orgs/${orgName}/groups/${cell.row.original.groupName}` +
          `/vulns/${findingId}/locations`;
        const text = cell.getValue<string>();

        return formatLinkHandler(link, text);
      },
      header: t("searchFindings.tabVuln.vulnTable.vulnerabilityType.title"),
      id: "finding-title",
      meta: { filterType: "select" },
    },
    {
      accessorKey: "where",
      enableColumnFilter: false,
      header: t("searchFindings.tabVuln.vulnTable.vulnerability"),
    },
    {
      accessorFn: (): string => "View",
      cell: (cell): JSX.Element => {
        const orgName = cell.row.original.organizationName ?? "";
        const findingId =
          cell.row.original.finding === undefined
            ? ""
            : cell.row.original.finding.id;
        const link =
          `/orgs/${orgName}/groups/${cell.row.original.groupName}` +
          `/vulns/${findingId}/evidence`;
        const text = cell.getValue<string>();

        return formatLinkHandler(link, text);
      },
      enableColumnFilter: false,
      header: "Evidence",
    },
    {
      accessorFn: (row): string => row.verification ?? "-",
      header: t("searchFindings.tabVuln.vulnTable.verification"),
      id: "verification",
      meta: { filterType: "select" },
    },
    {
      accessorFn: (row): number => getSeverity(cvssVersion, row),
      cell: (cell): JSX.Element => severityFormatter(Number(cell.getValue())),
      header: cvssVersion.header,
      id: "severity",
      meta: { filterType: "select" },
    },
    {
      accessorKey: "tag",
      cell: (cell): JSX.Element => tagsFormatter(cell.getValue()),
      header: t("searchFindings.tabVuln.vulnTable.tags"),
    },
  ];

  const toggleEdit = useCallback((): void => {
    setIscurrentOpen(
      Object.entries(
        _.groupBy(
          modalConfig.selectedVulnerabilities,
          (vuln): string => vuln.groupName,
        ),
      ).map((__, index): boolean => index === 0),
    );
    setIsEditing(!isEditing);
  }, [isEditing, modalConfig.selectedVulnerabilities]);

  const handleCloseUpdateModal = useCallback(
    (index: number): void => {
      setIscurrentOpen((current): boolean[] => {
        const newCurrent = current.map(
          (isCurrentOpen, currentIndex): boolean => {
            if (currentIndex === index || currentIndex === index + 1) {
              return !isCurrentOpen;
            }

            return isCurrentOpen;
          },
        );
        if (newCurrent.every((isCurrentOpen): boolean => !isCurrentOpen)) {
          setIsEditing(false);
          modalConfig.clearSelected();
        }

        return newCurrent;
      });
    },
    [modalConfig],
  );

  const toggleModal = useCallback((): void => {
    setIsOpen(true);
  }, []);
  const closeRemediationModal = useCallback((): void => {
    setIsOpen(false);
  }, []);
  const onReattack = useCallback((): void => {
    if (isReattacking) {
      setIsReattacking(!isReattacking);
    } else {
      const { selectedVulnerabilities } = modalConfig;
      const validVulnerabilitiesId =
        filteredContinuousVulnerabilitiesOnReattackIds(
          selectedVulnerabilities,
          groups,
        );
      const newValidVulnerabilities = Array.from(
        new Set(
          selectedVulnerabilities.filter((selectedVulnerability): boolean =>
            validVulnerabilitiesId.includes(selectedVulnerability.id),
          ),
        ),
      );
      if (selectedVulnerabilities.length > newValidVulnerabilities.length) {
        setIsReattacking(!isReattacking);
        if (newValidVulnerabilities.length === 0) {
          msgError(t("searchFindings.tabVuln.errors.selectedVulnerabilities"));
        } else {
          setModalConfig(
            (currentConfig): IModalConfig => ({
              clearSelected: currentConfig.clearSelected,
              selectedVulnerabilities: newValidVulnerabilities,
            }),
          );
          setIsOpen(true);
        }
      } else if (selectedVulnerabilities.length > 0) {
        setIsOpen(true);
        setIsReattacking(!isReattacking);
      } else {
        setIsReattacking(!isReattacking);
      }
    }
  }, [groups, isReattacking, modalConfig, setIsReattacking, t]);

  const onDelete = useCallback((): void => {
    if (isDeleting) {
      setIsDeleting(!isDeleting);
    } else {
      const { selectedVulnerabilities } = modalConfig;
      const newVulnerabilities = filterOutVulnerabilities(
        selectedVulnerabilities,
        vulnerabilities,
        getNonSelectableVulnerabilitiesOnDeleteIds,
      );
      if (selectedVulnerabilities.length > newVulnerabilities.length) {
        setIsDeleting(!isDeleting);
        if (newVulnerabilities.length === 0) {
          msgError(t("searchFindings.tabVuln.errors.selectedVulnerabilities"));
        } else {
          setModalConfig(
            (currentModalConfig): IModalConfig => ({
              clearSelected: currentModalConfig.clearSelected,
              selectedVulnerabilities: newVulnerabilities,
            }),
          );
          setIsDeleteOpen(true);
        }
      } else if (selectedVulnerabilities.length > 0) {
        setIsDeleteOpen(true);
        setIsDeleting(!isDeleting);
      } else {
        setIsDeleting(!isDeleting);
      }
    }
  }, [isDeleting, modalConfig, t, vulnerabilities]);
  const DEBOUNCE_DELAY_MS = 2000;
  const refreshAssignedDebounced = useDebouncedCallback((): void => {
    refreshAssigned();
  }, DEBOUNCE_DELAY_MS);

  const onDeleteResult = useCallback(
    (removeVulnerabilityResult: RemoveVulnerabilityMutationMutation): void => {
      refreshAssignedDebounced();
      onRemoveVulnerabilityResultHelper(removeVulnerabilityResult, t);
      modalConfig.clearSelected();
      setIsDeleting(false);
      setIsDeleteOpen(false);
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [modalConfig, t],
  );

  const onClose = useCallback(
    (index: number): (() => void) =>
      (): void => {
        handleCloseUpdateModal(index);
      },
    [handleCloseUpdateModal],
  );

  const enabledRows = useCallback(
    (row: Row<IVulnRowAttr>): boolean => {
      if (isReattacking && isRejectedOrSafeOrSubmitted(row)) {
        return false;
      }

      if (isReattacking && isVerificationRequestedOrOnHold(row)) {
        return false;
      }

      if (isReattacking && !isValidVulnerability(row.original, groups)) {
        return false;
      }

      if (isDeleting && isSafe(row)) {
        return false;
      }

      return true;
    },
    [groups, isDeleting, isReattacking],
  );

  const displayMessage = (): void => {
    if (isReattacking && !isOpen) {
      msgSuccess(
        t("searchFindings.tabVuln.info.text"),
        t("searchFindings.tabVuln.info.title"),
      );
    }
  };

  useEffect(displayMessage, [isReattacking, isOpen, t]);

  useEffect((): VoidFunction => {
    return (): void => {
      resetVulnsStore();
    };
  }, [resetVulnsStore]);

  return (
    <React.StrictMode>
      <VulnComponent
        columns={columns}
        cvssVersion={cvssVersion.accessorKey}
        extraBelowButtons={
          <Toggle
            defaultChecked={cvssVersion.accessorKey === "severityThreatScore"}
            leftDescription={"CVSS 3.1"}
            name={"cvssToggle"}
            onChange={onChangeCvssVersion}
            rightDescription={"CVSS 4.0"}
          />
        }
        extraButtons={
          <Container alignItems={"center"} display={"flex"} gap={0.5}>
            <ActionButtons
              areVulnerabilitiesReattacked={
                vulnerabilities.filter(
                  (vuln): boolean =>
                    !vuln.remediated &&
                    filteredContinuousVulnerabilitiesOnReattackIds(
                      vulnerabilities,
                      groups,
                    ).includes(vuln.id),
                ).length === 0
              }
              areVulnsSelected={modalConfig.selectedVulnerabilities.length > 0}
              isEditing={isEditing}
              isRequestingReattack={isReattacking}
              onRequestReattack={onReattack}
              openModal={toggleModal}
              refreshAssigned={refreshAssigned}
            />
            <DeleteVulnerabilitiesButton
              areDeletableLocations={
                vulnerabilities.filter((vuln): boolean => vuln.state !== "SAFE")
                  .length > 0
              }
              areVulnsSelected={modalConfig.selectedVulnerabilities.length > 0}
              isDeleting={isDeleting}
              isEditing={isEditing}
              isRequestingReattack={isReattacking}
              onCancel={closeDeleteModal}
              onDeleting={onDelete}
            />
            <EditButton
              isDisabled={modalConfig.selectedVulnerabilities.length === 0}
              isEditing={isEditing}
              isRequestingReattack={isReattacking}
              onEdit={toggleEdit}
            />
          </Container>
        }
        filters={
          <AssignedVulnerabilitiesFilter
            setFilteredData={setFilteredData}
            vulnerabilities={vulnerabilities}
          />
        }
        nonValidOnReattackVulns={Array.from(
          new Set(
            vulnerabilities.filter(
              (vulnerability: IVulnRowAttr): boolean =>
                !filteredContinuousVulnerabilitiesOnReattackIds(
                  vulnerabilities,
                  groups,
                ).includes(vulnerability.id),
            ),
          ),
        )}
        onVulnSelect={openRemediationModal}
        refetchData={refetchVulnerabilitiesAssigned}
        tableOptions={{ enableRowSelection: enabledRows }}
        tableRefProps={{ id: "taskView" }}
        vulnerabilities={filteredData}
      />
      <RemoveVulnerabilityModal
        onClose={closeDeleteModal}
        onRemoveVulnRes={onDeleteResult}
        open={isDeleteOpen}
        vulnerabilities={modalConfig.selectedVulnerabilities}
      />
      {isOpen ? (
        <UpdateVerificationModal
          clearSelected={_.get(modalConfig, "clearSelected")}
          handleCloseModal={closeRemediationModal}
          refetchData={refetchVulnerabilitiesAssigned}
          setRequestState={onReattack}
          setVerifyState={onReattack}
          vulns={modalConfig.selectedVulnerabilities}
        />
      ) : undefined}
      {isEditing && modalConfig.selectedVulnerabilities.length > 0
        ? Object.entries(
            _.groupBy(
              modalConfig.selectedVulnerabilities,
              (vuln): string => vuln.groupName,
            ),
          ).map(
            ([vulnGroupName, vulnerabilitiesToUpdated], index): JSX.Element => (
              <TreatmentModal
                findingId={""}
                groupName={vulnGroupName}
                handleClearSelected={_.get(modalConfig, "clearSelected")}
                key={vulnGroupName}
                modalProps={{
                  close: onClose(index),
                  isOpen: iscurrentOpen[index],
                  name: "edit-vulnerability-modal",
                  open: toggleEdit,
                  setIsOpen: setIsEditing,
                  toggle: open,
                }}
                refetchData={refetchVulnerabilitiesAssigned}
                vulnerabilities={vulnerabilitiesToUpdated}
              />
            ),
          )
        : undefined}
    </React.StrictMode>
  );
};

export { AssignedVulnerabilities };
