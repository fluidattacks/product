import { graphql } from "gql";

const GET_ME_VULNERABILITIES_ASSIGNED = graphql(`
  query GetMeVulnerabilitiesAssigned($canRetrieveHacker: Boolean! = false) {
    me {
      userEmail
      vulnerabilitiesAssigned {
        ...vulnFields
        finding {
          id
          title
        }
      }
    }
  }
`);

export { GET_ME_VULNERABILITIES_ASSIGNED };
