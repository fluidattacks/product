import { graphql } from "gql";

graphql(`
  fragment vulnSeverityRequestFields on Vulnerability {
    id
    closingDate
    customSeverity
    externalBugTrackingSystem
    findingId
    groupName
    hacker @include(if: $canRetrieveHacker)
    lastEditedBy
    lastStateDate
    lastTreatmentDate
    lastVerificationDate
    priority
    proposedSeverityAuthor
    proposedSeverityThreatScore
    proposedSeverityVectorV4
    remediated
    reportDate
    rootNickname
    severityTemporalScore
    severityThreatScore

    severityVector
    severityVectorV4

    source
    specific
    state
    stateReasons
    stream
    tag
    technique
    treatmentAcceptanceDate
    treatmentAcceptanceStatus
    treatmentAssigned
    treatmentJustification
    treatmentStatus
    treatmentUser
    verification
    verificationJustification
    vulnerabilityType
    where
    zeroRisk
    advisories {
      cve
      epss
      package
      vulnerableVersion
    }
    root {
      __typename
      ... on GitRoot {
        branch
        criticality
      }
    }
  }
`);

export const GET_VULNERABILITIES_SEVERITY_UPDATE_REQUESTS = graphql(`
  query GetVulnerabilitiesSeverityUpdateRequests(
    $canRetrieveHacker: Boolean! = true
  ) {
    me {
      vulnerabilitiesSeverityRequests {
        edges {
          node {
            ...vulnSeverityRequestFields
          }
        }
        pageInfo {
          endCursor
          hasNextPage
        }
      }
    }
  }
`);
