import { screen, waitFor } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import { GraphQLError } from "graphql";
import _ from "lodash";
import { HttpResponse, type StrictResponse, graphql } from "msw";
import { Route, Routes } from "react-router-dom";

import { GET_VULNERABILITIES_SEVERITY_UPDATE_REQUESTS } from "./queries";

import { VulnerabilitySeverityUpdateRequests } from ".";
import { GET_USER_ORGANIZATIONS_GROUPS } from "../queries";
import { AllGroupPermissionsProvider } from "context/authz/all-group-permissions-provider";
import { GET_GROUP_USERS } from "features/vulnerabilities/queries";
import { REJECT_VULNS_SEVERITY_REQUEST } from "features/vulnerabilities/severity-request-form/queries";
import type {
  GetGroupUsersQuery,
  GetUserOrganizationsGroupsAtDashboardQuery,
  RejectVulnerabilitiesSeverityMutation,
} from "gql/graphql";
import {
  type GetVulnerabilitiesSeverityUpdateRequestsQuery,
  InvitationState,
  RootCriticality,
  Technique,
  type VulnSeverityRequestFieldsFragment,
  VulnerabilityState,
  VulnerabilityTreatment,
} from "gql/graphql";
import { render } from "mocks";
import { LINK } from "mocks/constants";
import type { IErrorMessage } from "mocks/types";
import { msgSuccess } from "utils/notifications";

jest.mock("utils/notifications", (): Record<string, unknown> => {
  const mockedNotifications: Record<string, () => Record<string, unknown>> =
    jest.requireActual("utils/notifications");
  jest.spyOn(mockedNotifications, "msgError").mockImplementation();
  jest.spyOn(mockedNotifications, "msgSuccess").mockImplementation();

  return mockedNotifications;
});

describe("vulnSeverityRequests", (): void => {
  const graphqlMocked = graphql.link(LINK);

  const mocksVulnerabilities = [
    graphqlMocked.query(
      GET_VULNERABILITIES_SEVERITY_UPDATE_REQUESTS,
      (): StrictResponse<{
        data: GetVulnerabilitiesSeverityUpdateRequestsQuery;
      }> => {
        return HttpResponse.json({
          data: {
            me: {
              __typename: "Me",
              vulnerabilitiesSeverityRequests: {
                __typename: "VulnerabilitiesConnection",
                edges: [
                  {
                    __typename: "VulnerabilityEdge",
                    node: {
                      __typename: "Vulnerability",
                      advisories: null,
                      closingDate: null,
                      customSeverity: null,
                      externalBugTrackingSystem: null,
                      findingId: "123",
                      groupName: "group2",
                      hacker: "test@test.test",
                      id: "test",
                      lastEditedBy: "test@test.test",
                      lastStateDate: "2019-09-13 08:17:41",
                      lastTreatmentDate: "2019-09-13 08:17:41",
                      lastVerificationDate: "2020-02-19 10:41:04",
                      priority: 250.0,
                      proposedSeverityAuthor: "hacker@test.com",
                      proposedSeverityThreatScore: 2.1,
                      proposedSeverityVectorV4:
                        "CVSS:4.0/AV:N/AC:L/AT:N/PR:L/UI:N/VC:L/VI:L/VA:N/SC:N/SI:N/SA:N/E:P",
                      remediated: false,
                      reportDate: "2019-09-13 08:17:41",
                      root: {
                        __typename: "GitRoot",
                        branch: "master",
                        criticality: RootCriticality.Medium,
                      },
                      rootNickname: "universe",
                      severityTemporalScore: 2.7,
                      severityThreatScore: 6.9,
                      severityVector:
                        "CVSS:3.1/AV:A/AC:H/PR:N/UI:R/S:C/C:N/I:N/A:L/E:F/RL:W/RC:R/CR:H/IR:L/AR:H/MAV:N/MAC:L/MPR:N/MUI:N/MS:U",
                      severityVectorV4:
                        "CVSS:4.0/AV:A/AC:H/AT:N/PR:N/UI:A/VC:N/VI:N/VA:L/SC:L/SI:L/SA:L/MAV:N/MAC:L/MPR:N/MUI:N/MSC:N/MSI:N/MSA:N/CR:H/IR:L/AR:H/E:A",
                      source: "asm",
                      specific: "666",
                      state: VulnerabilityState.Vulnerable,
                      stateReasons: null,
                      stream: null,
                      tag: "",
                      technique: Technique.Ptaas,
                      treatmentAcceptanceDate: null,
                      treatmentAcceptanceStatus: null,
                      treatmentAssigned: null,
                      treatmentJustification: null,
                      treatmentStatus: VulnerabilityTreatment.Untreated,
                      treatmentUser: null,
                      verification: "Verified",
                      verificationJustification: null,
                      vulnerabilityType: "ports",
                      where: "https://example.com/inputs",
                      zeroRisk: null,
                    } as VulnSeverityRequestFieldsFragment,
                  },
                ],
                pageInfo: {
                  __typename: "PageInfo",
                  endCursor:
                    "VULN#20c84933-40e7-47f0-a56b-1c0028a818d1#FIN#436992569",
                  hasNextPage: false,
                },
              },
            },
          },
        });
      },
    ),
  ];

  const mocksUserGroups = [
    graphqlMocked.query(
      GET_USER_ORGANIZATIONS_GROUPS,
      (): StrictResponse<{
        data: GetUserOrganizationsGroupsAtDashboardQuery;
      }> => {
        return HttpResponse.json({
          data: {
            me: {
              __typename: "Me",
              organizations: [
                {
                  groups: [
                    {
                      __typename: "Group",
                      name: "group1",
                      permissions: [
                        "integrates_api_mutations_approve_vulnerabilities_severity_update_mutate",
                      ],
                      serviceAttributes: ["has_advanced", "is_continuous"],
                    },
                    {
                      __typename: "Group",
                      name: "group2",
                      permissions: [
                        "integrates_api_mutations_approve_vulnerabilities_severity_update_mutate",
                      ],
                      serviceAttributes: ["has_advanced", "is_continuous"],
                    },
                  ],
                  name: "orgtest",
                },
              ],
              userEmail: "test@test.test",
            },
          },
        });
      },
    ),
  ];

  const mocksGroupStakeholder = [
    graphqlMocked.query(
      GET_GROUP_USERS,
      ({
        variables,
      }): StrictResponse<IErrorMessage | { data: GetGroupUsersQuery }> => {
        const { groupName } = variables;

        if (groupName === "group1") {
          return HttpResponse.json({
            data: {
              group: {
                __typename: "Group",
                name: groupName,
                stakeholders: [
                  {
                    __typename: "Stakeholder",
                    email: "manager1_test@test.test",
                    invitationState: InvitationState.Registered,
                    role: "user",
                  },
                ],
              },
            },
          });
        }
        if (groupName === "group2") {
          return HttpResponse.json({
            data: {
              group: {
                __typename: "Group",
                name: groupName,
                stakeholders: [
                  {
                    __typename: "Stakeholder",
                    email: "manager1_test@test.test",
                    invitationState: InvitationState.Registered,
                    role: "user",
                  },
                ],
              },
            },
          });
        }

        return HttpResponse.json({
          errors: [new GraphQLError("Get group user error")],
        });
      },
    ),
  ];

  it("should render a table of vulnerability severity requests", async (): Promise<void> => {
    expect.hasAssertions();

    render(
      <AllGroupPermissionsProvider>
        <Routes>
          <Route
            element={<VulnerabilitySeverityUpdateRequests />}
            path={"/todos/vuln-severity-requests"}
          />
        </Routes>
      </AllGroupPermissionsProvider>,
      {
        memoryRouter: {
          initialEntries: ["/todos/vuln-severity-requests/"],
        },
        mocks: [
          ...mocksVulnerabilities,
          ...mocksUserGroups,
          ...mocksGroupStakeholder,
        ],
      },
    );

    const btnApprove = "severityUpdateRequests.approveSeverityChanges.text";

    await waitFor((): void => {
      expect(
        screen.getByRole("cell", { name: "https://example.com/inputs" }),
      ).toBeInTheDocument();
    });

    await waitFor((): void => {
      expect(screen.getAllByRole("row")).toHaveLength(2);
    });

    expect(screen.getByRole("button", { name: btnApprove })).toBeDisabled();
  });

  it("should reject vuln severity request", async (): Promise<void> => {
    expect.hasAssertions();

    jest.clearAllMocks();

    const mocksMutation = [
      graphqlMocked.mutation(
        REJECT_VULNS_SEVERITY_REQUEST,
        ({
          variables,
        }): StrictResponse<
          IErrorMessage | { data: RejectVulnerabilitiesSeverityMutation }
        > => {
          const { findingId, justification, vulnerabilityIds } = variables;

          if (
            findingId === "123" &&
            justification === "Test reject justification" &&
            _.isEqual(vulnerabilityIds, ["test"])
          ) {
            return HttpResponse.json({
              data: {
                rejectVulnerabilitiesSeverityUpdate: { success: true },
              },
            });
          }

          return HttpResponse.json({
            errors: [new GraphQLError("Exception requesting verification")],
          });
        },
      ),
    ];

    render(
      <AllGroupPermissionsProvider>
        <Routes>
          <Route
            element={<VulnerabilitySeverityUpdateRequests />}
            path={"/todos/vuln-severity-requests"}
          />
        </Routes>
      </AllGroupPermissionsProvider>,
      {
        memoryRouter: {
          initialEntries: ["/todos/vuln-severity-requests/"],
        },
        mocks: [
          ...mocksVulnerabilities,
          ...mocksUserGroups,
          ...mocksGroupStakeholder,
          ...mocksMutation,
        ],
      },
    );
    await waitFor((): void => {
      expect(screen.getAllByRole("row")).toHaveLength(2);
    });
    const btnApprove = "severityUpdateRequests.approveSeverityChanges.text";

    expect(
      screen.getByRole("button", {
        name: btnApprove,
      }),
    ).toBeDisabled();

    expect(screen.getAllByRole("checkbox")[2]).not.toBeChecked();

    await userEvent.click(screen.getAllByRole("checkbox")[2]);

    expect(screen.getAllByRole("checkbox")[2]).toBeChecked();

    expect(screen.getByRole("button", { name: btnApprove })).toBeEnabled();

    await userEvent.click(screen.getByRole("button", { name: btnApprove }));

    expect(screen.getByRole("button", { name: "Confirm" })).toBeDisabled();

    const btnReject = "severityUpdateRequests.handleRequestApproval.reject";

    await userEvent.click(screen.getByRole("button", { name: btnReject }));

    await userEvent.type(
      screen.getByTestId("justification-text-area"),
      "Test reject justification",
    );

    expect(screen.getByRole("button", { name: "Confirm" })).toBeEnabled();

    await userEvent.click(screen.getByRole("button", { name: "Confirm" }));

    await waitFor((): void => {
      expect(msgSuccess).toHaveBeenCalledWith(
        "severityUpdateRequests.handleRequestApproval.success.rejected",
      );
    });
  });
});
