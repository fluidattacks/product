import { Button, Container } from "@fluidattacks/design";
import { useTranslation } from "react-i18next";

import { Authorize } from "components/@core/authorize";

const ActionButtons = ({
  areVulnsSelected,
  refreshVulns,
  onApprove,
}: Readonly<{
  areVulnsSelected: boolean;
  refreshVulns: () => void;
  onApprove: () => void;
}>): JSX.Element => {
  const { t } = useTranslation();

  return (
    <Container alignItems={"center"} display={"flex"} gap={0.5} scroll={"none"}>
      <Button
        icon={"refresh"}
        id={"refresh-drafts"}
        onClick={refreshVulns}
        variant={"ghost"}
      >
        {t("todoList.refresh")}
      </Button>
      <Authorize
        can={
          "integrates_api_mutations_approve_vulnerabilities_severity_update_mutate"
        }
        have={"is_continuous"}
      >
        <Button
          disabled={!areVulnsSelected}
          icon={"check"}
          id={"start-severity-approval"}
          onClick={onApprove}
          tooltip={t("severityUpdateRequests.approveSeverityChanges.tooltip")}
          variant={"ghost"}
        >
          {t("severityUpdateRequests.approveSeverityChanges.text")}
        </Button>
      </Authorize>
    </Container>
  );
};

export { ActionButtons };
