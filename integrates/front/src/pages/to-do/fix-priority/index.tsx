import { Text } from "@fluidattacks/design";
import type { ColumnDef } from "@tanstack/react-table";
import { StrictMode } from "react";
import * as React from "react";
import { useTranslation } from "react-i18next";
import { useTheme } from "styled-components";

import { formatLinkHandler } from "components/table/table-formatters";
import { PriorityRanking } from "features/fix-priority";
import type { IVulnerabilityAttr } from "features/fix-priority/types";
import { formatTreatment } from "features/fix-priority/utils";

const tableColumns: ColumnDef<IVulnerabilityAttr>[] = [
  {
    accessorFn: (row): string => row.where,
    enableColumnFilter: false,
    header: "Location",
  },
  {
    accessorFn: (row): string => row.groupName,
    cell: (cell): JSX.Element => {
      const orgName = cell.row.original.finding.organizationName;
      const link = `/orgs/${orgName}/groups/${cell.row.original.groupName}`;
      const text = cell.getValue<string>();

      return formatLinkHandler(link, text);
    },
    enableColumnFilter: false,
    header: "Group name",
  },
  {
    accessorFn: (row): string => row.finding.title,
    cell: (cell): JSX.Element => {
      const orgName = cell.row.original.finding.organizationName;
      const { findingId } = cell.row.original;
      const link =
        `/orgs/${orgName}/groups/${cell.row.original.groupName}` +
        `/vulns/${findingId}/locations`;
      const text = cell.getValue<string>();

      return formatLinkHandler(link, text);
    },
    enableColumnFilter: false,
    header: "Vulnerability",
  },
  {
    accessorFn: (row): number => row.priority,
    accessorKey: "priority",
    header: "Priority score",
  },
  {
    accessorFn: (row): string => formatTreatment(row.treatmentStatus),
    accessorKey: "treatmentStatus",
    header: "Treatment",
  },
  {
    accessorFn: (row): string => row.treatmentAssigned ?? "",
    accessorKey: "treatmentAssigned",
    header: "Assignees",
  },
  {
    accessorFn: (row): string => row.reportDate ?? "",
    enableColumnFilter: false,
    header: "Report date",
  },
];

const FixPriority: React.FC = (): JSX.Element => {
  const { t } = useTranslation();
  const theme = useTheme();

  return (
    <StrictMode>
      <Text
        color={theme.palette.gray[800]}
        fontWeight={"bold"}
        mb={1.25}
        size={"xl"}
      >
        {t("todoList.tabs.fixPriority.header")}
      </Text>
      <PriorityRanking columns={tableColumns} />
    </StrictMode>
  );
};

export { FixPriority };
