import { screen, waitFor, within } from "@testing-library/react";
import { userEvent } from "@testing-library/user-event";
import type { GraphQLHandler, StrictResponse } from "msw";
import { HttpResponse, graphql } from "msw";
import { Route, Routes } from "react-router-dom";

import { GET_TODO_EVENTS } from "./queries";

import { Events } from ".";
import type {
  Event,
  EventEdge,
  GetTodoEventsQuery as GetTodoEvents,
  Root,
} from "gql/graphql";
import { EventType } from "gql/graphql";
import { render } from "mocks";
import { LINK } from "mocks/constants";

describe("events", (): void => {
  const mockEvents = (events: Partial<Event>[]): GraphQLHandler =>
    graphql
      .link(LINK)
      .query(GET_TODO_EVENTS, (): StrictResponse<{ data: GetTodoEvents }> => {
        return HttpResponse.json({
          data: {
            me: {
              __typename: "Me",
              pendingEvents: {
                edges: events.map(
                  (event): EventEdge => ({
                    cursor: "",
                    node: event as Event,
                  }),
                ),
                pageInfo: {
                  endCursor: "",
                  hasNextPage: false,
                },
              },
              userEmail: "test@fluidattacks.com",
            },
          },
        });
      });

  const mockEventsList: Partial<Event>[] = [
    {
      __typename: "Event",
      detail: "El entorno parece tener problemas de instalación.",
      environment: null,
      eventDate: "2022-04-12 10:00:00",
      eventStatus: "CREATED",
      eventType: EventType.EnvironmentIssues,
      groupName: "unittesting",
      id: "1830384",
      organization: "okada",
      root: {
        id: "09a0aw3-a6ff-4a44-b889-2e01a0d102d",
        nickname: "bgx-backend",
      } as Root,
    },
  ];

  it("should render a component and its colunms and data", async (): Promise<void> => {
    expect.hasAssertions();

    render(
      <Routes>
        <Route element={<Events />} path={"/todos/drafts"} />
      </Routes>,
      {
        memoryRouter: {
          initialEntries: ["/todos/drafts"],
        },
        mocks: [mockEvents(mockEventsList)],
      },
    );

    await waitFor((): void => {
      expect(screen.getByText("Group name")).toBeInTheDocument();
    });

    expect(screen.queryAllByRole("table")).toHaveLength(1);

    expect(screen.getByText("Group name")).toBeInTheDocument();
    expect(
      screen.getByRole("columnheader", { name: "Description" }),
    ).toBeInTheDocument();
    expect(
      screen.getByRole("columnheader", { name: "Event date" }),
    ).toBeInTheDocument();
    expect(screen.getByText("Organization")).toBeInTheDocument();
    expect(screen.getByText("Root")).toBeInTheDocument();
  });

  it("should render a component and its rows and data", async (): Promise<void> => {
    expect.hasAssertions();

    render(
      <Routes>
        <Route element={<Events />} path={"/todos/drafts"} />
      </Routes>,
      {
        memoryRouter: {
          initialEntries: ["/todos/drafts"],
        },
        mocks: [mockEvents(mockEventsList)],
      },
    );

    await waitFor((): void => {
      expect(screen.getByText("unittesting")).toBeInTheDocument();
    });

    expect(screen.queryAllByRole("row")).toHaveLength(2);
    expect(screen.getByText("okada")).toBeInTheDocument();
    expect(screen.getAllByText("Unsolved")).toHaveLength(1);
    expect(screen.getByText("2022-04-12")).toBeInTheDocument();
    expect(
      screen.getByText("El entorno parece tener problemas de instalación."),
    ).toBeInTheDocument();
  });

  it("shoul filter", async (): Promise<void> => {
    expect.hasAssertions();

    const additionalEvents: Partial<Event>[] = [
      {
        __typename: "Event",
        detail: "Las credenciales no están siendo aceptadas.",
        environment: null,
        eventDate: "2022-04-12 10:00:00",
        eventStatus: "VERIFICATION_REQUESTED",
        eventType: EventType.CredentialIssues,
        groupName: "unittesting",
        id: "1830332",
        organization: "unittesting",
        root: {
          id: "09a0aw3-a6ff-4a44-b889-2e01a0d1032",
          nickname: "backend",
        } as Root,
      },
    ];

    render(
      <Routes>
        <Route element={<Events />} path={"/todos/drafts"} />
      </Routes>,
      {
        memoryRouter: {
          initialEntries: ["/todos/drafts"],
        },
        mocks: [mockEvents([...mockEventsList, ...additionalEvents])],
      },
    );

    await waitFor((): void => {
      expect(screen.queryAllByRole("row")).toHaveLength(3);
    });

    const filter = document.querySelectorAll("#filterBtn");
    const user = userEvent.setup();
    await user.click(filter[0]);

    const statusSelect = screen.getByTestId("eventStatus-select");
    await user.click(within(statusSelect).getByTestId("chevron-down-icon"));
    await user.click(within(statusSelect).getByText("Pending verification"));

    expect(screen.queryAllByRole("row")).toHaveLength(2);
    expect(
      screen.getByText("Las credenciales no están siendo aceptadas."),
    ).toBeInTheDocument();

    await user.click(screen.getByText("filter.cancel"));

    expect(screen.queryAllByRole("row")).toHaveLength(3);

    await userEvent.type(
      screen.getByRole("combobox", { name: "detail" }),
      "instalación",
    );

    expect(screen.queryAllByRole("row")).toHaveLength(2);
    expect(
      screen.getByText("El entorno parece tener problemas de instalación."),
    ).toBeInTheDocument();
  });

  it("should filter by organization", async (): Promise<void> => {
    expect.hasAssertions();

    const additionalEvents: Partial<Event>[] = [
      {
        __typename: "Event",
        detail: "Las credenciales no están siendo aceptadas.",
        environment: null,
        eventDate: "2022-04-12 10:00:00",
        eventStatus: "VERIFICATION_REQUESTED",
        eventType: EventType.CredentialIssues,
        groupName: "unittesting",
        id: "1830332",
        organization: "okada",
        root: {
          id: "09a0aw3-a6ff-4a44-b889-2e01a0d1032",
          nickname: "backend",
        } as Root,
      },
      {
        __typename: "Event",
        detail: "El entorno parece tener problemas de instalación.",
        environment: null,
        eventDate: "2022-04-12 10:00:00",
        eventStatus: "CREATED",
        eventType: EventType.EnvironmentIssues,
        groupName: "unittesting",
        id: "1830385",
        organization: "anotherOrg",
        root: {
          id: "09a0aw3-a6ff-4a44-b889-2e01a0d102e",
          nickname: "bgx-backend",
        } as Root,
      },
    ];

    render(
      <Routes>
        <Route element={<Events />} path={"/todos/drafts"} />
      </Routes>,
      {
        memoryRouter: {
          initialEntries: ["/todos/drafts"],
        },
        mocks: [mockEvents([...mockEventsList, ...additionalEvents])],
      },
    );

    await waitFor((): void => {
      expect(screen.queryAllByRole("row")).toHaveLength(4);
    });

    const filter = document.querySelectorAll("#filterBtn");
    const user = userEvent.setup();
    await user.click(filter[0]);

    const organizationInput = screen.getByTestId("organization-input");
    await user.type(organizationInput, "anotherOrg");

    expect(screen.queryAllByRole("row")).toHaveLength(2);
    expect(
      screen.getByText("El entorno parece tener problemas de instalación."),
    ).toBeInTheDocument();
  });
});
