import { useQuery } from "@apollo/client";
import { useEffect, useState } from "react";

import { GET_TODO_EVENTS } from "./queries";
import type { IEventAttr } from "./types";
import { formatTodoEvents } from "./utils";

import { useAudit } from "hooks/use-audit";

interface IUseEventsQuery {
  events: IEventAttr[];
  loading: boolean;
}

const useEventsQuery = (): IUseEventsQuery => {
  const [allEvents, setEvents] = useState<IEventAttr[]>([]);

  const { addAuditEvent } = useAudit();
  const { data, loading, fetchMore } = useQuery(GET_TODO_EVENTS, {
    fetchPolicy: "cache-first",
    onCompleted: (): void => {
      addAuditEvent("ToDo.Events", "unknown");
    },
    variables: { after: "", first: 100 },
  });

  useEffect((): void => {
    if (data !== undefined) {
      const events = data.me.pendingEvents.edges;
      if (events.length > 0) {
        setEvents(events.map((edge): IEventAttr => edge.node as IEventAttr));
      }
    }
  }, [data]);

  // REFAC NEEDED: Implement proper server-side pagination with the table
  useEffect((): void => {
    if (data !== undefined) {
      const { hasNextPage, endCursor } = data.me.pendingEvents.pageInfo;
      if (hasNextPage) {
        void fetchMore({ variables: { after: endCursor } });
      }
    }
  }, [data, fetchMore]);

  const events = formatTodoEvents(allEvents);

  return { events, loading };
};

export { useEventsQuery };
