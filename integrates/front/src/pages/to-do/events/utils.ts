import type { IEventAttr } from "./types";

import { castEventStatus, castEventType } from "utils/format-helpers";
import { translate } from "utils/translations/translate";

const formatTodoEvents = (dataset: IEventAttr[]): IEventAttr[] => {
  return dataset.map((event): IEventAttr => {
    const eventType = translate.t(castEventType(event.eventType));
    const eventStatus = translate.t(castEventStatus(event.eventStatus).status);

    return {
      ...event,
      eventStatus,
      eventType,
    };
  });
};

export { formatTodoEvents };
