import _ from "lodash";

import type { IOrganizationGroups } from "@types";
import { getOrganizationGroups } from "features/vulnerabilities/utils";
import type {
  IFindingFormatted,
  IFindingToReattackEdge,
  ITodoFindingToReattackAttr,
  IVulnerabilityEdge,
} from "pages/to-do/reattacks/types";

const getOldestRequestedReattackDate = (
  edges: IVulnerabilityEdge[],
): string => {
  const vulnsDates = edges.map(
    (vulnEdge): string => vulnEdge.node.lastRequestedReattackDate,
  );
  const minDate = _.min(vulnsDates);
  if (_.isUndefined(minDate)) {
    return "-";
  }

  return minDate;
};

const noDate = (finding: IFindingFormatted): boolean => {
  return finding.oldestReattackRequestedDate !== "-";
};

const formatFindings = (
  organizationsGroups: IOrganizationGroups[] | undefined,
  findings: IFindingToReattackEdge[],
): IFindingFormatted[] => {
  const formatted = findings
    .map((edge): ITodoFindingToReattackAttr => edge.node)
    .map((finding): IFindingFormatted => {
      const organizationGroups = getOrganizationGroups(
        organizationsGroups,
        finding.groupName,
      );

      const organizationName =
        organizationGroups === undefined ? "" : organizationGroups.name;

      return {
        ...finding,
        oldestReattackRequestedDate: getOldestRequestedReattackDate(
          finding.vulnerabilitiesToReattackConnection.edges,
        ),
        organizationName,
        url: `https://app.fluidattacks.com/orgs/${organizationName}/groups/${finding.groupName}/vulns/${finding.id}/locations`,
      };
    });

  const fmtd = formatted.filter(noDate);

  return _.orderBy(fmtd, ["oldestReattackRequestedDate"], ["asc"]);
};

export { formatFindings };
