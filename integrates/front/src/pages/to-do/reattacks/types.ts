interface ITodoFindingToReattackAttr {
  groupName: string;
  id: string;
  title: string;
  vulnerabilitiesToReattackConnection: IVulnerabilitiesConnection;
  verificationSummary: {
    onHold: string;
    requested: string;
    verified: string;
  };
}

interface IFindingToReattackEdge {
  node: ITodoFindingToReattackAttr;
}

interface IFindingToReattackConnection {
  edges: IFindingToReattackEdge[];
  pageInfo: {
    endCursor: string;
    hasNextPage: boolean;
  };
  total: number | undefined;
}

interface IVulnerabilityAttr {
  id: string;
  lastRequestedReattackDate: string;
}

interface IVulnerabilityEdge {
  node: IVulnerabilityAttr;
}

interface IVulnerabilitiesConnection {
  edges: IVulnerabilityEdge[];
  pageInfo: {
    endCursor: string;
    hasNextPage: boolean;
  };
  total: number | undefined;
}
interface IFindingFormatted extends ITodoFindingToReattackAttr {
  oldestReattackRequestedDate: string;
  organizationName: string | undefined;
  url: string;
}

export type {
  IFindingFormatted,
  IFindingToReattackConnection,
  IFindingToReattackEdge,
  ITodoFindingToReattackAttr,
  IVulnerabilityAttr,
  IVulnerabilityEdge,
};
