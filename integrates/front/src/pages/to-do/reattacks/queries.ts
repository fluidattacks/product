import { graphql } from "gql";

export const GET_TODO_REATTACKS = graphql(`
  query GetTodoReattacksVulnerable($after: String, $first: Int) {
    me {
      userEmail
      findingReattacksConnection(after: $after, first: $first) {
        total
        edges {
          node {
            id
            groupName
            title
            verificationSummary {
              onHold
              requested
              verified
            }
            vulnerabilitiesToReattackConnection {
              total
              edges {
                node {
                  id
                  lastRequestedReattackDate
                }
              }
              pageInfo {
                endCursor
                hasNextPage
              }
            }
          }
        }
        pageInfo {
          endCursor
          hasNextPage
        }
      }
    }
  }
`);
