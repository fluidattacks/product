# shellcheck shell=bash

function main {
  local return_value=1

  : \
    && pushd integrates/front \
    && npm ci \
    && if npm run types -- --check; then
      info 'Generated types are up-to-date!' \
        && return_value=0
    else
      info 'Generated types are not up-to-date.' \
        && info 'we will generate the types,' \
        && info 'but the job will fail.' \
        && npm run types \
        && return_value=1
    fi \
    && popd \
    && return "${return_value}"
}

main "${@}"
