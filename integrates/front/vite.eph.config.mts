import type { PluginOption, UserConfig, UserConfigExport } from "vite";
import istanbul from "vite-plugin-istanbul";

import prodConfig from "./vite.prod.config.mjs";

const ephConfig: UserConfigExport = {
  ...prodConfig,
  plugins: [
    ...((prodConfig as UserConfig).plugins as PluginOption[]),
    istanbul({
      cypress: true,
      forceBuildInstrument: true,
      requireEnv: false,
    }),
  ],
};

// eslint-disable-next-line import/no-default-export
export default ephConfig;
