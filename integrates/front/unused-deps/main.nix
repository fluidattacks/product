{ inputs, makeScript, ... }:
makeScript {
  entrypoint = ./entrypoint.sh;
  name = "integrates-front-unused-deps";
  searchPaths = { bin = [ inputs.nixpkgs.bash inputs.nixpkgs.nodejs_20 ]; };
}
