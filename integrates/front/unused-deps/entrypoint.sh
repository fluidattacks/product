# shellcheck shell=bash

function main {

  : \
    && pushd integrates/front \
    && npm install \
    && npm run depcheck \
    && popd \
    || return 1
}

main "${@}"
