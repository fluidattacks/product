{ inputs, makeScript, outputs, ... }: {
  jobs."/integrates/front/coverage" = makeScript {
    entrypoint = ''
      min_branch_cov=65
      min_line_cov=80

      npx --yes --silent nyc report --temp-dir integrates/front/.coverage --reporter=text-summary
      npx --yes --silent nyc check-coverage --temp-dir integrates/front/.coverage --branches "$min_branch_cov" --lines "$min_line_cov"

      aws_login dev 3600
      sops_export_vars common/secrets/dev.yaml CODECOV_TOKEN

      codecov-cli upload-process --flag integrates-front --dir integrates/front
    '';
    name = "integrates-front-coverage";
    searchPaths = {
      bin = [
        inputs.codecov-cli
        inputs.nixpkgs.bash
        inputs.nixpkgs.git
        inputs.nixpkgs.nodejs_20
      ];
      source = [ outputs."/common/utils/aws" outputs."/common/utils/sops" ];
    };
  };
}
