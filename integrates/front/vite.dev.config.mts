import type { PluginOption, UserConfig, UserConfigExport } from "vite";
import istanbul from "vite-plugin-istanbul";

import { commonConfig } from "./vite.common.config.mjs";

const devConfig: UserConfigExport = {
  ...commonConfig,
  mode: "development",
  plugins: [
    ...((commonConfig as UserConfig).plugins as PluginOption[]),
    istanbul({
      cypress: true,
      requireEnv: false,
    }),
  ],
  preview: {
    headers: { "Access-Control-Allow-Origin": "https://localhost:8001" },
    https: {
      cert: process.env.FI_WEBPACK_TLS_CERT,
      key: process.env.FI_WEBPACK_TLS_KEY,
    },
    port: 3000,
    strictPort: true,
  },
  server: {
    headers: { "Access-Control-Allow-Origin": "https://localhost:8001" },
    host: "0.0.0.0",
    https: {
      cert: process.env.FI_WEBPACK_TLS_CERT,
      key: process.env.FI_WEBPACK_TLS_KEY,
    },
    origin: "https://localhost:3000",
    port: 3000,
    strictPort: true,
  },
};

// eslint-disable-next-line import/no-default-export
export default devConfig;
