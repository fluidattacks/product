{ projectPath, ... }:
let
  arch = let
    commit = "3c8778c732597a2fdee42d7c20d1ab5d948e13d4";
    sha256 = "sha256:0ii4qam4ykq9gbi2lmkk9p9gixmrypmz54jgcji9hpypk4azxy3w";
    url =
      "https://gitlab.com/fluidattacks/universe/-/raw/${commit}/common/ci/arch.nix";
    src = builtins.fetchurl { inherit sha256 url; };
  in import src;

  rules = {
    all = {
      default = arch.core.rules.titleRule {
        products = [ "all" "integrates-(front|design)" "integrates[^-]" ];
        types = [ ];
      };
      noRotate = arch.core.rules.titleRule {
        products = [ "all" "integrates-(front|design)" "integrates" ];
        types = [ "feat" "fix" "refac" ];
      };
    };
  };
in {
  pipelines = {
    integratesFrontDefault = {
      gitlabPath = "/integrates/front/pipeline/default.yaml";
      jobs = [
        {
          output = "/pipelineOnGitlab/integratesFrontDefault";
          gitlabExtra = arch.extras.default // {
            rules = arch.rules.dev ++ [ rules.all.noRotate ];
            stage = arch.stages.test;
            tags = [ arch.tags.integrates ];
          };
        }
        {
          output = "/integrates/design lint";
          gitlabExtra = arch.extras.default // {
            rules = arch.rules.dev ++ [ rules.all.noRotate ];
            stage = arch.stages.test;
            tags = [ arch.tags.integrates ];
          };
        }
        {
          output = "/integrates/design deploy";
          gitlabExtra = arch.extras.default // {
            resource_group = "deploy/$CI_JOB_NAME";
            rules = arch.rules.prod ++ [ rules.all.noRotate ];
            stage = arch.stages.deploy;
            tags = [ arch.tags.integrates ];
          };
        }
        {
          output = "/integrates/front/deploy dev";
          gitlabExtra = arch.extras.default // {
            rules = arch.rules.dev ++ [ rules.all.default ];
            stage = arch.stages.deploy;
            tags = [ arch.tags.integrates ];
          };
        }
        {
          output = "/integrates/front/deploy prod";
          gitlabExtra = arch.extras.default // {
            resource_group = "deploy/$CI_JOB_NAME";
            rules = arch.rules.prod ++ [ rules.all.default ];
            stage = arch.stages.deploy;
            tags = [ arch.tags.integrates ];
          };
        }
        {
          output = "/integrates/front/i18n-unused-keys";
          gitlabExtra = arch.extras.default // {
            rules = arch.rules.dev ++ [ rules.all.noRotate ];
            stage = arch.stages.test;
            tags = [ arch.tags.integrates ];
          };
        }
        {
          output = "/integrates/front/unused-deps";
          gitlabExtra = arch.extras.default // {
            rules = arch.rules.dev ++ [ rules.all.noRotate ];
            stage = arch.stages.test;
            tags = [ arch.tags.integrates ];
          };
        }
        {
          output = "/integrates/front/lint";
          gitlabExtra = arch.extras.default // {
            cache = {
              key = "$CI_COMMIT_REF_NAME-integrates-front-lint";
              paths = [
                "integrates/front/.npm"
                "integrates/front/.stylelintcache"
                "integrates/front/tsconfig.tsbuildinfo"
              ];
            };
            rules = arch.rules.dev ++ [ rules.all.noRotate ];
            stage = arch.stages.test;
            tags = [ arch.tags.integrates ];
          };
        }
        {
          output = "/integrates/front/test";
          gitlabExtra = arch.extras.default // {
            artifacts = {
              paths = [ "integrates/front/.coverage/coverage-*.json" ];
              expire_in = "1 day";
            };
            cache = {
              key = "$CI_COMMIT_REF_NAME-integrates-front-test-$CI_NODE_INDEX";
              paths = [ "integrates/front/.jest" "integrates/front/.npm" ];
            };
            parallel = 3;
            rules = arch.rules.dev ++ [ rules.all.noRotate ];
            stage = arch.stages.test;
            tags = [ arch.tags.integrates ];
          };
        }
        {
          output = "/integrates/front/types";
          gitlabExtra = arch.extras.default // {
            rules = arch.rules.dev ++ [ rules.all.noRotate ];
            stage = arch.stages.test;
            tags = [ arch.tags.integrates ];
          };
        }
        {
          output = "/integrates/front/coverage";
          gitlabExtra = arch.extras.default // {
            needs = [ "/integrates/front/test" ];
            rules = arch.rules.dev ++ [ rules.all.noRotate ];
            stage = arch.stages.deploy;
            tags = [ arch.tags.integrates ];
            variables.GIT_DEPTH = 1000;
          };
        }
        {
          output = "/integrates/web/e2e/lint";
          gitlabExtra = arch.extras.default // {
            rules = arch.rules.dev ++ [ rules.all.noRotate ];
            stage = arch.stages.test;
            tags = [ arch.tags.integrates ];
          };
        }
        {
          output = "/integrates/web/e2e run";
          gitlabExtra = arch.extras.default // {
            artifacts = {
              paths = [
                "integrates/web/e2e/coverage/**"
                "integrates/web/e2e/cypress/screenshots/**"
              ];
              expire_in = "1 day";
              when = "always";
            };
            needs =
              [ "/integrates/back/deploy/dev" "/integrates/front/deploy dev" ];
            parallel = 10;
            rules = arch.rules.dev ++ [ rules.all.default ];
            stage = arch.stages.post-deploy;
            tags = [ arch.tags.integrates ];
          };
        }
        {
          output = "/integrates/machine/dast";
          gitlabExtra = arch.extras.default // {
            needs =
              [ "/integrates/back/deploy/dev" "/integrates/front/deploy dev" ];
            rules = arch.rules.dev ++ [ rules.all.noRotate ];
            script = [
              "m . /skims --strict scan https://$CI_COMMIT_REF_SLUG.app.fluidattacks.com"
            ];
            stage = arch.stages.post-deploy;
            tags = [ arch.tags.integrates ];
          };
        }
      ];
    };
  };
}
