/* eslint-disable functional/immutable-data */
import type { JestConfigWithTsJest } from "ts-jest";
import { createJsWithBabelPreset } from "ts-jest";

// Set environment variables
process.env.DEBUG_PRINT_LIMIT = "15000";
process.env.TZ = "UTC";

const presetConfig = createJsWithBabelPreset({ isolatedModules: true });

const esModules = [
  "@fluidattacks",
  "apollo-upload-client",
  "react-markdown",
].join("|");

const jestConfig: JestConfigWithTsJest = {
  ...presetConfig,
  bail: 1,
  cache: true,
  cacheDirectory: ".jest",
  collectCoverage: true,
  coverageDirectory: ".coverage",
  coverageReporters: [
    ["json", { file: `coverage-${process.env.CI_NODE_INDEX ?? 1}.json` }],
  ],
  globals: {
    FormData,
    Headers,
    Request,
    Response,
    fetch,
  },
  moduleDirectories: ["node_modules", "src"],
  moduleNameMapper: {
    "\\.(css|less)$": "identity-obj-proxy",
    "\\.(jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$":
      "<rootDir>/__mocks__/fileMock.js",
    "^@fluidattacks/design$":
      "<rootDir>/node_modules/@fluidattacks/design/dist/index.js",
    "use-resize-observer": "use-resize-observer/polyfilled",
  },
  setupFiles: ["jest-canvas-mock", "<rootDir>/jest.polyfills.js"],
  setupFilesAfterEnv: [
    "<rootDir>/jest-setup.ts",
    "<rootDir>/src/utils/validations.ts",
  ],
  testEnvironment: "jest-environment-jsdom",
  testEnvironmentOptions: {
    customExportConditions: [""],
  },
  transformIgnorePatterns: [`[/\\\\]node_modules[/\\\\](?!${esModules})/`],
  verbose: true,
};

/* eslint-disable import/no-default-export */
export default jestConfig;
