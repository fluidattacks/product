{ makeSearchPaths, makeTemplate, outputs, ... }: {
  dev.integratesStreams = {
    source = [
      outputs."/common/dev/global_deps"
      outputs."/integrates/streams/env"
      (makeSearchPaths { pythonPackage = [ "$PWD/integrates/streams" ]; })
    ];
  };
}
