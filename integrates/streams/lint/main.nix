{ makeScript, outputs, ... }:
makeScript {
  name = "integrates-streams-lint";
  entrypoint = ''
    pushd integrates/streams

    if test -n "''${CI:-}"; then
      ruff format --config ruff.toml --diff
      ruff check --config ruff.toml
    else
      ruff format --config ruff.toml
      ruff check --config ruff.toml --fix
    fi

    mypy --config-file mypy.ini streams
    mypy --config-file mypy.ini test
  '';
  searchPaths.source = [ outputs."/integrates/streams/env" ];
}
