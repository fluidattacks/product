# shellcheck shell=bash

function main {
  local test_group="${1}"

  : \
    && aws_login "dev" "3600" \
    && export COVERAGE_FILE=.coverage."${test_group}" \
    && export ENVIRONMENT="development" \
    && sops_export_vars __argSecretsDev__ \
      AZURE_OAUTH2_ISSUES_SECRET_DEV \
      BUGSNAG_API_KEY_STREAMS \
      GITLAB_ISSUES_OAUTH2_APP_ID \
      GITLAB_ISSUES_OAUTH2_SECRET \
      GOOGLE_CHAT_WEBHOOK_URL_DEV \
      CI_COMMIT_SHA \
      AWS_DYNAMODB_HOST_DEV \
      AWS_OPENSEARCH_HOST_DEV \
      WEBHOOK_POC_KEY_DEV \
      WEBHOOK_POC_ORG_DEV \
      WEBHOOK_POC_URL_DEV \
    && echo "[INFO] Running unit tests for: ${test_group}" \
    && pushd integrates/streams \
    && python -m fluidattacks_core.testing --no-cov --scope "${test_group}" \
    && popd \
    || return 1
}

main "${@}"
