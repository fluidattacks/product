from collections.abc import Generator

import boto3
from fluidattacks_core.testing import (
    injectable,
)
from fluidattacks_core.testing.fakers import (
    fake_finding,
    fake_group,
    fake_vulnerability,
)
from fluidattacks_core.testing.types import (
    DynamoDBResource,
    DynamoDBTable,
    FunctionFixture,
    Patch,
)

import streams.resource


@injectable(scope="function")
def prepare_data(
    patch_table: FunctionFixture[Patch],
) -> Generator[DynamoDBTable, None, None]:
    with patch_table(streams.resource, "TABLE_RESOURCE", "integrates_vms") as table:
        db_resource: DynamoDBResource = boto3.resource("dynamodb")
        table = db_resource.Table("integrates_vms")
        table.put_item(Item=fake_group("group1"))
        table.put_item(Item=fake_group("group2"))
        table.put_item(Item=fake_group("group3", "d3296134-2072-4d12-b41a-f4ca5bd54f27"))
        yield table


@injectable(scope="function")
def prepare_data_2(
    patch_table: FunctionFixture[Patch],
) -> Generator[DynamoDBTable, None, None]:
    with patch_table(streams.resource, "TABLE_RESOURCE", "integrates_vms") as table:
        table.put_item(Item=fake_finding())
        vuln = fake_vulnerability(
            vuln_id="fb454bdb-f339-4586-87bd-c63fe0a7ccf6",
            state="VULNERABLE",
            treatment_status="UNTREATED",
            bug_tracking_system_url="https://site.atlassian.net/IGTES-23",
            webhook_url="https://id.hello.atlassian-dev.net/x1/id",
        )
        table.put_item(Item=vuln)
        yield table


# Temporally added to conftest due to coverage limitations
def _flat_keys_and_values(raw_object: dict, prefix: str = "") -> dict:
    """
    Flat a nested dictionary into a single level dictionary.

    Example:
    {
        "a": {
            "b": {
                "c": "d"
            }
        }
    }
    will be transformed into:
    {
        "a.b.c": "d"
    }

    Args:
        raw_object (dict): Dict to flat
        prefix (str, optional): Value for recursion. Defaults to "".

    Returns:
        dict: Flat dictionary

    """
    flat_dict = {}
    for key, value in raw_object.items():
        to_set = f"{prefix}.{key}" if prefix else key
        if isinstance(value, dict):
            flat_dict.update(_flat_keys_and_values(value, to_set))
        else:
            flat_dict[to_set] = value
    return flat_dict


# Temporally added to conftest due to coverage limitations
def _list_dict_keywords(raw_object: dict) -> list:
    """
    Get all keywords from dict keys.

    Example:
    {
        "a": {
            "b": {
                "c": "d"
            }
        }
    }
    will be transformed into:
    ["a", "b", "c"]


    Args:
        raw_object (dict): _description_

    Returns:
        list: _description_

    """
    keywords = set(word for key in _flat_keys_and_values(raw_object) for word in key.split("."))
    return list(keywords)


# Temporally added to conftest due to coverage limitations
def format_expression_names(raw_object: dict) -> dict:
    """
    Get valid DynamoDB expression names from a dictionary.

    Example:
    { "id": "1" , "state": { "status": "active" } } ->
    { "#id": "id", "#state": "state", "#status": "status" }

    """
    keys_list = list(set(_list_dict_keywords(raw_object)))

    return {f"#{key}": f"{key}" for key in keys_list}


# Temporally added to conftest due to coverage limitations
def format_expression_values(raw_object: dict) -> dict:
    """
    Get valid DynamoDB expression values from a dictionary.

    Example:
    { "id": "1" , "state": { "status": "active" } } ->
    { ":id": "1", ":state_status": "active" }

    """
    dict_keys = _flat_keys_and_values(raw_object)
    return {f":{key.replace('.', '_')}": value for key, value in dict_keys.items()}


# Temporally added to conftest due to coverage limitations
def format_set_expression(raw_object: dict) -> str:
    """
    Returns a SET expression from dictionary for DynamoDB assuming
    formatted expression names and values using `format_expression_names`
    and `format_expression_values`.

    Example:
    { "id": "1" , "state": { "status": "active" } } ->
    "SET #id = :id,#state.#status = :state_status"

    Args:
        raw_object (dict): _description_

    Returns:
        str: _description_

    """
    dict_keys = _flat_keys_and_values(raw_object)
    return "SET " + ",".join(
        f"#{key.replace('.', '.#')} = :{key.replace('.', '_')}" for key in dict_keys
    )
