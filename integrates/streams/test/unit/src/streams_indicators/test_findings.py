import fluidattacks_core.testing as ftest
from fluidattacks_core.testing.constants import (
    FINDING_ID,
    GROUP_NAME,
)
from fluidattacks_core.testing.fakers import (
    fake_vulnerability,
)
from fluidattacks_core.testing.types import (
    DynamoDBTable,
)

from streams.indicators.findings import (
    treatment as finding_treatment,
)
from streams.indicators.findings import (
    vulnerabilities_summary as finding_vulnerabilities_summary,
)
from streams.indicators.operations import (
    get_finding,
)
from streams.indicators.operations import (
    get_released_nzr_vulns_by_finding as get_vulns,
)


@ftest.tag.streams_indicators
def test_inserted_vuln_updates_vulnerabilities_summary(
    prepare_data: DynamoDBTable,
) -> None:
    table = prepare_data
    table.put_item(Item=fake_vulnerability(vuln_id="1", state="SUBMITTED"))
    vulnerabilities = get_vulns(finding_id=f"FIN#{FINDING_ID}")
    finding = get_finding(pk=f"FIN#{FINDING_ID}", sk=f"GROUP#{GROUP_NAME}")

    new_indicators: dict = {}
    finding_vulnerabilities_summary.update_all(
        new_indicators=new_indicators,
        vulns=vulnerabilities,
        finding=finding,
    )

    assert new_indicators == {
        "submitted_vulnerabilities": 2,
        "rejected_vulnerabilities": 1,
        "vulnerabilities_summary": {
            "closed": 1,
            "open": 1,
            "submitted": 2,
            "rejected": 1,
            "open_critical": 0,
            "open_high": 0,
            "open_medium": 0,
            "open_low": 1,
            "open_critical_v3": 0,
            "open_high_v3": 0,
            "open_medium_v3": 0,
            "open_low_v3": 1,
        },
    }


@ftest.tag.streams_indicators
def test_inserted_vuln_updates_treatment_summary(
    prepare_data: DynamoDBTable,
) -> None:
    table = prepare_data
    table.put_item(
        Item=fake_vulnerability(vuln_id="1", state="VULNERABLE", treatment_status="IN_PROGRESS"),
    )
    table.put_item(
        Item=fake_vulnerability(vuln_id="2", state="VULNERABLE", treatment_status="ACCEPTED"),
    )
    vulnerabilities = get_vulns(finding_id=f"FIN#{FINDING_ID}")

    new_indicators: dict = {}
    finding_treatment.new_update_all(
        new_indicators=new_indicators,
        vulns=vulnerabilities,
    )

    assert new_indicators == {
        "treatment_summary": {
            "untreated": 1,
            "in_progress": 1,
            "accepted": 1,
            "accepted_undefined": 0,
        },
    }
