{ makeScript, outputs, projectPath, ... }:
makeScript {
  name = "streams-test";
  entrypoint = ./entrypoint.sh;
  replace = { __argSecretsDev__ = projectPath "/integrates/secrets/dev.yaml"; };
  searchPaths = {
    bin = [ outputs."/integrates/db" ];
    source = [
      outputs."/common/utils/aws"
      outputs."/common/utils/sops"
      outputs."/integrates/streams/env"
    ];
  };
}
