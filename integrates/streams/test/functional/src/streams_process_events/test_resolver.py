import time
from dataclasses import (
    dataclass,
)
from typing import (
    Any,
)

import pytest

from streams.search.handler import (
    process_events,
)
from test.functional.src.utils import (
    SearchParams,
    search,
)


@dataclass
class LambdaContext:
    def get_remaining_time_in_millis(self) -> int:
        return 0


@pytest.mark.resolver_test_group("streams_process_events")
@pytest.mark.parametrize(
    ["input_data", "output_data"],
    [
        [
            [
                {
                    "eventID": "71f4e326-fa33-412f-a2e3-ce32cd697af4",
                    "eventName": "MODIFY",
                    "eventVersion": "1.1",
                    "eventSource": "aws:dynamodb",
                    "awsRegion": "ddblocal",
                    "dynamodb": {
                        "ApproximateCreationDateTime": 1691164320000,
                        "Keys": {
                            "sk": {"S": "GROUP#group1"},
                            "pk": {"S": "EVENT#418900971"},
                        },
                        "NewImage": {
                            "group_name": {"S": "group1"},
                            "description": {"S": "ARM unit test"},
                            "unreliable_indicators": {"M": {}},
                            "type": {"S": "DATA_UPDATE_REQUIRED"},
                            "created_by": {"S": "unittest@fluidattacks.com"},
                            "hacker": {"S": "hacker@gmail.com"},
                            "event_date": {"S": "2018-06-27T12:00:00+00:00"},
                            "sk": {"S": "GROUP#group1"},
                            "client": {"S": "Fluid"},
                            "pk_2": {"S": "GROUP#group1"},
                            "created_date": {"S": "2018-06-27T12:00:00+00:00"},
                            "id": {"S": "418900971"},
                            "pk": {"S": "EVENT#418900971"},
                            "state": {
                                "M": {
                                    "reason": {"S": "TOE_CHANGE_APPROVED"},
                                    "modified_by": {"S": "hacker@gmail.com"},
                                    "modified_date": {"S": "2018-06-28T19:40:05+00:00"},
                                    "status": {"S": "SOLVED"},
                                },
                            },
                            "evidences": {
                                "M": {
                                    "file_1": {
                                        "M": {
                                            "file_name": {"S": ""},
                                            "modified_date": {"S": "2019-03-11T15:57:45Z"},
                                        },
                                    },
                                    "image_1": {
                                        "M": {
                                            "file_name": {"S": ""},
                                            "modified_date": {"S": "2019-03-11T15:57:45Z"},
                                        },
                                    },
                                },
                            },
                            "sk_2": {"S": "EVENT#SOLVED#true"},
                        },
                        "OldImage": {
                            "group_name": {"S": "group1"},
                            "description": {"S": "ARM unit test"},
                            "unreliable_indicators": {"M": {}},
                            "type": {"S": "OTHER"},
                            "created_by": {"S": "unittest@fluidattacks.com"},
                            "hacker": {"S": "hacker@gmail.com"},
                            "event_date": {"S": "2018-06-27T12:00:00+00:00"},
                            "sk": {"S": "GROUP#group1"},
                            "client": {"S": "Fluid"},
                            "pk_2": {"S": "GROUP#group1"},
                            "created_date": {"S": "2018-06-27T12:00:00+00:00"},
                            "id": {"S": "418900971"},
                            "pk": {"S": "EVENT#418900971"},
                            "state": {
                                "M": {
                                    "reason": {"S": "TOE_CHANGE_APPROVED"},
                                    "modified_by": {"S": "hacker@gmail.com"},
                                    "modified_date": {"S": "2018-06-28T19:40:05+00:00"},
                                    "status": {"S": "SOLVED"},
                                },
                            },
                            "evidences": {
                                "M": {
                                    "file_1": {
                                        "M": {
                                            "file_name": {"S": ""},
                                            "modified_date": {"S": "2019-03-11T15:57:45Z"},
                                        },
                                    },
                                    "image_1": {
                                        "M": {
                                            "file_name": {"S": ""},
                                            "modified_date": {"S": "2019-03-11T15:57:45Z"},
                                        },
                                    },
                                },
                            },
                            "sk_2": {"S": "EVENT#SOLVED#true"},
                        },
                        "SequenceNumber": "000000000000000000299",
                        "SizeBytes": 1120,
                        "StreamViewType": "NEW_AND_OLD_IMAGES",
                    },
                },
                {
                    "eventID": "35216650-65f3-4c65-8d13-5f9491c4913d",
                    "eventName": "MODIFY",
                    "eventVersion": "1.1",
                    "eventSource": "aws:dynamodb",
                    "awsRegion": "ddblocal",
                    "dynamodb": {
                        "ApproximateCreationDateTime": 1691164260000,
                        "Keys": {
                            "sk": {"S": "GROUP#group1"},
                            "pk": {"S": "EVENT#418900972"},
                        },
                        "NewImage": {
                            "group_name": {"S": "group1"},
                            "description": {"S": "ARM unit test"},
                            "unreliable_indicators": {"M": {}},
                            "type": {"S": "OTHER"},
                            "created_by": {"S": "unittest@fluidattacks.com"},
                            "hacker": {"S": "hacker@gmail.com"},
                            "event_date": {"S": "2018-06-27T12:00:00+00:00"},
                            "sk": {"S": "GROUP#group1"},
                            "client": {"S": "Fluid"},
                            "pk_2": {"S": "GROUP#group1"},
                            "created_date": {"S": "2018-06-27T12:00:00+00:00"},
                            "id": {"S": "418900972"},
                            "pk": {"S": "EVENT#418900972"},
                            "state": {
                                "M": {
                                    "reason": {"S": "TOE_WILL_REMAIN_UNCHANGED"},
                                    "modified_by": {"S": "hacker@gmail.com"},
                                    "modified_date": {"S": "2018-06-28T19:40:05+00:00"},
                                    "status": {"S": "SOLVED"},
                                },
                            },
                            "evidences": {
                                "M": {
                                    "file_1": {
                                        "M": {
                                            "file_name": {"S": ""},
                                            "modified_date": {"S": "2019-03-11T15:57:45Z"},
                                        },
                                    },
                                    "image_1": {
                                        "M": {
                                            "file_name": {"S": ""},
                                            "modified_date": {"S": "2019-03-11T15:57:45Z"},
                                        },
                                    },
                                },
                            },
                            "sk_2": {"S": "EVENT#SOLVED#true"},
                        },
                        "OldImage": {
                            "group_name": {"S": "group1"},
                            "description": {"S": "ARM unit test"},
                            "unreliable_indicators": {"M": {}},
                            "type": {"S": "OTHER"},
                            "created_by": {"S": "unittest@fluidattacks.com"},
                            "hacker": {"S": "hacker@gmail.com"},
                            "event_date": {"S": "2018-06-27T12:00:00+00:00"},
                            "sk": {"S": "GROUP#group1"},
                            "client": {"S": "Fluid"},
                            "pk_2": {"S": "GROUP#group1"},
                            "created_date": {"S": "2018-06-27T12:00:00+00:00"},
                            "id": {"S": "418900972"},
                            "pk": {"S": "EVENT#418900972"},
                            "state": {
                                "M": {
                                    "modified_by": {"S": "hacker@gmail.com"},
                                    "modified_date": {"S": "2018-06-27T19:40:05+00:00"},
                                    "status": {"S": "CREATED"},
                                },
                            },
                            "evidences": {
                                "M": {
                                    "file_1": {
                                        "M": {
                                            "file_name": {"S": ""},
                                            "modified_date": {"S": "2019-03-11T15:57:45Z"},
                                        },
                                    },
                                    "image_1": {
                                        "M": {
                                            "file_name": {"S": ""},
                                            "modified_date": {"S": "2019-03-11T15:57:45Z"},
                                        },
                                    },
                                },
                            },
                            "sk_2": {"S": "EVENT#SOLVED#false"},
                        },
                        "SequenceNumber": "000000000000000000098",
                        "SizeBytes": 1087,
                        "StreamViewType": "NEW_AND_OLD_IMAGES",
                    },
                },
            ],
            [
                {
                    "id": "418900972",
                    "pk": "EVENT#418900972",
                    "sk": "GROUP#group1",
                },
                {
                    "id": "418900971",
                    "pk": "EVENT#418900971",
                    "sk": "GROUP#group1",
                },
            ],
        ],
    ],
)
def test_streams_process_events(
    input_data: list[dict[str, Any]],
    output_data: list[dict[str, Any]],
) -> None:
    search_result = search(SearchParams(index_value="events_index", limit=10))

    assert search_result.total == 0

    process_events({"Records": input_data}, LambdaContext())
    time.sleep(5)

    search_result = search(SearchParams(index_value="events_index", limit=10))

    assert search_result.total == 2

    items = search_result.items

    for idx, item in enumerate(items):
        assert item["pk"] == output_data[idx]["pk"]
        assert item["sk"] == output_data[idx]["sk"]
        assert item["id"] == output_data[idx]["id"]
