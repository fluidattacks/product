import time
from dataclasses import (
    dataclass,
)
from typing import (
    Any,
)

import pytest

from streams.search.handler import (
    process_roots,
)
from test.functional.src.utils import (
    SearchParams,
    search,
)


@dataclass
class LambdaContext:
    def get_remaining_time_in_millis(self) -> int:
        return 0


@pytest.mark.resolver_test_group("streams_process_roots")
@pytest.mark.parametrize(
    ["input_data", "output_data"],
    [
        [
            [
                {
                    "eventID": "75b75ead-98d5-4f13-ba88-4f37e6911dcb",
                    "eventName": "MODIFY",
                    "eventVersion": "1.1",
                    "eventSource": "aws:dynamodb",
                    "awsRegion": "ddblocal",
                    "dynamodb": {
                        "ApproximateCreationDateTime": 1713373500000,
                        "Keys": {
                            "sk": {"S": "GROUP#unittesting"},
                            "pk": {"S": "ROOT#4039d098-ffc5-4984-8ed3-eb17bca98e19"},
                        },
                        "NewImage": {
                            "sk": {"S": "GROUP#unittesting"},
                            "pk_2": {"S": "ORG#okada"},
                            "created_date": {"S": "2020-11-19T13:37:10+00:00"},
                            "pk": {"S": "ROOT#4039d098-ffc5-4984-8ed3-eb17bca98e19"},
                            "state": {
                                "M": {
                                    "use_egress": {"BOOL": False},
                                    "gitignore": {
                                        "L": [
                                            {"S": "bower_components/*"},
                                            {"S": "node_modules/*"},
                                        ],
                                    },
                                    "use_ztna": {"BOOL": False},
                                    "criticality": {"S": "LOW"},
                                    "modified_date": {"S": "2024-04-17T17:05:59.194562+00:00"},
                                    "branch": {"S": "master"},
                                    "url": {"S": "https://gitlab.com/fluidattacks/universe.git"},
                                    "use_vpn": {"BOOL": False},
                                    "environment": {"S": "production"},
                                    "includes_health_check": {"BOOL": True},
                                    "modified_by": {"S": "aaguirre@fluidattacks.com"},
                                    "nickname": {"S": "universe"},
                                    "status": {"S": "ACTIVE"},
                                    "credential_id": {
                                        "S": "0df32c4a-526d-4879-8dc8-9ae191d2721b",
                                    },
                                },
                            },
                            "unreliable_indicators": {
                                "M": {
                                    "unreliable_last_status_update": {
                                        "S": "2020-11-19T13:37:10+00:00",
                                    },
                                },
                            },
                            "type": {"S": "Git"},
                            "created_by": {"S": "jdoe@fluidattacks.com"},
                            "cloning": {
                                "M": {
                                    "commit_date": {"S": "2022-02-15T18:45:06.493253+00:00"},
                                    "reason": {"S": "root OK"},
                                    "commit": {"S": "5b5c92105b5c92105b5c92105b5c92105b5c9210"},
                                    "modified_by": {"S": "integratesmanager@fluidattacks.com"},
                                    "modified_date": {"S": "2020-11-19T13:39:10+00:00"},
                                    "status": {"S": "OK"},
                                },
                            },
                            "sk_2": {"S": "ROOT#4039d098-ffc5-4984-8ed3-eb17bca98e19"},
                        },
                        "OldImage": {
                            "sk": {"S": "GROUP#unittesting"},
                            "pk_2": {"S": "ORG#okada"},
                            "created_date": {"S": "2020-11-19T13:37:10+00:00"},
                            "pk": {"S": "ROOT#4039d098-ffc5-4984-8ed3-eb17bca98e19"},
                            "state": {
                                "M": {
                                    "environment": {"S": "production"},
                                    "gitignore": {
                                        "L": [
                                            {"S": "bower_components/*"},
                                            {"S": "node_modules/*"},
                                        ],
                                    },
                                    "includes_health_check": {"BOOL": True},
                                    "criticality": {"S": "MEDIUM"},
                                    "modified_by": {"S": "jdoe@fluidattacks.com"},
                                    "nickname": {"S": "universe"},
                                    "environment_urls": {
                                        "L": [
                                            {"S": "https://app.fluidattacks.com"},
                                            {"S": "https://test.com"},
                                        ],
                                    },
                                    "modified_date": {"S": "2020-11-19T13:37:10+00:00"},
                                    "branch": {"S": "master"},
                                    "url": {"S": "https://gitlab.com/fluidattacks/universe.git"},
                                    "credential_id": {
                                        "S": "0df32c4a-526d-4879-8dc8-9ae191d2721b",
                                    },
                                    "status": {"S": "ACTIVE"},
                                },
                            },
                            "unreliable_indicators": {
                                "M": {
                                    "unreliable_last_status_update": {
                                        "S": "2020-11-19T13:37:10+00:00",
                                    },
                                },
                            },
                            "type": {"S": "Git"},
                            "created_by": {"S": "jdoe@fluidattacks.com"},
                            "cloning": {
                                "M": {
                                    "commit_date": {"S": "2022-02-15T18:45:06.493253+00:00"},
                                    "reason": {"S": "root OK"},
                                    "commit": {"S": "5b5c92105b5c92105b5c92105b5c92105b5c9210"},
                                    "modified_by": {"S": "integratesmanager@fluidattacks.com"},
                                    "modified_date": {"S": "2020-11-19T13:39:10+00:00"},
                                    "status": {"S": "OK"},
                                },
                            },
                            "sk_2": {"S": "ROOT#4039d098-ffc5-4984-8ed3-eb17bca98e19"},
                        },
                        "SequenceNumber": "000000000000000001445",
                        "SizeBytes": 1795,
                        "StreamViewType": "NEW_AND_OLD_IMAGES",
                    },
                },
                {
                    "eventID": "7e7a6a92-04e2-46fb-90c9-6a6a3368026b",
                    "eventName": "MODIFY",
                    "eventVersion": "1.1",
                    "eventSource": "aws:dynamodb",
                    "awsRegion": "ddblocal",
                    "dynamodb": {
                        "ApproximateCreationDateTime": 1713373740000,
                        "Keys": {
                            "sk": {"S": "GROUP#unittesting"},
                            "pk": {"S": "ROOT#765b1d0f-b6fb-4485-b4e2-2c2cb1555b1a"},
                        },
                        "NewImage": {
                            "sk": {"S": "GROUP#unittesting"},
                            "pk_2": {"S": "ORG#okada"},
                            "created_date": {"S": "2020-11-19T13:39:56+00:00"},
                            "pk": {"S": "ROOT#765b1d0f-b6fb-4485-b4e2-2c2cb1555b1a"},
                            "state": {
                                "M": {
                                    "use_egress": {"BOOL": False},
                                    "gitignore": {"L": []},
                                    "use_ztna": {"BOOL": False},
                                    "criticality": {"S": "HIGH"},
                                    "modified_date": {"S": "2024-04-17T17:09:02.697815+00:00"},
                                    "branch": {"S": "develop"},
                                    "url": {"S": "https://gitlab.com/fluidattacks/integrates"},
                                    "use_vpn": {"BOOL": False},
                                    "environment": {"S": "local"},
                                    "includes_health_check": {"BOOL": False},
                                    "modified_by": {"S": "aaguirre@fluidattacks.com"},
                                    "nickname": {"S": "integrates_1"},
                                    "status": {"S": "ACTIVE"},
                                },
                            },
                            "unreliable_indicators": {
                                "M": {
                                    "unreliable_last_status_update": {
                                        "S": "2020-11-19T13:39:56+00:00",
                                    },
                                },
                            },
                            "type": {"S": "Git"},
                            "created_by": {"S": "jdoe@fluidattacks.com"},
                            "cloning": {
                                "M": {
                                    "commit_date": {"S": "2022-02-15T18:45:06.493253+00:00"},
                                    "reason": {"S": "root creation"},
                                    "commit": {"S": "cdd48a681aa96082b3095dc06fb1b15ec4b5ea7b"},
                                    "modified_by": {"S": "jdoe@fluidattacks.com"},
                                    "modified_date": {"S": "2020-11-19T13:37:10+00:00"},
                                    "status": {"S": "UNKNOWN"},
                                },
                            },
                            "sk_2": {"S": "ROOT#765b1d0f-b6fb-4485-b4e2-2c2cb1555b1a"},
                        },
                        "OldImage": {
                            "sk": {"S": "GROUP#unittesting"},
                            "pk_2": {"S": "ORG#okada"},
                            "created_date": {"S": "2020-11-19T13:39:56+00:00"},
                            "pk": {"S": "ROOT#765b1d0f-b6fb-4485-b4e2-2c2cb1555b1a"},
                            "state": {
                                "M": {
                                    "environment": {"S": "QA"},
                                    "gitignore": {"L": []},
                                    "includes_health_check": {"BOOL": False},
                                    "modified_by": {"S": "jdoe@fluidattacks.com"},
                                    "nickname": {"S": "integrates_1"},
                                    "environment_urls": {"L": []},
                                    "modified_date": {"S": "2020-11-19T13:39:56+00:00"},
                                    "branch": {"S": "develop"},
                                    "url": {"S": "https://gitlab.com/fluidattacks/integrates"},
                                    "status": {"S": "ACTIVE"},
                                },
                            },
                            "unreliable_indicators": {
                                "M": {
                                    "unreliable_last_status_update": {
                                        "S": "2020-11-19T13:39:56+00:00",
                                    },
                                },
                            },
                            "type": {"S": "Git"},
                            "created_by": {"S": "jdoe@fluidattacks.com"},
                            "cloning": {
                                "M": {
                                    "commit_date": {"S": "2022-02-15T18:45:06.493253+00:00"},
                                    "reason": {"S": "root creation"},
                                    "commit": {"S": "cdd48a681aa96082b3095dc06fb1b15ec4b5ea7b"},
                                    "modified_by": {"S": "jdoe@fluidattacks.com"},
                                    "modified_date": {"S": "2020-11-19T13:37:10+00:00"},
                                    "status": {"S": "UNKNOWN"},
                                },
                            },
                            "sk_2": {"S": "ROOT#765b1d0f-b6fb-4485-b4e2-2c2cb1555b1a"},
                        },
                        "SequenceNumber": "000000000000000001516",
                        "SizeBytes": 1553,
                        "StreamViewType": "NEW_AND_OLD_IMAGES",
                    },
                },
            ],
            [
                {
                    "pk": "ROOT#765b1d0f-b6fb-4485-b4e2-2c2cb1555b1a",
                    "sk": "GROUP#unittesting",
                },
                {
                    "pk": "ROOT#4039d098-ffc5-4984-8ed3-eb17bca98e19",
                    "sk": "GROUP#unittesting",
                },
            ],
        ],
    ],
)
def test_streams_process_roots(
    input_data: list[dict[str, Any]],
    output_data: list[dict[str, Any]],
) -> None:
    search_result = search(SearchParams(index_value="roots_index", limit=10))

    assert search_result.total == 0

    process_roots({"Records": input_data}, LambdaContext())
    time.sleep(5)

    search_result = search(SearchParams(index_value="roots_index", limit=10))

    assert search_result.total == 2

    items = search_result.items

    for idx, item in enumerate(items):
        assert item["pk"] == output_data[idx]["pk"]
        assert item["sk"] == output_data[idx]["sk"]
