import time
from dataclasses import dataclass
from typing import Any

import pytest

from streams.search.handler import process_findings
from test.functional.src.utils import SearchParams, search


@dataclass
class LambdaContext:
    def get_remaining_time_in_millis(self) -> int:
        return 0


@pytest.mark.resolver_test_group("streams_process_findings")
@pytest.mark.parametrize(
    ["input_data", "output_data"],
    [
        [
            [
                {
                    "eventID": "e5e467d4-ce08-4494-aa2a-a6b47b8bd994",
                    "eventName": "INSERT",
                    "eventVersion": "1.1",
                    "eventSource": "aws:dynamodb",
                    "awsRegion": "ddblocal",
                    "dynamodb": {
                        "ApproximateCreationDateTime": 1691598600000,
                        "Keys": {
                            "sk": {"S": "GROUP#group1"},
                            "pk": {"S": "FIN#55054177-6183-4c53-b44c-f8445da39000"},
                        },
                        "NewImage": {
                            "requirements": {"S": ""},
                            "group_name": {"S": "group1"},
                            "unfulfilled_requirements": {"L": [{"S": "158"}]},
                            "description": {"S": "This is an attack vector"},
                            "recommendation": {"S": "Recommendation"},
                            "unreliable_indicators": {
                                "M": {
                                    "unreliable_where": {"S": ""},
                                    "verification_summary": {
                                        "M": {
                                            "requested": {"N": "0"},
                                            "verified": {"N": "0"},
                                            "on_hold": {"N": "0"},
                                        },
                                    },
                                    "max_open_severity_score": {"N": "0.0"},
                                    "unreliable_status": {"S": "DRAFT"},
                                    "unreliable_total_open_cvssf": {"N": "0.0"},
                                },
                            },
                            "title": {
                                "S": "366. Inappropriate coding practices - "
                                + "Transparency Conflict",
                            },
                            "cvss_version": {"S": "3.1"},
                            "sk": {"S": "GROUP#group1"},
                            "pk": {"S": "FIN#55054177-6183-4c53-b44c-f8445da39000"},
                            "id": {"S": "55054177-6183-4c53-b44c-f8445da39000"},
                            "state": {
                                "M": {
                                    "modified_by": {"S": "admin@gmail.com"},
                                    "justification": {"S": "NO_JUSTIFICATION"},
                                    "source": {"S": "ASM"},
                                    "modified_date": {"S": "2023-08-09T16:30:17.897634+00:00"},
                                    "status": {"S": "CREATED"},
                                },
                            },
                            "threat": {"S": "Threat test"},
                            "evidences": {"M": {}},
                            "min_time_to_remediate": {"N": "18"},
                            "sorts": {"S": "NO"},
                            "attack_vector_description": {"S": "This is an attack vector"},
                            "creation": {
                                "M": {
                                    "modified_by": {"S": "admin@gmail.com"},
                                    "justification": {"S": "NO_JUSTIFICATION"},
                                    "source": {"S": "ASM"},
                                    "modified_date": {"S": "2023-08-09T16:30:17.897634+00:00"},
                                    "status": {"S": "CREATED"},
                                },
                            },
                            "severity_score": {
                                "M": {
                                    "base_score": {"N": "6.3"},
                                    "temporal_score": {"N": "5.7"},
                                    "cvss_v3": {
                                        "S": "CVSS:3.1/AV:N/AC:L/PR:L/UI:N"
                                        + "/S:U/C:L/I:L/A:L/E:P/RL:O/RC:C",
                                    },
                                    "threat_score": {"N": "2.1"},
                                    "cvss_v4": {
                                        "S": "CVSS:4.0/AV:N/AC:L/AT:N/PR:L/UI:"
                                        + "N/VC:L/VI:L/VA:L/SC:N/SI:N/SA:N/E:P",
                                    },
                                    "cvssf": {"N": "10.556"},
                                    "cvssf_v4": {"N": "0.072"},
                                },
                            },
                        },
                        "SequenceNumber": "000000000000000000240",
                        "SizeBytes": 1280,
                        "StreamViewType": "NEW_AND_OLD_IMAGES",
                    },
                },
                {
                    "eventID": "32d63951-53ef-4a86-8bc1-c3b06d36741e",
                    "eventName": "INSERT",
                    "eventVersion": "1.1",
                    "eventSource": "aws:dynamodb",
                    "awsRegion": "ddblocal",
                    "dynamodb": {
                        "ApproximateCreationDateTime": 1691598600000,
                        "Keys": {
                            "sk": {"S": "GROUP#group1"},
                            "pk": {"S": "FIN#8d4ed65f-b13d-464c-b6a0-49ec87a78cf9"},
                        },
                        "NewImage": {
                            "requirements": {"S": ""},
                            "group_name": {"S": "group1"},
                            "unfulfilled_requirements": {"L": [{"S": "158"}]},
                            "description": {"S": "This is an attack vector 2"},
                            "recommendation": {"S": "Recommendation"},
                            "unreliable_indicators": {
                                "M": {
                                    "unreliable_where": {"S": ""},
                                    "verification_summary": {
                                        "M": {
                                            "requested": {"N": "0"},
                                            "verified": {"N": "0"},
                                            "on_hold": {"N": "0"},
                                        },
                                    },
                                    "max_open_severity_score": {"N": "0.0"},
                                    "unreliable_status": {"S": "DRAFT"},
                                    "unreliable_total_open_cvssf": {"N": "0.0"},
                                    "unreliable_total_open_priority": {"N": "0.0"},
                                },
                            },
                            "title": {
                                "S": "366. Inappropriate coding practices - "
                                + "Transparency Conflict",
                            },
                            "cvss_version": {"S": "3.1"},
                            "sk": {"S": "GROUP#group1"},
                            "pk": {"S": "FIN#8d4ed65f-b13d-464c-b6a0-49ec87a78cf9"},
                            "id": {"S": "8d4ed65f-b13d-464c-b6a0-49ec87a78cf9"},
                            "state": {
                                "M": {
                                    "modified_by": {"S": "admin@gmail.com"},
                                    "justification": {"S": "NO_JUSTIFICATION"},
                                    "source": {"S": "ASM"},
                                    "modified_date": {"S": "2023-08-09T16:30:27.357440+00:00"},
                                    "status": {"S": "CREATED"},
                                },
                            },
                            "threat": {"S": "Threat test 2"},
                            "evidences": {"M": {}},
                            "min_time_to_remediate": {"N": "18"},
                            "sorts": {"S": "NO"},
                            "attack_vector_description": {"S": "This is an attack vector"},
                            "creation": {
                                "M": {
                                    "modified_by": {"S": "admin@gmail.com"},
                                    "justification": {"S": "NO_JUSTIFICATION"},
                                    "source": {"S": "ASM"},
                                    "modified_date": {"S": "2023-08-09T16:30:27.357440+00:00"},
                                    "status": {"S": "CREATED"},
                                },
                            },
                            "severity_score": {
                                "M": {
                                    "base_score": {"N": "5.0"},
                                    "temporal_score": {"N": "4.5"},
                                    "cvss_v3": {
                                        "S": "CVSS:3.1/AV:N/AC:H/PR:L/UI:N"
                                        + "/S:U/C:L/I:L/A:L/E:P/RL:O/RC:C",
                                    },
                                    "cvssf": {"N": "2.000"},
                                    "threat_score": {"N": "2.1"},
                                    "cvss_v4": {
                                        "S": "CVSS:4.0/AV:N/AC:L/AT:N/PR:L/UI:"
                                        + "N/VC:L/VI:L/VA:L/SC:N/SI:N/SA:N/E:P",
                                    },
                                    "cvssf_v4": {"N": "0.072"},
                                },
                            },
                        },
                        "SequenceNumber": "000000000000000000249",
                        "SizeBytes": 1281,
                        "StreamViewType": "NEW_AND_OLD_IMAGES",
                    },
                },
            ],
            [
                {
                    "id": "8d4ed65f-b13d-464c-b6a0-49ec87a78cf9",
                    "pk": "FIN#8d4ed65f-b13d-464c-b6a0-49ec87a78cf9",
                    "sk": "GROUP#group1",
                },
                {
                    "id": "55054177-6183-4c53-b44c-f8445da39000",
                    "pk": "FIN#55054177-6183-4c53-b44c-f8445da39000",
                    "sk": "GROUP#group1",
                },
            ],
        ],
    ],
)
def test_streams_process_findings(
    input_data: list[dict[str, Any]],
    output_data: list[dict[str, Any]],
) -> None:
    search_result = search(SearchParams(index_value="findings_index", limit=10))

    assert search_result.total == 0

    process_findings({"Records": input_data}, LambdaContext())
    time.sleep(5)

    search_result = search(SearchParams(index_value="findings_index", limit=10))

    assert search_result.total == 2

    items = search_result.items

    for idx, item in enumerate(items):
        assert item["id"] == output_data[idx]["id"]
        assert item["pk"] == output_data[idx]["pk"]
        assert item["sk"] == output_data[idx]["sk"]
