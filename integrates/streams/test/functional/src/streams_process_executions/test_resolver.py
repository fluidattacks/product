import time
from dataclasses import (
    dataclass,
)
from typing import (
    Any,
)

import pytest

from streams.search.handler import (
    process_executions,
)
from test.functional.src.utils import (
    SearchParams,
    search,
)


@dataclass
class LambdaContext:
    def get_remaining_time_in_millis(self) -> int:
        return 0


@pytest.mark.resolver_test_group("streams_process_executions")
@pytest.mark.parametrize(
    ["input_data", "output_data"],
    [
        [
            [
                {
                    "eventID": "14199c76-e592-4dbb-a04c-d3beb1cab664",
                    "eventName": "INSERT",
                    "eventVersion": "1.1",
                    "eventSource": "aws:dynamodb",
                    "awsRegion": "ddblocal",
                    "dynamodb": {
                        "ApproximateCreationDateTime": 1691508420000,
                        "Keys": {
                            "sk": {"S": "GROUP#group1"},
                            "pk": {"S": "EXEC#18c1e735a73243f2ab1ee0757041f80e"},
                        },
                        "NewImage": {
                            "group_name": {"S": "group1"},
                            "execution_date": {"S": "2020-02-20T00:00:00+00:00"},
                            "kind": {"S": "dynamic"},
                            "origin": {"S": "http://origin-test.com"},
                            "repo": {"S": "Repository"},
                            "commit": {"S": "2e7b34c1358db2ff4123c3c76e7fe3bf9f2838f2"},
                            "branch": {"S": "master"},
                            "grace_period": {"N": "0"},
                            "sk": {"S": "GROUP#group1"},
                            "exit_code": {"S": "1"},
                            "vulnerabilities": {
                                "M": {
                                    "num_of_accepted_vulnerabilities": {"N": "1"},
                                    "num_of_open_vulnerabilities": {"N": "1"},
                                    "num_of_closed_vulnerabilities": {"N": "1"},
                                },
                            },
                            "strictness": {"S": "strict"},
                            "pk_2": {"S": "GROUP#group1"},
                            "pk": {"S": "EXEC#18c1e735a73243f2ab1ee0757041f80e"},
                            "id": {"S": "18c1e735a73243f2ab1ee0757041f80e"},
                            "sk_2": {"S": "EXEC#2020-02-20T00:00:00+00:00"},
                            "severity_threshold": {"N": "0"},
                        },
                        "SequenceNumber": "000000000000000000278",
                        "SizeBytes": 528,
                        "StreamViewType": "NEW_AND_OLD_IMAGES",
                    },
                },
            ],
            [
                {
                    "group_name": "group1",
                    "id": "18c1e735a73243f2ab1ee0757041f80e",
                    "pk": "EXEC#18c1e735a73243f2ab1ee0757041f80e",
                    "sk": "GROUP#group1",
                },
            ],
        ],
    ],
)
def test_streams_process_executions(
    input_data: list[dict[str, Any]],
    output_data: list[dict[str, Any]],
) -> None:
    search_result = search(SearchParams(index_value="executions_index", limit=10))

    assert search_result.total == 0

    process_executions({"Records": input_data}, LambdaContext())
    time.sleep(5)

    search_result = search(SearchParams(index_value="executions_index", limit=10))

    assert search_result.total == 1

    items = search_result.items

    for idx, item in enumerate(items):
        assert item["id"] == output_data[idx]["id"]
        assert item["group_name"] == output_data[idx]["group_name"]
        assert item["pk"] == output_data[idx]["pk"]
        assert item["sk"] == output_data[idx]["sk"]
