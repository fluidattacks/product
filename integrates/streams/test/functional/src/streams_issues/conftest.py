from datetime import (
    datetime,
)

import pytest

import streams.resource as resources


@pytest.fixture(autouse=True)
def mock_data_for_module() -> None:
    group_name = "test"
    resources.TABLE_RESOURCE.put_item(
        Item={
            "id": group_name,
            "pk": f"GROUP#{group_name}",
            "sk": "ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef1",
            "name": group_name,
            "description": f"{group_name} description",
            "language": "EN",
            "created_date": datetime.now().isoformat(),
            "state": {
                "managed": "MANAGED",
                "status": "ACTIVE",
            },
        },
    )
