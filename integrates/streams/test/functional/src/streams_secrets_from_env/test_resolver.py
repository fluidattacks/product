import pytest
from pytest import (
    MonkeyPatch,
)

from streams.utils import (
    def_secret,
)

pytestmark = pytest.mark.asyncio


@pytest.mark.resolver_test_group("streams_secrets_from_env")
@pytest.mark.parametrize(
    "prod_secret, dev_secret, output_data",
    [
        ("prod_value", "dev_value", "prod_value"),
        ("null", "dev_value", "dev_value"),
        ("prod_value", "null", "prod_value"),
    ],
)
def test_secrets_from_env(
    monkeypatch: MonkeyPatch,
    prod_secret: str,
    dev_secret: str,
    output_data: str,
) -> None:
    monkeypatch.setenv("prod_secret", prod_secret)
    monkeypatch.setenv("dev_secret", dev_secret)

    secret = def_secret("prod_secret", "dev_secret")
    assert secret == output_data


@pytest.mark.resolver_test_group("streams_secrets_from_env")
def test_def_secret_no_env_set() -> None:
    with pytest.raises(ValueError):
        def_secret("prod_secret", "dev_secret")
