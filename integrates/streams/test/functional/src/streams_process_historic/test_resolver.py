import time
from dataclasses import (
    dataclass,
)
from decimal import (
    Decimal,
)
from typing import (
    Any,
)

import pytest
from freezegun import (
    freeze_time,
)

from streams.historic.handler import (
    get_entity_historic,
    process_historic,
)


@dataclass
class LambdaContext:
    def get_remaining_time_in_millis(self) -> int:
        return 0


@pytest.mark.resolver_test_group("streams_process_historic")
@freeze_time("2022-11-11T15:58:31.280182")
@pytest.mark.parametrize(
    ["input_data"],
    [
        [
            [
                {
                    "eventID": "35216650-65f3-4c65-8d13-5f9491c4913d",
                    "eventName": "MODIFY",
                    "eventVersion": "1.1",
                    "eventSource": "aws:dynamodb",
                    "awsRegion": "ddblocal",
                    "dynamodb": {
                        "ApproximateCreationDateTime": 1691164260000,
                        "Keys": {
                            "sk": {"S": "GROUP#group1"},
                            "pk": {"S": "EVENT#418900972"},
                        },
                        "NewImage": {
                            "group_name": {"S": "group1"},
                            "description": {"S": "ARM unit test"},
                            "unreliable_indicators": {"M": {}},
                            "type": {"S": "OTHER"},
                            "created_by": {"S": "unittest@fluidattacks.com"},
                            "hacker": {"S": "hacker@gmail.com"},
                            "event_date": {"S": "2018-06-27T12:00:00+00:00"},
                            "sk": {"S": "GROUP#group1"},
                            "client": {"S": "Fluid"},
                            "pk_2": {"S": "GROUP#group1"},
                            "created_date": {"S": "2018-06-27T12:00:00+00:00"},
                            "id": {"S": "418900972"},
                            "pk": {"S": "EVENT#418900972"},
                            "state": {
                                "M": {
                                    "reason": {"S": "TOE_WILL_REMAIN_UNCHANGED"},
                                    "modified_by": {"S": "hacker@gmail.com"},
                                    "modified_date": {"S": "2018-06-28T19:40:05+00:00"},
                                    "status": {"S": "SOLVED"},
                                    "test_decimal": {"N": "3.9"},
                                    "test_set": {
                                        "SS": [
                                            "15375781-31f2-4953-ac77-" + "f31134225748",
                                        ],
                                    },
                                },
                            },
                            "evidences": {
                                "M": {
                                    "file_1": {
                                        "M": {
                                            "file_name": {"S": ""},
                                            "modified_date": {"S": "2019-03-11T15:57:45Z"},
                                        },
                                    },
                                    "image_1": {
                                        "M": {
                                            "file_name": {"S": ""},
                                            "modified_date": {"S": "2019-03-11T15:57:45Z"},
                                        },
                                    },
                                },
                            },
                            "sk_2": {"S": "EVENT#SOLVED#true"},
                        },
                        "OldImage": {
                            "group_name": {"S": "group1"},
                            "description": {"S": "ARM unit test"},
                            "unreliable_indicators": {"M": {}},
                            "type": {"S": "OTHER"},
                            "created_by": {"S": "unittest@fluidattacks.com"},
                            "hacker": {"S": "hacker@gmail.com"},
                            "event_date": {"S": "2018-06-27T12:00:00+00:00"},
                            "sk": {"S": "GROUP#group1"},
                            "client": {"S": "Fluid"},
                            "pk_2": {"S": "GROUP#group1"},
                            "created_date": {"S": "2018-06-27T12:00:00+00:00"},
                            "id": {"S": "418900972"},
                            "pk": {"S": "EVENT#418900972"},
                            "state": {
                                "M": {
                                    "modified_by": {"S": "hacker@gmail.com"},
                                    "modified_date": {"S": "2018-06-27T19:40:05+00:00"},
                                    "status": {"S": "CREATED"},
                                },
                            },
                            "evidences": {
                                "M": {
                                    "file_1": {
                                        "M": {
                                            "file_name": {"S": ""},
                                            "modified_date": {"S": "2019-03-11T15:57:45Z"},
                                        },
                                    },
                                    "image_1": {
                                        "M": {
                                            "file_name": {"S": ""},
                                            "modified_date": {"S": "2019-03-11T15:57:45Z"},
                                        },
                                    },
                                },
                            },
                            "sk_2": {"S": "EVENT#SOLVED#false"},
                        },
                        "SequenceNumber": "000000000000000000098",
                        "SizeBytes": 1087,
                        "StreamViewType": "NEW_AND_OLD_IMAGES",
                    },
                },
                {
                    "eventID": "35216650-65f3-4c65-8d13-5f9491c4914d",
                    "eventName": "MODIFY",
                    "eventVersion": "1.1",
                    "eventSource": "aws:dynamodb",
                    "awsRegion": "ddblocal",
                    "dynamodb": {
                        "ApproximateCreationDateTime": 1691164260000,
                        "Keys": {
                            "sk": {"S": "GROUP#asgard"},
                            "pk": {"S": "ROOT#3a69ee71-1183-4cbe-99d9-27b2617df7d5"},
                        },
                        "NewImage": {
                            "sk": {"S": "GROUP#asgard"},
                            "pk_2": {"S": "ORG#makimachi"},
                            "created_date": {"S": "2020-11-19T13:37:10+00:00"},
                            "pk": {"S": "ROOT#3a69ee71-1183-4cbe-99d9-27b2617df7d5"},
                            "state": {
                                "M": {
                                    "credential_id": {
                                        "S": "0df32c4a-526d-4879-8dc8-9ae191d2721b",
                                    },
                                    "environment": {"S": "production"},
                                    "gitignore": {"L": []},
                                    "includes_health_check": {"BOOL": True},
                                    "modified_by": {"S": "jdoe@fluidattacks.com"},
                                    "nickname": {"S": "bwapp"},
                                    "environment_urls": {
                                        "L": [
                                            {"S": "https://hub.docker.com/r/raesene/bwapp"},
                                            {"S": "https://test.com.co/bwapp"},
                                        ],
                                    },
                                    "modified_date": {"S": "2021-11-19T13:37:10+00:00"},
                                    "branch": {"S": "master"},
                                    "url": {"S": "https://gitlab.com/fluidattacks/bwapp.git"},
                                    "status": {"S": "INACTIVE"},
                                },
                            },
                            "unreliable_indicators": {
                                "M": {
                                    "unreliable_last_status_update": {
                                        "S": "2020-11-19T13:37:10+00:00",
                                    },
                                },
                            },
                            "type": {"S": "Git"},
                            "created_by": {"S": "jdoe@fluidattacks.com"},
                            "cloning": {
                                "M": {
                                    "commit": {"S": "98dbb971caf519fb6c56c67df7e02b253fd5a97f"},
                                    "commit_date": {"S": "2022-02-15T18:45:06.493253+00:00"},
                                    "reason": {"S": "root OK"},
                                    "modified_by": {"S": "integratesmanager@fluidattacks.com"},
                                    "modified_date": {"S": "2020-11-19T13:39:10+00:00"},
                                    "status": {"S": "OK"},
                                },
                            },
                            "sk_2": {"S": "ROOT#3a69ee71-1183-4cbe-99d9-27b2617df7d5"},
                        },
                        "OldImage": {
                            "sk": {"S": "GROUP#asgard"},
                            "pk_2": {"S": "ORG#makimachi"},
                            "created_date": {"S": "2020-11-19T13:37:10+00:00"},
                            "pk": {"S": "ROOT#3a69ee71-1183-4cbe-99d9-27b2617df7d5"},
                            "state": {
                                "M": {
                                    "credential_id": {
                                        "S": "0df32c4a-526d-4879-8dc8-9ae191d2721b",
                                    },
                                    "environment": {"S": "production"},
                                    "gitignore": {"L": []},
                                    "includes_health_check": {"BOOL": True},
                                    "modified_by": {"S": "jdoe@fluidattacks.com"},
                                    "nickname": {"S": "bwapp"},
                                    "environment_urls": {
                                        "L": [
                                            {"S": "https://hub.docker.com/r/raesene/bwapp"},
                                            {"S": "https://test.com.co/bwapp"},
                                        ],
                                    },
                                    "modified_date": {"S": "2020-11-19T13:37:10+00:00"},
                                    "branch": {"S": "master"},
                                    "url": {"S": "https://gitlab.com/fluidattacks/bwapp.git"},
                                    "status": {"S": "ACTIVE"},
                                },
                            },
                            "unreliable_indicators": {
                                "M": {
                                    "unreliable_last_status_update": {
                                        "S": "2020-11-19T13:37:10+00:00",
                                    },
                                },
                            },
                            "type": {"S": "Git"},
                            "created_by": {"S": "jdoe@fluidattacks.com"},
                            "cloning": {
                                "M": {
                                    "commit": {"S": "98dbb971caf519fb6c56c67df7e02b253fd5a97f"},
                                    "commit_date": {"S": "2022-02-15T18:45:06.493253+00:00"},
                                    "reason": {"S": "root OK"},
                                    "modified_by": {"S": "integratesmanager@fluidattacks.com"},
                                    "modified_date": {"S": "2020-11-19T13:39:10+00:00"},
                                    "status": {"S": "OK"},
                                },
                            },
                            "sk_2": {"S": "ROOT#3a69ee71-1183-4cbe-99d9-27b2617df7d5"},
                        },
                        "SequenceNumber": "000000000000000000098",
                        "SizeBytes": 1087,
                        "StreamViewType": "NEW_AND_OLD_IMAGES",
                    },
                },
            ],
        ],
    ],
)
def test_streams_process_modify_historic(
    input_data: list[dict[str, Any]],
) -> None:
    process_historic({"Records": input_data}, LambdaContext())
    time.sleep(5)
    event_historic = get_entity_historic(
        pk="EVENT#418900972",
        sk="GROUP#group1",
        state_name="state",
    )
    assert event_historic == [
        {
            "reason": "TOE_WILL_REMAIN_UNCHANGED",
            "test_decimal": Decimal("3.9"),
            "sk": "STATE#state#DATE#2018-06-28T19:40:05+00:00",
            "modified_by": "hacker@gmail.com",
            "pk_2": "EVENT#GROUP#group1",
            "pk": "EVENT#418900972#GROUP#group1",
            "pk_3": "GROUP#group1",
            "modified_date": "2018-06-28T19:40:05+00:00",
            "sk_2": "STATE#state#DATE#2018-06-28T19:40:05+00:00",
            "sk_3": "STATE#state#DATE#2018-06-28T19:40:05+00:00",
            "status": "SOLVED",
            "test_set": {"15375781-31f2-4953-ac77-f31134225748"},
        },
    ]
    root_historic = get_entity_historic(
        pk="ROOT#3a69ee71-1183-4cbe-99d9-27b2617df7d5",
        sk="GROUP#asgard",
        state_name="state",
    )
    assert root_historic == [
        {
            "gitignore": [],
            "modified_date": "2021-11-19T13:37:10+00:00",
            "branch": "master",
            "url": "https://gitlab.com/fluidattacks/bwapp.git",
            "environment": "production",
            "includes_health_check": True,
            "sk": "STATE#state#DATE#2021-11-19T13:37:10+00:00",
            "modified_by": "jdoe@fluidattacks.com",
            "nickname": "bwapp",
            "pk_2": "ROOT#GROUP#asgard",
            "pk_3": "GROUP#asgard",
            "pk": "ROOT#3a69ee71-1183-4cbe-99d9-27b2617df7d5#GROUP#asgard",
            "environment_urls": [
                "https://hub.docker.com/r/raesene/bwapp",
                "https://test.com.co/bwapp",
            ],
            "sk_2": "STATE#state#DATE#2021-11-19T13:37:10+00:00",
            "sk_3": "STATE#state#DATE#2021-11-19T13:37:10+00:00",
            "credential_id": "0df32c4a-526d-4879-8dc8-9ae191d2721b",
            "status": "INACTIVE",
        },
    ]


@pytest.mark.skip(reason="Rework in progress")
@pytest.mark.resolver_test_group("streams_process_historic")
@freeze_time("2022-11-11T15:58:31.280182")
@pytest.mark.parametrize(
    ["input_data"],
    [
        [
            [
                {
                    "eventID": "35216650-65f3-4c65-8d13-5f9491c4913d",
                    "eventName": "REMOVE",
                    "eventVersion": "1.1",
                    "eventSource": "aws:dynamodb",
                    "awsRegion": "ddblocal",
                    "dynamodb": {
                        "ApproximateCreationDateTime": 1691164260000,
                        "Keys": {
                            "sk": {"S": "GROUP#group1"},
                            "pk": {"S": "EVENT#688900977"},
                        },
                        "OldImage": {
                            "group_name": {"S": "group1"},
                            "description": {"S": "ARM unit test"},
                            "unreliable_indicators": {"M": {}},
                            "type": {"S": "OTHER"},
                            "created_by": {"S": "unittest@fluidattacks.com"},
                            "hacker": {"S": "hacker@gmail.com"},
                            "event_date": {"S": "2018-06-27T12:00:00+00:00"},
                            "sk": {"S": "GROUP#group1"},
                            "client": {"S": "Fluid"},
                            "pk_2": {"S": "GROUP#group1"},
                            "created_date": {"S": "2018-06-27T12:00:00+00:00"},
                            "id": {"S": "688900977"},
                            "pk": {"S": "EVENT#688900977"},
                            "state": {
                                "M": {
                                    "modified_by": {"S": "hacker@gmail.com"},
                                    "modified_date": {"S": "2018-06-27T19:40:05+00:00"},
                                    "status": {"S": "CREATED"},
                                },
                            },
                            "evidences": {
                                "M": {
                                    "file_1": {
                                        "M": {
                                            "file_name": {"S": ""},
                                            "modified_date": {"S": "2019-03-11T15:57:45Z"},
                                        },
                                    },
                                    "image_1": {
                                        "M": {
                                            "file_name": {"S": ""},
                                            "modified_date": {"S": "2019-03-11T15:57:45Z"},
                                        },
                                    },
                                },
                            },
                            "sk_2": {"S": "EVENT#SOLVED#false"},
                        },
                        "SequenceNumber": "000000000000000000098",
                        "SizeBytes": 1087,
                        "StreamViewType": "NEW_AND_OLD_IMAGES",
                    },
                },
                {
                    "eventID": "35216650-65f3-4c65-8d13-5f9491c4913d",
                    "eventName": "MODIFY",
                    "eventVersion": "1.1",
                    "eventSource": "aws:dynamodb",
                    "awsRegion": "ddblocal",
                    "dynamodb": {
                        "ApproximateCreationDateTime": 1691164260000,
                        "Keys": {
                            "sk": {"S": "GROUP#group1"},
                            "pk": {"S": "EVENT#788900977"},
                        },
                        "NewImage": {
                            "group_name": {"S": "group1"},
                            "description": {"S": "ARM unit test"},
                            "unreliable_indicators": {"M": {}},
                            "type": {"S": "OTHER"},
                            "created_by": {"S": "unittest@fluidattacks.com"},
                            "hacker": {"S": "hacker@gmail.com"},
                            "event_date": {"S": "2018-06-27T12:00:00+00:00"},
                            "sk": {"S": "GROUP#group1"},
                            "client": {"S": "Fluid"},
                            "pk_2": {"S": "GROUP#group1"},
                            "created_date": {"S": "2018-06-27T12:00:00+00:00"},
                            "id": {"S": "788900977"},
                            "pk": {"S": "EVENT#788900977"},
                            "state": {
                                "M": {
                                    "modified_by": {"S": "hacker@gmail.com"},
                                    "modified_date": {"S": "2018-06-29T19:40:05+00:00"},
                                    "status": {"S": "CREATED"},
                                },
                            },
                            "evidences": {
                                "M": {
                                    "file_1": {
                                        "M": {
                                            "file_name": {"S": ""},
                                            "modified_date": {"S": "2019-03-11T15:57:45Z"},
                                        },
                                    },
                                    "image_1": {
                                        "M": {
                                            "file_name": {"S": ""},
                                            "modified_date": {"S": "2019-03-11T15:57:45Z"},
                                        },
                                    },
                                },
                            },
                            "sk_2": {"S": "EVENT#SOLVED#false"},
                        },
                        "OldImage": {
                            "group_name": {"S": "group1"},
                            "description": {"S": "ARM unit test"},
                            "unreliable_indicators": {"M": {}},
                            "type": {"S": "OTHER"},
                            "created_by": {"S": "unittest@fluidattacks.com"},
                            "hacker": {"S": "hacker@gmail.com"},
                            "event_date": {"S": "2018-06-28T19:40:05+00:00"},
                            "sk": {"S": "GROUP#group1"},
                            "client": {"S": "Fluid"},
                            "pk_2": {"S": "GROUP#group1"},
                            "created_date": {"S": "2018-06-28T19:40:05+00:00"},
                            "id": {"S": "788900977"},
                            "pk": {"S": "EVENT#788900977"},
                            "state": {
                                "M": {
                                    "modified_by": {"S": "hacker@gmail.com"},
                                    "modified_date": {"S": "2018-06-28T19:40:05+00:00"},
                                    "status": {"S": "CREATED"},
                                },
                            },
                            "verification": {
                                "M": {
                                    "modified_by": {"S": "hacker@gmail.com"},
                                    "modified_date": {"S": "2018-06-28T19:40:05+00:00"},
                                    "status": {"S": "VERIFIED"},
                                },
                            },
                            "evidences": {
                                "M": {
                                    "file_1": {
                                        "M": {
                                            "file_name": {"S": ""},
                                            "modified_date": {"S": "2019-03-11T15:57:45Z"},
                                        },
                                    },
                                    "image_1": {
                                        "M": {
                                            "file_name": {"S": ""},
                                            "modified_date": {"S": "2019-03-11T15:57:45Z"},
                                        },
                                    },
                                },
                            },
                            "sk_2": {"S": "EVENT#SOLVED#false"},
                        },
                        "SequenceNumber": "000000000000000000098",
                        "SizeBytes": 1087,
                        "StreamViewType": "NEW_AND_OLD_IMAGES",
                    },
                },
            ],
        ],
    ],
)
def test_streams_process_remove_historic(
    input_data: list[dict[str, Any]],
) -> None:
    process_historic({"Records": input_data}, LambdaContext())
    time.sleep(5)
    state_historic = get_entity_historic(
        pk="EVENT#688900977",
        sk="GROUP#group1",
        state_name="state",
    )
    assert state_historic == []
    state_historic = get_entity_historic(
        pk="EVENT#788900977",
        sk="GROUP#group1",
        state_name="state",
    )
    assert state_historic == [
        {
            "modified_by": "unittest@fluidattacks.com",
            "modified_date": "2018-12-17T21:20:00+00:00",
            "pk": "EVENT#788900977#GROUP#group1",
            "sk_2": "STATE#state#DATE#2018-06-28T19:40:05+00:00",
            "sk_3": "STATE#state#DATE#2018-06-28T19:40:05+00:00",
            "pk_2": "EVENT#GROUP#group1",
            "pk_3": "GROUP#group1",
            "sk": "STATE#state#DATE#2018-06-28T19:40:05+00:00",
            "status": "OPEN",
        },
        {
            "modified_by": "hacker@gmail.com",
            "modified_date": "2018-06-29T19:40:05+00:00",
            "pk": "EVENT#788900977#GROUP#group1",
            "pk_2": "EVENT#GROUP#group1",
            "pk_3": "GROUP#group1",
            "sk": "STATE#state#DATE#2018-06-29T19:40:05+00:00",
            "sk_2": "STATE#state#DATE#2018-06-29T19:40:05+00:00",
            "sk_3": "STATE#state#DATE#2018-06-29T19:40:05+00:00",
            "status": "CREATED",
        },
    ]
