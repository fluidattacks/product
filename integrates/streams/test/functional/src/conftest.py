import os
from typing import (
    Any,
)

import pytest

# Constants
PATH = os.path.dirname(os.path.abspath(__file__))
TEST_GROUPS = {
    directory
    for directory in os.listdir(PATH)
    if os.path.isdir(os.path.join(PATH, directory)) and not directory.startswith("__")
}


def pytest_addoption(parser: pytest.Parser) -> None:
    parser.addoption(
        "--resolver-test-group",
        action="store",
        metavar="RESOLVER_TEST_GROUP",
    )


def pytest_runtest_setup(item: Any) -> None:
    resolver_test_group = item.config.getoption("--resolver-test-group")

    if not resolver_test_group:
        raise ValueError("resolver-test-group not specified")
    if resolver_test_group not in TEST_GROUPS:
        raise ValueError(f"resolver-test-group must be one of: {TEST_GROUPS}")

    runnable_groups = {mark.args[0] for mark in item.iter_markers(name="resolver_test_group")}

    if not runnable_groups or runnable_groups - TEST_GROUPS:
        raise ValueError(f"resolver-test-group must be one of: {TEST_GROUPS}")

    if resolver_test_group not in runnable_groups:
        pytest.skip(f"Requires resolver test group in: {runnable_groups}")
