# shellcheck shell=bash

function main {
  local resolver_test_group="${1}"
  export COVERAGE_FILE=.coverage."${resolver_test_group}"
  local pytest_args=(
    --cov 'streams/streams'
    --cov-branch
    --cov-report 'term'
    --disable-warnings
    --no-cov-on-fail
    --showlocals
    --resolver-test-group "${resolver_test_group}"
    -vv
  )

  : \
    && aws_login "dev" "3600" \
    && export ENVIRONMENT="development" \
    && sops_export_vars __argSecretsDev__ \
      AWS_DYNAMODB_HOST_DEV \
      AWS_OPENSEARCH_HOST_DEV \
      AZURE_OAUTH2_ISSUES_SECRET_DEV \
      BUGSNAG_API_KEY_STREAMS \
      GITLAB_ISSUES_OAUTH2_APP_ID \
      GITLAB_ISSUES_OAUTH2_SECRET \
      GOOGLE_CHAT_WEBHOOK_URL_DEV \
      WEBHOOK_POC_KEY_DEV \
      WEBHOOK_POC_ORG_DEV \
      WEBHOOK_POC_URL_DEV \
    && DAEMON=true POPULATE=false STREAMS=false integrates-db \
    && echo "[INFO] Running tests for: ${resolver_test_group}" \
    && pushd integrates \
    && PYTHONPATH="streams/:$PYTHONPATH" \
    && pytest streams/test/functional/src "${pytest_args[@]}" \
    && popd \
    || return 1
}

main "${@}"
