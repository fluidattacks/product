{ makePythonEnvironment, makeSearchPaths, projectPath, ... }:
let
  src = projectPath "/integrates/streams";
  pythonRequirements = makePythonEnvironment {
    pythonProjectDir = src;
    pythonVersion = "3.11";
  };
in makeSearchPaths {
  pythonPackage = [ src ];
  source = [ pythonRequirements ];
}
