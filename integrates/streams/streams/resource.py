from boto3 import (
    Session,
)

from streams.context import (
    FI_AWS_DYNAMODB_HOST,
)

SESSION = Session()
RESOURCE = SESSION.resource("dynamodb", endpoint_url=FI_AWS_DYNAMODB_HOST)
TABLE_RESOURCE = RESOURCE.Table("integrates_vms")
HISTORIC_TABLE = RESOURCE.Table("integrates_vms_historic")
