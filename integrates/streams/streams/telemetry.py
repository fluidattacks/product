import logging
import os

import bugsnag
from bugsnag.handlers import BugsnagHandler

from streams.context import FI_BUGSNAG_API_KEY_STREAMS, FI_CI_COMMIT_SHA, FI_ENABLE_TELEMETRY


def initialize_telemetry() -> None:
    if FI_ENABLE_TELEMETRY:
        bugsnag.configure(
            api_key=FI_BUGSNAG_API_KEY_STREAMS,
            app_type="worker",
            app_version=FI_CI_COMMIT_SHA,
            notify_release_stages=["production"],
            project_root=os.path.dirname(os.path.abspath(__file__)),
            release_stage="production",
        )
        bugsnag.start_session()

        logger = logging.getLogger()
        handler = BugsnagHandler(extra_fields={"extra": ["extra"]})
        handler.setLevel(logging.ERROR)
        logger.addFilter(handler.leave_breadcrumbs)
        logger.addHandler(handler)
