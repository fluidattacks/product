from enum import (
    Enum,
)
from typing import (
    TypedDict,
)


class HookEvent(str, Enum):
    DELETED_VULNERABILITY = "Vulnerability was deleted"
    EDITED_VULNERABILITY = "Vulnerability was edited"
    REPORTED_VULNERABILITY = "Vulnerability was reported"


class IssueSubtask(TypedDict):
    title: str
