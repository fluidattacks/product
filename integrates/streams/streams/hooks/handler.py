import itertools
from typing import (
    Any,
)

import bugsnag

from streams.hooks import (
    events,
    roots,
    vulnerabilities,
)
from streams.hooks.notifier import (
    is_under_review,
)
from streams.types import (
    Record,
)
from streams.utils import (
    format_record,
)


@bugsnag.aws_lambda_handler
def process_events(event: dict[str, Any], _context: Any) -> None:
    records = tuple(format_record(record) for record in event["Records"])

    sorted_records = sorted(records, key=get_key)
    records_by_group = itertools.groupby(sorted_records, key=get_key)

    for group_name, group_records_iter in records_by_group:
        if is_under_review(group_name):
            continue
        group_records = tuple(group_records_iter)
        events.notify_created(group_name, group_records)
        events.notify_solved(group_name, group_records)


def get_key(record: Record) -> str:
    if record.new_image:
        return record.new_image["sk"].lstrip("GROUP#")
    if record.old_image:
        return record.old_image["sk"].lstrip("GROUP#")
    return ""


@bugsnag.aws_lambda_handler
def process_roots(event: dict[str, Any], _context: Any) -> None:
    records = tuple(format_record(record) for record in event["Records"])

    sorted_records = sorted(records, key=get_key)
    records_by_group = itertools.groupby(sorted_records, key=get_key)

    for group_name, group_records_iter in records_by_group:
        if is_under_review(group_name):
            continue
        group_records = tuple(group_records_iter)
        roots.notify_created(group_name, group_records)
        roots.notify_disabled(group_name, group_records)


@bugsnag.aws_lambda_handler
def process_vulns(event: dict[str, Any], _context: Any) -> None:
    records = tuple(format_record(record) for record in event["Records"])

    def _get_key(record: Record) -> str:
        if record.new_image:
            return record.new_image["group_name"]
        if record.old_image:
            return record.old_image["group_name"]
        return ""

    sorted_records = sorted(records, key=_get_key)
    records_by_group = itertools.groupby(sorted_records, key=_get_key)

    for group_name, group_records_iter in records_by_group:
        if is_under_review(group_name):
            continue
        group_records = tuple(group_records_iter)
        vulnerabilities.notify_created(group_name, group_records)
        vulnerabilities.notify_deleted(group_name, group_records)
        vulnerabilities.notify_updated(group_name, group_records)
