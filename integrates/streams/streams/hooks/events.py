from streams.hooks import (
    notifier,
)
from streams.types import (
    Record,
    StreamEvent,
)


def _was_created(record: Record) -> bool:
    return bool(
        record.event_name == StreamEvent.INSERT
        and record.new_image
        and record.new_image["state"]["status"] == "OPEN",
    )


def _was_solved(record: Record) -> bool:
    return bool(
        record.event_name == StreamEvent.MODIFY
        and record.old_image
        and record.old_image["state"]["status"] != "SOLVED"
        and record.new_image
        and record.new_image["state"]["status"] == "SOLVED",
    )


def notify_solved(
    group_name: str,
    group_records: tuple[Record, ...],
) -> None:
    solved_records = tuple(record for record in group_records if _was_solved(record))
    if not solved_records:
        return

    for record in solved_records:
        event = record.new_image
        if not event:
            return

        notifier.notify_hooks(
            event="EVENT_CLOSED",
            group_name=group_name,
            info={"event_id": event["pk"].lstrip("EVENT#")},
        )


def notify_created(
    group_name: str,
    group_records: tuple[Record, ...],
) -> None:
    created_records = tuple(record for record in group_records if _was_created(record))
    if not created_records:
        return

    for record in created_records:
        event = record.new_image
        if not event:
            return
        notifier.notify_hooks(
            event="EVENT_CREATED",
            group_name=group_name,
            info={"event_id": event["pk"].lstrip("EVENT#")},
        )
