from collections import (
    Counter,
)
from typing import (
    Any,
)

from streams.indicators.types import (
    FindingIndicators,
)
from streams.indicators.utils import (
    handle_exceptions,
    is_zr_confirmed_or_requested,
)


@handle_exceptions
def new_update_all(
    *,
    new_indicators: dict[str, Any],
    vulns: list[Any],
) -> None:
    """
    Updates treatment summary (total of untreated, in_progress, accepted,
    accepted_undefined) in `new_indicators`.

    Args:
        new_indicators (dict[str, Any]): Finding indicators to update.
        vulns (list[Any]): Finding vulnerabilities.

    """
    released_vulns = [vuln for vuln in vulns if not is_zr_confirmed_or_requested(vuln)]
    treatment_counter = Counter(
        vuln["treatment"]["status"]
        for vuln in released_vulns
        if "treatment" in vuln and "state" in vuln and vuln["state"]["status"] == "VULNERABLE"
    )
    new_indicators[FindingIndicators.TREATMENT_SUMMARY] = {
        "accepted": treatment_counter["ACCEPTED"],
        "accepted_undefined": treatment_counter["ACCEPTED_UNDEFINED"],
        "in_progress": treatment_counter["IN_PROGRESS"],
        "untreated": treatment_counter["UNTREATED"],
    }
