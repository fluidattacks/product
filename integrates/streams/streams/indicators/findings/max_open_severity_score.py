from decimal import (
    Decimal,
)
from typing import (
    Any,
)

from streams.indicators.types import (
    FindingIndicators,
)
from streams.indicators.utils import (
    handle_exceptions,
    is_zr_confirmed_or_requested,
)


def _get_cvss_scores(vulns: list[Any], finding: Any) -> list[Decimal]:
    return [
        Decimal(vuln["severity_score"].get("temporal_score", "0"))
        if "severity_score" in vuln and "temporal_score" in vuln["severity_score"]
        else Decimal(finding["severity_score"].get("temporal_score", "0"))
        for vuln in vulns
        if "state" in vuln and vuln["state"]["status"] == "VULNERABLE"
    ]


def _get_cvss_v4_scores(vulns: list[Any], finding: Any) -> list[Decimal]:
    return [
        Decimal(vuln["severity_score"].get("threat_score", "0"))
        if "severity_score" in vuln and "threat_score" in vuln["severity_score"]
        else Decimal(finding["severity_score"].get("threat_score", "0"))
        for vuln in vulns
        if "state" in vuln and vuln["state"]["status"] == "VULNERABLE"
    ]


@handle_exceptions
def new_update(
    *,
    new_indicators: dict[str, Any],
    vulns: list[Any],
    finding: Any,
) -> None:
    """
    Updates max open severity score (v3 and v4) in `new_indicators`.

    Args:
        new_indicators: Finding indicators to update.
        vulns: Finding vulnerabilities.
        finding: Finding metadata.

    """
    released_vulns = [vuln for vuln in vulns if not is_zr_confirmed_or_requested(vuln)]
    scores = _get_cvss_scores(released_vulns, finding)
    scores_v4 = _get_cvss_v4_scores(released_vulns, finding)

    new_indicators[FindingIndicators.SEVERITY] = max(scores) if len(scores) > 0 else Decimal(0)

    new_indicators[FindingIndicators.SEVERITY_V4] = (
        max(scores_v4) if len(scores_v4) > 0 else Decimal(0)
    )
