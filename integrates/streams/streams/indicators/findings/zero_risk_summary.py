from collections import (
    Counter,
)
from typing import (
    Any,
)

from streams.indicators.types import (
    FindingIndicators,
)
from streams.indicators.utils import (
    handle_exceptions,
)


@handle_exceptions
def new_update_all(
    *,
    new_indicators: dict[str, Any],
    vulns: list[Any],
) -> None:
    """
    Updates zero risk summary (total of requested, confirmed, rejected) in
    `new_indicators`.

    Args:
        new_indicators: Finding indicators to update.
        vulns: Finding vulnerabilities.

    """
    zero_risk_counter = Counter(
        vuln["zero_risk"]["status"]
        for vuln in vulns
        if "zero_risk" in vuln and "state" in vuln and vuln["state"]["status"] == "VULNERABLE"
    )

    new_indicators[FindingIndicators.ZERO_RISK_SUMMARY] = {
        "confirmed": zero_risk_counter["CONFIRMED"],
        "rejected": zero_risk_counter["REJECTED"],
        "requested": zero_risk_counter["REQUESTED"],
    }
