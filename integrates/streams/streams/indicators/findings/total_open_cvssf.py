from decimal import (
    Decimal,
)
from typing import (
    Any,
    Literal,
)

from streams.indicators.types import (
    FindingIndicators,
)
from streams.indicators.utils import (
    handle_exceptions,
    is_zr_confirmed_or_requested,
)


def get_cvssf(vuln: Any, finding: Any, key: Literal["cvssf", "cvssf_v4"]) -> Decimal:
    vuln_score = vuln["severity_score"][key] if "severity_score" in vuln else 0
    finding_score = finding["severity_score"][key] if "severity_score" in finding else 0
    return vuln_score if vuln_score else finding_score


def get_total_cvssf(vulns: list[Any], finding: Any, key: Literal["cvssf", "cvssf_v4"]) -> Decimal:
    vulns_cvssf = [
        get_cvssf(vuln, finding, key)
        for vuln in vulns
        if vuln["state"] and vuln["state"]["status"] == "VULNERABLE"
    ]
    total_open_cvssf = sum(Decimal(value) for value in vulns_cvssf)
    return Decimal(total_open_cvssf)


@handle_exceptions
def new_update_all(
    *,
    new_indicators: dict[str, Any],
    vulns: list[Any],
    finding: Any,
) -> None:
    """
    Updates total open cvssf (v3 and v4) in `new_indicators`.

    Args:
        new_indicators: Finding indicators to update.
        vulns: Finding vulnerabilities.
        finding: Finding metadata.

    """
    released_vulns = [vuln for vuln in vulns if not is_zr_confirmed_or_requested(vuln)]

    new_indicators[FindingIndicators.TOTAL_OPEN_CVSSF] = get_total_cvssf(
        released_vulns,
        finding,
        "cvssf",
    )

    new_indicators[FindingIndicators.TOTAL_OPEN_CVSSF_V4] = get_total_cvssf(
        released_vulns,
        finding,
        "cvssf_v4",
    )
