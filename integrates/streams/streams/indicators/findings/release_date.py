from datetime import (
    datetime,
)
from typing import (
    Any,
)

from streams.indicators.types import (
    FindingIndicators,
)
from streams.indicators.utils import (
    handle_exceptions,
    is_zr_confirmed_or_requested,
)


@handle_exceptions
def new_update(
    *,
    new_indicators: dict[str, Any],
    vulns: list[Any],
) -> None:
    """
    Updates oldest report date and newest report date in `new_indicators`.

    Args:
        new_indicators: Finding indicators to update.
        vulns: Finding vulnerabilities.

    """
    released_vulns = [vuln for vuln in vulns if not is_zr_confirmed_or_requested(vuln)]
    dates = [
        datetime.fromisoformat(v["unreliable_indicators"]["unreliable_report_date"])
        if "unreliable_indicators" in v and "unreliable_report_date" in v["unreliable_indicators"]
        else datetime.fromisoformat(v["state"]["modified_date"])
        for v in released_vulns
    ]

    if len(dates) > 0:
        min_date, max_date = min(dates), max(dates)
        new_indicators[FindingIndicators.NEWEST_REPORT_DATE] = max_date.isoformat()
        new_indicators[FindingIndicators.OLDEST_REPORT_DATE] = min_date.isoformat()
    else:
        new_indicators[FindingIndicators.NEWEST_REPORT_DATE] = None
        new_indicators[FindingIndicators.OLDEST_REPORT_DATE] = None
