from decimal import (
    Decimal,
)
from typing import (
    Any,
)

from streams.indicators.types import (
    FindingIndicators,
)
from streams.indicators.utils import (
    handle_exceptions,
    is_zr_confirmed_or_requested,
)


def get_total_severity_score(vulns: list[Any]) -> Decimal:
    return Decimal(
        sum(
            Decimal(vuln["unreliable_indicators"].get("unreliable_priority", "0")) for vuln in vulns
        ),
    )


@handle_exceptions
def new_update(
    *,
    new_indicators: dict[str, Any],
    vulns: list[Any],
) -> None:
    """
    Updates total open priority in `new_indicators`.

    Args:
        new_indicators: Finding indicators to update.
        vulns: Finding vulnerabilities.

    """
    released_vulns = [vuln for vuln in vulns if not is_zr_confirmed_or_requested(vuln)]
    vulns_open = [
        vuln
        for vuln in released_vulns
        if "state" in vuln and vuln["state"]["status"] == "VULNERABLE"
    ]

    new_indicators[FindingIndicators.TOTAL_OPEN_PRIORITY] = get_total_severity_score(vulns_open)
