from typing import (
    Any,
)

from streams.indicators.types import (
    FindingIndicators,
)
from streams.indicators.utils import (
    handle_exceptions,
)


@handle_exceptions
def new_update(
    *,
    new_indicators: dict[str, Any],
    roots: list[Any],
) -> None:
    """
    Updates max open root criticality in `new_indicators`.

    Args:
        new_indicators: Finding indicators to update.
        roots: Finding roots with criticality.

    """
    values = {
        "LOW": 1,
        "MEDIUM": 2,
        "HIGH": 3,
    }

    criticalities = {
        root["state"].get("criticality", "LOW") if "state" in root else "LOW" for root in roots
    }

    if len(criticalities) == 0:
        new_indicators[FindingIndicators.ROOT_CRITICALITY] = "LOW"
        return

    new_indicators[FindingIndicators.ROOT_CRITICALITY] = max(
        criticalities,
        key=lambda x: values.get(x, 1),
    )
