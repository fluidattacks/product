from collections import (
    Counter,
)
from typing import (
    Any,
)

from streams.indicators.types import (
    FindingIndicators,
)
from streams.indicators.utils import (
    handle_exceptions,
)


@handle_exceptions
def new_update_all(
    *,
    new_indicators: dict[str, Any],
    vulns: list[Any],
) -> None:
    """
    Updates verification summary (total of requested, on_hold, verified) in
    `new_indicators`.

    Args:
        new_indicators: Finding indicators to update.
        vulns: Finding vulnerabilities.

    """
    verification_counter = Counter(
        vuln["verification"]["status"]
        for vuln in vulns
        if "verification" in vuln and "state" in vuln and vuln["state"]["status"] == "VULNERABLE"
    )

    new_indicators[FindingIndicators.VERIFICATION_SUMMARY] = {
        "verified": verification_counter["VERIFIED"],
        "on_hold": verification_counter["ON_HOLD"],
        "requested": verification_counter["REQUESTED"],
    }
