from collections.abc import Callable
from typing import (
    Any,
    NamedTuple,
)

from streams.types import (
    Record,
    StreamEvent,
)


class FindingItem(NamedTuple):
    id: str
    finding: Any
    indicators: dict[str, Any]
    to_update: dict[str, Any] = {}
    roots: list[Any] = []


class IndicatorsItem(NamedTuple):
    record: Record
    event: StreamEvent
    vuln: Any
    root: Any
    vulnerabilities: list[Any]
    finding_item: FindingItem


class FindingStore:
    """
    Data structure for storing finding information.

    It is used to store finding items before to send it to database.
    """

    _store: list[FindingItem]
    _root_dict: dict[str, Any]
    _finding_list: list[str]

    def __init__(
        self,
        records: tuple[Record, ...],
        get_finding: Callable,
        get_root: Callable,
    ) -> None:
        vulns = [record.new_image if record.new_image else record.old_image for record in records]

        self._finding_list = list(
            set(f"{vuln['sk']}|{vuln['pk_5']}" for vuln in vulns if vuln is not None),
        )

        stored_roots = {}
        for vuln in vulns:
            if (
                vuln
                and (key := f"{vuln['sk']}|{vuln['pk_2']}")
                and key not in stored_roots
                and (root := get_root(pk=vuln["pk_2"], sk=vuln["pk_5"]))
                and root
            ):
                stored_roots[key] = root

        self._root_dict = stored_roots.copy()

        self._store = [
            FindingItem(
                id=finding,
                finding=get_finding(
                    pk=fin_id,
                    sk=group_id,
                ),
                indicators={},
                roots=[
                    root
                    for root_key, root in self._root_dict.items()
                    if root_key.split("|")[0] == fin_id
                ],
            )
            for finding in self._finding_list
            if (fin_id := finding.split("|")[0]) and (group_id := finding.split("|")[1])
        ]

    def get_store(self) -> list[FindingItem]:
        return self._store

    def get_finding_list(self) -> list[str]:
        return self._finding_list

    def get_by_key(self, key: str) -> FindingItem:
        """Get a finding item by key."""
        return next(item for item in self._store if item.id == key)

    def update_by_key(self, key: str) -> None:
        """
        Update finding indicators in store by key.

        It uses `to_update` field to update `indicators` field.
        """
        item = self.get_by_key(key)
        item.indicators.update(item.to_update)
        item = item._replace(to_update={})
