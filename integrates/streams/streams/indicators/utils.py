# pylint: disable=broad-exception-caught
from collections.abc import Callable
from decimal import (
    Decimal,
)
from typing import (
    Any,
)

from streams.logger import (
    StreamsLogger,
)
from streams.types import (
    Record,
)

LOGGER = StreamsLogger("streams.indicators")
LOGGER.set_handler("📘")


def log_record(records: tuple[Record, ...]) -> None:
    rec = records[0]
    LOGGER.info("[%s] Total records: %s", get_process_id(rec), len(records))

    vulns = [record.new_image if record.new_image else record.old_image for record in records]
    finding_list = set(f"{vuln['sk']}|{vuln['pk_5']}" for vuln in vulns if vuln is not None)

    LOGGER.info("[%s] Findings: %s", get_process_id(rec), finding_list)


def get_process_id(record: Record) -> str:
    """Returns process id based on the record (`event-sequence`)."""
    return f"{record.event_name.value!s}-{record.sequence_number}"


def format_indicators(indicators: dict[str, Any]) -> dict[str, Any]:
    """
    Cleanup method for indicators.

    It adds the `unreliable_indicators` prefix to the keys of the
    `indicators` dict. Also, it removes the keys with `None` values.
    """
    return {f"unreliable_indicators.{key}": value for key, value in indicators.items()}


def _format_nested_key(key: str) -> str:
    return key.replace(".", "_")


def format_to_expression_attributes_names(values: set[str]) -> dict[str, str]:
    """
    Returns a dict with the `ExpressionAttributesNames`
    field format.

    Example: {"id", "name"} -> {"#id": "id", "#name": "name" }
    """
    return {f"#{value}": value for value in values}


def format_to_project_expression(values: set[str]) -> str:
    """
    Returns a string with the `ProjectExpression`
    field format.

    Example: {"id", "name"} -> "#id,#name"
    """
    return ",".join([f"#{value}" for value in values])


def format_to_expression_attributes_values(values: dict[str, Any]) -> dict[str, str]:
    """
    Returns a dict with the `ExpressionAttributesValues`
    field format.

    Example: {"id": 1, "first.name": "john"}
    -> {":id": 1, ":first_name": "john" }
    """
    return {f":{_format_nested_key(key)}": value for key, value in values.items()}


def format_to_update_expression(values: dict[str, Any]) -> str:
    """
    Returns a dict with the `UpdateExpression`
    field format.

    Example: {"id": 1, "first.name": "john"}
    -> "SET id = :id,first_name = :john"
    """
    return "SET " + ",".join(f"{key} = :{_format_nested_key(key)}" for key in values.keys())


def is_zr_confirmed_or_requested(vuln: dict[str, Any]) -> bool:
    """
    "If the vulnerability has zero risk and the status
    is `CONFIRMED` or `REQUESTED`, returns `True`.

    Otherwise, returns `False`.
    """
    return "zero_risk" in vuln and vuln["zero_risk"]["status"] in [
        "CONFIRMED",
        "REQUESTED",
    ]


def is_zr_requested(vuln: dict[str, Any]) -> bool:
    """
    "If the vulnerability has zero risk and the status
    is `REQUESTED`, returns `True`.

    Otherwise, returns `False`.
    """
    return "zero_risk" in vuln and vuln["zero_risk"]["status"] in [
        "REQUESTED",
    ]


def verbose(func: Callable) -> Callable:
    """
    Decorator for adding verbose logs
    for updating finding indicators method.
    """

    def wrapper() -> Callable:
        def decorated(*args: Any, **kwargs: Any) -> None:
            func(*args, **kwargs)
            finding = kwargs.get("finding")
            current = kwargs.get("current_indicators")
            new = kwargs.get("new_indicators")
            if finding is None or current is None or new is None:
                return

            for key in new:
                LOGGER.info(
                    "[%s] %s == %s >> %s",
                    finding.get("pk", ""),
                    key,
                    current,
                    new,
                )

        return decorated

    return wrapper


def handle_exceptions(func: Callable) -> Callable:
    """Decorator for handling common exceptions."""

    def decorated(*args: Any, **kwargs: Any) -> None:
        try:
            func(*args, **kwargs)
        except KeyError as ex:
            LOGGER.error("Handled KeyError: %s", str(ex))
        except Exception as ex:
            LOGGER.error("Unhandled exception: %s", str(ex))

    return decorated


def get_severity_level(severity: Decimal) -> str:
    """
    Qualitative severity rating scale as defined in
    https://www.first.org/cvss/v3.1/specification-document section 5.
    """
    if severity < 4:
        return "low"
    if 4 <= severity < 7:
        return "medium"
    if 7 <= severity < 9:
        return "high"

    return "critical"
