class CustomBaseException(Exception):
    pass


class _SingleMessageException(CustomBaseException):
    msg: str

    @classmethod
    def new(cls) -> "_SingleMessageException":
        return cls(cls.msg)


class FindingNotFound(_SingleMessageException):
    msg = "Exception - Finding not found"


class GroupNotFound(_SingleMessageException):
    msg = "Exception - Group not found"
