# shellcheck shell=bash
function export_secrets {
  local secrets=(
    AWS_DYNAMODB_HOST_DEV
    AWS_OPENSEARCH_HOST_DEV
    AZURE_OAUTH2_ISSUES_SECRET_DEV
    BUGSNAG_API_KEY_STREAMS
    GITLAB_ISSUES_OAUTH2_APP_ID
    GITLAB_ISSUES_OAUTH2_SECRET
    GOOGLE_CHAT_WEBHOOK_URL_DEV
    WEBHOOK_POC_KEY_DEV
    WEBHOOK_POC_ORG_DEV
    WEBHOOK_POC_URL_DEV
  )

  : && sops_export_vars __argSecretsDev__ "${secrets[@]}" \
    || return 1
}

function get_stream_arn {
  local table="${1}"
  local aws_args=(
    --endpoint-url "${AWS_DYNAMODB_HOST_DEV}"
    --table-name "${table}"
  )

  : && aws dynamodbstreams list-streams "${aws_args[@]}" \
    | jq --raw-output ".Streams[0].StreamArn" \
    || return 1
}

function run_consumer {
  local table="${1}"
  local name="${table}_consumer"
  local properties=(
    "applicationName = ${name}"
    "AWSCredentialsProvider = DefaultAWSCredentialsProviderChain"
    "dynamoDBEndpoint = ${AWS_DYNAMODB_HOST_DEV}"
    "executableName = python3 consumer.py"
    "idleTimeBetweenReadsInMillis = 250"
    "initialPositionInStream = TRIM_HORIZON"
    "kinesisEndpoint = ${AWS_DYNAMODB_HOST_DEV}"
    "metricsLevel = NONE"
    "regionName = ${AWS_DEFAULT_REGION}"
    "streamName = $(get_stream_arn "${table}")"
  )
  local properties_file="${STATE}/${name}.properties"

  : && echo "[INFO] Executing streams consumer" \
    && for property in "${properties[@]}"; do
      echo "${property}" >> "${properties_file}"
    done \
    && java \
      -Djava.util.logging.config.file=../logging.properties \
      "com.amazonaws.services.dynamodbv2.streamsadapter.StreamsMultiLangDaemon" \
      "${properties_file}" \
    || return 1
}

function serve {
  export AWS_DEFAULT_REGION="us-east-1"
  export CI_COMMIT_SHA

  : && PYTHONPATH="__argSrc__/:$PYTHONPATH" \
    && aws_login "dev" "3600" \
    && export_secrets \
    && if test -z "${CI_COMMIT_SHA-}"; then
      CI_COMMIT_SHA="$(get_commit_from_rev . HEAD)"
    fi \
    && pushd __argSrc__/local \
    && run_consumer integrates_vms \
    && popd \
    || return 1
}

function wait_for_file {
  local elapsed="1"
  local timeout="${1}"
  local path="${2}"

  while [ "${elapsed}" -le "${timeout}" ]; do
    if [ -e "${path}" ]; then
      break
    else
      info "Waiting 1 second for ${path} to exist: ${elapsed} seconds in total" \
        && sleep 1 \
        && elapsed="$(("${elapsed}" + 1))"
    fi
  done

  if [ "${elapsed}" -gt "${timeout}" ]; then
    error "Timeout while waiting for ${path} to exist" \
      && return 1
  fi
}

function serve_daemon {
  : && { serve "${@}" & } \
    && wait_for_file 300 /tmp/integrates-streams-ready \
    && info "Streams is ready"
}

function main {
  case "${DAEMON-}" in
    true) serve_daemon "${@}" ;;
    *) serve "${@}" ;;
  esac
}

main "${@}"
