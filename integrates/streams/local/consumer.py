from amazon_kclpy.kcl import (
    Checkpointer,
    CheckpointError,
    KCLProcess,
)
from amazon_kclpy.messages import (
    InitializeInput,
    ProcessRecordsInput,
    Record as KCLRecord,
    ShutdownInput,
)
from amazon_kclpy.v2 import (
    processor,
)
from dataclasses import (
    dataclass,
)
import importlib
import json
import logging
from pathlib import (
    Path,
)
from time import (
    sleep,
)
from typing import (
    Any,
)

logging.basicConfig(
    datefmt="%Y-%m-%d %H:%M:%S",
    format="%(asctime)s - [%(levelname)s] %(message)s",
)
LOGGER = logging.getLogger()


TRIGGERS = {
    key: value
    for key, value in json.loads(Path("../triggers.json").read_text()).items()
    if key != "$schema"
}


def matches_rule(
    rule_key: str, rule_value: dict[str, Any] | list, record: dict[str, Any]
) -> bool:
    record_value = record.get(rule_key) if isinstance(record, dict) else None
    if isinstance(rule_value, list):
        if isinstance(rule_value[0], (str, int)):
            return record_value in rule_value
        if isinstance(rule_value[0], dict):
            filter_functions = {
                "anything-but": lambda rv, fv: rv not in fv,
                "exists": lambda rv, fv: bool(rv) == fv,
                "prefix": lambda rv, fv: (
                    isinstance(rv, str) and rv.startswith(fv)
                ),
            }
            for key, filter_value in rule_value[0].items():
                if filter_functions[key](record_value, filter_value):
                    return True
            return False
    if isinstance(rule_value, dict):
        return matches_pattern(rule_value, record_value)
    return False


def matches_pattern(pattern: dict[str, Any], record: dict[str, Any]) -> bool:
    rules = pattern.items()
    return all(matches_rule(key, value, record) for key, value in rules)


@dataclass
class LambdaContext:
    def get_remaining_time_in_millis(self) -> int:
        return 0


class RecordProcessor(processor.RecordProcessorBase):
    def __init__(self) -> None:
        self.checkpoint_retries = 5
        self.sleep_seconds = 5

    def initialize(self, _initialize_input: InitializeInput) -> None:
        """Called by the KCL when the worker has been instanced"""
        Path("/tmp/integrates-streams-ready").touch()

    def checkpoint(
        self,
        checkpointer: Checkpointer,
        sequence_number: str | None = None,
        sub_sequence_number: int | None = None,
    ) -> None:
        """Keep track of progress so the KCL can pick up from there later"""
        retries = 0
        last_exception = None

        while retries < self.checkpoint_retries:
            try:
                checkpointer.checkpoint(sequence_number, sub_sequence_number)
                return
            except CheckpointError as ex:
                if ex.value == "ShutdownException":
                    LOGGER.info("Shutting down, skipping checkpoint.")
                    return

                if ex.value == "ThrottlingException":
                    LOGGER.info(
                        "Checkpoint throttled, waiting %s seconds.",
                        self.sleep_seconds,
                    )
                else:
                    last_exception = ex

            retries += 1
            sleep(self.sleep_seconds)

        LOGGER.error(
            "Couldn't checkpoint after %s retries",
            self.checkpoint_retries,
            exc_info=last_exception,
        )

    def process_records(
        self, process_records_input: ProcessRecordsInput
    ) -> None:
        """Called by the KCL when new records are read from the stream"""
        kcl_records: list[KCLRecord] = process_records_input.records
        records = tuple(
            json.loads(record.binary_data.decode("utf-8"))
            for record in kcl_records
        )

        for trigger_name, trigger in TRIGGERS.items():
            if not trigger["enabled"]:
                LOGGER.warning("Skipping disabled trigger: %s", trigger_name)
                continue

            matching_records = tuple(
                record
                for record in records
                if any(
                    matches_pattern(pattern, record)
                    for pattern in trigger["filters"]
                )
            )

            if matching_records:
                try:
                    name = f'streams.{trigger["handler"]}'
                    module_name, function_name = name.rsplit(".", 1)
                    module = importlib.import_module(module_name)
                    handler = getattr(module, function_name)
                    LOGGER.info("Invoking %s", name)
                    handler({"Records": matching_records}, LambdaContext())
                # Must keep going even if one processor fails
                # pylint: disable-next=broad-except
                except Exception as ex:
                    LOGGER.info(
                        "Unexpected error in streams consumer",
                        exc_info=ex,
                    )

        self.checkpoint(
            process_records_input.checkpointer,
            kcl_records[-1].sequence_number,
            kcl_records[-1].sub_sequence_number,
        )

    def shutdown(self, shutdown_input: ShutdownInput) -> None:
        """Called by the KCL when the worker will shutdown"""
        Path("/tmp/integrates-streams-ready").unlink()
        if shutdown_input.reason == "TERMINATE":
            self.checkpoint(shutdown_input.checkpointer)


def consume() -> None:
    """Consumes the DynamoDB stream"""
    try:
        kclprocess = KCLProcess(RecordProcessor())
        kclprocess.run()
    except KeyboardInterrupt:
        LOGGER.info("Shutting down")
    except BrokenPipeError:
        LOGGER.info("Closed connection")


if __name__ == "__main__":
    consume()
