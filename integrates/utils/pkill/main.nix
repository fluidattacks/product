{ inputs, makeScript, ... }:
makeScript {
  name = "integrates-pkill";
  searchPaths.bin = [ inputs.nixpkgs.procps ];
  entrypoint = ./entrypoint.sh;
}
