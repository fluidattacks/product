# shellcheck shell=bash

function main {
  : \
    && data-for-db \
    && dynamodb-for-db \
    && rm -rf integrates/db/.data \
    || return 1
}

main "${@}"
