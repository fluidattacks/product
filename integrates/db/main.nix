{ inputs, makeScript, outputs, ... }:
let
  dbs = [
    outputs."/integrates/db/dynamodb"
    outputs."/integrates/db/opensearch"
    outputs."/integrates/streams/local"
  ];
in makeScript {
  name = "integrates-db";
  searchPaths = {
    bin = [ inputs.nixpkgs.gawk inputs.nixpkgs.gnugrep inputs.nixpkgs.procps ]
      ++ dbs;
  };
  entrypoint = ./entrypoint.sh;
}
