{ inputs, isLinux, makeScript, managePorts, ... }:
let
  # Keep in sync with infra/src/search.tf -> engine_version
  version = "2.15.0";
  opensearch = inputs.nixpkgs.opensearch.overrideAttrs (old: {
    inherit version;
    # https://github.com/NixOS/nixpkgs/issues/237057
    installPhase =
      builtins.replaceStrings [ "rm $out/bin/opensearch-cli" ] [ "" ]
      old.installPhase;
    src = inputs.nixpkgs.fetchurl {
      url =
        "https://artifacts.opensearch.org/releases/core/opensearch/${version}/opensearch-min-${version}-linux-arm64.tar.gz";
      sha256 = "sha256-0iHUgMonItcYzLLQ0tuUr5C3VLzaJK4PUjNE2hwUTEI=";
    };
  });
in makeScript {
  entrypoint = ./entrypoint.sh;
  name = "opensearch";
  replace = {
    __argIsLinux__ = builtins.toString isLinux;
    __argOpensearch__ = opensearch;
  };
  searchPaths = {
    bin = [ inputs.nixpkgs.gnused opensearch ]
      ++ inputs.nixpkgs.lib.optionals isLinux [ inputs.nixpkgs.su-exec ];
    source = [ managePorts ];
  };
}
