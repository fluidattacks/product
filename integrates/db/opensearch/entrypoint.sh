# shellcheck shell=bash

function initialize_config {
  : && cp \
    --no-preserve=mode \
    --force \
    --recursive \
    __argOpensearch__/config "${STATE}/config" \
    && sed \
      -e "s|logs/gc.log|${STATE}/logs/gc.log|g" \
      -i "${STATE}/config/jvm.options" \
    && mkdir -p "${STATE}/logs"
}

function serve {
  export OPENSEARCH_PATH_CONF="${STATE}/config"

  : \
    && info "Launching OpenSearch" \
    && initialize_config \
    && if test -n "__argIsLinux__" && [ "${EUID}" -eq 0 ]; then
      mkdir -p "${STATE}/tmp" \
        && chown -R makes:makes "${STATE}" "/tmp" \
        && export OPENSEARCH_TMPDIR="${STATE}/tmp" \
        && su-exec makes opensearch \
          -Epath.data="${STATE}/data" \
          -Epath.logs="${STATE}/logs" \
          -Ehttp.port=9200
    else
      opensearch \
        -Epath.data="${STATE}/data" \
        -Epath.logs="${STATE}/logs" \
        -Ehttp.port=9200
    fi
}

function serve_daemon {
  : \
    && kill_port "9200" \
    && { serve "${@}" & } \
    && wait_port 300 "0.0.0.0:9200" \
    && info "Opensearch is ready"
}

function main {
  : \
    && if [ "${DAEMON-}" = "true" ]; then
      serve_daemon "${@}"
    else
      serve "${@}"
    fi
}

main "${@}"
