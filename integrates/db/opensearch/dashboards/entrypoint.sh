# shellcheck shell=bash

function initialize_config {
  : && cp \
    --no-preserve=mode \
    --force \
    --recursive \
    __argOpensearchDashboards__/config "${STATE}/config" \
    && echo "path.data: ${STATE}/data" >> "${STATE}/config/opensearch_dashboards.yml"
}

function main {
  export OPENSEARCH_DASHBOARDS_PATH_CONF="${STATE}/config"

  : && info "Launching OpenSearch Dashboards" \
    && initialize_config \
    && opensearch-dashboards
}

main "${@}"
