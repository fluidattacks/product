# shellcheck shell=bash

function start {
  : && { DAEMON=true dynamodb & } \
    && { DAEMON=true opensearch & } \
    && wait \
    && if [ "${STREAMS:-true}" = "true" ]; then
      integrates-streams
    fi
}

function stop_process {
  local process_name="${1}"
  local process
  # neither pgrep nor pkill are available on procps for mac
  # shellcheck disable=SC2009
  process=$(ps aux | grep "${process_name}" | grep -v "grep" | awk '{print $2}')

  if [ -n "${process}" ]; then
    kill "${process}"
  fi
}

function stop {
  : && info "Stopping dynamodb" \
    && stop_process "java.*DynamoDBLocal" \
    && info "Stopping opensearch" \
    && stop_process "java.*OpenSearch" \
    && info "Stopping streams" \
    && stop_process "java.*StreamsMultiLangDaemon"
}

function main {
  local action="${1:-start}"

  case "${action}" in
    start) start "${@}" ;;
    stop) stop "${@}" ;;
    *) error 'First argument must be one of: start, stop' ;;
  esac
}

main "${@}"
