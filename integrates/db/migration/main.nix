{ makeScript, outputs, ... }:
makeScript {
  entrypoint = ./entrypoint.sh;
  name = "integrates-db-migration";
  replace.__argIntegratesBackEnv__ = outputs."/integrates/back/env";
}
