# shellcheck shell=bash

function main {
  local default_args=(
    "/${CI_COMMIT_REF_NAME}"
    "--exclude" "${CI_COMMIT_REF_NAME}-batch/*"
    "--exclude" "${CI_COMMIT_REF_NAME}-charts-documents/*"
    "--exclude" "${CI_COMMIT_REF_NAME}-charts-snapshots/*"
    "--exclude" "${CI_COMMIT_REF_NAME}-subscriptions-analytics/*"
    "--exclude" "${CI_COMMIT_REF_NAME}-test-functional-*/*"
    "--exclude" "${CI_COMMIT_REF_NAME}-test-unit-*/*"
  )

  if [[ ${1-} == "--no-size-only" ]]; then
    populate_storage "--no-size-only" "${default_args[@]}"
  else
    populate_storage "${default_args[@]}"
  fi
}

main "${@}"
