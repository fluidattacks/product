resource "kubernetes_deployment_v1" "worker" {
  metadata {
    name      = "integrates-tasks-${var.deployment_name}"
    namespace = "prod-integrates"
  }

  spec {
    replicas = 1

    selector {
      match_labels = {
        app = "integrates-tasks-${var.deployment_name}"
      }
    }

    strategy {
      rolling_update {
        max_surge       = "10%"
        max_unavailable = "10%"
      }
    }

    template {
      metadata {
        labels = {
          app = "integrates-tasks-${var.deployment_name}"
        }
      }

      spec {
        automount_service_account_token = false
        service_account_name            = "prod-integrates"
        node_selector = {
          "worker_group" = "prod_skims"
        }

        container {
          name              = "tasks-worker"
          image             = "ghcr.io/fluidattacks/makes:24.12@sha256:26cd81b352b908bc47f76b8a5d94cf40a42a2bef952b47cc59f6ebb439589194"
          image_pull_policy = "Always"

          command = ["sh"]
          args    = ["-c", "m gitlab:fluidattacks/universe@${var.ci_commit_sha} /integrates/jobs/server_async"]

          env {
            name  = "MAKES_K8S_COMPAT"
            value = "1"
          }
          resources {
            requests = {
              cpu    = "500m"
              memory = "6000Mi"
            }
            limits = {
              cpu    = "1000m"
              memory = "7000Mi"
            }
          }

          env_from {
            secret_ref {
              name = "integrates-${var.deployment_name}"
            }
          }

          # NOFLUID makes container has to run as root to function correctly
          security_context {
            allow_privilege_escalation = false
            privileged                 = false
            # NOFLUID makes container requires write permission to the root filesystem to function correctly
            read_only_root_filesystem = false
          }

        }

        termination_grace_period_seconds = 120
      }
    }
  }
}
