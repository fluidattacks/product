# shellcheck shell=bash

function main {
  local env="${1-}"

  : && case "${env}" in
    dev) aws_login "dev" "3600" ;;
    eph) : ;;
    prod) : ;;
    prod-local) aws_login "prod_integrates" "3600" ;;
    *) error 'First argument must be one of: dev, eph, prod, prod-local' ;;
  esac \
    && case "${env}" in
      dev)
        : && sops_export_vars __argSecretsDev__ "${INTEGRATES_SECRETS_LIST[@]}" \
          && sops_export_vars __argSecretsDev__ "${INTEGRATES_COMMON_SECRETS_LIST[@]}" \
          && devpwd="$(pwd)" \
          && export PYTHONPATH="${devpwd}/integrates/back:${PYTHONPATH:-}" \
          && export AWS_S3_PATH_PREFIX="${CI_COMMIT_REF_NAME}/" \
          && export ENVIRONMENT="development" \
          && export PYTHONDEVMODE=1
        ;;
      eph)
        : && sops_export_vars __argSecretsDev__ "${INTEGRATES_SECRETS_LIST[@]}" \
          && sops_export_vars __argSecretsDev__ "${INTEGRATES_COMMON_SECRETS_LIST[@]}" \
          && export AWS_S3_PATH_PREFIX="${CI_COMMIT_REF_NAME}/" \
          && export ENVIRONMENT="ephemeral"
        ;;
      prod)
        : && sops_export_vars __argSecretsProd__ "${INTEGRATES_SECRETS_LIST[@]}" \
          && sops_export_vars __argSecretsDev__ "${INTEGRATES_COMMON_SECRETS_LIST[@]}" \
          && export ENVIRONMENT="production"
        ;;
      prod-local)
        : && sops_export_vars __argSecretsProd__ "${INTEGRATES_SECRETS_LIST[@]}" \
          && sops_export_vars __argSecretsDev__ "${INTEGRATES_COMMON_SECRETS_LIST[@]}" \
          && export ENVIRONMENT="production" \
          && export DEBUG=True
        ;;
      *) error 'First argument must be one of: dev, eph, prod, prod-local' ;;
    esac \
    && export CI_COMMIT_REF_NAME \
    && export CI_COMMIT_SHA \
    && export GIT_TERMINAL_PROMPT=0 \
    && export MACHINE_FINDINGS='__argManifestFindings__' \
    && export INTEGRATES_DB_MODEL_PATH='__argDbDesign__' \
    && export ASYNC_PROCESSING_DB_MODEL_PATH='__argIntegrates__/back/integrates/batch/fi_async_processing-design.json' \
    && export LLM_SCAN_DB_MODEL_PATH='__argIntegrates__/back/integrates/batch/llm_scan-design.json' \
    && export INTEGRATES_REPORTS_LOGO_PATH='__argIntegrates__/back/integrates/reports/resources/themes/background.png' \
    && export INTEGRATES_MAILER_TEMPLATES='__argIntegrates__/back/integrates/mailer/email_templates' \
    && export INTEGRATES_CRITERIA_COMPLIANCE='__argCriteriaCompliance__' \
    && export INTEGRATES_CRITERIA_REQUIREMENTS='__argCriteriaRequirements__' \
    && export INTEGRATES_CRITERIA_VULNERABILITIES='__argCriteriaVulnerabilities__' \
    && export INTEGRATES_QUEUE_SIZES='__argQueueSize__' \
    && export INTEGRATES_TREE_SITTER_PARSERS='__argSrcTreeSitterParsers__' \
    && export TREE_SITTER_PARSERS='__argSrcTreeSitterParsers__' \
    && export SKIMS_FLUID_WATERMARK='__argSrcMachineAssets__/logo_img/logo_fluid_attacks_854x329.png' \
    && export SKIMS_ROBOTO_FONT='__argSrcMachineAssets__/fonts/roboto_mono_from_google/regular.ttf' \
    && export STARTDIR="${PWD}" \
    && export TIKTOKEN_CACHE_DIR='__argTiktokenDir__' \
    && export TZ=UTC \
    && if test -z "${CI_COMMIT_REF_NAME-}"; then
      # Local environments specific
      CI_COMMIT_REF_NAME="$(get_abbrev_rev . HEAD)"
    fi \
    && if test -z "${CI_COMMIT_SHA-}"; then
      # Local environments specific
      CI_COMMIT_SHA="$(get_commit_from_rev . HEAD)"
    fi \
    && if ! test -e 'integrates'; then
      # Kubernetes specific
      : && mkdir 'integrates' \
        && copy '__argIntegrates__' 'integrates'
    fi
}

main "${@}"
