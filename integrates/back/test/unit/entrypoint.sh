# shellcheck shell=bash

function main {
  local test_group="${1}"
  local test_path="${2-}"
  local pytest_args=(
    --cov "back/integrates"
    --cov-branch
    --cov-config "back/test/unit/settings.cfg"
    --cov-report "term"
    --cov-report "html:build/coverage/html"
    --cov-report "xml:coverage.xml"
    --cov-report "annotate:build/coverage/annotate"
    --cov-report "term-missing"
    --disable-warnings
    --no-cov-on-fail
    -vvl
  )
  local marker
  export COVERAGE_FILE=".coverage.unit_${test_group}"
  export AWS_S3_PATH_PREFIX

  if [ "${test_group}" = "changes_db" ]; then
    marker="changes_db"
  elif [ "${test_group}" = "not_changes_db" ]; then
    marker="not changes_db"
  else
    error "${test_group} is not a valid argument"
  fi

  : && info "Executing unit tests for ${test_group}" \
    && DAEMON=true integrates-db \
    && trap "integrates-db stop" "EXIT" \
    && AWS_S3_PATH_PREFIX="${CI_COMMIT_REF_NAME}-test-unit-${test_group}/" \
    && populate_storage "/${CI_COMMIT_REF_NAME}-test-unit-${test_group}" \
    && source __argIntegratesBackEnv__/template "dev" \
    && pushd integrates \
    && PYTHONPATH="back/migrations/:${PYTHONPATH}" \
    && pytest -m "${marker}" "${pytest_args[@]}" "back/test/unit/src/${test_path}" \
    && popd \
    || return 1
}

main "${@}"
