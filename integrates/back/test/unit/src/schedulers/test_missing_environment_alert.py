from integrates.dataloaders import (
    get_new_context,
)
from integrates.schedulers.missing_environment_alert import (
    _send_mail_report as send_mail_missing_environment,
    has_environment,
    main,
    missing_environment_alert,
)
import pytest
from test.unit.src.utils import (
    get_module_at_test,
)
from typing import (
    Any,
)
from unittest.mock import (
    AsyncMock,
    patch,
)

MODULE_AT_TEST = f"integrates.{get_module_at_test(file_path=__file__)}"

pytestmark = [
    pytest.mark.asyncio,
]


@pytest.mark.asyncio
async def test_send_mail_missing_environment() -> None:
    await send_mail_missing_environment(
        loaders=get_new_context(),
        group="unittesting",
        group_date_delta=3,
    )


@pytest.mark.asyncio
async def test_has_environment() -> None:
    loaders = get_new_context()
    group = "unittesting"
    test = await has_environment(loaders, group)
    assert test is True


@patch(MODULE_AT_TEST + "FI_ENVIRONMENT", "production")
@patch(
    MODULE_AT_TEST + "orgs_domain.get_all_active_groups",
    new_callable=AsyncMock,
)
async def test_missing_environment_alert(
    mock_orgs_domain_get_all_active_groups: AsyncMock,
    mocked_data_for_module: Any,
) -> None:
    mock_orgs_domain_get_all_active_groups.return_value = (
        mocked_data_for_module(
            mock_path="orgs_domain.get_all_active_groups",
            mock_args=[],
            module_at_test=MODULE_AT_TEST,
        )
    )

    await missing_environment_alert()
    assert mock_orgs_domain_get_all_active_groups.called is True


@pytest.mark.asyncio
async def test_main() -> None:
    with patch(
        MODULE_AT_TEST + "missing_environment_alert",
        new_callable=AsyncMock,
    ) as mock:
        await main()
        mock.assert_awaited_once()
