from datetime import datetime
from unittest.mock import Mock, patch

import pytest

from integrates.db_model.toe_lines.types import ToeLine, ToeLineState
from integrates.schedulers.metrics_for_toe_lines_accuracy import (
    calculate_accuracy_by_group,
    calculate_accuracy_by_toe_line,
    get_accuracy,
    send_metric_to_cloudwatch,
)
from test.unit.src.utils import MockedInstance, get_module_at_test

pytestmark = [
    pytest.mark.asyncio,
]

MODULE_AT_TEST = f"integrates.{get_module_at_test(file_path=__file__)}"

pytestmark = [
    pytest.mark.asyncio,
]


@pytest.mark.parametrize(
    ["toe_line", "expected_output"],
    [
        (
            ToeLine(
                filename="nickname/back/src/model/user/index.js",
                group_name="group1",
                root_id="88637616-41d4-4242-854a-db8ff7fe1ab6",
                state=ToeLineState(
                    attacked_at=None,
                    attacked_by="machine@fluidattacks.com",
                    be_present=True,
                    be_present_until=None,
                    comments="",
                    first_attack_at=None,
                    has_vulnerabilities=False,
                    last_author="customer1@gmail.com",
                    last_commit="f9e4beba70c4f34d6117c3b0c23ebe6b2bff66c1",
                    last_commit_date=datetime.fromisoformat("2020-11-16T15:41:04+00:00"),
                    attacked_lines=23,
                    loc=44,
                    modified_by="machine@fluidattacks.com",
                    modified_date=datetime.fromisoformat("2020-11-16T15:41:04+00:00"),
                    seen_at=datetime.fromisoformat("2020-01-01T15:41:04+00:00"),
                    sorts_risk_level=0,
                ),
            ),
            (1, 1),
        ),
        (
            ToeLine(
                filename="nickname/back/src/model/user/index.js",
                group_name="group1",
                root_id="88637616-41d4-4242-854a-db8ff7fe1ab6",
                state=ToeLineState(
                    attacked_at=None,
                    attacked_by="machine@fluidattacks.com",
                    be_present=True,
                    be_present_until=None,
                    comments="",
                    first_attack_at=None,
                    has_vulnerabilities=False,
                    last_author="customer1@gmail.com",
                    last_commit="f9e4beba70c4f34d6117c3b0c23ebe6b2bff66c1",
                    last_commit_date=datetime.fromisoformat("2020-11-16T15:41:04+00:00"),
                    attacked_lines=53,
                    loc=44,
                    modified_by="machine@fluidattacks.com",
                    modified_date=datetime.fromisoformat("2020-11-16T15:41:04+00:00"),
                    seen_at=datetime.fromisoformat("2020-01-01T15:41:04+00:00"),
                    sorts_risk_level=0,
                ),
            ),
            (0, 1),
        ),
    ],
)
async def test_calculate_accuracy_by_toe_line(
    toe_line: ToeLine,
    expected_output: tuple[int, int],
) -> None:
    result = await calculate_accuracy_by_toe_line(toe_line=toe_line)
    assert result == expected_output


async def test_get_accuracy() -> None:
    assert str(get_accuracy(0, 0)) == "0"
    assert str(get_accuracy(1, 2)) == "50.0"


@pytest.mark.parametrize(
    ["group_name", "expected_output"],
    [
        (
            "unittesting",
            (11, 11),
        ),
    ],
)
async def test_calculate_accuracy_by_group(
    group_name: str, expected_output: tuple[int, int]
) -> None:
    result = await calculate_accuracy_by_group(group_name, 100)
    assert result == expected_output


async def test_send_metric_to_cloudwatch() -> None:
    mock_boto3 = Mock(
        return_value=MockedInstance(
            put_metric_data=Mock(
                return_value=lambda *_, **__: None,
            ),
        ),
    )
    with patch("boto3.client", mock_boto3) as mock_boto3:
        send_metric_to_cloudwatch(0.0)
        mock_boto3.assert_called_with("cloudwatch", "us-east-1")
