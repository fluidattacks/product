from datetime import (
    datetime,
)
from decimal import (
    Decimal,
)
from freezegun import (
    freeze_time,
)
from integrates.custom_exceptions import (
    ExpiredToken,
    InvalidAuthorization,
    OrganizationNotFound,
)
from integrates.dataloaders import (
    get_new_context,
)
from integrates.db_model.groups.enums import (
    GroupLanguage,
    GroupManaged,
    GroupService,
    GroupStateStatus,
    GroupSubscriptionType,
    GroupTier,
)
from integrates.db_model.groups.types import (
    Group,
    GroupState,
)
from integrates.db_model.types import (
    Policies,
)
from integrates.schedulers.devsecops_expiration_notification import (
    _get_near_expiration_emails,
    ForcesDataType,
    get_organization_name,
    main,
    send_devsecops_expiration,
    seven_days_left,
    unique_emails,
)
import pytest
from pytz import (
    UTC,
)
from test.unit.src.utils import (
    get_module_at_test,
)
from typing import (
    Any,
)
from unittest.mock import (
    AsyncMock,
    Mock,
    patch,
)

MODULE_AT_TEST = f"integrates.{get_module_at_test(file_path=__file__)}"

pytestmark = [
    pytest.mark.asyncio,
]


@pytest.mark.asyncio
@pytest.mark.parametrize(
    [
        "organization_id",
    ],
    [
        [
            "ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3",
        ],
    ],
)
async def test_get_organization_name(
    organization_id: str,
) -> None:
    org_names = await get_organization_name(get_new_context(), organization_id)
    assert org_names == "okada"


@pytest.mark.asyncio
@pytest.mark.parametrize(
    [
        "organization_id",
    ],
    [
        [
            "ORG#46eb8f25-7945-4173-ab6e-0af4ca5b7ef3",
        ],
    ],
)
async def test_get_organization_name_raise(
    organization_id: str,
) -> None:
    with pytest.raises(OrganizationNotFound):
        await get_organization_name(get_new_context(), organization_id)


@pytest.mark.parametrize(
    [
        "group",
    ],
    [
        [
            Group(
                created_by="unknown",
                created_date=datetime.fromisoformat(
                    "2019-01-20T22:00:00+00:00"
                ),
                description="oneshot testing",
                language=GroupLanguage.EN,
                name="oneshottest",
                organization_id="ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3",
                state=GroupState(
                    has_essential=True,
                    has_advanced=False,
                    managed=GroupManaged.NOT_MANAGED,
                    modified_by="unknown",
                    modified_date=datetime.fromisoformat(
                        "2019-01-20T22:00:00+00:00"
                    ),
                    status=GroupStateStatus.ACTIVE,
                    tier=GroupTier.ONESHOT,
                    type=GroupSubscriptionType.ONESHOT,
                    tags={
                        "test-tag",
                        "test-updates",
                        "another-tag",
                        "test-groups",
                    },
                    comments=None,
                    justification=None,
                    payment_id=None,
                    pending_deletion_date=None,
                    service=GroupService.BLACK,
                ),
                agent_token=None,
                business_id="14441323",
                business_name="Testing Company and Sons",
                context=None,
                disambiguation=None,
                files=None,
                policies=Policies(
                    modified_date=datetime.fromisoformat(
                        "2021-11-22T20:07:57+00:00"
                    ),
                    modified_by="integratesmanager@gmail.com",
                    inactivity_period=None,
                    max_acceptance_days=90,
                    max_acceptance_severity=Decimal("3.9"),
                    max_number_acceptances=3,
                    min_acceptance_severity=Decimal("0"),
                    min_breaking_severity=Decimal("3.9"),
                    vulnerability_grace_period=10,
                ),
                sprint_duration=2,
                sprint_start_date=datetime.fromisoformat(
                    "2022-04-18T00:00:00"
                ),
            ),
        ],
        [
            Group(
                agent_token=(
                    "eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJjaXBABCXYZ"
                ),
                context="This is a dummy context",
                created_by="integratesuser@gmail.com",
                created_date=datetime.fromisoformat(
                    "2020-05-20T22:00:00+00:00"
                ),
                description="this is group1",
                language=GroupLanguage.EN,
                name="unittesting",
                state=GroupState(
                    has_essential=True,
                    has_advanced=True,
                    managed=GroupManaged["MANAGED"],
                    modified_by="unknown",
                    modified_date=datetime.fromisoformat(
                        "2020-05-20T22:00:00+00:00"
                    ),
                    status=GroupStateStatus.ACTIVE,
                    tags={"testing"},
                    tier=GroupTier.ADVANCED,
                    type=GroupSubscriptionType.CONTINUOUS,
                    service=GroupService.WHITE,
                ),
                organization_id="ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3",
                business_id="1867",
                business_name="Testing Company",
                sprint_duration=3,
                sprint_start_date=datetime.fromisoformat(
                    "2022-06-06T00:00:00+00:00"
                ),
            ),
        ],
    ],
)
@patch(MODULE_AT_TEST + "forces_domain.get_expiration_date")
@patch(
    MODULE_AT_TEST + "get_group_emails_by_notification", new_callable=AsyncMock
)
@freeze_time("2023-09-18T10:00:00.0")
async def test_get_near_expiration_emails(
    mock_get_group_emails_by_notification: AsyncMock,
    mock_get_expiration_date: Mock,
    group: Group,
) -> None:
    if not group.agent_token:
        assert (
            await _get_near_expiration_emails(
                group=group, loaders=get_new_context()
            )
            is None
        )
    else:
        mock_get_expiration_date.return_value = datetime.fromisoformat(
            "2023-09-25T10:00:00+00:00"
        )
        mock_get_group_emails_by_notification.return_value = [
            "integratesuser@gmail.com",
        ]

        result = await _get_near_expiration_emails(
            group=group, loaders=get_new_context()
        )
        groups_data = {
            "unittesting": {
                "email_to": ("integratesuser@gmail.com",),
                "org_name": "okada",
                "exp_date": datetime(2023, 9, 25, 10, 0, 0, tzinfo=UTC),
            },
        }

        assert result is not None
        assert result == groups_data


@pytest.mark.parametrize(
    [
        "group",
    ],
    [
        [
            Group(
                agent_token=(
                    "eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJjaXBABCXYZ"
                ),
                context="This is a dummy context",
                created_by="integratesuser@gmail.com",
                created_date=datetime.fromisoformat(
                    "2020-05-20T22:00:00+00:00"
                ),
                description="this is group1",
                language=GroupLanguage.EN,
                name="unittesting",
                state=GroupState(
                    has_essential=True,
                    has_advanced=True,
                    managed=GroupManaged["MANAGED"],
                    modified_by="unknown",
                    modified_date=datetime.fromisoformat(
                        "2020-05-20T22:00:00+00:00"
                    ),
                    status=GroupStateStatus.ACTIVE,
                    tags={"testing"},
                    tier=GroupTier.ADVANCED,
                    type=GroupSubscriptionType.CONTINUOUS,
                    service=GroupService.WHITE,
                ),
                organization_id="ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3",
                business_id="1867",
                business_name="Testing Company",
                sprint_duration=3,
                sprint_start_date=datetime.fromisoformat(
                    "2022-06-06T00:00:00+00:00"
                ),
            ),
        ],
    ],
)
@patch(MODULE_AT_TEST + "forces_domain.get_expiration_date")
@freeze_time("2023-09-25T10:00:00+00:00")
async def test_get_near_expiration_emails_expired(
    mock_get_expiration_date: Mock,
    group: Group,
) -> None:
    result: dict[str, ForcesDataType] | None = {}
    mock_get_expiration_date.side_effect = InvalidAuthorization()

    try:
        result = await _get_near_expiration_emails(
            group=group, loaders=get_new_context()
        )
    except (ExpiredToken, InvalidAuthorization):
        assert result is None


@pytest.mark.parametrize(
    ["groups_data"],
    [
        [
            {
                "oneshottest": {
                    "org_name": "okada",
                    "email_to": (
                        "continuoushack2@gmail.com",
                        "customer_manager@fluidattacks.com",
                        "integratesmanager@fluidattacks.com",
                        "integratesmanager@gmail.com",
                        "integratesresourcer@fluidattacks.com",
                        "integratesuser2@gmail.com",
                        "integratesuser@gmail.com",
                    ),
                    "exp_date": datetime(2023, 7, 23, 22, 0, 0, tzinfo=UTC),
                },
                "unittesting": {
                    "org_name": "okada",
                    "email_to": (
                        "continuoushack2@gmail.com",
                        "continuoushacking@gmail.com",
                        "integratesmanager@fluidattacks.com",
                        "integratesmanager@gmail.com",
                        "integratesresourcer@fluidattacks.com",
                        "integratesuser2@gmail.com",
                        "unittest2@fluidattacks.com",
                    ),
                    "exp_date": datetime(2023, 7, 24, 22, 0, 0, tzinfo=UTC),
                },
            }
        ],
    ],
)
@freeze_time("2023-07-25T10:00:00.0")
def test_unique_emails(
    groups_data: dict[str, ForcesDataType],
) -> None:
    emails = unique_emails(dict(groups_data), ())
    assert len(emails) == 9


@pytest.mark.parametrize(
    ["exp_date"],
    [
        [
            datetime(2022, 7, 23, 22, 0, 0, tzinfo=UTC),
        ],
    ],
)
@freeze_time("2022-07-23T10:00:00.0")
def test_seven_days_left(
    exp_date: datetime,
) -> None:
    result = seven_days_left(exp_date)
    assert not result


@patch(MODULE_AT_TEST + "mail_forces_expiration", new_callable=AsyncMock)
@patch(MODULE_AT_TEST + "unique_emails")
@patch(MODULE_AT_TEST + "_get_near_expiration_emails", new_callable=AsyncMock)
@patch(
    MODULE_AT_TEST + "orgs_domain.get_all_active_groups",
    new_callable=AsyncMock,
)
@freeze_time("2023-09-18T10:00:00.0")
async def test_send_devsecops_expiration(
    mock_orgs_domain_get_all_active_groups: AsyncMock,
    mock_get_near_expiration_emails: AsyncMock,
    mock_unique_emails: Mock,
    mock_mail_forces_expiration: AsyncMock,
    mocked_data_for_module: Any,
) -> None:
    email: str = "integratesuser@gmail.com"
    mock_orgs_domain_get_all_active_groups.return_value = (
        mocked_data_for_module(
            mock_path="orgs_domain.get_all_active_groups",
            mock_args=[],
            module_at_test=MODULE_AT_TEST,
        )
    )

    # Set up mock's side_effect using mock_data_for_module fixture
    mock_get_near_expiration_emails.side_effect = mocked_data_for_module(
        mock_path="_get_near_expiration_emails",
        mock_args=[],
        module_at_test=MODULE_AT_TEST,
    )

    mock_unique_emails.return_value = [email]
    mock_mail_forces_expiration.return_value = None

    with patch(MODULE_AT_TEST + "FI_ENVIRONMENT", "production"):
        await send_devsecops_expiration()
        mock_get_near_expiration_emails.assert_not_called()

    await send_devsecops_expiration()
    assert mock_orgs_domain_get_all_active_groups.called is True
    mock_get_near_expiration_emails.assert_called()
    mock_unique_emails.assert_called()
    mock_mail_forces_expiration.assert_awaited()


@pytest.mark.asyncio
async def test_main() -> None:
    with patch(
        MODULE_AT_TEST + "send_devsecops_expiration",
        new_callable=AsyncMock,
    ) as mock:
        await main()
        mock.assert_awaited_once()
