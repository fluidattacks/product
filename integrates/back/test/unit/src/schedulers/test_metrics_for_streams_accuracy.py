from unittest.mock import Mock, patch

import pytest

from integrates.dataloaders import Dataloaders, get_new_context
from integrates.schedulers.metrics_for_streams_accuracy import (
    calculate_accuracy_by_finding,
    calculate_accuracy_by_group,
    get_accuracy,
    send_metric_to_cloudwatch,
)
from test.unit.src.utils import MockedInstance, get_module_at_test

pytestmark = [
    pytest.mark.asyncio,
]

MODULE_AT_TEST = f"integrates.{get_module_at_test(file_path=__file__)}"

pytestmark = [
    pytest.mark.asyncio,
]


async def test_get_accuracy() -> None:
    assert str(get_accuracy(0, 0)) == "0"
    assert str(get_accuracy(1, 2)) == "50.0"


@pytest.mark.parametrize(
    ["finding_id", "expected_output"],
    [
        ("151213607", (27, 27)),
        ("422286126", (25, 27)),
        ("436992569", (26, 27)),
        ("457497316", (27, 27)),
        ("463461507", (27, 27)),
    ],
)
async def test_calculate_accuracy_by_finding(
    finding_id: str,
    expected_output: tuple[int, int],
) -> None:
    loaders: Dataloaders = get_new_context()
    finding = await loaders.finding.load(finding_id)
    assert finding

    result = await calculate_accuracy_by_finding(
        loaders=loaders,
        finding=finding,
    )
    assert result == expected_output


@pytest.mark.parametrize(
    ["group_name", "expected_output"],
    [
        (
            "unittesting",
            (313, 324),
        ),
    ],
)
async def test_calculate_accuracy_by_group(
    group_name: str, expected_output: tuple[int, int]
) -> None:
    result = await calculate_accuracy_by_group(group_name, 100)
    assert result == expected_output


async def test_send_metric_to_cloudwatch() -> None:
    mock_boto3 = Mock(
        return_value=MockedInstance(
            put_metric_data=Mock(
                return_value=lambda *_, **__: None,
            ),
        ),
    )
    with patch("boto3.client", mock_boto3) as mock_boto3:
        send_metric_to_cloudwatch(0.0)
        mock_boto3.assert_called_with("cloudwatch", "us-east-1")
