import argparse
import json
import os
import typing
from collections.abc import Callable
from decimal import (
    Decimal,
)
from pathlib import (
    Path,
)
from typing import (
    Self,
)
from unittest.mock import (
    AsyncMock,
    MagicMock,
    patch,
)

import pytest
from fa_purity import (
    Result,
)
from freezegun import (
    freeze_time,
)

from integrates.class_types.types import (
    Item,
)
from integrates.db_model import (
    HISTORIC_TABLE as INTEGRATES_VMS_HISTORIC_TABLE,
)
from integrates.db_model import (
    TABLE as INTEGRATES_VMS_TABLE,
)
from integrates.dynamodb.types import (
    Table,
)
from integrates.schedulers.validate_db_schema import (
    _TARGET_TABLES,
    METADATA_FAILS_DIR,
    CueValidationError,
    InvalidSchemaBasePathError,
    JsonDBEncoder,
    UnknownTableError,
    _build_facet_schema_expression,
    _build_facet_schema_path,
    _find_facet,
    _handle_schema_base_arg,
    _handle_table_arg,
    _is_key_alias,
    _run_table_validation,
    _save_fail_metadata,
    _snake_to_pascal,
    _split_facets,
    _validate_schema,
    _validate_with_cue,
    main,
)
from test.unit.src.utils import (
    get_module_at_test,
)

MODULE_AT_TEST = f"integrates.{get_module_at_test(file_path=__file__)}"

pytestmark = [
    pytest.mark.asyncio,
]


class MockAsyncGenerator:
    def __init__(self, seq: list[Item]):
        self.iter = iter(seq)

    def __aiter__(self) -> Self:
        return self

    async def __anext__(self) -> Item:
        try:
            return next(self.iter)
        except StopIteration as exc:
            raise StopAsyncIteration from exc


def build_generator_mock(
    seq: list[Item],
) -> Callable[[Table], MockAsyncGenerator]:
    def generator_mock(  # pylint: disable=unused-argument
        table: Table,
    ) -> MockAsyncGenerator:
        return MockAsyncGenerator(seq)

    return generator_mock


@pytest.mark.parametrize(
    ["item_pk", "item_sk", "expected_facet"],
    [
        [
            "AWS_PRODUCT#be517c023bbd4f8b8f9b8f3bd",
            "AWS_CUSTOMER#D1Hv8MM4v6Q",
            "aws_marketplace_subscription_metadata",
        ],
        [
            "ORG#33c08ebd-2068-47e7-9673-e1aa03dc9448",
            "STATE#2019-11-22T20:07:57+00:00",
            "organization_historic_state",
        ],
        [
            "USER#integratesuser@gmail.com",
            "USER#integratesuser@gmail.com",
            "stakeholder_metadata",
        ],
        [
            "VULN#060b9ee2-cf52-4192-8822-a30420b6c34a",
            "FIN#aeb1a8c9-e0fc-44f6-b6a6-e6ccfc1997c1",
            "vulnerability_metadata",
        ],
    ],
)
def test_find_facet(
    item_pk: str,
    item_sk: str,
    expected_facet: str,
) -> None:
    facets = INTEGRATES_VMS_TABLE.facets
    result = _find_facet(facets=facets, item_pk=item_pk, item_sk=item_sk)
    assert result == expected_facet


@pytest.mark.parametrize(
    "snake_str,pascal_str",
    [
        [
            "one_example_string",
            "OneExampleString",
        ],
        [
            "some_really_long_and_complicated_snake_case_string",
            "SomeReallyLongAndComplicatedSnakeCaseString",
        ],
        [
            "snake",
            "Snake",
        ],
        [
            "snake_case",
            "SnakeCase",
        ],
    ],
)
def test_snake_to_pascal(snake_str: str, pascal_str: str) -> None:
    result = _snake_to_pascal(snake_str)
    assert result == pascal_str


@pytest.mark.parametrize(
    "content,expected_split_data,expected_non_matching",
    [
        (
            [
                {
                    "pk": "AWS_PRODUCT#be517c023bbd4f8b8f9b8f3bd",
                    "sk": "AWS_CUSTOMER#D1Hv8MM4v6Q",
                },
                {
                    "pk": "ORG#33c08ebd-2068-47e7-9673-e1aa03dc9448",
                    "sk": "STATE#2019-11-22T20:07:57+00:00",
                },
                {
                    "pk": "USER#integratesuser@gmail.com",
                    "sk": "USER#integratesuser@gmail.com",
                },
                {
                    "pk": "VULN#060b9ee2-cf52-4192-8822-a30420b6c34a",
                    "sk": "FIN#aeb1a8c9-e0fc-44f6-b6a6-e6ccfc1997c1",
                },
                {"pk": "UNKNOWN#unknown", "sk": "UNKNOWN#unknown"},
            ],
            {
                "aws_marketplace_subscription_metadata": [
                    {
                        "pk": "AWS_PRODUCT#be517c023bbd4f8b8f9b8f3bd",
                        "sk": "AWS_CUSTOMER#D1Hv8MM4v6Q",
                    }
                ],
                "organization_historic_state": [
                    {
                        "pk": "ORG#33c08ebd-2068-47e7-9673-e1aa03dc9448",
                        "sk": "STATE#2019-11-22T20:07:57+00:00",
                    }
                ],
                "stakeholder_metadata": [
                    {
                        "pk": "USER#integratesuser@gmail.com",
                        "sk": "USER#integratesuser@gmail.com",
                    }
                ],
                "vulnerability_metadata": [
                    {
                        "pk": "VULN#060b9ee2-cf52-4192-8822-a30420b6c34a",
                        "sk": "FIN#aeb1a8c9-e0fc-44f6-b6a6-e6ccfc1997c1",
                    }
                ],
            },
            [
                {"pk": "UNKNOWN#unknown", "sk": "UNKNOWN#unknown"},
            ],
        ),
    ],
)
def test_split_facets(
    content: list[Item],
    expected_split_data: dict[str, list[Item]],
    expected_non_matching: list[Item],
) -> None:
    table = INTEGRATES_VMS_TABLE
    split_data, non_matching_items = _split_facets(table, content)
    assert split_data == expected_split_data
    assert non_matching_items == expected_non_matching


@pytest.mark.parametrize(
    "facet_name, table, schemas_base, expected_path",
    [
        (
            "aws_marketplace_subscription_metadata",
            "integrates_vms",
            Path("/schemas"),
            Path("/schemas/tables/integrates_vms/facets/aws_marketplace_subscription_metadata.cue"),
        ),
        (
            "organization_historic_state",
            "integrates_vms",
            Path("/schemas"),
            Path("/schemas/tables/integrates_vms/facets/organization_historic_state.cue"),
        ),
    ],
)
def test_build_facet_schema_path(
    facet_name: str, table: str, schemas_base: Path, expected_path: Path
) -> None:
    result = _build_facet_schema_path(facet_name, table, schemas_base)
    assert result == expected_path


@pytest.mark.parametrize(
    "facet_name, expected_expression",
    [
        (
            "aws_marketplace_subscription_metadata",
            "[...#AwsMarketplaceSubscriptionItem]",
        ),
        (
            "organization_historic_state",
            "[...#OrganizationHistoricStateItem]",
        ),
        (
            "stakeholder_metadata",
            "[...#StakeholderItem]",
        ),
        (
            "vulnerability_metadata",
            "[...#VulnerabilityItem]",
        ),
    ],
)
def test_build_facet_schema_expression(facet_name: str, expected_expression: str) -> None:
    result = _build_facet_schema_expression(facet_name)
    assert result == expected_expression


@pytest.mark.parametrize(
    "returncode, stderr, expected_result",
    [
        (0, b"", Result.success(None)),
        (
            1,
            b"some error",
            Result.failure(
                CueValidationError(
                    failed_data=b"",
                    schema_path="",
                    stderr=b"some error",
                    expression="",
                )
            ),
        ),
    ],
)
@patch("integrates.schedulers.validate_db_schema.subprocess_run")
async def test_validate_with_cue(
    mock_subprocess_run: MagicMock,
    returncode: int,
    stderr: bytes,
    expected_result: Result[None, CueValidationError],
) -> None:
    mock_subprocess_run.return_value.returncode = returncode
    mock_subprocess_run.return_value.stderr = stderr

    schemas_base = Path("/schemas")
    schema_path = Path("facets/aws_marketplace_subscription_metadata.cue")
    data = b'{"pk": "AWS_PRODUCT#be517c023bbd4f8b8f9b8f3bd", "sk": "AWS_CUSTOMER#D1Hv8MM4v6Q"}'
    expression = "[...#AwsMarketplaceSubscriptionMetadataItem]"

    result = await _validate_with_cue(schemas_base, schema_path, data, expression)

    mock_subprocess_run.assert_called_once_with(
        ["cue", "vet", schema_path, "-d", expression, "-c"],
        input=data,
        capture_output=True,
        check=False,
        cwd=schemas_base,
    )
    assert result.value_or(False) == expected_result.value_or(False)


@pytest.mark.parametrize(
    [
        "facet_name",
        "table_name",
        "items",
        "schemas_base",
        "expected_result",
    ],
    [
        (
            "aws_marketplace_subscription_metadata",
            "integrates_vms",
            [
                {
                    "pk": "AWS_PRODUCT#be517c023bbd4f8b8f9b8f3bd",
                    "sk": "AWS_CUSTOMER#D1Hv8MM4v6Q",
                }
            ],
            Path("/schemas"),
            (Result.success(None), True),
        ),
        (
            "organization_historic_state",
            "integrates_vms",
            [
                {
                    "pk": "ORG#33c08ebd-2068-47e7-9673-e1aa03dc9448",
                    "sk": "STATE#2019-11-22T20:07:57+00:00",
                }
            ],
            Path("/schemas"),
            (
                Result.failure(
                    CueValidationError(
                        failed_data=b"",
                        schema_path="",
                        stderr=b"some error",
                        expression="",
                    )
                ),
                False,
            ),
        ),
    ],
)
async def test_validate_schema(
    facet_name: str,
    table_name: str,
    items: list[Item],
    schemas_base: Path,
    expected_result: tuple[Result[None, CueValidationError], bool],
) -> None:
    with (
        patch(
            "integrates.schedulers.validate_db_schema._validate_with_cue",
            new_callable=AsyncMock,
            return_value=expected_result[0],
        ) as mock_validate_with_cue,
        patch(
            "integrates.schedulers.validate_db_schema._build_facet_schema_path",
            return_value=Path(f"/schemas/tables/{table_name}/facets/{facet_name}.cue"),
        ) as mock_build_facet_schema_path,
        patch(
            "integrates.schedulers.validate_db_schema._build_facet_schema_expression",
            return_value=(f"[...#{_snake_to_pascal(facet_name)}Item]"),
        ) as mock_build_facet_schema_expression,
        patch(
            "integrates.schedulers.validate_db_schema._save_fail_metadata",
            new_callable=MagicMock,
            return_value=("data_file_path", "err_file_path"),
        ) as mock_save_fail_metadata,
    ):
        result = await _validate_schema(facet_name, table_name, items, schemas_base)
        assert result == expected_result[1]
        if not expected_result[1]:
            mock_save_fail_metadata.assert_called_once()

        mock_build_facet_schema_path.assert_called_once_with(
            facet_name, table_name, schemas_base=schemas_base
        )
        mock_build_facet_schema_expression.assert_called_once_with(facet_name)
        mock_validate_with_cue.assert_called_once_with(
            schemas_base=schemas_base,
            schema_path=mock_build_facet_schema_path.return_value,
            data=str.encode(json.dumps(items, cls=JsonDBEncoder, ensure_ascii=False)),
            expression=mock_build_facet_schema_expression.return_value,
        )


@pytest.mark.parametrize(
    "key, expected_result",
    [
        ("AWS", True),
        ("ORG", True),
        ("USER", True),
        ("VULN", True),
        ("aws", False),
        ("Org", False),
        ("user", False),
        ("vuln", False),
        ("AWS_PRODUCT", False),
        ("", False),
        ("123", False),
        ("AWS123", False),
    ],
)
def test_is_key_alias(key: str, expected_result: bool) -> None:
    result = _is_key_alias(key)
    assert result == expected_result


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "split_facets_return, scan_response",
    [
        (
            (
                {
                    "aws_marketplace_subscription_metadata": [
                        {
                            "pk": "AWS_PRODUCT#be517c023bbd4f8b8f9b8f3bd",
                            "sk": "AWS_CUSTOMER#D1Hv8MM4v6Q",
                        }
                    ]
                },
                [],
            ),
            [
                {
                    "pk": "AWS_PRODUCT#be517c023bbd4f8b8f9b8f3bd",
                    "sk": "AWS_CUSTOMER#D1Hv8MM4v6Q",
                }
            ],
        ),
    ],
)
@patch("integrates.schedulers.validate_db_schema._split_facets")
@patch(
    "integrates.schedulers.validate_db_schema._validate_schema",
    new_callable=AsyncMock,
    return_value=True,
)
async def test_run_table_validation_success(
    mock_validate_schema: AsyncMock,
    mock_split_facets: MagicMock,
    split_facets_return: tuple[dict[str, list[Item]], list[Item]],
    scan_response: list[Item],
) -> None:
    table = INTEGRATES_VMS_TABLE
    schemas_base = Path("/schemas")

    mock_split_facets.return_value = split_facets_return

    with (
        patch(
            "integrates.schedulers.validate_db_schema.scan_generator",
            new_callable=MagicMock,
            side_effect=build_generator_mock(scan_response),
        ),
        patch(
            "integrates.schedulers.validate_db_schema.dynamo_startup",
            new_callable=AsyncMock,
        ) as mock_dynamo_startup,
        patch(
            "integrates.schedulers.validate_db_schema.dynamo_shutdown",
            new_callable=AsyncMock,
        ) as mock_dynamo_shutdown,
    ):
        await _run_table_validation(table, schemas_base)
        mock_dynamo_startup.assert_awaited_once()
        mock_split_facets.assert_called_once()

        mock_validate_schema.assert_called_once_with(
            facet_name="aws_marketplace_subscription_metadata",
            table_name=table.name,
            items=scan_response,
            schemas_base=schemas_base,
        )
        mock_dynamo_shutdown.assert_awaited_once()


@pytest.mark.asyncio
@patch(
    "integrates.schedulers.validate_db_schema.dynamo_startup",
    new_callable=AsyncMock,
)
@patch(
    "integrates.schedulers.validate_db_schema.dynamo_shutdown",
    new_callable=AsyncMock,
)
@patch("integrates.schedulers.validate_db_schema._split_facets")
@patch(
    "integrates.schedulers.validate_db_schema._validate_schema",
    new_callable=AsyncMock,
    return_value=False,
)
async def test_run_table_validation_failure(
    mock_validate_schema: MagicMock,
    mock_split_facets: MagicMock,
    mock_dynamo_shutdown: AsyncMock,
    mock_dynamo_startup: AsyncMock,
) -> None:
    table = INTEGRATES_VMS_TABLE
    schemas_base = Path("/schemas")
    mock_split_facets.return_value = (
        {
            "aws_marketplace_subscription_metadata": [
                {
                    "pk": "AWS_PRODUCT#be517c023bbd4f8b8f9b8f3bd",
                    "sk": "AWS_CUSTOMER#D1Hv8MM4v6Q",
                }
            ]
        },
        [],
    )
    scan_response = [
        {
            "pk": "AWS_PRODUCT#be517c023bbd4f8b8f9b8f3bd",
            "sk": "AWS_CUSTOMER#D1Hv8MM4v6Q",
        }
    ]
    with (
        patch(
            "integrates.schedulers.validate_db_schema.scan_generator",
            new_callable=MagicMock,
            side_effect=build_generator_mock(scan_response),
        ),
        patch(
            "integrates.schedulers.validate_db_schema._save_fail_metadata",
            new_callable=MagicMock,
            return_value=("data_file_path", "err_file_path"),
        ) as mock_save_fail_metadata,
        pytest.raises(SystemExit) as pytest_wrapped_e,
    ):
        await _run_table_validation(table, schemas_base)

        assert pytest_wrapped_e.type == SystemExit
        assert pytest_wrapped_e.value.code == 1

        mock_dynamo_startup.assert_awaited_once()
        mock_split_facets.assert_called_once()
        mock_validate_schema.assert_called_once_with(
            facet_name="aws_marketplace_subscription_metadata",
            table_name=table.name,
            items=[
                {
                    "pk": "AWS_PRODUCT#be517c023bbd4f8b8f9b8f3bd",
                    "sk": "AWS_CUSTOMER#D1Hv8MM4v6Q",
                }
            ],
            schemas_base=schemas_base,
        )
        mock_save_fail_metadata.assert_called_once()
        mock_dynamo_shutdown.assert_awaited_once()


@pytest.mark.parametrize(
    "table_name, expected_table",
    [
        ("integrates_vms", INTEGRATES_VMS_TABLE),
        ("integrates_vms_historic", INTEGRATES_VMS_HISTORIC_TABLE),
    ],
)
def test_handle_table_arg_valid(table_name: str, expected_table: Table) -> None:
    result = _handle_table_arg(table_name)
    assert result == expected_table


@pytest.mark.parametrize(
    "table_name",
    [
        "unknown_table",
        "invalid_table",
        "non_existent_table",
    ],
)
def test_handle_table_arg_invalid(table_name: str) -> None:
    with pytest.raises(UnknownTableError) as exc_info:
        _handle_table_arg(table_name)
    assert str(exc_info.value) == (
        f"Unknown table: {table_name}. Please choose one in: {_TARGET_TABLES.keys()}"
    )


@pytest.mark.parametrize(
    "base_path, is_dir, expected_result",
    [
        ("/valid/path", True, Path("/valid/path")),
        ("/invalid/path", False, InvalidSchemaBasePathError),
    ],
)
def test_handle_schema_base_arg(
    base_path: str, is_dir: bool, expected_result: Path | type[Exception]
) -> None:
    with patch(
        "integrates.schedulers.validate_db_schema.Path.is_dir",
        return_value=is_dir,
    ):
        if is_dir:
            result = _handle_schema_base_arg(base_path)
            assert result == expected_result
        else:
            with pytest.raises(InvalidSchemaBasePathError) as exc_info:
                _handle_schema_base_arg(base_path)
            assert str(exc_info.value) == f"Invalid schema base path: {base_path}"


@pytest.mark.asyncio
@patch(
    "integrates.schedulers.validate_db_schema._run_table_validation",
    new_callable=AsyncMock,
)
@patch("integrates.schedulers.validate_db_schema._handle_table_arg")
@patch("integrates.schedulers.validate_db_schema._handle_schema_base_arg")
@patch("integrates.schedulers.validate_db_schema.argparse.ArgumentParser.parse_args")
async def test_main(
    mock_parse_args: MagicMock,
    mock_handle_schema_base_arg: MagicMock,
    mock_handle_table_arg: MagicMock,
    mock_run_table_validation: AsyncMock,
) -> None:
    mock_parse_args.return_value = argparse.Namespace(
        table="integrates_vms", schema_base_path="/schemas"
    )
    mock_handle_table_arg.return_value = "mock_table"
    mock_handle_schema_base_arg.return_value = "mock_schema_base"

    await main()

    mock_parse_args.assert_called_once()
    mock_handle_table_arg.assert_called_once_with("integrates_vms")
    mock_handle_schema_base_arg.assert_called_once_with("/schemas")
    mock_run_table_validation.assert_awaited_once_with("mock_table", "mock_schema_base")


@pytest.mark.parametrize(
    "input_data, expected_output",
    [
        (Decimal("10.5"), 10.5),
        (Decimal("10.0"), 10),
        ({"a", "b", "c"}, {"S": ["a", "b", "c"]}),
    ],
)
def test_json_db_encoder(
    input_data: Decimal | set | str, expected_output: Item | float | int
) -> None:
    encoder = JsonDBEncoder()
    result = encoder.default(input_data)
    if isinstance(input_data, set):
        result = typing.cast(Item, result)
        expected_output = typing.cast(Item, expected_output)
        assert set(result["S"]) == set(expected_output["S"])
    else:
        assert result == expected_output


@freeze_time("2024-10-10")
@pytest.mark.parametrize(
    "cue_validation_error,file_name",
    [
        [
            CueValidationError(
                failed_data=(
                    b'{"pk": "AWS_PRODUCT#be517c023bbd4f8b8f9b8f3bd",'
                    b'"sk": "AWS_CUSTOMER#D1Hv8MM4v6Q"}'
                ),
                schema_path="facets/aws_marketplace_subscription_metadata.cue",
                stderr=b"Validation error",
                expression="aws_marketplace_subscription_metadata",
            ),
            "aws_marketplace_subscription_metadata_1728518400",
        ],
        [
            CueValidationError(
                failed_data=(
                    b'{"pk": "ORG#33c08ebd-2068-47e7-9673-e1aa03dc9448",'
                    b' "sk": "STATE#2019-11-22T20:07:57+00:00"}'
                ),
                schema_path="facets/organization_historic_state.cue",
                stderr=b"Another validation error",
                expression="organization_historic_state",
            ),
            "organization_historic_state_1728518400",
        ],
    ],
)
def test_save_fail_metadata(cue_validation_error: CueValidationError, file_name: str) -> None:
    with (
        patch("integrates.schedulers.validate_db_schema.os.makedirs") as mock_makedirs,
        patch("integrates.schedulers.validate_db_schema.open", new_callable=MagicMock) as mock_open,
    ):
        _save_fail_metadata(cue_validation_error)

        mock_makedirs.assert_called_once_with(METADATA_FAILS_DIR, exist_ok=True)

        mock_open.assert_any_call(
            os.path.join(METADATA_FAILS_DIR, file_name + ".json"),
            "wb",
        )
        mock_open.assert_any_call(
            os.path.join(METADATA_FAILS_DIR, file_name + ".err"),
            "wb",
        )
