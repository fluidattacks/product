import botocore
from datetime import (
    datetime,
    timedelta,
)
from decimal import (
    Decimal,
)
from integrates.custom_exceptions import (
    BotoErrorException,
)
from integrates.db_model.groups.enums import (
    GroupLanguage,
    GroupManaged,
    GroupService,
    GroupStateStatus,
    GroupSubscriptionType,
    GroupTier,
)
from integrates.db_model.groups.types import (
    Group,
    GroupState,
)
from integrates.db_model.organizations.enums import (
    OrganizationStateStatus,
)
from integrates.db_model.organizations.types import (
    Organization,
    OrganizationState,
)
from integrates.db_model.roots.enums import (
    RootCloningStatus,
    RootCriticality,
    RootEnvironmentUrlStateStatus,
    RootStatus,
    RootType,
)
from integrates.db_model.roots.types import (
    GitRoot,
    GitRootCloning,
    GitRootState,
    RootEnvironmentUrl,
    RootEnvironmentUrlState,
    Secret,
    SecretState,
)
from integrates.db_model.types import (
    Policies,
)
from integrates.schedulers.nofluid_comments import (
    arn_no_fluid,
    count_cloud_exclusions,
    get_iam_resources_with_tag,
    run_boto3_fun,
)
import pytest
from test.unit.src.utils import (
    get_module_at_test,
)
from typing import (
    AsyncGenerator,
)
from unittest.mock import (
    AsyncMock,
    MagicMock,
    patch,
)

pytestmark = [
    pytest.mark.asyncio,
]

MODULE_AT_TEST = f"integrates.{get_module_at_test(file_path=__file__)}"

pytestmark = [
    pytest.mark.asyncio,
]

EMAIL_OWNER = "admin@gmail.com"


@pytest.mark.parametrize(
    "tags,expected_arn",
    [
        (
            [{"Key": "NOFLUID", "Value": "True"}],
            "arn:aws:iam::123456789012:user/Example",
        ),
        ([{"Key": "AnotherTag", "Value": "False"}], None),
        (
            [
                {"Key": "FirstTag", "Value": "True"},
                {"Key": "NOFLUID", "Value": "True"},
            ],
            "arn:aws:iam::123456789012:user/Example",
        ),
        ([], None),
    ],
)
def test_arn_no_fluid(
    tags: list[dict[str, str]], expected_arn: str | None
) -> None:
    arn = "arn:aws:iam::123456789012:user/Example"
    result = arn_no_fluid(tags, arn)
    assert result == expected_arn


async def test_run_boto3_fun() -> None:
    creds = [
        Secret(
            key="access_key_id",
            value="test_access_key_id",
            created_at=datetime.now(),
            state=SecretState(
                owner=EMAIL_OWNER,
                modified_by=EMAIL_OWNER,
                modified_date=datetime.now(),
            ),
        ),
        Secret(
            key="secret_access_key",
            value="test_secret_access_key",
            created_at=datetime.now(),
            state=SecretState(
                owner=EMAIL_OWNER,
                modified_by=EMAIL_OWNER,
                modified_date=datetime.now(),
            ),
        ),
        Secret(
            key="session_token",
            value="test_session_token",
            created_at=datetime.now(),
            state=SecretState(
                owner=EMAIL_OWNER,
                modified_by=EMAIL_OWNER,
                modified_date=datetime.now(),
            ),
        ),
    ]
    service = "s3"
    function_name = "list_buckets"
    expected_result = {"Buckets": [{"Name": "my-bucket"}]}

    with patch("aioboto3.Session") as session:
        mock_client = AsyncMock()
        mock_client.list_buckets = AsyncMock(return_value=expected_result)
        session.return_value.client.return_value.__aenter__.return_value = (
            mock_client
        )

        result = await run_boto3_fun(creds, service, function_name)

        assert result == expected_result

        mock_client.list_buckets.assert_awaited()


@pytest.mark.asyncio
async def test_run_boto3_fun_with_client_error() -> None:
    creds = [
        Secret(
            key="access_key_id",
            value="test_access_key_id",
            created_at=datetime.now(),
            state=SecretState(
                owner=EMAIL_OWNER,
                modified_by=EMAIL_OWNER,
                modified_date=datetime.now(),
            ),
        ),
        Secret(
            key="secret_access_key",
            value="test_secret_access_key",
            created_at=datetime.now(),
            state=SecretState(
                owner=EMAIL_OWNER,
                modified_by=EMAIL_OWNER,
                modified_date=datetime.now(),
            ),
        ),
        Secret(
            key="session_token",
            value="test_session_token",
            created_at=datetime.now(),
            state=SecretState(
                owner=EMAIL_OWNER,
                modified_by=EMAIL_OWNER,
                modified_date=datetime.now(),
            ),
        ),
    ]

    with patch("aioboto3.Session") as session, pytest.raises(
        BotoErrorException
    ):
        mock_client = AsyncMock()
        mock_client.list_buckets = AsyncMock(
            side_effect=botocore.exceptions.ClientError(
                {"Error": {"Code": "Error"}}, "list_buckets"
            )
        )
        session.return_value.client.return_value.__aenter__.return_value = (
            mock_client
        )

        await run_boto3_fun(creds, "s3", "list_buckets")


@pytest.mark.asyncio
@patch("integrates.schedulers.nofluid_comments.get_new_context")
@patch("integrates.schedulers.nofluid_comments.fetch_temp_aws_creds")
@patch("integrates.schedulers.nofluid_comments.get_arns_that_are_excluded")
async def test_count_cloud_exclusions(
    mock_get_arns_that_are_excluded: AsyncMock,
    mock_fetch_temp_aws_creds: AsyncMock,
    mock_get_new_context: AsyncMock,
) -> None:
    root = GitRoot(
        cloning=GitRootCloning(
            modified_by="Violeta Parra",
            reason="Reason for git root cloning",
            modified_date=datetime.now() - timedelta(days=4),
            status=RootCloningStatus.CLONING,
        ),
        created_by="root creator",
        created_date=datetime.now(),
        group_name="unittesting",
        id="4039e098-ffc5-4984-8ed3-eb17bca98e19",
        state=GitRootState(
            branch="trunk",
            criticality=RootCriticality.LOW,
            modified_by="some git root state modifier",
            modified_date=datetime.now(),
            nickname="status nickname",
            includes_health_check=False,
            url="https://url.org",
            status=RootStatus.ACTIVE,
        ),
        type=RootType.GIT,
        organization_name="Some org name",
    )
    group = Group(
        created_by="unknown",
        created_date=datetime.fromisoformat("2020-05-20T22:00:00+00:00"),
        description="Integrates group",
        language=GroupLanguage.EN,
        name="metropolis",
        organization_id="ORG#c6cecc0e-bb92-4079-8b6d-c4e815c10bb1",
        state=GroupState(
            has_essential=False,
            has_advanced=True,
            managed=GroupManaged.NOT_MANAGED,
            modified_by="unknown",
            modified_date=datetime.fromisoformat("2020-05-20T22:00:00+00:00"),
            status=GroupStateStatus.ACTIVE,
            tier=GroupTier.ESSENTIAL,
            type=GroupSubscriptionType.CONTINUOUS,
            tags={"test-nogroups"},
            comments=None,
            justification=None,
            payment_id=None,
            pending_deletion_date=None,
            service=GroupService.WHITE,
        ),
        agent_token=None,
        business_id="14441323",
        business_name="Testing Company and Sons",
        context=None,
        disambiguation=None,
        files=None,
        policies=None,
        sprint_duration=2,
        sprint_start_date=datetime.fromisoformat("2022-06-06T00:00:00"),
    )
    mock_loaders = AsyncMock()
    mock_organization_loader = AsyncMock()
    mock_organization_loader.load = AsyncMock(
        return_value=Organization(
            created_by="unknown",
            created_date=(datetime.fromisoformat("2019-11-22T20:07:57+00:00")),
            id="ORG#33c08ebd-2068-47e7-9673-e1aa03dc9448",
            name="kiba",
            policies=Policies(
                modified_date=(
                    datetime.fromisoformat("2019-11-22T20:07:57+00:00")
                ),
                modified_by="unknown",
                inactivity_period=90,
                max_acceptance_days=None,
                max_acceptance_severity=Decimal("10.0"),
                max_number_acceptances=None,
                min_acceptance_severity=Decimal("0.0"),
                min_breaking_severity=Decimal("0.0"),
                vulnerability_grace_period=None,
            ),
            state=OrganizationState(
                aws_external_id="some_external_id",
                status=OrganizationStateStatus.ACTIVE,
                modified_by="unknown",
                modified_date=(
                    datetime.fromisoformat("2019-11-22T20:07:57+00:00")
                ),
                pending_deletion_date=None,
            ),
            country="Colombia",
        )
    )
    mock_root_environment_urls_loader = AsyncMock()
    mock_root_environment_urls_loader.load = AsyncMock(
        return_value=[
            RootEnvironmentUrl(
                "arn:aws:iam::123456789012:role/SomeRole",
                "id",
                "root_id",
                group_name="metropolis",
                state=RootEnvironmentUrlState(
                    modified_by="admin@gmail.com",
                    modified_date=datetime.fromisoformat(
                        "2020-11-19T13:37:10+00:00"
                    ),
                    status=RootEnvironmentUrlStateStatus.CREATED,
                ),
            )
        ]
    )
    mock_loaders.organization = mock_organization_loader
    mock_loaders.root_environment_urls = mock_root_environment_urls_loader
    mock_get_new_context.return_value = mock_loaders

    mock_fetch_temp_aws_creds.return_value = [
        Secret(
            key="access_key_id",
            value="test_access_key_id",
            created_at=datetime.now(),
            state=SecretState(
                owner=EMAIL_OWNER,
                modified_by=EMAIL_OWNER,
                modified_date=datetime.now(),
            ),
        ),
        Secret(
            key="secret_access_key",
            value="test_secret_access_key",
            created_at=datetime.now(),
            state=SecretState(
                owner=EMAIL_OWNER,
                modified_by=EMAIL_OWNER,
                modified_date=datetime.now(),
            ),
        ),
        Secret(
            key="session_token",
            value="test_session_token",
            created_at=datetime.now(),
            state=SecretState(
                owner=EMAIL_OWNER,
                modified_by=EMAIL_OWNER,
                modified_date=datetime.now(),
            ),
        ),
    ]

    mock_get_arns_that_are_excluded.return_value = 0

    count = await count_cloud_exclusions(root, group)

    assert count == 0

    mock_fetch_temp_aws_creds.assert_awaited_with(
        "arn:aws:iam::123456789012:role/SomeRole", "some_external_id"
    )
    mock_get_arns_that_are_excluded.assert_awaited()


@pytest.mark.asyncio
@patch(MODULE_AT_TEST + "aioboto3.Session.client")
async def test_get_iam_resources_with_tag(mock_client: MagicMock) -> None:
    mock_iam_client = AsyncMock()
    mock_client.return_value.__aenter__.return_value = mock_iam_client

    async def paginate_mock() -> AsyncGenerator[
        dict[str, list[dict[str, str]]], None
    ]:
        yield {
            "Users": [
                {
                    "UserName": "user1",
                    "Arn": "arn:aws:iam::123:user/user1",
                    "Tags": "NOFLUID",
                }
            ]
        }

    paginator_mock = MagicMock()
    paginator_mock.paginate = paginate_mock
    mock_iam_client.get_paginator.return_value = paginator_mock

    mock_iam_client.list_user_tags = AsyncMock(
        return_value={"Tags": [{"Key": "NOFLUID"}]}
    )
    mock_iam_client.list_role_tags = AsyncMock(
        return_value={"Tags": [{"Key": "AnotherTag"}]}
    )

    credentials = [
        Secret(
            key="access_key_id",
            value="test_access_key_id",
            created_at=datetime.now(),
            state=SecretState(
                owner=EMAIL_OWNER,
                modified_by=EMAIL_OWNER,
                modified_date=datetime.now(),
            ),
        ),
        Secret(
            key="secret_access_key",
            value="test_secret_access_key",
            created_at=datetime.now(),
            state=SecretState(
                owner=EMAIL_OWNER,
                modified_by=EMAIL_OWNER,
                modified_date=datetime.now(),
            ),
        ),
        Secret(
            key="session_token",
            value="test_session_token",
            created_at=datetime.now(),
            state=SecretState(
                owner=EMAIL_OWNER,
                modified_by=EMAIL_OWNER,
                modified_date=datetime.now(),
            ),
        ),
    ]
    arns = []
    with pytest.raises(AttributeError):
        arns = await get_iam_resources_with_tag(credentials)

    assert arns is not None
    assert arns == []
