from ariadne.load_schema import (
    load_schema_from_path,
)
from datetime import (
    datetime,
)
from freezegun import (
    freeze_time,
)
from integrates.custom_utils.deprecations.types import (
    ApiDeprecation,
    ApiFieldType,
)
from integrates.db_model.stakeholders.types import (
    AccessTokens,
    Stakeholder,
)
from integrates.schedulers.send_deprecation_notice import (
    _format_deprecation_for_mail,
    main,
)
import os
import pytest
from test.unit.src.utils import (
    get_module_at_test,
)
from unittest.mock import (
    AsyncMock,
    patch,
)

MODULE_AT_TEST = f"integrates.{get_module_at_test(file_path=__file__)}"


@freeze_time()
@pytest.mark.parametrize(
    ["deprecations"],
    [
        [
            {
                "ModuleA": [
                    ApiDeprecation(
                        parent="ClassA",
                        field="field1",
                        reason="Deprecated due to security issues",
                        due_date=datetime(2023, 12, 31),
                        type=ApiFieldType.ENUM,
                    ),
                    ApiDeprecation(
                        parent="ClassB",
                        field="field2",
                        reason="Field no longer needed",
                        due_date=datetime(2023, 10, 15),
                        type=ApiFieldType.ENUM,
                    ),
                ],
                "ModuleB": [
                    ApiDeprecation(
                        parent="ClassA",
                        field="field3",
                        reason="Performance improvement",
                        due_date=datetime(2023, 11, 30),
                        type=ApiFieldType.ENUM,
                    )
                ],
            }
        ]
    ],
)
def test_format_deprecation_for_mail(
    deprecations: dict[str, list[ApiDeprecation]]
) -> None:
    result = _format_deprecation_for_mail(deprecations=deprecations)
    assert result["ModuleA"] == "field1 | field2"
    assert result["ModuleB"] == "field3"


@patch(
    MODULE_AT_TEST + "stakeholders_model.get_all_stakeholders",
    return_value=[
        Stakeholder(
            email="stakeholder1@unittest.mock",
            access_tokens=[
                AccessTokens(
                    id="jwt1",
                    issued_at=1694192604208,
                    jti_hashed="some hash",
                    salt="some salt",
                )
            ],
        ),
        Stakeholder(
            email="stakeholder2@unittest.mock",
            access_tokens=[
                AccessTokens(
                    id="jwt2",
                    issued_at=1694192604208,
                    jti_hashed="some hash",
                    salt="some salt",
                )
            ],
        ),
        Stakeholder(email="stakeholder3@unittest.mock"),
        Stakeholder(
            email="stakeholder4@unittest.mock",
            access_tokens=[
                AccessTokens(
                    id="jwt3",
                    issued_at=1694192604208,
                    jti_hashed="some hash",
                    salt="some salt",
                )
            ],
        ),
        Stakeholder(email="stakeholder5@unittest.mock"),
    ],
)
@patch(MODULE_AT_TEST + "send_mail_deprecation_notice", new_callable=AsyncMock)
@patch(
    MODULE_AT_TEST + "SDL_CONTENT",
    load_schema_from_path(
        os.path.join(os.path.dirname(os.path.dirname(__file__)), "mock")
    ),
)
@pytest.mark.asyncio
@freeze_time("2020-01-01")
async def test_main(
    mock_send_mail_deprecation_notice: AsyncMock,
    mock_get_all_stakeholders: AsyncMock,
) -> None:
    await main()
    mock_get_all_stakeholders.assert_awaited_once()
    mock_send_mail_deprecation_notice.assert_awaited_once()
    mail_args = mock_send_mail_deprecation_notice.call_args.kwargs
    assert "mail_deprecations" in mail_args
    for arg_name, arg_value in mail_args.items():
        if arg_name == "mail_deprecations":
            assert "SomeInputType" not in arg_value
            assert arg_value["MyType"] == "deprecatedField | deprecatedName"
            assert arg_value["MyEnum"] == "OLD_VALUE"
