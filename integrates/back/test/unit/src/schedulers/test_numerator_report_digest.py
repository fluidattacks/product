from datetime import (
    date,
    datetime,
)
from integrates.custom_utils.datetime import (
    DateRange,
    get_report_dates,
)
from integrates.custom_utils.toes import (
    ToeType,
)
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model.toe_lines.types import (
    ToeLine,
    ToeLineState,
)
from integrates.mailer.types import (
    CountReportInfo,
)
from integrates.schedulers.numerator_report_digest import (
    _common_generate_count_report,
    _finding_content,
    _finding_reattacked,
    _generate_fields,
    _generate_general_coverage,
    _generate_group_fields,
    _get_average,
    _get_coverage,
    _get_na_toe_lines,
    _have_data,
    _paginated_toes_processing,
    _send_mail_report,
    _set_quintile_info,
    _validate_date,
    get_percent,
    main,
)
import pytest
import pytz
from test.unit.src.utils import (
    get_module_at_test,
)
from typing import (
    Any,
)
from unittest.mock import (
    AsyncMock,
    patch,
)

MODULE_AT_TEST = f"integrates.{get_module_at_test(file_path=__file__)}"


pytestmark = [
    pytest.mark.asyncio,
]


def test_get_percent() -> None:
    assert get_percent(0, 10) == "+0%"
    assert get_percent(10, 0) == "-"
    # FP: local testing
    assert get_percent("0", 10) == "-"  # type: ignore
    assert get_percent(-10, 10) == "-100%"
    assert get_percent(0.55, 10) == "+6%"
    assert get_percent(2, 3) == "+67%"
    assert get_percent(3, 2) == "+150%"
    assert get_percent(-2, 3) == "-67%"
    assert get_percent(-3, 2) == "-150%"


@pytest.mark.parametrize(
    ["report_date", "date_range"],
    [
        [
            datetime(2023, 7, 25, 0, 0, tzinfo=pytz.UTC).date(),
            DateRange(
                end_date=datetime(2023, 7, 26, 0, 0, tzinfo=pytz.UTC),
                start_date=datetime(2023, 7, 24, 0, 0, tzinfo=pytz.UTC),
            ),
        ]
    ],
)
def test_validate_date(
    report_date: date,
    date_range: DateRange,
) -> None:
    assert _validate_date(report_date, date_range)


@pytest.mark.parametrize(
    ["report_date", "date_range"],
    [
        [
            datetime(2023, 7, 27, 0, 0, tzinfo=pytz.UTC).date(),
            DateRange(
                end_date=datetime(2023, 7, 26, 0, 0, tzinfo=pytz.UTC),
                start_date=datetime(2023, 7, 24, 0, 0, tzinfo=pytz.UTC),
            ),
        ]
    ],
)
def test_validate_date_fail(
    report_date: date,
    date_range: DateRange,
) -> None:
    assert not _validate_date(report_date, date_range)


def test_get_average() -> None:
    assert _get_average(10, 0) == 0
    assert _get_average(100, 5) == 20


@pytest.mark.parametrize(
    ["inputs", "outputs"],
    [
        [
            [
                {
                    "enumerated_inputs": {
                        "counter": {"past_day": 0, "today": 0}
                    },
                    "enumerated_ports": {
                        "counter": {"past_day": 0, "today": 0}
                    },
                    "verified_inputs": {
                        "counter": {"past_day": 0, "today": 0}
                    },
                    "verified_ports": {"counter": {"past_day": 0, "today": 0}},
                    "loc": {"counter": {"past_day": 1, "today": 0}},
                    "reattacked": {"counter": {"past_day": 0, "today": 0}},
                    "vulnerable": {"counter": {"past_day": 0, "today": 0}},
                    "sorts_verified_lines": {
                        "counter": {"past_day": 0, "today": 0}
                    },
                    "sorts_verified_lines_priority": {
                        "counter": {"past_day": 0, "today": 0}
                    },
                    "sorts_verified_lines_priority_avg": {
                        "counter": {"past_day": 0, "today": 0}
                    },
                    "submitted": {"counter": {"past_day": 0, "today": 0}},
                    "coverage_q1": {
                        "files": {"verified": 0, "total": 0, "percentage": 0},
                        "lines": {"verified": 0, "total": 0, "percentage": 0},
                    },
                    "coverage_q2": {
                        "files": {"verified": 0, "total": 0, "percentage": 0},
                        "lines": {"verified": 0, "total": 0, "percentage": 0},
                    },
                    "coverage_q3": {
                        "files": {"verified": 0, "total": 0, "percentage": 0},
                        "lines": {"verified": 0, "total": 0, "percentage": 0},
                    },
                    "coverage_q4": {
                        "files": {"verified": 0, "total": 0, "percentage": 0},
                        "lines": {"verified": 0, "total": 0, "percentage": 0},
                    },
                    "coverage_q5": {
                        "files": {"verified": 0, "total": 0, "percentage": 0},
                        "lines": {"verified": 0, "total": 0, "percentage": 0},
                    },
                    "coverage_na": {
                        "files": {"verified": 0, "total": 0, "percentage": 0},
                        "lines": {"verified": 0, "total": 0, "percentage": 0},
                    },
                    "max_cvss": {"vulnerable": 0.0, "submitted": 0.0},
                    "groups": {},
                },
                {
                    "enumerated_inputs": {
                        "counter": {"past_day": 0, "today": 0}
                    },
                    "enumerated_ports": {
                        "counter": {"past_day": 0, "today": 0}
                    },
                    "verified_inputs": {
                        "counter": {"past_day": 0, "today": 0}
                    },
                    "verified_ports": {"counter": {"past_day": 0, "today": 0}},
                    "loc": {"counter": {"past_day": 0, "today": 0}},
                    "reattacked": {"counter": {"past_day": 0, "today": 0}},
                    "vulnerable": {"counter": {"past_day": 0, "today": 0}},
                    "sorts_verified_lines": {
                        "counter": {"past_day": 0, "today": 0}
                    },
                    "sorts_verified_lines_priority": {
                        "counter": {"past_day": 0, "today": 0}
                    },
                    "sorts_verified_lines_priority_avg": {
                        "counter": {"past_day": 0, "today": 0}
                    },
                    "submitted": {"counter": {"past_day": 0, "today": 0}},
                    "coverage_q1": {
                        "files": {"verified": 0, "total": 0, "percentage": 0},
                        "lines": {"verified": 0, "total": 0, "percentage": 0},
                    },
                    "coverage_q2": {
                        "files": {"verified": 0, "total": 0, "percentage": 0},
                        "lines": {"verified": 0, "total": 0, "percentage": 0},
                    },
                    "coverage_q3": {
                        "files": {"verified": 0, "total": 0, "percentage": 0},
                        "lines": {"verified": 0, "total": 0, "percentage": 0},
                    },
                    "coverage_q4": {
                        "files": {"verified": 0, "total": 0, "percentage": 0},
                        "lines": {"verified": 0, "total": 0, "percentage": 0},
                    },
                    "coverage_q5": {
                        "files": {"verified": 0, "total": 0, "percentage": 0},
                        "lines": {"verified": 0, "total": 0, "percentage": 0},
                    },
                    "coverage_na": {
                        "files": {"verified": 0, "total": 0, "percentage": 0},
                        "lines": {"verified": 0, "total": 0, "percentage": 0},
                    },
                    "max_cvss": {"vulnerable": 0.0, "submitted": 0.0},
                    "groups": {},
                },
            ],
            [True, False],
        ]
    ],
)
def test_have_data(inputs: list[dict[str, Any]], outputs: list[bool]) -> None:
    for index, data in enumerate(inputs):
        assert _have_data(data) == outputs[index]


@pytest.mark.parametrize(
    ["group_name", "user_email"],
    [["unittesting", "integrateshacker@fluidattacks.com"]],
)
@patch(MODULE_AT_TEST + "_finding_reattacked", new_callable=AsyncMock)
async def test_finding_content(
    mock_finding_reattacked: AsyncMock,
    group_name: str,
    user_email: str,
) -> None:
    report_dates = get_report_dates(datetime.now(pytz.UTC))
    content: dict[str, Any] = {user_email: _generate_fields()}
    content[user_email]["groups"] = {
        group_name: _generate_group_fields(),
        "test_group": _generate_group_fields(),
    }
    await _finding_content(
        group=group_name,
        report_dates=report_dates,
        content=content,
        users_email=[user_email],
    )
    mock_finding_reattacked.assert_awaited()


@patch(
    MODULE_AT_TEST + "Dataloaders.finding_historic_verification",
    new_callable=AsyncMock,
)
async def test_finding_reattacked(
    mock_dataloaders_finding_historic_verification: AsyncMock,
    mocked_data_for_module: Any,
) -> None:
    finding_id: str = "436992569"
    group_name: str = "unittesting"
    report_dates = get_report_dates(datetime.now(pytz.UTC))
    user_email: str = "test@test.com"
    content: dict[str, Any] = {user_email: _generate_fields()}
    content[user_email]["groups"] = {
        "unittesting": _generate_group_fields(),
        "test_group": _generate_group_fields(),
    }
    mock_dataloaders_finding_historic_verification.load.return_value = (
        mocked_data_for_module(
            mock_path="Dataloaders.finding_historic_verification",
            mock_args=[finding_id],
            module_at_test=MODULE_AT_TEST,
        )
    )
    loaders: Dataloaders = get_new_context()
    await _finding_reattacked(
        loaders=loaders,
        finding_id=finding_id,
        group=group_name,
        report_dates=report_dates,
        content=content,
        users_email=["integratesanalyst2@gmail.com"],
    )
    assert mock_dataloaders_finding_historic_verification.load.called is True


def test_common_generate_count_report() -> None:
    report_dates = get_report_dates(datetime.now(pytz.UTC))
    user_email: str = "test@test.com"
    content: dict[str, Any] = {user_email: _generate_fields()}
    content[user_email]["groups"] = {
        "unittesting": _generate_group_fields(),
        "test_group": _generate_group_fields(),
    }
    fields: list[str] = [
        "verified_inputs",
        "verified_inputs",
        "verified_inputs",
        "verified_ports",
        "verified_ports",
        "enumerated_inputs",
        "enumerated_ports",
        "enumerated_ports",
        "vulnerable",
        "submitted",
    ]
    groups: list[str] = [
        "unittesting",
        "unittesting",
        "test_group",
        "test_group",
        "unittesting",
        "unittesting",
        "test_group",
        "unittesting",
        "test_group",
        "test_group",
        "test_group",
    ]

    for group, field in zip(groups, fields):
        _common_generate_count_report(
            report_dates.today.start_date,
            field,
            CountReportInfo(
                allowed_users=["test@test.com"],
                content=content,
                group=group,
                report_dates=report_dates,
            ),
            user_email,
        )

    assert content[user_email]["verified_inputs"]["counter"]["today"] == 3
    assert content[user_email]["enumerated_inputs"]["counter"]["today"] == 1
    assert content[user_email]["vulnerable"]["counter"]["today"] == 1
    assert content[user_email]["submitted"]["counter"]["today"] == 1
    assert (
        content[user_email]["groups"]["unittesting"]["enumerated_inputs"] == 1
    )
    assert (
        content[user_email]["groups"]["unittesting"]["enumerated_ports"] == 1
    )
    assert content[user_email]["groups"]["unittesting"]["verified_inputs"] == 2
    assert content[user_email]["groups"]["unittesting"]["verified_ports"] == 1
    assert (
        content[user_email]["groups"]["test_group"]["enumerated_inputs"] == 0
    )
    assert content[user_email]["groups"]["test_group"]["enumerated_ports"] == 1
    assert content[user_email]["groups"]["test_group"]["verified_inputs"] == 1
    assert content[user_email]["groups"]["test_group"]["verified_ports"] == 1
    assert content[user_email]["groups"]["test_group"]["vulnerable"] == 1
    assert content[user_email]["groups"]["test_group"]["submitted"] == 1
    count_info = CountReportInfo(
        allowed_users=["test@test.com"],
        content=content,
        group="test_group",
        report_dates=report_dates,
    )
    _common_generate_count_report(
        report_dates.past_day.start_date,
        "verified_inputs",
        count_info,
        user_email,
    )
    _common_generate_count_report(
        report_dates.past_day.start_date,
        "verified_inputs",
        count_info,
        user_email,
    )
    assert content[user_email]["verified_inputs"]["counter"]["past_day"] == 2
    _common_generate_count_report(
        report_dates.past_day.start_date,
        "verified_ports",
        count_info,
        user_email,
    )
    _common_generate_count_report(
        report_dates.past_day.start_date,
        "verified_ports",
        count_info,
        user_email,
    )
    assert content[user_email]["verified_ports"]["counter"]["past_day"] == 2


@pytest.mark.asyncio
@pytest.mark.parametrize(
    [
        "user_data",
    ],
    [
        [
            {
                "coverage_q1": {
                    "files": {
                        "verified": 10,
                        "total": 200,
                        "percentage": 0,
                    },
                    "lines": {
                        "verified": 5,
                        "total": 50,
                        "percentage": 0,
                    },
                },
                "coverage_q2": {
                    "files": {
                        "verified": 10,
                        "total": 200,
                        "percentage": 0,
                    },
                    "lines": {
                        "verified": 5,
                        "total": 50,
                        "percentage": 0,
                    },
                },
                "coverage_q3": {
                    "files": {
                        "verified": 10,
                        "total": 0,
                        "percentage": 0,
                    },
                    "lines": {
                        "verified": 5,
                        "total": 0,
                        "percentage": 0,
                    },
                },
                "coverage_q4": {
                    "files": {
                        "verified": 10,
                        "total": 200,
                        "percentage": 5,
                    },
                    "lines": {
                        "verified": 5,
                        "total": 50,
                        "percentage": 10,
                    },
                },
                "coverage_q5": {
                    "files": {
                        "verified": 10,
                        "total": 200,
                        "percentage": 5,
                    },
                    "lines": {
                        "verified": 5,
                        "total": 50,
                        "percentage": 10,
                    },
                },
                "coverage_na": {
                    "files": {
                        "verified": 50,
                        "total": 0,
                        "percentage": 0,
                    },
                    "lines": {
                        "verified": 90,
                        "total": 0,
                        "percentage": 0,
                    },
                },
            },
        ],
    ],
)
async def test_generate_general_coverage(
    user_data: dict[str, Any],
) -> None:
    _generate_general_coverage(user_data=user_data)
    assert user_data["coverage_q1"]["files"]["percentage"] == "5%"
    assert user_data["coverage_q1"]["lines"]["percentage"] == "10%"
    assert user_data["coverage_q2"]["files"]["percentage"] == "5%"
    assert user_data["coverage_q2"]["lines"]["percentage"] == "10%"
    assert user_data["coverage_q3"]["files"]["percentage"] == "N/A"
    assert user_data["coverage_q3"]["lines"]["percentage"] == "N/A"
    assert user_data["coverage_na"]["files"]["percentage"] == "N/A"
    assert user_data["coverage_na"]["lines"]["percentage"] == "N/A"


@pytest.mark.asyncio
@pytest.mark.parametrize(
    ["user_data"],
    [
        [
            {
                "coverage_na": {
                    "files": {
                        "verified": 0,
                        "total": 0,
                        "percentage": 0,
                    },
                    "lines": {
                        "verified": 0,
                        "total": 0,
                        "percentage": 0,
                    },
                },
                "groups": {
                    "group1": {
                        "coverage_na": {
                            "files_total": 0,
                            "lines_total": 0,
                        },
                    },
                },
            },
        ]
    ],
)
async def test_set_na_coverage_info(
    user_data: dict[str, Any],
) -> None:
    group_toe_lines: list[ToeLine] = [
        ToeLine(
            filename="test1/test.sh",
            group_name="group1",
            root_id="63298a73-9dff-46cf-b42d-9b2f01a56690",
            state=ToeLineState(
                attacked_at=datetime.fromisoformat(
                    "2020-01-14T15:41:04+00:00"
                ),
                attacked_by="test@test.com",
                attacked_lines=4324,
                be_present=True,
                be_present_until=datetime.fromisoformat(
                    "2021-01-19T15:41:04+00:00"
                ),
                comments="comment 1",
                first_attack_at=datetime.fromisoformat(
                    "2020-01-19T15:41:04+00:00"
                ),
                modified_by="test@test.com",
                modified_date=datetime.fromisoformat(
                    "2021-11-16T15:41:04+00:00"
                ),
                has_vulnerabilities=False,
                last_author="customer1@gmail.com",
                last_commit="f9e4beba70c4f34d6117c3b0c23ebe6b2bff66c1",
                last_commit_date=datetime.fromisoformat(
                    "2021-11-16T15:41:04+00:00"
                ),
                loc=4324,
                seen_at=datetime.fromisoformat("2020-01-01T15:41:04+00:00"),
                sorts_risk_level=1,
                sorts_priority_factor=-1,
            ),
        ),
        ToeLine(
            filename="front/index.html",
            group_name="group1",
            root_id="88637616-41d4-4242-854a-db8ff7fe1ab6",
            state=ToeLineState(
                attacked_at=datetime.fromisoformat(
                    "2020-01-14T15:41:04+00:00"
                ),
                attacked_by="machine@fluidattacks.com",
                attacked_lines=0,
                be_present=True,
                be_present_until=None,
                comments="",
                first_attack_at=datetime.fromisoformat(
                    "2020-01-14T15:41:04+00:00"
                ),
                has_vulnerabilities=False,
                last_author="customer3@gmail.com",
                last_commit="f9e4beba70c4f34d6117c3b0c23ebe6b2bff66c3",
                last_commit_date=datetime.fromisoformat(
                    "2020-11-16T15:41:04+00:00"
                ),
                loc=243,
                modified_by="machine@fluidattacks.com",
                modified_date=datetime.fromisoformat(
                    "2020-11-16T15:41:04+00:00"
                ),
                seen_at=datetime.fromisoformat("2019-01-01T15:41:04+00:00"),
                sorts_risk_level=80,
            ),
        ),
    ]
    na_toe_lines = _get_na_toe_lines(group_toe_lines)

    _set_quintile_info(
        group_name="group1",
        quintile="na",
        quintile_toe_lines=na_toe_lines,
        user_data=user_data,
    )
    assert user_data["coverage_na"]["files"]["verified"] == 1
    assert user_data["coverage_na"]["files"]["total"] == 2
    assert user_data["coverage_na"]["lines"]["verified"] == 4324
    assert user_data["coverage_na"]["lines"]["total"] == 4567


@pytest.mark.asyncio
@pytest.mark.parametrize(
    ["user_data"],
    [
        [
            {
                "coverage_na": {
                    "files": {
                        "verified": 0,
                        "total": 0,
                        "percentage": 0,
                    },
                    "lines": {
                        "verified": 0,
                        "total": 0,
                        "percentage": 0,
                    },
                },
                "groups": {
                    "group1": {
                        "coverage_na": {
                            "files_total": 0,
                            "lines_total": 0,
                        },
                    },
                },
            },
        ]
    ],
)
async def test_set_na_coverage_info_except(
    user_data: dict[str, Any],
) -> None:
    group_toe_lines: list[ToeLine] = [
        ToeLine(
            filename="test1/test.sh",
            group_name="group1",
            root_id="63298a73-9dff-46cf-b42d-9b2f01a56690",
            state=ToeLineState(
                attacked_at=None,
                attacked_by="test@test.com",
                attacked_lines=4324,
                be_present=False,
                be_present_until=datetime.fromisoformat(
                    "2021-01-19T15:41:04+00:00"
                ),
                comments="comment 1",
                first_attack_at=datetime.fromisoformat(
                    "2020-01-19T15:41:04+00:00"
                ),
                modified_by="test@test.com",
                modified_date=datetime.fromisoformat(
                    "2021-11-16T15:41:04+00:00"
                ),
                has_vulnerabilities=False,
                last_author="customer1@gmail.com",
                last_commit="f9e4beba70c4f34d6117c3b0c23ebe6b2bff66c1",
                last_commit_date=datetime.fromisoformat(
                    "2021-11-16T15:41:04+00:00"
                ),
                loc=4324,
                seen_at=datetime.fromisoformat("2020-01-01T15:41:04+00:00"),
                sorts_risk_level=1,
                sorts_priority_factor=1,
            ),
        ),
    ]
    na_toe_lines = _get_na_toe_lines(group_toe_lines)

    _set_quintile_info(
        group_name="group1",
        quintile="na",
        quintile_toe_lines=na_toe_lines,
        user_data=user_data,
    )
    assert user_data["coverage_na"]["files"]["total"] == 0
    assert user_data["coverage_na"]["lines"]["total"] == 0


@pytest.mark.asyncio
@pytest.mark.parametrize(
    [
        "content",
    ],
    [
        [
            {
                "enumerated_inputs": {
                    "counter": {
                        "past_day": 4,
                        "today": 10,
                    }
                },
                "verified_inputs": {
                    "counter": {
                        "past_day": 3,
                        "today": 5,
                    }
                },
                "enumerated_ports": {
                    "counter": {
                        "past_day": 4,
                        "today": 10,
                    }
                },
                "verified_ports": {
                    "counter": {
                        "past_day": 3,
                        "today": 5,
                    }
                },
                "loc": {
                    "counter": {
                        "past_day": 4,
                        "today": 5,
                    }
                },
                "reattacked": {
                    "counter": {
                        "past_day": 3,
                        "today": 2,
                    }
                },
                "vulnerable": {
                    "counter": {
                        "past_day": 1,
                        "today": 2,
                    }
                },
                "submitted": {
                    "counter": {
                        "past_day": 2,
                        "today": 3,
                    }
                },
                "coverage_q1": {
                    "files": {
                        "verified": 10,
                        "total": 200,
                        "percentage": 5,
                    },
                    "lines": {
                        "verified": 5,
                        "total": 50,
                        "percentage": 10,
                    },
                },
                "coverage_q2": {
                    "files": {
                        "verified": 10,
                        "total": 200,
                        "percentage": 5,
                    },
                    "lines": {
                        "verified": 5,
                        "total": 50,
                        "percentage": 10,
                    },
                },
                "coverage_q3": {
                    "files": {
                        "verified": 10,
                        "total": 200,
                        "percentage": 5,
                    },
                    "lines": {
                        "verified": 5,
                        "total": 50,
                        "percentage": 10,
                    },
                },
                "coverage_q4": {
                    "files": {
                        "verified": 10,
                        "total": 200,
                        "percentage": 5,
                    },
                    "lines": {
                        "verified": 5,
                        "total": 50,
                        "percentage": 10,
                    },
                },
                "coverage_q5": {
                    "files": {
                        "verified": 10,
                        "total": 200,
                        "percentage": 5,
                    },
                    "lines": {
                        "verified": 5,
                        "total": 50,
                        "percentage": 10,
                    },
                },
                "coverage_na": {
                    "files": {
                        "verified": 50,
                        "total": 500,
                        "percentage": 10,
                    },
                    "lines": {
                        "verified": 90,
                        "total": 300,
                        "percentage": 30,
                    },
                },
                "max_cvss": {"vulnerable": 0.0, "submitted": 0.0},
                "groups": {
                    "unittesting": {
                        "verified_inputs": 6,
                        "verified_ports": 6,
                        "enumerated_inputs": 3,
                        "enumerated_ports": 3,
                        "loc": 0,
                        "coverage_q1": {"files": 10, "lines": 48},
                        "coverage_q2": {"files": 5, "lines": 59},
                        "coverage_q3": {"files": 90, "lines": 82},
                        "coverage_q4": {"files": 54, "lines": 17},
                        "coverage_q5": {"files": 10, "lines": 35},
                        "coverage_na": {"files": 10, "lines": 23},
                    },
                    "test_group": {
                        "verified_inputs": 4,
                        "verified_ports": 4,
                        "enumerated_inputs": 2,
                        "enumerated_ports": 2,
                        "loc": 0,
                    },
                },
            },
        ],
    ],
)
async def test_send_mail_numerator_report(
    content: dict[str, Any],
) -> None:
    with patch(
        "integrates.schedulers.numerator_report_digest.mail_numerator_report",
        new_callable=AsyncMock,
    ) as mock_mail_numerator_report:
        mock_mail_numerator_report.return_value = True
        await _send_mail_report(
            content=content,
            report_date=datetime.fromisoformat(
                "2022-07-08T06:00:00+00:00"
            ).date(),
            responsible="integratesmanager@gmail.com",
        )
    assert mock_mail_numerator_report.called is True


@pytest.mark.asyncio
async def test_main() -> None:
    with patch(
        MODULE_AT_TEST + "send_numerator_report",
        new_callable=AsyncMock,
    ) as mock:
        await main()
        mock.assert_awaited_once()


@pytest.mark.parametrize(
    ["users", "toe_types", "group_names", "dates", "output_lengths"],
    [
        [
            ["test2@test.com", "test2@test.com", "test2@test.com"],
            [ToeType.INPUT, ToeType.LINE, ToeType.PORT],
            ["unittesting", "unittesting", "oneshottest"],
            [
                "2021-02-12T00:00:00+00:00",
                "2021-02-21T00:00:00+00:00",
                "2022-12-16T00:00:00+00:00",
            ],
            [2, 564, 0],
        ],
    ],
)
async def test_paginated_toes_processing(
    users: str,
    toe_types: list[ToeType],
    group_names: list[str],
    dates: list[str],
    output_lengths: list[int],
) -> None:
    for item, toe_type in enumerate(toe_types):
        content: dict[str, Any] = {}
        toe_lines: dict[str, Any] = {}
        toe_lines[group_names[item]] = []
        await _paginated_toes_processing(
            CountReportInfo(
                allowed_users=[users[item]],
                content=content,
                group=group_names[item],
                report_dates=get_report_dates(
                    datetime.fromisoformat(dates[item])
                ),
            ),
            toe_type,
            1,
            toe_lines,
        )
        if toe_type == ToeType.INPUT:
            assert (
                content[users[item]]["groups"][group_names[item]][
                    "verified_inputs"
                ]
                == output_lengths[item]
            )
            assert (
                content[users[item]]["verified_inputs"]["counter"]["today"]
                == output_lengths[item]
            )
        elif toe_type == ToeType.LINE:
            assert (
                content[users[item]]["groups"][group_names[item]]["loc"]
                == output_lengths[item]
            )
            assert (
                content[users[item]]["loc"]["counter"]["today"]
                == output_lengths[item]
            )


async def test_paginated_toes_processing_all_toes() -> None:
    group_name = "unittesting"
    content: dict[str, Any] = {}
    all_toe_lines: dict[str, Any] = {}
    all_toe_lines[group_name] = []
    with patch(MODULE_AT_TEST + "toes_counter") as mock:
        await _paginated_toes_processing(
            CountReportInfo(
                allowed_users=["test2@test.com"],
                content=content,
                group="unittesting",
                report_dates=get_report_dates(
                    datetime.fromisoformat("2021-02-21T00:00:00+00:00")
                ),
            ),
            ToeType.LINE,
            1,
            all_toe_lines,
            all_current_toes=True,
        )
        mock.assert_not_called()
        assert len(all_toe_lines[group_name]) == 5


def test_get_coverage() -> None:
    assert _get_coverage(0, 10) == "N/A"
    assert _get_coverage(10, 0) == "0%"
    assert _get_coverage(100, 50) == "50%"
    assert _get_coverage(100000, 50) == "0%"
    assert _get_coverage(-10, 10) == "-100%"
