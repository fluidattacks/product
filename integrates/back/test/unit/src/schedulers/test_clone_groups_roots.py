from integrates.schedulers.clone_groups_roots import (
    clone_groups_roots,
)
from integrates.schedulers.common import (
    is_machine_target,
)
import pytest
from test.unit.src.utils import (
    get_module_at_test,
)
from typing import (
    Any,
)
from unittest.mock import (
    AsyncMock,
    call,
    patch,
)

MODULE_AT_TEST = f"integrates.{get_module_at_test(file_path=__file__)}"

pytestmark = [
    pytest.mark.asyncio,
]


@patch(MODULE_AT_TEST + "get_new_context", return_value=None)
@patch(
    MODULE_AT_TEST + "orgs_domain.get_all_active_groups",
    new_callable=AsyncMock,
)
@patch(MODULE_AT_TEST + "_queue_sync_git_roots", new_callable=AsyncMock)
async def test_clone_groups_roots(
    mock_queue_sync_git_roots: AsyncMock,
    mock_orgs_domain_get_all_active_groups: AsyncMock,
    mock_loaders: AsyncMock,
    mocked_data_for_module: Any,
) -> None:
    mock_orgs_domain_get_all_active_groups.return_value = (
        mocked_data_for_module(
            mock_path="orgs_domain.get_all_active_groups",
            mock_args=[],
            module_at_test=MODULE_AT_TEST,
        )
    )

    await clone_groups_roots()
    mock_orgs_domain_get_all_active_groups.assert_awaited_once_with(
        mock_loaders.return_value
    )
    machine_groups: list[str] = [
        group.name
        for group in mock_orgs_domain_get_all_active_groups.return_value
        if is_machine_target(group)
    ]
    args = [
        call(loaders=mock_loaders.return_value, group_name=group)
        for group in machine_groups
    ]
    mock_queue_sync_git_roots.assert_has_awaits(args)


