from datetime import (
    datetime,
)
from decimal import (
    Decimal,
)
from typing import (
    Any,
)
from unittest.mock import (
    AsyncMock,
    patch,
)

import pytest
from freezegun import (
    freeze_time,
)

from integrates.dataloaders import (
    get_new_context,
)
from integrates.db_model.enums import (
    Source,
    StateRemovalJustification,
    TreatmentStatus,
)
from integrates.db_model.findings.enums import (
    FindingSorts,
    FindingStateStatus,
    FindingStatus,
)
from integrates.db_model.findings.types import (
    Finding,
    FindingEvidences,
    FindingState,
    FindingUnreliableIndicators,
    FindingVerificationSummary,
)
from integrates.db_model.types import (
    SeverityScore,
    Treatment,
    TreatmentToUpdate,
)
from integrates.db_model.vulnerabilities.enums import (
    VulnerabilityStateStatus,
    VulnerabilityTechnique,
    VulnerabilityToolImpact,
    VulnerabilityType,
)
from integrates.db_model.vulnerabilities.types import (
    Vulnerability,
    VulnerabilityState,
    VulnerabilityTool,
)
from integrates.schedulers.reset_expired_accepted_findings import (
    main,
    process_finding,
    process_group,
    process_vulnerability,
)
from test.unit.src.utils import (
    get_module_at_test,
)

MODULE_AT_TEST = f"integrates.{get_module_at_test(file_path=__file__)}"

pytestmark = [
    pytest.mark.asyncio,
]


@pytest.mark.parametrize(
    ["group_name", "progress"],
    [["lubbock", 0.0]],
)
@patch(MODULE_AT_TEST + "process_finding", new_callable=AsyncMock)
@patch(
    MODULE_AT_TEST + "Dataloaders.group_findings",
    new_callable=AsyncMock,
)
async def test_process_group(
    mock_loaders_group_findings: AsyncMock,
    mock_process_finding: AsyncMock,
    group_name: str,
    progress: float,
    mocked_data_for_module: Any,
) -> None:
    # Set up mock's result using mocked_data_for_module fixture
    mock_loaders_group_findings.load.return_value = mocked_data_for_module(
        mock_path="Dataloaders.group_findings",
        mock_args=[group_name],
        module_at_test=MODULE_AT_TEST,
    )

    # Functions inside collect have to be mocked using side_effect
    # so that the iterations work
    mock_process_finding.side_effect = mocked_data_for_module(
        mock_path="process_finding",
        mock_args=[group_name],
        module_at_test=MODULE_AT_TEST,
    )
    await process_group(group_name=group_name, progress=progress)
    assert isinstance(progress, float)
    assert mock_loaders_group_findings.load.called is True
    assert mock_process_finding.called is True


@freeze_time("2023-09-08T14:30:00Z")
@pytest.mark.parametrize(
    ["vulnerability"],
    [
        [
            Vulnerability(
                created_by="unittest@fluidattacks.com",
                created_date=datetime.fromisoformat(
                    "2020-04-08T00:45:15+00:00"
                ),
                finding_id="818828206",
                group_name="lubbock",
                organization_name="tatsumi",
                hacker_email="unittest@fluidattacks.com",
                id="46b84d52-1b18-41fa-84b5-bcb8134cb1ec",
                state=VulnerabilityState(
                    modified_by="unittest@fluidattacks.com",
                    modified_date=datetime.fromisoformat(
                        "2020-08-07T13:45:48+00:00"
                    ),
                    source=Source.ASM,
                    specific="9999",
                    status=VulnerabilityStateStatus.SAFE,
                    where="192.168.1.20",
                    commit=None,
                    reasons=None,
                    other_reason=None,
                    tool=VulnerabilityTool(
                        name="tool-1",
                        impact=VulnerabilityToolImpact.INDIRECT,
                    ),
                ),
                technique=VulnerabilityTechnique.PTAAS,
                type=VulnerabilityType.PORTS,
                bug_tracking_system_url=None,
                custom_severity=None,
                developer=None,
                event_id=None,
                hash=None,
                root_id="4039d098-ffc5-4984-8ed3-eb17bca98e19",
                skims_method=None,
                stream=None,
                tags=None,
                treatment=Treatment(
                    modified_date=datetime.fromisoformat(
                        "2021-11-08T00:59:06+00:00"
                    ),
                    status=TreatmentStatus.ACCEPTED,
                    acceptance_status=None,
                    accepted_until=datetime.fromisoformat(
                        "2022-04-08T00:59:06+00:00"
                    ),
                    justification="test justification temporarily accepted",
                    assigned="integratesuser2@gmail.com",
                    modified_by="integratesuser@gmail.com",
                ),
                verification=None,
                zero_risk=None,
            ),
        ],
        [
            Vulnerability(
                created_by="hacker@gmail.com",
                created_date=datetime.fromisoformat(
                    "2018-04-08T00:45:15+00:00"
                ),
                finding_id="4574973146",
                group_name="unittesting",
                organization_name="okada",
                hacker_email="hacker@gmail.com",
                id="be09edb7-cd5c-47ed-bee4-97c645acdce13",
                root_id="4039d098-ffc5-4984-8ed3-eb17bca98e19",
                state=VulnerabilityState(
                    modified_by="hacker@gmail.com",
                    modified_date=datetime.fromisoformat(
                        "2018-04-08T00:45:15+00:00"
                    ),
                    source=Source.ASM,
                    specific="9999",
                    status=VulnerabilityStateStatus.SUBMITTED,
                    where="192.168.1.20",
                ),
                technique=VulnerabilityTechnique.PTAAS,
                type=VulnerabilityType.PORTS,
            ),
        ],
    ],
)
@patch(
    MODULE_AT_TEST + "vulns_domain.add_vulnerability_treatment",
    new_callable=AsyncMock,
)
async def test_process_vulnerability(
    mock_vulns_domain_add_vulnerability_treatment: AsyncMock,
    vulnerability: Vulnerability,
) -> None:
    vuln_id = await process_vulnerability(vulnerability)

    if vulnerability.treatment:
        assert vuln_id
        mock_vulns_domain_add_vulnerability_treatment.assert_awaited_once_with(
            modified_by="integratesuser@gmail.com",
            treatment=TreatmentToUpdate(
                accepted_until=None,
                acceptance_status=None,
                assigned=None,
                justification="Expired accepted treatment",
                status=TreatmentStatus.UNTREATED,
            ),
            vulnerability=vulnerability,
        )
    else:
        assert not vuln_id


@pytest.mark.parametrize(
    ["finding"],
    [
        [
            Finding(
                group_name="lubbock",
                id="818828206",
                state=FindingState(
                    modified_by="integratesmanager@gmail.com",
                    modified_date=datetime.fromisoformat(
                        "2019-11-24T05:00:00+00:00"
                    ),
                    source=Source.ASM,
                    status=FindingStateStatus.CREATED,
                    rejection=None,
                    justification=StateRemovalJustification.NO_JUSTIFICATION,
                ),
                title="001. SQL injection - C Sharp SQL API",
                attack_vector_description="",
                creation=FindingState(
                    modified_by="integratesmanager@gmail.com",
                    modified_date=datetime.fromisoformat(
                        "2019-11-22T20:07:57+00:00"
                    ),
                    source=Source.ASM,
                    status=FindingStateStatus.CREATED,
                    rejection=None,
                    justification=StateRemovalJustification.NO_JUSTIFICATION,
                ),
                description="Se generan sentencias SQL dinámicas sin la "
                "validación requerida de datos y sin utilizar sentencias "
                "parametrizadas o procedimientos almacenados.",
                evidences=FindingEvidences(
                    animation=None,
                    evidence1=None,
                    evidence2=None,
                    evidence3=None,
                    evidence4=None,
                    evidence5=None,
                    exploitation=None,
                    records=None,
                ),
                min_time_to_remediate=18,
                recommendation="Realizar las consultas a la base de datos por "
                "medio de sentencias o procedimientos parametrizados.",
                requirements="REQ.0169. Debe usarse construcciones "
                "parametrizadas o procedimientos almacenados "
                "parametrizados para la creación dinámica de sentencias "
                "(ej: java.sql.PreparedStatement)",
                severity_score=SeverityScore(
                    base_score=Decimal("4.5"),
                    temporal_score=Decimal("4.1"),
                    cvss_v3="CVSS:3.1/AV:P/AC:H/PR:L/UI:N/S:C/C:L/I:L/A:L/"
                    "E:P/RL:O/CR:L/AR:H/MAV:N/MAC:H/MPR:H/MUI:R/MS:U/MC:L/"
                    "MA:L",
                    threat_score=Decimal("1.1"),
                    cvssf_v4=Decimal("0.018"),
                    cvss_v4="CVSS:4.0/AV:P/AC:H/AT:N/PR:L/UI:N/VC:L/VI:L/"
                    "VA:L/SC:L/SI:L/SA:L/E:P/AR:H/MAV:N/MAC:H/MPR:H/MUI:P"
                    "/MVC:L/MVA:L",
                    cvssf=Decimal("1.149"),
                ),
                sorts=FindingSorts.NO,
                threat="",
                unfulfilled_requirements=["169", "173"],
                unreliable_indicators=FindingUnreliableIndicators(
                    unreliable_newest_vulnerability_report_date=(
                        datetime.fromisoformat("2019-12-12T13:45:48+00:00")
                    ),
                    unreliable_oldest_open_vulnerability_report_date=(
                        datetime.fromisoformat("2019-12-12T13:45:48+00:00")
                    ),
                    unreliable_status=FindingStatus.VULNERABLE,
                    unreliable_where="",
                    verification_summary=FindingVerificationSummary(
                        requested=0, on_hold=0, verified=0
                    ),
                ),
                verification=None,
            )
        ],
    ],
)
@patch(MODULE_AT_TEST + "process_vulnerability", new_callable=AsyncMock)
@patch(
    MODULE_AT_TEST + "update_unreliable_indicators_by_deps",
    new_callable=AsyncMock,
)
async def test_process_finding(
    mock_process_vulnerability: AsyncMock,
    mock_update_unreliable_indicators_by_deps: AsyncMock,
    mocked_data_for_module: Any,
    finding: Finding,
) -> None:
    loaders: Any = get_new_context()
    loaders.finding_vulnerabilities.load = AsyncMock(
        return_value=mocked_data_for_module(
            mock_path="Dataloaders.finding_vulnerabilities.load",
            mock_args=[finding.id],
            module_at_test=MODULE_AT_TEST,
        )
    )

    mock_process_vulnerability.side_effect = mocked_data_for_module(
        mock_path="process_vulnerability",
        mock_args=[],
        module_at_test=MODULE_AT_TEST,
    )

    await process_finding(loaders=loaders, finding=finding)
    mock_process_vulnerability.assert_called_once()
    mock_update_unreliable_indicators_by_deps.assert_called_once()


@patch(MODULE_AT_TEST + "process_group", new_callable=AsyncMock)
@patch(
    MODULE_AT_TEST + "orgs_domain.get_all_active_group_names",
    new_callable=AsyncMock,
)
async def test_reset_expired_accepted_findings(
    mock_orgs_domain_get_all_active_group_names: AsyncMock,
    mock_process_group: AsyncMock,
    mocked_data_for_module: Any,
) -> None:
    mock_orgs_domain_get_all_active_group_names.return_value = (
        mocked_data_for_module(
            mock_path="orgs_domain.get_all_active_group_names",
            mock_args=[],
            module_at_test=MODULE_AT_TEST,
        )
    )

    mock_process_group.side_effect = mocked_data_for_module(
        mock_path="process_group",
        mock_args=[],
        module_at_test=MODULE_AT_TEST,
    )
    await main()
    assert mock_orgs_domain_get_all_active_group_names.called is True
    assert mock_process_group.call_count == 11


@pytest.mark.asyncio
async def test_main() -> None:
    with patch(
        MODULE_AT_TEST + "reset_expired_accepted_findings",
        new_callable=AsyncMock,
    ) as mock:
        await main()
        mock.assert_awaited_once()
