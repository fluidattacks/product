from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model.groups.enums import (
    GroupStateStatus,
)
from integrates.organizations import (
    domain as orgs_domain,
)
from integrates.schedulers import (
    delete_obsolete_groups,
)
import pytest
from pytest_mock import (
    MockerFixture,
)

pytestmark = [
    pytest.mark.asyncio,
]


@pytest.mark.changes_db
async def test_remove_obsolete_groups(mocker: MockerFixture) -> None:
    mock_sendgrid_client = mocker.Mock()
    mocker.patch(
        "integrates.mailer.common.SendGridAPIClient",
        return_value=mock_sendgrid_client,
    )
    loaders: Dataloaders = get_new_context()
    test_group_name_1 = "setpendingdeletion"
    test_group_name_2 = "deletegroup"
    all_active_groups_names = await orgs_domain.get_all_active_group_names(
        loaders
    )
    assert test_group_name_1 in all_active_groups_names
    assert test_group_name_2 in all_active_groups_names

    await delete_obsolete_groups.main()

    loaders = get_new_context()
    all_active_groups_names = await orgs_domain.get_all_active_group_names(
        loaders
    )
    assert test_group_name_1 in all_active_groups_names
    assert test_group_name_2 not in all_active_groups_names

    test_group_1 = await loaders.group.load(test_group_name_1)
    assert test_group_1
    assert test_group_1.state.status == GroupStateStatus.ACTIVE
    assert test_group_1.state.pending_deletion_date

    assert not await loaders.group.load(test_group_name_2)
