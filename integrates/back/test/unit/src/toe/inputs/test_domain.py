from aioextensions import (
    collect,
)
from datetime import (
    datetime,
    timezone,
)
from freezegun import (
    freeze_time,
)
from integrates.custom_utils.roots import (
    get_root_vulnerabilities,
)
from integrates.dataloaders import (
    get_new_context,
)
from integrates.db_model.roots.enums import (
    RootEnvironmentUrlStateStatus,
    RootEnvironmentUrlType,
)
from integrates.db_model.roots.types import (
    RootEnvironmentUrl,
    RootEnvironmentUrlRequest,
    RootEnvironmentUrlState,
)
from integrates.db_model.toe_inputs.types import (
    ToeInput,
    ToeInputMetadataToUpdate,
    ToeInputState,
    ToeInputUpdate,
)
from integrates.db_model.vulnerabilities.enums import (
    VulnerabilityStateStatus,
)
from integrates.toe.inputs.domain import (
    add,
    get_reduced_component,
    get_root_env_toe_inputs,
    modify_vulns_in_input_exclusion,
    update,
)
from integrates.toe.inputs.types import (
    ToeInputAttributesToAdd,
    ToeInputAttributesToUpdate,
)
import pytest
from test.unit.src.utils import (
    get_module_at_test,
    set_mocks_return_values,
)
from typing import (
    Any,
)
from unittest.mock import (
    AsyncMock,
    patch,
)

MODULE_AT_TEST = f"integrates.{get_module_at_test(file_path=__file__)}"

pytestmark = [
    pytest.mark.asyncio,
]


@pytest.mark.parametrize(
    [
        "component",
        "entry_point",
        "expected",
    ],
    [
        [
            "https://test.com/test/test.aspx",
            "btnTest",
            "test.com/test/test.aspx/btnTest",
        ],
        [
            "https://app.fluidattacks.com:8080/test",
            "idTest",
            "app.fluidattacks.com/test/idTest",
        ],
    ],
)
def test_get_reduced_component(
    component: str, entry_point: str, expected: str
) -> None:
    reduced = get_reduced_component(component, entry_point)
    assert reduced == expected


@pytest.mark.parametrize(
    [
        "group_name",
        "component",
        "entry_point",
        "environment_id",
        "root_id",
        "attributes",
        "is_moving_toe_input",
    ],
    [
        [
            "unittesting",
            "https://test.com/test/new.aspx",
            "btnTest",
            "b83beb1ed10e7ba4166aa3d6af0835bf57604801",
            "63298a73-9dff-46cf-b42d-9b2f01a56690",
            ToeInputAttributesToAdd(
                attacked_at=datetime.fromisoformat(
                    "2021-02-12T05:00:00+00:00"
                ),
                attacked_by="test@test.com",
                be_present=True,
                has_vulnerabilities=False,
                first_attack_at=datetime.fromisoformat(
                    "2021-02-12T05:00:00+00:00"
                ),
                seen_at=datetime.fromisoformat("2000-01-01T05:00:00+00:00"),
                seen_first_time_by="new@test.com",
            ),
            True,
        ],
        [
            "unittesting",
            "https://test.com/test/new.aspx",
            "btnTest",
            "a83beb1ed10e7ba4166aa3d6af0835bf57604801",
            "4039d098-ffc5-4984-8ed3-eb17bca98e19",
            ToeInputAttributesToAdd(
                attacked_at=datetime.fromisoformat(
                    "2021-02-12T05:00:00+00:00"
                ),
                attacked_by="test@test.com",
                be_present=True,
                has_vulnerabilities=False,
                first_attack_at=datetime.fromisoformat(
                    "2021-02-12T05:00:00+00:00"
                ),
                seen_at=datetime.fromisoformat("2000-01-01T05:00:00+00:00"),
                seen_first_time_by="new@test.com",
            ),
            False,
        ],
    ],
)
@patch(MODULE_AT_TEST + "toe_inputs_model.add", new_callable=AsyncMock)
@patch(MODULE_AT_TEST + "validate_toe_input", new_callable=AsyncMock)
@patch(MODULE_AT_TEST + "roots_utils.get_root", new_callable=AsyncMock)
@freeze_time("2022-11-11T05:00:00+00:00")
async def test_add(
    mock_roots_utils_get_root: AsyncMock,
    mock_validate_toe_input: AsyncMock,
    mock_toe_inputs_model_add: AsyncMock,
    *,
    group_name: str,
    component: str,
    entry_point: str,
    environment_id: str,
    root_id: str,
    attributes: ToeInputAttributesToAdd,
    is_moving_toe_input: bool,
) -> None:
    if is_moving_toe_input:
        assert set_mocks_return_values(
            mocks_args=[[group_name, component, entry_point, attributes]],
            mocked_objects=[mock_toe_inputs_model_add],
            module_at_test=MODULE_AT_TEST,
            paths_list=["toe_inputs_model.add"],
        )
    else:
        mocked_objects, mocked_paths = [
            [
                mock_roots_utils_get_root,
                mock_validate_toe_input,
                mock_toe_inputs_model_add,
            ],
            [
                "roots_utils.get_root",
                "validate_toe_input",
                "toe_inputs_model.add",
            ],
        ]
        mocks_args: list[list[Any]] = [
            [root_id, group_name],
            [root_id, group_name, component],
            [group_name, component, entry_point, attributes],
        ]
        assert set_mocks_return_values(
            mocks_args=mocks_args,
            mocked_objects=mocked_objects,
            module_at_test=MODULE_AT_TEST,
            paths_list=mocked_paths,
        )
    loaders = get_new_context()
    await add(
        loaders=loaders,
        entry_point=entry_point,
        environment_id=environment_id,
        component=component,
        group_name=group_name,
        root_id=root_id,
        attributes=attributes,
        is_moving_toe_input=is_moving_toe_input,
    )
    if is_moving_toe_input:
        assert mock_toe_inputs_model_add.called
    else:
        assert all(
            mock_object.called is True for mock_object in mocked_objects
        )


@pytest.mark.parametrize(
    [
        "current_value",
        "attributes",
        "modified_by",
        "is_moving_toe_input",
    ],
    [
        [
            ToeInput(
                component="https://test.com/test/test.aspx",
                entry_point="btnTest",
                environment_id="",
                group_name="unittesting",
                root_id="",
                state=ToeInputState(
                    attacked_at=datetime.fromisoformat(
                        "2021-02-02T05:00:00+00:00"
                    ),
                    attacked_by="test@test.com",
                    be_present=False,
                    be_present_until=datetime.fromisoformat(
                        "2021-03-20T15:41:04+00:00"
                    ),
                    first_attack_at=datetime.fromisoformat(
                        "2021-01-02T05:00:00+00:00"
                    ),
                    has_vulnerabilities=False,
                    modified_by="test2@test.com",
                    modified_date=datetime.fromisoformat(
                        "2021-02-11T05:00:00+00:00"
                    ),
                    seen_at=datetime.fromisoformat(
                        "2020-03-14T05:00:00+00:00"
                    ),
                    seen_first_time_by="test@test.com",
                ),
            ),
            ToeInputAttributesToUpdate(
                attacked_at=datetime.fromisoformat(
                    "2021-02-12T05:00:00+00:00"
                ),
                attacked_by="",
                be_present=True,
                first_attack_at=datetime.fromisoformat(
                    "2021-02-12T05:00:00+00:00"
                ),
                has_vulnerabilities=False,
                seen_at=datetime.fromisoformat("2000-01-01T05:00:00+00:00"),
                seen_first_time_by="edited@test.com",
            ),
            "edited@test.com",
            True,
        ],
    ],
)
@patch(
    MODULE_AT_TEST + "toe_inputs_model.update_state", new_callable=AsyncMock
)
@freeze_time("2022-11-11T15:00:00+00:00")
async def test_update(
    mock_toe_inputs_model_update_state: AsyncMock,
    current_value: ToeInput,
    attributes: ToeInputAttributesToUpdate,
    modified_by: str,
    is_moving_toe_input: bool,
) -> None:
    assert set_mocks_return_values(
        mocks_args=[[current_value, attributes, modified_by]],
        mocked_objects=[mock_toe_inputs_model_update_state],
        module_at_test=MODULE_AT_TEST,
        paths_list=["toe_inputs_model.update_state"],
    )

    await update(
        loaders=get_new_context(),
        current_value=current_value,
        attributes=attributes,
        modified_by=modified_by,
        is_moving_toe_input=is_moving_toe_input,
    )

    mock_toe_inputs_model_update_state.assert_called_with(
        ToeInputUpdate(
            current_value=current_value,
            new_state=ToeInputState(
                attacked_at=datetime.fromisoformat(
                    "2021-02-12T05:00:00+00:00"
                ),
                attacked_by="",
                be_present=True,
                be_present_until=None,
                first_attack_at=datetime.fromisoformat(
                    "2021-02-12T05:00:00+00:00"
                ),
                has_vulnerabilities=False,
                modified_by="edited@test.com",
                modified_date=datetime.fromisoformat(
                    "2022-11-11T15:00:00+00:00"
                ),
                seen_at=datetime.fromisoformat("2000-01-01T05:00:00+00:00"),
                seen_first_time_by="edited@test.com",
            ),
            metadata=ToeInputMetadataToUpdate(
                clean_attacked_at=False,
                clean_be_present_until=True,
                clean_first_attack_at=False,
                clean_seen_at=False,
            ),
        )
    )


@pytest.mark.parametrize(
    [
        "url_env",
        "expected",
    ],
    [
        [
            RootEnvironmentUrl(
                url="https://test.com",
                id="ba1598f6cf5e8512cd7d776d5dd8a5f79a4fed39",
                root_id="4039d098-ffc5-4984-8ed3-eb17bca98e19",
                group_name="unittesting",
                state=RootEnvironmentUrlState(
                    modified_by="integrates@fluidattacks.com",
                    modified_date=datetime(
                        2024,
                        3,
                        13,
                        17,
                        33,
                        52,
                        937480,
                        tzinfo=timezone.utc,
                    ),
                    status=RootEnvironmentUrlStateStatus.CREATED,
                    include=True,
                    url_type=RootEnvironmentUrlType.URL,
                    cloud_name=None,
                    use_egress=None,
                    use_vpn=None,
                    use_ztna=None,
                ),
                created_at=None,
                created_by=None,
            ),
            3,
        ],
    ],
)
async def test_get_root_env_toe_inputs(
    url_env: RootEnvironmentUrl, expected: int
) -> None:
    assert len(await get_root_env_toe_inputs(url_env)) == expected


@pytest.mark.parametrize(
    [
        "group_name",
        "root_id",
        "url_id",
    ],
    [
        [
            "unittesting",
            "4039d098-ffc5-4984-8ed3-eb17bca98e19",
            "ba1598f6cf5e8512cd7d776d5dd8a5f79a4fed39",
        ],
    ],
)
@freeze_time("2019-09-13")
async def test_modify_vulns_in_input_exclusion(
    group_name: str, root_id: str, url_id: str
) -> None:
    vulnerabilities_before = await get_root_vulnerabilities(root_id)
    initial_vulns_amount = len(
        [
            vulnerability
            for vulnerability in vulnerabilities_before
            if vulnerability.state.status == VulnerabilityStateStatus.SAFE
        ]
    )
    env_url = await get_new_context().environment_url.load(
        RootEnvironmentUrlRequest(
            root_id=root_id, group_name=group_name, url_id=url_id
        )
    )
    toe_inputs = await get_root_env_toe_inputs(env_url)
    await collect(
        [
            modify_vulns_in_input_exclusion(
                toe_input,
                vulnerabilities_before,
                "test@fluidattacks.com",
                loaders=get_new_context(),
            )
            for toe_input in toe_inputs
        ]
    )
    vulnerabilities_after = await get_root_vulnerabilities(root_id)
    assert len(vulnerabilities_before) == len(vulnerabilities_after) + 1
    assert (
        len(
            [
                vulnerability
                for vulnerability in vulnerabilities_after
                if vulnerability.state.status == VulnerabilityStateStatus.SAFE
            ]
        )
        == initial_vulns_amount + 1
    )
