from datetime import (
    datetime,
)
from integrates.db_model.roots.enums import (
    RootCloningStatus,
    RootCriticality,
    RootEnvironmentCloud,
    RootEnvironmentUrlStateStatus,
    RootStatus,
    RootType,
)
from integrates.db_model.roots.types import (
    GitRoot,
    GitRootCloning,
    GitRootState,
    RootEnvironmentUrl,
    RootEnvironmentUrlState,
    URLRoot,
    URLRootState,
)
import pytest
from typing import (
    Any,
    Callable,
)

MOCK_DATA: dict[str, dict[str, Any]] = {
    "test_validate_input_excluded_from_scope": {
        "git_root": GitRoot(
            cloning=GitRootCloning(
                modified_by="admin@gmail.com",
                modified_date=datetime.fromisoformat(
                    "2020-11-19T13:37:10+00:00"
                ),
                reason="root creation",
                status=RootCloningStatus("UNKNOWN"),
            ),
            created_by="admin@gmail.com",
            created_date=datetime.fromisoformat("2020-11-19T13:37:10+00:00"),
            group_name="group1",
            id="63298a73-9dff-46cf-b42d-9b2f01a56690",
            organization_name="orgtest",
            state=GitRootState(
                branch="master",
                criticality=RootCriticality.LOW,
                gitignore=["bower_components/*", "node_modules/*"],
                includes_health_check=True,
                modified_by="admin@gmail.com",
                modified_date=datetime.fromisoformat(
                    "2020-11-19T13:37:10+00:00"
                ),
                nickname="universe",
                other=None,
                reason=None,
                status=RootStatus.INACTIVE,
                url="https://gitlab.com/fluidattacks/universe",
            ),
            type=RootType.GIT,
        ),
        "root_env_urls": [
            RootEnvironmentUrl(
                url="https://test.test/GetList",
                id="8691a919992a590458748bc3e385deb82bbdc73a",
                root_id="63298a73-9dff-46cf-b42d-9b2f01a56690",
                group_name="group1",
                state=RootEnvironmentUrlState(
                    modified_by="admin@gmail.com",
                    modified_date=datetime.fromisoformat(
                        "2020-11-19T13:37:10+00:00"
                    ),
                    status=RootEnvironmentUrlStateStatus.CREATED,
                ),
            ),
            RootEnvironmentUrl(
                url="https://test.test",
                id="a83beb1ed10e7ba4166aa3d6af0835bf57604800",
                root_id="63298a73-9dff-46cf-b42d-9b2f01a56690",
                group_name="group1",
                state=RootEnvironmentUrlState(
                    modified_by="admin@gmail.com",
                    modified_date=datetime.fromisoformat(
                        "2020-11-19T13:37:10+00:00"
                    ),
                    status=RootEnvironmentUrlStateStatus.CREATED,
                ),
            ),
            RootEnvironmentUrl(
                url="https://test.com",
                id="b83beb1ed10e7ba4166aa3d6af0835bf57604801",
                root_id="63298a73-9dff-46cf-b42d-9b2f01a56690",
                group_name="group1",
                state=RootEnvironmentUrlState(
                    modified_by="admin@gmail.com",
                    modified_date=datetime.fromisoformat(
                        "2020-11-19T13:37:10+00:00"
                    ),
                    status=RootEnvironmentUrlStateStatus.CREATED,
                ),
            ),
        ],
        "root_env_urls_excluded": [
            RootEnvironmentUrl(
                url="https://test.test/GetList",
                id="8691a919992a590458748bc3e385deb82bbdc73a",
                root_id="63298a73-9dff-46cf-b42d-9b2f01a56690",
                group_name="group1",
                state=RootEnvironmentUrlState(
                    modified_by="admin@gmail.com",
                    modified_date=datetime.fromisoformat(
                        "2020-11-19T13:37:10+00:00"
                    ),
                    status=RootEnvironmentUrlStateStatus.CREATED,
                    include=False,
                ),
            ),
            RootEnvironmentUrl(
                url="https://test_2.test",
                id="a83beb1ed10e7ba4166aa3d6af0835bf57604800",
                root_id="63298a73-9dff-46cf-b42d-9b2f01a56690",
                group_name="group1",
                state=RootEnvironmentUrlState(
                    modified_by="admin@gmail.com",
                    modified_date=datetime.fromisoformat(
                        "2020-11-19T13:37:10+00:00"
                    ),
                    status=RootEnvironmentUrlStateStatus.CREATED,
                    include=False,
                ),
            ),
        ],
        "url_root": URLRoot(
            created_by="jdoe@fluidattacks.com",
            created_date=datetime.fromisoformat("2020-11-19T13:45:55+00:00"),
            group_name="oneshottest",
            id="8493c82f-2860-4902-86fa-75b0fef76034",
            organization_name="okada",
            state=URLRootState(
                host="test.test",
                modified_by="jdoe@fluidattacks.com",
                modified_date=datetime.fromisoformat(
                    "2020-11-19T13:45:55+00:00"
                ),
                nickname="url_root_1",
                other=None,
                path="/GetList",
                port="443",
                protocol="HTTPS",
                reason=None,
                status=RootStatus.ACTIVE,
                query="=1234&page=1&start=56789",
            ),
            type=RootType.URL,
        ),
        "url_root_excluded": URLRoot(
            created_by="jdoe@fluidattacks.com",
            created_date=datetime.fromisoformat("2020-11-19T13:45:55+00:00"),
            group_name="oneshottest",
            id="8493c82f-2860-4902-86fa-75b0fef76034",
            organization_name="okada",
            state=URLRootState(
                host="test.test",
                modified_by="jdoe@fluidattacks.com",
                modified_date=datetime.fromisoformat(
                    "2020-11-19T13:45:55+00:00"
                ),
                nickname="url_root_1",
                other=None,
                path="/GetError",
                port="443",
                protocol="HTTPS",
                reason=None,
                status=RootStatus.ACTIVE,
                query=None,
                excluded_sub_paths=["excluded"],
            ),
            type=RootType.URL,
        ),
    },
    "test_validate_component_cloud": {
        "root_env_urls": [
            RootEnvironmentUrl(
                url="arn:aws:iam::10210120:role/CSPMRole",
                id="a83beb1ed10e7ba4166aa3d6af0835bf57604801",
                root_id="4039d098-ffc5-4984-8ed3-eb17bca98e19",
                group_name="oneshottest",
                state=RootEnvironmentUrlState(
                    modified_by="admin@gmail.com",
                    modified_date=datetime.fromisoformat(
                        "2020-11-19T13:37:10+00:00"
                    ),
                    status=RootEnvironmentUrlStateStatus.CREATED,
                    cloud_name=RootEnvironmentCloud("AWS"),
                ),
            ),
        ],
    },
}


@pytest.fixture
def mock_data_for_module(
    request: pytest.FixtureRequest,
) -> Callable[[str], Any]:
    def _mock_data_for_module(
        mock_name: str,
    ) -> Any:
        return MOCK_DATA[request.function.__name__][mock_name]

    return _mock_data_for_module
