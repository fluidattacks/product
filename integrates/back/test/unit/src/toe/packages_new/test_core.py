from datetime import datetime

import pytest

from integrates.custom_exceptions import ToePackageNotFound
from integrates.dataloaders import get_new_context
from integrates.db_model.toe_packages.types import (
    ToePackage,
    ToePackageCoordinates,
    ToePackageHealthMetadata,
)
from integrates.toe.packages.domain.core import (
    get_package,
)


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "toe_package",
    [
        ToePackage(
            id="f75fd9178b7945bf",
            be_present=True,
            group_name="unittesting",
            root_id="4039d098-ffc5-4984-8ed3-eb17bca98e19",
            name="commons-io:commons-io",
            version="2.7",
            type_="java-archive",
            language="java",
            modified_date=datetime.fromisoformat("2024-01-05T14:38:51.241508+00:00"),
            locations=[
                ToePackageCoordinates(
                    path="build.gradle",
                    line="88",
                    layer=None,
                    image_ref=None,
                )
            ],
            found_by="java-parse-gradle-lock",
            outdated=True,
            package_advisories=[],
            package_url="pkg:maven/commons-io/commons-io@2.7",
            platform="MAVEN",
            licenses=["MIT"],
            url="https://repo1.maven.org/maven2/commons-io/commons-io/2.7/",
            vulnerable=False,
            has_related_vulnerabilities=False,
            vulnerability_ids=None,
            health_metadata=ToePackageHealthMetadata(
                artifact=None,
                authors="Scott Sanders <sanders@apache.org>",
                latest_version_created_at="2024-04-08T16:54:45Z",
                latest_version="2.16.1",
            ),
        ),
    ],
)
async def test_get_package_success(toe_package: ToePackage) -> None:
    loaders = get_new_context()

    result = await get_package(
        loaders=loaders,
        group_name="unittesting",
        root_id="4039d098-ffc5-4984-8ed3-eb17bca98e19",
        name="commons-io:commons-io",
        version="2.7",
    )

    assert result == toe_package


@pytest.mark.asyncio
async def test_get_package_not_found() -> None:
    loaders = get_new_context()

    with pytest.raises(ToePackageNotFound):
        await get_package(
            loaders=loaders,
            group_name="test-group",
            root_id="root123",
            name="nonexistent-package",
            version="1.0.0",
        )
