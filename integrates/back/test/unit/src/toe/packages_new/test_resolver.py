from datetime import (
    datetime,
)
from unittest.mock import (
    patch,
)

import pytest

from integrates.api.resolvers.group.toe_packages import (
    safe_format_toe_package,
)
from integrates.db_model.toe_packages.types import (
    ToePackage,
    ToePackageCoordinates,
)


def test_items_to_packages_success() -> None:
    with patch(
        "integrates.api.resolvers.group.toe_packages.format_toe_package"
    ) as mock_format_toe_package:
        mock_format_toe_package.return_value = ToePackage(
            group_name="group",
            root_id="root",
            name="name",
            version="1.0",
            language="java",
            platform="MAVEN",
            package_url="pkg:maven/com.nimbusds/nimbus-jose-jwt@8.3",
            found_by="java-parse-gradle-lock",
            type_="java-archive",
            locations=[
                ToePackageCoordinates(
                    path="path1",
                    line="10",
                    dependency_type="DIRECT",
                    scope="PROD",
                )
            ],
            modified_date=datetime.fromisoformat(
                "2024-01-05T14:38:51.241508+00:00"
            ),
            be_present=True,
            id="nb931d6e982c13ff",
            vulnerable=False,
            vulnerability_ids=None,
        )

        items = {
            "item1": {
                "group_name": "group",
                "root_id": "root",
                "name": "name",
                "path": "path",
                "version": "1.0",
                "language": "java",
                "platform": "MAVEN",
                "found_by": "java-parse-gradle-lock",
                "type_": "java-archive",
                "package_url": "pkg:maven/com.nimbusds/nimbus-jose-jwt@8.3",
                "locations": [
                    {
                        "path": "path1",
                        "line": "10",
                        "dependency_type": "DIRECT",
                        "scope": "PROD",
                    }
                ],
                "modified_date": "2024-01-05T14:38:51.241508+00:00",
                "be_present": True,
                "id": "nb931d6e982c13ff",
                "vulnerable": False,
                "vulnerability_ids": None,
            }
        }

        result = [
            result
            for item in items.values()
            if (
                result := safe_format_toe_package(
                    item  # type: ignore [arg-type]
                )
            )
            is not None
        ]
        assert len(result) == 1
        assert isinstance(result[0], ToePackage)
        assert result[0].name == "name"


def test_items_to_packages_key_error() -> None:
    with patch(
        "integrates.api.resolvers.group.toe_packages.format_toe_package"
    ) as mock_format_toe_package:
        mock_format_toe_package.side_effect = KeyError("Missing key")

        items = {
            "item1": {
                "group_name": "group",
                "root_id": "root",
                "name": "name",
                "path": "path",
                "version": "1.0",
                "language": "java",
                "platform": "MAVEN",
                "found_by": "java-parse-gradle-lock",
                "type_": "java-archive",
                "package_url": "pkg:maven/com.nimbusds/nimbus-jose-jwt@8.3",
                "modified_date": "2024-01-05T14:38:51.241508+00:00",
                "be_present": True,
                "id": "nb931d6e982c13ff",
                "vulnerable": False,
                "vulnerability_ids": None,
                "locations": [
                    {
                        "path": "path1",
                        "line": 10,
                        "dependency_type": "DIRECT",
                        "scope": "PROD",
                    }
                ],
            }
        }

        result = [
            result
            for item in items.values()
            if (
                result := safe_format_toe_package(
                    item  # type: ignore [arg-type]
                )
            )
            is not None
        ]
        assert len(result) == 0


def test_items_to_packages_partial_success() -> None:
    with patch(
        "integrates.api.resolvers.group.toe_packages.format_toe_package"
    ) as mock_format_toe_package:

        def side_effect(item: dict) -> ToePackage:
            if item["name"] == "name1":
                return ToePackage(
                    group_name="group1",
                    root_id="root1",
                    name="name1",
                    version="1.0",
                    language="java",
                    platform="MAVEN",
                    package_url="pkg:maven/com.nimbusds"
                    "/nimbus-jose-jwt@8.3",
                    found_by="java-parse-gradle-lock",
                    type_="java-archive",
                    modified_date=datetime.fromisoformat(
                        "2024-01-05T14:38:51.241508+00:00"
                    ),
                    be_present=True,
                    id="nb931d6e982c13ff",
                    vulnerable=False,
                    vulnerability_ids=None,
                    locations=[
                        ToePackageCoordinates(
                            path="path1",
                            line="10",
                            dependency_type="DIRECT",
                            scope="PROD",
                        )
                    ],
                )
            raise KeyError("Missing key")

        mock_format_toe_package.side_effect = side_effect

        items = {
            "item1": {
                "group_name": "group1",
                "root_id": "root1",
                "name": "name1",
                "path": "path1",
                "version": "1.0",
                "language": "java",
                "platform": "MAVEN",
                "package_url": "pkg:maven/com.nimbusds/nimbus-jose-jwt@8.3",
                "found_by": "java-parse-gradle-lock",
                "type_": "java-archive",
                "modified_date": "2024-01-05T14:38:51.241508+00:00",
                "be_present": True,
                "id": "nb931d6e982c13ff",
                "vulnerable": False,
                "vulnerability_ids": None,
                "locations": [
                    {
                        "path": "path1",
                        "line": 10,
                        "dependency_type": "DIRECT",
                        "scope": "PROD",
                    }
                ],
            },
            "item2": {
                "group_name": "group2",
                "root_id": "root2",
                "name": "name2",
                "path": "path2",
                "version": "2.0",
                "language": "java",
                "platform": "MAVEN",
                "package_url": "pkg:maven/com.nimbusds/nimbus-jose-jwt@8.3",
                "found_by": "java-parse-gradle-lock",
                "type_": "java-archive",
                "modified_date": "2024-01-05T14:38:51.241508+00:00",
                "be_present": True,
                "id": "nb931d6e982c13ff",
                "vulnerable": False,
                "vulnerability_ids": None,
                "locations": [
                    {
                        "path": "path2",
                        "line": 20,
                        "dependency_type": "DIRECT",
                        "scope": "PROD",
                    }
                ],
            },
        }

        result = [
            result
            for item in items.values()
            if (
                result := safe_format_toe_package(
                    item  # type: ignore [arg-type]
                )
            )
            is not None
        ]
        assert len(result) == 1
        assert isinstance(result[0], ToePackage)
        assert result[0].name == "name1"


if __name__ == "__main__":
    pytest.main()
