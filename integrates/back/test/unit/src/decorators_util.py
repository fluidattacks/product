from aiohttp import (
    ClientSession,
)
from functools import (
    wraps,
)
from typing import (
    Any,
    Awaitable,
    Callable,
    cast,
    TypeVar,
)
from unittest.mock import (
    AsyncMock,
    MagicMock,
    patch,
)

T = TypeVar("T", bound=Callable[..., Awaitable[Any]])


def assert_called_with(
    *args: Any,
    **kwargs: Any,
) -> Callable[[AsyncMock | MagicMock], None]:
    def _assert_called_with(mock: AsyncMock | MagicMock) -> None:
        mock.assert_called_once_with(*args, **kwargs)

    return _assert_called_with


def patch_aiohttp_clientsession_post(
    json_return_value: Any,
    callback: Callable[[AsyncMock], None] | None = None,
) -> Callable[[T], T]:
    def wrapper(func: T) -> T:
        @wraps(func)
        async def decorated(*args: Any, **kwargs: Any) -> None:
            with patch.object(
                ClientSession,
                "post",
                return_value=AsyncMock(
                    __aenter__=AsyncMock(
                        return_value=AsyncMock(
                            json=AsyncMock(return_value=json_return_value)
                        )
                    ),
                    __aexit__=AsyncMock(return_value=None),
                ),
            ) as mock_post:
                await func(*args, **kwargs)
                if callback:
                    callback(mock_post)

        return cast(T, decorated)

    return wrapper


def patch_aiohttp_clientsession_get(
    json_return_value: dict,
) -> Callable[[T], T]:
    def wrapper(func: T) -> T:
        @wraps(func)
        async def decorated(*args: Any, **kwargs: Any) -> T:
            with patch.object(
                ClientSession,
                "get",
                return_value=AsyncMock(
                    __aenter__=AsyncMock(
                        return_value=AsyncMock(
                            json=AsyncMock(return_value=json_return_value)
                        )
                    ),
                ),
            ):
                return await func(*args, **kwargs)

        return cast(T, decorated)

    return wrapper
