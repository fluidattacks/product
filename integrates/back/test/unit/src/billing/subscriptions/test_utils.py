from datetime import (
    datetime,
)

import pytest

from integrates.billing.subscriptions.utils import (
    format_treli_tokens,
)
from integrates.billing.types import (
    CustomerTokens,
    TreliTokensApiResponse,
)
from integrates.db_model.organizations.enums import (
    OrganizationStateStatus,
)
from integrates.db_model.organizations.types import (
    Organization,
    OrganizationBilling,
    OrganizationState,
)
from integrates.db_model.types import (
    Policies,
)
from test.unit.src.utils import (
    get_module_at_test,
)

MODULE_AT_TEST = f"integrates.{get_module_at_test(file_path=__file__)}"

pytestmark = [
    pytest.mark.asyncio,
]


ORGANIZATION = Organization(
    created_by="testing@fluid.com",
    created_date=datetime.now(),
    id="1234567890",
    name="testorg",
    billing_information=[
        OrganizationBilling(
            billing_email="test@fluidattacks.com",
            customer_id="cus_1234567890",
            last_modified_by="test@fluidattacks.com",
            modified_at=datetime.now(),
            billed_groups=["group_name"],
            subscription_id=3232321,
        )
    ],
    country="Panama",
    policies=Policies(
        modified_by="testing@fluid.com",
        modified_date=datetime.now(),
    ),
    state=OrganizationState(
        aws_external_id="1234567890",
        status=OrganizationStateStatus.ACTIVE,
        modified_by="testing@fluid.com",
        modified_date=datetime.now(),
    ),
)


def test_format_treli_tokens() -> None:
    tokens = TreliTokensApiResponse(
        email="test@fluidattacks.com",
        customerId="customer_id",
        cardTokenId="card_token_id",
        token_id=123,
        ctype="card",
        last="1234",
    )

    result = format_treli_tokens(tokens=[tokens])

    assert result == [CustomerTokens(card_token_id="card_token_id", token_id=123)]
