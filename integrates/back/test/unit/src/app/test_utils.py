from collections.abc import (
    Callable,
)
from httpx import (
    ConnectTimeout,
    ReadTimeout,
)
from integrates.app.utils import (
    get_jwt_userinfo,
)
from integrates.custom_utils.requests import (
    get_redirect_url,
)
import pytest
from test.unit.src.utils import (
    get_module_at_test,
)
from unittest.mock import (
    AsyncMock,
    MagicMock,
)

MODULE_AT_TEST = f"integrates.{get_module_at_test(file_path=__file__)}"

pytestmark = [
    pytest.mark.asyncio,
]


@pytest.mark.parametrize(["exception"], [[ConnectTimeout], [ReadTimeout]])
async def test_get_jwt_userinfo_retry_on_exception(
    request_fixture: Callable[[str], MagicMock],
    exception: type[Exception],
) -> None:
    client = AsyncMock()
    request = request_fixture("testing")
    client.authorize_access_token.side_effect = exception("testing")
    with pytest.raises(exception, match="testing"):
        await get_jwt_userinfo(client, request)


async def test_get_redirect_url_for_authz_bitbucket() -> None:
    client = MagicMock()
    client.url_for.return_value = "http://example.com/authz_bitbucket"
    result = get_redirect_url(client, "authz_bitbucket")
    assert result == "https://example.com/authz_bitbucket"
