import os
from collections.abc import (
    Callable,
)
from unittest.mock import (
    AsyncMock,
    MagicMock,
    patch,
)

import pytest
from starlette.responses import (
    Response,
)

from integrates.app.views.evidence import (
    enforce_group_level_role,
    get_evidence,
    retrieve_image,
)
from test.unit.src.utils import (
    get_module_at_test,
)

MODULE_AT_TEST = f"integrates.{get_module_at_test(file_path=__file__)}"

pytestmark = [
    pytest.mark.asyncio,
]


@patch(MODULE_AT_TEST + "sessions_domain.get_jwt_content", new_callable=AsyncMock)
async def test_enforce_group_level_role_catches_invalid_authorization(
    mock_get_jwt_content: AsyncMock,
    request_fixture: Callable[[str], MagicMock],
    mocked_data_for_module: dict,
    set_mock: Callable,
) -> None:
    set_mock(
        mock=mock_get_jwt_content,
        mocked_functionality_path="sessions_domain.get_jwt_content",
        mock_key="test_enforce_group_level_role_catches_invalid_authorization",
        module_at_test=MODULE_AT_TEST,
        mocked_data=mocked_data_for_module,
        side_effect=True,
    )
    response = await enforce_group_level_role(
        MagicMock(),
        request_fixture("testing"),
        "user",
        *["user"],
    )
    assert response is not None
    assert response.status_code == 403
    assert response.body == b"Access denied"
    mock_get_jwt_content.assert_awaited_once()


@patch(MODULE_AT_TEST + "sessions_domain.get_jwt_content", new_callable=AsyncMock)
async def test_get_evidence_invalid_authorization(
    mock_get_jwt_content: AsyncMock,
    request_fixture: Callable[[str], MagicMock],
    set_mock: Callable,
    mocked_data_for_module: dict,
) -> None:
    set_mock(
        mock=mock_get_jwt_content,
        mocked_functionality_path="sessions_domain.get_jwt_content",
        mock_key="Invalid Authorization",
        module_at_test=MODULE_AT_TEST,
        mocked_data=mocked_data_for_module,
        side_effect=True,
    )
    response: Response = await get_evidence(request_fixture("testing"), "findings")
    assert response.template.name == "login.html"  # type: ignore


async def test_retrieve_image_not_allowed_mime() -> None:
    current_path = os.path.join(os.path.dirname(os.path.abspath(__file__)))
    file_path = os.path.join(current_path, "mock/resources/test_file.txt")
    response: Response = await retrieve_image(file_path)
    assert response.body == b"Error: Invalid evidence image format"
