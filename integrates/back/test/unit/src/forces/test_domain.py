from datetime import (
    datetime,
    timedelta,
)
from integrates.custom_utils import (
    datetime as datetime_utils,
)
from integrates.forces.domain import (
    get_expiration_date,
    get_json_log_execution,
    update_token,
)
from integrates.sessions.domain import (
    encode_token,
)
from integrates.settings.session import (
    SESSION_COOKIE_AGE,
)
import pytest
from test.unit.src.utils import (
    get_mocked_path,
    set_mocks_return_values,
)
from unittest.mock import (
    AsyncMock,
    patch,
)

pytestmark = pytest.mark.asyncio


@pytest.mark.parametrize(
    [
        "group_name",
        "organization_id",
        "token",
    ],
    [
        [
            "unittesting",
            "ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3",
            "mock_token",
        ],
    ],
)
@patch(
    get_mocked_path("groups_domain.update_metadata"), new_callable=AsyncMock
)
async def test_update_secret_token(
    mock_groups_domain_update_metadata: AsyncMock,
    group_name: str,
    organization_id: str,
    token: str,
) -> None:
    mocked_objects, mocked_paths, mocks_args = [
        [mock_groups_domain_update_metadata],
        ["groups_domain.update_metadata"],
        [[group_name, token, organization_id]],
    ]
    assert set_mocks_return_values(
        mocked_objects=mocked_objects,
        paths_list=mocked_paths,
        mocks_args=mocks_args,
    )

    await update_token(group_name, organization_id, token)
    assert all(mock_object.called is True for mock_object in mocked_objects)


@pytest.mark.parametrize(
    [
        "group_name",
        "execution_id",
    ],
    [
        [
            "unittesting",
            "92968b3ba84645bf976c12b15f9464bb",
        ],
    ],
)
async def test_empty_json_log(group_name: str, execution_id: str) -> None:
    assert await get_json_log_execution(group_name, execution_id) is None


@pytest.mark.parametrize(
    ["payload", "subject"],
    [[{"user_email": "unittest@fluidattacks.com"}, "api_token"]],
)
async def test_get_expiration_date(
    payload: dict[str, str], subject: str
) -> None:
    expiration_time = datetime_utils.get_as_epoch(
        datetime.utcnow() + timedelta(seconds=SESSION_COOKIE_AGE)
    )
    token = encode_token(
        expiration_time, payload, subject, subject == "api_token"
    )
    assert get_expiration_date(token=token)
