from collections.abc import (
    Callable,
)
from datetime import (
    datetime,
)
from decimal import (
    Decimal,
)
from integrates.db_model.enums import (
    Source,
    StateRemovalJustification,
    TreatmentStatus,
)
from integrates.db_model.findings.enums import (
    FindingSorts,
    FindingStateStatus,
    FindingStatus,
)
from integrates.db_model.findings.types import (
    Finding,
    FindingEvidence,
    FindingEvidences,
    FindingState,
    FindingUnreliableIndicators,
    FindingVerificationSummary,
)
from integrates.db_model.types import (
    SeverityScore,
    Treatment,
)
from integrates.db_model.vulnerabilities.enums import (
    VulnerabilityStateStatus,
    VulnerabilityTechnique,
    VulnerabilityToolImpact,
    VulnerabilityType,
)
from integrates.db_model.vulnerabilities.types import (
    Vulnerability,
    VulnerabilityState,
    VulnerabilityTool,
    VulnerabilityUnreliableIndicators,
)
import pytest
from typing import (
    Any,
)

MOCKED_DATA: dict[str, Any] = {
    "integrates.unreliable_indicators.utils"
    ".Dataloaders.finding_vulnerabilities_released_nzr": {
        '["unittesting"]': [
            Vulnerability(
                created_by="unittest@fluidattacks.com",
                created_date=datetime.fromisoformat(
                    "2020-01-03T17:46:10+00:00"
                ),
                finding_id="422286126",
                group_name="unittesting",
                organization_name="okada",
                hacker_email="unittest@fluidattacks.com",
                id="0a848781-b6a4-422e-95fa-692151e6a98f",
                state=VulnerabilityState(
                    modified_by="unittest@fluidattacks.com",
                    modified_date=datetime.fromisoformat(
                        "2020-01-03T17:46:10+00:00"
                    ),
                    source=Source.ASM,
                    specific="12",
                    status=VulnerabilityStateStatus.VULNERABLE,
                    where="test/data/lib_path/f060/csharp.cs",
                    commit="ea871eee64cfd5ce293411efaf4d3b446d04eb4a",
                    reasons=None,
                    other_reason=None,
                    tool=VulnerabilityTool(
                        name="tool-2", impact=VulnerabilityToolImpact.INDIRECT
                    ),
                ),
                technique=VulnerabilityTechnique.SCR,
                type=VulnerabilityType.LINES,
                root_id="4039d098-ffc5-4984-8ed3-eb17bca98e19",
                treatment=Treatment(
                    modified_date=datetime.fromisoformat(
                        "2020-01-03T17:46:10+00:00"
                    ),
                    status=TreatmentStatus.IN_PROGRESS,
                    acceptance_status=None,
                    accepted_until=None,
                    justification="test justification",
                    assigned="integratesuser2@gmail.com",
                    modified_by="integratesuser@gmail.com",
                ),
                unreliable_indicators=VulnerabilityUnreliableIndicators(
                    unreliable_efficacy=Decimal("0"),
                    unreliable_reattack_cycles=0,
                    unreliable_report_date=datetime.fromisoformat(
                        "2020-01-03T17:46:10+00:00"
                    ),
                    unreliable_treatment_changes=1,
                ),
            ),
            Vulnerability(
                created_by="unittest@fluidattacks.com",
                created_date=datetime.fromisoformat(
                    "2019-04-08T00:45:15+00:00"
                ),
                finding_id="422286126",
                group_name="unittesting",
                organization_name="okada",
                hacker_email="unittest@fluidattacks.com",
                id="69b84d52-1b18-41fa-84b5-bcb8134cb1ec",
                state=VulnerabilityState(
                    modified_by="unittest@fluidattacks.com",
                    modified_date=datetime.fromisoformat(
                        "2019-08-07T13:45:48+00:00"
                    ),
                    source=Source.ASM,
                    specific="9999",
                    status=VulnerabilityStateStatus.SAFE,
                    where="192.168.1.20",
                    commit=None,
                    reasons=None,
                    other_reason=None,
                    tool=VulnerabilityTool(
                        name="tool-1", impact=VulnerabilityToolImpact.INDIRECT
                    ),
                ),
                technique=VulnerabilityTechnique.PTAAS,
                type=VulnerabilityType.PORTS,
                root_id="4039d098-ffc5-4984-8ed3-eb17bca98e19",
                treatment=Treatment(
                    modified_date=datetime.fromisoformat(
                        "2020-11-08T00:59:06+00:00"
                    ),
                    status=TreatmentStatus.ACCEPTED,
                    acceptance_status=None,
                    accepted_until=datetime.fromisoformat(
                        "2021-04-08T00:59:06+00:00"
                    ),
                    justification="test justification temporarily accepted",
                    assigned="integratesuser2@gmail.com",
                    modified_by="integratesuser@gmail.com",
                ),
                unreliable_indicators=VulnerabilityUnreliableIndicators(
                    unreliable_closing_date=datetime.fromisoformat(
                        "2019-08-07T13:45:48+00:00"
                    ),
                    unreliable_efficacy=Decimal("0"),
                    unreliable_reattack_cycles=0,
                    unreliable_report_date=datetime.fromisoformat(
                        "2019-04-08T00:45:15+00:00"
                    ),
                    unreliable_treatment_changes=1,
                ),
            ),
        ]
    },
    "finding": Finding(
        group_name="unittesting",
        id="422286126",
        state=FindingState(
            modified_by="integratesmanager@gmail.com",
            modified_date=datetime.fromisoformat("2018-07-09T05:00:00+00:00"),
            source=Source.ASM,
            status=FindingStateStatus.CREATED,
            rejection=None,
            justification=StateRemovalJustification.NO_JUSTIFICATION,
        ),
        title="060. Insecure service configuration - Host verification",
        attack_vector_description="This is an attack vector",
        creation=FindingState(
            modified_by="integratesmanager@gmail.com",
            modified_date=datetime.fromisoformat("2018-04-08T00:43:18+00:00"),
            source=Source.ASM,
            status=FindingStateStatus.CREATED,
            rejection=None,
            justification=StateRemovalJustification.NO_JUSTIFICATION,
        ),
        description="The source code uses generic exceptions to "
        "handle unexpected errors. Catching generic exceptions "
        "obscures the problem that caused the error and promotes "
        "a generic way to handle different categories or sources "
        "of error. This may cause security vulnerabilities to "
        "materialize, as some special flows go unnoticed.",
        evidences=FindingEvidences(
            animation=FindingEvidence(
                modified_date=datetime.fromisoformat(
                    "2018-07-09T05:00:00+00:00"
                ),
                description="Test description",
                url="unittesting-422286126-animation.gif",
            ),
            evidence1=FindingEvidence(
                modified_date=datetime.fromisoformat(
                    "2018-07-09T05:00:00+00:00"
                ),
                description="this is a test description",
                url="unittesting-422286126-evidence_route_1.png",
            ),
            evidence2=FindingEvidence(
                modified_date=datetime.fromisoformat(
                    "2018-07-09T05:00:00+00:00"
                ),
                description="exception",
                url="unittesting-422286126-evidence_route_2.jpg",
            ),
            evidence3=FindingEvidence(
                modified_date=datetime.fromisoformat(
                    "2018-07-09T05:00:00+00:00"
                ),
                description="Description",
                url="unittesting-422286126-evidence_route_3.png",
            ),
            evidence4=FindingEvidence(
                modified_date=datetime.fromisoformat(
                    "2018-07-09T05:00:00+00:00"
                ),
                description="changed for testing purposes",
                url="unittesting-422286126-evidence_route_4.png",
            ),
            evidence5=FindingEvidence(
                modified_date=datetime.fromisoformat(
                    "2018-07-09T05:00:00+00:00"
                ),
                description="Test description",
                url="unittesting-422286126-evidence_route_5.png",
            ),
            exploitation=FindingEvidence(
                modified_date=datetime.fromisoformat(
                    "2018-07-09T05:00:00+00:00"
                ),
                description="test",
                url="unittesting-422286126-exploitation.png",
            ),
            records=FindingEvidence(
                modified_date=datetime.fromisoformat(
                    "2018-07-09T05:00:00+00:00"
                ),
                description="test",
                url="unittesting-422286126-evidence_file.csv",
            ),
        ),
        min_time_to_remediate=18,
        recommendation="Implement password policies with the best "
        "practices for strong passwords.",
        requirements="R359. Avoid using generic exceptions.",
        severity_score=SeverityScore(
            base_score=Decimal("3.5"),
            temporal_score=Decimal("2.9"),
            cvss_v3="CVSS:3.1/AV:A/AC:L/PR:L/UI:N/S:U/C:N/I:L/A:N/E:U/"
            "RL:W/RC:U/MAV:A/MAC:L/MPR:L/MUI:N/MS:U/MI:L",
            cvssf=Decimal("0.218"),
            cvss_v4="CVSS:4.0/AV:A/AC:H/AT:N/PR:L/UI:N/VC:L/VI:L/VA:L/"
            "SC:N/SI:N/SA:N/MAV:A/MAC:H/MPR:L/MUI:N/MVC:L/MVI:L/MVA:L/"
            "MSC:N/MSI:N/MSA:N/E:P",
            threat_score=Decimal("1.2"),
            cvssf_v4=Decimal("0.021"),
        ),
        sorts=FindingSorts.NO,
        threat="An attacker can get passwords of users and "
        "impersonate them or used the credentials for practices "
        "malicious.",
        unreliable_indicators=FindingUnreliableIndicators(
            unreliable_newest_vulnerability_report_date=(
                datetime.fromisoformat("2020-01-03T17:46:10+00:00")
            ),
            unreliable_oldest_open_vulnerability_report_date=(
                datetime.fromisoformat("2020-01-03T17:46:10+00:00")
            ),
            verification_summary=FindingVerificationSummary(
                requested=0, on_hold=0, verified=0
            ),
            unreliable_status=FindingStatus.VULNERABLE,
            unreliable_where="test/data/lib_path/f060/csharp.cs",
        ),
    ),
}


@pytest.fixture
def mocked_data_for_module(
    *,
    resolve_mock_data: Callable,
) -> Any:
    def _mocked_data_for_module(
        mock_path: str, mock_args: list[Any], module_at_test: str
    ) -> Callable[[str, list[Any], str], Any]:
        return resolve_mock_data(
            mock_data=MOCKED_DATA,
            mock_path=mock_path,
            mock_args=mock_args,
            module_at_test=module_at_test,
        )

    return _mocked_data_for_module
