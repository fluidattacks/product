from integrates.custom_exceptions import (
    JiraInstallAlreadyCreated,
)
from integrates.custom_utils.datetime import get_utc_now
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model import (
    jira_installations,
)
from integrates.db_model.jira_installations.types import (
    JiraInstallState,
    JiraSecurityInstall,
    JiraSecurityInstallRequest,
)
from integrates.jira_security_integration.utils import (
    add_install,
)
import pytest


@pytest.mark.changes_db
@pytest.mark.asyncio
@pytest.mark.parametrize(
    [
        "body",
    ],
    [
        [
            {
                "baseUrl": "https://test_url.com",
                "clientKey": "00000000-0000-0000-0000-000000000000",
                "cloudId": "00000000-0000-0000-0000-000000000000",
                "description": "test",
                "displayUrl": "https://testing-testing.com",
                "eventType": "installed",
                "installationId": (
                    "ari:cloud:ecosystem::installation/"
                    "00000000-0000-0000-0000-000000000000"
                ),
                "key": "testing",
                "pluginsVersion": "1000",
                "productType": "jira",
                "publicKey": "test-plublic-key",
                "serverVersion": "100000",
                "sharedSecret": "test-shared-secret",
                "associated_email": "",
            },
        ],
    ],
)
async def test_install_add(body: dict[str, str]) -> None:
    await add_install(body)
    loaders: Dataloaders = get_new_context()
    jira_install_exists: (
        JiraSecurityInstall | None
    ) = await loaders.jira_install.load(
        JiraSecurityInstallRequest(
            base_url="https://test_url.com",
            client_key="00000000-0000-0000-0000-000000000000",
        )
    )
    assert jira_install_exists is not None


@pytest.mark.asyncio
@pytest.mark.parametrize(
    [
        "body",
    ],
    [
        [
            {
                "baseUrl": "https://fluidattacks-dev.atlassian.net",
                "clientKey": "42xjhmdl-2zga-e6e8-ox5m-5v7exn16un1k",
                "cloudId": "00000000-0000-0000-0000-000000000000",
                "description": "test",
                "displayUrl": "https://testing-testing.com",
                "eventType": "installed",
                "installationId": (
                    "ari:cloud:ecosystem::installation/"
                    "00000000-0000-0000-0000-000000000000"
                ),
                "key": "testing",
                "pluginsVersion": "1000",
                "productType": "jira",
                "publicKey": "test-plublic-key",
                "serverVersion": "100000",
                "sharedSecret": "test-shared-secret",
            },
        ],
    ],
)
async def test_install_add_fail(body: dict[str, str]) -> None:
    error = ""
    data = JiraSecurityInstall(
        associated_email="-",
        base_url=body["baseUrl"],
        client_key=body["clientKey"],
        cloud_id=body["cloudId"],
        description=body["description"],
        display_url=body["displayUrl"],
        event_type=body["eventType"],
        installation_id=body["installationId"],
        key=body["key"],
        plugins_version=body["pluginsVersion"],
        product_type=body["productType"],
        public_key=body["publicKey"],
        server_version=body["serverVersion"],
        shared_secret=body["sharedSecret"],
        state=JiraInstallState(
            modified_by="",
            modified_date=get_utc_now().isoformat(),
            used_api_token="",
            used_jira_jwt="",
        ),
    )
    try:
        await jira_installations.add(jira_install=data)
    except JiraInstallAlreadyCreated as ex:
        error = ex.msg
    assert error == "This install already exists"
