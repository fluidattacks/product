import pytest

from integrates.custom_utils.datetime import get_utc_now
from integrates.db_model.jira_installations.types import (
    JiraInstallState,
    JiraSecurityInstall,
)

pytestmark = [
    pytest.mark.asyncio,
]


@pytest.mark.asyncio
async def test_create_jira_security_install() -> None:
    data = JiraSecurityInstall(
        base_url="https://test.atlassian.net",
        client_key="test_client_key",
        cloud_id="test_cloud_id",
        description="test installation",
        display_url="https://test.atlassian.net/display",
        event_type="jira:installed",
        installation_id="12345",
        key="test_key",
        plugins_version="1.0.0",
        product_type="jira",
        public_key="test_public_key",
        server_version="v1.0",
        shared_secret="test_shared_secret",
        associated_email="test_associated_email",
        state=JiraInstallState(
            modified_by="",
            modified_date=get_utc_now().isoformat(),
            used_api_token="",
            used_jira_jwt="",
        ),
    )

    assert data.base_url == "https://test.atlassian.net"
    assert data.client_key == "test_client_key"
    assert data.cloud_id == "test_cloud_id"
    assert data.description == "test installation"
    assert data.display_url == "https://test.atlassian.net/display"
    assert data.event_type == "jira:installed"
    assert data.installation_id == "12345"
    assert data.key == "test_key"
    assert data.plugins_version == "1.0.0"
    assert data.product_type == "jira"
    assert data.public_key == "test_public_key"
    assert data.server_version == "v1.0"
    assert data.shared_secret == "test_shared_secret"
    assert data.associated_email == "test_associated_email"
