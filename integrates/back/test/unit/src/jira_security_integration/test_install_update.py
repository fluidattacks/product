from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model import (
    jira_installations,
)
from integrates.db_model.jira_installations.types import (
    JiraSecurityInstall,
    JiraSecurityInstallRequest,
)
import pytest


@pytest.mark.changes_db
@pytest.mark.asyncio
async def test_install_update() -> None:
    loaders: Dataloaders = get_new_context()

    await jira_installations.update_associate_email(
        base_url="https://fluidattacks-dev.atlassian.net",
        client_key="42xjhmdl-2zga-e6e8-ox5m-5v7exn16un1k",
        associated_email="test_email@fluidattacks.com",
    )

    jira_install: (
        JiraSecurityInstall | None
    ) = await loaders.jira_install.load(
        JiraSecurityInstallRequest(
            base_url="https://fluidattacks-dev.atlassian.net",
            client_key="42xjhmdl-2zga-e6e8-ox5m-5v7exn16un1k",
        )
    )

    assert jira_install is not None
    assert jira_install.associated_email == "test_email@fluidattacks.com"
