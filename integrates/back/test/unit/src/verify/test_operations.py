from integrates.custom_exceptions import (
    CouldNotStartStakeholderVerification,
    CouldNotVerifyStakeholder,
    InvalidMobileNumber,
    InvalidVerificationCode,
    TooManyRequests,
)
from integrates.verify.enums import (
    Channel,
)
from integrates.verify.operations import (
    check_verification,
    get_country_code,
    start_verification,
    translate_channel,
    validate_mobile,
)
import pytest
from test.unit.src.utils import (
    dict2obj,
    get_module_at_test,
)
from twilio.base.exceptions import (
    TwilioRestException,
)
from typing import (
    NamedTuple,
)
from unittest import (
    mock,
)

# Constants
pytestmark = [
    pytest.mark.asyncio,
]

MODULE_AT_TEST = f"integrates.{get_module_at_test(file_path=__file__)}"


async def test_get_country_code() -> None:
    class MockedTwilioObject(NamedTuple):
        caller_name: str
        carrier: dict
        country_code: str
        national_format: str
        phone_number: str
        add_ons: str
        url: str

    test_phone_number = "+15108675310"
    test_result = await get_country_code(test_phone_number)
    assert test_result == ""
    mocked_response = MockedTwilioObject(
        caller_name="null",
        carrier={
            "error_code": "null",
            "mobile_country_code": "310",
            "mobile_network_code": "456",
            "name": "verizon",
            "type": "mobile",
        },
        country_code="US",
        national_format="(510) 867-5310",
        phone_number="+15108675310",
        add_ons="null",
        url="https://lookups.twilio.com/v1/PhoneNumbers/+15108675310",
    )
    with mock.patch(
        "integrates.verify.operations.FI_ENVIRONMENT", "production"
    ):
        with mock.patch("integrates.verify.operations.client"):
            with mock.patch(
                "integrates.verify.operations.client.lookups.v1.phone_numbers"
            ) as mock_twilio:
                mock_twilio.return_value.fetch.return_value = mocked_response
                test_result = await get_country_code("+15108675310")
        with pytest.raises(InvalidMobileNumber):
            await get_country_code("0000")
    assert mock_twilio.called is True
    assert test_result == "US"


async def test_check_verification() -> None:
    class MockedTwilioObject(NamedTuple):
        sid: str
        service_sid: str
        account_sid: str
        to: str
        channel: str
        status: str
        valid: bool
        amount: str
        payee: str
        sna_attempts_error_codes: list
        date_created: str
        date_updated: str

    test_phone_number = "12345678"
    test_code = "US"
    await check_verification(recipient=test_phone_number, code=test_code)
    mocked_response = MockedTwilioObject(
        sid="VEXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
        service_sid="VAXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
        account_sid="ACXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
        to="+15017122661",
        channel="sms",
        status="approved",
        valid=True,
        amount="test",
        payee="test",
        sna_attempts_error_codes=[],
        date_created="2015-07-30T20:00:00Z",
        date_updated="2015-07-30T20:00:00Z",
    )
    with mock.patch(
        "integrates.verify.operations.FI_ENVIRONMENT", "production"
    ):
        with mock.patch("integrates.verify.operations.client"):
            with mock.patch(
                "integrates.verify.operations.client.verify.services"
            ) as mocked:
                mocked.return_value.verification_checks.create.return_value = (
                    mocked_response
                )
                await check_verification(recipient="+15017122661", code="US")

        with pytest.raises(CouldNotVerifyStakeholder):
            await check_verification(recipient="", code=test_code)
    assert mocked.called is True


@pytest.mark.parametrize(
    ["phone_number", "code"],
    [
        [
            "19287382",
            "1234",
        ]
    ],
)
@mock.patch(
    MODULE_AT_TEST + "in_thread",
    side_effect=TwilioRestException(
        status=404,
        uri="https://api.twilio.com/v1/some_endpoint",
        msg="Resource not found",
        code=12345,
        method="POST",
        details={"error_message": "The requested resource does not exist"},
    ),
)
async def test_check_verification_raises_could_not_verify(
    mock_in_thread: mock.AsyncMock, phone_number: str, code: str
) -> None:
    with mock.patch(
        "integrates.verify.operations.FI_ENVIRONMENT", "production"
    ):
        with pytest.raises(CouldNotVerifyStakeholder):
            await check_verification(recipient=phone_number, code=code)
            mock_in_thread.assert_awaited_once()


@pytest.mark.parametrize(
    ["phone_number", "code", "return_value"],
    [["19287382", "1234", {"status": "rejected"}]],
)
@mock.patch(MODULE_AT_TEST + "in_thread", new_callable=mock.AsyncMock)
async def test_check_verification_raises_invalid(
    mock_in_thread: mock.AsyncMock,
    phone_number: str,
    code: str,
    return_value: dict,
) -> None:
    mock_in_thread.return_value = dict2obj(return_value)
    with mock.patch(
        "integrates.verify.operations.FI_ENVIRONMENT", "production"
    ):
        with pytest.raises(InvalidVerificationCode):
            await check_verification(recipient=phone_number, code=code)
            mock_in_thread.assert_awaited_once()


async def test_start_verification() -> None:
    class MockedTwilioObject(NamedTuple):
        sid: str
        service_sid: str
        account_sid: str
        to: str
        channel: str
        status: str
        valid: bool
        date_created: str
        date_updated: str
        lookup: dict
        amount: str
        payee: str
        send_code_attempts: list
        sna: str
        url: str

    test_phone_number = "12345678"
    await start_verification(recipient=test_phone_number)
    mocked_response = MockedTwilioObject(
        sid="VEXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
        service_sid="VAXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
        account_sid="ACXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
        to="+15017122661",
        channel="sms",
        status="pending",
        valid=False,
        date_created="2015-07-30T20:00:00Z",
        date_updated="2015-07-30T20:00:00Z",
        lookup={"test_key": "test_value"},
        amount="0",
        payee="test",
        send_code_attempts=[
            {
                "time": "2015-07-30T20:00:00Z",
                "channel": "SMS",
                "attempt_sid": "VLXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
            },
        ],
        sna="test",
        url="""https://verify.twilio.com/v2/Services/VAXXXXXXXXXXXXXXXXXXXXXX
            XXXXXXXXXX/Verifications/VEXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX""",
    )
    with mock.patch(
        "integrates.verify.operations.FI_ENVIRONMENT", "production"
    ):
        with mock.patch("integrates.verify.operations.client"):
            with mock.patch(
                "integrates.verify.operations.client.verify.services"
            ) as mock_twilio:
                mock_twilio.return_value.verifications.create.return_value = (
                    mocked_response
                )
                await start_verification(recipient="+15017122661")
    assert mock_twilio.called is True


@pytest.mark.parametrize(
    ["phone_number", "params"],
    [
        [
            "12345678",
            {
                "status": 404,
                "uri": "https://api.twilio.com/v1/some_endpoint",
                "msg": "Resource not found",
                "code1": 12345,
                "code2": 20429,
                "method": "POST",
                "details": {
                    "error_message": "The requested resource does not exist"
                },
            },
        ]
    ],
)
@mock.patch(MODULE_AT_TEST + "in_thread")
async def test_start_verification_raises_ex(
    mock_in_thread: mock.AsyncMock, phone_number: str, params: dict
) -> None:
    mock_in_thread.side_effect = TwilioRestException(
        status=params["status"],
        uri=params["uri"],
        msg=params["msg"],
        code=params["code1"],
        method=params["method"],
        details=params["details"],
    )
    with mock.patch(
        "integrates.verify.operations.FI_ENVIRONMENT", "production"
    ):
        with pytest.raises(CouldNotStartStakeholderVerification):
            await start_verification(recipient=phone_number)

    mock_in_thread.side_effect = TwilioRestException(
        status=params["status"],
        uri=params["uri"],
        msg=params["msg"],
        code=params["code2"],
        method=params["method"],
        details=params["details"],
    )
    with mock.patch(
        "integrates.verify.operations.FI_ENVIRONMENT", "production"
    ):
        with pytest.raises(TooManyRequests):
            await start_verification(recipient=phone_number)


async def test_validate_mobile() -> None:
    class MockedTwilioObject(NamedTuple):
        caller_name: str
        carrier: dict
        country_code: str
        national_format: str
        phone_number: str
        add_ons: str
        url: str

    test_phone_number = "12345678"
    await validate_mobile(test_phone_number)
    mocked_response = MockedTwilioObject(
        caller_name="null",
        carrier={
            "error_code": "null",
            "mobile_country_code": "310",
            "mobile_network_code": "456",
            "name": "verizon",
            "type": "mobile",
        },
        country_code="US",
        national_format="(510) 867-5310",
        phone_number="+15108675310",
        add_ons="null",
        url="https://lookups.twilio.com/v1/PhoneNumbers/+15108675310",
    )
    with mock.patch(
        "integrates.verify.operations.FI_ENVIRONMENT", "production"
    ):
        with mock.patch("integrates.verify.operations.client"):
            with mock.patch(
                "integrates.verify.operations.client.lookups.v1.phone_numbers"
            ) as mock_twilio:
                mock_twilio.return_value.fetch.return_value = mocked_response
                await validate_mobile("+15108675310")
    assert mock_twilio.called is True


@pytest.mark.parametrize(
    ["phone_number", "return_value"],
    [["129387", {"country_code": "CO", "carrier": {"type": "fax"}}]],
)
@mock.patch(MODULE_AT_TEST + "in_thread", new_callable=mock.AsyncMock)
async def test_validate_mobile_raises_invalid_phone(
    mock_in_thread: mock.AsyncMock, phone_number: str, return_value: dict
) -> None:
    with mock.patch(
        "integrates.verify.operations.FI_ENVIRONMENT", "production"
    ):
        mock_in_thread.return_value = dict2obj(return_value)
        with pytest.raises(InvalidMobileNumber):
            await validate_mobile(phone_number=phone_number)

        mock_in_thread.side_effect = TwilioRestException(
            status=404,
            uri="https://api.twilio.com/v1/some_endpoint",
            msg="Resource not found",
            code=12345,
            method="POST",
            details={"error_message": "The requested resource does not exist"},
        )
        with pytest.raises(InvalidMobileNumber):
            await validate_mobile(phone_number=phone_number)


@pytest.mark.asyncio
async def test_valid_input_channel() -> None:
    input_channel = Channel.CALL
    translated_channel = translate_channel(input_channel)

    assert translated_channel == "Call"

    input_channel = Channel.EMAIL
    translated_channel = translate_channel(input_channel)

    assert translated_channel == "Email"

    input_channel = Channel.SMS
    translated_channel = translate_channel(input_channel)

    assert translated_channel == "SMS"

    input_channel = Channel.WHATSAPP
    translated_channel = translate_channel(input_channel)

    assert translated_channel == "WhatsApp"
