from collections.abc import (
    Callable,
)
from datetime import (
    datetime,
)
from integrates.db_model.roots.enums import (
    RootStatus,
)
from integrates.db_model.roots.get import (
    RootLoader,
)
from integrates.db_model.roots.types import (
    RootRequest,
    RootUnreliableIndicators,
)
import pytest
from test.unit.src.utils import (
    get_module_at_test,
)
from unittest.mock import (
    AsyncMock,
    patch,
)

MODULE_AT_TEST = f"integrates.{get_module_at_test(file_path=__file__)}"

pytestmark = [
    pytest.mark.asyncio,
]


@pytest.mark.parametrize(
    ["group_name", "root_id"],
    [
        [
            "test_group_1",
            "2851c4f5-fa0a-4a41-991e-8ad2c36aead9",
        ]
    ],
)
@patch(MODULE_AT_TEST + "_get_roots", new_callable=AsyncMock)
async def test_root_loader(
    mock__get_roots: AsyncMock,
    mocked_data_for_module: Callable,
    group_name: str,
    root_id: str,
) -> None:
    mock__get_roots.side_effect = mocked_data_for_module(
        mock_path="_get_roots",
        mock_args=[root_id],
        module_at_test=MODULE_AT_TEST,
    )
    root = RootLoader()
    assert isinstance(root, RootLoader)
    test_root = await root.load(
        RootRequest(group_name=group_name, root_id=root_id)
    )
    assert test_root
    assert test_root.state.nickname == "nickname1"
    assert test_root.state.status == RootStatus.ACTIVE
    assert test_root.unreliable_indicators == RootUnreliableIndicators(
        unreliable_last_status_update=datetime.fromisoformat(
            "2022-09-13T01:00:00+00:00"
        ),
        nofluid_quantity=1,
    )
