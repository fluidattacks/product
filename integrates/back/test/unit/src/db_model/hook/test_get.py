from integrates.db_model.hook.get import (
    GroupHookLoader,
    HookLoader,
)
from integrates.db_model.hook.types import (
    GroupHook,
    GroupHookRequest,
)
import pytest
from test.unit.src.utils import (
    get_module_at_test,
    set_mocks_return_values,
)
from unittest.mock import (
    AsyncMock,
    patch,
)

MODULE_AT_TEST = f"integrates.{get_module_at_test(file_path=__file__)}"

pytestmark = [
    pytest.mark.asyncio,
]


@pytest.mark.parametrize(
    ["group_name", "group_name_not_found"],
    [["unittesting", "unittesting2"]],
)
@patch(MODULE_AT_TEST + "_get_group_hook", new_callable=AsyncMock)
async def test_grouphookloader(
    mock__get_hook_no_fallback: AsyncMock,
    group_name: str,
    group_name_not_found: str,
) -> None:
    assert set_mocks_return_values(
        mocks_args=[[group_name]],
        mocked_objects=[mock__get_hook_no_fallback],
        module_at_test=MODULE_AT_TEST,
        paths_list=["_get_group_hook"],
    )

    loaders = GroupHookLoader()
    hook = await loaders.load(group_name)
    assert isinstance(hook, GroupHook)
    assert mock__get_hook_no_fallback.call_count == 1

    mock__get_hook_no_fallback.return_value = [None]

    non_existent_hook = await loaders.load(group_name_not_found)
    assert not isinstance(non_existent_hook, GroupHook)
    assert not non_existent_hook[0]
    assert mock__get_hook_no_fallback.call_count == 2


@pytest.mark.parametrize(
    ["hook_id", "hook_id_not_found"],
    [["unittesting", "unittesting2"]],
)
@patch(MODULE_AT_TEST + "_get_hook", new_callable=AsyncMock)
async def test_hookloader(
    mock__get_hook_no_fallback: AsyncMock,
    hook_id: str,
    hook_id_not_found: str,
) -> None:
    group_name = "unittesting"
    assert set_mocks_return_values(
        mocks_args=[[hook_id]],
        mocked_objects=[mock__get_hook_no_fallback],
        module_at_test=MODULE_AT_TEST,
        paths_list=["_get_group_hook"],
    )

    loaders = HookLoader()
    hook = await loaders.load(
        GroupHookRequest(id=hook_id, group_name=group_name)
    )
    assert isinstance(hook, GroupHook)
    assert mock__get_hook_no_fallback.call_count == 1

    mock__get_hook_no_fallback.return_value = [None]

    non_existent_hook = await loaders.load(
        GroupHookRequest(id=hook_id_not_found, group_name=group_name)
    )
    assert not isinstance(non_existent_hook, GroupHook)
    assert not non_existent_hook[0]
    assert mock__get_hook_no_fallback.call_count == 2
