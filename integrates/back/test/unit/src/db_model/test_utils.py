from datetime import (
    datetime,
    timezone,
)
from decimal import (
    Decimal,
)
from freezegun import (
    freeze_time,
)
from integrates.db_model.enums import (
    Source,
    TreatmentStatus,
)
from integrates.db_model.findings.enums import (
    FindingStateStatus,
)
from integrates.db_model.findings.types import (
    FindingState,
)
from integrates.db_model.types import (
    PoliciesToUpdate,
    Treatment,
)
from integrates.db_model.utils import (
    adjust_historic_dates,
    format_key_from_pk_search,
    format_policies_to_update,
    get_as_utc_iso_format,
    get_datetime_with_offset,
    get_first_day_iso_date,
    get_historic_gsi_2_key,
    get_historic_gsi_3_key,
    get_historic_gsi_sk,
    get_min_iso_date,
    serialize,
)
from integrates.dynamodb.types import (
    Facet,
    PrimaryKey,
)
import pytest

pytestmark = [
    pytest.mark.asyncio,
]


async def test_adjust_historic_dates() -> None:
    historic = (
        FindingState(
            modified_by="",
            modified_date=datetime.fromisoformat("2021-12-30T14:35:01+00:00"),
            source=Source.ASM,
            status=FindingStateStatus.CREATED,
        ),
    )
    assert adjust_historic_dates(historic) == (
        FindingState(
            modified_by="",
            modified_date=datetime.fromisoformat("2021-12-30T14:35:01+00:00"),
            source=Source.ASM,
            status=FindingStateStatus.CREATED,
        ),
    )

    historic_2 = (
        Treatment(
            modified_date=datetime.fromisoformat("2021-12-12T00:00:01+00:00"),
            status=TreatmentStatus.UNTREATED,
        ),
        Treatment(
            modified_date=datetime.fromisoformat("2021-12-12T00:00:01+00:00"),
            status=TreatmentStatus.IN_PROGRESS,
        ),
        Treatment(
            modified_date=datetime.fromisoformat("2021-01-01T00:00:00+00:00"),
            status=TreatmentStatus.ACCEPTED,
        ),
        Treatment(
            modified_date=datetime.fromisoformat("2021-12-30T14:35:01+00:00"),
            status=TreatmentStatus.ACCEPTED_UNDEFINED,
        ),
    )
    assert adjust_historic_dates(historic_2) == (
        Treatment(
            modified_date=datetime.fromisoformat("2021-12-12T00:00:01+00:00"),
            status=TreatmentStatus.UNTREATED,
        ),
        Treatment(
            modified_date=datetime.fromisoformat("2021-12-12T00:00:02+00:00"),
            status=TreatmentStatus.IN_PROGRESS,
        ),
        Treatment(
            modified_date=datetime.fromisoformat("2021-12-12T00:00:03+00:00"),
            status=TreatmentStatus.ACCEPTED,
        ),
        Treatment(
            modified_date=datetime.fromisoformat("2021-12-30T14:35:01+00:00"),
            status=TreatmentStatus.ACCEPTED_UNDEFINED,
        ),
    )


def test_get_as_utc_iso_format() -> None:
    with freeze_time("2022-01-01 12:34:56"):
        test_date = datetime.now(tz=timezone.utc)
        assert get_as_utc_iso_format(test_date) == "2022-01-01T12:34:56+00:00"


def test_get_min_iso_date() -> None:
    with freeze_time("2022-01-01 12:34:56"):
        test_date = datetime.now(tz=timezone.utc)
        min_date = get_min_iso_date(test_date)
        assert min_date.isoformat() == "2022-01-01T00:00:00"
        assert min_date == datetime(2022, 1, 1)


def test_get_first_day_iso_date() -> None:
    with freeze_time("2022-01-05"):
        first_day = get_first_day_iso_date()
        assert first_day == datetime(2022, 1, 3)


def test_get_historic_gsi_sk() -> None:
    state = "ACTIVE"
    iso8601utc = "2022-01-01T00:00:00Z"
    expected_sk = "STATE#ACTIVE#DATE#2022-01-01T00:00:00Z"
    assert get_historic_gsi_sk(state, iso8601utc) == expected_sk


def test_get_historic_gsi_2_key() -> None:
    facet = Facet(
        pk_alias="FACET#ALIAS#GROUP",
        attrs=("test", "data"),
        sk_alias="FACET#ALIAS#GROUP#SEC",
    )
    group_name = "TestGroup"
    state = "ACTIVE"
    iso8601utc = "2022-01-01T00:00:00Z"
    expected_key = PrimaryKey(
        partition_key="FACET#ALIAS#GROUP#TestGroup",
        sort_key="STATE#ACTIVE#DATE#2022-01-01T00:00:00Z",
    )
    assert (
        get_historic_gsi_2_key(facet, group_name, state, iso8601utc)
        == expected_key
    )


def test_get_historic_gsi_3_key() -> None:
    group_name = "TestGroup"
    state = "ACTIVE"
    iso8601utc = "2022-01-01T00:00:00Z"
    expected_key = PrimaryKey(
        partition_key="GROUP#TestGroup",
        sort_key="STATE#ACTIVE#DATE#2022-01-01T00:00:00Z",
    )
    assert (
        get_historic_gsi_3_key(group_name, state, iso8601utc) == expected_key
    )


def test_format_key_from_pk_search() -> None:
    group_result_str = "#TestGroup"
    group_result_non_str = 123
    assert format_key_from_pk_search(group_result_str) == "TestGroup"
    assert format_key_from_pk_search(group_result_non_str) == ""


def test_format_policies_to_update_all_fields() -> None:
    policies_data = {
        "inactivity_period": "30",
        "max_acceptance_days": "60",
        "max_acceptance_severity": "0.5",
        "max_number_acceptances": "5",
        "min_acceptance_severity": "0.2",
        "min_breaking_severity": "0.8",
        "vulnerability_grace_period": "15",
    }
    expected = PoliciesToUpdate(
        inactivity_period=30,
        max_acceptance_days=60,
        max_acceptance_severity=Decimal("0.5"),
        max_number_acceptances=5,
        min_acceptance_severity=Decimal("0.2"),
        min_breaking_severity=Decimal("0.8"),
        vulnerability_grace_period=15,
    )
    assert format_policies_to_update(policies_data) == expected


def test_format_policies_to_update_missing_fields() -> None:
    policies_data = {
        "inactivity_period": "30",
        "max_acceptance_days": "60",
    }
    expected = PoliciesToUpdate(
        inactivity_period=30,
        max_acceptance_days=60,
    )
    result = format_policies_to_update(policies_data)
    assert result == expected
    assert result.max_acceptance_severity is None
    assert result.max_number_acceptances is None
    assert result.min_acceptance_severity is None
    assert result.min_breaking_severity is None
    assert result.vulnerability_grace_period is None


def test_format_policies_to_update_none_fields() -> None:
    policies_data: dict[str, str] = {}
    expected = PoliciesToUpdate()

    assert format_policies_to_update(policies_data) == expected


@pytest.mark.parametrize(
    "base_iso8601,target_iso8601,offset,expected",
    [
        (
            datetime(2022, 1, 1, 12),
            datetime(2022, 1, 1, 12, 0, 5),
            1,
            datetime(2022, 1, 1, 12, 0, 5),
        ),
        (
            datetime(2022, 1, 1, 12),
            datetime(2022, 1, 1, 11, 59, 50),
            1,
            datetime(2022, 1, 1, 12, 0, 1),
        ),
        (
            datetime(2022, 1, 1, 12),
            datetime(2022, 1, 1, 11, 59, 58),
            10,
            datetime(2022, 1, 1, 12, 0, 10),
        ),
        (
            datetime(2022, 1, 1, 12),
            datetime(2022, 1, 1, 12),
            1,
            datetime(2022, 1, 1, 12, 0, 1),
        ),
    ],
)
def test_get_datetime_with_offset(
    base_iso8601: datetime,
    target_iso8601: datetime,
    offset: int,
    expected: datetime,
) -> None:
    result = get_datetime_with_offset(base_iso8601, target_iso8601, offset)
    assert result == expected


@pytest.mark.parametrize(
    "input_,expected",
    [
        ({1, 2, 3}, [1, 2, 3]),
        (
            datetime(2022, 1, 1, 12, tzinfo=timezone.utc),
            "2022-01-01T12:00:00+00:00",
        ),
        (3.14159, Decimal("3.14159")),
        ("string", "string"),
        (123, 123),
        (True, True),
    ],
)
def test_serialize(input_: object, expected: object) -> None:
    result = serialize(input_)
    if isinstance(expected, list):
        assert set(result) == set(expected)
    else:
        assert result == expected
