from collections.abc import (
    Callable,
)
from datetime import (
    datetime,
)
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model.reports.types import (
    Report,
    ReportState,
)
from integrates.tickets.domain import (
    _get_recipient_first_name_async,
    create_ticket_for_billing_anomaly_alert,
)
import pytest
from test.unit.src.utils import (
    get_module_at_test,
)
from unittest.mock import (
    AsyncMock,
    patch,
)
import uuid

MODULE_AT_TEST = f"integrates.{get_module_at_test(file_path=__file__)}"
pytestmark = [
    pytest.mark.asyncio,
]


@pytest.mark.parametrize(
    ["email", "expected_first_name"],
    [
        ["integratesuser@gmail.com", "Integrates"],
        ["forces.unittesting@fluidattacks.com", "forces.unittesting"],
    ],
)
@patch(
    MODULE_AT_TEST + "Dataloaders.stakeholder",
    new_callable=AsyncMock,
)
async def test_recipient_first_name(
    mock_dataloaders_stakeholder: AsyncMock,
    email: str,
    expected_first_name: str,
    mock_data_for_module: Callable,
) -> None:
    # Set up mock's result using mock_data_for_module fixture
    mock_dataloaders_stakeholder.load.return_value = mock_data_for_module(
        mock_path="Dataloaders.stakeholder",
        mock_args=[email],
        module_at_test=MODULE_AT_TEST,
    )
    loaders: Dataloaders = get_new_context()
    result = await _get_recipient_first_name_async(loaders, email)
    assert result == expected_first_name
    assert mock_dataloaders_stakeholder.load.called is True


@patch(
    MODULE_AT_TEST + "tickets_dal.create_ticket",
    new_callable=AsyncMock,
    return_value=None,
)
async def test_create_ticket_for_billing_anomaly_alert(
    mock_tickets_dal_create_ticket: AsyncMock,
) -> None:
    report = Report(
        entity_id=uuid.uuid4(),
        origin="billing",
        state=ReportState(
            modified_at=datetime.fromisoformat("2020-01-02T03:04:05Z"),
            modified_by="modified_by@fluidattacks.com",
            status="open",
            status_reason="status_reason",
        ),
        message="Authors Lost",
    )

    await create_ticket_for_billing_anomaly_alert(
        report=report,
        organization_name="organization_name",
        requester_email="requester_email@fluidattacks.com",
        customer_manager="customer_manager@fluidattacks.com",
    )

    mock_tickets_dal_create_ticket.assert_called_once_with(
        subject="A billing alert has been reported on [organization_name]",
        description="""
            - Take place at: 2020-01-02T03:04:05+00:00
            - Details: Authors Lost
            - Reported by: modified_by@fluidattacks.com
            - Organization customer manager: customer_manager@fluidattacks.com
        """,
        requester_email="requester_email@fluidattacks.com",
    )
