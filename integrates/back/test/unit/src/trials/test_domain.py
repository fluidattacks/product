from datetime import (
    datetime,
)
from freezegun import (
    freeze_time,
)
from integrates.db_model.trials.enums import (
    TrialStatus,
)
from integrates.db_model.trials.types import (
    Trial,
)
from integrates.trials.getters import (
    get_days_since_expiration,
    get_status,
)
import pytest

# Constants
pytestmark = [
    pytest.mark.asyncio,
]


@pytest.mark.parametrize(
    ["trial", "status"],
    [
        (
            Trial(
                email="",
                completed=False,
                start_date=None,
                extension_days=0,
                extension_date=None,
                reason=None,
            ),
            TrialStatus.TRIAL,
        ),
        (
            Trial(
                email="",
                completed=False,
                extension_date=None,
                extension_days=0,
                start_date=datetime.fromisoformat("2021-12-20T00:00:00+00:00"),
                reason=None,
            ),
            TrialStatus.TRIAL,
        ),
        (
            Trial(
                email="",
                completed=True,
                extension_date=None,
                extension_days=0,
                start_date=datetime.fromisoformat("2021-12-01T00:00:00+00:00"),
                reason=None,
            ),
            TrialStatus.TRIAL_ENDED,
        ),
        (
            Trial(
                email="",
                completed=False,
                extension_date=datetime.fromisoformat(
                    "2021-12-30T00:00:00+00:00"
                ),
                extension_days=9,
                start_date=datetime.fromisoformat("2021-12-01T00:00:00+00:00"),
                reason=None,
            ),
            TrialStatus.EXTENDED,
        ),
        (
            Trial(
                email="",
                completed=True,
                extension_date=datetime.fromisoformat(
                    "2021-12-01T00:00:00+00:00"
                ),
                extension_days=9,
                start_date=datetime.fromisoformat("2021-11-01T00:00:00+00:00"),
                reason=None,
            ),
            TrialStatus.EXTENDED_ENDED,
        ),
    ],
)
@freeze_time("2022-01-01")
async def test_get_status(trial: Trial, status: TrialStatus) -> None:
    assert get_status(trial) == status


@pytest.mark.parametrize(
    ["trial", "expected"],
    [
        (
            Trial(
                email="",
                completed=False,
                start_date=None,
                extension_days=0,
                extension_date=None,
                reason=None,
            ),
            0,
        ),
        (
            Trial(
                email="",
                completed=True,
                extension_date=datetime.fromisoformat(
                    "2023-10-01T00:00:00+00:00"
                ),
                extension_days=9,
                start_date=None,
                reason=None,
            ),
            2,
        ),
        (
            Trial(
                email="",
                completed=True,
                extension_date=None,
                extension_days=0,
                start_date=datetime.fromisoformat("2023-09-20T00:00:00+00:00"),
                reason=None,
            ),
            1,
        ),
        (
            Trial(
                email="",
                completed=True,
                extension_date=None,
                extension_days=0,
                start_date=None,
                reason=None,
            ),
            0,
        ),
    ],
)
@freeze_time("2023-10-12")
async def test_get_days_since_expiration(trial: Trial, expected: int) -> None:
    assert get_days_since_expiration(trial) == expected
