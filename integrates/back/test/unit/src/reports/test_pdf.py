from datetime import (
    datetime,
)
from integrates.dataloaders import (
    get_new_context,
)
from integrates.db_model.roots.enums import (
    RootEnvironmentCloud,
    RootEnvironmentUrlStateStatus,
    RootEnvironmentUrlType,
)
from integrates.db_model.roots.types import (
    RootEnvironmentUrl,
    RootEnvironmentUrlState,
)
from integrates.reports.pdf import (
    _format_url,
    _get_urls,
)
import pytest
from test.unit.src.utils import (
    get_module_at_test,
)
from typing import (
    Any,
)
from unittest.mock import (
    AsyncMock,
    patch,
)

MODULE_AT_TEST = f"integrates.{get_module_at_test(file_path=__file__)}"


@pytest.mark.parametrize(
    "root_url, expected_url",
    [
        (
            RootEnvironmentUrl(
                id="33445",
                url="http://test.com",
                root_id="",
                group_name="group1",
                state=RootEnvironmentUrlState(
                    modified_by="admin@gmail.com",
                    modified_date=datetime.fromisoformat(
                        "2020-11-19T13:37:10+00:00"
                    ),
                    status=RootEnvironmentUrlStateStatus.CREATED,
                    url_type=RootEnvironmentUrlType.URL,
                ),
            ),
            "http://test.com",
        ),
        (
            RootEnvironmentUrl(
                id="64563",
                url="http://test2.com",
                root_id="",
                group_name="group1",
                state=RootEnvironmentUrlState(
                    modified_by="admin@gmail.com",
                    modified_date=datetime.fromisoformat(
                        "2020-11-19T13:37:10+00:00"
                    ),
                    status=RootEnvironmentUrlStateStatus.CREATED,
                ),
            ),
            "http://test2.com",
        ),
        (
            RootEnvironmentUrl(
                id="6453563",
                url="http://test3.com",
                root_id="",
                group_name="group1",
                state=RootEnvironmentUrlState(
                    modified_by="admin@gmail.com",
                    modified_date=datetime.fromisoformat(
                        "2020-11-19T13:37:10+00:00"
                    ),
                    status=RootEnvironmentUrlStateStatus.CREATED,
                    cloud_name=RootEnvironmentCloud.GCP,
                    url_type=RootEnvironmentUrlType.CSPM,
                ),
            ),
            "CSPM: GCP: http://test3.com",
        ),
    ],
)
def test_format_url(root_url: RootEnvironmentUrl, expected_url: str) -> None:
    assert _format_url(root_url) == expected_url


@patch(
    MODULE_AT_TEST + "Dataloaders.root_environment_urls",
    new_callable=AsyncMock,
)
@pytest.mark.asyncio
async def test_get_urls_dataloaders(
    mock_data_for_module: Any,
    root_id: str = "78dd64d3198473115a7f5263d27bed15f9f2fc07",
) -> None:
    assert mock_data_for_module(
        mock_path="Dataloaders.root_environment_urls",
        mock_args=[root_id],
        module_at_test=MODULE_AT_TEST,
    )
    await _get_urls(get_new_context(), root_id, "group_1")
