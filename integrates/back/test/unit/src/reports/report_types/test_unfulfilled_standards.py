from integrates.custom_exceptions import (
    InvalidParameter,
    InvalidStandardId,
)
from integrates.dataloaders import (
    get_new_context,
)
from integrates.reports.report_types.unfulfilled_standards import (
    generate_csv_file,
)
import pytest


@pytest.mark.asyncio
@pytest.mark.parametrize(
    ["unfulfilled_standards"],
    [
        [["bsimm"]],
    ],
)
async def test_generate_csv_file(
    unfulfilled_standards: set[str] | None,
) -> None:
    group_name: str = "unittesting"
    loaders = get_new_context()
    result: str | None = await generate_csv_file(
        loaders=loaders,
        group_name=group_name,
        unfulfilled_standards=unfulfilled_standards,
    )

    assert result is not None
    assert "integrates_report_unfulfilled-standards-unittesting" in result


@pytest.mark.asyncio
@pytest.mark.parametrize(
    ["unfulfilled_standards"],
    [
        [["test_standard_error"]],
    ],
)
async def test_invalid_unfulfilled_standards_generate_csv_file(
    unfulfilled_standards: set[str] | None,
) -> None:
    group_name: str = "unittesting"
    loaders = get_new_context()
    with pytest.raises(InvalidStandardId) as exc_info:
        await generate_csv_file(
            loaders=loaders,
            group_name=group_name,
            unfulfilled_standards=unfulfilled_standards,
        )

    assert str(exc_info.value) == "Exception - The standard id is invalid"


@pytest.mark.asyncio
async def test_none_unfulfilled_standards_generate_csv_file() -> None:
    group_name: str = "unittesting"
    loaders = get_new_context()
    with pytest.raises(InvalidParameter) as exc_info:
        await generate_csv_file(
            loaders=loaders, group_name=group_name, unfulfilled_standards=None
        )

    assert (
        str(exc_info.value)
        == "Exception - Field unfulfilledStandards is invalid"
    )
