from datetime import (
    datetime,
)
from decimal import (
    Decimal,
)
from integrates.db_model.toe_lines.types import (
    ToeLine,
    ToeLineState,
)
from integrates.reports.report_types.toe_lines import (
    _get_coverage,
    get_valid_field,
)
import pytest


@pytest.mark.parametrize(
    ["field", "expect_result"],
    [
        ["valid_input", "valid_input"],
        ["__bad_input__", "__bad_input__"],
        ["=SUM(A1:A10)", ""],
        ["", ""],
    ],
)
def test_get_valid_field(field: str, expect_result: str) -> None:
    assert get_valid_field(field) == expect_result


@pytest.mark.parametrize(
    ["toe_line"],
    [
        [
            ToeLine(
                filename="README.md",
                group_name="unittesting",
                root_id="77637717-41d4-4242-854a-db8ff7fe5ed0",
                state=ToeLineState(
                    attacked_at=None,
                    attacked_by="machine@fluidattacks.com",
                    attacked_lines=50,
                    be_present=True,
                    be_present_until=None,
                    comments="",
                    first_attack_at=None,
                    loc=100,
                    modified_by="machine@fluidattacks.com",
                    modified_date=datetime.fromisoformat(
                        "2022-09-05T00:45:11+00:00"
                    ),
                    has_vulnerabilities=False,
                    last_author="customer1@gmail.com",
                    last_commit="be8d00f5fe64d59dc463adb34f9fabdf262e1ed9",
                    last_commit_date=datetime.fromisoformat(
                        "2022-09-05T00:45:11+00:00"
                    ),
                    seen_at=datetime.fromisoformat(
                        "2020-01-01T15:41:04+00:00"
                    ),
                    sorts_risk_level=0,
                ),
            )
        ],
    ],
)
def test_get_coverage(toe_line: ToeLine) -> None:
    result = _get_coverage(toe_line=toe_line)
    assert result == str(Decimal("0.500"))


@pytest.mark.parametrize(
    ["toe_line_with_loc_zero"],
    [
        [
            ToeLine(
                filename="README.md",
                group_name="unittesting",
                root_id="77637717-41d4-4242-854a-db8ff7fe5ed0",
                state=ToeLineState(
                    attacked_at=None,
                    attacked_by="machine@fluidattacks.com",
                    attacked_lines=50,
                    be_present=True,
                    be_present_until=None,
                    comments="",
                    first_attack_at=None,
                    loc=0,
                    modified_by="machine@fluidattacks.com",
                    modified_date=datetime.fromisoformat(
                        "2022-09-05T00:45:11+00:00"
                    ),
                    has_vulnerabilities=False,
                    last_author="customer1@gmail.com",
                    last_commit="be8d00f5fe64d59dc463adb34f9fabdf262e1ed9",
                    last_commit_date=datetime.fromisoformat(
                        "2022-09-05T00:45:11+00:00"
                    ),
                    seen_at=datetime.fromisoformat(
                        "2020-01-01T15:41:04+00:00"
                    ),
                    sorts_risk_level=0,
                ),
            )
        ],
    ],
)
def test_get_coverage_with_loc_zero(toe_line_with_loc_zero: ToeLine) -> None:
    result = _get_coverage(toe_line=toe_line_with_loc_zero)
    assert result == "1"
