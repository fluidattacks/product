from integrates.context import (
    STARTDIR,
)
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.reports.secure_pdf import (
    PDF,
    SecurePDF,
)
import pytest
from test.unit.src.utils import (
    get_module_at_test,
    set_mocks_return_values,
)
from unittest.mock import (
    AsyncMock,
    patch,
)

MODULE_AT_TEST = f"integrates.{get_module_at_test(file_path=__file__)}"

pytestmark = [
    pytest.mark.asyncio,
]


@pytest.mark.parametrize(
    ["group_name", "user_email", "output_filename"],
    [
        [
            "unittesting",
            "org_testgroupmanager2@fluidattacks.com",
            "output.pdf",
        ],
    ],
)
@patch(MODULE_AT_TEST + "Dataloaders.group", new_callable=AsyncMock)
async def test_secure_pdf_create_full(
    mock_dataloaders_group: AsyncMock,
    group_name: str,
    user_email: str,
    output_filename: str,
) -> None:
    assert set_mocks_return_values(
        mocks_args=[[group_name]],
        mocked_objects=[mock_dataloaders_group.load],
        module_at_test=MODULE_AT_TEST,
        paths_list=["Dataloaders.group"],
    )
    loaders: Dataloaders = get_new_context()
    path = f"{STARTDIR}/integrates/back/integrates/reports/tpls/results_pdf/"
    expected_path = path + output_filename
    secure_pdf = SecurePDF()

    report_filename = await secure_pdf.create_full(
        loaders, user_email, output_filename, group_name
    )

    assert expected_path == report_filename


class TestPDF:
    @pytest.fixture
    def pdf(self) -> PDF:
        return PDF()

    def test_set_user(self, pdf: PDF) -> None:
        pdf.set_user("test@user.com")
        assert pdf.user == "test@user.com"

    @patch("integrates.reports.secure_pdf.FPDF.set_y")
    @patch("integrates.reports.secure_pdf.FPDF.set_font")
    @patch("integrates.reports.secure_pdf.FPDF.cell")
    def test_footer(
        self,
        mock_cell: AsyncMock,
        mock_font: AsyncMock,
        mock_y: AsyncMock,
        pdf: PDF,
    ) -> None:
        pdf.footer()

        mock_y.assert_called_with(-15)
        mock_font.assert_called_with("Times", "", 12)

        assert mock_cell.call_count == 2
        assert mock_cell.call_args_list[0][0][2] == "Only for "
