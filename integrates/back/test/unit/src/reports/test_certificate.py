import datetime
from decimal import (
    Decimal,
)
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model.findings.types import (
    Finding,
)
from integrates.findings.types import (
    SeverityLevelsInfo,
    SeverityLevelSummary,
)
from integrates.reports.certificate import (
    _format_cvssf,
    _set_percentage,
    format_finding,
    resolve_month_name,
)
from integrates.reports.types import (
    CertFindingInfo,
)
import pytest
from test.unit.src.utils import (
    get_module_at_test,
)
from unittest.mock import (
    AsyncMock,
    MagicMock,
    patch,
)

MODULE_AT_TEST = f"integrates.{get_module_at_test(file_path=__file__)}"

pytestmark = [
    pytest.mark.asyncio,
]


@patch(
    MODULE_AT_TEST + "findings_domain.get_severity_levels_info",
    new_callable=AsyncMock,
)
@patch(
    MODULE_AT_TEST + "findings_domain.get_safe_cvssf_v3",
    new_callable=AsyncMock,
)
@patch(
    MODULE_AT_TEST + "findings_domain.get_total_cvssf_v3",
    new_callable=AsyncMock,
)
@patch(
    MODULE_AT_TEST + "findings_domain.get_severity_levels_info_v4",
    new_callable=AsyncMock,
)
@patch(
    MODULE_AT_TEST + "findings_domain.get_safe_cvssf",
    new_callable=AsyncMock,
)
@patch(
    MODULE_AT_TEST + "findings_domain.get_total_cvssf",
    new_callable=AsyncMock,
)
# pylint: disable=too-many-arguments
async def test_format_finding(
    mock_findings_domain_get_total_cvssf: AsyncMock,
    mock_findings_domain_get_safe_cvssf: AsyncMock,
    mock_findings_domain_get_severity_levels_info: AsyncMock,
    mock_findings_domain_get_total_cvssf_v4: AsyncMock,
    mock_findings_domain_get_safe_cvssf_v4: AsyncMock,
    mock_findings_domain_get_severity_levels_info_v4: AsyncMock,
) -> None:
    level_test = SeverityLevelsInfo(
        critical=SeverityLevelSummary(0, 1, 2),
        high=SeverityLevelSummary(0, 1, 3),
        medium=SeverityLevelSummary(0, 1, 4),
        low=SeverityLevelSummary(0, 1, 5),
    )
    mock_findings_domain_get_safe_cvssf.return_value = Decimal("15")
    mock_findings_domain_get_total_cvssf.return_value = Decimal("20")
    mock_findings_domain_get_severity_levels_info.return_value = level_test
    mock_findings_domain_get_safe_cvssf_v4.return_value = Decimal("15")
    mock_findings_domain_get_total_cvssf_v4.return_value = Decimal("20")
    mock_findings_domain_get_severity_levels_info_v4.return_value = level_test

    safe_vulnerabilities = mock_findings_domain_get_safe_cvssf.return_value
    total_vulnerabilities = mock_findings_domain_get_total_cvssf.return_value
    severity_levels = (
        mock_findings_domain_get_severity_levels_info.return_value
    )

    loaders: Dataloaders = get_new_context()
    finding = MagicMock(spec=Finding)
    finding.title = "title_tested"

    result = await format_finding(loaders, finding)

    expected_result = CertFindingInfo(
        closed_exposure=safe_vulnerabilities,
        total_exposure=total_vulnerabilities,
        severity_levels=severity_levels,
        closed_exposure_v4=mock_findings_domain_get_safe_cvssf_v4.return_value,
        total_exposure_v4=mock_findings_domain_get_total_cvssf_v4.return_value,
        severity_levels_v4=(
            mock_findings_domain_get_severity_levels_info_v4.return_value
        ),
    )

    assert result == expected_result


@pytest.mark.parametrize(
    ["total_vulns", "closed_vulns", "expect_result"],
    [
        ["10", "10", "100%"],
        ["10", "5", "50%"],
        ["6", "5", "83.3%"],
        ["0", "0", "N/A"],
        ["0", "5", "N/A"],
        ["10", "0", "0%"],
    ],
)
def test_set_percentage(
    total_vulns: str, closed_vulns: str, expect_result: str
) -> None:
    assert (
        _set_percentage(Decimal(total_vulns), Decimal(closed_vulns))
        == expect_result
    )


@pytest.mark.parametrize(
    ["lang", "words", "expect_result"],
    [
        ["en", {"test": "test_value"}, "November"],
        ["es", {"november": "test_november"}, "test_november"],
    ],
)
def test_resolve_month_name(
    lang: str, words: dict, expect_result: str
) -> None:
    date = datetime.datetime(2020, 11, 5)
    assert resolve_month_name(lang, date, words) == expect_result


@pytest.mark.parametrize(
    ["cvssf", "expect_cvssf"],
    [
        ["10.0", "10"],
        ["11.0", "11"],
        ["9.9", "9.9"],
        ["5.5", "5.5"],
        ["10.1", "11"],
    ],
)
def test__format_cvssf(
    cvssf: str,
    expect_cvssf: str,
) -> None:
    assert _format_cvssf(Decimal(cvssf)) == Decimal(expect_cvssf)
