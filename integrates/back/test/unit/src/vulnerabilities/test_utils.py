from datetime import (
    datetime,
)
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model.enums import (
    AcceptanceStatus,
    Source,
    TreatmentStatus,
)
from integrates.db_model.types import (
    Treatment,
)
from integrates.db_model.vulnerabilities.enums import (
    VulnerabilityStateStatus,
    VulnerabilityVerificationStatus,
    VulnerabilityZeroRiskStatus,
)
from integrates.db_model.vulnerabilities.types import (
    VulnerabilityState,
    VulnerabilityVerification,
    VulnerabilityZeroRisk,
)
from integrates.db_model.vulnerabilities.utils import (
    format_vulnerability,
    get_group_index_key,
    get_new_group_index_key,
)
from integrates.dynamodb.types import (
    PrimaryKey,
)
import pytest


@pytest.mark.asyncio
async def test_group_index_utils() -> None:
    loaders: Dataloaders = get_new_context()

    current_value = await loaders.vulnerability.load(
        "15375781-31f2-4953-ac77-f31134225747"
    )
    assert current_value
    assert get_group_index_key(current_value) == PrimaryKey(
        partition_key="GROUP#unittesting",
        sort_key="VULN#ZR#false#STATE#vulnerable#TREAT#false",
    )

    new_state = VulnerabilityState(
        modified_by="test@unittesting.com",
        modified_date=datetime.fromisoformat("2023-03-14T00:45:15+00:00"),
        source=Source.ASM,
        specific="2321",
        status=VulnerabilityStateStatus.SAFE,
        where="192.168.1.2",
    )
    assert get_new_group_index_key(current_value, new_state) == PrimaryKey(
        partition_key="GROUP#unittesting",
        sort_key="VULN#ZR#false#STATE#safe#TREAT#false",
    )

    new_accepted_treatment = Treatment(
        modified_date=datetime.fromisoformat("2023-03-14T00:45:15+00:00"),
        status=TreatmentStatus.ACCEPTED,
    )
    assert get_new_group_index_key(
        current_value, new_accepted_treatment
    ) == PrimaryKey(
        partition_key="GROUP#unittesting",
        sort_key="VULN#ZR#false#STATE#vulnerable#TREAT#false",
    )

    new_confirmed_accepted_treatment = Treatment(
        acceptance_status=AcceptanceStatus.APPROVED,
        modified_date=datetime.fromisoformat("2023-03-14T00:45:15+00:00"),
        status=TreatmentStatus.ACCEPTED,
    )
    assert get_new_group_index_key(
        current_value, new_confirmed_accepted_treatment
    ) == PrimaryKey(
        partition_key="GROUP#unittesting",
        sort_key="VULN#ZR#false#STATE#vulnerable#TREAT#true",
    )

    new_permanent_treatment = Treatment(
        modified_date=datetime.fromisoformat("2023-03-14T00:45:15+00:00"),
        status=TreatmentStatus.ACCEPTED_UNDEFINED,
    )
    assert get_new_group_index_key(
        current_value, new_permanent_treatment
    ) == PrimaryKey(
        partition_key="GROUP#unittesting",
        sort_key="VULN#ZR#false#STATE#vulnerable#TREAT#false",
    )

    new_confirmed_permanent_treatment = Treatment(
        acceptance_status=AcceptanceStatus.APPROVED,
        modified_date=datetime.fromisoformat("2023-03-14T00:45:15+00:00"),
        status=TreatmentStatus.ACCEPTED_UNDEFINED,
    )
    assert get_new_group_index_key(
        current_value, new_confirmed_permanent_treatment
    ) == PrimaryKey(
        partition_key="GROUP#unittesting",
        sort_key="VULN#ZR#false#STATE#vulnerable#TREAT#true",
    )

    new_verification = VulnerabilityVerification(
        modified_by="unittest@fluidattacks.com",
        modified_date=datetime.fromisoformat("2023-03-14T00:45:15+00:00"),
        status=VulnerabilityVerificationStatus.VERIFIED,
        event_id=None,
    )
    assert get_new_group_index_key(current_value, new_verification) is None

    new_requested_zero_risk = VulnerabilityZeroRisk(
        comment_id="123456",
        modified_by="test@gmail.com",
        modified_date=datetime.fromisoformat("2023-03-14T00:45:15+00:00"),
        status=VulnerabilityZeroRiskStatus.REQUESTED,
    )
    assert get_new_group_index_key(
        current_value, new_requested_zero_risk
    ) == PrimaryKey(
        partition_key="GROUP#unittesting",
        sort_key="VULN#ZR#true#STATE#vulnerable#TREAT#false",
    )

    new_confirmed_zero_risk = VulnerabilityZeroRisk(
        comment_id="123456",
        modified_by="test@gmail.com",
        modified_date=datetime.fromisoformat("2023-03-14T00:45:15+00:00"),
        status=VulnerabilityZeroRiskStatus.CONFIRMED,
    )
    assert get_new_group_index_key(
        current_value, new_confirmed_zero_risk
    ) == PrimaryKey(
        partition_key="GROUP#unittesting",
        sort_key="VULN#ZR#true#STATE#vulnerable#TREAT#false",
    )


def test_format_vulnerability_root_id() -> None:
    item = {
        "treatment": {
            "modified_date": "2021-09-13T20:19:17+00:00",
            "status": "UNTREATED",
        },
        "hacker_email": "unittest@fluidattacks.com",
        "group_name": "asgard",
        "pk_5": "GROUP#asgard",
        "pk_6": "FIN#aeb1a8c9-e0fc-44f6-b6a6-e6ccfc1997c1",
        "organization_name": "makimachi",
        "type": "LINES",
        "technique": "SCA",
        "created_by": "unittest@fluidattacks.com",
        "sk": "FIN#aeb1a8c9-e0fc-44f6-b6a6-e6ccfc1997c1",
        "sk_3": "VULN#060b9ee2-cf52-4192-8822-a30420b6c34a",
        "pk_2": "ROOT#3a69ee71-1183-4cbe-99d9-27b2617df7d6",
        "root_id": "3a69ee71-1183-4cbe-99d9-27b2617df7d5",
        "created_date": "2021-09-13T20:19:17+00:00",
        "pk": "VULN#060b9ee2-cf52-4192-8822-a30420b6c34a",
        "pk_3": "USER",
        "sk_5": "VULN#ZR#false#STATE#vulnerable#TREAT#false",
        "state": {
            "modified_by": "unittest@fluidattacks.com",
            "commit": "98dbb971caf519fb6c56c67df7e02b253fd5a97f",
            "where": "scanners/skipfish/Dockerfile",
            "source": "MACHINE",
            "modified_date": "2021-09-13T20:19:17+00:00",
            "specific": "0",
            "status": "VULNERABLE",
        },
        "sk_2": "VULN#060b9ee2-cf52-4192-8822-a30420b6c34a",
    }
    vulnerability_with_root_and_pk = format_vulnerability(
        item  # type: ignore [arg-type]
    )
    assert (
        vulnerability_with_root_and_pk.root_id
        == "3a69ee71-1183-4cbe-99d9-27b2617df7d5"
    )
    item.pop("root_id")
    vulnerability_with_pk = format_vulnerability(
        item  # type: ignore [arg-type]
    )
    assert (
        vulnerability_with_pk.root_id == "3a69ee71-1183-4cbe-99d9-27b2617df7d6"
    )
    item.pop("pk_2")
    vulnerability_with_no_root_pk = format_vulnerability(
        item  # type: ignore [arg-type]
    )
    assert vulnerability_with_no_root_pk.root_id == ""
