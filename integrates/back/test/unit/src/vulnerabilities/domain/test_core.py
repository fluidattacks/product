# pylint:disable=too-many-lines
from datetime import (
    datetime,
)
from decimal import (
    Decimal,
)
from freezegun import (
    freeze_time,
)
from integrates.class_types.types import (
    Item,
)
from integrates.custom_utils.datetime import (
    get_now_minus_delta,
)
from integrates.custom_utils.reports import (
    filter_context,
)
from integrates.dataloaders import (
    get_new_context,
)
from integrates.db_model.enums import (
    AcceptanceStatus,
    Source,
    TreatmentStatus,
)
from integrates.db_model.types import (
    Treatment,
)
from integrates.db_model.vulnerabilities.enums import (
    VulnerabilityStateStatus,
    VulnerabilityTechnique,
    VulnerabilityToolImpact,
    VulnerabilityType,
    VulnerabilityZeroRiskStatus,
)
from integrates.db_model.vulnerabilities.types import (
    Vulnerability,
    VulnerabilityState,
    VulnerabilityTool,
    VulnerabilityUnreliableIndicators,
    VulnerabilityZeroRisk,
)
from integrates.vulnerabilities.domain import (
    get_open_vulnerabilities_specific_by_type,
    get_reattack_requester,
    get_treatments_count,
    get_updated_manager_mail_content,
    group_vulnerabilities,
    mask_vulnerability,
    send_treatment_change_mail,
)
from integrates.vulnerabilities.domain.core import (
    add_comment_by_exclusion,
    add_comments_by_exclusion,
    add_tags,
    get_content,
    get_vulns_filters_by_root_nickname,
    group_vulnerabilities_by_finding_id,
)
from integrates.vulnerabilities.types import (
    ToolItem,
)
import pytest
from test.unit.src.utils import (
    get_mocked_path,
    get_module_at_test,
    set_mocks_return_values,
)
from unittest.mock import (
    AsyncMock,
    patch,
)

MODULE_AT_TEST = f"integrates.{get_module_at_test(file_path=__file__)}"

pytestmark = [
    pytest.mark.asyncio,
]


@pytest.mark.parametrize(
    ["finding_id", "expected"],
    [
        [
            "422286126",
            {
                VulnerabilityType.PORTS: [],
                VulnerabilityType.LINES: [
                    Vulnerability(
                        created_by="unittest@fluidattacks.com",
                        created_date=datetime.fromisoformat(
                            "2020-01-03T17:46:10+00:00"
                        ),
                        finding_id="422286126",
                        group_name="unittesting",
                        organization_name="okada",
                        hacker_email="unittest@fluidattacks.com",
                        id="0a848781-b6a4-422e-95fa-692151e6a98f",
                        state=VulnerabilityState(
                            commit="ea871eee64cfd5ce293411efaf4d3b446d04eb4a",
                            modified_by="unittest@fluidattacks.com",
                            modified_date=datetime.fromisoformat(
                                "2020-01-03T17:46:10+00:00"
                            ),
                            source=Source.ASM,
                            specific="12",
                            status=VulnerabilityStateStatus.VULNERABLE,
                            reasons=None,
                            tool=VulnerabilityTool(
                                name="tool-2",
                                impact=VulnerabilityToolImpact.INDIRECT,
                            ),
                            where="test/data/lib_path/f060/csharp.cs",
                        ),
                        technique=VulnerabilityTechnique.SCR,
                        type=VulnerabilityType.LINES,
                        bug_tracking_system_url=None,
                        custom_severity=None,
                        hash=None,
                        root_id="4039d098-ffc5-4984-8ed3-eb17bca98e19",
                        stream=None,
                        tags=None,
                        treatment=Treatment(
                            modified_date=datetime.fromisoformat(
                                "2020-01-03T17:46:10+00:00"
                            ),
                            status=TreatmentStatus.IN_PROGRESS,
                            acceptance_status=None,
                            accepted_until=None,
                            justification="test justification",
                            assigned="integratesuser2@gmail.com",
                            modified_by="integratesuser@gmail.com",
                        ),
                        verification=None,
                        zero_risk=None,
                    ),
                ],
                VulnerabilityType.INPUTS: [],
            },
        ],
        [
            "988493279",
            {
                VulnerabilityType.PORTS: [
                    Vulnerability(
                        created_by="unittest@fluidattacks.com",
                        created_date=datetime.fromisoformat(
                            "2019-04-08T00:45:15+00:00"
                        ),
                        finding_id="988493279",
                        group_name="unittesting",
                        organization_name="okada",
                        hacker_email="unittest@fluidattacks.com",
                        id="47ce0fb0-4108-49b0-93cc-160dce8168a6",
                        state=VulnerabilityState(
                            commit=None,
                            modified_by="unittest@fluidattacks.com",
                            modified_date=datetime.fromisoformat(
                                "2019-04-08T00:45:15+00:00"
                            ),
                            source=Source.ASM,
                            specific="8888",
                            status=VulnerabilityStateStatus.VULNERABLE,
                            reasons=None,
                            tool=VulnerabilityTool(
                                name="tool-1",
                                impact=VulnerabilityToolImpact.INDIRECT,
                            ),
                            where="192.168.1.19",
                        ),
                        technique=VulnerabilityTechnique.PTAAS,
                        type=VulnerabilityType.PORTS,
                        bug_tracking_system_url=None,
                        custom_severity=None,
                        hash=None,
                        root_id="4039d098-ffc5-4984-8ed3-eb17bca98e19",
                        stream=None,
                        tags=None,
                        treatment=Treatment(
                            modified_date=datetime.fromisoformat(
                                "2020-10-08T00:59:06+00:00"
                            ),
                            status=(TreatmentStatus.ACCEPTED_UNDEFINED),
                            acceptance_status=(AcceptanceStatus.APPROVED),
                            accepted_until=None,
                            justification=(
                                "Observations about permanently accepted"
                            ),
                            assigned="integratesuser@gmail.com",
                            modified_by="integratesuser@gmail.com",
                        ),
                        verification=None,
                        zero_risk=None,
                    ),
                ],
                VulnerabilityType.LINES: [],
                VulnerabilityType.INPUTS: [],
            },
        ],
    ],
)
@patch(
    get_mocked_path("loaders.finding_vulnerabilities_released_nzr.load"),
    new_callable=AsyncMock,
)
async def test_get_open_vulnerabilities_specific_by_type(
    mock_loaders_finding_vulnerabilities_released_nzr: AsyncMock,
    finding_id: str,
    expected: dict[VulnerabilityType, list[Vulnerability]],
) -> None:
    mocked_objects, mocked_paths, mocks_args = [
        [mock_loaders_finding_vulnerabilities_released_nzr],
        ["loaders.finding_vulnerabilities_released_nzr.load"],
        [[finding_id]],
    ]

    assert set_mocks_return_values(
        mocked_objects=mocked_objects,
        paths_list=mocked_paths,
        mocks_args=mocks_args,
    )
    loaders = get_new_context()
    result = await get_open_vulnerabilities_specific_by_type(
        loaders, finding_id
    )
    assert all(mock_object.called is True for mock_object in mocked_objects)
    assert result == expected


async def test_get_reattack_requester() -> None:
    loaders = get_new_context()
    vulnerability = await loaders.vulnerability.load(
        "3bcdb384-5547-4170-a0b6-3b397a245465"
    )
    assert vulnerability
    requester = await get_reattack_requester(
        loaders,
        vuln=vulnerability,
    )
    assert requester == "integratesuser@gmail.com"


@pytest.mark.parametrize(
    ["finding_id", "expected"],
    [
        ["988493279", [0, 1, 0, 0]],
        ["422286126", [0, 0, 1, 0]],
    ],
)
async def test_get_treatments(finding_id: str, expected: list[int]) -> None:
    context = get_new_context()
    finding_vulns_loader = context.finding_vulnerabilities_released_nzr
    vulns = await finding_vulns_loader.load(finding_id)
    treatments = get_treatments_count(vulns)
    assert treatments.accepted == expected[0]
    assert treatments.accepted_undefined == expected[1]
    assert treatments.in_progress == expected[2]
    assert treatments.untreated == expected[3]


@pytest.mark.parametrize(
    "vulnerabilities",
    (
        ({
            "ports": [],
            "lines": [
                {
                    "cvss_v3": (
                        "CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:U/C:N/I:N/A:H"
                    ),
                    "path": "test/data/lib_path/f060/csharp.cs",
                    "line": "12",
                    "state": "open",
                    "source": "analyst",
                    "tool": ToolItem(name="tool-2", impact="indirect"),
                    "commit_hash": "ea871ee",
                }
            ],
            "inputs": [
                {
                    "cvss_v3": (
                        "CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:U/C:N/I:N/A:H"
                    ),
                    "cwe_ids": [
                        "CWE-1035",
                        "CWE-770",
                        "CWE-937",
                    ],
                    "url": "https://example.com",
                    "field": "phone",
                    "state": "open",
                    "source": "analyst",
                    "tool": ToolItem(name="tool-2", impact="indirect"),
                }
            ],
        }),
    ),
)
async def test_get_updated_manager_mail_content(
    vulnerabilities: dict[str, list[Item]],
) -> None:
    test_data = get_updated_manager_mail_content(vulnerabilities)
    expected_output = (
        "test/data/lib_path/f060/csharp.cs (12)\nhttps://example.com (phone)\n"
    )
    assert test_data == expected_output


async def test_group_vulnerabilities() -> None:
    loaders = get_new_context()
    vulns = await loaders.finding_vulnerabilities_all.load("422286126")
    test_data = group_vulnerabilities(vulns)
    assert [
        {
            "where": vuln.state.where,
            "specific": vuln.state.specific,
            "commit": vuln.state.commit,
        }
        for vuln in test_data
    ] == [
        {
            "where": "test/data/lib_path/f060/csharp.cs",
            "specific": "12",
            "commit": "ea871ee",
        },
        {
            "where": "universe/path/to/file3.ext",
            "specific": "345",
            "commit": "e17059d",
        },
        {
            "where": "universe/path/to/file3.ext",
            "specific": "347",
            "commit": "e17059d",
        },
        {"where": "https://example.com", "specific": "phone", "commit": None},
    ]


@pytest.mark.parametrize(
    ["email", "vulnerability"],
    [
        [
            "integratesuser@gmail.com",
            Vulnerability(
                created_by="test@unittesting.com",
                created_date=datetime.fromisoformat(
                    "2020-09-09T21:01:26+00:00"
                ),
                finding_id="422286126",
                group_name="unittesting",
                organization_name="okada",
                hacker_email="test@unittesting.com",
                id="80d6a69f-a376-46be-98cd-2fdedcffdcc0",
                state=VulnerabilityState(
                    modified_by="test@unittesting.com",
                    modified_date=datetime.fromisoformat(
                        "2020-09-09T21:01:26+00:00"
                    ),
                    source=Source.ASM,
                    specific="phone",
                    status=VulnerabilityStateStatus.VULNERABLE,
                    where="https://example.com",
                    commit=None,
                    reasons=None,
                    other_reason=None,
                    tool=VulnerabilityTool(
                        name="tool-2",
                        impact=VulnerabilityToolImpact.INDIRECT,
                    ),
                ),
                technique=VulnerabilityTechnique.SCR,
                type=VulnerabilityType.INPUTS,
                bug_tracking_system_url=None,
                custom_severity=None,
                developer=None,
                event_id=None,
                hash=None,
                root_id="4039d098-ffc5-4984-8ed3-eb17bca98e19",
                skims_method=None,
                stream=None,
                tags=None,
                treatment=Treatment(
                    modified_date=datetime.fromisoformat(
                        "2020-11-23T17:46:10+00:00"
                    ),
                    status=TreatmentStatus.IN_PROGRESS,
                    acceptance_status=None,
                    accepted_until=None,
                    justification="This is a treatment justification",
                    assigned="integratesuser@gmail.com",
                    modified_by="integratesuser2@gmail.com",
                ),
                unreliable_indicators=VulnerabilityUnreliableIndicators(
                    unreliable_efficacy=Decimal("0"),
                    unreliable_reattack_cycles=0,
                    unreliable_treatment_changes=1,
                ),
                verification=None,
                zero_risk=VulnerabilityZeroRisk(
                    comment_id="123456",
                    modified_by="test@gmail.com",
                    modified_date=datetime.fromisoformat(
                        "2020-09-09T21:01:26+00:00"
                    ),
                    status=VulnerabilityZeroRiskStatus.CONFIRMED,
                ),
            ),
        ],
        [
            "integratesuser@gmail.com",
            Vulnerability(
                created_by="test1@gmail.com",
                created_date=datetime.fromisoformat(
                    "2018-04-08T00:45:15+00:00"
                ),
                finding_id="3c475384-834c-47b0-ac71-a41a022e401c",
                group_name="group1",
                organization_name="orgtest",
                hacker_email="test1@gmail.com",
                id="4dbc03e0-4cfc-4b33-9b70-bb7566c460bd",
                root_id="4039d098-ffc5-4984-8ed3-eb17bca98e19",
                state=VulnerabilityState(
                    modified_by="test1@gmail.com",
                    modified_date=datetime.fromisoformat(
                        "2018-04-08T00:45:15+00:00"
                    ),
                    source=Source.ASM,
                    specific="9999",
                    status=VulnerabilityStateStatus.DELETED,
                    where="192.168.1.20",
                ),
                treatment=Treatment(
                    modified_date=datetime.fromisoformat(
                        "2018-04-08T00:45:11+00:00"
                    ),
                    status=TreatmentStatus.UNTREATED,
                ),
                technique=VulnerabilityTechnique.PTAAS,
                type=VulnerabilityType.PORTS,
                unreliable_indicators=VulnerabilityUnreliableIndicators(
                    unreliable_treatment_changes=0,
                ),
            ),
        ],
    ],
)
@patch(get_mocked_path("vulns_model.remove"), new_callable=AsyncMock)
async def test_mask_vulnerability(
    mock_vulns_model_remove: AsyncMock,
    email: str,
    vulnerability: Vulnerability,
) -> None:
    if vulnerability.state.status != VulnerabilityStateStatus.DELETED:
        mocked_objects, mocked_paths, mocks_args = [
            [mock_vulns_model_remove],
            ["vulns_model.remove"],
            [[vulnerability.id]],
        ]

        assert set_mocks_return_values(
            mocked_objects=mocked_objects,
            paths_list=mocked_paths,
            mocks_args=mocks_args,
        )

        await mask_vulnerability(
            email=email,
            finding_id=vulnerability.finding_id,
            vulnerability=vulnerability,
        )

        assert all(
            mock_object.called is True for mock_object in mocked_objects
        )
    else:
        with patch(
            MODULE_AT_TEST + "vulns_model.update_historic_entry"
        ) as mock:
            await mask_vulnerability(
                email=email,
                finding_id=vulnerability.finding_id,
                vulnerability=vulnerability,
            )
            mock.assert_awaited_once()


@freeze_time("2020-10-08")
@pytest.mark.parametrize(
    ["finding_id", "expected"],
    [
        ["988493279", True],
        ["463461507", False],
    ],
)
async def test_send_treatment_change_mail(
    finding_id: str, expected: bool
) -> None:
    context = get_new_context()
    group_name = "dummy"
    finding_title = "dummy"
    modified_by = "unittest@fluidattacks.com"
    assigned = "vulnmanager@gmail.com"
    justification = "test"
    assert (
        await send_treatment_change_mail(
            loaders=context,
            assigned=assigned,
            finding_id=finding_id,
            finding_title=finding_title,
            group_name=group_name,
            justification=justification,
            min_date=get_now_minus_delta(days=1),
            modified_by=modified_by,
        )
        == expected
    )


@pytest.mark.parametrize(
    ["group_name", "root", "expected_id", "nickname"],
    [
        [
            "unittesting",
            "universe/universe/path/to/file3.ext",
            "4039d098-ffc5-4984-8ed3-eb17bca98e19",
            "universe",
        ],
        ["unittesting", "test/data/lib_path/f060/csharp.cs", "", "test"],
    ],
)
@patch(MODULE_AT_TEST + "roots_utils.get_root_id", new_callable=AsyncMock)
async def test_get_vulns_filters_by_root_nickname(
    mock_get_root_id: AsyncMock,
    group_name: str,
    root: str,
    expected_id: str,
    nickname: str,
) -> None:
    loaders = get_new_context()
    mock_get_root_id.return_value = expected_id

    filters = await get_vulns_filters_by_root_nickname(
        loaders, group_name, root
    )

    mock_get_root_id.assert_called_once_with(loaders, group_name, nickname)
    if expected_id:
        expected_filters = [
            {"term": {"root_id.keyword": expected_id}},
            {"match": {"type": VulnerabilityType.LINES.value}},
            {
                "match_phrase_prefix": {
                    "state.where": "universe/path/to/file3.ext"
                }
            },
        ]

        assert filters == expected_filters
    else:
        assert filters == []


@pytest.mark.parametrize(
    [
        "vuln",
        "tags",
    ],
    [
        [
            Vulnerability(
                created_by="Some vuln creator",
                created_date=datetime.now(),
                finding_id="finding_id",
                group_name="group",
                hacker_email="yourfavoritehacker@fluidattacks.com",
                id="vuln123",
                organization_name="your org name",
                root_id="4039d098-ffc5-4984-8ed3-eb17bca98e19",
                state=VulnerabilityState(
                    modified_by="modifier",
                    modified_date=datetime.now(),
                    source=Source.ANALYST,
                    specific="spec",
                    status=VulnerabilityStateStatus.MASKED,
                    where="somewhere",
                ),
                technique=VulnerabilityTechnique.PTAAS,
                type=VulnerabilityType.PORTS,
            ),
            ["tag1", "tag2", "tag3"],
        ],
        [
            Vulnerability(
                created_by="Some vuln creator",
                created_date=datetime.now(),
                finding_id="finding_id",
                group_name="group",
                hacker_email="yourfavoritehacker@fluidattacks.com",
                id="vuln123",
                organization_name="your org name",
                root_id="4039d098-ffc5-4984-8ed3-eb17bca98e19",
                tags=["tag1", "tag2", "tag3"],
                state=VulnerabilityState(
                    modified_by="modifier",
                    modified_date=datetime.now(),
                    source=Source.ANALYST,
                    specific="spec",
                    status=VulnerabilityStateStatus.MASKED,
                    where="somewhere",
                ),
                technique=VulnerabilityTechnique.PTAAS,
                type=VulnerabilityType.PORTS,
            ),
            ["tag4"],
        ],
        [
            Vulnerability(
                created_by="unittest@fluidattacks.com",
                created_date=datetime.fromisoformat(
                    "2019-04-08T00:45:15+00:00"
                ),
                finding_id="988493279",
                group_name="unittesting",
                organization_name="okada",
                hacker_email="unittest@fluidattacks.com",
                id="47ce0fb0-4108-49b0-93cc-160dce8168a6",
                state=VulnerabilityState(
                    commit=None,
                    modified_by="unittest@fluidattacks.com",
                    modified_date=datetime.fromisoformat(
                        "2019-04-08T00:45:15+00:00"
                    ),
                    source=Source.ASM,
                    specific="8888",
                    status=VulnerabilityStateStatus.VULNERABLE,
                    reasons=None,
                    tool=VulnerabilityTool(
                        name="tool-1",
                        impact=VulnerabilityToolImpact.INDIRECT,
                    ),
                    where="192.168.1.19",
                ),
                technique=VulnerabilityTechnique.PTAAS,
                type=VulnerabilityType.PORTS,
                bug_tracking_system_url=None,
                custom_severity=None,
                hash=None,
                root_id="4039d098-ffc5-4984-8ed3-eb17bca98e19",
            ),
            [],
        ],
    ],
)
@patch(MODULE_AT_TEST + "vulns_model.update_metadata", new_callable=AsyncMock)
async def test_add_tags(
    mock: AsyncMock,
    vuln: Vulnerability,
    tags: list[str],
) -> None:
    await add_tags(vulnerability=vuln, tags=tags)
    if not vuln.tags and tags:
        mock.assert_awaited_once()


@pytest.mark.parametrize(
    [
        "vulnerabilities",
    ],
    [
        [
            [
                Vulnerability(
                    created_by="user1",
                    created_date=datetime.now(),
                    finding_id="finding1",
                    group_name="group1",
                    hacker_email="hacker1@example.com",
                    id="vuln1",
                    organization_name="org1",
                    root_id="4039d098-ffc5-4984-8ed3-eb17bca98e19",
                    state=VulnerabilityState(
                        modified_by="modifier",
                        modified_date=datetime.now(),
                        source=Source.ANALYST,
                        specific="spec",
                        status=VulnerabilityStateStatus.VULNERABLE,
                        where="somewhere",
                    ),
                    technique=VulnerabilityTechnique.PTAAS,
                    type=VulnerabilityType.INPUTS,
                ),
                Vulnerability(
                    created_by="user2",
                    created_date=datetime.now(),
                    finding_id="finding2",
                    group_name="group1",
                    hacker_email="hacker2@example.com",
                    id="vuln2",
                    organization_name="org1",
                    root_id="4039d098-ffc5-4984-8ed3-eb17bca98e19",
                    state=VulnerabilityState(
                        modified_by="modifier",
                        modified_date=datetime.now(),
                        source=Source.ANALYST,
                        specific="spec2",
                        status=VulnerabilityStateStatus.VULNERABLE,
                        where="somewhere2",
                    ),
                    technique=VulnerabilityTechnique.PTAAS,
                    type=VulnerabilityType.INPUTS,
                ),
            ],
        ],
    ],
)
async def test_sort_vulns_by_finding_id(
    vulnerabilities: list[Vulnerability],
) -> None:
    result = group_vulnerabilities_by_finding_id(vulnerabilities)
    assert len(result) == 2
    assert "finding1" in result
    assert "finding2" in result
    assert len(result["finding1"]) == 1
    assert len(result["finding2"]) == 1


@pytest.mark.parametrize(
    ["vulnerabilities", "expected_where"],
    [
        [
            [
                Vulnerability(
                    created_by="user1",
                    created_date=datetime.now(),
                    finding_id="finding1",
                    group_name="group1",
                    hacker_email="hacker1@example.com",
                    id="vuln1",
                    organization_name="org1",
                    root_id="4039d098-ffc5-4984-8ed3-eb17bca98e19",
                    state=VulnerabilityState(
                        modified_by="modifier",
                        modified_date=datetime.now(),
                        source=Source.ANALYST,
                        specific="spec",
                        status=VulnerabilityStateStatus.MASKED,
                        where="universe/universe/path/to/file3.ext",
                    ),
                    technique=VulnerabilityTechnique.PTAAS,
                    type=VulnerabilityType.INPUTS,
                ),
            ],
            "- universe/universe/path/to/file3.ext",
        ],
    ],
)
@patch(
    "integrates.finding_comments.domain.get_vulns_wheres",
    new_callable=AsyncMock,
)
async def test_get_content(
    mock_get_vulns_wheres: AsyncMock,
    vulnerabilities: list[Vulnerability],
    expected_where: str,
) -> None:
    loaders = get_new_context()
    mock_get_vulns_wheres.return_value = expected_where
    exclusion_date = datetime.now()

    result = await get_content(loaders, vulnerabilities, exclusion_date)

    expected_content = (
        "Regarding vulnerabilities: \n- universe/universe/path/to/file3.ext\n"
        + f"\nThey were marked as SAFE on {exclusion_date.date()} due to "
        + "EXCLUSION"
    )
    assert result == expected_content
    mock_get_vulns_wheres.assert_called_once_with(loaders, vulnerabilities)


@pytest.mark.parametrize(
    ["vulnerabilities", "finding_id", "user_email"],
    [
        [
            [
                Vulnerability(
                    created_by="user1",
                    created_date=datetime.now(),
                    finding_id="finding1",
                    group_name="group1",
                    hacker_email="hacker1@example.com",
                    id="vuln1",
                    organization_name="org1",
                    root_id="4039d098-ffc5-4984-8ed3-eb17bca98e19",
                    state=VulnerabilityState(
                        modified_by="modifier",
                        modified_date=datetime.now(),
                        source=Source.ANALYST,
                        specific="spec",
                        status=VulnerabilityStateStatus.MASKED,
                        where="universe/universe/path/to/file3.ext",
                    ),
                    technique=VulnerabilityTechnique.PTAAS,
                    type=VulnerabilityType.INPUTS,
                ),
            ],
            "finding1",
            "example@fluidattacks.com",
        ],
    ],
)
@patch(
    MODULE_AT_TEST + "get_content", new_callable=AsyncMock, return_value=None
)
@patch(
    MODULE_AT_TEST + "comments_domain.add",
    new_callable=AsyncMock,
    return_value="somewhere",
)
async def test_add_comment_with_valid_inputs(
    mock_add_comments: AsyncMock,
    mock_get_content: AsyncMock,
    vulnerabilities: list[Vulnerability],
    finding_id: str,
    user_email: str,
) -> None:
    loaders = get_new_context()

    await add_comment_by_exclusion(
        loaders, finding_id, user_email, vulnerabilities
    )

    mock_add_comments.assert_called_once()
    mock_get_content.assert_awaited_once()


@pytest.mark.parametrize(
    ["vulnerabilities", "user_email"],
    [
        [
            [
                Vulnerability(
                    created_by="user1",
                    created_date=datetime.now(),
                    finding_id="finding1",
                    group_name="group1",
                    hacker_email="hacker1@example.com",
                    id="vuln1",
                    organization_name="org1",
                    root_id="4039d098-ffc5-4984-8ed3-eb17bca98e19",
                    state=VulnerabilityState(
                        modified_by="modifier",
                        modified_date=datetime.now(),
                        source=Source.ANALYST,
                        specific="spec",
                        status=VulnerabilityStateStatus.VULNERABLE,
                        where="somewhere",
                    ),
                    technique=VulnerabilityTechnique.PTAAS,
                    type=VulnerabilityType.INPUTS,
                ),
                Vulnerability(
                    created_by="user2",
                    created_date=datetime.now(),
                    finding_id="finding2",
                    group_name="group1",
                    hacker_email="hacker2@example.com",
                    id="vuln2",
                    organization_name="org1",
                    root_id="4039d098-ffc5-4984-8ed3-eb17bca98e19",
                    state=VulnerabilityState(
                        modified_by="modifier",
                        modified_date=datetime.now(),
                        source=Source.ANALYST,
                        specific="spec2",
                        status=VulnerabilityStateStatus.VULNERABLE,
                        where="somewhere2",
                    ),
                    technique=VulnerabilityTechnique.PTAAS,
                    type=VulnerabilityType.INPUTS,
                ),
            ],
            "example@fluidattacks.com",
        ],
    ],
)
@patch(
    MODULE_AT_TEST + "add_comment_by_exclusion",
    new_callable=AsyncMock,
    return_value=None,
)
async def test_correctly_groups_vulnerabilities_by_finding_id(
    mock_add_comment_by_exclusion: AsyncMock,
    vulnerabilities: list[Vulnerability],
    user_email: str,
) -> None:
    loaders = get_new_context()
    await add_comments_by_exclusion(
        loaders=loaders, user_email=user_email, vulns=vulnerabilities
    )

    mock_add_comment_by_exclusion.assert_any_call(
        loaders, "finding1", user_email, [vulnerabilities[0]]
    )
    mock_add_comment_by_exclusion.assert_any_call(
        loaders, "finding2", user_email, [vulnerabilities[1]]
    )
    assert mock_add_comment_by_exclusion.call_count == 2
