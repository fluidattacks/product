from datetime import (
    datetime,
)
from decimal import (
    Decimal,
)
from typing import (
    Any,
)
from unittest.mock import (
    AsyncMock,
    patch,
)

import pytest
from freezegun import (
    freeze_time,
)

from integrates.custom_exceptions import (
    InvalidNotificationRequest,
    InvalidVulnsNumber,
    VulnNotFound,
)
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model.enums import (
    Source,
    StateRemovalJustification,
)
from integrates.db_model.findings.enums import (
    FindingSorts,
    FindingStateStatus,
    FindingStatus,
)
from integrates.db_model.findings.types import (
    Finding,
    FindingEvidence,
    FindingEvidences,
    FindingState,
    FindingUnreliableIndicators,
)
from integrates.db_model.types import (
    SeverityScore,
)
from integrates.db_model.vulnerabilities.enums import (
    VulnerabilityStateStatus,
    VulnerabilityTechnique,
    VulnerabilityType,
)
from integrates.db_model.vulnerabilities.types import (
    Vulnerability,
    VulnerabilityState,
)
from integrates.vulnerabilities.domain import (
    get_managers_by_size,
    send_treatment_report_mail,
)
from integrates.vulnerabilities.domain.treatment import (
    handle_vulnerabilities_acceptance,
    validate_and_send_notification_request,
)
from test.unit.src.utils import (
    get_module_at_test,
    set_mocks_return_values,
)

MODULE_AT_TEST = f"integrates.{get_module_at_test(file_path=__file__)}"

pytestmark = [
    pytest.mark.asyncio,
]

finding_param = Finding(
    group_name="unittesting",
    id="4574973146",
    state=FindingState(
        modified_by="integratesmanager@gmail.com",
        modified_date=datetime.fromisoformat("2018-11-27T05:00:00+00:00"),
        source=Source.ASM,
        status=FindingStateStatus.CREATED,
        rejection=None,
        justification=(StateRemovalJustification.NO_JUSTIFICATION),
    ),
    title="037. Technical information leak",
    attack_vector_description="Test description",
    creation=FindingState(
        modified_by="integratesmanager@gmail.com",
        modified_date=datetime.fromisoformat("2018-04-08T00:43:18+00:00"),
        source=Source.ASM,
        status=FindingStateStatus.CREATED,
        rejection=None,
        justification=(StateRemovalJustification.NO_JUSTIFICATION),
    ),
    description="Descripción de fuga de información técnica",
    evidences=FindingEvidences(
        animation=None,
        evidence1=None,
        evidence2=FindingEvidence(
            description="Test description",
            modified_date=datetime.fromisoformat("2018-11-27T05:00:00+00:00"),
            url="unittesting-4574973146-evidence_route_2.jpg",
        ),
        evidence3=FindingEvidence(
            description="Comentario",
            modified_date=datetime.fromisoformat("2018-11-27T05:00:00+00:00"),
            url="unittesting-4574973146-evidence_route_3.png",
        ),
        evidence4=None,
        evidence5=None,
        exploitation=None,
        records=None,
    ),
    min_time_to_remediate=18,
    recommendation="Eliminar el banner de los servicios con "
    "fuga de información, Verificar que los encabezados HTTP "
    "no expongan ningún nombre o versión.",
    requirements="REQ.0077. La aplicación no debe revelar "
    "detalles del sistema interno como stack traces, "
    "fragmentos de sentencias SQL y nombres de base de datos "
    "o tablas. REQ.0176. El sistema debe restringir el acceso "
    "a objetos del sistema que tengan contenido sensible. "
    "Sólo permitirá su acceso a usuarios autorizados.",
    severity_score=SeverityScore(
        base_score=Decimal("4.6"),
        temporal_score=Decimal("3.9"),
        cvss_v3="CVSS:3.1/AV:A/AC:H/PR:L/UI:N/S:U/C:L/I:L/A:L/"
        "E:P/RL:T/RC:U/MAV:A/MAC:H/MPR:L/MUI:N/MS:U/MC:L/MI:L/"
        "MA:L",
        cvssf=Decimal("0.871"),
        cvss_v4="CVSS:4.0/AV:A/AC:H/AT:N/PR:L/UI:N/VC:L/VI:L/VA:L/SC:N/SI:N/"
        "SA:N/MAV:A/MAC:H/MPR:L/MUI:N/MVC:L/MVI:L/MVA:L/MSC:N/MSI:N/MSA:N/E:P",
        threat_score=Decimal("1.2"),
        cvssf_v4=Decimal("0.021"),
    ),
    sorts=FindingSorts.NO,
    threat="Amenaza.",
    unreliable_indicators=FindingUnreliableIndicators(
        unreliable_newest_vulnerability_report_date=(
            datetime.fromisoformat("2018-11-27T19:54:08+00:00")
        ),
        unreliable_oldest_open_vulnerability_report_date=(
            datetime.fromisoformat("2018-11-27T19:54:08+00:00")
        ),
        unreliable_status=FindingStatus.SAFE,
        unreliable_where="",
    ),
    verification=None,
)


@pytest.mark.parametrize(
    ["group_name", "list_size"],
    [
        ["unittesting", 2],
        ["unittesting", 3],
    ],
)
@patch(MODULE_AT_TEST + "group_access_domain.get_managers", new_callable=AsyncMock)
async def test_get_managers_by_size(
    mock_group_access_domain_get_managers: AsyncMock,
    group_name: str,
    list_size: int,
) -> None:
    mocked_objects, mocked_paths, mocks_args = [
        [mock_group_access_domain_get_managers],
        ["group_access_domain.get_managers"],
        [[group_name, list_size]],
    ]

    assert set_mocks_return_values(
        mocks_args=mocks_args,
        mocked_objects=mocked_objects,
        paths_list=mocked_paths,
        module_at_test=MODULE_AT_TEST,
    )
    email_managers = await get_managers_by_size(get_new_context(), group_name, list_size)
    assert list_size == len(email_managers)
    assert all(mock_object.called is True for mock_object in mocked_objects)


@pytest.mark.parametrize(
    [
        "modified_by",
        "justification",
        "vulnerability_id",
        "is_approved",
    ],
    [
        [
            "vulnmanager@gmail.com",
            "test",
            "15375781-31f2-4953-ac77-f31134225747",
            False,
        ],
    ],
)
@patch(
    MODULE_AT_TEST + "vulns_mailer.send_mail_treatment_report",
    new_callable=AsyncMock,
)
@patch(MODULE_AT_TEST + "get_managers_by_size", new_callable=AsyncMock)
@patch(
    MODULE_AT_TEST + "mailer_utils.get_group_emails_by_notification",
    new_callable=AsyncMock,
)
@patch(MODULE_AT_TEST + "findings_utils.get_finding", new_callable=AsyncMock)
async def test_send_treatment_report_mail(
    mock_get_finding: AsyncMock,
    mock_mailer_utils_get_group_emails_by_notification: AsyncMock,
    mock_get_managers_by_size: AsyncMock,
    mock_vulns_mailer_send_mail_treatment_report: AsyncMock,
    *,
    modified_by: str,
    justification: str,
    vulnerability_id: str,
    is_approved: bool,
) -> None:
    mocked_objects, mocked_paths = [
        [
            mock_get_finding,
            mock_mailer_utils_get_group_emails_by_notification,
            mock_get_managers_by_size,
            mock_vulns_mailer_send_mail_treatment_report,
        ],
        [
            "get_finding",
            "mailer_utils.get_group_emails_by_notification",
            "get_managers_by_size",
            "vulns_mailer.send_mail_treatment_report",
        ],
    ]
    mocks_args: list[list[Any]] = [
        [vulnerability_id],
        [vulnerability_id],
        [vulnerability_id],
        [vulnerability_id, justification, modified_by, [], is_approved],
    ]
    assert set_mocks_return_values(
        mocks_args=mocks_args,
        mocked_objects=mocked_objects,
        module_at_test=MODULE_AT_TEST,
        paths_list=mocked_paths,
    )
    loaders: Dataloaders = get_new_context()

    await send_treatment_report_mail(
        finding_id=vulnerability_id,
        loaders=loaders,
        modified_by=modified_by,
        justification=justification,
        updated_vulns=[],
        is_approved=is_approved,
    )
    assert all(mock_object.called is True for mock_object in mocked_objects)


@pytest.mark.parametrize(
    "params",
    [
        {
            "loaders": get_new_context(),
            "accepted_vulns": [
                "item1",
                "item2",
                "item3",
                "item4",
                "item5",
                "item6",
                "item7",
                "item8",
                "item9",
                "item10",
                "item11",
                "item12",
                "item13",
                "item14",
                "item15",
            ],
            "finding_id": "some_finding_id",
            "justification": "A pretty convincing justification",
            "rejected_vulns": [
                "item16",
                "item17",
                "item18",
                "item19",
                "item20",
                "item21",
                "item22",
                "item23",
                "item24",
                "item25",
                "item26",
                "item27",
                "item28",
                "item29",
                "item30",
                "item31",
                "item32",
                "item33",
            ],
            "user_email": "user@example.org",
        }
    ],
)
@freeze_time("2023-08-29T14:49:11.785Z")
async def test_handle_vulnerabilities_acceptance_raises_invalid_num(
    params: dict,
) -> None:
    with pytest.raises(InvalidVulnsNumber):
        await handle_vulnerabilities_acceptance(
            loaders=params["loaders"],
            accepted_vulns=params["accepted_vulns"],
            finding_id=params["finding_id"],
            justification=params["justification"],
            rejected_vulns=params["rejected_vulns"],
            user_email=params["user_email"],
        )


@pytest.mark.parametrize(
    ["params", "return_value"],
    [
        [
            {
                "loaders": get_new_context(),
                "accepted_vulns": [
                    "item1",
                    "item2",
                    "item3",
                    "item4",
                    "item5",
                ],
                "finding_id": "some_finding_id",
                "justification": "A pretty convincing justification",
                "rejected_vulns": [
                    "item16",
                    "item17",
                    "item18",
                    "item19",
                    "item20",
                ],
                "user_email": "user@example.org",
            },
            [
                Vulnerability(
                    created_by="user1",
                    created_date=datetime.now(),
                    finding_id="FINDING123",
                    group_name="security",
                    hacker_email="hacker@example.com",
                    id="VULN1",
                    organization_name="ACME Corp",
                    root_id="4039d098-ffc5-4984-8ed3-eb17bca98e19",
                    state=VulnerabilityState(
                        modified_by="admin",
                        modified_date=datetime.now(),
                        source=Source.ANALYST,
                        specific="Input validation",
                        status=VulnerabilityStateStatus.VULNERABLE,
                        where="app/src/main.py",
                    ),
                    technique=VulnerabilityTechnique.PTAAS,
                    type=VulnerabilityType.INPUTS,
                ),
                Vulnerability(
                    created_by="user2",
                    created_date=datetime.now(),
                    finding_id="FINDING456",
                    group_name="network",
                    hacker_email="attacker@example.com",
                    id="VULN2",
                    organization_name="Tech Solutions",
                    root_id="4039d098-ffc5-4984-8ed3-eb17bca98e19",
                    state=VulnerabilityState(
                        modified_by="admin",
                        modified_date=datetime.now(),
                        source=Source.ASM,
                        specific="Access control",
                        status=VulnerabilityStateStatus.VULNERABLE,
                        where="app/src/api.py",
                    ),
                    technique=VulnerabilityTechnique.PTAAS,
                    type=VulnerabilityType.PORTS,
                ),
                Vulnerability(
                    created_by="user3",
                    created_date=datetime.now(),
                    finding_id="FINDING789",
                    group_name="web",
                    hacker_email="intruder@example.com",
                    id="VULN3",
                    organization_name="Cyber Defense",
                    root_id="4039d098-ffc5-4984-8ed3-eb17bca98e19",
                    state=VulnerabilityState(
                        modified_by="admin",
                        modified_date=datetime.now(),
                        source=Source.CUSTOMER,
                        specific="Code injection",
                        status=VulnerabilityStateStatus.VULNERABLE,
                        where="app/src/controllers.py",
                    ),
                    technique=VulnerabilityTechnique.PTAAS,
                    type=VulnerabilityType.INPUTS,
                ),
                None,
            ],
        ]
    ],
)
@freeze_time("2023-08-29T14:49:11.785Z")
async def test_handle_vulnerabilities_acceptance_raises_vuln_not_found(
    params: dict, return_value: list[Vulnerability | None]
) -> None:
    dataloaders: Any = get_new_context()
    dataloaders.vulnerability.load_many = AsyncMock(return_value=return_value)
    with pytest.raises(VulnNotFound):
        await handle_vulnerabilities_acceptance(
            loaders=dataloaders,
            accepted_vulns=params["accepted_vulns"],
            finding_id=params["finding_id"],
            justification=params["justification"],
            rejected_vulns=params["rejected_vulns"],
            user_email=params["user_email"],
        )


@pytest.mark.parametrize(
    [
        "responsible",
        "assigned",
        "vulnerabilities",
    ],
    [
        [
            "vulnmanager@gmail.com",
            "integratesuser@gmail.com",
            ["6192c72f-2e10-4259-9207-717b2d90d8d2"],
        ],
        [
            "vulnmanager@gmail.com",
            "unittest@fluidattacks.com",
            ["6401bc87-8633-4a4a-8d8e-7dae0ca57e6a"],
        ],
    ],
)
@patch(
    MODULE_AT_TEST + "vulns_mailer.send_mail_assigned_vulnerability",
    new_callable=AsyncMock,
)
@patch(MODULE_AT_TEST + "send_treatment_change_mail", new_callable=AsyncMock)
@patch(MODULE_AT_TEST + "get_stakeholder", new_callable=AsyncMock)
@patch(
    MODULE_AT_TEST + "Dataloaders.finding_vulnerabilities_all",
    new_callable=AsyncMock,
)
@freeze_time("2019-09-16T10:15:00.00")
async def test_validate_and_send_notification_request(
    mock_loaders_finding_vulnerabilities_all: AsyncMock,
    mock_get_stakeholder: AsyncMock,
    mock_send_treatment_change_mail: AsyncMock,
    mock_vulns_mailer_send_mail_assigned_vulnerability: AsyncMock,
    *,
    responsible: str,
    assigned: str,
    vulnerabilities: list[str],
) -> None:
    finding: Finding = finding_param
    mocked_objects, mocked_paths = [
        [
            mock_loaders_finding_vulnerabilities_all.load,
            mock_get_stakeholder,
            mock_send_treatment_change_mail,
            mock_vulns_mailer_send_mail_assigned_vulnerability,
        ],
        [
            "Dataloaders.finding_vulnerabilities_all",
            "get_stakeholder",
            "send_treatment_change_mail",
            "vulns_mailer.send_mail_assigned_vulnerability",
        ],
    ]
    mocks_args: list[list[Any]] = [
        [finding.id],
        [assigned],
        [],
        [finding.id, responsible],
    ]
    assert set_mocks_return_values(
        mocks_args=mocks_args,
        mocked_objects=mocked_objects,
        module_at_test=MODULE_AT_TEST,
        paths_list=mocked_paths,
    )
    loaders: Dataloaders = get_new_context()

    await validate_and_send_notification_request(
        loaders=loaders,
        finding=finding,
        responsible=responsible,
        vulnerabilities=vulnerabilities,
    )
    assert any(mock_object.called is True for mock_object in mocked_objects)


@pytest.mark.parametrize(
    [
        "responsible",
        "vulnerabilities",
    ],
    [
        # Some of the provided vulns ids don't match existing vulns
        [
            "vulnmanager@gmail.com",
            ["4192c72f-2e10-4259-9207-717b2d90c8d3"],
        ],
        # Some of the provided vulns don't have any assigned hackers
        [
            "vulnmanager@gmail.com",
            ["be09edb7-cd5c-47ed-bee4-97c645acdce13"],
        ],
        # Too much time has passed to notify some of these changes
        [
            "vulnmanager@gmail.com",
            ["80d6a69f-a376-46be-98cd-2fdedcffdcc1"],
        ],
        # Not all the vulns provided have the same assigned hacker
        [
            "vulnmanager@gmail.com",
            [
                "6401bc87-8633-4a4a-8d8e-7dae0ca57e6a",
                "6192c72f-2e10-4259-9207-717b2d90d8d2",
            ],
        ],
    ],
)
@patch(
    MODULE_AT_TEST + "Dataloaders.finding_vulnerabilities_all",
    new_callable=AsyncMock,
)
@freeze_time("2019-09-16T10:15:00.00")
async def test_validate_and_send_notification_request_raises_exception(
    mock_loaders_finding_vulnerabilities_all: AsyncMock,
    responsible: str,
    vulnerabilities: list[str],
) -> None:
    finding: Finding = finding_param
    mocked_objects, mocked_paths, mocks_args = [
        [
            mock_loaders_finding_vulnerabilities_all.load,
        ],
        [
            "Dataloaders.finding_vulnerabilities_all",
        ],
        [
            [finding.id],
        ],
    ]

    assert set_mocks_return_values(
        mocks_args=mocks_args,
        mocked_objects=mocked_objects,
        module_at_test=MODULE_AT_TEST,
        paths_list=mocked_paths,
    )
    loaders: Dataloaders = get_new_context()
    with pytest.raises(InvalidNotificationRequest):
        await validate_and_send_notification_request(
            loaders=loaders,
            finding=finding,
            responsible=responsible,
            vulnerabilities=vulnerabilities,
        )
