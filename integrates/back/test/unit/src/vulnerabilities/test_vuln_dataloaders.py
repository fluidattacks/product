from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model.enums import (
    AcceptanceStatus,
)
from integrates.db_model.findings.types import (
    Finding,
)
from integrates.db_model.vulnerabilities.enums import (
    VulnerabilityStateStatus,
    VulnerabilityZeroRiskStatus,
)
from integrates.db_model.vulnerabilities.types import (
    GroupVulnerabilitiesRequest,
    RootVulnerabilitiesRequest,
)
from itertools import (
    chain,
)
import pytest


@pytest.mark.asyncio
async def test_vulnerability_dataloader_parity() -> None:
    group_name = "unittesting"
    loaders: Dataloaders = get_new_context()
    # Chaining Finding vuln dataloaders
    all_findings: list[Finding] = await loaders.group_findings_all.load(
        group_name
    )
    all_finding_vulnerabilities = tuple(
        chain.from_iterable(
            await loaders.finding_vulnerabilities_all.load_many(
                {finding.id for finding in all_findings}
            )
        )
    )
    non_confirmed_zr_vulnerabilities = [
        vulnerability
        for vulnerability in all_finding_vulnerabilities
        if (
            not vulnerability.zero_risk
            or vulnerability.zero_risk.status
            != VulnerabilityZeroRiskStatus.CONFIRMED
        )
    ]
    group_vulnerabilities_connection = (
        await loaders.group_vulnerabilities.load(
            GroupVulnerabilitiesRequest(
                group_name=group_name,
                state_status=None,
                paginate=False,
            )
        )
    )
    group_vulnerabilities = {
        edge.node for edge in group_vulnerabilities_connection.edges
    }
    assert sorted(non_confirmed_zr_vulnerabilities) == sorted(
        group_vulnerabilities
    )


@pytest.mark.asyncio
async def test_vulnerability_dataloader_parity_with_status() -> None:
    group_name = "unittesting"
    loaders: Dataloaders = get_new_context()
    # Chaining Finding vuln dataloaders
    all_findings: list[Finding] = await loaders.group_findings.load(group_name)
    all_finding_vulnerabilities = tuple(
        chain.from_iterable(
            await loaders.finding_vulnerabilities_all.load_many(
                {finding.id for finding in all_findings}
            )
        )
    )
    filtered_finding_vulns = tuple(
        filter(
            lambda vuln: vuln.state.status
            == VulnerabilityStateStatus.VULNERABLE
            and (
                (
                    vuln.treatment
                    and vuln.treatment.acceptance_status
                    != AcceptanceStatus.APPROVED
                )
                or vuln.treatment is None
            )
            and (
                (
                    vuln.zero_risk
                    and vuln.zero_risk.status
                    != VulnerabilityZeroRiskStatus.CONFIRMED
                )
                or vuln.zero_risk is None
            ),
            all_finding_vulnerabilities,
        )
    )

    # Group vulnerabilities dataloader (Forces)
    group_vulnerabilities_connection = (
        await loaders.group_vulnerabilities.load(
            GroupVulnerabilitiesRequest(
                group_name=group_name,
                is_accepted=False,
                state_status=VulnerabilityStateStatus.VULNERABLE,
                paginate=False,
            )
        )
    )
    vulnerable_locations_without_treatment = tuple(
        edge.node for edge in group_vulnerabilities_connection.edges
    )

    assert sorted(filtered_finding_vulns) == sorted(
        vulnerable_locations_without_treatment
    )


@pytest.mark.asyncio
async def test_root_dataloader_parity() -> None:
    root_id = "4039d098-ffc5-4984-8ed3-eb17bca98e19"
    loaders: Dataloaders = get_new_context()
    root_vulns = await loaders.root_vulnerabilities.load(root_id)
    root_connection_vulns = await loaders.root_vulnerabilities_connection.load(
        RootVulnerabilitiesRequest(root_id, first=100)
    )
    assert {root.id for root in root_vulns} == {
        edge.node.id for edge in root_connection_vulns.edges
    }
