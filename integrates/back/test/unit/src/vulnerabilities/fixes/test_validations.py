from datetime import (
    datetime,
)
from integrates.db_model.enums import (
    Source,
)
from integrates.db_model.findings.enums import (
    FindingStateStatus,
)
from integrates.db_model.findings.types import (
    Finding,
    FindingState,
)
from integrates.db_model.types import (
    SeverityScore,
)
from integrates.vulnerabilities.fixes.validations import (
    is_omitted_finding,
)


def test_is_omitted_finding() -> None:
    finding = Finding(
        group_name="group1",
        id="463558592",
        state=FindingState(
            modified_by="integratesmanager@gmail.com",
            modified_date=datetime.fromisoformat("2024-05-17T05:00:00+00:00"),
            source=Source.ASM,
            status=FindingStateStatus.CREATED,
        ),
        severity_score=SeverityScore(),
        title="007. Cross-site request forgery",
    )
    assert is_omitted_finding(finding) is False
    for title in (
        "009. Sensitive information in source code",
        "999. Sensitive information in source code - Anything",
        "999. Business information leak - Anything",
        "999. Non-encrypted confidential information - Anything",
    ):
        assert is_omitted_finding(finding._replace(title=title)) is True
