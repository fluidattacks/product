from datetime import (
    datetime,
)
from freezegun import (
    freeze_time,
)
from integrates.custom_exceptions import (
    RequestedInvitationTooSoon,
)
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model.group_access.types import (
    GroupAccess,
    GroupAccessMetadataToUpdate,
    GroupAccessRequest,
    GroupAccessState,
    GroupStakeholdersAccessRequest,
)
from integrates.group_access.domain import (
    add_access,
    exists,
    get_group_stakeholders_emails,
    get_stakeholder_role,
    remove_access,
    update,
    validate_new_invitation_time_limit,
)
import pytest
from test.unit.src.utils import (
    get_mocked_path,
    get_module_at_test,
    set_mocks_return_values,
)
from typing import (
    Any,
)
from unittest.mock import (
    AsyncMock,
    patch,
)

pytestmark = [
    pytest.mark.asyncio,
]

MODULE_AT_TEST = f"integrates.{get_module_at_test(file_path=__file__)}"


@pytest.mark.parametrize(
    ["email", "group_name", "role"],
    [
        ["unittest@fluidattacks.com", "unittesting", "user"],
    ],
)
@patch(MODULE_AT_TEST + "authz.grant_group_level_role", new_callable=AsyncMock)
@patch(MODULE_AT_TEST + "update", new_callable=AsyncMock)
async def test_add_access(
    mock_update: AsyncMock,
    mock_authz_grant_group_level_role: AsyncMock,
    email: str,
    group_name: str,
    role: str,
) -> None:
    mocked_objects, mocked_paths, mocks_args = [
        [
            mock_update,
            mock_authz_grant_group_level_role,
        ],
        [
            "update",
            "authz.grant_group_level_role",
        ],
        [
            [email, group_name],
            [email, group_name, role],
        ],
    ]
    assert set_mocks_return_values(
        mocks_args=mocks_args,
        mocked_objects=mocked_objects,
        module_at_test=MODULE_AT_TEST,
        paths_list=mocked_paths,
    )
    loaders: Dataloaders = get_new_context()
    await add_access(
        loaders=loaders,
        email=email,
        group_name=group_name,
        role=role,
        modified_by=email,
    )
    assert all(mock_object.called is True for mock_object in mocked_objects)


@pytest.mark.parametrize(
    ["email", "group_name"],
    [
        ["unittest@fluidattacks.com", "unittesting"],
    ],
)
@patch(MODULE_AT_TEST + "Dataloaders.group_access", new_callable=AsyncMock)
async def test_exists(
    mock_dataloaders_group_access: AsyncMock,
    email: str,
    group_name: str,
) -> None:
    assert set_mocks_return_values(
        mocks_args=[[group_name, email]],
        mocked_objects=[mock_dataloaders_group_access.load],
        module_at_test=MODULE_AT_TEST,
        paths_list=["Dataloaders.group_access"],
    )
    loaders: Dataloaders = get_new_context()
    assert await exists(loaders, group_name, email)
    assert mock_dataloaders_group_access.load.called is True
    mock_dataloaders_group_access.load.assert_called_with(
        GroupAccessRequest(group_name=group_name, email=email)
    )


@pytest.mark.parametrize(
    ["group_name", "expected_result"],
    [
        [
            "unittesting",
            [
                "continuoushack2@gmail.com",
                "continuoushacking@gmail.com",
                "customer_manager@fluidattacks.com",
                "forces.unittesting@fluidattacks.com",
                "integrateshacker@fluidattacks.com",
                "integratesmanager@fluidattacks.com",
                "integratesmanager@gmail.com",
                "integratesreattacker@fluidattacks.com",
                "integratesresourcer@fluidattacks.com",
                "integratesreviewer@fluidattacks.com",
                "integratesserviceforces@fluidattacks.com",
                "integratesuser2@fluidattacks.com",
                "integratesuser2@gmail.com",
                "integratesuser@gmail.com",
                "unittest2@fluidattacks.com",
                "unittest@fluidattacks.com",
                "vulnmanager@gmail.com",
            ],
        ],
    ],
)
@patch(
    MODULE_AT_TEST + "Dataloaders.group_stakeholders_access",
    new_callable=AsyncMock,
)
async def test_get_group_stakeholders_emails(
    mock_dataloaders_group_stakeholders_access: AsyncMock,
    group_name: str,
    expected_result: list,
) -> None:
    assert set_mocks_return_values(
        mocks_args=[[group_name]],
        mocked_objects=[mock_dataloaders_group_stakeholders_access.load],
        module_at_test=MODULE_AT_TEST,
        paths_list=["Dataloaders.group_stakeholders_access"],
    )
    loaders = get_new_context()
    users = await get_group_stakeholders_emails(loaders, group_name)
    for user in expected_result:
        assert user in users
    assert mock_dataloaders_group_stakeholders_access.load.called is True
    mock_dataloaders_group_stakeholders_access.load.assert_called_with(
        GroupStakeholdersAccessRequest(group_name=group_name)
    )


@pytest.mark.parametrize(
    ["email", "group_name"],
    [
        ["unittest@fluidattacks.com", "unittesting"],
    ],
)
@patch(get_mocked_path("group_access_model.remove"), new_callable=AsyncMock)
@patch(
    get_mocked_path("loaders.me_vulnerabilities.load"), new_callable=AsyncMock
)
@patch(
    get_mocked_path("loaders.group_findings.load"),
    new_callable=AsyncMock,
)
async def test_remove_access(
    mock_loaders_group_findings: AsyncMock,
    mock_loaders_me_vulnerabilities: AsyncMock,
    mock_group_access_model_remove: AsyncMock,
    email: str,
    group_name: str,
) -> None:
    mocked_objects, mocked_paths, mocks_args = [
        [
            mock_loaders_group_findings,
            mock_loaders_me_vulnerabilities,
            mock_group_access_model_remove,
        ],
        [
            "loaders.group_findings.load",
            "loaders.me_vulnerabilities.load",
            "group_access_model.remove",
        ],
        [
            [group_name],
            [email],
            [email, group_name],
            [email, group_name],
        ],
    ]
    assert set_mocks_return_values(
        mocked_objects=mocked_objects,
        paths_list=mocked_paths,
        mocks_args=mocks_args,
    )
    loaders: Dataloaders = get_new_context()
    await remove_access(loaders, email, group_name)
    assert all(mock_object.called is True for mock_object in mocked_objects)


@pytest.mark.parametrize(
    ("email", "group_name", "data_to_update"),
    (
        (
            "integratesuser@gmail.com",
            "unittesting",
            GroupAccessMetadataToUpdate(
                state=GroupAccessState(
                    has_access=True,
                    modified_date=datetime.fromisoformat(
                        "2023-02-14T00:43:18+00:00"
                    ),
                    responsibility="Responsible for testing the historic "
                    "facet",
                ),
            ),
        ),
    ),
)
@patch(
    MODULE_AT_TEST + "group_access_model.update_metadata",
    new_callable=AsyncMock,
)
@patch(MODULE_AT_TEST + "Dataloaders.group_access", new_callable=AsyncMock)
async def test_update(
    mock_dataloaders_group_access: AsyncMock,
    mock_group_access_model_update_metadata: AsyncMock,
    email: str,
    group_name: str,
    data_to_update: GroupAccessMetadataToUpdate,
) -> None:
    mocked_objects, mocked_paths = [
        [
            mock_dataloaders_group_access.load,
            mock_group_access_model_update_metadata,
        ],
        [
            "Dataloaders.group_access",
            "group_access_model.update_metadata",
        ],
    ]
    mocks_args: list[list[Any]] = [
        [email, group_name],
        [email, group_name, data_to_update],
        [[email, group_name], [email, group_name, data_to_update]],
    ]
    assert set_mocks_return_values(
        mocks_args=mocks_args,
        mocked_objects=mocked_objects,
        module_at_test=MODULE_AT_TEST,
        paths_list=mocked_paths,
    )
    loaders: Dataloaders = get_new_context()
    await update(
        loaders=loaders,
        email=email,
        group_name=group_name,
        metadata=data_to_update,
    )


@freeze_time("2023-01-23 00:35:00-05:00")
@pytest.mark.parametrize(
    ["inv_expiration_time", "inv_expiration_time_to_raise_exception"],
    [
        [1674452100, 1675056910],
    ],
)
def test_validate_new_invitation_time_limit(
    inv_expiration_time: int, inv_expiration_time_to_raise_exception: int
) -> None:
    assert validate_new_invitation_time_limit(inv_expiration_time)
    with pytest.raises(RequestedInvitationTooSoon) as invalid_too_soon:
        validate_new_invitation_time_limit(
            inv_expiration_time_to_raise_exception
        )
    assert (
        str(invalid_too_soon.value) == "Exception - The previous "
        "invitation to this user was requested less than a minute ago"
    )


@pytest.mark.parametrize(
    ["email", "group_name", "is_registered"],
    [
        [
            "unittest@fluidattacks.com",
            "unittesting",
            True,
        ],
    ],
)
@patch(
    MODULE_AT_TEST + "authz.get_group_level_role",
    new_callable=AsyncMock,
)
@patch(MODULE_AT_TEST + "exists", new_callable=AsyncMock)
async def test_get_stakeholder_role(
    mock_exists: AsyncMock,
    mock_authz_get_group_level_role: AsyncMock,
    email: str,
    group_name: str,
    is_registered: bool,
) -> None:
    mock_exists.return_value = True

    with patch(MODULE_AT_TEST + "get_group_access") as mock_get_group_access:
        mock_get_group_access.return_value = GroupAccess(
            email="unittest@fluidattacks.com",
            group_name="unittesting",
            state=GroupAccessState(
                modified_date=datetime.fromisoformat(
                    "2020-01-01T20:07:57+00:00"
                ),
                confirm_deletion=None,
                has_access=True,
                invitation=None,
                responsibility="Tester",
                role=None,
            ),
            expiration_time=None,
        )

    mock_authz_get_group_level_role.return_value = "admin"
    loaders: Dataloaders = get_new_context()
    role = await get_stakeholder_role(
        loaders, email, group_name, is_registered
    )
    assert role == "admin"


@pytest.mark.parametrize(
    ["email", "group_name"],
    [
        ["unittest@fluidattacks.com", "unittesting"],
    ],
)
async def test_get_access_by_domain(email: str, group_name: str) -> None:
    loaders: Dataloaders = get_new_context()
    group_access = await loaders.group_access.load(
        GroupAccessRequest(email=email, group_name=group_name)
    )

    assert (
        group_access is not None
    ), f"Group access for email {email} and group {group_name} not found."

    domain_access = await loaders.domain_group_access.load(
        email,
    )
    filtered_domain_access = [
        access
        for access in domain_access
        if access.email == group_access.email
        and access.group_name == group_access.group_name
    ]

    assert filtered_domain_access[0] == group_access
