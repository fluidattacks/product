from collections.abc import (
    Callable,
)
from datetime import (
    datetime,
)
from integrates.dataloaders import (
    get_new_context,
)
from integrates.db_model.roots.enums import (
    RootStatus,
)
from integrates.db_model.roots.types import (
    RootState,
)
from integrates.roots.update import (
    get_last_status_update,
    get_last_status_update_date,
)
import pytest
from test.unit.src.utils import (
    get_module_at_test,
)
from unittest.mock import (
    AsyncMock,
    patch,
)

MODULE_AT_TEST = f"integrates.{get_module_at_test(file_path=__file__)}"

pytestmark = [
    pytest.mark.asyncio,
]


@pytest.mark.parametrize(
    ["root_id", "expected_state"],
    [
        [
            "4039d098-ffc5-4984-8ed3-eb17bca98e19",
            RootState(
                modified_by="jdoe@fluidattacks.com",
                modified_date=datetime.fromisoformat(
                    "2023-01-01T12:03:00+00:00"
                ),
                nickname="MyAwesomeRepo",
                status=RootStatus.ACTIVE,
            ),
        ],
        [
            "808e6ccb-a5a3-4e6d-a9cf-99b21cdf0d41",
            RootState(
                modified_by="jdoe@fluidattacks.com",
                modified_date=datetime.fromisoformat(
                    "2023-01-01T12:00:00+00:00"
                ),
                nickname="MyAwesomeRepo",
                status=RootStatus.ACTIVE,
            ),
        ],
    ],
)
@patch(
    MODULE_AT_TEST + "Dataloaders.root_historic_state",
    new_callable=AsyncMock,
)
async def test_get_last_status_update(
    mock_dataloaders_root_historic_state: AsyncMock,
    root_id: str,
    expected_state: RootState,
    mocked_data_for_module: Callable,
) -> None:
    group_name = "unittesting"
    mock_dataloaders_root_historic_state.load.return_value = (
        mocked_data_for_module(
            mock_path="Dataloaders.root_historic_state",
            mock_args=[root_id],
            module_at_test=MODULE_AT_TEST,
        )
    )
    loaders = get_new_context()
    root_state = await get_last_status_update(loaders, root_id, group_name)
    assert root_state == expected_state
    status_date = await get_last_status_update_date(
        loaders, root_id, group_name
    )
    assert status_date == expected_state.modified_date
