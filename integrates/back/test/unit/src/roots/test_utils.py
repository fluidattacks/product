from datetime import (
    datetime,
)
from integrates.custom_exceptions import (
    InvalidParameter,
    InvalidProtocol,
)
from integrates.dataloaders import (
    get_new_context,
)
from integrates.db_model.groups.enums import (
    GroupLanguage,
    GroupManaged,
    GroupService,
    GroupStateStatus,
    GroupSubscriptionType,
    GroupTier,
)
from integrates.db_model.groups.types import (
    Group,
    GroupState,
)
from integrates.db_model.roots.enums import (
    RootCloningStatus,
    RootCriticality,
    RootStatus,
    RootType,
)
from integrates.db_model.roots.types import (
    GitRoot,
    GitRootCloning,
    GitRootState,
    IPRoot,
    IPRootState,
)
from integrates.roots import (
    utils as roots_utils,
)
import pytest
from test.unit.src.utils import (
    get_module_at_test,
)
from typing import (
    Any,
)
from unittest import (
    mock,
)
from unittest.mock import (
    AsyncMock,
    patch,
)

# Constants
pytestmark = [
    pytest.mark.asyncio,
]

MODULE_AT_TEST = f"integrates.{get_module_at_test(file_path=__file__)}"


@pytest.mark.parametrize(
    ["url_input", "expected"],
    [
        (
            "http://dev.azure.com/mycompany/myproject/_git/myproject",
            "http://dev.azure.com/mycompany/myproject/_git/myproject",
        ),
        (
            "https://user@dev.azure.com:30/mycompany/myproject/_git/myproject",
            "https://dev.azure.com:30/mycompany/myproject/_git/myproject",
        ),
        (
            "ssh://git@ssh.dev.azure.com:v3/fluidattacks-universe/demo/demo",
            "ssh://git@ssh.dev.azure.com:v3/fluidattacks-universe/demo/demo",
        ),
        (
            "https://user1@dev.azure.com/my%20company/myproject/_git/"
            "Weird%BF(Format%20Repo)",
            "https://dev.azure.com/my%20company/myproject/_git/"
            "Weird%BF(Format%20Repo)",
        ),
        (
            "codecommit://NewRepo",
            "codecommit://NewRepo",
        ),
    ],
)
def test_format_url(url_input: str, expected: str) -> None:
    result = roots_utils.format_git_repo_url(url_input)
    assert result == expected


@pytest.mark.parametrize(
    ["url_input", "expected_exc"],
    [
        (
            "Non an URL",
            InvalidProtocol("-"),
        ),
    ],
)
def test_format_url_fail(url_input: str, expected_exc: Exception) -> None:
    with pytest.raises(expected_exc.__class__):
        roots_utils.format_git_repo_url(url_input)


@pytest.mark.parametrize(
    ["params"],
    [
        [
            {
                "root": GitRoot(
                    cloning=GitRootCloning(
                        modified_by="admin@gmail.com",
                        modified_date=datetime.fromisoformat(
                            "2020-11-19T13:37:10+00:00"
                        ),
                        reason="root creation",
                        status=RootCloningStatus("UNKNOWN"),
                    ),
                    created_by="admin@gmail.com",
                    created_date=datetime.fromisoformat(
                        "2020-11-19T13:37:10+00:00"
                    ),
                    group_name="group1",
                    id="63298a73-9dff-46cf-b42d-9b2f01a56690",
                    organization_name="orgtest",
                    state=GitRootState(
                        branch="master",
                        criticality=RootCriticality.LOW,
                        gitignore=["bower_components/*", "node_modules/*"],
                        includes_health_check=True,
                        modified_by="modifier@fluidattacks.com",
                        modified_date=datetime.fromisoformat(
                            "2020-11-19T13:37:10+00:00"
                        ),
                        nickname="git_1",
                        other=None,
                        reason=None,
                        status=RootStatus.ACTIVE,
                        url="https://gitlab.com/fluidattacks/universe",
                    ),
                    type=RootType.GIT,
                ),
                "modified_by": "modifier@fluidattacks.com",
                "modified_date": datetime.fromisoformat(
                    "2023-09-14T13:58:38.533Z"
                ),
                "is_failed": False,
            }
        ]
    ],
)
@patch(
    MODULE_AT_TEST + "groups_mail.send_mail_root_cloning_status",
    new_callable=AsyncMock,
)
@patch(
    MODULE_AT_TEST + "mailer_utils.get_group_emails_by_notification",
    new_callable=AsyncMock,
    return_value=[
        "user1@fluidattacsk.com",
        "user2@fluidattacsk.com",
        "user3@fluidattacsk.com",
        "user4@fluidattacsk.com",
        "user5@fluidattacsk.com",
        "user6@fluidattacsk.com",
    ],
)
async def test_send_mail_root_cloning_status(
    mock_get_group_emails_by_notification: AsyncMock,
    mock_groups_mail_send_mail_root_cloning_status: AsyncMock,
    params: dict,
) -> None:
    loaders = get_new_context()
    await roots_utils.send_mail_root_cloning_status(
        loaders=loaders,
        root=params["root"],
        modified_by=params["modified_by"],
        modified_date=params["modified_date"],
        is_failed=params["is_failed"],
    )
    mock_get_group_emails_by_notification.assert_awaited_once_with(
        loaders=loaders,
        group_name=params["root"].group_name,
        notification="root_cloning_status",
    )
    mock_groups_mail_send_mail_root_cloning_status.assert_awaited_once_with(
        loaders=loaders,
        email_to=mock_get_group_emails_by_notification.return_value,
        group_name=params["root"].group_name,
        last_successful_cloning=params["root"].cloning.last_successful_cloning,
        root_creation_date=params["root"].created_date,
        root_nickname=params["root"].state.nickname,
        root_id=params["root"].id,
        report_date=params["modified_date"],
        modified_by=params["modified_by"],
        is_failed=params["is_failed"],
    )


@pytest.mark.parametrize(
    ["params"],
    [
        [
            {
                "group_name": "Some group name",
                "modified_date": datetime.fromisoformat(
                    "2023-09-14T14:20:29.343Z"
                ),
                "root": GitRoot(
                    cloning=GitRootCloning(
                        failed_count=2,
                        modified_by="modifier@fluidattacks.com",
                        modified_date=datetime.fromisoformat(
                            "2023-09-14T14:20:29.343Z"
                        ),
                        reason="root creation",
                        status=RootCloningStatus.FAILED,
                    ),
                    created_by="creator@fluidattacks.com",
                    created_date=datetime.fromisoformat(
                        "2023-09-14T14:20:29.343Z"
                    ),
                    group_name="group1",
                    id="63298a73-9dff-46cf-b42d-9b2f01a56690",
                    organization_name="orgtest",
                    state=GitRootState(
                        branch="master",
                        criticality=RootCriticality.LOW,
                        gitignore=["bower_components/*", "node_modules/*"],
                        includes_health_check=True,
                        modified_by="gitrootstatemod@fluidattacks.com",
                        modified_date=datetime.fromisoformat(
                            "2023-09-14T14:20:29.343Z"
                        ),
                        nickname="",
                        other=None,
                        reason=None,
                        status=RootStatus.ACTIVE,
                        url="https://gitlab.com/fluidattacks/universe",
                    ),
                    type=RootType.GIT,
                ),
                "status": RootCloningStatus.FAILED,
                "group": Group(
                    created_by="unknown",
                    created_date=datetime.fromisoformat(
                        "2020-05-20T22:00:00+00:00"
                    ),
                    description="-",
                    language=GroupLanguage.EN,
                    name="group1",
                    state=GroupState(
                        has_essential=False,
                        has_advanced=True,
                        managed=GroupManaged.MANAGED,
                        modified_by="unknown",
                        modified_date=datetime.fromisoformat(
                            "2020-05-20T22:00:00+00:00"
                        ),
                        service=GroupService.WHITE,
                        status=GroupStateStatus.ACTIVE,
                        tier=GroupTier.OTHER,
                        type=GroupSubscriptionType.CONTINUOUS,
                    ),
                    organization_id="40f6da5f-4f66-4bf0-825b-a2d9748ad6db",
                    sprint_start_date=datetime.fromisoformat(
                        "2022-06-06T00:00:00+00:00"
                    ),
                ),
            }
        ]
    ],
)
@patch(
    MODULE_AT_TEST + "send_mail_root_cloning_status", new_callable=AsyncMock
)
async def test_send_mail_root_cloning_failed(
    mock_send_mail_root_cloning_status: AsyncMock,
    params: dict,
) -> None:
    loaders: Any = get_new_context()
    loaders.group.load = AsyncMock(return_value=params["group"])
    await roots_utils.send_mail_root_cloning_failed(
        loaders=loaders,
        group_name=params["group_name"],
        modified_date=params["modified_date"],
        root=params["root"],
        status=params["status"],
    )
    loaders.group.load.assert_awaited_once_with(params["group_name"])
    is_failed: bool = (
        params["status"] == RootCloningStatus.FAILED
        and params["root"].cloning.failed_count == 2
    )
    mock_send_mail_root_cloning_status.assert_awaited_once_with(
        loaders=loaders,
        root=params["root"],
        modified_by=params["root"].state.modified_by,
        modified_date=params["modified_date"],
        is_failed=is_failed,
    )


@patch(MODULE_AT_TEST + "batch_dal.put_action", new_callable=AsyncMock)
async def test_queue_sync_roots_batch_small(
    mock_dal_put_action: AsyncMock,
) -> None:
    await roots_utils.queue_sync_roots_batch(
        group_name="group1",
        git_root_ids=["root1"],
        modified_by="machine@fluidattacks.com",
        should_queue_machine=False,
        should_queue_sbom=False,
    )
    mock_dal_put_action.assert_awaited_once_with(
        action=mock.ANY,
        attempt_duration_seconds=14400,
        entity="group1",
        subject="machine@fluidattacks.com",
        additional_info={
            "git_root_ids": ["root1"],
            "should_queue_machine": False,
            "should_queue_sbom": False,
        },
        queue=mock.ANY,
    )


@patch(MODULE_AT_TEST + "batch_dal.put_action", new_callable=AsyncMock)
async def test_queue_sync_roots_batch_big(
    mock_dal_put_action: AsyncMock,
) -> None:
    roots = [f"root{i}" for i in range(roots_utils.MAX_ROOTS_PER_JOB + 1)]
    await roots_utils.queue_sync_roots_batch(
        group_name="group1",
        git_root_ids=roots,
        modified_by="machine@fluidattacks.com",
        should_queue_machine=False,
        should_queue_sbom=False,
    )
    assert mock_dal_put_action.call_count == 2
    mock_dal_put_action.assert_called_with(
        action=mock.ANY,
        attempt_duration_seconds=14400,
        entity="group1",
        subject="machine@fluidattacks.com",
        additional_info={
            "git_root_ids": [f"root{roots_utils.MAX_ROOTS_PER_JOB}"],
            "should_queue_machine": False,
            "should_queue_sbom": False,
        },
        queue=mock.ANY,
    )


@pytest.mark.parametrize(
    ["params"],
    [
        [
            {
                "group_name": "Some group name",
                "modified_date": datetime.fromisoformat(
                    "2023-09-14T14:20:29.343Z"
                ),
                "root": IPRoot(
                    created_by="creator@fluidattacks.com",
                    created_date=datetime.fromisoformat(
                        "2023-09-14T20:48:27.788Z"
                    ),
                    group_name="group2",
                    id="83cadbdc-23f3-463a-9421-f50f8d0cb1e5",
                    organization_name="orgtest",
                    state=IPRootState(
                        address="192.168.1.1",
                        modified_by="mod@fluidattacks.com",
                        modified_date=datetime.fromisoformat(
                            "2023-09-14T20:48:27.788Z"
                        ),
                        nickname="",
                        other=None,
                        reason=None,
                        status=RootStatus.ACTIVE,
                    ),
                    type=RootType.IP,
                ),
                "status": RootCloningStatus.FAILED,
                "group": Group(
                    created_by="unknown",
                    created_date=datetime.fromisoformat(
                        "2020-05-20T22:00:00+00:00"
                    ),
                    description="-",
                    language=GroupLanguage.EN,
                    name="group1",
                    state=GroupState(
                        has_essential=False,
                        has_advanced=True,
                        managed=GroupManaged.MANAGED,
                        modified_by="unknown",
                        modified_date=datetime.fromisoformat(
                            "2020-05-20T22:00:00+00:00"
                        ),
                        service=GroupService.WHITE,
                        status=GroupStateStatus.ACTIVE,
                        tier=GroupTier.OTHER,
                        type=GroupSubscriptionType.CONTINUOUS,
                    ),
                    organization_id="40f6da5f-4f66-4bf0-825b-a2d9748ad6db",
                    sprint_start_date=datetime.fromisoformat(
                        "2022-06-06T00:00:00+00:00"
                    ),
                ),
            }
        ]
    ],
)
async def test_send_mail_root_cloning_failed_raises_ex(
    params: dict,
) -> None:
    loaders: Any = get_new_context()
    with pytest.raises(InvalidParameter):
        await roots_utils.send_mail_root_cloning_failed(
            loaders=loaders,
            group_name=params["group_name"],
            modified_date=params["modified_date"],
            root=params["root"],
            status=params["status"],
        )


@pytest.mark.parametrize(
    ["queue_on_batch", "expected_batch", "expected_cluster"],
    [
        [
            True,
            [
                "63298a73-9dff-46cf-b42d-9b2f01a50001",
                "63298a73-9dff-46cf-b42d-9b2f01a50002",
                "63298a73-9dff-46cf-b42d-9b2f01a50003",
            ],
            [
                "63298a73-9dff-46cf-b42d-9b2f01a50004",
            ],
        ],
        [
            False,
            [
                "63298a73-9dff-46cf-b42d-9b2f01a50002",
                "63298a73-9dff-46cf-b42d-9b2f01a50003",
            ],
            [
                "63298a73-9dff-46cf-b42d-9b2f01a50001",
                "63298a73-9dff-46cf-b42d-9b2f01a50004",
            ],
        ],
    ],
)
def test_disgregate_roots_for_queues(
    queue_on_batch: bool,
    expected_batch: list[str],
    expected_cluster: list[str],
) -> None:
    roots = [
        GitRoot(
            cloning=GitRootCloning(
                modified_by="modifier@fluidattacks.com",
                modified_date=datetime.fromisoformat(
                    "2022-06-06T00:00:00+00:00"
                ),
                reason="unknown",
                status=RootCloningStatus.OK,
            ),
            created_by="creator@fluidattacks.com",
            created_date=datetime.fromisoformat("2022-06-06T00:00:00+00:00"),
            group_name="acme",
            id="63298a73-9dff-46cf-b42d-9b2f01a50001",
            organization_name="zambito",
            state=GitRootState(
                branch="master",
                criticality=RootCriticality.LOW,
                includes_health_check=True,
                modified_by="demo@fluidattacks.com",
                modified_date=datetime.fromisoformat(
                    "2022-06-06T00:00:00+00:00"
                ),
                nickname="",
                status=RootStatus.ACTIVE,
                url="https://gitlab.com/fluidattacks/universe",
            ),
            type=RootType.GIT,
        ),
        GitRoot(
            cloning=GitRootCloning(
                modified_by="modifier@fluidattacks.com",
                modified_date=datetime.fromisoformat(
                    "2022-06-06T00:00:00+00:00"
                ),
                reason="unknown",
                status=RootCloningStatus.OK,
            ),
            created_by="creator@fluidattacks.com",
            created_date=datetime.fromisoformat("2022-06-06T00:00:00+00:00"),
            group_name="acme",
            id="63298a73-9dff-46cf-b42d-9b2f01a50002",
            organization_name="zambito",
            state=GitRootState(
                branch="master",
                criticality=RootCriticality.LOW,
                includes_health_check=True,
                modified_by="demo@fluidattacks.com",
                modified_date=datetime.fromisoformat(
                    "2022-06-06T00:00:00+00:00"
                ),
                nickname="",
                status=RootStatus.ACTIVE,
                url="https://gitlab.com/fluidattacks/universe",
                use_egress=True,
            ),
            type=RootType.GIT,
        ),
        GitRoot(
            cloning=GitRootCloning(
                modified_by="modifier@fluidattacks.com",
                modified_date=datetime.fromisoformat(
                    "2022-06-06T00:00:00+00:00"
                ),
                reason="unknown",
                status=RootCloningStatus.OK,
            ),
            created_by="creator@fluidattacks.com",
            created_date=datetime.fromisoformat("2022-06-06T00:00:00+00:00"),
            group_name="acme",
            id="63298a73-9dff-46cf-b42d-9b2f01a50003",
            organization_name="zambito",
            state=GitRootState(
                branch="master",
                criticality=RootCriticality.LOW,
                includes_health_check=True,
                modified_by="demo@fluidattacks.com",
                modified_date=datetime.fromisoformat(
                    "2022-06-06T00:00:00+00:00"
                ),
                nickname="",
                status=RootStatus.ACTIVE,
                url="https://gitlab.com/fluidattacks/universe",
                use_ztna=True,
            ),
            type=RootType.GIT,
        ),
        GitRoot(
            cloning=GitRootCloning(
                modified_by="modifier@fluidattacks.com",
                modified_date=datetime.fromisoformat(
                    "2022-06-06T00:00:00+00:00"
                ),
                reason="unknown",
                status=RootCloningStatus.OK,
            ),
            created_by="creator@fluidattacks.com",
            created_date=datetime.fromisoformat("2022-06-06T00:00:00+00:00"),
            group_name="acme",
            id="63298a73-9dff-46cf-b42d-9b2f01a50004",
            organization_name="zambito",
            state=GitRootState(
                branch="master",
                criticality=RootCriticality.LOW,
                includes_health_check=True,
                modified_by="demo@fluidattacks.com",
                modified_date=datetime.fromisoformat(
                    "2022-06-06T00:00:00+00:00"
                ),
                nickname="",
                status=RootStatus.ACTIVE,
                url="https://gitlab.com/fluidattacks/universe",
                use_vpn=True,
            ),
            type=RootType.GIT,
        ),
    ]
    result_batch, result_cluster = roots_utils.disgregate_roots_for_queues(
        queue_on_batch, roots
    )
    assert sorted(result_batch) == expected_batch
    assert sorted(result_cluster) == expected_cluster
