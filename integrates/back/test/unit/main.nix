{ makeScript, outputs, projectPath, ... }:
makeScript {
  entrypoint = ./entrypoint.sh;
  name = "integrates-back-test-unit";
  replace.__argIntegratesBackEnv__ = outputs."/integrates/back/env";
  searchPaths = {
    bin = [ outputs."/integrates/db" ];
    source = [
      outputs."/integrates/back/env/pypi"
      outputs."/integrates/storage/dev/lib/populate"
    ];
  };
}
