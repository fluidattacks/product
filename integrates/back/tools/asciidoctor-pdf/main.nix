{ makeRubyGemsEnvironment, projectPath, ... }:
makeRubyGemsEnvironment {
  name = "integrates-tools-asciidoctor-pdf";
  ruby = "3.1";
  sourcesYaml =
    projectPath "/integrates/back/tools/asciidoctor-pdf/sources.yaml";
}
