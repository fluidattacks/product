import asyncio
import contextvars

from integrates.audit import dal, utils
from integrates.audit.model import AuditContext, AuditEvent

AUDIT_BATCH = list[AuditEvent]()
AUDIT_CONTEXT = contextvars.ContextVar[AuditContext | None]("audit_context", default=None)
AUDIT_TASKS = set[asyncio.Task[None]]()


def add_audit_context(context: AuditContext) -> AuditContext:
    """
    Adds context for audit events in the current request

    This function does not block the calling code and does not raise any exceptions.

    Note: You can call in multiple places to incrementally add context
    """
    current_context = AUDIT_CONTEXT.get() or AuditContext()
    updated_context = utils.merge_context(current_context, context)
    AUDIT_CONTEXT.set(updated_context)
    return updated_context


def should_exclude_event(event: AuditEvent) -> bool:
    """Excludes high-volume events that the business has deemed unnecessary"""
    if event.object == "Root" and (
        event.mechanism == "SCHEDULER"
        or (
            event.mechanism == "TASK"
            and event.author.startswith("schedule_")
            and event.author.endswith("@fluidattacks.com")
        )
    ):
        return True
    if event.object == "ToeLine" and event.mechanism in {"SCHEDULER", "TASK"}:
        return True
    if event.object == "ToePackage" and event.mechanism == "TASK":
        return True
    return False


def add_audit_event(event: AuditEvent) -> AuditEvent | None:
    """
    Adds an audit event to be processed in the background.

    This function does not block the calling code and does not raise any exceptions.

    Note: Context values take priority over the optional values provided in AuditEvent.
    """
    context = AUDIT_CONTEXT.get() or AuditContext()
    event_with_context = utils.merge_event_context(event, context)

    if should_exclude_event(event_with_context):
        return None

    AUDIT_BATCH.append(event_with_context)

    # There is some async wizardry going on here so let me clarify:
    # create_task only schedules _dispatch to run, it does not run it immediately.
    # While it gets a chance to run, subsequent calls will only append events to the batch
    # and when it finally runs, it will dispatch the all the accumulated events.
    async def _dispatch() -> None:
        events = AUDIT_BATCH[:]
        AUDIT_BATCH.clear()
        await dal.add_audit_batch(events)

    if len(AUDIT_BATCH) == 1:
        task = asyncio.create_task(_dispatch())
        AUDIT_TASKS.add(task)
        task.add_done_callback(AUDIT_TASKS.discard)

    return event_with_context
