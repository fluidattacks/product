import asyncio
import dataclasses

from integrates.audit.core import (
    AUDIT_BATCH,
    AUDIT_TASKS,
    add_audit_context,
    add_audit_event,
    should_exclude_event,
)
from integrates.audit.model import AuditContext, AuditEvent, TMechanism
from integrates.testing.utils import parametrize


async def test_add_audit_event() -> None:
    add_audit_context(AuditContext(author_ip="127.0.0.1", author_user_agent="Mozilla/5.0"))
    context = add_audit_context(AuditContext(author_role="hacker", mechanism="API"))
    assert dataclasses.asdict(context) == {  # type: ignore[misc]
        "author": None,
        "author_ip": "127.0.0.1",
        "author_role": "hacker",
        "author_user_agent": "Mozilla/5.0",
        "mechanism": "API",
    }

    event = add_audit_event(
        AuditEvent(
            action="CREATE",
            author="test@gmail.com",
            metadata={},
            object="Stakeholder",
            object_id="test@gmail.com",
        )
    )
    assert event is not None
    assert dataclasses.asdict(event) == {  # type: ignore[misc]
        "action": "CREATE",
        "author_ip": "127.0.0.1",
        "author_role": "hacker",
        "author_user_agent": "Mozilla/5.0",
        "author": "test@gmail.com",
        "date": event.date,
        "mechanism": "API",
        "metadata": {},
        "object": "Stakeholder",
        "object_id": "test@gmail.com",
    }
    assert AUDIT_BATCH[-1] == event
    assert len(AUDIT_TASKS) == 1
    await asyncio.gather(*AUDIT_TASKS)
    assert len(AUDIT_TASKS) == 0


@parametrize(
    args=["obj", "mechanism", "expected"],
    cases=[
        ["ToeLine", "SCHEDULER", True],
        ["ToeLine", "TASK", True],
        ["ToeLine", "WEB", False],
        ["ToePackage", "TASK", True],
        ["ToePackage", "WEB", False],
    ],
)
def test_should_exclude_event(obj: str, mechanism: TMechanism, expected: bool) -> None:
    result = should_exclude_event(
        AuditEvent(
            action="CREATE",
            author="test@gmail.com",
            mechanism=mechanism,
            metadata={},
            object_id="test_id",
            object=obj,
        )
    )
    assert result == expected
