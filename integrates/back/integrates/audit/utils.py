import dataclasses
import json
from decimal import Decimal
from typing import TYPE_CHECKING, cast

if TYPE_CHECKING:  # pragma: nocover
    from _typeshed import DataclassInstance


from integrates.audit.model import AuditContext, AuditEvent


def _merge_dataclasses(
    current: "DataclassInstance",
    incoming: "DataclassInstance",
) -> "DataclassInstance":
    """Merges non-null values from incoming into current."""
    context_changes = {  # type: ignore[misc]
        key: value  # type: ignore[misc]
        for key, value in dataclasses.asdict(incoming).items()  # type: ignore[misc]
        if value is not None  # type: ignore[misc]
    }
    return dataclasses.replace(current, **context_changes)  # type: ignore[misc]


def merge_context(current: AuditContext, incoming: AuditContext) -> AuditContext:
    """
    Merges the incoming context into the current.

    Merging is safe as they are both AuditContext
    """
    return cast(AuditContext, _merge_dataclasses(current, incoming))


def merge_event_context(event: AuditEvent, context: AuditContext) -> AuditEvent:
    """
    Merges the context values into the event.

    Merging is safe as AuditEvent inherits from AuditContext
    """
    return cast(AuditEvent, _merge_dataclasses(event, context))


def dataclass_to_json(dataclass: "DataclassInstance") -> str:
    def _serialize(obj: object) -> object:
        if isinstance(obj, Decimal):
            return float(obj)
        if isinstance(obj, set):
            return list(obj)  # type: ignore[misc]
        raise TypeError(f"Object of type {obj.__class__.__name__} is not JSON serializable")

    return json.dumps(dataclasses.asdict(dataclass), default=_serialize)  # type: ignore[misc]
