from collections import (
    Counter,
)

from aioextensions import (
    run,
)
from async_lru import (
    alru_cache,
)

from integrates.charts.charts_utils import (
    iterate_groups,
    json_dump,
)
from integrates.charts.generators.bar_chart.utils import (
    format_vulnerabilities_by_data,
)
from integrates.dataloaders import (
    get_new_context,
)
from integrates.db_model.roots.enums import (
    RootStatus,
)
from integrates.db_model.roots.types import (
    GitRoot,
)


@alru_cache(maxsize=None, typed=True)
async def get_data_one_group(group: str) -> Counter[str]:
    loaders = get_new_context()
    roots = {}
    for root in await loaders.group_roots.load(group):
        if isinstance(root, GitRoot) and root.state.status == RootStatus.ACTIVE:
            roots[root.state.nickname] = (
                root.unreliable_indicators.nofluid_quantity
                if root.unreliable_indicators.nofluid_quantity
                else 0
            )
    return Counter(roots)


async def generate_all() -> None:
    async for group in iterate_groups():
        json_document, csv_document = format_vulnerabilities_by_data(
            counters=await get_data_one_group(group=group),
            column="Root",
            axis_rotated=True,
        )
        json_dump(
            document=json_document,
            entity="group",
            subject=group,
            csv_document=csv_document,
        )


def main() -> None:
    run(generate_all())
