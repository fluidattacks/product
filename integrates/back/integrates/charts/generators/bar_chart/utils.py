from collections import (
    Counter,
)
from collections.abc import (
    Awaitable,
    Callable,
    Iterable,
)
from datetime import (
    date as datetype,
)
from decimal import (
    ROUND_CEILING,
    Decimal,
)
from statistics import (
    mean,
)
from typing import (
    NamedTuple,
)

from aioextensions import (
    collect,
)
from async_lru import (
    alru_cache,
)

from integrates.charts.charts_utils import (
    CsvData,
    get_portfolios_groups,
    get_subject_days,
    iterate_groups,
    iterate_organizations_and_groups,
    json_dump,
)
from integrates.charts.generators.common.colors import (
    VULNERABILITIES_COUNT,
)
from integrates.charts.generators.common.utils import (
    BAR_RATIO_WIDTH,
    get_max_axis,
)
from integrates.custom_exceptions import (
    UnsanitizedInputFound,
)
from integrates.custom_utils.datetime import (
    get_now_minus_delta,
)
from integrates.custom_utils.findings import (
    get_group_findings,
)
from integrates.custom_utils.validations import (
    validate_sanitized_csv_input,
)
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model.findings.types import (
    Finding,
)
from integrates.db_model.vulnerabilities.enums import (
    VulnerabilityVerificationStatus,
)
from integrates.db_model.vulnerabilities.types import (
    VulnerabilityVerification,
)
from integrates.findings.domain.utils import (
    get_report_days,
)
from integrates.organizations import (
    domain as orgs_domain,
)

ORGANIZATION_CATEGORIES: list[str] = [
    "My organization",
    "Best organization",
    "Average organization",
    "Worst organization",
]

GROUP_CATEGORIES: list[str] = [
    "My group",
    "Best group",
    "Average group",
    "Worst group",
]

PORTFOLIO_CATEGORIES: list[str] = [
    "My portfolio",
    "Best portfolio",
    "Average portfolio",
    "Worst portfolio",
]

LIMIT: int = 12


class Remediate(NamedTuple):
    critical_severity: Decimal
    high_severity: Decimal
    medium_severity: Decimal
    low_severity: Decimal


class Benchmarking(NamedTuple):
    is_valid: bool
    mttr: Decimal
    subject: str
    number_of_reattacks: int
    number_of_active_roots: int


def get_vulnerability_reattacks(
    *, historic_verification: Iterable[VulnerabilityVerification]
) -> int:
    return sum(
        1
        for verification in historic_verification
        if verification.status == VulnerabilityVerificationStatus.REQUESTED
    )


def get_vulnerability_reattacks_date(
    *,
    historic_verification: Iterable[VulnerabilityVerification],
    min_date: datetype,
) -> int:
    return sum(
        1
        for verification in historic_verification
        if verification.status == VulnerabilityVerificationStatus.REQUESTED
        and verification.modified_date.date() > min_date
    )


def format_mttr_data(
    data: tuple[Decimal, Decimal, Decimal, Decimal],
    categories: list[str],
    y_label: str = "Days",
) -> dict:
    max_value: Decimal = sorted(
        [Decimal("0.0") if data[0] == Decimal("Infinity") else abs(value) for value in data],
        reverse=True,
    )[0]
    max_axis_value: Decimal = (
        get_max_axis(value=max_value) if max_value > Decimal("0.0") else Decimal("0.0")
    )

    return {
        "data": {
            "columns": [
                [
                    "Mean time to remediate",
                    Decimal("0")
                    if data[0] == Decimal("Infinity")
                    else data[0].to_integral_exact(rounding=ROUND_CEILING),
                    data[1],
                    data[2],
                    data[3],
                ]
            ],
            "colors": {"Mean time to remediate": "#cc6699"},
            "labels": True,
            "type": "bar",
        },
        "axis": {
            "x": {
                "categories": categories,
                "type": "category",
            },
            "y": {
                "min": 0,
                "padding": {
                    "bottom": 0,
                    "top": 0,
                },
                "label": {
                    "text": y_label,
                    "position": "inner-top",
                },
                "tick": {"count": 5},
                **({} if max_axis_value == Decimal("0.0") else {"max": max_axis_value}),
            },
        },
        "grid": {
            "x": {"show": False},
            "y": {"show": True},
        },
        "bar": {"width": {"ratio": BAR_RATIO_WIDTH}},
        "tooltip": {"show": False},
        "hideYAxisLine": True,
        "barChartYTickFormat": True,
        "legend": {"show": False},
        "mttrBenchmarking": True,
        "hideXTickLine": True,
    }


def get_valid_subjects(
    *,
    all_subjects: tuple[Benchmarking, ...],
) -> list[Benchmarking]:
    return [
        subject
        for subject in all_subjects
        if subject.is_valid and subject.mttr != Decimal("Infinity")
    ]


def get_mean_organizations(*, organizations: list[Benchmarking]) -> Decimal:
    return (
        Decimal(mean([organization.mttr for organization in organizations])).to_integral_exact(
            rounding=ROUND_CEILING
        )
        if organizations
        else Decimal("0")
    )


def get_best_mttr(subjects: list[Benchmarking]) -> Decimal:
    return (
        Decimal(min(subject.mttr for subject in subjects)).to_integral_exact(rounding=ROUND_CEILING)
        if subjects
        else Decimal("0")
    )


def get_worst_mttr(subjects: list[Benchmarking], oldest_open_age: Decimal) -> Decimal:
    valid_subjects = [subject for subject in subjects if subject.mttr != Decimal("Infinity")]

    return (
        Decimal(max(subject.mttr for subject in valid_subjects)).to_integral_exact(
            rounding=ROUND_CEILING
        )
        if valid_subjects
        else oldest_open_age
    )


def format_value(data: list[tuple[str, int]]) -> Decimal:
    if data:
        return Decimal(data[0][1]) if data[0][1] else Decimal("1.0")
    return Decimal("1.0")


def format_vulnerabilities_by_data(
    *,
    counters: Counter[str],
    column: str,
    axis_rotated: bool = False,
    text: str = "Level",
) -> tuple[dict, CsvData]:
    data = counters.most_common()
    limited_data = data[:LIMIT]
    max_value = format_value(limited_data)
    max_axis_value: Decimal = (
        get_max_axis(value=max_value)
        if max_value > Decimal("0.0") and not axis_rotated
        else Decimal("0.0")
    )

    json_data = {
        "data": {
            "columns": [
                [column, *[value for _, value in limited_data]],
            ],
            "colors": {
                column: VULNERABILITIES_COUNT,
            },
            "labels": None,
            "type": "bar",
        },
        "legend": {"show": False},
        "axis": {
            "rotated": axis_rotated,
            "x": {
                "categories": [key for key, _ in limited_data],
                "type": "category",
                **(
                    {}
                    if axis_rotated
                    else {
                        "label": {
                            "text": text,
                            "position": "outer-top",
                        }
                    }
                ),
                "tick": {
                    "rotate": 0,
                    "multiline": False,
                },
            },
            "y": {
                "min": 0,
                "padding": {"bottom": 0},
            }
            if max_axis_value == Decimal("0.0")
            else {
                "min": 0,
                "max": max_axis_value,
                "tick": {"count": 5},
                "padding": {
                    "bottom": 0,
                    "top": 0,
                },
            },
        },
        "barChartYTickFormat": True,
        "maxValue": max_value,
        **(
            {
                "exposureTrendsByCategories": True,
                "keepToltipColor": True,
                "grid": {"y": {"show": False}},
            }
            if axis_rotated
            else {
                "grid": {"y": {"show": True}},
                "hideYAxisLine": True,
                "hideXTickLine": True,
                "byLevel": True,
            }
        ),
    }
    csv_data = format_data_csv(
        header_value="Occurrences",
        values=[Decimal(value) for _, value in data],
        categories=[group for group, _ in data],
        header_title=column,
    )

    return (json_data, csv_data)


def _get_report_days(finding: Finding) -> int:
    unreliable_indicators = finding.unreliable_indicators
    return get_report_days(unreliable_indicators.unreliable_oldest_open_vulnerability_report_date)


async def _get_oldest_open_age(*, group: str, loaders: Dataloaders) -> Decimal:
    group_findings = await get_group_findings(group_name=group, loaders=loaders)
    findings_open_age = [_get_report_days(finding) for finding in group_findings]

    return (
        Decimal(max(findings_open_age)).to_integral_exact(rounding=ROUND_CEILING)
        if findings_open_age
        else Decimal("0.0")
    )


async def get_oldest_open_age(*, groups: list[str], loaders: Dataloaders) -> Decimal:
    oldest_open_age: tuple[Decimal, ...] = await collect(
        tuple(_get_oldest_open_age(group=group, loaders=loaders) for group in groups),
        workers=24,
    )

    return (
        Decimal(max(oldest_open_age)).to_integral_exact(rounding=ROUND_CEILING)
        if oldest_open_age
        else Decimal("0.0")
    )


@alru_cache(maxsize=None, typed=True)
async def get_data_many_groups_mttr(
    *,
    organization_id: str,
    groups: tuple[str, ...],
    get_data_one_group: Callable[[str, datetype | None], Awaitable[Benchmarking]],
    min_date: datetype | None,
) -> Benchmarking:
    groups_data = await collect(
        tuple(get_data_one_group(group, min_date) for group in groups),
        workers=24,
    )

    mttr = (
        Decimal(mean([group_data.mttr for group_data in groups_data])).to_integral_exact(
            rounding=ROUND_CEILING
        )
        if groups_data
        else Decimal("Infinity")
    )
    number_of_active_roots = sum(group_data.number_of_active_roots for group_data in groups_data)

    number_of_reattacks = sum(group_data.number_of_reattacks for group_data in groups_data)

    return Benchmarking(
        is_valid=number_of_reattacks > 1000 and number_of_active_roots > 0,
        subject=organization_id,
        mttr=mttr,
        number_of_reattacks=number_of_reattacks,
        number_of_active_roots=number_of_active_roots,
    )


async def _get_organizations_and_portfolios() -> (
    tuple[list[tuple[str, tuple[str, ...]]], list[tuple[str, tuple[str, ...]]]]
):
    organizations: list[tuple[str, tuple[str, ...]]] = []
    portfolios: list[tuple[str, tuple[str, ...]]] = []
    async for org_id, _, org_groups in iterate_organizations_and_groups():
        organizations.append((org_id, org_groups))

    async for org_id, org_name, _ in iterate_organizations_and_groups():
        for portfolio, p_groups in await get_portfolios_groups(org_name):
            portfolios.append((f"{org_id}PORTFOLIO#{portfolio}", tuple(p_groups)))
    return organizations, portfolios


async def _dump_groups(
    *,
    get_data_one_group: Callable[[str, datetype | None], Awaitable[Benchmarking]],
    min_date: datetype | None,
    days: int | None,
    groups_mttr_info: dict[str, dict[str, Decimal]],
    all_groups_data: dict[str, tuple[Benchmarking, ...]],
    alternative: str,
    y_label: str,
    header: str,
) -> None:
    async for group in iterate_groups():
        document = format_mttr_data(
            data=(
                (await get_data_one_group(group, min_date)).mttr,
                groups_mttr_info["best_group"]["best_mttr" + get_subject_days(days)],
                get_mean_organizations(
                    organizations=get_valid_subjects(
                        all_subjects=all_groups_data["all_groups_data" + get_subject_days(days)],
                    )
                ),
                groups_mttr_info["worst_group"]["worst_mttr" + get_subject_days(days)],
            ),
            categories=GROUP_CATEGORIES,
            y_label=y_label,
        )
        json_dump(
            document=document,
            entity="group",
            subject=group + get_subject_days(days),
            csv_document=format_csv_data(document=document, header=header, alternative=alternative),
        )


async def _dump_organizations(
    *,
    get_data_one_group: Callable[[str, datetype | None], Awaitable[Benchmarking]],
    min_date: datetype | None,
    days: int | None,
    organizations_mttr_info: dict[str, dict[str, Decimal]],
    all_organizations_data: dict[str, tuple[Benchmarking, ...]],
    alternative: str,
    y_label: str,
    header: str,
) -> None:
    async for org_id, _, org_groups in iterate_organizations_and_groups():
        document = format_mttr_data(
            data=(
                (
                    await get_data_many_groups_mttr(
                        organization_id=org_id,
                        groups=org_groups,
                        get_data_one_group=get_data_one_group,
                        min_date=min_date,
                    )
                ).mttr,
                organizations_mttr_info["best_organization"]["best_mttr" + get_subject_days(days)],
                get_mean_organizations(
                    organizations=get_valid_subjects(
                        all_subjects=all_organizations_data[
                            "all_organizations_data" + get_subject_days(days)
                        ],
                    )
                ),
                organizations_mttr_info["worst_organization"][
                    "worst_mttr" + get_subject_days(days)
                ],
            ),
            categories=ORGANIZATION_CATEGORIES,
            y_label=y_label,
        )
        json_dump(
            document=document,
            entity="organization",
            subject=org_id + get_subject_days(days),
            csv_document=format_csv_data(document=document, header=header, alternative=alternative),
        )


async def _dump_portfolios(
    *,
    get_data_one_group: Callable[[str, datetype | None], Awaitable[Benchmarking]],
    min_date: datetype | None,
    days: int | None,
    portfolios_mttr_info: dict[str, dict[str, Decimal]],
    all_portfolios_data: dict[str, tuple[Benchmarking, ...]],
    alternative: str,
    y_label: str,
    header: str,
) -> None:
    async for org_id, org_name, _ in iterate_organizations_and_groups():
        for portfolio, pgroup_names in await get_portfolios_groups(org_name):
            document = format_mttr_data(
                data=(
                    (
                        await get_data_many_groups_mttr(
                            organization_id=(f"{org_id}PORTFOLIO#{portfolio}"),
                            groups=pgroup_names,
                            get_data_one_group=get_data_one_group,
                            min_date=min_date,
                        )
                    ).mttr,
                    portfolios_mttr_info["best_portfolio"]["best_mttr" + get_subject_days(days)],
                    get_mean_organizations(
                        organizations=get_valid_subjects(
                            all_subjects=all_portfolios_data[
                                "all_portfolios_data" + get_subject_days(days)
                            ],
                        )
                    ),
                    portfolios_mttr_info["worst_portfolio"]["worst_mttr" + get_subject_days(days)],
                ),
                categories=PORTFOLIO_CATEGORIES,
                y_label=y_label,
            )
            json_dump(
                document=document,
                entity="portfolio",
                subject=f"{org_id}PORTFOLIO#{portfolio}" + get_subject_days(days),
                csv_document=format_csv_data(
                    document=document,
                    header=header,
                    alternative=alternative,
                ),
            )


async def _dump_groups_organizations_and_portfolios(
    *,
    list_days: list[int],
    dates: list[datetype],
    get_data_one_group: Callable[[str, datetype | None], Awaitable[Benchmarking]],
    groups_mttr_info: dict[str, dict[str, Decimal]],
    all_groups_data: dict[str, tuple[Benchmarking, ...]],
    y_label: str,
    alternative: str,
    organizations_mttr_info: dict[str, dict[str, Decimal]],
    all_organizations_data: dict[str, tuple[Benchmarking, ...]],
    portfolios_mttr_info: dict[str, dict[str, Decimal]],
    all_portfolios_data: dict[str, tuple[Benchmarking, ...]],
) -> None:
    header: str = "Categories"

    for days, min_date in zip([None, *list_days], [None, *dates], strict=False):
        await _dump_groups(
            get_data_one_group=get_data_one_group,
            min_date=min_date,
            days=days,
            groups_mttr_info=groups_mttr_info,
            all_groups_data=all_groups_data,
            alternative=alternative,
            y_label=y_label,
            header=header,
        )

        await _dump_organizations(
            get_data_one_group=get_data_one_group,
            min_date=min_date,
            days=days,
            organizations_mttr_info=organizations_mttr_info,
            all_organizations_data=all_organizations_data,
            alternative=alternative,
            y_label=y_label,
            header=header,
        )

        await _dump_portfolios(
            get_data_one_group=get_data_one_group,
            min_date=min_date,
            days=days,
            portfolios_mttr_info=portfolios_mttr_info,
            all_portfolios_data=all_portfolios_data,
            alternative=alternative,
            y_label=y_label,
            header=header,
        )


async def _get_data_category_mttr_by_date(
    *,
    category: list[tuple[str, tuple[str, ...]]],
    get_data_one_group: Callable[[str, datetype | None], Awaitable[Benchmarking]],
    min_date: datetype | None,
) -> tuple[Benchmarking, ...]:
    return await collect(
        [
            get_data_many_groups_mttr(
                organization_id=element[0],
                groups=element[1],
                get_data_one_group=get_data_one_group,
                min_date=min_date,
            )
            for element in category
        ],
        workers=24,
    )


async def _get_category_mttr_data(
    *,
    get_data_one_group: Callable[[str, datetype | None], Awaitable[Benchmarking]],
    category: list[tuple[str, tuple[str, ...]]],
    category_name: str,
    dates: list[datetype],
) -> dict[str, tuple[Benchmarking, ...]]:
    all_data = await _get_data_category_mttr_by_date(
        category=category, get_data_one_group=get_data_one_group, min_date=None
    )
    all_data_30 = await _get_data_category_mttr_by_date(
        category=category,
        get_data_one_group=get_data_one_group,
        min_date=dates[0],
    )
    all_data_90 = await _get_data_category_mttr_by_date(
        category=category,
        get_data_one_group=get_data_one_group,
        min_date=dates[1],
    )
    return {
        f"all_{category_name}s_data": all_data,
        f"all_{category_name}s_data_30": all_data_30,
        f"all_{category_name}s_data_90": all_data_90,
    }


async def _get_all_categories_data(
    *,
    get_data_one_group: Callable[[str, datetype | None], Awaitable[Benchmarking]],
    group_names: list[str],
    dates: list[datetype],
) -> tuple[
    dict[str, tuple[Benchmarking, ...]],
    dict[str, tuple[Benchmarking, ...]],
    dict[str, tuple[Benchmarking, ...]],
]:
    organizations, portfolios = await _get_organizations_and_portfolios()
    _all_groups_data = await collect(
        [get_data_one_group(group_name, None) for group_name in group_names],
        workers=16,
    )

    all_groups_data_30: tuple[Benchmarking, ...] = await collect(
        [get_data_one_group(group_name, dates[0]) for group_name in group_names],
        workers=16,
    )

    all_groups_data_90: tuple[Benchmarking, ...] = await collect(
        [get_data_one_group(group_name, dates[1]) for group_name in group_names],
        workers=16,
    )

    all_groups_data: dict[str, tuple[Benchmarking, ...]] = {
        "all_groups_data": _all_groups_data,
        "all_groups_data_30": all_groups_data_30,
        "all_groups_data_90": all_groups_data_90,
    }
    all_organizations_data = await _get_category_mttr_data(
        get_data_one_group=get_data_one_group,
        category=organizations,
        category_name="organization",
        dates=dates,
    )
    all_portfolios_data = await _get_category_mttr_data(
        get_data_one_group=get_data_one_group,
        category=portfolios,
        category_name="portfolio",
        dates=dates,
    )
    return all_groups_data, all_organizations_data, all_portfolios_data


def _filter_valid_organizations(
    category_data: dict[str, tuple[Benchmarking, ...]], key: str
) -> list[Benchmarking]:
    return [organization for organization in category_data[key] if organization.is_valid]


def _get_mttr_info(
    mttr_function: Callable[..., Decimal],
    organizations: list[Benchmarking],
    oldest_open_age: Decimal | None = None,
) -> Decimal:
    if oldest_open_age is not None:
        return mttr_function(organizations, oldest_open_age)
    return mttr_function(organizations)


def _get_category_mttr_info(
    *,
    category_data: dict[str, tuple[Benchmarking, ...]],
    category_name: str,
    oldest_open_age: Decimal,
) -> dict[str, dict[str, Decimal]]:
    category_mttr_info: dict[str, dict[str, Decimal]] = {}
    time_frames = [
        ("", oldest_open_age),
        ("_30", Decimal("30.0")),
        ("_90", Decimal("90.0")),
    ]
    best_key = f"best_{category_name}"
    worst_key = f"worst_{category_name}"
    category_mttr_info[best_key] = {}
    category_mttr_info[worst_key] = {}
    for suffix, age in time_frames:
        valid_organizations = _filter_valid_organizations(
            category_data, f"all_{category_name}s_data{suffix}"
        )
        category_mttr_info[best_key][f"best_mttr{suffix}"] = _get_mttr_info(
            get_best_mttr, valid_organizations
        )
        category_mttr_info[worst_key][f"worst_mttr{suffix}"] = _get_mttr_info(
            get_worst_mttr, valid_organizations, age
        )

    return category_mttr_info


async def generate_all_mttr_benchmarking(
    *,
    get_data_one_group: Callable[[str, datetype | None], Awaitable[Benchmarking]],
    alternative: str,
    y_label: str = "Days",
) -> None:
    loaders: Dataloaders = get_new_context()
    list_days: list[int] = [30, 90]
    dates: list[datetype] = [
        get_now_minus_delta(days=list_days[0]).date(),
        get_now_minus_delta(days=list_days[1]).date(),
    ]
    group_names: list[str] = sorted(
        await orgs_domain.get_all_active_group_names(loaders),
        reverse=True,
    )
    oldest_open_age = await get_oldest_open_age(groups=group_names, loaders=loaders)
    (
        all_groups_data,
        all_organizations_data,
        all_portfolios_data,
    ) = await _get_all_categories_data(
        get_data_one_group=get_data_one_group,
        group_names=group_names,
        dates=dates,
    )
    groups_mttr_info = _get_category_mttr_info(
        category_data=all_groups_data,
        category_name="group",
        oldest_open_age=oldest_open_age,
    )
    organizations_mttr_info = _get_category_mttr_info(
        category_data=all_organizations_data,
        category_name="organization",
        oldest_open_age=oldest_open_age,
    )
    portfolios_mttr_info = _get_category_mttr_info(
        category_data=all_portfolios_data,
        category_name="portfolio",
        oldest_open_age=oldest_open_age,
    )

    await _dump_groups_organizations_and_portfolios(
        list_days=list_days,
        dates=dates,
        get_data_one_group=get_data_one_group,
        groups_mttr_info=groups_mttr_info,
        all_groups_data=all_groups_data,
        y_label=y_label,
        alternative=alternative,
        organizations_mttr_info=organizations_mttr_info,
        all_organizations_data=all_organizations_data,
        portfolios_mttr_info=portfolios_mttr_info,
        all_portfolios_data=all_portfolios_data,
    )


def sum_mttr_many_groups(*, groups_data: tuple[Remediate, ...]) -> Remediate:
    return Remediate(
        critical_severity=Decimal(mean([group.critical_severity for group in groups_data]))
        .quantize(Decimal("0.1"))
        .to_integral_exact(rounding=ROUND_CEILING)
        if groups_data
        else Decimal("0"),
        high_severity=Decimal(mean([group.high_severity for group in groups_data]))
        .quantize(Decimal("0.1"))
        .to_integral_exact(rounding=ROUND_CEILING)
        if groups_data
        else Decimal("0"),
        medium_severity=Decimal(mean([group.medium_severity for group in groups_data]))
        .quantize(Decimal("0.1"))
        .to_integral_exact(rounding=ROUND_CEILING)
        if groups_data
        else Decimal("0"),
        low_severity=Decimal(mean([group.low_severity for group in groups_data]))
        .quantize(Decimal("0.1"))
        .to_integral_exact(rounding=ROUND_CEILING)
        if groups_data
        else Decimal("0"),
    )


async def generate_all_top_vulnerabilities(
    *,
    get_data_one_group: Callable[[str], Awaitable[Counter[str]]],
    get_data_many_groups: Callable[[list[str]], Awaitable[Counter[str]]],
    format_data: Callable[[Counter[str], bool], tuple[dict, CsvData]],
) -> None:
    async for group in iterate_groups():
        json_document, csv_document = format_data(
            await get_data_one_group(group),
            True,
        )
        json_dump(
            document=json_document,
            entity="group",
            subject=group,
            csv_document=csv_document,
        )

    async for org_id, _, org_groups in iterate_organizations_and_groups():
        json_document, csv_document = format_data(
            await get_data_many_groups(list(org_groups)),
            False,
        )
        json_dump(
            document=json_document,
            entity="organization",
            subject=org_id,
            csv_document=csv_document,
        )

    async for org_id, org_name, org_groups in iterate_organizations_and_groups():
        for portfolio, groups in await get_portfolios_groups(org_name, org_groups):
            json_document, csv_document = format_data(
                await get_data_many_groups(list(groups)),
                False,
            )
            json_dump(
                document=json_document,
                entity="portfolio",
                subject=f"{org_id}PORTFOLIO#{portfolio}",
                csv_document=csv_document,
            )


def format_csv_data(
    *, document: dict, header: str = "Group name", alternative: str = ""
) -> CsvData:
    columns: list[list[str]] = document["data"]["columns"]
    categories: list[str] = document["axis"]["x"]["categories"]
    rows: list[list[str]] = []
    for category, value in zip(categories, tuple(columns[0][1:]), strict=False):
        try:
            validate_sanitized_csv_input(str(category).rsplit(" - ", 1)[0])
            rows.append([str(category).rsplit(" - ", 1)[0], str(value)])
        except UnsanitizedInputFound:
            rows.append(["", ""])

    return CsvData(
        headers=[header, alternative if alternative else columns[0][0]],
        rows=rows,
    )


def format_data_csv(
    *,
    header_value: str,
    values: list[Decimal],
    categories: list[str],
    header_title: str = "Group name",
) -> CsvData:
    rows: list[list[str]] = []
    for category, value in zip(categories, values, strict=False):
        try:
            validate_sanitized_csv_input(str(category).rsplit(" - ", 1)[0])
            rows.append([str(category).rsplit(" - ", 1)[0], str(value)])
        except UnsanitizedInputFound:
            rows.append(["", ""])

    return CsvData(
        headers=[header_title, header_value],
        rows=rows,
    )
