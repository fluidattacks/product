from datetime import (
    date as datetype,
)
from decimal import (
    Decimal,
)

from aioextensions import (
    collect,
    run,
)
from async_lru import (
    alru_cache,
)

from integrates.charts.generators.bar_chart.mttr_benchmarking_cvssf import (
    get_historic_verification,
)
from integrates.charts.generators.bar_chart.utils import (
    Benchmarking,
    generate_all_mttr_benchmarking,
    get_vulnerability_reattacks_date,
)
from integrates.custom_utils.vulnerabilities import (
    is_accepted_undefined_vulnerability,
)
from integrates.dataloaders import (
    get_new_context,
)
from integrates.db_model.roots.enums import (
    RootStatus,
)
from integrates.groups import (
    domain as groups_domain,
)
from integrates.groups.domain import (
    get_mean_remediate_non_treated_severity_cvssf,
)


@alru_cache(maxsize=None, typed=True)
async def get_data_one_group(group: str, min_date: datetype | None) -> Benchmarking:
    loaders = get_new_context()
    group_vulns = await groups_domain.get_group_vulns_with_severity(loaders, group)
    vuln_ids_non_permanently_accepted = {
        vulnerability.id
        for vulnerability in group_vulns
        if not is_accepted_undefined_vulnerability(vulnerability)
    }

    active_roots = [
        root
        for root in await loaders.group_roots.load(group)
        if root.state.status is RootStatus.ACTIVE
    ]
    if min_date:
        historics_verification = await collect(
            [
                get_historic_verification(vulnerability)
                for vulnerability in group_vulns
                if vulnerability.id in vuln_ids_non_permanently_accepted
                and vulnerability.verification
            ],
            workers=4,
        )
        number_of_reattacks = sum(
            get_vulnerability_reattacks_date(historic_verification=historic, min_date=min_date)
            for historic in historics_verification
        )
    else:
        number_of_reattacks = sum(
            vulnerability.unreliable_indicators.unreliable_reattack_cycles or 0
            for vulnerability in group_vulns
            if vulnerability.id in vuln_ids_non_permanently_accepted
        )

    mttr = await get_mean_remediate_non_treated_severity_cvssf(
        loaders,
        group.lower(),
        Decimal("0.0"),
        Decimal("10.0"),
        min_date=min_date,
    )

    return Benchmarking(
        is_valid=(number_of_reattacks > 10 and len(active_roots) > 0),
        subject=group.lower(),
        number_of_active_roots=len(active_roots),
        mttr=mttr,
        number_of_reattacks=number_of_reattacks,
    )


def main() -> None:
    run(
        generate_all_mttr_benchmarking(
            get_data_one_group=get_data_one_group,
            alternative=("Mean time to remediate non treated per exposure benchmarking"),
        )
    )
