from decimal import (
    Decimal,
)
from operator import (
    attrgetter,
)
from typing import (
    NamedTuple,
)

from aioextensions import (
    collect,
    run,
)
from async_lru import (
    alru_cache,
)

from integrates.charts.charts_utils import (
    CsvData,
    get_portfolios_groups,
    iterate_groups,
    iterate_organizations_and_groups,
    json_dump,
)
from integrates.charts.generators.bar_chart.utils import (
    LIMIT,
    format_data_csv,
)
from integrates.charts.generators.bar_chart.utils_top_vulnerabilities_by_source import (
    format_max_value,
)
from integrates.charts.generators.common.colors import (
    OTHER_COUNT,
)
from integrates.custom_utils.datetime import (
    get_utc_now,
)
from integrates.dataloaders import (
    get_new_context,
)
from integrates.db_model.events.enums import (
    EventStateStatus,
)
from integrates.db_model.events.types import (
    GroupEventsRequest,
)


class EventsInfo(NamedTuple):
    name: str
    days: int


@alru_cache(maxsize=None, typed=True)
async def get_data_one_group(*, group: str) -> tuple[EventsInfo, ...]:
    loaders = get_new_context()
    events_group = await loaders.group_events.load(GroupEventsRequest(group_name=group))

    return tuple(
        sorted(
            [
                EventsInfo(
                    days=(get_utc_now().date() - event.event_date.date()).days,
                    name=event.id,
                )
                for event in events_group
                if event.state.status != EventStateStatus.SOLVED
            ],
            key=attrgetter("days"),
            reverse=True,
        )
    )


async def get_data_many_groups(*, groups: tuple[str, ...]) -> tuple[EventsInfo, ...]:
    groups_data: tuple[tuple[EventsInfo, ...], ...] = await collect(
        tuple(get_data_one_group(group=group) for group in groups),
        workers=32,
    )
    groups_events: tuple[EventsInfo, ...] = tuple(
        EventsInfo(
            days=group[0].days if group else 0,
            name=name,
        )
        for group, name in zip(groups_data, groups, strict=False)
    )

    return tuple(sorted(groups_events, key=attrgetter("days"), reverse=True))


def format_data(
    *, data: tuple[EventsInfo, ...], legend: str, x_label: str | None = None
) -> tuple[dict, CsvData]:
    limited_data = [group for group in data if group.days > 0][:LIMIT]

    json_data: dict = {
        "data": {
            "columns": [
                [legend] + [str(group.days) for group in limited_data],
            ],
            "colors": {legend: OTHER_COUNT},
            "labels": None,
            "type": "bar",
        },
        "legend": {"show": False},
        "axis": {
            "rotated": True,
            "x": {
                "categories": [group.name.capitalize() for group in limited_data],
                **({"label": {"text": x_label, "position": "outer-top"}} if x_label else {}),
                "tick": {
                    "multiline": False,
                    "outer": False,
                    "rotate": 0,
                },
                "type": "category",
            },
            "y": {
                "label": {
                    "position": "outer-top",
                    "text": "Days open",
                },
                "min": 0,
                "padding": {"bottom": 0},
            },
        },
        "exposureTrendsByCategories": True,
        "keepToltipColor": True,
        "barChartYTickFormat": True,
        "maxValue": format_max_value([(ldata.name, Decimal(ldata.days)) for ldata in limited_data]),
    }

    csv_data = format_data_csv(
        header_value=str(json_data["data"]["columns"][0][0]),
        values=[Decimal(group.days) for group in data],
        categories=[group.name for group in data],
        header_title=x_label if x_label else "Group name",
    )

    return (json_data, csv_data)


async def generate_all() -> None:
    legend_many_groups: str = "Days since the group is failing"

    async for group in iterate_groups():
        json_document, csv_document = format_data(
            data=await get_data_one_group(group=group),
            legend="Days since the event was reported",
            x_label="Event ID",
        )
        json_dump(
            document=json_document,
            entity="group",
            subject=group,
            csv_document=csv_document,
        )

    async for org_id, _, org_groups in iterate_organizations_and_groups():
        json_document, csv_document = format_data(
            data=await get_data_many_groups(groups=org_groups),
            legend=legend_many_groups,
        )
        json_dump(
            document=json_document,
            entity="organization",
            subject=org_id,
            csv_document=csv_document,
        )

    async for org_id, org_name, _ in iterate_organizations_and_groups():
        for portfolio, groups in await get_portfolios_groups(org_name):
            json_document, csv_document = format_data(
                data=await get_data_many_groups(groups=tuple(groups)),
                legend=legend_many_groups,
            )
            json_dump(
                document=json_document,
                entity="portfolio",
                subject=f"{org_id}PORTFOLIO#{portfolio}",
                csv_document=csv_document,
            )


def main() -> None:
    run(generate_all())
