from collections import (
    Counter,
)
from typing import (
    NamedTuple,
)

from aioextensions import (
    collect,
    run,
)
from async_lru import (
    alru_cache,
)

from integrates.charts.generators.common.colors import (
    TREATMENT_COLORS,
)
from integrates.charts.generators.pie_chart.utils import (
    generate_all,
)
from integrates.dataloaders import (
    get_new_context,
)
from integrates.db_model.enums import (
    TreatmentStatus,
)
from integrates.db_model.vulnerabilities.enums import (
    VulnerabilityStateStatus,
)
from integrates.groups import (
    domain as groups_domain,
)


class Treatment(NamedTuple):
    acceptedUndefined: int  # noqa: N815
    accepted: int
    inProgress: int  # noqa: N815
    undefined: int


@alru_cache(maxsize=None, typed=True)
async def get_data_one_group(group: str) -> Treatment:
    loaders = get_new_context()
    group_vulns = await groups_domain.get_group_vulns_with_severity(loaders, group)
    treatments: tuple[Counter[TreatmentStatus], ...] = tuple(
        Counter({vuln.treatment.status: vuln.severity_score.cvssf_v4})
        for vuln in group_vulns
        if vuln.treatment
        and vuln.state.status == VulnerabilityStateStatus.VULNERABLE
        and vuln.severity_score
    )
    treatment: Counter[TreatmentStatus] = sum(treatments, Counter())

    return Treatment(
        acceptedUndefined=treatment[TreatmentStatus.ACCEPTED_UNDEFINED],
        accepted=treatment[TreatmentStatus.ACCEPTED],
        inProgress=treatment[TreatmentStatus.IN_PROGRESS],
        undefined=treatment[TreatmentStatus.UNTREATED],
    )


async def get_data_many_groups(groups: tuple[str, ...]) -> Treatment:
    groups_data: tuple[Treatment, ...] = await collect(map(get_data_one_group, groups), workers=32)

    return Treatment(
        acceptedUndefined=sum(group.acceptedUndefined for group in groups_data),
        accepted=sum(group.accepted for group in groups_data),
        inProgress=sum(group.inProgress for group in groups_data),
        undefined=sum(group.undefined for group in groups_data),
    )


def format_data(data: Treatment) -> dict:
    translations: dict[str, str] = {
        "acceptedUndefined": "Permanently accepted",
        "accepted": "Temporarily accepted",
        "inProgress": "In progress",
        "undefined": "Untreated",
    }

    return {
        "data": {
            "columns": [
                [value.capitalize(), str(getattr(data, key))] for key, value in translations.items()
            ],
            "type": "pie",
            "colors": {
                "Permanently accepted": TREATMENT_COLORS.permanently_accepted,
                "Temporarily accepted": TREATMENT_COLORS.temporarily_accepted,
                "In progress": TREATMENT_COLORS.in_progress,
                "Untreated": TREATMENT_COLORS.untreated,
            },
        },
        "legend": {
            "position": "right",
        },
        "pie": {
            "label": {
                "show": True,
            },
        },
    }


def main() -> None:
    run(
        generate_all(
            get_data_one_group=get_data_one_group,
            get_data_many_groups=get_data_many_groups,
            format_document=format_data,
            header=["Treatment", "CVSSF"],
        )
    )
