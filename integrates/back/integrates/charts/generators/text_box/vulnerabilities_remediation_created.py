from aioextensions import (
    run,
)

from integrates.charts.generators.text_box.utils_vulnerabilities_remediation import (
    generate_all,
)


def main() -> None:
    run(generate_all("created", "Sprint exposure increment"))
