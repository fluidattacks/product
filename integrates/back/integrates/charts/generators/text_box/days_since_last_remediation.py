from decimal import (
    Decimal,
)

from aioextensions import (
    collect,
    run,
)
from async_lru import (
    alru_cache,
)

from integrates.charts import charts_utils
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model.groups.types import (
    GroupUnreliableIndicators,
)


@alru_cache(maxsize=None, typed=True)
async def generate_one(group: str) -> Decimal:
    loaders: Dataloaders = get_new_context()
    group_indicators: GroupUnreliableIndicators = await loaders.group_unreliable_indicators.load(
        group
    )

    return Decimal(group_indicators.last_closed_vulnerability_days or 0)


async def get_many_groups(groups: tuple[str, ...]) -> Decimal:
    groups_data = await collect(map(generate_one, groups), workers=32)

    return min(groups_data) if groups_data else Decimal("Infinity")


def format_data(last_closing_date: Decimal) -> dict[str, Decimal | float]:
    return {"fontSizeRatio": 0.5, "text": last_closing_date}


def format_csv_data(last_closing_date: Decimal) -> charts_utils.CsvData:
    return charts_utils.CsvData(
        headers=["Days since last remediation"],
        rows=[[str(last_closing_date)]],
    )


async def generate_all() -> None:
    last_closing_date: Decimal
    async for group in charts_utils.iterate_groups():
        last_closing_date = await generate_one(group)
        charts_utils.json_dump(
            document=format_data(last_closing_date=last_closing_date),
            entity="group",
            subject=group,
            csv_document=format_csv_data(last_closing_date=last_closing_date),
        )

    async for org_id, _, org_groups in charts_utils.iterate_organizations_and_groups():
        last_closing_date = await get_many_groups(org_groups)
        charts_utils.json_dump(
            document=format_data(last_closing_date=last_closing_date),
            entity="organization",
            subject=org_id,
            csv_document=format_csv_data(last_closing_date=last_closing_date),
        )

    async for org_id, org_name, _ in charts_utils.iterate_organizations_and_groups():
        for portfolio, groups in await charts_utils.get_portfolios_groups(org_name):
            last_closing_date = await get_many_groups(tuple(groups))
            charts_utils.json_dump(
                document=format_data(last_closing_date=last_closing_date),
                entity="portfolio",
                subject=f"{org_id}PORTFOLIO#{portfolio}",
                csv_document=format_csv_data(last_closing_date=last_closing_date),
            )


def main() -> None:
    run(generate_all())
