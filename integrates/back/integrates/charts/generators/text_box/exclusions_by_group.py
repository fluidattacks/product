from aioextensions import (
    run,
)

from integrates.charts import charts_utils
from integrates.charts.generators.text_box.utils import (
    format_csv_data,
    format_document,
)
from integrates.dataloaders import (
    get_new_context,
)
from integrates.db_model.groups.types import (
    GroupUnreliableIndicators,
)


async def generate_one(group: str) -> dict[str, float | int]:
    loaders = get_new_context()
    group_indicators: GroupUnreliableIndicators = await loaders.group_unreliable_indicators.load(
        group
    )
    return format_document(
        group_indicators.nofluid_quantity
        if isinstance(group_indicators.nofluid_quantity, int)
        else 0
    )


async def generate_all() -> None:
    async for group in charts_utils.iterate_groups():
        document = await generate_one(group)
        charts_utils.json_dump(
            document=document,
            entity="group",
            subject=group,
            csv_document=format_csv_data(header="Exclusions as code", value=str(document["text"])),
        )


def main() -> None:
    run(generate_all())
