from collections import (
    Counter,
)

from aioextensions import (
    collect,
    run,
)
from async_lru import (
    alru_cache,
)

from integrates.charts import charts_utils
from integrates.charts.generators.text_box.utils import (
    format_csv_data,
)
from integrates.custom_utils.criteria import (
    CRITERIA_VULNERABILITIES,
)
from integrates.custom_utils.findings import (
    get_group_findings,
)
from integrates.dataloaders import (
    get_new_context,
)

CATEGORIES = len(CRITERIA_VULNERABILITIES.keys())


@alru_cache(maxsize=None, typed=True)
async def generate_one(group: str) -> Counter[str]:
    loaders = get_new_context()
    group_findings = await get_group_findings(group_name=group, loaders=loaders)

    return Counter(finding.title.split(".")[0].strip() for finding in group_findings)


async def get_findings_count_many_groups(groups: tuple[str, ...]) -> Counter[str]:
    groups_findings = await collect(map(generate_one, groups), workers=32)

    return sum(groups_findings, Counter())


def format_data(findings_count: Counter[str]) -> dict[str, float | str]:
    return {
        "fontSizeRatio": 0.5,
        "text": f"{len(findings_count)} / {CATEGORIES}",
    }


async def generate_all() -> None:
    title: str = "Total types"
    async for group in charts_utils.iterate_groups():
        document = format_data(
            findings_count=await generate_one(group),
        )
        charts_utils.json_dump(
            document=document,
            entity="group",
            subject=group,
            csv_document=format_csv_data(header=title, value=str(document["text"])),
        )

    async for org_id, _, org_groups in charts_utils.iterate_organizations_and_groups():
        document = format_data(
            findings_count=await get_findings_count_many_groups(org_groups),
        )
        charts_utils.json_dump(
            document=document,
            entity="organization",
            subject=org_id,
            csv_document=format_csv_data(header=title, value=str(document["text"])),
        )

    async for org_id, org_name, _ in charts_utils.iterate_organizations_and_groups():
        for portfolio, groups in await charts_utils.get_portfolios_groups(org_name):
            document = format_data(
                findings_count=await get_findings_count_many_groups(groups),
            )
            charts_utils.json_dump(
                document=document,
                entity="portfolio",
                subject=f"{org_id}PORTFOLIO#{portfolio}",
                csv_document=format_csv_data(header=title, value=str(document["text"])),
            )


def main() -> None:
    run(generate_all())
