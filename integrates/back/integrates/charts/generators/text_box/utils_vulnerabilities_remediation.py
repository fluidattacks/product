from collections.abc import Iterable
from contextlib import suppress
from datetime import UTC, datetime
from decimal import Decimal
from typing import NamedTuple

from aioextensions import collect
from async_lru import alru_cache
from pandas import date_range

from integrates.charts.charts_utils import (
    CsvData,
    get_portfolios_groups,
    iterate_groups,
    iterate_organizations_and_groups,
    json_dump,
)
from integrates.charts.generators.common.colors import RISK
from integrates.custom_exceptions import UnsanitizedInputFound
from integrates.custom_utils.groups import get_group
from integrates.custom_utils.validations import validate_sanitized_csv_input
from integrates.dataloaders import get_new_context
from integrates.db_model import utils as db_model_utils
from integrates.db_model.types import SeverityScore
from integrates.db_model.vulnerabilities.enums import VulnerabilityStateStatus
from integrates.db_model.vulnerabilities.types import Vulnerability, VulnerabilityState
from integrates.decorators import retry_on_exceptions
from integrates.dynamodb.exceptions import UnavailabilityError
from integrates.groups import domain as groups_domain

ARROW_SMALL: str = "&#11014"


class FormatSprint(NamedTuple):
    created: Decimal
    solved: Decimal
    remediated: Decimal


def get_last_sprint_start_date(
    *, sprint_start_date: datetime | None, sprint_length: int
) -> datetime:
    end_date = db_model_utils.get_min_iso_date(datetime.now()).astimezone(tz=UTC)
    start_date = (
        sprint_start_date.astimezone(tz=UTC)
        if sprint_start_date
        else db_model_utils.get_first_day_iso_date().astimezone(tz=UTC)
    )

    sprint_dates = date_range(
        start=start_date.isoformat(),
        end=end_date.isoformat(),
        freq=f'{sprint_length}W-{start_date.strftime("%A")[:3].upper()}',
    )

    if sprint_dates.size > 0:
        return datetime.combine(sprint_dates.tolist()[-1].date(), datetime.min.time()).astimezone(
            tz=UTC
        )

    return start_date


def get_percentage_change(*, current: Decimal, total: Decimal) -> Decimal:
    if total == Decimal("0.0") and current <= Decimal("0.0"):
        return Decimal("0.0")

    if total == Decimal("0.0") and current > Decimal("0.0"):
        return Decimal("1.0")

    return Decimal(Decimal(current / total).normalize() * Decimal("100.0")).quantize(
        Decimal("0.01")
    )


def get_current_sprint_state(
    state: VulnerabilityState, sprint_start_date: datetime
) -> VulnerabilityState | None:
    if state.modified_date.timestamp() >= sprint_start_date.timestamp():
        return state

    return None


def get_last_state(state: VulnerabilityState, last_day: datetime) -> VulnerabilityState | None:
    if state.modified_date.timestamp() <= last_day.timestamp():
        return state

    return None


def had_state_by_then(
    *,
    last_day: datetime,
    state: VulnerabilityStateStatus,
    vulnerabilities: Iterable[tuple[VulnerabilityState, SeverityScore | None]],
    sprint: bool = False,
) -> Decimal:
    lasts_valid_states: tuple[VulnerabilityState | None, ...]
    if sprint:
        lasts_valid_states = tuple(
            get_current_sprint_state(vulnerability[0], last_day)
            for vulnerability in vulnerabilities
        )
    else:
        lasts_valid_states = tuple(
            get_last_state(vulnerability[0], last_day) for vulnerability in vulnerabilities
        )

    return Decimal(
        sum(
            vulnerability[1].cvssf_v4
            if vulnerability[1] and last_valid_state and last_valid_state.status == state
            else Decimal("0.0")
            for vulnerability, last_valid_state in zip(
                vulnerabilities, lasts_valid_states, strict=False
            )
        )
    )


async def get_totals_by_week(
    *,
    vulnerabilities: Iterable[Vulnerability],
    last_day: datetime,
    sprint: bool = False,
) -> tuple[Decimal, Decimal]:
    open_vulnerabilities = had_state_by_then(
        last_day=last_day,
        state=VulnerabilityStateStatus.VULNERABLE,
        vulnerabilities=[
            (vulnerability.state, vulnerability.severity_score) for vulnerability in vulnerabilities
        ],
        sprint=sprint,
    )

    closed_vulnerabilities = had_state_by_then(
        last_day=last_day,
        state=VulnerabilityStateStatus.SAFE,
        vulnerabilities=[
            (vulnerability.state, vulnerability.severity_score) for vulnerability in vulnerabilities
        ],
        sprint=sprint,
    )

    return Decimal(open_vulnerabilities), Decimal(closed_vulnerabilities)


@alru_cache(maxsize=None, typed=True)
async def generate_one(*, group_name: str) -> FormatSprint:
    loaders = get_new_context()
    group = await get_group(loaders, group_name)
    current_sprint_date = get_last_sprint_start_date(
        sprint_start_date=group.sprint_start_date,
        sprint_length=group.sprint_duration,
    )
    vulnerabilities = await groups_domain.get_group_vulns_with_severity(loaders, group_name)

    opened_current_sprint, closed_current_sprint = await get_totals_by_week(
        vulnerabilities=vulnerabilities,
        last_day=current_sprint_date,
        sprint=True,
    )

    total_current_open, total_current_closed = await get_totals_by_week(
        vulnerabilities=vulnerabilities,
        last_day=datetime.now(tz=UTC),
    )

    solved = get_percentage_change(
        current=closed_current_sprint * Decimal("-1.0")
        if closed_current_sprint > Decimal("0.0")
        else closed_current_sprint,
        total=total_current_closed + total_current_open,
    )
    created = get_percentage_change(
        current=opened_current_sprint,
        total=total_current_open + total_current_closed,
    )
    created = created if created > Decimal("0.0") else Decimal("0")

    return FormatSprint(
        solved=solved,
        created=created,
        remediated=Decimal(solved + created).quantize(Decimal("0.01")),
    )


async def get_many_groups(*, group_names: tuple[str, ...]) -> FormatSprint:
    groups_data = await collect(
        tuple(generate_one(group_name=group_name) for group_name in group_names),
        workers=16,
    )
    number_of_groups = len(groups_data)

    if number_of_groups:
        return FormatSprint(
            created=Decimal(
                sum(group.created for group in groups_data) / number_of_groups
            ).quantize(Decimal("0.01")),
            solved=Decimal(sum(group.solved for group in groups_data) / number_of_groups).quantize(
                Decimal("0.01")
            ),
            remediated=Decimal(
                sum(group.remediated for group in groups_data) / number_of_groups
            ).quantize(Decimal("0.01")),
        )

    return FormatSprint(
        created=Decimal("0.0"),
        remediated=Decimal("0.0"),
        solved=Decimal("0.0"),
    )


def format_data(count: Decimal, state: str) -> dict:
    if state == "created" and count > Decimal("0.0"):
        return {
            "arrowFontSizeRatio": 0.45,
            "fontSizeRatio": 0.5,
            "text": count,
            "color": RISK.more_aggressive,
            "arrow": ARROW_SMALL,
            "percentage": True,
        }

    if state == "solved" and count < Decimal("0.0"):
        return {
            "arrowFontSizeRatio": 0.45,
            "fontSizeRatio": 0.5,
            "text": count,
            "color": RISK.more_passive,
            "arrow": "&#11015;",
            "percentage": True,
        }

    if state == "solved" and count > Decimal("0.0"):
        return {
            "arrowFontSizeRatio": 0.45,
            "fontSizeRatio": 0.5,
            "text": count,
            "color": RISK.more_aggressive,
            "arrow": ARROW_SMALL,
            "percentage": True,
        }

    if state == "remediated" and count > Decimal("0.0"):
        return {
            "arrowFontSizeRatio": 0.45,
            "fontSizeRatio": 0.5,
            "text": count,
            "color": RISK.more_aggressive,
            "arrow": ARROW_SMALL,
            "percentage": True,
        }

    if state == "remediated" and count < Decimal("0.0"):
        return {
            "arrowFontSizeRatio": 0.45,
            "fontSizeRatio": 0.5,
            "text": count,
            "color": RISK.more_passive,
            "arrow": "&#11015;",
            "percentage": True,
        }

    return {
        "fontSizeRatio": 0.5,
        "text": count,
        "percentage": True,
    }


def format_csv_data(*, header: str, value: str) -> CsvData:
    headers_row = [""]
    with suppress(UnsanitizedInputFound):
        validate_sanitized_csv_input(header)
        headers_row = [header]

    return CsvData(headers=headers_row, rows=[[value]])


def format_count(count: FormatSprint) -> dict[str, Decimal]:
    return {
        "created": count.created.quantize(Decimal("0.1")),
        "remediated": count.remediated.quantize(Decimal("0.1")),
        "solved": count.solved.quantize(Decimal("0.1")),
    }


@retry_on_exceptions(
    exceptions=(UnavailabilityError,),
    max_attempts=5,
)
async def generate_all(state: str, title: str) -> None:
    async for group_name in iterate_groups():
        document = format_data(
            count=format_count(
                count=await generate_one(group_name=group_name),
            )[state],
            state=state,
        )
        json_dump(
            document=document,
            entity="group",
            subject=group_name,
            csv_document=format_csv_data(header=title, value=f"{document['text']!s}%"),
        )

    async for org_id, _, org_group_names in iterate_organizations_and_groups():
        document = format_data(
            count=format_count(
                count=await get_many_groups(group_names=org_group_names),
            )[state],
            state=state,
        )
        json_dump(
            document=document,
            entity="organization",
            subject=org_id,
            csv_document=format_csv_data(header=title, value=f"{document['text']!s}%"),
        )

    async for org_id, org_name, groups in iterate_organizations_and_groups():
        for portfolio, group_names in await get_portfolios_groups(org_name, org_groups=groups):
            document = format_data(
                count=format_count(
                    count=await get_many_groups(group_names=tuple(group_names)),
                )[state],
                state=state,
            )
            json_dump(
                document=document,
                entity="portfolio",
                subject=f"{org_id}PORTFOLIO#{portfolio}",
                csv_document=format_csv_data(header=title, value=f"{document['text']!s}%"),
            )
