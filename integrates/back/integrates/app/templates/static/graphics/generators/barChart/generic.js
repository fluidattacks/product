/* global bb */
/* global d3 */

const defaultPaddingRatio = 0.055;
const minCvssfValue = 10;
const normalizationFactor = 255;
const luminanceThreshold = 0.03928;
const lowLuminanceDivisor = 12.92;
const gammaCorrection = 2.4;
const redWeight = 0.2126;
const greenWeight = 0.7152;
const blueWeight = 0.0722;
const gammaAdjustment = 0.055;
const gammaDivisor = 1.055;
const transformThreshold = 0.8;

function getTooltip(datum, defaultValueFormat) {
  return `
    <table class="bb-tooltip" style="position: absolute; left: 20px">
      <tbody>
        <tr>
          <td> ${ defaultValueFormat(datum[0].value) }</td>
        </tr>
      </tbody>
    </table>
  `;
}

function getLuminance(red, green, blue) {
  const normalizedColors = [ red, green, blue ].map((colorValue) => {
    const normalized = colorValue / normalizationFactor;
    return normalized <= luminanceThreshold ?
      normalized / lowLuminanceDivisor :
      Math.pow((normalized + gammaAdjustment) / gammaDivisor, gammaCorrection);
  });

  const redLuminance = redWeight * normalizedColors[0];
  const greenLuminance = greenWeight * normalizedColors[1];
  const blueLuminance = blueWeight * normalizedColors[2];

  return redLuminance + greenLuminance + blueLuminance;
}

function getTextColor(r, g, b) {
  const luminance = getLuminance(r, g, b);
  return luminance < transformThreshold ? '#FFFFFF' : '#000000';
}

function centerLabel(dataDocument) {
  if (dataDocument.mttrBenchmarking || dataDocument.mttrCvssf || dataDocument.byLevel) {
    const rectHeight = parseFloat(parseFloat(d3.select('.bb-event-rect').attr('height')).toFixed(2));
    const transformText = 12;
    d3.selectAll('.bb-chart-texts .bb-text').each((_d, index, textList) => {
      d3.selectAll('.bb-bars path.bb-bar').each((_barData, barIndex) => {
        const fillColor = d3.select(textList[index]).style('fill');
        const rgbColor = d3.rgb(fillColor);
        const textColor = getTextColor(rgbColor.r, rgbColor.g, rgbColor.b);
        if (barIndex === index) {
          d3.select(textList[barIndex]).attr('style', `fill: ${ textColor } !important`);
        }
      });
      const haveDiffToMove = parseFloat(parseFloat(d3.select(textList[index]).attr('diffToMoveY')).toFixed(2));
      if (haveDiffToMove) {
        d3.select(textList[index]).attr('y', haveDiffToMove);
      } else {
        const textHeight = parseFloat(parseFloat(d3.select(textList[index]).attr('y')).toFixed(2));
        const diffHeight = parseFloat(parseFloat((rectHeight - textHeight) / 2).toFixed(2));
        if (diffHeight > transformText) {
          const diffToMove = (textHeight + diffHeight - transformText).toFixed(2);
          d3.select(textList[index]).attr('y', diffToMove).attr('diffToMoveY', diffToMove);
        }
      }
    });
  }
  if (dataDocument.originalIds) {
    d3.selectAll('.bb-axis-x .tick tspan').each((
      _datum,
      index,
      textList,
    ) => {
      const newSelf = d3.select(textList[index]);
      const text = newSelf.text();
      newSelf.html(
        `
          <a
            href="https://${ window.location.host }/${ dataDocument.originalIds[index] }"
            rel="nofollow noopener noreferrer" target="_blank"
          >
            ${ text }
          </a>
        `,
      );
    });
  }
}

function getPixels(value) {
  const maxPositiveNumber = 10000;
  const maxNegativeNumber = -1000;
  const moveTextPositive = parseFloat(value) > maxPositiveNumber ? '-60' : '-40';
  const moveTextNegative = parseFloat(value) < maxNegativeNumber ? '65' : '45';

  return parseFloat(value) > 0 ? moveTextPositive : moveTextNegative;
}

function getExposureColor(d) {
  return d[0].index === 0 ? '#ac0a17' : '#fda6ab';
}

function getAxisLabel(dataDocument) {
  if (!dataDocument.axis.rotated) {
    d3.select('.bb-axis-y-label').attr('dx', '-0.3em').attr('dy', '1em');
  }
}

function getMttrColor(d) {
  return d[0].index === 0 ? '#BF0B1A' : '#FBBAC0';
}

function getColorAdjusted(datum, originalValues) {
  if (originalValues[datum.x] > 0) {
    return '#da1e28';
  }

  return '#30c292';
}

function getMttrCvssfColor(d) {
  if (typeof d[0] === 'object') {
    switch (d[0].index.toString()) {
      case '0':
        return '#9A0915';
      case '1':
        return '#F04438';
      case '2':
        return '#FDB022';
      case '3':
        return '#FEF0C7';
      default:
        return '#177e89';
    }
  }

  return '#177e89';
}

function getColor(dataDocument, d, originalValues) {
  if (originalValues[d[0].x] > 0) {
    return '#da1e28';
  }
  if (dataDocument.exposureTrendsByCategories) {
    return '#30c292';
  }

  return '#33cc99';
}

function getTooltipColorContent(dataDocument, originalValues, d, color) {
  if (!dataDocument.keepToltipColor) {
    if (dataDocument.exposureTrendsByCategories) {
      return () => getColor(dataDocument, d, originalValues);
    }

    if (dataDocument.mttrBenchmarking) {
      return () => getMttrColor(d);
    }

    if (dataDocument.mttrCvssf) {
      return () => getMttrCvssfColor(d);
    }

    if (dataDocument.exposureBenchmarkingCvssf) {
      return () => getExposureColor(d);
    }
  }

  return color;
}

function formatYTick(value, tick, dataDocument) {
  if (tick && tick.count && !dataDocument.byLevel) {
    const valueParsed = parseFloat(parseFloat(value).toFixed(1));
    if (valueParsed < minCvssfValue) {
      return d3.format(',.1~f')(valueParsed);
    }
    return d3.format(',~d')(valueParsed);
  }

  return value % 1 === 0 ? d3.format(',~d')(value) : '';
}

function formatXTick(index, categories) {
  if (categories.length > 0 && index % 1 === 0) {
    const slicedSize = -60;
    if (Math.abs(slicedSize) > categories[index].length) {
      return categories[index];
    }

    return `...${ categories[index].slice(slicedSize) }`;
  }
  return '';
}

function formatYTickAdjusted(value) {
  if (value === 0.0) {
    return value;
  }
  const base = 100.0;
  const adjustedBase = 10.0;
  const yTick = Math.round(Math.pow(2.0, Math.abs(value)) * adjustedBase) / base;

  if (value < 0.0) {
    return d3.format(',.1~f')(-yTick);
  }

  return d3.format(',.1~f')(yTick);
}

function getMinLabelValueToDisplay(isRotated) {
  const rotatedMinValue = 0.08;
  const minValue = 0.10;

  if (isRotated) {
    return rotatedMinValue;
  }

  return minValue;
}

function formatLabelsAdjusted(datum, index, dataDocument, columns) {
  const { maxValueLogAdjusted, originalValues } = dataDocument;
  const minValue = getMinLabelValueToDisplay(dataDocument.axis.rotated);

  if ((Math.abs(datum / maxValueLogAdjusted) > minValue) && !dataDocument.axis.rotated) {
    if (typeof index === 'undefined') {
      const values = columns.filter((value) => value === datum);

      return values.length > 0 ? d3.format(',.1~f')(values[0]) : 0;
    }
    return d3.format(',.1~f')(originalValues[index]);
  }

  return '';
}

function formatLogYTick(value) {
  if (value === 0.0) {
    return value;
  }
  const base = 100.0;
  const valueParsed = parseFloat(parseFloat(Math.round(Math.pow(2.0, value) * base) / base).toFixed(1));
  if (valueParsed < minCvssfValue) {
    return d3.format(',.1~f')(valueParsed);
  }
  return d3.format(',~d')(valueParsed);
}

function formatLogLabels(datum, index, dataDocument, columns) {
  const { maxValueLog, originalValues } = dataDocument;
  const minValue = getMinLabelValueToDisplay(dataDocument.axis.rotated);

  if (datum / maxValueLog > minValue && !dataDocument.axis.rotated) {
    if (typeof index === 'undefined') {
      const values = columns.filter((value) => value === datum);

      return values.length > 0 ? d3.format(',.1~f')(values[0]) : 0;
    }

    return d3.format(',.1~f')(originalValues[index]);
  }

  return '';
}

function formatLabels(datum, maxValue, dataDocument) {
  const minValue = 0.15;
  if (datum / maxValue > minValue && !dataDocument.axis.rotated) {
    return datum;
  }

  return '';
}

// eslint-disable-next-line complexity
function render(dataDocument, height, width) {
  dataDocument.paddingRatioLeft = 0.065;
  dataDocument.paddingRatioBottom = 0.09;

  if (dataDocument.axis.rotated) {
    dataDocument.paddingRatioLeft = 0.35;
    dataDocument.paddingRatioRight = 0.03;
    dataDocument.paddingRatioTop = 0.02;
    dataDocument.paddingRatioBottom = 0.16;
  }

  if (dataDocument.barChartYTickFormat) {
    const { tick } = dataDocument.axis.y;
    dataDocument.axis.y.tick = { ...tick, format: (x) => formatYTick(x, tick, dataDocument) };
  }

  if (dataDocument.barChartXTickFormat) {
    dataDocument.axis.x.tick.format = (index) => formatXTick(index, dataDocument.axis.x.categories);
    dataDocument.tooltip.format.title = (_datum, index) => dataDocument.axis.x.categories[index];
  }

  if (dataDocument.maxValue) {
    dataDocument.data.labels = {
      format: (datum) => formatLabels(datum, dataDocument.maxValue, dataDocument),
    };
  }

  if (dataDocument.mttrBenchmarking) {
    dataDocument.data.colors = {
      'Mean time to remediate': (d) => getMttrColor([ d ]),
      'Exposure': (d) => getExposureColor([ d ]),
    };
  }

  if (dataDocument.maxValueLog) {
    const { originalValues, data: columnsData } = dataDocument;
    const { columns } = columnsData;
    const { tick } = dataDocument.axis.y;
    dataDocument.axis.y.tick = { ...tick, format: formatLogYTick };
    dataDocument.data.labels = {
      format: (datum, _id, index) => formatLogLabels(datum, index, dataDocument, columns),
    };
    const { tooltip } = dataDocument;
    dataDocument.tooltip = {
      ...tooltip, format: {
        value: (_datum, _r, _id, index) => d3.format(',.1~f')(originalValues[index]),
      },
    };
  }

  if (dataDocument.maxValueLogAdjusted) {
    const { originalValues, data: columnsData } = dataDocument;
    const { columns } = columnsData;
    dataDocument.axis.y.tick = { format: formatYTickAdjusted };
    dataDocument.data.color = (_color, datum) => getColorAdjusted(datum, originalValues);
    dataDocument.tooltip = { format: { value: (_datum, _r, _id, index) => d3.format(',.1~f')(originalValues[index]) } };
    dataDocument.data.labels = {
      format: (datum, _id, index) => formatLabelsAdjusted(
        datum, index, dataDocument, columns[0],
      ),
    };
  }

  if (dataDocument.mttrCvssf) {
    dataDocument.data.colors = {
      'Mean time to remediate': (d) => getMttrCvssfColor([ d ]),
    };
  }

  const { originalValues } = dataDocument;

  return bb.generate({
    // eslint-disable-next-line id-blacklist
    data: {
      onmouseover: () => {
        getAxisLabel(dataDocument);
      },
      onmouseout: () => {
        getAxisLabel(dataDocument);
      },
      onclick: () => {
        getAxisLabel(dataDocument);
      },
    },
    ...dataDocument,
    tooltip: {
      ...dataDocument.tooltip,
      contents(d, defaultTitleFormat, defaultValueFormat, color) {
        if (!dataDocument.axis.rotated) {
          return this.getTooltipContent(
            d,
            defaultTitleFormat,
            defaultValueFormat,
            getTooltipColorContent(dataDocument, originalValues, d, color),
          );
        }

        return getTooltip(d, defaultValueFormat);
      },
    },
    onrendered: () => {
      centerLabel(dataDocument);
      getAxisLabel(dataDocument);
    },
    onmouseover: () => {
      getAxisLabel(dataDocument);
    },
    onmouseout: () => {
      getAxisLabel(dataDocument);
    },
    bindto: '#root',
    padding: {
      bottom: (dataDocument.paddingRatioBottom ? dataDocument.paddingRatioBottom : defaultPaddingRatio) * height,
      left: (dataDocument.paddingRatioLeft ? dataDocument.paddingRatioLeft : defaultPaddingRatio) * width,
      right: (dataDocument.paddingRatioRight ? dataDocument.paddingRatioRight : defaultPaddingRatio) * width,
      top: (dataDocument.paddingRatioTop ? dataDocument.paddingRatioTop : defaultPaddingRatio) * height,
    },
    size: {
      height,
      width,
    },
    transition: {
      duration: 0,
    },
  });
}

function load() {
  const args = JSON.parse(document.getElementById('args').textContent.replace(/'/g, '"'));
  const dataDocument = JSON.parse(args.data);

  render(dataDocument, args.height, args.width);

  if (dataDocument.exposureTrendsByCategories && dataDocument.axis.rotated) {
    d3.select('#root').select('.bb-axis-y').select('.domain')
      .style('visibility', 'hidden');
    d3.select('#root').select('.bb-axis-y').selectAll('line')
      .style('visibility', 'hidden');
    d3.select('#root').select('.bb-axis-x').select('.domain')
      .style('visibility', 'hidden');
    d3.select('#root').select('.bb-axis-x').selectAll('line')
      .style('visibility', 'hidden');
    d3.select('#root')
      .selectAll('.bb-chart-texts .bb-text').each((_d, index, textList) => {
        const text = d3.select(textList[index]).text();
        const value = text.replace(',', '');
        const pixels = getPixels(value);

        if (parseFloat(value) === 0) {
          d3.select(textList[index])
            .style('visibility', 'hidden');
        } else {
          d3.select(textList[index]).style('transform', `translate(${ pixels }px, -1px)`);
        }
      });
  }

  if (dataDocument.hideYAxisLine) {
    d3.select('.bb-axis-y')
      .select('.domain')
      .style('visibility', 'hidden');
    d3.select('.bb-axis-y')
      .selectAll('.tick').each((_d, index, tickList) => {
        d3.select(tickList[index])
          .select('line')
          .style('visibility', 'hidden');
      });
  }

  if (dataDocument.hideXTickLine) {
    d3.select('.bb-axis-x')
      .selectAll('.tick').each((_d, index, tickList) => {
        d3.select(tickList[index])
          .select('line')
          .style('visibility', 'hidden');
      });
  }
}

window.load = load;
