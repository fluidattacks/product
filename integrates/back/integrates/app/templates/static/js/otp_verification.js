document.addEventListener("DOMContentLoaded", function () {
  const goBackBtn = document.getElementById("go-back");
  const tryAnotherWayBtn = document.getElementById("try-another-way");
  const otpInputs = document.querySelectorAll(".otp-input");
  const otpModal = document.getElementById("otp-modal");
  const methodModal = document.getElementById("method-modal");
  const verifyButton = document.getElementById("email-verify-submit");

  function toggleModalVisibility(modalToShow, modalToHide) {
    modalToShow.classList.remove("hidden");
    modalToHide.classList.add("hidden");
  }

  otpInputs.forEach(function (input, index) {
    input.addEventListener("input", function () {
      let value = this.value;
      value = value.replace(/\D/g, '');
      this.value = value;

      if (value.length >= 1 && index < otpInputs.length - 1) {
        otpInputs[index + 1].focus();
      }
    });

    input.addEventListener("keydown", function (event) {
      if (event.key === "Backspace" && this.value.length === 0 && index > 0) {
        otpInputs[index - 1].focus();
      }
    });

    input.addEventListener("paste", function(event) {
      event.preventDefault();

      const pasteData = event.clipboardData.getData("text/plain");
      const digits = pasteData.match(/\d/g);

      if (digits !== null && digits.length <= 6) {
          digits.forEach(function(digit, i) {
              otpInputs[index + i].value = digit;
          });
      }
    });
  });

  otpModal.addEventListener("keydown", function (event) {
    if (event.key === "Enter") {
      event.preventDefault();
      verifyButton.click();
    }
  });

  tryAnotherWayBtn.addEventListener("click", function () {
    toggleModalVisibility(methodModal, otpModal);
  });

  goBackBtn.addEventListener("click", function () {
    toggleModalVisibility(otpModal, methodModal);
  });
});
