from datetime import datetime

from starlette.requests import Request
from starlette.responses import Response

from integrates.app.views.evidence import enforce_group_level_role, get_finding_evidence
from integrates.dataloaders import get_new_context
from integrates.db_model.findings.types import FindingEvidence, FindingEvidences
from integrates.db_model.stakeholders.types import StateSessionType
from integrates.sessions.domain import encode_token
from integrates.sessions.utils import calculate_hash_token
from integrates.settings import JWT_COOKIE_NAME
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    FindingFaker,
    GroupAccessFaker,
    GroupAccessStateFaker,
    GroupFaker,
    OrganizationFaker,
    StakeholderFaker,
    StakeholderSessionTokenFaker,
    random_uuid,
)
from integrates.testing.mocks import mocks

USER_EMAIL = "jdoe@fluidattacks.com"
FIN_ID = "4222865126-2120-6701-9896-28519594d737"
FILE_ID = f"group1-{FIN_ID}-evidence1.png"
ORG_ID = "ORG#6b110f25-cd46-e94c-2bad-b9e182cf91a8"


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            findings=[
                FindingFaker(
                    group_name="group1",
                    id=FIN_ID,
                    evidences=FindingEvidences(
                        evidence1=FindingEvidence(
                            description="evidence1",
                            url=FILE_ID,
                            modified_date=datetime.fromisoformat("2024-11-19T15:30:10+00:00"),
                        )
                    ),
                ),
            ],
            group_access=[
                GroupAccessFaker(
                    email=USER_EMAIL,
                    group_name="group1",
                    state=GroupAccessStateFaker(role="group_manager"),
                )
            ],
            groups=[GroupFaker(name="group1", organization_id=ORG_ID)],
            organizations=[OrganizationFaker(id=ORG_ID, name="okada")],
            stakeholders=[
                StakeholderFaker(
                    email=USER_EMAIL,
                    enrolled=True,
                    role="group_manager",
                    session_token=StakeholderSessionTokenFaker(
                        jti=calculate_hash_token()["jti"],
                        state=StateSessionType.IS_VALID,
                    ),
                )
            ],
        ),
    )
)
async def test_get_finding_evidence() -> None:
    payload = {
        "user_email": USER_EMAIL,
        "first_name": "john",
        "last_name": "doe",
    }
    token = encode_token(
        expiration_time=None,
        payload=payload,
        subject="starlette_session",
    )
    request = Request(
        {
            "cookies": {JWT_COOKIE_NAME: token},
            "headers": {"content_type": "image/png"},
            "method": "GET",
            "scheme": "https",
            "session": {"username": USER_EMAIL, "session_key": random_uuid()},
            "state": {
                "store": {
                    "integrates.sessions.domain -> get_jwt_content()": {
                        "user_email": USER_EMAIL,
                    }
                }
            },
            "path": f"/orgs/okada/groups/group1/vulns/{FIN_ID}/evidence/{FILE_ID}",
            "path_params": {
                "org_name": "okada",
                "group_name": "group1",
                "finding_id": FIN_ID,
                "file_id": FILE_ID,
            },
            "type": "http",
        }
    )

    # Act
    response: Response = await get_finding_evidence(request)

    # Assert
    assert response.status_code == 200
    assert (
        response.body == b'{"data":[],"message":"Access denied or evidence not found","error":true}'
    )


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            findings=[FindingFaker(group_name="group1", id="5469032")],
            groups=[GroupFaker(name="group1", organization_id=ORG_ID)],
            organizations=[OrganizationFaker(id=ORG_ID, name="okada")],
        ),
    )
)
async def test_get_finding_evidence_fail() -> None:
    request = Request(
        {
            "cookies": {JWT_COOKIE_NAME: "token"},
            "headers": {"content_type": "image/png"},
            "method": "GET",
            "scheme": "https",
            "session": {"username": USER_EMAIL, "session_key": random_uuid},
            "state": {
                "store": {
                    "integrates.sessions.domain -> get_jwt_content()": {
                        "user_email": USER_EMAIL,
                    }
                }
            },
            "path": "/orgs/okada/groups/group1/vulns/5469032/evidence/test.png",
            "path_params": {
                "org_name": "okada",
                "group_name": "group1",
                "finding_id": "5469032",
                "file_id": "test.png",
            },
            "type": "http",
        }
    )

    # Act
    response: Response = await get_finding_evidence(request)

    # Assert
    assert response.status_code == 403
    assert response.body == b"Access denied"


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            findings=[
                FindingFaker(
                    group_name="group1",
                    id=FIN_ID,
                    evidences=FindingEvidences(
                        evidence1=FindingEvidence(
                            description="evidence1",
                            url=FILE_ID,
                            modified_date=datetime.fromisoformat("2024-11-19T15:30:10+00:00"),
                        )
                    ),
                ),
            ],
            group_access=[
                GroupAccessFaker(
                    email=USER_EMAIL,
                    group_name="group1",
                    state=GroupAccessStateFaker(role="group_manager"),
                )
            ],
            groups=[GroupFaker(name="group1", organization_id=ORG_ID)],
            organizations=[OrganizationFaker(id=ORG_ID, name="okada")],
            stakeholders=[
                StakeholderFaker(
                    email=USER_EMAIL,
                    enrolled=True,
                    role="group_manager",
                    session_token=StakeholderSessionTokenFaker(
                        jti=calculate_hash_token()["jti"],
                        state=StateSessionType.IS_VALID,
                    ),
                )
            ],
        ),
    )
)
async def test_enforce_group_level_role() -> None:
    payload = {
        "user_email": USER_EMAIL,
        "first_name": "john",
        "last_name": "doe",
    }
    token = encode_token(
        expiration_time=None,
        payload=payload,
        subject="starlette_session",
    )
    request = Request(
        {
            "cookies": {JWT_COOKIE_NAME: token},
            "headers": {"content_type": "image/png"},
            "method": "GET",
            "scheme": "https",
            "session": {"username": USER_EMAIL, "session_key": random_uuid()},
            "state": {
                "store": {
                    "integrates.sessions.domain -> get_jwt_content()": {
                        "user_email": USER_EMAIL,
                    }
                }
            },
            "path": f"/orgs/okada/groups/group1/vulns/{FIN_ID}/evidence/{FILE_ID}",
            "path_params": {
                "org_name": "okada",
                "group_name": "group1",
                "finding_id": FIN_ID,
                "file_id": FILE_ID,
            },
            "type": "http",
        }
    )
    loaders = get_new_context()
    assert await enforce_group_level_role(loaders, request, "group1", "group_manager") is None


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            findings=[
                FindingFaker(
                    group_name="group1",
                    id=FIN_ID,
                    evidences=FindingEvidences(
                        evidence1=FindingEvidence(
                            description="evidence1",
                            url=FILE_ID,
                            modified_date=datetime.fromisoformat("2024-11-19T15:30:10+00:00"),
                        )
                    ),
                ),
            ],
            group_access=[
                GroupAccessFaker(
                    email=USER_EMAIL,
                    group_name="group1",
                    state=GroupAccessStateFaker(role="group_manager"),
                )
            ],
            groups=[GroupFaker(name="group1", organization_id=ORG_ID)],
            organizations=[OrganizationFaker(id=ORG_ID, name="okada")],
            stakeholders=[
                StakeholderFaker(
                    email=USER_EMAIL,
                    enrolled=True,
                    role="group_manager",
                    session_token=StakeholderSessionTokenFaker(
                        jti=calculate_hash_token()["jti"],
                        state=StateSessionType.IS_VALID,
                    ),
                )
            ],
        ),
    )
)
async def test_enforce_group_level_role_no_access() -> None:
    payload = {
        "user_email": USER_EMAIL,
        "first_name": "john",
        "last_name": "doe",
    }
    token = encode_token(
        expiration_time=None,
        payload=payload,
        subject="starlette_session",
    )
    request = Request(
        {
            "cookies": {JWT_COOKIE_NAME: token},
            "headers": {"content_type": "image/png"},
            "method": "GET",
            "scheme": "https",
            "session": {"username": USER_EMAIL, "session_key": random_uuid()},
            "state": {
                "store": {
                    "integrates.sessions.domain -> get_jwt_content()": {
                        "user_email": USER_EMAIL,
                    }
                }
            },
            "path": f"/orgs/okada/groups/group1/vulns/{FIN_ID}/evidence/{FILE_ID}",
            "path_params": {
                "org_name": "okada",
                "group_name": "group1",
                "finding_id": FIN_ID,
                "file_id": FILE_ID,
            },
            "type": "http",
        }
    )
    loaders = get_new_context()
    response = await enforce_group_level_role(loaders, request, "group1", "role")

    assert response
    assert response.status_code == 403
    assert response.body == b"Access denied"
