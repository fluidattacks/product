from authlib.integrations.base_client.errors import OAuthError
from starlette.requests import Request
from starlette.responses import RedirectResponse

import integrates.mailer.common as mailer_common
from integrates.app import utils
from integrates.app.views import auth
from integrates.custom_exceptions import OnlyCorporateEmails
from integrates.dataloaders import get_new_context
from integrates.sessions.domain import encode_token
from integrates.sessions.types import UserAccessInfo
from integrates.settings.jwt import JWT_COOKIE_NAME
from integrates.stakeholders import (
    domain as stakeholders_domain,
)
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    GroupAccessFaker,
    GroupAccessStateFaker,
    GroupFaker,
    OrganizationAccessFaker,
    OrganizationFaker,
    StakeholderFaker,
    TrialFaker,
    random_uuid,
)
from integrates.testing.mocks import Mock, mocks
from integrates.testing.utils import parametrize, raises
from integrates.verify import operations as verify_operations

STAKEHOLDER1 = "jdoe@fluidattacks.com"
STAKEHOLDER2 = "jane@fluidattacks.com"
USER = "user@fluidattacks.com"


@parametrize(
    args=["user_info", "aws_customer_id"],
    cases=[
        [
            UserAccessInfo(
                first_name="Joe",
                last_name="Doe",
                subject="test#user",
                user_email=STAKEHOLDER1,
                verified=True,
            ),
            "1234",
        ],
        [
            UserAccessInfo(
                first_name="Joe",
                last_name="Doe",
                subject="test#user",
                user_email=STAKEHOLDER2,
                verified=True,
            ),
            None,
        ],
        [
            UserAccessInfo(
                first_name="Test",
                last_name="User",
                subject="test#user",
                user_email=USER,
                verified=True,
            ),
            None,
        ],
        [
            UserAccessInfo(
                first_name="Test",
                last_name="User",
                subject="test#user",
                user_email=USER,
                verified=True,
            ),
            "1234",
        ],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            stakeholders=[
                StakeholderFaker(email=STAKEHOLDER1),
                StakeholderFaker(email=STAKEHOLDER2, aws_customer_id="1234"),
            ]
        )
    ),
    others=[
        Mock(auth, "is_personal_email", "async", False),
        Mock(utils, "is_personal_email", "async", False),
        Mock(stakeholders_domain, "register_login", "async", False),
    ],
)
async def test_log_stakeholder_in(user_info: UserAccessInfo, aws_customer_id: str | None) -> None:
    loaders = get_new_context()
    payload = {
        "user_email": user_info.user_email,
        "first_name": user_info.first_name,
        "last_name": user_info.last_name,
    }
    token = encode_token(
        expiration_time=None,
        payload=payload,
        subject="starlette_session",
    )
    request = Request(
        {
            "cookies": {JWT_COOKIE_NAME: token},
            "headers": {
                "User-Agent": "Mozilla/5.0 (Linux; x86_64) Firefox/115.0",
            },
            "method": "GET",
            "scheme": "https",
            "session": {
                "username": user_info.user_email,
                "session_key": random_uuid(),
                "aws_customer_id": aws_customer_id,
            },
            "type": "http",
        }
    )
    await auth.log_stakeholder_in(loaders, user_info, request)


async def test_log_stakeholder_in_oauth_error() -> None:
    user_info = UserAccessInfo(
        first_name="Test",
        last_name="User",
        subject="test#user",
        user_email=USER,
        verified=False,
    )
    loaders = get_new_context()
    request = Request(
        {
            "method": "GET",
            "scheme": "https",
            "session": {
                "username": user_info.user_email,
                "session_key": random_uuid(),
            },
            "type": "http",
        }
    )
    with raises(OAuthError):
        await auth.log_stakeholder_in(loaders, user_info, request)


@mocks(
    aws=IntegratesAws(dynamodb=IntegratesDynamodb()),
    others=[
        Mock(auth, "is_personal_email", "async", True),
    ],
)
async def test_log_stakeholder_in_only_corporate_error() -> None:
    user_info = UserAccessInfo(
        first_name="Test",
        last_name="User",
        subject="test#user",
        user_email="user@gmail.com",
        verified=True,
    )
    loaders = get_new_context()
    request = Request(
        {
            "method": "GET",
            "scheme": "https",
            "session": {
                "username": user_info.user_email,
                "session_key": random_uuid(),
            },
            "type": "http",
        }
    )
    with raises(OnlyCorporateEmails):
        await auth.log_stakeholder_in(loaders, user_info, request)


@mocks(
    aws=IntegratesAws(dynamodb=IntegratesDynamodb()),
    others=[
        Mock(auth, "is_personal_email", "async", True),
    ],
)
async def test_get_auth_response_invalid_email() -> None:
    user_info = UserAccessInfo(
        first_name="Test",
        last_name="User",
        subject="test#user",
        user_email="user@gmail.com",
        verified=True,
    )
    request = Request(
        {
            "method": "GET",
            "scheme": "https",
            "session": {
                "username": user_info.user_email,
                "session_key": random_uuid(),
            },
            "type": "http",
        }
    )
    response = await auth.get_auth_response(request, user_info)
    assert isinstance(response, RedirectResponse)
    assert response.headers["location"] == "/SignUp?error=invalidEmail"


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            groups=[GroupFaker(name="group1", organization_id="org1")],
            stakeholders=[StakeholderFaker(email="some_user@test.com")],
            group_access=[
                GroupAccessFaker(
                    email="some_user@test.com",
                    domain="test.com",
                    group_name="group1",
                    state=GroupAccessStateFaker(role="organization_manager"),
                )
            ],
            organization_access=[
                OrganizationAccessFaker(
                    email="some_user@test.com",
                    organization_id="org1",
                )
            ],
        )
    ),
    others=[
        Mock(auth, "is_personal_email", "async", False),
        Mock(utils, "is_personal_email", "async", False),
        Mock(verify_operations, "check_verification", "async", False),
        Mock(mailer_common, "send_mail_async", "async", False),
    ],
)
async def test_get_auth_response_existing_domain() -> None:
    user_info = UserAccessInfo(
        first_name="Test",
        last_name="User",
        subject="test#user",
        user_email="user@test.com",
        verified=True,
    )
    request = Request(
        {
            "headers": {
                "User-Agent": "Mozilla/5.0 (Linux; x86_64) Firefox/115.0",
            },
            "method": "GET",
            "scheme": "https",
            "session": {
                "username": user_info.user_email,
                "session_key": random_uuid(),
            },
            "type": "http",
        }
    )
    response = await auth.get_auth_response(request, user_info)
    assert isinstance(response, RedirectResponse)
    assert response.headers["location"] == "/SignUp?error=existingDomain"


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            trials=[TrialFaker(email="user@test.com", completed=True)],
        )
    ),
    others=[
        Mock(auth, "is_personal_email", "async", False),
        Mock(utils, "is_personal_email", "async", False),
        Mock(verify_operations, "check_verification", "async", False),
        Mock(mailer_common, "send_mail_async", "async", False),
    ],
)
async def test_get_auth_response_previous_trial() -> None:
    user_info = UserAccessInfo(
        first_name="Test",
        last_name="User",
        subject="test#user",
        user_email="user@test.com",
        verified=True,
    )
    request = Request(
        {
            "headers": {
                "User-Agent": "Mozilla/5.0 (Linux; x86_64) Firefox/115.0",
            },
            "method": "GET",
            "scheme": "https",
            "session": {
                "username": user_info.user_email,
                "session_key": random_uuid(),
            },
            "type": "http",
        }
    )
    response = await auth.get_auth_response(request, user_info)
    assert isinstance(response, RedirectResponse)
    assert response.headers["location"] == "/SignUp?error=previousTrial"


@parametrize(args=["otp_token"], cases=[[None], ["123456"]])
@mocks(
    aws=IntegratesAws(dynamodb=IntegratesDynamodb()),
)
async def test_redirect_auth_response(otp_token: str) -> None:
    user_info = UserAccessInfo(
        first_name="Test",
        last_name="User",
        subject="test#user",
        user_email="user@test.com",
        verified=True,
    )
    request = Request(
        {
            "headers": {
                "User-Agent": "Mozilla/5.0 (Linux; x86_64) Firefox/115.0",
            },
            "method": "GET",
            "scheme": "https",
            "session": {
                "username": user_info.user_email,
                "session_key": random_uuid(),
            },
            "type": "http",
        }
    )
    response = await auth.redirect_auth_response(
        request=request,
        user_info=user_info,
        otp_token=otp_token,
    )
    assert isinstance(response, RedirectResponse)
    assert response.headers["location"] == "/home"
