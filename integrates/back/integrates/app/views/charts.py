# Starlette charts views
from starlette.requests import (
    Request,
)
from starlette.responses import (
    Response,
)

from integrates.analytics import (
    domain as analytics_domain,
)
from integrates.custom_exceptions import (
    InvalidAuthorization,
)
from integrates.custom_utils import (
    templates,
)
from integrates.sessions import (
    domain as sessions_domain,
)


async def graphic(request: Request) -> Response:
    return await analytics_domain.handle_graphic_request(request)


async def graphic_csv(request: Request) -> Response:
    return await analytics_domain.handle_graphic_csv_request(request)


async def graphics_for_entity(entity: str, request: Request) -> Response:
    try:
        await sessions_domain.get_jwt_content(request)
    except InvalidAuthorization:
        return templates.login(request)
    response = await analytics_domain.handle_graphics_for_entity_request(
        entity=entity,
        request=request,
    )
    return response


async def graphics_for_group(request: Request) -> Response:
    return await graphics_for_entity("group", request)


async def graphics_for_organization(request: Request) -> Response:
    return await graphics_for_entity("organization", request)


async def graphics_for_portfolio(request: Request) -> Response:
    return await graphics_for_entity("portfolio", request)


async def graphics_report(request: Request) -> Response:
    return await analytics_domain.handle_graphics_report_request(request)
