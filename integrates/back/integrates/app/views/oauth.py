import logging
import logging.config
import uuid
from datetime import (
    datetime,
)
from enum import (
    Enum,
)
from urllib.parse import (
    urlencode,
)

from aioextensions import (
    collect,
)
from authlib.integrations.starlette_client import (
    OAuthError,
)
from httpx import (
    ConnectTimeout,
)
from starlette.datastructures import (
    QueryParams,
)
from starlette.requests import (
    Request,
)
from starlette.responses import (
    RedirectResponse,
    Response,
)
from urllib3.util.url import (
    parse_url,
)

from integrates.authz.enforcer import (
    get_organization_level_enforcer,
)
from integrates.batch import (
    dal as batch_dal,
)
from integrates.batch.enums import (
    Action,
    IntegratesBatchQueue,
)
from integrates.context import (
    FI_AZURE_OAUTH2_REPOSITORY_SECRET,
)
from integrates.custom_exceptions import (
    InvalidAuthorization,
)
from integrates.custom_utils.datetime import (
    get_minus_delta,
    get_plus_delta,
    get_utc_now,
)
from integrates.custom_utils.requests import (
    get_redirect_url,
)
from integrates.custom_utils.stakeholders import (
    get_full_name,
)
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model import (
    credentials as credentials_model,
)
from integrates.db_model.credentials.types import (
    Credentials,
    CredentialsMetadataToUpdate,
    CredentialsState,
    OauthAzureSecret,
    OauthBitbucketSecret,
    OauthGithubSecret,
    OauthGitlabSecret,
    RepoSecret,
)
from integrates.db_model.credentials.update import (
    update_credentials,
)
from integrates.db_model.enums import (
    CredentialType,
)
from integrates.db_model.groups.enums import (
    GroupLanguage,
    GroupService,
    GroupSubscriptionType,
)
from integrates.db_model.organizations.types import (
    Organization,
)
from integrates.groups import (
    domain as groups_domain,
)
from integrates.oauth import (
    OAUTH,
)
from integrates.oauth.azure import (
    get_azure_refresh_token,
)
from integrates.oauth.bitbucket import (
    get_bitbucket_refresh_token,
)
from integrates.oauth.github import (
    get_access_token,
)
from integrates.oauth.gitlab import (
    get_refresh_token,
)
from integrates.organization_access import (
    domain as orgs_access,
)
from integrates.organizations import (
    domain as orgs_domain,
)
from integrates.organizations.utils import (
    get_organization,
)
from integrates.organizations.validations import (
    get_owner_credentials_oauth,
    validate_credentials_name_in_organization,
    validate_credentials_oauth,
)
from integrates.sessions.domain import (
    get_jwt_content,
)
from integrates.settings import (
    LOGGING,
)
from integrates.stakeholders import (
    domain as stakeholders_domain,
)

logging.config.dictConfig(LOGGING)

LOGGER = logging.getLogger(__name__)

AZURE_REDIRECT_URI = "azure_repository_authlib_redirect_uri"


class RepoProvider(Enum):
    AZURE = "azure"
    BITBUCKET = "bitbucket"
    GITHUB = "github"
    GITLAB = "gitlab"


async def _validate(
    *,
    loaders: Dataloaders,
    email: str,
    organization_id: str,
) -> None:
    enforcer = await get_organization_level_enforcer(loaders, email)
    if not enforcer(
        organization_id,
        "integrates_api_mutations_add_credentials_mutate",
    ) or not await orgs_access.has_access(
        loaders=loaders,
        email=email,
        organization_id=organization_id,
    ):
        raise PermissionError("Access denied")


async def _put_action(
    *,
    organization_id: str,
    credentials_id: str,
    user_email: str,
) -> None:
    await batch_dal.put_action(
        action=Action.UPDATE_ORGANIZATION_REPOSITORIES,
        queue=IntegratesBatchQueue.SMALL,
        additional_info={"credentials_id": credentials_id},
        entity=organization_id.lower().lstrip("org#"),
        attempt_duration_seconds=14400,
        subject=user_email,
    )


async def get_authorized_redirect(request: Request, provider: RepoProvider) -> Response:
    redirect_uri = get_redirect_url(request, f"oauth_{provider.value}")
    url = f"{redirect_uri}?{urlencode(request.query_params)}"
    oauth_client = OAUTH.create_client(provider.value)

    if provider == RepoProvider.AZURE:
        # Azure does not support propagating query params
        request.session[AZURE_REDIRECT_URI] = url
        return await oauth_client.authorize_redirect(request, redirect_uri)

    return await oauth_client.authorize_redirect(request, url)


async def _begin_repo_oauth(request: Request, provider: RepoProvider) -> Response:
    if request.query_params.get("fast_track"):
        return await get_authorized_redirect(request, provider)

    loaders = get_new_context()
    organization_id: str = request.query_params["subject"]
    user_info = await get_jwt_content(request)
    email: str = user_info["user_email"]
    try:
        await _validate(loaders=loaders, email=email, organization_id=organization_id)
    except PermissionError:
        LOGGER.warning(
            "Attempted to add oauth repos from %s without permission",
            provider.name,
            extra={
                "extra": {
                    "user_email": email,
                    "organization_id": organization_id,
                },
            },
        )
        return RedirectResponse(url="/home")

    return await get_authorized_redirect(request, provider)


async def do_azure_oauth(request: Request) -> Response:
    return await _begin_repo_oauth(request, RepoProvider.AZURE)


async def do_bitbucket_oauth(request: Request) -> Response:
    return await _begin_repo_oauth(request, RepoProvider.BITBUCKET)


async def do_github_oauth(request: Request) -> Response:
    return await _begin_repo_oauth(request, RepoProvider.GITHUB)


async def do_gitlab_oauth(request: Request) -> Response:
    return await _begin_repo_oauth(request, RepoProvider.GITLAB)


async def _get_azure_secret(request: Request) -> OauthAzureSecret:
    code: str = request.query_params["code"]
    redirect = get_redirect_url(request, "oauth_azure")
    token_data = await get_azure_refresh_token(
        code=code,
        redirect_uri=redirect,
        secret=FI_AZURE_OAUTH2_REPOSITORY_SECRET,
    )

    if not token_data:
        raise OAuthError()

    return OauthAzureSecret(
        arefresh_token=token_data["refresh_token"],
        redirect_uri=redirect,
        access_token=token_data["access_token"],
        valid_until=get_plus_delta(
            get_minus_delta(get_utc_now(), seconds=60),
            seconds=int(token_data["expires_in"]),
        ),
    )


async def _get_bitbucket_secret(request: Request) -> OauthBitbucketSecret:
    code: str = request.query_params["code"]
    redirect = get_redirect_url(request, "oauth_bitbucket")
    params = {
        key: value
        for key, value in request.query_params.items()
        if key in {"fast_track", "subject"}
    }
    uri = f"{redirect}?{urlencode(params)}"
    token_data = await get_bitbucket_refresh_token(code=code, redirect_uri=uri)

    if not token_data:
        raise OAuthError()

    return OauthBitbucketSecret(
        brefresh_token=token_data["refresh_token"],
        access_token=token_data["access_token"],
        valid_until=get_plus_delta(
            get_minus_delta(get_utc_now(), seconds=60),
            seconds=int(token_data["expires_in"]),
        ),
    )


async def _get_github_secret(request: Request) -> OauthGithubSecret:
    code: str = request.query_params["code"]
    token = await get_access_token(code=code)

    if not token:
        raise OAuthError()

    return OauthGithubSecret(access_token=token)


async def _get_gitlab_secret(request: Request) -> OauthGitlabSecret:
    code: str = request.query_params["code"]
    redirect = get_redirect_url(request, "oauth_gitlab")
    params = {
        key: value
        for key, value in request.query_params.items()
        if key in {"fast_track", "subject"}
    }
    uri = f"{redirect}?{urlencode(params)}"
    token_data = await get_refresh_token(
        code=code,
        redirect_uri=uri,
        code_verifier=request.session.get(f'_state_gitlab_{request.query_params["state"]}', {})
        .get("data", {})
        .get("code_verifier", ""),
    )

    if not token_data:
        raise OAuthError()

    return OauthGitlabSecret(
        refresh_token=token_data["refresh_token"],
        redirect_uri=uri,
        access_token=token_data["access_token"],
        valid_until=get_plus_delta(
            datetime.utcfromtimestamp(token_data["created_at"]),
            seconds=token_data["expires_in"],
        ),
    )


def _get_params_from_uri(uri: str) -> QueryParams:
    return QueryParams(str(parse_url(uri).query))


def _sanitize(name: str) -> str:
    return "".join([char for char in name if char.isalpha()])


def _generate_name(email: str) -> str:
    base_name = email.split("@")[0]
    domain_name = email.split("@")[1].split(".")[0]
    result = "".join([_sanitize(base_name)[:5], _sanitize(domain_name)[:10]])
    return result.lower()


async def _get_fast_track_org(request: Request) -> Organization | None:
    loaders = get_new_context()
    user_info = await get_jwt_content(request)
    email = user_info["user_email"]
    name = _generate_name(email)
    organization: Organization | None = None
    organization_id: str = ""
    if org_access := await loaders.stakeholder_organizations_access.load(email):
        organization_id = org_access[0].organization_id
    else:
        organization = await orgs_domain.add_organization(
            loaders=loaders,
            organization_name=name,
            email=email,
            country=request.headers.get("cf-ipcountry", "Undefined"),
        )
    organization_id = organization.id if organization else organization_id
    organization = (
        organization if organization else await loaders.organization.load(organization_id)
    )
    await groups_domain.add_group(
        loaders=loaders,
        description="Trial group",
        email=email,
        group_name=name,
        has_essential=True,
        has_advanced=False,
        language=GroupLanguage.EN,
        organization_name=organization_id,
        service=GroupService.WHITE,
        subscription=GroupSubscriptionType.CONTINUOUS,
    )
    await stakeholders_domain.add_enrollment(
        loaders=get_new_context(),
        user_email=email,
        full_name=get_full_name(user_info),
    )
    return organization


async def _get_organization_id(request: Request, provider: RepoProvider) -> str:
    try:
        if request.query_params.get("fast_track"):
            org = await _get_fast_track_org(request)
            return org.id if org else ""

        if provider == RepoProvider.AZURE:
            # Azure does not support propagating query params
            azure_uri = request.session.pop(AZURE_REDIRECT_URI)
            params_azure_uri = _get_params_from_uri(azure_uri)
            if params_azure_uri.get("fast_track"):
                org = await _get_fast_track_org(request)
                return org.id if org else ""

            return params_azure_uri["subject"]

        return request.query_params["subject"]
    except (KeyError, InvalidAuthorization) as ex:
        LOGGER.exception(ex, extra={"extra": locals()})
        raise ex


async def _get_secret(*, provider: RepoProvider, request: Request) -> RepoSecret:
    secret_functions = {
        RepoProvider.AZURE: _get_azure_secret,
        RepoProvider.BITBUCKET: _get_bitbucket_secret,
        RepoProvider.GITHUB: _get_github_secret,
        RepoProvider.GITLAB: _get_gitlab_secret,
    }[provider]
    try:
        return await secret_functions(request)  # type: ignore
    except (ConnectTimeout, KeyError, OAuthError) as ex:
        LOGGER.exception(ex, extra={"extra": locals()})
        raise ex


async def _add_credentials(*, loaders: Dataloaders, credential: Credentials) -> None:
    await collect(
        (
            validate_credentials_name_in_organization(
                loaders,
                credential.organization_id,
                credential.state.name,
            ),
            validate_credentials_oauth(
                loaders,
                credential.organization_id,
                credential.state.owner,
                type(credential.secret),  # type: ignore
            ),
        ),
    )
    await credentials_model.add(credential=credential)


async def _store_credentials(
    *,
    credentials_id: str,
    loaders: Dataloaders,
    organization_id: str,
    provider: RepoProvider,
    secret: RepoSecret,
    user_email: str,
) -> None:
    provider_name = {
        RepoProvider.AZURE: "Azure OAuth",
        RepoProvider.BITBUCKET: "Bitbucket OAuth",
        RepoProvider.GITHUB: "GitHub OAuth",
        RepoProvider.GITLAB: "GitLab OAuth",
    }[provider]

    name = f'{user_email.split("@", maxsplit=1)[0]}' + f"({provider_name})"

    credential = Credentials(
        id=credentials_id,
        organization_id=organization_id,
        state=CredentialsState(
            modified_by=user_email,
            modified_date=get_utc_now(),
            name=name,
            type=CredentialType.OAUTH,
            is_pat=False,
            azure_organization=None,
            owner=user_email,
        ),
        secret=secret,
    )

    exist_credential = await get_owner_credentials_oauth(
        loaders,
        credential.organization_id,
        credential.state.owner,
        type(secret),
    )
    if exist_credential:
        await update_credentials(
            current_value=exist_credential,
            organization_id=exist_credential.organization_id,
            credential_id=exist_credential.id,
            state=credential.state,
            metadata=CredentialsMetadataToUpdate(secret=exist_credential.secret),
        )
    else:
        await _add_credentials(loaders=loaders, credential=credential)


async def _end_repo_oauth(request: Request, provider: RepoProvider) -> RedirectResponse:
    loaders = get_new_context()

    try:
        user_info = await get_jwt_content(request)
        user_email = user_info["user_email"]
        if "code" not in request.query_params:
            LOGGER.warning(
                "Declined consent to access the app on %s",
                provider.name,
                extra={"extra": {"user_email": user_email}},
            )
            return RedirectResponse(url="/home")
        organization_id = await _get_organization_id(request, provider)

        await _validate(
            loaders=loaders,
            email=user_email,
            organization_id=organization_id,
        )

        secret = await _get_secret(request=request, provider=provider)
        credentials_id = str(uuid.uuid4())
        await _store_credentials(
            credentials_id=credentials_id,
            loaders=loaders,
            organization_id=organization_id,
            provider=provider,
            secret=secret,
            user_email=user_email,
        )

        await _put_action(
            organization_id=organization_id,
            credentials_id=credentials_id,
            user_email=user_email,
        )

        if request.query_params.get("fast_track"):
            return RedirectResponse(url="/home?fast_track_end=true")

        organization = await get_organization(loaders, organization_id)
        return RedirectResponse(url=f"/orgs/{organization.name}/credentials")
    except (ConnectTimeout, InvalidAuthorization, KeyError, OAuthError) as ex:
        LOGGER.error("Couldn't complete repo oauth", extra={"extra": {"exception": ex}})
        return RedirectResponse(url="/home")


async def oauth_azure(request: Request) -> RedirectResponse:
    return await _end_repo_oauth(request, RepoProvider.AZURE)


async def oauth_bitbucket(request: Request) -> RedirectResponse:
    return await _end_repo_oauth(request, RepoProvider.BITBUCKET)


async def oauth_github(request: Request) -> RedirectResponse:
    return await _end_repo_oauth(request, RepoProvider.GITHUB)


async def oauth_gitlab(request: Request) -> RedirectResponse:
    return await _end_repo_oauth(request, RepoProvider.GITLAB)
