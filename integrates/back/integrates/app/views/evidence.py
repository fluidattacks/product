# Starlette evidences-related methods


import logging
import logging.config
from collections.abc import (
    Sequence,
)

import aiofiles
import aiohttp
from magic import (
    Magic,
)
from starlette.requests import (
    Request,
)
from starlette.responses import (
    JSONResponse,
    Response,
)

from integrates import (
    authz,
)
from integrates.custom_exceptions import (
    EventNotFound,
    FindingNotFound,
    InvalidAuthorization,
)
from integrates.custom_utils import (
    files as files_utils,
)
from integrates.custom_utils import (
    logs as logs_utils,
)
from integrates.custom_utils import (
    templates,
    utils,
)
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.decorators import (
    retry_on_exceptions,
)
from integrates.events.domain import (
    has_access_to_event,
)
from integrates.findings.domain import (
    has_access_to_finding,
)
from integrates.s3.operations import (
    download_file,
    list_files,
)
from integrates.sessions import (
    domain as sessions_domain,
)
from integrates.settings.logger import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)
LOGGER = logging.getLogger(__name__)


download_evidence_file = retry_on_exceptions(
    exceptions=(
        aiohttp.ClientError,
        aiohttp.ClientPayloadError,
    ),
    max_attempts=3,
    sleep_seconds=float("0.2"),
)(download_file)


async def enforce_group_level_role(
    loaders: Dataloaders,
    request: Request,
    group: str,
    *allowed_roles: Sequence[str],
) -> Response | None:
    response = None
    try:
        user_info = await sessions_domain.get_jwt_content(request)
    except InvalidAuthorization:
        return Response("Access denied", status_code=403)
    email = user_info["user_email"]
    requester_role = await authz.get_group_level_role(loaders, email, group)
    if requester_role not in allowed_roles:
        response = Response("Access denied", status_code=403)
    return response


async def _has_access_to_evidence(
    loaders: Dataloaders,
    email: str,
    finding_id: str,
    evidence_type: str,
    group_name: str,
) -> bool:
    try:
        return (
            evidence_type in ["drafts", "findings", "vulns"]
            and await has_access_to_finding(loaders, email, finding_id)
        ) or (
            evidence_type == "events"
            and await has_access_to_event(loaders, email, finding_id, group_name)
        )
    except (FindingNotFound, EventNotFound) as exc:
        LOGGER.exception(
            exc,
            extra={"evidence type": evidence_type, "finding_id": finding_id},
        )
        return False


async def get_evidence(
    request: Request,
    evidence_type: str,
) -> Response:
    try:
        user_info = await sessions_domain.get_jwt_content(request)
    except InvalidAuthorization:
        return templates.login(request)
    loaders: Dataloaders = get_new_context()
    group_name = request.path_params["group_name"]
    finding_id = request.path_params["finding_id"]
    file_id = request.path_params["file_id"]

    allowed_roles = [
        "admin",
        "architect",
        "customer_manager",
        "group_manager",
        "hacker",
        "reattacker",
        "resourcer",
        "reviewer",
        "user",
        "vulnerability_manager",
    ]
    error = await enforce_group_level_role(loaders, request, group_name, *allowed_roles)
    if error is not None:
        return error
    if await _has_access_to_evidence(
        loaders,
        user_info["user_email"],
        finding_id,
        evidence_type,
        group_name,
    ):
        if file_id is None:
            return Response("Error - Unsent image ID", media_type="text/html")

        evidences_path: str = f"evidences/{group_name.lower()}/{finding_id}/{file_id}"
        evidences = await list_s3_evidences(evidences_path)
        if evidences:
            for evidence in evidences:
                start = evidence.find(finding_id) + len(finding_id)
                localfile = f"/tmp{evidence[start:]}"  # noqa: S108
                localtmp = utils.replace_all(
                    localfile,
                    {".png": ".tmp", ".gif": ".tmp", ".webm": ".tmp"},
                )
                await download_evidence_file(evidence, localtmp)
                return await retrieve_image(localtmp)
        else:
            return JSONResponse(
                {
                    "data": [],
                    "message": "Access denied or evidence not found",
                    "error": True,
                },
            )

    logs_utils.cloudwatch_log(
        request,
        "Attempted to retrieve evidence without permission",
        extra={"log_type": "Security"},
    )
    return JSONResponse({"data": [], "message": "Evidence type not found", "error": True})


async def get_event_evidence(
    request: Request,
) -> Response:
    return await get_evidence(request, "events")


async def get_finding_evidence(
    request: Request,
) -> Response:
    return await get_evidence(request, "findings")


async def list_s3_evidences(prefix: str) -> list[str]:
    return list(await list_files(prefix))


async def retrieve_image(img_file: str) -> Response:
    if files_utils.assert_file_mime(
        img_file,
        ["image/png", "image/jpeg", "image/gif", "video/webm"],
    ):
        async with aiofiles.open(img_file, "rb") as file_obj:
            mime = Magic(mime=True)
            mime_type = mime.from_file(img_file)
            return Response(await file_obj.read(), media_type=mime_type)
    else:
        return Response("Error: Invalid evidence image format", media_type="text/html")
