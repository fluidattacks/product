from collections import defaultdict

from starlette.middleware.base import BaseHTTPMiddleware, RequestResponseEndpoint
from starlette.requests import Request
from starlette.responses import Response

from integrates.audit import AuditContext, add_audit_context

HEADERS = {
    "server": "None",
    "Accept-Encoding": "identity",
    "Cross-Origin-Opener-Policy": "same-origin",
    "Referrer-Policy": "strict-origin-when-cross-origin",
    "WWW-Authenticate": ('OAuth realm="Access to FLUIDIntegrates" charset="UTF-8"'),
    "X-Permitted-Cross-Domain-Policies": "none",
    "X-Content-Type-Options": "nosniff",
    "X-XSS-Protection": "0",
    "Cache-Control": "must-revalidate, no-cache, no-store",
    "Content-Security-Policy": (
        "script-src "
        "'self' "
        "'unsafe-inline' "
        "localhost:* "
        "cdn.announcekit.app "
        "bam-cell.nr-data.net "
        "bam.nr-data.net "
        "unpkg.com/react@18.3.0/ "
        "unpkg.com/react-dom@18.3.0/ "
        "unpkg.com/graphiql/ "
        "https://unpkg.com/graphiql@3.0.6/ "
        "https://unpkg.com/@graphiql/plugin-explorer@0.3.5/ "
        "unpkg.com/@graphiql/plugin-explorer/ "
        "cdn.jsdelivr.net/npm/ "
        "d2yyd1h5u9mauk.cloudfront.net "
        "cdnjs.cloudflare.com "
        "integrates.front.fluidattacks.com "
        "https://connect-cdn.atl-paas.net "
        "*.cookiebot.com "
        "*.pagesense.io "
        "*.zdassets.com "
        "*.mxpnl.com "
        "*.pingdom.net "
        "*.cloudflareinsights.com "
        "https://kit.fontawesome.com/ "
        "https://salesiq.zoho.com/widget "
        "https://*.zohostatic.com/ "
        "https://*.zohocdn.com/ "
        "https://js.stripe.com; "
        "frame-ancestors "
        "'self' "
        "*.atlassian.net; "
        "object-src "
        "'none'; "
        "upgrade-insecure-requests;"
        "worker-src "
        "'self' "
        "blob:;"
    ),
    "Permissions-Policy": (
        "geolocation=(self), "
        "midi=(self), "
        "push=(self), "
        "sync-xhr=(self), "
        "microphone=(self), "
        "camera=(self), "
        "magnetometer=(self), "
        "gyroscope=(self), "
        "speaker=(self), "
        "vibrate=(self), "
        "fullscreen=(self), "
        "payment=(self) "
    ),
}


class CustomRequestMiddleware(BaseHTTPMiddleware):
    async def dispatch(self, request: Request, call_next: RequestResponseEndpoint) -> Response:
        request.state.store = defaultdict(lambda: None)
        add_audit_context(
            AuditContext(
                author_ip=request.headers.get(
                    "cf-connecting-ip",
                    request.client.host if request.client else None,
                ),
                author_user_agent=request.headers.get("user-agent"),
                mechanism="WEB",
            )
        )
        response = await call_next(request)
        response.headers.update(HEADERS)

        return response
