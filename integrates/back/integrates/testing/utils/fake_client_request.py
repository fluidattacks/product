import asyncio
from types import SimpleNamespace
from typing import Any, Optional, no_type_check


def create_fake_client_request(  # type: ignore[misc]
    method: str,
    url: str,
    headers: Optional[dict[str, str]] = None,
    query_params: Optional[dict[str, str]] = None,
    json_data: Optional[dict[str, Any]] = None,
    form_data: Optional[dict[str, str]] = None,
) -> SimpleNamespace:
    """Creates a fake client request object to http://localhost:8001."""
    headers = headers or {}
    query_params = query_params or {}
    json_data = json_data or {}  # type: ignore[misc]
    form_data = form_data or {}

    async def json() -> dict[str, Any]:  # type: ignore[misc]
        await asyncio.sleep(0)
        return json_data  # type: ignore[misc]

    async def form() -> dict[str, str]:
        await asyncio.sleep(0)
        return form_data

    @no_type_check
    def url_for(endpoint: str, **values: Any) -> str:
        base_url = "http://localhost:8001"
        url = f"{base_url}/{endpoint}"
        if values:
            query_string = "&".join(f"{key}={value}" for key, value in values.items())
            url = f"{url}?{query_string}"
        return url

    return SimpleNamespace(
        method=method,
        url=url,
        url_for=url_for,
        headers=headers,
        query_params=query_params,
        json=json,  # type: ignore[misc]
        form=form,
    )
