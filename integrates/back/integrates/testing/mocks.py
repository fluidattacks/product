import functools
from collections.abc import Awaitable, Callable, Iterator
from contextlib import contextmanager
from types import ModuleType
from typing import Literal, NamedTuple, ParamSpec, TypeVar

import pytest

from integrates.testing.aws import IntegratesAws, populate

T = TypeVar("T")
P = ParamSpec("P")
TargetType = Literal["sync", "async", "managed", "function"]


class Mock(NamedTuple):
    module: ModuleType
    target: str
    target_type: TargetType
    expected: object


def _mock(monkeypatch: pytest.MonkeyPatch, mock: Mock) -> None:
    def _sync(*_args: P.args, **_kwargs: P.kwargs) -> object:
        return mock.expected

    async def _async(*_args: P.args, **_kwargs: P.kwargs) -> object:
        return mock.expected

    @contextmanager
    def _managed_function() -> Iterator[object]:
        yield mock.expected

    match mock.target_type:
        case "sync":
            monkeypatch.setattr(mock.module, mock.target, _sync)
        case "async":
            monkeypatch.setattr(mock.module, mock.target, _async)
        case "function":
            monkeypatch.setattr(mock.module, mock.target, mock.expected)
        case _:
            monkeypatch.setattr(mock.module, mock.target, _managed_function)


def mocks(
    *,
    aws: IntegratesAws = IntegratesAws(),
    others: list[Mock] = [],
) -> Callable[[Callable[P, Awaitable[T]]], Callable[P, Awaitable[T]]]:
    """
    Decorator to include mocks fo the test function.

    ```python
    @mocks(
        aws=IntegratesAws(
            dynamodb=...,
            s3=...,
        ),
        others=[
            Mock(module, target, target_type, expected),
            ...
        ],
    )
    ```
    """

    def decorator(func: Callable[P, Awaitable[T]]) -> Callable[P, Awaitable[T]]:
        @functools.wraps(func)
        async def wrapper(*args: P.args, **kwargs: P.kwargs) -> T:
            async with populate(func, aws):
                with pytest.MonkeyPatch().context() as monkeypatch:  # type: ignore[misc]
                    for mock in others:
                        _mock(monkeypatch, mock)  # type: ignore[misc]
                    return await func(*args, **kwargs)

        return wrapper

    return decorator
