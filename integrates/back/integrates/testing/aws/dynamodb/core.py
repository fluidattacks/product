from aioextensions import (
    collect,
)

from integrates.dynamodb.resource import (
    get_resource,
)
from integrates.testing.aws.dynamodb import (
    populate,
)
from integrates.testing.aws.dynamodb.types import (
    DynamoDbTable,
    DynamoDbTableName,
    IntegratesDynamodb,
    TableSchemas,
)


async def _create_table(table_name: DynamoDbTableName) -> DynamoDbTable:
    resource = await get_resource()
    await resource.create_table(**TableSchemas[table_name])

    if table_name == "integrates_vms_historic":
        client = resource.meta.client
        await client.update_time_to_live(
            TableName=table_name,
            TimeToLiveSpecification={
                "Enabled": True,
                "AttributeName": "expiration_time",
            },
        )

    return await resource.Table(table_name)


async def init(
    *,
    dynamodb: IntegratesDynamodb = IntegratesDynamodb(),
) -> None:
    await collect(
        [
            _create_table("fi_async_processing"),
            _create_table("integrates_vms_historic"),
            _create_table("integrates_vms"),
            _create_table("llm_scan"),
        ],
    )

    await populate.main(dynamodb)
