from typing import NamedTuple

from integrates.testing.aws.dynamodb.types import IntegratesDynamodb
from integrates.testing.aws.opensearch.types import IntegratesOpensearch
from integrates.testing.aws.s3.types import IntegratesS3


class IntegratesAws(NamedTuple):
    dynamodb: IntegratesDynamodb = IntegratesDynamodb()
    s3: IntegratesS3 = IntegratesS3()
    opensearch: IntegratesOpensearch = IntegratesOpensearch()
