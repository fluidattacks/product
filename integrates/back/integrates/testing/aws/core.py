import os
from collections.abc import AsyncGenerator, Awaitable, Callable
from contextlib import asynccontextmanager
from typing import ParamSpec, TypeVar

from moto import mock_aws

from integrates.testing.aws.dynamodb import init as init_dynamodb
from integrates.testing.aws.opensearch import init as init_opensearch
from integrates.testing.aws.patch import patch_aio, patch_opensearch
from integrates.testing.aws.s3 import init as init_s3
from integrates.testing.aws.types import IntegratesAws

T = TypeVar("T")
P = ParamSpec("P")


async def _set_credentials() -> None:
    os.environ["AWS_ACCESS_KEY_ID"] = "test"
    os.environ["AWS_SECRET_ACCESS_KEY"] = "test"  # noqa: S105
    os.environ["AWS_SESSION_TOKEN"] = "test"  # noqa: S105
    os.environ["AWS_SECURITY_TOKEN"] = "test"  # noqa: S105
    os.environ["AWS_DEFAULT_REGION"] = "us-east-1"


@asynccontextmanager
async def populate(
    func: Callable[P, Awaitable[T]],
    aws: IntegratesAws = IntegratesAws(),
) -> AsyncGenerator[None, None]:
    with (
        patch_aio(),
        patch_opensearch(),
        mock_aws(),
    ):
        await _set_credentials()
        await init_dynamodb(dynamodb=aws.dynamodb)
        await init_s3(aws.s3, func)
        await init_opensearch(aws.opensearch)
        yield
