# mypy: ignore-errors
import aioboto3
from types_aiobotocore_dynamodbstreams import DynamoDBStreamsClient

from integrates.testing.aws.opensearch import populate as opensearch_populate
from integrates.testing.aws.opensearch.libs.streams_types import DDBRecord
from integrates.testing.aws.opensearch.types import IntegratesOpensearch


async def _get_events_from(streams: DynamoDBStreamsClient, table_name: str) -> list[DDBRecord]:
    result = await streams.list_streams()
    table_streams = [elem for elem in result["Streams"] if elem["TableName"] == table_name]
    stream_arns = [elem["StreamArn"] for elem in table_streams]

    shards = {
        arn: [shard["ShardId"] for shard in stream["StreamDescription"]["Shards"]]
        for arn in stream_arns
        if (stream := await streams.describe_stream(StreamArn=arn))
    }

    all_events = []
    for arn, ids in shards.items():
        for id in ids:
            iterator = await streams.get_shard_iterator(
                StreamArn=arn,
                ShardId=id,
                ShardIteratorType="TRIM_HORIZON",
            )
            events = await streams.get_records(ShardIterator=iterator["ShardIterator"], Limit=1000)
            all_events.extend(events["Records"])

    return all_events


async def init(opensearch: IntegratesOpensearch) -> None:
    if opensearch.autoload:
        async with aioboto3.Session().client("dynamodbstreams") as streams_client:
            events = await _get_events_from(streams_client, "integrates_vms")
            await opensearch_populate.main(events)
