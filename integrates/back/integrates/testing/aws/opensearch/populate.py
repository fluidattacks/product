# mypy: ignore-errors
from opensearchpy import AsyncOpenSearch

from integrates.search.client import get_client
from integrates.testing.aws.opensearch.libs.streams_types import DDBRecord, Record, StreamEvent
from integrates.testing.aws.opensearch.libs.streams_utils import (
    format_findings,
    format_record,
    format_vulns,
)


async def index_by_prefix(
    client: AsyncOpenSearch, records: list[Record], index: str, pk_prefix: str, sk_prefix: str
) -> None:
    for record in records:
        if (
            record.event_name != StreamEvent.REMOVE
            and record.pk.startswith(pk_prefix)
            and record.sk.startswith(sk_prefix)
        ):
            await client.index(
                index=index,
                id="#".join([record.pk, record.sk])[:512],
                body=record.new_image,
            )


async def main(raw_records: list[DDBRecord]) -> None:
    client = await get_client()
    records = [format_record(record) for record in raw_records]

    await index_by_prefix(client, records, "events_index", "EVENT#", "GROUP#")
    await index_by_prefix(client, records, "executions_index", "EXEC#", "GROUP#")
    await index_by_prefix(client, format_findings(records), "findings_index", "FIN#", "GROUP#")
    await index_by_prefix(client, records, "inputs_index", "GROUP#", "INPUTS#")
    await index_by_prefix(client, records, "lines_index", "GROUP#", "LINES#")
    await index_by_prefix(client, records, "packages_index", "GROUP#", "PACKAGES#")
    await index_by_prefix(client, records, "pkgs_index", "GROUP#", "PKGS#")
    await index_by_prefix(client, records, "roots_index", "ROOT#", "GROUP#")
    await index_by_prefix(client, format_vulns(records), "vulns_index", "VULN#", "FIN#")
