from typing import NamedTuple


class IntegratesOpensearch(NamedTuple):
    autoload: bool = False
    """
    Defaults to False. When it's True, events for `integrates_vms` table will be uploaded
    to mocked Opensearch.
    """
