# ruff: noqa
# mypy: ignore-errors
"""
Original source code from https://github.com/matthewdeanmartin/openmock/blob/main/openmock/fake_asyncopensearch.py

This modified AsyncOpenSearch fixes the errors related to no awaited coroutines.

This module provides a fake implementation of the OpenSearch client that can be used for testing
purposes.
"""

# pylint: disable=duplicate-code

import datetime
import json
from collections import defaultdict
from typing import Any, Optional

import opensearchpy
from openmock.behaviour.server_failure import server_failure
from openmock.fake_cluster import FakeClusterClient
from openmock.fake_indices import FakeIndicesClient
from openmock.fake_opensearch import FakeQueryCondition, MetricType, QueryType
from openmock.normalize_hosts import _normalize_hosts
from openmock.utilities import extract_ignore_as_iterable, get_random_id, get_random_scroll_id
from openmock.utilities.decorator import for_all_methods
from opensearchpy import AsyncTransport
from opensearchpy.client.utils import query_params
from opensearchpy.exceptions import ConflictError, NotFoundError, RequestError


@for_all_methods([server_failure])
class AsyncFakeOpenSearch(opensearchpy.AsyncOpenSearch):
    # pylint: disable=super-init-not-called
    def __init__(self, hosts=None, transport_class=None, **kwargs):
        self._FakeIndicesClient__documents_dict = {}
        self.__scrolls = {}
        self.transport = AsyncTransport(_normalize_hosts(hosts), **kwargs)

    @property
    def __documents_dict(self):
        return self._FakeIndicesClient__documents_dict

    @property
    def indices(self):
        return FakeIndicesClient(self)

    @property
    def cluster(self):
        return FakeClusterClient(self)

    @query_params()
    async def ping(self, params=None, headers=None):
        return True

    @query_params()
    async def info(self, params=None, headers=None):
        return {
            "status": 200,
            "cluster_name": "openmock",
            "version": {
                "lucene_version": "4.10.4",
                "build_hash": "00f95f4ffca6de89d68b7ccaf80d148f1f70e4d4",
                "number": "1.7.5",
                "build_timestamp": "2016-02-02T09:55:30Z",
                "build_snapshot": False,
            },
            "name": "Nightwatch",
            "tagline": "You Know, for Search",
        }

    @query_params(
        "consistency",
        "op_type",
        "parent",
        "refresh",
        "replication",
        "routing",
        "timeout",
        "timestamp",
        "ttl",
        "version",
        "version_type",
    )
    async def create(
        self,
        index: Any,
        id: Any,
        body: Any,
        params: Any = None,
        headers: Any = None,
    ) -> Any:
        doc_type = "_doc"
        if await self.exists(index, id, doc_type=doc_type, params=params):
            raise ConflictError(
                409,
                "action_request_validation_exception",
                "Validation Failed: 1: no documents to get;",
            )

        if index not in self.__documents_dict:
            self.__documents_dict[index] = []

        custom_id = id or get_random_id()

        self.__documents_dict[index].append(
            {
                "_type": doc_type,
                "_id": custom_id,
                "_source": body,
                "_index": index,
                "_version": 1,
            }
        )

        return {
            "_type": doc_type,
            "_id": custom_id,
            "created": True,
            "_version": 1,
            "_index": index,
            "result": "created",
        }

    @query_params(
        "consistency",
        "op_type",
        "parent",
        "refresh",
        "replication",
        "routing",
        "timeout",
        "timestamp",
        "ttl",
        "version",
        "version_type",
    )
    async def index(
        self,
        index: Any,
        body: Any,
        id: Any = None,
        params: Any = None,
        headers: Any = None,
        **kwargs,
    ) -> Any:
        doc_type = "_doc"
        if index not in self.__documents_dict:
            self.__documents_dict[index] = []

        version = 1

        result = "created"
        custom_id = id or get_random_id()

        if await self.exists(index, id, doc_type=doc_type, params=params):
            doc = await self.get(index, id, doc_type=doc_type, params=params)
            version = doc["_version"] + 1
            await self.delete(index, id, doc_type=doc_type)
            result = "updated"

        document = {
            "_type": doc_type,
            "_id": custom_id,
            "_source": body,
            "_index": index,
            "_version": version,
        }

        self.__documents_dict[index].append(document)

        return {
            "_type": doc_type,
            "_id": custom_id,
            "created": True,
            "_version": version,
            "_index": index,
            "result": result,
        }

    async def _process_bulk_line(self, params: Any, line: dict) -> tuple[dict, bool]:
        if any(action in line for action in ["index", "create", "update", "delete"]):
            action = next(iter(line.keys()))

            version = 1
            index = line[action].get("_index") or index
            doc_type = line[action].get("_type", "_doc")  # _type is deprecated in 7.x

            document_id = line[action].get("_id", get_random_id())

            if action == "delete":
                status, result, error = await self._validate_action(
                    action, index, document_id, doc_type, params=params
                )
                item = {
                    action: {
                        "_type": doc_type,
                        "_id": document_id,
                        "_index": index,
                        "_version": version,
                        "status": status,
                    }
                }
                if error:
                    errors = True
                    item[action]["error"] = result
                else:
                    await self.delete(index, document_id, doc_type=doc_type, params=params)
                    item[action]["result"] = result
                return item, errors

            if index not in self.__documents_dict:
                self.__documents_dict[index] = []
        else:
            if "doc" in line and action == "update":
                source = line["doc"]
            else:
                source = line
            status, result, error = await self._validate_action(
                action, index, document_id, doc_type, params=params
            )
            item = {
                action: {
                    "_type": doc_type,
                    "_id": document_id,
                    "_index": index,
                    "_version": version,
                    "status": status,
                }
            }
            if not error:
                item[action]["result"] = result
                if await self.exists(index, document_id, doc_type=doc_type, params=params):
                    doc = await self.get(index, document_id, doc_type=doc_type, params=params)
                    version = doc["_version"] + 1
                    await self.delete(index, document_id, doc_type=doc_type, params=params)

                self.__documents_dict[index].append(
                    {
                        "_type": doc_type,
                        "_id": document_id,
                        "_source": source,
                        "_index": index,
                        "_version": version,
                    }
                )
            else:
                errors = True
                item[action]["error"] = result
            return item, errors

    @query_params(
        "consistency",
        "op_type",
        "parent",
        "refresh",
        "replication",
        "routing",
        "timeout",
        "timestamp",
        "ttl",
        "version",
        "version_type",
    )
    async def bulk(
        self,
        body: Any,
        index: Any = None,
        params: Any = None,
        headers: Any = None,
    ) -> Any:
        doc_type = None
        items = []
        errors = False

        for raw_line in body.splitlines():
            if len(raw_line.strip()) > 0:
                line = json.loads(raw_line)
                item, errors = await self._process_bulk_line(line)
                if item:
                    items.append(item)

        return {"errors": errors, "items": items}

    async def _validate_action(self, action, index, document_id, doc_type, params=None):
        if action in ["index", "update"] and await self.exists(
            index, id=document_id, doc_type=doc_type, params=params
        ):
            return 200, "updated", False
        if action == "create" and await self.exists(
            index, id=document_id, doc_type=doc_type, params=params
        ):
            return 409, "version_conflict_engine_exception", True
        if action in ["index", "create"] and not await self.exists(
            index, id=document_id, doc_type=doc_type, params=params
        ):
            return 201, "created", False
        if action == "delete" and await self.exists(
            index, id=document_id, doc_type=doc_type, params=params
        ):
            return 200, "deleted", False
        if action == "update" and not await self.exists(
            index, id=document_id, doc_type=doc_type, params=params
        ):
            return 404, "document_missing_exception", True
        if action == "delete" and not await self.exists(
            index, id=document_id, doc_type=doc_type, params=params
        ):
            return 404, "not_found", True
        raise NotImplementedError(f"{action} behaviour hasn't been implemented")

    @query_params("parent", "preference", "realtime", "refresh", "routing")
    async def exists(
        self,
        index: Any,
        id: Any,
        params: Any = None,
        headers: Any = None,
        **kwargs,
    ) -> Any:
        return (
            any(document.get("_id") == id for document in self.__documents_dict[index])
            if index in self.__documents_dict
            else False
        )

    def _get_result(
        self,
        index: Any,
        id: Any,
        doc_type: Any,
    ) -> dict | None:
        if index in self.__documents_dict:
            for document in self.__documents_dict[index]:
                if document.get("_id") == id and (
                    doc_type == "_all" or document.get("_type") == doc_type
                ):
                    return document
        return None

    @query_params(
        "_source",
        "_source_exclude",
        "_source_include",
        "fields",
        "parent",
        "preference",
        "realtime",
        "refresh",
        "routing",
        "version",
        "version_type",
    )
    async def get(
        self, index: Any, id: Any, params: Any = None, headers: Any = None, **kwargs
    ) -> Any:
        doc_type = "_all"
        ignore = extract_ignore_as_iterable(params)
        result = self._get_result(index, id, doc_type)

        if result:
            result["found"] = True
            return result
        if params and 404 in ignore:
            return {"found": False}
        error_data = {"_index": index, "_type": doc_type, "_id": id, "found": False}
        raise NotFoundError(404, json.dumps(error_data))

    def _update_result(self, index: Any, body: Any):
        if index in self.__documents_dict:
            for document in self.__documents_dict[index]:
                if document.get("_id") == id:
                    if "doc" in body:
                        document["_source"] = {**document["_source"], **body["doc"]}
                        document["_version"] += 1

                        return {
                            "_index": index,
                            "_id": id,
                            "_type": document.get("_type", "_doc"),
                            "_version": document["_version"],
                            "result": "updated",
                            "_shards": {"total": 1, "successful": 1, "failed": 0},
                        }
                    elif "script" in body:
                        raise NotImplementedError("Using script is currently not supported.")
        return None

    @query_params(
        "_source",
        "_source_excludes",
        "_source_includes",
        "if_primary_term",
        "if_seq_no",
        "lang",
        "refresh",
        "require_alias",
        "retry_on_conflict",
        "routing",
        "timeout",
        "wait_for_active_shards",
    )
    async def update(self, index, id, body, params=None, headers=None):
        if not body:
            raise RequestError(
                400,
                "action_request_validation_exception",
                "Validation Failed: 1: script or doc is missing;",
            )
        if "doc" not in body and "script" not in body:
            field = list(body.keys())
            raise RequestError(
                400,
                "x_content_parse_exception",
                f"[1:2] [UpdateRequest] unknown field [{field[0]}]",
            )
        if "doc" in body and "script" in body:
            raise RequestError(
                400,
                "action_request_validation_exception",
                "Validation Failed: 1: can't provide both script and doc;",
            )

        result = self._update_result(index, body)

        if result:
            return result
        raise NotFoundError(404, "document_missing_exception", f"[{id}]: document missing")

    @query_params(
        "_source",
        "_source_excludes",
        "_source_includes",
        "allow_no_indices",
        "analyze_wildcard",
        "analyzer",
        "conflicts",
        "default_operator",
        "df",
        "expand_wildcards",
        "from_",
        "ignore_unavailable",
        "lenient",
        "max_docs",
        "pipeline",
        "preference",
        "q",
        "refresh",
        "request_cache",
        "requests_per_second",
        "routing",
        "scroll",
        "scroll_size",
        "search_timeout",
        "search_type",
        "size",
        "slices",
        "sort",
        "stats",
        "terminate_after",
        "timeout",
        "version",
        "version_type",
        "wait_for_active_shards",
        "wait_for_completion",
    )
    async def update_by_query(
        self,
        index: Any,
        body: Any = None,
        params: Any = None,
        headers: Any = None,
    ) -> Any:
        doc_type = None
        total_updated = 0
        if isinstance(index, list):
            (index,) = index
        new_values = {}
        script_params = body["script"]["params"]
        script_source = body["script"]["source"].replace("ctx._source.", "").split(";")
        for sentence in script_source:
            if sentence:
                field, _, value = sentence.split()
                if value.startswith("params."):
                    _, key = value.split(".")
                    value = script_params.get(key)
                new_values[field] = value

        matches = await self.search(
            index=index, doc_type=doc_type, body=body, params=params, headers=headers
        )
        if matches["hits"]["total"]:
            for hit in matches["hits"]["hits"]:
                body = hit["_source"]
                body.update(new_values)
                await self.index(index, body, doc_type=hit["_type"], id=hit["_id"])
                total_updated += 1

        return {
            "took": 1,
            "time_out": False,
            "total": matches["hits"]["total"],
            "updated": total_updated,
            "deleted": 0,
            "batches": 1,
            "version_conflicts": 0,
            "noops": 0,
            "retries": 0,
            "throttled_millis": 100,
            "requests_per_second": 100,
            "throttled_until_millis": 0,
            "failures": [],
        }

    @query_params(
        "_source",
        "_source_exclude",
        "_source_include",
        "preference",
        "realtime",
        "refresh",
        "routing",
        "stored_fields",
    )
    async def mget(
        self,
        body: Any,
        index: Any = None,
        params: Any = None,
        headers: Any = None,
    ) -> Any:
        doc_type = "_all"
        docs = body.get("docs")
        ids = [doc["_id"] for doc in docs]
        results = []
        for id in ids:
            # pylint: disable=bare-except
            try:
                results.append(
                    await self.get(index, id, doc_type=doc_type, params=params, headers=headers)
                )
            except:
                pass  # nosec
        if not results:
            raise RequestError(
                400,
                "action_request_validation_exception",
                "Validation Failed: 1: no documents to get;",
            )
        return {"docs": results}

    @query_params(
        "_source",
        "_source_exclude",
        "_source_include",
        "parent",
        "preference",
        "realtime",
        "refresh",
        "routing",
        "version",
        "version_type",
    )
    async def get_source(
        self,
        index: Any,
        id: Any,
        params: Any = None,
        headers: Any = None,
    ) -> Any:
        doc_type = None
        document = await self.get(index=index, doc_type=doc_type, id=id, params=params)
        return document.get("_source")

    @query_params(
        "_source",
        "_source_exclude",
        "_source_include",
        "allow_no_indices",
        "analyze_wildcard",
        "analyzer",
        "default_operator",
        "df",
        "expand_wildcards",
        "explain",
        "fielddata_fields",
        "fields",
        "from_",
        "ignore_unavailable",
        "lenient",
        "lowercase_expanded_terms",
        "min_score",
        "preference",
        "q",
        "request_cache",
        "routing",
        "scroll",
        "search_type",
        "size",
        "sort",
        "stats",
        "suggest_field",
        "suggest_mode",
        "suggest_size",
        "suggest_text",
        "terminate_after",
        "timeout",
        "track_scores",
        "version",
    )
    async def count(
        self,
        body: Any = None,
        index: Any = None,
        params: Any = None,
        headers: Any = None,
    ) -> Any:
        doc_type = None
        searchable_indexes = self._normalize_index_to_list(index)

        i = 0
        for searchable_index in searchable_indexes:
            for document in self.__documents_dict[searchable_index]:
                if doc_type and document.get("_type") != doc_type:
                    continue
                i += 1
        result = {
            "count": i,
            "_shards": {"successful": 1, "skipped": 0, "failed": 0, "total": 1},
        }

        return result

    def _get_fake_query_condition(self, query_type_str, condition):
        return FakeQueryCondition(QueryType.get_query_type(query_type_str), condition)

    @query_params(
        "ccs_minimize_roundtrips",
        "max_concurrent_searches",
        "max_concurrent_shard_requests",
        "pre_filter_shard_size",
        "rest_total_hits_as_int",
        "search_type",
        "typed_keys",
    )
    async def msearch(
        self,
        body: Any,
        index: Any = None,
        params: Any = None,
        headers: Any = None,
    ) -> Any:
        def grouped(iterable):
            if len(iterable) % 2 != 0:
                # pylint: disable=broad-exception-raised
                raise Exception("Malformed body")
            iterator = iter(iterable)
            while True:
                try:
                    yield (next(iterator)["index"], next(iterator))
                except StopIteration:
                    break

        responses = []
        took = 0
        for ind, query in grouped(body):
            response = await self.search(index=ind, body=query)
            took += response["took"]
            responses.append(response)
        result = {"took": took, "responses": responses}
        return result

    def _get_search_matches(self, body: Any, index: Any):
        searchable_indexes = self._normalize_index_to_list(index)
        matches = []
        conditions = []

        if body and "query" in body:
            query = body["query"]
            for query_type_str, condition in query.items():
                conditions.append(self._get_fake_query_condition(query_type_str, condition))

        for searchable_index in searchable_indexes:
            matches = [
                document
                for document in self.__documents_dict[searchable_index]
                if not conditions or any(condition.evaluate(document) for condition in conditions)
            ]

        return matches

    def _search_result(self, body: Any, index: Any, params: Any, matches: list[dict]) -> dict:
        doc_type: Optional[list] = None
        searchable_indexes = self._normalize_index_to_list(index)

        result = {
            "hits": {
                "total": {"value": len(matches), "relation": "eq"},
                "max_score": 1.0,
            },
            "_shards": {
                "successful": len(searchable_indexes),
                "skipped": 0,
                "failed": 0,
                "total": len(searchable_indexes),
            },
            "took": 1,
            "timed_out": False,
        }

        hits = []
        for match in matches:
            match["_score"] = 1.0
            hits.append(match)

        # build aggregations
        if body is not None and "aggs" in body:
            aggregations = {}

            for aggregation, definition in body["aggs"].items():
                aggregations[aggregation] = {
                    "doc_count_error_upper_bound": 0,
                    "sum_other_doc_count": 0,
                    "buckets": self.make_aggregation_buckets(definition, matches),
                }

            if aggregations:
                result["aggregations"] = aggregations

        print("-----------------")
        print(result)
        print("-----------------")

        if "scroll" in params:
            result["_scroll_id"] = str(get_random_scroll_id())
            params["size"] = int(params.get("size", 10))
            params["from"] = int(params.get("from") + params.get("size") if "from" in params else 0)
            self.__scrolls[result.get("_scroll_id")] = {
                "index": index,
                "doc_type": doc_type,
                "body": body,
                "params": params,
            }
            hits = hits[params.get("from") : params.get("from") + params.get("size")]
        elif "size" in params:
            hits = hits[: int(params["size"])]
        elif body and "size" in body:
            hits = hits[: int(body["size"])]

        result["hits"]["hits"] = hits

        return result

    @query_params(
        "_source",
        "_source_exclude",
        "_source_include",
        "allow_no_indices",
        "analyze_wildcard",
        "analyzer",
        "default_operator",
        "df",
        "expand_wildcards",
        "explain",
        "fielddata_fields",
        "fields",
        "from_",
        "ignore_unavailable",
        "lenient",
        "lowercase_expanded_terms",
        "preference",
        "q",
        "request_cache",
        "routing",
        "scroll",
        "search_type",
        "size",
        "sort",
        "stats",
        "suggest_field",
        "suggest_mode",
        "suggest_size",
        "suggest_text",
        "terminate_after",
        "timeout",
        "track_scores",
        "version",
    )
    async def search(
        self,
        body: Any = None,
        index: Any = None,
        params: Any = None,
        headers: Any = None,
        **kwargs,
    ) -> Any:
        matches = self._get_search_matches(body, index)

        for match in matches:
            self._find_and_convert_data_types(match["_source"])

        return self._search_result(body, index, params, matches)

    @query_params("scroll")
    async def scroll(
        self,
        body: Any = None,
        scroll_id: Any = None,
        params: Any = None,
        headers: Any = None,
    ) -> Any:
        scroll = self.__scrolls.pop(scroll_id)
        result = await self.search(
            index=scroll.get("index"),
            doc_type=scroll.get("doc_type"),
            body=scroll.get("body"),
            params=scroll.get("params"),
        )
        return result

    @query_params(
        "consistency",
        "parent",
        "refresh",
        "replication",
        "routing",
        "timeout",
        "version",
        "version_type",
    )
    async def delete(
        self, index: Any, id: Any, params: Any = None, headers: Any = None, **kwargs
    ) -> Any:
        doc_type = None
        found = await self.exists(index, id, params=params)
        ignore = extract_ignore_as_iterable(params)

        result_dict = {
            "found": found,
            "_index": index,
            "_type": doc_type,
            "_id": id,
            "_version": 1,
        }

        if found:
            for document in self.__documents_dict[index]:
                if document.get("_id") == id:
                    self.__documents_dict[index].remove(document)
            return result_dict
        if params and 404 in ignore:
            return {"found": False}
        raise NotFoundError(404, json.dumps(result_dict))

    @query_params(
        "allow_no_indices",
        "expand_wildcards",
        "ignore_unavailable",
        "preference",
        "routing",
    )
    def suggest(self, body, index=None, params=None, headers=None):
        if index is not None and index not in self.__documents_dict:
            raise NotFoundError(404, f"IndexMissingException[[{index}] missing]")

        result_dict = {}
        for key, value in body.items():
            text = value.get("text")
            suggestion = int(text) + 1 if isinstance(text, int) else f"{text}_suggestion"
            result_dict[key] = [
                {
                    "text": text,
                    "length": 1,
                    "options": [{"text": suggestion, "freq": 1, "score": 1.0}],
                    "offset": 0,
                }
            ]
        return result_dict

    def _normalize_index_to_list(self, index):
        # Ensure to have a list of index
        if index is None:
            searchable_indexes = self.__documents_dict.keys()
        elif isinstance(index, str):
            searchable_indexes = [index]
        elif isinstance(index, list):
            searchable_indexes = index
        else:
            # Is it the correct exception to use ?
            raise ValueError("Invalid param 'index'")

        # Check index(es) exists
        for searchable_index in searchable_indexes:
            if (
                searchable_index not in self.__documents_dict
                and searchable_index not in self._FakeIndicesClient__documents_dict
            ):
                raise NotFoundError(404, f"IndexMissingException[[{searchable_index}] missing]")

        return searchable_indexes

    @classmethod
    def _find_and_convert_data_types(cls, document):
        for key, value in document.items():
            if isinstance(value, dict):
                cls._find_and_convert_data_types(value)
            elif isinstance(value, datetime.datetime):
                document[key] = value.isoformat()

    def make_aggregation_buckets(self, aggregation, documents):
        if "composite" in aggregation:
            return self.make_composite_aggregation_buckets(aggregation, documents)
        return []

    def make_composite_aggregation_buckets(self, aggregation, documents):
        def make_key(doc_source, agg_source):
            attr = list(agg_source.values())[0]["terms"]["field"]
            return doc_source[attr]

        def make_bucket(bucket_key, bucket):
            out = {
                "key": dict(zip(bucket_key_fields, bucket_key, strict=False)),
                "doc_count": len(bucket),
            }

            for metric_key, metric_definition in aggregation["aggs"].items():
                metric_type_str = list(metric_definition)[0]
                metric_type = MetricType.get_metric_type(metric_type_str)
                attr = metric_definition[metric_type_str]["field"]
                data = [doc[attr] for doc in bucket]

                if metric_type == MetricType.CARDINALITY:
                    value = len(set(data))
                else:
                    raise NotImplementedError(f"Metric type '{metric_type}' not implemented")

                out[metric_key] = {"value": value}
            return out

        agg_sources = aggregation["composite"]["sources"]
        buckets = defaultdict(list)
        bucket_key_fields = [list(src)[0] for src in agg_sources]
        for document in documents:
            doc_src = document["_source"]
            key = tuple(
                make_key(doc_src, agg_src) for agg_src in aggregation["composite"]["sources"]
            )
            buckets[key].append(doc_src)

        buckets = sorted(((k, v) for k, v in buckets.items()), key=lambda x: x[0])
        buckets = [make_bucket(bucket_key, bucket) for bucket_key, bucket in buckets]
        return buckets
