import os
from collections.abc import Awaitable, Callable
from typing import ParamSpec, TypeVar

from aioextensions import collect

from integrates.s3.resource import get_client

T = TypeVar("T")
P = ParamSpec("P")


async def _add_file(full_path: str) -> None:
    client = await get_client()
    bucket_path = full_path.split("/test_data/")[1].split("/", maxsplit=1)[1]
    bucket_name = bucket_path.split("/", maxsplit=1)[0]
    filename = bucket_path.split("/", maxsplit=1)[1]

    await client.upload_file(Filename=full_path, Bucket=bucket_name, Key=filename)


async def main(func: Callable[P, Awaitable[T]]) -> None:
    module_path = "/".join(func.__code__.co_filename.split("/")[:-1])
    abs_path = f"{module_path}/test_data/{func.__name__}"

    if not os.path.exists(abs_path):
        return

    all_files = os.listdir(abs_path)
    buckets = [
        os.path.join(abs_path, dir)
        for dir in all_files
        if os.path.isdir(os.path.join(abs_path, dir))
    ]

    for bucket in buckets:
        for dir_, _, files in os.walk(bucket):
            await collect(list({_add_file(os.path.join(dir_, filename)) for filename in files}))
