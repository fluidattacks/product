from typing import NamedTuple


class IntegratesS3(NamedTuple):
    autoload: bool = False
    """
    Defaults to False. When it's True, bucket files in the `test_data/<function_name>`
    directory will be uploaded to mocked S3.
    """
