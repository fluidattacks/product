# ruff: noqa: PGH004
import argparse
import socket
import sys
from collections.abc import Generator
from contextlib import contextmanager
from decimal import Decimal
from pathlib import Path
from typing import NamedTuple

import moto.core  # noqa
import pytest

import coverage


class Args(NamedTuple):
    target: Path
    tests: list[str]


def _get_args() -> Args:
    parser = argparse.ArgumentParser(
        prog="integrates-back-test",
        description=(
            "🏹 Python package for unit and integration testing through Fluid Attacks projects"
        ),
    )

    parser.add_argument(
        "--target",
        metavar="TARGET",
        type=Path,  # type: ignore[misc]
        required=True,
        help="Directory to test. Default is current directory.",
    )

    parser.add_argument(
        "--tests",
        metavar="TESTS",
        type=str,
        required=False,
        nargs="*",
        help="Tests to run. Default is all tests.",
    )

    args = parser.parse_args()

    return Args(
        target=args.target,  # type: ignore[misc]
        tests=args.tests,  # type: ignore[misc]
    )


@contextmanager
def _track_coverage(args: Args) -> Generator[coverage.Coverage, None, None]:
    cov = coverage.Coverage()
    cov.set_option("run:source", [str(args.target)])
    cov.set_option("run:branch", True)
    cov.set_option("run:omit", ["**/*_test.py"])
    cov.start()

    yield cov

    cov.stop()


def _cov_read(cov_path: Path) -> int:
    if cov_path.is_file():
        with open(cov_path, encoding="utf-8") as cov_file:
            return int(cov_file.read())
    return 0


def _cov_write(cov_path: Path, cov: int) -> None:
    if not cov_path.is_file():
        cov_path.touch()
    with open(cov_path, "w", encoding="utf-8") as cov_file:
        cov_file.write(str(cov))


def _cov_test(args: Args, cov: coverage.Coverage) -> bool:
    path = Path(f"{args.target}/coverage")
    current = _cov_read(path)
    new = round(
        Decimal(
            cov.report(
                output_format="text",
                skip_covered=True,
                show_missing=True,
                skip_empty=True,
                sort="cover",
            ),
        )
    )

    if new == current:
        print(f"Coverage remained at {current}%.")
        return True
    if new > current:
        print(
            f"Coverage increased from {current}% to {new}%. "
            "Please update coverage file in your commit.",
        )
        _cov_write(path, new)
        return False
    print(f"Coverage decreased from {current}% to {new}%. Please add tests.")
    return False


class NetworkBlockPlugin:
    @pytest.fixture(autouse=True, scope="session")
    def block_network(self) -> None:
        def patched_connect(*args: None, **kwargs: None) -> None:
            raise RuntimeError(
                "This test attempted to perform a network request. "
                "You must mock the call to avoid introducing indeterminism.",
            )

        monkeypatch = pytest.MonkeyPatch()
        monkeypatch.setattr(socket.socket, "connect", patched_connect)


def _pytest(args: Args) -> bool:
    pytest_args = ["--asyncio-mode=auto", "--showlocals", "--strict-markers", "-vv"]
    if args.tests:
        pytest_args.extend(["-k", " or ".join(args.tests)])

    pytest_plugins = [NetworkBlockPlugin()]

    exit_code = pytest.main([str(args.target), *pytest_args], pytest_plugins)

    # https://docs.pytest.org/en/stable/reference/exit-codes.html
    # 0 for pass, 5 for no tests collected
    return exit_code in [0, 5]


def main() -> bool:
    args = _get_args()

    if not args.tests:
        with _track_coverage(args) as cov:
            result = _pytest(args)
            result = result and _cov_test(args, cov)
    else:
        result = _pytest(args)

    return sys.exit(0) if result else sys.exit(1)
