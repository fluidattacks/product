from datetime import (
    datetime,
)

from integrates.db_model.toe_inputs.types import (
    ToeInput,
    ToeInputState,
)
from integrates.roots.domain import (
    format_environment_id,
)
from integrates.testing.fakers.constants import (
    DATE_2019,
    EMAIL_FLUIDATTACKS_HACKER,
)
from integrates.testing.fakers.randoms import (
    random_uuid,
)


def ToeInputStateFaker(  # noqa: N802
    *,
    attacked_at: datetime | None = None,
    attacked_by: str | None = None,
    be_present: bool = True,
    be_present_until: datetime | None = None,
    first_attack_at: datetime | None = None,
    has_vulnerabilities: bool | None = False,
    modified_by: str | None = EMAIL_FLUIDATTACKS_HACKER,
    modified_date: datetime = DATE_2019,
    seen_at: datetime | None = DATE_2019,
    seen_first_time_by: str | None = EMAIL_FLUIDATTACKS_HACKER,
) -> ToeInputState:
    return ToeInputState(
        attacked_at=attacked_at,
        attacked_by=attacked_by,
        be_present=be_present,
        be_present_until=be_present_until,
        first_attack_at=first_attack_at,
        has_vulnerabilities=has_vulnerabilities,
        modified_by=modified_by,
        modified_date=modified_date,
        seen_at=seen_at,
        seen_first_time_by=seen_first_time_by,
    )


def ToeInputFaker(  # noqa: N802
    *,
    component: str = "https://fluidattacks.com/test",
    entry_point: str = "btnTest",
    environment_id: str | None = None,
    root_id: str = random_uuid(),
    group_name: str = "test-group",
    state: ToeInputState = ToeInputStateFaker(),
) -> ToeInput:
    if environment_id is None:
        environment_id = format_environment_id(component)

    return ToeInput(
        component=component,
        entry_point=entry_point,
        environment_id=environment_id,
        root_id=root_id,
        group_name=group_name,
        state=state,
    )
