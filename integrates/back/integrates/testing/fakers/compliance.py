from decimal import (
    Decimal,
)

from integrates.db_model.compliance.types import (
    ComplianceStandard,
    ComplianceUnreliableIndicators,
)

STANDARD_NAMES = [
    "bsimm",
    "capec",
    "cis",
    "cwe",
    "eprivacy",
    "gdpr",
    "hipaa",
    "iso27001",
    "nerccip",
    "nist80053",
    "nist80063",
    "asvs",
    "owasp10",
    "pci",
    "soc2",
    "cwe25",
    "owaspm10",
    "nist",
    "agile",
    "bizec",
    "ccpa",
    "cpra",
    "certc",
    "certj",
    "fcra",
    "facta",
    "glba",
    "misrac",
    "nydfs",
    "nyshield",
    "mitre",
    "padss",
    "sans25",
    "pdpa",
    "popia",
    "pdpo",
    "cmmc",
    "hitrust",
    "fedramp",
    "iso27002",
    "lgpd",
    "iec62443",
    "wassec",
    "osstmm3",
    "c2m2",
    "wasc",
    "ferpa",
    "nistssdf",
    "issaf",
    "ptes",
    "owasprisks",
    "mvsp",
    "owaspscp",
    "bsafss",
    "owaspmasvs",
    "nist800171",
    "nist800115",
    "swiftcsc",
    "osamm",
    "siglite",
    "sig",
]
DEFAULT_AVG = Decimal(0.85)
DEFAULT_BEST = Decimal(0.95)
DEFAULT_WORST = Decimal(0.6)


class ReachedStandardsLimitsException(Exception):
    pass


def _make_fake_standards(
    amount: int = 10,
) -> list[ComplianceStandard]:
    if amount > len(STANDARD_NAMES):
        raise ReachedStandardsLimitsException()
    return [
        ComplianceStandard(
            avg_organization_compliance_level=DEFAULT_AVG,
            best_organization_compliance_level=DEFAULT_BEST,
            standard_name=standard,
            worst_organization_compliance_level=DEFAULT_WORST,
        )
        for standard in STANDARD_NAMES[:amount]
    ]


def ComplianceUnreliableIndicatorsFaker(  # noqa: N802
    n_standards: int = 10,
) -> ComplianceUnreliableIndicators:
    return ComplianceUnreliableIndicators(standards=_make_fake_standards(n_standards))
