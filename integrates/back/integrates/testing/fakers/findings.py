from datetime import datetime
from decimal import Decimal

from integrates.db_model.enums import Source
from integrates.db_model.findings.enums import (
    FindingStateStatus,
    FindingStatus,
    FindingVerificationStatus,
)
from integrates.db_model.findings.types import (
    Finding,
    FindingEvidence,
    FindingEvidences,
    FindingState,
    FindingUnreliableIndicators,
    FindingVerification,
    FindingVerificationSummary,
    FindingVulnerabilitiesSummary,
    FindingZeroRiskSummary,
)
from integrates.db_model.types import SeverityScore
from integrates.testing.fakers.constants import (
    DATE_2019,
    EMAIL_FLUIDATTACKS_HACKER,
    EMAIL_GENERIC,
)
from integrates.testing.fakers.randoms import random_comment_id, random_uuid


def FindingEvidenceFaker(  # noqa: N802
    *,
    description: str = "evidence description",
    modified_date: datetime = DATE_2019,
    url: str = "group_name-finding_id-evidence_id",
    author_email: str = EMAIL_FLUIDATTACKS_HACKER,
    is_draft: bool = False,
) -> FindingEvidence:
    return FindingEvidence(
        description=description,
        modified_date=modified_date,
        url=url,
        author_email=author_email,
        is_draft=is_draft,
    )


def FindingEvidencesFaker(  # noqa: N802
    *,
    animation: FindingEvidence | None = FindingEvidenceFaker(
        description="animation",
    ),
    evidence1: FindingEvidence | None = FindingEvidenceFaker(
        description="evidence1",
    ),
    evidence2: FindingEvidence | None = FindingEvidenceFaker(
        description="evidence2",
    ),
    evidence3: FindingEvidence | None = FindingEvidenceFaker(
        description="evidence3",
    ),
    evidence4: FindingEvidence | None = FindingEvidenceFaker(
        description="evidence4",
    ),
    evidence5: FindingEvidence | None = FindingEvidenceFaker(
        description="evidence5",
    ),
    exploitation: FindingEvidence | None = FindingEvidenceFaker(
        description="exploitation",
    ),
) -> FindingEvidences:
    return FindingEvidences(
        animation=animation,
        evidence1=evidence1,
        evidence2=evidence2,
        evidence3=evidence3,
        evidence4=evidence4,
        evidence5=evidence5,
        exploitation=exploitation,
    )


def FindingStateFaker(  # noqa: N802
    *,
    modified_by: str = EMAIL_GENERIC,
    modified_date: datetime = DATE_2019,
    source: Source = Source.MACHINE,
    status: FindingStateStatus = FindingStateStatus.CREATED,
) -> FindingState:
    return FindingState(
        modified_by=modified_by,
        modified_date=modified_date,
        source=source,
        status=status,
    )


def FindingVerificationFaker(  # noqa: N802
    *,
    comment_id: str = random_comment_id(),
    modified_by: str = EMAIL_GENERIC,
    modified_date: datetime = DATE_2019,
    status: FindingVerificationStatus = FindingVerificationStatus.REQUESTED,
    vulnerability_ids: set[str] | None = None,
) -> FindingVerification:
    return FindingVerification(
        comment_id=comment_id,
        modified_by=modified_by,
        modified_date=modified_date,
        status=status,
        vulnerability_ids=vulnerability_ids,
    )


def FindingUnreliableIndicatorsFaker(  # noqa: N802
    *,
    unreliable_newest_vulnerability_report_date: datetime = DATE_2019,
    unreliable_oldest_vulnerability_report_date: datetime = DATE_2019,
    unreliable_oldest_open_vulnerability_report_date: datetime = DATE_2019,
    unreliable_status: FindingStatus = FindingStatus.SAFE,
    unreliable_where: str = "9999",
    unreliable_zero_risk_summary: FindingZeroRiskSummary = FindingZeroRiskSummary(
        confirmed=0,
        rejected=0,
        requested=0,
    ),
    newest_vulnerability_report_date: datetime = DATE_2019,
    oldest_vulnerability_report_date: datetime = DATE_2019,
    total_open_priority: Decimal = Decimal(0),
    verification_summary: FindingVerificationSummary = FindingVerificationSummary(
        requested=0,
        on_hold=0,
        verified=0,
    ),
    vulnerabilities_summary: FindingVulnerabilitiesSummary = (
        FindingVulnerabilitiesSummary(
            closed=1,
            open=0,
            submitted=0,
            rejected=0,
            open_critical=0,
            open_high=0,
            open_low=0,
            open_medium=0,
            open_critical_v3=0,
            open_high_v3=0,
            open_low_v3=0,
            open_medium_v3=0,
        )
    ),
) -> FindingUnreliableIndicators:
    return FindingUnreliableIndicators(
        unreliable_newest_vulnerability_report_date=(unreliable_newest_vulnerability_report_date),
        unreliable_oldest_open_vulnerability_report_date=(
            unreliable_oldest_open_vulnerability_report_date
        ),
        unreliable_oldest_vulnerability_report_date=unreliable_oldest_vulnerability_report_date,
        unreliable_status=unreliable_status,
        unreliable_where=unreliable_where,
        unreliable_zero_risk_summary=unreliable_zero_risk_summary,
        newest_vulnerability_report_date=newest_vulnerability_report_date,
        oldest_vulnerability_report_date=oldest_vulnerability_report_date,
        total_open_priority=total_open_priority,
        verification_summary=verification_summary,
        vulnerabilities_summary=vulnerabilities_summary,
    )


def FindingFaker(  # noqa: N802
    *,
    group_name: str = "test-group",
    id: str = random_uuid(),  # noqa: A002
    severity_score: SeverityScore = SeverityScore(
        base_score=Decimal("4.5"),
        temporal_score=Decimal("4.1"),
        cvss_v3=(
            "CVSS:3.1/AV:P/AC:H/PR:L/UI:N/S:C/C:L/I:L/A:L/"
            "E:P/RL:O/CR:L/AR:H/MAV:N/MAC:H/MPR:H/MUI:R/MS:U/MC:L/"
            "MA:L"
        ),
        threat_score=Decimal("1.1"),
        cvss_v4=(
            "CVSS:4.0/AV:P/AC:H/AT:N/PR:L/UI:N/VC:L/VI:L/"
            "VA:L/SC:L/SI:L/SA:L/E:P/AR:H/MAV:N/MAC:H/MPR:H/MUI:P"
            "/MVC:L/MVA:L"
        ),
        cvssf=Decimal("1.149"),
        cvssf_v4=Decimal("0.018"),
    ),
    state: FindingState = FindingStateFaker(),
    title: str = "001. SQL injection - C Sharp SQL API",
    attack_vector_description: str = "Test attack vector",
    description: str = "Test description",
    recommendation: str = "Test recommendation",
    threat: str = "Test threat",
    unfulfilled_requirements: list[str] = [],
    unreliable_indicators: FindingUnreliableIndicators = FindingUnreliableIndicatorsFaker(),
    evidences: FindingEvidences = FindingEvidences(),
    verification: FindingVerification | None = None,
) -> Finding:
    return Finding(
        group_name=group_name,
        id=id,
        severity_score=severity_score,
        state=state,
        title=title,
        attack_vector_description=attack_vector_description,
        description=description,
        recommendation=recommendation,
        threat=threat,
        unfulfilled_requirements=unfulfilled_requirements,
        unreliable_indicators=unreliable_indicators,
        evidences=evidences,
        verification=verification,
    )
