from datetime import (
    datetime,
)

from integrates.db_model.integration_repositories.types import (
    OrganizationIntegrationRepository,
)
from integrates.testing.fakers.randoms import (
    random_uuid,
)


def OrganizationIntegrationRepositoryFaker(  # noqa: N802
    *,
    id: str = (  # noqa: A002
        "62d6130b84736f251d03171352149ce238691c11f3b1535dd70fc7a2bfdf77fd"
    ),
    organization_id: str = random_uuid(),
    branch: str = "refs/heads/trunk",
    last_commit_date: datetime | None = None,
    url: str = "https://gitlab.com/fluidattacks/universe",
    credential_id: str | None = None,
    branches: tuple[str, ...] | None = None,
    name: str | None = None,
) -> OrganizationIntegrationRepository:
    return OrganizationIntegrationRepository(
        id=id,
        organization_id=organization_id,
        branch=branch,
        last_commit_date=last_commit_date,
        url=url,
        credential_id=credential_id,
        branches=branches,
        name=name,
    )
