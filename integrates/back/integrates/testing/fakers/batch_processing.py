from collections.abc import Sequence
from datetime import (
    datetime,
)

from integrates.batch.constants import (
    DEFAULT_ACTION_TIMEOUT,
)
from integrates.batch.enums import (
    Action,
    IntegratesBatchQueue,
    SkimsBatchQueue,
)
from integrates.batch.types import (
    BatchProcessing,
    DependentAction,
)
from integrates.testing.fakers.constants import (
    DATE_2019,
    EMAIL_GENERIC,
)
from integrates.testing.fakers.randoms import (
    random_uuid,
)


def BatchProcessingFaker(  # noqa: N802
    *,
    key: str = random_uuid(),
    action_name: Action = Action.REPORT,
    entity: str = "group1",
    subject: str = EMAIL_GENERIC,
    time: datetime = DATE_2019,
    additional_info: dict[str, None | int | Sequence[str]] = {
        "report_type": "XLS",
        "treatments": [
            "ACCEPTED",
            "ACCEPTED_UNDEFINED",
            "IN_PROGRESS",
            "UNTREATED",
        ],
        "states": ["SAFE"],
        "verifications": ["VERIFIED"],
        "closing_date": "2020-06-01T00:00:00",
        "finding_title": "065",
        "age": None,
        "min_severity": None,
        "max_severity": None,
        "last_report": None,
        "min_release_date": None,
        "max_release_date": None,
        "location": "",
    },
    queue: IntegratesBatchQueue | SkimsBatchQueue = IntegratesBatchQueue.SMALL,
    attempt_duration_seconds: int = DEFAULT_ACTION_TIMEOUT,
    batch_job_id: str | None = None,
    dependent_actions: list[DependentAction] | None = None,
    retries: int = 0,
    running: bool = False,
) -> BatchProcessing:
    return BatchProcessing(
        key=key,
        action_name=action_name,
        entity=entity,
        subject=subject,
        time=time,
        additional_info=additional_info,
        queue=queue,
        attempt_duration_seconds=attempt_duration_seconds,
        batch_job_id=batch_job_id,
        dependent_actions=dependent_actions,
        retries=retries,
        running=running,
    )


def DependentActionFaker(  # noqa: N802
    *,
    action_name: Action = Action.CLONE_ROOTS,
    additional_info: dict[str, None | int | Sequence[str]] = {
        "git_root_ids": ["id1", "id2"],
    },
    queue: IntegratesBatchQueue | SkimsBatchQueue = IntegratesBatchQueue.SMALL,
    attempt_duration_seconds: int = DEFAULT_ACTION_TIMEOUT,
    job_name: str | None = None,
) -> DependentAction:
    return DependentAction(
        action_name=action_name,
        additional_info=additional_info,
        queue=queue,
        attempt_duration_seconds=attempt_duration_seconds,
        job_name=job_name,
    )
