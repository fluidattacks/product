from decimal import (
    Decimal,
)

from integrates.db_model.portfolios.types import (
    Portfolio,
    PortfolioUnreliableIndicators,
)
from integrates.testing.fakers.randoms import (
    random_uuid,
)


def PortfolioUnreliableIndicatorsFaker(  # noqa: N802
    *,
    last_closing_date: int = 20,
    max_open_severity: Decimal = Decimal("6.5"),
    max_severity: Decimal = Decimal("6.5"),
    mean_remediate: Decimal = Decimal("100"),
    mean_remediate_critical_severity: Decimal = Decimal("0"),
    mean_remediate_high_severity: Decimal = Decimal("0"),
    mean_remediate_low_severity: Decimal = Decimal("0"),
    mean_remediate_medium_severity: Decimal = Decimal("0"),
) -> PortfolioUnreliableIndicators:
    return PortfolioUnreliableIndicators(
        last_closing_date=last_closing_date,
        max_open_severity=max_open_severity,
        max_severity=max_severity,
        mean_remediate=mean_remediate,
        mean_remediate_critical_severity=mean_remediate_critical_severity,
        mean_remediate_high_severity=mean_remediate_high_severity,
        mean_remediate_low_severity=mean_remediate_low_severity,
        mean_remediate_medium_severity=mean_remediate_medium_severity,
    )


def PortfolioFaker(  # noqa: N802
    *,
    id: str = random_uuid(),  # noqa: A002
    groups: set[str] = set(["test-group"]),
    organization_name: str = "test-organization",
    unreliable_indicators: PortfolioUnreliableIndicators = (PortfolioUnreliableIndicatorsFaker()),
) -> Portfolio:
    return Portfolio(
        id=id,
        groups=groups,
        organization_name=organization_name,
        unreliable_indicators=unreliable_indicators,
    )
