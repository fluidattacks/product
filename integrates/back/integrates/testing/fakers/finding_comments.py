from datetime import (
    datetime,
)

from integrates.custom_utils.datetime import (
    get_as_utc_iso_format,
)
from integrates.db_model.finding_comments.enums import (
    CommentType,
)
from integrates.db_model.finding_comments.types import (
    FindingComment,
)
from integrates.db_model.items import (
    FindingCommentItem,
)
from integrates.testing.fakers.constants import (
    DATE_2019,
    EMAIL_GENERIC,
)
from integrates.testing.fakers.randoms import (
    random_uuid,
)


def FindingCommentFaker(  # noqa: N802
    *,
    comment_type: CommentType = CommentType.CONSULT,
    content: str = "This is a finding comment test",
    creation_date: datetime = DATE_2019,
    email: str = EMAIL_GENERIC,
    finding_id: str = random_uuid(),
    id: str = "1558048727111",  # noqa: A002
    parent_id: str = "0",
    full_name: str | None = "Test User",
) -> FindingComment:
    return FindingComment(
        comment_type=comment_type,
        content=content,
        creation_date=creation_date,
        email=email,
        finding_id=finding_id,
        id=id,
        parent_id=parent_id,
        full_name=full_name,
    )


def FindingCommentItemFaker(  # noqa: N802
    *,
    comment_type: CommentType = CommentType.CONSULT,
    content: str = "This is a finding comment test",
    creation_date: datetime = DATE_2019,
    email: str = EMAIL_GENERIC,
    finding_id: str = random_uuid(),
    id: str = "1558048727111",  # noqa: A002
    parent_id: str = "0",
    full_name: str = "Test User",
) -> FindingCommentItem:
    return {
        "pk": f"CMNT#{finding_id}",
        "sk": f"CMNT#{finding_id}",
        "finding_id": finding_id,
        "id": id,
        "parent_id": parent_id,
        "comment_type": comment_type.value,
        "creation_date": get_as_utc_iso_format(creation_date),
        "content": content,
        "email": email,
        "full_name": full_name,
    }
