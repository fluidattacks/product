from datetime import datetime

from integrates.db_model.groups.enums import (
    GroupLanguage,
    GroupManaged,
    GroupService,
    GroupStateStatus,
    GroupSubscriptionType,
    GroupTier,
)
from integrates.db_model.groups.types import (
    Group,
    GroupAzureIssues,
    GroupFile,
    GroupGitLabIssues,
    GroupState,
)
from integrates.testing.fakers.constants import DATE_2019, EMAIL_GENERIC
from integrates.testing.fakers.randoms import random_uuid


def GroupGitLabIssuesFaker(  # noqa: N802
    *,
    redirect_uri: str = "https://testing.com",
    refresh_token: str = "refresh_token",  # noqa: S107
    assignee_ids: list[int] | None = None,
    gitlab_project: str | None = None,
    connection_date: datetime | None = None,
    issue_automation_enabled: bool = True,
    labels: list[str] | None = None,
) -> GroupGitLabIssues:
    return GroupGitLabIssues(
        redirect_uri=redirect_uri,
        refresh_token=refresh_token,
        assignee_ids=assignee_ids,
        gitlab_project=gitlab_project,
        connection_date=connection_date,
        issue_automation_enabled=issue_automation_enabled,
        labels=labels,
    )


def GroupAzureIssuesFaker(  # noqa: N802
    *,
    redirect_uri: str = "https://testing.com",
    refresh_token: str = "refresh_token",  # noqa: S107
    assigned_to: str | None = None,
    azure_organization: str | None = None,
    azure_project: str | None = None,
    connection_date: datetime | None = None,
    issue_automation_enabled: bool = True,
    tags: list[str] | None = None,
) -> GroupAzureIssues:
    return GroupAzureIssues(
        redirect_uri=redirect_uri,
        refresh_token=refresh_token,
        assigned_to=assigned_to,
        azure_organization=azure_organization,
        azure_project=azure_project,
        connection_date=connection_date,
        issue_automation_enabled=issue_automation_enabled,
        tags=tags,
    )


def GroupStateFaker(  # noqa: N802
    *,
    has_essential: bool = True,
    has_advanced: bool = True,
    managed: GroupManaged = GroupManaged.MANAGED,
    modified_by: str = EMAIL_GENERIC,
    modified_date: datetime = DATE_2019,
    pending_deletion_date: datetime | None = None,
    status: GroupStateStatus = GroupStateStatus.ACTIVE,
    tags: set[str] = set(["test-tag"]),
    tier: GroupTier = GroupTier.ADVANCED,
    type: GroupSubscriptionType = (  # noqa: A002
        GroupSubscriptionType.CONTINUOUS
    ),
    service: GroupService | None = None,
) -> GroupState:
    return GroupState(
        has_essential=has_essential,
        has_advanced=has_advanced,
        managed=managed,
        modified_by=modified_by,
        modified_date=modified_date,
        pending_deletion_date=pending_deletion_date,
        status=status,
        tags=tags,
        tier=tier,
        type=type,
        service=service,
    )


def GroupFaker(  # noqa: N802
    *,
    agent_token: str | None = None,
    azure_issues: GroupAzureIssues | None = None,
    created_by: str = EMAIL_GENERIC,
    created_date: datetime = DATE_2019,
    description: str = "test-group",
    files: list[GroupFile] = [
        GroupFile(
            description="test-group-file",
            file_name="test.zip",
            modified_by=EMAIL_GENERIC,
            modified_date=DATE_2019,
        )
    ],
    gitlab_issues: GroupGitLabIssues | None = None,
    language: GroupLanguage = GroupLanguage.EN,
    name: str = "test-group",
    organization_id: str = random_uuid(),
    state: GroupState = GroupStateFaker(),
    business_id: str | None = None,
    business_name: str | None = None,
) -> Group:
    return Group(
        agent_token=agent_token,
        azure_issues=azure_issues,
        created_by=created_by,
        created_date=created_date,
        description=description,
        files=files,
        gitlab_issues=gitlab_issues,
        language=language,
        name=name,
        organization_id=organization_id,
        state=state,
        business_id=business_id,
        business_name=business_name,
    )
