from datetime import (
    datetime,
)

from integrates.db_model.hook.enums import (
    HookEvent,
    HookStatus,
)
from integrates.db_model.hook.types import (
    GroupHook,
    HookState,
)
from integrates.testing.fakers.constants import (
    DATE_2019,
    EMAIL_FLUIDATTACKS_HACKER,
)
from integrates.testing.fakers.randoms import (
    random_uuid,
)


def GroupHookStateFaker(  # noqa: N802
    *,
    modified_by: str = EMAIL_FLUIDATTACKS_HACKER,
    modified_date: datetime = DATE_2019,
    status: HookStatus = HookStatus.ACTIVE,
) -> HookState:
    return HookState(
        modified_by=modified_by,
        modified_date=modified_date,
        status=status,
    )


def GroupHookFaker(  # noqa: N802
    *,
    group_name: str = "test-group",
    id: str = random_uuid(),  # noqa: A002
    entry_point: str = "url1",
    name: str = "Hook",
    token: str = "token",  # noqa: S107
    token_header: str = "token header",  # noqa: S107
    hook_events: set[HookEvent] = set([HookEvent.VULNERABILITY_CREATED]),
    state: HookState = GroupHookStateFaker(),
) -> GroupHook:
    return GroupHook(
        group_name=group_name,
        id=id,
        entry_point=entry_point,
        name=name,
        token=token,
        token_header=token_header,
        hook_events=hook_events,
        state=state,
    )
