from datetime import (
    datetime,
)

from integrates.db_model.toe_ports.types import (
    ToePort,
    ToePortState,
)
from integrates.testing.fakers.constants import (
    DATE_2019,
    EMAIL_FLUIDATTACKS_HACKER,
)
from integrates.testing.fakers.randoms import (
    random_uuid,
)


def ToePortStateFaker(  # noqa: N802
    *,
    attacked_at: datetime | None = None,
    attacked_by: str | None = None,
    be_present: bool = True,
    be_present_until: datetime | None = None,
    first_attack_at: datetime | None = None,
    has_vulnerabilities: bool = False,
    modified_by: str | None = EMAIL_FLUIDATTACKS_HACKER,
    modified_date: datetime = DATE_2019,
    seen_at: datetime | None = DATE_2019,
    seen_first_time_by: str | None = EMAIL_FLUIDATTACKS_HACKER,
) -> ToePortState:
    return ToePortState(
        attacked_at=attacked_at,
        attacked_by=attacked_by,
        be_present=be_present,
        be_present_until=be_present_until,
        first_attack_at=first_attack_at,
        has_vulnerabilities=has_vulnerabilities,
        modified_by=modified_by,
        modified_date=modified_date,
        seen_at=seen_at,
        seen_first_time_by=seen_first_time_by,
    )


def ToePortFaker(  # noqa: N802
    *,
    group_name: str = "test-group",
    address: str = "192.168.1.1",
    port: str = "8080",
    root_id: str = random_uuid(),
    state: ToePortState = ToePortStateFaker(),
) -> ToePort:
    return ToePort(
        group_name=group_name,
        address=address,
        port=port,
        root_id=root_id,
        state=state,
    )
