from datetime import datetime

DATE_2019 = datetime.fromisoformat("2019-04-10T12:59:52+00:00")
DATE_2020 = datetime.fromisoformat("2020-04-10T12:59:52+00:00")
DATE_2024 = datetime.fromisoformat("2024-04-10T12:59:52+00:00")
DATE_2025 = datetime.fromisoformat("2025-04-10T12:59:52+00:00")

EMAIL_FLUIDATTACKS_ADMIN = "admin@fluidattacks.com"
EMAIL_FLUIDATTACKS_CUSTOMER_MANAGER = "customer_manager@fluidattacks.com"
EMAIL_FLUIDATTACKS_GROUP_MANAGER = "group_manager@fluidattacks.com"
EMAIL_FLUIDATTACKS_HACKER = "hacker@fluidattacks.com"
EMAIL_FLUIDATTACKS_MACHINE = "machine@fluidattacks.com"
EMAIL_FLUIDATTACKS_ORG_MANAGER = "org_manager@fluidattacks.com"
EMAIL_FLUIDATTACKS_REATTACKER = "reattacker@fluidattacks.com"
EMAIL_FLUIDATTACKS_RESOURCER = "resourcer@fluidattacks.com"
EMAIL_FLUIDATTACKS_REVIEWER = "reviewer@fluidattacks.com"
EMAIL_FLUIDATTACKS_SERVICE_FORCES = "forces.test-group@fluidattacks.com"
EMAIL_FLUIDATTACKS_UNKNOWN = "unknown@fluidattacks.com"
EMAIL_FLUIDATTACKS_USER = "user@fluidattacks.com"
EMAIL_FLUIDATTACKS_USER = "user@fluidattacks.com"
EMAIL_FLUIDATTACKS_VULNERABILITY_MANAGER = "vulnerability_manager@fluidattacks.com"
EMAIL_GENERIC = "jane@entry.com"
