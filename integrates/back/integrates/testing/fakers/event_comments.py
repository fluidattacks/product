from datetime import (
    datetime,
)

from integrates.db_model.event_comments.types import (
    EventComment,
    EventCommentsRequest,
)
from integrates.testing.fakers.constants import (
    DATE_2019,
    EMAIL_FLUIDATTACKS_HACKER,
)
from integrates.testing.fakers.randoms import (
    random_uuid,
)


def EventCommentFaker(  # noqa: N802
    *,
    event_id: str = random_uuid(),
    id: str = random_uuid(),  # noqa: A002
    parent_id: str = random_uuid(),
    creation_date: datetime = DATE_2019,
    content: str = "Test event comment",
    email: str = EMAIL_FLUIDATTACKS_HACKER,
    group_name: str = "test-group",
    full_name: str | None = None,
) -> EventComment:
    return EventComment(
        event_id=event_id,
        id=id,
        parent_id=parent_id,
        creation_date=creation_date,
        content=content,
        email=email,
        group_name=group_name,
        full_name=full_name,
    )


def EventCommentsRequestFaker(  # noqa: N802
    *,
    event_id: str = random_uuid(),
    group_name: str = "test-group",
) -> EventCommentsRequest:
    return EventCommentsRequest(
        event_id=event_id,
        group_name=group_name,
    )
