from datetime import (
    datetime,
)

from integrates.db_model.enums import (
    TreatmentStatus,
)
from integrates.db_model.organization_finding_policies.enums import (
    PolicyStateStatus,
)
from integrates.db_model.organization_finding_policies.types import (
    OrgFindingPolicy,
    OrgFindingPolicyState,
)
from integrates.testing.fakers.constants import (
    DATE_2019,
    EMAIL_GENERIC,
)
from integrates.testing.fakers.randoms import (
    random_uuid,
)


def OrgFindingPolicyStateFaker(  # noqa: N802
    *,
    modified_by: str = EMAIL_GENERIC,
    modified_date: datetime = DATE_2019,
    status: PolicyStateStatus = PolicyStateStatus.ACTIVE,
) -> OrgFindingPolicyState:
    return OrgFindingPolicyState(
        modified_by=modified_by,
        modified_date=modified_date,
        status=status,
    )


def OrgFindingPolicyFaker(  # noqa: N802
    *,
    id: str = random_uuid(),  # noqa: A002
    name: str = "Test Policy",
    organization_name: str = "Test Organization",
    state: OrgFindingPolicyState = OrgFindingPolicyStateFaker(),
    tags: set[str] = set(),
    treatment_acceptance: TreatmentStatus = (TreatmentStatus.ACCEPTED),
) -> OrgFindingPolicy:
    return OrgFindingPolicy(
        id=id,
        name=name,
        organization_name=organization_name,
        state=state,
        tags=tags,
        treatment_acceptance=treatment_acceptance,
    )
