from datetime import (
    datetime,
)
from decimal import (
    Decimal,
)

from integrates.db_model.toe_packages.types import (
    Artifact,
    Digest,
    ToePackage,
    ToePackageAdvisory,
    ToePackageCoordinates,
    ToePackageHealthMetadata,
)
from integrates.testing.fakers.constants import (
    DATE_2019,
    DATE_2024,
)
from integrates.testing.fakers.randoms import (
    random_uuid,
)


def DigestFaker(  # noqa: N802
    *,
    algorithm: str | None = "SHA256",
    value: str = ("9a0d8bb8b59c1e02c8e49d7d8b5d45b9b1f845f60e93be1f1b6a5c392e57b25d"),
) -> Digest:
    return Digest(
        algorithm=algorithm,
        value=value,
    )


def ArtifactFaker(  # noqa: N802
    *,
    url: str = "https://example.com/file.zip",
    integrity: Digest | None = DigestFaker(),
) -> Artifact:
    return Artifact(
        url=url,
        integrity=integrity,
    )


def ToePackageHealthMetadataFaker(  # noqa: N802
    *,
    artifact: Artifact | None = ArtifactFaker(),
    authors: str | None = "John Doe <johndoe@example.com>",
    latest_version_created_at: str | None = DATE_2024.isoformat(),
    latest_version: str | None = "2.0.0",
) -> ToePackageHealthMetadata:
    return ToePackageHealthMetadata(
        artifact=artifact,
        authors=authors,
        latest_version_created_at=latest_version_created_at,
        latest_version=latest_version,
    )


def ToePackageAdvisoryFaker(  # noqa: N802
    *,
    cpes: list[str] = [
        "cpe:2.3:a:examplevendor:examplepackage:1.0:*:*:*:*:*:*:*",
        "cpe:2.3:a:dummyvendor:dummypackage:2.5:*:*:*:*:*:*:*",
    ],
    description: str | None = "Test package for testing in Python",
    epss: Decimal | None = Decimal("0.00128"),
    id: str = random_uuid(),  # noqa: A002
    namespace: str = "nvd:cpe",
    percentile: Decimal | None = Decimal("0.47635"),
    severity: str = "medium",
    urls: list[str] = [
        "https://example.com/advisory/2024/001",
        "https://dummy-site.com/advisory/2024/002",
    ],
    version_constraint: str | None = "<=1.1.0",
) -> ToePackageAdvisory:
    return ToePackageAdvisory(
        cpes=cpes,
        description=description,
        epss=epss,
        id=id,
        namespace=namespace,
        percentile=percentile,
        severity=severity,
        urls=urls,
        version_constraint=version_constraint,
    )


def ToePackageCoordinatesFaker(  # noqa: N802
    *,
    dependency_type: str | None = "UNKNOWN",
    scope: str | None = "PROD",
    path: str = "poetry.lock",
    line: str | None = "85",
    layer: str | None = None,
    image_ref: str | None = None,
) -> ToePackageCoordinates:
    return ToePackageCoordinates(
        scope=scope,
        dependency_type=dependency_type,
        path=path,
        line=line,
        layer=layer,
        image_ref=image_ref,
    )


def ToePackageFaker(  # noqa: N802
    *,
    id: str = random_uuid(),  # noqa: A002
    be_present: bool = True,
    group_name: str = "test-group",
    root_id: str = random_uuid(),
    name: str = "test-toe-package",
    version: str = "1.0.0",
    type_: str = "python",
    language: str = "python",
    modfied_date: datetime = DATE_2019,
    locations: list[ToePackageCoordinates] = [ToePackageCoordinatesFaker()],
    found_by: str | None = "python-poetry-lock-cataloger",
    outdated: bool | None = True,
    package_advisories: list[ToePackageAdvisory] = [ToePackageAdvisoryFaker()],
    package_url: str | None = "pkg:pypi/testpackage@1.0.0",
    platform: str | None = "PIP",
    licenses: list[str] | None = ["Apache-2.0"],
    url: str | None = "https://pypi.org/project/testpackage/1.0.0/",
    vulnerable: bool | None = True,
    has_related_vulnerabilities: bool | None = True,
    vulnerability_ids: set[str] | None = set(random_uuid()),
    health_metadata: ToePackageHealthMetadata | None = ToePackageHealthMetadataFaker(),
) -> ToePackage:
    return ToePackage(
        id=id,
        be_present=be_present,
        group_name=group_name,
        root_id=root_id,
        name=name,
        version=version,
        type_=type_,
        language=language,
        modified_date=modfied_date,
        locations=locations,
        found_by=found_by,
        outdated=outdated,
        package_advisories=package_advisories,
        package_url=package_url,
        platform=platform,
        licenses=licenses,
        url=url,
        vulnerable=vulnerable,
        has_related_vulnerabilities=has_related_vulnerabilities,
        vulnerability_ids=vulnerability_ids,
        health_metadata=health_metadata,
    )
