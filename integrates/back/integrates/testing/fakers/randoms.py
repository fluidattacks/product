import random
import uuid
from time import time


def random_comment_id() -> str:
    return str(round(time() * 1000))


def random_uuid() -> str:
    return str(uuid.uuid4())


def random_ipv4() -> str:
    return ".".join(str(random.randint(0, 255)) for _ in range(4))  # noqa: S311
