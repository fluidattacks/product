from datetime import (
    datetime,
)

from integrates.db_model.organization_access.types import (
    OrganizationAccess,
    OrganizationAccessState,
    OrganizationInvitation,
)
from integrates.testing.fakers.constants import (
    DATE_2019,
    EMAIL_GENERIC,
)
from integrates.testing.fakers.randoms import (
    random_uuid,
)


def OrganizationAccessStateFaker(  # noqa: N802
    *,
    modified_date: datetime = DATE_2019,
    modified_by: str = EMAIL_GENERIC,
    has_access: bool = True,
    invitation: OrganizationInvitation | None = None,
    role: str | None = "organization_manager",
) -> OrganizationAccessState:
    return OrganizationAccessState(
        modified_date=modified_date,
        modified_by=modified_by,
        has_access=has_access,
        invitation=invitation,
        role=role,
    )


def OrganizationAccessFaker(  # noqa: N802
    *,
    organization_id: str = random_uuid(),
    email: str = EMAIL_GENERIC,
    state: OrganizationAccessState = OrganizationAccessStateFaker(),
    expiration_time: int | None = None,
) -> OrganizationAccess:
    return OrganizationAccess(
        organization_id=organization_id,
        email=email,
        state=state,
        expiration_time=expiration_time,
    )
