from datetime import (
    datetime,
)

from integrates.db_model.stakeholders.types import (
    AccessTokens,
    NotificationsPreferences,
    Stakeholder,
    StakeholderPhone,
    StakeholderSessionToken,
    StakeholderState,
    StakeholderTours,
    StateSessionType,
    TrustedDevice,
)
from integrates.testing.fakers.constants import (
    DATE_2019,
    EMAIL_GENERIC,
)
from integrates.testing.fakers.randoms import (
    random_ipv4,
    random_uuid,
)


def AccessTokensFaker(  # noqa: N802
    *,
    id: str = random_uuid(),  # noqa: A002
    issued_at: int = int(DATE_2019.timestamp()),
    jti_hashed: str = "hashed",
    salt: str = "salt",
    name: str = "Token",
    last_use: datetime = DATE_2019,
) -> AccessTokens:
    return AccessTokens(
        id=id,
        issued_at=issued_at,
        jti_hashed=jti_hashed,
        salt=salt,
        name=name,
        last_use=last_use,
    )


def StakeholderSessionTokenFaker(  # noqa: N802
    *,
    jti: str = "",
    state: StateSessionType = StateSessionType.IS_VALID,
) -> StakeholderSessionToken:
    return StakeholderSessionToken(
        jti=jti,
        state=state,
    )


def StakeholderPhoneFaker(  # noqa: N802
    *,
    country_code: str = "CO",
    calling_country_code: str = "57",
    national_number: str = "3001112233",
) -> StakeholderPhone:
    return StakeholderPhone(
        country_code=country_code,
        calling_country_code=calling_country_code,
        national_number=national_number,
    )


def TrustedDeviceFaker(  # noqa: N802
    *,
    ip_address: str = random_ipv4(),
    device: str = "",
    location: str = "Colombia",
    browser: str = "Chrome",
    first_login_date: datetime = DATE_2019,
    last_login_date: datetime = DATE_2019,
    otp_token_jti: str | None = None,
    last_attempt: datetime | None = None,
) -> TrustedDevice:
    return TrustedDevice(
        ip_address=ip_address,
        device=device,
        location=location,
        browser=browser,
        first_login_date=first_login_date,
        last_login_date=last_login_date,
        otp_token_jti=otp_token_jti,
        last_attempt=last_attempt,
    )


def StakeholderStateFaker(  # noqa: N802
    *,
    modified_by: str = EMAIL_GENERIC,
    modified_date: datetime = DATE_2019,
    trusted_devices: list[TrustedDevice] = [TrustedDeviceFaker()],
    notifications_preferences: NotificationsPreferences = (NotificationsPreferences()),
) -> StakeholderState:
    return StakeholderState(
        notifications_preferences=notifications_preferences,
        modified_by=modified_by,
        modified_date=modified_date,
        trusted_devices=trusted_devices,
    )


def StakeholderFaker(  # noqa: N802
    *,
    email: str = EMAIL_GENERIC,
    role: str = "user",
    first_name: str = "John",
    last_name: str = "Doe",
    subject: str | None = None,
    last_login_date: datetime = DATE_2019,
    registration_date: datetime = DATE_2019,
    aws_customer_id: str | None = None,
    enrolled: bool = False,
    is_concurrent_session: bool = False,
    legal_remember: bool = False,
    is_registered: bool = True,
    jira_client_key: str | None = None,
    session_key: str | None = None,
    session_token: StakeholderSessionToken = StakeholderSessionTokenFaker(),
    access_tokens: list[AccessTokens] = [
        AccessTokensFaker(),
    ],
    state: StakeholderState = StakeholderStateFaker(),
    phone: StakeholderPhone | None = StakeholderPhoneFaker(),
    tours: StakeholderTours = StakeholderTours(),
) -> Stakeholder:
    return Stakeholder(
        email=email,
        role=role,
        first_name=first_name,
        last_name=last_name,
        subject=subject,
        last_login_date=last_login_date,
        registration_date=registration_date,
        aws_customer_id=aws_customer_id,
        enrolled=enrolled,
        is_concurrent_session=is_concurrent_session,
        legal_remember=legal_remember,
        is_registered=is_registered,
        jira_client_key=jira_client_key,
        session_key=session_key,
        session_token=session_token,
        access_tokens=access_tokens,
        state=state,
        phone=phone,
        tours=tours,
    )
