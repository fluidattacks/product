from integrates.db_model.jira_installations.types import (
    JiraInstallState,
    JiraSecurityInstall,
)


def JiraSecurityInstallFaker(  # noqa: N802
    *,
    associated_email: str = "",
    base_url: str = "https://test_url.com",
    client_key: str = "00000000-0000-0000-0000-000000000000",
    cloud_id: str = "00000000-0000-0000-0000-000000000000",
    description: str = "test_description",
    display_url: str = "https://testing-testing.com",
    event_type: str = "installed",
    installation_id: str = (
        "ari:cloud:ecosystem::installation/00000000-0000-0000-0000-000000000000"
    ),
    key: str = "com.testing.testing-jira-app",
    plugins_version: str = "1001.0.0-SNAPSHOT",
    product_type: str = "jira",
    public_key: str = (
        "hlybwXMQIETJzKkS6jbBzvH0LzL4PDxtmIASO"
        "A0tMp9EDAE5Q4ukNUkjvGVB7S6AjlG1RrogO6Y"
        "3H22oOfrdCgaghXFApMtInSnRo9lNWdpzsY8rqq"
        "3fV4fTJwZIRcHhwvx5H7ncjdZxndaq87TOoxOS9GV"
        "OsUJqoum0xwMSASvxtckj01cAFj99JRiTj2jako5jUAJjens9870be32mASE"
    ),
    server_version: str = "100100",
    shared_secret: str = (
        "IvRLesJmAybihOsZQJ8tt6c5OSK8bL8mxx8fxVyKVE"  # noqa: S107
        "6GxQyrP42nrqdfnAzUCwkzKofoevreaDsFViY2OEEx8YqSG2NgHzVTpL"
    ),
    state: JiraInstallState = JiraInstallState(
        modified_by="test_user",
        modified_date="2021-01-01",
        used_api_token="test_token",  # noqa: S106
        used_jira_jwt="test_jwt",
    ),
) -> JiraSecurityInstall:
    return JiraSecurityInstall(
        associated_email=associated_email,
        base_url=base_url,
        client_key=client_key,
        cloud_id=cloud_id,
        description=description,
        display_url=display_url,
        event_type=event_type,
        installation_id=installation_id,
        key=key,
        plugins_version=plugins_version,
        product_type=product_type,
        public_key=public_key,
        server_version=server_version,
        shared_secret=shared_secret,
        state=state,
    )
