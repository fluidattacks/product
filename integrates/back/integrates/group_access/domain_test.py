from integrates.dataloaders import (
    get_new_context,
)
from integrates.group_access.domain import get_managers
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    GroupAccessFaker,
    GroupAccessStateFaker,
    GroupFaker,
    StakeholderFaker,
)
from integrates.testing.mocks import mocks

MANAGERS = ["jdoe@company.com", "jadoe@company.com", "jdoe3@company.com", "vun_manager@company.com"]


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            groups=[
                GroupFaker(name="group1"),
            ],
            group_access=[
                GroupAccessFaker(
                    group_name="group1",
                    email="vun_manager@company.com",
                    state=GroupAccessStateFaker(role="vulnerability_manager"),
                ),
                *[
                    GroupAccessFaker(
                        group_name="group1",
                        email=email,
                        state=GroupAccessStateFaker(role="group_manager"),
                    )
                    for email in MANAGERS[:3]
                ],
            ],
            stakeholders=[StakeholderFaker(email=email) for email in MANAGERS],
        ),
    )
)
async def test_get_registered_domain() -> None:
    loaders = get_new_context()
    result = await get_managers(loaders, "group1")

    for manager in MANAGERS:
        assert manager in result
