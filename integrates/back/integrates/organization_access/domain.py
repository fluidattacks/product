from aioextensions import (
    collect,
)

from integrates import (
    authz,
)
from integrates.custom_exceptions import (
    OrganizationNotFound,
)
from integrates.custom_utils import (
    datetime as datetime_utils,
)
from integrates.custom_utils.stakeholders import (
    fill_members,
)
from integrates.custom_utils.validations import (
    is_fluid_staff,
)
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model import (
    organization_access as org_access_model,
)
from integrates.db_model.organization_access.types import (
    OrganizationAccessMetadataToUpdate,
    OrganizationAccessRequest,
    OrganizationAccessState,
)
from integrates.db_model.stakeholders.types import (
    Stakeholder,
)


async def add_access(
    loaders: Dataloaders,
    organization_id: str,
    email: str,
    role: str,
    modified_by: str,
) -> None:
    loaders.organization_access.clear(
        OrganizationAccessRequest(organization_id=organization_id, email=email),
    )
    org_access = await loaders.organization_access.load(
        OrganizationAccessRequest(organization_id=organization_id, email=email),
    )
    await org_access_model.update_metadata(
        organization_id=organization_id,
        email=email,
        metadata=OrganizationAccessMetadataToUpdate(
            state=OrganizationAccessState(
                modified_date=datetime_utils.get_utc_now(),
                modified_by=modified_by,
                has_access=True,
            )
            if org_access is None
            else org_access.state._replace(
                modified_date=datetime_utils.get_utc_now(),
                modified_by=modified_by,
                has_access=True,
            ),
        ),
    )
    await authz.grant_organization_level_role(loaders, email, organization_id, role, modified_by)


async def get_stakeholder_organizations_ids(
    loaders: Dataloaders, email: str, active: bool
) -> list[str]:
    return [
        access.organization_id
        for access in await loaders.stakeholder_organizations_access.load(email)
        if access.state.has_access == active
    ]


async def get_organization_members_emails(
    loaders: Dataloaders,
    organization_id: str,
    active: bool = True,
) -> list[str]:
    members_access = await loaders.organization_stakeholders_access.load(organization_id)
    active_members_email = [access.email for access in members_access if access.state.has_access]
    if active:
        return active_members_email

    return [access.email for access in members_access if access.email not in active_members_email]


async def get_organization_members_emails_by_preferences(
    *,
    loaders: Dataloaders,
    organization_id: str,
    notification: str | None,
    roles: set[str],
    exclude_trial: bool = False,
    only_fluid_staff: bool = False,
) -> list[str]:
    organization = await loaders.organization.load(organization_id)
    if not organization:
        raise OrganizationNotFound()
    trial = (
        await loaders.trial.load(organization.created_by)
        if "@" in organization.created_by
        else None
    )
    is_trial = not (trial and trial.completed)
    email_list = await get_members_email_by_roles(
        loaders=loaders,
        organization_id=organization_id,
        roles=roles,
    )
    members_data: list[Stakeholder] = []
    await fill_members(loaders, email_list, members_data)
    members_email = [
        member.email
        for member in members_data
        if (not notification or notification in member.state.notifications_preferences.email)
        and not (exclude_trial and is_trial)
        and not (only_fluid_staff and not is_fluid_staff(member.email))
    ]

    return members_email


async def get_members_email_by_roles(
    *,
    loaders: Dataloaders,
    organization_id: str,
    roles: set[str],
) -> list[str]:
    members = await get_organization_members_emails(loaders, organization_id, active=True)
    member_roles = await collect(
        tuple(
            authz.get_organization_level_role(loaders, member, organization_id)
            for member in members
        ),
    )
    email_list = [
        str(member)
        for member, member_role in zip(members, member_roles, strict=False)
        if member_role in roles
    ]
    return email_list


async def has_access(loaders: Dataloaders, organization_id: str, email: str) -> bool:
    if await authz.get_organization_level_role(loaders, email, organization_id) == "admin":
        return True

    if await loaders.organization_access.load(
        OrganizationAccessRequest(organization_id=organization_id, email=email),
    ):
        return True
    return False
