import asyncio
import json
import logging
from datetime import (
    datetime,
)

from authlib.integrations.starlette_client import (
    OAuthError,
)
from azure.devops.connection import (
    AzureDevOpsClientRequestError,
    Connection,
)
from azure.devops.credentials import (
    BasicTokenAuthentication,
)
from azure.devops.v7_1.core import (
    CoreClient,
    IdentityRef,
    TeamProjectReference,
    WebApiTeam,
)
from starlette.requests import (
    Request,
)
from starlette.responses import (
    RedirectResponse,
    Response,
)

from integrates.authz.enforcer import (
    get_group_level_enforcer,
)
from integrates.context import (
    FI_AZURE_OAUTH2_ISSUES_SECRET,
    FI_AZURE_OAUTH2_ISSUES_SECRET_OLD,
)
from integrates.custom_exceptions import (
    InvalidAuthorization,
)
from integrates.custom_utils import (
    templates,
)
from integrates.custom_utils.groups import get_group
from integrates.custom_utils.requests import (
    get_redirect_url,
)
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model.groups.types import (
    GroupAzureIssues,
    GroupMetadataToUpdate,
)
from integrates.groups import (
    domain as groups_domain,
)
from integrates.oauth import (
    OAUTH,
    azure,
)
from integrates.oauth.azure import (
    get_azure_refresh_token,
)
from integrates.sessions.domain import (
    get_jwt_content,
)

LOGGER = logging.getLogger(__name__)


async def _get_fresh_token(
    *,
    redirect_uri: str,
    refresh_token: str,
) -> dict | None:
    new = await azure.get_fresh_token(
        redirect_uri=redirect_uri,
        refresh_token=refresh_token,
        secret=FI_AZURE_OAUTH2_ISSUES_SECRET,
    )
    if new:
        return new
    return await azure.get_fresh_token(
        redirect_uri=redirect_uri,
        refresh_token=refresh_token,
        secret=FI_AZURE_OAUTH2_ISSUES_SECRET_OLD,
    )


async def get_organization_names(settings: GroupAzureIssues) -> list[str]:
    token_data = await _get_fresh_token(
        redirect_uri=settings.redirect_uri,
        refresh_token=settings.refresh_token,
    )
    if token_data is None:
        return []

    token = token_data["access_token"]
    profile = await azure.get_azure_profile(token=token)
    if profile is None:
        return []

    accounts = await azure.get_azure_accounts(
        as_a_member=True,
        public_alias=profile,
        token=token,
    )
    return [account.name for account in accounts]


async def get_project_names(
    *,
    azure_organization: str | None,
    settings: GroupAzureIssues,
) -> list[str]:
    token_data = await _get_fresh_token(
        redirect_uri=settings.redirect_uri,
        refresh_token=settings.refresh_token,
    )
    if token_data is None:
        return []

    connection = Connection(
        base_url=f"https://dev.azure.com/{azure_organization}",
        creds=BasicTokenAuthentication({"access_token": token_data["access_token"]}),
    )
    try:
        client: CoreClient = await asyncio.to_thread(connection.clients_v7_1.get_core_client)
        projects: list[TeamProjectReference] = await asyncio.to_thread(client.get_projects)
        return [project.name for project in projects]
    except AzureDevOpsClientRequestError as exc:
        if "404 status code" in str(exc):
            return []
        LOGGER.exception(exc)
        return []


async def get_project_members(
    *,
    azure_organization: str,
    azure_project: str,
    settings: GroupAzureIssues,
) -> list[IdentityRef]:
    token_data = await _get_fresh_token(
        redirect_uri=settings.redirect_uri,
        refresh_token=settings.refresh_token,
    )
    if token_data is None:
        return []

    connection = Connection(
        base_url=f"https://dev.azure.com/{azure_organization}",
        creds=BasicTokenAuthentication({"access_token": token_data["access_token"]}),
    )
    try:
        client: CoreClient = await asyncio.to_thread(connection.clients_v7_1.get_core_client)
        teams: list[WebApiTeam] = await asyncio.to_thread(
            client.get_teams,
            project_id=azure_project,
        )
        members: list[IdentityRef] = [
            member.identity
            for team_members in await asyncio.gather(
                *[
                    asyncio.to_thread(
                        client.get_team_members_with_extended_properties,
                        project_id=azure_project,
                        team_id=team.id,
                    )
                    for team in teams
                ],
            )
            for member in team_members
        ]

        return members
    except AzureDevOpsClientRequestError as exc:
        if "404 status code" in str(exc):
            return []
        LOGGER.exception(exc)
        return []


async def update_settings(
    *,
    assigned_to: str,
    azure_organization_name: str,
    azure_project_name: str,
    group_name: str,
    issue_automation_enabled: bool,
    loaders: Dataloaders,
    tags: list[str],
) -> None:
    group = await get_group(loaders, group_name)
    if not group.azure_issues:
        return
    await groups_domain.update_metadata(
        group_name=group_name,
        metadata=GroupMetadataToUpdate(
            azure_issues=group.azure_issues._replace(
                assigned_to=assigned_to,
                azure_organization=azure_organization_name,
                azure_project=azure_project_name,
                issue_automation_enabled=issue_automation_enabled,
                tags=tags,
            ),
        ),
        organization_id=group.organization_id,
    )


async def update_token(
    *,
    group_name: str,
    loaders: Dataloaders,
    redirect_uri: str,
    refresh_token: str,
) -> None:
    group = await get_group(loaders, group_name)
    await groups_domain.update_metadata(
        group_name=group_name,
        metadata=GroupMetadataToUpdate(
            azure_issues=GroupAzureIssues(
                connection_date=datetime.now(),
                redirect_uri=redirect_uri,
                refresh_token=refresh_token,
            ),
        ),
        organization_id=group.organization_id,
    )


async def _is_oauth_authorized(
    loaders: Dataloaders,
    request: Request,
    group_name: str,
) -> bool:
    try:
        user = await get_jwt_content(request)
        enforcer = await get_group_level_enforcer(loaders, user["user_email"])
        return enforcer(group_name, "oauth_azure_issues")
    except InvalidAuthorization:
        return False


async def begin_oauth_flow(request: Request) -> Response:
    group_name = request.query_params["groupName"]
    loaders = get_new_context()

    if not await _is_oauth_authorized(loaders, request, group_name):
        return templates.login(request)

    redirect_uri = get_redirect_url(request, "end_azure_issues_oauth")
    return await OAUTH.azure_issues.authorize_redirect(
        request,
        redirect_uri,
        state=json.dumps({"group_name": group_name}),
    )


async def end_oauth_flow(request: Request) -> Response:
    try:
        state = json.loads(request.query_params["state"])
        group_name = state["group_name"]
        loaders = get_new_context()

        if not await _is_oauth_authorized(loaders, request, group_name):
            return templates.login(request)

        redirect_uri = get_redirect_url(request, "end_azure_issues_oauth")
        token_data = await get_azure_refresh_token(
            code=request.query_params["code"],
            redirect_uri=redirect_uri,
            secret=FI_AZURE_OAUTH2_ISSUES_SECRET,
        )
        if not token_data:
            raise OAuthError()
        await update_token(
            group_name=group_name,
            loaders=loaders,
            redirect_uri=redirect_uri,
            refresh_token=token_data["refresh_token"],
        )
        return RedirectResponse(url="/integrations")
    except (json.JSONDecodeError, KeyError, OAuthError):
        return templates.login(request)
