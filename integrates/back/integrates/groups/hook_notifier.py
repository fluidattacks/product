import logging
import logging.config
from collections.abc import (
    Iterable,
)
from typing import (
    Any,
)

import aiohttp
import fluidattacks_core.http
from aioextensions import (
    collect,
    in_thread,
)
from fluidattacks_core.http.validations import (
    HTTPValidationError,
)

from integrates.custom_exceptions import (
    HostNotFound,
)
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.hook.enums import (
    HookStatus,
)
from integrates.db_model.hook.types import (
    GroupHook,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)
LOGGER = logging.getLogger(__name__)
TRANSACTIONS_LOGGER: logging.Logger = logging.getLogger("transactional")


def filter_hooks_with_event(
    event: str,
    hooks: Iterable[GroupHook],
) -> list[GroupHook]:
    return [
        hook
        for hook in hooks
        if event in hook.hook_events and hook.state.status == HookStatus.ACTIVE
    ]


async def _notify_client(event: str, info: dict[str, str], group: str, hook: GroupHook) -> None:
    LOGGER.info(
        "Send notification to hook client",
        extra={
            "extra": {
                "group_name": group,
                "event": event,
                "info": info,
                "hook_url": hook.entry_point,
            },
        },
    )
    try:
        response = await fluidattacks_core.http.request(
            hook.entry_point,
            method="POST",
            headers={hook.token_header: hook.token}
            if hook.token_header
            else {"x-api-key": hook.token},
            json={"group": group, "event": event, "info": info},
        )
        result = await response.json()

        if not response.ok:
            LOGGER.error(
                "Failed to send notification to hook client",
                extra={
                    "extra": {
                        **result,
                    },
                },
            )

        return result
    except (TimeoutError, aiohttp.ClientError, HTTPValidationError) as exc:
        LOGGER.error("%s", exc, extra={"extra": {"hook_url": hook.entry_point}})


async def check_hook(event: str, group: str, hook: dict[str, Any]) -> None:
    try:
        response = await fluidattacks_core.http.request(
            hook["entry_point"],
            method="POST",
            headers={hook["token_header"]: hook["token"]}
            if hook["token_header"]
            else {"x-api-key": hook["token"]},
            json={"group": group, "event": event},
        )

        if not response.ok:
            raise HostNotFound()
        return await response.json()
    except (TimeoutError, aiohttp.ClientError, HostNotFound, HTTPValidationError) as exc:
        LOGGER.error("%s", exc)
        raise HostNotFound() from exc


async def process_hook_event(
    loaders: Dataloaders,
    group_name: str,
    info: dict[str, Any],
    event: str,
) -> None:
    """Notifies external integrations"""
    await in_thread(
        TRANSACTIONS_LOGGER.info,
        "Processing hook event",
        extra={"group_name": group_name, "event": event},
    )
    group_hooks = await loaders.group_hook.load(group_name)
    group_hooks_with_event = filter_hooks_with_event(event=event, hooks=group_hooks)

    results = await collect(
        [
            _notify_client(event=event, info=info, group=group_name, hook=hook)
            for hook in group_hooks_with_event
        ],
        workers=4,
    )
    if results:
        await in_thread(
            TRANSACTIONS_LOGGER.info,
            "Processed hook event",
            extra={
                "group_name": group_name,
                "event": event,
                "results": str(results),
            },
        )
