import functools
import re
from collections.abc import (
    Callable,
)
from datetime import (
    datetime,
)
from typing import (
    ParamSpec,
    TypeVar,
)

from integrates.custom_exceptions import (
    HookInvalid,
    HookWithSameEntryPoint,
    InvalidAcceptanceSeverityRange,
    InvalidGroupServicesConfig,
    RepeatedValues,
)
from integrates.custom_utils import (
    validations,
)
from integrates.custom_utils.validations_deco import (
    validate_fields_deco,
    validate_length_deco,
)
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.groups.types import (
    GroupFile,
)
from integrates.db_model.hook.types import (
    GroupHook,
    GroupHookPayload,
)
from integrates.db_model.types import (
    PoliciesToUpdate,
)
from integrates.groups import (
    utils as groups_utils,
)
from integrates.vulnerabilities.domain.validations import (
    get_policy_max_acceptance_severity,
    get_policy_min_acceptance_severity,
)

P = ParamSpec("P")
T = TypeVar("T")


def validate_group_services_config_deco(
    has_essential_field: str,
    has_advanced_field: str,
    has_arm_field: str | bool,
) -> Callable[[Callable[P, T]], Callable[P, T]]:
    def wrapper(func: Callable[P, T]) -> Callable[P, T]:
        @functools.wraps(func)
        def decorated(*args: P.args, **kwargs: P.kwargs) -> T:
            has_essential = bool(kwargs.get(has_essential_field))
            has_advanced = bool(kwargs.get(has_advanced_field))
            has_arm = has_arm_field
            if isinstance(has_arm_field, str):
                has_arm = bool(kwargs.get(has_arm_field))
            if has_advanced:
                if not has_arm:
                    raise InvalidGroupServicesConfig("Advanced is only available when ASM is too")
                if not has_essential:
                    raise InvalidGroupServicesConfig(
                        "Advanced is only available when Machine is too",
                    )
            return func(*args, **kwargs)

        return decorated

    return wrapper


async def validate_group_tags(loaders: Dataloaders, group_name: str, tags: list[str]) -> list[str]:
    """Validate tags array."""
    pattern = re.compile("^[a-z0-9]+(?:-[a-z0-9]+)*$")
    if await groups_utils.has_repeated_tags(loaders, group_name, tags):
        raise RepeatedValues()
    return [tag for tag in tags if pattern.match(tag)]


@validate_fields_deco(["description"])
@validate_length_deco("description", max_length=200)
def validate_file_data(
    *,
    description: str,
    file_name: str,
    email: str,
    modified_date: datetime,
) -> GroupFile:
    validations.validate_file_name_scope(name=file_name)
    return GroupFile(
        description=description,
        file_name=file_name,
        modified_by=email,
        modified_date=modified_date,
    )


async def validate_acceptance_severity_range(
    *,
    group_name: str,
    loaders: Dataloaders,
    values: PoliciesToUpdate,
) -> bool:
    success: bool = True
    min_acceptance_severity = await get_policy_min_acceptance_severity(
        loaders=loaders,
        group_name=group_name,
    )
    max_acceptance_severity = await get_policy_max_acceptance_severity(
        loaders=loaders,
        group_name=group_name,
    )
    min_value = (
        values.min_acceptance_severity
        if values.min_acceptance_severity is not None
        else min_acceptance_severity
    )
    max_value = (
        values.max_acceptance_severity
        if values.max_acceptance_severity is not None
        else max_acceptance_severity
    )
    if min_value is not None and max_value is not None and (min_value > max_value):
        raise InvalidAcceptanceSeverityRange()
    return success


def validate_hook_add(hook: GroupHookPayload, group_hooks: list[GroupHook]) -> None:
    if len(hook.hook_events) == 0:
        raise HookInvalid()
    group_hooks_filtered = [
        group_hook for group_hook in group_hooks if hook.entry_point == group_hook.entry_point
    ]

    if len(group_hooks_filtered) > 0:
        raise HookWithSameEntryPoint()
