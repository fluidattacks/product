import os
import tempfile

from integrates.custom_utils.roots import get_active_git_root
from integrates.dataloaders import get_new_context
from integrates.db_model.groups.types import GroupUnreliableIndicators
from integrates.db_model.roots.types import RootUnreliableIndicators
from integrates.db_model.types import CodeLanguage
from integrates.groups import languages_distribution
from integrates.testing.aws import (
    GroupUnreliableIndicatorsToUpdate,
    IntegratesAws,
    IntegratesDynamodb,
)
from integrates.testing.fakers import (
    GitRootFaker,
    GitRootStateFaker,
    GroupFaker,
    OrganizationAccessFaker,
    OrganizationAccessStateFaker,
    OrganizationFaker,
    StakeholderFaker,
)
from integrates.testing.mocks import Mock, mocks

ORG_ID = "ORG#6b110f25-cd46-e94c-2bad-b9e182cf91a8"
ROOT_ID1 = "63298a73-9dff-46cf-b42d-9b2f01a56666"
ROOT_ID2 = "63298a73-9dff-46cf-b42d-9b2f01a57777"
ORG_NAME = "orgtest"
GROUP_NAME = "grouptest"
USER_EMAIL = "test@fluidattacks.com"


class MockTemporaryDirectory:
    def __init__(self, prefix: str, ignore_cleanup_errors: bool, *args: object):
        pass

    def __enter__(self) -> str:
        return os.path.join(
            os.path.dirname(__file__),
            "test_data",
            "test_get_roots_languages_distribution",
            "groups",
            "grouptest",
        )

    def __exit__(self, *args: object) -> None:
        pass


class MockRepo:
    _working_dir: str

    def __init__(self, working_dir: str):
        self._working_dir = working_dir

    @property
    def working_dir(self) -> str:
        return self._working_dir


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            groups=[
                GroupFaker(
                    name=GROUP_NAME,
                    organization_id=ORG_ID,
                )
            ],
            group_unreliable_indicators=[
                GroupUnreliableIndicatorsToUpdate(
                    group_name=GROUP_NAME,
                    indicators=GroupUnreliableIndicators(
                        code_languages=[
                            CodeLanguage("Python", 1000),
                            CodeLanguage("JavaScript", 2000),
                            CodeLanguage("Nix", 300),
                        ]
                    ),
                )
            ],
            organizations=[OrganizationFaker(id=ORG_ID, name=ORG_NAME)],
            organization_access=[
                OrganizationAccessFaker(
                    organization_id=ORG_ID,
                    email=USER_EMAIL,
                    state=OrganizationAccessStateFaker(has_access=True, role="group_manager"),
                ),
            ],
            stakeholders=[StakeholderFaker(email=USER_EMAIL, enrolled=True)],
            roots=[
                GitRootFaker(
                    group_name=GROUP_NAME,
                    organization_name=ORG_NAME,
                    id=ROOT_ID1,
                    state=GitRootStateFaker(
                        nickname="git_1", url="https://gitlab.com/fluidattacks/git_1"
                    ),
                    unreliable_indicators=RootUnreliableIndicators(
                        unreliable_code_languages=[CodeLanguage(language="Python", loc=100)]
                    ),
                ),
                GitRootFaker(
                    group_name=GROUP_NAME,
                    organization_name=ORG_NAME,
                    id=ROOT_ID2,
                    state=GitRootStateFaker(
                        nickname="git_2", url="https://gitlab.com/fluidattacks/git_2"
                    ),
                    unreliable_indicators=RootUnreliableIndicators(
                        unreliable_code_languages=[CodeLanguage(language="Python", loc=100)]
                    ),
                ),
            ],
        ),
    ),
)
async def test_update_language_indicators() -> None:
    loaders = get_new_context()
    root1 = await get_active_git_root(loaders, ROOT_ID1, GROUP_NAME)
    root2 = await get_active_git_root(loaders, ROOT_ID2, GROUP_NAME)
    group_indicators = await loaders.group_unreliable_indicators.load(GROUP_NAME)
    assert group_indicators.code_languages == [
        CodeLanguage("Python", 1000),
        CodeLanguage("JavaScript", 2000),
        CodeLanguage("Nix", 300),
    ]

    await languages_distribution.update_language_indicators(
        group_name=GROUP_NAME,
        git_roots=[root1, root2],
        roots_languages=[
            [
                CodeLanguage("YAML", 5),
                CodeLanguage("TOML", 4),
                CodeLanguage("Python", 3),
                CodeLanguage("Plain Text", 2),
                CodeLanguage("JavaScript", 1),
                CodeLanguage("AWK", 0),
                CodeLanguage("Ada", -1),
            ],
            [
                CodeLanguage("JavaScript", 10),
                CodeLanguage("Plain Text", 20),
                CodeLanguage("Python", 30),
                CodeLanguage("TOML", 40),
                CodeLanguage("Zig", 60),
                CodeLanguage("Zsh", 0),
            ],
        ],
    )

    loaders = get_new_context()
    root1 = await get_active_git_root(loaders, ROOT_ID1, GROUP_NAME)
    root2 = await get_active_git_root(loaders, ROOT_ID2, GROUP_NAME)
    assert root1.unreliable_indicators.unreliable_code_languages == [
        CodeLanguage("JavaScript", 1),
        CodeLanguage("Plain Text", 2),
        CodeLanguage("Python", 3),
        CodeLanguage("TOML", 4),
        CodeLanguage("YAML", 5),
    ]
    assert root2.unreliable_indicators.unreliable_code_languages == [
        CodeLanguage("JavaScript", 10),
        CodeLanguage("Plain Text", 20),
        CodeLanguage("Python", 30),
        CodeLanguage("TOML", 40),
        CodeLanguage("Zig", 60),
    ]
    group_indicators = await loaders.group_unreliable_indicators.load(GROUP_NAME)
    assert group_indicators.code_languages == [
        CodeLanguage("JavaScript", 11),
        CodeLanguage("Plain Text", 22),
        CodeLanguage("Python", 33),
        CodeLanguage("TOML", 44),
        CodeLanguage("YAML", 5),
        CodeLanguage("Zig", 60),
    ]


@mocks(
    others=[
        Mock(
            languages_distribution,
            "download_repo",
            "async",
            MockRepo(
                os.path.join(
                    os.path.dirname(__file__),
                    "test_data",
                    "test_get_roots_languages_distribution",
                    "groups",
                    "grouptest",
                    "git_1",
                )
            ),
        ),
        Mock(tempfile, "TemporaryDirectory", "function", MockTemporaryDirectory),
    ],
)
async def test_get_root_code_languages() -> None:
    git_root = GitRootFaker(
        group_name=GROUP_NAME,
        organization_name=ORG_NAME,
        id=ROOT_ID1,
        state=GitRootStateFaker(nickname="git_1", url="https://gitlab.com/fluidattacks/git_1"),
        unreliable_indicators=RootUnreliableIndicators(
            unreliable_code_languages=[CodeLanguage(language="Python", loc=100)]
        ),
    )
    distribution = await languages_distribution.get_root_code_languages(git_root)
    assert distribution == [CodeLanguage("Markdown", 2), CodeLanguage("Python", 3)]
