// Example code for tests

const fs = require('fs');
const path = require('path');

module.exports = function (dir) {
  const files = fs.readdirSync(dir);
  let stats = {
    total: 0,
    js: 0,
    css: 0,
    html: 0
  };

  files.forEach(function (file) {
    const ext = path.extname(file);
    const count = fs.readFileSync(path.join(dir, file), 'utf-8').split('\n').length;
    stats.total += count;

    if (ext === '.js') {
      stats.js += count;
    } else if (ext === '.css') {
      stats.css += count;
    } else if (ext === '.html') {
      stats.html += count;
    }
  });

  return stats;
}
