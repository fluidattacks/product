import hashlib
import logging
import logging.config
from contextlib import suppress
from itertools import chain

from aioextensions import collect, in_thread
from azure.core.exceptions import DeserializationError
from azure.devops.client import AzureDevOpsAuthenticationError
from azure.devops.connection import Connection
from azure.devops.exceptions import (
    AzureDevOpsClientRequestError,
    AzureDevOpsServiceError,
    ClientRequestError,
)
from azure.devops.v7_1.git.models import GitRepository
from msrest.authentication import BasicAuthentication
from requests.exceptions import ChunkedEncodingError
from urllib3.util.url import parse_url

from integrates.dataloaders import Dataloaders
from integrates.db_model.azure_repositories.constants import MAX_BRANCHES
from integrates.db_model.azure_repositories.get import (
    get_github_branches_names,
    get_github_repos,
    get_github_repos_commits,
    get_oauth_repositories,
    get_oauth_repositories_commits,
)
from integrates.db_model.azure_repositories.types import (
    CredentialsGitRepository,
    CredentialsGitRepositoryCommit,
    GitRepositoryCommit,
    OGitRepository,
    ProjectStats,
)
from integrates.db_model.azure_repositories.utils import does_not_exist_in_gitroot_urls
from integrates.db_model.credentials.types import Credentials, OauthAzureSecret, OauthGithubSecret
from integrates.db_model.credentials.utils import filter_pat_credentials
from integrates.db_model.integration_repositories.types import OrganizationIntegrationRepository
from integrates.decorators import retry_on_exceptions
from integrates.oauth.azure import get_azure_accounts, get_azure_profile
from integrates.oauth.common import get_credential_token
from integrates.settings import LOGGING

logging.config.dictConfig(LOGGING)
LOGGER = logging.getLogger(__name__)


async def get_pat_credentials_authors_stats(
    credentials: list[Credentials],
    urls: set[str],
    nicknames: set[str],
    loaders: Dataloaders,
) -> set[str]:
    pat_credentials = filter_pat_credentials(credentials)
    all_repositories = await loaders.organization_integration_repositories.load_many(
        pat_credentials,
    )
    repositories: tuple[CredentialsGitRepository, ...] = tuple(
        CredentialsGitRepository(
            credential=credential,
            repository=repository,
        )
        for credential, _repositories in zip(pat_credentials, all_repositories, strict=False)
        for repository in _repositories
        if does_not_exist_in_gitroot_urls(
            repository=repository,
            urls=urls,
            nicknames=nicknames,
        )
    )
    repositories_authors: tuple[set[str], ...] = await collect(
        tuple(
            _get_missed_authors(loaders=loaders, repository=repository)
            for repository in repositories
        ),
        workers=1,
    )

    return set().union(*list(repositories_authors))


async def get_azure_branches_names(
    *,
    repository_id: str,
    project_name: str,
    loaders: Dataloaders,
    credential: Credentials,
    base_url: str,
) -> tuple[str, ...]:
    access_token = await get_credential_token(
        credential=credential,
        loaders=loaders,
    )
    if not access_token:
        return tuple()

    credentials = BasicAuthentication("", access_token)
    connection = Connection(base_url=base_url, creds=credentials)
    try:
        git_client = connection.clients_v7_1.get_git_client()
        branches = await in_thread(
            git_client.get_branches,
            repository_id=repository_id,
            project=project_name,
        )
        sorted_branches = sorted(
            branches,
            key=lambda branch: branch.commit.author.date,
            reverse=True,
        )
        branches_names = [branch.name for branch in sorted_branches][:MAX_BRANCHES]

    except DeserializationError as exc:
        LOGGER.error(
            "Error getting azure repo branches data",
            extra={
                "extra": {
                    "exception": exc,
                    "repository_id": repository_id,
                    "project_name": project_name,
                    "credential_id": credential.id,
                },
            },
        )
        return tuple()
    except (
        AzureDevOpsAuthenticationError,
        AzureDevOpsClientRequestError,
        AzureDevOpsServiceError,
    ) as exc:
        if str(exc).startswith("TF400813"):
            raise exc

        if any(str(exc).startswith(error) for error in ["TF401019", "VS403403"]):
            return tuple()

        LOGGER.error(
            "Error getting azure repo branches data",
            extra={
                "extra": {
                    "exception": exc,
                    "repository_id": repository_id,
                    "project_name": project_name,
                    "credential_id": credential.id,
                },
            },
        )
        raise exc
    else:
        return tuple(branches_names)


async def get_github_credentials_authors(
    *,
    credentials: list[Credentials],
    urls: set[str],
    nicknames: set[str],
) -> set[str]:
    stats: tuple[ProjectStats, ...] = tuple(
        chain.from_iterable(
            await collect(
                tuple(
                    _get_github_credential_stats(
                        credential=credential,
                        urls=urls,
                        nicknames=nicknames,
                        get_all=True,
                    )
                    for credential in credentials
                ),
                workers=1,
            ),
        ),
    )
    filtered_stats: tuple[ProjectStats, ...] = tuple(
        {stat.project.id: stat for stat in stats}.values(),
    )

    return {
        str(commit["author"]).lower()
        for stat in filtered_stats
        for commit in stat.commits
        if commit.get("author") is not None
    }


async def __get_azure_credentials_stats(
    *,
    credential: Credentials,
    loaders: Dataloaders,
    urls: set[str],
    nicknames: set[str],
) -> tuple[OGitRepository, ...]:
    token = await get_credential_token(
        credential=credential,
        loaders=loaders,
    )

    if token is None:
        return tuple()

    profile = await get_azure_profile(token=token)

    if profile is None:
        return tuple()
    all_accounts_names = tuple(
        chain.from_iterable(
            await collect(
                [
                    get_azure_accounts(token=token, public_alias=profile),
                    get_azure_accounts(token=token, public_alias=profile, as_a_member=True),
                ],
            ),
        ),
    )

    accounts_names = tuple(
        [tuple({account.base_url: account for account in all_accounts_names}.values())],
    )

    all_repositories: tuple[list[list[GitRepository]], ...] = await collect(
        tuple(
            get_oauth_repositories(
                token=token,
                base_urls=tuple(account.base_url for account in _accounts_names),
            )
            for _accounts_names in accounts_names
        ),
        workers=1,
    )

    repositories: tuple[OGitRepository, ...] = tuple(
        OGitRepository(
            token=token,
            repository=repository,
            account=account,
            credential=credential,
            is_oauth=isinstance(credential.secret, OauthAzureSecret),
        )
        for _accounts, _all_repositories in zip(accounts_names, all_repositories, strict=False)
        for _repositories, account in zip(_all_repositories, _accounts, strict=False)
        for repository in _repositories
        if does_not_exist_in_gitroot_urls(
            repository=repository,
            urls=urls,
            nicknames=nicknames,
        )
    )

    return repositories


async def get_azure_credentials_authors_stats(
    *,
    credentials: list[Credentials],
    urls: set[str],
    nicknames: set[str],
    loaders: Dataloaders,
) -> set[str]:
    with suppress(
        AzureDevOpsClientRequestError,
        AzureDevOpsAuthenticationError,
        AzureDevOpsServiceError,
        ChunkedEncodingError,
        ClientRequestError,
    ):
        return await _get_azure_credentials_authors_stats(
            credentials=credentials,
            loaders=loaders,
            urls=urls,
            nicknames=nicknames,
        )

    return set()


@retry_on_exceptions(
    exceptions=(AzureDevOpsServiceError,),
    sleep_seconds=float("2"),
    max_attempts=2,
)
async def _get_azure_credentials_authors_stats(
    *,
    credentials: list[Credentials],
    urls: set[str],
    nicknames: set[str],
    loaders: Dataloaders,
) -> set[str]:
    repositories: tuple[OGitRepository, ...] = tuple(
        chain.from_iterable(
            await collect(
                tuple(
                    __get_azure_credentials_stats(
                        credential=credential,
                        loaders=loaders,
                        urls=urls,
                        nicknames=nicknames,
                    )
                    for credential in credentials
                    if isinstance(credential.secret, OauthAzureSecret)
                ),
            ),
        ),
    )

    repositories_authors = await collect(
        tuple(
            _get_oauth_missed_authors(loaders=loaders, repository=repository)
            for repository in repositories
        ),
        workers=20,
    )

    return set().union(*list(repositories_authors))


def _get_id(repository: CredentialsGitRepository | OGitRepository) -> str:
    return hashlib.sha256(
        str(parse_url(repository.repository.web_url).url).lower().encode("utf-8"),
    ).hexdigest()


def __get_id(url: str) -> str:
    return hashlib.sha256(url.lower().encode("utf-8")).hexdigest()


def _get_branch(repository: CredentialsGitRepository | OGitRepository) -> str:
    return str(
        repository.repository.default_branch
        if repository.repository.default_branch is not None
        else "main",
    )


async def _get_missed_authors(
    *,
    loaders: Dataloaders,
    repository: CredentialsGitRepository,
) -> set[str]:
    git_commits = await loaders.organization_integration_repositories_commits.load(
        CredentialsGitRepositoryCommit(
            credential=repository.credential,
            project_name=repository.repository.project.name,
            repository_id=repository.repository.id,
            total=True,
        ),
    )

    if git_commits:
        return {
            commit.author.email.lower()
            for commit in git_commits
            if commit.author is not None and commit.author.email is not None
        }

    return set()


async def _get_oauth_missed_authors(
    *,
    loaders: Dataloaders,
    repository: OGitRepository,
) -> set[str]:
    access_token = await get_credential_token(
        credential=repository.credential,
        loaders=loaders,
    )
    if not access_token:
        return set()

    git_commits = await get_oauth_repositories_commits(
        repositories=[
            GitRepositoryCommit(
                account_name=repository.account.name,
                access_token=access_token,
                project_name=repository.repository.project.name,
                repository_id=repository.repository.id,
                base_url=repository.account.base_url,
                total=True,
            ),
        ],
    )

    if git_commits and git_commits[0]:
        return {
            str(commit.author.email).lower()
            for commit in git_commits[0]
            if commit.author is not None and commit.author.email is not None
        }

    return set()


async def get_azure_credentials_stats(
    *,
    credentials: list[Credentials],
    loaders: Dataloaders,
    urls: set[str],
    nicknames: set[str],
    organization_id: str,
) -> tuple[OrganizationIntegrationRepository, ...]:
    with suppress(
        AzureDevOpsClientRequestError,
        AzureDevOpsAuthenticationError,
        AzureDevOpsServiceError,
        ChunkedEncodingError,
        ClientRequestError,
    ):
        return await _get_azure_credentials_stats(
            credentials=credentials,
            loaders=loaders,
            urls=urls,
            nicknames=nicknames,
            organization_id=organization_id,
        )

    return tuple()


def _create_org_integration_repo(
    repository: OGitRepository,
    branches: tuple[str, ...],
    organization_id: str,
) -> OrganizationIntegrationRepository:
    branch = _get_branch(repository)
    branch_start = "refs/heads/"
    formated_branch = branch[len(branch_start) :] if branch.startswith(branch_start) else branch
    return OrganizationIntegrationRepository(
        id=_get_id(repository),
        organization_id=organization_id,
        branch=branch,
        last_commit_date=(repository.repository.project.last_update_time),
        url=parse_url(repository.repository.web_url).url,
        credential_id=repository.credential.id,
        branches=tuple(set(branches + tuple([formated_branch]))),
        name=repository.repository.name,
    )


@retry_on_exceptions(
    exceptions=(AzureDevOpsServiceError,),
    sleep_seconds=float("60"),
    max_attempts=2,
)
async def _get_azure_credentials_stats(
    *,
    credentials: list[Credentials],
    loaders: Dataloaders,
    urls: set[str],
    nicknames: set[str],
    organization_id: str,
) -> tuple[OrganizationIntegrationRepository, ...]:
    repositories: tuple[OGitRepository, ...] = tuple(
        chain.from_iterable(
            await collect(
                tuple(
                    __get_azure_credentials_stats(
                        credential=credential,
                        loaders=loaders,
                        urls=urls,
                        nicknames=nicknames,
                    )
                    for credential in credentials
                    if isinstance(credential.secret, OauthAzureSecret)
                ),
            ),
        ),
    )

    repositories_branches = await collect(
        tuple(
            get_azure_branches_names(
                project_name=repository.repository.project.name,
                repository_id=repository.repository.id,
                credential=repository.credential,
                loaders=loaders,
                base_url=repository.account.base_url,
            )
            for repository in repositories
        ),
        workers=12,
    )

    return tuple(
        _create_org_integration_repo(repository, branches, organization_id)
        for repository, branches in zip(repositories, repositories_branches, strict=False)
    )


async def get_github_credentials_stats(
    *,
    credentials: list[Credentials],
    urls: set[str],
    nicknames: set[str],
    organization_id: str,
) -> tuple[OrganizationIntegrationRepository, ...]:
    stats: tuple[ProjectStats, ...] = tuple(
        chain.from_iterable(
            await collect(
                tuple(
                    _get_github_credential_stats(
                        credential=credential,
                        urls=urls,
                        nicknames=nicknames,
                    )
                    for credential in credentials
                ),
                workers=1,
            ),
        ),
    )
    filtered_stats: tuple[ProjectStats, ...] = tuple(
        {stat.project.id: stat for stat in stats}.values(),
    )

    return tuple(
        OrganizationIntegrationRepository(
            id=__get_id(stat.project.remote_url),
            organization_id=organization_id,
            branch=stat.project.branch,
            last_commit_date=stat.project.last_activity_at,
            url=stat.project.remote_url,
            credential_id=stat.credential.id,
            branches=stat.project.branches,
            name=stat.project.name,
        )
        for stat in filtered_stats
    )


async def _get_github_credential_stats(
    *,
    credential: Credentials,
    urls: set[str],
    nicknames: set[str],
    get_all: bool = False,
) -> tuple[ProjectStats, ...]:
    if isinstance(credential.secret, OauthGithubSecret):
        token: str = credential.secret.access_token

        repositories = await get_github_repos(token=token, credential_id=credential.id)
        filtered_repositories = tuple(
            repository
            for repository in repositories
            if does_not_exist_in_gitroot_urls(repository=repository, urls=urls, nicknames=nicknames)
        )
        repositories_branches: tuple[tuple[str, ...], ...] = tuple()
        if get_all:
            commits = await get_github_repos_commits(
                token=token,
                repositories=filtered_repositories,
                credential_id=credential.id,
            )
        else:
            repositories_branches = await collect(
                tuple(
                    in_thread(
                        get_github_branches_names,
                        repo=repo.repository,
                        cred_id=credential.id,
                        token=token,
                    )
                    for repo in filtered_repositories
                    if repo.repository
                ),
                workers=4,
            )

        return tuple(
            ProjectStats(
                project=project._replace(
                    branches=tuple(set(repositories_branches[index] + project.branches))
                    if repositories_branches
                    else project.branches,
                ),
                commits=commits[index] if get_all else tuple(),
                credential=credential,
            )
            for index, project in enumerate(filtered_repositories)
        )

    return tuple()


async def get_pat_credentials_stats(
    credentials: list[Credentials],
    urls: set[str],
    nicknames: set[str],
    loaders: Dataloaders,
    organization_id: str,
) -> tuple[OrganizationIntegrationRepository, ...]:
    pat_credentials = filter_pat_credentials(credentials)
    all_repositories = await loaders.organization_integration_repositories.load_many(
        pat_credentials,
    )

    repositories: tuple[CredentialsGitRepository, ...] = tuple(
        CredentialsGitRepository(
            credential=credential,
            repository=repository,
        )
        for credential, _repositories in zip(pat_credentials, all_repositories, strict=False)
        for repository in _repositories
        if does_not_exist_in_gitroot_urls(
            repository=repository,
            urls=urls,
            nicknames=nicknames,
        )
    )

    return tuple(
        OrganizationIntegrationRepository(
            id=_get_id(repository),
            organization_id=organization_id,
            branch=_get_branch(repository),
            last_commit_date=repository.repository.project.last_update_time,
            url=parse_url(repository.repository.web_url).url,
            credential_id=repository.credential.id,
        )
        for repository in repositories
    )
