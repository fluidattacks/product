import logging
from typing import Any

from snowflake.connector.cursor import SnowflakeCursor
from snowflake.connector.errors import DatabaseError
from snowflake.connector.vendored.requests.exceptions import RequestException

from .client import setup_snowflake_connection
from .utils import retry_on_exceptions

LOGGER = logging.getLogger()
LOGGER.setLevel(logging.INFO)


def execute_snowflake(
    cursor: SnowflakeCursor,
    sql_query: str,
    sql_vars: dict[str, Any] | None = None,
) -> None:
    try:
        setup_snowflake_connection(cursor)
        retry_on_exceptions(
            exceptions=(DatabaseError, RequestException),
            max_attempts=3,
            sleep_seconds=10,
        )(cursor.execute)(sql_query, sql_vars)
    except (DatabaseError, RequestException) as exc:
        LOGGER.exception(
            exc,
            extra={"query": sql_query, "vars": sql_vars},
        )
        raise
