from datetime import datetime

from integrates.archive.utils import mask_email
from integrates.class_types.types import Item


def format_row_metadata(
    item: Item,
) -> Item:
    root_id = item.get("root_id") or ""
    pk_2 = item.get("pk_2") or ""
    if not root_id:
        root_id = pk_2.split("#")[1] if pk_2.startswith("ROOT#") else ""

    return {
        "id": item.get("id") or str(item["pk"]).split("#")[1],
        "custom_severity": item.get("custom_severity"),
        "finding_id": item.get("finding_id") or str(item["sk"]).split("#")[1],
        "root_id": root_id,
        "severity_base_score": item.get("severity_score", {}).get("base_score"),
        "severity_cvss_v3": item.get("severity_score", {}).get("cvss_v3"),
        "severity_cvss_v4": item.get("severity_score", {}).get("cvss_v4"),
        "severity_cvssf": item.get("severity_score", {}).get("cvssf"),
        "severity_temporal_score": item.get("severity_score", {}).get("temporal_score"),
        "severity_threat_score": item.get("severity_score", {}).get("threat_score"),
        "skims_method": item.get("skims_method"),
        "technique": item.get("technique"),
        "type": item["type"],
        "where_str": item["state"]["where"].split(".")[-1].split(" ")[0]
        if item["type"] == "LINES" and len(item["state"]["where"].split(".")) > 1
        else None,
    }


def format_row_state(state: Item) -> Item:
    return {
        "id": str(state["pk"]).split("#")[1],
        "modified_by": mask_email(state["modified_by"]),
        "modified_date": datetime.fromisoformat(state["modified_date"]),
        "source": state["source"],
        "status": state["status"],
    }


def format_row_treatment(treatment: Item) -> Item:
    return {
        "id": str(treatment["pk"]).split("#")[1],
        "modified_date": datetime.fromisoformat(treatment["modified_date"]),
        "status": treatment["status"],
        "accepted_until": datetime.fromisoformat(treatment["accepted_until"])
        if treatment.get("accepted_until")
        else None,
        "acceptance_status": treatment.get("acceptance_status"),
    }


def format_row_verification(verification: Item) -> Item:
    return {
        "id": str(verification["pk"]).split("#")[1],
        "modified_date": datetime.fromisoformat(verification["modified_date"]),
        "status": verification["status"],
    }


def format_row_zero_risk(zero_risk: Item) -> Item:
    return {
        "id": str(zero_risk["pk"]).split("#")[1],
        "modified_date": datetime.fromisoformat(zero_risk["modified_date"]),
        "status": zero_risk["status"],
    }
