from snowflake.connector.cursor import SnowflakeCursor

from integrates.archive.client import snowflake_db_cursor
from integrates.archive.operations import execute_snowflake
from integrates.archive.utils import SCHEMA_NAME

METADATA_TABLE = "organizations_metadata"
STATE_TABLE = "organizations_state"


def _initialize_metadata_table(cursor: SnowflakeCursor) -> None:
    execute_snowflake(
        cursor,
        f"""
        CREATE TABLE IF NOT EXISTS {SCHEMA_NAME}.{METADATA_TABLE} (
            id STRING,
            country STRING,
            created_by STRING,
            created_date TIMESTAMP_TZ,
            name STRING,

            UNIQUE(id),
            PRIMARY KEY(id)
        )
        """,
    )


def _initialize_state_table(cursor: SnowflakeCursor) -> None:
    execute_snowflake(
        cursor,
        f"""
        CREATE TABLE IF NOT EXISTS {SCHEMA_NAME}.{STATE_TABLE} (
            id STRING,
            modified_by STRING,
            modified_date TIMESTAMP_TZ,
            pending_deletion_date TIMESTAMP_TZ,
            status STRING,

            PRIMARY KEY(id, modified_date),
            FOREIGN KEY(id) REFERENCES {SCHEMA_NAME}.{METADATA_TABLE}(id)
        )
        """,
    )


def initialize_tables() -> None:
    with snowflake_db_cursor() as cursor:
        _initialize_metadata_table(cursor)
        _initialize_state_table(cursor)
