from snowflake.connector.cursor import SnowflakeCursor

from integrates.archive.client import snowflake_db_cursor
from integrates.archive.utils import SCHEMA_NAME

METADATA_TABLE: str = "events_metadata"


def _initialize_metadata_table(cursor: SnowflakeCursor) -> None:
    cursor.execute(
        f"""
        CREATE TABLE IF NOT EXISTS {SCHEMA_NAME}.{METADATA_TABLE} (
            id STRING,
            created_by STRING,
            created_date TIMESTAMP_TZ,
            event_date TIMESTAMP_TZ,
            group_name STRING,
            hacker STRING,
            root_id STRING,
            solution_reason STRING,
            solving_date TIMESTAMP_TZ,
            status STRING,
            type STRING,
            UNIQUE(id),
            PRIMARY KEY(id)
        )
        """,
    )


def initialize_tables() -> None:
    with snowflake_db_cursor() as cursor:
        _initialize_metadata_table(cursor)
