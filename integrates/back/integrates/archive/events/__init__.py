from .insert import insert_bulk_metadata

__all__ = [
    "insert_bulk_metadata",
]
