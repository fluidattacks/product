from datetime import datetime

from integrates.class_types.types import Item

from ..utils import mask_by_encoding, mask_email


def format_row_metadata(
    item: Item,
) -> Item:
    return {
        "id": item["id"],
        "created_by": mask_email(item["created_by"]),
        "created_date": datetime.fromisoformat(item["created_date"])
        if item.get("created_date")
        else None,
        "event_date": datetime.fromisoformat(item["event_date"])
        if item.get("event_date")
        else None,
        "group_name": mask_by_encoding(item["group_name"]),
        "hacker": item["hacker"],
        "root_id": item.get("root_id"),
        "solution_reason": item["state"].get("reason"),
        "solving_date": datetime.fromisoformat(item["solving_date"])
        if item.get("solving_date")
        else None,
        "status": item["state"]["status"],
        "type": item["type"],
    }
