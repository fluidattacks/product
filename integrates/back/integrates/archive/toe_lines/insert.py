from snowflake.connector.cursor import SnowflakeCursor

from integrates.archive.operations import execute_snowflake
from integrates.archive.queries import SQL_MERGE_INTO_METADATA
from integrates.archive.utils import (
    format_merge_query,
    format_query_values_and_template,
    get_query_fields,
)
from integrates.class_types.types import Item

from .initialize import METADATA_TABLE
from .types import MetadataTableRow
from .utils import format_row_metadata


def insert_bulk_metadata(
    *,
    cursor: SnowflakeCursor,
    items: tuple[Item, ...],
) -> None:
    sql_fields = get_query_fields(MetadataTableRow)
    formatted_rows = [format_row_metadata(item) for item in items]
    sql_values, sql_values_template = format_query_values_and_template(formatted_rows)
    sql_query = format_merge_query(
        sql_template=SQL_MERGE_INTO_METADATA,
        table_name=METADATA_TABLE,
        sql_fields=sql_fields,
        sql_values_template=sql_values_template,
    )
    execute_snowflake(cursor, sql_query, sql_values)
