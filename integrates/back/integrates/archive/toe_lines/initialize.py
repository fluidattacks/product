from snowflake.connector.cursor import SnowflakeCursor

from integrates.archive.client import snowflake_db_cursor
from integrates.archive.operations import execute_snowflake
from integrates.archive.utils import SCHEMA_NAME

METADATA_TABLE: str = "toe_lines_metadata"


def _initialize_metadata_table(cursor: SnowflakeCursor) -> None:
    execute_snowflake(
        cursor,
        f"""
        CREATE TABLE IF NOT EXISTS {SCHEMA_NAME}.{METADATA_TABLE} (
            id STRING,
            attacked_at TIMESTAMP_TZ,
            attacked_by STRING,
            attacked_lines NUMBER,
            be_present BOOLEAN,
            be_present_until TIMESTAMP_TZ,
            first_attack_at TIMESTAMP_TZ,
            group_name STRING,
            has_vulnerabilities BOOLEAN,
            loc NUMBER,
            modified_date TIMESTAMP_TZ,
            root_id STRING,
            seen_at TIMESTAMP_TZ,
            seen_first_time_by STRING,
            sorts_risk_level NUMBER,

            UNIQUE (id),
            PRIMARY KEY (id)
        )
        """,
    )


def initialize_tables() -> None:
    with snowflake_db_cursor() as cursor:
        _initialize_metadata_table(cursor)
