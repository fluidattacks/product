from snowflake.connector.cursor import SnowflakeCursor

from integrates.archive.client import snowflake_db_cursor
from integrates.archive.operations import execute_snowflake
from integrates.archive.utils import SCHEMA_NAME

CODE_LANGUAGES_TABLE: str = "roots_code_languages"
METADATA_TABLE: str = "roots_metadata"


def _initialize_code_languages_table(
    cursor: SnowflakeCursor,
) -> None:
    execute_snowflake(
        cursor,
        f"""
        CREATE TABLE IF NOT EXISTS {SCHEMA_NAME}.{CODE_LANGUAGES_TABLE} (
            id STRING,
            language STRING,
            loc NUMBER,
            root_id STRING,

            UNIQUE (id),
            PRIMARY KEY (id)
        )
        """,
    )


def _initialize_metadata_table(cursor: SnowflakeCursor) -> None:
    execute_snowflake(
        cursor,
        f"""
        CREATE TABLE IF NOT EXISTS {SCHEMA_NAME}.{METADATA_TABLE} (
            id STRING,
            created_date TIMESTAMP_TZ,
            group_name STRING,
            organization_name STRING,
            type STRING,

            UNIQUE (id),
            PRIMARY KEY (id)
        )
        """,
    )


def initialize_tables() -> None:
    with snowflake_db_cursor() as cursor:
        _initialize_metadata_table(cursor)
        _initialize_code_languages_table(cursor)
