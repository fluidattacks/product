from itertools import chain

from snowflake.connector.cursor import SnowflakeCursor

from integrates.archive.operations import execute_snowflake
from integrates.archive.queries import SQL_MERGE_INTO_METADATA
from integrates.archive.utils import (
    format_merge_query,
    format_query_values_and_template,
    get_query_fields,
)
from integrates.class_types.types import Item

from .initialize import CODE_LANGUAGES_TABLE, METADATA_TABLE
from .types import CodeLanguagesTableRow, MetadataTableRow
from .utils import format_row_code_languages, format_row_metadata


def insert_bulk_metadata(
    *,
    cursor: SnowflakeCursor,
    items: tuple[Item, ...],
) -> None:
    sql_fields = get_query_fields(MetadataTableRow)
    formatted_rows = [format_row_metadata(item) for item in items]
    sql_values, sql_values_template = format_query_values_and_template(formatted_rows)
    sql_query = format_merge_query(
        sql_template=SQL_MERGE_INTO_METADATA,
        table_name=METADATA_TABLE,
        sql_fields=sql_fields,
        sql_values_template=sql_values_template,
    )
    execute_snowflake(cursor, sql_query, sql_values)

    sql_fields = get_query_fields(CodeLanguagesTableRow)
    formatted_rows = list(chain.from_iterable([format_row_code_languages(item) for item in items]))
    sql_values, sql_values_template = format_query_values_and_template(formatted_rows)
    sql_query = format_merge_query(
        sql_template=SQL_MERGE_INTO_METADATA,
        table_name=CODE_LANGUAGES_TABLE,
        sql_fields=sql_fields,
        sql_values_template=sql_values_template,
    )
    execute_snowflake(cursor, sql_query, sql_values)


def insert_code_languages(
    *,
    cursor: SnowflakeCursor,
    item: Item,
) -> None:
    sql_fields = get_query_fields(CodeLanguagesTableRow)
    formatted_rows = format_row_code_languages(item)
    if not formatted_rows:
        return

    sql_values, sql_values_template = format_query_values_and_template(formatted_rows)
    sql_query = format_merge_query(
        sql_template=SQL_MERGE_INTO_METADATA,
        table_name=CODE_LANGUAGES_TABLE,
        sql_fields=sql_fields,
        sql_values_template=sql_values_template,
    )
    execute_snowflake(cursor, sql_query, sql_values)


def insert_metadata(
    *,
    cursor: SnowflakeCursor,
    item: Item,
) -> None:
    sql_fields = get_query_fields(MetadataTableRow)
    formatted_rows = [format_row_metadata(item)]
    sql_values, sql_values_template = format_query_values_and_template(formatted_rows)
    sql_query = format_merge_query(
        sql_template=SQL_MERGE_INTO_METADATA,
        table_name=METADATA_TABLE,
        sql_fields=sql_fields,
        sql_values_template=sql_values_template,
    )
    execute_snowflake(cursor, sql_query, sql_values)


def insert_root(
    *,
    cursor: SnowflakeCursor,
    item: Item,
) -> None:
    insert_metadata(cursor=cursor, item=item)
    insert_code_languages(cursor=cursor, item=item)
