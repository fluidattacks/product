import atexit
import logging
import logging.config
from collections.abc import Iterator
from contextlib import contextmanager
from dataclasses import dataclass

import snowflake.connector
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import serialization
from snowflake.connector import SnowflakeConnection
from snowflake.connector.cursor import SnowflakeCursor

from integrates.archive.constants import SNOWFLAKE_DATABASE, SNOWFLAKE_ROLE, SNOWFLAKE_WAREHOUSE
from integrates.context import (
    FI_ENVIRONMENT,
    FI_SNOWFLAKE_ACCOUNT,
    FI_SNOWFLAKE_PRIVATE_KEY,
    FI_SNOWFLAKE_USER,
)
from integrates.settings import LOGGING

logging.config.dictConfig(LOGGING)
LOGGER = logging.getLogger(__name__)

_cache: dict[str, SnowflakeConnection] = {}


@dataclass(frozen=True)
class SnowflakeCredentials:
    user: str
    private_key: str
    account: str

    def __repr__(self) -> str:
        return "[MASKED]"

    def __str__(self) -> str:
        return "[MASKED]"


def get_snowflake_creds() -> SnowflakeCredentials:
    """Snowflake credentials from environment"""
    LOGGER.info("Using Snowflake credentials")
    private_key = FI_SNOWFLAKE_PRIVATE_KEY
    if not private_key:
        raise ValueError("Snowflake private key not found in environment variables")

    return SnowflakeCredentials(
        account=FI_SNOWFLAKE_ACCOUNT,
        private_key=private_key,
        user=FI_SNOWFLAKE_USER,
    )


def new_snowflake_connection() -> SnowflakeConnection:
    creds = get_snowflake_creds()
    private_key = serialization.load_pem_private_key(
        creds.private_key.encode("utf-8"),
        password=None,
        backend=default_backend(),
    )
    pkb = private_key.private_bytes(
        encoding=serialization.Encoding.DER,
        format=serialization.PrivateFormat.PKCS8,
        encryption_algorithm=serialization.NoEncryption(),
    )

    return snowflake.connector.connect(
        user=creds.user,
        private_key=pkb,
        account=creds.account,
        database=SNOWFLAKE_DATABASE,
        warehouse=SNOWFLAKE_WAREHOUSE,
    )


def snowflake_connection_singleton() -> SnowflakeConnection | None:
    if FI_ENVIRONMENT != "production":
        return None

    if _cache.get("connection"):
        return _cache["connection"]

    try:
        connection = new_snowflake_connection()
        setup_snowflake_connection(connection.cursor())
        _cache["connection"] = connection

        return connection

    except snowflake.connector.errors.DatabaseError as exc:
        LOGGER.error(exc)

        return None


def setup_snowflake_connection(cursor: SnowflakeCursor) -> None:
    cursor.execute(f"USE WAREHOUSE {SNOWFLAKE_WAREHOUSE}")
    cursor.execute(f"USE ROLE {SNOWFLAKE_ROLE}")
    cursor.execute(f"USE DATABASE {SNOWFLAKE_DATABASE}")


@contextmanager
def snowflake_db_cursor() -> Iterator[SnowflakeCursor]:
    if FI_ENVIRONMENT != "production":
        return

    try:
        LOGGER.info("Establishing connection to Snowflake")
        connection = snowflake_connection_singleton()
        if connection is not None:
            cursor = connection.cursor()
            try:
                yield cursor
            finally:
                cursor.close()

    except Exception as exc:
        LOGGER.error("Error connecting to Snowflake: %s", exc)
        if _cache.get("connection"):
            _cache["connection"].close()
            del _cache["connection"]

        raise exc


def exit_handler() -> None:
    # Handler for closing the connection when
    # the python interpreter terminates normally
    if _cache.get("connection"):
        LOGGER.info("Closing Snowflake connection")
        _cache["connection"].close()


atexit.register(exit_handler)
