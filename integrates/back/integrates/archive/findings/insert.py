from snowflake.connector.cursor import SnowflakeCursor

from integrates.archive.operations import execute_snowflake
from integrates.archive.queries import (
    SQL_MERGE_INTO_HISTORIC,
    SQL_MERGE_INTO_METADATA,
    SQL_MERGE_INTO_VERIFICATION_VULNS_IDS,
)
from integrates.archive.utils import (
    format_merge_query,
    format_query_values_and_template,
    get_query_fields,
)
from integrates.class_types.types import Item

from .initialize import METADATA_TABLE, STATE_TABLE, VERIFICATION_TABLE, VERIFICATION_VULN_IDS_TABLE
from .types import (
    MetadataTableRow,
    StateTableRow,
    VerificationTableRow,
    VerificationVulnIdsTableRow,
)
from .utils import (
    format_row_metadata,
    format_row_state,
    format_row_verification,
    format_row_verification_vuln_ids,
)


def insert_bulk_metadata(
    *,
    cursor: SnowflakeCursor,
    items: tuple[Item, ...],
) -> None:
    sql_fields = get_query_fields(MetadataTableRow)
    formatted_rows = [format_row_metadata(item) for item in items]
    sql_values, sql_values_template = format_query_values_and_template(formatted_rows)
    sql_query = format_merge_query(
        sql_template=SQL_MERGE_INTO_METADATA,
        table_name=METADATA_TABLE,
        sql_fields=sql_fields,
        sql_values_template=sql_values_template,
    )
    execute_snowflake(cursor, sql_query, sql_values)


def insert_historic_state(
    *,
    cursor: SnowflakeCursor,
    historic_state: tuple[Item, ...],
) -> None:
    sql_fields = get_query_fields(StateTableRow)
    formatted_rows = [format_row_state(item) for item in historic_state]
    sql_values, sql_values_template = format_query_values_and_template(formatted_rows)
    sql_query = format_merge_query(
        sql_template=SQL_MERGE_INTO_HISTORIC,
        table_name=STATE_TABLE,
        sql_fields=sql_fields,
        sql_values_template=sql_values_template,
    )
    execute_snowflake(cursor, sql_query, sql_values)


def insert_historic_verification(
    *,
    cursor: SnowflakeCursor,
    historic_verification: tuple[Item, ...],
) -> None:
    sql_fields = get_query_fields(VerificationTableRow)
    formatted_rows = [format_row_verification(item) for item in historic_verification]
    sql_values, sql_values_template = format_query_values_and_template(formatted_rows)
    sql_query = format_merge_query(
        sql_template=SQL_MERGE_INTO_HISTORIC,
        table_name=VERIFICATION_TABLE,
        sql_fields=sql_fields,
        sql_values_template=sql_values_template,
    )
    execute_snowflake(cursor, sql_query, sql_values)


def insert_historic_verification_vuln_ids(
    *,
    cursor: SnowflakeCursor,
    historic_verification: tuple[Item, ...],
) -> None:
    sql_fields = get_query_fields(VerificationVulnIdsTableRow)
    formatted_rows = [
        format_row_verification_vuln_ids(
            finding_id=str(item["pk"]).split("#")[1],
            modified_date=item["modified_date"],
            vulnerability_id=vulnerability_id,
        )
        for item in historic_verification
        if item.get("vulnerability_ids")
        for vulnerability_id in item["vulnerability_ids"]
    ]
    if not formatted_rows:
        return

    sql_values, sql_values_template = format_query_values_and_template(formatted_rows)
    sql_query = format_merge_query(
        sql_template=SQL_MERGE_INTO_VERIFICATION_VULNS_IDS,
        table_name=VERIFICATION_VULN_IDS_TABLE,
        sql_fields=sql_fields,
        sql_values_template=sql_values_template,
    )
    execute_snowflake(cursor, sql_query, sql_values)


def insert_finding(
    *,
    cursor: SnowflakeCursor,
    item: Item,
) -> None:
    insert_bulk_metadata(cursor=cursor, items=(item,))
    state_items = (
        item.get("state"),
        item.get("creation"),
        item.get("submission"),
        item.get("approval"),
    )
    state_items_filtered = tuple(
        {
            "pk": item["pk"],
            "sk": f'STATE#{state_item["modified_date"]}',
            **state_item,
        }
        for state_item in state_items
        if state_item
    )
    if state_items_filtered:
        insert_historic_state(
            cursor=cursor,
            historic_state=state_items_filtered,
        )
    verification = item.get("verification")
    if verification:
        historic_verification = (
            {
                "pk": item["pk"],
                "sk": f'VERIFICATION#{verification["modified_date"]}',
                **verification,
            },
        )
        insert_historic_verification(
            cursor=cursor,
            historic_verification=historic_verification,
        )
        insert_historic_verification_vuln_ids(
            cursor=cursor,
            historic_verification=historic_verification,
        )
