from dataclasses import dataclass
from datetime import datetime
from decimal import Decimal


@dataclass(frozen=True)
class MetadataTableRow:
    id: str
    cvss_version: str | None
    group_name: str
    hacker_email: str
    requirements: str
    severity_base_score: Decimal
    severity_cvss_v3: str
    severity_cvss_v4: str | None
    severity_cvssf: Decimal
    severity_temporal_score: Decimal
    severity_threat_score: Decimal | None
    sorts: str
    title: str


@dataclass(frozen=True)
class StateTableRow:
    id: str
    modified_by: str
    modified_date: datetime
    justification: str
    source: str
    status: str


@dataclass(frozen=True)
class VerificationTableRow:
    id: str
    modified_date: datetime
    status: str


@dataclass(frozen=True)
class VerificationVulnIdsTableRow:
    id: str
    modified_date: datetime
    vulnerability_id: str
