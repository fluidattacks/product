from datetime import datetime

from integrates.archive.utils import mask_by_encoding, mask_email
from integrates.class_types.types import Item


def format_row_metadata(
    item: Item,
) -> Item:
    return {
        "id": item.get("id") or str(item["pk"]).split("#")[1],
        "cvss_version": item["cvss_version"],
        "group_name": mask_by_encoding(item["group_name"]),
        "hacker_email": item.get("creation", {}).get("modified_by", ""),
        "requirements": item["requirements"],
        "severity_base_score": item["severity_score"]["base_score"],
        "severity_cvss_v3": item["severity_score"]["cvss_v3"],
        "severity_cvss_v4": item["severity_score"].get("cvss_v4"),
        "severity_cvssf": item["severity_score"]["cvssf"],
        "severity_temporal_score": item["severity_score"]["temporal_score"],
        "severity_threat_score": item["severity_score"].get("threat_score"),
        "sorts": item["sorts"],
        "title": item["title"],
    }


def format_row_state(state: Item) -> Item:
    return {
        "id": str(state["pk"]).split("#")[1],
        "modified_by": mask_email(state["modified_by"]),
        "modified_date": datetime.fromisoformat(state["modified_date"]),
        "justification": state["justification"],
        "source": state["source"],
        "status": state["status"],
    }


def format_row_verification(verification: Item) -> Item:
    return {
        "id": str(verification["pk"]).split("#")[1],
        "modified_date": datetime.fromisoformat(verification["modified_date"]),
        "status": verification["status"],
    }


def format_row_verification_vuln_ids(
    finding_id: str,
    modified_date: str,
    vulnerability_id: str,
) -> Item:
    return {
        "id": finding_id,
        "modified_date": datetime.fromisoformat(modified_date),
        "vulnerability_id": vulnerability_id,
    }
