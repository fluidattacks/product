from decimal import Decimal

from integrates.custom_exceptions import InvalidSeverity
from integrates.dataloaders import Dataloaders, get_new_context
from integrates.db_model.organizations.types import OrganizationPriorityPolicy
from integrates.organizations_priority_policies.domain import (
    remove_priority_policy,
    update_priority_policies,
)
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    OrganizationAccessFaker,
    OrganizationAccessStateFaker,
    OrganizationFaker,
    StakeholderFaker,
)
from integrates.testing.mocks import mocks
from integrates.testing.utils import parametrize, raises

ORG_ID = "ORG#6b110f25-cd46-e94c-2bad-b9e182cf91a8"


@parametrize(
    args=["updated_policies"],
    cases=[
        [
            [
                OrganizationPriorityPolicy("PTAAS", 100),
                OrganizationPriorityPolicy("DAST", 70),
                OrganizationPriorityPolicy("SAST", -500),
            ],
        ],
        [
            [
                OrganizationPriorityPolicy("AV:N", 200),
                OrganizationPriorityPolicy("E:H", 1000),
                OrganizationPriorityPolicy("RE", -5),
            ],
        ],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id=ORG_ID)],
            organization_access=[
                OrganizationAccessFaker(
                    organization_id=ORG_ID,
                    email="jdoe@fluidattacks.com",
                    state=OrganizationAccessStateFaker(
                        has_access=True, role="organization_manager"
                    ),
                ),
            ],
            stakeholders=[StakeholderFaker(email="jdoe@fluidattacks.com")],
        ),
    )
)
async def test_update_priority_policies(
    updated_policies: list[OrganizationPriorityPolicy],
) -> None:
    loaders: Dataloaders = get_new_context()

    # Act
    await update_priority_policies(
        loaders=loaders,
        organization_id=ORG_ID,
        updated_policies=updated_policies,
    )
    loaders.organization.clear(ORG_ID)
    organization = await loaders.organization.load(ORG_ID)

    # Assert
    assert organization
    assert organization.id == ORG_ID
    assert organization.priority_policies == updated_policies


@parametrize(
    args=["updated_policies"],
    cases=[[[OrganizationPriorityPolicy("PTAAS", 100000)]]],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id=ORG_ID)],
            organization_access=[
                OrganizationAccessFaker(
                    organization_id=ORG_ID,
                    email="jdoe@fluidattacks.com",
                    state=OrganizationAccessStateFaker(
                        has_access=True, role="organization_manager"
                    ),
                ),
            ],
            stakeholders=[StakeholderFaker(email="jdoe@fluidattacks.com")],
        ),
    )
)
async def test_update_priority_policies_fail(
    updated_policies: list[OrganizationPriorityPolicy],
) -> None:
    loaders: Dataloaders = get_new_context()

    # Act
    with raises(InvalidSeverity([Decimal(-1000), Decimal(1000)]).__class__):
        await update_priority_policies(
            loaders=loaders,
            organization_id=ORG_ID,
            updated_policies=updated_policies,
        )


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(
                    id=ORG_ID,
                    priority_policies=[
                        OrganizationPriorityPolicy("PTAAS", 1000),
                        OrganizationPriorityPolicy("AV:L", 50),
                    ],
                ),
            ],
            organization_access=[
                OrganizationAccessFaker(
                    organization_id=ORG_ID,
                    email="jdoe@fluidattacks.com",
                    state=OrganizationAccessStateFaker(
                        has_access=True, role="organization_manager"
                    ),
                ),
            ],
            stakeholders=[StakeholderFaker(email="jdoe@fluidattacks.com")],
        ),
    )
)
async def test_remove_priority_policy() -> None:
    organization_id = "ORG#6b110f25-cd46-e94c-2bad-b9e182cf91a8"
    loaders: Dataloaders = get_new_context()

    # Act
    await remove_priority_policy(
        loaders=loaders,
        organization_id=organization_id,
        policy_to_remove="PTAAS",
    )
    loaders.organization.clear(ORG_ID)
    organization = await loaders.organization.load(ORG_ID)

    # Assert
    assert organization
    assert organization.id == ORG_ID
    assert organization.priority_policies == [OrganizationPriorityPolicy("AV:L", 50)]
