from datetime import (
    UTC,
    datetime,
)
from typing import Optional

import pytest

from integrates.custom_exceptions import SameValues
from integrates.db_model.enums import (
    AcceptanceStatus,
    TreatmentStatus,
)
from integrates.db_model.types import (
    Treatment,
    TreatmentToUpdate,
)
from integrates.treatment.validations import validate_same_value


@pytest.mark.parametrize(
    ("current", "to_update", "valid_assigned"),
    [
        (
            Treatment(
                status=TreatmentStatus.ACCEPTED,
                justification="test justification",
                assigned="test@test.com",
                accepted_until=datetime.fromisoformat("2024-01-01"),
                acceptance_status=AcceptanceStatus.SUBMITTED,
                modified_by="test@test.com",
                modified_date=datetime.now(UTC),
            ),
            TreatmentToUpdate(
                status=TreatmentStatus.ACCEPTED,
                justification="test justification",
                assigned="test@test.com",
                accepted_until=datetime.fromisoformat("2024-01-01"),
            ),
            "test@test.com",
        ),
        (
            Treatment(
                status=TreatmentStatus.ACCEPTED_UNDEFINED,
                justification="test justification",
                assigned="test@test.com",
                accepted_until=None,
                acceptance_status=AcceptanceStatus.APPROVED,
                modified_by="test@test.com",
                modified_date=datetime.now(UTC),
            ),
            TreatmentToUpdate(
                status=TreatmentStatus.ACCEPTED_UNDEFINED,
                justification="different justification",
                assigned="test@test.com",
                accepted_until=None,
            ),
            "test@test.com",
        ),
        (
            Treatment(
                status=TreatmentStatus.ACCEPTED,
                justification="test justification",
                assigned="test@test.com",
                accepted_until=datetime.fromisoformat("2024-01-01"),
                acceptance_status=AcceptanceStatus.APPROVED,
                modified_by="test@test.com",
                modified_date=datetime.now(UTC),
            ),
            TreatmentToUpdate(
                status=TreatmentStatus.ACCEPTED,
                justification="test justification",
                assigned="test@test.com",
                accepted_until=datetime.fromisoformat("2024-01-01"),
            ),
            "test@test.com",
        ),
    ],
)
def test_validate_same_value_raises(
    current: Treatment,
    to_update: TreatmentToUpdate,
    valid_assigned: str,
) -> None:
    with pytest.raises(SameValues):
        validate_same_value(
            current_treatment=current,
            treatment_to_update=to_update,
            valid_assigned=valid_assigned,
        )


@pytest.mark.parametrize(
    ("current", "to_update", "valid_assigned"),
    [
        (
            Treatment(
                status=TreatmentStatus.ACCEPTED,
                justification="test justification",
                assigned="test@test.com",
                accepted_until=datetime.fromisoformat("2024-01-01"),
                acceptance_status=AcceptanceStatus.SUBMITTED,
                modified_by="test@test.com",
                modified_date=datetime.now(UTC),
            ),
            TreatmentToUpdate(
                status=TreatmentStatus.IN_PROGRESS,
                justification="different justification",
                assigned="test@test.com",
                accepted_until=None,
            ),
            "test@test.com",
        ),
        (
            Treatment(
                status=TreatmentStatus.ACCEPTED_UNDEFINED,
                justification="test justification",
                assigned="test@test.com",
                accepted_until=None,
                acceptance_status=AcceptanceStatus.SUBMITTED,
                modified_by="test@test.com",
                modified_date=datetime.now(UTC),
            ),
            TreatmentToUpdate(
                status=TreatmentStatus.ACCEPTED_UNDEFINED,
                justification="test justification",
                assigned="different@test.com",
                accepted_until=None,
            ),
            "different@test.com",
        ),
        (
            None,
            TreatmentToUpdate(
                status=TreatmentStatus.ACCEPTED,
                justification="test justification",
                assigned="test@test.com",
                accepted_until=datetime.fromisoformat("2024-01-01"),
            ),
            "test@test.com",
        ),
    ],
)
def test_validate_same_value_success(
    current: Optional[Treatment],
    to_update: TreatmentToUpdate,
    valid_assigned: str,
) -> None:
    validate_same_value(
        current_treatment=current,
        treatment_to_update=to_update,
        valid_assigned=valid_assigned,
    )
