from datetime import UTC, datetime, tzinfo

from integrates.custom_exceptions import InvalidAssigned
from integrates.dataloaders import get_new_context
from integrates.db_model.enums import TreatmentStatus
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import GroupAccessFaker, GroupAccessStateFaker, StakeholderFaker
from integrates.testing.mocks import mocks
from integrates.testing.utils import freeze_time, parametrize, raises
from integrates.treatment.utils import (
    get_accepted_until,
    get_inverted_treatment_converted,
    get_valid_assigned,
)

USER_EMAIL = "user@test.com"
GROUP_NAME = "test-group"


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            stakeholders=[StakeholderFaker(email=USER_EMAIL)],
            group_access=[
                GroupAccessFaker(
                    email=USER_EMAIL,
                    group_name=GROUP_NAME,
                    state=GroupAccessStateFaker(role="group_manager"),
                )
            ],
        ),
    )
)
async def test_get_valid_assigned_as_admin() -> None:
    loaders = get_new_context()
    result = await get_valid_assigned(
        loaders=loaders,
        assigned=USER_EMAIL,
        email=USER_EMAIL,
        group_name=GROUP_NAME,
    )

    assert result == USER_EMAIL


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            stakeholders=[StakeholderFaker(email=USER_EMAIL, is_registered=False)],
            group_access=[
                GroupAccessFaker(
                    email=USER_EMAIL,
                    group_name=GROUP_NAME,
                    state=GroupAccessStateFaker(role="user"),
                )
            ],
        ),
    )
)
async def test_get_valid_assigned_unregistered_user() -> None:
    loaders = get_new_context()
    with raises(InvalidAssigned):
        await get_valid_assigned(
            loaders=loaders,
            assigned=USER_EMAIL,
            email=USER_EMAIL,
            group_name=GROUP_NAME,
        )


def test_get_accepted_until_not_accepted() -> None:
    result = get_accepted_until(
        acceptance_date="2023-08-10", treatment_status=TreatmentStatus.IN_PROGRESS
    )
    assert result is None


def test_get_accepted_until_no_date() -> None:
    result = get_accepted_until(acceptance_date=None, treatment_status=TreatmentStatus.ACCEPTED)
    assert result is None


def test_get_accepted_until_with_time() -> None:
    result = get_accepted_until(
        acceptance_date="2023-08-10 14:30:00", treatment_status=TreatmentStatus.ACCEPTED
    )
    assert isinstance(result, datetime)
    assert result.year == 2023
    assert result.month == 8
    assert result.day == 10
    assert result.hour == 19
    assert result.minute == 30
    assert result.second == 0
    assert isinstance(result.tzinfo, tzinfo)
    assert result.tzinfo == UTC


@freeze_time("2024-12-20T10:00:00Z")
def test_get_accepted_until_date_only() -> None:
    result = get_accepted_until(
        acceptance_date="2023-08-10", treatment_status=TreatmentStatus.ACCEPTED
    )
    assert result is not None
    assert result.year == 2023
    assert result.month == 8
    assert result.day == 10
    assert result.tzinfo == UTC


def test_get_inverted_treatment_converted_new() -> None:
    result = get_inverted_treatment_converted("NEW")
    assert result == "UNTREATED"


@parametrize(args=["treatment"], cases=[["ACCEPTED"], ["IN_PROGRESS"], ["ACCEPTED_UNDEFINED"]])
def test_get_inverted_treatment_converted_other_values(treatment: str) -> None:
    result = get_inverted_treatment_converted(treatment)
    assert result == treatment


def test_get_inverted_treatment_converted_empty() -> None:
    result = get_inverted_treatment_converted("")
    assert result == ""


def test_get_inverted_treatment_converted_case_sensitive() -> None:
    result = get_inverted_treatment_converted("new")
    assert result == "new"
