from authlib.integrations.starlette_client import (
    OAuth,
    StarletteOAuth2App,
)

from .azure import (
    AZURE_ARGS,
)
from .bitbucket import (
    BITBUCKET_ARGS,
)
from .google import (
    GOOGLE_ARGS,
)


class OAuthTyped(OAuth):
    azure: StarletteOAuth2App
    bitbucket: StarletteOAuth2App
    google: StarletteOAuth2App


OAUTH = OAuthTyped()
OAUTH.register(**AZURE_ARGS)
OAUTH.register(**GOOGLE_ARGS)
OAUTH.register(**BITBUCKET_ARGS)

__all__ = ["AZURE_ARGS", "BITBUCKET_ARGS", "GOOGLE_ARGS", "OAUTH"]
