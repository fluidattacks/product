import hashlib
import logging
import logging.config
import re
from collections.abc import Callable, Iterable
from copy import deepcopy
from datetime import datetime
from fnmatch import fnmatch
from time import time
from typing import Any
from uuid import uuid4

from aioextensions import collect, schedule
from botocore.exceptions import EndpointConnectionError
from graphql import GraphQLResolveInfo
from starlette.datastructures import UploadFile
from urllib3.exceptions import LocationParseError
from urllib3.util.url import parse_url

from integrates.batch import dal as batch_dal
from integrates.batch.enums import Action, IntegratesBatchQueue
from integrates.batch.types import BatchProcessingToUpdate, DependentAction, PutActionResult
from integrates.class_types.types import Item
from integrates.custom_exceptions import (
    CustomBaseException,
    FileNotFound,
    InvalidAWSRoleTrustPolicy,
    InvalidCSVFormat,
    InvalidParameter,
    InvalidRootType,
    RepeatedRoot,
    RequiredCredentials,
    ResourceTypeNotFound,
    RootDockerfileNotExists,
    RootEnvironmentUrlNotFound,
    RootNotFound,
    ToeInputAlreadyUpdated,
    ToePackageAlreadyUpdated,
    UnavailabilityError,
)
from integrates.custom_utils import datetime as datetime_utils
from integrates.custom_utils import logs as logs_utils
from integrates.custom_utils import roots as roots_utils
from integrates.custom_utils import validations as custom_validations
from integrates.custom_utils import validations_deco as validation_deco_utils
from integrates.custom_utils.groups import get_group
from integrates.custom_utils.roots import (
    fetch_temp_aws_creds,
    filter_active_git_roots,
    get_active_git_roots,
    get_root,
    get_root_vulnerabilities,
    get_unsolved_events_by_root,
)
from integrates.dataloaders import Dataloaders
from integrates.db_model import credentials as creds_model
from integrates.db_model import roots as roots_model
from integrates.db_model import toe_packages
from integrates.db_model.credentials.types import Credentials, CredentialsRequest
from integrates.db_model.events.enums import EventSolutionReason, EventType
from integrates.db_model.finding_comments.enums import CommentType
from integrates.db_model.finding_comments.types import FindingComment
from integrates.db_model.findings.types import Finding
from integrates.db_model.groups.enums import GroupSubscriptionType
from integrates.db_model.groups.types import Group
from integrates.db_model.roots.enums import (
    RootCloningStatus,
    RootCriticality,
    RootDockerImageStateStatus,
    RootEnvironmentCloud,
    RootEnvironmentUrlStateStatus,
    RootStateReason,
    RootStatus,
    RootType,
)
from integrates.db_model.roots.types import (
    DockerImageHistory,
    GitRoot,
    GitRootCloning,
    GitRootState,
    IPRoot,
    IPRootState,
    Root,
    RootDockerImage,
    RootDockerImageLayer,
    RootDockerImagesRequest,
    RootDockerImageState,
    RootEnvironmentSecretsRequest,
    RootEnvironmentUrl,
    RootEnvironmentUrlRequest,
    RootEnvironmentUrlsRequest,
    RootRequest,
    RootUnreliableIndicators,
    Secret,
    URLRoot,
    URLRootState,
)
from integrates.db_model.roots.update import update_root_docker_image_state
from integrates.db_model.toe_lines.types import RootToeLinesRequest
from integrates.db_model.toe_packages.types import (
    RootDockerImagePackagesRequest,
    RootToePackagesRequest,
    ToePackage,
    ToePackageCoordinates,
    ToePackageRequest,
    ToePackagesConnection,
)
from integrates.db_model.vulnerabilities.enums import VulnerabilityStateStatus
from integrates.db_model.vulnerabilities.types import Vulnerability
from integrates.decorators import retry_on_exceptions
from integrates.dynamodb.exceptions import DynamoDbBaseException
from integrates.events.domain import solve_event
from integrates.finding_comments import domain as comments_domain
from integrates.mailer.utils import get_organization_name
from integrates.organizations import domain as orgs_domain
from integrates.organizations import utils as orgs_utils
from integrates.organizations import validations as orgs_validations
from integrates.roots import filter as roots_filter
from integrates.roots import utils, validations
from integrates.roots.docker_images import get_image_manifest
from integrates.roots.notifications import report_activity
from integrates.roots.types import (
    GitRootAttributesToAdd,
    IpRootAttributesToAdd,
    UrlRootAttributesToAdd,
)
from integrates.settings.logger import LOGGING
from integrates.tickets import domain as tickets_domain
from integrates.toe.inputs import domain as toe_inputs_domain
from integrates.toe.inputs.types import ToeInputAttributesToUpdate
from integrates.verify.enums import ResourceType

logging.config.dictConfig(LOGGING)

LOGGER = logging.getLogger(__name__)

APP_EXCEPTIONS = (CustomBaseException, DynamoDbBaseException)


async def notify_health_check(*, group_name: str, root: GitRoot, user_email: str) -> None:
    await tickets_domain.request_health_check(
        branch=root.state.branch,
        group_name=group_name,
        repo_url=root.state.url,
        requester_email=user_email,
    )


def format_environment_id(url: str) -> str:
    # NOFLUID SHA1 is used to generate a unique identifier
    return hashlib.sha1(url.encode()).hexdigest()  # noqa: S324


async def _get_credentials_to_add_and_redirected_url(
    *,
    loaders: Dataloaders,
    branch: str,
    credentials: dict[str, str] | None,
    organization_id: str,
    required_credentials: bool,
    url: str,
    use_egress: bool,
    use_vpn: bool,
    use_ztna: bool,
    user_email: str,
    group_name: str,
) -> tuple[str, Credentials | None]:
    """
    Get the organization credentials to add git root.

    Raises:
        RequiredCredentials: _description_

    """
    redirected_url = url
    if required_credentials and not credentials:
        raise RequiredCredentials()

    if not credentials:
        return url, None

    if credential_id := credentials.get("id"):
        await validations.validate_credential_in_organization(
            loaders,
            credential_id,
            organization_id,
        )
        organization_credential = await orgs_domain.get_credentials(
            loaders,
            credential_id,
            organization_id,
        )
        if not use_vpn and not use_ztna and not use_egress and required_credentials:
            redirected_url = await validations.validate_and_get_redirected_url(
                repo_url=url,
                secret=organization_credential.secret,
                group_name=group_name,
                loaders=loaders,
                credential_id=organization_credential.id,
                organization_id=organization_id,
            )
            await validations.working_credentials(
                redirected_url,
                branch,
                organization_credential,
                loaders,
                follow_redirects=False,
            )

        return redirected_url, organization_credential

    organization_credential = utils.format_root_credential_new(
        credentials,
        organization_id,
        user_email,
    )
    await orgs_validations.validate_credentials_name_in_organization(
        loaders,
        organization_credential.organization_id,
        organization_credential.state.name,
    )
    if not use_vpn and not use_ztna and not use_egress and required_credentials:
        redirected_url = await validations.validate_and_get_redirected_url(
            repo_url=url,
            secret=organization_credential.secret,
            group_name=group_name,
            loaders=loaders,
            credential_id=organization_credential.id,
            organization_id=organization_id,
        )
        await validations.working_credentials(
            redirected_url,
            branch,
            organization_credential,
            loaders,
            follow_redirects=False,
        )

    await creds_model.add(credential=organization_credential)

    return redirected_url, organization_credential


async def get_roots_from_file(
    file: UploadFile,
    group_name: str,
    loaders: Dataloaders,
    organization_id: str,
) -> tuple[list[Item], list[InvalidCSVFormat]]:
    credential_ids = [
        credential.id for credential in await loaders.organization_credentials.load(organization_id)
    ]
    uploaded_roots_by_user, errors = await utils.read_file_content(
        file=file,
        credential_ids=credential_ids,
    )
    unique_roots = await utils.get_unique_roots(
        group_name,
        loaders,
        uploaded_roots_by_user,
        organization_id,
    )
    return (
        [
            {
                "line_index": root.line_index,
                "line": root.original_row,
                "branch": root.branch,
                "credentials": {"id": root.credential_id},
                "gitignore": root.git_ignore,
                "group_name": group_name,
                "includes_health_check": root.includes_health_check,
                "url": root.url,
                "use_egress": root.connection == "egress",
                "use_ztna": root.connection == "ztna",
                "priority": root.priority.upper(),
                "nickname": root.nickname,
            }
            for root in unique_roots
        ],
        errors,
    )


async def process_git_root(
    *,
    info: GraphQLResolveInfo,
    attributes_to_add: GitRootAttributesToAdd,
    group: Group,
    user_email: str,
) -> str:
    loaders: Dataloaders = info.context.loaders
    root = await add_git_root(
        loaders=loaders,
        attributes_to_add=attributes_to_add,
        group=group,
        user_email=user_email,
        required_credentials=True,
    )
    if root.state.credential_id:
        await clone_root_and_update_group_languages(
            loaders=loaders,
            group=group,
            modified_by=user_email,
            root=root,
        )

    report_activity(info, loaders, root, root.group_name, user_email)

    return root.id


async def _validate_add_git_root(
    *,
    loaders: Dataloaders,
    branch: str,
    ensure_org_uniqueness: bool = True,
    gitignore: list[str],
    group: Group,
    includes_health_check: bool,
    nickname: str,
    url: str,
    user_email: str,
) -> None:
    if nickname:
        validations.validate_nickname(nickname)
        custom_validations.validate_sanitized_csv_input(nickname)

    custom_validations.validate_fields(url)
    validations.validate_url_and_branch(url, branch)
    validations.validate_health_check(includes_health_check, group)
    validations.validate_exclusions(gitignore)
    await validations.validate_git_ignore(
        loaders=loaders,
        gitignore=gitignore,
        group_name=group.name,
        url=url,
        user_email=user_email,
    )
    await validations.validate_unique_root(
        loaders=loaders,
        branch=branch,
        ensure_org_uniqueness=ensure_org_uniqueness,
        group_name=group.name,
        url=url,
    )


async def _format_root_nickname_to_add(
    *,
    loaders: Dataloaders,
    group_name: str,
    raw_nickname: str,
    url: str,
) -> str:
    formatted_nickname = await _clear_and_assign_nickname(
        loaders,
        group_name,
        utils.format_root_nickname(raw_nickname, url),
    )
    group_roots = await loaders.group_roots.load(group_name)

    return utils.ensure_unique_nickname(formatted_nickname, group_roots, url)


async def add_git_root(
    *,
    loaders: Dataloaders,
    attributes_to_add: GitRootAttributesToAdd,
    group: Group,
    user_email: str,
    ensure_org_uniqueness: bool = True,
    required_credentials: bool = False,
    should_notify_health_check: bool = True,
) -> GitRoot:
    url = utils.format_git_repo_url(attributes_to_add.url.strip())
    branch = attributes_to_add.branch.strip()
    await _validate_add_git_root(
        loaders=loaders,
        branch=branch,
        ensure_org_uniqueness=ensure_org_uniqueness,
        gitignore=attributes_to_add.gitignore,
        group=group,
        includes_health_check=attributes_to_add.includes_health_check,
        nickname=attributes_to_add.nickname,
        url=url,
        user_email=user_email,
    )
    (
        redirected_url,
        credential,
    ) = await _get_credentials_to_add_and_redirected_url(
        loaders=loaders,
        branch=branch,
        credentials=attributes_to_add.credentials,
        organization_id=group.organization_id,
        required_credentials=required_credentials,
        url=url,
        use_egress=attributes_to_add.use_egress,
        use_vpn=attributes_to_add.use_vpn,
        use_ztna=attributes_to_add.use_ztna,
        user_email=user_email,
        group_name=group.name,
    )
    if redirected_url != url:
        url = redirected_url
        await _validate_add_git_root(
            loaders=loaders,
            branch=branch,
            ensure_org_uniqueness=ensure_org_uniqueness,
            gitignore=attributes_to_add.gitignore,
            group=group,
            includes_health_check=attributes_to_add.includes_health_check,
            nickname=attributes_to_add.nickname,
            url=url,
            user_email=user_email,
        )
    nickname = await _format_root_nickname_to_add(
        loaders=loaders,
        group_name=group.name,
        raw_nickname=attributes_to_add.nickname,
        url=url,
    )
    organization_name = await get_organization_name(loaders, group.name)
    modified_date = datetime_utils.get_utc_now()
    root = GitRoot(
        cloning=GitRootCloning(
            modified_by=user_email,
            modified_date=modified_date,
            reason="ROOT_CREATED",
            status=RootCloningStatus.UNKNOWN,
        ),
        created_by=user_email,
        created_date=modified_date,
        group_name=group.name,
        id=str(uuid4()),
        organization_name=organization_name,
        state=GitRootState(
            branch=branch,
            credential_id=credential.id if credential else None,
            criticality=attributes_to_add.criticality or RootCriticality.LOW,
            gitignore=attributes_to_add.gitignore,
            includes_health_check=attributes_to_add.includes_health_check,
            modified_by=user_email,
            modified_date=modified_date,
            nickname=nickname,
            other=attributes_to_add.other,
            reason=attributes_to_add.reason,
            status=RootStatus.ACTIVE,
            url=url,
            use_egress=attributes_to_add.use_egress,
            use_vpn=attributes_to_add.use_vpn,
            use_ztna=attributes_to_add.use_ztna,
        ),
        type=RootType.GIT,
        unreliable_indicators=RootUnreliableIndicators(
            unreliable_last_status_update=modified_date,
        ),
    )
    await roots_model.add(root=root)

    if attributes_to_add.includes_health_check and should_notify_health_check:
        await notify_health_check(
            group_name=group.name,
            root=root,
            user_email=user_email,
        )

    return root


async def add_ip_root(
    *,
    loaders: Dataloaders,
    attributes_to_add: IpRootAttributesToAdd,
    group_name: str,
    user_email: str,
    ensure_org_uniqueness: bool = True,
) -> IPRoot:
    if not validations.is_valid_ip(attributes_to_add.address):
        raise InvalidParameter()

    group = await get_group(loaders, group_name)
    organization = await orgs_utils.get_organization(loaders, group.organization_id)
    if (
        ensure_org_uniqueness
        and (
            group.state.type != GroupSubscriptionType.ONESHOT
            and not validations.is_ip_unique(
                attributes_to_add.address,
                await loaders.organization_roots.load(organization.name),
                include_inactive=True,
            )
        )
    ) or (
        group.state.type != GroupSubscriptionType.CONTINUOUS
        and not validations.is_ip_group_unique(
            attributes_to_add.address,
            group_name,
            await loaders.organization_roots.load(organization.name),
            include_inactive=True,
        )
    ):
        raise RepeatedRoot()

    nickname = await _clear_and_assign_nickname(loaders, group_name, attributes_to_add.nickname)
    modified_date = datetime_utils.get_utc_now()
    root = IPRoot(
        created_by=user_email,
        created_date=modified_date,
        group_name=group_name,
        id=str(uuid4()),
        organization_name=organization.name,
        state=IPRootState(
            address=attributes_to_add.address,
            modified_by=user_email,
            modified_date=modified_date,
            nickname=nickname,
            other=attributes_to_add.other,
            reason=attributes_to_add.reason,
            status=RootStatus.ACTIVE,
        ),
        unreliable_indicators=RootUnreliableIndicators(
            unreliable_last_status_update=modified_date,
        ),
        type=RootType.IP,
    )
    await roots_model.add(root=root)

    return root


@custom_validations.assert_is_isalnum("group_name")
async def add_secret(
    *,
    secret: Secret,
    resource_id: str,
    resource_type: ResourceType,
    group_name: str,
    loaders: Dataloaders,
) -> None:
    custom_validations.validate_fields([secret.key, resource_id])
    custom_validations.validate_email_address(secret.state.owner)
    custom_validations.validate_field_length(
        secret.state.description or "",
        max_length=500,
    )
    custom_validations.validate_field_length(secret.value, min_length=1)

    group = await get_group(loaders, group_name)
    if resource_type == ResourceType.ROOT:
        await validations.validate_root_id(resource_id, group.name, loaders)
        await roots_model.add_secret(resource_id, secret)
    elif resource_type == ResourceType.URL:
        await validations.validate_environment_url_id(resource_id, group.name)
        await roots_model.add_root_environment_secret(group.name, resource_id, secret)
    else:
        raise ResourceTypeNotFound()


def should_filter_out_roots_with_unsolved_events(force: bool) -> bool:
    return not force and datetime_utils.get_hour_now() < "15:30:00"


async def clone_root_and_update_group_languages(
    *,
    loaders: Dataloaders,
    group: Group,
    modified_by: str,
    root: GitRoot,
) -> None:
    queued_sync = await queue_sync_git_roots(
        loaders=loaders,
        roots=(root,),
        group_name=root.group_name,
        modified_by=modified_by,
        group=group,
        queue_on_batch=True,
        should_queue_machine=True,
        should_queue_sbom=False,
    )
    if (
        queued_sync
        and queued_sync.batch_job_id
        and queued_sync.dynamo_pk
        and (clone_action := await batch_dal.get_action(action_key=queued_sync.dynamo_pk))
    ):
        await batch_dal.update_action_to_dynamodb(
            action=Action.CLONE_ROOTS,
            action_key=queued_sync.dynamo_pk,
            attributes=BatchProcessingToUpdate(
                dependent_actions=[
                    *(clone_action.dependent_actions or []),
                    DependentAction(
                        action_name=Action.UPDATE_GROUP_LANGUAGES,
                        additional_info={"root_ids": [root.id]},
                        queue=IntegratesBatchQueue.SMALL,
                    ),
                ],
            ),
        )


async def queue_sync_git_roots(
    *,
    loaders: Dataloaders,
    group_name: str,
    modified_by: str,
    group: Group | None = None,
    roots: Iterable[GitRoot] | None = None,
    check_existing_jobs: bool = True,
    force: bool = False,
    should_queue_machine: bool = False,
    should_queue_sbom: bool = False,
    queue_on_batch: bool = False,
) -> PutActionResult | None:
    group = group or await get_group(loaders, group_name)
    valid_roots = tuple(
        filter_active_git_roots(roots)
        if roots
        else await get_active_git_roots(loaders, group_name),
    )
    valid_roots = await roots_filter.filter_active_roots_with_credentials(
        loaders=loaders,
        roots=valid_roots,
        modified_by=modified_by,
    )
    if not valid_roots:
        LOGGER.info(
            "The group does not have valid roots to be cloned",
            extra={
                "extra": {
                    "group_name": group_name,
                    "roots": [(root.id, root.state.nickname) for root in roots or []],
                },
            },
        )
        return None
    if should_filter_out_roots_with_unsolved_events(force):
        specific_type: EventType | None = (
            EventType.CLONING_ISSUES if group.state.has_advanced else None
        )
        valid_roots = await roots_filter.filter_roots_unsolved_events(
            loaders=loaders,
            roots=valid_roots,
            group_name=group_name,
            modified_by=modified_by,
            specific_type=specific_type,
        )

    if check_existing_jobs:
        valid_roots = await roots_filter.filter_roots_already_in_queue(valid_roots)

    valid_roots = await roots_filter.filter_roots_working_creds(
        loaders=loaders,
        group_name=group_name,
        organization_id=group.organization_id,
        roots=valid_roots,
        force=force,
        modified_by=modified_by,
    )
    if not valid_roots:
        LOGGER.info(
            "The group does not have valid roots to be cloned, after checking for working creds",
            extra={
                "extra": {
                    "group_name": group_name,
                    "roots": [(root.id, root.state.nickname) for root in roots or []],
                },
            },
        )
        return None

    root_ids_batch, root_ids_cluster = utils.disgregate_roots_for_queues(
        queue_on_batch,
        list(valid_roots),
    )
    if root_ids_batch:
        batch_action = await utils.queue_sync_roots_batch(
            group_name=group_name,
            git_root_ids=root_ids_batch,
            modified_by=modified_by,
            should_queue_machine=should_queue_machine,
            should_queue_sbom=should_queue_sbom,
        )
    if root_ids_cluster:
        await collect(
            [
                utils.queue_sync_root_cluster(
                    group_name=group_name,
                    git_root_id=root_id,
                    modified_by=modified_by,
                    should_queue_machine=should_queue_machine,
                    should_queue_sbom=should_queue_sbom,
                )
                for root_id in root_ids_cluster
            ],
            workers=8,
        )
        batch_action = PutActionResult(
            success=True,
        )

    LOGGER.info(
        "Roots cloning queued",
        extra={
            "extra": {
                "batch_action": str(batch_action),
                "group_name": group_name,
                "len_root_ids_batch": len(root_ids_batch),
                "len_root_ids_cluster": len(root_ids_cluster),
            },
        },
    )

    return batch_action


async def _get_url_attributes(
    *,
    loaders: Dataloaders,
    url: str,
    group_name: str,
) -> tuple[str, str, str, str, str | None, Group, str]:
    try:
        url_attributes = parse_url(url)
    except LocationParseError as ex:
        raise InvalidParameter() from ex

    if not url_attributes.host or url_attributes.scheme not in {
        "http",
        "https",
        "file",
    }:
        raise InvalidParameter()

    host: str = url_attributes.host
    fragment: str | None = url_attributes.fragment
    path: str = url_attributes.path or "/"
    query: str | None = url_attributes.query
    default_port = "443" if url_attributes.scheme == "https" else "80"
    port = str(url_attributes.port) if url_attributes.port else default_port
    protocol: str = url_attributes.scheme.upper()
    group = await get_group(loaders, group_name)
    if protocol == "FILE":
        fragment = None
        query = None
        port = "0"
        if host not in {file.file_name for file in group.files or []}:
            raise FileNotFound()

    organization = await orgs_utils.get_organization(loaders, group.organization_id)
    organization_name = organization.name
    if fragment:
        path = f"{path}#{fragment}"

    return (
        host,
        path,
        port,
        protocol,
        query,
        group,
        organization_name,
    )


async def add_url_root(
    *,
    loaders: Dataloaders,
    attributes_to_add: UrlRootAttributesToAdd,
    group_name: str,
    user_email: str,
    ensure_org_uniqueness: bool = True,
) -> URLRoot:
    custom_validations.check_url(attributes_to_add.url)
    custom_validations.validate_sanitized_csv_input(
        attributes_to_add.url,
        attributes_to_add.nickname,
    )
    custom_validations.validate_fields([attributes_to_add.url])
    nickname = await _clear_and_assign_nickname(
        loaders,
        group_name,
        attributes_to_add.nickname,
    )
    (
        host,
        path,
        port,
        protocol,
        query,
        group,
        organization_name,
    ) = await _get_url_attributes(
        loaders=loaders,
        url=attributes_to_add.url,
        group_name=group_name,
    )
    if (
        ensure_org_uniqueness
        and (
            group.state.type != GroupSubscriptionType.ONESHOT
            and not validations.is_url_unique(
                host=host,
                path=path,
                port=port,
                protocol=protocol,
                query=query,
                roots=tuple(await loaders.organization_roots.load(organization_name)),
                include_inactive=True,
            )
        )
    ) or (
        group.state.type != GroupSubscriptionType.CONTINUOUS
        and not validations.is_url_group_unique(
            group_name=group_name,
            host=host,
            path=path,
            port=port,
            protocol=protocol,
            query=query,
            roots=tuple(await loaders.organization_roots.load(organization_name)),
            include_inactive=True,
        )
    ):
        raise RepeatedRoot()

    modified_date = datetime_utils.get_utc_now()
    root = URLRoot(
        created_by=user_email,
        created_date=modified_date,
        group_name=group_name,
        id=str(uuid4()),
        organization_name=organization_name,
        state=URLRootState(
            host=host,
            modified_by=user_email,
            modified_date=modified_date,
            nickname=nickname,
            other=attributes_to_add.other,
            path=path,
            port=port,
            protocol=protocol,
            query=query,
            reason=attributes_to_add.reason,
            status=RootStatus.ACTIVE,
        ),
        unreliable_indicators=RootUnreliableIndicators(
            unreliable_last_status_update=modified_date,
        ),
        type=RootType.URL,
    )
    await roots_model.add(root=root)

    return root


@validation_deco_utils.validate_sanitized_csv_input_deco(["new_nickname"])
@validations.validate_nickname_deco("new_nickname")
@validations.validate_nickname_is_unique_deco(
    nickname_field="new_nickname",
    roots_fields="_roots",
    old_nickname_field="nickname",
)
def assign_nickname(nickname: str, new_nickname: str, _roots: Iterable[Root]) -> str:
    if new_nickname and new_nickname != nickname:
        return new_nickname
    return nickname


async def _clear_and_assign_nickname(
    loaders: Dataloaders,
    group_name: str,
    new_nickname: str,
) -> str:
    loaders.group_roots.clear(group_name)
    nickname: str = assign_nickname(
        nickname="",
        new_nickname=new_nickname,
        _roots=await loaders.group_roots.load(group_name),
    )
    return nickname


async def activate_root(
    *,
    loaders: Dataloaders,
    email: str,
    group_name: str,
    root: Root,
) -> None:
    new_status = RootStatus.ACTIVE

    if root.state.status != new_status:
        group = await get_group(loaders, group_name)
        organization = await orgs_utils.get_organization(loaders, group.organization_id)
        organization_name = organization.name
        org_roots = await loaders.organization_roots.load(organization_name)

        await validations.validate_root_and_update(email, group_name, new_status, org_roots, root)


async def deactivate_root(
    *,
    loaders: Dataloaders,
    email: str,
    group_name: str,
    other: str | None,
    reason: RootStateReason,
    root: Root,
) -> None:
    if root.state.status == RootStatus.INACTIVE:
        return

    root_env_urls = await loaders.root_environment_urls.load(
        RootEnvironmentUrlsRequest(root_id=root.id, group_name=group_name),
    )
    urls_to_exclude = [url for url in root_env_urls if url.state.include]
    await collect(
        [
            roots_model.update_root_environment_url_state(
                current_value=url.state,
                root_id=root.id,
                url_id=url.id,
                group_name=group_name,
                state=url.state._replace(
                    modified_date=datetime_utils.get_now(),
                    modified_by=email,
                    include=url.state.include
                    if reason == RootStateReason.MOVED_TO_ANOTHER_GROUP
                    else False,
                ),
            )
            for url in urls_to_exclude
        ],
        workers=4,
    )

    if isinstance(root, GitRoot):
        root_docker_images: list[RootDockerImage] = await loaders.root_docker_images.load(
            RootDockerImagesRequest(root_id=root.id, group_name=group_name)
        )
        root_docker_images_to_exclude = [
            docker_image for docker_image in root_docker_images if docker_image.state.include
        ]
        await collect(
            [
                roots_model.update_root_docker_image_state(
                    current_value=docker_image.state,
                    root_id=docker_image.root_id,
                    group_name=group_name,
                    state=deepcopy(docker_image.state)._replace(
                        modified_date=datetime_utils.get_now(),
                        modified_by=email,
                        include=docker_image.state.include
                        if reason == RootStateReason.MOVED_TO_ANOTHER_GROUP
                        else False,
                    ),
                    uri=docker_image.uri,
                )
                for docker_image in root_docker_images_to_exclude
            ],
            workers=4,
        )

        await roots_model.update_root_state(
            current_value=root.state,
            group_name=group_name,
            root_id=root.id,
            state=GitRootState(
                branch=root.state.branch,
                credential_id=root.state.credential_id,
                criticality=root.state.criticality,
                gitignore=root.state.gitignore,
                includes_health_check=root.state.includes_health_check,
                modified_by=email,
                modified_date=datetime_utils.get_utc_now(),
                nickname=root.state.nickname,
                other=other,
                reason=reason,
                status=RootStatus.INACTIVE,
                url=root.state.url,
                use_egress=root.state.use_egress,
                use_vpn=root.state.use_vpn,
                use_ztna=root.state.use_ztna,
            ),
        )

    elif isinstance(root, IPRoot):
        await roots_model.update_root_state(
            current_value=root.state,
            group_name=group_name,
            root_id=root.id,
            state=IPRootState(
                address=root.state.address,
                modified_by=email,
                modified_date=datetime_utils.get_utc_now(),
                nickname=root.state.nickname,
                other=other,
                reason=reason,
                status=RootStatus.INACTIVE,
            ),
        )

    else:
        await roots_model.update_root_state(
            current_value=root.state,
            group_name=group_name,
            root_id=root.id,
            state=URLRootState(
                host=root.state.host,
                modified_by=email,
                modified_date=datetime_utils.get_utc_now(),
                nickname=root.state.nickname,
                other=other,
                path=root.state.path,
                port=root.state.port,
                protocol=root.state.protocol,
                reason=reason,
                status=RootStatus.INACTIVE,
            ),
        )


def get_root_id_by_nickname(
    nickname: str,
    group_roots: Iterable[Root],
    only_git_roots: bool = False,
) -> str:
    root_ids_by_nicknames = roots_utils.get_root_ids_by_nicknames(
        group_roots=group_roots,
        only_git_roots=only_git_roots,
    )
    return get_root_id_by_nicknames(nickname=nickname, root_ids_by_nicknames=root_ids_by_nicknames)


def get_root_id_by_nicknames(
    nickname: str,
    root_ids_by_nicknames: dict[str, str],
) -> str:
    try:
        root_id = root_ids_by_nicknames[nickname]
    except KeyError as exc:
        LOGGER.error(
            "Root not found",
            extra={
                "extra": {
                    "nickname": nickname,
                    "roots": root_ids_by_nicknames,
                },
            },
        )
        raise RootNotFound() from exc

    return root_id


async def get_environment_url_secrets(
    loaders: Dataloaders,
    env_url: RootEnvironmentUrl,
) -> list[Secret]:
    if env_url.state.cloud_name == RootEnvironmentCloud.AWS and re.match(
        r"^arn:aws:iam::\d{12}:role\/[\w+=,.@-]+$",
        env_url.url,
    ):
        root = await loaders.root.load(
            RootRequest(group_name=env_url.group_name, root_id=env_url.root_id),
        )
        group = await loaders.group.load(env_url.group_name)
        if isinstance(root, GitRoot) and group:
            org = await loaders.organization.load(group.organization_id)
            try:
                return await fetch_temp_aws_creds(
                    env_url.url,
                    org.state.aws_external_id if org else None,
                )
            except InvalidAWSRoleTrustPolicy:
                LOGGER.error(
                    "Unable to assume AWS Role for an Environment URL",
                    extra={"extra": {"group": group.name}},
                )
                return []
    return await loaders.environment_secrets.load(
        RootEnvironmentSecretsRequest(url_id=env_url.id, group_name=env_url.group_name),
    )


async def get_environment_url(
    loaders: Dataloaders,
    root_id: str,
    group_name: str,
    url_id: str,
) -> RootEnvironmentUrl:
    environment_url = await loaders.environment_url.load(
        RootEnvironmentUrlRequest(root_id=root_id, group_name=group_name, url_id=url_id),
    )
    if environment_url.state.status is RootEnvironmentUrlStateStatus.DELETED:
        LOGGER.error(
            "Environment URL not found",
            extra={
                "extra": {
                    "root_id": root_id,
                    "url_id": url_id,
                    "status": environment_url.state.status,
                },
            },
        )
        raise RootEnvironmentUrlNotFound()
    return environment_url


async def move_root(
    *,
    loaders: Dataloaders,
    email: str,
    group_name: str,
    root_id: str,
    target_group_name: str,
) -> Root:
    source_group = await get_group(loaders, group_name)
    target_group = await get_group(loaders, target_group_name)
    root = await roots_utils.get_root(loaders, root_id, group_name)
    if (
        root.state.status != RootStatus.ACTIVE
        or target_group_name == root.group_name
        or target_group_name
        not in await orgs_domain.get_group_names(loaders, source_group.organization_id)
        or source_group.state.service != target_group.state.service
    ):
        raise InvalidParameter("group_name")

    target_group_roots = await loaders.group_roots.load(target_group_name)
    new_root_state_other = "#".join(
        [
            f"GROUP#{group_name}",
            f"ROOT#{root.id}",
            f"NICKNAME#{root.state.nickname}",
        ],
    )

    if isinstance(root, GitRoot):
        if not validations.is_git_unique(
            url=root.state.url,
            branch=root.state.branch,
            group_name=group_name,
            roots=target_group_roots,
        ):
            raise RepeatedRoot()

        new_root: Root = await add_git_root(
            loaders=loaders,
            attributes_to_add=GitRootAttributesToAdd(
                branch=root.state.branch,
                credentials=(
                    {"id": root.state.credential_id} if root.state.credential_id else None
                ),
                gitignore=root.state.gitignore,
                includes_health_check=root.state.includes_health_check,
                url=root.state.url,
                criticality=root.state.criticality,
                nickname=root.state.nickname,
                other=new_root_state_other,
                reason=RootStateReason.MOVED_FROM_ANOTHER_GROUP,
                use_egress=root.state.use_egress,
                use_vpn=root.state.use_vpn,
                use_ztna=root.state.use_ztna,
            ),
            user_email=email,
            group=target_group,
            ensure_org_uniqueness=False,
            should_notify_health_check=False,
        )
    elif isinstance(root, IPRoot):
        if not validations.is_ip_unique(
            root.state.address,
            target_group_roots,
            include_inactive=True,
        ):
            raise RepeatedRoot()

        new_root = await add_ip_root(
            loaders=loaders,
            attributes_to_add=IpRootAttributesToAdd(
                address=root.state.address,
                nickname=root.state.nickname,
                other=new_root_state_other,
                reason=RootStateReason.MOVED_FROM_ANOTHER_GROUP,
            ),
            group_name=target_group_name,
            user_email=email,
            ensure_org_uniqueness=False,
        )
    else:
        if not validations.is_url_unique(
            host=root.state.host,
            path=root.state.path,
            port=root.state.port,
            protocol=root.state.protocol,
            query=root.state.query,
            roots=target_group_roots,
            include_inactive=True,
        ):
            raise RepeatedRoot()

        query = utils.get_query(root)
        path = utils.get_path(root)
        new_root = await add_url_root(
            loaders=loaders,
            attributes_to_add=UrlRootAttributesToAdd(
                nickname=root.state.nickname,
                url=(f"{root.state.protocol}://{root.state.host}:{root.state.port}{path}{query}"),
                other=new_root_state_other,
                reason=RootStateReason.MOVED_FROM_ANOTHER_GROUP,
            ),
            group_name=target_group_name,
            user_email=email,
            ensure_org_uniqueness=False,
        )

    await deactivate_root(
        loaders=loaders,
        group_name=group_name,
        other="#".join(
            [
                f"GROUP#{target_group_name}",
                f"ROOT#{new_root.id}",
                f"NICKNAME#{new_root.state.nickname}",
            ],
        ),
        reason=RootStateReason.MOVED_TO_ANOTHER_GROUP,
        root=root,
        email=email,
    )

    return new_root


async def remove_environment_url_id(
    *,
    loaders: Dataloaders,
    root_id: str,
    url_id: str,
    user_email: str,
    group_name: str,
    should_notify: bool = True,
) -> RootEnvironmentUrl:
    root = await loaders.root.load(RootRequest(group_name, root_id))
    if not isinstance(root, GitRoot):
        raise InvalidRootType()

    env_url = await get_environment_url(loaders, root_id, group_name, url_id)
    await roots_model.update_root_environment_url_state(
        current_value=env_url.state,
        root_id=root_id,
        group_name=group_name,
        state=env_url.state._replace(
            modified_date=datetime_utils.get_now(),
            modified_by=user_email,
            status=RootEnvironmentUrlStateStatus.DELETED,
        ),
        url_id=url_id,
    )
    if should_notify:
        schedule(
            utils.send_mail_environment(
                loaders=loaders,
                modified_date=datetime_utils.get_utc_now(),
                group_name=group_name,
                git_root=root.state.nickname,
                git_root_url=root.state.url,
                urls_added=[],
                urls_deleted=[env_url.url],
                user_email=user_email,
                other=None,
                reason=None,
            ),
        )

    return env_url


async def remove_secret(
    resource_id: str,
    secret_key: str,
    resource_type: ResourceType,
    group_name: str,
    loaders: Dataloaders,
) -> None:
    await get_group(loaders, group_name)
    if resource_type == ResourceType.ROOT:
        await validations.validate_root_id(resource_id, group_name, loaders)
        await roots_model.remove_root_secret(resource_id, secret_key)
    elif resource_type == ResourceType.URL:
        await validations.validate_environment_url_id(resource_id, group_name)
        await roots_model.remove_environment_url_secret(group_name, resource_id, secret_key)
    else:
        raise ResourceTypeNotFound()


async def remove_root(
    *,
    loaders: Dataloaders,
    email: str,
    group_name: str,
    reason: RootStateReason,
    root: Root,
) -> None:
    await deactivate_root(
        loaders=loaders,
        group_name=group_name,
        other=None,
        reason=reason,
        root=root,
        email=email,
    )
    if isinstance(root, GitRoot):
        await deactivate_root_toe_packages(loaders, root.id, group_name)
    await roots_model.remove(group_name=group_name, root_id=root.id, root_state=root.state)
    LOGGER.info(
        "Root removed",
        extra={
            "extra": {
                "root_id": root.id,
                "group_name": root.group_name,
            },
        },
    )


def _check_gitignore_pattern(path: str, pattern: str) -> bool:
    return fnmatch(path, pattern) or (not pattern.endswith("*") and fnmatch(path, f"{pattern}/*"))


def _check_start_gitignore_pattern(path: str, pattern: str) -> bool:
    if pattern.find("**/") != -1:
        new_pattern = pattern.replace("**/", "*")
        return _check_gitignore_pattern(path, new_pattern)
    return False


def _gitignore_enforcer(exclusions_list: list[str]) -> Callable[[str], bool]:
    """
    Returns a function that checks if a path is excluded by the given
    exclusions list.

    Args:
        exclusions_list (List[str]): List of exclusions in gitignore format.
    (https://mirrors.edge.kernel.org/pub/software/scm/git/docs/gitignore.html)

    Returns:
        Callable[[str], bool]: Function that checks if a path must be excluded.

    """
    exclusion_list = [
        validations.sanitize_exclusion_line(line)
        for line in exclusions_list
        if not line.startswith("!")
    ]

    white_list = [
        validations.sanitize_exclusion_line(line)
        for line in exclusions_list
        if line.startswith("!")
    ]

    def checker(path: str) -> bool:
        is_excluded = False
        is_protected = False

        for pattern in exclusion_list:
            if _check_start_gitignore_pattern(path, pattern) or _check_gitignore_pattern(
                path,
                pattern,
            ):
                is_excluded = True
                break

        for pattern in white_list:
            if _check_start_gitignore_pattern(path, pattern) or _check_gitignore_pattern(
                path,
                pattern,
            ):
                is_protected = True
                break

        return False if is_protected else is_excluded

    return checker


async def get_root_open_vulnerabilities(
    *,
    loaders: Dataloaders,
    root_id: str,
) -> list[Vulnerability]:
    return [
        vuln
        for vuln in await loaders.root_vulnerabilities.load(root_id)
        if vuln.state.status == VulnerabilityStateStatus.VULNERABLE
    ]


async def get_root_open_findings(
    *,
    loaders: Dataloaders,
    root_id: str,
) -> list[Finding]:
    vulns = await get_root_open_vulnerabilities(loaders=loaders, root_id=root_id)
    finding_ids = frozenset(vuln.finding_id for vuln in vulns)
    findings = await loaders.finding.load_many(finding_ids)
    return [finding for finding in findings if finding is not None]


async def comment_vulnerabilities_by_exclusions(
    *,
    loaders: Dataloaders,
    root_id: str,
    email: str,
    date: datetime,
    exclusions: list[str],
) -> list[Vulnerability]:
    check_exclusions: Callable[[str], bool] = _gitignore_enforcer(exclusions)

    vulnerabilities: list[Vulnerability] = [
        vuln
        for vuln in await loaders.root_vulnerabilities.load(root_id)
        if check_exclusions(vuln.state.where)
        and vuln.state.status == VulnerabilityStateStatus.VULNERABLE
    ]
    findings_ids = {vuln.finding_id for vuln in vulnerabilities}
    findings_and_vulns = {
        fin_id: [
            f"{vuln.id} ({vuln.state.where})"
            for vuln in vulnerabilities
            if vuln.finding_id == fin_id
        ]
        for fin_id in findings_ids
    }
    for finding_id in findings_and_vulns:
        comment_id = str(round(time() * 1000))
        vulns_list = "".join([f"- {vuln_info} \n" for vuln_info in findings_and_vulns[finding_id]])
        comment_date = date.strftime("%Y-%m-%d at %H:%M.")
        comment_data = FindingComment(
            finding_id=finding_id,
            id=comment_id,
            comment_type=CommentType.COMMENT,
            content=f"Vulnerabilities were closed due to exclusion performed "
            f"on {comment_date}\n\n"
            f"Closed vulnerabilities:\n"
            f"{vulns_list}\n"
            f"Exclusion performed by: {email}\n",
            parent_id="0",
            email=email,
            full_name="Fluid Attacks",
            creation_date=datetime_utils.get_utc_now(),
        )
        await comments_domain.add(loaders, comment_data)
    return vulnerabilities


async def queue_actions_on_status_change(
    loaders: Dataloaders,
    email: str,
    group_name: str,
    root: Root,
    new_status: RootStatus,
) -> None:
    if isinstance(root, GitRoot):
        if new_status == RootStatus.ACTIVE:
            await queue_sync_git_roots(
                loaders=loaders,
                roots=(root,),
                group_name=root.group_name,
                modified_by=email,
                force=True,
                should_queue_machine=True,
                queue_on_batch=True,
            )
        else:
            await batch_dal.put_action(
                action=Action.REFRESH_TOE_LINES,
                attempt_duration_seconds=7200,
                entity=group_name,
                subject=email,
                additional_info={"root_ids": [root.id]},
                queue=IntegratesBatchQueue.SMALL,
            )
        await batch_dal.put_action(
            action=Action.REFRESH_REPOSITORIES,
            attempt_duration_seconds=7200,
            entity=group_name,
            subject=email,
            additional_info={},
            queue=IntegratesBatchQueue.SMALL,
        )

    if isinstance(root, (GitRoot, URLRoot)):
        await batch_dal.put_action(
            action=Action.REFRESH_TOE_INPUTS,
            entity=group_name,
            subject=email,
            additional_info={"root_ids": [root.id]},
            queue=IntegratesBatchQueue.SMALL,
        )

    if isinstance(root, IPRoot):
        await batch_dal.put_action(
            action=Action.REFRESH_TOE_PORTS,
            entity=group_name,
            subject=email,
            additional_info={"root_ids": [root.id]},
            queue=IntegratesBatchQueue.SMALL,
        )


def _credentials_changed(credentials: dict[str, str] | None, root_credential: str | None) -> bool:
    if not credentials:
        return False
    if credentials.get("name") or (
        root_credential and credentials.get("id") and credentials.get("id") != root_credential
    ):
        return True
    return False


def _changed_any_use(
    root: GitRoot,
    **kwargs: Any,
) -> bool:
    return bool(
        root.state.use_egress != kwargs.get("use_egress")
        or root.state.use_vpn != kwargs.get("use_vpn")
        or root.state.use_ztna != kwargs.get("use_ztna"),
    )


async def roots_sync_needed(
    *,
    loaders: Dataloaders,
    root_id: str,
    **kwargs: Any,
) -> bool:
    group_name: str = str(kwargs.get("group_name")).lower()
    root = await get_root(loaders, root_id, group_name)
    if not isinstance(root, GitRoot):
        return False
    credentials_changed = _credentials_changed(kwargs.get("credentials"), root.state.credential_id)
    if (
        credentials_changed
        or root.state.url != kwargs.get("url")
        or root.state.branch != kwargs.get("branch")
        or _changed_any_use(root, **kwargs)
    ):
        return True

    return False


async def _get_credentials_image_to_add(
    *,
    loaders: Dataloaders,
    group: Group,
    credentials: str | Credentials,
) -> Credentials | None:
    organization_credential: Credentials | None = None
    if (
        credential_id := (credentials.id if isinstance(credentials, Credentials) else credentials)
    ) and credential_id:
        await validations.validate_credential_in_organization(
            loaders,
            credential_id,
            group.organization_id,
        )
        organization_credential = await loaders.credentials.load(
            CredentialsRequest(
                id=credential_id,
                organization_id=group.organization_id,
            ),
        )

    return organization_credential


async def _format_image_to_add(
    *,
    loaders: Dataloaders,
    group: Group,
    root_id: str,
    credential_id: str | None,
    user_email: str,
    uri: str,
) -> tuple[RootDockerImage, tuple[RootDockerImageLayer, ...]]:
    created_at = datetime.now()
    organization_credential: Credentials | None = None
    await validations.validate_image_uri_in_root(loaders, root_id, group.name, uri)
    organization = await orgs_utils.get_organization(loaders, group.organization_id)
    external_id = organization.state.aws_external_id if organization else None
    if credential_id:
        organization_credential = await _get_credentials_image_to_add(
            loaders=loaders,
            group=group,
            credentials=credential_id,
        )

    image_manifest_config = await get_image_manifest(
        image_ref=uri,
        credentials=organization_credential,
        config=True,
        external_id=external_id,
    )
    image_manifest = await get_image_manifest(
        image_ref=uri,
        credentials=organization_credential,
        external_id=external_id,
    )

    return RootDockerImage(
        uri=uri,
        created_at=created_at,
        created_by=user_email,
        root_id=root_id,
        group_name=group.name,
        state=RootDockerImageState(
            credential_id=(organization_credential.id if organization_credential else None),
            modified_date=created_at,
            modified_by=user_email,
            status=RootDockerImageStateStatus.CREATED,
            digest=image_manifest["Digest"],
            history=[
                DockerImageHistory(
                    created=datetime.fromisoformat(x["created"]),
                    created_by=x.get("created_by"),
                    empty_layer=x.get("empty_layer") or False,
                    comment=x.get("comment"),
                )
                for x in image_manifest_config.get("history", [])
            ],
            layers=image_manifest["Layers"],
        ),
    ), tuple(
        RootDockerImageLayer(
            image_uri=uri,
            digest=layer_digest["Digest"],
            size=layer_digest["Size"],
        )
        for layer_digest in image_manifest["LayersData"]
        if layer_digest["Digest"] in image_manifest["Layers"]
    )


async def add_root_docker_image(
    *,
    loaders: Dataloaders,
    group_name: str,
    root_id: str,
    credential_id: str | None,
    uri: str,
    user_email: str,
) -> RootDockerImage | None:
    group = await get_group(loaders, group_name.lower())
    root = await loaders.root.load(RootRequest(group.name, root_id))
    if not isinstance(root, GitRoot):
        raise InvalidRootType()

    toe_lines = await loaders.root_toe_lines.load(
        RootToeLinesRequest(group_name=group.name, root_id=root_id),
    )

    if not any(
        toe_line.node.filename.endswith("Dockerfile") and toe_line.node.state.be_present
        for toe_line in toe_lines.edges
    ):
        raise RootDockerfileNotExists()

    image, layers = await _format_image_to_add(
        loaders=loaders,
        group=group,
        root_id=root_id,
        credential_id=credential_id,
        user_email=user_email,
        uri=uri,
    )

    success_layers = all(
        await collect(
            roots_model.add_root_docker_image_layer(
                group_name=group.name,
                root_id=root_id,
                layer=layer,
            )
            for layer in layers
        ),
    )
    if not success_layers:
        LOGGER.error(
            "Failed to add Docker image layers",
            extra={
                "root_id": root.id,
                "group_name": root.group_name,
                "image_uri": image.uri,
            },
        )
        return None

    is_image_added = await roots_model.add_root_docker_image(
        group_name=group.name,
        root_id=root_id,
        image=image,
    )
    if not is_image_added:
        LOGGER.error(
            "Failed to add Docker image",
            extra={
                "root_id": root.id,
                "group_name": root.group_name,
                "image_uri": image.uri,
            },
        )
        return None

    return image


def update_locations(package: ToePackage, uri: str) -> list[ToePackageCoordinates]:
    new_locations = []
    new_locations = [
        location
        for location in package.locations
        if not (location.image_ref and location.image_ref == uri)
    ]
    return new_locations


async def update_package(package: ToePackage, root_image: RootDockerImage) -> None:
    updated_locations = update_locations(package, root_image.uri)
    new_state = package._replace(
        locations=updated_locations,
        be_present=False if not updated_locations else package.be_present,
        modified_date=datetime_utils.get_utc_now(),
    )
    await toe_packages.update_package(
        current_value=package,
        package=new_state,
    )


async def deactivate_image_related_resources(
    loaders: Dataloaders,
    root_image: RootDockerImage,
) -> None:
    root_docker_image_pkgs_ref = await loaders.root_image_toe_packages.load(
        RootDockerImagePackagesRequest(uri=root_image.uri, root_id=root_image.root_id),
    )
    pkgs_to_update = await loaders.toe_package.load_many(
        ToePackageRequest(root_image.group_name, ref.root_id, ref.name, ref.version)
        for ref in root_docker_image_pkgs_ref
    )

    await collect(
        tuple(
            update_package(package, root_image) for package in pkgs_to_update if package is not None
        ),
    )


@retry_on_exceptions(exceptions=(ToeInputAlreadyUpdated, UnavailabilityError), sleep_seconds=0.5)
async def deactivate_root_toe_package(loaders: Dataloaders, package: ToePackage) -> None:
    new_state = package._replace(
        be_present=False,
        modified_date=datetime_utils.get_utc_now(),
    )
    try:
        await toe_packages.update_package(
            current_value=package,
            package=new_state,
        )
    except ToePackageAlreadyUpdated:
        key = ToePackageRequest(
            group_name=package.group_name,
            root_id=package.root_id,
            name=package.name,
            version=package.version,
        )
        loaders.toe_package.clear(key)
        _package = await loaders.toe_package.load(key)
        if not _package or not _package.be_present:
            return
        await toe_packages.update_package(
            current_value=_package,
            package=_package._replace(
                be_present=False,
                modified_date=datetime_utils.get_utc_now(),
            ),
        )


@retry_on_exceptions(exceptions=(EndpointConnectionError,), sleep_seconds=5.0)
async def deactivate_root_toe_packages(loaders: Dataloaders, root_id: str, group_name: str) -> None:
    active_root_toe_packages: ToePackagesConnection = await loaders.root_toe_packages.load(
        RootToePackagesRequest(root_id=root_id, group_name=group_name, be_present=True),
    )

    if not active_root_toe_packages:
        return

    await collect(
        tuple(
            deactivate_root_toe_package(loaders, package_edge.node)
            for package_edge in active_root_toe_packages.edges
        ),
        workers=4,
    )


async def update_root_docker_image(
    *,
    loaders: Dataloaders,
    uri: str,
    group: Group,
    user_email: str,
    root_id: str,
    credential_id: str | None,
) -> None:
    root = await loaders.root.load(RootRequest(group.name, root_id))
    if not isinstance(root, GitRoot):
        raise InvalidRootType()
    docker_image = await utils.get_root_docker_image(loaders, root_id, group.name, uri)
    organization = await orgs_utils.get_organization(loaders, group.organization_id)
    external_id = organization.state.aws_external_id if organization else None

    organization_credential = (
        await _get_credentials_image_to_add(loaders=loaders, group=group, credentials=credential_id)
        if credential_id
        else None
    )

    await validations.validate_image_access(
        uri=uri, external_id=external_id, credentials=organization_credential
    )

    await roots_model.update_root_docker_image_state(
        current_value=docker_image.state,
        root_id=root_id,
        group_name=group.name,
        state=docker_image.state._replace(
            credential_id=organization_credential.id if organization_credential else None,
            modified_date=datetime_utils.get_now(),
            modified_by=user_email,
        ),
        uri=uri,
    )


async def deactivate_root_docker_image(
    loaders: Dataloaders,
    docker_image: RootDockerImage,
    user_email: str,
) -> None:
    await deactivate_image_related_resources(
        loaders=loaders,
        root_image=docker_image,
    )
    await update_root_docker_image_state(
        current_value=docker_image.state,
        root_id=docker_image.root_id,
        group_name=docker_image.group_name,
        state=deepcopy(docker_image.state)._replace(
            include=False,
            modified_date=datetime_utils.get_now(),
            modified_by=user_email,
        ),
        uri=docker_image.uri,
    )


async def remove_docker_image(
    *,
    loaders: Dataloaders,
    root_id: str,
    uri: str,
    user_email: str,
    group_name: str,
) -> None:
    root = await loaders.root.load(RootRequest(group_name, root_id))
    if not isinstance(root, GitRoot):
        raise InvalidRootType()

    docker_image = await utils.get_root_docker_image(loaders, root_id, group_name, uri)
    await roots_model.update_root_docker_image_state(
        current_value=docker_image.state,
        root_id=root_id,
        group_name=group_name,
        state=docker_image.state._replace(
            modified_date=datetime_utils.get_now(),
            modified_by=user_email,
            status=RootDockerImageStateStatus.DELETED,
        ),
        uri=uri,
    )

    await deactivate_image_related_resources(loaders, docker_image)


async def removed_related_events(
    loaders: Dataloaders, group_name: str, root_env: RootEnvironmentUrl
) -> None:
    unresolved_events = await get_unsolved_events_by_root(
        loaders=loaders,
        group_name=group_name,
    )

    role_events = [
        event
        for events in unresolved_events.values()
        for event in events
        if root_env.url == event.environment_url
    ]

    for event in role_events:
        await solve_event(
            loaders=loaders,
            event_id=event.id,
            group_name=group_name,
            hacker_email="machine@fluidattacks.com",
            reason=EventSolutionReason.AFFECTED_RESOURCE_REMOVED_FROM_SCOPE,
            other=None,
        )


async def remove_related_resources(
    loaders: Dataloaders,
    info: GraphQLResolveInfo,
    user_email: str,
    root_env: RootEnvironmentUrl,
) -> None:
    try:
        inputs_to_update = await toe_inputs_domain.get_root_env_toe_inputs(root_env)
        vulnerabilities = await get_root_vulnerabilities(root_env.root_id)
        await collect(
            [
                toe_inputs_domain.modify_vulns_in_input_exclusion(
                    toe_input,
                    vulnerabilities,
                    user_email,
                    loaders=loaders,
                )
                for toe_input in inputs_to_update
            ],
        )

        await collect(
            tuple(
                toe_inputs_domain.update(
                    loaders=loaders,
                    current_value=current_value,
                    attributes=ToeInputAttributesToUpdate(
                        be_present=False,
                    ),
                    modified_by=user_email,
                )
                for current_value in inputs_to_update
            ),
        )
        logs_utils.cloudwatch_log(
            info.context,
            "Updated vulnerabilities and toe inputs of the environment",
            extra={
                "url_id": root_env.id,
                "root_id": root_env.root_id,
                "log_type": "Security",
            },
        )
    except APP_EXCEPTIONS:
        logs_utils.cloudwatch_log(
            info.context,
            "Attempted to update vulnerabilities and toe inputs in the environment",
            extra={
                "url_id": root_env.id,
                "root_id": root_env.root_id,
                "log_type": "Security",
            },
        )
        raise
