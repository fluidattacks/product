import csv
import io
import itertools
import json
import logging
import logging.config
import re
from collections.abc import Callable, Sequence
from datetime import datetime
from typing import Literal
from urllib.parse import unquote, urlparse, urlunparse
from uuid import uuid4

from aioextensions import in_thread, schedule
from botocore.exceptions import ClientError
from fluidattacks_core.git import ls_remote
from more_itertools import chunked
from starlette.datastructures import UploadFile
from urllib3.util.url import Url

from integrates.batch import dal as batch_dal
from integrates.batch.enums import Action, IntegratesBatchQueue
from integrates.batch.types import PutActionResult
from integrates.class_types.types import Item
from integrates.context import FI_AWS_S3_CONTINUOUS_REPOSITORIES, FI_ENVIRONMENT
from integrates.custom_exceptions import (
    CredentialNotInOrganization,
    EmptyFile,
    ErrorUpdatingRootItem,
    InvalidCSVFormat,
    InvalidFileType,
    InvalidParameter,
    InvalidProtocol,
    MailerClientError,
    RootDockerImageNotFound,
    RootNicknameUsed,
    UnableToSendMail,
)
from integrates.custom_utils import datetime as datetime_utils
from integrates.custom_utils import files as files_utils
from integrates.custom_utils import roots as roots_utils
from integrates.custom_utils import validations as validation_utils
from integrates.custom_utils import validations_deco as validation_deco_utils
from integrates.custom_utils.constants import (
    SCHEDULE_CLONE_GROUPS_ROOTS_EMAIL,
    SCHEDULE_MACHINE_QUEUE_SAST_SCA_EMAIL,
)
from integrates.custom_utils.utils import codecommitparser, httpsparser, sshparser
from integrates.dataloaders import Dataloaders, get_new_context
from integrates.db_model import roots as roots_model
from integrates.db_model.credentials.types import (
    AWSRoleSecret,
    Credentials,
    CredentialsState,
    HttpsPatSecret,
    HttpsSecret,
    OauthAzureSecret,
    OauthBitbucketSecret,
    OauthGithubSecret,
    OauthGitlabSecret,
    SshSecret,
)
from integrates.db_model.enums import CredentialType
from integrates.db_model.groups.enums import GroupStateStatus
from integrates.db_model.roots.enums import (
    RootCloningStatus,
    RootCriticality,
    RootDockerImageStateStatus,
    RootMachineStatus,
    RootRebaseStatus,
    RootStatus,
    RootToeLinesStatus,
)
from integrates.db_model.roots.types import (
    GitRoot,
    GitRootMachine,
    GitRootRebase,
    GitRootState,
    GitRootToeLines,
    IPRootState,
    MachineExecutionsDate,
    Root,
    RootDockerImage,
    RootDockerImageRequest,
    URLRoot,
    URLRootState,
)
from integrates.decorators import retry_on_exceptions
from integrates.dynamodb.exceptions import UnavailabilityError
from integrates.mailer import groups as groups_mail
from integrates.mailer import utils as mailer_utils
from integrates.oauth.common import get_last_commit_info
from integrates.organizations import utils as orgs_utils
from integrates.roots import validations
from integrates.roots.types import GitRootFileInputs
from integrates.s3 import operations as s3_operations
from integrates.settings.logger import LOGGING
from integrates.sqs.resource import get_client

MAX_ROOTS_PER_JOB = 20
ROOTS_ON_BATCH_THRESHOLD = 5
MESSAGE_MAX_LENGTH = 400
RESTRICTED_REPO_URLS = ["https://gitlab.com/fluidattacks/universe"]

logging.config.dictConfig(LOGGING)

LOGGER = logging.getLogger(__name__)
CLONING_LOGGER = logging.getLogger("cloning")
MACHINE_LOGGER = logging.getLogger("machine")
REBASE_LOGGER = logging.getLogger("rebase")
REFRESH_TOE_LINES_LOGGER = logging.getLogger("refresh_toe_lines")
SQS_TASKS_LOGGER = logging.getLogger("sqs_tasks")


def format_excluded_sub_paths(
    excluded_sub_paths: list[str] | None,
) -> list[str] | None:
    if excluded_sub_paths is None:
        return None

    def _format_sub_path(sub_path: str) -> str:
        formatted_sub_path = sub_path.strip()
        while formatted_sub_path.startswith("/"):
            formatted_sub_path = formatted_sub_path.removeprefix("/")
        while formatted_sub_path.endswith("/"):
            formatted_sub_path = formatted_sub_path.removesuffix("/")
        return formatted_sub_path

    return sorted(set(map(_format_sub_path, excluded_sub_paths)))


def format_url(url: str) -> str:
    try:
        parsed = urlparse(url)
        formatted_url = urlunparse(parsed)
        if formatted_url.endswith("/"):
            formatted_url = formatted_url.rstrip("/")

        return formatted_url

    except ValueError:
        return url


def format_git_repo_url(raw_url: str) -> str:
    """
    Returns a formatted url without authentication info.

    Raises:
        InvalidProtocol: If the url has a protocol different to HTTP(S),
        SSH, or CODECOMMIT.
        InvalidUrl: If the hostname is invalid.
        InvalidPort: It the port is invalid.

    Returns:
        str: Formatted url.

    """
    quoted_url = quote_url(raw_url.strip())
    parser: Callable[[str], Url] | None = None

    if quoted_url.startswith("codecommit"):
        parser = codecommitparser
    if quoted_url.startswith("ssh"):
        parser = sshparser
    if quoted_url.startswith("http"):
        parser = httpsparser

    if parser is None:
        raise InvalidProtocol("-")

    parsed_url = parser(quoted_url)

    if parsed_url.scheme in ["http", "https"]:
        parsed_url = parsed_url._replace(auth=None)

    return str(parsed_url).rstrip(" /")


def quote_url(raw_url: str) -> str:
    """Convert whitespaces to `%20` in the URL."""
    return raw_url.replace(" ", "%20")


def unquote_url(raw_url: str) -> str:
    """Decode every %xx escapes to their single-character equivalent."""
    return unquote(raw_url)


def format_root_nickname(nickname: str, url: str) -> str:
    nick: str = nickname if nickname else _get_nickname_from_url(url)
    # Return the repo name as nickname
    if nick.endswith("_git"):
        return nick[:-4]
    return nick


def format_reapeted_nickname(nickname: str) -> str:
    uuid = str(uuid4())[:4]
    return f"{nickname}_{uuid}"


def ensure_unique_nickname(
    nickname: str,
    roots: list[Root],
    url: str,
    old_nickname: str = "",
) -> str:
    if not validations.is_nickname_unique(
        nickname=nickname,
        roots=roots,
        url=url,
        old_nickname=old_nickname,
    ):
        nickname = format_reapeted_nickname(nickname=nickname)

    return nickname


def get_root_by_nickname(roots: list[Root], nickname: str) -> GitRoot | None:
    for root in roots:
        if isinstance(root, GitRoot) and root.state.nickname == nickname:
            return root
    return None


async def update_inactive_root_nickname(
    loaders: Dataloaders,
    user_email: str,
    nickname: str,
    root_with_nickname: GitRoot,
) -> None:
    nickname_for_inactive = format_reapeted_nickname(nickname=nickname)
    new_state_inactive = root_with_nickname.state._replace(nickname=nickname_for_inactive)
    LOGGER.info(
        "Updating inactive root nickname",
        extra={
            "extra": {
                "root_id": root_with_nickname.id,
                "old_nickname": root_with_nickname.state.nickname,
                "new_nickname": new_state_inactive.nickname,
                "user_email": user_email,
            },
        },
    )
    await roots_model.update_root_state(
        current_value=root_with_nickname.state,
        group_name=root_with_nickname.group_name,
        root_id=root_with_nickname.id,
        state=new_state_inactive,
    )

    schedule(
        send_mail_updated_root(
            loaders=loaders,
            group_name=root_with_nickname.group_name,
            root=root_with_nickname,
            new_state=new_state_inactive,
            user_email=user_email,
        ),
    )


async def ensure_unique_nickname_for_update(
    *,
    loaders: Dataloaders,
    user_email: str,
    nickname: str,
    roots: list[Root],
    url: str,
    root_id: str,
    old_nickname: str = "",
) -> str:
    if not validations.is_nickname_unique(
        nickname=nickname,
        roots=roots,
        url=url,
        old_nickname=old_nickname,
    ):
        root_with_same_nickname = get_root_by_nickname(roots=roots, nickname=nickname)
        if (
            root_with_same_nickname
            and root_with_same_nickname.state.status == RootStatus.ACTIVE
            and root_with_same_nickname.id != root_id
        ):
            raise RootNicknameUsed(
                f"Nickname already in use for the root: {root_with_same_nickname.state.url}",
            )

        if root_with_same_nickname and root_with_same_nickname.state.status == RootStatus.INACTIVE:
            await update_inactive_root_nickname(
                loaders,
                user_email,
                nickname,
                root_with_same_nickname,
            )

    return nickname


def is_allowed(url: str) -> bool:
    if FI_ENVIRONMENT != "production":
        return True
    return url not in RESTRICTED_REPO_URLS


async def is_in_s3(group_name: str, root_nickname: str) -> bool:
    return bool(
        await s3_operations.list_files(
            f"{group_name}/{root_nickname}.tar.gz",
            bucket=FI_AWS_S3_CONTINUOUS_REPOSITORIES,
        ),
    )


def _format_git_ignore(raw_value: str) -> list[str]:
    return list(entry.strip() for entry in raw_value.split(";")) if raw_value else []


def _deduplicate_roots(
    roots: list[GitRootFileInputs],
) -> list[GitRootFileInputs]:
    return list({(root.url, root.branch): root for root in roots}.values())


def _format_row_data(index: int, row: Item) -> GitRootFileInputs:
    try:
        # Connection, priority and nickname are experimental and optional.
        # Enforce them once the announcement have been done.
        root = GitRootFileInputs(
            line_index=index,
            original_row=",".join(row.values()),
            url=row["url"].strip(),
            branch=row["branch"].strip(),
            credential_id=row["credential_id"].strip(),
            git_ignore=_format_git_ignore(row.get("git_ignore", "")),
            includes_health_check=row.get("includes_health_check", "").strip().lower() == "true",
            connection=row.get("connection", "").strip().lower(),
            priority=row.get("priority", "").strip().lower(),
            nickname=row.get("nickname", "").strip(),
        )
    except KeyError as exc:
        if exc.args:
            raise ValueError("missing_headers", f"Missing '{exc.args[0]}' header in file") from exc
        raise ValueError("missing_headers", "Missing headers in file") from exc

    return root


def _validate_url_and_gitignore(
    root: GitRootFileInputs,
    credential_ids: list[str],
) -> None:
    try:
        validation_utils.validate_sanitized_csv_input(root.url, root.branch)
        validation_utils.validate_fields([root.url, root.branch])
        validations.validate_url_and_branch(root.url, root.branch)
    except Exception as exc:
        raise ValueError("url", "Invalid url") from exc

    try:
        validation_utils.validate_sanitized_csv_input(root.credential_id)
        validation_utils.validate_fields([root.credential_id])
        validation_utils.validate_uuid4(root.credential_id)
        _is_valid_credential_id(root.credential_id, credential_ids)
    except CredentialNotInOrganization as exc:
        raise ValueError(
            "credential_id",
            "Credentials does not exist",
        ) from exc
    except Exception as exc:
        raise ValueError("credential_id", "Invalid credentials id") from exc

    try:
        validation_utils.validate_sanitized_csv_input(*root.git_ignore)
        validation_utils.validate_fields([*root.git_ignore])
        if not validations.is_exclude_valid(root.git_ignore, root.url):
            raise ValueError

        validations.validate_exclusions(root.git_ignore)
    except Exception as exc:
        raise ValueError("git_ignore", "Invalid gitignore") from exc


def _validate_row_data(
    root: GitRootFileInputs,
    credential_ids: list[str],
) -> None:
    _validate_url_and_gitignore(root, credential_ids)
    try:
        if root.nickname:
            validation_utils.validate_sanitized_csv_input(root.nickname)
            validations.validate_nickname(root.nickname)
    except Exception as exc:
        raise ValueError("nickname", "Invalid nickname") from exc

    if root.connection not in ["", "egress", "ztna"]:
        raise ValueError("connection", "Invalid connection")

    if root.priority.lower() not in ["", "low", "medium", "high"]:
        raise ValueError("priority", "Invalid priority")


def _is_valid_credential_id(credential_id: str, org_credentials: list[str]) -> bool:
    if credential_id not in org_credentials:
        raise CredentialNotInOrganization(credential_id)
    return True


async def get_unique_roots(
    group_name: str,
    loaders: Dataloaders,
    roots: list[GitRootFileInputs],
    organization_id: str,
) -> list[GitRootFileInputs]:
    organization = await orgs_utils.get_organization(loaders, organization_id)
    organization_roots = await loaders.organization_roots.load(
        organization.name,
    )
    roots = _deduplicate_roots(roots)
    return list(
        root
        for root in roots
        if validations.is_git_unique(
            url=root.url,
            branch=root.branch,
            group_name=group_name,
            roots=organization_roots,
        )
    )


@validation_deco_utils.validate_file_name_deco("file.filename")
@validation_deco_utils.validate_fields_deco(["file.content_type"])
@validation_deco_utils.validate_sanitized_csv_input_deco(["file.filename", "file.content_type"])
async def read_file_content(
    file: UploadFile,
    credential_ids: list[str],
) -> tuple[list[GitRootFileInputs], list[InvalidCSVFormat]]:
    if not await files_utils.assert_uploaded_file_mime(file, ["text/csv", "text/plain"]):
        raise InvalidFileType()

    data_bytes = await file.read()
    await file.seek(0)
    return await in_thread(_format_csv_file, data_bytes, credential_ids)


def _get_field_index(fields: Sequence[str], field: str) -> int:
    try:
        return fields.index(field)
    except ValueError:
        return 0


def _format_csv_file(
    file_content: bytes,
    credential_ids: list[str],
) -> tuple[list[GitRootFileInputs], list[InvalidCSVFormat]]:
    roots = []
    errors = []
    file_content_str = io.StringIO(file_content.decode("utf-8"))
    dialect = csv.Sniffer().sniff(file_content_str.getvalue())
    csv_reader = csv.DictReader(
        file_content_str,
        skipinitialspace=True,
        dialect=dialect,
    )
    fields = csv_reader.fieldnames
    max_rows_allowed: list[dict[str, str]] = list(
        itertools.islice(csv_reader, 1000),
    )
    if not max_rows_allowed or not fields:
        raise EmptyFile()

    for index, row in enumerate(max_rows_allowed, start=1):
        try:
            root = _format_row_data(index, row)
            _validate_row_data(root, credential_ids)
            roots.append(root)
        except ValueError as exc:
            errors.append(
                InvalidCSVFormat(
                    field=exc.args[0],
                    field_index=_get_field_index(fields, exc.args[0]),
                    line=",".join(row.values()),
                    line_index=index,
                    msg=exc.args[1] if len(exc.args) > 1 else "Invalid field",
                ),
            )

    return roots, errors


def _get_nickname_from_url(url: str) -> str:
    url_attributes = urlparse(url)
    if not url_attributes.path:
        last_path: str = urlparse(url).netloc.split(":")[-1]
    else:
        last_path = urlparse(url).path.split("/")[-1]

    return re.sub(r"(?![a-zA-Z_0-9-]).", "_", last_path[:128])


def format_root_credential_new(
    credentials: dict[str, str],
    organization_id: str,
    user_email: str,
) -> Credentials:
    credential_name = credentials["name"]
    credential_type = CredentialType(credentials["type"])
    is_pat: bool = bool(credentials.get("is_pat", False))

    if not credential_name:
        raise InvalidParameter()
    if is_pat:
        if "azure_organization" not in credentials:
            raise InvalidParameter("azure_organization")
        validation_utils.validate_space_field(credentials["azure_organization"])
    if not is_pat and "azure_organization" in credentials:
        raise InvalidParameter("azure_organization")

    secret = orgs_utils.format_credentials_secret_type(credentials)

    return Credentials(
        id=str(uuid4()),
        organization_id=organization_id,
        state=CredentialsState(
            modified_by=user_email,
            modified_date=datetime_utils.get_utc_now(),
            name=credentials["name"],
            type=credential_type,
            is_pat=is_pat,
            azure_organization=credentials["azure_organization"] if is_pat else None,
            owner=user_email,
        ),
        secret=secret,
    )


async def send_mail_root_cloning_status(
    *,
    loaders: Dataloaders,
    root: GitRoot,
    modified_by: str,
    modified_date: datetime,
    is_failed: bool,
) -> None:
    users_email = await mailer_utils.get_group_emails_by_notification(
        loaders=loaders,
        group_name=root.group_name,
        notification="root_cloning_status",
    )
    await groups_mail.send_mail_root_cloning_status(
        loaders=loaders,
        email_to=users_email,
        group_name=root.group_name,
        last_successful_cloning=root.cloning.last_successful_cloning,
        root_creation_date=root.created_date,
        root_nickname=root.state.nickname,
        root_id=root.id,
        report_date=modified_date,
        modified_by=modified_by,
        is_failed=is_failed,
    )


async def send_mail_root_cloning_failed(
    *,
    loaders: Dataloaders,
    group_name: str,
    modified_date: datetime,
    root: Root,
    status: RootCloningStatus,
) -> None:
    if not isinstance(root, GitRoot):
        raise InvalidParameter()

    has_failed_continuously = (
        status == RootCloningStatus.FAILED
        and root.cloning.status == RootCloningStatus.FAILED
        and root.cloning.failed_count == 2
    )
    is_cloning = status == RootCloningStatus.OK and root.cloning.status == RootCloningStatus.FAILED

    loaders.group.clear(group_name)
    group = await loaders.group.load(group_name)
    is_cloning_or_failed = is_cloning or has_failed_continuously
    if (
        group
        and not root.state.use_vpn
        and not root.state.use_ztna
        and group.state.status == GroupStateStatus.ACTIVE
        and is_cloning_or_failed
    ):
        await send_mail_root_cloning_status(
            loaders=loaders,
            root=root,
            modified_by=root.state.modified_by,
            modified_date=modified_date,
            is_failed=has_failed_continuously,
        )


async def send_mail_environment(
    *,
    loaders: Dataloaders,
    modified_date: datetime,
    group_name: str,
    git_root: str,
    git_root_url: str,
    urls_added: list[str],
    urls_deleted: list[str],
    user_email: str,
    other: str | None = None,
    reason: str | None = None,
) -> None:
    users_email = await mailer_utils.get_group_emails_by_notification(
        loaders=loaders,
        group_name=group_name,
        notification="environment_report",
    )
    await groups_mail.send_mail_environment_report(
        loaders=loaders,
        email_to=users_email,
        group_name=group_name,
        responsible=user_email,
        git_root=git_root,
        git_root_url=git_root_url,
        urls_added=urls_added,
        urls_deleted=urls_deleted,
        modified_date=modified_date,
        other=other,
        reason=reason,
    )


@retry_on_exceptions(
    exceptions=(UnableToSendMail, MailerClientError),
    max_attempts=4,
    sleep_seconds=2,
)
async def send_mail_updated_root(
    *,
    loaders: Dataloaders,
    group_name: str,
    root: Root,
    new_state: GitRootState | IPRootState | URLRootState,
    user_email: str,
) -> None:
    users_email = await mailer_utils.get_group_emails_by_notification(
        loaders=loaders,
        group_name=group_name,
        notification="updated_root",
    )

    old_state: Item = root.state._asdict()
    new_root_content: dict[str, str] = {
        key: value
        for key, value in new_state._asdict().items()
        if old_state[key] != value
        and key not in ["modified_by", "modified_date", "credential_id", "use_vpn"]
    }

    if new_root_content:
        await groups_mail.send_mail_updated_root(
            loaders=loaders,
            email_to=users_email,
            group_name=group_name,
            responsible=user_email,
            root_nickname=new_state.nickname,
            new_root_content=new_root_content,
            old_state=old_state,
            modified_date=new_state.modified_date,
        )


def get_query(root: URLRoot) -> str:
    return "" if root.state.query is None else f"?{root.state.query}"


def get_path(root: URLRoot) -> str:
    return "" if root.state.path == "/" else root.state.path


async def ls_remote_root(
    root: GitRoot,
    cred: Credentials | None,
    loaders: Dataloaders,
    follow_redirects: bool = False,
) -> tuple[str | None, str | None]:
    last_commit: str | None = None
    err_message: str | None = None
    repo_url = root.state.url
    repo_branch = root.state.branch
    if cred is None:
        if repo_url.startswith("http"):
            last_commit, err_message = await ls_remote(
                repo_url=repo_url,
                repo_branch=repo_branch,
                follow_redirects=follow_redirects,
            )
    elif isinstance(cred.secret, SshSecret):
        last_commit, err_message = await ls_remote(
            repo_url=repo_url,
            credential_key=cred.secret.key,
            repo_branch=repo_branch,
        )
    elif isinstance(cred.secret, HttpsSecret):
        last_commit, err_message = await ls_remote(
            repo_url=repo_url,
            user=cred.secret.user,
            password=cred.secret.password,
            repo_branch=repo_branch,
            follow_redirects=follow_redirects,
        )
    elif isinstance(cred.secret, HttpsPatSecret):
        last_commit, err_message = await ls_remote(
            repo_url=repo_url,
            token=cred.secret.token,
            repo_branch=repo_branch,
            is_pat=cred.state.is_pat,
            follow_redirects=follow_redirects,
        )
    elif isinstance(
        cred.secret,
        (
            OauthGithubSecret,
            OauthAzureSecret,
            OauthGitlabSecret,
            OauthBitbucketSecret,
        ),
    ):
        last_commit, err_message = await get_last_commit_info(
            root=root, cred=cred, loaders=loaders, follow_redirects=follow_redirects
        )
    elif isinstance(cred.secret, AWSRoleSecret):
        org = await loaders.organization.load(root.organization_name)
        quoted_url = quote_url(repo_url.strip())
        parsed_url = codecommitparser(quoted_url)
        last_commit, err_message = await ls_remote(
            repo_url=str(parsed_url).rstrip(" /"),
            arn=cred.secret.arn,
            repo_branch=repo_branch,
            org_external_id=org.state.aws_external_id if org else None,
            follow_redirects=follow_redirects,
        )
    else:
        raise InvalidParameter()

    return last_commit, err_message


async def queue_sync_roots_batch(
    *,
    group_name: str,
    git_root_ids: list[str],
    modified_by: str,
    should_queue_machine: bool = True,
    should_queue_sbom: bool = True,
) -> PutActionResult:
    queued_execution = PutActionResult(success=False)
    for roots_chunk in chunked(git_root_ids, MAX_ROOTS_PER_JOB):
        queued_execution = await batch_dal.put_action(
            action=Action.CLONE_ROOTS,
            attempt_duration_seconds=14400,
            entity=group_name,
            subject=modified_by,
            additional_info={
                "git_root_ids": roots_chunk,
                "should_queue_machine": should_queue_machine,
                "should_queue_sbom": should_queue_sbom,
            },
            queue=IntegratesBatchQueue.SMALL,
        )

    return queued_execution


async def queue_sync_root_cluster(
    *,
    group_name: str,
    git_root_id: str,
    modified_by: str,
    should_queue_machine: bool = True,
    should_queue_sbom: bool = True,
) -> None:
    try:
        sqs_client = await get_client()
        await sqs_client.send_message(
            QueueUrl=("https://sqs.us-east-1.amazonaws.com/205810638802/integrates_clone"),
            MessageBody=json.dumps(
                {
                    "id": f"{group_name}_{git_root_id}",
                    "task": "clone",
                    "args": [
                        group_name,
                        git_root_id,
                        should_queue_machine,
                        modified_by,
                        should_queue_sbom,
                    ],
                },
            ),
        )
        SQS_TASKS_LOGGER.info(
            "Enqueue the task",
            extra={
                "extra": {
                    "group_name": group_name,
                    "git_root_id": git_root_id,
                    "should_queue_machine": should_queue_machine,
                    "status": "QUEUED",
                    "task_name": "clone",
                    "user_email": modified_by,
                },
            },
        )
    except ClientError as exc:
        SQS_TASKS_LOGGER.error(
            "Attempted to enqueue the task",
            exc_info=exc,
            extra={
                "extra": {
                    "group_name": group_name,
                    "git_root_id": git_root_id,
                    "should_queue_machine": should_queue_machine,
                    "status": "FAILED",
                    "task_name": "clone",
                    "user_email": modified_by,
                },
            },
        )


@retry_on_exceptions(
    exceptions=(UnavailabilityError, ErrorUpdatingRootItem),
    max_attempts=3,
    sleep_seconds=1,
)
async def update_root_cloning_status(
    *,
    loaders: Dataloaders,
    group_name: str,
    root_id: str,
    status: RootCloningStatus,
    message: str,
    modified_by: str,
    commit: str | None = None,
    commit_date: datetime | None = None,
) -> None:
    root = await roots_utils.get_root(loaders, root_id, group_name, clear_loader=True)
    if not isinstance(root, GitRoot):
        raise InvalidParameter()

    modified_date = datetime_utils.get_utc_now()
    truncated_message = roots_utils.sanitize_cloning_error_message(message)[:MESSAGE_MAX_LENGTH]
    new_state = root.cloning._replace(
        modified_date=modified_date,
        reason=truncated_message,
        status=status,
        modified_by=modified_by,
    )
    if status == RootCloningStatus.OK:
        new_state = new_state._replace(
            commit=commit or root.cloning.commit,
            commit_date=commit_date or root.cloning.commit_date,
            failed_count=0,
            first_successful_cloning=root.cloning.first_successful_cloning or modified_date,
            last_successful_cloning=modified_date,
            successful_count=root.cloning.successful_count + 1,
        )
    elif status == RootCloningStatus.FAILED:
        new_state = new_state._replace(
            failed_count=root.cloning.failed_count + 1,
            first_failed_cloning=root.cloning.first_failed_cloning or modified_date,
            successful_count=0,
        )

    await roots_model.update_git_root_cloning(
        current_value=root.cloning,
        cloning=new_state,
        group_name=group_name,
        root_id=root_id,
    )
    CLONING_LOGGER.info(
        truncated_message,
        extra={
            "extra": {
                "commit_date": new_state.commit_date,
                "commit_sha": new_state.commit,
                "failed_count": new_state.failed_count,
                "first_failed_cloning": new_state.first_failed_cloning,
                "first_successful_cloning": new_state.first_successful_cloning,
                "group_name": group_name,
                "last_successful_cloning": new_state.last_successful_cloning,
                "root_id": root.id,
                "root_nickname": root.state.nickname,
                "status": status,
                "successful_count": new_state.successful_count,
                "user_email": modified_by,
            },
        },
    )
    if validations.validate_error_message(truncated_message):
        await send_mail_root_cloning_failed(
            loaders=loaders,
            group_name=group_name,
            modified_date=modified_date,
            root=root,
            status=status,
        )


def _format_commit_info(
    commit: str | None,
    commit_date: datetime | None,
    current_commit: str | None,
    current_commit_date: datetime | None,
    modified_by: str,
) -> tuple[str | None, datetime | None]:
    if modified_by not in {
        SCHEDULE_CLONE_GROUPS_ROOTS_EMAIL,
        SCHEDULE_MACHINE_QUEUE_SAST_SCA_EMAIL,
    }:
        return current_commit, current_commit_date

    return commit or current_commit, commit_date or current_commit_date


def _format_executed_techniques(
    executed_techniques: set[str] | None,
    last_executions_date: MachineExecutionsDate | None,
    modified_date: datetime,
) -> MachineExecutionsDate | None:
    if not executed_techniques:
        return last_executions_date

    current_executions_date = last_executions_date or MachineExecutionsDate()
    new_executions_date = MachineExecutionsDate(
        apk=modified_date if "apk" in executed_techniques else current_executions_date.apk,
        cspm=modified_date if "cspm" in executed_techniques else current_executions_date.cspm,
        dast=modified_date if "dast" in executed_techniques else current_executions_date.dast,
        sast=modified_date if "sast" in executed_techniques else current_executions_date.sast,
        sca=modified_date if "sca" in executed_techniques else current_executions_date.sca,
    )

    return new_executions_date


@retry_on_exceptions(
    exceptions=(UnavailabilityError, ErrorUpdatingRootItem),
    max_attempts=3,
    sleep_seconds=1,
)
async def update_root_machine_status(
    *,
    group_name: str,
    root_id: str,
    status: RootMachineStatus,
    message: str,
    modified_by: str,
    commit: str | None = None,
    commit_date: datetime | None = None,
    execution_id: str | None = None,
    executed_techniques: set[str] | None = None,
) -> None:
    root = await roots_utils.get_root(get_new_context(), root_id, group_name, clear_loader=True)
    if not isinstance(root, GitRoot):
        raise InvalidParameter()

    modified_date = datetime_utils.get_utc_now()
    truncated_message = message[:MESSAGE_MAX_LENGTH]
    new_state = (
        root.machine._replace(
            execution_id=execution_id,
            modified_by=modified_by,
            modified_date=modified_date,
            reason=truncated_message,
            status=status,
        )
        if root.machine
        else GitRootMachine(
            execution_id=execution_id,
            modified_by=modified_by,
            modified_date=modified_date,
            reason=truncated_message,
            status=status,
        )
    )
    if status == RootMachineStatus.SUCCESS:
        commit, commit_date = _format_commit_info(
            commit,
            commit_date,
            new_state.commit,
            new_state.commit_date,
            modified_by,
        )
        last_executions_date = _format_executed_techniques(
            executed_techniques=executed_techniques,
            last_executions_date=new_state.last_executions_date,
            modified_date=modified_date,
        )
        new_state = new_state._replace(
            commit=commit,
            commit_date=commit_date,
            last_executions_date=last_executions_date,
        )

    await roots_model.update_git_root_machine(
        current_value=root.machine,
        group_name=group_name,
        machine=new_state,
        root_id=root_id,
    )
    MACHINE_LOGGER.info(
        truncated_message,
        extra={
            "extra": {
                "commit_date": new_state.commit_date,
                "commit": new_state.commit,
                "executed_techniques": str(executed_techniques),
                "group_name": group_name,
                "root_id": root.id,
                "root_nickname": root.state.nickname,
                "status": status,
                "user_email": modified_by,
            },
        },
    )


@retry_on_exceptions(
    exceptions=(UnavailabilityError, ErrorUpdatingRootItem),
    max_attempts=3,
    sleep_seconds=1,
)
async def update_root_rebase_status(
    *,
    group_name: str,
    root_id: str,
    status: RootRebaseStatus,
    message: str,
    modified_by: str,
    commit: str | None = None,
    commit_date: datetime | None = None,
) -> None:
    root = await roots_utils.get_root(get_new_context(), root_id, group_name, clear_loader=True)
    if not isinstance(root, GitRoot):
        raise InvalidParameter()

    modified_date = datetime_utils.get_utc_now()
    truncated_message = message[:MESSAGE_MAX_LENGTH]
    new_state = (
        root.rebase._replace(
            modified_by=modified_by,
            modified_date=modified_date,
            reason=truncated_message,
            status=status,
        )
        if root.rebase
        else GitRootRebase(
            modified_by=modified_by,
            modified_date=modified_date,
            reason=truncated_message,
            status=status,
        )
    )
    if status == RootRebaseStatus.SUCCESS:
        new_state = new_state._replace(
            last_successful_rebase=modified_date,
            commit=commit or new_state.commit,
            commit_date=commit_date or new_state.commit_date,
        )

    await roots_model.update_git_root_rebase(
        current_value=root.rebase,
        group_name=group_name,
        rebase=new_state,
        root_id=root_id,
    )
    REBASE_LOGGER.info(
        truncated_message,
        extra={
            "extra": {
                "commit_date": new_state.commit_date,
                "commit": new_state.commit,
                "group_name": group_name,
                "last_successful_rebase": new_state.last_successful_rebase,
                "root_id": root.id,
                "root_nickname": root.state.nickname,
                "status": status,
                "user_email": modified_by,
            },
        },
    )


@retry_on_exceptions(
    exceptions=(UnavailabilityError, ErrorUpdatingRootItem),
    max_attempts=3,
    sleep_seconds=1,
)
async def update_root_toe_lines_status(
    *,
    group_name: str,
    root_id: str,
    status: RootToeLinesStatus,
    message: str,
    modified_by: str,
    commit: str | None = None,
    commit_date: datetime | None = None,
) -> None:
    root = await roots_utils.get_root(get_new_context(), root_id, group_name, clear_loader=True)
    if not isinstance(root, GitRoot):
        raise InvalidParameter()

    modified_date = datetime_utils.get_utc_now()
    truncated_message = message[:MESSAGE_MAX_LENGTH]
    new_state = (
        root.toe_lines._replace(
            modified_by=modified_by,
            modified_date=modified_date,
            reason=truncated_message,
            status=status,
        )
        if root.toe_lines
        else GitRootToeLines(
            modified_by=modified_by,
            modified_date=modified_date,
            reason=truncated_message,
            status=status,
        )
    )
    if status == RootToeLinesStatus.SUCCESS:
        new_state = new_state._replace(
            last_successful_refresh=modified_date,
            commit=commit or new_state.commit,
            commit_date=commit_date or new_state.commit_date,
        )

    await roots_model.update_git_root_toe_lines(
        current_value=root.toe_lines,
        group_name=group_name,
        root_id=root_id,
        toe_lines=new_state,
    )
    REFRESH_TOE_LINES_LOGGER.info(
        truncated_message,
        extra={
            "extra": {
                "commit_date": new_state.commit_date,
                "commit": new_state.commit,
                "group_name": group_name,
                "last_successful_refresh": new_state.last_successful_refresh,
                "root_id": root.id,
                "root_nickname": root.state.nickname,
                "status": status,
                "user_email": modified_by,
            },
        },
    )


def has_new_url_root_state(current_state: URLRootState, new_state: URLRootState) -> bool:
    return (
        new_state._replace(
            modified_by=current_state.modified_by,
            modified_date=current_state.modified_date,
        )
        != current_state
    )


def disgregate_roots_for_queues(
    queue_on_batch: bool,
    roots: list[GitRoot],
) -> tuple[list[str], list[str]]:
    """
    Classify the given roots on those that will be cloned on batch and those
    to be cloned on the k8s cluster.

    If the amount of roots is greater to ROOTS_ON_BATCH_THRESHOLD, it will
    force the flag queue_on_batch, so the machine executions can be
    grouped together in less jobs.

    Args:
        queue_on_batch (bool): Force batch option when possible
        roots (Iterable[GitRoot]): Roots to be separated

    Returns:
        list[str]: The root ids intended for batch
        list[str]: The ids for the cluster

    """
    queue_on_batch = queue_on_batch if len(roots) < ROOTS_ON_BATCH_THRESHOLD else True

    roots_non_legacy_vpn = [root for root in roots if not root.state.use_vpn]
    root_ids_batch = [
        root.id
        for root in roots_non_legacy_vpn
        if queue_on_batch
        or root.state.use_egress
        or root.state.use_ztna
        or root.state.url.startswith("codecommit")
    ]
    root_ids_cluster = list({root.id for root in roots} - set(root_ids_batch))

    return root_ids_batch, root_ids_cluster


def compare_root_criticalities(
    first: RootCriticality,
    comparison: Literal["lt", "gt", "eq"],
    second: RootCriticality,
) -> bool:
    values = list(RootCriticality.__members__.values())
    match comparison:
        case "lt":
            result = values.index(first) < values.index(second)
        case "gt":
            result = values.index(first) > values.index(second)
        case _:
            result = values.index(first) == values.index(second)
    return result


async def get_root_docker_image(
    loaders: Dataloaders,
    root_id: str,
    group_name: str,
    uri: str,
) -> RootDockerImage:
    docker_image = await loaders.docker_image.load(
        RootDockerImageRequest(root_id=root_id, group_name=group_name, uri_id=uri),
    )
    if not docker_image or (docker_image.state.status is RootDockerImageStateStatus.DELETED):
        LOGGER.error(
            "Docker Image not found",
            extra={
                "extra": {
                    "root_id": root_id,
                    "uri": uri,
                    "status": docker_image.state.status if docker_image else "not found",
                },
            },
        )
        raise RootDockerImageNotFound()
    return docker_image
