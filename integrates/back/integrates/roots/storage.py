from starlette.datastructures import (
    UploadFile,
)

from integrates.custom_exceptions import (
    InvalidFileName,
)
from integrates.s3 import (
    operations as s3_ops,
)


async def sign_url(file_name: str) -> str:
    return await s3_ops.sign_url(f"reports/{file_name}", 900)


async def upload_file(file: UploadFile) -> str:
    if not file.filename:
        raise InvalidFileName()
    await s3_ops.upload_memory_file(
        file,
        f"reports/{file.filename}",
    )
    return file.filename
