from integrates import roots
from integrates.custom_exceptions import InvalidField, InvalidRootType, RepeatedRootEnvironmentUrl
from integrates.dataloaders import get_new_context
from integrates.db_model.roots.enums import RootEnvironmentCloud, RootEnvironmentUrlStateStatus
from integrates.db_model.roots.types import (
    RootEnvironmentSecretsRequest,
    RootEnvironmentUrlsRequest,
)
from integrates.roots.environments import add_root_environment_cspm, add_root_environment_url
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    GitRootFaker,
    GitRootStateFaker,
    GroupFaker,
    IPRootFaker,
    OrganizationFaker,
    OrganizationStateFaker,
    RootEnvironmentUrlFaker,
    RootEnvironmentUrlStateFaker,
    StakeholderFaker,
)
from integrates.testing.mocks import Mock, mocks
from integrates.testing.utils import parametrize, raises

EMAIL_TEST = "admin@fluidattacks.com"
GROUP_NAME = "group1"
ROOT_ID = "root1"


@parametrize(
    args=["should_notify", "is_production"],
    cases=[[True, True], [False, False]],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            stakeholders=[StakeholderFaker(email=EMAIL_TEST, role="admin")],
            groups=[GroupFaker(name=GROUP_NAME, organization_id="org1")],
            roots=[
                GitRootFaker(
                    group_name=GROUP_NAME,
                    id=ROOT_ID,
                    state=GitRootStateFaker(
                        nickname="back",
                    ),
                ),
            ],
        )
    ),
    others=[Mock(roots.utils, "send_mail_environment", "async", None)],
)
async def test_add_root_environment_url(should_notify: bool, is_production: bool) -> None:
    result = await add_root_environment_url(
        loaders=get_new_context(),
        group_name=GROUP_NAME,
        root_id=ROOT_ID,
        url="https://nice-env-test.com",
        url_type="URL",
        user_email=EMAIL_TEST,
        should_notify=should_notify,
        use_connection={
            "use_egress": None,
            "use_vpn": None,
            "use_ztna": None,
        },
        is_production=is_production,
    )
    assert result
    root_env_urls = await get_new_context().root_environment_urls.load(
        RootEnvironmentUrlsRequest(root_id=ROOT_ID, group_name=GROUP_NAME),
    )
    url_ids = [env_url.id for env_url in root_env_urls]
    assert len(url_ids) == 1


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            stakeholders=[StakeholderFaker(email=EMAIL_TEST, role="admin")],
            groups=[GroupFaker(name=GROUP_NAME, organization_id="org1")],
            roots=[
                GitRootFaker(
                    group_name=GROUP_NAME,
                    id=ROOT_ID,
                    state=GitRootStateFaker(
                        nickname="back",
                    ),
                ),
            ],
        )
    )
)
async def test_add_root_environment_url_failed_due_to_cspm() -> None:
    loaders = get_new_context()
    with raises(InvalidField):
        await add_root_environment_url(
            loaders=loaders,
            group_name=GROUP_NAME,
            root_id=ROOT_ID,
            url="https://nice-env-test.com",
            url_type="URL",
            user_email=EMAIL_TEST,
            should_notify=True,
            cloud_type=RootEnvironmentCloud.AWS,
            use_connection={},
            is_moving_environment=False,
            is_production=True,
        )


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            stakeholders=[StakeholderFaker(email=EMAIL_TEST, role="admin")],
            groups=[GroupFaker(name=GROUP_NAME, organization_id="org1")],
            roots=[
                IPRootFaker(
                    group_name=GROUP_NAME,
                    id=ROOT_ID,
                ),
            ],
        )
    )
)
async def test_add_root_environment_url_failed_due_to_root_type() -> None:
    loaders = get_new_context()
    with raises(InvalidRootType):
        await add_root_environment_url(
            loaders=loaders,
            group_name=GROUP_NAME,
            root_id=ROOT_ID,
            url="https://nice-env-test.com",
            url_type="URL",
            user_email=EMAIL_TEST,
            should_notify=True,
            use_connection={},
            is_moving_environment=True,
            is_production=True,
        )


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            stakeholders=[StakeholderFaker(email=EMAIL_TEST, role="admin")],
            groups=[GroupFaker(name=GROUP_NAME, organization_id="org1")],
            roots=[
                GitRootFaker(
                    group_name=GROUP_NAME,
                    id=ROOT_ID,
                    state=GitRootStateFaker(nickname="back"),
                ),
            ],
            root_environment_urls=[
                RootEnvironmentUrlFaker(
                    url="https://nice-env-test.com",
                    group_name="group1",
                    root_id="root1",
                    state=RootEnvironmentUrlStateFaker(modified_by="hacker@fluidattacks.com"),
                ),
            ],
        )
    )
)
async def test_add_root_environment_url_failed_due_to_existing_env() -> None:
    loaders = get_new_context()
    with raises(RepeatedRootEnvironmentUrl):
        await add_root_environment_url(
            loaders=loaders,
            group_name=GROUP_NAME,
            root_id=ROOT_ID,
            url="https://nice-env-test.com",
            url_type="URL",
            user_email=EMAIL_TEST,
            should_notify=True,
            use_connection={
                "use_egress": None,
                "use_vpn": None,
                "use_ztna": None,
            },
            is_production=False,
        )


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            stakeholders=[StakeholderFaker(email=EMAIL_TEST, role="admin")],
            groups=[GroupFaker(name=GROUP_NAME, organization_id="org1")],
            roots=[
                GitRootFaker(
                    group_name=GROUP_NAME,
                    id=ROOT_ID,
                    state=GitRootStateFaker(nickname="back"),
                ),
            ],
            root_environment_urls=[
                RootEnvironmentUrlFaker(
                    url="https://nice-env-test.com",
                    group_name=GROUP_NAME,
                    root_id=ROOT_ID,
                    state=RootEnvironmentUrlStateFaker(
                        modified_by="hacker@fluidattacks.com",
                        status=RootEnvironmentUrlStateStatus.DELETED,
                    ),
                ),
            ],
        )
    )
)
async def test_add_root_environment_url_deleted_env() -> None:
    """Adding an environment equal to an existing one in a deleted state"""
    result = await add_root_environment_url(
        loaders=get_new_context(),
        group_name=GROUP_NAME,
        root_id=ROOT_ID,
        url="https://nice-env-test.com",
        url_type="URL",
        user_email=EMAIL_TEST,
        should_notify=True,
        use_connection={
            "use_egress": None,
            "use_vpn": None,
            "use_ztna": None,
        },
        is_production=False,
    )
    assert result
    root_env_urls = await get_new_context().root_environment_urls.load(
        RootEnvironmentUrlsRequest(root_id=ROOT_ID, group_name=GROUP_NAME),
    )
    url_ids = [
        env_url.id
        for env_url in root_env_urls
        if env_url.state.status == RootEnvironmentUrlStateStatus.CREATED
    ]
    assert len(url_ids) == 1


@parametrize(args=["cloud_type"], cases=[["AZURE"], ["GCP"]])
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(
                    id="org1",
                    state=OrganizationStateFaker(
                        aws_external_id="be991c12-9b68-4312-85c4-16ce5fa2fca0",
                    ),
                ),
            ],
            groups=[GroupFaker(name="group1", organization_id="org1")],
            roots=[
                GitRootFaker(
                    group_name="group1",
                    id="root1",
                    state=GitRootStateFaker(nickname="testroot"),
                ),
            ],
        )
    ),
    others=[Mock(roots.environments, "validate_role_status", "async", True)],
)
async def test_add_root_environment_cspm(cloud_type: str) -> None:
    loaders = get_new_context()
    result = await add_root_environment_cspm(
        loaders=loaders,
        group_name="group1",
        root_id="root1",
        url="632589123658",
        user_email="admin@gmail.com",
        cloud_type=cloud_type,
        secrets={
            "AZURE_CLIENT_ID": "azure_client_id_1",
            "AZURE_CLIENT_SECRET": "azure_client_secret_1",
            "AZURE_TENANT_ID": "azure_tenant_id_1",
            "AZURE_SUBSCRIPTION_ID": "azure_subscription_id_1",
            "GCP_PRIVATE_KEY": "gcp_private_key_1",
        },
    )

    assert result
    env_urls = [
        env_url
        for env_url in await loaders.root_environment_urls.load(
            RootEnvironmentUrlsRequest(root_id="root1", group_name="group1"),
        )
        if env_url.state.include
        and env_url.state.cloud_name
        and env_url.state.cloud_name.value == cloud_type
    ]
    assert len(env_urls) == 1
    root_env_url = env_urls[0]
    env_secrets = await loaders.environment_secrets.load(
        RootEnvironmentSecretsRequest(url_id=root_env_url.id, group_name=root_env_url.group_name),
    )
    assert len(env_secrets) == 4 if cloud_type == "AZURE" else 1
