from integrates.batch.dal.get import get_actions
from integrates.batch.enums import Action
from integrates.batch.types import PutActionResult
from integrates.custom_exceptions import (
    CustomBaseException,
    FileNotFound,
    GroupNotFound,
    InvalidChar,
    InvalidGroupService,
    InvalidParameter,
    InvalidRoleSetGitIgnore,
    InvalidRootType,
    RepeatedRoot,
    RepeatedRootNickname,
    RequiredCredentials,
    ResourceTypeNotFound,
    RootAlreadyCloning,
)
from integrates.custom_utils.datetime import get_utc_now
from integrates.custom_utils.groups import get_group
from integrates.custom_utils.roots import get_root
from integrates.dataloaders import Dataloaders, get_new_context
from integrates.db_model.credentials.types import HttpsSecret
from integrates.db_model.groups.enums import GroupService, GroupTier
from integrates.db_model.roots.enums import (
    RootCloningStatus,
    RootCriticality,
    RootStateReason,
    RootStatus,
)
from integrates.db_model.roots.types import (
    GitRoot,
    IPRoot,
    RootEnvironmentSecretsRequest,
    RootEnvironmentUrlsRequest,
    RootRequest,
    Secret,
    URLRoot,
)
from integrates.db_model.toe_inputs.types import RootToeInputsRequest
from integrates.db_model.toe_packages.types import RootToePackagesRequest, ToePackageRequest
from integrates.db_model.toe_packages.update import update_package
from integrates.db_model.vulnerabilities.enums import VulnerabilityStateStatus
from integrates.roots import domain as roots_domain
from integrates.roots import utils as roots_utils
from integrates.roots.domain import (
    _validate_add_git_root,
    activate_root,
    add_git_root,
    add_ip_root,
    add_secret,
    add_url_root,
    deactivate_root,
    deactivate_root_toe_package,
    process_git_root,
    queue_sync_git_roots,
    remove_environment_url_id,
    remove_related_resources,
    remove_secret,
)
from integrates.roots.types import (
    GitRootAttributesToAdd,
    IpRootAttributesToAdd,
    UrlRootAttributesToAdd,
)
from integrates.testing.aws import (
    IntegratesAws,
    IntegratesDynamodb,
    RootEnvironmentSecretsToUpdate,
    RootSecretsToUpdate,
)
from integrates.testing.fakers import (
    DATE_2024,
    CredentialsFaker,
    FindingFaker,
    GitRootCloningFaker,
    GitRootFaker,
    GitRootStateFaker,
    GraphQLResolveInfoFaker,
    GroupAccessFaker,
    GroupAccessStateFaker,
    GroupFaker,
    GroupStateFaker,
    IPRootFaker,
    OrganizationFaker,
    RootEnvironmentUrlFaker,
    SecretFaker,
    StakeholderFaker,
    ToeInputFaker,
    ToeInputStateFaker,
    ToePackageCoordinatesFaker,
    ToePackageFaker,
    UrlRootFaker,
    UrlRootStateFaker,
    VulnerabilityFaker,
    VulnerabilityStateFaker,
)
from integrates.testing.mocks import Mock, mocks
from integrates.testing.utils import freeze_time, parametrize, raises
from integrates.tickets import domain as tickets_domain
from integrates.verify.enums import ResourceType

EMAIL_TEST = "admin@fluidattacks.com"
GROUP_NAME = "group1"
ROOT_ID = "root1"


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            stakeholders=[StakeholderFaker(email=EMAIL_TEST, role="admin")],
            groups=[GroupFaker(name=GROUP_NAME, organization_id="org1")],
        ),
    ),
)
async def test_add_ip_root() -> None:
    result = await add_ip_root(
        loaders=get_new_context(),
        user_email=EMAIL_TEST,
        group_name=GROUP_NAME,
        attributes_to_add=IpRootAttributesToAdd(
            address="192.158.1.38",
            nickname="test-nickname-2",
        ),
    )

    loaders = get_new_context()
    root = await loaders.root.load(RootRequest(GROUP_NAME, result.id))
    assert isinstance(root, IPRoot)
    assert root.state.nickname == "test-nickname-2"
    assert root.state.address == "192.158.1.38"


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            stakeholders=[StakeholderFaker(email=EMAIL_TEST, role="admin")],
            groups=[GroupFaker(name=GROUP_NAME, organization_id="org1")],
            roots=[
                GitRootFaker(
                    group_name=GROUP_NAME,
                    id=ROOT_ID,
                    state=GitRootStateFaker(
                        nickname="back",
                    ),
                ),
            ],
        ),
    ),
    others=[Mock(tickets_domain, "new_group", "async", None)],
)
async def test_add_git_root() -> None:
    loaders: Dataloaders = get_new_context()
    group = await get_group(loaders, GROUP_NAME)
    branch = "trunk"
    url = "https://gitlab.com/fluidattacks/universe.git"
    credentials: dict = {
        "isPat": False,
        "token": "token",
        "name": "Credentials test",
        "type": "HTTPS",
    }

    result = await add_git_root(
        loaders=loaders,
        attributes_to_add=GitRootAttributesToAdd(
            branch=branch,
            credentials=credentials,
            gitignore=[],
            includes_health_check=False,
            url=url,
            nickname="nickname",
        ),
        user_email=EMAIL_TEST,
        group=group,
    )

    root = await loaders.root.load(RootRequest(GROUP_NAME, result.id))

    assert isinstance(root, GitRoot)
    assert root.state.branch == branch
    assert root.state.criticality == RootCriticality.LOW
    assert root.state.url == url
    assert root.cloning.status.value == "UNKNOWN"
    assert root.cloning.reason == "ROOT_CREATED"


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            stakeholders=[StakeholderFaker(email=EMAIL_TEST, role="admin")],
            groups=[
                GroupFaker(
                    name=GROUP_NAME,
                    organization_id="org1",
                    state=GroupStateFaker(has_advanced=False),
                )
            ],
        ),
    )
)
async def test_validate_add_git_root_invalid_group_service() -> None:
    loaders = get_new_context()
    group = await get_group(loaders, GROUP_NAME)
    with raises(InvalidGroupService):
        await _validate_add_git_root(
            loaders=loaders,
            branch="trunk",
            ensure_org_uniqueness=True,
            gitignore=[],
            group=group,
            includes_health_check=True,
            nickname="universe",
            url="https://gitlab.com/fluidattacks/universe.git",
            user_email=EMAIL_TEST,
        )


@parametrize(
    args=["url", "branch"],
    cases=[["https://gitlab.com/fluidattacks/test", "trunk fail"], ["testUrl", "trunk"]],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            stakeholders=[StakeholderFaker(email=EMAIL_TEST, role="admin")],
            groups=[
                GroupFaker(
                    name=GROUP_NAME,
                    organization_id="org1",
                    state=GroupStateFaker(has_advanced=False),
                )
            ],
        ),
    )
)
async def test_validate_add_git_root_invalid_parameter(url: str, branch: str) -> None:
    loaders = get_new_context()
    group = await get_group(loaders, GROUP_NAME)
    with raises(InvalidParameter):
        await _validate_add_git_root(
            loaders=loaders,
            branch=branch,
            ensure_org_uniqueness=True,
            gitignore=[],
            group=group,
            includes_health_check=True,
            nickname="universe",
            url=url,
            user_email=EMAIL_TEST,
        )


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            stakeholders=[StakeholderFaker(email=EMAIL_TEST, role="admin")],
            groups=[GroupFaker(name=GROUP_NAME, organization_id="org1")],
        ),
    )
)
async def test_process_git_root_required_credentials() -> None:
    loaders = get_new_context()
    group = await get_group(loaders, GROUP_NAME)
    with raises(RequiredCredentials):
        await add_git_root(
            loaders=loaders,
            attributes_to_add=GitRootAttributesToAdd(
                branch="trunk",
                credentials={},
                gitignore=[],
                includes_health_check=False,
                url="https://gitlab.com/fluidattacks/universe.git",
                nickname="nickname",
            ),
            user_email=EMAIL_TEST,
            group=group,
            required_credentials=True,
        )


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            stakeholders=[StakeholderFaker(email=EMAIL_TEST, role="admin")],
            groups=[
                GroupFaker(
                    name=GROUP_NAME,
                    organization_id="org1",
                    state=GroupStateFaker(has_advanced=False),
                )
            ],
            roots=[
                GitRootFaker(
                    group_name=GROUP_NAME, id=ROOT_ID, state=GitRootStateFaker(nickname="back")
                )
            ],
        ),
    )
)
async def test_add_git_root_repeated_root() -> None:
    loaders = get_new_context()
    group = await get_group(loaders, GROUP_NAME)
    with raises(RepeatedRoot):
        await add_git_root(
            loaders=loaders,
            attributes_to_add=GitRootAttributesToAdd(
                branch="master",
                credentials={"token": "token", "name": "Credentials test", "type": "HTTPS"},
                gitignore=[],
                includes_health_check=False,
                url="https://gitlab.com/fluidattacks/nickname1",
                nickname="back",
            ),
            user_email=EMAIL_TEST,
            group=group,
        )


@parametrize(
    args=["url", "branch", "gitignore"],
    cases=[
        ["https://gitlab.com/fluidattacks", "trunk", ["back\\test.py", "bower_components/*"]],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            stakeholders=[StakeholderFaker(email=EMAIL_TEST, role="hacker")],
            groups=[GroupFaker(name=GROUP_NAME, organization_id="org1")],
        )
    )
)
async def test_add_git_root_invalid_char_gitignore(
    url: str, branch: str, gitignore: list[str]
) -> None:
    loaders = get_new_context()
    group = await get_group(loaders, GROUP_NAME)
    with raises(InvalidChar):
        await add_git_root(
            loaders=loaders,
            attributes_to_add=GitRootAttributesToAdd(
                branch=branch,
                credentials={},
                gitignore=gitignore,
                includes_health_check=False,
                url=url,
                nickname="nickname",
            ),
            user_email=EMAIL_TEST,
            group=group,
        )


@parametrize(
    args=["url", "branch", "gitignore"],
    cases=[
        ["https://gitlab.com/fluidattacks/universe.git", "trunk", ["**/test.py"]],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            stakeholders=[StakeholderFaker(email=EMAIL_TEST, role="hacker")],
            groups=[GroupFaker(name=GROUP_NAME, organization_id="org1")],
            group_access=[
                GroupAccessFaker(
                    email=EMAIL_TEST, state=GroupAccessStateFaker(has_access=True, role="user")
                )
            ],
        )
    )
)
async def test_add_git_root_invalid_role_set_gitignore(
    url: str, branch: str, gitignore: list[str]
) -> None:
    loaders = get_new_context()
    group = await get_group(loaders, GROUP_NAME)
    with raises(InvalidRoleSetGitIgnore):
        await add_git_root(
            loaders=loaders,
            attributes_to_add=GitRootAttributesToAdd(
                branch=branch,
                credentials={},
                gitignore=gitignore,
                includes_health_check=False,
                url=url,
                nickname="nickname",
            ),
            user_email=EMAIL_TEST,
            group=group,
        )


@freeze_time(DATE_2024.isoformat())
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            stakeholders=[StakeholderFaker(email="adming@fluidattacks.com", role="admin")],
            groups=[GroupFaker(name=GROUP_NAME, organization_id="org1")],
            credentials=[
                CredentialsFaker(
                    credential_id="cred1",
                    organization_id="ORG#org1",
                    secret=HttpsSecret(user="user@test.test", password=""),
                )
            ],
        ),
    ),
    others=[
        Mock(roots_domain, "report_activity", "sync", None),
        Mock(roots_domain, "notify_health_check", "async", None),
    ],
)
async def test_add_git_root_queue() -> None:
    loaders = get_new_context()
    group = await get_group(loaders, GROUP_NAME)
    info = GraphQLResolveInfoFaker(
        user_email="adming@fluidattacks.com",
        decorators=[],
    )
    root_id = await process_git_root(
        info=info,
        attributes_to_add=GitRootAttributesToAdd(
            branch="trunk",
            credentials={"id": "cred1"},
            gitignore=[],
            includes_health_check=True,
            url="https://gitlab.com/fluidattacks/universe.git",
            nickname="",
            use_ztna=True,
        ),
        user_email=EMAIL_TEST,
        group=group,
    )

    loaders = get_new_context()
    root = await loaders.root.load(RootRequest(GROUP_NAME, root_id))
    action = next(
        (action for action in await get_actions() if action.entity == GROUP_NAME),
        None,
    )

    assert isinstance(root, GitRoot)
    assert root.cloning.status.value == "QUEUED"
    assert root.cloning.reason == "Cloning queued..."
    assert root.state.branch == "trunk"
    assert root.state.criticality == RootCriticality.LOW
    assert root.state.url == "https://gitlab.com/fluidattacks/universe.git"
    assert root.cloning.status.value == "QUEUED"
    assert root.cloning.reason == "Cloning queued..."
    assert action is not None
    assert action.subject == "admin@fluidattacks.com"
    assert action.action_name == Action.CLONE_ROOTS
    assert action.additional_info["git_root_ids"] == [root.id]


@freeze_time(DATE_2024.isoformat())
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            stakeholders=[StakeholderFaker(email=EMAIL_TEST, role="admin")],
            groups=[GroupFaker(name=GROUP_NAME, organization_id="org1")],
            roots=[
                GitRootFaker(
                    cloning=GitRootCloningFaker(
                        modified_date=get_utc_now(), status=RootCloningStatus.CLONING
                    ),
                    id="root1",
                    group_name=GROUP_NAME,
                    state=GitRootStateFaker(nickname="back"),
                )
            ],
        )
    )
)
async def test_queue_sync_git_roots_already_cloning() -> None:
    loaders = get_new_context()
    root = await get_root(loaders=loaders, group_name=GROUP_NAME, root_id="root1")
    assert isinstance(root, GitRoot)
    with raises(RootAlreadyCloning):
        await queue_sync_git_roots(
            loaders=loaders,
            roots=(root,),
            group_name=root.group_name,
            force=True,
            modified_by=EMAIL_TEST,
            queue_on_batch=True,
            should_queue_machine=True,
            should_queue_sbom=False,
        )


@freeze_time(DATE_2024.isoformat())
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            stakeholders=[StakeholderFaker(email=EMAIL_TEST, role="admin")],
            groups=[GroupFaker(name=GROUP_NAME, organization_id="org1")],
            roots=[
                GitRootFaker(
                    cloning=GitRootCloningFaker(
                        modified_date=get_utc_now(), status=RootCloningStatus.OK
                    ),
                    id="root1",
                    group_name=GROUP_NAME,
                    state=GitRootStateFaker(
                        nickname="back", url="ssh://git@gitlab.com:fluidattacks/universe.git"
                    ),
                )
            ],
        ),
    )
)
async def test_queue_sync_git_roots_failed() -> None:
    loaders = get_new_context()
    root = await get_root(loaders=loaders, group_name=GROUP_NAME, root_id="root1")
    assert isinstance(root, GitRoot)
    await queue_sync_git_roots(
        loaders=loaders,
        roots=(root,),
        group_name=root.group_name,
        force=True,
        modified_by=EMAIL_TEST,
        queue_on_batch=True,
        should_queue_machine=True,
        should_queue_sbom=False,
    )

    root = await get_root(
        loaders=loaders, group_name=GROUP_NAME, root_id="root1", clear_loader=True
    )
    assert isinstance(root, GitRoot)
    assert root.cloning.status == RootCloningStatus.FAILED
    assert root.cloning.reason == "Invalid credentials"


@freeze_time(DATE_2024.isoformat())
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            stakeholders=[StakeholderFaker(email=EMAIL_TEST, role="user")],
            groups=[GroupFaker(name=GROUP_NAME, organization_id="org1")],
            group_access=[
                GroupAccessFaker(
                    group_name=GROUP_NAME, state=GroupAccessStateFaker(has_access=True, role="user")
                )
            ],
            roots=[
                GitRootFaker(
                    cloning=GitRootCloningFaker(
                        modified_date=get_utc_now(), status=RootCloningStatus.OK
                    ),
                    id="root1",
                    group_name=GROUP_NAME,
                    state=GitRootStateFaker(nickname="back"),
                )
            ],
        )
    ),
    others=[Mock(roots_utils, "ls_remote_root", "async", "")],
)
async def test_queue_sync_git_roots() -> None:
    loaders = get_new_context()
    root = await get_root(loaders=loaders, group_name=GROUP_NAME, root_id="root1")
    assert isinstance(root, GitRoot)
    result = await queue_sync_git_roots(
        loaders=loaders,
        roots=(root,),
        group_name=root.group_name,
        force=True,
        modified_by=EMAIL_TEST,
        queue_on_batch=True,
        should_queue_machine=True,
        should_queue_sbom=False,
    )

    root = await get_root(
        loaders=loaders, group_name=GROUP_NAME, root_id="root1", clear_loader=True
    )
    assert isinstance(root, GitRoot)
    assert isinstance(result, PutActionResult)
    assert result.success
    assert root.cloning.status == RootCloningStatus.QUEUED


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            stakeholders=[StakeholderFaker(email="admin@test.test", role="admin")],
            groups=[
                GroupFaker(
                    name=GROUP_NAME,
                    organization_id="org1",
                    state=GroupStateFaker(service=GroupService.BLACK),
                )
            ],
            roots=[
                UrlRootFaker(
                    root_id="root2",
                    group_name=GROUP_NAME,
                    state=UrlRootStateFaker(status=RootStatus.ACTIVE),
                ),
                UrlRootFaker(
                    root_id="root3",
                    group_name=GROUP_NAME,
                    state=UrlRootStateFaker(status=RootStatus.INACTIVE),
                ),
            ],
        )
    )
)
async def test_activate_root_failed() -> None:
    root = await get_root(loaders=get_new_context(), group_name=GROUP_NAME, root_id="root3")
    with raises(RepeatedRoot):
        await activate_root(
            loaders=get_new_context(), email="admin@test.test", group_name=GROUP_NAME, root=root
        )


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            groups=[
                GroupFaker(
                    name="testgroup",
                    organization_id="org1",
                    state=GroupStateFaker(
                        has_essential=False, service=GroupService.WHITE, tier=GroupTier.OTHER
                    ),
                ),
            ],
            roots=[
                GitRootFaker(
                    id="root1",
                    group_name="testgroup",
                    state=GitRootStateFaker(
                        gitignore=["bower_components/*", "node_modules/*"],
                        includes_health_check=True,
                        status=RootStatus.ACTIVE,
                    ),
                ),
            ],
            stakeholders=[StakeholderFaker(email="admin@test.test", enrolled=True)],
            root_environment_urls=[
                RootEnvironmentUrlFaker(
                    url="https://test.com/",
                    group_name="testgroup",
                    root_id="root1",
                ),
            ],
            group_access=[
                GroupAccessFaker(
                    group_name="testgroup",
                    email="admin@test.test",
                    state=GroupAccessStateFaker(has_access=True, role="user"),
                ),
            ],
            toe_packages=[
                ToePackageFaker(
                    group_name="testgroup",
                    root_id="root1",
                    name="test-package",
                    version="1.5.0",
                    outdated=False,
                    licenses=["Apache-1.0"],
                    locations=[
                        ToePackageCoordinatesFaker(
                            path="var/lib/etc",
                            layer="sha256:d4fc045c9e3a848011de66f34b81f0"
                            "52d4f2c15a17bb196d637e526349601820",
                            image_ref="dummy-image",
                        )
                    ],
                ),
            ],
        )
    )
)
async def test_deactivate_root_git() -> None:
    loaders = get_new_context()
    group_name = "testgroup"
    email = "user@test.test"
    root_id = "root1"

    await deactivate_root(
        loaders=loaders,
        email=email,
        group_name=group_name,
        reason=RootStateReason.REGISTERED_BY_MISTAKE,
        other="custom reason",
        root=await get_root(loaders=loaders, group_name=group_name, root_id=root_id),
    )

    loaders = get_new_context()
    root = await get_root(
        loaders=loaders, group_name=group_name, root_id=root_id, clear_loader=True
    )
    root_env_urls = await loaders.root_environment_urls.load(
        RootEnvironmentUrlsRequest(root_id=root.id, group_name=group_name),
    )
    assert isinstance(root, GitRoot)
    assert root.state.status == RootStatus.INACTIVE
    assert root.state.reason == RootStateReason.REGISTERED_BY_MISTAKE
    assert root.state.other == "custom reason"
    assert len([url for url in root_env_urls if url.state.include]) == 0


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            groups=[
                GroupFaker(
                    name="testgroup",
                    organization_id="org1",
                    state=GroupStateFaker(
                        has_essential=False, service=GroupService.WHITE, tier=GroupTier.OTHER
                    ),
                ),
            ],
            roots=[
                GitRootFaker(
                    id="root1",
                    group_name="testgroup",
                    state=GitRootStateFaker(
                        gitignore=["bower_components/*", "node_modules/*"],
                        includes_health_check=True,
                        status=RootStatus.ACTIVE,
                    ),
                ),
            ],
            stakeholders=[StakeholderFaker(email="admin@test.test", enrolled=True)],
            root_environment_urls=[
                RootEnvironmentUrlFaker(
                    url="https://test.com/",
                    group_name="testgroup",
                    root_id="root1",
                ),
            ],
            group_access=[
                GroupAccessFaker(
                    group_name="testgroup",
                    email="admin@test.test",
                    state=GroupAccessStateFaker(has_access=True, role="user"),
                ),
            ],
            toe_packages=[
                ToePackageFaker(
                    group_name="testgroup",
                    be_present=True,
                    root_id="root1",
                    name="test-package2",
                    version="1.5.0",
                    outdated=False,
                    licenses=["Apache-1.0"],
                    locations=[
                        ToePackageCoordinatesFaker(
                            path="common/config",
                            layer=None,
                            image_ref=None,
                        )
                    ],
                ),
            ],
        )
    )
)
async def test_deactivate_root_toe_package() -> None:
    loaders = get_new_context()
    key = ToePackageRequest(
        group_name="testgroup", root_id="root1", name="test-package2", version="1.5.0"
    )
    package = await loaders.toe_package.load(key)

    assert package
    await update_package(
        current_value=package,
        package=package._replace(be_present=True, modified_date=get_utc_now()),
    )
    await deactivate_root_toe_package(loaders, package)

    loaders = get_new_context()
    root_toe_packages = await loaders.root_toe_packages.load(
        RootToePackagesRequest(root_id="root1", group_name="testgroup")
    )
    assert len([package for package in root_toe_packages.edges if package.node.be_present]) == 0


@parametrize(args=["root_id"], cases=[["root2"], ["root3"]])
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            groups=[
                GroupFaker(
                    name="testgroup",
                    organization_id="org1",
                    state=GroupStateFaker(
                        has_essential=False, service=GroupService.WHITE, tier=GroupTier.OTHER
                    ),
                ),
            ],
            roots=[
                UrlRootFaker(root_id="root2", group_name="testgroup"),
                IPRootFaker(id="root3", group_name="testgroup"),
            ],
            stakeholders=[StakeholderFaker(email="admin@test.test", enrolled=True)],
            group_access=[
                GroupAccessFaker(
                    group_name="testgroup",
                    email="admin@test.test",
                    state=GroupAccessStateFaker(has_access=True, role="user"),
                ),
            ],
        )
    )
)
async def test_deactivate_root(root_id: str) -> None:
    loaders = get_new_context()
    group_name = "testgroup"
    email = "user@test.test"

    await deactivate_root(
        loaders=loaders,
        email=email,
        group_name=group_name,
        reason=RootStateReason.REGISTERED_BY_MISTAKE,
        other="custom reason",
        root=await get_root(loaders=loaders, group_name=group_name, root_id=root_id),
    )

    loaders = get_new_context()
    root = await get_root(
        loaders=loaders, group_name=group_name, root_id=root_id, clear_loader=True
    )

    assert root is not None
    assert root.state.status == RootStatus.INACTIVE
    assert root.state.reason == RootStateReason.REGISTERED_BY_MISTAKE
    assert root.state.other == "custom reason"


@parametrize(
    args=["nickname", "url", "exception"],
    cases=[
        ["back1", "file://app.fluidattacks.com/path/to", FileNotFound()],
        ["back1", "ssh://test3@test.com??", InvalidParameter()],
        ["back1", "mailto:test3@test.com?", InvalidParameter()],
        ["back", "https://test3.com?=test", RepeatedRootNickname()],
        ["back1", "https://app.fluidattacks.com/", RepeatedRoot()],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            stakeholders=[StakeholderFaker(email=EMAIL_TEST, role="admin")],
            groups=[GroupFaker(name=GROUP_NAME, organization_id="org1")],
            roots=[
                UrlRootFaker(
                    group_name=GROUP_NAME,
                    state=UrlRootStateFaker(nickname="back", protocol="HTTPS", path="/"),
                ),
            ],
        ),
    )
)
async def test_add_url_root_exception(
    nickname: str, url: str, exception: CustomBaseException
) -> None:
    loaders = get_new_context()
    with raises(CustomBaseException, match=str(exception)):
        await add_url_root(
            loaders=loaders,
            attributes_to_add=UrlRootAttributesToAdd(nickname=nickname, url=url),
            group_name=GROUP_NAME,
            user_email=EMAIL_TEST,
        )


@parametrize(
    args=["nickname", "url", "query", "path", "host"],
    cases=[
        ["back1", "https://app.fluidattacks.com/", None, "/", "app.fluidattacks.com"],
        ["nickname1", "https://test2.com?test=test2", "test=test2", "/", "test2.com"],
        ["nickname2", "https://test.com?=test#frag", "=test", "/#frag", "test.com"],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            stakeholders=[StakeholderFaker(email=EMAIL_TEST, role="admin")],
            groups=[GroupFaker(name=GROUP_NAME, organization_id="org1")],
        )
    )
)
async def test_add_url_root(
    nickname: str, url: str, query: str | None, path: str, host: str
) -> None:
    loaders = get_new_context()
    url_root = await add_url_root(
        loaders=loaders,
        attributes_to_add=UrlRootAttributesToAdd(nickname=nickname, url=url),
        group_name=GROUP_NAME,
        user_email=EMAIL_TEST,
    )

    root = await get_root(loaders=loaders, group_name=GROUP_NAME, root_id=url_root.id)
    assert isinstance(root, URLRoot)
    assert root.state.status == RootStatus.ACTIVE
    assert root.state.nickname == nickname
    assert root.state.host == host
    assert root.state.path == path
    assert root.state.port == "443"
    assert root.state.protocol == "HTTPS"
    assert root.state.query == query


@parametrize(
    args=["secret", "resource_id", "resource_type"],
    cases=[
        [SecretFaker(), "gitroot1", ResourceType.ROOT],
        [SecretFaker(), "urlroot1", ResourceType.ROOT],
        [SecretFaker(), "iproot1", ResourceType.ROOT],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            groups=[GroupFaker(name=GROUP_NAME)],
            roots=[
                UrlRootFaker(root_id="urlroot1", group_name=GROUP_NAME),
                GitRootFaker(id="gitroot1", group_name=GROUP_NAME),
                IPRootFaker(id="iproot1", group_name=GROUP_NAME),
            ],
        )
    )
)
async def test_add_secret(
    secret: Secret,
    resource_id: str,
    resource_type: ResourceType,
) -> None:
    loaders = get_new_context()
    await add_secret(
        secret=secret,
        resource_id=resource_id,
        resource_type=resource_type,
        group_name=GROUP_NAME,
        loaders=loaders,
    )
    loaders.root_secrets.clear_all()
    secrets = await loaders.root_secrets.load(resource_id)
    assert len(secrets) > 0

    assert secrets[0] == secret


@parametrize(
    args=["secret", "resource_id", "resource_type"],
    cases=[
        [SecretFaker(), "gitroot1", ResourceType.ROOT],
    ],
)
@mocks(aws=IntegratesAws(dynamodb=IntegratesDynamodb()))
async def test_add_secret_group_not_found(
    secret: Secret,
    resource_id: str,
    resource_type: ResourceType,
) -> None:
    loaders = get_new_context()
    with raises(GroupNotFound):
        await add_secret(
            secret=secret,
            resource_id=resource_id,
            resource_type=resource_type,
            group_name=GROUP_NAME,
            loaders=loaders,
        )


@parametrize(
    args=["secret", "resource_id", "resource_type"],
    cases=[
        [SecretFaker(), "gitroot1", "ANOTHER"],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            groups=[GroupFaker(name=GROUP_NAME)],
            roots=[
                UrlRootFaker(root_id="urlroot1", group_name=GROUP_NAME),
                GitRootFaker(id="gitroot1", group_name=GROUP_NAME),
                IPRootFaker(id="iproot1", group_name=GROUP_NAME),
            ],
        )
    )
)
async def test_add_secret_resource_type_not_found(
    secret: Secret,
    resource_id: str,
    resource_type: ResourceType,
) -> None:
    loaders = get_new_context()
    with raises(ResourceTypeNotFound):
        await add_secret(
            secret=secret,
            resource_id=resource_id,
            resource_type=resource_type,
            group_name=GROUP_NAME,
            loaders=loaders,
        )


@parametrize(
    args=["root_id", "url_id", "user_email", "should_notify"],
    cases=[
        ["urlroot1", "url1", "email", True],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            roots=[
                UrlRootFaker(root_id="urlroot1", group_name=GROUP_NAME),
            ],
        )
    )
)
async def test_remove_environment_url_id_invalid_root_type(
    root_id: str,
    url_id: str,
    user_email: str,
    should_notify: bool,
) -> None:
    loaders = get_new_context()
    with raises(InvalidRootType):
        await remove_environment_url_id(
            loaders=loaders,
            root_id=root_id,
            url_id=url_id,
            group_name=GROUP_NAME,
            user_email=user_email,
            should_notify=should_notify,
        )


@parametrize(
    args=["root_id", "url_id", "user_email", "should_notify"],
    cases=[
        ["gitroot1", "url1", "user1@gmail.com", False],
        ["gitroot1", "url1", "user2@gmail.com", True],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            groups=[GroupFaker(name=GROUP_NAME)],
            roots=[
                GitRootFaker(id="gitroot1", group_name=GROUP_NAME),
            ],
            root_environment_urls=[
                RootEnvironmentUrlFaker(root_id="gitroot1", id="url1", group_name=GROUP_NAME)
            ],
        )
    )
)
async def test_remove_environment_url_id_success(
    root_id: str,
    url_id: str,
    user_email: str,
    should_notify: bool,
) -> None:
    loaders = get_new_context()
    await remove_environment_url_id(
        loaders=loaders,
        root_id=root_id,
        url_id=url_id,
        group_name=GROUP_NAME,
        user_email=user_email,
        should_notify=should_notify,
    )
    loaders.root_environment_urls.clear_all()
    urls = await loaders.root_environment_urls.load(
        RootEnvironmentUrlsRequest(root_id=root_id, group_name=GROUP_NAME)
    )
    assert len(urls) == 0


@parametrize(
    args=["resource_id", "secret_key", "resource_type", "group_name", "exception"],
    cases=[
        ["gitroot1", "url1", "ROOT", "unexistinggroup", GroupNotFound()],
        ["gitroot1", "url1", "ANOTHER", GROUP_NAME, ResourceTypeNotFound()],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            groups=[GroupFaker(name=GROUP_NAME)],
        )
    )
)
async def test_remove_secret_exception(
    resource_id: str,
    secret_key: str,
    resource_type: ResourceType,
    group_name: str,
    exception: CustomBaseException,
) -> None:
    loaders = get_new_context()

    with raises(CustomBaseException, match=str(exception)):
        await remove_secret(
            resource_id=resource_id,
            secret_key=secret_key,
            resource_type=resource_type,
            group_name=group_name,
            loaders=loaders,
        )


@parametrize(
    args=["resource_id", "secret_key", "resource_type"],
    cases=[
        ["gitroot1", "key1", "ROOT"],
        ["url1", "key2", "URL"],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            groups=[GroupFaker(name=GROUP_NAME)],
            roots=[
                GitRootFaker(id="gitroot1", group_name=GROUP_NAME),
            ],
            root_environment_urls=[
                RootEnvironmentUrlFaker(root_id="url1", id="url1", group_name=GROUP_NAME)
            ],
            root_secrets=[
                RootSecretsToUpdate(resource_id="gitroot1", secret=SecretFaker(key="key1"))
            ],
            root_environment_secrets=[
                RootEnvironmentSecretsToUpdate(
                    group_name=GROUP_NAME, resource_id="url1", secret=SecretFaker(key="key2")
                )
            ],
        )
    )
)
async def test_remove_secret_success(
    resource_id: str,
    secret_key: str,
    resource_type: ResourceType,
) -> None:
    loaders = get_new_context()
    secrets = []
    if resource_type == ResourceType.ROOT:
        secrets = await loaders.root_secrets.load(resource_id)
    else:
        secrets = await loaders.environment_secrets.load(
            RootEnvironmentSecretsRequest(group_name=GROUP_NAME, url_id=resource_id)
        )
    assert len(secrets) == 1

    await remove_secret(
        resource_id=resource_id,
        secret_key=secret_key,
        resource_type=resource_type,
        group_name=GROUP_NAME,
        loaders=loaders,
    )
    loaders.root_secrets.clear_all()
    loaders.environment_secrets.clear_all()

    secrets = []
    if resource_type == ResourceType.ROOT:
        secrets = await loaders.root_secrets.load(resource_id)
    else:
        secrets = await loaders.environment_secrets.load(
            RootEnvironmentSecretsRequest(group_name=GROUP_NAME, url_id=resource_id)
        )
    assert len(secrets) == 0


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            stakeholders=[StakeholderFaker(email="admin@test.test", enrolled=True)],
            groups=[
                GroupFaker(
                    name=GROUP_NAME,
                    organization_id="org1",
                    state=GroupStateFaker(
                        has_essential=False, service=GroupService.WHITE, tier=GroupTier.OTHER
                    ),
                ),
            ],
            group_access=[
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email="admin@test.test",
                    state=GroupAccessStateFaker(has_access=True, role="user"),
                ),
            ],
            roots=[
                GitRootFaker(
                    id="root1",
                    group_name=GROUP_NAME,
                    state=GitRootStateFaker(
                        gitignore=["bower_components/*", "node_modules/*"],
                        includes_health_check=True,
                        status=RootStatus.ACTIVE,
                    ),
                ),
            ],
            root_environment_urls=[
                RootEnvironmentUrlFaker(
                    url="https://test.com/",
                    group_name=GROUP_NAME,
                    root_id="root1",
                ),
            ],
            toe_inputs=[
                ToeInputFaker(
                    component="https://test.com/",
                    entry_point="login",
                    root_id=ROOT_ID,
                    group_name=GROUP_NAME,
                    state=ToeInputStateFaker(be_present=True),
                )
            ],
            findings=[FindingFaker(group_name=GROUP_NAME, id="fin1")],
            vulnerabilities=[
                VulnerabilityFaker(
                    finding_id="fin1",
                    group_name=GROUP_NAME,
                    root_id=ROOT_ID,
                    id="vuln_id_1",
                    hacker_email="non_machine@fluidattacks.com",
                    state=VulnerabilityStateFaker(
                        status=VulnerabilityStateStatus.VULNERABLE,
                        where="https://test.com/",
                        specific="login (about)",
                    ),
                ),
                VulnerabilityFaker(
                    finding_id="fin1",
                    group_name=GROUP_NAME,
                    root_id=ROOT_ID,
                    id="vuln_id_2",
                    hacker_email="machine@fluidattacks.com",
                    state=VulnerabilityStateFaker(
                        status=VulnerabilityStateStatus.SUBMITTED, where="https://test.com/"
                    ),
                ),
            ],
        )
    )
)
async def test_remove_related_resources() -> None:
    loaders = get_new_context()
    email = "user@test.test"
    info = GraphQLResolveInfoFaker(user_email=email, decorators=[])
    root = await get_root(
        loaders=loaders, group_name=GROUP_NAME, root_id=ROOT_ID, clear_loader=True
    )
    root_vulns = await loaders.root_vulnerabilities.load(root.id)
    assert len(root_vulns) == 2
    root_env_urls = await loaders.root_environment_urls.load(
        RootEnvironmentUrlsRequest(root_id=root.id, group_name=GROUP_NAME),
    )
    assert len(root_env_urls) == 1

    await remove_related_resources(
        loaders=loaders,
        info=info,
        user_email=email,
        root_env=root_env_urls[0],
    )

    new_loaders = get_new_context()

    root_toe_inputs = await new_loaders.root_toe_inputs.load_nodes(
        RootToeInputsRequest(group_name=GROUP_NAME, root_id=ROOT_ID)
    )
    assert len(root_toe_inputs) == 1
    assert root_toe_inputs[0].state.be_present is False
    assert root_toe_inputs[0].component == root_env_urls[0].url

    new_root_vulns = await new_loaders.root_vulnerabilities.load(root.id)
    assert len(new_root_vulns) == 1
    assert new_root_vulns[0].state.status == VulnerabilityStateStatus.SAFE


@mocks(
    aws=IntegratesAws(dynamodb=IntegratesDynamodb(roots=[GitRootFaker(id="root1")])),
)
async def test_move_root_group_not_found() -> None:
    loaders = get_new_context()
    with raises(GroupNotFound):
        await roots_domain.move_root(
            loaders=loaders,
            email=EMAIL_TEST,
            group_name=GROUP_NAME,
            root_id="root1",
            target_group_name="target_group",
        )


@parametrize(
    args=["root_id", "target_group"],
    cases=[
        ["root1", "group_outside"],
        ["root2", "group_target"],
        ["root3", "group_source"],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            groups=[
                GroupFaker(name="group_source"),
                GroupFaker(name="group_target"),
                GroupFaker(name="group_outside", organization_id="another_org"),
            ],
            roots=[
                GitRootFaker(id="root1", group_name="group_source"),
                GitRootFaker(
                    id="root2",
                    group_name="group_source",
                    state=GitRootStateFaker(status=RootStatus.INACTIVE, url="https://test.com/"),
                ),
                GitRootFaker(
                    id="root3",
                    group_name="group_source",
                    state=GitRootStateFaker(url="https://test-2.com/"),
                ),
            ],
        )
    ),
)
async def test_move_root_group_invalid_parameter(root_id: str, target_group: str) -> None:
    loaders = get_new_context()
    with raises(InvalidParameter, match="group_name"):
        await roots_domain.move_root(
            loaders=loaders,
            email=EMAIL_TEST,
            group_name="group_source",
            root_id=root_id,
            target_group_name=target_group,
        )


@parametrize(
    args=["root_id"],
    cases=[
        ["gitroot"],
        ["urlroot"],
        ["iproot"],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            groups=[
                GroupFaker(name="group_source"),
                GroupFaker(name="group_target"),
            ],
            roots=[
                GitRootFaker(id="gitroot", group_name="group_source"),
                UrlRootFaker(
                    root_id="urlroot",
                    group_name="group_source",
                ),
                IPRootFaker(
                    id="iproot",
                    group_name="group_source",
                ),
                GitRootFaker(group_name="group_target"),
                UrlRootFaker(group_name="group_target"),
                IPRootFaker(group_name="group_target"),
            ],
        )
    ),
)
async def test_move_root_group_repeated_root(root_id: str) -> None:
    loaders = get_new_context()
    with raises(RepeatedRoot):
        await roots_domain.move_root(
            loaders=loaders,
            email=EMAIL_TEST,
            group_name="group_source",
            root_id=root_id,
            target_group_name="group_target",
        )


@parametrize(
    args=["root_id"],
    cases=[
        ["git_root"],
        ["url_root"],
        ["ip_root"],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            groups=[
                GroupFaker(name=GROUP_NAME, organization_id="org1"),
                GroupFaker(name="target_group", organization_id="org1"),
            ],
            roots=[
                GitRootFaker(
                    group_name=GROUP_NAME,
                    id="git_root",
                ),
                UrlRootFaker(
                    group_name=GROUP_NAME,
                    root_id="url_root",
                ),
                IPRootFaker(
                    group_name=GROUP_NAME,
                    id="ip_root",
                ),
            ],
        )
    ),
)
async def test_move_root(
    root_id: str,
) -> None:
    loaders = get_new_context()
    new_root = await roots_domain.move_root(
        loaders=loaders,
        email=EMAIL_TEST,
        group_name=GROUP_NAME,
        root_id=root_id,
        target_group_name="target_group",
    )
    assert new_root.group_name == "target_group"
    assert new_root.state.reason == RootStateReason.MOVED_FROM_ANOTHER_GROUP
    loaders.root.clear_all()
    source_root = await loaders.root.load(RootRequest(GROUP_NAME, root_id))
    assert source_root
    assert source_root.state.reason == RootStateReason.MOVED_TO_ANOTHER_GROUP
