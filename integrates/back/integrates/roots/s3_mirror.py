import asyncio
import configparser
import logging
import os
import tarfile
import tempfile
from asyncio import subprocess
from os import path
from pathlib import Path

from fluidattacks_core.git import download_repo_from_s3
from git import NoSuchPathError, Repo

from integrates.context import FI_AWS_S3_CONTINUOUS_REPOSITORIES, FI_AWS_S3_PATH_PREFIX
from integrates.custom_utils.exceptions import NETWORK_ERRORS
from integrates.decorators import retry_on_exceptions
from integrates.s3.operations import file_exists
from integrates.s3.resource import get_client
from integrates.settings.logger import LOGGING

logging.config.dictConfig(LOGGING)
LOGGER = logging.getLogger(__name__)


async def download_repo(
    group_name: str,
    git_root_nickname: str,
    path_to_extract: str,
    gitignore: list[str] | None = None,
) -> Repo | None:
    repo_path = os.path.join(path_to_extract, git_root_nickname)
    if (
        download_url := await get_download_url(group_name, git_root_nickname)
    ) and await download_repo_from_s3(download_url, Path(repo_path), gitignore):
        try:
            return Repo(repo_path)
        except NoSuchPathError as error:
            LOGGER.error(
                error,
                extra={
                    "group_name": group_name,
                    "repo_nickname": git_root_nickname,
                },
            )

    return None


async def get_download_url(group_name: str, root_nickname: str) -> str | None:
    object_name = f"{group_name}/{root_nickname}.tar.gz"
    object_key = f"{FI_AWS_S3_PATH_PREFIX}{object_name}"
    client = await get_client()
    if not await file_exists(object_key, bucket=FI_AWS_S3_CONTINUOUS_REPOSITORIES):
        return None

    return await client.generate_presigned_url(
        ClientMethod="get_object",
        Params={
            "Bucket": FI_AWS_S3_CONTINUOUS_REPOSITORIES,
            "Key": object_key,
        },
        ExpiresIn=1800,
    )


async def get_upload_url(group_name: str, root_nickname: str) -> str | None:
    object_name = f"{group_name}/{root_nickname}.tar.gz"
    client = await get_client()
    return await client.generate_presigned_url(
        ClientMethod="put_object",
        Params={
            "Bucket": FI_AWS_S3_CONTINUOUS_REPOSITORIES,
            "Key": f"{FI_AWS_S3_PATH_PREFIX}{object_name}",
        },
        ExpiresIn=1800,
    )


def _clean_git_config_file(git_config_path: str) -> bool:
    # Read the contents of the .git/config file
    config = configparser.ConfigParser()
    config.read(git_config_path)

    # Remove the URL sections
    if 'remote "origin"' in config.sections():
        config.remove_section('remote "origin"')

    # Save the modified config file
    with open(git_config_path, "w", encoding="utf-8") as config_file:
        config.write(config_file)

    return True


def _create_git_root_tar_file(
    root_nickname: str,
    repo_path: str,
    tar_file_name: str,
) -> bool:
    git_dir = path.normpath(f"{repo_path}/.git")
    config_path = path.join(git_dir, "config")
    if path.exists(config_path):
        _clean_git_config_file(config_path)
    with tarfile.open(tar_file_name, "w:gz") as tar_handler:
        if path.exists(git_dir):
            tar_handler.add(git_dir, arcname=f"{root_nickname}/.git", recursive=True)
            return True
        return False


async def upload_cloned_repo_to_s3_tar(
    *,
    repo_path: str,
    group_name: str,
    nickname: str,
) -> bool:
    s3_client = await get_client()
    proc = await asyncio.create_subprocess_exec(
        "aws",
        "s3",
        "sync",
        repo_path,
        (
            f"s3://{FI_AWS_S3_CONTINUOUS_REPOSITORIES}"
            f"/{FI_AWS_S3_PATH_PREFIX}{group_name}/{nickname}/"
        ),
        "--exclude",
        ".git/**",
        "--exclude",
        ".git/*",
        "--delete",
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
    )
    await proc.communicate()

    with tempfile.NamedTemporaryFile() as tmp_file:
        if not _create_git_root_tar_file(nickname, repo_path, tmp_file.name):
            LOGGER.error(
                "Failed to compress root %s",
                nickname,
                extra={"extra": locals()},
            )

            return False

        s3_client_upload_file = retry_on_exceptions(
            exceptions=NETWORK_ERRORS,
            max_attempts=3,
            sleep_seconds=30,
        )(s3_client.upload_file)
        try:
            await s3_client_upload_file(
                tmp_file.name,
                FI_AWS_S3_CONTINUOUS_REPOSITORIES,
                f"{FI_AWS_S3_PATH_PREFIX}{group_name}/{nickname}.tar.gz",
            )

            return True
        except NETWORK_ERRORS as ex:
            LOGGER.exception(
                ex,
                extra={"extra": {"group_name": group_name, "root_nickname": nickname}},
            )

            return False
