import csv
import tempfile
import uuid
from collections import defaultdict
from io import BytesIO
from itertools import groupby

import aiofiles
from aioextensions import in_thread
from starlette.datastructures import Headers, UploadFile

from integrates.custom_utils.groups import get_group
from integrates.dataloaders import Dataloaders
from integrates.db_model.roots.enums import RootType
from integrates.db_model.roots.types import (
    GitRootState,
    GroupEnvironmentUrlsRequest,
    RootEnvironmentUrl,
)
from integrates.roots.storage import sign_url, upload_file
from integrates.roots.types import EditedInfo


async def get_git_roots_file(
    loaders: Dataloaders,
    group_name: str,
) -> str:
    group = await get_group(loaders, group_name)
    roots = await loaders.group_roots.load(group_name)
    environment_urls = await loaders.group_environment_urls.load(
        GroupEnvironmentUrlsRequest(group_name=group_name),
    )
    environment_urls_by_root_id = defaultdict(
        list[RootEnvironmentUrl],
        {
            root_id: list(env_urls)
            for root_id, env_urls in groupby(
                sorted(environment_urls, key=lambda env_url: env_url.root_id),
                key=lambda env_url: env_url.root_id,
            )
        },
    )
    org_credentials = await loaders.organization_credentials.load(group.organization_id)
    org_credentials_by_id = {credentials.id: credentials for credentials in org_credentials}

    def _get_last_edited_info(
        state: GitRootState,
        env_urls: list[RootEnvironmentUrl],
    ) -> EditedInfo:
        last_edited_info = EditedInfo(
            modified_by=state.modified_by,
            modified_date=state.modified_date,
        )
        for env_url in env_urls:
            if env_url.state.modified_date > last_edited_info.modified_date:
                last_edited_info = EditedInfo(
                    modified_by=env_url.state.modified_by,
                    modified_date=env_url.state.modified_date,
                )
        return last_edited_info

    def _save_git_roots_file(filename: str) -> None:
        with open(filename, "w", encoding="utf-8") as file:
            writer = csv.DictWriter(
                file,
                fieldnames=[
                    "Id",
                    "Nickname",
                    "State",
                    "Branch",
                    "URL",
                    "Cloning message",
                    "Cloning status",
                    "Criticality",
                    "Includes_health_check",
                    "Use egress",
                    "Use vpn",
                    "Use ztna",
                    "Repo creation date",
                    "Added by (mail)",
                    "Last edited by (mail)",
                    "Date last edited",
                    "Credentials Id",
                    "Credentials name",
                    "Credentials type",
                    "Gitignore",
                    "Environments",
                    "Excluded environments",
                ],
            )
            writer.writeheader()
            writer.writerows(
                [
                    {
                        "Id": root.id,
                        "Nickname": root.state.nickname,
                        "State": root.state.status,
                        "Branch": root.state.branch,
                        "URL": root.state.url,
                        "Cloning message": root.cloning.reason,
                        "Cloning status": root.cloning.status,
                        "Criticality": root.state.criticality or "UNKNOWN",
                        "Includes_health_check": (root.state.includes_health_check),
                        "Use egress": root.state.use_egress,
                        "Use vpn": root.state.use_vpn,
                        "Use ztna": root.state.use_ztna,
                        "Repo creation date": root.created_date,
                        "Added by (mail)": root.created_by,
                        "Last edited by (mail)": last_edited_info.modified_by,
                        "Date last edited": last_edited_info.modified_date,
                        **(
                            {
                                "Credentials Id": credentials.id,
                                "Credentials name": credentials.state.name,
                                "Credentials type": credentials.state.type,
                            }
                            if root.state.credential_id
                            and (credentials := org_credentials_by_id.get(root.state.credential_id))
                            else {}
                        ),
                        "Gitignore": " | ".join(root.state.gitignore),
                        "Environments": " | ".join(
                            map(
                                lambda env_url: env_url.url,
                                filter(
                                    lambda env_url: env_url.state.include,
                                    environment_urls_by_root_id[root.id],
                                ),
                            ),
                        ),
                        "Excluded environments": " | ".join(
                            map(
                                lambda env_url: env_url.url,
                                filter(
                                    lambda env_url: not env_url.state.include,
                                    environment_urls_by_root_id[root.id],
                                ),
                            ),
                        ),
                    }
                    for root in roots
                    if root.type is RootType.GIT
                    and (
                        last_edited_info := _get_last_edited_info(
                            root.state,
                            environment_urls_by_root_id[root.id],
                        )
                    )
                ],
            )

    with tempfile.NamedTemporaryFile() as temp_file:
        await in_thread(_save_git_roots_file, temp_file.name)
        async with aiofiles.open(temp_file.name, "rb") as file:
            uploaded_file = UploadFile(
                filename=f"{group_name}_git-roots_{uuid.uuid4()!s}.csv",
                file=BytesIO(),
                headers=Headers(headers={"content_type": "text/csv"}),
            )
            await uploaded_file.write(await file.read())
            await uploaded_file.seek(0)
            uploaded_file_name = await upload_file(uploaded_file)
            uploaded_file_url = await sign_url(uploaded_file_name)
    return uploaded_file_url
