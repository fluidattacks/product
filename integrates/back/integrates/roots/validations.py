import functools
import logging
import logging.config
import os
import re
from collections.abc import Callable, Iterable
from contextlib import suppress
from ipaddress import ip_address
from typing import Any
from urllib.parse import ParseResult, unquote_plus, urlparse

import aiohttp
import fluidattacks_core.git as git_utils
from fluidattacks_core.git import InvalidParameter as GitInvalidParameter
from fluidattacks_core.git.https_utils import format_redirected_url, get_redirected_url
from fluidattacks_core.http.validations import HTTPValidationError
from git.cmd import Git
from git.exc import GitCommandError
from urllib3.exceptions import LocationParseError
from urllib3.util.url import Url, parse_url

from integrates import authz
from integrates.authz.model import GIT_IDENTIFIER
from integrates.custom_exceptions import (
    BranchNotFound,
    ExcludedEnvironment,
    InactiveRoot,
    InvalidAuthorization,
    InvalidChar,
    InvalidGitCredentials,
    InvalidGitRoot,
    InvalidGroupService,
    InvalidParameter,
    InvalidPort,
    InvalidProtocol,
    InvalidRoleSetGitIgnore,
    InvalidRootExclusion,
    InvalidRootType,
    InvalidSubPath,
    InvalidSubPathInUrlRootWithQuery,
    InvalidUrl,
    OrganizationNotFound,
    RepeatedRoot,
    RepeatedRootEnvironmentUrl,
    RepeatedRootNickname,
    RequiredCredentials,
    RequiredValue,
    UriAlreadyExists,
)
from integrates.custom_utils import datetime as datetime_utils
from integrates.custom_utils import validations as custom_validations
from integrates.custom_utils.groups import get_group
from integrates.custom_utils.roots import get_root_base_url
from integrates.custom_utils.utils import HOST_PAT, LOCAL_HOSTS, codecommitparser, validate_filename
from integrates.custom_utils.validations import get_attr_value
from integrates.dataloaders import Dataloaders
from integrates.db_model import roots as roots_model
from integrates.db_model.credentials.types import (
    AWSRoleSecret,
    Credentials,
    CredentialsRequest,
    HttpsPatSecret,
    HttpsSecret,
    OauthAzureSecret,
    OauthBitbucketSecret,
    OauthGithubSecret,
    OauthGitlabSecret,
    Secret,
    SshSecret,
)
from integrates.db_model.groups.enums import GroupSubscriptionType
from integrates.db_model.groups.types import Group
from integrates.db_model.roots.enums import (
    RootDockerImageStateStatus,
    RootEnvironmentCloud,
    RootEnvironmentUrlType,
    RootStateReason,
    RootStatus,
)
from integrates.db_model.roots.get import get_git_environment_url_by_id
from integrates.db_model.roots.types import (
    GitRoot,
    GitRootState,
    IPRoot,
    IPRootState,
    Root,
    RootDockerImageRequest,
    RootEnvironmentUrl,
    RootEnvironmentUrlsRequest,
    RootRequest,
    URLRoot,
    URLRootState,
)
from integrates.decorators import retry_on_exceptions
from integrates.oauth.common import get_credential_token, get_oauth_type
from integrates.organizations import utils as orgs_utils
from integrates.organizations.domain import get_credentials
from integrates.roots import utils as roots_utils
from integrates.roots.docker_images import get_image_manifest
from integrates.settings import LOGGING
from integrates.tickets import domain as tickets_domain

logging.config.dictConfig(LOGGING)

LOGGER = logging.getLogger(__name__)


async def validate_git_access(
    *,
    url: str,
    branch: str,
    secret: (
        HttpsSecret
        | HttpsPatSecret
        | OauthAzureSecret
        | OauthBitbucketSecret
        | OauthGithubSecret
        | OauthGitlabSecret
        | SshSecret
        | AWSRoleSecret
    ),
    loaders: Dataloaders,
    is_pat: bool = False,
    credential_id: str | None = None,
    organization_id: str | None = None,
    follow_redirects: bool = False,
) -> None:
    url = roots_utils.format_git_repo_url(url)

    if isinstance(secret, SshSecret):
        secret = SshSecret(key=orgs_utils.format_credentials_ssh_key(secret.key))
    await validate_git_credentials(
        repo_url=url,
        branch=branch,
        secret=secret,
        loaders=loaders,
        is_pat=is_pat,
        credential_id=credential_id,
        organization_id=organization_id,
        follow_redirects=follow_redirects,
    )


async def validate_credential_in_organization(
    loaders: Dataloaders,
    credential_id: str,
    organization_id: str,
) -> None:
    credential = await loaders.credentials.load(
        CredentialsRequest(
            id=credential_id,
            organization_id=organization_id,
        ),
    )
    if credential is None:
        raise InvalidGitCredentials()


@retry_on_exceptions(
    exceptions=(aiohttp.ClientError, TimeoutError),
    sleep_seconds=0.5,
    max_attempts=3,
)
async def _get_redirected_url(
    *,
    repo_url: str,
    group_name: str,
    user: str | None = None,
    password: str | None = None,
    token: str | None = None,
    is_pat: bool = False,
) -> str:
    try:
        redirected_url = await get_redirected_url(
            url=repo_url,
            user=user,
            password=password,
            token=token,
            is_pat=is_pat,
        )
    except (ValueError, HTTPValidationError) as exc:
        LOGGER.warning(
            "Validation Error",
            extra={
                "extra": {"repo_url": repo_url, "group_name": group_name},
            },
        )
        raise InvalidGitCredentials from exc
    else:
        with suppress(LocationParseError):
            return (
                format_redirected_url(parse_url(repo_url), parse_url(redirected_url))
                .removesuffix("/")
                .strip()
            )
        LOGGER.warning(
            "Parse url error",
            extra={
                "extra": {"repo_url": repo_url, "group_name": group_name},
            },
        )
    return repo_url


async def validate_and_get_redirected_url(
    *,
    repo_url: str,
    secret: Secret | None,
    group_name: str,
    loaders: Dataloaders,
    credential_id: str | None,
    organization_id: str | None,
) -> str:
    try:
        if secret is None:
            if repo_url.startswith("http"):
                return await _get_redirected_url(repo_url=repo_url, group_name=group_name)
        elif isinstance(secret, HttpsSecret):
            return await _get_redirected_url(
                repo_url=repo_url,
                user=secret.user,
                password=secret.password,
                group_name=group_name,
            )
        elif isinstance(secret, HttpsPatSecret):
            return await _get_redirected_url(
                repo_url=repo_url,
                token=secret.token,
                is_pat=True,
                group_name=group_name,
            )
        elif (
            isinstance(
                secret,
                (
                    OauthGithubSecret,
                    OauthAzureSecret,
                    OauthGitlabSecret,
                    OauthBitbucketSecret,
                ),
            )
            and credential_id
            and organization_id
            and (
                cred := await loaders.credentials.load(
                    CredentialsRequest(
                        id=credential_id,
                        organization_id=organization_id,
                    )
                )
            )
        ):
            token = await get_credential_token(loaders=loaders, credential=cred)
            return await _get_redirected_url(repo_url=repo_url, token=token, group_name=group_name)
    except (aiohttp.ClientError, TimeoutError) as exc:
        LOGGER.exception(
            "Failed to get redirected url",
            extra={
                "extra": {
                    "credential_id": cred.id if cred else "",
                    "group_name": group_name,
                    "repo_url": repo_url,
                    "exc": exc,
                },
            },
        )
        return repo_url
    except (InvalidAuthorization, GitInvalidParameter) as exc:
        LOGGER.error(
            "Invalid Authorization",
            extra={
                "exc": exc,
                "repo_url": repo_url,
                "group_name": group_name,
                "credential_id": cred.id if cred else "",
            },
        )
        raise InvalidGitCredentials from exc

    return repo_url


async def working_credentials(
    url: str,
    branch: str,
    credentials: Credentials | None,
    loaders: Dataloaders,
    follow_redirects: bool = False,
) -> None:
    if not credentials:
        raise RequiredCredentials()

    await validate_git_access(
        url=url,
        branch=branch,
        secret=credentials.secret,
        loaders=loaders,
        is_pat=credentials.state.is_pat,
        organization_id=credentials.organization_id,
        credential_id=credentials.id,
        follow_redirects=follow_redirects,
    )


async def validate_image_uri_in_root(
    loaders: Dataloaders,
    root_id: str,
    group_name: str,
    uri: str,
) -> None:
    docker_image = await loaders.docker_image.load(RootDockerImageRequest(root_id, group_name, uri))
    if docker_image:
        if docker_image.state.status == RootDockerImageStateStatus.DELETED:
            return
        raise UriAlreadyExists()


async def validate_image_access(
    *,
    uri: str,
    external_id: str | None = None,
    credentials: Credentials | None = None,
) -> None:
    await get_image_manifest(
        image_ref=uri,
        credentials=credentials,
        external_id=external_id,
    )


async def working_image_credentials(
    uri: str,
    credentials: Credentials | None,
    external_id: str | None,
) -> None:
    if not credentials:
        raise RequiredCredentials()

    await validate_image_access(
        uri=uri,
        credentials=credentials,
        external_id=external_id,
    )


def validate_exclusions(patterns: list[str]) -> bool:
    """
    Validate if exclusion patterns are valid

    Args:
        patterns (list[str]): list of patterns to validate

    Raises:
        InvalidChar: if the pattern contains invalid characters

    """
    invalid_chars = ["\\"]
    for char in invalid_chars:
        for pattern in patterns:
            if char in pattern:
                raise InvalidChar(f"{pattern} ('{char}')")

    for pattern in patterns:
        if len(pattern.strip()) == 0:
            raise RequiredValue()

    return True


def is_exclude_valid(exclude_patterns: list[str], url: str) -> bool:
    is_valid: bool = True

    if "*" in exclude_patterns:
        return False

    # Get repository name
    url_obj = urlparse(url)
    url_path = unquote_plus(url_obj.path)
    repo_name = os.path.basename(url_path)
    if repo_name.endswith(GIT_IDENTIFIER):
        repo_name = repo_name[0:-4]

    for pattern in exclude_patterns:
        pattern_as_list: list[str] = pattern.lower().split("/")
        if repo_name in pattern_as_list and pattern_as_list.index(repo_name) == 0:
            is_valid = False
    return is_valid


def is_valid_url(url: str) -> bool:
    if url.startswith("codecommit"):
        return True
    try:
        url_attributes: Url | ParseResult = parse_url(url)
    except LocationParseError:
        url_attributes = urlparse(url)

    return bool(url_attributes.netloc and url_attributes.scheme)


def is_valid_git_branch(branch_name: str) -> bool:
    try:
        Git().check_ref_format("--branch", branch_name)
        return True
    except GitCommandError:
        return False


def is_nickname_unique(
    nickname: str,
    roots: Iterable[Root],
    url: str,
    old_nickname: str = "",
) -> bool:
    new_host_name = urlparse(url).netloc
    host_name_exists = False

    nickname_exists = any(root.state.nickname == nickname for root in roots)
    for root in roots:
        if isinstance(root, GitRoot):
            host_name_exists = urlparse(root.state.url).netloc == new_host_name
            if host_name_exists:
                break

    if nickname != old_nickname and nickname_exists:
        return False

    return not (nickname_exists and not host_name_exists)


def validate_nickname_is_unique_deco(
    nickname_field: str,
    roots_fields: str,
    old_nickname_field: str = "",
) -> Callable:
    """
    Validate the uniqueness of the nickname of a root (IPRoot and URLRoot
    only)

    Args:
        nickname_field (str): The field name of the new nickname
        roots_fields (str): The field name of the roots
        old_nickname_field (str, optional): The field name of previous
        root name. Defaults to "".

    Raises:
        RepeatedRootNickname: if the nickname is not unique.

    """

    def wrapper(func: Callable) -> Callable:
        @functools.wraps(func)
        def decorated(*args: Any, **kwargs: Any) -> Any:
            nickname: str = get_attr_value(field=nickname_field, kwargs=kwargs, obj_type=str)
            roots: tuple[Root, ...] = get_attr_value(
                field=roots_fields,
                kwargs=kwargs,
                obj_type=tuple[Root, ...],
            )
            old_nickname: str = get_attr_value(
                field=old_nickname_field,
                kwargs=kwargs,
                obj_type=str,
            )
            for root in roots:
                if (
                    isinstance(root, (IPRoot, URLRoot))
                    and nickname != old_nickname
                    and nickname == root.state.nickname
                ):
                    raise RepeatedRootNickname()
            return func(*args, **kwargs)

        return decorated

    return wrapper


def valid_url_branch(
    group_name: str,
    new_url: str,
    root: GitRoot,
    branch: str,
    root_id: str | None = None,
) -> bool:
    root_url = root.state.url[:-4] if root.state.url.endswith(GIT_IDENTIFIER) else root.state.url

    if (
        (new_url.lower(), group_name)
        == (
            root_url.lower(),
            root.group_name,
        )
        and root.state.status == RootStatus.ACTIVE
        and (not root_id or root.id != root_id)
    ):
        return False

    if (new_url.lower(), branch) == (
        root_url.lower(),
        root.state.branch,
    ):
        return False
    return True


def is_git_unique(
    *,
    url: str,
    branch: str,
    group_name: str,
    roots: Iterable[Root],
    include_inactive: bool = True,
    root_id: str | None = None,
) -> bool:
    """
    Validation util to check whether a git root is unique

    This logic must match the associated documentation page at:
    https://help.fluidattacks.com/portal/en/kb/articles/manage-roots#Manage_Git_Roots
    """
    new_url = url[:-4] if url.endswith(GIT_IDENTIFIER) else url

    for root in roots:
        if (
            isinstance(root, GitRoot)
            and (root.state.status == RootStatus.ACTIVE or include_inactive)
            and root.state.reason != RootStateReason.GROUP_DELETED
            and not valid_url_branch(group_name, new_url, root, branch, root_id)
        ):
            return False

    return True


def is_valid_ip(address: str) -> bool:
    try:
        ip_address(address)
        return True
    except ValueError:
        return False


def is_valid_root(root: Root, include_inactive: bool) -> bool:
    return (
        root.state.status == RootStatus.ACTIVE or include_inactive
    ) and root.state.reason != RootStateReason.GROUP_DELETED


async def validate_root_id(root_id: str, group_name: str, loaders: Dataloaders) -> None:
    root = await loaders.root.load(RootRequest(group_name, root_id))
    if not isinstance(root, (GitRoot, IPRoot, URLRoot)):
        raise InvalidRootType()


async def validate_environment_url_id(url_id: str, group_name: str) -> None:
    environment_url = await get_git_environment_url_by_id(url_id=url_id, group_name=group_name)
    if not isinstance(environment_url, RootEnvironmentUrl):
        raise InvalidRootType()


def is_ip_group_unique(
    address: str,
    group_name: str,
    roots: Iterable[Root],
    include_inactive: bool = False,
) -> bool:
    return address not in tuple(
        root.state.address
        for root in roots
        if isinstance(root, IPRoot)
        and is_valid_root(root, include_inactive)
        and (address, group_name) == (root.state.address, root.group_name)
    )


def is_ip_unique(
    address: str,
    roots: Iterable[Root],
    include_inactive: bool = False,
) -> bool:
    return address not in tuple(
        root.state.address
        for root in roots
        if isinstance(root, IPRoot) and is_valid_root(root, include_inactive)
    )


def is_url_group_unique(
    *,
    group_name: str,
    host: str,
    path: str,
    port: str,
    protocol: str,
    query: str | None,
    roots: Iterable[Root],
    include_inactive: bool = False,
) -> bool:
    return (host, path, port, protocol, query, group_name) not in tuple(
        (
            root.state.host,
            root.state.path,
            root.state.port,
            root.state.protocol,
            root.state.query,
            root.group_name,
        )
        for root in roots
        if isinstance(root, URLRoot) and is_valid_root(root, include_inactive)
    )


def is_url_unique(
    *,
    host: str,
    path: str,
    port: str,
    protocol: str,
    query: str | None,
    roots: Iterable[Root],
    include_inactive: bool = False,
) -> bool:
    return (host, path, port, protocol, query) not in tuple(
        (
            root.state.host,
            root.state.path,
            root.state.port,
            root.state.protocol,
            root.state.query,
        )
        for root in roots
        if isinstance(root, URLRoot) and is_valid_root(root, include_inactive)
    )


def validate_active_root(root: Root) -> None:
    if root.state.status == RootStatus.ACTIVE:
        return
    raise InactiveRoot()


def validate_git_root(root: Root) -> None:
    if not isinstance(root, GitRoot):
        raise InvalidGitRoot()


def validate_nickname(nickname: str, ex: Exception = InvalidChar()) -> None:
    if not re.match(r"^[a-zA-Z0-9@:_\-\.]{1,128}$", nickname):
        raise ex


def validate_nickname_deco(nickname_field: str) -> Callable:
    """
    Validate the nickname of a root with regex

    Args:
        nickname_field (str): the field name of the nickname

    Raises:
        InvalidChar: if the nickname is not valid

    """

    def wrapper(func: Callable) -> Callable:
        @functools.wraps(func)
        def decorated(*args: Any, **kwargs: Any) -> Any:
            nickname: str = get_attr_value(field=nickname_field, kwargs=kwargs, obj_type=str)
            if nickname:
                validate_nickname(nickname)
            return func(*args, **kwargs)

        return decorated

    return wrapper


async def _validate_git_credentials_ssh(repo_url: str, branch: str, credential_key: str) -> None:
    last_commit, _ = await git_utils.ssh_ls_remote(
        repo_url=repo_url,
        branch=branch,
        credential_key=credential_key,
    )
    if last_commit is None:
        raise InvalidGitCredentials()

    if not last_commit:
        raise BranchNotFound()


async def validate_git_credentials_oauth(
    repo_url: str,
    branch: str,
    loaders: Dataloaders,
    credential_id: str,
    organization_id: str,
    follow_redirects: bool = False,
) -> None:
    credential = await loaders.credentials.load(
        CredentialsRequest(
            id=credential_id,
            organization_id=organization_id,
        ),
    )
    if credential is None:
        raise InvalidGitCredentials()

    token = await get_credential_token(
        credential=credential,
        loaders=loaders,
    )

    if token is None:
        raise InvalidGitCredentials()

    last_commit, _ = await git_utils.https_ls_remote(
        branch=branch,
        repo_url=repo_url,
        password=None,
        token=token,
        user=None,
        provider=get_oauth_type(credential),
        follow_redirects=follow_redirects,
    )
    if last_commit is None:
        raise InvalidGitCredentials()

    if not last_commit:
        raise BranchNotFound()


async def _validate_git_credentials_https(
    *,
    repo_url: str,
    branch: str,
    user: str | None = None,
    password: str | None = None,
    token: str | None = None,
    is_pat: bool = False,
    follow_redirects: bool = False,
) -> None:
    last_commit, _ = await git_utils.https_ls_remote(
        branch=branch,
        repo_url=repo_url,
        password=password,
        token=token,
        user=user,
        is_pat=is_pat,
        follow_redirects=follow_redirects,
    )
    if last_commit is None:
        raise InvalidGitCredentials()

    if not last_commit:
        raise BranchNotFound()


async def _validate_git_credentials_aws_role(
    *,
    loaders: Dataloaders,
    repo_url: str,
    branch: str,
    arn: str | None = None,
    organization_id: str | None,
    follow_redirects: bool = False,
) -> None:
    if organization_id is None:
        raise OrganizationNotFound()
    org = await loaders.organization.load(organization_id)
    external_id = org.state.aws_external_id if org else None
    quoted_url = roots_utils.quote_url(repo_url.strip())
    parsed_url = codecommitparser(quoted_url)

    last_commit, _ = await git_utils.ls_remote(
        repo_branch=branch,
        repo_url=str(parsed_url).rstrip(" /"),
        arn=arn,
        org_external_id=external_id,
        follow_redirects=follow_redirects,
    )
    if last_commit is None:
        raise InvalidGitCredentials()

    if not last_commit:
        raise BranchNotFound()


async def validate_git_credentials(
    *,
    repo_url: str,
    branch: str,
    secret: (
        HttpsSecret
        | HttpsPatSecret
        | OauthAzureSecret
        | OauthBitbucketSecret
        | OauthGithubSecret
        | OauthGitlabSecret
        | SshSecret
        | AWSRoleSecret
    ),
    loaders: Dataloaders,
    is_pat: bool = False,
    credential_id: str | None = None,
    organization_id: str | None = None,
    follow_redirects: bool = False,
) -> None:
    if isinstance(secret, SshSecret):
        await _validate_git_credentials_ssh(repo_url, branch, secret.key)
    elif isinstance(secret, AWSRoleSecret):
        await _validate_git_credentials_aws_role(
            loaders=loaders,
            repo_url=repo_url,
            branch=branch,
            arn=secret.arn,
            organization_id=organization_id,
            follow_redirects=follow_redirects,
        )
    elif isinstance(secret, HttpsPatSecret):
        redirected_url = await validate_and_get_redirected_url(
            repo_url=repo_url,
            secret=secret,
            group_name="",
            loaders=loaders,
            credential_id=None,
            organization_id=organization_id,
        )
        await _validate_git_credentials_https(
            repo_url=redirected_url,
            branch=branch,
            token=secret.token,
            user=None,
            password=None,
            is_pat=is_pat,
            follow_redirects=follow_redirects,
        )
    elif isinstance(secret, HttpsSecret):
        redirected_url = await validate_and_get_redirected_url(
            repo_url=repo_url,
            secret=secret,
            group_name="",
            loaders=loaders,
            credential_id=None,
            organization_id=organization_id,
        )
        await _validate_git_credentials_https(
            repo_url=redirected_url,
            branch=branch,
            token=None,
            user=secret.user,
            password=secret.password,
            follow_redirects=follow_redirects,
        )
    elif (
        isinstance(
            secret,
            (
                OauthGithubSecret,
                OauthAzureSecret,
                OauthBitbucketSecret,
                OauthGitlabSecret,
            ),
        )
        and organization_id
        and credential_id
    ):
        redirected_url = await validate_and_get_redirected_url(
            repo_url=repo_url,
            secret=(
                await get_credentials(
                    loaders,
                    credential_id,
                    organization_id,
                )
            ).secret,
            group_name="",
            loaders=loaders,
            credential_id=credential_id,
            organization_id=organization_id,
        )
        await validate_git_credentials_oauth(
            redirected_url,
            branch=branch,
            loaders=loaders,
            organization_id=organization_id,
            credential_id=credential_id,
            follow_redirects=follow_redirects,
        )


def validate_url_branch_deco(url_field: str, branch_field: str) -> Callable:
    def wrapper(func: Callable) -> Callable:
        @functools.wraps(func)
        def decorated(*args: Any, **kwargs: Any) -> Any:
            url: str = get_attr_value(field=url_field, kwargs=kwargs, obj_type=str)
            branch: str = get_attr_value(field=branch_field, kwargs=kwargs, obj_type=str)
            if not (is_valid_url(url) and is_valid_git_branch(branch)):
                raise InvalidParameter()
            return func(*args, **kwargs)

        return decorated

    return wrapper


def validate_git_root_url(root: GitRoot, gitignore: list[str]) -> None:
    if not is_exclude_valid(gitignore, root.state.url):
        raise InvalidRootExclusion()


async def validate_gitignore_update_permission(
    *,
    loaders: Dataloaders,
    gitignore: list[str],
    group_name: str,
    root: GitRoot,
    user_email: str,
) -> None:
    if gitignore != root.state.gitignore:
        enforcer = await authz.get_group_level_enforcer(loaders, user_email)
        if not enforcer(group_name, "update_git_root_filter"):
            raise InvalidRoleSetGitIgnore()


def sanitize_exclusion_line(exclusion_line: str) -> str:
    """
    Returns a sanitized string for fnmatch pattern matching

    See https://docs.python.org/3/library/fnmatch.html for more information
    """
    line = exclusion_line.strip()
    is_ignored = line.startswith("!")
    is_middle_slash = line.find("/", 1, len(line) - 1) != -1

    if line.startswith("#"):
        return ""

    if is_ignored:
        line = line[1:]

    if line.startswith("/"):
        line = line[1:]
    elif not (is_middle_slash or line.startswith("**") or is_ignored):
        line = "*" + line

    if line.endswith("/"):
        line = line + "*"

    line = line.replace("\\", "")

    return line


def validate_error_message(
    message: str,
) -> bool:
    errors_list: list[str] = [
        "fatal: not a git repository",
        "fatal: HTTP request failed",
        "error: branch ‘remotes/origin/ABC’ not found",
        "fatal: authentication failed",
        "remote: Invalid username or password",
        "Permission denied (publickey)",
    ]
    for error in errors_list:
        if error in message:
            return True
    return False


async def validate_excluded_paths(
    environment_url: RootEnvironmentUrl,
    loaders: Dataloaders,
    root_id: str,
    group_name: str,
) -> None:
    environment_urls = await loaders.root_environment_urls.load(
        RootEnvironmentUrlsRequest(root_id=root_id, group_name=group_name),
    )
    for env_url in environment_urls:
        if (
            environment_url.id != env_url.id
            and not env_url.state.include
            and environment_url.url.strip().startswith(env_url.url.strip())
        ):
            raise ExcludedEnvironment()


def validate_excluded_sub_paths(root: URLRoot, excluded_sub_paths: list[str] | None) -> None:
    if not excluded_sub_paths:
        return

    if root.state.query:
        raise InvalidSubPathInUrlRootWithQuery()

    base_url = get_root_base_url(root)
    for sub_path in excluded_sub_paths:
        if not sub_path or not is_valid_url(f"{base_url}/{sub_path}"):
            raise InvalidSubPath()


async def validate_root_and_update(
    email: str,
    group_name: str,
    new_status: RootStatus,
    org_roots: list[Root],
    root: Root,
) -> None:
    if isinstance(root, GitRoot):
        if not is_git_unique(
            url=root.state.url,
            branch=root.state.branch,
            group_name=group_name,
            roots=org_roots,
            include_inactive=False,
        ):
            raise RepeatedRoot()

        await roots_model.update_root_state(
            current_value=root.state,
            group_name=group_name,
            root_id=root.id,
            state=GitRootState(
                branch=root.state.branch,
                credential_id=root.state.credential_id,
                criticality=root.state.criticality,
                gitignore=root.state.gitignore,
                includes_health_check=root.state.includes_health_check,
                modified_by=email,
                modified_date=datetime_utils.get_utc_now(),
                nickname=root.state.nickname,
                other=None,
                reason=None,
                status=new_status,
                url=root.state.url,
                use_egress=root.state.use_egress,
                use_vpn=root.state.use_vpn,
                use_ztna=root.state.use_ztna,
            ),
        )

        if root.state.includes_health_check:
            await tickets_domain.request_health_check(
                branch=root.state.branch,
                group_name=group_name,
                repo_url=root.state.url,
                requester_email=email,
            )

    elif isinstance(root, IPRoot):
        if not is_ip_unique(root.state.address, org_roots):
            raise RepeatedRoot()

        await roots_model.update_root_state(
            current_value=root.state,
            group_name=group_name,
            root_id=root.id,
            state=IPRootState(
                address=root.state.address,
                modified_by=email,
                modified_date=datetime_utils.get_utc_now(),
                nickname=root.state.nickname,
                other=None,
                reason=None,
                status=new_status,
            ),
        )

    else:
        if not is_url_unique(
            host=root.state.host,
            path=root.state.path,
            port=root.state.port,
            protocol=root.state.protocol,
            query=root.state.query,
            roots=org_roots,
        ):
            raise RepeatedRoot()

        await roots_model.update_root_state(
            current_value=root.state,
            group_name=group_name,
            root_id=root.id,
            state=URLRootState(
                host=root.state.host,
                modified_by=email,
                modified_date=datetime_utils.get_utc_now(),
                nickname=root.state.nickname,
                other=None,
                path=root.state.path,
                port=root.state.port,
                protocol=root.state.protocol,
                reason=None,
                status=new_status,
            ),
        )


def validate_url_and_branch(url: str, branch: str) -> None:
    """
    Validates the url and if the branch belongs to git root

    Raises:
        InvalidParameter: if the url or branch are invalid

    """
    if not (is_valid_url(url) and roots_utils.is_allowed(url) and is_valid_git_branch(branch)):
        raise InvalidParameter("url or branch")


def validate_health_check(includes_health_check: bool, group: Group) -> None:
    """
    Validates if the group includes health check and has the
    Advanced service

    Args:
        includes_health_check (bool): group includes health check
        group (Group): group to validate

    Raises:
        InvalidGroupService: if the group does not have the Advanced service

    """
    service_enforcer = authz.get_group_service_attributes_enforcer(group)
    if includes_health_check and not service_enforcer("has_advanced"):
        raise InvalidGroupService()


async def validate_git_ignore(
    loaders: Dataloaders,
    gitignore: list[str],
    group_name: str,
    url: str,
    user_email: str,
) -> None:
    """
    Validates the gitignore and if the user has the permission to update it

    Raises:
        InvalidRoleSetGitIgnore: if the user does not allow to
        `update_git_root_filter` and it's trying to update the gitignore
        InvalidRootExclusion: if the gitignore is not valid

    """
    group_enforcer = await authz.get_group_level_enforcer(loaders, user_email)
    if gitignore and not group_enforcer(group_name, "update_git_root_filter"):
        raise InvalidRoleSetGitIgnore()
    if not is_exclude_valid(gitignore, url):
        raise InvalidRootExclusion()


async def validate_unique_root(
    *,
    loaders: Dataloaders,
    branch: str,
    ensure_org_uniqueness: bool,
    group_name: str,
    url: str,
) -> None:
    """
    Validates if the root is unique

    Raises:
        RepeatedRoot: if the root is not unique

    """
    group = await get_group(loaders, group_name)
    organization = await loaders.organization.load(group.organization_id)
    if not organization:
        raise OrganizationNotFound()

    org_roots = await loaders.organization_roots.load(organization.name)
    if (
        ensure_org_uniqueness
        and group.state.type != GroupSubscriptionType.ONESHOT
        and not is_git_unique(
            url=url,
            branch=branch,
            group_name=group_name,
            roots=org_roots,
        )
    ):
        raise RepeatedRoot()


def _validate_env_url(url: str) -> Url:
    try:
        formatted_url = url.strip(" ")
        url_object: Url = parse_url(formatted_url)
        host_regex = re.compile(HOST_PAT, re.UNICODE | re.DOTALL)

        scheme = url_object.scheme
        host = url_object.host

        if scheme not in {"http", "https"}:
            raise InvalidProtocol(scheme or "")

        if host is None or not host_regex.match(host or "") or host in LOCAL_HOSTS:
            raise InvalidUrl()

        return url_object
    except LocationParseError as exc:
        raise InvalidPort() from exc


async def validate_env_url_cspm(
    loaders: Dataloaders,
    environment: RootEnvironmentUrl,
) -> None:
    if environment.state.cloud_name == RootEnvironmentCloud.AWS:
        group = await get_group(loaders, environment.group_name)
        org = await loaders.organization.load(group.organization_id)
        await custom_validations.validate_aws_role(
            environment.url,
            org.state.aws_external_id if org else None,
        )


async def validate_env_url_in_root(loaders: Dataloaders, environment: RootEnvironmentUrl) -> None:
    root_env_urls = await loaders.root_environment_urls.load(
        RootEnvironmentUrlsRequest(group_name=environment.group_name, root_id=environment.root_id),
    )
    if any(
        roots_utils.format_url(environment.url) == roots_utils.format_url(env_url.url)
        for env_url in root_env_urls
        if env_url.url
    ):
        raise RepeatedRootEnvironmentUrl()


async def validate_env_url(
    loaders: Dataloaders,
    environment: RootEnvironmentUrl,
) -> None:
    if (
        environment.state.cloud_name
        and environment.state.url_type is not RootEnvironmentUrlType.CSPM
    ):
        raise InvalidParameter("cloudName")

    if not environment.url:
        raise InvalidParameter("url")

    await validate_env_url_cspm(loaders, environment)

    if environment.state.url_type is RootEnvironmentUrlType.APK:
        validate_filename(environment.url.lower())
    elif environment.state.url_type is RootEnvironmentUrlType.URL:
        await validate_env_url_in_root(loaders, environment)
        url_attributes = _validate_env_url(environment.url)
        if not bool(url_attributes.host):
            raise InvalidParameter("url")
