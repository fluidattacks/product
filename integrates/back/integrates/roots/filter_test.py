from integrates.custom_utils.roots import get_active_git_root, get_active_git_roots
from integrates.dataloaders import Dataloaders, get_new_context
from integrates.db_model.events.enums import (
    EventStateStatus,
    EventType,
)
from integrates.roots.filter import filter_roots_unsolved_events
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    EMAIL_GENERIC,
    EventFaker,
    EventStateFaker,
    GitRootFaker,
    GitRootStateFaker,
    GroupFaker,
    OrganizationFaker,
)
from integrates.testing.mocks import mocks
from integrates.testing.utils import parametrize

ORG_ID = "ORG#dcb069b4-dd78-2180-460e-05f8b353b7bc"
EVENT_DESCRIPTION = "Lorem ipsum dolor sit amet"


@parametrize(
    args=[
        "group_name",
        "specific_type",
        "expected_filtered_root_ids",
        "expected_filtered_out_roots_ids",
    ],
    cases=[
        ["group1", None, [], ["root1"]],
        ["group2", EventType.CLONING_ISSUES, ["root3", "root4"], ["root2"]],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            groups=[
                GroupFaker(name="group1", organization_id=ORG_ID),
                GroupFaker(name="group2", organization_id=ORG_ID),
            ],
            organizations=[OrganizationFaker(id=ORG_ID, name="org1")],
            roots=[
                GitRootFaker(
                    group_name="group1",
                    id="root1",
                    organization_name="org1",
                    state=GitRootStateFaker(url="url1"),
                ),
                GitRootFaker(
                    group_name="group2",
                    id="root2",
                    organization_name="org1",
                    state=GitRootStateFaker(url="url2"),
                ),
                GitRootFaker(
                    group_name="group2",
                    id="root3",
                    organization_name="org1",
                    state=GitRootStateFaker(url="url3"),
                ),
                GitRootFaker(
                    group_name="group2",
                    id="root4",
                    organization_name="org1",
                    state=GitRootStateFaker(url="url4"),
                ),
            ],
            events=[
                EventFaker(
                    group_name="group1",
                    id="event1",
                    root_id="root1",
                    description=EVENT_DESCRIPTION,
                ),
                EventFaker(group_name="group2", id="event2", root_id="root2"),
                EventFaker(
                    group_name="group2",
                    id="event3",
                    root_id="root2",
                    type=EventType.CLONING_ISSUES,
                    description=EVENT_DESCRIPTION,
                ),
                EventFaker(group_name="group2", id="event4", root_id="root3"),
                EventFaker(
                    group_name="group2",
                    id="event5",
                    root_id="root4",
                    state=EventStateFaker(status=EventStateStatus.SOLVED),
                    type=EventType.CLONING_ISSUES,
                ),
            ],
        ),
    )
)
async def test_filter_roots_unsolved_events(
    group_name: str,
    specific_type: EventType | None,
    expected_filtered_root_ids: list[str],
    expected_filtered_out_roots_ids: list[str],
) -> None:
    loaders: Dataloaders = get_new_context()
    active_git_roots = await get_active_git_roots(loaders, group_name)
    filtered_roots = await filter_roots_unsolved_events(
        loaders=loaders,
        roots=active_git_roots,
        group_name=group_name,
        modified_by=EMAIL_GENERIC,
        specific_type=specific_type,
    )

    filtered_root_ids = sorted([root.id for root in filtered_roots])
    assert filtered_root_ids == expected_filtered_root_ids
    filtered_out_root_ids = sorted(
        [root.id for root in active_git_roots if root.id not in filtered_root_ids]
    )
    assert filtered_out_root_ids == expected_filtered_out_roots_ids

    loaders = get_new_context()
    for root_id in filtered_out_root_ids:
        root = await get_active_git_root(loaders, root_id, group_name)
        assert root.cloning.reason == EVENT_DESCRIPTION
