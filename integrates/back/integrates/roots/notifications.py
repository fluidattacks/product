from collections import (
    Counter,
)

from aioextensions import (
    schedule,
)
from graphql import (
    GraphQLResolveInfo,
)

from integrates.custom_exceptions import (
    MailerClientError,
    UnableToSendMail,
)
from integrates.custom_utils import (
    datetime as datetime_utils,
)
from integrates.custom_utils import (
    logs as logs_utils,
)
from integrates.custom_utils.filter_vulnerabilities import (
    filter_non_zero_risk,
    filter_open_vulns,
)
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.roots.enums import (
    RootCloningStatus,
)
from integrates.db_model.roots.types import (
    GitRoot,
    IPRoot,
    Root,
    URLRoot,
)
from integrates.db_model.vulnerabilities.enums import (
    VulnerabilityType,
)
from integrates.db_model.vulnerabilities.types import (
    Vulnerability,
)
from integrates.decorators import (
    retry_on_exceptions,
)
from integrates.mailer import (
    groups as groups_mail,
)
from integrates.roots import (
    update as roots_update,
)


@retry_on_exceptions(
    exceptions=(UnableToSendMail, MailerClientError),
    max_attempts=4,
    sleep_seconds=2,
)
async def send_deactivation_notification(
    *,
    email: str,
    group_name: str,
    loaders: Dataloaders,
    reason: str,
    root: Root,
    root_vulns_non_deleted: list[Vulnerability],
    other: str | None,
) -> None:
    root_vulns_non_deleted = filter_open_vulns(filter_non_zero_risk(root_vulns_non_deleted))
    last_status_update = await roots_update.get_last_status_update(
        loaders,
        root.id,
        root.group_name,
    )
    historic_state_date = last_status_update.modified_date
    root_age = (datetime_utils.get_utc_now() - historic_state_date).days

    last_clone_date_msg: str = "Never cloned"
    last_root_state: str = "Unknown"
    if isinstance(root, GitRoot) and root.cloning.status != RootCloningStatus.UNKNOWN:
        last_clone_date_msg = str(root.cloning.modified_date.date())
        last_root_state = root.cloning.status.value

    sast_vulns = [vuln for vuln in root_vulns_non_deleted if vuln.type == VulnerabilityType.LINES]
    amount_sast_vulns = len(sast_vulns)
    vuln_counter = Counter(
        [
            vulnerability.technique if vulnerability.technique else "PTAAS"
            for vulnerability in root_vulns_non_deleted
        ],
    )

    await groups_mail.send_mail_deactivated_root(
        loaders=loaders,
        activated_by=last_status_update.modified_by,
        group_name=group_name,
        last_clone_date_msg=last_clone_date_msg,
        last_root_state=last_root_state,
        other=other,
        reason=reason,
        root_age=root_age,
        root_nickname=root.state.nickname,
        sast_vulns=-1 if isinstance(root, URLRoot | IPRoot) else amount_sast_vulns,
        vuln_counter=vuln_counter,
        responsible=email,
    )


def report_activity(
    info: GraphQLResolveInfo,
    loaders: Dataloaders,
    root: GitRoot,
    group_name: str,
    user_email: str,
) -> None:
    schedule(
        groups_mail.send_mail_added_root(
            loaders=loaders,
            branch=root.state.branch,
            group_name=group_name,
            health_check=root.state.includes_health_check,
            root_nickname=root.state.nickname,
            root_url=root.state.url,
            responsible=user_email,
            modified_date=root.state.modified_date,
            vpn_required=root.state.use_vpn,
            egress_required=root.state.use_egress,
            ztna_required=root.state.use_ztna,
        ),
    )

    logs_utils.cloudwatch_log(
        info.context,
        "Added Git root in the group",
        extra={
            "group_name": group_name,
            "root_id": root.id,
            "root_nickname": root.state.nickname,
            "user_email": user_email,
            "log_type": "Security",
        },
    )
