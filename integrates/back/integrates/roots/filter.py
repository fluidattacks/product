import logging
import logging.config
from collections.abc import Iterable
from contextlib import suppress
from datetime import datetime

import pytz
from aioextensions import collect

from integrates.custom_exceptions import (
    ErrorUpdatingCredential,
    InvalidAuthorization,
    InvalidGitCredentials,
    InvalidUrl,
    RootAlreadyCloning,
)
from integrates.custom_utils.roots import (
    get_root_unsolved_cloning_issues_events,
    get_unsolved_events_by_root,
)
from integrates.dataloaders import Dataloaders
from integrates.db_model.credentials.types import (
    Credentials,
    CredentialsRequest,
    OauthAzureSecret,
    OauthBitbucketSecret,
    OauthGitlabSecret,
)
from integrates.db_model.events.enums import EventType
from integrates.db_model.roots.enums import RootCloningStatus
from integrates.db_model.roots.types import GitRoot
from integrates.events.domain import add_cloning_issues_event
from integrates.oauth.common import get_credential_token
from integrates.roots import utils as roots_utils
from integrates.settings.logger import LOGGING

logging.config.dictConfig(LOGGING)

LOGGER = logging.getLogger(__name__)


async def filter_active_roots_with_credentials(
    *,
    loaders: Dataloaders,
    roots: Iterable[GitRoot],
    modified_by: str,
) -> tuple[GitRoot, ...]:
    roots_without_creds = tuple(
        root
        for root in roots
        if not (root.state.credential_id or root.state.url.startswith("http"))
    )
    await collect(
        [
            roots_utils.update_root_cloning_status(
                loaders=loaders,
                group_name=root.group_name,
                root_id=root.id,
                status=RootCloningStatus.FAILED,
                message="Invalid credentials",
                modified_by=modified_by,
            )
            for root in roots_without_creds
        ],
        workers=16,
    )

    return tuple(
        root
        for root in roots
        if root.state.credential_id
        # The https public repositories can be cloned without credentials
        or root.state.url.startswith("http")
    )


async def filter_roots_unsolved_events(
    *,
    loaders: Dataloaders,
    roots: Iterable[GitRoot],
    group_name: str,
    modified_by: str,
    specific_type: EventType | None,
) -> tuple[GitRoot, ...]:
    unsolved_events_by_root_id = await get_unsolved_events_by_root(
        loaders,
        group_name,
        specific_type=specific_type,
    )
    await collect(
        [
            roots_utils.update_root_cloning_status(
                loaders=loaders,
                group_name=group_name,
                root_id=root_id,
                status=RootCloningStatus.FAILED,
                message=events[0].description,
                modified_by=modified_by,
            )
            for root_id, events in unsolved_events_by_root_id.items()
        ],
        workers=16,
    )

    return tuple(root for root in roots if root.id not in unsolved_events_by_root_id)


async def filter_roots_already_in_queue(
    roots: Iterable[GitRoot],
) -> tuple[GitRoot, ...]:
    valid_roots = tuple(
        root
        for root in roots
        if root.cloning.status not in (RootCloningStatus.CLONING, RootCloningStatus.QUEUED)
        or (
            datetime.now(tz=pytz.UTC) - root.cloning.modified_date.replace(tzinfo=pytz.UTC)
        ).total_seconds()
        / 60
        > 60
    )
    if not valid_roots:
        raise RootAlreadyCloning()

    return valid_roots


async def _refresh_credential(credential: Credentials, loaders: Dataloaders) -> None:
    with suppress(InvalidAuthorization, InvalidGitCredentials, ErrorUpdatingCredential):
        await get_credential_token(
            credential=credential,
            loaders=loaders,
        )


def _is_synced_with_commit(root: GitRoot, last_commit: str) -> bool:
    cloning_commit = root.cloning.commit
    toe_lines_commit = root.toe_lines.commit if root.toe_lines else None
    rebase_commit = root.rebase.commit if root.rebase else None

    return last_commit == cloning_commit == toe_lines_commit == rebase_commit


async def _validate_root_working_credential(
    *,
    loaders: Dataloaders,
    group_name: str,
    organization_id: str,
    root: GitRoot,
    force: bool,
    modified_by: str,
) -> GitRoot | None:
    credential = (
        await loaders.credentials.load(
            CredentialsRequest(
                id=root.state.credential_id,
                organization_id=organization_id,
            ),
        )
        if root.state.credential_id
        else None
    )
    if credential and isinstance(
        credential.secret,
        (OauthAzureSecret, OauthGitlabSecret, OauthBitbucketSecret),
    ):
        await _refresh_credential(
            credential=credential,
            loaders=loaders,
        )

    if force or root.state.use_vpn or root.state.use_ztna or root.state.use_egress:
        await roots_utils.update_root_cloning_status(
            loaders=loaders,
            group_name=group_name,
            root_id=root.id,
            status=RootCloningStatus.QUEUED,
            message="Cloning queued...",
            modified_by=modified_by,
        )
        return root

    try:
        ls_remote_commit, err_message = await roots_utils.ls_remote_root(root, credential, loaders)
    except InvalidUrl as exc:
        ls_remote_commit = None
        err_message = "Invalid Url"
        LOGGER.exception(
            err_message,
            extra={
                "extra": {"group": root.group_name, "root": root.id, "exc": exc},
            },
        )

    if ls_remote_commit is not None:
        if (
            _is_synced_with_commit(root, ls_remote_commit)
            and await roots_utils.is_in_s3(group_name, root.state.nickname)
            and not await get_root_unsolved_cloning_issues_events(loaders, group_name, root.id)
        ):
            if root.cloning.status != RootCloningStatus.OK:
                await roots_utils.update_root_cloning_status(
                    loaders=loaders,
                    group_name=group_name,
                    root_id=root.id,
                    status=RootCloningStatus.OK,
                    message="The repository was not cloned as no new changes were detected.",
                    commit=root.cloning.commit,
                    modified_by=modified_by,
                )
            return None

        await roots_utils.update_root_cloning_status(
            loaders=loaders,
            group_name=group_name,
            root_id=root.id,
            status=RootCloningStatus.QUEUED,
            message="Cloning queued...",
            modified_by=modified_by,
        )
        return root

    err_message = (
        err_message or "Invalid credentials, access could not be granted prior to the cloning task"
    )
    await roots_utils.update_root_cloning_status(
        loaders=loaders,
        group_name=group_name,
        root_id=root.id,
        status=RootCloningStatus.FAILED,
        message=err_message,
        commit=root.cloning.commit,
        modified_by=modified_by,
    )
    await add_cloning_issues_event(
        loaders=loaders,
        git_root=root,
        message=err_message,
        branch=root.state.branch,
    )

    return None


async def filter_roots_working_creds(
    *,
    loaders: Dataloaders,
    group_name: str,
    organization_id: str,
    roots: Iterable[GitRoot],
    force: bool,
    modified_by: str,
) -> tuple[GitRoot, ...]:
    validated_roots = await collect(
        [
            _validate_root_working_credential(
                loaders=loaders,
                group_name=group_name,
                organization_id=organization_id,
                root=root,
                force=force,
                modified_by=modified_by,
            )
            for root in roots
        ],
        workers=8,
    )

    return tuple(filter(None, validated_roots))
