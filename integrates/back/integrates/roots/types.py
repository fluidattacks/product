from datetime import datetime
from typing import NamedTuple

from integrates.db_model.roots.enums import RootCriticality, RootStateReason


class RootEnvironmentUrlAttributesToUpdate(NamedTuple):
    include: bool | None = None


class RootUrlAttributesToUpdate(NamedTuple):
    excluded_sub_paths: list[str] | None = None
    nickname: str | None = None


class GitRootFileInputs(NamedTuple):
    line_index: int
    original_row: str
    branch: str
    credential_id: str
    git_ignore: list[str]
    includes_health_check: bool
    url: str
    nickname: str
    connection: str
    priority: str


class EditedInfo(NamedTuple):
    modified_date: datetime
    modified_by: str


class GitRootAttributesToAdd(NamedTuple):
    branch: str
    credentials: dict[str, str] | None
    gitignore: list[str]
    includes_health_check: bool
    url: str
    criticality: RootCriticality | None = None
    nickname: str = ""
    other: str | None = None
    reason: RootStateReason | None = None
    use_egress: bool = False
    use_vpn: bool = False
    use_ztna: bool = False


class IpRootAttributesToAdd(NamedTuple):
    address: str
    nickname: str
    other: str | None = None
    reason: RootStateReason | None = None


class UrlRootAttributesToAdd(NamedTuple):
    nickname: str
    url: str
    other: str | None = None
    reason: RootStateReason | None = None
