import logging
import logging.config
from contextlib import suppress
from datetime import datetime
from itertools import groupby
from operator import attrgetter
from typing import Any

from aioextensions import collect, schedule
from urllib3.exceptions import LocationParseError
from urllib3.util import Url, parse_url

from integrates import authz
from integrates.custom_exceptions import (
    InvalidGroupService,
    InvalidParameter,
    InvalidRootType,
    PermissionDenied,
    RepeatedRoot,
    RequiredCredentials,
    ResourceTypeNotFound,
)
from integrates.custom_utils import datetime as datetime_utils
from integrates.custom_utils import roots as roots_utils
from integrates.custom_utils import validations as custom_validations
from integrates.custom_utils import validations_deco as validation_deco_utils
from integrates.custom_utils.groups import get_group
from integrates.dataloaders import Dataloaders
from integrates.db_model import roots as roots_model
from integrates.db_model.credentials.types import CredentialsRequest
from integrates.db_model.groups.types import Group
from integrates.db_model.roots.enums import RootEnvironmentUrlType, RootStatus
from integrates.db_model.roots.get import get_secret_by_key
from integrates.db_model.roots.types import (
    GitRoot,
    GitRootState,
    IPRoot,
    IPRootState,
    Root,
    RootEnvironmentUrl,
    RootEnvironmentUrlsRequest,
    RootRequest,
    RootState,
    Secret,
    URLRoot,
    URLRootState,
)
from integrates.organizations.domain import get_credentials
from integrates.roots import domain, utils, validations
from integrates.roots.types import RootEnvironmentUrlAttributesToUpdate, RootUrlAttributesToUpdate
from integrates.settings import LOGGING
from integrates.toe.inputs import domain as inputs_domain
from integrates.verify.enums import ResourceType

logging.config.dictConfig(LOGGING)
LOGGER = logging.getLogger(__name__)


@validation_deco_utils.validate_fields_deco(["attributes.excluded_sub_paths"])
@validation_deco_utils.validate_sanitized_csv_input_deco(["attributes.excluded_sub_paths"])
async def update_url_root(
    *,
    attributes: RootUrlAttributesToUpdate,
    loaders: Dataloaders,
    user_email: str,
    group_name: str,
    root_id: str,
) -> None:
    root = await loaders.root.load(RootRequest(group_name, root_id))
    if not (isinstance(root, URLRoot) and root.state.status == RootStatus.ACTIVE):
        raise InvalidParameter()
    domain.assign_nickname(
        new_nickname=attributes.nickname or root.state.nickname,
        nickname=root.state.nickname,
        _roots=await loaders.group_roots.load(group_name),
    )
    formatted_excluded_sub_paths = utils.format_excluded_sub_paths(attributes.excluded_sub_paths)
    validations.validate_excluded_sub_paths(root, formatted_excluded_sub_paths)
    new_state: URLRootState = URLRootState(
        excluded_sub_paths=root.state.excluded_sub_paths
        if attributes.excluded_sub_paths is None
        else formatted_excluded_sub_paths,
        host=root.state.host,
        modified_by=user_email,
        modified_date=datetime_utils.get_utc_now(),
        nickname=attributes.nickname or root.state.nickname,
        other=None,
        path=root.state.path,
        port=root.state.port,
        protocol=root.state.protocol,
        reason=None,
        status=RootStatus.ACTIVE,
    )
    if not utils.has_new_url_root_state(root.state, new_state):
        return

    await roots_model.update_root_state(
        current_value=root.state,
        group_name=group_name,
        root_id=root_id,
        state=new_state,
    )
    base_url = roots_utils.get_root_base_url(root)
    await collect(
        [
            inputs_domain.disallow_toe_inputs_for_environment(
                loaders=loaders,
                group_name=group_name,
                root_id=root_id,
                url=f"{base_url}/{sub_path}",
                modified_by=user_email,
                environment_url=None,
            )
            for sub_path in set(new_state.excluded_sub_paths or [])
            - set(root.state.excluded_sub_paths or [])
        ],
    )

    schedule(
        utils.send_mail_updated_root(
            loaders=loaders,
            group_name=group_name,
            root=root,
            new_state=new_state,
            user_email=user_email,
        ),
    )


async def update_secret(
    *,
    group_name: str,
    resource_id: str,
    resource_type: ResourceType,
    new_secret: Secret,
    loaders: Dataloaders,
) -> None:
    group = await get_group(loaders, group_name)
    current_secret = await get_secret_by_key(
        secret_key=new_secret.key,
        group_name=group.name,
        resource_id=resource_id,
        resource_type=resource_type,
    )

    if resource_type == ResourceType.URL:
        await validations.validate_environment_url_id(resource_id, group_name)
        await roots_model.update_environment_url_secret(
            group_name,
            resource_id,
            new_secret,
            current_secret.state,
        )
    elif resource_type == ResourceType.ROOT:
        await validations.validate_root_id(resource_id, group_name, loaders)
        await roots_model.update_root_secret(resource_id, new_secret, current_secret.state)
    else:
        raise ResourceTypeNotFound()


async def _update_git_root_credentials(
    loaders: Dataloaders,
    organization_id: str,
    credentials: dict[str, str] | None,
    user_email: str,
    url: str,
    branch: str,
    existing_credential_id: str | None,
    use_egress: bool,
    use_vpn: bool,
    use_ztna: bool,
    group_name: str,
) -> tuple[str, str | None]:
    if existing_credential_id and credentials is None:
        raise RequiredCredentials()

    if not credentials:
        return url, None

    if credential_id := credentials.get("id"):
        if credential_id != existing_credential_id:
            (
                redirected_url,
                existing_credential,
            ) = await domain._get_credentials_to_add_and_redirected_url(
                loaders=loaders,
                organization_id=organization_id,
                credentials=credentials,
                user_email=user_email,
                url=url,
                branch=branch,
                use_egress=use_egress,
                use_vpn=use_vpn,
                use_ztna=use_ztna,
                required_credentials=True,
                group_name=group_name,
            )
            if existing_credential:
                return redirected_url, existing_credential.id

        if not use_vpn and not use_ztna and not use_egress:
            redirected_url = await validations.validate_and_get_redirected_url(
                repo_url=url,
                secret=(
                    await get_credentials(
                        loaders,
                        existing_credential_id,
                        organization_id,
                    )
                ).secret
                if existing_credential_id
                else None,
                loaders=loaders,
                group_name=group_name,
                credential_id=existing_credential_id,
                organization_id=organization_id,
            )
            return redirected_url, existing_credential_id
        return url, existing_credential_id

    redirected_url, new_credential = await domain._get_credentials_to_add_and_redirected_url(
        loaders=loaders,
        organization_id=organization_id,
        credentials=credentials,
        user_email=user_email,
        url=url,
        branch=branch,
        use_egress=use_egress,
        use_vpn=use_vpn,
        use_ztna=use_ztna,
        required_credentials=True,
        group_name=group_name,
    )
    if new_credential:
        return redirected_url, new_credential.id

    return url, None


async def _check_url_changes(
    *,
    loaders: Dataloaders,
    user_email: str,
    branch: str,
    group_name: str,
    url: str,
    old_url: str,
    org_name: str,
    root_id: str,
    old_branch: str,
) -> None:
    enforcer = await authz.get_group_level_enforcer(loaders, user_email)
    encoded_url = utils.quote_url(url)
    formatted_url = utils.unquote_url(encoded_url)

    url_or_branch_changed = (
        formatted_url != utils.unquote_url(old_url)
        or branch.strip().lower() != old_branch.strip().lower()
    )

    if url_or_branch_changed:
        if not enforcer(group_name, "update_git_root_edit_url"):
            raise PermissionDenied("Edit URL")

        organization_roots = await loaders.organization_roots.load(org_name)

        custom_validations.validate_sanitized_csv_input(*[encoded_url])

        if not validations.is_git_unique(
            url=url,
            branch=branch,
            group_name=group_name,
            roots=organization_roots,
            root_id=root_id,
        ):
            raise RepeatedRoot()


async def _check_health_check_changes(
    *,
    group: Group,
    includes_health_check: bool,
    root: GitRoot,
    user_email: str,
) -> None:
    health_check_changed: bool = includes_health_check != root.state.includes_health_check

    if health_check_changed:
        service_enforcer = authz.get_group_service_attributes_enforcer(group)
        if includes_health_check and not service_enforcer("has_advanced"):
            raise InvalidGroupService()
        if includes_health_check:
            await domain.notify_health_check(
                group_name=group.name,
                root=root,
                user_email=user_email,
            )


async def _update_credentials_and_get_git_root_new_state(
    *,
    loaders: Dataloaders,
    root: GitRoot,
    organization_id: str,
    user_email: str,
    **kwargs: Any,
) -> GitRootState:
    credentials: dict[str, str] | None = kwargs.get("credentials")
    new_nickname: str = str(kwargs.get("nickname", ""))
    url: str = str(kwargs.get("url")).strip()
    formatted_url = utils.unquote_url(utils.quote_url(url))
    url_changed = formatted_url != utils.unquote_url(root.state.url)
    if url_changed:
        url = utils.format_git_repo_url(url)

    redirected_url, credential_id = await _update_git_root_credentials(
        loaders=loaders,
        organization_id=organization_id,
        credentials=credentials,
        user_email=user_email,
        url=url,
        branch=str(kwargs.get("branch")).strip(),
        existing_credential_id=root.state.credential_id,
        use_egress=kwargs.get("use_egress", root.state.use_egress),
        use_vpn=kwargs.get("use_vpn", root.state.use_vpn),
        use_ztna=kwargs.get("use_ztna", root.state.use_ztna),
        group_name=kwargs.get("group_name", ""),
    )
    if redirected_url != url:
        url = redirected_url
        formatted_url = utils.unquote_url(utils.quote_url(url))
        url_changed = formatted_url != utils.unquote_url(root.state.url)
        if url_changed:
            url = utils.format_git_repo_url(url)

    if (
        url_changed
        and (credential_id or root.state.credential_id)
        and not kwargs.get("use_egress", root.state.use_egress)
        and not kwargs.get("use_vpn", root.state.use_vpn)
        and not kwargs.get("use_ztna", root.state.use_ztna)
    ):
        organization_credential = await loaders.credentials.load(
            CredentialsRequest(
                id=credential_id or root.state.credential_id or "",
                organization_id=organization_id,
            ),
        )
        await validations.working_credentials(
            url,
            str(kwargs.get("branch")).strip(),
            organization_credential,
            loaders,
            follow_redirects=False,
        )

    roots: list[Root] = await loaders.group_roots.load(root.group_name)
    nickname = domain.assign_nickname(
        nickname=root.state.nickname,
        new_nickname=utils.format_root_nickname(new_nickname, str(kwargs.get("url")).strip()),
        _roots=roots,
    )

    nickname = await utils.ensure_unique_nickname_for_update(
        loaders=loaders,
        user_email=user_email,
        nickname=nickname,
        roots=roots,
        url=url,
        root_id=root.id,
        old_nickname=root.state.nickname,
    )

    return GitRootState(
        branch=str(kwargs.get("branch")).strip(),
        credential_id=credential_id,
        criticality=kwargs.get("criticality", root.state.criticality),
        gitignore=kwargs.get("gitignore", root.state.gitignore),
        includes_health_check=kwargs.get("includes_health_check", False),
        modified_by=user_email,
        modified_date=datetime_utils.get_utc_now(),
        nickname=nickname,
        other=None,
        reason=None,
        status=root.state.status,
        url=url,
        use_egress=kwargs.get("use_egress", root.state.use_egress),
        use_vpn=kwargs.get("use_vpn", root.state.use_vpn),
        use_ztna=kwargs.get("use_ztna", root.state.use_ztna),
    )


@validation_deco_utils.validate_fields_deco(["url"])
@validations.validate_url_branch_deco(url_field="url", branch_field="branch")
async def update_git_root(
    loaders: Dataloaders,
    user_email: str,
    group: Group,
    **kwargs: Any,
) -> Root:
    group_name = group.name
    root_id: str = str(kwargs.get("id"))
    root = await roots_utils.get_active_git_root(loaders, root_id, group_name)

    new_state = await _update_credentials_and_get_git_root_new_state(
        loaders=loaders,
        root=root,
        organization_id=group.organization_id,
        user_email=user_email,
        **kwargs,
    )

    await _check_url_changes(
        loaders=loaders,
        user_email=user_email,
        branch=new_state.branch,
        group_name=group_name,
        url=new_state.url,
        old_url=root.state.url,
        org_name=root.organization_name,
        root_id=root.id,
        old_branch=root.state.branch,
    )

    await _check_health_check_changes(
        group=group,
        includes_health_check=new_state.includes_health_check,
        root=root,
        user_email=user_email,
    )

    validations.validate_git_root_url(root, new_state.gitignore)
    validations.validate_exclusions(new_state.gitignore)
    await validations.validate_gitignore_update_permission(
        loaders=loaders,
        gitignore=new_state.gitignore,
        group_name=group_name,
        root=root,
        user_email=user_email,
    )

    await roots_model.update_root_state(
        current_value=root.state,
        group_name=group_name,
        root_id=root_id,
        state=new_state,
    )

    schedule(
        utils.send_mail_updated_root(
            loaders=loaders,
            group_name=group_name,
            root=root,
            new_state=new_state,
            user_email=user_email,
        ),
    )

    return GitRoot(
        created_by=root.created_by,
        created_date=root.created_date,
        cloning=root.cloning,
        group_name=root.group_name,
        id=root.id,
        organization_name=root.organization_name,
        state=new_state,
        type=root.type,
        unreliable_indicators=root.unreliable_indicators,
    )


async def update_ip_root(
    *,
    loaders: Dataloaders,
    user_email: str,
    group_name: str,
    root_id: str,
    nickname: str,
) -> None:
    root = await loaders.root.load(RootRequest(group_name, root_id))
    if not (isinstance(root, IPRoot) and root.state.status == RootStatus.ACTIVE):
        raise InvalidParameter()

    if nickname == root.state.nickname:
        return

    domain.assign_nickname(
        new_nickname=nickname,
        nickname="",
        _roots=await loaders.group_roots.load(group_name),
    )
    new_state: IPRootState = IPRootState(
        address=root.state.address,
        modified_by=user_email,
        modified_date=datetime_utils.get_utc_now(),
        nickname=nickname,
        other=None,
        reason=None,
        status=root.state.status,
    )

    await roots_model.update_root_state(
        current_value=root.state,
        group_name=group_name,
        root_id=root_id,
        state=new_state,
    )

    schedule(
        utils.send_mail_updated_root(
            loaders=loaders,
            group_name=group_name,
            root=root,
            new_state=new_state,
            user_email=user_email,
        ),
    )


async def get_last_status_update(loaders: Dataloaders, root_id: str, group_name: str) -> RootState:
    """
    Returns the state item where the status last changed

    ACTIVE, [ACTIVE], INACTIVE, ACTIVE
    """
    historic_state = await loaders.root_historic_state.load(
        RootRequest(group_name=group_name, root_id=root_id),
    )
    status_changes = tuple(
        tuple(group) for _, group in groupby(historic_state, key=attrgetter("status"))
    )
    with_current_status = status_changes[-1]

    return with_current_status[0]


async def get_last_status_update_date(
    loaders: Dataloaders,
    root_id: str,
    group_name: str,
) -> datetime:
    """Returns the date where the status last changed"""
    last_status_update = await get_last_status_update(loaders, root_id, group_name)

    return last_status_update.modified_date


async def update_root_environment_url(
    *,
    attributes: RootEnvironmentUrlAttributesToUpdate,
    group_name: str,
    loaders: Dataloaders,
    modified_by: str,
    root_id: str,
    url_id: str,
) -> None:
    root = await roots_utils.get_root(loaders, root_id, group_name)
    if not isinstance(root, GitRoot):
        raise InvalidRootType()
    validations.validate_active_root(root=root)
    environment_url = await domain.get_environment_url(loaders, root_id, group_name, url_id)
    match attributes.include:
        case False:
            await inputs_domain.disallow_toe_inputs_for_environment(
                loaders=loaders,
                group_name=group_name,
                root_id=root_id,
                url=environment_url.url,
                modified_by=modified_by,
                environment_url=environment_url,
            )
            await roots_model.update_root_environment_url_state(
                current_value=environment_url.state,
                root_id=root_id,
                url_id=url_id,
                group_name=group_name,
                state=environment_url.state._replace(
                    modified_date=datetime_utils.get_now(),
                    modified_by=modified_by,
                    include=attributes.include,
                ),
            )
            loaders.root_environment_urls.clear(
                RootEnvironmentUrlsRequest(
                    root_id=environment_url.root_id,
                    group_name=environment_url.group_name,
                ),
            )
            await _exclude_root_sub_environments(
                environment_url,
                group_name,
                loaders,
                modified_by,
                root_id,
            )
        case True:
            await validations.validate_excluded_paths(environment_url, loaders, root_id, group_name)
            await roots_model.update_root_environment_url_state(
                current_value=environment_url.state,
                root_id=root_id,
                url_id=url_id,
                group_name=group_name,
                state=environment_url.state._replace(
                    modified_date=datetime_utils.get_now(),
                    modified_by=modified_by,
                    include=attributes.include,
                ),
            )

    loaders.root_toe_inputs.clear_all()


async def _exclude_root_sub_environments(
    environment_url: RootEnvironmentUrl,
    group_name: str,
    loaders: Dataloaders,
    modified_by: str,
    root_id: str,
) -> None:
    formatted_url = f"{environment_url.url.strip().removesuffix('/')}"

    async def _exclude_root_sub_environment(
        included_environment_url: RootEnvironmentUrl,
    ) -> None:
        if (
            not included_environment_url.url.strip().startswith(formatted_url)
            and len(await inputs_domain.get_environment_urls(loaders, included_environment_url)) > 1
        ):
            return
        await inputs_domain.disallow_toe_inputs_for_environment(
            loaders=loaders,
            group_name=group_name,
            root_id=root_id,
            url=included_environment_url.url,
            modified_by=modified_by,
            environment_url=included_environment_url,
        )
        await roots_model.update_root_environment_url_state(
            current_value=included_environment_url.state,
            root_id=root_id,
            url_id=included_environment_url.id,
            group_name=group_name,
            state=environment_url.state._replace(
                modified_date=datetime_utils.get_now(),
                modified_by=modified_by,
                include=False,
            ),
        )
        loaders.root_environment_urls.clear(
            RootEnvironmentUrlsRequest(
                root_id=included_environment_url.root_id,
                group_name=included_environment_url.group_name,
            ),
        )

    environment_urls = await loaders.root_environment_urls.load(
        RootEnvironmentUrlsRequest(root_id=root_id, group_name=group_name),
    )
    formatted_urls = [formatted_url]
    if (
        environment_url.state.url_type is RootEnvironmentUrlType.URL
        and len(await inputs_domain.get_environment_urls(loaders, environment_url)) == 1
    ):
        with suppress(LocationParseError):
            parsed_url = parse_url(environment_url.url)
            url = Url(
                scheme=None,
                auth=parsed_url.auth,
                host=parsed_url.host,
                port=parsed_url.port,
                path=parsed_url.path,
                query=parsed_url.query,
                fragment=parsed_url.fragment,
            ).url
            formatted_urls = [
                f"{protocol}{url}".rstrip("/") for protocol in ["http://", "https://"]
            ]

    await collect(
        [
            _exclude_root_sub_environment(env_url)
            for env_url in environment_urls
            if env_url.state.include
            and env_url.id != environment_url.id
            and any(env_url.url.strip().startswith(env) for env in formatted_urls)
        ],
    )
