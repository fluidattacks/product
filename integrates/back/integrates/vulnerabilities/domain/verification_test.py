from decimal import Decimal

import integrates.db_model.vulnerabilities.get as vulns_model_get
from integrates.dataloaders import Dataloaders, get_new_context
from integrates.db_model.vulnerabilities.enums import (
    VulnerabilityStateStatus,
    VulnerabilityVerificationStatus,
)
from integrates.db_model.vulnerabilities.types import VulnerabilityUnreliableIndicators
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    VulnerabilityFaker,
    VulnerabilityStateFaker,
    VulnerabilityVerificationFaker,
)
from integrates.testing.mocks import Mock, mocks
from integrates.testing.utils import parametrize
from integrates.vulnerabilities.domain.verification import get_efficacy


@parametrize(
    args=["vuln_id", "expected"],
    cases=[
        ["be09edb7-cd5c-47ed-bee4-97c645acdce10", Decimal(0)],
        ["be09edb7-cd5c-47ed-bee4-97c645acdce11", Decimal(0)],
        ["be09edb7-cd5c-47ed-bee4-97c645acdce12", Decimal(10)],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            vulnerabilities=[
                VulnerabilityFaker(id="be09edb7-cd5c-47ed-bee4-97c645acdce10"),
                VulnerabilityFaker(
                    id="be09edb7-cd5c-47ed-bee4-97c645acdce11",
                    state=VulnerabilityStateFaker(status=VulnerabilityStateStatus.SAFE),
                    verification=VulnerabilityVerificationFaker(
                        status=VulnerabilityVerificationStatus.REQUESTED,
                    ),
                ),
                VulnerabilityFaker(
                    id="be09edb7-cd5c-47ed-bee4-97c645acdce12",
                    state=VulnerabilityStateFaker(status=VulnerabilityStateStatus.SAFE),
                    unreliable_indicators=VulnerabilityUnreliableIndicators(
                        unreliable_reattack_cycles=10,
                    ),
                    verification=VulnerabilityVerificationFaker(
                        status=VulnerabilityVerificationStatus.VERIFIED,
                    ),
                ),
            ],
        ),
    )
)
async def test_get_efficacy(
    vuln_id: str,
    expected: Decimal,
) -> None:
    loaders: Dataloaders = get_new_context()
    vuln = await loaders.vulnerability.load(vuln_id)
    assert vuln

    result = await get_efficacy(loaders, vuln)
    assert result == expected


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            vulnerabilities=[
                VulnerabilityFaker(
                    id="be09edb7-cd5c-47ed-bee4-97c645acdce13",
                    state=VulnerabilityStateFaker(status=VulnerabilityStateStatus.SAFE),
                    unreliable_indicators=VulnerabilityUnreliableIndicators(),
                    verification=VulnerabilityVerificationFaker(
                        status=VulnerabilityVerificationStatus.VERIFIED,
                    ),
                ),
            ],
        ),
    ),
    others=[
        Mock(
            vulns_model_get,
            "_get_historic_verification",
            "async",
            [
                VulnerabilityVerificationFaker(status=VulnerabilityVerificationStatus.REQUESTED),
                VulnerabilityVerificationFaker(status=VulnerabilityVerificationStatus.REQUESTED),
                VulnerabilityVerificationFaker(status=VulnerabilityVerificationStatus.REQUESTED),
            ],
        )
    ],
)
async def test_get_efficacy_no_unreliable_indicators() -> None:
    loaders: Dataloaders = get_new_context()
    vuln = await loaders.vulnerability.load("be09edb7-cd5c-47ed-bee4-97c645acdce13")
    assert vuln

    result = await get_efficacy(loaders, vuln)
    assert result == Decimal("33.33")
