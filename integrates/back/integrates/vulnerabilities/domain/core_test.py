import random
import string
from datetime import datetime
from decimal import Decimal
from typing import Any

from integrates.custom_exceptions import (
    AlreadyZeroRiskRequested,
    InvalidJustificationMaxLength,
    InvalidParameter,
    InvalidStatusForSeverityUpdate,
    InvalidStatusForSeverityUpdateRequest,
    MissingApprovedEvidence,
    NotZeroRiskRequested,
    PendingEvidenceDraft,
    VulnerabilityHasNotBeenReleased,
    VulnerabilityHasNotBeenSubmitted,
    VulnNotInFinding,
    VulnSeverityScoreUpdateAlreadyRequested,
)
from integrates.custom_utils.datetime import get_utc_now
from integrates.custom_utils.reports import filter_context
from integrates.dataloaders import get_new_context
from integrates.db_model.enums import Source
from integrates.db_model.finding_comments.enums import CommentType
from integrates.db_model.finding_comments.types import FindingCommentsRequest
from integrates.db_model.findings.enums import FindingStatus
from integrates.db_model.findings.types import Finding, FindingEvidence, FindingEvidences
from integrates.db_model.types import SeverityScore
from integrates.db_model.vulnerabilities.enums import (
    VulnerabilitySeverityProposalStatus,
    VulnerabilityStateReason,
    VulnerabilityStateStatus,
    VulnerabilityTechnique,
    VulnerabilityType,
    VulnerabilityVerificationStatus,
    VulnerabilityZeroRiskStatus,
)
from integrates.db_model.vulnerabilities.types import (
    GroupVulnerabilitiesRequest,
    Vulnerability,
    VulnerabilityVerification,
    VulnerabilityZeroRisk,
)
from integrates.finding_comments.domain import get_vuln_nickname
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    DATE_2024,
    FindingFaker,
    FindingUnreliableIndicatorsFaker,
    GitRootFaker,
    GitRootStateFaker,
    GroupFaker,
    IPRootFaker,
    IPRootStateFaker,
    OrganizationFaker,
    ToeLinesFaker,
    ToeLinesStateFaker,
    ToePortFaker,
    VulnerabilityAdvisoryFaker,
    VulnerabilityFaker,
    VulnerabilitySeverityProposalFaker,
    VulnerabilityStateFaker,
    VulnerabilityZeroRiskFaker,
)
from integrates.testing.mocks import Mock, mocks
from integrates.testing.utils import parametrize, raises
from integrates.tickets import domain as tickets_domain
from integrates.vulnerabilities.domain.core import (
    add_comments_by_exclusion,
    approve_severity_score_update,
    confirm_vulnerabilities,
    confirm_vulnerabilities_zero_risk,
    get_grouped_vulnerabilities_info,
    get_reported_vulnerabilities,
    get_vulnerability,
    group_vulnerabilities_info,
    mark_as_safe,
    reject_vulnerabilities,
    reject_vulnerabilities_zero_risk,
    remove_vulnerability,
    remove_vulnerability_tags,
    request_severity_score_update,
    request_vulnerabilities_zero_risk,
    update_description,
    update_severity_score,
)
from integrates.vulnerabilities.types import (
    FindingGroupedVulnerabilitiesInfo,
    GroupedVulnerabilitiesInfo,
    VulnerabilityDescriptionToUpdate,
)

FINDING_ID = "finding-1"
ROOT_ID = "root-1"
ORG_ID = "ORG#dcb069b4-dd78-2180-460e-05f8b353b7bc"
VULN_ID_1 = "be09edb7-cd5c-47ed-bee4-97c645acdce10"
VULN_ID_2 = "335681c3-a6b5-4eec-b828-0d56957ba7c5"
VULN_ID_3 = "be09edb7-cd5c-47ed-bee4-97c645acdce12"
MAX_JUST_LEN = 10000
LONG_STRING = "".join(random.choice(string.ascii_letters) for _ in range(MAX_JUST_LEN + 1))  # noqa: S311


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            vulnerabilities=[
                VulnerabilityFaker(
                    finding_id=FINDING_ID,
                    id="be09edb7-cd5c-47ed-bee4-97c645acdce10",
                    root_id=ROOT_ID,
                    state=VulnerabilityStateFaker(specific="123"),
                ),
                VulnerabilityFaker(
                    finding_id=FINDING_ID,
                    id="be09edb7-cd5c-47ed-bee4-97c645acdce11",
                    root_id=ROOT_ID,
                    state=VulnerabilityStateFaker(
                        status=VulnerabilityStateStatus.SAFE, specific="234"
                    ),
                ),
                VulnerabilityFaker(
                    finding_id=FINDING_ID,
                    id="be09edb7-cd5c-47ed-bee4-97c645acdce12",
                    root_id=ROOT_ID,
                    state=VulnerabilityStateFaker(
                        status=VulnerabilityStateStatus.SAFE, specific="345"
                    ),
                ),
                VulnerabilityFaker(
                    finding_id=FINDING_ID,
                    id="be09edb7-cd5c-47ed-bee4-97c645acdce13",
                    root_id=ROOT_ID,
                    state=VulnerabilityStateFaker(specific="456"),
                ),
            ],
        ),
    )
)
async def test_add_comments_by_exclusion() -> None:
    await add_comments_by_exclusion(
        loaders=get_new_context(),
        user_email="test@test.test",
        vulns=await get_new_context().root_vulnerabilities.load(ROOT_ID),
    )

    loaders = get_new_context()
    consults = await loaders.finding_comments.load(
        FindingCommentsRequest(comment_type=CommentType.COMMENT, finding_id=FINDING_ID)
    )
    loaders = get_new_context()
    assert len(consults) == 1
    assert (
        await get_vuln_nickname(
            loaders, await get_vulnerability(loaders, "be09edb7-cd5c-47ed-bee4-97c645acdce10")
        )
    ) in consults[0].content
    assert (
        await get_vuln_nickname(
            loaders, await get_vulnerability(loaders, "be09edb7-cd5c-47ed-bee4-97c645acdce11")
        )
    ) not in consults[0].content


@parametrize(
    args=["vuln_id"],
    cases=[
        ["be09edb7-cd5c-47ed-bee4-97c645acdce10"],
        ["be09edb7-cd5c-47ed-bee4-97c645acdce11"],
        ["be09edb7-cd5c-47ed-bee4-97c645acdce12"],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            findings=[
                FindingFaker(
                    group_name="testgroup",
                    id=FINDING_ID,
                    unreliable_indicators=FindingUnreliableIndicatorsFaker(
                        unreliable_status=FindingStatus.VULNERABLE
                    ),
                ),
            ],
            vulnerabilities=[
                VulnerabilityFaker(
                    group_name="testgroup",
                    finding_id=FINDING_ID,
                    id="be09edb7-cd5c-47ed-bee4-97c645acdce10",
                    root_id=ROOT_ID,
                    state=VulnerabilityStateFaker(specific="123"),
                    zero_risk=VulnerabilityZeroRisk(
                        comment_id="123456",
                        modified_by="test@gmail.com",
                        modified_date=(datetime.fromisoformat("2018-09-28T15:32:58+00:00")),
                        status=VulnerabilityZeroRiskStatus.REQUESTED,
                    ),
                ),
                VulnerabilityFaker(
                    group_name="testgroup",
                    finding_id=FINDING_ID,
                    id="be09edb7-cd5c-47ed-bee4-97c645acdce11",
                    root_id=ROOT_ID,
                    state=VulnerabilityStateFaker(status=VulnerabilityStateStatus.SUBMITTED),
                ),
                VulnerabilityFaker(
                    group_name="testgroup",
                    finding_id="finding2",
                    id="be09edb7-cd5c-47ed-bee4-97c645acdce12",
                    root_id=ROOT_ID,
                ),
            ],
        ),
    )
)
async def test_request_vulnerabilities_zero_risk_invalid(vuln_id: str) -> None:
    user_info = {"user_email": "group_manager@gmail.com", "first_name": "Jhoe", "last_name": "Done"}

    with raises((AlreadyZeroRiskRequested, VulnerabilityHasNotBeenReleased, VulnNotInFinding)):
        await request_vulnerabilities_zero_risk(
            loaders=get_new_context(),
            finding_id=FINDING_ID,
            vuln_ids=set([vuln_id]),
            user_info=user_info,
            justification="Test justification",
        )


@parametrize(
    args=["vuln_ids", "justification"],
    cases=[
        [{VULN_ID_1, VULN_ID_2}, "Some justification"],
        [{VULN_ID_2, VULN_ID_3}, "Another justification"],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id=ORG_ID)],
            groups=[GroupFaker(name="testgroup", organization_id=ORG_ID)],
            findings=[
                FindingFaker(
                    group_name="testgroup",
                    id=FINDING_ID,
                    unreliable_indicators=FindingUnreliableIndicatorsFaker(
                        unreliable_status=FindingStatus.VULNERABLE
                    ),
                ),
            ],
            vulnerabilities=[
                VulnerabilityFaker(
                    group_name="testgroup",
                    finding_id=FINDING_ID,
                    id=VULN_ID_1,
                    root_id=ROOT_ID,
                    state=VulnerabilityStateFaker(status=VulnerabilityStateStatus.VULNERABLE),
                    type=VulnerabilityType.LINES,
                ),
                VulnerabilityFaker(
                    group_name="testgroup",
                    finding_id=FINDING_ID,
                    id=VULN_ID_2,
                    root_id=ROOT_ID,
                    state=VulnerabilityStateFaker(status=VulnerabilityStateStatus.VULNERABLE),
                    type=VulnerabilityType.PORTS,
                ),
                VulnerabilityFaker(
                    group_name="testgroup",
                    finding_id=FINDING_ID,
                    id=VULN_ID_3,
                    root_id=ROOT_ID,
                    state=VulnerabilityStateFaker(status=VulnerabilityStateStatus.SAFE),
                    type=VulnerabilityType.INPUTS,
                ),
            ],
        ),
    ),
    others=[Mock(tickets_domain, "request_vulnerability_zero_risk", "async", None)],
)
async def test_request_vulnerabilities_zero_risk_success(
    vuln_ids: set[str], justification: str
) -> None:
    user_info = {"user_email": "group_manager@gmail.com", "first_name": "Jhoe", "last_name": "Done"}

    await request_vulnerabilities_zero_risk(
        loaders=get_new_context(),
        finding_id=FINDING_ID,
        vuln_ids=set(vuln_ids),
        user_info=user_info,
        justification=justification,
    )

    loaders = get_new_context()
    vulns = await loaders.vulnerability.load_many(vuln_ids)
    for vuln in vulns:
        assert vuln
        assert vuln.zero_risk
        assert vuln.zero_risk.status == VulnerabilityZeroRiskStatus.REQUESTED
        zero_risk_comments = await loaders.finding_comments.load(
            FindingCommentsRequest(comment_type=CommentType.ZERO_RISK, finding_id=FINDING_ID)
        )
        assert zero_risk_comments[-1].finding_id == FINDING_ID
        assert zero_risk_comments[-1].content == justification
        assert zero_risk_comments[-1].comment_type == CommentType.ZERO_RISK
        assert zero_risk_comments[-1].email == user_info["user_email"]
        # STREAMS_REQUIRED: vulnerabilities historic zero risk entries should be assertion desirable


@parametrize(
    args=["vuln_ids", "finding_id", "modified_by"],
    cases=[
        [[VULN_ID_1, VULN_ID_2], FINDING_ID, "user1@gmail.com"],
        [[VULN_ID_2, VULN_ID_3], FINDING_ID, "user2@gmail.com"],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            findings=[
                FindingFaker(
                    id=FINDING_ID,
                )
            ],
            vulnerabilities=[
                VulnerabilityFaker(
                    id=VULN_ID_1,
                    finding_id=FINDING_ID,
                    state=VulnerabilityStateFaker(status=VulnerabilityStateStatus.SAFE),
                ),
                VulnerabilityFaker(
                    id=VULN_ID_2,
                    finding_id=FINDING_ID,
                    state=VulnerabilityStateFaker(status=VulnerabilityStateStatus.VULNERABLE),
                ),
                VulnerabilityFaker(
                    id=VULN_ID_3,
                    finding_id=FINDING_ID,
                    state=VulnerabilityStateFaker(status=VulnerabilityStateStatus.REJECTED),
                ),
            ],
        )
    )
)
async def test_confirm_vulnerabilities_invalid_vuln_state(
    vuln_ids: set[str],
    finding_id: str,
    modified_by: str,
) -> None:
    loaders = get_new_context()
    with raises(VulnerabilityHasNotBeenSubmitted):
        await confirm_vulnerabilities(
            loaders=loaders, finding_id=finding_id, vuln_ids=vuln_ids, modified_by=modified_by
        )


@parametrize(
    args=["vuln_ids", "finding_id", "modified_by"],
    cases=[[[VULN_ID_1], FINDING_ID, "user1@gmail.com"]],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            findings=[
                FindingFaker(
                    id=FINDING_ID,
                )
            ],
            vulnerabilities=[
                VulnerabilityFaker(
                    id=VULN_ID_1,
                    finding_id=FINDING_ID,
                    state=VulnerabilityStateFaker(status=VulnerabilityStateStatus.SUBMITTED),
                ),
            ],
        )
    )
)
async def test_confirm_vulnerabilities_missing_approved(
    vuln_ids: set[str],
    finding_id: str,
    modified_by: str,
) -> None:
    loaders = get_new_context()
    with raises(MissingApprovedEvidence):
        await confirm_vulnerabilities(
            loaders=loaders, finding_id=finding_id, vuln_ids=vuln_ids, modified_by=modified_by
        )


@parametrize(
    args=["vuln_ids", "finding_id", "modified_by"],
    cases=[[[VULN_ID_1], FINDING_ID, "user1@gmail.com"]],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            findings=[
                FindingFaker(
                    id=FINDING_ID,
                    evidences=FindingEvidences(
                        evidence1=FindingEvidence(
                            description="records",
                            url="evidence-record",
                            modified_date=DATE_2024,
                            is_draft=True,
                        ),
                    ),
                ),
            ],
            vulnerabilities=[
                VulnerabilityFaker(
                    id=VULN_ID_1,
                    finding_id=FINDING_ID,
                    state=VulnerabilityStateFaker(status=VulnerabilityStateStatus.SUBMITTED),
                ),
            ],
        )
    )
)
async def test_confirm_vulnerabilities_pending_draft(
    vuln_ids: set[str],
    finding_id: str,
    modified_by: str,
) -> None:
    loaders = get_new_context()
    with raises(PendingEvidenceDraft):
        await confirm_vulnerabilities(
            loaders=loaders, finding_id=finding_id, vuln_ids=vuln_ids, modified_by=modified_by
        )


@parametrize(
    args=["vuln_ids", "finding_id", "modified_by"],
    cases=[[{VULN_ID_1, VULN_ID_2}, FINDING_ID, "user1@gmail.com"]],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            findings=[
                FindingFaker(
                    id=FINDING_ID,
                    evidences=FindingEvidences(
                        evidence1=FindingEvidence(
                            description="records",
                            url="evidence-record",
                            modified_date=DATE_2024,
                        ),
                    ),
                ),
            ],
            vulnerabilities=[
                VulnerabilityFaker(
                    id=VULN_ID_1,
                    finding_id=FINDING_ID,
                    state=VulnerabilityStateFaker(status=VulnerabilityStateStatus.SUBMITTED),
                ),
                VulnerabilityFaker(
                    id=VULN_ID_2,
                    finding_id=FINDING_ID,
                    state=VulnerabilityStateFaker(status=VulnerabilityStateStatus.SUBMITTED),
                ),
            ],
        )
    )
)
async def test_confirm_vulnerabilities_success(
    vuln_ids: set[str],
    finding_id: str,
    modified_by: str,
) -> None:
    loaders = get_new_context()
    confirmed_vulns = await confirm_vulnerabilities(
        loaders=loaders, finding_id=finding_id, vuln_ids=vuln_ids, modified_by=modified_by
    )
    assert confirmed_vulns
    assert set(vuln.id for vuln in confirmed_vulns) == vuln_ids

    # STREAMS_REQUIRED
    # findings unreliable indicators update should be asserted but this cannot be done until
    # streams is enabled for the test environment

    loaders.vulnerability.clear_all()

    for vuln_id in vuln_ids:
        vulnerability = await loaders.vulnerability.load(vuln_id)
        assert vulnerability
        assert vulnerability.state.status == VulnerabilityStateStatus.VULNERABLE
        assert vulnerability.state.modified_by == modified_by


@parametrize(
    args=["vuln_ids", "finding_id", "modified_by", "reasons", "other_reason"],
    cases=[
        [
            {VULN_ID_1, VULN_ID_2},
            FINDING_ID,
            "user1@gmail.com",
            {VulnerabilityStateReason.OTHER},
            None,
        ],
    ],
)
@mocks()
async def test_reject_vulnerabilities_fails_other_reason(
    vuln_ids: set[str],
    finding_id: str,
    modified_by: str,
    reasons: set[VulnerabilityStateReason],
    other_reason: str | None,
) -> None:
    loaders = get_new_context()
    with raises(InvalidParameter, match="other_reason"):
        await reject_vulnerabilities(
            loaders=loaders,
            finding_id=finding_id,
            vuln_ids=vuln_ids,
            modified_by=modified_by,
            reasons=reasons,
            other_reason=other_reason,
        )


@parametrize(
    args=["vuln_ids", "finding_id", "modified_by", "reasons", "other_reason"],
    cases=[
        [
            {VULN_ID_1, VULN_ID_2},
            FINDING_ID,
            "user1@gmail.com",
            {VulnerabilityStateReason.OMISSION},
            "a justification which should not exist",
        ],
    ],
)
@mocks()
async def test_reject_vulnerabilities_fails_justification(
    vuln_ids: set[str],
    finding_id: str,
    modified_by: str,
    reasons: set[VulnerabilityStateReason],
    other_reason: str | None,
) -> None:
    loaders = get_new_context()
    with raises(InvalidParameter, match="justification"):
        await reject_vulnerabilities(
            loaders=loaders,
            finding_id=finding_id,
            vuln_ids=vuln_ids,
            modified_by=modified_by,
            reasons=reasons,
            other_reason=other_reason,
        )


@parametrize(
    args=["vuln_ids", "finding_id", "modified_by", "reasons"],
    cases=[
        [
            {VULN_ID_1, VULN_ID_2},
            FINDING_ID,
            "user1@gmail.com",
            {VulnerabilityStateReason.OTHER},
        ],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            findings=[
                FindingFaker(id=FINDING_ID),
            ],
            vulnerabilities=[
                VulnerabilityFaker(
                    id=VULN_ID_1,
                    state=VulnerabilityStateFaker(status=VulnerabilityStateStatus.SUBMITTED),
                    finding_id=FINDING_ID,
                ),
                VulnerabilityFaker(
                    id=VULN_ID_2,
                    state=VulnerabilityStateFaker(status=VulnerabilityStateStatus.SUBMITTED),
                    finding_id=FINDING_ID,
                ),
            ],
        )
    )
)
async def test_reject_vulnerabilities_fails_justification_len(
    vuln_ids: set[str],
    finding_id: str,
    modified_by: str,
    reasons: set[VulnerabilityStateReason],
) -> None:
    loaders = get_new_context()
    with raises(InvalidJustificationMaxLength):
        await reject_vulnerabilities(
            loaders=loaders,
            finding_id=finding_id,
            vuln_ids=vuln_ids,
            modified_by=modified_by,
            reasons=reasons,
            other_reason=LONG_STRING,
        )


@parametrize(
    args=["vuln_ids", "finding_id", "modified_by", "reasons", "other_reason"],
    cases=[
        [
            {VULN_ID_1, VULN_ID_2},
            FINDING_ID,
            "user1@gmail.com",
            {VulnerabilityStateReason.OTHER},
            "a convincing justification",
        ],
        [
            {VULN_ID_1, VULN_ID_2},
            FINDING_ID,
            "user1@gmail.com",
            {VulnerabilityStateReason.DUPLICATED},
            None,
        ],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            findings=[
                FindingFaker(id=FINDING_ID),
            ],
            vulnerabilities=[
                VulnerabilityFaker(
                    id=VULN_ID_1,
                    state=VulnerabilityStateFaker(status=VulnerabilityStateStatus.SUBMITTED),
                    finding_id=FINDING_ID,
                ),
                VulnerabilityFaker(
                    id=VULN_ID_2,
                    state=VulnerabilityStateFaker(status=VulnerabilityStateStatus.SUBMITTED),
                    finding_id=FINDING_ID,
                ),
            ],
        )
    )
)
async def test_reject_vulnerabilities_success(
    vuln_ids: set[str],
    finding_id: str,
    modified_by: str,
    reasons: set[VulnerabilityStateReason],
    other_reason: str | None,
) -> None:
    loaders = get_new_context()
    await reject_vulnerabilities(
        loaders=loaders,
        finding_id=finding_id,
        vuln_ids=vuln_ids,
        modified_by=modified_by,
        reasons=reasons,
        other_reason=other_reason,
    )
    loaders.vulnerability.clear_all()
    vulns = await loaders.vulnerability.load_many(vuln_ids)
    for vuln in vulns:
        assert vuln
        assert vuln.state.status == VulnerabilityStateStatus.REJECTED
        assert vuln.state.modified_by == modified_by
        assert vuln.state.reasons is not None
        assert set(vuln.state.reasons) == reasons

    # STREAMS_REQUIRED
    # vulnerabilities historic entries should be asserted, but streams is required


@parametrize(
    args=["vuln_ids", "finding_id", "user_info", "justification"],
    cases=[
        [
            {VULN_ID_1, VULN_ID_2},
            FINDING_ID,
            {"email": "user1@gmail.com"},
            "a convincing justification",
        ],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            findings=[FindingFaker(id=FINDING_ID)],
            vulnerabilities=[
                VulnerabilityFaker(
                    id=VULN_ID_1,
                    state=VulnerabilityStateFaker(status=VulnerabilityStateStatus.SUBMITTED),
                    finding_id=FINDING_ID,
                ),
                VulnerabilityFaker(
                    id=VULN_ID_2,
                    state=VulnerabilityStateFaker(status=VulnerabilityStateStatus.SUBMITTED),
                    finding_id=FINDING_ID,
                ),
            ],
        )
    )
)
async def test_confirm_vulnerabilities_zero_risk_fails_not_requested(
    vuln_ids: set[str],
    finding_id: str,
    user_info: dict[str, Any],
    justification: str,
) -> None:
    loaders = get_new_context()
    with raises(NotZeroRiskRequested):
        await confirm_vulnerabilities_zero_risk(
            loaders=loaders,
            vuln_ids=vuln_ids,
            finding_id=finding_id,
            user_info=user_info,
            justification=justification,
        )


@parametrize(
    args=["vuln_ids", "finding_id", "user_info", "justification"],
    cases=[
        [
            {VULN_ID_1, VULN_ID_2},
            FINDING_ID,
            {
                "user_email": "user1@gmail.com",
                "first_name": "Joe",
                "last_name": "Doe",
            },
            "A convincing justification",
        ],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            findings=[FindingFaker(id=FINDING_ID)],
            vulnerabilities=[
                VulnerabilityFaker(
                    id=VULN_ID_1,
                    state=VulnerabilityStateFaker(status=VulnerabilityStateStatus.VULNERABLE),
                    finding_id=FINDING_ID,
                    zero_risk=VulnerabilityZeroRiskFaker(
                        status=VulnerabilityZeroRiskStatus.REQUESTED
                    ),
                ),
                VulnerabilityFaker(
                    id=VULN_ID_2,
                    state=VulnerabilityStateFaker(status=VulnerabilityStateStatus.VULNERABLE),
                    finding_id=FINDING_ID,
                    zero_risk=VulnerabilityZeroRiskFaker(
                        status=VulnerabilityZeroRiskStatus.REQUESTED
                    ),
                ),
            ],
        )
    )
)
async def test_confirm_vulnerabilities_zero_risk_success(
    vuln_ids: set[str],
    finding_id: str,
    user_info: dict[str, Any],
    justification: str,
) -> None:
    loaders = get_new_context()
    await confirm_vulnerabilities_zero_risk(
        loaders=loaders,
        vuln_ids=vuln_ids,
        finding_id=finding_id,
        user_info=user_info,
        justification=justification,
    )
    loaders.vulnerability.clear_all()
    vulns = await loaders.vulnerability.load_many(vuln_ids)
    new_vulnerable_locations = await loaders.group_vulnerabilities.clear_all().load(
        GroupVulnerabilitiesRequest(
            group_name="test-group",
            state_status=VulnerabilityStateStatus.VULNERABLE,
            paginate=False,
        )
    )

    for vuln in vulns:
        assert vuln
        assert vuln.state.status == VulnerabilityStateStatus.VULNERABLE
        assert vuln.zero_risk
        assert vuln.zero_risk.status == VulnerabilityZeroRiskStatus.CONFIRMED
        assert vuln.zero_risk.modified_by == user_info["user_email"]
        assert vuln not in tuple(edge.node for edge in new_vulnerable_locations.edges)

    zero_risk_comments = await loaders.finding_comments.load(
        FindingCommentsRequest(comment_type=CommentType.ZERO_RISK, finding_id=finding_id)
    )
    assert zero_risk_comments[-1].finding_id == finding_id
    assert zero_risk_comments[-1].content == justification
    assert zero_risk_comments[-1].comment_type == CommentType.ZERO_RISK
    assert zero_risk_comments[-1].email == user_info["user_email"]

    # STREAMS_REQUIRED
    # vulnerabilities historic zero risk entries should be asserted, but streams is required


@parametrize(
    args=["vuln_ids", "finding_id", "user_info", "justification"],
    cases=[
        [
            {VULN_ID_1, VULN_ID_2},
            FINDING_ID,
            {
                "user_email": "user1@gmail.com",
                "first_name": "Joe",
                "last_name": "Doe",
            },
            "A convincing justification",
        ],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            findings=[FindingFaker(id=FINDING_ID)],
            vulnerabilities=[
                VulnerabilityFaker(
                    id=VULN_ID_1,
                    state=VulnerabilityStateFaker(status=VulnerabilityStateStatus.VULNERABLE),
                    finding_id=FINDING_ID,
                ),
                VulnerabilityFaker(
                    id=VULN_ID_2,
                    state=VulnerabilityStateFaker(status=VulnerabilityStateStatus.VULNERABLE),
                    finding_id=FINDING_ID,
                ),
            ],
        )
    )
)
async def test_reject_vulnerabilities_zero_risk_not_requested(
    *,
    vuln_ids: set[str],
    finding_id: str,
    user_info: dict[str, Any],
    justification: str,
) -> None:
    loaders = get_new_context()
    with raises(NotZeroRiskRequested):
        await reject_vulnerabilities_zero_risk(
            loaders=loaders,
            vuln_ids=vuln_ids,
            finding_id=finding_id,
            user_info=user_info,
            justification=justification,
        )


@parametrize(
    args=["vuln_ids", "finding_id", "user_info", "justification"],
    cases=[
        [
            {VULN_ID_1, VULN_ID_2},
            FINDING_ID,
            {
                "user_email": "user1@gmail.com",
                "first_name": "Joe",
                "last_name": "Doe",
            },
            "A convincing justification",
        ],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            findings=[FindingFaker(id=FINDING_ID)],
            vulnerabilities=[
                VulnerabilityFaker(
                    id=VULN_ID_1,
                    state=VulnerabilityStateFaker(status=VulnerabilityStateStatus.VULNERABLE),
                    finding_id=FINDING_ID,
                    zero_risk=VulnerabilityZeroRiskFaker(
                        status=VulnerabilityZeroRiskStatus.REQUESTED
                    ),
                ),
                VulnerabilityFaker(
                    id=VULN_ID_2,
                    state=VulnerabilityStateFaker(status=VulnerabilityStateStatus.VULNERABLE),
                    finding_id=FINDING_ID,
                    zero_risk=VulnerabilityZeroRiskFaker(
                        status=VulnerabilityZeroRiskStatus.REQUESTED
                    ),
                ),
            ],
        )
    )
)
async def test_reject_vulnerabilities_zero_risk_success(
    *,
    vuln_ids: set[str],
    finding_id: str,
    user_info: dict[str, Any],
    justification: str,
) -> None:
    loaders = get_new_context()
    await reject_vulnerabilities_zero_risk(
        loaders=loaders,
        vuln_ids=vuln_ids,
        finding_id=finding_id,
        user_info=user_info,
        justification=justification,
    )

    loaders.vulnerability.clear_all()

    vulnerability = await loaders.vulnerability.load(VULN_ID_1)
    assert vulnerability
    assert vulnerability.state.status == VulnerabilityStateStatus.VULNERABLE
    assert vulnerability.zero_risk
    assert vulnerability.zero_risk.status == VulnerabilityZeroRiskStatus.REJECTED
    zero_risk_comments = await loaders.finding_comments.load(
        FindingCommentsRequest(comment_type=CommentType.ZERO_RISK, finding_id=FINDING_ID)
    )
    assert zero_risk_comments[-1].finding_id == FINDING_ID
    assert zero_risk_comments[-1].content == justification
    assert zero_risk_comments[-1].comment_type == CommentType.ZERO_RISK
    assert zero_risk_comments[-1].email == user_info["user_email"]

    new_vulnerable_locations = await loaders.group_vulnerabilities.clear_all().load(
        GroupVulnerabilitiesRequest(
            group_name="test-group",
            state_status=VulnerabilityStateStatus.VULNERABLE,
            paginate=False,
        )
    )
    assert vulnerability in tuple(edge.node for edge in new_vulnerable_locations.edges)


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            groups=[GroupFaker(name="group1", organization_id="org1")],
            findings=[FindingFaker(id="fin1", group_name="group1")],
            vulnerabilities=[
                VulnerabilityFaker(
                    id="vuln1",
                    cwe_ids=["CWE-123"],
                    state=VulnerabilityStateFaker(status=VulnerabilityStateStatus.VULNERABLE),
                    finding_id=FINDING_ID,
                    stream=["home", "about"],
                    type=VulnerabilityType.INPUTS,
                ),
            ],
        )
    )
)
async def test_mark_as_safe_empty_file() -> None:
    loaders = get_new_context()
    await mark_as_safe(
        user_info={"user_email": "test@test.test"},
        loaders=loaders,
        modified_date=get_utc_now(),
        closed_vulns_ids=["vuln1"],
        vulns_to_close_from_file=[],
        closing_reason=VulnerabilityStateReason.VERIFIED_AS_SAFE,
    )
    loaders.vulnerability.clear_all()

    vulnerability = await loaders.vulnerability.load("vuln1")
    assert vulnerability
    assert vulnerability.state.status == VulnerabilityStateStatus.SAFE
    assert vulnerability.state.modified_by == "test@test.test"
    assert vulnerability.state.reasons == [VulnerabilityStateReason.VERIFIED_AS_SAFE]
    assert vulnerability.severity_score == VulnerabilityFaker().severity_score
    assert vulnerability.stream == ["home", "about"]
    assert vulnerability.cwe_ids == ["CWE-123"]
    assert vulnerability.type == VulnerabilityType.INPUTS


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            groups=[GroupFaker(name="group1", organization_id="org1")],
            findings=[FindingFaker(id="fin1", group_name="group1")],
            vulnerabilities=[
                VulnerabilityFaker(
                    id="vuln1",
                    cwe_ids=["CWE-123"],
                    stream=["home", "about"],
                    type=VulnerabilityType.INPUTS,
                    state=VulnerabilityStateFaker(status=VulnerabilityStateStatus.VULNERABLE),
                    finding_id=FINDING_ID,
                ),
            ],
        )
    )
)
async def test_mark_as_safe() -> None:
    loaders = get_new_context()
    severity_score = SeverityScore(
        base_score=Decimal("10.0"),
        temporal_score=Decimal("10.0"),
        cvss_v3="CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:C/C:H/I:H/A:H",
        cvss_v4="CVSS:4.0/AV:N/AC:L/AT:N/PR:N/UI:N/VC:H/VI:H/VA:H/SC:N/SI:N/SA:N",
        threat_score=Decimal("9.3"),
        cvssf=Decimal("4096.0"),
        cvssf_v4=Decimal("1552.094"),
    )
    await mark_as_safe(
        user_info={"user_email": "test@test.test"},
        loaders=loaders,
        modified_date=get_utc_now(),
        closed_vulns_ids=["vuln1"],
        vulns_to_close_from_file=[
            VulnerabilityFaker(
                cwe_ids=["CWE-456"],
                severity_score=severity_score,
                stream=["home", "abc", "about"],
                type=VulnerabilityType.INPUTS,
            )
        ],
        closing_reason=VulnerabilityStateReason.VERIFIED_AS_SAFE,
    )
    loaders.vulnerability.clear_all()

    vulnerability = await loaders.vulnerability.load("vuln1")
    assert vulnerability
    assert vulnerability.state.status == VulnerabilityStateStatus.SAFE
    assert vulnerability.state.modified_by == "test@test.test"
    assert vulnerability.state.reasons == [VulnerabilityStateReason.VERIFIED_AS_SAFE]
    assert vulnerability.severity_score == severity_score
    assert vulnerability.stream == ["home", "abc", "about"]
    assert vulnerability.cwe_ids == ["CWE-456"]
    assert vulnerability.type == VulnerabilityType.INPUTS


@parametrize(args=["vuln_id"], cases=[["vuln1"], ["vuln2"]])
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            groups=[GroupFaker(name="group1", organization_id="org1")],
            findings=[FindingFaker(id="fin1", group_name="group1")],
            vulnerabilities=[
                VulnerabilityFaker(
                    id="vuln1",
                    type=VulnerabilityType.INPUTS,
                    state=VulnerabilityStateFaker(status=VulnerabilityStateStatus.SAFE),
                    finding_id=FINDING_ID,
                ),
                VulnerabilityFaker(
                    id="vuln2",
                    type=VulnerabilityType.INPUTS,
                    state=VulnerabilityStateFaker(status=VulnerabilityStateStatus.VULNERABLE),
                    finding_id=FINDING_ID,
                ),
            ],
        )
    )
)
async def test_update_severity_score_fail(vuln_id: str) -> None:
    loaders = get_new_context()
    with raises(InvalidStatusForSeverityUpdate):
        await update_severity_score(
            loaders,
            vuln_id,
            "CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:C/C:H/I:H/A:H",
            "CVSS:4.0/AV:N/AC:L/AT:N/PR:N/UI:N/VC:H/VI:H/VA:H/SC:N/SI:N/SA:N",
        )


@parametrize(args=["vuln_id"], cases=[["vuln1"], ["vuln2"]])
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            groups=[GroupFaker(name="group1", organization_id="org1")],
            findings=[FindingFaker(id="fin1", group_name="group1")],
            vulnerabilities=[
                VulnerabilityFaker(
                    id="vuln1",
                    type=VulnerabilityType.INPUTS,
                    state=VulnerabilityStateFaker(status=VulnerabilityStateStatus.SUBMITTED),
                    finding_id=FINDING_ID,
                ),
                VulnerabilityFaker(
                    id="vuln2",
                    type=VulnerabilityType.INPUTS,
                    state=VulnerabilityStateFaker(status=VulnerabilityStateStatus.REJECTED),
                    finding_id=FINDING_ID,
                ),
            ],
        )
    )
)
async def test_update_severity_score_success(vuln_id: str) -> None:
    loaders = get_new_context()
    vector_v3 = "CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:C/C:H/I:H/A:H"
    vector_v4 = "CVSS:4.0/AV:N/AC:L/AT:N/PR:N/UI:N/VC:H/VI:H/VA:H/SC:N/SI:N/SA:N"
    await update_severity_score(
        loaders,
        vuln_id,
        vector_v3,
        vector_v4,
    )
    loaders.vulnerability.clear_all()
    vuln = await loaders.vulnerability.load(vuln_id)
    assert vuln is not None
    assert vuln.severity_score is not None
    assert vuln.severity_score.cvss_v3 == vector_v3
    assert vuln.severity_score.cvss_v4 == vector_v4


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            groups=[GroupFaker(name="group1", organization_id="org1")],
            findings=[FindingFaker(id="fin1", group_name="group1")],
            vulnerabilities=[
                VulnerabilityFaker(id="vuln1", finding_id="fin1", tags=["tag_1"]),
            ],
        )
    )
)
async def test_remove_vulnerability_tags() -> None:
    loaders = get_new_context()
    await remove_vulnerability_tags(
        loaders=loaders, finding_id="fin1", vuln_ids={"vuln1"}, tag_to_remove="tag_1"
    )
    loaders.vulnerability.clear_all()

    vuln = await loaders.vulnerability.load("vuln1")
    assert vuln
    assert vuln.tags == []


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            groups=[GroupFaker(name="group1", organization_id="org1")],
            findings=[FindingFaker(id="fin1", group_name="group1")],
            vulnerabilities=[
                VulnerabilityFaker(id="vuln1", finding_id="fin1", tags=None),
            ],
        )
    )
)
async def test_remove_vulnerability_tags_none() -> None:
    loaders = get_new_context()
    await remove_vulnerability_tags(
        loaders=loaders, finding_id="fin1", vuln_ids={"vuln1"}, tag_to_remove="tag_1"
    )
    loaders.vulnerability.clear_all()

    vuln = await loaders.vulnerability.load("vuln1")
    assert vuln
    assert vuln.tags is None


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            groups=[GroupFaker(name="group1", organization_id="org1")],
            findings=[FindingFaker(id="fin1", group_name="group1")],
            vulnerabilities=[
                VulnerabilityFaker(id="vuln1", finding_id="fin1", tags=["tag_1", "tag_2"]),
            ],
        )
    )
)
async def test_remove_vulnerability_tags_empty() -> None:
    loaders = get_new_context()
    await remove_vulnerability_tags(
        loaders=loaders, finding_id="fin1", vuln_ids={"vuln1"}, tag_to_remove=""
    )
    loaders.vulnerability.clear_all()

    vulnerability = await loaders.vulnerability.load("vuln1")
    assert vulnerability
    assert vulnerability.tags is None


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            vulnerabilities=[
                VulnerabilityFaker(
                    id="vuln1",
                ),
            ],
        )
    )
)
async def test_request_severity_score_update() -> None:
    loaders = get_new_context()
    cvss_vector = "CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:C/C:H/I:H/A:H"
    cvss4_vector = "CVSS:4.0/AV:N/AC:L/AT:N/PR:N/UI:N/VC:H/VI:H/VA:H/SC:N/SI:N/SA:N"
    email = "test@gmail.com"
    await request_severity_score_update(
        loaders=loaders,
        vulnerability_id="vuln1",
        cvss_vector=cvss_vector,
        cvss4_vector=cvss4_vector,
        user_email=email,
    )
    loaders.vulnerability.clear_all()
    vuln = await loaders.vulnerability.load("vuln1")
    assert vuln
    assert vuln.state.proposed_severity is not None
    assert vuln.state.proposed_severity.status == VulnerabilitySeverityProposalStatus.REQUESTED
    assert vuln.state.proposed_severity.severity_score.cvss_v3 == cvss_vector
    assert vuln.state.proposed_severity.severity_score.cvss_v4 == cvss4_vector
    assert vuln.state.proposed_severity.modified_by == email


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            vulnerabilities=[
                VulnerabilityFaker(
                    id="vuln1",
                    state=VulnerabilityStateFaker(status=VulnerabilityStateStatus.SAFE),
                ),
            ],
        )
    )
)
async def test_request_severity_score_update_invalid_status() -> None:
    loaders = get_new_context()
    with raises(InvalidStatusForSeverityUpdateRequest):
        await request_severity_score_update(
            loaders=loaders,
            vulnerability_id="vuln1",
            cvss_vector="CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:C/C:H/I:H/A:H",
            cvss4_vector="CVSS:4.0/AV:N/AC:L/AT:N/PR:N/UI:N/VC:H/VI:H/VA:H/SC:N/SI:N/SA:N",
            user_email="test@gmail.com",
        )


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            vulnerabilities=[
                VulnerabilityFaker(
                    id="vuln1",
                    state=VulnerabilityStateFaker(
                        proposed_severity=VulnerabilitySeverityProposalFaker(
                            status=VulnerabilitySeverityProposalStatus.REQUESTED
                        )
                    ),
                ),
            ],
        )
    )
)
async def test_request_severity_score_update_already_requested() -> None:
    loaders = get_new_context()
    with raises(VulnSeverityScoreUpdateAlreadyRequested):
        await request_severity_score_update(
            loaders=loaders,
            vulnerability_id="vuln1",
            cvss_vector="CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:C/C:H/I:H/A:H",
            cvss4_vector="CVSS:4.0/AV:N/AC:L/AT:N/PR:N/UI:N/VC:H/VI:H/VA:H/SC:N/SI:N/SA:N",
            user_email="test@gmail.com",
        )


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            vulnerabilities=[
                VulnerabilityFaker(
                    id="vuln1",
                    state=VulnerabilityStateFaker(
                        proposed_severity=VulnerabilitySeverityProposalFaker(
                            status=VulnerabilitySeverityProposalStatus.REQUESTED
                        )
                    ),
                    severity_score=SeverityScore(),
                ),
            ],
        )
    )
)
async def test_approve_severity_score_update() -> None:
    loaders = get_new_context()
    email = "user@test.com"
    vuln_id = "vuln1"

    await approve_severity_score_update(
        loaders=loaders, vulnerability_id=vuln_id, modified_by=email
    )
    loaders.vulnerability.clear_all()
    vuln = await loaders.vulnerability.load(vuln_id)
    assert vuln
    assert vuln.state.proposed_severity is not None
    assert vuln.state.proposed_severity.status == VulnerabilitySeverityProposalStatus.APPROVED
    assert vuln.state.proposed_severity.modified_by == email
    assert vuln.severity_score == vuln.state.proposed_severity.severity_score


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            findings=[FindingFaker(id="fin1")],
            vulnerabilities=[
                VulnerabilityFaker(id="vuln1", finding_id="fin1"),
                VulnerabilityFaker(id="vuln2", finding_id="fin1"),
                VulnerabilityFaker(id="vuln3", finding_id="fin1"),
            ],
        )
    )
)
async def test_get_reported_vulns() -> None:
    loaders = get_new_context()
    finding_id = "fin1"

    # Act
    vulns = await get_reported_vulnerabilities(loaders, finding_id)

    # Assert
    assert len(vulns) == 3


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            findings=[FindingFaker(id="fin1")],
            vulnerabilities=[
                VulnerabilityFaker(
                    id="vuln1",
                    finding_id="fin1",
                    state=VulnerabilityStateFaker(status=VulnerabilityStateStatus.SUBMITTED),
                )
            ],
        )
    )
)
async def test_remove_vulnerability() -> None:
    loaders = get_new_context()
    finding_id = "fin1"
    vuln_id = "vuln1"
    email = "reviewer@fluidattacks.com"

    # Act
    await remove_vulnerability(
        loaders=loaders,
        finding_id=finding_id,
        vulnerability_id=vuln_id,
        justification=VulnerabilityStateReason.EXCLUSION,
        email=email,
    )

    # Assert
    loaders.vulnerability.clear_all()
    vuln = await loaders.vulnerability.load(vuln_id)

    assert vuln is None


@parametrize(
    args=["vuln_id", "description", "same_hash"],
    cases=[
        [
            "vuln1",
            VulnerabilityDescriptionToUpdate(where="anotherplace.file", specific="12"),
            False,
        ],
        [
            "vuln2",
            VulnerabilityDescriptionToUpdate(where="anotherplace.file", specific="14"),
            True,
        ],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            findings=[FindingFaker(id="fin1")],
            vulnerabilities=[
                VulnerabilityFaker(
                    vuln_machine_hash=123,
                    id="vuln1",
                    finding_id="fin1",
                    state=VulnerabilityStateFaker(source=Source.MACHINE),
                    vuln_skims_description="vuln found on root",
                    vuln_skims_method="c_sharp.sql_injection",
                    hacker_email="machine@fluidattacks.com",
                ),
                VulnerabilityFaker(vuln_machine_hash=456, id="vuln2", finding_id="fin1"),
            ],
        )
    )
)
async def test_update_description(
    vuln_id: str, description: VulnerabilityDescriptionToUpdate, same_hash: bool
) -> None:
    loaders = get_new_context()
    vulnerability = await get_vulnerability(loaders, vuln_id, clear_loader=True)
    old_hash = vulnerability.hash

    await update_description(loaders, vuln_id, description, "test@test.test")

    vulnerability = await get_vulnerability(loaders, vuln_id, clear_loader=True)
    assert vulnerability.state.where == description.where
    assert vulnerability.state.specific == description.specific
    assert (vulnerability.hash == old_hash) is same_hash


@parametrize(
    args=["finding_id", "expected_group"],
    cases=[
        [
            "988493279",
            FindingGroupedVulnerabilitiesInfo(
                grouped_inputs_vulnerabilities=(),
                grouped_lines_vulnerabilities=(),
                grouped_ports_vulnerabilities=(
                    GroupedVulnerabilitiesInfo(
                        where="192.168.1.19",
                        specific="8888",
                        commit_hash="",
                        severity_score=Decimal("1.1"),
                    ),
                ),
                where="-",
            ),
        ],
        [
            "422286126",
            FindingGroupedVulnerabilitiesInfo(
                grouped_inputs_vulnerabilities=(),
                grouped_lines_vulnerabilities=(
                    GroupedVulnerabilitiesInfo(
                        where=filter_context("test/data/lib_path/f060/csharp.cs"),
                        specific="12",
                        commit_hash="ea871ee",
                        severity_score=Decimal("1.1"),
                    ),
                ),
                grouped_ports_vulnerabilities=(),
                where="-",
            ),
        ],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(id="org1"),
            ],
            groups=[
                GroupFaker(organization_id="org1"),
            ],
            roots=[
                GitRootFaker(
                    id="root1",
                    state=GitRootStateFaker(
                        gitignore=["bower_components/*", "node_modules/*"],
                        includes_health_check=True,
                        nickname="universe",
                        url="https://gitlab.com/fluidattacks/universe",
                    ),
                ),
                IPRootFaker(
                    id="root2",
                    state=IPRootStateFaker(
                        address="192.168.1.19",
                        nickname="universe19",
                    ),
                ),
            ],
            findings=[
                FindingFaker(
                    id="988493279",
                    evidences=FindingEvidences(
                        evidence1=FindingEvidence(
                            description="evidence1",
                            url=("group1-3c475384-834c-47b0-ac71-a41a022e401c-evidence1"),
                            modified_date=datetime.fromisoformat("2020-11-19T13:37:10+00:00"),
                        ),
                    ),
                ),
                FindingFaker(
                    id="422286126",
                    evidences=FindingEvidences(
                        evidence1=FindingEvidence(
                            description="evidence1",
                            url=("group1-3c475384-834c-47b0-ac71-a41a022e401c-evidence1"),
                            modified_date=datetime.fromisoformat("2020-11-19T13:37:10+00:00"),
                        ),
                    ),
                ),
            ],
            toe_lines=[
                ToeLinesFaker(
                    filename="test/data/lib_path/f060/csharp.cs",
                    root_id="root1",
                    state=ToeLinesStateFaker(loc=4324),
                ),
            ],
            toe_ports=[
                ToePortFaker(address="192.168.1.19", port="8888", root_id="root2"),
            ],
            vulnerabilities=[
                VulnerabilityFaker(
                    id="vuln1",
                    finding_id="988493279",
                    root_id="root2",
                    state=VulnerabilityStateFaker(specific="8888", where="192.168.1.19"),
                    type=VulnerabilityType.PORTS,
                    verification=VulnerabilityVerification(
                        modified_by="unittest@fluidattacks.com",
                        modified_date=datetime.fromisoformat("2018-04-08T01:45:11+00:00"),
                        status=VulnerabilityVerificationStatus.REQUESTED,
                    ),
                ),
                VulnerabilityFaker(
                    id="vuln2",
                    finding_id="422286126",
                    root_id="root1",
                    state=VulnerabilityStateFaker(
                        specific="12", where="test/data/lib_path/f060/csharp.cs", commit="ea871ee"
                    ),
                    type=VulnerabilityType.LINES,
                ),
            ],
        ),
    ),
)
async def test_get_grouped_vulnerabilities_info(
    finding_id: str,
    expected_group: FindingGroupedVulnerabilitiesInfo,
) -> None:
    loaders = get_new_context()
    finding = await loaders.finding.load(finding_id)
    assert finding is not None
    test_data = await get_grouped_vulnerabilities_info(loaders, finding)
    assert test_data == expected_group


@parametrize(
    args=[
        "finding_vulns",
        "vuln_type",
        "finding",
        "expected_result",
    ],
    cases=[
        [
            [
                VulnerabilityFaker(
                    created_by="machine@fluidattacks.com",
                    created_date=datetime.now(),
                    finding_id="finding_id",
                    group_name="group",
                    hacker_email="machine@fluidattacks.com",
                    id="123456",
                    organization_name="test",
                    root_id="4039d098-ffc5-4984-8ed3-eb17bca98e19",
                    state=VulnerabilityStateFaker(
                        modified_by="modifier",
                        modified_date=datetime.now(),
                        source=Source.MACHINE,
                        specific="Unsafe Cookie",
                        status=VulnerabilityStateStatus.VULNERABLE,
                        where="http:localhost.com:4000",
                    ),
                    technique=VulnerabilityTechnique.PTAAS,
                    type=VulnerabilityType.INPUTS,
                ),
            ],
            VulnerabilityType.INPUTS,
            FindingFaker(
                id="finding_id",
                evidences=FindingEvidences(
                    evidence1=FindingEvidence(
                        description="evidence1",
                        url=("group1-3c475384-834c-47b0-ac71-a41a022e401c-evidence1"),
                        modified_date=datetime.fromisoformat("2020-11-19T13:37:10+00:00"),
                    ),
                ),
            ),
            (
                GroupedVulnerabilitiesInfo(
                    where="http:localhost.com:4000",
                    specific="Unsafe Cookie",
                    commit_hash="",
                    severity_score=Decimal("1.1"),
                ),
            ),
        ],
        [
            [
                VulnerabilityFaker(
                    created_by="machine@fluidattacks.com",
                    created_date=datetime.now(),
                    finding_id="finding_id",
                    group_name="group",
                    hacker_email="machine@fluidattacks.com",
                    id="123456",
                    organization_name="test",
                    root_id="4039d098-ffc5-4984-8ed3-eb17bca98e19",
                    state=VulnerabilityStateFaker(
                        modified_by="modifier",
                        modified_date=datetime.now(),
                        source=Source.MACHINE,
                        specific="33",
                        status=VulnerabilityStateStatus.VULNERABLE,
                        where="src/package.json",
                        advisories=VulnerabilityAdvisoryFaker(
                            package="ruff",
                            vulnerable_version="2.0.0",
                            cve=["CVE-2024-2362", "GHSA-ty7n-95362g"],
                            epss=50,
                        ),
                        commit="ea871ee",
                    ),
                    technique=VulnerabilityTechnique.SCR,
                    type=VulnerabilityType.LINES,
                ),
                VulnerabilityFaker(
                    created_by="machine@fluidattacks.com",
                    created_date=datetime.now(),
                    finding_id="finding_id",
                    group_name="group",
                    hacker_email="machine@fluidattacks.com",
                    id="123456",
                    organization_name="test",
                    root_id="4039d098-ffc5-4984-8ed3-eb17bca98e19",
                    state=VulnerabilityStateFaker(
                        modified_by="modifier",
                        modified_date=datetime.now(),
                        source=Source.MACHINE,
                        specific="65",
                        status=VulnerabilityStateStatus.VULNERABLE,
                        where="src/package.json",
                        advisories=VulnerabilityAdvisoryFaker(
                            package="react",
                            vulnerable_version="18.0.0",
                            cve=["CVE-2024-6235", "GHSA-ty6d-9g95fg"],
                            epss=None,
                        ),
                        commit="ea871ee",
                    ),
                    technique=VulnerabilityTechnique.SCR,
                    type=VulnerabilityType.LINES,
                ),
            ],
            VulnerabilityType.LINES,
            FindingFaker(
                id="finding_ig",
                evidences=FindingEvidences(
                    evidence1=FindingEvidence(
                        description="evidence1",
                        url=("group1-3c475384-834c-47b0-ac71-a41a022e401c-evidence1"),
                        modified_date=datetime.fromisoformat("2020-11-19T13:37:10+00:00"),
                    ),
                ),
            ),
            (
                GroupedVulnerabilitiesInfo(
                    where=("src/package.json: Package ruff with version 2.0.0 and EPSS 50%"),
                    specific="33",
                    commit_hash="ea871ee",
                    severity_score=Decimal("0.9"),
                ),
                GroupedVulnerabilitiesInfo(
                    where=("src/package.json: Package react with version 18.0.0"),
                    specific="65",
                    commit_hash="ea871ee",
                    severity_score=Decimal("0.9"),
                ),
            ),
        ],
    ],
)
def test_group_vulnerabilities_info_2(
    finding_vulns: list[Vulnerability],
    vuln_type: VulnerabilityType,
    finding: Finding,
    expected_result: tuple[GroupedVulnerabilitiesInfo, ...],
) -> None:
    test_data = group_vulnerabilities_info(finding_vulns, vuln_type, finding)
    assert test_data == expected_result
