from datetime import datetime

from integrates.custom_exceptions import InvalidVulnerabilityAlreadyExists
from integrates.dataloaders import get_new_context
from integrates.db_model.findings.types import FindingEvidence, FindingEvidences
from integrates.db_model.vulnerabilities.enums import VulnerabilityStateStatus, VulnerabilityType
from integrates.db_model.vulnerabilities.types import Vulnerability
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    FindingFaker,
    GitRootFaker,
    GitRootStateFaker,
    GroupFaker,
    OrganizationFaker,
    ToeLinesFaker,
    ToeLinesStateFaker,
    VulnerabilityFaker,
    VulnerabilityStateFaker,
)
from integrates.testing.mocks import mocks
from integrates.testing.utils import parametrize, raises
from integrates.vulnerabilities.domain.rebase import rebase as rebase_vulnerability

VULN_1 = VulnerabilityFaker(
    id="vuln1",
    finding_id="finding1",
    root_id="root1",
    state=VulnerabilityStateFaker(
        status=VulnerabilityStateStatus.VULNERABLE,
        commit="a6288824c036e6a09f6a4ebe44eb2f01fb67eeb6",
        where="path/to/file1.ext",
        specific="50",
    ),
    type=VulnerabilityType.LINES,
)


@parametrize(
    args=["vulnerability", "commit", "where", "specific"],
    cases=[
        [VULN_1, "a6288824c036e6a09f6a4ebe44eb2f01fb67eeb6", "path/to/file1.ext", "51"],
        [VULN_1, "a6288824c036e6a09f6a4ebe44eb2f01fb67eeb7", "path/to/file1.ext", "50"],
        [VULN_1, "a6288824c036e6a09f6a4ebe44eb2f01fb67eeb6", "path/to/file2.ext", "50"],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(id="org1"),
            ],
            groups=[
                GroupFaker(organization_id="org1"),
            ],
            roots=[
                GitRootFaker(
                    id="root1",
                    state=GitRootStateFaker(
                        gitignore=["bower_components/*", "node_modules/*"],
                        includes_health_check=True,
                        nickname="universe",
                        url="https://gitlab.com/fluidattacks/universe",
                    ),
                ),
            ],
            findings=[
                FindingFaker(
                    id="finding1",
                    evidences=FindingEvidences(
                        evidence1=FindingEvidence(
                            description="evidence1",
                            url=("group1-3c475384-834c-47b0-ac71-a41a022e401c-evidence1"),
                            modified_date=datetime.fromisoformat("2020-11-19T13:37:10+00:00"),
                        ),
                    ),
                ),
            ],
            toe_lines=[
                ToeLinesFaker(
                    filename="path/to/file1.ext",
                    root_id="root1",
                    state=ToeLinesStateFaker(loc=4324),
                ),
            ],
            vulnerabilities=[
                VULN_1,
                VulnerabilityFaker(
                    id="vuln2",
                    finding_id="finding1",
                    root_id="root1",
                    state=VulnerabilityStateFaker(
                        status=VulnerabilityStateStatus.VULNERABLE,
                        commit="a6288824c036e6a09f6a4ebe44eb2f01fb67eeb6",
                        where="path/to/file1.ext",
                        specific="51",
                    ),
                    type=VulnerabilityType.LINES,
                ),
                VulnerabilityFaker(
                    id="vuln3",
                    finding_id="finding1",
                    root_id="root1",
                    state=VulnerabilityStateFaker(
                        status=VulnerabilityStateStatus.VULNERABLE,
                        commit="a6288824c036e6a09f6a4ebe44eb2f01fb67eeb7",
                        where="path/to/file1.ext",
                        specific="50",
                    ),
                    type=VulnerabilityType.LINES,
                ),
                VulnerabilityFaker(
                    id="vuln4",
                    finding_id="finding1",
                    root_id="root1",
                    state=VulnerabilityStateFaker(
                        status=VulnerabilityStateStatus.VULNERABLE,
                        commit="a6288824c036e6a09f6a4ebe44eb2f01fb67eeb6",
                        where="path/to/file2.ext",
                        specific="50",
                    ),
                    type=VulnerabilityType.LINES,
                ),
            ],
        )
    ),
)
async def test_rebase_exceptions(
    vulnerability: Vulnerability, commit: str, where: str, specific: str
) -> None:
    loaders = get_new_context()
    finding_vulns = await loaders.finding_vulnerabilities.load(vulnerability.finding_id)
    with raises(
        InvalidVulnerabilityAlreadyExists,
        match="Invalid, vulnerability already exists",
    ):
        await rebase_vulnerability(
            loaders=get_new_context(),
            finding_id=vulnerability.finding_id,
            finding_vulns=tuple(finding_vulns),
            vuln_to_update=vulnerability,
            vulnerability_commit=commit,
            vulnerability_where=where,
            vulnerability_specific=specific,
        )


@parametrize(
    args=["vulnerability", "where", "specific"],
    cases=[
        [VULN_1, "path/to/file1.ext", "51"],
        [VULN_1, "path/to/file2.ext", "50"],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(id="org1"),
            ],
            groups=[
                GroupFaker(organization_id="org1"),
            ],
            roots=[
                GitRootFaker(
                    id="root1",
                    state=GitRootStateFaker(
                        gitignore=["bower_components/*", "node_modules/*"],
                        includes_health_check=True,
                        nickname="universe",
                        url="https://gitlab.com/fluidattacks/universe",
                    ),
                ),
            ],
            findings=[
                FindingFaker(
                    id="finding1",
                    evidences=FindingEvidences(
                        evidence1=FindingEvidence(
                            description="evidence1",
                            url=("group1-3c475384-834c-47b0-ac71-a41a022e401c-evidence1"),
                            modified_date=datetime.fromisoformat("2020-11-19T13:37:10+00:00"),
                        ),
                    ),
                ),
            ],
            toe_lines=[
                ToeLinesFaker(
                    filename="path/to/file1.ext",
                    root_id="root1",
                    state=ToeLinesStateFaker(loc=4324),
                ),
                ToeLinesFaker(
                    filename="path/to/file2.ext",
                    root_id="root1",
                    state=ToeLinesStateFaker(loc=4324),
                ),
            ],
            vulnerabilities=[
                VULN_1,
            ],
        )
    ),
)
async def test_rebase(vulnerability: Vulnerability, where: str, specific: str) -> None:
    loaders = get_new_context()
    finding_vulns = await loaders.finding_vulnerabilities.load(vulnerability.finding_id)

    await rebase_vulnerability(
        loaders=get_new_context(),
        finding_id=vulnerability.finding_id,
        finding_vulns=tuple(finding_vulns),
        vuln_to_update=vulnerability,
        vulnerability_commit=vulnerability.state.commit,
        vulnerability_where=where,
        vulnerability_specific=specific,
    )

    updated_vulnerability = await get_new_context().vulnerability.load(vulnerability.id)

    if not updated_vulnerability:
        return

    assert updated_vulnerability.state.where == where
    assert updated_vulnerability.state.specific == specific
