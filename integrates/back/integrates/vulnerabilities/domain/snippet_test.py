from fluidattacks_core.serializers.snippet import Function, Snippet
from voyageai.client_async import AsyncClient
from voyageai.object.embeddings import EmbeddingsObject

from integrates.custom_exceptions import VulnNotFound
from integrates.dataloaders import get_new_context
from integrates.db_model.vulnerabilities.enums import VulnerabilityStateStatus, VulnerabilityType
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    FindingFaker,
    GroupFaker,
    StakeholderFaker,
    VulnerabilityFaker,
    VulnerabilityStateFaker,
)
from integrates.testing.mocks import Mock, mocks
from integrates.vulnerabilities.domain import snippet
from integrates.vulnerabilities.domain.snippet import (
    get_vulnerable_line_content,
    store_in_open_search,
)

GROUP_NAME = "group1"
EMMBEDING = EmbeddingsObject()
EMMBEDING.embeddings = [[1.0, 2.0, 3.0], [4.0, 5.0, 6.0], [7.0, 8.0, 9.0]]


def test_get_vulnerable_line_content() -> None:
    snippet = Snippet(
        content=(
            "function LimpiarCamposUsu()\n{\n"
            " debugger;\n"
            " thisForm = document.Entrada;\n"
            ' thisForm.txtNombresUsu.value = "";\n'
            ' thisForm.txtApellidosUsu.value = "";\n'
            ' thisForm.txtCedulaUsu.value = "";\n'
            ' thisForm.txtCelularUsu.value = "";\n'
            ' //thisForm.txtTelefonoCasaUsu.value = "";\n'
            ' //thisForm.txtTelefonoOficUsu.value = "";\n'
            ' //thisForm.txtFaxUsu.value = "";\n'
            ' //thisForm.txtPaisUsu.value = "";\n'
            ' //thisForm.txtEstadoUsu.value = "";\n'
            ' //thisForm.txtCiudadUsu.value = "";\n'
            ' //thisForm.txtUrbanizacionUsu.value = "";\n'
            ' //thisForm.txtAvenidaUsu.value = "";\n'
            ' //thisForm.txtEdificioUsu.value = "";\n'
            ' //thisForm.txtNroCasaEdifUsu.value = "";\n'
            ' //thisForm.txtZonaPostalUsu.value = "";\n'
            ' thisForm.txtEmailUsu.value = "";\n'
            " thisForm.lstEstatusUsu.options.selectedIndex = 0;\n"
            " thisForm.dropEmpresa.options.selectedIndex = thisForm.dropEmpresa.length - 1;\n"
            " //thisForm.dropOficina.options.selectedIndex = thisForm.dropOficina.length - 1;\n"
            " thisForm.dropOficina.remove;\n"
            " return false;\n}"
        ),
        offset=372,
        line=375,
        column=None,
        columns_per_line=120,
        line_context=-1,
        wrap=False,
        show_line_numbers=False,
        highlight_line_number=False,
        start_point=None,
        end_point=None,
        is_function=True,
        function=Function(
            name="LimpiarCamposUsu", node_type="function_declaration", field_identifier_name=None
        ),
    )
    vulnerable_line = get_vulnerable_line_content(snippet)
    assert vulnerable_line == " debugger;"

    assert (
        get_vulnerable_line_content(
            Snippet(
                content=("function LimpiarCamposUsu()\n{\n"),
                offset=0,
            )
        )
        is None
    )


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            groups=[GroupFaker(name=GROUP_NAME)],
            stakeholders=[StakeholderFaker(email="user1@test.test")],
            findings=[FindingFaker(id="finding1", group_name=GROUP_NAME)],
            vulnerabilities=[
                VulnerabilityFaker(
                    id="vuln1",
                    finding_id="finding1",
                    root_id="root1",
                    state=VulnerabilityStateFaker(
                        status=VulnerabilityStateStatus.VULNERABLE,
                        commit="a6288824c036e6a09f6a4ebe44eb2f01fb67eeb6",
                        where="path/to/file2.ext",
                        specific="50",
                    ),
                    type=VulnerabilityType.LINES,
                    group_name=GROUP_NAME,
                ),
                VulnerabilityFaker(
                    id="vuln2",
                    finding_id="finding1",
                    root_id="root1",
                    state=VulnerabilityStateFaker(
                        status=VulnerabilityStateStatus.VULNERABLE,
                        commit="a6288824c036e6a09f6a4ebe44eb2f01fb67eeb6",
                        where="path/to/file2.ext",
                        specific="50",
                    ),
                    type=VulnerabilityType.LINES,
                    group_name="invalid_group_name",
                ),
            ],
        ),
    ),
    others=[
        Mock(
            snippet,
            "get_functional_semantics",
            "async",
            ("test", "test", None),
        ),
        Mock(
            AsyncClient,  # type: ignore
            "embed",
            "async",
            EMMBEDING,
        ),
    ],
)
async def test_store_in_open_search() -> None:
    loaders = get_new_context()
    snippet = Snippet(
        content=(
            "function LimpiarCamposUsu()\n{\n"
            " debugger;\n"
            " thisForm = document.Entrada;\n"
            ' thisForm.txtNombresUsu.value = "";\n'
            ' thisForm.txtApellidosUsu.value = "";\n'
            ' thisForm.txtCedulaUsu.value = "";\n'
            ' thisForm.txtCelularUsu.value = "";\n'
            ' //thisForm.txtTelefonoCasaUsu.value = "";\n'
            ' //thisForm.txtTelefonoOficUsu.value = "";\n'
            ' //thisForm.txtFaxUsu.value = "";\n'
            ' //thisForm.txtPaisUsu.value = "";\n'
            ' //thisForm.txtEstadoUsu.value = "";\n'
            ' //thisForm.txtCiudadUsu.value = "";\n'
            ' //thisForm.txtUrbanizacionUsu.value = "";\n'
            ' //thisForm.txtAvenidaUsu.value = "";\n'
            ' //thisForm.txtEdificioUsu.value = "";\n'
            ' //thisForm.txtNroCasaEdifUsu.value = "";\n'
            ' //thisForm.txtZonaPostalUsu.value = "";\n'
            ' thisForm.txtEmailUsu.value = "";\n'
            " thisForm.lstEstatusUsu.options.selectedIndex = 0;\n"
            " thisForm.dropEmpresa.options.selectedIndex = thisForm.dropEmpresa.length - 1;\n"
            " //thisForm.dropOficina.options.selectedIndex = thisForm.dropOficina.length - 1;\n"
            " thisForm.dropOficina.remove;\n"
            " return false;\n}"
        ),
        offset=372,
        line=375,
        column=None,
        columns_per_line=120,
        line_context=-1,
        wrap=False,
        show_line_numbers=False,
        highlight_line_number=False,
        start_point=None,
        end_point=None,
        is_function=True,
        function=Function(
            name="LimpiarCamposUsu", node_type="function_declaration", field_identifier_name=None
        ),
    )
    await store_in_open_search(loaders, "vuln1", snippet)
    try:
        await store_in_open_search(loaders, "no_existing_vuln", snippet)
        raise AssertionError()
    except VulnNotFound:
        assert True
    await store_in_open_search(loaders, "vuln2", snippet)
