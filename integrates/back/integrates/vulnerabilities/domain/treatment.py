from collections.abc import (
    Iterable,
)
from datetime import (
    UTC,
    datetime,
    timedelta,
)
from itertools import (
    islice,
)

from aioextensions import (
    collect,
    schedule,
)

from integrates.custom_exceptions import (
    InvalidAcceptanceDays,
    InvalidNotificationRequest,
    InvalidVulnsNumber,
    VulnNotFound,
)
from integrates.custom_utils import (
    datetime as datetime_utils,
)
from integrates.custom_utils import findings as findings_utils
from integrates.custom_utils import (
    validations,
    validations_deco,
)
from integrates.custom_utils import (
    vulnerabilities as vulns_utils,
)
from integrates.custom_utils.findings import (
    is_finding_released,
)
from integrates.custom_utils.stakeholders import (
    get_stakeholder,
)
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model import (
    utils as db_model_utils,
)
from integrates.db_model import (
    vulnerabilities as vulns_model,
)
from integrates.db_model.enums import (
    AcceptanceStatus,
    Notification,
    TreatmentStatus,
)
from integrates.db_model.findings.types import (
    Finding,
)
from integrates.db_model.organization_finding_policies.enums import (
    PolicyStateStatus,
)
from integrates.db_model.roots.types import (
    RootRequest,
)
from integrates.db_model.types import (
    Treatment,
    TreatmentToUpdate,
)
from integrates.db_model.vulnerabilities.types import (
    Vulnerability,
    VulnerabilityRequest,
)
from integrates.group_access import (
    domain as group_access_domain,
)
from integrates.mailer import (
    utils as mailer_utils,
)
from integrates.mailer import (
    vulnerabilities as vulns_mailer,
)
from integrates.treatment import (
    validations as treatment_validations,
)
from integrates.treatment.utils import get_valid_assigned
from integrates.vulnerabilities.domain.core import (
    get_updated_manager_mail_content,
    get_vulnerability,
    group_vulnerabilities,
    should_send_update_treatment,
)
from integrates.vulnerabilities.domain.utils import (
    format_vulnerability_locations,
    format_vulnerability_treatment_data,
    get_organization_finding_policy,
    is_permanent_acceptance,
    validate_acceptance,
)
from integrates.vulnerabilities.domain.validations import (
    validate_accepted_treatment_change,
)


async def add_vulnerability_treatment(
    *,
    modified_by: str,
    vulnerability: Vulnerability,
    treatment: TreatmentToUpdate,
) -> None:
    await vulns_model.update_treatment(
        current_value=vulnerability,
        finding_id=vulnerability.finding_id,
        vulnerability_id=vulnerability.id,
        treatment=Treatment(
            acceptance_status=AcceptanceStatus.SUBMITTED
            if treatment.status == TreatmentStatus.ACCEPTED_UNDEFINED
            else treatment.acceptance_status,
            accepted_until=treatment.accepted_until,
            justification=treatment.justification,
            assigned=treatment.assigned or modified_by,
            modified_by=modified_by,
            modified_date=datetime_utils.get_utc_now(),
            status=treatment.status,
        ),
    )


def get_treatment_change(
    vulnerability: Vulnerability,
    min_date: datetime,
) -> tuple[str, str, Vulnerability] | None:
    if vulnerability.treatment is not None:
        last_treatment_date = vulnerability.treatment.modified_date
        if last_treatment_date > min_date:
            treatment = str(vulnerability.treatment.status.value)
            status = (
                vulnerability.treatment.acceptance_status.value
                if vulnerability.treatment.acceptance_status is not None
                else ""
            )
            return treatment, status, vulnerability
        return None
    return None


async def get_treatment_changes(
    loaders: Dataloaders,
    vuln: Vulnerability,
) -> int:
    historic = await loaders.vulnerability_historic_treatment.load(
        VulnerabilityRequest(vulnerability_id=vuln.id, finding_id=vuln.finding_id),
    )
    if historic:
        first_treatment = historic[0]
        return (
            len(historic) - 1
            if first_treatment.status == TreatmentStatus.UNTREATED
            else len(historic)
        )
    return 0


async def _handle_vulnerability_acceptance(
    *,
    loaders: Dataloaders,
    finding_id: str,
    new_treatment: Treatment,
    vulnerability: Vulnerability,
) -> None:
    if (
        new_treatment.acceptance_status == AcceptanceStatus.APPROVED
        and vulnerability.treatment
        and vulnerability.treatment.assigned
    ):
        treatment_to_add = new_treatment._replace(assigned=vulnerability.treatment.assigned)
        await vulns_model.update_treatment(
            current_value=vulnerability,
            finding_id=finding_id,
            vulnerability_id=vulnerability.id,
            treatment=treatment_to_add,
        )
    elif new_treatment.acceptance_status == AcceptanceStatus.REJECTED:
        treatment_loader = loaders.vulnerability_historic_treatment
        historic_treatment = await treatment_loader.load(
            VulnerabilityRequest(
                vulnerability_id=vulnerability.id,
                finding_id=vulnerability.finding_id,
            ),
        )
        treatments_to_add = db_model_utils.adjust_historic_dates(
            (
                new_treatment,
                Treatment(
                    modified_date=new_treatment.modified_date,
                    status=TreatmentStatus.IN_PROGRESS,
                    modified_by=new_treatment.modified_by,
                    assigned=historic_treatment[-1].modified_by,
                    justification=new_treatment.justification,
                ),
            ),
        )

        await collect(
            (
                vulns_model.update_treatment(
                    current_value=vulnerability,
                    finding_id=finding_id,
                    vulnerability_id=vulnerability.id,
                    treatment=treatments_to_add[1],
                ),
                vulns_model.add_historic_entry(
                    entry=new_treatment,
                    vulnerability_id=vulnerability.id,
                ),
            ),
        )

    else:
        await vulns_model.update_treatment(
            current_value=vulnerability,
            finding_id=finding_id,
            vulnerability_id=vulnerability.id,
            treatment=new_treatment,
        )


@validations_deco.validate_length_deco("justification", max_length=10000)
@validations_deco.validate_fields_deco(["justification"])
async def handle_vulnerabilities_acceptance(
    *,
    loaders: Dataloaders,
    accepted_vulns: list[str],
    finding_id: str,
    justification: str,
    rejected_vulns: list[str],
    user_email: str,
) -> None:
    today = datetime_utils.get_utc_now()

    all_vulns = accepted_vulns + rejected_vulns
    max_number_of_vulns = 32
    if len(all_vulns) > max_number_of_vulns:
        raise InvalidVulnsNumber(number_of_vulns=max_number_of_vulns)

    all_vulnerabilities = await loaders.vulnerability.load_many(all_vulns)
    vulnerabilities: list[Vulnerability] = [
        vulnerability for vulnerability in all_vulnerabilities if vulnerability is not None
    ]
    if len(all_vulnerabilities) != len(vulnerabilities):
        raise VulnNotFound()
    if any(
        vulnerability for vulnerability in vulnerabilities if vulnerability.finding_id != finding_id
    ):
        raise VulnNotFound()
    for vuln in vulnerabilities:
        validate_acceptance(vuln)

    await collect(
        tuple(
            _handle_vulnerability_acceptance(
                loaders=loaders,
                finding_id=finding_id,
                new_treatment=format_vulnerability_treatment_data(
                    acceptance=vuln.id in accepted_vulns,
                    accepted_until=vuln.treatment.accepted_until,
                    justification=justification,
                    today=today,
                    user_email=user_email,
                    status=vuln.treatment.status,
                ),
                vulnerability=vuln,
            )
            for vuln in vulnerabilities
            if vuln.treatment
        ),
        workers=32,
    )


async def send_treatment_change_mail(
    *,
    loaders: Dataloaders,
    assigned: str,
    finding_id: str,
    finding_title: str,
    group_name: str,
    justification: str,
    min_date: datetime,
    modified_by: str,
) -> bool:
    vulns = await loaders.finding_vulnerabilities_released_nzr.load(finding_id)
    changes = list(filter(None, [get_treatment_change(vuln, min_date) for vuln in vulns]))
    treatments = {(change[0], change[1]) for change in changes}
    for treatment in treatments:
        treatments_change = [change for change in changes if change[0] == treatment[0]]
        updated_vulns = [change[2] for change in treatments_change]
        if treatment[1] == "APPROVED":
            await send_treatment_report_mail(
                loaders=loaders,
                modified_by=modified_by,
                finding_id=finding_id,
                justification=(
                    updated_vulns[0].treatment.justification
                    if updated_vulns and updated_vulns[0].treatment is not None
                    else ""
                ),
                updated_vulns=updated_vulns,
                is_approved=True,
                is_permanent=is_permanent_acceptance(treatment[0]),
            )
            continue
        if treatment[1] == "SUBMITTED":
            await send_treatment_report_mail(
                loaders=loaders,
                modified_by=modified_by,
                finding_id=finding_id,
                justification=(
                    updated_vulns[0].treatment.justification
                    if updated_vulns and updated_vulns[0].treatment is not None
                    else ""
                ),
                updated_vulns=updated_vulns,
                is_approved=False,
                is_permanent=is_permanent_acceptance(treatment[0]),
            )
            continue

        await should_send_update_treatment(
            loaders=loaders,
            assigned=assigned,
            finding_id=finding_id,
            finding_title=finding_title,
            group_name=group_name,
            justification=justification,
            treatment=treatment[0],
            updated_vulns=updated_vulns,
            modified_by=modified_by,
        )

    return bool(treatments)


async def send_treatment_report_mail(
    *,
    loaders: Dataloaders,
    modified_by: str | None,
    justification: str | None,
    finding_id: str,
    updated_vulns: Iterable[Vulnerability],
    is_approved: bool = False,
    is_permanent: bool = False,
) -> None:
    finding = await findings_utils.get_finding(loaders, finding_id)
    vulns_grouped = group_vulnerabilities(updated_vulns)
    vulns_roots = await loaders.root.load_many(
        [
            RootRequest(group_name=finding.group_name, root_id=vuln.root_id or "")
            for vuln in vulns_grouped
        ],
    )
    vulns_data = vulns_utils.format_vulnerabilities(vulns_grouped, vulns_roots)
    mail_content = get_updated_manager_mail_content(vulns_data)
    users_email, managers_email = await collect(
        (
            mailer_utils.get_group_emails_by_notification(
                loaders=loaders,
                group_name=finding.group_name,
                notification="treatment_report",
            ),
            get_managers_by_size(loaders, finding.group_name, 3),
        ),
    )
    schedule(
        vulns_mailer.send_mail_treatment_report(
            loaders=loaders,
            finding_id=finding_id,
            finding_title=finding.title,
            group_name=finding.group_name,
            justification=justification,
            managers_email=managers_email,
            modified_by=modified_by,
            modified_date=datetime_utils.get_utc_now(),
            location=mail_content,
            email_to=users_email,
            is_approved=is_approved,
            is_permanent=is_permanent,
        ),
    )


async def get_managers_by_size(loaders: Dataloaders, group_name: str, list_size: int) -> list[str]:
    """Returns a list of managers with an specific length for the array"""
    managers = list(
        islice(
            await group_access_domain.get_managers(loaders, group_name),
            list_size,
        ),
    )
    return managers


@validations_deco.validate_fields_deco(["treatment.justification", "treatment.assigned"])
@validations_deco.validate_length_deco("treatment.justification", max_length=10000)
async def update_vulnerabilities_treatment(
    *,
    loaders: Dataloaders,
    finding: Finding,
    modified_by: str,
    vulnerability_id: str,
    treatment: TreatmentToUpdate,
) -> None:
    vulnerability = await get_vulnerability(loaders, vulnerability_id)
    validations.validate_closed(vulnerability)
    validations.validate_released(vulnerability)
    if vulnerability.finding_id != finding.id:
        raise VulnNotFound()
    valid_assigned = await get_valid_assigned(
        loaders=loaders,
        assigned=treatment.assigned or modified_by,
        email=modified_by,
        group_name=finding.group_name,
    )
    treatment_validations.validate_same_value(
        current_treatment=vulnerability.treatment,
        treatment_to_update=treatment,
        valid_assigned=valid_assigned,
    )

    if treatment.status == TreatmentStatus.ACCEPTED:
        if not treatment.accepted_until:
            raise InvalidAcceptanceDays("Acceptance parameter missing")
        historic_treatment = await loaders.vulnerability_historic_treatment.load(
            VulnerabilityRequest(
                finding_id=vulnerability.finding_id,
                vulnerability_id=vulnerability.id,
            ),
        )
        await validate_accepted_treatment_change(
            loaders=loaders,
            accepted_until=treatment.accepted_until,
            acceptance_severity=vulns_utils.get_severity_threat_score(vulnerability, finding),
            group_name=finding.group_name,
            historic_treatment=historic_treatment,
        )

        temporary_acceptance_finding_policy = await get_organization_finding_policy(
            finding_title=finding.title,
            group_name=finding.group_name,
            loaders=loaders,
            treatment=TreatmentStatus.ACCEPTED,
        )
        if (
            temporary_acceptance_finding_policy
            and temporary_acceptance_finding_policy.treatment_acceptance == TreatmentStatus.ACCEPTED
            and temporary_acceptance_finding_policy.state.status == PolicyStateStatus.APPROVED
        ):
            treatment = treatment._replace(
                acceptance_status=AcceptanceStatus.SUBMITTED,
            )
        else:
            treatment = treatment._replace(acceptance_status=AcceptanceStatus.APPROVED)

    await add_vulnerability_treatment(
        modified_by=modified_by,
        treatment=treatment._replace(
            assigned=valid_assigned,
        ),
        vulnerability=vulnerability,
    )


def are_all_approved(*, vulnerabilities: list[Vulnerability]) -> bool:
    return all(
        vuln.treatment and vuln.treatment.acceptance_status == AcceptanceStatus.APPROVED
        for vuln in vulnerabilities
    )


def _raise_vulns_invalid_notifications(vulnerabilities: list[Vulnerability], assigned: str) -> None:
    for vuln in vulnerabilities:
        if vuln.treatment:
            if datetime_utils.get_utc_now() - vuln.treatment.modified_date > timedelta(minutes=10):
                raise InvalidNotificationRequest(
                    "Too much time has passed to notify some of these changes",
                )
            if vuln.treatment.assigned != assigned:
                raise InvalidNotificationRequest(
                    "Not all the vulns provided have the same assigned hacker",
                )


async def validate_and_send_notification_request(
    loaders: Dataloaders,
    finding: Finding,
    responsible: str,
    vulnerabilities: list[str],
) -> None:
    # Validate finding with vulns in group
    finding_vulns: list[Vulnerability] = await loaders.finding_vulnerabilities_all.load(finding.id)
    assigned_vulns: list[Vulnerability] = list(
        vuln
        for vuln in finding_vulns
        for vulnerability_id in vulnerabilities
        if vuln.id == vulnerability_id
    )
    if len(assigned_vulns) != len(vulnerabilities):
        raise InvalidNotificationRequest(
            "Some of the provided vulns ids don't match existing vulns",
        )
    assigned = ""
    # Validate assigned
    if assigned_vulns[0].treatment:
        assigned = str(assigned_vulns[0].treatment.assigned)
        justification = str(assigned_vulns[0].treatment.justification)
    if not assigned:
        raise InvalidNotificationRequest(
            "Some of the provided vulns don't have any assigned hackers",
        )
    # Validate recent changes in treatment
    _raise_vulns_invalid_notifications(assigned_vulns, assigned)
    where_str = format_vulnerability_locations(list(vuln.state.where for vuln in assigned_vulns))

    stakeholder = await get_stakeholder(loaders, assigned)
    await send_treatment_change_mail(
        loaders=loaders,
        assigned=assigned,
        finding_id=finding.id,
        finding_title=finding.title,
        group_name=finding.group_name,
        justification=justification,
        min_date=datetime.now(UTC) - timedelta(minutes=20),
        modified_by=responsible,
    )
    if (
        Notification.VULNERABILITY_ASSIGNED in stakeholder.state.notifications_preferences.email
        and not are_all_approved(vulnerabilities=assigned_vulns)
    ):
        await vulns_mailer.send_mail_assigned_vulnerability(
            loaders=loaders,
            email_to=[assigned],
            is_finding_released=is_finding_released(finding),
            group_name=finding.group_name,
            finding_id=finding.id,
            finding_title=finding.title,
            responsible=responsible,
            where=where_str,
        )
