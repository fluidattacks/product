from collections import defaultdict
from time import time

from aioextensions import collect

from integrates.custom_exceptions import VulnNotFound
from integrates.custom_utils import datetime as datetime_utils
from integrates.custom_utils import findings as findings_utils
from integrates.custom_utils import validations
from integrates.custom_utils.constants import SCHEDULE_CLONE_GROUPS_ROOTS_EMAIL
from integrates.dataloaders import Dataloaders
from integrates.db_model import findings as findings_model
from integrates.db_model import vulnerabilities as vulns_model
from integrates.db_model.events.enums import EventType
from integrates.db_model.finding_comments.enums import CommentType
from integrates.db_model.finding_comments.types import FindingComment
from integrates.db_model.findings.enums import FindingVerificationStatus
from integrates.db_model.findings.types import FindingVerification
from integrates.db_model.vulnerabilities.enums import (
    VulnerabilityStateStatus,
    VulnerabilityVerificationStatus,
)
from integrates.db_model.vulnerabilities.types import Vulnerability, VulnerabilityVerification
from integrates.finding_comments import domain as finding_comments_domain
from integrates.vulnerabilities import domain as vulns_domain


async def request_hold(
    *,
    event_id: str,
    vulnerability: Vulnerability,
    modified_by: str,
) -> None:
    verification = VulnerabilityVerification(
        event_id=event_id,
        modified_by=modified_by,
        modified_date=datetime_utils.get_utc_now(),
        status=VulnerabilityVerificationStatus.ON_HOLD,
    )
    await vulns_model.update_historic_entry(
        current_value=vulnerability,
        finding_id=vulnerability.finding_id,
        vulnerability_id=vulnerability.id,
        entry=verification,
    )
    await vulns_model.update_event_index(
        finding_id=vulnerability.finding_id,
        entry=verification,
        vulnerability_id=vulnerability.id,
    )


async def request_vulnerabilities_hold(
    *,
    loaders: Dataloaders,
    finding_id: str,
    event_id: str,
    event_type: EventType,
    user_info: dict[str, str],
    vulnerability_ids: set[str],
) -> None:
    vulnerabilities: tuple[Vulnerability, ...] | list[Vulnerability]
    justification: str = f"These reattacks have been put on hold because of Event #{event_id}"
    finding = await findings_utils.get_finding(loaders, finding_id)
    vulnerabilities = await vulns_domain.get_by_finding_and_vuln_ids(
        loaders,
        finding_id,
        vulnerability_ids,
    )
    vulnerabilities = [validations.validate_requested_hold(vuln) for vuln in vulnerabilities]
    vulnerabilities = [validations.validate_closed(vuln) for vuln in vulnerabilities]
    for vuln in vulnerabilities:
        validations.validate_vuln_association(event_type, vuln)
        validations.validate_released(vuln)

    if not vulnerabilities:
        raise VulnNotFound()

    comment_id = str(round(time() * 1000))
    user_email = str(user_info["user_email"])
    verification = FindingVerification(
        comment_id=comment_id,
        modified_by=user_email,
        modified_date=datetime_utils.get_utc_now(),
        status=FindingVerificationStatus.ON_HOLD,
        vulnerability_ids=vulnerability_ids,
    )
    await findings_model.update_verification(
        current_value=finding.verification,
        group_name=finding.group_name,
        finding_id=finding.id,
        verification=verification,
    )
    await collect(
        [
            request_hold(event_id=event_id, vulnerability=vuln, modified_by=user_email)
            for vuln in vulnerabilities
        ],
        workers=32,
    )
    comment_data = FindingComment(
        finding_id=finding_id,
        comment_type=CommentType.VERIFICATION,
        content=justification,
        parent_id="0",
        id=comment_id,
        email=user_email,
        creation_date=datetime_utils.get_utc_now(),
        full_name=" ".join([user_info["first_name"], user_info["last_name"]]),
    )
    await finding_comments_domain.add(loaders, comment_data)


async def handle_vulnerabilities_on_hold(
    *,
    loaders: Dataloaders,
    root_id: str,
    event_id: str,
    event_type: EventType,
) -> None:
    root_vulns = await loaders.root_vulnerabilities.load(root_id)
    vulns_by_finding = defaultdict(list)

    for vuln in root_vulns:
        if (
            vuln.state.status == VulnerabilityStateStatus.VULNERABLE
            and vuln.verification
            and vuln.verification.status == VulnerabilityVerificationStatus.REQUESTED
        ):
            vulns_by_finding[vuln.finding_id].append(vuln.id)

    for finding_id, vulns_ids in vulns_by_finding.items():
        user_info = {
            "user_email": SCHEDULE_CLONE_GROUPS_ROOTS_EMAIL,
            "first_name": "Scanner",
            "last_name": "Services",
            "extra": f"""
                Reattack set to On Hold because an automatic
                event of type {event_type} was created.
                """,
        }
        await vulns_domain.request_vulnerabilities_hold(
            loaders=loaders,
            event_id=event_id,
            event_type=event_type,
            finding_id=finding_id,
            user_info=user_info,
            vulnerability_ids=set(vulns_ids),
        )
