import functools
import logging
import logging.config
import re
from collections.abc import Callable, Iterable
from datetime import datetime
from decimal import Decimal
from string import hexdigits
from typing import Any
from urllib.parse import urlparse

from aioextensions import collect

from integrates.custom_exceptions import (
    InvalidAcceptanceDays,
    InvalidAcceptanceSeverity,
    InvalidNumberAcceptances,
    InvalidParameter,
    InvalidPath,
    InvalidPort,
    InvalidSource,
    InvalidStream,
    InvalidVulnCommitHash,
    InvalidVulnerabilityAlreadyExists,
    InvalidVulnSpecific,
    InvalidVulnWhere,
)
from integrates.custom_utils import datetime as datetime_utils
from integrates.custom_utils.groups import get_group
from integrates.custom_utils.validations import check_exp, get_attr_value
from integrates.custom_utils.vulnerabilities import get_path_from_integrates_vulnerability
from integrates.dataloaders import Dataloaders
from integrates.db_model.constants import (
    DEFAULT_MAX_ACCEPTANCE_DAYS,
    DEFAULT_MAX_SEVERITY,
    DEFAULT_MIN_SEVERITY,
)
from integrates.db_model.enums import Source
from integrates.db_model.types import Treatment
from integrates.db_model.vulnerabilities.enums import (
    VulnerabilityType,
    VulnerabilityVerificationStatus,
)
from integrates.db_model.vulnerabilities.types import Vulnerability
from integrates.organizations.utils import get_organization
from integrates.settings.logger import LOGGING
from integrates.vulnerabilities.domain.utils import get_hash, is_valid_accepted_treatment

logging.config.dictConfig(LOGGING)
LOGGER = logging.getLogger(__name__)


async def get_policy_max_acceptance_days(*, loaders: Dataloaders, group_name: str) -> int | None:
    group = await get_group(loaders, group_name)
    if group.policies and group.policies.max_acceptance_days:
        return group.policies.max_acceptance_days

    organization = await get_organization(loaders, group.organization_id)

    return organization.policies.max_acceptance_days


async def get_policy_days_until_it_breaks(*, loaders: Dataloaders, group_name: str) -> int | None:
    group = await get_group(loaders, group_name)
    if group.policies:
        return group.policies.days_until_it_breaks

    organization = await get_organization(loaders, group.organization_id)

    return organization.policies.days_until_it_breaks


async def get_policy_max_number_acceptances(*, loaders: Dataloaders, group_name: str) -> int | None:
    group = await get_group(loaders, group_name)
    if group.policies:
        return group.policies.max_number_acceptances

    organization = await get_organization(loaders, group.organization_id)

    return organization.policies.max_number_acceptances


async def get_policy_max_acceptance_severity(*, loaders: Dataloaders, group_name: str) -> Decimal:
    group = await get_group(loaders, group_name)
    if group.policies:
        return (
            group.policies.max_acceptance_severity
            if group.policies.max_acceptance_severity is not None
            else DEFAULT_MAX_SEVERITY
        )

    organization = await get_organization(loaders, group.organization_id)

    return (
        organization.policies.max_acceptance_severity
        if organization.policies.max_acceptance_severity is not None
        else DEFAULT_MAX_SEVERITY
    )


async def get_policy_min_acceptance_severity(*, loaders: Dataloaders, group_name: str) -> Decimal:
    group = await get_group(loaders, group_name)
    if group.policies:
        return group.policies.min_acceptance_severity or DEFAULT_MIN_SEVERITY

    organization = await get_organization(loaders, group.organization_id)

    return organization.policies.min_acceptance_severity or DEFAULT_MIN_SEVERITY


async def validate_acceptance_days(
    loaders: Dataloaders,
    accepted_until: datetime,
    group_name: str,
) -> None:
    """Checks if acceptance date complies with group and organization policies."""
    today = datetime_utils.get_utc_now()
    acceptance_days = Decimal((accepted_until - today).days)
    max_acceptance_days = await get_policy_max_acceptance_days(
        loaders=loaders,
        group_name=group_name,
    )

    if max_acceptance_days is None:
        max_acceptance_days = DEFAULT_MAX_ACCEPTANCE_DAYS
    if (acceptance_days > max_acceptance_days) or acceptance_days < 0:
        raise InvalidAcceptanceDays(
            "Chosen date is either in the past or exceeds "
            "the maximum number of days allowed by the defined policy",
        )


async def validate_acceptance_severity(
    loaders: Dataloaders,
    group_name: str,
    acceptance_severity: Decimal,
) -> None:
    """
    Checks if the severity to be temporarily accepted is inside
    the range set by the defined policy.
    """
    min_value = await get_policy_min_acceptance_severity(loaders=loaders, group_name=group_name)
    max_value = await get_policy_max_acceptance_severity(loaders=loaders, group_name=group_name)
    if not min_value <= acceptance_severity <= max_value:
        raise InvalidAcceptanceSeverity(str(acceptance_severity))


async def validate_number_acceptances(
    loaders: Dataloaders,
    group_name: str,
    historic_treatment: Iterable[Treatment],
) -> None:
    """
    Check that a vulnerability to temporarily accept does not exceed the
    maximum number of acceptances the organization set.
    """
    max_acceptances = await get_policy_max_number_acceptances(
        loaders=loaders,
        group_name=group_name,
    )
    current_acceptances: int = sum(
        1 for item in historic_treatment if is_valid_accepted_treatment(item)
    )
    if max_acceptances is not None and current_acceptances + 1 > max_acceptances:
        raise InvalidNumberAcceptances(str(current_acceptances) if current_acceptances else "-")


async def validate_accepted_treatment_change(
    *,
    loaders: Dataloaders,
    accepted_until: datetime,
    acceptance_severity: Decimal,
    group_name: str,
    historic_treatment: Iterable[Treatment],
) -> None:
    await collect(
        [
            validate_acceptance_days(loaders, accepted_until, group_name),
            validate_acceptance_severity(loaders, group_name, acceptance_severity),
            validate_number_acceptances(loaders, group_name, historic_treatment),
        ],
    )


def validate_lines_specific(specific: str) -> None:
    if not specific.isdigit():
        raise InvalidVulnSpecific.new()


def validate_lines_specific_deco(specific_field: str) -> Callable:
    def wrapper(func: Callable) -> Callable:
        @functools.wraps(func)
        def decorated(*args: Any, **kwargs: Any) -> Any:
            specific = get_attr_value(field=specific_field, kwargs=kwargs, obj_type=str)
            if not specific.isdigit():
                raise InvalidVulnSpecific.new()
            return func(*args, **kwargs)

        return decorated

    return wrapper


def validate_ports_specific(specific: str) -> None:
    if not specific.isdigit():
        raise InvalidVulnSpecific.new()
    if not 0 <= int(specific) <= 65535:
        raise InvalidPort(expr=f'"values": "{specific}"')


def validate_ports_specific_deco(vuln_type_field: str, specific_field: str) -> Callable:
    def wrapper(func: Callable) -> Callable:
        @functools.wraps(func)
        def decorated(*args: Any, **kwargs: Any) -> Any:
            vuln_type = get_attr_value(field=vuln_type_field, kwargs=kwargs, obj_type=str)
            specific = get_attr_value(field=specific_field, kwargs=kwargs, obj_type=str)
            if vuln_type == VulnerabilityType.PORTS:
                validate_ports_specific(specific=specific)

            return func(*args, **kwargs)

        return decorated

    return wrapper


def validate_uniqueness(
    *,
    finding_vulns_data: tuple[Vulnerability, ...],
    vulnerability_where: str,
    vulnerability_specific: str,
    vulnerability_type: VulnerabilityType,
    vulnerability_id: str,
) -> None:
    current_vuln = next(
        (item for item in finding_vulns_data if item.id == vulnerability_id),
        None,
    )
    if not current_vuln:
        return
    new_vuln_hash: int = get_hash(
        specific=vulnerability_specific,
        type_=vulnerability_type.value,
        where=get_path_from_integrates_vulnerability(vulnerability_where, vulnerability_type)[1]
        if current_vuln.type == VulnerabilityType.INPUTS
        else vulnerability_where,
        root_id=current_vuln.root_id,
    )
    for vuln in finding_vulns_data:
        vuln_hash = hash(vuln)
        if vuln_hash == new_vuln_hash:
            raise InvalidVulnerabilityAlreadyExists.new()


def validate_commit_hash(vuln_commit: str) -> None:
    if len(vuln_commit) != 40 or not set(hexdigits).issuperset(set(vuln_commit)):
        raise InvalidVulnCommitHash.new()


def validate_commit_hash_deco(vuln_commit_field: str) -> Callable:
    def wrapper(func: Callable) -> Callable:
        @functools.wraps(func)
        def decorated(*args: Any, **kwargs: Any) -> Any:
            vuln_commit = get_attr_value(field=vuln_commit_field, kwargs=kwargs, obj_type=str)
            if len(vuln_commit) != 40 or not set(hexdigits).issuperset(set(vuln_commit)):
                raise InvalidVulnCommitHash.new()
            return func(*args, **kwargs)

        return decorated

    return wrapper


def validate_stream(
    where: str,
    stream: str,
    index: int,
    vuln_type: str,
) -> bool:
    url_parsed = urlparse(where)
    if (len(url_parsed.path) == 0 or url_parsed.path == "/") and not (
        stream.lower().startswith("home,") or stream.lower().startswith("query,")
    ):
        raise InvalidStream(vuln_type, f"{index}")
    return True


def validate_stream_deco(
    where_field: str,
    stream_field: str,
    index_field: str,
    vuln_type_field: str,
) -> Callable:
    def wrapper(func: Callable) -> Callable:
        @functools.wraps(func)
        def decorated(*args: Any, **kwargs: Any) -> Any:
            where = get_attr_value(field=where_field, kwargs=kwargs, obj_type=str)
            stream = get_attr_value(field=stream_field, kwargs=kwargs, obj_type=str)
            index = get_attr_value(field=index_field, kwargs=kwargs, obj_type=int)
            vuln_type = get_attr_value(field=vuln_type_field, kwargs=kwargs, obj_type=str)
            validate_stream(where=where, stream=stream, index=index, vuln_type=vuln_type)
            return func(*args, **kwargs)

        return decorated

    return wrapper


def validate_where(where: str) -> None:
    if not re.match("^[^=/]+.+$", where):
        raise InvalidVulnWhere.new()


def validate_where_deco(where_field: str) -> Callable:
    def wrapper(func: Callable) -> Callable:
        @functools.wraps(func)
        def decorated(*args: Any, **kwargs: Any) -> Any:
            where = get_attr_value(field=where_field, kwargs=kwargs, obj_type=str)
            check_exp(where, r"^[^=/]+.+$", InvalidVulnWhere.new())
            return func(*args, **kwargs)

        return decorated

    return wrapper


def validate_path(path: str) -> None:
    # Use Unix-like paths
    if path.find("\\") >= 0:
        invalid_path = path.replace("\\", "\\\\")
        raise InvalidPath(expr=f'"values": "{invalid_path}"')


def validate_path_deco(path_field: str) -> Callable:
    def wrapper(func: Callable) -> Callable:
        @functools.wraps(func)
        def decorated(*args: Any, **kwargs: Any) -> Any:
            path = get_attr_value(field=path_field, kwargs=kwargs, obj_type=Source)
            if path.find("\\") >= 0:
                invalid_path = path.replace("\\", "\\\\")
                raise InvalidPath(expr=f'"values": "{invalid_path}"')
            return func(*args, **kwargs)

        return decorated

    return wrapper


def validate_source_deco(source_field: str) -> Callable:
    def wrapper(func: Callable) -> Callable:
        @functools.wraps(func)
        def decorated(*args: Any, **kwargs: Any) -> Any:
            source = get_attr_value(field=source_field, kwargs=kwargs, obj_type=Source)
            if source and source not in {
                Source.ANALYST,
                Source.CUSTOMER,
                Source.DETERMINISTIC,
                Source.ESCAPE,
                Source.MACHINE,
            }:
                raise InvalidSource()
            return func(*args, **kwargs)

        return decorated

    return wrapper


def validate_updated_commit_deco(vulnerability_type_field: str, commit_field: str) -> Callable:
    def wrapper(func: Callable) -> Callable:
        @functools.wraps(func)
        def decorated(*args: Any, **kwargs: Any) -> Any:
            vulnerability_type = get_attr_value(
                field=vulnerability_type_field,
                kwargs=kwargs,
                obj_type=str,
            )
            commit = get_attr_value(field=commit_field, kwargs=kwargs, obj_type=str)
            if commit:
                if vulnerability_type is not VulnerabilityType.LINES:
                    raise InvalidParameter("commit")
                validate_commit_hash(commit)
            return func(*args, **kwargs)

        return decorated

    return wrapper


def validate_updated_specific_deco(vulnerability_type_field: str, specific_field: str) -> Callable:
    def wrapper(func: Callable) -> Callable:
        @functools.wraps(func)
        def decorated(*args: Any, **kwargs: Any) -> Any:
            vulnerability_type = get_attr_value(
                field=vulnerability_type_field,
                kwargs=kwargs,
                obj_type=str,
            )
            specific = get_attr_value(
                field=specific_field,
                kwargs=kwargs,
                obj_type=str,
            )
            if specific and vulnerability_type is VulnerabilityType.LINES:
                validate_lines_specific(specific)
            if specific and vulnerability_type is VulnerabilityType.PORTS:
                validate_ports_specific(specific)
            return func(*args, **kwargs)

        return decorated

    return wrapper


def validate_updated_where_deco(vulnerability_type_field: str, where_field: str) -> Callable:
    def wrapper(func: Callable) -> Callable:
        @functools.wraps(func)
        def decorated(*args: Any, **kwargs: Any) -> Any:
            vulnerability_type = get_attr_value(
                field=vulnerability_type_field,
                kwargs=kwargs,
                obj_type=str,
            )
            where = get_attr_value(
                field=where_field,
                kwargs=kwargs,
                obj_type=str,
            )
            if where:
                if vulnerability_type is VulnerabilityType.LINES:
                    validate_path(where)
                validate_where(where)
            return func(*args, **kwargs)

        return decorated

    return wrapper


def is_verification_already_requested(
    vulnerability: Vulnerability,
    is_closing_event: bool = False,
) -> bool:
    """
    Validate if the vulnerability is not requested. If no Event is being
    closed, vulnerabilities on hold count as requested.
    """
    if (
        vulnerability.verification
        and vulnerability.verification.status == VulnerabilityVerificationStatus.REQUESTED
    ):
        return True

    if (
        not is_closing_event
        and vulnerability.verification
        and vulnerability.verification.status == VulnerabilityVerificationStatus.ON_HOLD
    ):
        return True

    return False


def has_valid_plan(vulnerability: Vulnerability, has_advanced: bool) -> bool:
    if has_advanced:
        return True

    return vulnerability.state.source is Source.MACHINE
