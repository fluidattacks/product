from integrates.custom_exceptions import InvalidNotificationRequest
from integrates.dataloaders import get_new_context
from integrates.db_model.enums import AcceptanceStatus, Notification, TreatmentStatus
from integrates.db_model.stakeholders.types import NotificationsPreferences
from integrates.db_model.types import Treatment
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    DATE_2019,
    FindingFaker,
    GroupFaker,
    OrganizationFaker,
    StakeholderFaker,
    StakeholderStateFaker,
    VulnerabilityFaker,
)
from integrates.testing.mocks import mocks
from integrates.testing.utils import freeze_time, parametrize, raises
from integrates.vulnerabilities.domain.core import get_vulnerability
from integrates.vulnerabilities.domain.treatment import (
    handle_vulnerabilities_acceptance,
    validate_and_send_notification_request,
)

GROUP_NAME = "group1"
FINDING_ID = "32d4c2c4-411c-4e30-bf33-7efbfe4ef353"


@freeze_time(DATE_2019.isoformat())
@parametrize(
    args=["responsible", "vulns"],
    cases=[["user1@gmail.com", ["vuln1", "vuln2"]], ["user2@gmail.com", ["vuln3"]]],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            groups=[GroupFaker(name=GROUP_NAME, organization_id="org1")],
            stakeholders=[
                StakeholderFaker(
                    email="user1@gmail.com",
                    state=StakeholderStateFaker(
                        notifications_preferences=NotificationsPreferences(
                            email=[Notification.VULNERABILITY_ASSIGNED]
                        )
                    ),
                ),
                StakeholderFaker(
                    email="user2@gmail.com",
                ),
            ],
            findings=[
                FindingFaker(
                    id=FINDING_ID,
                    group_name=GROUP_NAME,
                ),
            ],
            vulnerabilities=[
                VulnerabilityFaker(
                    finding_id=FINDING_ID,
                    id="vuln1",
                    treatment=Treatment(
                        modified_date=DATE_2019,
                        status=TreatmentStatus.UNTREATED,
                        assigned="user1@gmail.com",
                    ),
                ),
                VulnerabilityFaker(
                    finding_id=FINDING_ID,
                    id="vuln2",
                    treatment=Treatment(
                        modified_date=DATE_2019,
                        status=TreatmentStatus.IN_PROGRESS,
                        assigned="user1@gmail.com",
                        justification="justification",
                    ),
                ),
                VulnerabilityFaker(
                    finding_id=FINDING_ID,
                    id="vuln3",
                    treatment=Treatment(
                        modified_date=DATE_2019,
                        status=TreatmentStatus.IN_PROGRESS,
                        assigned="user1@gmail.com",
                        justification="justification",
                        acceptance_status=AcceptanceStatus.APPROVED,
                    ),
                ),
            ],
        ),
    ),
)
async def test_validate_and_send_notification_request_success(
    responsible: str, vulns: list[str]
) -> None:
    loaders = get_new_context()
    finding = await loaders.finding.load(FINDING_ID)
    assert finding
    await validate_and_send_notification_request(
        loaders=loaders,
        finding=finding,
        responsible=responsible,
        vulnerabilities=vulns,
    )


@freeze_time(DATE_2019.isoformat())
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            groups=[GroupFaker(name=GROUP_NAME)],
            stakeholders=[
                StakeholderFaker(
                    email="user1@gmail.com",
                ),
            ],
            findings=[
                FindingFaker(
                    id=FINDING_ID,
                    group_name=GROUP_NAME,
                ),
            ],
            vulnerabilities=[
                VulnerabilityFaker(
                    finding_id=FINDING_ID,
                    id="vuln1",
                    treatment=Treatment(
                        modified_date=DATE_2019,
                        status=TreatmentStatus.UNTREATED,
                    ),
                ),
            ],
        ),
    ),
)
async def test_validate_and_send_notification_request_unassigned() -> None:
    loaders = get_new_context()
    finding = await loaders.finding.load(FINDING_ID)
    assert finding
    with raises(InvalidNotificationRequest, match="assigned hacker"):
        await validate_and_send_notification_request(
            loaders=loaders,
            finding=finding,
            responsible="user1@gmail.com",
            vulnerabilities=["vuln1"],
        )


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            groups=[GroupFaker(name=GROUP_NAME)],
            stakeholders=[StakeholderFaker(email="user1@test.test")],
            findings=[FindingFaker(id=FINDING_ID, group_name=GROUP_NAME)],
            vulnerabilities=[
                VulnerabilityFaker(
                    finding_id=FINDING_ID,
                    id="vuln1",
                    treatment=Treatment(
                        modified_date=DATE_2019,
                        status=TreatmentStatus.ACCEPTED,
                        acceptance_status=AcceptanceStatus.SUBMITTED,
                    ),
                ),
                VulnerabilityFaker(
                    finding_id=FINDING_ID,
                    id="vuln2",
                    treatment=Treatment(
                        modified_date=DATE_2019,
                        status=TreatmentStatus.ACCEPTED_UNDEFINED,
                        acceptance_status=AcceptanceStatus.SUBMITTED,
                    ),
                ),
                VulnerabilityFaker(
                    finding_id=FINDING_ID,
                    id="vuln3",
                    treatment=Treatment(
                        modified_date=DATE_2019,
                        status=TreatmentStatus.ACCEPTED,
                        acceptance_status=AcceptanceStatus.SUBMITTED,
                    ),
                ),
                VulnerabilityFaker(
                    finding_id=FINDING_ID,
                    id="vuln4",
                    treatment=Treatment(
                        modified_date=DATE_2019,
                        status=TreatmentStatus.ACCEPTED_UNDEFINED,
                        acceptance_status=AcceptanceStatus.SUBMITTED,
                    ),
                ),
            ],
        ),
    ),
)
async def test_handle_vulnerabilities_acceptance() -> None:
    loaders = get_new_context()
    await handle_vulnerabilities_acceptance(
        loaders=loaders,
        accepted_vulns=["vuln1", "vuln2"],
        finding_id=FINDING_ID,
        justification="A justification",
        rejected_vulns=["vuln3", "vuln4"],
        user_email="user1@test.test",
    )

    vuln1 = await get_vulnerability(loaders, "vuln1", clear_loader=True)
    vuln2 = await get_vulnerability(loaders, "vuln2", clear_loader=True)
    vuln3 = await get_vulnerability(loaders, "vuln3", clear_loader=True)
    vuln4 = await get_vulnerability(loaders, "vuln4", clear_loader=True)

    assert vuln1.treatment
    assert vuln2.treatment
    assert vuln3.treatment
    assert vuln4.treatment
    assert vuln1.treatment.acceptance_status == AcceptanceStatus.APPROVED
    assert vuln2.treatment.acceptance_status == AcceptanceStatus.APPROVED
    assert vuln3.treatment.acceptance_status is None
    assert vuln4.treatment.acceptance_status is None
    assert vuln1.treatment.status == TreatmentStatus.ACCEPTED
    assert vuln2.treatment.status == TreatmentStatus.ACCEPTED_UNDEFINED
    assert vuln3.treatment.status == TreatmentStatus.IN_PROGRESS
    assert vuln4.treatment.status == TreatmentStatus.IN_PROGRESS
    assert vuln1.treatment.justification == "A justification"
    assert vuln3.treatment.modified_by == "user1@test.test"
