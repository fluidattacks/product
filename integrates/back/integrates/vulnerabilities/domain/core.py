import logging
import logging.config
from collections import Counter
from collections.abc import Iterable
from contextlib import suppress
from datetime import datetime
from decimal import Decimal
from itertools import cycle, groupby
from time import time
from typing import Any

from aioextensions import collect, schedule
from botocore.exceptions import ReadTimeoutError

from integrates import authz
from integrates.class_types.types import Item
from integrates.custom_exceptions import (
    InvalidParameter,
    InvalidRemovalVulnReleased,
    InvalidRemovalVulnState,
    MissingApprovedEvidence,
    PendingEvidenceDraft,
    RequiredFieldToBeUpdate,
    RootNotFound,
    VulnNotFound,
    VulnNotInFinding,
)
from integrates.custom_utils import cvss as cvss_utils
from integrates.custom_utils import datetime as datetime_utils
from integrates.custom_utils import filter_vulnerabilities as filter_vulns_utils
from integrates.custom_utils import findings as findings_utils
from integrates.custom_utils import reports as reports_utils
from integrates.custom_utils import roots as roots_utils
from integrates.custom_utils import validations
from integrates.custom_utils import vulnerabilities as vulns_utils
from integrates.custom_utils.validations_deco import (
    validate_length_deco,
    validate_sanitized_csv_input_deco,
    validate_severity_range_deco,
    validate_url_deco,
)
from integrates.dataloaders import Dataloaders
from integrates.db_model import vulnerabilities as vulns_model
from integrates.db_model.enums import Source, TreatmentStatus
from integrates.db_model.finding_comments.enums import CommentType
from integrates.db_model.finding_comments.types import FindingComment
from integrates.db_model.findings.enums import FindingVerificationStatus
from integrates.db_model.findings.types import Finding, FindingRequest
from integrates.db_model.organization_finding_policies.enums import PolicyStateStatus
from integrates.db_model.organization_finding_policies.types import OrgFindingPolicy
from integrates.db_model.roots.types import GitRoot, RootRequest
from integrates.db_model.vulnerabilities.constants import (
    DRAFT_FILTER_STATUSES,
    RELEASED_FILTER_STATUSES,
)
from integrates.db_model.vulnerabilities.enums import (
    VulnerabilitySeverityProposalStatus,
    VulnerabilityStateReason,
    VulnerabilityStateStatus,
    VulnerabilityType,
    VulnerabilityVerificationStatus,
    VulnerabilityZeroRiskStatus,
)
from integrates.db_model.vulnerabilities.types import (
    FindingVulnerabilitiesZrRequest,
    VulnerabilitiesConnection,
    Vulnerability,
    VulnerabilityMetadataToUpdate,
    VulnerabilityRequest,
    VulnerabilitySeverityProposal,
    VulnerabilityState,
    VulnerabilityVerification,
    VulnerabilityZeroRisk,
)
from integrates.finding_comments import domain as comments_domain
from integrates.mailer import vulnerabilities as vulns_mail
from integrates.settings import LOGGING
from integrates.tickets import domain as tickets_domain
from integrates.vulnerabilities.domain.utils import get_hash_from_machine_vuln, set_snippet_remote
from integrates.vulnerabilities.domain.validations import (
    validate_source_deco,
    validate_uniqueness,
    validate_updated_commit_deco,
    validate_updated_specific_deco,
    validate_updated_where_deco,
)
from integrates.vulnerabilities.types import (
    FindingGroupedVulnerabilitiesInfo,
    GroupedVulnerabilitiesInfo,
    Treatments,
    VulnerabilityDescriptionToUpdate,
    ZeroRisk,
)

logging.config.dictConfig(LOGGING)
LOGGER = logging.getLogger(__name__)


async def get_vulnerability(
    loaders: Dataloaders,
    vulnerability_id: str,
    clear_loader: bool = False,
) -> Vulnerability:
    if clear_loader:
        loaders.vulnerability.clear(vulnerability_id)

    vulnerability = await loaders.vulnerability.load(vulnerability_id)
    if vulnerability is None:
        raise VulnNotFound()

    return vulnerability


async def get_vulnerabilities(
    loaders: Dataloaders,
    vulnerability_ids: Iterable[str],
    clear_loader: bool = False,
) -> list[Vulnerability]:
    if clear_loader:
        for key in vulnerability_ids:
            loaders.vulnerability.clear(key)
    result: list[Vulnerability] = []
    vulnerabilities = await loaders.vulnerability.load_many(vulnerability_ids)
    if any(
        vulnerability is None or vulns_utils.is_deleted(vulnerability)
        for vulnerability in vulnerabilities
    ):
        raise VulnNotFound()
    for vulnerability in vulnerabilities:
        if vulnerability and (
            (
                git_root := await loaders.root.load(
                    RootRequest(vulnerability.group_name, vulnerability.root_id)
                )
            )
            and isinstance(git_root, GitRoot)
            and git_root.cloning.commit
        ):
            result.append(
                vulnerability._replace(
                    state=vulnerability.state._replace(commit=git_root.cloning.commit)
                )
            )
        elif vulnerability:
            result.append(vulnerability)
    return result


async def get_reported_vulnerabilities(
    loaders: Dataloaders,
    finding_id: str,
) -> list[Vulnerability]:
    try:
        vulnerabilities = await loaders.finding_vulnerabilities_released_nzr.load(finding_id)
        return [
            vuln
            for vuln in vulnerabilities
            if vuln.state.status == VulnerabilityStateStatus.VULNERABLE
        ]
    except ReadTimeoutError as exc:
        LOGGER.error(exc, extra={"vars": locals()})
        return []


async def confirm_vulnerabilities_zero_risk(
    *,
    loaders: Dataloaders,
    vuln_ids: set[str],
    finding_id: str,
    user_info: dict[str, Any],
    justification: str,
) -> None:
    validations.validate_justification_length(justification)
    vulnerabilities = await get_by_finding_and_vuln_ids(loaders, finding_id, vuln_ids)
    vulnerabilities = [validations.validate_zero_risk_requested(vuln) for vuln in vulnerabilities]

    comment_id = str(round(time() * 1000))
    user_email = str(user_info["user_email"])
    comment_data = FindingComment(
        finding_id=finding_id,
        content=justification,
        comment_type=CommentType.ZERO_RISK,
        id=comment_id,
        email=user_email,
        full_name=" ".join([user_info["first_name"], user_info["last_name"]]),
        creation_date=datetime_utils.get_utc_now(),
        parent_id="0",
    )
    await comments_domain.add(loaders, comment_data)
    await collect(
        vulns_model.update_historic_entry(
            current_value=vuln,
            finding_id=vuln.finding_id,
            vulnerability_id=vuln.id,
            entry=VulnerabilityZeroRisk(
                comment_id=comment_id,
                modified_by=user_email,
                modified_date=datetime_utils.get_utc_now(),
                status=VulnerabilityZeroRiskStatus.CONFIRMED,
            ),
        )
        for vuln in vulnerabilities
    )


async def add_tags(
    vulnerability: Vulnerability,
    tags: list[str],
) -> None:
    if not tags:
        return
    if vulnerability.tags:
        tags.extend(vulnerability.tags)
    await vulns_model.update_metadata(
        finding_id=vulnerability.finding_id,
        vulnerability_id=vulnerability.id,
        metadata=VulnerabilityMetadataToUpdate(
            tags=sorted(list(set(tags))),
        ),
    )


async def _remove_all_tags(
    vulnerability: Vulnerability,
) -> None:
    await vulns_model.update_metadata(
        finding_id=vulnerability.finding_id,
        vulnerability_id=vulnerability.id,
        metadata=VulnerabilityMetadataToUpdate(tags=None),
    )


async def _remove_tag(
    vulnerability: Vulnerability,
    tag_to_remove: str,
) -> None:
    tags: list[str] | None = vulnerability.tags
    if tags and tag_to_remove in tags:
        tags.remove(tag_to_remove)
        await vulns_model.update_metadata(
            finding_id=vulnerability.finding_id,
            vulnerability_id=vulnerability.id,
            metadata=VulnerabilityMetadataToUpdate(
                tags=tags,
            ),
        )


async def resubmit_vulnerabilities(
    *,
    loaders: Dataloaders,
    vulnerability_ids: set[str],
    finding_id: str,
    modified_by: str,
) -> None:
    vulnerabilities = await get_by_finding_and_vuln_ids(loaders, finding_id, vulnerability_ids)
    for vulnerability in vulnerabilities:
        validations.validate_rejected(vulnerability)

    await collect(
        vulns_model.update_historic_entry(
            current_value=vulnerability,
            finding_id=vulnerability.finding_id,
            vulnerability_id=vulnerability.id,
            entry=vulnerability.state._replace(
                modified_by=modified_by,
                modified_date=datetime_utils.get_utc_now(),
                status=VulnerabilityStateStatus.SUBMITTED,
            ),
        )
        for vulnerability in vulnerabilities
    )


async def remove_vulnerability_tags(
    *,
    loaders: Dataloaders,
    vuln_ids: set[str],
    finding_id: str,
    tag_to_remove: str,
) -> None:
    vulnerabilities = await get_by_finding_and_vuln_ids(loaders, finding_id, vuln_ids)
    if tag_to_remove:
        await collect(_remove_tag(vuln, tag_to_remove) for vuln in vulnerabilities)
        return
    await collect(_remove_all_tags(vuln) for vuln in vulnerabilities)


async def validate_hacker_permissions(
    loaders: Dataloaders, user_email: str, group_name: str, vulnerability: Vulnerability
) -> None:
    if (
        await authz.get_group_level_role(loaders, user_email, group_name) == "hacker"
        and vulnerability.state.status not in DRAFT_FILTER_STATUSES
    ):
        raise InvalidRemovalVulnReleased.new()


async def remove_vulnerability(
    *,
    loaders: Dataloaders,
    finding_id: str,
    vulnerability_id: str,
    justification: VulnerabilityStateReason,
    email: str,
    include_closed_vuln: bool = False,
    existing_vulnerability: Vulnerability | None = None,
) -> None:
    vulnerability = existing_vulnerability or await get_vulnerability(loaders, vulnerability_id)
    if include_closed_vuln and vulnerability.state.status == VulnerabilityStateStatus.DELETED:
        return

    if vulnerability.state.status == VulnerabilityStateStatus.DELETED:
        raise VulnNotFound()

    await validate_hacker_permissions(
        loaders=loaders,
        user_email=email,
        group_name=vulnerability.group_name,
        vulnerability=vulnerability,
    )
    if (
        vulnerability.state.status
        not in {
            VulnerabilityStateStatus.VULNERABLE,
            VulnerabilityStateStatus.REJECTED,
            VulnerabilityStateStatus.SUBMITTED,
        }
        and not include_closed_vuln
    ):
        raise InvalidRemovalVulnState.new()

    deletion_state = VulnerabilityState(
        commit=vulnerability.state.commit,
        modified_by=email,
        modified_date=datetime_utils.get_utc_now(),
        source=vulnerability.state.source,
        specific=vulnerability.state.specific,
        status=VulnerabilityStateStatus.DELETED,
        reasons=[justification],
        tool=vulnerability.state.tool,
        where=vulnerability.state.where,
    )
    await vulns_model.update_historic_entry(
        current_value=vulnerability,
        entry=deletion_state,
        finding_id=finding_id,
        vulnerability_id=vulnerability_id,
        force_update=True,
    )
    if vulnerability.state.status not in RELEASED_FILTER_STATUSES:
        await vulns_model.remove(vulnerability_id=vulnerability_id, should_archive=True)


async def get_by_finding_and_vuln_ids(
    loaders: Dataloaders,
    finding_id: str,
    vuln_ids: set[str],
) -> list[Vulnerability]:
    finding_vulns = await loaders.finding_vulnerabilities.load(finding_id)
    filtered_vulns = filter_vulns_by_id_in_findings(finding_vulns, vuln_ids)

    return filtered_vulns


def filter_vulns_by_id_in_findings(
    finding_vulns: list[Vulnerability],
    vuln_ids: set[str],
) -> list[Vulnerability]:
    filtered_vulns = [vuln for vuln in finding_vulns if vuln.id in vuln_ids]
    if len(filtered_vulns) != len(vuln_ids):
        raise VulnNotInFinding()

    return filtered_vulns


async def get_closing_date(vulnerability: Vulnerability) -> datetime | None:
    """Get the closing date in ISO8601 UTC format."""
    current_closing_date = vulnerability.unreliable_indicators.unreliable_closing_date
    if current_closing_date:
        return current_closing_date

    if vulnerability.state.status is VulnerabilityStateStatus.SAFE:
        return vulnerability.state.modified_date

    return None


async def get_report_date(vulnerability: Vulnerability) -> datetime | None:
    current_opening_date = vulnerability.unreliable_indicators.unreliable_report_date
    if current_opening_date:
        return current_opening_date

    if vulnerability.state.status is VulnerabilityStateStatus.VULNERABLE:
        return vulnerability.state.modified_date

    return None


async def get_open_vulnerabilities_specific_by_type(
    loaders: Dataloaders,
    finding_id: str,
) -> dict[VulnerabilityType, list[Vulnerability]]:
    vulns = await loaders.finding_vulnerabilities_released_nzr.load(finding_id)
    vulns_by_type: dict[VulnerabilityType, list[Vulnerability]] = {
        VulnerabilityType.LINES: [],
        VulnerabilityType.INPUTS: [],
        VulnerabilityType.PORTS: [],
    }
    for vuln in vulns:
        if vuln.state.status == VulnerabilityStateStatus.VULNERABLE and vuln.type in vulns_by_type:
            vulns_by_type[vuln.type].append(vuln)

    return vulns_by_type


def _get_vuln_detailed_where(vulnerability: Vulnerability) -> str:
    vuln_where = vulnerability.state.where
    if adv := vulnerability.state.advisories:
        vuln_where += f": Package {adv.package} with version {adv.vulnerable_version}"
        if adv.epss:
            vuln_where += f" and EPSS {adv.epss}%"
    return vuln_where


def group_vulnerabilities_info(
    vulnerabilities: list[Vulnerability], vulnerabilities_type: VulnerabilityType, finding: Finding
) -> tuple[GroupedVulnerabilitiesInfo, ...]:
    if any(vuln.state.advisories for vuln in vulnerabilities):
        grouped_vulnerabilities = tuple(
            map(
                lambda vuln: GroupedVulnerabilitiesInfo(
                    where=reports_utils.filter_context(_get_vuln_detailed_where(vuln)),
                    specific=reports_utils.filter_context(vuln.state.specific),
                    commit_hash=vuln.state.commit or "",
                    severity_score=vuln.severity_score.threat_score
                    if vuln.severity_score
                    else vulns_utils.get_severity_threat_score(vuln, finding),
                ),
                vulnerabilities,
            ),
        )
    else:
        grouped_vulnerabilities = tuple(
            map(
                lambda vuln: GroupedVulnerabilitiesInfo(
                    where=reports_utils.filter_context(vuln.state.where),
                    specific=reports_utils.filter_context(vuln.state.specific),
                    commit_hash=vuln.state.commit or "",
                    severity_score=vuln.severity_score.threat_score
                    if vuln.severity_score
                    else vulns_utils.get_severity_threat_score(vuln, finding),
                ),
                vulns_utils.group_specific(vulnerabilities, vulnerabilities_type),
            ),
        )

    return grouped_vulnerabilities


async def get_grouped_vulnerabilities_info(
    loaders: Dataloaders,
    finding: Finding,
) -> FindingGroupedVulnerabilitiesInfo:
    vulnerabilities_by_type = await get_open_vulnerabilities_specific_by_type(loaders, finding.id)

    ports_vulnerabilities = vulnerabilities_by_type[VulnerabilityType.PORTS]
    lines_vulnerabilities = vulnerabilities_by_type[VulnerabilityType.LINES]
    inputs_vulnerabilities = vulnerabilities_by_type[VulnerabilityType.INPUTS]

    if ports_vulnerabilities:
        grouped_ports_vulnerabilities = group_vulnerabilities_info(
            ports_vulnerabilities, VulnerabilityType.PORTS, finding
        )
    else:
        grouped_ports_vulnerabilities = tuple()

    if lines_vulnerabilities:
        grouped_lines_vulnerabilities = group_vulnerabilities_info(
            lines_vulnerabilities, VulnerabilityType.LINES, finding
        )
    else:
        grouped_lines_vulnerabilities = tuple()

    if inputs_vulnerabilities:
        grouped_inputs_vulnerabilities = group_vulnerabilities_info(
            inputs_vulnerabilities, VulnerabilityType.INPUTS, finding
        )
    else:
        grouped_inputs_vulnerabilities = tuple()

    return FindingGroupedVulnerabilitiesInfo(
        grouped_ports_vulnerabilities=grouped_ports_vulnerabilities,
        grouped_lines_vulnerabilities=grouped_lines_vulnerabilities,
        grouped_inputs_vulnerabilities=grouped_inputs_vulnerabilities,
        where="-",
    )


def get_treatments_count(
    vulnerabilities: Iterable[Vulnerability],
) -> Treatments:
    treatment_counter = Counter(
        vuln.treatment.status
        for vuln in vulnerabilities
        if vuln.treatment and vuln.state.status == VulnerabilityStateStatus.VULNERABLE
    )
    return Treatments(
        accepted=treatment_counter[TreatmentStatus.ACCEPTED],
        accepted_undefined=treatment_counter[TreatmentStatus.ACCEPTED_UNDEFINED],
        in_progress=treatment_counter[TreatmentStatus.IN_PROGRESS],
        untreated=treatment_counter[TreatmentStatus.UNTREATED],
    )


def get_zr_count(
    vulnerabilities: Iterable[Vulnerability],
) -> ZeroRisk:
    zero_risk_counter = Counter(
        vuln.zero_risk.status
        for vuln in vulnerabilities
        if vuln.zero_risk and vuln.state.status == VulnerabilityStateStatus.VULNERABLE
    )
    return ZeroRisk(
        confirmed=zero_risk_counter[VulnerabilityZeroRiskStatus.CONFIRMED],
        rejected=zero_risk_counter[VulnerabilityZeroRiskStatus.REJECTED],
        requested=zero_risk_counter[VulnerabilityZeroRiskStatus.REQUESTED],
    )


def group_vulnerabilities(
    vulnerabilities: Iterable[Vulnerability],
) -> list[Vulnerability]:
    """Group vulnerabilities by specific field."""
    vuln_types = (
        VulnerabilityType.LINES,
        VulnerabilityType.PORTS,
        VulnerabilityType.INPUTS,
    )
    vuln_states = (
        VulnerabilityStateStatus.VULNERABLE,
        VulnerabilityStateStatus.SAFE,
        VulnerabilityStateStatus.SUBMITTED,
        VulnerabilityStateStatus.REJECTED,
    )
    total_vulnerabilities: dict[str, dict[str, list[Vulnerability]]] = {}
    result_vulns: list[Vulnerability] = []
    for vuln_type in vuln_types:
        total_vulnerabilities[vuln_type] = {}
        for vuln_state in vuln_states:
            total_vulnerabilities[vuln_type][vuln_state] = []

    for vuln in vulnerabilities:
        total_vulnerabilities[vuln.type][vuln.state.status].append(vuln)

    for vuln_type in vuln_types:
        for vuln_state in vuln_states:
            grouped_vulns = vulns_utils.group_specific(
                total_vulnerabilities[vuln_type][vuln_state],
                vuln_type,
            )
            result_vulns.extend(grouped_vulns)

    return result_vulns


async def mask_vulnerability(
    *,
    email: str,
    finding_id: str,
    vulnerability: Vulnerability,
) -> None:
    if vulnerability.state.status == VulnerabilityStateStatus.DELETED:
        # Vulnerabilities in the MASKED state will be archived by Streams
        # for analytics purposes
        await vulns_model.update_historic_entry(
            current_value=vulnerability,
            entry=vulnerability.state._replace(
                modified_by=email,
                modified_date=datetime_utils.get_utc_now(),
                status=VulnerabilityStateStatus.MASKED,
            ),
            finding_id=finding_id,
            vulnerability_id=vulnerability.id,
            force_update=True,
        )
    await vulns_model.remove(vulnerability_id=vulnerability.id)


async def update_entries(modified_by: str, vulnerability: Vulnerability) -> None:
    modified_date = datetime_utils.get_utc_now()
    vuln_state_entry = vulnerability.state._replace(
        modified_by=modified_by,
        modified_date=modified_date,
        status=VulnerabilityStateStatus.VULNERABLE,
        reasons=[VulnerabilityStateReason.NO_JUSTIFICATION],
        other_reason=None,
    )

    await vulns_model.update_historic_entry(
        current_value=vulnerability,
        finding_id=vulnerability.finding_id,
        entry=vuln_state_entry,
        vulnerability_id=vulnerability.id,
    )

    if vulnerability.treatment is not None:
        vuln_treatment_entry = vulnerability.treatment._replace(
            modified_by=modified_by,
            modified_date=modified_date,
        )

        await vulns_model.update_treatment(
            current_value=vulnerability._replace(state=vuln_state_entry),
            finding_id=vulnerability.finding_id,
            vulnerability_id=vulnerability.id,
            treatment=vuln_treatment_entry,
        )


async def confirm_vulnerabilities(
    *,
    loaders: Dataloaders,
    vuln_ids: set[str],
    finding_id: str,
    modified_by: str,
) -> list[Vulnerability]:
    finding_vulns = await loaders.finding_vulnerabilities.load(finding_id)
    vulnerabilities = filter_vulns_by_id_in_findings(finding_vulns, vuln_ids)
    for vulnerability in vulnerabilities:
        validations.validate_submitted(vulnerability)

    finding = await findings_utils.get_finding(loaders, finding_id)

    formatted_evidence = findings_utils.get_formatted_evidence(finding)

    has_evidence_draft = any(
        bool(evidence["url"]) for evidence in formatted_evidence.values() if evidence["is_draft"]
    )

    has_evidence = any(
        bool(evidence["url"])
        for evidence in formatted_evidence.values()
        if evidence["is_draft"] is False
    )

    has_released_vuln = any(
        vuln
        for vuln in finding_vulns
        if vuln.state.status
        in {
            VulnerabilityStateStatus.VULNERABLE,
            VulnerabilityStateStatus.SAFE,
        }
    )

    if not has_evidence and not has_released_vuln:
        if has_evidence_draft:
            raise PendingEvidenceDraft.new()

        raise MissingApprovedEvidence.new()

    await collect(
        tuple(
            update_entries(modified_by=modified_by, vulnerability=vulnerability)
            for vulnerability in vulnerabilities
        ),
    )
    await collect(
        set_snippet_remote(vulnerability.id)
        for vulnerability in vulnerabilities
        if vulnerability.type == VulnerabilityType.LINES
        and vulnerability.state.status
        in (
            VulnerabilityStateStatus.VULNERABLE,
            VulnerabilityStateStatus.SUBMITTED,
            VulnerabilityStateStatus.MASKED,
            VulnerabilityStateStatus.REJECTED,
        )
    )

    return vulnerabilities


async def reject_vulnerabilities(
    *,
    loaders: Dataloaders,
    vuln_ids: set[str],
    finding_id: str,
    modified_by: str,
    reasons: set[VulnerabilityStateReason],
    other_reason: str | None,
) -> None:
    if VulnerabilityStateReason.OTHER not in reasons and other_reason:
        raise InvalidParameter("justification")
    if VulnerabilityStateReason.OTHER in reasons and not other_reason:
        raise InvalidParameter("other_reason")
    if other_reason is not None:
        validations.validate_justification_length(other_reason)
    vulnerabilities = await get_by_finding_and_vuln_ids(loaders, finding_id, vuln_ids)
    for vulnerability in vulnerabilities:
        validations.validate_submitted(vulnerability)

    await collect(
        tuple(
            vulns_model.update_historic_entry(
                current_value=vulnerability,
                finding_id=vulnerability.finding_id,
                vulnerability_id=vulnerability.id,
                entry=vulnerability.state._replace(
                    modified_by=modified_by,
                    modified_date=datetime_utils.get_utc_now(),
                    status=VulnerabilityStateStatus.REJECTED,
                    reasons=list(reasons),
                    other_reason=other_reason,
                ),
            )
            for vulnerability in vulnerabilities
        ),
    )


async def reject_vulnerabilities_zero_risk(
    *,
    loaders: Dataloaders,
    vuln_ids: set[str],
    finding_id: str,
    user_info: dict[str, Any],
    justification: str,
) -> None:
    validations.validate_justification_length(justification)
    vulnerabilities = await get_by_finding_and_vuln_ids(loaders, finding_id, vuln_ids)
    vulnerabilities = [validations.validate_zero_risk_requested(vuln) for vuln in vulnerabilities]

    comment_id = str(round(time() * 1000))
    user_email = str(user_info["user_email"])
    comment_data = FindingComment(
        finding_id=finding_id,
        content=justification,
        comment_type=CommentType.ZERO_RISK,
        id=comment_id,
        email=user_email,
        full_name=" ".join([user_info["first_name"], user_info["last_name"]]),
        creation_date=datetime_utils.get_utc_now(),
        parent_id="0",
    )
    await comments_domain.add(loaders, comment_data)
    await collect(
        vulns_model.update_historic_entry(
            current_value=vuln,
            finding_id=vuln.finding_id,
            vulnerability_id=vuln.id,
            entry=VulnerabilityZeroRisk(
                comment_id=comment_id,
                modified_by=user_email,
                modified_date=datetime_utils.get_utc_now(),
                status=VulnerabilityZeroRiskStatus.REJECTED,
            ),
        )
        for vuln in vulnerabilities
    )


async def request_verification(
    vulnerability: Vulnerability,
    modified_by: str,
    justification: str,
) -> None:
    await vulns_model.update_historic_entry(
        current_value=vulnerability,
        finding_id=vulnerability.finding_id,
        vulnerability_id=vulnerability.id,
        entry=VulnerabilityVerification(
            modified_by=modified_by,
            modified_date=datetime_utils.get_utc_now(),
            status=VulnerabilityVerificationStatus.REQUESTED,
            justification=justification,
        ),
    )


async def request_vulnerabilities_zero_risk(
    *,
    loaders: Dataloaders,
    vuln_ids: set[str],
    finding_id: str,
    user_info: dict[str, Any],
    justification: str,
) -> None:
    validations.validate_justification_length(justification)
    vulnerabilities = await get_by_finding_and_vuln_ids(loaders, finding_id, vuln_ids)
    for vuln in vulnerabilities:
        validations.validate_non_zero_risk_requested(vuln)
        validations.validate_released(vuln)

    comment_id = str(round(time() * 1000))
    user_email = str(user_info["user_email"])
    comment_data = FindingComment(
        finding_id=finding_id,
        content=justification,
        comment_type=CommentType.ZERO_RISK,
        id=comment_id,
        email=user_email,
        full_name=" ".join([user_info["first_name"], user_info["last_name"]]),
        creation_date=datetime_utils.get_utc_now(),
        parent_id="0",
    )
    await comments_domain.add(loaders, comment_data)
    await collect(
        vulns_model.update_historic_entry(
            current_value=vuln,
            finding_id=vuln.finding_id,
            vulnerability_id=vuln.id,
            entry=VulnerabilityZeroRisk(
                comment_id=comment_id,
                modified_by=user_email,
                modified_date=datetime_utils.get_utc_now(),
                status=VulnerabilityZeroRiskStatus.REQUESTED,
            ),
        )
        for vuln in vulnerabilities
    )
    await tickets_domain.request_vulnerability_zero_risk(
        loaders=loaders,
        finding_id=finding_id,
        justification=justification,
        requester_email=user_email,
        vulnerabilities=vulnerabilities,
    )


def get_updated_manager_mail_content(vulnerabilities: dict[str, list[Item]]) -> str:
    mail_content = ""
    vuln_type_translation = {
        "ports": {"where": "host", "specific": "port"},
        "lines": {"where": "path", "specific": "line"},
        "inputs": {"where": "url", "specific": "field"},
    }
    for vuln_type in ["ports", "lines", "inputs"]:
        type_vulns = vulnerabilities.get(vuln_type)
        if type_vulns:
            mail_content += "\n".join(
                [
                    f"{vuln[vuln_type_translation[vuln_type]['where']]} "
                    f"({vuln[vuln_type_translation[vuln_type]['specific']]})"
                    for vuln in type_vulns
                ],
            )
            mail_content += "\n"

    return mail_content


async def should_send_update_treatment(
    *,
    loaders: Dataloaders,
    assigned: str,
    finding_id: str,
    finding_title: str,
    group_name: str,
    justification: str,
    treatment: str,
    updated_vulns: Iterable[Vulnerability],
    modified_by: str,
) -> None:
    translations: dict[str, str] = {
        "IN_PROGRESS": "In progress",
        "ACCEPTED": "Temporarily accepted",
    }
    if treatment in translations:
        vulns_grouped = group_vulnerabilities(updated_vulns)
        vulns_roots = await loaders.root.load_many(
            [
                RootRequest(group_name=group_name, root_id=vuln.root_id or "")
                for vuln in vulns_grouped
            ],
        )
        vulns_data = vulns_utils.format_vulnerabilities(vulns_grouped, vulns_roots)
        mail_content = get_updated_manager_mail_content(vulns_data)
        schedule(
            vulns_mail.send_mail_updated_treatment(
                loaders=loaders,
                assigned=assigned,
                finding_id=finding_id,
                finding_title=finding_title,
                group_name=group_name,
                justification=justification,
                treatment=translations[treatment],
                vulnerabilities=mail_content,
                modified_by=modified_by,
            ),
        )


async def update_issue(
    issue_url: str,
    loaders: Dataloaders,
    vulnerability_id: str,
    webhook_url: str | None,
) -> None:
    vulnerability = await get_vulnerability(loaders, vulnerability_id)
    await vulns_model.update_metadata(
        finding_id=vulnerability.finding_id,
        vulnerability_id=vulnerability.id,
        metadata=VulnerabilityMetadataToUpdate(
            bug_tracking_system_url=issue_url,
            webhook_url=webhook_url,
        ),
    )


@validate_severity_range_deco("custom_severity", min_value=Decimal(-1000), max_value=Decimal(1000))
@validate_url_deco("bug_tracking_system_url")
@validate_length_deco("bug_tracking_system_url", max_length=80)
@validate_length_deco("tags_to_append", max_length=30)
async def update_metadata(
    *,
    loaders: Dataloaders,
    vulnerability_id: str,
    finding_id: str,
    bug_tracking_system_url: str | None,
    custom_severity: int | None,
    tags_to_append: list[str] | None,
) -> None:
    vulnerability = await get_vulnerability(loaders, vulnerability_id)
    all_tags = []
    if vulnerability.tags:
        all_tags.extend(vulnerability.tags)
    if tags_to_append:
        all_tags.extend(tags_to_append)
    await vulns_model.update_metadata(
        finding_id=finding_id,
        vulnerability_id=vulnerability_id,
        metadata=VulnerabilityMetadataToUpdate(
            bug_tracking_system_url=bug_tracking_system_url,
            custom_severity=None if custom_severity is None else custom_severity,
            tags=sorted(list(set(all_tags))),
        ),
    )


async def update_metadata_and_state(
    *,
    vulnerability: Vulnerability,
    new_metadata: VulnerabilityMetadataToUpdate,
    new_state: VulnerabilityState,
    finding_policy: OrgFindingPolicy | None = None,
) -> str:
    """Update vulnerability metadata and historics."""
    if (
        vulnerability.state.source != new_state.source
        and vulnerability.state.status == VulnerabilityStateStatus.SAFE
    ):
        await vulns_model.update_historic_entry(
            current_value=vulnerability,
            finding_id=vulnerability.finding_id,
            vulnerability_id=vulnerability.id,
            entry=VulnerabilityState(
                modified_by=vulnerability.state.modified_by,
                modified_date=vulnerability.state.modified_date,
                source=new_state.source,
                status=vulnerability.state.status,
                reasons=vulnerability.state.reasons,
                tool=vulnerability.state.tool,
                commit=vulnerability.state.commit,
                where=vulnerability.state.where,
                specific=vulnerability.state.specific,
            ),
        )
    elif (
        vulnerability.state.status != new_state.status
        or (
            vulnerability.state.tool != new_state.tool
            and vulnerability.state.status
            in {
                VulnerabilityStateStatus.VULNERABLE,
                VulnerabilityStateStatus.SUBMITTED,
            }
        )
        or vulnerability.state.source != new_state.source
    ):
        await vulns_model.update_historic_entry(
            current_value=vulnerability,
            finding_id=vulnerability.finding_id,
            vulnerability_id=vulnerability.id,
            entry=new_state,
        )
    is_vuln_treatment_not_accepted_undefined = (
        vulnerability.treatment
        and vulnerability.treatment.status != TreatmentStatus.ACCEPTED_UNDEFINED
    )
    if (
        vulnerability.state.status != new_state.status
        and finding_policy
        and new_state.status == VulnerabilityStateStatus.VULNERABLE
        and finding_policy.state.status == PolicyStateStatus.APPROVED
        and is_vuln_treatment_not_accepted_undefined
    ):
        treatment_to_update = vulns_utils.get_treatment_from_org_finding_policy(
            modified_date=new_state.modified_date,
            user_email=finding_policy.state.modified_by,
        )
        await vulns_model.update_treatment(
            current_value=vulnerability,
            finding_id=vulnerability.finding_id,
            vulnerability_id=vulnerability.id,
            treatment=treatment_to_update[0],
        )
        await vulns_model.update_treatment(
            current_value=vulnerability._replace(treatment=treatment_to_update[0]),
            finding_id=vulnerability.finding_id,
            vulnerability_id=vulnerability.id,
            treatment=treatment_to_update[1],
        )

    await vulns_model.update_metadata(
        finding_id=vulnerability.finding_id,
        vulnerability_id=vulnerability.id,
        metadata=new_metadata,
    )
    if vulnerability.type == VulnerabilityType.LINES and vulnerability.state.status in (
        VulnerabilityStateStatus.VULNERABLE,
        VulnerabilityStateStatus.SUBMITTED,
        VulnerabilityStateStatus.MASKED,
        VulnerabilityStateStatus.REJECTED,
    ):
        await set_snippet_remote(vulnerability.id)
    return vulnerability.id


async def mark_as_safe(
    *,
    loaders: Dataloaders,
    modified_date: datetime,
    closed_vulns_ids: list[str],
    vulns_to_close_from_file: list[Vulnerability],
    user_info: dict[str, str],
    closing_reason: VulnerabilityStateReason,
) -> None:
    list_closed_vulns = await get_vulnerabilities(
        loaders,
        sorted(closed_vulns_ids),
        clear_loader=True,
    )
    modified_by = user_info["user_email"]

    await collect(
        update_metadata_and_state(
            vulnerability=vuln_to_close,
            new_metadata=VulnerabilityMetadataToUpdate(
                cwe_ids=close_item.cwe_ids,
                stream=close_item.stream if close_item.type == VulnerabilityType.INPUTS else None,
                severity_score=close_item.severity_score,
            )
            if close_item
            else VulnerabilityMetadataToUpdate(),
            new_state=VulnerabilityState(
                commit=close_item.state.commit
                if close_item and close_item.type == VulnerabilityType.LINES
                else None
                if close_item and close_item.type != VulnerabilityType.LINES
                else vuln_to_close.state.commit,
                modified_by=modified_by,
                modified_date=modified_date,
                other_reason=vuln_to_close.state.other_reason,
                reasons=[closing_reason],
                source=vuln_to_close.state.source,
                specific=vuln_to_close.state.specific,
                status=VulnerabilityStateStatus.SAFE,
                tool=close_item.state.tool if close_item else vuln_to_close.state.tool,
                where=vuln_to_close.state.where,
            ),
        )
        for vuln_to_close, close_item in zip(
            list_closed_vulns,
            vulns_to_close_from_file or cycle([None]),
            strict=False,
        )
    )


async def mark_as_verified(
    vulnerability: Vulnerability,
    modified_by: str,
    justification: str | None = "",
) -> None:
    await vulns_model.update_historic_entry(
        current_value=vulnerability,
        finding_id=vulnerability.finding_id,
        vulnerability_id=vulnerability.id,
        entry=VulnerabilityVerification(
            modified_by=modified_by,
            modified_date=datetime_utils.get_utc_now(),
            status=VulnerabilityVerificationStatus.VERIFIED,
            justification=justification,
        ),
    )


async def get_reattack_requester(
    loaders: Dataloaders,
    vuln: Vulnerability,
) -> str | None:
    historic_verification = await loaders.finding_historic_verification.load(
        FindingRequest(finding_id=vuln.finding_id, group_name=vuln.group_name),
    )
    reversed_historic_verification = list(reversed(historic_verification))
    for verification in reversed_historic_verification:
        if (
            verification.status == FindingVerificationStatus.REQUESTED
            and verification.vulnerability_ids is not None
            and vuln.id in verification.vulnerability_ids
        ):
            return verification.modified_by

    return None


async def get_last_requested_reattack_date(
    loaders: Dataloaders,
    vuln: Vulnerability,
) -> datetime | None:
    if not vuln.verification:
        return None
    if vuln.verification.status == VulnerabilityVerificationStatus.REQUESTED:
        return vuln.verification.modified_date

    historic = await loaders.vulnerability_historic_verification.load(
        VulnerabilityRequest(
            vulnerability_id=vuln.id,
            finding_id=vuln.finding_id,
        ),
    )

    return next(
        (
            verification.modified_date
            for verification in reversed(historic)
            if verification.status == VulnerabilityVerificationStatus.REQUESTED
        ),
        None,
    )


async def get_last_reattack_date(
    loaders: Dataloaders,
    vuln: Vulnerability,
) -> datetime | None:
    if not vuln.verification:
        return None
    if vuln.verification.status == VulnerabilityVerificationStatus.VERIFIED:
        return vuln.verification.modified_date

    historic = await loaders.vulnerability_historic_verification.load(
        VulnerabilityRequest(
            vulnerability_id=vuln.id,
            finding_id=vuln.finding_id,
        ),
    )

    return next(
        (
            verification.modified_date
            for verification in reversed(historic)
            if verification.status == VulnerabilityVerificationStatus.VERIFIED
        ),
        None,
    )


async def _get_vulnerabilities_connection(
    *,
    finding_id: str,
    loaders: Dataloaders,
) -> VulnerabilitiesConnection:
    return await loaders.finding_vulnerabilities_released_nzr_c.load(
        FindingVulnerabilitiesZrRequest(
            finding_id=finding_id,
            paginate=False,
            state_status=VulnerabilityStateStatus.VULNERABLE,
        ),
    )


async def get_zr_vulnerabilities_connection(
    *,
    finding_id: str,
    loaders: Dataloaders,
) -> VulnerabilitiesConnection:
    return await loaders.finding_vulnerabilities_released_zr_c.load(
        FindingVulnerabilitiesZrRequest(
            finding_id=finding_id,
            paginate=False,
            state_status=VulnerabilityStateStatus.VULNERABLE,
        ),
    )


async def update_description(
    loaders: Dataloaders,
    vulnerability_id: str,
    description: VulnerabilityDescriptionToUpdate,
    stakeholder_email: str,
) -> None:
    if all(attribute is None for attribute in description):
        raise RequiredFieldToBeUpdate()

    vulnerability = await get_vulnerability(loaders, vulnerability_id)
    updated_commit = validate_and_get_updated_commit(
        vulnerability=vulnerability,
        description=description,
    )
    updated_specific = validate_and_get_updated_specific(
        vulnerability=vulnerability,
        description=description,
    )
    updated_source = validate_and_get_updated_source(
        vulnerability=vulnerability,
        description=description,
    )
    updated_where = validate_and_get_updated_where(
        vulnerability=vulnerability,
        description=description,
    )

    if any([description.specific is not None, description.where is not None]):
        vulnerabilities_connection: VulnerabilitiesConnection
        zr_vulnerabilities_connection: VulnerabilitiesConnection
        (
            vulnerabilities_connection,
            zr_vulnerabilities_connection,
        ) = await collect(
            (
                _get_vulnerabilities_connection(
                    finding_id=vulnerability.finding_id,
                    loaders=loaders,
                ),
                get_zr_vulnerabilities_connection(
                    finding_id=vulnerability.finding_id,
                    loaders=loaders,
                ),
            ),
        )

        validate_uniqueness(
            finding_vulns_data=tuple(
                edge.node
                for edge in vulnerabilities_connection.edges + zr_vulnerabilities_connection.edges
            ),
            vulnerability_where=updated_where,
            vulnerability_specific=updated_specific,
            vulnerability_type=vulnerability.type,
            vulnerability_id=vulnerability_id,
        )
        await vulns_utils.validate_vulnerability_in_toe(
            loaders,
            vulnerability._replace(
                state=vulnerability.state._replace(
                    commit=updated_commit,
                    specific=updated_specific,
                    source=updated_source,
                    where=updated_where,
                ),
            ),
            index=0,
        )

    if not (
        updated_commit == vulnerability.state.commit
        and updated_source == vulnerability.state.source
        and updated_specific == vulnerability.state.specific
        and updated_where == vulnerability.state.where
    ):
        await vulns_model.update_historic_entry(
            current_value=vulnerability,
            entry=vulnerability.state._replace(
                commit=updated_commit,
                modified_by=stakeholder_email,
                modified_date=datetime_utils.get_utc_now(),
                source=updated_source,
                specific=updated_specific,
                where=updated_where,
            ),
            finding_id=vulnerability.finding_id,
            vulnerability_id=vulnerability.id,
        )
        if (
            (
                updated_where != vulnerability.state.where
                or updated_specific != vulnerability.state.specific
            )
            and vulns_utils.is_machine_vuln(vulnerability)
            and vulnerability.skims_method is not None
        ):
            with suppress(InvalidParameter):
                await vulns_model.update_metadata(
                    finding_id=vulnerability.finding_id,
                    root_id=vulnerability.root_id,
                    vulnerability_id=vulnerability.id,
                    metadata=VulnerabilityMetadataToUpdate(
                        hash=await get_hash_from_machine_vuln(
                            loaders,
                            await get_vulnerability(loaders, vulnerability.id, clear_loader=True),
                        ),
                    ),
                )


async def update_severity_score(
    loaders: Dataloaders,
    vulnerability_id: str,
    cvss_vector: str,
    cvss4_vector: str,
) -> None:
    cvss_utils.validate_cvss_vector(cvss_vector)
    cvss_utils.validate_cvss4_vector(cvss4_vector)

    vulnerability = await get_vulnerability(loaders, vulnerability_id)
    validations.severity_can_be_directly_updated(vulnerability)

    severity_score = cvss_utils.get_severity_score_from_cvss_vector(cvss_vector, cvss4_vector)
    await vulns_model.update_metadata(
        finding_id=vulnerability.finding_id,
        vulnerability_id=vulnerability_id,
        metadata=VulnerabilityMetadataToUpdate(severity_score=severity_score),
    )


async def request_severity_score_update(
    *,
    loaders: Dataloaders,
    vulnerability_id: str,
    cvss_vector: str,
    cvss4_vector: str,
    user_email: str,
) -> None:
    cvss_utils.validate_cvss_vector(cvss_vector)
    cvss_utils.validate_cvss4_vector(cvss4_vector)

    vulnerability = await get_vulnerability(loaders, vulnerability_id)
    validations.severity_update_can_be_requested(vulnerability)
    validations.severity_update_not_requested(vulnerability)

    proposed_severity = VulnerabilitySeverityProposal(
        modified_by=user_email,
        severity_score=cvss_utils.get_severity_score_from_cvss_vector(cvss_vector, cvss4_vector),
        modified_date=datetime_utils.get_utc_now(),
        status=VulnerabilitySeverityProposalStatus.REQUESTED,
    )
    await vulns_model.update_historic_entry(
        current_value=vulnerability,
        finding_id=vulnerability.finding_id,
        vulnerability_id=vulnerability_id,
        entry=vulnerability.state._replace(
            proposed_severity=proposed_severity,
        ),
    )


async def approve_severity_score_update(
    *, loaders: Dataloaders, vulnerability_id: str, modified_by: str
) -> None:
    vulnerability = await get_vulnerability(loaders, vulnerability_id)

    validations.severity_request_can_be_handled(vulnerability)

    previous_proposal = validations.severity_update_requested(vulnerability)
    approved_proposal = VulnerabilitySeverityProposal(
        severity_score=previous_proposal.severity_score,
        modified_by=modified_by,
        modified_date=datetime_utils.get_utc_now(),
        status=VulnerabilitySeverityProposalStatus.APPROVED,
    )
    await vulns_model.update_metadata(
        finding_id=vulnerability.finding_id,
        metadata=VulnerabilityMetadataToUpdate(severity_score=previous_proposal.severity_score),
        vulnerability_id=vulnerability_id,
    )

    return await vulns_model.update_historic_entry(
        current_value=vulnerability,
        finding_id=vulnerability.finding_id,
        entry=vulnerability.state._replace(
            modified_by=modified_by,
            modified_date=datetime_utils.get_utc_now(),
            proposed_severity=approved_proposal,
        ),
        vulnerability_id=vulnerability_id,
    )


async def reject_severity_score_update(
    *, loaders: Dataloaders, vulnerability_id: str, modified_by: str, justification: str
) -> None:
    vulnerability = await get_vulnerability(loaders, vulnerability_id)

    validations.severity_request_can_be_handled(vulnerability)

    previous_proposal = validations.severity_update_requested(vulnerability)
    rejected_proposal = VulnerabilitySeverityProposal(
        severity_score=previous_proposal.severity_score,
        modified_by=modified_by,
        modified_date=datetime_utils.get_utc_now(),
        status=VulnerabilitySeverityProposalStatus.REJECTED,
        justification=justification,
    )

    return await vulns_model.update_historic_entry(
        current_value=vulnerability,
        finding_id=vulnerability.finding_id,
        entry=vulnerability.state._replace(
            modified_by=modified_by,
            modified_date=datetime_utils.get_utc_now(),
            proposed_severity=rejected_proposal,
        ),
        vulnerability_id=vulnerability_id,
    )


@validate_updated_commit_deco("vulnerability.type", "description.commit")
def validate_and_get_updated_commit(
    vulnerability: Vulnerability,
    description: VulnerabilityDescriptionToUpdate,
) -> str | None:
    return description.commit if description.commit is not None else vulnerability.state.commit


@validate_sanitized_csv_input_deco(["description.specific"])
@validate_updated_specific_deco("vulnerability.type", "description.specific")
def validate_and_get_updated_specific(
    vulnerability: Vulnerability,
    description: VulnerabilityDescriptionToUpdate,
) -> str:
    if description.specific is not None:
        return description.specific
    return vulnerability.state.specific


@validate_source_deco("description.source")
def validate_and_get_updated_source(
    vulnerability: Vulnerability,
    description: VulnerabilityDescriptionToUpdate,
) -> Source:
    if description.source is not None:
        return description.source
    return vulnerability.state.source


@validate_updated_where_deco("vulnerability.type", "description.where")
def validate_and_get_updated_where(
    vulnerability: Vulnerability,
    description: VulnerabilityDescriptionToUpdate,
) -> str:
    if description.where is not None:
        return description.where
    return vulnerability.state.where


async def get_vulns_filters_by_root_nickname(
    loaders: Dataloaders,
    group_name: str,
    root: str | None,
) -> list:
    path = root or ""
    parts = path.split("/")
    root_nickname = parts[0]
    try:
        root_id = await roots_utils.get_root_id(loaders, group_name, root_nickname)

        if root_id:
            result = [
                {"term": {"root_id.keyword": root_id}},
                {"match": {"type": VulnerabilityType.LINES.value}},
            ]
            path_without_root = "/".join(parts[1:])
            if path_without_root:
                result.append({"match_phrase_prefix": {"state.where": path_without_root}})
            return result
    except RootNotFound as ex:
        LOGGER.exception(ex, extra={"extra": {"root": root}})
    return []


async def deactivate_vulnerabilities(
    *,
    email: str,
    loaders: Dataloaders,
    vulns_nzr: list[Vulnerability],
) -> None:
    non_released_vulns = filter_vulns_utils.filter_non_released_vulns(vulns_nzr)

    await collect(
        [
            remove_vulnerability(
                loaders=loaders,
                finding_id=vuln.finding_id,
                vulnerability_id=vuln.id,
                justification=VulnerabilityStateReason.EXCLUSION,
                email=email,
                existing_vulnerability=vuln,
            )
            for vuln in non_released_vulns
        ],
        workers=32,
    )


def group_vulnerabilities_by_finding_id(
    vulnerabilities: list[Vulnerability],
) -> dict[str, list[Vulnerability]]:
    grouped_vulnerabilities = {
        finding_id: list(vulns)
        for finding_id, vulns in groupby(
            sorted(vulnerabilities, key=lambda vuln: vuln.finding_id),
            key=lambda x: x.finding_id,
        )
    }
    return grouped_vulnerabilities


async def get_content(
    loaders: Dataloaders,
    vulns: list[Vulnerability],
    exclusion_date: datetime,
) -> str:
    wheres = await comments_domain.get_vulns_wheres(loaders, vulns)
    content = (
        f"Regarding vulnerabilities: \n{wheres}\n\nThey "
        + f"were marked as SAFE on {exclusion_date.date()} due to EXCLUSION"
    )
    return content


async def add_comment_by_exclusion(
    loaders: Dataloaders,
    finding_id: str,
    user_email: str,
    vulns: list[Vulnerability],
) -> None:
    comment_id = str(round(time() * 1000))
    exclusion_date = datetime_utils.get_utc_now()
    content = await get_content(
        loaders,
        vulns,
        exclusion_date,
    )
    comment_data = FindingComment(
        finding_id=finding_id,
        comment_type=CommentType.COMMENT,
        content=content,
        parent_id="0",
        id=comment_id,
        email=user_email,
        full_name="Fluid Attacks",
        creation_date=exclusion_date,
    )
    await comments_domain.add(loaders, comment_data)


async def add_comments_by_exclusion(
    *,
    loaders: Dataloaders,
    user_email: str,
    vulns: list[Vulnerability],
) -> None:
    grouped_vulns = group_vulnerabilities_by_finding_id(filter_vulns_utils.filter_open_vulns(vulns))

    for finding_id, vulnerabilities in grouped_vulns.items():
        await add_comment_by_exclusion(loaders, finding_id, user_email, vulnerabilities)


async def close_released_vulnerabilities(
    *,
    email: str,
    loaders: Dataloaders,
    vulns_nzr: list[Vulnerability],
) -> None:
    vulnerable_vulns = filter_vulns_utils.filter_open_vulns(vulns_nzr)
    closing_reason = VulnerabilityStateReason.ROOT_OR_ENVIRONMENT_DEACTIVATED

    await collect(
        [
            vulns_utils.close_vulnerability(
                vulnerability=vuln,
                modified_by=email,
                loaders=loaders,
                closing_reason=closing_reason,
            )
            for vuln in vulnerable_vulns
        ],
        workers=32,
    )

    await add_comments_by_exclusion(loaders=loaders, user_email=email, vulns=vulnerable_vulns)
