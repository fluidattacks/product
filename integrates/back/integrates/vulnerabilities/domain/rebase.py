import logging
import re
from contextlib import suppress

from integrates.custom_exceptions import InvalidParameter
from integrates.custom_utils import datetime as datetime_utils
from integrates.custom_utils.vulnerabilities import is_machine_vuln, validate_vulnerability_in_toe
from integrates.dataloaders import Dataloaders
from integrates.db_model import vulnerabilities as vulns_model
from integrates.db_model.vulnerabilities.enums import (
    VulnerabilityStateReason,
    VulnerabilityStateStatus,
)
from integrates.db_model.vulnerabilities.types import Vulnerability, VulnerabilityMetadataToUpdate
from integrates.db_model.vulnerabilities.update import update_metadata
from integrates.settings.logger import LOGGING
from integrates.vulnerabilities.domain.core import get_vulnerability
from integrates.vulnerabilities.domain.utils import get_hash_from_machine_vuln
from integrates.vulnerabilities.domain.validations import (
    validate_commit_hash_deco,
    validate_lines_specific_deco,
    validate_uniqueness,
    validate_where_deco,
)

logging.config.dictConfig(LOGGING)

# Constants
LOGGER = logging.getLogger(__name__)


def _get_vulnerability_where(
    vuln_to_update: Vulnerability,
    vulnerability_where: str,
) -> str:
    if commit_hash_search := re.search(
        r"\(commit (\w{40})\)",
        vuln_to_update.state.where,
    ):
        vulnerability_where += f" {commit_hash_search.group(0)}"

    return vulnerability_where


@validate_commit_hash_deco("vulnerability_commit")
@validate_lines_specific_deco("vulnerability_specific")
@validate_where_deco("vulnerability_where")
async def rebase(
    *,
    loaders: Dataloaders,
    finding_id: str,
    finding_vulns: tuple[Vulnerability, ...],
    vuln_to_update: Vulnerability,
    vulnerability_commit: str,
    vulnerability_where: str,
    vulnerability_specific: str,
) -> None:
    validate_uniqueness(
        finding_vulns_data=tuple(finding_vulns),
        vulnerability_where=vulnerability_where,
        vulnerability_specific=vulnerability_specific,
        vulnerability_type=vuln_to_update.type,
        vulnerability_id=vuln_to_update.id,
    )
    await validate_vulnerability_in_toe(
        loaders,
        vuln_to_update._replace(
            state=vuln_to_update.state._replace(
                specific=vulnerability_specific,
                where=vulnerability_where,
                commit=vulnerability_commit,
            ),
        ),
        index=0,
    )

    await vulns_model.update_historic_entry(
        current_value=vuln_to_update,
        finding_id=finding_id,
        vulnerability_id=vuln_to_update.id,
        entry=vuln_to_update.state._replace(
            commit=vulnerability_commit,
            specific=vulnerability_specific,
            where=_get_vulnerability_where(vuln_to_update, vulnerability_where),
            modified_date=datetime_utils.get_utc_now(),
            modified_by="rebase@fluidattacks.com",
        ),
    )

    if is_machine_vuln(vuln_to_update) and vuln_to_update.skims_method is not None:
        await update_metadata(
            finding_id=finding_id,
            root_id=vuln_to_update.root_id,
            vulnerability_id=vuln_to_update.id,
            metadata=VulnerabilityMetadataToUpdate(
                hash=await get_hash_from_machine_vuln(
                    loaders,
                    await get_vulnerability(loaders, vuln_to_update.id, clear_loader=True),
                ),
            ),
        )


async def close_vulnerability(
    loaders: Dataloaders,
    vulnerability: Vulnerability,
    vulnerability_commit: str,
    vulnerability_where: str,
    vulnerability_specific: str,
) -> None:
    await vulns_model.update_historic_entry(
        current_value=vulnerability,
        finding_id=vulnerability.finding_id,
        vulnerability_id=vulnerability.id,
        entry=vulnerability.state._replace(
            commit=vulnerability_commit,
            specific=vulnerability_specific,
            where=vulnerability_where,
            modified_date=datetime_utils.get_utc_now(),
            modified_by="rebase@fluidattacks.com",
            reasons=[VulnerabilityStateReason.CONSISTENCY],
            status=VulnerabilityStateStatus.SAFE,
            other_reason=(
                "The content of the vulnerability could not be found in the HEAD commit."
            ),
        ),
    )
    vulnerability = await get_vulnerability(loaders, vulnerability.id, clear_loader=True)
    with suppress(InvalidParameter):
        await update_metadata(
            finding_id=vulnerability.finding_id,
            root_id=vulnerability.root_id,
            vulnerability_id=vulnerability.id,
            metadata=VulnerabilityMetadataToUpdate(
                hash=await get_hash_from_machine_vuln(loaders, vulnerability),
            ),
        )
