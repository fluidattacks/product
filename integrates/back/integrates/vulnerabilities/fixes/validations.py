from integrates.custom_exceptions import (
    ExpectedVulnToBeOfLinesType,
    FindingNotFound,
    RootNotFound,
    UnfixableFinding,
    VulnNotFound,
)
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.findings.types import (
    Finding,
)
from integrates.db_model.roots.types import (
    GitRoot,
    RootRequest,
)
from integrates.db_model.vulnerabilities.enums import (
    VulnerabilityType,
)
from integrates.db_model.vulnerabilities.types import (
    Vulnerability,
)
from integrates.vulnerabilities.fixes.types import (
    FixInputs,
)


def is_omitted_finding(finding: Finding) -> bool:
    """
    Returns True if the Finding is omitted from autofixes and custom fixes

    Args:
        finding (Finding): The Finding to check for

    Returns:
        bool: True if omitted from fixes, False otherwise

    """
    omitted_findings = {
        "009",  # Sensitive information in source code
        "011",  # Use of Software with Known Vulnerabilities
        "020",  # Non-encrypted confidential information
        "079",  # Non-upgradable Dependencies
        "117",  # Unverifiable files
        "119",  # Metadata with sensitive information
        "140",  # Insecure exceptions - Empty or no catch
        "142",  # Sensitive information in source code - API Key
        "215",  # Business information leak - Repository
        "216",  # Business information leak - Source Code
        "222",  # Business information leak - DB
        "223",  # Business information leak - JFROG
        "224",  # Business information leak - AWS
        "226",  # Business information leak - Personal Information
        "247",  # Non-encrypted confidential information - AWS
        "248",  # Non-encrypted confidential information - LDAP
        "249",  # Non-encrypted confidential information - Credentials
        "251",  # Non-encrypted confidential information - JFROG
        "284",  # Non-encrypted confidential information - Base 64
        "316",  # Improper resource allocation - Buffer overflow
        "326",  # Sensitive information in source code - Dependencies
        "359",  # Sensitive information in source code - Credentials
        "385",  # Non-encrypted confidential information - Keys
        "393",  # Use of software with known vulnerabilities in development
        "433",  # Non-encrypted confidential information - Redshift Cluster
        "439",  # Sensitive information in source code - IP
        "441",  # Non-encrypted confidential information - Azure
    }
    keywords_to_omit = (
        "sensitive information",
        "information leak",
        "confidential information",
    )

    return finding.get_criteria_code() in omitted_findings or any(
        keyword in finding.title.lower() for keyword in keywords_to_omit
    )


async def get_and_validate_fixable_vulnerability(
    loaders: Dataloaders,
    vulnerability_id: str,
) -> FixInputs:
    """
    Makes sure the vulnerability and the related root and finding exist, have
    the appropriate types and are not omitted from fixes
    """
    vulnerability: Vulnerability | None = await loaders.vulnerability.load(vulnerability_id)
    if vulnerability is None or vulnerability.state.commit is None:
        raise VulnNotFound()
    if vulnerability.type != VulnerabilityType.LINES:
        raise ExpectedVulnToBeOfLinesType()
    if not vulnerability.root_id:
        raise RootNotFound()

    git_root = await loaders.root.load(RootRequest(vulnerability.group_name, vulnerability.root_id))
    if not git_root or not isinstance(git_root, GitRoot):
        raise RootNotFound()

    finding: Finding | None = await loaders.finding.load(vulnerability.finding_id)
    if finding is None:
        raise FindingNotFound()
    if is_omitted_finding(finding):
        raise UnfixableFinding()

    return FixInputs(vulnerability=vulnerability, finding=finding, git_root=git_root)
