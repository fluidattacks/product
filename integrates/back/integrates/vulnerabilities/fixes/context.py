from typing import (
    Any,
)

from integrates.custom_exceptions import (
    FindingNotFound,
    VulnNotFound,
)


def get_vuln_criteria(criteria: dict[str, Any], finding_code: str) -> dict[str, Any]:
    try:
        return criteria[finding_code]
    except KeyError as exc:
        raise FindingNotFound() from exc


def get_vuln_line(file_content: str, vulnerability_line: int) -> str:
    try:
        return file_content.split("\n")[vulnerability_line - 1]
    except IndexError as exc:
        raise VulnNotFound() from exc


def generate_vuln_context(vuln_criteria: dict[str, Any]) -> str:
    return (
        "# Vulnerability Context:\n"
        f'Title: {vuln_criteria["en"]["title"]}\n'
        f'Description: {vuln_criteria["en"]["description"]}\n'
        f'Recommendation: {vuln_criteria["en"]["recommendation"]}\n'
    )
