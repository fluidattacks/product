from typing import (
    Literal,
    NamedTuple,
    TypeAlias,
)

from integrates.db_model.findings.types import (
    Finding,
)
from integrates.db_model.roots.types import (
    GitRoot,
)
from integrates.db_model.vulnerabilities.types import (
    Vulnerability,
)

ConverseRole: TypeAlias = Literal[
    "assistant",
    "user",
    "system",
]


class FixInputs(NamedTuple):
    vulnerability: Vulnerability
    finding: Finding
    git_root: GitRoot


class SuggestedFixFunction(NamedTuple):
    vulnerability_id: str
    code_imports: str | None
    function: str
    vulnerable_line_content: str


class ConverseMessage(NamedTuple):
    content: str
    role: ConverseRole


class GptTokenParameters(NamedTuple):
    encoding: str
    tokens_per_name: int
    tokens_per_message: int
