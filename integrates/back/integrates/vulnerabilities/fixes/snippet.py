from contextlib import suppress

from fluidattacks_core.serializers.snippet import (
    SnippetViewport,
    make_snippet,
    make_snippet_function,
)

from integrates.custom_exceptions import (
    InvalidFileType,
)
from integrates.vulnerabilities.fixes.estimation import get_tokens_per_message
from integrates.vulnerabilities.fixes.types import ConverseMessage


def get_vuln_snippet(
    file_content: str,
    vulnerability_line: int,
    language: str,
    force: bool = False,
) -> str | None:
    vulnerability_snippet: str | None = None
    snippet = make_snippet_function(
        file_content=file_content,
        language=language,
        viewport=SnippetViewport(
            vulnerability_line,
            show_line_numbers=False,
            highlight_line_number=False,
        ),
    )
    if not snippet and force:
        snippet = make_snippet(
            content=file_content,
            viewport=SnippetViewport(
                vulnerability_line,
                line_context=8,
                show_line_numbers=False,
                highlight_line_number=False,
            ),
        )
    if not snippet:
        return None
    vulnerability_snippet = snippet.content
    with suppress(InvalidFileType, OSError):
        if not vulnerability_snippet:
            return None
        tokens = get_tokens_per_message(
            ConverseMessage(content=vulnerability_snippet, role="user"),
        )
        if tokens > 10000:
            return None

    return vulnerability_snippet
