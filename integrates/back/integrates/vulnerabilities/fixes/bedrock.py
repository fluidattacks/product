import asyncio
from collections.abc import (
    AsyncGenerator,
    Iterable,
)
from typing import TypedDict

import aioboto3
import botocore.exceptions
from openai import AsyncOpenAI
from types_aiobotocore_bedrock_runtime.type_defs import (
    InferenceConfigurationTypeDef,
    MessageTypeDef,
    SystemContentBlockTypeDef,
)

from integrates.context import FI_AWS_REGION_NAME, FI_OPENAI_API_KEY
from integrates.decorators import retry_on_exceptions
from integrates.vulnerabilities.fixes.constants import (
    BEDROCK_MODEL_ID,
    EXPERIMENTAL_MODEL_ID,
    REGION,
)
from integrates.vulnerabilities.fixes.estimation import get_tokens_per_message
from integrates.vulnerabilities.fixes.types import (
    ConverseMessage,
)


class Tokens(TypedDict):
    inputTokens: int
    outputTokens: int
    totalTokens: int


async def converse(
    messages: tuple[ConverseMessage, ...],
    *,
    feature_preview: bool = False,
    max_tokens: int | None = None,
    temperature: float | None = None,
) -> AsyncGenerator[str, None]:
    # Bedrock common parameters
    inference_parameters: InferenceConfigurationTypeDef = {
        "temperature": temperature or 0.8,
        "maxTokens": max_tokens or 4096,
        "stopSequences": ["\n\nHuman:"],
        "topP": 1.0,
    }
    # Custom Claude parameters
    custom_parameters = {
        "top_k": 250,
    }
    # Custom parameters for the feature preview mode
    experimental_custom_parameters = {
        "top_k": 250,
    }

    # Prompts
    system_messages: list[SystemContentBlockTypeDef] = [
        {"text": message.content} for message in messages if message.role == "system"
    ]
    user_messages: list[MessageTypeDef] = [
        {
            "role": message.role,
            "content": [{"text": message.content}],
        }
        for message in messages
        if message.role != "system"
    ]

    async with aioboto3.Session().client(
        service_name="bedrock-runtime",
        region_name=REGION,
    ) as bedrock:
        response = await bedrock.converse_stream(
            modelId=EXPERIMENTAL_MODEL_ID if feature_preview else BEDROCK_MODEL_ID,
            messages=user_messages,
            system=system_messages,
            inferenceConfig=inference_parameters,
            additionalModelRequestFields=experimental_custom_parameters
            if feature_preview
            else custom_parameters,
        )
        async for chunk in response["stream"]:
            if "contentBlockDelta" in chunk:
                yield chunk["contentBlockDelta"]["delta"]["text"]


async def converse_plain(
    messages: Iterable[ConverseMessage], model: str | None = None
) -> tuple[str | None, Tokens | None]:
    model = model or "gpt-4o-mini"
    if sum(get_tokens_per_message(message) for message in messages) > 10000:
        return None, None
    if model.startswith("gpt"):
        ai_client = AsyncOpenAI(
            api_key=(FI_OPENAI_API_KEY),
        )
        response = await ai_client.chat.completions.create(
            messages=[
                {  # type: ignore
                    "content": x.content,
                    "role": x.role,
                    "name": x.role,
                }
                for x in messages
            ],
            model="gpt-4o-mini",
        )
        tokens: Tokens = {
            "inputTokens": 0,
            "outputTokens": 0,
            "totalTokens": 0,
        }
        if response.usage:
            tokens = {
                "inputTokens": response.usage.prompt_tokens,
                "outputTokens": response.usage.completion_tokens,
                "totalTokens": response.usage.total_tokens,
            }
        return response.choices[0].message.content, tokens

    system_messages: list[SystemContentBlockTypeDef] = [
        {"text": message.content} for message in messages if message.role == "system"
    ]
    user_messages: list[MessageTypeDef] = [
        {
            "role": message.role,
            "content": [{"text": message.content}],
        }
        for message in messages
        if message.role != "system"
    ]
    inference_parameters: InferenceConfigurationTypeDef = {
        "temperature": 0.8,
        "maxTokens": 4096,
        "stopSequences": ["\n\nHuman:"],
        "topP": 1.0,
    }
    async with aioboto3.Session().client(
        service_name="bedrock-runtime",
        region_name=FI_AWS_REGION_NAME,
    ) as bedrock:
        try:
            response_b = await retry_on_exceptions(
                exceptions=(Exception,), sleep_seconds=20, max_attempts=10
            )(bedrock.converse)(
                modelId=model,
                messages=user_messages,
                system=system_messages,
                inferenceConfig=inference_parameters,
            )
            return (response_b["output"]["message"]["content"][0]["text"], response_b["usage"])
        except (
            bedrock.exceptions.ThrottlingException,
            botocore.exceptions.ReadTimeoutError,
        ):
            await asyncio.sleep(10)
            return None, None
