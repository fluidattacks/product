"""
Enqueue a refresh_toe_lines sqs task on roots that still dont have
the toe_lines field on db.
This will allow to take advantage of the incremental update on
refreshing the toe lines.

Start Time:         2023-11-01 at 20:38:24 UTC
Finalization Time:  2023-11-01 at 20:39:33 UTC
"""

import logging
import logging.config
import time

from aioextensions import (
    collect,
    run,
)

from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model.groups.enums import (
    GroupManaged,
    GroupService,
)
from integrates.db_model.roots.enums import (
    RootStatus,
)
from integrates.db_model.roots.types import (
    GitRoot,
)
from integrates.organizations import (
    domain as orgs_domain,
)
from integrates.server_async.enqueue import (
    queue_refresh_toe_lines_async,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)
LOGGER_CONSOLE = logging.getLogger("console")


async def process_group(group_name: str) -> None:
    loaders: Dataloaders = get_new_context()
    roots = tuple(
        root
        for root in await loaders.group_roots.load(group_name)
        if root.state.status == RootStatus.ACTIVE
        and isinstance(root, GitRoot)
        and root.cloning.last_successful_cloning
        and (not root.toe_lines or not root.toe_lines.commit)
    )
    if not roots:
        return

    await collect(
        [
            queue_refresh_toe_lines_async(
                group_name=group_name,
                git_root_id=root.id,
                user_email="integrates@fluidattacks.com",
            )
            for root in roots
        ],
        workers=16,
    )
    LOGGER_CONSOLE.info(
        "Group queued",
        extra={
            "extra": {
                "group_name": group_name,
                "roots_len": len(roots),
                "root_ids": [root.id for root in roots],
                "root_nicknames": [root.state.nickname for root in roots],
            },
        },
    )


async def main() -> None:
    loaders: Dataloaders = get_new_context()
    groups = await orgs_domain.get_all_active_groups(loaders)
    enabled_groups = sorted(
        [
            group.name
            for group in groups
            if group.state.service == GroupService.WHITE
            and group.state.managed != GroupManaged.UNDER_REVIEW
        ],
    )
    LOGGER_CONSOLE.info(
        "Groups to process",
        extra={
            "extra": {
                "group_names": enabled_groups,
                "len": len(enabled_groups),
            },
        },
    )

    await collect(
        [process_group(group_name) for group_name in enabled_groups],
        workers=16,
    )


if __name__ == "__main__":
    execution_time = time.strftime("Start Time: %Y-%m-%d at %H:%M:%S UTC")
    LOGGER_CONSOLE.info("%s", execution_time)
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S UTC")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
