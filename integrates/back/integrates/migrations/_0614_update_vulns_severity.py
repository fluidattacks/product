"""
Execution Time:    2024-10-23 at 22:11:36 UTC
Finalization Time: 2024-10-23 at 22:13:01 UTC
Execution Time:    2024-10-24 at 13:53:38 UTC
Finalization Time: 2024-10-24 at 13:54:41 UTC
Execution Time:    2024-10-24 at 14:11:33 UTC
Finalization Time: 2024-10-24 at 14:12:26 UTC
"""

import sys
import time
from decimal import (
    Decimal,
)

from aioextensions import (
    collect,
    run,
)
from boto3.dynamodb.conditions import (
    Key,
)
from psycopg2 import (  # type: ignore[import-untyped]
    sql,
)
from psycopg2.extensions import (  # type: ignore[import-untyped]
    cursor as cursor_cls,
)

from integrates.db_model import (
    TABLE as INTEGRATES_VMS_TABLE,
)
from integrates.db_model.last_sync import (
    get_db_cursor,
)
from integrates.db_model.types import (
    SeverityScore,
)
from integrates.dynamodb import (
    keys,
    operations,
)
from integrates.migrations._0591_update_severity_score_streams import (
    get_severity_score,
    update_table,
)
from integrates.migrations._0596_update_finding_severity_score_stream import (
    ATTACK_COMPLEXITY,
    ATTACK_VECTOR,
    CONFIDENTIALITY_REQUIREMENT,
    EXPLOITABILITY,
    INTEGRITY_IMPACT,
    PRIVILEGES_REQUIRED,
    REMEDIATION_LEVEL,
    REPORT_CONFIDENCE,
    SEVERITY_SCOPE,
    USER_INTERACTION,
)

BACKUP_TABLE_NAME = ""
BACKUP_TABLE = INTEGRATES_VMS_TABLE._replace(name=BACKUP_TABLE_NAME)


def _get_field(metrics: dict[str, Decimal], name: str, mapped_values: dict[Decimal, str]) -> str:
    value = metrics.get(name)
    if value:
        return mapped_values.get(value, "X")
    return "X"


def get_vector_from_metrics(metrics: dict[str, Decimal]) -> str:
    confidentiality_requirement = _get_field(
        metrics,
        "confidentiality_requirement",
        CONFIDENTIALITY_REQUIREMENT,
    )
    integrity_requirement = _get_field(
        metrics,
        "integrity_requirement",
        CONFIDENTIALITY_REQUIREMENT,
    )
    availability_requirement = _get_field(
        metrics,
        "availability_requirement",
        CONFIDENTIALITY_REQUIREMENT,
    )
    modified_attack_complexity = _get_field(
        metrics,
        "modified_attack_complexity",
        ATTACK_COMPLEXITY,
    )
    modified_privileges_required = _get_field(
        metrics,
        "modified_privileges_required",
        PRIVILEGES_REQUIRED,
    )
    modified_user_interaction = _get_field(metrics, "modified_user_interaction", USER_INTERACTION)
    modified_confidentiality_impact = _get_field(
        metrics,
        "modified_confidentiality_impact",
        INTEGRITY_IMPACT,
    )
    modified_integrity_impact = _get_field(metrics, "modified_integrity_impact", INTEGRITY_IMPACT)
    modified_availability_impact = _get_field(
        metrics,
        "modified_availability_impact",
        INTEGRITY_IMPACT,
    )
    vector = (
        f'CVSS:3.1/AV:{_get_field(metrics, "attack_vector",ATTACK_VECTOR)}'
        f'/AC:{_get_field(metrics,"attack_complexity", ATTACK_COMPLEXITY)}'
        f'/PR:{_get_field(metrics,"privileges_required", PRIVILEGES_REQUIRED)}'
        f'/UI:{_get_field(metrics,"user_interaction", USER_INTERACTION)}'
        f'/S:{_get_field(metrics,"severity_scope", SEVERITY_SCOPE)}'
        f'/C:{_get_field(metrics,"confidentiality_impact", INTEGRITY_IMPACT)}'
        f'/I:{_get_field(metrics,"integrity_impact", INTEGRITY_IMPACT)}'
        f'/A:{_get_field(metrics,"availability_impact", INTEGRITY_IMPACT)}'
        f'/E:{_get_field(metrics,"exploitability", EXPLOITABILITY)}'
        f'/RL:{_get_field(metrics,"remediation_level", REMEDIATION_LEVEL)}'
        f'/RC:{_get_field(metrics,"report_confidence", REPORT_CONFIDENCE)}'
        f"/CR:{confidentiality_requirement}"
        f"/IR:{integrity_requirement}"
        f"/AR:{availability_requirement}"
        f'/MAV:{_get_field(metrics,"modified_attack_vector", ATTACK_VECTOR)}'
        f"/MAC:{modified_attack_complexity}"
        f"/MPR:{modified_privileges_required}"
        f"/MUI:{modified_user_interaction}"
        f'/MS:{_get_field(metrics,"modified_severity_scope", SEVERITY_SCOPE)}'
        f"/MC:{modified_confidentiality_impact}"
        f"/MI:{modified_integrity_impact}"
        f"/MA:{modified_availability_impact}"
    )
    if metrics.get("privileges_required") == Decimal("0.68") or metrics.get(
        "privileges_required",
    ) == Decimal("0.50"):
        vector = vector.replace("MS:U", "MS:C")

    if all(x in vector for x in ["/MC:N", "/MI:N", "/MA:N"]):
        return vector.split("/MC:N")[0]

    return vector


async def update_finding(
    finding_severity: tuple[str, SeverityScore | None],
    cursor: cursor_cls,
) -> None:
    finding_id, severity_score = finding_severity
    if severity_score:
        update_table(cursor, finding_id, severity_score, "findings_metadata")


async def get_finding_by_id(
    finding_id: str,
) -> tuple[str, SeverityScore | None]:
    primary_key = keys.build_key(
        facet=BACKUP_TABLE.facets["finding_metadata"],
        values={"id": finding_id},
    )

    key_structure = BACKUP_TABLE.primary_key
    response = await operations.query(
        condition_expression=(
            Key(key_structure.partition_key).eq(primary_key.partition_key)
            & Key(key_structure.sort_key).begins_with(primary_key.sort_key)
        ),
        facets=(BACKUP_TABLE.facets["finding_metadata"],),
        limit=1,
        table=BACKUP_TABLE,
    )

    if not response.items:
        return (finding_id, None)

    metrics: dict[str, Decimal] | None = response.items[0].get("severity")
    if metrics:
        vector_v3 = get_vector_from_metrics(metrics)
        severity_score = get_severity_score(
            response.items[0].get("title"),
            vector_v3,
        )
        if severity_score:
            return (finding_id, severity_score)

    return (finding_id, None)


def get_findings_missing_severity(cursor: cursor_cls) -> list[tuple[str]]:
    cursor.execute(
        sql.SQL(
            """
            SELECT
                id
            FROM
                integrates.findings_metadata
            WHERE
                severity_cvss_v3 is NULL
                AND severity_cvss_v4 is NULL;
            """,
        ),
    )

    return list(cursor.fetchall())


def get_vulns_missing_severity(cursor: cursor_cls) -> list[tuple[str]]:
    cursor.execute(
        sql.SQL(
            """
            SELECT
                finding_id, id
            FROM
                integrates.vulnerabilities_metadata
            WHERE
                severity_cvss_v3 is NULL
                AND severity_cvss_v4 is NULL;
            """,
        ),
    )

    return list(cursor.fetchall())


async def main(*args: str) -> None:
    global BACKUP_TABLE_NAME, BACKUP_TABLE
    BACKUP_TABLE_NAME = args[0].strip() or BACKUP_TABLE_NAME
    BACKUP_TABLE = INTEGRATES_VMS_TABLE._replace(name=BACKUP_TABLE_NAME)
    with get_db_cursor() as cursor:
        __findings = get_findings_missing_severity(cursor)
        _vulnerabilities = get_vulns_missing_severity(cursor)
        _findings = {fin[0] for fin in __findings}
        vulnerabilities = {vuln[0] for vuln in _vulnerabilities}
        findings = _findings.intersection(vulnerabilities)
        findings_severity = await collect(
            (get_finding_by_id(finding) for finding in findings),
            workers=32,
        )
        await collect(
            update_finding(finding_severity, cursor) for finding_severity in findings_severity
        )


if __name__ == "__main__":
    migration_args = sys.argv[1:]
    execution_time = time.strftime("Execution Time:    %Y-%m-%d at %H:%M:%S %Z")
    run(main(*migration_args))
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S %Z")
    print("\n%s\n%s", execution_time, finalization_time)
