"""
Update tier in group state when value is MACHINE or SQUAD

Start Time:    2024-04-19 at 15:24:59 UTC
Finalization Time: 2024-04-19 at 15:29:01 UTC, extra=None

"""

import logging
import logging.config
import time

from aioextensions import (
    collect,
    run,
)

from integrates.custom_utils.groups import get_group
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model import (
    groups as groups_model,
)
from integrates.db_model.groups.enums import (
    GroupTier,
)
from integrates.organizations import (
    domain as orgs_domain,
)
from integrates.organizations import (
    utils as orgs_utils,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)
LOGGER = logging.getLogger("console")


async def process_group(
    loaders: Dataloaders,
    group_name: str,
) -> None:
    group = await get_group(loaders, group_name)
    organization = await orgs_utils.get_organization(loaders, group.organization_id)
    if group.state.tier == GroupTier.MACHINE:  # type: ignore
        tier_update = GroupTier.ESSENTIAL
    elif group.state.tier == GroupTier.SQUAD:  # type: ignore
        tier_update = GroupTier.ADVANCED
    else:
        tier_update = group.state.tier

    await groups_model.update_state(
        group_name=group_name,
        organization_id=organization.id,
        state=group.state._replace(
            comments=group.state.comments,
            modified_date=group.state.modified_date,
            has_essential=group.state.has_essential,
            has_advanced=group.state.has_advanced,
            tier=tier_update,
        ),
    )
    LOGGER.info(
        "Group processed",
        extra={
            "extra": {
                "group_name": group_name,
                "new state": group.state,
            },
        },
    )


async def main() -> None:
    loaders: Dataloaders = get_new_context()
    group_name = sorted(await orgs_domain.get_all_group_names(loaders=loaders))
    LOGGER.info(
        "All groups",
        extra={"extra": {"groups_len": len(group_name)}},
    )
    await collect(
        tuple(
            process_group(
                loaders=loaders,
                group_name=group_name,
            )
            for group_name in group_name
        ),
        workers=1,
    )


if __name__ == "__main__":
    execution_time = time.strftime("Start Time:    %Y-%m-%d at %H:%M:%S UTC")
    LOGGER.info("%s", execution_time)
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S UTC")
    LOGGER.info("\n%s\n%s", execution_time, finalization_time)
