# type: ignore
# NOTE: CONDITION EXPRESSION IN GROUPS AND CREDENTIALS MUST BE DISABLED
# NOTE 2: REMOVE IN GROUPS MODEL MUST BE MODIFIED TO FIND BY ORG ID
"""
Execution Time:    2023-11-15 at 18:35:02 UTC
Finalization Time: 2023-11-15 at 18:35:14 UTC

Execution Time:    2023-11-15 at 19:22:25 UTC
Finalization Time: 2023-11-15 at 19:22:36 UTC

Execution Time:    2023-11-15 at 20:50:33 UTC
Finalization Time: 2023-11-15 at 20:50:39 UTC

Execution Time:    2023-11-15 at 22:50:38 UTC
Finalization Time: 2023-11-15 at 22:50:47 UTC

Execution Time:    2023-11-16 at 13:48:01 UTC
Finalization Time: 2023-11-16 at 13:48:06 UTC

Execution Time:    2023-11-16 at 13:55:30 UTC
Finalization Time: 2023-11-16 at 13:55:38 UTC

Execution Time:    2023-11-16 at 14:03:31 UTC
Finalization Time: 2023-11-16 at 14:03:37 UTC

Execution Time:    2023-11-16 at 14:07:04 UTC
Finalization Time: 2023-11-16 at 14:07:09 UTC

Execution Time:    2023-11-16 at 14:15:55 UTC
Finalization Time: 2023-11-16 at 14:16:05 UTC

Execution Time:    2023-11-16 at 14:24:31 UTC
Finalization Time: 2023-11-16 at 14:24:39 UTC

Execution Time:    2023-11-16 at 14:29:24 UTC
Finalization Time: 2023-11-16 at 14:30:04 UTC

Execution Time:    2023-11-16 at 14:58:28 UTC
Finalization Time: 2023-11-16 at 14:58:45 UTC

Execution Time:    2023-11-16 at 16:22:41 UTC
Finalization Time: 2023-11-16 at 16:23:43 UTC

Execution Time:    2023-11-16 at 17:44:52 UTC
Finalization Time: 2023-11-16 at 17:44:57 UTC

Execution Time:    2023-11-16 at 17:51:57 UTC
Finalization Time: 2023-11-16 at 17:52:03 UTC

Execution Time:    2023-11-16 at 17:57:19 UTC
Finalization Time: 2023-11-16 at 17:58:45 UTC

Execution Time:    2023-11-16 at 18:04:22 UTC
Finalization Time: 2023-11-16 at 18:04:33 UTC

Execution Time:    2023-11-16 at 18:10:38 UTC
Finalization Time: 2023-11-16 at 18:10:49 UTC

Execution Time:    2023-11-16 at 18:14:04 UTC
Finalization Time: 2023-11-16 at 18:14:18 UTC

Execution Time:    2023-11-16 at 19:51:25 UTC
Finalization Time: 2023-11-16 at 19:51:33 UTC

Execution Time:    2023-12-06 at 17:44:46 UTC
Finalization Time: 2023-12-06 at 17:44:54 UTC

Execution Time:    2024-01-11 at 16:09:40 UTC
Finalization Time: 2024-01-11 at 16:09:43 UTC

Execution Time:    2024-06-17 at 16:16:18 UTC
Finalization Time: 2024-06-17 at 16:16:37 UTC
"""

import logging
import logging.config
import time

from aioextensions import (
    collect,
    run,
)
from boto3.dynamodb.conditions import (
    Attr,
)

from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model import (
    TABLE,
)
from integrates.db_model import (
    groups as groups_model,
)
from integrates.db_model import (
    roots as roots_model,
)
from integrates.db_model.findings.types import (
    Finding,
)
from integrates.db_model.group_access.types import (
    GroupStakeholdersAccessRequest,
)
from integrates.db_model.groups.types import (
    Group,
)
from integrates.db_model.organizations.types import (
    Organization,
)
from integrates.db_model.portfolios.types import (
    Portfolio,
)
from integrates.db_model.portfolios.update import (
    update as update_portfolio,
)
from integrates.db_model.roots.types import (
    Root,
)
from integrates.db_model.vulnerabilities.types import (
    Vulnerability,
)
from integrates.dynamodb import (
    keys,
    operations,
)
from integrates.organization_access import (
    domain as orgs_access,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)

# Constants
LOGGER = logging.getLogger(__name__)
LOGGER_CONSOLE = logging.getLogger("console")


async def add_org_access(
    loaders: Dataloaders,
    email: str,
    new_organization_id: str,
    role: str,
) -> None:
    try:
        await orgs_access.add_access(loaders, new_organization_id, email, role)
    except Exception as error:
        logging.error(error)


async def process_organization_stakeholders_access(
    loaders: Dataloaders,
    old_organization_id: str,
    new_organization_id: str,
    group_name: str,
) -> None:
    group_stakeholders = [
        access.email
        for access in await loaders.group_stakeholders_access.load(
            GroupStakeholdersAccessRequest(group_name=group_name),
        )
    ]

    stakeholders_access = await loaders.organization_stakeholders_access.load(old_organization_id)

    new_stakeholders_access = [
        access.email
        for access in await loaders.organization_stakeholders_access.load(new_organization_id)
    ]

    organization_stakeholders = [
        access
        for access in stakeholders_access
        if access.email in group_stakeholders and access.email not in new_stakeholders_access
    ]

    await collect(
        tuple(
            add_org_access(loaders, access.email, new_organization_id, access.state.role)
            for access in organization_stakeholders
        ),
        workers=32,
    )


async def update_roots(
    new_organization_name: str,
    root: Root,
) -> None:
    try:
        new_root = root._replace(organization_name=new_organization_name)
        await roots_model.add(root=new_root)
    except Exception as error:
        logging.error(error)


async def process_roots(
    loaders: Dataloaders,
    group_name: str,
    new_organization_name: str,
) -> None:
    roots = await loaders.group_roots.load(group_name)
    await collect(
        tuple(
            update_roots(
                new_organization_name=new_organization_name,
                root=root,
            )
            for root in roots
        ),
        workers=8,
    )

    LOGGER.info(
        "Roots processed",
        extra={
            "extra": {
                "group_name": group_name,
                "roots": len(roots),
            },
        },
    )


async def process_group(
    group: Group,
    new_organization_id: str,
) -> None:
    try:
        new_group = group._replace(organization_id=new_organization_id)
        await groups_model.remove(group_name=group.name)
        await groups_model.add(group=new_group)
    except Exception as error:
        logging.error(error)

    LOGGER.info(
        "Group processed",
        extra={
            "extra": {
                "group_name": group.name,
                "new_organization_id": new_organization_id,
            },
        },
    )


async def process_vulnerability(new_organization_name: str, vulnerability: Vulnerability) -> None:
    key_structure = TABLE.primary_key
    vulnerability_key = keys.build_key(
        facet=TABLE.facets["vulnerability_metadata"],
        values={
            "finding_id": vulnerability.finding_id,
            "id": vulnerability.id,
        },
    )
    try:
        await operations.update_item(
            condition_expression=Attr(key_structure.partition_key).exists(),
            item={"organization_name": new_organization_name},
            key=vulnerability_key,
            table=TABLE,
        )
    except Exception as error:
        logging.error(error)

    LOGGER.info(
        "Vulnerability processed",
        extra={
            "extra": {
                "finding_id": vulnerability.finding_id,
                "vuln_id": vulnerability.id,
            },
        },
    )


async def process_finding(
    loaders: Dataloaders,
    finding: Finding,
    new_organization_name: str,
) -> None:
    finding_vulnerabilities = await loaders.finding_vulnerabilities_all.load(finding.id)

    await collect(
        tuple(
            process_vulnerability(new_organization_name, vulnerability)
            for vulnerability in finding_vulnerabilities
            if vulnerability.organization_name != new_organization_name
        ),
        workers=100,
    )

    LOGGER.info(
        "Finding processed",
        extra={
            "extra": {
                "finding_id": finding.id,
                "vulnerabilities": len(finding_vulnerabilities),
            },
        },
    )


async def process_findings(
    loaders: Dataloaders,
    group_name: str,
    new_organization_name: str,
) -> None:
    group_findings = await loaders.group_findings_all.load(group_name)
    await collect(
        tuple(
            process_finding(loaders, finding, new_organization_name) for finding in group_findings
        ),
    )


async def update_portfolio_metadata(
    portfolio: Portfolio,
    group_name: str,
) -> None:
    try:
        new_groups = portfolio.groups - {group_name}
        portfolio = portfolio._replace(groups=new_groups)
        await update_portfolio(portfolio=portfolio)
    except Exception as ex:
        logging.error(ex)

    LOGGER.info(
        "Portfolio processed",
        extra={
            "extra": {
                "group_name": group_name,
                "portfolio": portfolio.id,
            },
        },
    )


async def process_portfolios(
    loaders: Dataloaders,
    group_name: str,
) -> None:
    org_portfolios = await loaders.organization_portfolios.load(group_name)

    await collect(
        tuple(
            update_portfolio_metadata(portfolio, group_name)
            for portfolio in org_portfolios
            if group_name in portfolio.groups
        ),
        workers=32,
    )

    LOGGER.info(
        "Portfolios processed",
        extra={
            "extra": {
                "group": group_name,
                "portfolios": len(org_portfolios),
            },
        },
    )


async def process_organization(
    loaders: Dataloaders,
    group_name: str,
    new_organization_id: str,
) -> None:
    group: Group = await loaders.group.load(group_name)
    if group is None:
        LOGGER_CONSOLE.info(
            "group NOT processed",
            extra={
                "extra": {
                    "group_name": group_name,
                },
            },
        )
        return

    old_organization_id = group.organization_id
    old_organization: Organization = await loaders.organization.load(old_organization_id)
    old_organization_name = old_organization.name
    new_organization: Organization = await loaders.organization.load(new_organization_id)
    new_organization_name = new_organization.name

    if old_organization_name == new_organization_name:
        return

    await process_organization_stakeholders_access(
        loaders,
        old_organization_id,
        new_organization_id,
        group_name,
    )
    await process_roots(loaders, group_name, new_organization_name)
    await process_group(group, new_organization_id)
    await process_findings(loaders, group_name, new_organization_name)
    await process_portfolios(loaders, old_organization_name)

    LOGGER_CONSOLE.info(
        "Organization processed",
        extra={
            "extra": {
                "group_name": group_name,
                "organization_name": old_organization_name,
                "new_organization": new_organization_name,
            },
        },
    )


async def main() -> None:
    loaders: Dataloaders = get_new_context()

    await process_organization(loaders, "unittesting", "ORG#c6cecc0e-bb92-4079-8b6d-c4e815c10bb1")


if __name__ == "__main__":
    execution_time = time.strftime("Execution Time:    %Y-%m-%d at %H:%M:%S %Z")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S %Z")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
