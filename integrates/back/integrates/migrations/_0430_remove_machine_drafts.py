# type: ignore
"""
Remove old machine drafts to restart the review process

Execution Time:    2023-08-30 at 16:21:46 UTC
Finalization Time: 2023-08-31 at 00:29:38 UTC
"""

import csv
import time

from aioextensions import (
    collect,
    run,
)

from integrates.dataloaders import (
    get_new_context,
)
from integrates.db_model.enums import (
    Source,
    StateRemovalJustification,
)
from integrates.db_model.vulnerabilities.enums import (
    VulnerabilityStateReason,
    VulnerabilityStateStatus,
)
from integrates.findings.domain import (
    remove_finding,
)
from integrates.organizations.domain import (
    get_all_active_group_names,
)
from integrates.vulnerabilities.domain import (
    remove_vulnerability,
)


async def delete_machine_drafts_locations(
    loaders,
    group: str,
    fin_enum: set[str],
) -> None:
    findings = await loaders.group_findings.load(group)
    findings_numb = [finding for finding in findings if finding.title[:3] in fin_enum]
    findings_vulns = await loaders.finding_vulnerabilities.load_many(
        [finding.id for finding in findings_numb],
    )
    rows = []
    for finding, vulns in zip(findings_numb, findings_vulns, strict=False):
        if len(vulns) == 0:
            continue
        draft_vulns = [
            vuln
            for vuln in vulns
            if (
                (
                    vuln.state.source == Source.MACHINE
                    or vuln.hacker_email == "machine@fluidattacks.com"
                )
                and vuln.skims_method is not None
                and vuln.state.status
                in {
                    VulnerabilityStateStatus.REJECTED,
                    VulnerabilityStateStatus.SUBMITTED,
                }
            )
        ]
        if len(draft_vulns) == len(vulns):
            try:
                await remove_finding(
                    loaders,
                    "flagos@fluidattacks.com",
                    finding.id,
                    StateRemovalJustification.NO_JUSTIFICATION,
                    Source.MACHINE,
                )
                rows.append([group, finding.title[:3], finding.id, "FINDING REMOVED"])
            except Exception:
                rows.append([group, finding.title[:3], finding.id, "ERROR REMOVING"])

        elif len(draft_vulns) > 0:
            try:
                await collect(
                    (
                        remove_vulnerability(
                            loaders=loaders,
                            finding_id=vuln.finding_id,
                            vulnerability_id=vuln.id,
                            justification=VulnerabilityStateReason.CONSISTENCY,
                            email="flagos@fluidattacks.com",
                            include_closed_vuln=True,
                        )
                        for vuln in draft_vulns
                    ),
                    workers=15,
                )
                rows.append([group, finding.title[:3], finding.id, "VULNS REMOVED"])
            except Exception:
                rows.append([group, finding.title[:3], finding.id, "ERROR REMOVING"])

    with open("removed_drafts.csv", "a+", encoding="utf-8") as handler:
        writer = csv.writer(handler)
        writer.writerows(rows)


async def main() -> None:
    loaders = get_new_context()
    groups = sorted(await get_all_active_group_names(loaders))
    findings = {
        "001",
        "004",
        "005",
        "035",
        "048",
        "058",
        "059",
        "060",
        "065",
        "097",
        "098",
        "101",
        "127",
        "128",
        "129",
        "130",
        "133",
        "169",
        "176",
        "182",
        "200",
        "203",
        "207",
        "267",
        "277",
        "280",
        "281",
        "297",
        "300",
        "320",
        "363",
        "371",
        "372",
        "396",
        "401",
        "402",
        "406",
        "407",
        "411",
        "412",
        "413",
        "416",
        "433",
    }
    for group in groups:
        await delete_machine_drafts_locations(loaders, group, findings)


if __name__ == "__main__":
    execution_time = time.strftime("Execution Time:    %Y-%m-%d at %H:%M:%S UTC")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S UTC")
