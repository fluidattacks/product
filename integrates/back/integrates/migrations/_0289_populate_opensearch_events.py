# type: ignore


"""
Populates OpenSearch with all the events from active groups

Execution Time:    2022-09-29 at 18:04:37 UTC
Finalization Time: 2022-09-29 at 18:05:38 UTC

Execution Time:    2023-04-10 at 15:18:38 UTC
Finalization Time: 2023-04-10 at 15:19:31 UTC

Execution Time:    2023-09-01 at 15:23:22 UTC
Finalization Time: 2023-09-01 at 15:24:56 UTC

Execution Time:    2023-11-09 at 03:35:45 UTC
Finalization Time: 2023-11-09 at 03:37:01 UTC

Execution Time:    2023-11-16 at 16:12:11 UTC
Finalization Time: 2023-11-16 at 16:13:25 UTC
"""

import asyncio
import logging
import logging.config
import time

from aioextensions import (
    collect,
    run,
)
from aiohttp.client_exceptions import (
    ClientConnectorError,
    ClientPayloadError,
    ServerDisconnectedError,
    ServerTimeoutError,
)
from boto3.dynamodb.conditions import (
    Key,
)
from botocore.exceptions import (
    ClientError,
    ConnectTimeoutError,
    HTTPClientError,
    ReadTimeoutError,
)
from more_itertools import (
    chunked,
)
from opensearchpy.helpers import (
    BulkIndexError,
    async_bulk,
)

from integrates.class_types.types import (
    Item,
)
from integrates.dataloaders import (
    get_new_context,
)
from integrates.db_model import (
    TABLE,
)
from integrates.decorators import (
    retry_on_exceptions,
)
from integrates.dynamodb import (
    keys,
    operations,
)
from integrates.organizations.domain import (
    get_all_active_group_names,
)
from integrates.search.client import (
    get_client,
    search_shutdown,
    search_startup,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)
LOGGER = logging.getLogger(__name__)
NETWORK_ERRORS = (
    asyncio.TimeoutError,
    ClientConnectorError,
    ClientError,
    ClientPayloadError,
    ConnectionError,
    ConnectionResetError,
    ConnectTimeoutError,
    HTTPClientError,
    ReadTimeoutError,
    ServerDisconnectedError,
    ServerTimeoutError,
)


async def process_events(group_name: str, events: tuple[Item, ...]) -> None:
    actions = [
        {
            "_id": "#".join([event["pk"], event["sk"]]),
            "_index": "events_index",
            "_op_type": "index",
            "_source": event,
        }
        for event in events
    ]

    client = await get_client()
    try:
        await async_bulk(client=client, actions=actions)
    except BulkIndexError as ex:
        for error in ex.errors:
            LOGGER.info("%s %s", group_name, error["index"]["error"]["reason"])


@retry_on_exceptions(exceptions=NETWORK_ERRORS, max_attempts=3, sleep_seconds=3.0)
async def process_group(group_name: str) -> None:
    primary_key = keys.build_key(
        facet=TABLE.facets["event_metadata"],
        values={"name": group_name},
    )
    index = TABLE.indexes["inverted_index"]
    key_structure = index.primary_key
    condition_expression = Key(key_structure.partition_key).eq(primary_key.sort_key) & Key(
        key_structure.sort_key,
    ).begins_with(primary_key.partition_key)
    response = await operations.query(
        condition_expression=condition_expression,
        facets=(TABLE.facets["event_metadata"],),
        table=TABLE,
        index=index,
    )
    executions = response.items

    await collect(
        tuple(
            process_events(group_name, executions_chunk)
            for executions_chunk in chunked(executions, 100)
        ),
    )
    LOGGER.info(
        "Group processed",
        extra={
            "extra": {
                "group_name": group_name,
                "events": len(executions),
            },
        },
    )


async def main() -> None:
    loaders = get_new_context()
    active_group_names = sorted(await get_all_active_group_names(loaders))
    await search_startup()
    client = await get_client()
    await client.indices.delete(index="events_index")
    await client.indices.create(index="events_index")
    await collect(
        tuple(process_group(group_name) for group_name in active_group_names),
        workers=4,
    )
    await search_shutdown()


if __name__ == "__main__":
    execution_time = time.strftime("Execution Time:    %Y-%m-%d at %H:%M:%S UTC")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S UTC")
    LOGGER.info("\n%s\n%s", execution_time, finalization_time)
