"""
Populate finding.unreliable_indicators.zero_risk_summary
for all findings.

Start Time:         2024-10-31 at 14:32:35 UTC
Finalization Time:  2024-10-31 at 14:34:17 UTC
Start Time:         2024-11-01 at 17:53:19 UTC
Finalization Time:  2024-11-01 at 17:57:14 UTC
Start Time:         2024-11-05 at 20:21:27 UTC
Finalization Time:  2024-11-05 at 20:24:39 UTC
"""

import asyncio
import logging
import logging.config
import time

from aioextensions import (
    collect,
    run,
)
from aiohttp.client_exceptions import (
    ClientConnectorError,
    ClientOSError,
    ClientPayloadError,
    ServerDisconnectedError,
    ServerTimeoutError,
)
from botocore.exceptions import (
    ClientError,
    ConnectTimeoutError,
    HTTPClientError,
    ReadTimeoutError,
)

from integrates.custom_exceptions import (
    IndicatorAlreadyUpdated,
)
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model import (
    findings as findings_model,
)
from integrates.db_model.findings.types import (
    Finding,
    FindingUnreliableIndicatorsToUpdate,
    FindingZeroRiskSummary,
)
from integrates.db_model.vulnerabilities.types import (
    FindingVulnerabilitiesZrRequest,
)
from integrates.decorators import (
    retry_on_exceptions,
)
from integrates.dynamodb.exceptions import (
    ConditionalCheckFailedException,
    UnavailabilityError,
)
from integrates.organizations import (
    domain as orgs_domain,
)
from integrates.settings import (
    LOGGING,
)
from integrates.vulnerabilities import (
    domain as vulns_domain,
)

logging.config.dictConfig(LOGGING)
LOGGER = logging.getLogger("console")
NETWORK_ERRORS = (
    asyncio.TimeoutError,
    ClientConnectorError,
    ClientError,
    ClientOSError,
    ClientPayloadError,
    ConnectionError,
    ConnectionResetError,
    ConnectTimeoutError,
    HTTPClientError,
    ReadTimeoutError,
    ServerDisconnectedError,
    ServerTimeoutError,
    UnavailabilityError,
)


@retry_on_exceptions(
    exceptions=NETWORK_ERRORS,
    sleep_seconds=5,
)
async def process_finding(finding: Finding) -> None:
    loaders: Dataloaders = get_new_context()
    connection = await loaders.finding_vulnerabilities_released_zr_c.load(
        FindingVulnerabilitiesZrRequest(
            finding_id=finding.id,
            paginate=False,
        ),
    )
    vulns = [edge.node for edge in connection.edges]
    zr_vulns_counter = vulns_domain.get_zr_count(vulns)
    zr_summary = FindingZeroRiskSummary(
        confirmed=zr_vulns_counter.confirmed,
        rejected=zr_vulns_counter.rejected,
        requested=zr_vulns_counter.requested,
    )
    zero_risk_summary = finding.unreliable_indicators.unreliable_zero_risk_summary

    try:
        await findings_model.update_unreliable_indicators(
            current_value=None,
            group_name=finding.group_name,
            finding_id=finding.id,
            indicators=FindingUnreliableIndicatorsToUpdate(
                unreliable_zero_risk_summary=zr_summary,
            ),
        )

        LOGGER.info(
            "Finding updated",
            extra={
                "extra": {
                    "finding_id": finding.id,
                    "old_zero_risk_summary": zero_risk_summary,
                    "new_zero_risk_summary": zr_summary,
                },
            },
        )
    except (ConditionalCheckFailedException, IndicatorAlreadyUpdated) as ex:
        LOGGER.error("Failed to update finding: %s - %s", finding.id, str(ex))


@retry_on_exceptions(
    exceptions=NETWORK_ERRORS,
    sleep_seconds=5,
)
async def process_group(group_name: str, progress: float) -> None:
    loaders: Dataloaders = get_new_context()
    all_findings = await loaders.group_findings_all.load(group_name)
    await collect(
        [process_finding(finding) for finding in all_findings],
        workers=8,
    )
    LOGGER.info(
        "Group processed",
        extra={
            "extra": {
                "group_name": group_name,
                "findings_processed": len(all_findings),
                "progress": round(progress, 2),
            },
        },
    )


@retry_on_exceptions(
    exceptions=NETWORK_ERRORS,
    sleep_seconds=5,
)
async def main() -> None:
    loaders: Dataloaders = get_new_context()
    group_names = sorted(await orgs_domain.get_all_group_names(loaders=loaders))
    LOGGER.info(
        "All groups",
        extra={
            "extra": {
                "groups_len": len(group_names),
                "group_names": group_names,
            },
        },
    )
    await collect(
        [
            process_group(
                group_name=group_name,
                progress=count / len(group_names),
            )
            for count, group_name in enumerate(group_names)
        ],
        workers=1,
    )


if __name__ == "__main__":
    execution_time = time.strftime("Start Time:    %Y-%m-%d at %H:%M:%S UTC")
    LOGGER.info("%s", execution_time)
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S UTC")
    LOGGER.info("\n%s\n%s\n", execution_time, finalization_time)
