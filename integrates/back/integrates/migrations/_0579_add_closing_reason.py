"""
Add closing reason to vulnerabilities

Start Time:           2024-08-20 at 17:02:04 UTC
Finalization Time:    2024-08-20 at 17:21:46 UTC
"""

import logging
import logging.config
import time

from aioextensions import (
    collect,
    run,
)

from integrates.custom_exceptions import (
    VulnNotFound,
)
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model import (
    vulnerabilities as vulns_model,
)
from integrates.db_model.findings.enums import (
    FindingStateStatus,
)
from integrates.db_model.findings.types import (
    Finding,
)
from integrates.db_model.vulnerabilities.enums import (
    VulnerabilityStateReason,
    VulnerabilityStateStatus,
)
from integrates.db_model.vulnerabilities.types import (
    Vulnerability,
    VulnerabilityState,
)
from integrates.organizations.domain import (
    get_all_group_names,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)

# Constants
LOGGER = logging.getLogger(__name__)
LOGGER_CONSOLE = logging.getLogger("console")


def _determine_reason(
    vulnerability_state: VulnerabilityState,
) -> list[VulnerabilityStateReason]:
    current_reasons = vulnerability_state.reasons or []

    if not current_reasons and vulnerability_state.other_reason:
        return [VulnerabilityStateReason.OTHER]

    modified_by = vulnerability_state.modified_by

    if len(current_reasons) and (
        (len(current_reasons) > 1)
        or (current_reasons[0] != VulnerabilityStateReason.NO_JUSTIFICATION)
    ):
        return current_reasons

    if modified_by == "machine@fluidattacks.com":
        return [VulnerabilityStateReason.CLOSED_BY_MACHINE]

    if modified_by.endswith("@fluidattacks.com"):
        return [VulnerabilityStateReason.VERIFIED_AS_SAFE]

    return [VulnerabilityStateReason.NO_JUSTIFICATION]


async def _add_closing_reason(vulnerability: Vulnerability) -> None:
    if vulnerability.state.status is VulnerabilityStateStatus.DELETED:
        return

    current_value = vulnerability.state.reasons
    new_reason = _determine_reason(vulnerability.state)

    new_state = vulnerability.state._replace(reasons=new_reason)

    try:
        await vulns_model.update_historic_entry(
            current_value=vulnerability,
            finding_id=vulnerability.finding_id,
            vulnerability_id=vulnerability.id,
            entry=new_state,
        )
        LOGGER_CONSOLE.info(
            "Closing reason added",
            extra={
                "extra": {
                    "vuln_id": vulnerability.id,
                    "finding_id": vulnerability.finding_id,
                    "reason_before": current_value,
                    "reason_after": new_reason,
                },
            },
        )
    except VulnNotFound as exc:
        LOGGER.error(exc, extra={"extra": {"vuln_id": vulnerability.id}})


async def _process_finding(loaders: Dataloaders, finding: Finding) -> None:
    if finding.state.status is FindingStateStatus.DELETED:
        return

    all_vulns = await loaders.finding_vulnerabilities_all.load(finding.id)
    closed_vulns_without_reason = [
        vuln for vuln in all_vulns if vuln.state.status is VulnerabilityStateStatus.SAFE
    ]
    await collect(
        [_add_closing_reason(vuln) for vuln in closed_vulns_without_reason],
        workers=16,
    )


async def _process_group(group_name: str) -> None:
    loaders = get_new_context()
    findings = await loaders.group_findings.load(group_name)

    if findings:
        LOGGER.info("Processing group %s", group_name)

        await collect(
            [_process_finding(loaders, finding) for finding in findings],
            workers=16,
        )

        LOGGER_CONSOLE.info("done %s", group_name)


async def main() -> None:
    loaders = get_new_context()
    all_group_names = sorted(await get_all_group_names(loaders=loaders))

    group_names = [
        group_name
        for group_name in all_group_names
        if group_name[0] not in ["a", "b", "c", "d", "e", "f"]
    ]

    await collect(
        [_process_group(group_name=group) for group in group_names],
        workers=15,
    )


if __name__ == "__main__":
    execution_time = time.strftime("Execution Time:    %Y-%m-%d at %H:%M:%S UTC%Z")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S UTC%Z")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
