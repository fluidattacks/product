"""
Remove duplicated organizations

First Start Time:         2024-03-10 at 03:43:09 UTC
First Finalization Time:  2024-03-10 at 03:58:09 UTC
Second Start Time:        2024-03-10 at 18:07:10 UTC
Second Finalization Time: 2024-03-10 at 18:12:40 UTC
"""

import logging
import logging.config
import time

from aioextensions import (
    collect,
    run,
)

from integrates.custom_utils.datetime import (
    get_utc_now,
)
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model import (
    credentials as credentials_model,
)
from integrates.db_model import (
    organizations as orgs_model,
)
from integrates.db_model.organizations.enums import (
    OrganizationStateStatus,
)
from integrates.db_model.organizations.types import (
    OrganizationState,
)
from integrates.organizations import (
    domain as orgs_domain,
)
from integrates.schedulers.delete_obsolete_orgs import (
    _remove_group,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)
LOGGER_CONSOLE = logging.getLogger("console")
INTEGRATES_EMAIL = "integrates@fluidattacks.com"


async def get_organizations() -> set[str]:
    organizations = set()
    same_name_organization_id = set()
    async for organization in orgs_domain.iterate_organizations():
        if organization.name in organizations:
            same_name_organization_id.add(organization.id)
        organizations.add(organization.name)

    return same_name_organization_id


async def remove_organization(
    loaders: Dataloaders,
    organization_id: str,
    organization_name: str,
    modified_by: str,
    aws_external_id: str,
) -> None:
    if len(await orgs_domain.get_stakeholders_emails(loaders, organization_id)) > 1:
        return
    if await loaders.organization_credentials.load(organization_id):
        return

    group_names = await orgs_domain.get_group_names(loaders, organization_id)
    await collect(_remove_group(loaders, group) for group in group_names)
    await collect(
        orgs_domain.remove_access(organization_id, email, modified_by)
        for email in await orgs_domain.get_stakeholders_emails(loaders, organization_id)
    )

    await orgs_model.update_state(
        organization_id=organization_id,
        organization_name=organization_name,
        state=OrganizationState(
            aws_external_id=aws_external_id,
            modified_by=modified_by,
            modified_date=get_utc_now(),
            status=OrganizationStateStatus.DELETED,
            pending_deletion_date=None,
        ),
    )
    await credentials_model.remove_organization_credentials(organization_id=organization_id)
    await orgs_model.remove(organization_id=organization_id, organization_name=organization_name)
    LOGGER_CONSOLE.info(
        "Organization",
        extra={
            "extra": {
                "organization_id": organization_id,
                "group_names": group_names,
            },
        },
    )


async def main() -> None:
    ids = await get_organizations()
    loaders = get_new_context()
    organizations = await loaders.organization.load_many(ids)
    await collect(
        [
            remove_organization(
                loaders=loaders,
                organization_id=organization.id,
                organization_name=organization.name,
                modified_by=INTEGRATES_EMAIL,
                aws_external_id=organization.state.aws_external_id,
            )
            for organization in organizations
            if organization
        ],
        workers=1,
    )


if __name__ == "__main__":
    execution_time = time.strftime("Start Time:        %Y-%m-%d at %H:%M:%S UTC")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S UTC")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
