"""
Distribute okada mailmap using a commits report

This migration downloads a commits report from S3, which indicates the
organizations where a given author has made commits.

Using this data, the migration constructs a nested dictionary that maps
organizations to authors that map to aliases. The focus is on aliases, and
that's why only the lines for which "alias" is "True" are selected.

For each organization and author, we query the database to retrieve the
matching aliases. A new author is created with these matching aliases and
copied to the corresponding organization. We track the authors for whom all
aliases have been moved, allowing us to remove them from okada at the end of
the script. Additionally, we keep track of the copied aliases so they can be
removed from their corresponding authors in okada.

Start Time:        2024-09-03 at 21:05:58 UTC
Finalization Time: 2024-09-03 at 21:06:09 UTC

"""

import asyncio
import csv
import logging
import logging.config
import os
import time
from collections import (
    defaultdict,
)

from aioextensions import (
    run,
)

from integrates.custom_exceptions import (
    ErrorDownloadingFile,
    MailmapEntryAlreadyExists,
    MailmapEntryNotFound,
    MailmapOrganizationNotFound,
    MailmapSubentryAlreadyExists,
    MailmapSubentryNotFound,
)
from integrates.db_model.mailmap.types import (
    MailmapEntryWithSubentries,
    MailmapSubentry,
)
from integrates.mailmap.create import (
    create_mailmap_entry_with_subentries,
)
from integrates.mailmap.delete import (
    delete_mailmap_entry_with_subentries,
    delete_mailmap_subentry,
)
from integrates.mailmap.get import (
    get_mailmap_entry_with_subentries,
)
from integrates.s3.operations import (
    download_file,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)
LOGGER_CONSOLE = logging.getLogger("console")


ORGANIZATION_ID = "okada"
COMMITS_REPORT_FILE_PATH = "okada_mailmap_commits_by_author.csv"


async def download_commits_report_file(file_path: str) -> None:
    try:
        await download_file(
            file_name=f"mailmap-resources/{file_path}",
            file_path=file_path,
            bucket="integrates.dev",
        )
    except ErrorDownloadingFile as ex:
        LOGGER_CONSOLE.error("%s\n\n", ex)


def remove_commits_report_file(file_path: str) -> None:
    if os.path.exists(file_path):
        os.remove(file_path)
        LOGGER_CONSOLE.info("Commits report file removed successfully.\n\n")
    else:
        LOGGER_CONSOLE.error("Commits report file doesn't exist.\n\n")


def build_nested_dict_by_organization(
    file_path: str,
) -> dict[str, dict[str, list[str]]]:
    organization_dict: defaultdict = defaultdict(lambda: defaultdict(list))

    with open(file_path, newline="", encoding="utf-8") as csv_file:
        reader = csv.DictReader(csv_file)

        for row in reader:
            is_alias = row["alias"].strip().lower() == "true"
            if is_alias:
                author_email = row["author_entry"].split(" - ")[0]
                alias = row["commit_author"]
                organizations = row["company_present"].strip().split(", ")

                for organization in organizations:
                    organization_dict[organization][author_email].append(alias)

    result = {org: dict(author_dict) for org, author_dict in organization_dict.items()}
    return result


async def copy_author(author: MailmapEntryWithSubentries, org_name: str) -> None:
    try:
        LOGGER_CONSOLE.info(
            "Author %s can be moved to %s\n\n",
            author,
            org_name,
        )
        await create_mailmap_entry_with_subentries(
            entry_with_subentries=author,
            organization_id=org_name,
        )
        LOGGER_CONSOLE.info(
            "Author %s moved to %s successfully\n\n",
            author,
            org_name,
        )
    except (
        MailmapOrganizationNotFound,
        MailmapEntryAlreadyExists,
        MailmapEntryNotFound,
        MailmapSubentryAlreadyExists,
        MailmapSubentryNotFound,
    ) as ex:
        LOGGER_CONSOLE.error("Cannot copy author: %s\n\n", ex)


async def remove_author(entry_email: str) -> int:
    try:
        await delete_mailmap_entry_with_subentries(
            entry_email=entry_email,
            organization_id=ORGANIZATION_ID,
        )
        LOGGER_CONSOLE.info(
            "Author %s removed successfully\n\n",
            entry_email,
        )
        return 1
    except (
        MailmapOrganizationNotFound,
        MailmapEntryAlreadyExists,
        MailmapEntryNotFound,
        MailmapSubentryAlreadyExists,
        MailmapSubentryNotFound,
    ) as ex:
        LOGGER_CONSOLE.error("Cannot remove author: %s\n\n", ex)
        return 0


async def remove_aliases(entry_email: str, subentries: list[MailmapSubentry]) -> None:
    for subentry in subentries:
        try:
            await delete_mailmap_subentry(
                entry_email=entry_email,
                subentry_email=subentry["mailmap_subentry_email"],
                subentry_name=subentry["mailmap_subentry_name"],
                organization_id=ORGANIZATION_ID,
            )
            LOGGER_CONSOLE.info(
                "Alias %s - %s removed successfully\n\n",
                entry_email,
                subentry,
            )
        except (
            MailmapOrganizationNotFound,
            MailmapEntryAlreadyExists,
            MailmapEntryNotFound,
            MailmapSubentryAlreadyExists,
            MailmapSubentryNotFound,
        ) as ex:
            LOGGER_CONSOLE.error("Cannot remove alias: %s\n\n", ex)


async def main() -> None:
    await download_commits_report_file(COMMITS_REPORT_FILE_PATH)
    # Build dict mapping orgs to authors to aliases
    result = build_nested_dict_by_organization(COMMITS_REPORT_FILE_PATH)
    remove_commits_report_file(COMMITS_REPORT_FILE_PATH)

    moved_entries = 0
    removed_entries = 0

    authors_to_remove = set()
    aliases_to_remove = []

    for org_name, authors in result.items():
        for author_email, aliases in authors.items():
            try:
                # Get author from database if it exists
                entry_with_subentries = await get_mailmap_entry_with_subentries(
                    entry_email=author_email,
                    organization_id=ORGANIZATION_ID,
                )
                # Find matching aliases in obtained author
                matched_subentries = [
                    subentry
                    for subentry in entry_with_subentries["subentries"]
                    for alias in aliases
                    if (
                        subentry["mailmap_subentry_email"] == alias.split(" - ")[0]
                        and subentry["mailmap_subentry_name"] == alias.split(" - ")[1]
                    )
                ]
                # If no matches, continue
                if len(matched_subentries) == 0:
                    continue
                # Build a new author with the matching aliases and copy it
                await copy_author(
                    author=MailmapEntryWithSubentries(
                        entry=entry_with_subentries["entry"],
                        subentries=matched_subentries,
                    ),
                    org_name=org_name,
                )
                moved_entries += 1
                # If all aliases were copied, the author can be removed
                if len(matched_subentries) == len(entry_with_subentries["subentries"]):
                    authors_to_remove.add(author_email)
                # Keep track of aliases that can be removed
                else:
                    aliases_to_remove.append(
                        (
                            author_email,
                            matched_subentries,
                        ),
                    )

            except MailmapEntryNotFound as ex:
                LOGGER_CONSOLE.error("Author not found: %s\n\n", ex)

    # Remove authors and count the successes
    for author_email in list(authors_to_remove):
        removed_entries += await remove_author(entry_email=author_email)

    # Remove the aliases: If the author was removed, this has no effect
    await asyncio.gather(
        *(
            remove_aliases(entry_email=record[0], subentries=record[1])
            for record in aliases_to_remove
        ),
    )

    LOGGER_CONSOLE.info(
        "Distributed entries: %s\n\n",
        moved_entries,
    )

    LOGGER_CONSOLE.info(
        "Deleted entries: %s\n\n",
        removed_entries,
    )


if __name__ == "__main__":
    execution_time = time.strftime("Start Time:        %Y-%m-%d at %H:%M:%S UTC")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S UTC")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
