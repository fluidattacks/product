"""
Update the 'technique' field for some vulnerabilities.

LINES vulnerabilities with CSPM technique -> SAST technique

Execution Time:    2024-07-19 at 19:35:05 UTC
Finalization Time: 2024-07-19 at 20:25:04 UTC
"""

import csv
import logging
import logging.config
import time

from aioextensions import (
    collect,
    run,
)

from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model import (
    vulnerabilities as vulns_model,
)
from integrates.db_model.enums import (
    Source,
)
from integrates.db_model.vulnerabilities.enums import (
    VulnerabilityTechnique,
    VulnerabilityType,
)
from integrates.db_model.vulnerabilities.types import (
    Vulnerability,
    VulnerabilityMetadataToUpdate,
)
from integrates.organizations.domain import (
    get_all_group_names,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)
LOGGER_CONSOLE = logging.getLogger("console")


async def process_group(
    loaders: Dataloaders,
    group_name: str,
) -> None:
    group_findings = await loaders.group_findings.load(group_name)

    vulns: list[Vulnerability] = await loaders.finding_vulnerabilities.load_many_chained(
        [fin.id for fin in group_findings],
    )

    cspm_lines_machine_vulns = [
        vuln
        for vuln in vulns
        if (vuln.hacker_email == "machine@fluidattacks.com" or vuln.state.source == Source.MACHINE)
        and vuln.type == VulnerabilityType.LINES
        and vuln.technique == VulnerabilityTechnique.CSPM
    ]

    if cspm_lines_machine_vulns:
        await collect(
            tuple(
                vulns_model.update_metadata(
                    finding_id=vuln.finding_id,
                    vulnerability_id=vuln.id,
                    metadata=VulnerabilityMetadataToUpdate(technique=VulnerabilityTechnique.SAST),
                )
                for vuln in cspm_lines_machine_vulns
            ),
            workers=32,
        )

        affected_vulns = [
            [
                vuln.group_name,
                vuln.id,
                vuln.state.where,
                vuln.state.status.value,
                vuln.technique,
            ]
            for vuln in cspm_lines_machine_vulns
        ]

        with open("cspm_vulns.csv", "a+", encoding="utf-8") as handler:
            writer = csv.writer(handler)
            writer.writerows(affected_vulns)


async def main() -> None:
    loaders: Dataloaders = get_new_context()
    groups = sorted(await get_all_group_names(loaders))
    for group in groups:
        if group == "grupoi":
            continue
        LOGGER_CONSOLE.info("Processing group %s", group)
        await process_group(loaders, group)


if __name__ == "__main__":
    execution_time = time.strftime("Execution Time:    %Y-%m-%d at %H:%M:%S UTC")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S UTC")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
