"""
Fix inconsistent comments left by cloning scheduler.

Execution Time:    2024-12-05 at 23:43:30 UTC
Finalization Time: 2024-12-05 at 23:43:37 UTC
"""

import csv
import time

from aioextensions import (
    run,
)
from boto3.dynamodb.conditions import (
    Attr,
)

from integrates.dataloaders import (
    get_new_context,
)
from integrates.db_model import (
    TABLE,
)
from integrates.db_model.finding_comments.enums import (
    CommentType,
)
from integrates.db_model.finding_comments.types import (
    FindingComment,
    FindingCommentsRequest,
)
from integrates.db_model.utils import (
    get_as_utc_iso_format,
)
from integrates.dynamodb import (
    keys,
    operations,
)
from integrates.dynamodb.exceptions import (
    ConditionalCheckFailedException,
)


async def update_comment(
    *,
    finding_comment: FindingComment,
) -> None:
    try:
        key_structure = TABLE.primary_key
        primary_key = keys.build_key(
            facet=TABLE.facets["finding_comment"],
            values={
                "id": finding_comment.id,
                "finding_id": finding_comment.finding_id,
            },
        )
        item = {
            "finding_id": finding_comment.finding_id,
            "id": finding_comment.id,
            "parent_id": finding_comment.parent_id,
            "comment_type": finding_comment.comment_type.value,
            "creation_date": get_as_utc_iso_format(finding_comment.creation_date),
            "content": finding_comment.content,
            "email": finding_comment.email,
            "full_name": finding_comment.full_name,
        }
        condition_expression = Attr(key_structure.partition_key).exists()
        await operations.update_item(
            condition_expression=condition_expression,
            item=item,
            key=primary_key,
            table=TABLE,
        )
    except ConditionalCheckFailedException:
        print(f"Unable to update finding comment {finding_comment.id}")


async def main() -> None:
    loaders = get_new_context()
    csv_rows = []
    with open("inconsistent_comments.csv", encoding="utf-8") as file:
        csv_file = csv.reader(file)
        for line in csv_file:
            csv_rows.append(line[0])

    for finding_id in csv_rows:
        fin_comments: list[FindingComment] = await loaders.finding_comments.load(
            FindingCommentsRequest(comment_type=CommentType.VERIFICATION, finding_id=finding_id),
        )

        scheduler_comments = [comment for comment in fin_comments if comment.full_name == "- -"]
        for comment in scheduler_comments:
            new_comment = comment._replace(full_name="Scanner Services")
            await update_comment(finding_comment=new_comment)


if __name__ == "__main__":
    execution_time = time.strftime("Execution Time:    %Y-%m-%d at %H:%M:%S UTC")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S UTC")
    print("\n%s\n%s", execution_time, finalization_time)
