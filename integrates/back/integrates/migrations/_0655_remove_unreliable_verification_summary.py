"""
Rm finding indicator unreliable_verification_summary
It is now handled via streams

Execution Time: 2025-01-28 at 15:58:03 UTC, extra=None
Finalization Time: 2025-01-28 at 16:12:01 UTC, extra=None
"""

import logging
import logging.config

from aioextensions import collect
from boto3.dynamodb.conditions import (
    Attr,
    Key,
)

from integrates.class_types.types import (
    Item,
)
from integrates.dataloaders import get_new_context
from integrates.db_model import (
    TABLE,
)
from integrates.dynamodb import (
    keys,
    operations,
)
from integrates.migrations.utils import log_time
from integrates.organizations import domain as orgs_domain

LOGGER = logging.getLogger("console")


async def get_findings_by_group(group_name: str) -> tuple[Item, ...]:
    primary_key = keys.build_key(
        facet=TABLE.facets["finding_metadata"],
        values={"group_name": group_name},
    )
    index = TABLE.indexes["inverted_index"]
    key_structure = index.primary_key
    response = await operations.query(
        condition_expression=(
            Key(key_structure.partition_key).eq(primary_key.sort_key)
            & Key(key_structure.sort_key).begins_with(primary_key.partition_key)
        ),
        facets=(TABLE.facets["finding_metadata"],),
        index=index,
        table=TABLE,
    )

    return response.items


async def remove_unreliable_verification_summary(
    finding_id: str,
    group_name: str,
) -> None:
    metadata_key = keys.build_key(
        facet=TABLE.facets["finding_metadata"],
        values={"group_name": group_name, "id": finding_id},
    )
    attribute = "unreliable_indicators.unreliable_verification_summary"
    await operations.update_item(
        condition_expression=Attr(attribute).exists(),
        item={attribute: None},
        key=metadata_key,
        table=TABLE,
    )


async def process_group(group_name: str) -> None:
    all_findings = await get_findings_by_group(group_name)
    if not all_findings:
        return

    findings_to_update = tuple(
        item
        for item in all_findings
        if item["unreliable_indicators"].get("unreliable_verification_summary")
    )
    if not findings_to_update:
        return

    await collect(
        [
            remove_unreliable_verification_summary(item["id"], group_name)
            for item in findings_to_update
        ],
        workers=32,
    )
    LOGGER.info(
        "Group processed: %s, %s",
        group_name,
        len(findings_to_update),
    )


@log_time(LOGGER)
async def main() -> None:
    loaders = get_new_context()
    group_names = sorted(await orgs_domain.get_all_group_names(loaders))
    for group_name in group_names:
        await process_group(group_name)


if __name__ == "__main__":
    main()  # type: ignore[unused-coroutine]
