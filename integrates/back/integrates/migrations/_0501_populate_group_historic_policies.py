"""
Populate group historic policies in integrates_vms_historic table.

Start Time: 2024-02-27 at 20:59:23 UTC
Finalization Time: 2024-02-27 at 20:59:35 UTC
"""

import asyncio
import logging
import logging.config
import time

from aioextensions import (
    run,
)
from aiohttp.client_exceptions import (
    ClientConnectorError,
    ClientOSError,
    ClientPayloadError,
    ServerDisconnectedError,
    ServerTimeoutError,
)
from boto3.dynamodb.conditions import (
    Key,
)
from botocore.exceptions import (
    ClientError,
    ConnectTimeoutError,
    HTTPClientError,
    ReadTimeoutError,
)
from more_itertools import (
    chunked,
)

from integrates.class_types.types import (
    Item,
)
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model import (
    HISTORIC_TABLE,
    TABLE,
)
from integrates.db_model.utils import (
    get_historic_gsi_2_key,
    get_historic_gsi_3_key,
    get_historic_gsi_sk,
)
from integrates.decorators import (
    retry_on_exceptions,
)
from integrates.dynamodb import (
    keys,
    operations,
)
from integrates.dynamodb.exceptions import (
    UnavailabilityError,
)
from integrates.organizations import (
    domain as orgs_domain,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)
LOGGER_CONSOLE = logging.getLogger("console")
NETWORK_ERRORS = (
    asyncio.TimeoutError,
    ClientConnectorError,
    ClientError,
    ClientOSError,
    ClientPayloadError,
    ConnectionError,
    ConnectionResetError,
    ConnectTimeoutError,
    HTTPClientError,
    ReadTimeoutError,
    ServerDisconnectedError,
    ServerTimeoutError,
    UnavailabilityError,
)


batch_put_item = retry_on_exceptions(
    exceptions=NETWORK_ERRORS,
    sleep_seconds=300,
)(operations.batch_put_item)


@retry_on_exceptions(
    exceptions=NETWORK_ERRORS,
    sleep_seconds=10,
)
async def _get_historic_policies(
    group_name: str,
) -> tuple[Item, ...]:
    primary_key = keys.build_key(
        facet=TABLE.facets["group_historic_policies"],
        values={"name": group_name},
    )
    key_structure = TABLE.primary_key
    response = await operations.query(
        condition_expression=(
            Key(key_structure.partition_key).eq(primary_key.partition_key)
            & Key(key_structure.sort_key).begins_with(primary_key.sort_key)
        ),
        facets=(TABLE.facets["group_historic_policies"],),
        table=TABLE,
    )
    return response.items


@retry_on_exceptions(
    exceptions=NETWORK_ERRORS,
    sleep_seconds=10,
)
async def _get_group(*, group_name: str) -> Item:
    primary_key = keys.build_key(
        facet=TABLE.facets["group_metadata"],
        values={"name": group_name},
    )

    key_structure = TABLE.primary_key
    response = await operations.query(
        condition_expression=(
            Key(key_structure.partition_key).eq(primary_key.partition_key)
            & Key(key_structure.sort_key).begins_with(primary_key.sort_key)
        ),
        facets=(TABLE.facets["group_metadata"],),
        limit=1,
        table=TABLE,
    )
    return response.items[0]


def format_policies_item(policies_item: Item, group_item: Item, group_name: str) -> Item:
    item_pk = f"{group_item['pk']}#{group_item['sk']}"
    item_sk = get_historic_gsi_sk("policies", policies_item["modified_date"])
    gsi_2_key = get_historic_gsi_2_key(
        HISTORIC_TABLE.facets["group_policies"],
        group_name,
        "policies",
        policies_item["modified_date"],
    )
    gsi_3_key = get_historic_gsi_3_key(group_name, "policies", policies_item["modified_date"])
    gsi_keys = {
        "pk_2": gsi_2_key.partition_key,
        "sk_2": gsi_2_key.sort_key,
        "pk_3": gsi_3_key.partition_key,
        "sk_3": gsi_3_key.sort_key,
    }
    return {
        **(
            group_item["policies"]
            if group_item.get("policies")
            and policies_item["modified_date"] == group_item["policies"]["modified_date"]
            else policies_item
        ),
        "pk": item_pk,
        "sk": item_sk,
        **gsi_keys,
    }


async def process_group(group_name: str) -> None:
    group_item = await _get_group(group_name=group_name)
    group_historic_policies: tuple[Item, ...] = await _get_historic_policies(group_name=group_name)
    # if not group_historic_policies and group_item.get("policies"):
    #     group_historic_policies = (group_item["policies"],)

    formatted_items = tuple(
        format_policies_item(policies_item, group_item, group_name)
        for policies_item in group_historic_policies
    )
    for chunked_formatted_items in chunked(formatted_items, 25):
        await batch_put_item(
            items=tuple(chunked_formatted_items),
            table=HISTORIC_TABLE,
        )
    LOGGER_CONSOLE.info(
        "Group processed",
        extra={
            "extra": {
                "group_name": group_name,
                "group_historic_policies": len(group_historic_policies),
            },
        },
    )


async def main() -> None:
    loaders: Dataloaders = get_new_context()
    all_group_names = sorted(await orgs_domain.get_all_group_names(loaders))
    LOGGER_CONSOLE.info(
        "All group names",
        extra={
            "extra": {
                "total": len(all_group_names),
            },
        },
    )
    for count, group_name in enumerate(all_group_names, start=1):
        LOGGER_CONSOLE.info(
            "Group",
            extra={
                "extra": {
                    "group_name": group_name,
                    "count": count,
                },
            },
        )
        await process_group(group_name)


if __name__ == "__main__":
    execution_time = time.strftime("Start Time:        %Y-%m-%d at %H:%M:%S UTC")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S UTC")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
