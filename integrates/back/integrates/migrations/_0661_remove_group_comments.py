"""
Remove unused data from facet group_comment.

Execution Time:    2025-02-10 at 17:07:32 UTC
Finalization Time: 2025-02-10 at 17:07:39 UTC
"""

import logging
import logging.config
import time

from aioextensions import run
from boto3.dynamodb.conditions import Key

from integrates.dataloaders import Dataloaders, get_new_context
from integrates.db_model import TABLE
from integrates.dynamodb import keys, operations
from integrates.dynamodb.types import PrimaryKey
from integrates.organizations.domain import get_all_group_names
from integrates.settings import LOGGING

logging.config.dictConfig(LOGGING)
LOGGER_CONSOLE = logging.getLogger("console")


async def remove_group_comments(
    *,
    group_name: str,
) -> None:
    facet = TABLE.facets["group_comment"]
    primary_key = keys.build_key(
        facet=facet,
        values={"name": group_name},
    )
    index = TABLE.indexes["inverted_index"]
    key_structure = index.primary_key
    condition_expression = Key(key_structure.partition_key).eq(primary_key.sort_key) & Key(
        key_structure.sort_key,
    ).begins_with(primary_key.partition_key)
    response = await operations.query(
        condition_expression=condition_expression,
        facets=(facet,),
        table=TABLE,
        index=index,
    )
    if not response.items:
        LOGGER_CONSOLE.info("Found no items to delete for group %s", group_name)
        return
    keys_to_delete = set(
        PrimaryKey(
            partition_key=item[TABLE.primary_key.partition_key],
            sort_key=item[TABLE.primary_key.sort_key],
        )
        for item in response.items
    )
    LOGGER_CONSOLE.info("Removing %d group comments for group: %s", len(keys_to_delete), group_name)
    await operations.batch_delete_item(
        keys=tuple(keys_to_delete),
        table=TABLE,
    )


async def main() -> None:
    loaders: Dataloaders = get_new_context()
    groups = sorted(await get_all_group_names(loaders))
    for group_name in groups:
        await remove_group_comments(group_name=group_name)


if __name__ == "__main__":
    execution_time = time.strftime("Execution Time:    %Y-%m-%d at %H:%M:%S UTC")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S UTC")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
