# type: ignore

"""
Populates OpenSearch with all the findings from active groups

Execution Time:    2022-09-09 at 21:04:22 UTC
Finalization Time: 2022-09-09 at 21:06:57 UTC

Execution Time:    2023-09-01 at 14:35:43 UTC
Finalization Time: 2023-09-01 at 14:39:11 UTC

Execution Time:    2023-09-07 at 15:39:24 UTC
Finalization Time: 2023-09-07 at 15:41:28 UTC

Execution Time:    2023-10-27 at 21:32:37 UTC
Finalization Time: 2023-10-27 at 21:35:57 UTC

Execution Time:    2023-10-31 at 18:13:51 UTC
Finalization Time: 2023-10-31 at 18:15:56 UTC

Execution Time:    2023-11-08 at 15:06:25 UTC
Finalization Time: 2023-11-08 at 15:11:03 UTC

Execution Time:    2023-11-09 at 01:37:38 UTC
Finalization Time: 2023-11-09 at 01:40:09 UTC

Execution Time:    2023-11-16 at 19:03:34 UTC
Finalization Time: 2023-11-16 at 19:05:15 UTC
"""

import asyncio
import logging
import logging.config
import time

from aioextensions import (
    collect,
    run,
)
from aiohttp.client_exceptions import (
    ClientConnectorError,
    ClientPayloadError,
    ServerDisconnectedError,
    ServerTimeoutError,
)
from boto3.dynamodb.conditions import (
    Key,
)
from botocore.exceptions import (
    ClientError,
    ConnectTimeoutError,
    HTTPClientError,
    ReadTimeoutError,
)
from more_itertools import (
    chunked,
)
from opensearchpy.helpers import (
    BulkIndexError,
    async_bulk,
)

from integrates.dataloaders import (
    get_new_context,
)
from integrates.db_model import (
    TABLE,
)
from integrates.decorators import (
    retry_on_exceptions,
)
from integrates.dynamodb import (
    keys,
    operations,
)
from integrates.organizations.domain import (
    get_all_active_group_names,
)
from integrates.search.client import (
    get_client,
    search_shutdown,
    search_startup,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)
LOGGER = logging.getLogger(__name__)
NETWORK_ERRORS = (
    asyncio.TimeoutError,
    ClientConnectorError,
    ClientError,
    ClientPayloadError,
    ConnectionError,
    ConnectionResetError,
    ConnectTimeoutError,
    HTTPClientError,
    ReadTimeoutError,
    ServerDisconnectedError,
    ServerTimeoutError,
)


async def process_findings(group_name: str, findings: tuple[dict[str, str], ...]) -> None:
    actions = [
        {
            "_id": "#".join([finding["pk"], finding["sk"]]),
            "_index": "findings_index",
            "_op_type": "index",
            "_source": finding,
        }
        for finding in findings
    ]
    client = await get_client()
    try:
        await async_bulk(client=client, actions=actions)
    except BulkIndexError as ex:
        for error in ex.errors:
            LOGGER.info("%s %s", group_name, error["index"]["error"]["reason"])


@retry_on_exceptions(exceptions=NETWORK_ERRORS, max_attempts=3, sleep_seconds=3.0)
async def process_group(group_name: str) -> None:
    primary_key = keys.build_key(
        facet=TABLE.facets["finding_metadata"],
        values={"group_name": group_name},
    )
    index = TABLE.indexes["inverted_index"]
    key_structure = index.primary_key
    response = await operations.query(
        condition_expression=(
            Key(key_structure.partition_key).eq(primary_key.sort_key)
            & Key(key_structure.sort_key).begins_with(primary_key.partition_key)
        ),
        facets=(TABLE.facets["finding_metadata"],),
        index=index,
        table=TABLE,
    )
    findings = response.items

    await collect(
        tuple(
            process_findings(group_name, findings_chunk)
            for findings_chunk in chunked(findings, 100)
        ),
    )
    LOGGER.info(
        "Group processed",
        extra={
            "extra": {
                "group_name": group_name,
                "findings": len(findings),
            },
        },
    )


async def main() -> None:
    loaders = get_new_context()
    active_group_names = sorted(await get_all_active_group_names(loaders))
    await search_startup()
    client = await get_client()
    await client.indices.delete(index="findings_index")
    await client.indices.create(index="findings_index")
    await collect(
        tuple(process_group(group_name) for group_name in active_group_names),
        workers=5,
    )
    await search_shutdown()


if __name__ == "__main__":
    execution_time = time.strftime("Execution Time:    %Y-%m-%d at %H:%M:%S UTC")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S UTC")
    LOGGER.info("\n%s\n%s", execution_time, finalization_time)
