# type: ignore
"""
Move roots.

Execution Time:    2024-05-27 at 20:11:15 UTC
Finalization Time: 2024-05-27 at 20:11:24 UTC

Execution Time:    2024-05-27 at 20:18:52 UTC
Finalization Time: 2024-05-27 at 20:18:54 UTC
"""

import itertools
import logging
import logging.config
import time
from operator import (
    attrgetter,
)

from aioextensions import (
    collect,
    run,
)
from boto3.dynamodb.conditions import (
    Key,
)

from integrates.batch import (
    dal as batch_dal,
)
from integrates.batch.enums import (
    Action,
    IntegratesBatchQueue,
)
from integrates.batch_dispatch.move_root import (
    process_finding,
    process_toe_input,
    process_toe_lines,
    process_toe_port,
    process_unsolved_events,
)
from integrates.custom_utils import (
    filter_vulnerabilities as filter_vulns_utils,
)
from integrates.custom_utils import (
    roots as roots_utils,
)
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model import (
    TABLE,
)
from integrates.db_model.roots.types import (
    GitRoot,
    IPRoot,
    Root,
    RootRequest,
    URLRoot,
)
from integrates.db_model.roots.utils import (
    format_root,
    valid_item,
)
from integrates.db_model.toe_inputs.types import (
    GroupToeInputsRequest,
)
from integrates.db_model.toe_lines.types import (
    RootToeLinesRequest,
)
from integrates.db_model.toe_ports.types import (
    RootToePortsRequest,
)
from integrates.db_model.vulnerabilities.constants import (
    ROOT_INDEX_METADATA,
)
from integrates.db_model.vulnerabilities.types import (
    Vulnerability,
)
from integrates.db_model.vulnerabilities.utils import (
    format_vulnerability,
)
from integrates.dynamodb import (
    keys,
    operations,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)

LOGGER = logging.getLogger(__name__)
LOGGER_CONSOLE = logging.getLogger("console")
BACKUP_TABLE = TABLE._replace(name="name")
INFO_TO_MOVE_THE_ROOT = {
    "source_group_name": "group1",
    "source_root_id": "id1",
    "target_root_id": "id2",
    "target_group_name": "group2",
}
EMAIL_INTEGRATES = "integrates@fluidattacks.com"


async def _get_root(*, request: RootRequest) -> list[Root]:
    primary_keys = keys.build_key(
        facet=BACKUP_TABLE.facets["git_root_metadata"],
        values={"name": request.group_name, "uuid": request.root_id},
    )

    items = await operations.batch_get_item(keys=(primary_keys,), table=BACKUP_TABLE)

    LOGGER.info("Root to process", extra={"extra": {"len": len(items)}})

    return [format_root(item) for item in items if valid_item(item)]


async def load_root(requests: RootRequest) -> list[Root]:
    root = await _get_root(request=requests)

    return root


async def _get_root_vulnerabilities(*, root_id: str) -> list[Vulnerability]:
    primary_key = keys.build_key(
        facet=ROOT_INDEX_METADATA,
        values={"root_id": root_id},
    )

    index = BACKUP_TABLE.indexes["gsi_2"]
    key_structure = index.primary_key
    response = await operations.query(
        condition_expression=(
            Key(key_structure.partition_key).eq(primary_key.partition_key)
            & Key(key_structure.sort_key).begins_with(primary_key.sort_key)
        ),
        facets=(ROOT_INDEX_METADATA,),
        table=BACKUP_TABLE,
        index=index,
    )

    return [format_vulnerability(item) for item in response.items]


async def move_root(
    *,
    target_group_name: str,
    target_root_id: str,
    source_group_name: str,
    source_root_id: str,
    root: Root,
) -> None:
    loaders: Dataloaders = get_new_context()

    LOGGER.info("Updating Events")
    event_ids = await process_unsolved_events(
        loaders=loaders,
        item_subject=EMAIL_INTEGRATES,
        source_group_name=source_group_name,
        source_root_id=source_root_id,
        target_group_name=target_group_name,
        target_root_id=target_root_id,
    )
    LOGGER.info("Moving root", extra={"extra": {"root_id": source_root_id}})
    root_vulnerabilities = filter_vulns_utils.filter_non_deleted(
        await _get_root_vulnerabilities(root_id=source_root_id),
    )
    vulns_by_finding = itertools.groupby(
        sorted(root_vulnerabilities, key=attrgetter("finding_id")),
        key=attrgetter("finding_id"),
    )
    LOGGER.info(
        "Root content",
        extra={
            "extra": {
                "vulnerabilities": len(root_vulnerabilities),
            },
        },
    )
    # From here it is not necessary differentiate between tables
    await collect(
        tuple(
            process_finding(
                loaders=loaders,
                source_group_name=source_group_name,
                target_group_name=target_group_name,
                target_root_id=target_root_id,
                source_finding_id=source_finding_id,
                vulns=tuple(vulns),
                item_subject=EMAIL_INTEGRATES,
                event_ids=event_ids,
            )
            for source_finding_id, vulns in vulns_by_finding
        ),
        workers=10,
    )
    LOGGER.info("Moving completed")
    target_root = await roots_utils.get_root(loaders, target_root_id, target_group_name)
    if isinstance(root, (GitRoot, URLRoot)):
        LOGGER.info("Updating ToE inputs")
        group_toe_inputs = await loaders.group_toe_inputs.load_nodes(
            GroupToeInputsRequest(group_name=source_group_name),
        )
        root_toe_inputs = tuple(
            toe_input for toe_input in group_toe_inputs if toe_input.root_id == source_root_id
        )
        await collect(
            tuple(
                process_toe_input(
                    loaders,
                    target_group_name,
                    target_root_id,
                    toe_input,
                )
                for toe_input in root_toe_inputs
            ),
        )
        await batch_dal.put_action(
            action=Action.REFRESH_TOE_INPUTS,
            entity=target_group_name,
            subject=EMAIL_INTEGRATES,
            additional_info={"root_ids": [target_root.id]},
            queue=IntegratesBatchQueue.SMALL,
        )
    if isinstance(root, GitRoot):
        LOGGER.info("Updating ToE lines")
        repo_toe_lines = await loaders.root_toe_lines.load_nodes(
            RootToeLinesRequest(group_name=source_group_name, root_id=source_root_id),
        )
        await collect(
            tuple(
                process_toe_lines(
                    loaders,
                    target_group_name,
                    target_root_id,
                    toe_lines,
                )
                for toe_lines in repo_toe_lines
            ),
        )
        await batch_dal.put_action(
            action=Action.REFRESH_TOE_LINES,
            attempt_duration_seconds=7200,
            entity=target_group_name,
            subject=EMAIL_INTEGRATES,
            additional_info={"root_ids": [target_root.id]},
            queue=IntegratesBatchQueue.SMALL,
        )

    if isinstance(root, IPRoot):
        LOGGER.info("Updating ToE ports")
        await collect(
            tuple(
                process_toe_port(
                    loaders,
                    target_group_name,
                    target_root_id,
                    toe_port,
                    EMAIL_INTEGRATES,
                )
                for toe_port in await loaders.root_toe_ports.load_nodes(
                    RootToePortsRequest(group_name=source_group_name, root_id=source_root_id),
                )
            ),
        )
        await batch_dal.put_action(
            action=Action.REFRESH_TOE_PORTS,
            entity=target_group_name,
            subject=EMAIL_INTEGRATES,
            additional_info={"root_ids": [target_root.id]},
            queue=IntegratesBatchQueue.SMALL,
        )
    LOGGER.info("Task completed successfully.")


async def get_source_root(
    *,
    source_group_name: str,
    source_root_id: str,
) -> Root:
    root = await load_root(RootRequest(group_name=source_group_name, root_id=source_root_id))

    if not root:
        LOGGER.info(
            "No root found",
            extra={
                "extra": {
                    "root_id": source_root_id,
                },
            },
        )
        return None

    LOGGER.info(
        "Root to process",
        extra={
            "extra": {
                "root_id": source_root_id,
                "group_name": source_group_name,
                "len": len(root),
            },
        },
    )

    return root[0]


async def main() -> None:
    target_group_name = INFO_TO_MOVE_THE_ROOT["target_group_name"]
    target_root_id = INFO_TO_MOVE_THE_ROOT["target_root_id"]
    source_group_name = INFO_TO_MOVE_THE_ROOT["source_group_name"]
    source_root_id = INFO_TO_MOVE_THE_ROOT["source_root_id"]
    source_root = await get_source_root(
        source_group_name=source_group_name,
        source_root_id=source_root_id,
    )

    await move_root(
        target_group_name=target_group_name,
        target_root_id=target_root_id,
        source_group_name=source_group_name,
        source_root_id=source_root_id,
        root=source_root,
    )

    LOGGER.info("Root moved successfully")


if __name__ == "__main__":
    execution_time = time.strftime("Execution Time:    %Y-%m-%d at %H:%M:%S UTC")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S UTC")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
