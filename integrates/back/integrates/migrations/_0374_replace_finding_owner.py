# type: ignore
"""
If a finding was created by an analyst, but has machine vulnerabilities,
pass ownership of the finding to machine

Execution Time:    2023-04-05 at 22:34:07 UTC
Finalization Time: 2023-04-05 at 22:42:41 UTCw
"""

import logging
import logging.config
import time
from datetime import (
    datetime,
)

import pytz
from aioextensions import (
    collect,
    run,
)

from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model import (
    findings as findings_model,
)
from integrates.db_model.findings.types import (
    Finding,
)
from integrates.db_model.vulnerabilities.enums import (
    VulnerabilityStateStatus,
)
from integrates.db_model.vulnerabilities.types import (
    Vulnerability,
    VulnerabilityState,
)
from integrates.db_model.vulnerabilities.update import (
    update_historic_entry,
)
from integrates.organizations import (
    domain as orgs_domain,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)
LOGGER_CONSOLE = logging.getLogger("console")


async def process_vuln(loaders: Dataloaders, vulnerability: Vulnerability) -> None:
    states: list[VulnerabilityState] = await loaders.vulnerability_historic_state.load(
        vulnerability.id,
    )
    if len(states) > 1:
        await update_historic_entry(
            current_value=vulnerability,
            finding_id=vulnerability.finding_id,
            entry=states[-2]._replace(modified_date=datetime.utcnow()),
            vulnerability_id=vulnerability.id,
        )


async def process_finding(loaders: Dataloaders, group_name: str, finding: Finding) -> None:
    if finding.creation.modified_by == "machine@fluidattacks.com":
        return
    vulns: list[Vulnerability] = await loaders.finding_vulnerabilities.load(finding.id)
    vulns = [vuln for vuln in vulns if vuln.created_by == "machine@fluidattacks.com"]
    if not vulns:
        return
    LOGGER_CONSOLE.info("%s -> %s: %s", group_name, finding.id, finding.title)
    await findings_model.update_historic_state(
        group_name=group_name,
        finding_id=finding.id,
        historic_state=[
            ((await loaders.finding_historic_state.load(finding.id))[0])._replace(
                modified_by="machine@fluidattacks.com",
            ),
        ],
    )

    vulns = [
        vuln
        for vuln in vulns
        if vuln.state.status in (VulnerabilityStateStatus.SAFE,)
        and vuln.state.modified_by == "machine@fluidattacks.com"
        and vuln.state.modified_date.replace(tzinfo=pytz.UTC)
        > datetime(2023, 3, 31).replace(tzinfo=pytz.UTC)
    ]
    await collect([process_vuln(loaders, vuln) for vuln in vulns], workers=10)


async def process_group(group: str) -> None:
    LOGGER_CONSOLE.info("Processing %s", group)
    loaders: Dataloaders = get_new_context()

    findings: dict[str, Finding] = {
        fin.id: fin for fin in (await loaders.group_drafts_and_findings.load(group))
    }

    await collect(
        [process_finding(loaders, group, finding) for finding in findings.values()],
        workers=3,
    )


async def main() -> None:
    loaders: Dataloaders = get_new_context()
    groups = sorted(await orgs_domain.get_all_active_group_names(loaders=loaders))
    await collect([process_group(group) for group in reversed(groups)], workers=10)


if __name__ == "__main__":
    execution_time = time.strftime("Execution Time:    %Y-%m-%d at %H:%M:%S %Z")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S %Z")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
