"""
Execution Time:    2024-10-22 at 22:40:13 UTC
Finalization Time: 2024-10-23 at 00:03:49 UTC

Execution Time:    2024-10-30 at 01:42:18 UTC
Finalization Time: 2024-10-30 at 02:05:50 UTC
"""

import logging
import logging.config
import time

from aioextensions import (
    collect,
    run,
)
from boto3.dynamodb.conditions import (
    Attr,
)
from botocore.exceptions import (
    ConnectTimeoutError,
    HTTPClientError,
    ReadTimeoutError,
)

from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model import (
    TABLE,
)
from integrates.db_model.enums import (
    Source,
)
from integrates.db_model.vulnerabilities.types import (
    Vulnerability,
    VulnerabilityRequest,
)
from integrates.db_model.vulnerabilities.update import (
    update_historic,
)
from integrates.decorators import (
    retry_on_exceptions,
)
from integrates.dynamodb import (
    keys,
    operations,
)
from integrates.organizations.domain import (
    get_all_group_names,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)

# Constants
LOGGER = logging.getLogger(__name__)
LOGGER_CONSOLE = logging.getLogger("console")


def get_new_source(vuln: Vulnerability) -> Source | None:
    created_by = vuln.created_by

    if created_by == "dmora@fluidattacks.com":
        return Source.ESCAPE
    if created_by in [
        "machine@fluidattacks.com",
        "kamado@fluidattacks.com",
        "jrestrepo@kernelship.com",
        "drestrepo@fluidattacks.com",
        "rballestas@fluidattacks.com",
    ]:
        return Source.MACHINE
    if created_by == "lgarcia@fluidattacks.com":
        return Source.DETERMINISTIC
    if not created_by.endswith("@fluidattacks.com"):
        return Source.CUSTOMER
    if created_by.endswith("@fluidattacks.com"):
        return Source.ANALYST

    return None


@retry_on_exceptions(
    exceptions=(HTTPClientError, ReadTimeoutError, ConnectTimeoutError),
    sleep_seconds=10,
)
async def process_vuln(
    loaders: Dataloaders,
    vuln: Vulnerability,
) -> None:
    if isinstance(vuln.state.source, Source) and vuln.state.source != Source.ASM:
        return

    new_source = get_new_source(vuln)

    if new_source is None:
        return

    primary_key = keys.build_key(
        facet=TABLE.facets["vulnerability_metadata"],
        values={
            "id": vuln.id,
            "finding_id": vuln.finding_id,
        },
    )
    key_structure = TABLE.primary_key
    historic_state = await loaders.vulnerability_historic_state.load(
        VulnerabilityRequest(vulnerability_id=vuln.id, finding_id=vuln.finding_id),
    )
    fixed_historic_state = tuple(entry._replace(source=new_source) for entry in historic_state)

    await operations.update_item(
        condition_expression=Attr(key_structure.partition_key).exists(),
        item={
            "unreliable_indicators.unreliable_source": new_source,
        },
        key=primary_key,
        table=TABLE,
    )

    await update_historic(
        current_value=vuln,
        historic=fixed_historic_state,
    )

    LOGGER_CONSOLE.info(
        "Vulnerability updated",
        extra={
            "extra": {
                "vuln_id": vuln.id,
                "created_by": vuln.created_by,
                "old_source": vuln.state.source,
                "old_unreliable_source": (vuln.unreliable_indicators.unreliable_source),
                "new_source": new_source,
            },
        },
    )


async def process_group(group_name: str, progress: float) -> None:
    loaders = get_new_context()
    findings = await loaders.group_findings_all.load(
        group_name,
    )
    vulns = await loaders.finding_vulnerabilities_all.load_many_chained(
        [finding.id for finding in findings],
    )

    await collect(
        [process_vuln(loaders, vuln) for vuln in vulns],
        workers=10,
    )

    LOGGER_CONSOLE.info(
        "Group updated",
        extra={
            "extra": {
                "group_name": group_name,
                "progress": str(round(progress, 2)),
            },
        },
    )


async def main() -> None:
    loaders = get_new_context()
    group_names = sorted(await get_all_group_names(loaders=loaders))

    for count, group in enumerate(group_names):
        await process_group(
            group_name=group,
            progress=count / len(group_names),
        )


if __name__ == "__main__":
    execution_time = time.strftime("Execution Time:    %Y-%m-%d at %H:%M:%S %Z")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S %Z")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
