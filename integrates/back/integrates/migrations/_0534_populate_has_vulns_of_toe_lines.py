"""
_
Populate the has_vulnerabilities attribute of toe lines.

Start Time: 2024-05-03 at 20:34:00 UTC
Finalization Time: 2024-05-03 at 20:53:48 UTC
"""

import asyncio
import logging
import logging.config
import time

from aioextensions import (
    collect,
    run,
)
from aiohttp.client_exceptions import (
    ClientConnectorError,
    ClientOSError,
    ClientPayloadError,
    ServerDisconnectedError,
    ServerTimeoutError,
)
from boto3.dynamodb.conditions import (
    Attr,
    Key,
)
from botocore.exceptions import (
    ClientError,
    ConnectTimeoutError,
    HTTPClientError,
    ReadTimeoutError,
)

from integrates.class_types.types import (
    Item,
)
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model import (
    TABLE,
)
from integrates.decorators import (
    retry_on_exceptions,
)
from integrates.dynamodb import (
    keys,
    operations,
)
from integrates.dynamodb.exceptions import (
    UnavailabilityError,
)
from integrates.dynamodb.types import (
    PageInfo,
    PrimaryKey,
)
from integrates.organizations import (
    domain as orgs_domain,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)
LOGGER_CONSOLE = logging.getLogger("console")


NETWORK_ERRORS = (
    asyncio.TimeoutError,
    ClientConnectorError,
    ClientError,
    ClientOSError,
    ClientPayloadError,
    ConnectionError,
    ConnectionResetError,
    ConnectTimeoutError,
    HTTPClientError,
    ReadTimeoutError,
    ServerDisconnectedError,
    ServerTimeoutError,
    UnavailabilityError,
)


batch_delete_item = retry_on_exceptions(
    exceptions=NETWORK_ERRORS,
    sleep_seconds=300,
)(operations.batch_delete_item)


@retry_on_exceptions(
    exceptions=NETWORK_ERRORS,
    sleep_seconds=10,
)
async def _get_toe_lines_by_group(
    group_name: str,
    after: str | None,
) -> tuple[tuple[Item, ...], PageInfo]:
    primary_key = keys.build_key(
        facet=TABLE.facets["toe_lines_metadata"],
        values={"group_name": group_name},
    )
    index = None
    key_structure = TABLE.primary_key
    response = await operations.query(
        paginate=True,
        after=after,
        condition_expression=(
            Key(key_structure.partition_key).eq(primary_key.partition_key)
            & Key(key_structure.sort_key).begins_with(primary_key.sort_key.replace("#FILENAME", ""))
        ),
        facets=(TABLE.facets["toe_lines_metadata"],),
        index=index,
        table=TABLE,
    )

    return response.items, response.page_info


async def process_toe_lines(toe_item: Item) -> None:
    has_vulnerabilities = toe_item["state"].get("has_vulnerabilities")
    if has_vulnerabilities is not None:
        return
    key_structure = TABLE.primary_key
    item = {
        "state.has_vulnerabilities": False,
    }
    condition_expression = Attr(key_structure.partition_key).exists()
    await operations.update_item(
        condition_expression=condition_expression,
        item=item,
        key=PrimaryKey(partition_key=toe_item["pk"], sort_key=toe_item["sk"]),
        table=TABLE,
    )


async def process_group(group_name: str, count: int) -> None:
    LOGGER_CONSOLE.info(
        "Group",
        extra={
            "extra": {
                "group_name": group_name,
                "count": count,
            },
        },
    )
    after = None
    counter = 0
    while True:
        group_toe_lines, page_info = await _get_toe_lines_by_group(group_name, after)
        await collect(
            tuple(process_toe_lines(item) for item in group_toe_lines),
            workers=32,
        )
        LOGGER_CONSOLE.info(
            "Processing",
            extra={
                "extra": {
                    "group_name": group_name,
                    "count": count,
                    "group_toe_lines": len(group_toe_lines),
                },
            },
        )
        counter += len(group_toe_lines)
        after = page_info.end_cursor
        if not page_info.has_next_page:
            break

    LOGGER_CONSOLE.info(
        "Group processed",
        extra={
            "extra": {
                "group_name": group_name,
                "count": count,
                "group_toe_lines": counter,
            },
        },
    )


async def main() -> None:
    loaders: Dataloaders = get_new_context()
    all_group_names = sorted(await orgs_domain.get_all_group_names(loaders))
    LOGGER_CONSOLE.info(
        "All group names",
        extra={
            "extra": {
                "total": len(all_group_names),
            },
        },
    )
    for count, group_name in enumerate(all_group_names, start=1):
        await process_group(group_name, count)


if __name__ == "__main__":
    execution_time = time.strftime("Start Time:        %Y-%m-%d at %H:%M:%S UTC")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S UTC")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
