"""
Populate new domain group access field.

Execution Time:    2024-08-26 at 20:50:21 UTC
Finalization Time: 2024-08-26 at 20:57:36 UTC
"""

import logging
import logging.config
import time

from aioextensions import (
    collect,
    run,
)
from botocore.exceptions import (
    ConnectTimeoutError,
    ReadTimeoutError,
)

from integrates.custom_utils import (
    datetime as datetime_utils,
)
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model import (
    group_access as group_access_model,
)
from integrates.db_model.group_access.types import (
    GroupAccess,
    GroupAccessMetadataToUpdate,
    GroupAccessState,
    GroupStakeholdersAccessRequest,
)
from integrates.decorators import (
    retry_on_exceptions,
)
from integrates.organizations import (
    domain as orgs_domain,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)

# Constants
LOGGER = logging.getLogger(__name__)
LOGGER_CONSOLE = logging.getLogger("console")


@retry_on_exceptions(
    exceptions=(ReadTimeoutError, ConnectTimeoutError),
    sleep_seconds=3,
)
async def process_group_access(stakeholder_access: GroupAccess) -> None:
    new_state = stakeholder_access.state._replace(modified_date=datetime_utils.get_utc_now())
    await group_access_model.update_metadata(
        email=stakeholder_access.email,
        group_name=stakeholder_access.group_name,
        metadata=GroupAccessMetadataToUpdate(
            expiration_time=stakeholder_access.expiration_time,
            state=GroupAccessState(
                modified_date=new_state.modified_date,
                modified_by=new_state.modified_by,
                confirm_deletion=new_state.confirm_deletion,
                has_access=new_state.has_access,
                invitation=new_state.invitation,
                responsibility=new_state.responsibility,
                role=new_state.role,
            ),
        ),
    )


async def process_group(loaders: Dataloaders, group_name: str) -> None:
    group_stakeholders_access = await loaders.group_stakeholders_access.load(
        GroupStakeholdersAccessRequest(group_name=group_name),
    )
    await collect(
        tuple(process_group_access(group_access) for group_access in group_stakeholders_access),
        workers=100,
    )
    LOGGER_CONSOLE.info(
        "Group processed",
        extra={
            "extra": {
                "group_name": group_name,
                "group_stakeholders_access": len(group_stakeholders_access),
            },
        },
    )


async def main() -> None:
    loaders: Dataloaders = get_new_context()
    all_group_names = sorted(await orgs_domain.get_all_group_names(loaders))
    LOGGER_CONSOLE.info(
        "All group names",
        extra={
            "extra": {
                "total": len(all_group_names),
            },
        },
    )
    for count, group_name in enumerate(all_group_names, start=1):
        LOGGER_CONSOLE.info(
            "Group",
            extra={
                "extra": {
                    "group_name": group_name,
                    "count": count,
                },
            },
        )
        await process_group(loaders=loaders, group_name=group_name)


if __name__ == "__main__":
    execution_time = time.strftime("Execution Time:    %Y-%m-%d at %H:%M:%S %Z")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S %Z")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
