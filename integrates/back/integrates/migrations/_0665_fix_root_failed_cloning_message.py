"""
Identify roots where the cloning message does not correspond with the FAILED cloning status
and update it to the related event description

Execution Time:    2025-02-17 at 19:13:10 UTC
Finalization Time: 2025-02-17 at 19:16:43 UTC, extra=None
"""

import logging
import logging.config
import time

from aioextensions import collect, run

from integrates.custom_utils import roots as roots_utils
from integrates.dataloaders import Dataloaders, get_new_context
from integrates.db_model.events.enums import EventType
from integrates.db_model.roots.enums import RootCloningStatus
from integrates.db_model.roots.types import GitRoot
from integrates.organizations.domain import get_all_active_group_names
from integrates.roots.utils import update_root_cloning_status
from integrates.settings import LOGGING

logging.config.dictConfig(LOGGING)
LOGGER = logging.getLogger("migrations")
EMAIL = "integrates@fluidattacks.com"


async def process_root(loaders: Dataloaders, root: GitRoot) -> None:
    unsolved_events_by_root_id = await roots_utils.get_unsolved_events_by_root(
        loaders,
        root.group_name,
        specific_type=EventType.CLONING_ISSUES,
    )

    root_cloning_message = (
        unsolved_events_by_root_id[root.id][0].description
        if unsolved_events_by_root_id.get(root.id)
        else "Failed to clone without message"
    )
    await update_root_cloning_status(
        loaders=loaders,
        group_name=root.group_name,
        root_id=root.id,
        status=RootCloningStatus.FAILED,
        message=root_cloning_message,
        modified_by=EMAIL,
    )


async def process_group(group_name: str) -> None:
    loaders: Dataloaders = get_new_context()
    group_roots = await roots_utils.get_active_git_roots(loaders, group_name)

    roots_to_process = [
        root
        for root in group_roots
        if root.cloning.status == RootCloningStatus.FAILED
        and root.cloning.reason
        in {
            "The repository was not cloned as no new changes were detected.",
            "Cloned successfully",
        }
    ]

    if not roots_to_process:
        return

    await collect(
        [process_root(loaders, root) for root in roots_to_process],
        workers=4,
    )
    LOGGER.info(
        "Processed group %s, %d, %s",
        group_name,
        len(roots_to_process),
        [root.id for root in roots_to_process],
    )


async def main() -> None:
    loaders: Dataloaders = get_new_context()
    group_names = sorted(await get_all_active_group_names(loaders))
    for group_name in group_names:
        await process_group(group_name)


if __name__ == "__main__":
    execution_time = time.strftime("Execution Time:    %Y-%m-%d at %H:%M:%S UTC")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S UTC")
    LOGGER.info("\n%s\n%s", execution_time, finalization_time)
