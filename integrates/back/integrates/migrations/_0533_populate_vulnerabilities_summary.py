"""
Populate finding.unreliable_indicators.vulnerabilities_summary
for all findings.

Start Time:        2024-04-29 at 23:24:34 UTC
Finalization Time: 2024-04-29 at 23:31:24 UTC
Start Time:        2024-05-21 at 04:15:30 UTC
Finalization Time: 2024-05-21 at 05:12:27 UTC
Start Time:        2024-05-29 at 14:48:24 UTC
Finalization Time: 2024-05-29 at 15:40:17 UTC
"""

import asyncio
import logging
import logging.config
import time
from collections import (
    Counter,
)

from aioextensions import (
    collect,
    run,
)
from aiohttp.client_exceptions import (
    ClientConnectorError,
    ClientOSError,
    ClientPayloadError,
    ServerDisconnectedError,
    ServerTimeoutError,
)
from botocore.exceptions import (
    ClientError,
    ConnectTimeoutError,
    HTTPClientError,
    ReadTimeoutError,
)

from integrates.custom_exceptions import (
    IndicatorAlreadyUpdated,
)
from integrates.custom_utils.cvss import (
    get_severity_level,
)
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model import (
    findings as findings_model,
)
from integrates.db_model.findings.types import (
    Finding,
    FindingUnreliableIndicatorsToUpdate,
    FindingVulnerabilitiesSummary,
)
from integrates.db_model.vulnerabilities.enums import (
    VulnerabilityStateStatus,
)
from integrates.db_model.vulnerabilities.types import (
    FindingVulnerabilitiesRequest,
    FindingVulnerabilitiesZrRequest,
    Vulnerability,
)
from integrates.decorators import (
    retry_on_exceptions,
)
from integrates.dynamodb.exceptions import (
    ConditionalCheckFailedException,
    UnavailabilityError,
)
from integrates.organizations import (
    domain as orgs_domain,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)
LOGGER = logging.getLogger("console")
NETWORK_ERRORS = (
    asyncio.TimeoutError,
    ClientConnectorError,
    ClientError,
    ClientOSError,
    ClientPayloadError,
    ConnectionError,
    ConnectionResetError,
    ConnectTimeoutError,
    HTTPClientError,
    ReadTimeoutError,
    ServerDisconnectedError,
    ServerTimeoutError,
    UnavailabilityError,
)


def get_vuln_severity(vuln: Vulnerability, finding: Finding) -> str:
    score = (
        vuln.severity_score.threat_score
        if vuln.severity_score
        else finding.severity_score.threat_score
    )
    return get_severity_level(score)


def get_vuln_severity_v3(vuln: Vulnerability, finding: Finding) -> str:
    score = (
        vuln.severity_score.temporal_score
        if vuln.severity_score
        else finding.severity_score.temporal_score
    )
    return get_severity_level(score)


@retry_on_exceptions(
    exceptions=NETWORK_ERRORS,
    sleep_seconds=5,
)
async def process_finding(finding: Finding) -> None:
    loaders: Dataloaders = get_new_context()
    connection = await loaders.finding_vulnerabilities_released_nzr_c.load(
        FindingVulnerabilitiesZrRequest(
            finding_id=finding.id,
            paginate=False,
        ),
    )
    vulns = [edge.node for edge in connection.edges]
    vulns_open = [vuln for vuln in vulns if vuln.state.status == "VULNERABLE"]
    open_vulns_counter = Counter(get_vuln_severity(vuln, finding) for vuln in vulns_open)
    open_vulns_counter_v3 = Counter(get_vuln_severity_v3(vuln, finding) for vuln in vulns_open)
    drafts = await loaders.finding_vulnerabilities_draft_c.load_nodes(
        FindingVulnerabilitiesRequest(finding_id=finding.id),
    )
    vulns_summary = FindingVulnerabilitiesSummary(
        closed=len(vulns) - len(vulns_open),
        open=len(vulns_open),
        submitted=len(
            [draft for draft in drafts if draft.state.status is VulnerabilityStateStatus.SUBMITTED],
        ),
        rejected=len(
            [draft for draft in drafts if draft.state.status is VulnerabilityStateStatus.REJECTED],
        ),
        open_critical=open_vulns_counter.get("critical", 0),
        open_high=open_vulns_counter.get("high", 0),
        open_low=open_vulns_counter.get("low", 0),
        open_medium=open_vulns_counter.get("medium", 0),
        open_critical_v3=open_vulns_counter_v3.get("critical", 0),
        open_high_v3=open_vulns_counter_v3.get("high", 0),
        open_low_v3=open_vulns_counter_v3.get("low", 0),
        open_medium_v3=open_vulns_counter_v3.get("medium", 0),
    )
    vulnerabilities_summary = finding.unreliable_indicators.vulnerabilities_summary
    if vulnerabilities_summary == vulns_summary:
        return

    try:
        await findings_model.update_unreliable_indicators(
            current_value=finding.unreliable_indicators,
            group_name=finding.group_name,
            finding_id=finding.id,
            indicators=FindingUnreliableIndicatorsToUpdate(
                vulnerabilities_summary=vulns_summary,
            ),
        )
        LOGGER.info(
            "Finding updated",
            extra={
                "extra": {
                    "finding_id": finding.id,
                    "old_vulns_summary": vulnerabilities_summary,
                    "new_vulns_summary": vulns_summary,
                },
            },
        )
    except (ConditionalCheckFailedException, IndicatorAlreadyUpdated) as ex:
        LOGGER.error("Failed to update finding: %s - %s", finding.id, str(ex))


@retry_on_exceptions(
    exceptions=NETWORK_ERRORS,
    sleep_seconds=5,
)
async def process_group(group_name: str, progress: float) -> None:
    loaders: Dataloaders = get_new_context()
    all_findings = await loaders.group_findings_all.load(group_name)
    await collect(
        [process_finding(finding) for finding in all_findings],
        workers=8,
    )
    LOGGER.info(
        "Group processed",
        extra={
            "extra": {
                "group_name": group_name,
                "findings_processed": len(all_findings),
                "progress": round(progress, 2),
            },
        },
    )


@retry_on_exceptions(
    exceptions=NETWORK_ERRORS,
    sleep_seconds=5,
)
async def main() -> None:
    loaders: Dataloaders = get_new_context()
    group_names = sorted(await orgs_domain.get_all_group_names(loaders=loaders))
    LOGGER.info(
        "All groups",
        extra={
            "extra": {
                "groups_len": len(group_names),
                "group_names": group_names,
            },
        },
    )
    await collect(
        [
            process_group(
                group_name=group_name,
                progress=count / len(group_names),
            )
            for count, group_name in enumerate(group_names)
        ],
        workers=1,
    )


if __name__ == "__main__":
    execution_time = time.strftime("Start Time:    %Y-%m-%d at %H:%M:%S UTC")
    LOGGER.info("%s", execution_time)
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S UTC")
    LOGGER.info("\n%s\n%s\n", execution_time, finalization_time)
