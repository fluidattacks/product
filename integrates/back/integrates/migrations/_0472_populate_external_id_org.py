"""
Populate field aws_external_id for organizations

Start Time:        2024-01-11 at 16:27:30 UTC
Finalization Time: 2023-01-11 at 16:30:15 UTC

"""

import logging
import logging.config
import time
from uuid import (
    uuid4,
)

from aioextensions import (
    run,
)

from integrates.custom_utils.datetime import (
    get_utc_now,
)
from integrates.db_model.organizations.types import (
    OrganizationState,
)
from integrates.db_model.organizations.update import (
    update_state,
)
from integrates.organizations.domain import (
    iterate_organizations,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)

LOGGER = logging.getLogger(__name__)


async def main() -> None:
    async for organization in iterate_organizations():
        external_id = str(uuid4())
        new_state = OrganizationState(
            status=organization.state.status,
            modified_by="ugomez@fluidattacks.com",
            modified_date=get_utc_now(),
            aws_external_id=external_id,
            pending_deletion_date=organization.state.pending_deletion_date,
        )
        await update_state(
            organization_id=organization.id,
            organization_name=organization.name,
            state=new_state,
        )


if __name__ == "__main__":
    execution_time = time.strftime("Execution Time:    %Y-%m-%d at %H:%M:%S UTC")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S UTC")
    LOGGER.info("\n%s\n%s", execution_time, finalization_time)
