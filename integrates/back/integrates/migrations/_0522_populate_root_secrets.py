# type: ignore

"""
Populate modified_date, modified_by, created_at, owner and move description to
state field

Start Time: 2024-04-11 at 22:11:36 UTC
Finalization Time: 2024-04-11 at 22:18:13 UTC
"""

import logging
import logging.config
import time

from aioextensions import (
    collect,
    run,
)
from boto3.dynamodb.conditions import (
    Attr,
)

from integrates.class_types.types import (
    Item,
)
from integrates.custom_utils.datetime import (
    get_iso_date,
)
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model import (
    TABLE,
)
from integrates.db_model.roots.types import (
    Root,
    Secret,
)
from integrates.db_model.utils import (
    get_as_utc_iso_format,
)
from integrates.dynamodb import (
    keys,
    operations,
)
from integrates.organizations import (
    domain as orgs_domain,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)
LOGGER_CONSOLE = logging.getLogger("console")
EMAIL_INTEGRATES = "integrates@fluidattacks.com"


async def process_root(loaders: Dataloaders, root: Root) -> None:
    secrets = await loaders.root_secrets.load(root.id)
    await collect(
        tuple(process_secret(root, secret) for secret in secrets),
        workers=32,
    )
    LOGGER_CONSOLE.info("Root processed  %s", root.id)


def _format_state(
    old_secret: Secret,
    root: Root,
) -> Item:
    if old_secret.state:
        return {
            "owner": old_secret.state.owner if old_secret.state.owner else root.created_by,
            "description": old_secret.state.description
            if old_secret.state.description
            else old_secret.description,
            "modified_by": old_secret.state.modified_by
            if old_secret.state.modified_by
            else EMAIL_INTEGRATES,
            "modified_date": get_as_utc_iso_format(old_secret.state.modified_date)
            if old_secret.state.modified_date
            else get_iso_date(),
        }

    return {
        "owner": root.created_by,
        "description": old_secret.description,
        "modified_by": EMAIL_INTEGRATES,
        "modified_date": get_iso_date(),
    }


async def process_secret(
    root: Root,
    old_secret: Secret,
) -> None:
    key_structure = TABLE.primary_key
    secret_key = keys.build_key(
        facet=TABLE.facets["root_secret"],
        values={"uuid": root.id, "key": old_secret.key},
    )
    secret_item = {
        "key": old_secret.key,
        "value": old_secret.value,
        "description": None,
        "created_at": get_as_utc_iso_format(old_secret.created_at)
        if old_secret.created_at
        else get_as_utc_iso_format(root.created_date),
        "state": _format_state(old_secret, root),
    }
    await operations.update_item(
        condition_expression=(Attr(key_structure.partition_key).exists()),
        item=secret_item,
        key=secret_key,
        table=TABLE,
    )
    LOGGER_CONSOLE.info("Secret processed  %s", old_secret.key)


async def process_group(
    loaders: Dataloaders,
    group_name: str,
) -> None:
    group_roots: list[Root] = await loaders.group_roots.load(group_name)
    await collect(
        tuple(process_root(loaders, root) for root in group_roots),
        workers=32,
    )
    LOGGER_CONSOLE.info("Group processed  %s", group_name)


async def main() -> None:
    loaders: Dataloaders = get_new_context()
    all_group_names = sorted(await orgs_domain.get_all_group_names(loaders=loaders))
    count = 0
    LOGGER_CONSOLE.info("all_group_names %s", len(all_group_names))
    for group_name in all_group_names:
        count += 1
        LOGGER_CONSOLE.info("group %s %s", group_name, count)
        await process_group(loaders, group_name)


if __name__ == "__main__":
    execution_time = time.strftime("Start Time:    %Y-%m-%d at %H:%M:%S UTC")
    LOGGER_CONSOLE.info("%s", execution_time)
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S UTC")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
