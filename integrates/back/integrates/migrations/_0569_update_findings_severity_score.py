"""
Update cvss_v4 field in findings.
Execution Time:    2024-07-23 at 20:21:20 UTC
Finalization Time: 2024-07-23 at 20:24:31 UTC
Execution Time:    2024-09-04 at 03:30:33 UTC
Finalization Time: 2024-09-04 at 03:38:37 UTC
"""

import contextlib
import logging
import logging.config
import time
from decimal import (
    Decimal,
)

from aioextensions import (
    collect,
    run,
)
from botocore.exceptions import (
    HTTPClientError,
)

from integrates.custom_exceptions import (
    FindingNotFound,
)
from integrates.custom_utils.cvss import (
    get_severity_score_from_cvss_vector,
)
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model.findings.enums import (
    FindingStateStatus,
)
from integrates.db_model.findings.types import (
    Finding,
)
from integrates.decorators import (
    retry_on_exceptions,
)
from integrates.findings.domain.core import (
    update_severity,
)
from integrates.organizations.domain import (
    get_all_group_names,
)
from integrates.settings import (
    LOGGING,
)
from integrates.vulnerability_files.utils import (
    get_from_v3,
)

logging.config.dictConfig(LOGGING)

# Constants
LOGGER = logging.getLogger(__name__)
LOGGER_CONSOLE = logging.getLogger("console")


async def process_finding(loaders: Dataloaders, finding: Finding) -> None:
    if finding.state.status is FindingStateStatus.DELETED:
        return
    if finding.severity_score.threat_score == Decimal(
        "0.0",
    ) and finding.severity_score.temporal_score > Decimal("0.0"):
        cvss_v3 = finding.severity_score.cvss_v3
        cvss_v4 = str(
            get_from_v3(
                finding.severity_score.cvss_v3,
            ),
        )

        with contextlib.suppress(FindingNotFound):
            await update_severity(loaders, finding.id, cvss_v3, cvss_v4)

        LOGGER_CONSOLE.info(
            "Finding updated",
            extra={
                "extra": {
                    "finding_id": finding.id,
                    "cvss_v4": finding.severity_score.cvss_v4,
                    "cvss_v3": finding.severity_score.cvss_v3,
                    "new_score": get_severity_score_from_cvss_vector(
                        finding.severity_score.cvss_v3,
                        str(
                            get_from_v3(
                                finding.severity_score.cvss_v3,
                            ),
                        ),
                    ),
                },
            },
        )


@retry_on_exceptions(
    exceptions=(HTTPClientError,),
    sleep_seconds=10,
)
async def process_group(group_name: str, progress: float) -> None:
    loaders = get_new_context()
    all_findings = await loaders.group_findings_all.load(group_name)

    await collect(
        [process_finding(loaders, finding) for finding in all_findings],
        workers=16,
    )

    LOGGER_CONSOLE.info(
        "Group updated",
        extra={
            "extra": {
                "group_name": group_name,
                "progress": str(round(progress, 2)),
            },
        },
    )


async def main() -> None:
    loaders = get_new_context()
    group_names = sorted(await get_all_group_names(loaders=loaders))

    for count, group in enumerate(group_names):
        await process_group(
            group_name=group,
            progress=count / len(group_names),
        )


if __name__ == "__main__":
    execution_time = time.strftime("Execution Time:    %Y-%m-%d at %H:%M:%S UTC%Z")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S UTC%Z")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
