# type: ignore
"""
Update the 'where' field for some vulnerabilities.

Fix CSPM vulnerabilities with wrong ARN

Execution Time: 2024-07-11 at 22:38:55 UTC
Finalization Time: 2024-07-11 at 23:28:57 UTC, extra=None

"""

from aioextensions import (
    run,
)
from boto3.dynamodb.conditions import (
    Key,
)
from datetime import (
    datetime,
)
import hashlib
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model import (
    TABLE,
)
from integrates.db_model.enums import (
    Source,
)
from integrates.db_model.vulnerabilities.enums import (
    VulnerabilityStateReason,
    VulnerabilityStateStatus,
)
from integrates.db_model.vulnerabilities.types import (
    Vulnerability,
    VulnerabilityMetadataToUpdate,
)
from integrates.db_model.vulnerabilities.update import (
    update_historic_entry,
    update_metadata,
)
from integrates.dynamodb import (
    keys,
    operations,
)
from integrates.organizations import (
    domain as orgs_domain,
)
from integrates.settings import (
    LOGGING,
)
import logging
import logging.config
import time

logging.config.dictConfig(LOGGING)
LOGGER_CONSOLE = logging.getLogger("console")

# flake8: noqa
BROKEN_ARN_METHODS = {
    "aws.apigateway_allows_anonymous_access": "arn:aws:apigateway:${Region}::/restapis/${RestApiId}",
    "aws.has_unencrypted_amis": "arn:aws:ec2:${Region}::image/${ImageId}",
    "aws.has_publicly_shared_amis": "arn:aws:ec2:${Region}::image/${ImageId}",
    "aws.has_unencrypted_snapshots": "arn:aws:ec2:${Region}::snapshot/${SnapshotId}",
}


async def process_vulnerability(vulnerability: Vulnerability, new_where: str, data: dict) -> None:
    new_hash = int.from_bytes(
        hashlib.sha256(
            bytes(
                (
                    new_where
                    + vulnerability.state.specific
                    + vulnerability.finding_id
                    + vulnerability.skims_method
                ),
                encoding="utf-8",
            )
        ).digest()[:8],
        "little",
    )
    original_vuln = vulnerability
    new_state = vulnerability.state._replace(
        where=new_where,
        modified_date=datetime.utcnow(),
        reasons=[VulnerabilityStateReason.CONSISTENCY],
        modified_by="lsaavedra@fluidattacks.com",
    )

    await update_historic_entry(
        current_value=original_vuln,
        finding_id=vulnerability.finding_id,
        entry=new_state,
        vulnerability_id=vulnerability.id,
    )

    await update_metadata(
        finding_id=vulnerability.finding_id,
        metadata=VulnerabilityMetadataToUpdate(hash=new_hash),
        root_id=vulnerability.root_id,
        vulnerability_id=vulnerability.id,
    )

    await process_toe(data)


async def process_toe(data) -> None:
    primary_key = keys.build_key(
        facet=TABLE.facets["toe_input_metadata"],
        values={
            "component": data["component"],
            "entry_point": data["entry_point"],
            "group_name": data["group_name"],
            "root_id": data["root_id"],
        },
    )
    key_structure = TABLE.primary_key

    condition_expression = Key(key_structure.partition_key).eq(primary_key.partition_key) & Key(
        key_structure.sort_key
    ).begins_with("INPUTS#")
    item = {
        "component": data["new_component"],
    }
    await operations.update_item(
        condition_expression=condition_expression,
        item=item,
        key=primary_key,
        table=TABLE,
    )


def build_arn(arn: str, parameters: dict[str, str]) -> str:
    arn = arn.replace("$", "")
    interpolate_arn = arn.format(**parameters)
    return interpolate_arn


async def process_group(loaders: Dataloaders, group: str) -> None:
    findings = await loaders.group_findings.load(group)
    vulns = await loaders.finding_vulnerabilities.load_many_chained([fin.id for fin in findings])
    get_keys = set(BROKEN_ARN_METHODS.keys())
    machine_vulns = [
        vuln
        for vuln in vulns
        if vuln.skims_method
        and vuln.state.source == Source.MACHINE
        and vuln.skims_method in get_keys
        and vuln.state.status == VulnerabilityStateStatus.VULNERABLE
    ]

    for vuln in machine_vulns:
        group_name = group
        where = vuln.state.where
        where_split = where.split(" ")
        component = where_split[0]
        namespace = where_split[1]

        region = component.split(":")[3]
        resource_id = component.split(":")[-1].split("/")[-1]

        arn = BROKEN_ARN_METHODS[vuln.skims_method]
        translation_table = str.maketrans("", "", "{}$")
        id_name = arn.split("/")[-1].translate(translation_table)
        parameters = {
            "Region": region,
            id_name: resource_id,
        }

        new_arn = build_arn(arn, parameters)
        new_where = new_arn + " " + namespace

        data = {
            "group_name": group_name,
            "entry_point": "",
            "component": component,
            "root_id": vuln.root_id,
            "new_component": new_arn,
        }

        LOGGER_CONSOLE.info(
            "Method: %s method, Where: %s, New where: %s",
            vuln.skims_method,
            vuln.state.where,
            new_where,
        )
        await process_vulnerability(vuln, new_where, data)


async def main() -> None:
    loaders = get_new_context()
    groups = await orgs_domain.get_all_active_group_names(loaders=loaders)

    for group in groups:
        await process_group(loaders, group)


if __name__ == "__main__":
    execution_time = time.strftime("Execution Time:    %Y-%m-%d at %H:%M:%S %Z")

    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S %Z")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
