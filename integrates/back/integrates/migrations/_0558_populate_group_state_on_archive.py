"""
Restore and archive on redshift data related to groups' state, lost during
a known time window

Execution Time:    2024-07-03 at 15:42:44
Finalization Time: 2024-07-03 at 15:44:53, extra=None
"""

import csv
import logging
import time
from datetime import (
    datetime,
    timedelta,
)

from aioextensions import (
    run,
)
from boto3.dynamodb.conditions import (
    Key,
)

from integrates.class_types.types import (
    Item,
)
from integrates.db_model import (
    TABLE,
)
from integrates.db_model.utils import (
    get_as_utc_iso_format,
)
from integrates.dynamodb import (
    keys,
    operations,
)
from integrates.dynamodb.types import (
    PrimaryKey,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)

LOGGER = logging.getLogger(__name__)
LOGGER_CONSOLE = logging.getLogger("console")

BACKUP_TABLE_NAMES = [
    "vms_backup_may_12",
    "vms_backup_may_05",
    "vms_backup_apr_28",
    "vms_backup_apr_21",
    "vms_backup_apr_14",
    "vms_backup_apr_07",
    "vms_backup_mar_03",
]
EMAIL_INTEGRATES = "integrates@fluidattacks.com"


def get_deleted_state_item(group_name: str, raw: Item) -> Item | None:
    if raw["status"] == "DELETED":
        return None

    key_structure = TABLE.primary_key
    modified_date_str = get_as_utc_iso_format(
        datetime.fromisoformat(raw["modified_date"]) + timedelta(seconds=1),
    )
    primary_key = keys.build_key(
        facet=TABLE.facets["group_historic_state"],
        values={
            "name": group_name,
            "iso8601utc": modified_date_str,
        },
    )

    return {
        **raw,
        key_structure.partition_key: primary_key.partition_key,
        key_structure.sort_key: primary_key.sort_key,
        "comments": "Added for data consistency",
        "has_essential": False,
        "has_advanced": False,
        "has_machine": False,
        "has_squad": False,
        "justification": "MIGRATION",
        "modified_date": modified_date_str,
        "modified_by": EMAIL_INTEGRATES,
        "status": "DELETED",
    }


async def search_group_on_backups(primary_key: PrimaryKey) -> tuple[Item, ...]:
    key_structure = TABLE.primary_key
    items: tuple[Item, ...] = tuple()
    for backup_table_name in BACKUP_TABLE_NAMES:
        response = await operations.query(
            condition_expression=(Key(key_structure.partition_key).eq(primary_key.partition_key)),
            facets=(
                TABLE.facets["group_metadata"],
                TABLE.facets["group_historic_state"],
                TABLE.facets["group_unreliable_indicators"],
            ),
            table=TABLE._replace(name=backup_table_name),
        )
        if response.items:
            return response.items

    return items


async def process_group(group_name: str) -> None:
    primary_key = keys.build_key(
        facet=TABLE.facets["group_metadata"],
        values={
            "name": group_name,
            "organization_id": "",
        },
    )
    items_to_insert = await search_group_on_backups(primary_key)
    if not items_to_insert:
        LOGGER.warning(
            "No info found for: %s",
            group_name,
        )
        return

    key_structure = TABLE.primary_key
    metadata_item = next(
        item
        for item in items_to_insert
        if item[key_structure.partition_key] == primary_key.partition_key
        and str(item[key_structure.sort_key]).startswith(primary_key.sort_key)
    )
    if deleted_state := get_deleted_state_item(group_name=group_name, raw=metadata_item["state"]):
        items_to_insert = (deleted_state, *items_to_insert)

    await operations.batch_put_item(items=items_to_insert, table=TABLE)
    await operations.batch_delete_item(
        keys=tuple(
            PrimaryKey(
                partition_key=item[key_structure.partition_key],
                sort_key=item[key_structure.sort_key],
            )
            for item in items_to_insert
        ),
        table=TABLE,
    )
    LOGGER.info(
        "Processed: %s, items: %s",
        group_name,
        len(items_to_insert),
    )


async def main() -> None:
    with open("_0558_data.csv", encoding="utf8") as file:
        group_names = [row[0] for row in csv.reader(file)]

    LOGGER.info(
        "Groups to process: %s, items: %s",
        group_names,
        len(group_names),
    )
    for group_name in group_names:
        await process_group(group_name)


if __name__ == "__main__":
    execution_time = time.strftime("Execution Time:    %Y-%m-%d at %H:%M:%S")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
