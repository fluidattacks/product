"""
Find inconsistencies on organization access items

Execution Time:    2024-11-27 at 16:01:25 UTC
Finalization Time: 2024-11-27 at 16:02:35 UTC

"""

import logging
import time

from aioextensions import (
    run,
)
from boto3.dynamodb.conditions import (
    Key,
)

from integrates.class_types.types import Item
from integrates.db_model import TABLE
from integrates.db_model.organizations.get import get_all_organizations
from integrates.dynamodb import keys, operations
from integrates.dynamodb.types import Facet

LOGGER = logging.getLogger("console")

PSEUDO_FACET = Facet(
    attrs=("email", "has_access", "organization_id", "role", "pk_2", "sk_2"),
    pk_alias="USER#org#org_id",
    sk_alias="ORG#email",
)


async def _get_org_inconsistent_items(org_id: str) -> tuple[Item, ...]:
    key_structure = TABLE.primary_key
    primary_key = keys.build_key(
        facet=PSEUDO_FACET, values={"org_id": org_id.split("#")[1], "org": "org"}
    )
    response = await operations.query(
        condition_expression=(Key(key_structure.partition_key).eq(primary_key.partition_key)),
        facets=(PSEUDO_FACET,),
        table=TABLE,
    )
    return response.items


async def main() -> None:
    orgs = await get_all_organizations()
    for org in orgs:
        items = await _get_org_inconsistent_items(org.id)
        if len(items) > 0:
            LOGGER.info("Found %d inconsistencies for org: %s", len(items), org.name)
        else:
            LOGGER.info("No inconsistent items found for org: %s", org.name)


if __name__ == "__main__":
    execution_time = time.strftime("Execution Time:    %Y-%m-%d at %H:%M:%S UTC")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S UTC")
    LOGGER.info("\n%s\n%s", execution_time, finalization_time)
