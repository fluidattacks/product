# type: ignore
"""
Remove machine reports from F409 finding since this was a FP

Execution Time:    2023-09-01 at 21:37:38 UTC
Finalization Time: 2023-09-01 at 21:52:15 UTC
"""

import csv
import time

from aioextensions import (
    collect,
    run,
)

from integrates.dataloaders import (
    get_new_context,
)
from integrates.db_model.enums import (
    Source,
    StateRemovalJustification,
)
from integrates.db_model.vulnerabilities.enums import (
    VulnerabilityStateReason,
)
from integrates.findings.domain import (
    remove_finding,
)
from integrates.organizations.domain import (
    get_all_active_group_names,
)
from integrates.unreliable_indicators.enums import (
    EntityAttr,
)
from integrates.unreliable_indicators.operations import (
    update_finding_unreliable_indicators,
)
from integrates.vulnerabilities.domain import (
    remove_vulnerability,
)


async def close_machine_vulnerabilities(
    loaders,
    group: str,
    fin_enum: str,
    method_name: str,
) -> None:
    findings = await loaders.group_findings.load(group)
    findings_numb = [finding for finding in findings if finding.title.startswith(fin_enum)]
    findings_vulns = await loaders.finding_vulnerabilities.load_many(
        [finding.id for finding in findings_numb],
    )
    rows = []
    for finding, vulns in zip(findings_numb, findings_vulns, strict=False):
        wrong_vulns = [
            vuln
            for vuln in vulns
            if (
                (
                    vuln.state.source == Source.MACHINE
                    or vuln.hacker_email == "machine@fluidattacks.com"
                )
                and vuln.skims_method is not None
                and vuln.skims_method.endswith(method_name)
            )
        ]
        if len(wrong_vulns) > 0 and len(wrong_vulns) == len(vulns):
            rows.append([group, finding.title[:3], finding.id, "FINDING REMOVED"])

            await remove_finding(
                loaders,
                "flagos@fluidattacks.com",
                finding.id,
                StateRemovalJustification.FALSE_POSITIVE,
                Source.MACHINE,
            )

        elif len(wrong_vulns) > 0:
            rows.append(
                [
                    group,
                    finding.title[:3],
                    finding.id,
                    len(wrong_vulns),
                    "VULNS REMOVED",
                ],
            )

            await collect(
                (
                    remove_vulnerability(
                        loaders=loaders,
                        finding_id=vuln.finding_id,
                        vulnerability_id=vuln.id,
                        justification=VulnerabilityStateReason.FALSE_POSITIVE,
                        email="flagos@fluidattacks.com",
                        include_closed_vuln=True,
                    )
                    for vuln in wrong_vulns
                ),
                workers=15,
            )

            await update_finding_unreliable_indicators(
                finding.id,
                {
                    EntityAttr.newest_vulnerability_report_date,
                    EntityAttr.oldest_open_vulnerability_report_date,
                    EntityAttr.oldest_vulnerability_report_date,
                    EntityAttr.status,
                    EntityAttr.where,
                    EntityAttr.treatment_summary,
                    EntityAttr.verification_summary,
                },
            )

    with open("removed_409_machine.csv", "a+", encoding="utf-8") as handler:
        writer = csv.writer(handler)
        writer.writerows(rows)


async def main() -> None:
    loaders = get_new_context()
    groups = sorted(await get_all_active_group_names(loaders))
    fin_enum = "409"
    # Method name was altered for the spell checker
    # However, migration was run using the correct name in the skims model
    method_name = "server_side_encryption_disabled"
    for group in groups:
        await close_machine_vulnerabilities(loaders, group, fin_enum, method_name)


if __name__ == "__main__":
    execution_time = time.strftime("Execution Time:    %Y-%m-%d at %H:%M:%S UTC")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S UTC")
