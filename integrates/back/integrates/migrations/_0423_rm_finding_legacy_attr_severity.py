"""
Remove map severity from the metadata item for all findings in db.
It holds the legacy metrics for the severity representation,
now deprecated in favor of the vector string.

Start Time:        2023-08-01 at 22:00:53 UTC
Finalization Time: 2023-08-01 at 22:14:25 UTC
"""

import logging
import logging.config
import time

from aioextensions import (
    collect,
    run,
)
from boto3.dynamodb.conditions import (
    Attr,
    Key,
)

from integrates.class_types.types import (
    Item,
)
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model import (
    TABLE,
)
from integrates.dynamodb import (
    keys,
    operations,
)
from integrates.dynamodb.types import (
    PrimaryKey,
)
from integrates.organizations import (
    domain as orgs_domain,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)
LOGGER_CONSOLE = logging.getLogger("console")

ATTR_TO_REMOVE = "severity"


async def process_finding(item: Item) -> None:
    primary_key = PrimaryKey(partition_key=item["pk"], sort_key=item["sk"])
    key_structure = TABLE.primary_key
    await operations.update_item(
        condition_expression=Attr(key_structure.partition_key).exists(),
        item={ATTR_TO_REMOVE: None},
        key=primary_key,
        table=TABLE,
    )


async def get_findings_by_group(
    group_name: str,
) -> tuple[Item, ...]:
    primary_key = keys.build_key(
        facet=TABLE.facets["finding_metadata"],
        values={"group_name": group_name},
    )
    index = TABLE.indexes["inverted_index"]
    key_structure = index.primary_key
    response = await operations.query(
        condition_expression=(
            Key(key_structure.partition_key).eq(primary_key.sort_key)
            & Key(key_structure.sort_key).begins_with(primary_key.partition_key)
        ),
        filter_expression=Attr(ATTR_TO_REMOVE).exists(),
        facets=(TABLE.facets["finding_metadata"],),
        index=index,
        table=TABLE,
    )

    return response.items


async def process_group(group_name: str, progress: float) -> None:
    if not (group_findings := await get_findings_by_group(group_name)):
        return

    await collect(
        tuple(process_finding(item) for item in group_findings),
        workers=64,
    )
    LOGGER_CONSOLE.info(
        "Group processed %s %s %s",
        group_name,
        f"{round(progress, 2)!s}",
        f"{len(group_findings)=}",
    )


async def main() -> None:
    loaders: Dataloaders = get_new_context()
    all_group_names = sorted(await orgs_domain.get_all_group_names(loaders=loaders))
    LOGGER_CONSOLE.info("%s", f"{all_group_names=}")
    LOGGER_CONSOLE.info("%s", f"{len(all_group_names)=}")
    await collect(
        tuple(
            process_group(
                group_name=group,
                progress=count / len(all_group_names),
            )
            for count, group in enumerate(all_group_names)
        ),
        workers=1,
    )


if __name__ == "__main__":
    execution_time = time.strftime("Start Time:        %Y-%m-%d at %H:%M:%S UTC")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S UTC")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
