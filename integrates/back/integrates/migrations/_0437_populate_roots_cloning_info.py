# type: ignore
"""
Populate git root's cloning info with commit and failed cloning count.
This will allow the removal of the historic cloning

Start Time:         2023-09-14 at 17:36:16 UTC
Finalization Time:  2023-09-14 at 17:41:32 UTC

Start Time:         2023-09-20 at 04:16:05 UTC
Finalization Time:  2023-09-20 at 05:02:31 UTC
"""

import logging
import logging.config
import time
from datetime import (
    datetime,
)
from itertools import (
    groupby,
)
from operator import (
    attrgetter,
)
from typing import (
    NamedTuple,
)

from aioextensions import (
    collect,
    run,
)
from boto3.dynamodb.conditions import (
    Attr,
)

from integrates.custom_exceptions import (
    GroupNotFound,
)
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model import (
    TABLE,
)
from integrates.db_model.roots.enums import (
    RootCloningStatus,
)
from integrates.db_model.roots.types import (
    GitRoot,
    GitRootCloning,
)
from integrates.db_model.utils import (
    get_as_utc_iso_format,
)
from integrates.dynamodb import (
    keys,
    operations,
)
from integrates.organizations import (
    domain as orgs_domain,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)
LOGGER_CONSOLE = logging.getLogger("console")


class CloningInfo(NamedTuple):
    commit: str | None
    commit_date: datetime | None
    failed_count: int
    successful_count: int


async def historic_cloning_grouped(
    loaders: Dataloaders,
    root_id: str,
) -> tuple[tuple[GitRootCloning, ...], ...]:
    """Returns the history of cloning failures and successes grouped"""
    loaders.root_historic_cloning.clear(root_id)
    historic_cloning = await loaders.root_historic_cloning.load(root_id)
    filtered_historic_cloning: tuple[GitRootCloning, ...] = tuple(
        filter(
            lambda cloning: cloning.status in [RootCloningStatus.OK, RootCloningStatus.FAILED],
            historic_cloning,
        ),
    )
    grouped_historic_cloning = tuple(
        tuple(group) for _, group in groupby(filtered_historic_cloning, key=attrgetter("status"))
    )

    return grouped_historic_cloning


async def update_git_root_cloning(
    *,
    cloning_info: CloningInfo,
    current_value: GitRootCloning,
    group_name: str,
    root_id: str,
) -> None:
    key_structure = TABLE.primary_key
    primary_key = keys.build_key(
        facet=TABLE.facets["git_root_metadata"],
        values={"name": group_name, "uuid": root_id},
    )
    await operations.update_item(
        condition_expression=(
            Attr(key_structure.partition_key).exists()
            & Attr("cloning.modified_date").eq(get_as_utc_iso_format(current_value.modified_date))
        ),
        item={
            "cloning.commit": cloning_info.commit,
            "cloning.commit_date": get_as_utc_iso_format(cloning_info.commit_date)
            if cloning_info.commit_date
            else None,
            "cloning.failed_count": cloning_info.failed_count,
            "cloning.successful_count": cloning_info.successful_count,
        },
        key=primary_key,
        table=TABLE,
    )


async def get_root_cloning_info(loaders: Dataloaders, root: GitRoot) -> CloningInfo:
    historic_cloning = await loaders.root_historic_cloning.load(root.id)
    cloning_ok = next(
        (cloning for cloning in reversed(historic_cloning) if cloning.status == "OK"),
        None,
    )

    failed_count = 0
    successful_count = 0
    if status_changes := await historic_cloning_grouped(loaders, root.id):
        last_cloning_status_group = status_changes[-1]
        failed_count = (
            len(last_cloning_status_group)
            if last_cloning_status_group[-1].status == RootCloningStatus.FAILED
            else 0
        )
        successful_count = (
            len(last_cloning_status_group)
            if last_cloning_status_group[-1].status == RootCloningStatus.OK
            else 0
        )

    if root.cloning.status == "OK":
        return CloningInfo(
            commit=root.cloning.commit,
            commit_date=root.cloning.commit_date,
            failed_count=0,
            successful_count=successful_count,
        )

    return CloningInfo(
        commit=cloning_ok.commit if cloning_ok else None,
        commit_date=cloning_ok.commit_date if cloning_ok else None,
        failed_count=failed_count,
        successful_count=successful_count,
    )


async def process_root(loaders: Dataloaders, root: GitRoot) -> None:
    cloning_info = await get_root_cloning_info(loaders, root)
    LOGGER_CONSOLE.info(
        "Root: %s \t%s %s \t%s",
        root.group_name,
        root.id,
        root.state.nickname,
        str(cloning_info),
    )
    await update_git_root_cloning(
        cloning_info=cloning_info,
        current_value=root.cloning,
        group_name=root.group_name,
        root_id=root.id,
    )


async def process_group(loaders: Dataloaders, group_name: str) -> None:
    group = await loaders.group.load(group_name)
    if not group:
        raise GroupNotFound()

    roots = tuple(
        root for root in await loaders.group_roots.load(group_name) if isinstance(root, GitRoot)
    )
    await collect(
        tuple(process_root(loaders, root) for root in roots),
        workers=5,
    )


async def main() -> None:
    loaders: Dataloaders = get_new_context()
    all_group_names = sorted(await orgs_domain.get_all_group_names(loaders=loaders))
    await collect(
        tuple(process_group(loaders, group) for group in all_group_names),
        workers=5,
    )


if __name__ == "__main__":
    execution_time = time.strftime("Start Time:    %Y-%m-%d at %H:%M:%S UTC")
    LOGGER_CONSOLE.info("%s", execution_time)
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S UTC")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
