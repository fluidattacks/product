"""
Remove root_url
Execution Time:    2024-10-17 at 19:37:32 UTC
Finalization Time: 2024-10-17 at 19:37:33 UTC
Execution Time:    2024-10-17 at 21:30:00 UTC
Finalization Time: 2024-10-17 at 21:30:01 UTC
"""

import logging
import logging.config
import time

from aioextensions import (
    collect,
    run,
)

from integrates.db_model import (
    TABLE,
)
from integrates.dynamodb import (
    keys,
    operations,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)

# Constants
LOGGER = logging.getLogger(__name__)
LOGGER_CONSOLE = logging.getLogger("console")


async def process_url(*, url: str, branch: str, group_name: str) -> None:
    current_url_key = keys.build_key(
        facet=TABLE.facets["git_root_metadata"],
        values={
            "name": group_name,
            "uuid": f"URL#{url}#BRANCH#{branch}",
        },
    )
    await operations.delete_item(key=current_url_key, table=TABLE)


async def main() -> None:
    urls: list[tuple[str, str]] = []
    await collect([process_url(url=url[0], branch=url[1], group_name="") for url in urls])


if __name__ == "__main__":
    execution_time = time.strftime("Execution Time:    %Y-%m-%d at %H:%M:%S %Z")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S %Z")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
