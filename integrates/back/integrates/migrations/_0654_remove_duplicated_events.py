"""
Delete duplicated, unsolved events of CLONING_ISSUES type.

Execution Time: 2025-01-27 at 19:33:50 UTC
Finalization Time: 2025-01-27 at 19:35:57 UTC

Execution Time: 2025-01-28 at 03:08:06 UTC
Finalization Time: 2025-01-28 at 03:11:28 UTC

Execution Time:     2025-02-14 at 16:24:12 UTC
Finalization Time:  2025-02-14 at 16:34:34 UTC, extra=None
"""

import logging
import logging.config
import time

from aioextensions import (
    collect,
    run,
)

from integrates.dataloaders import (
    get_new_context,
)
from integrates.db_model.events.enums import (
    EventType,
)
from integrates.db_model.events.remove import (
    remove,
)
from integrates.db_model.events.types import (
    Event,
    GroupEventsRequest,
)
from integrates.organizations.domain import (
    get_all_groups,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)
LOGGER_CONSOLE = logging.getLogger("console")


async def _process_group_events(group_name: str) -> None:
    loaders = get_new_context()
    events = set(
        await loaders.group_events.load(GroupEventsRequest(group_name=group_name, is_solved=False))
    )
    root_issues: dict[str, list[Event]] = {}

    for event in events:
        if event.type == EventType.CLONING_ISSUES and event.root_id:
            root_issues.setdefault(event.root_id, []).append(event)

    for _, issues in root_issues.items():
        if len(issues) > 1:
            await collect([remove(event_id=issue.id) for issue in issues[1:]])
            LOGGER_CONSOLE.info(
                "Duplicated root events: %s on group %s removed",
                len(issues) - 1,
                group_name,
            )


async def main() -> None:
    loaders = get_new_context()
    groups = await get_all_groups(loaders=loaders)
    await collect([_process_group_events(group_name=group.name) for group in groups])


if __name__ == "__main__":
    execution_time = time.strftime("Execution Time:     %Y-%m-%d at %H:%M:%S %Z")
    run(main())
    finalization_time = time.strftime("Finalization Time:  %Y-%m-%d at %H:%M:%S %Z")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
