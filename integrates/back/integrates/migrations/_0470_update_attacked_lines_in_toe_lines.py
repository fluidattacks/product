"""
Fix attacked lines that are greater than loc in toe lines

Execution Time: 2024-01-18 at 00:02:23 UTC
Finalization Time: 2024-01-18 at 00:23:30 UTC
"""

import asyncio
import logging
import logging.config
import time

from aioextensions import (
    collect,
    run,
)
from aiohttp.client_exceptions import (
    ClientConnectorError,
    ClientPayloadError,
    ServerDisconnectedError,
    ServerTimeoutError,
)
from botocore.exceptions import (
    ClientError,
    ConnectTimeoutError,
    HTTPClientError,
    ReadTimeoutError,
)

from integrates.custom_exceptions import (
    ToeLinesAlreadyUpdated,
)
from integrates.custom_utils import (
    datetime as datetime_utils,
)
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model import (
    toe_lines as toe_lines_model,
)
from integrates.db_model.toe_lines.types import (
    GroupToeLinesRequest,
    ToeLine,
)
from integrates.decorators import (
    retry_on_exceptions,
)
from integrates.organizations import (
    domain as orgs_domain,
)
from integrates.settings import (
    LOGGING,
)
from integrates.toe.lines.constants import (
    CHECKED_FILES,
)

NETWORK_ERRORS = (
    asyncio.TimeoutError,
    ClientConnectorError,
    ClientError,
    ClientPayloadError,
    ConnectionError,
    ConnectionResetError,
    ConnectTimeoutError,
    HTTPClientError,
    ReadTimeoutError,
    ServerDisconnectedError,
    ServerTimeoutError,
)

logging.config.dictConfig(LOGGING)

# Constants
LOGGER = logging.getLogger(__name__)


@retry_on_exceptions(exceptions=NETWORK_ERRORS, max_attempts=3, sleep_seconds=3.0)
async def _update_toe_lines(toe_lines: ToeLine) -> None:
    attacked_lines = toe_lines.state.attacked_lines
    loc = toe_lines.state.loc
    file_extension = toe_lines.filename.split(".")[-1]
    new_value = 0

    if file_extension in CHECKED_FILES:
        new_value = loc

    if attacked_lines > loc:
        try:
            await toe_lines_model.update_state(
                current_value=toe_lines,
                new_state=toe_lines.state._replace(
                    attacked_lines=new_value,
                    modified_date=datetime_utils.get_utc_now(),
                ),
            )
            LOGGER.info(
                "(%s) [%s] attacked lines updated from %s to %s (loc: %s)",
                toe_lines.group_name,
                toe_lines.filename,
                attacked_lines,
                new_value,
                loc,
            )
        except ToeLinesAlreadyUpdated:
            LOGGER.info(
                "(%s) [%s] Exception - toe lines already updated",
                toe_lines.group_name,
                toe_lines.filename,
            )


async def process_group(
    loaders: Dataloaders,
    group_name: str,
    count: int,
    total: int,
) -> None:
    group_toe_lines = await loaders.group_toe_lines.load_nodes(
        GroupToeLinesRequest(group_name=group_name),
    )
    if not group_toe_lines:
        return

    start = time.time()
    await collect(tuple(_update_toe_lines(toe_lines) for toe_lines in group_toe_lines))
    end = time.time()

    LOGGER.info(
        "[%s] %s toe lines processed in %s",
        f"{count / total * 100:.2f} %",
        group_name,
        f"{(end - start) * 1000:.2f} ms",
    )


async def main() -> None:
    loaders: Dataloaders = get_new_context()
    all_group_names = sorted(await orgs_domain.get_all_active_group_names(loaders))
    await collect(
        tuple(
            process_group(
                loaders=loaders,
                group_name=group_name,
                count=count + 1,
                total=len(all_group_names),
            )
            for count, group_name in enumerate(all_group_names)
        ),
    )


if __name__ == "__main__":
    execution_time = time.strftime("Execution Time:    %Y-%m-%d at %H:%M:%S %Z")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S %Z")
    LOGGER.info("\n%s\n%s", execution_time, finalization_time)
