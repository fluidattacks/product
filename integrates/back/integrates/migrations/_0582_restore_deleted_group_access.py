"""
Restore deleted group access information.

Execution Time:    2024-08-23 at 17:44:28 UTC
Finalization Time: 2024-08-23 at 17:47:45 UTC
"""

import logging
import logging.config
import time

from aioextensions import (
    collect,
    run,
)

from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model import (
    TABLE,
)
from integrates.db_model.group_access.types import (
    GroupStakeholdersAccessRequest,
)
from integrates.dynamodb import (
    operations,
)
from integrates.dynamodb.types import (
    PrimaryKey,
)
from integrates.organizations.domain import (
    get_all_active_group_names,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)

LOGGER = logging.getLogger(__name__)
LOGGER_CONSOLE = logging.getLogger("console")
BACKUP_TABLE = TABLE._replace(name="backup_restore")


async def restore_group_access(group_name: str, user_email: str) -> None:
    access_from_backup = await operations.get_item(
        key=PrimaryKey(
            partition_key=f"USER#{user_email}",
            sort_key=f"GROUP#{group_name}",
        ),
        facets=(TABLE.facets["group_access"],),
        table=BACKUP_TABLE,
    )
    if access_from_backup is None:
        return
    await operations.put_item(
        table=TABLE,
        item=access_from_backup,
        facet=TABLE.facets["group_access"],
    )


async def restore_group(loaders: Dataloaders, group_name: str) -> None:
    group_stakeholders_access = await loaders.group_stakeholders_access.load(
        GroupStakeholdersAccessRequest(group_name=group_name),
    )
    await collect(
        [restore_group_access(group_name, access.email) for access in group_stakeholders_access],
        workers=10,
    )
    LOGGER_CONSOLE.info("Restored group %s", group_name)


async def main() -> None:
    loaders: Dataloaders = get_new_context()
    active_group_names = sorted(await get_all_active_group_names(loaders))
    LOGGER.info("Groups to process: %s", len(active_group_names))
    await collect(
        [restore_group(loaders, group) for group in active_group_names],
        workers=5,
    )


if __name__ == "__main__":
    execution_time = time.strftime("Execution Time:    %Y-%m-%d at %H:%M:%S UTC")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S UTC")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
