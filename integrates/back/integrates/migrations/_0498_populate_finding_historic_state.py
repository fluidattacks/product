"""
Populate finding historic state in integrates_vms_historic table.

Start Time: 2024-02-26 at 15:54:29 UTC
Finalization Time: 2024-02-26 at 15:57:01 UTC
"""

import asyncio
import logging
import logging.config
import time

from aioextensions import (
    collect,
    run,
)
from aiohttp.client_exceptions import (
    ClientConnectorError,
    ClientOSError,
    ClientPayloadError,
    ServerDisconnectedError,
    ServerTimeoutError,
)
from boto3.dynamodb.conditions import (
    Key,
)
from botocore.exceptions import (
    ClientError,
    ConnectTimeoutError,
    HTTPClientError,
    ReadTimeoutError,
)
from more_itertools import (
    chunked,
)

from integrates.class_types.types import (
    Item,
)
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model import (
    HISTORIC_TABLE,
    TABLE,
)
from integrates.db_model.utils import (
    get_historic_gsi_2_key,
    get_historic_gsi_3_key,
    get_historic_gsi_sk,
)
from integrates.decorators import (
    retry_on_exceptions,
)
from integrates.dynamodb import (
    keys,
    operations,
)
from integrates.dynamodb.exceptions import (
    UnavailabilityError,
)
from integrates.organizations import (
    domain as orgs_domain,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)
LOGGER_CONSOLE = logging.getLogger("console")
NETWORK_ERRORS = (
    asyncio.TimeoutError,
    ClientConnectorError,
    ClientError,
    ClientOSError,
    ClientPayloadError,
    ConnectionError,
    ConnectionResetError,
    ConnectTimeoutError,
    HTTPClientError,
    ReadTimeoutError,
    ServerDisconnectedError,
    ServerTimeoutError,
    UnavailabilityError,
)


batch_put_item = retry_on_exceptions(
    exceptions=NETWORK_ERRORS,
    sleep_seconds=300,
)(operations.batch_put_item)


@retry_on_exceptions(
    exceptions=NETWORK_ERRORS,
    sleep_seconds=10,
)
async def _get_findings_by_group(
    group_name: str,
) -> tuple[Item, ...]:
    primary_key = keys.build_key(
        facet=TABLE.facets["finding_metadata"],
        values={"group_name": group_name},
    )
    index = TABLE.indexes["inverted_index"]
    key_structure = index.primary_key
    response = await operations.query(
        condition_expression=(
            Key(key_structure.partition_key).eq(primary_key.sort_key)
            & Key(key_structure.sort_key).begins_with(primary_key.partition_key)
        ),
        facets=(TABLE.facets["finding_metadata"],),
        index=index,
        table=TABLE,
    )
    return response.items


@retry_on_exceptions(
    exceptions=NETWORK_ERRORS,
    sleep_seconds=10,
)
async def _get_historic_state(
    finding_id: str,
) -> tuple[Item, ...]:
    primary_key = keys.build_key(
        facet=TABLE.facets["finding_historic_state"],
        values={"id": finding_id},
    )
    key_structure = TABLE.primary_key
    response = await operations.query(
        condition_expression=(
            Key(key_structure.partition_key).eq(primary_key.partition_key)
            & Key(key_structure.sort_key).begins_with(primary_key.sort_key)
        ),
        facets=(TABLE.facets["finding_historic_state"],),
        table=TABLE,
    )
    return response.items


def format_state_item(state_item: Item, finding_item: Item, group_name: str) -> Item:
    item_pk = f"{finding_item['pk']}#{finding_item['sk']}"
    item_sk = get_historic_gsi_sk("state", state_item["modified_date"])
    gsi_2_key = get_historic_gsi_2_key(
        HISTORIC_TABLE.facets["finding_state"],
        group_name,
        "state",
        state_item["modified_date"],
    )
    gsi_3_key = get_historic_gsi_3_key(group_name, "state", state_item["modified_date"])
    gsi_keys = {
        "pk_2": gsi_2_key.partition_key,
        "sk_2": gsi_2_key.sort_key,
        "pk_3": gsi_3_key.partition_key,
        "sk_3": gsi_3_key.sort_key,
    }
    return {
        **(
            finding_item["state"]
            if finding_item.get("state")
            and state_item["modified_date"] == finding_item["state"]["modified_date"]
            else state_item
        ),
        "pk": item_pk,
        "sk": item_sk,
        **gsi_keys,
    }


async def process_finding(finding_item: Item, group_name: str) -> None:
    finding_id = finding_item["pk"].split("#")[1]
    finding_historic_state: tuple[Item, ...] = await _get_historic_state(finding_id=finding_id)
    if not finding_historic_state and finding_item.get("state"):
        finding_historic_state = (finding_item["state"],)

    formatted_items = tuple(
        format_state_item(state_item, finding_item, group_name)
        for state_item in finding_historic_state
    )
    for chunked_formatted_items in chunked(formatted_items, 25):
        await batch_put_item(
            items=tuple(chunked_formatted_items),
            table=HISTORIC_TABLE,
        )


async def process_group(group_name: str) -> None:
    group_findings = await _get_findings_by_group(group_name=group_name)
    await collect(
        tuple(process_finding(finding_item, group_name) for finding_item in group_findings),
        workers=64,
    )
    LOGGER_CONSOLE.info(
        "Group processed",
        extra={
            "extra": {
                "group_name": group_name,
                "group_findings": len(group_findings),
            },
        },
    )


async def main() -> None:
    loaders: Dataloaders = get_new_context()
    all_group_names = sorted(await orgs_domain.get_all_group_names(loaders))
    LOGGER_CONSOLE.info(
        "All group names",
        extra={
            "extra": {
                "total": len(all_group_names),
            },
        },
    )
    for count, group_name in enumerate(all_group_names, start=1):
        LOGGER_CONSOLE.info(
            "Group",
            extra={
                "extra": {
                    "group_name": group_name,
                    "count": count,
                },
            },
        )
        await process_group(group_name)


if __name__ == "__main__":
    execution_time = time.strftime("Start Time:        %Y-%m-%d at %H:%M:%S UTC")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S UTC")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
