"""
Update gsi_3 index for roots table

Execution Time:     2024-12-20 at 21:01:04 UTC
Finalization Time:  2024-12-20 at 21:08:02 UTC
"""

import logging

from aioextensions import collect

from integrates.dataloaders import Dataloaders, get_new_context
from integrates.db_model.organizations.get import get_all_organizations
from integrates.db_model.roots.types import GitRoot
from integrates.db_model.roots.update import update_root_gsi_3
from integrates.migrations.utils import log_time

LOGGER = logging.getLogger("migrations")


@log_time(LOGGER)
async def main() -> None:
    loaders: Dataloaders = get_new_context()
    all_orgs = [org.name for org in await get_all_organizations()]

    count = 0
    total_orgs = len(all_orgs)
    for org in all_orgs:
        roots = await loaders.organization_roots.load(org)
        results = await collect(
            [
                update_root_gsi_3(
                    current_value=root.state,
                    group_name=root.group_name,
                    root_id=root.id,
                    state=root.state,
                )
                for root in roots
                if isinstance(root, GitRoot)
            ],
            workers=8,
        )

        for result in results:
            if isinstance(result, Exception):
                LOGGER.error("Error in organization %s: %s", org, result)

        count += 1
        LOGGER.info("(%s/%s) Roots GSI 3 updated for organization %s", count, total_orgs, org)


if __name__ == "__main__":
    main()  # type: ignore[unused-coroutine]
