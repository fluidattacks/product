# type: ignore
"""
Removes findings in draft state with no vulnerabilities.
Removes vulns in draft state whose method is already approved.

Timestamps taken from the execution. The line with the logger
printing the timestamp was not included by mistake.

Execution Time:    2024-10-22T23:43:43+0000
Finalization Time: 2024-10-23T00:40:51+0000

"""

import csv
import time

from aioextensions import (
    collect,
    run,
)

from integrates.dataloaders import (
    get_new_context,
)
from integrates.db_model.enums import (
    Source,
    StateRemovalJustification,
)
from integrates.db_model.vulnerabilities.enums import (
    VulnerabilityStateReason,
    VulnerabilityStateStatus,
)
from integrates.findings.domain import (
    remove_finding,
)
from integrates.organizations.domain import (
    get_all_active_group_names,
)
from integrates.vulnerabilities.domain import (
    remove_vulnerability,
)


async def delete_machine_drafts_locations(loaders, group: str) -> None:
    findings = await loaders.group_findings.load(group)

    machine_findings = [finding for finding in findings if finding.state.source == Source.MACHINE]

    findings_vulns = await loaders.finding_vulnerabilities.load_many(
        [finding.id for finding in machine_findings],
    )
    rows = []
    for finding, vulns in zip(machine_findings, findings_vulns, strict=False):
        draft_vulns = [
            vuln
            for vuln in vulns
            if (
                (
                    vuln.state.source == Source.MACHINE
                    or vuln.hacker_email == "machine@fluidattacks.com"
                )
                and vuln.skims_method is not None
                and vuln.state.status
                in {
                    VulnerabilityStateStatus.REJECTED,
                    VulnerabilityStateStatus.SUBMITTED,
                }
            )
        ]
        if len(vulns) == 0:
            try:
                await remove_finding(
                    loaders,
                    "lpatino@fluidattacks.com",
                    finding.id,
                    StateRemovalJustification.NO_JUSTIFICATION,
                    Source.MACHINE,
                )
                rows.append(
                    [
                        group,
                        finding.title[:3],
                        finding.id,
                        "FINDING WITH NO VULNS REMOVED",
                        len(draft_vulns),
                    ],
                )
            except Exception:
                rows.append(
                    [
                        group,
                        finding.title[:3],
                        finding.id,
                        "ERROR REMOVING",
                        len(draft_vulns),
                    ],
                )

        elif len(draft_vulns) > 0:
            try:
                await collect(
                    (
                        remove_vulnerability(
                            loaders=loaders,
                            finding_id=vuln.finding_id,
                            vulnerability_id=vuln.id,
                            justification=VulnerabilityStateReason.CONSISTENCY,
                            email="lpatino@fluidattacks.com",
                            include_closed_vuln=True,
                        )
                        for vuln in draft_vulns
                    ),
                    workers=15,
                )
                rows.append(
                    [
                        group,
                        finding.title[:3],
                        finding.id,
                        "REMOVED DRAFT VULNS FROM APPROVED METHODS",
                        len(draft_vulns),
                    ],
                )
            except Exception:
                rows.append(
                    [
                        group,
                        finding.title[:3],
                        finding.id,
                        "ERROR REMOVING VULNS",
                        len(draft_vulns),
                    ],
                )

    with open("removed_drafts.csv", "a+", encoding="utf-8") as handler:
        writer = csv.writer(handler)
        writer.writerows(rows)


async def main() -> None:
    loaders = get_new_context()
    groups = sorted(await get_all_active_group_names(loaders))

    for group in groups:
        await delete_machine_drafts_locations(loaders, group)


if __name__ == "__main__":
    execution_time = time.strftime("Execution Time:    %Y-%m-%d at %H:%M:%S UTC")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S UTC")
