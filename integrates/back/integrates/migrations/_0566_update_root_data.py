"""
Migration to update root data

Execution Time:    2024-07-19 at 18:04:33 UTC
Finalization Time: 2024-07-19 at 18:04:51 UTC

Execution Time:    2024-08-12 at 20:21:09 UTC
Finalization Time: 2024-08-12 at 20:22:33 UTC
"""

import logging
import logging.config
import time
from contextlib import (
    suppress,
)

from aioextensions import (
    collect,
    run,
)

from integrates.custom_exceptions import (
    RootAlreadyCloning,
)
from integrates.dataloaders import (
    get_new_context,
)
from integrates.db_model.credentials.types import (
    CredentialsRequest,
)
from integrates.db_model.roots.enums import (
    RootStatus,
)
from integrates.db_model.roots.types import (
    GitRoot,
)
from integrates.groups.domain import (
    fetch_and_remove_repositories,
)
from integrates.roots.domain import (
    queue_sync_git_roots,
)
from integrates.roots.update import (
    update_git_root,
)
from integrates.roots.validations import (
    working_credentials,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)

# Constants
LOGGER = logging.getLogger(__name__)
LOGGER_CONSOLE = logging.getLogger("console")
EMAIL_INTEGRATES = "integrates@fluidattacks.com"


async def _process_root(old_root: GitRoot, roots: dict[str, str], group_name: str) -> None:
    loaders = get_new_context()
    group = await loaders.group.load(group_name)
    if not group:
        return
    if (
        not any(old_root.state.url == _root for _root in roots.keys())
        or old_root.state.status == RootStatus.INACTIVE
    ):
        return

    new_url = roots[old_root.state.url]
    new_nickname = roots[old_root.state.url].split("/")[-1]
    LOGGER_CONSOLE.info(
        "Root processing started",
        extra={"extra": {"root_id": old_root.id}},
    )

    if not old_root.state.use_vpn and not old_root.state.use_ztna and old_root.state.credential_id:
        organization_credential = await loaders.credentials.load(
            CredentialsRequest(
                id=old_root.state.credential_id,
                organization_id=group.organization_id,
            ),
        )
        LOGGER_CONSOLE.info(
            "Credentials validation",
            extra={
                "extra": {
                    "credential_id": old_root.state.credential_id,
                    "root_id": old_root.id,
                    "branch": old_root.state.branch,
                },
            },
        )
        await working_credentials(
            new_url,
            old_root.state.branch,
            organization_credential,
            loaders,
        )

    new_root = await update_git_root(
        loaders,
        EMAIL_INTEGRATES,
        group,
        force=True,
        id=old_root.id,
        url=new_url,
        credentials={"id": old_root.state.credential_id},
        nickname=new_nickname,
        branch=old_root.state.branch,
        criticality=old_root.state.criticality,
        gitignore=old_root.state.gitignore,
        includes_health_check=old_root.state.includes_health_check,
    )
    with suppress(RootAlreadyCloning):
        await queue_sync_git_roots(
            loaders=loaders,
            roots=(new_root,),
            group_name=new_root.group_name,
            modified_by=EMAIL_INTEGRATES,
            group=group,
            queue_on_batch=True,
            force=True,
        )

    await fetch_and_remove_repositories(loaders, group.organization_id, new_url)

    LOGGER_CONSOLE.info(
        "Root has been updated",
        extra={
            "extra": {
                "before_state": old_root.state,
                "after_state": new_root.state,
            },
        },
    )


async def main() -> None:
    roots: dict[str, str] = {}
    group_name = "group_name"
    loaders = get_new_context()
    await collect(
        [
            _process_root(root, roots, group_name)
            for root in await loaders.group_roots.load(group_name)
            if isinstance(root, GitRoot) and root.state.url in roots
        ],
        workers=1,
    )


if __name__ == "__main__":
    execution_time = time.strftime("Execution Time:    %Y-%m-%d at %H:%M:%S %Z")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S %Z")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
