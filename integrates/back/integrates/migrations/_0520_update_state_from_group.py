"""
Update state from group when database has has_squad or has_machine.

Start Time:    2024-04-03 at 22:23:48 UTC
Finalization Time: 2024-04-03 at 22:27:23 UTC, extra=None

Start Time:    2024-04-12 at 20:13:02 UTC
Finalization Time: 2024-04-12 at 20:14:26 UTC, extra=None

"""

import logging
import logging.config
import time

from aioextensions import (
    collect,
    run,
)

from integrates.custom_utils.groups import get_group
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model import (
    groups as groups_model,
)
from integrates.organizations import (
    domain as orgs_domain,
)
from integrates.organizations import (
    utils as orgs_utils,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)
LOGGER = logging.getLogger("console")


async def process_group(
    loaders: Dataloaders,
    group_name: str,
    progress: float,
) -> None:
    group = await get_group(loaders, group_name)
    organization = await orgs_utils.get_organization(loaders, group.organization_id)
    await groups_model.update_state(
        group_name=group_name,
        organization_id=organization.id,
        state=group.state,
    )
    LOGGER.info(
        "Group processed",
        extra={
            "extra": {
                "group_name": group_name,
                "state": group.state,
                "progress": round(progress, 2),
            },
        },
    )


async def main() -> None:
    loaders: Dataloaders = get_new_context()
    group_name = sorted(await orgs_domain.get_all_group_names(loaders=loaders))
    LOGGER.info(
        "All groups",
        extra={"extra": {"groups_len": len(group_name)}},
    )
    await collect(
        tuple(
            process_group(
                loaders=loaders,
                group_name=group_name,
                progress=count / len(group_name),
            )
            for count, group_name in enumerate(group_name)
        ),
        workers=1,
    )


if __name__ == "__main__":
    execution_time = time.strftime("Start Time:    %Y-%m-%d at %H:%M:%S UTC")
    LOGGER.info("%s", execution_time)
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S UTC")
    LOGGER.info("\n%s\n%s", execution_time, finalization_time)
