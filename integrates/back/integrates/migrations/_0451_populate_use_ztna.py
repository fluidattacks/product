# type: ignore
"""
Populate field use_ztna for repos and environment urls

Start Time: 2023-10-27 at 00:54:06 UTC
Finalization Time: 2023-10-27 at 00:58:18 UTC
"""

import logging
import logging.config
import time

from aioextensions import (
    collect,
    run,
)

from integrates.custom_exceptions import (
    GroupNotFound,
)
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model import (
    TABLE,
)
from integrates.db_model import (
    roots as roots_model,
)
from integrates.db_model.roots.types import (
    GitRoot,
    Root,
)
from integrates.dynamodb import (
    keys,
    operations,
)
from integrates.organizations import (
    domain as orgs_domain,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)
LOGGER_CONSOLE = logging.getLogger("console")


async def process_root_environment_urls(loaders: Dataloaders, root: Root) -> None:
    urls = await loaders.root_environment_urls.load(root.id)
    if urls:
        for url in urls:
            url_key = keys.build_key(
                facet=TABLE.facets["root_environment_url"],
                values={"uuid": root.id, "hash": url.id},
            )

            use_ztna = {"use_ztna": url.use_secure_access}

            await operations.update_item(
                item=use_ztna,
                key=url_key,
                table=TABLE,
            )


async def process_git_root(root: GitRoot) -> None:
    LOGGER_CONSOLE.info("Root: %s", root)
    await roots_model.update_root_state(
        group_name=root.group_name,
        root_id=root.id,
        current_value=root.state,
        state=root.state._replace(
            use_ztna=root.state.use_secure_access,
        ),
    )


async def process_group(loaders: Dataloaders, group_name: str) -> None:
    group = await loaders.group.load(group_name)
    if not group:
        raise GroupNotFound()

    roots = await loaders.group_roots.load(group_name)

    await collect(
        tuple(process_root_environment_urls(loaders, root) for root in roots),
        workers=10,
    )

    await collect(
        tuple(process_git_root(root) for root in roots if isinstance(root, GitRoot)),
        workers=5,
    )


async def main() -> None:
    loaders: Dataloaders = get_new_context()

    all_group_names = sorted(await orgs_domain.get_all_group_names(loaders=loaders))

    await collect(
        tuple(process_group(loaders, group) for group in all_group_names),
        workers=5,
    )


if __name__ == "__main__":
    execution_time = time.strftime("Start Time:    %Y-%m-%d at %H:%M:%S UTC")
    LOGGER_CONSOLE.info("%s", execution_time)
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S UTC")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
