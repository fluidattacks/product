"""
Set the timeout field attempt_duration_seconds on the
table fi_async_processing for all actions

Start Time:        2024-02-20 at 16:33:26 UTC
Finalization Time: 2024-02-20 at 16:33:34 UTC, extra=None
"""

import logging
import logging.config
import time

from aioextensions import (
    collect,
    run,
)

from integrates.batch.constants import (
    DEFAULT_ACTION_TIMEOUT,
)
from integrates.batch.dal import (
    update_action_to_dynamodb,
)
from integrates.batch.enums import (
    Action,
)
from integrates.batch.resources import (
    TABLE,
)
from integrates.batch.types import (
    BatchProcessingToUpdate,
)
from integrates.class_types.types import (
    Item,
)
from integrates.dynamodb import (
    operations as dynamodb_ops,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)
LOGGER_CONSOLE = logging.getLogger("console")


async def process_action(item: Item) -> None:
    action = Action[str(item["action_name"]).upper().replace("-", "_")]
    match action:
        case Action.EXECUTE_MACHINE | Action.GENERATE_ROOT_SBOM:
            timeout = 43200
        case Action.UPDATE_ORGANIZATION_REPOSITORIES:
            timeout = 14400
        case (
            Action.CLONE_ROOTS
            | Action.REFRESH_TOE_LINES
            | Action.DEACTIVATE_ROOT
            | Action.REPORT
            | Action.SEND_EXPORTED_FILE
            | Action.REMOVE_GROUP_RESOURCES
        ):
            timeout = 7200
        case _:
            timeout = DEFAULT_ACTION_TIMEOUT

    await update_action_to_dynamodb(
        action=action,
        action_key=item["pk"],
        attributes=BatchProcessingToUpdate(attempt_duration_seconds=timeout),
    )


async def main() -> None:
    items = await dynamodb_ops.scan(table=TABLE)
    items_to_update = [item for item in items if not item.get("attempt_duration_seconds")]
    LOGGER_CONSOLE.info(
        "Actions in fi_async_processing",
        extra={
            "extra": {
                "all": len(items),
                "to_update": len(items_to_update),
            },
        },
    )

    await collect([process_action(item) for item in items_to_update], workers=4)


if __name__ == "__main__":
    execution_time = time.strftime("Start Time:        %Y-%m-%d at %H:%M:%S UTC")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S UTC")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
