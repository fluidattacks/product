# type: ignore
"""
Refresh events unreliable_indicators when an empty string is in an
attribute that would hold a date. These empty strings are causing an
indexation error in opensearch. The attribute will be removed instead.

Execution Time:    2023-01-13 at 16:46:06 UTC
Finalization Time: 2023-01-13 at 16:54:40 UTC
"""

import logging
import logging.config
import time

from aioextensions import (
    collect,
    run,
)
from boto3.dynamodb.conditions import (
    Key,
)

from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model import (
    TABLE,
)
from integrates.db_model import (
    events as events_model,
)
from integrates.db_model.events.types import (
    EventUnreliableIndicatorsToUpdate,
)
from integrates.db_model.events.utils import (
    format_event,
)
from integrates.db_model.items import EventItem
from integrates.dynamodb import (
    keys,
    operations,
)
from integrates.organizations import (
    domain as orgs_domain,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)
LOGGER_CONSOLE = logging.getLogger("console")


async def get_group_event_items(group_name: str) -> tuple[EventItem, ...]:
    facet = TABLE.facets["event_metadata"]
    primary_key = keys.build_key(
        facet=facet,
        values={"name": group_name},
    )
    index = TABLE.indexes["inverted_index"]
    key_structure = index.primary_key
    condition_expression = Key(key_structure.partition_key).eq(primary_key.sort_key) & Key(
        key_structure.sort_key,
    ).begins_with(primary_key.partition_key)

    response = await operations.DynamoClient[EventItem].query(
        condition_expression=condition_expression,
        facets=(TABLE.facets["event_metadata"],),
        table=TABLE,
        index=index,
    )

    return response.items


async def process_event(event: EventItem) -> None:
    indicators = event["unreliable_indicators"]
    if not indicators:
        return
    if indicators.get("unreliable_solving_date") == "":
        await events_model.update_unreliable_indicators(
            current_value=format_event(event),
            indicators=EventUnreliableIndicatorsToUpdate(
                clean_unreliable_solving_date=True,
            ),
        )
        LOGGER_CONSOLE.info("Event updated %s", f"{event.get('id')=}")


async def process_group(
    group_name: str,
    progress: float,
) -> None:
    group_events = await get_group_event_items(group_name)
    await collect(
        tuple(process_event(event) for event in group_events),
        workers=1,
    )
    LOGGER_CONSOLE.info(
        "Processed %s, %s, progress: %s",
        group_name,
        f"{len(group_events)=}",
        f"{round(progress, 2)}",
    )


async def main() -> None:
    loaders: Dataloaders = get_new_context()
    group_names = sorted(await orgs_domain.get_all_group_names(loaders=loaders))
    LOGGER_CONSOLE.info("%s", f"{len(group_names)=}")
    await collect(
        tuple(
            process_group(
                group_name=group_name,
                progress=count / len(group_names),
            )
            for count, group_name in enumerate(group_names)
        ),
        workers=1,
    )


if __name__ == "__main__":
    execution_time = time.strftime("Execution Time:    %Y-%m-%d at %H:%M:%S %Z")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S %Z")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
