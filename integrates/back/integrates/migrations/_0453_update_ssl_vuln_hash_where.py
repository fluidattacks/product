"""
Migration to update all ssl open machine vulnerabilities
- Update the where to the new format that will be reported by skims
- Update the specific to the updated values from skims translations
- Update the hash based on these changes

Start Time: 2023-11-03 at 22:40:28 UTC
Finalization Time: 2023-11-03 at 22:59:28 UTC

"""

import csv
import hashlib
import logging
import logging.config
import re
import time
from datetime import (
    datetime,
)
from enum import (
    Enum,
)

import yaml
from aioextensions import (
    collect,
    run,
)
from urllib3.util.url import (
    parse_url,
)

from integrates.custom_utils.vulnerabilities import (
    get_path_from_integrates_vulnerability,
)
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model.enums import (
    Source,
)
from integrates.db_model.findings.types import (
    Finding,
)
from integrates.db_model.vulnerabilities.enums import (
    VulnerabilityStateReason,
    VulnerabilityStateStatus,
    VulnerabilityType,
)
from integrates.db_model.vulnerabilities.types import (
    Vulnerability,
    VulnerabilityMetadataToUpdate,
    VulnerabilityState,
)
from integrates.db_model.vulnerabilities.update import (
    update_historic_entry,
    update_metadata,
)
from integrates.organizations.domain import (
    get_all_active_groups,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)
LOGGER_CONSOLE = logging.getLogger("console")

WEAK_CIPHERS_EN = (
    r"[sS]erver accepts connections with weak cipher method (?P<cipher>[\w]*)"
    r"(, included in )?(?P<protocol>[\w\.]*)?"
)
WEAK_CIPHERS_ES = (
    r"[eE]l servidor acepta conexiones con el método de cifrado débil "
    r"(?P<cipher>[\w]*)(, included in )?(?P<protocol>[\w\.]*)?"
)
CBC_ENABLED_EN = (
    r"[sS]erver accepts connections with weak cipher method: "
    r"(?P<cipher>[\w]*)(, that supports CBC in )?(?P<protocol>[\w\.]*)?"
)
CBC_ENABLED_ES = (
    r"[eE]l servidor acepta conexiones con el método de encripción insegura: "
    r"(?P<cipher>[\w]*)(, que soporta CBC en )?(?P<protocol>[\w\.]*)?"
)

PREVIOUS_TRANSLATIONS = {
    "analyze_protocol.weak_ciphers_allowed": {
        "EN": WEAK_CIPHERS_EN,
        "ES": WEAK_CIPHERS_ES,
    },
    "analyze_protocol.cbc_enabled": {
        "EN": CBC_ENABLED_EN,
        "ES": CBC_ENABLED_ES,
    },
}


class LocalesEnum(Enum):
    EN: str = "EN"
    ES: str = "ES"


def get_translations() -> dict:
    translations = {}
    path = "universe/skims/static/translations/" "criteria/vulnerabilities/SSL_DESCRIPTIONS.yaml"
    with open(path, encoding="utf-8") as handle:
        for key, data in yaml.safe_load(handle).items():
            translations[key] = {
                locale_code: data[locale_code.lower()]
                for locale in LocalesEnum
                for locale_code in [locale.value]
            }
    return translations


TRANSLATIONS: dict[str, dict[str, str]] = get_translations()


async def get_ssl_vulns(
    loaders: Dataloaders,
    findings: list[Finding],
    fin_code: str,
) -> list[Vulnerability]:
    vulns = await loaders.finding_vulnerabilities.load_many_chained(
        [fin.id for fin in findings if fin.title.startswith(fin_code)],
    )
    return [
        vuln
        for vuln in vulns
        if vuln.state.status == VulnerabilityStateStatus.VULNERABLE
        and (vuln.state.source == Source.MACHINE or vuln.hacker_email == "machine@fluidattacks.com")
        and vuln.skims_method is not None
        and vuln.type == VulnerabilityType.INPUTS
        and "analyze_protocol" in vuln.skims_method
    ]


async def process_vuln(
    vuln: Vulnerability,
    fin_code: str,
    language: str,
) -> tuple[str, str, int, VulnerabilityState, str]:
    namespace, where = get_path_from_integrates_vulnerability(
        vuln.state.where,
        VulnerabilityType.INPUTS,
    )
    parsed_url = parse_url(where)
    port = parsed_url.port or 443
    new_where = f"https://{parsed_url.host}:{port}"
    method = str(vuln.skims_method)
    new_specific = vuln.state.specific
    if method in PREVIOUS_TRANSLATIONS and (
        match := re.match(
            PREVIOUS_TRANSLATIONS[method][language],
            vuln.state.specific,
        )
    ):
        cipher_suite = match.group("cipher")
        protocol = match.group("protocol") or "TLSv1.2"

        new_specific = TRANSLATIONS[f"lib_ssl.{method}"][language].format(
            v_name=protocol,
            insecure_cipher=cipher_suite,
        )

    new_hash = int.from_bytes(
        hashlib.sha256(
            bytes(
                (new_where + new_specific + fin_code + method),
                encoding="utf-8",
            ),
        ).digest()[:8],
        "little",
    )

    new_state = vuln.state._replace(
        where=f"{new_where} ({namespace})",
        specific=new_specific,
        modified_date=datetime.utcnow(),
        reasons=[VulnerabilityStateReason.CONSISTENCY],
        modified_by="flagos@fluidattacks.com",
    )

    return new_where, new_specific, new_hash, new_state, namespace


async def process_vulns(
    ssl_vulns: list[Vulnerability],
    fin_code: str,
    language: str,
) -> tuple[list, list]:
    futures = []
    rows = []
    for vuln in ssl_vulns:
        (
            new_where,
            new_specific,
            new_hash,
            new_state,
            namespace,
        ) = await process_vuln(vuln, fin_code, language)

        futures.append(
            update_historic_entry(
                current_value=vuln,
                finding_id=vuln.finding_id,
                entry=new_state,
                vulnerability_id=vuln.id,
            ),
        )

        futures.append(
            update_metadata(
                finding_id=vuln.finding_id,
                metadata=VulnerabilityMetadataToUpdate(hash=new_hash),
                root_id=vuln.root_id,
                vulnerability_id=vuln.id,
            ),
        )

        rows.extend(
            [
                [
                    vuln.group_name,
                    fin_code,
                    vuln.skims_method,
                    f"{new_where} ({namespace})",
                    new_specific.strip(),
                    vuln.id,
                ],
            ],
        )
    return futures, rows


async def adjust_group_ssl_reports(
    loaders: Dataloaders,
    group: str,
    language: str,
    searched_fins: tuple[str, ...],
) -> None:
    LOGGER_CONSOLE.info("Processing %s", group)
    findings = await loaders.group_findings.load(group)
    all_futures = []
    all_rows = []
    for fin_code in searched_fins:
        ssl_vulns = await get_ssl_vulns(loaders, findings, fin_code)
        futures, rows = await process_vulns(ssl_vulns, fin_code, language)
        all_futures.extend(futures)
        all_rows.extend(rows)

    await collect(all_futures, workers=20)

    with open("ssl_vulns_migration.csv", "a+", encoding="utf-8") as handler:
        writer = csv.writer(handler)
        writer.writerows(all_rows)


async def main() -> None:
    searched_fins = ("016", "052", "094", "133")
    loaders = get_new_context()
    active_groups = await get_all_active_groups(loaders)
    groups = sorted([(group.name, group.language.value) for group in active_groups])

    for group, language in groups:
        await adjust_group_ssl_reports(
            loaders,
            group,
            language,
            searched_fins,
        )


if __name__ == "__main__":
    execution_time = time.strftime("Execution Time:    %Y-%m-%d at %H:%M:%S UTC")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S UTC")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
