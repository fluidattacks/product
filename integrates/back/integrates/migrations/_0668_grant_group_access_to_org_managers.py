"""
Grant access to groups to organization managers as manage groups.
If the organization manager already has access to the group with a role different
from group_manager, the access will be updated.

Execution Time: 2025-02-24 at 18:04:29 UTC
Finalization Time: 2025-02-24 at 18:05:12 UTC
"""

import logging.config
import time

from aioextensions import (
    collect,
    run,
)

from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model.organization_access.types import (
    OrganizationAccess,
)
from integrates.db_model.stakeholders.get import (
    get_all_stakeholders,
)
from integrates.db_model.stakeholders.types import (
    Stakeholder,
)
from integrates.group_access.domain import (
    add_access,
)
from integrates.settings import LOGGING

logging.config.dictConfig(LOGGING)
LOGGER = logging.getLogger("migrations")

EMAIL = "integrates@fluidattacks.com"


async def _grant_groups_access(loaders: Dataloaders, org_access: OrganizationAccess) -> None:
    org_groups = await loaders.organization_groups.load(org_access.organization_id)
    stakeholder_groups_access = await loaders.stakeholder_groups_access.load(org_access.email)
    groups_with_granted_access = {access.group_name: access for access in stakeholder_groups_access}

    await collect(
        [
            add_access(
                loaders,
                org_access.email,
                group.name,
                role="group_manager",
                modified_by=EMAIL,
            )
            for group in org_groups
            if (
                group.name not in groups_with_granted_access
                or groups_with_granted_access[group.name].state.role != "group_manager"
            )
            and group.organization_id == org_access.organization_id
        ],
        workers=32,
    )

    LOGGER.info(
        "Granted group_manager access to %s in %s",
        org_access.email,
        org_access.organization_id,
    )


async def process_stakeholder(stakeholder: Stakeholder) -> None:
    loaders = get_new_context()
    stakeholder_orgs_access = await loaders.stakeholder_organizations_access.load(stakeholder.email)
    organization_managers_accesses = (
        access for access in stakeholder_orgs_access if access.state.role == "organization_manager"
    )

    await collect(
        [
            _grant_groups_access(loaders, org_access)
            for org_access in organization_managers_accesses
        ],
    )


async def main() -> None:
    all_stakeholders = await get_all_stakeholders()
    await collect(
        (process_stakeholder(stakeholder) for stakeholder in all_stakeholders),
        workers=32,
    )


if __name__ == "__main__":
    execution_time = time.strftime("Execution Time:    %Y-%m-%d at %H:%M:%S %Z")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S %Z")
    print("\n%s\n%s", execution_time, finalization_time)
