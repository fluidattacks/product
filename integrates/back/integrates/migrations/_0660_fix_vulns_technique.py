"""
Populate "technique" field for all vulns that do not have it.
Delete "skims_technique" field for all vulns.
"""

import csv
import logging
import logging.config
import time

from aioextensions import (
    collect,
    run,
)
from boto3.dynamodb.conditions import (
    Attr,
)

from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model import (
    TABLE,
)
from integrates.db_model.enums import (
    Source,
)
from integrates.db_model.vulnerabilities.enums import (
    VulnerabilityTechnique,
    VulnerabilityType,
)
from integrates.db_model.vulnerabilities.types import (
    Vulnerability,
)
from integrates.dynamodb import (
    keys,
    operations,
)
from integrates.dynamodb.exceptions import (
    ConditionalCheckFailedException,
)
from integrates.organizations.domain import (
    get_all_group_names,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)
LOGGER_CONSOLE = logging.getLogger("console")


def get_technique(vuln: Vulnerability) -> VulnerabilityTechnique:
    if not (vuln.hacker_email == "machine@fluidattacks.com" or vuln.state.source == Source.MACHINE):
        return VulnerabilityTechnique.PTAAS

    if vuln.type == VulnerabilityType.LINES:
        new_technique = VulnerabilityTechnique.SAST
        if vuln.state.advisories:
            new_technique = VulnerabilityTechnique.SCA
    else:
        new_technique = VulnerabilityTechnique.DAST
        if vuln.skims_method and vuln.skims_method.startswith(("aws", "azure", "gcp")):
            new_technique = VulnerabilityTechnique.CSPM

    return new_technique


def get_metadata_to_update(vuln: Vulnerability) -> dict[str, str | None]:
    if vuln.technique:
        return {"skims_technique": None}
    new_technique = get_technique(vuln)
    return {"technique": new_technique.value, "skims_technique": None}


async def update_attributes(
    finding_id: str, vulnerability_id: str, new_item: dict[str, str | None]
) -> None:
    key_structure = TABLE.primary_key
    vulnerability_key = keys.build_key(
        facet=TABLE.facets["vulnerability_metadata"],
        values={"finding_id": finding_id, "id": vulnerability_id},
    )
    try:
        await operations.update_item(
            condition_expression=Attr(key_structure.partition_key).exists(),
            item=new_item,
            key=vulnerability_key,
            table=TABLE,
        )
    except ConditionalCheckFailedException:
        LOGGER_CONSOLE.info("Unable to update vuln %s", vulnerability_id)


async def process_group(
    loaders: Dataloaders,
    group_name: str,
) -> None:
    csv_vulns = []
    group_findings = await loaders.group_findings_all.load(group_name)

    vulns = await loaders.finding_vulnerabilities_all.load_many_chained(
        [fin.id for fin in group_findings],
    )

    await collect(
        (
            update_attributes(
                finding_id=vuln.finding_id,
                vulnerability_id=vuln.id,
                new_item=get_metadata_to_update(vuln),
            )
            for vuln in vulns
        ),
        workers=16,
    )

    csv_vulns.extend(
        [
            [
                vuln.group_name,
                vuln.finding_id,
                vuln.id,
                vuln.state.status.value,
            ]
            for vuln in vulns
            if not vuln.technique and vuln.state.status.value != "DELETED"
        ],
    )

    with open("current_vulns_modified.csv", "a+", encoding="utf-8") as handler:
        writer = csv.writer(handler)
        writer.writerows(csv_vulns)

    LOGGER_CONSOLE.info("Non deleted vulns processed  %s", len(csv_vulns))
    LOGGER_CONSOLE.info("Group processed  %s", group_name)


async def main() -> None:
    loaders: Dataloaders = get_new_context()
    groups = sorted(await get_all_group_names(loaders))
    for group in groups:
        await process_group(loaders, group)


if __name__ == "__main__":
    execution_time = time.strftime("Execution Time:    %Y-%m-%d at %H:%M:%S UTC")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S UTC")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
