"""
Remove inconsistencies on organization access items

Execution Time:    2024-11-28 at 15:34:18 UTC
Finalization Time: 2024-11-28 at 15:34:19 UTC

"""

import logging
import time

from aioextensions import (
    run,
)

from integrates.class_types.types import Item
from integrates.db_model import TABLE
from integrates.db_model.organizations.get import _get_organization_by_name
from integrates.dynamodb import operations
from integrates.dynamodb.types import PrimaryKey
from integrates.migrations._0636_find_inconsistent_org_access import _get_org_inconsistent_items

LOGGER = logging.getLogger("console")

ORGS_WITH_INCONSISTENCIES: list[str] = []


async def _delete_org_inconsistent_items(items: tuple[Item, ...]) -> None:
    keys_to_delete = tuple(
        PrimaryKey(partition_key=item["pk"], sort_key=item["sk"]) for item in items
    )
    await operations.batch_delete_item(keys=keys_to_delete, table=TABLE)


async def main() -> None:
    for org_name in ORGS_WITH_INCONSISTENCIES:
        org = await _get_organization_by_name(organization_name=org_name)
        if not org:
            continue
        items_to_delete = await _get_org_inconsistent_items(org.id)
        await _delete_org_inconsistent_items(items_to_delete)
        LOGGER.info("Deleted %d inconsistent items for org: %s", len(items_to_delete), org_name)


if __name__ == "__main__":
    execution_time = time.strftime("Execution Time:    %Y-%m-%d at %H:%M:%S UTC")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S UTC")
    LOGGER.info("\n%s\n%s", execution_time, finalization_time)
