"""
Populate id field of root environment url.

Start Time: 2024-03-20 at 17:40:00 UTC
Finalization Time: 2024-03-20 at 17:50:00 UTC
"""

import logging
import logging.config
import time

from aioextensions import (
    collect,
    run,
)
from boto3.dynamodb.conditions import (
    Attr,
    Key,
)

from integrates.class_types.types import (
    Item,
)
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model import (
    TABLE,
)
from integrates.db_model.roots.types import (
    Root,
)
from integrates.dynamodb import (
    keys,
    operations,
)
from integrates.organizations import (
    domain as orgs_domain,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)
LOGGER_CONSOLE = logging.getLogger("console")


async def process_root_environment_url(group_name: str, root_id: str, url_item: Item) -> None:
    url_id = url_item["sk"].split("URL#")[-1]
    if not url_id:
        raise Exception(
            "Invalid url id",
        )
    key_structure = TABLE.primary_key
    item = {
        "root_id": root_id,
        "group_name": group_name,
        "id": url_id,
    }
    condition_expression = Attr(key_structure.partition_key).exists()
    primary_key = keys.build_key(
        facet=TABLE.facets["root_environment_url"],
        values={"uuid": root_id, "group_name": group_name, "hash": url_id},
    )
    await operations.update_item(
        condition_expression=condition_expression,
        item=item,
        key=primary_key,
        table=TABLE,
    )


async def _get_root_environment_urls(*, root_id: str, group_name: str) -> tuple[Item, ...]:
    primary_key = keys.build_key(
        facet=TABLE.facets["root_environment_url"],
        values={
            "uuid": root_id,
            "group_name": group_name,
        },
    )
    key_structure = TABLE.primary_key
    response = await operations.query(
        condition_expression=(
            Key(key_structure.partition_key).eq(primary_key.partition_key)
            & Key(key_structure.sort_key).begins_with(primary_key.sort_key)
        ),
        facets=(TABLE.facets["root_environment_url"],),
        table=TABLE,
    )
    return response.items


async def process_root_environment_urls(group_name: str, root: Root) -> None:
    urls = await _get_root_environment_urls(root_id=root.id, group_name=group_name)
    LOGGER_CONSOLE.info(
        "Root environment to process",
        extra={
            "extra": {
                "root_id": root.id,
                "items": len(urls),
            },
        },
    )
    await collect(
        tuple(process_root_environment_url(group_name, root.id, url_item) for url_item in urls),
    )


async def process_group(loaders: Dataloaders, group_name: str, count: int) -> None:
    roots = await loaders.group_roots.load(group_name)
    LOGGER_CONSOLE.info(
        "Group",
        extra={"extra": {"name": group_name, "roots": len(roots), "count": count}},
    )
    await collect(
        tuple(process_root_environment_urls(group_name, root) for root in roots),
    )


async def main() -> None:
    loaders = get_new_context()
    all_group_names = sorted(await orgs_domain.get_all_group_names(loaders))
    LOGGER_CONSOLE.info(
        "All group names",
        extra={
            "extra": {
                "total": len(all_group_names),
            },
        },
    )
    for count, group_name in enumerate(all_group_names, start=1):
        await process_group(loaders, group_name, count)


if __name__ == "__main__":
    execution_time = time.strftime("Start Time:    %Y-%m-%d at %H:%M:%S UTC")
    LOGGER_CONSOLE.info("%s", execution_time)
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S UTC")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
