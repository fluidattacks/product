# type: ignore
"""
Populate last historic cloning entry with the las known successful cloning
date

Start Time:         2023-09-12 at 20:31:25 UTC
Finalization Time:  2023-09-12 at 20:36:06 UTC
"""

import logging
import logging.config
import time
from contextlib import (
    suppress,
)
from datetime import (
    datetime,
)

from aioextensions import (
    collect,
    run,
)
from boto3.dynamodb.conditions import (
    Attr,
)

from integrates.custom_exceptions import (
    GroupNotFound,
)
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model import (
    TABLE,
)
from integrates.db_model.roots.types import (
    GitRoot,
    GitRootCloning,
)
from integrates.db_model.utils import (
    get_as_utc_iso_format,
)
from integrates.dynamodb import (
    keys,
    operations,
)
from integrates.dynamodb.exceptions import (
    ConditionalCheckFailedException,
)
from integrates.organizations import (
    domain as orgs_domain,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)
LOGGER_CONSOLE = logging.getLogger("console")


async def update_git_root_cloning(
    *,
    last_successful_cloning: datetime | None,
    current_value: GitRootCloning,
    group_name: str,
    root_id: str,
) -> None:
    key_structure = TABLE.primary_key
    primary_key = keys.build_key(
        facet=TABLE.facets["git_root_metadata"],
        values={"name": group_name, "uuid": root_id},
    )
    await operations.update_item(
        condition_expression=(
            Attr(key_structure.partition_key).exists()
            & Attr("cloning.modified_date").eq(get_as_utc_iso_format(current_value.modified_date))
        ),
        item={
            "cloning.last_successful_cloning": get_as_utc_iso_format(last_successful_cloning)
            if last_successful_cloning
            else None,
        },
        key=primary_key,
        table=TABLE,
    )
    historic_key = keys.build_key(
        facet=TABLE.facets["git_root_historic_cloning"],
        values={
            "uuid": root_id,
            "iso8601utc": get_as_utc_iso_format(current_value.modified_date),
        },
    )
    with suppress(ConditionalCheckFailedException):
        await operations.update_item(
            condition_expression=(
                Attr(key_structure.partition_key).exists()
                & Attr(key_structure.sort_key).eq(historic_key.sort_key)
            ),
            item={
                "last_successful_cloning": get_as_utc_iso_format(last_successful_cloning)
                if last_successful_cloning
                else None,
            },
            key=historic_key,
            table=TABLE,
        )


async def get_root_last_successful_cloning(loaders: Dataloaders, root: GitRoot) -> datetime | None:
    if root.cloning.status == "OK":
        return root.cloning.modified_date

    historic_cloning = await loaders.root_historic_cloning.load(root.id)
    if not historic_cloning:
        return None

    cloning_ok = next(
        (cloning for cloning in reversed(historic_cloning) if cloning.status == "OK"),
        None,
    )

    return cloning_ok.modified_date if cloning_ok else None


async def process_root(loaders: Dataloaders, root: GitRoot) -> None:
    last_successful_cloning = await get_root_last_successful_cloning(loaders, root)
    LOGGER_CONSOLE.info(
        "Updated: %s \t%s %s \t%s",
        root.group_name,
        root.id,
        root.state.nickname,
        str(last_successful_cloning),
    )
    await update_git_root_cloning(
        last_successful_cloning=last_successful_cloning,
        current_value=root.cloning,
        group_name=root.group_name,
        root_id=root.id,
    )


async def process_group(loaders: Dataloaders, group_name: str) -> None:
    group = await loaders.group.load(group_name)
    if not group:
        raise GroupNotFound()

    roots = tuple(
        root for root in await loaders.group_roots.load(group_name) if isinstance(root, GitRoot)
    )
    await collect(
        tuple(process_root(loaders, root) for root in roots),
        workers=5,
    )


async def main() -> None:
    loaders: Dataloaders = get_new_context()
    all_group_names = sorted(await orgs_domain.get_all_group_names(loaders=loaders))
    await collect(
        tuple(process_group(loaders, group) for group in all_group_names),
        workers=5,
    )


if __name__ == "__main__":
    execution_time = time.strftime("Start Time:    %Y-%m-%d at %H:%M:%S UTC")
    LOGGER_CONSOLE.info("%s", execution_time)
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S UTC")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
