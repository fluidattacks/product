"""
Queue roots with related events of the type CLONING_ISSUES,
opened during an error with the SSH repositories.

Execution Time:    2024-07-17 at 17:43:03
Finalization Time: 2024-07-17 at 17:44:34, extra=None
"""

import logging
import time
from datetime import (
    datetime,
)

from aioextensions import (
    collect,
    run,
)

from integrates.custom_utils.roots import (
    get_active_git_roots,
)
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model.events.enums import (
    EventType,
)
from integrates.db_model.events.types import (
    GroupEventsRequest,
)
from integrates.organizations import (
    domain as orgs_domain,
)
from integrates.roots import (
    domain as roots_domain,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)

LOGGER = logging.getLogger(__name__)
LOGGER_CONSOLE = logging.getLogger("console")
EMAIL_INTEGRATES = "integrates@fluidattacks.com"


async def process_group(loaders: Dataloaders, group_name: str) -> None:
    group_events = await loaders.group_events.load(
        GroupEventsRequest(group_name=group_name, is_solved=False),
    )
    cloning_issues_events = [
        event
        for event in group_events
        if event.type == EventType.CLONING_ISSUES
        and event.created_date > datetime.fromisoformat("2024-07-16T15:00:00-05:00")
        and event.created_date < datetime.fromisoformat("2024-07-16T20:00:00-05:00")
    ]
    if not cloning_issues_events:
        return

    LOGGER.info(
        "Events found on: %s, qty: %s, items: %s",
        group_name,
        len(cloning_issues_events),
        [event.id for event in cloning_issues_events],
    )

    cloning_issues_root_ids = {event.root_id for event in cloning_issues_events}
    group_roots_to_sync = [
        root
        for root in await get_active_git_roots(loaders, group_name)
        if root.id in cloning_issues_root_ids
    ]
    if not group_roots_to_sync:
        return

    await roots_domain.queue_sync_git_roots(
        loaders=get_new_context(),
        roots=group_roots_to_sync,
        group_name=group_name,
        force=True,
        modified_by=EMAIL_INTEGRATES,
    )
    LOGGER.info(
        "Group processed: %s, qty: %s, root ids: %s",
        group_name,
        len(group_roots_to_sync),
        [root.id for root in group_roots_to_sync],
    )


async def main() -> None:
    loaders = get_new_context()
    group_names = sorted(await orgs_domain.get_all_active_group_names(loaders))
    LOGGER.info(
        "Groups to process: %s, items: %s",
        group_names,
        len(group_names),
    )

    await collect(
        [process_group(loaders, group_name) for group_name in group_names],
        workers=4,
    )


if __name__ == "__main__":
    execution_time = time.strftime("Execution Time:    %Y-%m-%d at %H:%M:%S")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
