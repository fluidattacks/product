"""
Remove groups.

Start Time:    2024-05-27 at 21:28:54 UTC
Finalization Time: 2024-05-27 at 21:29:07 UTC

Start Time:    2024-05-27 at 23:09:49 UTC
Finalization Time: 2024-05-27 at 23:09:53 UTC

Start Time:    2024-07-22 at 20:32:16 UTC
Finalization Time: 2024-07-22 at 20:48:09 UTC

Start Time:    2024-10-03 at 01:07:51 UTC
Finalization Time: 2024-10-03 at 12:55:43 UTC

Start Time: 2024-11-29 at 22:35:42 UTC
Finalization Time: 2024-11-29 at 22:35:58 UTC

"""

import asyncio
import logging
import logging.config
import time

from aioextensions import (
    collect,
    run,
)

from integrates.context import (
    FI_AWS_S3_CONTINUOUS_REPOSITORIES,
)
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model.groups.types import (
    Group,
)
from integrates.db_model.portfolios.remove import (
    remove as remove_portfolio,
)
from integrates.db_model.portfolios.types import (
    Portfolio,
)
from integrates.db_model.portfolios.update import (
    update as update_portfolio,
)
from integrates.groups import (
    domain as groups_domain,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)
LOGGER_CONSOLE = logging.getLogger("console")


async def _remove_associated_portfolios(
    *,
    group_name: str,
    group_org_id: str,
    loaders: Dataloaders,
) -> None:
    group_org = await loaders.organization.load(group_org_id)
    if group_org:
        org_portfolios = await loaders.organization_portfolios.load(group_org.name)
        portfolios_to_update: list[Portfolio] = []
        portfolios_to_delete: list[Portfolio] = []
        for portfolio in org_portfolios:
            if group_name in portfolio.groups:
                if len(portfolio.groups) == 1:
                    portfolios_to_delete.append(portfolio)
                else:
                    portfolios_to_update.append(
                        Portfolio(
                            id=portfolio.id,
                            groups={x for x in portfolio.groups if x != group_name},
                            organization_name=portfolio.organization_name,
                            unreliable_indicators=(portfolio.unreliable_indicators),
                        ),
                    )
        await collect(
            (update_portfolio(portfolio=portfolio) for portfolio in portfolios_to_update),
            workers=2,
        )
        await collect(
            (
                remove_portfolio(
                    organization_name=portfolio.organization_name,
                    portfolio_id=portfolio.id,
                )
                for portfolio in portfolios_to_delete
            ),
            workers=2,
        )


async def _remove_group_resources(
    *,
    group: Group,
    loaders: Dataloaders,
) -> None:
    email = "integrates@fluidattacks.com"
    group_name = group.name

    await groups_domain.remove_resources(
        loaders=loaders,
        email=email,
        group_name=group_name,
        validate_pending_actions=True,
        subscription=group.state.type,
        has_advanced=False,
        should_notify=False,
    )

    # Delete related cloned repos
    group_roots = await loaders.group_roots.load(group_name)

    if group_roots:
        LOGGER_CONSOLE.info(
            "Roots to be processed",
            extra={
                "extra": {
                    "group_name": group.name,
                    "roots": [(root.id, root.state.nickname) for root in group_roots],
                },
            },
        )

        bucket_path: str = FI_AWS_S3_CONTINUOUS_REPOSITORIES

        await collect(
            [
                asyncio.create_subprocess_exec(
                    "aws",
                    "s3",
                    "rm",
                    f"s3://{bucket_path}/{group_name}/{root.state.nickname}*",
                )
                for root in group_roots
            ],
            workers=8,
        )

    await _remove_associated_portfolios(
        group_name=group_name,
        group_org_id=group.organization_id,
        loaders=loaders,
    )

    LOGGER_CONSOLE.info(
        "Remove group resources and repos completed",
        extra={
            "extra": {
                "group_name": group.name,
            },
        },
    )


async def _remove_group(*, name: str) -> None:
    loaders = get_new_context()
    group = await loaders.group.load(name)
    if not group:
        LOGGER_CONSOLE.info(
            "Group not found",
            extra={
                "extra": {
                    "group_name": name,
                },
            },
        )
        return
    await _remove_group_resources(group=group, loaders=loaders)


async def main() -> None:
    groups_to_delete: list[str] = ["testcode", "otherprojects"]
    await collect(
        [_remove_group(name=group_name) for group_name in groups_to_delete],
        workers=2,
    )


if __name__ == "__main__":
    execution_time = time.strftime("Start Time:    %Y-%m-%d at %H:%M:%S UTC")
    LOGGER_CONSOLE.info("%s", execution_time)
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S UTC")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
