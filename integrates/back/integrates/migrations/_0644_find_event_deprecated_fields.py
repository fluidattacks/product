"""
Purge unused event_metadata fields

Execution Time:    2024-12-11 at 19:53:12 UTC
Finalization Time: 2024-12-11 at 19:56:05 UTC
"""

import logging
import time

from aioextensions import (
    run,
)
from boto3.dynamodb.conditions import (
    Key,
)

from integrates.dataloaders import get_new_context
from integrates.db_model import TABLE
from integrates.dynamodb import keys, operations
from integrates.organizations.domain import get_all_groups
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)

LOGGER = logging.getLogger("console")
DEPRECATED_FIELDS = ["accessibility", "affected_components"]


async def find_event_fields_for_group(group_name: str) -> None:
    facet = TABLE.facets["event_metadata"]
    primary_key = keys.build_key(
        facet=facet,
        values={"name": group_name},
    )
    index = TABLE.indexes["inverted_index"]
    key_structure = index.primary_key
    response = await operations.query(
        condition_expression=Key(key_structure.partition_key).eq(
            primary_key.sort_key,
        )
        & Key(key_structure.sort_key).begins_with(primary_key.partition_key),
        facets=(TABLE.facets["event_metadata"],),
        table=TABLE,
        index=index,
    )
    for item in response.items:
        if any(field in item for field in DEPRECATED_FIELDS):
            LOGGER.info("Group %s has deprecated fields in event_metadata", group_name)


async def main() -> None:
    loaders = get_new_context()
    groups = await get_all_groups(loaders)
    for group in groups:
        await find_event_fields_for_group(group.name)


if __name__ == "__main__":
    execution_time = time.strftime("Execution Time:    %Y-%m-%d at %H:%M:%S UTC")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S UTC")
    LOGGER.info("\n%s\n%s", execution_time, finalization_time)
