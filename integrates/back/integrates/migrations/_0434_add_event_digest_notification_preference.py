"""
Add notification preference for event digests.
https://gitlab.com/fluidattacks/universe/-/issues/6333

Execution Time: 2023-09-08 at 14:06:31 UTC
Finalization Time: 2023-09-08 at 14:06:47 UTC
"""

import logging
import logging.config
import time

from aioextensions import (
    collect,
    run,
)

from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model import (
    stakeholders as stakeholders_model,
)
from integrates.db_model.stakeholders.types import (
    Stakeholder,
)
from integrates.settings import (
    LOGGING,
)
from integrates.stakeholders.domain import (
    update_notification_preferences,
)

logging.config.dictConfig(LOGGING)

LOGGER = logging.getLogger(__name__)
LOGGER_CONSOLE = logging.getLogger("console")


async def process_user(loaders: Dataloaders, user: Stakeholder, progress: float) -> None:
    preference_to_add = "EVENT_DIGEST"
    if preference_to_add not in user.state.notifications_preferences.email:
        user.state.notifications_preferences.email.append(preference_to_add)
        await update_notification_preferences(
            loaders=loaders,
            email=user.email,
            preferences=user.state.notifications_preferences,
        )
    LOGGER_CONSOLE.info(
        "User processed",
        extra={
            "extra": {
                "user email": user.email,
                "progress": round(progress, 2),
            },
        },
    )


async def main() -> None:
    loaders: Dataloaders = get_new_context()
    all_stakeholders = await stakeholders_model.get_all_stakeholders()
    LOGGER_CONSOLE.info(
        "Active users",
        extra={"extra": {"users_len": len(all_stakeholders)}},
    )
    await collect(
        tuple(
            process_user(
                loaders=loaders,
                user=stakeholder,
                progress=count / len(all_stakeholders),
            )
            for count, stakeholder in enumerate(all_stakeholders)
        ),
        workers=16,
    )


if __name__ == "__main__":
    execution_time = time.strftime("Execution Time:    %Y-%m-%d at %H:%M:%S UTC")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S UTC")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
