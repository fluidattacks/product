"""
Remove noisy historic entries from vulnerabilities which were incorrectly inserted

Execution Time:    2024-11-27 at 00:56:04 UTC
Finalization Time: 2024-11-27 at 00:56:18 UTC

"""

import logging
import time

from aioextensions import (
    run,
)

from integrates.context import (
    FI_ENVIRONMENT,
)
from integrates.dataloaders import (
    get_new_context,
)
from integrates.db_model import HISTORIC_TABLE
from integrates.db_model.utils import get_as_utc_iso_format
from integrates.db_model.vulnerabilities.types import Vulnerability
from integrates.dynamodb import keys, operations
from integrates.dynamodb.types import PrimaryKey
from integrates.migrations._0634_update_vuln_inconsistent_historic import Target, _init_vulns_reader

LOGGER = logging.getLogger(__name__)


async def _get_keys_to_delete(vulns: list[Vulnerability | None]) -> tuple[PrimaryKey, ...]:
    return tuple(
        keys.build_key(
            facet=HISTORIC_TABLE.facets["vulnerability_state"],
            values={
                "id": vuln.id,
                "iso8601utc": get_as_utc_iso_format(vuln.state.modified_date),
            },
        )
        for vuln in vulns
        if vuln
    )


async def main() -> None:
    target: Target = "s3" if FI_ENVIRONMENT == "production" else "local"
    loaders = get_new_context()
    async with _init_vulns_reader(target) as reader:
        vulns = await loaders.vulnerability.load_many([row["vuln_id"] for row in reader])
        LOGGER.info("Found %d vulns", len(vulns))
        keys = await _get_keys_to_delete(vulns)
        await operations.batch_delete_item(keys=keys, table=HISTORIC_TABLE)
        LOGGER.info("Successfully deleted %d items", len(keys))


if __name__ == "__main__":
    execution_time = time.strftime("Execution Time:    %Y-%m-%d at %H:%M:%S UTC")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S UTC")
    LOGGER.info("\n%s\n%s", execution_time, finalization_time)
