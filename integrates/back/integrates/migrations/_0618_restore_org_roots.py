"""
Restore items fom integrates_vms which whose pk match the one defined for
git_root_metadata.

Execution Time:    2024-10-28 at 18:22:15
Finalization Time: 2024-10-28 at 20:03:08
"""

import logging
import time
from typing import (
    Any,
)

from aioextensions import (
    collect,
    run,
)
from boto3.dynamodb.conditions import (
    Key,
)

from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model import (
    TABLE as CURRENT_TABLE,
)
from integrates.dynamodb import (
    keys,
)
from integrates.dynamodb.operations import (
    batch_put_item,
    query,
)

LOGGER = logging.getLogger("console")
BACKUP_TABLE = CURRENT_TABLE._replace(name="")

TARGET_ORGS: list[str] = []


def _filter_unwanted_items(items: tuple[dict[str, Any], ...]) -> tuple[dict[str, Any], ...]:
    return tuple(
        item
        for item in items
        if not item["sk"].startswith(
            "MACHINE#",
        )
    )


async def _restore_items_for_root(
    root_id: str,
) -> tuple[int, int]:
    key_structure = BACKUP_TABLE.primary_key
    target_facets = (
        BACKUP_TABLE.facets["git_root_historic_state"],
        BACKUP_TABLE.facets["git_root_metadata"],
        BACKUP_TABLE.facets["ip_root_historic_state"],
        BACKUP_TABLE.facets["ip_root_metadata"],
        BACKUP_TABLE.facets["machine_git_root_execution"],
        BACKUP_TABLE.facets["root_secret"],
        BACKUP_TABLE.facets["url_root_historic_state"],
        BACKUP_TABLE.facets["url_root_metadata"],
    )
    response = await query(
        condition_expression=(Key(key_structure.partition_key).eq(root_id)),
        facets=target_facets,
        table=BACKUP_TABLE,
        index=None,
    )
    target_items = _filter_unwanted_items(response.items)
    await batch_put_item(items=target_items, table=CURRENT_TABLE)
    return len(response.items), len(target_items)


async def _restore_items(root_ids: list[str]) -> tuple[int, int]:
    restored = await collect([_restore_items_for_root(root) for root in root_ids], workers=16)
    found = sum(x[0] for x in restored)
    total_restored = sum(x[1] for x in restored)

    return found, total_restored


async def _get_group_root_ids(group_name: str) -> list[str]:
    primary_key = keys.build_key(
        facet=BACKUP_TABLE.facets["git_root_metadata"],
        values={"name": group_name},
    )

    index = BACKUP_TABLE.indexes["inverted_index"]
    key_structure = index.primary_key
    response = await query(
        condition_expression=(
            Key(key_structure.partition_key).eq(primary_key.sort_key)
            & Key(key_structure.sort_key).begins_with(primary_key.partition_key)
        ),
        facets=(
            BACKUP_TABLE.facets["git_root_metadata"],
            BACKUP_TABLE.facets["ip_root_metadata"],
            BACKUP_TABLE.facets["url_root_metadata"],
        ),
        index=index,
        table=BACKUP_TABLE,
    )

    roots: list[str] = []
    for item in response.items:
        roots.append(item["pk"])

    return roots


async def _restore_items_for_group(
    group_name: str,
) -> tuple[int, int]:
    root_ids = await _get_group_root_ids(group_name=group_name)

    found, restored = await _restore_items(root_ids)
    LOGGER.info(
        "Found %d items and restored %d items for group %s",
        found,
        restored,
        group_name,
    )
    return found, restored


async def _restore_items_for_org(
    loaders: Dataloaders,
    org_name: str,
) -> tuple[int, int]:
    org = await loaders.organization.load(org_name)
    if not org:
        LOGGER.error("Org: %s does not exists.", org_name)
        return 0, 0
    LOGGER.info("Processing org %s", org.name)
    found_items_for_org = 0
    restored_items_for_org = 0
    groups = await loaders.organization_groups.load(org.id)
    LOGGER.info("Found %d groups for org %s", len(groups), org_name)
    for group in groups:
        data = await _restore_items_for_group(group.name)
        found_items_for_org += data[0]
        restored_items_for_org += data[1]
    LOGGER.info(
        "Found %d items and restored %d for org %s",
        found_items_for_org,
        restored_items_for_org,
        org.name,
    )
    return found_items_for_org, restored_items_for_org


async def main() -> None:
    loaders: Dataloaders = get_new_context()

    found = 0
    restored = 0

    for org in TARGET_ORGS:
        data = await _restore_items_for_org(loaders, org)
        found += data[0]
        restored += data[1]

    LOGGER.info("Found %s items in total.", restored)
    LOGGER.info("Restored %s items in total.", restored)


if __name__ == "__main__":
    execution_time = time.strftime("Execution Time:    %Y-%m-%d at %H:%M:%S %Z")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S %Z")
    LOGGER.info("\n%s\n%s", execution_time, finalization_time)
