"""
Find and remove environment url duplications in same root

Execution Time: 2025-02-14 at 23:43:06 UTC
Finalization Time: 2025-02-14 at 23:57:48 UTC
"""

import logging
import logging.config
from collections import Counter

from aioextensions import collect

from integrates.dataloaders import (
    get_new_context,
)
from integrates.db_model.roots.types import (
    Root,
    RootEnvironmentUrl,
    RootEnvironmentUrlsRequest,
)
from integrates.migrations.utils import log_time
from integrates.organizations.domain import (
    get_all_group_names,
)
from integrates.roots.domain import remove_environment_url_id
from integrates.roots.utils import format_url
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)

LOGGER = logging.getLogger(__name__)
LOGGER_CONSOLE = logging.getLogger("console")
INTEGRATES_EMAIL = "integrates@fluidattacks.com"


def find_duplicates(items: list[str]) -> list[str]:
    item_counts = Counter(items)
    return [item for item, count in item_counts.items() if count > 1]


def get_env_duplicates(
    duplicate_url: str, root_env_urls: list[RootEnvironmentUrl]
) -> list[RootEnvironmentUrl]:
    return [env_url for env_url in root_env_urls if format_url(env_url.url) == duplicate_url]


async def process_env_duplicates(
    environment_duplicates: dict[str, list[RootEnvironmentUrl]],
) -> None:
    for duplicate, env_urls in environment_duplicates.items():
        sorted_env_urls = sorted(
            env_urls, key=lambda env_url: env_url.state.modified_date, reverse=True
        )
        env_to_keep = sorted_env_urls[0]
        envs_to_remove = [env_url for env_url in sorted_env_urls if env_url.id != env_to_keep.id]

        LOGGER.info(
            "Processing root environment url duplicate",
            extra={
                "extra": {
                    "group_name": env_to_keep.group_name,
                    "root_id": env_to_keep.root_id,
                    "duplicate": duplicate,
                    "env_to_keep": env_to_keep.url,
                    "envs_to_remove": [env_url.url for env_url in envs_to_remove],
                },
            },
        )
        result = await collect(
            [
                remove_environment_url_id(
                    loaders=get_new_context(),
                    root_id=env_url.root_id,
                    url_id=env_url.id,
                    group_name=env_url.group_name,
                    user_email=INTEGRATES_EMAIL,
                    should_notify=False,
                )
                for env_url in envs_to_remove
            ]
        )
        LOGGER.info(
            "Environment urls removed",
            extra={
                "extra": {
                    "group_name": env_to_keep.group_name,
                    "root_id": env_to_keep.root_id,
                    "duplicate": duplicate,
                    "removed_envs": [removed_env.url for removed_env in result if removed_env],
                },
            },
        )


async def process_root(group_name: str, root: Root) -> None:
    loaders = get_new_context()

    root_env_urls = await loaders.root_environment_urls.load(
        RootEnvironmentUrlsRequest(group_name=group_name, root_id=root.id),
    )
    env_urls = [format_url(env_url.url) for env_url in root_env_urls]

    duplicates = find_duplicates(env_urls)

    if not duplicates:
        return

    environment_duplicates = {
        duplicate: get_env_duplicates(duplicate, root_env_urls) for duplicate in duplicates
    }

    LOGGER_CONSOLE.info(
        "Root environment url duplicates found",
        extra={
            "extra": {
                "group_name": group_name,
                "root_id": root.id,
                "root_nickname": root.state.nickname,
                "env_urls": env_urls,
                "duplicates": duplicates,
            },
        },
    )

    await process_env_duplicates(environment_duplicates)


async def process_group(group_name: str) -> None:
    loaders = get_new_context()
    group_roots = await loaders.group_roots.load(group_name)
    if not group_roots:
        return

    await collect(
        [process_root(group_name=group_name, root=root) for root in group_roots], workers=4
    )

    LOGGER_CONSOLE.info(
        "Group processed",
        extra={"extra": {"group_name": group_name}},
    )


@log_time(LOGGER)
async def main() -> None:
    loaders = get_new_context()
    group_names = sorted(await get_all_group_names(loaders=loaders))
    for group in group_names:
        await process_group(group_name=group)


if __name__ == "__main__":
    main()  # type: ignore[unused-coroutine]
