"""
Remove a trial group without an expiration period.

Execution Time:    2024-11-19 at 22:23:11 UTC
Finalization Time: 2024-11-19 at 22:23:13 UTC
"""

import logging
import logging.config
import time

from aioextensions import (
    run,
)

from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model.groups.enums import (
    GroupManaged,
    GroupStateJustification,
    GroupStateStatus,
)
from integrates.db_model.groups.types import (
    Group,
)
from integrates.db_model.trials.types import (
    Trial,
    TrialMetadataToUpdate,
)
from integrates.group_access.domain import (
    EMAIL_INTEGRATES,
)
from integrates.groups import (
    domain as groups_domain,
)
from integrates.settings import (
    LOGGING,
)
from integrates.trials import (
    domain as trials_domain,
)
from integrates.trials import (
    getters as trials_getters,
)

logging.config.dictConfig(LOGGING)

LOGGER = logging.getLogger(__name__)
LOGGER_CONSOLE = logging.getLogger("console")


async def _expire_trial(loaders: Dataloaders, group: Group, trial: Trial) -> None:
    if not trial.completed:
        await trials_domain.update_metadata(
            email=trial.email,
            metadata=TrialMetadataToUpdate(completed=True),
        )
    if group.state.managed != GroupManaged.UNDER_REVIEW:
        await groups_domain.update_group_managed(
            loaders=loaders,
            comments="Trial period has expired",
            email=EMAIL_INTEGRATES,
            group_name=group.name,
            justification=GroupStateJustification.TRIAL_FINALIZATION,
            managed=GroupManaged.UNDER_REVIEW,
        )


async def _remove_group_data(
    group: Group,
    trial: Trial,
) -> None:
    if group.state.status != GroupStateStatus.DELETED:
        LOGGER.info(
            "Removing data for group %s, created_by %s, start_date %s,"
            " days since expiration: %d",
            group.name,
            group.created_by,
            trial.start_date,
            trials_getters.get_days_since_expiration(trial),
        )
        await groups_domain.remove_group(
            loaders=get_new_context(),
            comments="Scheduled removal of the group and its data",
            email=EMAIL_INTEGRATES,
            group_name=group.name,
            justification=GroupStateJustification.TRIAL_FINALIZATION,
            validate_pending_actions=False,
        )
    else:
        LOGGER.info("Group %s already removed, skipping data removal", group.name)


async def main() -> None:
    group_name = ""
    loaders = get_new_context()

    group = await loaders.group.load(group_name)
    if not group:
        LOGGER.info("Group %s not found", group_name)
        return

    trial = await loaders.trial.load(group.created_by)
    if not trial:
        LOGGER.info("Trial not found for group %s", group_name)
        return

    await _expire_trial(loaders, group, trial)
    await _remove_group_data(group, trial)


if __name__ == "__main__":
    execution_time = time.strftime("Execution Time:    %Y-%m-%d at %H:%M:%S UTC")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S UTC")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
