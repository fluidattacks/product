# type: ignore
"""
billing_customer field was replaced by billing_information

Execution Time: 2024-09-25 at 00:04:06
Finalization Time: 2024-09-25 at 00:04:06
"""

import logging
import time

import stripe
from aioextensions import (
    collect,
    run,
)
from boto3.dynamodb.conditions import (
    Attr,
)

from integrates.context import (
    FI_STRIPE_API_KEY,
)
from integrates.db_model import (
    TABLE,
)
from integrates.db_model.organizations import (
    get_all_organizations,
)
from integrates.db_model.organizations.types import (
    Organization,
)
from integrates.db_model.organizations.utils import (
    remove_org_id_prefix,
)
from integrates.dynamodb import (
    keys,
    operations,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)

LOGGER = logging.getLogger(__name__)
LOGGER_CONSOLE = logging.getLogger("console")

API_VERSION = "2024-06-20"

stripe.api_key = FI_STRIPE_API_KEY
stripe.api_version = API_VERSION


async def _update_metadata(
    *,
    organization_id: str,
    organization_name: str,
) -> None:
    key_structure = TABLE.primary_key
    primary_key = keys.build_key(
        facet=TABLE.facets["organization_metadata"],
        values={
            "id": remove_org_id_prefix(organization_id),
            "name": organization_name,
        },
    )
    item = {"billing_customer": None}
    await operations.update_item(
        condition_expression=Attr(key_structure.partition_key).exists(),
        item=item,
        key=primary_key,
        table=TABLE,
    )
    LOGGER_CONSOLE.info("organization %s updated", organization_name)


async def _process_org(organization: Organization) -> None:
    await _update_metadata(organization_id=organization.id, organization_name=organization.name)


async def main() -> None:
    organizations = await get_all_organizations()
    await collect(
        [
            _process_org(organization)
            for organization in organizations
            if organization.billing_customer
        ],
        workers=15,
    )


if __name__ == "__main__":
    execution_time = time.strftime("Execution Time:    %Y-%m-%d at %H:%M:%S")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
