"""
Migration used to restore vulnerabilities of some deleted root

Execution Time:    2024-07-18 at 13:48:33 UTC
Finalization Time: 2024-07-18 at 13:49:21 UTC
"""

import asyncio
import logging
import logging.config
import time
from datetime import (
    datetime,
)

from aioextensions import (
    run,
)

from integrates.custom_utils import (
    datetime as datetime_utils,
)
from integrates.custom_utils import (
    roots as root_utils,
)
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model import (
    vulnerabilities as vulns_model,
)
from integrates.db_model.finding_comments.enums import (
    CommentType,
)
from integrates.db_model.finding_comments.types import (
    FindingComment,
)
from integrates.db_model.roots.enums import (
    RootStatus,
)
from integrates.db_model.roots.types import (
    GitRoot,
    Root,
)
from integrates.db_model.vulnerabilities.enums import (
    VulnerabilityStateReason,
    VulnerabilityStateStatus,
)
from integrates.db_model.vulnerabilities.types import (
    Vulnerability,
    VulnerabilityState,
)
from integrates.finding_comments import (
    domain as comments_domain,
)
from integrates.roots.domain import (
    activate_root,
)
from integrates.settings import (
    LOGGING,
)
from integrates.vulnerabilities.domain.core import (
    group_vulnerabilities_by_finding_id,
)

logging.config.dictConfig(LOGGING)

# Constants
LOGGER = logging.getLogger(__name__)
LOGGER_CONSOLE = logging.getLogger("console")


async def _reopen_vulnerabilities(
    semaphore: asyncio.Semaphore,
    vulnerability: Vulnerability,
    modified_by: str,
) -> None:
    async with semaphore:
        await vulns_model.update_historic_entry(
            current_value=vulnerability,
            finding_id=vulnerability.finding_id,
            vulnerability_id=vulnerability.id,
            entry=VulnerabilityState(
                commit=vulnerability.state.commit,
                modified_by=modified_by,
                modified_date=datetime_utils.get_utc_now(),
                source=vulnerability.state.source,
                specific=vulnerability.state.specific,
                status=VulnerabilityStateStatus.VULNERABLE,
                reasons=[VulnerabilityStateReason.OTHER],
                where=vulnerability.state.where,
            ),
            force_update=True,
        )


async def _get_content(
    loaders: Dataloaders,
    vulns: list[Vulnerability],
    exclusion_date: datetime,
) -> str:
    wheres = await comments_domain.get_vulns_wheres(loaders, vulns)
    content = (
        f"Regarding vulnerabilities: \n{wheres}\n\nThey "
        + f"were marked as {VulnerabilityStateStatus.VULNERABLE.value} "
        + f"on {exclusion_date.date()} "
        + f"due to {VulnerabilityStateReason.OTHER.value}"
    )
    return content


async def _add_reopen_vulnerabilities_comment(
    loaders: Dataloaders,
    finding_id: str,
    user_email: str,
    vulns: list[Vulnerability],
) -> None:
    comment_id = str(round(time.time() * 1000))
    exclusion_date = datetime_utils.get_utc_now()
    content = await _get_content(
        loaders,
        vulns,
        exclusion_date,
    )
    comment_data = FindingComment(
        finding_id=finding_id,
        comment_type=CommentType.COMMENT,
        content=content,
        parent_id="0",
        id=comment_id,
        email=user_email,
        full_name="Fluid Attacks",
        creation_date=exclusion_date,
    )
    await comments_domain.add(loaders, comment_data)


async def _add_finding_comments(
    loaders: Dataloaders,
    user_email: str,
    vulns: list[Vulnerability],
) -> None:
    grouped_vulns = group_vulnerabilities_by_finding_id(vulns)

    for finding_id, vulnerabilities in grouped_vulns.items():
        await _add_reopen_vulnerabilities_comment(loaders, finding_id, user_email, vulnerabilities)


async def _process_vulnerabilities(loaders: Dataloaders, root_vulns: list[Vulnerability]) -> None:
    deleted_vulns = [
        vuln
        for vuln in root_vulns
        if vuln.state.status == VulnerabilityStateStatus.SAFE
        and vuln.state.reasons
        and VulnerabilityStateReason.EXCLUSION in vuln.state.reasons
    ]
    semaphore = asyncio.Semaphore(16)
    await asyncio.gather(
        *[
            _reopen_vulnerabilities(semaphore, vulnerability, root_utils.EMAIL_INTEGRATES)
            for vulnerability in deleted_vulns
        ],
    )
    LOGGER_CONSOLE.info(
        "Modified vulns",
        extra={"extra": {"modified_vuln_ids": [vuln.id for vuln in deleted_vulns]}},
    )
    await _add_finding_comments(loaders, root_utils.EMAIL_INTEGRATES, deleted_vulns)


async def _process_root(loaders: Dataloaders, root: Root) -> None:
    LOGGER_CONSOLE.info(
        "Root processing started",
        extra={"extra": {"root_nickname": root.state.nickname}},
    )
    if root.state.status == RootStatus.INACTIVE:
        await activate_root(
            loaders=loaders,
            email=root_utils.EMAIL_INTEGRATES,
            group_name=root.group_name,
            root=root,
        )
        LOGGER_CONSOLE.info(
            "Root has been activated",
            extra={"extra": {"root_nickname": root.state.nickname}},
        )
    root_vulns = await loaders.root_vulnerabilities.load(root.id)
    await _process_vulnerabilities(loaders, root_vulns)


async def main() -> None:
    root_nickname = "root_nickname"
    group_name = "group_name"
    loaders: Dataloaders = get_new_context()
    group_roots = {root.state.nickname: root for root in await loaders.group_roots.load(group_name)}
    if not group_roots.get(root_nickname):
        LOGGER_CONSOLE.info(
            "Root nickname not available in group",
            extra={"extra": {"root_nickname": root_nickname}},
        )
        return
    if isinstance(root := group_roots[root_nickname], GitRoot):
        await _process_root(loaders, root)


if __name__ == "__main__":
    execution_time = time.strftime("Execution Time:    %Y-%m-%d at %H:%M:%S %Z")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S %Z")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
