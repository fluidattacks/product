"""
Update vulnerabilities field in forces to include extra data
Execution Time:    2024-04-27 at 02:27:23 UTC
Finalization Time: 2024-04-27 at 04:34:53 UTC

"""

import logging
import logging.config
import time

from aioextensions import (
    collect,
    run,
)
from boto3.dynamodb.conditions import (
    Attr,
)
from botocore.exceptions import (
    HTTPClientError,
)

from integrates.class_types.types import (
    Item,
)
from integrates.dataloaders import (
    get_new_context,
)
from integrates.db_model import (
    TABLE,
)
from integrates.db_model.forces.types import (
    ExecutionVulnerabilities,
    ForcesExecution,
    GroupForcesExecutionsRequest,
)
from integrates.decorators import (
    retry_on_exceptions,
)
from integrates.dynamodb import (
    keys,
    operations,
)
from integrates.forces.domain import (
    get_json_log_execution,
)
from integrates.organizations.domain import (
    get_all_group_names,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)

# Constants
LOGGER = logging.getLogger(__name__)
LOGGER_CONSOLE = logging.getLogger("console")


def format_item(vulnerabilities: ExecutionVulnerabilities) -> Item:
    return {
        "vulnerabilities": {
            "num_of_accepted_vulnerabilities": (vulnerabilities.num_of_accepted_vulnerabilities),
            "num_of_open_vulnerabilities": (vulnerabilities.num_of_open_vulnerabilities),
            "num_of_closed_vulnerabilities": (vulnerabilities.num_of_closed_vulnerabilities),
            "num_of_open_managed_vulnerabilities": (
                vulnerabilities.num_of_open_managed_vulnerabilities
            ),
            "num_of_open_unmanaged_vulnerabilities": (
                vulnerabilities.num_of_open_unmanaged_vulnerabilities
            ),
            **({"open": vulnerabilities.open} if vulnerabilities.open is not None else {}),
            **({"closed": vulnerabilities.closed} if vulnerabilities.closed is not None else {}),
            **(
                {"accepted": vulnerabilities.accepted}
                if vulnerabilities.accepted is not None
                else {}
            ),
            **(
                {"num_of_vulnerabilities_in_exploits": (vulnerabilities.num_of_vulns_in_exploits)}
                if vulnerabilities.num_of_vulns_in_exploits is not None
                else {}
            ),
            **(
                {
                    "num_of_vulnerabilities_in_integrates_exploits": (
                        vulnerabilities.num_of_vulns_in_integrates_exploits
                    ),
                }
                if vulnerabilities.num_of_vulns_in_integrates_exploits is not None
                else {}
            ),
            **(
                {
                    "num_of_vulnerabilities_in_accepted_exploits": (
                        vulnerabilities.num_of_vulns_in_accepted_exploits
                    ),
                }
                if vulnerabilities.num_of_vulns_in_accepted_exploits is not None
                else {}
            ),
        },
    }


async def update_metadata(
    *,
    group_name: str,
    execution_id: str,
    metadata: ExecutionVulnerabilities,
) -> None:
    key_structure = TABLE.primary_key
    primary_key = keys.build_key(
        facet=TABLE.facets["forces_execution"],
        values={
            "id": execution_id,
            "name": group_name,
        },
    )
    item_formatted = format_item(metadata)
    LOGGER_CONSOLE.info(
        "item_formatted",
        extra={
            "extra": {
                "item_formatted": item_formatted,
                "group_name": group_name,
                "execution_id": execution_id,
            },
        },
    )

    await operations.update_item(
        condition_expression=Attr(key_structure.partition_key).exists(),
        item=item_formatted,
        key=primary_key,
        table=TABLE,
    )


async def process_forces(execution: ForcesExecution) -> None:
    if (
        execution.vulnerabilities.num_of_open_managed_vulnerabilities is not None
        or execution.vulnerabilities.num_of_open_unmanaged_vulnerabilities is not None
    ):
        return

    logs = await get_json_log_execution(execution.group_name, execution.id)
    if logs is None:
        return

    num_of_open_unmanaged_vulnerabilities = len(
        [
            vuln
            for log in logs["findings"]
            for vuln in log["vulnerabilities"]
            if "compliance" in vuln
            and str(vuln.get("state", "")).lower() in {"vulnerable", "open"}
            and bool(vuln["compliance"])
        ],
    )
    num_of_open_managed_vulnerabilities = len(
        [
            vuln
            for log in logs["findings"]
            for vuln in log["vulnerabilities"]
            if "compliance" in vuln and not bool(vuln["compliance"])
        ],
    )
    vulnerabilities = execution.vulnerabilities._replace(
        num_of_open_unmanaged_vulnerabilities=(num_of_open_unmanaged_vulnerabilities),
        num_of_open_managed_vulnerabilities=(num_of_open_managed_vulnerabilities),
    )

    await update_metadata(
        group_name=execution.group_name,
        execution_id=execution.id,
        metadata=vulnerabilities,
    )


@retry_on_exceptions(
    exceptions=(HTTPClientError,),
    sleep_seconds=10,
)
async def process_group(group_name: str, progress: float) -> None:
    loaders = get_new_context()
    forces_executions = await loaders.group_forces_executions.load(
        GroupForcesExecutionsRequest(group_name=group_name),
    )

    if not forces_executions:
        return

    await collect(
        [process_forces(execution=execution) for execution in forces_executions],
        workers=16,
    )

    LOGGER_CONSOLE.info(
        "Group updated",
        extra={
            "extra": {
                "group_name": group_name,
                "progress": str(round(progress, 2)),
            },
        },
    )


async def main() -> None:
    loaders = get_new_context()
    group_names = sorted(await get_all_group_names(loaders=loaders), reverse=True)

    for count, group in enumerate(group_names):
        await process_group(
            group_name=group,
            progress=count / len(group_names),
        )


if __name__ == "__main__":
    execution_time = time.strftime("Execution Time:    %Y-%m-%d at %H:%M:%S %Z")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S %Z")
    print("\n%s\n%s", execution_time, finalization_time)
