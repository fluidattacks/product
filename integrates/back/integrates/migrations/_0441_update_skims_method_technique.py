# type: ignore
"""
Update the vulnerability technique information about skims CSPM methods
since we add CSPM as a new technique

Execution Time:    2023-09-26 at 16:39:56 UTC
Finalization Time: 2023-09-26 at 17:31:12 UTC, extra=None

"""

import logging
import logging.config
import time

from aioextensions import (
    run,
)

from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model.enums import (
    Source,
)
from integrates.db_model.vulnerabilities.types import (
    Vulnerability,
    VulnerabilityMetadataToUpdate,
)
from integrates.db_model.vulnerabilities.update import (
    update_metadata,
)
from integrates.organizations import (
    domain as orgs_domain,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)
LOGGER_CONSOLE = logging.getLogger("console")


async def main() -> None:
    loaders: Dataloaders = get_new_context()
    groups = await orgs_domain.get_all_active_group_names(loaders=loaders)
    total_groups: int = len(groups)
    for idx, group in enumerate(groups):
        LOGGER_CONSOLE.info("Processing group %s (%s/%s)...", group, idx + 1, total_groups)
        findings = await loaders.group_findings.load(group)
        vulns = await loaders.finding_vulnerabilities.load_many_chained(
            [fin.id for fin in findings],
        )
        machine_vulns: list[Vulnerability] = [
            vuln for vuln in vulns if (vuln.state.source == Source.MACHINE) or vuln.skims_method
        ]

        if not machine_vulns:
            continue

        for vuln in machine_vulns:
            check_condition = vuln.skims_method is not None and (
                (
                    "gcp." in vuln.skims_method
                    or ("aws." in vuln.skims_method and vuln.technique == "DAST")
                )
                or (
                    "cloudformation." in vuln.skims_method
                    or ("terraform." in vuln.skims_method and vuln.technique == "SAST")
                )
            )
            if check_condition:
                LOGGER_CONSOLE.info(
                    "%s method with %s technique will be updated",
                    vuln.skims_method,
                    vuln.technique,
                )

                await update_metadata(
                    finding_id=vuln.finding_id,
                    metadata=VulnerabilityMetadataToUpdate(
                        technique="CSPM",
                    ),
                    vulnerability_id=vuln.id,
                )


if __name__ == "__main__":
    execution_time = time.strftime("Execution Time:    %Y-%m-%d at %H:%M:%S %Z")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S %Z")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
