"""
Update stakeholder metadata
Execution Time:    2024-07-26 at 15:18:33 UTC
Finalization Time: 2024-07-26 at 15:18:33 UTC
"""

import logging
import logging.config
import time

from aioextensions import (
    run,
)

from integrates.dataloaders import (
    get_new_context,
)
from integrates.db_model.stakeholders import (
    update_metadata,
)
from integrates.db_model.stakeholders.types import (
    StakeholderMetadataToUpdate,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)

# Constants
LOGGER = logging.getLogger(__name__)
LOGGER_CONSOLE = logging.getLogger("console")


async def main() -> None:
    loaders = get_new_context()
    email = ""
    stakeholder = await loaders.stakeholder.load(email)
    if stakeholder and stakeholder.phone:
        await update_metadata(
            metadata=StakeholderMetadataToUpdate(
                phone=None,
            ),
            email=email,
        )


if __name__ == "__main__":
    execution_time = time.strftime("Execution Time:    %Y-%m-%d at %H:%M:%S %Z")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S %Z")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
