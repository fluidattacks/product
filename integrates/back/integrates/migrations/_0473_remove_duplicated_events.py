# type: ignore

"""
Delete all events with the message "Could not find remote branch",
as these events are duplicated.

Execution Time:     2024-01-17 at 14:34:06 UTC
Finalization Time:  2024-01-17 at 14:45:30 UTC, extra=None

"""

import logging
import logging.config
import time

from aioextensions import (
    run,
)

from integrates.dataloaders import (
    get_new_context,
)
from integrates.db_model.events.enums import (
    EventStateStatus,
)
from integrates.db_model.events.remove import (
    remove as remove_event,
)
from integrates.db_model.events.types import (
    GroupEventsRequest,
)
from integrates.organizations.domain import (
    get_all_groups,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)
LOGGER_CONSOLE = logging.getLogger("console")


async def main() -> None:
    loaders = get_new_context()
    groups = await get_all_groups(loaders=loaders)
    events = []
    for group in groups:
        group_events = await loaders.group_events.load(GroupEventsRequest(group_name=group.name))
        events += group_events
    search_term: str = "Could not find remote branch"
    branch_not_found_events_id = [
        event.id
        for event in events
        if search_term in event.description and event.state.status == EventStateStatus.CREATED
    ]

    for event_id in branch_not_found_events_id:
        await remove_event(event_id=event_id)
        print("Removed event: ", event_id)


if __name__ == "__main__":
    execution_time = time.strftime("Execution Time:     %Y-%m-%d at %H:%M:%S %Z")
    run(main())
    finalization_time = time.strftime("Finalization Time:  %Y-%m-%d at %H:%M:%S %Z")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
