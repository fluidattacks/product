"""
Execution Time:    2025-02-03 at 16:40:24 UTC
Finalization Time: 2025-02-03 at 16:40:47 UTC

Update vulnerability
"""

import logging

from integrates.dataloaders import get_new_context
from integrates.db_model.vulnerabilities.enums import VulnerabilityZeroRiskStatus
from integrates.db_model.vulnerabilities.types import VulnerabilityRequest
from integrates.db_model.vulnerabilities.update import update_historic, update_new_historic
from integrates.migrations.utils import log_time
from integrates.unreliable_indicators.enums import EntityDependency
from integrates.unreliable_indicators.operations import update_unreliable_indicators_by_deps
from integrates.vulnerabilities.domain.core import get_vulnerability

LOGGER = logging.getLogger("migrations")


@log_time(LOGGER)
async def main() -> None:
    loaders = get_new_context()
    vulnerability_id = ""
    vulnerability = await get_vulnerability(loaders, vulnerability_id)
    zero_risk = await loaders.vulnerability_historic_zero_risk.load(
        VulnerabilityRequest(finding_id=vulnerability.finding_id, vulnerability_id=vulnerability.id)
    )
    if zero_risk[-2].status != VulnerabilityZeroRiskStatus.REQUESTED:
        return

    await update_new_historic(current_value=vulnerability, historic=tuple(zero_risk[:-1]))

    vulnerability = await get_vulnerability(loaders, vulnerability_id, clear_loader=True)
    await update_historic(current_value=vulnerability, historic=tuple(zero_risk[:-1]))
    await update_unreliable_indicators_by_deps(
        EntityDependency.request_vulnerabilities_zero_risk,
        finding_ids=[vulnerability.finding_id],
        vulnerability_ids=[vulnerability.id],
    )


if __name__ == "__main__":
    main()  # type: ignore[unused-coroutine]
