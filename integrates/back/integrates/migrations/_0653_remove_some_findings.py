"""
Execution Time:    2025-01-23 at 20:44:16 UTC
Finalization Time: 2025-01-23 at 20:44:29 UTC

Remove some empty findings
"""

import logging
from datetime import datetime

from aioextensions import collect

from integrates.dataloaders import get_new_context
from integrates.db_model.findings.types import Finding
from integrates.db_model.groups.types import Group
from integrates.findings.domain.evidence import EVIDENCE_NAMES
from integrates.migrations.utils import log_time
from integrates.organizations.domain import get_all_groups

LOGGER = logging.getLogger("migrations")


def same_creation_evidences(dates: list[datetime]) -> bool:
    for d in dates:
        for date in dates:
            if (max(d, date) - min(d, date)).seconds > 30:
                return False

    return True


async def process_finding(finding: Finding) -> None:
    loaders = get_new_context()
    dates: list[datetime] = [
        getattr(finding.evidences, evidence_name).modified_date
        for _, evidence_name in EVIDENCE_NAMES.items()
        if getattr(finding.evidences, evidence_name)
    ]
    if (
        finding.unreliable_indicators.unreliable_status.DRAFT
        and len(dates) > 1
        and finding.creation
        and any((date - finding.creation.modified_date).days > 10 for date in dates)
        and same_creation_evidences(dates)
    ):
        vulns = await loaders.finding_vulnerabilities_all.load(finding.id)
        if not vulns:
            LOGGER.info("Empty", extra={"extra": {"finding_id": finding.id, "dates": dates}})


async def process_group(group: Group) -> None:
    loaders = get_new_context()
    findings = await loaders.group_findings_all.load(group.name)
    await collect((process_finding(finding) for finding in findings), workers=32)


@log_time(LOGGER)
async def main() -> None:
    _all_groups = await get_all_groups(get_new_context())
    all_groups = sorted(_all_groups, key=lambda x: x.name)
    await collect([process_group(group) for group in all_groups if group.name], workers=4)


if __name__ == "__main__":
    main()  # type: ignore[unused-coroutine]
