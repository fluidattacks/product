# type: ignore
"""
Restore deleted group information.

Execution Time:    2023-11-17 at 21:04:45 UTC
Finalization Time: 2023-11-17 at 21:41:23 UTC

Execution Time:    2023-11-20 at 15:10:09 UTC
Finalization Time: 2023-11-20 at 15:10:14 UTC

Execution Time:    2024-06-27 at 19:03:10 UTC
Finalization Time: 2024-06-27 at 19:07:46 UTC
"""

import logging
import logging.config
import time

from aioextensions import (
    collect,
    run,
)

from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model import (
    TABLE,
)
from integrates.db_model import (
    finding_comments as finding_comments_model,
)
from integrates.db_model import (
    findings as findings_model,
)
from integrates.db_model import (
    group_access as group_access_model,
)
from integrates.db_model import (
    groups as groups_model,
)
from integrates.db_model import (
    roots as roots_model,
)
from integrates.db_model import (
    stakeholders as stakeholders_model,
)
from integrates.db_model import (
    toe_inputs as toe_inputs_model,
)
from integrates.db_model import (
    toe_lines as toe_lines_model,
)
from integrates.db_model import (
    toe_packages as toe_packages_model,
)
from integrates.db_model import (
    vulnerabilities as vulns_model,
)
from integrates.db_model.finding_comments.types import (
    FindingCommentsRequest,
)
from integrates.db_model.findings.types import (
    Finding,
)
from integrates.db_model.group_access.types import (
    GroupAccess,
    GroupAccessMetadataToUpdate,
    GroupStakeholdersAccessRequest,
)
from integrates.db_model.organization_access.get import (
    get_organization_access,
)
from integrates.db_model.organization_access.types import (
    OrganizationAccessRequest,
)
from integrates.db_model.roots.add import (
    add_root_environment_url,
)
from integrates.db_model.roots.types import (
    Root,
    RootEnvironmentSecretsRequest,
    RootEnvironmentUrl,
)
from integrates.db_model.stakeholders.get import (
    get_stakeholders_no_fallback,
)
from integrates.db_model.stakeholders.types import (
    StakeholderMetadataToUpdate,
)
from integrates.db_model.toe_inputs.types import (
    GroupToeInputsRequest,
)
from integrates.db_model.toe_lines.types import (
    GroupToeLinesRequest,
)
from integrates.db_model.toe_packages.types import (
    GroupToePackagesRequest,
)
from integrates.organization_access import (
    domain as orgs_access,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)

LOGGER = logging.getLogger(__name__)
LOGGER_CONSOLE = logging.getLogger("console")
BACKUP_TABLE = TABLE._replace(name="back_up_table_name")
CURRENT_TABLE = TABLE
GROUP_NAME = "group_name"
ORGANIZATION_ID = "ORG#id"


async def process_toe_packages(loaders: Dataloaders) -> None:
    group_toe_package = await loaders.group_toe_packages.load_nodes(
        GroupToePackagesRequest(group_name=GROUP_NAME),
    )

    await collect(
        tuple(toe_packages_model.add(toe_package=toe_package) for toe_package in group_toe_package),
        workers=16,
    )

    LOGGER.info(
        "toe packages processed",
        extra={
            "extra": {
                "packages": len(group_toe_package),
            },
        },
    )


async def process_toe_lines(loaders: Dataloaders) -> None:
    group_toe_lines = await loaders.group_toe_lines.load_nodes(
        GroupToeLinesRequest(group_name=GROUP_NAME),
    )

    await collect(
        tuple(toe_lines_model.add(toe_lines=toe_lines) for toe_lines in group_toe_lines),
        workers=16,
    )

    LOGGER.info(
        "toe lines processed",
        extra={
            "extra": {
                "lines": len(group_toe_lines),
            },
        },
    )


async def process_toe_inputs(loaders: Dataloaders) -> None:
    group_toe_inputs = await loaders.group_toe_inputs.load_nodes(
        GroupToeInputsRequest(group_name=GROUP_NAME),
    )

    await collect(
        tuple(toe_inputs_model.add(toe_input=toe_input) for toe_input in group_toe_inputs),
        workers=16,
    )

    LOGGER.info(
        "toe inputs processed",
        extra={
            "extra": {
                "inputs": len(group_toe_inputs),
            },
        },
    )


async def restore_all_toe(loaders: Dataloaders) -> None:
    await process_toe_inputs(loaders)
    await process_toe_lines(loaders)
    await process_toe_packages(loaders)


async def restore_all_vulnerabilities(loaders: Dataloaders, finding_id: str) -> None:
    vulnerabilities = await loaders.finding_vulnerabilities_all.load(finding_id)

    if not vulnerabilities:
        return

    await collect(
        tuple(vulns_model.add(vulnerability=vulnerability) for vulnerability in vulnerabilities),
        workers=10,
    )

    LOGGER.info(
        "Vulnerabilities processed",
        extra={
            "extra": {
                "finding_id": finding_id,
                "vulns": len(vulnerabilities),
            },
        },
    )


async def restore_all_comments(loaders: Dataloaders, finding_id: str) -> None:
    all_comments = await loaders.finding_comments.load(
        FindingCommentsRequest(
            finding_id=finding_id,
        ),
    )
    if not all_comments:
        return

    await collect(
        tuple(finding_comments_model.add(finding_comment=comment) for comment in all_comments),
    )

    LOGGER.info(
        "Comments processed",
        extra={
            "extra": {
                "finding_id": finding_id,
                "comments": len(all_comments),
            },
        },
    )


async def process_finding(loaders: Dataloaders, finding: Finding) -> None:
    await findings_model.add(finding=finding)
    await restore_all_comments(loaders, finding.id)
    await restore_all_vulnerabilities(loaders, finding.id)


async def restore_all_findings(loaders: Dataloaders) -> None:
    all_findings = await loaders.group_findings_all.load(GROUP_NAME)

    await collect(
        tuple(process_finding(loaders, finding) for finding in all_findings),
        workers=2,
    )

    LOGGER.info(
        "Findings processed",
        extra={
            "extra": {
                "findings": len(all_findings),
                "findings_ids": [finding.id for finding in all_findings],
            },
        },
    )


async def process_stakeholder(loaders: Dataloaders, stakeholder_access: GroupAccess) -> None:
    current_stakeholder = await get_stakeholders_no_fallback(
        emails=[stakeholder_access.email],
        table=CURRENT_TABLE,
    )
    org_access = await get_organization_access(
        requests=[
            OrganizationAccessRequest(
                organization_id=ORGANIZATION_ID,
                email=stakeholder_access.email,
            ),
        ],
        table=BACKUP_TABLE,
    )
    org_access_role = org_access[0].role if org_access[0] else None
    if current_stakeholder:
        has_org_access = await get_organization_access(
            requests=[
                OrganizationAccessRequest(
                    organization_id=ORGANIZATION_ID,
                    email=stakeholder_access.email,
                ),
            ],
            table=CURRENT_TABLE,
        )
        if not has_org_access:
            await orgs_access.add_access(
                loaders,
                ORGANIZATION_ID,
                stakeholder_access.email,
                org_access_role if org_access_role else "user",
            )
    else:
        back_up_stakeholder = await get_stakeholders_no_fallback(
            emails=[stakeholder_access.email],
            table=BACKUP_TABLE,
        )
        stakeholder = back_up_stakeholder[0]
        if stakeholder:
            await stakeholders_model.update_metadata(
                email=stakeholder_access.email,
                metadata=StakeholderMetadataToUpdate(
                    access_tokens=stakeholder.access_tokens,
                    enrolled=stakeholder.enrolled,
                    first_name=stakeholder.first_name,
                    is_concurrent_session=stakeholder.is_concurrent_session,
                    is_registered=stakeholder.is_registered,
                    last_login_date=stakeholder.last_login_date,
                    last_name=stakeholder.last_name,
                    legal_remember=stakeholder.legal_remember,
                    phone=stakeholder.phone,
                    registration_date=stakeholder.registration_date,
                    role=stakeholder.role,
                    session_key=stakeholder.session_key,
                    session_token=stakeholder.session_token,
                    tours=stakeholder.tours,
                ),
            )
            await orgs_access.add_access(
                loaders,
                ORGANIZATION_ID,
                stakeholder_access.email,
                org_access_role if org_access_role else "user",
            )

    await group_access_model.update_metadata(
        email=stakeholder_access.email,
        group_name=GROUP_NAME,
        metadata=GroupAccessMetadataToUpdate(
            has_access=True,
            role=stakeholder_access.role,
            state=stakeholder_access.state,
            invitation=stakeholder_access.invitation,
            responsibility=stakeholder_access.responsibility,
        ),
    )


async def restore_all_stakeholders(loaders: Dataloaders) -> None:
    stakeholders_access = await loaders.group_stakeholders_access.load(
        GroupStakeholdersAccessRequest(group_name=GROUP_NAME),
    )
    await collect(
        tuple(
            process_stakeholder(loaders, stakeholder_access)
            for stakeholder_access in stakeholders_access
        ),
    )

    LOGGER.info(
        "Stakeholders processed",
        extra={
            "extra": {
                "stakeholders": len(stakeholders_access),
                "emails": [stakeholder_access.email for stakeholder_access in stakeholders_access],
            },
        },
    )


async def restore_secrets(loaders: Dataloaders, root_id: str) -> None:
    secrets = await loaders.root_secrets.load(root_id)

    if not secrets:
        return

    await collect(
        tuple(roots_model.add_secret(root_id, secret) for secret in secrets),
        workers=5,
    )

    LOGGER.info(
        "Secrets processed",
        extra={
            "extra": {
                "root_id": root_id,
                "secrets": len(secrets),
            },
        },
    )


async def restore_environment_secrets(loaders: Dataloaders, url_id: str) -> None:
    environment_secrets = await loaders.environment_secrets.load(
        RootEnvironmentSecretsRequest(url_id=url_id, group_name=GROUP_NAME),
    )

    if not environment_secrets:
        return

    await collect(
        tuple(
            roots_model.add_root_environment_secret(
                group_name=GROUP_NAME,
                resource_id=url_id,
                secret=environment_secret,
            )
            for environment_secret in environment_secrets
        ),
    )

    LOGGER.info(
        "Environment url secret processed",
        extra={
            "extra": {
                "url_id": url_id,
                "env_secrets": len(environment_secrets),
            },
        },
    )


async def restore_environment_url(
    loaders: Dataloaders,
    root_id: str,
    url: RootEnvironmentUrl,
) -> None:
    await add_root_environment_url(root_id, url)
    await restore_environment_secrets(
        loaders,
        url.id,
    )

    LOGGER.info(
        "Environment url processed",
        extra={"extra": {"root_id": root_id, "url_id": url.id}},
    )


async def restore_all_environment_urls(loaders: Dataloaders, root_id: str) -> None:
    urls = await loaders.root_environment_urls.load(root_id)
    if not urls:
        return

    await collect(tuple(restore_environment_url(loaders, root_id, url) for url in urls))

    LOGGER.info(
        "Environment urls processed",
        extra={
            "extra": {
                "root_id": root_id,
                "urls": len(urls),
            },
        },
    )


async def process_root(loaders: Dataloaders, root: Root) -> None:
    await roots_model.add(root=root)
    await restore_all_environment_urls(loaders, root.id)
    await restore_secrets(loaders, root.id)

    LOGGER.info(
        "Root processed",
        extra={
            "extra": {
                "root_id": root.id,
            },
        },
    )


async def restore_all_roots(loaders: Dataloaders) -> None:
    roots = await loaders.group_roots.load(GROUP_NAME)

    await collect(
        tuple(process_root(loaders, root) for root in roots),
        workers=32,
    )

    LOGGER.info(
        "Roots processed",
        extra={
            "extra": {
                "roots": len(roots),
                "root_ids": [(root.id, root.state.nickname) for root in roots],
            },
        },
    )


async def main() -> None:
    loaders: Dataloaders = get_new_context()
    group_info = await loaders.group.load(GROUP_NAME)
    if group_info:
        await groups_model.add(group=group_info)
        await restore_all_roots(loaders)
        await restore_all_stakeholders(loaders)
        await restore_all_findings(loaders)
        await restore_all_toe(loaders)

        LOGGER.info(
            "Group processed",
            extra={
                "extra": {
                    "group_name": group_info.name,
                },
            },
        )


if __name__ == "__main__":
    execution_time = time.strftime("Execution Time:    %Y-%m-%d at %H:%M:%S UTC")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S UTC")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
