"""
Add business_id and business_name to organization

Execution Time:    2024-09-10 at 23:05:48 UTC
Finalization Time: 2024-09-10 at 23:05:51 UTC
"""

import logging
import logging.config
import time

from aioextensions import (
    collect,
    run,
)

from integrates.dataloaders import (
    get_new_context,
)
from integrates.db_model.groups.types import (
    Group,
)
from integrates.db_model.organizations.get import (
    get_all_organizations,
)
from integrates.db_model.organizations.types import (
    Organization,
    OrganizationMetadataToUpdate,
)
from integrates.db_model.organizations.update import (
    update_metadata,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)

# Constants
LOGGER = logging.getLogger(__name__)
LOGGER_CONSOLE = logging.getLogger("console")


def _get_groups_business_id_and_name(
    groups: list[Group],
) -> tuple[str | None, str | None]:
    groups_attributes: dict[str, str | None] = {
        "business_id": None,
        "business_name": None,
    }

    for group in groups:
        if group.business_id:
            if not groups_attributes["business_id"]:
                groups_attributes["business_id"] = group.business_id.strip()
            elif groups_attributes["business_id"] != group.business_id.strip():
                LOGGER_CONSOLE.error(
                    "Different business_id found in group %s",
                    group.name,
                    extra={
                        "extra": {
                            "organization": group.organization_id,
                            "business_id": group.business_id,
                            "business_name": group.business_name,
                        },
                    },
                )

                raise ValueError("Different business_id found in groups")

        if group.business_name:
            if not groups_attributes["business_name"]:
                groups_attributes["business_name"] = group.business_name
            elif groups_attributes["business_name"] != group.business_name:
                LOGGER_CONSOLE.error(
                    "Different business_name found in group %s",
                    group.name,
                    extra={
                        "extra": {
                            "organization": group.organization_id,
                            "business_id": group.business_id,
                            "business_name": group.business_name,
                        },
                    },
                )

                raise ValueError("Different business_name found in groups")

    return groups_attributes["business_id"], groups_attributes["business_name"]


async def _process_org(organization: Organization) -> None:
    loaders = get_new_context()
    groups = await loaders.organization_groups.load(organization.id)

    try:
        business_id, business_name = _get_groups_business_id_and_name(groups)

        if not business_id or not business_name:
            LOGGER_CONSOLE.info(
                "Business_id or business_name not found in organization %s",
                organization.name,
                extra={
                    "extra": {
                        "organization": organization.name,
                        "business_id": business_id,
                        "business_name": business_name,
                    },
                },
            )

        if business_id or business_name:
            await update_metadata(
                organization_id=organization.id,
                organization_name=organization.name,
                metadata=OrganizationMetadataToUpdate(
                    business_id=business_id,
                    business_name=business_name,
                ),
            )

            LOGGER_CONSOLE.info("Organization %s updated", organization.name)

    except ValueError as ex:
        LOGGER_CONSOLE.error(
            "Error processing organization %s",
            organization.name,
            extra={"extra": {"organization": organization.name}},
        )
        LOGGER.exception(ex)


async def main() -> None:
    all_organizations = await get_all_organizations()

    await collect(
        [_process_org(organization=organization) for organization in all_organizations],
        workers=15,
    )


if __name__ == "__main__":
    execution_time = time.strftime("Execution Time:    %Y-%m-%d at %H:%M:%S UTC%Z")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S UTC%Z")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
