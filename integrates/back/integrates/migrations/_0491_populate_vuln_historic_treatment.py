"""
Populate vulnerability historic treatment in integrates_vms_historic table.

Start Time: 2024-02-15 at 16:58:50 UTC
Finalization Time: 2024-02-15 at 18:33:32 UTC

Start Time: 2024-02-16 at 15:58:37 UTC
Finalization Time: 2024-02-16 at 17:32:13 UTC
"""

import asyncio
import logging
import logging.config
import time

from aioextensions import (
    collect,
    run,
)
from aiohttp.client_exceptions import (
    ClientConnectorError,
    ClientOSError,
    ClientPayloadError,
    ServerDisconnectedError,
    ServerTimeoutError,
)
from boto3.dynamodb.conditions import (
    Key,
)
from botocore.exceptions import (
    ClientError,
    ConnectTimeoutError,
    HTTPClientError,
    ReadTimeoutError,
)
from more_itertools import (
    chunked,
)

from integrates.class_types.types import (
    Item,
)
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model import (
    HISTORIC_TABLE,
    TABLE,
)
from integrates.db_model.utils import (
    get_historic_gsi_2_key,
    get_historic_gsi_3_key,
    get_historic_gsi_sk,
)
from integrates.db_model.vulnerabilities.constants import (
    GROUP_INDEX_METADATA,
)
from integrates.decorators import (
    retry_on_exceptions,
)
from integrates.dynamodb import (
    keys,
    operations,
)
from integrates.dynamodb.exceptions import (
    UnavailabilityError,
)
from integrates.organizations import (
    domain as orgs_domain,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)
LOGGER_CONSOLE = logging.getLogger("console")
NETWORK_ERRORS = (
    asyncio.TimeoutError,
    ClientConnectorError,
    ClientError,
    ClientOSError,
    ClientPayloadError,
    ConnectionError,
    ConnectionResetError,
    ConnectTimeoutError,
    HTTPClientError,
    ReadTimeoutError,
    ServerDisconnectedError,
    ServerTimeoutError,
    UnavailabilityError,
)


batch_put_item = retry_on_exceptions(
    exceptions=NETWORK_ERRORS,
    sleep_seconds=300,
)(operations.batch_put_item)


@retry_on_exceptions(
    exceptions=NETWORK_ERRORS,
    sleep_seconds=10,
)
async def _get_vulnerabilities_by_group(
    group_name: str,
) -> tuple[Item, ...]:
    primary_key = keys.build_key(
        facet=GROUP_INDEX_METADATA,
        values={
            "group_name": group_name,
        },
    )
    group_index = TABLE.indexes["gsi_5"]
    key_structure = group_index.primary_key
    sort_key = "VULN#ZR#"
    condition_expression = Key(key_structure.partition_key).eq(primary_key.partition_key) & Key(
        key_structure.sort_key,
    ).begins_with(sort_key)
    response = await operations.query(
        condition_expression=condition_expression,
        facets=(GROUP_INDEX_METADATA,),
        index=group_index,
        table=TABLE,
    )
    return response.items


@retry_on_exceptions(
    exceptions=NETWORK_ERRORS,
    sleep_seconds=10,
)
async def _get_historic_treatment(
    vulnerability_id: str,
) -> tuple[Item, ...]:
    primary_key = keys.build_key(
        facet=TABLE.facets["vulnerability_historic_treatment"],
        values={"id": vulnerability_id},
    )
    key_structure = TABLE.primary_key
    response = await operations.query(
        condition_expression=(
            Key(key_structure.partition_key).eq(primary_key.partition_key)
            & Key(key_structure.sort_key).begins_with(primary_key.sort_key)
        ),
        facets=(TABLE.facets["vulnerability_historic_treatment"],),
        table=TABLE,
    )
    return response.items


def format_treatment_item(treatment_item: Item, vulnerability_item: Item, group_name: str) -> Item:
    item_pk = f"{vulnerability_item['pk']}#{vulnerability_item['sk']}"
    item_sk = get_historic_gsi_sk("treatment", treatment_item["modified_date"])
    gsi_2_key = get_historic_gsi_2_key(
        HISTORIC_TABLE.facets["vulnerability_treatment"],
        group_name,
        "treatment",
        treatment_item["modified_date"],
    )
    gsi_3_key = get_historic_gsi_3_key(group_name, "treatment", treatment_item["modified_date"])
    gsi_keys = {
        "pk_2": gsi_2_key.partition_key,
        "sk_2": gsi_2_key.sort_key,
        "pk_3": gsi_3_key.partition_key,
        "sk_3": gsi_3_key.sort_key,
    }
    return {
        **(
            vulnerability_item["treatment"]
            if treatment_item["modified_date"] == vulnerability_item["treatment"]["modified_date"]
            else treatment_item
        ),
        "pk": item_pk,
        "sk": item_sk,
        **gsi_keys,
    }


async def process_vulnerability(vulnerability_item: Item, group_name: str) -> None:
    vulnerability_id = vulnerability_item["pk"].split("#")[1]
    vulnerability_historic_treatment: tuple[Item, ...] = await _get_historic_treatment(
        vulnerability_id=vulnerability_id,
    )
    if not vulnerability_historic_treatment and vulnerability_item.get("treatment"):
        vulnerability_historic_treatment = (vulnerability_item["treatment"],)

    formatted_items = tuple(
        format_treatment_item(treatment_item, vulnerability_item, group_name)
        for treatment_item in vulnerability_historic_treatment
    )
    for chunked_formatted_items in chunked(formatted_items, 25):
        await batch_put_item(
            items=tuple(chunked_formatted_items),
            table=HISTORIC_TABLE,
        )


async def process_group(group_name: str) -> None:
    group_vulnerabilities = await _get_vulnerabilities_by_group(group_name=group_name)
    await collect(
        tuple(
            process_vulnerability(vulnerability_item, group_name)
            for vulnerability_item in group_vulnerabilities
        ),
        workers=64,
    )
    LOGGER_CONSOLE.info(
        "Group processed",
        extra={
            "extra": {
                "group_name": group_name,
                "group_vulnerabilities": len(group_vulnerabilities),
            },
        },
    )


async def main() -> None:
    loaders: Dataloaders = get_new_context()
    all_group_names = sorted(await orgs_domain.get_all_group_names(loaders))
    LOGGER_CONSOLE.info(
        "All group names",
        extra={
            "extra": {
                "total": len(all_group_names),
            },
        },
    )
    for count, group_name in enumerate(all_group_names, start=1):
        LOGGER_CONSOLE.info(
            "Group",
            extra={
                "extra": {
                    "group_name": group_name,
                    "count": count,
                },
            },
        )
        await process_group(group_name)


if __name__ == "__main__":
    execution_time = time.strftime("Start Time:        %Y-%m-%d at %H:%M:%S UTC")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S UTC")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
