"""
Populate toe input historic state in integrates_vms_historic table
for toe input with no historic.

Start Time: 2024-02-05 at 20:15:12 UTC
Finalization Time: 2024-02-05 at 20:21:51
"""

import logging
import logging.config
import time

from aioextensions import (
    collect,
    run,
)
from boto3.dynamodb.conditions import (
    Key,
)
from botocore.exceptions import (
    ConnectTimeoutError,
    ReadTimeoutError,
)

from integrates.class_types.types import (
    Item,
)
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model import (
    HISTORIC_TABLE,
    TABLE,
)
from integrates.db_model.toe_inputs.types import (
    ToeInputRequest,
)
from integrates.db_model.utils import (
    get_historic_gsi_2_key,
    get_historic_gsi_3_key,
    get_historic_gsi_sk,
)
from integrates.decorators import (
    retry_on_exceptions,
)
from integrates.dynamodb import (
    keys,
    operations,
)
from integrates.organizations import (
    domain as orgs_domain,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)
LOGGER_CONSOLE = logging.getLogger("console")


async def get_toe_inputs_by_group(
    group_name: str,
) -> tuple[Item, ...]:
    primary_key = keys.build_key(
        facet=TABLE.facets["toe_input_metadata"],
        values={"group_name": group_name},
    )
    key_structure = TABLE.primary_key
    response = await operations.query(
        condition_expression=(
            Key(key_structure.partition_key).eq(primary_key.partition_key)
            & Key(key_structure.sort_key).begins_with(
                primary_key.sort_key.replace("#ROOT#COMPONENT#ENTRYPOINT", ""),
            )
        ),
        facets=(TABLE.facets["toe_input_metadata"],),
        index=None,
        table=TABLE,
    )

    return response.items


async def get_historic_toe_input(
    request: ToeInputRequest,
) -> tuple[Item, ...]:
    primary_key = keys.build_key(
        facet=TABLE.facets["toe_input_historic_metadata"],
        values={
            "component": request.component,
            "entry_point": request.entry_point,
            "group_name": request.group_name,
            "root_id": request.root_id,
        },
    )
    key_structure = TABLE.primary_key
    response = await operations.query(
        condition_expression=(
            Key(key_structure.partition_key).eq(primary_key.partition_key)
            & Key(key_structure.sort_key).begins_with(primary_key.sort_key)
        ),
        facets=(TABLE.facets["toe_input_historic_metadata"],),
        table=TABLE,
    )

    return response.items


@retry_on_exceptions(
    exceptions=(ReadTimeoutError, ConnectTimeoutError),
    sleep_seconds=3,
)
async def process_historic_toe_input(item: Item) -> None:
    item_pk = f"{item['pk']}#{item['sk']}"
    item_sk = get_historic_gsi_sk("state", item["state"]["modified_date"])
    group_name = item["group_name"]
    gsi_2_key = get_historic_gsi_2_key(
        HISTORIC_TABLE.facets["toe_input_state"],
        group_name,
        "state",
        item["state"]["modified_date"],
    )
    gsi_3_key = get_historic_gsi_3_key(group_name, "state", item["state"]["modified_date"])
    gsi_keys = {
        "pk_2": gsi_2_key.partition_key,
        "sk_2": gsi_2_key.sort_key,
        "pk_3": gsi_3_key.partition_key,
        "sk_3": gsi_3_key.sort_key,
    }
    await operations.put_item(
        facet=HISTORIC_TABLE.facets["toe_input_state"],
        item={
            "pk": item_pk,
            "sk": item_sk,
            **gsi_keys,
            **item["state"],
        },
        table=HISTORIC_TABLE,
    )


async def process_historic_toe_inputs(group_name: str, toe_item: Item) -> None:
    historic_toe_inputs: tuple[Item, ...] = await get_historic_toe_input(
        ToeInputRequest(
            component=toe_item["component"],
            entry_point=toe_item["entry_point"],
            group_name=group_name,
            root_id=toe_item["root_id"],
        ),
    )
    if not historic_toe_inputs:
        print("no_historic")
        await process_historic_toe_input(toe_item)


async def process_group(group_name: str) -> None:
    group_toe_inputs = await get_toe_inputs_by_group(group_name)
    await collect(
        tuple(process_historic_toe_inputs(group_name, item) for item in group_toe_inputs),
        workers=64,
    )
    LOGGER_CONSOLE.info(
        "Group processed",
        extra={
            "extra": {
                "group_name": group_name,
                "group_toe_inputs": len(group_toe_inputs),
            },
        },
    )


async def main() -> None:
    loaders: Dataloaders = get_new_context()
    all_group_names = sorted(await orgs_domain.get_all_group_names(loaders))
    LOGGER_CONSOLE.info(
        "All group names",
        extra={
            "extra": {
                "total": len(all_group_names),
            },
        },
    )
    for count, group_name in enumerate(all_group_names, start=1):
        LOGGER_CONSOLE.info(
            "Group",
            extra={
                "extra": {
                    "group_name": group_name,
                    "count": count,
                },
            },
        )
        await process_group(group_name)


if __name__ == "__main__":
    execution_time = time.strftime("Start Time:        %Y-%m-%d at %H:%M:%S UTC")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S UTC")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
