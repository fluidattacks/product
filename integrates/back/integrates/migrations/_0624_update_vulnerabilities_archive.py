"""Update field from vulnerabilities archive"""

import time

from aioextensions import (
    collect,
    in_thread,
    run,
)
from psycopg2 import (  # type: ignore[import-untyped]
    sql,
)
from psycopg2.extensions import (  # type: ignore[import-untyped]
    cursor as cursor_cls,
)

from integrates.db_model.last_sync import (
    get_db_cursor,
)

TECHNIQUES = {
    "SCA": {"120", "393", "011", "431"},
    "DAST": {
        "440",
        "064",
        "133",
        "137",
        "094",
        "398",
        "082",
        "023",
        "182",
        "103",
        "129",
        "036",
        "347",
        "132",
        "268",
    },
    "CSPM": {"392", "446", "200", "257", "319", "158", "277", "281", "101"},
    "SAST": {
        "089",
        "354",
        "063",
        "250",
        "001",
        "068",
        "236",
        "085",
        "368",
        "134",
        "143",
        "010",
        "002",
        "112",
        "097",
        "009",
        "123",
        "153",
        "401",
        "408",
        "353",
        "012",
        "096",
        "188",
        "359",
        "106",
        "309",
        "034",
        "385",
        "265",
        "083",
        "330",
        "017",
        "021",
        "111",
        "358",
        "098",
        "008",
        "035",
        "164",
        "262",
        "423",
        "027",
        "044",
        "160",
        "421",
        "062",
        "395",
        "320",
        "042",
        "056",
        "100",
        "169",
        "115",
        "108",
        "362",
        "037",
        "332",
        "426",
        "436",
        "344",
        "418",
        "004",
        "149",
        "416",
        "343",
        "136",
        "117",
        "078",
        "412",
        "371",
        "380",
        "211",
        "267",
        "414",
        "350",
        "029",
        "266",
        "079",
        "091",
        "413",
        "146",
        "390",
        "107",
        "239",
        "404",
        "338",
        "065",
        "249",
        "007",
        "437",
    },
}


def _update_vulns_metadata(
    *,
    cursor: cursor_cls,
    _id: str,
    technique: str,
) -> None:
    cursor.execute(
        sql.SQL(
            """
                UPDATE
                    integrates.vulnerabilities_metadata
                SET
                    technique = %(technique)s
                WHERE
                    id = %(id)s;
            """,
        ),
        {
            "id": _id,
            "technique": technique,
        },
    )


async def update_vulns_metadata(
    *,
    cursor: cursor_cls,
    vuln_id: str,
    title: str,
) -> None:
    code = title.split(".")[0].strip()
    technique = next((key for key, value in TECHNIQUES.items() if code in value), None)
    if technique:
        await in_thread(
            _update_vulns_metadata,
            cursor=cursor,
            _id=vuln_id,
            technique=technique,
        )


def get_vulns_missing_field(cursor: cursor_cls) -> list[tuple[str, str, str]]:
    cursor.execute(
        sql.SQL(
            """
            SELECT
                v.id, f.title, v.skims_method
            FROM
                integrates.vulnerabilities_metadata v
                LEFT JOIN integrates.findings_metadata f ON f.id = v.finding_id
            WHERE
                v.technique is NULL
                AND v.skims_method is not NULL
                AND f.title is not NULL;
            """,
        ),
    )

    return list(cursor.fetchall())


async def main() -> None:
    with get_db_cursor() as cursor:
        vulns_redshift = get_vulns_missing_field(cursor)
        await collect(
            (
                update_vulns_metadata(cursor=cursor, vuln_id=vuln[0], title=vuln[1])
                for vuln in vulns_redshift
                if vuln[1] is not None and vuln[2]
            ),
            workers=32,
        )


if __name__ == "__main__":
    execution_time = time.strftime("Execution Time:    %Y-%m-%d at %H:%M:%S %Z")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S %Z")
    print("\n%s\n%s", execution_time, finalization_time)
