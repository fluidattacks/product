"""
Distribute okada mailmap entries to other organizations

This migration script distributes the okada mailmap entries to their
corresponding organizations based on the email domains.

This migration is to be executed locally.

To run the migration you will need:

1. A CSV file mapping each organization name to their email domain(s).
2. To set the path to this CSV file in the ORGANIZATIONS_FILE_PATH variable.

The CSV file must have two columns:

1. "name": The name of the organization
2. "domain": The email domain associated with the organization

If your CSV file uses different column names, you will need to adjust the
script accordingly.

Note: Ensure that the CSV file is properly formatted and contains all necessary
organization-domain mappings before running the script.

Start Time:        2024-08-06 at 15:41:36 UTC
Finalization Time: 2024-08-06 at 17:47:59 UTC

"""

import csv
import logging
import logging.config
import time

from aioextensions import (
    run,
)

from integrates.custom_exceptions import (
    MailmapEntryAlreadyExists,
    MailmapEntryNotFound,
    MailmapOrganizationNotFound,
    MailmapSubentryAlreadyExists,
    MailmapSubentryNotFound,
)
from integrates.mailmap.get import (
    get_mailmap_entries_with_subentries,
)
from integrates.mailmap.update import (
    move_mailmap_entry_with_subentries,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)
LOGGER_CONSOLE = logging.getLogger("console")


ORGANIZATION_ID = ""
ORGANIZATIONS_FILE_PATH = ""


def build_orgs_domain_name_map(file_path: str) -> dict[str, str]:
    orgs_domain_name_map: dict[str, str] = {}

    with open(file_path, newline="", encoding="utf-8") as csv_file:
        reader = csv.DictReader(csv_file)
        for row in reader:
            domain: str = row["domain"].strip()
            name: str = row["name"]
            if domain:
                orgs_domain_name_map[domain] = name

    return orgs_domain_name_map


async def main() -> None:
    orgs_domain_name_map = build_orgs_domain_name_map(ORGANIZATIONS_FILE_PATH)

    entries_with_subentries = await get_mailmap_entries_with_subentries(
        organization_id=ORGANIZATION_ID,
    )

    for entry_with_subentries in entries_with_subentries:
        entry = entry_with_subentries["entry"]
        subentries = entry_with_subentries["subentries"]

        entry_email = entry["mailmap_entry_email"]
        subentries_emails = [subentry["mailmap_subentry_email"] for subentry in subentries]

        emails = [entry_email] + subentries_emails

        for org_domain, org_name in orgs_domain_name_map.items():
            if all(email.endswith(org_domain) for email in emails):
                LOGGER_CONSOLE.info(
                    "Mailmap entry %s with emails %s can be moved to %s - %s",
                    entry_email,
                    emails,
                    org_name,
                    org_domain,
                )
                try:
                    await move_mailmap_entry_with_subentries(
                        entry_email=entry_email,
                        organization_id=ORGANIZATION_ID,
                        new_organization_id=org_name,
                    )
                    LOGGER_CONSOLE.info(
                        "Mailmap entry %s moved to %s - %s successfully",
                        entry_email,
                        org_name,
                        org_domain,
                    )
                except (
                    MailmapEntryAlreadyExists,
                    MailmapOrganizationNotFound,
                    MailmapEntryNotFound,
                    MailmapSubentryAlreadyExists,
                    MailmapSubentryNotFound,
                ) as ex:
                    LOGGER_CONSOLE.error(ex)


if __name__ == "__main__":
    execution_time = time.strftime("Start Time:        %Y-%m-%d at %H:%M:%S UTC")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S UTC")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
