"""
Clean accumulated machine actions due to a confluence of high load
schedules

Execution Time:    2025-02-17 at 22:07:51 UTC
Finalization Time: 2025-02-17 at 22:18:22 UTC, extra=None

Execution Time:    2025-02-18 at 15:27:01 UTC
Finalization Time: 2025-02-18 at 15:34:29 UTC, extra=None
"""

import logging
import logging.config
import time
from operator import attrgetter

from aioextensions import collect, run

from integrates.batch import dal as batch_dal
from integrates.batch.enums import Action
from integrates.batch.types import BatchProcessing
from integrates.settings import LOGGING

logging.config.dictConfig(LOGGING)
LOGGER = logging.getLogger("migrations")


async def clean_machine_action(action: BatchProcessing) -> None:
    await batch_dal.delete_action(action_key=action.key)
    if action.batch_job_id:
        jobs_info = await batch_dal.describe_jobs(action.batch_job_id)
        if len(jobs_info) and jobs_info[0].get("status") == "RUNNABLE":
            await batch_dal.terminate_batch_job(
                job_id=action.batch_job_id,
                reason="Manually terminated, migration script from integrates",
            )

    LOGGER.info("Deleted/Terminated: %s %s", action.key, action.batch_job_id)


async def main() -> None:
    all_actions = await batch_dal.get_actions()
    machine_postclone_actions = sorted(
        [
            action
            for action in all_actions
            if action.action_name == Action.EXECUTE_MACHINE_SAST
            and action.subject
            in {
                "schedule_clone_groups_roots@fluidattacks.com",
                "schedule_clone_roots_not_cloned@fluidattacks.com",
            }
        ],
        key=attrgetter("time"),
    )
    LOGGER.info("Registered machine post-clone actions: %s", str(len(machine_postclone_actions)))

    await collect(
        [clean_machine_action(action) for action in machine_postclone_actions],
        workers=32,
    )


if __name__ == "__main__":
    execution_time = time.strftime("Execution Time:    %Y-%m-%d at %H:%M:%S UTC")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S UTC")
    LOGGER.info("\n%s\n%s", execution_time, finalization_time)
