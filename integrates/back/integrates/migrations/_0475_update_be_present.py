"""
modify be_present attribute in toe_lines for specific group

Execution Time:    2024-01-22 at 17:16:49 UTC
Finalization Time: 2024-01-22 at 17:17:05 UTC
"""

import time

from aioextensions import (
    run,
)

from integrates.dataloaders import (
    get_new_context,
)
from integrates.db_model.toe_lines.types import (
    RootToeLinesRequest,
)
from integrates.toe.lines import (
    domain as toe_lines_domain,
)
from integrates.toe.lines.types import (
    ToeLinesAttributesToUpdate,
)


async def main() -> None:
    loaders = get_new_context()
    toe_lines = await loaders.root_toe_lines.load_nodes(
        RootToeLinesRequest(
            group_name="unittesting",
            root_id="4039d098-ffc5-4984-8ed3-eb17bca98e19",
        ),
    )

    for toe_line in toe_lines:
        await toe_lines_domain.update(
            toe_line,
            ToeLinesAttributesToUpdate(
                be_present=False,
            ),
        )


if __name__ == "__main__":
    execution_time = time.strftime("Execution Time:    %Y-%m-%d at %H:%M:%S UTC")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S UTC")
    print(f"{execution_time}\n{finalization_time}")
