"""
Clear this Git Root status so it can be re-populated with the expected
Machine Executions info

Start Time:        2024-04-23 at 00:57:20 UTC
Finalization Time: 2024-04-23 at 01:05:50 UTC
Start Time:        2024-09-23 at 14:34:51 UTC
Finalization Time: 2024-09-23 at 14:35:48 UTC
"""

import asyncio
import logging
import logging.config
import time

from aioextensions import (
    collect,
    run,
)
from aiohttp.client_exceptions import (
    ClientConnectorError,
    ClientOSError,
    ClientPayloadError,
    ServerDisconnectedError,
    ServerTimeoutError,
)
from boto3.dynamodb.conditions import (
    Attr,
)
from botocore.exceptions import (
    ClientError,
    ConnectTimeoutError,
    HTTPClientError,
    ReadTimeoutError,
)

from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model import (
    TABLE,
)
from integrates.db_model.groups.types import (
    Group,
)
from integrates.db_model.roots.enums import (
    RootCriticality,
)
from integrates.db_model.roots.types import (
    GitRoot,
)
from integrates.decorators import (
    retry_on_exceptions,
)
from integrates.dynamodb import (
    keys,
    operations,
)
from integrates.dynamodb.exceptions import (
    UnavailabilityError,
)
from integrates.organizations import (
    domain as orgs_domain,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)
LOGGER = logging.getLogger("console")
NETWORK_ERRORS = (
    asyncio.TimeoutError,
    ClientConnectorError,
    ClientError,
    ClientOSError,
    ClientPayloadError,
    ConnectionError,
    ConnectionResetError,
    ConnectTimeoutError,
    HTTPClientError,
    ReadTimeoutError,
    ServerDisconnectedError,
    ServerTimeoutError,
    UnavailabilityError,
)


@retry_on_exceptions(
    exceptions=NETWORK_ERRORS,
    sleep_seconds=5,
)
async def update_git_root_criticality(group_name: str, root_id: str) -> None:
    key_structure = TABLE.primary_key
    primary_key = keys.build_key(
        facet=TABLE.facets["git_root_metadata"],
        values={"name": group_name, "uuid": root_id},
    )
    await operations.update_item(
        condition_expression=Attr(key_structure.partition_key).exists(),
        item={"state.criticality": None},
        key=primary_key,
        table=TABLE,
    )


@retry_on_exceptions(
    exceptions=NETWORK_ERRORS,
    sleep_seconds=5,
)
async def process_group(group: Group, progress: float) -> None:
    loaders: Dataloaders = get_new_context()
    git_roots = [
        root
        for root in await loaders.group_roots.load(group.name)
        if isinstance(root, GitRoot) and root.state.criticality == RootCriticality.LOW
    ]
    await collect(
        [update_git_root_criticality(root.group_name, root.id) for root in git_roots],
        workers=8,
    )
    LOGGER.info(
        "Group processed",
        extra={
            "extra": {
                "group_name": group.name,
                "roots_processed": len(git_roots),
                "progress": round(progress, 2),
            },
        },
    )


@retry_on_exceptions(
    exceptions=NETWORK_ERRORS,
    sleep_seconds=5,
)
async def main() -> None:
    loaders: Dataloaders = get_new_context()
    all_groups = await orgs_domain.get_all_groups(loaders=loaders)
    LOGGER.info(
        "All groups",
        extra={
            "extra": {
                "groups_len": len(all_groups),
                "group_names": [group.name for group in all_groups],
            },
        },
    )
    await collect(
        [
            process_group(
                group=group,
                progress=count / len(all_groups),
            )
            for count, group in enumerate(all_groups)
        ],
        workers=1,
    )


if __name__ == "__main__":
    execution_time = time.strftime("Start Time:    %Y-%m-%d at %H:%M:%S UTC")
    LOGGER.info("%s", execution_time)
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S UTC")
    LOGGER.info("\n%s\n%s\n", execution_time, finalization_time)
