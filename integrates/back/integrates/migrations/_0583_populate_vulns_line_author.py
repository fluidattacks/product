"""Populate codebase author's info for all active LINES vulns in active roots"""

import logging
import logging.config
import os
import tempfile
import time

from aioextensions import (
    collect,
    run,
)

from integrates.batch_dispatch.update_vulns_author import (
    update_root_vulns_author,
)
from integrates.custom_utils.roots import (
    get_active_git_roots,
)
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model.roots.types import (
    GitRoot,
)
from integrates.organizations.domain import (
    get_all_active_group_names,
)
from integrates.roots.s3_mirror import (
    download_repo,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)
LOGGER = logging.getLogger("console")
EMAIL_INTEGRATES = "integrates@fluidattacks.com"


async def process_root(root: GitRoot) -> None:
    with tempfile.TemporaryDirectory(
        prefix=f"integrates_vulns_authors_{root.id}_",
        ignore_cleanup_errors=True,
    ) as tmpdir:
        os.chdir(tmpdir)
        repo_path = os.path.join(tmpdir, root.state.nickname)
        repo = await download_repo(
            root.group_name,
            root.state.nickname,
            tmpdir,
            root.state.gitignore,
        )
        if not repo:
            return

        os.chdir(repo_path)
        await update_root_vulns_author(root, EMAIL_INTEGRATES, repo)


async def process_group(group_name: str) -> None:
    loaders: Dataloaders = get_new_context()
    active_roots = await get_active_git_roots(loaders, group_name)
    if not active_roots:
        return

    await collect(
        [process_root(root) for root in active_roots],
        workers=4,
    )
    LOGGER.info("Processed group: %s", group_name)


async def main() -> None:
    loaders: Dataloaders = get_new_context()
    active_group_names = sorted(await get_all_active_group_names(loaders))
    LOGGER.info("Groups to process: %s", len(active_group_names))
    await collect(
        [process_group(group_name) for group_name in active_group_names],
        workers=4,
    )


if __name__ == "__main__":
    execution_time = time.strftime("Execution Time:    %Y-%m-%d at %H:%M:%S UTC")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S UTC")
    LOGGER.info("\n%s\n%s", execution_time, finalization_time)
