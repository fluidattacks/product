"""
Remove billing customer from organization

Execution Time: 2024-07-17 at 01:05:00
Finalization Time: 2024-07-17 at 01:05:01

Execution Time: 2024-07-23 at 20:14:29 UTC
Finalization Time: 2024-07-23 at 20:14:29 UTC

"""

import logging
import time

from aioextensions import (
    run,
)
from boto3.dynamodb.conditions import (
    Attr,
)

from integrates.dataloaders import (
    get_new_context,
)
from integrates.db_model import (
    TABLE,
)
from integrates.db_model.organizations.utils import (
    remove_org_id_prefix,
)
from integrates.dynamodb import (
    keys,
    operations,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)
LOGGER_CONSOLE = logging.getLogger("console")

ORGANIZATION_NAME = "org_name"


async def update_metadata(
    *,
    organization_id: str,
    organization_name: str,
) -> None:
    key_structure = TABLE.primary_key
    primary_key = keys.build_key(
        facet=TABLE.facets["organization_metadata"],
        values={
            "id": remove_org_id_prefix(organization_id),
            "name": organization_name,
        },
    )
    item = {"billing_customer": None, "billing_information": None}
    await operations.update_item(
        condition_expression=Attr(key_structure.partition_key).exists(),
        item=item,
        key=primary_key,
        table=TABLE,
    )
    LOGGER_CONSOLE.info("Done!")


async def main() -> None:
    loaders = get_new_context()
    organization = await loaders.organization.load(ORGANIZATION_NAME)

    if organization is None:
        LOGGER_CONSOLE.error("Organization not found")
        return

    await update_metadata(
        organization_id=organization.id,
        organization_name=organization.name,
    )


if __name__ == "__main__":
    execution_time = time.strftime("Execution Time:    %Y-%m-%d at %H:%M:%S UTC")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S UTC")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
