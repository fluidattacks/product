"""
Restore expired access

Execution Time:    2024-11-06 at 19:12:11 UTC
Finalization Time: 2024-11-06 at 19:13:06 UTC
Execution Time:    2024-11-06 at 20:04:46 UTC
Finalization Time: 2024-11-06 at 20:05:37 UTC
Execution Time:    2024-11-06 at 21:08:52 UTC
Finalization Time: 2024-11-06 at 21:09:39 UTC
Execution Time:    2025-01-16 at 20:49:55 UTC
Finalization Time: 2025-01-16 at 20:52:35 UTC
Execution Time:    2025-01-17 at 17:49:31 UTC
Finalization Time: 2025-01-17 at 17:49:38 UTC

"""

import time
from typing import cast

from aioextensions import (
    collect,
    run,
)
from boto3.dynamodb.conditions import (
    Key,
)

from integrates.custom_utils.groups import filter_active_groups
from integrates.dataloaders import (
    get_new_context,
)
from integrates.db_model import (
    TABLE as INTEGRATES_VMS_TABLE,
)
from integrates.db_model.group_access.types import GroupAccessRequest
from integrates.db_model.groups.enums import GroupStateStatus
from integrates.db_model.items import OrganizationAccessItem
from integrates.db_model.organization_access import update_metadata
from integrates.db_model.organization_access.types import (
    OrganizationAccess,
    OrganizationAccessMetadataToUpdate,
    OrganizationAccessRequest,
)
from integrates.db_model.organization_access.utils import (
    format_organization_access,
)
from integrates.db_model.stakeholders.get import (
    get_all_stakeholders,
)
from integrates.db_model.stakeholders.types import (
    Stakeholder,
)
from integrates.dynamodb import (
    keys,
    operations,
)

BACKUP_TABLE_NAME = ""
BACKUP_TABLE = INTEGRATES_VMS_TABLE._replace(name=BACKUP_TABLE_NAME)


async def get_stakeholder_organizations_access(
    *,
    email: str,
) -> list[OrganizationAccess]:
    email = email.lower().strip()
    primary_key = keys.build_key(
        facet=BACKUP_TABLE.facets["organization_access"],
        values={
            "email": email,
        },
    )

    key_structure = BACKUP_TABLE.primary_key
    response = await operations.query(
        condition_expression=(
            Key(key_structure.partition_key).eq(primary_key.partition_key)
            & Key(key_structure.sort_key).begins_with(primary_key.sort_key)
        ),
        facets=(BACKUP_TABLE.facets["organization_access"],),
        table=BACKUP_TABLE,
    )

    return [
        format_organization_access(cast(OrganizationAccessItem, item)) for item in response.items
    ]


async def get_stakeholder(stakeholder: Stakeholder) -> None | Stakeholder:
    loaders = get_new_context()
    found = False
    _groups = await loaders.stakeholder_groups_access.load(stakeholder.email)
    groups_access = [access.group_name for access in _groups if access.state.has_access]
    groups = await loaders.group.load_many(groups_access)
    orgs = {
        group.organization_id
        for group in groups
        if group and group.state.status == GroupStateStatus.ACTIVE
    }
    for org_access, _id in zip(
        await loaders.organization_access.load_many(
            OrganizationAccessRequest(email=stakeholder.email, organization_id=org_id)
            for org_id in orgs
        ),
        orgs,
        strict=False,
    ):
        if org_access is None and (await loaders.organization.load(_id)) is not None:
            found = True
    return stakeholder if found else None


async def process_stakeholder(stakeholder: Stakeholder) -> None:
    loaders = get_new_context()
    stakeholder_orgs = await get_stakeholder_organizations_access(email=stakeholder.email)
    organizations = await loaders.organization.load_many(
        [access.organization_id for access in stakeholder_orgs]
    )
    stakeholder_orgs = [
        access
        for access, organization in zip(stakeholder_orgs, organizations, strict=False)
        if organization
    ]
    new_stakeholder_orgs = await loaders.stakeholder_organizations_access.load(stakeholder.email)
    orgs_ids = {access.organization_id for access in new_stakeholder_orgs}
    expired = [
        access
        for access in stakeholder_orgs
        if access.state.invitation
        and access.expiration_time is not None
        and access.organization_id not in orgs_ids
    ]
    for access in expired:
        groups = await loaders.organization_groups.load(access.organization_id)
        active_groups = filter_active_groups(groups)
        if active_groups:
            group_access = await loaders.group_access.load_many(
                GroupAccessRequest(email=access.email, group_name=group.name)
                for group in active_groups
            )
            if list(filter(None, group_access)) and access.state.invitation:
                await update_metadata(
                    email=access.email,
                    organization_id=access.organization_id,
                    metadata=OrganizationAccessMetadataToUpdate(
                        expiration_time=0,
                        state=access.state._replace(
                            has_access=True,
                            role=access.state.invitation.role,
                            invitation=access.state.invitation._replace(is_used=True),
                        ),
                    ),
                )


async def main() -> None:
    all_stakeholders = await get_all_stakeholders()
    stakeholders = await collect(
        (get_stakeholder(stakeholder) for stakeholder in all_stakeholders),
        workers=16,
    )
    await collect(
        (process_stakeholder(stakeholder) for stakeholder in stakeholders if stakeholder),
        workers=4,
    )


if __name__ == "__main__":
    execution_time = time.strftime("Execution Time:    %Y-%m-%d at %H:%M:%S %Z")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S %Z")
    print("\n%s\n%s", execution_time, finalization_time)
