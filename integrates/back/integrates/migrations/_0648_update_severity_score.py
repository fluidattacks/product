"""
Execution Time:    2025-01-13 at 14:01:05 UTC
Finalization Time: 2025-01-13 at 14:04:23 UTC

Update severity_score in some vulnerabilities
"""

import logging
from datetime import datetime
from typing import Any

from aioextensions import collect
from snowflake.connector.cursor import SnowflakeCursor

from integrates.archive.client import setup_snowflake_connection, snowflake_db_cursor
from integrates.db_model.types import SeverityScore
from integrates.db_model.vulnerabilities.enums import VulnerabilityStateStatus
from integrates.migrations._0647_update_severity_score import get_vulnerability
from integrates.migrations.utils import log_time

LOGGER = logging.getLogger("migrations")


def get_vulns_missing_field(cursor: SnowflakeCursor) -> list[tuple[Any, ...]]:
    cursor.execute(
        """
            SELECT
                v.id, vs.status, vv.modified_date
            FROM
                integrates.vulnerabilities_metadata v
                LEFT JOIN integrates.vulnerabilities_verification vv ON vv.id = v.id
                LEFT JOIN integrates.vulnerabilities_state vs ON vs.id = v.id
            WHERE
                vv.status = '{verification_status}'
                AND v.severity_cvss_v4 is NULL
                AND vv.modified_date > '{min_date}'
                AND vv.modified_date < '{max_date}'
                AND vs.status = '{state_status}'
            ORDER BY
                vv.modified_date DESC;
        """.format(
            verification_status="VERIFIED",
            min_date=datetime.fromisoformat("2024-03-15T00:00:00+00:00"),
            max_date=datetime.fromisoformat("2025-01-09T00:00:00+00:00"),
            state_status="SAFE",
        )
    )

    return list(cursor.fetchall())  # type: ignore


def _update_vulnerability(cursor: SnowflakeCursor, _id: str, severity_score: SeverityScore) -> None:
    cursor.execute(
        """
            UPDATE
                integrates.vulnerabilities_metadata
            SET
                severity_cvss_v3 = '{cvss_v3}',
                severity_cvss_v4 = '{cvss_v4}',
                severity_temporal_score = '{temporal_score}',
                severity_threat_score = '{threat_score}'
            WHERE
                id = '{id}';
        """.format(  # noqa: UP032
            threat_score=severity_score.threat_score,
            temporal_score=severity_score.temporal_score,
            cvss_v3=severity_score.cvss_v3,
            cvss_v4=severity_score.cvss_v4,
            id=_id,
        )
    )


async def update_vulnerability(
    *, cursor: SnowflakeCursor, vulnerability: tuple[str, str], backup_name: str
) -> None:
    if backup_name == "2024-02":
        return None

    _vulnerability = await get_vulnerability(
        vulnerability_id=vulnerability[0], backup_name=backup_name
    )
    if _vulnerability is None or _vulnerability.state.status in [
        VulnerabilityStateStatus.DELETED,
        VulnerabilityStateStatus.SAFE,
    ]:
        return await update_vulnerability(
            cursor=cursor,
            vulnerability=vulnerability,
            backup_name=f"{backup_name[:-1]}{int(backup_name[-1]) - 1}",
        )
    if _vulnerability.severity_score is not None:
        return _update_vulnerability(cursor, _vulnerability.id, _vulnerability.severity_score)


@log_time(LOGGER)
async def main() -> None:
    with snowflake_db_cursor() as cursor:
        setup_snowflake_connection(cursor)
        _vulns = get_vulns_missing_field(cursor)
        vulns_ids = set()
        vulns = []
        for vuln in _vulns:
            if vuln[0] not in vulns_ids:
                vulns_ids.add(vuln[0])
                vulns.append((vuln[0], vuln[2].isoformat()[:7]))
        await collect(
            [
                update_vulnerability(cursor=cursor, vulnerability=vuln, backup_name=vuln[1])
                for vuln in vulns
            ],
            workers=8,
        )


if __name__ == "__main__":
    main()  # type: ignore[unused-coroutine]
