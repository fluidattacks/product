"""
Identify vulnerabilities with inconsistent status between the
vulnerability and its latest historic state.

Execution Time:    2024-11-14 at 15:02:08 UTC
Finalization Time: 2024-11-14 at 17:03:31 UTC

Execution Time:    2024-11-22 at 15:58:34 UTC
Finalization Time: 2024-11-22 at 19:19:43 UTC
"""

import csv
import logging
import time
from io import (
    BytesIO,
    StringIO,
)
from itertools import (
    chain,
)
from typing import (
    Literal,
)

import aiofiles
from aioextensions import (
    collect,
    run,
)
from boto3.dynamodb.conditions import (
    Key,
)

from integrates.context import (
    FI_ENVIRONMENT,
)
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model import (
    TABLE,
)
from integrates.db_model.items import VulnerabilityStateItem
from integrates.db_model.organizations.get import (
    get_all_organizations,
)
from integrates.db_model.vulnerabilities.get import (
    _get_historic_state,
)
from integrates.db_model.vulnerabilities.types import (
    GroupVulnerabilitiesRequest,
    Vulnerability,
    VulnerabilityRequest,
    VulnerabilityState,
)
from integrates.db_model.vulnerabilities.utils import (
    format_state,
)
from integrates.dynamodb import (
    keys,
    operations,
)
from integrates.s3.resource import (
    get_client,
)

StorageTarget = Literal["local", "s3"]


LOGGER = logging.getLogger("console")
TARGET_BUCKET = "integrates.dev"
S3_FOLDER = "vuln_inconsistencies"


async def _save_to_target(content: bytes, filename: str, target: StorageTarget) -> None:
    match target:
        case "s3":
            client = await get_client()
            await client.upload_fileobj(
                BytesIO(content),
                TARGET_BUCKET,
                f"{S3_FOLDER}/{filename}",
            )
        case "local":
            async with aiofiles.open(filename, "w", encoding="utf-8") as csv_file:
                await csv_file.write(content.decode("utf-8"))


async def _save_group_vulns_data(
    vulns_data: dict[str, dict[str, str]],
    group_name: str,
    target: StorageTarget,
) -> None:
    buffer = StringIO()
    headers = list(list(vulns_data.values())[0].keys())
    csv_writer = csv.DictWriter(buffer, fieldnames=["vuln_id"] + headers)
    csv_writer.writeheader()
    for vuln_id, vuln_data in vulns_data.items():
        csv_writer.writerow({"vuln_id": vuln_id, **vuln_data})
    filename = f"vulns_inconsistent_status_{group_name}.csv"
    await _save_to_target(
        content=buffer.getvalue().encode("utf-8"),
        filename=filename,
        target=target,
    )


async def _save_summary_data(data: dict[str, tuple[int, int]], target: StorageTarget) -> None:
    buffer = StringIO()
    csv_writer = csv.DictWriter(buffer, fieldnames=["group", "vulns", "inconsistencies"])
    csv_writer.writeheader()
    for key, value in data.items():
        csv_writer.writerow({"group": key, "vulns": value[0], "inconsistencies": value[1]})
    filename = "vulns_inconsistencies_summary.csv"
    await _save_to_target(
        content=buffer.getvalue().encode("utf-8"),
        filename=filename,
        target=target,
    )


async def _get_vuln_states_vms(
    vuln_id: str,
) -> list[VulnerabilityState]:
    primary_key = keys.build_key(
        facet=TABLE.facets["vulnerability_historic_state"],
        values={"id": vuln_id},
    )
    key_structure = TABLE.primary_key
    response = await operations.DynamoClient[VulnerabilityStateItem].query(
        condition_expression=(
            Key(key_structure.partition_key).eq(primary_key.partition_key)
            & Key(key_structure.sort_key).begins_with(primary_key.sort_key)
        ),
        facets=(TABLE.facets["vulnerability_historic_state"],),
        table=TABLE,
    )

    return list(map(format_state, response.items))


async def _get_vuln_states_from_vms_table(
    vulns: list[Vulnerability],
) -> list[list[VulnerabilityState]]:
    return list(await collect([_get_vuln_states_vms(vuln.id) for vuln in vulns], workers=8))


async def _get_vuln_states_from_historic_table(
    vulns: list[Vulnerability],
) -> list[list[VulnerabilityState]]:
    return list(
        await collect(
            tuple(
                _get_historic_state(
                    request=VulnerabilityRequest(
                        finding_id=vuln.finding_id,
                        vulnerability_id=vuln.id,
                    ),
                )
                for vuln in vulns
            ),
            workers=8,
        ),
    )


def _find_inconsistent_data(
    vulns: list[Vulnerability],
    *,
    vuln_historic_states: list[list[VulnerabilityState]],
    vuln_states: list[list[VulnerabilityState]],
) -> dict[str, dict[str, str]]:
    return {
        vuln.id: {
            "state": vuln.state.status,
            "last_state[integrates_vms_historic]": historic_states[-1].status
            if historic_states
            else "N/A",
            "last_state[integrates_vms]": states[-1].status if states else "N/A",
            "last_modification": vuln.state.modified_date.isoformat(),
            "last_modification[integrates_vms_historic]": historic_states[
                -1
            ].modified_date.isoformat()
            if historic_states
            else "N/A",
            "last_modification[integrates_vms]": states[-1].modified_date.isoformat()
            if states
            else "N/A",
            "total_states[integrates_vms_historic]": str(len(historic_states)),
            "total_states[integrates_vms]": str(len(states)),
        }
        for vuln, historic_states, states in zip(
            vulns,
            vuln_historic_states,
            vuln_states,
            strict=False,
        )
        if not historic_states or not states or vuln.state.status != historic_states[-1].status
    }


async def _process_group_inconsistencies(
    loaders: Dataloaders,
    group_name: str,
    target: StorageTarget,
) -> tuple[int, int]:
    vulns_conn = await loaders.group_vulnerabilities.load(
        GroupVulnerabilitiesRequest(group_name=group_name),
    )
    vulns = [edge.node for edge in vulns_conn.edges]
    if not vulns:
        LOGGER.info("Found no vulns for group %s", group_name)
        return 0, 0

    states_integrates_vms_historic, states_integrates_vms = await collect(
        [
            _get_vuln_states_from_historic_table(vulns),
            _get_vuln_states_from_vms_table(vulns),
        ],
        workers=2,
    )
    inconsistencies = _find_inconsistent_data(
        vulns,
        vuln_historic_states=states_integrates_vms_historic,
        vuln_states=states_integrates_vms,
    )
    LOGGER.info(
        "Found %d/%d vulns with inconsistent states for group %s",
        len(inconsistencies),
        len(vulns),
        group_name,
    )

    if inconsistencies:
        await _save_group_vulns_data(
            vulns_data=inconsistencies,
            group_name=group_name,
            target=target,
        )
    return len(vulns), len(inconsistencies)


async def main() -> None:
    orgs = await get_all_organizations()
    loaders = get_new_context()
    groups = list(
        chain.from_iterable(
            await loaders.organization_groups.batch_load_fn(org.id for org in orgs),
        ),
    )
    LOGGER.info("Total groups: %s", len(groups))

    # Does not require a thread lock since accessed key is unique per worker
    results: dict[str, tuple[int, int]] = {}

    storage_target: StorageTarget = "s3" if FI_ENVIRONMENT == "production" else "local"

    async def _populate_group_result(group_name: str) -> None:
        results[group_name] = await _process_group_inconsistencies(
            loaders=loaders,
            group_name=group_name,
            target=storage_target,
        )

    await collect([_populate_group_result(group.name) for group in groups], workers=2)

    await _save_summary_data(results, target=storage_target)


if __name__ == "__main__":
    execution_time = time.strftime("Execution Time:    %Y-%m-%d at %H:%M:%S %Z")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S %Z")
    LOGGER.info("\n%s\n%s", execution_time, finalization_time)
