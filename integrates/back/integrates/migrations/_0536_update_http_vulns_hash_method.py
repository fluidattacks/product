"""
Migration to update http open machine vulnerabilities whose method has
been modified by recent method splits.
- Update the method associated with a vuln according to its unmodified
  specific.
- Update the hash based on this change

Execution Time:    2024-05-21 at 00:35:00 UTC
Finalization Time: 2024-05-21 at 00:48:00 UTC

"""

import csv
import hashlib
import logging
import logging.config
import time

from aioextensions import (
    collect,
    run,
)

from integrates.custom_utils import (
    datetime as datetime_utils,
)
from integrates.custom_utils.vulnerabilities import (
    get_path_from_integrates_vulnerability,
)
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model.enums import (
    Source,
)
from integrates.db_model.findings.types import (
    Finding,
)
from integrates.db_model.vulnerabilities.enums import (
    VulnerabilityStateReason,
    VulnerabilityStateStatus,
    VulnerabilityType,
)
from integrates.db_model.vulnerabilities.types import (
    Vulnerability,
    VulnerabilityMetadataToUpdate,
    VulnerabilityState,
)
from integrates.db_model.vulnerabilities.update import (
    update_historic_entry,
    update_metadata,
)
from integrates.organizations.domain import (
    get_all_active_groups,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)
LOGGER_CONSOLE = logging.getLogger("console")

# This dictionary maps descriptions with new method_names

DESC_METHOD_MAPPER = {
    # F043 coming from CONTENT_SECURITY_POLICY
    "Missing object-src attribute": "analyze_headers.content_security_policy",
    "Atributo object-src ausente": "analyze_headers.content_security_policy",
    "Missing frame-ancestors header": "analyze_headers.cont_sec_pol_frame_" "ancestors",
    "Encabezado frame-ancestors ausente": "analyze_headers.cont_sec_pol_frame_" "ancestors",
    "Missing script-src attribute": "analyze_headers.cont_sec_pol_missing_" "script",
    "Atributo script-src ausente": "analyze_headers.cont_sec_pol_missing_" "script",
    "script-src has unsafe-inline attribute": "analyze_headers.cont_sec_pol_" "unsafe_line",
    "script-src contiene el atributo unsafe-inline": "analyze_headers.cont_sec" "_pol_unsafe_line",
    "Use of deprecated block-all-mixed-content": "analyze_headers.cont_sec_pol" "_mixed_content",
    "Uso del deprecado block-all-mixed-content": "analyze_headers.cont_sec_pol" "_mixed_content",
    "hosts JSONP": "analyze_headers.cont_sec_pol_hosts_jsonp",
    "aloja JSONP": "analyze_headers.cont_sec_pol_hosts_jsonp",
    "contains the unsafe wildcard": "analyze_headers.cont_sec_pol_wild_uri",
    "contiene el comodín inseguro": "analyze_headers.cont_sec_pol_wild_uri",
    # F071 coming from REFERRER_POLICY
    "Missing Referrer-Policy header": "analyze_headers.missing_referrer_" "policy",
    "La cabecera Referrer-Policy está ausente": "analyze_headers.missing_" "referrer_policy",
    # F131 coming from STRICT_TRANSPORT_SECURITY
    "max-age is less than 31536000": "analyze_headers.strict_transport_low_" "max_age",
    "max-age es menor que 31536000": "analyze_headers.strict_transport_low_" "max_age",
    "Make sure the include_sub_domains directive is enabled": "analyze_"
    "headers.strict_transport_include_subdomains",
    "Asegúrese de que la directiva include_sub_domains este activada": "analyz"
    "e_headers.strict_transport_include_subdomains",
    # F132 coming from X_CONTENT_TYPE_OPTIONS
    "Value is not nosniff": "analyze_headers.x_content_type_options_nosniff",
    "Valor diferente a nosniff": "analyze_headers.x_content_type_options_" "nosniff",
}


async def get_http_vulns(
    loaders: Dataloaders,
    findings: list[Finding],
    fin_code: str,
) -> list[Vulnerability]:
    vulns = await loaders.finding_vulnerabilities.load_many_chained(
        [fin.id for fin in findings if fin.title.startswith(fin_code)],
    )
    return [
        vuln
        for vuln in vulns
        if vuln.state.status == VulnerabilityStateStatus.VULNERABLE
        and (vuln.state.source == Source.MACHINE or vuln.hacker_email == "machine@fluidattacks.com")
        and vuln.skims_method is not None
        and vuln.type == VulnerabilityType.INPUTS
        and "analyze_headers" in vuln.skims_method
    ]


async def process_vuln(
    vuln: Vulnerability,
    fin_code: str,
) -> tuple[int, VulnerabilityState, str, str] | None:
    _, where = get_path_from_integrates_vulnerability(
        vuln.state.where,
        VulnerabilityType.INPUTS,
    )
    old_method = str(vuln.skims_method)
    new_method = str(vuln.skims_method)
    specific = vuln.state.specific

    for desc, method in DESC_METHOD_MAPPER.items():
        if desc.lower() in specific.lower():
            new_method = method

    new_hash = int.from_bytes(
        hashlib.sha256(
            bytes(
                (where + specific + fin_code + new_method),
                encoding="utf-8",
            ),
        ).digest()[:8],
        "little",
    )

    if new_method == old_method:
        return None

    new_state = vuln.state._replace(
        modified_date=datetime_utils.get_utc_now(),
        reasons=[VulnerabilityStateReason.CONSISTENCY],
        modified_by="lpatino@fluidattacks.com",
    )

    return new_hash, new_state, new_method, old_method


async def process_vulns(
    http_vulns: list[Vulnerability],
    fin_code: str,
) -> tuple[list, list]:
    futures = []
    rows = []
    for vuln in http_vulns:
        vuln_new_attrs = await process_vuln(vuln, fin_code)

        if not vuln_new_attrs:
            continue

        (new_hash, new_state, new_method, old_method) = vuln_new_attrs
        futures.append(
            update_historic_entry(
                current_value=vuln,
                finding_id=vuln.finding_id,
                entry=new_state,
                vulnerability_id=vuln.id,
            ),
        )

        futures.append(
            update_metadata(
                finding_id=vuln.finding_id,
                metadata=VulnerabilityMetadataToUpdate(hash=new_hash, skims_method=new_method),
                root_id=vuln.root_id,
                vulnerability_id=vuln.id,
            ),
        )

        rows.extend(
            [
                [
                    vuln.group_name,
                    fin_code,
                    vuln.id,
                    old_method,
                    new_method,
                ],
            ],
        )
    return futures, rows


async def adjust_group_http_reports(
    loaders: Dataloaders,
    group: str,
    searched_fins: tuple[str, ...],
) -> None:
    LOGGER_CONSOLE.info("Processing %s", group)
    findings = await loaders.group_findings.load(group)
    all_futures = []
    all_rows = []
    for fin_code in searched_fins:
        http_vulns = await get_http_vulns(loaders, findings, fin_code)
        futures, rows = await process_vulns(http_vulns, fin_code)
        all_futures.extend(futures)
        all_rows.extend(rows)

    await collect(all_futures, workers=16)

    with open("http_vulns_migration.csv", "a+", encoding="utf-8") as handler:
        writer = csv.writer(handler)
        writer.writerows(all_rows)


async def main() -> None:
    searched_fins = ("043", "071", "131", "132")
    loaders = get_new_context()
    active_groups = await get_all_active_groups(loaders)
    groups = sorted([group.name for group in active_groups])

    for group in groups:
        await adjust_group_http_reports(
            loaders,
            group,
            searched_fins,
        )


if __name__ == "__main__":
    execution_time = time.strftime("Execution Time:    %Y-%m-%d at %H:%M:%S UTC")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S UTC")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
