"""
Add a forces user to the group if the user does not exist.

Execution Time:    2023-09-07 at 14:51:03 UTC
Finalization Time: 2023-09-07 at 14:54:16 UTC
"""

import logging
import logging.config
import time

from aioextensions import (
    run,
)

from integrates.dataloaders import (
    get_new_context,
)
from integrates.db_model.group_access.types import (
    GroupStakeholdersAccessRequest,
)
from integrates.forces.utils import (
    format_forces_email,
)
from integrates.groups import (
    domain as groups_domain,
)
from integrates.organizations import (
    domain as orgs_domain,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)
LOGGER_CONSOLE = logging.getLogger("console")
EMAIL_INTEGRATES = "integrates@fluidattacks.com"


async def process_group(
    *,
    group_name: str,
    progress: float,
) -> None:
    loaders = get_new_context()
    stakeholders_access = await loaders.group_stakeholders_access.load(
        GroupStakeholdersAccessRequest(group_name=group_name),
    )
    forces_user_email = format_forces_email(group_name)
    has_forces_user = bool(
        [
            stakeholder_access
            for stakeholder_access in stakeholders_access
            if stakeholder_access.email == forces_user_email
        ],
    )
    if not has_forces_user:
        await groups_domain.add_forces_stakeholder(
            loaders=loaders,
            group_name=group_name,
            modified_by=EMAIL_INTEGRATES,
        )

    LOGGER_CONSOLE.info("Group processed %s %s", group_name, f"{round(progress, 2)!s}")


async def main() -> None:
    loaders = get_new_context()
    group_names = sorted(await orgs_domain.get_all_active_group_names(loaders))
    LOGGER_CONSOLE.info("%s", f"{group_names=}")
    LOGGER_CONSOLE.info("%s", f"{len(group_names)=}")
    for count, group_name in enumerate(group_names):
        LOGGER_CONSOLE.info("Group %s %s", group_name, str(count))
        await process_group(
            group_name=group_name,
            progress=count / len(group_names),
        )


if __name__ == "__main__":
    execution_time = time.strftime("Execution Time:    %Y-%m-%d at %H:%M:%S UTC")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S UTC")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
