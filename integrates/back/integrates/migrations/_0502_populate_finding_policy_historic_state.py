"""
Populate finding policy historic state in integrates_vms_historic table.

Start Time: 2024-02-29 at 19:36:28 UTC
Finalization Time: 2024-02-29 at 19:36:32 UTC
"""

import asyncio
import logging
import logging.config
import time

from aioextensions import (
    collect,
    run,
)
from aiohttp.client_exceptions import (
    ClientConnectorError,
    ClientOSError,
    ClientPayloadError,
    ServerDisconnectedError,
    ServerTimeoutError,
)
from boto3.dynamodb.conditions import (
    Key,
)
from botocore.exceptions import (
    ClientError,
    ConnectTimeoutError,
    HTTPClientError,
    ReadTimeoutError,
)
from more_itertools import (
    chunked,
)

from integrates.class_types.types import (
    Item,
)
from integrates.db_model import (
    HISTORIC_TABLE,
    TABLE,
)
from integrates.db_model.utils import (
    get_historic_gsi_sk,
)
from integrates.decorators import (
    retry_on_exceptions,
)
from integrates.dynamodb import (
    keys,
    operations,
)
from integrates.dynamodb.exceptions import (
    UnavailabilityError,
)
from integrates.dynamodb.types import (
    PrimaryKey,
)
from integrates.organizations import (
    domain as orgs_domain,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)
LOGGER_CONSOLE = logging.getLogger("console")
NETWORK_ERRORS = (
    asyncio.TimeoutError,
    ClientConnectorError,
    ClientError,
    ClientOSError,
    ClientPayloadError,
    ConnectionError,
    ConnectionResetError,
    ConnectTimeoutError,
    HTTPClientError,
    ReadTimeoutError,
    ServerDisconnectedError,
    ServerTimeoutError,
    UnavailabilityError,
)


batch_put_item = retry_on_exceptions(
    exceptions=NETWORK_ERRORS,
    sleep_seconds=300,
)(operations.batch_put_item)
batch_delete_item = retry_on_exceptions(
    exceptions=NETWORK_ERRORS,
    sleep_seconds=300,
)(operations.batch_delete_item)


@retry_on_exceptions(
    exceptions=NETWORK_ERRORS,
    sleep_seconds=10,
)
async def _get_finding_policies_by_org(
    organization_name: str,
) -> tuple[Item, ...]:
    primary_key = keys.build_key(
        facet=TABLE.facets["org_finding_policy_metadata"],
        values={
            "name": organization_name,
        },
    )

    index = TABLE.indexes["inverted_index"]
    key_structure = index.primary_key
    response = await operations.query(
        condition_expression=(
            Key(key_structure.partition_key).eq(primary_key.sort_key)
            & Key(key_structure.sort_key).begins_with(primary_key.partition_key)
        ),
        facets=(TABLE.facets["org_finding_policy_metadata"],),
        table=TABLE,
        index=index,
    )
    return response.items


@retry_on_exceptions(
    exceptions=NETWORK_ERRORS,
    sleep_seconds=10,
)
async def _get_new_historic_state(
    finding_policy_uuid: str,
    organization_name: str,
) -> tuple[Item, ...]:
    primary_key = keys.build_key(
        facet=HISTORIC_TABLE.facets["org_finding_policy_state"],
        values={
            "uuid": finding_policy_uuid,
            "name": organization_name,
            "state": "state",
        },
    )
    key_structure = HISTORIC_TABLE.primary_key
    response = await operations.query(
        condition_expression=(
            Key(key_structure.partition_key).eq(primary_key.partition_key)
            & Key(key_structure.sort_key).begins_with(primary_key.sort_key)
        ),
        facets=(HISTORIC_TABLE.facets["org_finding_policy_state"],),
        table=HISTORIC_TABLE,
    )
    return response.items


@retry_on_exceptions(
    exceptions=NETWORK_ERRORS,
    sleep_seconds=10,
)
async def _get_historic_state(
    finding_policy_uuid: str,
) -> tuple[Item, ...]:
    primary_key = keys.build_key(
        facet=TABLE.facets["org_finding_policy_historic_state"],
        values={"uuid": finding_policy_uuid},
    )
    key_structure = TABLE.primary_key
    response = await operations.query(
        condition_expression=(
            Key(key_structure.partition_key).eq(primary_key.partition_key)
            & Key(key_structure.sort_key).begins_with(primary_key.sort_key)
        ),
        facets=(TABLE.facets["org_finding_policy_historic_state"],),
        table=TABLE,
    )
    return response.items


def format_state_item(state_item: Item, finding_item: Item) -> Item:
    item_pk = f"{finding_item['pk']}#{finding_item['sk']}"
    item_sk = get_historic_gsi_sk("state", state_item["modified_date"])
    return {
        **(
            finding_item["state"]
            if finding_item.get("state")
            and state_item["modified_date"] == finding_item["state"]["modified_date"]
            else state_item
        ),
        "pk": item_pk,
        "sk": item_sk,
    }


async def process_finding_policy(
    finding_policy_item: Item,
    organization_name: str,
) -> None:
    finding_policy_uuid = finding_policy_item["pk"].split("#")[1]
    new_historic_state = await _get_new_historic_state(
        finding_policy_uuid=finding_policy_uuid,
        organization_name=organization_name,
    )
    new_primary_keys = tuple(
        PrimaryKey(partition_key=item["pk"], sort_key=item["sk"]) for item in new_historic_state
    )
    for chunked_keys in chunked(new_primary_keys, 25):
        await batch_delete_item(
            keys=tuple(chunked_keys),
            table=HISTORIC_TABLE,
        )
    finding_historic_state: tuple[Item, ...] = await _get_historic_state(
        finding_policy_uuid=finding_policy_uuid,
    )
    if not finding_historic_state and finding_policy_item.get("state"):
        finding_historic_state = (finding_policy_item["state"],)

    formatted_items = tuple(
        format_state_item(historic_item, finding_policy_item)
        for historic_item in finding_historic_state
    )
    for chunked_formatted_items in chunked(formatted_items, 25):
        await batch_put_item(
            items=tuple(chunked_formatted_items),
            table=HISTORIC_TABLE,
        )


async def process_organization(org_id: str, org_name: str) -> None:
    finding_policies = await _get_finding_policies_by_org(organization_name=org_name)
    await collect(
        tuple(
            process_finding_policy(finding_policy_item, org_name)
            for finding_policy_item in finding_policies
        ),
        workers=64,
    )
    LOGGER_CONSOLE.info(
        "Organization processed",
        extra={
            "extra": {
                "org_id": org_id,
                "org_name": org_name,
                "finding_policies": len(finding_policies),
            },
        },
    )


async def main() -> None:
    organizations = sorted(
        [organization async for organization in orgs_domain.iterate_organizations()],
        key=lambda org: org.name,
    )
    LOGGER_CONSOLE.info(
        "All organizations",
        extra={
            "extra": {
                "total": len(organizations),
            },
        },
    )
    for count, organization in enumerate(organizations, start=1):
        LOGGER_CONSOLE.info(
            "Organization",
            extra={
                "extra": {
                    "org_id": organization.id,
                    "org_name": organization.name,
                    "count": count,
                },
            },
        )
        await process_organization(organization.id, organization.name)


if __name__ == "__main__":
    execution_time = time.strftime("Start Time:        %Y-%m-%d at %H:%M:%S UTC")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S UTC")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
