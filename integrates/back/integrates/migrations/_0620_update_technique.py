"""
Review vulnerability technique field to restore CSPM values.

Execution Time:    2024-10-31 at 00:44:00 UTC
Finalization Time: 2024-10-31 at 01:32:40 UTC
"""

import logging
import logging.config
import time

from aioextensions import (
    collect,
    run,
)

from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model import (
    vulnerabilities as vulns_model,
)
from integrates.db_model.vulnerabilities.enums import (
    VulnerabilityTechnique,
)
from integrates.db_model.vulnerabilities.types import (
    Vulnerability,
    VulnerabilityMetadataToUpdate,
)
from integrates.organizations.domain import (
    get_all_group_names,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)
LOGGER_CONSOLE = logging.getLogger("console")


async def _update_vulnerability(vulnerability: Vulnerability) -> None:
    await vulns_model.update_metadata(
        finding_id=vulnerability.finding_id,
        vulnerability_id=vulnerability.id,
        metadata=VulnerabilityMetadataToUpdate(technique=VulnerabilityTechnique.CSPM),
    )
    LOGGER_CONSOLE.info(
        "Updated vulnerability %s from %s",
        vulnerability.id,
        vulnerability.group_name,
    )


async def process_group(
    loaders: Dataloaders,
    group_name: str,
) -> None:
    group_findings = await loaders.group_findings.load(group_name)

    LOGGER_CONSOLE.info("Group to process  %s", group_name)

    vulns = await loaders.finding_vulnerabilities.load_many_chained(
        [fin.id for fin in group_findings],
    )

    await collect(
        tuple(
            _update_vulnerability(vuln)
            for vuln in vulns
            if vuln.technique
            and vuln.technique == VulnerabilityTechnique.PTAAS
            and vuln.hacker_email == "machine@fluidattacks.com"
        ),
        workers=20,
    )

    LOGGER_CONSOLE.info("Group processed  %s", group_name)


async def main() -> None:
    loaders: Dataloaders = get_new_context()
    groups = sorted(await get_all_group_names(loaders))
    await collect([process_group(loaders, group) for group in groups], workers=8)


if __name__ == "__main__":
    execution_time = time.strftime("Execution Time:    %Y-%m-%d at %H:%M:%S UTC")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S UTC")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
