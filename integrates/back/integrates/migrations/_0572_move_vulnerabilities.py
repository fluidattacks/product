"""
move vulnerabilities, requested on:
https://gitlab.com/fluidattacks/universe/-/issues/12658
Execution Time:    2024-07-31 at 16:17:43 UTC
Finalization Time: 2024-07-31 at 16:17:46 UTC
"""

import logging
import logging.config
import time
import uuid
from contextlib import (
    suppress,
)
from datetime import (
    datetime,
)
from io import (
    BytesIO,
)
from typing import (
    Literal,
    TypedDict,
)

import aiofiles
from aioextensions import (
    collect,
    run,
)
from botocore.exceptions import (
    ConnectTimeoutError,
    ReadTimeoutError,
)
from starlette.datastructures import (
    UploadFile,
)

from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model import (
    finding_comments as finding_comments_model,
)
from integrates.db_model import (
    vulnerabilities as vulns_model,
)
from integrates.db_model.finding_comments.enums import (
    CommentType,
)
from integrates.db_model.finding_comments.types import (
    FindingComment,
    FindingCommentsRequest,
)
from integrates.db_model.findings.add import (
    add,
)
from integrates.db_model.findings.types import (
    Finding,
    FindingEvidence,
)
from integrates.db_model.vulnerabilities.enums import (
    VulnerabilityStateStatus,
)
from integrates.db_model.vulnerabilities.types import (
    Vulnerability,
    VulnerabilityRequest,
)
from integrates.decorators import (
    retry_on_exceptions,
)
from integrates.findings.domain.evidence import (
    EVIDENCE_NAMES,
    download_evidence_file,
    update_evidence,
)
from integrates.findings.domain.utils import (
    _format_vulnerabilities_nickname,
    _get_vuln_nickname,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)
LOGGER = logging.getLogger(__name__)
LOGGER_CONSOLE = logging.getLogger("console")

MODIFIED_BY = "integrates@fluidattacks.com"
SOURCE_GROUP = "source"
TARGET_GROUP = "target"
TARGET_ROOT_ID = "target_root_id"


class MigrationInfo(TypedDict):
    operation: Literal["Move Vulnerabilities", "Move Finding"]
    vulnerabilities: list[str] | Literal["All Vulnerable"]
    target_finding_id: str | None
    source_finding_id: str


DATA: list[MigrationInfo] = []


@retry_on_exceptions(
    exceptions=(ReadTimeoutError, ConnectTimeoutError),
    sleep_seconds=3,
)
async def process_finding_evidence(
    *,
    loaders: Dataloaders,
    target_finding: Finding,
    source_finding: Finding,
) -> None:
    for evidence_id, evidence_name in EVIDENCE_NAMES.items():
        evidence: FindingEvidence | None = getattr(source_finding.evidences, evidence_name)
        if not evidence:
            continue
        loaders.finding.clear(target_finding.id)
        with suppress(Exception):
            file_path = await download_evidence_file(
                source_finding.group_name,
                source_finding.id,
                evidence.url,
            )
            async with aiofiles.open(file_path, "rb") as new_file:
                file_contents = await new_file.read()
                await update_evidence(
                    loaders=loaders,
                    finding_id=target_finding.id,
                    evidence_id=evidence_id,
                    file_object=UploadFile(
                        filename=new_file.name,  # type: ignore[arg-type]
                        file=BytesIO(file_contents),
                    ),
                    author_email=evidence.author_email,
                    is_draft=evidence.is_draft,
                    description=evidence.description,
                )
    LOGGER_CONSOLE.info("Evidence processed on %s", source_finding.title)


@retry_on_exceptions(
    exceptions=(ReadTimeoutError, ConnectTimeoutError),
    sleep_seconds=3,
)
async def _process_finding_comments(
    *,
    loaders: Dataloaders,
    source_finding_id: str,
    target_finding_id: str,
) -> None:
    comments = await loaders.finding_comments.load(
        FindingCommentsRequest(
            comment_type=CommentType.COMMENT,
            finding_id=source_finding_id,
        ),
    )

    new_comment = FindingComment(
        comment_type=CommentType.OBSERVATION,
        content=(f"This finding was cloned from {SOURCE_GROUP} under client request"),
        creation_date=datetime.now(),
        email=MODIFIED_BY,
        finding_id=target_finding_id,
        id=str(round(time.time() * 1000)),
        parent_id="0",
    )

    comments.append(new_comment)

    await collect(
        tuple(
            finding_comments_model.add(
                finding_comment=comment._replace(finding_id=target_finding_id),
            )
            for comment in comments
        ),
    )

    LOGGER_CONSOLE.info(
        "Comments processed",
        extra={
            "extra": {
                "comments": len(comments),
            },
        },
    )


@retry_on_exceptions(
    exceptions=(ReadTimeoutError, ConnectTimeoutError),
    sleep_seconds=3,
)
async def _move_vulnerability(
    *,
    loaders: Dataloaders,
    target_finding_id: str,
    vulnerability: Vulnerability,
) -> Vulnerability:
    LOGGER_CONSOLE.info(
        "Processing vulnerability",
        extra={
            "extra": {
                "vulnerability": vulnerability.id,
            },
        },
    )

    new_vulnerability = vulnerability._replace(
        id=str(uuid.uuid4()),
        finding_id=target_finding_id,
        group_name=TARGET_GROUP,
        root_id=TARGET_ROOT_ID,
    )

    await vulns_model.add(vulnerability=new_vulnerability)

    vulnerability_request = VulnerabilityRequest(
        finding_id=vulnerability.finding_id,
        vulnerability_id=vulnerability.id,
    )

    historic_state = await loaders.vulnerability_historic_state.load(vulnerability_request)
    historic_treatment = await loaders.vulnerability_historic_treatment.load(vulnerability_request)
    historic_verification = await loaders.vulnerability_historic_verification.load(
        vulnerability_request,
    )
    historic_zero_risk = await loaders.vulnerability_historic_zero_risk.load(vulnerability_request)

    await vulns_model.update_historic(
        current_value=new_vulnerability,
        historic=tuple(historic_state) or (vulnerability.state,),
    )
    if historic_treatment:
        await vulns_model.update_historic(
            current_value=new_vulnerability,
            historic=tuple(historic_treatment),
        )
    if historic_verification:
        await vulns_model.update_historic(
            current_value=new_vulnerability,
            historic=tuple(historic_verification),
        )
    if historic_zero_risk:
        await vulns_model.update_historic(
            current_value=new_vulnerability,
            historic=tuple(historic_zero_risk),
        )

    LOGGER_CONSOLE.info(
        "Vulnerability processed",
        extra={
            "extra": {
                "old_vulnerability": vulnerability.id,
                "new_vulnerability": new_vulnerability.id,
                "target_finding": target_finding_id,
            },
        },
    )

    return new_vulnerability


async def _move_finding(
    *,
    loaders: Dataloaders,
    source_finding: Finding,
) -> tuple[str, list[Vulnerability]]:
    target_finding = source_finding._replace(
        group_name=TARGET_GROUP,
        id=str(uuid.uuid4()),
    )
    LOGGER_CONSOLE.info(
        "Processing finding",
        extra={
            "extra": {
                "source_finding": source_finding.id,
                "target_finding": target_finding.id,
            },
        },
    )
    await add(finding=target_finding)
    vulnerabilities = await loaders.finding_vulnerabilities_all.load(source_finding.id)

    await process_finding_evidence(
        loaders=loaders,
        source_finding=source_finding,
        target_finding=target_finding,
    )
    await _process_finding_comments(
        loaders=loaders,
        target_finding_id=target_finding.id,
        source_finding_id=source_finding.id,
    )

    return target_finding.id, vulnerabilities


async def _process_data(
    *,
    operation_info: MigrationInfo,
) -> None:
    loaders = get_new_context()
    target_finding_id = operation_info["target_finding_id"]

    if operation_info["operation"] == "Move Finding":
        source_finding = await loaders.finding.load(operation_info["source_finding_id"])
        if not source_finding:
            return

        target_finding_id, vulnerabilities = await _move_finding(
            loaders=loaders,
            source_finding=source_finding,
        )
        LOGGER_CONSOLE.info("Finding %s processed", operation_info["source_finding_id"])
    else:
        if not target_finding_id:
            return

        is_moving_all_vulnerable = operation_info["vulnerabilities"] == "All Vulnerable"

        if is_moving_all_vulnerable:
            vulnerabilities = await loaders.finding_vulnerabilities_released_nzr.load(
                operation_info["source_finding_id"],
            )
        else:
            _vulnerabilities = await loaders.vulnerability.load_many(
                operation_info["vulnerabilities"],
            )
            vulnerabilities = [vulnerability for vulnerability in _vulnerabilities if vulnerability]

    filtered_vulnerabilities = (
        [
            vulnerability
            for vulnerability in vulnerabilities
            if vulnerability and vulnerability.state.status == VulnerabilityStateStatus.VULNERABLE
            if vulnerability
        ]
        if operation_info["operation"] != "Move Finding"
        else vulnerabilities
    )

    added_vulns: tuple[Vulnerability, ...] = await collect(
        tuple(
            _move_vulnerability(
                loaders=loaders,
                target_finding_id=target_finding_id,
                vulnerability=vulnerability,
            )
            for vulnerability in filtered_vulnerabilities
        ),
    )

    justification = (
        "The following vulnerabilities were added to this " + "finding under client request:"
    )

    vulns_nicknames = await collect(
        [_get_vuln_nickname(loaders, vuln) for vuln in added_vulns],
        workers=32,
    )
    justification += await _format_vulnerabilities_nickname(
        "Added Vulnerabilities",
        vulns_nicknames,
    )

    new_comment = FindingComment(
        comment_type=CommentType.OBSERVATION,
        content=justification,
        creation_date=datetime.now(),
        email=MODIFIED_BY,
        finding_id=target_finding_id,
        id=str(round(time.time() * 1000)),
        parent_id="0",
    )

    await finding_comments_model.add(finding_comment=new_comment)


async def main() -> None:
    await collect(
        [
            _process_data(
                operation_info=DATA[index],
            )
            for index in range(len(DATA))
        ],
    )


if __name__ == "__main__":
    execution_time = time.strftime("Execution Time:    %Y-%m-%d at %H:%M:%S %Z")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S %Z")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
