"""
Migration to update open sast machine vulnerabilities whose method name
has been modified by recent changes aimed to standardize method names.

- Updates vuln method name to its updated version.
- Adjusts vuln hash according to it.

Execution Time:    2024-06-19 at 23:45:04 UTC
Finalization Time: 2024-06-19 at 23:55:52 UTC

Execution Time:    2024-10-24 at 01:00:45 UTC
Finalization Time: 2024-10-24 at 01:01:34 UTC

"""

import csv
import hashlib
import logging
import logging.config
import time

from aioextensions import (
    collect,
    run,
)

from integrates.custom_utils import (
    datetime as datetime_utils,
)
from integrates.custom_utils.vulnerabilities import (
    get_path_from_integrates_vulnerability,
)
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model.enums import (
    Source,
)
from integrates.db_model.findings.types import (
    Finding,
)
from integrates.db_model.vulnerabilities.enums import (
    VulnerabilityStateReason,
    VulnerabilityType,
)
from integrates.db_model.vulnerabilities.types import (
    Vulnerability,
    VulnerabilityMetadataToUpdate,
    VulnerabilityState,
)
from integrates.db_model.vulnerabilities.update import (
    update_historic_entry,
    update_metadata,
)
from integrates.organizations.domain import (
    get_all_active_groups,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)
LOGGER_CONSOLE = logging.getLogger("console")

JS_2022_23540 = "javascript.js_cve_2022_23540"
TS_2022_23540 = "typescript.ts_cve_2022_23540"

# This dictionary maps old method names with new ones

METHODS_NAME_MAPPER = {
    # F115
    "javascript.js_ssrf_in_cypress_request": "javascript.js_cve_2023_28155",
    "typescript.ts_ssrf_in_cypress_request": "typescript.ts_cve_2023_28155",
    # F211
    "javascript.js_moment_rfc2822_redos": "javascript.js_cve_2022_31129",
    # F309
    "javascript.js_insecure_jwt_token_jsonwebtoken": JS_2022_23540,
    "typescript.ts_insecure_jwt_token_jsonwebtoken": TS_2022_23540,
}


async def get_sast_vulns(
    loaders: Dataloaders,
    findings: list[Finding],
    fin_code: str,
) -> list[Vulnerability]:
    vulns = await loaders.finding_vulnerabilities.load_many_chained(
        [fin.id for fin in findings if fin.title.startswith(fin_code)],
    )

    return [
        vuln
        for vuln in vulns
        if (vuln.state.source == Source.MACHINE or vuln.hacker_email == "machine@fluidattacks.com")
        and vuln.skims_method is not None
        and vuln.type == VulnerabilityType.LINES
    ]


async def process_vuln(
    vuln: Vulnerability,
    fin_code: str,
) -> tuple[int, VulnerabilityState, str, str] | None:
    _, where = get_path_from_integrates_vulnerability(
        vuln.state.where,
        VulnerabilityType.LINES,
    )
    old_method_name = str(vuln.skims_method)
    new_method = METHODS_NAME_MAPPER.get(old_method_name)

    # From prior filters we know here are vulns that
    # Are open and have skims method
    # They only need to be processed if its old_method_name is in the mapper
    if new_method is None:
        return None

    specific = vuln.state.specific

    new_hash = int.from_bytes(
        hashlib.sha256(
            bytes(
                (where + specific + fin_code + new_method),
                encoding="utf-8",
            ),
        ).digest()[:8],
        "little",
    )

    new_state = vuln.state._replace(
        modified_date=datetime_utils.get_utc_now(),
        reasons=[VulnerabilityStateReason.CONSISTENCY],
        modified_by="lpatino@fluidattacks.com",
    )

    return new_hash, new_state, new_method, old_method_name


async def process_vulns(
    sast_vulns: list[Vulnerability],
    fin_code: str,
) -> tuple[list, list]:
    futures = []
    rows = []
    for vuln in sast_vulns:
        vuln_new_attrs = await process_vuln(vuln, fin_code)

        if not vuln_new_attrs:
            continue

        (new_hash, new_state, new_method, old_method) = vuln_new_attrs
        futures.append(
            update_historic_entry(
                current_value=vuln,
                finding_id=vuln.finding_id,
                entry=new_state,
                vulnerability_id=vuln.id,
            ),
        )

        futures.append(
            update_metadata(
                finding_id=vuln.finding_id,
                metadata=VulnerabilityMetadataToUpdate(hash=new_hash, skims_method=new_method),
                root_id=vuln.root_id,
                vulnerability_id=vuln.id,
            ),
        )

        rows.extend(
            [
                [
                    vuln.group_name,
                    fin_code,
                    vuln.id,
                    old_method,
                    new_method,
                ],
            ],
        )
    return futures, rows


async def adjust_group_sast_reports(
    loaders: Dataloaders,
    group: str,
    searched_fins: tuple[str, ...],
) -> None:
    LOGGER_CONSOLE.info("Processing %s", group)
    findings = await loaders.group_findings.load(group)
    all_futures = []
    all_rows = []
    for fin_code in searched_fins:
        sast_vulns = await get_sast_vulns(loaders, findings, fin_code)

        futures, rows = await process_vulns(sast_vulns, fin_code)
        all_futures.extend(futures)
        all_rows.extend(rows)

    await collect(all_futures, workers=16)

    with open("sast_vulns_migration.csv", "a+", encoding="utf-8") as handler:
        writer = csv.writer(handler)
        writer.writerows(all_rows)


async def main() -> None:
    searched_fins = (
        "115",
        "211",
        "309",
    )
    loaders = get_new_context()
    active_groups = await get_all_active_groups(loaders)
    groups = sorted([group.name for group in active_groups])

    for group in groups:
        await adjust_group_sast_reports(
            loaders,
            group,
            searched_fins,
        )


if __name__ == "__main__":
    execution_time = time.strftime("Execution Time:    %Y-%m-%d at %H:%M:%S UTC")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S UTC")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
