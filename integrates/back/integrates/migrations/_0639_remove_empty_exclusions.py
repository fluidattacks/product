"""
Remove empty exclusions from git roots.

Execution Time:    2024-12-04 at 16:23:52 UTC
Finalization Time: 2024-12-04 at 16:26:36 UTC
"""

import logging
import time

from aioextensions import collect, run

from integrates.dataloaders import (
    get_new_context,
)
from integrates.db_model.roots.types import GitRoot
from integrates.db_model.roots.update import update_root_state
from integrates.organizations.domain import get_all_active_group_names

LOGGER = logging.getLogger("migrations")


def validate_exclusions(patterns: list[str]) -> bool:
    for pattern in patterns:
        if len(pattern.strip()) == 0:
            return False

    return True


async def process_empty_exclusions(root: GitRoot) -> None:
    exclusions = root.state.gitignore[:]
    for index, pattern in enumerate(exclusions):
        if not validate_exclusions([pattern]):
            del exclusions[index]
    new_state = root.state._replace(gitignore=exclusions)

    try:
        await update_root_state(
            current_value=root.state,
            group_name=root.group_name,
            root_id=root.id,
            state=new_state,
        )
        LOGGER.info(
            "Removed empty exclusions in %s root of %s group",
            root.state.nickname,
            root.group_name,
            extra={
                "extra": {
                    "exclusions_before": root.state.gitignore,
                    "exclusions_after": new_state.gitignore,
                }
            },
        )
    except Exception as ex:
        logging.error(ex)

    LOGGER.info(
        "Finished processing empty exclusions in %s root of %s group",
        root.state.nickname,
        root.group_name,
    )


async def process_group(group_name: str) -> None:
    loaders = get_new_context()

    group_roots = await loaders.group_roots.load(group_name)

    if not group_roots:
        LOGGER.info("No group roots found", extra={"extra": {"group_name": group_name}})
        return

    empty_exclusion_roots: list[GitRoot] = [
        root
        for root in group_roots
        if isinstance(root, GitRoot)
        and root.state.gitignore
        and not validate_exclusions(root.state.gitignore)
    ]

    if empty_exclusion_roots:
        LOGGER.info(
            "Empty exclusions found in %s group",
            group_name,
            extra={
                "extra": {
                    "empty_exclusions": {
                        root.state.nickname: root.state.gitignore for root in empty_exclusion_roots
                    }
                }
            },
        )
        await collect([process_empty_exclusions(root) for root in empty_exclusion_roots])
    else:
        LOGGER.info("Empty exclusions not found in %s group", group_name)


async def main() -> None:
    loaders = get_new_context()
    group_names = sorted(await get_all_active_group_names(loaders))

    for group_name in group_names:
        LOGGER.info("Processing %s group", group_name)
        await process_group(group_name)


if __name__ == "__main__":
    execution_time = time.strftime("Execution Time:    %Y-%m-%d at %H:%M:%S UTC")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S UTC")
    LOGGER.info("\n%s\n%s", execution_time, finalization_time)
