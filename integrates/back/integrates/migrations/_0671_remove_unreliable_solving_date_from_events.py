"""
Execution Time: 2025-03-05 at 22:00:31 UTC
Finalization Time: 2025-03-05 at 22:16:33 UTC

Remove unreliable from events and add solving_date to event metadata if event is solved
"""

import logging
import logging.config
from datetime import UTC, datetime

from aioextensions import collect
from boto3.dynamodb.conditions import Attr

from integrates.audit import AuditEvent, add_audit_event
from integrates.dataloaders import Dataloaders, get_new_context
from integrates.db_model import TABLE
from integrates.db_model.events.enums import EventStateStatus
from integrates.db_model.events.types import (
    Event,
    EventRequest,
    EventState,
    GroupEventsRequest,
)
from integrates.dynamodb import keys, operations
from integrates.dynamodb.exceptions import ConditionalCheckFailedException
from integrates.migrations.utils import log_time
from integrates.organizations import domain as orgs_domain

LOGGER = logging.getLogger("migrations")


async def _update_event_metadata(event: Event, solving_date: datetime) -> None:
    key_structure = TABLE.primary_key
    primary_key = keys.build_key(
        facet=TABLE.facets["event_metadata"],
        values={"id": event.id, "name": event.group_name},
    )
    item = {
        "unreliable_indicators": {"unreliable_solving_date": None},
        "solving_date": solving_date.astimezone(tz=UTC).isoformat(),
    }
    try:
        await operations.update_item(
            condition_expression=Attr(key_structure.partition_key).exists(),
            item=item,
            key=primary_key,
            table=TABLE,
        )
        add_audit_event(
            AuditEvent(
                action="UPDATE",
                author="unknown",
                metadata=item,
                object="Event",
                object_id=event.id,
            )
        )
    except ConditionalCheckFailedException:
        LOGGER.error(
            "Failed to update event metadata",
            extra={"extra": {"event_id": event.id, "group_name": event.group_name}},
        )


async def _get_solving_state(
    loaders: Dataloaders,
    event_id: str,
    group_name: str,
) -> EventState | None:
    historic_states = await loaders.event_historic_state.load(
        EventRequest(event_id=event_id, group_name=group_name)
    )
    for state in sorted(
        historic_states,
        key=lambda state: state.modified_date,
    ):
        if state.status == EventStateStatus.SOLVED:
            return state

    return None


async def _get_solving_date(loaders: Dataloaders, event: Event) -> datetime | None:
    if date := event.solving_date:
        return date

    closing_state = await _get_solving_state(loaders, event.id, event.group_name)

    if not closing_state:
        LOGGER.error(
            "Event has no closing state",
            extra={"extra": {"event_id": event.id, "group_name": event.group_name}},
        )
        return None

    return closing_state.modified_date


async def _update_event(loaders: Dataloaders, event: Event) -> None:
    solving_date = await _get_solving_date(loaders, event)
    if solving_date:
        await _update_event_metadata(event, solving_date)
        LOGGER.info(
            "Updated event",
            extra={
                "extra": {
                    "event_id": event.id,
                    "group_name": event.group_name,
                    "solving_date": solving_date,
                }
            },
        )


async def process_group(group_name: str) -> None:
    loaders = get_new_context()
    group_solved_events = [
        event
        for event in await loaders.group_events.load(GroupEventsRequest(group_name=group_name))
        if event.state.status == EventStateStatus.SOLVED
    ]

    await collect((_update_event(loaders, event) for event in group_solved_events), workers=4)


@log_time(LOGGER)
async def main() -> None:
    loaders: Dataloaders = get_new_context()
    all_group_names = sorted(await orgs_domain.get_all_group_names(loaders))
    await collect((process_group(group_name) for group_name in all_group_names), workers=4)


if __name__ == "__main__":
    main()  # type: ignore[unused-coroutine]
