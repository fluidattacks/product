"""
Get a CSV report for duplicated roots in the organizations

Execution Time: 2024-11-06 at 21:33:11 UTC
Finalization Time: 2024-11-06 at 21:35:08 UTC
"""

import logging

from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model.organizations.get import (
    get_all_organizations,
)
from integrates.db_model.roots.types import (
    GitRoot,
)
from integrates.migrations.utils import (
    log_time,
)

LOGGER = logging.getLogger("migrations")


@log_time(LOGGER)
async def main() -> None:
    loaders: Dataloaders = get_new_context()
    LOGGER.info("org,group,url,branch,status,created_date")

    all_orgs = [org.name for org in await get_all_organizations()]
    for org in all_orgs:
        roots: list[GitRoot] = [
            root for root in await loaders.organization_roots.load(org) if isinstance(root, GitRoot)
        ]
        urls = [root.state.url for root in roots]
        counter = {url: urls.count(url) for url in set(urls)}
        for root in roots:
            if counter[root.state.url] <= 1:
                continue

            LOGGER.info(
                "%s,%s,%s,%s,%s,%s",
                org,
                root.group_name,
                root.state.url,
                root.state.branch,
                root.state.status,
                root.created_date,
            )


if __name__ == "__main__":
    main()  # type: ignore[unused-coroutine]
