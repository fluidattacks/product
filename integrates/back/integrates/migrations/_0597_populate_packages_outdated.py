"""
Sets outdated value in toe packages

Execution Time:    2024-09-19 at 16:51:23 UTC
Finalization Time: 2024-09-19 at 18:27:23 UTC
"""

import asyncio
import logging
import logging.config
import time

from integrates.custom_exceptions import (
    ToePackageAlreadyUpdated,
)
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model.toe_packages.types import (
    GroupToePackagesRequest,
    ToePackage,
)
from integrates.db_model.toe_packages.update import (
    update_package,
)
from integrates.organizations import (
    domain as orgs_domain,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)

LOGGER = logging.getLogger(__name__)


async def process_package(packages_semaphore: asyncio.Semaphore, package: ToePackage) -> None:
    async with packages_semaphore:
        try:
            await update_package(
                current_value=package,
                package=package._replace(
                    outdated=(
                        package.version != package.health_metadata.latest_version
                        if package.health_metadata
                        else None
                    ),
                ),
            )
        except ToePackageAlreadyUpdated:
            LOGGER.info(
                "%s: package %s updated by another operation",
                package.group_name,
                package.name,
            )


async def process_group(
    groups_semaphore: asyncio.Semaphore,
    loaders: Dataloaders,
    group_name: str,
) -> None:
    async with groups_semaphore:
        LOGGER.info("Working on group %s", group_name)
        connection = await loaders.group_toe_packages.load(
            GroupToePackagesRequest(group_name=group_name),
        )
        packages_semaphore = asyncio.Semaphore(50)
        await asyncio.gather(
            *[process_package(packages_semaphore, edge.node) for edge in connection.edges],
        )
        LOGGER.info(
            "Finished group %s, %s packages processed",
            group_name,
            len(connection.edges),
        )


async def main() -> None:
    loaders = get_new_context()
    all_group_names = sorted(await orgs_domain.get_all_group_names(loaders=loaders))
    groups_semaphore = asyncio.Semaphore(5)
    await asyncio.gather(
        *[process_group(groups_semaphore, loaders, group_name) for group_name in all_group_names],
    )


if __name__ == "__main__":
    execution_time = time.strftime("Execution Time:    %Y-%m-%d at %H:%M:%S %Z")
    asyncio.run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S %Z")
    LOGGER.info("\n%s\n%s", execution_time, finalization_time)
