"""
Mask all group names from deleted groups in Streams Database

Execution Time:
Finalization Time:

=
"""

import hashlib
import logging
import logging.config
import time
from collections.abc import (
    Iterator,
)
from contextlib import (
    contextmanager,
)

import psycopg2  # type: ignore[import-untyped]
from aioextensions import (
    run,
)
from psycopg2 import (
    OperationalError,
    sql,
)
from psycopg2.extensions import (  # type: ignore[import-untyped]
    ISOLATION_LEVEL_AUTOCOMMIT,
)
from psycopg2.extensions import (
    cursor as cursor_cls,
)

from integrates.context import FI_AWS_REDSHIFT_HOST  # type: ignore[attr-defined]
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)

LOGGER = logging.getLogger(__name__)
LOGGER_CONSOLE = logging.getLogger("console")
SCHEMA_NAME: str = "integrates"
AWS_REDSHIFT_PORT = 5439
TABLES = [
    "groups_code_languages",
    "groups_state",
    "events_metadata",
    "findings_metadata",
    "roots_metadata",
    "toe_inputs_metadata",
    "toe_lines_metadata",
]


@contextmanager
def db_cursor() -> Iterator[cursor_cls]:
    try:
        connection = psycopg2.connect(
            dbname="FI_AWS_REDSHIFT_DBNAME",
            host=FI_AWS_REDSHIFT_HOST,
            password="FI_AWS_REDSHIFT_PASSWORD",  # noqa: S106
            port=AWS_REDSHIFT_PORT,
            user="FI_AWS_REDSHIFT_USER",
        )
    except OperationalError as exc:
        LOGGER.error(exc)
        return
    connection.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)
    try:
        cursor: cursor_cls = connection.cursor()
        try:
            yield cursor
        finally:
            cursor.close()
    finally:
        connection.close()


def mask_value(value: str) -> str:
    return hashlib.shake_256(value.encode("utf-8")).hexdigest(10)


def get_group_names(cursor: cursor_cls) -> list[tuple[str]]:
    cursor.execute(
        sql.SQL(
            """
            SELECT
                id
            FROM
                integrates.groups_metadata ORDER BY id ASC;
            """,
        ),
    )

    return list(cursor.fetchall())


def update_group_metadata(
    cursor: cursor_cls,
    groups: list[tuple[str]],
) -> None:
    for group in groups:
        masked_group = mask_value(group[0])
        cursor.execute(
            sql.SQL(
                """
                    UPDATE
                        integrates.groups_metadata AS g_metadata
                    SET
                        g_metadata.id = { masked_name },
                        g_metadata.group_name = { masked_name }
                    WHERE
                        g_metadata.id = { group_name }
                        AND g_metadata.group_name = { group_name };
                """,
            ).format(
                group_name=sql.Identifier(group[0]),
                masked_name=sql.Identifier(masked_group),
            ),
        )


def update_table(
    cursor: cursor_cls,
    groups: list[tuple[str]],
    table: str,
) -> None:
    for group in groups:
        masked_group = mask_value(group[0])
        cursor.execute(
            sql.SQL(
                """
                    UPDATE
                        integrates.{table}
                    SET
                        integrates.{table}.group_name = {masked_name}
                    WHERE
                        integrates.{table}.group_name = {group_name};
                """,
            ).format(
                group_name=sql.Identifier(group[0]),
                masked_name=sql.Identifier(masked_group),
                table=sql.Identifier(table),
            ),
        )


async def main() -> None:
    with db_cursor() as cursor:
        groups = get_group_names(cursor)
        for table in TABLES:
            update_table(
                cursor=cursor,
                groups=groups,
                table=table,
            )
            update_group_metadata(
                cursor=cursor,
                groups=groups,
            )


if __name__ == "__main__":
    execution_time = time.strftime("Execution Time:    %Y-%m-%d at %H:%M:%S UTC")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S UTC")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
