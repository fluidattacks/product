"""
Populate the cvss_v4 field in findings & vulnerabilities.
First Execution Time:     2024-09-05 at 04:38:32 UTC
First Finalization Time:  2024-09-05 at 15:15:12 UTC
Second Execution Time:    2024-09-06 at 13:05:42 UTC
Second Finalization Time: 2024-09-06 at 19:44:48 UTC
Third Execution Time:     2024-09-07 at 12:44:37 UTC
Third Finalization Time:  2024-09-07 at 21:39:40 UTC
Execution Time:    2024-10-24 st 21:16:22 UTC
Finalization Time: 2024-10-25 at 06:42:02 UTC
"""

import logging
import logging.config
import time
from collections.abc import (
    Iterator,
)
from contextlib import (
    contextmanager,
)
from dataclasses import (
    dataclass,
)
from itertools import (
    chain,
)

import boto3
import psycopg2  # type: ignore[import-untyped]
from aioextensions import (
    collect,
    in_thread,
    run,
)
from cvss import (
    CVSS4,
    CVSS4Error,
)
from mypy_boto3_redshift.type_defs import (
    ClusterCredentialsTypeDef,
)
from psycopg2 import (
    OperationalError,
    sql,
)
from psycopg2.extensions import (  # type: ignore[import-untyped]
    ISOLATION_LEVEL_AUTOCOMMIT,
)
from psycopg2.extensions import (
    cursor as cursor_cls,
)

from integrates.context import FI_AWS_REDSHIFT_HOST  # type: ignore[attr-defined]
from integrates.custom_exceptions import (
    InvalidCVSS3VectorString,
    InvalidCVSS4VectorString,
    InvalidCVSSVectorString,
)
from integrates.custom_utils.cvss import (
    get_severity_score_from_cvss_vector,
)
from integrates.dataloaders import (
    get_new_context,
)
from integrates.db_model.types import (
    SeverityScore,
)
from integrates.organizations.domain import (
    get_all_group_names,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)

LOGGER = logging.getLogger(__name__)
LOGGER_CONSOLE = logging.getLogger("console")

REDSHIFT_CLUSTER = "observes"
REDSHIFT_DATABASE = "observes"
REDSHIFT_PORT = 5439
REDSHIFT_USER = "integrates"


@dataclass(frozen=True)
class Credentials:
    user: str
    password: str

    def __repr__(self) -> str:
        return f"Creds(user={self.user})"


def _decode(raw: ClusterCredentialsTypeDef) -> Credentials:
    return Credentials(raw["DbUser"], raw["DbPassword"])


def get_temp_creds(cluster: str, db_name: str, user: str) -> Credentials:
    """[IMPURE] get redshift creds using aws"""
    LOGGER.info("Using temporal DB credentials")
    client = boto3.client("redshift")
    raw = client.get_cluster_credentials(
        DbUser=user,
        DbName=db_name,
        ClusterIdentifier=cluster,
    )
    return _decode(raw)


@contextmanager
def db_cursor() -> Iterator[cursor_cls]:
    try:
        creds = get_temp_creds(REDSHIFT_CLUSTER, REDSHIFT_DATABASE, REDSHIFT_USER)
        connection = psycopg2.connect(
            dbname=REDSHIFT_DATABASE,
            host=FI_AWS_REDSHIFT_HOST,
            password=creds.password,
            port=REDSHIFT_PORT,
            user=creds.user,
        )
    except OperationalError as exc:
        LOGGER.error(exc)
        return
    connection.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)
    try:
        cursor: cursor_cls = connection.cursor()
        try:
            yield cursor
        finally:
            cursor.close()
    finally:
        connection.close()


def _get_ui_default(field: str) -> str:
    value = field.split(":")[1]
    if value == "N":
        return field
    return "UI:A"


def _get_modified_ui_default(field: str) -> str:
    first_value = field.split(":")[1]
    if first_value == "N":
        return field
    return "MUI:A"


def _get_subsystem(field: str) -> str:
    value = field.split(":")[1]
    if value == "U":
        return "SC:N/SI:N/SA:N"
    return "SC:L/SI:L/SA:L"


def _get_modified_subsystem(field: str) -> str:
    value = field.split(":")[1]
    if value == "U":
        return "MSC:N/MSI:N/MSA:N"
    return "MSC:L/MSI:L/MSA:L"


def _get_exploit_maturity(field: str) -> str:
    first_value = field.split(":")[1]
    if first_value in {"F", "H"}:
        return "E:A"
    return field


def _get_modified_ui_special(field: str) -> str:
    first_value = field.split(":")[1]
    if first_value == "N":
        return field
    return "MUI:P"


def _get_ui_special(field: str) -> str:
    value = field.split(":")[1]
    if value == "N":
        return field
    return "UI:P"


def get_field(field: str, code: str) -> str:
    value = ""
    if field in {"CVSS:3.1", "CVSS:3.0"}:
        return "CVSS:4.0"
    if any(
        field.startswith(starts)
        for starts in [
            "AV:",
            "AC:",
            "PR:",
            "MAV:",
            "MAC:",
            "MPR:",
            "CR:",
            "IR:",
            "AR:",
        ]
    ):
        return field
    if field.startswith("E:"):
        return _get_exploit_maturity(field)

    if code in ["009", "359", "367", "411"]:
        value = get_field_special(field)
    else:
        value = get_field_default(field)

    return value


def get_field_special(field: str) -> str:
    value = ""
    if field.startswith("UI:"):
        value = _get_ui_special(field)
    if any(field.startswith(starts) for starts in ["C:", "I:", "A:"]):
        value = f"V{field[:1]}:N/S{field}"
    if any(field.startswith(starts) for starts in ["MC:", "MI:", "MA:"]):
        value = field[:1] + "V" + field[1:2] + ":N/" + field[:1] + "S" + field[1:]
    if field.startswith("MUI:"):
        value = _get_modified_ui_special(field)

    return value


def get_field_default(field: str) -> str:
    value = ""
    if field.startswith("UI:"):
        value = _get_ui_default(field)
    if any(field.startswith(starts) for starts in ["C:", "I:", "A:"]):
        value = f"V{field}"
    if field.startswith("S:"):
        value = _get_subsystem(field)
    if any(field.startswith(starts) for starts in ["MC:", "MI:", "MA:"]):
        value = field[:1] + "V" + field[1:]
    if field.startswith("MS:"):
        value = _get_modified_subsystem(field)
    if field.startswith("MUI:"):
        value = _get_modified_ui_default(field)

    return value


def get_from_v3(field: str | None, code: str) -> str | None:
    if field is None:
        return None

    converted = (
        "/".join(
            filter(
                None,
                [get_field(field, code) for field in field.split("/")],
            ),
        )
        + "/AT:N"
    )

    try:
        return CVSS4(converted).clean_vector()
    except CVSS4Error:
        return None


def get_all_findings(cursor: cursor_cls) -> list[tuple[str, str | None]]:
    cursor.execute(
        sql.SQL(
            """
            SELECT
                id, title
            FROM
                integrates.findings_metadata;
            """,
        ),
    )

    return list(cursor.fetchall())


def get_findings_missing_version(
    cursor: cursor_cls,
) -> list[tuple[str, str | None, str | None, str | None]]:
    cursor.execute(
        sql.SQL(
            """
            SELECT
                id, title, severity_cvss_v3, severity_cvss_v4
            FROM
                integrates.findings_metadata
            WHERE
                title is not NULL
                AND severity_cvss_v3 is not NULL
                AND severity_cvss_v4 is NULL;
            """,
        ),
    )

    return list(cursor.fetchall())


def get_vulnerabilities_missing_version(
    cursor: cursor_cls,
) -> list[tuple[str, str | None, str | None, str | None]]:
    cursor.execute(
        sql.SQL(
            """
            SELECT
                id, finding_id, severity_cvss_v3, severity_cvss_v4
            FROM
                integrates.vulnerabilities_metadata
            WHERE
                finding_id is not NULL
                AND severity_cvss_v3 is not NULL
                AND severity_cvss_v4 is NULL;
            """,
        ),
    )

    return list(cursor.fetchall())


def get_severity_score(title: str | None, vector: str) -> SeverityScore | None:
    if not title:
        return None

    cvss4_vector = str(get_from_v3(vector, title.split(".")[0].strip()))
    try:
        return get_severity_score_from_cvss_vector(vector, cvss4_vector)
    except (
        InvalidCVSS4VectorString,
        InvalidCVSSVectorString,
        InvalidCVSS3VectorString,
    ) as exc:
        LOGGER_CONSOLE.error(
            "Invalid vector",
            extra={
                "extra": {
                    "cvss4_vector": cvss4_vector,
                    "exc": exc,
                },
            },
        )
        return None


def update_finding(
    cursor: cursor_cls,
    finding: tuple[str, str | None, str | None, str | None],
) -> None:
    if finding[2] is None or finding[3] is not None:
        return
    severity_score = get_severity_score(finding[1], finding[2])
    if severity_score is None:
        return

    update_table(cursor, finding[0], severity_score, "findings_metadata")


def update_vulnerability(
    cursor: cursor_cls,
    title: str,
    vulnerability: tuple[str, str | None, str | None, str | None],
) -> None:
    if vulnerability[2] is None or vulnerability[3] is not None:
        return
    severity_score = get_severity_score(title, vulnerability[2])
    if severity_score is None:
        return

    update_table(cursor, vulnerability[0], severity_score, "vulnerabilities_metadata")


def update_table(
    cursor: cursor_cls,
    _id: str,
    severity_score: SeverityScore,
    table: str,
) -> None:
    cursor.execute(
        sql.SQL(
            """
                UPDATE
                    {table}
                SET
                    severity_cvss_v4 = %(severity_cvss_v4)s,
                    severity_threat_score = %(threat_score)s
                WHERE
                    id = %(id)s;
            """,
        ).format(
            table=sql.Identifier(REDSHIFT_USER, table),
        ),
        {
            "id": _id,
            "severity_cvss_v4": severity_score.cvss_v4,
            "threat_score": severity_score.threat_score,
        },
    )


async def main() -> None:
    findings_title: dict[str, str] = {}
    loaders = get_new_context()
    group_names = await get_all_group_names(loaders)
    findings = list(chain.from_iterable(await loaders.group_findings_all.load_many(group_names)))
    with db_cursor() as cursor:
        all_findings = get_all_findings(cursor)
        removed_findings = get_findings_missing_version(cursor)
        for _finding in findings:
            findings_title[_finding.id] = _finding.title
        for __finding in all_findings:
            findings_title[__finding[0]] = __finding[1] if __finding[1] else ""

        await collect(
            [
                in_thread(
                    update_finding,
                    cursor,
                    finding,
                )
                for finding in removed_findings
            ],
            workers=4,
        )

        removed_vulnerabilities = get_vulnerabilities_missing_version(cursor)
        await collect(
            [
                in_thread(
                    update_vulnerability,
                    cursor,
                    findings_title.get(str(vuln[1]), ""),
                    vuln,
                )
                for vuln in removed_vulnerabilities
            ],
            workers=64,
        )


if __name__ == "__main__":
    execution_time = time.strftime("Execution Time:    %Y-%m-%d at %H:%M:%S UTC")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S UTC")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
