# type: ignore
"""
Replace org external ID with root external
ID if an ARN has been added to the repo.

Start Time:    2024-01-31 at 20:15:06 UTC
Finalization Time: 2024-01-31 at 20:45:53 UTC
"""

import logging
import logging.config
import re
import time
from collections.abc import (
    Awaitable,
)

from aioextensions import (
    collect,
    run,
)

from integrates.custom_utils.datetime import (
    get_utc_now,
)
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model.groups.types import (
    Group,
)
from integrates.db_model.organizations.get import (
    iterate_organizations,
)
from integrates.db_model.organizations.types import (
    Organization,
    OrganizationState,
)
from integrates.db_model.organizations.update import (
    update_state,
)
from integrates.db_model.roots.types import (
    GitRoot,
    GitRootState,
    Root,
    RootEnvironmentUrl,
)
from integrates.db_model.roots.update import (
    update_root_state,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)
LOGGER_CONSOLE = logging.getLogger(__name__)


async def _get_root_environment_urls(loaders: Dataloaders, root: Root) -> list[RootEnvironmentUrl]:
    return await loaders.root_environment_urls.load(root.id)


def create_new_gitroot_state(root: GitRoot, aws_external_id: str) -> GitRootState:
    return GitRootState(
        aws_external_id=aws_external_id,
        branch=root.state.branch,
        environment=root.state.environment,
        includes_health_check=root.state.includes_health_check,
        modified_by="ugomez@fluidattacks.com",
        modified_date=get_utc_now(),
        nickname=root.state.nickname,
        status=root.state.status,
        url=root.state.url,
        credential_id=root.state.credential_id,
        gitignore=root.state.gitignore,
        other=root.state.other,
        reason=root.state.reason,
        use_egress=root.state.use_egress,
        use_vpn=root.state.use_vpn,
        use_ztna=root.state.use_ztna,
    )


async def process_org(
    loaders: Dataloaders,
    group: Group,
    organization: Organization,
) -> tuple[list[Awaitable[None]], int]:
    roots = [
        root for root in await loaders.group_roots.load(group.name) if isinstance(root, GitRoot)
    ]
    arn_root = None
    arn_count = 0

    for root in roots:
        urls = await _get_root_environment_urls(loaders, root)
        for url in urls:
            if re.match(r"^arn:aws:iam::\d{12}:role\/[\w+=,.@-]+$", url.url):
                arn_count += 1
                arn_root = root

    update_tasks: list[Awaitable[None]] = []
    aws_external_id = (
        arn_root.state.aws_external_id if arn_count == 1 else organization.state.aws_external_id
    )

    if arn_count == 1:
        new_org_state = OrganizationState(
            status=organization.state.status,
            modified_by="ugomez@fluidattacks.com",
            modified_date=get_utc_now(),
            aws_external_id=aws_external_id,
            pending_deletion_date=organization.state.pending_deletion_date,
        )
        update_tasks.append(
            update_state(
                organization_id=organization.id,
                organization_name=organization.name,
                state=new_org_state,
            ),
        )

    for root in roots:
        new_state = create_new_gitroot_state(root, aws_external_id)
        update_tasks.append(
            update_root_state(
                current_value=root.state,
                group_name=root.group_name,
                root_id=root.id,
                state=new_state,
            ),
        )

    return update_tasks, arn_count


async def main() -> None:
    loaders: Dataloaders = get_new_context()
    async for organization in iterate_organizations():
        org_groups = await loaders.organization_groups.load(organization.id)
        all_update_tasks: list[Awaitable[None]] = []
        total_arn_count = 0

        for group in org_groups:
            update_tasks, arn_count = await process_org(loaders, group, organization)
            total_arn_count += arn_count
            all_update_tasks.extend(update_tasks)

        if total_arn_count <= 1:
            await collect(all_update_tasks, workers=16)


if __name__ == "__main__":
    execution_time = time.strftime("Start Time:    %Y-%m-%d at %H:%M:%S UTC")
    LOGGER_CONSOLE.info("%s", execution_time)
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S UTC")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
