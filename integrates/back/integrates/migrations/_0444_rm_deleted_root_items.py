"""
Remove items related to some identified deleted roots, without metadata
on db.

Start Time:	2023-09-26 at 21:10:17 UTC
End Time:	2023-09-26 at 21:17:25 UTC
"""

import csv
import logging
import logging.config
import time

from aioextensions import (
    collect,
    run,
)
from boto3.dynamodb.conditions import (
    Key,
)

from integrates.db_model import (
    TABLE,
)
from integrates.dynamodb import (
    operations,
)
from integrates.dynamodb.types import (
    PrimaryKey,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)
LOGGER = logging.getLogger("console")


async def process_root(root_pk: str) -> None:
    key_structure = TABLE.primary_key
    response = await operations.query(
        condition_expression=Key(key_structure.partition_key).eq(root_pk),
        facets=(TABLE.facets["git_root_metadata"],),
        table=TABLE,
    )
    await operations.batch_delete_item(
        keys=tuple(
            PrimaryKey(
                partition_key=item["pk"],
                sort_key=item["sk"],
            )
            for item in response.items
        ),
        table=TABLE,
    )
    LOGGER.info("Deleted %d items for %s", len(response.items), root_pk)


async def main() -> None:
    with open("roots_uuid_no_metadata.csv", encoding="utf8") as file:
        reader = csv.reader(file)
        await collect(
            [process_root(root_pk) for root_pk in [row[0] for row in reader]],
            workers=8,
        )


if __name__ == "__main__":
    start_time = time.strftime("Start Time:\t%Y-%m-%d at %H:%M:%S UTC")
    run(main())
    end_time = time.strftime("End Time:\t%Y-%m-%d at %H:%M:%S UTC")
    LOGGER.info("\n%s\n%s", start_time, end_time)
