"""
Archive in Redshift the data related to some missing DELETED groups.
Data is taken from restored backup "integrates_vms" tables.

Facets to process:
events
findings
groups
organizations
roots
toe_inputs
toe_lines
vulnerabilities

"""

import logging
import logging.config
import sys
import time
from typing import (
    TypedDict,
)

from aioextensions import (
    run,
)
from boto3.dynamodb.conditions import (
    Key,
)

from integrates.archive.utils import (
    mask_by_encoding,
)
from integrates.class_types.types import (
    Item,
)
from integrates.db_model import (
    TABLE as INTEGRATES_VMS_TABLE,
)
from integrates.db_model.vulnerabilities.constants import (
    GROUP_INDEX_METADATA as GROUP_INDEX_METADATA_FACET,
)
from integrates.dynamodb import (
    keys,
    operations,
)
from integrates.dynamodb.types import (
    Facet,
    Index,
    PrimaryKey,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)
LOGGER = logging.getLogger("console")
BACKUP_TABLE_NAME = "vms_backup_sep_01"
BACKUP_TABLE = INTEGRATES_VMS_TABLE._replace(name=BACKUP_TABLE_NAME)
INVERTED_INDEX = BACKUP_TABLE.indexes["inverted_index"]
GSI_5_INDEX = BACKUP_TABLE.indexes["gsi_5"]
EMAIL_INTEGRATES = "integrates@fluidattacks.com"
GROUP_NAMES: list[str] = []  # MASKED


class GroupSummary(TypedDict):
    source: str
    group_name: str
    total_findings: str
    total_vulns: str
    total_roots: str
    total_events: str
    total_toe_inputs: str
    total_toe_lines: str


async def run_query_on_backup(
    primary_key: PrimaryKey,
    facets: tuple[Facet, ...],
    index: Index | None = None,
) -> tuple[Item, ...]:
    if index:
        key_structure = index.primary_key
        condition_expression = Key(key_structure.partition_key).eq(primary_key.sort_key) & Key(
            key_structure.sort_key,
        ).begins_with(primary_key.partition_key)
    else:
        key_structure = BACKUP_TABLE.primary_key
        condition_expression = Key(key_structure.partition_key).eq(primary_key.partition_key) & Key(
            key_structure.sort_key,
        ).begins_with(primary_key.sort_key)

    response = await operations.query(
        condition_expression=condition_expression,
        facets=facets,
        index=index,
        table=BACKUP_TABLE,
    )

    return response.items


async def _get_total_group_events(group_name: str) -> int:
    metadata_items = await run_query_on_backup(
        primary_key=keys.build_key(
            facet=BACKUP_TABLE.facets["event_metadata"],
            values={"name": group_name},
        ),
        facets=(BACKUP_TABLE.facets["event_metadata"],),
        index=INVERTED_INDEX,
    )
    items = tuple(
        item
        for item in metadata_items
        if item["pk"].startswith("EVENT#") and item["sk"].startswith("GROUP#")
    )

    return len(items if items else [])


async def _get_total_group_vulns(group_name: str) -> int:
    primary_key = keys.build_key(
        facet=GROUP_INDEX_METADATA_FACET,
        values={"group_name": group_name},
    )
    key_structure = GSI_5_INDEX.primary_key
    response = await operations.query(
        condition_expression=(Key(key_structure.partition_key).eq(primary_key.partition_key)),
        facets=(BACKUP_TABLE.facets["vulnerability_metadata"],),
        table=BACKUP_TABLE,
        index=GSI_5_INDEX,
    )
    items = tuple(
        item
        for item in response.items
        if item["pk"].startswith("VULN#") and item["sk"].startswith("FIN#")
    )

    return len(items if items else [])


async def _get_total_group_findings(group_name: str) -> int:
    metadata_items = await run_query_on_backup(
        primary_key=keys.build_key(
            facet=BACKUP_TABLE.facets["finding_metadata"],
            values={"group_name": group_name},
        ),
        facets=(BACKUP_TABLE.facets["finding_metadata"],),
        index=INVERTED_INDEX,
    )
    items = tuple(
        item
        for item in metadata_items
        if item["pk"].startswith("FIN#") and item["sk"].startswith("GROUP#")
    )

    return len(items if items else [])


async def _get_total_group_roots(group_name: str) -> int:
    metadata_items = await run_query_on_backup(
        primary_key=keys.build_key(
            facet=BACKUP_TABLE.facets["git_root_metadata"],
            values={"name": group_name},
        ),
        facets=(BACKUP_TABLE.facets["git_root_metadata"],),
        index=INVERTED_INDEX,
    )
    items = tuple(
        item
        for item in metadata_items
        if item["pk"].startswith("ROOT#")
        and not item["pk"].startswith("ROOT#URL#")
        and item["sk"].startswith("GROUP#")
    )

    return len(items if items else [])


async def _get_total_group_toe_inputs(group_name: str) -> int:
    primary_key = keys.build_key(
        facet=BACKUP_TABLE.facets["toe_input_metadata"],
        values={"group_name": group_name},
    )
    key_structure = BACKUP_TABLE.primary_key
    response = await operations.query(
        condition_expression=(
            Key(key_structure.partition_key).eq(primary_key.partition_key)
            & Key(key_structure.sort_key).begins_with(
                primary_key.sort_key.replace("#COMPONENT#ENTRYPOINT", ""),
            )
        ),
        facets=(BACKUP_TABLE.facets["toe_input_metadata"],),
        table=BACKUP_TABLE,
    )
    items = [
        item
        for item in response.items
        if item["pk"].startswith("GROUP#") and item["sk"].startswith("INPUTS#")
    ]

    return len(items if items else [])


async def _get_total_group_toe_lines(group_name: str) -> int:
    primary_key = keys.build_key(
        facet=BACKUP_TABLE.facets["toe_lines_metadata"],
        values={"group_name": group_name},
    )
    key_structure = BACKUP_TABLE.primary_key
    response = await operations.query(
        condition_expression=(
            Key(key_structure.partition_key).eq(primary_key.partition_key)
            & Key(key_structure.sort_key).begins_with(primary_key.sort_key.replace("#FILENAME", ""))
        ),
        facets=(BACKUP_TABLE.facets["toe_lines_metadata"],),
        table=BACKUP_TABLE,
    )
    items = [
        item
        for item in response.items
        if item["pk"].startswith("GROUP#") and item["sk"].startswith("LINES#")
    ]

    return len(items if items else [])


async def _process_group(group_name: str) -> GroupSummary:
    items = await run_query_on_backup(
        primary_key=keys.build_key(
            facet=BACKUP_TABLE.facets["group_metadata"],
            values={"name": group_name},
        ),
        facets=(BACKUP_TABLE.facets["group_metadata"],),
    )

    if not items:
        return {
            "source": BACKUP_TABLE_NAME,
            "group_name": group_name,
            "total_findings": "-",
            "total_vulns": "-",
            "total_roots": "-",
            "total_events": "-",
            "total_toe_inputs": "-",
            "total_toe_lines": "-",
        }

    total_vulns = await _get_total_group_vulns(group_name)
    total_findings = await _get_total_group_findings(group_name)
    total_events = await _get_total_group_events(group_name)
    total_roots = await _get_total_group_roots(group_name)
    total_inputs = await _get_total_group_toe_inputs(group_name)
    total_lines = await _get_total_group_toe_lines(group_name)

    return {
        "source": BACKUP_TABLE_NAME,
        "group_name": group_name,
        "total_findings": str(total_findings),
        "total_vulns": str(total_vulns),
        "total_roots": str(total_roots),
        "total_events": str(total_events),
        "total_toe_inputs": str(total_inputs),
        "total_toe_lines": str(total_lines),
    }


async def main(*_args: str) -> None:
    groups_to_process: list[str] = GROUP_NAMES

    for group_name in groups_to_process:
        value = await _process_group(group_name)
        LOGGER.info(
            "%s,%s,%s,%s,%s,%s,%s,%s,%s",
            mask_by_encoding(group_name),
            value["source"],
            value["group_name"],
            value["total_findings"],
            value["total_vulns"],
            value["total_roots"],
            value["total_events"],
            value["total_toe_inputs"],
            value["total_toe_lines"],
        )


if __name__ == "__main__":
    migration_args = sys.argv[1:]
    LOGGER.info("Arguments passed down to the script: %s", str(migration_args))
    execution_time = time.strftime("Execution Time:    %Y-%m-%d at %H:%M:%S %Z")
    run(main(*migration_args))
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S %Z")
    LOGGER.info("\n%s\n%s", execution_time, finalization_time)
