"""
Update nickname if repeated between group roots

Start Time:        2023-10-13 at 03:38:42 UTC
Finalization Time: 2023-10-13 at 03:41:11 UTC
"""

import logging
import logging.config
import time

from aioextensions import (
    collect,
    run,
)

from integrates.custom_exceptions import (
    GroupNotFound,
)
from integrates.custom_utils.datetime import (
    get_utc_now,
)
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model.roots.types import (
    GitRoot,
    GitRootState,
)
from integrates.db_model.roots.update import (
    update_root_state,
)
from integrates.organizations import (
    domain as orgs_domain,
)
from integrates.roots.domain import (
    assign_nickname,
)
from integrates.roots.utils import (
    format_reapeted_nickname,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)
LOGGER_CONSOLE = logging.getLogger("console")


async def process_root(root: GitRoot) -> None:
    nickname = format_reapeted_nickname(nickname=root.state.nickname)
    nickname = assign_nickname(
        nickname=root.state.nickname,
        new_nickname=nickname,
        _roots=await get_new_context().group_roots.load(root.group_name),
    )
    new_state = GitRootState(
        branch=root.state.branch,
        credential_id=root.state.credential_id,
        gitignore=root.state.gitignore,
        includes_health_check=root.state.includes_health_check,
        modified_by=orgs_domain.EMAIL_INTEGRATES,
        modified_date=get_utc_now(),
        nickname=nickname,
        other=None,
        reason=None,
        status=root.state.status,
        url=root.state.url,
        use_vpn=root.state.use_vpn,
    )
    await update_root_state(
        current_value=root.state,
        group_name=root.group_name,
        root_id=root.id,
        state=new_state,
    )


async def process_group(loaders: Dataloaders, group_name: str) -> None:
    group = await loaders.group.load(group_name)
    if not group:
        raise GroupNotFound()

    roots = [
        root for root in await loaders.group_roots.load(group_name) if isinstance(root, GitRoot)
    ]
    nicknames = [root.state.nickname for root in roots]
    for root in roots:
        if len([nickname for nickname in nicknames if root.state.nickname == nickname]) > 1:
            await process_root(root)
            nicknames.remove(root.state.nickname)

    LOGGER_CONSOLE.info("Processed group %s", group_name)


async def main() -> None:
    loaders: Dataloaders = get_new_context()
    all_group_names = sorted(await orgs_domain.get_all_group_names(loaders=loaders))
    await collect(
        tuple(process_group(loaders, group) for group in all_group_names),
        workers=4,
    )


if __name__ == "__main__":
    execution_time = time.strftime("Start Time:    %Y-%m-%d at %H:%M:%S UTC")
    LOGGER_CONSOLE.info("%s", execution_time)
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S UTC")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
