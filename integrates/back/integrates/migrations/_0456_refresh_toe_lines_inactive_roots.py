"""
Refresh toe line be present attr on inactive roots

Start Time:         2023-11-17 at 21:10:01 UTC
Finalization Time:  2023-11-17 at 21:10:10 UTC

Start Time: 2024-01-23 at 17:46:18 UTC
Finalization Time: 2024-01-23 at 17:49:21 UTC
"""

import logging
import logging.config
import time

from aioextensions import (
    collect,
    run,
)

from integrates.batch_dispatch.refresh_toe_lines import (
    process_non_present_toe_lines_to_update,
)
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model.groups.enums import (
    GroupManaged,
    GroupService,
)
from integrates.db_model.roots.enums import (
    RootStatus,
)
from integrates.db_model.roots.types import (
    GitRoot,
)
from integrates.db_model.toe_lines.types import (
    RootToeLinesRequest,
)
from integrates.organizations import (
    domain as orgs_domain,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)
LOGGER_CONSOLE = logging.getLogger("console")


async def process_root_toe_lines(*, loaders: Dataloaders, git_root: GitRoot) -> None:
    repo_toe_lines = {
        toe_lines.filename: toe_lines
        for toe_lines in await loaders.root_toe_lines.load_nodes(
            RootToeLinesRequest(group_name=git_root.group_name, root_id=git_root.id),
        )
    }

    await process_non_present_toe_lines_to_update(
        git_root,
        set(),
        repo_toe_lines,
    )


async def process_group(group_name: str) -> None:
    loaders: Dataloaders = get_new_context()
    inactive_roots = tuple(
        root
        for root in await loaders.group_roots.load(group_name)
        if root.state.status == RootStatus.INACTIVE
    )
    if not inactive_roots:
        return

    await collect(
        [
            process_root_toe_lines(
                loaders=loaders,
                git_root=root,
            )
            for root in inactive_roots
            if isinstance(root, GitRoot)
        ],
        workers=16,
    )
    LOGGER_CONSOLE.info(
        "Group queued",
        extra={
            "extra": {
                "group_name": group_name,
                "roots_len": len(inactive_roots),
                "root_ids": [root.id for root in inactive_roots],
                "root_nicknames": [root.state.nickname for root in inactive_roots],
            },
        },
    )


async def main() -> None:
    loaders: Dataloaders = get_new_context()
    groups = await orgs_domain.get_all_active_groups(loaders)
    enabled_groups = sorted(
        [
            group.name
            for group in groups
            if group.state.service == GroupService.WHITE
            and group.state.managed != GroupManaged.UNDER_REVIEW
        ],
    )
    LOGGER_CONSOLE.info(
        "Groups to process",
        extra={
            "extra": {
                "group_names": enabled_groups,
                "len": len(enabled_groups),
            },
        },
    )

    await collect(
        [process_group(group_name) for group_name in enabled_groups],
        workers=16,
    )


if __name__ == "__main__":
    execution_time = time.strftime("Start Time: %Y-%m-%d at %H:%M:%S UTC")
    LOGGER_CONSOLE.info("%s", execution_time)
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S UTC")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
