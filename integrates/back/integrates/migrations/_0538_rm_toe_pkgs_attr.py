"""
Remove repeated fields line and lines
from the metadata item for all toe pckgs in db.
"""

import logging
import logging.config
import time
from itertools import (
    chain,
)
from typing import (
    Any,
)

from aioextensions import (
    collect,
    run,
)
from boto3.dynamodb.conditions import (
    Attr,
    Key,
)

from integrates.class_types.types import (
    Item,
)
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model import (
    TABLE,
)
from integrates.dynamodb import (
    keys,
    operations,
)
from integrates.dynamodb.types import (
    PrimaryKey,
)
from integrates.organizations import (
    domain as orgs_domain,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)
LOGGER_CONSOLE = logging.getLogger("console")

ATTRS_TO_REMOVE = ["line", "lines"]


async def process_packages(item: Item) -> dict[str, Any]:
    primary_key = PrimaryKey(partition_key=item["pk"], sort_key=item["sk"])
    key_structure = TABLE.primary_key
    state = item.get("state", {})
    if "state" in item:
        await operations.update_item(
            condition_expression=Attr(key_structure.partition_key).exists(),
            item={
                f"state.{attr}": None if attr in state else item.get(attr)
                for attr in ATTRS_TO_REMOVE
            },
            key=primary_key,
            table=TABLE,
        )

    return {
        "group_name": item["group_name"],
        "package_id": item["state"]["id"],
        "attrs_to_remove": (item["state"][attr] for attr in ATTRS_TO_REMOVE),
        "creation_modified_by": item["state"]["modified_by"],
    }


async def get_packages_by_group(
    group_name: str,
) -> tuple[Item, ...]:
    facet = TABLE.facets["toe_packages_metadata"]
    primary_key = keys.build_key(
        facet=facet,
        values={"group_name": group_name},
    )
    index = None
    key_structure = TABLE.primary_key
    response = await operations.query(
        condition_expression=Key(key_structure.partition_key).eq(primary_key.partition_key)
        & Key(key_structure.sort_key).begins_with(
            primary_key.sort_key.replace("#ROOT#PATH#PACKAGE_NAME#VERSION", ""),
        ),
        filter_expression=Attr("state").exists(),
        facets=(facet,),
        index=index,
        table=TABLE,
    )
    return response.items


async def process_group(
    group_name: str,
    progress: float,
) -> list[dict[str, str]]:
    if not (group_packages := await get_packages_by_group(group_name)):
        return []

    results = await collect(
        tuple(process_packages(item) for item in group_packages),
        workers=64,
    )
    LOGGER_CONSOLE.info(
        "Group processed %s %s %s",
        group_name,
        f"{round(progress, 2)!s}",
        f"{len(group_packages)=}",
    )
    return list(results)


async def main() -> None:
    loaders: Dataloaders = get_new_context()
    all_group_names = sorted(await orgs_domain.get_all_group_names(loaders=loaders))
    LOGGER_CONSOLE.info("%s", f"{all_group_names=}")
    LOGGER_CONSOLE.info("%s", f"{len(all_group_names)=}")
    results = list(
        chain.from_iterable(
            await collect(
                tuple(
                    process_group(
                        group_name=group,
                        progress=count / len(all_group_names),
                    )
                    for count, group in enumerate(all_group_names)
                ),
                workers=1,
            ),
        ),
    )
    LOGGER_CONSOLE.info("%s", f"{len(results)=}")


if __name__ == "__main__":
    execution_time = time.strftime("Start Time:        %Y-%m-%d at %H:%M:%S UTC")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S UTC")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
