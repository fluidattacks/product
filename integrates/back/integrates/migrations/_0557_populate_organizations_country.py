"""
Populate organizations country
Execution Time:    2024-06-18 at 16:53:44
Finalization Time: 2024-06-18 at 16:53:46

Execution Time:    2024-06-18 at 19:20:53
Finalization Time: 2024-06-18 at 19:20:56
"""

import logging
import time
from typing import (
    Any,
)

from aioextensions import (
    collect,
    run,
)
from boto3.dynamodb.conditions import (
    Attr,
    Key,
)

from integrates.db_model import (
    TABLE,
)
from integrates.db_model.organizations.enums import (
    OrganizationStateStatus,
)
from integrates.db_model.organizations.utils import (
    remove_org_id_prefix,
)
from integrates.dynamodb import (
    keys,
    operations,
)
from integrates.dynamodb.exceptions import (
    ConditionalCheckFailedException,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)

LOGGER = logging.getLogger(__name__)
LOGGER_CONSOLE = logging.getLogger("console")


ORGANIZATIONS_COUNTRIES = {
    "ORGANIZATION_NAME": "COUNTRY",
}

SORTED_DICT = dict(sorted(ORGANIZATIONS_COUNTRIES.items(), key=lambda item: item[1]))


async def _get_organization_by_name(*, organization_name: str) -> dict[str, Any] | None:
    organization_name = organization_name.lower().strip()
    primary_key = keys.build_key(
        facet=TABLE.facets["organization_metadata"],
        values={"name": organization_name},
    )

    key_structure = TABLE.primary_key
    response = await operations.query(
        condition_expression=(
            Key(key_structure.sort_key).eq(primary_key.sort_key)
            & Key(key_structure.partition_key).begins_with(primary_key.partition_key)
        ),
        facets=(TABLE.facets["organization_metadata"],),
        index=TABLE.indexes["inverted_index"],
        limit=1,
        table=TABLE,
    )

    if not response.items:
        return None

    return response.items[0]


async def update_country(
    *,
    organization_id: str,
    organization_name: str,
    country: str,
) -> None:
    organization_id = remove_org_id_prefix(organization_id)
    key_structure = TABLE.primary_key

    try:
        primary_key = keys.build_key(
            facet=TABLE.facets["organization_metadata"],
            values={
                "id": organization_id,
                "name": organization_name,
            },
        )
        organization_item = {"country": country}
        condition_expression = Attr(key_structure.partition_key).exists() & Attr("state.status").ne(
            OrganizationStateStatus.DELETED.value,
        )
        await operations.update_item(
            condition_expression=condition_expression,
            item=organization_item,
            key=primary_key,
            table=TABLE,
        )
        LOGGER.info("Organization country updated, new country: %s", country)
    except ConditionalCheckFailedException:
        return LOGGER.error("Organization not found: %s", organization_name)


async def update_organizations_countries(organization_name: str, country: str) -> None:
    organization = await _get_organization_by_name(organization_name=organization_name.lower())
    if not organization:
        LOGGER.error("Organization not found: %s", organization_name)
        return

    if organization.get("country", "") != country:
        await update_country(
            organization_id=organization["id"],
            organization_name=organization_name.lower(),
            country=country,
        )
        LOGGER.info(
            "Organization to update: %s, %s -> %s",
            organization_name,
            organization.get("country", ""),
            country,
        )
    else:
        LOGGER.error("Organization not update: %s", organization_name)


async def main() -> None:
    await collect(
        [
            update_organizations_countries(organization, country)
            for organization, country in SORTED_DICT.items()
        ],
        workers=10,
    )


if __name__ == "__main__":
    execution_time = time.strftime("Execution Time:    %Y-%m-%d at %H:%M:%S")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
