"""
Update trial completed parameter to fix free trial users.

Execution Time:    2024-11-22 at 14:50:32 UTC
Finalization Time: 2024-11-22 at 14:50:42 UTC
"""

import logging
import logging.config
import time

from aioextensions import (
    collect,
    run,
)

from integrates.dataloaders import (
    get_new_context,
)
from integrates.db_model.trials.types import (
    Trial,
    TrialMetadataToUpdate,
)
from integrates.settings import (
    LOGGING,
)
from integrates.trials import (
    domain as trials_domain,
)

logging.config.dictConfig(LOGGING)

LOGGER = logging.getLogger(__name__)
LOGGER_CONSOLE = logging.getLogger("console")


async def _update_trial(trial: Trial) -> None:
    await trials_domain.update_metadata(
        email=trial.email,
        metadata=TrialMetadataToUpdate(completed=False),
    )
    LOGGER.info("Completed parameter of Trial for %s has been updated.", trial.email)


async def main() -> None:
    emails: list[str] = []
    loaders = get_new_context()
    trials = await loaders.trial.load_many(emails)

    await collect(tuple(_update_trial(trial) for trial in trials if trial and trial.completed))


if __name__ == "__main__":
    execution_time = time.strftime("Execution Time:    %Y-%m-%d at %H:%M:%S UTC")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S UTC")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
