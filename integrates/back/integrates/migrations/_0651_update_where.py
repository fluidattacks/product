"""
Execution Time:    2025-01-20 at 00:51:15 UTC
Finalization Time: 2025-01-20 at 09:50:24 UTC
Execution Time:    2025-01-20 at 12:44:51 UTC
Finalization Time: 2025-01-20 at 13:51:00 UTC
Execution Time:    2025-01-20 at 21:35:32 UTC
Finalization Time: 2025-01-20 at 22:01:55 UTC
Execution Time:    2025-01-21 at 15:04:45 UTC
Finalization Time: 2025-01-21 at 16:01:17 UTC

Update where in some vulnerabilities
"""

import logging
import re

from aioextensions import collect

from integrates.custom_utils.datetime import get_utc_now
from integrates.dataloaders import get_new_context
from integrates.db_model.groups.types import Group
from integrates.db_model.vulnerabilities.enums import VulnerabilityType
from integrates.db_model.vulnerabilities.types import (
    GroupVulnerabilitiesRequest,
    Vulnerability,
    VulnerabilityRequest,
)
from integrates.db_model.vulnerabilities.update import update_historic_entry
from integrates.migrations.utils import log_time
from integrates.organizations.domain import get_all_groups

LOGGER = logging.getLogger("migrations")


def get_commit_hash(commit_hash: str) -> str:
    for index, char in enumerate(commit_hash):
        if char.isalnum():
            commit_hash = commit_hash[index:]
            break
    for index, char in reversed(list(enumerate(commit_hash))):
        if char.isalnum():
            commit_hash = commit_hash[: min(index + 1, len(commit_hash))]
            break

    return commit_hash


def get_commit_hash_from_vulnerability_where(where: str) -> str | None:
    if commit_hash_search := re.search(
        r"[\[\(](commit|commite|commit_hash) (\w{40})[\]\)]", where, re.IGNORECASE
    ):
        return commit_hash_search.group(0)

    return None


def get_first_commit_hash_from_where(where: str) -> str | None:
    if commit_hash_search := re.search(r"( |\[|\()[0-9a-f]{40}(\]|\)|)", where):
        return commit_hash_search.group(0)

    return None


async def update_vulnerability(*, vulnerability: Vulnerability) -> None:
    loaders = get_new_context()
    current_commit_hash = get_commit_hash_from_vulnerability_where(vulnerability.state.where)
    vulns_state = await loaders.vulnerability_historic_state.load(
        VulnerabilityRequest(finding_id=vulnerability.finding_id, vulnerability_id=vulnerability.id)
    )
    if not vulns_state:
        return

    first_commit_hash = get_first_commit_hash_from_where(vulns_state[0].where)
    if not first_commit_hash:
        return
    first_commit_hash = get_commit_hash(first_commit_hash)

    if (
        not current_commit_hash
        or first_commit_hash not in current_commit_hash
        or current_commit_hash[0] == "["
    ):
        commit_hash_search = get_commit_hash_from_vulnerability_where(
            vulnerability.state.where
        ) or get_first_commit_hash_from_where(vulnerability.state.where)
        where = (
            vulnerability.state.where.replace(commit_hash_search, "")
            if commit_hash_search
            else vulnerability.state.where
        )
        where += (
            f"(commit {first_commit_hash})"
            if where[-1] == " "
            else f" (commit {first_commit_hash})"
        )
        LOGGER.info(
            "Different where",
            extra={
                "extra": {
                    "where": where,
                    "vuln_id": vulnerability.id,
                    "first_commit_hash": first_commit_hash,
                    "current_commit_hash": current_commit_hash,
                    "vuln_where": vulnerability.state.where,
                    "group_name": vulnerability.group_name,
                }
            },
        )
        await update_historic_entry(
            current_value=vulnerability,
            entry=vulnerability.state._replace(
                modified_by="integrates@fluidattacks.com",
                modified_date=get_utc_now(),
                where=where,
            ),
            finding_id=vulnerability.finding_id,
            vulnerability_id=vulnerability.id,
        )


async def process_group(group: Group) -> None:
    loaders = get_new_context()
    vulnerabilities = [
        edge.node
        for edge in (
            await loaders.group_vulnerabilities.load(
                GroupVulnerabilitiesRequest(group_name=group.name)
            )
        ).edges
    ]

    await collect(
        [
            update_vulnerability(vulnerability=vulnerability)
            for vulnerability in vulnerabilities
            if vulnerability.type == VulnerabilityType.LINES
        ],
        workers=16,
    )


@log_time(LOGGER)
async def main() -> None:
    _all_groups = await get_all_groups(get_new_context())
    all_groups = sorted(_all_groups, key=lambda x: x.name, reverse=True)
    await collect([process_group(group) for group in all_groups], workers=4)


if __name__ == "__main__":
    main()  # type: ignore[unused-coroutine]
