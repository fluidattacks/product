"""
Fix cve advisor data field in vulnerability.
Start Time:    2023-09-15 at 18:20:38 UTC
Finalization Time: 2023-09-15 at 19:17:15 UTC

"""

import logging
import logging.config
import time

from aioextensions import (
    collect,
    run,
)

from integrates.custom_exceptions import (
    GroupNotFound,
)
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model.vulnerabilities.types import (
    Vulnerability,
)
from integrates.db_model.vulnerabilities.update import (
    update_historic_entry,
)
from integrates.organizations.domain import (
    get_all_group_names,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)

# Constants
LOGGER = logging.getLogger(__name__)
LOGGER_CONSOLE = logging.getLogger("console")


async def process_vulnerability_where(vulnerability: Vulnerability, loader: Dataloaders) -> None:
    if vulnerability.state.advisories and isinstance(vulnerability.state.advisories.cve, str):
        states = await loader.vulnerability_historic_state.load(vulnerability.id)
        states = states[-1:]
        for state in reversed(states):
            if state.advisories and isinstance(state.advisories.cve, str):
                LOGGER_CONSOLE.info(
                    "Vuln processed %s %s",
                    vulnerability.id,
                    state.advisories.cve,
                )
                await update_historic_entry(
                    current_value=vulnerability,
                    finding_id=vulnerability.finding_id,
                    entry=state._replace(
                        advisories=state.advisories._replace(cve=[state.advisories.cve]),
                    ),
                    vulnerability_id=vulnerability.id,
                    force_update=True,
                )
                return


async def process_group(
    loaders: Dataloaders,
    group_name: str,
    progress: float,
) -> None:
    group = await loaders.group.load(group_name)
    if not group:
        raise GroupNotFound()
    group_findings = await loaders.group_findings_all.load(group_name)
    findings_filtered = [
        finding
        for finding in group_findings
        if finding.state.status and any(finding.title.startswith(code) for code in ["011", "393"])
    ]

    if not findings_filtered:
        return
    group_vulns = await loaders.finding_vulnerabilities_all.load_many_chained(
        [finding.id for finding in findings_filtered],
    )

    await collect(
        tuple(process_vulnerability_where(vuln, loaders) for vuln in group_vulns),
        workers=16,
    )
    LOGGER_CONSOLE.info("Group processed %s %s", group_name, f"{round(progress, 2)!s}")


async def main() -> None:
    loaders: Dataloaders = get_new_context()
    group_names = sorted(await get_all_group_names(loaders=loaders))
    LOGGER_CONSOLE.info("%s", f"{group_names=}")
    LOGGER_CONSOLE.info("%s", f"{len(group_names)=}")
    await collect(
        tuple(
            process_group(
                loaders=loaders,
                group_name=group,
                progress=count / len(group_names),
            )
            for count, group in enumerate(group_names)
        ),
        workers=16,
    )


if __name__ == "__main__":
    execution_time = time.strftime("Start Time:    %Y-%m-%d at %H:%M:%S UTC")
    LOGGER_CONSOLE.info("%s", execution_time)
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S UTC")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
