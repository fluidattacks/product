"""
Execution Time: 2025-03-05 at 20:12:08 UTC
Finalization Time: 2025-03-05 at 20:17:46 UTC
Execution Time: 2025-03-05 at 20:46:45 UTC
Finalization Time: 2025-03-05 at 20:47:47 UTC
Execution Time: 2025-03-05 at 21:00:29 UTC
Finalization Time: 2025-03-05 at 21:03:19 UTC
Execution Time: 2025-03-05 at 21:05:27 UTC
Finalization Time: 2025-03-05 at 21:06:05 UTC

Move some Environment to URLRoot
"""

import itertools
import logging
import logging.config
import uuid
from collections.abc import Callable, Iterator
from datetime import datetime
from operator import attrgetter

from aioextensions import collect
from aiohttp.client_exceptions import ClientPayloadError

from integrates.context import FI_AWS_S3_MAIN_BUCKET, FI_AWS_S3_PATH_PREFIX
from integrates.custom_exceptions import (
    RepeatedToeInput,
    ToeInputAlreadyUpdated,
    ToeInputNotFound,
    UnavailabilityError,
)
from integrates.custom_utils import filter_vulnerabilities as filter_vulns_utils
from integrates.custom_utils import findings as findings_utils
from integrates.custom_utils import roots as roots_utils
from integrates.custom_utils.datetime import get_utc_now
from integrates.custom_utils.groups import get_group
from integrates.custom_utils.vulnerabilities import close_vulnerability
from integrates.dataloaders import Dataloaders, get_new_context
from integrates.db_model import findings as findings_model
from integrates.db_model import roots as roots_model
from integrates.db_model import vulnerabilities as vulns_model
from integrates.db_model.constants import MACHINE_EMAIL
from integrates.db_model.enums import TreatmentStatus
from integrates.db_model.findings.enums import FindingStateStatus
from integrates.db_model.findings.types import Finding, FindingState
from integrates.db_model.roots.enums import (
    RootEnvironmentUrlStateStatus,
    RootEnvironmentUrlType,
    RootStateReason,
)
from integrates.db_model.roots.types import (
    GitRoot,
    Root,
    RootEnvironmentSecretsRequest,
    RootEnvironmentUrl,
    RootEnvironmentUrlsRequest,
)
from integrates.db_model.toe_inputs.types import GroupToeInputsRequest, ToeInput, ToeInputRequest
from integrates.db_model.types import Treatment
from integrates.db_model.vulnerabilities.enums import (
    VulnerabilityStateReason,
    VulnerabilityStateStatus,
    VulnerabilityType,
)
from integrates.db_model.vulnerabilities.types import (
    Vulnerability,
    VulnerabilityRequest,
    VulnerabilityState,
    VulnerabilityVerification,
    VulnerabilityZeroRisk,
)
from integrates.decorators import retry_on_exceptions
from integrates.groups.domain import add_file
from integrates.migrations.utils import log_time
from integrates.roots import domain as roots_domain
from integrates.roots.types import UrlRootAttributesToAdd
from integrates.roots.utils import ensure_unique_nickname, format_root_nickname
from integrates.s3.resource import get_client
from integrates.settings import LOGGING
from integrates.toe.inputs import domain as toe_inputs_domain
from integrates.toe.inputs.types import ToeInputAttributesToAdd, ToeInputAttributesToUpdate
from integrates.vulnerabilities import domain as vulns_domain

logging.config.dictConfig(LOGGING)

# Constants
LOGGER = logging.getLogger("migrations")
SOURCE_GROUP_NAME = ""
TARGET_GROUP_NAME = ""
MODIFIED_BY = "integrates@fluidattacks.com"


toe_inputs_add = retry_on_exceptions(exceptions=(UnavailabilityError,), sleep_seconds=5)(
    toe_inputs_domain.add,
)


@retry_on_exceptions(exceptions=(ToeInputAlreadyUpdated, UnavailabilityError), sleep_seconds=5)
async def update_toe_inputs(
    loaders: Dataloaders,
    toe_input: ToeInput,
    attributes_to_update: ToeInputAttributesToUpdate,
    target_root_id: str,
) -> None:
    try:
        await toe_inputs_domain.update(
            loaders=loaders,
            current_value=toe_input,
            attributes=attributes_to_update,
            modified_by=MODIFIED_BY,
            is_moving_toe_input=True,
        )
    except ToeInputAlreadyUpdated:
        key = ToeInputRequest(
            component=toe_input.component,
            entry_point=toe_input.entry_point,
            group_name=TARGET_GROUP_NAME,
            root_id=target_root_id,
        )
        loaders.toe_input.clear(key)
        _toe_input = await loaders.toe_input.load(key)
        if not _toe_input or not _toe_input.state.be_present:
            return
        await toe_inputs_domain.update(
            loaders=loaders,
            current_value=_toe_input,
            attributes=attributes_to_update,
            modified_by=MODIFIED_BY,
            is_moving_toe_input=True,
        )


async def _update_historic(
    loaders: Dataloaders,
    new_id: str,
    modified_date: datetime,
    item_subject: str,
    to_update: Callable,
    historic_state: list[VulnerabilityState],
    historic_treatment: list[Treatment],
    historic_verification: list[VulnerabilityVerification],
    historic_zero_risk: list[VulnerabilityZeroRisk],
) -> None:
    new_historic_treatment = Treatment(
        modified_by=item_subject,
        modified_date=modified_date,
        justification="Environment was moved to the current root",
        status=historic_treatment[-1].status if historic_treatment else TreatmentStatus.UNTREATED,
        acceptance_status=historic_treatment[-1].acceptance_status if historic_treatment else None,
        accepted_until=historic_treatment[-1].accepted_until if historic_treatment else None,
        assigned=historic_treatment[-1].assigned if historic_treatment else None,
    )
    await to_update(
        current_value=await vulns_domain.get_vulnerability(loaders, new_id, clear_loader=True),
        historic=tuple(
            [*historic_treatment, new_historic_treatment]
            if historic_treatment
            else [new_historic_treatment]
        ),
    )

    if historic_state:
        await to_update(
            current_value=await vulns_domain.get_vulnerability(loaders, new_id, clear_loader=True),
            historic=tuple(historic_state),
        )
    if historic_verification:
        await to_update(
            current_value=await vulns_domain.get_vulnerability(loaders, new_id, clear_loader=True),
            historic=tuple(historic_verification),
        )
    if historic_zero_risk:
        await to_update(
            current_value=await vulns_domain.get_vulnerability(loaders, new_id, clear_loader=True),
            historic=tuple(historic_zero_risk),
        )


async def _process_vuln(
    *,
    loaders: Dataloaders,
    vuln: Vulnerability,
    target_finding_id: str,
    target_group_name: str,
    target_root_id: str,
    item_subject: str,
    timestamp: datetime,
) -> str:
    LOGGER.info(
        "Processing vuln",
        extra={
            "extra": {
                "vuln_id": vuln.id,
                "target_finding_id": target_finding_id,
                "target_root_id": target_root_id,
            },
        },
    )
    historic_state = await loaders.vulnerability_historic_state.load(
        VulnerabilityRequest(vulnerability_id=vuln.id, finding_id=vuln.finding_id),
    )
    historic_treatment = await loaders.vulnerability_historic_treatment.load(
        VulnerabilityRequest(vulnerability_id=vuln.id, finding_id=vuln.finding_id),
    )
    historic_verification = await loaders.vulnerability_historic_verification.load(
        VulnerabilityRequest(vulnerability_id=vuln.id, finding_id=vuln.finding_id),
    )
    historic_zero_risk = await loaders.vulnerability_historic_zero_risk.load(
        VulnerabilityRequest(vulnerability_id=vuln.id, finding_id=vuln.finding_id),
    )
    new_id = str(uuid.uuid4())
    await vulns_model.add(
        vulnerability=vuln._replace(
            finding_id=target_finding_id,
            group_name=target_group_name,
            id=new_id,
            root_id=target_root_id,
            state=vuln.state,
            verification=VulnerabilityVerification(
                event_id=None,
                modified_by=vuln.verification.modified_by,
                modified_date=vuln.verification.modified_date,
                status=vuln.verification.status,
            )
            if vuln.verification
            else None,
            event_id=None,
        ),
    )
    LOGGER.info(
        "Created new vuln",
        extra={
            "extra": {
                "target_finding_id": target_finding_id,
                "new_id": new_id,
            },
        },
    )
    await _update_historic(
        loaders,
        new_id,
        timestamp,
        item_subject,
        vulns_model.update_historic,
        historic_state,
        historic_treatment,
        historic_verification,
        historic_zero_risk,
    )

    await _update_historic(
        loaders,
        new_id,
        timestamp,
        item_subject,
        vulns_model.update_new_historic,
        historic_state,
        historic_treatment,
        historic_verification,
        historic_zero_risk,
    )

    await close_vulnerability(
        vulnerability=vuln,
        modified_by=item_subject,
        loaders=loaders,
        closing_reason=VulnerabilityStateReason.MOVED_TO_ANOTHER_GROUP,
    )

    return new_id


async def _get_target_finding(
    *,
    loaders: Dataloaders,
    source_finding: Finding,
    target_group_name: str,
) -> Finding | None:
    target_group_findings = await loaders.group_findings.load(target_group_name)
    source_created_by = (
        source_finding.creation.modified_by
        if source_finding.creation
        else source_finding.state.modified_by
    )
    if source_created_by == MACHINE_EMAIL:
        return next(
            (
                finding
                for finding in target_group_findings
                if finding.get_criteria_code() == source_finding.get_criteria_code()
                and finding.creation
                and finding.creation.modified_by == MACHINE_EMAIL
            ),
            None,
        )

    return next(
        (
            finding
            for finding in target_group_findings
            if finding.title == source_finding.title
            and finding.description == source_finding.description
            and finding.recommendation == source_finding.recommendation
        ),
        None,
    )


async def _process_finding(
    *,
    loaders: Dataloaders,
    source_group_name: str,
    target_group_name: str,
    target_root_id: str,
    source_finding_id: str,
    vulns: tuple[Vulnerability, ...],
    item_subject: str,
    timestamp: datetime,
) -> None:
    if not vulns:
        return

    LOGGER.info(
        "Processing finding",
        extra={
            "extra": {
                "source_group_name": source_group_name,
                "target_group_name": target_group_name,
                "target_root_id": target_root_id,
                "source_finding_id": source_finding_id,
                "vulns": len(vulns),
            },
        },
    )
    source_finding = await loaders.finding.load(source_finding_id)
    if source_finding is None or findings_utils.is_deleted(source_finding):
        return

    target_finding = await _get_target_finding(
        loaders=loaders,
        source_finding=source_finding,
        target_group_name=target_group_name,
    )
    if target_finding:
        target_finding_id = target_finding.id
        LOGGER.info(
            "Found equivalent finding in target_group_findings",
            extra={
                "extra": {
                    "target_group_name": target_group_name,
                    "target_finding_id": target_finding_id,
                },
            },
        )
    else:
        target_finding_id = str(uuid.uuid4())
        if source_finding.creation:
            initial_state = FindingState(
                modified_by=source_finding.creation.modified_by,
                modified_date=source_finding.creation.modified_date,
                source=source_finding.creation.source,
                status=FindingStateStatus.CREATED,
            )
        await findings_model.add(
            finding=Finding(
                attack_vector_description=(source_finding.attack_vector_description),
                description=source_finding.description,
                group_name=target_group_name,
                id=target_finding_id,
                state=initial_state,
                recommendation=source_finding.recommendation,
                requirements=source_finding.requirements,
                severity_score=source_finding.severity_score,
                title=source_finding.title,
                threat=source_finding.threat,
                unfulfilled_requirements=(source_finding.unfulfilled_requirements),
            ),
        )
        LOGGER.info(
            "Equivalent finding not found. Created new one",
            extra={
                "extra": {
                    "target_group_name": target_group_name,
                    "target_finding_id": target_finding_id,
                },
            },
        )

    await collect(
        tuple(
            _process_vuln(
                loaders=loaders,
                vuln=vuln,
                target_finding_id=target_finding_id,
                target_group_name=target_group_name,
                target_root_id=target_root_id,
                item_subject=item_subject,
                timestamp=timestamp,
            )
            for vuln in vulns
        ),
        workers=10,
    )


@retry_on_exceptions(
    exceptions=(ToeInputAlreadyUpdated,),
)
async def _process_toe_input(
    loaders: Dataloaders,
    target_group_name: str,
    target_root_id: str,
    toe_input: ToeInput,
    vulns: tuple[Vulnerability, ...],
) -> None:
    if toe_input.state.seen_at is None:
        return
    attributes_to_add = ToeInputAttributesToAdd(
        attacked_at=toe_input.state.attacked_at,
        attacked_by=toe_input.state.attacked_by,
        be_present=True,
        first_attack_at=toe_input.state.first_attack_at,
        has_vulnerabilities=len(vulns) > 0,
        seen_first_time_by=toe_input.state.seen_first_time_by,
        seen_at=toe_input.state.seen_at,
    )
    try:
        await toe_inputs_add(
            loaders=loaders,
            entry_point=toe_input.entry_point,
            environment_id=toe_input.environment_id,
            component=toe_input.component,
            group_name=target_group_name,
            root_id=target_root_id,
            attributes=attributes_to_add,
            is_moving_toe_input=True,
        )
    except RepeatedToeInput as exc:
        current_value = await loaders.toe_input.load(
            ToeInputRequest(
                component=toe_input.component,
                entry_point=toe_input.entry_point,
                group_name=target_group_name,
                root_id=target_root_id,
            ),
        )
        attributes_to_update = ToeInputAttributesToUpdate(
            attacked_at=toe_input.state.attacked_at,
            attacked_by=toe_input.state.attacked_by,
            be_present=True,
            first_attack_at=toe_input.state.first_attack_at,
            has_vulnerabilities=len(vulns) > 0,
            seen_at=toe_input.state.seen_at,
            seen_first_time_by=toe_input.state.seen_first_time_by,
        )
        if current_value:
            await update_toe_inputs(
                loaders=loaders,
                toe_input=current_value,
                attributes_to_update=attributes_to_update,
                target_root_id=target_root_id,
            )
        else:
            raise ToeInputNotFound() from exc


async def move_environment_url(
    *,
    loaders: Dataloaders,
    environment_url: RootEnvironmentUrl,
    root: Root,
    target_root: Root,
) -> None:
    LOGGER.info("Moving environment url to root")
    environment_secrets = await loaders.environment_secrets.load(
        RootEnvironmentSecretsRequest(url_id=environment_url.id, group_name=root.group_name),
    )

    await roots_domain.remove_environment_url_id(
        loaders=loaders,
        root_id=root.id,
        url_id=environment_url.id,
        user_email=environment_url.state.modified_by,
        group_name=environment_url.group_name,
        should_notify=False,
    )

    await collect(
        tuple(
            roots_model.remove_environment_url_secret(
                group_name=root.group_name,
                resource_id=environment_url.id,
                secret_key=environment_secret.key,
            )
            for environment_secret in environment_secrets
        ),
    )
    await collect(
        tuple(
            roots_model.add_root_environment_secret(
                group_name=target_root.group_name,
                resource_id=target_root.id,
                secret=environment_secret,
            )
            for environment_secret in environment_secrets
        ),
    )


async def move_toes(
    *,
    loaders: Dataloaders,
    toe_inputs: tuple[ToeInput, ...],
    vulns: tuple[Vulnerability, ...],
    target_root: Root,
) -> None:
    await collect(
        tuple(
            _process_toe_input(
                loaders,
                target_root.group_name,
                target_root.id,
                toe_input,
                vulns=tuple(
                    vuln
                    for vuln in vulns
                    if vuln.state.status == VulnerabilityStateStatus.VULNERABLE
                    and vuln.state.where == toe_input.component
                    and vuln.state.specific == toe_input.entry_point
                ),
            )
            for toe_input in toe_inputs
        ),
        workers=10,
    )

    attributes_to_update = ToeInputAttributesToUpdate(be_present=False)
    await collect(
        tuple(
            update_toe_inputs(
                loaders=loaders,
                toe_input=toe_input,
                attributes_to_update=attributes_to_update,
                target_root_id=target_root.id,
            )
            for toe_input in toe_inputs
        ),
        workers=10,
    )


async def copy_file(file_name: str) -> None:
    client = await get_client()
    try:
        await client.copy(
            {
                "Bucket": FI_AWS_S3_MAIN_BUCKET,
                "Key": f"{FI_AWS_S3_PATH_PREFIX}resources/{SOURCE_GROUP_NAME}/{file_name}",
            },
            FI_AWS_S3_MAIN_BUCKET,
            f"{FI_AWS_S3_PATH_PREFIX}resources/{TARGET_GROUP_NAME}/{file_name}",
        )
    except ClientPayloadError as ex:
        LOGGER.exception(ex, extra={"extra": locals()})
        raise UnavailabilityError().new() from ex


async def get_target_root(
    loaders: Dataloaders, source_env: RootEnvironmentUrl
) -> tuple[Root, Root, tuple[ToeInput, ...], tuple[Vulnerability, ...]]:
    source_root_id = source_env.root_id
    root = await roots_utils.get_root(loaders, source_root_id, SOURCE_GROUP_NAME)
    source_group = await get_group(loaders, SOURCE_GROUP_NAME)

    if source_env.state.url_type == RootEnvironmentUrlType.APK and source_group.files:
        file_origin = next(f for f in source_group.files if f.file_name == source_env.url)
        await copy_file(source_env.url)
        await add_file(
            loaders=get_new_context(),
            description=file_origin.description,
            file_name=file_origin.file_name,
            group_name=TARGET_GROUP_NAME,
            email=file_origin.modified_by,
            should_notify=False,
            should_queue=False,
        )

    nickname = await roots_domain._format_root_nickname_to_add(
        loaders=loaders,
        group_name=TARGET_GROUP_NAME,
        raw_nickname=ensure_unique_nickname(
            format_root_nickname("", source_env.url),
            await loaders.group_roots.load(TARGET_GROUP_NAME),
            source_env.url,
        ),
        url=source_env.url,
    )
    target_root = await roots_domain.add_url_root(
        loaders=loaders,
        attributes_to_add=UrlRootAttributesToAdd(
            nickname=nickname,
            url=f"file://{source_env.url}"
            if source_env.state.url_type == RootEnvironmentUrlType.APK
            else source_env.url,
            reason=RootStateReason.MOVED_FROM_ANOTHER_GROUP,
        ),
        group_name=TARGET_GROUP_NAME,
        user_email=MODIFIED_BY,
        ensure_org_uniqueness=False,
    )

    group_toe_inputs = await loaders.group_toe_inputs.load_nodes(
        GroupToeInputsRequest(group_name=SOURCE_GROUP_NAME)
    )
    env_toe_inputs = tuple(
        toe_input
        for toe_input in group_toe_inputs
        if toe_input.root_id == source_root_id and source_env.url in toe_input.component
    )
    locations = list(set(input.component for input in env_toe_inputs))

    root_vulns = await loaders.root_vulnerabilities.load(root.id)
    env_vulns = tuple(
        vuln
        for vuln in filter_vulns_utils.filter_non_deleted(root_vulns)
        if vuln.type == VulnerabilityType.INPUTS and vuln.state.where in locations
    )

    return (root, target_root, env_toe_inputs, env_vulns)


async def move_environment(source_env: RootEnvironmentUrl) -> None:
    loaders: Dataloaders = get_new_context()

    LOGGER.info("Moving environment", extra={"extra": {"env": source_env}})
    (
        root,
        target_root,
        env_toe_inputs,
        env_vulns,
    ) = await get_target_root(loaders, source_env)

    await move_toes(
        loaders=loaders,
        toe_inputs=env_toe_inputs,
        vulns=env_vulns,
        target_root=target_root,
    )

    await move_environment_url(
        loaders=loaders,
        environment_url=source_env,
        root=root,
        target_root=target_root,
    )

    env_vulns_by_finding: Iterator[tuple] = itertools.groupby(
        sorted(env_vulns, key=attrgetter("finding_id")),
        key=attrgetter("finding_id"),
    )

    await collect(
        tuple(
            _process_finding(
                loaders=loaders,
                source_group_name=SOURCE_GROUP_NAME,
                target_group_name=TARGET_GROUP_NAME,
                target_root_id=target_root.id,
                source_finding_id=source_finding_id,
                vulns=tuple(vulns),
                item_subject=MODIFIED_BY,
                timestamp=get_utc_now(),
            )
            for source_finding_id, vulns in env_vulns_by_finding
            if vulns
        ),
        workers=10,
    )

    LOGGER.info("Environment moved successfully", extra={"extra": {"env": source_env}})


@log_time(LOGGER)
async def main() -> None:
    environments_urls: list[RootEnvironmentUrl] = []
    loaders = get_new_context()
    group_roots = [
        root
        for root in await loaders.group_roots.load(SOURCE_GROUP_NAME)
        if isinstance(root, GitRoot)
    ]
    for root in group_roots:
        envs = await loaders.root_environment_urls.load(
            RootEnvironmentUrlsRequest(root.id, root.group_name)
        )
        for env in envs:
            if env.state.status != RootEnvironmentUrlStateStatus.DELETED:
                environments_urls.append(env)

    await collect((move_environment(env) for env in environments_urls), workers=1)


if __name__ == "__main__":
    main()  # type: ignore[unused-coroutine]
