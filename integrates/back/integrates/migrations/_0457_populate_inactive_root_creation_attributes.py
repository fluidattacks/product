"""
Populate the creation attributes for the roots

Execution Time:    2023-11-20 at 15:43:28 UTC
Finalization Time: 2023-11-20 at 15:43:50 UTC
"""

import logging
import logging.config
import time

from aioextensions import (
    collect,
    run,
)
from boto3.dynamodb.conditions import (
    Attr,
)

from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model import (
    TABLE,
)
from integrates.db_model.roots.types import (
    Root,
)
from integrates.dynamodb import (
    keys,
    operations,
)
from integrates.organizations import (
    domain as orgs_domain,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)

# Constants
LOGGER = logging.getLogger(__name__)
LOGGER_CONSOLE = logging.getLogger("console")


async def process_root(root: Root) -> None:
    root_types = {
        "Git": "git_root_metadata",
        "IP": "ip_root_metadata",
        "URL": "url_root_metadata",
    }
    creation_attributes = [
        {
            "modified_by": "unknown@unknown.com",
            "modified_date": "2023-11-17T18:00:00+00:00",
        },
    ]
    creation_state = creation_attributes[0]
    modified_by = creation_state["modified_by"]
    modified_date = creation_state["modified_date"]
    primary_key = keys.build_key(
        facet=TABLE.facets[root_types[root.type]],
        values={"name": root.group_name, "uuid": root.id},
    )
    key_structure = TABLE.primary_key
    item = {
        "created_by": modified_by,
        "created_date": modified_date,
    }
    LOGGER_CONSOLE.info("item %s", item)
    await operations.update_item(
        condition_expression=Attr(key_structure.partition_key).exists(),
        item=item,
        key=primary_key,
        table=TABLE,
    )


async def get_unprocessed_org_roots(
    loaders: Dataloaders,
    org_name: str,
) -> list[Root]:
    roots = [
        root for root in await loaders.organization_roots.load(org_name) if root.created_by == "-"
    ]
    return roots


async def main() -> None:
    loaders: Dataloaders = get_new_context()
    org_names = [organization.name async for organization in orgs_domain.iterate_organizations()]
    all_org_roots = await collect(
        get_unprocessed_org_roots(loaders, org_name) for org_name in org_names
    )
    await collect(
        tuple(process_root(root) for org_roots in all_org_roots for root in org_roots),
        workers=100,
    )


if __name__ == "__main__":
    execution_time = time.strftime("Execution Time:    %Y-%m-%d at %H:%M:%S %Z")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S %Z")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
