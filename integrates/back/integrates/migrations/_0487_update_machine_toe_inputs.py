# type: ignore
"""
Update machine attacked toe_inputs
Execution Time:    2024-02-13 at 20:06:17 UTC
Finalization Time: 2024-02-13 at 20:45:46 UTC
"""

import asyncio
import csv
import time
from contextlib import (
    suppress,
)

from aioextensions import (
    collect,
    run,
)
from aiohttp import (
    ClientConnectorError,
    ClientError,
    ClientOSError,
    ClientPayloadError,
    ServerDisconnectedError,
    ServerTimeoutError,
)
from botocore.exceptions import (
    ClientError as BotoCLientError,
)
from botocore.exceptions import (
    ConnectTimeoutError,
    HTTPClientError,
    ReadTimeoutError,
)

from integrates.custom_exceptions import (
    ToeInputAlreadyUpdated,
)
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model.toe_inputs.types import (
    GroupToeInputsRequest,
    ToeInput,
)
from integrates.decorators import (
    retry_on_exceptions,
)
from integrates.dynamodb.exceptions import (
    UnavailabilityError,
)
from integrates.organizations import (
    domain as orgs_domain,
)
from integrates.toe.inputs import (
    domain as toe_inputs_domain,
)
from integrates.toe.inputs.types import (
    ToeInputAttributesToUpdate,
)

MACHINE_EMAIL = "machine@fluidattacks.com"


NETWORK_ERRORS = (
    BotoCLientError,
    asyncio.TimeoutError,
    ClientConnectorError,
    ClientError,
    ClientOSError,
    ClientPayloadError,
    ConnectionError,
    ConnectionResetError,
    ConnectTimeoutError,
    HTTPClientError,
    ReadTimeoutError,
    ServerDisconnectedError,
    ServerTimeoutError,
    UnavailabilityError,
)


@retry_on_exceptions(exceptions=NETWORK_ERRORS, max_attempts=2)
async def update_toe_input(
    loaders: Dataloaders,
    current_value: ToeInput,
    attributes: ToeInputAttributesToUpdate,
) -> None:
    with suppress(ToeInputAlreadyUpdated):
        await toe_inputs_domain.update(
            loaders=loaders,
            current_value=current_value,
            attributes=attributes,
            modified_by=MACHINE_EMAIL,
            is_moving_toe_input=True,
        )


@retry_on_exceptions(exceptions=NETWORK_ERRORS, max_attempts=2)
async def remove_toe_input(current_value: ToeInput) -> None:
    await toe_inputs_domain.remove(current_value=current_value)


async def process_toe_inputs(group_name: str) -> None:
    loaders = get_new_context()
    rows = []

    group_toe_inputs = await loaders.group_toe_inputs.load_nodes(
        GroupToeInputsRequest(group_name=group_name),
    )

    toe_inputs_to_update = [
        toe_input
        for toe_input in group_toe_inputs
        if toe_input.state.seen_first_time_by == MACHINE_EMAIL
        and not toe_input.component.endswith(("css", "jpg", "jpeg", "png", "svg"))
        and (not toe_input.state.first_attack_at or not toe_input.state.attacked_at)
    ]
    toe_update_coroutines = [
        update_toe_input(
            loaders,
            toe_input,
            ToeInputAttributesToUpdate(
                first_attack_at=toe_input.state.seen_at,
                attacked_at=toe_input.state.seen_at,
                attacked_by=MACHINE_EMAIL,
            ),
        )
        for toe_input in toe_inputs_to_update
    ]
    rows.extend([[toe.component, toe.group_name, "UPDATE"] for toe in toe_inputs_to_update])

    toe_inputs_to_remove = [
        toe_input
        for toe_input in group_toe_inputs
        if toe_input.state.seen_first_time_by == MACHINE_EMAIL
        and toe_input.component.endswith(("css", "jpg", "jpeg", "png", "svg"))
    ]
    toe_remove_coroutines = [remove_toe_input(toe_input) for toe_input in toe_inputs_to_remove]
    rows.extend([[toe.component, toe.group_name, "REMOVE"] for toe in toe_inputs_to_remove])
    with open("toe_inputs_update.csv", "a+", encoding="utf-8") as handler:
        writer = csv.writer(handler)
        writer.writerows(rows)

    await collect(toe_update_coroutines, workers=32)
    await collect(toe_remove_coroutines, workers=32)


async def main() -> None:
    loaders: Dataloaders = get_new_context()
    group_names = sorted(await orgs_domain.get_all_active_group_names(loaders))
    for group in group_names:
        print(f"Processing group: {group}")
        try:
            await process_toe_inputs(group)
        except Exception:
            print(f"An error occurred for group {group}")


if __name__ == "__main__":
    execution_time = time.strftime("Execution Time:    %Y-%m-%d at %H:%M:%S UTC")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S UTC")
    print(f"{execution_time}\n{finalization_time}")
