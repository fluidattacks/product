"""
Remove not longer in use field in organization

Start Time:        2024-02-14 at 17:27:12 UTC
Finalization Time: 2024-02-14 at 17:29:14 UTC
"""

import logging
import logging.config
import time

from aioextensions import (
    run,
)
from boto3.dynamodb.conditions import (
    Attr,
)

from integrates.custom_exceptions import (
    OrganizationNotFound,
)
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model import (
    TABLE,
)
from integrates.db_model.organizations.utils import (
    remove_org_id_prefix,
)
from integrates.dynamodb import (
    keys,
    operations,
)
from integrates.dynamodb.exceptions import (
    ConditionalCheckFailedException,
)
from integrates.organizations import (
    domain as orgs_domain,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)
LOGGER_CONSOLE = logging.getLogger("console")


async def update_metadata(*, organization_id: str, organization_name: str) -> None:
    key_structure = TABLE.primary_key
    primary_key = keys.build_key(
        facet=TABLE.facets["organization_metadata"],
        values={
            "id": remove_org_id_prefix(organization_id),
            "name": organization_name,
        },
    )
    try:
        await operations.update_item(
            condition_expression=Attr(key_structure.partition_key).exists(),
            item={"vulnerabilities_url": None},
            key=primary_key,
            table=TABLE,
        )
    except ConditionalCheckFailedException as ex:
        raise OrganizationNotFound() from ex


async def process_organization(organization_id: str, organization_name: str) -> None:
    await update_metadata(organization_id=organization_id, organization_name=organization_name)
    LOGGER_CONSOLE.info(
        "Organization",
        extra={
            "extra": {
                "organization_name": organization_name,
            },
        },
    )


async def main() -> None:
    loaders: Dataloaders = get_new_context()
    async for org_id, org_name, _ in orgs_domain.iterate_organizations_and_groups(loaders):
        await process_organization(org_id, org_name)


if __name__ == "__main__":
    execution_time = time.strftime("Start Time:        %Y-%m-%d at %H:%M:%S UTC")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S UTC")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
