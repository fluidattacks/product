# type: ignore
"""
Populate state field in environments secrets

Start Time: 2024-04-16 at 17:03:25 UTC
Finalization Time: 2024-04-16 at 17:04:45 UTC
"""

import logging
import logging.config
import time

from aioextensions import (
    collect,
    run,
)
from boto3.dynamodb.conditions import (
    Attr,
)

from integrates.custom_utils.datetime import (
    get_iso_date,
)
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model import (
    TABLE,
)
from integrates.db_model.roots.types import (
    Root,
    RootEnvironmentSecretsRequest,
    RootEnvironmentUrl,
    RootEnvironmentUrlsRequest,
    Secret,
)
from integrates.db_model.utils import (
    get_as_utc_iso_format,
)
from integrates.dynamodb import (
    keys,
    operations,
)
from integrates.organizations import (
    domain as orgs_domain,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)
LOGGER_CONSOLE = logging.getLogger("console")
EMAIL_INTEGRATES = "integrates@fluidattacks.com"


async def process_environment_secret(
    old_secret: Secret,
    group_name: str,
    environment: RootEnvironmentUrl,
    root: Root,
) -> None:
    LOGGER_CONSOLE.info("Processing secret  %s", old_secret.key)
    key_structure = TABLE.primary_key
    secret_key = keys.build_key(
        facet=TABLE.facets["root_environment_secret"],
        values={
            "group_name": group_name,
            "hash": environment.id,
            "key": old_secret.key,
        },
    )
    state_item = {
        "owner": old_secret.state.owner,
        "description": old_secret.state.description
        if old_secret.state.description
        else old_secret.description,
        "modified_by": EMAIL_INTEGRATES,
        "modified_date": get_iso_date(),
    }
    secret_item = {
        "key": old_secret.key,
        "value": old_secret.value,
        "description": None,
        "created_at": get_as_utc_iso_format(old_secret.created_at)
        if old_secret.created_at
        else get_as_utc_iso_format(root.created_date),
        "state": state_item,
    }
    await operations.update_item(
        condition_expression=Attr(key_structure.partition_key).exists(),
        item=secret_item,
        key=secret_key,
        table=TABLE,
    )
    LOGGER_CONSOLE.info("Secret processed  %s", old_secret.key)


async def process_environment_url(
    loaders: Dataloaders,
    group_name: str,
    environment: RootEnvironmentUrl,
    root: Root,
) -> None:
    LOGGER_CONSOLE.info("Processing environment  %s", environment.id)
    environment_secrets = await loaders.environment_secrets.load(
        RootEnvironmentSecretsRequest(url_id=environment.id, group_name=group_name),
    )
    await collect(
        tuple(
            process_environment_secret(environment_secret, group_name, environment, root)
            for environment_secret in environment_secrets
        ),
        workers=32,
    )
    LOGGER_CONSOLE.info("Environment processed  %s", environment.id)


async def process_root(loaders: Dataloaders, root: Root, group_name: str) -> None:
    LOGGER_CONSOLE.info("Processing root  %s", root.id)
    environments_urls = await loaders.root_environment_urls.load(
        RootEnvironmentUrlsRequest(root_id=root.id, group_name=group_name),
    )
    await collect(
        tuple(
            process_environment_url(loaders, group_name, environment, root)
            for environment in environments_urls
        ),
        workers=32,
    )
    LOGGER_CONSOLE.info("Root processed  %s", root.id)


async def process_group(
    loaders: Dataloaders,
    group_name: str,
) -> None:
    group_roots = await loaders.group_roots.load(group_name)
    await collect(
        tuple(process_root(loaders, root, group_name) for root in group_roots),
        workers=32,
    )
    LOGGER_CONSOLE.info("Group processed  %s", group_name)


async def main() -> None:
    loaders: Dataloaders = get_new_context()
    all_group_names = sorted(await orgs_domain.get_all_group_names(loaders=loaders))
    count = 0
    LOGGER_CONSOLE.info("all_group_names %s", len(all_group_names))
    for group_name in all_group_names:
        count += 1
        LOGGER_CONSOLE.info("Processing group %s %s", group_name, count)
        await process_group(loaders, group_name)


if __name__ == "__main__":
    execution_time = time.strftime("Start Time:    %Y-%m-%d at %H:%M:%S UTC")
    LOGGER_CONSOLE.info("%s", execution_time)
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S UTC")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
