"""
Remove repeated fields line and lines
from the metadata item for all toe pckgs in db.
"""

import logging
import logging.config
import time
from itertools import (
    chain,
)
from typing import (
    Any,
)

from aioextensions import (
    collect,
    run,
)
from boto3.dynamodb.conditions import (
    Attr,
    Key,
)

from integrates.class_types.types import (
    Item,
)
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model import (
    HISTORIC_TABLE,
)
from integrates.dynamodb import (
    keys,
    operations,
)
from integrates.dynamodb.types import (
    Facet,
    PrimaryKey,
)
from integrates.organizations import (
    domain as orgs_domain,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)
LOGGER_CONSOLE = logging.getLogger("console")

ATTRS_TO_REMOVE = ["line", "lines"]

GSI_3_HISTORIC_STATE_FACET = Facet(
    attrs=HISTORIC_TABLE.facets["toe_package_state"].attrs,
    pk_alias="GROUP#PACKAGES#ROOT#PATH#PACKAGE_NAME#VERSION#group_name",
    sk_alias="STATE#state#DATE#iso8601utc",
)


async def process_packages_states(item: Item) -> dict[str, Any]:
    primary_key = PrimaryKey(partition_key=item["pk"], sort_key=item["sk"])
    key_structure = HISTORIC_TABLE.primary_key
    await operations.update_item(
        condition_expression=Attr(key_structure.partition_key).exists(),
        item={attr: None for attr in ATTRS_TO_REMOVE},
        key=primary_key,
        table=HISTORIC_TABLE,
    )

    return {
        "pkg_url": item["package_url"],
        "attrs_to_remove": (item["state"][attr] for attr in ATTRS_TO_REMOVE),
        "modified_date": item["modified_date"],
    }


async def get_state_packages_by_group(
    group_name: str,
) -> tuple[Item, ...]:
    facet = GSI_3_HISTORIC_STATE_FACET
    primary_key = keys.build_key(
        facet=facet,
        values={
            "group_name": group_name,
            "state": "state",
        },
    )
    index = HISTORIC_TABLE.indexes["gsi_2"]
    key_structure = index.primary_key
    response = await operations.query(
        condition_expression=Key(key_structure.partition_key).eq(primary_key.partition_key)
        & Key(key_structure.sort_key).begins_with(primary_key.sort_key),
        facets=(facet,),
        index=index,
        table=HISTORIC_TABLE,
    )
    return response.items


async def process_group(
    group_name: str,
    progress: float,
) -> list[dict[str, str]]:
    if not (group_packages := await get_state_packages_by_group(group_name)):
        print("No STATES")
        return []

    results = await collect(
        tuple(process_packages_states(item) for item in group_packages),
        workers=64,
    )
    LOGGER_CONSOLE.info(
        "Group processed %s %s %s",
        group_name,
        f"{round(progress, 2)!s}",
        f"{len(group_packages)=}",
    )
    return list(results)


async def main() -> None:
    loaders: Dataloaders = get_new_context()
    all_group_names = sorted(await orgs_domain.get_all_group_names(loaders=loaders))
    LOGGER_CONSOLE.info("%s", f"{all_group_names=}")
    LOGGER_CONSOLE.info("%s", f"{len(all_group_names)=}")
    results = list(
        chain.from_iterable(
            await collect(
                tuple(
                    process_group(
                        group_name=group,
                        progress=count / len(all_group_names),
                    )
                    for count, group in enumerate(all_group_names)
                ),
                workers=1,
            ),
        ),
    )
    LOGGER_CONSOLE.info("%s", f"{len(results)=}")


if __name__ == "__main__":
    execution_time = time.strftime("Start Time:        %Y-%m-%d at %H:%M:%S UTC")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S UTC")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
