"""
Populate organization historic policies in integrates_vms_historic table.

Start Time: 2024-03-01 at 20:35:17 UTC
Finalization Time: 2024-03-01 at 20:35:23 UTC
"""

import asyncio
import logging
import logging.config
import time

from aioextensions import (
    run,
)
from aiohttp.client_exceptions import (
    ClientConnectorError,
    ClientOSError,
    ClientPayloadError,
    ServerDisconnectedError,
    ServerTimeoutError,
)
from boto3.dynamodb.conditions import (
    Key,
)
from botocore.exceptions import (
    ClientError,
    ConnectTimeoutError,
    HTTPClientError,
    ReadTimeoutError,
)
from more_itertools import (
    chunked,
)

from integrates.class_types.types import (
    Item,
)
from integrates.db_model import (
    HISTORIC_TABLE,
    TABLE,
)
from integrates.db_model.organizations.constants import (
    ALL_ORGANIZATIONS_INDEX_METADATA,
)
from integrates.db_model.utils import (
    get_historic_gsi_sk,
)
from integrates.decorators import (
    retry_on_exceptions,
)
from integrates.dynamodb import (
    keys,
    operations,
)
from integrates.dynamodb.exceptions import (
    UnavailabilityError,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)
LOGGER_CONSOLE = logging.getLogger("console")
NETWORK_ERRORS = (
    asyncio.TimeoutError,
    ClientConnectorError,
    ClientError,
    ClientOSError,
    ClientPayloadError,
    ConnectionError,
    ConnectionResetError,
    ConnectTimeoutError,
    HTTPClientError,
    ReadTimeoutError,
    ServerDisconnectedError,
    ServerTimeoutError,
    UnavailabilityError,
)


batch_put_item = retry_on_exceptions(
    exceptions=NETWORK_ERRORS,
    sleep_seconds=300,
)(operations.batch_put_item)


async def _get_all_organizations() -> tuple[Item, ...]:
    primary_key = keys.build_key(
        facet=ALL_ORGANIZATIONS_INDEX_METADATA,
        values={"all": "all"},
    )
    index = TABLE.indexes["gsi_2"]
    key_structure = index.primary_key
    response = await operations.query(
        condition_expression=(
            Key(key_structure.partition_key).eq(primary_key.partition_key)
            & Key(key_structure.sort_key).begins_with(primary_key.sort_key)
        ),
        facets=(ALL_ORGANIZATIONS_INDEX_METADATA,),
        table=TABLE,
        index=index,
    )
    return response.items


@retry_on_exceptions(
    exceptions=NETWORK_ERRORS,
    sleep_seconds=10,
)
async def _get_historic_policies(
    organization_id: str,
) -> tuple[Item, ...]:
    primary_key = keys.build_key(
        facet=TABLE.facets["organization_historic_policies"],
        values={"id": organization_id},
    )
    key_structure = TABLE.primary_key
    response = await operations.query(
        condition_expression=(
            Key(key_structure.partition_key).eq(primary_key.partition_key)
            & Key(key_structure.sort_key).begins_with(primary_key.sort_key)
        ),
        facets=(TABLE.facets["organization_historic_policies"],),
        table=TABLE,
    )
    return response.items


def format_policies_item(policies_item: Item, organization_item: Item) -> Item:
    item_pk = f"{organization_item['pk']}#{organization_item['sk']}"
    item_sk = get_historic_gsi_sk("policies", policies_item["modified_date"])
    return {
        **(
            organization_item["policies"]
            if organization_item.get("policies")
            and policies_item["modified_date"] == organization_item["policies"]["modified_date"]
            else policies_item
        ),
        "pk": item_pk,
        "sk": item_sk,
    }


async def process_organization(organization_item: Item) -> None:
    organization_id = organization_item["id"]
    org_historic_policies: tuple[Item, ...] = await _get_historic_policies(
        organization_id=organization_id,
    )
    if not org_historic_policies and organization_item.get("policies"):
        org_historic_policies = (organization_item["policies"],)

    formatted_items = tuple(
        format_policies_item(policies_item, organization_item)
        for policies_item in org_historic_policies
    )
    for chunked_formatted_items in chunked(formatted_items, 25):
        await batch_put_item(
            items=tuple(chunked_formatted_items),
            table=HISTORIC_TABLE,
        )
    LOGGER_CONSOLE.info(
        "Organization processed",
        extra={
            "extra": {
                "pk": organization_item["pk"],
                "sk": organization_item["sk"],
                "org_historic_policies": len(org_historic_policies),
            },
        },
    )


async def main() -> None:
    organizations = await _get_all_organizations()
    LOGGER_CONSOLE.info(
        "All organizations",
        extra={
            "extra": {
                "total": len(organizations),
            },
        },
    )
    for count, organization in enumerate(organizations, start=1):
        LOGGER_CONSOLE.info(
            "Organization",
            extra={
                "extra": {
                    "pk": organization["pk"],
                    "sk": organization["sk"],
                    "count": count,
                },
            },
        )
        await process_organization(organization)


if __name__ == "__main__":
    execution_time = time.strftime("Start Time:        %Y-%m-%d at %H:%M:%S UTC")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S UTC")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
