"""
Distribute okada mailmap using a commits report

This migration downloads a commits report from S3, which indicates the
organizations where a given author has made commits.

Using this data, the migration constructs a dictionary that maps authors to
their respective organizations. The focus is on authors with only one
occurrence, and aliases are ignored for now.

For each author found in the okada mailmap, the migration attempts to create a
copy of the entry in each of the corresponding organizations.
At the end the author is removed from the okada mailmap.

Start Time:        2024-08-31 at 00:35:42 UTC
Finalization Time: 2024-08-31 at 00:43:38 UTC

"""

import csv
import logging
import logging.config
import os
import time

from aioextensions import (
    run,
)

from integrates.custom_exceptions import (
    ErrorDownloadingFile,
    MailmapEntryAlreadyExists,
    MailmapEntryNotFound,
    MailmapOrganizationNotFound,
    MailmapSubentryAlreadyExists,
    MailmapSubentryNotFound,
)
from integrates.mailmap.create import (
    create_mailmap_entry_with_subentries,
)
from integrates.mailmap.delete import (
    delete_mailmap_entry_with_subentries,
)
from integrates.mailmap.get import (
    get_mailmap_entry_with_subentries,
)
from integrates.s3.operations import (
    download_file,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)
LOGGER_CONSOLE = logging.getLogger("console")


ORGANIZATION_ID = "okada"
COMMITS_REPORT_FILE_PATH = "okada_mailmap_commits_by_author.csv"


async def download_commits_report_file(file_path: str) -> None:
    try:
        await download_file(
            file_name=f"mailmap-resources/{file_path}",
            file_path=file_path,
            bucket="integrates.dev",
        )
    except ErrorDownloadingFile as ex:
        LOGGER_CONSOLE.error("%s\n\n", ex)


def remove_commits_report_file(file_path: str) -> None:
    if os.path.exists(file_path):
        os.remove(file_path)
        LOGGER_CONSOLE.info("Commits report file removed successfully.\n\n")
    else:
        LOGGER_CONSOLE.error("Commits report file doesn't exist.\n\n")


def get_unique_authors_organizations_mapping(
    file_path: str,
) -> dict[str, list[str]]:
    unique_authors = {}

    with open(file_path, newline="", encoding="utf-8") as csv_file:
        reader = csv.DictReader(csv_file)

        for row in reader:
            author_email = row["author_entry"].split(" - ")[0]
            is_alias = row["alias"].strip().lower() == "true"
            organizations = row["company_present"].strip().split(", ")

            if author_email not in unique_authors and not is_alias:
                unique_authors[author_email] = organizations

    return unique_authors


async def main() -> None:
    await download_commits_report_file(COMMITS_REPORT_FILE_PATH)
    unique_authors = get_unique_authors_organizations_mapping(COMMITS_REPORT_FILE_PATH)
    remove_commits_report_file(COMMITS_REPORT_FILE_PATH)

    moved_entries = 0
    removed_entries = 0
    for author_email, organizations in unique_authors.items():
        try:
            entry_with_subentries = await get_mailmap_entry_with_subentries(
                entry_email=author_email,
                organization_id=ORGANIZATION_ID,
            )
            for org_name in organizations:
                try:
                    LOGGER_CONSOLE.info(
                        "Author %s can be moved to %s\n\n",
                        entry_with_subentries,
                        org_name,
                    )
                    # We use create instead of move to not remove it yet
                    await create_mailmap_entry_with_subentries(
                        entry_with_subentries=entry_with_subentries,
                        organization_id=org_name,
                    )
                    LOGGER_CONSOLE.info(
                        "Author %s moved to %s successfully\n\n",
                        entry_with_subentries,
                        org_name,
                    )
                    moved_entries += 1
                except (
                    MailmapOrganizationNotFound,
                    MailmapEntryAlreadyExists,
                    MailmapEntryNotFound,
                    MailmapSubentryAlreadyExists,
                    MailmapSubentryNotFound,
                ) as ex:
                    LOGGER_CONSOLE.error("Cannot copy author: %s\n\n", ex)
            try:
                await delete_mailmap_entry_with_subentries(
                    entry_email=author_email,
                    organization_id=ORGANIZATION_ID,
                )
                LOGGER_CONSOLE.info(
                    "Author %s removed successfully\n\n",
                    entry_with_subentries,
                )
                removed_entries += 1
            except (
                MailmapOrganizationNotFound,
                MailmapEntryAlreadyExists,
                MailmapEntryNotFound,
                MailmapSubentryAlreadyExists,
                MailmapSubentryNotFound,
            ) as ex:
                LOGGER_CONSOLE.error("Cannot remove author: %s\n\n", ex)
        except MailmapEntryNotFound as ex:
            LOGGER_CONSOLE.error("Author not found: %s\n\n", ex)

    LOGGER_CONSOLE.info(
        "Distributed entries: %s out of %s\n\n",
        moved_entries,
        len(unique_authors.keys()),
    )

    LOGGER_CONSOLE.info(
        "Deleted entries: %s out of %s\n\n",
        removed_entries,
        len(unique_authors.keys()),
    )


if __name__ == "__main__":
    execution_time = time.strftime("Start Time:        %Y-%m-%d at %H:%M:%S UTC")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S UTC")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
