"""
Remove field analyst_email from the metadata item for all findings in db.

Start Time:        2023-07-24 at 14:54:23 UTC
Finalization Time: 2023-07-24 at 15:08:46 UTC
"""

import csv
import logging
import logging.config
import time
from itertools import (
    chain,
)

from aioextensions import (
    collect,
    run,
)
from boto3.dynamodb.conditions import (
    Attr,
    Key,
)

from integrates.class_types.types import (
    Item,
)
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model import (
    TABLE,
)
from integrates.dynamodb import (
    keys,
    operations,
)
from integrates.dynamodb.types import (
    PrimaryKey,
)
from integrates.organizations import (
    domain as orgs_domain,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)
LOGGER_CONSOLE = logging.getLogger("console")

ATTR_TO_REMOVE = "analyst_email"


async def process_finding(item: Item) -> dict[str, str]:
    primary_key = PrimaryKey(partition_key=item["pk"], sort_key=item["sk"])
    key_structure = TABLE.primary_key
    await operations.update_item(
        condition_expression=Attr(key_structure.partition_key).exists(),
        item={ATTR_TO_REMOVE: None},
        key=primary_key,
        table=TABLE,
    )

    return {
        "group_name": item["group_name"],
        "finding_id": item["id"],
        "title": item["title"],
        ATTR_TO_REMOVE: item[ATTR_TO_REMOVE],
        "creation_modified_by": item["creation"]["modified_by"],
    }


async def get_findings_by_group(
    group_name: str,
) -> tuple[Item, ...]:
    primary_key = keys.build_key(
        facet=TABLE.facets["finding_metadata"],
        values={"group_name": group_name},
    )
    index = TABLE.indexes["inverted_index"]
    key_structure = index.primary_key
    response = await operations.query(
        condition_expression=(
            Key(key_structure.partition_key).eq(primary_key.sort_key)
            & Key(key_structure.sort_key).begins_with(primary_key.partition_key)
        ),
        filter_expression=Attr(ATTR_TO_REMOVE).exists(),
        facets=(TABLE.facets["finding_metadata"],),
        index=index,
        table=TABLE,
    )

    return response.items


async def process_group(
    group_name: str,
    progress: float,
) -> list[dict[str, str]]:
    if not (group_findings := await get_findings_by_group(group_name)):
        return []

    results = await collect(
        tuple(process_finding(item) for item in group_findings),
        workers=64,
    )
    LOGGER_CONSOLE.info(
        "Group processed %s %s %s",
        group_name,
        f"{round(progress, 2)!s}",
        f"{len(group_findings)=}",
    )
    return list(results)


async def main() -> None:
    loaders: Dataloaders = get_new_context()
    all_group_names = sorted(await orgs_domain.get_all_group_names(loaders=loaders))
    LOGGER_CONSOLE.info("%s", f"{all_group_names=}")
    LOGGER_CONSOLE.info("%s", f"{len(all_group_names)=}")
    results = list(
        chain.from_iterable(
            await collect(
                tuple(
                    process_group(
                        group_name=group,
                        progress=count / len(all_group_names),
                    )
                    for count, group in enumerate(all_group_names)
                ),
                workers=1,
            ),
        ),
    )
    LOGGER_CONSOLE.info("%s", f"{len(results)=}")

    csv_columns = [
        "group_name",
        "finding_id",
        "title",
        "analyst_email",
        "creation_modified_by",
    ]
    csv_file = "0420_results.csv"
    try:
        with open(csv_file, "w", encoding="utf8") as file:
            writer = csv.DictWriter(file, fieldnames=csv_columns)
            writer.writeheader()
            for data in results:
                writer.writerow(data)
    except OSError:
        LOGGER_CONSOLE.info("   === I/O error")


if __name__ == "__main__":
    execution_time = time.strftime("Start Time:        %Y-%m-%d at %H:%M:%S UTC")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S UTC")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
