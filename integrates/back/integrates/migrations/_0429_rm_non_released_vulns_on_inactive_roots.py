"""
Remove non released vulns (SUBMITTED, REJECTED) on inactive roots.
Those with VULNERABLE status should be closed by EXCLUSION.

Execution Time:    2023-11-15 at 16:56:20 UTC
Finalization Time: 2023-11-15 at 16:59:27 UTC
"""

import csv
import logging
import logging.config
import time
from itertools import (
    chain,
)

from aioextensions import (
    collect,
    run,
)

from integrates.custom_utils import (
    vulnerabilities as vulns_utils,
)
from integrates.custom_utils.filter_vulnerabilities import (
    filter_non_deleted,
    filter_non_zero_risk,
)
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model.roots.enums import (
    RootStatus,
)
from integrates.db_model.roots.types import (
    Root,
)
from integrates.db_model.vulnerabilities.enums import (
    VulnerabilityStateReason,
    VulnerabilityStateStatus,
)
from integrates.organizations import (
    domain as orgs_domain,
)
from integrates.settings import (
    LOGGING,
)
from integrates.vulnerabilities import (
    domain as vulns_domain,
)

logging.config.dictConfig(LOGGING)
LOGGER_CONSOLE = logging.getLogger("console")
EMAIL_INTEGRATES = "integrates@fluidattacks.com"


async def process_root(loaders: Dataloaders, root: Root) -> list[list[str]]:
    vulns_nzr = filter_non_zero_risk(
        filter_non_deleted(await loaders.root_vulnerabilities.load(root.id)),
    )
    # Non released vulns
    vulns_rejected_submitted = [
        vuln
        for vuln in vulns_nzr
        if vuln.state.status
        in {
            VulnerabilityStateStatus.REJECTED,
            VulnerabilityStateStatus.SUBMITTED,
        }
    ]
    # Released vulns still vulnerable
    vulns_vulnerable = [
        vuln for vuln in vulns_nzr if vuln.state.status == VulnerabilityStateStatus.VULNERABLE
    ]
    if not vulns_rejected_submitted + vulns_vulnerable:
        return []

    await collect(
        [
            vulns_domain.remove_vulnerability(
                loaders=loaders,
                finding_id=vuln.finding_id,
                vulnerability_id=vuln.id,
                justification=VulnerabilityStateReason.EXCLUSION,
                email=EMAIL_INTEGRATES,
            )
            for vuln in vulns_rejected_submitted
        ],
        workers=32,
    )
    await collect(
        [
            vulns_utils.close_vulnerability(
                vulnerability=vuln,
                modified_by=EMAIL_INTEGRATES,
                loaders=loaders,
                closing_reason=VulnerabilityStateReason.CONSISTENCY,
            )
            for vuln in vulns_vulnerable
        ],
        workers=32,
    )
    LOGGER_CONSOLE.info(
        "%s %s %s %s %s",
        f"{root.group_name=}",
        f"{root.id=}",
        f"{root.state.nickname=}",
        f"{len(vulns_rejected_submitted)=}",
        f"{len(vulns_vulnerable)=}",
    )

    return [
        [
            root.group_name,
            root.id,
            root.state.status.value,
            root.state.nickname,
            vuln.id,
            vuln.state.status.value,
        ]
        for vuln in vulns_rejected_submitted + vulns_vulnerable
    ]


async def process_group(
    *,
    loaders: Dataloaders,
    group_name: str,
    progress: float,
) -> None:
    inactive_roots = [
        root
        for root in await loaders.group_roots.load(group_name)
        if root.state.status == RootStatus.INACTIVE
    ]
    if not inactive_roots:
        return

    results = chain.from_iterable(
        await collect(
            [process_root(loaders=loaders, root=root) for root in inactive_roots],
            workers=8,
        ),
    )
    with open("0428.csv", "a+", encoding="utf-8") as handler:
        writer = csv.writer(handler)
        writer.writerows(results)

    LOGGER_CONSOLE.info(
        "Group processed %s %s %s",
        group_name,
        f"{round(progress, 2)!s}",
        f"{len(inactive_roots)=}",
    )


async def main() -> None:
    loaders = get_new_context()
    group_names = sorted(await orgs_domain.get_all_active_group_names(loaders))
    LOGGER_CONSOLE.info("%s", f"{group_names=}")
    LOGGER_CONSOLE.info("%s", f"{len(group_names)=}")
    for count, group_name in enumerate(group_names):
        await process_group(
            loaders=loaders,
            group_name=group_name,
            progress=count / len(group_names),
        )


if __name__ == "__main__":
    execution_time = time.strftime("Execution Time:    %Y-%m-%d at %H:%M:%S UTC")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S UTC")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
