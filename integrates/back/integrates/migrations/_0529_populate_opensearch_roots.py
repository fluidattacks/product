"""Populates OpenSearch with all the roots from active groups"""

import asyncio
import logging
import logging.config
import time
from contextlib import (
    suppress,
)

from aioextensions import (
    collect,
    run,
)
from aiohttp.client_exceptions import (
    ClientConnectorError,
    ClientPayloadError,
    ServerDisconnectedError,
    ServerTimeoutError,
)
from boto3.dynamodb.conditions import (
    Key,
)
from botocore.exceptions import (
    ClientError,
    ConnectTimeoutError,
    HTTPClientError,
    ReadTimeoutError,
)
from more_itertools import (
    chunked,
)
from opensearchpy.exceptions import (
    NotFoundError,
)
from opensearchpy.helpers import (
    BulkIndexError,
    async_bulk,
)

from integrates.class_types.types import (
    Item,
)
from integrates.dataloaders import (
    get_new_context,
)
from integrates.db_model import (
    TABLE,
)
from integrates.decorators import (
    retry_on_exceptions,
)
from integrates.dynamodb import (
    keys,
    operations,
)
from integrates.organizations.domain import (
    get_all_active_group_names,
)
from integrates.search.client import (
    get_client,
    search_shutdown,
    search_startup,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)
LOGGER_CONSOLE = logging.getLogger("console")
NETWORK_ERRORS = (
    asyncio.TimeoutError,
    ClientConnectorError,
    ClientError,
    ClientPayloadError,
    ConnectionError,
    ConnectionResetError,
    ConnectTimeoutError,
    HTTPClientError,
    ReadTimeoutError,
    ServerDisconnectedError,
    ServerTimeoutError,
)


@retry_on_exceptions(
    exceptions=NETWORK_ERRORS,
    sleep_seconds=10,
)
async def _get_roots_by_group(
    group_name: str,
) -> tuple[Item, ...]:
    primary_key = keys.build_key(
        facet=TABLE.facets["git_root_metadata"],
        values={"name": group_name},
    )

    index = TABLE.indexes["inverted_index"]
    key_structure = index.primary_key
    response = await operations.query(
        condition_expression=(
            Key(key_structure.partition_key).eq(primary_key.sort_key)
            & Key(key_structure.sort_key).begins_with(primary_key.partition_key)
        ),
        facets=(
            TABLE.facets["git_root_metadata"],
            TABLE.facets["ip_root_metadata"],
            TABLE.facets["url_root_metadata"],
        ),
        index=index,
        table=TABLE,
    )

    return response.items


async def process_roots(group_name: str, roots: tuple[Item, ...]) -> None:
    actions = [
        {
            "_id": "#".join([root["pk"], root["sk"]]),
            "_index": "roots_index",
            "_op_type": "index",
            "_source": root,
        }
        for root in roots
    ]

    client = await get_client()
    try:
        await async_bulk(client=client, actions=actions)
    except BulkIndexError as ex:
        for error in ex.errors:
            LOGGER_CONSOLE.info("%s %s", group_name, error["index"]["error"]["reason"])


@retry_on_exceptions(exceptions=NETWORK_ERRORS, max_attempts=3, sleep_seconds=3.0)
async def process_group(group_name: str, count: int) -> None:
    roots_by_group = await _get_roots_by_group(group_name=group_name)
    await collect(
        tuple(
            process_roots(group_name, tuple(chunked_roots))
            for chunked_roots in chunked(roots_by_group, 100)
        ),
    )
    LOGGER_CONSOLE.info(
        "Group",
        extra={
            "extra": {
                "group_name": group_name,
                "roots": len(roots_by_group),
                "count": count,
            },
        },
    )


async def main() -> None:
    loaders = get_new_context()
    active_group_names = sorted(await get_all_active_group_names(loaders))
    await search_startup()
    client = await get_client()
    with suppress(NotFoundError):
        await client.indices.delete(index="roots_index")
    await client.indices.create(index="roots_index")
    LOGGER_CONSOLE.info(
        "All group names",
        extra={
            "extra": {
                "active_group_names": len(active_group_names),
            },
        },
    )
    for count, group_name in enumerate(active_group_names, start=1):
        await process_group(group_name, count)
    await search_shutdown()


if __name__ == "__main__":
    execution_time = time.strftime("Execution Time:    %Y-%m-%d at %H:%M:%S UTC")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S UTC")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
