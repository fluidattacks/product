"""
Populates the stakeholder enrolled attribute with data previosly stored
in the enrollment facet

Execution Time:    2023-09-28 at 20:53:52 UTC
Finalization Time: 2023-09-28 at 20:53:52 UTC
"""

import logging
import logging.config
import time
from datetime import (
    datetime,
)

from aioextensions import (
    run,
)

from integrates.custom_exceptions import (
    InvalidExpirationTime,
)
from integrates.custom_utils import (
    datetime as datetime_utils,
)
from integrates.sessions import (
    domain as sessions_domain,
)
from integrates.sessions import (
    utils as sessions_utils,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)

LOGGER = logging.getLogger(__name__)
LOGGER_CONSOLE = logging.getLogger("console")


async def main() -> None:
    token = "Add your token"  # noqa: S105
    token_claims = sessions_domain.decode_token(token)
    LOGGER_CONSOLE.info(
        "Token claims",
        extra={"extra": {"Claims": token_claims}},
    )

    # Generate new token
    email = token_claims["user_email"]
    expiration_time = int(datetime_utils.get_now_plus_delta(days=180).timestamp())

    token_data = sessions_utils.calculate_hash_token()

    if sessions_utils.is_valid_expiration_time(expiration_time):
        issued_at = int(datetime.utcnow().timestamp())
        session_jwt = sessions_domain.encode_token(
            expiration_time=expiration_time,
            payload={
                "user_email": email,
                "jti": token_data["jti"],
                "issued_at": issued_at,
            },
            subject="api_token",
            api=True,
        )
        access_token = {
            "issued_at": issued_at,
            "jti_hashed": token_data["jti_hashed"],
            "salt": token_data["salt"],
        }
        LOGGER_CONSOLE.info("\n Token= %s \n Add to local data %s", session_jwt, access_token)
    else:
        raise InvalidExpirationTime()


if __name__ == "__main__":
    execution_time = time.strftime("Execution Time:    %Y-%m-%d at %H:%M:%S UTC")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S UTC")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
