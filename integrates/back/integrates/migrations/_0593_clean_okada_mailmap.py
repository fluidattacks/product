"""
Delete remaining authors from okada mailmap using a commits report

This migration downloads a commits report from S3, which indicates the
organizations where a given author has made commits.

Using this data, the migration iterates over the authors in the Okada mailmap
and checks whether none of their emails are present in the commits report. If
none of their emails are found, the author is removed from the database.

Start Time:        2024-09-10 at 18:33:31 UTC
Finalization Time: 2024-09-10 at 18:34:16 UTC

"""

import csv
import logging
import logging.config
import os
import time

from aioextensions import (
    run,
)

from integrates.custom_exceptions import (
    ErrorDownloadingFile,
    MailmapEntryAlreadyExists,
    MailmapEntryNotFound,
    MailmapOrganizationNotFound,
    MailmapSubentryAlreadyExists,
    MailmapSubentryAlreadyExistsInOrganization,
    MailmapSubentryNotFound,
)
from integrates.mailmap.delete import (
    delete_mailmap_entry_with_subentries,
)
from integrates.mailmap.get import (
    get_mailmap_entries_with_subentries,
)
from integrates.s3.operations import (
    download_file,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)
LOGGER_CONSOLE = logging.getLogger("console")


ORGANIZATION_ID = "okada"
COMMITS_REPORT_FILE_PATH = "remaining_okada_mailmap_authors.csv"


async def download_commits_report_file(file_path: str) -> None:
    try:
        await download_file(
            file_name=f"mailmap-resources/{file_path}",
            file_path=file_path,
            bucket="integrates.dev",
        )
    except ErrorDownloadingFile as ex:
        LOGGER_CONSOLE.error("%s\n\n", ex)


def remove_commits_report_file(file_path: str) -> None:
    if os.path.exists(file_path):
        os.remove(file_path)
        LOGGER_CONSOLE.info("Commits report file removed successfully.\n\n")
    else:
        LOGGER_CONSOLE.error("Commits report file doesn't exist.\n\n")


def get_emails_from_csv(file_path: str) -> list[str]:
    emails = []

    with open(file_path, newline="", encoding="utf-8") as csv_file:
        reader = csv.DictReader(csv_file)

        for row in reader:
            # Extract email from both 'author_entry' and 'commit_author'
            author_email = row["author_entry"].split(" - ")[0]
            alias_email = row["commit_author"].split(" - ")[0]

            # Add both emails to the list if not already present
            emails.append(author_email)
            emails.append(alias_email)

    return sorted(list(set(emails)))


async def main() -> None:
    await download_commits_report_file(COMMITS_REPORT_FILE_PATH)
    report_emails = get_emails_from_csv(COMMITS_REPORT_FILE_PATH)
    remove_commits_report_file(COMMITS_REPORT_FILE_PATH)

    entries_with_subentries = await get_mailmap_entries_with_subentries(
        organization_id=ORGANIZATION_ID,
    )

    removed_entries = 0
    for entry_with_subentries in entries_with_subentries:
        entry = entry_with_subentries["entry"]
        subentries = entry_with_subentries["subentries"]

        entry_email = entry["mailmap_entry_email"]
        subentries_emails = [subentry["mailmap_subentry_email"] for subentry in subentries]

        emails = [entry_email] + subentries_emails

        if all(email not in report_emails for email in emails):
            try:
                LOGGER_CONSOLE.info(
                    "Author %s: %s: %s: can be removed\n\n",
                    entry_email,
                    sorted(subentries_emails),
                    report_emails,
                )
                await delete_mailmap_entry_with_subentries(
                    entry_email=entry_email,
                    organization_id=ORGANIZATION_ID,
                )
                LOGGER_CONSOLE.info(
                    "Author %s removed successfully\n\n",
                    entry_email,
                )
                removed_entries += 1
            except (
                MailmapOrganizationNotFound,
                MailmapEntryAlreadyExists,
                MailmapEntryNotFound,
                MailmapSubentryAlreadyExists,
                MailmapSubentryAlreadyExistsInOrganization,
                MailmapSubentryNotFound,
            ) as ex:
                LOGGER_CONSOLE.error("Cannot remove author: %s\n\n", ex)

    LOGGER_CONSOLE.info(
        "Deleted entries: %s out of %s\n\n",
        removed_entries,
        len(entries_with_subentries),
    )


if __name__ == "__main__":
    execution_time = time.strftime("Start Time:        %Y-%m-%d at %H:%M:%S UTC")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S UTC")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
