"""
Update group from trial to managed

Execution Time:    2024-11-21 at 15:33:01 UTC
Finalization Time: 2024-11-21 at 15:33:02 UTC
"""

import logging
import logging.config
import time

from aioextensions import (
    run,
)

from integrates.custom_utils import (
    datetime as datetime_utils,
)
from integrates.dataloaders import (
    get_new_context,
)
from integrates.db_model.groups.enums import (
    GroupManaged,
    GroupStateStatus,
)
from integrates.db_model.groups.types import (
    Group,
    GroupState,
)
from integrates.groups.domain import (
    update_state,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)

LOGGER = logging.getLogger(__name__)
LOGGER_CONSOLE = logging.getLogger("console")


async def _update_group_to_managed(group: Group) -> None:
    await update_state(
        group_name=group.name,
        organization_id=group.organization_id,
        state=GroupState(
            comments=group.state.comments,
            modified_date=datetime_utils.get_utc_now(),
            has_essential=group.state.has_essential,
            has_advanced=group.state.has_advanced,
            managed=GroupManaged.MANAGED,
            payment_id=group.state.payment_id,
            justification=group.state.justification,
            modified_by="integrates@fluidattacks.com",
            service=group.state.service,
            status=GroupStateStatus.ACTIVE,
            tags=group.state.tags,
            tier=group.state.tier,
            type=group.state.type,
        ),
    )


async def main() -> None:
    group_name = ""
    loaders = get_new_context()

    group = await loaders.group.load(group_name)
    if not group:
        LOGGER.info("Group %s not found", group_name)
        return

    await _update_group_to_managed(group)
    LOGGER.info("Group Updated")


if __name__ == "__main__":
    execution_time = time.strftime("Execution Time:    %Y-%m-%d at %H:%M:%S UTC")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S UTC")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
