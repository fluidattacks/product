"""
Restore finding
Execution Time:    2024-11-13 at 17:43:27 UTC
Finalization Time: 2024-11-13 at 17:43:40 UTC
Execution Time:    2024-11-13 at 17:50:18 UTC
Finalization Time: 2024-11-13 at 17:50:31 UTC
Execution Time:    2024-11-14 at 14:32:47 UTC
Finalization Time: 2024-11-14 at 14:32:49 UTC
Execution Time:    2024-11-14 at 15:01:48 UTC
Finalization Time: 2024-11-14 at 15:01:49 UTC
Execution Time:    2024-12-04 at 18:50:53 UTC
Finalization Time: 2024-12-04 at 18:50:55 UTC
"""

import logging
import logging.config
import time
from contextlib import (
    suppress,
)
from datetime import (
    datetime,
)
from typing import (
    cast,
)

from aioextensions import (
    collect,
    run,
)
from boto3.dynamodb.conditions import (
    Key,
)

from integrates.custom_exceptions import (
    VulnAlreadyCreated,
)
from integrates.dataloaders import (
    get_new_context,
)
from integrates.db_model import (
    TABLE,
)
from integrates.db_model import (
    finding_comments as finding_comments_model,
)
from integrates.db_model import (
    findings as findings_model,
)
from integrates.db_model import (
    vulnerabilities as vulns_model,
)
from integrates.db_model.finding_comments.types import (
    FindingComment,
)
from integrates.db_model.finding_comments.utils import (
    format_finding_comments,
)
from integrates.db_model.findings.types import (
    Finding,
)
from integrates.db_model.findings.utils import (
    format_finding,
)
from integrates.db_model.items import (
    FindingCommentItem,
    FindingItem,
    VulnerabilityItem,
)
from integrates.db_model.vulnerabilities.types import (
    Vulnerability,
    VulnerabilityRequest,
)
from integrates.db_model.vulnerabilities.utils import (
    format_vulnerability,
)
from integrates.dynamodb import (
    keys,
    operations,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)

LOGGER = logging.getLogger(__name__)
LOGGER_CONSOLE = logging.getLogger("console")
BACKUP_TABLE = TABLE._replace(name="")


async def get_finding_by_id(finding_id: str) -> Finding | None:
    primary_key = keys.build_key(
        facet=BACKUP_TABLE.facets["finding_metadata"],
        values={"id": finding_id},
    )

    key_structure = BACKUP_TABLE.primary_key
    response = await operations.query(
        condition_expression=(
            Key(key_structure.partition_key).eq(primary_key.partition_key)
            & Key(key_structure.sort_key).begins_with(primary_key.sort_key)
        ),
        facets=(BACKUP_TABLE.facets["finding_metadata"],),
        limit=1,
        table=BACKUP_TABLE,
    )

    if not response.items:
        return None

    return format_finding(cast(FindingItem, response.items[0]))


async def get_finding_vulnerabilities(*, finding_id: str) -> list[Vulnerability]:
    primary_key = keys.build_key(
        facet=BACKUP_TABLE.facets["vulnerability_metadata"],
        values={"finding_id": finding_id},
    )

    index = BACKUP_TABLE.indexes["inverted_index"]
    key_structure = index.primary_key
    response = await operations.DynamoClient[VulnerabilityItem].query(
        condition_expression=(
            Key(key_structure.partition_key).eq(primary_key.sort_key)
            & Key(key_structure.sort_key).begins_with(primary_key.partition_key)
        ),
        facets=(BACKUP_TABLE.facets["vulnerability_metadata"],),
        table=BACKUP_TABLE,
        index=index,
    )

    return [format_vulnerability(item) for item in response.items]


async def get_vulnerability(*, vulnerability_id: str) -> Vulnerability | None:
    primary_key = keys.build_key(
        facet=BACKUP_TABLE.facets["vulnerability_metadata"],
        values={"id": vulnerability_id},
    )

    key_structure = BACKUP_TABLE.primary_key
    response = await operations.DynamoClient[VulnerabilityItem].query(
        condition_expression=(
            Key(key_structure.partition_key).eq(primary_key.partition_key)
            & Key(key_structure.sort_key).begins_with(primary_key.sort_key)
        ),
        facets=(BACKUP_TABLE.facets["vulnerability_metadata"],),
        limit=1,
        table=BACKUP_TABLE,
    )

    if not response.items:
        return None

    return format_vulnerability(response.items[0])


async def get_comments(*, finding_id: str) -> list[FindingComment]:
    primary_key = keys.build_key(
        facet=BACKUP_TABLE.facets["finding_comment"],
        values={"finding_id": finding_id},
    )
    key_structure = BACKUP_TABLE.primary_key
    response = await operations.query(
        condition_expression=(
            Key(key_structure.sort_key).eq(primary_key.sort_key)
            & Key(key_structure.partition_key).begins_with(primary_key.partition_key)
        ),
        facets=(BACKUP_TABLE.facets["finding_comment"],),
        index=BACKUP_TABLE.indexes["inverted_index"],
        table=BACKUP_TABLE,
    )

    return [format_finding_comments(cast(FindingCommentItem, item)) for item in response.items]


async def update_vuln(vulnerability: Vulnerability) -> None:
    loaders = get_new_context()
    if vulnerability.state.modified_date > datetime.fromisoformat(""):
        historic_state = await loaders.vulnerability_historic_state.load(
            VulnerabilityRequest(
                vulnerability_id=vulnerability.id,
                finding_id=vulnerability.finding_id,
            ),
        )
        await vulns_model.update_new_historic(
            current_value=vulnerability,
            historic=tuple(historic_state[:-1]) or (vulnerability.state,),
        )


async def add_vuln(vulnerability: Vulnerability) -> None:
    with suppress(VulnAlreadyCreated):
        await vulns_model.add(vulnerability=vulnerability)


async def restore_all_vulnerabilities(finding_id: str) -> None:
    vulnerabilities = await get_finding_vulnerabilities(finding_id=finding_id)

    if not vulnerabilities:
        return

    await collect(
        tuple(add_vuln(vulnerability=vulnerability) for vulnerability in vulnerabilities),
        workers=10,
    )

    await collect(
        tuple(update_vuln(vulnerability=vulnerability) for vulnerability in vulnerabilities),
        workers=10,
    )

    LOGGER.info(
        "Vulnerabilities processed",
        extra={
            "extra": {
                "finding_id": finding_id,
                "vulns": len(vulnerabilities),
            },
        },
    )


async def restore_all_comments(finding_id: str) -> None:
    all_comments = await get_comments(finding_id=finding_id)
    if not all_comments:
        return

    await collect(
        tuple(finding_comments_model.add(finding_comment=comment) for comment in all_comments),
    )

    LOGGER.info(
        "Comments processed",
        extra={
            "extra": {
                "finding_id": finding_id,
                "comments": len(all_comments),
            },
        },
    )


async def process_finding(finding_id: str) -> None:
    finding = await get_finding_by_id(finding_id)
    if not finding:
        LOGGER.error(
            "Finding not found",
            extra={
                "extra": {
                    "finding_id": finding_id,
                },
            },
        )
        return

    await findings_model.add(finding=finding)
    await restore_all_comments(finding.id)
    await restore_all_vulnerabilities(finding.id)
    LOGGER.info(
        "Finding processed",
        extra={
            "extra": {
                "finding_id": finding_id,
                "finding_title": finding.title,
            },
        },
    )


async def main() -> None:
    await process_finding("")


if __name__ == "__main__":
    execution_time = time.strftime("Execution Time:    %Y-%m-%d at %H:%M:%S UTC")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S UTC")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
