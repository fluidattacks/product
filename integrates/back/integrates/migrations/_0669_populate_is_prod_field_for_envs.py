"""
Populate the is_production field in environments.

Execution Time: 2025-02-26 at 18:57:08 UTC
Finalization Time: 2025-02-26 at 19:25:20 UTC

"""

import logging

from integrates.custom_utils import datetime as datetime_utils
from integrates.dataloaders import get_new_context
from integrates.db_model import roots as roots_model
from integrates.db_model.roots.enums import RootEnvironmentUrlType
from integrates.db_model.roots.types import GroupEnvironmentUrlsRequest
from integrates.migrations.utils import log_time
from integrates.organizations.domain import get_all_group_names

LOGGER = logging.getLogger("migrations")
MODIFIED_BY = "integrates@fluidattacks.com"


async def process_group(group_name: str, progress: float) -> None:
    LOGGER.info("Processing group %s", group_name)
    loaders = get_new_context()
    group_roots = await loaders.group_roots.load(group_name)
    if not group_roots:
        return

    group_env_urls = await loaders.group_environment_urls.load(
        GroupEnvironmentUrlsRequest(group_name=group_name),
    )

    if not group_env_urls:
        LOGGER.warning("No environments found for group %s", group_name)
        return

    LOGGER.info("Processing %s envs for %s", len(group_env_urls), group_name)

    for env in group_env_urls:
        if env.state.is_production is not None:
            LOGGER.info("is_production already set for %s", env.url)
            continue

        if env.state.url_type in {RootEnvironmentUrlType.URL, RootEnvironmentUrlType.APK}:
            # It is assumed that environments created before the
            # addition of the new field are not production (is_production=False).
            await roots_model.update_root_environment_url_state(
                current_value=env.state,
                root_id=env.root_id,
                url_id=env.id,
                group_name=group_name,
                state=env.state._replace(
                    modified_date=datetime_utils.get_now(),
                    modified_by=MODIFIED_BY,
                    is_production=False,
                ),
            )
            LOGGER.info(
                "Updated is_production for %s (url_type=%s) to %s",
                env.url,
                env.state.url_type,
                False,
            )
        else:
            # For environments other than URL and APK, the field is not applicable,
            # so it is set to None.
            await roots_model.update_root_environment_url_state(
                current_value=env.state,
                root_id=env.root_id,
                url_id=env.id,
                group_name=group_name,
                state=env.state._replace(
                    modified_date=datetime_utils.get_now(),
                    modified_by=MODIFIED_BY,
                    is_production=None,
                ),
            )
            LOGGER.info(
                "Updated is_production for %s (url_type=%s) to %s",
                env.url,
                env.state.url_type,
                None,
            )

        LOGGER.info(
            "Updated is_production for %s (url_type=%s) to %s",
            env.url,
            env.state.url_type,
            False
            if env.state.url_type in {RootEnvironmentUrlType.URL, RootEnvironmentUrlType.APK}
            else None,
        )

    LOGGER.info(
        "Group updated",
        extra={
            "extra": {
                "group_name": group_name,
                "env_urls_to_update": len(group_env_urls),
                "progress": str(round(progress, 2)),
            },
        },
    )


@log_time(LOGGER)
async def main() -> None:
    LOGGER.info("Started processing all groups")
    loaders = get_new_context()
    group_names = sorted(await get_all_group_names(loaders=loaders))
    for count, group in enumerate(group_names):
        await process_group(
            group_name=group,
            progress=count / len(group_names),
        )
    LOGGER.info("Finished processing all groups")


if __name__ == "__main__":
    main()  # type: ignore[unused-coroutine]
