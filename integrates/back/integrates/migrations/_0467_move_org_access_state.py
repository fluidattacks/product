"""
Move the metadata attributes to the state in the organization access.

Execution Time: 2023-12-28 at 18:45:44 UTC
Finalization Time: 2023-12-28 at 18:46:04 UTC
"""

import logging
import logging.config
import time

from aioextensions import (
    collect,
    run,
)
from boto3.dynamodb.conditions import (
    Attr,
    Key,
)
from botocore.exceptions import (
    ConnectTimeoutError,
    ReadTimeoutError,
)

from integrates.class_types.types import (
    Item,
)
from integrates.custom_utils.datetime import (
    get_iso_date,
)
from integrates.db_model import (
    TABLE,
)
from integrates.db_model.organizations.utils import (
    remove_org_id_prefix,
)
from integrates.decorators import (
    retry_on_exceptions,
)
from integrates.dynamodb import (
    keys,
    operations,
)
from integrates.dynamodb.types import (
    PrimaryKey,
)
from integrates.organizations import (
    domain as orgs_domain,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)

# Constants
LOGGER = logging.getLogger(__name__)
LOGGER_CONSOLE = logging.getLogger("console")
EMAIL_INTEGRATES = "integrates@fluidattacks.com"


ATTRS_TO_MIGRATE = [
    "has_access",
    "invitation",
    "role",
]


async def _get_organization_stakeholders_access(
    *,
    organization_id: str,
) -> tuple[Item, ...]:
    primary_key = keys.build_key(
        facet=TABLE.facets["organization_access"],
        values={
            "id": remove_org_id_prefix(organization_id),
        },
    )

    index = TABLE.indexes["inverted_index"]
    key_structure = index.primary_key
    response = await operations.query(
        condition_expression=(
            Key(key_structure.partition_key).eq(primary_key.sort_key)
            & Key(key_structure.sort_key).begins_with(primary_key.partition_key)
        ),
        facets=(TABLE.facets["organization_access"],),
        table=TABLE,
        index=index,
    )

    return response.items


@retry_on_exceptions(
    exceptions=(ReadTimeoutError, ConnectTimeoutError),
    sleep_seconds=3,
)
async def process_organization_access(item: Item) -> None:
    key_structure = TABLE.primary_key
    state = item.get("state", {})
    new_item = {
        f"state.{attr}": state[attr] if attr in state else item.get(attr)
        for attr in ATTRS_TO_MIGRATE
    }
    # cleanup
    new_item.update({attr: None for attr in ATTRS_TO_MIGRATE})
    if "state" not in item:
        modified_date = get_iso_date()
        await operations.update_item(
            condition_expression=Attr(key_structure.partition_key).exists(),
            item={
                "state": {
                    "modified_date": modified_date,
                    "modified_by": EMAIL_INTEGRATES,
                },
            },
            key=PrimaryKey(partition_key=item["pk"], sort_key=item["sk"]),
            table=TABLE,
        )
        new_item["state.modified_by"] = EMAIL_INTEGRATES
        new_item["state.modified_date"] = modified_date
    await operations.update_item(
        condition_expression=Attr(key_structure.partition_key).exists(),
        item=new_item,
        key=PrimaryKey(partition_key=item["pk"], sort_key=item["sk"]),
        table=TABLE,
    )


async def process_organization(organization_id: str) -> None:
    organization_stakeholders_access = await _get_organization_stakeholders_access(
        organization_id=organization_id,
    )
    await collect(
        tuple(
            process_organization_access(organization_access)
            for organization_access in organization_stakeholders_access
        ),
        workers=1000,
    )
    LOGGER_CONSOLE.info(
        "Organization processed",
        extra={
            "extra": {
                "organization_id": organization_id,
                "organization_stakeholders_access": len(organization_stakeholders_access),
            },
        },
    )


async def main() -> None:
    organization_ids = [
        organization.id async for organization in orgs_domain.iterate_organizations()
    ]
    for count, organization_id in enumerate(organization_ids, start=1):
        LOGGER_CONSOLE.info(
            "Organization to process",
            extra={
                "extra": {
                    "organization_id": organization_id,
                    "count": count,
                },
            },
        )
        await process_organization(organization_id)


if __name__ == "__main__":
    execution_time = time.strftime("Execution Time:    %Y-%m-%d at %H:%M:%S %Z")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S %Z")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
