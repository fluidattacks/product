"""
Identify which roots have been migrated from one group to another,
certainly through the mutation move_root, and add info the root
states regarding the roots involved

Execution Time:    2024-08-14 at 19:00:57 UTC
Finalization Time: 2024-08-14 at 19:19:38 UTC

Execution Time:    2024-08-15 at 03:58:32 UTC
Finalization Time: 2024-08-15 at 04:02:04 UTC
"""

import logging
import logging.config
import time
from collections.abc import Callable
from operator import (
    attrgetter,
)
from urllib.parse import (
    quote,
    unquote,
)

import simplejson as json
from aioextensions import (
    run,
)
from boto3.dynamodb.conditions import (
    Key,
)

from integrates.class_types.types import (
    Item,
)
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model import (
    HISTORIC_TABLE,
    TABLE,
)
from integrates.db_model.organizations import (
    get_all_organizations,
)
from integrates.db_model.roots.enums import (
    RootStateReason,
    RootStatus,
)
from integrates.db_model.roots.types import (
    GitRoot,
    GitRootState,
    IPRoot,
    IPRootState,
    Root,
    URLRootState,
)
from integrates.db_model.roots.update import (
    update_root_historic_state,
)
from integrates.db_model.roots.utils import (
    format_git_state,
    format_ip_state,
    format_url_state,
)
from integrates.db_model.utils import (
    get_as_utc_iso_format,
    serialize,
)
from integrates.dynamodb import (
    keys,
    operations,
)
from integrates.roots.update import (
    get_last_status_update_date,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)
LOGGER = logging.getLogger("console")


def get_state_other(group_name: str, root_id: str, nickname: str) -> str:
    return "#".join(
        [
            f"GROUP#{group_name}",
            f"ROOT#{root_id}",
            f"NICKNAME#{nickname}",
        ],
    )


def get_root_url_or_address(
    state: GitRootState | IPRootState | URLRootState,
) -> str:
    parsed_url = ""
    if isinstance(state, GitRootState):
        parsed_url = quote(unquote(state.url)).replace(".git", "")
    if isinstance(state, IPRootState):
        parsed_url = state.address
    if isinstance(state, URLRootState):
        parsed_url = f"{state.protocol}://{state.host}:{state.port}{state.path}{state.query}"

    return parsed_url


async def update_root_new_historic_state(
    *,
    group_name: str,
    root_id: str,
    historic_state: tuple[GitRootState | IPRootState | URLRootState, ...],
) -> None:
    if not historic_state:
        return

    key_structure = HISTORIC_TABLE.primary_key
    root_facets = {
        GitRootState: HISTORIC_TABLE.facets["git_root_state"],
        IPRootState: HISTORIC_TABLE.facets["ip_root_state"],
        URLRootState: HISTORIC_TABLE.facets["url_root_state"],
    }
    historic_facet = root_facets[type(historic_state[-1])]
    historic_key = keys.build_key(
        facet=historic_facet,
        values={
            "uuid": root_id,
            "group_name": group_name,
            "state": "state",
        },
    )
    response = await operations.query(
        condition_expression=(
            Key(key_structure.partition_key).eq(historic_key.partition_key)
            & Key(key_structure.sort_key).begins_with(historic_key.sort_key)
        ),
        facets=(historic_facet,),
        table=HISTORIC_TABLE,
    )
    current_keys = {
        keys.build_key(
            facet=historic_facet,
            values={
                "uuid": root_id,
                "group_name": group_name,
                "state": "state",
                "iso8601utc": item["modified_date"],
            },
        )
        for item in response.items
    }
    new_keys = tuple(
        keys.build_key(
            facet=historic_facet,
            values={
                "uuid": root_id,
                "group_name": group_name,
                "state": "state",
                "iso8601utc": get_as_utc_iso_format(entry.modified_date),
            },
        )
        for entry in historic_state
    )
    new_items = tuple(
        {
            key_structure.partition_key: key.partition_key,
            key_structure.sort_key: key.sort_key,
            "pk_2": f"ROOT#GROUP#{group_name}",
            "pk_3": f"GROUP#{group_name}",
            "sk_2": (f"STATE#state#DATE#{get_as_utc_iso_format(entry.modified_date)}"),
            "sk_3": (f"STATE#state#DATE#{get_as_utc_iso_format(entry.modified_date)}"),
            **json.loads(json.dumps(entry, default=serialize)),
        }
        for key, entry in zip(new_keys, historic_state, strict=False)
    )
    await operations.batch_put_item(items=new_items, table=HISTORIC_TABLE)
    await operations.batch_delete_item(
        keys=tuple(key for key in current_keys if key not in new_keys),
        table=HISTORIC_TABLE,
    )


async def get_historic_state(
    root: Root,
) -> list[GitRootState | IPRootState | URLRootState]:
    primary_key = keys.build_key(
        facet=TABLE.facets["git_root_historic_state"],
        values={"uuid": root.id},
    )
    key_structure = TABLE.primary_key
    response = await operations.query(
        condition_expression=(
            Key(key_structure.partition_key).eq(primary_key.partition_key)
            & Key(key_structure.sort_key).begins_with(primary_key.sort_key)
        ),
        facets=(
            TABLE.facets["git_root_historic_state"],
            TABLE.facets["ip_root_historic_state"],
            TABLE.facets["url_root_historic_state"],
        ),
        table=TABLE,
    )
    formatter: Callable[[Item], GitRootState | IPRootState | URLRootState] = format_url_state  # type: ignore [assignment]
    if isinstance(root, GitRoot):
        formatter = format_git_state  # type: ignore [assignment]
    if isinstance(root, IPRoot):
        formatter = format_ip_state  # type: ignore [assignment]

    return list(map(formatter, response.items))


async def process_source_root_historic_state(
    source_root: Root,
    source_historic_state: list[GitRootState | IPRootState | URLRootState],
    target_root: Root,
    target_historic_state: list[GitRootState | IPRootState | URLRootState],
) -> None:
    new_state_other = get_state_other(
        target_root.group_name,
        target_root.id,
        target_historic_state[0].nickname,
    )
    new_historic_state: list[GitRootState | IPRootState | URLRootState] = []
    for state in source_historic_state:
        if state.reason == RootStateReason.MOVED_TO_ANOTHER_GROUP:
            state = state._replace(other=new_state_other)
        new_historic_state.append(state)

    await update_root_new_historic_state(
        group_name=source_root.group_name,
        root_id=source_root.id,
        historic_state=tuple(new_historic_state),
    )
    await update_root_historic_state(
        current_state=source_root.state,
        group_name=source_root.group_name,
        root_id=source_root.id,
        historic_state=tuple(new_historic_state),
    )


async def process_target_root_historic_state(
    source_root: Root,
    target_root: Root,
    target_historic_state: list[GitRootState | IPRootState | URLRootState],
) -> None:
    new_state_other = get_state_other(
        source_root.group_name,
        source_root.id,
        source_root.state.nickname,
    )
    new_historic_state: list[GitRootState | IPRootState | URLRootState] = [
        target_historic_state[0]._replace(
            other=new_state_other,
            reason=RootStateReason.MOVED_FROM_ANOTHER_GROUP,
        ),
        *target_historic_state[1:],
    ]
    await update_root_new_historic_state(
        group_name=target_root.group_name,
        root_id=target_root.id,
        historic_state=tuple(new_historic_state),
    )
    await update_root_historic_state(
        current_state=target_root.state,
        group_name=target_root.group_name,
        root_id=target_root.id,
        historic_state=tuple(new_historic_state),
    )


async def select_target_root(source_root: Root, org_roots: list[Root]) -> Root | None:
    group_roots = [
        org_root
        for org_root in org_roots
        if org_root.group_name == source_root.state.other
        and org_root.type == source_root.type
        and org_root.id != source_root.id
    ]
    last_status_update = await get_last_status_update_date(
        get_new_context(),
        source_root.id,
        source_root.group_name,
    )
    matched_roots = [
        root
        for root in group_roots
        if last_status_update.replace(second=0, microsecond=0)
        == root.created_date.replace(second=0, microsecond=0)
    ]
    if len(matched_roots) == 1:
        target_root = matched_roots[0]
        target_historic_state = await get_historic_state(target_root)
        if target_historic_state and (
            get_root_url_or_address(target_historic_state[0])
            == get_root_url_or_address(source_root.state)
        ):
            return matched_roots[0]

    matched_roots = [
        root
        for root in group_roots
        if get_root_url_or_address(root.state) == get_root_url_or_address(source_root.state)
    ]
    if len(matched_roots) == 0:
        return None

    matched_roots = [
        candidate
        for candidate in matched_roots
        if last_status_update.replace(minute=0, second=0, microsecond=0)
        == candidate.created_date.replace(minute=0, second=0, microsecond=0)
    ]
    if len(matched_roots) > 1:
        LOGGER.error(
            "More than one match",
            extra={
                "extra": {
                    "group_name": source_root.group_name,
                    "id": source_root.id,
                    "nickname": source_root.state.nickname,
                    "type": source_root.type,
                    "state": source_root.state,
                    "len_match_roots": len(matched_roots),
                },
            },
        )

        return None

    return matched_roots[0]


async def process_root(source_root: Root, org_roots: list[Root]) -> None:
    target_root = await select_target_root(source_root, org_roots)
    if not target_root:
        LOGGER.error(
            "Target root NOT found",
            extra={
                "extra": {
                    "group_name": source_root.group_name,
                    "id": source_root.id,
                    "nickname": source_root.state.nickname,
                    "type": source_root.type,
                    "state": source_root.state,
                },
            },
        )
        return

    target_historic_state = await get_historic_state(target_root)
    root_historic_state = await get_historic_state(source_root)
    await process_target_root_historic_state(
        source_root=source_root,
        target_root=target_root,
        target_historic_state=target_historic_state,
    )
    await process_source_root_historic_state(
        source_root=source_root,
        source_historic_state=root_historic_state,
        target_root=target_root,
        target_historic_state=target_historic_state,
    )
    LOGGER.info(
        "Processed root",
        extra={
            "extra": {
                "group_name": source_root.group_name,
                "id": source_root.id,
                "nickname": source_root.state.nickname,
                "type": source_root.type,
                "target_group_name": target_root.group_name,
                "target_id": target_root.id,
                "target_nickname": target_root.state.nickname,
            },
        },
    )


async def process_organization(org_name: str) -> None:
    loaders: Dataloaders = get_new_context()
    org_roots = sorted(
        await loaders.organization_roots.load(org_name),
        key=attrgetter("group_name"),
    )
    if not org_roots:
        return

    org_migrated_roots = list(
        filter(
            lambda x: x.state.status == RootStatus.INACTIVE
            and x.state.reason == RootStateReason.MOVED_TO_ANOTHER_GROUP
            and x.state.other
            and "GROUP#" not in x.state.other,
            org_roots,
        ),
    )
    if not org_migrated_roots:
        return

    LOGGER.info(
        "Moved roots within the organization",
        extra={"extra": {"org_name": org_name, "len": len(org_migrated_roots)}},
    )
    for root in org_migrated_roots:
        await process_root(root, org_roots)


async def main() -> None:
    all_org_names = sorted([org.name for org in await get_all_organizations()])
    LOGGER.info(
        "Organizations to process",
        extra={"extra": {"len": len(all_org_names)}},
    )
    for org_name in all_org_names:
        await process_organization(org_name)


if __name__ == "__main__":
    execution_time = time.strftime("Execution Time:    %Y-%m-%d at %H:%M:%S UTC")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S UTC")
    LOGGER.info("\n%s\n%s", execution_time, finalization_time)
