"""Retrieve all users with "admin" role"""

import csv
import logging
import logging.config
import time

from aioextensions import (
    run,
)

from integrates.db_model.stakeholders import (
    get_all_stakeholders,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)

LOGGER = logging.getLogger(__name__)
LOGGER_CONSOLE = logging.getLogger("console")


async def main() -> None:
    users = await get_all_stakeholders()
    results = [user for user in users if user.role == "admin"]

    if not results:
        LOGGER.error("Users NOT found")

        return

    csv_columns = [
        "email",
        "role_user",
        "enrolled",
        "is_registered",
        "len_access_tokens",
    ]
    csv_file = "_0628_results.csv"
    try:
        with open(csv_file, "w", encoding="utf8") as file:
            writer = csv.DictWriter(file, fieldnames=csv_columns)
            writer.writeheader()
            for data in results:
                LOGGER.info("User: USER#%s", data.email)
                writer.writerow(
                    {
                        "email": data.email,
                        "role_user": data.role,
                        "enrolled": data.enrolled,
                        "is_registered": data.is_registered,
                        "len_access_tokens": len(data.access_tokens),
                    },
                )
    except OSError:
        LOGGER_CONSOLE.info("   === I/O error")


if __name__ == "__main__":
    execution_time = time.strftime("Execution Time:    %Y-%m-%d at %H:%M:%S UTC")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S UTC")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
