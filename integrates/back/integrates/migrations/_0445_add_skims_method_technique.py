# type: ignore
"""
Add the vulnerability technique information to
the vulnerabilities with de field in None

Start Time:	2023-11-09 at 23:20:54 UTC
End Time:	2023-11-09 at 23:55:25 UTC

"""

import asyncio
import logging
import logging.config
import time

from aioextensions import (
    run,
)

from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model.enums import (
    Source,
)
from integrates.db_model.vulnerabilities.types import (
    VulnerabilityMetadataToUpdate,
)
from integrates.db_model.vulnerabilities.update import (
    update_metadata,
)
from integrates.organizations import (
    domain as orgs_domain,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)
LOGGER_CONSOLE = logging.getLogger("console")


async def process_group(
    group: str,
    loaders: Dataloaders,
    sast_set: set,
    semaphore: asyncio.Semaphore,
    sca_set: set,
) -> None:
    async with semaphore:
        LOGGER_CONSOLE.info("Processing group %s...", group)
        findings = await loaders.group_findings.load(group)
        vulns = await loaders.finding_vulnerabilities.load_many_chained(
            [fin.id for fin in findings],
        )

        machine_vulns = [
            vuln for vuln in vulns if (vuln.state.source == Source.MACHINE) or vuln.skims_method
        ]

        for vuln in machine_vulns:
            if vuln.skims_method is not None and vuln.technique is None:
                skims_method_parts = vuln.skims_method.split(".")
                skims_method_first_part = skims_method_parts[0]
                if skims_method_first_part in sast_set:
                    LOGGER_CONSOLE.info(
                        "%s method will get SAST technique",
                        vuln.skims_method,
                    )
                    await update_metadata(
                        finding_id=vuln.finding_id,
                        metadata=VulnerabilityMetadataToUpdate(
                            technique="SAST",
                        ),
                        vulnerability_id=vuln.id,
                    )
                if skims_method_first_part in sca_set:
                    LOGGER_CONSOLE.info(
                        "%s method will get SCA technique",
                        vuln.skims_method,
                    )
                    await update_metadata(
                        finding_id=vuln.finding_id,
                        metadata=VulnerabilityMetadataToUpdate(
                            technique="SCA",
                        ),
                        vulnerability_id=vuln.id,
                    )


async def main() -> None:
    loaders: Dataloaders = get_new_context()
    groups = await orgs_domain.get_all_active_group_names(loaders=loaders)

    sast_set = {
        "java",
        "js",
        "typescript",
        "c_sharp",
        "conf_files",
        "kotlin",
        "swift",
        "python",
        "go",
        "dart",
        "docker_compose",
        "dotnetconfig",
        "android",
        "analyze_headers",
        "docker",
        "kubernetes",
        "bash",
        "generic",
        "html",
    }

    sca_set = {"maven"}
    semaphore = asyncio.Semaphore(10)
    await asyncio.gather(
        *[
            process_group(group, loaders, sast_set, semaphore, sca_set)
            for idx, group in enumerate(groups)
            if idx < 200
        ],
    )
    await asyncio.gather(
        *[
            process_group(group, loaders, sast_set, semaphore, sca_set)
            for idx, group in enumerate(groups)
            if 200 >= idx < 400
        ],
    )
    await asyncio.gather(
        *[
            process_group(group, loaders, sast_set, semaphore, sca_set)
            for idx, group in enumerate(groups)
            if idx >= 400
        ],
    )


if __name__ == "__main__":
    execution_time = time.strftime("Execution Time:    %Y-%m-%d at %H:%M:%S %Z")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S %Z")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
