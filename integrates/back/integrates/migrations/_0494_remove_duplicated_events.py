"""
Remove the events reported by schedule_clone_groups_roots@fluidattacks.com
on Friday, February 16, since they were generated on roots
that already had events reported.

Execution Time:     2024-02-19 at 22:55:55 UTC
Finalization Time:  2024-02-19 at 23:07:44 UTC
"""

import logging
import logging.config
import time
from datetime import (
    datetime,
)

from aioextensions import (
    run,
)

from integrates.dataloaders import (
    get_new_context,
)
from integrates.db_model.events.enums import (
    EventStateStatus,
)
from integrates.db_model.events.remove import (
    remove as remove_event,
)
from integrates.db_model.events.types import (
    GroupEventsRequest,
)
from integrates.organizations.domain import (
    get_all_groups,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)
LOGGER_CONSOLE = logging.getLogger("console")


async def main() -> None:
    init_date = datetime.fromisoformat("2024-02-16 03:30:00+00:00")
    end_date = datetime.fromisoformat("2024-02-17 03:30:00+00:00")
    loaders = get_new_context()
    groups = await get_all_groups(loaders=loaders)
    events = []
    for group in groups:
        group_events = await loaders.group_events.load(GroupEventsRequest(group_name=group.name))
        events += group_events
    events_to_remove = [
        event
        for event in events
        if event.state.status == EventStateStatus.CREATED
        and event.created_by == "schedule_clone_groups_roots@fluidattacks.com"
        and event.created_date > init_date
        and event.created_date < end_date
        and event.type == "CLONING_ISSUES"
    ]

    for event in events_to_remove:
        LOGGER_CONSOLE.info(
            "Removing event: %s",
            event.id,
            extra={
                "extra": {
                    "created_by": event.created_by,
                    "group_name": event.group_name,
                    "description": event.description,
                    "created_date": event.created_date,
                },
            },
        )
        await remove_event(event_id=event.id)
        LOGGER_CONSOLE.info("Removed event: %s", event.id)


if __name__ == "__main__":
    execution_time = time.strftime("Execution Time:     %Y-%m-%d at %H:%M:%S %Z")
    run(main())
    finalization_time = time.strftime("Finalization Time:  %Y-%m-%d at %H:%M:%S %Z")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
