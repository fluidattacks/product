"""
Populate the 'technique' field for all vulnerabilities.
This field indicates the technique
for each vulnerability based on specific rules.

The current type 'LINES' should populate `technique`
to one of the following types: 'SAST' | 'SCA' | 'CSPM'.

Execution Time: 2023-07-21 at 19:06:14 UTC
Finalization Time: 2023-07-21 at 19:20:48 UTC
"""

import logging
import logging.config
import time

from aioextensions import (
    collect,
    run,
)

from integrates.custom_utils.vulnerabilities import (
    is_machine_vuln,
)
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model import (
    vulnerabilities as vulns_model,
)
from integrates.db_model.vulnerabilities.enums import (
    VulnerabilityTechnique,
    VulnerabilityType,
)
from integrates.db_model.vulnerabilities.types import (
    Vulnerability,
    VulnerabilityMetadataToUpdate,
)
from integrates.organizations.domain import (
    get_all_group_names,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)
LOGGER_CONSOLE = logging.getLogger("console")


def get_technique(vuln: Vulnerability) -> VulnerabilityTechnique:
    if vuln.type == VulnerabilityType.LINES:
        convert_to = VulnerabilityTechnique.SAST
        if vuln.state.advisories or vuln.skims_method in {
            "python.pip_incomplete_dependencies_list",
        }:
            convert_to = VulnerabilityTechnique.SCA
    else:
        convert_to = VulnerabilityTechnique.DAST
        if vuln.skims_method and "aws" in vuln.skims_method:
            convert_to = VulnerabilityTechnique.CSPM
    return convert_to


async def process_group(
    loaders: Dataloaders,
    group_name: str,
) -> None:
    group_findings = await loaders.group_findings.load(group_name)

    vulns = await loaders.finding_vulnerabilities.load_many_chained(
        [fin.id for fin in group_findings],
    )

    machine_vulns = [vuln for vuln in vulns if is_machine_vuln(vuln) and not vuln.technique]

    if machine_vulns:
        await collect(
            vulns_model.update_metadata(
                finding_id=vuln.finding_id,
                vulnerability_id=vuln.id,
                metadata=VulnerabilityMetadataToUpdate(technique=get_technique(vuln)),
            )
            for vuln in machine_vulns
        )

    LOGGER_CONSOLE.info("Group processed %s", group_name)


async def main() -> None:
    loaders: Dataloaders = get_new_context()
    groups = sorted(await get_all_group_names(loaders))
    await collect([process_group(loaders, group) for group in groups], workers=15)


if __name__ == "__main__":
    execution_time = time.strftime("Execution Time:    %Y-%m-%d at %H:%M:%S UTC")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S UTC")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
