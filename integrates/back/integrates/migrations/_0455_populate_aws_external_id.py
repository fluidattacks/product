"""
Populate field aws_external_id for git repositories

Start Time:        2023-11-09 at 02:35:30 UTC
Finalization Time: 2023-11-09 at 02:53:15 UTC
"""

import logging
import logging.config
import time

from aioextensions import (
    collect,
    run,
)

from integrates.custom_utils.datetime import (
    get_utc_now,
)
from integrates.dataloaders import (
    get_new_context,
)
from integrates.db_model.roots.types import (
    GitRoot,
    GitRootState,
)
from integrates.db_model.roots.update import (
    update_root_state,
)
from integrates.organizations.domain import (
    get_all_group_names,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)
LOGGER_CONSOLE = logging.getLogger("console")


async def populate_external_id(root: GitRoot) -> None:
    LOGGER_CONSOLE.info("Modifying root %s", root.id)
    new_state = GitRootState(
        branch=root.state.branch,
        includes_health_check=root.state.includes_health_check,
        modified_by="acuberos@fluidattacks.com",
        modified_date=get_utc_now(),
        nickname=root.state.nickname,
        status=root.state.status,
        url=root.state.url,
        credential_id=root.state.credential_id,
        gitignore=root.state.gitignore,
        other=root.state.other,
        reason=root.state.reason,
        use_vpn=root.state.use_vpn,
        use_ztna=root.state.use_ztna,
    )
    await update_root_state(
        current_value=root.state,
        group_name=root.group_name,
        root_id=root.id,
        state=new_state,
    )


async def main() -> None:
    loaders = get_new_context()
    groups = await get_all_group_names(loaders)
    groups_roots = await loaders.group_roots.load_many_chained(groups)
    groups_git_roots = [root for root in groups_roots if isinstance(root, GitRoot)]
    LOGGER_CONSOLE.info("%s roots to modify", len(groups_git_roots))
    await collect(
        (populate_external_id(root) for root in groups_git_roots),
        workers=32,
    )


if __name__ == "__main__":
    execution_time = time.strftime("Start Time:    %Y-%m-%d at %H:%M:%S UTC")
    LOGGER_CONSOLE.info("%s", execution_time)
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S UTC")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
