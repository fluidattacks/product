"""Get from Redshift archive the data related to some groups."""

import logging
import logging.config
import sys
import time
from typing import (
    TypedDict,
)

from aioextensions import (
    run,
)
from psycopg2 import (  # type: ignore[import-untyped]
    sql,
)
from psycopg2.extensions import (  # type: ignore[import-untyped]
    cursor as cursor_cls,
)

from integrates.archive.utils import (
    mask_by_encoding,
)
from integrates.migrations._0591_update_severity_score_streams import (
    db_cursor,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)
LOGGER = logging.getLogger("console")
GROUP_NAMES: list[str] = []  # MASKED


class GroupSummary(TypedDict):
    source: str
    group_name: str
    total_findings: str
    total_vulns: str
    total_roots: str
    total_events: str
    total_toe_inputs: str
    total_toe_lines: str


def log_results(data: dict[str, GroupSummary]) -> None:
    for key, value in data.items():
        LOGGER.info(
            "%s,%s,%s,%s,%s,%s,%s,%s,%s",
            key,
            value["source"],
            value["group_name"],
            value["total_findings"],
            value["total_vulns"],
            value["total_roots"],
            value["total_events"],
            value["total_toe_inputs"],
            value["total_toe_lines"],
        )


def get_archive_findings_and_vulns(cursor: cursor_cls, groups: list[str]) -> list[tuple[str, ...]]:
    masked_groups = [sql.Literal(mask_by_encoding(group)) for group in groups]
    cursor.execute(
        sql.SQL(
            """
            SELECT
                g.name,
                COUNT(DISTINCT f.id),
                COUNT(DISTINCT v.id)
            FROM groups_metadata g
            LEFT JOIN findings_metadata f ON f.group_name = g.name
            LEFT JOIN vulnerabilities_metadata v ON v.finding_id = f.id
            WHERE g.name IN ({groups})
            GROUP BY g.name
            """,
        ).format(
            groups=sql.SQL(", ").join(masked_groups),
        ),
    )

    return list(cursor.fetchall())


def get_archive_roots_and_events(cursor: cursor_cls, groups: list[str]) -> list[tuple[str, ...]]:
    masked_groups = [sql.Literal(mask_by_encoding(group)) for group in groups]
    cursor.execute(
        sql.SQL(
            """
            SELECT
                g.name,
                COUNT(DISTINCT r.id),
                COUNT(DISTINCT e.id)
            FROM groups_metadata g
            LEFT JOIN roots_metadata r ON r.group_name = g.name
            LEFT JOIN events_metadata e ON e.group_name = g.name
            WHERE g.name IN ({groups})
            GROUP BY g.name
            """,
        ).format(
            groups=sql.SQL(", ").join(masked_groups),
        ),
    )

    return list(cursor.fetchall())


def get_archive_toes(cursor: cursor_cls, groups: list[str]) -> list[tuple[str, ...]]:
    masked_groups = [sql.Literal(mask_by_encoding(group)) for group in groups]
    cursor.execute(
        sql.SQL(
            """
            SELECT
                g.name,
                COUNT(DISTINCT ti.id),
                COUNT(DISTINCT tl.id)
            FROM groups_metadata g
            LEFT JOIN toe_inputs_metadata ti ON ti.group_name = g.name
            LEFT JOIN toe_lines_metadata tl ON tl.group_name = g.name
            WHERE g.name IN ({groups})
            GROUP BY g.name
            """,
        ).format(
            groups=sql.SQL(", ").join(masked_groups),
        ),
    )

    return list(cursor.fetchall())


def get_archive(cursor: cursor_cls, groups_to_process: list[str]) -> None:
    data: dict[str, GroupSummary] = {
        mask_by_encoding(group): {
            "source": "redshift",
            "group_name": group,
            "total_findings": "-",
            "total_vulns": "-",
            "total_roots": "-",
            "total_events": "-",
            "total_toe_inputs": "-",
            "total_toe_lines": "-",
        }
        for group in groups_to_process
    }

    archive_1 = get_archive_findings_and_vulns(cursor, groups_to_process)
    archive_2 = get_archive_roots_and_events(cursor, groups_to_process)
    archive_3 = get_archive_toes(cursor, groups_to_process)

    for finding, root, toe in zip(archive_1, archive_2, archive_3, strict=False):
        data[finding[0]]["total_findings"] = finding[1]
        data[finding[0]]["total_vulns"] = finding[2]
        data[root[0]]["total_roots"] = root[1]
        data[root[0]]["total_events"] = root[2]
        data[toe[0]]["total_toe_inputs"] = toe[1]
        data[toe[0]]["total_toe_lines"] = toe[2]

    log_results(data)


async def main() -> None:
    groups_to_process: list[str] = GROUP_NAMES

    with db_cursor() as cursor:
        get_archive(cursor, groups_to_process)


if __name__ == "__main__":
    migration_args = sys.argv[1:]
    LOGGER.info("Arguments passed down to the script: %s", str(migration_args))
    execution_time = time.strftime("Execution Time:    %Y-%m-%d at %H:%M:%S %Z")
    run(main(*migration_args))
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S %Z")
    LOGGER.info("\n%s\n%s", execution_time, finalization_time)
