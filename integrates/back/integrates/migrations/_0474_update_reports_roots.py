# type: ignore
"""
Update root from integrates.machine
reports that were impacted by wrong filtering of
duplicate reports

Execution Time:    2024-01-19 at 17:37:10 UTC
Finalization Time: 2024-01-19 at 19:24:53 UTC
"""

import csv
import time
from datetime import (
    UTC,
    datetime,
)

from aioextensions import (
    collect,
    run,
)

from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model.roots.enums import (
    RootType,
)
from integrates.db_model.roots.types import (
    Root,
)
from integrates.db_model.vulnerabilities.enums import (
    VulnerabilityType,
)
from integrates.db_model.vulnerabilities.types import (
    Vulnerability,
    VulnerabilityMetadataToUpdate,
)
from integrates.db_model.vulnerabilities.update import (
    update_metadata,
)
from integrates.organizations.domain import (
    get_all_active_groups,
)


async def get_group_roots(loaders: Dataloaders, group: str) -> dict[str, str]:
    group_roots: list[Root] = await loaders.group_roots.load(group)
    root_nicknames: dict[str, str] = {}
    for root in group_roots:
        if root.type != RootType.GIT:
            continue

        if root.state.nickname in root_nicknames:
            # Two roots with same nickname, not deterministic
            del root_nicknames[root.state.nickname]
            continue

        root_nicknames[root.state.nickname] = root.id

    return root_nicknames


async def update_vulnerabilities(
    loaders: Dataloaders,
    organization_id: str,
    group: str,
    root_nicknames: dict[str, str],
) -> None:
    findings = await loaders.group_findings.load(group)
    rows = []
    vulns = await loaders.finding_vulnerabilities.load_many_chained([fin.id for fin in findings])

    vulns = [
        vuln
        for vuln in vulns
        if vuln.type == VulnerabilityType.LINES
        and vuln.hacker_email == "machine@fluidattacks.com"
        and vuln.state.modified_date >= datetime(2024, 1, 5, tzinfo=UTC)
        and vuln.skims_description is not None
    ]

    vulns_to_update: list[tuple[Vulnerability, str]] = []

    for vuln in vulns:
        if not vuln.skims_description:
            continue
        root_nickname_from_description = vuln.skims_description.split()[-1].split("/")[0]

        if root_nickname_from_description not in root_nicknames:
            continue
        description_root_id = root_nicknames[root_nickname_from_description]
        if vuln.root_id != description_root_id:
            vulns_to_update.append((vuln, description_root_id))

    await collect(
        [
            update_metadata(
                finding_id=vuln.finding_id,
                metadata=VulnerabilityMetadataToUpdate(root_id=new_root_id),
                vulnerability_id=vuln.id,
            )
            for vuln, new_root_id in vulns_to_update
        ],
        workers=32,
    )

    rows.extend(
        [
            [
                organization_id,
                group,
                vuln.root_id,
                new_root_id,
                vuln.state.where,
                vuln.state.specific,
            ]
            for vuln, new_root_id in vulns_to_update
        ],
    )

    with open("vulns_with_problems.csv", "a+", encoding="utf-8") as handler:
        writer = csv.writer(handler)
        writer.writerows(rows)


async def main() -> None:
    loaders = get_new_context()
    groups = await get_all_active_groups(loaders)
    for group in groups:
        print(f"Processing {group.name}")
        root_nicknames = await get_group_roots(loaders, group.name)
        await update_vulnerabilities(loaders, group.organization_id, group.name, root_nicknames)


if __name__ == "__main__":
    execution_time = time.strftime("Execution Time:    %Y-%m-%d at %H:%M:%S UTC")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S UTC")
    print(f"{execution_time}\n{finalization_time}")
