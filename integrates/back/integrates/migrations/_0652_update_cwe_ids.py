# type: ignore
"""
Update machine vulnerability CWE_IDS

Execution Time:    2025-01-21 at 21:51:34 UTC
Finalization Time: 2025-01-21 at 23:19:29 UTC
"""

import csv
import logging
import logging.config
import time

from aioextensions import (
    collect,
    run,
)

from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model.vulnerabilities.types import (
    Vulnerability,
    VulnerabilityMetadataToUpdate,
)
from integrates.db_model.vulnerabilities.update import (
    update_metadata,
)
from integrates.organizations import (
    domain as orgs_domain,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)
LOGGER_CONSOLE = logging.getLogger("console")
CWE_IDS_MAP: dict[str, list[str]] = {
    "c_sharp.c_sharp_cve_2024_21907": ["CWE-121"],
    "javascript.js_cve_2019_10775": ["CWE-770"],
    "typescript.ts_cve_2019_10775": ["CWE-770"],
    "java.java_insecure_authentication": ["CWE-319"],
    "c_sharp.c_sharp_service_point_manager_disabled": ["CWE-327", "CWE-290"],
    "c_sharp.c_sharp_insecure_shared_access_protocol": ["CWE-319"],
    "c_sharp.c_sharp_httpclient_no_revocation_list": ["CWE-295"],
    "cloudformation.cfn_serves_content_over_insecure_protocols": ["CWE-327"],
    "cloudformation.cfn_elb_without_sslpolicy": ["CWE-319"],
    "kotlin.kt_default_http_client_deprecated": ["CWE-327"],
    "php.php_insecure_ssl_tls_stream": ["CWE-327"],
    "terraform.tfm_aws_serves_content_over_insecure_protocols": ["CWE-327"],
    "terraform.tfm_azure_serves_content_over_insecure_protocols": ["CWE-327"],
    "terraform.tfm_aws_elb_without_sslpolicy": ["CWE-319"],
    "java.java_null_cipher": ["CWE-312"],
    "cloudformation.cfn_ec2_has_unrestricted_ports": ["CWE-183"],
    "cloudformation.cfn_groups_without_egress": ["CWE-923"],
    "cloudformation.cfn_unrestricted_ip_protocols": ["CWE-923"],
    "cloudformation.cfn_ec2_has_open_all_ports_to_the_public": ["CWE-923"],
    "cloudformation.cfn_ec2_has_unrestricted_dns_access": ["CWE-862"],
    "cloudformation.cfn_ec2_has_unrestricted_ftp_access": ["CWE-862"],
    "terraform.tfm_allows_anyone_to_admin_ports": ["CWE-862"],
    "terraform.tfm_ec2_has_unrestricted_dns_access": ["CWE-862"],
    "terraform.tfm_ec2_has_unrestricted_ftp_access": ["CWE-862"],
    "terraform.tfm_ec2_has_open_all_ports_to_the_public": ["CWE-923"],
    "terraform.tfm_ec2_instances_without_profile": ["CWE-272"],
    "terraform.tfm_aws_ec2_cfn_unrestricted_ip_protocols": ["CWE-923"],
    "terraform.tfm_ec2_has_unrestricted_ports": ["CWE-183"],
    "javascript.js_file_size_limit_missing": ["CWE-770"],
    "typescript.ts_file_size_limit_missing": ["CWE-770"],
    "kotlin.kotlin_weak_random": ["CWE-338"],
    "javascript.javascript_weak_random": ["CWE-338"],
    "php.php_weak_random": ["CWE-338"],
    "typescript.typescript_weak_random": ["CWE-338"],
    "dotnetconfig.dotnetconfig_not_suppress_vuln_header": ["CWE-497"],
    "javascript.js_cve_2021_23566": ["CWE-704"],
    "typescript.ts_cve_2021_23566": ["CWE-704"],
    "java.java_properties_missing_ssl": ["CWE-319"],
    "java.java_properties_weak_cipher_suite": ["CWE-327"],
    "c_sharp.c_sharp_insecure_hash": ["CWE-328"],
    "c_sharp.c_sharp_insecure_cipher": ["CWE-327"],
    "c_sharp.c_sharp_managed_secure_mode": ["CWE-327"],
    "c_sharp.c_sharp_rsa_secure_mode": ["CWE-780"],
    "c_sharp.c_sharp_insecure_keys": ["CWE-326"],
    "c_sharp.c_sharp_disabled_strong_crypto": ["CWE-327"],
    "c_sharp.c_sharp_obsolete_key_derivation": ["CWE-327"],
    "c_sharp.c_sharp_weak_rsa_encrypt_padding": ["CWE-780"],
    "swift.swift_insecure_cipher": ["CWE-326"],
    "swift.swift_insecure_cryptor": ["CWE-327"],
    "swift.swift_hc_secret_jwt": ["CWE-321"],
    "go.go_hardcoded_symmetric_key": ["CWE-321"],
    "go.go_insecure_hash": ["CWE-328"],
    "go.go_insecure_cipher": ["CWE-327"],
    "java.java_insecure_cipher_jmqi": ["CWE-327"],
    "java.java_insecure_cipher_ssl": ["CWE-327"],
    "java.java_insecure_engine_cipher_ssl": ["CWE-327"],
    "java.java_insecure_connection": ["CWE-327"],
    "java.java_insecure_key_ec": ["CWE-327"],
    "java.java_insecure_key_rsa": ["CWE-327"],
    "java.java_insecure_key_secret": ["CWE-327"],
    "java.java_jwt_without_proper_sign": ["CWE-345"],
    "java.java_jwt_unsafe_decode": ["CWE-345"],
    "java.java_insecure_pass": ["CWE-327"],
    "java.java_insec_sign_algorithm": ["CWE-321"],
    "java.java_weak_rsa_key": ["CWE-326"],
    "javascript.javascript_insecure_encrypt": ["CWE-327"],
    "typescript.typescript_insecure_encrypt": ["CWE-327"],
    "javascript.javascript_insecure_create_cipher": ["CWE-327"],
    "javascript.javascript_insecure_ecdh_key": ["CWE-327"],
    "typescript.ts_insecure_ecdh_key": ["CWE-327"],
    "javascript.javascript_insecure_ec_keypair": ["CWE-327"],
    "typescript.typescript_insecure_ec_keypair": ["CWE-327"],
    "javascript.javascript_insecure_rsa_keypair": ["CWE-326"],
    "typescript.ts_insecure_rsa_keypair": ["CWE-326"],
    "javascript.javascript_insecure_hash": ["CWE-327"],
    "typescript.typescript_insecure_hash": ["CWE-327"],
    "typescript.ts_insecure_create_cipher": ["CWE-327"],
    "kotlin.kotlin_hc_secret_alg_instance": ["CWE-321"],
    "kotlin.kotlin_insecure_cipher": ["CWE-327"],
    "kotlin.kotlin_insecure_cipher_mode": ["CWE-327"],
    "kotlin.kotlin_insecure_cipher_http": ["CWE-327"],
    "kotlin.kotlin_insecure_cipher_ssl": ["CWE-327"],
    "kotlin.kotlin_insecure_hash": ["CWE-328"],
    "kotlin.kotlin_insecure_key": ["CWE-327"],
    "kotlin.kt_insecure_init_vector": ["CWE-321"],
    "kotlin.kt_insecure_host_verification": ["CWE-295"],
    "kotlin.kt_insecure_certificate_validation": ["CWE-295"],
    "kotlin.kt_insecure_key_gen": ["CWE-326"],
    "kotlin.kt_insecure_key_pair_gen": ["CWE-326"],
    "kotlin.kt_insecure_parameter_spec": ["CWE-329"],
    "kotlin.kt_insecure_encryption_key": ["CWE-321"],
    "kotlin.kotlin_insecure_key_ec": ["CWE-327"],
    "python.python_insec_hash_library": ["CWE-328"],
    "python.python_insecure_jwt_key": ["CWE-345"],
    "python.python_unsafe_cipher": ["CWE-327"],
    "php.php_insecure_mcrypt": ["CWE-327"],
    "php.php_insecure_openssl": ["CWE-327"],
    "python.python_insecure_cipher_mode": ["CWE-327"],
    "javascript.javascript_insecure_hash_library": ["CWE-328"],
    "conf_files.json_anon_connection_config": ["CWE-306"],
    "dotnetconfig.dotnetconfig_anon_auth_enabled": ["CWE-306"],
    "c_sharp.c_sharp_override_auth_modifier": ["CWE-306"],
    "android.keyboard_cache_exposure": ["CWE-524"],
    "html.html_has_autocomplete": ["CWE-525"],
    "c_sharp.c_sharp_cve_2021_43045": ["CWE-770"],
    "android.android_apk_exported_cp": ["CWE-926"],
    "cloudformation.cfn_bucket_policy_has_server_side_encryption_disabled": ["CWE-312"],
    "terraform.tfm_bucket_policy_has_server_side_encryption_disabled": ["CWE-312"],
    "typescript.ts_nosql_injection": ["CWE-943"],
    "javascript.js_nosql_injection": ["CWE-943"],
    "typescript.ts_nosql_injection_ternary": ["CWE-943"],
    "javascript.js_nosql_injection_ternary": ["CWE-943"],
    "python.python_django_insecure_cors": ["CWE-942"],
    "python.python_fastapi_insecure_cors": ["CWE-942"],
    "python.python_flask_insecure_cors": ["CWE-942"],
    "cloudformation.yml_serverless_cors": ["CWE-942"],
    "c_sharp.csharp_insecure_cors": ["CWE-942"],
    "c_sharp.csharp_insecure_cors_origin": ["CWE-942"],
    "php.php_insecure_cors": ["CWE-942"],
    "java.java_insecure_cors_origin": ["CWE-942"],
    "java.java_insecure_cors_web_view": ["CWE-942"],
    "javascript.javascript_insecure_cors_origin": ["CWE-942"],
    "typescript.typescript_insecure_cors_origin": ["CWE-942"],
    "java.java_cve_2023_26919": ["CWE-749"],
    "cloudformation.cfn_elasticache_is_transit_encryption_disabled": ["CWE-319"],
    "terraform.tfm_elasticache_is_transit_encryption_disabled": ["CWE-319"],
    "cloudformation.cfn_sns_is_server_side_encryption_disabled": ["CWE-312"],
    "terraform.tfm_sns_is_server_side_encryption_disabled": ["CWE-312"],
    "cloudformation.cfn_sqs_is_encryption_disabled": ["CWE-312"],
    "terraform.tfm_sqs_is_encryption_disabled": ["CWE-312"],
    "cloudformation.cfn_redshift_not_requires_ssl": ["CWE-319"],
    "terraform.tfm_redshift_not_requires_ssl": ["CWE-319"],
    "terraform.tfm_iam_permissions_policy_allow_not_resource": ["CWE-732"],
    "terraform.tfm_iam_permissions_policy_allow_not_action": ["CWE-732"],
    "terraform.tfm_iam_role_is_over_privileged": ["CWE-266"],
    "terraform.tfm_iam_trust_policy_not_action": ["CWE-732"],
    "terraform.tfm_iam_policy_apply_to_users": ["CWE-266"],
    "cloudformation.cfn_iam_permissions_policy_allow_not_resource": ["CWE-732"],
    "cloudformation.cfn_iam_permissions_policy_allow_not_action": ["CWE-732"],
    "cloudformation.cfn_iam_trust_policy_allow_not_principal": ["CWE-266"],
    "cloudformation.cfn_iam_trust_policy_not_action": ["CWE-732"],
    "cloudformation.cfn_iam_permissions_policy_aplly_users": ["CWE-266"],
    "cloudformation.cfn_s3_buckets_allow_unauthorized_public_access": ["CWE-732"],
    "terraform.tfm_public_buckets": ["CWE-732"],
    "terraform.tfm_s3_buckets_allow_unauthorized_public_access": ["CWE-732"],
    "javascript.js_cve_2019_1010266": ["CWE-770"],
    "typescript.ts_cve_2019_1010266": ["CWE-770"],
    "dotnetconfig.dotnetconfig_asp_version_enabled": ["CWE-497"],
    "php.php_discloses_server_version": ["CWE-497"],
    "conf_files.tsconfig_sourcemap_enabled": ["CWE-497"],
    "dotnetconfig.dotnetconfig_not_custom_errors": ["CWE-209"],
    "c_sharp.csharp_info_leak_errors": ["CWE-209"],
    "php.php_info_leak_errors": ["CWE-209"],
    "php.php_sql_leak_errors": ["CWE-209"],
    "php.php_server_leaks_errors": ["CWE-209"],
    "cloudformation.cfn_rds_has_unencrypted_storage": ["CWE-312"],
    "terraform.tfm_rds_has_unencrypted_storage": ["CWE-312"],
    "python.python_aws_hardcoded_credentials": ["CWE-798"],
    "terraform.tfm_rds_no_deletion_protection": ["CWE-732"],
    "terraform.tfm_rds_has_not_automated_backups": ["CWE-778"],
    "terraform.tfm_dynamo_has_not_deletion_protection": ["CWE-732"],
    "cloudformation.cfn_bucket_policy_has_secure_transport": ["CWE-319"],
    "terraform.tfm_bucket_policy_has_secure_transport": ["CWE-319"],
    "terraform.tfm_azure_as_client_certificates_enabled": ["CWE-306"],
    "terraform.tfm_azure_app_service_authentication_is_not_enabled": ["CWE-306"],
    "terraform.tfm_redis_cache_authnotrequired_enabled": ["CWE-306"],
    "terraform.tfm_azure_dev_portal_has_auth_methods_inactive": ["CWE-306"],
    "java.java_none_alg_auth0_jwt_sign_key": ["CWE-347"],
    "javascript.js_uses_insecure_jwt_token": ["CWE-347"],
    "javascript.js_cve_2022_23540": ["CWE-347"],
    "typescript.ts_cve_2022_23540": ["CWE-347"],
    "typescript.ts_insecure_jwt_token": ["CWE-347"],
    "cloudformation.cfn_ec2_associate_public_ip_address": ["CWE-497"],
    "terraform.tfm_ec2_associate_public_ip_address": ["CWE-497"],
    "java.java_hostname_verification_off": ["CWE-295"],
    "java.java_use_insecure_trust_manager": ["CWE-295"],
    "java.java_declare_insecure_trust_manager": ["CWE-295"],
    "c_sharp.c_sharp_token_validation_checks": ["CWE-347"],
    "javascript.js_decode_insecure_jwt_token": ["CWE-347"],
    "typescript.ts_decode_insecure_jwt_token": ["CWE-347"],
    "php.php_technical_info_leak": ["CWE-497"],
    "javascript.js_exposed_private_key": ["CWE-798"],
    "typescript.ts_exposed_private_key": ["CWE-798"],
    "javascript.js_hardcoded_key_hmac": ["CWE-798"],
    "typescript.ts_hardcoded_key_hmac": ["CWE-798"],
    "terraform.tfm_kms_key_is_key_rotation_absent_or_disabled": ["CWE-262"],
    "cloudformation.cfn_kms_key_is_key_rotation_absent_or_disabled": ["CWE-262"],
    "c_sharp.c_sharp_code_injection": ["CWE-94"],
    "cloudformation.cfn_aws_efs_unencrypted": ["CWE-312"],
    "terraform.tfm_aws_efs_unencrypted": ["CWE-312"],
    "cloudformation.cfn_aws_ebs_volumes_unencrypted": ["CWE-312"],
    "terraform.tfm_aws_ebs_volumes_unencrypted": ["CWE-312"],
    "docker.docker_using_add_command": ["CWE-829"],
    "docker.docker_weak_ssl_tls": ["CWE-326"],
    "docker.docker_insecure_builder_sandbox": ["CWE-250"],
    "docker.docker_insecure_cleartext_protocol": ["CWE-319"],
    "docker.docker_weak_hash_algorithm": ["CWE-328"],
    "docker.docker_insecure_network_host": ["CWE-923"],
    "docker.docker_insecure_context_directory": ["CWE-530"],
    "cloudformation.cfn_redshift_has_encryption_disabled": ["CWE-312"],
    "terraform.tfm_redshift_has_encryption_disabled": ["CWE-312"],
    "github_actions.github_actions_without_hash": ["CWE-829"],
    "typescript.typescript_cookie_service_sensitive_info": ["CWE-614"],
}


async def update_finding_vulns(loaders: Dataloaders, finding_id: str, group_name: str) -> None:
    vulns = await loaders.finding_vulnerabilities.load(finding_id)
    machine_vulns: list[Vulnerability] = [
        [group_name, vuln.id, vuln.finding_id, vuln.skims_method, CWE_IDS_MAP[vuln.skims_method]]
        for vuln in vulns
        if vuln.hacker_email == "machine@fluidattacks.com"
        and vuln.skims_method
        and vuln.skims_method in CWE_IDS_MAP
        and vuln.cwe_ids != CWE_IDS_MAP[vuln.skims_method]
    ]

    if not machine_vulns:
        return

    collect(
        (
            await update_metadata(
                finding_id=vuln[2],
                metadata=VulnerabilityMetadataToUpdate(cwe_ids=vuln[4]),
                vulnerability_id=vuln[1],
            )
            for vuln in machine_vulns
        ),
        workers=8,
    )

    with open("updated_vulns.csv", "a+", encoding="utf-8") as handler:
        writer = csv.writer(handler)
        writer.writerows(machine_vulns)


async def main() -> None:
    loaders: Dataloaders = get_new_context()
    groups = sorted(await orgs_domain.get_all_active_group_names(loaders=loaders))

    for group in groups:
        LOGGER_CONSOLE.info("Processing group %s", group)
        finding_ids = [
            fin.id
            for fin in await loaders.group_findings.load(group)
            if fin.creation and fin.creation.modified_by == "machine@fluidattacks.com"
        ]
        for finding_id in finding_ids:
            await update_finding_vulns(loaders, finding_id, group)


if __name__ == "__main__":
    execution_time = time.strftime("Execution Time:    %Y-%m-%d at %H:%M:%S %Z")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S %Z")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
