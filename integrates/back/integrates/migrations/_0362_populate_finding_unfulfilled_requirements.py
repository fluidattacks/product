# type: ignore
"""
Populate the finding unfulfilled requirements with the criteria files

Execution Time:    2023-02-17 at 20:49:19 UTC
Finalization Time: 2023-02-17 at 20:56:24 UTC
"""

import logging
import logging.config
import time

from aioextensions import (
    collect,
    run,
)

from integrates.custom_utils.criteria import (
    CRITERIA_VULNERABILITIES,
)
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model import (
    findings as findings_model,
)
from integrates.db_model.findings.types import (
    Finding,
    FindingMetadataToUpdate,
)
from integrates.organizations import (
    domain as orgs_domain,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)

# Constants
LOGGER = logging.getLogger(__name__)
LOGGER_CONSOLE = logging.getLogger("console")


async def process_finding(
    finding: Finding,
) -> None:
    criteria_vulnerability_id = finding.get_criteria_code()
    criteria_vulnerability = CRITERIA_VULNERABILITIES.get(criteria_vulnerability_id)
    requirements: list[str] = (
        criteria_vulnerability["requirements"] if criteria_vulnerability else []
    )
    await findings_model.update_metadata(
        group_name=finding.group_name,
        finding_id=finding.id,
        metadata=FindingMetadataToUpdate(unfulfilled_requirements=requirements),
    )


async def process_group(group_name: str) -> None:
    loaders: Dataloaders = get_new_context()
    findings = await loaders.group_drafts_and_findings.load(group_name)
    await collect(tuple(process_finding(finding) for finding in findings))
    LOGGER_CONSOLE.info(
        "Group processed",
        extra={
            "extra": {
                "group_name": group_name,
            },
        },
    )


async def main() -> None:
    loaders: Dataloaders = get_new_context()
    all_group_names = sorted(await orgs_domain.get_all_group_names(loaders=loaders))
    count = 0
    LOGGER_CONSOLE.info(
        "All group names",
        extra={
            "extra": {
                "total": len(all_group_names),
            },
        },
    )
    for group_name in all_group_names:
        count += 1
        LOGGER_CONSOLE.info(
            "Group",
            extra={
                "extra": {
                    "group_name": group_name,
                    "count": count,
                },
            },
        )
        await process_group(group_name)


if __name__ == "__main__":
    execution_time = time.strftime("Execution Time:    %Y-%m-%d at %H:%M:%S %Z")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S %Z")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
