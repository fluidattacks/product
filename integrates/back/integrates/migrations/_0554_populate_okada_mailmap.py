"""
Populate okada mailmap with services mailmap data

The purpose of this migration is to add the new mailmap entries that were
added to the services repository to make sure both the database and the repo
have the same data

This migration is executed locally.

To run it, download the services mailmap file (.groups-mailmap) to your
machine and set its path in the MAILMAP_PATH variable.

Start Time:        2024-06-13 at 15:56:59 UTC
Finalization Time: 2024-06-13 at 18:36:13 UTC

"""

import logging
import logging.config
import time

from aioextensions import (
    run,
)
from pandas import (
    DataFrame,
)

from integrates.custom_exceptions import (
    MailmapEntryAlreadyExists,
    MailmapOrganizationNotFound,
    MailmapSubentryAlreadyExists,
)
from integrates.db_model.mailmap.types import (
    MailmapEntry,
    MailmapEntryWithSubentries,
    MailmapSubentry,
)
from integrates.mailmap.create import (
    create_mailmap_entry_with_subentries,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)
LOGGER_CONSOLE = logging.getLogger("console")


# okada organization id in prod database
ORGANIZATION_ID = "ORG#a23457e2-f81f-44a2-867f-230082af676c"
MAILMAP_PATH = ""


def find_unformatted_lines_candidates(raw_mailmap_df: DataFrame) -> None:
    # find rows containing more than one '<' symbol
    unformatted_lines = raw_mailmap_df[raw_mailmap_df["raw_lines"].str.count("<") > 2]
    if not unformatted_lines.empty:
        LOGGER_CONSOLE.info("Unformatted Lines Candidates:")
        for idx, row in unformatted_lines.iterrows():
            raw_line = row["raw_lines"]
            LOGGER_CONSOLE.info("%d: %s", idx, raw_line)


def find_mapping_issues(mailmap_df: DataFrame) -> None:
    # find rows that associate more than one entry_name to one entry_email
    unique_entry_names = mailmap_df.groupby("entry_email")["entry_name"].nunique()
    entries_with_multiple_names = unique_entry_names[unique_entry_names > 1].index.tolist()
    result_df = mailmap_df[mailmap_df["entry_email"].isin(entries_with_multiple_names)]

    if not result_df.empty:
        # print problematic lines
        LOGGER_CONSOLE.info("Multiple Entry Name Mapping Issues:")
        prev_entry_email = ""
        for idx, row in result_df.iterrows():
            entry_email = row["entry_email"]
            new_line = "\n" if entry_email != prev_entry_email else ""
            entry_email = row["entry_email"]
            entry_name = row["entry_name"]
            LOGGER_CONSOLE.info("%s%d: %s - %s", new_line, idx, entry_email, entry_name)
            prev_entry_email = entry_email


def read_mailmap(path: str) -> DataFrame:
    # read mailmap file
    with open(path, encoding="utf-8") as file:
        raw_lines = file.readlines()

    raw_mailmap_df = DataFrame({"raw_lines": raw_lines})

    find_unformatted_lines_candidates(raw_mailmap_df)

    pattern1 = r"(?P<entry_name>.*?) <(?P<entry_email>.*?)>"
    pattern2 = r"(?P<subentry_name>.*?) <(?P<subentry_email>.*?)>"
    pattern = f"{pattern1} {pattern2}"
    extracted_df = raw_mailmap_df["raw_lines"].str.extract(pattern)

    # removes empty line at the end
    filtered_mailmap_df = extracted_df.dropna(axis=0, how="all")

    mailmap_df = filtered_mailmap_df.sort_values(
        by=["entry_email", "entry_name", "subentry_email", "subentry_name"],
    )

    find_mapping_issues(mailmap_df)

    return mailmap_df


def build_entries_with_subentries(
    mailmap_df: DataFrame,
) -> list[MailmapEntryWithSubentries]:
    entries_with_subentries = []

    grouped = mailmap_df.groupby("entry_email")

    for entry_email, group_df in grouped:
        # Construct subentries for the current entry_email group
        subentries = []
        for _, row in group_df.iterrows():
            subentry = MailmapSubentry(
                mailmap_subentry_email=row["subentry_email"],
                mailmap_subentry_name=row["subentry_name"],
            )
            subentries.append(subentry)

        entry = MailmapEntry(
            mailmap_entry_email=str(entry_email),
            mailmap_entry_name=group_df.iloc[0]["entry_name"],
        )

        entry_with_subentries = MailmapEntryWithSubentries(
            entry=entry,
            subentries=subentries,
        )

        entries_with_subentries.append(entry_with_subentries)

    return entries_with_subentries


async def main() -> None:
    mailmap_df = read_mailmap(MAILMAP_PATH)
    entries_with_subentries = build_entries_with_subentries(mailmap_df)
    LOGGER_CONSOLE.info(
        "Total number of entries %s",
        len(entries_with_subentries),
    )
    for entry_with_subentries in entries_with_subentries:
        try:
            await create_mailmap_entry_with_subentries(
                entry_with_subentries=entry_with_subentries,
                organization_id=ORGANIZATION_ID,
            )
            LOGGER_CONSOLE.info(
                "Mailmap entry inserted %s",
                entry_with_subentries["entry"]["mailmap_entry_email"],
            )
        except (MailmapEntryAlreadyExists, MailmapSubentryAlreadyExists) as ex:
            LOGGER_CONSOLE.error(ex)
        except MailmapOrganizationNotFound as ex:
            LOGGER_CONSOLE.error(ex)

    LOGGER_CONSOLE.info("Mailmap data inserted successfully")


if __name__ == "__main__":
    execution_time = time.strftime("Start Time:        %Y-%m-%d at %H:%M:%S UTC")
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S UTC")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
