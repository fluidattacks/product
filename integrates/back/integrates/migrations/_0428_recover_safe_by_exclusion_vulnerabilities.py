"""
Converts vulnerabilities that are SAFE by exclusion, to VULNERABLE.

Start Time:        2023-09-01 at 15:41:31 UTC
Finalization Time: 2023-09-01 at 15:42:40 UTC
"""

import logging
import time
from itertools import (
    chain,
)

from aioextensions import (
    collect,
    run,
)

from integrates.custom_utils import (
    datetime as datetime_utils,
)
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model.roots.types import (
    Root,
)
from integrates.db_model.vulnerabilities.enums import (
    VulnerabilityStateReason,
    VulnerabilityStateStatus,
)
from integrates.db_model.vulnerabilities.types import (
    Vulnerability,
)
from integrates.db_model.vulnerabilities.update import (
    update_historic_entry,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)

LOGGER = logging.getLogger(__name__)
LOGGER_CONSOLE = logging.getLogger("console")


async def get_group_roots(
    loaders: Dataloaders,
    groups: list,
) -> list[Root]:
    grouped_roots = await loaders.group_roots.load_many(groups)
    return list(chain(*grouped_roots))


async def get_root_vulnerabilities(
    loaders: Dataloaders,
    root_id: str,
) -> list[Vulnerability]:
    return await loaders.root_vulnerabilities.load(root_id)


async def set_new_vulnerability_state(vuln: Vulnerability) -> None:
    vulnerability_update = vuln.state._replace(
        modified_date=datetime_utils.get_utc_now(),
        status=VulnerabilityStateStatus.VULNERABLE,
        reasons=[VulnerabilityStateReason.OTHER],
    )
    await update_historic_entry(
        current_value=vuln,
        finding_id=vuln.finding_id,
        entry=vulnerability_update,
        vulnerability_id=vuln.id,
        force_update=True,
    )


async def main() -> None:
    loaders: Dataloaders = get_new_context()
    groups = [""]
    nicknames = [""]
    roots = await get_group_roots(loaders, groups)
    vulnerabilities = tuple(
        chain.from_iterable(
            await collect(
                tuple(
                    get_root_vulnerabilities(loaders, root.id)
                    for root in roots
                    if root.state.nickname in nicknames
                ),
                workers=10,
            ),
        ),
    )

    for vuln in vulnerabilities:
        await set_new_vulnerability_state(vuln)


if __name__ == "__main__":
    execution_time = time.strftime("Start Time:    %Y-%m-%d at %H:%M:%S UTC")
    LOGGER_CONSOLE.info("%s", execution_time)
    run(main())
    finalization_time = time.strftime("Finalization Time: %Y-%m-%d at %H:%M:%S UTC")
    LOGGER_CONSOLE.info("\n%s\n%s", execution_time, finalization_time)
