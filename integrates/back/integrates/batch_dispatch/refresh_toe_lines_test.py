import os
import shutil
import tarfile
from datetime import datetime

from fluidattacks_core.git import CommitInfo, reset_repo
from git.repo import Repo

from integrates import batch_dispatch
from integrates.batch_dispatch.refresh_toe_lines import refresh_toe_lines_simple_args
from integrates.dataloaders import Dataloaders, get_new_context
from integrates.db_model.toe_lines.types import RootToeLinesRequest
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    GitRootFaker,
    GitRootStateFaker,
    GroupFaker,
    OrganizationFaker,
    StakeholderFaker,
    ToeLinesFaker,
    ToeLinesStateFaker,
)
from integrates.testing.mocks import Mock, mocks
from integrates.testing.utils import freeze_time

EMAIL_TEST = "admin@fluidattacks.com"
CLIENT_EMAIL_TEST = "client@client_app.com"
GROUP_NAME = "group1"
ROOT_ID = "root1"
BASE_PATH = os.path.join(os.path.dirname(__file__), "test-data")
FILE_PATH = os.path.join(BASE_PATH, "repo_mock.tar.gz")
REPO_PATH = os.path.join(BASE_PATH, "repo_mock")


def create_repo() -> Repo:
    with tarfile.open(FILE_PATH, "r:gz") as tar:
        tar.extractall(path=BASE_PATH, filter="data")
    return Repo(path=REPO_PATH)


@freeze_time("2024-10-14T00:00:00")
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(id="org1"),
            ],
            stakeholders=[
                StakeholderFaker(email=EMAIL_TEST, role="admin"),
            ],
            groups=[
                GroupFaker(name=GROUP_NAME, organization_id="org1"),
            ],
            roots=[
                GitRootFaker(
                    group_name=GROUP_NAME,
                    id=ROOT_ID,
                    state=GitRootStateFaker(
                        nickname="back",
                    ),
                ),
            ],
            toe_lines=[
                ToeLinesFaker(
                    filename="test1/test.sh",
                    group_name="group1",
                    root_id=ROOT_ID,
                    state=ToeLinesStateFaker(
                        loc=4324,
                        be_present=True,
                        has_vulnerabilities=True,
                        attacked_by="test@test.com",
                        attacked_lines=23,
                    ),
                ),
            ],
        ),
    ),
    others=[Mock(batch_dispatch.refresh_toe_lines, "download_repo", "async", create_repo())],
)
async def test_refresh_toe_lines_full() -> None:
    await reset_repo(REPO_PATH)

    result = await refresh_toe_lines_simple_args(
        group_name=GROUP_NAME, git_root_id=ROOT_ID, user_email="admin@flduiattacks.com"
    )
    assert result == (True, "Finished refreshing toe lines on ACTIVE root")
    shutil.rmtree(REPO_PATH)
    loaders: Dataloaders = get_new_context()
    root_toe_lines = await loaders.root_toe_lines.load_nodes(
        RootToeLinesRequest(group_name=GROUP_NAME, root_id=ROOT_ID),
    )
    result_toe_lines: list[dict[str, str]] = [
        {
            "attackedBy": toe_line.state.attacked_by,
            "attackedLines": str(toe_line.state.attacked_lines),
            "filename": toe_line.filename,
            "lastAuthor": toe_line.state.last_author,
            "lastCommit": toe_line.state.last_commit,
            "loc": str(toe_line.state.loc),
        }
        for toe_line in root_toe_lines
    ]
    sorted_toe_lines = sorted(result_toe_lines, key=lambda x: x["filename"])

    expected_toe_lines = [
        {
            "attackedBy": "",
            "attackedLines": "0",
            "filename": "back/mock.py",
            "lastAuthor": "authoremail@test.com",
            "lastCommit": "6e119ae968656c52bfe85f80329c6b8400fb7921",
            "loc": "6",
        },
        {
            "attackedBy": "",
            "attackedLines": "0",
            "filename": "back/src/mock.py",
            "lastAuthor": "authoremail@test.com",
            "lastCommit": "6e119ae968656c52bfe85f80329c6b8400fb7921",
            "loc": "2",
        },
        {
            "attackedBy": "",
            "attackedLines": "1",
            "filename": "front/mock.css",
            "lastAuthor": "authoremail@test.com",
            "lastCommit": "6e4a6706dff4332a73251e4f2e7fbf67025f369e",
            "loc": "1",
        },
        {
            "attackedBy": "",
            "attackedLines": "0",
            "filename": "front/mock.js",
            "lastAuthor": "authoremail@test.com",
            "lastCommit": "3ca2ffbfeae4f2df16810359a9363231fabc1750",
            "loc": "4",
        },
        {
            "attackedBy": "test@test.com",
            "attackedLines": "0",
            "filename": "test1/test.sh",
            "lastAuthor": "authoremail@test.com",
            "lastCommit": "50a516954a321f95c6fb8baccb640e87d2f5d193",
            "loc": "4",
        },
        {
            "attackedBy": "",
            "attackedLines": "0",
            "filename": "test4/test.sh",
            "lastAuthor": "authoremail@test.com",
            "lastCommit": "50a516954a321f95c6fb8baccb640e87d2f5d193",
            "loc": "4",
        },
        {
            "attackedBy": "",
            "attackedLines": "0",
            "filename": "test5/test.sh",
            "lastAuthor": "authoremail@test.com",
            "lastCommit": "50a516954a321f95c6fb8baccb640e87d2f5d193",
            "loc": "3",
        },
    ]

    assert sorted_toe_lines == expected_toe_lines


@freeze_time("2024-10-14T00:00:00")
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            stakeholders=[StakeholderFaker(email=EMAIL_TEST, role="admin")],
            groups=[GroupFaker(name=GROUP_NAME, organization_id="org1")],
            roots=[
                GitRootFaker(
                    group_name=GROUP_NAME,
                    id=ROOT_ID,
                    state=GitRootStateFaker(nickname="back"),
                ),
            ],
            toe_lines=[
                ToeLinesFaker(
                    filename="src/main.py",
                    group_name="group1",
                    root_id=ROOT_ID,
                    state=ToeLinesStateFaker(
                        loc=180,
                        be_present=True,
                        has_vulnerabilities=False,
                        attacked_by="",
                    ),
                ),
                ToeLinesFaker(
                    filename="package.json",
                    group_name="group1",
                    root_id=ROOT_ID,
                    state=ToeLinesStateFaker(
                        loc=1800,
                        be_present=True,
                        has_vulnerabilities=False,
                        attacked_by="",
                    ),
                ),
                ToeLinesFaker(
                    filename="test/test.py",
                    group_name="group1",
                    root_id=ROOT_ID,
                    state=ToeLinesStateFaker(
                        loc=180,
                        be_present=True,
                        has_vulnerabilities=False,
                        last_commit="a4d56f9b8d7c4c9e9f5b12c3a7f8c9e1e2a4b4c6",
                        last_author="authoremail@test.com",
                        attacked_by="",
                    ),
                ),
            ],
        ),
    ),
    others=[
        Mock(batch_dispatch.refresh_toe_lines, "download_repo", "async", Repo()),
        Mock(
            batch_dispatch.refresh_toe_lines,
            "get_present_filenames",
            "async",
            {"src/main.py", "package.json", "new_app/main.py"},
        ),
        Mock(batch_dispatch.refresh_toe_lines, "files_get_lines_count", "async", 100),
        Mock(
            batch_dispatch.refresh_toe_lines,
            "git_get_last_commit_info",
            "async",
            CommitInfo(
                hash="6e119ae968656c52bfe85f80329c6b8400fb7921",
                author=CLIENT_EMAIL_TEST,
                modified_date=datetime.fromisoformat("2021-11-11T17:41:46+00:00"),
            ),
        ),
    ],
)
async def test_refresh_toe_lines_mocked_features() -> None:
    result = await refresh_toe_lines_simple_args(
        group_name=GROUP_NAME, git_root_id=ROOT_ID, user_email="admin@flduiattacks.com"
    )
    assert result == (True, "Finished refreshing toe lines on ACTIVE root")

    loaders: Dataloaders = get_new_context()
    root_toe_lines = await loaders.root_toe_lines.load_nodes(
        RootToeLinesRequest(group_name=GROUP_NAME, root_id=ROOT_ID),
    )

    result_toe_lines: list[dict[str, str]] = [
        {
            "attackedBy": toe_line.state.attacked_by,
            "attackedLines": str(toe_line.state.attacked_lines),
            "filename": toe_line.filename,
            "lastAuthor": toe_line.state.last_author,
            "lastCommit": toe_line.state.last_commit,
            "loc": str(toe_line.state.loc),
        }
        for toe_line in root_toe_lines
    ]
    sorted_toe_lines = sorted(result_toe_lines, key=lambda x: x["filename"])

    expected_toe_lines = [
        {
            "attackedBy": "",
            "attackedLines": "0",
            "filename": "new_app/main.py",
            "lastAuthor": CLIENT_EMAIL_TEST,
            "lastCommit": "6e119ae968656c52bfe85f80329c6b8400fb7921",
            "loc": "100",
        },
        {
            "attackedBy": "",
            "attackedLines": "100",
            "filename": "package.json",
            "lastAuthor": CLIENT_EMAIL_TEST,
            "lastCommit": "6e119ae968656c52bfe85f80329c6b8400fb7921",
            "loc": "100",
        },
        {
            "attackedBy": "",
            "attackedLines": "0",
            "filename": "src/main.py",
            "lastAuthor": CLIENT_EMAIL_TEST,
            "lastCommit": "6e119ae968656c52bfe85f80329c6b8400fb7921",
            "loc": "100",
        },
        {
            "attackedBy": "",
            "attackedLines": "0",
            "filename": "test/test.py",
            "lastAuthor": "authoremail@test.com",
            "lastCommit": "a4d56f9b8d7c4c9e9f5b12c3a7f8c9e1e2a4b4c6",
            "loc": "180",
        },
    ]

    assert sorted_toe_lines == expected_toe_lines

    first_toe_line = root_toe_lines[0]
    modified_date = first_toe_line.state.modified_date
    assert (modified_date.year, modified_date.month, modified_date.day) == (2024, 10, 14)
    assert first_toe_line.state.sorts_priority_factor == -1
    seen_at = first_toe_line.state.seen_at
    assert (seen_at.year, seen_at.month, seen_at.day) == (2024, 10, 14)
