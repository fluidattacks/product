from datetime import datetime

from integrates.batch.dal.get import get_actions_by_name
from integrates.batch.enums import Action, IntegratesBatchQueue
from integrates.batch.types import BatchProcessing
from integrates.batch_dispatch.move_root import move_root
from integrates.dataloaders import get_new_context
from integrates.db_model.credentials.types import HttpsSecret
from integrates.db_model.roots.types import RootDockerImagesRequest
from integrates.db_model.toe_inputs.types import RootToeInputsRequest
from integrates.db_model.toe_lines.types import RootToeLinesRequest
from integrates.db_model.vulnerabilities.enums import (
    VulnerabilityStateReason,
    VulnerabilityStateStatus,
)
from integrates.db_model.vulnerabilities.types import GroupVulnerabilitiesRequest
from integrates.jobs_orchestration import common_orchestration_utils
from integrates.roots import domain as roots_domain
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    CredentialsFaker,
    FindingFaker,
    GitRootFaker,
    GroupFaker,
    OrganizationAccessFaker,
    OrganizationAccessStateFaker,
    OrganizationFaker,
    RootDockerImageFaker,
    RootDockerImageStateFaker,
    StakeholderFaker,
    ToeInputFaker,
    ToeInputStateFaker,
    ToeLinesFaker,
    VulnerabilityFaker,
    VulnerabilityStateFaker,
)
from integrates.testing.mocks import Mock, mocks
from integrates.vulnerabilities.domain.core import get_vulnerability


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            groups=[
                GroupFaker(name="west", organization_id="org1"),
                GroupFaker(name="east", organization_id="org1"),
            ],
            findings=[
                FindingFaker(group_name="west", id="find1"),
                FindingFaker(
                    group_name="west", id="find2", title="002. Asymmetric denial of service"
                ),
                FindingFaker(group_name="west", id="find3", title="004. Remote command execution"),
            ],
            roots=[
                GitRootFaker(group_name="west", id="root1"),
                GitRootFaker(group_name="east", id="root2"),
            ],
            vulnerabilities=[
                VulnerabilityFaker(
                    group_name="west",
                    root_id="root1",
                    finding_id="find1",
                    id="vuln1",
                    created_by="hacker@fluidattacks.com",
                    state=VulnerabilityStateFaker(status=VulnerabilityStateStatus.VULNERABLE),
                ),
                VulnerabilityFaker(
                    group_name="west",
                    root_id="root1",
                    finding_id="find1",
                    id="vuln2",
                    created_by="hacker@fluidattacks.com",
                    state=VulnerabilityStateFaker(
                        status=VulnerabilityStateStatus.SAFE,
                        reasons=[VulnerabilityStateReason.EXCLUSION],
                    ),
                ),
                VulnerabilityFaker(
                    group_name="west",
                    root_id="root1",
                    finding_id="find1",
                    id="vuln3",
                    created_by="hacker@fluidattacks.com",
                    state=VulnerabilityStateFaker(status=VulnerabilityStateStatus.SUBMITTED),
                ),
                VulnerabilityFaker(
                    group_name="west",
                    root_id="root1",
                    finding_id="find1",
                    id="vuln4",
                    created_by="hacker@fluidattacks.com",
                    state=VulnerabilityStateFaker(status=VulnerabilityStateStatus.REJECTED),
                ),
                VulnerabilityFaker(
                    group_name="west",
                    root_id="root1",
                    finding_id="find2",
                    id="vuln5",
                    created_by="hacker@fluidattacks.com",
                    state=VulnerabilityStateFaker(
                        status=VulnerabilityStateStatus.SAFE,
                        reasons=[VulnerabilityStateReason.ROOT_MOVED_TO_ANOTHER_GROUP],
                    ),
                ),
                VulnerabilityFaker(
                    group_name="west",
                    root_id="root1",
                    finding_id="find3",
                    id="vuln6",
                    created_by="hacker@fluidattacks.com",
                    state=VulnerabilityStateFaker(status=VulnerabilityStateStatus.DELETED),
                ),
            ],
            stakeholders=[
                StakeholderFaker(email="jdoe@fluidattacks.com", role="customer_manager"),
            ],
            organization_access=[
                OrganizationAccessFaker(
                    email="jdoe@fluidattacks.com",
                    organization_id="org1",
                    state=OrganizationAccessStateFaker(role="customer_manager", has_access=True),
                ),
            ],
            toe_inputs=[
                ToeInputFaker(
                    group_name="west",
                    root_id="root1",
                    component="https://test.com/",
                    entry_point="user",
                    state=ToeInputStateFaker(has_vulnerabilities=True),
                ),
            ],
        ),
    )
)
async def test_move_root() -> None:
    source = ("west", "root1")
    target_group_name = "east"
    target_root_id = "root2"
    email = "jdoe@fluidattacks.com"
    hacker_email = "hacker@fluidattacks.com"

    await move_root(
        item=BatchProcessing(
            action_name=Action.MOVE_ROOT,
            entity=source[0],
            subject=email,
            additional_info={
                "target_group_name": target_group_name,
                "source_group_name": source[0],
                "source_root_id": source[1],
                "target_root_id": target_root_id,
            },
            queue=IntegratesBatchQueue.SMALL,
            key="key1",
            batch_job_id="job1",
            time=datetime.now(),
        ),
    )

    loaders = get_new_context()
    result_inputs = await loaders.root_toe_inputs.load(
        RootToeInputsRequest(group_name=target_group_name, root_id=target_root_id),
    )
    assert len(result_inputs.edges) == 1
    assert result_inputs.edges[0].node.component == "https://test.com/"
    assert result_inputs.edges[0].node.entry_point == "user"

    result_vulns = await loaders.group_vulnerabilities.load(
        GroupVulnerabilitiesRequest(group_name=target_group_name),
    )
    assert len(result_vulns.edges) == 4
    assert result_vulns.edges[0].node.created_by == hacker_email
    for vuln in result_vulns.edges:
        if vuln.node.state.status == VulnerabilityStateStatus.SAFE:
            assert vuln.node.state.reasons == [VulnerabilityStateReason.EXCLUSION]

    inputs_actions = await get_actions_by_name(
        action_name=Action.REFRESH_TOE_INPUTS, entity=target_group_name
    )
    lines_actions = await get_actions_by_name(
        action_name=Action.REFRESH_TOE_LINES, entity=target_group_name
    )
    ports_actions = await get_actions_by_name(
        action_name=Action.REFRESH_TOE_PORTS, entity=target_group_name
    )
    assert len(inputs_actions) == 1
    assert len(lines_actions) == 1
    assert len(ports_actions) == 0

    findings = await loaders.group_findings.load(target_group_name)
    assert len(findings) == 1

    vuln1 = await get_vulnerability(loaders, "vuln1", clear_loader=True)
    vuln2 = await get_vulnerability(loaders, "vuln2", clear_loader=True)
    vuln3 = await get_vulnerability(loaders, "vuln3", clear_loader=True)
    vuln4 = await get_vulnerability(loaders, "vuln4", clear_loader=True)

    assert vuln1.state.status == VulnerabilityStateStatus.SAFE
    assert vuln2.state.status == VulnerabilityStateStatus.SAFE
    assert vuln3.state.status == VulnerabilityStateStatus.SUBMITTED
    assert vuln4.state.status == VulnerabilityStateStatus.REJECTED

    await move_root(
        item=BatchProcessing(
            action_name=Action.MOVE_ROOT,
            entity=source[0],
            subject=email,
            additional_info={
                "target_group_name": target_group_name,
                "source_group_name": source[0],
                "source_root_id": source[1],
                "target_root_id": target_root_id,
            },
            queue=IntegratesBatchQueue.SMALL,
            key="key1",
            batch_job_id="job1",
            time=datetime.now(),
        ),
    )

    loaders = get_new_context()
    result_inputs = await loaders.root_toe_inputs.load(
        RootToeInputsRequest(group_name=target_group_name, root_id=target_root_id),
    )
    assert len(result_inputs.edges) == 1
    assert result_inputs.edges[0].node.component == "https://test.com/"
    assert result_inputs.edges[0].node.entry_point == "user"

    result_vulns = await loaders.group_vulnerabilities.load(
        GroupVulnerabilitiesRequest(group_name=target_group_name),
    )
    assert len(result_vulns.edges) == 4
    assert result_vulns.edges[0].node.created_by == hacker_email

    inputs_actions = await get_actions_by_name(
        action_name=Action.REFRESH_TOE_INPUTS, entity=target_group_name
    )
    lines_actions = await get_actions_by_name(
        action_name=Action.REFRESH_TOE_LINES, entity=target_group_name
    )
    ports_actions = await get_actions_by_name(
        action_name=Action.REFRESH_TOE_PORTS, entity=target_group_name
    )
    assert len(inputs_actions) == 1
    assert len(lines_actions) == 1
    assert len(ports_actions) == 0

    findings = await loaders.group_findings.load(target_group_name)
    assert len(findings) == 1


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="ORG#org1")],
            groups=[
                GroupFaker(name="west", organization_id="ORG#org1"),
                GroupFaker(name="east", organization_id="ORG#org1"),
            ],
            roots=[
                GitRootFaker(group_name="west", id="root1"),
                GitRootFaker(group_name="east", id="root2"),
            ],
            stakeholders=[
                StakeholderFaker(email="jdoe@fluidattacks.com", role="customer_manager"),
            ],
            organization_access=[
                OrganizationAccessFaker(
                    email="jdoe@fluidattacks.com",
                    organization_id="ORG#org1",
                    state=OrganizationAccessStateFaker(role="customer_manager", has_access=True),
                ),
            ],
            docker_images=[
                RootDockerImageFaker(
                    uri="docker.io/test-dummy-image:0.0.1",
                    group_name="west",
                    root_id="root1",
                    state=RootDockerImageStateFaker(credential_id="test-credential"),
                ),
                RootDockerImageFaker(
                    uri="docker.io/test-dummy-image:0.0.2",
                    group_name="west",
                    root_id="root1",
                    state=RootDockerImageStateFaker(
                        credential_id="test-credential",
                        digest=(
                            "sha256:7c2c9c72b3c65b9881c2a3ff19c8b3c43b6a0d925602cbf"
                            "9033467b0b7f6f9b132"
                        ),
                    ),
                ),
            ],
            toe_lines=[
                ToeLinesFaker(
                    filename="Dockerfile",
                    group_name="west",
                    root_id="root1",
                ),
            ],
            credentials=[
                CredentialsFaker(
                    credential_id="test-credential",
                    organization_id="ORG#org1",
                    secret=HttpsSecret(user="jdoe@fluidattacks.com", password="pass"),  # noqa: S106
                ),
            ],
        ),
    ),
    others=[
        Mock(
            common_orchestration_utils,
            "upload_config_to_s3",
            "async",
            None,
        ),
        Mock(
            roots_domain,
            "get_image_manifest",
            "async",
            (
                {
                    "Digest": "sha256:fe6a5f3f5555b3c6bfb5b6e0e"
                    "7639db8b8c2272d0848f214d7340d92521bf42d",
                    "Layers": [
                        "sha256:de44b265507ae44b212defcb50694d666f136b35c1090d9709068bc861bb2d64",
                        "sha256:add2cfa32b4d238ee12f15bfd6aa93ccfe9642fd196c8547875c8283b2368d7a",
                    ],
                    "LayersData": [
                        {
                            "MIMEType": "application/vnd.oci.image.layer.v1.tar+gzip",
                            "Digest": "sha256:de44b265507ae44b212defcb50694d666f136b35"
                            "c1090d9709068bc861bb2d64",
                            "Size": 29751968,
                            "Annotations": None,
                        },
                        {
                            "MIMEType": "application/vnd.oci.image.layer.v1.tar+gzip",
                            "Digest": "sha256:add2cfa32b4d238ee12f15bfd6aa93ccfe9642fd"
                            "196c8547875c8283b2368d7a",
                            "Size": 1217,
                            "Annotations": None,
                        },
                    ],
                }
            ),
        ),
    ],
)
async def test_move_root_move_docker_images() -> None:
    source = ("west", "root1")
    target_group_name = "east"
    target_root_id = "root2"
    email = "jdoe@fluidattacks.com"
    loaders = get_new_context()

    repo_toe_lines = await loaders.root_toe_lines.load_nodes(
        RootToeLinesRequest(group_name=source[0], root_id=source[1]),
    )
    assert len(repo_toe_lines) == 1

    await move_root(
        item=BatchProcessing(
            action_name=Action.MOVE_ROOT,
            entity=source[0],
            subject=email,
            additional_info={
                "target_group_name": target_group_name,
                "source_group_name": source[0],
                "source_root_id": source[1],
                "target_root_id": target_root_id,
            },
            queue=IntegratesBatchQueue.SMALL,
            key="key1",
            batch_job_id="job1",
            time=datetime.now(),
        ),
    )

    loaders = get_new_context()
    results_docker_images = await loaders.root_docker_images.load(
        RootDockerImagesRequest(root_id=target_root_id, group_name=target_group_name)
    )
    assert results_docker_images
    assert len(results_docker_images) == 2
    assert results_docker_images[0].uri == "docker.io/test-dummy-image:0.0.1"
    assert results_docker_images[1].uri == "docker.io/test-dummy-image:0.0.2"
    assert results_docker_images[0].state.credential_id == "test-credential"

    source_docker_images = await loaders.root_docker_images.load(
        RootDockerImagesRequest(root_id=source[1], group_name=source[0])
    )
    assert source_docker_images
    assert len(source_docker_images) == 2
    assert source_docker_images[0].uri == "docker.io/test-dummy-image:0.0.1"
    assert source_docker_images[1].uri == "docker.io/test-dummy-image:0.0.2"
    assert source_docker_images[0].state.include is False
    assert source_docker_images[1].state.include is False
