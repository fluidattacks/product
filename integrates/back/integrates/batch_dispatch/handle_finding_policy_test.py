from integrates.batch.enums import Action, IntegratesBatchQueue
from integrates.batch.types import BatchProcessing
from integrates.batch_dispatch.handle_finding_policy import handle_finding_policy
from integrates.dataloaders import get_new_context
from integrates.db_model.enums import TreatmentStatus
from integrates.db_model.findings.enums import FindingStatus
from integrates.db_model.organization_finding_policies.enums import PolicyStateStatus
from integrates.db_model.types import Treatment
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    DATE_2024,
    FindingFaker,
    FindingUnreliableIndicatorsFaker,
    GroupFaker,
    OrganizationAccessFaker,
    OrganizationFaker,
    OrgFindingPolicyFaker,
    OrgFindingPolicyStateFaker,
    StakeholderFaker,
    VulnerabilityFaker,
    random_uuid,
)
from integrates.testing.mocks import mocks

FIN_NAME = "318. Insecurely generated token - Validation"
FIN_ID = "3c475384-834c-47b0-ac71-a41a022e401c"
EMAIL_TEST = "test@fluidattacks.com"
GROUP_NAME = "grouptest"
ORG_NAME = "orgtest"


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            findings=[
                FindingFaker(
                    id=FIN_ID,
                    title=FIN_NAME,
                    group_name=GROUP_NAME,
                    unreliable_indicators=FindingUnreliableIndicatorsFaker(
                        unreliable_status=FindingStatus.VULNERABLE
                    ),
                )
            ],
            organization_finding_policies=[
                OrgFindingPolicyFaker(
                    id="test-finding-policy-id",
                    name=FIN_NAME,
                    organization_name=ORG_NAME,
                    state=OrgFindingPolicyStateFaker(
                        modified_by=EMAIL_TEST,
                        modified_date=DATE_2024,
                        status=PolicyStateStatus.APPROVED,
                    ),
                    treatment_acceptance=TreatmentStatus.ACCEPTED_UNDEFINED,
                )
            ],
            organization_access=[OrganizationAccessFaker(organization_id="org1", email=EMAIL_TEST)],
            organizations=[OrganizationFaker(id="org1", name=ORG_NAME)],
            stakeholders=[StakeholderFaker(email=EMAIL_TEST, role="admin", enrolled=True)],
            groups=[GroupFaker(name=GROUP_NAME, organization_id="org1")],
            vulnerabilities=[
                VulnerabilityFaker(
                    id=random_uuid(),
                    created_by=EMAIL_TEST,
                    finding_id=FIN_ID,
                    group_name=GROUP_NAME,
                    organization_name=ORG_NAME,
                    treatment=Treatment(modified_date=DATE_2024, status=TreatmentStatus.UNTREATED),
                ),
                VulnerabilityFaker(
                    id=random_uuid(),
                    created_by=EMAIL_TEST,
                    finding_id=FIN_ID,
                    group_name=GROUP_NAME,
                    organization_name=ORG_NAME,
                    treatment=Treatment(modified_date=DATE_2024, status=TreatmentStatus.UNTREATED),
                ),
            ],
        ),
    ),
)
async def test_handle_finding_policy() -> None:
    # Act
    loaders = get_new_context()

    await handle_finding_policy(
        item=BatchProcessing(
            key=random_uuid(),
            action_name=Action.HANDLE_FINDING_POLICY,
            time=DATE_2024,
            entity="test-finding-policy-id",
            subject=EMAIL_TEST,
            queue=IntegratesBatchQueue.SMALL,
            additional_info={"organization_name": ORG_NAME},
        )
    )
    vulns = await loaders.finding_vulnerabilities_released_nzr.load_many_chained([FIN_ID])

    # Assert
    assert vulns
    assert len(vulns) == 2
    assert all(
        vuln.treatment.status == TreatmentStatus.ACCEPTED_UNDEFINED
        for vuln in vulns
        if vuln.treatment
    )
