import integrates.mailer.common as mailer_common
from integrates.batch import dal as batch_dal
from integrates.batch.enums import Action, IntegratesBatchQueue
from integrates.batch.types import BatchProcessing
from integrates.batch_dispatch.remove_group_resources import remove_group_resources
from integrates.dataloaders import get_new_context
from integrates.db_model.group_access.types import GroupStakeholdersAccessRequest
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    DATE_2024,
    BatchProcessingFaker,
    GitRootFaker,
    GitRootStateFaker,
    GroupAccessFaker,
    GroupAccessStateFaker,
    GroupFaker,
    OrganizationAccessFaker,
    OrganizationFaker,
    StakeholderFaker,
    random_uuid,
)
from integrates.testing.mocks import Mock, mocks
from integrates.testing.utils import freeze_time

EMAIL_TEST = "test@fluidattacks.com"
GROUP_NAME = "grouptest"
ROOT_ID = "root1"


@freeze_time("2024-10-14T00:00:00")
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            actions=[
                BatchProcessingFaker(
                    action_name=Action.EXECUTE_MACHINE,
                    entity=GROUP_NAME,
                    additional_info={"roots": [ROOT_ID], "roots_config_files": ["root1.yaml"]},
                ),
            ],
            organizations=[OrganizationFaker(id="org1", name="orgtest")],
            organization_access=[OrganizationAccessFaker(organization_id="org1", email=EMAIL_TEST)],
            stakeholders=[StakeholderFaker(email=EMAIL_TEST, role="admin")],
            groups=[GroupFaker(name=GROUP_NAME, organization_id="org1")],
            group_access=[
                GroupAccessFaker(
                    email=EMAIL_TEST,
                    group_name=GROUP_NAME,
                    state=GroupAccessStateFaker(modified_date=DATE_2024, role="customer_manager"),
                )
            ],
            roots=[
                GitRootFaker(
                    group_name=GROUP_NAME,
                    id=ROOT_ID,
                    organization_name="orgtest",
                    state=GitRootStateFaker(nickname="git1"),
                ),
            ],
        ),
    ),
    others=[Mock(mailer_common, "send_mail_async", "async", None)],
)
async def test_remove_group_resources() -> None:
    # Act
    loaders = get_new_context()

    await remove_group_resources(
        item=BatchProcessing(
            key=random_uuid(),
            action_name=Action.REMOVE_GROUP_RESOURCES,
            time=DATE_2024,
            entity=GROUP_NAME,
            subject=EMAIL_TEST,
            queue=IntegratesBatchQueue.SMALL,
            additional_info={
                "validate_pending_actions": "True",
                "subscription": "CONTINUOUS",
                "has_advanced": "True",
                "emails": [],
            },
        )
    )
    group_actions: list[BatchProcessing] = [
        action
        for action in await batch_dal.get_actions()
        if action.entity == GROUP_NAME and action.action_name != Action.REMOVE_GROUP_RESOURCES
    ]

    # Assert
    assert group_actions == []
    assert await loaders.group_roots.load(GROUP_NAME) == []
    assert (
        await loaders.group_stakeholders_access.load(
            GroupStakeholdersAccessRequest(group_name=GROUP_NAME),
        )
        == []
    )
