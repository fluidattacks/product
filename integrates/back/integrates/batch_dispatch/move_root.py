import itertools
import logging
import logging.config
import uuid
from contextlib import suppress
from io import BytesIO
from operator import attrgetter

import aiofiles
from aioextensions import collect
from opentelemetry import trace
from starlette.datastructures import UploadFile

import integrates.findings.domain.events
from integrates.batch import dal as batch_dal
from integrates.batch.enums import Action, IntegratesBatchQueue
from integrates.batch.types import BatchProcessing
from integrates.batch_dispatch.utils.telemetry import is_observable
from integrates.custom_exceptions import (
    EventAlreadyCreated,
    FileIsExcluded,
    RepeatedToeInput,
    RepeatedToeLines,
    RepeatedToePort,
    RootDockerfileNotExists,
    ToeInputAlreadyUpdated,
    ToeInputNotFound,
    ToeLinesAlreadyUpdated,
    ToeLinesNotFound,
    ToePortAlreadyUpdated,
    ToePortNotFound,
    VulnAlreadyCreated,
)
from integrates.custom_utils import datetime as datetime_utils
from integrates.custom_utils import filter_vulnerabilities as filter_vulns_utils
from integrates.custom_utils import findings as findings_utils
from integrates.custom_utils import roots as roots_utils
from integrates.custom_utils import vulnerabilities as vulns_utils
from integrates.custom_utils.stakeholders import get_stakeholder
from integrates.dataloaders import Dataloaders, get_new_context
from integrates.db_model import event_comments as event_comments_model
from integrates.db_model import events as events_model
from integrates.db_model import findings as findings_model
from integrates.db_model import roots as roots_model
from integrates.db_model import vulnerabilities as vulns_model
from integrates.db_model.constants import MACHINE_EMAIL
from integrates.db_model.enums import Notification
from integrates.db_model.event_comments.types import EventCommentsRequest
from integrates.db_model.events.enums import EventEvidenceId, EventSolutionReason
from integrates.db_model.events.types import (
    Event,
    EventEvidence,
    EventEvidences,
    EventRequest,
    GroupEventsRequest,
)
from integrates.db_model.findings.enums import FindingStateStatus
from integrates.db_model.findings.types import Finding, FindingEvidence, FindingState
from integrates.db_model.roots.types import (
    GitRoot,
    IPRoot,
    RootDockerImage,
    RootDockerImagesRequest,
    RootEnvironmentSecretsRequest,
    RootEnvironmentUrl,
    RootEnvironmentUrlsRequest,
    URLRoot,
)
from integrates.db_model.toe_inputs.types import GroupToeInputsRequest, ToeInput, ToeInputRequest
from integrates.db_model.toe_lines.types import RootToeLinesRequest, ToeLine, ToeLineRequest
from integrates.db_model.toe_ports.types import RootToePortsRequest, ToePort, ToePortRequest
from integrates.db_model.vulnerabilities.enums import VulnerabilityStateReason
from integrates.db_model.vulnerabilities.types import (
    Vulnerability,
    VulnerabilityRequest,
    VulnerabilityVerification,
)
from integrates.decorators import retry_on_exceptions
from integrates.dynamodb.exceptions import UnavailabilityError
from integrates.events import domain as events_domain
from integrates.events.constants import EVIDENCE_NAMES as EVENT_EVIDENCE_NAMES
from integrates.findings.domain.evidence import (
    EVIDENCE_NAMES,
    download_evidence_file,
    update_evidence,
)
from integrates.jobs_orchestration.jobs import queue_sbom_image_job
from integrates.jobs_orchestration.model.types import SbomFormat, SbomJobType
from integrates.mailer import utils as mailer_utils
from integrates.mailer.common import send_mails_async
from integrates.roots import domain as roots_domain
from integrates.settings import LOGGING
from integrates.toe.inputs import domain as toe_inputs_domain
from integrates.toe.inputs.types import ToeInputAttributesToAdd, ToeInputAttributesToUpdate
from integrates.toe.lines import domain as toe_lines_domain
from integrates.toe.lines.types import ToeLinesAttributesToAdd, ToeLinesAttributesToUpdate
from integrates.toe.ports import domain as toe_ports_domain
from integrates.toe.ports.types import ToePortAttributesToAdd, ToePortAttributesToUpdate
from integrates.unreliable_indicators.enums import EntityDependency
from integrates.unreliable_indicators.operations import update_unreliable_indicators_by_deps
from integrates.vulnerabilities import domain as vulns_domain
from integrates.vulnerability_files.domain import _sort_vulns_for_comparison

logging.config.dictConfig(LOGGING)

# Constants
TRACER = trace.get_tracer("batch")
LOGGER = logging.getLogger(__name__)


def compare_with_vuln_in_db(vuln_in_db: Vulnerability, vuln_to_add: Vulnerability) -> bool:
    if vuln_in_db.state.status == vuln_to_add.state.status:
        if vuln_to_add.hacker_email == MACHINE_EMAIL:
            return vuln_to_add.hash == vuln_in_db.hash and vuln_to_add.root_id == vuln_in_db.root_id
        return hash(vuln_to_add) == hash(vuln_in_db)

    return False


async def _process_vuln(
    *,
    loaders: Dataloaders,
    vuln: Vulnerability,
    target_finding_id: str,
    target_group_name: str,
    target_root_id: str,
    item_subject: str,
    event_ids: dict[str, str],
    sorted_vulns: list[Vulnerability],
) -> str | None:
    if any(
        compare_with_vuln_in_db(_vuln, vuln._replace(root_id=target_root_id))
        for _vuln in sorted_vulns
    ):
        return None

    LOGGER.info(
        "Processing vuln",
        extra={
            "extra": {
                "vuln_id": vuln.id,
                "target_finding_id": target_finding_id,
                "target_root_id": target_root_id,
            },
        },
    )
    historic_state = await loaders.vulnerability_historic_state.load(
        VulnerabilityRequest(vulnerability_id=vuln.id, finding_id=vuln.finding_id),
    )
    historic_treatment = await loaders.vulnerability_historic_treatment.load(
        VulnerabilityRequest(vulnerability_id=vuln.id, finding_id=vuln.finding_id),
    )
    historic_verification = await loaders.vulnerability_historic_verification.load(
        VulnerabilityRequest(vulnerability_id=vuln.id, finding_id=vuln.finding_id),
    )
    historic_zero_risk = await loaders.vulnerability_historic_zero_risk.load(
        VulnerabilityRequest(vulnerability_id=vuln.id, finding_id=vuln.finding_id),
    )
    new_id = str(uuid.uuid4())
    current_event_id = vuln.event_id.removeprefix("EVENT#") if vuln.event_id else None
    event_id = (
        event_ids.get(current_event_id)
        if current_event_id and event_ids.get(current_event_id)
        else current_event_id
    )
    try:
        await vulns_model.add(
            vulnerability=vuln._replace(
                finding_id=target_finding_id,
                group_name=target_group_name,
                id=new_id,
                root_id=target_root_id,
                state=vuln.state,
                verification=VulnerabilityVerification(
                    event_id=event_id,
                    modified_by=vuln.verification.modified_by,
                    modified_date=vuln.verification.modified_date,
                    status=vuln.verification.status,
                )
                if vuln.verification
                else None,
                event_id=event_id,
            ),
        )
    except VulnAlreadyCreated:
        return None

    LOGGER.info(
        "Created new vuln",
        extra={
            "extra": {
                "target_finding_id": target_finding_id,
                "new_id": new_id,
                "event_id": event_id,
            },
        },
    )
    new_vulnerability = await vulns_domain.get_vulnerability(loaders, new_id)
    await vulns_model.update_historic(
        current_value=new_vulnerability,
        historic=tuple(historic_state) or (vuln.state,),
    )
    new_vulnerability = await vulns_domain.get_vulnerability(loaders, new_id, clear_loader=True)
    await vulns_model.update_new_historic(
        current_value=new_vulnerability,
        historic=tuple(historic_state) or (vuln.state,),
    )
    if historic_treatment:
        new_vulnerability = await vulns_domain.get_vulnerability(loaders, new_id, clear_loader=True)
        await vulns_model.update_historic(
            current_value=new_vulnerability,
            historic=tuple(historic_treatment),
        )
        new_vulnerability = await vulns_domain.get_vulnerability(loaders, new_id, clear_loader=True)
        await vulns_model.update_new_historic(
            current_value=new_vulnerability,
            historic=tuple(historic_treatment),
        )
    if historic_verification:
        new_vulnerability = await vulns_domain.get_vulnerability(loaders, new_id, clear_loader=True)
        await vulns_model.update_historic(
            current_value=new_vulnerability,
            historic=tuple(historic_verification),
        )
        new_vulnerability = await vulns_domain.get_vulnerability(loaders, new_id, clear_loader=True)
        await vulns_model.update_new_historic(
            current_value=new_vulnerability,
            historic=tuple(historic_verification),
        )
    if historic_zero_risk:
        new_vulnerability = await vulns_domain.get_vulnerability(loaders, new_id, clear_loader=True)
        await vulns_model.update_historic(
            current_value=new_vulnerability,
            historic=tuple(historic_zero_risk),
        )
        new_vulnerability = await vulns_domain.get_vulnerability(loaders, new_id, clear_loader=True)
        await vulns_model.update_new_historic(
            current_value=new_vulnerability,
            historic=tuple(historic_zero_risk),
        )
    await vulns_utils.close_vulnerability(
        vulnerability=vuln,
        modified_by=item_subject,
        loaders=loaders,
        closing_reason=VulnerabilityStateReason.ROOT_MOVED_TO_ANOTHER_GROUP,
    )
    LOGGER.info(
        "Old vuln closed because of root move",
        extra={
            "extra": {
                "finding_id": vuln.finding_id,
                "vuln_id": vuln.id,
            },
        },
    )
    return new_id


async def _add_evidence(
    *,
    finding_id: str,
    evidence_id: str,
    loaders: Dataloaders,
    source_finding: Finding,
    evidence: FindingEvidence | None,
) -> None:
    if not evidence:
        return
    loaders.finding.clear(finding_id)
    with suppress(Exception):
        file_path = await download_evidence_file(
            source_finding.group_name,
            source_finding.id,
            evidence.url,
        )
        async with aiofiles.open(file_path, "rb") as new_file:
            file_contents = await new_file.read()
            await update_evidence(
                loaders=loaders,
                finding_id=finding_id,
                evidence_id=evidence_id,
                file_object=UploadFile(filename=str(new_file.name), file=BytesIO(file_contents)),
                author_email=evidence.author_email,
                is_draft=evidence.is_draft,
                description=evidence.description,
            )


async def _get_target_finding(
    *,
    loaders: Dataloaders,
    source_finding: Finding,
    target_group_name: str,
) -> Finding | None:
    target_group_findings = await loaders.group_findings.load(target_group_name)
    source_created_by = (
        source_finding.creation.modified_by
        if source_finding.creation
        else source_finding.state.modified_by
    )
    if source_created_by == MACHINE_EMAIL:
        return next(
            (
                finding
                for finding in target_group_findings
                if finding.get_criteria_code() == source_finding.get_criteria_code()
                and finding.creation
                and finding.creation.modified_by == MACHINE_EMAIL
            ),
            None,
        )

    return next(
        (
            finding
            for finding in target_group_findings
            if finding.title == source_finding.title
            and finding.description == source_finding.description
            and finding.recommendation == source_finding.recommendation
        ),
        None,
    )


@is_observable(TRACER)
async def _process_finding(
    *,
    loaders: Dataloaders,
    source_group_name: str,
    target_group_name: str,
    target_root_id: str,
    source_finding_id: str,
    vulns: tuple[Vulnerability, ...],
    item_subject: str,
    event_ids: dict[str, str],
) -> None:
    if not vulns:
        return

    LOGGER.info(
        "Processing finding",
        extra={
            "extra": {
                "source_group_name": source_group_name,
                "target_group_name": target_group_name,
                "target_root_id": target_root_id,
                "source_finding_id": source_finding_id,
                "vulns": len(vulns),
            },
        },
    )
    source_finding = await loaders.finding.load(source_finding_id)
    if source_finding is None or findings_utils.is_deleted(source_finding):
        LOGGER.info("Not found", extra={"extra": {"finding_id": source_finding_id}})
        return

    target_finding = await _get_target_finding(
        loaders=loaders,
        source_finding=source_finding,
        target_group_name=target_group_name,
    )
    if target_finding:
        target_finding_id = target_finding.id
        LOGGER.info(
            "Found equivalent finding in target_group_findings",
            extra={
                "extra": {
                    "target_group_name": target_group_name,
                    "target_finding_id": target_finding_id,
                },
            },
        )
    else:
        target_finding_id = str(uuid.uuid4())
        if source_finding.creation:
            initial_state = FindingState(
                modified_by=source_finding.creation.modified_by,
                modified_date=source_finding.creation.modified_date,
                source=source_finding.creation.source,
                status=FindingStateStatus.CREATED,
            )
        await findings_model.add(
            finding=Finding(
                attack_vector_description=(source_finding.attack_vector_description),
                description=source_finding.description,
                group_name=target_group_name,
                id=target_finding_id,
                state=initial_state,
                recommendation=source_finding.recommendation,
                requirements=source_finding.requirements,
                severity_score=source_finding.severity_score,
                title=source_finding.title,
                threat=source_finding.threat,
                unfulfilled_requirements=(source_finding.unfulfilled_requirements),
            ),
        )
        await collect(
            [
                _add_evidence(
                    finding_id=target_finding_id,
                    evidence_id=evidence_id,
                    loaders=loaders,
                    source_finding=source_finding,
                    evidence=getattr(source_finding.evidences, evidence_name),
                )
                for evidence_id, evidence_name in EVIDENCE_NAMES.items()
            ],
            workers=1,
        )
        LOGGER.info(
            "Equivalent finding not found. Created new one",
            extra={
                "extra": {
                    "target_group_name": target_group_name,
                    "target_finding_id": target_finding_id,
                },
            },
        )
    sorted_vulns = _sort_vulns_for_comparison(
        await get_new_context().finding_vulnerabilities_all.load(target_finding_id)
    )

    target_vuln_ids = await collect(
        tuple(
            _process_vuln(
                loaders=loaders,
                vuln=vuln,
                target_finding_id=target_finding_id,
                target_group_name=target_group_name,
                target_root_id=target_root_id,
                item_subject=item_subject,
                event_ids=event_ids,
                sorted_vulns=sorted_vulns,
            )
            for vuln in vulns
        ),
        workers=100,
    )
    LOGGER.info(
        "Updating finding indicators",
        extra={
            "extra": {
                "source_group_name": source_group_name,
                "source_finding_id": source_finding_id,
                "target_group_name": target_group_name,
                "target_finding_id": target_finding_id,
            },
        },
    )
    await collect(
        (
            update_unreliable_indicators_by_deps(
                EntityDependency.move_root,
                finding_ids=[source_finding_id],
                vulnerability_ids=[vuln.id for vuln in vulns],
            ),
            update_unreliable_indicators_by_deps(
                EntityDependency.move_root,
                finding_ids=[target_finding_id],
                vulnerability_ids=list(filter(None, target_vuln_ids)),
            ),
        ),
    )


toe_inputs_add = retry_on_exceptions(exceptions=(UnavailabilityError,), sleep_seconds=5)(
    toe_inputs_domain.add,
)
toe_inputs_update = retry_on_exceptions(exceptions=(UnavailabilityError,), sleep_seconds=5)(
    toe_inputs_domain.update,
)


@retry_on_exceptions(
    exceptions=(ToeInputAlreadyUpdated,),
)
@is_observable(TRACER)
async def _process_toe_input(
    loaders: Dataloaders,
    target_group_name: str,
    target_root_id: str,
    toe_input: ToeInput,
) -> None:
    if toe_input.state.seen_at is None:
        return
    attributes_to_add = ToeInputAttributesToAdd(
        attacked_at=toe_input.state.attacked_at,
        attacked_by=toe_input.state.attacked_by,
        be_present=False,
        first_attack_at=toe_input.state.first_attack_at,
        has_vulnerabilities=toe_input.state.has_vulnerabilities,
        seen_first_time_by=toe_input.state.seen_first_time_by,
        seen_at=toe_input.state.seen_at,
    )
    try:
        await toe_inputs_add(
            loaders=loaders,
            entry_point=toe_input.entry_point,
            environment_id=toe_input.environment_id,
            component=toe_input.component,
            group_name=target_group_name,
            root_id=target_root_id,
            attributes=attributes_to_add,
            is_moving_toe_input=True,
        )
    except RepeatedToeInput as exc:
        current_value = await loaders.toe_input.load(
            ToeInputRequest(
                component=toe_input.component,
                entry_point=toe_input.entry_point,
                group_name=target_group_name,
                root_id=target_root_id,
            ),
        )
        attributes_to_update = ToeInputAttributesToUpdate(
            attacked_at=toe_input.state.attacked_at,
            attacked_by=toe_input.state.attacked_by,
            first_attack_at=toe_input.state.first_attack_at,
            has_vulnerabilities=toe_input.state.has_vulnerabilities,
            seen_at=toe_input.state.seen_at,
            seen_first_time_by=toe_input.state.seen_first_time_by,
        )
        if current_value:
            await toe_inputs_update(
                loaders=loaders,
                current_value=current_value,
                attributes=attributes_to_update,
                modified_by="machine@fluidattacks.com",
                is_moving_toe_input=True,
            )
        else:
            raise ToeInputNotFound() from exc


toe_lines_add = retry_on_exceptions(exceptions=(UnavailabilityError,), sleep_seconds=5)(
    toe_lines_domain.add,
)
toe_lines_update = retry_on_exceptions(exceptions=(UnavailabilityError,), sleep_seconds=5)(
    toe_lines_domain.update,
)


@retry_on_exceptions(
    exceptions=(ToeLinesAlreadyUpdated,),
)
@is_observable(TRACER)
async def _process_toe_lines(
    loaders: Dataloaders,
    target_group_name: str,
    target_root_id: str,
    toe_lines: ToeLine,
) -> None:
    attributes_to_add = ToeLinesAttributesToAdd(
        attacked_at=toe_lines.state.attacked_at,
        attacked_by=toe_lines.state.attacked_by,
        attacked_lines=toe_lines.state.attacked_lines,
        comments=toe_lines.state.comments,
        last_author=toe_lines.state.last_author,
        be_present=toe_lines.state.be_present,
        be_present_until=toe_lines.state.be_present_until,
        first_attack_at=toe_lines.state.first_attack_at,
        has_vulnerabilities=toe_lines.state.has_vulnerabilities,
        loc=toe_lines.state.loc,
        last_commit=toe_lines.state.last_commit,
        last_commit_date=toe_lines.state.last_commit_date,
        seen_at=toe_lines.state.seen_at,
        sorts_risk_level=toe_lines.state.sorts_risk_level,
    )
    try:
        await toe_lines_add(
            loaders=loaders,
            group_name=target_group_name,
            root_id=target_root_id,
            filename=toe_lines.filename,
            attributes=attributes_to_add,
            rules_excluded=True,
        )
    except FileIsExcluded:
        pass
    except RepeatedToeLines as exc:
        current_value = await loaders.toe_lines.load(
            ToeLineRequest(
                filename=toe_lines.filename,
                group_name=target_group_name,
                root_id=target_root_id,
            ),
        )
        attributes_to_update = ToeLinesAttributesToUpdate(
            attacked_at=toe_lines.state.attacked_at,
            attacked_by=toe_lines.state.attacked_by,
            attacked_lines=toe_lines.state.attacked_lines,
            comments=toe_lines.state.comments,
            first_attack_at=toe_lines.state.first_attack_at,
            has_vulnerabilities=toe_lines.state.has_vulnerabilities,
            seen_at=toe_lines.state.seen_at,
            sorts_risk_level=toe_lines.state.sorts_risk_level,
        )
        if current_value:
            await toe_lines_update(
                current_value,
                attributes_to_update,
                rules_excluded=True,
            )
        else:
            raise ToeLinesNotFound() from exc


toe_ports_add = retry_on_exceptions(exceptions=(UnavailabilityError,), sleep_seconds=5)(
    toe_ports_domain.add,
)
toe_ports_update = retry_on_exceptions(exceptions=(UnavailabilityError,), sleep_seconds=5)(
    toe_ports_domain.update,
)


@retry_on_exceptions(
    exceptions=(ToePortAlreadyUpdated,),
)
@is_observable(TRACER)
async def _process_toe_port(
    loaders: Dataloaders,
    target_group_name: str,
    target_root_id: str,
    toe_port: ToePort,
    modified_by: str,
) -> None:
    if toe_port.state.seen_at is None:
        return
    attributes_to_add = ToePortAttributesToAdd(
        attacked_at=toe_port.state.attacked_at,
        attacked_by=toe_port.state.attacked_by,
        be_present=False,
        first_attack_at=toe_port.state.first_attack_at,
        has_vulnerabilities=toe_port.state.has_vulnerabilities,
        seen_first_time_by=toe_port.state.seen_first_time_by,
        seen_at=toe_port.state.seen_at,
    )
    try:
        await toe_ports_add(
            loaders=loaders,
            group_name=target_group_name,
            address=toe_port.address,
            port=toe_port.port,
            root_id=target_root_id,
            attributes=attributes_to_add,
            modified_by=modified_by,
            is_moving_toe_port=True,
        )
    except RepeatedToePort as ex:
        current_value = await loaders.toe_port.load(
            ToePortRequest(
                address=toe_port.address,
                port=toe_port.port,
                group_name=toe_port.group_name,
                root_id=toe_port.root_id,
            ),
        )
        if current_value is None:
            raise ToePortNotFound() from ex

        attributes_to_update = ToePortAttributesToUpdate(
            attacked_at=toe_port.state.attacked_at,
            attacked_by=toe_port.state.attacked_by,
            first_attack_at=toe_port.state.first_attack_at,
            has_vulnerabilities=toe_port.state.has_vulnerabilities,
            seen_at=toe_port.state.seen_at,
            seen_first_time_by=toe_port.state.seen_first_time_by,
        )
        await toe_ports_update(
            current_value,
            attributes_to_update,
            modified_by,
            is_moving_toe_port=True,
        )


async def _add_event_evidence(
    *,
    event_id: str,
    evidence_id: str,
    loaders: Dataloaders,
    source_event: Event,
    target_group_name: str,
    evidence: EventEvidence | None,
) -> None:
    if not evidence:
        return
    loaders.event.clear(EventRequest(event_id=event_id, group_name=target_group_name))
    with suppress(Exception):
        file_path = await download_evidence_file(
            source_event.group_name,
            source_event.id,
            evidence.file_name,
        )
        async with aiofiles.open(file_path, "rb") as new_file:
            file_contents = await new_file.read()
            await events_domain.update_evidence(
                loaders=loaders,
                event_id=event_id,
                evidence_id=EventEvidenceId(evidence_id),
                file=UploadFile(filename=str(new_file.name), file=BytesIO(file_contents)),
                group_name=target_group_name,
                update_date=datetime_utils.get_utc_now(),
            )


@retry_on_exceptions(
    exceptions=(EventAlreadyCreated,),
)
async def _process_event(
    *,
    event: Event,
    item_subject: str,
    loaders: Dataloaders,
    source_group_name: str,
    target_group_name: str,
    target_root_id: str,
) -> tuple[str, str]:
    historic = tuple(
        await loaders.event_historic_state.load(
            EventRequest(event_id=event.id, group_name=event.group_name),
        ),
    )
    target_event_id = str(uuid.uuid4())
    await events_model.add(
        event=event._replace(
            id=target_event_id,
            group_name=target_group_name,
            root_id=target_root_id,
            evidences=EventEvidences(),
        ),
    )
    await events_model.update_historic_state(
        event_id=target_event_id,
        group_name=target_group_name,
        historic_state=historic,
    )
    await events_model.update_new_historic_state(
        event_id=target_event_id,
        group_name=target_group_name,
        historic_state=historic,
    )
    event_comments = await loaders.event_comments.load(
        EventCommentsRequest(event_id=event.id, group_name=source_group_name),
    )
    await collect(
        tuple(
            event_comments_model.add(
                event_comment=comment._replace(
                    event_id=target_event_id,
                    group_name=target_group_name,
                ),
            )
            for comment in event_comments
        ),
    )
    await collect(
        [
            _add_event_evidence(
                event_id=target_event_id,
                evidence_id=evidence_id,
                loaders=loaders,
                source_event=event,
                target_group_name=target_group_name,
                evidence=getattr(event.evidences, evidence_name),
            )
            for evidence_id, evidence_name in EVENT_EVIDENCE_NAMES.items()
        ],
        workers=1,
    )

    await integrates.findings.domain.events.solve_event_and_process_vulnerabilities(
        loaders=loaders,
        event_id=event.id,
        group_name=source_group_name,
        hacker_email=item_subject,
        reason=EventSolutionReason.MOVED_TO_ANOTHER_GROUP,
        other=None,
        user_info={
            "user_email": item_subject,
            "first_name": "Machine",
            "last_name": "Services",
        },
    )

    return event.id, target_event_id


@is_observable(TRACER)
async def _process_unsolved_events(
    *,
    item_subject: str,
    loaders: Dataloaders,
    source_group_name: str,
    source_root_id: str,
    target_group_name: str,
    target_root_id: str,
) -> dict[str, str]:
    source_group_events = await loaders.group_events.load(
        GroupEventsRequest(group_name=source_group_name, is_solved=False),
    )
    source_root_events = tuple(
        event for event in source_group_events if event.root_id == source_root_id
    )
    event_ids = await collect(
        tuple(
            _process_event(
                event=event,
                item_subject=item_subject,
                loaders=loaders,
                source_group_name=source_group_name,
                target_group_name=target_group_name,
                target_root_id=target_root_id,
            )
            for event in source_root_events
        ),
    )
    return dict(event_ids)


async def _process_environment_url(
    *,
    environment_url: RootEnvironmentUrl,
    loaders: Dataloaders,
    source_group_name: str,
    source_root_id: str,
    target_group_name: str,
    target_root_id: str,
) -> None:
    await roots_model.add_root_environment_url(
        group_name=target_group_name,
        root_id=target_root_id,
        url=environment_url._replace(
            group_name=target_group_name,
            root_id=target_root_id,
        ),
    )
    await roots_model.update_root_environment_url_state(
        current_value=environment_url.state,
        root_id=source_root_id,
        url_id=environment_url.id,
        group_name=source_group_name,
        state=environment_url.state._replace(
            include=False,
        ),
    )
    environment_secrets = await loaders.environment_secrets.load(
        RootEnvironmentSecretsRequest(url_id=environment_url.id, group_name=source_group_name),
    )
    await collect(
        tuple(
            roots_model.add_root_environment_secret(
                group_name=target_group_name,
                resource_id=environment_url.id,
                secret=environment_secret,
            )
            for environment_secret in environment_secrets
        ),
    )


@is_observable(TRACER)
async def _process_environment_urls(
    *,
    loaders: Dataloaders,
    source_group_name: str,
    source_root_id: str,
    target_group_name: str,
    target_root_id: str,
) -> None:
    source_environment_urls = await loaders.root_environment_urls.load(
        RootEnvironmentUrlsRequest(root_id=source_root_id, group_name=source_group_name),
    )
    await collect(
        tuple(
            _process_environment_url(
                environment_url=environment_url,
                loaders=loaders,
                source_root_id=source_root_id,
                source_group_name=source_group_name,
                target_root_id=target_root_id,
                target_group_name=target_group_name,
            )
            for environment_url in source_environment_urls
        ),
    )


async def _process_docker_image(
    *,
    loaders: Dataloaders,
    docker_image: RootDockerImage,
    source_root_id: str,
    source_group_name: str,
    target_root_id: str,
    target_group_name: str,
) -> None:
    try:
        root_docker_image = await roots_domain.add_root_docker_image(
            uri=docker_image.uri,
            user_email=docker_image.created_by,
            loaders=loaders,
            group_name=target_group_name,
            root_id=target_root_id,
            credential_id=docker_image.state.credential_id,
        )
        if root_docker_image:
            await queue_sbom_image_job(
                loaders=loaders,
                group_name=target_group_name,
                modified_by=docker_image.created_by,
                uris_with_credentials=[
                    (root_docker_image.uri, root_docker_image.state.credential_id)
                ],
                job_type=SbomJobType.SCHEDULER,
                sbom_format=SbomFormat.FLUID_JSON,
            )
    except RootDockerfileNotExists:
        return

    await roots_model.update_root_docker_image_state(
        current_value=docker_image.state,
        root_id=source_root_id,
        uri=docker_image.uri,
        group_name=source_group_name,
        state=docker_image.state._replace(
            include=False,
        ),
    )


@is_observable(TRACER)
async def _process_docker_images(
    *,
    loaders: Dataloaders,
    source_group_name: str,
    source_root_id: str,
    target_group_name: str,
    target_root_id: str,
) -> None:
    source_docker_images = await loaders.root_docker_images.load(
        RootDockerImagesRequest(root_id=source_root_id, group_name=source_group_name)
    )
    await collect(
        tuple(
            _process_docker_image(
                loaders=loaders,
                docker_image=docker_image,
                source_root_id=source_root_id,
                source_group_name=source_group_name,
                target_root_id=target_root_id,
                target_group_name=target_group_name,
            )
            for docker_image in source_docker_images
        ),
    )


async def get_recipients(
    loaders: Dataloaders,
    email_to: list[str],
    source_group_name: str,
    target_group_name: str,
) -> list[str]:
    stakeholder = await get_stakeholder(loaders, email_to[0])
    if Notification.ROOT_UPDATE not in stakeholder.state.notifications_preferences.email:
        email_to = []
    source_group_emails = await mailer_utils.get_group_emails_by_notification(
        loaders=loaders,
        group_name=source_group_name,
        notification="root_moved",
    )
    email_to.extend(source_group_emails)
    target_group_emails = await mailer_utils.get_group_emails_by_notification(
        loaders=loaders,
        group_name=target_group_name,
        notification="root_moved",
    )
    email_to.extend(target_group_emails)

    return email_to


@is_observable(TRACER)
async def move_root(*, item: BatchProcessing) -> None:
    target_group_name = item.additional_info["target_group_name"]
    target_root_id = item.additional_info["target_root_id"]
    source_group_name = item.additional_info["source_group_name"]
    source_root_id = item.additional_info["source_root_id"]
    loaders: Dataloaders = get_new_context()

    LOGGER.info("Updating Events")
    event_ids = await _process_unsolved_events(
        item_subject=item.subject,
        loaders=loaders,
        source_group_name=source_group_name,
        source_root_id=source_root_id,
        target_group_name=target_group_name,
        target_root_id=target_root_id,
    )
    LOGGER.info("Moving root", extra={"extra": item.additional_info})
    loaders = get_new_context()
    root = await roots_utils.get_root(loaders, source_root_id, source_group_name)
    root_vulnerabilities = [
        vuln
        for vuln in filter_vulns_utils.filter_non_deleted(
            await loaders.root_vulnerabilities.load(root.id),
        )
        if vuln.state.reasons is None
        or VulnerabilityStateReason.ROOT_MOVED_TO_ANOTHER_GROUP not in vuln.state.reasons
    ]
    vulns_by_finding = itertools.groupby(
        sorted(root_vulnerabilities, key=attrgetter("finding_id")),
        key=attrgetter("finding_id"),
    )
    LOGGER.info(
        "Root content",
        extra={
            "extra": {
                "vulnerabilities": len(root_vulnerabilities),
            },
        },
    )
    await collect(
        tuple(
            _process_finding(
                loaders=loaders,
                source_group_name=source_group_name,
                target_group_name=target_group_name,
                target_root_id=target_root_id,
                source_finding_id=source_finding_id,
                vulns=tuple(vulns),
                item_subject=item.subject,
                event_ids=event_ids,
            )
            for source_finding_id, vulns in vulns_by_finding
        ),
        workers=10,
    )
    LOGGER.info("Moving completed")
    target_root = await roots_utils.get_root(loaders, target_root_id, target_group_name)
    if isinstance(root, (GitRoot, URLRoot)):
        LOGGER.info("Updating ToE inputs")
        group_toe_inputs = await loaders.group_toe_inputs.load_nodes(
            GroupToeInputsRequest(group_name=source_group_name),
        )
        root_toe_inputs = tuple(
            toe_input for toe_input in group_toe_inputs if toe_input.root_id == source_root_id
        )
        await collect(
            tuple(
                _process_toe_input(
                    loaders,
                    target_group_name,
                    target_root_id,
                    toe_input,
                )
                for toe_input in root_toe_inputs
            ),
        )
        await batch_dal.put_action(
            action=Action.REFRESH_TOE_INPUTS,
            entity=target_group_name,
            subject=item.subject,
            additional_info={"root_ids": [target_root.id]},
            queue=IntegratesBatchQueue.SMALL,
        )
    if isinstance(root, GitRoot):
        LOGGER.info("Updating ToE lines")
        repo_toe_lines = await loaders.root_toe_lines.load_nodes(
            RootToeLinesRequest(group_name=source_group_name, root_id=source_root_id),
        )
        await collect(
            tuple(
                _process_toe_lines(
                    loaders,
                    target_group_name,
                    target_root_id,
                    toe_lines,
                )
                for toe_lines in repo_toe_lines
            ),
        )
        await batch_dal.put_action(
            action=Action.REFRESH_TOE_LINES,
            attempt_duration_seconds=7200,
            entity=target_group_name,
            subject=item.subject,
            additional_info={"root_ids": [target_root.id]},
            queue=IntegratesBatchQueue.SMALL,
        )
        LOGGER.info("Updating Environment Urls")
        await _process_environment_urls(
            loaders=loaders,
            source_group_name=source_group_name,
            source_root_id=source_root_id,
            target_group_name=target_group_name,
            target_root_id=target_root_id,
        )
        LOGGER.info("Updating Docker images")
        await _process_docker_images(
            loaders=loaders,
            source_group_name=source_group_name,
            source_root_id=source_root_id,
            target_group_name=target_group_name,
            target_root_id=target_root_id,
        )
        LOGGER.info("Deactivating old packages")
        await roots_domain.deactivate_root_toe_packages(loaders, source_root_id, source_group_name)

    if isinstance(root, IPRoot):
        LOGGER.info("Updating ToE ports")
        await collect(
            tuple(
                _process_toe_port(
                    loaders,
                    target_group_name,
                    target_root_id,
                    toe_port,
                    item.subject,
                )
                for toe_port in await loaders.root_toe_ports.load_nodes(
                    RootToePortsRequest(group_name=source_group_name, root_id=source_root_id),
                )
            ),
        )
        await batch_dal.put_action(
            action=Action.REFRESH_TOE_PORTS,
            entity=target_group_name,
            subject=item.subject,
            additional_info={"root_ids": [target_root.id]},
            queue=IntegratesBatchQueue.SMALL,
        )
    LOGGER.info(
        "Notifying stakeholders",
        extra={
            "extra": {
                "subject": item.subject,
            },
        },
    )
    await send_mails_async(
        loaders=get_new_context(),
        email_to=await get_recipients(
            loaders,
            [item.subject],
            source_group_name,
            target_group_name,
        ),
        context={
            "group": source_group_name,
            "nickname": root.state.nickname,
            "target": target_group_name,
        },
        subject=(f"Root moved from [{source_group_name}] to [{target_group_name}]"),
        template_name="root_moved",
    )
    LOGGER.info("Task completed successfully.")
