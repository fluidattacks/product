import logging

from integrates.batch.types import BatchProcessing
from integrates.jira_security_integration import (
    utils,
)

LOGGER = logging.getLogger(__name__)


async def bulk_jira_vulnerabilities(*, item: BatchProcessing) -> None:
    jira_jwt = item.additional_info["jira_jwt"]
    base_url = item.additional_info["base_url"]

    LOGGER.info(
        "Bulk Jira vulnerabilities",
        extra={
            "extra": {
                "jira_jwt_iss": utils.get_iss_from_jwt(jira_jwt),
                "base_url": base_url,
            },
        },
    )

    await utils.bulk_vulnerabilities(jira_jwt=jira_jwt, base_url=base_url)
