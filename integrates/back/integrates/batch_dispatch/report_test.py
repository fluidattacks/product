import datetime

from integrates.batch.enums import Action, IntegratesBatchQueue
from integrates.batch.types import BatchProcessing
from integrates.batch_dispatch.report import report
from integrates.mailer import (
    groups as groups_mail,
)
from integrates.reports.enums import ReportType
from integrates.roots.domain import format_environment_id
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    GitRootFaker,
    GitRootStateFaker,
    GroupAccessFaker,
    GroupAccessStateFaker,
    GroupFaker,
    OrganizationFaker,
    StakeholderFaker,
    ToeInputFaker,
    ToeInputStateFaker,
    ToeLinesFaker,
    ToeLinesStateFaker,
)
from integrates.testing.mocks import Mock, mocks
from integrates.testing.utils import parametrize

GROUP_NAME = "group1"
ORG_ID = "org1"
ROOT_ID = "root_id"
URL = "https://nice-env.net"
URL_ID = format_environment_id(URL)
EMAIL = "group_manager@gmail.com"


@parametrize(
    args=[
        "item",
        "expected_filename_content",
        "expected_filesize",
    ],
    cases=[
        [
            BatchProcessing(
                key="test_key",
                action_name=Action.REPORT,
                entity=GROUP_NAME,
                subject=EMAIL,
                time=datetime.datetime.now(),
                additional_info={"report_type": ReportType.TOE_LINES, "notitication_id": "test_id"},
                queue=IntegratesBatchQueue.SMALL,
            ),
            "toe-lines-group1",
            519,
        ],
        [
            BatchProcessing(
                key="test_key",
                action_name=Action.REPORT,
                entity=GROUP_NAME,
                subject=EMAIL,
                time=datetime.datetime.now(),
                additional_info={"report_type": ReportType.TOE_INPUTS},
                queue=IntegratesBatchQueue.SMALL,
            ),
            "toe-inputs-group1",
            235,
        ],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(
                    id=ORG_ID,
                ),
            ],
            groups=[
                GroupFaker(name=GROUP_NAME, organization_id=ORG_ID),
            ],
            stakeholders=[
                StakeholderFaker(email=EMAIL, role="group_manager"),
            ],
            group_access=[
                GroupAccessFaker(
                    email=EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="group_manager"),
                ),
            ],
            roots=[
                GitRootFaker(
                    group_name=GROUP_NAME,
                    id=ROOT_ID,
                    state=GitRootStateFaker(nickname="testroot"),
                ),
            ],
            toe_inputs=[
                ToeInputFaker(
                    environment_id="",
                    root_id=ROOT_ID,
                    component=URL,
                    group_name=GROUP_NAME,
                    entry_point="user",
                    state=ToeInputStateFaker(),
                ),
                ToeInputFaker(
                    environment_id="",
                    root_id=ROOT_ID,
                    group_name=GROUP_NAME,
                    component="http://nice-env.net",
                    entry_point="user",
                    state=ToeInputStateFaker(attacked_by="machine@fluidattacks.com"),
                ),
                ToeInputFaker(
                    environment_id="",
                    root_id=ROOT_ID,
                    group_name=GROUP_NAME,
                    component="http://nice-env.net",
                    entry_point="user3",
                    state=ToeInputStateFaker(),
                ),
            ],
            toe_lines=[
                ToeLinesFaker(
                    filename="path/to/file1.ext",
                    group_name=GROUP_NAME,
                    root_id=ROOT_ID,
                    state=ToeLinesStateFaker(loc=4324),
                ),
                ToeLinesFaker(
                    filename="path/to/file22.ext",
                    group_name=GROUP_NAME,
                    root_id=ROOT_ID,
                    state=ToeLinesStateFaker(loc=99),
                ),
                ToeLinesFaker(
                    filename="test/1",
                    group_name=GROUP_NAME,
                    root_id=ROOT_ID,
                    state=ToeLinesStateFaker(loc=100),
                ),
            ],
        ),
    ),
    others=[Mock(groups_mail, "send_mail_group_report", "async", None)],
)
async def test_report(
    item: BatchProcessing, expected_filename_content: str, expected_filesize: int
) -> None:
    filename, filesize = await report(item=item)
    assert expected_filename_content in filename
    assert filesize == expected_filesize
