from datetime import datetime

from integrates.batch.enums import Action, IntegratesBatchQueue
from integrates.batch.types import BatchProcessing
from integrates.batch_dispatch.move_environment import move_environment
from integrates.custom_utils import datetime as datetime_utils
from integrates.dataloaders import Dataloaders, get_new_context
from integrates.db_model.roots.types import RootEnvironmentUrlRequest
from integrates.db_model.toe_inputs.types import RootToeInputsRequest
from integrates.db_model.vulnerabilities.enums import (
    VulnerabilityStateStatus,
    VulnerabilityType,
)
from integrates.db_model.vulnerabilities.types import (
    GroupVulnerabilitiesRequest,
    VulnerabilityRequest,
)
from integrates.roots.domain import format_environment_id
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    FindingFaker,
    GitRootFaker,
    GroupFaker,
    OrganizationAccessFaker,
    OrganizationAccessStateFaker,
    OrganizationFaker,
    RootEnvironmentUrlFaker,
    RootEnvironmentUrlStateFaker,
    StakeholderFaker,
    ToeInputFaker,
    ToeInputStateFaker,
    VulnerabilityFaker,
    VulnerabilityStateFaker,
)
from integrates.testing.mocks import mocks


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            groups=[
                GroupFaker(name="west", organization_id="org1"),
                GroupFaker(name="east", organization_id="org1"),
            ],
            findings=[
                FindingFaker(group_name="west", id="find1"),
                FindingFaker(
                    group_name="west", id="find2", title="002. Asymmetric denial of service"
                ),
                FindingFaker(group_name="west", id="find3", title="004. Remote command execution"),
            ],
            roots=[
                GitRootFaker(group_name="west", id="root1"),
                GitRootFaker(group_name="east", id="root2"),
            ],
            vulnerabilities=[
                VulnerabilityFaker(
                    group_name="west",
                    root_id="root1",
                    finding_id="find1",
                    id="vuln1",
                    created_by="hacker@fluidattacks.com",
                    type=VulnerabilityType.INPUTS,
                    state=VulnerabilityStateFaker(
                        status=VulnerabilityStateStatus.VULNERABLE, where="https://test.com/"
                    ),
                    treatment=None,
                ),
                VulnerabilityFaker(
                    group_name="west",
                    root_id="root1",
                    finding_id="find1",
                    id="vuln2",
                    created_by="hacker@fluidattacks.com",
                    type=VulnerabilityType.INPUTS,
                    state=VulnerabilityStateFaker(
                        status=VulnerabilityStateStatus.SAFE, where="https://test.com/"
                    ),
                    treatment=None,
                ),
                VulnerabilityFaker(
                    group_name="west",
                    root_id="root1",
                    finding_id="find1",
                    id="vuln3",
                    created_by="hacker@fluidattacks.com",
                    type=VulnerabilityType.INPUTS,
                    state=VulnerabilityStateFaker(
                        status=VulnerabilityStateStatus.SUBMITTED, where="https://test.com/"
                    ),
                    treatment=None,
                ),
                VulnerabilityFaker(
                    group_name="west",
                    root_id="root1",
                    finding_id="find1",
                    id="vuln4",
                    created_by="hacker@fluidattacks.com",
                    type=VulnerabilityType.INPUTS,
                    state=VulnerabilityStateFaker(
                        status=VulnerabilityStateStatus.REJECTED, where="https://test.com/"
                    ),
                    treatment=None,
                ),
                VulnerabilityFaker(
                    group_name="west",
                    root_id="root1",
                    finding_id="find3",
                    id="vuln6",
                    created_by="hacker@fluidattacks.com",
                    type=VulnerabilityType.INPUTS,
                    state=VulnerabilityStateFaker(
                        status=VulnerabilityStateStatus.DELETED, where="https://test.com/"
                    ),
                    treatment=None,
                ),
            ],
            stakeholders=[
                StakeholderFaker(email="jdoe@fluidattacks.com", role="customer_manager"),
            ],
            organization_access=[
                OrganizationAccessFaker(
                    email="jdoe@fluidattacks.com",
                    organization_id="org1",
                    state=OrganizationAccessStateFaker(role="customer_manager", has_access=True),
                ),
            ],
            toe_inputs=[
                ToeInputFaker(
                    group_name="west",
                    root_id="root1",
                    component="https://test.com/",
                    entry_point="user",
                    state=ToeInputStateFaker(has_vulnerabilities=True),
                ),
            ],
            root_environment_urls=[
                RootEnvironmentUrlFaker(
                    url="https://test.com/",
                    group_name="west",
                    root_id="root1",
                    state=RootEnvironmentUrlStateFaker(modified_by="hacker@fluidattacks.com"),
                ),
            ],
        ),
    )
)
async def test_move_environment_between_different_groups() -> None:
    # Arrange
    url = "https://test.com/"
    source = ("west", "root1")
    target_group_name = "east"
    target_root_id = "root2"
    email = "jdoe@fluidattacks.com"
    entry_point = "user"
    hacker_email = "hacker@fluidattacks.com"
    source_env_id = format_environment_id("https://test.com/")

    # Act
    await move_environment(
        item=BatchProcessing(
            action_name=Action.MOVE_ENVIRONMENT,
            entity=source[0],
            subject=email,
            additional_info={
                "source_env_id": source_env_id,
                "source_group_name": source[0],
                "source_root_id": source[1],
                "target_group_name": target_group_name,
                "target_root_id": target_root_id,
            },
            queue=IntegratesBatchQueue.SMALL,
            key="key1",
            batch_job_id="job1",
            time=datetime.now(),
        ),
    )

    # Assert
    loaders: Dataloaders = get_new_context()
    result = await loaders.environment_url.load(
        RootEnvironmentUrlRequest(
            group_name=target_group_name,
            root_id=target_root_id,
            url_id=source_env_id,
        ),
    )
    assert result.url == url
    assert result.state.include

    result_inputs = await loaders.root_toe_inputs.load(
        RootToeInputsRequest(
            group_name=target_group_name,
            root_id=target_root_id,
            be_present=True,
        ),
    )
    assert len(result_inputs.edges) == 1
    assert result_inputs.edges[0].node.component == url
    assert result_inputs.edges[0].node.entry_point == entry_point

    result_vulns = await loaders.group_vulnerabilities.load(
        GroupVulnerabilitiesRequest(group_name=target_group_name),
    )
    assert len(result_vulns.edges) == 4
    assert result_vulns.edges[0].node.created_by == hacker_email

    for vuln in result_vulns.edges:
        historic = await loaders.vulnerability_historic_treatment.load(
            VulnerabilityRequest(finding_id=vuln.node.finding_id, vulnerability_id=vuln.node.id),
        )
        assert len(historic) == 1
        assert historic[0].justification == "Environment was moved to the current root"
        assert historic[0].modified_by == email

    findings = await loaders.group_findings.load(target_group_name)
    assert len(findings) == 1


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(id="org1"),
            ],
            groups=[
                GroupFaker(name="west", organization_id="org1"),
                GroupFaker(name="east", organization_id="org1"),
            ],
            findings=[FindingFaker(group_name="west", id="find1")],
            roots=[
                GitRootFaker(group_name="west", id="root1"),
                GitRootFaker(group_name="east", id="root2"),
            ],
            vulnerabilities=[
                VulnerabilityFaker(
                    group_name="west",
                    root_id="root1",
                    finding_id="find1",
                    id="vuln1",
                    created_by="hacker@fluidattacks.com",
                    type=VulnerabilityType.INPUTS,
                    state=VulnerabilityStateFaker(
                        status=VulnerabilityStateStatus.VULNERABLE, where="https://test.com/"
                    ),
                    treatment=None,
                ),
                VulnerabilityFaker(
                    group_name="west",
                    root_id="root1",
                    finding_id="find1",
                    id="vuln2",
                    created_by="hacker@fluidattacks.com",
                    type=VulnerabilityType.INPUTS,
                    state=VulnerabilityStateFaker(
                        status=VulnerabilityStateStatus.VULNERABLE, where="https://example.com/"
                    ),
                    treatment=None,
                ),
            ],
            stakeholders=[StakeholderFaker(email="jdoe@fluidattacks.com", role="customer_manager")],
            organization_access=[
                OrganizationAccessFaker(
                    email="jdoe@fluidattacks.com",
                    organization_id="org1",
                    state=OrganizationAccessStateFaker(role="customer_manager", has_access=True),
                ),
            ],
            toe_inputs=[
                ToeInputFaker(
                    group_name="west",
                    root_id="root1",
                    component="https://test.com/",
                    entry_point="user",
                    state=ToeInputStateFaker(has_vulnerabilities=True),
                ),
            ],
            root_environment_urls=[
                RootEnvironmentUrlFaker(
                    url="https://test.com/",
                    group_name="west",
                    root_id="root1",
                    state=RootEnvironmentUrlStateFaker(modified_by="hacker@fluidattacks.com"),
                ),
            ],
        ),
    )
)
async def test_only_root_vulns_are_moved() -> None:
    # Arrange
    url = "https://test.com/"
    source = ("west", "root1")
    target_group_name = "east"
    target_root_id = "root2"
    email = "jdoe@fluidattacks.com"
    entry_point = "user"
    hacker_email = "hacker@fluidattacks.com"
    source_env_id = format_environment_id("https://test.com/")

    # Act
    await move_environment(
        item=BatchProcessing(
            action_name=Action.MOVE_ENVIRONMENT,
            entity=source[0],
            subject=email,
            additional_info={
                "source_env_id": source_env_id,
                "source_group_name": source[0],
                "source_root_id": source[1],
                "target_group_name": target_group_name,
                "target_root_id": target_root_id,
            },
            queue=IntegratesBatchQueue.SMALL,
            key="key1",
            batch_job_id="job1",
            time=datetime.now(),
        ),
    )

    # Assert
    loaders: Dataloaders = get_new_context()
    result = await loaders.environment_url.load(
        RootEnvironmentUrlRequest(
            group_name=target_group_name,
            root_id=target_root_id,
            url_id=source_env_id,
        ),
    )
    assert result.url == url
    assert result.state.include

    result_inputs = await loaders.root_toe_inputs.load(
        RootToeInputsRequest(
            group_name=target_group_name,
            root_id=target_root_id,
            be_present=True,
        ),
    )
    assert len(result_inputs.edges) == 1
    assert result_inputs.edges[0].node.component == url
    assert result_inputs.edges[0].node.entry_point == entry_point

    result_vulns = await loaders.group_vulnerabilities.load(
        GroupVulnerabilitiesRequest(group_name=target_group_name),
    )
    assert len(result_vulns.edges) == 1
    assert result_vulns.edges[0].node.created_by == hacker_email

    for vuln in result_vulns.edges:
        historic = await loaders.vulnerability_historic_treatment.load(
            VulnerabilityRequest(finding_id=vuln.node.finding_id, vulnerability_id=vuln.node.id),
        )
        assert len(historic) == 1
        assert historic[0].justification == "Environment was moved to the current root"
        assert historic[0].modified_by == email


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            groups=[
                GroupFaker(name="west", organization_id="org1"),
                GroupFaker(name="east", organization_id="org1"),
            ],
            findings=[FindingFaker(group_name="west", id="find1")],
            roots=[
                GitRootFaker(group_name="west", id="root1"),
                GitRootFaker(group_name="east", id="root2"),
            ],
            vulnerabilities=[
                VulnerabilityFaker(
                    group_name="west",
                    root_id="root1",
                    finding_id="find1",
                    id="vuln1",
                    created_by="hacker@fluidattacks.com",
                    type=VulnerabilityType.INPUTS,
                    state=VulnerabilityStateFaker(
                        status=VulnerabilityStateStatus.VULNERABLE, where="https://test.com/"
                    ),
                ),
            ],
            stakeholders=[
                StakeholderFaker(email="jdoe@fluidattacks.com", role="customer_manager"),
            ],
            organization_access=[
                OrganizationAccessFaker(
                    email="jdoe@fluidattacks.com",
                    organization_id="org1",
                    state=OrganizationAccessStateFaker(role="customer_manager", has_access=True),
                ),
            ],
            toe_inputs=[
                ToeInputFaker(
                    group_name="west",
                    root_id="root1",
                    component="https://test.com/",
                    entry_point="user",
                    state=ToeInputStateFaker(has_vulnerabilities=True),
                ),
            ],
            root_environment_urls=[
                RootEnvironmentUrlFaker(
                    url="https://test.com/",
                    group_name="west",
                    root_id="root1",
                    state=RootEnvironmentUrlStateFaker(modified_by="hacker@fluidattacks.com"),
                ),
            ],
        ),
    )
)
async def test_revert_move_environment() -> None:
    # Arrange
    url = "https://test.com/"
    source = ("west", "root1")
    target_group_name = "east"
    target_root_id = "root2"
    email = "jdoe@fluidattacks.com"
    hacker_email = "hacker@fluidattacks.com"
    source_env_id = format_environment_id("https://test.com/")

    # Act
    await move_environment(
        item=BatchProcessing(
            action_name=Action.MOVE_ENVIRONMENT,
            entity=source[0],
            subject=email,
            additional_info={
                "source_env_id": source_env_id,
                "source_group_name": source[0],
                "source_root_id": source[1],
                "target_group_name": target_group_name,
                "target_root_id": target_root_id,
            },
            queue=IntegratesBatchQueue.SMALL,
            key="key1",
            batch_job_id="job1",
            time=datetime_utils.get_utc_now(),
        ),
    )
    await move_environment(
        item=BatchProcessing(
            action_name=Action.MOVE_ENVIRONMENT,
            entity=target_group_name,
            subject=email,
            additional_info={
                "source_env_id": source_env_id,
                "source_group_name": target_group_name,
                "source_root_id": target_root_id,
                "target_group_name": source[0],
                "target_root_id": source[1],
            },
            queue=IntegratesBatchQueue.SMALL,
            key="key2",
            batch_job_id="job2",
            time=datetime_utils.get_utc_now(),
        ),
    )

    # Assert
    loaders: Dataloaders = get_new_context()
    result = await loaders.environment_url.load(
        RootEnvironmentUrlRequest(
            group_name=target_group_name,
            root_id=target_root_id,
            url_id=source_env_id,
        ),
    )
    assert result.url == url
    assert result.state.include

    result_vulns = await loaders.group_vulnerabilities.load(
        GroupVulnerabilitiesRequest(
            group_name=source[0], state_status=VulnerabilityStateStatus.VULNERABLE
        ),
    )
    assert len(result_vulns.edges) == 1
    assert result_vulns.edges[0].node.created_by == hacker_email

    for vuln in result_vulns.edges:
        historic = await loaders.vulnerability_historic_treatment.load(
            VulnerabilityRequest(finding_id=vuln.node.finding_id, vulnerability_id=vuln.node.id),
        )
        assert len(historic) == 3
        assert historic[1].justification == "Environment was moved to the current root"
        assert historic[1].modified_by == email
        assert historic[2].justification == "Environment was moved to the current root"
        assert historic[2].modified_by == email
