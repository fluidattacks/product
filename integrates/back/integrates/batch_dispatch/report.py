import logging
import logging.config
import os
from datetime import (
    UTC,
    datetime,
    timedelta,
)
from decimal import (
    Decimal,
)

from integrates.batch import (
    dal as batch_dal,
)
from integrates.batch.types import (
    BatchProcessing,
)
from integrates.class_types.types import (
    Item,
)
from integrates.custom_exceptions import (
    ErrorUploadingFileS3,
    UnavailabilityError,
)
from integrates.custom_utils.reports import (
    upload_report,
)
from integrates.dataloaders import (
    get_new_context,
)
from integrates.db_model.enums import (
    TreatmentStatus,
)
from integrates.db_model.notifications.types import ReportStatus
from integrates.db_model.vulnerabilities.enums import (
    VulnerabilityStateStatus,
    VulnerabilityVerificationStatus,
)
from integrates.decorators import (
    retry_on_exceptions,
)
from integrates.notifications.reports import (
    update_report_notification,
)
from integrates.reports import (
    domain as reports_domain,
)
from integrates.reports.enums import (
    ReportType,
)
from integrates.reports.it_report import (
    Filters,
)
from integrates.settings import (
    LOGGING,
)
from integrates.tickets import (
    domain as tickets_domain,
)

logging.config.dictConfig(LOGGING)

# Constants
LOGGER = logging.getLogger(__name__)
TRANSACTIONS_LOGGER: logging.Logger = logging.getLogger("transactional")

upload_report_file = retry_on_exceptions(
    exceptions=(UnavailabilityError,),
    max_attempts=4,
    sleep_seconds=1,
)(upload_report)


async def get_report(
    *,
    item: BatchProcessing,
    report_type: ReportType,
    filters: Filters,
) -> tuple[str, int]:
    report_file_name: str | None = None
    try:
        report_file_name = await reports_domain.get_group_report_url(
            report_type=report_type,
            group_name=item.entity,
            user_email=item.subject,
            filters=filters,
        )
        if report_file_name is not None:
            uploaded_file_name = await upload_report_file(report_file_name)
            uploaded_file_size = os.path.getsize(report_file_name)
    except ErrorUploadingFileS3 as exc:
        LOGGER.exception(
            exc,
            extra={
                "extra": {
                    "group_name": item.entity,
                    "user_email": item.subject,
                },
            },
        )
        return "", 0
    else:
        return uploaded_file_name, uploaded_file_size
    finally:
        if report_file_name and os.path.exists(report_file_name):
            os.unlink(report_file_name)


async def send_password_protected_report(
    item: BatchProcessing,
    report_type: ReportType,
) -> None:
    loaders = get_new_context()
    translations: dict[ReportType, str] = {
        ReportType.CERT: "Certificate",
        ReportType.DATA: "Group Data",
        ReportType.PDF: "Executive",
        ReportType.XLS: "Technical",
    }
    await tickets_domain.new_password_protected_report(
        loaders=loaders,
        user_email=item.subject,
        group_name=item.entity,
        file_type=translations[report_type],
    )


async def send_report(
    *,
    item: BatchProcessing,
    report_type: ReportType,
    report_filename: str,
    report_filesize: int,
    states: set[VulnerabilityStateStatus],
    treatments: set[TreatmentStatus],
    verifications: set[VulnerabilityVerificationStatus],
    finding_title: str,
    age: int | None,
    min_severity: Decimal | None,
    max_severity: Decimal | None,
    last_report: int | None,
    date_info: dict[str, datetime | None],
    location: str,
) -> None:
    if not await batch_dal.get_action(action_key=item.key):
        return

    await update_report_notification(
        expiration_time=(datetime.now() + timedelta(days=7)),
        notification_id=item.additional_info["notification_id"],
        s3_file_path=f"reports/{report_filename}",
        size=report_filesize,
        status=ReportStatus.READY,
        user_email=item.subject,
    )
    TRANSACTIONS_LOGGER.info(
        "Send report requested",
        extra={
            "report_type": report_type,
            "user_email": item.subject,
            "group": item.entity,
            **get_filters_info(
                report_type=report_type,
                treatments=treatments,
                states=states,
                verifications=verifications,
                finding_title=finding_title,
                age=age,
                min_severity=min_severity,
                max_severity=max_severity,
                last_report=last_report,
                date_info=date_info,
                location=location,
            ),
        },
    )

    await send_password_protected_report(item, report_type)


def get_filters_info(
    *,
    report_type: ReportType,
    states: set[VulnerabilityStateStatus],
    treatments: set[TreatmentStatus],
    verifications: set[VulnerabilityVerificationStatus],
    finding_title: str,
    age: int | None,
    min_severity: Decimal | None,
    max_severity: Decimal | None,
    last_report: int | None,
    date_info: dict[str, datetime | None],
    location: str,
) -> dict:
    def _set_field(filters_message: dict, key: str, value: object | None) -> None:
        if value is not None:
            filters_message[key] = value

    if report_type == ReportType.XLS:
        return {}

    closing_date = date_info["closing_date"]
    start_closing_date = date_info["start_closing_date"]
    filter_message: dict = {}
    if closing_date or start_closing_date:
        states = {
            VulnerabilityStateStatus["SAFE"],
        }
        treatments = set(TreatmentStatus)
        if verifications != {
            VulnerabilityVerificationStatus["VERIFIED"],
        }:
            verifications = set()

    if sorted(states) != sorted(
        {
            VulnerabilityStateStatus["SAFE"],
            VulnerabilityStateStatus["VULNERABLE"],
        },
    ):
        _set_field(filter_message, "states", states)
    if sorted(treatments) != sorted(set(TreatmentStatus)):
        _set_field(filter_message, "treatments", treatments)

    _set_field(filter_message, "verifications", verifications)
    _set_field(filter_message, "closing_date", closing_date)
    _set_field(filter_message, "start_closing_date", start_closing_date)
    _set_field(filter_message, "finding_title", finding_title)
    _set_field(filter_message, "age_in_days", age)
    _set_field(filter_message, "min_cvss_4.0_severity_score", min_severity)
    _set_field(filter_message, "max_cvss_4.0_severity_score", max_severity)
    _set_field(filter_message, "last_report_in_days", last_report)
    _set_field(filter_message, "minimum_release_date", date_info["min_release_date"])
    _set_field(filter_message, "max_release_date", date_info["max_release_date"])
    _set_field(filter_message, "location", location)

    return filter_message


async def send_group_toes_report(
    *,
    item: BatchProcessing,
    report_filename: str,
    report_filesize: int,
    report_type: ReportType,
) -> None:
    loaders = get_new_context()
    translations: dict[ReportType, str] = {
        ReportType.TOE_LINES: "Group Toe Lines",
        ReportType.TOE_INPUTS: "Group Toe Inputs",
    }
    if not await batch_dal.get_action(action_key=item.key):
        return

    await update_report_notification(
        expiration_time=(datetime.now() + timedelta(days=7)),
        notification_id=item.additional_info["notification_id"],
        s3_file_path=f"reports/{report_filename}",
        size=report_filesize,
        status=ReportStatus.READY,
        user_email=item.subject,
    )

    TRANSACTIONS_LOGGER.info(
        "Send report requested",
        extra={
            "report_type": report_type,
            "user_email": item.subject,
            "group": item.entity,
        },
    )
    await tickets_domain.new_password_protected_report(
        loaders=loaders,
        user_email=item.subject,
        group_name=item.entity,
        file_type=translations[report_type],
        include_report=False,
    )


async def _get_group_toes_report(
    *, report_type: ReportType, item: BatchProcessing
) -> tuple[str, int]:
    report_file_name: str | None = None
    try:
        if report_type == ReportType.TOE_LINES:
            report_file_name = await reports_domain.get_toe_lines_report(
                group_name=item.entity,
                email=item.subject,
            )
        if report_type == ReportType.TOE_INPUTS:
            report_file_name = await reports_domain.get_toe_inputs_report(
                group_name=item.entity,
                email=item.subject,
            )
        if report_file_name is not None:
            uploaded_file_name = await upload_report_file(report_file_name)
            uploaded_file_size = os.path.getsize(report_file_name)
    except ErrorUploadingFileS3 as exc:
        LOGGER.exception(
            exc,
            extra={
                "extra": {
                    "group_name": item.entity,
                    "user_email": item.subject,
                },
            },
        )
        return "", 0
    else:
        return uploaded_file_name, uploaded_file_size
    finally:
        if report_file_name and os.path.exists(report_file_name):
            os.unlink(report_file_name)


async def get_group_toes_report(
    *,
    item: BatchProcessing,
    report_type: ReportType,
) -> tuple[str, int]:
    TRANSACTIONS_LOGGER.info(
        "Processing report requested",
        extra={
            "report_type": report_type,
            "user_email": item.subject,
            "group": item.entity,
        },
    )
    report_filename, report_filesize = await _get_group_toes_report(
        report_type=report_type,
        item=item,
    )
    if report_filename:
        await send_group_toes_report(
            item=item,
            report_type=report_type,
            report_filename=report_filename,
            report_filesize=report_filesize,
        )
    return report_filename, report_filesize


def _get_date_info_report(additional_info: Item) -> dict[str, datetime | None]:
    closing_date: datetime | None = (
        datetime.fromisoformat(str(additional_info["closing_date"])).astimezone(tz=UTC)
        if additional_info.get("closing_date")
        else None
    )
    start_closing_date: datetime | None = (
        datetime.fromisoformat(str(additional_info["start_closing_date"])).astimezone(
            tz=UTC,
        )
        if additional_info.get("start_closing_date")
        else None
    )
    min_release_date: datetime | None = (
        datetime.fromisoformat(str(additional_info.get("min_release_date"))).astimezone(
            tz=UTC,
        )
        if additional_info.get("min_release_date")
        else None
    )
    max_release_date: datetime | None = (
        datetime.fromisoformat(str(additional_info["max_release_date"])).astimezone(tz=UTC)
        if additional_info.get("max_release_date")
        else None
    )
    return {
        "closing_date": closing_date,
        "start_closing_date": start_closing_date,
        "min_release_date": min_release_date,
        "max_release_date": max_release_date,
    }


async def report(*, item: BatchProcessing) -> tuple[str, int]:
    additional_info = item.additional_info
    report_type = ReportType[additional_info["report_type"]]

    await update_report_notification(
        expiration_time=None,
        notification_id=additional_info.get("notification_id", ""),
        s3_file_path=None,
        size=None,
        status=ReportStatus.PROCESSING,
        user_email=item.subject,
    )

    if report_type in [ReportType.TOE_LINES, ReportType.TOE_INPUTS]:
        return await get_group_toes_report(
            item=item,
            report_type=report_type,
        )

    treatments = {TreatmentStatus[treatment] for treatment in additional_info.get("treatments", [])}
    states = {VulnerabilityStateStatus[state] for state in additional_info.get("states", [])}
    verifications = {
        VulnerabilityVerificationStatus[verification]
        for verification in additional_info.get("verifications", [])
    }
    finding_title: str = additional_info.get("finding_title", "")
    age: int | None = additional_info.get("age", None)
    min_severity: Decimal | None = (
        Decimal(additional_info["min_severity"]).quantize(Decimal("0.1"))
        if additional_info.get("min_severity") is not None
        else None
    )
    max_severity: Decimal | None = (
        Decimal(additional_info["max_severity"]).quantize(Decimal("0.1"))
        if additional_info.get("max_severity") is not None
        else None
    )
    last_report: int | None = additional_info.get("last_report", None)
    location: str = additional_info.get("location", "")
    date_info = _get_date_info_report(additional_info)

    TRANSACTIONS_LOGGER.info(
        "Processing report requested",
        extra={
            "report_type": report_type,
            "user_email": item.subject,
            "group": item.entity,
            **get_filters_info(
                report_type=report_type,
                treatments=treatments,
                states=states,
                verifications=verifications,
                finding_title=finding_title,
                age=age,
                min_severity=min_severity,
                max_severity=max_severity,
                last_report=last_report,
                date_info=date_info,
                location=location,
            ),
        },
    )

    report_filename, report_filesize = await get_report(
        item=item,
        report_type=report_type,
        filters=Filters(
            treatments=treatments,
            states=states,
            verifications=verifications,
            finding_title=finding_title,
            age=age,
            min_severity=min_severity,
            max_severity=max_severity,
            last_report=last_report,
            closing_date=date_info["closing_date"],
            start_closing_date=date_info["start_closing_date"],
            min_release_date=date_info["min_release_date"],
            max_release_date=date_info["max_release_date"],
            location=location,
        ),
    )
    if report_filename:
        await send_report(
            item=item,
            report_type=report_type,
            report_filename=report_filename,
            report_filesize=report_filesize,
            treatments=treatments,
            states=states,
            verifications=verifications,
            finding_title=finding_title,
            age=age,
            min_severity=min_severity,
            max_severity=max_severity,
            last_report=last_report,
            date_info=date_info,
            location=location,
        )
    return report_filename, report_filesize
