import logging
import logging.config

from integrates.batch.types import (
    BatchProcessing,
)
from integrates.custom_utils import (
    groups as groups_utils,
)
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model.vulnerabilities.enums import (
    VulnerabilityStateStatus,
)
from integrates.db_model.vulnerabilities.types import (
    GroupVulnerabilitiesRequest,
)
from integrates.organizations.utils import (
    get_organization,
)
from integrates.settings import (
    LOGGING,
)
from integrates.unreliable_indicators.enums import (
    EntityDependency,
)
from integrates.unreliable_indicators.operations import (
    update_unreliable_indicators_by_deps,
)

logging.config.dictConfig(LOGGING)

LOGGER = logging.getLogger(__name__)


async def update_organization_priority_policies(*, item: BatchProcessing) -> None:
    loaders: Dataloaders = get_new_context()
    org_priority_policies = item.additional_info.get("priority_policies", None)
    policy_to_remove = item.additional_info.get("policy_to_remove", None)
    organization_name = item.entity
    organization = await get_organization(loaders, organization_name)
    organization_id = organization.id
    groups = await loaders.organization_groups.load(organization_id)
    active_groups = groups_utils.filter_active_groups(groups)
    active_group_names = [group.name for group in active_groups]

    LOGGER.info(
        "Groups to be processed",
        extra={
            "extra": {
                "organization_name": organization_name,
                "org_priority_policies": org_priority_policies,
                "policy_to_remove": policy_to_remove,
                "groups": active_group_names,
            },
        },
    )

    for group_name in active_group_names:
        group_vulnerabilities = await loaders.group_vulnerabilities.load(
            GroupVulnerabilitiesRequest(
                group_name=group_name,
                state_status=VulnerabilityStateStatus.VULNERABLE,
            ),
        )
        await update_unreliable_indicators_by_deps(
            EntityDependency.update_organization_priority_policies,
            vulnerability_ids=[edge.node.id for edge in group_vulnerabilities.edges],
        )
