import os
import tarfile
import tempfile
from datetime import UTC, datetime

from fluidattacks_core.git import reset_repo
from git.repo import Repo

from integrates import batch_dispatch
from integrates.batch.enums import Action, IntegratesBatchQueue
from integrates.batch.types import BatchProcessing
from integrates.batch_dispatch.rebase import rebase
from integrates.dataloaders import get_new_context
from integrates.db_model.roots.enums import RootCloningStatus, RootRebaseStatus
from integrates.db_model.roots.types import GitRoot, GitRootRebase, RootRequest
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    DATE_2024,
    FindingFaker,
    GitRootCloningFaker,
    GitRootFaker,
    GitRootStateFaker,
    GroupFaker,
    OrganizationFaker,
    StakeholderFaker,
    VulnerabilityFaker,
    VulnerabilityStateFaker,
    random_uuid,
)
from integrates.testing.mocks import Mock, mocks
from integrates.testing.utils import freeze_time

FIN_ID = "3c475384-834c-47b0-ac71-a41a022e401c"
EMAIL_TEST = "testuser@fluidattacks.com"
CLIENT_EMAIL_TEST = "client@client_app.com"
GROUP_NAME = "grouptest"
ORG_NAME = "orgtest"
ROOT_ID = "root1"
BASE_PATH = os.path.join(os.path.dirname(__file__), "test-data")
FILE_PATH = os.path.join(BASE_PATH, "repo_mock.tar.gz")
REPO_PATH = os.path.join(BASE_PATH, "repo_mock")


def create_repo() -> Repo:
    with tarfile.open(FILE_PATH, "r:gz") as tar:
        tar.extractall(path=BASE_PATH, filter="data")
    return Repo(path=REPO_PATH)


class MockTemporaryDirectory:
    def __init__(
        self,
        suffix: str | None = None,
        prefix: str | None = None,
        ignore_cleanup_errors: bool = True,
    ) -> None:
        self.name = tempfile.mkdtemp(suffix, prefix)

    def __enter__(self, *_args: object) -> str:
        return REPO_PATH

    def __exit__(self, *_args: object) -> None:
        """
        This Mock simulates a controlled temp directory for testing.
        Requires no exit implementation.
        """


@freeze_time("2024-10-14T00:00:00")
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            findings=[FindingFaker(id=FIN_ID, group_name=GROUP_NAME)],
            organizations=[OrganizationFaker(id="org1", name=ORG_NAME)],
            stakeholders=[StakeholderFaker(email=EMAIL_TEST, role="admin", enrolled=True)],
            groups=[GroupFaker(name=GROUP_NAME, organization_id="org1")],
            roots=[
                GitRootFaker(
                    cloning=GitRootCloningFaker(
                        first_successful_cloning=DATE_2024,
                        commit="1895293a72993156070fecfa5f47ff1801426557",
                        modified_by=EMAIL_TEST,
                        status=RootCloningStatus.OK,
                    ),
                    group_name=GROUP_NAME,
                    id=ROOT_ID,
                    state=GitRootStateFaker(nickname="git1"),
                ),
            ],
            vulnerabilities=[
                VulnerabilityFaker(
                    id=random_uuid(),
                    created_by=EMAIL_TEST,
                    finding_id=FIN_ID,
                    root_id=ROOT_ID,
                    group_name=GROUP_NAME,
                    organization_name=ORG_NAME,
                    state=VulnerabilityStateFaker(
                        specific="12", commit="6be17a6aa6b2d0fae7b887af72e5fa59bda512c5"
                    ),
                ),
                VulnerabilityFaker(
                    id=random_uuid(),
                    created_by=EMAIL_TEST,
                    finding_id=FIN_ID,
                    root_id=ROOT_ID,
                    group_name=GROUP_NAME,
                    organization_name=ORG_NAME,
                    state=VulnerabilityStateFaker(
                        specific="1014", commit="c8dd25a1ade4e80cef986b49d8e15d1814994627"
                    ),
                ),
            ],
        ),
    ),
    others=[
        Mock(batch_dispatch.rebase, "download_repo", "async", create_repo()),
        Mock(tempfile, "TemporaryDirectory", "function", MockTemporaryDirectory),
    ],
)
async def test_rebase_root() -> None:
    # Act
    loaders = get_new_context()
    loaders.root.clear_all()
    await reset_repo(REPO_PATH)

    await rebase(
        item=BatchProcessing(
            additional_info={"root_ids": [ROOT_ID]},
            key=random_uuid(),
            action_name=Action.REBASE,
            batch_job_id=None,
            entity=GROUP_NAME,
            subject=EMAIL_TEST,
            time=DATE_2024,
            queue=IntegratesBatchQueue.SMALL,
        ),
    )
    git_root = await loaders.root.load(RootRequest(group_name=GROUP_NAME, root_id=ROOT_ID))

    # Assert
    assert git_root
    assert isinstance(git_root, GitRoot)
    assert git_root.id == ROOT_ID
    assert git_root.group_name == GROUP_NAME
    assert git_root.rebase == GitRootRebase(
        modified_by=EMAIL_TEST,
        modified_date=datetime(2024, 10, 14, 0, 0, tzinfo=UTC),
        reason="Git root has been rebased",
        status=RootRebaseStatus.SUCCESS,
        commit="50a516954a321f95c6fb8baccb640e87d2f5d193",
        commit_date=datetime(2021, 11, 11, 17, 41, 46, tzinfo=UTC),
        last_successful_rebase=datetime(2024, 10, 14, 0, 0, tzinfo=UTC),
    )
