import logging
import logging.config

from integrates.batch.types import BatchProcessing
from integrates.custom_utils.groups import get_group
from integrates.dataloaders import Dataloaders, get_new_context
from integrates.organizations import utils as orgs_utils
from integrates.outside_repositories.utils import update_organization_repositories
from integrates.settings import LOGGING

logging.config.dictConfig(LOGGING)

LOGGER = logging.getLogger(__name__)


async def refresh_repositories(*, item: BatchProcessing) -> None:
    loaders: Dataloaders = get_new_context()
    group_name = item.entity
    group = await get_group(loaders, group_name)
    organization = await orgs_utils.get_organization(loaders, group.organization_id)

    await update_organization_repositories(
        organization=organization,
        progress=0,
        all_group_names=set([group_name]),
    )

    LOGGER.info("Updating repositories for: %s", group_name)
