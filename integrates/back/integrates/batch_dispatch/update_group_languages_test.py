from collections import Counter

from integrates.batch_dispatch.update_group_languages import (
    aggregate_root_language_deltas,
    get_root_languages_deltas,
)
from integrates.db_model.types import CodeLanguage
from integrates.testing.utils import parametrize

EMAIL_TEST = "test@fluidattacks.com"
GROUP_NAME = "grouptest"
ROOT_ID = "root1"


@parametrize(
    args=["previous_languages", "new_languages", "expected"],
    cases=[
        [
            [],
            [
                CodeLanguage("C", 50),
                CodeLanguage("GraphQL", 50),
            ],
            Counter(
                {
                    "C": 50,
                    "GraphQL": 50,
                }
            ),
        ],
        [
            [
                CodeLanguage("C", 50),
                CodeLanguage("Python", 10),
            ],
            [],
            Counter(
                {
                    "C": -50,
                    "Python": -10,
                }
            ),
        ],
        [
            [
                CodeLanguage("C", 50),
                CodeLanguage("Python", 10),
                CodeLanguage("TypeScript", 20),
            ],
            [
                CodeLanguage("C", 50),
                CodeLanguage("GraphQL", 50),
                CodeLanguage("TypeScript", 30),
                CodeLanguage("YAML", 0),
            ],
            Counter(
                {
                    "C": 0,
                    "GraphQL": 50,
                    "Python": -10,
                    "TypeScript": 10,
                    "YAML": 0,
                }
            ),
        ],
    ],
)
def test_get_root_languages_deltas(
    previous_languages: list[CodeLanguage],
    new_languages: list[CodeLanguage],
    expected: Counter[str],
) -> None:
    assert get_root_languages_deltas(previous_languages, new_languages) == expected


@parametrize(
    args=["group_languages", "languages_deltas", "expected"],
    cases=[
        [
            [],
            Counter(
                {
                    "C": 0,
                    "GraphQL": 50,
                    "Python": -10,
                    "TypeScript": 10,
                    "YAML": 0,
                    "XML": -10,
                }
            ),
            [
                CodeLanguage("GraphQL", 50),
                CodeLanguage("TypeScript", 10),
            ],
        ],
        [
            [
                CodeLanguage("C", 50),
                CodeLanguage("Python", 10),
                CodeLanguage("TypeScript", 20),
            ],
            Counter(
                {
                    "C": -50,
                    "Python": -11,
                    "TypeScript": -22,
                }
            ),
            [],
        ],
        [
            [
                CodeLanguage("C", 50),
                CodeLanguage("Python", 10),
                CodeLanguage("TypeScript", 20),
            ],
            Counter(
                {
                    "C": 0,
                    "GraphQL": 50,
                    "Python": -10,
                    "TypeScript": 10,
                    "YAML": 0,
                    "XML": -10,
                }
            ),
            [
                CodeLanguage("C", 50),
                CodeLanguage("GraphQL", 50),
                CodeLanguage("TypeScript", 30),
            ],
        ],
        [
            [
                CodeLanguage("TypeScript", 20),
                CodeLanguage("Python", 10),
                CodeLanguage("C", 50),
            ],
            Counter(
                {
                    "XML": -10,
                    "YAML": 0,
                    "TypeScript": 10,
                    "Python": -10,
                    "GraphQL": 50,
                    "C": 0,
                }
            ),
            [
                CodeLanguage("C", 50),
                CodeLanguage("GraphQL", 50),
                CodeLanguage("TypeScript", 30),
            ],
        ],
    ],
)
def test_aggregate_root_language_deltas(
    group_languages: list[CodeLanguage],
    languages_deltas: Counter[str],
    expected: list[CodeLanguage],
) -> None:
    assert aggregate_root_language_deltas(group_languages, languages_deltas) == expected
