from integrates.batch.enums import Action, IntegratesBatchQueue
from integrates.batch.types import BatchProcessing
from integrates.batch_dispatch.update_toe_inputs import update_toe_inputs
from integrates.custom_utils.datetime import get_utc_now
from integrates.dataloaders import get_new_context
from integrates.db_model.enums import Source
from integrates.db_model.roots.enums import RootEnvironmentUrlType
from integrates.db_model.toe_inputs.types import ToeInputRequest
from integrates.db_model.vulnerabilities.enums import VulnerabilityType
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    FindingFaker,
    GitRootFaker,
    GroupFaker,
    OrganizationFaker,
    RootEnvironmentUrlFaker,
    RootEnvironmentUrlStateFaker,
    ToeInputFaker,
    ToeInputStateFaker,
    VulnerabilityFaker,
    VulnerabilityStateFaker,
)
from integrates.testing.mocks import mocks
from integrates.vulnerabilities.domain.core import get_vulnerability


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            groups=[GroupFaker(name="testgroup", organization_id="org1")],
            roots=[GitRootFaker(group_name="group1", id="root1")],
            root_environment_urls=[
                RootEnvironmentUrlFaker(
                    url="test.1.2.apk",
                    id="env1",
                    group_name="group1",
                    root_id="root1",
                    state=RootEnvironmentUrlStateFaker(url_type=RootEnvironmentUrlType.APK),
                )
            ],
            toe_inputs=[
                ToeInputFaker(
                    group_name="group1",
                    root_id="root1",
                    component="test.1.1.apk",
                    entry_point="user2",
                    state=ToeInputStateFaker(has_vulnerabilities=True),
                ),
                ToeInputFaker(
                    group_name="group1",
                    root_id="root1",
                    component="test.1.1.apk",
                    entry_point="",
                    state=ToeInputStateFaker(has_vulnerabilities=True),
                ),
            ],
            findings=[FindingFaker(id="fin1")],
            vulnerabilities=[
                VulnerabilityFaker(
                    vuln_machine_hash=123,
                    id="vuln1",
                    finding_id="fin1",
                    group_name="group1",
                    root_id="root1",
                    type=VulnerabilityType.INPUTS,
                    state=VulnerabilityStateFaker(
                        source=Source.MACHINE, where="test.1.1.apk", specific=""
                    ),
                    vuln_skims_description="vuln found on root",
                    vuln_skims_method="c_sharp.sql_injection",
                    hacker_email="machine@fluidattacks.com",
                ),
                VulnerabilityFaker(
                    vuln_machine_hash=456,
                    id="vuln2",
                    group_name="group1",
                    root_id="root1",
                    type=VulnerabilityType.INPUTS,
                    state=VulnerabilityStateFaker(where="test.1.1.apk", specific="user2"),
                    finding_id="fin1",
                ),
            ],
        )
    )
)
async def test_update_toe_inputs() -> None:
    loaders = get_new_context()
    await update_toe_inputs(
        item=BatchProcessing(
            action_name=Action.UPDATE_TOE_INPUTS,
            entity="env1",
            subject="admin@fluidattacks.com",
            key="2",
            additional_info={
                "group_name": "group1",
                "root_id": "root1",
                "old_url": "test.1.1.apk",
            },
            queue=IntegratesBatchQueue.SMALL,
            time=get_utc_now(),
        )
    )

    vuln1 = await get_vulnerability(loaders, "vuln1", clear_loader=True)
    vuln2 = await get_vulnerability(loaders, "vuln2", clear_loader=True)
    old = await loaders.toe_input.load(
        ToeInputRequest(
            component="test.1.1.apk", entry_point="user2", group_name="group1", root_id="root1"
        ),
    )
    current = await loaders.toe_input.load(
        ToeInputRequest(
            component="test.1.2.apk", entry_point="user2", group_name="group1", root_id="root1"
        ),
    )

    assert old
    assert current
    assert not old.state.be_present
    assert current.state.be_present
    assert vuln1.state.where.startswith("test.1.2.apk")
    assert vuln2.state.where.startswith("test.1.2.apk")
    assert vuln1.hash != 123
    assert vuln2.hash == 456
