import logging
import logging.config

from integrates.batch.types import (
    BatchProcessing,
)
from integrates.custom_exceptions import (
    OrgFindingPolicyNotFound,
)
from integrates.custom_utils import (
    groups as groups_utils,
)
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model.organization_finding_policies.enums import (
    PolicyStateStatus,
)
from integrates.db_model.organization_finding_policies.types import (
    OrgFindingPolicy,
    OrgFindingPolicyRequest,
)
from integrates.organizations.utils import (
    get_organization,
)
from integrates.organizations_finding_policies.domain import (
    update_finding_policy_in_groups,
)
from integrates.settings import (
    LOGGING,
)
from integrates.unreliable_indicators.enums import (
    EntityDependency,
)
from integrates.unreliable_indicators.operations import (
    update_unreliable_indicators_by_deps,
)

logging.config.dictConfig(LOGGING)

LOGGER = logging.getLogger(__name__)
TRANSACTIONS_LOGGER: logging.Logger = logging.getLogger("transactional")


async def handle_finding_policy(*, item: BatchProcessing) -> None:
    loaders: Dataloaders = get_new_context()
    organization_name: str = item.additional_info["organization_name"]

    TRANSACTIONS_LOGGER.info(
        "Processing handle organization finding policy requested",
        extra={
            "user_email": item.subject,
            "organization_name": organization_name,
            "policy_id": item.entity,
        },
    )

    finding_policy: OrgFindingPolicy | None = await loaders.organization_finding_policy.load(
        OrgFindingPolicyRequest(organization_name=organization_name, policy_id=item.entity),
    )
    if not finding_policy:
        raise OrgFindingPolicyNotFound()
    if finding_policy.state.status in {
        PolicyStateStatus.APPROVED,
        PolicyStateStatus.INACTIVE,
    }:
        organization = await get_organization(loaders, organization_name)
        organization_id = organization.id
        groups = await loaders.organization_groups.load(organization_id)
        active_groups = groups_utils.filter_active_groups(groups)
        active_group_names = [group.name for group in active_groups]
        finding_name = finding_policy.name.lower()
        (
            updated_finding_ids,
            updated_vuln_ids,
        ) = await update_finding_policy_in_groups(
            loaders=loaders,
            email=item.subject,
            finding_name=finding_name,
            group_names=active_group_names,
            status=finding_policy.state.status,
            tags=set(finding_policy.tags),
        )
        await update_unreliable_indicators_by_deps(
            EntityDependency.handle_finding_policy,
            finding_ids=updated_finding_ids,
            vulnerability_ids=updated_vuln_ids,
        )
