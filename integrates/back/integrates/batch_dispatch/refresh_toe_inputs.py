import logging
import logging.config

from aioextensions import (
    collect,
)

from integrates.batch.types import (
    BatchProcessing,
)
from integrates.custom_exceptions import (
    RepeatedToeInput,
    ToeInputAlreadyUpdated,
)
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model import (
    toe_inputs as toe_inputs_model,
)
from integrates.db_model.roots.enums import (
    RootStatus,
)
from integrates.db_model.roots.types import (
    GitRoot,
    URLRoot,
)
from integrates.db_model.toe_inputs.types import (
    GroupToeInputsRequest,
    ToeInput,
)
from integrates.decorators import (
    retry_on_exceptions,
)
from integrates.dynamodb.exceptions import (
    UnavailabilityError,
)
from integrates.settings import (
    LOGGING,
)
from integrates.toe.inputs import (
    domain as toe_inputs_domain,
)
from integrates.toe.inputs.types import (
    ToeInputAttributesToUpdate,
)

logging.config.dictConfig(LOGGING)

LOGGER = logging.getLogger(__name__)


toe_inputs_remove = retry_on_exceptions(exceptions=(UnavailabilityError,), sleep_seconds=5)(
    toe_inputs_model.remove_toe_inputs,
)
toe_inputs_update = retry_on_exceptions(exceptions=(UnavailabilityError,), sleep_seconds=5)(
    toe_inputs_domain.update,
)


def get_non_present_toe_inputs_to_update(
    root: GitRoot | URLRoot,
    root_toe_inputs: tuple[ToeInput, ...],
) -> tuple[tuple[ToeInput, ToeInputAttributesToUpdate], ...]:
    LOGGER.info(
        "Getting non present toe inputs to update",
        extra={
            "extra": {
                "repo_nickname": root.state.nickname,
            },
        },
    )
    return tuple(
        (
            toe_input,
            ToeInputAttributesToUpdate(be_present=False),
        )
        for toe_input in root_toe_inputs
        if root.state.status == RootStatus.INACTIVE
        and toe_input.state.be_present
        and toe_input.state.seen_at is not None
    )


def get_toe_inputs_to_remove(
    root: GitRoot | URLRoot,
    root_toe_inputs: tuple[ToeInput, ...],
) -> tuple[ToeInput, ...]:
    LOGGER.info(
        "Getting toe inputs to remove",
        extra={
            "extra": {
                "repo_nickname": root.state.nickname,
            },
        },
    )
    return tuple(
        toe_input
        for toe_input in root_toe_inputs
        if root.state.status == RootStatus.INACTIVE and toe_input.state.seen_at is None
    )


def get_present_toe_inputs_to_update(
    root: GitRoot | URLRoot,
    root_toe_inputs: tuple[ToeInput, ...],
) -> tuple[tuple[ToeInput, ToeInputAttributesToUpdate], ...]:
    LOGGER.info(
        "Getting present toe inputs to update",
        extra={
            "extra": {
                "repo_nickname": root.state.nickname,
            },
        },
    )
    return tuple(
        (
            toe_input,
            ToeInputAttributesToUpdate(be_present=True),
        )
        for toe_input in root_toe_inputs
        if root.state.status == RootStatus.ACTIVE and not toe_input.state.be_present
    )


async def refresh_active_root_toe_inputs(
    loaders: Dataloaders,
    group_name: str,
    root: GitRoot | URLRoot,
) -> tuple[bool, str]:
    LOGGER.info(
        "Refreshing active toe inputs",
        extra={
            "extra": {
                "repo_nickname": root.state.nickname,
            },
        },
    )
    group_toe_inputs = await loaders.group_toe_inputs.load_nodes(
        GroupToeInputsRequest(group_name=group_name),
    )
    root_toe_inputs = tuple(
        toe_input for toe_input in group_toe_inputs if toe_input.root_id == root.id
    )
    present_toe_inputs_to_update = get_present_toe_inputs_to_update(root, root_toe_inputs)
    await collect(
        tuple(
            toe_inputs_update(
                loaders=loaders,
                current_value=current_value,
                attributes=attrs_to_update,
                modified_by="machine@fluidattacks.com",
                is_moving_toe_input=True,
            )
            for current_value, attrs_to_update in (present_toe_inputs_to_update)
        ),
    )
    LOGGER.info(
        "Finish refreshing active toe inputs",
        extra={
            "extra": {
                "repo_nickname": root.state.nickname,
            },
        },
    )
    return True, "Finish refreshing active toe inputs"


async def refresh_inactive_root_toe_inputs(
    loaders: Dataloaders,
    group_name: str,
    root: GitRoot | URLRoot,
) -> tuple[bool, str]:
    LOGGER.info(
        "Refreshing inactive toe inputs",
        extra={
            "extra": {
                "repo_nickname": root.state.nickname,
            },
        },
    )
    group_toe_inputs = await loaders.group_toe_inputs.load_nodes(
        GroupToeInputsRequest(group_name=group_name),
    )
    root_toe_inputs = tuple(
        toe_input for toe_input in group_toe_inputs if toe_input.root_id == root.id
    )
    non_present_toe_inputs_to_update = get_non_present_toe_inputs_to_update(root, root_toe_inputs)
    await collect(
        tuple(
            toe_inputs_update(
                loaders=loaders,
                current_value=current_value,
                attributes=attrs_to_update,
                modified_by="machine@fluidattacks.com",
                is_moving_toe_input=True,
            )
            for current_value, attrs_to_update in (non_present_toe_inputs_to_update)
        ),
    )
    await toe_inputs_remove(
        toe_inputs=get_toe_inputs_to_remove(root, root_toe_inputs),
    )
    LOGGER.info(
        "Finish refreshing inactive toe inputs",
        extra={
            "extra": {
                "repo_nickname": root.state.nickname,
            },
        },
    )
    return True, "Finish refreshing inactive toe inputs"


@retry_on_exceptions(
    exceptions=(
        RepeatedToeInput,
        ToeInputAlreadyUpdated,
    ),
)
async def refresh_root_toe_inputs(group_name: str, root: GitRoot | URLRoot) -> None:
    loaders: Dataloaders = get_new_context()
    if root.state.status == RootStatus.ACTIVE:
        await refresh_active_root_toe_inputs(loaders, group_name, root)
    else:
        await refresh_inactive_root_toe_inputs(loaders, group_name, root)


async def refresh_toe_inputs(*, item: BatchProcessing) -> None:
    loaders: Dataloaders = get_new_context()
    group_name = item.entity
    root_ids = set(item.additional_info["root_ids"])
    valid_roots = [
        root
        for root in await loaders.group_roots.load(group_name)
        if root.id in root_ids and isinstance(root, (GitRoot, URLRoot))
    ]
    LOGGER.info(
        "Roots to be processed",
        extra={
            "extra": {
                "group_name": group_name,
                "roots": [(root.id, root.state.nickname) for root in valid_roots],
            },
        },
    )

    for root in valid_roots:
        await refresh_root_toe_inputs(group_name, root)
