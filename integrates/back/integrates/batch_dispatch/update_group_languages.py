import logging
import logging.config
from collections import Counter
from contextlib import suppress

from integrates.batch.types import BatchProcessing
from integrates.custom_exceptions import IndicatorAlreadyUpdated
from integrates.custom_utils.roots import get_active_git_roots
from integrates.dataloaders import get_new_context
from integrates.db_model import roots as roots_model
from integrates.db_model.groups.types import GroupUnreliableIndicators
from integrates.db_model.roots.types import RootUnreliableIndicatorsToUpdate
from integrates.db_model.types import CodeLanguage
from integrates.groups import domain as groups_domain
from integrates.groups.languages_distribution import format_code_languages, get_root_code_languages
from integrates.settings import LOGGING

logging.config.dictConfig(LOGGING)
LOGGER = logging.getLogger(__name__)


def get_root_languages_deltas(
    previous_languages: list[CodeLanguage], new_languages: list[CodeLanguage]
) -> Counter[str]:
    current_languages_count = Counter({entry.language: entry.loc for entry in previous_languages})
    languages_deltas = Counter({entry.language: entry.loc for entry in new_languages})
    languages_deltas.subtract(current_languages_count)

    return languages_deltas


def aggregate_root_language_deltas(
    group_languages: list[CodeLanguage], languages_deltas: Counter[str]
) -> list[CodeLanguage]:
    group_languages_count = Counter({entry.language: entry.loc for entry in group_languages})
    group_languages_count.update(languages_deltas)

    return format_code_languages(
        [CodeLanguage(language, loc) for language, loc in dict(group_languages_count).items()],
    )


async def update_group_languages(*, item: BatchProcessing) -> None:
    loaders = get_new_context()
    group_name = item.entity
    root_ids = set(item.additional_info["root_ids"])
    valid_roots = [
        root for root in await get_active_git_roots(loaders, group_name) if root.id in root_ids
    ]
    LOGGER.info(
        "Roots to be processed",
        extra={
            "extra": {
                "group_name": group_name,
                "roots": [(root.id, root.state.nickname) for root in valid_roots],
            },
        },
    )

    group_indicators = await loaders.group_unreliable_indicators.load(group_name)
    group_languages: list[CodeLanguage] = group_indicators.code_languages or []
    for root in valid_roots:
        root_languages = await get_root_code_languages(root)
        with suppress(IndicatorAlreadyUpdated):
            await roots_model.update_unreliable_indicators(
                current_value=root.unreliable_indicators,
                group_name=root.group_name,
                indicators=RootUnreliableIndicatorsToUpdate(
                    unreliable_code_languages=root_languages,
                ),
                root_id=root.id,
            )
            LOGGER.info(
                "Root languages updated",
                extra={
                    "extra": {
                        "root_id": root.id,
                        "root_nickname": root.state.nickname,
                    },
                },
            )
            languages_deltas = get_root_languages_deltas(
                previous_languages=root.unreliable_indicators.unreliable_code_languages,
                new_languages=root_languages,
            )
            group_languages = aggregate_root_language_deltas(group_languages, languages_deltas)

    await groups_domain.update_indicators(
        group_name=group_name,
        indicators=GroupUnreliableIndicators(
            code_languages=group_languages,
        ),
    )
    LOGGER.info(
        "Group languages updated",
        extra={
            "extra": {
                "group_name": group_name,
            },
        },
    )
