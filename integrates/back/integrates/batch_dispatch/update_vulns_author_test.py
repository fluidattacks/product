import fluidattacks_core.git as git_utils

from integrates.batch_dispatch.update_vulns_author import get_vulns_to_process, update_vuln_author
from integrates.dataloaders import get_new_context
from integrates.db_model.roots.types import GitRoot, RootRequest
from integrates.db_model.vulnerabilities.enums import VulnerabilityStateStatus, VulnerabilityType
from integrates.db_model.vulnerabilities.types import VulnerabilityAuthor
from integrates.testing.aws import (
    IntegratesAws,
    IntegratesDynamodb,
)
from integrates.testing.fakers import (
    DATE_2019,
    DATE_2024,
    EMAIL_GENERIC,
    GitRootFaker,
    VulnerabilityFaker,
    VulnerabilityStateFaker,
    random_uuid,
)
from integrates.testing.mocks import Mock, mocks

GROUP_NAME = "test-group"
ROOT_ID = random_uuid()
VULN_ID = random_uuid()


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            roots=[GitRootFaker(id=ROOT_ID, group_name=GROUP_NAME)],
            vulnerabilities=[
                VulnerabilityFaker(
                    author=VulnerabilityAuthor(
                        author_email=EMAIL_GENERIC,
                        commit="d2a38fd760ebb5e5bc4ed73571c909f8a5b1b389",
                        commit_date=DATE_2019,
                    ),
                    group_name=GROUP_NAME,
                    id=random_uuid(),
                    root_id=ROOT_ID,
                    state=VulnerabilityStateFaker(
                        status=VulnerabilityStateStatus.VULNERABLE,
                        commit="8190fa99edd6146c1a625fbb9598c62d9788a4f9",
                        specific="1",
                    ),
                    type=VulnerabilityType.LINES,
                ),
                VulnerabilityFaker(
                    author=None,
                    group_name=GROUP_NAME,
                    id=random_uuid(),
                    root_id=ROOT_ID,
                    state=VulnerabilityStateFaker(
                        status=VulnerabilityStateStatus.SAFE,
                        commit="8190fa99edd6146c1a625fbb9598c62d9788a4f9",
                        specific="1",
                    ),
                    type=VulnerabilityType.LINES,
                ),
                VulnerabilityFaker(
                    author=None,
                    group_name=GROUP_NAME,
                    id=random_uuid(),
                    root_id=ROOT_ID,
                    state=VulnerabilityStateFaker(
                        status=VulnerabilityStateStatus.VULNERABLE,
                        commit="8190fa99edd6146c1a625fbb9598c62d9788a4f9",
                        specific="0",
                    ),
                    type=VulnerabilityType.LINES,
                ),
                VulnerabilityFaker(
                    author=None,
                    group_name=GROUP_NAME,
                    id=random_uuid(),
                    root_id=ROOT_ID,
                    state=VulnerabilityStateFaker(
                        status=VulnerabilityStateStatus.VULNERABLE,
                        commit="8190fa99edd6146c1a625fbb9598c62d9788a4f9",
                        specific="input",
                    ),
                    type=VulnerabilityType.INPUTS,
                ),
                VulnerabilityFaker(
                    author=None,
                    group_name=GROUP_NAME,
                    id=VULN_ID,
                    root_id=ROOT_ID,
                    state=VulnerabilityStateFaker(
                        status=VulnerabilityStateStatus.VULNERABLE,
                        commit="8190fa99edd6146c1a625fbb9598c62d9788a4f9",
                        specific="1",
                    ),
                    type=VulnerabilityType.LINES,
                ),
            ],
        ),
    ),
)
async def test_get_vulns_to_process() -> None:
    loaders = get_new_context()
    root = await loaders.root.load(RootRequest(GROUP_NAME, ROOT_ID))
    assert isinstance(root, GitRoot)
    vulns = await get_vulns_to_process(loaders, root)
    assert len(vulns) == 1
    assert vulns[0].id == VULN_ID


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            vulnerabilities=[
                VulnerabilityFaker(
                    author=None,
                    created_date=DATE_2024,
                    id=VULN_ID,
                )
            ],
        ),
    ),
    others=[
        Mock(
            git_utils,
            "get_line_author",
            "async",
            git_utils.CommitInfo(
                hash="d2a38fd760ebb5e5bc4ed73571c909f8a5b1b389",
                author=EMAIL_GENERIC,
                modified_date=DATE_2019,
            ),
        )
    ],
)
async def test_update_vuln_author() -> None:
    loaders = get_new_context()
    vuln = await loaders.vulnerability.load(VULN_ID)
    assert vuln
    assert vuln.author is None

    await update_vuln_author(vuln=vuln, repo_working_dir="repo_working_dir")

    loaders = get_new_context()
    vuln = await loaders.vulnerability.load(VULN_ID)
    assert vuln
    assert vuln.author == VulnerabilityAuthor(
        author_email=EMAIL_GENERIC,
        commit="d2a38fd760ebb5e5bc4ed73571c909f8a5b1b389",
        commit_date=DATE_2019,
    )


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            vulnerabilities=[
                VulnerabilityFaker(
                    author=None,
                    created_date=DATE_2019,
                    id=VULN_ID,
                )
            ],
        ),
    ),
    others=[
        Mock(
            git_utils,
            "get_line_author",
            "async",
            git_utils.CommitInfo(
                hash="d2a38fd760ebb5e5bc4ed73571c909f8a5b1b389",
                author=EMAIL_GENERIC,
                modified_date=DATE_2024,
            ),
        )
    ],
)
async def test_update_vuln_author_invalid() -> None:
    loaders = get_new_context()
    vuln = await loaders.vulnerability.load(VULN_ID)
    assert vuln
    assert vuln.author is None

    await update_vuln_author(vuln=vuln, repo_working_dir="repo_working_dir")

    loaders = get_new_context()
    vuln = await loaders.vulnerability.load(VULN_ID)
    assert vuln
    assert vuln.author is None
