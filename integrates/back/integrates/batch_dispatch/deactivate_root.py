import logging
import logging.config

from integrates.batch.types import (
    BatchProcessing,
)
from integrates.custom_utils.filter_vulnerabilities import (
    filter_non_deleted,
)
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model.roots.types import GitRoot
from integrates.roots.domain import deactivate_root_toe_packages
from integrates.settings import (
    LOGGING,
)
from integrates.unreliable_indicators.enums import (
    EntityDependency,
)
from integrates.unreliable_indicators.operations import (
    update_unreliable_indicators_by_deps,
)
from integrates.vulnerabilities import (
    domain as vulns_domain,
)

logging.config.dictConfig(LOGGING)

LOGGER = logging.getLogger(__name__)


async def deactivate_root(*, item: BatchProcessing) -> None:
    loaders: Dataloaders = get_new_context()
    group_name = item.entity
    email = item.subject
    root_ids = set(item.additional_info["root_ids"])
    valid_roots = [
        root for root in await loaders.group_roots.load(group_name) if root.id in root_ids
    ]
    LOGGER.info(
        "Roots to be processed",
        extra={
            "extra": {
                "group_name": group_name,
                "roots": [(root.id, root.state.nickname) for root in valid_roots],
            },
        },
    )

    for root in valid_roots:
        root_vulns = await loaders.root_vulnerabilities.load(root.id)
        root_vulns_non_deleted = filter_non_deleted(root_vulns)
        if isinstance(root, GitRoot):
            await deactivate_root_toe_packages(loaders, root.id, group_name)
        await vulns_domain.deactivate_vulnerabilities(
            email=email,
            loaders=loaders,
            vulns_nzr=root_vulns_non_deleted,
        )
        await vulns_domain.close_released_vulnerabilities(
            email=email,
            loaders=loaders,
            vulns_nzr=root_vulns_non_deleted,
        )
        await update_unreliable_indicators_by_deps(
            EntityDependency.deactivate_root,
            finding_ids=list({vuln.finding_id for vuln in root_vulns_non_deleted}),
            vulnerability_ids=list({vuln.id for vuln in root_vulns_non_deleted}),
            root_ids=[(group_name, root.id)],
        )
