import logging
from typing import Any, Generic, NamedTuple, cast

import simplejson as json
from opensearchpy import NotFoundError
from opensearchpy.helpers import aggs, query
from opensearchpy.helpers.utils import DslBase

from integrates.class_types.types import Item, TItem
from integrates.dynamodb.types import PageInfo
from integrates.search.client import get_client
from integrates.search.enums import Sort
from integrates.search.types import BoolQuery, ItemSearchResponse, ScriptQuery, SearchResponse

LOGGER = logging.getLogger(__name__)


class SearchParams(NamedTuple):
    index_value: str
    limit: int
    bool_filters: list[BoolQuery] | None = None
    exact_filters: Item | None = None
    after: str | list[str] | None = None
    query: str | None = None
    type_query: str | None = None
    collapse: str | None = None
    should_filters: list[Item] | None = None
    should_and_filters: list[Item] | None = None
    should_match_prefix_filters: list[Item] | None = None
    should_range_filters: list[Item] | None = None
    minimum_should_match: int | str = 1
    must_filters: list[Item] | None = None
    must_match_prefix_filters: list[Item] | None = None
    range_filters: list[Item] | None = None
    must_not_filters: list[Item] | None = None
    paginate: bool = True
    script_filters: list[ScriptQuery] | None = None
    sort_by: list[Item] | None = None
    wildcard_queries: list[Item] | None = None


def _get_end_cursor(*, hits: list[Item], sort_by: list[Item] | None) -> Any:
    if hits:
        if sort_by and "sort" in hits[-1] and hits[-1]["sort"]:
            return json.dumps(hits[-1]["sort"])
        return hits[-1]["_id"]

    if sort_by:
        return json.dumps([])
    return ""


def _get_terms_queries(exact_filters: Item | None) -> list[Item]:
    return (
        [{"terms": {key: value}} for key, value in exact_filters.items() if isinstance(value, list)]
        if exact_filters
        else []
    )


def _get_term_queries(exact_filters: Item | None) -> list[Item]:
    return (
        [
            {"term": {key: value}}
            for key, value in exact_filters.items()
            if not isinstance(value, list)
        ]
        if exact_filters
        else []
    )


def _get_full_text_queries(query: str | None, type_query: str | None) -> list[Item]:
    type_q = type_query if type_query else "best_fields"
    return [{"multi_match": {"query": query, "type": type_q}}] if query else []


def _get_full_match_prefix_filters(
    must_match_prefix_filters: list[Item] | None,
) -> list[Item]:
    return (
        [
            {"match_phrase_prefix": {key: value}}
            for attrs in must_match_prefix_filters
            for key, value in attrs.items()
        ]
        if must_match_prefix_filters
        else []
    )


def _get_query_range(range_filters: list[Item] | None) -> list[Item]:
    return [{"range": range} for range in range_filters] if range_filters else []


def _get_full_or_filters(should_filters: list[Item] | None) -> list[Item]:
    return (
        [{"match": {key: value}} for attrs in should_filters for key, value in attrs.items()]
        if should_filters
        else []
    )


def _get_full_should_must_filters(
    should_filters: list[Item] | None,
) -> list[Item]:
    return [{"bool": {"must": should_filters}}] if should_filters else []


def _get_should_range_filters(
    should_range_filters: list[Item] | None,
) -> list[Item]:
    return (
        [{"range": should_range_filter} for should_range_filter in should_range_filters]
        if should_range_filters
        else []
    )


def _get_full_and_filters(must_filters: list[Item] | None) -> list[Item]:
    return (
        [
            {"match": {key: {"query": value, "operator": "and"}}}
            for attrs in must_filters
            for key, value in attrs.items()
        ]
        if must_filters
        else []
    )


def _get_not_exists_filters(fields: list[str] | None) -> list[Item]:
    return [{"bool": {"must_not": [{"exists": {"field": field}}]}} for field in fields or []]


def _get_bool_query_range(range_filters: list[Item] | None) -> list[Item]:
    return (
        [{"bool": {"should": [{"range": range} for range in range_filters]}}]
        if range_filters
        else []
    )


def _get_bool_queries(bool_filters: list[BoolQuery] | None) -> list[Item]:
    def _get_bool_query(bool_filter: BoolQuery) -> Item:
        return {
            "must": [
                *_get_terms_queries(bool_filter.get("and_exact_filters")),
                *_get_not_exists_filters(bool_filter.get("and_not_exists_filters")),
                *_get_bool_query_range(bool_filter.get("and_range_filters")),
            ],
            "should": [
                *_get_bool_queries(bool_filter.get("or_bool_filters")),
                *_get_query_range(bool_filter.get("or_range_filters")),
            ],
        }

    return (
        [{"bool": _get_bool_query(bool_filter)} for bool_filter in bool_filters]
        if bool_filters
        else []
    )


def _get_wildcards_queries(queries: list[Item] | None) -> list[Item]:
    return (
        [{"wildcard": {key: {"value": value}}} for attrs in queries for key, value in attrs.items()]
        if queries
        else []
    )


def _get_must_script_queries(queries: list[ScriptQuery] | None) -> list[Item]:
    return (
        [
            {
                "script": {
                    "script": {
                        "source": attrs.source,
                        "lang": "painless",
                        "params": attrs.params,
                    },
                },
            }
            for attrs in queries
        ]
        if queries
        else []
    )


def _get_must_query(search_params: SearchParams) -> list[Item]:
    full_and_filters = _get_full_and_filters(search_params.must_filters)
    query_range = _get_query_range(search_params.range_filters)
    full_match_prefix_filters = _get_full_match_prefix_filters(
        search_params.must_match_prefix_filters,
    )
    full_text_queries = _get_full_text_queries(search_params.query, search_params.type_query)
    term_queries = _get_term_queries(search_params.exact_filters)
    terms_queries = _get_terms_queries(search_params.exact_filters)
    wildcards_queries = _get_wildcards_queries(search_params.wildcard_queries)
    bool_queries = _get_bool_queries(search_params.bool_filters)
    script_queries = _get_must_script_queries(search_params.script_filters)
    return [
        *full_and_filters,
        *full_text_queries,
        *query_range,
        *term_queries,
        *terms_queries,
        *full_match_prefix_filters,
        *bool_queries,
        *wildcards_queries,
        *script_queries,
    ]


def _parse_sort_by(sort_by: list[Item] | None) -> list[Item]:
    return sort_by if sort_by else [{"_id": {"order": Sort.DESCENDING.value}}]


def _parse_after(after: str | list[str]) -> list[str]:
    if isinstance(after, str):
        try:
            return json.loads(after)
        except json.JSONDecodeError:
            return [after]
    return after


def _parse_collapse(collapse: str) -> dict[str, str]:
    return {"field": collapse}


def _get_bool_query(search_params: SearchParams) -> dict:
    full_must_not_filters = _get_full_and_filters(search_params.must_not_filters)
    full_or_filters = _get_full_or_filters(search_params.should_filters)
    full_or_and_filters = _get_full_should_must_filters(search_params.should_and_filters)
    full_should_match_prefix_filters = _get_full_match_prefix_filters(
        search_params.should_match_prefix_filters,
    )
    should_range_filters = _get_should_range_filters(search_params.should_range_filters)
    must = _get_must_query(search_params)
    should = [
        *full_or_filters,
        *full_or_and_filters,
        *full_should_match_prefix_filters,
        *should_range_filters,
    ]
    min_should_match = (
        search_params.minimum_should_match
        if full_or_filters
        or full_should_match_prefix_filters
        or full_or_and_filters
        or should_range_filters
        else None
    )
    must_not = [*full_must_not_filters]

    return {
        "bool": {
            **({"must": must} if len(must) > 0 else {}),
            **({"should": should} if len(should) > 0 else {}),
            **({"minimum_should_match": min_should_match} if min_should_match else {}),
            **({"must_not": must_not} if len(must_not) > 0 else {}),
        },
    }


async def get_unique_terms(*, index: str, field: str, conditions: DslBase) -> list[str]:
    client = await get_client()

    body = {
        "query": conditions.to_dict(),
        "aggs": {"results": aggs.Terms(field=field, order={"_key": "desc"}).to_dict()},
    }

    response = await client.search(
        body=body,
        index=index,
        size=0,
    )

    return [str(item.get("key")) for item in response["aggregations"]["results"]["buckets"]]


async def _search(
    index: str,
    body: dict,
    size: int,
    paginate: bool = True,
    sort_by: list[Item] | None = None,
    after: str | list[str] | None = None,
    collapse: str | None = None,
) -> SearchResponse:
    client = await get_client()

    body["sort"] = _parse_sort_by(sort_by)

    if after:
        body["search_after"] = _parse_after(after)

    if collapse:
        body["collapse"] = _parse_collapse(collapse)

    try:
        limit = size + 1
        response = await client.search(
            body=body,
            index=index,
            size=limit,
            track_total_hits=True,
        )
        has_next_page = len(response["hits"]["hits"]) == limit
        hits: list[Item] = (
            response["hits"]["hits"][:-1] if has_next_page else response["hits"]["hits"]
        )
        total: int = response["hits"]["total"]["value"]
        end_cursor = _get_end_cursor(hits=hits, sort_by=sort_by)

        if not paginate:
            while has_next_page:
                body["search_after"] = [end_cursor]
                response = await client.search(
                    body=body,
                    index=index,
                    size=limit,
                )
                has_next_page = len(response["hits"]["hits"]) == limit
                hits += response["hits"]["hits"][:-1] if has_next_page else response["hits"]["hits"]
                end_cursor = _get_end_cursor(hits=hits, sort_by=sort_by)

    except NotFoundError as ex:
        LOGGER.warning(ex)
        hits = []
        total = 0
        end_cursor = ""
        has_next_page = False

    return SearchResponse(
        items=tuple(hit["_source"] for hit in hits),
        page_info=PageInfo(
            end_cursor=end_cursor,
            has_next_page=has_next_page,
        ),
        total=total,
    )


async def search(
    search_params: SearchParams,
) -> SearchResponse:
    """
    Searches for items matching both the user input (full-text)
    and the provided filters (exact matches)

    https://opensearch.org/docs/1.2/opensearch/query-dsl/index/
    https://opensearch-project.github.io/opensearch-py/api-ref/client.html#opensearchpy.OpenSearch.search
    """
    body: dict = {"query": _get_bool_query(search_params)}

    return await _search(
        index=search_params.index_value,
        body=body,
        size=search_params.limit,
        paginate=search_params.paginate,
        sort_by=search_params.sort_by,
        after=search_params.after,
        collapse=search_params.collapse,
    )


def sanitize_query_helpers(query_rules: list[DslBase]) -> list[DslBase]:
    """Remove empty query rules from a list of query rules."""
    terms = [
        rule
        for rule in query_rules
        if (
            isinstance(rule, query.Terms)
            and all(value is not None and len(value) > 0 for value in rule._params.values())
        )
    ]
    wildcards = [
        rule
        for rule in query_rules
        if isinstance(rule, query.Wildcard)
        and all(value is not None and len(value) > 0 for value in rule._params.values())
    ]
    others = [rule for rule in query_rules if not isinstance(rule, (query.Terms, query.Wildcard))]

    return [
        *terms,
        *wildcards,
        *others,
    ]


class SearchClient(Generic[TItem]):
    @staticmethod
    async def search(
        search_params: SearchParams,
    ) -> ItemSearchResponse[TItem]:
        response = await search(search_params)
        return ItemSearchResponse(
            items=tuple(cast(TItem, item) for item in response.items),
            page_info=response.page_info,
            total=response.total,
        )

    @staticmethod
    async def raw_search(
        index: str,
        body: dict,
        size: int = 10000,
        sort_by: list[Item] | None = None,
        paginate: bool = True,
        after: str | list[str] | None = None,
        collapse: str | None = None,
    ) -> ItemSearchResponse[TItem]:
        response = await _search(
            index=index,
            body=body,
            size=size,
            sort_by=sort_by,
            paginate=paginate,
            after=after,
            collapse=collapse,
        )
        return ItemSearchResponse(
            items=tuple(cast(TItem, item) for item in response.items),
            page_info=response.page_info,
            total=response.total,
        )
