from opensearchpy.helpers import query

from integrates.db_model.items import VulnerabilityItem
from integrates.search.operations import SearchClient, SearchParams, get_unique_terms
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb, IntegratesOpensearch
from integrates.testing.fakers import VulnerabilityFaker
from integrates.testing.mocks import mocks


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            vulnerabilities=[
                VulnerabilityFaker(id="1", group_name="one"),
                VulnerabilityFaker(id="2", group_name="two"),
                VulnerabilityFaker(id="3", group_name="three"),
            ]
        ),
        opensearch=IntegratesOpensearch(autoload=True),
    )
)
async def test_search_in_opensearch() -> None:
    results = await SearchClient[VulnerabilityItem].search(
        SearchParams(
            after="",
            exact_filters={"group_name": "three"},
            index_value="vulns_index",
            limit=10,
            query="",
        ),
    )
    assert len(results.items) == 1
    assert results.items[0]["pk"] == "VULN#3"
    assert results.items[0]["group_name"] == "three"


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            vulnerabilities=[
                VulnerabilityFaker(id="1", group_name="one"),
                VulnerabilityFaker(id="2", group_name="two"),
                VulnerabilityFaker(id="3", group_name="three"),
            ]
        ),
        opensearch=IntegratesOpensearch(autoload=True),
    )
)
async def test_raw_search_in_opensearch() -> None:
    results = await SearchClient[VulnerabilityItem].raw_search(
        index="vulns_index",
        body={
            "query": query.Bool(
                must=[query.MatchAll()],
                filter=[
                    query.Term(group_name="three"),
                ],
            ).to_dict(),
        },
    )
    assert len(results.items) == 1
    assert results.items[0]["pk"] == "VULN#3"
    assert results.items[0]["group_name"] == "three"


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            vulnerabilities=[
                VulnerabilityFaker(id="1", group_name="one"),
                VulnerabilityFaker(id="2", group_name="two"),
                VulnerabilityFaker(id="3", group_name="three"),
            ]
        ),
        opensearch=IntegratesOpensearch(autoload=True),
    )
)
async def test_get_unique_terms() -> None:
    results = await get_unique_terms(
        index="vulns_index",
        field="group_name.keyword",
        conditions=query.Bool(must=[query.MatchAll()]),
    )

    # Openmock is unable to return aggregations yet
    assert results == []
