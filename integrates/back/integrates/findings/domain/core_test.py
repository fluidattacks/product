from decimal import Decimal

from integrates.custom_exceptions import (
    InvalidCVSS3VectorString,
    InvalidCVSS4VectorString,
)
from integrates.dataloaders import Dataloaders, get_new_context
from integrates.db_model.types import SeverityScore
from integrates.findings.domain.core import (
    update_severity,
)
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    FindingFaker,
    GroupFaker,
)
from integrates.testing.mocks import mocks
from integrates.testing.utils import raises

GROUP_NAME = "group1"
FINDING_ID = "3c475384-834c-47b0-ac71-a41a022e401c"

CVSS_V3 = (
    "CVSS:3.1/AV:A/AC:L/PR:N/UI:N/S:C/C:H/I:L/A:L/E:P/RL:O/CR:L/"
    "AR:H/MAV:N/MAC:H/MPR:H/MUI:R/MS:U/MC:L/MA:L"
)
CVSS_V4 = "CVSS:4.0/AV:N/AC:L/AT:N/PR:N/UI:N/VC:H/VI:L/VA:L/SC:L/SI:L/SA:N"


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            groups=[GroupFaker(name=GROUP_NAME)],
            findings=[
                FindingFaker(
                    id=FINDING_ID,
                    group_name=GROUP_NAME,
                    severity_score=SeverityScore(
                        base_score=Decimal("4.5"),
                        temporal_score=Decimal("4.1"),
                        cvss_v3=(
                            "CVSS:3.1/AV:P/AC:H/PR:L/UI:N/S:C/C:L/I:L/A:L/"
                            "E:P/RL:O/CR:L/AR:H/MAV:N/MAC:H/MPR:H/MUI:R/MS:U/MC:L/MA:L"
                        ),
                        threat_score=Decimal("1.1"),
                        cvss_v4=(
                            "CVSS:4.0/AV:P/AC:H/AT:N/PR:L/UI:N/VC:L/VI:L/"
                            "VA:L/SC:L/SI:L/SA:L/E:P/AR:H/MAV:N/MAC:H/MPR:H/MUI:P/MVC:L/MVA:L"
                        ),
                        cvssf=Decimal("1.149"),
                        cvssf_v4=Decimal("0.018"),
                    ),
                ),
            ],
        ),
    )
)
async def test_update_severity() -> None:
    await update_severity(
        loaders=get_new_context(),
        finding_id=FINDING_ID,
        cvss_vector=CVSS_V3,
        cvss4_vector=CVSS_V4,
    )

    loaders: Dataloaders = get_new_context()
    finding = await loaders.finding.load(FINDING_ID)
    assert finding
    assert finding.severity_score
    assert finding.severity_score == SeverityScore(
        base_score=Decimal("8.8"),
        temporal_score=Decimal("7.9"),
        cvss_v3=CVSS_V3,
        cvss_v4=CVSS_V4,
        threat_score=Decimal("8.8"),
        cvssf=Decimal("222.861"),
        cvssf_v4=Decimal("776.047"),
    )


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            groups=[GroupFaker(name=GROUP_NAME)],
            findings=[
                FindingFaker(id=FINDING_ID, group_name=GROUP_NAME),
            ],
        ),
    )
)
async def test_update_severity_wrong_cvss() -> None:
    loaders: Dataloaders = get_new_context()
    with raises(InvalidCVSS3VectorString):
        await update_severity(
            loaders=loaders,
            finding_id=FINDING_ID,
            cvss_vector="CVSS:2.0/AV:L/AC:M/Au:N/C:P/I:P/A:P/E:U/RL:OF/RC:UC",
            cvss4_vector=CVSS_V4,
        )
    with raises(InvalidCVSS4VectorString):
        await update_severity(
            loaders=loaders,
            finding_id=FINDING_ID,
            cvss_vector=CVSS_V3,
            cvss4_vector="CVSS:2.0/AV:L/AC:M/Au:N/C:P/I:P/A:P/E:U/RL:OF/RC:UC",
        )
