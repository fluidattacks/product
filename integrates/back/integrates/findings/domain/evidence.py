import csv
import itertools
import logging
import logging.config
from typing import Any, cast

from aioextensions import schedule
from magic import Magic
from starlette.datastructures import UploadFile

from integrates.authz import get_group_level_enforcer
from integrates.context import BASE_URL
from integrates.custom_exceptions import (
    EvidenceNotFound,
    InvalidFileSize,
    InvalidFileType,
)
from integrates.custom_utils import datetime as datetime_utils
from integrates.custom_utils import files as files_utils
from integrates.custom_utils import findings as finding_utils
from integrates.custom_utils import utils
from integrates.custom_utils import validations_deco as validations_utils
from integrates.custom_utils.findings import get_finding, validate_evidence_name
from integrates.custom_utils.groups import get_group
from integrates.custom_utils.reports import get_extension
from integrates.dataloaders import Dataloaders
from integrates.db_model import findings as findings_model
from integrates.db_model.findings.enums import FindingEvidenceName
from integrates.db_model.findings.types import Finding, FindingEvidence, FindingEvidenceToUpdate
from integrates.decorators import retry_on_exceptions
from integrates.dynamodb.exceptions import ConditionalCheckFailedException, UnavailabilityError
from integrates.findings import storage as findings_storage
from integrates.mailer.common import send_mails_async
from integrates.mailer.utils import get_organization_name
from integrates.organizations.utils import get_organization
from integrates.settings import LOGGING

logging.config.dictConfig(LOGGING)

EVIDENCE_NAMES = {
    "animation": "animation",
    "evidence_route_1": "evidence1",
    "evidence_route_2": "evidence2",
    "evidence_route_3": "evidence3",
    "evidence_route_4": "evidence4",
    "evidence_route_5": "evidence5",
    "exploitation": "exploitation",
    "fileRecords": "records",
}
LOGGER = logging.getLogger(__name__)


async def download_evidence_file(group_name: str, finding_id: str, file_name: str) -> str:
    file_id = "/".join([group_name.lower(), finding_id, file_name])
    file_exists = await findings_storage.search_evidence(file_id)
    if file_exists:
        start = file_id.find(finding_id) + len(finding_id)
        localfile = f"/tmp{file_id[start:]}"  # noqa: S108
        ext = {".py": ".tmp"}
        tmp_filepath = utils.replace_all(localfile, ext)
        await findings_storage.download_evidence(file_id, tmp_filepath)
        return tmp_filepath
    raise EvidenceNotFound()


async def get_records_from_file(
    group_name: str,
    finding_id: str,
    file_name: str,
) -> list[dict[object, object]]:
    file_path = await download_evidence_file(group_name, finding_id, file_name)
    file_content = []
    encoding = Magic(mime_encoding=True).from_file(file_path)
    try:
        with open(file_path, encoding=encoding) as records_file:
            csv_reader = csv.reader(records_file, skipinitialspace=True)
            max_rows = 1000
            headers = next(csv_reader)
            file_content = [
                utils.list_to_dict(headers, row) for row in itertools.islice(csv_reader, max_rows)
            ]
    except (csv.Error, LookupError, UnicodeDecodeError) as ex:
        LOGGER.exception(ex, extra={"extra": locals()})
    return file_content


async def remove_evidence(loaders: Dataloaders, evidence_id: str, finding_id: str) -> None:
    finding = await get_finding(loaders, finding_id)
    evidence: FindingEvidence | None = getattr(finding.evidences, EVIDENCE_NAMES[evidence_id])
    if not evidence:
        raise EvidenceNotFound()

    full_name = f"{finding.group_name}/{finding.id}/{evidence.url}"
    await findings_storage.remove_evidence(full_name)
    await findings_model.remove_evidence(
        group_name=finding.group_name,
        finding_id=finding.id,
        evidence_name=FindingEvidenceName[EVIDENCE_NAMES[evidence_id]],
    )


async def validate_filename(loaders: Dataloaders, filename: str, finding: Finding) -> None:
    group = await get_group(loaders, finding.group_name)
    organization = await get_organization(loaders, group.organization_id)
    filename = filename.lower()
    validate_evidence_name(
        organization_name=organization.name.lower(),
        group_name=group.name.lower(),
        filename=filename,
    )


async def replace_different_format(
    *,
    finding: Finding,
    evidence: FindingEvidence,
    extension: str,
    evidence_id: str,
) -> None:
    old_full_name = f"{finding.group_name}/{finding.id}/{evidence.url}"
    ends: str = old_full_name.rsplit(".", 1)[-1]
    if evidence_id != "fileRecords" and ends != old_full_name and f".{ends}" != extension:
        await findings_storage.remove_evidence(old_full_name)


@retry_on_exceptions(
    exceptions=(ConditionalCheckFailedException, UnavailabilityError),
    max_attempts=3,
    sleep_seconds=5,
)
async def update_evidence(
    *,
    loaders: Dataloaders,
    finding_id: str,
    evidence_id: str,
    file_object: UploadFile,
    author_email: str,
    is_draft: bool = True,
    description: str | None = None,
    validate_name: bool | None = False,
) -> None:
    finding = await get_finding(loaders, finding_id)
    await validate_evidence(
        evidence_id=evidence_id,
        file=file_object,
        loaders=loaders,
        finding=finding,
        validate_name=validate_name,
    )
    mime_type = await files_utils.get_uploaded_file_mime(file_object)
    extension = get_extension(mime_type)
    filename = f"{finding.group_name}-{finding.id}-{evidence_id}{extension}"
    if evidence_id == "fileRecords":
        old_filename = finding.evidences.records.url if finding.evidences.records else ""
        if old_filename != "":
            old_records = await get_records_from_file(finding.group_name, finding.id, old_filename)
            if old_records:
                file_object = await finding_utils.append_records_to_file(
                    cast(list[dict[str, str]], old_records),
                    file_object,
                )

    await findings_storage.save_evidence(
        file_object,
        f"{finding.group_name}/{finding.id}/{filename}",
    )
    evidence: FindingEvidence | None = getattr(finding.evidences, EVIDENCE_NAMES[evidence_id])
    if evidence:
        await replace_different_format(
            finding=finding,
            evidence=evidence,
            extension=extension,
            evidence_id=evidence_id,
        )
        evidence_to_update = FindingEvidenceToUpdate(
            url=filename,
            is_draft=is_draft,
            modified_date=datetime_utils.get_utc_now(),
            description=description,
        )
        await findings_model.update_evidence(
            current_value=evidence,
            group_name=finding.group_name,
            finding_id=finding.id,
            evidence_name=FindingEvidenceName[EVIDENCE_NAMES[evidence_id]],
            evidence=evidence_to_update,
        )
    else:
        evidence = FindingEvidence(
            author_email=author_email,
            description=description or "",
            is_draft=is_draft,
            modified_date=datetime_utils.get_utc_now(),
            url=filename,
        )
        await findings_model.add_evidence(
            group_name=finding.group_name,
            finding_id=finding.id,
            evidence_name=FindingEvidenceName[EVIDENCE_NAMES[evidence_id]],
            evidence=evidence,
        )


async def approve_evidence(*, loaders: Dataloaders, evidence_id: str, finding_id: str) -> None:
    finding = await get_finding(loaders, finding_id)
    evidence: FindingEvidence | None = getattr(finding.evidences, EVIDENCE_NAMES[evidence_id])

    if not evidence or evidence.is_draft is False:
        raise EvidenceNotFound()

    await findings_model.update_evidence(
        current_value=evidence,
        group_name=finding.group_name,
        finding_id=finding.id,
        evidence_name=FindingEvidenceName[EVIDENCE_NAMES[evidence_id]],
        evidence=FindingEvidenceToUpdate(
            is_draft=False,
            modified_date=datetime_utils.get_utc_now(),
        ),
    )


async def send_mail_evidence_rejected(
    loaders: Dataloaders,
    email_to: list[str],
    context: dict[str, Any],
    finding_name: str,
    group_name: str,
) -> None:
    await send_mails_async(
        loaders=loaders,
        email_to=email_to,
        context=context,
        subject=(f"Rejected evidence of [{finding_name}] in [{group_name}]"),
        template_name="evidence_rejection",
    )


async def reject_evidence(
    *,
    loaders: Dataloaders,
    evidence_id: str,
    finding_id: str,
    justification: str,
) -> None:
    finding = await get_finding(loaders, finding_id)
    evidence: FindingEvidence | None = getattr(finding.evidences, EVIDENCE_NAMES[evidence_id])

    if not evidence or evidence.is_draft is False:
        raise EvidenceNotFound()

    org_name = await get_organization_name(loaders, finding.group_name)

    mail_to = [evidence.author_email]
    email_context: dict[str, str] = {
        "group": finding.group_name,
        "finding_name": finding.title,
        "evidence_url": evidence.url,
        "justification": justification,
        "finding_url": (
            f"{BASE_URL}/orgs/{org_name}/groups/{finding.group_name}"
            f"/vulns/{finding.id}/locations"
        ),
    }
    schedule(
        send_mail_evidence_rejected(
            loaders=loaders,
            email_to=mail_to,
            context=email_context,
            finding_name=finding.title,
            group_name=finding.group_name,
        ),
    )


@validations_utils.validate_fields_deco(["description"])
@validations_utils.validate_length_deco("description", max_length=5000)
async def update_evidence_description(
    loaders: Dataloaders,
    finding_id: str,
    evidence_id: str,
    description: str,
) -> None:
    finding = await get_finding(loaders, finding_id)
    evidence: FindingEvidence | None = getattr(finding.evidences, EVIDENCE_NAMES[evidence_id])
    if not evidence:
        raise EvidenceNotFound()

    await findings_model.update_evidence(
        current_value=evidence,
        group_name=finding.group_name,
        finding_id=finding.id,
        evidence_name=FindingEvidenceName[EVIDENCE_NAMES[evidence_id]],
        evidence=FindingEvidenceToUpdate(description=description),
    )


@validations_utils.validate_fields_deco(["file.content_type"])
@validations_utils.validate_file_name_deco("file.filename")
async def validate_evidence(
    evidence_id: str,
    file: UploadFile,
    loaders: Dataloaders,
    finding: Finding,
    validate_name: bool | None = False,
) -> bool:
    mib = 2097152
    success = False
    allowed_mimes = []
    max_size = 20

    if evidence_id in ["animation", "exploitation"] or evidence_id.startswith("evidence"):
        allowed_mimes = ["image/png", "video/webm"]
    elif evidence_id == "fileRecords":
        allowed_mimes = ["text/csv", "text/plain", "application/csv"]

    if not await files_utils.assert_uploaded_file_mime(file, allowed_mimes):
        raise InvalidFileType()

    if await files_utils.get_file_size(file) < max_size * mib:
        success = True
    else:
        raise InvalidFileSize()

    file_name = file.filename if file.filename is not None else ""

    if validate_name:
        await validate_filename(loaders, file_name, finding)

    return success


async def filter_drafts(
    *,
    email: str,
    evidences: dict[str, dict[str, Any]],
    group_name: str,
    loaders: Dataloaders,
) -> dict[str, dict[str, Any]]:
    enforcer = await get_group_level_enforcer(loaders, email)
    can_see_drafts = enforcer(group_name, "integrates_api_resolvers_group_drafts_resolve")

    return {
        evidence_id: {
            key: (None if key == "author_email" and not can_see_drafts else value)
            for key, value in evidence.items()
        }
        if not evidence["is_draft"] or can_see_drafts
        else {key: None for key in evidence}
        for evidence_id, evidence in evidences.items()
    }
