from integrates.custom_utils import datetime as datetime_utils
from integrates.custom_utils import roots as roots_utils
from integrates.custom_utils.roots import get_unsolved_events_by_root
from integrates.dataloaders import Dataloaders
from integrates.db_model.events.enums import EventSolutionReason, EventType
from integrates.db_model.roots.enums import RootEnvironmentCloud, RootStatus
from integrates.db_model.roots.types import RootEnvironmentUrl
from integrates.db_model.vulnerabilities.enums import (
    VulnerabilityStateReason,
    VulnerabilityStateStatus,
)
from integrates.events.domain import add_event, solve_event
from integrates.findings.domain.vulnerabilities import (
    request_vulnerabilities_verification,
    verify_vulnerabilities,
)

MACHINE_EMAIL = "machine@fluidattacks.com"


def _map_event_solution_to_vuln_closing_reason(
    event_reason: EventSolutionReason,
) -> VulnerabilityStateReason:
    match event_reason:
        case EventSolutionReason.CLONED_SUCCESSFULLY:
            return VulnerabilityStateReason.VERIFIED_AS_SAFE
        case EventSolutionReason.MOVED_TO_ANOTHER_GROUP:
            return VulnerabilityStateReason.ROOT_MOVED_TO_ANOTHER_GROUP
        case _:
            return VulnerabilityStateReason.OTHER


async def _process_reattacks_and_verifications_after_event(
    *,
    loaders: Dataloaders,
    user_info: dict[str, str],
    event_id: str,
    event_solution_reason: EventSolutionReason,
) -> tuple[dict[str, set[str]], dict[str, set[str]]]:
    reattacks_dict: dict[str, set[str]] = {}
    verifications_dict: dict[str, set[str]] = {}
    affected_reattacks = await loaders.event_vulnerabilities_loader.load(event_id)
    if not affected_reattacks:
        return {}, {}

    for vuln in affected_reattacks:
        if vuln.state.status == VulnerabilityStateStatus.VULNERABLE:
            reattacks_dict.setdefault(vuln.finding_id, set()).add(vuln.id)
        elif vuln.state.status == VulnerabilityStateStatus.SAFE:
            verifications_dict.setdefault(vuln.finding_id, set()).add(vuln.id)

    for finding_id, reattack_ids in reattacks_dict.items():
        await request_vulnerabilities_verification(
            loaders=loaders,
            finding_id=finding_id,
            user_info=user_info,
            justification=(
                f"Event #{event_id} was solved. The reattacks are back to the Requested state"
            ),
            vulnerability_ids=reattack_ids,
            is_closing_event=True,
        )

    for finding_id, verification_ids in verifications_dict.items():
        await verify_vulnerabilities(
            finding_id=finding_id,
            user_info=user_info,
            justification=(
                f"Event #{event_id} was solved. As these vulnerabilities "
                "were closed, the reattacks are set to Verified"
            ),
            open_vulns_ids=[],
            closed_vulns_ids=list(verification_ids),
            vulns_to_close_from_file=[],
            is_closing_event=True,
            loaders=loaders,
            verification_reason=_map_event_solution_to_vuln_closing_reason(event_solution_reason),
        )

    return reattacks_dict, verifications_dict


async def solve_event_and_process_vulnerabilities(
    *,
    loaders: Dataloaders,
    event_id: str,
    group_name: str,
    hacker_email: str,
    reason: EventSolutionReason,
    other: str | None,
    user_info: dict[str, str],
) -> tuple[dict[str, set[str]], dict[str, set[str]]]:
    """
    Solve the event and process reattacks and verifications.
    It can either return two empty dicts or
    the `reattacks_dict[finding_id, set_of_respective_vuln_ids]`
    and the `verifications_dict[finding_id, set_of_respective_vuln_ids]`
    """
    await solve_event(
        loaders=loaders,
        event_id=event_id,
        group_name=group_name,
        hacker_email=hacker_email,
        reason=reason,
        other=other,
    )

    return await _process_reattacks_and_verifications_after_event(
        loaders=loaders,
        user_info=user_info,
        event_id=event_id,
        event_solution_reason=reason,
    )


async def manage_cspm_environment_events(
    *,
    loaders: Dataloaders,
    env_url: RootEnvironmentUrl,
    is_role_valid: bool,
    user_info: dict[str, str],
) -> None:
    if env_url.state.cloud_name not in {
        RootEnvironmentCloud.AWS,
        RootEnvironmentCloud.AZURE,
        RootEnvironmentCloud.GCP,
    }:
        return

    detail = (
        f"Failed to request session token: The environment {env_url.url} "
        "is invalid or misconfigured. Please check the credentials permissions"
        " and configuration."
    )

    unresolved_events = await get_unsolved_events_by_root(
        loaders=loaders,
        group_name=env_url.group_name,
        specific_type=EventType.ENVIRONMENT_ISSUES,
    )
    role_events = [
        event
        for events in unresolved_events.values()
        for event in events
        if detail == event.description and env_url.url == event.environment_url
    ]

    root = await roots_utils.get_root(loaders, env_url.root_id, env_url.group_name)

    if not is_role_valid and not role_events and root.state.status == RootStatus.ACTIVE:
        await add_event(
            loaders=loaders,
            hacker_email=MACHINE_EMAIL,
            group_name=env_url.group_name,
            detail=detail,
            event_date=datetime_utils.get_utc_now(),
            root_id=env_url.root_id,
            event_type=EventType.ENVIRONMENT_ISSUES,
            environment_url=env_url.url,
        )

    if is_role_valid:
        for event in role_events:
            await solve_event_and_process_vulnerabilities(
                loaders=loaders,
                event_id=event.id,
                group_name=env_url.group_name,
                hacker_email=MACHINE_EMAIL,
                reason=EventSolutionReason.ENVIRONMENT_IS_WORKING_NOW,
                user_info=user_info,
                other="",
            )
