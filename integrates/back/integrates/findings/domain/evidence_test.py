import integrates.s3.operations
from integrates import findings
from integrates.custom_exceptions import (
    EvidenceNotFound,
)
from integrates.dataloaders import get_new_context
from integrates.findings.domain import (
    reject_evidence,
    remove_evidence,
)
from integrates.s3.operations import (
    file_exists,
)
from integrates.testing.aws import (
    IntegratesAws,
    IntegratesDynamodb,
    IntegratesS3,
)
from integrates.testing.fakers import (
    FindingEvidenceFaker,
    FindingEvidencesFaker,
    FindingFaker,
    FindingStateFaker,
    GitRootFaker,
    GitRootStateFaker,
    GroupFaker,
    OrganizationFaker,
    StakeholderFaker,
)
from integrates.testing.mocks import Mock, mocks
from integrates.testing.utils import raises

EMAIL_TEST = "admin@fluidattacks.com"
GROUP_NAME = "group1"
ROOT_ID = "root1"


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            stakeholders=[StakeholderFaker(email=EMAIL_TEST, role="admin")],
            groups=[GroupFaker(name=GROUP_NAME, organization_id="org1")],
            roots=[
                GitRootFaker(
                    group_name=GROUP_NAME,
                    id=ROOT_ID,
                    state=GitRootStateFaker(nickname="back"),
                ),
            ],
            findings=[
                FindingFaker(
                    group_name="group1",
                    id="fin_id_1",
                    state=FindingStateFaker(modified_by="hacker@fluidattacks.com"),
                    title="237. Logging of sensitive data",
                    evidences=FindingEvidencesFaker(evidence1=FindingEvidenceFaker(is_draft=True)),
                ),
            ],
        )
    ),
    others=[Mock(findings.domain.evidence, "send_mail_evidence_rejected", "async", None)],
)
async def test_reject_evidence() -> None:
    loaders = get_new_context()

    await reject_evidence(
        loaders=loaders,
        evidence_id="evidence_route_1",
        finding_id="fin_id_1",
        justification="Wrong evidence",
    )

    loaders = get_new_context()
    finding_evidence = await loaders.finding.load("fin_id_1")
    assert finding_evidence is not None
    assert finding_evidence.evidences.evidence1 is not None
    assert finding_evidence.evidences.evidence1.is_draft is True


FINDING_ID = "422286126"


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            stakeholders=[StakeholderFaker(email=EMAIL_TEST, role="admin")],
            groups=[GroupFaker(name=GROUP_NAME, organization_id="org1")],
            roots=[
                GitRootFaker(
                    group_name=GROUP_NAME,
                    id=ROOT_ID,
                    state=GitRootStateFaker(nickname="back"),
                ),
            ],
            findings=[
                FindingFaker(
                    group_name="group1",
                    id=FINDING_ID,
                    state=FindingStateFaker(modified_by="hacker@fluidattacks.com"),
                    title="237. Logging of sensitive data",
                    evidences=FindingEvidencesFaker(
                        evidence1=FindingEvidenceFaker(
                            is_draft=True, url="group1-422286126-evidence_route_1.png"
                        )
                    ),
                ),
            ],
        ),
        s3=IntegratesS3(autoload=True),
    ),
    others=[Mock(integrates.s3.operations, "FI_AWS_S3_PATH_PREFIX", "function", "")],
)
async def test_remove_evidence() -> None:
    loaders = get_new_context()
    finding = await loaders.finding.load(FINDING_ID)
    assert finding
    assert finding.evidences.evidence1 is not None
    evidence_name = f"evidences/{finding.group_name}/{finding.id}/{finding.evidences.evidence1.url}"
    assert await file_exists(evidence_name, "integrates.dev")

    await remove_evidence(
        loaders=loaders,
        evidence_id="evidence_route_1",
        finding_id=FINDING_ID,
    )

    finding_after = await get_new_context().finding.load(FINDING_ID)
    assert finding_after is not None
    assert finding_after.evidences.evidence1 is None
    assert not await file_exists(evidence_name, "integrates.dev")


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            stakeholders=[StakeholderFaker(email=EMAIL_TEST, role="admin")],
            groups=[GroupFaker(name=GROUP_NAME, organization_id="org1")],
            roots=[
                GitRootFaker(
                    group_name=GROUP_NAME,
                    id=ROOT_ID,
                    state=GitRootStateFaker(nickname="back"),
                ),
            ],
            findings=[
                FindingFaker(
                    group_name="group1",
                    id=FINDING_ID,
                    state=FindingStateFaker(modified_by="hacker@fluidattacks.com"),
                    title="237. Logging of sensitive data",
                    evidences=FindingEvidencesFaker(evidence1=None),
                ),
            ],
        ),
    ),
)
async def test_remove_evidence_not_exists() -> None:
    loaders = get_new_context()
    with raises(EvidenceNotFound):
        await remove_evidence(
            loaders=loaders,
            evidence_id="evidence_route_1",
            finding_id=FINDING_ID,
        )
