from typing import (
    NamedTuple,
)

from integrates.db_model.enums import (
    Source,
)
from integrates.db_model.findings.enums import (
    FindingSorts,
)


class FindingDescriptionToUpdate(NamedTuple):
    attack_vector_description: str | None = None
    description: str | None = None
    recommendation: str | None = None
    sorts: FindingSorts | None = None
    threat: str | None = None
    title: str | None = None
    unfulfilled_requirements: list[str] | None = None


class FindingAttributesToAdd(NamedTuple):
    attack_vector_description: str
    cvss_vector: str
    cvss4_vector: str | None
    description: str
    min_time_to_remediate: int | None
    recommendation: str
    source: Source
    threat: str
    title: str
    unfulfilled_requirements: list[str]


class SeverityLevelSummary(NamedTuple):
    accepted: int
    closed: int
    total: int


class SeverityLevelsInfo(NamedTuple):
    critical: SeverityLevelSummary
    high: SeverityLevelSummary
    medium: SeverityLevelSummary
    low: SeverityLevelSummary


class Tracking(NamedTuple):
    cycle: int
    date: str
    accepted: int
    accepted_undefined: int
    assigned: str
    justification: str
    safe: int
    vulnerable: int
