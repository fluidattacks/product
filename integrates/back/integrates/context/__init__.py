import logging
import os


class MissingSecret(Exception):
    """Exception to be raised when a required secret is missing"""

    def __init__(self, prod_key: str, dev_key: str) -> None:
        """Constructor"""
        msg = f"Environment variable {prod_key} or {dev_key} is missing"
        super().__init__(msg)


def _get_secret(prod_key: str, dev_key: str) -> str:
    key = prod_key if os.environ["ENVIRONMENT"] == "production" else dev_key
    secret = os.environ.get(key)
    if secret is None:
        raise MissingSecret(prod_key, dev_key)
    return secret


logging.basicConfig(level=logging.DEBUG, format="[%(levelname)s] %(message)s")
LOGGER = logging.getLogger(__name__)

try:
    CI_COMMIT_REF_NAME = os.environ["CI_COMMIT_REF_NAME"]
    CI_COMMIT_SHA = os.environ["CI_COMMIT_SHA"]
    CI_COMMIT_SHORT_SHA = CI_COMMIT_SHA[0:8]
    FI_AWS_DYNAMODB_HOST = (
        None
        if os.environ.get("INTEGRATES_MOTO_TEST")
        else _get_secret("AWS_DYNAMODB_HOST", "AWS_DYNAMODB_HOST_DEV")
    )
    FI_AWS_OPENSEARCH_HOST = _get_secret("AWS_OPENSEARCH_HOST", "AWS_OPENSEARCH_HOST_DEV")
    FI_AWS_SQS_AUDIT_QUEUE_URL = _get_secret(
        "AWS_SQS_AUDIT_QUEUE_URL",
        "AWS_SQS_AUDIT_QUEUE_URL_DEV",
    )
    FI_AZUREAD_OAUTH2_KEY = os.environ["AZUREAD_OAUTH2_KEY"]
    FI_AZUREAD_OAUTH2_SECRET = os.environ["AZUREAD_OAUTH2_SECRET"]
    FI_BITBUCKET_OAUTH2_KEY = _get_secret("BITBUCKET_OAUTH2_KEY", "BITBUCKET_OAUTH2_KEY_DEV")
    FI_BITBUCKET_OAUTH2_SECRET = _get_secret(
        "BITBUCKET_OAUTH2_SECRET",
        "BITBUCKET_OAUTH2_SECRET_DEV",
    )
    FI_BUGSNAG_API_KEY_BACK = os.environ["BUGSNAG_API_KEY_BACK"]
    FI_BUGSNAG_API_KEY_SCHEDULER = os.environ["BUGSNAG_API_KEY_SCHEDULER"]
    FI_EPAYCO_PUBLIC_KEY = _get_secret("EPAYCO_PUBLIC_KEY", "EPAYCO_PUBLIC_KEY_DEV")
    FI_EPAYCO_SECRET_KEY = _get_secret("EPAYCO_SECRET_KEY", "EPAYCO_SECRET_KEY_DEV")
    FI_INTEGRATES_CRITERIA_COMPLIANCE = os.environ["INTEGRATES_CRITERIA_COMPLIANCE"]
    FI_INTEGRATES_CRITERIA_REQUIREMENTS = os.environ["INTEGRATES_CRITERIA_REQUIREMENTS"]
    FI_INTEGRATES_CRITERIA_VULNERABILITIES = os.environ["INTEGRATES_CRITERIA_VULNERABILITIES"]
    FI_INTEGRATES_QUEUE_SIZES = os.environ["INTEGRATES_QUEUE_SIZES"]
    FI_INTEGRATES_REPORTS_LOGO_PATH = os.environ["INTEGRATES_REPORTS_LOGO_PATH"]
    FI_INTEGRATES_DB_MODEL_PATH = os.environ["INTEGRATES_DB_MODEL_PATH"]
    FI_ASYNC_PROCESSING_DB_MODEL_PATH = os.environ["ASYNC_PROCESSING_DB_MODEL_PATH"]
    FI_LLM_SCAN_DB_MODEL_PATH = os.environ["LLM_SCAN_DB_MODEL_PATH"]
    FI_DEBUG = _get_secret("DEBUG", "DEBUG_DEV")
    FI_EMAIL_TEMPLATES: str = os.environ["INTEGRATES_MAILER_TEMPLATES"]
    FI_ENVIRONMENT = os.environ["ENVIRONMENT"]
    FI_AZURE_OAUTH2_ISSUES_APP_ID = _get_secret(
        "AZURE_OAUTH2_ISSUES_APP_ID",
        "AZURE_OAUTH2_ISSUES_APP_ID_DEV",
    )
    FI_AZURE_OAUTH2_ISSUES_SECRET = _get_secret(
        "AZURE_OAUTH2_ISSUES_SECRET",
        "AZURE_OAUTH2_ISSUES_SECRET_DEV",
    )
    FI_AZURE_OAUTH2_ISSUES_APP_ID_OLD = _get_secret(
        "AZURE_OAUTH2_ISSUES_APP_ID_OLD",
        "AZURE_OAUTH2_ISSUES_APP_ID_DEV",
    )
    FI_AZURE_OAUTH2_ISSUES_SECRET_OLD = _get_secret(
        "AZURE_OAUTH2_ISSUES_SECRET_OLD",
        "AZURE_OAUTH2_ISSUES_SECRET_DEV",
    )
    FI_AZURE_OAUTH2_REPOSITORY_APP_ID = os.environ["AZURE_OAUTH2_REPOSITORY_APP_ID"]
    FI_AZURE_OAUTH2_REPOSITORY_SECRET = os.environ["AZURE_OAUTH2_REPOSITORY_SECRET"]
    FI_BITBUCKET_OAUTH2_REPOSITORY_APP_ID = _get_secret(
        "BITBUCKET_OAUTH2_REPOSITORY_APP_ID",
        "BITBUCKET_OAUTH2_REPOSITORY_APP_ID_DEV",
    )
    FI_BITBUCKET_OAUTH2_REPOSITORY_SECRET = _get_secret(
        "BITBUCKET_OAUTH2_REPOSITORY_SECRET",
        "BITBUCKET_OAUTH2_REPOSITORY_SECRET_DEV",
    )
    FI_CLOUDFLARE_ACCOUNT_ID = os.environ["CLOUDFLARE_ACCOUNT_ID"]
    FI_CLOUDFLARE_API_TOKEN = _get_secret("CLOUDFLARE_API_TOKEN_INTEGRATES", "CLOUDFLARE_API_TOKEN")
    FI_FERNET_TOKEN = os.environ["FERNET_TOKEN"]
    FI_GITHUB_OAUTH2_APP_ID = os.environ["GITHUB_OAUTH2_APP_ID"]
    FI_GITHUB_OAUTH2_SECRET = os.environ["GITHUB_OAUTH2_SECRET"]
    FI_GITLAB_OAUTH2_APP_ID = os.environ["GITLAB_OAUTH2_APP_ID"]

    FI_GITLAB_ISSUES_OAUTH2_APP_ID = _get_secret(
        "GITLAB_ISSUES_OAUTH2_APP_ID_PROD",
        "GITLAB_ISSUES_OAUTH2_APP_ID",
    )
    FI_GITLAB_ISSUES_OAUTH2_SECRET = _get_secret(
        "GITLAB_ISSUES_OAUTH2_SECRET_PROD",
        "GITLAB_ISSUES_OAUTH2_SECRET",
    )
    FI_GITLAB_OAUTH2_SECRET = os.environ["GITLAB_OAUTH2_SECRET"]
    FI_GOOGLE_OAUTH2_KEY = os.environ["GOOGLE_OAUTH2_KEY"]
    FI_GOOGLE_OAUTH2_SECRET = os.environ["GOOGLE_OAUTH2_SECRET"]
    FI_JWT_ENCRYPTION_KEY = _get_secret("JWT_ENCRYPTION_KEY", "JWT_ENCRYPTION_KEY_DEV")
    FI_JWT_SECRET = os.environ["JWT_SECRET"]
    FI_JWT_SECRET_API = os.environ["JWT_SECRET_API"]
    FI_JWT_SECRET_RS512 = _get_secret("JWT_SECRET_RS512", "JWT_SECRET_RS512_DEV")
    FI_JWT_SECRET_API_RS512 = _get_secret("JWT_SECRET_API_RS512", "JWT_SECRET_API_RS512_DEV")
    FI_JWT_SECRET_ES512 = _get_secret("JWT_SECRET_ES512", "JWT_SECRET_ES512_DEV")
    FI_JWT_SECRET_API_ES512 = _get_secret("JWT_SECRET_API_ES512", "JWT_SECRET_API_ES512_DEV")
    FI_MAIL_CONTINUOUS = _get_secret("MAIL_CONTINUOUS", "MAIL_CONTINUOUS_DEV")
    FI_MAIL_COS = _get_secret("MAIL_COS", "MAIL_COS_DEV")
    FI_MAIL_CTO = _get_secret("MAIL_CTO", "MAIL_CTO_DEV")
    FI_MAIL_CXO = _get_secret("MAIL_CXO", "MAIL_CXO_DEV")
    FI_SNOWFLAKE_ACCOUNT = _get_secret("SNOWFLAKE_ACCOUNT", "SNOWFLAKE_ACCOUNT_DEV")
    FI_SNOWFLAKE_PRIVATE_KEY = _get_secret("SNOWFLAKE_PRIVATE_KEY", "SNOWFLAKE_PRIVATE_KEY_DEV")
    FI_SNOWFLAKE_USER = _get_secret("SNOWFLAKE_USER", "SNOWFLAKE_USER_DEV")
    FI_MAIL_CUSTOMER_EXPERIENCE = _get_secret(
        "MAIL_CUSTOMER_EXPERIENCE",
        "MAIL_CUSTOMER_EXPERIENCE_DEV",
    )
    FI_MAIL_CUSTOMER_SUCCESS = _get_secret("MAIL_CUSTOMER_SUCCESS", "MAIL_CUSTOMER_SUCCESS_DEV")
    FI_MAIL_FINANCE = _get_secret("MAIL_FINANCE", "MAIL_FINANCE_DEV")
    FI_MAIL_PRODUCTION = _get_secret("MAIL_PRODUCTION", "MAIL_PRODUCTION_DEV")
    FI_MAIL_PROFILING = _get_secret("MAIL_PROFILING", "MAIL_PROFILING_DEV")
    FI_MAIL_PROJECTS = _get_secret("MAIL_PROJECTS", "MAIL_PROJECTS_DEV")
    FI_MAIL_REVIEWERS = _get_secret("MAIL_REVIEWERS", "MAIL_REVIEWERS_DEV")
    FI_MAIL_TELEMARKETING = _get_secret("MAIL_TELEMARKETING", "MAIL_TELEMARKETING_DEV")
    FI_MARKETPLACE_EXTERNAL_ID = os.environ.get("MARKETPLACE_EXTERNAL_ID", "")
    FI_MARKETPLACE_PRODUCT_CODE = _get_secret(
        "MARKETPLACE_PRODUCT_CODE",
        "MARKETPLACE_PRODUCT_CODE_DEV",
    )
    FI_MARKETPLACE_ROLE_ARN = os.environ.get("MARKETPLACE_ROLE_ARN", "")
    FI_MANUAL_CLONING_PROJECTS = os.environ.get("MANUAL_CLONING_PROJECTS", "")
    FI_MIXPANEL_API_SECRET = os.environ["MIXPANEL_API_SECRET"]
    FI_MIXPANEL_API_TOKEN = os.environ["MIXPANEL_API_TOKEN"]
    FI_MIXPANEL_PROJECT_ID = os.environ["MIXPANEL_PROJECT_ID"]
    FI_OPENAI_API_KEY = os.environ.get("OPENAI_API_KEY", "")
    FI_SENDGRID_API_KEY = os.environ["SENDGRID_API_KEY"]
    FI_STARLETTE_SESSION_KEY = _get_secret("STARLETTE_SESSION_KEY", "STARLETTE_SESSION_KEY_DEV")
    FI_STRIPE_API_KEY = _get_secret("STRIPE_API_KEY", "STRIPE_API_KEY_DEV")
    FI_TEST_ORGS = os.environ["TEST_ORGS"]
    FI_TEST_PROJECTS = _get_secret("TEST_PROJECTS", "TEST_PROJECTS_DEV")
    FI_TREE_SITTER_PARSERS = os.environ["INTEGRATES_TREE_SITTER_PARSERS"]
    FI_TRELI_PASSWORD = _get_secret("TRELI_PASSWORD_PROD", "TRELI_PASSWORD_DEV")
    FI_TRELI_USER_NAME = _get_secret("TRELI_USER_NAME_PROD", "TRELI_USER_NAME_DEV")
    FI_TWILIO_ACCOUNT_SID = os.environ["TWILIO_ACCOUNT_SID"]
    FI_TWILIO_AUTH_TOKEN = os.environ["TWILIO_AUTH_TOKEN"]
    FI_TWILIO_VERIFY_SERVICE_SID = os.environ["TWILIO_VERIFY_SERVICE_SID"]
    FI_VOYAGE_API_KEY = os.environ["VOYAGE_API_KEY"]
    FI_ZOHO_CLIENT_ID = os.environ.get("ZOHO_CLIENT_ID", "")
    FI_ZOHO_CLIENT_SECRET = os.environ.get("ZOHO_CLIENT_SECRET", "")
    FI_ZOHO_DEPARTMENT_ID = os.environ.get("ZOHO_DEPARTMENT_ID", "")
    FI_ZOHO_ORG_ID = os.environ.get("ZOHO_ORG_ID", "")
    FI_ZOHO_REFRESH_TOKEN = os.environ.get("ZOHO_REFRESH_TOKEN", "")
    STARTDIR = os.environ["STARTDIR"]

    # not secrets but must be environment vars
    BASE_URL = (
        "https://app.fluidattacks.com"
        if FI_ENVIRONMENT == "production"
        else f"https://{CI_COMMIT_REF_NAME}.app.fluidattacks.com"
    )
    FI_AWS_REGION_NAME = "us-east-1"
    FI_AWS_S3_MAIN_BUCKET = "integrates" if FI_ENVIRONMENT == "production" else "integrates.dev"
    FI_AWS_S3_CONTINUOUS_REPOSITORIES = (
        "integrates.continuous-repositories" if FI_ENVIRONMENT == "production" else "integrates.dev"
    )
    FI_AWS_S3_ZTNA_LOGS = (
        "logpush.fluidattacks.com" if FI_ENVIRONMENT == "production" else "integrates.dev"
    )
    FI_AWS_S3_PATH_PREFIX = os.environ.get("AWS_S3_PATH_PREFIX", "")
except KeyError as exe:
    LOGGER.error("Environment variable %s doesn't exist", exe.args[0])
    raise
