import logging
import logging.config
from collections.abc import Awaitable, Iterable
from contextlib import suppress
from functools import partial
from typing import Any, cast

from aioextensions import collect, resolve

from integrates.custom_exceptions import IndicatorAlreadyUpdated, VulnNotFound
from integrates.custom_utils import roots as roots_utils
from integrates.custom_utils import vulnerabilities as vulns_utils
from integrates.custom_utils.findings import get_finding
from integrates.dataloaders import Dataloaders, get_new_context
from integrates.db_model import findings as findings_model
from integrates.db_model import roots as roots_model
from integrates.db_model import vulnerabilities as vulns_model
from integrates.db_model.findings.enums import FindingStatus
from integrates.db_model.findings.types import (
    FindingUnreliableIndicatorsToUpdate,
    FindingZeroRiskSummary,
)
from integrates.db_model.roots.types import RootUnreliableIndicatorsToUpdate
from integrates.db_model.vulnerabilities.types import (
    Vulnerability,
    VulnerabilityUnreliableIndicatorsToUpdate,
)
from integrates.decorators import retry_on_exceptions
from integrates.dynamodb.exceptions import UnavailabilityError
from integrates.findings import domain as findings_domain
from integrates.roots import update as roots_update
from integrates.settings import LOGGING
from integrates.unreliable_indicators.enums import Entity, EntityAttr, EntityDependency, EntityId
from integrates.vulnerabilities import domain as vulns_domain

from . import model as unreliable_indicators_model

logging.config.dictConfig(LOGGING)

LOGGER = logging.getLogger(__name__)


def _format_unreliable_status(status: str | None) -> FindingStatus | None:
    unreliable_status = None
    if status:
        unreliable_status = FindingStatus[status.upper()]
    return unreliable_status


async def update_findings_unreliable_indicators(
    finding_ids: list[str],
    attrs_to_update: set[EntityAttr],
) -> None:
    await collect(
        tuple(
            update_finding_unreliable_indicators(
                finding_id,
                attrs_to_update,
            )
            for finding_id in set(finding_ids)
        ),
    )


@retry_on_exceptions(
    exceptions=(
        IndicatorAlreadyUpdated,
        UnavailabilityError,
    ),
    max_attempts=20,
    sleep_seconds=0,
)
async def update_finding_unreliable_indicators(
    finding_id: str,
    attrs_to_update: set[EntityAttr],
) -> None:
    loaders = get_new_context()
    finding = await get_finding(loaders, finding_id)
    indicator_functions: dict[EntityAttr, partial] = {
        EntityAttr.max_open_severity_score: partial(
            findings_domain.get_max_open_severity_score,
            loaders,
            finding.id,
        ),
        EntityAttr.max_open_severity_score_v4: partial(
            findings_domain.get_max_open_severity_score_v4,
            loaders,
            finding.id,
        ),
        EntityAttr.max_open_root_criticality: partial(
            findings_domain.get_max_open_root_criticality,
            loaders,
            finding.id,
        ),
        EntityAttr.newest_vulnerability_report_date: partial(
            findings_domain.get_newest_vulnerability_report_date,
            loaders,
            finding.id,
        ),
        EntityAttr.oldest_open_vulnerability_report_date: partial(
            findings_domain.get_oldest_open_vulnerability_report_date,
            loaders,
            finding.id,
        ),
        EntityAttr.oldest_vulnerability_report_date: partial(
            findings_domain.get_oldest_vulnerability_report_date,
            loaders,
            finding.id,
        ),
        EntityAttr.rejected_vulnerabilities: partial(
            findings_domain.get_rejected_vulnerabilities,
            loaders,
            finding.id,
        ),
        EntityAttr.status: partial(
            findings_domain.get_status,
            loaders,
            finding.id,
        ),
        EntityAttr.submitted_vulnerabilities: partial(
            findings_domain.get_submitted_vulnerabilities,
            loaders,
            finding.id,
        ),
        EntityAttr.total_open_cvssf: partial(
            findings_domain.get_total_open_cvssf,
            loaders,
            finding.id,
        ),
        EntityAttr.total_open_cvssf_v4: partial(
            findings_domain.get_total_open_cvssf_v4,
            loaders,
            finding.id,
        ),
        EntityAttr.treatment_summary: partial(
            findings_domain.get_treatment_summary,
            loaders,
            finding.id,
        ),
        EntityAttr.vulnerabilities_summary: partial(
            findings_domain.get_vulnerabilities_summary,
            loaders,
            finding.id,
        ),
        EntityAttr.where: partial(
            findings_domain.get_where,
            loaders,
            finding.id,
        ),
        EntityAttr.zero_risk_summary: partial(
            findings_domain.get_zero_risk_summary,
            loaders,
            finding.id,
        ),
    }
    indicators: dict[EntityAttr, Any] = {
        indicator: indicator_function()
        for indicator, indicator_function in indicator_functions.items()
        if indicator in attrs_to_update
    }
    result = dict(zip(indicators.keys(), await collect(indicators.values()), strict=False))
    indicators_to_update = FindingUnreliableIndicatorsToUpdate(
        unreliable_newest_vulnerability_report_date=result.get(
            EntityAttr.newest_vulnerability_report_date,
        ),
        unreliable_oldest_open_vulnerability_report_date=result.get(
            EntityAttr.oldest_open_vulnerability_report_date,
        ),
        unreliable_oldest_vulnerability_report_date=result.get(
            EntityAttr.oldest_vulnerability_report_date,
        ),
        unreliable_status=_format_unreliable_status(result.get(EntityAttr.status)),
        max_open_severity_score=result.get(EntityAttr.max_open_severity_score),
        max_open_severity_score_v4=result.get(EntityAttr.max_open_severity_score_v4),
        total_open_cvssf=result.get(EntityAttr.total_open_cvssf),
        total_open_cvssf_v4=result.get(EntityAttr.total_open_cvssf_v4),
        max_open_root_criticality=result.get(EntityAttr.max_open_root_criticality),
        unreliable_where=result.get(EntityAttr.where),
        unreliable_zero_risk_summary=cast(
            FindingZeroRiskSummary, result.get(EntityAttr.zero_risk_summary)
        ),
        clean_unreliable_newest_vulnerability_report_date=(
            EntityAttr.newest_vulnerability_report_date in result
            and result[EntityAttr.newest_vulnerability_report_date] is None
        ),
        clean_unreliable_oldest_open_vulnerability_report_date=(
            EntityAttr.oldest_open_vulnerability_report_date in result
            and result[EntityAttr.oldest_open_vulnerability_report_date] is None
        ),
        clean_unreliable_oldest_vulnerability_report_date=(
            EntityAttr.oldest_vulnerability_report_date in result
            and result[EntityAttr.oldest_vulnerability_report_date] is None
        ),
        vulnerabilities_summary=result.get(EntityAttr.vulnerabilities_summary),
    )
    await findings_model.update_unreliable_indicators(
        current_value=finding.unreliable_indicators,
        group_name=finding.group_name,
        finding_id=finding.id,
        indicators=indicators_to_update,
    )


async def update_vulnerabilities_unreliable_indicators(
    vulnerability_ids: list[str],
    attrs_to_update: set[EntityAttr],
) -> None:
    # Placed the loader here since the same finding_historic_verification is
    # shared by many vulnerabilities
    loaders = get_new_context()
    await collect(
        tuple(
            update_vulnerability_unreliable_indicators(
                loaders,
                vulnerability_id,
                attrs_to_update,
            )
            for vulnerability_id in set(vulnerability_ids)
        ),
        workers=32,
    )


async def update_root_unreliable_indicators(
    loaders: Dataloaders,
    group_name: str,
    root_id: str,
    attrs_to_update: set[EntityAttr],
) -> None:
    root = await roots_utils.get_root(loaders, root_id, group_name)
    indicators: dict[EntityAttr, Any] = {}

    if EntityAttr.last_status_update in attrs_to_update:
        indicators[EntityAttr.last_status_update] = roots_update.get_last_status_update_date(
            loaders,
            root.id,
            root.group_name,
        )

    result = dict(zip(indicators.keys(), await collect(indicators.values()), strict=False))

    with suppress(IndicatorAlreadyUpdated):
        await roots_model.update_unreliable_indicators(
            current_value=root.unreliable_indicators,
            group_name=group_name,
            indicators=RootUnreliableIndicatorsToUpdate(
                unreliable_last_status_update=result.get(EntityAttr.last_status_update),
            ),
            root_id=root_id,
        )


async def update_roots_unreliable_indicators(
    root_ids: list[tuple[str, str]],
    attrs_to_update: set[EntityAttr],
) -> None:
    loaders = get_new_context()
    await collect(
        tuple(
            update_root_unreliable_indicators(
                loaders,
                group_name,
                root_id,
                attrs_to_update,
            )
            for group_name, root_id in set(root_ids)
        ),
    )


@retry_on_exceptions(
    exceptions=(
        IndicatorAlreadyUpdated,
        UnavailabilityError,
    ),
    max_attempts=20,
    sleep_seconds=float("0.2"),
)
async def update_vulnerability_unreliable_indicators(
    loaders: Dataloaders,
    vulnerability_id: str,
    attrs_to_update: set[EntityAttr],
) -> None:
    try:
        vulnerability = await vulns_domain.get_vulnerability(loaders, vulnerability_id)
    except VulnNotFound as exc:
        LOGGER.exception(exc, extra={"extra": {"vulnerability_id": vulnerability_id}})
        return

    indicators = _get_new_vulnerability_indicators(loaders, attrs_to_update, vulnerability)
    result = dict(zip(indicators.keys(), await collect(indicators.values()), strict=False))
    indicators_to_update = VulnerabilityUnreliableIndicatorsToUpdate(
        unreliable_closing_date=result.get(EntityAttr.closing_date),
        unreliable_efficacy=result.get(EntityAttr.efficacy),
        unreliable_last_reattack_date=result.get(EntityAttr.last_reattack_date),
        unreliable_last_reattack_requester=result.get(EntityAttr.last_reattack_requester),
        unreliable_last_requested_reattack_date=result.get(EntityAttr.last_requested_reattack_date),
        unreliable_reattack_cycles=result.get(EntityAttr.reattack_cycles),
        unreliable_report_date=result.get(EntityAttr.report_date),
        unreliable_treatment_changes=result.get(EntityAttr.treatment_changes),
        unreliable_priority=result.get(EntityAttr.priority),
        clean_unreliable_closing_date=(
            EntityAttr.closing_date in result and result[EntityAttr.closing_date] is None
        ),
        clean_unreliable_last_reattack_date=(
            EntityAttr.last_reattack_date in result
            and result[EntityAttr.last_reattack_date] is None
        ),
        clean_unreliable_last_requested_reattack_date=(
            EntityAttr.last_requested_reattack_date in result
            and result[EntityAttr.last_requested_reattack_date] is None
        ),
    )

    await vulns_model.update_unreliable_indicators(
        current_value=vulnerability,
        indicators=indicators_to_update,
    )


def _get_new_vulnerability_indicators(
    loaders: Dataloaders,
    attrs_to_update: set[EntityAttr],
    vulnerability: Vulnerability,
) -> dict[EntityAttr, Any]:
    indicators: dict[EntityAttr, Any] = {}
    if EntityAttr.closing_date in attrs_to_update:
        indicators[EntityAttr.closing_date] = vulns_domain.get_closing_date(vulnerability)

    if EntityAttr.efficacy in attrs_to_update:
        indicators[EntityAttr.efficacy] = vulns_domain.get_efficacy(loaders, vulnerability)

    if EntityAttr.last_reattack_date in attrs_to_update:
        indicators[EntityAttr.last_reattack_date] = vulns_domain.get_last_reattack_date(
            loaders,
            vulnerability,
        )

    if EntityAttr.last_reattack_requester in attrs_to_update:
        indicators[EntityAttr.last_reattack_requester] = vulns_domain.get_reattack_requester(
            loaders,
            vulnerability,
        )

    if EntityAttr.last_requested_reattack_date in attrs_to_update:
        indicators[EntityAttr.last_requested_reattack_date] = (
            vulns_domain.get_last_requested_reattack_date(loaders, vulnerability)
        )

    if EntityAttr.reattack_cycles in attrs_to_update:
        indicators[EntityAttr.reattack_cycles] = vulns_domain.get_reattack_cycles(
            loaders,
            vulnerability,
        )

    if EntityAttr.report_date in attrs_to_update:
        indicators[EntityAttr.report_date] = vulns_domain.get_report_date(vulnerability)

    if EntityAttr.treatment_changes in attrs_to_update:
        indicators[EntityAttr.treatment_changes] = vulns_domain.get_treatment_changes(
            loaders,
            vulnerability,
        )

    if EntityAttr.priority in attrs_to_update:
        indicators[EntityAttr.priority] = vulns_utils.get_priority_value(loaders, vulnerability)

    return indicators


async def _collect_handling_errors(
    awaitables: Iterable[Awaitable[Any]],
    error_message: str,
) -> None:
    """Temporal method for using collect function but handling errors"""
    for item in resolve(awaitables, workers=1024):
        try:
            await item
        except Exception as error:
            error_body = {
                "type": type(error).__name__,
                "message": f"{error}",
                "cause": error.__cause__,
                "context": error.__context__,
            }
            extra = {"extra": error_body}
            LOGGER.error(error_message, extra=extra)


async def update_unreliable_indicators_by_deps(
    dependency: EntityDependency,
    **args: list[Any],
) -> None:
    entities_to_update = unreliable_indicators_model.get_entities_to_update_by_dependency(
        dependency,
        **args,
    )
    updates = []
    dependent_updates = []

    if Entity.finding in entities_to_update:
        dependent_updates.append(
            update_findings_unreliable_indicators(
                entities_to_update[Entity.finding].entity_ids[EntityId.ids],
                entities_to_update[Entity.finding].attributes_to_update,
            ),
        )

    if Entity.root in entities_to_update:
        updates.append(
            update_roots_unreliable_indicators(
                cast(
                    list[tuple[str, str]],
                    entities_to_update[Entity.root].entity_ids[EntityId.ids],
                ),
                entities_to_update[Entity.root].attributes_to_update,
            ),
        )

    if Entity.vulnerability in entities_to_update:
        updates.append(
            update_vulnerabilities_unreliable_indicators(
                entities_to_update[Entity.vulnerability].entity_ids[EntityId.ids],
                entities_to_update[Entity.vulnerability].attributes_to_update,
            ),
        )

    await _collect_handling_errors(
        awaitables=updates,
        error_message="Updating unreliable indicators failed",
    )
    await _collect_handling_errors(
        awaitables=dependent_updates,
        error_message="Updating unreliable indicators failed",
    )
