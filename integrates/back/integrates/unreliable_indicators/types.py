import datetime
from decimal import (
    Decimal,
)
from typing import (
    Any,
    NamedTuple,
    TypedDict,
)

from integrates.db_model.types import (
    Treatment,
)
from integrates.db_model.vulnerabilities.types import (
    VulnerabilityState,
)
from integrates.unreliable_indicators.enums import (
    EntityAttr,
    EntityId,
)


class EntityToUpdate(NamedTuple):
    entity_ids: dict[EntityId, list[Any]]
    attributes_to_update: set[EntityAttr]


class DataRequiredToCalculateIndicators(TypedDict):
    vulnerabilities_severity: list[Decimal]
    historic_states: list[list[VulnerabilityState]]
    historic_treatments: list[list[Treatment]]
    reports_dates: tuple[datetime.datetime, ...]
