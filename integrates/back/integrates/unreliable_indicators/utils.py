from calendar import (
    monthrange,
)
from collections import (
    OrderedDict,
)
from collections.abc import (
    Iterable,
)
from datetime import (
    UTC,
    datetime,
)
from decimal import (
    Decimal,
)
from typing import (
    Literal,
    NamedTuple,
    TypedDict,
)

from integrates.custom_utils import (
    cvss as cvss_utils,
)
from integrates.custom_utils import (
    datetime as datetime_utils,
)
from integrates.custom_utils import (
    vulnerabilities as vulns_utils,
)
from integrates.custom_utils.findings import (
    get_group_findings,
)
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.enums import (
    TreatmentStatus,
)
from integrates.db_model.findings.types import (
    Finding,
)
from integrates.db_model.groups.types import (
    Group,
    GroupUnreliableIndicators,
)
from integrates.db_model.types import (
    Treatment,
)
from integrates.db_model.vulnerabilities.enums import (
    VulnerabilityStateStatus,
)
from integrates.db_model.vulnerabilities.types import (
    Vulnerability,
    VulnerabilityRequest,
    VulnerabilityState,
)
from integrates.decorators import (
    retry_on_exceptions,
)
from integrates.dynamodb.exceptions import (
    UnavailabilityError,
)
from integrates.findings import (
    domain as findings_domain,
)
from integrates.groups import (
    domain as groups_domain,
)

from .types import (
    DataRequiredToCalculateIndicators,
)

TReportInterval = Literal["last_30_days", "last_90_days", "since_first_report"]


class VulnerabilityStatusByTimeRange(NamedTuple):
    vulnerabilities: int
    cvssf: Decimal


class VulnerabilitiesStatusByTimeRange(NamedTuple):
    accepted_vulnerabilities: int
    closed_vulnerabilities: int
    found_vulnerabilities: int
    open_vulnerabilities: int
    accepted_cvssf: Decimal
    closed_cvssf: Decimal
    found_cvssf: Decimal
    open_cvssf: Decimal


class RegisterByTime(NamedTuple):
    vulnerabilities: list[list[dict[str, str | Decimal]]]
    vulnerabilities_cvssf: list[list[dict[str, str | Decimal]]]
    exposed_cvssf: list[list[dict[str, str | Decimal]]]
    vulnerabilities_yearly: list[list[dict[str, str | Decimal]]]
    vulnerabilities_cvssf_yearly: list[list[dict[str, str | Decimal]]]
    exposed_cvssf_yearly: list[list[dict[str, str | Decimal]]]


class VulnerabilityStats(TypedDict):
    found: int
    accepted: int
    closed: int
    exposed_cvssf: Decimal
    found_cvssf: Decimal


def create_data_format_chart(
    all_registers: dict[str, dict[str, Decimal]],
) -> list[list[dict[str, str | Decimal]]]:
    result_data = []
    plot_points: dict[str, list[dict[str, str | Decimal]]] = {
        "found": [],
        "closed": [],
        "accepted": [],
        "assumed_closed": [],
        "opened": [],
    }
    for week, dict_status in list(all_registers.items()):
        for key, value in plot_points.items():
            value.append({"x": week, "y": dict_status[key]})
    for _, value in plot_points.items():
        result_data.append(value)

    return result_data


def format_exposed_chart(
    all_registers: dict[str, dict[str, Decimal]],
) -> list[list[dict[str, str | Decimal]]]:
    result_data = []
    plot_points: dict[str, list[dict[str, str | Decimal]]] = {
        "low": [],
        "medium": [],
        "high": [],
        "critical": [],
    }
    for week, dict_status in list(all_registers.items()):
        for key, value in plot_points.items():
            value.append({"x": week, "y": dict_status[key]})
    for key, value in plot_points.items():
        result_data.append(value)

    return result_data


def format_data_chart_yearly(
    all_registers: dict[str, dict[str, Decimal]],
) -> list[list[dict[str, str | Decimal]]]:
    result_data = []
    plot_points: dict[str, list[dict[str, str | Decimal]]] = {
        "found": [],
        "closed": [],
        "accepted": [],
        "assumed_closed": [],
        "opened": [],
    }
    for week, dict_status in list(all_registers.items()):
        for key, value in plot_points.items():
            value.append({"x": datetime_utils.get_yearly(week), "y": dict_status[key]})
    for _, value in plot_points.items():
        result_data.append(list({data["x"]: data for data in value}.values()))

    return result_data


def format_exposed_chart_yearly(
    all_registers: dict[str, dict[str, Decimal]],
) -> list[list[dict[str, str | Decimal]]]:
    result_data = []
    plot_points: dict[str, list[dict[str, str | Decimal]]] = {
        "low": [],
        "medium": [],
        "high": [],
        "critical": [],
    }
    for week, dict_status in list(all_registers.items()):
        for key, value in plot_points.items():
            value.append({"x": datetime_utils.get_yearly(week), "y": dict_status[key]})
    for key, value in plot_points.items():
        result_data.append(list({data["x"]: data for data in value}.values()))

    return result_data


def _find_minimum_date(
    reports_dates: Iterable[datetime],
    interval: TReportInterval,
) -> datetime:
    delta_map: dict[TReportInterval, int] = {
        "last_30_days": 30,
        "last_90_days": 90,
    }

    min_date = (
        min(reports_dates)
        if interval == "since_first_report"
        else datetime.combine(
            datetime_utils.get_now_minus_delta(
                days=delta_map[interval],
            ),
            datetime.min.time(),
        ).astimezone(tz=UTC)
    )

    return min_date


def _get_weekly_reports(
    *,
    vulnerabilities: Iterable[Vulnerability],
    vulnerabilities_severity: Iterable[Decimal],
    historic_states: Iterable[Iterable[VulnerabilityState]],
    historic_treatments: Iterable[Iterable[Treatment]],
    min_date: datetime,
) -> tuple[
    dict[str, dict[str, Decimal]],
    dict[str, dict[str, Decimal]],
    dict[str, dict[str, Decimal]],
]:
    vulnerability_stats: VulnerabilityStats = {
        "found": 0,
        "accepted": 0,
        "closed": 0,
        "exposed_cvssf": Decimal(0.0),
        "found_cvssf": Decimal(0.0),
    }

    all_registers: dict[str, dict[str, Decimal]] = OrderedDict()
    all_registers_cvsff: dict[str, dict[str, Decimal]] = OrderedDict()
    all_registers_exposed_cvsff: dict[str, dict[str, Decimal]] = OrderedDict()

    first_day, last_day = vulns_utils.get_first_week_dates(min_date)
    first_day_last_week = vulns_utils.get_date_last_vulns(vulnerabilities)

    while first_day <= first_day_last_week:
        result_vulns_by_week: VulnerabilitiesStatusByTimeRange = get_status_vulns_by_time_range(
            vulnerabilities=vulnerabilities,
            vulnerabilities_severity=vulnerabilities_severity,
            vulnerabilities_historic_states=historic_states,
            vulnerabilities_historic_treatments=historic_treatments,
            first_day=first_day,
            last_day=last_day,
            min_date=min_date,
        )
        result_cvssf_by_week = cvss_utils.get_exposed_cvssf_by_time_range(
            vulnerabilities_severity=vulnerabilities_severity,
            vulnerabilities_historic_states=historic_states,
            last_day=last_day,
        )
        vulnerability_stats["found"] += result_vulns_by_week.found_vulnerabilities
        vulnerability_stats["found_cvssf"] += result_vulns_by_week.found_cvssf
        week_dates = datetime_utils.create_weekly_date(first_day)
        if any(
            [
                result_vulns_by_week.found_vulnerabilities,
                vulnerability_stats["accepted"] != result_vulns_by_week.accepted_vulnerabilities,
                vulnerability_stats["closed"] != result_vulns_by_week.closed_vulnerabilities,
            ],
        ):
            all_registers[week_dates] = {
                "found": Decimal(vulnerability_stats["found"]),
                "closed": Decimal(result_vulns_by_week.closed_vulnerabilities),
                "accepted": Decimal(result_vulns_by_week.accepted_vulnerabilities),
                "assumed_closed": Decimal(
                    result_vulns_by_week.accepted_vulnerabilities
                    + result_vulns_by_week.closed_vulnerabilities,
                ),
                "opened": Decimal(result_vulns_by_week.open_vulnerabilities),
            }
            all_registers_cvsff[week_dates] = {
                "found": vulnerability_stats["found_cvssf"].quantize(Decimal("0.1")),
                "closed": result_vulns_by_week.closed_cvssf.quantize(Decimal("0.1")),
                "accepted": result_vulns_by_week.accepted_cvssf.quantize(Decimal("0.1")),
                "assumed_closed": (
                    result_vulns_by_week.accepted_cvssf + result_vulns_by_week.closed_cvssf
                ).quantize(Decimal("0.1")),
                "opened": result_vulns_by_week.open_cvssf.quantize(Decimal("0.1")),
            }
        if vulnerability_stats["exposed_cvssf"] != (
            result_cvssf_by_week.low
            + result_cvssf_by_week.medium
            + result_cvssf_by_week.high
            + result_cvssf_by_week.critical
        ):
            all_registers_exposed_cvsff[week_dates] = {
                "low": result_cvssf_by_week.low.quantize(Decimal("0.1")),
                "medium": result_cvssf_by_week.medium.quantize(Decimal("0.1")),
                "high": result_cvssf_by_week.high.quantize(Decimal("0.1")),
                "critical": result_cvssf_by_week.critical.quantize(Decimal("0.1")),
            }

        vulnerability_stats["exposed_cvssf"] = (
            result_cvssf_by_week.low
            + result_cvssf_by_week.medium
            + result_cvssf_by_week.high
            + result_cvssf_by_week.critical
        )
        vulnerability_stats["accepted"] = result_vulns_by_week.accepted_vulnerabilities
        vulnerability_stats["closed"] = result_vulns_by_week.closed_vulnerabilities
        first_day = datetime_utils.get_plus_delta(first_day, days=7)
        last_day = datetime_utils.get_plus_delta(last_day, days=7)
    return all_registers, all_registers_cvsff, all_registers_exposed_cvsff


async def _get_data_required_to_calculate_indicators(
    *,
    vulnerabilities: Iterable[Vulnerability],
    findings: Iterable[Finding],
    loaders: Dataloaders,
) -> DataRequiredToCalculateIndicators:
    reports_dates = tuple(
        vuln.unreliable_indicators.unreliable_report_date
        for vuln in vulnerabilities
        if vuln.unreliable_indicators.unreliable_report_date
    )

    if not reports_dates:
        return DataRequiredToCalculateIndicators(
            vulnerabilities_severity=[],
            historic_states=[],
            historic_treatments=[],
            reports_dates=reports_dates,
        )

    vulnerabilities_severity = [
        vulns_utils.get_severity_threat_score(
            vulnerability,
            next(finding for finding in findings if finding.id == vulnerability.finding_id),
        )
        for vulnerability in vulnerabilities
    ]

    vulnerability_requests = [
        VulnerabilityRequest(vulnerability_id=vuln.id, finding_id=vuln.finding_id)
        for vuln in vulnerabilities
    ]
    historic_states = await loaders.vulnerability_historic_state.load_many(vulnerability_requests)
    historic_treatments = await loaders.vulnerability_historic_treatment.load_many(
        vulnerability_requests,
    )

    return DataRequiredToCalculateIndicators(
        vulnerabilities_severity=vulnerabilities_severity,
        historic_states=historic_states,
        historic_treatments=historic_treatments,
        reports_dates=reports_dates,
    )


def _process_weekly_registers(
    *,
    vulnerabilities: Iterable[Vulnerability],
    historic_states: Iterable[Iterable[VulnerabilityState]],
    historic_treatments: Iterable[Iterable[Treatment]],
    vulnerabilities_severity: list[Decimal],
    interval: TReportInterval,
    reports_dates: tuple[datetime, ...],
) -> RegisterByTime:
    if not reports_dates:
        return RegisterByTime(
            vulnerabilities=[],
            vulnerabilities_cvssf=[],
            exposed_cvssf=[],
            vulnerabilities_yearly=[],
            vulnerabilities_cvssf_yearly=[],
            exposed_cvssf_yearly=[],
        )

    min_date = _find_minimum_date(reports_dates=reports_dates, interval=interval)

    (
        all_registers,
        all_registers_cvsff,
        all_registers_exposed_cvsff,
    ) = _get_weekly_reports(
        vulnerabilities=vulnerabilities,
        vulnerabilities_severity=vulnerabilities_severity,
        historic_states=historic_states,
        historic_treatments=historic_treatments,
        min_date=min_date,
    )

    return RegisterByTime(
        vulnerabilities=create_data_format_chart(all_registers),
        vulnerabilities_cvssf=create_data_format_chart(all_registers_cvsff),
        exposed_cvssf=format_exposed_chart(all_registers_exposed_cvsff),
        vulnerabilities_yearly=[],
        vulnerabilities_cvssf_yearly=[],
        exposed_cvssf_yearly=[],
    )


def _process_monthly_registers(
    *,
    vulnerabilities: Iterable[Vulnerability],
    vulnerabilities_severity: Iterable[Decimal],
    historic_states: Iterable[Iterable[VulnerabilityState]],
    historic_treatments: Iterable[Iterable[Treatment]],
) -> RegisterByTime:
    vulnerability_stats: VulnerabilityStats = {
        "found": 0,
        "accepted": 0,
        "closed": 0,
        "exposed_cvssf": Decimal(0.0),
        "found_cvssf": Decimal(0.0),
    }
    all_registers: dict[str, dict[str, Decimal]] = OrderedDict()
    all_registers_cvsff: dict[str, dict[str, Decimal]] = OrderedDict()
    all_registers_exposed_cvsff: dict[str, dict[str, Decimal]] = OrderedDict()

    first_day, last_day = vulns_utils.get_first_dates(historic_states)
    first_day_last_month = vulns_utils.get_last_vulnerabilities_date(vulnerabilities)
    while first_day <= first_day_last_month:
        result_vulns_by_month: VulnerabilitiesStatusByTimeRange = get_status_vulns_by_time_range(
            vulnerabilities=vulnerabilities,
            vulnerabilities_severity=vulnerabilities_severity,
            vulnerabilities_historic_states=historic_states,
            vulnerabilities_historic_treatments=historic_treatments,
            first_day=first_day,
            last_day=last_day,
            min_date=None,
        )
        result_cvssf_by_month = cvss_utils.get_exposed_cvssf_by_time_range(
            vulnerabilities_severity=vulnerabilities_severity,
            vulnerabilities_historic_states=historic_states,
            last_day=last_day,
        )
        vulnerability_stats["found"] += result_vulns_by_month.found_vulnerabilities
        vulnerability_stats["found_cvssf"] += result_vulns_by_month.found_cvssf
        month_dates = datetime_utils.create_date(first_day)
        if any(
            [
                result_vulns_by_month.found_vulnerabilities,
                vulnerability_stats["accepted"] != result_vulns_by_month.accepted_vulnerabilities,
                vulnerability_stats["closed"] != result_vulns_by_month.closed_vulnerabilities,
            ],
        ):
            all_registers[month_dates] = {
                "found": Decimal(vulnerability_stats["found"]),
                "closed": Decimal(result_vulns_by_month.closed_vulnerabilities),
                "accepted": Decimal(result_vulns_by_month.accepted_vulnerabilities),
                "assumed_closed": Decimal(
                    result_vulns_by_month.accepted_vulnerabilities
                    + result_vulns_by_month.closed_vulnerabilities,
                ),
                "opened": Decimal(result_vulns_by_month.open_vulnerabilities),
            }
            all_registers_cvsff[month_dates] = {
                "found": vulnerability_stats["found_cvssf"].quantize(Decimal("0.1")),
                "closed": result_vulns_by_month.closed_cvssf.quantize(Decimal("0.1")),
                "accepted": result_vulns_by_month.accepted_cvssf.quantize(Decimal("0.1")),
                "assumed_closed": (
                    result_vulns_by_month.accepted_cvssf + result_vulns_by_month.closed_cvssf
                ).quantize(Decimal("0.1")),
                "opened": result_vulns_by_month.open_cvssf.quantize(Decimal("0.1")),
            }

        if vulnerability_stats["exposed_cvssf"] != (
            result_cvssf_by_month.low
            + result_cvssf_by_month.medium
            + result_cvssf_by_month.high
            + result_cvssf_by_month.critical
        ):
            all_registers_exposed_cvsff[month_dates] = {
                "low": result_cvssf_by_month.low.quantize(Decimal("0.1")),
                "medium": result_cvssf_by_month.medium.quantize(Decimal("0.1")),
                "high": result_cvssf_by_month.high.quantize(Decimal("0.1")),
                "critical": result_cvssf_by_month.critical.quantize(Decimal("0.1")),
            }

        vulnerability_stats["exposed_cvssf"] = (
            result_cvssf_by_month.low
            + result_cvssf_by_month.medium
            + result_cvssf_by_month.high
            + result_cvssf_by_month.critical
        )

        vulnerability_stats["accepted"] = result_vulns_by_month.accepted_vulnerabilities
        vulnerability_stats["closed"] = result_vulns_by_month.closed_vulnerabilities
        first_day = datetime_utils.get_plus_delta(
            first_day,
            days=monthrange(
                int(first_day.strftime("%Y")),
                int(first_day.strftime("%m")),
            )[1],
        )
        last_day_one = datetime_utils.get_plus_delta(last_day, days=1)
        last_day = datetime_utils.get_plus_delta(
            last_day,
            days=monthrange(
                int(last_day_one.strftime("%Y")),
                int(last_day_one.strftime("%m")),
            )[1],
        )
    return RegisterByTime(
        vulnerabilities=create_data_format_chart(all_registers),
        vulnerabilities_cvssf=create_data_format_chart(all_registers_cvsff),
        exposed_cvssf=format_exposed_chart(all_registers_exposed_cvsff),
        vulnerabilities_yearly=format_data_chart_yearly(all_registers),
        vulnerabilities_cvssf_yearly=format_data_chart_yearly(all_registers_cvsff),
        exposed_cvssf_yearly=format_exposed_chart_yearly(all_registers_exposed_cvsff),
    )


def get_accepted_vulns(
    historic_state: Iterable[VulnerabilityState],
    historic_treatment: Iterable[Treatment],
    severity: Decimal,
    last_day: datetime,
    min_date: datetime | None = None,
) -> VulnerabilityStatusByTimeRange:
    accepted_treatments = {
        TreatmentStatus.ACCEPTED,
        TreatmentStatus.ACCEPTED_UNDEFINED,
    }
    treatments = [
        treatment
        for treatment in historic_treatment
        if treatment.modified_date.timestamp() <= last_day.timestamp()
    ]
    if treatments and treatments[-1].status in accepted_treatments:
        return get_by_time_range(
            historic_state,
            VulnerabilityStateStatus.VULNERABLE,
            severity,
            last_day,
            min_date,
        )

    return VulnerabilityStatusByTimeRange(vulnerabilities=0, cvssf=Decimal("0.0"))


def get_open_vulnerabilities(
    *,
    historic_state: Iterable[VulnerabilityState],
    historic_treatment: Iterable[Treatment],
    severity: Decimal,
    last_day: datetime,
    min_date: datetime | None = None,
) -> VulnerabilityStatusByTimeRange:
    accepted_treatments = {
        TreatmentStatus.ACCEPTED,
        TreatmentStatus.ACCEPTED_UNDEFINED,
    }
    treatments = [
        treatment
        for treatment in historic_treatment
        if treatment.modified_date.timestamp() <= last_day.timestamp()
    ]
    states = [
        state for state in historic_state if state.modified_date.timestamp() <= last_day.timestamp()
    ]
    if (
        states
        and states[-1].modified_date.timestamp() <= last_day.timestamp()
        and states[-1].status == VulnerabilityStateStatus.VULNERABLE
        and not (
            min_date and list(historic_state)[0].modified_date.timestamp() < min_date.timestamp()
        )
    ):
        if treatments and treatments[-1].status in accepted_treatments:
            return VulnerabilityStatusByTimeRange(vulnerabilities=0, cvssf=Decimal("0.0"))

        return VulnerabilityStatusByTimeRange(
            vulnerabilities=1,
            cvssf=cvss_utils.get_cvssf_score(severity),
        )

    return VulnerabilityStatusByTimeRange(vulnerabilities=0, cvssf=Decimal("0.0"))


def get_by_time_range(
    historic_state: Iterable[VulnerabilityState],
    status: VulnerabilityStateStatus,
    severity: Decimal,
    last_day: datetime,
    min_date: datetime | None = None,
) -> VulnerabilityStatusByTimeRange:
    states = [
        state for state in historic_state if state.modified_date.timestamp() <= last_day.timestamp()
    ]
    if (
        states
        and states[-1].modified_date.timestamp() <= last_day.timestamp()
        and states[-1].status == status
        and not (
            min_date and list(historic_state)[0].modified_date.timestamp() < min_date.timestamp()
        )
    ):
        return VulnerabilityStatusByTimeRange(
            vulnerabilities=1,
            cvssf=cvss_utils.get_cvssf_score(severity),
        )

    return VulnerabilityStatusByTimeRange(vulnerabilities=0, cvssf=Decimal("0.0"))


def get_status_vulns_by_time_range(
    *,
    vulnerabilities: Iterable[Vulnerability],
    vulnerabilities_severity: Iterable[Decimal],
    vulnerabilities_historic_states: Iterable[Iterable[VulnerabilityState]],
    vulnerabilities_historic_treatments: Iterable[Iterable[Treatment]],
    first_day: datetime,
    last_day: datetime,
    min_date: datetime | None = None,
) -> VulnerabilitiesStatusByTimeRange:
    """Get total closed and found vulnerabilities by time range."""
    vulnerabilities_found = [
        get_found_vulnerabilities(vulnerability, severity, first_day, last_day)
        for vulnerability, severity in zip(
            vulnerabilities,
            vulnerabilities_severity,
            strict=False,
        )
    ]
    vulnerabilities_closed = [
        get_by_time_range(
            historic_state,
            VulnerabilityStateStatus.SAFE,
            severity,
            last_day,
            min_date,
        )
        for historic_state, severity in zip(
            vulnerabilities_historic_states,
            vulnerabilities_severity,
            strict=False,
        )
    ]
    vulnerabilities_accepted = [
        get_accepted_vulns(historic_state, historic_treatment, severity, last_day, min_date)
        for historic_state, historic_treatment, severity in zip(
            vulnerabilities_historic_states,
            vulnerabilities_historic_treatments,
            vulnerabilities_severity,
            strict=False,
        )
    ]
    vulnerabilities_open = [
        get_open_vulnerabilities(
            historic_state=historic_state,
            historic_treatment=historic_treatment,
            severity=severity,
            last_day=last_day,
            min_date=min_date,
        )
        for historic_state, historic_treatment, severity in zip(
            vulnerabilities_historic_states,
            vulnerabilities_historic_treatments,
            vulnerabilities_severity,
            strict=False,
        )
    ]

    return VulnerabilitiesStatusByTimeRange(
        found_vulnerabilities=sum(found.vulnerabilities for found in vulnerabilities_found),
        found_cvssf=Decimal(sum(found.cvssf for found in vulnerabilities_found)),
        open_vulnerabilities=sum(
            vulnerability_open.vulnerabilities for vulnerability_open in vulnerabilities_open
        ),
        open_cvssf=Decimal(
            sum(vulnerability_open.cvssf for vulnerability_open in vulnerabilities_open),
        ),
        accepted_vulnerabilities=sum(
            accepted.vulnerabilities for accepted in vulnerabilities_accepted
        ),
        accepted_cvssf=Decimal(sum(accepted.cvssf for accepted in vulnerabilities_accepted)),
        closed_vulnerabilities=sum(closed.vulnerabilities for closed in vulnerabilities_closed),
        closed_cvssf=Decimal(sum(closed.cvssf for closed in vulnerabilities_closed)),
    )


def get_found_vulnerabilities(
    vulnerability: Vulnerability,
    severity: Decimal,
    first_day: datetime,
    last_day: datetime,
) -> VulnerabilityStatusByTimeRange:
    found = VulnerabilityStatusByTimeRange(vulnerabilities=0, cvssf=Decimal("0.0"))
    if (
        first_day.timestamp()
        <= vulnerability.state.modified_date.timestamp()
        <= last_day.timestamp()
        and vulns_utils.is_deleted(vulnerability)
    ):
        found = VulnerabilityStatusByTimeRange(
            vulnerabilities=found.vulnerabilities - 1,
            cvssf=found.cvssf + (cvss_utils.get_cvssf_score(severity) * Decimal("-1.0")),
        )
    if (
        vulnerability.unreliable_indicators.unreliable_report_date
        and first_day <= vulnerability.unreliable_indicators.unreliable_report_date <= last_day
    ):
        found = VulnerabilityStatusByTimeRange(
            vulnerabilities=found.vulnerabilities + 1,
            cvssf=found.cvssf + cvss_utils.get_cvssf_score(severity),
        )

    return found


async def _get_remediate_severity(
    loaders: Dataloaders,
    group_name: str,
) -> dict[str, Decimal]:
    mean_remediate = await groups_domain.get_mean_remediate_severity(
        loaders,
        group_name,
        Decimal("0.0"),
        Decimal("10.0"),
    )
    remediate_critical = await groups_domain.get_mean_remediate_severity(
        loaders,
        group_name,
        Decimal("9.0"),
        Decimal("10.0"),
    )
    remediate_high = await groups_domain.get_mean_remediate_severity(
        loaders,
        group_name,
        Decimal("7.0"),
        Decimal("8.9"),
    )
    remediate_medium = await groups_domain.get_mean_remediate_severity(
        loaders,
        group_name,
        Decimal("4.0"),
        Decimal("6.9"),
    )
    remediate_low = await groups_domain.get_mean_remediate_severity(
        loaders,
        group_name,
        Decimal("0.1"),
        Decimal("3.9"),
    )
    return {
        "mean": mean_remediate,
        "critical": remediate_critical,
        "high": remediate_high,
        "medium": remediate_medium,
        "low": remediate_low,
    }


@retry_on_exceptions(
    exceptions=(UnavailabilityError,),
    max_attempts=10,
    sleep_seconds=5,
)
async def _get_indicator_info(
    findings: Iterable[Finding],
    vulnerabilities: Iterable[Vulnerability],
    loaders: Dataloaders,
) -> tuple[RegisterByTime, RegisterByTime, RegisterByTime, RegisterByTime] | None:
    data_required_to_calculate_indicators = await _get_data_required_to_calculate_indicators(
        vulnerabilities=vulnerabilities,
        findings=findings,
        loaders=loaders,
    )
    reports_dates = data_required_to_calculate_indicators["reports_dates"]
    if not reports_dates:
        return None

    vulnerabilities_severity = data_required_to_calculate_indicators["vulnerabilities_severity"]
    historic_states = data_required_to_calculate_indicators["historic_states"]
    historic_treatments = data_required_to_calculate_indicators["historic_treatments"]

    remediated_over_time = _process_weekly_registers(
        historic_states=historic_states,
        historic_treatments=historic_treatments,
        vulnerabilities_severity=vulnerabilities_severity,
        reports_dates=reports_dates,
        vulnerabilities=vulnerabilities,
        interval="since_first_report",
    )
    remediated_over_thirty_days = _process_weekly_registers(
        historic_states=historic_states,
        historic_treatments=historic_treatments,
        vulnerabilities_severity=vulnerabilities_severity,
        reports_dates=reports_dates,
        vulnerabilities=vulnerabilities,
        interval="last_30_days",
    )
    remediated_over_ninety_days = _process_weekly_registers(
        historic_states=historic_states,
        historic_treatments=historic_treatments,
        vulnerabilities_severity=vulnerabilities_severity,
        reports_dates=reports_dates,
        vulnerabilities=vulnerabilities,
        interval="last_90_days",
    )
    over_time_month = _process_monthly_registers(
        vulnerabilities=vulnerabilities,
        vulnerabilities_severity=vulnerabilities_severity,
        historic_states=historic_states,
        historic_treatments=historic_treatments,
    )

    return (
        remediated_over_time,
        remediated_over_thirty_days,
        remediated_over_ninety_days,
        over_time_month,
    )


@retry_on_exceptions(
    exceptions=(UnavailabilityError,),
    max_attempts=5,
    sleep_seconds=3,
)
async def get_group_indicators(
    loaders: Dataloaders,
    group: Group,
) -> GroupUnreliableIndicators:
    current_indicators: GroupUnreliableIndicators = await loaders.group_unreliable_indicators.load(
        group.name,
    )
    findings = await get_group_findings(group_name=group.name, loaders=loaders)
    vulnerabilities = await loaders.finding_vulnerabilities_released_nzr.load_many_chained(
        finding.id for finding in findings
    )

    (
        last_closed_vulnerability_days,
        last_closed_vulnerability,
    ) = await findings_domain.get_last_closed_vulnerability_info(loaders, findings)
    (
        max_open_severity,
        max_open_severity_finding,
    ) = await findings_domain.get_max_open_severity_finding(loaders, findings)

    remediate = await _get_remediate_severity(loaders, group.name)

    indicators = await _get_indicator_info(
        findings=findings,
        vulnerabilities=vulnerabilities,
        loaders=loaders,
    )

    if not indicators:
        return current_indicators

    (
        remediated_over_time,
        remediated_over_thirty_days,
        remediated_over_ninety_days,
        over_time_month,
    ) = indicators

    return GroupUnreliableIndicators(
        closed_vulnerabilities=await groups_domain.get_closed_vulnerabilities(loaders, group.name),
        code_languages=current_indicators.code_languages,
        last_closed_vulnerability_days=last_closed_vulnerability_days,
        last_closed_vulnerability_finding=(
            last_closed_vulnerability.finding_id if last_closed_vulnerability else ""
        ),
        max_open_severity=max_open_severity,
        max_open_severity_finding=max_open_severity_finding.id if max_open_severity_finding else "",
        max_severity=await groups_domain.get_max_severity(loaders, group.name),
        mean_remediate=remediate["mean"],
        open_findings=await groups_domain.get_open_findings(loaders, group.name),
        mean_remediate_critical_severity=remediate["critical"],
        mean_remediate_high_severity=remediate["high"],
        mean_remediate_low_severity=remediate["low"],
        mean_remediate_medium_severity=remediate["medium"],
        open_vulnerabilities=await groups_domain.get_open_vulnerabilities(loaders, group.name),
        remediated_over_time=remediated_over_time.vulnerabilities[-18:],
        remediated_over_time_month=over_time_month.vulnerabilities[-80:],
        remediated_over_time_year=over_time_month.vulnerabilities_yearly[-18:],
        remediated_over_time_cvssf=(remediated_over_time.vulnerabilities_cvssf[-18:]),
        remediated_over_time_month_cvssf=(over_time_month.vulnerabilities_cvssf[-80:]),
        remediated_over_time_year_cvssf=(over_time_month.vulnerabilities_cvssf_yearly[-18:]),
        exposed_over_time_cvssf=remediated_over_time.exposed_cvssf[-18:],
        exposed_over_time_month_cvssf=over_time_month.exposed_cvssf[-80:],
        exposed_over_time_year_cvssf=over_time_month.exposed_cvssf_yearly[-18:],
        remediated_over_time_30=remediated_over_thirty_days.vulnerabilities,
        remediated_over_time_cvssf_30=(remediated_over_thirty_days.vulnerabilities_cvssf),
        remediated_over_time_90=remediated_over_ninety_days.vulnerabilities,
        remediated_over_time_cvssf_90=(remediated_over_ninety_days.vulnerabilities_cvssf),
        treatment_summary=await groups_domain.get_treatment_summary(loaders, group.name),
        unfulfilled_standards=current_indicators.unfulfilled_standards,
    )
