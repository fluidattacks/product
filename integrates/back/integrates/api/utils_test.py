from integrates.api.types import Operation
from integrates.api.utils import get_first_operation
from integrates.testing.utils import parametrize


@parametrize(
    args=["query", "expected_output"],
    cases=[
        [
            "",
            Operation(
                name="GraphQL Operation",
                operation_type="query",
                query="",
                variables={},
            ),
        ],
        [
            "     a     ",
            Operation(
                name="GraphQL Operation",
                operation_type="query",
                query="a",
                variables={},
            ),
        ],
        [
            "@",
            Operation(
                name="GraphQL Operation",
                operation_type="query",
                query="@",
                variables={},
            ),
        ],
        [
            "@ #",
            Operation(
                name="GraphQL Operation",
                operation_type="query",
                query="@ #",
                variables={},
            ),
        ],
        [
            "@ # { a a a }",
            Operation(
                name="GraphQL Operation",
                operation_type="query",
                query="@ # { a a a }",
                variables={},
            ),
        ],
        [
            "queri A {me{userEmail}}",
            Operation(
                name="GraphQL Operation",
                operation_type="query",
                query="queri A {me{userEmail}}",
                variables={},
            ),
        ],
        [
            "{me{userEmail}}",
            Operation(
                name="GraphQL Operation",
                operation_type="query",
                query="{me{userEmail}}",
                variables={},
            ),
        ],
        [
            "query {me{userEmail}}",
            Operation(
                name="GraphQL Operation",
                operation_type="query",
                query="query {me{userEmail}}",
                variables={},
            ),
        ],
        [
            "query A {me{userEmail}}",
            Operation(
                name="A",
                operation_type="query",
                query="query A {me{userEmail}}",
                variables={},
            ),
        ],
        [
            """
            query MyQuery {
                me {
                    userEmail
                }
            }
            """,
            Operation(
                name="MyQuery",
                operation_type="query",
                query="query MyQuery { me { userEmail } }",
                variables={},
            ),
        ],
        [
            """
            query NewQuery {
                me {
                    userEmail
                }
            }
            query NewQuery2 {
                me {
                    userEmail
                }
            }
            """,
            Operation(
                name="NewQuery",
                operation_type="query",
                query=("query NewQuery { me { userEmail } } query NewQuery2 { me { userEmail } }"),
                variables={},
            ),
        ],
        [
            """
            fragment consultFields on Consult {
                content
                created
                email
                fullName
                id
                modified
                parentComment
            }

            query GetFindingConsulting($findingId: String!) {
                finding(identifier: $findingId) {
                consulting {
                    ...consultFields
                }
                id
                }
            }
            """,
            Operation(
                name="GetFindingConsulting",
                operation_type="query",
                query=(
                    "fragment consultFields on Consult { content created email"
                    " fullName id modified parentComment }"
                    " query GetFindingConsulting($findingId: String!) {"
                    " finding(identifier: $findingId) { consulting {"
                    " ...consultFields } id } }"
                ),
                variables={},
            ),
        ],
        [
            """
            mutation GetFindingInfo($findingId: String!, $groupName: String!) {
                finding(identifier: $findingId) {
                    id
                }
            }
            """,
            Operation(
                name="GetFindingInfo",
                operation_type="mutation",
                query=(
                    "mutation GetFindingInfo($findingId: String!, $groupName: "
                    "String!) { finding(identifier: $findingId) { id } }"
                ),
                variables={},
            ),
        ],
        [
            "subscription Check { me { organizations { name } } }",
            Operation(
                name="Check",
                operation_type="subscription",
                query=("subscription Check { me { organizations { name } } }"),
                variables={},
            ),
        ],
    ],
)
def test_get_first_operation(query: str, expected_output: Operation) -> None:
    # Act
    result = get_first_operation(query, None)

    # Assert
    assert result == expected_output
