import json
from typing import (
    Any,
)

from ariadne import (
    ScalarType,
)

JSON_STRING_SCALAR = ScalarType("JSONString")


@JSON_STRING_SCALAR.serializer
def serialize_jsonstring(value: str) -> str:
    return json.dumps(value)


@JSON_STRING_SCALAR.value_parser
def parse_jsonstring_value(value: str) -> str:
    return json.loads(value)


@JSON_STRING_SCALAR.literal_parser  # type: ignore
def parse_jsonstring_literal(ast: Any) -> str:
    value = str(ast.value)
    return json.loads(value)
