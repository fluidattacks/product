import logging
import os
from typing import Any, Literal

from ariadne import load_schema_from_path, make_executable_schema
from ariadne.asgi import GraphQL, WebSocketConnectionError
from ariadne.asgi.handlers import GraphQLHTTPHandler, GraphQLTransportWSHandler
from graphql import ASTValidationRule, DocumentNode, parse
from opentelemetry.trace import get_current_span
from starlette.requests import ClientDisconnect, Request
from starlette.websockets import WebSocket

from integrates.api.extension import LowercaseGroupNameExtension
from integrates.audit import AuditContext, add_audit_context
from integrates.class_types.types import Item
from integrates.custom_exceptions import InvalidAuthorization, StakeholderNotInGroup
from integrates.custom_utils import logs as logs_utils
from integrates.custom_utils.utils import camelcase_to_snakecase
from integrates.dataloaders import apply_context_attrs
from integrates.decorators import resolve_group_name
from integrates.group_access.domain import get_group_access
from integrates.sessions.domain import decode_token, get_jwt_content
from integrates.settings import JWT_COOKIE_NAME
from integrates.settings.logger import LOGGING
from integrates.settings.various import DEBUG
from integrates.telemetry.ariadne import opentelemetry_extension

from .enums import ENUMS
from .explorer import IntegratesAPIExplorer
from .resolvers import TYPES
from .scalars import SCALARS
from .types import Operation, UserMetadata
from .unions import UNIONS
from .utils import get_first_operation
from .validations.characters import validate_characters
from .validations.directives import validate_directives
from .validations.query_breadth import QueryBreadthValidation
from .validations.query_depth import QueryDepthValidation
from .validations.variables_validation import variables_check

logging.config.dictConfig(LOGGING)

# Constants
LOGGER = logging.getLogger(__name__)


async def _get_user_metadata(
    request: Request | WebSocket,
    context: Any,
    operation: Operation,
) -> UserMetadata:
    try:
        formatted_variables = _format_variables(operation.variables)
        user_data = await get_jwt_content(request)
        group_name = await resolve_group_name(context, args=None, kwargs=formatted_variables)
        group_access = (
            await get_group_access(
                loaders=context.loaders,
                group_name=group_name,
                email=user_data["user_email"],
            )
            if group_name
            else None
        )
        roles = (
            [group_access.state.role]
            if group_access and group_access.state and group_access.state.role
            else []
        )
    except (InvalidAuthorization, ClientDisconnect):
        user_data = {"user_email": "unauthenticated"}
        group_name = ""
        roles = []
    except StakeholderNotInGroup:
        roles = []
    return UserMetadata(
        user_agent=request.headers.get("user-agent", ""),
        user_email=user_data["user_email"],
        group_name=group_name,
        roles=roles,
    )


def _format_variables(variables: Item | None) -> Item:
    return (
        {camelcase_to_snakecase(key): value for key, value in variables.items()}
        if variables
        else {}
    )


def _sanitize(variables: Item) -> Item:
    allowed_variables = {
        "accepted_vulnerabilities",
        "email",
        "event_id",
        "evidence_id",
        "finding_id",
        "finding_name",
        "group_name",
        "identifier",
        "nickname",
        "organization_id",
        "organization_name",
        "rejected_vulnerabilities",
        "root_id",
        "url_id",
        "user_email",
        "uuid",
        "vuln_id",
        "vulnerability_ids",
    }
    return {key: value for key, value in variables.items() if key in allowed_variables}


def _get_mechanism(
    operation: Operation,
    request: Request | WebSocket,
) -> Literal["API", "FORCES", "JIRA", "MELTS", "RETRIEVES", "WEB"]:
    user_agent = request.headers.get("user-agent", "")
    if user_agent.startswith("IntegratesJira"):
        return "JIRA"
    if user_agent.startswith("Retrieves") or (
        user_agent.startswith("okhttp") and operation.name.startswith("IntelliJ")
    ):
        return "RETRIEVES"
    # Heuristic as some products doesn't have a specific user-agent
    if user_agent.lower().startswith("python"):
        if operation.name.startswith("Forces"):
            return "FORCES"
        if "query Melts" in operation.query or "mutation Melts" in operation.query:
            return "MELTS"
    if request.headers.get("referer"):
        return "WEB"
    return "API"


async def _log_request(
    request: Request | WebSocket,
    operation: Operation,
    user_metadata: UserMetadata,
) -> None:
    """
    Sends API operation metadata to cloud logging services for
    analytical purposes.
    """
    formatted_variables = _format_variables(operation.variables)
    sanitized_variables = _sanitize(formatted_variables)

    span = get_current_span()
    if span and span.is_recording():
        span.set_attribute("user.email", user_metadata.user_email)
        span.set_attribute("user.roles", user_metadata.roles)
        span.set_attribute("user.context.group_name", user_metadata.group_name)
        span.set_attribute("user_agent.original", user_metadata.user_agent)

    add_audit_context(AuditContext(mechanism=_get_mechanism(operation, request)))
    logs_utils.cloudwatch_log(
        request,
        "API",
        extra={
            "operation_name": operation.name,
            "parameters": sanitized_variables,
            "complete_query": operation.query,
            "user_metadata": user_metadata._asdict(),
        },
    )


def parse_query(_context: Any, data: Item) -> DocumentNode:
    validate_directives(data["query"])
    return parse(data["query"])


def _validate_ws_connection(  # pragma: no cover
    websocket: WebSocket,
    params: dict[str, str],
) -> None:  # pragma: no cover
    token: str | None = None
    if isinstance(params, dict) and (_token := params.get("Authorization")):
        token = token or _token
    if not token and (_auth := websocket.headers.get("Authorization", None)):
        token = token or _auth.replace("Bearer ", "")

    websocket.cookies[JWT_COOKIE_NAME] = websocket.cookies.get(JWT_COOKIE_NAME) or token or ""
    if not websocket.cookies[JWT_COOKIE_NAME]:
        raise WebSocketConnectionError("Missing auth")
    try:
        decode_token(websocket.cookies[JWT_COOKIE_NAME])
    except (InvalidAuthorization, KeyError) as exc:
        raise WebSocketConnectionError("Missing auth") from exc


def get_validation_rules(
    context_value: Any,
    _document: DocumentNode,
    _data: Item,
) -> tuple[type[ASTValidationRule], ...]:
    return (  # type: ignore
        QueryBreadthValidation,
        QueryDepthValidation,
        validate_characters(context_value),
        variables_check(context_value),
    )


API_PATH = os.path.dirname(__file__)
SDL_CONTENT = "\n".join(
    [
        load_schema_from_path(os.path.join(API_PATH, module))
        for module in os.listdir(API_PATH)
        if os.path.isdir(os.path.join(API_PATH, module))
    ],
)
SCHEMA = make_executable_schema(
    SDL_CONTENT,
    *ENUMS,
    *SCALARS,
    *TYPES,
    *UNIONS,
    convert_names_case=True,
)


async def _get_context_value(  # pragma: no cover
    request: Request | WebSocket,
    data: Item,
) -> Request | WebSocket:
    context = apply_context_attrs(request)
    operation = get_first_operation(data.get("query", ""), data.get("variables", {}))
    user_metadata = await _get_user_metadata(request, context, operation)
    decoded_body = getattr(context, "_body", b"").decode("utf-8")

    context.operation = operation  # type: ignore[union-attr]
    context.user_metadata = user_metadata  # type: ignore[union-attr]
    context.decoded_body = decoded_body  # type: ignore[union-attr]

    if (
        isinstance(request, WebSocket)
        and isinstance(data, dict)
        and (token := data.get("Authorization", None))
    ):
        request.cookies[JWT_COOKIE_NAME] = token

    try:
        await _log_request(request, operation, user_metadata)
    except Exception as exc:
        LOGGER.error(
            "Couldn't log API operation",
            extra={
                "extra": {
                    "exception": exc,
                    "request": request.headers,
                    "user_metadata": user_metadata._asdict(),
                }
            },
        )

    return context


def _get_root_span_name(context: Any) -> str:
    operation: Operation = context.operation
    result = f"graphql_{operation.name}"
    return result.replace(" ", "_")


IntegratesAPI = GraphQL(
    context_value=_get_context_value,
    debug=DEBUG,
    explorer=IntegratesAPIExplorer(
        title="API | Fluid Attacks",
        explorer_plugin=True,
    ),
    http_handler=GraphQLHTTPHandler(
        extensions=[
            opentelemetry_extension(root_span_name=_get_root_span_name),
            LowercaseGroupNameExtension,
        ],
    ),
    query_parser=parse_query,
    schema=SCHEMA,
    validation_rules=get_validation_rules,
    websocket_handler=GraphQLTransportWSHandler(on_connect=_validate_ws_connection),
)
