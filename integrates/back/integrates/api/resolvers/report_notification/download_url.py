from datetime import datetime
from pathlib import Path
from urllib.parse import urlparse

from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.context import FI_AWS_S3_MAIN_BUCKET
from integrates.db_model.notifications.types import ReportNotification
from integrates.s3.operations import sign_url

from .schema import (
    REPORT_NOTIFICATION,
)


@REPORT_NOTIFICATION.field("downloadURL")
async def resolve(parent: ReportNotification, _info: GraphQLResolveInfo) -> str | None:
    is_expired = parent.expiration_time is not None and datetime.now() > parent.expiration_time
    if is_expired or parent.s3_file_path is None:
        return None

    is_legacy_url = "amazonaws.com" in parent.s3_file_path
    if is_legacy_url:
        return parent.s3_file_path

    is_sbom_file = "sbom_results" in parent.s3_file_path

    if is_sbom_file:
        simple_file_name = Path(urlparse(parent.s3_file_path).path).name
        return await sign_url(
            file_name=parent.s3_file_path,
            expire_seconds=60,
            bucket="machine.data",
            response_content_disposition=f"attachment; filename={simple_file_name}",
        )

    return await sign_url(
        file_name=parent.s3_file_path,
        expire_seconds=60,
        bucket=FI_AWS_S3_MAIN_BUCKET,
    )
