from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.db_model.toe_inputs.types import (
    ToeInput,
)

from .schema import (
    TOE_INPUT,
)


@TOE_INPUT.field("bePresent")
def resolve(parent: ToeInput, _info: GraphQLResolveInfo, **_kwargs: None) -> bool:
    return parent.state.be_present
