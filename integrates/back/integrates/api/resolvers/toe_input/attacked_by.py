from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.db_model.toe_inputs.types import (
    ToeInput,
)
from integrates.decorators import (
    enforce_group_level_auth_async,
)

from .schema import (
    TOE_INPUT,
)


@TOE_INPUT.field("attackedBy")
@enforce_group_level_auth_async
def resolve(parent: ToeInput, _info: GraphQLResolveInfo, **_kwargs: None) -> str:
    return parent.state.attacked_by if parent.state.attacked_by else ""
