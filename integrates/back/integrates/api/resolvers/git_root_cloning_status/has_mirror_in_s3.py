from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.roots.utils import (
    is_in_s3,
)

from .schema import (
    GIT_ROOT_CLONING_STATUS,
)
from .types import (
    GitRootCloningStatus,
)


@GIT_ROOT_CLONING_STATUS.field("hasMirrorInS3")
async def resolve(parent: GitRootCloningStatus, _info: GraphQLResolveInfo) -> bool:
    return await is_in_s3(parent.group_name, parent.root_nickname)
