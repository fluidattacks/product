from integrates.api.resolvers.group_compliance.types import GroupUnfulfilledStandard
from integrates.api.resolvers.group_compliance.unfulfilled_standards import resolve
from integrates.api.resolvers.types import Requirement
from integrates.db_model.groups.types import GroupUnreliableIndicators, UnfulfilledStandard
from integrates.decorators import enforce_group_level_auth_async
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    GraphQLResolveInfoFaker,
    GroupAccessFaker,
    GroupAccessStateFaker,
    GroupFaker,
    StakeholderFaker,
)
from integrates.testing.mocks import mocks


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            groups=[GroupFaker(name="group2")],
            group_access=[
                GroupAccessFaker(email="user@test.test", state=GroupAccessStateFaker(role="hacker"))
            ],
            stakeholders=[StakeholderFaker(email="user@test.test")],
        )
    )
)
async def test_group_compliance_unfulfilled_standards() -> None:
    info = GraphQLResolveInfoFaker(
        user_email="user@test.test",
        decorators=[[enforce_group_level_auth_async]],
    )
    result = await resolve(
        parent=GroupUnreliableIndicators(
            unfulfilled_standards=[
                UnfulfilledStandard(name="bsimm", unfulfilled_requirements=["155", "159", "273"])
            ],
        ),
        _info=info,
    )
    assert result == [
        GroupUnfulfilledStandard(
            standard_id="bsimm",
            title="BSIMM",
            unfulfilled_requirements=[
                Requirement(
                    id="155",
                    summary="The application code must be free of malicious code.\n",
                    title="Application free of malicious code",
                ),
                Requirement(
                    id="159",
                    summary="The source code must be obfuscated in production environments.\n",
                    title="Obfuscate code",
                ),
                Requirement(
                    id="273",
                    summary="All the workstations in production must have an unalterable security "
                    "suite (Anti-virus, Antispyware, Host Firewall, Host-IDS, Host-IPS).\n",
                    title="Define a fixed security suite",
                ),
            ],
        )
    ]
