from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.api.resolvers.types import (
    Requirement,
)
from integrates.custom_utils.criteria import (
    CRITERIA_COMPLIANCE,
    CRITERIA_REQUIREMENTS,
)
from integrates.db_model.groups.types import (
    GroupUnreliableIndicators,
)

from .schema import (
    GROUP_COMPLIANCE,
)
from .types import (
    GroupUnfulfilledStandard,
)


@GROUP_COMPLIANCE.field("unfulfilledStandards")
async def resolve(
    parent: GroupUnreliableIndicators,
    _info: GraphQLResolveInfo,
    **_kwargs: None,
) -> list[GroupUnfulfilledStandard]:
    return [
        GroupUnfulfilledStandard(
            standard_id=unfulfilled_standard.name,
            title=CRITERIA_COMPLIANCE[unfulfilled_standard.name]["title"],
            unfulfilled_requirements=[
                Requirement(
                    id=requirement_id,
                    summary=CRITERIA_REQUIREMENTS[requirement_id]["en"]["summary"],
                    title=CRITERIA_REQUIREMENTS[requirement_id]["en"]["title"],
                )
                for requirement_id in (unfulfilled_standard.unfulfilled_requirements)
                if requirement_id in CRITERIA_REQUIREMENTS
            ],
        )
        for unfulfilled_standard in parent.unfulfilled_standards or []
        if unfulfilled_standard.name in CRITERIA_COMPLIANCE
    ]
