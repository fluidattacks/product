from typing import (
    NamedTuple,
)

from integrates.api.resolvers.types import (
    Requirement,
)


class GroupUnfulfilledStandard(NamedTuple):
    standard_id: str
    title: str
    unfulfilled_requirements: list[Requirement]
