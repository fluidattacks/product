from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.billing import (
    domain as billing_domain,
)
from integrates.billing.types import (
    Price,
)
from integrates.decorators import (
    require_login,
)

from .schema import (
    BILLING,
)


@BILLING.field("prices")
@require_login
async def resolve(_parent: None, _info: GraphQLResolveInfo, **_kwargs: None) -> dict[str, Price]:
    return await billing_domain.get_prices()
