from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.db_model.toe_packages.types import (
    ToePackage,
    ToePackageCoordinates,
)

from .schema import (
    TOE_PACKAGE,
)


@TOE_PACKAGE.field("locations")
def resolve(
    parent: ToePackage,
    _info: GraphQLResolveInfo,
    **_kwargs: None,
) -> list[ToePackageCoordinates]:
    return parent.locations
