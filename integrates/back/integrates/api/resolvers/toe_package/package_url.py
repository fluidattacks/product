from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.db_model.toe_packages.types import (
    ToePackage,
)

from .schema import (
    TOE_PACKAGE,
)


@TOE_PACKAGE.field("packageUrl")
def resolve(
    parent: ToePackage,
    _info: GraphQLResolveInfo,
    **_kwargs: None,
) -> str | None:
    return parent.package_url
