from ariadne import (
    ObjectType,
)

TOE_PACKAGE = ObjectType("ToePackage")
TOE_PACKAGE.set_alias("advisories", "package_advisories")
