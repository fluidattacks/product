from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.db_model.roots.types import (
    Secret,
)

from .schema import (
    SECRET,
)


@SECRET.field("owner")
def resolve(parent: Secret, _info: GraphQLResolveInfo, **_kwargs: None) -> str:
    return parent.state.owner
