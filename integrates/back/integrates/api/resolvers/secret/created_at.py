from datetime import (
    datetime,
)

from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.db_model.roots.types import (
    Secret,
)

from .schema import (
    SECRET,
)


@SECRET.field("createdAt")
def resolve(parent: Secret, _info: GraphQLResolveInfo, **_kwargs: None) -> datetime | None:
    return parent.created_at
