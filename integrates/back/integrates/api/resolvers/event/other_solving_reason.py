from graphql import (
    GraphQLResolveInfo,
)

from integrates.db_model.events.enums import (
    EventSolutionReason,
    EventStateStatus,
)
from integrates.db_model.events.types import (
    Event,
)

from .schema import (
    EVENT,
)


@EVENT.field("otherSolvingReason")
def resolve(
    parent: Event,
    _info: GraphQLResolveInfo,
    **_kwargs: None,
) -> str | None:
    return (
        parent.state.other
        if parent.state.status == EventStateStatus.SOLVED
        and parent.state.reason == EventSolutionReason.OTHER
        else None
    )
