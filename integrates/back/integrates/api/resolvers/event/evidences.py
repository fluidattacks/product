from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.db_model.events.types import (
    Event,
    EventEvidences,
)

from .schema import (
    EVENT,
)


@EVENT.field("evidences")
def resolve(
    parent: Event,
    _info: GraphQLResolveInfo,
    **_kwargs: None,
) -> EventEvidences:
    evidences = parent.evidences
    return evidences
