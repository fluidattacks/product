from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.class_types.types import (
    Item,
)
from integrates.custom_utils.event_comments import (
    format_event_consulting_resolve,
)
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.events.types import (
    Event,
)
from integrates.event_comments import (
    domain as event_comments_domain,
)
from integrates.sessions import (
    domain as sessions_domain,
)

from .schema import (
    EVENT,
)


@EVENT.field("consulting")
async def resolve(
    parent: Event,
    info: GraphQLResolveInfo,
    **_kwargs: None,
) -> list[Item]:
    loaders: Dataloaders = info.context.loaders
    user_data: dict[str, str] = await sessions_domain.get_jwt_content(info.context)
    event_comments = await event_comments_domain.get_comments(
        loaders,
        parent.group_name,
        parent.id,
        user_data["user_email"],
    )

    return [
        format_event_consulting_resolve(comment, target_email=user_data["user_email"])
        for comment in event_comments
    ]
