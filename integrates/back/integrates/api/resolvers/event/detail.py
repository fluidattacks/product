from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.db_model.events.types import (
    Event,
)

from .schema import (
    EVENT,
)


@EVENT.field("detail")
def resolve(
    parent: Event,
    _info: GraphQLResolveInfo,
    **_kwargs: None,
) -> str:
    detail = parent.description

    return detail
