from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.custom_utils import (
    datetime as datetime_utils,
)
from integrates.db_model.events.enums import (
    EventStateStatus,
)
from integrates.db_model.events.types import (
    Event,
)

from .schema import (
    EVENT,
)


@EVENT.field("closingDate")
def resolve(
    parent: Event,
    _info: GraphQLResolveInfo,
    **_kwargs: None,
) -> str:
    if parent.state.status == EventStateStatus.SOLVED and parent.solving_date:
        return datetime_utils.get_as_str(parent.solving_date)

    return "-"
