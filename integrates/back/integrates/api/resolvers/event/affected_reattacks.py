from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.events.types import (
    Event,
)
from integrates.db_model.vulnerabilities.types import (
    Vulnerability,
)

from .schema import (
    EVENT,
)


@EVENT.field("affectedReattacks")
async def resolve(
    parent: Event,
    info: GraphQLResolveInfo,
    **_kwargs: None,
) -> list[Vulnerability]:
    event_id = parent.id
    loaders: Dataloaders = info.context.loaders

    return await loaders.event_vulnerabilities_loader.load(event_id)
