from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.db_model.toe_ports.types import (
    ToePort,
)
from integrates.decorators import (
    enforce_group_level_auth_async,
)

from .schema import (
    TOE_PORT,
)


@TOE_PORT.field("attackedBy")
@enforce_group_level_auth_async
def resolve(parent: ToePort, _info: GraphQLResolveInfo, **_kwargs: None) -> str | None:
    return parent.state.attacked_by
