from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.db_model.mailmap.types import (
    MailmapEntriesConnection,
)
from integrates.decorators import (
    concurrent_decorators,
    enforce_organization_level_auth_async,
    require_login,
)
from integrates.mailmap import (
    get_mailmap_entries,
)

from .schema import (
    QUERY,
)


@QUERY.field("mailmapEntries")
@concurrent_decorators(
    require_login,
    enforce_organization_level_auth_async,
)
async def resolve(
    _parent: None,
    _info: GraphQLResolveInfo,
    organization_id: str,
    first: int,
    after: str = "",
) -> MailmapEntriesConnection:
    connection = await get_mailmap_entries(
        after=after,
        first=first,
        organization_id=organization_id,
    )
    return connection
