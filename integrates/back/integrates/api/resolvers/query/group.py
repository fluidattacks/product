from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.custom_utils.groups import get_group
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.groups.types import (
    Group,
)
from integrates.decorators import (
    concurrent_decorators,
    enforce_group_level_auth_async,
    require_is_not_under_review,
    require_login,
)

from .schema import (
    QUERY,
)


@QUERY.field("group")
@concurrent_decorators(require_login, enforce_group_level_auth_async, require_is_not_under_review)
async def resolve(_parent: None, info: GraphQLResolveInfo, **kwargs: str) -> Group:
    group_name: str = str(kwargs["group_name"]).lower()
    loaders: Dataloaders = info.context.loaders
    group = await get_group(loaders, group_name.lower())

    return group
