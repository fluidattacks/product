from typing import (
    cast,
)

from graphql import (
    GraphQLResolveInfo,
)

from integrates.api.mutations.payloads.types import (
    SimplePayloadMessage,
)
from integrates.custom_exceptions import (
    AutomaticFixError,
)
from integrates.decorators import (
    concurrent_decorators,
    enforce_group_level_auth_async,
    require_is_not_under_review,
)
from integrates.vulnerabilities.fixes.generate import (
    get_suggested_fix,
)
from integrates.vulnerabilities.fixes.types import (
    SuggestedFixFunction,
)

from .schema import (
    QUERY,
)


@QUERY.field("suggestedFix")
@concurrent_decorators(
    enforce_group_level_auth_async,
    require_is_not_under_review,
)
async def resolve(
    _parent: None,
    info: GraphQLResolveInfo,
    vulnerability_id: str,
    vulnerable_function: str,
    vulnerable_line_content: str,
    vulnerable_code_imports: None | str,
    feature_preview: bool = False,
) -> SimplePayloadMessage:
    try:
        result = cast(
            str | None,
            await get_suggested_fix(
                loaders=info.context.loaders,
                vulnerable_code=SuggestedFixFunction(
                    vulnerability_id=vulnerability_id,
                    code_imports=vulnerable_code_imports,
                    function=vulnerable_function,
                    vulnerable_line_content=vulnerable_line_content,
                ),
                stream=False,
                feature_preview=feature_preview,
            ),
        )
        return SimplePayloadMessage(success=result is not None, message=result or "")

    except Exception as exc:
        raise AutomaticFixError(
            kind="suggested",
            vuln_id=vulnerability_id,
        ) from exc
