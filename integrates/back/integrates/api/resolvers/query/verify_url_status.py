import fluidattacks_core.http
from graphql.type.definition import (
    GraphQLResolveInfo,
)
from requests.exceptions import (
    ConnectionError as RequestConnectionError,
)
from requests.exceptions import (
    ConnectTimeout,
    SSLError,
)

from integrates.decorators import (
    require_login,
    retry_on_exceptions,
)

from .schema import (
    QUERY,
)


async def get_request_data(url: str) -> str | None:
    response = await fluidattacks_core.http.request(url, method="GET")
    if response.status == 200:
        return await response.text()
    return None


@QUERY.field("verifyUrlStatus")
@require_login
@retry_on_exceptions(
    exceptions=(
        RequestConnectionError,
        ConnectTimeout,
        SSLError,
    ),
    sleep_seconds=2,
)
async def resolve(
    _parent: None,
    _info: GraphQLResolveInfo,
    url: str,
    **_kwargs: None,
) -> bool:
    try:
        data = await get_request_data(url)
        return bool(data)
    except RequestConnectionError:
        return False
