from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.custom_utils.criteria import (
    CRITERIA_REQUIREMENTS,
)
from integrates.db_model.criteria_requirements import (
    CriteriaRequirementEdge,
    CriteriaRequirementsConnection,
    format_criteria_requirements,
    prepare_requirements_info,
)
from integrates.decorators import (
    require_login,
)
from integrates.dynamodb.types import (
    PageInfo,
)

from .schema import (
    QUERY,
)


@QUERY.field("criteriaRequirementsConnection")
@require_login
def resolve(
    _parent: None,
    _info: GraphQLResolveInfo,
    first: int | None,
    after: str,
) -> CriteriaRequirementsConnection:
    requirements_info = CRITERIA_REQUIREMENTS
    (
        sliced_vulns,
        end_cursor,
        has_next_page,
    ) = prepare_requirements_info(first=first, after=after, requirements_info=requirements_info)
    edges = tuple(
        CriteriaRequirementEdge(
            cursor=key,
            node=format_criteria_requirements(requirement_number=key, item=item),
        )
        for key, item in sliced_vulns.items()
    )

    return CriteriaRequirementsConnection(
        edges=edges,
        page_info=PageInfo(
            end_cursor=end_cursor,
            has_next_page=has_next_page,
        ),
        total=len(sliced_vulns),
    )
