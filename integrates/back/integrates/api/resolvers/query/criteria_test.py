from integrates.api.resolvers.query.criteria import resolve
from integrates.decorators import (
    require_login,
)
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    EMAIL_FLUIDATTACKS_ADMIN,
    GraphQLResolveInfoFaker,
    GroupAccessFaker,
    GroupAccessStateFaker,
    GroupFaker,
    StakeholderFaker,
)
from integrates.testing.mocks import mocks
from integrates.testing.utils import parametrize

GROUP_NAME = "test-group"


@parametrize(
    args=["user_email"],
    cases=[
        [EMAIL_FLUIDATTACKS_ADMIN],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            groups=[
                GroupFaker(name=GROUP_NAME),
            ],
            group_access=[
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email=EMAIL_FLUIDATTACKS_ADMIN,
                    state=GroupAccessStateFaker(has_access=True, role="admin"),
                ),
            ],
            stakeholders=[
                StakeholderFaker(email=EMAIL_FLUIDATTACKS_ADMIN),
            ],
        ),
    )
)
async def test_criteria(user_email: str) -> None:
    info = GraphQLResolveInfoFaker(
        user_email=user_email,
        decorators=[
            [require_login],
        ],
    )
    result = await resolve(
        None,
        info,
        first=None,
        after="",
    )
    assert result
    assert result.total == 441
    assert result.page_info.has_next_page is False
