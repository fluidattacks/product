from integrates.api.resolvers.query.root import resolve
from integrates.custom_exceptions import RootNotFound
from integrates.decorators import (
    enforce_group_level_auth_async,
    require_attribute_internal,
    require_is_not_under_review,
    require_login,
)
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    GitRootFaker,
    GraphQLResolveInfoFaker,
    GroupAccessFaker,
    GroupAccessStateFaker,
    GroupFaker,
    StakeholderFaker,
)
from integrates.testing.mocks import mocks
from integrates.testing.utils import parametrize, raises

GROUP_NAME = "test-group"


@parametrize(
    args=["email"],
    cases=[
        ["resourcer@gmail.com"],
        ["reviewer@gmail.com"],
        ["group_manager@gmail.com"],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            stakeholders=[
                StakeholderFaker(email="resourcer@gmail.com"),
                StakeholderFaker(email="reviewer@gmail.com"),
                StakeholderFaker(email="group_manager@gmail.com"),
            ],
            groups=[
                GroupFaker(name=GROUP_NAME, organization_id="org1"),
            ],
            group_access=[
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email="resourcer@gmail.com",
                    state=GroupAccessStateFaker(has_access=True, role="resourcer"),
                ),
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email="reviewer@gmail.com",
                    state=GroupAccessStateFaker(has_access=True, role="reviewer"),
                ),
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email="group_manager@gmail.com",
                    state=GroupAccessStateFaker(has_access=True, role="group_manager"),
                ),
            ],
            roots=[
                GitRootFaker(
                    id="test-root-456",
                    group_name=GROUP_NAME,
                    organization_name="org1",
                ),
            ],
        )
    )
)
async def test_get_root_different_roles(email: str) -> None:
    root = await resolve(
        None,
        info=GraphQLResolveInfoFaker(
            user_email=email,
            decorators=[
                [require_login],
                [enforce_group_level_auth_async],
                [require_is_not_under_review],
                [require_attribute_internal, "is_under_review", GROUP_NAME],
            ],
        ),
        group_name=GROUP_NAME,
        root_id="test-root-456",
    )
    assert root.id == "test-root-456"
    assert root.group_name == GROUP_NAME


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            stakeholders=[
                StakeholderFaker(email="admin@gmail.com"),
            ],
            groups=[
                GroupFaker(name=GROUP_NAME),
            ],
            group_access=[
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email="admin@gmail.com",
                    state=GroupAccessStateFaker(has_access=True, role="admin"),
                ),
            ],
        )
    )
)
async def test_get_root_not_found() -> None:
    with raises(RootNotFound):
        await resolve(
            None,
            info=GraphQLResolveInfoFaker(
                user_email="admin@gmail.com",
                decorators=[
                    [require_login],
                    [enforce_group_level_auth_async],
                    [require_is_not_under_review],
                    [require_attribute_internal, "is_under_review", GROUP_NAME],
                ],
            ),
            group_name=GROUP_NAME,
            root_id="non-existent-root",
        )


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            stakeholders=[
                StakeholderFaker(email="admin@gmail.com"),
            ],
            groups=[
                GroupFaker(name=GROUP_NAME),
            ],
            roots=[
                GitRootFaker(id="test-root-789", group_name=GROUP_NAME),
            ],
        )
    )
)
async def test_get_root_no_access() -> None:
    with raises(Exception, match="Access denied"):
        await resolve(
            None,
            info=GraphQLResolveInfoFaker(
                user_email="admin@gmail.com",
                decorators=[
                    [require_login],
                    [enforce_group_level_auth_async],
                    [require_is_not_under_review],
                    [require_attribute_internal, "is_under_review", GROUP_NAME],
                ],
            ),
            group_name=GROUP_NAME,
            root_id="test-root-789",
        )
