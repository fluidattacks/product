from dataclasses import dataclass

import fluidattacks_core.http
from requests.exceptions import (
    ConnectionError as RequestConnectionError,
)

from integrates.api.resolvers.query.verify_url_status import resolve
from integrates.decorators import (
    require_login,
)
from integrates.testing.fakers import GraphQLResolveInfoFaker
from integrates.testing.mocks import Mock, mocks

DEFAULT_DECORATORS = [[require_login]]
ADMIN = "admin@gmail.com"
URL = "https://www.google.com"


@dataclass
class ResponseMock:
    status: int

    async def text(self) -> str:
        return "{}"


async def request_func_mock(url: str, method: str) -> ResponseMock:
    raise RequestConnectionError


@mocks(others=[Mock(fluidattacks_core.http, "request", "async", ResponseMock(200))])
async def test_verity_url_status_resolve() -> None:
    info = GraphQLResolveInfoFaker(user_email=ADMIN, decorators=DEFAULT_DECORATORS)
    assert await resolve(None, info, url=URL)


@mocks(others=[Mock(fluidattacks_core.http, "request", "async", ResponseMock(404))])
async def test_verity_url_status_resolve_false() -> None:
    info = GraphQLResolveInfoFaker(user_email=ADMIN, decorators=DEFAULT_DECORATORS)
    assert await resolve(None, info, url=URL) is False


@mocks(others=[Mock(fluidattacks_core.http, "request", "function", request_func_mock)])
async def test_verity_url_status_resolve_conn_error() -> None:
    info = GraphQLResolveInfoFaker(user_email=ADMIN, decorators=DEFAULT_DECORATORS)
    assert await resolve(None, info, url=URL) is False
