from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.events.types import (
    Event,
    GroupEventsRequest,
)
from integrates.decorators import (
    concurrent_decorators,
    enforce_group_level_auth_async,
    require_asm,
    require_is_not_under_review,
    require_login,
)

from .schema import (
    QUERY,
)


@QUERY.field("events")
@concurrent_decorators(
    require_login,
    enforce_group_level_auth_async,
    require_is_not_under_review,
    require_asm,
)
async def resolve(_parent: None, info: GraphQLResolveInfo, **kwargs: str) -> list[Event]:
    loaders: Dataloaders = info.context.loaders
    group_name = kwargs["group_name"].lower()

    return await loaders.group_events.load(GroupEventsRequest(group_name=group_name))
