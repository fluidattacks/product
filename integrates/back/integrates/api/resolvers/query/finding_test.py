from integrates.api.resolvers.query.finding import resolve
from integrates.decorators import (
    enforce_group_level_auth_async,
    require_attribute,
    require_attribute_internal,
    require_login,
)
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    EMAIL_FLUIDATTACKS_ADMIN,
    EMAIL_FLUIDATTACKS_GROUP_MANAGER,
    EMAIL_FLUIDATTACKS_HACKER,
    EMAIL_FLUIDATTACKS_REATTACKER,
    EMAIL_FLUIDATTACKS_REVIEWER,
    EMAIL_FLUIDATTACKS_USER,
    EMAIL_FLUIDATTACKS_VULNERABILITY_MANAGER,
    FindingFaker,
    GraphQLResolveInfoFaker,
    GroupAccessFaker,
    GroupAccessStateFaker,
    GroupFaker,
    StakeholderFaker,
    random_uuid,
)
from integrates.testing.mocks import mocks
from integrates.testing.utils import parametrize, raises

GROUP_NAME = "test-group"
FINDING_ID = random_uuid()


@parametrize(
    args=["user_email"],
    cases=[
        [EMAIL_FLUIDATTACKS_ADMIN],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            groups=[
                GroupFaker(name=GROUP_NAME),
            ],
            group_access=[
                GroupAccessFaker(
                    email=EMAIL_FLUIDATTACKS_ADMIN,
                    state=GroupAccessStateFaker(has_access=True, role="admin"),
                ),
            ],
            stakeholders=[
                StakeholderFaker(email=EMAIL_FLUIDATTACKS_ADMIN),
            ],
            findings=[FindingFaker(id=FINDING_ID)],
        ),
    )
)
async def test_finding(user_email: str) -> None:
    info = GraphQLResolveInfoFaker(
        user_email=user_email,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_attribute, "has_asm", GROUP_NAME],
            [require_attribute_internal, "is_under_review", GROUP_NAME],
        ],
    )
    result = await resolve(
        _parent=None,
        info=info,
        finding_id=FINDING_ID,
        group_name=GROUP_NAME,
    )
    assert result


@parametrize(
    args=["user_email"],
    cases=[
        [EMAIL_FLUIDATTACKS_HACKER],
        [EMAIL_FLUIDATTACKS_REATTACKER],
        [EMAIL_FLUIDATTACKS_USER],
        [EMAIL_FLUIDATTACKS_GROUP_MANAGER],
        [EMAIL_FLUIDATTACKS_VULNERABILITY_MANAGER],
        [EMAIL_FLUIDATTACKS_REVIEWER],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            groups=[
                GroupFaker(),
            ],
            group_access=[
                GroupAccessFaker(
                    email=EMAIL_FLUIDATTACKS_ADMIN,
                    state=GroupAccessStateFaker(has_access=True, role="admin"),
                ),
            ],
            stakeholders=[
                StakeholderFaker(email=EMAIL_FLUIDATTACKS_ADMIN),
            ],
            findings=[FindingFaker(id=FINDING_ID)],
        ),
    )
)
async def test_finding_access_denied(user_email: str) -> None:
    info = GraphQLResolveInfoFaker(
        user_email=user_email,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_attribute, "has_asm", GROUP_NAME],
            [require_attribute_internal, "is_under_review", GROUP_NAME],
        ],
    )
    with raises(Exception, match="Access denied"):
        await resolve(
            _parent=None,
            info=info,
            finding_id=FINDING_ID,
            group_name=GROUP_NAME,
        )
