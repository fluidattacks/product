from typing import (
    Any,
)

import fluidattacks_core.http
from graphql.type.definition import (
    GraphQLResolveInfo,
)
from requests.exceptions import (
    ConnectionError as RequestConnectionError,
)
from requests.exceptions import (
    ConnectTimeout,
    SSLError,
)

from integrates.custom_utils.environments_validations import (
    validate_role_status,
)
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.decorators import (
    concurrent_decorators,
    enforce_group_level_auth_async,
    require_is_not_under_review,
    require_login,
    retry_on_exceptions,
)

from .schema import (
    QUERY,
)


async def get_request_data(url: str) -> str | None:
    response = await fluidattacks_core.http.request(url, method="GET")
    if response.status == 200:
        return await response.text()
    return None


@QUERY.field("verifyEnvironmentIntegrity")
@concurrent_decorators(
    require_login,
    enforce_group_level_auth_async,
    require_is_not_under_review,
)
@retry_on_exceptions(
    exceptions=(
        RequestConnectionError,
        ConnectTimeout,
        SSLError,
    ),
    sleep_seconds=2,
)
async def resolve(
    _parent: None,
    info: GraphQLResolveInfo,
    group_name: str,
    url: str,
    url_type: str,
    **kwargs: Any,
) -> bool:
    loaders: Dataloaders = info.context.loaders
    cloud_name: str | None = kwargs.get("cloud_name")

    if url_type == "CSPM" and cloud_name:
        secrets = {
            "AZURE_CLIENT_ID": kwargs.get("azure_client_id"),
            "AZURE_CLIENT_SECRET": kwargs.get("azure_client_secret"),
            "AZURE_TENANT_ID": kwargs.get("azure_tenant_id"),
            "GCP_PRIVATE_KEY": kwargs.get("gcp_private_key"),
        }
        return await validate_role_status(loaders, url, cloud_name, group_name, secrets)

    if url_type == "URL":
        try:
            data = await get_request_data(url)
            return bool(data)
        except RequestConnectionError:
            return False
    return False
