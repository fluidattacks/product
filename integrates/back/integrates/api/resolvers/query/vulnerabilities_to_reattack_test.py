from typing import Any

from integrates.api.resolvers.query.vulnerabilities_to_reattack import resolve
from integrates.db_model.vulnerabilities.enums import VulnerabilityVerificationStatus
from integrates.db_model.vulnerabilities.types import VulnerabilityVerification
from integrates.decorators import (
    enforce_user_level_auth_async,
    require_login,
)
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    DATE_2019,
    FindingFaker,
    GraphQLResolveInfoFaker,
    GroupAccessFaker,
    GroupAccessStateFaker,
    GroupFaker,
    OrganizationFaker,
    StakeholderFaker,
    VulnerabilityFaker,
)
from integrates.testing.mocks import mocks
from integrates.testing.utils import parametrize

USER = "admin@gmail.com"
GROUP_NAME_1 = "group1"
GROUP_NAME_2 = "group2"
ORG_ID = "org1"
FINDING_ID_1 = "fin1"
FINDING_ID_2 = "fin2"

DEFAULT_DECORATORS: list[list[Any]] = [
    [require_login],
    [enforce_user_level_auth_async],
]


@parametrize(
    args=["kwargs", "expected_len"],
    cases=[
        [{"group": GROUP_NAME_1}, 1],
        [{}, 3],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id=ORG_ID)],
            groups=[
                GroupFaker(name=GROUP_NAME_1, organization_id=ORG_ID),
                GroupFaker(name=GROUP_NAME_2, organization_id=ORG_ID),
            ],
            stakeholders=[StakeholderFaker(email=USER, role="admin")],
            group_access=[
                GroupAccessFaker(
                    email=USER, group_name=GROUP_NAME_1, state=GroupAccessStateFaker(role="admin")
                )
            ],
            findings=[
                FindingFaker(id=FINDING_ID_1, group_name=GROUP_NAME_1),
                FindingFaker(id=FINDING_ID_2, group_name=GROUP_NAME_2),
            ],
            vulnerabilities=[
                VulnerabilityFaker(
                    id="vul1",
                    group_name=GROUP_NAME_1,
                    finding_id=FINDING_ID_1,
                    verification=VulnerabilityVerification(
                        modified_date=DATE_2019, status=VulnerabilityVerificationStatus.REQUESTED
                    ),
                ),
                VulnerabilityFaker(
                    id="vul2",
                    group_name=GROUP_NAME_2,
                    finding_id=FINDING_ID_2,
                    verification=VulnerabilityVerification(
                        modified_date=DATE_2019, status=VulnerabilityVerificationStatus.REQUESTED
                    ),
                ),
                VulnerabilityFaker(
                    id="vul3",
                    group_name=GROUP_NAME_2,
                    finding_id=FINDING_ID_2,
                    verification=VulnerabilityVerification(
                        modified_date=DATE_2019, status=VulnerabilityVerificationStatus.REQUESTED
                    ),
                ),
            ],
        )
    )
)
async def test_resolve_vulnerabilities_to_reattack(
    kwargs: dict[str, str], expected_len: int
) -> None:
    info = GraphQLResolveInfoFaker(user_email=USER, decorators=DEFAULT_DECORATORS)
    result = await resolve(None, info=info, **kwargs)
    assert len(result) == expected_len
