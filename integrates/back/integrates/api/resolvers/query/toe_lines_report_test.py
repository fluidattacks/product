from integrates.api.resolvers import query
from integrates.api.resolvers.query.toe_lines_report import resolve
from integrates.batch import dal as batch_dal
from integrates.batch.enums import Action
from integrates.custom_exceptions import ReportAlreadyRequested, RequiredNewPhoneNumber
from integrates.dataloaders import Dataloaders, get_new_context
from integrates.decorators import (
    enforce_group_level_auth_async,
    require_attribute_internal,
    require_login,
)
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    BatchProcessingFaker,
    GitRootFaker,
    GitRootStateFaker,
    GraphQLResolveInfoFaker,
    GroupFaker,
    OrganizationFaker,
    StakeholderFaker,
    StakeholderPhoneFaker,
)
from integrates.testing.mocks import Mock, mocks
from integrates.testing.utils import raises

EMAIL_TEST = "admin@fluidattacks.com"
GROUP_NAME = "group1"
ROOT_ID = "root1"


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(id="org1"),
            ],
            stakeholders=[
                StakeholderFaker(email=EMAIL_TEST, role="admin", phone=StakeholderPhoneFaker()),
            ],
            groups=[
                GroupFaker(name=GROUP_NAME, organization_id="org1"),
            ],
            roots=[
                GitRootFaker(
                    group_name=GROUP_NAME,
                    id=ROOT_ID,
                    state=GitRootStateFaker(
                        nickname="back",
                    ),
                ),
            ],
        ),
    ),
    others=[
        Mock(query.toe_lines_report, "check_verification", "async", None),
        Mock(batch_dal.put, "put_action_to_batch", "async", "123456"),
    ],
)
async def test_queue_toe_lines_report() -> None:
    loaders: Dataloaders = get_new_context()
    info = GraphQLResolveInfoFaker(
        user_email=EMAIL_TEST,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_attribute_internal, "is_under_review", GROUP_NAME],
        ],
    )

    result = await resolve(
        _parent=None,
        info=info,
        group_name=GROUP_NAME,
        verification_code="00000",
    )

    all_actions = await batch_dal.get_actions()
    report_action = next(
        action
        for action in all_actions
        if action.entity == GROUP_NAME
        and action.additional_info["report_type"] == "TOE_LINES"
        and EMAIL_TEST in action.subject
    )

    assert result == {"success": True}
    assert report_action
    notifications = await loaders.user_notifications.load(EMAIL_TEST)
    assert len(notifications) == 1


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(id="org1"),
            ],
            stakeholders=[
                StakeholderFaker(email=EMAIL_TEST, role="admin", phone=StakeholderPhoneFaker()),
            ],
            groups=[
                GroupFaker(name=GROUP_NAME, organization_id="org1"),
            ],
            actions=[
                BatchProcessingFaker(
                    action_name=Action.REPORT,
                    subject=EMAIL_TEST,
                    entity=GROUP_NAME,
                    additional_info={
                        "report_type": "TOE_LINES",
                    },
                ),
            ],
        ),
    )
)
async def test_queue_toe_lines_report_duplicate_action() -> None:
    info = GraphQLResolveInfoFaker(
        user_email=EMAIL_TEST,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_attribute_internal, "is_under_review", GROUP_NAME],
        ],
    )
    with raises(ReportAlreadyRequested):
        await resolve(
            _parent=None,
            info=info,
            group_name=GROUP_NAME,
            verification_code="00000",
        )


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(id="org1"),
            ],
            stakeholders=[
                StakeholderFaker(email=EMAIL_TEST, role="admin", phone=None),
            ],
            groups=[
                GroupFaker(name=GROUP_NAME, organization_id="org1"),
            ],
        ),
    )
)
async def test_queue_toe_lines_report_fail() -> None:
    info = GraphQLResolveInfoFaker(
        user_email=EMAIL_TEST,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_attribute_internal, "is_under_review", GROUP_NAME],
        ],
    )
    with raises(RequiredNewPhoneNumber):
        await resolve(
            _parent=None,
            info=info,
            group_name=GROUP_NAME,
            verification_code="00000",
        )
