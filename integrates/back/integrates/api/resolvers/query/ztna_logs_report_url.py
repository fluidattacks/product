from datetime import (
    UTC,
    datetime,
)

from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.custom_exceptions import (
    RequiredNewPhoneNumber,
)
from integrates.custom_utils.datetime import (
    get_utc_now,
)
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.decorators import (
    concurrent_decorators,
    enforce_organization_level_auth_async,
    require_login,
)
from integrates.reports.domain import (
    get_signed_ztna_logs_report_url,
)
from integrates.sessions import (
    domain as sessions_domain,
)
from integrates.stakeholders.utils import (
    get_international_format_phone_number,
)
from integrates.verify import (
    operations as verify_operations,
)

from .schema import (
    QUERY,
)


@QUERY.field("ztnaLogsReportUrl")
@concurrent_decorators(
    require_login,
    enforce_organization_level_auth_async,
)
async def resolve(
    _parent: None,
    info: GraphQLResolveInfo,
    organization_id: str,
    verification_code: str,
    log_type: str,
    date: datetime = get_utc_now(),
    **_kwargs: None,
) -> str:
    loaders: Dataloaders = info.context.loaders
    user_info = await sessions_domain.get_jwt_content(info.context)
    stakeholder_email = user_info["user_email"]
    stakeholder = await loaders.stakeholder.load(stakeholder_email)
    user_phone = stakeholder.phone if stakeholder else None
    if not user_phone:
        raise RequiredNewPhoneNumber()

    await verify_operations.check_verification(
        recipient=get_international_format_phone_number(user_phone),
        code=verification_code,
    )

    return await get_signed_ztna_logs_report_url(
        loaders=loaders,
        organization_id=organization_id,
        email=stakeholder_email,
        log_type=log_type.lower(),
        date=date.astimezone(tz=UTC),
    )
