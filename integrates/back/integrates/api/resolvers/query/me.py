from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.class_types.types import (
    Item,
)
from integrates.custom_utils import (
    datetime as datetime_utils,
)
from integrates.decorators import (
    require_login,
)
from integrates.sessions import (
    domain as sessions_domain,
)

from .schema import (
    QUERY,
)


@QUERY.field("me")
@require_login
async def resolve(_parent: None, info: GraphQLResolveInfo) -> Item:
    user_data: Item = await sessions_domain.get_jwt_content(info.context)
    exp: str = datetime_utils.get_as_utc_iso_format(datetime_utils.get_from_epoch(user_data["exp"]))
    return {
        "session_expiration": exp,
        "user_email": user_data["user_email"],
        "user_name": " ".join([user_data.get("first_name", ""), user_data.get("last_name", "")]),
    }
