import asyncio

from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.db_model.roots.get import (
    get_secret_by_key,
)
from integrates.db_model.roots.types import (
    Secret,
)
from integrates.decorators import (
    concurrent_decorators,
    enforce_group_level_auth_async,
    enforce_owner,
    require_is_not_under_review,
    require_login,
)
from integrates.verify.enums import (
    AuthenticationLevel,
    ResourceType,
)

from .schema import (
    QUERY,
)


@enforce_owner(AuthenticationLevel.GROUP)
async def _resolve_secret(
    secret: Secret,
    _info: GraphQLResolveInfo,
    **_kwargs: str,
) -> Secret:
    await asyncio.sleep(0)
    return secret


@QUERY.field("secret")
@concurrent_decorators(require_login, enforce_group_level_auth_async, require_is_not_under_review)
async def resolve(
    _parent: None,
    info: GraphQLResolveInfo,
    group_name: str,
    secret_key: str,
    resource_id: str,
    resource_type: ResourceType,
    **_kwargs: str,
) -> Secret:
    secret = await get_secret_by_key(
        secret_key=secret_key,
        group_name=group_name,
        resource_id=resource_id,
        resource_type=resource_type,
    )

    authenticated_secret = await _resolve_secret(secret, info, group_name=group_name)

    return authenticated_secret
