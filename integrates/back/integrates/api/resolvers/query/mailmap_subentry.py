from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.db_model.mailmap.types import (
    MailmapSubentry,
)
from integrates.decorators import (
    concurrent_decorators,
    enforce_organization_level_auth_async,
    require_login,
)
from integrates.mailmap import (
    get_mailmap_subentry,
)

from .schema import (
    QUERY,
)


@QUERY.field("mailmapSubentry")
@concurrent_decorators(
    require_login,
    enforce_organization_level_auth_async,
)
async def resolve(
    _parent: None,
    _info: GraphQLResolveInfo,
    entry_email: str,
    subentry_email: str,
    subentry_name: str,
    organization_id: str,
) -> MailmapSubentry:
    subentry = await get_mailmap_subentry(
        entry_email=entry_email,
        subentry_email=subentry_email,
        subentry_name=subentry_name,
        organization_id=organization_id,
    )
    return subentry
