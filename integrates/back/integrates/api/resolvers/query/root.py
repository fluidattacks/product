from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.custom_utils import (
    roots as roots_utils,
)
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.roots.types import (
    Root,
)
from integrates.decorators import (
    concurrent_decorators,
    enforce_group_level_auth_async,
    require_is_not_under_review,
    require_login,
)

from .schema import (
    QUERY,
)


@QUERY.field("root")
@concurrent_decorators(require_login, enforce_group_level_auth_async, require_is_not_under_review)
async def resolve(_parent: None, info: GraphQLResolveInfo, **kwargs: str) -> Root:
    group_name: str = kwargs["group_name"]
    root_id: str = kwargs["root_id"]
    loaders: Dataloaders = info.context.loaders
    root = await roots_utils.get_root(loaders, root_id, group_name.lower())

    return root
