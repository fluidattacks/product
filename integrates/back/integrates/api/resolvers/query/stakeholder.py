from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates import (
    authz,
)
from integrates.custom_exceptions import (
    InvalidParameter,
    StakeholderNotFound,
)
from integrates.custom_utils.stakeholders import (
    get_stakeholder,
)
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.stakeholders.types import (
    Stakeholder,
)
from integrates.decorators import (
    concurrent_decorators,
    enforce_group_level_auth_async,
    enforce_organization_level_auth_async,
    require_is_not_under_review,
    require_login,
)
from integrates.sessions import (
    domain as sessions_domain,
)

from .schema import (
    QUERY,
)


@enforce_organization_level_auth_async
async def _resolve_for_organization(
    *,
    info: GraphQLResolveInfo,
    email: str,
    organization_id: str,
    user_email: str,
) -> Stakeholder:
    loaders: Dataloaders = info.context.loaders
    org_role = await authz.get_organization_level_role(loaders, email, organization_id)
    if not organization_id or not org_role:
        raise StakeholderNotFound()
    return await get_stakeholder(loaders, email, user_email)


@concurrent_decorators(enforce_group_level_auth_async, require_is_not_under_review)
async def _resolve_for_group(
    *,
    info: GraphQLResolveInfo,
    email: str,
    group_name: str,
    user_email: str,
) -> Stakeholder:
    loaders: Dataloaders = info.context.loaders
    group_role = await authz.get_group_level_role(loaders, email, group_name)
    if not group_name or not group_role:
        raise StakeholderNotFound()
    return await get_stakeholder(loaders, email, user_email)


@QUERY.field("stakeholder")
@require_login
async def resolve(_parent: None, info: GraphQLResolveInfo, **kwargs: str) -> Stakeholder:
    request_store = sessions_domain.get_request_store(info.context)
    entity: str = kwargs["entity"]
    request_store["entity"] = entity
    email: str = kwargs["user_email"]
    user_data = await sessions_domain.get_jwt_content(info.context)
    user_email: str = user_data["user_email"]

    if entity == "ORGANIZATION" and "organization_id" in kwargs:
        org_id: str = kwargs["organization_id"]
        request_store["organization_id"] = org_id
        return await _resolve_for_organization(
            info=info,
            email=email,
            organization_id=org_id,
            user_email=user_email,
        )

    if entity == "GROUP" and "group_name" in kwargs:
        group_name: str = kwargs["group_name"]
        request_store["group_name"] = group_name
        return await _resolve_for_group(
            info=info,
            email=email,
            group_name=group_name,
            user_email=user_email,
        )

    raise InvalidParameter()
