from typing import Any

from integrates import authz
from integrates.api.resolvers.query.stakeholder import (
    resolve,
)
from integrates.custom_exceptions import InvalidParameter, StakeholderNotFound
from integrates.decorators import (
    require_attribute_internal,
    require_login,
)
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    GraphQLResolveInfoFaker,
    GroupAccessFaker,
    GroupAccessStateFaker,
    GroupFaker,
    OrganizationAccessFaker,
    OrganizationFaker,
    StakeholderFaker,
)
from integrates.testing.mocks import Mock, mocks
from integrates.testing.utils import parametrize, raises

USER = "user@gmail.com"
ORG_ID = "ORG#40f6da5f-4f66-4bf0-825b-a2d9748ad6db"
GROUP_NAME = "group1"

DEFAULT_DECORATORS: list[list[Any]] = [
    [require_login],
    [require_attribute_internal, "is_under_review", GROUP_NAME],
]


@parametrize(
    args=["kwargs"],
    cases=[
        [
            {
                "entity": "ORGANIZATION",
                "organization_id": ORG_ID,
                "user_email": USER,
            },
        ],
        [
            {"entity": "GROUP", "group_name": GROUP_NAME, "user_email": USER},
        ],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id=ORG_ID)],
            stakeholders=[StakeholderFaker(email=USER)],
            organization_access=[OrganizationAccessFaker(email=USER, organization_id=ORG_ID)],
            groups=[GroupFaker(name=GROUP_NAME)],
            group_access=[
                GroupAccessFaker(
                    email=USER, group_name=GROUP_NAME, state=GroupAccessStateFaker(role="admin")
                )
            ],
        )
    )
)
async def test_resolve_stakeholder(
    kwargs: dict[str, str],
) -> None:
    info = GraphQLResolveInfoFaker(user_email=USER, decorators=DEFAULT_DECORATORS)

    stakeholder = await resolve(None, info=info, **kwargs)
    assert stakeholder


@mocks(aws=IntegratesAws(dynamodb=IntegratesDynamodb(stakeholders=[StakeholderFaker(email=USER)])))
async def test_resolve_stakeholder_invalid_parameter() -> None:
    info = GraphQLResolveInfoFaker(user_email=USER, decorators=DEFAULT_DECORATORS)

    kwargs = {"entity": "INVALID", "user_email": USER}

    with raises(InvalidParameter):
        await resolve(None, info=info, **kwargs)


@parametrize(
    args=["kwargs"],
    cases=[
        [
            {
                "entity": "ORGANIZATION",
                "organization_id": ORG_ID,
                "user_email": USER,
            },
        ],
        [
            {"entity": "GROUP", "group_name": GROUP_NAME, "user_email": USER},
        ],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id=ORG_ID)],
            stakeholders=[StakeholderFaker(email=USER)],
            organization_access=[OrganizationAccessFaker(email=USER, organization_id=ORG_ID)],
            groups=[GroupFaker(name=GROUP_NAME)],
            group_access=[
                GroupAccessFaker(
                    email=USER, group_name=GROUP_NAME, state=GroupAccessStateFaker(role="admin")
                )
            ],
        )
    ),
    others=[
        Mock(authz, "get_organization_level_role", "async", ""),
        Mock(authz, "get_group_level_role", "async", ""),
    ],
)
async def test_resolve_stakeholder_invalid_not_found(kwargs: dict[str, str]) -> None:
    info = GraphQLResolveInfoFaker(user_email=USER, decorators=DEFAULT_DECORATORS)

    with raises(StakeholderNotFound):
        await resolve(None, info=info, **kwargs)
