from integrates.api.resolvers.query.report import resolve
from integrates.batch import dal as batch_dal
from integrates.batch.dal.get import get_actions_by_name
from integrates.batch.enums import Action
from integrates.dataloaders import Dataloaders, get_new_context
from integrates.decorators import (
    enforce_group_level_auth_async,
    require_attribute_internal,
    require_login,
)
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    GitRootFaker,
    GitRootStateFaker,
    GraphQLResolveInfoFaker,
    GroupFaker,
    OrganizationFaker,
    StakeholderFaker,
    StakeholderPhoneFaker,
)
from integrates.testing.mocks import Mock, mocks
from integrates.verify import (
    operations as verify_operations,
)

EMAIL_TEST = "admin@fluidattacks.com"
GROUP_NAME = "group1"
ROOT_ID = "root1"


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(id="org1"),
            ],
            stakeholders=[
                StakeholderFaker(email=EMAIL_TEST, role="admin", phone=StakeholderPhoneFaker()),
            ],
            groups=[
                GroupFaker(name=GROUP_NAME, organization_id="org1"),
            ],
            roots=[
                GitRootFaker(
                    group_name=GROUP_NAME,
                    id=ROOT_ID,
                    state=GitRootStateFaker(
                        nickname="back",
                    ),
                ),
            ],
        ),
    ),
    others=[
        Mock(verify_operations, "check_verification", "async", None),
        Mock(batch_dal.put, "put_action_to_batch", "async", "123456"),
    ],
)
async def test_report() -> None:
    loaders: Dataloaders = get_new_context()
    info = GraphQLResolveInfoFaker(
        user_email=EMAIL_TEST,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_attribute_internal, "is_under_review", GROUP_NAME],
        ],
    )

    result = await resolve(
        _parent=None,
        info=info,
        group_name=GROUP_NAME,
        verification_code="00000",
        report_type="XLS",
    )
    assert result == {"success": True}
    report_action = await get_actions_by_name(action_name=Action.REPORT, entity=GROUP_NAME)

    assert len(report_action) == 1
    notifications = await loaders.user_notifications.load(EMAIL_TEST)
    assert len(notifications) == 1
