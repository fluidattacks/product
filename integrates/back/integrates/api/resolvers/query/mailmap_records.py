from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.db_model.mailmap.types import (
    MailmapRecord,
)
from integrates.decorators import (
    concurrent_decorators,
    enforce_organization_level_auth_async,
    require_login,
)
from integrates.mailmap import (
    get_mailmap_records,
)

from .schema import (
    QUERY,
)


@QUERY.field("mailmapRecords")
@concurrent_decorators(
    require_login,
    enforce_organization_level_auth_async,
)
async def resolve(
    _parent: None,
    _info: GraphQLResolveInfo,
    organization_id: str,
) -> list[MailmapRecord]:
    records = await get_mailmap_records(
        organization_id=organization_id,
    )
    return records
