from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.custom_exceptions import (
    ExecutionNotFound,
)
from integrates.custom_utils.forces import (
    format_forces_to_resolve,
)
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.forces.types import (
    ForcesExecution,
    ForcesExecutionRequest,
)
from integrates.decorators import (
    concurrent_decorators,
    enforce_group_level_auth_async,
    require_asm,
    require_is_not_under_review,
    require_login,
)

from .schema import (
    QUERY,
)


@QUERY.field("forcesExecution")
@concurrent_decorators(
    require_login,
    enforce_group_level_auth_async,
    require_is_not_under_review,
    require_asm,
)
async def resolve(_: None, info: GraphQLResolveInfo, **kwargs: str) -> dict[str, str]:
    execution_id: str = kwargs["execution_id"]
    group_name: str = kwargs["group_name"]
    loaders: Dataloaders = info.context.loaders
    execution: ForcesExecution | None = await loaders.forces_execution.load(
        ForcesExecutionRequest(group_name=group_name, execution_id=execution_id),
    )

    if execution:
        return format_forces_to_resolve(execution=execution)

    raise ExecutionNotFound()
