from collections.abc import AsyncGenerator
from typing import Any

import integrates.vulnerabilities.fixes.generate as fixes_generate
from integrates.api.resolvers.query.suggested_fix import resolve
from integrates.custom_exceptions import AutomaticFixError
from integrates.dataloaders import Dataloaders
from integrates.decorators import (
    enforce_group_level_auth_async,
    require_attribute_internal,
    require_is_not_under_review,
)
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    FindingFaker,
    GitRootFaker,
    GraphQLResolveInfoFaker,
    GroupAccessFaker,
    StakeholderFaker,
    VulnerabilityFaker,
    VulnerabilityStateFaker,
    random_uuid,
)
from integrates.testing.mocks import Mock, mocks
from integrates.testing.utils import raises
from integrates.vulnerabilities.fixes import generate
from integrates.vulnerabilities.fixes.types import ConverseMessage, SuggestedFixFunction

ADMIN = "admin@gmail.com"
VULN_ID = "vuln1"
FINDING_ID = random_uuid()
GROUP_NAME = "group1"
VULN_FUNC = "foo"
VULN_LINE_CONTENT = "out = subprocess_run(args, input=data, capture_output=True)"
ROOT_ID = "root1"

DEFAULT_DECORATORS: list[list[Any]] = [
    [require_is_not_under_review],
    [enforce_group_level_auth_async],
    [require_attribute_internal, "is_under_review", GROUP_NAME],
]


async def mocked_converse(
    messages: tuple[ConverseMessage, ...],
    *,
    feature_preview: bool = False,
    max_tokens: int | None = None,
    temperature: float | None = None,
) -> AsyncGenerator[str, None]:
    yield "Here is your suggested fix..."


async def mocked_suggested_fix_fails(
    *,
    loaders: Dataloaders,
    vulnerable_code: SuggestedFixFunction,
    stream: bool = False,
    feature_preview: bool = False,
) -> str:
    raise Exception("Failed")


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            stakeholders=[StakeholderFaker(email=ADMIN, role="admin")],
            group_access=[GroupAccessFaker(email=ADMIN, group_name=GROUP_NAME)],
            findings=[FindingFaker(id=FINDING_ID, group_name=GROUP_NAME)],
            roots=[GitRootFaker(id=ROOT_ID, group_name=GROUP_NAME)],
            vulnerabilities=[
                VulnerabilityFaker(
                    id=VULN_ID,
                    finding_id=FINDING_ID,
                    group_name=GROUP_NAME,
                    state=VulnerabilityStateFaker(commit="1234"),
                    root_id=ROOT_ID,
                )
            ],
        )
    ),
    others=[Mock(fixes_generate, "converse", "function", mocked_converse)],
)
async def test_resolve_suggested_fix() -> None:
    info = GraphQLResolveInfoFaker(user_email=ADMIN, decorators=DEFAULT_DECORATORS)
    response = await resolve(
        None,
        info=info,
        vulnerability_id=VULN_ID,
        vulnerable_function=VULN_FUNC,
        vulnerable_line_content=VULN_LINE_CONTENT,
        vulnerable_code_imports=None,
    )

    assert response.success


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            stakeholders=[StakeholderFaker(email=ADMIN, role="admin")],
            findings=[FindingFaker(id=FINDING_ID, group_name=GROUP_NAME)],
            vulnerabilities=[
                VulnerabilityFaker(id=VULN_ID, finding_id=FINDING_ID, group_name=GROUP_NAME)
            ],
        )
    ),
    others=[Mock(generate, "get_suggested_fix", "function", mocked_suggested_fix_fails)],
)
async def test_resolve_suggested_fix_exception() -> None:
    info = GraphQLResolveInfoFaker(user_email=ADMIN, decorators=DEFAULT_DECORATORS)
    with raises(AutomaticFixError):
        await resolve(
            None,
            info=info,
            vulnerability_id=VULN_ID,
            vulnerable_function=VULN_FUNC,
            vulnerable_line_content=VULN_LINE_CONTENT,
            vulnerable_code_imports=None,
        )
