import fluidattacks_core.http

from integrates.api.resolvers.query.verify_environment_integrity import resolve
from integrates.db_model.roots.enums import RootEnvironmentCloud, RootEnvironmentUrlType
from integrates.decorators import (
    enforce_group_level_auth_async,
    require_attribute_internal,
    require_login,
)
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    GitRootFaker,
    GraphQLResolveInfoFaker,
    GroupAccessFaker,
    GroupAccessStateFaker,
    GroupFaker,
    RootEnvironmentUrlFaker,
    RootEnvironmentUrlStateFaker,
    StakeholderFaker,
    ToeInputFaker,
    ToeInputStateFaker,
)
from integrates.testing.mocks import Mock, mocks
from integrates.testing.utils import create_fake_client_response, parametrize


@parametrize(
    args=["user_email"],
    cases=[
        ["admin@fluidattacks.com"],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            groups=[GroupFaker()],
            group_access=[
                GroupAccessFaker(
                    email="admin@fluidattacks.com",
                    state=GroupAccessStateFaker(has_access=True, role="admin"),
                ),
            ],
            stakeholders=[StakeholderFaker(email="admin@fluidattacks.com")],
            roots=[GitRootFaker(id="root1")],
            toe_inputs=[
                ToeInputFaker(
                    root_id="root1",
                    component="https://test.com/",
                    entry_point="user",
                    state=ToeInputStateFaker(has_vulnerabilities=True),
                ),
                ToeInputFaker(
                    root_id="root1",
                    component="arn:test",
                    entry_point="user",
                    state=ToeInputStateFaker(has_vulnerabilities=True),
                ),
            ],
            root_environment_urls=[
                RootEnvironmentUrlFaker(
                    id="00fbe3579a253b43239954a545dc0536",
                    url="https://test.com/",
                    root_id="root1",
                    state=RootEnvironmentUrlStateFaker(modified_by="admin@fluidattacks.com"),
                ),
                RootEnvironmentUrlFaker(
                    id="00fbe3579a253b43239954a545dc0537",
                    url="arn:test",
                    root_id="root1",
                    state=RootEnvironmentUrlStateFaker(
                        modified_by="admin@fluidattacks.com",
                        url_type=RootEnvironmentUrlType.CSPM,
                        cloud_name=RootEnvironmentCloud.AWS,
                    ),
                ),
                RootEnvironmentUrlFaker(
                    id="00fbe3579a253b43239954a545dc0538",
                    url="123123123123123123123123123123123123",
                    root_id="root1",
                    state=RootEnvironmentUrlStateFaker(
                        modified_by="admin@fluidattacks.com",
                        url_type=RootEnvironmentUrlType.CSPM,
                        cloud_name=RootEnvironmentCloud.AZURE,
                    ),
                ),
                RootEnvironmentUrlFaker(
                    id="00fbe3579a253b43239954a545dc0539",
                    url="test-group1212",
                    root_id="root1",
                    state=RootEnvironmentUrlStateFaker(
                        modified_by="admin@fluidattacks.com",
                        url_type=RootEnvironmentUrlType.CSPM,
                        cloud_name=RootEnvironmentCloud.GCP,
                    ),
                ),
            ],
        ),
    ),
    others=[
        Mock(
            fluidattacks_core.http,
            "request",
            "async",
            create_fake_client_response(
                status="400",
                json_data={"message": "Error", "error": {"id": 1, "title": "Test Post"}},
            ),
        )
    ],
)
async def test_verify_environment_integrity(user_email: str) -> None:
    # Arrange
    group_name = "test-group"
    info = GraphQLResolveInfoFaker(
        user_email=user_email,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_attribute_internal, "is_under_review", group_name],
        ],
    )

    # Act
    result = await resolve(
        _parent=None,
        info=info,
        url="https://test.com/",
        url_type=RootEnvironmentUrlType.URL,
        cloud_name=None,
        group_name=group_name,
    )
    assert not result

    result = await resolve(
        _parent=None,
        info=info,
        url="arn:test",
        url_type=RootEnvironmentUrlType.CSPM,
        cloud_name=RootEnvironmentCloud.AWS,
        group_name=group_name,
    )
    assert not result

    result = await resolve(
        _parent=None,
        info=info,
        url="123123123123123123123123123123123123",
        url_type=RootEnvironmentUrlType.CSPM,
        cloud_name=RootEnvironmentCloud.AZURE,
        group_name=group_name,
    )
    assert not result

    result = await resolve(
        _parent=None,
        info=info,
        url="test-group1212",
        url_type=RootEnvironmentUrlType.CSPM,
        cloud_name=RootEnvironmentCloud.GCP,
        group_name=group_name,
    )
    assert not result
