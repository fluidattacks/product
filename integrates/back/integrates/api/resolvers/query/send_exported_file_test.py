from typing import Any

from integrates.api.resolvers.query.send_exported_file import resolve
from integrates.batch import (
    dal as batch_dal,
)
from integrates.batch.types import PutActionResult
from integrates.custom_exceptions import ReportAlreadyRequested, RequestedReportError
from integrates.decorators import (
    enforce_group_level_auth_async,
    require_attribute_internal,
    require_is_not_under_review,
    require_login,
)
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    GraphQLResolveInfoFaker,
    GroupAccessFaker,
    GroupAccessStateFaker,
    GroupFaker,
    StakeholderFaker,
)
from integrates.testing.mocks import Mock, mocks
from integrates.testing.utils import raises

GROUP_NAME = "test-group"
USER_EMAIL = "user@gmail.com"

DEFAULT_DECORATORS: list[list[Any]] = [
    [require_login],
    [enforce_group_level_auth_async],
    [require_is_not_under_review],
    [require_attribute_internal, "is_under_review", GROUP_NAME],
]


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            groups=[GroupFaker(name=GROUP_NAME)],
            group_access=[
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email=USER_EMAIL,
                    state=GroupAccessStateFaker(role="admin"),
                )
            ],
            stakeholders=[
                StakeholderFaker(email=USER_EMAIL),
            ],
        )
    )
)
async def test_send_exported_file() -> None:
    info = GraphQLResolveInfoFaker(user_email=USER_EMAIL, decorators=DEFAULT_DECORATORS)
    response = await resolve(None, info=info, group_name=GROUP_NAME)
    assert response["success"] is True

    with raises(ReportAlreadyRequested):
        await resolve(None, info=info, group_name=GROUP_NAME)


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            groups=[GroupFaker(name=GROUP_NAME)],
            group_access=[
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email=USER_EMAIL,
                    state=GroupAccessStateFaker(role="admin"),
                )
            ],
            stakeholders=[
                StakeholderFaker(email=USER_EMAIL),
            ],
        )
    ),
    others=[Mock(batch_dal, "put_action", "async", PutActionResult(success=False))],
)
async def test_send_exported_file_request_error() -> None:
    info = GraphQLResolveInfoFaker(user_email=USER_EMAIL, decorators=DEFAULT_DECORATORS)

    with raises(RequestedReportError):
        await resolve(None, info=info, group_name=GROUP_NAME)
