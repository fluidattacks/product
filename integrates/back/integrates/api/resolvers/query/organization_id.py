from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.organizations.types import (
    Organization,
)
from integrates.decorators import (
    concurrent_decorators,
    require_login,
    require_organization_access,
)

from .schema import (
    QUERY,
)


@QUERY.field("organizationId")
@concurrent_decorators(
    require_login,
    require_organization_access,
)
async def resolve(_parent: None, info: GraphQLResolveInfo, **kwargs: str) -> Organization | None:
    loaders: Dataloaders = info.context.loaders
    organization_name: str = kwargs["organization_name"]

    return await loaders.organization.load(organization_name)
