from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.db_model.integration_repositories.types import (
    OrganizationIntegrationRepository,
)

from .schema import (
    ORGANIZATION_INTEGRATION_REPOSITORIES,
)


@ORGANIZATION_INTEGRATION_REPOSITORIES.field("branches")
def resolve(
    parent: OrganizationIntegrationRepository,
    _info: GraphQLResolveInfo,
) -> tuple[str, ...]:
    return parent.branches if parent.branches is not None else tuple()
