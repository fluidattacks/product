from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.custom_utils import (
    datetime as datetime_utils,
)
from integrates.db_model.integration_repositories.types import (
    OrganizationIntegrationRepository,
)

from .schema import (
    ORGANIZATION_INTEGRATION_REPOSITORIES,
)


@ORGANIZATION_INTEGRATION_REPOSITORIES.field("lastCommitDate")
def resolve(
    parent: OrganizationIntegrationRepository,
    _info: GraphQLResolveInfo,
) -> str | None:
    return datetime_utils.get_as_str(parent.last_commit_date) if parent.last_commit_date else None
