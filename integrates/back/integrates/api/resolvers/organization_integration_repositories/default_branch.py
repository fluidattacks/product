from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.db_model.integration_repositories.types import (
    OrganizationIntegrationRepository,
)

from .schema import (
    ORGANIZATION_INTEGRATION_REPOSITORIES,
)


@ORGANIZATION_INTEGRATION_REPOSITORIES.field("defaultBranch")
def resolve(
    parent: OrganizationIntegrationRepository,
    _info: GraphQLResolveInfo,
) -> str:
    return parent.branch if parent.branch is not None else ""
