from datetime import (
    datetime,
)

from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.db_model.events.types import (
    EventEvidence,
)

from .schema import (
    EVENT_EVIDENCE_ITEM,
)


@EVENT_EVIDENCE_ITEM.field("date")
def resolve(
    parent: EventEvidence,
    _info: GraphQLResolveInfo,
    **_kwargs: None,
) -> datetime | None:
    return parent.modified_date
