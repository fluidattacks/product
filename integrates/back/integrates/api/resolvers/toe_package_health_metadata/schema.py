from ariadne import (
    ObjectType,
)

TOE_PACKAGE_HEALTH_METADATA = ObjectType("ToePackageHealthMetadata")
