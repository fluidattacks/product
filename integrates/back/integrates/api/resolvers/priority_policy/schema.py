from ariadne import (
    ObjectType,
)

PRIORITY_POLICY: ObjectType = ObjectType("PriorityPolicy")
