from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.db_model.findings.enums import (
    FindingVerificationStatus,
)
from integrates.db_model.findings.types import (
    Finding,
)

from .schema import (
    FINDING,
)


@FINDING.field("remediated")
def resolve(parent: Finding, _info: GraphQLResolveInfo, **_kwargs: None) -> bool:
    return bool(
        parent.verification
        and parent.verification.status == FindingVerificationStatus.REQUESTED
        and not parent.verification.vulnerability_ids,
    )
