from integrates.api.resolvers.finding.verified import resolve
from integrates.db_model.findings.types import FindingVerificationSummary
from integrates.testing.fakers import (
    EMAIL_FLUIDATTACKS_HACKER,
    FindingFaker,
    FindingUnreliableIndicatorsFaker,
    GraphQLResolveInfoFaker,
)
from integrates.testing.utils import parametrize


@parametrize(
    args=["verification_summary_requested", "expected_output"],
    cases=[
        [0, True],
        [1, False],
    ],
)
def test_finding_verified(verification_summary_requested: int, expected_output: bool) -> None:
    info = GraphQLResolveInfoFaker(
        user_email=EMAIL_FLUIDATTACKS_HACKER,
    )
    parent = FindingFaker(
        unreliable_indicators=FindingUnreliableIndicatorsFaker(
            verification_summary=(
                FindingVerificationSummary(
                    requested=verification_summary_requested,
                    on_hold=0,
                    verified=0,
                )
            ),
        ),
    )
    result = resolve(
        parent=parent,
        _info=info,
    )
    assert result == expected_output
