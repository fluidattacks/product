from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.class_types.types import (
    Item,
)
from integrates.custom_utils.finding_comments import (
    format_finding_consulting_resolve,
)
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.finding_comments.types import (
    FindingComment,
)
from integrates.db_model.findings.enums import (
    FindingStateStatus,
)
from integrates.db_model.findings.types import (
    Finding,
)
from integrates.decorators import (
    enforce_group_level_auth_async,
)
from integrates.finding_comments import (
    domain as comments_domain,
)
from integrates.sessions import (
    domain as sessions_domain,
)

from .schema import (
    FINDING,
)


@FINDING.field("observations")
@enforce_group_level_auth_async
async def resolve(
    parent: Finding,
    info: GraphQLResolveInfo,
    **_kwargs: None,
) -> list[Item]:
    loaders: Dataloaders = info.context.loaders
    user_data = await sessions_domain.get_jwt_content(info.context)
    observations: list[FindingComment] = await comments_domain.get_observations(
        loaders,
        parent.group_name,
        parent.id,
        user_data["user_email"],
    )
    is_draft = parent.state.status == FindingStateStatus.CREATED

    return [
        format_finding_consulting_resolve(
            finding_comment=comment,
            target_email=user_data["user_email"],
            is_draft=is_draft,
        )
        for comment in observations
    ]
