from integrates.api.resolvers.finding.remediated import resolve
from integrates.db_model.findings.enums import FindingVerificationStatus
from integrates.db_model.findings.types import Finding
from integrates.testing.fakers import (
    EMAIL_FLUIDATTACKS_HACKER,
    FindingFaker,
    FindingVerificationFaker,
    GraphQLResolveInfoFaker,
)
from integrates.testing.utils import parametrize


@parametrize(
    args=["parent", "expected_output"],
    cases=[
        [
            FindingFaker(
                verification=None,
            ),
            False,
        ],
        [
            FindingFaker(
                verification=FindingVerificationFaker(
                    status=FindingVerificationStatus.VERIFIED,
                ),
            ),
            False,
        ],
        [
            FindingFaker(
                verification=FindingVerificationFaker(
                    status=FindingVerificationStatus.REQUESTED, vulnerability_ids=None
                ),
            ),
            True,
        ],
    ],
)
def test_finding_remediated(parent: Finding, expected_output: bool) -> None:
    info = GraphQLResolveInfoFaker(
        user_email=EMAIL_FLUIDATTACKS_HACKER,
    )
    result = resolve(
        parent=parent,
        _info=info,
    )
    assert result == expected_output
