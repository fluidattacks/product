from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.db_model.findings.types import (
    Finding,
    FindingVulnerabilitiesSummary,
)

from .schema import (
    FINDING,
)


@FINDING.field("vulnerabilitiesSummaryV4")
def resolve(
    parent: Finding,
    _info: GraphQLResolveInfo,
    **_kwargs: None,
) -> FindingVulnerabilitiesSummary:
    return parent.unreliable_indicators.vulnerabilities_summary
