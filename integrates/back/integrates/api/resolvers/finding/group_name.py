from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.db_model.findings.types import (
    Finding,
)

from .schema import (
    FINDING,
)


@FINDING.field("groupName")
def resolve(parent: Finding, _info: GraphQLResolveInfo, **_kwargs: None) -> str:
    return parent.group_name
