from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.db_model.findings.types import (
    Finding,
)
from integrates.decorators import (
    enforce_group_level_auth_async,
)

from .schema import (
    FINDING,
)


@FINDING.field("submittedVulnerabilities")
@enforce_group_level_auth_async
def resolve(parent: Finding, _info: GraphQLResolveInfo, **_kwargs: None) -> int:
    return parent.unreliable_indicators.submitted_vulnerabilities
