from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.db_model.findings.types import (
    Finding,
)
from integrates.db_model.vulnerabilities.utils import (
    get_inverted_state_converted,
)

from .schema import (
    FINDING,
)


@FINDING.field("status")
def resolve(parent: Finding, _info: GraphQLResolveInfo, **_kwargs: None) -> str:
    return get_inverted_state_converted(
        parent.unreliable_indicators.unreliable_status.value.upper(),
    )
