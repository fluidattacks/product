from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.db_model.findings.types import (
    Finding,
)

from .schema import (
    FINDING,
)


@FINDING.field("severityVector")
def resolve(parent: Finding, _info: GraphQLResolveInfo, **_kwargs: None) -> str:
    return parent.severity_score.cvss_v3
