from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.db_model.findings.types import (
    Finding,
)
from integrates.decorators import (
    enforce_group_level_auth_async,
)

from .schema import (
    FINDING,
)


@FINDING.field("hacker")
@enforce_group_level_auth_async
def resolve(parent: Finding, _info: GraphQLResolveInfo, **_kwargs: None) -> str:
    return parent.get_hacker_email()
