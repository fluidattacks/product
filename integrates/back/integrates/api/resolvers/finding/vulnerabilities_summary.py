from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.db_model.findings.types import (
    Finding,
    FindingVulnerabilitiesSummary,
)

from .schema import (
    FINDING,
)


@FINDING.field("vulnerabilitiesSummary")
def resolve(
    parent: Finding,
    _info: GraphQLResolveInfo,
    **_kwargs: None,
) -> FindingVulnerabilitiesSummary:
    vulnerabilities_summary = parent.unreliable_indicators.vulnerabilities_summary
    return vulnerabilities_summary._replace(
        open_critical=vulnerabilities_summary.open_critical_v3,
        open_high=vulnerabilities_summary.open_high_v3,
        open_low=vulnerabilities_summary.open_low_v3,
        open_medium=vulnerabilities_summary.open_medium_v3,
    )
