from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.custom_utils.datetime import (
    get_as_str,
)
from integrates.db_model.findings.types import (
    Finding,
)

from .schema import (
    FINDING,
)


@FINDING.field("releaseDate")
def resolve(parent: Finding, _info: GraphQLResolveInfo, **_kwargs: None) -> str | None:
    indicators = parent.unreliable_indicators
    if indicators.oldest_vulnerability_report_date:
        return get_as_str(indicators.oldest_vulnerability_report_date)
    return None
