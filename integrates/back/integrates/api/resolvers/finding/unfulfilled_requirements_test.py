from integrates.api.resolvers.finding.unfulfilled_requirements import resolve
from integrates.api.resolvers.types import Requirement
from integrates.testing.fakers import (
    EMAIL_FLUIDATTACKS_HACKER,
    FindingFaker,
    GraphQLResolveInfoFaker,
)
from integrates.testing.utils import freeze_time, parametrize


@freeze_time("2024-01-30T00:00:00+00:00")
@parametrize(
    args=["unfulfilled_requirements", "expected_output"],
    cases=[
        [[], []],
        [
            ["048", "050"],
            [
                Requirement(
                    id="048",
                    summary="The components in the source code must have as few dependencies "
                    "as possible.\n",
                    title="Components with minimal dependencies",
                ),
                Requirement(
                    id="050",
                    summary="Interpreted code (e.g., Javascript, CSS) must be loaded from domains "
                    "controlled by the organization.\n",
                    title="Control calls to interpreted code",
                ),
            ],
        ],
    ],
)
def test_finding_age(unfulfilled_requirements: list[str], expected_output: int) -> None:
    info = GraphQLResolveInfoFaker(
        user_email=EMAIL_FLUIDATTACKS_HACKER,
    )
    parent = FindingFaker(unfulfilled_requirements=unfulfilled_requirements)
    result = resolve(
        parent=parent,
        _info=info,
    )
    assert result == expected_output
