from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.db_model.findings.types import (
    Finding,
)

from .schema import (
    FINDING,
)


@FINDING.field("where")
def resolve(parent: Finding, _info: GraphQLResolveInfo, **_kwargs: None) -> str:
    return parent.unreliable_indicators.unreliable_where
