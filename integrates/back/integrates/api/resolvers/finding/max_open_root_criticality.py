from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.db_model.findings.types import (
    Finding,
)
from integrates.db_model.roots.enums import (
    RootCriticality,
)

from .schema import (
    FINDING,
)


@FINDING.field("maxOpenRootCriticality")
def resolve(parent: Finding, _info: GraphQLResolveInfo, **_kwargs: None) -> RootCriticality:
    return parent.unreliable_indicators.max_open_root_criticality
