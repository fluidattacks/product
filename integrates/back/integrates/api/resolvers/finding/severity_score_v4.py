from decimal import (
    Decimal,
)

from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.db_model.findings.types import (
    Finding,
)

from .schema import (
    FINDING,
)


@FINDING.field("severityScoreV4")
def resolve(parent: Finding, _info: GraphQLResolveInfo, **_kwargs: None) -> Decimal:
    return parent.severity_score.threat_score
