from collections.abc import Callable
from typing import Any

from graphql.type.definition import GraphQLResolveInfo
from opensearchpy.helpers import query
from opensearchpy.helpers.utils import DslBase

from integrates import authz
from integrates.api.enums.core import VulnerabilitySort
from integrates.api.inputs.core import VulnerabilityFiltersInput, VulnerabilitySortInput
from integrates.custom_utils import validations
from integrates.dataloaders import get_new_context
from integrates.db_model.findings.types import Finding
from integrates.db_model.items import VulnerabilityItem
from integrates.db_model.vulnerabilities.types import VulnerabilitiesConnection, VulnerabilityEdge
from integrates.db_model.vulnerabilities.utils import format_vulnerability
from integrates.decorators import enforce_group_level_auth_async
from integrates.dynamodb.types import PageInfo
from integrates.search.operations import SearchClient, get_unique_terms, sanitize_query_helpers
from integrates.sessions import domain as sessions_domain
from integrates.vulnerabilities import domain as vulns_domain

from .schema import FINDING

SPECIFIC_SCRIPT = """
    def field = doc['state.specific.keyword'].value;
    if (field == null || field.isEmpty()) return Integer.MAX_VALUE - 1;
    try {
        return Integer.parseInt(field);
    } catch (Exception e) {
        return Integer.MAX_VALUE;
    }
"""


def format_assignee(assignee: str) -> str:
    return str(assignee).replace(".", "\\.")


def format_where(where: str) -> list[str]:
    """Returns root nickname and where clause."""
    nickname = where.split("/", maxsplit=1)[0]
    where_clause = where.split("/", maxsplit=1)[1] if "/" in where else ""
    return [nickname, where_clause]


def validate_query_fields(filters: VulnerabilityFiltersInput) -> None:
    if assignees := filters.get("assignees", None):
        for assignee in assignees:
            validations.check_email(assignee)

    if where := filters.get("where", None):
        validations.check_wildcard(where)


def validate_permissions(
    filters: VulnerabilityFiltersInput,
    group_name: str,
    group_enforcer: Callable[[str, str], bool],
) -> None:
    # Permissions
    can_get_draft = group_enforcer(group_name, "see_draft_status")
    can_get_zr = group_enforcer(group_name, "see_zero_risk_status")

    state = filters.get("state", [])
    zero_risk = filters.get("zero_risk", [])

    # Non-allowed users to request for drafts gets 0 vulns
    if not can_get_draft and ("SUBMITTED" in state or "REJECTED" in state):
        state = [value for value in state if value not in ["SUBMITTED", "REJECTED"]]
        if not state:
            raise PermissionError

    if not can_get_draft and len(state) == 0:
        filters["state"] = ["VULNERABLE", "SAFE"]

    # Non-allowed users to request for zero risk status gets 0 vulns
    if not can_get_zr and zero_risk:
        raise PermissionError


def sanitize_query_fields(
    filters: VulnerabilityFiltersInput | None,
    group_name: str,
    group_enforcer: Callable[[str, str], bool],
) -> dict[str, Any]:
    if not filters:
        return {}

    validate_query_fields(filters)
    validate_permissions(filters, group_name, group_enforcer)

    state = filters.get("state", [])
    state_not = filters.get("state_not", [])
    zero_risk = filters.get("zero_risk", [])
    zero_risk_not = filters.get("zero_risk_not", [])
    reattack = filters.get("reattack", None)

    # By default, confirmed zero risk will not be listed
    if not zero_risk:
        zero_risk_not.append("CONFIRMED")

    # "NotRequested" is a special case to exclude "REQUESTED" and "CONFIRMED"
    verification = []
    verification_not = []
    if reattack == "NotRequested":
        verification_not.append("REQUESTED")
        verification_not.append("CONFIRMED")
    elif reattack is not None and reattack != "":
        verification.append(reattack.upper())

    # Tags should be in lowercase for better matching
    tags = [tag.lower() for tag in filters.get("tags", []) if tag]
    tags_not = [tag.lower() for tag in filters.get("tags_not", []) if tag]

    # Treatments should be in uppercase for better matching
    treatment = (
        [treatment.upper() for treatment in filters.get("treatment", [])]
        if filters.get("treatment")
        else None
    )
    treatment_not = (
        [treatment.upper() for treatment in filters.get("treatment_not", [])]
        if filters.get("treatment_not")
        else None
    )

    # Technique should be in uppercase for better matching
    technique = (
        [tech.upper() for tech in filters.get("technique", [])]
        if filters.get("technique")
        else None
    )
    technique_not = (
        [tech.upper() for tech in filters.get("technique_not", [])]
        if filters.get("technique_not")
        else None
    )

    # Assignees should handle own format for better matching
    assignees = (
        [format_assignee(assignee) for assignee in filters.get("assignees", [])]
        if filters.get("assignees", [])
        else None
    )

    return {
        "state": state,
        "state_not": state_not,
        "zero_risk": zero_risk,
        "zero_risk_not": zero_risk_not,
        "tags": tags,
        "tags_not": tags_not,
        **({"treatment": treatment} if treatment else {}),
        **({"treatment_not": treatment_not} if treatment_not else {}),
        **({"technique": technique} if technique else {}),
        **({"technique_not": technique_not} if technique_not else {}),
        **({"verification": verification} if verification else {}),
        **({"verification_not": verification_not} if verification_not else {}),
        **({"reported_after": filters["reported_after"]} if "reported_after" in filters else {}),
        **({"reported_before": filters["reported_before"]} if "reported_before" in filters else {}),
        **({"search": filters["search"]} if "search" in filters else {}),
        **({"where": filters["where"]} if "where" in filters else {}),
        **({"assignees": assignees} if assignees else {}),
    }


def format_sort(parent: Finding, field: str, order: str) -> dict[str, dict[str, Any]]:
    missing: str | None = None
    if field == VulnerabilitySort.CVSSF_SCORE.value:
        cvssf = parent.unreliable_indicators.max_open_severity_score
        finding_severity = parent.severity_score.temporal_score
        if finding_severity >= cvssf:
            missing = "_last" if order == "asc" else "_first"
        else:
            missing = "_last"

    elif field == VulnerabilitySort.CVSSF_V4_SCORE.value:
        cvssf = parent.unreliable_indicators.max_open_severity_score_v4
        finding_severity = parent.severity_score.threat_score
        if finding_severity >= cvssf:
            missing = "_last" if order == "asc" else "_first"
        else:
            missing = "_last"

    elif field == VulnerabilitySort.SPECIFIC.value:
        return {
            "_script": {
                "type": "number",
                "script": {
                    "source": SPECIFIC_SCRIPT,
                },
                "order": order,
            }
        }

    return {
        field: {
            "order": order,
            **({"missing": missing} if missing else {}),
        }
    }


def sanitize_sort_by(
    sort_by: list[VulnerabilitySortInput] | None,
    parent: Finding,
) -> list[dict]:
    if not sort_by:
        return [
            format_sort(parent, "state.status.keyword", "desc"),
            format_sort(parent, "severity_score.cvssf_v4", "desc"),
            format_sort(parent, "_id", "desc"),
        ]

    return [
        *[format_sort(parent, value["field"], value["order"].lower()) for value in sort_by],
        format_sort(parent, "_id", "desc"),
    ]


def _get_and_filter(params: dict[str, Any]) -> list[DslBase]:
    ranges = []
    if params.get("reported_after"):
        ranges.append(
            query.Range(
                unreliable_indicators__unreliable_report_date={
                    "gt": str(params["reported_after"].date()) + "T00:00:00"
                }
            )
        )
    if params.get("reported_before"):
        ranges.append(
            query.Range(
                unreliable_indicators__unreliable_report_date={
                    "lt": str(params["reported_before"].date()) + "T23:59:59",
                },
            )
        )

    return sanitize_query_helpers(
        [
            query.Terms(state__status__keyword=params.get("state")),
            query.Terms(technique__keyword=params.get("technique")),
            query.Terms(verification__status__keyword=params.get("verification")),
            query.Terms(zero_risk__status__keyword=params.get("zero_risk")),
            query.Terms(treatment__status__keyword=params.get("treatment")),
            query.Terms(tags__keyword=params.get("tags")),
            query.Terms(treatment__assigned__keyword=params.get("assignees")),
            query.Wildcard(state__where__keyword=params.get("where")),
            *ranges,
        ]
    )


async def _get_or_filter(
    params: dict[str, Any], group_name: str, with_root_filtered: bool
) -> list[DslBase]:
    search = params.get("search")
    or_filters: list[DslBase] = []
    roots_ids = []

    if not with_root_filtered and search:
        nickname = format_where(search)[0]
        roots_ids = await get_unique_terms(
            index="roots_index",
            field="pk.keyword",
            conditions=query.Bool(
                must=[
                    query.Match(sk={"query": f"GROUP#{group_name}", "operator": "and"}),
                    query.Wildcard(state__nickname=f"*{nickname}*"),
                ]
            ),
        )
        or_filters.append(query.Terms(pk_2__keyword=roots_ids))

        chars_to_replace = '\\+-&|!(){}[]^"~*?:/'
        for char in chars_to_replace:
            search = search.replace(char, f"\\{char}")

        search = (
            search.replace(" AND ", " ")
            .replace(" OR ", " ")
            .replace(" NOT ", " ")
            .replace(" TO ", " ")
        )

        query_string = f"state.where.keyword:*{search}* OR state.specific.keyword:*{search}*"
        or_filters.append(query.QueryString(query=query_string))

    return sanitize_query_helpers(or_filters)


def _get_not_filter(params: dict[str, Any]) -> list[DslBase]:
    return sanitize_query_helpers(
        [
            query.Terms(
                state__status__keyword=["DELETED", "MASKED", *(params.get("state_not") or [])]
            ),
            query.Terms(zero_risk__status__keyword=params.get("zero_risk_not")),
            query.Terms(technique__keyword=params.get("technique_not")),
            query.Terms(verification__status__keyword=params.get("verification_not")),
            query.Terms(treatment__status__keyword=params.get("treatment_not")),
            query.Terms(tags__keyword=params.get("tags_not")),
        ]
    )


def _get_search_key(parent: Finding) -> list[DslBase]:
    return [
        query.Match(group_name={"query": parent.group_name, "operator": "and"}),
        query.Match(sk={"query": f"FIN#{parent.id}", "operator": "and"}),
    ]


@FINDING.field("vulnerabilities")
@enforce_group_level_auth_async
async def resolve(
    parent: Finding,
    _info: GraphQLResolveInfo,
    first: int,
    **kwargs: Any,
) -> VulnerabilitiesConnection:
    user_info = await sessions_domain.get_jwt_content(_info.context)
    user_email = user_info["user_email"]
    group_enforcer = await authz.get_group_level_enforcer(_info.context.loaders, user_email)

    after: str | None = kwargs.get("after")
    filters: VulnerabilityFiltersInput | None = kwargs.get("filters")
    sort_by: list[VulnerabilitySortInput] | None = kwargs.get("sort_by")

    try:
        params = sanitize_query_fields(filters, parent.group_name, group_enforcer)
        sort = sanitize_sort_by(sort_by, parent)

    except PermissionError:
        return VulnerabilitiesConnection(
            edges=tuple(),
            page_info=PageInfo(end_cursor="", has_next_page=False),
            total=0,
        )

    root_filters = []
    if params.get("search"):
        loaders = get_new_context()
        root_filters = await vulns_domain.get_vulns_filters_by_root_nickname(
            loaders=loaders,
            group_name=parent.group_name,
            root=params["search"],
        )

        # remove "match" condition from search
        if len(root_filters) > 0:
            root_filters.pop(1)

    search_and = [*_get_and_filter(params), *root_filters]
    search_or = await _get_or_filter(params, parent.group_name, len(root_filters) > 0)
    search_not = _get_not_filter(params)
    search_filter = _get_search_key(parent)

    results = await SearchClient[VulnerabilityItem].raw_search(
        index="vulns_index",
        body={
            "query": query.Bool(
                must=search_and,
                should=search_or,
                minimum_should_match=1 if len(search_or) > 0 else 0,
                must_not=search_not,
                filter=search_filter,
            ).to_dict(),
        },
        sort_by=sort,
        size=first or 100,
        after=after,
    )

    vulnerabilities = tuple(format_vulnerability(result) for result in results.items)

    return VulnerabilitiesConnection(
        edges=tuple(
            VulnerabilityEdge(
                cursor=results.page_info.end_cursor,
                node=vulnerability,
            )
            for vulnerability in vulnerabilities
        ),
        page_info=results.page_info,
        total=results.total,
    )
