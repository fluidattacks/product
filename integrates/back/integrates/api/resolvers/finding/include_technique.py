from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.findings.types import (
    Finding,
)
from integrates.findings.domain.core import (
    has_vulns_with_technique,
)

from .schema import (
    FINDING,
)


@FINDING.field("includeTechnique")
async def resolve(
    parent: Finding,
    info: GraphQLResolveInfo,
    **kwargs: str,
) -> bool:
    loaders: Dataloaders = info.context.loaders
    technique = kwargs.get("technique")

    return await has_vulns_with_technique(loaders, parent.id, technique)
