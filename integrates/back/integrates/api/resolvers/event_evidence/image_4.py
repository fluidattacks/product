from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.db_model.events.types import (
    EventEvidences,
)

from .schema import (
    EVENT_EVIDENCE,
)


@EVENT_EVIDENCE.field("image4")
def resolve(
    parent: EventEvidences,
    _info: GraphQLResolveInfo,
    **_kwargs: None,
) -> object:
    image_4 = parent.image_4
    return image_4
