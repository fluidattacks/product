from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.db_model.events.types import (
    EventEvidences,
)

from .schema import (
    EVENT_EVIDENCE,
)


@EVENT_EVIDENCE.field("image1")
def resolve(
    parent: EventEvidences,
    _info: GraphQLResolveInfo,
    **_kwargs: None,
) -> object:
    image_1 = parent.image_1
    return image_1
