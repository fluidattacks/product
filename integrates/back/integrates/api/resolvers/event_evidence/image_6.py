from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.db_model.events.types import (
    EventEvidences,
)

from .schema import (
    EVENT_EVIDENCE,
)


@EVENT_EVIDENCE.field("image6")
def resolve(
    parent: EventEvidences,
    _info: GraphQLResolveInfo,
    **_kwargs: None,
) -> object:
    image_6 = parent.image_6
    return image_6
