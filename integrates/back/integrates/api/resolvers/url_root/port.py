from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.db_model.roots.types import (
    URLRoot,
)

from .schema import (
    URL_ROOT,
)


@URL_ROOT.field("port")
def resolve(parent: URLRoot, _info: GraphQLResolveInfo) -> int:
    return int(parent.state.port)
