from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.db_model.roots.types import (
    URLRoot,
)

from .schema import (
    URL_ROOT,
)


@URL_ROOT.field("excludedSubPaths")
def resolve(parent: URLRoot, _info: GraphQLResolveInfo) -> list[str]:
    return parent.state.excluded_sub_paths or []
