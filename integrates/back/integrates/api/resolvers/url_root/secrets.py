from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.roots.types import (
    GitRoot,
    Secret,
    URLRoot,
)
from integrates.decorators import (
    concurrent_decorators,
    enforce_group_level_auth_async,
    require_is_not_under_review,
)

from .schema import (
    URL_ROOT,
)


@URL_ROOT.field("secrets")
@concurrent_decorators(enforce_group_level_auth_async, require_is_not_under_review)
async def resolve(parent: GitRoot | URLRoot, info: GraphQLResolveInfo) -> list[Secret]:
    loaders: Dataloaders = info.context.loaders
    return await loaders.root_secrets.load(parent.id)
