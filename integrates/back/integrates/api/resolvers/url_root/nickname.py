from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.db_model.roots.types import (
    Root,
)

from .schema import (
    URL_ROOT,
)


@URL_ROOT.field("nickname")
def resolve(parent: Root, _info: GraphQLResolveInfo) -> str:
    return parent.state.nickname
