from decimal import Decimal

from integrates.api.resolvers.organization_compliance.standards import resolve
from integrates.api.resolvers.organization_compliance.types import OrganizationComplianceStandard
from integrates.db_model.organizations.types import (
    OrganizationStandardCompliance,
    OrganizationUnreliableIndicators,
)
from integrates.decorators import enforce_group_level_auth_async
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    GraphQLResolveInfoFaker,
    GroupAccessFaker,
    GroupAccessStateFaker,
    GroupFaker,
    StakeholderFaker,
)
from integrates.testing.mocks import mocks


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            groups=[GroupFaker(name="group2")],
            group_access=[
                GroupAccessFaker(email="user@test.test", state=GroupAccessStateFaker(role="hacker"))
            ],
            stakeholders=[StakeholderFaker(email="user@test.test")],
        )
    )
)
async def test_group_compliance_standards() -> None:
    info = GraphQLResolveInfoFaker(
        user_email="user@test.test",
        decorators=[[enforce_group_level_auth_async]],
    )
    result = await resolve(
        parent=OrganizationUnreliableIndicators(
            covered_authors=0,
            covered_repositories=0,
            has_ztna_roots=False,
            missed_authors=0,
            missed_repositories=0,
            compliance_level=Decimal("0.95"),
            compliance_weekly_trend=Decimal("0.00"),
            estimated_days_to_full_compliance=Decimal("0.01"),
            standard_compliances=[
                OrganizationStandardCompliance(
                    standard_name="capec", compliance_level=Decimal("0.93")
                )
            ],
        ),
        info=info,
    )
    assert result[0] == OrganizationComplianceStandard(
        avg_organization_compliance_level=Decimal("0.0"),
        best_organization_compliance_level=Decimal("0.0"),
        compliance_level=Decimal("0.0"),
        standard_id="bsimm",
        standard_title="BSIMM",
        worst_organization_compliance_level=Decimal("0.0"),
    )
