from graphql import (
    GraphQLResolveInfo,
)

from integrates.groups import (
    gitlab_issues_integration,
)

from .schema import (
    GITLAB_ISSUES_INTEGRATION,
)


@GITLAB_ISSUES_INTEGRATION.field("projectNames")
async def resolve(parent: dict, info: GraphQLResolveInfo) -> list[str]:
    return await gitlab_issues_integration.get_project_names(
        group_name=parent["group_name"],
        loaders=info.context.loaders,
    )
