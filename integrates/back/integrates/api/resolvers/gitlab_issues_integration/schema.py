from ariadne import (
    ObjectType,
)

GITLAB_ISSUES_INTEGRATION = ObjectType("GitLabIssuesIntegration")
