from datetime import (
    datetime,
)

from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.db_model.toe_lines.types import (
    ToeLine,
)
from integrates.decorators import (
    enforce_group_level_auth_async,
)

from .schema import (
    TOE_LINES,
)


@TOE_LINES.field("attackedAt")
@enforce_group_level_auth_async
def resolve(parent: ToeLine, _info: GraphQLResolveInfo, **_kwargs: None) -> datetime | None:
    return parent.state.attacked_at
