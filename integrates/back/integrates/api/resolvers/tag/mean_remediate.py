from decimal import (
    Decimal,
)

from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.db_model.portfolios.types import (
    Portfolio,
)

from .schema import (
    TAG,
)


@TAG.field("meanRemediate")
def resolve(
    parent: Portfolio,
    _info: GraphQLResolveInfo,
    **_kwargs: None,
) -> Decimal | None:
    mean_remediate = parent.unreliable_indicators.mean_remediate
    return mean_remediate
