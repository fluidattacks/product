from aioextensions import (
    collect,
)
from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.custom_utils import (
    groups as groups_utils,
)
from integrates.custom_utils.groups import get_group
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.groups.types import (
    Group,
)
from integrates.db_model.portfolios.types import (
    Portfolio,
)

from .schema import (
    TAG,
)


@TAG.field("groups")
async def resolve(
    parent: Portfolio,
    info: GraphQLResolveInfo,
    **_kwargs: None,
) -> list[Group]:
    group_names = parent.groups
    loaders: Dataloaders = info.context.loaders
    groups = await collect([get_group(loaders, group_name) for group_name in group_names])

    return groups_utils.filter_active_groups(groups)
