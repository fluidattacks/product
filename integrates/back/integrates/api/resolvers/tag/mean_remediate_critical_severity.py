from decimal import (
    Decimal,
)

from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.db_model.portfolios.types import (
    Portfolio,
)

from .schema import (
    TAG,
)


@TAG.field("meanRemediateCriticalSeverity")
def resolve(
    parent: Portfolio,
    _info: GraphQLResolveInfo,
    **_kwargs: None,
) -> Decimal | None:
    mean_remediate_critical_severity = parent.unreliable_indicators.mean_remediate_critical_severity
    return mean_remediate_critical_severity
