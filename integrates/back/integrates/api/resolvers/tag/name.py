from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.db_model.portfolios.types import (
    Portfolio,
)

from .schema import (
    TAG,
)


@TAG.field("name")
def resolve(
    parent: Portfolio,
    _info: GraphQLResolveInfo,
    **_kwargs: None,
) -> str:
    portfolio_name = parent.id
    return portfolio_name
