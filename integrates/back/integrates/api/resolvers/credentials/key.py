from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.db_model.credentials.types import (
    Credentials,
    SshSecret,
)
from integrates.decorators import (
    enforce_owner,
)
from integrates.verify.enums import (
    AuthenticationLevel,
)

from .schema import (
    CREDENTIALS,
)


@CREDENTIALS.field("key")
@enforce_owner(AuthenticationLevel.USER)
def resolve(parent: Credentials, _info: GraphQLResolveInfo) -> str | None:
    return parent.secret.key if isinstance(parent.secret, SshSecret) else None
