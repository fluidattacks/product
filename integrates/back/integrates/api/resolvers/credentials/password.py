from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.db_model.credentials.types import (
    Credentials,
    HttpsSecret,
)
from integrates.decorators import (
    enforce_owner,
)
from integrates.verify.enums import (
    AuthenticationLevel,
)

from .schema import (
    CREDENTIALS,
)


@CREDENTIALS.field("password")
@enforce_owner(AuthenticationLevel.USER)
def resolve(parent: Credentials, _info: GraphQLResolveInfo) -> str | None:
    return parent.secret.password if isinstance(parent.secret, HttpsSecret) else None
