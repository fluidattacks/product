from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.db_model.credentials.types import (
    Credentials,
    HttpsPatSecret,
    OauthAzureSecret,
    OauthBitbucketSecret,
    OauthGithubSecret,
    OauthGitlabSecret,
)
from integrates.decorators import (
    enforce_owner,
)
from integrates.oauth.common import (
    get_credential_token,
)
from integrates.verify.enums import (
    AuthenticationLevel,
)

from .schema import (
    CREDENTIALS,
)


@CREDENTIALS.field("token")
@enforce_owner(AuthenticationLevel.USER)
async def resolve(parent: Credentials, info: GraphQLResolveInfo) -> str | None:
    if isinstance(parent.secret, HttpsPatSecret):
        return parent.secret.token

    if isinstance(
        parent.secret,
        (
            OauthGithubSecret,
            OauthBitbucketSecret,
            OauthAzureSecret,
            OauthGitlabSecret,
        ),
    ):
        return await get_credential_token(
            loaders=info.context.loaders,
            credential=parent,
        )

    return None
