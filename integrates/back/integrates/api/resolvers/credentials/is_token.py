from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.db_model.credentials.types import (
    Credentials,
    HttpsPatSecret,
)

from .schema import (
    CREDENTIALS,
)


@CREDENTIALS.field("isToken")
def resolve(parent: Credentials, _info: GraphQLResolveInfo) -> bool:
    return isinstance(parent.secret, HttpsPatSecret)
