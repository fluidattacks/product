from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.db_model.credentials.types import AWSRoleSecret, Credentials
from integrates.db_model.enums import CredentialType
from integrates.decorators import (
    enforce_owner,
)
from integrates.verify.enums import (
    AuthenticationLevel,
)

from .schema import (
    CREDENTIALS,
)


@CREDENTIALS.field("arn")
@enforce_owner(AuthenticationLevel.USER)
def resolve(parent: Credentials, _info: GraphQLResolveInfo) -> str | None:
    return (
        parent.secret.arn
        if parent.state.type == CredentialType.AWSROLE and isinstance(parent.secret, AWSRoleSecret)
        else None
    )
