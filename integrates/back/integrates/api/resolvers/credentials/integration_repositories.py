from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.credentials.types import (
    Credentials,
)
from integrates.db_model.integration_repositories.types import (
    OrganizationIntegrationRepository,
)
from integrates.decorators import (
    require_organization_access,
)
from integrates.outside_repositories.utils import (
    get_credentials_repositories,
)

from .schema import (
    CREDENTIALS,
)


@CREDENTIALS.field("integrationRepositories")
@require_organization_access
async def resolve(
    parent: Credentials,
    info: GraphQLResolveInfo,
) -> tuple[OrganizationIntegrationRepository, ...]:
    loaders: Dataloaders = info.context.loaders
    unreliable_repositories = await loaders.credential_unreliable_repositories.load(parent.id)
    if unreliable_repositories:
        return tuple(unreliable_repositories)

    repositories = await get_credentials_repositories(loaders, parent)

    return repositories
