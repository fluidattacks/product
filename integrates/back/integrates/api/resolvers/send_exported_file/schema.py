from ariadne import (
    ObjectType,
)

SEND_EXPORTED_FILE = ObjectType("SendExportedFile")
