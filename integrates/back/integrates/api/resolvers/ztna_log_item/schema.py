from ariadne import (
    ObjectType,
)

ZTNA_HTTP_ITEM = ObjectType("ZtnaHttpItem")
ZTNA_NETWORK_ITEM = ObjectType("ZtnaNetworkItem")
ZTNA_SESSION_ITEM = ObjectType("ZtnaSessionItem")
