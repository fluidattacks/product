from integrates.api.resolvers.organization.credentials import resolve
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    CredentialsFaker,
    CredentialsStateFaker,
    GraphQLResolveInfoFaker,
    OrganizationAccessFaker,
    OrganizationFaker,
    StakeholderFaker,
)
from integrates.testing.mocks import mocks

ORG_ID = "ORG#40f6da5f-4f66-4bf0-825b-a2d9748ad6db"
USER_EMAIL = "jdoe@orgtest.com"
ORG_NAME = "orgtest"


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            credentials=[
                CredentialsFaker(
                    credential_id="credential-test-id",
                    organization_id=ORG_ID,
                    state=CredentialsStateFaker(modified_by=USER_EMAIL, name="credential-name1"),
                ),
                CredentialsFaker(
                    credential_id="credential-test-id2",
                    organization_id=ORG_ID,
                    state=CredentialsStateFaker(modified_by=USER_EMAIL, name="credential-name2"),
                ),
            ],
            stakeholders=[StakeholderFaker(email=USER_EMAIL, enrolled=True)],
            organization_access=[OrganizationAccessFaker(organization_id=ORG_ID, email=USER_EMAIL)],
            organizations=[OrganizationFaker(id=ORG_ID, name=ORG_NAME)],
        )
    )
)
async def test_organization_credential() -> None:
    # Act
    parent = OrganizationFaker(id=ORG_ID, name=ORG_NAME)
    info = GraphQLResolveInfoFaker(user_email=USER_EMAIL)

    result = await resolve(parent=parent, info=info)

    # Assert
    assert result
    assert len(result) == 2
    assert result[0].id == "credential-test-id"
    assert result[1].id == "credential-test-id2"
