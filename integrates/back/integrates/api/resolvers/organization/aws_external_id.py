from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.db_model.organizations.types import (
    Organization,
)

from .schema import (
    ORGANIZATION,
)


@ORGANIZATION.field("awsExternalId")
def resolve(parent: Organization, _info: GraphQLResolveInfo) -> str:
    return parent.state.aws_external_id
