from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.db_model.constants import (
    DEFAULT_INACTIVITY_PERIOD,
)
from integrates.db_model.organizations.types import (
    Organization,
)

from .schema import (
    ORGANIZATION,
)


@ORGANIZATION.field("inactivityPeriod")
def resolve(
    parent: Organization,
    _info: GraphQLResolveInfo,
    **_kwargs: None,
) -> int:
    return (
        parent.policies.inactivity_period
        if parent.policies.inactivity_period is not None
        else DEFAULT_INACTIVITY_PERIOD
    )
