from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.db_model.organizations.types import (
    Organization,
    OrganizationPriorityPolicy,
)

from .schema import (
    ORGANIZATION,
)


@ORGANIZATION.field("priorityPolicies")
def resolve(
    parent: Organization,
    _info: GraphQLResolveInfo,
    **_kwargs: None,
) -> list[OrganizationPriorityPolicy] | None:
    return parent.priority_policies
