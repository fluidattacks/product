from integrates.api.resolvers.organization.finding_policies import resolve
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    GraphQLResolveInfoFaker,
    OrganizationAccessFaker,
    OrganizationFaker,
    OrgFindingPolicyFaker,
    StakeholderFaker,
    random_uuid,
)
from integrates.testing.mocks import mocks

ORG_ID = "ORG#40f6da5f-4f66-4bf0-825b-a2d9748ad6db"
USER_EMAIL = "jdoe@orgtest.com"
ORG_NAME = "orgtest"


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            stakeholders=[StakeholderFaker(email=USER_EMAIL, enrolled=True)],
            organization_access=[OrganizationAccessFaker(organization_id=ORG_ID, email=USER_EMAIL)],
            organizations=[OrganizationFaker(id=ORG_ID, name=ORG_NAME)],
            organization_finding_policies=[
                OrgFindingPolicyFaker(id=random_uuid(), name="policy1", organization_name=ORG_NAME),
                OrgFindingPolicyFaker(id=random_uuid(), name="policy2", organization_name=ORG_NAME),
            ],
        )
    )
)
async def test_organization_finding_policies() -> None:
    # Act
    parent = OrganizationFaker(id=ORG_ID, name=ORG_NAME)
    info = GraphQLResolveInfoFaker(user_email=USER_EMAIL)

    result = await resolve(parent=parent, info=info)

    # Assert
    assert result
    assert len(result) == 2
    assert sorted([org_finding.name for org_finding in result]) == ["policy1", "policy2"]
