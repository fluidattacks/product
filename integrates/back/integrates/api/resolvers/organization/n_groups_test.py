from integrates.api.resolvers.organization.n_groups import resolve
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    GraphQLResolveInfoFaker,
    GroupAccessFaker,
    GroupAccessStateFaker,
    GroupFaker,
    OrganizationAccessFaker,
    OrganizationFaker,
    StakeholderFaker,
)
from integrates.testing.mocks import mocks

ORG_ID = "ORG#40f6da5f-4f66-4bf0-825b-a2d9748ad6db"
USER_EMAIL = "jdoe@orgtest.com"
ORG_NAME = "orgtest"


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            stakeholders=[StakeholderFaker(email=USER_EMAIL, enrolled=True)],
            organization_access=[OrganizationAccessFaker(organization_id=ORG_ID, email=USER_EMAIL)],
            organizations=[OrganizationFaker(id=ORG_ID, name=ORG_NAME)],
            groups=[
                GroupFaker(organization_id=ORG_ID, name="group1"),
                GroupFaker(organization_id=ORG_ID, name="group2"),
                GroupFaker(organization_id=ORG_ID, name="group3"),
            ],
            group_access=[
                GroupAccessFaker(
                    email=USER_EMAIL,
                    group_name="group1",
                    state=GroupAccessStateFaker(role="organization_manager"),
                ),
                GroupAccessFaker(
                    email=USER_EMAIL,
                    group_name="group2",
                    state=GroupAccessStateFaker(role="organization_manager"),
                ),
                GroupAccessFaker(
                    email=USER_EMAIL,
                    group_name="group3",
                    state=GroupAccessStateFaker(role="organization_manager"),
                ),
            ],
        )
    )
)
async def test_organization_n_groups() -> None:
    # Act
    parent = OrganizationFaker(id=ORG_ID, name=ORG_NAME)
    info = GraphQLResolveInfoFaker(user_email=USER_EMAIL)

    result = await resolve(parent=parent, info=info)

    # Assert
    assert result
    assert result == 3
