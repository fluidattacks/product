from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.organizations.types import (
    Organization,
    OrganizationUnreliableIndicators,
)

from .schema import (
    ORGANIZATION,
)


@ORGANIZATION.field("missedRepositories")
async def resolve(
    parent: Organization,
    info: GraphQLResolveInfo,
    **_kwargs: None,
) -> int:
    loaders: Dataloaders = info.context.loaders
    indicators: OrganizationUnreliableIndicators = (
        await loaders.organization_unreliable_indicators.load(parent.id)
    )

    return indicators.missed_repositories if indicators.missed_repositories else 0
