from integrates.api.resolvers.organization.integration_repositories_connection import resolve
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    DATE_2024,
    CredentialsFaker,
    GraphQLResolveInfoFaker,
    OrganizationAccessFaker,
    OrganizationFaker,
    OrganizationIntegrationRepositoryFaker,
    StakeholderFaker,
    random_uuid,
)
from integrates.testing.mocks import mocks

CRED_ID = "test-credentials-id"
ORG_ID = "ORG#40f6da5f-4f66-4bf0-825b-a2d9748ad6db"
USER_EMAIL = "jdoe@orgtest.com"
ORG_NAME = "orgtest"


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            stakeholders=[StakeholderFaker(email=USER_EMAIL, enrolled=True)],
            organization_access=[OrganizationAccessFaker(organization_id=ORG_ID, email=USER_EMAIL)],
            organizations=[OrganizationFaker(id=ORG_ID, name=ORG_NAME)],
            credentials=[CredentialsFaker(credential_id=CRED_ID, organization_id=ORG_ID)],
            organization_integration_repositories=[
                OrganizationIntegrationRepositoryFaker(
                    credential_id=CRED_ID,
                    id=random_uuid(),
                    last_commit_date=DATE_2024,
                    organization_id=ORG_ID,
                    url="https://github.com/test1",
                ),
                OrganizationIntegrationRepositoryFaker(
                    credential_id=CRED_ID,
                    id=random_uuid(),
                    last_commit_date=DATE_2024,
                    organization_id=ORG_ID,
                    url="https://github.com/test2",
                ),
            ],
        )
    )
)
async def test_organization_integration_repositories_connection() -> None:
    # Act
    parent = OrganizationFaker(id=ORG_ID, name=ORG_NAME)
    info = GraphQLResolveInfoFaker(user_email=USER_EMAIL)

    result = await resolve(parent=parent, info=info, after=None, first=None)

    # Assert
    assert result
    assert len(result.edges) == 2
    assert sorted([integration_repo.node.url for integration_repo in result.edges]) == [
        "https://github.com/test1",
        "https://github.com/test2",
    ]
