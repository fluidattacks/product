from datetime import datetime, timedelta

from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.custom_exceptions import (
    DocumentNotFound,
    RequiredNewPhoneNumber,
    RequiredVerificationCode,
)
from integrates.custom_utils import (
    logs as logs_utils,
)
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.notifications.types import ReportStatus
from integrates.db_model.organizations.types import (
    Organization,
)
from integrates.decorators import (
    concurrent_decorators,
    enforce_organization_level_auth_async,
    require_login,
)
from integrates.notifications.reports import add_report_notification, update_report_notification
from integrates.s3.operations import (
    sign_url,
)
from integrates.sessions import (
    domain as sessions_domain,
)
from integrates.sessions import (
    utils as sessions_utils,
)
from integrates.stakeholders.utils import (
    get_international_format_phone_number,
)
from integrates.verify.operations import (
    check_verification,
)

from .schema import (
    ORGANIZATION,
)

TTL = 60


@ORGANIZATION.field("vulnerabilitiesUrl")
@concurrent_decorators(
    require_login,
    enforce_organization_level_auth_async,
)
async def resolve(
    parent: Organization,
    info: GraphQLResolveInfo,
    verification_code: str | None = None,
    **_kwargs: None,
) -> str:
    logs_utils.cloudwatch_log(
        info.context,
        "Tried to get vulnerabilities for organization",
        extra={"organization_id": parent.id, "log_type": "Security"},
    )

    loaders: Dataloaders = info.context.loaders
    user_info: dict[str, str] = await sessions_domain.get_jwt_content(info.context)
    user_email = user_info["user_email"]
    if not sessions_utils.is_api_token(user_info):
        stakeholder = await loaders.stakeholder.load(user_email)
        user_phone = stakeholder.phone if stakeholder else None
        if not user_phone:
            raise RequiredNewPhoneNumber()

        if not verification_code:
            raise RequiredVerificationCode()

        await check_verification(
            recipient=get_international_format_phone_number(user_phone),
            code=verification_code,
        )

    if parent.vulnerabilities_pathfile is None:
        raise DocumentNotFound()

    logs_utils.cloudwatch_log(
        info.context,
        "Got vulnerabilities for the organization",
        extra={"organization_id": parent.id, "log_type": "Security"},
    )
    notification = await add_report_notification(
        report_format="DATA",
        report_name=f"Vulnerabilities ({parent.name})",
        user_email=user_email,
    )
    await update_report_notification(
        expiration_time=(datetime.now() + timedelta(days=7)),
        notification_id=notification.id,
        s3_file_path=parent.vulnerabilities_pathfile,
        size=None,
        status=ReportStatus.READY,
        user_email=user_email,
    )
    return await sign_url(parent.vulnerabilities_pathfile, TTL)
