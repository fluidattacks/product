from integrates.api.resolvers.organization.vulnerabilities_url import resolve
from integrates.decorators import enforce_organization_level_auth_async, require_login
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    GraphQLResolveInfoFaker,
    OrganizationAccessFaker,
    OrganizationFaker,
    StakeholderFaker,
)
from integrates.testing.mocks import mocks

ORG_ID = "ORG#40f6da5f-4f66-4bf0-825b-a2d9748ad6db"
USER_EMAIL = "jdoe@orgtest.com"
ORG_NAME = "orgtest"


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            stakeholders=[StakeholderFaker(email=USER_EMAIL, enrolled=True)],
            organization_access=[OrganizationAccessFaker(organization_id=ORG_ID, email=USER_EMAIL)],
            organizations=[
                OrganizationFaker(
                    id=ORG_ID, name=ORG_NAME, vulnerabilities_pathfile="vulns_path_test"
                )
            ],
        )
    ),
)
async def test_organization_vulnerabilities_url() -> None:
    # Act
    parent = OrganizationFaker(id=ORG_ID, name=ORG_NAME, vulnerabilities_pathfile="vulns_path_test")
    info = GraphQLResolveInfoFaker(
        user_email=USER_EMAIL,
        decorators=[[enforce_organization_level_auth_async], [require_login]],
    )

    result = await resolve(
        parent=parent,
        info=info,
        organization_id=ORG_ID,
        organization_name=ORG_NAME,
        verification_code="12347",
    )

    # Assert
    assert result
    assert result.startswith("https://s3.amazonaws.com/integrates.dev/")
    assert result.find("vulns_path_test") > 0
