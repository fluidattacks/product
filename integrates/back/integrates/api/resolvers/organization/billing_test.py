from integrates.api.resolvers.organization.billing import resolve
from integrates.billing import domain as billing_domain
from integrates.billing.types import Price
from integrates.db_model.organizations.types import (
    OrganizationDocuments,
    OrganizationPaymentMethods,
)
from integrates.decorators import enforce_organization_level_auth_async, require_login
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    DATE_2025,
    GraphQLResolveInfoFaker,
    GroupAccessFaker,
    GroupAccessStateFaker,
    GroupFaker,
    OrganizationAccessFaker,
    OrganizationFaker,
    StakeholderFaker,
)
from integrates.testing.mocks import Mock, mocks

ORG_ID = "ORG#40f6da5f-4f66-4bf0-825b-a2d9748ad6db"
USER_EMAIL = "jdoe@orgtest.com"
ORG_NAME = "orgtest"


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            stakeholders=[StakeholderFaker(email=USER_EMAIL, enrolled=True)],
            organization_access=[OrganizationAccessFaker(organization_id=ORG_ID, email=USER_EMAIL)],
            groups=[
                GroupFaker(organization_id=ORG_ID, name="group1"),
                GroupFaker(organization_id=ORG_ID, name="group2"),
                GroupFaker(organization_id=ORG_ID, name="group3"),
            ],
            group_access=[
                GroupAccessFaker(
                    email=USER_EMAIL,
                    group_name="group1",
                    state=GroupAccessStateFaker(role="organization_manager"),
                ),
                GroupAccessFaker(
                    email=USER_EMAIL,
                    group_name="group2",
                    state=GroupAccessStateFaker(role="organization_manager"),
                ),
                GroupAccessFaker(
                    email=USER_EMAIL,
                    group_name="group3",
                    state=GroupAccessStateFaker(role="organization_manager"),
                ),
            ],
            organizations=[
                OrganizationFaker(
                    id=ORG_ID,
                    name=ORG_NAME,
                    payment_methods=[
                        OrganizationPaymentMethods(
                            id="org-payment-method",
                            business_name=ORG_NAME,
                            email=USER_EMAIL,
                            country="US",
                            state="TX",
                            city="Austin",
                            documents=OrganizationDocuments(rut=None, tax_id=None),
                        )
                    ],
                )
            ],
        )
    ),
    others=[
        Mock(
            billing_domain,
            "get_prices",
            "async",
            {
                "advanced": Price("price_id_123", "USD", 1500),
                "essential": Price("price_id_456", "USD", 2000),
            },
        )
    ],
)
async def test_organization_billing() -> None:
    # Act
    parent = OrganizationFaker(id=ORG_ID, name=ORG_NAME)
    info = GraphQLResolveInfoFaker(
        user_email=USER_EMAIL, decorators=[[require_login], [enforce_organization_level_auth_async]]
    )

    result = await resolve(
        parent=parent, info=info, date=DATE_2025, organization_id=ORG_ID, organization_name=ORG_NAME
    )

    # Assert
    assert result
    assert result.organization == ORG_ID
