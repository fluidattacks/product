from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.integration_repositories.types import (
    OrganizationIntegrationRepositoryConnection,
    OrganizationIntegrationRepositoryRequest,
)
from integrates.db_model.organizations.types import (
    Organization,
)

from .schema import (
    ORGANIZATION,
)


@ORGANIZATION.field("integrationRepositoriesConnection")
async def resolve(
    parent: Organization,
    info: GraphQLResolveInfo,
    after: str | None = None,
    first: int | None = None,
    **_kwargs: None,
) -> OrganizationIntegrationRepositoryConnection:
    loaders: Dataloaders = info.context.loaders
    current_repositories = await loaders.organization_unreliable_outside_repositories_c.load(
        OrganizationIntegrationRepositoryRequest(
            organization_id=parent.id,
            after=after,
            first=first,
            paginate=True,
        ),
    )

    return current_repositories._replace(
        edges=tuple({edge.node.url: edge for edge in current_repositories.edges}.values()),
    )
