from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.db_model.organizations.types import (
    Organization,
)

from .schema import (
    ORGANIZATION,
)


@ORGANIZATION.field("maxNumberAcceptances")
def resolve(
    parent: Organization,
    _info: GraphQLResolveInfo,
    **_kwargs: None,
) -> int | None:
    return parent.policies.max_number_acceptances
