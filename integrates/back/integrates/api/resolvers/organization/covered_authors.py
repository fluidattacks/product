from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.organizations.types import (
    Organization,
    OrganizationUnreliableIndicators,
)

from .schema import (
    ORGANIZATION,
)


@ORGANIZATION.field("coveredAuthors")
async def resolve(
    parent: Organization,
    info: GraphQLResolveInfo,
    **_kwargs: None,
) -> int:
    loaders: Dataloaders = info.context.loaders
    org_indicators: OrganizationUnreliableIndicators = (
        await loaders.organization_unreliable_indicators.load(parent.id)
    )

    return org_indicators.covered_authors if org_indicators.covered_authors else 0
