from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.credentials.types import (
    Credentials,
)
from integrates.db_model.organizations.types import (
    Organization,
)

from .schema import (
    ORGANIZATION,
)


@ORGANIZATION.field("credentials")
async def resolve(
    parent: Organization,
    info: GraphQLResolveInfo,
    **_kwargs: None,
) -> list[Credentials]:
    loaders: Dataloaders = info.context.loaders
    return await loaders.organization_credentials.load(parent.id)
