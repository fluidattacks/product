from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.organizations.types import (
    Organization,
)
from integrates.groups import (
    domain as groups_domain,
)
from integrates.sessions import (
    domain as sessions_domain,
)

from .schema import (
    ORGANIZATION,
)


@ORGANIZATION.field("nGroups")
async def resolve(
    parent: Organization,
    info: GraphQLResolveInfo,
    **_kwargs: None,
) -> int:
    loaders: Dataloaders = info.context.loaders
    session_info = await sessions_domain.get_jwt_content(info.context)
    email: str = session_info["user_email"]
    stakeholder_group_names = await groups_domain.get_groups_by_stakeholder(
        loaders,
        email,
        organization_id=parent.id,
    )

    return len(stakeholder_group_names)
