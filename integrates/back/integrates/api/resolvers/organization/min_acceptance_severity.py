from decimal import (
    Decimal,
)

from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.db_model.constants import (
    DEFAULT_MIN_SEVERITY,
)
from integrates.db_model.organizations.types import (
    Organization,
)

from .schema import (
    ORGANIZATION,
)


@ORGANIZATION.field("minAcceptanceSeverity")
def resolve(
    parent: Organization,
    _info: GraphQLResolveInfo,
    **_kwargs: None,
) -> Decimal:
    return parent.policies.min_acceptance_severity or DEFAULT_MIN_SEVERITY
