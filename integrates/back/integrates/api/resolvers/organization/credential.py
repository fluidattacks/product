from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.credentials.types import (
    Credentials,
    CredentialsRequest,
)
from integrates.db_model.organizations.types import (
    Organization,
)

from .schema import (
    ORGANIZATION,
)


@ORGANIZATION.field("credential")
async def resolve(
    parent: Organization,
    info: GraphQLResolveInfo,
    **kwargs: str,
) -> Credentials | None:
    credential_id = kwargs.get("id")
    loaders: Dataloaders = info.context.loaders

    if credential_id:
        credential_stats = CredentialsRequest(id=credential_id, organization_id=parent.id)
        return await loaders.credentials.load(credential_stats)

    return None
