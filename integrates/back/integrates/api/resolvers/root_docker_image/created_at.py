from datetime import (
    datetime,
)

from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.db_model.roots.types import (
    RootDockerImage,
)

from .schema import (
    ROOT_DOCKER_IMAGE,
)


@ROOT_DOCKER_IMAGE.field("createdAt")
def resolve(parent: RootDockerImage, _info: GraphQLResolveInfo, **__: None) -> datetime | None:
    return parent.created_at
