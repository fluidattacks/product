from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.custom_utils.groups import get_group
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.credentials.types import (
    Credentials,
    CredentialsRequest,
)
from integrates.db_model.roots.types import (
    RootDockerImage,
)

from .schema import (
    ROOT_DOCKER_IMAGE,
)


@ROOT_DOCKER_IMAGE.field("credentials")
async def resolve(parent: RootDockerImage, info: GraphQLResolveInfo) -> Credentials | None:
    if not parent.state.credential_id:
        return None
    loaders: Dataloaders = info.context.loaders
    group = await get_group(loaders, parent.group_name)
    request = CredentialsRequest(
        id=parent.state.credential_id,
        organization_id=group.organization_id,
    )
    return await loaders.credentials.load(request)
