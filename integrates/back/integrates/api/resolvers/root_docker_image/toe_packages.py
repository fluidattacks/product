from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.roots.types import (
    RootDockerImage,
)
from integrates.db_model.toe_packages.types import (
    RootDockerImagePackagesRequest,
    RootDockerImagePkg,
    ToePackageRequest,
)
from integrates.db_model.toe_packages.types import (
    ToePackage as ToePackageNew,
)

from .schema import (
    ROOT_DOCKER_IMAGE,
)


@ROOT_DOCKER_IMAGE.field("toePackages")
async def resolve(
    parent: RootDockerImage,
    _info: GraphQLResolveInfo,
    **__: None,
) -> list[ToePackageNew | None]:
    loaders: Dataloaders = _info.context.loaders
    root_docker_image_pkgs_ref: list[
        RootDockerImagePkg
    ] = await _info.context.loaders.root_image_toe_packages.load(
        RootDockerImagePackagesRequest(uri=parent.uri, root_id=parent.root_id),
    )
    return await loaders.toe_package.load_many(
        ToePackageRequest(
            group_name=parent.group_name,
            root_id=parent.root_id,
            name=x.name,
            version=x.version,
        )
        for x in root_docker_image_pkgs_ref
    )
