from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.db_model.roots.types import (
    DockerImageHistory,
    RootDockerImage,
)

from .schema import (
    ROOT_DOCKER_IMAGE,
)


@ROOT_DOCKER_IMAGE.field("history")
def resolve(
    parent: RootDockerImage,
    _info: GraphQLResolveInfo,
    **__: None,
) -> list[DockerImageHistory]:
    return parent.state.history
