from typing import (
    NotRequired,
    TypedDict,
    Unpack,
)

from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.class_types.types import (
    Item,
)
from integrates.db_model.items import ToePackagesItem
from integrates.db_model.roots.types import (
    RootDockerImage,
)
from integrates.db_model.toe_packages.types import (
    ToePackageEdge,
    ToePackagesConnection,
)
from integrates.db_model.toe_packages.utils import (
    format_toe_package,
)
from integrates.decorators import (
    validate_connection,
)
from integrates.search.enums import (
    Sort,
)
from integrates.search.operations import (
    SearchClient,
    SearchParams,
)

from .schema import (
    ROOT_DOCKER_IMAGE,
)


class ResolverArgs(TypedDict):
    after: NotRequired[str | None]
    first: NotRequired[int | None]
    outdated: NotRequired[bool | None]
    platform: NotRequired[str | None]
    search: NotRequired[str | None]
    vulnerable: NotRequired[bool | None]


@ROOT_DOCKER_IMAGE.field("toePackagesConnection")
@validate_connection
async def resolve(
    parent: RootDockerImage,
    _info: GraphQLResolveInfo,
    **kwargs: Unpack[ResolverArgs],
) -> ToePackagesConnection:
    def _get_must_filters() -> list[Item]:
        must_filters: list[Item] = [{"be_present": True}]

        vulnerable = kwargs.get("vulnerable")
        if vulnerable is not None:
            must_filters.append({"vulnerable": vulnerable})

        outdated = kwargs.get("outdated")
        if outdated is not None:
            must_filters.append({"outdated": outdated})

        return must_filters

    def _get_must_match_prefix_filters() -> list[dict[str, str]]:
        must_match_filters = []

        if platform := kwargs.get("platform"):
            must_match_filters.append({"platform": platform})

        return must_match_filters

    def _get_should_filters() -> list[Item]:
        should_filters = {}

        platforms = kwargs.get("platforms")
        if isinstance(platforms, list) and platforms:
            should_filters["platform"] = " OR ".join(platforms)

        return [should_filters]

    first = kwargs.get("first")
    results = await SearchClient[ToePackagesItem].search(
        SearchParams(
            after=kwargs.get("after"),
            exact_filters={
                "group_name": parent.group_name,
                "locations.image_ref.keyword": parent.uri,
                "root_id.keyword": parent.root_id,
            },
            index_value="pkgs_index",
            limit=first if first is not None else 10,
            must_filters=_get_must_filters(),
            must_match_prefix_filters=_get_must_match_prefix_filters(),
            query=kwargs.get("search"),
            should_filters=_get_should_filters(),
            sort_by=[
                {"vulnerable": {"order": Sort.DESCENDING.value}},
                {"outdated": {"order": Sort.DESCENDING.value}},
                {"name.keyword": {"order": Sort.ASCENDING.value}},
            ],
            type_query="phrase_prefix",
        ),
    )
    return ToePackagesConnection(
        edges=tuple(
            ToePackageEdge(
                cursor=results.page_info.end_cursor,
                node=format_toe_package(item),
            )
            for item in results.items
        ),
        page_info=results.page_info,
        total=results.total,
    )
