from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.db_model.roots.types import (
    RootDockerImage,
)

from .schema import (
    ROOT_DOCKER_IMAGE,
)


@ROOT_DOCKER_IMAGE.field("uri")
def resolve(parent: RootDockerImage, _info: GraphQLResolveInfo, **__: None) -> str:
    return parent.uri
