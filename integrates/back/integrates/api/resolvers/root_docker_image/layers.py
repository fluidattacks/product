from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.db_model.roots.types import (
    RootDockerImage,
)

from .schema import (
    ROOT_DOCKER_IMAGE,
)


@ROOT_DOCKER_IMAGE.field("layers")
def resolve(parent: RootDockerImage, _info: GraphQLResolveInfo, **__: None) -> list[str]:
    return parent.state.layers
