from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.forces import (
    domain as forces_domain,
)

from .schema import (
    FORCES_EXECUTION,
)


@FORCES_EXECUTION.field("log")
async def resolve(
    parent: dict[str, str],
    _info: GraphQLResolveInfo,
    **_kwargs: None,
) -> str:
    group_name = str(parent["group_name"])
    execution_id = str(parent["execution_id"])

    return await forces_domain.get_log_execution(group_name, execution_id)
