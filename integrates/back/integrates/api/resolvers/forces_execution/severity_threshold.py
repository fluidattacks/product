from graphql.type.definition import (
    GraphQLResolveInfo,
)

from .schema import (
    FORCES_EXECUTION,
)


@FORCES_EXECUTION.field("severityThreshold")
def resolve(
    parent: dict[str, float],
    _info: GraphQLResolveInfo,
    **_kwargs: None,
) -> float:
    return float(str(parent["severity_threshold"])) if parent.get("severity_threshold") else 0.0
