from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.class_types.types import (
    Item,
)
from integrates.forces import (
    domain as forces_domain,
)

from .schema import (
    FORCES_EXECUTION,
)


@FORCES_EXECUTION.field("vulnerabilities")
async def resolve(
    parent: Item,
    _info: GraphQLResolveInfo,
    **_kwargs: None,
) -> Item:
    group_name = str(parent["group_name"])
    execution_id = str(parent["execution_id"])
    vulnerabilities = parent.get("vulnerabilities", {})

    return {
        **vulnerabilities,
        **await forces_domain.get_vulns_execution(group_name, execution_id),
    }
