from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.forces import (
    domain as forces_domain,
)

from .schema import (
    FORCES_EXECUTION,
)


@FORCES_EXECUTION.field("jsonLog")
async def resolve(
    parent: dict[str, str],
    _info: GraphQLResolveInfo,
    **_kwargs: None,
) -> dict[str, str] | None:
    group_name = str(parent["group_name"])
    execution_id = str(parent["execution_id"])

    return await forces_domain.get_json_log_execution(group_name, execution_id)
