from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.db_model.groups.enums import (
    GroupTier,
)
from integrates.db_model.groups.types import (
    Group,
)

from .schema import (
    GROUP,
)


@GROUP.field("tier")
def resolve(
    parent: Group,
    _info: GraphQLResolveInfo,
) -> GroupTier:
    return parent.state.tier
