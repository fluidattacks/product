from datetime import datetime, timedelta

from integrates.api.resolvers.group.forces_exp_date import resolve
from integrates.custom_utils import datetime as datetime_utils
from integrates.decorators import enforce_group_level_auth_async, require_login
from integrates.sessions.domain import encode_token
from integrates.testing.aws import (
    IntegratesAws,
    IntegratesDynamodb,
)
from integrates.testing.fakers import (
    GraphQLResolveInfoFaker,
    GroupAccessFaker,
    GroupAccessStateFaker,
    GroupFaker,
    OrganizationAccessFaker,
    OrganizationFaker,
    StakeholderFaker,
)
from integrates.testing.mocks import mocks
from integrates.testing.utils import freeze_time

ORG_ID = "ORG#40f6da5f-4f66-4bf0-825b-a2d9748ad6db"
GROUP_NAME = "grouptest"
USER_EMAIL = "jdoe@fluidattacks.com"
ORG_NAME = "orgtest"


@freeze_time("2025-01-09T05:00:00+00:00")
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            groups=[GroupFaker(name=GROUP_NAME, organization_id=ORG_ID)],
            group_access=[
                GroupAccessFaker(
                    email=USER_EMAIL,
                    group_name=GROUP_NAME,
                    state=GroupAccessStateFaker(role="customer_manager"),
                )
            ],
            stakeholders=[
                StakeholderFaker(email=USER_EMAIL, enrolled=True, role="customer_manager")
            ],
            organization_access=[OrganizationAccessFaker(organization_id=ORG_ID, email=USER_EMAIL)],
            organizations=[OrganizationFaker(id=ORG_ID, name=ORG_NAME)],
        )
    )
)
async def test_group_forces_exp_date() -> None:
    # Act
    parent = GroupFaker(
        agent_token=encode_token(
            expiration_time=datetime_utils.get_as_epoch(datetime.now() + timedelta(seconds=60.0)),
            payload={
                "user_email": USER_EMAIL,
                "first_name": "john",
                "last_name": "doe",
            },
            subject="api_token",
            api=True,
        ),
        name=GROUP_NAME,
        organization_id=ORG_ID,
    )
    info = GraphQLResolveInfoFaker(
        user_email=USER_EMAIL,
        decorators=[[enforce_group_level_auth_async], [require_login]],
    )

    result = await resolve(parent=parent, info=info, _info=info, group_name=GROUP_NAME)

    # Assert
    assert result
    assert result == "2025-01-09"
