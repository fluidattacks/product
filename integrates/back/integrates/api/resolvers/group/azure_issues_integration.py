from graphql import (
    GraphQLResolveInfo,
)

from integrates.db_model.groups.types import (
    Group,
    GroupAzureIssues,
)

from .schema import (
    GROUP,
)


@GROUP.field("azureIssuesIntegration")
def resolve(
    parent: Group,
    _info: GraphQLResolveInfo,
) -> GroupAzureIssues | None:
    return parent.azure_issues
