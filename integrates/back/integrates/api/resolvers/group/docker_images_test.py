from integrates.api.resolvers.group.docker_images import resolve
from integrates.dataloaders import get_new_context
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    GitRootFaker,
    GraphQLResolveInfoFaker,
    GroupFaker,
    OrganizationAccessFaker,
    OrganizationFaker,
    RootDockerImageFaker,
    StakeholderFaker,
)
from integrates.testing.mocks import mocks

ORG_ID = "ORG#40f6da5f-4f66-4bf0-825b-a2d9748ad6db"
ROOT_ID = "38eb8f25-7945-4173-ab6e-0af4ad8b7ef3"
GROUP_NAME = "grouptest"
USER_EMAIL = "jdoe@orgtest.com"
ORG_NAME = "orgtest"


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            docker_images=[
                RootDockerImageFaker(root_id=ROOT_ID, group_name=GROUP_NAME, created_by=USER_EMAIL)
            ],
            groups=[GroupFaker(name=GROUP_NAME, organization_id=ORG_ID)],
            stakeholders=[StakeholderFaker(email=USER_EMAIL, enrolled=True)],
            organization_access=[OrganizationAccessFaker(organization_id=ORG_ID, email=USER_EMAIL)],
            organizations=[OrganizationFaker(id=ORG_ID, name=ORG_NAME)],
            roots=[GitRootFaker(id=ROOT_ID, group_name=GROUP_NAME)],
        )
    )
)
async def test_group_docker_images() -> None:
    # Act
    loaders = get_new_context()
    parent = await loaders.group.load(GROUP_NAME)
    info = GraphQLResolveInfoFaker(user_email=USER_EMAIL)

    result = await resolve(parent=parent, info=info)

    # Assert
    assert result
    assert len(result) == 1
    assert result[0].uri == "docker.io/test_image"
    assert result[0].root_id == ROOT_ID
    assert result[0].group_name == GROUP_NAME
