from typing import (
    TypedDict,
    Unpack,
)

from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.class_types.types import (
    Item,
)
from integrates.custom_utils.validations_deco import (
    validate_all_fields_length_deco,
    validate_fields_deco,
)
from integrates.db_model.groups.types import (
    Group,
)
from integrates.db_model.items import IpRootItem
from integrates.db_model.roots.types import (
    IPRootEdge,
    IPRootsConnection,
)
from integrates.db_model.roots.utils import (
    format_ip_root,
)
from integrates.search.operations import (
    SearchClient,
    SearchParams,
)

from .schema import (
    GROUP,
)


class Kwargs(TypedDict, total=False):
    after: str
    first: int
    search: str


def _get_exact_filters(group_name: str) -> Item:
    exact_filters = {
        "sk.keyword": f"GROUP#{group_name}",
        "type.keyword": "IP",
    }

    return exact_filters


@GROUP.field("ipRoots")
@validate_all_fields_length_deco(max_length=300)
@validate_fields_deco(
    [
        "after",
        "search",
    ],
)
async def resolve(
    parent: Group,
    _info: GraphQLResolveInfo,
    **kwargs: Unpack[Kwargs],
) -> IPRootsConnection:
    group_name = parent.name
    results = await SearchClient[IpRootItem].search(
        SearchParams(
            after=kwargs.get("after"),
            exact_filters=_get_exact_filters(group_name),
            index_value="roots_index",
            type_query="phrase_prefix",
            limit=kwargs.get("first") or 100,
            query=kwargs.get("search"),
        ),
    )
    return IPRootsConnection(
        edges=tuple(
            IPRootEdge(
                cursor=results.page_info.end_cursor,
                node=format_ip_root(item),
            )
            for item in results.items
        ),
        page_info=results.page_info,
        total=results.total,
    )
