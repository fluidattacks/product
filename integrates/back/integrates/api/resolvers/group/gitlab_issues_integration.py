from graphql import (
    GraphQLResolveInfo,
)

from integrates.db_model.groups.types import (
    Group,
)

from .schema import (
    GROUP,
)


@GROUP.field("gitlabIssuesIntegration")
def resolve(
    parent: Group,
    _info: GraphQLResolveInfo,
) -> dict | None:
    if parent.gitlab_issues is None:
        return None
    return {**parent.gitlab_issues._asdict(), "group_name": parent.name}
