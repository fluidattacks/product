from decimal import (
    Decimal,
)

from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.groups.types import (
    Group,
)
from integrates.groups.domain import (
    get_total_priority_findings,
)

from .schema import (
    GROUP,
)


@GROUP.field("totalOpenPriority")
async def resolve(
    parent: Group,
    info: GraphQLResolveInfo,
    **_kwargs: None,
) -> Decimal:
    loaders: Dataloaders = info.context.loaders
    return await get_total_priority_findings(loaders, parent.name)
