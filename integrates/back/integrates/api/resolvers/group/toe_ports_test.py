from integrates.api.resolvers.group.toe_ports import resolve
from integrates.dataloaders import get_new_context
from integrates.decorators import (
    enforce_group_level_auth_async,
    require_attribute_internal,
    validate_connection,
)
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    GraphQLResolveInfoFaker,
    GroupAccessFaker,
    GroupAccessStateFaker,
    GroupFaker,
    IPRootFaker,
    OrganizationAccessFaker,
    OrganizationFaker,
    StakeholderFaker,
    ToePortFaker,
)
from integrates.testing.mocks import mocks
from integrates.testing.utils import raises

ORG_ID = "ORG#40f6da5f-4f66-4bf0-825b-a2d9748ad6db"
ROOT_ID = "38eb8f25-7945-4173-ab6e-0af4ad8b7ef3"
GROUP_NAME = "grouptest"
USER_EMAIL = "jdoe@orgtest.com"
ORG_NAME = "orgtest"


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            groups=[GroupFaker(name=GROUP_NAME, organization_id=ORG_ID)],
            group_access=[
                GroupAccessFaker(
                    email=USER_EMAIL,
                    group_name=GROUP_NAME,
                    state=GroupAccessStateFaker(role="group_manager"),
                ),
            ],
            stakeholders=[StakeholderFaker(email=USER_EMAIL, enrolled=True)],
            organization_access=[OrganizationAccessFaker(organization_id=ORG_ID, email=USER_EMAIL)],
            organizations=[OrganizationFaker(id=ORG_ID, name=ORG_NAME)],
            roots=[IPRootFaker(id=ROOT_ID, group_name=GROUP_NAME, organization_name=ORG_NAME)],
            toe_ports=[ToePortFaker(group_name=GROUP_NAME, root_id=ROOT_ID)],
        )
    )
)
async def test_group_toe_ports() -> None:
    # Act
    loaders = get_new_context()
    parent = await loaders.group.load(GROUP_NAME)
    info = GraphQLResolveInfoFaker(
        user_email=USER_EMAIL,
        decorators=[
            [enforce_group_level_auth_async],
            [validate_connection],
            [require_attribute_internal, "is_under_review", GROUP_NAME],
        ],
    )

    result = await resolve(parent=parent, info=info, group_name=GROUP_NAME, root_id=ROOT_ID)

    # Assert
    assert result
    assert len(result.edges) == 1
    assert result.edges[0].node.root_id == ROOT_ID
    assert result.edges[0].node.group_name == GROUP_NAME
    assert result.edges[0].node.address == "192.168.1.1"
    assert result.edges[0].node.port == "8080"


NOT_GROUP_USER = "user@fluidattacks.com"


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id=ORG_ID, name=ORG_NAME)],
            stakeholders=[StakeholderFaker(email=NOT_GROUP_USER, enrolled=True)],
            organization_access=[
                OrganizationAccessFaker(organization_id=ORG_ID, email=NOT_GROUP_USER)
            ],
            groups=[GroupFaker(name=GROUP_NAME, organization_id=ORG_ID)],
            roots=[IPRootFaker(id=ROOT_ID, group_name=GROUP_NAME, organization_name=ORG_NAME)],
            toe_ports=[ToePortFaker(group_name=GROUP_NAME, root_id=ROOT_ID)],
        )
    )
)
async def test_group_toe_ports_access_denied() -> None:
    loaders = get_new_context()
    parent = await loaders.group.load(GROUP_NAME)
    info = GraphQLResolveInfoFaker(
        user_email=NOT_GROUP_USER,
        decorators=[
            [enforce_group_level_auth_async],
            [validate_connection],
            [require_attribute_internal, "is_under_review", GROUP_NAME],
        ],
    )
    with raises(Exception, match="Access denied"):
        await resolve(parent=parent, info=info, group_name=GROUP_NAME, root_id=ROOT_ID)
