import logging
import logging.config
from typing import (
    NotRequired,
    TypedDict,
    Unpack,
)

from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.authz import (
    FLUID_IDENTIFIER,
)
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.findings.types import (
    Finding,
)
from integrates.db_model.groups.types import (
    Group,
)
from integrates.db_model.vulnerabilities.enums import (
    VulnerabilityTechnique,
)
from integrates.decorators import (
    concurrent_decorators,
    require_asm,
    require_is_not_under_review,
)
from integrates.findings.domain import (
    get_filtered_findings,
)
from integrates.sessions import (
    domain as sessions_domain,
)

from .schema import (
    GROUP,
)

LOGGER = logging.getLogger(__name__)


class ResolverArgs(TypedDict):
    root: NotRequired[str | None]
    technique: NotRequired[VulnerabilityTechnique | None]
    title: NotRequired[str | None]


@GROUP.field("findings")
@concurrent_decorators(require_asm, require_is_not_under_review)
async def resolve(
    parent: Group,
    info: GraphQLResolveInfo,
    **kwargs: Unpack[ResolverArgs],
) -> list[Finding]:
    user_info: dict[str, str] = await sessions_domain.get_jwt_content(info.context)
    group_name: str = parent.name
    user_email: str = user_info["user_email"]
    has_internal_role: bool = user_email.endswith(FLUID_IDENTIFIER)
    root = kwargs.get("root")
    technique = kwargs.get("technique")
    title = kwargs.get("title")
    loaders: Dataloaders = info.context.loaders

    findings = await get_filtered_findings(
        group_name=group_name,
        loaders=loaders,
        root=root,
        technique=technique,
        title=title,
        has_internal_role=has_internal_role,
    )

    return findings
