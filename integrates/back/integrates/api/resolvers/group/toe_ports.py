from typing import (
    TypedDict,
    Unpack,
)

from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.groups.types import (
    Group,
)
from integrates.db_model.toe_ports.types import (
    GroupToePortsRequest,
    RootToePortsRequest,
    ToePortsConnection,
)
from integrates.decorators import (
    concurrent_decorators,
    enforce_group_level_auth_async,
    require_is_not_under_review,
    validate_connection,
)

from .schema import (
    GROUP,
)


class ToePortsArgs(TypedDict):
    root_id: str | None
    after: str | None
    be_present: bool | None
    first: int | None


@GROUP.field("toePorts")
@concurrent_decorators(
    enforce_group_level_auth_async,
    validate_connection,
    require_is_not_under_review,
)
async def resolve(
    parent: Group,
    info: GraphQLResolveInfo,
    **kwargs: Unpack[ToePortsArgs],
) -> ToePortsConnection:
    loaders: Dataloaders = info.context.loaders
    group_name: str = parent.name
    root_id = kwargs.get("root_id")
    after = kwargs.get("after")
    be_present = kwargs.get("be_present")
    first = kwargs.get("first")

    if root_id is not None:
        return await loaders.root_toe_ports.load(
            RootToePortsRequest(
                group_name=group_name,
                root_id=root_id,
                after=after,
                be_present=be_present,
                first=first,
                paginate=True,
            ),
        )

    return await loaders.group_toe_ports.load(
        GroupToePortsRequest(
            group_name=group_name,
            after=after,
            be_present=be_present,
            first=first,
            paginate=True,
        ),
    )
