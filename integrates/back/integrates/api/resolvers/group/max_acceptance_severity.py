from decimal import (
    Decimal,
)

from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.db_model.groups.types import (
    Group,
)
from integrates.vulnerabilities.domain.validations import (
    get_policy_max_acceptance_severity,
)

from .schema import (
    GROUP,
)


@GROUP.field("maxAcceptanceSeverity")
async def resolve(
    parent: Group,
    info: GraphQLResolveInfo,
    **_kwargs: None,
) -> Decimal:
    return await get_policy_max_acceptance_severity(
        loaders=info.context.loaders,
        group_name=parent.name,
    )
