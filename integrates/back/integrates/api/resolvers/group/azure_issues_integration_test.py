from integrates.api.resolvers.group.azure_issues_integration import resolve
from integrates.dataloaders import get_new_context
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    GraphQLResolveInfoFaker,
    GroupAzureIssuesFaker,
    GroupFaker,
    OrganizationAccessFaker,
    OrganizationFaker,
    StakeholderFaker,
)
from integrates.testing.mocks import mocks

ORG_ID = "ORG#40f6da5f-4f66-4bf0-825b-a2d9748ad6db"
GROUP_NAME = "grouptest"
USER_EMAIL = "jdoe@orgtest.com"
ORG_NAME = "orgtest"


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            groups=[
                GroupFaker(
                    name=GROUP_NAME,
                    organization_id=ORG_ID,
                    azure_issues=GroupAzureIssuesFaker(assigned_to=USER_EMAIL),
                )
            ],
            stakeholders=[StakeholderFaker(email=USER_EMAIL, enrolled=True)],
            organization_access=[OrganizationAccessFaker(organization_id=ORG_ID, email=USER_EMAIL)],
            organizations=[OrganizationFaker(id=ORG_ID, name=ORG_NAME)],
        )
    )
)
async def test_group_azure_issues_integration() -> None:
    # Act
    loaders = get_new_context()
    parent = await loaders.group.load(GROUP_NAME)
    info = GraphQLResolveInfoFaker(user_email=USER_EMAIL)

    result = resolve(parent=parent, _info=info)

    # Assert
    assert result
    assert result.redirect_uri == "https://testing.com"
    assert result.assigned_to == USER_EMAIL
