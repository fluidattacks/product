from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.db_model.groups.enums import (
    GroupStateStatus,
)
from integrates.db_model.groups.types import (
    Group,
)

from .schema import (
    GROUP,
)


@GROUP.field("userDeletion")
def resolve(
    parent: Group,
    _info: GraphQLResolveInfo,
) -> str | None:
    return parent.state.modified_by if parent.state.status == GroupStateStatus.DELETED else None
