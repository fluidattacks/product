from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.groups.types import (
    Group,
)
from integrates.db_model.toe_packages.types import (
    ToePackage,
    ToePackageRequest,
)

from .schema import (
    GROUP,
)


@GROUP.field("package")
async def resolve(
    parent: Group,
    info: GraphQLResolveInfo,
    *,
    name: str,
    root_id: str,
    version: str,
) -> ToePackage | None:
    loaders: Dataloaders = info.context.loaders
    package = await loaders.toe_package.load(
        ToePackageRequest(
            group_name=parent.name,
            root_id=root_id,
            name=name,
            version=version,
        ),
    )

    return package
