from integrates.api.resolvers.group.code_languages import resolve
from integrates.db_model.groups.types import GroupUnreliableIndicators
from integrates.db_model.types import CodeLanguage
from integrates.decorators import require_attribute, require_attribute_internal
from integrates.testing.aws import (
    GroupUnreliableIndicatorsToUpdate,
    IntegratesAws,
    IntegratesDynamodb,
)
from integrates.testing.fakers import (
    GraphQLResolveInfoFaker,
    GroupAccessFaker,
    GroupAccessStateFaker,
    GroupFaker,
    OrganizationAccessFaker,
    OrganizationFaker,
    StakeholderFaker,
)
from integrates.testing.mocks import mocks

ORG_ID = "ORG#40f6da5f-4f66-4bf0-825b-a2d9748ad6db"
GROUP_NAME = "grouptest"
USER_EMAIL = "jdoe@orgtest.com"
ORG_NAME = "orgtest"


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            groups=[GroupFaker(name=GROUP_NAME, organization_id=ORG_ID)],
            group_access=[
                GroupAccessFaker(
                    email=USER_EMAIL,
                    group_name=GROUP_NAME,
                    state=GroupAccessStateFaker(role="user"),
                )
            ],
            stakeholders=[StakeholderFaker(email=USER_EMAIL, enrolled=True)],
            organization_access=[OrganizationAccessFaker(organization_id=ORG_ID, email=USER_EMAIL)],
            organizations=[OrganizationFaker(id=ORG_ID, name=ORG_NAME)],
            group_unreliable_indicators=[
                GroupUnreliableIndicatorsToUpdate(
                    group_name=GROUP_NAME,
                    indicators=GroupUnreliableIndicators(
                        code_languages=[CodeLanguage("python", 100), CodeLanguage("GraphQL", 150)]
                    ),
                )
            ],
        )
    )
)
async def test_group_code_languages() -> None:
    # Act
    parent = GroupFaker(name=GROUP_NAME, organization_id=ORG_ID)
    info = GraphQLResolveInfoFaker(
        user_email=USER_EMAIL,
        decorators=[
            [require_attribute_internal, "is_under_review", GROUP_NAME],
            [require_attribute, "has_asm", GROUP_NAME],
        ],
    )

    result = await resolve(parent=parent, info=info, group_name=GROUP_NAME)

    # Assert
    assert result
    assert result == [CodeLanguage("python", 100), CodeLanguage("GraphQL", 150)]
