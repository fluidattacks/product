from integrates.api.resolvers.group.roots import resolve
from integrates.dataloaders import get_new_context
from integrates.db_model.roots.enums import RootType
from integrates.decorators import require_attribute_internal
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    GitRootFaker,
    GraphQLResolveInfoFaker,
    GroupAccessFaker,
    GroupAccessStateFaker,
    GroupFaker,
    IPRootFaker,
    OrganizationAccessFaker,
    OrganizationFaker,
    StakeholderFaker,
    UrlRootFaker,
    random_uuid,
)
from integrates.testing.mocks import mocks

ORG_ID = "ORG#40f6da5f-4f66-4bf0-825b-a2d9748ad6db"
GROUP_NAME = "grouptest"
USER_EMAIL = "jdoe@orgtest.com"
ORG_NAME = "orgtest"


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            groups=[GroupFaker(name=GROUP_NAME, organization_id=ORG_ID)],
            group_access=[
                GroupAccessFaker(
                    email=USER_EMAIL,
                    group_name=GROUP_NAME,
                    state=GroupAccessStateFaker(role="user"),
                )
            ],
            stakeholders=[StakeholderFaker(email=USER_EMAIL, enrolled=True)],
            organization_access=[OrganizationAccessFaker(organization_id=ORG_ID, email=USER_EMAIL)],
            organizations=[OrganizationFaker(id=ORG_ID, name=ORG_NAME)],
            roots=[
                GitRootFaker(id=random_uuid(), group_name=GROUP_NAME, organization_name=ORG_NAME),
                IPRootFaker(id=random_uuid(), group_name=GROUP_NAME, organization_name=ORG_NAME),
                UrlRootFaker(
                    root_id=random_uuid(), group_name=GROUP_NAME, organization_name=ORG_NAME
                ),
            ],
        )
    )
)
async def test_group_roots() -> None:
    # Act
    loaders = get_new_context()
    parent = await loaders.group.load(GROUP_NAME)
    info = GraphQLResolveInfoFaker(
        user_email=USER_EMAIL,
        decorators=[[require_attribute_internal, "is_under_review", GROUP_NAME]],
    )

    result = await resolve(parent=parent, info=info, group_name=GROUP_NAME)

    # Assert
    assert result
    assert len(result) == 3
    assert sorted(root.type for root in result) == [RootType.GIT, RootType.IP, RootType.URL]
