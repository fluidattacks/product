from typing import (
    Any,
)

from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.class_types.types import (
    Item,
)
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.groups.types import (
    Group,
)
from integrates.db_model.items import ToeInputItem
from integrates.db_model.toe_inputs.types import (
    ToeInputEdge,
    ToeInputsConnection,
)
from integrates.db_model.toe_inputs.utils import (
    format_toe_input,
)
from integrates.decorators import (
    concurrent_decorators,
    enforce_group_level_auth_async,
    require_is_not_under_review,
    validate_connection,
)
from integrates.roots.domain import (
    get_root_id_by_nickname,
)
from integrates.search.operations import (
    SearchClient,
    SearchParams,
)

from .schema import (
    GROUP,
)


@GROUP.field("toeInputs")
@concurrent_decorators(
    enforce_group_level_auth_async,
    require_is_not_under_review,
    validate_connection,
)
async def resolve(
    parent: Group,
    info: GraphQLResolveInfo,
    **kwargs: Any,
) -> ToeInputsConnection:
    loaders: Dataloaders = info.context.loaders
    roots = await loaders.group_roots.load(parent.name)

    def get_items_to_filter(
        filters: Item,
        kwargs: Any,
        parameter: str | None = None,
        range_condition: str | None = None,
    ) -> list[dict[str, str]]:
        items_to_filter = [
            {
                (field if path == "common" else f"{path}.{field}"): (
                    {range_condition: filter_value} if range_condition else filter_value
                ),
            }
            for path, fields in filters.items()
            for field in fields
            if (filter_value := kwargs.get(f"{parameter}_{field}" if parameter else field))
            not in [None, ""]
        ]
        return items_to_filter

    def must_filter(**kwargs: str) -> list[dict[str, str]]:
        filters: Item = {
            "common": ["component", "root_id", "environment_id"],
            "state": [
                "be_present",
                "has_vulnerabilities",
            ],
        }
        if root_nick := kwargs.get("root_nickname"):
            root_id = get_root_id_by_nickname(root_nick, roots)
            kwargs["root_id"] = root_id

        must_filters = get_items_to_filter(filters, kwargs)

        return must_filters

    def must_match_prefix_filter(**kwargs: str) -> list[dict[str, str]]:
        filters: Item = {
            "common": ["entry_point"],
            "state": ["seen_first_time_by", "attacked_by"],
        }

        must_match_filters = get_items_to_filter(filters, kwargs)

        return must_match_filters

    def must_range_filter(**kwargs: str) -> list[dict[str, str]]:
        from_to_filters: Item = {
            "state": [
                "seen_at",
                "attacked_at",
                "first_attack_at",
                "be_present_until",
            ],
        }

        must_range_filters: list[Item] = [
            *get_items_to_filter(from_to_filters, kwargs, "from", "gte"),
            *get_items_to_filter(from_to_filters, kwargs, "to", "lte"),
        ]

        return must_range_filters

    def toe_inputs_filter(**kwargs: str) -> Item:
        inputs_must_filters: list[dict[str, str]] = must_filter(**kwargs)
        inputs_must_match_prefix_filters: list[dict[str, str]] = must_match_prefix_filter(**kwargs)
        inputs_must_range_filters: list[Item] = must_range_filter(**kwargs)

        filters: Item = {
            "must_filters": inputs_must_filters,
            "must_match_filters": inputs_must_match_prefix_filters,
            "must_range_filters": inputs_must_range_filters,
        }

        return filters

    def _get_should_filters() -> list[Item]:
        should_filters = []

        components = kwargs.get("components")
        if isinstance(components, list) and components:
            should_filters.extend([{"component.keyword": component} for component in components])

        return should_filters

    toe_inputs_filters: Item = toe_inputs_filter(**kwargs)

    results = await SearchClient[ToeInputItem].search(
        SearchParams(
            after=kwargs.get("after"),
            exact_filters={"group_name": parent.name},
            must_filters=toe_inputs_filters["must_filters"],
            must_match_prefix_filters=toe_inputs_filters["must_match_filters"],
            range_filters=toe_inputs_filters["must_range_filters"],
            should_filters=_get_should_filters(),
            index_value="inputs_index",
            type_query="phrase_prefix",
            limit=kwargs.get("first") or 10,
            query=kwargs.get("search"),
        ),
    )

    toe_inputs = tuple(format_toe_input(parent.name, toe_input) for toe_input in results.items)

    inputs = ToeInputsConnection(
        edges=tuple(
            ToeInputEdge(
                cursor=results.page_info.end_cursor,
                node=toe_input,
            )
            for toe_input in toe_inputs
        ),
        page_info=results.page_info,
        total=results.total,
    )

    return inputs
