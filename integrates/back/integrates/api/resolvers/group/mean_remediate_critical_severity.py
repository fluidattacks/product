from decimal import (
    Decimal,
)

from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.groups.types import (
    Group,
    GroupUnreliableIndicators,
)
from integrates.decorators import (
    require_asm,
)

from .schema import (
    GROUP,
)


@GROUP.field("meanRemediateCriticalSeverity")
@require_asm
async def resolve(
    parent: Group,
    info: GraphQLResolveInfo,
    **_kwargs: None,
) -> Decimal | None:
    loaders: Dataloaders = info.context.loaders
    group_name: str = parent.name
    group_indicators: GroupUnreliableIndicators = await loaders.group_unreliable_indicators.load(
        group_name,
    )
    return group_indicators.mean_remediate_critical_severity
