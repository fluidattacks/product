from datetime import datetime

from integrates.api.resolvers.group.toe_lines_connection import resolve
from integrates.dataloaders import get_new_context
from integrates.decorators import (
    enforce_group_level_auth_async,
    require_attribute_internal,
    validate_connection,
)
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb, IntegratesOpensearch
from integrates.testing.fakers import (
    GitRootFaker,
    GitRootStateFaker,
    GraphQLResolveInfoFaker,
    GroupAccessFaker,
    GroupAccessStateFaker,
    GroupFaker,
    OrganizationAccessFaker,
    OrganizationFaker,
    StakeholderFaker,
    ToeLinesFaker,
    ToeLinesStateFaker,
)
from integrates.testing.mocks import mocks
from integrates.testing.utils import parametrize

ORG_ID = "ORG#40f6da5f-4f66-4bf0-825b-a2d9748ad6db"
ROOT_ID = "38eb8f25-7945-4173-ab6e-0af4ad8b7ef3"
GROUP_NAME = "grouptest"
ADMIN_EMAIL = "admin@fluidattacks.com"

ORG_NAME = "orgtest"


@parametrize(
    args=["variable", "expected"],
    cases=[
        [{"min_loc": 200}, 1],
        [{"max_loc": 100}, 2],
        [{"min_attacked_lines": 1}, 1],
        [{"from_attacked_at": "2021-02-20T05:00:00+00:00"}, 1],
        [{"from_seen_at": "2023-02-20T05:00:00+00:00"}, 1],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            groups=[GroupFaker(name=GROUP_NAME, organization_id=ORG_ID)],
            group_access=[
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email=ADMIN_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="admin"),
                ),
            ],
            stakeholders=[StakeholderFaker(email=ADMIN_EMAIL)],
            organization_access=[OrganizationAccessFaker(organization_id=ORG_ID)],
            organizations=[OrganizationFaker(id=ORG_ID, name=ORG_NAME)],
            roots=[
                GitRootFaker(id=ROOT_ID, group_name=GROUP_NAME, organization_name=ORG_NAME),
                GitRootFaker(
                    id="7597da7c-d6a8-451a-a7a5-a7d4625b2b6c",
                    group_name=GROUP_NAME,
                    organization_name=ORG_NAME,
                    state=GitRootStateFaker(branch="main"),
                ),
            ],
            toe_lines=[
                ToeLinesFaker(
                    filename="test1/test.sh",
                    group_name=GROUP_NAME,
                    root_id=ROOT_ID,
                    state=ToeLinesStateFaker(
                        has_vulnerabilities=True,
                        attacked_lines=1,
                        attacked_at=datetime.fromisoformat("2021-02-20T05:00:00+00:00"),
                    ),
                ),
                ToeLinesFaker(
                    filename="test2/test.sh",
                    group_name=GROUP_NAME,
                    root_id=ROOT_ID,
                    state=ToeLinesStateFaker(
                        seen_at=datetime.fromisoformat("2023-02-20T05:00:00+00:00"),
                        last_commit="f9e4beb",
                        last_author="test@test.test",
                    ),
                ),
                ToeLinesFaker(
                    filename="test3/test.sh",
                    group_name=GROUP_NAME,
                    root_id=ROOT_ID,
                    state=ToeLinesStateFaker(
                        loc=200, attacked_at=datetime.fromisoformat("2021-01-20T05:00:00+00:00")
                    ),
                ),
            ],
        ),
        opensearch=IntegratesOpensearch(autoload=True),
    )
)
async def test_get_toe_lines_connection(variable: dict[str, int | bool], expected: int) -> None:
    loaders = get_new_context()
    parent = await loaders.group.load(GROUP_NAME)
    info = GraphQLResolveInfoFaker(
        user_email=ADMIN_EMAIL,
        decorators=[
            [enforce_group_level_auth_async],
            [validate_connection],
            [require_attribute_internal, "is_under_review", GROUP_NAME],
        ],
    )

    result = await resolve(parent=parent, _info=info, group_name=GROUP_NAME, **variable, info=info)

    assert result
    assert len(result.edges) == expected
