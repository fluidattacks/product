from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.db_model.groups.types import (
    Group,
)
from integrates.decorators import (
    concurrent_decorators,
    enforce_group_level_auth_async,
    require_is_not_under_review,
    require_login,
)

from .schema import (
    GROUP,
)


@GROUP.field("forcesToken")
@concurrent_decorators(require_login, enforce_group_level_auth_async, require_is_not_under_review)
def resolve(
    parent: Group,
    _info: GraphQLResolveInfo,
    **_kwargs: None,
) -> str | None:
    return parent.agent_token
