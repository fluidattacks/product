from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.db_model.groups.types import (
    Group,
)

from .schema import (
    GROUP,
)


@GROUP.field("hasEssential")
def resolve(
    parent: Group,
    _info: GraphQLResolveInfo,
) -> bool:
    return parent.state.has_essential
