from typing import (
    Any,
)

from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.class_types.types import (
    Item,
)
from integrates.db_model.groups.types import (
    Group,
)
from integrates.db_model.items import ToeLinesItem
from integrates.db_model.toe_lines.types import (
    ToeLineEdge,
    ToeLinesConnection,
)
from integrates.db_model.toe_lines.utils import (
    format_toe_lines,
)
from integrates.decorators import (
    concurrent_decorators,
    enforce_group_level_auth_async,
    rename_kwargs,
    require_is_not_under_review,
    validate_connection,
)
from integrates.search.enums import (
    Sort,
)
from integrates.search.operations import (
    SearchClient,
    SearchParams,
)
from integrates.search.types import (
    ItemSearchResponse,
    ScriptQuery,
)

from .schema import (
    GROUP,
)

DEFAULT_PAGE_SIZE = 10
INDEX_NAME = "lines_index"


@GROUP.field("toeLinesConnection")
@rename_kwargs(
    {
        "from_modified_date": "from_last_commit_date",
        "to_modified_date": "to_last_commit_date",
    },
)
@concurrent_decorators(
    enforce_group_level_auth_async,
    require_is_not_under_review,
    validate_connection,
)
async def resolve(
    parent: Group,
    _info: GraphQLResolveInfo,
    **kwargs: str,
) -> ToeLinesConnection:
    search_params = create_search_params(parent, kwargs)
    results = await SearchClient[ToeLinesItem].search(SearchParams(**search_params))
    response = create_toe_lines_connection(results)

    return response


def create_search_params(parent: Group, kwargs: Any) -> Item:
    toe_lines_filters: dict[str, str] = toe_lines_filter(**kwargs)
    sort_values = kwargs.get("sort", {})
    sort_field = sort_values.get("field", "sorts_priority_factor").lower()
    sort_order = sort_values.get("order", Sort.DESCENDING.value).lower()

    return {
        "after": kwargs.get("after"),
        "exact_filters": {"group_name": parent.name},
        "must_filters": toe_lines_filters["must_filters"],
        "must_match_prefix_filters": toe_lines_filters["must_match_filters"],
        "range_filters": toe_lines_filters["must_range_filters"],
        "index_value": INDEX_NAME,
        "limit": kwargs.get("first") or DEFAULT_PAGE_SIZE,
        "script_filters": toe_lines_filters["script_filters"],
        "sort_by": [
            {f"state.{sort_field}": {"order": sort_order}},
            {"_id": {"order": sort_order}},
        ],
    }


def create_toe_lines_connection(results: ItemSearchResponse[ToeLinesItem]) -> ToeLinesConnection:
    toe_lines = tuple(format_toe_lines(result) for result in results.items)

    edges = tuple(
        ToeLineEdge(
            cursor=results.page_info.end_cursor,
            node=toe_line,
        )
        for toe_line in toe_lines
    )

    return ToeLinesConnection(
        edges=edges,
        page_info=results.page_info,
        total=results.total,
    )


def toe_lines_filter(**kwargs: str) -> Item:
    vulns_must_filters: list[dict[str, str]] = must_filter(**kwargs)
    vulns_must_match_prefix_filters: list[dict[str, str]] = must_match_prefix_filter(**kwargs)
    exec_must_range_filters: list[dict[str, str]] = must_range_filter(**kwargs)
    script_filters: list[ScriptQuery] = _get_script_filters(**kwargs)

    filters: Item = {
        "must_filters": vulns_must_filters,
        "must_match_filters": vulns_must_match_prefix_filters,
        "must_range_filters": exec_must_range_filters,
        "script_filters": script_filters,
    }

    return filters


def get_items_to_filter(
    filters: Item,
    kwargs: Any,
    parameter: str | None = None,
    range_condition: str | None = None,
) -> list[dict[str, str]]:
    items_to_filter = [
        {
            (field if path == "common" else f"{path}.{field}"): (
                {range_condition: filter_value} if range_condition else filter_value
            ),
        }
        for path, fields in filters.items()
        for field in fields
        if (filter_value := kwargs.get(f"{parameter}_{field}" if parameter else field))
        not in [None, ""]
    ]
    return items_to_filter


def must_filter(**kwargs: str) -> list[dict[str, str]]:
    filters: Item = {
        "common": ["root_id"],
        "state": ["be_present", "has_vulnerabilities"],
    }
    must_filters = get_items_to_filter(filters, kwargs)

    return must_filters


def must_match_prefix_filter(**kwargs: Any) -> list[dict[str, str]]:
    filters: Item = {
        "common": ["filename"],
        "state": ["attacked_by", "comments", "last_commit", "last_author"],
    }

    must_match_filters = get_items_to_filter(filters, kwargs)

    return must_match_filters


def must_range_filter(**kwargs: str) -> list[dict[str, str]]:
    from_to_filters: Item = {
        "state": [
            "seen_at",
            "first_attack_at",
            "attacked_at",
            "be_present_until",
            "last_commit_date",
        ],
    }

    min_max_filters: Item = {
        "state": [
            "loc",
            "attacked_lines",
            "sorts_priority_factor",
            "sorts_risk_level",
        ],
    }

    must_range_filters: list[Item] = [
        *get_items_to_filter(from_to_filters, kwargs, "from", "gte"),
        *get_items_to_filter(min_max_filters, kwargs, "min", "gte"),
        *get_items_to_filter(from_to_filters, kwargs, "to", "lte"),
        *get_items_to_filter(min_max_filters, kwargs, "max", "lte"),
    ]

    return must_range_filters


def _get_script_filters(**kwargs: str) -> list[ScriptQuery]:
    script_filters: list[ScriptQuery] = []
    coverage = """
        doc['state.loc'].value == 0
            ? 1
            : doc['state.attacked_lines'].value / doc['state.loc'].value
    """
    coverage_formatted = f"Math.round(({coverage}) * 100)"

    if min_coverage := kwargs.get("min_coverage"):
        source = f"{coverage_formatted} >= params.min_coverage"
        params = {"min_coverage": min_coverage}
        script_filters.append(ScriptQuery(source=source, params=params))

    if max_coverage := kwargs.get("max_coverage"):
        source = f"{coverage_formatted} <= params.max_coverage"
        params = {"max_coverage": max_coverage}
        script_filters.append(ScriptQuery(source=source, params=params))

    return script_filters
