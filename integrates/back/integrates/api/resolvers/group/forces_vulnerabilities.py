from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.custom_utils.vulnerabilities import (
    get_inverted_state_converted,
)
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.groups.types import (
    Group,
)
from integrates.db_model.vulnerabilities.constants import (
    RELEASED_FILTER_STATUSES,
)
from integrates.db_model.vulnerabilities.enums import (
    VulnerabilityStateStatus,
    VulnerabilityTechnique,
)
from integrates.db_model.vulnerabilities.types import (
    GroupVulnerabilitiesRequest,
    VulnerabilitiesConnection,
    VulnerabilityEdge,
)
from integrates.decorators import (
    require_is_not_under_review,
)

from .schema import (
    GROUP,
)


def get_edge(edge: VulnerabilityEdge, info: GraphQLResolveInfo) -> VulnerabilityEdge:
    if (
        info.context.operation.name == "ForcesGetGroupLocations"
        and edge.node.technique == VulnerabilityTechnique.PTAAS
    ):
        return edge._replace(node=edge.node._replace(technique=VulnerabilityTechnique.MPT))

    return edge


@GROUP.field("forcesVulnerabilities")
@require_is_not_under_review
async def resolve(
    parent: Group,
    info: GraphQLResolveInfo,
    after: str | None = None,
    first: int | None = None,
    state: str | None = None,
    **_kwargs: None,
) -> VulnerabilitiesConnection:
    loaders: Dataloaders = info.context.loaders
    state_status = (
        None if state is None else VulnerabilityStateStatus[get_inverted_state_converted(state)]
    )
    connection = await loaders.group_vulnerabilities.load(
        GroupVulnerabilitiesRequest(
            is_accepted=None if state_status is None else False,
            group_name=parent.name,
            state_status=state_status,
            after=after,
            first=first,
            paginate=True,
        ),
    )
    if state_status in RELEASED_FILTER_STATUSES:
        # Wait to process the indicators
        return VulnerabilitiesConnection(
            edges=tuple(
                get_edge(edge, info)
                for edge in connection.edges
                if edge.node.unreliable_indicators.unreliable_report_date
            ),
            page_info=connection.page_info,
        )
    return connection
