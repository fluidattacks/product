from datetime import UTC, datetime

from integrates.api.resolvers.group.toe_inputs import resolve
from integrates.dataloaders import get_new_context
from integrates.db_model.toe_inputs.types import ToeInput, ToeInputEdge, ToeInputState
from integrates.db_model.vulnerabilities.enums import VulnerabilityStateStatus, VulnerabilityType
from integrates.decorators import (
    enforce_group_level_auth_async,
    require_attribute_internal,
    validate_connection,
)
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb, IntegratesOpensearch
from integrates.testing.fakers import (
    GitRootFaker,
    GraphQLResolveInfoFaker,
    GroupAccessFaker,
    GroupAccessStateFaker,
    GroupFaker,
    IPRootFaker,
    OrganizationAccessFaker,
    OrganizationFaker,
    StakeholderFaker,
    ToeInputFaker,
    ToeInputStateFaker,
    ToePortFaker,
    VulnerabilityFaker,
    VulnerabilityStateFaker,
)
from integrates.testing.mocks import mocks
from integrates.testing.utils import parametrize, raises

ORG_ID = "ORG#40f6da5f-4f66-4bf0-825b-a2d9748ad6db"
ROOT_ID = "38eb8f25-7945-4173-ab6e-0af4ad8b7ef3"
GROUP_NAME = "grouptest"
ADMIN_EMAIL = "admin@fluidattacks.com"
REATTACKER_EMAIL = "reattacker@fluidattacks.com"
HACKER_EMAIL = "hacker@fluidattacks.com"
RESOURCER_EMAIL = "resourcer@fluidattacks.com"
CUSTOMER_MANAGER_EMAIL = "customer_manager@fluidattacks.com"
REVIEWER_EMAIL = "reviewer@fluidattacks.com"

ORG_NAME = "orgtest"


@parametrize(
    args=["email"],
    cases=[
        [ADMIN_EMAIL],
        [CUSTOMER_MANAGER_EMAIL],
        [REATTACKER_EMAIL],
        [HACKER_EMAIL],
        [RESOURCER_EMAIL],
        [REVIEWER_EMAIL],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            groups=[GroupFaker(name=GROUP_NAME, organization_id=ORG_ID)],
            group_access=[
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email=ADMIN_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="admin"),
                ),
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email=CUSTOMER_MANAGER_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="customer_manager"),
                ),
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email=HACKER_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="hacker"),
                ),
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email=REATTACKER_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="reattacker"),
                ),
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email=RESOURCER_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="resourcer"),
                ),
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email=REVIEWER_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="reviewer"),
                ),
            ],
            stakeholders=[
                StakeholderFaker(email=ADMIN_EMAIL),
                StakeholderFaker(email=CUSTOMER_MANAGER_EMAIL),
                StakeholderFaker(email=HACKER_EMAIL),
                StakeholderFaker(email=REATTACKER_EMAIL),
                StakeholderFaker(email=RESOURCER_EMAIL),
                StakeholderFaker(email=REVIEWER_EMAIL),
            ],
            organization_access=[OrganizationAccessFaker(organization_id=ORG_ID)],
            organizations=[OrganizationFaker(id=ORG_ID, name=ORG_NAME)],
            roots=[GitRootFaker(id=ROOT_ID, group_name=GROUP_NAME, organization_name=ORG_NAME)],
            vulnerabilities=[
                VulnerabilityFaker(
                    group_name=GROUP_NAME,
                    root_id=ROOT_ID,
                    finding_id="find1",
                    id="vuln1",
                    created_by="hacker@fluidattacks.com",
                    type=VulnerabilityType.INPUTS,
                    state=VulnerabilityStateFaker(
                        status=VulnerabilityStateStatus.VULNERABLE, where="https://test.com/"
                    ),
                ),
            ],
            toe_inputs=[
                ToeInputFaker(
                    group_name=GROUP_NAME,
                    root_id=ROOT_ID,
                    component="https://test.com/",
                    entry_point="user",
                    state=ToeInputStateFaker(has_vulnerabilities=True),
                ),
            ],
        ),
        opensearch=IntegratesOpensearch(autoload=True),
    )
)
async def test_get_toe_inputs(email: str) -> None:
    # Act
    loaders = get_new_context()
    parent = await loaders.group.load(GROUP_NAME)
    cursor: str = (
        "GROUP#grouptest#INPUTS#ROOT#38eb8f25-7945-4173-ab6e-0af4ad8"
        "b7ef3#COMPONENT#https://test.com/#ENTRYPOINT#user"
    )
    info = GraphQLResolveInfoFaker(
        user_email=email,
        decorators=[
            [enforce_group_level_auth_async],
            [validate_connection],
            [require_attribute_internal, "is_under_review", GROUP_NAME],
        ],
    )

    result = await resolve(parent=parent, info=info, group_name=GROUP_NAME)

    # Assert
    assert result
    assert len(result.edges) == 1
    assert result.edges[0] == ToeInputEdge(
        node=ToeInput(
            component="https://test.com/",
            entry_point="user",
            environment_id="74aba0e323ca69ba7659a7b7ed687b88675ef179",
            root_id=ROOT_ID,
            group_name=GROUP_NAME,
            state=ToeInputState(
                attacked_at=None,
                attacked_by=None,
                be_present=True,
                be_present_until=None,
                first_attack_at=None,
                has_vulnerabilities=True,
                modified_by=HACKER_EMAIL,
                modified_date=datetime(2019, 4, 10, 12, 59, 52, tzinfo=UTC),
                seen_at=datetime(2019, 4, 10, 12, 59, 52, tzinfo=UTC),
                seen_first_time_by=HACKER_EMAIL,
            ),
        ),
        cursor=cursor,
    )


NOT_GROUP_USER = "user@fluidattacks.com"


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id=ORG_ID, name=ORG_NAME)],
            stakeholders=[StakeholderFaker(email=NOT_GROUP_USER, enrolled=True)],
            organization_access=[
                OrganizationAccessFaker(organization_id=ORG_ID, email=NOT_GROUP_USER)
            ],
            groups=[GroupFaker(name=GROUP_NAME, organization_id=ORG_ID)],
            roots=[IPRootFaker(id=ROOT_ID, group_name=GROUP_NAME, organization_name=ORG_NAME)],
            toe_ports=[ToePortFaker(group_name=GROUP_NAME, root_id=ROOT_ID)],
        )
    )
)
async def test_get_toe_inputs_access_denied() -> None:
    loaders = get_new_context()
    parent = await loaders.group.load(GROUP_NAME)
    info = GraphQLResolveInfoFaker(
        user_email=NOT_GROUP_USER,
        decorators=[
            [enforce_group_level_auth_async],
            [validate_connection],
            [require_attribute_internal, "is_under_review", GROUP_NAME],
        ],
    )
    with raises(Exception, match="Access denied"):
        await resolve(parent=parent, info=info, group_name=GROUP_NAME)
