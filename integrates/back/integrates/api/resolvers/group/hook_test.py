from integrates.api.resolvers.group.hook import resolve
from integrates.decorators import require_attribute_internal
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    GraphQLResolveInfoFaker,
    GroupFaker,
    GroupHookFaker,
    OrganizationAccessFaker,
    OrganizationFaker,
    StakeholderFaker,
    random_uuid,
)
from integrates.testing.mocks import mocks

ORG_ID = "ORG#40f6da5f-4f66-4bf0-825b-a2d9748ad6db"
GROUP_NAME = "grouptest"
USER_EMAIL = "jdoe@orgtest.com"
ORG_NAME = "orgtest"


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            group_hook=[
                GroupHookFaker(
                    group_name=GROUP_NAME,
                    id=random_uuid(),
                    entry_point="https://hooktest1.com",
                    name="hook1",
                ),
                GroupHookFaker(
                    group_name=GROUP_NAME,
                    id=random_uuid(),
                    entry_point="https://hooktest2.com",
                    name="hook2",
                ),
            ],
            groups=[GroupFaker(name=GROUP_NAME, organization_id=ORG_ID)],
            stakeholders=[StakeholderFaker(email=USER_EMAIL, enrolled=True)],
            organization_access=[OrganizationAccessFaker(organization_id=ORG_ID, email=USER_EMAIL)],
            organizations=[OrganizationFaker(id=ORG_ID, name=ORG_NAME)],
        )
    )
)
async def test_group_hook() -> None:
    # Act
    parent = GroupFaker(name=GROUP_NAME, organization_id=ORG_ID)
    info = GraphQLResolveInfoFaker(
        user_email=USER_EMAIL,
        decorators=[[require_attribute_internal, "is_under_review", GROUP_NAME]],
    )

    result = await resolve(parent=parent, info=info, group_name=GROUP_NAME)

    # Assert
    assert result
    assert len(result) == 2
    assert sorted([hook.entry_point for hook in result]) == [
        "https://hooktest1.com",
        "https://hooktest2.com",
    ]
