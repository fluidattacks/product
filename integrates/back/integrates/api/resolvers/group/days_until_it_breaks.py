from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.db_model.groups.types import (
    Group,
)
from integrates.vulnerabilities.domain.validations import (
    get_policy_days_until_it_breaks,
)

from .schema import (
    GROUP,
)


@GROUP.field("daysUntilItBreaks")
async def resolve(
    parent: Group,
    info: GraphQLResolveInfo,
    **_kwargs: None,
) -> int | None:
    return await get_policy_days_until_it_breaks(
        loaders=info.context.loaders,
        group_name=parent.name,
    )
