from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.groups.types import (
    Group,
)
from integrates.db_model.roots.types import (
    GroupEnvironmentUrlsRequest,
    RootEnvironmentUrl,
)
from integrates.decorators import (
    require_is_not_under_review,
)

from .schema import (
    GROUP,
)


@GROUP.field("gitEnvironmentUrls")
@require_is_not_under_review
async def resolve(
    parent: Group, info: GraphQLResolveInfo, **_kwargs: None
) -> list[RootEnvironmentUrl]:
    loaders: Dataloaders = info.context.loaders
    return await loaders.group_environment_urls.load(
        GroupEnvironmentUrlsRequest(group_name=parent.name),
    )
