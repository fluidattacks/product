from azure.devops.v7_1.core import (
    IdentityRef,
)
from graphql import (
    GraphQLResolveInfo,
)

from integrates.db_model.groups.types import (
    GroupAzureIssues,
)
from integrates.groups import (
    azure_issues_integration,
)

from .schema import (
    AZURE_ISSUES_INTEGRATION,
)


@AZURE_ISSUES_INTEGRATION.field("projectMembers")
async def resolve(
    parent: GroupAzureIssues,
    _info: GraphQLResolveInfo,
    *,
    azure_organization: str,
    azure_project: str,
) -> list[IdentityRef]:
    return await azure_issues_integration.get_project_members(
        azure_organization=azure_organization,
        azure_project=azure_project,
        settings=parent,
    )
