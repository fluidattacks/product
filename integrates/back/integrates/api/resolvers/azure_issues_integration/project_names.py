from graphql import (
    GraphQLResolveInfo,
)

from integrates.db_model.groups.types import (
    GroupAzureIssues,
)
from integrates.groups import (
    azure_issues_integration,
)

from .schema import (
    AZURE_ISSUES_INTEGRATION,
)


@AZURE_ISSUES_INTEGRATION.field("projectNames")
async def resolve(
    parent: GroupAzureIssues,
    _info: GraphQLResolveInfo,
    *,
    azure_organization: str,
) -> list[str]:
    return await azure_issues_integration.get_project_names(
        azure_organization=azure_organization,
        settings=parent,
    )
