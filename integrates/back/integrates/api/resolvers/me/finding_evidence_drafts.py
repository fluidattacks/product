from typing import (
    Any,
    cast,
)

from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates import (
    authz,
)
from integrates.custom_utils.access import get_stakeholder_groups_names
from integrates.custom_utils.validations_deco import (
    validate_all_fields_length_deco,
)
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.findings.enums import (
    FindingStateStatus,
)
from integrates.db_model.findings.types import (
    FindingEdge,
    FindingEvidences,
    FindingsConnection,
)
from integrates.db_model.findings.utils import (
    format_finding,
)
from integrates.db_model.items import (
    FindingItem,
)
from integrates.dynamodb.types import (
    PageInfo,
)
from integrates.search.operations import (
    SearchParams,
    search,
)

from .schema import (
    ME,
)


def _get_evidences_name() -> list[str]:
    return [name for name in FindingEvidences._fields if name not in {"records"}]


@ME.field("findingEvidenceDrafts")
@validate_all_fields_length_deco(max_length=300)
async def resolve(
    parent: dict[str, str],
    info: GraphQLResolveInfo,
    **kwargs: Any,
) -> FindingsConnection:
    email = parent["user_email"]
    loaders: Dataloaders = info.context.loaders

    must_not_filters = [
        {"state.status": FindingStateStatus.DELETED},
        {"state.status": FindingStateStatus.MASKED},
    ]
    should_filters = [
        {f"evidences.{evidence}.is_draft": True} for evidence in _get_evidences_name()
    ]
    group_names = await get_stakeholder_groups_names(loaders, email, True)
    enforcer = await authz.get_group_level_enforcer(loaders, email)
    groups_to_search = [
        group_name
        for group_name in group_names
        if enforcer(group_name, "integrates_api_resolvers_group_drafts_resolve")
    ]

    if not groups_to_search:
        return FindingsConnection(
            edges=tuple(),
            page_info=PageInfo(has_next_page=False, end_cursor=""),
            total=0,
        )

    results = await search(
        SearchParams(
            after=kwargs.get("after"),
            exact_filters={"group_name": groups_to_search},
            must_not_filters=must_not_filters,
            should_filters=should_filters,
            index_value="findings_index",
            limit=kwargs.get("first") or 100,
        ),
    )

    return FindingsConnection(
        edges=tuple(
            FindingEdge(
                cursor=results.page_info.end_cursor,
                node=format_finding(cast(FindingItem, item)),
            )
            for item in results.items
        ),
        page_info=results.page_info,
        total=results.total,
    )
