import logging
import logging.config

from aioextensions import collect
from graphql.type.definition import GraphQLResolveInfo

from integrates.custom_utils import organizations as orgs_utils
from integrates.dataloaders import Dataloaders
from integrates.db_model.organizations.types import Organization
from integrates.organization_access.domain import get_stakeholder_organizations_ids
from integrates.settings import LOGGING

from .schema import ME

logging.config.dictConfig(LOGGING)

LOGGER = logging.getLogger(__name__)


@ME.field("organizations")
async def resolve(
    parent: dict[str, str],
    info: GraphQLResolveInfo,
    **_kwargs: None,
) -> list[Organization]:
    loaders: Dataloaders = info.context.loaders
    user_email = str(parent["user_email"])
    organization_ids = await get_stakeholder_organizations_ids(loaders, user_email, True)
    organizations = await collect(
        list(_get_organization(loaders, organization_id) for organization_id in organization_ids),
    )
    org_actives = orgs_utils.filter_active_organizations(
        [organization for organization in organizations if organization],
    )
    return org_actives


async def _get_organization(loaders: Dataloaders, organization_key: str) -> Organization | None:
    organization = await loaders.organization.load(organization_key)
    if not organization:
        LOGGER.error("Organization %s not found", organization_key)
        return None

    return organization
