from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.notifications.get import NotificationRequest
from integrates.db_model.notifications.types import (
    Notification,
)

from .schema import (
    ME,
)


@ME.field("notification")
async def resolve(
    parent: dict[str, str],
    info: GraphQLResolveInfo,
    *,
    notification_id: str,
) -> Notification | None:
    loaders: Dataloaders = info.context.loaders
    user_email = parent["user_email"]
    return await loaders.notification.load(
        NotificationRequest(notification_id=notification_id, user_email=user_email)
    )
