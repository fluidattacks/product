from typing import (
    Any,
    cast,
)

from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates import (
    authz,
)
from integrates.api.resolvers.me.finding_evidence_drafts import (
    _get_evidences_name,
)
from integrates.api.resolvers.me.vulnerability_drafts import (
    RangeOperator,
)
from integrates.class_types.types import (
    Item,
)
from integrates.custom_utils.groups import (
    get_group_names_except_test_groups,
)
from integrates.custom_utils.validations_deco import (
    validate_all_fields_length_deco,
)
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.findings.enums import (
    FindingStateStatus,
)
from integrates.db_model.findings.types import (
    FindingEdge,
    FindingsConnection,
)
from integrates.db_model.findings.utils import (
    format_finding,
)
from integrates.db_model.items import (
    FindingItem,
)
from integrates.dynamodb.types import (
    PageInfo,
)
from integrates.search.operations import (
    SearchParams,
    search,
)

from .schema import (
    ME,
)


@ME.field("findingEmptyEvidence")
@validate_all_fields_length_deco(max_length=300)
async def resolve(parent: Item, info: GraphQLResolveInfo, **kwargs: Any) -> FindingsConnection:
    email = str(parent["user_email"])
    loaders: Dataloaders = info.context.loaders

    must_not_filters = [
        {"state.status": FindingStateStatus.DELETED},
        {"state.status": FindingStateStatus.MASKED},
    ]
    group_names = await get_group_names_except_test_groups(
        loaders=loaders,
        stakeholder_email=email,
    )
    enforcer = await authz.get_group_level_enforcer(loaders, email)
    groups_to_search = [
        group_name
        for group_name in group_names
        if enforcer(
            group_name,
            "integrates_api_resolvers_group_drafts_resolve",
        )
    ]

    if not groups_to_search:
        return FindingsConnection(
            edges=tuple(),
            page_info=PageInfo(has_next_page=False, end_cursor=""),
            total=0,
        )
    indicators = "unreliable_indicators"
    results = await search(
        SearchParams(
            after=kwargs.get("after"),
            exact_filters={"group_name": groups_to_search},
            must_not_filters=must_not_filters,
            bool_filters=[
                {
                    "and_not_exists_filters": [
                        f"evidences.{evidence}" for evidence in _get_evidences_name()
                    ],
                    "and_range_filters": [
                        {
                            f"{indicators}.vulnerabilities_summary.open": {
                                RangeOperator.GTE.value: 1,
                            },
                        },
                        {
                            f"{indicators}.vulnerabilities_summary.closed": {
                                RangeOperator.GTE.value: 1,
                            },
                        },
                    ],
                },
            ],
            index_value="findings_index",
            limit=kwargs.get("first") or 100,
        ),
    )

    return FindingsConnection(
        edges=tuple(
            FindingEdge(
                cursor=results.page_info.end_cursor,
                node=format_finding(cast(FindingItem, item)),
            )
            for item in results.items
        ),
        page_info=results.page_info,
        total=results.total,
    )
