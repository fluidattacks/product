from typing import (
    Any,
    cast,
)

from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.custom_utils.groups import (
    get_group_names_except_test_groups,
)
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.findings.types import (
    FindingEdge,
    FindingsConnection,
)
from integrates.db_model.findings.utils import (
    format_finding,
)
from integrates.db_model.items import (
    FindingItem,
)
from integrates.decorators import (
    require_login,
)
from integrates.search.operations import (
    SearchParams,
    search,
)

from .schema import (
    ME,
)


@ME.field("findingReattacksConnection")
@require_login
async def resolve(
    parent: dict[str, str],
    info: GraphQLResolveInfo,
    **kwargs: Any,
) -> FindingsConnection:
    user_email = parent["user_email"]
    not_zero_requested = {"unreliable_indicators.verification_summary.requested": 0}
    results = await search(
        SearchParams(
            after=kwargs.get("after"),
            must_filters=[
                {"unreliable_indicators.unreliable_status": "VULNERABLE"},
            ],
            must_not_filters=[not_zero_requested],
            index_value="findings_index",
            limit=kwargs.get("first") or 100,
        ),
    )
    loaders: Dataloaders = info.context.loaders
    stakeholder_groups = await get_group_names_except_test_groups(
        loaders=loaders,
        stakeholder_email=user_email,
    )

    return FindingsConnection(
        edges=tuple(
            FindingEdge(
                cursor=results.page_info.end_cursor,
                node=format_finding(cast(FindingItem, result)),
            )
            for result in results.items
            if result["group_name"] in stakeholder_groups
        ),
        page_info=results.page_info,
        total=results.total,
    )
