from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.custom_utils.stakeholders import (
    get_stakeholder,
)
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.stakeholders.types import (
    TrustedDevice,
)

from .schema import (
    ME,
)


@ME.field("trustedDevices")
async def resolve(parent: dict[str, str], info: GraphQLResolveInfo) -> list[TrustedDevice]:
    loaders: Dataloaders = info.context.loaders
    email = str(parent["user_email"])
    stakeholder = await get_stakeholder(loaders, email)

    return stakeholder.state.trusted_devices
