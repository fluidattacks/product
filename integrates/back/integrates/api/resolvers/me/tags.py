from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.portfolios.types import (
    Portfolio,
)
from integrates.decorators import (
    require_organization_access,
)
from integrates.groups.domain import (
    get_groups_by_stakeholder,
)
from integrates.organizations.utils import (
    get_organization,
)

from .schema import (
    ME,
)


@ME.field("tags")
@require_organization_access
async def resolve(
    parent: dict[str, str],
    info: GraphQLResolveInfo,
    **kwargs: str,
) -> list[Portfolio]:
    loaders: Dataloaders = info.context.loaders
    user_email = parent["user_email"]

    identifier = (
        kwargs["organization_id"] if "organization_id" in kwargs else kwargs["organization_name"]
    )
    organization = await get_organization(loaders, identifier)
    org_tags = await loaders.organization_portfolios.load(organization.name)
    user_groups = await get_groups_by_stakeholder(loaders, user_email)

    return [tag for tag in org_tags if any(group in tag.groups for group in user_groups)]
