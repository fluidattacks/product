from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.class_types.types import (
    Item,
)
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.group_access.domain import (
    get_group_access,
)
from integrates.groups.domain import (
    get_groups_by_stakeholder,
)
from integrates.trials import (
    getters as trials_getters,
)

from .schema import (
    ME,
)


@ME.field("trial")
async def resolve(
    parent: dict[str, str],
    info: GraphQLResolveInfo,
    **_kwargs: None,
) -> Item | None:
    loaders: Dataloaders = info.context.loaders
    user_email = parent["user_email"]
    trial = await loaders.trial.load(user_email)

    if trial:
        completed = trial.completed
        role = None

        if not completed:
            user_groups = await get_groups_by_stakeholder(loaders, user_email)
            if len(user_groups) > 0:
                group = await get_group_access(loaders, user_groups[0], user_email)
                role = group.state.role

        return {
            "completed": trial.completed,
            "extension_date": trial.extension_date or "",
            "extension_days": trial.extension_days,
            "start_date": trial.start_date or "",
            "state": trials_getters.get_status(trial),
            "group_role": role,
        }
    return None
