from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.notifications.types import (
    Notification,
)

from .schema import (
    ME,
)


@ME.field("notifications")
async def resolve(
    parent: dict[str, str],
    info: GraphQLResolveInfo,
) -> list[Notification]:
    loaders: Dataloaders = info.context.loaders
    user_email = parent["user_email"]
    return await loaders.user_notifications.load(user_email)
