from datetime import datetime, timedelta

from integrates.api.resolvers.me.aws_subscription import resolve
from integrates.dataloaders import get_new_context
from integrates.db_model.groups.enums import GroupManaged, GroupTier
from integrates.db_model.marketplace.enums import (
    AWSMarketplacePricingDimension,
    AWSMarketplaceSubscriptionStatus,
)
from integrates.db_model.marketplace.types import (
    AWSMarketplaceSubscription,
    AWSMarketplaceSubscriptionEntitlement,
)
from integrates.groups.domain import get_groups_owned_by_user
from integrates.marketplace.utils import get_groups_entitlement
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    AWSMarketplaceSubscriptionFaker,
    AWSMarketplaceSubscriptionStateFaker,
    GraphQLResolveInfoFaker,
    GroupAccessFaker,
    GroupAccessStateFaker,
    GroupFaker,
    GroupStateFaker,
    OrganizationAccessFaker,
    OrganizationFaker,
    StakeholderFaker,
)
from integrates.testing.mocks import mocks
from integrates.testing.utils import parametrize

ORG_ID = "ORG#40f6da5f-4f66-4bf0-825b-a2d9748ad6db"
GROUP_NAME = "aws-group-1"
GROUP_NAME_OTHER = "aws-group-2"
GROUP_MANAGER = "group_manager@company.com"
USER_EMAIL = "user@company.com"
ORG_NAME = "orgtest"


@parametrize(
    args=[
        "email",
        "contracted_groups",
        "used_groups",
        "status",
    ],
    cases=[
        [
            "group_manager@company.com",
            3,
            2,
            AWSMarketplaceSubscriptionStatus.ACTIVE,
        ],
        [
            "user@company.com",
            5,
            0,
            AWSMarketplaceSubscriptionStatus.EXPIRED,
        ],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            groups=[
                GroupFaker(
                    name=GROUP_NAME,
                    created_by=GROUP_MANAGER,
                    organization_id=ORG_ID,
                    state=GroupStateFaker(
                        has_essential=True,
                        has_advanced=False,
                        managed=GroupManaged.NOT_MANAGED,
                        tier=GroupTier.ESSENTIAL,
                    ),
                ),
                GroupFaker(
                    name=GROUP_NAME_OTHER,
                    created_by=GROUP_MANAGER,
                    organization_id=ORG_ID,
                    state=GroupStateFaker(
                        has_essential=True,
                        has_advanced=False,
                        managed=GroupManaged.NOT_MANAGED,
                        tier=GroupTier.ESSENTIAL,
                    ),
                ),
            ],
            group_access=[
                GroupAccessFaker(
                    email=GROUP_MANAGER,
                    group_name=GROUP_NAME,
                    state=GroupAccessStateFaker(role="group_manager"),
                ),
                GroupAccessFaker(
                    email=USER_EMAIL,
                    group_name=GROUP_NAME_OTHER,
                    state=GroupAccessStateFaker(role="user"),
                ),
                GroupAccessFaker(
                    email=GROUP_MANAGER,
                    group_name=GROUP_NAME_OTHER,
                    state=GroupAccessStateFaker(role="group_manager"),
                ),
            ],
            stakeholders=[
                StakeholderFaker(email=USER_EMAIL, enrolled=True, aws_customer_id="wEMmJjXM3hU"),
                StakeholderFaker(email=GROUP_MANAGER, enrolled=True, aws_customer_id="xYwVpnKesFR"),
            ],
            organization_access=[OrganizationAccessFaker(organization_id=ORG_ID, email=USER_EMAIL)],
            organizations=[OrganizationFaker(id=ORG_ID, name=ORG_NAME)],
            aws_subscriptions=[
                AWSMarketplaceSubscriptionFaker(
                    aws_customer_id="xYwVpnKesFR",
                    created_at=datetime.fromisoformat("2024-07-19T11:00:00+00:00"),
                    state=AWSMarketplaceSubscriptionStateFaker(
                        entitlements=[
                            AWSMarketplaceSubscriptionEntitlement(
                                dimension=AWSMarketplacePricingDimension.GROUPS,
                                expiration_date=(datetime.now() + timedelta(days=365)),
                                value=3,
                            )
                        ],
                        has_free_trial=False,
                        modified_date=datetime.fromisoformat("2024-07-19T11:00:00+00:00"),
                        status=AWSMarketplaceSubscriptionStatus.ACTIVE,
                    ),
                    user="group_manager@company.com",
                ),
                AWSMarketplaceSubscriptionFaker(
                    aws_customer_id="wEMmJjXM3hU",
                    created_at=datetime.fromisoformat("2024-07-19T11:00:00+00:00"),
                    state=AWSMarketplaceSubscriptionStateFaker(
                        entitlements=[
                            AWSMarketplaceSubscriptionEntitlement(
                                dimension=AWSMarketplacePricingDimension.GROUPS,
                                expiration_date=(datetime.now() + timedelta(days=365)),
                                value=5,
                            )
                        ],
                        has_free_trial=False,
                        modified_date=datetime.fromisoformat("2024-07-19T11:00:00+00:00"),
                        status=AWSMarketplaceSubscriptionStatus.EXPIRED,
                    ),
                    user="user@company.com",
                ),
            ],
        )
    )
)
async def test_me_aws_subscriptions(
    email: str,
    contracted_groups: int | None,
    used_groups: int | None,
    status: AWSMarketplaceSubscriptionStatus | None,
) -> None:
    # Act
    loaders = get_new_context()
    parent = {"user_email": email}
    info = GraphQLResolveInfoFaker(user_email=email)

    result: AWSMarketplaceSubscription | None = await resolve(parent=parent, info=info)

    # Assert
    assert result
    assert result.state.status == status
    assert len(await get_groups_owned_by_user(loaders=loaders, email=email)) == used_groups
    assert get_groups_entitlement(result).value == contracted_groups
