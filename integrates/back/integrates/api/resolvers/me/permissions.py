import logging
import logging.config

from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates import (
    authz,
)
from integrates.dataloaders import (
    Dataloaders,
)

from .schema import (
    ME,
)

# Constants
LOGGER = logging.getLogger(__name__)


@ME.field("permissions")
async def resolve(parent: dict[str, str], info: GraphQLResolveInfo, **_kwargs: str) -> set[str]:
    loaders: Dataloaders = info.context.loaders
    user_email = str(parent["user_email"])
    permissions: set[str] = await authz.get_user_level_actions(loaders, user_email)
    if user_email.endswith(authz.FLUID_IDENTIFIER):
        permissions.add("can_assign_vulnerabilities_to_fluidattacks_staff")
    if not permissions:
        LOGGER.error(
            "Empty permissions on _get_user_permissions",
            extra={
                "extra": {
                    "parent": locals()["parent"],
                    "permissions": locals()["permissions"],
                },
            },
        )

    return permissions
