from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.credentials.types import (
    Credentials,
)

from .schema import (
    ME,
)


@ME.field("credentials")
async def resolve(parent: dict[str, str], info: GraphQLResolveInfo) -> list[Credentials]:
    loaders: Dataloaders = info.context.loaders
    email = str(parent["user_email"])

    return await loaders.user_credentials.load(email)
