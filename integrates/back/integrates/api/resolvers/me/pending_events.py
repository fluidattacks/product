from graphql.type.definition import GraphQLResolveInfo

from integrates.custom_utils.groups import get_group_names_except_test_groups
from integrates.dataloaders import Dataloaders
from integrates.db_model.events.types import EventEdge, EventsConnection
from integrates.db_model.events.utils import format_event
from integrates.db_model.items import EventItem
from integrates.decorators import require_login
from integrates.dynamodb.types import PageInfo
from integrates.search.operations import SearchClient, SearchParams

from .schema import ME


@ME.field("pendingEvents")
@require_login
async def resolve(
    parent: dict[str, str],
    info: GraphQLResolveInfo,
    **kwargs: None,
) -> EventsConnection:
    user_email = str(parent["user_email"])
    loaders: Dataloaders = info.context.loaders
    stakeholder_groups = await get_group_names_except_test_groups(
        loaders=loaders,
        stakeholder_email=user_email,
    )

    if not stakeholder_groups:
        return EventsConnection(
            edges=tuple(),
            page_info=PageInfo(has_next_page=False, end_cursor=""),
            total=0,
        )

    results = await SearchClient[EventItem].search(
        SearchParams(
            after=kwargs.get("after"),
            exact_filters={"group_name": stakeholder_groups},
            must_not_filters=[{"state.status": "SOLVED"}],
            index_value="events_index",
            limit=kwargs.get("first") or 1000,
        ),
    )

    return EventsConnection(
        edges=tuple(
            EventEdge(
                cursor=results.page_info.end_cursor,
                node=format_event(item),
            )
            for item in results.items
        ),
        page_info=results.page_info,
        total=results.total,
    )
