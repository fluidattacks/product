from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.custom_utils.stakeholders import (
    get_stakeholder,
)
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.stakeholders.types import (
    Stakeholder,
    StakeholderTours,
)

from .schema import (
    ME,
)


@ME.field("tours")
async def resolve(
    parent: dict[str, str],
    info: GraphQLResolveInfo,
    **_kwargs: None,
) -> StakeholderTours:
    user_email = parent["user_email"]
    loaders: Dataloaders = info.context.loaders
    stakeholder: Stakeholder = await get_stakeholder(loaders, user_email)
    return stakeholder.tours
