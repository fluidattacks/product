from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.custom_utils.stakeholders import (
    get_stakeholder,
)
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.stakeholders.types import (
    Stakeholder,
)

from .schema import (
    ME,
)


@ME.field("enrolled")
async def resolve(parent: dict[str, str], info: GraphQLResolveInfo, **_kwargs: None) -> bool:
    user_email = str(parent["user_email"])
    loaders: Dataloaders = info.context.loaders
    stakeholder: Stakeholder = await get_stakeholder(loaders, user_email)
    return stakeholder.enrolled
