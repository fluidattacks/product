from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.roots.enums import (
    RootDockerImageStateStatus,
)
from integrates.db_model.roots.types import (
    GitRoot,
    RootDockerImage,
    RootDockerImagesRequest,
)

from .schema import (
    GIT_ROOT,
)


@GIT_ROOT.field("dockerImages")
async def resolve(parent: GitRoot, info: GraphQLResolveInfo) -> list[RootDockerImage]:
    loaders: Dataloaders = info.context.loaders

    images = await loaders.root_docker_images.load(
        RootDockerImagesRequest(root_id=parent.id, group_name=parent.group_name),
    )
    return [
        image for image in images if image.state.status is not RootDockerImageStateStatus.DELETED
    ]
