from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.db_model.roots.types import (
    GitRoot,
)

from .schema import (
    GIT_ROOT,
)


@GIT_ROOT.field("gitignore")
def resolve(parent: GitRoot, _info: GraphQLResolveInfo) -> list[str]:
    return parent.state.gitignore
