from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.db_model.roots.types import (
    GitRoot,
)

from .schema import (
    GIT_ROOT,
)


@GIT_ROOT.field("lastEditedBy")
def resolve(parent: GitRoot, _info: GraphQLResolveInfo) -> str:
    return parent.state.modified_by
