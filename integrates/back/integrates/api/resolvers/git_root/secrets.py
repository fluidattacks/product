from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.roots.types import (
    GitRoot,
    Secret,
    URLRoot,
)
from integrates.decorators import (
    enforce_group_level_auth_async,
)

from .schema import (
    GIT_ROOT,
)


@GIT_ROOT.field("secrets")
@enforce_group_level_auth_async
async def resolve(parent: GitRoot | URLRoot, info: GraphQLResolveInfo) -> list[Secret]:
    loaders: Dataloaders = info.context.loaders
    return await loaders.root_secrets.load(parent.id)
