from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.roots.types import (
    GitRoot,
    RootEnvironmentUrl,
    RootEnvironmentUrlsRequest,
)

from .schema import (
    GIT_ROOT,
)


@GIT_ROOT.field("gitEnvironmentUrls")
async def resolve(parent: GitRoot, info: GraphQLResolveInfo) -> list[RootEnvironmentUrl]:
    loaders: Dataloaders = info.context.loaders
    urls = await loaders.root_environment_urls.load(
        RootEnvironmentUrlsRequest(root_id=parent.id, group_name=parent.group_name),
    )

    return [url._replace(group_name=parent.group_name) for url in urls]
