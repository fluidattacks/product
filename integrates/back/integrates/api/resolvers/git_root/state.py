from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.db_model.roots.types import (
    Root,
)

from .schema import (
    GIT_ROOT,
)


@GIT_ROOT.field("state")
def resolve(parent: Root, _info: GraphQLResolveInfo) -> str:
    return parent.state.status
