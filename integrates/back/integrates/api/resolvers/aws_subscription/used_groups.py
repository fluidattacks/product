from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.marketplace.types import (
    AWSMarketplaceSubscription,
)
from integrates.groups.domain import (
    get_groups_owned_by_user,
)
from integrates.sessions.domain import (
    get_jwt_content,
)

from .schema import (
    AWS_SUBSCRIPTION,
)


@AWS_SUBSCRIPTION.field("usedGroups")
async def resolve(_parent: AWSMarketplaceSubscription, info: GraphQLResolveInfo) -> int:
    loaders: Dataloaders = info.context.loaders
    user_info = await get_jwt_content(info.context)
    user_email = user_info["user_email"]

    return len(await get_groups_owned_by_user(loaders=loaders, email=user_email))
