from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.db_model.marketplace.enums import (
    AWSMarketplaceSubscriptionStatus,
)
from integrates.db_model.marketplace.types import (
    AWSMarketplaceSubscription,
)
from integrates.marketplace.utils import (
    get_groups_entitlement,
)

from .schema import (
    AWS_SUBSCRIPTION,
)


@AWS_SUBSCRIPTION.field("contractedGroups")
def resolve(parent: AWSMarketplaceSubscription, _info: GraphQLResolveInfo) -> int | None:
    return (
        get_groups_entitlement(parent).value
        if parent.state.status == AWSMarketplaceSubscriptionStatus.ACTIVE
        else None
    )
