from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.db_model.marketplace.types import (
    AWSMarketplaceSubscription,
)

from .schema import (
    AWS_SUBSCRIPTION,
)


@AWS_SUBSCRIPTION.field("status")
def resolve(parent: AWSMarketplaceSubscription, _info: GraphQLResolveInfo) -> str:
    return parent.state.status
