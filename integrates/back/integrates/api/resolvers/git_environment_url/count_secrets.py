from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.roots.types import (
    RootEnvironmentSecretsRequest,
    RootEnvironmentUrl,
)
from integrates.decorators import (
    enforce_group_level_auth_async,
)

from .schema import (
    GIT_ENVIRONMENT_URL,
)


@GIT_ENVIRONMENT_URL.field("countSecrets")
@enforce_group_level_auth_async
async def resolve(parent: RootEnvironmentUrl, info: GraphQLResolveInfo, **__: None) -> int:
    loaders: Dataloaders = info.context.loaders
    secrets = await loaders.environment_secrets.load(
        RootEnvironmentSecretsRequest(url_id=parent.id, group_name=parent.group_name),
    )
    return len(secrets)
