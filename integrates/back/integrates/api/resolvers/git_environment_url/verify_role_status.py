from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.custom_utils.environments_validations import (
    validate_role_status,
)
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.roots.enums import (
    RootEnvironmentUrlType,
)
from integrates.db_model.roots.types import (
    RootEnvironmentUrl,
)
from integrates.findings.domain.events import (
    manage_cspm_environment_events,
)
from integrates.roots.domain import (
    get_environment_url_secrets,
)
from integrates.sessions import domain as sessions_domain

from .schema import (
    GIT_ENVIRONMENT_URL,
)


@GIT_ENVIRONMENT_URL.field("verifyRoleStatus")
async def resolve(parent: RootEnvironmentUrl, info: GraphQLResolveInfo, **__: None) -> bool:
    loaders: Dataloaders = info.context.loaders
    user_info: dict[str, str] = await sessions_domain.get_jwt_content(info.context)
    if parent.state.url_type == RootEnvironmentUrlType.CSPM and parent.state.cloud_name:
        secrets_list = await get_environment_url_secrets(loaders, parent)
        secrets: dict[str, str] = {secret.key: secret.value for secret in secrets_list}
        is_role_valid = await validate_role_status(
            loaders, parent.url, parent.state.cloud_name, parent.group_name, secrets
        )
        await manage_cspm_environment_events(
            loaders=loaders,
            env_url=parent,
            is_role_valid=is_role_valid,
            user_info=user_info,
        )
        return is_role_valid
    return False
