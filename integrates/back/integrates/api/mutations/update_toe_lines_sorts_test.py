from datetime import datetime

from integrates import roots
from integrates.api.mutations.update_toe_lines_sorts import mutate
from integrates.custom_exceptions import (
    InvalidFindingTitle,
    InvalidSortsParameters,
    InvalidSortsRiskLevel,
    ToeLinesNotFound,
)
from integrates.dataloaders import Dataloaders, get_new_context
from integrates.db_model.toe_lines.types import RootToeLinesRequest, SortsSuggestion
from integrates.decorators import enforce_group_level_auth_async, require_attribute, require_login
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    GitRootFaker,
    GitRootStateFaker,
    GraphQLResolveInfoFaker,
    GroupFaker,
    OrganizationFaker,
    StakeholderFaker,
    ToeLinesFaker,
    ToeLinesStateFaker,
)
from integrates.testing.mocks import Mock, mocks
from integrates.testing.utils import raises

from .payloads.types import SimplePayload

EMAIL_TEST = "admin@fluidattacks.com"
GROUP_NAME = "group1"
ROOT_ID = "root1"
TEST_NICKNAME = "back"

TEST_DATE = datetime(2020, 1, 1)


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(id="org1"),
            ],
            stakeholders=[
                StakeholderFaker(email=EMAIL_TEST, role="admin"),
            ],
            groups=[
                GroupFaker(name=GROUP_NAME, organization_id="org1"),
            ],
            roots=[
                GitRootFaker(
                    group_name=GROUP_NAME,
                    id=ROOT_ID,
                    state=GitRootStateFaker(
                        nickname=TEST_NICKNAME,
                    ),
                ),
            ],
            toe_lines=[
                ToeLinesFaker(
                    filename="src/main.py",
                    group_name=GROUP_NAME,
                    root_id=ROOT_ID,
                    state=ToeLinesStateFaker(
                        loc=100,
                    ),
                ),
            ],
        ),
    ),
    others=[Mock(roots.utils, "send_mail_environment", "async", None)],
)
async def test_update_toe_lines_sorts_success() -> None:
    loaders: Dataloaders = get_new_context()
    root_toe_lines = await loaders.root_toe_lines.load_nodes(
        RootToeLinesRequest(group_name=GROUP_NAME, root_id=ROOT_ID),
    )

    assert len(root_toe_lines) == 1
    assert root_toe_lines[0].filename == "src/main.py"
    assert root_toe_lines[0].state.sorts_risk_level == 0

    info = GraphQLResolveInfoFaker(
        user_email=EMAIL_TEST,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_attribute, "has_asm", GROUP_NAME],
        ],
    )

    result = await mutate(
        _=None,
        info=info,
        group_name=GROUP_NAME,
        root_nickname=TEST_NICKNAME,
        filename="src/main.py",
        sorts_risk_level=10,
        sorts_risk_level_date=TEST_DATE,
        sorts_suggestions=[
            {"finding_title": "001. SQL injection - C Sharp SQL API", "probability": 20}
        ],
    )
    assert result == SimplePayload(success=True)

    root_toe_lines = await get_new_context().root_toe_lines.load_nodes(
        RootToeLinesRequest(group_name=GROUP_NAME, root_id=ROOT_ID),
    )

    assert len(root_toe_lines) == 1
    assert root_toe_lines[0].filename == "src/main.py"
    assert root_toe_lines[0].state.sorts_risk_level == 10
    sorts_date = root_toe_lines[0].state.sorts_risk_level_date
    assert sorts_date
    assert (sorts_date.year, sorts_date.month, sorts_date.day) == (
        TEST_DATE.year,
        TEST_DATE.month,
        TEST_DATE.day,
    )
    assert root_toe_lines[0].state.sorts_suggestions == [
        SortsSuggestion(finding_title="001. SQL injection - C Sharp SQL API", probability=20)
    ]


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(id="org1"),
            ],
            stakeholders=[
                StakeholderFaker(email=EMAIL_TEST, role="admin"),
            ],
            groups=[
                GroupFaker(name=GROUP_NAME, organization_id="org1"),
            ],
            roots=[
                GitRootFaker(
                    group_name=GROUP_NAME,
                    id=ROOT_ID,
                    state=GitRootStateFaker(
                        nickname=TEST_NICKNAME,
                    ),
                ),
            ],
            toe_lines=[
                ToeLinesFaker(
                    filename="src/main.py",
                    group_name=GROUP_NAME,
                    root_id=ROOT_ID,
                    state=ToeLinesStateFaker(
                        loc=180,
                    ),
                ),
            ],
        ),
    )
)
async def test_update_toe_lines_sorts_fail() -> None:
    info = GraphQLResolveInfoFaker(
        user_email=EMAIL_TEST,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_attribute, "has_asm", GROUP_NAME],
        ],
    )
    with raises(InvalidSortsRiskLevel):
        await mutate(
            _=None,
            info=info,
            group_name=GROUP_NAME,
            root_id=ROOT_ID,
            filename="src/main.py",
            sorts_risk_level=150,
            sorts_risk_level_date=TEST_DATE,
            sorts_suggestions=[
                {"finding_title": "001. SQL injection - C Sharp SQL API", "probability": 20}
            ],
        )


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(id="org1"),
            ],
            stakeholders=[
                StakeholderFaker(email=EMAIL_TEST, role="admin"),
            ],
            groups=[
                GroupFaker(name=GROUP_NAME, organization_id="org1"),
            ],
            roots=[
                GitRootFaker(
                    group_name=GROUP_NAME,
                    id=ROOT_ID,
                    state=GitRootStateFaker(
                        nickname=TEST_NICKNAME,
                    ),
                ),
            ],
            toe_lines=[
                ToeLinesFaker(
                    filename="src/main.py",
                    group_name=GROUP_NAME,
                    root_id=ROOT_ID,
                    state=ToeLinesStateFaker(
                        loc=180,
                    ),
                ),
            ],
        ),
    )
)
async def test_update_toe_lines_sorts_fail_2() -> None:
    info = GraphQLResolveInfoFaker(
        user_email=EMAIL_TEST,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_attribute, "has_asm", GROUP_NAME],
        ],
    )
    with raises(InvalidFindingTitle):
        await mutate(
            _=None,
            info=info,
            group_name=GROUP_NAME,
            root_nickname=TEST_NICKNAME,
            filename="src/main.py",
            sorts_risk_level=50,
            sorts_risk_level_date=TEST_DATE,
            sorts_suggestions=[{"finding_title": "Invalid title", "probability": -10}],
        )


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(id="org1"),
            ],
            stakeholders=[
                StakeholderFaker(email=EMAIL_TEST, role="admin"),
            ],
            groups=[
                GroupFaker(name=GROUP_NAME, organization_id="org1"),
            ],
            roots=[
                GitRootFaker(
                    group_name=GROUP_NAME,
                    id=ROOT_ID,
                    state=GitRootStateFaker(
                        nickname=TEST_NICKNAME,
                    ),
                ),
            ],
            toe_lines=[
                ToeLinesFaker(
                    filename="src/main.py",
                    group_name=GROUP_NAME,
                    root_id=ROOT_ID,
                    state=ToeLinesStateFaker(
                        loc=180,
                    ),
                ),
            ],
        ),
    )
)
async def test_update_toe_lines_sorts_fail_3() -> None:
    info = GraphQLResolveInfoFaker(
        user_email=EMAIL_TEST,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_attribute, "has_asm", GROUP_NAME],
        ],
    )
    with raises(ToeLinesNotFound):
        await mutate(
            _=None,
            info=info,
            group_name=GROUP_NAME,
            root_nickname=TEST_NICKNAME,
            filename="src/test.py",
            sorts_risk_level=50,
        )


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(id="org1"),
            ],
            stakeholders=[
                StakeholderFaker(email=EMAIL_TEST, role="admin"),
            ],
            groups=[
                GroupFaker(name=GROUP_NAME, organization_id="org1"),
            ],
        ),
    )
)
async def test_update_toe_lines_sorts_fail_4() -> None:
    info = GraphQLResolveInfoFaker(
        user_email=EMAIL_TEST,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_attribute, "has_asm", GROUP_NAME],
        ],
    )
    with raises(InvalidSortsParameters):
        await mutate(
            _=None,
            info=info,
            group_name=GROUP_NAME,
            root_nickname=TEST_NICKNAME,
            filename="src/test.py",
        )
