from typing import (
    TypedDict,
    Unpack,
)

from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.api.types import (
    APP_EXCEPTIONS,
)
from integrates.custom_exceptions import (
    ToeInputNotFound,
)
from integrates.custom_utils import (
    datetime as datetime_utils,
)
from integrates.custom_utils import (
    logs as logs_utils,
)
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.toe_inputs.types import (
    ToeInput,
    ToeInputRequest,
)
from integrates.decorators import (
    concurrent_decorators,
    enforce_group_level_auth_async,
    require_asm,
    require_login,
)
from integrates.sessions import (
    domain as sessions_domain,
)
from integrates.toe.inputs import (
    domain as toe_inputs_domain,
)
from integrates.toe.inputs.types import (
    ToeInputAttributesToUpdate,
)

from .payloads.types import (
    UpdateToeInputPayload,
)
from .schema import (
    MUTATION,
)


class UpdateToeInputArgs(TypedDict):
    be_present: bool
    component: str
    entry_point: str
    group_name: str
    has_recent_attack: bool | None
    root_id: str


@MUTATION.field("updateToeInput")
@concurrent_decorators(
    require_login,
    enforce_group_level_auth_async,
    require_asm,
)
async def mutate(
    _parent: None,
    info: GraphQLResolveInfo,
    **kwargs: Unpack[UpdateToeInputArgs],
) -> UpdateToeInputPayload:
    try:
        user_info = await sessions_domain.get_jwt_content(info.context)
        user_email: str = user_info["user_email"]
        group_name = kwargs["group_name"]
        component = kwargs["component"]
        entry_point = kwargs["entry_point"]
        root_id = kwargs["root_id"]
        be_present = kwargs["be_present"]
        loaders: Dataloaders = info.context.loaders
        current_value: ToeInput | None = await loaders.toe_input.load(
            ToeInputRequest(
                component=component,
                entry_point=entry_point,
                group_name=group_name,
                root_id=root_id,
            ),
        )
        if current_value:
            be_present_to_update = (
                None if be_present is current_value.state.be_present else be_present
            )
            attacked_at_to_update = (
                datetime_utils.get_utc_now() if kwargs.get("has_recent_attack") is True else None
            )
            attacked_by_to_update = None if attacked_at_to_update is None else user_email
            await toe_inputs_domain.update(
                loaders=loaders,
                current_value=current_value,
                attributes=ToeInputAttributesToUpdate(
                    attacked_at=attacked_at_to_update,
                    attacked_by=attacked_by_to_update,
                    be_present=be_present_to_update,
                ),
                modified_by=user_email,
            )
        else:
            raise ToeInputNotFound()

        logs_utils.cloudwatch_log(
            info.context,
            "Updated toe input in the group",
            extra={"group_name": group_name, "log_type": "Security"},
        )
    except APP_EXCEPTIONS:
        logs_utils.cloudwatch_log(
            info.context,
            "Attempted to update toe input in the group",
            extra={"group_name": group_name, "log_type": "Security"},
        )
        raise

    return UpdateToeInputPayload(
        component=component,
        entry_point=entry_point,
        group_name=group_name,
        root_id=root_id,
        success=True,
    )
