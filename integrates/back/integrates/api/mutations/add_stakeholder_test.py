from integrates.api.mutations.add_stakeholder import (
    mutate,
)
from integrates.api.mutations.payloads.types import AddStakeholderPayload
from integrates.custom_exceptions import InvalidRoleProvided
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.decorators import (
    enforce_user_level_auth_async,
    require_login,
)
from integrates.testing.aws import (
    IntegratesAws,
    IntegratesDynamodb,
)
from integrates.testing.fakers import (
    GraphQLResolveInfoFaker,
    GroupAccessFaker,
    GroupAccessStateFaker,
    GroupFaker,
    OrganizationFaker,
    StakeholderFaker,
)
from integrates.testing.mocks import (
    mocks,
)
from integrates.testing.utils import raises

ADMIN_EMAIL = "admin@gmail.com"
GROUP_NAME = "group1"


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(id="org1"),
            ],
            groups=[
                GroupFaker(name=GROUP_NAME, organization_id="org1"),
            ],
            stakeholders=[
                StakeholderFaker(email=ADMIN_EMAIL, role="admin"),
            ],
            group_access=[
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email=ADMIN_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="admin"),
                ),
            ],
        ),
    )
)
async def test_add_stakeholder() -> None:
    loaders: Dataloaders = get_new_context()
    info = GraphQLResolveInfoFaker(
        user_email=ADMIN_EMAIL,
        decorators=[
            [require_login],
            [enforce_user_level_auth_async],
        ],
    )
    email = "new_user_test@gmail.com"
    result = await mutate(
        _parent=None,
        info=info,
        email=email,
        role="admin",
    )
    assert result == AddStakeholderPayload(success=True, email=email)

    loaders = get_new_context()
    stakeholder = await loaders.stakeholder.load(email)
    assert stakeholder
    assert stakeholder.email == email


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(id="org1"),
            ],
            groups=[
                GroupFaker(name=GROUP_NAME, organization_id="org1"),
            ],
            stakeholders=[
                StakeholderFaker(email=ADMIN_EMAIL, role="admin"),
            ],
            group_access=[
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email=ADMIN_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="admin"),
                ),
            ],
        ),
    )
)
async def test_add_stakeholder_fail() -> None:
    info = GraphQLResolveInfoFaker(
        user_email=ADMIN_EMAIL,
        decorators=[
            [require_login],
            [enforce_user_level_auth_async],
        ],
    )

    with raises(Exception, match=str(InvalidRoleProvided(role="customer_manager"))):
        await mutate(
            _parent=None,
            info=info,
            email="customer_manager@gmail.com",
            role="customer_manager",
        )
