from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.custom_exceptions import (
    ErrorAddingGroupTags,
    ErrorUpdatingGroup,
)
from integrates.custom_utils import (
    logs as logs_utils,
)
from integrates.custom_utils.groups import get_group
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.decorators import (
    concurrent_decorators,
    enforce_group_level_auth_async,
    require_asm,
    require_login,
)
from integrates.groups import (
    domain as groups_domain,
)
from integrates.groups import (
    validations as groups_validations,
)
from integrates.sessions import (
    domain as sessions_domain,
)

from .payloads.types import (
    SimpleGroupPayload,
)
from .schema import (
    MUTATION,
)


@MUTATION.field("addGroupTags")
@concurrent_decorators(require_login, enforce_group_level_auth_async, require_asm)
async def mutate(
    _: None,
    info: GraphQLResolveInfo,
    group_name: str,
    tags_data: list[str],
) -> SimpleGroupPayload:
    loaders: Dataloaders = info.context.loaders
    group_name = group_name.lower()
    user_info = await sessions_domain.get_jwt_content(info.context)
    email = user_info["user_email"]

    if not await groups_domain.is_valid(loaders, group_name):
        logs_utils.cloudwatch_log(
            info.context,
            "Attempted to add tags without the allowed validations",
            extra={"group_name": group_name, "log_type": "Security"},
        )
        raise ErrorUpdatingGroup.new()

    if not await groups_validations.validate_group_tags(loaders, group_name, tags_data):
        logs_utils.cloudwatch_log(
            info.context,
            "Attempted to add tags without allowed structure",
            extra={"group_name": group_name, "log_type": "Security"},
        )
        raise ErrorAddingGroupTags.new()

    await groups_domain.add_tags(
        loaders=loaders,
        email=email,
        group=await get_group(loaders, group_name),
        tags_to_add=set(tags_data),
    )
    logs_utils.cloudwatch_log(
        info.context,
        "Added tags to the group",
        extra={"group_name": group_name, "log_type": "Security"},
    )

    loaders.group.clear(group_name)
    group = await get_group(loaders, group_name)

    return SimpleGroupPayload(success=True, group=group)
