from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.custom_utils import (
    logs as logs_utils,
)
from integrates.custom_utils.groups import get_group
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.groups.types import (
    Group,
)
from integrates.db_model.types import (
    PoliciesToUpdate,
)
from integrates.db_model.utils import (
    format_policies_to_update,
)
from integrates.decorators import (
    enforce_organization_level_auth_async,
    require_login,
)
from integrates.groups.domain import (
    update_policies,
)
from integrates.sessions import (
    domain as sessions_domain,
)

from .payloads.types import (
    SimplePayload,
)
from .schema import (
    MUTATION,
)


@MUTATION.field("updateGroupPolicies")
@require_login
async def mutate(
    _parent: None,
    info: GraphQLResolveInfo,
    group_name: str,
    **kwargs: str,
) -> SimplePayload:
    loaders: Dataloaders = info.context.loaders
    group = await get_group(loaders, group_name.lower())
    policies_to_update = format_policies_to_update(kwargs)

    await _update_group(
        info=info,
        group=group,
        organization_id=group.organization_id,
        policies_to_update=policies_to_update,
    )

    return SimplePayload(success=True)


@enforce_organization_level_auth_async
async def _update_group(
    *,
    info: GraphQLResolveInfo,
    organization_id: str,
    group: Group,
    policies_to_update: PoliciesToUpdate,
) -> None:
    loaders: Dataloaders = info.context.loaders
    user_data = await sessions_domain.get_jwt_content(info.context)
    email = user_data["user_email"]
    await update_policies(
        loaders=loaders,
        email=email,
        group_name=group.name,
        organization_id=organization_id,
        policies_to_update=policies_to_update,
    )
    logs_utils.cloudwatch_log(
        info.context,
        "Updated policies in the group",
        extra={"group_name": group.name, "log_type": "Security"},
    )
