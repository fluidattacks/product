from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.api.types import (
    APP_EXCEPTIONS,
)
from integrates.custom_utils import (
    logs as logs_utils,
)
from integrates.custom_utils.findings import get_finding
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.decorators import (
    concurrent_decorators,
    enforce_group_level_auth_async,
    require_asm,
    require_finding_access,
    require_login,
)
from integrates.findings import (
    domain as findings_domain,
)

from .payloads.types import (
    SimpleFindingPayload,
)
from .schema import (
    MUTATION,
)


@MUTATION.field("removeEvidence")
@concurrent_decorators(
    require_login,
    enforce_group_level_auth_async,
    require_asm,
    require_finding_access,
)
async def mutate(
    _: None,
    info: GraphQLResolveInfo,
    evidence_id: str,
    finding_id: str,
) -> SimpleFindingPayload:
    loaders: Dataloaders = info.context.loaders
    try:
        await findings_domain.remove_evidence(loaders, evidence_id, finding_id)
        logs_utils.cloudwatch_log(
            info.context,
            "Removed evidence in the finding",
            extra={"finding_id": finding_id, "log_type": "Security"},
        )
    except APP_EXCEPTIONS:
        logs_utils.cloudwatch_log(
            info.context,
            "Attempted to remove evidence in the finding",
            extra={"finding_id": finding_id, "log_type": "Security"},
        )
        raise

    loaders.finding.clear(finding_id)
    finding = await get_finding(loaders, finding_id)

    return SimpleFindingPayload(finding=finding, success=True)
