from integrates.api.mutations.download_git_roots_file import mutate
from integrates.context import CI_COMMIT_REF_NAME
from integrates.decorators import (
    enforce_group_level_auth_async,
    require_attribute,
    require_attribute_internal,
    require_login,
)
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    CredentialsFaker,
    CredentialsStateFaker,
    GitRootFaker,
    GitRootStateFaker,
    GraphQLResolveInfoFaker,
    GroupAccessFaker,
    GroupAccessStateFaker,
    GroupFaker,
    OrganizationAccessFaker,
    OrganizationAccessStateFaker,
    OrganizationFaker,
    RootEnvironmentUrlFaker,
    StakeholderFaker,
    random_uuid,
)
from integrates.testing.mocks import mocks

ORG_ID = "ORG#6b110f25-cd46-e94c-2bad-b9e182cf91a8"
ROOT_ID = "38eb8f25-7945-4173-ab6e-0af4ad8b7ef3"
CRED_ID = "credential-test-id"
ORG_NAME = "orgtest"
GROUP_NAME = "testgroup"
EMAIL_TEST = "testuser@orgtest.com"


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            credentials=[
                CredentialsFaker(
                    credential_id=CRED_ID,
                    organization_id=ORG_ID,
                    state=CredentialsStateFaker(modified_by=EMAIL_TEST, name="test-cred-name"),
                )
            ],
            organizations=[OrganizationFaker(id=ORG_ID, name=ORG_NAME)],
            groups=[GroupFaker(name=GROUP_NAME, organization_id=ORG_ID)],
            stakeholders=[StakeholderFaker(email=EMAIL_TEST, enrolled=True)],
            organization_access=[
                OrganizationAccessFaker(
                    organization_id=ORG_ID,
                    email=EMAIL_TEST,
                    state=OrganizationAccessStateFaker(has_access=True, role="group_manager"),
                ),
            ],
            group_access=[
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email=EMAIL_TEST,
                    state=GroupAccessStateFaker(has_access=True, role="group_manager"),
                ),
            ],
            root_environment_urls=[
                RootEnvironmentUrlFaker(
                    id=random_uuid(), root_id=ROOT_ID, group_name=GROUP_NAME, url="https://test.com"
                )
            ],
            roots=[
                GitRootFaker(
                    created_by=EMAIL_TEST,
                    id=ROOT_ID,
                    group_name=GROUP_NAME,
                    organization_name=ORG_NAME,
                    state=GitRootStateFaker(credential_id=CRED_ID, nickname="git1"),
                )
            ],
        ),
    ),
)
async def test_download_git_roots_file() -> None:
    # Arrange
    info = GraphQLResolveInfoFaker(
        user_email=EMAIL_TEST,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_attribute_internal, "is_under_review", GROUP_NAME],
            [require_attribute, "has_asm", GROUP_NAME],
        ],
    )

    # Act
    result = await mutate(_=None, info=info, group_name=GROUP_NAME)

    # Assert
    assert result
    assert result.success
    assert (result.url).startswith(
        f"https://s3.amazonaws.com/integrates.dev/{CI_COMMIT_REF_NAME}/reports/testgroup_git-roots_"
    )
