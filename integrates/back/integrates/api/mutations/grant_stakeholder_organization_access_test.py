from integrates.api.mutations.grant_stakeholder_organization_access import mutate
from integrates.custom_exceptions import InvalidRoleProvided, StakeholderHasOrganizationAccess
from integrates.custom_utils import utils
from integrates.dataloaders import get_new_context
from integrates.db_model.organization_access.types import OrganizationAccessRequest
from integrates.decorators import enforce_organization_level_auth_async
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    GraphQLResolveInfoFaker,
    OrganizationAccessFaker,
    OrganizationAccessStateFaker,
    OrganizationFaker,
    StakeholderFaker,
)
from integrates.testing.mocks import Mock, mocks
from integrates.testing.utils import raises

ORG_ID = "ORG#40f6da5f-4f66-4bf0-825b-a2d9748ad6db"


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id=ORG_ID, name="test-org")],
            stakeholders=[StakeholderFaker(email="requester@corporate.com", role="admin")],
            organization_access=[
                OrganizationAccessFaker(
                    organization_id=ORG_ID,
                    email="requester@corporate.com",
                    state=OrganizationAccessStateFaker(has_access=True, role="admin"),
                ),
            ],
        ),
    )
)
async def test_grant_stakeholder_organization_access_invalid_role() -> None:
    info = GraphQLResolveInfoFaker(
        user_email="requester@corporate.com",
        decorators=[[enforce_organization_level_auth_async]],
    )

    with raises(InvalidRoleProvided):
        await mutate(
            _=None,
            info=info,
            organization_id=ORG_ID,
            user_email="new@corporate.com",
            role="invalid_role",
        )


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            stakeholders=[
                StakeholderFaker(email="requester@corporate.com", role="admin"),
                StakeholderFaker(email="existing@corporate.com", role="customer"),
            ],
            organizations=[
                OrganizationFaker(name="test_org", id=ORG_ID),
            ],
            organization_access=[
                OrganizationAccessFaker(
                    organization_id=ORG_ID,
                    email="existing@corporate.com",
                    state=OrganizationAccessStateFaker(has_access=True),
                ),
            ],
        ),
    )
)
async def test_grant_stakeholder_organization_access_already_has_access() -> None:
    info = GraphQLResolveInfoFaker(
        user_email="requester@corporate.com",
        decorators=[[enforce_organization_level_auth_async]],
    )
    with raises(StakeholderHasOrganizationAccess):
        await mutate(
            _=None,
            info=info,
            organization_id=ORG_ID,
            user_email="existing@corporate.com",
            role="customer",
        )


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(name="test-org", id=ORG_ID)],
            stakeholders=[StakeholderFaker(email="requester@corporate.com", role="admin")],
            organization_access=[
                OrganizationAccessFaker(
                    organization_id=ORG_ID,
                    email="requester@corporate.com",
                    state=OrganizationAccessStateFaker(has_access=True, role="admin"),
                ),
            ],
        ),
    ),
    others=[Mock(utils, "is_personal_email", "async", False)],
)
async def test_grant_stakeholder_organization_access_success() -> None:
    info = GraphQLResolveInfoFaker(
        user_email="requester@corporate.com",
        decorators=[[enforce_organization_level_auth_async]],
    )

    result = await mutate(
        _=None,
        info=info,
        organization_id=ORG_ID,
        user_email="new@corporate.com",
        role="user",
    )
    assert result.success
    assert result.granted_stakeholder.email == "new@corporate.com"

    loaders = get_new_context()
    access = await loaders.organization_access.load(
        OrganizationAccessRequest(
            organization_id=ORG_ID,
            email="new@corporate.com",
        ),
    )
    assert access is not None
