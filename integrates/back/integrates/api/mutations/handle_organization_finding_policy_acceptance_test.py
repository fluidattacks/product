from integrates.api.mutations import handle_organization_finding_policy_acceptance
from integrates.api.mutations.handle_organization_finding_policy_acceptance import mutate
from integrates.batch.types import PutActionResult
from integrates.custom_exceptions import OrgFindingPolicyNotFound, PolicyAlreadyHandled
from integrates.dataloaders import Dataloaders, get_new_context
from integrates.db_model.enums import TreatmentStatus
from integrates.db_model.organization_finding_policies.enums import PolicyStateStatus
from integrates.decorators import enforce_organization_level_auth_async, require_login
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    GraphQLResolveInfoFaker,
    OrganizationAccessFaker,
    OrganizationFaker,
    OrgFindingPolicyFaker,
    OrgFindingPolicyStateFaker,
    StakeholderFaker,
)
from integrates.testing.mocks import Mock, mocks
from integrates.testing.utils import raises

from .payloads.types import SimplePayload

EMAIL_TEST = "jdoe@fluidattacks.com"
FIN_ID = "test-finding-policy-id"
FIN_NAME = "265. Insecure encryption algorithm - AES"
ORG_ID = "43245t-1bf5-902l-2df9-9ot4l2s4"
ORG_NAME = "orgtest"


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organization_finding_policies=[
                OrgFindingPolicyFaker(
                    id=FIN_ID,
                    name=FIN_NAME,
                    organization_name=ORG_NAME,
                    state=OrgFindingPolicyStateFaker(
                        modified_by=EMAIL_TEST, status=PolicyStateStatus.SUBMITTED
                    ),
                    treatment_acceptance=TreatmentStatus.ACCEPTED_UNDEFINED,
                )
            ],
            organization_access=[OrganizationAccessFaker(organization_id=ORG_ID, email=EMAIL_TEST)],
            organizations=[OrganizationFaker(id=ORG_ID, name=ORG_NAME)],
            stakeholders=[StakeholderFaker(email=EMAIL_TEST, role="customer_manager")],
        ),
    ),
    others=[
        Mock(
            handle_organization_finding_policy_acceptance,
            "put_action",
            "async",
            PutActionResult(success=True),
        )
    ],
)
async def test_handle_organization_finding_policy_acceptance() -> None:
    # Arrange
    info = GraphQLResolveInfoFaker(
        user_email=EMAIL_TEST,
        decorators=[[require_login], [enforce_organization_level_auth_async]],
    )

    # Act
    loaders: Dataloaders = get_new_context()
    result = await mutate(
        _parent=None,
        info=info,
        finding_policy_id=FIN_ID,
        organization_name=ORG_NAME,
        status="APPROVED",
        treatment_acceptance="ACCEPTED_UNDEFINED",
    )
    org_finding_policies = await loaders.organization_finding_policies.load(ORG_NAME)

    # Assert
    assert result == SimplePayload(success=True)
    assert org_finding_policies
    assert org_finding_policies[0].name == FIN_NAME
    assert org_finding_policies[0].state.status == PolicyStateStatus.APPROVED


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organization_access=[OrganizationAccessFaker(organization_id=ORG_ID, email=EMAIL_TEST)],
            organizations=[OrganizationFaker(id=ORG_ID, name=ORG_NAME)],
            stakeholders=[StakeholderFaker(email=EMAIL_TEST, role="customer_manager")],
        ),
    )
)
async def test_handle_organization_finding_policy_acceptance_fail() -> None:
    # Arrange
    info = GraphQLResolveInfoFaker(
        user_email=EMAIL_TEST,
        decorators=[[require_login], [enforce_organization_level_auth_async]],
    )

    # Assert
    with raises(OrgFindingPolicyNotFound):
        await mutate(
            _parent=None,
            info=info,
            finding_policy_id=FIN_ID,
            organization_name=ORG_NAME,
            status="APPROVED",
            treatment_acceptance="ACCEPTED",
        )


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organization_finding_policies=[
                OrgFindingPolicyFaker(
                    id=FIN_ID,
                    name=FIN_NAME,
                    organization_name=ORG_NAME,
                    state=OrgFindingPolicyStateFaker(modified_by=EMAIL_TEST),
                )
            ],
            organization_access=[OrganizationAccessFaker(organization_id=ORG_ID, email=EMAIL_TEST)],
            organizations=[OrganizationFaker(id=ORG_ID, name=ORG_NAME)],
            stakeholders=[StakeholderFaker(email=EMAIL_TEST, role="customer_manager")],
        ),
    )
)
async def test_handle_organization_finding_policy_acceptance_fail_status() -> None:
    # Arrange
    info = GraphQLResolveInfoFaker(
        user_email=EMAIL_TEST,
        decorators=[[require_login], [enforce_organization_level_auth_async]],
    )

    # Assert
    with raises(PolicyAlreadyHandled):
        await mutate(
            _parent=None,
            info=info,
            finding_policy_id=FIN_ID,
            organization_name=ORG_NAME,
            status="APPROVED",
            treatment_acceptance="ACCEPTED",
        )
