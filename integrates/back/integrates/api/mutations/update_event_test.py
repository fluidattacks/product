from integrates.api.mutations.update_event import mutate
from integrates.custom_utils.events import get_event
from integrates.dataloaders import get_new_context
from integrates.db_model.events.enums import EventSolutionReason, EventStateStatus, EventType
from integrates.db_model.events.types import EventRequest
from integrates.decorators import (
    enforce_group_level_auth_async,
    require_asm,
    require_attribute,
    require_login,
)
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    EventFaker,
    EventStateFaker,
    GraphQLResolveInfoFaker,
    GroupFaker,
    OrganizationFaker,
    StakeholderFaker,
)
from integrates.testing.mocks import mocks

EMAIL_TEST = "admin@fluidattacks.com"
GROUP_NAME = "group1"


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            stakeholders=[StakeholderFaker(email=EMAIL_TEST, role="admin")],
            groups=[GroupFaker(name=GROUP_NAME, organization_id="org1")],
            events=[
                EventFaker(
                    id="event1",
                    group_name=GROUP_NAME,
                    type=EventType.AUTHORIZATION_SPECIAL_ATTACK,
                    description="Initial description",
                    state=EventStateFaker(
                        status=EventStateStatus.SOLVED,
                    ),
                ),
            ],
        ),
    )
)
async def test_update_event_success() -> None:
    loaders = get_new_context()
    info = GraphQLResolveInfoFaker(
        user_email=EMAIL_TEST,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_asm],
            [require_attribute, "has_asm", GROUP_NAME],
        ],
    )
    result = await mutate(
        _parent=None,
        info=info,
        event_id="event1",
        group_name=GROUP_NAME,
        event_type=EventType.CLONING_ISSUES,
        event_description="Updated description",
        solving_reason=EventSolutionReason.CLONED_SUCCESSFULLY,
        other_solving_reason="Custom reason",
    )
    assert result.success is True

    updated_event = await get_event(loaders, EventRequest(event_id="event1", group_name=GROUP_NAME))
    assert updated_event.type == EventType.CLONING_ISSUES
    assert updated_event.description == "Updated description"
    assert updated_event.state.reason == EventSolutionReason.CLONED_SUCCESSFULLY


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            stakeholders=[StakeholderFaker(email=EMAIL_TEST, role="admin")],
            groups=[GroupFaker(name=GROUP_NAME, organization_id="org1")],
            events=[
                EventFaker(
                    id="event1",
                    group_name=GROUP_NAME,
                    type=EventType.AUTHORIZATION_SPECIAL_ATTACK,
                ),
            ],
        ),
    )
)
async def test_update_event_partial() -> None:
    loaders = get_new_context()
    info = GraphQLResolveInfoFaker(
        user_email=EMAIL_TEST,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_asm],
            [require_attribute, "has_asm", GROUP_NAME],
        ],
    )
    result = await mutate(
        _parent=None,
        info=info,
        event_id="event1",
        group_name=GROUP_NAME,
        event_description="Only description updated",
        event_type=EventType.AUTHORIZATION_SPECIAL_ATTACK,
    )
    assert result.success is True

    request = EventRequest(event_id="event1", group_name=GROUP_NAME)
    loaders.event.clear(request)
    updated_event = await get_event(loaders, request)
    assert updated_event.type == EventType.AUTHORIZATION_SPECIAL_ATTACK
    assert updated_event.description == "Only description updated"
    assert updated_event.state.reason is None
    assert updated_event.state.other is None
