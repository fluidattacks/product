from integrates.api.mutations.request_vulnerabilities_verification import mutate
from integrates.decorators import (
    enforce_group_level_auth_async,
    require_asm,
    require_attribute,
    require_attribute_internal,
    require_continuous,
    require_finding_access,
    require_is_not_under_review,
    require_login,
    require_report_vulnerabilities,
)
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    FindingFaker,
    GraphQLResolveInfoFaker,
    GroupAccessFaker,
    GroupAccessStateFaker,
    GroupFaker,
    StakeholderFaker,
    VulnerabilityFaker,
)
from integrates.testing.mocks import mocks
from integrates.testing.utils import parametrize, raises

ADMIN_EMAIL = "admin@gmail.com"
USER_EMAIL = "user@gmail.com"
VULNERABILITY_MANAGER_EMAIL = "vulnerability_manager@fluidattacks.com"
HACKER_EMAIL = "hacker@fluidattacks.com"
REVIEWER_EMAIL = "reviewer@fluidattacks.com"
GROUP_NAME = "test-group"
CUSTOMER_MANAGER_EMAIL = "customer_manager@fluidattacks.com"
FINDING_ID = "3c475384-834c-47b0-ac71-a41a022e401c"


@parametrize(
    args=["email"],
    cases=[
        [ADMIN_EMAIL],
        [HACKER_EMAIL],
        [REVIEWER_EMAIL],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            stakeholders=[
                StakeholderFaker(email=ADMIN_EMAIL),
                StakeholderFaker(email=HACKER_EMAIL),
                StakeholderFaker(email=REVIEWER_EMAIL),
            ],
            findings=[FindingFaker(id=FINDING_ID, group_name=GROUP_NAME)],
            vulnerabilities=[
                VulnerabilityFaker(id="vuln1", finding_id=FINDING_ID),
                VulnerabilityFaker(id="vuln2", finding_id=FINDING_ID),
            ],
            group_access=[
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email=ADMIN_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="admin"),
                ),
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email=HACKER_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="hacker"),
                ),
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email=REVIEWER_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="reviewer"),
                ),
            ],
            groups=[GroupFaker(name=GROUP_NAME)],
        )
    )
)
async def test_request_vulnerabilities_verification_success(email: str) -> None:
    info = GraphQLResolveInfoFaker(
        user_email=email,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_is_not_under_review],
            [require_attribute_internal, "is_under_review", GROUP_NAME],
            [require_continuous],
            [require_attribute, "is_continuous", GROUP_NAME],
            [require_asm],
            [require_attribute, "has_asm", GROUP_NAME],
            [require_report_vulnerabilities],
            [require_attribute, "can_report_vulnerabilities", GROUP_NAME],
            [require_finding_access],
        ],
    )
    result = await mutate(
        _=None,
        info=info,
        finding_id=FINDING_ID,
        justification="Test verification request",
        vulnerabilities=["vuln1", "vuln2"],
    )
    assert result.success


@parametrize(
    args=["email"],
    cases=[
        [USER_EMAIL],
        [CUSTOMER_MANAGER_EMAIL],
        [VULNERABILITY_MANAGER_EMAIL],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            stakeholders=[
                StakeholderFaker(email=USER_EMAIL),
                StakeholderFaker(email=CUSTOMER_MANAGER_EMAIL),
                StakeholderFaker(email=VULNERABILITY_MANAGER_EMAIL),
            ],
            findings=[FindingFaker(id=FINDING_ID, group_name=GROUP_NAME)],
            vulnerabilities=[
                VulnerabilityFaker(id="vuln1", finding_id=FINDING_ID),
                VulnerabilityFaker(id="vuln2", finding_id=FINDING_ID),
            ],
            group_access=[
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email=USER_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="user"),
                ),
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email=CUSTOMER_MANAGER_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="customer_manager"),
                ),
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email=VULNERABILITY_MANAGER_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="vulnerability_manager"),
                ),
            ],
        )
    )
)
async def test_request_vulnerabilities_verification_unauthorized(email: str) -> None:
    info = GraphQLResolveInfoFaker(
        user_email=email,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_is_not_under_review],
            [require_attribute_internal, "is_under_review", GROUP_NAME],
            [require_continuous],
            [require_attribute, "is_continuous", GROUP_NAME],
            [require_asm],
            [require_attribute, "has_asm", GROUP_NAME],
            [require_report_vulnerabilities],
            [require_attribute, "can_report_vulnerabilities", GROUP_NAME],
            [require_finding_access],
        ],
    )
    with raises(Exception, match="Access denied"):
        await mutate(
            _=None,
            info=info,
            finding_id=FINDING_ID,
            justification="Test verification request",
            vulnerabilities=["vuln1"],
        )


@parametrize(
    args=["email"],
    cases=[
        [ADMIN_EMAIL],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            stakeholders=[
                StakeholderFaker(email=ADMIN_EMAIL),
            ],
            findings=[FindingFaker(id=FINDING_ID, group_name=GROUP_NAME)],
            vulnerabilities=[
                VulnerabilityFaker(id="vuln1", finding_id=FINDING_ID),
                VulnerabilityFaker(id="vuln2", finding_id=FINDING_ID),
            ],
            group_access=[
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email=ADMIN_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="admin"),
                ),
            ],
        )
    )
)
async def test_request_vulnerabilities_verification_invalid_data(email: str) -> None:
    info = GraphQLResolveInfoFaker(
        user_email=email,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_is_not_under_review],
            [require_attribute_internal, "is_under_review", GROUP_NAME],
            [require_continuous],
            [require_attribute, "is_continuous", GROUP_NAME],
            [require_asm],
            [require_attribute, "has_asm", GROUP_NAME],
            [require_report_vulnerabilities],
            [require_attribute, "can_report_vulnerabilities", GROUP_NAME],
            [require_finding_access],
        ],
    )
    with raises(Exception):
        await mutate(
            _=None,
            info=info,
            finding_id=FINDING_ID,
            justification="",
            vulnerabilities=[],
        )
    with raises(Exception):
        await mutate(
            _=None,
            info=info,
            finding_id="invalid-finding-id",
            justification="Test verification request",
            vulnerabilities=["vuln1"],
        )
