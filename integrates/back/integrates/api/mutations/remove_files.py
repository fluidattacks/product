import logging
import logging.config
import re

from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.class_types.types import (
    Item,
)
from integrates.custom_exceptions import (
    ErrorUpdatingGroup,
)
from integrates.custom_utils import (
    logs as logs_utils,
)
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.decorators import (
    concurrent_decorators,
    enforce_group_level_auth_async,
    require_asm,
    require_is_not_under_review,
    require_login,
)
from integrates.groups import (
    domain as groups_domain,
)
from integrates.sessions import (
    domain as sessions_domain,
)

from .payloads.types import (
    SimplePayload,
)
from .schema import (
    MUTATION,
)

LOGGER = logging.getLogger(__name__)


@MUTATION.field("removeFiles")
@concurrent_decorators(
    require_login,
    enforce_group_level_auth_async,
    require_is_not_under_review,
    require_asm,
)
async def mutate(
    _: None,
    info: GraphQLResolveInfo,
    files_data_input: Item,
    group_name: str,
) -> SimplePayload:
    loaders: Dataloaders = info.context.loaders
    files_data_input = {
        re.sub(r"_([a-z])", lambda x: x.group(1).upper(), k): v for k, v in files_data_input.items()
    }
    file_name = str(files_data_input.get("fileName"))
    group_name = group_name.lower()
    user_info = await sessions_domain.get_jwt_content(info.context)
    user_email = user_info["user_email"]

    try:
        await groups_domain.remove_file(
            loaders=loaders,
            group_name=group_name,
            file_name=file_name,
            email=user_email,
        )
    except ErrorUpdatingGroup:
        LOGGER.error(
            "Couldn't remove file",
            extra={"extra": {"file_name": file_name, "group_name": group_name}},
        )
        logs_utils.cloudwatch_log(
            info.context,
            "Attempted to remove files in the group",
            extra={"group_name": group_name, "log_type": "Security"},
        )
        raise

    logs_utils.cloudwatch_log(
        info.context,
        "Removed files in the group",
        extra={
            "group_name": group_name,
            "log_type": "Security",
        },
    )

    return SimplePayload(success=True)
