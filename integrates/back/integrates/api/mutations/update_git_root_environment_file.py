from typing import (
    TypedDict,
    Unpack,
)

from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.api.types import (
    APP_EXCEPTIONS,
)
from integrates.custom_utils import (
    logs as logs_utils,
)
from integrates.decorators import (
    concurrent_decorators,
    enforce_group_level_auth_async,
    require_is_not_under_review,
    require_login,
)
from integrates.groups.domain import (
    update_environment_url,
)
from integrates.sessions import (
    domain as sessions_domain,
)

from .payloads.types import (
    SimplePayload,
)
from .schema import (
    MUTATION,
)


class UpdateGitEnvironmentFileArgs(TypedDict):
    group_name: str
    root_id: str
    url_id: str
    file_name: str
    old_file_name: str


@MUTATION.field("updateGitRootEnvironmentFile")
@concurrent_decorators(
    require_login,
    require_is_not_under_review,
    enforce_group_level_auth_async,
)
async def mutate(
    _: None,
    info: GraphQLResolveInfo,
    **kwargs: Unpack[UpdateGitEnvironmentFileArgs],
) -> SimplePayload:
    try:
        user_info = await sessions_domain.get_jwt_content(info.context)
        await update_environment_url(
            loaders=info.context.loaders,
            group_name=kwargs["group_name"],
            root_id=kwargs["root_id"],
            file_name=kwargs["file_name"],
            url_id=kwargs["url_id"],
            old_file_name=kwargs["old_file_name"],
            user_email=user_info["user_email"],
        )
        logs_utils.cloudwatch_log(
            info.context,
            "Updated an environment File in the root",
            extra={
                "group_name": kwargs["group_name"],
                "root_id": kwargs["root_id"],
                "url_id": kwargs["url_id"],
                "log_type": "Security",
            },
        )
    except APP_EXCEPTIONS:
        logs_utils.cloudwatch_log(
            info.context,
            "Attempted to update an environment File in the root",
            extra={
                "group_name": kwargs["group_name"],
                "root_id": kwargs["root_id"],
                "url_id": kwargs["url_id"],
                "log_type": "Security",
            },
        )
        raise

    return SimplePayload(success=True)
