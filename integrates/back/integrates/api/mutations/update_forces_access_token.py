from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.custom_exceptions import (
    InvalidExpirationTime,
)
from integrates.custom_utils import (
    datetime as datetime_utils,
)
from integrates.custom_utils import (
    logs as logs_utils,
)
from integrates.custom_utils.groups import get_group
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.decorators import (
    concurrent_decorators,
    enforce_group_level_auth_async,
    require_is_not_under_review,
)
from integrates.forces import (
    domain as forces_domain,
)
from integrates.forces.utils import (
    format_forces_email,
)
from integrates.groups import (
    domain as groups_domain,
)
from integrates.sessions import (
    domain as sessions_domain,
)
from integrates.stakeholders import (
    domain as stakeholders_domain,
)

from .payloads.types import (
    UpdateAccessTokenPayload,
)
from .schema import (
    MUTATION,
)


@MUTATION.field("updateForcesAccessToken")
@concurrent_decorators(enforce_group_level_auth_async, require_is_not_under_review)
async def mutate(
    _parent: None,
    info: GraphQLResolveInfo,
    group_name: str,
) -> UpdateAccessTokenPayload:
    loaders: Dataloaders = info.context.loaders
    user_info = await sessions_domain.get_jwt_content(info.context)
    responsible = user_info["user_email"]
    group = await get_group(loaders, group_name)

    email = format_forces_email(group_name)
    if not await stakeholders_domain.exists(loaders, email):
        logs_utils.cloudwatch_log(
            info.context,
            "Attempted to update token for a forces user that does not exist",
            extra={
                "group_name": group_name,
                "user_email": user_info["user_email"],
                "forces_user_email": email,
                "log_type": "Security",
            },
        )
        return UpdateAccessTokenPayload(success=False, session_jwt="")

    expiration_time = int(datetime_utils.get_now_plus_delta(days=180).timestamp())
    try:
        result = await groups_domain.update_forces_access_token(
            loaders=loaders,
            group_name=group_name,
            email=email,
            expiration_time=expiration_time,
            responsible=responsible,
        )
        if result:
            logs_utils.cloudwatch_log(
                info.context,
                "Updated access token for the group",
                extra={
                    "user_email": user_info["user_email"],
                    "group_name": group_name,
                    "log_type": "Security",
                },
            )
            await forces_domain.update_token(
                group_name=group_name,
                organization_id=group.organization_id,
                token=result,
            )
            logs_utils.cloudwatch_log(
                info.context,
                "Stored in secretsmanager forces token for a forces user",
                extra={
                    "group_name": group_name,
                    "user_email": user_info["user_email"],
                    "forces_user_email": email,
                    "log_type": "Security",
                },
            )
            return UpdateAccessTokenPayload(success=True, session_jwt=result)

        logs_utils.cloudwatch_log(
            info.context,
            "Attempted to update access token for group",
            extra={
                "group_name": group_name,
                "user_email": user_info["user_email"],
                "forces_user_email": email,
                "log_type": "Security",
            },
        )
        return UpdateAccessTokenPayload(success=False, session_jwt=result)

    except InvalidExpirationTime as exc:
        logs_utils.cloudwatch_log(
            info.context,
            "Attempted to use expiration time greater than six months or minor"
            " than current time",
            extra={
                "group_name": group_name,
                "user_email": user_info["user_email"],
                "log_type": "Security",
            },
        )
        raise exc
