from integrates.api.mutations.update_credentials import mutate
from integrates.custom_exceptions import (
    CredentialAlreadyExists,
    InvalidGitCredentials,
    InvalidParameter,
)
from integrates.dataloaders import Dataloaders, get_new_context
from integrates.db_model.credentials.types import (
    HttpsPatSecret,
    HttpsSecret,
    SshSecret,
)
from integrates.db_model.enums import CredentialType
from integrates.decorators import enforce_organization_level_auth_async, require_login
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    CredentialsFaker,
    CredentialsStateFaker,
    GraphQLResolveInfoFaker,
    OrganizationAccessFaker,
    OrganizationAccessStateFaker,
    OrganizationFaker,
    StakeholderFaker,
)
from integrates.testing.mocks import mocks
from integrates.testing.utils import parametrize, raises

ORG_ID = "ORG#40f6da5f-4f66-4bf0-825b-a2d9748ad6db"
CUSTOMER_MANAGER_EMAIL = "customer_manager@client_app.com"
ORG_MANAGER_EMAIL_TEST = "organization_manager@client_app.com"


@parametrize(
    args=["user_email", "credentials_id", "credentials"],
    cases=[
        [
            CUSTOMER_MANAGER_EMAIL,
            "cred_id_1",
            {
                "name": "cred1",
                "type": "HTTPS",
                "user": "user test 1",
                "password": "lorem.ipsum,Pain.w>oiu(p1",
            },
        ],
        [
            ORG_MANAGER_EMAIL_TEST,
            "cred_id_2",
            {
                "name": "cred2",
                "type": "HTTPS",
                "user": "user test 2",
                "password": "lorem.ipsum,Dolor.w>oiu(p1",
            },
        ],
        [
            ORG_MANAGER_EMAIL_TEST,
            "cred_id_3",
            {
                "name": "cred3",
                "type": "SSH",
                "key": ("LS0tLS1CRUdJTiBPUEVOU1NIIFBSSVZBVEUgS0VZLS0tLS0KTUlJCg=="),
            },
        ],
        [
            ORG_MANAGER_EMAIL_TEST,
            "cred_id_4",
            {
                "name": "cred4",
                "type": "HTTPS",
                "azure_organization": "orgcred5",
                "is_pat": True,
                "token": "token test",
            },
        ],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id=ORG_ID)],
            stakeholders=[
                StakeholderFaker(email=ORG_MANAGER_EMAIL_TEST),
                StakeholderFaker(email=CUSTOMER_MANAGER_EMAIL),
            ],
            organization_access=[
                OrganizationAccessFaker(
                    organization_id=ORG_ID,
                    email=ORG_MANAGER_EMAIL_TEST,
                    state=OrganizationAccessStateFaker(
                        has_access=True, role="organization_manager"
                    ),
                ),
                OrganizationAccessFaker(
                    organization_id=ORG_ID,
                    email=CUSTOMER_MANAGER_EMAIL,
                    state=OrganizationAccessStateFaker(has_access=True, role="customer_manager"),
                ),
            ],
            credentials=[
                CredentialsFaker(
                    credential_id="cred_id_1",
                    organization_id=ORG_ID,
                    state=CredentialsStateFaker(
                        name="cred1",
                        state_type=CredentialType.HTTPS,
                        owner=CUSTOMER_MANAGER_EMAIL,
                    ),
                    secret=HttpsSecret(user="user1", password="pass_123"),  # noqa: S106
                ),
                CredentialsFaker(
                    credential_id="cred_id_2",
                    organization_id=ORG_ID,
                    state=CredentialsStateFaker(
                        name="cred2",
                        state_type=CredentialType.HTTPS,
                        owner=ORG_MANAGER_EMAIL_TEST,
                        is_pat=False,
                    ),
                    secret=HttpsSecret(user="user2", password="pass_456"),  # noqa: S106
                ),
                CredentialsFaker(
                    credential_id="cred_id_3",
                    organization_id=ORG_ID,
                    state=CredentialsStateFaker(
                        name="cred3",
                        state_type=CredentialType.SSH,
                        owner=ORG_MANAGER_EMAIL_TEST,
                        is_pat=False,
                    ),
                    secret=SshSecret(key="123456"),
                ),
                CredentialsFaker(
                    credential_id="cred_id_4",
                    organization_id=ORG_ID,
                    state=CredentialsStateFaker(
                        name="cred4",
                        state_type=CredentialType.HTTPS,
                        owner=ORG_MANAGER_EMAIL_TEST,
                        is_pat=True,
                        azure_organization="azure_org_1",
                    ),
                    secret=HttpsPatSecret(token="my_token"),  # noqa: S106
                ),
            ],
        ),
    )
)
async def test_update_credentials(
    user_email: str,
    credentials_id: str,
    credentials: dict,
) -> None:
    loaders: Dataloaders = get_new_context()

    info = GraphQLResolveInfoFaker(
        user_email=user_email,
        decorators=[
            [require_login],
            [enforce_organization_level_auth_async],
        ],
    )

    await mutate(
        _=None,
        info=info,
        organization_id=ORG_ID,
        credentials_id=credentials_id,
        credentials=credentials,
    )

    org_credentials = await loaders.organization_credentials.load(ORG_ID)
    updated_credentials = next(
        (
            credential
            for credential in org_credentials
            if credential.state.name == credentials["name"]
        ),
        None,
    )
    assert updated_credentials is not None
    assert updated_credentials.state.owner == user_email
    assert updated_credentials.state.name == credentials["name"]
    assert updated_credentials.state.type == CredentialType[credentials["type"]]
    assert getattr(updated_credentials.secret, "token", None) == credentials.get("token")
    assert getattr(updated_credentials.secret, "key", None) == credentials.get("key")
    assert getattr(updated_credentials.secret, "user", None) == credentials.get("user")
    assert getattr(updated_credentials.secret, "password", None) == credentials.get("password")
    assert getattr(updated_credentials.state, "is_pat", False) == credentials.get("is_pat", False)
    assert getattr(updated_credentials.state, "azure_organization", None) == credentials.get(
        "azure_organization",
    )


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id=ORG_ID)],
            stakeholders=[
                StakeholderFaker(email="user@gmail.com"),
            ],
            organization_access=[
                OrganizationAccessFaker(
                    organization_id=ORG_ID,
                    email="user@gmail.com",
                    state=OrganizationAccessStateFaker(has_access=True, role="user"),
                ),
            ],
        ),
    )
)
async def test_update_credentials_access_denied() -> None:
    info = GraphQLResolveInfoFaker(
        user_email="user@gmail.com",
        decorators=[
            [require_login],
            [enforce_organization_level_auth_async],
        ],
    )

    with raises(Exception, match="Access denied"):
        await mutate(
            _=None,
            info=info,
            organization_id=ORG_ID,
            credentials={"name": "cred4", "type": "SSH", "key": "YWJ"},
        )


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id=ORG_ID)],
            stakeholders=[
                StakeholderFaker(email=ORG_MANAGER_EMAIL_TEST),
                StakeholderFaker(email=CUSTOMER_MANAGER_EMAIL),
            ],
            organization_access=[
                OrganizationAccessFaker(
                    organization_id=ORG_ID,
                    email=ORG_MANAGER_EMAIL_TEST,
                    state=OrganizationAccessStateFaker(
                        has_access=True, role="organization_manager"
                    ),
                ),
                OrganizationAccessFaker(
                    organization_id=ORG_ID,
                    email=CUSTOMER_MANAGER_EMAIL,
                    state=OrganizationAccessStateFaker(has_access=True, role="customer_manager"),
                ),
            ],
            credentials=[
                CredentialsFaker(
                    credential_id="cred_id_1",
                    organization_id=ORG_ID,
                    state=CredentialsStateFaker(
                        name="cred1",
                        state_type=CredentialType.HTTPS,
                        owner=CUSTOMER_MANAGER_EMAIL,
                    ),
                    secret=HttpsSecret(user="user1", password="pass_123"),  # noqa: S106
                ),
                CredentialsFaker(
                    credential_id="cred_id_2",
                    organization_id=ORG_ID,
                    state=CredentialsStateFaker(
                        name="another_credential",
                        state_type=CredentialType.HTTPS,
                        owner=ORG_MANAGER_EMAIL_TEST,
                        is_pat=False,
                    ),
                    secret=HttpsSecret(user="user2", password="pass_456"),  # noqa: S106
                ),
            ],
        ),
    )
)
async def test_update_credentials_failures() -> None:
    info = GraphQLResolveInfoFaker(
        user_email=CUSTOMER_MANAGER_EMAIL,
        decorators=[
            [require_login],
            [enforce_organization_level_auth_async],
        ],
    )
    with raises(InvalidParameter):
        await mutate(
            _=None,
            info=info,
            organization_id=ORG_ID,
            credentials_id="cred_id_1",
            credentials={
                "name": "cred2",
                "type": "HTTPS",
                "user": "user test 2",
                "password": "lorem.ipsum,Dolor.w>oiu(p1",
                "is_pat": True,
            },
        )
    with raises(InvalidGitCredentials):
        await mutate(
            _=None,
            info=info,
            organization_id=ORG_ID,
            credentials_id="cred_not_existent",
            credentials={
                "name": "cred2",
                "type": "HTTPS",
                "user": "user test 2",
                "password": "lorem.ipsum,Dolor.w>oiu(p1",
            },
        )
    with raises(CredentialAlreadyExists):
        await mutate(
            _=None,
            info=info,
            organization_id=ORG_ID,
            credentials_id="cred_id_1",
            credentials={
                "name": "another_credential",
                "type": "HTTPS",
                "user": "user test 2",
                "password": "lorem.ipsum,Dolor.w>oiu(p1",
            },
        )
