from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.custom_utils import (
    logs as logs_utils,
)
from integrates.decorators import (
    concurrent_decorators,
    enforce_organization_level_auth_async,
    require_login,
)
from integrates.mailmap import (
    set_mailmap_entry_as_mailmap_subentry,
)

from .payloads.types import (
    SimplePayload,
)
from .schema import (
    MUTATION,
)


@MUTATION.field("setMailmapEntryAsMailmapSubentry")
@concurrent_decorators(
    require_login,
    enforce_organization_level_auth_async,
)
async def mutate(
    _parent: None,
    info: GraphQLResolveInfo,
    entry_email: str,
    target_entry_email: str,
    organization_id: str,
) -> SimplePayload:
    await set_mailmap_entry_as_mailmap_subentry(
        entry_email=entry_email,
        target_entry_email=target_entry_email,
        organization_id=organization_id,
    )

    logs_utils.cloudwatch_log(
        info.context,
        "Set mailmap author as alias",
        extra={
            "entry_email": entry_email,
            "target_entry_email": target_entry_email,
            "organization_id": organization_id,
        },
    )

    return SimplePayload(success=True)
