from aioextensions import collect
from graphql.type.definition import GraphQLResolveInfo
from starlette.datastructures import UploadFile

from integrates.api.mutations.payloads.types import UploadGitRootFilePayload
from integrates.api.types import APP_EXCEPTIONS
from integrates.custom_exceptions import (
    CustomBaseException,
    EmptyFile,
    InvalidCSVFormat,
    InvalidFileType,
)
from integrates.custom_utils import logs as logs_utils
from integrates.custom_utils.groups import get_group
from integrates.dataloaders import Dataloaders
from integrates.db_model.groups.types import Group
from integrates.decorators import (
    concurrent_decorators,
    enforce_group_level_auth_async,
    require_asm,
    require_is_not_under_review,
    require_login,
)
from integrates.roots.domain import get_roots_from_file, process_git_root
from integrates.roots.types import GitRootAttributesToAdd
from integrates.sessions import domain as sessions_domain

from .schema import MUTATION


async def _process_git_root(
    *,
    info: GraphQLResolveInfo,
    attributes_to_add: GitRootAttributesToAdd,
    group: Group,
    user_email: str,
    line_index: int,
    line: str,
) -> str | InvalidCSVFormat:
    try:
        return await process_git_root(
            info=info,
            attributes_to_add=attributes_to_add,
            group=group,
            user_email=user_email,
        )
    except CustomBaseException as exc:
        logs_utils.cloudwatch_log(
            info.context,
            "Attempted to upload root in the group",
            extra={
                "group_name": group.name,
                "exc": exc,
                "log_type": "Security",
            },
        )
        return InvalidCSVFormat(
            field="",
            field_index=0,
            line=line,
            line_index=line_index,
            msg=str(exc),
        )


@MUTATION.field("uploadGitRootFile")
@concurrent_decorators(
    require_login,
    enforce_group_level_auth_async,
    require_is_not_under_review,
    require_asm,
)
async def mutate(
    _: None,
    info: GraphQLResolveInfo,
    group_name: str,
    file: UploadFile,
) -> UploadGitRootFilePayload:
    user_info: dict[str, str] = await sessions_domain.get_jwt_content(info.context)
    user_email: str = user_info["user_email"]
    loaders: Dataloaders = info.context.loaders
    group = await get_group(loaders, group_name.lower())

    try:
        roots_to_process, roots_with_errors = await get_roots_from_file(
            file=file,
            group_name=group_name,
            loaders=loaders,
            organization_id=group.organization_id,
        )
        error_lines: list[str] = []

        _roots_ids = list(
            await collect(
                tuple(
                    _process_git_root(
                        info=info,
                        attributes_to_add=GitRootAttributesToAdd(
                            branch=root["branch"],
                            credentials=root["credentials"],
                            gitignore=root["gitignore"],
                            includes_health_check=root["includes_health_check"],
                            url=root["url"],
                            nickname=root.get("nickname", ""),
                            use_egress=root.get("use_egress", False),
                            use_ztna=root.get("use_ztna", False),
                            criticality=root.get("priority", None),
                        ),
                        group=group,
                        user_email=user_email,
                        line_index=root["line_index"],
                        line=root["line"],
                    )
                    for root in roots_to_process
                ),
                workers=32,
            ),
        )
        roots_ids = [root_id for root_id in _roots_ids if isinstance(root_id, str)]
        roots_with_errors.extend([id for id in _roots_ids if not isinstance(id, str)])

        logs_utils.cloudwatch_log(
            info.context,
            "Uploaded roots in the group",
            extra={
                "roots": roots_ids,
                "group_name": group_name,
                "roots_ids_exc": len(roots_with_errors),
                "log_type": "Security",
            },
        )

        if len(roots_with_errors) > 0:
            error_lines = [
                f"{exc.line_index}|{exc.field_index + 1}|{exc.field}|{exc.line}|{exc.msg}"
                for exc in roots_with_errors
            ]

        return UploadGitRootFilePayload(
            success=True,
            message=f"Roots created: {len(roots_ids)}",
            error_lines=error_lines,
            total_success=len(roots_ids),
            total_errors=len(roots_with_errors),
        )

    except InvalidFileType:
        return UploadGitRootFilePayload(
            success=False,
            message="Only CSV files are supported.",
            error_lines=[],
            total_success=0,
            total_errors=0,
        )

    except EmptyFile:
        return UploadGitRootFilePayload(
            success=False,
            message="The file is empty.",
            error_lines=[],
            total_success=0,
            total_errors=0,
        )

    except APP_EXCEPTIONS:
        logs_utils.cloudwatch_log(
            info.context,
            "Attempted to upload roots file in the group",
            extra={
                "group_name": group_name,
                "log_type": "Security",
            },
        )
        raise
