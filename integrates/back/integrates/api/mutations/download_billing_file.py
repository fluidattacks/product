from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.billing import (
    domain as billing_domain,
)
from integrates.custom_exceptions import (
    ErrorDownloadingFile,
    InvalidBillingPaymentMethod,
)
from integrates.custom_utils import (
    logs as logs_utils,
)
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.decorators import (
    concurrent_decorators,
    enforce_organization_level_auth_async,
    require_login,
)
from integrates.organizations import (
    utils as orgs_utils,
)

from .payloads.types import (
    DownloadFilePayload,
)
from .schema import (
    MUTATION,
)


@MUTATION.field("downloadBillingFile")
@concurrent_decorators(
    require_login,
    enforce_organization_level_auth_async,
)
async def mutate(
    _: None,
    info: GraphQLResolveInfo,
    payment_method_id: str,
    file_name: str,
    **kwargs: str,
) -> DownloadFilePayload:
    loaders: Dataloaders = info.context.loaders
    organization = await orgs_utils.get_organization(loaders, kwargs["organization_id"])
    try:
        signed_url = await billing_domain.get_document_link(
            organization,
            payment_method_id,
            file_name,
        )
        if signed_url:
            logs_utils.cloudwatch_log(
                info.context,
                "Downloaded file of the payment method",
                extra={
                    "payment_method_id": payment_method_id,
                    "log_type": "Security",
                },
            )
    except InvalidBillingPaymentMethod as exc:
        logs_utils.cloudwatch_log(
            info.context,
            "Attempted to download file of the payment method",
            extra={
                "payment_method_id": payment_method_id,
                "log_type": "Security",
            },
        )
        raise ErrorDownloadingFile.new() from exc

    return DownloadFilePayload(success=True, url=signed_url)
