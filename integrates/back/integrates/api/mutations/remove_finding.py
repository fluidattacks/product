from aioextensions import (
    schedule,
)
from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.api.types import (
    APP_EXCEPTIONS,
)
from integrates.custom_utils import (
    logs as logs_utils,
)
from integrates.custom_utils import (
    requests as requests_utils,
)
from integrates.custom_utils.findings import get_finding
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.enums import (
    StateRemovalJustification,
)
from integrates.decorators import (
    concurrent_decorators,
    enforce_group_level_auth_async,
    require_asm,
    require_finding_access,
    require_login,
)
from integrates.findings import (
    domain as findings_domain,
)
from integrates.mailer import (
    findings as findings_mail,
)
from integrates.sessions import (
    domain as sessions_domain,
)

from .payloads.types import (
    SimplePayload,
)
from .schema import (
    MUTATION,
)


@MUTATION.field("removeFinding")
@concurrent_decorators(
    require_login,
    enforce_group_level_auth_async,
    require_asm,
    require_finding_access,
)
async def mutate(
    _: None,
    info: GraphQLResolveInfo,
    finding_id: str,
    justification: str,
) -> SimplePayload:
    try:
        loaders: Dataloaders = info.context.loaders
        user_info = await sessions_domain.get_jwt_content(info.context)
        user_email = user_info["user_email"]
        state_justification = StateRemovalJustification[justification]
        finding = await get_finding(loaders, finding_id)
        source = requests_utils.get_source_new(info.context)
        await findings_domain.remove_finding(
            loaders=loaders,
            email=user_email,
            finding_id=finding_id,
            justification=state_justification,
            source=source,
        )
        schedule(
            findings_mail.send_mail_remove_finding(
                loaders,
                finding,
                state_justification,
                user_email,
            ),
        )
        logs_utils.cloudwatch_log(
            info.context,
            "Removed finding",
            extra={"finding_id": finding_id, "log_type": "Security"},
        )
    except APP_EXCEPTIONS:
        logs_utils.cloudwatch_log(
            info.context,
            "Attempted to remove finding",
            extra={"finding_id": finding_id, "log_type": "Security"},
        )
        raise

    return SimplePayload(success=True)
