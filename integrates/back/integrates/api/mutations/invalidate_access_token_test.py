from integrates.api.mutations.invalidate_access_token import mutate
from integrates.api.mutations.payloads.types import SimplePayload
from integrates.db_model.stakeholders.types import Stakeholder
from integrates.decorators import (
    enforce_group_level_auth_async,
    require_attribute,
    require_attribute_internal,
    require_is_not_under_review,
    require_login,
)
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    DATE_2019,
    GraphQLResolveInfoFaker,
    GroupAccessFaker,
    GroupAccessStateFaker,
    GroupFaker,
    OrganizationFaker,
    StakeholderFaker,
)
from integrates.testing.mocks import mocks
from integrates.testing.utils import parametrize


@parametrize(
    args=["email"],
    cases=[
        ["admin@fluidattacks.com"],
        ["hacker@fluidattacks.com"],
        ["reattacker@fluidattacks.com"],
        ["user@gmail.com"],
        ["group_manager@gmail.com"],
        ["org_manager@gmail.com"],
        ["vulnerability_manager@gmail.com"],
        ["resourcer@fluidattacks.com"],
        ["reviewer@fluidattacks.com"],
        ["forces.test-group@fluidattacks.com"],
        ["customer_manager@fluidattacks.com"],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            groups=[
                GroupFaker(organization_id="org1"),
            ],
            stakeholders=[
                StakeholderFaker(email="admin@fluidattacks.com"),
                StakeholderFaker(email="hacker@fluidattacks.com"),
                StakeholderFaker(email="reattacker@fluidattacks.com"),
                StakeholderFaker(email="user@gmail.com"),
                StakeholderFaker(email="group_manager@gmail.com"),
                StakeholderFaker(email="org_manager@gmail.com"),
                StakeholderFaker(email="vulnerability_manager@gmail.com"),
                StakeholderFaker(email="resourcer@fluidattacks.com"),
                StakeholderFaker(email="reviewer@fluidattacks.com"),
                StakeholderFaker(email="forces.test-group@fluidattacks.com"),
                StakeholderFaker(email="customer_manager@fluidattacks.com"),
            ],
            group_access=[
                GroupAccessFaker(
                    email="admin@fluidattacks.com",
                    state=GroupAccessStateFaker(has_access=True, role="admin"),
                ),
                GroupAccessFaker(
                    email="hacker@fluidattacks.com",
                    state=GroupAccessStateFaker(has_access=True, role="hacker"),
                ),
                GroupAccessFaker(
                    email="reattacker@fluidattacks.com",
                    state=GroupAccessStateFaker(has_access=True, role="reattacker"),
                ),
                GroupAccessFaker(
                    email="user@gmail.com",
                    state=GroupAccessStateFaker(has_access=True, role="user"),
                ),
                GroupAccessFaker(
                    email="group_manager@gmail.com",
                    state=GroupAccessStateFaker(has_access=True, role="group_manager"),
                ),
                GroupAccessFaker(
                    email="org_manager@gmail.com",
                    state=GroupAccessStateFaker(has_access=True, role="organization_manager"),
                ),
                GroupAccessFaker(
                    email="vulnerability_manager@gmail.com",
                    state=GroupAccessStateFaker(has_access=True, role="vulnerability_manager"),
                ),
                GroupAccessFaker(
                    email="resourcer@fluidattacks.com",
                    state=GroupAccessStateFaker(has_access=True, role="resourcer"),
                ),
                GroupAccessFaker(
                    email="reviewer@fluidattacks.com",
                    state=GroupAccessStateFaker(has_access=True, role="reviewer"),
                ),
                GroupAccessFaker(
                    email="forces.test-group@fluidattacks.com",
                    state=GroupAccessStateFaker(has_access=True, role="service_forces"),
                ),
                GroupAccessFaker(
                    email="customer_manager@fluidattacks.com",
                    state=GroupAccessStateFaker(has_access=True, role="customer_manager"),
                ),
            ],
            organizations=[
                OrganizationFaker(id="org1"),
            ],
        ),
    )
)
async def test_invalidate_access_token(email: str) -> None:
    info = GraphQLResolveInfoFaker(
        user_email=email,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_is_not_under_review],
            [require_attribute_internal, "is_under_review", "test-group"],
            [require_attribute, "has_asm", "test-group"],
        ],
    )

    # Not all roles have permission to see the group stakeholders
    stakeholder_with_token: Stakeholder = await info.context.loaders.stakeholder.load(email)
    assert (
        len(stakeholder_with_token.access_tokens) == 1
        and stakeholder_with_token.access_tokens[0].issued_at == DATE_2019.timestamp()
    )

    result: SimplePayload = await mutate(
        _=None,
        info=info,
        user=email,
    )
    assert result.success

    info.context.loaders.stakeholder.clear_all()
    stakeholder_without_token: Stakeholder = await info.context.loaders.stakeholder.load(email)
    assert stakeholder_without_token.access_tokens == []
