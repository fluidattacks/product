from integrates import batch
from integrates.api.mutations.add_git_root import mutate
from integrates.batch.dal.get import get_actions
from integrates.batch.enums import Action, IntegratesBatchQueue
from integrates.batch.types import DependentAction
from integrates.custom_exceptions import RepeatedRoot
from integrates.dataloaders import get_new_context
from integrates.db_model.credentials.types import HttpsSecret
from integrates.db_model.enums import CredentialType
from integrates.db_model.integration_repositories.types import (
    OrganizationIntegrationRepositoryRequest,
)
from integrates.db_model.roots.enums import RootCriticality, RootStatus
from integrates.db_model.roots.types import GitRoot, RootRequest
from integrates.decorators import (
    enforce_group_level_auth_async,
    require_attribute,
    require_attribute_internal,
    require_login,
)
from integrates.roots import domain as roots_domain
from integrates.roots import validations
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    CredentialsFaker,
    GitRootFaker,
    GitRootStateFaker,
    GraphQLResolveInfoFaker,
    GroupAccessFaker,
    GroupAccessStateFaker,
    GroupFaker,
    OrganizationFaker,
    OrganizationIntegrationRepositoryFaker,
    StakeholderFaker,
)
from integrates.testing.mocks import Mock, mocks
from integrates.testing.utils import parametrize, raises

EMAIL_TEST = "admin@fluidattacks.com"
GROUP_NAME = "group1"
ROOT_ID = "root1"


@parametrize(
    args=["email"],
    cases=[["hacker@gmail.com"], ["reattacker@gmail.com"]],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            stakeholders=[
                StakeholderFaker(email="hacker@gmail.com", role="hacker"),
                StakeholderFaker(email="reattacker@gmail.com", role="reattacker"),
            ],
            groups=[GroupFaker(name=GROUP_NAME, organization_id="org1")],
            group_access=[
                GroupAccessFaker(
                    email="hacker@gmail.com",
                    state=GroupAccessStateFaker(has_access=True, role="hacker"),
                ),
                GroupAccessFaker(
                    email="reattacker@gmail.com",
                    state=GroupAccessStateFaker(has_access=True, role="reattacker"),
                ),
            ],
        ),
    )
)
async def test_add_git_root_access_denied(email: str) -> None:
    info = GraphQLResolveInfoFaker(
        user_email=email,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_attribute, "has_service_white", GROUP_NAME],
            [require_attribute_internal, "is_under_review", GROUP_NAME],
        ],
    )

    with raises(Exception, match="Access denied"):
        await mutate(
            _parent=None,
            info=info,
            group_name=GROUP_NAME,
            branch="trunk",
            includes_health_check=False,
            url="https://gitlab.com/fluidattacks/universe",
            credentials=None,
            criticality=None,
            gitignore=None,
            nickname=None,
            use_egress=False,
            use_vpn=False,
            use_ztna=False,
        )


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            stakeholders=[StakeholderFaker(email="adming@fluidattacks.com", role="admin")],
            groups=[GroupFaker(name=GROUP_NAME, organization_id="org1")],
            credentials=[
                CredentialsFaker(
                    credential_id="cred1",
                    organization_id="ORG#org1",
                    secret=HttpsSecret(user="user@test.test", password=""),
                )
            ],
            organization_integration_repositories=[
                OrganizationIntegrationRepositoryFaker(
                    id="549fa00adacf621dd920afb648fef37422d416da52a1902c378e8b62e5f46902",
                    organization_id="org1",
                    url="https://github.com/fluidattacks/makes.git",
                    credential_id="cred1",
                )
            ],
        )
    ),
    others=[
        Mock(roots_domain, "report_activity", "sync", None),
        Mock(
            validations,
            "validate_and_get_redirected_url",
            "async",
            "https://gitlab.com/fluidattacks/makes.git",
        ),
        Mock(batch.dal.put, "put_action_to_batch", "async", "123456"),
        Mock(roots_domain, "notify_health_check", "async", None),
    ],
)
async def test_add_git_root() -> None:
    info = GraphQLResolveInfoFaker(
        user_email="adming@fluidattacks.com",
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_attribute, "has_service_white", GROUP_NAME],
            [require_attribute_internal, "is_under_review", GROUP_NAME],
        ],
    )
    result = await mutate(
        _parent=None,
        info=info,
        group_name=GROUP_NAME,
        branch="trunk",
        includes_health_check=True,
        url="https://github.com/fluidattacks/makes.git",
        credentials={"id": "cred1"},
        criticality=RootCriticality.MEDIUM,
        gitignore=[],
        nickname=None,
        use_egress=False,
        use_vpn=False,
        use_ztna=True,
    )

    loaders = get_new_context()
    root_id = result.root_id
    root = await loaders.root.load(RootRequest(GROUP_NAME, root_id))
    current_repositories = await loaders.organization_unreliable_outside_repositories_c.load(
        OrganizationIntegrationRepositoryRequest(organization_id="org1"),
    )
    action = next(
        (action for action in await get_actions() if action.entity == GROUP_NAME),
        None,
    )

    assert result.success is True
    assert isinstance(root, GitRoot)
    assert root.cloning.status.value == "QUEUED"
    assert root.cloning.reason == "Cloning queued..."
    assert root.state.branch == "trunk"
    assert root.state.criticality == RootCriticality.MEDIUM
    assert root.state.url == "https://github.com/fluidattacks/makes.git"
    assert root.cloning.status.value == "QUEUED"
    assert root.cloning.reason == "Cloning queued..."
    assert not current_repositories.edges
    assert action is not None
    assert action.subject == "adming@fluidattacks.com"
    assert action.action_name == Action.CLONE_ROOTS
    assert action.additional_info["git_root_ids"] == [root.id]
    assert action.dependent_actions == [
        DependentAction(
            action_name=Action.UPDATE_GROUP_LANGUAGES,
            additional_info={"root_ids": [root.id]},
            queue=IntegratesBatchQueue.SMALL,
        )
    ]


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            stakeholders=[StakeholderFaker(email="adming@fluidattacks.com", role="admin")],
            groups=[GroupFaker(name=GROUP_NAME, organization_id="org1")],
        ),
    ),
    others=[
        Mock(roots_domain, "report_activity", "sync", None),
        Mock(
            validations,
            "validate_and_get_redirected_url",
            "async",
            "https://gitlab.com/fluidattacks/universe.git",
        ),
        Mock(validations, "working_credentials", "async", None),
    ],
)
async def test_add_git_root_exclusion() -> None:
    info = GraphQLResolveInfoFaker(
        user_email="adming@fluidattacks.com",
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_attribute, "has_service_white", GROUP_NAME],
            [require_attribute_internal, "is_under_review", GROUP_NAME],
        ],
    )
    credentials = {"isPat": False, "token": "token", "name": "Credentials test", "type": "HTTPS"}
    result = await mutate(
        _parent=None,
        info=info,
        group_name=GROUP_NAME,
        branch="trunk",
        includes_health_check=False,
        url="https://gitlab.com/fluidattacks/universe.git",
        credentials=credentials,
        criticality=None,
        gitignore=["back/**/test.py", "bower_components/*", "!/node_modules/", "*.txt"],
        nickname=None,
        use_egress=False,
        use_vpn=False,
        use_ztna=False,
    )

    loaders = get_new_context()
    root_id = result.root_id
    root = await loaders.root.load(RootRequest(GROUP_NAME, root_id))
    org_credentials = await loaders.organization_credentials.load("ORG#org1")
    new_credentials = next(
        (
            credential
            for credential in org_credentials
            if credential.state.name == credentials["name"]
        ),
        None,
    )
    assert result.success is True
    assert isinstance(root, GitRoot)
    assert root.cloning.status.value == "QUEUED"
    assert root.cloning.reason == "Cloning queued..."
    assert new_credentials is not None
    assert new_credentials.state.owner == "adming@fluidattacks.com"
    assert new_credentials.state.name == credentials["name"]
    assert new_credentials.state.type == CredentialType[str(credentials["type"])]
    assert getattr(new_credentials.secret, "token", None) == credentials.get("token")
    assert getattr(new_credentials.state, "is_pat", False) == credentials.get("isPat", False)
    assert getattr(new_credentials.state, "azure_organization", None) == credentials.get(
        "azureOrganization"
    )


@parametrize(
    args=["group_name", "url", "branch"],
    cases=[
        # No the same root in the same group
        ["group1", "https://github.com/a/a", "main"],
        # No the same root in another group
        ["group2", "https://github.com/a/a", "main"],
        # No the same root with different branch in the same group
        ["group1", "https://github.com/a/a", "develop"],
        # No the same root in the same group (even inactive)
        ["group1", "https://github.com/n/n", "main"],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            groups=[
                GroupFaker(name="group1", organization_id="org1"),
                GroupFaker(name="group2", organization_id="org1"),
            ],
            stakeholders=[StakeholderFaker(email="group_manager@gmail.com")],
            group_access=[
                GroupAccessFaker(
                    email="group_manager@gmail.com",
                    group_name="group1",
                    state=GroupAccessStateFaker(has_access=True, role="group_manager"),
                ),
                GroupAccessFaker(
                    email="group_manager@gmail.com",
                    group_name="group2",
                    state=GroupAccessStateFaker(has_access=True, role="group_manager"),
                ),
            ],
            roots=[
                GitRootFaker(
                    id="root1",
                    group_name="group1",
                    created_by="group_manager@gmail.com",
                    state=GitRootStateFaker(
                        url="https://github.com/a/a",
                        branch="main",
                        status=RootStatus.ACTIVE,
                        nickname="root1",
                    ),
                ),
                GitRootFaker(
                    id="root2",
                    group_name="group1",
                    created_by="group_manager@gmail.com",
                    state=GitRootStateFaker(
                        url="https://github.com/n/n",
                        branch="main",
                        status=RootStatus.INACTIVE,
                        nickname="root2",
                    ),
                ),
            ],
        )
    )
)
async def test_root_uniqueness_in_organization(group_name: str, url: str, branch: str) -> None:
    info = GraphQLResolveInfoFaker(
        user_email="group_manager@gmail.com",
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_attribute, "has_service_white", group_name],
            [require_attribute_internal, "is_under_review", group_name],
        ],
    )

    with raises(RepeatedRoot):
        await mutate(
            _parent=None,
            info=info,
            includes_health_check=False,
            group_name=group_name,
            url=url,
            branch=branch,
        )
