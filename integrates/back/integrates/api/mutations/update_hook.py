from graphql import (
    GraphQLResolveInfo,
)

from integrates.custom_utils import (
    logs as logs_utils,
)
from integrates.db_model.hook.utils import (
    format_group_hook_payload,
)
from integrates.decorators import (
    concurrent_decorators,
    enforce_group_level_auth_async,
    require_is_not_under_review,
    require_login,
)
from integrates.groups.domain import (
    update_group_hook,
)
from integrates.groups.hook_notifier import (
    check_hook,
)
from integrates.sessions import (
    domain as sessions_domain,
)

from .payloads.types import (
    SimplePayload,
)
from .schema import (
    MUTATION,
)


@MUTATION.field("updateHook")
@concurrent_decorators(
    require_login,
    enforce_group_level_auth_async,
    require_is_not_under_review,
)
async def mutate(
    _parent: None,
    info: GraphQLResolveInfo,
    hook_id: str,
    group_name: str,
    hook: dict[str, str],
) -> SimplePayload:
    user_data = await sessions_domain.get_jwt_content(info.context)
    user_email = user_data["user_email"]

    await check_hook(event="Hook updated", group=group_name, hook=hook)
    await update_group_hook(
        loaders=info.context.loaders,
        hook_id=hook_id,
        group_name=group_name,
        user_email=user_email,
        hook=format_group_hook_payload(hook),
    )

    logs_utils.cloudwatch_log(
        info.context,
        "Updated Hook",
        extra={
            "group_name": group_name,
            "hook_id": hook_id,
            "log_type": "Security",
        },
    )

    return SimplePayload(success=True)
