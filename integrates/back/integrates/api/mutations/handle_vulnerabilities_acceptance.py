from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.api.types import (
    APP_EXCEPTIONS,
)
from integrates.custom_utils import (
    logs as logs_utils,
)
from integrates.custom_utils.findings import get_finding
from integrates.decorators import (
    concurrent_decorators,
    enforce_group_level_auth_async,
    require_asm,
    require_is_not_under_review,
    require_login,
)
from integrates.sessions import (
    domain as sessions_domain,
)
from integrates.unreliable_indicators.enums import (
    EntityDependency,
)
from integrates.unreliable_indicators.operations import (
    update_unreliable_indicators_by_deps,
)
from integrates.vulnerabilities.domain import (
    handle_vulnerabilities_acceptance,
)

from .payloads.types import (
    SimplePayload,
)
from .schema import (
    MUTATION,
)


@MUTATION.field("handleVulnerabilitiesAcceptance")
@concurrent_decorators(
    require_login,
    enforce_group_level_auth_async,
    require_is_not_under_review,
    require_asm,
)
async def mutate(
    _: None,
    info: GraphQLResolveInfo,
    finding_id: str,
    justification: str,
    accepted_vulnerabilities: list[str],
    rejected_vulnerabilities: list[str],
) -> SimplePayload:
    try:
        user_info = await sessions_domain.get_jwt_content(info.context)
        email = user_info["user_email"]
        finding = await get_finding(info.context.loaders, finding_id)
        await handle_vulnerabilities_acceptance(
            loaders=info.context.loaders,
            accepted_vulns=accepted_vulnerabilities,
            finding_id=finding_id,
            justification=justification,
            rejected_vulns=rejected_vulnerabilities,
            user_email=email,
        )
        await update_unreliable_indicators_by_deps(
            EntityDependency.handle_vulnerabilities_acceptance,
            finding_ids=[finding_id],
            vulnerability_ids=accepted_vulnerabilities + rejected_vulnerabilities,
        )
        logs_utils.cloudwatch_log(
            info.context,
            "Handled vulnerabilities acceptance in finding",
            extra={
                "finding_id": finding_id,
                "group_name": finding.group_name,
                "log_type": "Security",
            },
        )
    except APP_EXCEPTIONS:
        logs_utils.cloudwatch_log(
            info.context,
            "Attempted to handle vulnerabilities acceptance in finding",
            extra={
                "finding_id": finding_id,
                "group_name": finding.group_name,
                "log_type": "Security",
            },
        )
        raise

    return SimplePayload(success=True)
