from datetime import datetime

from integrates.api.mutations.update_root_cloning_status import mutate
from integrates.custom_exceptions import RootNotFound
from integrates.decorators import (
    enforce_group_level_auth_async,
    require_attribute_internal,
    require_is_not_under_review,
    require_login,
)
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    GitRootFaker,
    GraphQLResolveInfoFaker,
    GroupAccessFaker,
    GroupAccessStateFaker,
    GroupFaker,
    StakeholderFaker,
)
from integrates.testing.mocks import mocks
from integrates.testing.utils import parametrize, raises

ADMIN_EMAIL = "admin@gmail.com"
GROUP_NAME = "test-group"
RESOURCER_EMAIL = "resourcer@fluidattacks.com"
EXECUTIVE_EMAIL = "executive@fluidattacks.com"


@parametrize(
    args=["status", "message", "commit", "queue_machine"],
    cases=[
        ["OK", "Clone successful", "abc123", True],
        ["FAILED", "Clone failed", None, False],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            stakeholders=[
                StakeholderFaker(email=ADMIN_EMAIL),
            ],
            groups=[
                GroupFaker(name=GROUP_NAME),
            ],
            group_access=[
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email=ADMIN_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="admin"),
                ),
            ],
            roots=[
                GitRootFaker(
                    id="e2e1c881-83c4-4f85-b791-8f5a5757b51f",
                    group_name=GROUP_NAME,
                    organization_name="org1",
                ),
            ],
        )
    )
)
async def test_update_root_cloning_status(
    status: str,
    message: str,
    commit: str | None,
    queue_machine: bool,
) -> None:
    root_id = "e2e1c881-83c4-4f85-b791-8f5a5757b51f"
    info = GraphQLResolveInfoFaker(
        user_email=ADMIN_EMAIL,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_is_not_under_review],
            [require_attribute_internal, "is_under_review", GROUP_NAME],
        ],
    )
    result = await mutate(
        _=None,
        info=info,
        group_name=GROUP_NAME,
        root_id=root_id,
        status=status,
        message=message,
        commit=commit,
        queue_machine=queue_machine,
        commit_date=datetime.now(),
    )
    assert result.success is True


@parametrize(
    args=["email"],
    cases=[
        [RESOURCER_EMAIL],
        [EXECUTIVE_EMAIL],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            stakeholders=[
                StakeholderFaker(email=RESOURCER_EMAIL),
                StakeholderFaker(email=EXECUTIVE_EMAIL),
            ],
            group_access=[
                GroupAccessFaker(
                    email=RESOURCER_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="resourcer"),
                ),
                GroupAccessFaker(
                    email=EXECUTIVE_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="executive"),
                ),
            ],
        )
    )
)
async def test_update_root_cloning_status_unauthorized(email: str) -> None:
    info = GraphQLResolveInfoFaker(
        user_email=email,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_is_not_under_review],
            [require_attribute_internal, "is_under_review", GROUP_NAME],
        ],
    )
    with raises(Exception, match="Access denied"):
        await mutate(
            _=None,
            info=info,
            group_name=GROUP_NAME,
            root_id="e2e1c881-83c4-4f85-b791-8f5a5757b51f",
            status="OK",
            message="Test message",
            commit="abc123",
            queue_machine=True,
        )


@parametrize(
    args=["root_id", "status"],
    cases=[
        ["e2e1c881-83c4-4f85-b791-8f5a5757b51f", "INVALID_STATUS"],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            stakeholders=[StakeholderFaker(email=ADMIN_EMAIL)],
            groups=[
                GroupFaker(name=GROUP_NAME),
            ],
            group_access=[
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email=ADMIN_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="admin"),
                ),
            ],
            roots=[
                GitRootFaker(
                    id="e2e1c881-83c4-4f85-b791-8f5a5757b51f",
                    group_name=GROUP_NAME,
                    organization_name="org1",
                ),
            ],
        )
    )
)
async def test_update_root_cloning_status_invalid_params(
    root_id: str,
    status: str,
) -> None:
    info = GraphQLResolveInfoFaker(
        user_email=ADMIN_EMAIL,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_is_not_under_review],
            [require_attribute_internal, "is_under_review", GROUP_NAME],
        ],
    )
    with raises(Exception, match="'INVALID_STATUS' is not a valid RootCloningStatus"):
        await mutate(
            _=None,
            info=info,
            group_name=GROUP_NAME,
            root_id=root_id,
            status=status,
            message="Test message",
            commit="abc123",
            queue_machine=True,
        )


@parametrize(
    args=["root_id", "status"],
    cases=[
        ["not_found_root_id", "OK"],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            stakeholders=[StakeholderFaker(email=ADMIN_EMAIL)],
            groups=[
                GroupFaker(name=GROUP_NAME),
            ],
            group_access=[
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email=ADMIN_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="admin"),
                ),
            ],
        )
    )
)
async def test_update_root_cloning_status_not_found(
    root_id: str,
    status: str,
) -> None:
    info = GraphQLResolveInfoFaker(
        user_email=ADMIN_EMAIL,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_is_not_under_review],
            [require_attribute_internal, "is_under_review", GROUP_NAME],
        ],
    )
    with raises(RootNotFound):
        await mutate(
            _=None,
            info=info,
            group_name=GROUP_NAME,
            root_id=root_id,
            status=status,
            message="Test message",
            commit="abc123",
            queue_machine=True,
        )
