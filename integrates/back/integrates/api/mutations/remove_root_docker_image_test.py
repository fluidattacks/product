from integrates.api.mutations.remove_root_docker_image import mutate
from integrates.dataloaders import get_new_context
from integrates.db_model.roots.types import RootDockerImageRequest
from integrates.db_model.toe_packages.get import _get_package
from integrates.db_model.toe_packages.types import ToePackageRequest
from integrates.decorators import (
    enforce_group_level_auth_async,
    require_attribute,
    require_attribute_internal,
    require_login,
)
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    GitRootFaker,
    GraphQLResolveInfoFaker,
    GroupFaker,
    OrganizationFaker,
    RootDockerImageFaker,
    StakeholderFaker,
    ToePackageCoordinatesFaker,
    ToePackageFaker,
)
from integrates.testing.mocks import mocks
from integrates.testing.utils import parametrize, raises

ADMIN_EMAIL = "admin@gmail.com"
ROOT_ID = "63298a73-9dff-46cf-b42d-9b2f01a56690"
GROUP_NAME = "grouptest"
URI = "fluidattacks/forces:latest"


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            stakeholders=[StakeholderFaker(email=ADMIN_EMAIL, role="admin")],
            groups=[GroupFaker(name=GROUP_NAME, organization_id="org1")],
            roots=[
                GitRootFaker(
                    group_name=GROUP_NAME,
                    id=ROOT_ID,
                ),
            ],
            docker_images=[
                RootDockerImageFaker(
                    root_id=ROOT_ID, group_name=GROUP_NAME, created_by=ADMIN_EMAIL, uri=URI
                )
            ],
            toe_packages=[
                ToePackageFaker(
                    group_name=GROUP_NAME,
                    root_id=ROOT_ID,
                    name="test-package",
                    version="1.5.0",
                    outdated=False,
                    licenses=["Apache-1.0"],
                    locations=[
                        ToePackageCoordinatesFaker(
                            path="var/lib/etc",
                            layer="sha256:d4fc045c9e3a848011de66f34b81f0"
                            "52d4f2c15a17bb196d637e526349601820",
                            image_ref=URI,
                        )
                    ],
                ),
            ],
        ),
    )
)
async def test_remove_docker_image() -> None:
    loaders = get_new_context()
    info = GraphQLResolveInfoFaker(
        user_email=ADMIN_EMAIL,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_attribute, "has_service_white", GROUP_NAME],
            [require_attribute_internal, "is_under_review", GROUP_NAME],
        ],
    )

    toe_package = (
        await _get_package(
            [
                ToePackageRequest(
                    group_name=GROUP_NAME,
                    name="test-package",
                    version="1.5.0",
                    root_id=ROOT_ID,
                ),
            ],
        )
    )[0]
    assert toe_package
    assert toe_package.be_present is True

    result = await mutate(
        _parent=None,
        info=info,
        user_email=ADMIN_EMAIL,
        group_name=GROUP_NAME,
        uri=URI,
        root_id=ROOT_ID,
    )
    assert result.success is True

    image = await loaders.docker_image.load(RootDockerImageRequest(ROOT_ID, GROUP_NAME, URI))
    assert image
    assert image.state.status == "DELETED"
    toe_package = (
        await _get_package(
            [
                ToePackageRequest(
                    group_name=GROUP_NAME,
                    name="test-package",
                    version="1.5.0",
                    root_id=ROOT_ID,
                ),
            ],
        )
    )[0]
    assert toe_package
    assert toe_package.be_present is False


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            stakeholders=[StakeholderFaker(email=ADMIN_EMAIL, role="admin")],
            groups=[GroupFaker(name=GROUP_NAME, organization_id="org1")],
            roots=[
                GitRootFaker(
                    group_name=GROUP_NAME,
                    id=ROOT_ID,
                ),
            ],
            docker_images=[
                RootDockerImageFaker(root_id=ROOT_ID, group_name=GROUP_NAME, created_by=ADMIN_EMAIL)
            ],
        ),
    )
)
async def test_remove_docker_image_not_found() -> None:
    info = GraphQLResolveInfoFaker(
        user_email=ADMIN_EMAIL,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_attribute, "has_service_white", GROUP_NAME],
            [require_attribute_internal, "is_under_review", GROUP_NAME],
        ],
    )

    with raises(Exception, match="Exception - Root image not found"):
        await mutate(
            _parent=None,
            info=info,
            user_email=ADMIN_EMAIL,
            group_name=GROUP_NAME,
            uri=URI,
            root_id=ROOT_ID,
        )


@parametrize(
    args=["email"],
    cases=[
        ["hacker@gmail.com"],
        ["reattacker@gmail.com"],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            stakeholders=[
                StakeholderFaker(email="reattacker@gmail.com", role="reattacker"),
                StakeholderFaker(email="hacker@gmail.com", role="hacker"),
            ],
            groups=[GroupFaker(name=GROUP_NAME, organization_id="org1")],
            roots=[
                GitRootFaker(
                    group_name=GROUP_NAME,
                    id=ROOT_ID,
                ),
            ],
            docker_images=[
                RootDockerImageFaker(root_id=ROOT_ID, group_name=GROUP_NAME, created_by=ADMIN_EMAIL)
            ],
        ),
    )
)
async def test_remove_docker_image_access_denied(email: str) -> None:
    info = GraphQLResolveInfoFaker(
        user_email=email,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_attribute, "has_service_white", GROUP_NAME],
            [require_attribute_internal, "is_under_review", GROUP_NAME],
        ],
    )

    with raises(Exception, match="Access denied"):
        await mutate(
            _parent=None,
            info=info,
            user_email=email,
            group_name=GROUP_NAME,
            uri=URI,
            root_id=ROOT_ID,
        )
