from typing import (
    Any,
)

from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.api.types import (
    APP_EXCEPTIONS,
)
from integrates.custom_utils import (
    logs as logs_utils,
)
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.decorators import (
    concurrent_decorators,
    enforce_group_level_auth_async,
    require_is_not_under_review,
    require_login,
)
from integrates.roots import (
    update as roots_update,
)
from integrates.roots.types import (
    RootEnvironmentUrlAttributesToUpdate,
)
from integrates.sessions import (
    domain as sessions_domain,
)

from .payloads.types import (
    SimplePayload,
)
from .schema import (
    MUTATION,
)


@MUTATION.field("updateGitRootEnvironmentUrl")
@concurrent_decorators(
    require_login,
    enforce_group_level_auth_async,
    require_is_not_under_review,
)
async def mutate(
    _: None,
    info: GraphQLResolveInfo,
    group_name: str,
    root_id: str,
    url_id: str,
    **kwargs: Any,
) -> SimplePayload:
    loaders: Dataloaders = info.context.loaders
    try:
        user_info = await sessions_domain.get_jwt_content(info.context)
        user_email = user_info["user_email"]
        await roots_update.update_root_environment_url(
            attributes=RootEnvironmentUrlAttributesToUpdate(include=kwargs.get("include")),
            loaders=loaders,
            group_name=group_name,
            modified_by=user_email,
            root_id=root_id,
            url_id=url_id,
        )
        logs_utils.cloudwatch_log(
            info.context,
            "Updated an environment URL in the root",
            extra={
                "group_name": group_name,
                "root_id": root_id,
                "log_type": "Security",
            },
        )
    except APP_EXCEPTIONS:
        logs_utils.cloudwatch_log(
            info.context,
            "Attempted to update an environment URL in the root",
            extra={
                "group_name": group_name,
                "root_id": root_id,
                "log_type": "Security",
            },
        )
        raise
    return SimplePayload(success=True)
