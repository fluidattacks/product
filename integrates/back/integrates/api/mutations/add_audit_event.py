from typing import TypedDict, Unpack

from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.audit import AuditEvent, add_audit_event
from integrates.decorators import (
    require_login,
)
from integrates.sessions import (
    domain as sessions_domain,
)

from .payloads.types import (
    SimplePayload,
)
from .schema import (
    MUTATION,
)


class AddAuditEventInput(TypedDict):
    object: str
    object_id: str


@MUTATION.field("addAuditEvent")
@require_login
async def mutate(
    _parent: None,
    info: GraphQLResolveInfo,
    **kwargs: Unpack[AddAuditEventInput],
) -> SimplePayload:
    user_info = await sessions_domain.get_jwt_content(info.context)
    user_email = user_info["user_email"]
    add_audit_event(
        AuditEvent(
            action="READ",
            author=user_email,
            metadata={"referer": info.context.headers.get("referer", "unknown")},
            object=kwargs["object"],
            object_id=kwargs["object_id"],
        )
    )

    return SimplePayload(success=True)
