from decimal import (
    Decimal,
    InvalidOperation,
)
from typing import cast

from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.class_types.types import (
    Item,
)
from integrates.custom_exceptions import (
    InvalidCVSSField,
)
from integrates.db_model.items import NotificationsPreferencesItem
from integrates.db_model.stakeholders.types import (
    Stakeholder,
)
from integrates.db_model.stakeholders.utils import (
    format_notifications_preferences,
)
from integrates.sessions import (
    domain as sessions_domain,
)
from integrates.stakeholders.domain import (
    update_notification_preferences,
)

from .payloads.types import (
    SimplePayload,
)
from .schema import (
    MUTATION,
)


@MUTATION.field("updateNotificationsPreferences")
async def mutate(
    _: None,
    info: GraphQLResolveInfo,
    notifications_preferences: Item,
) -> SimplePayload:
    loaders = info.context.loaders
    user_info = await sessions_domain.get_jwt_content(info.context)
    user_email: str = user_info["user_email"]

    if notifications_preferences.get("parameters", False):
        try:
            min_severity = Decimal(str(notifications_preferences["parameters"]["min_severity"]))
            notifications_preferences.update({"parameters": {"min_severity": min_severity}})
        except InvalidOperation as ex:
            raise InvalidCVSSField() from ex
    else:
        stakeholder: Stakeholder = await loaders.stakeholder.load(user_email)
        cvss = stakeholder.state.notifications_preferences.parameters.min_severity
        notifications_preferences.update({"parameters": {"min_severity": Decimal(cvss)}})

    await update_notification_preferences(
        loaders=loaders,
        email=user_email,
        preferences=format_notifications_preferences(
            cast(NotificationsPreferencesItem, notifications_preferences)
        ),
    )

    return SimplePayload(success=True)
