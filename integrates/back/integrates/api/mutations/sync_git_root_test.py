from contextlib import suppress

from integrates.api.mutations.sync_git_root import mutate
from integrates.custom_exceptions import InvalidGitRoot
from integrates.decorators import (
    enforce_group_level_auth_async,
    require_attribute,
    require_attribute_internal,
    require_login,
)
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    GraphQLResolveInfoFaker,
    GroupFaker,
    OrganizationFaker,
    StakeholderFaker,
    UrlRootFaker,
)
from integrates.testing.mocks import mocks

GROUP_NAME = "group1"


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            stakeholders=[StakeholderFaker(email="admin@gmail.com", role="admin")],
            groups=[GroupFaker(name=GROUP_NAME, organization_id="org1")],
            roots=[UrlRootFaker(group_name=GROUP_NAME, root_id="root1")],
        )
    )
)
async def test_sync_git_root_invalid() -> None:
    info = GraphQLResolveInfoFaker(
        user_email="admin@gmail.com",
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_attribute, "has_service_white", GROUP_NAME],
            [require_attribute_internal, "is_under_review", GROUP_NAME],
        ],
    )
    with suppress(InvalidGitRoot):
        await mutate(_parent=None, info=info, root_id="root1", group_name=GROUP_NAME)
