from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.custom_utils import (
    logs as logs_utils,
)
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.events.enums import (
    EventEvidenceId,
)
from integrates.decorators import (
    concurrent_decorators,
    enforce_group_level_auth_async,
    require_asm,
    require_login,
)
from integrates.events import (
    domain as events_domain,
)

from .payloads.types import (
    SimplePayload,
)
from .schema import (
    MUTATION,
)


@MUTATION.field("removeEventEvidence")
@concurrent_decorators(
    require_login,
    enforce_group_level_auth_async,
    require_asm,
)
async def mutate(
    _parent: None,
    info: GraphQLResolveInfo,
    event_id: str,
    evidence_type: str,
    group_name: str,
    **_kwargs: str,
) -> SimplePayload:
    loaders: Dataloaders = info.context.loaders
    evidence_id = EventEvidenceId[evidence_type]
    await events_domain.remove_evidence(loaders, evidence_id, event_id, group_name)
    logs_utils.cloudwatch_log(
        info.context,
        "Removed evidence in the event",
        extra={
            "event_id": event_id,
            "log_type": "Security",
        },
    )

    return SimplePayload(success=True)
