from integrates.api.mutations.update_git_root import mutate
from integrates.custom_exceptions import RepeatedRoot, RequiredCredentials
from integrates.dataloaders import get_new_context
from integrates.db_model.credentials.types import HttpsSecret
from integrates.db_model.enums import CredentialType
from integrates.db_model.roots.enums import RootCriticality
from integrates.db_model.roots.types import GitRoot, RootRequest
from integrates.db_model.vulnerabilities.enums import (
    VulnerabilityStateReason,
    VulnerabilityStateStatus,
)
from integrates.decorators import (
    enforce_group_level_auth_async,
    require_attribute,
    require_attribute_internal,
    require_login,
)
from integrates.roots import domain, utils, validations
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    CredentialsFaker,
    GitRootFaker,
    GitRootStateFaker,
    GraphQLResolveInfoFaker,
    GroupAccessFaker,
    GroupAccessStateFaker,
    GroupFaker,
    OrganizationFaker,
    StakeholderFaker,
    VulnerabilityFaker,
    VulnerabilityStateFaker,
)
from integrates.testing.mocks import Mock, mocks
from integrates.testing.utils import parametrize, raises

GROUP_NAME = "group1"


@parametrize(
    args=["email"],
    cases=[
        ["hacker@gmail.com"],
        ["reattacker@gmail.com"],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            stakeholders=[
                StakeholderFaker(email="hacker@gmail.com", role="hacker"),
                StakeholderFaker(email="reattacker@gmail.com", role="reattacker"),
            ],
            groups=[GroupFaker(name=GROUP_NAME, organization_id="org1")],
            group_access=[
                GroupAccessFaker(
                    email="hacker@gmail.com",
                    state=GroupAccessStateFaker(has_access=True, role="hacker"),
                ),
                GroupAccessFaker(
                    email="reattacker@gmail.com",
                    state=GroupAccessStateFaker(has_access=True, role="reattacker"),
                ),
            ],
            roots=[
                GitRootFaker(
                    group_name=GROUP_NAME, id="root1", state=GitRootStateFaker(nickname="back")
                )
            ],
        )
    )
)
async def test_update_git_root_access_denied(email: str) -> None:
    info = GraphQLResolveInfoFaker(
        user_email=email,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_attribute, "has_service_white", GROUP_NAME],
            [require_attribute_internal, "is_under_review", GROUP_NAME],
        ],
    )

    with raises(Exception, match="Access denied"):
        await mutate(
            _parent=None,
            info=info,
            id="root1",
            group_name=GROUP_NAME,
            branch="trunk",
            includes_health_check=False,
            url="https://gitlab.com/fluidattacks/universe",
            credentials=None,
            criticality=None,
            gitignore=None,
            nickname=None,
            use_egress=False,
            use_vpn=False,
            use_ztna=False,
        )


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            groups=[GroupFaker()],
            roots=[
                GitRootFaker(
                    state=GitRootStateFaker(
                        url="https://test.com/repo.git",
                        branch="main",
                        nickname="original",
                    ),
                ),
                GitRootFaker(
                    id="root1",
                    state=GitRootStateFaker(
                        url="https://test.com/repo2.git",
                        branch="main",
                        nickname="original",
                    ),
                ),
            ],
            stakeholders=[StakeholderFaker(email="group_manager@gmail.com")],
            group_access=[
                GroupAccessFaker(
                    email="group_manager@gmail.com",
                    state=GroupAccessStateFaker(has_access=True, role="group_manager"),
                ),
            ],
        )
    )
)
async def test_existing_url_and_branch_fail() -> None:
    # Arrange
    root_id = "root1"
    url = "https://test.com/repo.git"
    branch = "main"
    group = "test-group"
    user = "group_manager@gmail.com"
    nickname = "new-root"
    info = GraphQLResolveInfoFaker(
        user_email=user,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_attribute, "is_continuous", group],
            [require_attribute_internal, "is_under_review", group],
        ],
    )

    # Act
    with raises(RepeatedRoot):
        await mutate(
            _parent=None,
            info=info,
            id=root_id,
            nickname=nickname,
            group_name=group,
            url=url,
            branch=branch,
        )


@parametrize(
    args=["root_id", "email", "url", "gitignore", "expected_safe_vulns"],
    cases=[
        [
            "3f316c6d-5e31-4f95-91a2-48b829610bae",
            "admin@gmail.com",
            "https://gitlab.com/fluidattacks/nickname5.git",
            [],
            0,
        ],
        [
            "3f316c6d-5e31-4f95-91a2-48b829610bae",
            "admin@gmail.com",
            "https://gitlab.com/fluidattacks/nickname5.git",
            ["test/data/*"],
            2,
        ],
        [
            "3f316c6d-5e31-4f95-91a2-48b829610bae",
            "admin@gmail.com",
            "https://gitlab.com/fluidattacks/nickname5.git",
            ["path/*"],
            1,
        ],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            stakeholders=[StakeholderFaker(email="admin@gmail.com", role="admin")],
            groups=[GroupFaker(name=GROUP_NAME, organization_id="org1")],
            credentials=[
                CredentialsFaker(
                    credential_id="cred1",
                    organization_id="ORG#org1",
                    secret=HttpsSecret(user="user@test.test", password=""),
                )
            ],
            roots=[
                GitRootFaker(
                    id="3f316c6d-5e31-4f95-91a2-48b829610bae",
                    group_name=GROUP_NAME,
                    state=GitRootStateFaker(
                        branch="main",
                        url="https://gitlab.com/fluidattacks/nickname5.git",
                        nickname="nickname5",
                        credential_id="cred1",
                        includes_health_check=True,
                    ),
                ),
            ],
            vulnerabilities=[
                VulnerabilityFaker(
                    id="vuln1",
                    group_name=GROUP_NAME,
                    root_id="3f316c6d-5e31-4f95-91a2-48b829610bae",
                    state=VulnerabilityStateFaker(where="test/data/f060/file.cs"),
                ),
                VulnerabilityFaker(
                    id="vuln2",
                    group_name=GROUP_NAME,
                    root_id="3f316c6d-5e31-4f95-91a2-48b829610bae",
                    state=VulnerabilityStateFaker(where="path/file.py"),
                ),
                VulnerabilityFaker(
                    id="vuln3",
                    group_name=GROUP_NAME,
                    root_id="3f316c6d-5e31-4f95-91a2-48b829610bae",
                    state=VulnerabilityStateFaker(where="test/data/f061/file.cs"),
                ),
            ],
        )
    ),
    others=[
        Mock(
            validations,
            "validate_and_get_redirected_url",
            "async",
            "https://gitlab.com/fluidattacks/universe.git",
        ),
        Mock(validations, "working_credentials", "async", None),
        Mock(utils, "send_mail_updated_root", "async", None),
        Mock(domain, "clone_root_and_update_group_languages", "async", None),
    ],
)
async def test_update_git_root_exclusions(
    root_id: str, email: str, url: str, gitignore: list[str], expected_safe_vulns: int
) -> None:
    info = GraphQLResolveInfoFaker(
        user_email=email,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_attribute, "has_service_white", GROUP_NAME],
            [require_attribute, "is_continuous", GROUP_NAME],
            [require_attribute_internal, "is_under_review", GROUP_NAME],
        ],
    )
    result = await mutate(
        _parent=None,
        info=info,
        id=root_id,
        group_name=GROUP_NAME,
        branch="trunk",
        includes_health_check=False,
        url=url,
        credentials={"id": "cred1"},
        criticality=RootCriticality.MEDIUM,
        gitignore=gitignore,
        nickname=None,
        use_egress=False,
        use_vpn=False,
        use_ztna=False,
    )

    loaders = get_new_context()
    root = await loaders.root.load(RootRequest(GROUP_NAME, root_id))
    exclusion_root_vulns = [
        vuln
        for vuln in await loaders.root_vulnerabilities.load(root_id)
        if vuln.state.status == VulnerabilityStateStatus.SAFE
        and vuln.state.reasons == [VulnerabilityStateReason.EXCLUSION]
    ]
    assert result.success
    assert root
    assert isinstance(root, GitRoot)
    assert root.state.gitignore == gitignore
    assert len(exclusion_root_vulns) == expected_safe_vulns


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            stakeholders=[StakeholderFaker(email="admin@gmail.com", role="admin")],
            groups=[GroupFaker(name=GROUP_NAME, organization_id="org1")],
            credentials=[
                CredentialsFaker(
                    credential_id="cred1",
                    organization_id="ORG#org1",
                    secret=HttpsSecret(user="user@test.test", password=""),
                )
            ],
            roots=[
                GitRootFaker(
                    id="3f316c6d-5e31-4f95-91a2-48b829610bae",
                    group_name=GROUP_NAME,
                    state=GitRootStateFaker(
                        branch="main",
                        url="https://gitlab.com/fluidattacks/nickname5.git",
                        nickname="nickname5",
                        credential_id="cred1",
                        includes_health_check=True,
                    ),
                ),
            ],
        )
    ),
)
async def test_update_git_root_remove_existing_cred_fail() -> None:
    info = GraphQLResolveInfoFaker(
        user_email="admin@gmail.com",
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_attribute, "has_service_white", GROUP_NAME],
            [require_attribute, "is_continuous", GROUP_NAME],
            [require_attribute_internal, "is_under_review", GROUP_NAME],
        ],
    )
    with raises(RequiredCredentials):
        await mutate(
            _parent=None,
            info=info,
            id="3f316c6d-5e31-4f95-91a2-48b829610bae",
            group_name=GROUP_NAME,
            branch="trunk",
            includes_health_check=False,
            url="https://gitlab.com/fluidattacks/nickname5.git",
            credentials=None,
        )


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            stakeholders=[StakeholderFaker(email="admin@gmail.com", role="admin")],
            groups=[GroupFaker(name=GROUP_NAME, organization_id="org1")],
            roots=[
                GitRootFaker(
                    id="root1",
                    group_name=GROUP_NAME,
                    state=GitRootStateFaker(
                        branch="main",
                        url="https://gitlab.com/fluidattacks/nickname5.git",
                        nickname="nickname5",
                        includes_health_check=False,
                    ),
                ),
            ],
            vulnerabilities=[
                VulnerabilityFaker(
                    id="vuln1",
                    group_name=GROUP_NAME,
                    root_id="root1",
                    state=VulnerabilityStateFaker(where="test/data/f060/file.cs"),
                ),
                VulnerabilityFaker(
                    id="vuln2",
                    group_name=GROUP_NAME,
                    root_id="root1",
                    state=VulnerabilityStateFaker(where="test/data/f061/file.cs"),
                ),
            ],
        )
    ),
    others=[
        Mock(
            validations,
            "validate_and_get_redirected_url",
            "async",
            "https://gitlab.com/fluidattacks/nickname6.git",
        ),
        Mock(validations, "working_credentials", "async", None),
        Mock(utils, "send_mail_updated_root", "async", None),
        Mock(domain, "clone_root_and_update_group_languages", "async", None),
        Mock(domain, "notify_health_check", "async", None),
    ],
)
async def test_update_git_root() -> None:
    info = GraphQLResolveInfoFaker(
        user_email="admin@gmail.com",
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_attribute, "has_service_white", GROUP_NAME],
            [require_attribute, "is_continuous", GROUP_NAME],
            [require_attribute_internal, "is_under_review", GROUP_NAME],
        ],
    )
    result = await mutate(
        _parent=None,
        info=info,
        id="root1",
        group_name=GROUP_NAME,
        branch="trunk",
        includes_health_check=True,
        url="https://gitlab.com/fluidattacks/nickname6.git",
        credentials={"isPat": False, "token": "token", "name": "Credentials test", "type": "HTTPS"},
        criticality=RootCriticality.HIGH,
        gitignore=["path/*"],
        use_egress=False,
        use_vpn=False,
        use_ztna=False,
    )

    loaders = get_new_context()
    org_credentials = await loaders.organization_credentials.load("ORG#org1")
    new_credentials = next(
        (
            credential
            for credential in org_credentials
            if credential.state.name == "Credentials test"
        ),
        None,
    )
    root = await loaders.root.load(RootRequest(GROUP_NAME, "root1"))
    root_vulns = [
        vuln
        for vuln in await loaders.root_vulnerabilities.load("root1")
        if vuln.state.status == VulnerabilityStateStatus.VULNERABLE
    ]
    assert result.success
    assert root
    assert isinstance(root, GitRoot)
    assert root.state.criticality == RootCriticality.HIGH
    assert root.state.gitignore == ["path/*"]
    assert root.state.includes_health_check
    assert root.state.url == "https://gitlab.com/fluidattacks/nickname6.git"
    assert root.state.nickname == "nickname6"
    assert root.state.branch == "trunk"
    assert root_vulns[0].unreliable_indicators.unreliable_priority == 75.0
    assert new_credentials is not None
    assert new_credentials.state.owner == "admin@gmail.com"
    assert new_credentials.state.type == CredentialType.HTTPS
    assert getattr(new_credentials.secret, "token", None) == "token"
    assert not getattr(new_credentials.state, "is_pat", False)
    assert getattr(new_credentials.state, "azure_organization", None) is None
