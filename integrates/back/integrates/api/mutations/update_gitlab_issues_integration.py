from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.api.mutations.payloads.types import (
    SimplePayload,
)
from integrates.api.mutations.schema import (
    MUTATION,
)
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.decorators import (
    concurrent_decorators,
    enforce_group_level_auth_async,
    require_is_not_under_review,
)
from integrates.groups import (
    gitlab_issues_integration,
)


@MUTATION.field("updateGitLabIssuesIntegration")
@concurrent_decorators(enforce_group_level_auth_async, require_is_not_under_review)
async def mutate(
    _parent: None,
    info: GraphQLResolveInfo,
    *,
    gitlab_project_name: str,
    group_name: str,
    assignee_ids: list[int] | None = None,
    issue_automation_enabled: bool = True,
    labels: list[str] | None = None,
) -> SimplePayload:
    loaders: Dataloaders = info.context.loaders
    await gitlab_issues_integration.update_settings(
        assignee_ids=assignee_ids or [],
        gitlab_project_name=gitlab_project_name,
        group_name=group_name,
        issue_automation_enabled=issue_automation_enabled,
        labels=labels or [],
        loaders=loaders,
    )
    return SimplePayload(success=True)
