from aioextensions import (
    collect,
)
from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.api.types import (
    APP_EXCEPTIONS,
)
from integrates.custom_utils import (
    logs as logs_utils,
)
from integrates.custom_utils.findings import get_finding
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.hook.enums import (
    HookEvent,
)
from integrates.decorators import (
    concurrent_decorators,
    enforce_group_level_auth_async,
    require_login,
)
from integrates.groups import (
    hook_notifier,
)
from integrates.vulnerabilities import (
    domain as vulns_domain,
)

from .payloads.types import (
    SimplePayload,
)
from .schema import (
    MUTATION,
)


@MUTATION.field("updateVulnerabilitiesSeverity")
@concurrent_decorators(require_login, enforce_group_level_auth_async)
async def mutate(
    _: None,
    info: GraphQLResolveInfo,
    cvss_vector: str,
    finding_id: str,
    vulnerability_ids: list[str],
    cvss_4_vector: str = "",
) -> SimplePayload:
    try:
        loaders: Dataloaders = info.context.loaders
        vulnerability_ids_set = set(vulnerability_ids)
        vulnerabilities = await vulns_domain.get_by_finding_and_vuln_ids(
            loaders,
            finding_id,
            vulnerability_ids_set,
        )
        await collect(
            [
                vulns_domain.update_severity_score(
                    loaders=loaders,
                    vulnerability_id=vulnerability.id,
                    cvss_vector=cvss_vector,
                    cvss4_vector=cvss_4_vector,
                )
                for vulnerability in vulnerabilities
            ],
            workers=32,
        )

        finding = await get_finding(loaders, finding_id)
        await hook_notifier.process_hook_event(
            loaders=loaders,
            group_name=finding.group_name,
            info={
                "findingId": finding_id,
                "vulns_ids": list(vulnerability_ids_set),
            },
            event=HookEvent.VULNERABILITY_SEVERITY_CHANGED,
        )

        logs_utils.cloudwatch_log(
            info.context,
            "Updated severity in the vulnerabilities",
            extra={
                "finding_id": finding_id,
                "vulns_ids": vulnerability_ids_set,
                "log_type": "Security",
            },
        )
    except APP_EXCEPTIONS:
        logs_utils.cloudwatch_log(
            info.context,
            "Attempted to update severity in the vulnerabilities",
            extra={
                "finding_id": finding_id,
                "vulns_ids": vulnerability_ids_set,
                "log_type": "Security",
            },
        )
        raise

    return SimplePayload(success=True)
