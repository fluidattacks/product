from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.custom_utils import (
    logs as logs_utils,
)
from integrates.decorators import (
    concurrent_decorators,
    enforce_organization_level_auth_async,
    require_login,
)
from integrates.mailmap import (
    move_mailmap_entry_with_subentries,
)

from .payloads.types import (
    SimplePayload,
)
from .schema import (
    MUTATION,
)


@MUTATION.field("moveMailmapEntryWithSubentries")
@concurrent_decorators(
    require_login,
    enforce_organization_level_auth_async,
)
async def mutate(
    _parent: None,
    info: GraphQLResolveInfo,
    entry_email: str,
    organization_id: str,
    new_organization_id: str,
) -> SimplePayload:
    await move_mailmap_entry_with_subentries(
        entry_email=entry_email,
        organization_id=organization_id,
        new_organization_id=new_organization_id,
    )

    logs_utils.cloudwatch_log(
        info.context,
        "Moved mailmap author with aliases",
        extra={
            "entry_email": entry_email,
            "organization_id": organization_id,
            "new_organization_id": new_organization_id,
        },
    )

    return SimplePayload(success=True)
