from datetime import datetime

from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.db_model.notifications.types import (
    ReportStatus,
)
from integrates.notifications.reports import update_report_notification
from integrates.sessions import (
    domain as sessions_domain,
)

from .payloads.types import (
    SimplePayload,
)
from .schema import (
    MUTATION,
)


@MUTATION.field("updateNotifications")
async def mutate(
    _: None,
    info: GraphQLResolveInfo,
    notification_id: str,
    s_3_file_path: str | None,
    size: int | None,
) -> SimplePayload:
    user_info = await sessions_domain.get_jwt_content(info.context)
    user_email: str = user_info["user_email"]
    await update_report_notification(
        expiration_time=(datetime.now()),
        notification_id=notification_id,
        status=ReportStatus.DELETED,
        user_email=user_email,
        s3_file_path=s_3_file_path,
        size=size,
    )

    return SimplePayload(success=True)
