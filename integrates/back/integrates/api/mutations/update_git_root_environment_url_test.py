from integrates.api.mutations.update_git_root_environment_url import mutate
from integrates.custom_utils.roots import get_root_vulnerabilities
from integrates.dataloaders import Dataloaders, get_new_context
from integrates.db_model.enums import Source
from integrates.db_model.roots.types import RootEnvironmentUrlsRequest
from integrates.db_model.toe_inputs.types import RootToeInputsRequest
from integrates.db_model.vulnerabilities.enums import VulnerabilityStateStatus, VulnerabilityType
from integrates.decorators import (
    enforce_group_level_auth_async,
    require_attribute_internal,
    require_login,
)
from integrates.roots.domain import format_environment_id
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    FindingFaker,
    GitRootFaker,
    GraphQLResolveInfoFaker,
    GroupAccessFaker,
    GroupAccessStateFaker,
    GroupFaker,
    OrganizationAccessFaker,
    OrganizationAccessStateFaker,
    OrganizationFaker,
    RootEnvironmentUrlFaker,
    RootEnvironmentUrlStateFaker,
    StakeholderFaker,
    ToeInputFaker,
    VulnerabilityFaker,
    VulnerabilityStateFaker,
)
from integrates.testing.mocks import mocks

ORG_ID = "ORG#6b110f25-cd46-e94c-2bad-b9e182cf91a8"


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id=ORG_ID)],
            groups=[
                GroupFaker(
                    name="testgroup",
                    organization_id=ORG_ID,
                ),
            ],
            roots=[
                GitRootFaker(
                    id="root1",
                    group_name="testgroup",
                ),
            ],
            root_environment_urls=[
                RootEnvironmentUrlFaker(
                    id=format_environment_id("https://test.com/"),
                    url="https://test.com/",
                    group_name="testgroup",
                    root_id="root1",
                    state=RootEnvironmentUrlStateFaker(modified_by="hacker@fluidattacks.com"),
                ),
            ],
            toe_inputs=[
                ToeInputFaker(
                    group_name="testgroup",
                    root_id="root1",
                    component="https://test.com/",
                    entry_point="user",
                ),
            ],
            findings=[
                FindingFaker(group_name="testgroup", id="find1"),
            ],
            vulnerabilities=[
                VulnerabilityFaker(
                    group_name="testgroup",
                    root_id="root1",
                    finding_id="find1",
                    hacker_email="machine@fluidattacks.com",
                    id="vuln1",
                    created_by="machine@fluidattacks.com",
                    type=VulnerabilityType.INPUTS,
                    state=VulnerabilityStateFaker(where="https://test.com/", source=Source.MACHINE),
                ),
            ],
            stakeholders=[
                StakeholderFaker(email="customer_manager@fluidattacks.com", enrolled=True),
            ],
            organization_access=[
                OrganizationAccessFaker(
                    organization_id=ORG_ID,
                    email="customer_manager@fluidattacks.com",
                    state=OrganizationAccessStateFaker(has_access=True, role="customer_manager"),
                ),
            ],
            group_access=[
                GroupAccessFaker(
                    group_name="testgroup",
                    email="customer_manager@fluidattacks.com",
                    state=GroupAccessStateFaker(has_access=True, role="customer_manager"),
                ),
            ],
        ),
    )
)
async def test_update_git_root_environment_url() -> None:
    # Arrange
    loaders: Dataloaders = get_new_context()
    group = "testgroup"
    user = "customer_manager@fluidattacks.com"
    root_id = "root1"
    url_id = format_environment_id("https://test.com/")
    info = GraphQLResolveInfoFaker(
        user_email=user,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_attribute_internal, "is_under_review", group],
        ],
    )

    # Act
    await mutate(
        _=None,
        info=info,
        group_name=group,
        root_id=root_id,
        url_id=url_id,
        include=False,
    )

    # Assert
    env_url = next(
        (
            url
            for url in await loaders.root_environment_urls.load(
                RootEnvironmentUrlsRequest(root_id=root_id, group_name=group)
            )
            if url.id == url_id
        ),
        None,
    )
    assert env_url is not None
    assert env_url.state.include is False

    vulnerabilities = await get_root_vulnerabilities(root_id)
    assert vulnerabilities[0].state.status is VulnerabilityStateStatus.SAFE
    present_toe_inputs = await loaders.root_toe_inputs.load_nodes(
        RootToeInputsRequest(group_name=group, root_id=root_id, be_present=True)
    )
    assert len(present_toe_inputs) == 0
    not_present_toe_inputs = await loaders.root_toe_inputs.load_nodes(
        RootToeInputsRequest(group_name=group, root_id=root_id, be_present=False)
    )
    assert len(not_present_toe_inputs) == 1


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id=ORG_ID)],
            groups=[
                GroupFaker(
                    name="testgroup",
                    organization_id=ORG_ID,
                ),
            ],
            roots=[
                GitRootFaker(
                    id="root1",
                    group_name="testgroup",
                ),
            ],
            root_environment_urls=[
                RootEnvironmentUrlFaker(
                    id=format_environment_id("https://test.com/"),
                    url="https://test.com/",
                    group_name="testgroup",
                    root_id="root1",
                    state=RootEnvironmentUrlStateFaker(
                        modified_by="hacker@fluidattacks.com", include=False
                    ),
                ),
            ],
            toe_inputs=[
                ToeInputFaker(
                    group_name="testgroup",
                    root_id="root1",
                    component="https://test.com/",
                    entry_point="user",
                ),
            ],
            findings=[
                FindingFaker(group_name="testgroup", id="find1"),
            ],
            vulnerabilities=[
                VulnerabilityFaker(
                    group_name="testgroup",
                    root_id="root1",
                    finding_id="find1",
                    hacker_email="machine@fluidattacks.com",
                    id="vuln1",
                    created_by="machine@fluidattacks.com",
                    type=VulnerabilityType.INPUTS,
                    state=VulnerabilityStateFaker(where="https://test.com/", source=Source.MACHINE),
                ),
            ],
            stakeholders=[
                StakeholderFaker(email="customer_manager@fluidattacks.com", enrolled=True),
            ],
            organization_access=[
                OrganizationAccessFaker(
                    organization_id=ORG_ID,
                    email="customer_manager@fluidattacks.com",
                    state=OrganizationAccessStateFaker(has_access=True, role="customer_manager"),
                ),
            ],
            group_access=[
                GroupAccessFaker(
                    group_name="testgroup",
                    email="customer_manager@fluidattacks.com",
                    state=GroupAccessStateFaker(has_access=True, role="customer_manager"),
                ),
            ],
        ),
    )
)
async def test_update_git_root_environment_url_include() -> None:
    # Arrange
    loaders: Dataloaders = get_new_context()
    group = "testgroup"
    user = "customer_manager@fluidattacks.com"
    root_id = "root1"
    url_id = format_environment_id("https://test.com/")
    info = GraphQLResolveInfoFaker(
        user_email=user,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_attribute_internal, "is_under_review", group],
        ],
    )

    # Act
    await mutate(
        _=None,
        info=info,
        group_name=group,
        root_id=root_id,
        url_id=url_id,
        include=True,
    )

    # Assert
    env_url = next(
        (
            url
            for url in await loaders.root_environment_urls.load(
                RootEnvironmentUrlsRequest(root_id=root_id, group_name=group)
            )
            if url.id == url_id
        ),
        None,
    )
    assert env_url is not None
    assert env_url.state.include is True
