from typing import (
    Any,
)

from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.custom_utils import (
    logs as logs_utils,
)
from integrates.decorators import (
    concurrent_decorators,
    enforce_group_level_auth_async,
    require_is_not_under_review,
    require_login,
    require_service_black,
)
from integrates.roots.types import (
    RootUrlAttributesToUpdate,
)
from integrates.roots.update import (
    update_url_root,
)
from integrates.sessions import (
    domain as sessions_domain,
)

from .payloads.types import (
    SimplePayload,
)
from .schema import (
    MUTATION,
)


@MUTATION.field("updateUrlRoot")
@concurrent_decorators(
    require_login,
    enforce_group_level_auth_async,
    require_service_black,
    require_is_not_under_review,
)
async def mutate(
    _parent: None,
    info: GraphQLResolveInfo,
    group_name: str,
    root_id: str,
    **kwargs: Any,
) -> SimplePayload:
    user_info: dict[str, str] = await sessions_domain.get_jwt_content(info.context)
    user_email: str = user_info["user_email"]

    await update_url_root(
        attributes=RootUrlAttributesToUpdate(
            excluded_sub_paths=kwargs.get("excluded_sub_paths"),
            nickname=kwargs.get("nickname"),
        ),
        loaders=info.context.loaders,
        user_email=user_email,
        group_name=group_name.lower(),
        root_id=root_id,
    )

    logs_utils.cloudwatch_log(
        info.context,
        "Updated URL root",
        extra={
            "group_name": group_name,
            "root_id": root_id,
            "log_type": "Security",
        },
    )

    return SimplePayload(success=True)
