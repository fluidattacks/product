from io import BytesIO

from starlette.datastructures import Headers, UploadFile

from integrates.api.mutations.update_event_evidence import mutate
from integrates.custom_utils import (
    files as files_utils,
)
from integrates.decorators import (
    enforce_group_level_auth_async,
    require_asm,
    require_attribute,
    require_login,
)
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    EventFaker,
    GraphQLResolveInfoFaker,
    GroupAccessFaker,
    GroupAccessStateFaker,
    GroupFaker,
    OrganizationFaker,
    StakeholderFaker,
)
from integrates.testing.mocks import Mock, mocks
from integrates.testing.utils import parametrize, raises

ADMIN_EMAIL = "admin@gmail.com"
HACKER_EMAIL = "hacker@fluidattacks.com"
GROUP_NAME = "test-group"
REATTACKER_EMAIL = "reattacker@fluidattacks.com"
RESOURCER_EMAIL = "resourcer@fluidattacks.com"
CUSTOMER_MANAGER_EMAIL = "customer_manager@fluidattacks.com"
EXECUTIVE_EMAIL = "executive@fluidattacks.com"
ORG_NAME = "test-org"


@parametrize(
    args=["email", "evidence_type"],
    cases=[
        [ADMIN_EMAIL, "IMAGE_1"],
        [REATTACKER_EMAIL, "IMAGE_2"],
        [HACKER_EMAIL, "FILE_1"],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            stakeholders=[
                StakeholderFaker(email=ADMIN_EMAIL),
                StakeholderFaker(email=HACKER_EMAIL),
                StakeholderFaker(email=REATTACKER_EMAIL),
            ],
            events=[EventFaker(id="497f6eca-6276-4993-bfeb-53cbbbba6f08", group_name=GROUP_NAME)],
            organizations=[OrganizationFaker(id="org1")],
            groups=[
                GroupFaker(name=GROUP_NAME, organization_id="org1"),
            ],
            group_access=[
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email=ADMIN_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="admin"),
                ),
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email=HACKER_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="hacker"),
                ),
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email=REATTACKER_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="reattacker"),
                ),
            ],
        )
    ),
    others=[Mock(files_utils, "assert_uploaded_file_mime", "async", True)],
)
async def test_update_event_evidence_success(email: str, evidence_type: str) -> None:
    info = GraphQLResolveInfoFaker(
        user_email=email,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_asm],
            [require_attribute, "has_asm", GROUP_NAME],
        ],
    )
    test_file = UploadFile(
        filename=f"{ORG_NAME}-{GROUP_NAME}-testing123.webm",
        file=BytesIO(b"test content"),
        headers=Headers(headers={"content_type": "video/webm"}),
    )
    result = await mutate(
        _parent=None,
        info=info,
        event_id="497f6eca-6276-4993-bfeb-53cbbbba6f08",
        evidence_type=evidence_type,
        file=test_file,
        group_name=GROUP_NAME,
    )
    assert result.success


@parametrize(
    args=["email"],
    cases=[
        [RESOURCER_EMAIL],
        [CUSTOMER_MANAGER_EMAIL],
        [EXECUTIVE_EMAIL],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            stakeholders=[
                StakeholderFaker(email=RESOURCER_EMAIL),
                StakeholderFaker(email=CUSTOMER_MANAGER_EMAIL),
                StakeholderFaker(email=EXECUTIVE_EMAIL),
            ],
            events=[EventFaker(id="497f6eca-6276-4993-bfeb-53cbbbba6f08", group_name=GROUP_NAME)],
            group_access=[
                GroupAccessFaker(
                    email=RESOURCER_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="resourcer"),
                ),
                GroupAccessFaker(
                    email=CUSTOMER_MANAGER_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="customer_manager"),
                ),
                GroupAccessFaker(
                    email=EXECUTIVE_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="executive"),
                ),
            ],
        )
    )
)
async def test_update_event_evidence_unauthorized(email: str) -> None:
    info = GraphQLResolveInfoFaker(
        user_email=email,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_asm],
            [require_attribute, "has_asm", GROUP_NAME],
        ],
    )
    test_file = UploadFile(filename="test.png", file=BytesIO(b"test content"))
    with raises(Exception, match="Access denied"):
        await mutate(
            _parent=None,
            info=info,
            event_id="497f6eca-6276-4993-bfeb-53cbbbba6f08",
            evidence_type="FILE_1",
            file=test_file,
            group_name=GROUP_NAME,
        )


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            stakeholders=[StakeholderFaker(email=ADMIN_EMAIL)],
            events=[EventFaker(id="497f6eca-6276-4993-bfeb-53cbbbba6f08", group_name=GROUP_NAME)],
            organizations=[OrganizationFaker(id="org1")],
            groups=[
                GroupFaker(name=GROUP_NAME, organization_id="org1"),
            ],
            group_access=[
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email=ADMIN_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="admin"),
                ),
            ],
        )
    )
)
async def test_update_event_evidence_invalid_file_type() -> None:
    info = GraphQLResolveInfoFaker(
        user_email=ADMIN_EMAIL,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_asm],
            [require_attribute, "has_asm", GROUP_NAME],
        ],
    )
    test_file = UploadFile(
        filename=f"{ORG_NAME}-{GROUP_NAME}-testing123.test",
        file=BytesIO(b"test content"),
        headers=Headers(headers={"content_type": "test/test"}),
    )
    with raises(Exception, match="Invalid file type"):
        await mutate(
            _parent=None,
            info=info,
            event_id="497f6eca-6276-4993-bfeb-53cbbbba6f08",
            evidence_type="IMAGE_1",
            file=test_file,
            group_name=GROUP_NAME,
        )
