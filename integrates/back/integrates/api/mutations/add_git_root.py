from typing import (
    TypedDict,
    Unpack,
)

from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.custom_utils.groups import get_group
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.roots.enums import (
    RootCriticality,
)
from integrates.decorators import (
    concurrent_decorators,
    enforce_group_level_auth_async,
    require_is_not_under_review,
    require_login,
    require_service_white,
)
from integrates.groups import (
    domain as groups_domain,
)
from integrates.roots import (
    domain as roots_domain,
)
from integrates.roots.types import (
    GitRootAttributesToAdd,
)
from integrates.sessions import (
    domain as sessions_domain,
)

from .payloads.types import (
    AddRootPayload,
)
from .schema import (
    MUTATION,
)


class AddGitRootArgs(TypedDict):
    branch: str
    includes_health_check: bool
    url: str
    credentials: dict[str, str] | None
    criticality: RootCriticality | None
    gitignore: list[str] | None
    nickname: str | None
    use_egress: bool | None
    use_vpn: bool | None
    use_ztna: bool | None


@MUTATION.field("addGitRoot")
@concurrent_decorators(
    require_login,
    require_is_not_under_review,
    enforce_group_level_auth_async,
    require_service_white,
)
async def mutate(
    _parent: None,
    info: GraphQLResolveInfo,
    group_name: str,
    **kwargs: Unpack[AddGitRootArgs],
) -> AddRootPayload:
    user_info: dict[str, str] = await sessions_domain.get_jwt_content(info.context)
    user_email: str = user_info["user_email"]
    loaders: Dataloaders = info.context.loaders
    group_name = group_name.lower()
    group = await get_group(loaders, group_name)
    root_id = await roots_domain.process_git_root(
        info=info,
        attributes_to_add=GitRootAttributesToAdd(
            branch=kwargs["branch"],
            credentials=kwargs.get("credentials"),
            gitignore=kwargs.get("gitignore") or [],
            includes_health_check=kwargs["includes_health_check"],
            url=kwargs["url"],
            criticality=kwargs.get("criticality") or None,
            nickname=kwargs.get("nickname") or "",
            use_egress=kwargs.get("use_egress") or False,
            use_vpn=kwargs.get("use_vpn") or False,
            use_ztna=kwargs.get("use_ztna") or False,
        ),
        group=group,
        user_email=user_email,
    )
    await groups_domain.fetch_and_remove_repositories(loaders, group.organization_id, kwargs["url"])

    return AddRootPayload(root_id=root_id, success=True)
