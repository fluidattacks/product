from integrates.api.mutations.remove_trusted_device import mutate
from integrates.dataloaders import get_new_context
from integrates.decorators import require_login
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    GraphQLResolveInfoFaker,
    StakeholderFaker,
    StakeholderStateFaker,
    TrustedDeviceFaker,
)
from integrates.testing.mocks import mocks

TEST_EMAIL = "test@example.com"


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            stakeholders=[
                StakeholderFaker(
                    email="test@example.com",
                    state=StakeholderStateFaker(
                        trusted_devices=[
                            TrustedDeviceFaker(browser="Chrome", device="mobile"),
                            TrustedDeviceFaker(browser="Mozilla", device="desktop"),
                        ]
                    ),
                )
            ],
        )
    )
)
async def test_remove_trusted_device() -> None:
    loaders = get_new_context()
    stakeholder = await loaders.stakeholder.load(TEST_EMAIL)

    assert stakeholder
    assert len(stakeholder.state.trusted_devices) == 2

    info = GraphQLResolveInfoFaker(
        user_email="test@example.com",
        decorators=[[require_login]],
    )

    result = await mutate(
        _=None,
        info=info,
        trusted_device={"browser": "Chrome", "device": "mobile"},
    )
    assert result.success is True

    stakeholder = await get_new_context().stakeholder.load(TEST_EMAIL)

    assert stakeholder
    assert len(stakeholder.state.trusted_devices) == 1
    assert stakeholder.state.trusted_devices[0].browser == "Mozilla"
