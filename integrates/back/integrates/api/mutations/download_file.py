import logging
import logging.config

from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.custom_exceptions import (
    ErrorDownloadingFile,
)
from integrates.custom_utils import (
    analytics,
)
from integrates.custom_utils import (
    logs as logs_utils,
)
from integrates.decorators import (
    concurrent_decorators,
    enforce_group_level_auth_async,
    require_asm,
    require_is_not_under_review,
    require_login,
)
from integrates.resources import (
    domain as resources_domain,
)
from integrates.sessions import (
    domain as sessions_domain,
)

from .payloads.types import (
    DownloadFilePayload,
)
from .schema import (
    MUTATION,
)

LOGGER = logging.getLogger(__name__)


@MUTATION.field("downloadFile")
@concurrent_decorators(
    require_login,
    enforce_group_level_auth_async,
    require_is_not_under_review,
    require_asm,
)
async def mutate(
    _: None,
    info: GraphQLResolveInfo,
    group_name: str,
    **parameters: str,
) -> DownloadFilePayload:
    file_info = parameters["files_data_input"]
    group_name = group_name.lower()
    user_info = await sessions_domain.get_jwt_content(info.context)
    user_email = user_info["user_email"]
    signed_url = await resources_domain.download_file(file_info, group_name)
    if signed_url:
        logs_utils.cloudwatch_log(
            info.context,
            "Downloaded file of the group",
            extra={
                "files_data_input": parameters["files_data_input"],
                "group_name": group_name,
                "log_type": "Security",
            },
        )
        await analytics.mixpanel_track(
            user_email,
            "DownloadGroupFile",
            Group=group_name.upper(),
            FileName=parameters["files_data_input"],
        )
    else:
        logs_utils.cloudwatch_log(
            info.context,
            "Attempted to download file of the group",
            extra={
                "files_data_input": parameters["files_data_input"],
                "group_name": group_name,
                "log_type": "Security",
            },
        )
        LOGGER.error("Couldn't generate signed URL", extra={"extra": parameters})
        raise ErrorDownloadingFile.new()

    return DownloadFilePayload(success=True, url=str(signed_url))
