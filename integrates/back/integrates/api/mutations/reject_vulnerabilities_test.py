from typing import Any

from integrates.api.mutations.reject_vulnerabilities import mutate
from integrates.custom_exceptions import VulnNotInFinding
from integrates.db_model.vulnerabilities.enums import VulnerabilityStateStatus
from integrates.decorators import (
    enforce_group_level_auth_async,
    require_attribute,
    require_login,
    require_report_vulnerabilities,
)
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    FindingFaker,
    GraphQLResolveInfoFaker,
    GroupAccessFaker,
    GroupAccessStateFaker,
    GroupFaker,
    StakeholderFaker,
    VulnerabilityFaker,
    VulnerabilityStateFaker,
)
from integrates.testing.mocks import mocks
from integrates.testing.utils import parametrize, raises

# Allowed users
ADMIN_EMAIL = "admin@gmail.com"
REVIEWER_EMAIL = "reviewer@gmail.com"

# Not allowed users
CUSTOMER_MANAGER_EMAIL = "customer_manager@gmail.com"
HACKER_EMAIL = "hacker@gmail.com"
REATTACKER_EMAIL = "reattacker@gmail.com"
RESOURCER_EMAIL = "resourcer@gmail.com"
USER_EMAIL = "user@gmail.com"
GROUP_MANAGER_EMAIL = "group_manager@gmail.com"
VULNERABILITY_MANAGER_EMAIL = "vulnerability_manager@gmail.com"

FINDING_ID = "3c475384-834c-47b0-ac71-a41a022e401c"
VULN_ID_1 = "be09edb7-cd5c-47ed-bee4-97c645acdce10"


DEFAULT_DECORATORS: list[list[Any]] = [
    [require_login],
    [require_report_vulnerabilities],
    [enforce_group_level_auth_async],
    [require_attribute, "can_report_vulnerabilities", "test-group"],
]


@parametrize(
    args=["email", "vuln_ids", "reasons"],
    cases=[
        [ADMIN_EMAIL, [VULN_ID_1], ["OTHER"]],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            groups=[GroupFaker()],
            findings=[
                FindingFaker(id=FINDING_ID),
            ],
            vulnerabilities=[
                VulnerabilityFaker(
                    id=VULN_ID_1,
                )
            ],
            stakeholders=[
                StakeholderFaker(email=ADMIN_EMAIL),
                StakeholderFaker(email=REVIEWER_EMAIL),
            ],
            group_access=[
                GroupAccessFaker(
                    email=ADMIN_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="admin"),
                ),
                GroupAccessFaker(
                    email=REVIEWER_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="reviewer"),
                ),
            ],
        )
    )
)
async def test_mutate_vuln_not_in_finding(
    email: str,
    vuln_ids: list[str],
    reasons: list[str],
) -> None:
    info = GraphQLResolveInfoFaker(
        user_email=email,
        decorators=DEFAULT_DECORATORS,
    )
    with raises(VulnNotInFinding):
        await mutate(
            _=None,
            info=info,
            finding_id=FINDING_ID,
            vulnerabilities=vuln_ids,
            reasons=reasons,
        )


@parametrize(
    args=["email", "vuln_ids", "reasons"],
    cases=[
        [CUSTOMER_MANAGER_EMAIL, [VULN_ID_1], ["OTHER"]],
        [HACKER_EMAIL, [VULN_ID_1], ["OTHER"]],
        [REATTACKER_EMAIL, [VULN_ID_1], ["OTHER"]],
        [RESOURCER_EMAIL, [VULN_ID_1], ["OTHER"]],
        [USER_EMAIL, [VULN_ID_1], ["OTHER"]],
        [GROUP_MANAGER_EMAIL, [VULN_ID_1], ["OTHER"]],
        [VULNERABILITY_MANAGER_EMAIL, [VULN_ID_1], ["OTHER"]],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            groups=[GroupFaker()],
            findings=[
                FindingFaker(),
            ],
            vulnerabilities=[VulnerabilityFaker()],
            stakeholders=[
                StakeholderFaker(email=CUSTOMER_MANAGER_EMAIL),
                StakeholderFaker(email=HACKER_EMAIL),
                StakeholderFaker(email=REATTACKER_EMAIL),
                StakeholderFaker(email=RESOURCER_EMAIL),
                StakeholderFaker(email=USER_EMAIL),
                StakeholderFaker(email=GROUP_MANAGER_EMAIL),
                StakeholderFaker(email=VULNERABILITY_MANAGER_EMAIL),
            ],
            group_access=[
                GroupAccessFaker(
                    email=CUSTOMER_MANAGER_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="customer_manager"),
                ),
                GroupAccessFaker(
                    email=HACKER_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="hacker"),
                ),
                GroupAccessFaker(
                    email=REATTACKER_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="reattacker"),
                ),
                GroupAccessFaker(
                    email=USER_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="user"),
                ),
                GroupAccessFaker(
                    email=GROUP_MANAGER_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="group_manager"),
                ),
                GroupAccessFaker(
                    email=VULNERABILITY_MANAGER_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="vulnerability_manager"),
                ),
                GroupAccessFaker(
                    email=RESOURCER_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="resourcer"),
                ),
            ],
        )
    )
)
async def test_mutate_not_allowed(
    email: str,
    vuln_ids: list[str],
    reasons: list[str],
) -> None:
    info = GraphQLResolveInfoFaker(
        user_email=email,
        decorators=DEFAULT_DECORATORS,
    )
    with raises(Exception, match="Access denied"):
        await mutate(
            _=None,
            info=info,
            finding_id=FINDING_ID,
            vulnerabilities=vuln_ids,
            reasons=reasons,
        )


@parametrize(
    args=["email", "vuln_ids", "reasons", "other_reason"],
    cases=[
        [ADMIN_EMAIL, [VULN_ID_1], ["OTHER"], "some other reason"],
        [REVIEWER_EMAIL, [VULN_ID_1], ["OTHER"], "a very convincing reason"],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            groups=[GroupFaker()],
            findings=[
                FindingFaker(id=FINDING_ID),
            ],
            vulnerabilities=[
                VulnerabilityFaker(
                    id=VULN_ID_1,
                    state=VulnerabilityStateFaker(status=VulnerabilityStateStatus.SUBMITTED),
                    finding_id=FINDING_ID,
                )
            ],
            stakeholders=[
                StakeholderFaker(email=ADMIN_EMAIL),
                StakeholderFaker(email=REVIEWER_EMAIL),
            ],
            group_access=[
                GroupAccessFaker(
                    email=ADMIN_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="admin"),
                ),
                GroupAccessFaker(
                    email=REVIEWER_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="reviewer"),
                ),
            ],
        )
    )
)
async def test_mutate_success(
    email: str,
    vuln_ids: list[str],
    reasons: list[str],
    other_reason: str | None,
) -> None:
    info = GraphQLResolveInfoFaker(
        user_email=email,
        decorators=DEFAULT_DECORATORS,
    )
    await mutate(
        _=None,
        info=info,
        finding_id=FINDING_ID,
        vulnerabilities=vuln_ids,
        reasons=reasons,
        other_reason=other_reason,
    )
