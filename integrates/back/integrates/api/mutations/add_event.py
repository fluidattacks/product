from datetime import (
    datetime,
)

from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.custom_utils import (
    logs as logs_utils,
)
from integrates.db_model.events.enums import (
    EventType,
)
from integrates.decorators import (
    concurrent_decorators,
    enforce_group_level_auth_async,
    require_asm,
    require_is_not_under_review,
    require_login,
)
from integrates.events import (
    domain as events_domain,
)
from integrates.sessions import (
    domain as sessions_domain,
)

from .payloads.types import (
    AddEventPayload,
)
from .schema import (
    MUTATION,
)


@MUTATION.field("addEvent")
@concurrent_decorators(
    require_login,
    enforce_group_level_auth_async,
    require_is_not_under_review,
    require_asm,
)
async def mutate(
    _: None,
    info: GraphQLResolveInfo,
    group_name: str,
    detail: str,
    event_date: datetime,
    event_type: str,
    root_id: str | None = None,
    environment_url: str | None = None,
) -> AddEventPayload:
    """Resolve add_event mutation."""
    user_info = await sessions_domain.get_jwt_content(info.context)
    hacker_email = user_info["user_email"]

    event_id = await events_domain.add_event(
        loaders=info.context.loaders,
        hacker_email=hacker_email,
        group_name=group_name.lower(),
        detail=detail,
        event_date=event_date,
        event_type=EventType[event_type],
        environment_url=environment_url,
        root_id=root_id,
    )
    logs_utils.cloudwatch_log(
        info.context,
        "Added a new event in the group",
        extra={
            "event_id": event_id,
            "group_name": group_name,
            "log_type": "Security",
        },
    )

    return AddEventPayload(event_id=event_id, success=True)
