from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.custom_utils import (
    logs as logs_utils,
)
from integrates.db_model.events.enums import (
    EventSolutionReason,
)
from integrates.decorators import (
    concurrent_decorators,
    enforce_group_level_auth_async,
    require_asm,
    require_login,
)
from integrates.findings.domain.events import solve_event_and_process_vulnerabilities
from integrates.sessions import (
    domain as sessions_domain,
)
from integrates.unreliable_indicators.enums import (
    EntityDependency,
)
from integrates.unreliable_indicators.operations import (
    update_unreliable_indicators_by_deps,
)

from .payloads.types import (
    SimplePayload,
)
from .schema import (
    MUTATION,
)


@MUTATION.field("solveEvent")
@concurrent_decorators(
    require_login,
    enforce_group_level_auth_async,
    require_asm,
)
async def mutate(
    _: None,
    info: GraphQLResolveInfo,
    event_id: str,
    reason: str,
    group_name: str,
    **kwargs: str,
) -> SimplePayload:
    other = kwargs.get("other")
    user_info = await sessions_domain.get_jwt_content(info.context)
    hacker_email = user_info["user_email"]
    reattacks_dict, verifications_dict = await solve_event_and_process_vulnerabilities(
        loaders=info.context.loaders,
        event_id=event_id,
        group_name=group_name,
        hacker_email=hacker_email,
        reason=EventSolutionReason[reason],
        other=other,
        user_info=user_info,
    )
    logs_utils.cloudwatch_log(
        info.context,
        "Solved event",
        extra={
            "event_id": event_id,
            "group_name": group_name,
            "log_type": "Security",
        },
    )

    if bool(reattacks_dict):
        await update_unreliable_indicators_by_deps(
            EntityDependency.request_vulnerabilities_verification,
            finding_ids=list(reattacks_dict.keys()),
            vulnerability_ids=[
                vuln_id for reattack_ids in reattacks_dict.values() for vuln_id in reattack_ids
            ],
        )
    if bool(verifications_dict):
        await update_unreliable_indicators_by_deps(
            EntityDependency.verify_vulnerabilities_request,
            finding_ids=list(verifications_dict.keys()),
            vulnerability_ids=[
                vuln_id
                for verification_ids in verifications_dict.values()
                for vuln_id in verification_ids
            ],
        )
    else:
        logs_utils.cloudwatch_log(
            info.context,
            "Attempted to solve event",
            extra={
                "event_id": event_id,
                "group_name": group_name,
                "log_type": "Security",
            },
        )

    return SimplePayload(success=True)
