from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.api.types import (
    APP_EXCEPTIONS,
)
from integrates.custom_exceptions import (
    InvalidVulnsNumber,
)
from integrates.custom_utils import (
    logs as logs_utils,
)
from integrates.custom_utils.findings import get_finding
from integrates.db_model.hook.enums import (
    HookEvent,
)
from integrates.db_model.vulnerabilities.enums import (
    VulnerabilityStateReason,
)
from integrates.decorators import (
    concurrent_decorators,
    enforce_group_level_auth_async,
    require_asm,
    require_finding_access,
    require_login,
    require_report_vulnerabilities,
)
from integrates.findings import (
    domain as findings_domain,
)
from integrates.groups import (
    hook_notifier,
)
from integrates.sessions import (
    domain as sessions_domain,
)
from integrates.unreliable_indicators.enums import (
    EntityDependency,
)
from integrates.unreliable_indicators.operations import (
    update_unreliable_indicators_by_deps,
)

from .payloads.types import (
    SimplePayload,
)
from .schema import (
    MUTATION,
)


@MUTATION.field("verifyVulnerabilitiesRequest")
@concurrent_decorators(
    require_login,
    enforce_group_level_auth_async,
    require_asm,
    require_report_vulnerabilities,
    require_finding_access,
)
async def mutate(
    _: None,
    info: GraphQLResolveInfo,
    finding_id: str,
    justification: str,
    open_vulnerabilities: list[str],
    closed_vulnerabilities: list[str],
    **kwargs: VulnerabilityStateReason,
) -> SimplePayload:
    try:
        user_info = await sessions_domain.get_jwt_content(info.context)

        max_number_of_vulns = 150
        if len(open_vulnerabilities + closed_vulnerabilities) > max_number_of_vulns:
            raise InvalidVulnsNumber(number_of_vulns=max_number_of_vulns)

        await findings_domain.verify_vulnerabilities(
            finding_id=finding_id,
            user_info=user_info,
            justification=justification,
            open_vulns_ids=open_vulnerabilities,
            closed_vulns_ids=closed_vulnerabilities,
            vulns_to_close_from_file=[],
            loaders=info.context.loaders,
            verification_reason=kwargs.get(
                "verification_reason",
                VulnerabilityStateReason.VERIFIED_AS_SAFE,
            ),
        )
        finding = await get_finding(info.context.loaders, finding_id)
        await hook_notifier.process_hook_event(
            loaders=info.context.loaders,
            group_name=finding.group_name,
            info={
                "findingId": finding_id,
                "safe": closed_vulnerabilities,
                "vulnerable": open_vulnerabilities,
            },
            event=HookEvent.VULNERABILITY_VERIFIED,
        )
        await update_unreliable_indicators_by_deps(
            EntityDependency.verify_vulnerabilities_request,
            finding_ids=[finding_id],
            vulnerability_ids=open_vulnerabilities + closed_vulnerabilities,
        )
        logs_utils.cloudwatch_log(
            info.context,
            "Verified vuln verification in the finding",
            extra={"finding_id": finding_id, "log_type": "Security"},
        )
    except APP_EXCEPTIONS:
        logs_utils.cloudwatch_log(
            info.context,
            "Attempted to verify vuln verification in the finding",
            extra={"finding_id": finding_id, "log_type": "Security"},
        )
        raise

    return SimplePayload(success=True)
