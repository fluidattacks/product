from integrates.api.mutations.remove_finding_evidence import (
    mutate,
)
from integrates.dataloaders import get_new_context
from integrates.decorators import (
    enforce_group_level_auth_async,
    require_attribute,
    require_login,
)
from integrates.findings import (
    storage as findings_storage,
)
from integrates.testing.aws import (
    IntegratesAws,
    IntegratesDynamodb,
)
from integrates.testing.fakers import (
    FindingEvidenceFaker,
    FindingEvidencesFaker,
    FindingFaker,
    FindingStateFaker,
    GitRootFaker,
    GitRootStateFaker,
    GraphQLResolveInfoFaker,
    GroupAccessFaker,
    GroupAccessStateFaker,
    GroupFaker,
    OrganizationFaker,
    StakeholderFaker,
)
from integrates.testing.mocks import Mock, mocks
from integrates.testing.utils import parametrize, raises

EMAIL_TEST = "hacker@fluidattacks.com"
GROUP_NAME = "group1"
ROOT_ID = "root1"
FINDING_ID = "9a85b711-4b4e-439a-832f-18fb286e5baa"


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            stakeholders=[StakeholderFaker(email=EMAIL_TEST, role="hacker")],
            groups=[GroupFaker(name=GROUP_NAME, organization_id="org1")],
            group_access=[
                GroupAccessFaker(
                    email=EMAIL_TEST,
                    group_name=GROUP_NAME,
                    state=GroupAccessStateFaker(has_access=True, role="hacker"),
                ),
            ],
            roots=[
                GitRootFaker(
                    group_name=GROUP_NAME,
                    id=ROOT_ID,
                    state=GitRootStateFaker(nickname="back"),
                ),
            ],
            findings=[
                FindingFaker(
                    group_name="group1",
                    id=FINDING_ID,
                    state=FindingStateFaker(modified_by="hacker@fluidattacks.com"),
                    title="237. Logging of sensitive data",
                    evidences=FindingEvidencesFaker(evidence1=FindingEvidenceFaker(is_draft=True)),
                ),
            ],
        )
    ),
    others=[Mock(findings_storage, "remove_evidence", "async", None)],
)
async def test_remove_finding_evidence() -> None:
    start_finding = await get_new_context().finding.load(FINDING_ID)
    assert start_finding is not None
    assert start_finding.evidences.evidence1 is not None

    info = GraphQLResolveInfoFaker(
        user_email=EMAIL_TEST,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_attribute, "has_asm", GROUP_NAME],
        ],
    )
    result = await mutate(_=None, info=info, evidence_id="evidence_route_1", finding_id=FINDING_ID)
    assert result.success is True

    final_finding = await get_new_context().finding.load(FINDING_ID)
    assert final_finding is not None
    assert final_finding.evidences.evidence1 is None


@parametrize(
    args=["email"],
    cases=[["reviewer@fluidattacks.com"], ["user@test.test"]],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            stakeholders=[
                StakeholderFaker(email="hacker@test.test", role="hacker"),
                StakeholderFaker(email="reattacker@test.test", role="reattacker"),
                StakeholderFaker(email="uset@test.test", role="user"),
            ],
            groups=[GroupFaker(name=GROUP_NAME, organization_id="org1")],
            group_access=[
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email="reviewer@fluidattacks.com",
                    state=GroupAccessStateFaker(has_access=True, role="reviewer"),
                ),
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email="user@test.test",
                    state=GroupAccessStateFaker(has_access=True, role="user"),
                ),
            ],
            roots=[
                GitRootFaker(
                    group_name=GROUP_NAME, id="root1", state=GitRootStateFaker(nickname="back")
                )
            ],
            findings=[
                FindingFaker(
                    group_name="group1",
                    id=FINDING_ID,
                    state=FindingStateFaker(modified_by="hacker@fluidattacks.com"),
                    title="237. Logging of sensitive data",
                    evidences=FindingEvidencesFaker(evidence1=FindingEvidenceFaker(is_draft=True)),
                ),
            ],
        ),
    )
)
async def test_remove_finding_evidence_access_denied(email: str) -> None:
    info = GraphQLResolveInfoFaker(
        user_email=email,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_attribute, "has_asm", GROUP_NAME],
        ],
    )

    with raises(Exception, match="Access denied"):
        await mutate(_=None, info=info, evidence_id="evidence_route_1", finding_id=FINDING_ID)
