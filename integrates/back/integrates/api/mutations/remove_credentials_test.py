from integrates import (
    batch,
)
from integrates.api.mutations.remove_credentials import (
    mutate,
)
from integrates.batch.dal.get import (
    get_actions_by_name,
)
from integrates.batch.enums import (
    Action,
)
from integrates.custom_exceptions import (
    InvalidGitCredentials,
)
from integrates.dataloaders import get_new_context
from integrates.db_model.enums import CredentialType
from integrates.decorators import (
    enforce_group_level_auth_async,
    require_login,
)
from integrates.organizations.domain import (
    get_credentials,
)
from integrates.testing.aws import (
    IntegratesAws,
    IntegratesDynamodb,
)
from integrates.testing.fakers import (
    CredentialsFaker,
    CredentialsStateFaker,
    GraphQLResolveInfoFaker,
    OrganizationAccessFaker,
    OrganizationAccessStateFaker,
    OrganizationFaker,
    StakeholderFaker,
)
from integrates.testing.mocks import Mock, mocks
from integrates.testing.utils import parametrize, raises

from .payloads.types import (
    SimplePayload,
)

EMAIL_TEST = "user@clientapp.com"
ORG_ID = "ORG#40f6da5f-4f66-4bf0-825b-a2d9748ad6db"


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id=ORG_ID)],
            stakeholders=[StakeholderFaker(email=EMAIL_TEST, role="organization_manager")],
            organization_access=[
                OrganizationAccessFaker(
                    organization_id=ORG_ID,
                    email=EMAIL_TEST,
                    state=OrganizationAccessStateFaker(
                        has_access=True, role="organization_manager"
                    ),
                ),
            ],
            credentials=[
                CredentialsFaker(
                    credential_id="credential-1-id",
                    organization_id=ORG_ID,
                    state=CredentialsStateFaker(
                        name="credential-1", state_type=CredentialType.SSH, owner=EMAIL_TEST
                    ),
                )
            ],
        )
    ),
    others=[Mock(batch.dal.put, "put_action_to_batch", "async", "123456")],
)
async def test_remove_credentials() -> None:
    info = GraphQLResolveInfoFaker(
        user_email=EMAIL_TEST,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
        ],
    )
    result = await mutate(
        _=None,
        info=info,
        credentials_id="credentials_id_1",
        organization_id=ORG_ID,
    )
    assert result == SimplePayload(success=True)

    with raises(InvalidGitCredentials):
        await get_credentials(get_new_context(), "credential_id_1", ORG_ID)

    pending_executions = await get_actions_by_name(
        action_name=Action.UPDATE_ORGANIZATION_REPOSITORIES,
        entity=ORG_ID.lower().lstrip("org#"),
    )
    assert len(pending_executions) == 1


@parametrize(
    args=["email"],
    cases=[["hacker@test.test"], ["reattacker@test.test"], ["user@test.test"]],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id=ORG_ID)],
            stakeholders=[
                StakeholderFaker(email="hacker@test.test", role="hacker"),
                StakeholderFaker(email="reattacker@test.test", role="reattacker"),
                StakeholderFaker(email="uset@test.test", role="user"),
            ],
            organization_access=[
                OrganizationAccessFaker(
                    organization_id=ORG_ID,
                    email="hacker@test.test",
                    state=OrganizationAccessStateFaker(has_access=True, role="hacker"),
                ),
                OrganizationAccessFaker(
                    organization_id=ORG_ID,
                    email="reattacker@test.test",
                    state=OrganizationAccessStateFaker(has_access=True, role="reattacker"),
                ),
                OrganizationAccessFaker(
                    organization_id=ORG_ID,
                    email="user@test.test",
                    state=OrganizationAccessStateFaker(has_access=True, role="user"),
                ),
            ],
        ),
    )
)
async def test_remove_credentials_access_denied(email: str) -> None:
    info = GraphQLResolveInfoFaker(
        user_email=email,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
        ],
    )

    with raises(Exception, match="Access denied"):
        await mutate(
            _=None,
            info=info,
            credentials_id="credentials_id_1",
            organization_id=ORG_ID,
        )
