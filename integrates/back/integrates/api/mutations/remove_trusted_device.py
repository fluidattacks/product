from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.class_types.types import (
    Item,
)
from integrates.db_model.stakeholders.types import (
    TrustedDevice,
)
from integrates.decorators import (
    require_login,
)
from integrates.sessions import (
    domain as sessions_domain,
)
from integrates.stakeholders.domain import (
    remove_trusted_device,
)

from .payloads.types import (
    SimplePayload,
)
from .schema import (
    MUTATION,
)


@MUTATION.field("removeTrustedDevice")
@require_login
async def mutate(
    _: None,
    info: GraphQLResolveInfo,
    trusted_device: Item,
) -> SimplePayload:
    loaders = info.context.loaders
    user_info = await sessions_domain.get_jwt_content(info.context)
    user_email: str = user_info["user_email"]
    trusted_device_object: TrustedDevice = TrustedDevice(
        browser=trusted_device["browser"],
        device=trusted_device["device"],
        otp_token_jti=trusted_device.get("otp_token_jti", None),
    )

    await remove_trusted_device(
        loaders=loaders,
        trusted_device=trusted_device_object,
        user_email=user_email,
    )

    return SimplePayload(success=True)
