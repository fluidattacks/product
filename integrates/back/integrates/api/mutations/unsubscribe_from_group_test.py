from graphql import GraphQLError

from integrates.api.mutations.unsubscribe_from_group import mutate
from integrates.dataloaders import get_new_context
from integrates.decorators import (
    enforce_group_level_auth_async,
    require_attribute,
    require_attribute_internal,
    require_login,
)
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    GraphQLResolveInfoFaker,
    GroupAccessFaker,
    GroupAccessStateFaker,
    GroupFaker,
    OrganizationFaker,
    StakeholderFaker,
)
from integrates.testing.mocks import mocks
from integrates.testing.utils import raises

GROUP_NAME = "group1"
USER_EMAIL = "user@gmail.com"


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(id="org1"),
            ],
            stakeholders=[
                StakeholderFaker(email=USER_EMAIL, role="user"),
            ],
            groups=[
                GroupFaker(name=GROUP_NAME, organization_id="org1"),
            ],
            group_access=[
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email=USER_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="user"),
                ),
            ],
        ),
    )
)
async def test_unsubscribe_from_group() -> None:
    loaders = get_new_context()
    stakeholder = await loaders.stakeholder.load(USER_EMAIL)
    assert stakeholder is not None

    info = GraphQLResolveInfoFaker(
        user_email=USER_EMAIL,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_attribute_internal, "is_under_review", GROUP_NAME],
            [require_attribute, "has_asm", GROUP_NAME],
        ],
    )
    result = await mutate(
        _=None,
        info=info,
        group_name=GROUP_NAME,
    )
    assert result.success
    stakeholder = await get_new_context().stakeholder.load(USER_EMAIL)
    assert stakeholder is None


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(id="org1"),
            ],
            stakeholders=[
                StakeholderFaker(email=USER_EMAIL, role="user"),
            ],
            groups=[
                GroupFaker(name=GROUP_NAME, organization_id="org1"),
            ],
        ),
    )
)
async def test_unsubscribe_from_group_access_denied() -> None:
    info = GraphQLResolveInfoFaker(
        user_email=USER_EMAIL,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_attribute_internal, "is_under_review", GROUP_NAME],
            [require_attribute, "has_asm", GROUP_NAME],
        ],
    )
    with raises(GraphQLError, match="Access denied"):
        await mutate(
            _=None,
            info=info,
            group_name=GROUP_NAME,
        )
