from integrates.api.mutations.update_finding_description import mutate
from integrates.custom_exceptions import InvalidFieldChange
from integrates.decorators import (
    enforce_group_level_auth_async,
    require_asm,
    require_attribute,
    require_finding_access,
    require_login,
    require_report_vulnerabilities,
)
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    FindingFaker,
    GraphQLResolveInfoFaker,
    GroupFaker,
    OrganizationFaker,
    StakeholderFaker,
)
from integrates.testing.mocks import mocks
from integrates.testing.utils import raises

GROUP_NAME = "test-group"
USER_EMAIL = "user@gmail.com"


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(id="org1"),
            ],
            stakeholders=[
                StakeholderFaker(email=USER_EMAIL, role="admin"),
            ],
            groups=[
                GroupFaker(name=GROUP_NAME, organization_id="org1"),
            ],
            findings=[
                FindingFaker(
                    id="463af698-e87f-4f0f-8c88-4e9c5196c965",
                    group_name=GROUP_NAME,
                    title="Original Title",
                    description="Original description",
                    attack_vector_description="Original attack vector",
                    recommendation="Original recommendation",
                    threat="Original threat",
                ),
            ],
        ),
    ),
)
async def test_update_description_invalid_title_policy() -> None:
    info = GraphQLResolveInfoFaker(
        user_email=USER_EMAIL,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_asm],
            [require_attribute, "has_asm", GROUP_NAME],
            [require_report_vulnerabilities],
            [require_attribute, "can_report_vulnerabilities", GROUP_NAME],
            [require_finding_access],
        ],
    )
    with raises(InvalidFieldChange):
        await mutate(
            _=None,
            info=info,
            finding_id="463af698-e87f-4f0f-8c88-4e9c5196c965",
            title="New Invalid Title",
            description="New description",
            attack_vector_description="New attack vector",
            recommendation="New recommendation",
            threat="New threat",
            sorts="YES",
        )


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(id="org1"),
            ],
            stakeholders=[
                StakeholderFaker(email=USER_EMAIL, role="admin"),
            ],
            groups=[
                GroupFaker(name=GROUP_NAME, organization_id="org1"),
            ],
            findings=[
                FindingFaker(
                    id="463af698-e87f-4f0f-8c88-4e9c5196c965",
                    group_name=GROUP_NAME,
                    description="Original description",
                    attack_vector_description="Original attack vector",
                    recommendation="Original recommendation",
                    threat="Original threat",
                ),
            ],
        ),
    ),
)
async def test_update_description_with_unfulfilled_requirements() -> None:
    info = GraphQLResolveInfoFaker(
        user_email=USER_EMAIL,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_asm],
            [require_attribute, "has_asm", GROUP_NAME],
            [require_report_vulnerabilities],
            [require_attribute, "can_report_vulnerabilities", GROUP_NAME],
            [require_finding_access],
        ],
    )
    result = await mutate(
        _=None,
        info=info,
        finding_id="463af698-e87f-4f0f-8c88-4e9c5196c965",
        title="001. SQL injection - C Sharp SQL API",
        description="Updated description",
        attack_vector_description="Updated attack vector",
        recommendation="Updated recommendation",
        threat="Updated threat",
        sorts="YES",
        unfulfilled_requirements=["169", "173"],
    )
    assert result.success
    assert result.finding.description == "Updated description"
    assert result.finding.unfulfilled_requirements == ["169", "173"]


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(id="org1"),
            ],
            stakeholders=[
                StakeholderFaker(email=USER_EMAIL, role="admin"),
            ],
            groups=[
                GroupFaker(name=GROUP_NAME, organization_id="org1"),
            ],
            findings=[
                FindingFaker(
                    id="463af698-e87f-4f0f-8c88-4e9c5196c965",
                    group_name=GROUP_NAME,
                    description="Original description",
                    attack_vector_description="Original attack vector",
                    recommendation="Original recommendation",
                    threat="Original threat",
                ),
            ],
        ),
    ),
)
async def test_update_description_without_sorts() -> None:
    info = GraphQLResolveInfoFaker(
        user_email=USER_EMAIL,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_asm],
            [require_attribute, "has_asm", GROUP_NAME],
            [require_report_vulnerabilities],
            [require_attribute, "can_report_vulnerabilities", GROUP_NAME],
            [require_finding_access],
        ],
    )
    result = await mutate(
        _=None,
        info=info,
        finding_id="463af698-e87f-4f0f-8c88-4e9c5196c965",
        title="001. SQL injection - C Sharp SQL API",
        description="Updated description",
        attack_vector_description="Updated attack vector",
        recommendation="Updated recommendation",
        threat="Updated threat",
    )
    assert result.success
    assert result.finding.description == "Updated description"
