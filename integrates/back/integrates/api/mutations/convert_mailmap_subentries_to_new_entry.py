from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.custom_utils import (
    logs as logs_utils,
)
from integrates.db_model.mailmap.types import (
    MailmapSubentry,
)
from integrates.decorators import (
    concurrent_decorators,
    enforce_organization_level_auth_async,
    require_login,
)
from integrates.mailmap import (
    convert_mailmap_subentries_to_new_entry,
)

from .payloads.types import (
    SimplePayload,
)
from .schema import (
    MUTATION,
)


@MUTATION.field("convertMailmapSubentriesToNewEntry")
@concurrent_decorators(
    require_login,
    enforce_organization_level_auth_async,
)
async def mutate(
    _parent: None,
    info: GraphQLResolveInfo,
    entry_email: str,
    main_subentry: MailmapSubentry,
    other_subentries: list[MailmapSubentry],
    organization_id: str,
) -> SimplePayload:
    await convert_mailmap_subentries_to_new_entry(
        entry_email=entry_email,
        main_subentry=main_subentry,
        other_subentries=other_subentries,
        organization_id=organization_id,
    )

    logs_utils.cloudwatch_log(
        info.context,
        "Converted mailmap aliases to new author",
        extra={
            "entry_email": entry_email,
            "main_subentry_email": main_subentry["mailmap_subentry_email"],
            "other_subentries_emails": [
                subentry["mailmap_subentry_email"] for subentry in other_subentries
            ],
            "organization_id": organization_id,
        },
    )

    return SimplePayload(success=True)
