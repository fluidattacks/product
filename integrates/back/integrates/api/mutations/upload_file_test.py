import os
from datetime import datetime
from io import BytesIO

import aiofiles
from graphql import GraphQLError
from starlette.datastructures import Headers, UploadFile

from integrates.api.mutations.upload_file import mutate
from integrates.custom_exceptions import InvalidFileType, RootNotFound
from integrates.custom_utils.findings import get_finding
from integrates.dataloaders import get_new_context
from integrates.db_model.findings.enums import FindingStatus
from integrates.db_model.findings.types import FindingEvidence, FindingEvidences
from integrates.db_model.vulnerabilities.enums import VulnerabilityStateStatus
from integrates.decorators import enforce_group_level_auth_async, require_attribute, require_login
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    EMAIL_FLUIDATTACKS_HACKER,
    FindingFaker,
    FindingUnreliableIndicatorsFaker,
    GitRootFaker,
    GitRootStateFaker,
    GraphQLResolveInfoFaker,
    GroupAccessFaker,
    GroupAccessStateFaker,
    GroupFaker,
    OrganizationFaker,
    RootEnvironmentUrlFaker,
    RootEnvironmentUrlStateFaker,
    StakeholderFaker,
    ToeInputFaker,
)
from integrates.testing.mocks import mocks
from integrates.testing.utils import parametrize, raises


async def read_file(yaml_file_name: str) -> UploadFile:
    path: str = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    filename: str = os.path.join(path, f"test_files/upload_file/{yaml_file_name}")
    async with aiofiles.open(filename, "rb") as test_file:
        headers = Headers(headers={"content_type": "text/x-yaml"})
        file_contents = await test_file.read()
        return UploadFile(
            filename=str(test_file.name),
            headers=headers,
            file=BytesIO(file_contents),
        )


async def _get_vulns(
    finding_id: str,
    group_name: str,
) -> list:
    loaders = get_new_context()
    finding_vulns = await loaders.finding_vulnerabilities.load(finding_id)
    roots = await loaders.group_roots.load(group_name)
    roots_nickname: dict[str, str] = {root.id: root.state.nickname for root in roots}
    return sorted(
        (
            {
                "commit_hash": vuln.state.commit,
                "repo_nickname": roots_nickname[vuln.root_id or ""],
                "specific": vuln.state.specific,
                "state": vuln.state.status.value,
                "stream": vuln.stream,
                "treatment_status": vuln.treatment.status.value if vuln.treatment else None,
                "type": vuln.type.value,
                "verification_status": vuln.verification.status.value
                if vuln.verification
                else None,
                "where": vuln.state.where,
                "technique": vuln.technique,
            }
            for vuln in finding_vulns
            if vuln.zero_risk is None
        ),
        key=str,
    )


@parametrize(
    args=["email", "finding_id", "file_name", "error_message"],
    cases=[
        [
            EMAIL_FLUIDATTACKS_HACKER,
            "3c475384-834c-47b0-ac71-a41a022e401c",
            "test-img.png",
            "Exception - Invalid file type",
        ],
        [
            EMAIL_FLUIDATTACKS_HACKER,
            "3c475384-834c-47b0-ac71-a41a022e401c",
            "test-vulns.yaml",
            "Exception - Access denied or root not found",
        ],
        [
            "group_manager@gmail.com",
            "3c475384-834c-47b0-ac71-a41a022e401c",
            "test-vulns.yaml",
            "Access denied",
        ],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(id="org1"),
            ],
            groups=[
                GroupFaker(organization_id="org1"),
            ],
            findings=[
                FindingFaker(
                    id="3c475384-834c-47b0-ac71-a41a022e401c",
                    evidences=FindingEvidences(
                        evidence1=FindingEvidence(
                            description="evidence1",
                            url=("test-org-test-group-1234567890.png"),
                            modified_date=datetime.fromisoformat("2020-11-19T13:37:10+00:00"),
                        )
                    ),
                ),
                FindingFaker(id="918fbc15-2121-4c2a-83a8-dfa8748bcb2e"),
            ],
            stakeholders=[
                StakeholderFaker(email=EMAIL_FLUIDATTACKS_HACKER, role="hacker"),
                StakeholderFaker(email="group_manager@gmail.com", role="group_manager"),
            ],
            group_access=[
                GroupAccessFaker(
                    email=EMAIL_FLUIDATTACKS_HACKER,
                    state=GroupAccessStateFaker(has_access=True, role="hacker"),
                ),
                GroupAccessFaker(
                    email="group_manager@gmail.com",
                    state=GroupAccessStateFaker(has_access=True, role="group_manager"),
                ),
            ],
        )
    )
)
async def test_upload_file_mutation_errors(
    email: str,
    finding_id: str,
    file_name: str,
    error_message: str,
) -> None:
    info = GraphQLResolveInfoFaker(
        user_email=email,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_attribute, "can_report_vulnerabilities", "test-group"],
            [require_attribute, "has_asm", "test-group"],
        ],
    )

    file = await read_file(file_name)

    with raises(
        (GraphQLError, InvalidFileType, RootNotFound),
        match=error_message,
    ):
        await mutate(
            _=None,
            info=info,
            finding_id=finding_id,
            file=file,
        )


@parametrize(
    args=["email", "finding_id", "group_name"],
    cases=[
        [
            EMAIL_FLUIDATTACKS_HACKER,
            "3c475384-834c-47b0-ac71-a41a022e401c",
            "group1",
        ]
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(id="org1"),
            ],
            groups=[
                GroupFaker(name="group1", organization_id="org1"),
            ],
            roots=[
                GitRootFaker(
                    id="root1",
                    group_name="group1",
                    state=GitRootStateFaker(
                        gitignore=["bower_components/*", "node_modules/*"],
                        includes_health_check=True,
                        nickname="universe",
                        url="https://gitlab.com/fluidattacks/universe",
                    ),
                ),
            ],
            root_environment_urls=[
                RootEnvironmentUrlFaker(
                    url="https://example.com",
                    group_name="group1",
                    root_id="root1",
                    state=RootEnvironmentUrlStateFaker(),
                ),
            ],
            findings=[
                FindingFaker(
                    id="3c475384-834c-47b0-ac71-a41a022e401c",
                    group_name="group1",
                    evidences=FindingEvidences(
                        evidence1=FindingEvidence(
                            description="evidence1",
                            url=("group1-3c475384-834c-47b0-ac71-a41a022e401c-evidence1"),
                            modified_date=datetime.fromisoformat("2020-11-19T13:37:10+00:00"),
                        ),
                    ),
                    unreliable_indicators=FindingUnreliableIndicatorsFaker(
                        unreliable_status=FindingStatus.DRAFT
                    ),
                ),
            ],
            stakeholders=[
                StakeholderFaker(email=EMAIL_FLUIDATTACKS_HACKER, role="hacker"),
            ],
            group_access=[
                GroupAccessFaker(
                    group_name="group1",
                    email=EMAIL_FLUIDATTACKS_HACKER,
                    state=GroupAccessStateFaker(has_access=True, role="hacker"),
                ),
            ],
            toe_inputs=[
                ToeInputFaker(
                    component="https://example.com",
                    entry_point="pphone (en)",
                    group_name="group1",
                    root_id="root1",
                )
            ],
        )
    )
)
async def test_upload_file_finding_indicators(email: str, finding_id: str, group_name: str) -> None:
    info = GraphQLResolveInfoFaker(
        user_email=email,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_attribute, "can_report_vulnerabilities", "group1"],
            [require_attribute, "has_asm", "group1"],
        ],
    )

    file = await read_file("test-vuln-input.yaml")

    finding = await get_finding(get_new_context(), finding_id)
    assert finding.unreliable_indicators.unreliable_status == FindingStatus.DRAFT

    await mutate(
        _=None,
        info=info,
        finding_id=finding_id,
        file=file,
    )

    vulns_data = await _get_vulns(finding_id, group_name)
    assert any(item["specific"] == "pphone (en) [CVE-0000-1111]" for item in vulns_data)
    assert any(item["specific"] == "pphone (en)" for item in vulns_data)

    all_submitted = [item["state"] == VulnerabilityStateStatus.SUBMITTED for item in vulns_data]
    assert len(vulns_data) == 2
    assert all(all_submitted)

    finding_after = await get_finding(get_new_context(), finding_id)
    assert finding_after.unreliable_indicators.unreliable_status == "DRAFT"
