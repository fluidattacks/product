from integrates.api.mutations.remove_files import (
    mutate,
)
from integrates.custom_utils.groups import get_group
from integrates.dataloaders import get_new_context
from integrates.db_model.groups.types import (
    GroupFile,
)
from integrates.decorators import (
    enforce_group_level_auth_async,
    require_attribute,
    require_attribute_internal,
    require_login,
)
from integrates.resources import (
    domain as resources_domain,
)
from integrates.testing.aws import (
    IntegratesAws,
    IntegratesDynamodb,
)
from integrates.testing.fakers import (
    GraphQLResolveInfoFaker,
    GroupAccessFaker,
    GroupAccessStateFaker,
    GroupFaker,
    OrganizationFaker,
    StakeholderFaker,
)
from integrates.testing.mocks import Mock, mocks
from integrates.testing.utils import parametrize, raises

EMAIL_TEST = "customer_manager@clientapp.com"
GROUP_NAME = "group1"


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            stakeholders=[StakeholderFaker(email=EMAIL_TEST, role="customer_manager")],
            groups=[
                GroupFaker(
                    name=GROUP_NAME,
                    organization_id="org1",
                    files=[
                        GroupFile(
                            description="client test file",
                            file_name="file_test.zip",
                            modified_by=EMAIL_TEST,
                        )
                    ],
                )
            ],
            group_access=[
                GroupAccessFaker(
                    email=EMAIL_TEST,
                    group_name=GROUP_NAME,
                    state=GroupAccessStateFaker(has_access=True, role="customer_manager"),
                ),
            ],
        )
    ),
    others=[Mock(resources_domain, "remove_file", "async", None)],
)
async def test_remove_files() -> None:
    group = await get_group(get_new_context(), GROUP_NAME)

    assert group
    assert group.files
    assert group.files[0].file_name == "file_test.zip"

    info = GraphQLResolveInfoFaker(
        user_email=EMAIL_TEST,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_attribute, "has_asm", GROUP_NAME],
            [require_attribute_internal, "is_under_review", GROUP_NAME],
        ],
    )
    file_data = {
        "description": "Test",
        "fileName": "file_test.zip",
        "uploadDate": "2019-03-01 15:21",
    }
    result = await mutate(_=None, info=info, files_data_input=file_data, group_name=GROUP_NAME)
    assert result.success is True

    group = await get_group(get_new_context(), GROUP_NAME)

    assert group
    assert group.files is None


@parametrize(
    args=["email"],
    cases=[["reviewer@fluidattacks.com"], ["reattacker@test.test"]],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            stakeholders=[
                StakeholderFaker(email="reviewer@fluidattacks.com", role="hacker"),
                StakeholderFaker(email="reattacker@test.test", role="reattacker"),
            ],
            groups=[GroupFaker(name=GROUP_NAME, organization_id="org1")],
            group_access=[
                GroupAccessFaker(
                    email="reviewer@fluidattacks.com",
                    state=GroupAccessStateFaker(has_access=True, role="reviewer"),
                ),
                GroupAccessFaker(
                    email="reattacker@test.test",
                    state=GroupAccessStateFaker(has_access=True, role="reattacker"),
                ),
            ],
        ),
    )
)
async def test_remove_files_access_denied(email: str) -> None:
    info = GraphQLResolveInfoFaker(
        user_email=email,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_attribute, "has_asm", GROUP_NAME],
            [require_attribute_internal, "is_under_review", GROUP_NAME],
        ],
    )
    file_data = {
        "description": "Test",
        "fileName": "file_test.apk",
        "uploadDate": "2019-03-01 15:21",
    }

    with raises(Exception, match="Access denied"):
        await mutate(_=None, info=info, files_data_input=file_data, group_name=GROUP_NAME)
