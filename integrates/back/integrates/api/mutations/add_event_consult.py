from time import (
    time,
)

from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.custom_utils import (
    datetime as datetime_utils,
)
from integrates.custom_utils import (
    logs as logs_utils,
)
from integrates.custom_utils import (
    stakeholders as stakeholders_utils,
)
from integrates.db_model.event_comments.types import (
    EventComment,
)
from integrates.decorators import (
    concurrent_decorators,
    enforce_group_level_auth_async,
    require_advanced,
    require_asm,
    require_is_not_under_review,
    require_login,
)
from integrates.events import (
    domain as events_domain,
)
from integrates.sessions import (
    domain as sessions_domain,
)

from .payloads.types import (
    AddConsultPayload,
)
from .schema import (
    MUTATION,
)


@MUTATION.field("addEventConsult")
@concurrent_decorators(
    require_login,
    enforce_group_level_auth_async,
    require_is_not_under_review,
    require_asm,
    require_advanced,
)
async def mutate(
    _: None,
    info: GraphQLResolveInfo,
    content: str,
    event_id: str,
    group_name: str,
    parent_comment: str,
) -> AddConsultPayload:
    comment_id: str = str(round(time() * 1000))
    user_info: dict[str, str] = await sessions_domain.get_jwt_content(info.context)
    email = str(user_info["user_email"])

    comment_data = EventComment(
        event_id=event_id,
        group_name=group_name,
        parent_id=str(parent_comment),
        creation_date=datetime_utils.get_utc_now(),
        content=content,
        id=comment_id,
        email=email,
        full_name=stakeholders_utils.get_full_name(user_info),
    )
    await events_domain.add_comment(
        loaders=info.context.loaders,
        comment_data=comment_data,
        email=email,
        event_id=event_id,
        group_name=group_name,
        parent_comment=parent_comment,
    )
    logs_utils.cloudwatch_log(
        info.context,
        "Added comment to the event",
        extra={"event_id": event_id, "log_type": "Security"},
    )

    return AddConsultPayload(success=True, comment_id=str(comment_id))
