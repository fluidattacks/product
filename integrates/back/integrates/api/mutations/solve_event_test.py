from integrates.api.mutations.solve_event import mutate
from integrates.custom_utils.events import get_event
from integrates.dataloaders import get_new_context
from integrates.db_model.enums import Source
from integrates.db_model.events.enums import EventSolutionReason, EventStateStatus, EventType
from integrates.db_model.events.types import EventRequest
from integrates.db_model.finding_comments.enums import CommentType
from integrates.db_model.finding_comments.types import FindingCommentsRequest
from integrates.db_model.findings.enums import FindingVerificationStatus
from integrates.db_model.vulnerabilities.enums import VulnerabilityVerificationStatus
from integrates.decorators import (
    enforce_group_level_auth_async,
    require_asm,
    require_attribute,
    require_login,
)
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    EventFaker,
    EventStateFaker,
    FindingFaker,
    FindingVerificationFaker,
    GraphQLResolveInfoFaker,
    GroupAccessFaker,
    GroupAccessStateFaker,
    GroupFaker,
    OrganizationFaker,
    StakeholderFaker,
    VulnerabilityFaker,
    VulnerabilityStateFaker,
    VulnerabilityVerificationFaker,
)
from integrates.testing.mocks import mocks
from integrates.testing.utils import parametrize, raises

EMAIL_TEST = "admin@fluidattacks.com"
CUSTOMER_MANAGER_EMAIL = "customer_manager@fluidattacks.com"
GROUP_NAME = "group1"


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            stakeholders=[StakeholderFaker(email=EMAIL_TEST, role="admin")],
            groups=[GroupFaker(name=GROUP_NAME, organization_id="org1")],
            events=[
                EventFaker(
                    id="event1",
                    group_name=GROUP_NAME,
                    type=EventType.OTHER,
                    description="Initial description",
                    state=EventStateFaker(
                        status=EventStateStatus.OPEN,
                    ),
                ),
            ],
        ),
    )
)
async def test_solve_event() -> None:
    loaders = get_new_context()
    info = GraphQLResolveInfoFaker(
        user_email=EMAIL_TEST,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_asm],
            [require_attribute, "has_asm", GROUP_NAME],
        ],
    )
    result = await mutate(
        _=None,
        info=info,
        event_id="event1",
        group_name=GROUP_NAME,
        reason=EventSolutionReason.OTHER,
        other="Custom reason",
    )
    assert result.success is True

    solved_event = await get_event(loaders, EventRequest(event_id="event1", group_name=GROUP_NAME))
    assert solved_event.state.status == EventStateStatus.SOLVED
    assert solved_event.state.modified_by == EMAIL_TEST
    assert solved_event.solving_date == solved_event.state.modified_date


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            stakeholders=[StakeholderFaker(email=CUSTOMER_MANAGER_EMAIL)],
            groups=[GroupFaker(name=GROUP_NAME, organization_id="org1")],
            group_access=[
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email=CUSTOMER_MANAGER_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="customer_manager"),
                )
            ],
            events=[
                EventFaker(
                    id="event1",
                    group_name=GROUP_NAME,
                    type=EventType.OTHER,
                    description="Initial description",
                    state=EventStateFaker(
                        status=EventStateStatus.OPEN,
                    ),
                ),
            ],
            findings=[
                FindingFaker(
                    id="3c475384-834c-47b0-ac71-a41a022e401c",
                    group_name=GROUP_NAME,
                    verification=FindingVerificationFaker(
                        modified_by=CUSTOMER_MANAGER_EMAIL,
                        status=FindingVerificationStatus.ON_HOLD,
                        vulnerability_ids={"4dbc03e0-4cfc-4b33-9b70-bb7566c460bd"},
                        comment_id="42343435",
                    ),
                ),
            ],
            vulnerabilities=[
                VulnerabilityFaker(
                    id="4dbc03e0-4cfc-4b33-9b70-bb7566c460bd",
                    group_name=GROUP_NAME,
                    finding_id="3c475384-834c-47b0-ac71-a41a022e401c",
                    state=VulnerabilityStateFaker(
                        source=Source.MACHINE,
                    ),
                    verification=VulnerabilityVerificationFaker(
                        status=VulnerabilityVerificationStatus.ON_HOLD,
                        event_id="event1",
                    ),
                ),
            ],
        ),
    )
)
async def test_solve_event_on_hold() -> None:
    info = GraphQLResolveInfoFaker(
        user_email=CUSTOMER_MANAGER_EMAIL,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_asm],
            [require_attribute, "has_asm", GROUP_NAME],
        ],
    )
    result = await mutate(
        _=None,
        info=info,
        event_id="event1",
        group_name=GROUP_NAME,
        reason=EventSolutionReason.OTHER,
        other="Custom reason",
    )
    assert result.success is True

    loaders = get_new_context()
    finding = await loaders.finding.load("3c475384-834c-47b0-ac71-a41a022e401c")
    assert finding
    vulnerability = await loaders.vulnerability.load("4dbc03e0-4cfc-4b33-9b70-bb7566c460bd")
    assert vulnerability
    requester: str = "test1@gmail.com"
    solve_consult: str = "The reattacks are back to"
    consults = await loaders.finding_comments.load(
        FindingCommentsRequest(comment_type=CommentType.COMMENT, finding_id=finding.id)
    ) + await loaders.finding_comments.load(
        FindingCommentsRequest(comment_type=CommentType.VERIFICATION, finding_id=finding.id)
    )
    solved_event = await get_event(loaders, EventRequest(event_id="event1", group_name=GROUP_NAME))
    assert solved_event
    assert solved_event.state.status == EventStateStatus.SOLVED
    assert solved_event.state.modified_by == CUSTOMER_MANAGER_EMAIL
    assert any(solve_consult in consult.content for consult in consults)
    assert finding.verification
    assert finding.verification.status == FindingVerificationStatus.REQUESTED
    assert finding.verification.modified_by != CUSTOMER_MANAGER_EMAIL
    assert finding.verification.modified_by == requester
    assert vulnerability.unreliable_indicators.unreliable_last_reattack_requester == requester
    assert vulnerability.verification
    assert vulnerability.verification.status == VulnerabilityVerificationStatus.REQUESTED
    assert any(CUSTOMER_MANAGER_EMAIL in consult.email for consult in consults)
    assert not any(requester in consult.email for consult in consults)


@parametrize(
    args=["email"],
    cases=[
        ["user@gmail.com"],
        ["group_manager@gmail.com"],
        ["vulnerability_manager@gmail.com"],
        ["reviewer@gmail.com"],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            stakeholders=[
                StakeholderFaker(email="user@gmail.com", role="user"),
                StakeholderFaker(email="group_manager@gmail.com", role="group_manager"),
                StakeholderFaker(
                    email="vulnerability_manager@gmail.com", role="vulnerability_manager"
                ),
                StakeholderFaker(email="reviewer@gmail.com", role="reviewer"),
            ],
            groups=[GroupFaker(name=GROUP_NAME, organization_id="org1")],
            events=[
                EventFaker(
                    id="event1",
                    group_name=GROUP_NAME,
                    type=EventType.OTHER,
                    description="Initial description",
                    state=EventStateFaker(
                        status=EventStateStatus.OPEN,
                    ),
                ),
            ],
        ),
    )
)
async def test_solve_event_access_denied(email: str) -> None:
    info = GraphQLResolveInfoFaker(
        user_email=email,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_asm],
            [require_attribute, "has_asm", GROUP_NAME],
        ],
    )

    with raises(Exception, match="Access denied"):
        await mutate(
            _=None,
            info=info,
            event_id="event1",
            group_name=GROUP_NAME,
            reason=EventSolutionReason.OTHER,
            other="Other",
        )
