from integrates.api.mutations.remove_group_tag import mutate
from integrates.custom_exceptions import ErrorUpdatingGroup
from integrates.custom_utils.groups import get_group
from integrates.dataloaders import get_new_context
from integrates.decorators import (
    enforce_group_level_auth_async,
    require_attribute,
    require_attribute_internal,
    require_login,
)
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    GraphQLResolveInfoFaker,
    GroupAccessFaker,
    GroupAccessStateFaker,
    GroupFaker,
    GroupStateFaker,
    OrganizationFaker,
    StakeholderFaker,
)
from integrates.testing.mocks import mocks
from integrates.testing.utils import parametrize, raises

GROUP_NAME = "group1"


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            stakeholders=[StakeholderFaker(email="group_manager@test.test", role="group_manager")],
            groups=[
                GroupFaker(
                    name=GROUP_NAME,
                    organization_id="org1",
                    state=GroupStateFaker(tags={"test", "tag"}),
                )
            ],
            group_access=[
                GroupAccessFaker(
                    email="group_manager@test.test",
                    group_name=GROUP_NAME,
                    state=GroupAccessStateFaker(has_access=True, role="group_manager"),
                ),
            ],
        ),
    )
)
async def test_remove_group_tag() -> None:
    info = GraphQLResolveInfoFaker(
        user_email="group_manager@test.test",
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_attribute, "has_asm", GROUP_NAME],
            [require_attribute_internal, "is_under_review", GROUP_NAME],
        ],
    )

    await mutate(_=None, info=info, group_name=GROUP_NAME, tag="test")

    group = await get_group(get_new_context(), GROUP_NAME)
    assert group.state.tags == {"tag"}


@parametrize(
    args=["email"],
    cases=[
        ["hacker@test.test"],
        ["reattacker@test.test"],
        ["resourcer@test.test"],
        ["reviewer@test.test"],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            stakeholders=[
                StakeholderFaker(email="hacker@test.test", role="hacker"),
                StakeholderFaker(email="reattacker@test.test", role="hacker"),
                StakeholderFaker(email="resourcer@test.test", role="hacker"),
                StakeholderFaker(email="reviewer@test.test", role="hacker"),
            ],
            groups=[GroupFaker(name=GROUP_NAME, organization_id="org1")],
            group_access=[
                GroupAccessFaker(
                    email="reviewer@test.test",
                    group_name=GROUP_NAME,
                    state=GroupAccessStateFaker(has_access=True, role="reviewer"),
                ),
                GroupAccessFaker(
                    email="hacker@test.test",
                    group_name=GROUP_NAME,
                    state=GroupAccessStateFaker(has_access=True, role="hacker"),
                ),
                GroupAccessFaker(
                    email="reattacker@test.test",
                    group_name=GROUP_NAME,
                    state=GroupAccessStateFaker(has_access=True, role="reattacker"),
                ),
                GroupAccessFaker(
                    email="resourcer@test.test",
                    group_name=GROUP_NAME,
                    state=GroupAccessStateFaker(has_access=True, role="resourcer"),
                ),
            ],
        ),
    )
)
async def test_remove_group_tag_access_denied(email: str) -> None:
    info = GraphQLResolveInfoFaker(
        user_email=email,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_attribute, "has_asm", GROUP_NAME],
            [require_attribute_internal, "is_under_review", GROUP_NAME],
        ],
    )

    with raises(Exception, match="Access denied"):
        await mutate(_=None, info=info, group_name=GROUP_NAME, tag="a")


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            stakeholders=[StakeholderFaker(email="group_manager@test.test", role="group_manager")],
            groups=[
                GroupFaker(
                    name=GROUP_NAME, organization_id="org1", state=GroupStateFaker(tags=set())
                )
            ],
            group_access=[
                GroupAccessFaker(
                    email="group_manager@test.test",
                    group_name=GROUP_NAME,
                    state=GroupAccessStateFaker(has_access=True, role="group_manager"),
                ),
            ],
        ),
    )
)
async def test_remove_group_tag_update_error() -> None:
    info = GraphQLResolveInfoFaker(
        user_email="group_manager@test.test",
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_attribute, "has_asm", GROUP_NAME],
            [require_attribute_internal, "is_under_review", GROUP_NAME],
        ],
    )
    with raises(ErrorUpdatingGroup):
        await mutate(_=None, info=info, group_name=GROUP_NAME, tag="a")
