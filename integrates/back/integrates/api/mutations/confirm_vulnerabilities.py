from aioextensions import (
    schedule,
)
from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.api.types import (
    APP_EXCEPTIONS,
)
from integrates.custom_utils import (
    cvss as cvss_utils,
)
from integrates.custom_utils import (
    logs as logs_utils,
)
from integrates.custom_utils import (
    vulnerabilities as vulns_utils,
)
from integrates.custom_utils.findings import get_finding
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.findings.types import (
    Finding,
)
from integrates.db_model.vulnerabilities.enums import (
    VulnerabilityStateStatus,
)
from integrates.db_model.vulnerabilities.types import (
    Vulnerability,
)
from integrates.decorators import (
    concurrent_decorators,
    enforce_group_level_auth_async,
    require_login,
    require_report_vulnerabilities,
)
from integrates.findings import (
    domain as findings_domain,
)
from integrates.mailer import (
    findings as findings_mail,
)
from integrates.sessions import (
    domain as sessions_domain,
)
from integrates.unreliable_indicators.enums import (
    EntityDependency,
)
from integrates.unreliable_indicators.operations import (
    update_unreliable_indicators_by_deps,
)
from integrates.vulnerabilities import (
    domain as vulns_domain,
)

from .payloads.types import (
    SimplePayload,
)
from .schema import (
    MUTATION,
)


async def send_vulnerability_email(
    loaders: Dataloaders,
    finding: Finding,
    finding_id: str,
    reported_vulnerabilities: list[Vulnerability],
) -> None:
    severity_score = vulns_utils.get_vulnerabilities_max_score(
        finding,
        reported_vulnerabilities,
        VulnerabilityStateStatus.SUBMITTED,
    )
    severity_score_v3 = vulns_utils.get_vulnerabilities_max_score_v3(
        finding,
        reported_vulnerabilities,
        VulnerabilityStateStatus.SUBMITTED,
    )
    severity_level = cvss_utils.get_severity_level(severity_score)
    vulnerabilities_properties = await findings_domain.vulns_properties(
        loaders,
        finding_id,
        reported_vulnerabilities,
    )
    responsible = (
        set(vulnerability.hacker_email for vulnerability in reported_vulnerabilities)
        if reported_vulnerabilities
        else {finding.get_hacker_email()}
    )

    await findings_mail.send_mail_vulnerability_report(
        loaders=loaders,
        group_name=finding.group_name,
        finding_title=finding.title,
        finding_id=finding_id,
        vulnerabilities_properties=vulnerabilities_properties,
        responsible=responsible,
        severity_score=severity_score,
        severity_level=severity_level,
        severity_score_v3=severity_score_v3,
        severity_level_v3=cvss_utils.get_severity_level(severity_score_v3),
    )


async def process_vulnerabilities_confirmation(
    loaders: Dataloaders,
    finding_id: str,
    stakeholder_email: str,
    vulnerabilities: list[str],
) -> list[Vulnerability]:
    vulnerabilities_set = set(vulnerabilities)
    finding_vulnerabilities = await vulns_domain.confirm_vulnerabilities(
        loaders=loaders,
        vuln_ids=vulnerabilities_set,
        finding_id=finding_id,
        modified_by=stakeholder_email,
    )
    await update_unreliable_indicators_by_deps(
        EntityDependency.confirm_vulnerabilities,
        finding_ids=[finding_id],
        vulnerability_ids=list(vulnerabilities_set),
    )

    return finding_vulnerabilities


@MUTATION.field("confirmVulnerabilities")
@concurrent_decorators(
    require_login,
    require_report_vulnerabilities,
    enforce_group_level_auth_async,
)
async def mutate(
    _: None,
    info: GraphQLResolveInfo,
    finding_id: str,
    vulnerabilities: list[str],
) -> SimplePayload:
    try:
        loaders: Dataloaders = info.context.loaders
        user_data = await sessions_domain.get_jwt_content(info.context)
        stakeholder_email = user_data["user_email"]

        reported_vulnerabilities = await process_vulnerabilities_confirmation(
            loaders=loaders,
            finding_id=finding_id,
            stakeholder_email=stakeholder_email,
            vulnerabilities=vulnerabilities,
        )

        finding = await get_finding(loaders, finding_id)

        schedule(
            send_vulnerability_email(
                loaders,
                finding,
                finding_id,
                reported_vulnerabilities,
            ),
        )
        logs_utils.cloudwatch_log(
            info.context,
            "Confirmed vulnerabilities in the finding",
            extra={"finding_id": finding_id, "log_type": "Security"},
        )
    except APP_EXCEPTIONS:
        logs_utils.cloudwatch_log(
            info.context,
            "Attempted to confirm vulnerabilities in the finding",
            extra={
                "finding_id": finding_id,
                "vuln_ids": vulnerabilities,
                "log_type": "Security",
            },
        )
        raise

    return SimplePayload(success=True)
