from typing import Any

from integrates.api.mutations.add_secret import AddSecretArgs, mutate
from integrates.custom_exceptions import InvalidRootType
from integrates.dataloaders import get_new_context
from integrates.decorators import (
    enforce_group_level_auth_async,
    require_attribute_internal,
    require_is_not_under_review,
    require_login,
)
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    GitRootFaker,
    GraphQLResolveInfoFaker,
    GroupAccessFaker,
    GroupAccessStateFaker,
    GroupFaker,
    IPRootFaker,
    StakeholderFaker,
    UrlRootFaker,
)
from integrates.testing.mocks import mocks
from integrates.testing.utils import parametrize, raises

# Allowed

ADMIN = "admin@gmail.com"
CUSTOMER_MANAGER = "customer_manager@gmail.com"
GROUP_MANAGER = "group_manager@gmail.com"
USER = "user@gmail.com"

# Denied
ARCHITECT = "architect@gmail.com"
HACKER = "hacker@gmail.com"
REATTACKER = "reattacker@gmail.com"

GROUP_NAME = "group1"

DEFAULT_DECORATORS: list[list[Any]] = [
    [require_login],
    [enforce_group_level_auth_async],
    [require_is_not_under_review],
    [require_attribute_internal, "is_under_review", GROUP_NAME],
]


@parametrize(
    args=["user_email"],
    cases=[
        [ARCHITECT],
        [HACKER],
        [REATTACKER],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            stakeholders=[
                StakeholderFaker(email=ARCHITECT),
                StakeholderFaker(email=HACKER),
                StakeholderFaker(email=REATTACKER),
            ],
            group_access=[
                GroupAccessFaker(email=ARCHITECT, state=GroupAccessStateFaker(role="architect")),
                GroupAccessFaker(email=HACKER, state=GroupAccessStateFaker(role="hacker")),
                GroupAccessFaker(email=REATTACKER, state=GroupAccessStateFaker(role="reattacker")),
            ],
        )
    )
)
async def test_add_secret_access_denied(
    user_email: str,
) -> None:
    kwargs = {
        "description": "description",
        "group_name": "group_name",
        "key": "key",
        "resource_id": "resource_id",
        "resource_type": "URL",
        "value": "value",
    }
    info = GraphQLResolveInfoFaker(user_email=user_email, decorators=DEFAULT_DECORATORS)
    with raises(Exception, match="Access denied"):
        await mutate(_=None, info=info, **kwargs)


@parametrize(
    args=["args", "user_email"],
    cases=[
        [
            {
                "description": "description",
                "group_name": GROUP_NAME,
                "key": "key",
                "resource_id": "urlroot1",
                "resource_type": "ROOT",
                "value": "value",
            },
            ADMIN,
        ],
        [
            {
                "description": None,
                "group_name": GROUP_NAME,
                "key": "key",
                "resource_id": "gitroot1",
                "resource_type": "ROOT",
                "value": "value",
            },
            CUSTOMER_MANAGER,
        ],
        [
            {
                "description": None,
                "group_name": GROUP_NAME,
                "key": "key",
                "resource_id": "iproot1",
                "resource_type": "ROOT",
                "value": "value",
            },
            USER,
        ],
        [
            {
                "description": None,
                "group_name": GROUP_NAME,
                "key": "key",
                "resource_id": "gitroot1",
                "resource_type": "ROOT",
                "value": "value",
            },
            GROUP_MANAGER,
        ],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            roots=[
                UrlRootFaker(root_id="urlroot1", group_name=GROUP_NAME),
                GitRootFaker(id="gitroot1", group_name=GROUP_NAME),
                IPRootFaker(id="iproot1", group_name=GROUP_NAME),
            ],
            groups=[GroupFaker(name=GROUP_NAME)],
            stakeholders=[
                StakeholderFaker(email=ADMIN),
                StakeholderFaker(email=CUSTOMER_MANAGER),
                StakeholderFaker(email=USER),
                StakeholderFaker(email=GROUP_MANAGER),
            ],
            group_access=[
                GroupAccessFaker(
                    email=ADMIN, group_name=GROUP_NAME, state=GroupAccessStateFaker(role="admin")
                ),
                GroupAccessFaker(
                    email=CUSTOMER_MANAGER,
                    group_name=GROUP_NAME,
                    state=GroupAccessStateFaker(role="customer_manager"),
                ),
                GroupAccessFaker(
                    email=GROUP_MANAGER,
                    group_name=GROUP_NAME,
                    state=GroupAccessStateFaker(role="group_manager"),
                ),
                GroupAccessFaker(
                    email=USER, group_name=GROUP_NAME, state=GroupAccessStateFaker(role="user")
                ),
            ],
        )
    )
)
async def test_add_secret(
    args: AddSecretArgs,
    user_email: str,
) -> None:
    info = GraphQLResolveInfoFaker(user_email=user_email, decorators=DEFAULT_DECORATORS)
    await mutate(_=None, info=info, **args)
    loaders = get_new_context()
    secrets = await loaders.root_secrets.load(args["resource_id"])
    assert len(secrets) > 0

    assert secrets[0].key == args["key"]
    assert secrets[0].value == args["value"]


@parametrize(
    args=["args", "user_email"],
    cases=[
        [
            {
                "description": "description",
                "group_name": GROUP_NAME,
                "key": "key",
                "resource_id": "urlroot1",
                "resource_type": "URL",
                "value": "value",
            },
            ADMIN,
        ],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            roots=[UrlRootFaker(root_id="urlroot1", group_name=GROUP_NAME)],
            groups=[GroupFaker(name=GROUP_NAME)],
            stakeholders=[StakeholderFaker(email=ADMIN)],
            group_access=[
                GroupAccessFaker(
                    email=ADMIN, group_name=GROUP_NAME, state=GroupAccessStateFaker(role="admin")
                ),
            ],
        )
    )
)
async def test_add_secret_fails(
    args: AddSecretArgs,
    user_email: str,
) -> None:
    info = GraphQLResolveInfoFaker(user_email=user_email, decorators=DEFAULT_DECORATORS)
    with raises(InvalidRootType):
        await mutate(_=None, info=info, **args)
