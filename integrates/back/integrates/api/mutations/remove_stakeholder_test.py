from integrates.api.mutations.remove_stakeholder import mutate
from integrates.decorators import require_login
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import GraphQLResolveInfoFaker, StakeholderFaker
from integrates.testing.mocks import mocks


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            stakeholders=[StakeholderFaker(email="test@example.com")],
        )
    )
)
async def test_remove_stakeholder() -> None:
    info = GraphQLResolveInfoFaker(
        user_email="test@example.com",
        decorators=[[require_login]],
    )

    result = await mutate(_=None, info=info)
    assert result.success is True
