from typing import Any

from integrates.api.mutations.remove_environment_url import mutate
from integrates.dataloaders import get_new_context
from integrates.db_model.roots.types import RootEnvironmentUrlsRequest
from integrates.decorators import (
    enforce_group_level_auth_async,
    require_attribute,
    require_attribute_internal,
    require_is_not_under_review,
    require_login,
    require_service_white,
)
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    GitRootFaker,
    GraphQLResolveInfoFaker,
    GroupAccessFaker,
    GroupAccessStateFaker,
    GroupFaker,
    RootEnvironmentUrlFaker,
    StakeholderFaker,
)
from integrates.testing.mocks import mocks
from integrates.testing.utils import parametrize, raises

GROUP_NAME = "group_name"

# Allowed
ADMIN = "admin@gmail.com"
CUSTOMER_MANAGER = "customer_manager@gmail.com"
RESOURCER = "resourcer@gmail.com"
GROUP_MANAGER = "group_manager@gmail.com"
USER = "user@gmail.com"
VULNERABILITY_MANAGER = "vulnerability_manager@gmail.com"

# Not allowed
HACKER = "hacker@gmail.com"
REATTACKER = "reattacker@gmail.com"
ARCHITECT = "architect@gmail.com"


DEFAULT_DECORATORS: list[list[Any]] = [
    [require_login],
    [enforce_group_level_auth_async],
    [require_is_not_under_review],
    [require_service_white],
    [require_attribute_internal, "is_under_review", GROUP_NAME],
    [require_attribute, "has_service_white", GROUP_NAME],
]


@parametrize(
    args=["user_email"],
    cases=[[HACKER], [REATTACKER], [ARCHITECT]],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            groups=[GroupFaker(name=GROUP_NAME)],
            stakeholders=[
                StakeholderFaker(email=ADMIN),
            ],
            group_access=[
                GroupAccessFaker(
                    email=ADMIN, group_name=GROUP_NAME, state=GroupAccessStateFaker(role="admin")
                ),
            ],
        )
    )
)
async def test_remove_git_environment_url_access_denied(
    user_email: str,
) -> None:
    info = GraphQLResolveInfoFaker(user_email=user_email, decorators=DEFAULT_DECORATORS)
    with raises(Exception, match="Access denied"):
        await mutate(None, info=info, group_name=GROUP_NAME, root_id="root", url_id="url")


@parametrize(
    args=["user_email", "group_name", "root_id", "url_id"],
    cases=[
        [ADMIN, GROUP_NAME, "root1", "url1"],
        [CUSTOMER_MANAGER, GROUP_NAME, "root1", "url1"],
        [RESOURCER, GROUP_NAME, "root1", "url1"],
        [GROUP_MANAGER, GROUP_NAME, "root1", "url1"],
        [USER, GROUP_NAME, "root1", "url1"],
        [VULNERABILITY_MANAGER, GROUP_NAME, "root1", "url1"],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            groups=[GroupFaker(name=GROUP_NAME)],
            roots=[GitRootFaker(id="root1", group_name=GROUP_NAME)],
            root_environment_urls=[
                RootEnvironmentUrlFaker(
                    url="test.apk",
                    id="url1",
                    root_id="root1",
                    group_name=GROUP_NAME,
                ),
            ],
            stakeholders=[
                StakeholderFaker(email=ADMIN),
                StakeholderFaker(email=CUSTOMER_MANAGER),
                StakeholderFaker(email=RESOURCER),
                StakeholderFaker(email=GROUP_MANAGER),
                StakeholderFaker(email=USER),
                StakeholderFaker(email=VULNERABILITY_MANAGER),
            ],
            group_access=[
                GroupAccessFaker(
                    email=ADMIN, group_name=GROUP_NAME, state=GroupAccessStateFaker(role="admin")
                ),
                GroupAccessFaker(
                    email=CUSTOMER_MANAGER,
                    group_name=GROUP_NAME,
                    state=GroupAccessStateFaker(role="customer_manager"),
                ),
                GroupAccessFaker(
                    email=RESOURCER,
                    group_name=GROUP_NAME,
                    state=GroupAccessStateFaker(role="resourcer"),
                ),
                GroupAccessFaker(
                    email=GROUP_MANAGER,
                    group_name=GROUP_NAME,
                    state=GroupAccessStateFaker(role="group_manager"),
                ),
                GroupAccessFaker(
                    email=USER, group_name=GROUP_NAME, state=GroupAccessStateFaker(role="user")
                ),
                GroupAccessFaker(
                    email=VULNERABILITY_MANAGER,
                    group_name=GROUP_NAME,
                    state=GroupAccessStateFaker(role="vulnerability_manager"),
                ),
            ],
        )
    )
)
async def test_remove_git_environment_url(
    user_email: str,
    group_name: str,
    root_id: str,
    url_id: str,
) -> None:
    info = GraphQLResolveInfoFaker(user_email=user_email, decorators=DEFAULT_DECORATORS)
    await mutate(None, info=info, group_name=group_name, root_id=root_id, url_id=url_id)
    loaders = get_new_context()
    url_env = await loaders.root_environment_urls.load(
        RootEnvironmentUrlsRequest(root_id=root_id, group_name=group_name)
    )
    assert len(url_env) == 0
