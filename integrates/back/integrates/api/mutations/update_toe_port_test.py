from integrates.api.mutations.update_toe_port import mutate
from integrates.custom_exceptions import ToePortNotFound
from integrates.dataloaders import Dataloaders, get_new_context
from integrates.db_model.toe_ports.types import RootToePortsRequest
from integrates.decorators import enforce_group_level_auth_async, require_attribute, require_login
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    GraphQLResolveInfoFaker,
    GroupAccessFaker,
    GroupAccessStateFaker,
    GroupFaker,
    IPRootFaker,
    IPRootStateFaker,
    OrganizationFaker,
    StakeholderFaker,
    ToePortFaker,
    ToePortStateFaker,
)
from integrates.testing.mocks import mocks
from integrates.testing.utils import freeze_time, parametrize, raises

from .payloads.types import UpdateToePortPayload

GROUP_NAME = "group1"
ROOT_ID = "root1"
ADRESS = "192.180.0.0"
PORT = "8080"


@parametrize(
    args=["user_email"],
    cases=[
        ["hacker@gmail.com"],
        ["reattacker@fluidattacks.com"],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(id="org1"),
            ],
            stakeholders=[
                StakeholderFaker(email="hacker@gmail.com", role="hacker"),
                StakeholderFaker(email="reattacker@fluidattacks.com", role="reattacker"),
            ],
            groups=[
                GroupFaker(name=GROUP_NAME, organization_id="org1"),
            ],
            group_access=[
                GroupAccessFaker(
                    email="reattacker@fluidattacks.com",
                    group_name=GROUP_NAME,
                    state=GroupAccessStateFaker(has_access=True, role="reattacker"),
                ),
                GroupAccessFaker(
                    email="hacker@gmail.com",
                    group_name=GROUP_NAME,
                    state=GroupAccessStateFaker(has_access=True, role="hacker"),
                ),
            ],
            roots=[
                IPRootFaker(
                    group_name=GROUP_NAME,
                    id=ROOT_ID,
                    state=IPRootStateFaker(
                        nickname="back",
                    ),
                ),
            ],
            toe_ports=[
                ToePortFaker(
                    address=ADRESS,
                    port=PORT,
                    group_name=GROUP_NAME,
                    root_id=ROOT_ID,
                    state=ToePortStateFaker(
                        be_present=False,
                        attacked_by="other_hacker@fluidattacks.com",
                    ),
                ),
            ],
        ),
    ),
)
@freeze_time("2024-10-14T00:00:00")
async def test_update_toe_port(user_email: str) -> None:
    loaders: Dataloaders = get_new_context()
    toe_ports = await loaders.root_toe_ports.load_nodes(
        RootToePortsRequest(group_name=GROUP_NAME, root_id=ROOT_ID),
    )

    assert len(toe_ports) == 1
    assert toe_ports[0].address == ADRESS
    assert toe_ports[0].state.attacked_by == "other_hacker@fluidattacks.com"
    assert toe_ports[0].state.be_present is False

    info = GraphQLResolveInfoFaker(
        user_email=user_email,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_attribute, "has_asm", GROUP_NAME],
        ],
    )

    result = await mutate(
        _parent=None,
        info=info,
        be_present=True,
        address=ADRESS,
        port=PORT,
        group_name=GROUP_NAME,
        has_recent_attack=True,
        root_id=ROOT_ID,
    )
    assert result == UpdateToePortPayload(
        address=ADRESS,
        port=PORT,
        group_name=GROUP_NAME,
        root_id=ROOT_ID,
        success=True,
    )

    toe_ports_updated = await get_new_context().root_toe_ports.load_nodes(
        RootToePortsRequest(group_name=GROUP_NAME, root_id=ROOT_ID),
    )

    assert len(toe_ports_updated) == 1
    assert toe_ports_updated[0].address == ADRESS
    assert toe_ports_updated[0].state.be_present is True
    toe_port_attacked_at = toe_ports_updated[0].state.attacked_at
    assert toe_port_attacked_at
    assert (toe_port_attacked_at.year, toe_port_attacked_at.month, toe_port_attacked_at.day) == (
        2024,
        10,
        14,
    )
    assert toe_ports_updated[0].state.attacked_by == user_email


@parametrize(
    args=["user_email"],
    cases=[
        ["customer_manager@gmail.com"],
        ["user@gmail.com"],
        ["reviewer@gmail.com"],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(id="org1"),
            ],
            stakeholders=[
                StakeholderFaker(email="customer_manager@gmail.com", role="customer_manager"),
                StakeholderFaker(email="user@gmail.com", role="user"),
                StakeholderFaker(email="reviewer@gmail.com", role="reviewer"),
            ],
            groups=[
                GroupFaker(name=GROUP_NAME, organization_id="org1"),
            ],
            group_access=[
                GroupAccessFaker(
                    email="customer_manager@gmail.com",
                    group_name=GROUP_NAME,
                    state=GroupAccessStateFaker(has_access=True, role="customer_manager"),
                ),
                GroupAccessFaker(
                    email="user@gmail.com",
                    group_name=GROUP_NAME,
                    state=GroupAccessStateFaker(has_access=True, role="user"),
                ),
                GroupAccessFaker(
                    email="reviewer@gmail.com",
                    group_name=GROUP_NAME,
                    state=GroupAccessStateFaker(has_access=True, role="reviewer"),
                ),
            ],
            roots=[
                IPRootFaker(
                    group_name=GROUP_NAME,
                    id=ROOT_ID,
                    state=IPRootStateFaker(
                        nickname="back",
                    ),
                ),
            ],
            toe_ports=[
                ToePortFaker(
                    address=ADRESS,
                    port=PORT,
                    group_name=GROUP_NAME,
                    root_id=ROOT_ID,
                    state=ToePortStateFaker(
                        be_present=False,
                        attacked_by="other_hacker@fluidattacks.com",
                    ),
                ),
            ],
        ),
    )
)
async def test_update_toe_port_access_denied(user_email: str) -> None:
    info = GraphQLResolveInfoFaker(
        user_email=user_email,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_attribute, "has_asm", GROUP_NAME],
        ],
    )
    with raises(Exception, match="Access denied"):
        await mutate(
            _parent=None,
            info=info,
            comments="Updated toe line in group",
            group_name=GROUP_NAME,
            root_id=ROOT_ID,
            filename="test.py",
            attacked_lines=180,
        )


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            stakeholders=[StakeholderFaker(email="hacker@gmail.com", role="hacker")],
            groups=[GroupFaker(name=GROUP_NAME, organization_id="org1")],
            group_access=[
                GroupAccessFaker(
                    email="hacker@gmail.com",
                    group_name=GROUP_NAME,
                    state=GroupAccessStateFaker(has_access=True, role="hacker"),
                ),
            ],
            roots=[
                IPRootFaker(
                    group_name=GROUP_NAME,
                    id=ROOT_ID,
                    state=IPRootStateFaker(
                        nickname="back",
                    ),
                ),
            ],
            toe_ports=[
                ToePortFaker(
                    address=ADRESS,
                    port=PORT,
                    group_name=GROUP_NAME,
                    root_id=ROOT_ID,
                    state=ToePortStateFaker(
                        be_present=False,
                        attacked_by="other_hacker@fluidattacks.com",
                    ),
                ),
            ],
        ),
    ),
)
async def test_update_toe_port_not_found() -> None:
    info = GraphQLResolveInfoFaker(
        user_email="hacker@gmail.com",
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_attribute, "has_asm", GROUP_NAME],
        ],
    )
    with raises(ToePortNotFound):
        await mutate(
            _parent=None,
            info=info,
            group_name=GROUP_NAME,
            root_id=ROOT_ID,
            address=ADRESS,
            port="43",
            be_present=True,
            has_recent_attack=True,
        )
