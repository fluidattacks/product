from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.custom_utils import (
    roots as roots_utils,
)
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.roots.enums import (
    RootStatus,
)
from integrates.db_model.roots.types import (
    GitRoot,
    IPRoot,
    URLRoot,
)
from integrates.decorators import (
    concurrent_decorators,
    enforce_group_level_auth_async,
    require_is_not_under_review,
    require_login,
    require_service_black,
    require_service_white,
)
from integrates.roots import (
    domain as roots_domain,
)
from integrates.sessions import (
    domain as sessions_domain,
)
from integrates.unreliable_indicators.enums import (
    EntityDependency,
)
from integrates.unreliable_indicators.operations import (
    update_unreliable_indicators_by_deps,
)

from .payloads.types import (
    SimplePayloadMessage,
)
from .schema import (
    MUTATION,
)


@require_service_white
async def activate_git_root(
    *,
    info: GraphQLResolveInfo,
    root: GitRoot,
    user_email: str,
    **kwargs: str,
) -> None:
    await roots_domain.activate_root(
        loaders=info.context.loaders,
        group_name=kwargs["group_name"],
        root=root,
        email=user_email,
    )


@require_service_black
async def activate_ip_or_url_root(
    *,
    info: GraphQLResolveInfo,
    root: IPRoot | URLRoot,
    user_email: str,
    **kwargs: str,
) -> None:
    await roots_domain.activate_root(
        loaders=info.context.loaders,
        group_name=kwargs["group_name"],
        root=root,
        email=user_email,
    )


@MUTATION.field("activateRoot")
@concurrent_decorators(
    require_login,
    enforce_group_level_auth_async,
    require_is_not_under_review,
)
async def mutate(
    _parent: None,
    info: GraphQLResolveInfo,
    **kwargs: str,
) -> SimplePayloadMessage:
    user_info: dict[str, str] = await sessions_domain.get_jwt_content(info.context)
    user_email: str = user_info["user_email"]
    loaders: Dataloaders = info.context.loaders
    group_name = kwargs["group_name"]
    root_id = kwargs["id"]
    root = await roots_utils.get_root(loaders, root_id, group_name)

    if root.state.status == RootStatus.ACTIVE:
        return SimplePayloadMessage(success=True, message="Root is already active")

    if isinstance(root, GitRoot):
        await activate_git_root(info=info, root=root, user_email=user_email, **kwargs)
    elif isinstance(root, (IPRoot, URLRoot)):
        await activate_ip_or_url_root(info=info, root=root, user_email=user_email, **kwargs)

    await update_unreliable_indicators_by_deps(
        EntityDependency.activate_root,
        root_ids=[(root.group_name, root.id)],
    )

    await roots_domain.queue_actions_on_status_change(
        loaders=loaders,
        email=user_email,
        group_name=group_name,
        root=await roots_utils.get_root(loaders, root_id, group_name, clear_loader=True),
        new_status=RootStatus.ACTIVE,
    )

    return SimplePayloadMessage(success=True, message="Root was activated")
