from typing import Any, NamedTuple

from integrates.db_model.findings.types import Finding
from integrates.db_model.groups.types import Group
from integrates.db_model.mailmap.types import MailmapEntry, MailmapEntryWithSubentries
from integrates.db_model.organizations.types import Organization
from integrates.db_model.stakeholders.types import Stakeholder


class AddOrganizationPayload(NamedTuple):
    success: bool
    organization: Organization


class GrantStakeholderAccessPayload(NamedTuple):
    success: bool
    granted_stakeholder: Stakeholder


class AddConsultPayload(NamedTuple):
    success: bool
    comment_id: str


class AddStakeholderPayload(NamedTuple):
    success: bool
    email: str


class AddRootPayload(NamedTuple):
    root_id: str
    success: bool


class AddEventPayload(NamedTuple):
    event_id: str
    success: bool


class AddEnvironmentUrlPayload(NamedTuple):
    url_id: str
    success: bool


class AddRootDockerImagePayload(NamedTuple):
    uri: str
    sbom_job_queued: bool
    success: bool


class DownloadFilePayload(NamedTuple):
    success: bool
    url: str


class UpdateStakeholderPayload(NamedTuple):
    success: bool
    modified_stakeholder: dict[str, Any]


class UpdateToeInputPayload(NamedTuple):
    component: str
    entry_point: str
    group_name: str
    root_id: str
    success: bool


class UpdateToeLinesPayload(NamedTuple):
    filename: str
    group_name: str
    root_id: str
    success: bool


class UpdateToePortPayload(NamedTuple):
    address: str
    port: str
    group_name: str
    root_id: str
    success: bool


class ExecuteMachinePayload(NamedTuple):
    success: bool
    pipeline_url: str


class RemoveStakeholderAccessPayload(NamedTuple):
    success: bool
    removed_email: str


class SimpleFindingPayload(NamedTuple):
    success: bool
    finding: Finding


class SimpleGroupPayload(NamedTuple):
    success: bool
    group: Group


class SimplePayload(NamedTuple):
    success: bool


class SimplePayloadMessage(NamedTuple):
    success: bool
    message: str


class UploadGitRootFilePayload(NamedTuple):
    error_lines: list[str]
    message: str
    success: bool
    total_success: int
    total_errors: int


class UpdateAccessTokenPayload(NamedTuple):
    success: bool
    session_jwt: str


class SignPostUrlsPayload(NamedTuple):
    success: bool
    url: dict[str, Any]


class MailmapEntryPayload(NamedTuple):
    success: bool
    entry: MailmapEntry


class MailmapEntryWithSubentriesPayload(NamedTuple):
    success: bool
    entry_with_subentries: MailmapEntryWithSubentries


class ImportMailmapPayload(NamedTuple):
    success: bool
    success_count: int
    exceptions_messages: list[str]
