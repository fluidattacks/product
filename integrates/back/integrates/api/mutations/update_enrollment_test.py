from datetime import datetime
from typing import Any

import integrates.decorators
from integrates.api.mutations.payloads.types import (
    SimplePayload,
)
from integrates.api.mutations.update_enrollment import (
    mutate,
)
from integrates.dataloaders import (
    get_new_context,
)
from integrates.db_model.groups.enums import (
    GroupManaged,
)
from integrates.db_model.trials.enums import (
    TrialReason,
)
from integrates.decorators import (
    require_corporate_email,
    require_login,
)
from integrates.testing.aws import (
    IntegratesAws,
    IntegratesDynamodb,
)
from integrates.testing.fakers import (
    GraphQLResolveInfoFaker,
    GroupAccessFaker,
    GroupFaker,
    GroupStateFaker,
    OrganizationAccessFaker,
    OrganizationAccessStateFaker,
    OrganizationFaker,
    StakeholderFaker,
    TrialFaker,
    random_uuid,
)
from integrates.testing.mocks import Mock, mocks
from integrates.testing.utils import freeze_time

ORG_ID = random_uuid()
EMAIL_TRIAL = "company@company.com"

DEFAULT_DECORATORS: list[list[Any]] = [
    [require_login],
    [require_corporate_email],
]


@freeze_time("2022-10-21T15:58:31.280182+00:00")
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            groups=[
                GroupFaker(
                    organization_id=ORG_ID,
                    state=GroupStateFaker(
                        has_essential=True,
                        has_advanced=False,
                        managed=GroupManaged.TRIAL,
                    ),
                )
            ],
            group_access=[GroupAccessFaker(email=EMAIL_TRIAL)],
            organizations=[OrganizationFaker(id=ORG_ID)],
            organization_access=[
                OrganizationAccessFaker(
                    email=EMAIL_TRIAL,
                    organization_id=ORG_ID,
                    state=OrganizationAccessStateFaker(has_access=True),
                )
            ],
            stakeholders=[
                StakeholderFaker(email=EMAIL_TRIAL, is_registered=True),
            ],
            trials=[
                TrialFaker(
                    email=EMAIL_TRIAL,
                    completed=False,
                    extension_date=None,
                    extension_days=0,
                    start_date=datetime.fromisoformat("2022-10-21T15:58:31.280182+00:00"),
                    reason=None,
                )
            ],
        ),
    ),
    others=[Mock(integrates.decorators, "is_personal_email", "async", False)],
)
async def test_update_enrollment() -> None:
    trial_reason = TrialReason.COMPLIANCE
    info = GraphQLResolveInfoFaker(
        user_email=EMAIL_TRIAL,
        decorators=DEFAULT_DECORATORS,
    )
    result: SimplePayload = await mutate(
        _=None,
        info=info,
        reason=trial_reason,
    )
    assert result.success

    loaders = get_new_context()
    trial = await loaders.trial.load(EMAIL_TRIAL)
    stakeholder = await loaders.stakeholder.load(EMAIL_TRIAL)
    assert stakeholder
    assert trial
    assert trial.reason == trial_reason
