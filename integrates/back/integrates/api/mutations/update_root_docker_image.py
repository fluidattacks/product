from typing import (
    TypedDict,
    Unpack,
)

from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.api.types import (
    APP_EXCEPTIONS,
)
from integrates.custom_utils import (
    logs as logs_utils,
)
from integrates.custom_utils.groups import get_group
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.decorators import (
    concurrent_decorators,
    enforce_group_level_auth_async,
    require_is_not_under_review,
    require_login,
    require_service_white,
)
from integrates.roots import (
    domain as roots_domain,
)
from integrates.sessions.domain import (
    get_jwt_content,
)

from .payloads.types import (
    SimplePayload,
)
from .schema import (
    MUTATION,
)


class RootDockerImageArgs(TypedDict):
    credentials: dict[str, str]
    group_name: str
    root_id: str
    uri: str


@MUTATION.field("updateRootDockerImage")
@concurrent_decorators(
    require_login,
    enforce_group_level_auth_async,
    require_is_not_under_review,
    require_service_white,
)
async def mutate(
    _parent: None,
    info: GraphQLResolveInfo,
    **kwargs: Unpack[RootDockerImageArgs],
) -> SimplePayload:
    try:
        loaders: Dataloaders = info.context.loaders
        user_info = await get_jwt_content(info.context)
        group_name = kwargs["group_name"]
        credential_id = kwargs.get("credentials", {}).get("id")
        user_email = user_info["user_email"]
        uri = kwargs["uri"]
        root_id = kwargs["root_id"]
        group = await get_group(loaders, group_name.lower())
        await roots_domain.update_root_docker_image(
            loaders=loaders,
            group=group,
            root_id=root_id,
            credential_id=credential_id,
            uri=uri,
            user_email=user_email,
        )
        logs_utils.cloudwatch_log(
            info.context,
            "Updated Docker image",
            extra={
                "group_name": group_name,
                "root_id": root_id,
                "log_type": "Security",
            },
        )
    except APP_EXCEPTIONS:
        logs_utils.cloudwatch_log(
            info.context,
            "Attempted to update Docker image in the root",
            extra={
                "uri": uri,
                "root_id": root_id,
                "log_type": "Security",
            },
        )
        raise

    return SimplePayload(success=True)
