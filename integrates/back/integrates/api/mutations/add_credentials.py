from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.batch import (
    dal as batch_dal,
)
from integrates.batch.enums import (
    Action,
    IntegratesBatchQueue,
)
from integrates.custom_exceptions import (
    InvalidParameter,
)
from integrates.custom_utils import (
    logs as logs_utils,
)
from integrates.custom_utils import (
    validations as validation_utils,
)
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.enums import (
    CredentialType,
)
from integrates.decorators import (
    concurrent_decorators,
    enforce_organization_level_auth_async,
    require_login,
)
from integrates.organizations import (
    domain as orgs_domain,
)
from integrates.organizations.types import (
    CredentialAttributesToAdd,
)
from integrates.sessions import (
    domain as sessions_domain,
)

from .payloads.types import (
    SimplePayload,
)
from .schema import (
    MUTATION,
)


@MUTATION.field("addCredentials")
@concurrent_decorators(
    require_login,
    enforce_organization_level_auth_async,
)
async def mutate(
    _: None,
    info: GraphQLResolveInfo,
    organization_id: str,
    credentials: dict[str, str],
) -> SimplePayload:
    loaders: Dataloaders = info.context.loaders
    user_data = await sessions_domain.get_jwt_content(info.context)
    user_email = user_data["user_email"]
    is_pat: bool = bool(credentials.get("is_pat", False))
    if "name" not in credentials:
        raise InvalidParameter("name")
    if "type" not in credentials:
        raise InvalidParameter("type")
    if is_pat:
        orgs_domain.verify_azure_org(azure_organization=credentials.get("azure_organization"))
    if not is_pat and "azure_organization" in credentials:
        raise InvalidParameter("azure_organization")

    name: str = credentials["name"]
    validation_utils.validate_space_field(name)

    credentials_id: str = await orgs_domain.add_credentials(
        loaders,
        CredentialAttributesToAdd(
            arn=credentials.get("arn"),
            name=name,
            key=credentials.get("key"),
            token=credentials.get("token"),
            type=CredentialType[credentials["type"]],
            user=credentials.get("user"),
            password=credentials.get("password"),
            is_pat=is_pat,
            azure_organization=credentials["azure_organization"] if is_pat else None,
        ),
        organization_id,
        user_email,
    )

    logs_utils.cloudwatch_log(
        info.context,
        "Added credentials to the organization",
        extra={"organization_id": organization_id, "log_type": "Security"},
    )

    if is_pat:
        await batch_dal.put_action(
            action=Action.UPDATE_ORGANIZATION_REPOSITORIES,
            queue=IntegratesBatchQueue.SMALL,
            additional_info={"credentials_id": credentials_id},
            entity=organization_id.lower().lstrip("org#"),
            attempt_duration_seconds=14400,
            subject=user_email,
        )

    return SimplePayload(success=True)
