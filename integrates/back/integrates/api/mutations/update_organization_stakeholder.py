import logging
import logging.config

from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.custom_exceptions import (
    StakeholderNotFound,
)
from integrates.custom_utils import (
    logs as logs_utils,
)
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.organization_access.types import (
    OrganizationAccessRequest,
)
from integrates.decorators import (
    concurrent_decorators,
    enforce_organization_level_auth_async,
    require_login,
    require_organization_access,
)
from integrates.organizations import (
    domain as orgs_domain,
)
from integrates.organizations import (
    utils as orgs_utils,
)
from integrates.sessions import (
    domain as sessions_domain,
)

from .payloads.types import (
    UpdateStakeholderPayload,
)
from .schema import (
    MUTATION,
)

# Constants
LOGGER = logging.getLogger(__name__)


@MUTATION.field("updateOrganizationStakeholder")
@concurrent_decorators(
    require_login,
    require_organization_access,
    enforce_organization_level_auth_async,
)
async def mutate(_: None, info: GraphQLResolveInfo, **parameters: str) -> UpdateStakeholderPayload:
    loaders: Dataloaders = info.context.loaders
    organization_id: str = str(parameters.get("organization_id"))
    organization = await orgs_utils.get_organization(loaders, organization_id)
    requester_data = await sessions_domain.get_jwt_content(info.context)
    requester_email = requester_data["user_email"]

    user_email: str = str(parameters.get("user_email"))
    new_role: str = str(parameters.get("role")).lower()

    if (
        organization_access := await loaders.organization_access.load(
            OrganizationAccessRequest(organization_id=organization_id, email=user_email),
        )
    ) is None:
        raise StakeholderNotFound()

    # Validate role requirements before changing anything
    await orgs_domain.update_stakeholder_role(
        loaders=loaders,
        user_email=user_email,
        organization_id=organization_id,
        organization_access=organization_access,
        new_role=new_role,
        modified_by=requester_email,
    )
    logs_utils.cloudwatch_log(
        info.context,
        "Updated the stakeholder information in the organization",
        extra={
            "organization_id": organization_id,
            "organization_name": organization.name,
            "requester_email": requester_email,
            "updated_stakeholder": user_email,
            "log_type": "Security",
        },
    )

    return UpdateStakeholderPayload(success=True, modified_stakeholder={"email": user_email})
