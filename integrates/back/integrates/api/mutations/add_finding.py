from typing import (
    TypedDict,
    Unpack,
)

from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.api.mutations.schema import (
    MUTATION,
)
from integrates.api.types import (
    APP_EXCEPTIONS,
)
from integrates.custom_utils import (
    logs as logs_utils,
)
from integrates.custom_utils import (
    requests as requests_utils,
)
from integrates.custom_utils.validations import (
    check_and_set_min_time_to_remediate,
)
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.decorators import (
    concurrent_decorators,
    enforce_group_level_auth_async,
    require_asm,
    require_is_not_under_review,
    require_login,
    require_report_vulnerabilities,
)
from integrates.findings import (
    domain as findings_domain,
)
from integrates.findings.types import (
    FindingAttributesToAdd,
)
from integrates.sessions import (
    domain as sessions_domain,
)

from .payloads.types import (
    SimplePayload,
)


class AddFindingArgs(TypedDict):
    attack_vector_description: str
    cvss_vector: str
    description: str
    group_name: str
    recommendation: str
    title: str
    threat: str
    unfulfilled_requirements: list[str]
    min_time_to_remediate: int | None
    cvss_4_vector: str


@MUTATION.field("addFinding")
@concurrent_decorators(
    require_login,
    enforce_group_level_auth_async,
    require_asm,
    require_is_not_under_review,
    require_report_vulnerabilities,
)
async def mutate(
    _parent: None,
    info: GraphQLResolveInfo,
    **kwargs: Unpack[AddFindingArgs],
) -> SimplePayload:
    loaders: Dataloaders = info.context.loaders
    stakeholder_info = await sessions_domain.get_jwt_content(info.context)
    stakeholder_email = stakeholder_info["user_email"]
    group_name = kwargs["group_name"]
    title = kwargs["title"]
    try:
        await findings_domain.add_finding(
            loaders=loaders,
            group_name=group_name,
            stakeholder_email=stakeholder_email,
            attributes=FindingAttributesToAdd(
                attack_vector_description=kwargs["attack_vector_description"],
                cvss_vector=kwargs["cvss_vector"],
                cvss4_vector=kwargs.get("cvss_4_vector", ""),
                description=kwargs["description"],
                min_time_to_remediate=check_and_set_min_time_to_remediate(
                    kwargs.get("min_time_to_remediate"),
                ),
                recommendation=kwargs["recommendation"],
                source=requests_utils.get_source_new(info.context),
                threat=kwargs["threat"],
                title=title,
                unfulfilled_requirements=kwargs["unfulfilled_requirements"],
            ),
        )
        logs_utils.cloudwatch_log(
            info.context,
            "Added finding in the group",
            extra={
                "group_name": group_name,
                "finding_title": title,
                "log_type": "Security",
            },
        )
    except APP_EXCEPTIONS:
        logs_utils.cloudwatch_log(
            info.context,
            "Attempted to add finding in the group",
            extra={
                "group_name": group_name,
                "finding_title": title,
                "log_type": "Security",
            },
        )
        raise

    return SimplePayload(success=True)
