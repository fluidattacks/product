from integrates.api.mutations import (
    remove_group,
)
from integrates.api.mutations.remove_group import (
    mutate,
)
from integrates.custom_utils.groups import get_group
from integrates.dataloaders import get_new_context
from integrates.db_model.groups.types import (
    GroupFile,
)
from integrates.decorators import (
    enforce_group_level_auth_async,
    require_attribute,
    require_login,
)
from integrates.testing.aws import (
    IntegratesAws,
    IntegratesDynamodb,
)
from integrates.testing.fakers import (
    GraphQLResolveInfoFaker,
    GroupAccessFaker,
    GroupAccessStateFaker,
    GroupFaker,
    OrganizationFaker,
    StakeholderFaker,
)
from integrates.testing.mocks import Mock, mocks
from integrates.testing.utils import parametrize, raises

EMAIL_TEST = "customer_manager@clientapp.com"
GROUP_NAME = "group1"


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            stakeholders=[StakeholderFaker(email=EMAIL_TEST, role="customer_manager")],
            groups=[
                GroupFaker(
                    name=GROUP_NAME,
                    organization_id="org1",
                    files=[
                        GroupFile(
                            description="client test file",
                            file_name="file_test.zip",
                            modified_by=EMAIL_TEST,
                        )
                    ],
                )
            ],
            group_access=[
                GroupAccessFaker(
                    email=EMAIL_TEST,
                    group_name=GROUP_NAME,
                    state=GroupAccessStateFaker(has_access=True, role="customer_manager"),
                ),
            ],
        )
    ),
    others=[Mock(remove_group, "get_unique_authors", "async", 2)],
)
async def test_remove_group() -> None:
    group = await get_group(get_new_context(), GROUP_NAME)
    assert group

    info = GraphQLResolveInfoFaker(
        user_email=EMAIL_TEST,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_attribute, "has_asm", GROUP_NAME],
        ],
    )
    result = await mutate(
        _=None,
        info=info,
        group_name=GROUP_NAME,
        reason="POC_OVER",
        comments="Groups did not extend trial",
    )
    assert result.success is True

    group_after = await get_new_context().group.load(GROUP_NAME)

    assert group_after is None


@parametrize(
    args=["email"],
    cases=[
        ["reviewer@fluidattacks.com"],
        ["reattacker@test.test"],
        ["vulnerability_manager@test.test"],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            stakeholders=[
                StakeholderFaker(email="reviewer@fluidattacks.com", role="hacker"),
                StakeholderFaker(email="user@test.test", role="user"),
                StakeholderFaker(
                    email="vulnerability_manager@test.test", role="vulnerability_manager"
                ),
            ],
            groups=[GroupFaker(name=GROUP_NAME, organization_id="org1")],
            group_access=[
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email="reviewer@fluidattacks.com",
                    state=GroupAccessStateFaker(has_access=True, role="reviewer"),
                ),
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email="user@test.test",
                    state=GroupAccessStateFaker(has_access=True, role="user"),
                ),
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email="user@test.test",
                    state=GroupAccessStateFaker(has_access=True, role="user"),
                ),
            ],
        ),
    )
)
async def test_remove_group_access_denied(email: str) -> None:
    info = GraphQLResolveInfoFaker(
        user_email=email,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_attribute, "has_asm", GROUP_NAME],
        ],
    )

    with raises(Exception, match="Access denied"):
        await mutate(
            _=None,
            info=info,
            group_name=GROUP_NAME,
            reason="POC_OVER",
            comments="Groups did not extend trial",
        )
