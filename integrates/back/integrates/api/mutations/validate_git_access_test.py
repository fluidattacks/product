import os

import fluidattacks_core.git as git_utils

from integrates.api.mutations.validate_git_access import mutate
from integrates.custom_exceptions import InvalidGitCredentials
from integrates.decorators import require_login
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    GraphQLResolveInfoFaker,
    GroupFaker,
    OrganizationFaker,
    StakeholderFaker,
)
from integrates.testing.mocks import Mock, mocks
from integrates.testing.utils import raises

from .payloads.types import SimplePayload

EMAIL_TEST = "admin@fluidattacks.com"


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            stakeholders=[StakeholderFaker(email=EMAIL_TEST, role="admin")],
            groups=[GroupFaker(name="group1", organization_id="org1")],
        ),
    ),
    others=[
        Mock(git_utils, "ssh_ls_remote", "async", ["95cb2d96b43dfc08388633e463ef141daaf54aef", ""])
    ],
)
async def test_validate_git_access() -> None:
    branch: str = "trunk"
    key: str = "" if os.environ["TEST_SSH_KEY"] == "null" else os.environ["TEST_SSH_KEY"]
    url = "ssh://git@gitlab.com:fluidattacks/universe.git"
    info = GraphQLResolveInfoFaker(user_email=EMAIL_TEST, decorators=[[require_login]])
    mutation_result = await mutate(
        _parent=None,
        info=info,
        url=url,
        branch=branch,
        credentials={
            "key": key,
            "name": "SSH Key",
            "type": "SSH",
            "is_pat": False,
        },
    )

    assert mutation_result == SimplePayload(success=True)


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            stakeholders=[StakeholderFaker(email="hacker@fluidattacks.com", role="hacker")],
            groups=[GroupFaker(name="group1", organization_id="org1")],
        ),
    ),
    others=[
        Mock(git_utils, "ssh_ls_remote", "async", (None, "Error")),
    ],
)
async def test_validate_git_access_fail() -> None:
    branch = "trunk"
    key = "VGVzdCBTU0gK"
    url = "ssh://git@gitlab.com:fluidattacks/test-fail-functional.git"

    info = GraphQLResolveInfoFaker(user_email=EMAIL_TEST, decorators=[[require_login]])
    with raises(InvalidGitCredentials):
        await mutate(
            _parent=None,
            info=info,
            url=url,
            branch=branch,
            credentials={
                "key": key,
                "name": "SSH Key",
                "type": "SSH",
                "is_pat": False,
            },
        )


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            stakeholders=[StakeholderFaker(email=EMAIL_TEST, role="admin")],
            groups=[GroupFaker(name="group1")],
        )
    ),
    others=[Mock(git_utils, "ssh_ls_remote", "async", ["", "Branch not found"])],
)
async def test_validate_git_access_wrong_branch() -> None:
    branch: str = "master"
    key: str = "" if os.environ["TEST_SSH_KEY"] == "null" else os.environ["TEST_SSH_KEY"]
    url = "ssh://git@gitlab.com:fluidattacks/universe.git"
    info = GraphQLResolveInfoFaker(user_email=EMAIL_TEST, decorators=[[require_login]])
    with raises(Exception, match="Branch not found"):
        await mutate(
            _parent=None,
            info=info,
            url=url,
            branch=branch,
            credentials={
                "key": key,
                "name": "SSH Key",
                "type": "SSH",
                "is_pat": False,
            },
        )
