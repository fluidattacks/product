from graphql.type.definition import (
    GraphQLResolveInfo,
)
from starlette.datastructures import (
    UploadFile,
)

from integrates.custom_utils import (
    logs as logs_utils,
)
from integrates.decorators import (
    concurrent_decorators,
    enforce_organization_level_auth_async,
    require_login,
)
from integrates.mailmap import (
    import_mailmap,
)

from .payloads.types import (
    ImportMailmapPayload,
)
from .schema import (
    MUTATION,
)


@MUTATION.field("importMailmap")
@concurrent_decorators(
    require_login,
    enforce_organization_level_auth_async,
)
async def mutate(
    _: None,
    info: GraphQLResolveInfo,
    file: UploadFile,
    organization_id: str,
) -> ImportMailmapPayload:
    success_count, exceptions_messages = await import_mailmap(
        file=file,
        organization_id=organization_id,
    )
    import_mailmap_payload = ImportMailmapPayload(
        success=True,
        success_count=success_count,
        exceptions_messages=exceptions_messages,
    )

    logs_utils.cloudwatch_log(
        info.context,
        "Imported mailmap",
        extra={
            "organization_id": organization_id,
            "success_count": success_count,
            "exceptions_messages": exceptions_messages,
        },
    )

    return import_mailmap_payload
