from integrates.api.mutations.add_ip_root import (
    mutate,
)
from integrates.custom_exceptions import InvalidParameter, RepeatedRootNickname
from integrates.dataloaders import get_new_context
from integrates.db_model.roots.types import IPRoot, RootRequest
from integrates.decorators import (
    enforce_group_level_auth_async,
    require_attribute,
    require_attribute_internal,
    require_is_not_under_review,
    require_login,
    require_service_black,
)
from integrates.testing.aws import (
    IntegratesAws,
    IntegratesDynamodb,
)
from integrates.testing.fakers import (
    GraphQLResolveInfoFaker,
    GroupAccessFaker,
    GroupAccessStateFaker,
    GroupFaker,
    OrganizationFaker,
    StakeholderFaker,
)
from integrates.testing.mocks import mocks
from integrates.testing.utils import raises

GROUP_NAME = "test-group"
EMAIL_TEST = "test@fluidattacks"
ORG_ID = "ORG#40f6da5f-4f6a-45ad-b96a-52c2d82f26a1"


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(id=ORG_ID),
            ],
            stakeholders=[
                StakeholderFaker(email=EMAIL_TEST, role="user"),
            ],
            groups=[
                GroupFaker(name=GROUP_NAME, organization_id=ORG_ID),
            ],
            group_access=[
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email=EMAIL_TEST,
                    state=GroupAccessStateFaker(has_access=True, role="admin"),
                ),
            ],
        ),
    )
)
async def test_add_ip_root_invalid_address() -> None:
    info = GraphQLResolveInfoFaker(
        user_email=EMAIL_TEST,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_service_black],
            [require_attribute, "has_service_black", GROUP_NAME],
            [require_is_not_under_review],
            [require_attribute_internal, "is_under_review", GROUP_NAME],
        ],
    )
    with raises(InvalidParameter):
        await mutate(
            _parent=None,
            info=info,
            address="invalid_ip",
            group_name=GROUP_NAME,
            nickname="test-ip",
        )


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(id=ORG_ID),
            ],
            stakeholders=[
                StakeholderFaker(email=EMAIL_TEST, role="user"),
            ],
            groups=[
                GroupFaker(name=GROUP_NAME, organization_id=ORG_ID),
            ],
            group_access=[
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email=EMAIL_TEST,
                    state=GroupAccessStateFaker(has_access=True, role="admin"),
                ),
            ],
        ),
    )
)
async def test_add_ip_root_duplicate_nickname() -> None:
    info = GraphQLResolveInfoFaker(
        user_email=EMAIL_TEST,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_service_black],
            [require_attribute, "has_service_black", GROUP_NAME],
            [require_is_not_under_review],
            [require_attribute_internal, "is_under_review", GROUP_NAME],
        ],
    )
    await mutate(
        _parent=None,
        info=info,
        address="192.168.1.1",
        group_name=GROUP_NAME,
        nickname="test-ip",
    )
    with raises(RepeatedRootNickname):
        await mutate(
            _parent=None,
            info=info,
            address="192.168.1.2",
            group_name=GROUP_NAME,
            nickname="test-ip",
        )


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(id=ORG_ID),
            ],
            stakeholders=[
                StakeholderFaker(email=EMAIL_TEST, role="user"),
            ],
            groups=[
                GroupFaker(name=GROUP_NAME, organization_id=ORG_ID),
            ],
            group_access=[
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email=EMAIL_TEST,
                    state=GroupAccessStateFaker(has_access=True, role="admin"),
                ),
            ],
        ),
    )
)
async def test_add_ip_root_success() -> None:
    loaders = get_new_context()
    info = GraphQLResolveInfoFaker(
        user_email=EMAIL_TEST,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_service_black],
            [require_attribute, "has_service_black", GROUP_NAME],
            [require_is_not_under_review],
            [require_attribute_internal, "is_under_review", GROUP_NAME],
        ],
    )
    result = await mutate(
        _parent=None,
        info=info,
        address="192.168.1.1",
        group_name=GROUP_NAME,
        nickname="test-ip",
    )
    assert result.success

    root = await loaders.root.load(RootRequest(GROUP_NAME, result.root_id))
    assert root is not None
    assert isinstance(root, IPRoot)
    assert root.state.address == "192.168.1.1"
    assert root.state.nickname == "test-ip"
    assert root.group_name == GROUP_NAME
