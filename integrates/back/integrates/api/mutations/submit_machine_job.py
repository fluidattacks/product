from botocore.exceptions import (
    ClientError,
)
from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.custom_exceptions import (
    ErrorSubmittingJob,
    FindingNotFound,
    MachineCouldNotBeQueued,
    MachineExecutionAlreadySubmitted,
)
from integrates.custom_utils.findings import get_finding
from integrates.custom_utils.roots import (
    get_active_git_roots,
)
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.decorators import (
    concurrent_decorators,
    enforce_group_level_auth_async,
    require_asm,
    require_is_not_under_review,
    require_login,
)
from integrates.jobs_orchestration.create_machine_config import (
    MachineJobOrigin,
    MachineJobTechnique,
    MachineModulesToExecute,
)
from integrates.jobs_orchestration.jobs import (
    get_finding_code_from_title,
    has_pending_executions,
    queue_machine_job,
)
from integrates.sessions import (
    domain as sessions_domain,
)

from .payloads.types import (
    SimplePayloadMessage,
)
from .schema import (
    MUTATION,
)


@MUTATION.field("submitMachineJob")
@concurrent_decorators(
    require_login,
    enforce_group_level_auth_async,
    require_is_not_under_review,
    require_asm,
)
async def mutate(
    _: None,
    info: GraphQLResolveInfo,
    finding_id: str,
    root_nicknames: list[str] | None = None,
) -> SimplePayloadMessage:
    loaders: Dataloaders = info.context.loaders
    user_info = await sessions_domain.get_jwt_content(info.context)
    user_email = user_info["user_email"]

    finding = await get_finding(loaders, finding_id)
    finding_code = get_finding_code_from_title(finding.title)
    if not finding_code:
        raise FindingNotFound()

    group_name = finding.group_name

    active_roots_nicknames = {
        root.state.nickname for root in await get_active_git_roots(loaders, group_name)
    }
    if root_nicknames:
        roots_to_execute = active_roots_nicknames.intersection(root_nicknames)
    else:
        roots_to_execute = active_roots_nicknames

    try:
        if await has_pending_executions(group_name, roots_to_execute):
            raise MachineExecutionAlreadySubmitted()

        queued_job = await queue_machine_job(
            loaders=loaders,
            group_name=group_name,
            modified_by=user_email,
            root_nicknames=roots_to_execute,
            modules_to_execute=MachineModulesToExecute(
                APK=True,
                CSPM=True,
                DAST=True,
                SAST=True,
                SCA=True,
            ),
            checks=[finding_code],
            job_origin=MachineJobOrigin.API,
            job_technique=MachineJobTechnique.ALL,
        )
        if queued_job is None:
            raise MachineCouldNotBeQueued()
    except ClientError as ex:
        raise ErrorSubmittingJob() from ex

    return SimplePayloadMessage(
        success=True,
        message="Machine execution was successfully queued",
    )
