from integrates import roots
from integrates.api.mutations.update_toe_lines_loc import mutate
from integrates.custom_exceptions import NumberOutOfRange, ToeLinesAlreadyUpdated, ToeLinesNotFound
from integrates.dataloaders import Dataloaders, get_new_context
from integrates.db_model.toe_lines.types import RootToeLinesRequest
from integrates.decorators import enforce_group_level_auth_async, require_attribute, require_login
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    GitRootFaker,
    GitRootStateFaker,
    GraphQLResolveInfoFaker,
    GroupFaker,
    OrganizationFaker,
    StakeholderFaker,
    ToeLinesFaker,
    ToeLinesStateFaker,
)
from integrates.testing.mocks import Mock, mocks
from integrates.testing.utils import raises

from .payloads.types import UpdateToeLinesPayload

EMAIL_TEST = "admin@fluidattacks.com"
GROUP_NAME = "group1"
ROOT_ID = "root1"


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(id="org1"),
            ],
            stakeholders=[
                StakeholderFaker(email=EMAIL_TEST, role="admin"),
            ],
            groups=[
                GroupFaker(name=GROUP_NAME, organization_id="org1"),
            ],
            roots=[
                GitRootFaker(
                    group_name=GROUP_NAME,
                    id=ROOT_ID,
                    state=GitRootStateFaker(
                        nickname="back",
                    ),
                ),
            ],
            toe_lines=[
                ToeLinesFaker(
                    filename="src/main.py",
                    group_name=GROUP_NAME,
                    root_id=ROOT_ID,
                    state=ToeLinesStateFaker(
                        loc=0,
                    ),
                ),
            ],
        ),
    ),
    others=[Mock(roots.utils, "send_mail_environment", "async", None)],
)
async def test_update_toe_lines_loc_success() -> None:
    loaders: Dataloaders = get_new_context()
    root_toe_lines = await loaders.root_toe_lines.load_nodes(
        RootToeLinesRequest(group_name=GROUP_NAME, root_id=ROOT_ID),
    )

    assert len(root_toe_lines) == 1
    assert root_toe_lines[0].filename == "src/main.py"
    assert root_toe_lines[0].state.loc == 0

    info = GraphQLResolveInfoFaker(
        user_email=EMAIL_TEST,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_attribute, "has_asm", GROUP_NAME],
        ],
    )

    result = await mutate(
        _parent=None,
        info=info,
        comments="Updated toe line in group",
        group_name=GROUP_NAME,
        root_id=ROOT_ID,
        filename="src/main.py",
        loc=200,
    )
    assert result == UpdateToeLinesPayload(
        filename="src/main.py", group_name=GROUP_NAME, root_id=ROOT_ID, success=True
    )

    root_toe_lines = await get_new_context().root_toe_lines.load_nodes(
        RootToeLinesRequest(group_name=GROUP_NAME, root_id=ROOT_ID),
    )

    assert len(root_toe_lines) == 1
    assert root_toe_lines[0].filename == "src/main.py"
    assert root_toe_lines[0].state.loc == 200
    assert root_toe_lines[0].state.comments == "Updated toe line in group"


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            stakeholders=[StakeholderFaker(email=EMAIL_TEST, role="admin")],
            groups=[GroupFaker(name=GROUP_NAME, organization_id="org1")],
            roots=[
                GitRootFaker(
                    group_name=GROUP_NAME,
                    id=ROOT_ID,
                    state=GitRootStateFaker(nickname="back"),
                ),
            ],
            toe_lines=[
                ToeLinesFaker(
                    filename="src/main.py",
                    group_name=GROUP_NAME,
                    root_id=ROOT_ID,
                    state=ToeLinesStateFaker(loc=180),
                ),
            ],
        ),
    )
)
async def test_update_toe_lines_loc_fail() -> None:
    info = GraphQLResolveInfoFaker(
        user_email=EMAIL_TEST,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_attribute, "has_asm", GROUP_NAME],
        ],
    )
    with raises(ToeLinesNotFound):
        await mutate(
            _parent=None,
            info=info,
            comments="Updated toe line in group",
            group_name=GROUP_NAME,
            root_id=ROOT_ID,
            filename="test.py",
            loc=180,
        )


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            stakeholders=[StakeholderFaker(email=EMAIL_TEST, role="admin")],
            groups=[GroupFaker(name=GROUP_NAME, organization_id="org1")],
            roots=[
                GitRootFaker(
                    group_name=GROUP_NAME,
                    id=ROOT_ID,
                    state=GitRootStateFaker(nickname="back"),
                ),
            ],
            toe_lines=[
                ToeLinesFaker(
                    filename="src/main.py",
                    group_name=GROUP_NAME,
                    root_id=ROOT_ID,
                    state=ToeLinesStateFaker(loc=180),
                ),
            ],
        ),
    )
)
async def test_update_toe_lines_loc_fail_2() -> None:
    info = GraphQLResolveInfoFaker(
        user_email=EMAIL_TEST,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_attribute, "has_asm", GROUP_NAME],
        ],
    )
    with raises(ToeLinesAlreadyUpdated):
        await mutate(
            _parent=None,
            info=info,
            comments="Updated toe line in group",
            group_name=GROUP_NAME,
            root_id=ROOT_ID,
            filename="src/main.py",
            loc=180,
        )


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            stakeholders=[StakeholderFaker(email=EMAIL_TEST, role="admin")],
            groups=[GroupFaker(name=GROUP_NAME, organization_id="org1")],
            roots=[
                GitRootFaker(
                    group_name=GROUP_NAME,
                    id=ROOT_ID,
                    state=GitRootStateFaker(nickname="back"),
                ),
            ],
            toe_lines=[
                ToeLinesFaker(
                    filename="src/main.py",
                    group_name=GROUP_NAME,
                    root_id=ROOT_ID,
                    state=ToeLinesStateFaker(loc=180),
                ),
            ],
        ),
    )
)
async def test_update_toe_lines_loc_fail_3() -> None:
    info = GraphQLResolveInfoFaker(
        user_email=EMAIL_TEST,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_attribute, "has_asm", GROUP_NAME],
        ],
    )
    with raises(NumberOutOfRange):
        await mutate(
            _parent=None,
            info=info,
            comments="Updated toe line in group",
            group_name=GROUP_NAME,
            root_id=ROOT_ID,
            filename="src/main.py",
            loc=-1,
        )
