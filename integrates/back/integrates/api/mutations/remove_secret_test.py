from typing import Any

from integrates.api.mutations.remove_secret import mutate
from integrates.dataloaders import get_new_context
from integrates.db_model.roots.types import RootEnvironmentSecretsRequest
from integrates.decorators import (
    enforce_group_level_auth_async,
    require_attribute_internal,
    require_is_not_under_review,
    require_login,
)
from integrates.testing.aws import (
    IntegratesAws,
    IntegratesDynamodb,
    RootEnvironmentSecretsToUpdate,
    RootSecretsToUpdate,
)
from integrates.testing.fakers import (
    GitRootFaker,
    GraphQLResolveInfoFaker,
    GroupAccessFaker,
    GroupAccessStateFaker,
    GroupFaker,
    RootEnvironmentUrlFaker,
    SecretFaker,
    SecretStateFaker,
    StakeholderFaker,
)
from integrates.testing.mocks import mocks
from integrates.testing.utils import parametrize, raises
from integrates.verify.enums import ResourceType

GROUP_NAME = "group_name"

DEFAULT_DECORATORS: list[list[Any]] = [
    [require_login],
    [enforce_group_level_auth_async],
    [require_is_not_under_review],
    [require_attribute_internal, "is_under_review", GROUP_NAME],
]

# Allowed
ADMIN = "admin@gmail.com"
CUSTOMER_MANAGER = "customer_manager@gmail.com"
RESOURCER = "resourcer@gmail.com"
GROUP_MANAGER = "group_manager@gmail.com"
USER = "user@gmail.com"
VULNERABILITY_MANAGER = "vulnerability_manager@gmail.com"

# Unallowed
HACKER = "hacker@gmail.com"
REATTACKER = "reattacker@gmail.com"
ARCHITECT = "architect@gmail.com"


@parametrize(
    args=["user_email"],
    cases=[[HACKER], [REATTACKER], [ARCHITECT]],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            groups=[GroupFaker(name=GROUP_NAME)],
            stakeholders=[
                StakeholderFaker(email=HACKER),
                StakeholderFaker(email=REATTACKER),
                StakeholderFaker(email=ARCHITECT),
            ],
            group_access=[
                GroupAccessFaker(
                    email=HACKER,
                    group_name=GROUP_NAME,
                    state=GroupAccessStateFaker(role="hacker"),
                ),
                GroupAccessFaker(
                    email=REATTACKER,
                    group_name=GROUP_NAME,
                    state=GroupAccessStateFaker(role="reattacker"),
                ),
                GroupAccessFaker(
                    email=ARCHITECT,
                    group_name=GROUP_NAME,
                    state=GroupAccessStateFaker(role="architect"),
                ),
            ],
        ),
    )
)
async def test_remove_secret_access_denied(user_email: str) -> None:
    info = GraphQLResolveInfoFaker(user_email=user_email, decorators=DEFAULT_DECORATORS)
    with raises(Exception, match="Access denied"):
        await mutate(
            _=None,
            info=info,
            key="key",
            resource_id="resource_id",
            resource_type="resource_type",
            group_name=GROUP_NAME,
        )


@parametrize(
    args=["user_email", "key", "resource_type", "resource_id", "env_secrets", "root_secrets"],
    cases=[
        [ADMIN, "key_admin", ResourceType.URL, "url1", 4, 1],
        [CUSTOMER_MANAGER, "key_customer_manager", ResourceType.URL, "url1", 4, 1],
        [RESOURCER, "key_resourcer", ResourceType.ROOT, "root1", 5, 0],
        [GROUP_MANAGER, "key_group_manager", ResourceType.URL, "url1", 4, 1],
        [USER, "key_user", ResourceType.URL, "url1", 4, 1],
        [VULNERABILITY_MANAGER, "key_vulnerability_manager", ResourceType.URL, "url1", 4, 1],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            groups=[GroupFaker(name=GROUP_NAME)],
            stakeholders=[
                StakeholderFaker(email=ADMIN),
                StakeholderFaker(email=CUSTOMER_MANAGER),
                StakeholderFaker(email=RESOURCER),
                StakeholderFaker(email=GROUP_MANAGER),
                StakeholderFaker(email=USER),
                StakeholderFaker(email=VULNERABILITY_MANAGER),
            ],
            group_access=[
                GroupAccessFaker(
                    email=ADMIN,
                    group_name=GROUP_NAME,
                    state=GroupAccessStateFaker(role="admin"),
                ),
                GroupAccessFaker(
                    email=CUSTOMER_MANAGER,
                    group_name=GROUP_NAME,
                    state=GroupAccessStateFaker(role="customer_manager"),
                ),
                GroupAccessFaker(
                    email=RESOURCER,
                    group_name=GROUP_NAME,
                    state=GroupAccessStateFaker(role="resourcer"),
                ),
                GroupAccessFaker(
                    email=GROUP_MANAGER,
                    group_name=GROUP_NAME,
                    state=GroupAccessStateFaker(role="group_manager"),
                ),
                GroupAccessFaker(
                    email=USER,
                    group_name=GROUP_NAME,
                    state=GroupAccessStateFaker(role="user"),
                ),
                GroupAccessFaker(
                    email=VULNERABILITY_MANAGER,
                    group_name=GROUP_NAME,
                    state=GroupAccessStateFaker(role="vulnerability_manager"),
                ),
            ],
            root_environment_urls=[
                RootEnvironmentUrlFaker(id="url1", root_id="root1", group_name=GROUP_NAME)
            ],
            roots=[GitRootFaker(id="root1", group_name=GROUP_NAME)],
            root_secrets=[
                RootSecretsToUpdate(resource_id="root1", secret=SecretFaker(key="key_resourcer"))
            ],
            root_environment_secrets=[
                RootEnvironmentSecretsToUpdate(
                    group_name=GROUP_NAME,
                    resource_id="url1",
                    secret=SecretFaker(key="key_admin", state=SecretStateFaker(owner=ADMIN)),
                ),
                RootEnvironmentSecretsToUpdate(
                    group_name=GROUP_NAME,
                    resource_id="url1",
                    secret=SecretFaker(
                        key="key_customer_manager", state=SecretStateFaker(owner=CUSTOMER_MANAGER)
                    ),
                ),
                RootEnvironmentSecretsToUpdate(
                    group_name=GROUP_NAME,
                    resource_id="url1",
                    secret=SecretFaker(
                        key="key_group_manager", state=SecretStateFaker(owner=GROUP_MANAGER)
                    ),
                ),
                RootEnvironmentSecretsToUpdate(
                    group_name=GROUP_NAME,
                    resource_id="url1",
                    secret=SecretFaker(key="key_user", state=SecretStateFaker(owner=USER)),
                ),
                RootEnvironmentSecretsToUpdate(
                    group_name=GROUP_NAME,
                    resource_id="url1",
                    secret=SecretFaker(
                        key="key_vulnerability_manager",
                        state=SecretStateFaker(owner=VULNERABILITY_MANAGER),
                    ),
                ),
            ],
        ),
    )
)
async def test_remove_env_url_secret_success(
    user_email: str,
    key: str,
    resource_type: ResourceType,
    resource_id: str,
    env_secrets: int,
    root_secrets: int,
) -> None:
    info = GraphQLResolveInfoFaker(user_email=user_email, decorators=DEFAULT_DECORATORS)
    await mutate(
        _=None,
        info=info,
        key=key,
        resource_id=resource_id,
        resource_type=resource_type,
        group_name=GROUP_NAME,
    )
    loaders = get_new_context()
    e_secrets = await loaders.environment_secrets.load(
        RootEnvironmentSecretsRequest(url_id="url1", group_name=GROUP_NAME)
    )
    assert len(e_secrets) == env_secrets
    r_secrets = await loaders.root_secrets.load("root1")
    assert len(r_secrets) == root_secrets


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            groups=[GroupFaker(name=GROUP_NAME)],
            stakeholders=[StakeholderFaker(email=RESOURCER), StakeholderFaker(email=USER)],
            group_access=[
                GroupAccessFaker(
                    email=RESOURCER,
                    group_name=GROUP_NAME,
                    state=GroupAccessStateFaker(role="resourcer"),
                ),
                GroupAccessFaker(
                    email=USER, group_name=GROUP_NAME, state=GroupAccessStateFaker(role="user")
                ),
            ],
            root_environment_urls=[
                RootEnvironmentUrlFaker(id="url1", root_id="root1", group_name=GROUP_NAME)
            ],
            root_environment_secrets=[
                RootEnvironmentSecretsToUpdate(
                    group_name=GROUP_NAME,
                    resource_id="url1",
                    secret=SecretFaker(
                        key="key_resourcer", state=SecretStateFaker(owner=RESOURCER)
                    ),
                ),
                RootEnvironmentSecretsToUpdate(
                    group_name=GROUP_NAME,
                    resource_id="url1",
                    secret=SecretFaker(key="key_user", state=SecretStateFaker(owner=USER)),
                ),
            ],
        ),
    )
)
async def test_remove_env_url_secret_fail_ownership() -> None:
    info = GraphQLResolveInfoFaker(user_email=USER, decorators=DEFAULT_DECORATORS)
    with raises(Exception, match="Access denied"):
        await mutate(
            _=None,
            info=info,
            key="key_resourcer",
            resource_id="url1",
            resource_type=ResourceType.URL,
            group_name=GROUP_NAME,
        )


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            groups=[GroupFaker(name=GROUP_NAME)],
            stakeholders=[StakeholderFaker(email=RESOURCER), StakeholderFaker(email=USER)],
            group_access=[
                GroupAccessFaker(
                    email=USER, group_name=GROUP_NAME, state=GroupAccessStateFaker(role="user")
                ),
            ],
            root_environment_urls=[
                RootEnvironmentUrlFaker(id="url1", root_id="root1", group_name=GROUP_NAME)
            ],
            root_environment_secrets=[
                RootEnvironmentSecretsToUpdate(
                    group_name=GROUP_NAME,
                    resource_id="url1",
                    secret=SecretFaker(
                        key="key_resourcer", state=SecretStateFaker(owner=RESOURCER)
                    ),
                ),
            ],
        ),
    )
)
async def test_remove_env_url_secret_wrong_group_name() -> None:
    info = GraphQLResolveInfoFaker(
        user_email=RESOURCER,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_is_not_under_review],
            [require_attribute_internal, "is_under_review", "group1"],
        ],
    )
    with raises(Exception, match="Access denied"):
        await mutate(
            _=None,
            info=info,
            key="key_resourcer",
            resource_id="url1",
            resource_type=ResourceType.URL,
            group_name="group1",
        )
