from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.api.types import (
    APP_EXCEPTIONS,
)
from integrates.custom_utils import (
    logs as logs_utils,
)
from integrates.decorators import (
    concurrent_decorators,
    enforce_group_level_auth_async,
    require_asm,
    require_finding_access,
    require_login,
)
from integrates.findings import (
    domain as findings_domain,
)

from .payloads.types import (
    SimplePayload,
)
from .schema import (
    MUTATION,
)


@MUTATION.field("updateEvidenceDescription")
@concurrent_decorators(
    require_login,
    enforce_group_level_auth_async,
    require_asm,
    require_finding_access,
)
async def mutate(
    _: None,
    info: GraphQLResolveInfo,
    finding_id: str,
    evidence_id: str,
    description: str,
) -> SimplePayload:
    try:
        await findings_domain.update_evidence_description(
            loaders=info.context.loaders,
            finding_id=finding_id,
            evidence_id=evidence_id,
            description=description,
        )
        logs_utils.cloudwatch_log(
            info.context,
            "Updated evidence description in the finding",
            extra={
                "evidence_id": evidence_id,
                "finding_id": finding_id,
                "log_type": "Security",
            },
        )
    except APP_EXCEPTIONS:
        logs_utils.cloudwatch_log(
            info.context,
            "Attempted to update evidence description in the finding",
            extra={
                "evidence_id": evidence_id,
                "finding_id": finding_id,
                "log_type": "Security",
            },
        )
        raise

    return SimplePayload(success=True)
