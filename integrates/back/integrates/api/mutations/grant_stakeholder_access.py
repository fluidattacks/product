import logging
import logging.config

from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates import (
    authz,
)
from integrates.custom_exceptions import (
    InvalidRoleProvided,
    StakeholderHasGroupAccess,
)
from integrates.custom_utils import (
    logs as logs_utils,
)
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.group_access.types import (
    GroupAccessRequest,
)
from integrates.db_model.stakeholders.types import (
    Stakeholder,
)
from integrates.decorators import (
    concurrent_decorators,
    enforce_group_level_auth_async,
    require_asm,
    require_is_not_under_review,
    require_login,
)
from integrates.group_access.domain import (
    exists,
    validate_new_invitation_time_limit,
)
from integrates.groups import (
    domain as groups_domain,
)
from integrates.sessions import (
    domain as sessions_domain,
)

from .payloads.types import (
    GrantStakeholderAccessPayload,
)
from .schema import (
    MUTATION,
)

# Constants
LOGGER = logging.getLogger(__name__)


@MUTATION.field("grantStakeholderAccess")
@concurrent_decorators(
    require_login,
    enforce_group_level_auth_async,
    require_is_not_under_review,
    require_asm,
)
async def mutate(
    _: None,
    info: GraphQLResolveInfo,
    group_name: str,
    role: str,
    **kwargs: str,
) -> GrantStakeholderAccessPayload:
    loaders: Dataloaders = info.context.loaders
    group_name = group_name.lower()
    user_data = await sessions_domain.get_jwt_content(info.context)
    user_email = user_data["user_email"]
    new_user_email = kwargs.get("email", "").lower().strip()
    new_user_responsibility = kwargs.get("responsibility", "-")

    if await exists(loaders, group_name, new_user_email):
        group_access = await loaders.group_access.load(
            GroupAccessRequest(group_name=group_name, email=new_user_email),
        )
        # Stakeholder has already accepted the invitation
        if group_access and group_access.state.has_access:
            raise StakeholderHasGroupAccess()
        # Too soon to send another email invitation to the same stakeholder
        if group_access and group_access.expiration_time:
            validate_new_invitation_time_limit(group_access.expiration_time)

    enforcer = await authz.get_group_level_enforcer(loaders, user_email)

    if not enforcer(group_name, f"grant_group_level_role:{role}"):
        LOGGER.error(
            "Invalid role provided",
            extra={
                "extra": {
                    "role": role,
                    "group_name": group_name,
                    "requester_email": user_email,
                },
            },
        )
        raise InvalidRoleProvided(role=role)

    await groups_domain.invite_to_group(
        loaders=loaders,
        email=new_user_email,
        responsibility=new_user_responsibility,
        role=role,
        group_name=group_name,
        modified_by=user_email,
    )
    logs_utils.cloudwatch_log(
        info.context,
        "Grant access in the group",
        extra={
            "new_stakeholder": new_user_email,
            "group_name": group_name,
            "log_type": "Security",
            "role": role,
        },
    )

    return GrantStakeholderAccessPayload(
        success=True,
        granted_stakeholder=Stakeholder(
            email=new_user_email,
        ),
    )
