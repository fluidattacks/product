from decimal import Decimal

from integrates.api.mutations.payloads.types import SimplePayload
from integrates.api.mutations.update_organization_policies import mutate
from integrates.custom_exceptions import (
    InvalidAcceptanceDaysUntilItBreaksRange,
    InvalidMaxAcceptanceDays,
)
from integrates.dataloaders import get_new_context
from integrates.decorators import enforce_organization_level_auth_async
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    GraphQLResolveInfoFaker,
    OrganizationAccessFaker,
    OrganizationAccessStateFaker,
    OrganizationFaker,
    StakeholderFaker,
)
from integrates.testing.mocks import mocks
from integrates.testing.utils import parametrize, raises


@parametrize(
    args=["email"],
    cases=[
        ["organization_manager@gmail.com"],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="ORG#org1", name="test-org")],
            stakeholders=[
                StakeholderFaker(email="organization_manager@gmail.com"),
            ],
            organization_access=[
                OrganizationAccessFaker(
                    organization_id="ORG#org1",
                    email="organization_manager@gmail.com",
                    state=OrganizationAccessStateFaker(
                        has_access=True, role="organization_manager"
                    ),
                ),
            ],
        ),
    )
)
async def test_update_organization_policies(email: str) -> None:
    org_id = "ORG#org1"
    org_name = "test-org"

    info = GraphQLResolveInfoFaker(
        user_email=email,
        decorators=[
            [enforce_organization_level_auth_async],
        ],
    )

    loaders = get_new_context()
    result: SimplePayload = await mutate(
        _parent=None,
        info=info,
        organization_id=org_id,
        organization_name=org_name,
        inactivity_period=270,
        max_acceptance_days=5,
        max_acceptance_severity=8.2,
        max_number_acceptances=3,
        min_acceptance_severity=0.0,
        min_breaking_severity=5.7,
        vulnerability_grace_period=1000,
        days_until_it_breaks=2000,
    )

    assert result == SimplePayload(success=True)

    organization = await loaders.organization.load(org_id)

    assert organization
    assert organization.id == org_id
    assert organization.name == org_name
    assert organization.policies.inactivity_period == 270
    assert organization.policies.max_acceptance_days == 5
    assert organization.policies.max_acceptance_severity == Decimal("8.2")
    assert organization.policies.max_number_acceptances == 3
    assert organization.policies.min_acceptance_severity == Decimal("0.0")
    assert organization.policies.min_breaking_severity == Decimal("5.7")
    assert organization.policies.vulnerability_grace_period == 1000
    assert organization.policies.days_until_it_breaks == 2000

    result = await mutate(
        _parent=None,
        info=info,
        organization_id=org_id,
        organization_name=org_name,
        inactivity_period=270,
        max_acceptance_days=5,
        max_acceptance_severity=8.2,
        max_number_acceptances=3,
        min_acceptance_severity=0.0,
        min_breaking_severity=5.7,
        vulnerability_grace_period=1000,
    )

    assert result == SimplePayload(success=True)

    organization = await get_new_context().organization.load(org_id)

    assert organization
    assert organization.id == org_id
    assert organization.name == org_name
    assert organization.policies.inactivity_period == 270
    assert organization.policies.max_acceptance_days == 5
    assert organization.policies.max_acceptance_severity == Decimal("8.2")
    assert organization.policies.max_number_acceptances == 3
    assert organization.policies.min_acceptance_severity == Decimal("0.0")
    assert organization.policies.min_breaking_severity == Decimal("5.7")
    assert organization.policies.vulnerability_grace_period == 1000
    assert organization.policies.days_until_it_breaks is None


@parametrize(
    args=["email"],
    cases=[
        ["hacker@gmail.com"],
        ["reattacker@gmail.com"],
        ["user@gmail.com"],
        ["customer_manager@fluidattacks.com"],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="ORG#org1", name="test-org")],
            stakeholders=[
                StakeholderFaker(email="hacker@gmail.com"),
                StakeholderFaker(email="reattacker@gmail.com"),
                StakeholderFaker(email="user@gmail.com"),
                StakeholderFaker(email="customer_manager@fluidattacks.com"),
            ],
            organization_access=[
                OrganizationAccessFaker(
                    organization_id="ORG#org1",
                    email="hacker@gmail.com",
                    state=OrganizationAccessStateFaker(has_access=False, role="hacker"),
                ),
                OrganizationAccessFaker(
                    organization_id="ORG#org1",
                    email="reattacker@gmail.com",
                    state=OrganizationAccessStateFaker(has_access=False, role="reattacker"),
                ),
                OrganizationAccessFaker(
                    organization_id="ORG#org1",
                    email="user@gmail.com",
                    state=OrganizationAccessStateFaker(has_access=False, role="user"),
                ),
                OrganizationAccessFaker(
                    organization_id="ORG#org1",
                    email="customer_manager@fluidattacks.com",
                    state=OrganizationAccessStateFaker(has_access=False, role="customer_manager"),
                ),
            ],
        ),
    )
)
async def test_update_organization_policies_fail_1(email: str) -> None:
    org_id = "ORG#org1"
    org_name = "test-org"

    info = GraphQLResolveInfoFaker(
        user_email=email,
        decorators=[
            [enforce_organization_level_auth_async],
        ],
    )

    with raises(Exception, match="Access denied"):
        await mutate(
            _parent=None,
            info=info,
            organization_id=org_id,
            organization_name=org_name,
            inactivity_period=270,
            max_acceptance_days=5,
            max_acceptance_severity=8.2,
            max_number_acceptances=3,
            min_acceptance_severity=0.0,
            min_breaking_severity=5.7,
            vulnerability_grace_period=1000,
            days_until_it_breaks=2000,
        )


@parametrize(
    args=["email"],
    cases=[
        ["organization_manager@gmail.com"],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="ORG#org1", name="test-org")],
            stakeholders=[
                StakeholderFaker(email="organization_manager@gmail.com"),
            ],
            organization_access=[
                OrganizationAccessFaker(
                    organization_id="ORG#org1",
                    email="organization_manager@gmail.com",
                    state=OrganizationAccessStateFaker(
                        has_access=True, role="organization_manager"
                    ),
                ),
            ],
        ),
    )
)
async def test_update_organization_policies_fail_2(email: str) -> None:
    org_id = "ORG#org1"
    org_name = "test-org"

    info = GraphQLResolveInfoFaker(
        user_email=email,
        decorators=[
            [enforce_organization_level_auth_async],
        ],
    )

    with raises(InvalidAcceptanceDaysUntilItBreaksRange):
        await mutate(
            _parent=None,
            info=info,
            organization_id=org_id,
            organization_name=org_name,
            inactivity_period=270,
            max_acceptance_days=5,
            max_acceptance_severity=8.2,
            max_number_acceptances=3,
            min_acceptance_severity=0.0,
            min_breaking_severity=5.7,
            vulnerability_grace_period=1000,
            days_until_it_breaks=10,
        )


@parametrize(
    args=["email"],
    cases=[
        ["organization_manager@gmail.com"],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="ORG#org1", name="test-org")],
            stakeholders=[
                StakeholderFaker(email="organization_manager@gmail.com"),
            ],
            organization_access=[
                OrganizationAccessFaker(
                    organization_id="ORG#org1",
                    email="organization_manager@gmail.com",
                    state=OrganizationAccessStateFaker(
                        has_access=True, role="organization_manager"
                    ),
                ),
            ],
        ),
    )
)
async def test_update_organization_policies_fail_3(email: str) -> None:
    org_id = "ORG#org1"
    org_name = "test-org"

    info = GraphQLResolveInfoFaker(
        user_email=email,
        decorators=[
            [enforce_organization_level_auth_async],
        ],
    )

    with raises(InvalidMaxAcceptanceDays):
        await mutate(
            _parent=None,
            info=info,
            organization_id=org_id,
            organization_name=org_name,
            inactivity_period=270,
            max_acceptance_days=1000,
            max_acceptance_severity=8.2,
            max_number_acceptances=3,
            min_acceptance_severity=0.0,
            min_breaking_severity=5.7,
            vulnerability_grace_period=1000,
            days_until_it_breaks=2000,
        )
