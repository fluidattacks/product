from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.custom_utils import (
    logs as logs_utils,
)
from integrates.decorators import (
    concurrent_decorators,
    enforce_group_level_auth_async,
    require_login,
    require_report_vulnerabilities,
    require_request_zero_risk,
)
from integrates.sessions import (
    domain as sessions_domain,
)
from integrates.unreliable_indicators.enums import (
    EntityDependency,
)
from integrates.unreliable_indicators.operations import (
    update_unreliable_indicators_by_deps,
)
from integrates.vulnerabilities import (
    domain as vulns_domain,
)

from .payloads.types import (
    SimplePayload,
)
from .schema import (
    MUTATION,
)


@MUTATION.field("confirmVulnerabilitiesZeroRisk")
@concurrent_decorators(
    require_request_zero_risk,
    require_login,
    require_report_vulnerabilities,
    enforce_group_level_auth_async,
)
async def mutate(
    _: None,
    info: GraphQLResolveInfo,
    finding_id: str,
    justification: str,
    vulnerabilities: list[str],
) -> SimplePayload:
    """Resolve confirm_vulnerabilities_zero_risk mutation."""
    user_info = await sessions_domain.get_jwt_content(info.context)
    await vulns_domain.confirm_vulnerabilities_zero_risk(
        loaders=info.context.loaders,
        vuln_ids=set(vulnerabilities),
        finding_id=finding_id,
        user_info=user_info,
        justification=justification,
    )
    await update_unreliable_indicators_by_deps(
        EntityDependency.confirm_vulnerabilities_zero_risk,
        finding_ids=[finding_id],
        vulnerability_ids=vulnerabilities,
    )
    logs_utils.cloudwatch_log(
        info.context,
        "Confirmed zero risk vulnerabilities in the finding",
        extra={"finding_id": finding_id, "log_type": "Security"},
    )

    return SimplePayload(success=True)
