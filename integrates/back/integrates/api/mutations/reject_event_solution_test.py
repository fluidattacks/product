from graphql import GraphQLError

from integrates.api.mutations.payloads.types import SimplePayload
from integrates.api.mutations.reject_event_solution import mutate
from integrates.decorators import enforce_group_level_auth_async, require_attribute, require_login
from integrates.events import domain as events_domain
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    EMAIL_FLUIDATTACKS_ADMIN,
    EMAIL_FLUIDATTACKS_HACKER,
    GraphQLResolveInfoFaker,
    GroupAccessFaker,
    GroupAccessStateFaker,
    GroupFaker,
    OrganizationFaker,
    StakeholderFaker,
)
from integrates.testing.mocks import Mock, mocks
from integrates.testing.utils import parametrize, raises


@parametrize(
    args=["email", "event_id", "comments"],
    cases=[
        ["user@gmail.com", "418900971", "comment test"],
        ["group_manager@gmail.com", "418900972", "comment test"],
        ["org_manager@gmail.com", "418900977", "comment test"],
        ["vulnerability_manager@gmail.com", "418900973", "comment test"],
        ["reviewer@gmail.com", "418900974", "comment test"],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(id="org1"),
            ],
            groups=[
                GroupFaker(organization_id="org1"),
            ],
            stakeholders=[
                StakeholderFaker(email="user@gmail.com", role="user"),
                StakeholderFaker(email="group_manager@gmail.com", role="group_manager"),
                StakeholderFaker(email="org_manager@gmail.com", role="organization_manager"),
                StakeholderFaker(
                    email="vulnerability_manager@gmail.com", role="vulnerability_manager"
                ),
                StakeholderFaker(email="reviewer@gmail.com", role="reviewer"),
            ],
            group_access=[
                GroupAccessFaker(
                    email="user@gmail.com",
                    state=GroupAccessStateFaker(has_access=True, role="user"),
                ),
                GroupAccessFaker(
                    email="group_manager@gmail.com",
                    state=GroupAccessStateFaker(has_access=True, role="group_manager"),
                ),
                GroupAccessFaker(
                    email="org_manager@gmail.com",
                    state=GroupAccessStateFaker(has_access=True, role="organization_manager"),
                ),
                GroupAccessFaker(
                    email="vulnerability_manager@gmail.com",
                    state=GroupAccessStateFaker(has_access=True, role="vulnerability_manager"),
                ),
                GroupAccessFaker(
                    email="reviewer@gmail.com",
                    state=GroupAccessStateFaker(has_access=True, role="reviewer"),
                ),
            ],
        )
    )
)
async def test_reject_event_solution_access_denied(
    email: str,
    event_id: str,
    comments: str,
) -> None:
    info = GraphQLResolveInfoFaker(
        user_email=email,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_attribute, "has_asm", "test-group"],
        ],
    )

    with raises(GraphQLError, match="Access denied"):
        await mutate(
            _=None,
            info=info,
            comments=comments,
            event_id=event_id,
            group_name="test-group",
        )


@parametrize(
    args=["email", "event_id", "comments"],
    cases=[
        [EMAIL_FLUIDATTACKS_ADMIN, "418900971", "comment test"],
        [EMAIL_FLUIDATTACKS_HACKER, "418900972", "comment test"],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(id="org1"),
            ],
            groups=[
                GroupFaker(organization_id="org1"),
            ],
            stakeholders=[
                StakeholderFaker(email=EMAIL_FLUIDATTACKS_ADMIN, role="admin"),
                StakeholderFaker(email=EMAIL_FLUIDATTACKS_HACKER, role="hacker"),
            ],
            group_access=[
                GroupAccessFaker(
                    email=EMAIL_FLUIDATTACKS_ADMIN,
                    state=GroupAccessStateFaker(has_access=True, role="admin"),
                ),
                GroupAccessFaker(
                    email=EMAIL_FLUIDATTACKS_HACKER,
                    state=GroupAccessStateFaker(has_access=True, role="hacker"),
                ),
            ],
        )
    ),
    others=[Mock(events_domain, "reject_solution", "async", None)],
)
async def test_reject_event_solution(
    email: str,
    event_id: str,
    comments: str,
) -> None:
    info = GraphQLResolveInfoFaker(
        user_email=email,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_attribute, "has_asm", "test-group"],
        ],
    )

    result: SimplePayload = await mutate(
        _=None,
        info=info,
        comments=comments,
        event_id=event_id,
        group_name="test-group",
    )

    assert result.success
