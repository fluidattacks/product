from io import BytesIO

from starlette.datastructures import UploadFile

from integrates.api.mutations.upload_git_root_file import (
    mutate,
)
from integrates.decorators import (
    enforce_group_level_auth_async,
    require_asm,
    require_attribute,
    require_attribute_internal,
    require_is_not_under_review,
    require_login,
)
from integrates.testing.aws import (
    IntegratesAws,
    IntegratesDynamodb,
)
from integrates.testing.fakers import (
    GraphQLResolveInfoFaker,
    GroupAccessFaker,
    GroupAccessStateFaker,
    GroupFaker,
    OrganizationFaker,
    StakeholderFaker,
)
from integrates.testing.mocks import mocks

ADMIN_EMAIL = "admin@gmail.com"
GROUP_NAME = "test-group"
ORG_ID = "ORG#123"


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id=ORG_ID)],
            stakeholders=[
                StakeholderFaker(email=ADMIN_EMAIL),
            ],
            groups=[
                GroupFaker(name=GROUP_NAME, organization_id=ORG_ID),
            ],
            group_access=[
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email=ADMIN_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="admin"),
                ),
            ],
        )
    )
)
async def test_upload_git_root_file_empty() -> None:
    info = GraphQLResolveInfoFaker(
        user_email=ADMIN_EMAIL,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_is_not_under_review],
            [require_attribute_internal, "is_under_review", GROUP_NAME],
            [require_asm],
            [require_attribute, "has_asm", GROUP_NAME],
        ],
    )
    file_content = b""
    file = UploadFile(filename="test.csv", file=BytesIO(file_content))
    result = await mutate(
        _=None,
        info=info,
        group_name=GROUP_NAME,
        file=file,
    )
    assert not result.success
    assert result.message == "Only CSV files are supported."


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id=ORG_ID)],
            stakeholders=[
                StakeholderFaker(email=ADMIN_EMAIL),
            ],
            groups=[
                GroupFaker(name=GROUP_NAME, organization_id=ORG_ID),
            ],
            group_access=[
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email=ADMIN_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="admin"),
                ),
            ],
        )
    )
)
async def test_upload_git_root_file_success() -> None:
    info = GraphQLResolveInfoFaker(
        user_email=ADMIN_EMAIL,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_is_not_under_review],
            [require_attribute_internal, "is_under_review", GROUP_NAME],
            [require_asm],
            [require_attribute, "has_asm", GROUP_NAME],
        ],
    )
    file_content = (
        b"url,branch,credentials,gitignore,includes_health_check,nickname,use_egress,use_ztna,priority\n"
        b"https://github.com/test/repo,main,token123,*.pyc,true,test-repo,true,false,HIGH"
    )
    file = UploadFile(filename="test.csv", file=BytesIO(file_content))
    result = await mutate(
        _=None,
        info=info,
        group_name=GROUP_NAME,
        file=file,
    )
    assert result.success
