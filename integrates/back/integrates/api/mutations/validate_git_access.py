from typing import (
    Any,
)

from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.audit import AuditEvent, add_audit_event
from integrates.custom_exceptions import (
    InvalidGitCredentials,
)
from integrates.custom_utils import (
    analytics,
)
from integrates.custom_utils import (
    logs as logs_utils,
)
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.decorators import (
    require_login,
)
from integrates.organizations import (
    utils as orgs_utils,
)
from integrates.roots import (
    utils as roots_utils,
)
from integrates.roots import (
    validations as roots_validations,
)
from integrates.sessions import (
    domain as sessions_domain,
)

from .payloads.types import (
    SimplePayload,
)
from .schema import (
    MUTATION,
)


@MUTATION.field("validateGitAccess")
@require_login
async def mutate(_parent: None, info: GraphQLResolveInfo, **kwargs: Any) -> SimplePayload:
    loaders: Dataloaders = info.context.loaders
    user_info: dict[str, str] = await sessions_domain.get_jwt_content(info.context)
    user_email: str = user_info["user_email"]
    url = roots_utils.format_git_repo_url(kwargs["url"])
    branch = kwargs["branch"]
    secret = orgs_utils.format_credentials_secret_type(kwargs["credentials"])
    stakeholder = await loaders.stakeholder.load(user_email)

    if stakeholder and not stakeholder.enrolled:
        await analytics.mixpanel_track(
            user_email,
            "AutoenrollCheckAccess",
            credential_type=kwargs["credentials"]["type"],
            url=url,
        )
        add_audit_event(
            AuditEvent(
                author=user_email,
                action="READ",
                metadata={"credential_type": kwargs["credentials"]["type"], "url": url},
                object="Autoenroll.CheckAccess",
                object_id="unknown",
            )
        )

    try:
        await roots_validations.validate_git_access(
            url=url,
            branch=branch,
            secret=secret,
            loaders=loaders,
            is_pat=kwargs["credentials"].get("is_pat", False),
            organization_id=kwargs.get("organization_id"),
            follow_redirects=False,
        )
    except InvalidGitCredentials as exc:
        logs_utils.cloudwatch_log(
            info.context,
            "Attempted to validate credentials file in the organization",
            extra={
                "organization_id": kwargs.get("organization_id"),
                "log_type": "Security",
            },
        )
        raise exc

    logs_utils.cloudwatch_log(
        info.context,
        "Validated Git root access",
        extra={"branch": branch, "url": url, "log_type": "Security"},
    )

    return SimplePayload(success=True)
