from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.custom_utils import (
    logs as logs_utils,
)
from integrates.db_model.mailmap.types import (
    MailmapEntry,
)
from integrates.decorators import (
    concurrent_decorators,
    enforce_organization_level_auth_async,
    require_login,
)
from integrates.mailmap import (
    update_mailmap_entry,
)

from .payloads.types import (
    SimplePayload,
)
from .schema import (
    MUTATION,
)


@MUTATION.field("updateMailmapEntry")
@concurrent_decorators(
    require_login,
    enforce_organization_level_auth_async,
)
async def mutate(
    _parent: None,
    info: GraphQLResolveInfo,
    entry_email: str,
    entry: MailmapEntry,
    organization_id: str,
) -> SimplePayload:
    await update_mailmap_entry(
        entry_email=entry_email,
        entry=entry,
        organization_id=organization_id,
    )

    logs_utils.cloudwatch_log(
        info.context,
        "Updated mailmap author",
        extra={
            "old_entry_email": entry_email,
            "new_entry_email": entry["mailmap_entry_email"],
            "organization_id": organization_id,
        },
    )

    return SimplePayload(success=True)
