from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.custom_utils import (
    environments_validations,
)
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.roots.get import (
    get_git_environment_url_by_id,
)
from integrates.db_model.roots.types import (
    RootEnvironmentUrl,
)
from integrates.decorators import (
    concurrent_decorators,
    enforce_group_level_auth_async,
    require_asm,
    require_is_not_under_review,
    require_login,
)
from integrates.findings.domain.events import (
    manage_cspm_environment_events,
)
from integrates.roots.domain import (
    get_environment_url_secrets,
)
from integrates.sessions import domain as sessions_domain

from .payloads.types import (
    SimplePayload,
)
from .schema import (
    MUTATION,
)


@MUTATION.field("addEnvironmentIntegrityEvent")
@concurrent_decorators(
    require_login,
    enforce_group_level_auth_async,
    require_is_not_under_review,
    require_asm,
)
async def mutate(
    _: None,
    info: GraphQLResolveInfo,
    group_name: str,
    url_id: str,
) -> SimplePayload:
    """Resolve add_environment_integrity_event mutation."""
    loaders: Dataloaders = info.context.loaders
    user_info: dict[str, str] = await sessions_domain.get_jwt_content(info.context)
    parent: RootEnvironmentUrl | None = await get_git_environment_url_by_id(
        url_id=url_id,
        group_name=group_name,
    )

    if parent and parent.state.cloud_name in {"AWS", "GCP", "AZURE"}:
        url = parent.url
        cloud_name = parent.state.cloud_name.value

        secrets_list = await get_environment_url_secrets(loaders, parent)
        secrets: dict[str, str] = {secret.key: secret.value for secret in secrets_list}

        is_role_valid = await environments_validations.validate_role_status(
            loaders,
            url,
            cloud_name,
            group_name,
            secrets,
        )
        await manage_cspm_environment_events(
            loaders=loaders,
            env_url=parent,
            is_role_valid=is_role_valid,
            user_info=user_info,
        )
        return SimplePayload(success=True)

    return SimplePayload(success=False)
