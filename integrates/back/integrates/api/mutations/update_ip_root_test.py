from graphql import GraphQLError

from integrates.api.mutations.update_ip_root import mutate
from integrates.custom_exceptions import InvalidParameter
from integrates.dataloaders import get_new_context
from integrates.db_model.roots.types import IPRoot, RootRequest
from integrates.decorators import (
    enforce_group_level_auth_async,
    require_attribute,
    require_attribute_internal,
    require_is_not_under_review,
    require_login,
    require_service_black,
)
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    GraphQLResolveInfoFaker,
    GroupFaker,
    IPRootFaker,
    OrganizationFaker,
    StakeholderFaker,
)
from integrates.testing.mocks import mocks
from integrates.testing.utils import raises

ADMIN_EMAIL = "admin@gmail.com"
HACKER_EMAIL = "hacker@fluidattacks.com"
GROUP_NAME = "test-group"
RESOURCER_EMAIL = "resourcer@fluidattacks.com"
USER_EMAIL = "user@gmail.com"
ROOT_ID = "root1"


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(id="org1"),
            ],
            stakeholders=[
                StakeholderFaker(email=USER_EMAIL, role="admin"),
            ],
            groups=[
                GroupFaker(name=GROUP_NAME, organization_id="org1"),
            ],
            roots=[
                IPRootFaker(
                    group_name=GROUP_NAME,
                    id=ROOT_ID,
                ),
            ],
        ),
    ),
)
async def test_update_ip_root_success() -> None:
    loaders = get_new_context()
    info = GraphQLResolveInfoFaker(
        user_email=USER_EMAIL,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_is_not_under_review],
            [require_attribute_internal, "is_under_review", GROUP_NAME],
            [require_service_black],
            [require_attribute, "has_service_black", GROUP_NAME],
        ],
    )

    result = await mutate(
        _parent=None,
        info=info,
        group_name=GROUP_NAME,
        root_id=ROOT_ID,
        nickname="new_nickname",
    )
    assert result.success

    root = await loaders.root.load(RootRequest(GROUP_NAME, ROOT_ID))
    assert isinstance(root, IPRoot)
    assert root.state.nickname == "new_nickname"


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(id="org1"),
            ],
            stakeholders=[
                StakeholderFaker(email=USER_EMAIL, role="user"),
            ],
            groups=[
                GroupFaker(name=GROUP_NAME, organization_id="org1"),
            ],
        ),
    ),
)
async def test_update_ip_root_unauthorized() -> None:
    info = GraphQLResolveInfoFaker(
        user_email=USER_EMAIL,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_is_not_under_review],
            [require_attribute_internal, "is_under_review", GROUP_NAME],
            [require_service_black],
            [require_attribute, "has_service_black", GROUP_NAME],
        ],
    )

    with raises(GraphQLError) as exc:
        await mutate(
            _parent=None,
            info=info,
            group_name=GROUP_NAME,
            root_id=ROOT_ID,
            nickname="new_nickname",
        )
    assert "Access denied" in str(exc.value)


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(id="org1"),
            ],
            stakeholders=[
                StakeholderFaker(email=USER_EMAIL, role="admin"),
            ],
            groups=[
                GroupFaker(name=GROUP_NAME, organization_id="org1"),
            ],
        ),
    ),
)
async def test_update_ip_root_nonexistent() -> None:
    info = GraphQLResolveInfoFaker(
        user_email=USER_EMAIL,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_is_not_under_review],
            [require_attribute_internal, "is_under_review", GROUP_NAME],
            [require_service_black],
            [require_attribute, "has_service_black", GROUP_NAME],
        ],
    )

    with raises(InvalidParameter):
        await mutate(
            _parent=None,
            info=info,
            group_name=GROUP_NAME,
            root_id="nonexistent_root",
            nickname="new_nickname",
        )
