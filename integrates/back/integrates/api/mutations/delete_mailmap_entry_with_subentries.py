from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.custom_utils import (
    logs as logs_utils,
)
from integrates.decorators import (
    concurrent_decorators,
    enforce_organization_level_auth_async,
    require_login,
)
from integrates.mailmap import (
    delete_mailmap_entry_with_subentries,
)

from .payloads.types import (
    SimplePayload,
)
from .schema import (
    MUTATION,
)


@MUTATION.field("deleteMailmapEntryWithSubentries")
@concurrent_decorators(
    require_login,
    enforce_organization_level_auth_async,
)
async def mutate(
    _parent: None,
    info: GraphQLResolveInfo,
    entry_email: str,
    organization_id: str,
) -> SimplePayload:
    await delete_mailmap_entry_with_subentries(
        entry_email=entry_email,
        organization_id=organization_id,
    )

    logs_utils.cloudwatch_log(
        info.context,
        "Deleted mailmap author with aliases",
        extra={
            "entry_email": entry_email,
            "organization_id": organization_id,
        },
    )

    return SimplePayload(success=True)
