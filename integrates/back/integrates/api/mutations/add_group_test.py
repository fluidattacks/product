from integrates.api.mutations.add_group import mutate
from integrates.custom_utils import utils
from integrates.dataloaders import Dataloaders, get_new_context
from integrates.db_model.groups.enums import GroupLanguage, GroupManaged
from integrates.decorators import enforce_organization_level_auth_async, require_login
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    GraphQLResolveInfoFaker,
    OrganizationAccessFaker,
    OrganizationAccessStateFaker,
    OrganizationFaker,
    StakeholderFaker,
)
from integrates.testing.mocks import Mock, mocks
from integrates.testing.utils import parametrize
from integrates.tickets import domain as tickets_domain

ORGANIZATION_TEST_ID = "ORG#40f6da5f-4f66-4bf0-825b-a2d9748ad6db"


@parametrize(
    args=["user"],
    cases=[
        ["customer_manager@fluidattacks.com"],
        ["organization_manager@company.com"],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(
                    id=ORGANIZATION_TEST_ID,
                    name="testidi",
                    created_by="customer_manager@fluidattacks.com",
                )
            ],
            stakeholders=[
                StakeholderFaker(email="customer_manager@fluidattacks.com"),
                StakeholderFaker(email="organization_manager@company.com"),
            ],
            organization_access=[
                OrganizationAccessFaker(
                    organization_id=ORGANIZATION_TEST_ID,
                    email="customer_manager@fluidattacks.com",
                    state=OrganizationAccessStateFaker(has_access=True, role="customer_manager"),
                ),
                OrganizationAccessFaker(
                    organization_id=ORGANIZATION_TEST_ID,
                    email="organization_manager@company.com",
                    state=OrganizationAccessStateFaker(
                        has_access=True, role="organization_manager"
                    ),
                ),
            ],
        ),
    ),
    others=[
        Mock(tickets_domain, "send_mails_async", "async", None),
        Mock(utils, "is_personal_email", "async", False),
    ],
)
async def test_create_group_customer_manager(user: str) -> None:
    # Arrange
    info = GraphQLResolveInfoFaker(
        user_email=user,
        decorators=[
            [require_login],
            [enforce_organization_level_auth_async],
        ],
    )
    group_name = user.split("_")[0]

    # Act
    await mutate(
        _=None,
        info=info,
        description=f"created by {user}",
        organization_name="testidi",
        subscription="continuous",
        language="en",
        group_name=group_name,
        has_advanced=True,
        has_essential=True,
    )

    # Assert‰
    loaders: Dataloaders = get_new_context()
    assert await loaders.organization.load("testidi")
    group = await loaders.group.load(group_name)
    assert group
    assert group.name == group_name
    assert group.state.managed == GroupManaged.MANAGED
    assert group.description == f"created by {user}"
    assert group.language == GroupLanguage.EN
