from integrates.api.mutations.add_group_tags import mutate
from integrates.custom_exceptions import ErrorAddingGroupTags
from integrates.decorators import (
    enforce_group_level_auth_async,
    require_asm,
    require_attribute,
    require_login,
)
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    GraphQLResolveInfoFaker,
    GroupAccessFaker,
    GroupAccessStateFaker,
    GroupFaker,
    StakeholderFaker,
)
from integrates.testing.mocks import mocks
from integrates.testing.utils import parametrize, raises

ADMIN_EMAIL = "admin@gmail.com"
USER_EMAIL = "user@gmail.com"
VULNERABILITY_MANAGER_EMAIL = "vulnerability_manager@fluidattacks.com"
HACKER_EMAIL = "hacker@fluidattacks.com"
GROUP_NAME = "test-group"
CUSTOMER_MANAGER_EMAIL = "customer_manager@fluidattacks.com"
REATTACKER_EMAIL = "reattacker@fluidattacks.com"


@parametrize(
    args=["email"],
    cases=[
        [ADMIN_EMAIL],
        [CUSTOMER_MANAGER_EMAIL],
        [VULNERABILITY_MANAGER_EMAIL],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            stakeholders=[
                StakeholderFaker(email=ADMIN_EMAIL),
                StakeholderFaker(email=CUSTOMER_MANAGER_EMAIL),
                StakeholderFaker(email=VULNERABILITY_MANAGER_EMAIL),
            ],
            groups=[
                GroupFaker(name=GROUP_NAME),
            ],
            group_access=[
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email=ADMIN_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="admin"),
                ),
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email=CUSTOMER_MANAGER_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="customer_manager"),
                ),
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email=VULNERABILITY_MANAGER_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="vulnerability_manager"),
                ),
            ],
        )
    )
)
async def test_add_group_tags_success(email: str) -> None:
    info = GraphQLResolveInfoFaker(
        user_email=email,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_asm],
            [require_attribute, "has_asm", GROUP_NAME],
        ],
    )
    tags = ["tag1", "tag2", "tag3"]
    result = await mutate(
        _=None,
        info=info,
        group_name=GROUP_NAME,
        tags_data=tags,
    )
    assert result.success is True
    assert result.group.name == GROUP_NAME


@parametrize(
    args=["email"],
    cases=[
        [HACKER_EMAIL],
        [REATTACKER_EMAIL],
        [USER_EMAIL],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            stakeholders=[
                StakeholderFaker(email=HACKER_EMAIL),
                StakeholderFaker(email=REATTACKER_EMAIL),
            ],
            groups=[
                GroupFaker(name=GROUP_NAME),
            ],
            group_access=[
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email=HACKER_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="hacker"),
                ),
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email=REATTACKER_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="reattacker"),
                ),
            ],
        )
    )
)
async def test_add_group_tags_unauthorized(email: str) -> None:
    info = GraphQLResolveInfoFaker(
        user_email=email,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_asm],
            [require_attribute, "has_asm", GROUP_NAME],
        ],
    )
    tags = ["tag1", "tag2"]
    with raises(Exception, match="Access denied"):
        await mutate(
            _=None,
            info=info,
            group_name=GROUP_NAME,
            tags_data=tags,
        )


@parametrize(
    args=["invalid_tags"],
    cases=[
        [[""]],
        [["tag with spaces"]],
        [["tag@invalid"]],
        [["tag#invalid"]],
        [["very_long_tag_that_exceeds_the_maximum_length_allowed_by_the_system"]],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            stakeholders=[
                StakeholderFaker(email=ADMIN_EMAIL),
            ],
            groups=[
                GroupFaker(name=GROUP_NAME),
            ],
            group_access=[
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email=ADMIN_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="admin"),
                ),
            ],
        )
    )
)
async def test_add_group_tags_invalid_format(invalid_tags: list[str]) -> None:
    info = GraphQLResolveInfoFaker(
        user_email=ADMIN_EMAIL,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_asm],
            [require_attribute, "has_asm", GROUP_NAME],
        ],
    )
    with raises(ErrorAddingGroupTags):
        await mutate(
            _=None,
            info=info,
            group_name=GROUP_NAME,
            tags_data=invalid_tags,
        )
