from typing import (
    Any,
)

from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.api.types import (
    APP_EXCEPTIONS,
)
from integrates.custom_exceptions import (
    NumberOutOfRange,
    ToeLinesAlreadyUpdated,
    ToeLinesNotFound,
)
from integrates.custom_utils import (
    logs as logs_utils,
)
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.toe_lines.types import (
    ToeLineRequest,
)
from integrates.decorators import (
    concurrent_decorators,
    enforce_group_level_auth_async,
    require_asm,
    require_login,
)
from integrates.sessions import (
    domain as sessions_domain,
)
from integrates.toe.lines import (
    domain as toe_lines_domain,
)
from integrates.toe.lines.types import (
    ToeLinesAttributesToUpdate,
)

from .payloads.types import (
    UpdateToeLinesPayload,
)
from .schema import (
    MUTATION,
)

LOC_RANGE = {min: 1, max: 10**6}


@MUTATION.field("updateToeLinesLoc")
@concurrent_decorators(
    require_login,
    enforce_group_level_auth_async,
    require_asm,
)
async def mutate(
    _parent: None,
    info: GraphQLResolveInfo,
    comments: str,
    filename: str,
    group_name: str,
    root_id: str,
    **kwargs: Any,
) -> UpdateToeLinesPayload:
    try:
        user_info = await sessions_domain.get_jwt_content(info.context)
        user_email: str = user_info["user_email"]
        loaders: Dataloaders = info.context.loaders

        loc = kwargs.get("loc")
        if loc is None or not LOC_RANGE[min] <= loc <= LOC_RANGE[max]:
            raise NumberOutOfRange(LOC_RANGE[min], LOC_RANGE[max], True)

        current_value = await loaders.toe_lines.load(
            ToeLineRequest(filename=filename, group_name=group_name, root_id=root_id),
        )
        if current_value is None:
            raise ToeLinesNotFound()

        if current_value.state.loc != 0:
            raise ToeLinesAlreadyUpdated()

        await toe_lines_domain.update(
            current_value,
            ToeLinesAttributesToUpdate(
                comments=comments,
                loc=loc,
                modified_by=user_email,
            ),
        )
        logs_utils.cloudwatch_log(
            info.context,
            "Updated toe lines in the group",
            extra={"group_name": group_name, "log_type": "Security"},
        )
    except APP_EXCEPTIONS:
        logs_utils.cloudwatch_log(
            info.context,
            "Attempted to update toe lines in the group",
            extra={"group_name": group_name, "log_type": "Security"},
        )
        raise

    return UpdateToeLinesPayload(
        success=True,
        group_name=group_name,
        filename=filename,
        root_id=root_id,
    )
