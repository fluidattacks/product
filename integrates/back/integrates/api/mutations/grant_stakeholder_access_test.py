from datetime import datetime

from integrates.api.mutations.grant_stakeholder_access import mutate
from integrates.custom_exceptions import InvalidRoleProvided, StakeholderHasGroupAccess
from integrates.decorators import (
    enforce_group_level_auth_async,
    require_asm,
    require_attribute,
    require_attribute_internal,
    require_is_not_under_review,
    require_login,
)
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    GraphQLResolveInfoFaker,
    GroupAccessFaker,
    GroupAccessStateFaker,
    GroupFaker,
    OrganizationFaker,
    StakeholderFaker,
)
from integrates.testing.mocks import mocks
from integrates.testing.utils import raises

GROUP_NAME = "test-group"


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            stakeholders=[
                StakeholderFaker(email="admin@fluidattacks.com", role="admin"),
                StakeholderFaker(email="new@fluidattacks.com", role="user"),
            ],
            groups=[GroupFaker(name=GROUP_NAME, organization_id="org1")],
        ),
    )
)
async def test_grant_stakeholder_access_success() -> None:
    info = GraphQLResolveInfoFaker(
        user_email="admin@fluidattacks.com",
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_is_not_under_review],
            [require_attribute_internal, "is_under_review", GROUP_NAME],
            [require_asm],
            [require_attribute, "has_asm", GROUP_NAME],
        ],
    )
    result = await mutate(
        _=None,
        info=info,
        group_name=GROUP_NAME,
        role="reviewer",
        email="new@fluidattacks.com",
        responsibility="QA Testing",
    )
    assert result.success is True
    assert result.granted_stakeholder.email == "new@fluidattacks.com"


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            stakeholders=[
                StakeholderFaker(email="admin@fluidattacks.com", role="admin"),
                StakeholderFaker(email="analyst@fluidattacks.com", role="analyst"),
            ],
            groups=[GroupFaker(name=GROUP_NAME, organization_id="org1")],
        ),
    )
)
async def test_grant_stakeholder_access_invalid_role() -> None:
    info = GraphQLResolveInfoFaker(
        user_email="admin@fluidattacks.com",
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_is_not_under_review],
            [require_attribute_internal, "is_under_review", GROUP_NAME],
            [require_asm],
            [require_attribute, "has_asm", GROUP_NAME],
        ],
    )
    with raises(InvalidRoleProvided):
        await mutate(
            _=None,
            info=info,
            group_name=GROUP_NAME,
            role="invalid_role",
            email="new@fluidattacks.com",
            responsibility="Testing",
        )


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            stakeholders=[StakeholderFaker(email="admin@fluidattacks.com", role="admin")],
            groups=[GroupFaker(name=GROUP_NAME, organization_id="org1")],
            group_access=[
                GroupAccessFaker(
                    email="existing@fluidattacks.com",
                    group_name=GROUP_NAME,
                    state=GroupAccessStateFaker(has_access=True),
                ),
            ],
        ),
    )
)
async def test_grant_stakeholder_access_existing_member() -> None:
    info = GraphQLResolveInfoFaker(
        user_email="admin@fluidattacks.com",
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_is_not_under_review],
            [require_attribute_internal, "is_under_review", GROUP_NAME],
            [require_asm],
            [require_attribute, "has_asm", GROUP_NAME],
        ],
    )
    with raises(StakeholderHasGroupAccess):
        await mutate(
            _=None,
            info=info,
            group_name=GROUP_NAME,
            role="analyst",
            email="existing@fluidattacks.com",
            responsibility="Testing",
        )


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            stakeholders=[StakeholderFaker(email="admin@fluidattacks.com", role="admin")],
            groups=[GroupFaker(name=GROUP_NAME, organization_id="org1")],
            group_access=[
                GroupAccessFaker(
                    email="pending@fluidattacks.com",
                    group_name=GROUP_NAME,
                    state=GroupAccessStateFaker(has_access=False),
                    expiration_time=int(datetime.now().timestamp()),
                ),
            ],
        ),
    )
)
async def test_grant_stakeholder_access_pending_invitation() -> None:
    info = GraphQLResolveInfoFaker(
        user_email="admin@fluidattacks.com",
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_is_not_under_review],
            [require_attribute_internal, "is_under_review", GROUP_NAME],
            [require_asm],
            [require_attribute, "has_asm", GROUP_NAME],
        ],
    )
    with raises(Exception):
        await mutate(
            _=None,
            info=info,
            group_name=GROUP_NAME,
            role="analyst",
            email="pending@fluidattacks.com",
            responsibility="Testing",
        )
