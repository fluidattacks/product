from aioextensions import (
    collect,
)
from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.custom_exceptions import GroupNotFound
from integrates.custom_utils import (
    logs as logs_utils,
)
from integrates.custom_utils.groups import get_group
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.types import (
    PoliciesToUpdate,
)
from integrates.db_model.utils import (
    format_policies_to_update,
)
from integrates.decorators import (
    enforce_organization_level_auth_async,
)
from integrates.groups.domain import (
    update_policies,
)
from integrates.sessions import (
    domain as sessions_domain,
)

from .payloads.types import (
    SimplePayload,
)
from .schema import (
    MUTATION,
)


@MUTATION.field("updateGroupsPolicies")
@enforce_organization_level_auth_async
async def mutate(
    _parent: None,
    info: GraphQLResolveInfo,
    *,
    organization_id: str,
    groups: list[str],
    **kwargs: str,
) -> SimplePayload:
    loaders: Dataloaders = info.context.loaders
    user_data = await sessions_domain.get_jwt_content(info.context)
    email = user_data["user_email"]
    policies_to_update = format_policies_to_update(kwargs)

    await collect(
        [
            _update_group(
                group_name=group_name,
                organization_id=organization_id,
                user_email=email,
                info=info,
                policies=policies_to_update,
                loaders=loaders,
            )
            for group_name in groups
        ]
    )

    return SimplePayload(success=True)


@enforce_organization_level_auth_async
async def _update_group(
    *,
    group_name: str,
    organization_id: str,
    user_email: str,
    info: GraphQLResolveInfo,
    policies: PoliciesToUpdate,
    loaders: Dataloaders,
) -> None:
    group = await get_group(loaders, group_name.lower())
    if group.organization_id != organization_id:
        raise GroupNotFound()

    await update_policies(
        loaders=loaders,
        email=user_email,
        group_name=group_name,
        organization_id=group.organization_id,
        policies_to_update=policies,
    )
    logs_utils.cloudwatch_log(
        info.context,
        "Updated policies in the group",
        extra={"group_name": group_name, "log_type": "Security"},
    )
