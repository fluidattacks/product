from datetime import (
    datetime,
)
from typing import (
    TypedDict,
    Unpack,
)

from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.batch import (
    dal as batch_dal,
)
from integrates.batch.enums import (
    Action,
    IntegratesBatchQueue,
)
from integrates.batch.types import (
    BatchProcessingToUpdate,
    DependentAction,
)
from integrates.custom_exceptions import (
    InvalidParameter,
)
from integrates.custom_utils import (
    logs as logs_utils,
)
from integrates.custom_utils import (
    roots as roots_utils,
)
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.roots.enums import (
    RootCloningStatus,
)
from integrates.db_model.roots.types import (
    GitRoot,
)
from integrates.decorators import (
    concurrent_decorators,
    enforce_group_level_auth_async,
    rename_kwargs,
    require_is_not_under_review,
    require_login,
)
from integrates.jobs_orchestration.create_machine_config import (
    MachineJobOrigin,
    MachineJobTechnique,
    MachineModulesToExecute,
    generate_batch_action_config,
)
from integrates.roots.utils import (
    update_root_cloning_status,
)
from integrates.sessions import (
    domain as sessions_domain,
)

from .payloads.types import (
    SimplePayload,
)
from .schema import (
    MUTATION,
)


class UpdateRootCloningStatusArgs(TypedDict):
    group_name: str
    root_id: str
    status: str
    message: str
    queue_machine: bool | None
    commit: str | None
    commit_date: datetime | None


async def queue_required_actions(
    *,
    loaders: Dataloaders,
    commit: str | None,
    cloning_status: RootCloningStatus,
    queue_machine: bool,
    root: GitRoot,
    user_email: str,
) -> None:
    if cloning_status != RootCloningStatus.OK or not commit or commit == root.cloning.commit:
        return

    queued_action = await batch_dal.put_action(
        action=Action.REFRESH_TOE_LINES,
        attempt_duration_seconds=7200,
        entity=root.group_name,
        subject=user_email,
        additional_info={"root_ids": [root.id]},
        queue=IntegratesBatchQueue.SMALL,
    )
    if queue_machine and queued_action.batch_job_id and queued_action.dynamo_pk:
        (
            additional_info,
            job_name,
            batch_queue,
        ) = await generate_batch_action_config(
            loaders=loaders,
            group_name=root.group_name,
            root_nicknames=[root.state.nickname],
            modules_to_execute=MachineModulesToExecute(
                APK=False,
                CSPM=False,
                DAST=False,
                SAST=True,
                SCA=True,
            ),
            job_origin_and_technique=(
                MachineJobOrigin.POSTCLONE,
                MachineJobTechnique.SAST,
            ),
        )
        await batch_dal.update_action_to_dynamodb(
            action=Action.REFRESH_TOE_LINES,
            action_key=queued_action.dynamo_pk,
            attributes=BatchProcessingToUpdate(
                dependent_actions=[
                    DependentAction(
                        action_name=Action.EXECUTE_MACHINE,
                        additional_info=additional_info,
                        queue=batch_queue,
                        attempt_duration_seconds=43200,
                        job_name=job_name,
                    ),
                ],
            ),
        )


@MUTATION.field("updateRootCloningStatus")
@concurrent_decorators(
    require_login,
    enforce_group_level_auth_async,
    require_is_not_under_review,
)
@rename_kwargs({"id": "root_id"})
async def mutate(
    _: None,
    info: GraphQLResolveInfo,
    **kwargs: Unpack[UpdateRootCloningStatusArgs],
) -> SimplePayload:
    loaders: Dataloaders = info.context.loaders
    user_info = await sessions_domain.get_jwt_content(info.context)
    user_email = user_info["user_email"]
    group_name = kwargs["group_name"]
    root = await roots_utils.get_root(loaders, kwargs["root_id"], group_name)
    commit = kwargs.get("commit")
    if not isinstance(root, GitRoot):
        raise InvalidParameter()

    cloning_status = RootCloningStatus(kwargs["status"])
    queue_machine = bool(kwargs.get("queue_machine"))
    await queue_required_actions(
        loaders=loaders,
        commit=commit,
        cloning_status=cloning_status,
        queue_machine=queue_machine,
        root=root,
        user_email=user_email,
    )
    await update_root_cloning_status(
        loaders=loaders,
        group_name=group_name,
        root_id=root.id,
        status=cloning_status,
        message=kwargs["message"],
        commit=commit,
        commit_date=kwargs.get("commit_date"),
        modified_by=user_email,
    )
    logs_utils.cloudwatch_log(
        info.context,
        "Updated root cloning status",
        extra={
            "group_name": group_name,
            "root_id": root.id,
            "root_nickname": root.state.nickname,
            "user_email": user_email,
            "log_type": "Security",
        },
    )

    return SimplePayload(success=True)
