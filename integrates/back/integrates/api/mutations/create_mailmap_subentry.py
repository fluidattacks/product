from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.audit import AuditEvent, add_audit_event
from integrates.custom_utils import (
    logs as logs_utils,
)
from integrates.db_model.mailmap.types import (
    MailmapSubentry,
)
from integrates.decorators import (
    concurrent_decorators,
    enforce_organization_level_auth_async,
    require_login,
)
from integrates.mailmap import (
    create_mailmap_subentry,
)
from integrates.sessions.domain import get_jwt_content

from .payloads.types import (
    SimplePayload,
)
from .schema import (
    MUTATION,
)


@MUTATION.field("createMailmapSubentry")
@concurrent_decorators(
    require_login,
    enforce_organization_level_auth_async,
)
async def mutate(
    _parent: None,
    info: GraphQLResolveInfo,
    subentry: MailmapSubentry,
    entry_email: str,
    organization_id: str,
) -> SimplePayload:
    await create_mailmap_subentry(
        subentry=subentry,
        entry_email=entry_email,
        organization_id=organization_id,
    )

    logs_utils.cloudwatch_log(
        info.context,
        "Created mailmap alias",
        extra={
            "subentry_email": subentry["mailmap_subentry_email"],
            "entry_email": entry_email,
            "organization_id": organization_id,
        },
    )
    user_info = await get_jwt_content(info.context)
    add_audit_event(
        AuditEvent(
            action="CREATE",
            author=user_info["user_email"],
            metadata={
                "entry_email": entry_email,
                "organization_id": organization_id,
                "subentry_email": subentry["mailmap_subentry_email"],
            },
            object="MailmapSubentry",
            object_id=subentry["mailmap_subentry_email"],
        )
    )

    return SimplePayload(success=True)
