import integrates.decorators
from integrates.api.mutations.add_organization import mutate
from integrates.dataloaders import Dataloaders, get_new_context
from integrates.decorators import require_corporate_email, require_login
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import GraphQLResolveInfoFaker, StakeholderFaker
from integrates.testing.mocks import Mock, mocks

EMAIL_TEST = "jdoe@corporate.com"
ORG_NAME = "orgtest"


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            stakeholders=[StakeholderFaker(email=EMAIL_TEST, role="customer_manager")],
        ),
    ),
    others=[Mock(integrates.decorators, "is_personal_email", "async", False)],
)
async def test_add_organization() -> None:
    # Arrange
    info = GraphQLResolveInfoFaker(
        user_email=EMAIL_TEST,
        decorators=[[require_login], [require_corporate_email]],
    )

    # Act
    loaders: Dataloaders = get_new_context()
    result = await mutate(
        _parent=None,
        info=info,
        name=ORG_NAME,
    )
    organization = await loaders.organization.load(ORG_NAME)

    # Assert
    assert result.success
    assert organization
    assert organization.name == ORG_NAME
    assert organization.created_by == EMAIL_TEST
