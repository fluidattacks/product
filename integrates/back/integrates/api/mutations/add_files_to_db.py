import logging
import logging.config

from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.custom_exceptions import (
    DocumentNotFound,
    ErrorFileNameAlreadyExists,
    InvalidChar,
)
from integrates.custom_utils import (
    logs as logs_utils,
)
from integrates.custom_utils.utils import (
    camel_case_list_dict,
)
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.decorators import (
    concurrent_decorators,
    enforce_group_level_auth_async,
    require_asm,
    require_is_not_under_review,
    require_login,
)
from integrates.groups import (
    domain as groups_domain,
)
from integrates.sessions import (
    domain as sessions_domain,
)

from .payloads.types import (
    SimplePayload,
)
from .schema import (
    MUTATION,
)

LOGGER = logging.getLogger(__name__)


@MUTATION.field("addFilesToDb")
@concurrent_decorators(
    require_login,
    enforce_group_level_auth_async,
    require_is_not_under_review,
    require_asm,
)
async def mutate(
    _: None,
    info: GraphQLResolveInfo,
    group_name: str,
    **kwargs: list[dict[str, str]],
) -> SimplePayload:
    loaders: Dataloaders = info.context.loaders
    group_name = str(group_name).lower()
    files_data = kwargs["files_data_input"]
    new_files_data = camel_case_list_dict(files_data)
    user_info = await sessions_domain.get_jwt_content(info.context)
    user_email = user_info["user_email"]

    try:
        for file_data in new_files_data:
            await groups_domain.add_file(
                loaders=loaders,
                description=file_data["description"],
                file_name=file_data["fileName"],
                group_name=group_name,
                email=user_email,
            )

    except (InvalidChar, ErrorFileNameAlreadyExists, DocumentNotFound):
        LOGGER.error("Couldn't add the file to the db", extra={"extra": kwargs})
        logs_utils.cloudwatch_log(
            info.context,
            "Attempted to add resource files to the group",
            extra={
                "group_name": group_name,
                "log_type": "Security",
            },
        )

        raise

    logs_utils.cloudwatch_log(
        info.context,
        "Added file to db in the group",
        extra={
            "group_name": group_name,
            "files_data_input": kwargs["files_data_input"],
            "log_type": "Security",
        },
    )

    return SimplePayload(success=True)
