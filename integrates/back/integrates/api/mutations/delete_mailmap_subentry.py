from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.audit import AuditEvent, add_audit_event
from integrates.custom_utils import (
    logs as logs_utils,
)
from integrates.decorators import (
    concurrent_decorators,
    enforce_organization_level_auth_async,
    require_login,
)
from integrates.mailmap import (
    delete_mailmap_subentry,
)
from integrates.sessions.domain import get_jwt_content

from .payloads.types import (
    SimplePayload,
)
from .schema import (
    MUTATION,
)


@MUTATION.field("deleteMailmapSubentry")
@concurrent_decorators(
    require_login,
    enforce_organization_level_auth_async,
)
async def mutate(
    _parent: None,
    info: GraphQLResolveInfo,
    entry_email: str,
    subentry_email: str,
    subentry_name: str,
    organization_id: str,
) -> SimplePayload:
    await delete_mailmap_subentry(
        entry_email=entry_email,
        subentry_email=subentry_email,
        subentry_name=subentry_name,
        organization_id=organization_id,
    )

    logs_utils.cloudwatch_log(
        info.context,
        "Deleted mailmap alias",
        extra={
            "entry_email": entry_email,
            "subentry_email": subentry_email,
            "organization_id": organization_id,
        },
    )
    user_info = await get_jwt_content(info.context)
    add_audit_event(
        AuditEvent(
            action="DELETE",
            author=user_info["user_email"],
            metadata={
                "entry_email": entry_email,
                "organization_id": organization_id,
                "subentry_email": subentry_email,
            },
            object="MailmapSubentry",
            object_id=subentry_email,
        )
    )

    return SimplePayload(success=True)
