from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.billing import (
    domain as billing_domain,
)
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.decorators import (
    concurrent_decorators,
    enforce_organization_level_auth_async,
    require_login,
)
from integrates.organizations import (
    utils as orgs_utils,
)

from .payloads.types import (
    SimplePayload,
)
from .schema import (
    MUTATION,
)


@MUTATION.field("removePaymentMethod")
@concurrent_decorators(
    require_login,
    enforce_organization_level_auth_async,
)
async def mutate(
    _parent: None,
    info: GraphQLResolveInfo,
    **kwargs: str,
) -> SimplePayload:
    loaders: Dataloaders = info.context.loaders
    organization = await orgs_utils.get_organization(loaders, kwargs["organization_id"])

    return SimplePayload(
        success=await billing_domain.remove_payment_method(
            org=organization,
            payment_method_id=kwargs["payment_method_id"],
        ),
    )
