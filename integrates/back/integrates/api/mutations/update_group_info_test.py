from datetime import datetime, timedelta

from graphql import GraphQLError

from integrates.api.mutations.update_group_info import mutate
from integrates.custom_exceptions import InvalidDate, InvalidParameter
from integrates.decorators import (
    enforce_group_level_auth_async,
    require_asm,
    require_attribute,
    require_login,
)
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    GraphQLResolveInfoFaker,
    GroupAccessFaker,
    GroupAccessStateFaker,
    GroupFaker,
    OrganizationFaker,
    StakeholderFaker,
)
from integrates.testing.mocks import mocks
from integrates.testing.utils import raises

GROUP_NAME = "test-group"
USER_EMAIL = "user@gmail.com"


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(id="org1"),
            ],
            stakeholders=[
                StakeholderFaker(email=USER_EMAIL, role="user"),
            ],
            groups=[
                GroupFaker(name=GROUP_NAME, organization_id="org1"),
            ],
            group_access=[
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email=USER_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="admin"),
                ),
            ],
        ),
    )
)
async def test_update_group_info_invalid_description() -> None:
    info = GraphQLResolveInfoFaker(
        user_email=USER_EMAIL,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_asm],
            [require_attribute, "has_asm", GROUP_NAME],
        ],
    )
    with raises(InvalidParameter):
        await mutate(
            _=None,
            info=info,
            description="   ",
            group_name=GROUP_NAME,
            language="en",
        )


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(id="org1"),
            ],
            stakeholders=[
                StakeholderFaker(email=USER_EMAIL, role="user"),
            ],
            groups=[
                GroupFaker(name=GROUP_NAME, organization_id="org1"),
            ],
            group_access=[
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email=USER_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="admin"),
                ),
            ],
        ),
    )
)
async def test_update_group_info_future_sprint_date() -> None:
    info = GraphQLResolveInfoFaker(
        user_email=USER_EMAIL,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_asm],
            [require_attribute, "has_asm", GROUP_NAME],
        ],
    )
    future_date = datetime.now() + timedelta(days=30)
    with raises(InvalidDate):
        await mutate(
            _=None,
            info=info,
            description="Valid description",
            group_name=GROUP_NAME,
            language="en",
            sprint_start_date=future_date,
        )


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(id="org1"),
            ],
            stakeholders=[
                StakeholderFaker(email=USER_EMAIL, role="user"),
            ],
            groups=[
                GroupFaker(name=GROUP_NAME, organization_id="org1"),
            ],
            group_access=[
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email=USER_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="hacker"),
                ),
            ],
        ),
    )
)
async def test_update_group_info_permission_denied() -> None:
    info = GraphQLResolveInfoFaker(
        user_email=USER_EMAIL,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_asm],
            [require_attribute, "has_asm", GROUP_NAME],
        ],
    )
    with raises(GraphQLError, match="Access denied"):
        await mutate(
            _=None,
            info=info,
            description="Valid description",
            group_name=GROUP_NAME,
            language="en",
        )


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(id="org1"),
            ],
            stakeholders=[
                StakeholderFaker(email=USER_EMAIL, role="user"),
            ],
            groups=[
                GroupFaker(name=GROUP_NAME, organization_id="org1"),
            ],
            group_access=[
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email=USER_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="admin"),
                ),
            ],
        ),
    )
)
async def test_update_group_info_success_with_optional_params() -> None:
    info = GraphQLResolveInfoFaker(
        user_email=USER_EMAIL,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_asm],
            [require_attribute, "has_asm", GROUP_NAME],
        ],
    )
    result = await mutate(
        _=None,
        info=info,
        description="Valid description",
        group_name=GROUP_NAME,
        language="es",
        business_id="123",
        business_name="Test Business",
        sprint_duration=10,
        sprint_start_date=datetime.now() - timedelta(days=1),
    )
    assert result.success
