from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.api.types import (
    APP_EXCEPTIONS,
)
from integrates.custom_utils import (
    logs as logs_utils,
)
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.decorators import (
    concurrent_decorators,
    enforce_group_level_auth_async,
    require_is_not_under_review,
    require_login,
    require_service_white,
)
from integrates.roots.domain import (
    remove_docker_image,
)
from integrates.sessions import (
    domain as sessions_domain,
)

from .payloads.types import (
    SimplePayload,
)
from .schema import (
    MUTATION,
)


@MUTATION.field("removeRootDockerImage")
@concurrent_decorators(
    require_login,
    enforce_group_level_auth_async,
    require_is_not_under_review,
    require_service_white,
)
async def mutate(
    _parent: None,
    info: GraphQLResolveInfo,
    group_name: str,
    root_id: str,
    uri: str,
    **_kwargs: None,
) -> SimplePayload:
    loaders: Dataloaders = info.context.loaders
    user_info = await sessions_domain.get_jwt_content(info.context)
    user_email = user_info["user_email"]
    try:
        await remove_docker_image(
            loaders=loaders,
            root_id=root_id,
            uri=uri,
            user_email=user_email,
            group_name=group_name,
        )
        logs_utils.cloudwatch_log(
            info.context,
            "Removed Docker Image of the root",
            extra={
                "uri": uri,
                "root_id": root_id,
                "log_type": "Security",
            },
        )
    except APP_EXCEPTIONS:
        logs_utils.cloudwatch_log(
            info.context,
            "Attempted to remove Docker image of the root",
            extra={
                "uri": uri,
                "root_id": root_id,
                "log_type": "Security",
            },
        )
        raise
    return SimplePayload(success=True)
