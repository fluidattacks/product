from integrates.api.mutations.activate_root import mutate
from integrates.batch.dal.get import get_actions_by_name
from integrates.batch.enums import Action
from integrates.dataloaders import get_new_context
from integrates.db_model.groups.enums import GroupService
from integrates.db_model.roots.enums import RootStatus
from integrates.db_model.roots.types import GitRoot, RootRequest
from integrates.decorators import (
    enforce_group_level_auth_async,
    require_attribute,
    require_attribute_internal,
    require_login,
)
from integrates.roots import utils as roots_utils
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    GitRootFaker,
    GitRootStateFaker,
    GraphQLResolveInfoFaker,
    GroupAccessFaker,
    GroupAccessStateFaker,
    GroupFaker,
    GroupStateFaker,
    IPRootFaker,
    IPRootStateFaker,
    OrganizationFaker,
    StakeholderFaker,
    UrlRootFaker,
    UrlRootStateFaker,
)
from integrates.testing.mocks import Mock, mocks
from integrates.testing.utils import parametrize, raises

EMAIL_TEST = "admin@test.test"
GROUP_NAME = "group1"
ROOT_ID = "root1"


@parametrize(
    args=["email"],
    cases=[["hacker@test.test"], ["reattacker@test.test"], ["user@test.test"]],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            stakeholders=[
                StakeholderFaker(email="hacker@test.test", role="hacker"),
                StakeholderFaker(email="reattacker@test.test", role="reattacker"),
                StakeholderFaker(email="uset@test.test", role="user"),
            ],
            groups=[GroupFaker(name=GROUP_NAME, organization_id="org1")],
            group_access=[
                GroupAccessFaker(
                    email="hacker@test.test",
                    state=GroupAccessStateFaker(has_access=True, role="hacker"),
                ),
                GroupAccessFaker(
                    email="reattacker@test.test",
                    state=GroupAccessStateFaker(has_access=True, role="reattacker"),
                ),
                GroupAccessFaker(
                    email="user@test.test",
                    state=GroupAccessStateFaker(has_access=True, role="user"),
                ),
            ],
            roots=[
                GitRootFaker(
                    group_name=GROUP_NAME, id="root1", state=GitRootStateFaker(nickname="back")
                )
            ],
        ),
    )
)
async def test_activate_root_access_denied(email: str) -> None:
    info = GraphQLResolveInfoFaker(
        user_email=email,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_attribute, "has_service_white", GROUP_NAME],
            [require_attribute_internal, "is_under_review", GROUP_NAME],
        ],
    )

    with raises(Exception, match="Access denied"):
        await mutate(_parent=None, info=info, group_name=GROUP_NAME, id="root1")


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            stakeholders=[StakeholderFaker(email="admin@test.test", role="admin")],
            groups=[GroupFaker(name=GROUP_NAME, organization_id="org1")],
            roots=[
                GitRootFaker(
                    group_name=GROUP_NAME,
                    id="root1",
                    state=GitRootStateFaker(
                        credential_id="x", nickname="back", status=RootStatus.INACTIVE
                    ),
                ),
            ],
        ),
    ),
    others=[Mock(roots_utils, "ls_remote_root", "async", "x")],
)
async def test_activate_git_root() -> None:
    info = GraphQLResolveInfoFaker(
        user_email="admin@test.test",
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_attribute, "has_service_white", GROUP_NAME],
            [require_attribute_internal, "is_under_review", GROUP_NAME],
        ],
    )
    result = await mutate(_parent=None, info=info, group_name=GROUP_NAME, id="root1")

    loaders = get_new_context()
    root = await loaders.root.load(RootRequest(GROUP_NAME, "root1"))

    assert result.success is True
    assert isinstance(root, GitRoot)
    assert root.state.status == RootStatus.ACTIVE
    assert root.cloning.status.value == "QUEUED"
    assert root.cloning.reason == "Cloning queued..."
    assert (
        len(await get_actions_by_name(action_name=Action.REFRESH_REPOSITORIES, entity=GROUP_NAME))
        == 1
    )
    assert (
        len(await get_actions_by_name(action_name=Action.REFRESH_TOE_INPUTS, entity=GROUP_NAME))
        == 1
    )


@parametrize(
    args=["root_id", "action"],
    cases=[["root2", Action.REFRESH_TOE_INPUTS], ["root3", Action.REFRESH_TOE_PORTS]],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            stakeholders=[StakeholderFaker(email="admin@test.test", role="admin")],
            groups=[
                GroupFaker(
                    name=GROUP_NAME,
                    organization_id="org1",
                    state=GroupStateFaker(service=GroupService.BLACK),
                )
            ],
            roots=[
                UrlRootFaker(
                    root_id="root2",
                    group_name=GROUP_NAME,
                    state=UrlRootStateFaker(status=RootStatus.INACTIVE),
                ),
                IPRootFaker(
                    id="root3",
                    group_name=GROUP_NAME,
                    state=IPRootStateFaker(status=RootStatus.INACTIVE),
                ),
            ],
        ),
    )
)
async def test_activate_root(root_id: str, action: Action) -> None:
    info = GraphQLResolveInfoFaker(
        user_email="admin@test.test",
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_attribute, "has_service_black", GROUP_NAME],
            [require_attribute_internal, "is_under_review", GROUP_NAME],
        ],
    )
    result = await mutate(_parent=None, info=info, group_name=GROUP_NAME, id=root_id)

    loaders = get_new_context()
    root = await loaders.root.load(RootRequest(GROUP_NAME, root_id))

    assert result.success is True
    assert root is not None
    assert root.state.status == RootStatus.ACTIVE
    assert len(await get_actions_by_name(action_name=action, entity=GROUP_NAME)) == 1
