from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.custom_utils import (
    logs as logs_utils,
)
from integrates.db_model.roots.get import (
    get_secret_by_key,
)
from integrates.db_model.roots.types import (
    Secret,
)
from integrates.decorators import (
    concurrent_decorators,
    enforce_group_level_auth_async,
    enforce_owner,
    require_is_not_under_review,
    require_login,
)
from integrates.roots import (
    domain as roots_domain,
)
from integrates.verify.enums import (
    AuthenticationLevel,
    ResourceType,
)

from .payloads.types import (
    SimplePayload,
)
from .schema import (
    MUTATION,
)


@enforce_owner(AuthenticationLevel.GROUP)
async def _remove_secret(
    secret: Secret,
    info: GraphQLResolveInfo,
    resource_id: str,
    resource_type: ResourceType,
    group_name: str,
) -> None:
    await roots_domain.remove_secret(
        resource_id=resource_id,
        secret_key=secret.key,
        resource_type=resource_type,
        group_name=group_name,
        loaders=info.context.loaders,
    )
    logs_utils.cloudwatch_log(
        info.context,
        "Removed secret",
        extra={
            "group_name": group_name,
            "removed secret": secret.key,
            "resource type": resource_type,
            "log_type": "Security",
        },
    )


@MUTATION.field("removeSecret")
@concurrent_decorators(
    require_login,
    enforce_group_level_auth_async,
    require_is_not_under_review,
)
async def mutate(
    _: None,
    info: GraphQLResolveInfo,
    key: str,
    resource_id: str,
    resource_type: ResourceType,
    group_name: str,
    **_kwargs: str,
) -> SimplePayload:
    secret = await get_secret_by_key(
        secret_key=key,
        group_name=group_name,
        resource_id=resource_id,
        resource_type=resource_type,
    )

    await _remove_secret(secret, info, resource_id, resource_type, group_name=group_name)

    return SimplePayload(success=True)
