from integrates.api.mutations.update_group_stakeholder import mutate
from integrates.custom_exceptions import InvalidRoleProvided, StakeholderNotFound
from integrates.db_model.group_access.types import GroupInvitation
from integrates.decorators import (
    enforce_group_level_auth_async,
    require_attribute,
    require_attribute_internal,
    require_is_not_under_review,
    require_login,
)
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    GraphQLResolveInfoFaker,
    GroupAccessFaker,
    GroupAccessStateFaker,
    GroupFaker,
    OrganizationFaker,
    StakeholderFaker,
)
from integrates.testing.mocks import mocks
from integrates.testing.utils import parametrize, raises

ADMIN_EMAIL = "admin@gmail.com"
USER_EMAIL = "user@gmail.com"
GROUP_MANAGER = "group_manager@gmail.com"
CUSTOMER_MANAGER = "customer_manager@fluidattacks.com"
GROUP_NAME = "group1"
ORG_ID = "ORG#6b110f25-cd46-e94c-2bad-b9e182cf91a8"


@parametrize(
    args=["granting_email", "modified_email", "modified_role"],
    cases=[
        [ADMIN_EMAIL, USER_EMAIL, "vulnerability_manager"],
        [GROUP_MANAGER, USER_EMAIL, "user"],
        [CUSTOMER_MANAGER, GROUP_MANAGER, "group_manager"],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id=ORG_ID)],
            groups=[
                GroupFaker(name=GROUP_NAME, organization_id=ORG_ID),
            ],
            stakeholders=[
                StakeholderFaker(email=CUSTOMER_MANAGER),
                StakeholderFaker(email=USER_EMAIL),
                StakeholderFaker(email=GROUP_MANAGER),
                StakeholderFaker(email=ADMIN_EMAIL, role="admin"),
            ],
            group_access=[
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email=CUSTOMER_MANAGER,
                    state=GroupAccessStateFaker(has_access=True, role="customer_manager"),
                ),
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email=USER_EMAIL,
                    state=GroupAccessStateFaker(
                        has_access=True,
                        role="user",
                        invitation=GroupInvitation(
                            is_used=False, role="user", url_token="", responsibility=""
                        ),
                    ),
                ),
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email=GROUP_MANAGER,
                    state=GroupAccessStateFaker(has_access=True, role="group_manager"),
                ),
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email=ADMIN_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="admin"),
                ),
            ],
        )
    )
)
async def test_update_group_stakeholder(
    granting_email: str, modified_email: str, modified_role: str
) -> None:
    info = GraphQLResolveInfoFaker(
        user_email=granting_email,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_is_not_under_review],
            [require_attribute_internal, "is_under_review", GROUP_NAME],
            [require_attribute, "has_asm", GROUP_NAME],
        ],
    )
    result = await mutate(
        _=None,
        info=info,
        group_name=GROUP_NAME,
        email=modified_email,
        organization_id=ORG_ID,
        responsibility="Test",
        role=modified_role,
    )

    assert result.success


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id=ORG_ID)],
            groups=[
                GroupFaker(name=GROUP_NAME, organization_id=ORG_ID),
            ],
            stakeholders=[
                StakeholderFaker(email=ADMIN_EMAIL, enrolled=True, is_registered=True),
                StakeholderFaker(email=USER_EMAIL, enrolled=True, is_registered=True),
            ],
            group_access=[
                GroupAccessFaker(
                    email=ADMIN_EMAIL,
                    group_name=GROUP_NAME,
                    state=GroupAccessStateFaker(role="admin"),
                ),
                GroupAccessFaker(
                    email=USER_EMAIL,
                    group_name=GROUP_NAME,
                    state=GroupAccessStateFaker(role="user"),
                ),
            ],
        )
    )
)
async def test_update_group_stakeholder_role_fail() -> None:
    info = GraphQLResolveInfoFaker(
        user_email=ADMIN_EMAIL,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_is_not_under_review],
            [require_attribute_internal, "is_under_review", GROUP_NAME],
            [require_attribute, "has_asm", GROUP_NAME],
        ],
    )
    with raises(Exception, match=str(InvalidRoleProvided(role="admin"))):
        await mutate(
            _=None,
            info=info,
            group_name=GROUP_NAME,
            email=USER_EMAIL,
            organization_id=ORG_ID,
            responsibility="Test",
            role="admin",
        )


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id=ORG_ID)],
            groups=[
                GroupFaker(name=GROUP_NAME, organization_id=ORG_ID),
            ],
            stakeholders=[
                StakeholderFaker(email="admin@fluidattacks.com", enrolled=True, is_registered=True),
            ],
            group_access=[
                GroupAccessFaker(
                    email=ADMIN_EMAIL,
                    group_name=GROUP_NAME,
                    state=GroupAccessStateFaker(role="admin"),
                ),
            ],
        )
    )
)
async def test_update_group_stakeholder_fail() -> None:
    info = GraphQLResolveInfoFaker(
        user_email="admin@fluidattacks.com",
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_is_not_under_review],
            [require_attribute_internal, "is_under_review", GROUP_NAME],
            [require_attribute, "has_asm", GROUP_NAME],
        ],
    )
    with raises(Exception, match="Access denied"):
        await mutate(
            _=None,
            info=info,
            group_name=GROUP_NAME,
            email="admin@fluidattacks.com",
            organization_id=ORG_ID,
            responsibility="Test",
            role="user",
        )


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id=ORG_ID)],
            groups=[
                GroupFaker(name=GROUP_NAME, organization_id=ORG_ID),
            ],
            stakeholders=[
                StakeholderFaker(
                    email=ADMIN_EMAIL, enrolled=True, is_registered=True, role="admin"
                ),
            ],
            group_access=[
                GroupAccessFaker(
                    email=ADMIN_EMAIL,
                    group_name=GROUP_NAME,
                    state=GroupAccessStateFaker(role="admin"),
                ),
            ],
        )
    )
)
async def test_update_group_stakeholder_not_found() -> None:
    info = GraphQLResolveInfoFaker(
        user_email=ADMIN_EMAIL,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_is_not_under_review],
            [require_attribute_internal, "is_under_review", GROUP_NAME],
            [require_attribute, "has_asm", GROUP_NAME],
        ],
    )
    with raises(StakeholderNotFound):
        await mutate(
            _=None,
            info=info,
            group_name=GROUP_NAME,
            email="admintest@fluidattacks.com",
            organization_id=ORG_ID,
            responsibility="Test",
            role="user",
        )
