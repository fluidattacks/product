from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.batch import (
    dal as batch_dal,
)
from integrates.batch.enums import (
    Action,
    IntegratesBatchQueue,
)
from integrates.custom_utils import (
    logs as logs_utils,
)
from integrates.custom_utils import (
    roots as roots_utils,
)
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.roots.types import (
    GitRoot,
    IPRoot,
    URLRoot,
)
from integrates.decorators import (
    concurrent_decorators,
    enforce_group_level_auth_async,
    rename_kwargs,
    require_is_not_under_review,
    require_login,
)
from integrates.roots import (
    domain as roots_domain,
)
from integrates.sessions import (
    domain as sessions_domain,
)

from .payloads.types import (
    SimplePayload,
)
from .schema import (
    MUTATION,
)


@MUTATION.field("moveRoot")
@concurrent_decorators(require_login, enforce_group_level_auth_async, require_is_not_under_review)
@rename_kwargs(
    {
        "group_name": "source_group_name",
        "target_group_name": "group_name",
    },
)
@concurrent_decorators(enforce_group_level_auth_async, require_is_not_under_review)
@rename_kwargs(
    {
        "group_name": "target_group_name",
        "source_group_name": "group_name",
        "id": "root_id",
    },
)
async def mutate(
    _parent: None,
    info: GraphQLResolveInfo,
    group_name: str,
    root_id: str,
    target_group_name: str,
) -> SimplePayload:
    loaders: Dataloaders = info.context.loaders
    user_info = await sessions_domain.get_jwt_content(info.context)
    email = user_info["user_email"]
    group_name = group_name.lower()
    target_group_name = target_group_name.lower()
    new_root = await roots_domain.move_root(
        loaders=loaders,
        email=email,
        group_name=group_name,
        root_id=root_id,
        target_group_name=target_group_name,
    )
    await batch_dal.put_action(
        action=Action.MOVE_ROOT,
        entity=group_name,
        subject=email,
        additional_info={
            "target_group_name": target_group_name,
            "target_root_id": new_root.id,
            "source_group_name": group_name,
            "source_root_id": root_id,
        },
        queue=IntegratesBatchQueue.SMALL,
    )
    root = await roots_utils.get_root(loaders, root_id, group_name)
    if isinstance(root, GitRoot):
        await batch_dal.put_action(
            action=Action.REFRESH_TOE_LINES,
            attempt_duration_seconds=7200,
            entity=group_name,
            subject=email,
            additional_info={"root_ids": [root.id]},
            queue=IntegratesBatchQueue.SMALL,
        )
    if isinstance(root, (GitRoot, URLRoot)):
        await batch_dal.put_action(
            action=Action.REFRESH_TOE_INPUTS,
            entity=group_name,
            subject=email,
            additional_info={"root_ids": [root.id]},
            queue=IntegratesBatchQueue.SMALL,
        )
    if isinstance(root, IPRoot):
        await batch_dal.put_action(
            action=Action.REFRESH_TOE_PORTS,
            entity=group_name,
            subject=email,
            additional_info={"root_ids": [root.id]},
            queue=IntegratesBatchQueue.SMALL,
        )

    logs_utils.cloudwatch_log(
        info.context,
        "Moved root to another group",
        extra={
            "source_group_name": group_name,
            "target_group_name": target_group_name,
            "log_type": "Security",
        },
    )

    return SimplePayload(success=True)
