from typing import Any

from integrates.api.mutations.move_root import mutate
from integrates.batch import (
    dal as batch_dal,
)
from integrates.batch.enums import Action
from integrates.dataloaders import get_new_context
from integrates.db_model.roots.enums import RootStateReason
from integrates.db_model.roots.types import RootRequest
from integrates.decorators import (
    enforce_group_level_auth_async,
    require_attribute_internal,
    require_is_not_under_review,
    require_login,
)
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    GitRootFaker,
    GraphQLResolveInfoFaker,
    GroupAccessFaker,
    GroupAccessStateFaker,
    GroupFaker,
    IPRootFaker,
    OrganizationFaker,
    StakeholderFaker,
    UrlRootFaker,
)
from integrates.testing.mocks import mocks
from integrates.testing.utils import parametrize

GROUP_NAME = "group1"
TARGET_GROUP_NAME = "group2"
ORG_ID = "org1"

DEFAULT_DECORATORS: list[list[Any]] = [
    [require_login],
    [enforce_group_level_auth_async],
    [require_is_not_under_review],
    [require_attribute_internal, "is_under_review", "group1"],
    [require_attribute_internal, "is_under_review", "group2"],
]

# Allowed

ADMIN = "admin@gmail.com"
GROUP_MANAGER = "group_manager@gmail.com"


@parametrize(
    args=["email", "root_id", "expected_actions"],
    cases=[
        [ADMIN, "git_root", [Action.REFRESH_TOE_LINES, Action.REFRESH_TOE_INPUTS]],
        [ADMIN, "url_root", [Action.REFRESH_TOE_INPUTS]],
        [GROUP_MANAGER, "ip_root", [Action.REFRESH_TOE_PORTS]],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id=ORG_ID)],
            groups=[
                GroupFaker(name=GROUP_NAME, organization_id=ORG_ID),
                GroupFaker(name=TARGET_GROUP_NAME, organization_id=ORG_ID),
            ],
            roots=[
                GitRootFaker(
                    group_name=GROUP_NAME,
                    id="git_root",
                ),
                UrlRootFaker(
                    group_name=GROUP_NAME,
                    root_id="url_root",
                ),
                IPRootFaker(
                    group_name=GROUP_NAME,
                    id="ip_root",
                ),
            ],
            stakeholders=[
                StakeholderFaker(email=ADMIN),
                StakeholderFaker(email=GROUP_MANAGER),
            ],
            group_access=[
                GroupAccessFaker(
                    email=ADMIN,
                    group_name=GROUP_NAME,
                    state=GroupAccessStateFaker(role="admin"),
                ),
                GroupAccessFaker(
                    email=ADMIN,
                    group_name=TARGET_GROUP_NAME,
                    state=GroupAccessStateFaker(role="admin"),
                ),
                GroupAccessFaker(
                    email=GROUP_MANAGER,
                    group_name=GROUP_NAME,
                    state=GroupAccessStateFaker(role="group_manager"),
                ),
                GroupAccessFaker(
                    email=GROUP_MANAGER,
                    group_name=TARGET_GROUP_NAME,
                    state=GroupAccessStateFaker(role="group_manager"),
                ),
            ],
        )
    ),
)
async def test_move_root(email: str, root_id: str, expected_actions: list[Action]) -> None:
    info = GraphQLResolveInfoFaker(
        user_email=email,
        decorators=DEFAULT_DECORATORS,
    )
    payload = await mutate(
        _parent=None,
        info=info,
        group_name=GROUP_NAME,
        root_id=root_id,
        target_group_name=TARGET_GROUP_NAME,
    )
    assert payload.success

    # Batch assertions
    batch_actions = await batch_dal.get_actions()
    assert len(batch_actions) == len(expected_actions) + 1
    actions_by_name = {action.action_name: action for action in batch_actions}
    assert Action.MOVE_ROOT in actions_by_name
    for action in expected_actions:
        assert action in actions_by_name

    # Root assertions
    loaders = get_new_context()
    source_root = await loaders.root.load(RootRequest(GROUP_NAME, root_id))
    assert source_root
    assert source_root.state.reason == RootStateReason.MOVED_TO_ANOTHER_GROUP

    target_roots = await loaders.group_roots.load(TARGET_GROUP_NAME)
    assert target_roots[0]
    assert target_roots[0].group_name == TARGET_GROUP_NAME
    assert target_roots[0].state.reason == RootStateReason.MOVED_FROM_ANOTHER_GROUP
    assert target_roots[0].type == source_root.type
