from integrates.api.mutations.update_organization_stakeholder import mutate
from integrates.decorators import (
    enforce_organization_level_auth_async,
    require_login,
    require_organization_access,
)
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    GraphQLResolveInfoFaker,
    OrganizationAccessFaker,
    OrganizationAccessStateFaker,
    OrganizationFaker,
    StakeholderFaker,
)
from integrates.testing.mocks import mocks
from integrates.testing.utils import parametrize, raises

ADMIN = "admin@gmail.com"
ORG_MANAGER = "organization_manager@gmail.com"
CUSTOMER_MANAGER = "customer_manager@fluidattacks.com"
GROUP_NAME = "group1"
ORG_ID = "ORG#6b110f25-cd46-e94c-2bad-b9e182cf91a8"


@parametrize(
    args=["email"],
    cases=[[ADMIN], [ORG_MANAGER], [CUSTOMER_MANAGER]],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id=ORG_ID)],
            stakeholders=[
                StakeholderFaker(email=ADMIN, enrolled=True, is_registered=True),
                StakeholderFaker(email=ORG_MANAGER, enrolled=True, is_registered=True),
                StakeholderFaker(email=CUSTOMER_MANAGER, enrolled=True, is_registered=True),
            ],
            organization_access=[
                OrganizationAccessFaker(
                    email=ADMIN,
                    organization_id=ORG_ID,
                    state=OrganizationAccessStateFaker(
                        modified_by=ADMIN, has_access=True, role="admin"
                    ),
                ),
                OrganizationAccessFaker(
                    email=ORG_MANAGER,
                    organization_id=ORG_ID,
                    state=OrganizationAccessStateFaker(
                        modified_by=ORG_MANAGER, has_access=True, role="organization_manager"
                    ),
                ),
                OrganizationAccessFaker(
                    email=CUSTOMER_MANAGER,
                    organization_id=ORG_ID,
                    state=OrganizationAccessStateFaker(
                        modified_by=ADMIN, has_access=True, role="customer_manager"
                    ),
                ),
            ],
        )
    )
)
async def test_update_organization_stakeholder(email: str) -> None:
    info = GraphQLResolveInfoFaker(
        user_email=email,
        decorators=[
            [require_login],
            [require_organization_access],
            [enforce_organization_level_auth_async],
        ],
    )
    result = await mutate(_=None, info=info, user_email=email, organization_id=ORG_ID, role="USER")
    assert result.success


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id=ORG_ID)],
            stakeholders=[
                StakeholderFaker(email=ADMIN, enrolled=True, is_registered=True),
            ],
            organization_access=[
                OrganizationAccessFaker(
                    email=ADMIN,
                    organization_id=ORG_ID,
                    state=OrganizationAccessStateFaker(
                        modified_by=ADMIN, has_access=True, role="admin"
                    ),
                ),
            ],
        )
    )
)
async def test_update_organization_stakeholder_not_found() -> None:
    info = GraphQLResolveInfoFaker(
        user_email=ADMIN,
        decorators=[
            [require_login],
            [require_organization_access],
            [enforce_organization_level_auth_async],
        ],
    )
    with raises(Exception, match="Access denied or stakeholder not found"):
        await mutate(
            _=None, info=info, user_email="admintest@gmail.com", organization_id=ORG_ID, role="USER"
        )


@parametrize(
    args=["email"],
    cases=[
        ["hacker@gmail.com"],
        ["reattacker@gmail.com"],
        ["user@gmail.com"],
        ["vulnerability_manager@gmail.com"],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id=ORG_ID)],
            stakeholders=[
                StakeholderFaker(email=ADMIN, enrolled=True, is_registered=True),
                StakeholderFaker(email=ORG_MANAGER, enrolled=True, is_registered=True),
                StakeholderFaker(email=CUSTOMER_MANAGER, enrolled=True, is_registered=True),
            ],
        )
    )
)
async def test_update_organization_stakeholder_fail(email: str) -> None:
    info = GraphQLResolveInfoFaker(
        user_email=email,
        decorators=[
            [require_login],
            [require_organization_access],
            [enforce_organization_level_auth_async],
        ],
    )
    with raises(Exception, match="Access denied"):
        await mutate(_=None, info=info, user_email=email, organization_id=ORG_ID, role="USER")
