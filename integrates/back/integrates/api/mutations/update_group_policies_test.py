from decimal import Decimal

from integrates.api.mutations.update_group_policies import mutate
from integrates.dataloaders import get_new_context
from integrates.decorators import enforce_organization_level_auth_async, require_login
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    GraphQLResolveInfoFaker,
    GroupFaker,
    OrganizationAccessFaker,
    OrganizationAccessStateFaker,
    OrganizationFaker,
    StakeholderFaker,
)
from integrates.testing.mocks import mocks
from integrates.testing.utils import parametrize, raises

GROUP_NAME = "testgroup"

# Allowed
ORGANIZATION_MANAGER = "organization_manager@gmail.com"

# Unallowed
HACKER = "hacker@gmail.com"
REATTACKER = "reattacker@gmail.com"
USER = "user@gmail.com"

ORGANIZATION_ID = "ORG#org1"


@parametrize(
    args=["email"],
    cases=[
        [HACKER],
        [REATTACKER],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id=ORGANIZATION_ID)],
            groups=[GroupFaker(name=GROUP_NAME, organization_id=ORGANIZATION_ID)],
            stakeholders=[
                StakeholderFaker(email=HACKER),
                StakeholderFaker(email=REATTACKER),
                StakeholderFaker(email=USER),
            ],
            organization_access=[
                OrganizationAccessFaker(
                    organization_id=GROUP_NAME,
                    email=HACKER,
                    state=OrganizationAccessStateFaker(role="hacker"),
                ),
                OrganizationAccessFaker(
                    organization_id=GROUP_NAME,
                    email=REATTACKER,
                    state=OrganizationAccessStateFaker(role="reattacker"),
                ),
                OrganizationAccessFaker(
                    organization_id=GROUP_NAME,
                    email=USER,
                    state=OrganizationAccessStateFaker(role="user"),
                ),
            ],
        ),
    ),
)
async def test_update_group_policies_access_denied(email: str) -> None:
    info = GraphQLResolveInfoFaker(
        user_email=email, decorators=[[require_login], [enforce_organization_level_auth_async]]
    )
    with raises(Exception, match="Access denied"):
        await mutate(_parent=None, info=info, group_name=GROUP_NAME)


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id=ORGANIZATION_ID)],
            groups=[GroupFaker(name=GROUP_NAME, organization_id=ORGANIZATION_ID)],
            stakeholders=[
                StakeholderFaker(email=ORGANIZATION_MANAGER),
            ],
            organization_access=[
                OrganizationAccessFaker(
                    organization_id=ORGANIZATION_ID,
                    email=ORGANIZATION_MANAGER,
                    state=OrganizationAccessStateFaker(role="organization_manager"),
                ),
            ],
        ),
    ),
)
async def test_update_group_policies() -> None:
    kwargs = {
        "max_acceptance_days": 30,
        "max_acceptance_severity": 6.9,
        "max_number_acceptances": 2,
        "min_acceptance_severity": 0.0,
        "min_breaking_severity": 7.0,
        "user": "email",
        "vulnerability_grace_period": 61,
        "days_until_it_breaks": 100,
    }

    info = GraphQLResolveInfoFaker(
        user_email=ORGANIZATION_MANAGER,
        decorators=[[require_login], [enforce_organization_level_auth_async]],
    )
    await mutate(_parent=None, info=info, group_name=GROUP_NAME, **kwargs)
    loaders = get_new_context()
    group = await loaders.group.load(GROUP_NAME)
    assert group
    assert group.policies
    assert group.policies.max_acceptance_days == 30
    assert group.policies.max_acceptance_severity == Decimal("6.9")
    assert group.policies.max_number_acceptances == 2
    assert group.policies.min_acceptance_severity == Decimal("0.0")
    assert group.policies.min_breaking_severity == Decimal("7.0")
    assert group.policies.vulnerability_grace_period == 61
    assert group.policies.days_until_it_breaks == 100
