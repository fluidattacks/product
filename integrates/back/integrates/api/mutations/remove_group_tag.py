from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.custom_exceptions import (
    ErrorUpdatingGroup,
)
from integrates.custom_utils import (
    logs as logs_utils,
)
from integrates.custom_utils.groups import get_group
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.decorators import (
    concurrent_decorators,
    enforce_group_level_auth_async,
    require_asm,
    require_is_not_under_review,
    require_login,
)
from integrates.groups import (
    domain as groups_domain,
)
from integrates.sessions import (
    domain as sessions_domain,
)

from .payloads.types import (
    SimpleGroupPayload,
)
from .schema import (
    MUTATION,
)


@MUTATION.field("removeGroupTag")
@concurrent_decorators(
    require_login,
    enforce_group_level_auth_async,
    require_is_not_under_review,
    require_asm,
)
async def mutate(
    _: None,
    info: GraphQLResolveInfo,
    group_name: str,
    tag: str,
) -> SimpleGroupPayload:
    group_name = group_name.lower()
    loaders: Dataloaders = info.context.loaders
    group = await get_group(loaders, group_name)
    user_info = await sessions_domain.get_jwt_content(info.context)
    email = user_info["user_email"]

    if await groups_domain.is_valid(loaders, group_name) and group.state.tags:
        await groups_domain.remove_tag(
            loaders=loaders,
            email=email,
            group=group,
            tag_to_remove=tag,
        )
        logs_utils.cloudwatch_log(
            info.context,
            "Remove tag in the group",
            extra={
                "group_name": group_name,
                "tag": tag,
                "log_type": "Security",
            },
        )
    else:
        logs_utils.cloudwatch_log(
            info.context,
            "Attempted to remove tag in the group",
            extra={
                "group_name": group_name,
                "tag": tag,
                "log_type": "Security",
            },
        )

        raise ErrorUpdatingGroup.new()

    loaders.group.clear(group_name)
    group = await get_group(loaders, group_name)

    return SimpleGroupPayload(success=True, group=group)
