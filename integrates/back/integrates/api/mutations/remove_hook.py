from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.decorators import (
    concurrent_decorators,
    enforce_group_level_auth_async,
    require_is_not_under_review,
    require_login,
)
from integrates.groups.domain import (
    delete_group_hook,
)

from .payloads.types import (
    SimplePayload,
)
from .schema import (
    MUTATION,
)


@MUTATION.field("removeGroupHook")
@concurrent_decorators(
    require_login,
    enforce_group_level_auth_async,
    require_is_not_under_review,
)
async def mutate(
    _: None,
    info: GraphQLResolveInfo,
    group_name: str,
    hook_id: str,
    **_kwargs: object,
) -> SimplePayload:
    await delete_group_hook(loaders=info.context.loaders, group_name=group_name, hook_id=hook_id)

    return SimplePayload(success=True)
