from integrates.api.mutations.deactivate_root import mutate
from integrates.api.mutations.payloads.types import SimplePayload
from integrates.batch.dal.get import get_actions_by_name
from integrates.batch.enums import Action
from integrates.custom_exceptions import RootNotFound
from integrates.custom_utils.roots import get_unsolved_events_by_root
from integrates.dataloaders import Dataloaders, get_new_context
from integrates.db_model.enums import Source
from integrates.db_model.events.enums import EventType
from integrates.db_model.groups.enums import GroupService, GroupSubscriptionType, GroupTier
from integrates.db_model.roots.enums import RootStatus
from integrates.db_model.roots.types import (
    GitRoot,
    RootDockerImage,
    RootDockerImagesRequest,
    RootEnvironmentUrlsRequest,
)
from integrates.db_model.toe_packages.types import RootToePackagesRequest, ToePackagesConnection
from integrates.db_model.vulnerabilities.enums import VulnerabilityType
from integrates.decorators import (
    enforce_group_level_auth_async,
    require_attribute,
    require_attribute_internal,
    require_login,
)
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    EventFaker,
    FindingFaker,
    GitRootFaker,
    GitRootStateFaker,
    GraphQLResolveInfoFaker,
    GroupAccessFaker,
    GroupAccessStateFaker,
    GroupFaker,
    GroupStateFaker,
    OrganizationAccessFaker,
    OrganizationAccessStateFaker,
    OrganizationFaker,
    RootDockerImageFaker,
    RootDockerImageStateFaker,
    RootEnvironmentUrlFaker,
    StakeholderFaker,
    ToePackageCoordinatesFaker,
    ToePackageFaker,
    UrlRootFaker,
    VulnerabilityFaker,
    VulnerabilityStateFaker,
)
from integrates.testing.mocks import mocks
from integrates.testing.utils import parametrize, raises

ORG_ID = "ORG#6b110f25-cd46-e94c-2bad-b9e182cf91a8"


@parametrize(
    args=[
        "group_name",
        "service",
        "root_id",
        "reason",
        "other",
        "len_active_roots",
    ],
    cases=[
        [
            "testgroup",
            "has_service_white",
            "root1",
            "OTHER",
            "custom reason",
            2,
        ],
        [
            "testgroup2",
            "has_service_black",
            "urlroot1",
            "REGISTERED_BY_MISTAKE",
            None,
            1,
        ],
        [
            "testgroup",
            "has_service_white",
            "root2",
            "REGISTERED_BY_MISTAKE",
            None,
            2,
        ],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id=ORG_ID)],
            groups=[
                GroupFaker(
                    name="testgroup",
                    organization_id=ORG_ID,
                    state=GroupStateFaker(
                        has_essential=False, service=GroupService.WHITE, tier=GroupTier.OTHER
                    ),
                ),
                GroupFaker(
                    name="testgroup2",
                    organization_id=ORG_ID,
                    state=GroupStateFaker(
                        has_essential=False,
                        service=GroupService.BLACK,
                        tier=GroupTier.OTHER,
                        type=GroupSubscriptionType.ONESHOT,
                    ),
                ),
            ],
            roots=[
                GitRootFaker(
                    id="root1",
                    group_name="testgroup",
                    state=GitRootStateFaker(
                        gitignore=["bower_components/*", "node_modules/*"],
                        includes_health_check=True,
                    ),
                ),
                UrlRootFaker(root_id="urlroot1", group_name="testgroup2"),
                GitRootFaker(
                    id="root2",
                    group_name="testgroup",
                    state=GitRootStateFaker(
                        gitignore=["node_modules/*"],
                        includes_health_check=True,
                        url="https://gitlab.com/fluidattacks/asm_1",
                    ),
                ),
            ],
            events=[
                EventFaker(
                    group_name="testgroup",
                    root_id="root1",
                    type=EventType.CLONING_ISSUES,
                )
            ],
            root_environment_urls=[
                RootEnvironmentUrlFaker(
                    url="https://test.com/",
                    group_name="testgroup",
                    root_id="root1",
                ),
                RootEnvironmentUrlFaker(
                    url="https://test.com/test",
                    group_name="testgroup",
                    root_id="root1",
                ),
            ],
            docker_images=[
                RootDockerImageFaker(
                    root_id="root1",
                    group_name="testgroup",
                    uri="test_docker_image1",
                    state=RootDockerImageStateFaker(include=True),
                ),
                RootDockerImageFaker(
                    root_id="root1",
                    group_name="testgroup",
                    uri="test_docker_image2",
                    state=RootDockerImageStateFaker(include=True),
                ),
                RootDockerImageFaker(
                    root_id="root1",
                    group_name="testgroup",
                    uri="test_docker_image3",
                    state=RootDockerImageStateFaker(include=False),
                ),
            ],
            findings=[FindingFaker(group_name="testgroup", id="find1")],
            vulnerabilities=[
                VulnerabilityFaker(
                    group_name="testgroup",
                    root_id="root1",
                    finding_id="find1",
                    hacker_email="machine@fluidattacks.com",
                    id="vuln1",
                    created_by="machine@fluidattacks.com",
                    type=VulnerabilityType.INPUTS,
                    state=VulnerabilityStateFaker(where="https://test.com/", source=Source.MACHINE),
                ),
            ],
            stakeholders=[StakeholderFaker(email="admin@gmail.com", enrolled=True)],
            organization_access=[
                OrganizationAccessFaker(
                    organization_id=ORG_ID,
                    email="admin@gmail.com",
                    state=OrganizationAccessStateFaker(has_access=True, role="admin"),
                ),
            ],
            group_access=[
                GroupAccessFaker(
                    group_name="testgroup",
                    email="admin@gmail.com",
                    state=GroupAccessStateFaker(has_access=True, role="admin"),
                ),
                GroupAccessFaker(
                    group_name="testgroup2",
                    email="admin@gmail.com",
                    state=GroupAccessStateFaker(has_access=True, role="admin"),
                ),
            ],
            toe_packages=[
                ToePackageFaker(
                    group_name="testgroup",
                    root_id="root1",
                    name="test-package",
                    version="1.5.0",
                    outdated=False,
                    licenses=["Apache-1.0"],
                    locations=[
                        ToePackageCoordinatesFaker(
                            path="var/lib/etc",
                            layer="sha256:d4fc045c9e3a848011de66f34b81f0"
                            "52d4f2c15a17bb196d637e526349601820",
                            image_ref="dummy-image",
                        )
                    ],
                ),
                ToePackageFaker(
                    group_name="testgroup",
                    root_id="root1",
                    name="test-package2",
                    version="1.5.0",
                    outdated=False,
                    licenses=["Apache-1.0"],
                    locations=[
                        ToePackageCoordinatesFaker(
                            path="common/config",
                            layer=None,
                            image_ref=None,
                        )
                    ],
                ),
            ],
        )
    )
)
async def test_deactivate_root(
    group_name: str,
    service: str,
    root_id: str,
    reason: str,
    other: str | None,
    len_active_roots: int,
) -> None:
    # Arrange
    loaders: Dataloaders = get_new_context()
    user = "admin@gmail.com"

    info = GraphQLResolveInfoFaker(
        user_email=user,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_attribute_internal, "is_under_review", group_name],
            [require_attribute, service, group_name],
        ],
    )

    # Act
    await mutate(
        _parent=None,
        info=info,
        group_name=group_name,
        reason=reason,
        other=other,
        id=root_id,
    )

    # Assert
    active_group_roots = [
        root
        for root in await loaders.group_roots.load(group_name)
        if root.state.status == RootStatus.ACTIVE
    ]
    assert len(active_group_roots) == len_active_roots - 1
    assert root_id not in [root.id for root in active_group_roots]

    root_env_urls = await loaders.root_environment_urls.load(
        RootEnvironmentUrlsRequest(root_id=root_id, group_name=group_name)
    )
    urls_to_exclude = [url for url in root_env_urls if url.state.include]
    assert len(urls_to_exclude) == 0

    root = await loaders.group_roots.load(group_name)
    assert (
        len(await get_actions_by_name(action_name=Action.DEACTIVATE_ROOT, entity=group_name)) == 1
    )

    if isinstance(root, GitRoot):
        loaders.group_events.clear_all()
        unsolved_group_events = await get_unsolved_events_by_root(
            loaders=loaders,
            group_name=group_name,
        )

        unsolved_root_events = unsolved_group_events.get(root_id, [])
        assert len(unsolved_root_events) == 0

        all_root_docker_images: list[RootDockerImage] = await loaders.root_docker_images.load(
            RootDockerImagesRequest(root_id=root_id, group_name=group_name)
        )
        root_docker_images_to_exclude = [
            docker_image for docker_image in all_root_docker_images if docker_image.state.include
        ]
        assert len(root_docker_images_to_exclude) == 0

        root_toe_packages: ToePackagesConnection = await loaders.root_toe_packages.load(
            RootToePackagesRequest(root_id=root_id, group_name=group_name),
        )
        present_root_toe_packages = [
            toe_package for toe_package in root_toe_packages.edges if toe_package.node.be_present
        ]
        assert len(present_root_toe_packages) == 0


@parametrize(
    args=[
        "group_name",
        "service",
        "root_id",
        "reason",
        "other",
    ],
    cases=[
        [
            "testgroup",
            "has_service_white",
            "root1",
            "OTHER",
            "custom reason",
        ],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id=ORG_ID)],
            groups=[
                GroupFaker(
                    name="testgroup",
                    organization_id=ORG_ID,
                    state=GroupStateFaker(
                        has_essential=False, service=GroupService.WHITE, tier=GroupTier.OTHER
                    ),
                ),
            ],
            roots=[
                GitRootFaker(
                    id="root1",
                    group_name="testgroup",
                    state=GitRootStateFaker(
                        gitignore=["bower_components/*", "node_modules/*"],
                        includes_health_check=True,
                        status=RootStatus.INACTIVE,
                    ),
                ),
            ],
            stakeholders=[StakeholderFaker(email="admin@gmail.com", enrolled=True)],
            organization_access=[
                OrganizationAccessFaker(
                    organization_id=ORG_ID,
                    email="admin@gmail.com",
                    state=OrganizationAccessStateFaker(has_access=True, role="admin"),
                ),
            ],
            group_access=[
                GroupAccessFaker(
                    group_name="testgroup",
                    email="admin@gmail.com",
                    state=GroupAccessStateFaker(has_access=True, role="admin"),
                ),
            ],
        )
    )
)
async def test_deactivate_root_inactive(
    group_name: str,
    service: str,
    root_id: str,
    reason: str,
    other: str | None,
) -> None:
    # Arrange
    user = "admin@gmail.com"

    info = GraphQLResolveInfoFaker(
        user_email=user,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_attribute_internal, "is_under_review", group_name],
            [require_attribute, service, group_name],
        ],
    )

    # Act
    mutation_result: SimplePayload = await mutate(
        _parent=None,
        info=info,
        group_name=group_name,
        reason=reason,
        other=other,
        id=root_id,
    )

    # Assert
    assert mutation_result.success


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id=ORG_ID)],
            groups=[
                GroupFaker(
                    name="testgroup",
                    organization_id=ORG_ID,
                    state=GroupStateFaker(service=GroupService.WHITE, tier=GroupTier.OTHER),
                ),
                GroupFaker(
                    name="testgroup1",
                    organization_id=ORG_ID,
                    state=GroupStateFaker(service=GroupService.WHITE, tier=GroupTier.OTHER),
                ),
            ],
            roots=[
                GitRootFaker(
                    id="root1",
                    group_name="testgroup1",
                    state=GitRootStateFaker(includes_health_check=True, status=RootStatus.INACTIVE),
                ),
            ],
            stakeholders=[StakeholderFaker(email="admin@gmail.com", enrolled=True)],
            organization_access=[
                OrganizationAccessFaker(
                    organization_id=ORG_ID,
                    email="admin@gmail.com",
                    state=OrganizationAccessStateFaker(has_access=True, role="admin"),
                ),
            ],
            group_access=[
                GroupAccessFaker(
                    group_name="testgroup",
                    email="admin@gmail.com",
                    state=GroupAccessStateFaker(has_access=True, role="admin"),
                ),
                GroupAccessFaker(
                    group_name="testgroup1",
                    email="admin@gmail.com",
                    state=GroupAccessStateFaker(has_access=True, role="admin"),
                ),
            ],
        )
    )
)
async def test_deactivate_root_fail() -> None:
    info = GraphQLResolveInfoFaker(
        user_email="admin@gmail.com",
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_attribute_internal, "is_under_review", "testgroup"],
            [require_attribute, "has_service_white", "testgroup"],
        ],
    )

    with raises(RootNotFound):
        await mutate(
            _parent=None,
            info=info,
            group_name="testgroup",
            reason="custom reason",
            other="OTHER",
            id="root1",
        )
