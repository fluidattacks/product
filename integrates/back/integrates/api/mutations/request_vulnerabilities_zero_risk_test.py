import random
import string
from datetime import datetime
from typing import Any

from integrates.api.mutations.request_vulnerabilities_zero_risk import mutate
from integrates.custom_exceptions import InvalidJustificationMaxLength
from integrates.dataloaders import Dataloaders, get_new_context
from integrates.db_model.enums import AcceptanceStatus, TreatmentStatus
from integrates.db_model.finding_comments.enums import CommentType
from integrates.db_model.finding_comments.types import FindingCommentsRequest
from integrates.db_model.findings.enums import FindingStatus
from integrates.db_model.types import Treatment
from integrates.db_model.vulnerabilities.enums import (
    VulnerabilityStateStatus,
    VulnerabilityType,
    VulnerabilityVerificationStatus,
    VulnerabilityZeroRiskStatus,
)
from integrates.db_model.vulnerabilities.types import (
    GroupVulnerabilitiesRequest,
    VulnerabilityVerification,
)
from integrates.decorators import (
    enforce_group_level_auth_async,
    require_attribute,
    require_attribute_internal,
    require_login,
)
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    DATE_2024,
    FindingFaker,
    FindingUnreliableIndicatorsFaker,
    GraphQLResolveInfoFaker,
    GroupAccessFaker,
    GroupAccessStateFaker,
    GroupFaker,
    OrganizationAccessFaker,
    OrganizationAccessStateFaker,
    OrganizationFaker,
    StakeholderFaker,
    VulnerabilityFaker,
)
from integrates.testing.mocks import Mock, mocks
from integrates.testing.utils import parametrize, raises
from integrates.tickets import domain as tickets_domain
from integrates.vulnerabilities import domain as vulns_domain

ORG_ID = "ORG#40f6da5f-4f66-4bf0-825b-a2d9748ad6db"

GROUP_NAME = "testgroup"


ADMIN_EMAIL = "admin@gmail.com"
HACKER_EMAIL = "hacker@gmail.com"

ARCHITECT_EMAIL = "architect@gmail.com"
REVIEWER_EMAIL = "reviewer@gmail.com"
RESOURCER_EMAIL = "resourcer@gmail.com"

DEFAULT_DECORATORS: list[list[Any]] = [
    [require_login],
    [require_attribute, "can_request_zero_risk", GROUP_NAME],
    [require_attribute, "can_report_vulnerabilities", GROUP_NAME],
    [enforce_group_level_auth_async],
    [require_attribute_internal, "is_under_review", GROUP_NAME],
]


@parametrize(
    args=["email", "vuln_id"],
    cases=[
        ["admin@gmail.com", "vuln1"],
        ["hacker@gmail.com", "vuln2"],
        ["group_manager@gmail.com", "vuln3"],
        ["customer_manager@fluidattacks.com", "vuln4"],
        ["reattacker@gmail.com", "vuln5"],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id=ORG_ID)],
            groups=[GroupFaker(name="testgroup", organization_id=ORG_ID)],
            stakeholders=[
                StakeholderFaker(email="customer_manager@fluidattacks.com"),
                StakeholderFaker(email="group_manager@gmail.com"),
                StakeholderFaker(email="admin@gmail.com"),
                StakeholderFaker(email="reattacker@gmail.com"),
                StakeholderFaker(email="hacker@gmail.com"),
            ],
            organization_access=[
                OrganizationAccessFaker(
                    organization_id=ORG_ID,
                    email="customer_manager@fluidattacks.com",
                    state=OrganizationAccessStateFaker(has_access=True, role="customer_manager"),
                ),
                OrganizationAccessFaker(
                    organization_id=ORG_ID,
                    email="group_manager@gmail.com",
                    state=OrganizationAccessStateFaker(has_access=True, role="group_manager"),
                ),
                OrganizationAccessFaker(
                    organization_id=ORG_ID,
                    email="admin@gmail.com",
                    state=OrganizationAccessStateFaker(has_access=True, role="admin"),
                ),
                OrganizationAccessFaker(
                    organization_id=ORG_ID,
                    email="reattacker@gmail.com",
                    state=OrganizationAccessStateFaker(has_access=True, role="reattacker"),
                ),
                OrganizationAccessFaker(
                    organization_id=ORG_ID,
                    email="hacker@gmail.com",
                    state=OrganizationAccessStateFaker(has_access=True, role="hacker"),
                ),
            ],
            group_access=[
                GroupAccessFaker(
                    group_name="testgroup",
                    email="customer_manager@fluidattacks.com",
                    state=GroupAccessStateFaker(has_access=True, role="customer_manager"),
                ),
                GroupAccessFaker(
                    group_name="testgroup",
                    email="group_manager@gmail.com",
                    state=GroupAccessStateFaker(has_access=True, role="group_manager"),
                ),
                GroupAccessFaker(
                    group_name="testgroup",
                    email="admin@gmail.com",
                    state=GroupAccessStateFaker(has_access=True, role="admin"),
                ),
                GroupAccessFaker(
                    group_name="testgroup",
                    email="reattacker@gmail.com",
                    state=GroupAccessStateFaker(has_access=True, role="reattacker"),
                ),
                GroupAccessFaker(
                    group_name="testgroup",
                    email="hacker@gmail.com",
                    state=GroupAccessStateFaker(has_access=True, role="hacker"),
                ),
            ],
            findings=[
                FindingFaker(
                    group_name="testgroup",
                    id="3c475384-834c-47b0-ac71-a41a022e401c",
                    unreliable_indicators=FindingUnreliableIndicatorsFaker(
                        unreliable_status=FindingStatus.VULNERABLE
                    ),
                ),
            ],
            vulnerabilities=[
                VulnerabilityFaker(
                    finding_id="3c475384-834c-47b0-ac71-a41a022e401c",
                    group_name="testgroup",
                    id="vuln1",
                    root_id="root1",
                ),
                VulnerabilityFaker(
                    finding_id="3c475384-834c-47b0-ac71-a41a022e401c",
                    group_name="testgroup",
                    id="vuln2",
                    root_id="root1",
                ),
                VulnerabilityFaker(
                    finding_id="3c475384-834c-47b0-ac71-a41a022e401c",
                    group_name="testgroup",
                    id="vuln3",
                    root_id="root1",
                    type=VulnerabilityType.PORTS,
                ),
                VulnerabilityFaker(
                    finding_id="3c475384-834c-47b0-ac71-a41a022e401c",
                    group_name="testgroup",
                    id="vuln4",
                    root_id="root1",
                    type=VulnerabilityType.PORTS,
                ),
                VulnerabilityFaker(
                    finding_id="3c475384-834c-47b0-ac71-a41a022e401c",
                    group_name="testgroup",
                    id="vuln5",
                    root_id="root1",
                    type=VulnerabilityType.PORTS,
                    verification=VulnerabilityVerification(
                        modified_by="admin@gmail.com",
                        modified_date=datetime.fromisoformat("2019-04-10T12:59:52Z"),
                        status=VulnerabilityVerificationStatus.REQUESTED,
                    ),
                ),
            ],
        ),
    ),
    others=[Mock(tickets_domain, "request_vulnerability_zero_risk", "async", None)],
)
async def test_request_vulnerabilities_zero_risk(email: str, vuln_id: str) -> None:
    # Arrange
    finding_id: str = "3c475384-834c-47b0-ac71-a41a022e401c"
    group_name = "testgroup"
    info = GraphQLResolveInfoFaker(
        user_email=email,
        decorators=DEFAULT_DECORATORS,
    )

    # Act
    await mutate(
        _=None,
        info=info,
        finding_id=finding_id,
        justification="Request zero risk vuln",
        vulnerabilities=[vuln_id],
    )

    # Assert
    loaders: Dataloaders = get_new_context()
    loaders.vulnerability.clear(vuln_id)
    vulnerability = await loaders.vulnerability.load(vuln_id)
    assert vulnerability
    assert vulnerability.state.status == VulnerabilityStateStatus.VULNERABLE
    assert vulnerability.zero_risk
    assert vulnerability.zero_risk.status == VulnerabilityZeroRiskStatus.REQUESTED
    zero_risk_comments = await loaders.finding_comments.load(
        FindingCommentsRequest(comment_type=CommentType.ZERO_RISK, finding_id=finding_id)
    )
    assert zero_risk_comments[-1].finding_id == finding_id
    assert zero_risk_comments[-1].content == "Request zero risk vuln"
    assert zero_risk_comments[-1].comment_type == CommentType.ZERO_RISK
    assert zero_risk_comments[-1].email == email

    new_vulnerable_locations = await loaders.group_vulnerabilities.clear_all().load(
        GroupVulnerabilitiesRequest(
            group_name=group_name,
            state_status=VulnerabilityStateStatus.VULNERABLE,
            paginate=False,
        )
    )
    assert vulnerability not in tuple(edge.node for edge in new_vulnerable_locations.edges)


@parametrize(
    args=["email"],
    cases=[[ARCHITECT_EMAIL], [RESOURCER_EMAIL], [REVIEWER_EMAIL]],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id=ORG_ID)],
            groups=[GroupFaker(organization_id=ORG_ID, name=GROUP_NAME)],
            stakeholders=[
                StakeholderFaker(email=ARCHITECT_EMAIL),
                StakeholderFaker(email=RESOURCER_EMAIL),
                StakeholderFaker(email=REVIEWER_EMAIL),
            ],
            group_access=[
                GroupAccessFaker(
                    email=ARCHITECT_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="architect"),
                    group_name=GROUP_NAME,
                ),
                GroupAccessFaker(
                    email=REVIEWER_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="reviewer"),
                    group_name=GROUP_NAME,
                ),
                GroupAccessFaker(
                    email=RESOURCER_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="resourcer"),
                    group_name=GROUP_NAME,
                ),
            ],
            findings=[
                FindingFaker(id="3c475384-834c-47b0-ac71-a41a022e401c", group_name=GROUP_NAME),
            ],
            vulnerabilities=[
                VulnerabilityFaker(
                    finding_id="3c475384-834c-47b0-ac71-a41a022e401c",
                    id="vuln1",
                    group_name=GROUP_NAME,
                ),
            ],
        ),
    )
)
async def test_request_vulnerabilities_zero_risk_unauthorized(email: str) -> None:
    finding_id: str = "3c475384-834c-47b0-ac71-a41a022e401c"
    info = GraphQLResolveInfoFaker(
        user_email=email,
        decorators=DEFAULT_DECORATORS,
    )

    with raises(Exception, match="Access denied"):
        await mutate(
            _=None,
            info=info,
            finding_id=finding_id,
            justification="Request zero risk vuln",
            vulnerabilities=["vuln1"],
        )


@parametrize(
    args=["email"],
    cases=[[ADMIN_EMAIL], [HACKER_EMAIL]],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id=ORG_ID)],
            groups=[GroupFaker(organization_id=ORG_ID, name=GROUP_NAME)],
            stakeholders=[
                StakeholderFaker(email=ADMIN_EMAIL),
                StakeholderFaker(email=HACKER_EMAIL),
            ],
            group_access=[
                GroupAccessFaker(
                    email=ADMIN_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="admin"),
                    group_name=GROUP_NAME,
                ),
                GroupAccessFaker(
                    email=HACKER_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="hacker"),
                    group_name=GROUP_NAME,
                ),
            ],
            findings=[
                FindingFaker(id="3c475384-834c-47b0-ac71-a41a022e401c", group_name=GROUP_NAME),
            ],
            vulnerabilities=[
                VulnerabilityFaker(
                    finding_id="3c475384-834c-47b0-ac71-a41a022e401c",
                    id="vuln1",
                    group_name=GROUP_NAME,
                ),
            ],
        ),
    )
)
async def test_request_vulnerabilities_raises_custom_exc(email: str) -> None:
    finding_id: str = "3c475384-834c-47b0-ac71-a41a022e401c"
    justification = "".join(random.choice(string.ascii_letters) for _ in range(10000 + 1))  # noqa: S311
    info = GraphQLResolveInfoFaker(
        user_email=email,
        decorators=DEFAULT_DECORATORS,
    )

    with raises(InvalidJustificationMaxLength):
        await mutate(
            _=None,
            info=info,
            finding_id=finding_id,
            justification=justification,
            vulnerabilities=["vuln1"],
        )


@parametrize(
    args=["email"],
    cases=[[ADMIN_EMAIL], [HACKER_EMAIL]],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id=ORG_ID)],
            groups=[GroupFaker(name=GROUP_NAME, organization_id=ORG_ID)],
            stakeholders=[
                StakeholderFaker(email="admin@gmail.com"),
                StakeholderFaker(email="hacker@gmail.com"),
            ],
            organization_access=[
                OrganizationAccessFaker(
                    organization_id=ORG_ID,
                    email=ADMIN_EMAIL,
                    state=OrganizationAccessStateFaker(has_access=True, role="admin"),
                ),
                OrganizationAccessFaker(
                    organization_id=ORG_ID,
                    email=HACKER_EMAIL,
                    state=OrganizationAccessStateFaker(has_access=True, role="hacker"),
                ),
            ],
            group_access=[
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email=ADMIN_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="admin"),
                ),
                GroupAccessFaker(
                    group_name=GROUP_NAME,
                    email=HACKER_EMAIL,
                    state=GroupAccessStateFaker(has_access=True, role="hacker"),
                ),
            ],
            findings=[
                FindingFaker(
                    group_name=GROUP_NAME,
                    id="3c475384-834c-47b0-ac71-a41a022e401c",
                    unreliable_indicators=FindingUnreliableIndicatorsFaker(
                        unreliable_status=FindingStatus.VULNERABLE
                    ),
                ),
            ],
            vulnerabilities=[
                VulnerabilityFaker(
                    finding_id="3c475384-834c-47b0-ac71-a41a022e401c",
                    group_name="testgroup",
                    id="vuln1",
                    root_id="root1",
                    type=VulnerabilityType.PORTS,
                    verification=VulnerabilityVerification(
                        modified_by="admin@gmail.com",
                        modified_date=datetime.fromisoformat("2019-04-10T12:59:52Z"),
                        status=VulnerabilityVerificationStatus.REQUESTED,
                    ),
                    treatment=Treatment(
                        modified_date=DATE_2024,
                        status=TreatmentStatus.IN_PROGRESS,
                        acceptance_status=AcceptanceStatus.SUBMITTED,
                    ),
                ),
            ],
        ),
    ),
    others=[
        Mock(vulns_domain, "handle_vulnerabilities_acceptance", "async", None),
        Mock(tickets_domain, "request_vulnerability_zero_risk", "async", None),
    ],
)
async def test_request_vulnerabilities_treatment_changed(email: str) -> None:
    finding_id: str = "3c475384-834c-47b0-ac71-a41a022e401c"
    vuln_id = "vuln1"

    info = GraphQLResolveInfoFaker(
        user_email=email,
        decorators=DEFAULT_DECORATORS,
    )

    await mutate(
        _=None,
        info=info,
        finding_id=finding_id,
        justification="some generic justification",
        vulnerabilities=[vuln_id],
    )

    loaders = get_new_context()

    vulnerability = await loaders.vulnerability.load(vuln_id)
    assert vulnerability
    assert vulnerability.state.status == VulnerabilityStateStatus.VULNERABLE
    assert vulnerability.zero_risk
    assert vulnerability.zero_risk.status == VulnerabilityZeroRiskStatus.REQUESTED
