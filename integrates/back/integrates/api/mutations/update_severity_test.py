from decimal import Decimal

from integrates.api.mutations.update_severity import mutate
from integrates.dataloaders import Dataloaders, get_new_context
from integrates.db_model.findings.types import FindingUnreliableIndicators
from integrates.db_model.types import SeverityScore
from integrates.db_model.vulnerabilities.enums import VulnerabilityStateStatus
from integrates.decorators import (
    enforce_group_level_auth_async,
    require_attribute,
    require_finding_access,
    require_login,
)
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    FindingFaker,
    GitRootFaker,
    GitRootStateFaker,
    GraphQLResolveInfoFaker,
    GroupAccessFaker,
    GroupAccessStateFaker,
    GroupFaker,
    StakeholderFaker,
    VulnerabilityFaker,
    VulnerabilityStateFaker,
)
from integrates.testing.mocks import mocks
from integrates.testing.utils import parametrize, raises

GROUP_NAME = "group1"
FINDING_ID = "3c475384-834c-47b0-ac71-a41a022e401c"

CVSS_V3 = (
    "CVSS:3.1/AV:A/AC:L/PR:L/UI:N/S:U/C:N/I:L/A:N/E:U/"
    "RL:W/RC:U/CR:M/IR:H/AR:L/MAV:A/MAC:L/MPR:L/MUI:N/MS:U/MI:L"
)
CVSS_V4 = "CVSS:4.0/AV:N/AC:L/AT:N/PR:N/UI:A/VC:N/VI:N/VA:N/SC:L/SI:L/SA:N"


@parametrize(
    args=["user_email"],
    cases=[
        ["hacker@gmail.com"],
        ["architect@fluidattacks.com"],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            groups=[GroupFaker(name=GROUP_NAME)],
            stakeholders=[
                StakeholderFaker(email="architect@fluidattacks.com"),
                StakeholderFaker(email="hacker@gmail.com"),
            ],
            group_access=[
                GroupAccessFaker(
                    email="architect@fluidattacks.com",
                    group_name=GROUP_NAME,
                    state=GroupAccessStateFaker(has_access=True, role="architect"),
                ),
                GroupAccessFaker(
                    email="hacker@gmail.com",
                    group_name=GROUP_NAME,
                    state=GroupAccessStateFaker(has_access=True, role="hacker"),
                ),
            ],
            roots=[
                GitRootFaker(
                    id="root_id_1",
                    group_name=GROUP_NAME,
                    state=GitRootStateFaker(
                        branch="trunk", url="https://gitlab.com/fluidattacks/universe.git"
                    ),
                ),
            ],
            findings=[
                FindingFaker(
                    id=FINDING_ID,
                    group_name=GROUP_NAME,
                    severity_score=SeverityScore(
                        base_score=Decimal("4.5"),
                        temporal_score=Decimal("4.1"),
                        cvss_v3=(
                            "CVSS:3.1/AV:P/AC:H/PR:L/UI:N/S:C/C:L/I:L/A:L/"
                            "E:P/RL:O/CR:L/AR:H/MAV:N/MAC:H/MPR:H/MUI:R/MS:U/MC:L/MA:L"
                        ),
                        threat_score=Decimal("1.1"),
                        cvss_v4=(
                            "CVSS:4.0/AV:P/AC:H/AT:N/PR:L/UI:N/VC:L/VI:L/"
                            "VA:L/SC:L/SI:L/SA:L/E:P/AR:H/MAV:N/MAC:H/MPR:H/MUI:P/MVC:L/MVA:L"
                        ),
                        cvssf=Decimal("1.149"),
                        cvssf_v4=Decimal("0.018"),
                    ),
                    unreliable_indicators=FindingUnreliableIndicators(
                        max_open_severity_score=Decimal("5.7"),
                        max_open_severity_score_v4=Decimal("6.9"),
                        total_open_cvssf=Decimal("21.112"),
                        total_open_priority=Decimal("0.50"),
                    ),
                ),
            ],
            vulnerabilities=[
                VulnerabilityFaker(
                    id="be09edb7-cd5c-47ed-bee4-97c645acdce10",
                    finding_id=FINDING_ID,
                    group_name=GROUP_NAME,
                    root_id="root_id_1",
                    state=VulnerabilityStateFaker(status=VulnerabilityStateStatus.VULNERABLE),
                ),
                VulnerabilityFaker(
                    id="be09edb7-cd5c-47ed-bee4-97c645acdce11",
                    finding_id=FINDING_ID,
                    group_name=GROUP_NAME,
                    root_id="root_id_1",
                    state=VulnerabilityStateFaker(status=VulnerabilityStateStatus.VULNERABLE),
                ),
            ],
        ),
    )
)
async def test_update_severity(user_email: str) -> None:
    info = GraphQLResolveInfoFaker(
        user_email=user_email,
        decorators=[
            [require_login],
            [require_finding_access],
            [enforce_group_level_auth_async],
            [require_attribute, "has_asm", GROUP_NAME],
        ],
    )

    result = await mutate(
        _=None,
        info=info,
        finding_id=FINDING_ID,
        cvss_vector=CVSS_V3,
        cvss_4_vector=CVSS_V4,
    )

    assert result.success is True

    loaders: Dataloaders = get_new_context()
    finding = await loaders.finding.load(FINDING_ID)
    assert finding
    assert finding.severity_score
    assert finding.severity_score == SeverityScore(
        base_score=Decimal("3.5"),
        temporal_score=Decimal("2.9"),
        cvss_v3=CVSS_V3,
        cvss_v4=CVSS_V4,
        threat_score=Decimal("5.1"),
        cvssf=Decimal("0.218"),
        cvssf_v4=Decimal("4.595"),
    )
    assert finding.unreliable_indicators.max_open_severity_score == Decimal("4.8")
    assert finding.unreliable_indicators.max_open_severity_score_v4 == Decimal("0.9")
    assert finding.unreliable_indicators.total_open_cvssf == Decimal("6.062")


@parametrize(
    args=["user_email"],
    cases=[
        ["customer_manager@gmail.com"],
        ["resourcer@gmail.com"],
        ["reviewer@gmail.com"],
        ["service_forces@gmail.com"],
        ["user@gmail.com"],
        ["group_manager@gmail.com"],
        ["vulnerability_manager@gmail.com"],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            groups=[GroupFaker()],
            findings=[
                FindingFaker(
                    id=FINDING_ID,
                ),
            ],
            vulnerabilities=[
                VulnerabilityFaker(
                    id="be09edb7-cd5c-47ed-bee4-97c645acdce10",
                    finding_id=FINDING_ID,
                ),
                VulnerabilityFaker(
                    id="be09edb7-cd5c-47ed-bee4-97c645acdce11",
                    finding_id=FINDING_ID,
                ),
            ],
            stakeholders=[
                StakeholderFaker(email="jdoe@fluidattacks.com"),
                StakeholderFaker(email="customer_manager@gmail.com"),
                StakeholderFaker(email="vulnerability_manager@gmail.com"),
                StakeholderFaker(email="user@gmail.com"),
            ],
            group_access=[
                GroupAccessFaker(
                    email="jdoe@fluidattacks.com",
                    state=GroupAccessStateFaker(has_access=True, role="group_manager"),
                ),
                GroupAccessFaker(
                    email="customer_manager@gmail.com",
                    state=GroupAccessStateFaker(has_access=True, role="customer_manager"),
                ),
                GroupAccessFaker(
                    email="vulnerability_manager@gmail.com",
                    state=GroupAccessStateFaker(has_access=True, role="vulnerability_manager"),
                ),
                GroupAccessFaker(
                    email="user@gmail.com",
                    state=GroupAccessStateFaker(has_access=True, role="user"),
                ),
            ],
        ),
    )
)
async def test_update_severity_access_denied(user_email: str) -> None:
    info = GraphQLResolveInfoFaker(
        user_email=user_email,
        decorators=[
            [require_login],
            [require_finding_access],
            [enforce_group_level_auth_async],
            [require_attribute, "has_asm", GROUP_NAME],
        ],
    )
    with raises(Exception, match="Access denied"):
        await mutate(
            _=None,
            info=info,
            finding_id=FINDING_ID,
            cvss_vector=CVSS_V3,
            cvss_4_vector=CVSS_V4,
        )
