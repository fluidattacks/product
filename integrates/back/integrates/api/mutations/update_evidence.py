from typing import (
    Any,
)

from graphql.type.definition import (
    GraphQLResolveInfo,
)
from starlette.datastructures import (
    UploadFile,
)

from integrates.api.types import (
    APP_EXCEPTIONS,
)
from integrates.custom_utils import (
    logs as logs_utils,
)
from integrates.decorators import (
    concurrent_decorators,
    enforce_group_level_auth_async,
    require_asm,
    require_finding_access,
    require_login,
)
from integrates.findings import (
    domain as findings_domain,
)
from integrates.sessions import (
    domain as sessions_domain,
)

from .payloads.types import (
    SimplePayload,
)
from .schema import (
    MUTATION,
)


@MUTATION.field("updateEvidence")
@concurrent_decorators(
    require_login,
    enforce_group_level_auth_async,
    require_asm,
    require_finding_access,
)
async def mutate(_: None, info: GraphQLResolveInfo, **kwargs: Any) -> SimplePayload:
    file_object: UploadFile = kwargs["file"]
    finding_id: str = kwargs["finding_id"]
    evidence_id: str = kwargs["evidence_id"]
    user_data = await sessions_domain.get_jwt_content(info.context)
    user_email = user_data["user_email"]
    try:
        await findings_domain.update_evidence(
            loaders=info.context.loaders,
            finding_id=finding_id,
            evidence_id=evidence_id,
            file_object=file_object,
            author_email=user_email,
            validate_name=True,
        )

        logs_utils.cloudwatch_log(
            info.context,
            "Updated evidence in the finding",
            extra={
                "evidence_id": evidence_id,
                "finding_id": finding_id,
                "log_type": "Security",
            },
        )
    except APP_EXCEPTIONS:
        logs_utils.cloudwatch_log(
            info.context,
            "Attempted to update evidence in the finding",
            extra={
                "evidence_id": evidence_id,
                "finding_id": finding_id,
                "log_type": "Security",
            },
        )
        raise

    return SimplePayload(success=True)
