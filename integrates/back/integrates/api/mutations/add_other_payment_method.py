from graphql.type.definition import (
    GraphQLResolveInfo,
)
from starlette.datastructures import (
    UploadFile,
)

from integrates.billing import (
    domain as billing_domain,
)
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.decorators import (
    concurrent_decorators,
    enforce_organization_level_auth_async,
    require_login,
)
from integrates.organizations import (
    utils as orgs_utils,
)
from integrates.sessions import (
    domain as sessions_domain,
)

from .payloads.types import (
    SimplePayload,
)
from .schema import (
    MUTATION,
)


@MUTATION.field("addOtherPaymentMethod")
@concurrent_decorators(
    require_login,
    enforce_organization_level_auth_async,
)
async def mutate(
    _parent: None,
    info: GraphQLResolveInfo,
    rut: UploadFile | None = None,
    tax_id: UploadFile | None = None,
    **kwargs: str,
) -> SimplePayload:
    loaders: Dataloaders = info.context.loaders
    organization = await orgs_utils.get_organization(loaders, kwargs["organization_id"])
    user_info = await sessions_domain.get_jwt_content(info.context)
    user_email = user_info["user_email"]

    return SimplePayload(
        success=await billing_domain.create_other_payment_method(
            org=organization,
            user_email=user_email,
            business_name=kwargs["business_name"],
            city=kwargs["city"],
            country=kwargs["country"],
            email=kwargs["email"],
            state=kwargs["state"],
            rut=rut,
            tax_id=tax_id,
        ),
    )
