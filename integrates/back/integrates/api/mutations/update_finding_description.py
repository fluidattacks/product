from typing import Any

from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.api.types import (
    APP_EXCEPTIONS,
)
from integrates.custom_utils import (
    logs as logs_utils,
)
from integrates.custom_utils import (
    validations,
)
from integrates.custom_utils.findings import get_finding
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.findings.enums import (
    FindingSorts,
)
from integrates.decorators import (
    concurrent_decorators,
    enforce_group_level_auth_async,
    require_asm,
    require_finding_access,
    require_login,
    require_report_vulnerabilities,
)
from integrates.findings import (
    domain as findings_domain,
)
from integrates.findings.types import (
    FindingDescriptionToUpdate,
)

from .payloads.types import (
    SimpleFindingPayload,
)
from .schema import (
    MUTATION,
)


@MUTATION.field("updateDescription")
@concurrent_decorators(
    require_login,
    enforce_group_level_auth_async,
    require_asm,
    require_report_vulnerabilities,
    require_finding_access,
)
async def mutate(
    _: None,
    info: GraphQLResolveInfo,
    finding_id: str,
    **kwargs: Any,
) -> SimpleFindingPayload:
    loaders: Dataloaders = info.context.loaders
    try:
        old_finding = await get_finding(loaders, finding_id)
        validations.validate_finding_title_change_policy(
            old_title=old_finding.title,
            new_title=kwargs["title"],
            status=old_finding.unreliable_indicators.unreliable_status,
        )

        description = FindingDescriptionToUpdate(
            attack_vector_description=kwargs["attack_vector_description"],
            description=kwargs["description"],
            recommendation=kwargs["recommendation"],
            sorts=FindingSorts[kwargs["sorts"]] if kwargs.get("sorts") else None,
            threat=kwargs["threat"],
            title=kwargs["title"],
            unfulfilled_requirements=kwargs.get("unfulfilled_requirements"),
        )
        await findings_domain.update_description(
            loaders=loaders,
            finding_id=finding_id,
            description=description,
        )
        logs_utils.cloudwatch_log(
            info.context,
            "Updated description in the finding",
            extra={
                "finding_id": finding_id,
                "log_type": "Security",
            },
        )
    except APP_EXCEPTIONS:
        logs_utils.cloudwatch_log(
            info.context,
            "Attempted to update description in the finding",
            extra={
                "finding_id": finding_id,
                "log_type": "Security",
            },
        )
        raise

    loaders.finding.clear(finding_id)
    finding = await get_finding(loaders, finding_id)

    return SimpleFindingPayload(finding=finding, success=True)
