from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.custom_utils import (
    logs as logs_utils,
)
from integrates.decorators import (
    concurrent_decorators,
    enforce_group_level_auth_async,
    require_asm,
    require_login,
)
from integrates.forces.validations import (
    validate_non_forces_user,
)
from integrates.groups import (
    domain as groups_domain,
)
from integrates.sessions import (
    domain as sessions_domain,
)

from .payloads.types import (
    RemoveStakeholderAccessPayload,
)
from .schema import (
    MUTATION,
)


@MUTATION.field("removeStakeholderAccess")
@concurrent_decorators(
    require_login,
    enforce_group_level_auth_async,
    require_asm,
)
async def mutate(
    _: None,
    info: GraphQLResolveInfo,
    user_email: str,
    group_name: str,
) -> RemoveStakeholderAccessPayload:
    user_data = await sessions_domain.get_jwt_content(info.context)
    requester_email = user_data["user_email"]
    validate_non_forces_user(user_email)
    await groups_domain.remove_stakeholder(
        loaders=info.context.loaders,
        email_to_revoke=user_email,
        group_name=group_name,
        modified_by=requester_email,
    )
    logs_utils.cloudwatch_log(
        info.context,
        "Removed stakeholder in the group",
        extra={
            "group_name": group_name,
            "removed stakeholder": user_email,
            "requester_email": requester_email,
            "log_type": "Security",
        },
    )

    return RemoveStakeholderAccessPayload(success=True, removed_email=user_email)
