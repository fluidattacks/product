from asyncio import (
    sleep,
)
from typing import (
    Any,
)

from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.api.types import (
    APP_EXCEPTIONS,
)
from integrates.custom_utils import (
    logs as logs_utils,
)
from integrates.db_model.vulnerabilities.enums import (
    VulnerabilityStateReason,
)
from integrates.decorators import (
    concurrent_decorators,
    enforce_group_level_auth_async,
    require_login,
    require_report_vulnerabilities,
)
from integrates.unreliable_indicators.enums import (
    EntityDependency,
)
from integrates.unreliable_indicators.operations import (
    update_unreliable_indicators_by_deps,
)
from integrates.vulnerability_files.close import (
    close_vulnerabilities,
)

from .payloads.types import (
    SimplePayload,
)
from .schema import (
    MUTATION,
)


@MUTATION.field("closeVulnerabilities")
@concurrent_decorators(
    require_login,
    require_report_vulnerabilities,
    enforce_group_level_auth_async,
)
async def mutate(
    _: None,
    info: GraphQLResolveInfo,
    finding_id: str,
    vulnerabilities: list[str],
    justification: VulnerabilityStateReason,
    **_kwargs: Any,
) -> SimplePayload:
    try:
        processed_vulnerabilities = await close_vulnerabilities(
            loaders=info.context.loaders,
            vulnerabilities_ids=set(vulnerabilities),
            finding_id=finding_id,
            justification=justification,
            info=info,
        )
        await update_unreliable_indicators_by_deps(
            EntityDependency.upload_file,
            finding_ids=[finding_id],
            vulnerability_ids=list(processed_vulnerabilities),
        )
        await sleep(0.5)  # wait for streams to update

        logs_utils.cloudwatch_log(
            info.context,
            "Closed vulnerabilities in the finding",
            extra={"finding_id": finding_id, "log_type": "Security"},
        )
    except APP_EXCEPTIONS:
        logs_utils.cloudwatch_log(
            info.context,
            "Attempted to close vulnerabilities in the finding",
            extra={"finding_id": finding_id, "log_type": "Security"},
        )
        raise

    return SimplePayload(success=True)
