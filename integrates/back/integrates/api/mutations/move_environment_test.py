from graphql.error.graphql_error import GraphQLError

from integrates.api.mutations.move_environment import mutate
from integrates.db_model.vulnerabilities.enums import VulnerabilityStateStatus, VulnerabilityType
from integrates.decorators import (
    enforce_group_level_auth_async,
    require_attribute_internal,
    require_login,
)
from integrates.roots.domain import format_environment_id
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    FindingFaker,
    GitRootFaker,
    GraphQLResolveInfoFaker,
    GroupAccessFaker,
    GroupAccessStateFaker,
    GroupFaker,
    OrganizationAccessFaker,
    OrganizationAccessStateFaker,
    OrganizationFaker,
    RootEnvironmentUrlFaker,
    RootEnvironmentUrlStateFaker,
    StakeholderFaker,
    ToeInputFaker,
    ToeInputStateFaker,
    VulnerabilityFaker,
    VulnerabilityStateFaker,
)
from integrates.testing.mocks import mocks
from integrates.testing.utils import parametrize, raises


@parametrize(
    args=["user"],
    cases=[
        ["admin@fluidattacks.com"],
        ["customer_manager@fluidattacks.com"],
        ["group_manager@gmail.com"],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(id="org1"),
            ],
            groups=[
                GroupFaker(name="west", organization_id="org1"),
                GroupFaker(name="east", organization_id="org1"),
            ],
            findings=[
                FindingFaker(group_name="west", id="find1"),
            ],
            roots=[
                GitRootFaker(group_name="west", id="root1"),
                GitRootFaker(group_name="east", id="root2"),
            ],
            vulnerabilities=[
                VulnerabilityFaker(
                    group_name="west",
                    root_id="root1",
                    finding_id="find1",
                    id="vuln1",
                    created_by="hacker@fluidattacks.com",
                    type=VulnerabilityType.INPUTS,
                    state=VulnerabilityStateFaker(
                        status=VulnerabilityStateStatus.VULNERABLE, where="https://test.com/"
                    ),
                ),
            ],
            stakeholders=[
                StakeholderFaker(email="admin@fluidattacks.com", role="admin"),
                StakeholderFaker(
                    email="customer_manager@fluidattacks.com", role="customer_manager"
                ),
                StakeholderFaker(email="group_manager@gmail.com", role="group_manager"),
            ],
            organization_access=[
                OrganizationAccessFaker(
                    email="admin@fluidattacks.com",
                    organization_id="org1",
                    state=OrganizationAccessStateFaker(role="admin", has_access=True),
                ),
                OrganizationAccessFaker(
                    email="customer_manager@fluidattacks.com",
                    organization_id="org1",
                    state=OrganizationAccessStateFaker(has_access=True),
                ),
                OrganizationAccessFaker(
                    email="group_manager@gmail.com",
                    organization_id="org1",
                    state=OrganizationAccessStateFaker(role="group_manager", has_access=True),
                ),
            ],
            group_access=[
                GroupAccessFaker(
                    email="customer_manager@fluidattacks.com",
                    group_name="west",
                    state=GroupAccessStateFaker(has_access=True, role="customer_manager"),
                ),
                GroupAccessFaker(
                    email="customer_manager@fluidattacks.com",
                    group_name="east",
                    state=GroupAccessStateFaker(has_access=True, role="customer_manager"),
                ),
                GroupAccessFaker(
                    email="group_manager@gmail.com",
                    group_name="west",
                    state=GroupAccessStateFaker(has_access=True, role="group_manager"),
                ),
                GroupAccessFaker(
                    email="group_manager@gmail.com",
                    group_name="east",
                    state=GroupAccessStateFaker(has_access=True, role="group_manager"),
                ),
            ],
            toe_inputs=[
                ToeInputFaker(
                    group_name="west",
                    root_id="root1",
                    component="https://test.com/",
                    entry_point="user",
                    state=ToeInputStateFaker(has_vulnerabilities=True),
                ),
            ],
            root_environment_urls=[
                RootEnvironmentUrlFaker(
                    url="https://test.com/",
                    group_name="west",
                    root_id="root1",
                    state=RootEnvironmentUrlStateFaker(modified_by="hacker@fluidattacks.com"),
                ),
            ],
        ),
    )
)
async def test_permissions(user: str) -> None:
    # Arrange
    group_name = "west"
    root_id = "root1"
    target_group_name = "east"
    target_root_id = "root2"
    url_id = format_environment_id("https://test.com/")
    info = GraphQLResolveInfoFaker(
        user_email=user,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_attribute_internal, "is_under_review", group_name],
            [require_attribute_internal, "is_under_review", target_group_name],
        ],
    )

    # Act
    result = await mutate(
        _parent=None,
        info=info,
        url_id=url_id,
        root_id=root_id,
        group_name=group_name,
        target_root_id=target_root_id,
        target_group_name=target_group_name,
    )

    # Assert
    assert result.success


@parametrize(
    args=["user"],
    cases=[["user1@gmail.com"], ["vulnerability_manager@gmail.com"]],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(id="org1"),
            ],
            groups=[
                GroupFaker(name="west", organization_id="org1"),
                GroupFaker(name="east", organization_id="org1"),
            ],
            findings=[
                FindingFaker(group_name="west", id="find1"),
            ],
            roots=[
                GitRootFaker(group_name="west", id="root1"),
                GitRootFaker(group_name="east", id="root2"),
            ],
            vulnerabilities=[
                VulnerabilityFaker(
                    group_name="west",
                    root_id="root1",
                    finding_id="find1",
                    id="vuln1",
                    created_by="hacker@fluidattacks.com",
                    type=VulnerabilityType.INPUTS,
                    state=VulnerabilityStateFaker(
                        status=VulnerabilityStateStatus.VULNERABLE, where="https://test.com/"
                    ),
                ),
            ],
            stakeholders=[
                StakeholderFaker(email="user@fluidattacks.com", role="user"),
                StakeholderFaker(
                    email="vulnerability_manager@gmail.com", role="vulnerability_manager"
                ),
            ],
            organization_access=[
                OrganizationAccessFaker(
                    email="user1@gmail.com",
                    organization_id="org1",
                    state=OrganizationAccessStateFaker(role="user", has_access=True),
                ),
                OrganizationAccessFaker(
                    email="vulnerability_manager@gmail.com",
                    organization_id="org1",
                    state=OrganizationAccessStateFaker(has_access=True),
                ),
            ],
            group_access=[
                GroupAccessFaker(
                    email="user1@gmail.com",
                    group_name="west",
                    state=GroupAccessStateFaker(has_access=True, role="user"),
                ),
                GroupAccessFaker(
                    email="user1@gmail.com",
                    group_name="east",
                    state=GroupAccessStateFaker(has_access=True, role="user"),
                ),
                GroupAccessFaker(
                    email="vulnerability_manager@gmail.com",
                    group_name="west",
                    state=GroupAccessStateFaker(has_access=True, role="vulnerability_manager"),
                ),
                GroupAccessFaker(
                    email="vulnerability_manager@gmail.com",
                    group_name="east",
                    state=GroupAccessStateFaker(has_access=True, role="vulnerability_manager"),
                ),
            ],
            toe_inputs=[
                ToeInputFaker(
                    group_name="west",
                    root_id="root1",
                    component="https://test.com/",
                    entry_point="user",
                    state=ToeInputStateFaker(has_vulnerabilities=True),
                ),
            ],
            root_environment_urls=[
                RootEnvironmentUrlFaker(
                    url="https://test.com/",
                    group_name="west",
                    root_id="root1",
                    state=RootEnvironmentUrlStateFaker(modified_by="hacker@fluidattacks.com"),
                ),
            ],
        ),
    ),
)
async def test_no_permissions(user: str) -> None:
    # Arrange
    group_name = "west"
    root_id = "root1"
    target_group_name = "east"
    target_root_id = "root2"
    url_id = format_environment_id("https://test.com/")
    info = GraphQLResolveInfoFaker(
        user_email=user,
        decorators=[
            [require_login],
            [enforce_group_level_auth_async],
            [require_attribute_internal, "is_under_review", group_name],
            [require_attribute_internal, "is_under_review", target_group_name],
        ],
    )

    # Act
    with raises(GraphQLError):
        await mutate(
            _parent=None,
            info=info,
            url_id=url_id,
            root_id=root_id,
            group_name=group_name,
            target_root_id=target_root_id,
            target_group_name=target_group_name,
        )
