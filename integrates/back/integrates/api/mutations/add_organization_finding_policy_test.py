from integrates.api.mutations.add_organization_finding_policy import mutate
from integrates.custom_exceptions import (
    InvalidFindingNameTreatmentPolicy,
    RepeatedFindingNamePolicy,
)
from integrates.dataloaders import Dataloaders, get_new_context
from integrates.db_model.enums import TreatmentStatus
from integrates.decorators import enforce_organization_level_auth_async, require_login
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    GraphQLResolveInfoFaker,
    OrganizationAccessFaker,
    OrganizationFaker,
    OrgFindingPolicyFaker,
    StakeholderFaker,
)
from integrates.testing.mocks import mocks
from integrates.testing.utils import parametrize, raises

EMAIL_TEST = "jdoe@fluidattacks.com"
FIN_NAME = "318. Insecurely generated token - Validation"
ORG_ID = "43245t-1bf5-902l-2df9-9ot4l2s4"
ORG_NAME = "orgtest"


@parametrize(
    args=["finding_name", "treatment_acceptance"],
    cases=[
        [
            "031. Excessive privileges - AWS",
            "ACCEPTED_UNDEFINED",
        ],
        [
            "009. Sensitive information in source code",
            "ACCEPTED_UNDEFINED",
        ],
        [
            "265. Insecure encryption algorithm - AES",
            "ACCEPTED",
        ],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organization_access=[OrganizationAccessFaker(organization_id=ORG_ID, email=EMAIL_TEST)],
            organizations=[OrganizationFaker(id=ORG_ID, name=ORG_NAME)],
            stakeholders=[StakeholderFaker(email=EMAIL_TEST, role="customer_manager")],
        ),
    )
)
async def test_add_organization_finding_policy(
    finding_name: str, treatment_acceptance: str
) -> None:
    # Arrange
    info = GraphQLResolveInfoFaker(
        user_email=EMAIL_TEST,
        decorators=[[require_login], [enforce_organization_level_auth_async]],
    )

    # Act
    loaders: Dataloaders = get_new_context()
    await mutate(
        _parent=None,
        info=info,
        finding_name=finding_name,
        organization_name=ORG_NAME,
        tags=set(["tag1", "tag2"]),
        treatment_acceptance=treatment_acceptance,
    )
    org_finding_policies = await loaders.organization_finding_policies.load(ORG_NAME)

    # Assert
    assert org_finding_policies
    assert org_finding_policies[0].name == finding_name


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organization_access=[OrganizationAccessFaker(organization_id=ORG_ID, email=EMAIL_TEST)],
            organizations=[OrganizationFaker(id=ORG_ID, name=ORG_NAME)],
            stakeholders=[StakeholderFaker(email=EMAIL_TEST, role="customer_manager")],
        ),
    )
)
async def test_add_organization_finding_policy_fail() -> None:
    # Arrange
    info = GraphQLResolveInfoFaker(
        user_email=EMAIL_TEST,
        decorators=[[require_login], [enforce_organization_level_auth_async]],
    )

    # Assert
    with raises(InvalidFindingNameTreatmentPolicy):
        await mutate(
            _parent=None,
            info=info,
            finding_name="All vulnerability types",
            organization_name=ORG_NAME,
            tags=set(["tag1", "tag2"]),
            treatment_acceptance="ACCEPTED_UNDEFINED",
        )


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organization_finding_policies=[
                OrgFindingPolicyFaker(
                    id="finding-policy-id",
                    name=FIN_NAME,
                    organization_name=ORG_NAME,
                    treatment_acceptance=TreatmentStatus.ACCEPTED_UNDEFINED,
                )
            ],
            organization_access=[OrganizationAccessFaker(organization_id=ORG_ID, email=EMAIL_TEST)],
            organizations=[OrganizationFaker(id=ORG_ID, name=ORG_NAME)],
            stakeholders=[StakeholderFaker(email=EMAIL_TEST, role="customer_manager")],
        ),
    ),
)
async def test_add_organization_finding_policy_fail_repeated() -> None:
    # Arrange
    info = GraphQLResolveInfoFaker(
        user_email=EMAIL_TEST,
        decorators=[[require_login], [enforce_organization_level_auth_async]],
    )

    # Assert
    with raises(RepeatedFindingNamePolicy):
        await mutate(
            _parent=None,
            info=info,
            finding_name=FIN_NAME,
            organization_name=ORG_NAME,
            treatment_acceptance="ACCEPTED_UNDEFINED",
        )
