from graphql.type.definition import (
    GraphQLResolveInfo,
)

from integrates.api.mutations.payloads.types import (
    SimplePayload,
)
from integrates.api.mutations.schema import (
    MUTATION,
)
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.decorators import (
    concurrent_decorators,
    enforce_group_level_auth_async,
    require_is_not_under_review,
)
from integrates.groups import (
    azure_issues_integration,
)


@MUTATION.field("updateAzureIssuesIntegration")
@concurrent_decorators(enforce_group_level_auth_async, require_is_not_under_review)
async def mutate(
    _parent: None,
    info: GraphQLResolveInfo,
    *,
    azure_organization_name: str,
    azure_project_name: str,
    group_name: str,
    assigned_to: str | None = None,
    issue_automation_enabled: bool = True,
    tags: list[str] | None = None,
) -> SimplePayload:
    loaders: Dataloaders = info.context.loaders
    await azure_issues_integration.update_settings(
        assigned_to=assigned_to or "",
        azure_organization_name=azure_organization_name,
        azure_project_name=azure_project_name,
        group_name=group_name,
        issue_automation_enabled=issue_automation_enabled,
        loaders=loaders,
        tags=tags or [],
    )
    return SimplePayload(success=True)
