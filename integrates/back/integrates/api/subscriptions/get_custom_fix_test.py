from integrates.api.subscriptions.get_custom_fix import resolve
from integrates.decorators import (
    enforce_group_level_auth_async,
    require_attribute_internal,
)
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    FindingFaker,
    GraphQLResolveInfoFaker,
    GroupFaker,
    OrganizationFaker,
    VulnerabilityFaker,
)
from integrates.testing.mocks import mocks
from integrates.testing.utils import raises


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            groups=[GroupFaker(organization_id="org1")],
            organizations=[OrganizationFaker(id="org1")],
            findings=[FindingFaker(id="f33ce4d4-79a4-439e-8552-e04843affdae")],
            vulnerabilities=[
                VulnerabilityFaker(
                    id="f938ac7b-7ad9-45c9-a3ee-43fc17d88e54",
                    finding_id="f33ce4d4-79a4-439e-8552-e04843affdae",
                )
            ],
        )
    )
)
async def test_get_custom_fix_access_denied() -> None:
    info = GraphQLResolveInfoFaker(
        user_email="randommail@test.test",
        decorators=[
            [enforce_group_level_auth_async],
            [require_attribute_internal, "is_under_review", "test-group"],
        ],
    )
    with raises(Exception, match="Access denied"):
        await resolve(
            count="", _info=info, info=info, vulnerability_id="f938ac7b-7ad9-45c9-a3ee-43fc17d88e54"
        )
