from collections.abc import (
    Awaitable,
    Callable,
)
from contextlib import (
    AsyncExitStack,
)
from typing import (
    Any,
)

import aioboto3
from aiobotocore.config import (
    AioConfig,
)
from types_aiobotocore_dynamodb import DynamoDBServiceResource
from types_aiobotocore_dynamodb.service_resource import Table as DynamoTable

from integrates.context import (
    FI_AWS_DYNAMODB_HOST,
    FI_AWS_REGION_NAME,
)
from integrates.dynamodb.types import (
    Table,
)

SESSION = aioboto3.Session()
TABLE_RESOURCES: dict[str, DynamoTable] = {}
StartupCallable = Callable[[], Awaitable[None]]
ShutdownCallable = Callable[[], Awaitable[None]]
GetResourceCallable = Callable[[], Awaitable[DynamoDBServiceResource]]
DynamoContext = tuple[StartupCallable, ShutdownCallable, GetResourceCallable]


def create_dynamo_context() -> DynamoContext:
    context_stack = None
    resource = None

    async def _startup() -> None:
        nonlocal context_stack, resource

        context_stack = AsyncExitStack()
        resource = await context_stack.enter_async_context(
            SESSION.resource(
                config=AioConfig(
                    # The time in seconds until a timeout exception is raised
                    # when attempting to make a connection. [60]
                    connect_timeout=10,
                    # Maximum amount of simultaneously opened connections. [10]
                    # https://docs.aiohttp.org/en/stable/client_advanced.html#limiting-connection-pool-size
                    max_pool_connections=0,
                    # The time in seconds until a timeout exception is raised
                    # when attempting to read from a connection. [60]
                    read_timeout=5,
                    # https://boto3.amazonaws.com/v1/documentation/api/latest/guide/retries.html
                    retries={"max_attempts": 10, "mode": "standard"},
                ),
                endpoint_url=FI_AWS_DYNAMODB_HOST,
                region_name=FI_AWS_REGION_NAME,
                service_name="dynamodb",
                use_ssl=True,
                verify=True,
            ),
        )
        TABLE_RESOURCES["integrates_vms"] = await resource.Table("integrates_vms")
        TABLE_RESOURCES["integrates_vms_historic"] = await resource.Table("integrates_vms_historic")
        TABLE_RESOURCES["fi_async_processing"] = await resource.Table("fi_async_processing")

    async def _shutdown() -> None:
        if context_stack:
            await context_stack.aclose()

    async def _get_resource() -> Any:
        if resource is None:
            await dynamo_startup()

        return resource

    return _startup, _shutdown, _get_resource


dynamo_startup, dynamo_shutdown, get_resource = create_dynamo_context()


async def get_table_resource(table: Table) -> DynamoTable:
    if table.name in TABLE_RESOURCES:
        return TABLE_RESOURCES[table.name]

    resource = await get_resource()
    return await resource.Table(table.name)
