from typing import (
    Any,
)

from starlette.requests import (
    Request,
)

from integrates.custom_exceptions import (
    InvalidSource,
)
from integrates.db_model.enums import (
    Source,
)


def get_source(context: Any) -> str:
    headers = context.headers
    source: str = headers.get("x-integrates-source", "asm")
    # Compatibility with old API
    mapped_source: str = map_source(source)
    if mapped_source not in {"asm", "machine"}:
        raise InvalidSource()
    return mapped_source


def get_source_new(context: Any) -> Source:
    source = get_source(context)
    return Source[source.upper()]


def map_source(source: str) -> str:
    """Maps old, deprecated sources to their new denomination"""
    if source == "integrates":
        return "asm"
    if source == "skims":
        return "machine"
    return source


def get_redirect_url(request: Request, pattern: str) -> str:
    return str(request.url_for(pattern)).replace("http:", "https:")
