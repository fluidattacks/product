from datetime import datetime

from integrates.custom_exceptions import GroupNotFound
from integrates.custom_utils.groups import filter_active_groups, get_group
from integrates.dataloaders import get_new_context
from integrates.db_model.groups.enums import (
    GroupLanguage,
    GroupManaged,
    GroupService,
    GroupStateStatus,
    GroupSubscriptionType,
    GroupTier,
)
from integrates.db_model.groups.types import GroupState
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import GroupFaker, GroupStateFaker
from integrates.testing.mocks import mocks
from integrates.testing.utils import parametrize, raises


@parametrize(
    args=["group_name"],
    cases=[
        ["group1"],
        ["group2"],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            groups=[
                GroupFaker(
                    name="group1",
                    state=GroupStateFaker(status=GroupStateStatus.ACTIVE),
                ),
                GroupFaker(
                    name="group2",
                    state=GroupStateFaker(status=GroupStateStatus.DELETED),
                ),
            ]
        )
    )
)
async def test_get_group(group_name: str) -> None:
    group = await get_group(get_new_context(), group_name)
    assert group.name == group_name


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            groups=[
                GroupFaker(
                    name="group1",
                    state=GroupStateFaker(status=GroupStateStatus.ACTIVE),
                ),
                GroupFaker(
                    name="group2",
                    state=GroupStateFaker(status=GroupStateStatus.DELETED),
                ),
            ]
        )
    )
)
async def test_get_group_failed() -> None:
    with raises(GroupNotFound):
        await get_group(get_new_context(), "group3")


@parametrize(
    args=["group_name", "expected"],
    cases=[
        # Active
        ["group1", True],
        # Inactive
        ["group2", False],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            groups=[
                GroupFaker(
                    created_by="johndoe@fluidattacks.com",
                    created_date=datetime.fromisoformat("2022-10-21T15:58:31.280182+00:00"),
                    description="test description",
                    language=GroupLanguage.EN,
                    name="group1",
                    organization_id="",
                    state=GroupState(
                        has_essential=True,
                        has_advanced=False,
                        managed=GroupManaged.MANAGED,
                        modified_by="johndoe@fluidattacks.com",
                        modified_date=datetime.fromisoformat("2022-10-21T15:58:31.280182+00:00"),
                        service=GroupService.WHITE,
                        status=GroupStateStatus.ACTIVE,
                        tier=GroupTier.FREE,
                        type=GroupSubscriptionType.CONTINUOUS,
                    ),
                ),
                GroupFaker(
                    created_by="johndoe@fluidattacks.com",
                    created_date=datetime.fromisoformat("2022-10-21T15:58:31.280182+00:00"),
                    description="test description",
                    language=GroupLanguage.EN,
                    name="group2",
                    organization_id="",
                    state=GroupState(
                        has_essential=True,
                        has_advanced=False,
                        managed=GroupManaged.MANAGED,
                        modified_by="johndoe@fluidattacks.com",
                        modified_date=datetime.fromisoformat("2022-10-21T15:58:31.280182+00:00"),
                        service=GroupService.WHITE,
                        status=GroupStateStatus.DELETED,
                        tier=GroupTier.FREE,
                        type=GroupSubscriptionType.CONTINUOUS,
                    ),
                ),
            ]
        )
    )
)
async def test_filter_active_groups(group_name: str, expected: bool) -> None:
    loaders = get_new_context()
    group = await loaders.group.load(group_name)
    assert group
    filtered = filter_active_groups([group])
    assert bool(filtered) == expected
