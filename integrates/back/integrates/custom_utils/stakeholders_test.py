from integrates.class_types.types import (
    Item,
)
from integrates.custom_exceptions import StakeholderNotFound
from integrates.custom_utils.stakeholders import format_invitation_state, get_stakeholder
from integrates.dataloaders import (
    get_new_context,
)
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    StakeholderFaker,
)
from integrates.testing.mocks import mocks
from integrates.testing.utils import parametrize, raises

STAFF_EMAIL = "johndoe@fluidattacks.com"


@parametrize(
    args=["invitation", "is_registered", "expect_result"],
    cases=[
        [
            {"is_used": False},
            False,
            "PENDING",
        ],
        [None, False, "UNREGISTERED"],
        [None, True, "REGISTERED"],
    ],
)
def test_format_invitation_state(invitation: Item, is_registered: bool, expect_result: str) -> None:
    result = format_invitation_state(invitation, is_registered)

    assert result == expect_result


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            stakeholders=[
                StakeholderFaker(email=STAFF_EMAIL, role="customer_manager"),
            ],
        ),
    )
)
async def test_get_registered_domain() -> None:
    loaders = get_new_context()
    result = await get_stakeholder(loaders, STAFF_EMAIL)
    assert result
    assert result.email == STAFF_EMAIL


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            stakeholders=[
                StakeholderFaker(email=STAFF_EMAIL, role="customer_manager"),
                StakeholderFaker(email="johndoe@nonfluidatatcks.com", role="organization_manager"),
            ],
        ),
    )
)
async def test_get_registered_domain_stakeholder_not_found() -> None:
    loaders = get_new_context()
    with raises(StakeholderNotFound):
        await get_stakeholder(loaders, STAFF_EMAIL, "johndoe@nonfluidatatcks.com")
