# Starlette templates renders

import json
import traceback

from starlette.requests import Request
from starlette.responses import HTMLResponse
from starlette.templating import Jinja2Templates

from integrates.settings import DEBUG, STATIC_URL, TEMPLATES_DIR
from integrates.verify.enums import Channel
from integrates.verify.operations import translate_channel

TEMPLATING_ENGINE = Jinja2Templates(directory=TEMPLATES_DIR)


def error401(request: Request) -> HTMLResponse:
    return TEMPLATING_ENGINE.TemplateResponse(name="HTTP401.html", request=request)


def error500(request: Request) -> HTMLResponse:
    return TEMPLATING_ENGINE.TemplateResponse(name="HTTP500.html", request=request)


def graphic_error(request: Request) -> HTMLResponse:
    return TEMPLATING_ENGINE.TemplateResponse(
        name="graphic-error.html",
        context={"debug": DEBUG, "traceback": traceback.format_exc()},
        request=request,
    )


def graphics_for_entity_view(request: Request, entity: str) -> HTMLResponse:
    entity_title = entity.title()
    return TEMPLATING_ENGINE.TemplateResponse(
        name="graphics-for-entity.html",
        context={
            "debug": DEBUG,
            "entity": entity_title,
            "js_runtime": f"{STATIC_URL}/dashboard/index-vite-bundle.min.js",
            "js_vendors": f"{STATIC_URL}/dashboard/vendors-vite-bundle.min.js",
            "css_vendors": (f"{STATIC_URL}/dashboard/vendors-vite-style.min.css"),
            "entity_graphic_css": f"{STATIC_URL}/styles/generic_graphic.css",
            "js": (f"{STATIC_URL}/dashboard/graphicsFor{entity_title}-vite-bundle.min.js"),
            "css": (f"{STATIC_URL}/dashboard/graphicsFor{entity_title}-vite-style.min.css"),
        },
        request=request,
    )


def graphic_view(
    *,
    request: Request,
    document: object,
    height: int,
    width: int,
    generator_type: str,
    generator_name: str,
) -> HTMLResponse:
    return TEMPLATING_ENGINE.TemplateResponse(
        name="graphic.html",
        context={
            "debug": DEBUG,
            "args": {
                "data": json.dumps(document),
                "height": height,
                "width": width,
            },
            "generator_src": (f"graphics/generators/{generator_type}/{generator_name}.js"),
            "generator_js": (
                f"{STATIC_URL}/"
                "graphics/"
                "generators/"
                f"{generator_type}/"
                f"{generator_name}.js"
            ),
            "graphic_css": f"{STATIC_URL}/styles/graphic.css",
        },
        request=request,
    )


def invalid_invitation(request: Request, error: str = "", entity_name: str = "") -> HTMLResponse:
    return TEMPLATING_ENGINE.TemplateResponse(
        name="invalid_invitation.html",
        context={"error": error, "entity_name": entity_name},
        request=request,
    )


def login(request: Request) -> HTMLResponse:
    return TEMPLATING_ENGINE.TemplateResponse(
        name="login.html",
        context={
            "debug": DEBUG,
            "css_vendors": f"{STATIC_URL}/dashboard/vendors-vite-style.min.css",
            "js": f"{STATIC_URL}/dashboard/app-vite-bundle.min.js",
        },
        request=request,
    )


def main_app(request: Request) -> HTMLResponse:
    return TEMPLATING_ENGINE.TemplateResponse(
        name="app.html",
        context={
            "debug": DEBUG,
            "css_vendors": f"{STATIC_URL}/dashboard/vendors-vite-style.min.css",
            "js": f"{STATIC_URL}/dashboard/app-vite-bundle.min.js",
        },
        request=request,
    )


def valid_invitation(request: Request, entity_name: str) -> HTMLResponse:
    return TEMPLATING_ENGINE.TemplateResponse(
        name="valid_invitation.html",
        context={"entity_name": entity_name},
        request=request,
    )


def confirm_deletion(*, request: Request) -> HTMLResponse:
    request.session.clear()
    return TEMPLATING_ENGINE.TemplateResponse(
        name="valid_delete_confirmation.html",
        request=request,
    )


def invalid_confirm_deletion(
    *,
    request: Request,
    error: str,
) -> HTMLResponse:
    return TEMPLATING_ENGINE.TemplateResponse(
        name="invalid_delete_confirmation.html",
        context={"error": error},
        request=request,
    )


def reject_invitation(request: Request, entity_name: str) -> HTMLResponse:
    return TEMPLATING_ENGINE.TemplateResponse(
        name="reject_invitation.html",
        context={"entity_name": entity_name},
        request=request,
    )


def verify_otp(
    *,
    request: Request,
    entity_name: str,
    user_email: str,
    show_alert: bool = False,
    phone_number: str | None = None,
    channel: str | None = None,
) -> HTMLResponse:
    translated_channel = None
    if channel and channel in [channel_enum.value for channel_enum in Channel]:
        input_channel = Channel(channel)
        translated_channel = translate_channel(input_channel)

    return TEMPLATING_ENGINE.TemplateResponse(
        name="verify_otp.html",
        context={
            "entity_name": entity_name,
            "show_alert": show_alert,
            "user_email": user_email,
            "phone_number": phone_number,
            "channel": translated_channel,
        },
        request=request,
    )


def unsubscribe(
    *,
    request: Request,
    confirmed: bool = False,
) -> HTMLResponse:
    return TEMPLATING_ENGINE.TemplateResponse(
        name="unsubscribe.html",
        context={
            "confirmed": confirmed,
        },
        request=request,
    )
