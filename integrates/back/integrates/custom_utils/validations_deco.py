import functools
from collections.abc import (
    Callable,
    Iterable,
)
from decimal import (
    Decimal,
)
from typing import (
    ParamSpec,
    TypeVar,
)

from integrates.custom_exceptions import (
    InvalidParameter,
    InvalidRootType,
    InvalidSeverity,
    NumberOutOfRange,
)
from integrates.custom_utils import (
    validations,
)
from integrates.db_model.findings.types import (
    Finding,
)
from integrates.db_model.groups.types import (
    GroupFile,
)
from integrates.db_model.roots.types import (
    GitRoot,
    IPRoot,
    Root,
    URLRoot,
)

T = TypeVar("T")
P = ParamSpec("P")


def validate_active_root_deco(root_field: str) -> Callable:
    def wrapper(func: Callable) -> Callable:
        @functools.wraps(func)
        def decorated(*args: str, **kwargs: GitRoot | IPRoot | URLRoot) -> Callable:
            root: Root = kwargs[root_field]
            validations.check_active_root(root=root)
            return func(*args, **kwargs)

        return decorated

    return wrapper


def validate_email_address_deco(field: str) -> Callable:
    def wrapper(func: Callable) -> Callable:
        @functools.wraps(func)
        def decorated(*args: str, **kwargs: str) -> Callable:
            field_content = validations.get_attr_value(field=field, kwargs=kwargs, obj_type=str)
            validations.check_email(field_content)
            return func(*args, **kwargs)

        return decorated

    return wrapper


def validate_fields_deco(
    fields: Iterable[str],
) -> Callable[[Callable[P, T]], Callable[P, T]]:
    """
    Validates if all the fields contain valid characters

    Args:
        fields (Iterable[str]): List of fields to validate

    Raises:
        InvalidChar: if any of the fields contains invalid characters

    """

    def wrapper(func: Callable[P, T]) -> Callable[P, T]:
        @functools.wraps(func)
        def decorated(*args: P.args, **kwargs: P.kwargs) -> T:
            validations.check_fields(fields, kwargs)
            return func(*args, **kwargs)

        return decorated

    return wrapper


def validate_git_root_deco(root_field: str) -> Callable:
    def wrapper(func: Callable) -> Callable:
        @functools.wraps(func)
        def decorated(*args: str, **kwargs: GitRoot | IPRoot | URLRoot) -> Callable:
            root: Root = kwargs[root_field]
            validations.check_git_root(root=root)
            return func(*args, **kwargs)

        return decorated

    return wrapper


def validate_url_deco(url_field: str) -> Callable:
    def wrapper(func: Callable) -> Callable:
        @functools.wraps(func)
        def decorated(*args: str, **kwargs: str) -> Callable:
            url = validations.get_attr_value(field=url_field, kwargs=kwargs, obj_type=str)
            validations.check_url(url=url)
            return func(*args, **kwargs)

        return decorated

    return wrapper


def validate_chart_field_deco(param_value_field: str, param_name_field: str) -> Callable:
    def wrapper(func: Callable) -> Callable:
        @functools.wraps(func)
        def decorated(*args: str, **kwargs: str) -> Callable:
            param_value = validations.get_attr_value(
                field=param_value_field,
                kwargs=kwargs,
                obj_type=str,
            )
            param_name = validations.get_attr_value(
                field=param_name_field,
                kwargs=kwargs,
                obj_type=str,
            )
            validations.validate_chart_field(param_value=param_value, param_name=param_name)
            return func(*args, **kwargs)

        return decorated

    return wrapper


def validate_chart_field_dict_deco(dict_field: str, params_names: Iterable[str]) -> Callable:
    def wrapper(func: Callable) -> Callable:
        @functools.wraps(func)
        def decorated(*args: str, **kwargs: str) -> Callable:
            dict_value = validations.get_attr_value(field=dict_field, kwargs=kwargs, obj_type=dict)
            validations.validate_chart_field_dict(dict_value=dict_value, params_names=params_names)

            return func(*args, **kwargs)

        return decorated

    return wrapper


def validate_file_name_deco(
    field: str,
) -> Callable[[Callable[P, T]], Callable[P, T]]:
    """
    Verify that filename has valid characters. Raises InvalidChar
    otherwise.
    """

    def wrapper(func: Callable[P, T]) -> Callable[P, T]:
        @functools.wraps(func)
        def decorated(*args: P.args, **kwargs: P.kwargs) -> T:
            field_content = str(
                validations.get_attr_value(field=field, kwargs=kwargs, obj_type=str),
            )
            validations.validate_file_name(name=field_content)
            return func(*args, **kwargs)

        return decorated

    return wrapper


def validate_file_exists_deco(field_name: str, field_group_files: str) -> Callable:
    def wrapper(func: Callable) -> Callable:
        @functools.wraps(func)
        def decorated(*args: str, **kwargs: str) -> Callable:
            file_name = validations.get_attr_value(field=field_name, kwargs=kwargs, obj_type=str)
            group_files = validations.get_attr_value(
                field=field_group_files,
                kwargs=kwargs,
                obj_type=list[GroupFile],
            )
            validations.validate_file_exists(file_name=file_name, group_files=group_files)

            return func(*args, **kwargs)

        return decorated

    return wrapper


def validate_length_deco(
    field: str,
    min_length: int | None = None,
    max_length: int | None = None,
) -> Callable[[Callable[P, T]], Callable[P, T]]:
    """
    Validates if field length is between `min_length` and
    `max_length` (inclusive).

    `min_length` and `max_length` are optional. If they are
    not provided, they are not checked.

    Empty strings are allowed when `min_length` is not defined.

    Backward compatibility: if an object has a field as `None`
    and you want to validate its length, it is always valid.

    Throws:
        InvalidFieldLength()
    """

    def wrapper(func: Callable[P, T]) -> Callable[P, T]:
        @functools.wraps(func)
        def decorated(*args: P.args, **kwargs: P.kwargs) -> T:
            field_content = validations.get_attr_value(field=field, kwargs=kwargs, obj_type=str)
            validations.validate_field_length(field_content, min_length, max_length)
            return func(*args, **kwargs)

        return decorated

    return wrapper


def validate_length_field_dict_deco(
    dict_field: str,
    params_names: Iterable[str],
    min_length: int | None = None,
    max_length: int | None = None,
) -> Callable[[Callable[P, T]], Callable[P, T]]:
    def wrapper(func: Callable[P, T]) -> Callable[P, T]:
        @functools.wraps(func)
        def decorated(*args: P.args, **kwargs: P.kwargs) -> T:
            dict_value = validations.get_attr_value(field=dict_field, kwargs=kwargs, obj_type=dict)
            for param_name in params_names:
                validations.validate_field_length(
                    str(dict_value.get(param_name)), min_length, max_length
                )

            return func(*args, **kwargs)

        return decorated

    return wrapper


def validate_all_fields_length_deco(max_length: int, min_length: int | None = None) -> Callable:
    """
    Validates if field length is between `min_length` and
    `max_length` (inclusive) for every field.

    `min_length` is optional. If they are
    not provided, they are not checked.

    Empty strings are allowed when `min_length` is not defined.

    Throws:
        InvalidFieldLength()
    """

    def wrapper(func: Callable) -> Callable:
        @functools.wraps(func)
        def decorated(*args: str, **kwargs: str) -> Callable:
            for value in kwargs.values():
                validations.validate_field_length(value, min_length, max_length)
            return func(*args, **kwargs)

        return decorated

    return wrapper


def validate_fields_length_deco(
    fields: Iterable[str],
    min_length: int | None = None,
    max_length: int | None = None,
) -> Callable:
    """
    Validates if field length is between `min_length` and
    `max_length` (inclusive) for each field in `fields`.

    `min_length` and `max_length` are optional. If they are
    not provided, they are not checked.

    Empty strings are allowed when `min_length` is not defined.

    Backward compatibility: if an object has a field as `None`
    and you want to validate its length, it is always valid.

    Throws:
        InvalidFieldLength()
    """

    def wrapper(func: Callable) -> Callable:
        @functools.wraps(func)
        def decorated(*args: str, **kwargs: str) -> Callable:
            for field in fields:
                field_content = validations.get_sized_attr_value(field=field, kwargs=kwargs)
                validations.validate_field_length(field_content, min_length, max_length)
            return func(*args, **kwargs)

        return decorated

    return wrapper


def validate_finding_id_deco(field: str) -> Callable:
    def wrapper(func: Callable) -> Callable:
        @functools.wraps(func)
        def decorated(*args: str, **kwargs: str) -> Callable:
            field_content = str(
                validations.get_attr_value(field=field, kwargs=kwargs, obj_type=str),
            )
            validations.validate_finding_id(finding_id=field_content)
            return func(*args, **kwargs)

        return decorated

    return wrapper


def validate_group_language_deco(field: str) -> Callable:
    def wrapper(func: Callable) -> Callable:
        @functools.wraps(func)
        def decorated(*args: str, **kwargs: str) -> Callable:
            language = str(validations.get_attr_value(field=field, kwargs=kwargs, obj_type=str))
            validations.validate_group_language(language=language)
            return func(*args, **kwargs)

        return decorated

    return wrapper


def validate_markdown_deco(text_field: str) -> Callable:
    """
    Escapes special characters and accepts only
    the use of certain html tags
    """

    def wrapper(func: Callable) -> Callable:
        @functools.wraps(func)
        def decorated(*args: str, **kwargs: str) -> Callable:
            text = validations.get_attr_value(field=text_field, kwargs=kwargs, obj_type=str)

            validations.validate_markdown(text=text)
            return func(*args, **kwargs)

        return decorated

    return wrapper


def validate_space_field_deco(field: str) -> Callable:
    def wrapper(func: Callable) -> Callable:
        @functools.wraps(func)
        def decorated(*args: str, **kwargs: str) -> Callable:
            field_content = str(
                validations.get_attr_value(field=field, kwargs=kwargs, obj_type=str),
            )
            validations.validate_space_field(field=field_content)
            return func(*args, **kwargs)

        return decorated

    return wrapper


def validate_alphanumeric_field_deco(field: str) -> Callable:
    def wrapper(func: Callable) -> Callable:
        @functools.wraps(func)
        def decorated(*args: str, **kwargs: str) -> Callable:
            field_content = str(
                validations.get_attr_value(field=field, kwargs=kwargs, obj_type=str),
            )
            validations.validate_alphanumeric_field(field=field_content)

            return func(*args, **kwargs)

        return decorated

    return wrapper


def validate_no_duplicate_drafts_deco(
    new_title_field: str,
    drafts_field: str,
    findings_field: str,
) -> Callable:
    def wrapper(func: Callable) -> Callable:
        @functools.wraps(func)
        def decorated(*args: str, **kwargs: str) -> Callable:
            new_title = validations.get_attr_value(
                field=new_title_field,
                kwargs=kwargs,
                obj_type=str,
            )
            drafts = validations.get_attr_value(
                field=drafts_field,
                kwargs=kwargs,
                obj_type=tuple[Finding, ...],
            )
            findings = validations.get_attr_value(
                field=findings_field,
                kwargs=kwargs,
                obj_type=tuple[Finding, ...],
            )
            validations.validate_no_duplicate_drafts(
                new_title=new_title,
                drafts=drafts,
                findings=findings,
            )
            return func(*args, **kwargs)

        return decorated

    return wrapper


def validate_sanitized_csv_input_deco(
    field_names: list[str],
) -> Callable[[Callable[P, T]], Callable[P, T]]:
    """
    Validates if all the fields contain valid characters (avoid CSV
    injection)

    Args:
        field_names (list[str]): List of fields to validate

    Raises:
        UnsanitizedInputFound: if any of the fields contains
        invalid characters

    """

    def decorator(func: Callable[P, T]) -> Callable[P, T]:
        @functools.wraps(func)
        def wrapper(*args: P.args, **kwargs: P.kwargs) -> T:
            """
            Checks for the presence of any character that could be
            interpreted as the start of a formula by a spreadsheet editor
            according to
            https://owasp.org/www-community/attacks/CSV_Injection
            """
            fields_to_validate = [
                str(kwargs.get(field))
                if "." not in field
                else str(getattr(kwargs.get(field.split(".")[0]), field.split(".")[1]))
                for field in field_names
            ]
            validations.validate_sanitized_csv_input(*fields_to_validate)
            return func(*args, **kwargs)

        return wrapper

    return decorator


def validate_commit_hash_deco(field: str) -> Callable:
    def wrapper(func: Callable) -> Callable:
        @functools.wraps(func)
        def decorated(*args: str, **kwargs: str) -> Callable:
            commit_hash = str(validations.get_attr_value(field=field, kwargs=kwargs, obj_type=str))
            validations.check_commit_hash(commit_hash)
            return func(*args, **kwargs)

        return decorated

    return wrapper


def validate_int_range_deco(
    field: str,
    lower_bound: int,
    upper_bound: int,
    inclusive: bool,
) -> Callable:
    def wrapper(func: Callable) -> Callable:
        @functools.wraps(func)
        def decorated(*args: str, **kwargs: str) -> Callable:
            value = validations.get_attr_value(field=field, kwargs=kwargs, obj_type=int)
            validations.check_range(
                value,
                lower_bound,
                upper_bound,
                inclusive,
                NumberOutOfRange(lower_bound, upper_bound, inclusive),
            )
            return func(*args, **kwargs)

        return decorated

    return wrapper


def validate_root_type_deco(root_field: str, root_type: tuple) -> Callable:
    def wrapper(func: Callable) -> Callable:
        @functools.wraps(func)
        def decorated(*args: str, **kwargs: str) -> Callable:
            root = validations.get_attr_value(
                field=root_field,
                kwargs=kwargs,
                obj_type=type(GitRoot | IPRoot | URLRoot),
            )
            if not isinstance(root, root_type):
                raise InvalidRootType()
            return func(*args, **kwargs)

        return decorated

    return wrapper


def validate_start_letter_deco(field: str) -> Callable:
    def wrapper(func: Callable) -> Callable:
        @functools.wraps(func)
        def decorated(*args: str, **kwargs: str) -> Callable:
            field_content = str(
                validations.get_attr_value(field=field, kwargs=kwargs, obj_type=str),
            )
            validations.validate_start_letter(value=field_content)

            return func(*args, **kwargs)

        return decorated

    return wrapper


def validate_include_number_deco(field: str) -> Callable:
    def wrapper(func: Callable) -> Callable:
        @functools.wraps(func)
        def decorated(*args: str, **kwargs: str) -> Callable:
            field_content = str(
                validations.get_attr_value(field=field, kwargs=kwargs, obj_type=str),
            )
            validations.validate_include_number(value=field_content)
            return func(*args, **kwargs)

        return decorated

    return wrapper


def validate_include_lowercase_deco(field: str) -> Callable:
    def wrapper(func: Callable) -> Callable:
        @functools.wraps(func)
        def decorated(*args: str, **kwargs: str) -> Callable:
            field_content = str(
                validations.get_attr_value(field=field, kwargs=kwargs, obj_type=str),
            )
            validations.validate_include_lowercase(value=field_content)

            return func(*args, **kwargs)

        return decorated

    return wrapper


def validate_include_uppercase_deco(field: str) -> Callable:
    def wrapper(func: Callable) -> Callable:
        @functools.wraps(func)
        def decorated(*args: str, **kwargs: str) -> Callable:
            field_content = str(
                validations.get_attr_value(field=field, kwargs=kwargs, obj_type=str),
            )
            validations.validate_include_uppercase(value=field_content)
            return func(*args, **kwargs)

        return decorated

    return wrapper


def validate_sequence_deco(field: str) -> Callable:
    def wrapper(func: Callable) -> Callable:
        @functools.wraps(func)
        def decorated(*args: str, **kwargs: str) -> Callable:
            value = str(validations.get_attr_value(field=field, kwargs=kwargs, obj_type=str))
            validations.validate_sequence(value=value)
            return func(*args, **kwargs)

        return decorated

    return wrapper


def validate_symbols_deco(field: str) -> Callable:
    def wrapper(func: Callable) -> Callable:
        @functools.wraps(func)
        def decorated(*args: str, **kwargs: str) -> Callable:
            value = str(validations.get_attr_value(field=field, kwargs=kwargs, obj_type=str))
            validations.validate_symbols(value=value)
            return func(*args, **kwargs)

        return decorated

    return wrapper


def validate_severity_range_deco(field: str, min_value: Decimal, max_value: Decimal) -> Callable:
    def wrapper(func: Callable) -> Callable:
        @functools.wraps(func)
        def decorated(*args: str, **kwargs: str) -> Callable:
            severity = validations.get_attr_value(field=field, kwargs=kwargs, obj_type=int)
            validations.check_range_severity(
                severity,
                min_value,
                max_value,
                InvalidSeverity([min_value, max_value]),
            )
            return func(*args, **kwargs)

        return decorated

    return wrapper


def validate_field_exist_deco(field: str) -> Callable:
    """
    Validates if a field is not None

    Args:
        field (str): Name of the field to validate

    Raises:
        InvalidParameter: if the field is None

    """

    def wrapper(func: Callable) -> Callable:
        @functools.wraps(func)
        def decorated(*args: str, **kwargs: str) -> Callable:
            if kwargs[field] is None:
                raise InvalidParameter(field)
            return func(*args, **kwargs)

        return decorated

    return wrapper
