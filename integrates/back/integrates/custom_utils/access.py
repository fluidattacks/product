from integrates.dataloaders import Dataloaders
from integrates.db_model.group_access.types import GroupInvitation
from integrates.db_model.organization_access.enums import (
    OrganizationInvitiationState,
)
from integrates.db_model.organization_access.types import (
    OrganizationInvitation,
)


def format_invitation_state(
    invitation: OrganizationInvitation | GroupInvitation | None,
    is_registered: bool,
) -> str:
    if invitation and not invitation.is_used:
        return OrganizationInvitiationState.PENDING
    if not is_registered:
        return OrganizationInvitiationState.UNREGISTERED
    return OrganizationInvitiationState.REGISTERED


async def get_stakeholder_groups_names(loaders: Dataloaders, email: str, active: bool) -> list[str]:
    groups_access = await loaders.stakeholder_groups_access.load(email)

    return [
        group_access.group_name
        for group_access in groups_access
        if group_access.state.has_access == active
    ]
