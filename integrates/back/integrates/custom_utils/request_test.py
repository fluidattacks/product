from integrates.custom_utils.requests import (
    map_source,
)
from integrates.testing.utils import parametrize


@parametrize(
    args=["source", "expect_source"],
    cases=[
        ["integrates", "asm"],
        ["skims", "machine"],
    ],
)
async def test_map_source(source: str, expect_source: str) -> None:
    assert map_source(source) == expect_source
