from datetime import (
    datetime,
)

from integrates.custom_utils.event_comments import (
    format_event_consulting_resolve,
)
from integrates.db_model.event_comments.types import (
    EventComment,
)


async def test_format_event_consulting_resolve() -> None:
    test_data = EventComment(
        event_id="422286126",
        id="1566336916294",
        group_name="group1",
        parent_id="0",
        creation_date=datetime.fromisoformat("2019-08-20T21:35:16+00:00"),
        content="This is a commenting test",
        email="unittest@fluidattacks.com",
        full_name="Unit Test",
    )

    res_data_fullname = format_event_consulting_resolve(test_data)
    assert res_data_fullname["fullname"] == "Fluid Attacks"

    res_data_fullname = format_event_consulting_resolve(
        test_data, target_email="test@fluidattacks.com"
    )
    assert res_data_fullname["fullname"] == "Unit Test"

    test_data = test_data._replace(
        email="unittest@gmail.com",
    )
    res_data_empty_fullname = format_event_consulting_resolve(test_data)
    assert res_data_empty_fullname["fullname"] == "Unit Test"

    test_data = test_data._replace(full_name=None)
    res_data_empty_fullname = format_event_consulting_resolve(test_data)
    assert res_data_empty_fullname["fullname"] == "unittest@gmail.com"
