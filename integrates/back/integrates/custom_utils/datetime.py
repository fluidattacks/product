from calendar import (
    monthrange,
)
from datetime import (
    UTC,
    datetime,
    timedelta,
)
from time import (
    strptime,
)
from typing import (
    NamedTuple,
)

import pytz
from dateutil.relativedelta import (
    relativedelta,
)

from integrates.settings import (
    TIME_ZONE,
)

DEFAULT_DATE_FORMAT: str = "%Y-%m-%d %H:%M:%S"
DEFAULT_ISO_STR = "2000-01-01T05:00:00+00:00"
TZ = pytz.timezone(TIME_ZONE)


class DateRange(NamedTuple):
    start_date: datetime
    end_date: datetime


class ReportDates(NamedTuple):
    today: DateRange
    past_day: DateRange


def as_zone(
    date: datetime,
    zone: str = TIME_ZONE,
) -> datetime:
    return date.astimezone(tz=pytz.timezone(zone))


def format_comment_datetime(date: datetime) -> str:
    return get_as_str(date, date_format="%Y/%m/%d %H:%M:%S")


def get_from_str(
    date_str: str,
    date_format: str = DEFAULT_DATE_FORMAT,
    zone: str = TIME_ZONE,
) -> datetime:
    unaware_datetime = datetime.strptime(date_str, date_format)
    return pytz.timezone(zone).localize(unaware_datetime, is_dst=False)


def get_as_str(
    date: datetime,
    date_format: str = DEFAULT_DATE_FORMAT,
    zone: str = TIME_ZONE,
) -> str:
    return date.astimezone(tz=pytz.timezone(zone)).strftime(date_format)


def get_now(zone: str = TIME_ZONE) -> datetime:
    return datetime.now(tz=pytz.timezone(zone))


def get_utc_now() -> datetime:
    return get_now(zone="UTC")


def get_now_as_str(zone: str = TIME_ZONE) -> str:
    return get_as_str(get_now(zone))


def get_utc_timestamp() -> float:
    return datetime.now().timestamp()


def get_hour_now() -> str:
    now = get_now()
    hour_str = now.strftime("%H:%M:%S")
    return hour_str


def get_plus_delta(
    date: datetime,
    *,
    days: float = 0,
    seconds: float = 0,
    microseconds: float = 0,
    milliseconds: float = 0,
    minutes: float = 0,
    hours: float = 0,
    weeks: float = 0,
) -> datetime:
    date_plus_delta = date + timedelta(
        days=days,
        seconds=seconds,
        microseconds=microseconds,
        milliseconds=milliseconds,
        minutes=minutes,
        hours=hours,
        weeks=weeks,
    )
    return date_plus_delta


def get_now_plus_delta(
    *,
    days: float = 0,
    seconds: float = 0,
    microseconds: float = 0,
    milliseconds: float = 0,
    minutes: float = 0,
    hours: float = 0,
    weeks: float = 0,
    zone: str = TIME_ZONE,
) -> datetime:
    now = get_now(zone=zone)
    now_plus_delta = get_plus_delta(
        now,
        days=days,
        seconds=seconds,
        microseconds=microseconds,
        milliseconds=milliseconds,
        minutes=minutes,
        hours=hours,
        weeks=weeks,
    )
    return now_plus_delta


def get_minus_delta(
    date: datetime,
    *,
    days: float = 0,
    seconds: float = 0,
    microseconds: float = 0,
    milliseconds: float = 0,
    minutes: float = 0,
    hours: float = 0,
    weeks: float = 0,
) -> datetime:
    date_minus_delta = date - timedelta(
        days=days,
        seconds=seconds,
        microseconds=microseconds,
        milliseconds=milliseconds,
        minutes=minutes,
        hours=hours,
        weeks=weeks,
    )
    return date_minus_delta


def get_now_minus_delta(
    *,
    days: float = 0,
    seconds: float = 0,
    microseconds: float = 0,
    milliseconds: float = 0,
    minutes: float = 0,
    hours: float = 0,
    weeks: float = 0,
    zone: str = TIME_ZONE,
) -> datetime:
    now = get_now(zone=zone)
    now_minus_delta = get_minus_delta(
        now,
        days=days,
        seconds=seconds,
        microseconds=microseconds,
        milliseconds=milliseconds,
        minutes=minutes,
        hours=hours,
        weeks=weeks,
    )
    return now_minus_delta


def get_from_epoch(epoch: int) -> datetime:
    return datetime.fromtimestamp(epoch, TZ)


def get_as_epoch(date: datetime) -> int:
    return int(date.timestamp())


def get_as_utc_iso_format(date: datetime) -> str:
    return date.astimezone(tz=UTC).isoformat()


def get_iso_date() -> str:
    return datetime.now(tz=UTC).isoformat()


def is_valid_format(date_str: str, date_format: str = DEFAULT_DATE_FORMAT) -> bool:
    try:
        get_from_str(date_str, date_format)
        return True
    except ValueError:
        return False


def get_first_day_next_month(date: datetime) -> datetime:
    return date.replace(day=1) + relativedelta(months=+1)


def get_days_since(date: datetime) -> int:
    return (get_utc_now() - date).days


def get_report_dates(
    date: datetime = datetime.now(pytz.UTC),
) -> ReportDates:
    report_date = date.replace(hour=0, minute=0, second=0, microsecond=0)
    date_range_today = 3 if report_date.weekday() == 0 else 1
    date_range_past_day = 3 if report_date.weekday() == 1 else 1
    return ReportDates(
        today=DateRange(
            start_date=report_date - timedelta(days=date_range_today),
            end_date=report_date,
        ),
        past_day=DateRange(
            start_date=report_date - timedelta(days=date_range_today + date_range_past_day),
            end_date=report_date - timedelta(days=date_range_today),
        ),
    )


def translate_date_last(date_str: str) -> datetime:
    parts = date_str.replace(",", "").replace("- ", "").split(" ")

    if len(parts) == 6:
        date_year, date_month, date_day = parts[5], parts[3], parts[4]
    elif len(parts) == 5:
        date_year, date_month, date_day = parts[4], parts[2], parts[3]
    elif len(parts) == 4:
        date_year, date_month, date_day = parts[3], parts[0], parts[2]
    else:
        raise ValueError(f"Unexpected number of parts: {parts}")

    return datetime(int(date_year), strptime(date_month, "%b").tm_mon, int(date_day))


def get_yearly(x_date: str) -> str:
    data_date = translate_date_last(x_date)
    year_last_day = datetime(data_date.year, 12, 31).date()
    today = datetime.now().date()
    limit_time = datetime.min.time()
    date_format = "%Y - %m - %d"

    if year_last_day < today:
        return datetime.combine(year_last_day, limit_time).strftime(date_format)

    return datetime.combine(datetime.now(), limit_time).strftime(date_format)


def create_weekly_date(first_date: datetime) -> str:
    """Create format weekly date."""
    begin = get_minus_delta(first_date, days=(first_date.isoweekday() - 1) % 7)
    end = get_plus_delta(begin, days=6)
    if begin.year != end.year:
        date = "{0:%b} {0.day}, {0.year} - {1:%b} {1.day}, {1.year}"
    elif begin.month != end.month:
        date = "{0:%b} {0.day} - {1:%b} {1.day}, {1.year}"
    else:
        date = "{0:%b} {0.day} - {1.day}, {1.year}"

    return date.format(begin, end)


def create_date(first_date: datetime) -> str:
    month_days: int = monthrange(int(first_date.strftime("%Y")), int(first_date.strftime("%m")))[1]
    begin = get_minus_delta(first_date, days=(int(first_date.strftime("%d")) - 1) % month_days)
    end = get_plus_delta(begin, days=month_days - 1)
    if begin.year != end.year:
        date = "{0:%b} {0.day}, {0.year} - {1:%b} {1.day}, {1.year}"
    elif begin.month != end.month:
        date = "{0:%b} {0.day} - {1:%b} {1.day}, {1.year}"
    else:
        date = "{0:%b} {0.day} - {1.day}, {1.year}"

    return date.format(begin, end)
