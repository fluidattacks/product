from collections.abc import (
    Iterable,
)
from typing import (
    Any,
)

from pycountry import (
    countries,
)

from integrates.db_model.organizations.enums import OrganizationStateStatus
from integrates.db_model.organizations.types import Organization


def is_deleted(organization: Organization) -> bool:
    return organization.state.status == OrganizationStateStatus.DELETED


def filter_active_organizations(
    organizations: Iterable[Organization],
) -> list[Organization]:
    return [organization for organization in organizations if not is_deleted(organization)]


def get_organization_country(context: Any) -> str:
    country_code = context.headers.get("cf-ipcountry", "undefined")
    return country_code if country_code == "undefined" else countries.get(alpha_2=country_code).name
