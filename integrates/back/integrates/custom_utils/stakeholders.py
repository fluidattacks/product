from collections.abc import (
    Sequence,
)

from aioextensions import (
    collect,
)

from integrates.class_types.types import (
    Item,
)
from integrates.custom_exceptions import (
    StakeholderNotFound,
)
from integrates.custom_utils import (
    validations,
)
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.group_access.types import (
    GroupAccess,
)
from integrates.db_model.organization_access.types import (
    OrganizationAccess,
)
from integrates.db_model.stakeholders.types import (
    Stakeholder,
)


def get_full_name(stakeholder_info: dict[str, str]) -> str:
    return str.join(
        " ",
        [
            stakeholder_info.get("first_name", ""),
            stakeholder_info.get("last_name", ""),
        ],
    )


def format_invitation_state(invitation: Item, is_registered: bool) -> str:
    if invitation and not invitation["is_used"]:
        return "PENDING"
    if not is_registered:
        return "UNREGISTERED"
    return "REGISTERED"


async def append_to_members(loaders: Dataloaders, email: str, members: list[Stakeholder]) -> None:
    if member := await loaders.stakeholder.load(email):
        members.append(member)
    else:
        members.append(Stakeholder(email=email))


async def fill_members(
    loaders: Dataloaders,
    email_list: list[str],
    members: list[Stakeholder],
) -> None:
    await collect([append_to_members(loaders, email, members) for email in email_list])


async def _get_group_access(
    loaders: Dataloaders,
    email: str,
) -> Sequence[GroupAccess | OrganizationAccess]:
    return await loaders.stakeholder_groups_access.load(email)


async def _get_organization_access(
    loaders: Dataloaders,
    email: str,
) -> Sequence[GroupAccess | OrganizationAccess]:
    return await loaders.stakeholder_organizations_access.load(email)


async def _pending_invitations(
    loaders: Dataloaders,
    email: str,
) -> tuple[Sequence[GroupAccess | OrganizationAccess], ...]:
    return await collect(
        (
            _get_group_access(loaders, email),
            _get_organization_access(loaders, email),
        ),
    )


async def has_all_invitation_pending(loaders: Dataloaders, email: str) -> bool:
    _accesses = await _pending_invitations(loaders, email)
    accesses = [access for _access in _accesses for access in _access]
    if not accesses:
        return False

    return not any(
        access.state.invitation.is_used if access.state.invitation else True for access in accesses
    )


async def get_stakeholder(
    loaders: Dataloaders,
    email: str,
    user_email: str | None = None,
) -> Stakeholder:
    if (
        user_email
        and not validations.is_fluid_staff(user_email)
        and validations.is_fluid_staff(email)
    ):
        raise StakeholderNotFound()

    stakeholder = await loaders.stakeholder.load(email)

    if stakeholder:
        return stakeholder

    raise StakeholderNotFound()
