import base64
import json
import logging
import logging.config
from contextlib import (
    suppress,
)
from datetime import (
    datetime,
)
from decimal import (
    Decimal,
    InvalidOperation,
)
from urllib.parse import (
    quote,
)

import aiohttp
from aioextensions import (
    in_thread,
)
from mixpanel import (
    Consumer,
    Mixpanel,
    MixpanelException,
)
from pytz import (
    timezone,
)

from integrates.context import (
    FI_ENVIRONMENT,
)
from integrates.settings import (
    LOGGING,
    MIXPANEL_API_SECRET,
    MIXPANEL_API_TOKEN,
)

logging.config.dictConfig(LOGGING)

# Constants
LOGGER = logging.getLogger(__name__)
MIXPANEL_EVENT_EXPORT_URL = "https://data.mixpanel.com/api/2.0/export"


def is_decimal(num: str) -> bool:
    try:
        Decimal(num)
        return True
    except InvalidOperation:
        with suppress(InvalidOperation):
            Decimal(num[:-1])
            return True
        return False


async def mixpanel_track(email: str, event: str, **extra: str) -> None:
    if FI_ENVIRONMENT == "production":
        mp_instance = Mixpanel(
            MIXPANEL_API_TOKEN,
            Consumer(request_timeout=10, retry_backoff_factor=1, retry_limit=5),
        )
        try:
            await in_thread(
                mp_instance.track,
                email,
                event,
                {"integrates_user_email": email, **extra},
            )
        except MixpanelException as exc:
            LOGGER.error(
                "Failed to track event",
                extra={"extra": {"exception": exc, "event": event}},
            )


async def get_mixpanel_events(
    event: list[str],
    from_date: datetime = datetime.now(tz=timezone("America/Bogota")),
    to_date: datetime = datetime.now(tz=timezone("America/Bogota")),
) -> list[dict[str, dict[str, str | int] | str]]:
    url = (
        f"{MIXPANEL_EVENT_EXPORT_URL}?"
        + f"from_date={from_date.strftime('%Y-%m-%d')}&"
        + f"to_date={to_date.strftime('%Y-%m-%d')}&"
        + f"event={quote(json.dumps(event))}"
    )
    encoding = "UTF-8"
    b64_payload = base64.b64encode(MIXPANEL_API_SECRET.encode(encoding)).decode(encoding)
    error_msg = "Couldn't fetch mixpanel events. "

    try:
        async with aiohttp.ClientSession(timeout=aiohttp.ClientTimeout(total=600)) as session:
            # NOFLUID Consumed data is not critical
            async with session.get(
                headers={
                    "accept": "text/plain",
                    "authorization": f"Basic {b64_payload}",
                },
                url=url,
            ) as response:
                if response.status == 200:
                    data = await response.text()
                    return [json.loads(item) for item in data.split("\n") if item]
                LOGGER.error(
                    error_msg,
                    extra={"extra": {"response": response}},
                )
                return []
    except (TimeoutError, aiohttp.ClientError) as exception:
        LOGGER.error("%s %s", error_msg, exception)
        return []
