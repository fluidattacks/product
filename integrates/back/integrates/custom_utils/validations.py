import logging
import logging.config
import re
from collections.abc import Callable, Iterable, Sized
from datetime import UTC, datetime
from decimal import Decimal
from functools import wraps
from typing import LiteralString, NamedTuple, ParamSpec, TypeVar, cast
from urllib.parse import ParseResult, unquote, urlparse
from uuid import UUID

import nh3
from bs4 import BeautifulSoup
from markdown import markdown
from starlette.datastructures import UploadFile
from urllib3.exceptions import LocationParseError
from urllib3.util.url import Url, parse_url
from xmldiff import main

from integrates.custom_exceptions import (
    AlreadyOnHold,
    AlreadyZeroRiskRequested,
    CustomBaseException,
    DuplicateDraftFound,
    ErrorFileNameAlreadyExists,
    InactiveRoot,
    InvalidAffectedReattacks,
    InvalidChar,
    InvalidCommitHash,
    InvalidField,
    InvalidFieldChange,
    InvalidFieldLength,
    InvalidGitRoot,
    InvalidJustificationMaxLength,
    InvalidMarkdown,
    InvalidMinTimeToRemediate,
    InvalidParameter,
    InvalidProtocol,
    InvalidReportFilter,
    InvalidSpacesField,
    InvalidStatusForSeverityUpdate,
    InvalidStatusForSeverityUpdateRequest,
    NotVerificationRequested,
    NotZeroRiskRequested,
    NumberOutOfRange,
    OutdatedRepository,
    UnsanitizedInputFound,
    VulnAlreadyClosed,
    VulnerabilitiesHasUnsolvedEvents,
    VulnerabilityHasNotBeenRejected,
    VulnerabilityHasNotBeenReleased,
    VulnerabilityHasNotBeenSubmitted,
    VulnSeverityScoreUpdateAlreadyRequested,
    VulnSeverityScoreUpdateNotRequested,
)
from integrates.custom_utils.roots import fetch_temp_aws_creds
from integrates.dataloaders import Dataloaders
from integrates.db_model.events.enums import EventType
from integrates.db_model.findings.enums import FindingStatus
from integrates.db_model.findings.types import Finding
from integrates.db_model.groups.types import GroupFile
from integrates.db_model.roots.enums import RootStatus, RootType
from integrates.db_model.roots.types import Root
from integrates.db_model.vulnerabilities.constants import RELEASED_FILTER_STATUSES
from integrates.db_model.vulnerabilities.enums import (
    VulnerabilitySeverityProposalStatus,
    VulnerabilityStateStatus,
    VulnerabilityType,
    VulnerabilityVerificationStatus,
    VulnerabilityZeroRiskStatus,
)
from integrates.db_model.vulnerabilities.types import Vulnerability, VulnerabilitySeverityProposal
from integrates.settings import LOGGING

logging.config.dictConfig(LOGGING)

T = TypeVar("T")
P = ParamSpec("P")
LOGGER = logging.getLogger(__name__)


def is_valid_url(url: str) -> bool:
    try:
        url_attributes: Url | ParseResult = parse_url(url)
    except LocationParseError:
        url_attributes = urlparse(url)

    return bool(url_attributes.netloc and url_attributes.scheme)


def check_exp(field: str, regexp: str, ex: CustomBaseException) -> None:
    if not re.match(regexp, field.replace("\n", " ").strip(), re.MULTILINE):
        raise ex


def check_wildcard(value: str) -> None:
    if "*" in value or "?" in value:
        raise InvalidChar()


def check_email(
    email: str,
    ex: CustomBaseException = InvalidField("email address"),
) -> None:
    if "+" in email:
        raise ex
    check_exp(
        email,
        r"^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,})$",
        ex,
    )


def validate_email_address(email: str) -> bool:
    if "+" in email:
        raise InvalidField("email address")
    try:
        check_field(
            email,
            r"^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,})$",
        )
        return True
    except InvalidChar as ex:
        raise InvalidField("email address") from ex


def validate_fields(fields: Iterable[str]) -> None:
    allowed_chars = (
        r"a-zA-Z0-9ñáéíóúäëïöüÑÁÉÍÓÚÄËÏÖÜ\s'~:;%@&_$#=¡!¿\,\.\*\-\?\"\[\]\|\(\)\/\{\}\>\+"
    )
    regex = rf'^[{allowed_chars.replace("=", "")}][{allowed_chars}]*$'
    for field in map(str, fields):
        if field:
            check_field(field, regex)


def isinstance_namedtuple(obj: object) -> bool:
    return isinstance(obj, tuple) and hasattr(obj, "_asdict") and hasattr(obj, "_fields")


def check_all_attr(obj: object, regex: str, ex: CustomBaseException) -> None:
    for val in list(cast(NamedTuple, obj)._asdict().values()):
        check_exp(str(val), regex, ex)


def check_all_list(list_to_check: list[str], regex: str, ex: CustomBaseException) -> None:
    for val in list_to_check:
        if val:
            check_exp(str(val), regex, ex)


def _check_field(
    field: str, kwargs: dict, regex: LiteralString, ex: CustomBaseException = InvalidChar()
) -> None:
    value = kwargs.get(field)
    if isinstance_namedtuple(value):
        check_all_attr(value, regex, ex)
    elif isinstance(value, list):
        check_all_list(value, regex, ex)
    elif "." in field:
        obj_name, attr_name = field.split(".")
        obj = kwargs.get(obj_name)
        if obj_name in kwargs:
            field_content = (
                obj.get(attr_name, "") if isinstance(obj, dict) else getattr(obj, attr_name)
            )
            if field_content:
                check_exp(str(field_content), regex, ex)
    elif field in kwargs and value:
        check_exp(str(value), regex, ex)


def check_fields(
    fields: Iterable[str], kwargs: dict, ex: CustomBaseException = InvalidChar()
) -> None:
    allowed_chars = (
        r"a-zA-Z0-9ñáéíóúäëïöüÑÁÉÍÓÚÄËÏÖÜ\s'~:;%@&_$#=¡!¿\,\.\*\-\?\"\[\]\|\(\)\/\{\}\>\+"
    )
    regex = rf'^[{allowed_chars.replace("=", "")}][{allowed_chars}]*$'
    for field in fields:
        _check_field(field, kwargs, regex, ex)


def is_fluid_staff(email: str) -> bool:
    return email.endswith("@fluidattacks.com")


def validate_uuid4(uuid_string: str) -> None:
    try:
        UUID(uuid_string, version=4)
    except ValueError as ex:
        raise InvalidParameter("credential_id") from ex


def check_url(url: str | None, ex: CustomBaseException = InvalidChar()) -> None:
    clean_url = url if url is not None else ""

    if clean_url != "":
        if any(char in clean_url for char in ["{", "}"]):
            raise ex
        decoded_url = unquote(clean_url).replace(" ", "")
        allowed_chars = r"a-zA-Z0-9(){},./:;&@_$#=\?\-áéíóúÁÉÍÓÚ"

        if not re.fullmatch(f"^[{allowed_chars}]+$", decoded_url):
            raise ex


def check_active_root(root: Root) -> None:
    if root.state.status != RootStatus.ACTIVE:
        raise InactiveRoot()


def check_git_root(root: Root) -> None:
    if root.type != RootType.GIT:
        raise InvalidGitRoot()


def check_protocol(new_url: str) -> None:
    new_scheme = new_url.split("://")[0]
    if new_scheme == "http":
        raise InvalidProtocol("http")


def validate_chart_field(param_value: str, param_name: str) -> None:
    is_valid = bool(re.search("^[A-Za-z0-9 #_-]*$", str(param_value)))
    if not is_valid:
        raise InvalidChar(param_name)


def validate_chart_field_dict(dict_value: dict, params_names: Iterable[str]) -> None:
    for param_name in params_names:
        check_exp(
            str(dict_value.get(param_name)),
            r"^[A-Za-z0-9 #_-]*$",
            InvalidChar(param_name),
        )


def validate_file_name(name: str) -> None:
    """
    Verify that filename has valid characters. Raises InvalidChar
    otherwise.
    """
    name = str(name)
    name_len = len(name.split("."))
    if name_len <= 2:
        is_valid = bool(re.search("^[A-Za-z0-9!_.*/'()&$@=;:+,? -]*$", str(name)))
        if not is_valid:
            raise InvalidChar("filename")
    else:
        raise InvalidChar("filename")


def validate_file_exists(file_name: str, group_files: list[GroupFile] | None) -> None:
    """Verify that file name is not already in group files."""
    if group_files:
        file_to_check = next(
            (group_file for group_file in group_files if group_file.file_name == file_name),
            None,
        )
        if file_to_check is not None:
            raise ErrorFileNameAlreadyExists.new()


def check_field(field: str, regexp: str) -> None:
    if not re.match(regexp, field.replace("\n", " ").strip(), re.MULTILINE):
        raise InvalidChar()


def check_length(
    payload: str,
    min_length: int | None = None,
    max_length: int | None = None,
    ex: CustomBaseException = InvalidFieldLength(),
) -> bool:
    """
    Checks if payload length is between `min_length` and `max_length` and
    returns **True** if it is. Otherwise, throws ex.

    min_length and max_length are inclusive and optional.
    If they are not provided, they are not checked.
    """
    if payload is None:
        raise ex
    str_length = len(payload)
    if (min_length is not None and str_length < min_length) or (
        max_length is not None and str_length > max_length
    ):
        raise ex
    return True


def validate_finding_id(finding_id: str) -> None:
    if not re.fullmatch(
        r"[0-9A-Za-z]{8}-[0-9A-Za-z]{4}-4[0-9A-Za-z]{3}-[89ABab]"
        r"[0-9A-Za-z]{3}-[0-9A-Za-z]{12}|\d+",
        finding_id,
    ):
        raise InvalidField("finding id")


def validate_group_language(language: str) -> None:
    if language.upper() not in {"EN", "ES"}:
        raise InvalidField("group language")


def assert_is_isalnum(key: str) -> Callable[[Callable[P, T]], Callable[P, T]]:
    def wrapper(func: Callable[P, T]) -> Callable[P, T]:
        @wraps(func)
        def decorated(*args: P.args, **kwargs: P.kwargs) -> T:
            if not str(kwargs[key]).isalnum():
                raise InvalidField(key.replace("_", " "))

            return func(*args, **kwargs)

        return decorated

    return wrapper


def validate_markdown(text: str) -> str:
    """
    Escapes special characters and accepts only
    the use of certain html tags
    """
    if not text.strip():
        return text

    allowed_tags = {
        "a",
        "b",
        "br",
        "div",
        "dl",
        "dt",
        "em",
        "h1",
        "h2",
        "h3",
        "h4",
        "h5",
        "h6",
        "img",
        "li",
        "ol",
        "p",
        "small",
        "strong",
        "table",
        "tbody",
        "td",
        "tfoot",
        "th",
        "tr",
        "tt",
        "ul",
    }
    try:
        diffs_after_validations = main.diff_texts(
            BeautifulSoup(markdown(text), "lxml").prettify(),
            BeautifulSoup(
                nh3.clean(markdown(text), tags=allowed_tags, link_rel=None).replace("&amp;", "&"),
                "lxml",
            ).prettify(),
        )
    except Exception as exc:
        LOGGER.error("Error validating markdown", extra={"extra": {"exception": exc}})
        raise InvalidMarkdown() from exc

    if diffs_after_validations:
        raise InvalidMarkdown()

    return nh3.clean(text, tags=allowed_tags).replace("&amp;", "&")


def validate_space_field(field: str) -> None:
    if not re.search(r"\S", field):
        raise InvalidSpacesField


def check_alnum(field: str) -> bool:
    is_alnum = all(word.isalnum() for word in field.split())
    return is_alnum or field == "-" or not field


def validate_alphanumeric_field(field: str) -> bool:
    """Optional whitespace separated string, with alphanumeric characters."""
    if check_alnum(field=field):
        return True
    raise InvalidField()


def validate_finding_title_change_policy(
    old_title: str,
    new_title: str,
    status: FindingStatus,
) -> bool:
    """
    Blocks finding title changes from going through if the Finding has been
    already approved
    """
    if old_title != new_title and status in [
        FindingStatus.SAFE,
        FindingStatus.VULNERABLE,
    ]:
        raise InvalidFieldChange(
            fields=["title"],
            reason=("The title of a Finding cannot be edited if there are vulnerabilities on it"),
        )
    return True


def get_attr_value(field: str, kwargs: dict, obj_type: type[T]) -> T:
    if "." not in field:
        return cast(T, kwargs.get(field))
    obj_name, obj_attr = field.split(".", 1)
    parts = obj_attr.split(".")
    obj = kwargs.get(obj_name)
    for part in parts:
        value = getattr(obj, part)
        obj = value
    if isinstance_namedtuple(value) and obj_type is dict:
        return value._asdict()
    if not isinstance(value, obj_type):
        return cast(T, value)
    return value


def get_sized_attr_value(field: str, kwargs: dict) -> Sized:
    try:
        if "." in field:
            obj_name, obj_attr = field.split(".", 1)
            parts = obj_attr.split(".")
            obj = kwargs[obj_name]
            field_name = parts[-1]
            for part in parts:
                value = getattr(obj, part)
                obj = value
        else:
            value = kwargs[field]
            field_name = field
        if not isinstance(value, Sized):
            raise InvalidField(field_name)
    except KeyError as exc:
        LOGGER.error("Unexpected key", extra={"extra": {"exception": exc}})
    return value


def validate_no_duplicate_drafts(
    new_title: str,
    drafts: tuple[Finding, ...],
    findings: tuple[Finding, ...],
) -> bool:
    """
    Checks for new draft proposals that are already present in the group,
    returning `True` if there are no duplicates
    """
    for draft in drafts:
        if new_title == draft.title:
            raise DuplicateDraftFound(kind="draft")
    for finding in findings:
        if new_title == finding.title:
            raise DuplicateDraftFound(kind="finding")
    return True


def check_and_set_min_time_to_remediate(mttr: int | str | None) -> int | None:
    """
    Makes sure that min_time_to_remediate is either None or a positive
    number and returns it as an integer.
    """
    try:
        if mttr is None:
            return None
        if int(mttr) > 0:
            return int(mttr)
        raise InvalidMinTimeToRemediate()
    except ValueError as error:
        raise InvalidMinTimeToRemediate() from error


def validate_sanitized_csv_input(*fields: str) -> None:
    """
    Checks for the presence of any character that could be interpreted as
    the start of a formula by a spreadsheet editor according to
    https://owasp.org/www-community/attacks/CSV_Injection
    """
    forbidden_characters: tuple[str, ...] = (
        "-",
        "=",
        "+",
        "@",
        "\t",
        "\r",
        "\n",
        "\\",
    )
    forbidden_characters_str = "".join(forbidden_characters)
    separators = ('"', "'", ",", ";")
    fields_flat = " ".join(fields).split()
    for field in fields_flat:
        # Check if the field starts with a forbidden character
        if field[0] in forbidden_characters:
            raise UnsanitizedInputFound()

        # Check for forbidden characters followed by separators
        for match in re.finditer(f"[{re.escape(forbidden_characters_str)}]", field):
            if match.start() > 0 and field[match.start() - 1] in separators:
                raise UnsanitizedInputFound()


def validate_commit_hash(commit_hash: str) -> None:
    if not (
        # validate SHA-1
        re.match(
            r"^[A-Fa-f0-9]{40}$",
            commit_hash,
        )
        # validate SHA-256
        or re.match(
            r"^[A-Fa-f0-9]{64}$",
            commit_hash,
        )
    ):
        raise InvalidCommitHash()


def check_commit_hash(
    commit_hash: str,
    ex: CustomBaseException = InvalidCommitHash(),
) -> None:
    # validate SHA-1 or SHA-256
    check_exp(commit_hash, r"^[A-Fa-f0-9]{40}$|^[A-Fa-f0-9]{64}$", ex)


def validate_int_range(
    value: int,
    lower_bound: int,
    upper_bound: int,
    inclusive: bool = True,
) -> None:
    if inclusive:
        if not lower_bound <= value <= upper_bound:
            raise NumberOutOfRange(lower_bound, upper_bound, inclusive)
    elif not lower_bound < value < upper_bound:
        raise NumberOutOfRange(lower_bound, upper_bound, inclusive)


def validate_start_letter(value: str) -> None:
    if not value[0].isalpha():
        raise InvalidReportFilter("Password should start with a letter")


def validate_include_number(value: str) -> None:
    if not re.search(r"\d", value):
        raise InvalidReportFilter("Password should include at least one number")


def validate_include_lowercase(value: str) -> None:
    if not any(val.islower() for val in value):
        raise InvalidReportFilter("Password should include lowercase characters")


def validate_include_uppercase(value: str) -> None:
    if not any(val.isupper() for val in value):
        raise InvalidReportFilter("Password should include uppercase characters")


def sequence_increasing(
    char: str,
    current_ord: int,
    sequence: list[int],
    is_increasing: bool,
) -> list[int]:
    if is_increasing and str(chr(sequence[-1])).isalnum() and char.isalnum():
        return [*sequence, current_ord]

    return [current_ord]


def sequence_decreasing(
    char: str,
    current_ord: int,
    sequence: list[int],
    is_increasing: bool,
) -> list[int]:
    if not is_increasing and str(chr(sequence[-1])).isalnum() and char.isalnum():
        return [*sequence, current_ord]

    return [current_ord]


def has_sequence(value: str, sequence_size: int = 3) -> bool:
    if len(value) < sequence_size or sequence_size <= 0:
        return False

    sequence: list[int] = [ord(value[0])]
    is_increasing = False
    for char in value[1:]:
        current_ord: int = ord(char)

        match sequence[-1]:
            case last_sequence_value if last_sequence_value + 1 == current_ord:
                if len(sequence) == 1:
                    is_increasing = True
                sequence = sequence_increasing(char, current_ord, sequence, is_increasing)
            case last_sequence_value if last_sequence_value - 1 == current_ord:
                if len(sequence) == 1:
                    is_increasing = False
                sequence = sequence_decreasing(char, current_ord, sequence, is_increasing)
            case _:
                sequence = [current_ord]

        if len(sequence) == sequence_size:
            return True

    return False


def validate_sequence(value: str) -> None:
    if has_sequence(value):
        raise InvalidReportFilter("Password should not include sequentials characters")


def validate_symbols(value: str) -> None:
    if not re.search(r"[!\";#\$%&'\(\)\*\+,-./:<=>\?@\[\]^_`\{\|\}~]", value):
        raise InvalidReportFilter("Password should include symbols characters")


def check_range(
    value: int,
    lower_bound: int,
    upper_bound: int,
    inclusive: bool,
    ex: CustomBaseException,
) -> None:
    if inclusive:
        if not lower_bound <= value <= upper_bound:
            raise ex
    elif not lower_bound < value < upper_bound:
        raise ex


def check_range_severity(
    severity: int,
    min_value: Decimal,
    max_value: Decimal,
    ex: CustomBaseException,
) -> None:
    if severity and severity != -1:
        check_range(
            severity,
            int(min_value),
            int(max_value),
            True,
            ex,
        )


def validate_closed(vulnerability: Vulnerability) -> Vulnerability:
    """Validate if the vulnerability is closed."""
    if vulnerability.state.status == VulnerabilityStateStatus.SAFE:
        raise VulnAlreadyClosed()
    return vulnerability


async def validate_unsolved_events(
    loaders: Dataloaders,
    vuln_id: str,
    roots_with_events: list[str],
) -> None:
    vulnerability = await loaders.vulnerability.load(vuln_id)
    if vulnerability is not None and vulnerability.root_id in roots_with_events:
        raise VulnerabilitiesHasUnsolvedEvents()


def validate_outdated_repository(
    vulnerability: Vulnerability,
    last_commit_hash: str,
    cloning_date: datetime,
) -> Vulnerability:
    """Validate if the repository is not outdated for reattack."""
    if (
        vulnerability.type == VulnerabilityType.LINES
        and vulnerability.state.commit
        and last_commit_hash == vulnerability.state.commit
        and cloning_date
        and (datetime.now(UTC) - cloning_date).days >= 1
    ):
        LOGGER.error(
            "Could not request reattack",
            extra={
                "extra": {
                    "last_commit_hash": last_commit_hash,
                    "commit_date": cloning_date,
                    "vuln_state_modified_date": (vulnerability.state.modified_date),
                    "vuln_state_commit": vulnerability.state.commit,
                    "vuln_type": vulnerability.type,
                },
            },
        )
        raise OutdatedRepository()

    return vulnerability


def validate_vuln_association(event_type: EventType, vulnerability: Vulnerability) -> None:
    """Check if the vulnerability type is compatible with the event type"""
    if vulnerability.type == VulnerabilityType.LINES and event_type == EventType.ENVIRONMENT_ISSUES:
        raise InvalidAffectedReattacks()


def validate_requested_hold(
    vulnerability: Vulnerability,
) -> Vulnerability:
    """
    Validate if the vulnerability is not on hold and a reattack has been
    requested beforehand
    """
    if (
        vulnerability.verification
        and vulnerability.verification.status == VulnerabilityVerificationStatus.ON_HOLD
    ):
        raise AlreadyOnHold()
    if vulnerability.verification is None or (
        vulnerability.verification
        and vulnerability.verification.status != VulnerabilityVerificationStatus.REQUESTED
    ):
        raise NotVerificationRequested()
    return vulnerability


def validate_reattack_requested(
    vulnerability: Vulnerability,
) -> Vulnerability:
    """Validate if the vulnerability does not have a reattack requested."""
    if (
        not vulnerability.verification
        or vulnerability.verification.status != VulnerabilityVerificationStatus.REQUESTED
    ):
        raise NotVerificationRequested()
    return vulnerability


def validate_justification_length(justification: str) -> None:
    """Validate justification length."""
    max_justification_length = 10000
    if len(justification) > max_justification_length:
        raise InvalidJustificationMaxLength(max_justification_length)


def validate_non_zero_risk_requested(
    vulnerability: Vulnerability,
) -> None:
    """Validate if zero risk vuln is not already resquested."""
    if (
        vulnerability.zero_risk
        and vulnerability.zero_risk.status == VulnerabilityZeroRiskStatus.REQUESTED
    ):
        raise AlreadyZeroRiskRequested()


def validate_rejected(
    vulnerability: Vulnerability,
) -> None:
    """Validate if the vulnerability has been rejected."""
    if vulnerability.state.status is not VulnerabilityStateStatus.REJECTED:
        raise VulnerabilityHasNotBeenRejected()


def validate_released(
    vulnerability: Vulnerability,
) -> None:
    """Validate if the vulnerability is in a released status."""
    if vulnerability.state.status not in RELEASED_FILTER_STATUSES:
        raise VulnerabilityHasNotBeenReleased()


def validate_submitted(
    vulnerability: Vulnerability,
) -> None:
    """Validate if the vulnerability has been submitted."""
    if vulnerability.state.status is not VulnerabilityStateStatus.SUBMITTED:
        raise VulnerabilityHasNotBeenSubmitted()


def validate_zero_risk_requested(
    vulnerability: Vulnerability,
) -> Vulnerability:
    """Validate if zero risk vuln is already resquested."""
    if (
        not vulnerability.zero_risk
        or vulnerability.zero_risk.status != VulnerabilityZeroRiskStatus.REQUESTED
    ):
        raise NotZeroRiskRequested()
    return vulnerability


def validate_file_name_scope(name: str) -> None:
    is_valid = bool(re.search("^[A-Za-z0-9!_.*/'()&$@=;:+,? -]*$", str(name)))
    if not is_valid:
        raise InvalidChar("filename")


def validate_field_length(
    field_content: str | Sized,
    min_length: int | None = None,
    max_length: int | None = None,
) -> bool:
    """
    This method validates field content
    according to type and length.

    If field_content was validated successfully, it returns True.
    Otherwise, it raises InvalidFieldLength.

    If field_content is None, it returns False.
    """
    if field_content is None:
        return False
    if isinstance(field_content, list):
        if min_length is not None and len(field_content) == 0:
            raise InvalidFieldLength()
        for val in field_content:
            check_length(val, min_length, max_length, InvalidFieldLength())
    if isinstance(field_content, str):
        check_length(field_content.strip(), min_length, max_length, InvalidFieldLength())
    return True


def validate_country_tax(country: str, rut: UploadFile | None, tax_id: UploadFile | None) -> None:
    if (country.lower() == "colombia" and rut is None) or (
        country.lower() != "colombia" and tax_id is None
    ):
        raise InvalidParameter("rut field" if country.lower() == "colombia" else "tax id field")


async def validate_aws_role(role: str, external_id: str | None) -> None:
    if external_id and re.match(r"^arn:aws:iam::\d{12}:role\/[\w+=,.@-]+$", role):
        await fetch_temp_aws_creds(role, external_id)


def severity_request_can_be_handled(vulnerability: Vulnerability) -> None:
    if vulnerability.state.status not in [
        VulnerabilityStateStatus.SUBMITTED,
        VulnerabilityStateStatus.VULNERABLE,
        VulnerabilityStateStatus.SAFE,
    ]:
        raise InvalidStatusForSeverityUpdate()


def severity_update_can_be_requested(vulnerability: Vulnerability) -> None:
    if vulnerability.state.status != VulnerabilityStateStatus.VULNERABLE:
        raise InvalidStatusForSeverityUpdateRequest()


def severity_update_requested(
    vulnerability: Vulnerability,
) -> VulnerabilitySeverityProposal:
    if (
        vulnerability.state.proposed_severity is None
        or vulnerability.state.proposed_severity.status
        != VulnerabilitySeverityProposalStatus.REQUESTED
    ):
        raise VulnSeverityScoreUpdateNotRequested()
    return vulnerability.state.proposed_severity


def severity_update_not_requested(
    vulnerability: Vulnerability,
) -> None:
    if (
        vulnerability.state.proposed_severity is None
        or vulnerability.state.proposed_severity.status
        != VulnerabilitySeverityProposalStatus.REQUESTED
    ):
        return
    raise VulnSeverityScoreUpdateAlreadyRequested()


def severity_can_be_directly_updated(vulnerability: Vulnerability) -> None:
    if vulnerability.state.status not in [
        VulnerabilityStateStatus.SUBMITTED,
        VulnerabilityStateStatus.REJECTED,
    ]:
        raise InvalidStatusForSeverityUpdate()
