import html
import itertools
import re
from calendar import monthrange
from collections import Counter
from collections.abc import Iterable
from datetime import UTC, datetime
from datetime import date as datetype
from decimal import ROUND_CEILING, Decimal
from typing import NamedTuple

from integrates.class_types.types import Item
from integrates.custom_exceptions import (
    InvalidRange,
    LineDoesNotExistInTheLinesOfCodeRange,
    VulnerabilityPathDoesNotExistInToeLines,
    VulnerabilityPortFieldDoNotExistInToePorts,
    VulnerabilityUrlFieldAreNotPresentInToeInputs,
    VulnerabilityUrlFieldDoNotExistInToeInputs,
)
from integrates.custom_utils import datetime as datetime_utils
from integrates.custom_utils.exceptions import raise_or_return
from integrates.custom_utils.files import match_files
from integrates.custom_utils.filter_vulnerabilities import filter_same_values
from integrates.custom_utils.groups import get_group
from integrates.custom_utils.utils import ignore_advisories
from integrates.dataloaders import Dataloaders
from integrates.db_model.constants import CVSS_V4_DEFAULT
from integrates.db_model.enums import AcceptanceStatus, Source, TreatmentStatus
from integrates.db_model.findings.types import Finding
from integrates.db_model.roots.types import GitRoot, Root, RootRequest
from integrates.db_model.toe_inputs.types import ToeInput, ToeInputRequest
from integrates.db_model.toe_lines.types import ToeLine, ToeLineRequest
from integrates.db_model.toe_ports.types import ToePort, ToePortRequest
from integrates.db_model.types import SeverityScore, Treatment
from integrates.db_model.utils import adjust_historic_dates
from integrates.db_model.vulnerabilities.constants import (
    RELEASED_FILTER_STATUSES,
    ZR_FILTER_STATUSES,
)
from integrates.db_model.vulnerabilities.enums import (
    VulnerabilityStateReason,
    VulnerabilityStateStatus,
    VulnerabilityToolImpact,
    VulnerabilityType,
    VulnerabilityVerificationStatus,
)
from integrates.db_model.vulnerabilities.types import (
    Vulnerability,
    VulnerabilityState,
    VulnerabilityTool,
    VulnerabilityVerification,
)
from integrates.db_model.vulnerabilities.update import update_event_index, update_historic_entry
from integrates.db_model.vulnerabilities.utils import get_current_treatment_converted
from integrates.treatment.utils import get_inverted_treatment_converted


class Action(NamedTuple):
    action: str
    assigned: str
    date: str
    justification: str
    times: int


PRIORITY_CRITICALITY_FACTOR = {
    "CRITICAL": Decimal(100),
    "DEFAULT": Decimal(0),
    "HIGH": Decimal(75),
    "LOW": Decimal(0),
    "MEDIUM": Decimal(50),
}


def get_path_from_integrates_vulnerability(
    vulnerability_where: str,
    vulnerability_type: VulnerabilityType,
    ignore_cve: bool = False,
) -> tuple[str, str]:
    if vulnerability_type in {
        VulnerabilityType.INPUTS,
        VulnerabilityType.PORTS,
    }:
        if len(chunks := vulnerability_where.rsplit(" (", maxsplit=1)) == 2:
            where, namespace = chunks
            namespace = namespace[:-1]
        else:
            where, namespace = chunks[0], ""
    else:
        where = vulnerability_where
        namespace = ""
    if ignore_cve:
        where = ignore_advisories(where)
    return namespace, where


def as_range(iterable: Iterable[int]) -> str:
    """Convert range into string."""
    my_list = list(iterable)
    range_value = ""
    if len(my_list) > 1:
        range_value = f"{my_list[0]}-{my_list[-1]}"
    else:
        range_value = f"{my_list[0]}"
    return range_value


def is_accepted_undefined_vulnerability(
    vulnerability: Vulnerability,
) -> bool:
    return bool(
        vulnerability.treatment
        and vulnerability.treatment.status == TreatmentStatus.ACCEPTED_UNDEFINED
        and vulnerability.state.status == VulnerabilityStateStatus.VULNERABLE,
    )


def is_deleted(
    vulnerability: Vulnerability,
) -> bool:
    return vulnerability.state.status in {
        VulnerabilityStateStatus.DELETED,
        VulnerabilityStateStatus.MASKED,
    }


def is_machine_vuln(vuln: Vulnerability, author_email: str | None = None) -> bool:
    return vuln.state.source == Source.MACHINE or vuln.hacker_email == (
        author_email or "machine@fluidattacks.com"
    )


def is_reattack_requested(vulnerability: Vulnerability) -> bool:
    return bool(
        vulnerability.verification
        and vulnerability.verification.status == VulnerabilityVerificationStatus.REQUESTED,
    )


def is_reattack_on_hold(vulnerability: Vulnerability) -> bool:
    return bool(
        vulnerability.verification
        and vulnerability.verification.status == VulnerabilityVerificationStatus.ON_HOLD,
    )


def is_range(specific: str) -> bool:
    """Validate if a specific field has range value."""
    return "-" in specific


def _format_tool_item(
    tool: VulnerabilityTool | None,
) -> Item:
    if tool:
        return {
            "name": tool.name,
            "impact": str(tool.impact.value).lower(),
        }

    return {
        "name": "none",
        "impact": VulnerabilityToolImpact.DIRECT.lower(),
    }


def format_vulnerabilities(
    vulnerabilities: Iterable[Vulnerability],
    vulnerabilities_roots: Iterable[Root | None],
) -> dict[str, list[dict[str, str | Item]]]:
    finding: dict[str, list[Item]] = {
        "ports": [],
        "lines": [],
        "inputs": [],
    }
    vuln_values = {
        "ports": {"where": "host", "specific": "port"},
        "lines": {"where": "path", "specific": "line"},
        "inputs": {"where": "url", "specific": "field"},
    }
    for vuln, root in zip(vulnerabilities, vulnerabilities_roots, strict=False):
        vuln_type = str(vuln.type.value).lower()
        item = {
            vuln_values[vuln_type]["where"]: html.unescape(vuln.state.where),
            vuln_values[vuln_type]["specific"]: (html.unescape(vuln.state.specific)),
            "state": get_current_state_converted(vuln.state.status.value).lower(),
            "source": str(vuln.state.source.value).lower()
            if vuln.state.source != Source.ASM
            else "analyst",
            "tool": _format_tool_item(vuln.state.tool),
            "commit_hash": vuln.state.commit,
            "stream": ",".join(vuln.stream) if vuln.stream else None,
            "repo_nickname": root.state.nickname if vuln.root_id and root else None,
            "cvss_v3": vuln.severity_score.cvss_v3
            if vuln.severity_score and vuln.severity_score.cvss_v3
            else None,
            "cvss_v4": vuln.severity_score.cvss_v4
            if vuln.severity_score and vuln.severity_score.cvss_v4
            else None,
            "cwe_ids": vuln.cwe_ids,
        }
        finding[vuln_type].append({key: value for key, value in item.items() if value is not None})

    return finding


def get_opening_date(
    vuln: Vulnerability,
    min_date: datetype | None = None,
) -> datetype | None:
    opening_date: datetype | None = (
        vuln.unreliable_indicators.unreliable_report_date.date()
        if vuln.unreliable_indicators.unreliable_report_date
        else None
    )
    if min_date and opening_date and min_date > opening_date:
        return None
    return opening_date


def get_closing_date(
    vulnerability: Vulnerability,
    min_date: datetype | None = None,
) -> datetype | None:
    closing_date: datetype | None = None
    if vulnerability.state.status == VulnerabilityStateStatus.SAFE:
        closing_date = (
            vulnerability.unreliable_indicators.unreliable_closing_date.date()
            if vulnerability.unreliable_indicators.unreliable_closing_date
            else vulnerability.state.modified_date.date()
        )
        if min_date and min_date > closing_date:
            return None

    return closing_date


def get_mean_remediate_vulnerabilities_cvssf(
    vulns: Iterable[Vulnerability],
    vulns_cvssf: Iterable[Decimal],
    min_date: datetype | None = None,
) -> Decimal:
    total_days = Decimal("0.0")
    open_vuln_dates = [get_opening_date(vuln, min_date) for vuln in vulns]
    filtered_open_vuln_dates = [date for date in open_vuln_dates if date]
    closed_vuln_dates: list[tuple[datetype | None, Decimal]] = [
        (
            get_closing_date(vuln, min_date),
            vuln_cvssf,
        )
        for vuln, open_vuln, vuln_cvssf in zip(vulns, open_vuln_dates, vulns_cvssf, strict=False)
        if open_vuln
    ]
    for index, closed_vuln_date in enumerate(closed_vuln_dates):
        if closed_vuln_date[0] is not None:
            total_days += Decimal(
                (closed_vuln_date[0] - filtered_open_vuln_dates[index]).days * closed_vuln_date[1],
            )
        else:
            current_day = datetime_utils.get_utc_now().date()
            total_days += Decimal(
                (current_day - filtered_open_vuln_dates[index]).days * closed_vuln_date[1],
            )
    total_cvssf = Decimal(
        sum(
            vuln_cvssf
            for open_date, vuln_cvssf in zip(open_vuln_dates, vulns_cvssf, strict=False)
            if open_date
        ),
    )
    if total_cvssf:
        mean_vulnerabilities = Decimal(total_days / total_cvssf).quantize(Decimal("0.001"))
    else:
        mean_vulnerabilities = Decimal(0).quantize(Decimal("0.1"))

    return mean_vulnerabilities


def get_mean_remediate_vulnerabilities(
    vulns: Iterable[Vulnerability],
    min_date: datetype | None = None,
) -> Decimal:
    """Get mean time to remediate a vulnerability, in days."""
    total_vuln = 0
    total_days = 0
    open_vuln_dates = [get_opening_date(vuln, min_date) for vuln in vulns]
    filtered_open_vuln_dates = [date for date in open_vuln_dates if date]
    closed_vuln_dates = [
        get_closing_date(vuln, min_date)
        for vuln, open_vuln in zip(vulns, open_vuln_dates, strict=False)
        if open_vuln
    ]
    for index, closed_vuln_date in enumerate(closed_vuln_dates):
        if closed_vuln_date:
            total_days += int((closed_vuln_date - filtered_open_vuln_dates[index]).days)
        else:
            current_day = datetime_utils.get_utc_now().date()
            total_days += int((current_day - filtered_open_vuln_dates[index]).days)
    total_vuln = len(filtered_open_vuln_dates)
    if total_vuln:
        mean_vulnerabilities = Decimal(round(total_days / float(total_vuln))).quantize(
            Decimal("0.1"),
        )
    else:
        mean_vulnerabilities = Decimal(0).quantize(Decimal("0.1"))

    return mean_vulnerabilities.to_integral_exact(rounding=ROUND_CEILING)


def get_ranges(numberlist: list[int]) -> str:
    """Transform list into ranges."""
    range_str = ",".join(
        as_range(g)
        for _, g in itertools.groupby(
            numberlist,
            key=lambda n, c=itertools.count(): n - next(c),  # type: ignore
        )
    )
    return range_str


def get_report_dates(
    vulns: Iterable[Vulnerability],
) -> list[datetime]:
    """
    Get report dates for vulnerabilities (created date
    if report date is missing).
    """
    return [
        vuln.unreliable_indicators.unreliable_report_date
        if vuln.unreliable_indicators.unreliable_report_date
        else vuln.created_date
        for vuln in vulns
    ]


def get_oldest_report_dates(
    vulns: Iterable[Vulnerability],
) -> list[datetime]:
    """
    Get report dates for vulnerabilities (state modified date
    if report date is missing).
    """
    return [
        vuln.unreliable_indicators.unreliable_report_date
        if vuln.unreliable_indicators.unreliable_report_date
        else vuln.state.modified_date
        for vuln in vulns
    ]


def group_specific(
    vulns: Iterable[Vulnerability],
    vuln_type: VulnerabilityType,
) -> list[Vulnerability]:
    """Group vulnerabilities by its specific field."""
    sorted_by_where = sort_vulnerabilities(vulns)
    grouped_vulns = []
    for key, group_iter in itertools.groupby(
        sorted_by_where,
        key=lambda vuln: (vuln.state.where, vuln.state.commit),
    ):
        group = list(group_iter)
        specific_grouped = (
            ",".join([vuln.state.specific for vuln in group])
            if vuln_type == VulnerabilityType.INPUTS
            else get_ranges(sorted([int(vuln.state.specific) for vuln in group]))
        )
        grouped_vulns.append(
            Vulnerability(
                created_by=group[0].created_by,
                created_date=group[0].created_date,
                finding_id=group[0].finding_id,
                group_name=group[0].group_name,
                hacker_email=group[0].hacker_email,
                id=group[0].id,
                organization_name=group[0].organization_name,
                root_id=group[0].root_id,
                state=group[0].state._replace(
                    commit=(
                        group[0].state.commit[0:7] if group[0].state.commit is not None else None
                    ),
                    specific=specific_grouped,
                    where=key[0],
                ),
                type=group[0].type,
            ),
        )

    return grouped_vulns


def sort_vulnerabilities(item: Iterable[Vulnerability]) -> list[Vulnerability]:
    """Sort a vulnerability by its where field."""
    return sorted(item, key=lambda vulnerability: vulnerability.state.where)


def range_to_list(range_value: str) -> list[str]:
    """Convert a range value into list."""
    limits = range_value.split("-")
    init_val = int(limits[0])
    end_val = int(limits[1]) + 1
    if end_val <= init_val:
        error_value = f'"values": "{init_val} >= {end_val}"'
        raise InvalidRange(expr=error_value)
    specific_values = list(map(str, list(range(init_val, end_val))))
    return specific_values


def ungroup_specific(specific: str) -> list[str]:
    """Ungroup specific value."""
    values = specific.split(",")
    specific_values = []
    for val in values:
        if is_range(val):
            range_list = range_to_list(val)
            specific_values.extend(range_list)
        else:
            specific_values.append(val)
    return specific_values


def get_treatment_from_org_finding_policy(
    *,
    modified_date: datetime,
    user_email: str,
) -> tuple[Treatment, ...]:
    treatments = adjust_historic_dates(
        (
            Treatment(
                acceptance_status=AcceptanceStatus.SUBMITTED,
                justification="From organization findings policy",
                assigned=user_email,
                modified_by=user_email,
                modified_date=modified_date,
                status=TreatmentStatus.ACCEPTED_UNDEFINED,
            ),
            Treatment(
                acceptance_status=AcceptanceStatus.APPROVED,
                justification="From organization findings policy",
                assigned=user_email,
                modified_by=user_email,
                modified_date=modified_date,
                status=TreatmentStatus.ACCEPTED_UNDEFINED,
            ),
        ),
    )
    return treatments


def _get_vuln_state_action(
    historic_state: Iterable[VulnerabilityState],
) -> list[Action]:
    actions: list[Action] = [
        Action(
            action=state.status.value,
            date=str(state.modified_date.date()),
            justification="",
            assigned="",
            times=1,
        )
        for state in filter_same_values(list(historic_state))
        if state.status in RELEASED_FILTER_STATUSES
    ]

    return list({action.date: action for action in actions}.values())


def get_state_actions(
    vulns_state: Iterable[Iterable[VulnerabilityState]],
) -> list[Action]:
    states_actions = list(
        itertools.chain.from_iterable(
            _get_vuln_state_action(historic_state) for historic_state in vulns_state
        ),
    )
    actions = [
        action._replace(times=times) for action, times in Counter(states_actions).most_common()
    ]

    return actions


def _get_vuln_treatment_actions(
    historic_treatment: Iterable[Treatment],
) -> list[Action]:
    actions = [
        Action(
            action=treatment.status.value,
            date=str(treatment.modified_date.date()),
            justification=treatment.justification,
            assigned=treatment.assigned,
            times=1,
        )
        for treatment in historic_treatment
        if (
            treatment.status
            in {
                TreatmentStatus.ACCEPTED,
                TreatmentStatus.ACCEPTED_UNDEFINED,
            }
            and treatment.acceptance_status
            not in {
                AcceptanceStatus.REJECTED,
                AcceptanceStatus.SUBMITTED,
            }
            and treatment.justification
            and treatment.assigned
        )
    ]
    return list({action.date: action for action in actions}.values())


def get_treatment_actions(
    vulns_treatment: Iterable[Iterable[Treatment]],
) -> list[Action]:
    treatments_actions = list(
        itertools.chain.from_iterable(
            _get_vuln_treatment_actions(historic_treatment)
            for historic_treatment in vulns_treatment
        ),
    )
    actions = [
        action._replace(times=times) for action, times in Counter(treatments_actions).most_common()
    ]

    return actions


def get_treatment_changes(historic_treatment: tuple[Treatment, ...]) -> int:
    if historic_treatment:
        first_treatment = historic_treatment[0]
        return (
            len(historic_treatment) - 1
            if first_treatment.status == TreatmentStatus.UNTREATED
            else len(historic_treatment)
        )
    return 0


def format_vulnerability_treatment_item(
    treatment: Treatment,
    should_convert: bool = False,
) -> Item:
    item = {
        "date": datetime_utils.get_as_str(treatment.modified_date),
        "treatment": get_inverted_treatment_converted(treatment.status.value)
        if should_convert
        else get_current_treatment_converted(treatment.status.value),
    }
    if treatment.accepted_until:
        item["acceptance_date"] = datetime_utils.get_as_str(treatment.accepted_until)
    if treatment.justification:
        item["justification"] = treatment.justification
    if treatment.modified_by:
        item["user"] = treatment.modified_by
    if treatment.acceptance_status:
        item["acceptance_status"] = treatment.acceptance_status.value
    if treatment.assigned:
        item["assigned"] = treatment.assigned
    return item


async def validate_vulnerability_in_toe_lines(
    loaders: Dataloaders,
    vulnerability: Vulnerability,
    where: str,
    index: int,
    raises: bool = True,
) -> ToeLine | None:
    if not vulnerability.root_id:
        return None
    git_root = await loaders.root.load(
        RootRequest(
            group_name=vulnerability.group_name,
            root_id=vulnerability.root_id,
        ),
    )
    if not git_root or not isinstance(git_root, GitRoot):
        return None

    if list(match_files(git_root.state.gitignore, [where])):
        raise VulnerabilityPathDoesNotExistInToeLines(index=f"{index}")

    toe_lines: ToeLine | None = await loaders.toe_lines.load(
        ToeLineRequest(
            filename=where,
            group_name=vulnerability.group_name,
            root_id=vulnerability.root_id,
        ),
    )
    if not toe_lines and raises:
        raise VulnerabilityPathDoesNotExistInToeLines(index=f"{index}")

    if not toe_lines:
        return None
    if not 0 <= int(vulnerability.state.specific) <= toe_lines.state.loc:
        if raises:
            raise LineDoesNotExistInTheLinesOfCodeRange(
                line=vulnerability.state.specific,
                index=f"{index}",
            )
        return None
    return toe_lines


async def toe_input_if_specific_is_unformatted(
    *,
    loaders: Dataloaders,
    vulnerability: Vulnerability,
    where: str,
    input_specific: str,
    toe_input: ToeInput | None = None,
) -> ToeInput | None:
    specific = html.unescape(vulnerability.state.specific)
    if (
        not toe_input
        and not is_machine_vuln(vulnerability)
        and vulnerability.root_id
        and specific != input_specific
    ):
        if second_try_toe_input := await loaders.toe_input.load(
            ToeInputRequest(
                component=where,
                entry_point=specific,
                group_name=vulnerability.group_name,
                root_id=vulnerability.root_id,
            ),
        ):
            return second_try_toe_input

        if (
            formatted_specific := re.sub(r"(\s+\[.*\])?", "", specific)
        ) and formatted_specific != specific:
            return await loaders.toe_input.load(
                ToeInputRequest(
                    component=where,
                    entry_point=formatted_specific,
                    group_name=vulnerability.group_name,
                    root_id=vulnerability.root_id,
                ),
            )

    return toe_input


async def validate_vulnerability_in_toe_inputs(
    *,
    loaders: Dataloaders,
    vulnerability: Vulnerability,
    where: str,
    index: int,
    raises: bool = True,
    be_present: bool = True,
) -> ToeInput | None:
    if not vulnerability.root_id:
        return None
    specific = html.unescape(vulnerability.state.specific)
    if match_specific := re.match(r"(?P<specific>.*)\s\(.*\)(\s\[.*\])?$", specific):
        specific = match_specific.groupdict()["specific"]

    toe_input: ToeInput | None = await loaders.toe_input.load(
        ToeInputRequest(
            component=where,
            entry_point="" if is_machine_vuln(vulnerability) else specific,
            group_name=vulnerability.group_name,
            root_id=vulnerability.root_id,
        ),
    )
    toe_input = await toe_input_if_specific_is_unformatted(
        loaders=loaders,
        vulnerability=vulnerability,
        where=where,
        input_specific=specific,
        toe_input=toe_input,
    )

    if not toe_input:
        return raise_or_return(
            exc=VulnerabilityUrlFieldDoNotExistInToeInputs(index=str(index)),
            raise_exc=raises,
            value_to_return=None,
        )

    if be_present and not toe_input.state.be_present:
        return raise_or_return(
            exc=VulnerabilityUrlFieldAreNotPresentInToeInputs(index=str(index)),
            raise_exc=raises,
            value_to_return=None,
        )
    return toe_input


async def validate_vulnerability_in_toe_ports(
    loaders: Dataloaders,
    vulnerability: Vulnerability,
    where: str,
    index: int,
    raises: bool = True,
) -> ToePort | None:
    if vulnerability.root_id:
        toe_port: ToePort | None = await loaders.toe_port.load(
            ToePortRequest(
                address=where,
                port=vulnerability.state.specific,
                group_name=vulnerability.group_name,
                root_id=vulnerability.root_id,
            ),
        )
        if not toe_port:
            if raises:
                raise VulnerabilityPortFieldDoNotExistInToePorts(index=f"{index}")
            return None
        return toe_port
    return None


async def validate_vulnerability_in_toe(
    loaders: Dataloaders,
    vulnerability: Vulnerability,
    index: int,
    raises: bool = True,
) -> Vulnerability | None:
    where = html.unescape(vulnerability.state.where)
    # There are cases, like SCA vulns, where the `where` attribute
    # has additional information `filename (package) [CVE]`
    where = ignore_advisories(where)
    toe_lines: ToeLine | None = None
    toe_input: ToeInput | None = None
    toe_port: ToePort | None = None

    if vulnerability.type == VulnerabilityType.LINES:
        toe_lines = await validate_vulnerability_in_toe_lines(
            loaders=loaders,
            vulnerability=vulnerability,
            where=where,
            index=index,
            raises=raises,
        )

    if vulnerability.type == VulnerabilityType.INPUTS:
        toe_input = await validate_vulnerability_in_toe_inputs(
            loaders=loaders,
            vulnerability=vulnerability,
            where=where,
            index=index,
            raises=raises,
        )

    if vulnerability.type == VulnerabilityType.PORTS:
        toe_port = await validate_vulnerability_in_toe_ports(
            loaders=loaders,
            vulnerability=vulnerability,
            where=where,
            index=index,
            raises=raises,
        )

    if any([toe_lines, toe_input, toe_port]):
        return vulnerability

    return None


def get_current_state_converted(state: str) -> str:
    if state in {"SAFE", "VULNERABLE"}:
        translation: dict[str, str] = {
            "SAFE": "CLOSED",
            "VULNERABLE": "OPEN",
        }

        return translation[state]
    return state


def get_inverted_state_converted(state: str) -> str:
    if state in {"CLOSED", "OPEN"}:
        translation: dict[str, str] = {
            "CLOSED": "SAFE",
            "OPEN": "VULNERABLE",
        }

        return translation[state]
    return state


def get_severity_score(vulnerability: Vulnerability, finding: Finding) -> SeverityScore:
    return vulnerability.severity_score if vulnerability.severity_score else finding.severity_score


def get_severity_cvss_vector(vulnerability: Vulnerability, finding: Finding) -> str:
    return (
        vulnerability.severity_score.cvss_v3
        if vulnerability.severity_score
        else finding.severity_score.cvss_v3
    )


def get_proposed_severity_cvss_vector(vulnerability: Vulnerability) -> str | None:
    return (
        vulnerability.state.proposed_severity.severity_score.cvss_v3
        if vulnerability.state.proposed_severity
        else None
    )


def get_severity_cvss4_vector(vulnerability: Vulnerability, finding: Finding) -> str:
    if vulnerability.severity_score is None:
        return finding.severity_score.cvss_v4

    return (
        vulnerability.severity_score.cvss_v4
        if vulnerability.severity_score.cvss_v4
        else finding.severity_score.cvss_v4
    )


def get_proposed_severity_cvss4_vector(vulnerability: Vulnerability) -> str | None:
    return (
        vulnerability.state.proposed_severity.severity_score.cvss_v4
        if vulnerability.state.proposed_severity
        else None
    )


def get_proposed_severity_author(vulnerability: Vulnerability) -> str | None:
    return (
        vulnerability.state.proposed_severity.modified_by
        if vulnerability.state.proposed_severity
        else None
    )


def get_severity_cvssf(vulnerability: Vulnerability, finding: Finding) -> Decimal:
    return (
        vulnerability.severity_score.cvssf
        if vulnerability.severity_score
        else finding.severity_score.cvssf
    )


def get_severity_cvssf_v4(vulnerability: Vulnerability, finding: Finding) -> Decimal:
    return (
        vulnerability.severity_score.cvssf_v4
        if vulnerability.severity_score
        else finding.severity_score.cvssf_v4
    )


def get_priority(vulnerability: Vulnerability) -> Decimal:
    return (
        vulnerability.unreliable_indicators.unreliable_priority
        if vulnerability.unreliable_indicators.unreliable_priority
        else Decimal(0)
    )


def get_severity_temporal_score(vulnerability: Vulnerability, finding: Finding) -> Decimal:
    return (
        vulnerability.severity_score.temporal_score
        if vulnerability.severity_score
        else finding.severity_score.temporal_score
    )


def get_proposed_severity_temporal_score(vulnerability: Vulnerability) -> Decimal | None:
    return (
        vulnerability.state.proposed_severity.severity_score.temporal_score
        if vulnerability.state.proposed_severity
        else None
    )


def get_severity_threat_score(vulnerability: Vulnerability, finding: Finding) -> Decimal:
    return (
        vulnerability.severity_score.threat_score
        if vulnerability.severity_score and vulnerability.severity_score.threat_score
        else finding.severity_score.threat_score
    )


def get_proposed_severity_threat_score(
    vulnerability: Vulnerability,
) -> Decimal | None:
    return (
        vulnerability.state.proposed_severity.severity_score.threat_score
        if vulnerability.state.proposed_severity
        else None
    )


def get_vulnerabilities_max_score_v3(
    finding: Finding,
    vulnerabilities: Iterable[Vulnerability],
    status: VulnerabilityStateStatus = VulnerabilityStateStatus.VULNERABLE,
) -> Decimal:
    return max(
        (
            get_severity_temporal_score(vulnerability, finding)
            for vulnerability in vulnerabilities
            if vulnerability.state.status == status
        ),
        default=finding.severity_score.temporal_score,
    )


def get_vulnerabilities_max_score(
    finding: Finding,
    vulnerabilities: Iterable[Vulnerability],
    status: VulnerabilityStateStatus = VulnerabilityStateStatus.VULNERABLE,
) -> Decimal:
    return max(
        (
            get_severity_threat_score(vulnerability, finding)
            for vulnerability in vulnerabilities
            if vulnerability.state.status == status
        ),
        default=finding.severity_score.threat_score,
    )


def _get_first_date(
    historic_state: Iterable[VulnerabilityState],
) -> datetime | None:
    return next(
        (
            state.modified_date
            for state in historic_state
            if state.status == VulnerabilityStateStatus.VULNERABLE
        ),
        None,
    )


def get_first_dates(
    historic_states: Iterable[Iterable[VulnerabilityState]],
) -> tuple[datetime, datetime]:
    valid_first_dates = filter(None, [_get_first_date(historic) for historic in historic_states])

    first_date = min(valid_first_dates) if valid_first_dates else datetime_utils.get_utc_now()
    day_month: int = int(first_date.strftime("%d"))
    first_day_delta = datetime_utils.get_minus_delta(first_date, days=day_month - 1)
    first_day = datetime.combine(first_day_delta, datetime.min.time()).astimezone(tz=UTC)
    last_day_delta = datetime_utils.get_plus_delta(
        first_day,
        days=monthrange(int(first_date.strftime("%Y")), int(first_date.strftime("%m")))[1] - 1,
    )
    last_day = datetime.combine(
        last_day_delta,
        datetime.max.time().replace(microsecond=0),
    ).astimezone(tz=UTC)

    return (first_day, last_day)


def get_date_last_vulns(vulns: Iterable[Vulnerability]) -> datetime:
    """Get date of the last vulnerabilities."""
    last_date = max(vuln.state.modified_date for vuln in vulns)
    day_week = last_date.weekday()
    first_day = datetime_utils.get_minus_delta(last_date, days=day_week)

    return first_day


def get_last_vulnerabilities_date(vulns: Iterable[Vulnerability]) -> datetime:
    last_date = max(vuln.state.modified_date for vuln in vulns)
    day_month: int = int(last_date.strftime("%d"))
    first_day_delta = datetime_utils.get_minus_delta(last_date, days=day_month - 1)
    first_day = datetime.combine(first_day_delta, datetime.min.time()).astimezone(tz=UTC)

    return first_day


def get_first_week_dates(min_date: datetime) -> tuple[datetime, datetime]:
    """Get first week vulnerabilities."""
    day_week = min_date.weekday()
    first_day_delta = datetime_utils.get_minus_delta(min_date, days=day_week)
    first_day = datetime.combine(first_day_delta, datetime.min.time()).astimezone(tz=UTC)
    last_day_delta = datetime_utils.get_plus_delta(first_day, days=6)
    last_day = datetime.combine(
        last_day_delta,
        datetime.max.time().replace(microsecond=0),
    ).astimezone(tz=UTC)

    return (first_day, last_day)


async def close_vulnerability(
    vulnerability: Vulnerability,
    modified_by: str,
    loaders: Dataloaders,
    closing_reason: VulnerabilityStateReason,
) -> None:
    if vulnerability.state.status != VulnerabilityStateStatus.VULNERABLE:
        return
    last_commit = vulnerability.state.commit
    if (
        (
            git_root := await loaders.root.load(
                RootRequest(vulnerability.group_name, vulnerability.root_id)
            )
        )
        and isinstance(git_root, GitRoot)
        and git_root.cloning.commit
    ):
        last_commit = git_root.cloning.commit

    await update_historic_entry(
        current_value=vulnerability,
        finding_id=vulnerability.finding_id,
        vulnerability_id=vulnerability.id,
        entry=vulnerability.state._replace(
            modified_by=modified_by,
            modified_date=datetime_utils.get_utc_now(),
            reasons=[closing_reason],
            status=VulnerabilityStateStatus.SAFE,
            commit=last_commit,
        ),
        force_update=True,
    )
    if is_reattack_requested(vulnerability) or is_reattack_on_hold(vulnerability):
        loaders.vulnerability.clear(vulnerability.id)
        _vulnerability = await loaders.vulnerability.load(vulnerability.id)
        if _vulnerability is None:
            return
        await update_historic_entry(
            current_value=_vulnerability,
            finding_id=_vulnerability.finding_id,
            vulnerability_id=_vulnerability.id,
            entry=VulnerabilityVerification(
                modified_by=modified_by,
                modified_date=datetime_utils.get_utc_now(),
                status=VulnerabilityVerificationStatus.VERIFIED,
                justification="",
            ),
        )
        # If the root was deactivated/moved, we need to remove
        # the on_hold status from vulns and remove them from
        # the corresponding Event
        if _vulnerability.event_id is not None:
            await update_event_index(
                finding_id=_vulnerability.finding_id,
                entry=VulnerabilityVerification(
                    modified_by=modified_by,
                    modified_date=datetime_utils.get_utc_now(),
                    status=VulnerabilityVerificationStatus.VERIFIED,
                    event_id=None,
                ),
                vulnerability_id=_vulnerability.id,
                delete_index=True,
            )


def _get_cvssf_score_default(vuln: Vulnerability, finding: Finding | None) -> Decimal:
    if finding:
        cvssf = get_severity_cvssf_v4(vuln, finding)
        return cvssf / Decimal(4.096)

    return Decimal(0)


def _get_severity_metrics(vuln: Vulnerability, finding: Finding | None) -> set:
    return set(
        get_severity_cvss4_vector(vuln, finding).split("/")
        if finding
        else CVSS_V4_DEFAULT.split("/"),
    )


def _calculate_initial_priority(root: Root | None, severity_score_default: Decimal) -> Decimal:
    criticality = (
        root.state.criticality
        if isinstance(root, GitRoot) and root.state.criticality is not None
        else "DEFAULT"
    )
    return PRIORITY_CRITICALITY_FACTOR[criticality] + severity_score_default


def _calculate_policy_priority(
    policies: list,
    vulnerability: Vulnerability,
    severity_metrics: set,
    cust_priority: int,
) -> Decimal:
    total_policy_priority = Decimal(cust_priority)
    for policy in policies:
        policy_value = Decimal(policy.value)
        policy_str = str(policy.policy).lower()

        if policy.policy == vulnerability.technique or any(
            metric in policy.policy for metric in severity_metrics
        ):
            total_policy_priority += policy_value

        if "reachable" in policy_str:
            has_advisories = bool(vulnerability.state.advisories)
            if ("true" in policy_str and has_advisories) or (
                "false" in policy_str and not has_advisories
            ):
                total_policy_priority += policy_value
    return total_policy_priority


async def get_priority_value(
    loaders: Dataloaders,
    vulnerability: Vulnerability,
) -> Decimal:
    if vulnerability.state.status != VulnerabilityStateStatus.VULNERABLE or (
        vulnerability.zero_risk and vulnerability.zero_risk.status in ZR_FILTER_STATUSES
    ):
        return Decimal(0)

    group = await get_group(loaders, vulnerability.group_name)
    finding = await loaders.finding.load(vulnerability.finding_id)
    organization = await loaders.organization.load(group.organization_id)
    root = await loaders.root.load(RootRequest(vulnerability.group_name, vulnerability.root_id))
    policies = (
        organization.priority_policies if organization and organization.priority_policies else []
    )
    custom_vuln_priority = vulnerability.custom_severity or 0
    severity_score_default = _get_cvssf_score_default(vulnerability, finding)
    severity_metrics = _get_severity_metrics(vulnerability, finding)

    total_priority = _calculate_initial_priority(root, severity_score_default)
    total_priority += _calculate_policy_priority(
        policies,
        vulnerability,
        severity_metrics,
        custom_vuln_priority,
    )

    return round(total_priority, 2)
