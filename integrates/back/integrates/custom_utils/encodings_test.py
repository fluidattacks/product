from integrates.custom_utils.encodings import (
    safe_decode,
)
from integrates.testing.utils import parametrize


@parametrize(
    args=["hexstr", "expect_result"],
    cases=[
        ["74657374", "test"],
        ["68656c6c6f20776f726c64", "hello world"],
    ],
)
async def test_safe_decode(hexstr: str, expect_result: str) -> None:
    decoded = safe_decode(hexstr)
    assert decoded == expect_result
