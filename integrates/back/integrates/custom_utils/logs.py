import logging
import logging.config

from aioextensions import (
    in_thread,
    schedule,
)
from starlette.requests import (
    Request,
)
from starlette.websockets import (
    WebSocket,
)

from integrates.context import (
    FI_ENVIRONMENT,
)
from integrates.custom_exceptions import (
    ExpiredToken,
    InvalidAuthorization,
)
from integrates.sessions import (
    domain as sessions_domain,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)

# Constants
TRANSACTIONS_LOGGER: logging.Logger = logging.getLogger("transactional")


def cloudwatch_log(request: Request | WebSocket, msg: str, extra: dict) -> None:
    schedule(cloudwatch_log_async(request, msg, extra))


async def cloudwatch_log_async(request: Request | WebSocket, msg: str, extra: dict) -> None:
    try:
        user_data = await sessions_domain.get_jwt_content(request)
    except (ExpiredToken, InvalidAuthorization):
        user_data = {"user_email": "unauthenticated"}

    await in_thread(
        TRANSACTIONS_LOGGER.info,
        msg,
        extra={
            "environment": FI_ENVIRONMENT,
            "user_email": user_data["user_email"],
            **extra,
        },
    )
