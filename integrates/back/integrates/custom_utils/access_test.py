from integrates.custom_utils.access import format_invitation_state
from integrates.db_model.organization_access.enums import OrganizationInvitiationState
from integrates.db_model.organization_access.types import OrganizationInvitation
from integrates.testing.utils import parametrize


@parametrize(
    args=["invitation", "is_registered", "expect_result"],
    cases=[
        [
            OrganizationInvitation(is_used=False, role="group_manager", url_token=""),
            False,
            OrganizationInvitiationState.PENDING,
        ],
        [
            OrganizationInvitation(is_used=True, role="user", url_token=""),
            False,
            OrganizationInvitiationState.UNREGISTERED,
        ],
        [
            OrganizationInvitation(is_used=True, role="other", url_token=""),
            True,
            OrganizationInvitiationState.REGISTERED,
        ],
    ],
)
def test_format_invitation_state(
    invitation: OrganizationInvitation,
    is_registered: bool,
    expect_result: OrganizationInvitiationState,
) -> None:
    result = format_invitation_state(invitation, is_registered)

    assert result == expect_result
