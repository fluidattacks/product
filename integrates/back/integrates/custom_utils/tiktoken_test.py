import tiktoken


def test_tiktoken_cache() -> None:
    assert tiktoken.get_encoding("cl100k_base") is not None
    assert tiktoken.get_encoding("o200k_base") is not None

    test_string = """Only three people have ever really understood the
    Schleswig-Holstein business: the Prince Consort, who is dead, a
    German professor, who has gone mad, and I, who have forgotten all
    about it!"""

    gpt_models = (
        "gpt-3.5-turbo-16k-0613",
        "gpt-3.5-turbo-0301",
        "gpt-3.5-turbo-0613",
        "gpt-4-32k-0314",
        "gpt-4-32k-0613",
        "gpt-4-0314",
        "gpt-4-0613",
        "gpt-4o-2024-05-13",
    )
    for model in gpt_models:
        encoding = tiktoken.encoding_for_model(model)
        num_tokens = len(encoding.encode(test_string))
        assert num_tokens > 40
