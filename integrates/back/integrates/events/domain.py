import logging
import logging.config
import re
import uuid
from datetime import datetime
from time import time

import pytz
from aioextensions import collect, schedule
from starlette.datastructures import UploadFile

import integrates.vulnerabilities.domain as vulns_domain
from integrates import authz
from integrates.context import FI_MANUAL_CLONING_PROJECTS
from integrates.custom_exceptions import (
    EventAlreadyClosed,
    EventHasNotBeenSolved,
    EventVerificationAlreadyRequested,
    EventVerificationNotRequested,
    InvalidCommentParent,
    InvalidDate,
    InvalidEventSolvingReason,
    InvalidFileSize,
    InvalidFileType,
    InvalidParameter,
    RequiredFieldToBeUpdate,
)
from integrates.custom_utils import datetime as datetime_utils
from integrates.custom_utils import files as files_utils
from integrates.custom_utils import roots as roots_utils
from integrates.custom_utils import validations_deco
from integrates.custom_utils.constants import SCHEDULE_CLONE_GROUPS_ROOTS_EMAIL
from integrates.custom_utils.events import get_event
from integrates.custom_utils.findings import validate_evidence_name
from integrates.custom_utils.groups import get_group
from integrates.custom_utils.reports import get_extension
from integrates.dataloaders import Dataloaders, get_new_context
from integrates.db_model import events as events_model
from integrates.db_model.event_comments.types import EventComment, EventCommentsRequest
from integrates.db_model.events.enums import (
    EventEvidenceId,
    EventSolutionReason,
    EventStateStatus,
    EventType,
)
from integrates.db_model.events.types import (
    Event,
    EventEvidence,
    EventEvidences,
    EventMetadataToUpdate,
    EventRequest,
    EventState,
    GroupEventsRequest,
)
from integrates.db_model.roots.enums import RootStatus
from integrates.db_model.roots.types import GitRoot
from integrates.event_comments import domain as event_comments_domain
from integrates.events.constants import (
    FILE_EVIDENCE_IDS,
    IMAGE_EVIDENCE_IDS,
    SOLUTION_REASON_BY_EVENT_TYPE,
)
from integrates.events.types import EventAttributesToUpdate
from integrates.mailer import events as events_mail
from integrates.s3 import operations as s3_ops
from integrates.settings import LOGGING, TIME_ZONE

from . import validations as events_validations

logging.config.dictConfig(LOGGING)
LOGGER = logging.getLogger(__name__)

BRANCH_NOT_FOUND_ERROR_MESSAGES = (
    "Could not find remote branch",
    "not found in upstream origin",
)
CONNECTIVITY_ERROR_MESSAGES = (
    "Could not resolve host",
    "Couldn't connect to server",
    "Network is unreachable",
)
INVALID_CREDENTIALS_ERROR_MESSAGES = (
    "Authentication failed for",
    "fatal: could not read Password for",
    "Invalid credentials",
    "make sure you have the correct access rights",
    "Permission denied",
    "terminal prompts disabled",
    "The requested URL returned error: 403",
    "you don't have permission to view it",
)
REGEX_REPOSITORY_NOT_FOUND = r"fatal: repository '.*' not found"
REPOSITORY_NOT_FOUND_OR_INVALID_CREDENTIALS = (
    "and the repository exists",
    "does not exist or you do not have permissions",
    "The following project does not exist",
    "You may not have access to this repository or it no longer exists in this workspace",
)
INVALID_CREDENTIALS_MSG = "Invalid credentials"
MAX_CLONING_FAILURES_ALLOWED = 3
MAX_WEEKS_CREATED_DATE = 104


async def save_evidence(file_object: object, file_name: str) -> None:
    await s3_ops.upload_memory_file(
        file_object,
        f"evidences/{file_name}",
    )


async def search_evidence(file_name: str) -> list[str]:
    return await s3_ops.list_files(f"evidences/{file_name}")


async def remove_file_evidence(file_name: str) -> None:
    await s3_ops.remove_file(file_name)


@authz.validate_handle_comment_scope_deco(
    "loaders",
    "_content",
    "_email",
    "group_name",
    "parent_comment",
)
async def _check_invalid_comment(
    *,
    loaders: Dataloaders,
    _content: str,
    _email: str,
    group_name: str,
    parent_comment: str,
    event_id: str,
) -> None:
    if parent_comment != "0":
        event_comments = await loaders.event_comments.load(
            EventCommentsRequest(event_id=event_id, group_name=group_name),
        )
        event_comments_ids = [comment.id for comment in event_comments]

        if parent_comment not in event_comments_ids:
            raise InvalidCommentParent()


@validations_deco.validate_length_deco("comment_data.content", max_length=20000)
@validations_deco.validate_fields_deco(["comment_data.content"])
async def add_comment(
    *,
    loaders: Dataloaders,
    comment_data: EventComment,
    email: str,
    event_id: str,
    group_name: str,
    parent_comment: str,
) -> None:
    parent_comment = str(parent_comment)
    content = comment_data.content
    event = await get_event(loaders, EventRequest(event_id=event_id, group_name=group_name))
    group_name = event.group_name

    await _check_invalid_comment(
        loaders=loaders,
        _content=content,
        _email=email,
        group_name=group_name,
        parent_comment=parent_comment,
        event_id=event_id,
    )

    await event_comments_domain.add(comment_data)


async def _reopen(
    *,
    event: Event,
    hacker_email: str,
    detail: str,
) -> None:
    loaders = get_new_context()
    comment_id: str = str(round(time() * 1000))
    await add_comment(
        loaders=loaders,
        comment_data=EventComment(
            event_id=event.id,
            group_name=event.group_name,
            parent_id="0",
            id=comment_id,
            content=f"Reopen - {detail}",
            creation_date=datetime_utils.get_utc_now(),
            email=hacker_email,
            full_name=hacker_email,
        ),
        email=hacker_email,
        event_id=event.id,
        group_name=event.group_name,
        parent_comment="0",
    )
    await events_model.update_state(
        current_value=event,
        group_name=event.group_name,
        state=EventState(
            modified_by=hacker_email,
            modified_date=datetime_utils.get_utc_now(),
            comment_id=comment_id,
            status=EventStateStatus.CREATED,
        ),
    )

    schedule(
        events_mail.send_mail_event_report(loaders=loaders, event=event, subject="persistent"),
    )


async def _verify_if_already_reported_and_unsolved(
    *,
    group_name: str,
    root_id: str | None,
    environment_url: str | None,
    event_type: EventType,
    hacker_email: str,
    detail: str,
) -> str | None:
    loaders = get_new_context()
    unsolved_events = await loaders.group_events.load(
        GroupEventsRequest(group_name=group_name, is_solved=False)
    )

    if event_type == EventType.CLONING_ISSUES:
        cloning_issue_event = next(
            (
                event
                for event in unsolved_events
                if event.root_id == root_id and event.type == event_type
            ),
            None,
        )

        if cloning_issue_event:
            return cloning_issue_event.id

    event = next(
        (
            event
            for event in unsolved_events
            if event.root_id == root_id
            and event.type == event_type
            and event.environment_url == environment_url
            and event.description == detail
        ),
        None,
    )

    if not event:
        return None

    if event.state.status == EventStateStatus.VERIFICATION_REQUESTED:
        await _reopen(
            event=event,
            hacker_email=hacker_email,
            detail=detail,
        )

    return event.id


@validations_deco.validate_length_deco("detail", max_length=1000)
@validations_deco.validate_fields_deco(["detail", "root_id"])
async def add_event(
    *,
    loaders: Dataloaders,
    hacker_email: str,
    group_name: str,
    detail: str,
    event_type: EventType,
    root_id: str | None,
    event_date: datetime,
    environment_url: str | None,
) -> str:
    group = await get_group(loaders, group_name)
    if root_id:
        root = await roots_utils.get_root(loaders, root_id, group_name)
        root_id = root.id
        if root.state.status != RootStatus.ACTIVE:
            raise InvalidParameter(field="rootId")

    event_id = await _verify_if_already_reported_and_unsolved(
        group_name=group_name,
        root_id=root_id,
        hacker_email=hacker_email,
        environment_url=environment_url,
        detail=detail,
        event_type=event_type,
    )

    if event_id:
        return event_id

    tzn = pytz.timezone(TIME_ZONE)
    event_date = event_date.astimezone(tzn)
    if event_date > datetime_utils.get_now():
        raise InvalidDate()
    if datetime_utils.get_now_minus_delta(weeks=MAX_WEEKS_CREATED_DATE) >= event_date:
        raise InvalidDate()

    created_date = datetime_utils.get_utc_now()
    event = Event(
        client=group.organization_id,
        created_by=hacker_email,
        created_date=created_date,
        description=detail,
        environment_url=environment_url,
        event_date=event_date,
        evidences=EventEvidences(),
        group_name=group_name,
        hacker=hacker_email,
        id=str(uuid.uuid4()),
        root_id=root_id,
        state=EventState(
            modified_by=hacker_email,
            modified_date=event_date,
            status=EventStateStatus.OPEN,
        ),
        type=event_type,
    )
    await events_model.add(event=event)
    await events_model.update_state(
        current_value=event,
        group_name=group_name,
        state=EventState(
            modified_by=hacker_email,
            modified_date=created_date,
            status=EventStateStatus.CREATED,
        ),
    )

    schedule(
        events_mail.send_mail_event_report(
            loaders=loaders,
            event=event,
        ),
    )

    return event.id


async def get_unsolved_events(group_name: str) -> list[Event]:
    loaders = get_new_context()
    return await loaders.group_events.load(
        GroupEventsRequest(group_name=group_name, is_solved=False)
    )


async def get_evidence_link(
    loaders: Dataloaders,
    event_id: str,
    file_name: str,
    group_name: str,
) -> str:
    await get_event(loaders, EventRequest(event_id=event_id, group_name=group_name))
    file_url = f"evidences/{group_name}/{event_id}/{file_name}"
    return await s3_ops.sign_url(file_url, 900)


async def has_access_to_event(
    loaders: Dataloaders,
    email: str,
    event_id: str,
    group_name: str,
) -> bool:
    """Verify if the user has access to a event submission."""
    await get_event(loaders, EventRequest(event_id=event_id, group_name=group_name))
    return await authz.has_access_to_group(loaders, email, group_name)


async def remove_event(event_id: str, group_name: str) -> None:
    evidence_prefix = f"{group_name}/{event_id}"
    list_evidences = await search_evidence(evidence_prefix)
    await collect(
        [
            *[remove_file_evidence(file_name) for file_name in list_evidences],
            event_comments_domain.remove_comments(event_id, group_name),
        ],
    )
    await events_model.remove(event_id=event_id)
    LOGGER.info(
        "Event removed",
        extra={
            "extra": {
                "event_id": event_id,
                "group_name": group_name,
            },
        },
    )


async def remove_evidence(
    loaders: Dataloaders,
    evidence_id: EventEvidenceId,
    event_id: str,
    group_name: str,
) -> None:
    event = await get_event(loaders, EventRequest(event_id=event_id, group_name=group_name))
    group_name = event.group_name

    if (evidence := getattr(event.evidences, str(evidence_id.value).lower(), None)) and isinstance(
        evidence,
        EventEvidence,
    ):
        full_name = f"evidences/{group_name}/{event_id}/{evidence.file_name}"
        await s3_ops.remove_file(full_name)
        await events_model.update_evidence(
            event_id=event_id,
            group_name=group_name,
            evidence_info=None,
            evidence_id=evidence_id,
        )


@validations_deco.validate_fields_deco(["comments"])
async def reject_solution(
    *,
    loaders: Dataloaders,
    event_id: str,
    comments: str,
    group_name: str,
    stakeholder_email: str,
    stakeholder_full_name: str,
) -> None:
    event = await get_event(loaders, EventRequest(event_id=event_id, group_name=group_name))
    if event.state.status is not EventStateStatus.VERIFICATION_REQUESTED:
        raise EventVerificationNotRequested()

    comment_id: str = str(round(time() * 1000))
    parent_comment_id = event.state.comment_id if event.state.comment_id else "0"
    await add_comment(
        loaders=loaders,
        comment_data=EventComment(
            event_id=event.id,
            group_name=group_name,
            parent_id=parent_comment_id,
            id=comment_id,
            content=comments,
            creation_date=datetime_utils.get_utc_now(),
            email=stakeholder_email,
            full_name=stakeholder_full_name,
        ),
        email=stakeholder_email,
        event_id=event.id,
        group_name=group_name,
        parent_comment=parent_comment_id,
    )
    await events_model.update_state(
        current_value=event,
        group_name=event.group_name,
        state=EventState(
            modified_by=stakeholder_email,
            modified_date=datetime_utils.get_utc_now(),
            comment_id=comment_id,
            status=EventStateStatus.CREATED,
        ),
    )


@validations_deco.validate_fields_deco(["comments"])
@validations_deco.validate_length_deco("comments", max_length=1000)
async def request_verification(
    *,
    loaders: Dataloaders,
    event_id: str,
    comments: str,
    group_name: str,
    stakeholder_email: str,
    stakeholder_full_name: str,
) -> None:
    event = await get_event(loaders, EventRequest(event_id=event_id, group_name=group_name))
    if event.state.status == EventStateStatus.SOLVED:
        raise EventAlreadyClosed()
    if event.state.status == EventStateStatus.VERIFICATION_REQUESTED:
        raise EventVerificationAlreadyRequested()

    comment_id: str = str(round(time() * 1000))
    await add_comment(
        loaders=loaders,
        comment_data=EventComment(
            event_id=event.id,
            group_name=group_name,
            parent_id="0",
            id=comment_id,
            content=comments,
            creation_date=datetime_utils.get_utc_now(),
            email=stakeholder_email,
            full_name=stakeholder_full_name,
        ),
        email=stakeholder_email,
        event_id=event.id,
        group_name=group_name,
        parent_comment="0",
    )
    await events_model.update_state(
        current_value=event,
        group_name=event.group_name,
        state=EventState(
            modified_by=stakeholder_email,
            modified_date=datetime_utils.get_utc_now(),
            comment_id=comment_id,
            status=EventStateStatus.VERIFICATION_REQUESTED,
        ),
    )


async def update_event(
    loaders: Dataloaders,
    event_id: str,
    group_name: str,
    stakeholder_email: str,
    attributes: EventAttributesToUpdate,
) -> None:
    event = await get_event(loaders, EventRequest(event_id=event_id, group_name=group_name))
    solving_reason = attributes.solving_reason or event.state.reason
    other_solving_reason = (
        attributes.other_solving_reason or event.state.other
        if solving_reason == EventSolutionReason.OTHER
        else None
    )
    event_type = attributes.event_type or event.type
    if all(attribute is None for attribute in attributes):
        raise RequiredFieldToBeUpdate()

    if attributes.event_type:
        events_validations.validate_type(attributes.event_type)

    if solving_reason == EventSolutionReason.OTHER and not other_solving_reason:
        raise InvalidParameter("otherSolvingReason")

    if solving_reason is not None and event.state.status != EventStateStatus.SOLVED:
        raise EventHasNotBeenSolved()

    if (
        event.state.status == EventStateStatus.SOLVED
        and solving_reason not in SOLUTION_REASON_BY_EVENT_TYPE[event_type]
    ):
        raise InvalidEventSolvingReason()

    if attributes.event_type:
        await events_model.update_metadata(
            event_id=event.id,
            group_name=event.group_name,
            metadata=EventMetadataToUpdate(
                type=attributes.event_type,
                description=attributes.event_description,
            ),
        )

    if (
        attributes.solving_reason != event.state.reason
        or attributes.other_solving_reason != event.state.other
    ):
        await events_model.update_state(
            current_value=event,
            group_name=event.group_name,
            state=EventState(
                modified_by=stakeholder_email,
                modified_date=datetime_utils.get_utc_now(),
                other=other_solving_reason,
                reason=solving_reason,
                status=event.state.status,
            ),
        )


async def replace_different_format(
    *,
    event: Event,
    evidence_id: EventEvidenceId,
    extension: str,
) -> None:
    evidence: EventEvidence | None = getattr(event.evidences, str(evidence_id.value).lower())
    if evidence:
        old_full_name = f"evidences/{event.group_name}/{event.id}/{evidence.file_name}"
        ends: str = old_full_name.rsplit(".", 1)[-1]
        if ends != old_full_name and f".{ends}" != extension:
            await remove_file_evidence(old_full_name)


def _get_file_name(group_name: str, event_id: str, value: str, extension: str) -> str:
    return f"{group_name}_{event_id}_evidence_{value}{extension}"


@validations_deco.validate_sanitized_csv_input_deco(["group_name", "event_id", "file_name"])
def _get_full_name(group_name: str, event_id: str, file_name: str) -> str:
    return f"{group_name}/{event_id}/{file_name}"


@validations_deco.validate_sanitized_csv_input_deco(
    ["event_id", "file.filename", "file.content_type"],
)
async def update_evidence(
    *,
    loaders: Dataloaders,
    event_id: str,
    evidence_id: EventEvidenceId,
    file: UploadFile,
    group_name: str,
    update_date: datetime,
) -> None:
    event = await get_event(loaders, EventRequest(event_id=event_id, group_name=group_name))
    if event.state.status == EventStateStatus.SOLVED:
        raise EventAlreadyClosed()

    mime_type = await files_utils.get_uploaded_file_mime(file)
    extension = get_extension(mime_type)
    group_name = event.group_name
    file_name = _get_file_name(
        group_name=group_name,
        event_id=event_id,
        value=str(evidence_id.value).lower(),
        extension=extension,
    )
    full_name = _get_full_name(group_name=group_name, event_id=event_id, file_name=file_name)

    await collect(
        (
            save_evidence(file, full_name),
            events_model.update_evidence(
                event_id=event_id,
                group_name=group_name,
                evidence_info=EventEvidence(
                    file_name=file_name,
                    modified_date=update_date,
                ),
                evidence_id=evidence_id,
            ),
        ),
    )

    await replace_different_format(event=event, evidence_id=evidence_id, extension=extension)


@validations_deco.validate_file_name_deco("file.filename")
@validations_deco.validate_fields_deco(["file.content_type"])
async def validate_evidence(
    *,
    group_name: str,
    organization_name: str,
    evidence_id: EventEvidenceId,
    file: UploadFile,
) -> None:
    mib = 1048576

    if evidence_id in IMAGE_EVIDENCE_IDS:
        allowed_mimes = ["image/jpeg", "image/png", "video/webm"]
        if not await files_utils.assert_uploaded_file_mime(file, allowed_mimes):
            raise InvalidFileType("EVENT_IMAGE")
    elif evidence_id in FILE_EVIDENCE_IDS:
        allowed_mimes = [
            "application/csv",
            "application/pdf",
            "application/zip",
            "text/csv",
            "text/plain",
        ]
        if not await files_utils.assert_uploaded_file_mime(file, allowed_mimes):
            raise InvalidFileType("EVENT_FILE")
    else:
        raise InvalidFileType("EVENT")

    if await files_utils.get_file_size(file) > 10 * mib:
        raise InvalidFileSize()

    filename = file.filename.lower() if file.filename is not None else ""

    validate_evidence_name(
        organization_name=organization_name.lower(),
        group_name=group_name.lower(),
        filename=filename,
    )


async def solve_event(
    *,
    loaders: Dataloaders,
    event_id: str,
    group_name: str,
    hacker_email: str,
    reason: EventSolutionReason,
    other: str | None,
) -> None:
    event = await get_event(loaders, EventRequest(event_id=event_id, group_name=group_name))
    other_reason = other or ""

    if event.state.status == EventStateStatus.SOLVED:
        raise EventAlreadyClosed()

    if (
        reason not in SOLUTION_REASON_BY_EVENT_TYPE[event.type]
        and reason != EventSolutionReason.MOVED_TO_ANOTHER_GROUP
    ):
        raise InvalidEventSolvingReason()

    await events_model.update_state(
        current_value=event,
        group_name=group_name,
        state=EventState(
            modified_by=hacker_email,
            modified_date=datetime_utils.get_utc_now(),
            other=other_reason,
            reason=reason,
            status=EventStateStatus.SOLVED,
        ),
    )
    schedule(
        events_mail.send_mail_event_report(
            loaders=loaders,
            event=event,
            justification=other or reason,
            subject="solved",
        ),
    )


async def add_cloning_issues_event(
    *,
    loaders: Dataloaders,
    git_root: GitRoot,
    message: str,
    branch: str,
) -> None:
    if (
        git_root.group_name in FI_MANUAL_CLONING_PROJECTS.split(",")
        or git_root.cloning.failed_count < MAX_CLONING_FAILURES_ALLOWED
    ):
        return

    log_error = None
    if any(err in message for err in BRANCH_NOT_FOUND_ERROR_MESSAGES):
        log_error = f"Could not find remote branch {branch} to clone"
    elif any(err in message for err in CONNECTIVITY_ERROR_MESSAGES):
        log_error = "Could not find the repository or resolve the host"
    elif any(err in message for err in REPOSITORY_NOT_FOUND_OR_INVALID_CREDENTIALS):
        log_error = "Repository does not exist or you do not have permissions"
    elif any(err in message for err in INVALID_CREDENTIALS_ERROR_MESSAGES):
        log_error = INVALID_CREDENTIALS_MSG
    elif re.search(REGEX_REPOSITORY_NOT_FOUND, message):
        log_error = "Repository not found"
    else:
        LOGGER.error(
            "Automatic event creation on failed cloning not covered",
            extra={
                "extra": {
                    "group": git_root.group_name,
                    "root_id": git_root.id,
                    "root_nickname": git_root.state.nickname,
                    "reason": roots_utils.sanitize_cloning_error_message(message),
                },
            },
        )

        return

    event_type = EventType.CLONING_ISSUES
    new_event_id = await add_event(
        loaders=loaders,
        hacker_email=SCHEDULE_CLONE_GROUPS_ROOTS_EMAIL,
        group_name=git_root.group_name,
        detail=log_error,
        event_date=datetime.now(),
        event_type=event_type,
        root_id=git_root.id,
        environment_url=None,
    )
    await vulns_domain.handle_vulnerabilities_on_hold(
        loaders=loaders, root_id=git_root.id, event_id=new_event_id, event_type=event_type
    )
