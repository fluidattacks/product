from typing import (
    NamedTuple,
)

from integrates.db_model.events.enums import (
    EventSolutionReason,
    EventType,
)


class EventAttributesToUpdate(NamedTuple):
    event_description: str | None = None
    event_type: EventType | None = None
    other_solving_reason: str | None = None
    solving_reason: EventSolutionReason | None = None
