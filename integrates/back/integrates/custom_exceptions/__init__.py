from __future__ import (
    annotations,
)

from collections.abc import (
    Sequence,
)
from decimal import (
    Decimal,
)
from typing import (
    Literal,
)


class CustomBaseException(Exception):
    pass


class _SingleMessageException(CustomBaseException):
    msg: str

    @classmethod
    def new(cls) -> _SingleMessageException:
        return cls(cls.msg)


class ErrorFileNameAlreadyExists(_SingleMessageException):
    msg: str = "File name already exists in group files"


class ExecutionAlreadyCreated(_SingleMessageException):
    msg: str = "This execution already exists"


class JiraInstallAlreadyCreated(_SingleMessageException):
    msg: str = "This install already exists"


class ErrorDownloadingFile(_SingleMessageException):
    msg: str = "Unable to download the requested file"


class ErrorLoadingOrganizations(_SingleMessageException):
    msg: str = "Unable to read organizations data"


class ErrorLoadingStakeholders(_SingleMessageException):
    msg: str = "Unable to read stakeholders data"


class ErrorSubmittingJob(_SingleMessageException):
    msg: str = "Unable to queue machine execution or job"


class ErrorUpdatingCredential(_SingleMessageException):
    msg: str = "Unable to update credential"


class ErrorUpdatingGroup(_SingleMessageException):
    msg: str = "Unable to update group"


class ErrorAddingGroupTags(_SingleMessageException):
    msg: str = "Incorrect tag syntax e.g. tag1, test-tag1, my-complex-tag"


class ErrorUploadingFileS3(_SingleMessageException):
    msg: str = "Unable to upload file to S3 service"


class ExpectedVulnToBeOfLinesType(_SingleMessageException):
    msg: str = "Expected vulnerability to be of type: lines"


class EventAlreadyCreated(_SingleMessageException):
    msg: str = "This event has already been created"


class GroupAlreadyCreated(_SingleMessageException):
    msg: str = "Exception - This group has already been created"


class AutomaticFixError(CustomBaseException):
    """Exception to monitor errors in autofixes"""

    def __init__(self, kind: Literal["custom", "suggested"], vuln_id: str) -> None:
        """Constructor"""
        msg = f"Exception - Failed to generate a {kind} fix for vulnerability {vuln_id}"
        super().__init__(msg)


class IndicatorAlreadyUpdated(CustomBaseException):
    """Exception to control the indicator has not been updated"""

    def __init__(self) -> None:
        """Constructor"""
        msg = "Exception - The indicator has been updated by another operation"
        super().__init__(msg)


class InvalidSeverityCweIds(_SingleMessageException):
    msg = "Exception - Error invalid CWE ids given for vulnerability"


class InvalidCVSS3VectorString(_SingleMessageException):
    msg = "Exception - Error invalid severity CVSS v3.1 vector string"


class InvalidCVSS4VectorString(_SingleMessageException):
    msg = "Exception - Error invalid severity CVSS v4 vector string"


class InvalidCVSSVectorString(CustomBaseException):
    def __init__(self, cvss_version: str = "v4.0") -> None:
        msg = f"Exception - Error missing severity CVSS {cvss_version} vector string"
        super().__init__(msg)


class InvalidGroupName(_SingleMessageException):
    msg = "Exception - Error invalid group name"


class InvalidInactivityPeriod(_SingleMessageException):
    msg = "Exception - Inactivity period should be greater than the provided value"


class InvalidRemovalVulnState(_SingleMessageException):
    msg: str = "Invalid, you cannot remove a closed vulnerability"


class InvalidRemovalVulnReleased(_SingleMessageException):
    msg: str = "Invalid, you cannot remove a vulnerability that has already been released"


class InvalidRemovalFindingState(_SingleMessageException):
    msg: str = "Invalid, you cannot delete a finding that contains released vulnerabilities"


class InvalidSortsParameters(_SingleMessageException):
    msg: str = "Invalid, missing parameters in mutation"


class PendingEvidenceDraft(_SingleMessageException):
    msg = "Invalid, still evidence to approve"


class MissingApprovedEvidence(_SingleMessageException):
    msg = "Invalid, missing approved evidence"


class InvalidSortsSuggestions(_SingleMessageException):
    msg: str = "Invalid, incorrect parameters in ToE Lines Sorts suggestions"


class InvalidSortsRiskLevel(_SingleMessageException):
    msg: str = "Invalid, value not in range [0, 100]"


class InvalidSortsRiskLevelDate(_SingleMessageException):
    msg: str = "Invalid, date can not be a future date."


class InvalidVulnerabilityAlreadyExists(_SingleMessageException):
    msg: str = "Invalid, vulnerability already exists"


class InvalidVulnCommitHash(_SingleMessageException):
    msg: str = "Commit Hash should be a 40 chars long hexadecimal"


class InvalidVulnSpecific(_SingleMessageException):
    msg: str = "Vulnerability Specific must be integer"


class InvalidVulnWhere(_SingleMessageException):
    msg: str = "Vulnerability where should match: ^(?!=)+[^/]+/.+$"


class OrganizationAlreadyCreated(_SingleMessageException):
    msg: str = "This organization has already been created"


class OrgFindingPolicyNotFound(_SingleMessageException):
    msg: str = "Organization finding policy not found"


class PortfolioNotFound(_SingleMessageException):
    msg: str = "Portfolio not found"


class SnapshotNotFound(_SingleMessageException):
    msg: str = "Snapshot not found in analytics bucket"


class UnableToSkimsQueue(_SingleMessageException):
    msg: str = "Unable to queue a verification request"


class MailerClientError(_SingleMessageException):
    msg: str = "Mailer client has delivered an error"


class UnableToSendMail(_SingleMessageException):
    msg: str = "Unable to send mail message"


class UnavailabilityError(_SingleMessageException):
    msg: str = "AWS service unavailable, please retry"


class VulnAlreadyCreated(_SingleMessageException):
    msg: str = "This vulnerability has already been created"


class EmptyFile(_SingleMessageException):
    msg: str = "Exception - The file can not be empty"


class AcceptanceNotRequested(CustomBaseException):
    """Exception to control if acceptance is not valid"""

    def __init__(self) -> None:
        """Constructor"""
        msg = "Exception - It cant handle acceptance without being requested"
        super().__init__(msg)


class AlreadyCreated(CustomBaseException):
    """Exception to control draft-only operations"""

    def __init__(self) -> None:
        """Constructor"""
        msg = "Exception - This draft has already been created"
        super().__init__(msg)


class AlreadyRequested(CustomBaseException):
    """Exception to control verifications already requested"""

    def __init__(self) -> None:
        """Constructor"""
        msg = "Exception - Request verification already requested"
        super().__init__(msg)


class AlreadyOnHold(CustomBaseException):
    """Exception to control requested verifications already put on hold"""

    def __init__(self) -> None:
        """Constructor"""
        msg = "Exception - Request verification already on hold"
        super().__init__(msg)


class AuthzApiError(CustomBaseException):
    """Exception to control authz api errors"""

    def __init__(self) -> None:
        """Constructor"""
        msg = "Exception - Error talking to authz API"
        super().__init__(msg)


class InvalidAffectedReattacks(CustomBaseException):
    def __init__(self) -> None:
        """Constructor"""
        msg = "Invalid, type vulnerability LINES cannot be associated"
        super().__init__(msg)


class AlreadyZeroRiskRequested(CustomBaseException):
    """Exception to control zero risk already is already requested"""

    def __init__(self) -> None:
        """Constructor"""
        msg = "Exception - Zero risk vulnerability is already requested"
        super().__init__(msg)


class AlreadyZeroRiskConfirmed(CustomBaseException):
    """Exception to control uploaded vulns that were already flagged as ZR"""

    def __init__(self, info: str = "") -> None:
        """Constructor"""
        if info:
            msg = f"Exception - Uploaded vulnerability is a confirmed Zero Risk: {info}"
        else:
            msg = "Exception - Uploaded vulnerability is a confirmed Zero Risk"
        super().__init__(msg)


class DocumentNotFound(CustomBaseException):
    """Exception to control analytics data availability"""

    def __init__(self) -> None:
        """Constructor"""
        msg = "Exception - Document not found"
        super().__init__(msg)


class DuplicateDraftFound(CustomBaseException):
    """Exception to control duplicates in the draft creation process"""

    def __init__(self, kind: str) -> None:
        """Constructor"""
        msg = (
            f"Exception - A {kind} of this type has been already created."
            " Please submit vulnerabilities there"
        )
        super().__init__(msg)


class EventAlreadyClosed(CustomBaseException):
    """Exception to control event updates"""

    def __init__(self) -> None:
        """Constructor"""
        msg = "Exception - The event has already been closed"
        super().__init__(msg)


class EventNotFound(CustomBaseException):
    """Exception to control event data availability"""

    def __init__(self) -> None:
        """Constructor"""
        msg = "Exception - Event not found"
        super().__init__(msg)


class EvidenceNotFound(CustomBaseException):
    """Exception to control evidence data availability"""

    def __init__(self) -> None:
        """Constructor"""
        msg = "Exception - Evidence not found"
        super().__init__(msg)


class ExpiredToken(CustomBaseException):
    """Exception to control if an user token exists, so has not expired"""

    def __init__(self) -> None:
        msg = "Exception - User token has expired"
        super().__init__(msg)


class ExpiredForcesToken(CustomBaseException):
    def __init__(self) -> None:
        msg = "Exception - Forces token has expired"
        super().__init__(msg)


class InvalidAlgorithm(CustomBaseException):
    """
    Exception to control and handle cases where an unsupported or
    invalid algorithm is used to sign or verify a JWT token.
    """

    def __init__(self) -> None:
        msg = "Invalid algorithm used in the JWT token."
        super().__init__(msg)


class FindingNotFound(CustomBaseException):
    """Exception to control finding data availability"""

    def __init__(self) -> None:
        """Constructor"""
        msg = "Access denied"
        super().__init__(msg)


class EnrollmentNotFound(CustomBaseException):
    """Exception to control enrollment availability"""

    def __init__(self) -> None:
        """Constructor"""
        msg = "Access denied or enrollment not found"
        super().__init__(msg)


class GroupNotFound(CustomBaseException):
    """Exception to control group availability"""

    def __init__(self) -> None:
        """Constructor"""
        msg = "Access denied or group not found"
        super().__init__(msg)


class HasVulns(CustomBaseException):
    def __init__(self) -> None:
        msg = "Exception - A root with reported vulns can't be updated"
        super().__init__(msg)


class HookInvalid(CustomBaseException):
    """Exception to control hook invalidity"""

    def __init__(self) -> None:
        """Constructor"""
        msg = "Hook data is invalid"
        super().__init__(msg)


class HookNotFound(CustomBaseException):
    """Exception to control hook availability"""

    def __init__(self) -> None:
        """Constructor"""
        msg = "Access denied or hook not found"
        super().__init__(msg)


class HookWithSameEntryPoint(CustomBaseException):
    """Exception to control hook entry_point availability"""

    def __init__(self) -> None:
        """Constructor"""
        msg = "Entry point already exists"
        super().__init__(msg)


class HostNotFound(CustomBaseException):
    """Exception to control host availability"""

    def __init__(self) -> None:
        """Constructor"""
        msg = "Can not find host"
        super().__init__(msg)


class IncompleteFinding(CustomBaseException):
    def __init__(self, fields: Sequence[str]) -> None:
        """Constructor"""
        msg = f"Exception - This finding has missing fields: {', '.join(fields)}"
        super().__init__(msg)


class InvalidAcceptanceDays(CustomBaseException):
    """Exception to control correct input in organization settings"""

    def __init__(self, expr: str = "") -> None:
        if expr:
            msg = f"Exception - {expr}"
        else:
            msg = "Exception - Acceptance days should be a positive integer"
        super().__init__(msg)


class InvalidCSVFormat(CustomBaseException):
    """Exception to catch wrong CSV format"""

    field: str
    field_index: int
    line: str
    line_index: int
    msg: str

    def __init__(
        self,
        field: str,
        field_index: int = 0,
        line: str = "",
        line_index: int = 0,
        msg: str = "Wrong format",
    ) -> None:
        self.field = field
        self.field_index = field_index
        self.line = line
        self.line_index = line_index
        self.msg = msg
        super().__init__(msg)


class InvalidMinTimeToRemediate(CustomBaseException):
    """Exception to control correct MTTR input in draft creation"""

    def __init__(self) -> None:
        msg = "Exception - Min time to remediate should be a positive number"
        super().__init__(msg)


class InvalidAcceptanceSeverity(CustomBaseException):
    def __init__(self, expr: str = "") -> None:
        if expr:
            msg = (
                "Exception - Vulnerability cannot be accepted, severity "
                "outside of range set by the defined policy"
            )
        else:
            msg = (
                "Exception - Severity value must be a positive floating number between 0.0 and 10.0"
            )
        super().__init__(msg)


class InvalidAcceptanceSeverityRange(CustomBaseException):
    def __init__(self) -> None:
        msg = "Exception - Min acceptance severity value should not be higher than the max value"
        super().__init__(msg)


class InvalidAcceptanceDaysUntilItBreaksRange(CustomBaseException):
    def __init__(self) -> None:
        msg = "Exception - Grace period value should not be higher than Days until it breaks value"
        super().__init__(msg)


class InvalidDaysUntilItBreaks(CustomBaseException):
    def __init__(self) -> None:
        msg = "Exception - Days until it breaks must be a number greater than 0"
        super().__init__(msg)


class InvalidMaxAcceptanceDays(CustomBaseException):
    def __init__(self) -> None:
        msg = "Exception - The max acceptance days value must be between 0 and 999"
        super().__init__(msg)


class InvalidVulnerabilityGracePeriod(CustomBaseException):
    """
    Exception to control correct input in organization settings
    (DevSecOps vulnerability grace period)
    """

    def __init__(self, expr: str = "") -> None:
        if expr:
            msg = f"Exception - {expr}"
        else:
            msg = "Exception - Vulnerability grace period value should be a positive integer"
        super().__init__(msg)


class InvalidAuthorization(CustomBaseException):
    """Exception to control authorization."""

    def __init__(self) -> None:
        """Constructor"""
        msg = "Exception - Invalid Authorization"
        super().__init__(msg)


class InvalidChar(CustomBaseException):
    """Exception to control invalid characters in forms"""

    def __init__(self, expr: str = "") -> None:
        if expr:
            msg = f"Exception - Invalid characters in {expr}"
        else:
            msg = "Exception - Invalid characters"
        super().__init__(msg)


class InvalidBePresentFilterCursor(CustomBaseException):
    """Exception to control the be present filter cursor"""

    def __init__(self) -> None:
        msg = "Exception - The cursor is invalid for the value in the be present filter"
        super().__init__(msg)


class InvalidFilter(CustomBaseException):
    """Exception to control the supported filters"""

    def __init__(self, filter_name: str) -> None:
        msg = f"Exception - The filter is not supported: {filter_name}"
        super().__init__(msg)


class InvalidFilterCursor(CustomBaseException):
    """Exception to control the cursor with filters"""

    def __init__(self) -> None:
        msg = "Exception - The cursor is invalid with a filter"
        super().__init__(msg)


class InvalidCommentParent(CustomBaseException):
    """Exception to prevent repeated values"""

    def __init__(self) -> None:
        """Constructor"""
        msg = "Exception - Comment parent is invalid"
        super().__init__(msg)


class InvalidSpacesField(CustomBaseException):
    """Exception to avoid fields from being filled with spaces."""

    def __init__(self) -> None:
        """Constructor"""
        msg = "Exception - Field cannot fill with blank characters"
        super().__init__(msg)


class MachineCouldNotBeQueued(CustomBaseException):
    """Exception to handle when a Machine job cannot be queued"""

    def __init__(self) -> None:
        msg: str = (
            "Exception - Machine execution could not be queued. "
            "Either the group has the service disabled "
            "or the roots specified are invalid"
        )
        super().__init__(msg)


class MachineExecutionAlreadySubmitted(CustomBaseException):
    """
    Exception to handle when a Machine job cannot be queued
    due to an existing one
    """

    def __init__(self) -> None:
        msg: str = (
            "Exception - There is already a Machine execution queued with the same parameters"
        )
        super().__init__(msg)


class RepeatedComment(CustomBaseException):
    """Exception to prevent repeated values"""

    def __init__(self) -> None:
        """Constructor"""
        msg = "Exception - Comment already exists "
        super().__init__(msg)


class ResourceTypeNotFound(CustomBaseException):
    """Exception to prevent wrong resource types"""

    def __init__(self) -> None:
        """Constructor"""
        msg = "Exception - Resource Type not found"
        super().__init__(msg)


class InvalidCVSSField(CustomBaseException):
    """Exception to control CVSS field values"""

    def __init__(self) -> None:
        """Constructor"""
        msg = "Exception - CVSS field value must be a number"
        super().__init__(msg)


class InvalidCVSSVersion(CustomBaseException):
    """Exception to control CVSS version"""

    def __init__(self) -> None:
        """Constructor"""
        msg: str = "Invalid, CVSS version is not supported"
        super().__init__(msg)


class InvalidDate(CustomBaseException):
    """Exception to control the date inserted in an Accepted vulnerability"""

    def __init__(self) -> None:
        """Constructor"""
        msg = "Exception - The inserted date is invalid"
        super().__init__(msg)


class InvalidDateFormat(CustomBaseException):
    """
    Exception to control the date format inserted in an Accepted
    vulnerability and the API deprecation notices
    """

    def __init__(self, expr: str = "") -> None:
        """Constructor"""
        if expr:
            msg = f"Exception - The date format is invalid: {expr}"
        else:
            msg = "Exception - The date format is invalid"
        super().__init__(msg)


class InvalidDraftConsult(CustomBaseException):
    """Exception to halt consults made in drafts"""

    def __init__(self) -> None:
        """Constructor"""
        msg = "Exception - Consults are not allowed in Drafts"
        super().__init__(msg)


class InvalidExpirationTime(CustomBaseException):
    """Exception to control valid expiration time."""

    def __init__(self) -> None:
        """Constructor"""
        msg = "Exception - Invalid Expiration Time"
        super().__init__(msg)


class TokenCouldNotBeAdded(CustomBaseException):
    """Exception to control number of added tokens."""

    def __init__(self) -> None:
        """Constructor"""
        msg = "Exception - Could not add token, maximum number of tokens at the same time is 2"
        super().__init__(msg)


class TokenNotFound(CustomBaseException):
    def __init__(self) -> None:
        msg = "Exception - Access denied or token not found"
        super().__init__(msg)


class InvalidField(CustomBaseException):
    """Exception to control invalid fields in forms"""

    def __init__(self, field: str = "field") -> None:
        """Constructor"""
        msg = f"Exception - Invalid {field} in form"
        super().__init__(msg)


class InvalidFieldLength(CustomBaseException):
    """Exception to control invalid field length in forms"""

    def __init__(self) -> None:
        """Constructor"""
        msg = "Exception - Invalid field length in form"
        super().__init__(msg)


class InvalidFileSize(CustomBaseException):
    """Exception to control file size."""

    def __init__(self) -> None:
        """Constructor"""
        msg = "Exception - Invalid file size"
        super().__init__(msg)


class InvalidFileStructure(CustomBaseException):
    """Exception to control file structure."""

    def __init__(self) -> None:
        """Constructor"""
        msg = "Exception - Wrong file structure"
        super().__init__(msg)


class InvalidFileType(CustomBaseException):
    """Exception to control file type."""

    def __init__(self, detail: str = "") -> None:
        """Constructor"""
        msg = "Exception - Invalid file type"
        if detail:
            msg += f": {detail}"
        super().__init__(msg)


class InvalidFileName(CustomBaseException):
    """Exception to control type filename."""

    def __init__(self, detail: str = "") -> None:
        """Constructor"""
        msg = "Exception - Invalid file name"
        if detail:
            msg += f": {detail}"
        super().__init__(msg)


class InvalidFindingTitle(CustomBaseException):
    """Exception to control draft and finding titles"""

    def __init__(self) -> None:
        """Constructor"""
        msg = "Exception - The inserted Draft/Finding title is invalid"
        super().__init__(msg)


class InvalidFieldChange(CustomBaseException):
    """Exception to control forbidden field changes"""

    def __init__(self, fields: Sequence[str], reason: str) -> None:
        """Constructor"""
        msg = f"Exception - Forbidden change on field: {', '.join(fields)}  Reason: {reason}"
        super().__init__(msg)


class InvalidGroupServicesConfig(CustomBaseException):
    """Exception to control that services attached to a group are valid."""

    def __init__(self, msg: str) -> None:
        """Constructor"""
        super().__init__(f"Exception - {msg}")


class InvalidGroupTier(CustomBaseException):
    def __init__(self) -> None:
        msg = (
            "Exception - Invalid tier. Only 'oneshot', 'essential', 'advanced' and 'free' allowed."
        )
        super().__init__(msg)


class InvalidJustificationMaxLength(CustomBaseException):
    """Exception to control justification length"""

    def __init__(self, field: int) -> None:
        """Constructor"""
        msg = f"Exception - Justification must have a maximum of {field} characters"
        super().__init__(msg)


class InvalidMarkdown(CustomBaseException):
    """Exception to control invalid markdown fields"""

    def __init__(self) -> None:
        msg = "Exception - Invalid markdown"
        super().__init__(msg)


class InvalidNotificationRequest(CustomBaseException):
    """Exception to control invalid email notification requests"""

    def __init__(self, expr: str = "") -> None:
        if expr:
            msg = f"Exception - Invalid email notification request: {expr}"
        else:
            msg = "Exception - Invalid email notification request"
        super().__init__(msg)


class InvalidNumberAcceptances(CustomBaseException):
    def __init__(self, expr: str = "") -> None:
        if expr:
            msg = (
                "Exception - Vulnerability has been accepted the maximum "
                "number of times allowed by the defined policy"
            )
        else:
            msg = "Exception - Number of acceptances should be zero or positive"
        super().__init__(msg)


class InvalidOrganization(CustomBaseException):
    """Exception to prevent repeated organizations"""

    def __init__(self, msg: str = "") -> None:
        """Constructor"""
        if msg == "":
            msg = "Access denied"
        super().__init__(msg)


class InvalidParameter(CustomBaseException):
    field: str
    """Exception to control empty required parameters"""

    def __init__(self, field: str = "") -> None:
        """Constructor"""
        self.field = field

        if field:
            msg = f"Exception - Field {field} is invalid"
        else:
            msg = "Exception - Error value is not valid"
        super().__init__(msg)


class InvalidPath(CustomBaseException):
    """Exception to control valid path value in vulnerabilities."""

    def __init__(self, expr: str) -> None:
        """Constructor"""
        msg = f'{{"msg": "Exception - Error in path value", {expr}}}'
        super().__init__(msg)


class InvalidPort(CustomBaseException):
    """Exception to control valid port value in vulnerabilities."""

    def __init__(self, expr: str = "") -> None:
        """Constructor"""
        msg = f'{{"msg": "Exception - Error in port value", {expr}}}'
        super().__init__(msg)


class InvalidProtocol(CustomBaseException):
    """Exception to control valid protocol to use."""

    def __init__(self, protocol: str) -> None:
        """Constructor"""
        msg = f"Exception - Protocol {protocol} is invalid"
        super().__init__(msg)


class InvalidPositiveArgument(CustomBaseException):
    def __init__(self, arg: str) -> None:
        """Constructor"""
        msg = f"The argument must be a positive integer: {arg}"
        super().__init__(f"Exception - {msg}")


class InvalidCriteriaRange(CustomBaseException):
    """Exception to control valid range in criteria."""

    def __init__(self, expr: str = "") -> None:
        """Constructor"""
        msg = f"{'Exception - Error, field is out of range', {expr}}"
        super().__init__(msg)


class InvalidRange(CustomBaseException):
    """Exception to control valid range in vulnerabilities."""

    def __init__(self, expr: str = "") -> None:
        """Constructor"""
        msg = f'{{"msg": "Exception - Error in range limit numbers", {expr}}}'
        super().__init__(msg)


class InvalidRoleProvided(CustomBaseException):
    """Exception to control that users only grant roles they're allowed to."""

    def __init__(self, role: str) -> None:
        """Constructor"""
        msg = f"Invalid role or not enough permissions to grant role: {role}"
        super().__init__(f"Exception - {msg}")


class InvalidRootComponent(CustomBaseException):
    """Exception to control the root has the component"""

    def __init__(self) -> None:
        """Constructor"""
        msg = "Exception - The root does not have the component"
        super().__init__(msg)


class InvalidIpAddressInRoot(CustomBaseException):
    def __init__(self) -> None:
        """Constructor"""
        msg = "Exception - The root does not have the IP address"
        super().__init__(msg)


class InvalidRootExclusion(CustomBaseException):
    """Exception to control exclusion paths"""

    def __init__(self) -> None:
        """Constructor"""
        msg = "Exception - Root name should not be included in the exception pattern"
        super().__init__(msg)


class InvalidSchema(CustomBaseException):
    """Exception to control schema validation."""

    def __init__(self, expr: str = "") -> None:
        """Constructor"""
        msg = f'{{"msg": "Exception - Invalid Schema", {expr}}}'
        super().__init__(msg)


class InvalidSeverity(CustomBaseException):
    """Exception to control severity value"""

    def __init__(self, fields: Sequence[Decimal]) -> None:
        """Constructor"""
        msg = "Exception - Severity value must be between {fields[0]} and {fields[1]}"
        super().__init__(msg)


class InvalidReportFilter(CustomBaseException):
    """Exception to control severity value"""

    def __init__(self, expr: str = "") -> None:
        msg: str = "Exception - Invalid filter"
        if expr:
            msg = f"Exception - {expr}"
        super().__init__(msg)


class InvalidSource(CustomBaseException):
    """Exception to control if the source is valid."""

    def __init__(self) -> None:
        """Constructor"""
        msg = "Exception - Invalid source"
        super().__init__(msg)


class InvalidStream(CustomBaseException):
    """Exception to control stream validation."""

    def __init__(self, vuln_type: str = "", index: str = "") -> None:
        """Constructor"""
        msg = (
            '{"msg": "Exception - Invalid stream should start \'home\' or '
            f'\'query\'", "path": "/{vuln_type}/{index}"}}'
        )
        super().__init__(msg)


class InvalidStateStatus(CustomBaseException):
    """Exception to control state status."""

    def __init__(self) -> None:
        """Constructor"""
        msg = "Exception - Invalid state status"
        super().__init__(msg)


class InvalidAssigned(CustomBaseException):
    """Exception to control if assigned user is valid"""

    def __init__(self) -> None:
        msg = "Assigned not valid"
        super().__init__(msg)


class InvalidUserProvided(CustomBaseException):
    """
    Exception to control that users belong to Fluid Attacks before they're
    granted a restricted role
    """

    def __init__(self) -> None:
        """Constructor"""
        msg = "This role can only be granted to Fluid Attacks users"
        super().__init__(f"Exception - {msg}")


class InvalidVulnsNumber(CustomBaseException):
    """Exception to control number of vulnerabilities provided to upload."""

    def __init__(self, number_of_vulns: int = 100) -> None:
        msg = f"Exception - You can upload a maximum of {number_of_vulns} vulnerabilities per file"
        super().__init__(msg)


class NotVerificationRequested(CustomBaseException):
    """Exception to control finding verification"""

    def __init__(self) -> None:
        """Constructor"""
        msg = "Exception - Error verification not requested"
        super().__init__(msg)


class NotZeroRiskRequested(CustomBaseException):
    """Exception to control zero risk already is not requested"""

    def __init__(self) -> None:
        """Constructor"""
        msg = "Exception - Zero risk vulnerability is not requested"
        super().__init__(msg)


class OrganizationNotFound(CustomBaseException):
    """Exception to control organization availability"""

    def __init__(self) -> None:
        """Constructor"""
        msg = "Access denied or organization not found"
        super().__init__(msg)


class PermissionDenied(CustomBaseException):
    """Exception to control permission"""

    def __init__(self, expr: str = "") -> None:
        msg = f"Exception - Error permission denied{' - ' if expr else ''}{expr}"
        super().__init__(msg)


class InvalidGroupService(CustomBaseException):
    """
    Exception to control that only advanced groups can request
    health check.
    """

    def __init__(self) -> None:
        msg = "Exception - Health check cannot be requested outside of Advanced groups"
        super().__init__(msg)


class InvalidRoleSetGitIgnore(CustomBaseException):
    """Exception to control permission to set a git ignore."""

    def __init__(self) -> None:
        msg = "Exception - Permission denied to exclude files"
        super().__init__(msg)


class InvalidGitIgnoreExpression(CustomBaseException):
    """Exception to invalid git ignore expression."""

    def __init__(self, expr: str = "") -> None:
        msg = f"Exception - Invalid expression - {expr}"
        super().__init__(msg)


class RepeatedRoot(CustomBaseException):
    """Exception to prevent repeated roots"""

    def __init__(self) -> None:
        """Constructor"""
        msg = "Exception - Root with the same URL/branch already exists"
        super().__init__(msg)


class RepeatedRootEnvironmentFile(CustomBaseException):
    def __init__(self) -> None:
        """Constructor"""
        msg = "Exception - Root environment with the same file already exists"
        super().__init__(msg)


class RepeatedRootEnvironmentUrl(CustomBaseException):
    def __init__(self) -> None:
        """Constructor"""
        msg = "Exception - Root environment with the same url already exists"
        super().__init__(msg)


class RepeatedRootNickname(CustomBaseException):
    """Exception to prevent repeated roots"""

    def __init__(self) -> None:
        """Constructor"""
        msg = "Exception - Root with the same nickname already exists"
        super().__init__(msg)


class RootNicknameUsed(CustomBaseException):
    """Exception to control schema validation."""

    def __init__(self, reason: str = "") -> None:
        """Constructor"""
        msg = f"Exception - Invalid nickname - {reason}"
        super().__init__(msg)


class RepeatedToeInput(CustomBaseException):
    def __init__(self) -> None:
        msg = "Exception - Toe input already exists"
        super().__init__(msg)


class RepeatedToeLines(CustomBaseException):
    def __init__(self) -> None:
        msg = "Exception - Toe lines already exists"
        super().__init__(msg)


class RepeatedToePackage(CustomBaseException):
    def __init__(self, pkg_identifier: str | None = None) -> None:
        msg = "Exception - Package already exists"
        if pkg_identifier:
            msg += f" for {pkg_identifier}"
        super().__init__(msg)


class RepeatedToePort(CustomBaseException):
    def __init__(self) -> None:
        msg = "Exception - Toe port already exists"
        super().__init__(msg)


class RepeatedValues(CustomBaseException):
    """Exception to prevent repeated values"""

    def __init__(self) -> None:
        """Constructor"""
        msg = "Exception - One or more values already exist"
        super().__init__(msg)


class RequestedInvitationTooSoon(CustomBaseException):
    """
    Exception to control that new invitations to the same user in the same
    group/org are spaced out by at least one minute
    """

    def __init__(self) -> None:
        """Constructor"""
        msg = "The previous invitation to this user was requested less than a minute ago"
        super().__init__(f"Exception - {msg}")


class RequestedReportError(CustomBaseException):
    """Exception to control cert, pdf, xls or data report error."""

    def __init__(self, expr: str = "") -> None:
        if expr:
            msg = f"Error - {expr}"
        else:
            msg = "Error - Some error ocurred generating the report"
        super().__init__(msg)


class ReportAlreadyRequested(CustomBaseException):
    def __init__(self) -> None:
        msg: str = "Exception - The user already has a requested report for the same group"
        super().__init__(msg)


class CredentialNotFound(CustomBaseException):
    def __init__(self) -> None:
        msg = "Exception - Access denied or credential not found"
        super().__init__(msg)


class RootNotFound(CustomBaseException):
    def __init__(self) -> None:
        msg = "Exception - Access denied or root not found"
        super().__init__(msg)


class RootEnvironmentUrlNotFound(CustomBaseException):
    def __init__(self) -> None:
        msg = "Exception - Access denied or root environment url not found"
        super().__init__(msg)


class RootDockerImageNotFound(CustomBaseException):
    def __init__(self) -> None:
        msg = "Exception - Root image not found"
        super().__init__(msg)


class RootDockerImageInvalidRef(CustomBaseException):
    def __init__(self) -> None:
        msg = "Exception - Ivalid docker image reference"
        super().__init__(msg)


class SecretValueNotFound(CustomBaseException):
    def __init__(self) -> None:
        msg = "Exception - Secret key or resource id not found"
        super().__init__(msg)


class SameValues(CustomBaseException):
    """Exception to control save values updating treatment"""

    def __init__(self) -> None:
        msg = "Exception - Same values"
        super().__init__(msg)


class SecureAccessException(CustomBaseException):
    """Exception that controls access to resources with authentication."""

    def __init__(self) -> None:
        """Constructor"""
        msg = "Exception - Access to resources without active session"
        super().__init__(msg)


class StakeholderHasGroupAccess(CustomBaseException):
    def __init__(self) -> None:
        msg = "Exception - The stakeholder has been granted access to the group previously"
        super().__init__(msg)


class StakeholderHasOrganizationAccess(CustomBaseException):
    def __init__(self) -> None:
        msg = "Exception - The stakeholder has been granted access to the organization previously"
        super().__init__(msg)


class StakeholderNotFound(CustomBaseException):
    """Exception to control stakeholder availability"""

    def __init__(self) -> None:
        """Constructor"""
        msg = "Access denied or stakeholder not found"
        super().__init__(msg)


class TagNotFound(CustomBaseException):
    """Exception to control tag availability"""

    def __init__(self) -> None:
        """Constructor"""
        msg = "Access denied or tag not found"
        super().__init__(msg)


class ToeInputAlreadyUpdated(CustomBaseException):
    """Exception to control the toe input has not been updated"""

    def __init__(self) -> None:
        """Constructor"""
        msg = "Exception - The toe input has been updated by another operation"
        super().__init__(msg)


class ToeInputNotFound(CustomBaseException):
    def __init__(self) -> None:
        msg = "Exception - Toe input has not been found"
        super().__init__(msg)


class ToePackageAlreadyUpdated(CustomBaseException):
    """Exception to control the toe package has not been updated"""

    def __init__(self) -> None:
        """Constructor"""
        msg = "Exception - The toe package has been updated by another operation"
        super().__init__(msg)


class ToeLinesAlreadyUpdated(CustomBaseException):
    """Exception to control the toe lines has not been updated"""

    def __init__(self) -> None:
        """Constructor"""
        msg = "Exception - The toe lines has been updated by another operation"
        super().__init__(msg)


class ToeLinesNotFound(CustomBaseException):
    def __init__(self) -> None:
        msg = "Exception - Toe lines has not been found"
        super().__init__(msg)


class ToePortAlreadyUpdated(CustomBaseException):
    def __init__(self) -> None:
        """Constructor"""
        msg = "Exception - The toe port has been updated by another operation"
        super().__init__(msg)


class ToePortNotFound(CustomBaseException):
    def __init__(self) -> None:
        msg = "Exception - Toe port has not been found"
        super().__init__(msg)


class UnexpectedUserRole(CustomBaseException):
    """Exception to control that roles attached to an user are valid."""

    def __init__(self, msg: str) -> None:
        """Constructor"""
        super().__init__(f"Exception - {msg}")


class StakeholderNotInOrganization(CustomBaseException):
    """Exception to control stakeholder access to organizations."""

    def __init__(self, expr: str = "") -> None:
        if expr:
            msg = "Exception - Stakeholder is not a member of the organization"
        else:
            msg = "Access denied"
        super().__init__(msg)


class StakeholderNotInGroup(CustomBaseException):
    """Exception to control stakeholder access to groups."""

    def __init__(self, expr: str = "") -> None:
        if expr:
            msg = "Exception - Stakeholder is not a member of the group"
        else:
            msg = "Access denied"
        super().__init__(msg)


class ExecutionNotFound(CustomBaseException):
    """Exception to control data availability."""

    def __init__(self, expr: str = "") -> None:
        if expr:
            msg = "Exception - Execution not found"
        else:
            msg = "Access denied"
        super().__init__(msg)


class VulnAlreadyClosed(CustomBaseException):
    """Exception to control vulnerability updates"""

    def __init__(self) -> None:
        """Constructor"""
        msg = "Exception - The vulnerability has already been closed"
        super().__init__(msg)


class VulnNotFound(CustomBaseException):
    """Exception to control vulnerability data availability"""

    def __init__(self) -> None:
        """Constructor"""
        msg = "Exception - Vulnerability not found"
        super().__init__(msg)


class VulnNotInFinding(CustomBaseException):
    """Exception to control vulnerability in finding"""

    def __init__(self) -> None:
        msg = "Exception - Vulnerability does not belong to finding"
        super().__init__(msg)


class InvalidFindingNameTreatmentPolicy(CustomBaseException):
    def __init__(self) -> None:
        msg = "Exception - Global policy only available for temporary acceptance"
        super().__init__(msg)


class RepeatedFindingNamePolicy(CustomBaseException):
    def __init__(self) -> None:
        msg = "Exception - The finding name policy already exists"
        super().__init__(msg)


class PolicyAlreadyHandled(CustomBaseException):
    def __init__(self) -> None:
        msg = "Exception - This policy has already been reviewed"
        super().__init__(msg)


class InactiveRoot(CustomBaseException):
    def __init__(self) -> None:
        msg = "Exception - The root is not active"
        super().__init__(msg)


class RootAlreadyCloning(CustomBaseException):
    def __init__(self) -> None:
        msg = "Exception - The root already has an active cloning process"
        super().__init__(msg)


class CloningCouldNotBeQueued(CustomBaseException):
    def __init__(self) -> None:
        msg = "Exception - Cloning job or task could not be queued"
        super().__init__(msg)


class ToeInputNotPresent(CustomBaseException):
    def __init__(self) -> None:
        msg = "Exception - The toe input is not present"
        super().__init__(msg)


class InvalidToeInputAttackedAt(CustomBaseException):
    def __init__(self) -> None:
        msg = "Exception - The attack time must be between the previous attack and the current time"
        super().__init__(msg)


class InvalidToeInputAttackedBy(CustomBaseException):
    def __init__(self) -> None:
        msg = "Exception - The input attacked by attribute is mandatory"
        super().__init__(msg)


class InvalidToeLinesAttackAt(CustomBaseException):
    def __init__(self) -> None:
        msg = "Exception - The attack time must be between the previous attack and the current time"
        super().__init__(msg)


class InvalidToeLinesAttackedLines(CustomBaseException):
    def __init__(self) -> None:
        msg = "Exception - The attacked lines must be between 0 and the loc (lines of code)"
        super().__init__(msg)


class InvalidToeLinesAttackedBy(CustomBaseException):
    def __init__(self) -> None:
        msg = "Exception - The input attacked by attribute is mandatory"
        super().__init__(msg)


class ToePortNotPresent(CustomBaseException):
    def __init__(self) -> None:
        msg = "Exception - The toe port is not present"
        super().__init__(msg)


class InvalidToePortAttackedAt(CustomBaseException):
    def __init__(self) -> None:
        msg = "Exception - The attack time must be between the previous attack and the current time"
        super().__init__(msg)


class InvalidToePortAttackedBy(CustomBaseException):
    def __init__(self) -> None:
        msg = "Exception - The port attacked by attribute is mandatory"
        super().__init__(msg)


class InvalidBillingCustomer(CustomBaseException):
    def __init__(self) -> None:
        msg = "Exception - Cannot perform action. Please add a valid payment method first"
        super().__init__(msg)


class NumberOutOfRange(CustomBaseException):
    def __init__(self, lower_bound: int, upper_bound: int, inclusive: bool) -> None:
        inclusive_str = " (inclusive)" if inclusive else ""
        msg = (
            f"Exception - Value must be between {lower_bound}{inclusive_str} "
            f"and {upper_bound}{inclusive_str}"
        )
        super().__init__(msg)


class BillingSubscriptionSameActive(CustomBaseException):
    def __init__(self) -> None:
        msg = "Exception - Invalid subscription. Provided subscription is already active"
        super().__init__(msg)


class CouldNotCreateSubscription(CustomBaseException):
    def __init__(self, entity_name: str) -> None:
        msg = f"Exception - Subscription for {entity_name} could not be created"
        super().__init__(msg)


class BillingCustomerHasNoPaymentMethod(CustomBaseException):
    def __init__(self) -> None:
        msg = "Exception - Invalid customer. Provided customer does not have a payment method"
        super().__init__(msg)


class InvalidBillingPaymentMethod(CustomBaseException):
    def __init__(self) -> None:
        msg = (
            "Exception - Invalid payment method. "
            "Provided payment method does not exist for this organization"
        )
        super().__init__(msg)


class InvalidExpiryDateField(CustomBaseException):
    """Exception to control expiry credit card date field values"""

    def __init__(self) -> None:
        """Constructor"""
        msg = "Exception - Expiriy month or year field value must be a number"
        super().__init__(msg)


class InvalidPaymentBusinessName(CustomBaseException):
    def __init__(self) -> None:
        msg = "Exception - Payment method business name must be match with group business name"

        super().__init__(msg)


class CouldNotUpdateEntity(CustomBaseException):
    def __init__(self, entity_name: str) -> None:
        msg = f"Exception - {entity_name} could not be updated"
        super().__init__(msg)


class CouldNotDowngradeSubscription(CustomBaseException):
    def __init__(self) -> None:
        msg = "Exception - Subscription could not be downgraded, payment intent for Advanced failed"
        super().__init__(msg)


class CouldNotCreatePaymentMethod(CustomBaseException):
    def __init__(self) -> None:
        msg = "Exception - Provided payment method could not be created"
        super().__init__(msg)


class PaymentMethodAlreadyExists(CustomBaseException):
    def __init__(self) -> None:
        msg: str = (
            "Exception - Provided payment method already exists. Please update or delete it first"
        )
        super().__init__(msg)


class CouldNotRemovePaymentMethod(CustomBaseException):
    def __init__(self) -> None:
        msg: str = "Exception - Provided payment method could not be removed"
        super().__init__(msg)


class InvalidManagedChange(CustomBaseException):
    def __init__(self) -> None:
        msg: str = (
            "Exception - Incorrect change in managed parameter. "
            "Please review the payment conditions"
        )
        super().__init__(msg)


class InvalidGitCredentials(CustomBaseException):
    def __init__(self) -> None:
        msg: str = "Exception - Git repository was not accessible with given credentials"
        super().__init__(msg)


class OutdatedRepository(CustomBaseException):
    def __init__(self) -> None:
        msg: str = "Exception - The git repository is outdated"
        super().__init__(msg)


class EmptyHistoric(CustomBaseException):
    """Exception to control the historic is not empty"""

    def __init__(self) -> None:
        """Constructor"""
        msg = "Exception - The historic can not be empty"
        super().__init__(msg)


class InvalidUrl(CustomBaseException):
    """Exception to control the url is valid"""

    def __init__(self) -> None:
        """Constructor"""
        msg = "Exception - The URL is not valid"
        super().__init__(msg)


class CouldNotStartStakeholderVerification(CustomBaseException):
    def __init__(self) -> None:
        msg = "Exception - Stakeholder verification could not be started"
        super().__init__(msg)


class CouldNotVerifyStakeholder(CustomBaseException):
    def __init__(self) -> None:
        msg = "Exception - Stakeholder could not be verified"
        super().__init__(msg)


class InvalidVerificationCode(CustomBaseException):
    def __init__(self) -> None:
        msg = "Exception - The verification code is invalid"
        super().__init__(msg)


class InvalidMobileNumber(CustomBaseException):
    def __init__(self) -> None:
        msg = "Exception - A mobile number is required with the international format"
        super().__init__(msg)


class RequiredValue(CustomBaseException):
    def __init__(self, msg: str = "A value") -> None:
        msg = f"Exception - {msg} must be provided"
        super().__init__(msg)


class RequiredVerificationCode(CustomBaseException):
    def __init__(self) -> None:
        msg = "Exception - The verification code is required"
        super().__init__(msg)


class RequiredNewPhoneNumber(CustomBaseException):
    def __init__(self) -> None:
        msg = "Exception - A new phone number is required"
        super().__init__(msg)


class SamePhoneNumber(CustomBaseException):
    def __init__(self) -> None:
        msg = "Exception - The new phone number is the current phone number"
        super().__init__(msg)


class VulnerabilityPathDoesNotExistInToeLines(CustomBaseException):
    def __init__(self, index: str = "") -> None:
        msg = (
            '{"msg": "Exception - The vulnerability path does not exist in '
            f'the toe lines", "path": "/lines/{index}"}}'
        )
        super().__init__(msg)


class VulnerabilityUrlFieldDoNotExistInToeInputs(CustomBaseException):
    def __init__(self, index: str = "") -> None:
        msg = (
            '{"msg": "Exception -  The vulnerability URL and field do not '
            f'exist in the toe inputs", "path": "/inputs/{index}"}}'
        )
        super().__init__(msg)


class VulnerabilityUrlFieldAreNotPresentInToeInputs(CustomBaseException):
    def __init__(self, index: str = "") -> None:
        msg = (
            '{"msg": "Exception -  The vulnerability URL and field are not '
            f'present in the toe inputs", "path": "/inputs/{index}"}}'
        )
        super().__init__(msg)


class VulnerabilityPortFieldDoNotExistInToePorts(CustomBaseException):
    def __init__(self, index: str = "") -> None:
        msg = (
            '{"msg": "Exception -  The vulnerability address and port do not '
            f'exist in the toe ports", "path": "/ports/{index}"}}'
        )
        super().__init__(msg)


class LineDoesNotExistInTheLinesOfCodeRange(CustomBaseException):
    def __init__(self, line: str, index: str) -> None:
        msg = (
            '{"msg": "Exception -  The line does not exist in the range '
            f'of 0 and lines of code: {line}", "path": "/lines/{index}"}}'
        )
        super().__init__(msg)


class VulnerabilityEntryNotFound(CustomBaseException):
    def __init__(self) -> None:
        msg = "Exception - Vulnerability entry not found"
        super().__init__(msg)


class RequiredStateStatus(CustomBaseException):
    def __init__(self) -> None:
        msg = "Exception - State status is required"
        super().__init__(msg)


class UnsanitizedInputFound(CustomBaseException):
    """Exception to control unsanitized input"""

    def __init__(self) -> None:
        """Constructor"""
        msg = "Exception - Unsanitized input found"
        super().__init__(msg)


class InvalidCommitHash(CustomBaseException):
    def __init__(self) -> None:
        msg = "Exception - The commit hash is invalid"
        super().__init__(msg)


class CommitHashNotProvided(CustomBaseException):
    def __init__(self) -> None:
        msg = "Exception - The commit hash is required on the path"
        super().__init__(msg)


class InvalidCommitHashFormat(CustomBaseException):
    """Exception to control valid commit fortmat in path of vulnerabilities."""

    def __init__(self, expr: str) -> None:
        """Constructor"""
        msg = f'{{"msg": "Exception - The commit hash format is invalid", {expr}}}'
        super().__init__(msg)


class InvalidGitRoot(CustomBaseException):
    def __init__(self) -> None:
        msg = "Exception - A git root is required"
        super().__init__(msg)


class InvalidModifiedDate(CustomBaseException):
    def __init__(self) -> None:
        msg = "Exception - The modified date can not be a future date"
        super().__init__(msg)


class InvalidLinesOfCode(CustomBaseException):
    def __init__(self) -> None:
        msg = "Exception - Lines of code must be equal or greater than 0"
        super().__init__(msg)


class CredentialAlreadyExists(CustomBaseException):
    def __init__(self) -> None:
        msg = "Exception - A credential exists with the same name"
        super().__init__(msg)


class UriAlreadyExists(CustomBaseException):
    def __init__(self) -> None:
        msg = "Exception - Docker image URI already exists"
        super().__init__(msg)


class UriFormatError(CustomBaseException):
    def __init__(self) -> None:
        msg = "Exception - URI format incorrect"
        super().__init__(msg)


class RequiredCredentials(CustomBaseException):
    def __init__(self) -> None:
        msg = "Exception - Credentials is required"
        super().__init__(msg)


class CredentialNotInOrganization(CustomBaseException):
    def __init__(self, credential_id: str) -> None:
        msg = (
            "Exception - Credential id does not belong to the "
            f"organization. cred_id: {credential_id}"
        )
        super().__init__(msg)


class OnlyCorporateEmails(CustomBaseException):
    def __init__(self) -> None:
        msg = "Exception - Only corporate emails are allowed"
        super().__init__(msg)


class NewMemberWithExistingDomain(CustomBaseException):
    def __init__(self) -> None:
        msg = "Exception - New member with existing domain"
        super().__init__(msg)


class FreeTrialAlreadyUsed(CustomBaseException):
    def __init__(self) -> None:
        msg = "Exception - Free trial has already been used"
        super().__init__(msg)


class InvalidCredentialSecret(CustomBaseException):
    def __init__(self) -> None:
        msg = "Exception - Invalid secret for the credential type"
        super().__init__(msg)


class RepeatedCredential(CustomBaseException):
    def __init__(self) -> None:
        msg = "Exception - Credential already exists"
        super().__init__(msg)


class CredentialInUse(CustomBaseException):
    """When credential is still in use by any root before deleting it"""

    def __init__(self) -> None:
        msg = "Exception - Credential is still in use by a root"
        super().__init__(msg)


class InvalidBase64SshKey(CustomBaseException):
    def __init__(self) -> None:
        msg = "Exception - The ssh key must be in base64"
        super().__init__(msg)


class UnableToSendSms(CustomBaseException):
    def __init__(self) -> None:
        msg: str = "Exception - Unable to send sms message"
        super().__init__(msg)


class EventHasNotBeenSolved(CustomBaseException):
    def __init__(self) -> None:
        msg = "Exception - The event has not been solved"
        super().__init__(msg)


class BranchNotFound(CustomBaseException):
    def __init__(self) -> None:
        msg = "Exception - Branch not found"
        super().__init__(msg)


class RequiredFieldToBeUpdate(CustomBaseException):
    def __init__(self) -> None:
        msg = "Exception - A field is required to be updated"
        super().__init__(msg)


class EventVerificationAlreadyRequested(CustomBaseException):
    def __init__(self) -> None:
        msg = "Exception - The event verification has been requested"
        super().__init__(msg)


class EventVerificationNotRequested(CustomBaseException):
    def __init__(self) -> None:
        msg = "Exception - The event verification has not been requested"
        super().__init__(msg)


class TooManyRequests(CustomBaseException):
    def __init__(self) -> None:
        msg = "Exception - Too many requests"
        super().__init__(msg)


class InvalidResourceType(CustomBaseException):
    def __init__(self) -> None:
        msg = "Exception - The type of the resource is invalid"
        super().__init__(msg)


class InvalidRootType(CustomBaseException):
    def __init__(self) -> None:
        msg = "Exception - The type of the root is invalid"
        super().__init__(msg)


class RootDockerfileNotExists(CustomBaseException):
    def __init__(self) -> None:
        msg = "Exception - The git root does not contain any docker files"
        super().__init__(msg)


class FileNotFound(CustomBaseException):
    def __init__(self) -> None:
        msg = "Exception - The file has not been found"
        super().__init__(msg)


class VulnerabilitiesHasUnsolvedEvents(CustomBaseException):
    def __init__(self) -> None:
        msg = "Exception - The location have unresolved events"
        super().__init__(msg)


class InvalidEventSolvingReason(CustomBaseException):
    def __init__(self) -> None:
        msg = "Exception - The solving reason is not valid for the event type"
        super().__init__(msg)


class TrialRestriction(CustomBaseException):
    def __init__(self) -> None:
        msg = "Exception - The action is not allowed during the free trial"
        super().__init__(msg)


class GroupHasPendingActions(CustomBaseException):
    def __init__(self, action_names: list[str]) -> None:
        msg = f"Exception - The group has pending actions: {action_names!s}"
        super().__init__(msg)


class VulnerabilityHasNotBeenReleased(CustomBaseException):
    def __init__(self) -> None:
        msg = "Exception - The vulnerability has not been released"
        super().__init__(msg)


class VulnerabilityHasNotBeenRejected(CustomBaseException):
    def __init__(self) -> None:
        msg = "Exception - The vulnerability has not been rejected"
        super().__init__(msg)


class VulnerabilityHasNotBeenSubmitted(CustomBaseException):
    def __init__(self) -> None:
        msg = "Exception - The vulnerability has not been submitted"
        super().__init__(msg)


class InvalidStandardId(CustomBaseException):
    def __init__(self) -> None:
        msg = "Exception - The standard id is invalid"
        super().__init__(msg)


class GroupHasNotUnfulfilledStandards(CustomBaseException):
    def __init__(self) -> None:
        msg = "Exception -  No unfulfilled standards in group"
        super().__init__(msg)


class InvalidVulnerabilityRequirement(CustomBaseException):
    def __init__(self) -> None:
        msg = "Exception - The requirement is not valid in the vulnerability"
        super().__init__(msg)


class RepeatedFindingThreat(CustomBaseException):
    def __init__(self) -> None:
        msg = "Exception - Finding with the same threat already exists"
        super().__init__(msg)


class RepeatedFindingDescription(CustomBaseException):
    def __init__(self) -> None:
        msg = "Exception - Finding with the same description already exists"
        super().__init__(msg)


class RequiredUnfulfilledRequirements(CustomBaseException):
    def __init__(self) -> None:
        msg = "Exception - The unfulfilled requirements are required"
        super().__init__(msg)


class RepeatedFindingMachineDescription(CustomBaseException):
    def __init__(self) -> None:
        msg = "Exception - Finding with the same description, threat and severity already exists"
        super().__init__(msg)


class RepeatedMachineFindingCode(CustomBaseException):
    def __init__(self) -> None:
        msg = "Exception - Finding with the same finding code already exists"
        super().__init__(msg)


class RequiredSubmittedStatus(CustomBaseException):
    def __init__(self, where: str, specific: str) -> None:
        msg = (
            '{"msg": "Exception - New vulnerabilities require the submitted'
            f' status", "where": "{where}", "specific": "{specific}" }}'
        )
        super().__init__(msg)


class SecretAlreadyExists(CustomBaseException):
    def __init__(self) -> None:
        msg = "Exception - A secret with the same key already exists"
        super().__init__(msg)


class VulnerabilityCantNotChangeStatus(CustomBaseException):
    def __init__(self, where: str, specific: str, status: str) -> None:
        msg = (
            '{"msg": "Exception - Uploaded vulnerability can not change the'
            f' status", "status": "{status}", "where": "{where}", "specific":'
            f' "{specific}" }}'
        )

        super().__init__(msg)


class InvalidNewVulnStateStatus(_SingleMessageException):
    def __init__(self, status: str) -> None:
        msg = f"Invalid, only New vulnerabilities with {status} state are allowed"
        super().__init__(msg)


class ExcludedSubPath(CustomBaseException):
    def __init__(self) -> None:
        msg = "Exception - The sub-path has been excluded"
        super().__init__(msg)


class ExcludedEnvironment(CustomBaseException):
    def __init__(self) -> None:
        msg = "Exception - The environment has been excluded"
        super().__init__(msg)


class InvalidSubPath(CustomBaseException):
    def __init__(self) -> None:
        msg = "Exception - The sub-path is not valid"
        super().__init__(msg)


class InvalidSubPathInUrlRootWithQuery(CustomBaseException):
    def __init__(self) -> None:
        msg = "Exception - The URL root with a query can not have an excluded sub-path"
        super().__init__(msg)


class InvalidForcesStakeholder(CustomBaseException):
    def __init__(self) -> None:
        msg = "Exception - Invalid forces stakeholder"
        super().__init__(msg)


class MailmapEntryNotFound(CustomBaseException):
    def __init__(self, entry_email: str, organization_id: str) -> None:
        header = "Exception - Mailmap entry not found"
        data = f"{organization_id} - {entry_email}"
        msg = f"{header}: {data}"
        super().__init__(msg)


class MailmapSubentryNotFound(CustomBaseException):
    def __init__(
        self,
        entry_email: str,
        subentry_email: str,
        subentry_name: str,
        organization_id: str,
    ) -> None:
        # Split string just to achieve < 80 line size limit
        header = "Exception - Mailmap subentry not found"
        data1 = f"{organization_id} - {entry_email}"
        data2 = f"{subentry_email} - {subentry_name}"
        msg = f"{header}: {data1} - {data2}"
        super().__init__(msg)


class MailmapOrganizationNotFound(CustomBaseException):
    def __init__(
        self,
        organization_id: str,
    ) -> None:
        msg = f"Exception - Organization not found: {organization_id}"
        super().__init__(msg)


class NoMailmapSubentriesFound(CustomBaseException):  # pragma: no cover
    # logic guarantees that there is at least 1 subentry per entry
    def __init__(self, entry_email: str, organization_id: str) -> None:
        # Split string just to achieve < 80 line size limit
        header = "Exception - No mailmap subentries found"
        data = f"{organization_id} - {entry_email}"
        msg = f"{header}: {data}"
        super().__init__(msg)


class MailmapEntryAlreadyExists(CustomBaseException):
    def __init__(self, entry_email: str, organization_id: str) -> None:
        # Split string just to achieve < 80 line size limit
        header = "Exception - Mailmap entry already exists"
        data = f"{organization_id} - {entry_email}"
        msg = f"{header}: {data}"
        super().__init__(msg)


class MailmapSubentryAlreadyExists(CustomBaseException):
    def __init__(
        self,
        entry_email: str,
        subentry_email: str,
        subentry_name: str,
        organization_id: str,
    ) -> None:
        # Split string just to achieve < 80 line size limit
        header = "Exception - Mailmap subentry already exists"
        data1 = f"{organization_id} - {entry_email}"
        data2 = f"{subentry_email} - {subentry_name}"
        msg = f"{header}: {data1} - {data2}"
        super().__init__(msg)


class MailmapSubentryAlreadyExistsInOrganization(CustomBaseException):
    def __init__(
        self,
        subentry_email: str,
        subentry_name: str,
        organization_id: str,
    ) -> None:
        # Split string just to achieve < 80 line size limit
        header = "Exception - Mailmap subentry already exists in organization"
        data1 = f"{organization_id}"
        data2 = f"{subentry_email} - {subentry_name}"
        msg = f"{header}: {data1} - {data2}"
        super().__init__(msg)


class ConflictingMailmapMappingError(CustomBaseException):
    def __init__(self, mapping_issues: list[str]) -> None:
        header = "Exception - Conflicting mappings detected in mailmap file"
        data = f"{mapping_issues}"
        msg = f"{header}: {data}"
        super().__init__(msg)


class ErrorUpdatingRootItem(_SingleMessageException):
    msg: str = "Root item could not be updated"


class ErrorUpdatingSecret(_SingleMessageException):
    msg: str = "Secret could not be updated"


class InvalidAWSRoleTrustPolicy(CustomBaseException):
    def __init__(self) -> None:
        msg = "Exception - Cannot assume cross account IAM role"
        super().__init__(msg)


class ReattackNotRequested(CustomBaseException):
    def __init__(self) -> None:
        msg = "Exception - Reattack request was rejected"
        super().__init__(msg)


class SbomFileNotRequested(CustomBaseException):
    def __init__(self, custom_msg: str) -> None:
        msg = f"Exception - Sbom file request was rejected: {custom_msg}"
        super().__init__(msg)


class FileIsExcluded(CustomBaseException):
    def __init__(self) -> None:
        msg = "Exception - File is excluded"
        super().__init__(msg)


class InvalidRegexSearch(CustomBaseException):
    def __init__(self) -> None:
        msg = "Exception - The regex search is invalid"
        super().__init__(msg)


class TrustedDeviceNotFound(CustomBaseException):
    def __init__(self) -> None:
        msg = "Exception - Trusted device not found"
        super().__init__(msg)


class StakeholderPhoneNumberNotFound(CustomBaseException):
    """Exception to control stakeholder phone availability"""

    def __init__(self) -> None:
        """Constructor"""
        msg = "Stakeholder phone number not found"
        super().__init__(msg)


class UnfixableFinding(CustomBaseException):
    """Exception to control omitted findings in fixes"""

    def __init__(self) -> None:
        """Constructor"""
        msg = "Exception - Fixes are not enabled for this finding"
        super().__init__(msg)


class InvalidAWSMarketplaceProductCode(CustomBaseException):
    """Exception to handle wrong product code resolved from AWS Marketplace"""

    def __init__(self) -> None:
        msg = "Exception - AWS Marketplace product code is invalid"
        super().__init__(msg)


class PriorityScoreUpdateAlreadyRequested(CustomBaseException):
    def __init__(self) -> None:
        msg: str = "Exception - The user already modify the policies for the same organization"
        super().__init__(msg)


class BotoErrorException(Exception):
    pass


class ToePackageNotFound(CustomBaseException):
    """Exception to control toe package data availability"""

    def __init__(self) -> None:
        """Constructor"""
        msg = "Exception - Toe package not found"
        super().__init__(msg)


class TreatmentNotFound(CustomBaseException):
    def __init__(self) -> None:
        msg = "Exception - Treatment not found"
        super().__init__(msg)


class TreatabilityNotAllowed(CustomBaseException):
    def __init__(self) -> None:
        msg = "Exception - Treatability not allowed"
        super().__init__(msg)


class VulnSeverityScoreUpdateAlreadyRequested(CustomBaseException):
    def __init__(self) -> None:
        msg = "Exception - Severity score update already requested for vuln"
        super().__init__(msg)


class VulnSeverityScoreUpdateNotRequested(CustomBaseException):
    def __init__(self) -> None:
        msg = "Exception - Severity score update has not been requested yet"
        super().__init__(msg)


class InvalidStatusForSeverityUpdate(CustomBaseException):
    def __init__(self) -> None:
        msg = "Exception - Vulnerability severity update not available for current status"
        super().__init__(msg)


class InvalidStatusForSeverityUpdateRequest(CustomBaseException):
    def __init__(self) -> None:
        msg = "Exception - Vulnerability severity update request not available for current status"
        super().__init__(msg)


class ToePackageLocationNotFound(CustomBaseException):
    def __init__(self) -> None:
        msg = "Exception - Location not found in toe package"
        super().__init__(msg)


class VulnerabilityListMismatchError(Exception):
    """Exception to control when there is no correspondence between two lists of vulnerabilities."""

    def __init__(self) -> None:
        """Constructor"""
        msg = "Exception - Vulnerability lists do not match"
        super().__init__(msg)
