from integrates.dataloaders import Dataloaders, get_new_context
from integrates.db_model.groups.enums import GroupManaged
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    GroupAccessFaker,
    GroupAccessStateFaker,
    GroupFaker,
    GroupStateFaker,
)
from integrates.testing.mocks import mocks
from integrates.testing.utils import parametrize
from integrates.trials.domain import has_active_trial


@parametrize(
    args=["user_email", "expected_output"],
    cases=[
        [
            "without_groups_user@testdomain.com",
            False,
        ],
        [
            "without_trial_groups_user@testdomain.com",
            False,
        ],
        [
            "trial_group_user@testdomain.com",
            True,
        ],
        [
            "trial_group_under_review_user@testdomain.com",
            False,
        ],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            group_access=[
                GroupAccessFaker(
                    email="without_trial_groups_user@testdomain.com",
                    group_name="group1",
                    state=GroupAccessStateFaker(has_access=True),
                ),
                GroupAccessFaker(
                    email="trial_group_user@testdomain.com",
                    group_name="trial_group",
                    state=GroupAccessStateFaker(has_access=True),
                ),
                GroupAccessFaker(
                    email="trial_group_user@testdomain.com",
                    group_name="trial_group_under_review",
                    state=GroupAccessStateFaker(has_access=True),
                ),
            ],
            groups=[
                GroupFaker(
                    name="group1",
                ),
                GroupFaker(
                    name="trial_group",
                    created_by="trial_group_user@testdomain.com",
                    state=GroupStateFaker(
                        managed=GroupManaged.TRIAL,
                    ),
                ),
                GroupFaker(
                    name="trial_group_under_review",
                    created_by="trial_group_under_review_user@testdomain.com",
                    state=GroupStateFaker(
                        managed=GroupManaged.UNDER_REVIEW,
                    ),
                ),
            ],
        ),
    )
)
async def test_has_active_trial(
    user_email: str,
    expected_output: bool,
) -> None:
    loaders: Dataloaders = get_new_context()
    assert await has_active_trial(user_email, loaders) == expected_output
