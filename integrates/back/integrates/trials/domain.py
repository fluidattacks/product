from integrates.custom_utils.access import get_stakeholder_groups_names
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model import (
    trials as trials_model,
)
from integrates.db_model.groups.enums import (
    GroupManaged,
)
from integrates.db_model.marketplace.enums import (
    AWSMarketplaceSubscriptionStatus,
)
from integrates.db_model.organizations.types import (
    Organization,
)
from integrates.db_model.trials.types import (
    Trial,
    TrialMetadataToUpdate,
)

from .getters import (
    get_remaining_days,
)


def has_expired(trial: Trial) -> bool:
    return not trial.completed and trial.start_date is not None and get_remaining_days(trial) == 0


async def update_metadata(
    email: str,
    metadata: TrialMetadataToUpdate,
) -> None:
    await trials_model.update_metadata(
        email=email,
        metadata=metadata,
    )


async def in_trial(
    loaders: Dataloaders,
    user_email: str,
    organization: Organization | None = None,
) -> bool:
    has_trial = await should_have_free_trial(email=user_email, loaders=loaders)
    trial = await loaders.trial.load(user_email)

    if trial:
        has_trial = True
        if trial.completed and organization:
            has_trial = not bool(organization.payment_methods)

    return has_trial


async def should_have_free_trial(email: str, loaders: Dataloaders) -> bool:
    has_free_trial: bool = False
    user = await loaders.stakeholder.load(email)
    if user is not None:
        has_free_trial = not user.enrolled
        if user.aws_customer_id is not None:
            subscription = await loaders.aws_marketplace_subscriptions.load(user.aws_customer_id)
            if (
                subscription is not None
                and subscription.state.status == AWSMarketplaceSubscriptionStatus.ACTIVE
            ):
                has_free_trial = has_free_trial and subscription.state.has_free_trial

    return has_free_trial


async def has_active_trial(email: str, loaders: Dataloaders) -> bool:
    group_names = await get_stakeholder_groups_names(loaders, email, True)
    groups = await loaders.group.load_many(group_names)
    return any(
        group.state.managed == GroupManaged.TRIAL and group.created_by == email
        for group in groups
        if group is not None
    )
