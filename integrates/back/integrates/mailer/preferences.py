from collections.abc import (
    Iterable,
)
from typing import (
    NamedTuple,
    Optional,
    Union,
)

from integrates.custom_utils import (
    datetime as datetime_utils,
)
from integrates.db_model.enums import (
    Notification,
)
from integrates.mailer.types import ZTNAAlertContext


class PreferenceRole(NamedTuple):
    group: Iterable[str]
    org: Iterable[str]


ContextType = Union[type[ZTNAAlertContext]]


class NotificationPreferences(NamedTuple):
    email_preferences: Notification | None
    exclude_trial: bool
    only_fluid_staff: bool
    roles: PreferenceRole
    context_type: Optional[ContextType] = None


MAIL_PREFERENCES: dict[str, NotificationPreferences] = {
    "abandoned_trial": NotificationPreferences(
        email_preferences=None,
        exclude_trial=False,
        only_fluid_staff=False,
        roles=PreferenceRole(group={}, org={}),
    ),
    "abandoned_trial_2": NotificationPreferences(
        email_preferences=None,
        exclude_trial=False,
        only_fluid_staff=False,
        roles=PreferenceRole(group={}, org={}),
    ),
    "abandoned_trial_3": NotificationPreferences(
        email_preferences=None,
        exclude_trial=False,
        only_fluid_staff=False,
        roles=PreferenceRole(group={}, org={}),
    ),
    "access_granted": NotificationPreferences(
        email_preferences=None,
        exclude_trial=False,
        only_fluid_staff=False,
        roles=PreferenceRole(group={}, org={}),
    ),
    "confirm_deletion": NotificationPreferences(
        email_preferences=None,
        exclude_trial=False,
        only_fluid_staff=False,
        roles=PreferenceRole(group={}, org={}),
    ),
    "consulting_digest": NotificationPreferences(
        email_preferences=Notification.NEW_COMMENT,
        exclude_trial=False,
        only_fluid_staff=datetime_utils.get_now().hour > 12,
        roles=PreferenceRole(
            group={
                "admin",
                "architect",
                "customer_manager",
                "hacker",
                "reattacker",
                "resourcer",
                "reviewer",
                "service_forces",
                "user",
                "group_manager",
                "vulnerability_manager",
            },
            org={},
        ),
    ),
    "credit_card_added": NotificationPreferences(
        email_preferences=None,
        exclude_trial=False,
        only_fluid_staff=True,
        roles=PreferenceRole(group={}, org={}),
    ),
    "delete_finding": NotificationPreferences(
        email_preferences=None,
        exclude_trial=False,
        only_fluid_staff=False,
        roles=PreferenceRole(group={}, org={}),
    ),
    "deprecation_notice": NotificationPreferences(
        email_preferences=None,
        exclude_trial=True,
        only_fluid_staff=False,
        roles=PreferenceRole(group={}, org={}),
    ),
    "devsecops_agent_token": NotificationPreferences(
        email_preferences=Notification.AGENT_TOKEN,
        exclude_trial=False,
        only_fluid_staff=False,
        roles=PreferenceRole(group={"group_manager"}, org={}),
    ),
    "devsecops_expiration": NotificationPreferences(
        email_preferences=Notification.AGENT_TOKEN,
        exclude_trial=True,
        only_fluid_staff=False,
        roles=PreferenceRole(group={"group_manager"}, org={}),
    ),
    "environment_moved": NotificationPreferences(
        email_preferences=Notification.ROOT_UPDATE,
        exclude_trial=False,
        only_fluid_staff=False,
        roles=PreferenceRole(group={"admin", "customer_manager", "group_manager"}, org={}),
    ),
    "environment_report": NotificationPreferences(
        email_preferences=Notification.ROOT_UPDATE,
        exclude_trial=True,
        only_fluid_staff=False,
        roles=PreferenceRole(
            group={"customer_manager", "resourcer", "hacker", "group_manager"},
            org={},
        ),
    ),
    "event_report": NotificationPreferences(
        email_preferences=Notification.EVENT_REPORT,
        exclude_trial=False,
        only_fluid_staff=False,
        roles=PreferenceRole(
            group={
                "admin",
                "architect",
                "customer_manager",
                "hacker",
                "reattacker",
                "resourcer",
                "reviewer",
                "service_forces",
                "user",
                "group_manager",
                "vulnerability_manager",
            },
            org={},
        ),
    ),
    "events_digest": NotificationPreferences(
        email_preferences=Notification.EVENT_DIGEST,
        exclude_trial=False,
        only_fluid_staff=False,
        roles=PreferenceRole(
            group={
                "admin",
                "architect",
                "customer_manager",
                "hacker",
                "reattacker",
                "resourcer",
                "reviewer",
                "service_forces",
                "user",
                "group_manager",
                "vulnerability_manager",
            },
            org={},
        ),
    ),
    "evidence_rejection": NotificationPreferences(
        email_preferences=None,
        exclude_trial=True,
        only_fluid_staff=True,
        roles=PreferenceRole(group={}, org={}),
    ),
    "file_report": NotificationPreferences(
        email_preferences=Notification.FILE_UPDATE,
        exclude_trial=True,
        only_fluid_staff=False,
        roles=PreferenceRole(
            group={
                "customer_manager",
                "hacker",
                "resourcer",
                "group_manager",
            },
            org={},
        ),
    ),
    "free_trial": NotificationPreferences(
        email_preferences=None,
        exclude_trial=False,
        only_fluid_staff=False,
        roles=PreferenceRole(group={}, org={}),
    ),
    "free_trial_2": NotificationPreferences(
        email_preferences=None,
        exclude_trial=False,
        only_fluid_staff=False,
        roles=PreferenceRole(group={}, org={}),
    ),
    "free_trial_3": NotificationPreferences(
        email_preferences=None,
        exclude_trial=False,
        only_fluid_staff=False,
        roles=PreferenceRole(group={}, org={}),
    ),
    "free_trial_4": NotificationPreferences(
        email_preferences=None,
        exclude_trial=False,
        only_fluid_staff=False,
        roles=PreferenceRole(group={}, org={}),
    ),
    "free_trial_5": NotificationPreferences(
        email_preferences=None,
        exclude_trial=False,
        only_fluid_staff=False,
        roles=PreferenceRole(group={}, org={}),
    ),
    "free_trial_6": NotificationPreferences(
        email_preferences=None,
        exclude_trial=False,
        only_fluid_staff=False,
        roles=PreferenceRole(group={}, org={}),
    ),
    "free_trial_7": NotificationPreferences(
        email_preferences=None,
        exclude_trial=False,
        only_fluid_staff=False,
        roles=PreferenceRole(group={}, org={}),
    ),
    "free_trial_8": NotificationPreferences(
        email_preferences=None,
        exclude_trial=False,
        only_fluid_staff=False,
        roles=PreferenceRole(group={}, org={}),
    ),
    "free_trial_over": NotificationPreferences(
        email_preferences=None,
        exclude_trial=False,
        only_fluid_staff=False,
        roles=PreferenceRole(group={}, org={}),
    ),
    "group_added": NotificationPreferences(
        email_preferences=None,
        exclude_trial=True,
        only_fluid_staff=False,
        roles=PreferenceRole(group={}, org={}),
    ),
    "group_alert": NotificationPreferences(
        email_preferences=Notification.GROUP_INFORMATION,
        exclude_trial=False,
        only_fluid_staff=False,
        roles=PreferenceRole(group={"customer_manager", "group_manager"}, org={}),
    ),
    "group_report": NotificationPreferences(
        email_preferences=None,
        exclude_trial=False,
        only_fluid_staff=False,
        roles=PreferenceRole(group={}, org={}),
    ),
    "missing_environment": NotificationPreferences(
        email_preferences=Notification.ROOT_UPDATE,
        exclude_trial=False,
        only_fluid_staff=False,
        roles=PreferenceRole(
            group={
                "customer_manager",
                "group_manager",
                "vulnerability_manager",
            },
            org={},
        ),
    ),
    "missing_member": NotificationPreferences(
        email_preferences=None,
        exclude_trial=False,
        only_fluid_staff=False,
        roles=PreferenceRole(group={}, org={"group_manager"}),
    ),
    "new_comment": NotificationPreferences(
        email_preferences=Notification.NEW_COMMENT,
        exclude_trial=True,
        only_fluid_staff=False,
        roles=PreferenceRole(group={}, org={}),
    ),
    "new_enrolled": NotificationPreferences(
        email_preferences=None,
        exclude_trial=True,
        only_fluid_staff=False,
        roles=PreferenceRole(group={}, org={}),
    ),
    "new_sign_in_report": NotificationPreferences(
        email_preferences=None,
        exclude_trial=False,
        only_fluid_staff=False,
        roles=PreferenceRole(group={}, org={}),
    ),
    "numerator_digest": NotificationPreferences(
        email_preferences=None,
        exclude_trial=False,
        only_fluid_staff=False,
        roles=PreferenceRole(
            group={
                "hacker",
                "reattacker",
                "resourcer",
            },
            org={},
        ),
    ),
    "portfolio_report": NotificationPreferences(
        email_preferences=Notification.PORTFOLIO_UPDATE,
        exclude_trial=True,
        only_fluid_staff=False,
        roles=PreferenceRole(group={"customer_manager", "resourcer", "group_manager"}, org={}),
    ),
    "remediate_finding": NotificationPreferences(
        email_preferences=Notification.REMEDIATE_FINDING,
        exclude_trial=False,
        only_fluid_staff=False,
        roles=PreferenceRole(group={"reattacker"}, org={}),
    ),
    "reminder": NotificationPreferences(
        email_preferences=None,
        exclude_trial=True,
        only_fluid_staff=False,
        roles=PreferenceRole(group={}, org={}),
    ),
    "reviewer_progress_report": NotificationPreferences(
        email_preferences=None,
        exclude_trial=True,
        only_fluid_staff=True,
        roles=PreferenceRole(group={"reviewer"}, org={}),
    ),
    "root_added": NotificationPreferences(
        email_preferences=Notification.ROOT_UPDATE,
        exclude_trial=True,
        only_fluid_staff=False,
        roles=PreferenceRole(
            group={"customer_manager", "hacker", "resourcer", "group_manager"},
            org={},
        ),
    ),
    "root_cloning_status": NotificationPreferences(
        email_preferences=Notification.ROOT_UPDATE,
        exclude_trial=False,
        only_fluid_staff=False,
        roles=PreferenceRole(group={"customer_manager", "resourcer", "group_manager"}, org={}),
    ),
    "root_deactivated": NotificationPreferences(
        email_preferences=Notification.ROOT_UPDATE,
        exclude_trial=False,
        only_fluid_staff=False,
        roles=PreferenceRole(group={"customer_manager", "resourcer", "group_manager"}, org={}),
    ),
    "root_moved": NotificationPreferences(
        email_preferences=Notification.ROOT_UPDATE,
        exclude_trial=False,
        only_fluid_staff=False,
        roles=PreferenceRole(group={"customer_manager", "group_manager"}, org={}),
    ),
    "send_exported_file": NotificationPreferences(
        email_preferences=None,
        exclude_trial=False,
        only_fluid_staff=True,
        roles=PreferenceRole(group={}, org={}),
    ),
    "send_sbom_file": NotificationPreferences(
        email_preferences=None,
        exclude_trial=False,
        only_fluid_staff=True,
        roles=PreferenceRole(group={}, org={}),
    ),
    "treatment_report": NotificationPreferences(
        email_preferences=Notification.UPDATED_TREATMENT,
        exclude_trial=False,
        only_fluid_staff=False,
        roles=PreferenceRole(
            group={
                "customer_manager",
                "resourcer",
                "group_manager",
                "vulnerability_manager",
            },
            org={},
        ),
    ),
    "trial_first_scanning": NotificationPreferences(
        email_preferences=None,
        exclude_trial=False,
        only_fluid_staff=False,
        roles=PreferenceRole(group={}, org={}),
    ),
    "updated_group_info": NotificationPreferences(
        email_preferences=Notification.GROUP_INFORMATION,
        exclude_trial=True,
        only_fluid_staff=False,
        roles=PreferenceRole(group={"customer_manager", "resourcer", "group_manager"}, org={}),
    ),
    "updated_policies": NotificationPreferences(
        email_preferences=Notification.GROUP_INFORMATION,
        exclude_trial=True,
        only_fluid_staff=False,
        roles=PreferenceRole(
            group={"customer_manager", "group_manager"},
            org={"customer_manager", "organization_manager"},
        ),
    ),
    "updated_root": NotificationPreferences(
        email_preferences=Notification.ROOT_UPDATE,
        exclude_trial=True,
        only_fluid_staff=False,
        roles=PreferenceRole(group={"customer_manager", "resourcer", "group_manager"}, org={}),
    ),
    "updated_services": NotificationPreferences(
        email_preferences=Notification.SERVICE_UPDATE,
        exclude_trial=True,
        only_fluid_staff=False,
        roles=PreferenceRole(group={"customer_manager", "resourcer", "group_manager"}, org={}),
    ),
    "updated_treatment": NotificationPreferences(
        email_preferences=Notification.UPDATED_TREATMENT,
        exclude_trial=False,
        only_fluid_staff=False,
        roles=PreferenceRole(group={"vulnerability_manager", "group_manager"}, org={}),
    ),
    "user_unsubscribed": NotificationPreferences(
        email_preferences=Notification.UNSUBSCRIPTION_ALERT,
        exclude_trial=False,
        only_fluid_staff=False,
        roles=PreferenceRole(group={"customer_manager", "resourcer", "group_manager"}, org={}),
    ),
    "users_weekly_report": NotificationPreferences(
        email_preferences=Notification.GROUP_INFORMATION,
        exclude_trial=False,
        only_fluid_staff=False,
        roles=PreferenceRole(group={"customer_manager", "group_manager"}, org={}),
    ),
    "vulnerabilities_expiring": NotificationPreferences(
        email_preferences=Notification.UPDATED_TREATMENT,
        exclude_trial=False,
        only_fluid_staff=False,
        roles=PreferenceRole(
            group={
                "customer_manager",
                "resourcer",
                "group_manager",
                "vulnerability_manager",
            },
            org={},
        ),
    ),
    "vulnerability_assigned": NotificationPreferences(
        email_preferences=None,
        exclude_trial=False,
        only_fluid_staff=False,
        roles=PreferenceRole(group={}, org={}),
    ),
    "vulnerability_rejection": NotificationPreferences(
        email_preferences=None,
        exclude_trial=True,
        only_fluid_staff=True,
        roles=PreferenceRole(group={}, org={}),
    ),
    "vulnerability_report": NotificationPreferences(
        email_preferences=Notification.VULNERABILITY_REPORT,
        exclude_trial=False,
        only_fluid_staff=False,
        roles=PreferenceRole(
            group={
                "admin",
                "architect",
                "customer_manager",
                "hacker",
                "reattacker",
                "resourcer",
                "reviewer",
                "service_forces",
                "group_manager",
                "vulnerability_manager",
            },
            org={},
        ),
    ),
    "vulnerability_submission": NotificationPreferences(
        email_preferences=None,
        exclude_trial=True,
        only_fluid_staff=True,
        roles=PreferenceRole(group={}, org={}),
    ),
    "ztna_down": NotificationPreferences(
        email_preferences=None,
        exclude_trial=False,
        only_fluid_staff=False,
        roles=PreferenceRole(group={}, org={"customer_manager", "organization_manager"}),
        context_type=ZTNAAlertContext,
    ),
}
