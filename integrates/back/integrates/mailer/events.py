from typing import (
    Literal,
    TypedDict,
)

from integrates.context import (
    BASE_URL,
)
from integrates.custom_utils import (
    datetime as datetime_utils,
)
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.events.enums import (
    EventSolutionReason,
)
from integrates.db_model.events.types import (
    Event,
)
from integrates.db_model.roots.types import (
    GitRoot,
    RootRequest,
)
from integrates.mailer.utils import (
    get_group_emails_by_notification,
    get_organization_name,
)
from integrates.settings import (
    TIME_ZONE,
)

from .common import (
    send_mails_async,
)
from .types import EventReportContext


class PrepareEmailContext(TypedDict):
    context: EventReportContext
    email_to: list[str]
    subject: str


TSubject = Literal["solved", "reported", "persistent"]

EVENT_TYPE_FORMAT = {
    "AUTHORIZATION_SPECIAL_ATTACK": "Authorization for a special attack",
    "CLIENT_CANCELS_PROJECT_MILESTONE": ("The client cancels a project milestone"),
    "CLIENT_EXPLICITLY_SUSPENDS_PROJECT": ("The client suspends the project"),
    "CLONING_ISSUES": "Cloning issues",
    "CREDENTIAL_ISSUES": "Credentials issues",
    "DATA_UPDATE_REQUIRED": "Request user modification/workflow update",
    "ENVIRONMENT_ISSUES": "Environment issues",
    "INSTALLER_ISSUES": "Installer issues",
    "MOVED_TO_ANOTHER_GROUP": "Moved to another group",
    "MISSING_SUPPLIES": "Missing supplies",
    "NETWORK_ACCESS_ISSUES": "Network access issues",
    "OTHER": "Other",
    "REMOTE_ACCESS_ISSUES": "Remote access issues",
    "TOE_DIFFERS_APPROVED": "ToE different than agreed upon",
    "VPN_ISSUES": "VPN issues",
}


async def _get_root_url(root_id: str | None, group_name: str, loaders: Dataloaders) -> str | None:
    root = await loaders.root.load(RootRequest(group_name, root_id)) if root_id else None
    root_url: str | None = root.state.url if root and isinstance(root, GitRoot) else None
    return root_url


async def _get_email_context(
    *,
    loaders: Dataloaders,
    event: Event,
    subject: TSubject,
    reason_format: str,
    reminder_notification: bool,
) -> EventReportContext:
    group_name = event.group_name.lower()
    event_age: int = (
        datetime_utils.get_now().date() - datetime_utils.as_zone(event.event_date, TIME_ZONE).date()
    ).days
    org_name = await get_organization_name(loaders, group_name)
    root_url = await _get_root_url(event.root_id, group_name, loaders)
    email_context: EventReportContext = {
        "group": group_name,
        "event_type": EVENT_TYPE_FORMAT[event.type.value],
        "description": event.description.strip("."),
        "event_age": event_age,
        "event_url": (
            f"{BASE_URL}/orgs/{org_name}/groups/{group_name}/events/{event.id}/description"
        ),
        "n_holds": event.n_holds,
        "reason": reason_format,
        "reminder_notification": reminder_notification,
        "report_date": str(event.event_date.date()),
        "root_url": root_url,
        "state": subject,
    }

    return email_context


async def prepare_email_context(
    *,
    loaders: Dataloaders,
    event: Event,
    subject: TSubject,
    reminder_notification: bool,
    reason_format: str,
) -> PrepareEmailContext:
    group_name = event.group_name.lower()
    email_context = await _get_email_context(
        subject=subject,
        loaders=loaders,
        event=event,
        reason_format=reason_format,
        reminder_notification=reminder_notification,
    )

    stakeholders_email = await get_group_emails_by_notification(
        loaders=loaders,
        group_name=group_name,
        notification="event_report",
    )
    if reminder_notification:
        stakeholders_email.extend(
            await get_group_emails_by_notification(
                loaders=loaders,
                group_name=group_name,
                notification="events_digest",
            ),
        )

    subjects: dict[TSubject, str] = {
        "solved": f"Event #[{event.id}] was solved in [{group_name}]",
        "reported": "ACTION NEEDED: Your group " + f"[{group_name}] is at risk",
        "persistent": f"Event #[{event.id}] was reported as persistent on [{group_name}]",
    }

    return {
        "context": email_context,
        "email_to": list(set(stakeholders_email)),
        "subject": subjects[subject],
    }


async def send_mail_event_report(
    *,
    loaders: Dataloaders,
    event: Event,
    justification: EventSolutionReason | str = "",
    subject: TSubject = "reported",
    reminder_notification: bool = False,
) -> None:
    reason_format = justification
    if isinstance(justification, EventSolutionReason):
        reason_format = str(justification).replace("_", " ").capitalize()

    email_context = await prepare_email_context(
        loaders=loaders,
        subject=subject,
        reminder_notification=reminder_notification,
        event=event,
        reason_format=reason_format,
    )
    await send_mails_async(
        loaders=loaders,
        template_name="event_report",
        context=dict(email_context["context"]),
        subject=email_context["subject"],
        email_to=email_context["email_to"],
    )
