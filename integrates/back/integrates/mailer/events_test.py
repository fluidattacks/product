import uuid
from datetime import datetime

import integrates.mailer.common as mailer_common
from integrates.context import (
    BASE_URL,
)
from integrates.dataloaders import get_new_context
from integrates.db_model.enums import (
    Notification,
)
from integrates.db_model.events.types import EventRequest
from integrates.db_model.stakeholders.types import (
    NotificationsPreferences,
)
from integrates.mailer.events import (
    _get_email_context,
    prepare_email_context,
    send_mail_event_report,
)
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    EventFaker,
    GroupAccessFaker,
    GroupAccessStateFaker,
    GroupFaker,
    OrganizationFaker,
    StakeholderFaker,
    StakeholderStateFaker,
)
from integrates.testing.mocks import Mock, mocks
from integrates.testing.utils import freeze_time

DATE_2019 = datetime.fromisoformat("2019-04-10T12:59:52+00:00")
EVENT_ID = str(uuid.uuid4())
ORGANIZATION_ID = str(uuid.uuid4())


@freeze_time("2024-10-14T00:00:00")
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            events=[
                EventFaker(
                    id=EVENT_ID,
                    group_name="group_name",
                    description="description",
                    event_date=DATE_2019,
                )
            ],
            groups=[GroupFaker(name="group_name", organization_id=ORGANIZATION_ID)],
            organizations=[OrganizationFaker(id=ORGANIZATION_ID)],
        ),
    ),
)
async def test_get_email_context() -> None:
    loaders = get_new_context()
    event = await loaders.event.load(EventRequest(event_id=EVENT_ID, group_name="group_name"))

    assert event

    email_context = await _get_email_context(
        loaders=loaders,
        event=event,
        subject="persistent",
        reason_format="reason_format",
        reminder_notification=True,
    )

    assert email_context == {
        "group": "group_name",
        "event_type": "Environment issues",
        "description": "description",
        "event_age": 2013,
        "event_url": f"{BASE_URL}/orgs/test-org/groups/group_name/events/{EVENT_ID}/description",
        "n_holds": None,
        "reason": "reason_format",
        "reminder_notification": True,
        "report_date": str(DATE_2019.date()),
        "root_url": None,
        "state": "persistent",
    }


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            events=[
                EventFaker(
                    id=EVENT_ID,
                    group_name="group_name",
                    description="description",
                    event_date=DATE_2019,
                )
            ],
            groups=[GroupFaker(name="group_name", organization_id=ORGANIZATION_ID)],
            organizations=[OrganizationFaker(id=ORGANIZATION_ID)],
            stakeholders=[
                StakeholderFaker(
                    email="jdoe@company.com",
                    state=StakeholderStateFaker(
                        notifications_preferences=NotificationsPreferences(
                            email=[Notification.EVENT_REPORT]
                        ),
                    ),
                )
            ],
            group_access=[
                GroupAccessFaker(
                    group_name="group_name",
                    email="jdoe@company.com",
                    state=GroupAccessStateFaker(role="user"),
                )
            ],
        ),
    ),
    others=[Mock(mailer_common, "send_mail_async", "async", False)],
)
async def test_prepare_email_context() -> None:
    loaders = get_new_context()
    event = await loaders.event.load(EventRequest(event_id=EVENT_ID, group_name="group_name"))

    assert event

    context = await prepare_email_context(
        loaders=loaders,
        event=event,
        subject="reported",
        reason_format="reason_format",
        reminder_notification=False,
    )
    await send_mail_event_report(
        loaders=loaders,
        event=event,
        justification="justification",
        subject="reported",
        reminder_notification=True,
    )

    assert context["email_to"] == ["jdoe@company.com"]
    assert context["subject"] == "ACTION NEEDED: Your group [group_name] is at risk"
