import logging
import logging.config
from collections.abc import (
    Callable,
)
from datetime import (
    datetime,
)
from html import (
    escape,
)
from http.client import (
    RemoteDisconnected,
)
from typing import (
    Any,
)

from aioextensions import (
    collect,
    in_thread,
)
from jinja2 import (
    Environment,
    FileSystemLoader,
)
from mjml import (
    mjml2html,
)
from requests.exceptions import (
    ConnectionError as RequestConnectionError,
)
from sendgrid import (
    SendGridAPIClient,
)
from sendgrid.helpers.mail import (
    Attachment,
    Cc,
    Content,
    Email,
    Mail,
    MailSettings,
    Personalization,
    SandBoxMode,
    To,
)
from simplejson.errors import (
    JSONDecodeError,
)

from integrates.authz import (
    FLUID_IDENTIFIER,
)
from integrates.class_types.types import (
    Item,
)
from integrates.context import (
    BASE_URL,
    FI_EMAIL_TEMPLATES,
    FI_ENVIRONMENT,
    FI_MAIL_CONTINUOUS,
    FI_MAIL_COS,
    FI_MAIL_CTO,
    FI_MAIL_CUSTOMER_EXPERIENCE,
    FI_MAIL_CUSTOMER_SUCCESS,
    FI_MAIL_FINANCE,
    FI_MAIL_PRODUCTION,
    FI_MAIL_PROFILING,
    FI_MAIL_PROJECTS,
    FI_MAIL_REVIEWERS,
    FI_MAIL_TELEMARKETING,
    FI_SENDGRID_API_KEY,
    FI_TEST_PROJECTS,
)
from integrates.custom_exceptions import (
    MailerClientError,
    UnableToSendMail,
)
from integrates.custom_utils import (
    datetime as datetime_utils,
)
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.stakeholders.types import (
    Stakeholder,
)
from integrates.sessions import (
    domain as sessions_domain,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)


# Constants
LOGGER_ERRORS = logging.getLogger(__name__)
TRANSACTIONS_LOGGER: logging.Logger = logging.getLogger("transactional")
TEMPLATES = Environment(
    autoescape=True,
    loader=FileSystemLoader(FI_EMAIL_TEMPLATES),
    lstrip_blocks=True,
    trim_blocks=True,
)
VERIFY_TAG: list[str] = ["verify"]
BATCH_SIZE: int = 50


def create_instance_context() -> Callable[[], SendGridAPIClient]:
    client_cache: SendGridAPIClient | None = None

    def instance() -> SendGridAPIClient:
        nonlocal client_cache

        if client_cache is None:
            client_cache = SendGridAPIClient(FI_SENDGRID_API_KEY)
        return client_cache

    return instance


get_instance = create_instance_context()


def _escape_email_context(context: dict[str, Any]) -> dict[str, Any]:
    return {
        key: escape(value)
        if isinstance(value, str) and key not in ["filelink", "download_url"]
        else value
        for key, value in context.items()
    }


def get_content(template_name: str, context: dict[str, Any]) -> str:
    template = TEMPLATES.get_template(f"{template_name}.mjml")
    jinja_render = template.render(_escape_email_context(context))
    return mjml2html(jinja_render)


def _get_constant_emails() -> set[str]:
    fi_mail_lists = [
        FI_MAIL_CONTINUOUS,
        FI_MAIL_COS,
        FI_MAIL_CTO,
        FI_MAIL_CUSTOMER_EXPERIENCE,
        FI_MAIL_CUSTOMER_SUCCESS,
        FI_MAIL_PRODUCTION,
        FI_MAIL_REVIEWERS,
        FI_MAIL_PROFILING,
        FI_MAIL_FINANCE,
        FI_MAIL_PROJECTS,
        FI_MAIL_TELEMARKETING,
    ]

    constant_emails = {
        email.lower()
        for email_list in [emails.split(",") for emails in fi_mail_lists]
        for email in email_list
    }

    return constant_emails


def create_constant_emails_context() -> Callable[[], set[str]]:
    constant_emails: set[str] | None = None

    def constant_emails_singleton() -> set[str]:
        nonlocal constant_emails

        if constant_emails is None:
            constant_emails = _get_constant_emails()
        return constant_emails

    return constant_emails_singleton


get_constant_emails_singleton = create_constant_emails_context()


def _get_stakeholder_first_name(
    constant_emails: set[str],
    first_name: str,
    stakeholder: Stakeholder | None,
    email: str,
    is_access_granted: bool = False,
) -> str | None:
    is_registered = bool(stakeholder.is_registered) if stakeholder else False

    if email.lower() in constant_emails or is_registered or is_access_granted:
        return (
            str(stakeholder.first_name).title()
            if stakeholder and stakeholder.first_name
            else str(first_name)
        )
    return None


async def get_recipient_first_name(
    loaders: Dataloaders,
    email: str,
    is_access_granted: bool = False,
) -> str | None:
    first_name = email.split("@")[0]
    stakeholder = await loaders.stakeholder.load(email)
    constant_emails = get_constant_emails_singleton()

    return _get_stakeholder_first_name(
        constant_emails=constant_emails,
        first_name=first_name,
        stakeholder=stakeholder,
        email=email,
        is_access_granted=is_access_granted,
    )


async def get_recipients_first_name(
    loaders: Dataloaders,
    emails: list[str],
    is_access_granted: bool = False,
) -> Item:
    emails = [email for recipients in emails for email in recipients.split(",")]
    first_names = {}
    stakeholders = await loaders.stakeholder.load_many(emails)
    constant_emails = _get_constant_emails()

    for email, stakeholder in zip(emails, stakeholders, strict=False):
        if name := _get_stakeholder_first_name(
            constant_emails=constant_emails,
            first_name=email.split("@")[0],
            stakeholder=stakeholder,
            email=email,
            is_access_granted=is_access_granted,
        ):
            first_names[email] = name

    return first_names


def _get_unsubscription_url(user_email: str) -> str:
    url_token = sessions_domain.encode_token(
        expiration_time=None,
        payload={
            "user_email": user_email,
        },
        subject="unsubscribe",
    )
    unsubscribe_url = f"{BASE_URL}/unsubscribe/{url_token}"
    return unsubscribe_url


def _process_content(
    email_to: str,
    first_name: str,
    context: dict[str, Any],
    template_name: str,
) -> str:
    year = datetime_utils.get_as_str(datetime_utils.get_now(), "%Y")
    context["name"] = first_name
    context["year"] = year
    context["mailto"] = email_to
    context["unsubscribe_url"] = _get_unsubscription_url(email_to)
    content = get_content(template_name, context).replace("{{", r"\{{")

    return content


def _create_attachments(attachments: list[dict[str, str]]) -> list[Attachment]:
    attachment_list = []
    for attachment in attachments:
        attach = Attachment()
        attach.file_content = attachment["content"]
        attach.file_name = attachment["name"]
        attach.file_type = attachment["type"]
        attach.disposition = "attachment"
        attachment_list.append(attach)
    return attachment_list


def _add_mail_settings(message: Mail) -> None:
    if FI_ENVIRONMENT != "production":
        mail_settings = MailSettings()
        mail_settings.sandbox_mode = SandBoxMode(True)
        message.mail_settings = mail_settings


def _add_recipients(
    *,
    email_cc: dict[str, str] | None,
    email_to: str,
    first_name: str,
    personalization: Personalization,
) -> None:
    personalization.add_to(To(email=email_to, name=first_name))

    if email_cc:
        for email, name in email_cc.items():
            personalization.add_cc(Cc(email=email, name=name))


def _add_attachments(message: Mail, context: dict[str, Any]) -> None:
    if "attachments" in context:
        attachment_list = _create_attachments(context["attachments"])
        for attach in attachment_list:
            message.add_attachment(attach)


def _generate_email_message(
    *,
    email_to: str,
    first_name: str,
    email_cc: dict[str, str] | None = None,
    context: dict[str, Any],
    subject: str,
    template_name: str,
) -> Mail:
    content = _process_content(email_to, first_name, context, template_name)
    message = Mail(
        from_email=Email("noreply@fluidattacks.com", "Fluid Attacks"),
        html_content=Content("text/html", content),
        subject=subject,
    )

    _add_mail_settings(message)

    personalization = Personalization()
    _add_recipients(
        email_cc=email_cc,
        email_to=email_to,
        first_name=first_name,
        personalization=personalization,
    )

    message.add_personalization(personalization)

    _add_attachments(message, context)

    return message


async def send_mail_async(
    *,
    email_to: str,
    first_name: str,
    email_cc: dict[str, str] | None = None,
    context: dict[str, Any],
    subject: str,
    template_name: str,
) -> None:
    sendgrid_client = get_instance()
    if email_to.startswith("forces.") and email_to.endswith(FLUID_IDENTIFIER):
        return

    message = _generate_email_message(
        email_to=email_to,
        first_name=first_name,
        email_cc=email_cc,
        context=context,
        subject=subject,
        template_name=template_name,
    )

    try:
        response = await in_thread(sendgrid_client.send, message)
        TRANSACTIONS_LOGGER.info(
            "Mail sent",
            extra={
                "email_to": email_to,
                "template": template_name,
                "subject": subject,
                "response": response.status_code,
                "log_type": "Mailer",
            },
        )
    except (
        JSONDecodeError,
        RemoteDisconnected,
        RequestConnectionError,
    ) as ex:
        LOGGER_ERRORS.exception(
            "A connection error occurred while sending mail",
            extra={
                "extra": {
                    "email_to": email_to,
                    "template": template_name,
                    "subject": subject,
                    "context": context,
                },
            },
        )
        raise UnableToSendMail() from ex
    except Exception as ex:
        LOGGER_ERRORS.exception(
            "An error occurred while sending mail",
            extra={
                "extra": {
                    "email_to": email_to,
                    "template": template_name,
                    "subject": subject,
                    "context": context,
                    "exception": ex,
                    "Message": message,
                },
            },
        )
        raise MailerClientError() from ex


async def send_mails_async(
    *,
    loaders: Dataloaders,
    email_to: list[str],
    subject: str,
    template_name: str,
    context: dict[str, Any] | None = None,
    email_cc: list[str] | None = None,
    is_access_granted: bool = False,
) -> None:
    test_group_list = FI_TEST_PROJECTS.split(",") if FI_TEST_PROJECTS else []
    context = context or {}
    group = str(context.get("group", context.get("group_name", ""))).lower()

    if group in test_group_list:
        return

    recipients_cc: Item | None = (
        await get_recipients_first_name(loaders, email_cc, is_access_granted=is_access_granted)
        if email_cc
        else None
    )

    async def send_batch(email_batch: list[str]) -> None:
        recipients = await get_recipients_first_name(
            loaders,
            email_batch,
            is_access_granted=is_access_granted,
        )

        await collect(
            tuple(
                send_mail_async(
                    email_to=email,
                    first_name=first_name,
                    email_cc=recipients_cc,
                    context=context,
                    subject=subject,
                    template_name=template_name,
                )
                for email, first_name in recipients.items()
                if first_name
            ),
        )

    await collect(
        tuple(
            send_batch(email_to[i : i + BATCH_SIZE]) for i in range(0, len(email_to), BATCH_SIZE)
        ),
    )


async def send_mail_confirm_deletion(
    loaders: Dataloaders,
    email_to: list[str],
    context: dict[str, Any],
) -> None:
    await send_mails_async(
        loaders=loaders,
        email_to=email_to,
        context=context,
        subject="Confirm account deletion",
        template_name="confirm_deletion",
    )


async def report_login_activity(
    *,
    loaders: Dataloaders,
    browser: str,
    ip_address: str,
    location: str,
    operating_system: str,
    report_date: datetime,
    user_email: str,
) -> None:
    template_name = "new_sign_in_report"
    context = {
        "location": location,
        "ip_address": ip_address,
        "browser": browser,
        "operating_system": operating_system,
        "report_date": report_date.strftime("on %Y/%m/%d at %H:%M:%S"),
    }
    try:
        await send_mails_async(
            context=context,
            email_to=[user_email],
            loaders=loaders,
            subject="Login to your account",
            template_name=template_name,
        )
    except (UnableToSendMail, MailerClientError) as ex:
        LOGGER_ERRORS.exception(
            "An error ocurred while sending email",
            extra={
                "extra": {
                    "exception": ex,
                    "email_to": user_email,
                    "template": template_name,
                },
            },
        )


async def unsubscribe(email: str) -> None:
    sendgrid_client = get_instance()
    data = {"recipient_emails": [email]}
    try:
        await in_thread(
            sendgrid_client.client.asm.suppressions._("global").post,
            request_body=data,
        )
    except Exception as ex:
        raise MailerClientError() from ex
