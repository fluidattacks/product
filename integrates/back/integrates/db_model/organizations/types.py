from datetime import (
    datetime,
)
from decimal import (
    Decimal,
)
from typing import (
    NamedTuple,
)

from integrates.db_model.organizations.enums import (
    OrganizationStateStatus,
)
from integrates.db_model.types import (
    Policies,
)


class DocumentFile(NamedTuple):
    file_name: str
    modified_date: datetime


class OrganizationDocuments(NamedTuple):
    rut: DocumentFile | None = None
    tax_id: DocumentFile | None = None


class OrganizationPaymentMethods(NamedTuple):
    id: str
    business_name: str
    email: str
    country: str
    state: str
    city: str
    documents: OrganizationDocuments


class OrganizationPriorityPolicy(NamedTuple):
    policy: str
    value: int


class OrganizationState(NamedTuple):
    aws_external_id: str
    status: OrganizationStateStatus
    modified_by: str
    modified_date: datetime
    pending_deletion_date: datetime | None = None


class OrganizationStandardCompliance(NamedTuple):
    standard_name: str
    compliance_level: Decimal


class OrganizationUnreliableIndicators(NamedTuple):
    covered_authors: int | None = None
    covered_repositories: int | None = None
    has_ztna_roots: bool | None = None
    missed_authors: int | None = None
    missed_repositories: int | None = None
    compliance_level: Decimal | None = None
    compliance_weekly_trend: Decimal | None = None
    estimated_days_to_full_compliance: Decimal | None = None
    standard_compliances: list[OrganizationStandardCompliance] | None = None


class OrganizationBilling(NamedTuple):
    billing_email: str
    customer_id: str
    last_modified_by: str
    modified_at: datetime
    billed_groups: list[str] | None = None
    subscription_id: int | None = None


class Organization(NamedTuple):
    created_by: str
    created_date: datetime | None
    id: str
    name: str
    policies: Policies
    state: OrganizationState
    country: str
    jira_associated_id: str | None = None
    payment_methods: list[OrganizationPaymentMethods] | None = None
    priority_policies: list[OrganizationPriorityPolicy] | None = None
    billing_information: list[OrganizationBilling] | None = None
    vulnerabilities_pathfile: str | None = None


class OrganizationMetadataToUpdate(NamedTuple):
    billing_information: list[OrganizationBilling] | None = None
    business_id: str | None = None
    business_name: str | None = None
    country: str | None = None
    jira_associated_id: str | None = None
    payment_methods: list[OrganizationPaymentMethods] | None = None
    priority_policies: list[OrganizationPriorityPolicy] | None = None
    vulnerabilities_pathfile: str | None = None


class OrganizationUnreliableIndicatorsToUpdate(NamedTuple):
    covered_authors: int | None = None
    covered_repositories: int | None = None
    has_ztna_roots: bool | None = None
    missed_authors: int | None = None
    missed_repositories: int | None = None
    compliance_level: Decimal | None = None
    compliance_weekly_trend: Decimal | None = None
    estimated_days_to_full_compliance: Decimal | None = None
    standard_compliances: list[OrganizationStandardCompliance] | None = None
