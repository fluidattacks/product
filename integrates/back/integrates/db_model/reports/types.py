from datetime import (
    datetime,
)
from typing import (
    Literal,
)

from pydantic import (
    UUID4,
    BaseModel,
    EmailStr,
)

TReportOrigin = Literal["billing"]
TReportStatus = Literal["open", "closed", "reviewing", "deleted"]


class ReportState(BaseModel):
    modified_at: datetime
    modified_by: EmailStr
    status: TReportStatus
    status_reason: str


class Report(BaseModel):
    entity_id: UUID4 | str
    origin: TReportOrigin
    message: str
    state: ReportState
