from datetime import (
    date,
)

from boto3.dynamodb.conditions import (
    Key,
)

from integrates.db_model import (
    TABLE,
)
from integrates.db_model.reports.types import (
    Report,
    TReportOrigin,
)
from integrates.dynamodb import (
    keys,
    operations,
)


async def _get_organization_reports(
    *,
    entity_id: str,
    origin: TReportOrigin,
    report_date: date,
) -> list[Report]:
    """
    sk: YYYY-MM-DD#iso8601utc_date#REPORT_ID#report_id
    pk: REPORT_FOR#entity_id#ORIGIN#origin
    """
    primary_key = keys.build_key(
        facet=TABLE.facets["reports_metadata"],
        values={
            "entity_id": entity_id,
            "origin": origin,
            "iso8601utc_date": report_date.isoformat(),
        },
    )
    key_structure = TABLE.primary_key
    response = await operations.query(
        condition_expression=(
            Key(key_structure.partition_key).eq(primary_key.partition_key)
            & Key(key_structure.sort_key).begins_with(primary_key.sort_key)
        ),
        facets=(TABLE.facets["reports_metadata"],),
        table=TABLE,
    )

    return [Report(**item) for item in response.items]
