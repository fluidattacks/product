from datetime import (
    date,
    datetime,
)
from typing import (
    NamedTuple,
)

from integrates.db_model.types import (
    Edge,
)
from integrates.dynamodb.types import (
    FilterExpression,
    PageInfo,
)


class SortsSuggestion(NamedTuple):
    finding_title: str
    probability: int


class ToeLineState(NamedTuple):
    attacked_at: datetime | None
    attacked_by: str
    attacked_lines: int
    be_present: bool
    be_present_until: datetime | None
    comments: str
    first_attack_at: datetime | None
    has_vulnerabilities: bool
    last_author: str
    last_commit: str
    last_commit_date: datetime
    loc: int
    modified_by: str
    modified_date: datetime
    seen_at: datetime
    sorts_risk_level: int
    sorts_priority_factor: int | None = None
    sorts_risk_level_date: datetime | None = None
    sorts_suggestions: list[SortsSuggestion] | None = None


class ToeLineEnrichedState(NamedTuple):
    filename: str
    group_name: str
    root_id: str
    state: ToeLineState


class ToeLine(NamedTuple):
    filename: str
    group_name: str
    root_id: str
    state: ToeLineState
    seen_first_time_by: str | None = None


class ToeLineEdge(NamedTuple):
    node: ToeLine
    cursor: str


class ToeLinesConnection(NamedTuple):
    edges: tuple[ToeLineEdge, ...]
    page_info: PageInfo
    total: int | None = None


class ToeLinesEnrichedStatesConnection(NamedTuple):
    edges: tuple[Edge[ToeLineEnrichedState], ...]
    page_info: PageInfo


class ToeLineRequest(NamedTuple):
    filename: str
    group_name: str
    root_id: str


class GroupHistoricToeLinesRequest(NamedTuple):
    group_name: str
    filters: FilterExpression | None = None
    after: str | None = None
    first: int | None = None
    paginate: bool = False
    start_date: date | None = None
    end_date: date | None = None


class GroupToeLinesRequest(NamedTuple):
    group_name: str
    after: str | None = None
    be_present: bool | None = None
    first: int | None = None
    paginate: bool = False


class RootToeLinesRequest(NamedTuple):
    group_name: str
    root_id: str
    after: str | None = None
    be_present: bool | None = None
    first: int | None = None
    paginate: bool = False
