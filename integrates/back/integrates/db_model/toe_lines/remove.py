from collections.abc import Iterable

from boto3.dynamodb.conditions import Key
from snowflake.connector.cursor import SnowflakeCursor

from integrates.archive.domain import archive_group_toe_lines
from integrates.audit import AuditEvent, add_audit_event
from integrates.class_types.types import Item
from integrates.context import FI_ENVIRONMENT
from integrates.db_model import TABLE
from integrates.db_model.utils import archive
from integrates.dynamodb import keys, operations
from integrates.dynamodb.types import PrimaryKey


async def _remove_toe_lines(items: Iterable[Item]) -> None:
    key_structure = TABLE.primary_key
    await operations.batch_delete_item(
        keys=tuple(
            PrimaryKey(
                partition_key=item[key_structure.partition_key],
                sort_key=item[key_structure.sort_key],
            )
            for item in items
        ),
        table=TABLE,
    )
    for item in items:
        add_audit_event(
            AuditEvent(
                action="DELETE",
                author="unknown",
                metadata={},
                object="ToeLine",
                object_id=item[key_structure.sort_key],
            )
        )


async def remove_toe_lines(cursor: SnowflakeCursor, items: Iterable[Item]) -> None:
    await archive_group_toe_lines(cursor, items)
    await _remove_toe_lines(items)


async def remove_group_toe_lines(*, group_name: str) -> None:
    facet = TABLE.facets["toe_lines_metadata"]
    primary_key = keys.build_key(
        facet=facet,
        values={"group_name": group_name},
    )
    key_structure = TABLE.primary_key
    response = await operations.query(
        condition_expression=(
            Key(key_structure.partition_key).eq(primary_key.partition_key)
            & Key(key_structure.sort_key).begins_with(primary_key.sort_key.replace("#FILENAME", ""))
        ),
        facets=(facet,),
        table=TABLE,
    )
    if not response.items:
        return

    await archive(response.items, remove_toe_lines)

    if FI_ENVIRONMENT != "production":
        await _remove_toe_lines(response.items)
