import simplejson as json
from boto3.dynamodb.conditions import (
    Attr,
)

from integrates.audit import AuditEvent, add_audit_event
from integrates.class_types.types import (
    Item,
)
from integrates.custom_exceptions import (
    ToeLinesAlreadyUpdated,
)
from integrates.db_model import (
    TABLE,
)
from integrates.db_model.toe_lines.constants import (
    GSI_2_FACET,
)
from integrates.db_model.toe_lines.types import (
    ToeLine,
    ToeLineState,
)
from integrates.db_model.utils import (
    get_as_utc_iso_format,
    serialize,
)
from integrates.dynamodb import (
    keys,
    operations,
)
from integrates.dynamodb.exceptions import (
    ConditionalCheckFailedException,
)


async def update_state(
    *,
    current_value: ToeLine,
    new_state: ToeLineState,
) -> None:
    key_structure = TABLE.primary_key
    gsi_2_index = TABLE.indexes["gsi_2"]
    metadata_key = keys.build_key(
        facet=TABLE.facets["toe_lines_metadata"],
        values={
            "filename": current_value.filename,
            "group_name": current_value.group_name,
            "root_id": current_value.root_id,
        },
    )
    new_state_item: Item = json.loads(json.dumps(new_state, default=serialize))

    condition_expression = Attr(key_structure.partition_key).exists() & Attr(
        "state.modified_date",
    ).eq(get_as_utc_iso_format(current_value.state.modified_date))

    gsi_2_key = keys.build_key(
        facet=GSI_2_FACET,
        values={
            "be_present": str(new_state.be_present).lower(),
            "filename": current_value.filename,
            "group_name": current_value.group_name,
            "root_id": current_value.root_id,
        },
    )
    gsi_2_index = TABLE.indexes["gsi_2"]
    metadata_item = {
        gsi_2_index.primary_key.sort_key: gsi_2_key.sort_key,
        "state": new_state_item,
    }
    try:
        await operations.update_item(
            condition_expression=condition_expression,
            item=metadata_item,
            key=metadata_key,
            table=TABLE,
        )
        add_audit_event(
            AuditEvent(
                action="UPDATE",
                author=new_state.modified_by,
                metadata=metadata_item,
                object="ToeLine",
                object_id=metadata_key.sort_key,
            )
        )
    except ConditionalCheckFailedException as ex:
        raise ToeLinesAlreadyUpdated() from ex
