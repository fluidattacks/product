import itertools

from integrates.class_types.types import (
    Item,
)
from integrates.custom_exceptions import (
    InvalidCriteriaRange,
    InvalidParameter,
)
from integrates.db_model.criteria.types import (
    Criteria,
    CriteriaBaseScore,
    CriteriaBaseScoreV4,
    CriteriaMainData,
    CriteriaScore,
    CriteriaScoreV4,
    CriteriaTemporalScore,
    CriteriaThreatScoreV4,
)


def split_vulns(vulns: dict, after: str) -> dict:
    splitted_vulns = {key: item for key, item in vulns.items() if key > after}
    return splitted_vulns


def prepare_vulns_info(first: int | None, after: str, vulns_info: dict) -> tuple[dict, str, bool]:
    min_items = 1
    if first is not None and first < min_items:
        expr = f'The "first" field must be greater than {min_items}'
        raise InvalidCriteriaRange(expr=expr)
    if after != "" and vulns_info.get(after) is None:
        raise InvalidParameter(field="after")

    splitted_vulns = split_vulns(vulns=vulns_info, after=after)
    sliced_vulns = (
        splitted_vulns if first is None else dict(itertools.islice(splitted_vulns.items(), first))
    )
    end_cursor = list(sliced_vulns.keys())[-1]
    has_next_page = len(split_vulns(vulns=splitted_vulns, after=end_cursor)) >= 1
    return sliced_vulns, end_cursor, has_next_page


def format_vulnerabilities_criteria(finding_number: str, item: Item) -> Criteria:
    return Criteria(
        en=CriteriaMainData(
            description=item["en"]["description"],
            impact=item["en"]["impact"],
            recommendation=item["en"]["recommendation"],
            threat=item["en"]["threat"],
            title=f'{finding_number}. {item["en"]["title"]}',
        ),
        es=CriteriaMainData(
            description=item["es"]["description"],
            impact=item["es"]["impact"],
            recommendation=item["es"]["recommendation"],
            threat=item["es"]["threat"],
            title=f'{finding_number}. {item["es"]["title"]}',
        ),
        finding_number=finding_number,
        remediation_time=item["remediation_time"],
        requirements=item["requirements"],
        score=CriteriaScore(
            base=CriteriaBaseScore(
                attack_complexity=item["score"]["base"]["attack_complexity"],
                attack_vector=item["score"]["base"]["attack_vector"],
                availability=item["score"]["base"]["availability"],
                confidentiality=item["score"]["base"]["confidentiality"],
                integrity=item["score"]["base"]["integrity"],
                privileges_required=item["score"]["base"]["privileges_required"],
                scope=item["score"]["base"]["scope"],
                user_interaction=item["score"]["base"]["user_interaction"],
            ),
            temporal=CriteriaTemporalScore(
                exploit_code_maturity=item["score"]["temporal"]["exploit_code_maturity"],
                remediation_level=item["score"]["temporal"]["remediation_level"],
                report_confidence=item["score"]["temporal"]["report_confidence"],
            ),
        ),
        score_v4=CriteriaScoreV4(
            base=CriteriaBaseScoreV4(
                attack_complexity=item["score_v4"]["base"]["attack_complexity"],
                attack_vector=item["score_v4"]["base"]["attack_vector"],
                attack_requirements=item["score_v4"]["base"]["attack_requirements"],
                availability_va=item["score_v4"]["base"]["availability_va"],
                confidentiality_vc=item["score_v4"]["base"]["confidentiality_vc"],
                integrity_vi=item["score_v4"]["base"]["integrity_vi"],
                availability_sa=item["score_v4"]["base"]["availability_sa"],
                confidentiality_sc=item["score_v4"]["base"]["confidentiality_sc"],
                integrity_si=item["score_v4"]["base"]["integrity_si"],
                privileges_required=item["score_v4"]["base"]["privileges_required"],
                user_interaction=item["score_v4"]["base"]["user_interaction"],
            ),
            threat=CriteriaThreatScoreV4(
                exploit_maturity=item["score_v4"]["threat"]["exploit_maturity"],
            ),
        ),
    )
