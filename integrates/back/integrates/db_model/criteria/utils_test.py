from integrates.class_types.types import Item
from integrates.custom_exceptions import InvalidCriteriaRange, InvalidParameter
from integrates.db_model.criteria.types import (
    Criteria,
    CriteriaBaseScore,
    CriteriaBaseScoreV4,
    CriteriaMainData,
    CriteriaScore,
    CriteriaScoreV4,
    CriteriaTemporalScore,
    CriteriaThreatScoreV4,
)
from integrates.db_model.criteria.utils import format_vulnerabilities_criteria, prepare_vulns_info
from integrates.testing.utils import raises


def test_prepare_vulns_info_valid() -> None:
    vulns_info = {
        "vuln1": {"data": "info1"},
        "vuln2": {"data": "info2"},
        "vuln3": {"data": "info3"},
    }
    first = 2
    after = "vuln1"
    sliced_vulns, end_cursor, has_next_page = prepare_vulns_info(
        first=first, after=after, vulns_info=vulns_info
    )
    assert sliced_vulns == {"vuln2": {"data": "info2"}, "vuln3": {"data": "info3"}}
    assert end_cursor == "vuln3"
    assert has_next_page is False


def test_prepare_vulns_info_invalid_first() -> None:
    vulns_info = {
        "vuln1": {"data": "info1"},
        "vuln2": {"data": "info2"},
    }
    first = 0
    after = "vuln1"
    with raises(InvalidCriteriaRange):
        prepare_vulns_info(first=first, after=after, vulns_info=vulns_info)


def test_prepare_vulns_info_invalid_after() -> None:
    vulns_info = {
        "vuln1": {"data": "info1"},
        "vuln2": {"data": "info2"},
    }
    first = 1
    after = "vuln3"
    with raises(InvalidParameter):
        prepare_vulns_info(first=first, after=after, vulns_info=vulns_info)


def test_prepare_vulns_info_no_first() -> None:
    vulns_info = {
        "vuln1": {"data": "info1"},
        "vuln2": {"data": "info2"},
        "vuln3": {"data": "info3"},
    }
    first = None
    after = "vuln1"
    sliced_vulns, end_cursor, has_next_page = prepare_vulns_info(
        first=first, after=after, vulns_info=vulns_info
    )
    assert sliced_vulns == {"vuln2": {"data": "info2"}, "vuln3": {"data": "info3"}}
    assert end_cursor == "vuln3"
    assert has_next_page is False


def test_prepare_vulns_info_no_after() -> None:
    vulns_info = {
        "vuln1": {"data": "info1"},
        "vuln2": {"data": "info2"},
        "vuln3": {"data": "info3"},
    }
    first = 2
    after = ""
    sliced_vulns, end_cursor, has_next_page = prepare_vulns_info(
        first=first, after=after, vulns_info=vulns_info
    )
    assert sliced_vulns == {"vuln1": {"data": "info1"}, "vuln2": {"data": "info2"}}
    assert end_cursor == "vuln2"
    assert has_next_page is True


def test_format_vulnerabilities_criteria() -> None:
    finding_number = "1"
    item = Item(
        en={
            "description": "English description",
            "impact": "High",
            "recommendation": "Apply patch",
            "threat": "Critical",
            "title": "Vulnerability Title EN",
        },
        es={
            "description": "Descripción en español",
            "impact": "Alto",
            "recommendation": "Aplicar parche",
            "threat": "Crítico",
            "title": "Título de la vulnerabilidad ES",
        },
        remediation_time="1 week",
        requirements=["Requirement 1", "Requirement 2"],
        score={
            "base": {
                "attack_complexity": "Low",
                "attack_vector": "Network",
                "availability": "High",
                "confidentiality": "High",
                "integrity": "High",
                "privileges_required": "None",
                "scope": "Unchanged",
                "user_interaction": "None",
            },
            "temporal": {
                "exploit_code_maturity": "High",
                "remediation_level": "Official Fix",
                "report_confidence": "Confirmed",
            },
        },
        score_v4={
            "base": {
                "attack_complexity": "Low",
                "attack_vector": "Network",
                "attack_requirements": "None",
                "availability_va": "High",
                "confidentiality_vc": "High",
                "integrity_vi": "High",
                "availability_sa": "High",
                "confidentiality_sc": "High",
                "integrity_si": "High",
                "privileges_required": "None",
                "user_interaction": "None",
            },
            "threat": {
                "exploit_maturity": "High",
            },
        },
    )

    criteria = format_vulnerabilities_criteria(finding_number=finding_number, item=item)

    assert isinstance(criteria, Criteria)
    assert criteria.en == CriteriaMainData(
        description="English description",
        impact="High",
        recommendation="Apply patch",
        threat="Critical",
        title="1. Vulnerability Title EN",
    )
    assert criteria.es == CriteriaMainData(
        description="Descripción en español",
        impact="Alto",
        recommendation="Aplicar parche",
        threat="Crítico",
        title="1. Título de la vulnerabilidad ES",
    )
    assert criteria.finding_number == finding_number
    assert criteria.remediation_time == "1 week"
    assert criteria.requirements == ["Requirement 1", "Requirement 2"]
    assert criteria.score == CriteriaScore(
        base=CriteriaBaseScore(
            attack_complexity="Low",
            attack_vector="Network",
            availability="High",
            confidentiality="High",
            integrity="High",
            privileges_required="None",
            scope="Unchanged",
            user_interaction="None",
        ),
        temporal=CriteriaTemporalScore(
            exploit_code_maturity="High",
            remediation_level="Official Fix",
            report_confidence="Confirmed",
        ),
    )
    assert criteria.score_v4 == CriteriaScoreV4(
        base=CriteriaBaseScoreV4(
            attack_complexity="Low",
            attack_vector="Network",
            attack_requirements="None",
            availability_va="High",
            confidentiality_vc="High",
            integrity_vi="High",
            availability_sa="High",
            confidentiality_sc="High",
            integrity_si="High",
            privileges_required="None",
            user_interaction="None",
        ),
        threat=CriteriaThreatScoreV4(
            exploit_maturity="High",
        ),
    )
