from boto3.dynamodb.conditions import (
    Key,
)

from integrates.audit import AuditEvent, add_audit_event
from integrates.db_model import (
    TABLE,
)
from integrates.dynamodb import (
    keys,
    operations,
)
from integrates.dynamodb.types import (
    PrimaryKey,
)


async def remove_group_forces_executions(
    *,
    group_name: str,
) -> None:
    primary_key = keys.build_key(
        facet=TABLE.facets["forces_execution"],
        values={"name": group_name},
    )
    index = TABLE.indexes["inverted_index"]
    key_structure = TABLE.primary_key
    condition_expression = Key(key_structure.sort_key).eq(primary_key.sort_key) & Key(
        key_structure.partition_key,
    ).begins_with(primary_key.partition_key)
    response = await operations.query(
        condition_expression=condition_expression,
        facets=(TABLE.facets["forces_execution"],),
        table=TABLE,
        index=index,
    )
    await operations.batch_delete_item(
        keys=tuple(
            PrimaryKey(
                partition_key=item["pk"],
                sort_key=item["sk"],
            )
            for item in response.items
        ),
        table=TABLE,
    )
    for item in response.items:
        add_audit_event(
            AuditEvent(
                action="DELETE",
                author="unknown",
                metadata={},
                object="ForcesExecution",
                object_id=item["pk"],
            )
        )
