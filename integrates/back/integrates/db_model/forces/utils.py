from datetime import (
    datetime,
)
from decimal import (
    Decimal,
)

from integrates.class_types.types import (
    Item,
)
from integrates.db_model.forces.enums import (
    VulnerabilityExploitStatus,
)
from integrates.db_model.forces.types import (
    ExecutionVulnerabilities,
    ExploitResult,
    ForcesExecution,
)
from integrates.db_model.items import (
    ExecutionVulnerabilitiesItem,
    ExploitResultItem,
    ForcesExecutionItem,
)
from integrates.db_model.utils import (
    get_as_utc_iso_format,
)
from integrates.dynamodb.types import (
    PrimaryKey,
)


def format_exploit_result(
    result: list[ExploitResultItem],
) -> list[ExploitResult]:
    exploit = [
        ExploitResult(
            exploitability=item["exploitability"],
            kind=item["kind"],
            state=VulnerabilityExploitStatus(item["state"]),
            where=item["where"],
            who=item["who"],
        )
        for item in result
    ]
    return exploit


def format_forces_vulnerabilities(
    vulnerabilities: ExecutionVulnerabilitiesItem,
) -> ExecutionVulnerabilities:
    return ExecutionVulnerabilities(
        num_of_accepted_vulnerabilities=int(vulnerabilities["num_of_accepted_vulnerabilities"]),
        num_of_open_vulnerabilities=int(vulnerabilities["num_of_open_vulnerabilities"]),
        num_of_closed_vulnerabilities=int(vulnerabilities["num_of_closed_vulnerabilities"]),
        num_of_open_managed_vulnerabilities=int(
            vulnerabilities["num_of_open_managed_vulnerabilities"],
        )
        if vulnerabilities.get("num_of_open_managed_vulnerabilities") is not None
        else 0,
        num_of_open_unmanaged_vulnerabilities=int(
            vulnerabilities["num_of_open_unmanaged_vulnerabilities"],
        )
        if vulnerabilities.get("num_of_open_unmanaged_vulnerabilities") is not None
        else 0,
        open=format_exploit_result(vulnerabilities["open"]) if vulnerabilities.get("open") else [],
        closed=format_exploit_result(vulnerabilities["closed"])
        if vulnerabilities.get("closed")
        else [],
        accepted=format_exploit_result(vulnerabilities["accepted"])
        if vulnerabilities.get("accepted")
        else [],
        num_of_vulns_in_exploits=vulnerabilities.get("num_of_vulnerabilities_in_exploits"),
        num_of_vulns_in_integrates_exploits=vulnerabilities.get(
            "num_of_vulnerabilities_in_integrates_exploits",
        ),
        num_of_vulns_in_accepted_exploits=vulnerabilities.get(
            "num_of_vulnerabilities_in_accepted_exploits",
        ),
    )


def format_forces_execution(item: ForcesExecutionItem) -> ForcesExecution:
    return ForcesExecution(
        id=item["id"],
        group_name=item["group_name"],
        execution_date=datetime.fromisoformat(item["execution_date"]),
        commit=item["commit"],
        repo=item["repo"],
        branch=item["branch"],
        kind=item["kind"],
        exit_code=item["exit_code"],
        strictness=item["strictness"],
        origin=item["origin"],
        grace_period=item["grace_period"] if item.get("grace_period") else 0,
        severity_threshold=Decimal(item["severity_threshold"])
        if item.get("severity_threshold")
        else Decimal("0.0"),
        days_until_it_breaks=int(item["days_until_it_breaks"])
        if item.get("days_until_it_breaks") is not None
        else None,
        vulnerabilities=format_forces_vulnerabilities(item["vulnerabilities"]),
    )


def format_forces_item(
    execution: ForcesExecution,
    *,
    primary_key: PrimaryKey,
    gsi_2_key: PrimaryKey,
) -> ForcesExecutionItem:
    item: ForcesExecutionItem = {
        "pk": primary_key.partition_key,
        "sk": primary_key.sort_key,
        "pk_2": gsi_2_key.partition_key,
        "sk_2": gsi_2_key.sort_key,
        "id": execution.id,
        "group_name": execution.group_name,
        "execution_date": get_as_utc_iso_format(execution.execution_date),
        "commit": execution.commit,
        "repo": execution.repo,
        "branch": execution.branch,
        "kind": execution.kind,
        "exit_code": execution.exit_code,
        "strictness": execution.strictness,
        "origin": execution.origin,
        "grace_period": execution.grace_period if execution.grace_period else 0,
        "severity_threshold": execution.severity_threshold
        if execution.severity_threshold
        else Decimal("0.0"),
        "vulnerabilities": {
            "num_of_accepted_vulnerabilities": (
                execution.vulnerabilities.num_of_accepted_vulnerabilities
            ),
            "num_of_open_vulnerabilities": (execution.vulnerabilities.num_of_open_vulnerabilities),
            "num_of_closed_vulnerabilities": (
                execution.vulnerabilities.num_of_closed_vulnerabilities
            ),
            "num_of_open_managed_vulnerabilities": (
                execution.vulnerabilities.num_of_open_managed_vulnerabilities
            ),
            "num_of_open_unmanaged_vulnerabilities": (
                execution.vulnerabilities.num_of_open_unmanaged_vulnerabilities
            ),
        },
    }

    if execution.days_until_it_breaks is not None:
        item["days_until_it_breaks"] = execution.days_until_it_breaks

    return item


def format_break_build(exit_code: str) -> str:
    if exit_code == "66":
        return "Yes"
    if exit_code == "0":
        return "No"

    return "Error"


def format_forces_to_file(execution: ForcesExecution) -> Item:
    return {
        "group_name": execution.group_name,
        "execution_date": get_as_utc_iso_format(execution.execution_date),
        "kind": execution.kind,
        "repo": execution.repo,
        "origin": execution.origin,
        "commit": execution.commit,
        "branch": execution.branch,
        "grace_period": execution.grace_period if execution.grace_period else 0,
        "exit_code": execution.exit_code,
        "num_of_accepted_vulnerabilities": (
            execution.vulnerabilities.num_of_open_managed_vulnerabilities
        )
        if execution.vulnerabilities.num_of_open_managed_vulnerabilities
        else 0,
        "num_of_unmanaged_vulnerabilities": (
            execution.vulnerabilities.num_of_open_unmanaged_vulnerabilities
        )
        if execution.vulnerabilities.num_of_open_unmanaged_vulnerabilities
        else 0,
        "strictness": execution.strictness,
        "id": execution.id,
        "severity_threshold": execution.severity_threshold
        if execution.severity_threshold
        else Decimal("0.0"),
        "days_until_it_breaks": execution.days_until_it_breaks,
        "breaks_build": format_break_build(execution.exit_code),
    }
