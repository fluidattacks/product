from datetime import (
    datetime,
)

from integrates.audit import AuditEvent, add_audit_event
from integrates.db_model import (
    TABLE,
)
from integrates.db_model.hook.enums import (
    HookStatus,
)
from integrates.db_model.hook.types import (
    GroupHookPayload,
    HookState,
)
from integrates.db_model.hook.utils import (
    format_group_hook_item_to_update,
)
from integrates.dynamodb import (
    keys,
    operations,
)


async def update(
    hook_id: str,
    group_name: str,
    user_email: str,
    modified_date: datetime,
    hook: GroupHookPayload,
) -> None:
    hook_key = keys.build_key(
        facet=TABLE.facets["hook_metadata"],
        values={
            "id": hook_id,
            "group_name": group_name,
        },
    )

    state = HookState(
        modified_by=user_email,
        modified_date=modified_date,
        status=HookStatus.ACTIVE,
    )
    item = format_group_hook_item_to_update(group_hook=hook, state=state)

    await operations.update_item(
        item=item,
        key=hook_key,
        table=TABLE,
    )
    add_audit_event(
        AuditEvent(
            action="UPDATE",
            author=user_email,
            metadata=item,
            object="Hook",
            object_id=hook_id,
        )
    )
