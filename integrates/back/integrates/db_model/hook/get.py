from collections.abc import (
    Iterable,
)

from aiodataloader import (
    DataLoader,
)
from aioextensions import (
    collect,
)
from boto3.dynamodb.conditions import (
    Key,
)

from integrates.db_model import (
    TABLE,
)
from integrates.db_model.hook.types import (
    GroupHook,
    GroupHookRequest,
)
from integrates.db_model.hook.utils import (
    format_group_hook,
)
from integrates.db_model.items import HookItem
from integrates.dynamodb import (
    keys,
    operations,
)


async def _get_group_hook(*, group_name: str) -> list[GroupHook]:
    primary_key = keys.build_key(
        facet=TABLE.facets["hook_metadata"],
        values={
            "group_name": group_name,
        },
    )
    index = TABLE.indexes["inverted_index"]
    key_structure = index.primary_key

    response = await operations.DynamoClient[HookItem].query(
        condition_expression=(
            Key(key_structure.partition_key).eq(primary_key.sort_key)
            & Key(key_structure.sort_key).begins_with(primary_key.partition_key)
        ),
        facets=(TABLE.facets["hook_metadata"],),
        index=index,
        table=TABLE,
    )

    return [format_group_hook(item) for item in response.items]


class GroupHookLoader(DataLoader[str, list[GroupHook]]):
    async def batch_load_fn(self, requests: Iterable[str]) -> list[list[GroupHook]]:
        return list(
            await collect(
                tuple(_get_group_hook(group_name=group_name) for group_name in requests),
                workers=32,
            ),
        )


async def _get_hook(*, group_name: str, hook_id: str) -> list[GroupHook]:
    primary_key = keys.build_key(
        facet=TABLE.facets["hook_metadata"],
        values={
            "group_name": group_name,
            "id": hook_id,
        },
    )
    index = TABLE.indexes["inverted_index"]
    key_structure = index.primary_key

    response = await operations.DynamoClient[HookItem].query(
        condition_expression=(
            Key(key_structure.partition_key).eq(primary_key.sort_key)
            & Key(key_structure.sort_key).begins_with(primary_key.partition_key)
        ),
        facets=(TABLE.facets["hook_metadata"],),
        index=index,
        table=TABLE,
    )

    return [format_group_hook(item) for item in response.items]


class HookLoader(DataLoader[GroupHookRequest, list[GroupHook]]):
    async def batch_load_fn(self, requests: Iterable[GroupHookRequest]) -> list[list[GroupHook]]:
        return list(
            await collect(
                tuple(
                    _get_hook(group_name=request.group_name, hook_id=request.id)
                    for request in requests
                ),
                workers=32,
            ),
        )
