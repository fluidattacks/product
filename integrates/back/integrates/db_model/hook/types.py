from datetime import (
    datetime,
)
from typing import (
    NamedTuple,
)

from integrates.db_model.hook.enums import (
    HookEvent,
    HookStatus,
)


class HookState(NamedTuple):
    modified_by: str
    modified_date: datetime
    status: HookStatus


class GroupHook(NamedTuple):
    id: str
    entry_point: str
    group_name: str
    name: str
    token: str
    token_header: str
    hook_events: set[HookEvent]
    state: HookState


class GroupHookPayload(NamedTuple):
    entry_point: str
    name: str
    token: str
    token_header: str
    hook_events: set[HookEvent]
    state: HookState | None = None


class GroupHookRequest(NamedTuple):
    id: str
    group_name: str
