from boto3.dynamodb.conditions import (
    Key,
)

from integrates.audit import AuditEvent, add_audit_event
from integrates.db_model import (
    TABLE,
)
from integrates.dynamodb import (
    keys,
    operations,
)
from integrates.dynamodb.types import (
    PrimaryKey,
)


async def remove(
    *,
    credential_id: str,
    organization_id: str,
) -> None:
    credential_key = keys.build_key(
        facet=TABLE.facets["credentials_metadata"],
        values={
            "organization_id": organization_id,
            "id": credential_id,
        },
    )
    await operations.delete_item(key=credential_key, table=TABLE)
    add_audit_event(
        AuditEvent(
            action="DELETE",
            author="unknown",
            metadata={},
            object="Credential",
            object_id=credential_id,
        )
    )


async def remove_organization_credentials(
    *,
    organization_id: str,
) -> None:
    facet = TABLE.facets["credentials_metadata"]
    primary_key = keys.build_key(
        facet=facet,
        values={
            "organization_id": organization_id,
        },
    )
    index = TABLE.indexes["inverted_index"]
    key_structure = index.primary_key
    condition_expression = Key(key_structure.partition_key).eq(primary_key.sort_key) & Key(
        key_structure.sort_key,
    ).begins_with(primary_key.partition_key)
    response = await operations.query(
        condition_expression=condition_expression,
        facets=(facet,),
        table=TABLE,
        index=index,
    )
    if not response.items:
        return
    keys_to_delete = set(
        PrimaryKey(
            partition_key=item[TABLE.primary_key.partition_key],
            sort_key=item[TABLE.primary_key.sort_key],
        )
        for item in response.items
    )
    await operations.batch_delete_item(
        keys=tuple(keys_to_delete),
        table=TABLE,
    )
    for key in keys_to_delete:
        add_audit_event(
            AuditEvent(
                action="DELETE",
                author="unknown",
                metadata={},
                object="Credential",
                object_id=key.partition_key,
            )
        )
