import simplejson as json
from boto3.dynamodb.conditions import (
    Attr,
)

from integrates.audit import AuditEvent, add_audit_event
from integrates.custom_exceptions import (
    RepeatedCredential,
)
from integrates.db_model import (
    TABLE,
)
from integrates.db_model.credentials.constants import (
    OWNER_INDEX_FACET,
)
from integrates.db_model.credentials.types import (
    Credentials,
)
from integrates.db_model.credentials.utils import (
    validate_secret,
)
from integrates.db_model.utils import (
    serialize,
)
from integrates.dynamodb import (
    keys,
    operations,
)
from integrates.dynamodb.exceptions import (
    ConditionalCheckFailedException,
)


async def add(*, credential: Credentials) -> None:
    validate_secret(credential.state, credential.secret)
    key_structure = TABLE.primary_key
    metadata_key = keys.build_key(
        facet=TABLE.facets["credentials_metadata"],
        values={
            "organization_id": credential.organization_id,
            "id": credential.id,
        },
    )
    gsi_2_index = TABLE.indexes["gsi_2"]
    gsi_2_key = keys.build_key(
        facet=OWNER_INDEX_FACET,
        values={
            "owner": credential.state.owner,
            "id": credential.id,
        },
    )
    item = {
        key_structure.partition_key: metadata_key.partition_key,
        key_structure.sort_key: metadata_key.sort_key,
        gsi_2_index.primary_key.partition_key: gsi_2_key.partition_key,
        gsi_2_index.primary_key.sort_key: gsi_2_key.sort_key,
        **json.loads(json.dumps(credential, default=serialize)),
    }
    condition_expression = Attr(key_structure.partition_key).not_exists()
    try:
        await operations.put_item(
            condition_expression=condition_expression,
            facet=TABLE.facets["credentials_metadata"],
            item=item,
            table=TABLE,
        )
        add_audit_event(
            AuditEvent(
                action="CREATE",
                author=credential.state.modified_by,
                metadata=item,
                object="Credential",
                object_id=credential.id,
            )
        )

    except ConditionalCheckFailedException as ex:
        raise RepeatedCredential() from ex
