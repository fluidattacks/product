from datetime import (
    datetime,
)
from typing import (
    NamedTuple,
)

from integrates.db_model.enums import (
    CredentialType,
)


class HttpsSecret(NamedTuple):
    user: str
    password: str


class HttpsPatSecret(NamedTuple):
    token: str


class OauthGitlabSecret(NamedTuple):
    refresh_token: str
    redirect_uri: str
    access_token: str
    valid_until: datetime


class OauthAzureSecret(NamedTuple):
    arefresh_token: str
    redirect_uri: str
    access_token: str
    valid_until: datetime


class OauthBitbucketSecret(NamedTuple):
    brefresh_token: str
    access_token: str
    valid_until: datetime


class OauthGithubSecret(NamedTuple):
    access_token: str


class SshSecret(NamedTuple):
    key: str


class AWSRoleSecret(NamedTuple):
    arn: str


RepoSecret = OauthAzureSecret | OauthBitbucketSecret | OauthGithubSecret | OauthGitlabSecret


Secret = HttpsSecret | HttpsPatSecret | RepoSecret | SshSecret | AWSRoleSecret


class CredentialsState(NamedTuple):
    modified_by: str
    modified_date: datetime
    name: str
    type: CredentialType
    is_pat: bool
    owner: str
    azure_organization: str | None = None


class Credentials(NamedTuple):
    id: str
    organization_id: str
    state: CredentialsState
    secret: Secret


class CredentialsRequest(NamedTuple):
    id: str
    organization_id: str


class CredentialsMetadataToUpdate(NamedTuple):
    secret: Secret | None = None
