from datetime import (
    datetime,
)

from integrates.custom_exceptions import (
    InvalidCredentialSecret,
)
from integrates.db_model.credentials.types import (
    AWSRoleSecret,
    Credentials,
    CredentialsState,
    HttpsPatSecret,
    HttpsSecret,
    OauthAzureSecret,
    OauthBitbucketSecret,
    OauthGithubSecret,
    OauthGitlabSecret,
    SshSecret,
)
from integrates.db_model.enums import (
    CredentialType,
)
from integrates.db_model.items import (
    CredentialsItem,
    SecretItem,
)


def get_http_secret(
    item: SecretItem,
) -> HttpsPatSecret | HttpsSecret:
    if "token" in item:
        return HttpsPatSecret(token=item["token"])

    return HttpsSecret(
        user=item["user"],
        password=item["password"],
    )


def get_oauth_secret(
    item: SecretItem,
) -> OauthAzureSecret | OauthBitbucketSecret | OauthGithubSecret | OauthGitlabSecret | None:
    if "brefresh_token" in item:
        return OauthBitbucketSecret(
            brefresh_token=item["brefresh_token"],
            access_token=item["access_token"],
            valid_until=datetime.fromisoformat(item["valid_until"]),
        )

    if "refresh_token" in item:
        return OauthGitlabSecret(
            refresh_token=item["refresh_token"],
            redirect_uri=item["redirect_uri"],
            access_token=item["access_token"],
            valid_until=datetime.fromisoformat(item["valid_until"]),
        )

    if "arefresh_token" in item:
        return OauthAzureSecret(
            arefresh_token=item["arefresh_token"],
            redirect_uri=item["redirect_uri"],
            access_token=item["access_token"],
            valid_until=datetime.fromisoformat(item["valid_until"]),
        )

    if "access_token" in item:
        return OauthGithubSecret(access_token=item["access_token"])

    return None


def format_secret(
    credential_type: CredentialType,
    item: SecretItem,
) -> (
    HttpsPatSecret
    | HttpsSecret
    | OauthAzureSecret
    | OauthBitbucketSecret
    | OauthGithubSecret
    | OauthGitlabSecret
    | SshSecret
    | AWSRoleSecret
):
    if credential_type is CredentialType.HTTPS:
        return get_http_secret(item)

    if credential_type is CredentialType.OAUTH:
        oauth_secret = get_oauth_secret(item)
        if oauth_secret:
            return oauth_secret
    elif credential_type is CredentialType.AWSROLE and "arn" in item:
        return AWSRoleSecret(arn=item["arn"])

    return SshSecret(key=item["key"])


def format_credential(item: CredentialsItem) -> Credentials:
    credential_type = CredentialType(item["state"]["type"])
    return Credentials(
        id=item["id"],
        organization_id=item["organization_id"],
        state=CredentialsState(
            modified_by=item["state"]["modified_by"],
            modified_date=datetime.fromisoformat(item["state"]["modified_date"]),
            name=item["state"]["name"],
            is_pat=item["state"].get("is_pat", False),
            azure_organization=item["state"].get("azure_organization", None),
            owner=item["state"]["owner"],
            type=credential_type,
        ),
        secret=format_secret(credential_type, item["secret"]),
    )


def validate_secret(
    state: CredentialsState,
    secret: (
        HttpsSecret
        | HttpsPatSecret
        | OauthAzureSecret
        | OauthBitbucketSecret
        | OauthGithubSecret
        | OauthGitlabSecret
        | SshSecret
        | AWSRoleSecret
        | None
    ) = None,
) -> None:
    if state.is_pat and (
        state.type is not CredentialType.HTTPS
        or not isinstance(secret, HttpsPatSecret)
        or state.azure_organization is None
    ):
        raise InvalidCredentialSecret()
    if (state.type is CredentialType.SSH and not isinstance(secret, SshSecret)) or (
        state.type is CredentialType.HTTPS and not isinstance(secret, (HttpsSecret, HttpsPatSecret))
    ):
        raise InvalidCredentialSecret()
    if state.type is CredentialType.AWSROLE and not isinstance(secret, AWSRoleSecret):
        raise InvalidCredentialSecret()


def filter_pat_credentials(
    credentials: list[Credentials],
) -> list[Credentials]:
    return [
        credential
        for credential in credentials
        if credential.state.is_pat and credential.state.azure_organization is not None
    ]
