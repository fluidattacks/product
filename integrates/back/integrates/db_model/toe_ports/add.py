from boto3.dynamodb.conditions import Attr

from integrates.audit import AuditEvent, add_audit_event
from integrates.custom_exceptions import InvalidParameter, RepeatedToePort
from integrates.db_model import HISTORIC_TABLE, TABLE
from integrates.db_model.toe_ports.constants import GSI_2_FACET, GSI_2_HISTORIC_STATE_FACET
from integrates.db_model.toe_ports.types import ToePort
from integrates.db_model.toe_ports.utils import format_toe_port_item, format_toe_port_state_item
from integrates.db_model.utils import get_as_utc_iso_format
from integrates.dynamodb import keys, operations
from integrates.dynamodb.exceptions import ConditionalCheckFailedException


async def add(*, toe_port: ToePort, validate_state: bool = True) -> None:
    if validate_state and toe_port.state.modified_date is None:
        raise InvalidParameter("modified_date")
    if validate_state and toe_port.state.modified_by is None:
        raise InvalidParameter("modified_by")

    key_structure = TABLE.primary_key
    facet = TABLE.facets["toe_port_metadata"]
    toe_port_key = keys.build_key(
        facet=facet,
        values={
            "address": toe_port.address,
            "port": toe_port.port,
            "group_name": toe_port.group_name,
            "root_id": toe_port.root_id,
        },
    )
    gsi_2_key = keys.build_key(
        facet=GSI_2_FACET,
        values={
            "be_present": str(toe_port.state.be_present).lower(),
            "group_name": toe_port.group_name,
            "address": toe_port.address,
            "port": toe_port.port,
            "root_id": toe_port.root_id,
        },
    )
    toe_port_item = format_toe_port_item(
        toe_port_key,
        gsi_2_key,
        toe_port,
    )
    condition_expression = Attr(key_structure.partition_key).not_exists()
    try:
        await operations.put_item(
            condition_expression=condition_expression,
            facet=facet,
            item=toe_port_item,
            table=TABLE,
        )
        add_audit_event(
            AuditEvent(
                action="CREATE",
                author=toe_port.state.modified_by or "unknown",
                metadata=toe_port_item,
                object="ToePort",
                object_id=toe_port_key.sort_key,
            )
        )
    except ConditionalCheckFailedException as ex:
        raise RepeatedToePort() from ex

    if not isinstance(toe_port_item["state"]["modified_date"], str):
        raise InvalidParameter("modified_date")


async def add_historic(*, toe_port: ToePort) -> None:
    if toe_port.state.modified_date is None:
        raise InvalidParameter("modified_date")
    if toe_port.state.modified_by is None:
        raise InvalidParameter("modified_by")

    key_structure = HISTORIC_TABLE.primary_key

    historic_key = keys.build_key(
        facet=HISTORIC_TABLE.facets["toe_port_state"],
        values={
            "address": toe_port.address,
            "port": toe_port.port,
            "group_name": toe_port.group_name,
            "root_id": toe_port.root_id,
            "iso8601utc": get_as_utc_iso_format(toe_port.state.modified_date),
        },
    )

    gsi_2_historic_key = keys.build_key(
        facet=GSI_2_HISTORIC_STATE_FACET,
        values={
            "group_name": toe_port.group_name,
            "iso8601utc": get_as_utc_iso_format(toe_port.state.modified_date),
            "state": "state",
        },
    )

    historic_item = format_toe_port_state_item(
        historic_key,
        gsi_2_historic_key,
        toe_port,
    )

    try:
        await operations.put_item(
            facet=HISTORIC_TABLE.facets["toe_port_state"],
            condition_expression=Attr(key_structure.sort_key).not_exists(),
            item=historic_item,
            table=HISTORIC_TABLE,
        )
    except ConditionalCheckFailedException as ex:
        raise RepeatedToePort() from ex
