from typing import (
    NamedTuple,
)


class JiraInstallState(NamedTuple):
    modified_by: str
    modified_date: str
    used_api_token: str
    used_jira_jwt: str


class JiraSecurityInstall(NamedTuple):
    associated_email: str
    base_url: str
    client_key: str
    cloud_id: str
    description: str
    display_url: str
    event_type: str
    installation_id: str
    key: str
    plugins_version: str
    product_type: str
    public_key: str
    server_version: str
    shared_secret: str
    state: JiraInstallState


class JiraSecurityInstallRequest(NamedTuple):
    client_key: str
    base_url: str


class UserJiraSecurityInstallRequest(NamedTuple):
    client_key: str
    limit: int | None = None
