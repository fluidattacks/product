from integrates.dataloaders import get_new_context
from integrates.db_model import jira_installations as jira_installations_model
from integrates.db_model.jira_installations.types import JiraSecurityInstallRequest
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import JiraSecurityInstallFaker
from integrates.testing.mocks import mocks


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            jira_installations=[
                JiraSecurityInstallFaker(
                    base_url="https://example.com",
                    client_key="test-client-key",
                    shared_secret="shared_secret",  # noqa: S106
                ),
            ],
        ),
    )
)
async def test_delete_jira_install() -> None:
    loaders = get_new_context()
    await jira_installations_model.remove_jira_installation(
        client_key="test-client-key",
    )
    install = await loaders.jira_install.load(
        JiraSecurityInstallRequest(
            base_url="https://example.com",
            client_key="test-client-key",
        )
    )
    assert install is None
