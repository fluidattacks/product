from boto3.dynamodb.conditions import Key

from integrates.audit import AuditEvent, add_audit_event
from integrates.db_model import (
    TABLE,
)
from integrates.dynamodb import (
    operations,
)
from integrates.dynamodb.types import (
    PrimaryKey,
)


async def remove_jira_installation(
    *,
    client_key: str,
) -> None:
    condition_expression = Key("pk").eq(f"CLIENT_KEY#{client_key}")
    response = await operations.query(
        facets=(TABLE.facets["jira_install"],),
        condition_expression=condition_expression,
        table=TABLE,
    )
    if response.items:
        await operations.batch_delete_item(
            keys=tuple(
                PrimaryKey(
                    partition_key=item["pk"],
                    sort_key=item["sk"],
                )
                for item in response.items
            ),
            table=TABLE,
        )
        add_audit_event(
            AuditEvent(
                action="DELETE",
                author="unknown",
                metadata={},
                object="Hook",
                object_id=f"CLIENT_KEY#{client_key}",
            )
        )
