from collections.abc import (
    Iterable,
)

from aiodataloader import (
    DataLoader,
)
from aioextensions import (
    collect,
)
from boto3.dynamodb.conditions import (
    Key,
)

from integrates.db_model import (
    TABLE,
)
from integrates.db_model.items import JiraInstallItem
from integrates.db_model.jira_installations.types import (
    JiraSecurityInstall,
    JiraSecurityInstallRequest,
    UserJiraSecurityInstallRequest,
)
from integrates.db_model.jira_installations.utils import (
    format_jira_install,
)
from integrates.dynamodb import (
    keys,
    operations,
)


async def _get_jira_installations(
    *,
    requests: Iterable[JiraSecurityInstallRequest],
) -> list[JiraSecurityInstall | None]:
    primary_keys = tuple(
        keys.build_key(
            facet=TABLE.facets["jira_install"],
            values={
                "client_key": request.client_key,
                "base_url": request.base_url,
            },
        )
        for request in requests
    )
    items = await operations.DynamoClient[JiraInstallItem].batch_get_item(
        keys=primary_keys, table=TABLE
    )

    response = {
        JiraSecurityInstallRequest(
            client_key=install.client_key,
            base_url=install.base_url,
        ): install
        for install in [format_jira_install(item) for item in items]
    }

    return list(response.get(request) for request in requests)


async def _get_jira_installations_per_user(
    *,
    request: UserJiraSecurityInstallRequest,
) -> list[JiraSecurityInstall]:
    primary_key = keys.build_key(
        facet=TABLE.facets["jira_install"],
        values={"client_key": request.client_key},
    )
    key_structure = TABLE.primary_key
    response = await operations.DynamoClient[JiraInstallItem].query(
        condition_expression=(Key(key_structure.partition_key).eq(primary_key.partition_key)),
        facets=(TABLE.facets["jira_install"],),
        table=TABLE,
    )
    return [format_jira_install(item) for item in response.items]


class UserJiraInstallLoader(DataLoader[UserJiraSecurityInstallRequest, list[JiraSecurityInstall]]):
    async def batch_load_fn(
        self,
        requests: Iterable[UserJiraSecurityInstallRequest],
    ) -> list[list[JiraSecurityInstall]]:
        return list(
            await collect(
                tuple(_get_jira_installations_per_user(request=request) for request in requests),
                workers=32,
            ),
        )


class JiraInstallLoader(DataLoader[JiraSecurityInstallRequest, JiraSecurityInstall | None]):
    async def batch_load_fn(
        self,
        requests: Iterable[JiraSecurityInstallRequest],
    ) -> list[JiraSecurityInstall | None]:
        return await _get_jira_installations(requests=requests)
