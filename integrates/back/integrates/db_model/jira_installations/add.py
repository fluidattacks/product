import simplejson as json
from boto3.dynamodb.conditions import (
    Attr,
)

from integrates.audit import AuditEvent, add_audit_event
from integrates.custom_exceptions import (
    JiraInstallAlreadyCreated,
)
from integrates.db_model import (
    TABLE,
)
from integrates.db_model.jira_installations.types import (
    JiraSecurityInstall,
)
from integrates.db_model.utils import (
    serialize,
)
from integrates.dynamodb import (
    keys,
    operations,
)
from integrates.dynamodb.exceptions import (
    ConditionalCheckFailedException,
)


async def add(*, jira_install: JiraSecurityInstall) -> None:
    key_structure = TABLE.primary_key
    primary_key = keys.build_key(
        facet=TABLE.facets["jira_install"],
        values={
            "client_key": jira_install.client_key,
            "base_url": jira_install.base_url,
        },
    )
    item = {
        key_structure.partition_key: primary_key.partition_key,
        key_structure.sort_key: primary_key.sort_key,
        **json.loads(json.dumps(jira_install, default=serialize)),
    }
    condition_expression = Attr(key_structure.partition_key).not_exists()
    try:
        await operations.put_item(
            condition_expression=condition_expression,
            facet=TABLE.facets["jira_install"],
            item=item,
            table=TABLE,
        )
        add_audit_event(
            AuditEvent(
                action="CREATE",
                author=jira_install.base_url,
                metadata=item,
                object="JiraSecurityInstall",
                object_id=f"CLIENT_KEY#{jira_install.client_key}#SITE#{jira_install.base_url}",
            )
        )
    except ConditionalCheckFailedException as ex:
        raise JiraInstallAlreadyCreated().new() from ex
