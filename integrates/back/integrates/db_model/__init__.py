import json

from integrates.context import (
    FI_INTEGRATES_DB_MODEL_PATH,
)
from integrates.dynamodb.tables import (
    load_tables,
)

with open(FI_INTEGRATES_DB_MODEL_PATH, encoding="utf-8") as file:
    tables = load_tables(json.load(file))
    TABLE = tables[0]
    HISTORIC_TABLE = tables[1]
