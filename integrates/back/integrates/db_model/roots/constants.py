from integrates.db_model import TABLE
from integrates.dynamodb.types import Facet

ORG_INDEX_METADATA = Facet(
    attrs=TABLE.facets["git_root_metadata"].attrs,
    pk_alias="ORG#name",
    sk_alias="ROOT#uuid",
)

GROUP_INDEX_METADATA = Facet(
    attrs=TABLE.facets["git_root_metadata"].attrs,
    pk_alias="GROUP#name",
    sk_alias="ROOT#STATUS#status#URL#url#BRANCH#branch",
)

GROUP_ENVS_INDEX_METADATA = Facet(
    attrs=TABLE.facets["root_environment_url"].attrs,
    pk_alias="GROUP#group_name",
    sk_alias="URL#hash",
)

GROUP_IMAGES_INDEX_METADATA = Facet(
    attrs=TABLE.facets["root_docker_image"].attrs,
    pk_alias="GROUP#group_name",
    sk_alias="URI#uri",
)
