from datetime import (
    datetime,
)
from typing import (
    Literal,
    NamedTuple,
)

from integrates.db_model.items import GitRootItem, IpRootItem, UrlRootItem
from integrates.db_model.roots.enums import (
    RootCloningStatus,
    RootCriticality,
    RootDockerImageStateStatus,
    RootEnvironmentCloud,
    RootEnvironmentUrlStateStatus,
    RootEnvironmentUrlType,
    RootMachineStatus,
    RootRebaseStatus,
    RootStateReason,
    RootStatus,
    RootToeLinesStatus,
    RootType,
)
from integrates.db_model.types import (
    CodeLanguage,
)
from integrates.dynamodb.types import (
    PageInfo,
)


class DockerImageHistory(NamedTuple):
    created: datetime
    created_by: str | None = None
    empty_layer: bool = False
    comment: str | None = None


class RootDockerImageState(NamedTuple):
    modified_by: str
    modified_date: datetime
    status: RootDockerImageStateStatus
    history: list[DockerImageHistory]
    layers: list[str]
    credential_id: str | None = None
    digest: str | None = None
    include: bool = True


class RootDockerImage(NamedTuple):
    uri: str
    root_id: str
    group_name: str
    state: RootDockerImageState
    created_by: str
    created_at: datetime | None = None


class RootDockerImageLayer(NamedTuple):
    image_uri: str
    digest: str
    size: int


class RootUnreliableIndicators(NamedTuple):
    unreliable_code_languages: list[CodeLanguage] = []
    unreliable_last_status_update: datetime | None = None
    nofluid_quantity: int | None = None


class RootUnreliableIndicatorsToUpdate(NamedTuple):
    unreliable_code_languages: list[CodeLanguage] | None = None
    unreliable_last_status_update: datetime | None = None
    nofluid_quantity: int | None = None


class GitRootCloning(NamedTuple):
    modified_by: str
    modified_date: datetime
    reason: str
    status: RootCloningStatus
    commit: str | None = None
    commit_date: datetime | None = None
    failed_count: int = 0
    first_failed_cloning: datetime | None = None
    first_successful_cloning: datetime | None = None
    last_successful_cloning: datetime | None = None
    successful_count: int = 0


class MachineExecutionsDate(NamedTuple):
    apk: datetime | None = None
    cspm: datetime | None = None
    dast: datetime | None = None
    sast: datetime | None = None
    sca: datetime | None = None


class GitRootMachine(NamedTuple):
    modified_by: str
    modified_date: datetime
    reason: str
    status: RootMachineStatus
    commit: str | None = None
    commit_date: datetime | None = None
    execution_id: str | None = None
    last_executions_date: MachineExecutionsDate | None = None


class GitRootRebase(NamedTuple):
    modified_by: str
    modified_date: datetime
    reason: str
    status: RootRebaseStatus
    commit: str | None = None
    commit_date: datetime | None = None
    last_successful_rebase: datetime | None = None


class GitRootToeLines(NamedTuple):
    modified_by: str
    modified_date: datetime
    reason: str
    status: RootToeLinesStatus
    commit: str | None = None
    commit_date: datetime | None = None
    last_successful_refresh: datetime | None = None


class SecretState(NamedTuple):
    owner: str
    modified_by: str
    modified_date: datetime
    description: str | None = None


class Secret(NamedTuple):
    key: str
    value: str
    created_at: datetime
    state: SecretState


class RootEnvironmentUrlState(NamedTuple):
    modified_by: str
    modified_date: datetime
    status: RootEnvironmentUrlStateStatus
    include: bool = True
    is_production: bool | None = None
    url_type: RootEnvironmentUrlType = RootEnvironmentUrlType.URL
    cloud_name: RootEnvironmentCloud | None = None
    use_egress: bool | None = None
    use_vpn: bool | None = None
    use_ztna: bool | None = None


class RootEnvironmentUrl(NamedTuple):
    url: str
    id: str
    root_id: str
    group_name: str
    state: RootEnvironmentUrlState
    created_at: datetime | None = None
    created_by: str | None = None


class RootEnvironmentUrlToUpdate(NamedTuple):
    include: bool | None = None
    url_type: RootEnvironmentUrlType | None = None


class GitRootState(NamedTuple):
    branch: str
    includes_health_check: bool
    modified_by: str
    modified_date: datetime
    nickname: str
    status: RootStatus
    url: str
    credential_id: str | None = None
    criticality: RootCriticality | None = None
    gitignore: list[str] = []
    other: str | None = None
    reason: RootStateReason | None = None
    use_egress: bool = False
    use_vpn: bool = False
    use_ztna: bool = False


class GitRoot(NamedTuple):
    cloning: GitRootCloning
    created_by: str
    created_date: datetime
    group_name: str
    id: str
    organization_name: str
    state: GitRootState
    type: Literal[RootType.GIT]
    machine: GitRootMachine | None = None
    rebase: GitRootRebase | None = None
    toe_lines: GitRootToeLines | None = None
    unreliable_indicators: RootUnreliableIndicators = RootUnreliableIndicators()

    def __eq__(self, other: object) -> bool:
        if not isinstance(other, GitRoot):
            return False
        return self.id == other.id

    def __hash__(self) -> int:
        return hash(self.id)


class IPRootState(NamedTuple):
    address: str
    modified_by: str
    modified_date: datetime
    nickname: str
    status: RootStatus
    other: str | None = None
    reason: RootStateReason | None = None


class IPRoot(NamedTuple):
    created_by: str
    created_date: datetime
    group_name: str
    id: str
    organization_name: str
    state: IPRootState
    type: Literal[RootType.IP]
    unreliable_indicators: RootUnreliableIndicators = RootUnreliableIndicators()


class URLRootState(NamedTuple):
    host: str
    modified_by: str
    modified_date: datetime
    nickname: str
    path: str
    port: str
    protocol: str
    status: RootStatus
    excluded_sub_paths: list[str] | None = None
    other: str | None = None
    query: str | None = None
    reason: RootStateReason | None = None


class URLRoot(NamedTuple):
    created_by: str
    created_date: datetime
    group_name: str
    id: str
    organization_name: str
    state: URLRootState
    type: Literal[RootType.URL]
    unreliable_indicators: RootUnreliableIndicators = RootUnreliableIndicators()


Root = GitRoot | IPRoot | URLRoot


class RootState(NamedTuple):
    modified_by: str
    modified_date: datetime
    nickname: str | None
    status: RootStatus
    other: str | None = None
    reason: str | None = None
    # GitRoot
    credential_id: str | None = None


class RootRequest(NamedTuple):
    group_name: str
    root_id: str


class RootEnvironmentSecretsRequest(NamedTuple):
    group_name: str
    url_id: str


class RootEnvironmentUrlRequest(NamedTuple):
    root_id: str
    group_name: str
    url_id: str


class RootEnvironmentUrlsRequest(NamedTuple):
    root_id: str
    group_name: str


class GroupEnvironmentUrlsRequest(NamedTuple):
    group_name: str


class RootDockerImageRequest(NamedTuple):
    root_id: str
    group_name: str
    uri_id: str


class RootDockerImagesRequest(NamedTuple):
    root_id: str
    group_name: str


class GroupDockerImagesRequest(NamedTuple):
    group_name: str


class DockerImageLayersRequest(NamedTuple):
    group_name: str
    root_id: str
    image_uri: str


class GitRootEdge(NamedTuple):
    node: GitRoot
    cursor: str


class GitRootsConnection(NamedTuple):
    edges: tuple[GitRootEdge, ...]
    page_info: PageInfo
    total: int | None = None


class URLRootEdge(NamedTuple):
    node: URLRoot
    cursor: str


class URLRootsConnection(NamedTuple):
    edges: tuple[URLRootEdge, ...]
    page_info: PageInfo
    total: int | None = None


class IPRootEdge(NamedTuple):
    node: IPRoot
    cursor: str


class IPRootsConnection(NamedTuple):
    edges: tuple[IPRootEdge, ...]
    page_info: PageInfo
    total: int | None = None


RootItem = GitRootItem | IpRootItem | UrlRootItem
