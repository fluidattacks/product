from integrates.custom_exceptions import RepeatedRoot
from integrates.db_model.roots.add import add
from integrates.db_model.roots.enums import RootStatus
from integrates.db_model.roots.types import GitRoot
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import GitRootFaker, GitRootStateFaker, GroupFaker, OrganizationFaker
from integrates.testing.mocks import mocks
from integrates.testing.utils import parametrize, raises


@parametrize(
    args=["root"],
    cases=[
        # No the same root in the same group
        [
            GitRootFaker(
                group_name="group1",
                state=GitRootStateFaker(url="https://github.com/a/a", branch="main"),
            )
        ],
        # No the same root in the same group (even inactive)
        [
            GitRootFaker(
                group_name="group1",
                state=GitRootStateFaker(url="https://github.com/n/n", branch="main"),
            )
        ],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            groups=[
                GroupFaker(name="group1", organization_id="org1"),
                GroupFaker(name="group2", organization_id="org1"),
            ],
            roots=[
                GitRootFaker(
                    id="root1",
                    group_name="group1",
                    state=GitRootStateFaker(url="https://github.com/a/a", branch="main"),
                ),
                GitRootFaker(
                    id="root2",
                    group_name="group1",
                    state=GitRootStateFaker(
                        url="https://github.com/n/n", status=RootStatus.ACTIVE, branch="main"
                    ),
                ),
            ],
        )
    )
)
async def test_add_duplicated_git_root_fail(root: GitRoot) -> None:
    # Act
    with raises(RepeatedRoot):
        await add(root=root)
