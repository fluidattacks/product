from enum import (
    Enum,
)


class RootCloningStatus(str, Enum):
    CLONING: str = "CLONING"
    FAILED: str = "FAILED"
    OK: str = "OK"
    QUEUED: str = "QUEUED"
    UNKNOWN: str = "UNKNOWN"


class RootCriticality(str, Enum):
    CRITICAL: str = "CRITICAL"
    LOW: str = "LOW"
    MEDIUM: str = "MEDIUM"
    HIGH: str = "HIGH"


class RootEnvironmentUrlType(str, Enum):
    URL: str = "URL"
    CSPM: str = "CSPM"
    DATABASE: str = "DATABASE"
    APK: str = "APK"


class RootEnvironmentCloud(str, Enum):
    AWS: str = "AWS"
    GCP: str = "GCP"
    AZURE: str = "AZURE"
    KUBERNETES: str = "KUBERNETES"


class RootMachineJobType(str, Enum):
    FULL: str = "FULL"
    REATTACK: str = "REATTACK"


class RootMachineStatus(str, Enum):
    FAILED: str = "FAILED"
    IN_PROGRESS: str = "IN_PROGRESS"
    SUCCESS: str = "SUCCESS"


class RootRebaseStatus(str, Enum):
    FAILED: str = "FAILED"
    IN_PROGRESS: str = "IN_PROGRESS"
    SUCCESS: str = "SUCCESS"


class RootStateReason(str, Enum):
    GROUP_CONFIG_CHANGED: str = "GROUP_CONFIG_CHANGED"
    GROUP_DELETED: str = "GROUP_DELETED"
    MOVED_FROM_ANOTHER_GROUP: str = "MOVED_FROM_ANOTHER_GROUP"
    MOVED_TO_ANOTHER_GROUP: str = "MOVED_TO_ANOTHER_GROUP"
    OTHER: str = "OTHER"
    OUT_OF_SCOPE: str = "OUT_OF_SCOPE"
    REGISTERED_BY_MISTAKE: str = "REGISTERED_BY_MISTAKE"


class RootStatus(str, Enum):
    ACTIVE: str = "ACTIVE"
    INACTIVE: str = "INACTIVE"


class RootToeLinesStatus(str, Enum):
    FAILED: str = "FAILED"
    IN_PROGRESS: str = "IN_PROGRESS"
    SUCCESS: str = "SUCCESS"


class RootType(str, Enum):
    GIT: str = "Git"
    IP: str = "IP"
    URL: str = "URL"


class RootEnvironmentUrlStateStatus(str, Enum):
    CREATED: str = "CREATED"
    DELETED: str = "DELETED"


class RootDockerImageStateStatus(str, Enum):
    CREATED: str = "CREATED"
    DELETED: str = "DELETED"
