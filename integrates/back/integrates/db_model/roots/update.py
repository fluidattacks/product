import simplejson as json
from boto3.dynamodb.conditions import Attr, ConditionBase, Key

from integrates.audit import AuditEvent, add_audit_event
from integrates.custom_exceptions import (
    EmptyHistoric,
    ErrorUpdatingRootItem,
    ErrorUpdatingSecret,
    IndicatorAlreadyUpdated,
    RepeatedRoot,
    RootDockerImageNotFound,
    RootEnvironmentUrlNotFound,
    RootNotFound,
)
from integrates.db_model import TABLE
from integrates.db_model.roots.types import (
    GitRootCloning,
    GitRootMachine,
    GitRootRebase,
    GitRootState,
    GitRootToeLines,
    IPRootState,
    RootDockerImageState,
    RootEnvironmentUrlState,
    RootUnreliableIndicators,
    RootUnreliableIndicatorsToUpdate,
    Secret,
    SecretState,
    URLRootState,
)
from integrates.db_model.roots.utils import get_gsi_3, get_uniqueness_constraint
from integrates.db_model.utils import adjust_historic_dates, get_as_utc_iso_format, serialize
from integrates.dynamodb import keys, operations
from integrates.dynamodb.exceptions import ConditionalCheckFailedException


async def update_root_environment_url_state(
    *,
    current_value: RootEnvironmentUrlState,
    root_id: str,
    group_name: str,
    state: RootEnvironmentUrlState,
    url_id: str,
) -> None:
    key_structure = TABLE.primary_key
    state_item = json.loads(json.dumps(state, default=serialize))
    item = {
        "state": state_item,
    }
    condition_expression = Attr(key_structure.partition_key).exists() & Attr(
        "state.modified_date",
    ).eq(get_as_utc_iso_format(current_value.modified_date))
    primary_key = keys.build_key(
        facet=TABLE.facets["root_environment_url"],
        values={"uuid": root_id, "group_name": group_name, "hash": url_id},
    )
    try:
        await operations.update_item(
            condition_expression=condition_expression,
            item=item,
            key=primary_key,
            table=TABLE,
        )
        add_audit_event(
            AuditEvent(
                action="UPDATE",
                author=state.modified_by,
                metadata=state_item,
                object="RootEnvironment",
                object_id=url_id,
            )
        )
    except ConditionalCheckFailedException as ex:
        raise RootEnvironmentUrlNotFound() from ex


async def update_root_docker_image_state(
    *,
    current_value: RootDockerImageState,
    root_id: str,
    group_name: str,
    state: RootDockerImageState,
    uri: str,
) -> None:
    key_structure = TABLE.primary_key
    state_item = json.loads(json.dumps(state, default=serialize))
    item = {
        "state": state_item,
    }
    condition_expression = Attr(key_structure.partition_key).exists() & Attr(
        "state.modified_date",
    ).eq(get_as_utc_iso_format(current_value.modified_date))
    primary_key = keys.build_key(
        facet=TABLE.facets["root_docker_image"],
        values={"uuid": root_id, "group_name": group_name, "uri": uri},
    )
    try:
        await operations.update_item(
            condition_expression=condition_expression,
            item=item,
            key=primary_key,
            table=TABLE,
        )
        add_audit_event(
            AuditEvent(
                action="UPDATE",
                author=state.modified_by,
                metadata=state_item,
                object="RootDockerImage",
                object_id=uri,
            )
        )
    except ConditionalCheckFailedException as ex:
        raise RootDockerImageNotFound() from ex

    historic_state_key = keys.build_key(
        facet=TABLE.facets["root_docker_image_historic_state"],
        values={
            "group_name": group_name,
            "uuid": root_id,
            "uri": uri,
            "iso8601utc": get_as_utc_iso_format(state.modified_date),
        },
    )
    historic_item = {
        key_structure.partition_key: historic_state_key.partition_key,
        key_structure.sort_key: historic_state_key.sort_key,
        **state_item,
    }
    await operations.put_item(
        facet=TABLE.facets["root_docker_image_historic_state"],
        item=historic_item,
        table=TABLE,
    )


async def update_url_pk(
    current_value: GitRootState | IPRootState | URLRootState,
    group_name: str,
    state: GitRootState | IPRootState | URLRootState,
) -> None:
    if (
        isinstance(state, GitRootState)
        and isinstance(current_value, GitRootState)
        and (current_value.url != state.url or current_value.branch != state.branch)
    ):
        try:
            new_url_pk = get_uniqueness_constraint(group_name=group_name, state=state)
            await operations.put_item(
                condition_expression=Attr("pk").not_exists(),
                facet=TABLE.facets["git_root_metadata"],
                item=new_url_pk,
                table=TABLE,
            )
            url_pk = f"URL#{current_value.url}#BRANCH#{current_value.branch}"
            current_url_key = keys.build_key(
                facet=TABLE.facets["git_root_metadata"],
                values={
                    "name": group_name,
                    "uuid": url_pk,
                },
            )
            await operations.delete_item(key=current_url_key, table=TABLE)

        except ConditionalCheckFailedException as ex:
            raise RepeatedRoot() from ex


async def update_root_state(
    *,
    current_value: GitRootState | IPRootState | URLRootState,
    group_name: str,
    root_id: str,
    state: GitRootState | IPRootState | URLRootState,
) -> None:
    key_structure = TABLE.primary_key
    root_facets = {
        GitRootState: (
            TABLE.facets["git_root_metadata"],
            TABLE.facets["git_root_historic_state"],
        ),
        IPRootState: (
            TABLE.facets["ip_root_metadata"],
            TABLE.facets["ip_root_historic_state"],
        ),
        URLRootState: (
            TABLE.facets["url_root_metadata"],
            TABLE.facets["url_root_historic_state"],
        ),
    }
    metadata_facet, historic_facet = root_facets[type(state)]
    state_item = json.loads(json.dumps(state, default=serialize))

    root_key = keys.build_key(
        facet=metadata_facet,
        values={"name": group_name, "uuid": root_id},
    )
    root_item = {
        **(
            get_gsi_3(group_name=group_name, state=state) if isinstance(state, GitRootState) else {}
        ),
        "state": state_item,
    }

    await update_url_pk(current_value, group_name, state)

    await operations.update_item(
        condition_expression=(
            Attr(key_structure.partition_key).exists()
            & Attr("state.modified_date").eq(get_as_utc_iso_format(current_value.modified_date))
        ),
        item=root_item,
        key=root_key,
        table=TABLE,
    )
    add_audit_event(
        AuditEvent(
            action="UPDATE",
            author=state.modified_by,
            metadata=state_item,
            object="Root",
            object_id=root_id,
        )
    )

    historic_key = keys.build_key(
        facet=historic_facet,
        values={
            "uuid": root_id,
            "iso8601utc": get_as_utc_iso_format(state.modified_date),
        },
    )
    historic_item = {
        key_structure.partition_key: historic_key.partition_key,
        key_structure.sort_key: historic_key.sort_key,
        **state_item,
    }
    await operations.put_item(
        facet=historic_facet,
        item=historic_item,
        table=TABLE,
    )


async def update_root_historic_state(
    *,
    current_state: GitRootState | IPRootState | URLRootState,
    group_name: str,
    root_id: str,
    historic_state: tuple[GitRootState | IPRootState | URLRootState, ...],
) -> None:
    if not historic_state:
        raise EmptyHistoric()

    key_structure = TABLE.primary_key
    root_facets = {
        GitRootState: (
            TABLE.facets["git_root_metadata"],
            TABLE.facets["git_root_historic_state"],
        ),
        IPRootState: (
            TABLE.facets["ip_root_metadata"],
            TABLE.facets["ip_root_historic_state"],
        ),
        URLRootState: (
            TABLE.facets["url_root_metadata"],
            TABLE.facets["url_root_historic_state"],
        ),
    }
    historic_state = adjust_historic_dates(historic_state)
    metadata_facet, historic_facet = root_facets[type(historic_state[-1])]
    root_metadata_key = keys.build_key(
        facet=metadata_facet,
        values={"name": group_name, "uuid": root_id},
    )

    await update_url_pk(current_state, group_name, historic_state[-1])

    try:
        await operations.update_item(
            condition_expression=Attr(key_structure.partition_key).exists(),
            item={"state": json.loads(json.dumps(historic_state[-1], default=serialize))},
            key=root_metadata_key,
            table=TABLE,
        )
    except ConditionalCheckFailedException as ex:
        raise RootNotFound() from ex

    historic_key = keys.build_key(
        facet=historic_facet,
        values={
            "uuid": root_id,
        },
    )
    response = await operations.query(
        condition_expression=(
            Key(key_structure.partition_key).eq(historic_key.partition_key)
            & Key(key_structure.sort_key).begins_with(historic_key.sort_key)
        ),
        facets=(historic_facet,),
        table=TABLE,
    )
    current_keys = {
        keys.build_key(
            facet=historic_facet,
            values={
                "uuid": root_id,
                "iso8601utc": item["modified_date"],
            },
        )
        for item in response.items
    }
    new_keys = tuple(
        keys.build_key(
            facet=historic_facet,
            values={
                "uuid": root_id,
                "iso8601utc": get_as_utc_iso_format(entry.modified_date),
            },
        )
        for entry in historic_state
    )
    new_items = tuple(
        {
            key_structure.partition_key: key.partition_key,
            key_structure.sort_key: key.sort_key,
            **json.loads(json.dumps(entry, default=serialize)),
        }
        for key, entry in zip(new_keys, historic_state, strict=False)
    )
    await operations.batch_put_item(items=new_items, table=TABLE)
    await operations.batch_delete_item(
        keys=tuple(key for key in current_keys if key not in new_keys),
        table=TABLE,
    )


async def update_environment_url_secret(
    group_name: str,
    url_id: str,
    secret: Secret,
    current_value: SecretState,
) -> None:
    key_structure = TABLE.primary_key
    secret_key = keys.build_key(
        facet=TABLE.facets["root_environment_secret"],
        values={
            "group_name": group_name,
            "hash": url_id,
            "key": secret.key,
        },
    )
    secret_item = json.loads(json.dumps(secret, default=serialize))
    condition_expression: ConditionBase = Attr(key_structure.partition_key).exists()
    condition_expression &= Attr("state.modified_date").eq(
        get_as_utc_iso_format(current_value.modified_date),
    )
    try:
        await operations.update_item(
            condition_expression=condition_expression,
            item=secret_item,
            key=secret_key,
            table=TABLE,
        )
        add_audit_event(
            AuditEvent(
                action="UPDATE",
                author=secret.state.modified_by,
                metadata=secret_item,
                object="RootEnvironmentSecret",
                object_id=secret.key,
            )
        )
    except ConditionalCheckFailedException as ex:
        raise ErrorUpdatingSecret().new() from ex


async def update_root_secret(resource_id: str, secret: Secret, current_value: SecretState) -> None:
    key_structure = TABLE.primary_key
    secret_key = keys.build_key(
        facet=TABLE.facets["root_secret"],
        values={"uuid": resource_id, "key": secret.key},
    )
    secret_item = json.loads(json.dumps(secret, default=serialize))
    condition_expression: ConditionBase = Attr(key_structure.partition_key).exists()
    condition_expression &= Attr("state.modified_date").eq(
        get_as_utc_iso_format(current_value.modified_date),
    )
    try:
        await operations.update_item(
            condition_expression=condition_expression,
            item=secret_item,
            key=secret_key,
            table=TABLE,
        )
        add_audit_event(
            AuditEvent(
                action="UPDATE",
                author=secret.state.modified_by,
                metadata=secret_item,
                object="RootSecret",
                object_id=secret.key,
            )
        )
    except ConditionalCheckFailedException as ex:
        raise ErrorUpdatingSecret().new() from ex


async def update_git_root_cloning(
    *,
    cloning: GitRootCloning,
    current_value: GitRootCloning,
    group_name: str,
    root_id: str,
) -> None:
    key_structure = TABLE.primary_key
    cloning_item = json.loads(json.dumps(cloning, default=serialize))

    root_key = keys.build_key(
        facet=TABLE.facets["git_root_metadata"],
        values={"name": group_name, "uuid": root_id},
    )
    root_item = {"cloning": cloning_item}

    try:
        await operations.update_item(
            condition_expression=(
                Attr(key_structure.partition_key).exists()
                & Attr("cloning.modified_date").eq(
                    get_as_utc_iso_format(current_value.modified_date),
                )
            ),
            item=root_item,
            key=root_key,
            table=TABLE,
        )
        add_audit_event(
            AuditEvent(
                action="UPDATE",
                author=cloning.modified_by,
                metadata=root_item,
                object="Root",
                object_id=root_id,
            )
        )
    except ConditionalCheckFailedException as ex:
        raise ErrorUpdatingRootItem().new() from ex


async def update_git_root_machine(
    *,
    current_value: GitRootMachine | None,
    group_name: str,
    machine: GitRootMachine,
    root_id: str,
) -> None:
    key_structure = TABLE.primary_key
    primary_key = keys.build_key(
        facet=TABLE.facets["git_root_metadata"],
        values={"name": group_name, "uuid": root_id},
    )
    root_item = {"machine": json.loads(json.dumps(machine, default=serialize))}
    condition_expression: ConditionBase = Attr(key_structure.partition_key).exists()
    condition_expression &= (
        Attr("machine.modified_date").eq(get_as_utc_iso_format(current_value.modified_date))
        if current_value
        else Attr("machine").not_exists()
    )
    try:
        await operations.update_item(
            condition_expression=condition_expression,
            item=root_item,
            key=primary_key,
            table=TABLE,
        )
        add_audit_event(
            AuditEvent(
                action="UPDATE",
                author=machine.modified_by,
                metadata=root_item,
                object="Root",
                object_id=root_id,
            )
        )
    except ConditionalCheckFailedException as ex:
        raise ErrorUpdatingRootItem().new() from ex


async def update_git_root_rebase(
    *,
    current_value: GitRootRebase | None,
    group_name: str,
    rebase: GitRootRebase,
    root_id: str,
) -> None:
    key_structure = TABLE.primary_key
    primary_key = keys.build_key(
        facet=TABLE.facets["git_root_metadata"],
        values={"name": group_name, "uuid": root_id},
    )
    root_item = {"rebase": json.loads(json.dumps(rebase, default=serialize))}
    condition_expression: ConditionBase = Attr(key_structure.partition_key).exists()
    condition_expression &= (
        Attr("rebase.modified_date").eq(get_as_utc_iso_format(current_value.modified_date))
        if current_value
        else Attr("rebase").not_exists()
    )
    try:
        await operations.update_item(
            condition_expression=condition_expression,
            item=root_item,
            key=primary_key,
            table=TABLE,
        )
        add_audit_event(
            AuditEvent(
                action="UPDATE",
                author=rebase.modified_by,
                metadata=root_item,
                object="Root",
                object_id=root_id,
            )
        )
    except ConditionalCheckFailedException as ex:
        raise ErrorUpdatingRootItem().new() from ex


async def update_git_root_toe_lines(
    *,
    current_value: GitRootToeLines | None,
    group_name: str,
    root_id: str,
    toe_lines: GitRootToeLines,
) -> None:
    key_structure = TABLE.primary_key
    primary_key = keys.build_key(
        facet=TABLE.facets["git_root_metadata"],
        values={"name": group_name, "uuid": root_id},
    )
    root_item = {"toe_lines": json.loads(json.dumps(toe_lines, default=serialize))}
    condition_expression: ConditionBase = Attr(key_structure.partition_key).exists()
    condition_expression &= (
        Attr("toe_lines.modified_date").eq(get_as_utc_iso_format(current_value.modified_date))
        if current_value
        else Attr("toe_lines").not_exists()
    )
    try:
        await operations.update_item(
            condition_expression=condition_expression,
            item=root_item,
            key=primary_key,
            table=TABLE,
        )
        add_audit_event(
            AuditEvent(
                action="UPDATE",
                author=toe_lines.modified_by,
                metadata=root_item,
                object="Root",
                object_id=root_id,
            )
        )
    except ConditionalCheckFailedException as ex:
        raise ErrorUpdatingRootItem().new() from ex


async def update_unreliable_indicators(
    *,
    current_value: RootUnreliableIndicators,
    group_name: str,
    indicators: RootUnreliableIndicatorsToUpdate,
    root_id: str,
) -> None:
    key_structure = TABLE.primary_key
    primary_key = keys.build_key(
        facet=TABLE.facets["git_root_metadata"],
        values={"name": group_name, "uuid": root_id},
    )
    unreliable_indicators_item = {
        f"unreliable_indicators.{key}": value
        for key, value in json.loads(json.dumps(indicators, default=serialize)).items()
        if value is not None
    }
    if not unreliable_indicators_item:
        return

    current_indicators_item = {
        f"unreliable_indicators.{key}": value
        for key, value in json.loads(json.dumps(current_value, default=serialize)).items()
        if value is not None
    }
    conditions = (
        Attr(indicator_name).eq(current_indicators_item[indicator_name])
        for indicator_name in unreliable_indicators_item
        if indicator_name in current_indicators_item
    )
    condition_expression: ConditionBase = Attr(key_structure.partition_key).exists()
    for condition in conditions:
        condition_expression &= condition

    try:
        await operations.update_item(
            condition_expression=condition_expression,
            item=unreliable_indicators_item,
            key=primary_key,
            table=TABLE,
        )
    except ConditionalCheckFailedException as ex:
        raise IndicatorAlreadyUpdated() from ex


async def update_root_gsi_3(
    *,
    current_value: GitRootState,
    group_name: str,
    root_id: str,
    state: GitRootState,
) -> None:
    key_structure = TABLE.primary_key
    metadata_facet = TABLE.facets["git_root_metadata"]

    root_key = keys.build_key(
        facet=metadata_facet,
        values={"name": group_name, "uuid": root_id},
    )
    root_item = get_gsi_3(group_name=group_name, state=state)

    await operations.update_item(
        condition_expression=(
            Attr(key_structure.partition_key).exists()
            & Attr("state.modified_date").eq(get_as_utc_iso_format(current_value.modified_date))
        ),
        item=root_item,
        key=root_key,
        table=TABLE,
    )
