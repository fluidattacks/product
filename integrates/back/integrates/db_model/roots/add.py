import simplejson as json
from boto3.dynamodb.conditions import Attr

from integrates.audit import AuditEvent, add_audit_event
from integrates.custom_exceptions import RepeatedRoot, SecretAlreadyExists
from integrates.db_model import TABLE
from integrates.db_model.roots.constants import (
    GROUP_ENVS_INDEX_METADATA,
    GROUP_IMAGES_INDEX_METADATA,
    ORG_INDEX_METADATA,
)
from integrates.db_model.roots.enums import (
    RootDockerImageStateStatus,
    RootEnvironmentUrlStateStatus,
)
from integrates.db_model.roots.types import (
    GitRoot,
    Root,
    RootDockerImage,
    RootDockerImageLayer,
    RootEnvironmentUrl,
    Secret,
)
from integrates.db_model.roots.utils import get_gsi_3, get_uniqueness_constraint
from integrates.db_model.utils import get_as_utc_iso_format, serialize
from integrates.dynamodb import keys, operations
from integrates.dynamodb.exceptions import ConditionalCheckFailedException


async def _add_uniqueness_item(root: Root) -> None:
    if isinstance(root, GitRoot):
        try:
            url_pk_metadata = get_uniqueness_constraint(
                group_name=root.group_name, state=root.state
            )
            await operations.put_item(
                condition_expression=Attr("pk").not_exists(),
                facet=TABLE.facets["git_root_metadata"],
                item=url_pk_metadata,
                table=TABLE,
            )
        except ConditionalCheckFailedException as ex:
            raise RepeatedRoot() from ex


async def add(*, root: Root) -> None:
    items = []
    key_structure = TABLE.primary_key
    metadata_key = keys.build_key(
        facet=TABLE.facets["git_root_metadata"],
        values={"name": root.group_name, "uuid": root.id},
    )
    gsi_2_index = TABLE.indexes["gsi_2"]
    gsi_2_key = keys.build_key(
        facet=ORG_INDEX_METADATA,
        values={"name": root.organization_name, "uuid": root.id},
    )
    initial_metadata = {
        key_structure.partition_key: metadata_key.partition_key,
        key_structure.sort_key: metadata_key.sort_key,
        gsi_2_index.primary_key.partition_key: gsi_2_key.partition_key,
        gsi_2_index.primary_key.sort_key: gsi_2_key.sort_key,
        **(
            get_gsi_3(group_name=root.group_name, state=root.state)
            if isinstance(root, GitRoot)
            else {}
        ),
        **json.loads(json.dumps(root, default=serialize)),
    }

    await _add_uniqueness_item(root=root)

    items.append(initial_metadata)

    state_key = keys.build_key(
        facet=TABLE.facets["git_root_historic_state"],
        values={
            "uuid": root.id,
            "iso8601utc": get_as_utc_iso_format(root.state.modified_date),
        },
    )
    historic_state_item = {
        key_structure.partition_key: state_key.partition_key,
        key_structure.sort_key: state_key.sort_key,
        **json.loads(json.dumps(root.state, default=serialize)),
    }
    items.append(historic_state_item)

    await operations.batch_put_item(items=tuple(items), table=TABLE)
    add_audit_event(
        AuditEvent(
            action="CREATE",
            author=root.created_by,
            metadata=initial_metadata,
            object="Root",
            object_id=root.id,
        )
    )


async def add_secret(
    resource_id: str,
    secret: Secret,
) -> None:
    key_structure = TABLE.primary_key
    secret_key = keys.build_key(
        facet=TABLE.facets["root_secret"],
        values={"uuid": resource_id, "key": secret.key},
    )
    secret_item = {
        key_structure.partition_key: secret_key.partition_key,
        key_structure.sort_key: secret_key.sort_key,
        **json.loads(json.dumps(secret, default=serialize)),
    }
    condition_expresion = Attr(key_structure.partition_key).not_exists()
    try:
        await operations.put_item(
            condition_expression=condition_expresion,
            facet=TABLE.facets["root_secret"],
            item=secret_item,
            table=TABLE,
        )
        add_audit_event(
            AuditEvent(
                action="CREATE",
                author=secret.state.modified_by,
                metadata=secret_item,
                object="RootSecret",
                object_id=secret.key,
            )
        )
    except ConditionalCheckFailedException as ex:
        raise SecretAlreadyExists() from ex


async def add_root_environment_secret(
    group_name: str,
    resource_id: str,
    secret: Secret,
) -> None:
    key_structure = TABLE.primary_key
    secret_key = keys.build_key(
        facet=TABLE.facets["root_environment_secret"],
        values={
            "group_name": group_name,
            "hash": resource_id,
            "key": secret.key,
        },
    )
    secret_item = {
        key_structure.partition_key: secret_key.partition_key,
        key_structure.sort_key: secret_key.sort_key,
        **json.loads(json.dumps(secret, default=serialize)),
    }
    condition_expresion = Attr(key_structure.partition_key).not_exists()
    try:
        await operations.put_item(
            condition_expression=condition_expresion,
            facet=TABLE.facets["root_environment_secret"],
            item=secret_item,
            table=TABLE,
        )
        add_audit_event(
            AuditEvent(
                action="CREATE",
                author=secret.state.modified_by,
                metadata=secret_item,
                object="RootEnvironmentSecret",
                object_id=secret.key,
            )
        )
    except ConditionalCheckFailedException as ex:
        raise SecretAlreadyExists() from ex


async def add_root_environment_url(
    *,
    group_name: str,
    root_id: str,
    url: RootEnvironmentUrl,
) -> bool:
    key_structure = TABLE.primary_key
    gsi_2_index = TABLE.indexes["gsi_2"]
    url_key = keys.build_key(
        facet=TABLE.facets["root_environment_url"],
        values={"uuid": root_id, "group_name": group_name, "hash": url.id},
    )
    gsi_2_key = keys.build_key(
        facet=GROUP_ENVS_INDEX_METADATA,
        values={"group_name": group_name, "hash": url.id},
    )

    url_item = {
        key_structure.partition_key: url_key.partition_key,
        key_structure.sort_key: url_key.sort_key,
        gsi_2_index.primary_key.partition_key: gsi_2_key.partition_key,
        gsi_2_index.primary_key.sort_key: gsi_2_key.sort_key,
        "id": url.id,
        "group_name": group_name,
        "root_id": root_id,
        "url": url.url,
        "created_at": get_as_utc_iso_format(url.created_at) if url.created_at else None,
        "created_by": url.created_by,
        "state": json.loads(json.dumps(url.state, default=serialize)),
    }

    condition_expression = Attr("state.status").ne(RootEnvironmentUrlStateStatus.CREATED.value)
    try:
        await operations.put_item(
            condition_expression=condition_expression,
            facet=TABLE.facets["root_environment_url"],
            item=url_item,
            table=TABLE,
        )
        add_audit_event(
            AuditEvent(
                action="CREATE",
                author=url.state.modified_by,
                metadata=url_item,
                object="RootEnvironment",
                object_id=url.id,
            )
        )
        return True
    except ConditionalCheckFailedException:
        return False


async def add_root_docker_image(
    *,
    group_name: str,
    root_id: str,
    image: RootDockerImage,
) -> bool:
    key_structure = TABLE.primary_key
    gsi_2_index = TABLE.indexes["gsi_2"]
    image_key = keys.build_key(
        facet=TABLE.facets["root_docker_image"],
        values={"uuid": root_id, "group_name": group_name, "uri": image.uri},
    )
    gsi_2_key = keys.build_key(
        facet=GROUP_IMAGES_INDEX_METADATA,
        values={"group_name": group_name, "uri": image.uri},
    )

    image_item = {
        key_structure.partition_key: image_key.partition_key,
        key_structure.sort_key: image_key.sort_key,
        gsi_2_index.primary_key.partition_key: gsi_2_key.partition_key,
        gsi_2_index.primary_key.sort_key: gsi_2_key.sort_key,
        "id": image.uri,
        "group_name": group_name,
        "root_id": root_id,
        "created_at": get_as_utc_iso_format(image.created_at) if image.created_at else None,
        "created_by": image.created_by,
        "state": json.loads(json.dumps(image.state, default=serialize)),
    }

    condition_expression = Attr("state.status").ne(RootDockerImageStateStatus.CREATED.value)
    try:
        await operations.put_item(
            condition_expression=condition_expression,
            facet=TABLE.facets["root_docker_image"],
            item=image_item,
            table=TABLE,
        )
        add_audit_event(
            AuditEvent(
                action="CREATE",
                author=image.state.modified_by,
                metadata=image_item,
                object="RootDockerImage",
                object_id=image.uri,
            )
        )
        return True
    except ConditionalCheckFailedException:
        return False


async def add_root_docker_image_layer(
    *,
    group_name: str,
    root_id: str,
    layer: RootDockerImageLayer,
) -> bool:
    key_structure = TABLE.primary_key
    layer_key = keys.build_key(
        facet=TABLE.facets["root_docker_image_layer"],
        values={
            "uuid": root_id,
            "group_name": group_name,
            "uri": layer.image_uri,
            "digest": layer.digest,
        },
    )

    image_item = {
        key_structure.partition_key: layer_key.partition_key,
        key_structure.sort_key: layer_key.sort_key,
        "digest": layer.digest,
        "size": layer.size,
    }

    try:
        await operations.put_item(
            facet=TABLE.facets["root_docker_image_layer"],
            item=image_item,
            table=TABLE,
        )
        return True
    except ConditionalCheckFailedException:
        return False
