from integrates.custom_exceptions import SecretValueNotFound
from integrates.db_model.roots.get import get_secret_by_key
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import GroupFaker
from integrates.testing.mocks import mocks
from integrates.testing.utils import parametrize, raises
from integrates.verify.enums import ResourceType


@parametrize(args=["resource_type"], cases=[[ResourceType.ROOT], [ResourceType.URL]])
@mocks(aws=IntegratesAws(dynamodb=IntegratesDynamodb(groups=[GroupFaker(name="group_name")])))
async def test_get_secret_by_key(resource_type: ResourceType) -> None:
    with raises(SecretValueNotFound):
        await get_secret_by_key(
            group_name="group_name",
            resource_id="resource_id",
            resource_type=resource_type,
            secret_key="",
        )
