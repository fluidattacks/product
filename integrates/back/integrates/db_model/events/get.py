from collections.abc import (
    Iterable,
)

from aiodataloader import (
    DataLoader,
)
from aioextensions import (
    collect,
)
from boto3.dynamodb.conditions import (
    ConditionBase,
    Key,
)

from integrates.db_model import (
    HISTORIC_TABLE,
    TABLE,
)
from integrates.db_model.events.constants import (
    EVENT_INDEX_METADATA,
    GSI_2_FACET,
)
from integrates.db_model.events.types import (
    Event,
    EventRequest,
    EventState,
    GroupEventsRequest,
)
from integrates.db_model.events.utils import (
    format_event,
    format_state,
)
from integrates.db_model.historic_items import EventStateItem
from integrates.db_model.items import EventItem
from integrates.dynamodb import (
    keys,
    operations,
)


async def _get_event(*, request: EventRequest) -> Event | None:
    primary_key = keys.build_key(
        facet=TABLE.facets["event_metadata"],
        values={
            "id": request.event_id,
            "name": request.group_name,
        },
    )

    key_structure = TABLE.primary_key
    response = await operations.DynamoClient[EventItem].query(
        condition_expression=(
            Key(key_structure.partition_key).eq(primary_key.partition_key)
            & Key(key_structure.sort_key).eq(primary_key.sort_key)
        ),
        facets=(TABLE.facets["event_metadata"],),
        limit=1,
        table=TABLE,
    )

    if not response.items:
        return None

    return format_event(response.items[0])


class EventLoader(DataLoader[EventRequest, Event | None]):
    async def batch_load_fn(
        self,
        requests: Iterable[EventRequest],
    ) -> list[Event | None]:
        return list(
            await collect(
                tuple(_get_event(request=request) for request in requests),
                workers=32,
            ),
        )


async def _get_historic_state(
    *,
    request: EventRequest,
) -> list[EventState]:
    primary_key = keys.build_key(
        facet=HISTORIC_TABLE.facets["event_state"],
        values={
            "id": request.event_id,
            "group_name": request.group_name,
            "state": "state",
        },
    )
    key_structure = HISTORIC_TABLE.primary_key
    response = await operations.DynamoClient[EventStateItem].query(
        condition_expression=(
            Key(key_structure.partition_key).eq(primary_key.partition_key)
            & Key(key_structure.sort_key).begins_with(primary_key.sort_key)
        ),
        facets=(HISTORIC_TABLE.facets["event_state"],),
        table=HISTORIC_TABLE,
    )
    return list(map(format_state, response.items))


class EventsHistoricStateLoader(DataLoader[EventRequest, list[EventState]]):
    async def batch_load_fn(self, requests: Iterable[EventRequest]) -> list[list[EventState]]:
        return list(
            await collect(
                tuple(_get_historic_state(request=request) for request in requests),
                workers=32,
            ),
        )


async def _get_group_events(
    *,
    event_dataloader: EventLoader,
    request: GroupEventsRequest,
) -> list[Event]:
    if request.is_solved is None:
        facet = TABLE.facets["event_metadata"]
        primary_key = keys.build_key(
            facet=facet,
            values={"name": request.group_name},
        )
        index = TABLE.indexes["inverted_index"]
        key_structure = index.primary_key
        condition_expression: ConditionBase = Key(key_structure.partition_key).eq(
            primary_key.sort_key,
        ) & Key(key_structure.sort_key).begins_with(primary_key.partition_key)
    else:
        facet = GSI_2_FACET
        primary_key = keys.build_key(
            facet=facet,
            values={
                "group_name": request.group_name,
                "is_solved": str(request.is_solved).lower(),
            },
        )
        index = TABLE.indexes["gsi_2"]
        key_structure = index.primary_key
        condition_expression = Key(key_structure.partition_key).eq(primary_key.partition_key) & Key(
            key_structure.sort_key,
        ).begins_with(primary_key.sort_key)

    response = await operations.DynamoClient[EventItem].query(
        condition_expression=condition_expression,
        facets=(TABLE.facets["event_metadata"],),
        table=TABLE,
        index=index,
    )

    events: list[Event] = []
    for item in response.items:
        if request.is_solved is None:
            gsi_4_index = TABLE.indexes["gsi_4"]
            key_structure = gsi_4_index.primary_key
            gsi_4_key = keys.build_key(
                facet=EVENT_INDEX_METADATA,
                values={
                    "event_id": item["id"],
                    "vuln_id": "",
                },
            )
            condition_expression = Key(key_structure.partition_key).eq(gsi_4_key.partition_key)

            holds = await operations.query(
                condition_expression=condition_expression,
                facets=(TABLE.facets["vulnerability_metadata"],),
                table=TABLE,
                index=gsi_4_index,
            )

            item["n_holds"] = len(holds.items)

        event = format_event(item)
        events.append(event)
        event_dataloader.prime(
            EventRequest(event_id=event.id, group_name=request.group_name),
            event,
        )

    return events


class GroupEventsLoader(DataLoader[GroupEventsRequest, list[Event]]):
    def __init__(self, dataloader: EventLoader) -> None:
        super().__init__()
        self.dataloader = dataloader

    async def batch_load_fn(self, requests: Iterable[GroupEventsRequest]) -> list[list[Event]]:
        return list(
            await collect(
                tuple(
                    _get_group_events(
                        event_dataloader=self.dataloader,
                        request=request,
                    )
                    for request in requests
                ),
                workers=32,
            ),
        )
