from integrates.db_model import (
    TABLE,
)
from integrates.dynamodb.types import (
    Facet,
)

EVENT_INDEX_METADATA = Facet(
    attrs=TABLE.facets["vulnerability_metadata"].attrs,
    pk_alias="EVENT#event_id",
    sk_alias="VULN#vuln_id",
)

GSI_2_FACET = Facet(
    attrs=TABLE.facets["event_metadata"].attrs,
    pk_alias="GROUP#group_name",
    sk_alias="EVENT#SOLVED#is_solved",
)
