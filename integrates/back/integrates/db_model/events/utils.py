from datetime import (
    datetime,
)

import simplejson as json

from integrates.class_types.types import (
    Item,
)
from integrates.db_model.events.constants import (
    GSI_2_FACET,
)
from integrates.db_model.events.enums import (
    EventSolutionReason,
    EventStateStatus,
    EventType,
)
from integrates.db_model.events.types import (
    Event,
    EventEvidence,
    EventEvidences,
    EventMetadataToUpdate,
    EventState,
)
from integrates.db_model.groups.types import (
    Group,
)
from integrates.db_model.items import (
    EventEvidencesItem,
    EventHistoricStateItem,
    EventItem,
)
from integrates.db_model.utils import (
    get_as_utc_iso_format,
    serialize,
)
from integrates.dynamodb import (
    keys,
)
from integrates.dynamodb.types import (
    PrimaryKey,
)


def filter_events_not_in_groups(
    *,
    groups: list[Group],
    events: list[Event],
) -> list[Event]:
    return [
        event for event in events if event.group_name not in set(group.name for group in groups)
    ]


def format_evidences(evidences: EventEvidencesItem) -> EventEvidences:
    return EventEvidences(
        file_1=EventEvidence(
            file_name=evidences["file_1"]["file_name"],
            modified_date=datetime.fromisoformat(evidences["file_1"]["modified_date"]),
        )
        if evidences.get("file_1")
        else None,
        image_1=EventEvidence(
            file_name=evidences["image_1"]["file_name"],
            modified_date=datetime.fromisoformat(evidences["image_1"]["modified_date"]),
        )
        if evidences.get("image_1")
        else None,
        image_2=EventEvidence(
            file_name=evidences["image_2"]["file_name"],
            modified_date=datetime.fromisoformat(evidences["image_2"]["modified_date"]),
        )
        if evidences.get("image_2")
        else None,
        image_3=EventEvidence(
            file_name=evidences["image_3"]["file_name"],
            modified_date=datetime.fromisoformat(evidences["image_3"]["modified_date"]),
        )
        if evidences.get("image_3")
        else None,
        image_4=EventEvidence(
            file_name=evidences["image_4"]["file_name"],
            modified_date=datetime.fromisoformat(evidences["image_4"]["modified_date"]),
        )
        if evidences.get("image_4")
        else None,
        image_5=EventEvidence(
            file_name=evidences["image_5"]["file_name"],
            modified_date=datetime.fromisoformat(evidences["image_5"]["modified_date"]),
        )
        if evidences.get("image_5")
        else None,
        image_6=EventEvidence(
            file_name=evidences["image_6"]["file_name"],
            modified_date=datetime.fromisoformat(evidences["image_6"]["modified_date"]),
        )
        if evidences.get("image_6")
        else None,
    )


def format_event(item: EventItem) -> Event:
    return Event(
        client=item["client"],
        created_by=item["created_by"],
        created_date=datetime.fromisoformat(item["created_date"]),
        description=item["description"],
        environment_url=item.get("environment_url"),
        event_date=datetime.fromisoformat(item["event_date"]),
        evidences=format_evidences(item["evidences"]),
        group_name=item["group_name"],
        hacker=item["hacker"],
        id=item["id"],
        root_id=item.get("root_id"),
        state=EventState(
            comment_id=item["state"].get("comment_id"),
            modified_by=item["state"]["modified_by"],
            modified_date=datetime.fromisoformat(item["state"]["modified_date"]),
            status=EventStateStatus[item["state"]["status"]],
            other=item["state"].get("other"),
            reason=EventSolutionReason[item["state"]["reason"]]
            if item["state"].get("reason")
            else None,
        ),
        solving_date=_get_solving_date(item),
        type=EventType[item["type"]],
        n_holds=item["n_holds"] if "n_holds" in item else None,
    )


def format_event_item(event: Event, *, primary_key: PrimaryKey, gsi_2_key: PrimaryKey) -> EventItem:
    item: EventItem = {
        "pk": primary_key.partition_key,
        "sk": primary_key.sort_key,
        "sk_2": gsi_2_key.sort_key,
        "pk_2": gsi_2_key.partition_key,
        "client": event.client,
        "created_by": event.created_by,
        "created_date": get_as_utc_iso_format(event.created_date),
        "description": event.description,
        "event_date": get_as_utc_iso_format(event.event_date),
        "evidences": json.loads(json.dumps(event.evidences, default=serialize)),
        "group_name": event.group_name,
        "hacker": event.hacker,
        "id": event.id,
        "state": json.loads(json.dumps(event.state, default=serialize)),
        "type": event.type.value,
    }
    if event.root_id is not None:
        item["root_id"] = event.root_id
    if event.environment_url is not None:
        item["environment_url"] = event.environment_url
    return item


def format_metadata_item(metadata: EventMetadataToUpdate) -> Item:
    item = {
        "client": metadata.client,
        "description": metadata.description,
        "root_id": metadata.root_id,
        "type": metadata.type,
    }
    return {key: value for key, value in item.items() if value is not None}


def format_state(item: EventHistoricStateItem) -> EventState:
    return EventState(
        modified_by=item["modified_by"],
        modified_date=datetime.fromisoformat(item["modified_date"]),
        status=EventStateStatus[item["status"]],
        other=item.get("other"),
        reason=EventSolutionReason[item["reason"]] if item.get("reason") else None,
    )


def _get_solving_date(item: EventItem) -> datetime | None:
    if _date := item.get("solving_date"):
        return datetime.fromisoformat(_date)

    return None


def get_gsi_2_key(group_name: str, last_state: EventState) -> PrimaryKey:
    return keys.build_key(
        facet=GSI_2_FACET,
        values={
            "is_solved": str(last_state.status is EventStateStatus.SOLVED).lower(),
            "group_name": group_name,
        },
    )


def format_event_state_item(state: EventState, *, state_key: PrimaryKey) -> EventHistoricStateItem:
    item: EventHistoricStateItem = {
        "pk": state_key.partition_key,
        "sk": state_key.sort_key,
        "modified_by": state.modified_by,
        "modified_date": get_as_utc_iso_format(state.modified_date),
        "status": state.status.value,
    }
    if state.comment_id is not None:
        item["comment_id"] = state.comment_id
    if state.reason is not None:
        item["reason"] = state.reason.value
    if state.other is not None:
        item["other"] = state.other
    return item
