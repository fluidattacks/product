from datetime import (
    datetime,
)
from typing import (
    NamedTuple,
)

from integrates.db_model.enums import (
    TreatmentStatus,
)
from integrates.db_model.organization_finding_policies.enums import (
    PolicyStateStatus,
)


class OrgFindingPolicyState(NamedTuple):
    modified_by: str
    modified_date: datetime
    status: PolicyStateStatus


class OrgFindingPolicy(NamedTuple):
    id: str
    name: str
    organization_name: str
    state: OrgFindingPolicyState
    tags: set[str]
    treatment_acceptance: TreatmentStatus


class OrgFindingPolicyRequest(NamedTuple):
    organization_name: str
    policy_id: str
