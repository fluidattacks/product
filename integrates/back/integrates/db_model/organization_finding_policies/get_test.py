from integrates.dataloaders import get_new_context
from integrates.db_model.organization_finding_policies.types import OrgFindingPolicyRequest
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import OrganizationFaker, OrgFindingPolicyFaker, random_uuid
from integrates.testing.mocks import mocks

ORG_NAME = "orgtest"
POLICY_ID = random_uuid()


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(name=ORG_NAME)],
            organization_finding_policies=[
                OrgFindingPolicyFaker(organization_name=ORG_NAME, id=POLICY_ID),
                OrgFindingPolicyFaker(organization_name=ORG_NAME, id="1234"),
                OrgFindingPolicyFaker(organization_name=ORG_NAME, id="9876"),
            ],
        )
    )
)
async def test_load_organization_finding_policies() -> None:
    loaders = get_new_context()
    finding_policies = await loaders.organization_finding_policies.load(ORG_NAME)
    assert len(finding_policies) == 3


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(name=ORG_NAME)],
            organization_finding_policies=[
                OrgFindingPolicyFaker(organization_name=ORG_NAME, id=POLICY_ID)
            ],
        )
    )
)
async def test_load_single_organization_finding_policies() -> None:
    loaders = get_new_context()
    finding_policy = await loaders.organization_finding_policy.load(
        OrgFindingPolicyRequest(organization_name=ORG_NAME, policy_id=POLICY_ID)
    )
    assert finding_policy is not None
