import simplejson as json
from boto3.dynamodb.conditions import (
    Attr,
)

from integrates.audit import AuditEvent, add_audit_event
from integrates.custom_exceptions import (
    OrgFindingPolicyNotFound,
)
from integrates.db_model import (
    TABLE,
)
from integrates.db_model.organization_finding_policies.types import (
    OrgFindingPolicyState,
)
from integrates.db_model.utils import (
    get_as_utc_iso_format,
    serialize,
)
from integrates.dynamodb import (
    keys,
    operations,
)
from integrates.dynamodb.exceptions import (
    ConditionalCheckFailedException,
)


async def update(
    *,
    organization_name: str,
    finding_policy_id: str,
    state: OrgFindingPolicyState,
) -> None:
    key_structure = TABLE.primary_key
    metadata_key = keys.build_key(
        facet=TABLE.facets["org_finding_policy_metadata"],
        values={
            "name": organization_name,
            "uuid": finding_policy_id,
        },
    )
    state_item = json.loads(json.dumps(state, default=serialize))
    try:
        item = {"state": state_item}
        await operations.update_item(
            condition_expression=Attr(key_structure.partition_key).exists(),
            item=item,
            key=metadata_key,
            table=TABLE,
        )
        add_audit_event(
            AuditEvent(
                action="UPDATE",
                author=state.modified_by,
                metadata=item,
                object="OrgFindingPolicy",
                object_id=finding_policy_id,
            )
        )
    except ConditionalCheckFailedException as ex:
        raise OrgFindingPolicyNotFound() from ex

    historic_state_key = keys.build_key(
        facet=TABLE.facets["org_finding_policy_historic_state"],
        values={
            "iso8601utc": get_as_utc_iso_format(state.modified_date),
            "uuid": finding_policy_id,
        },
    )
    historic_state_item = {
        key_structure.partition_key: historic_state_key.partition_key,
        key_structure.sort_key: historic_state_key.sort_key,
        **state_item,
    }
    await operations.put_item(
        facet=TABLE.facets["org_finding_policy_historic_state"],
        item=historic_state_item,
        table=TABLE,
    )
