from decimal import Decimal

from integrates.dataloaders import get_new_context
from integrates.db_model.groups.types import GroupUnreliableIndicators
from integrates.testing.aws import (
    GroupUnreliableIndicatorsToUpdate,
    IntegratesAws,
    IntegratesDynamodb,
)
from integrates.testing.fakers import GroupFaker
from integrates.testing.mocks import mocks
from integrates.testing.utils import freeze_time, parametrize
from integrates.unreliable_indicators.utils import get_group_indicators

GROUP_NAME = "unittesting"


@parametrize(
    args=["group_name"],
    cases=[["unittesting"]],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            groups=[GroupFaker(name=GROUP_NAME)],
            group_unreliable_indicators=[
                GroupUnreliableIndicatorsToUpdate(
                    group_name=GROUP_NAME,
                    indicators=GroupUnreliableIndicators(
                        closed_vulnerabilities=8,
                        last_closed_vulnerability_days=23,
                        last_closed_vulnerability_finding="457497316",
                        max_open_severity=Decimal("6.3"),
                        max_open_severity_finding="988493279",
                        mean_remediate=Decimal("245"),
                        mean_remediate_critical_severity=Decimal("0"),
                        mean_remediate_high_severity=Decimal("0"),
                        mean_remediate_low_severity=Decimal("232"),
                        mean_remediate_medium_severity=Decimal("287"),
                        open_findings=5,
                        open_vulnerabilities=31,
                    ),
                )
            ],
        )
    )
)
@freeze_time("2023-09-21")
async def test_get_group_indicators(group_name: str) -> None:
    loaders = get_new_context()
    group = await loaders.group.load(group_name)
    assert group is not None
    indicators = await get_group_indicators(
        loaders,
        group,
    )

    assert indicators.closed_vulnerabilities == 8
    assert indicators.last_closed_vulnerability_days == 23
    assert indicators.last_closed_vulnerability_finding == "457497316"
    assert indicators.max_open_severity == Decimal("6.3")
    assert indicators.max_open_severity_finding == "988493279"
    assert indicators.mean_remediate == Decimal("245")
    assert indicators.mean_remediate_critical_severity == Decimal("0")
    assert indicators.mean_remediate_high_severity == Decimal("0")
    assert indicators.mean_remediate_low_severity == Decimal("232")
    assert indicators.mean_remediate_medium_severity == Decimal("287")
    assert indicators.open_findings == 5
    assert indicators.open_vulnerabilities == 31
