from collections.abc import (
    Iterable,
)
from datetime import (
    datetime,
)

from aiodataloader import (
    DataLoader,
)

from integrates.context import (
    FI_MARKETPLACE_PRODUCT_CODE,
)
from integrates.db_model import (
    TABLE,
)
from integrates.db_model.items import AwsMarketplaceSubscriptionItem
from integrates.db_model.marketplace.enums import (
    AWSMarketplacePricingDimension,
    AWSMarketplaceSubscriptionStatus,
)
from integrates.db_model.marketplace.types import (
    AWSMarketplaceSubscription,
    AWSMarketplaceSubscriptionEntitlement,
    AWSMarketplaceSubscriptionState,
)
from integrates.dynamodb import (
    keys,
    operations,
)


def _format_marketplace_subscription(
    item: AwsMarketplaceSubscriptionItem,
) -> AWSMarketplaceSubscription:
    return AWSMarketplaceSubscription(
        aws_customer_id=item["sk"].split("#")[1],
        created_at=datetime.fromisoformat(item["created_at"]),
        state=AWSMarketplaceSubscriptionState(
            entitlements=[
                AWSMarketplaceSubscriptionEntitlement(
                    dimension=AWSMarketplacePricingDimension[str(entitlement["dimension"]).upper()],
                    expiration_date=datetime.fromisoformat(entitlement["expiration_date"]),
                    value=entitlement["value"],
                )
                for entitlement in item["state"]["entitlements"]
            ],
            has_free_trial=item["state"]["has_free_trial"],
            modified_date=datetime.fromisoformat(item["state"]["modified_date"]),
            private_offer_id=item["state"].get("private_offer_id"),
            status=AWSMarketplaceSubscriptionStatus[item["state"]["status"]],
        ),
        aws_account_id=item.get("aws_account_id"),
        user=item.get("user"),
    )


async def _get_marketplace_subscriptions(
    aws_customer_ids: Iterable[str],
) -> list[AWSMarketplaceSubscription]:
    primary_keys = tuple(
        keys.build_key(
            facet=TABLE.facets["aws_marketplace_subscription_metadata"],
            values={
                "aws_customer_id": aws_customer_id,
                "product_code": FI_MARKETPLACE_PRODUCT_CODE,
            },
        )
        for aws_customer_id in aws_customer_ids
    )
    items = await operations.DynamoClient[AwsMarketplaceSubscriptionItem].batch_get_item(
        keys=primary_keys,
        table=TABLE,
    )

    return [_format_marketplace_subscription(item) for item in items]


class AWSMarketplaceSubscriptionLoader(DataLoader[str, AWSMarketplaceSubscription | None]):
    async def batch_load_fn(
        self,
        aws_customer_ids: Iterable[str],
    ) -> list[AWSMarketplaceSubscription | None]:
        results = {
            subscription.aws_customer_id: subscription
            for subscription in await _get_marketplace_subscriptions(
                aws_customer_ids=aws_customer_ids,
            )
        }

        return [results.get(aws_customer_id) for aws_customer_id in aws_customer_ids]
