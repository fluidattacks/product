from typing import cast

import simplejson as json
from boto3.dynamodb.conditions import (
    Attr,
)

from integrates.audit import AuditEvent, add_audit_event
from integrates.context import (
    FI_MARKETPLACE_PRODUCT_CODE,
)
from integrates.db_model import (
    TABLE,
)
from integrates.db_model.items import AwsMarketplaceSubscriptionItem
from integrates.db_model.marketplace.types import (
    AWSMarketplaceSubscription,
)
from integrates.db_model.utils import serialize
from integrates.dynamodb import (
    keys,
    operations,
)


def _format_marketplace_subscription_item(
    subscription: AWSMarketplaceSubscription,
) -> AwsMarketplaceSubscriptionItem:
    primary_key = keys.build_key(
        facet=TABLE.facets["aws_marketplace_subscription_metadata"],
        values={
            "aws_customer_id": subscription.aws_customer_id,
            "product_code": FI_MARKETPLACE_PRODUCT_CODE,
        },
    )
    item = {
        "pk": primary_key.partition_key,
        "sk": primary_key.sort_key,
        **{
            k: v
            for k, v in json.loads(json.dumps(subscription, default=serialize)).items()
            if k not in ("aws_customer_id",)
        },
    }
    return cast(AwsMarketplaceSubscriptionItem, item)


async def add_marketplace_subscription(
    subscription: AWSMarketplaceSubscription,
) -> None:
    key_structure = TABLE.primary_key
    item = _format_marketplace_subscription_item(subscription)
    await operations.put_item(
        condition_expression=Attr(key_structure.sort_key).not_exists(),
        facet=TABLE.facets["aws_marketplace_subscription_metadata"],
        item=item,
        table=TABLE,
    )
    add_audit_event(
        AuditEvent(
            action="CREATE",
            author=subscription.aws_customer_id,
            metadata=item,
            object="AWSMarketplaceSubscription",
            object_id=subscription.aws_customer_id,
        )
    )
