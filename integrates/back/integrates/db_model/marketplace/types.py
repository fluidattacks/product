from datetime import (
    datetime,
)
from typing import (
    NamedTuple,
    NotRequired,
    TypedDict,
)

from integrates.db_model.marketplace.enums import (
    AWSMarketplacePricingDimension,
    AWSMarketplaceSubscriptionStatus,
)

AWSMarketplaceSubscriptionSNSUpdate = TypedDict(
    "AWSMarketplaceSubscriptionSNSUpdate",
    {
        "action": str,
        "customer-identifier": str,
        "product-code": str,
        "offer-identifier": NotRequired[str],
        "isFreeTrialTermPresent": NotRequired[str],
    },
)


class AWSMarketplaceSubscriptionEntitlement(NamedTuple):
    dimension: AWSMarketplacePricingDimension
    expiration_date: datetime
    value: int


class AWSMarketplaceSubscriptionState(NamedTuple):
    entitlements: list[AWSMarketplaceSubscriptionEntitlement]
    has_free_trial: bool
    modified_date: datetime
    status: AWSMarketplaceSubscriptionStatus
    private_offer_id: str | None = None


class AWSMarketplaceSubscription(NamedTuple):
    aws_customer_id: str
    created_at: datetime
    state: AWSMarketplaceSubscriptionState
    aws_account_id: str | None = None
    user: str | None = None


class AWSMarketplaceSubscriptionUpdate(NamedTuple):
    aws_account_id: str | None = None
    user: str | None = None
