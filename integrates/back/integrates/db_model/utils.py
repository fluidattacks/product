from collections.abc import (
    Awaitable,
    Callable,
    Iterable,
)
from datetime import (
    UTC,
    datetime,
    timedelta,
)
from decimal import (
    Decimal,
)
from enum import (
    Enum,
)
from typing import (
    Any,
    TypeVar,
)

from aioextensions import (
    collect,
)
from more_itertools import (
    chunked,
)
from snowflake.connector.cursor import (
    SnowflakeCursor,
)

from integrates.archive.client import (
    snowflake_db_cursor,
)
from integrates.class_types.types import (
    GenericItem,
    Item,
)
from integrates.context import (
    FI_ENVIRONMENT,
)
from integrates.db_model.enums import (
    AcceptanceStatus,
    TreatmentStatus,
)
from integrates.db_model.types import (
    Connection,
    Edge,
    PoliciesToUpdate,
    Treatment,
    TreatmentItem,
)
from integrates.dynamodb.types import (
    Facet,
    Index,
    ItemQueryResponse,
    PrimaryKey,
    Table,
)
from integrates.dynamodb.utils import (
    get_cursor,
)

T = TypeVar("T")
MAX_CHUNK_SIZE_TO_ARCHIVE = 1000


def adjust_historic_dates(historic: tuple[T, ...]) -> tuple[T, ...]:
    """
    Ensure dates are not the same and in ascending order.
    Also add a minimum 1 second offset among them.
    """
    if not historic:
        return tuple()
    new_historic = [historic[0]]
    base_date: datetime = historic[0].modified_date  # type: ignore
    for entry in historic[1:]:
        base_date = get_datetime_with_offset(
            base_date,
            entry.modified_date,  # type: ignore
        )
        new_historic.append(
            entry._replace(modified_date=base_date),  # type: ignore
        )

    return tuple(new_historic)


def get_as_utc_iso_format(date: datetime) -> str:
    return date.astimezone(tz=UTC).isoformat()


def get_min_iso_date(date: datetime) -> datetime:
    return datetime.combine(
        date.astimezone(tz=UTC),
        datetime.min.time(),
    )


def get_first_day_iso_date() -> datetime:
    now = get_min_iso_date(datetime.now(tz=UTC))

    return now - timedelta(days=(now.isoweekday() - 1) % 7)


def get_datetime_with_offset(
    base_iso8601: datetime,
    target_iso8601: datetime,
    offset: int = 1,
) -> datetime:
    """Guarantee at least n seconds separation between dates."""
    return max(base_iso8601 + timedelta(seconds=offset), target_iso8601)


def format_policies_to_update(
    policies_data: dict,
) -> PoliciesToUpdate:
    return PoliciesToUpdate(
        days_until_it_breaks=int(policies_data["days_until_it_breaks"])
        if policies_data.get("days_until_it_breaks") is not None
        else None,
        inactivity_period=int(policies_data["inactivity_period"])
        if policies_data.get("inactivity_period") is not None
        else None,
        max_acceptance_days=int(policies_data["max_acceptance_days"])
        if policies_data.get("max_acceptance_days") is not None
        else None,
        max_acceptance_severity=Decimal(policies_data["max_acceptance_severity"]).quantize(
            Decimal("0.1"),
        )
        if policies_data.get("max_acceptance_severity") is not None
        else None,
        max_number_acceptances=int(policies_data["max_number_acceptances"])
        if policies_data.get("max_number_acceptances") is not None
        else None,
        min_acceptance_severity=Decimal(policies_data["min_acceptance_severity"]).quantize(
            Decimal("0.1"),
        )
        if policies_data.get("min_acceptance_severity") is not None
        else None,
        min_breaking_severity=Decimal(policies_data["min_breaking_severity"]).quantize(
            Decimal("0.1"),
        )
        if policies_data.get("min_breaking_severity") is not None
        else None,
        vulnerability_grace_period=int(policies_data["vulnerability_grace_period"])
        if policies_data.get("vulnerability_grace_period") is not None
        else None,
    )


def serialize(object_: object) -> Any:
    if isinstance(object_, set):
        return list(serialize(o) for o in object_)
    if isinstance(object_, datetime):
        return object_.astimezone(tz=UTC).isoformat()
    if isinstance(object_, float):
        return Decimal(str(object_))
    if isinstance(object_, Enum):
        return object_.value

    return object_


def format_edge(
    index: Index | None,
    item: GenericItem,
    formatter: Callable[[GenericItem], T],
    table: Table,
) -> Edge[T]:
    return Edge[T](node=formatter(item), cursor=get_cursor(index, item, table))


def format_connection(
    *,
    index: Index | None,
    formatter: Callable[[GenericItem], T],
    response: ItemQueryResponse,
    table: Table,
) -> Connection[T]:
    return Connection[T](
        edges=tuple(format_edge(index, item, formatter, table) for item in response.items),
        page_info=response.page_info,
    )


def format_edges(
    *,
    index: Index | None,
    formatter: Callable[[GenericItem], T],
    items: tuple[GenericItem, ...],
    table: Table,
) -> tuple[Edge[T], ...]:
    return tuple(format_edge(index, item, formatter, table) for item in items)


def get_historic_gsi_sk(state: str, iso8601utc: str) -> str:
    return f"STATE#{state}#DATE#{iso8601utc}"


def get_historic_gsi_2_key(
    facet: Facet,
    group_name: str,
    state: str,
    iso8601utc: str,
) -> PrimaryKey:
    key_parts = [part for part in facet.pk_alias.split("#") if part.isupper()]
    key_parts.append(group_name)
    return PrimaryKey(
        partition_key="#".join(key_parts),
        sort_key=get_historic_gsi_sk(state, iso8601utc),
    )


def get_historic_gsi_3_key(group_name: str, state: str, iso8601utc: str) -> PrimaryKey:
    return PrimaryKey(
        partition_key=f"GROUP#{group_name}",
        sort_key=get_historic_gsi_sk(state, iso8601utc),
    )


def format_key_from_pk_search(group_result: Any) -> str:
    return group_result[1:] if isinstance(group_result, str) else ""


async def archive(
    items: tuple[Item, ...],
    _archive: Callable[[SnowflakeCursor, Iterable[Item]], Awaitable[None]],
) -> None:
    if not items or FI_ENVIRONMENT != "production":
        return

    with snowflake_db_cursor() as cursor:
        await collect(
            [_archive(cursor, chunk) for chunk in chunked(items, MAX_CHUNK_SIZE_TO_ARCHIVE)],
            workers=8,
        )


def format_treatment(item: TreatmentItem) -> Treatment:
    return Treatment(
        accepted_until=datetime.fromisoformat(item["accepted_until"])
        if item.get("accepted_until")
        else None,
        acceptance_status=AcceptanceStatus[item["acceptance_status"]]
        if item.get("acceptance_status")
        else None,
        justification=item.get("justification"),
        assigned=item.get("assigned"),
        modified_by=item.get("modified_by"),
        modified_date=datetime.fromisoformat(item["modified_date"]),
        status=TreatmentStatus[item["status"]],
    )


def get_assigned(*, treatment: Treatment | None) -> str:
    if treatment is None or treatment.assigned is None:
        return ""

    return treatment.assigned
