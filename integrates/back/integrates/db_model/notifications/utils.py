from typing import cast

import simplejson

from integrates.class_types.types import (
    Item,
)
from integrates.db_model.items import NotificationItem
from integrates.db_model.notifications.types import (
    Notification,
)
from integrates.db_model.utils import (
    serialize,
)


def format_full_item(notification: Notification) -> NotificationItem:
    return cast(
        NotificationItem,
        {
            **format_base_item(notification),
            "pk": f"USER#{notification.user_email}",
            "sk": f"NOTIFICATION#{notification.id}",
        },
    )


def format_base_item(notification: Notification) -> Item:
    item = simplejson.loads(simplejson.dumps(notification, default=serialize))

    if notification.expiration_time:
        return {
            **item,
            "expiration_time": int(notification.expiration_time.timestamp()),
        }
    return item
