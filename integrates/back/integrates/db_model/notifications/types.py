from datetime import (
    datetime,
)
from enum import StrEnum
from typing import (
    Literal,
    NamedTuple,
    Union,
)

NotificationType = Literal["REPORT"]


class ReportStatus(StrEnum):
    PREPARING = "PREPARING"
    PROCESSING = "PROCESSING"
    READY = "READY"
    DELETED = "DELETED"


class ReportNotification(NamedTuple):
    format: str
    id: str
    name: str
    notified_at: datetime
    status: ReportStatus
    type: NotificationType
    user_email: str
    expiration_time: datetime | None = None
    s3_file_path: str | None = None
    size: int | None = None


Notification = Union[ReportNotification]
