from integrates.db_model import (
    TABLE,
)
from integrates.db_model.organizations.utils import (
    remove_org_id_prefix,
)
from integrates.dynamodb import (
    keys,
)
from integrates.dynamodb.types import (
    Facet,
    PrimaryKey,
)

GSI_2_FACETS = {
    "mailmap_entry": Facet(
        attrs=TABLE.facets["mailmap_entry"].attrs,
        pk_alias="ORG#organization_id#MAILMAP_ENTRY#all",
        sk_alias="ORG#organization_id#MAILMAP_ENTRY#entry_email",
    ),
    "mailmap_subentry": Facet(
        attrs=TABLE.facets["mailmap_subentry"].attrs,
        pk_alias="ORG#organization_id#MAILMAP_SUBENTRY#all",
        sk_alias="MAILMAP_SUBENTRY#subentry_email",
    ),
}


def generate_gsi_2_key(facet_name: str, organization_id: str) -> PrimaryKey:
    key = keys.build_key(
        facet=GSI_2_FACETS[facet_name],
        values={
            "organization_id": remove_org_id_prefix(organization_id),
            "all": "all",
        },
    )
    return key
