from integrates.db_model.mailmap.utils.delete import (
    delete_entry_item,
    delete_entry_subentries_items,
    delete_subentry_item,
)
from integrates.db_model.mailmap.utils.get import (
    check_if_organization_not_found,
    ensure_organization_id,
)
from integrates.db_model.mailmap.utils.post_processing import (
    post_process_delete_entry,
    post_process_delete_entry_subentries,
    post_process_delete_subentry,
)


async def delete_mailmap_entry(
    entry_email: str,
    organization_id: str,
) -> None:
    _organization_id = await ensure_organization_id(organization_id)
    await check_if_organization_not_found(_organization_id)
    # Delete item
    await delete_entry_item(
        entry_email=entry_email,
        organization_id=_organization_id,
    )
    # Post-processing: set first subentry as entry and update subentries pk
    await post_process_delete_entry(
        entry_email=entry_email,
        organization_id=_organization_id,
    )


async def delete_mailmap_subentry(
    entry_email: str,
    subentry_email: str,
    subentry_name: str,
    organization_id: str,
) -> None:
    _organization_id = await ensure_organization_id(organization_id)
    await check_if_organization_not_found(_organization_id)
    # Delete item
    await delete_subentry_item(
        entry_email=entry_email,
        subentry_email=subentry_email,
        subentry_name=subentry_name,
        organization_id=_organization_id,
    )
    # Post-processing: set entry as subentry if no remaining subentries
    await post_process_delete_subentry(
        entry_email=entry_email,
        organization_id=_organization_id,
    )


async def delete_mailmap_entry_subentries(
    entry_email: str,
    organization_id: str,
) -> None:
    _organization_id = await ensure_organization_id(organization_id)
    await check_if_organization_not_found(_organization_id)
    # Delete items
    await delete_entry_subentries_items(
        entry_email=entry_email,
        organization_id=_organization_id,
    )
    # Post-processing: set entry as subentry
    await post_process_delete_entry_subentries(
        entry_email=entry_email,
        organization_id=_organization_id,
    )


async def delete_mailmap_entry_with_subentries(
    entry_email: str,
    organization_id: str,
) -> None:
    _organization_id = await ensure_organization_id(organization_id)
    await check_if_organization_not_found(_organization_id)
    # Delete items: No post-processing needed
    await delete_entry_item(
        entry_email=entry_email,
        organization_id=_organization_id,
    )
    # Don't check if entry exists because it's already removed
    await delete_entry_subentries_items(
        entry_email=entry_email,
        organization_id=_organization_id,
        check_if_entry_exists=False,
    )
