from typing import (
    NamedTuple,
    Optional,
    TypedDict,
)

from integrates.dynamodb.types import (
    PageInfo,
)


class MailmapSubentry(dict):
    def __init__(
        self,
        mailmap_subentry_name: str,
        mailmap_subentry_email: str,
        mailmap_subentry_created_at: Optional[str] = None,
        mailmap_subentry_updated_at: Optional[str] = None,
    ):
        self["mailmap_subentry_name"] = mailmap_subentry_name
        self["mailmap_subentry_email"] = mailmap_subentry_email
        self["mailmap_subentry_created_at"] = mailmap_subentry_created_at
        self["mailmap_subentry_updated_at"] = mailmap_subentry_updated_at

    def __eq__(self, other: object) -> bool:
        if isinstance(other, MailmapSubentry):
            return (
                self["mailmap_subentry_name"] == other["mailmap_subentry_name"]
                and self["mailmap_subentry_email"] == other["mailmap_subentry_email"]
            )
        return False  # pragma: no cover


class MailmapEntry(dict):
    def __init__(
        self,
        mailmap_entry_name: str,
        mailmap_entry_email: str,
        mailmap_entry_created_at: Optional[str] = None,
        mailmap_entry_updated_at: Optional[str] = None,
        organization_id: str = "",
    ):
        self["mailmap_entry_name"] = mailmap_entry_name
        self["mailmap_entry_email"] = mailmap_entry_email
        self["mailmap_entry_created_at"] = mailmap_entry_created_at
        self["mailmap_entry_updated_at"] = mailmap_entry_updated_at
        self["organization_id"] = organization_id

    def __eq__(self, other: object) -> bool:
        if isinstance(other, MailmapEntry):
            return (
                self["mailmap_entry_name"] == other["mailmap_entry_name"]
                and self["mailmap_entry_email"] == other["mailmap_entry_email"]
            )
        return False  # pragma: no cover


class MailmapEntryWithSubentries(TypedDict):
    entry: MailmapEntry
    subentries: list[MailmapSubentry]


class MailmapEntryEdge(NamedTuple):
    node: MailmapEntry
    cursor: str


class MailmapEntriesConnection(NamedTuple):
    edges: tuple[MailmapEntryEdge, ...]
    page_info: PageInfo


class MailmapRecord(NamedTuple):
    mailmap_entry_name: str
    mailmap_entry_email: str
    mailmap_subentry_name: str
    mailmap_subentry_email: str
    mailmap_entry_created_at: Optional[str] = None
    mailmap_entry_updated_at: Optional[str] = None
    mailmap_subentry_created_at: Optional[str] = None
    mailmap_subentry_updated_at: Optional[str] = None
