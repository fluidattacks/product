from integrates.db_model.mailmap.create import create_mailmap_entry
from integrates.db_model.mailmap.types import MailmapEntry
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import OrganizationFaker
from integrates.testing.mocks import mocks
from integrates.testing.utils import parametrize


@parametrize(
    args=["author_name", "author_email", "organization_id"],
    cases=[
        [
            "Jane Doe",
            "jane@entry.com",
            "40f6da5f-4f66-4bf0-825b-a2d9748ad6db",
        ],
        [
            "John Doe",
            "john@entry.com",
            "40f6da5f-4f66-4bf0-825b-a2d9748ad6db",
        ],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="40f6da5f-4f66-4bf0-825b-a2d9748ad6db")],
        ),
    )
)
async def test_create_mailmap_entry(
    author_name: str,
    author_email: str,
    organization_id: str,
) -> None:
    # Act
    result = await create_mailmap_entry(
        entry=MailmapEntry(
            mailmap_entry_name=author_name,
            mailmap_entry_email=author_email,
        ),
        organization_id=organization_id,
    )

    # Assert
    assert result["entry"]["mailmap_entry_email"] == author_email
    assert result["entry"]["mailmap_entry_name"] == author_name
    assert len(result["subentries"]) == 1
    assert result["subentries"][0]["mailmap_subentry_email"] == author_email
    assert result["subentries"][0]["mailmap_subentry_name"] == author_name
