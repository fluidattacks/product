from integrates.db_model.mailmap.types import (
    MailmapEntry,
    MailmapEntryWithSubentries,
    MailmapSubentry,
)
from integrates.db_model.mailmap.utils.conversions import (
    assemble_entry_with_subentries,
    entry_to_subentry,
)
from integrates.db_model.mailmap.utils.create import (
    create_entry_item,
    create_entry_item_with_subentries_items,
    create_subentry_item,
)
from integrates.db_model.mailmap.utils.get import (
    check_if_organization_not_found,
    ensure_organization_id,
)
from integrates.db_model.mailmap.utils.timestamps import (
    set_entry_created_at_timestamp,
    set_entry_with_subentries_created_at_timestamp,
    set_subentry_created_at_timestamp,
)


async def create_mailmap_entry(
    entry: MailmapEntry,
    organization_id: str,
) -> MailmapEntryWithSubentries:
    _organization_id = await ensure_organization_id(organization_id)
    await check_if_organization_not_found(_organization_id)

    # Set `created_at` timestamp
    _entry = set_entry_created_at_timestamp(entry)

    # Create entry
    await create_entry_item(entry=_entry, organization_id=_organization_id)
    # Create subentry equal to the received entry
    subentry = entry_to_subentry(entry=_entry)
    await create_subentry_item(
        subentry=subentry,
        entry_email=_entry["mailmap_entry_email"],
        organization_id=_organization_id,
    )
    # Return complete object: entry with its subentry
    entry_with_subentries = assemble_entry_with_subentries(entry=_entry, subentries=[subentry])
    return entry_with_subentries


async def create_mailmap_subentry(
    subentry: MailmapSubentry,
    entry_email: str,
    organization_id: str,
) -> bool:
    _organization_id = await ensure_organization_id(organization_id)
    await check_if_organization_not_found(_organization_id)

    # Set `created_at` timestamp
    _subentry = set_subentry_created_at_timestamp(subentry)

    # Create subentry
    return await create_subentry_item(
        subentry=_subentry,
        entry_email=entry_email,
        organization_id=_organization_id,
    )


async def create_mailmap_entry_with_subentries(
    entry_with_subentries: MailmapEntryWithSubentries,
    organization_id: str,
    raise_exception: bool = True,
) -> tuple[bool, list[bool]]:
    _organization_id = await ensure_organization_id(organization_id)
    await check_if_organization_not_found(_organization_id)

    # Set `created_at` timestamp
    _entry_with_subentries = set_entry_with_subentries_created_at_timestamp(entry_with_subentries)

    # Create entry with subentries
    (
        entry_result,
        subentries_results,
    ) = await create_entry_item_with_subentries_items(
        entry_with_subentries=_entry_with_subentries,
        organization_id=_organization_id,
        raise_exception=raise_exception,
    )

    return entry_result, subentries_results
