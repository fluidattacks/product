from integrates.db_model.mailmap.types import (
    MailmapEntry,
    MailmapEntryWithSubentries,
    MailmapSubentry,
)
from integrates.db_model.mailmap.utils.conversions import (
    entry_item_to_subentry_item,
    item_to_entry,
    item_to_subentry,
    subentry_item_to_entry_item,
    subentry_to_entry,
)
from integrates.db_model.mailmap.utils.create import (
    create_entry_item_with_subentries_items,
)
from integrates.db_model.mailmap.utils.delete import (
    delete_entry_item,
    delete_subentry_item,
)
from integrates.db_model.mailmap.utils.get import (
    check_if_entry_not_found,
    check_if_organization_not_found,
    ensure_organization_id,
    get_entry_item,
    get_entry_subentries_items,
    get_items,
    get_subentry_item,
)
from integrates.db_model.mailmap.utils.post_processing import (
    post_process_delete_subentry,
)
from integrates.db_model.mailmap.utils.put import (
    put_item,
)
from integrates.db_model.mailmap.utils.timestamps import (
    set_entry_updated_at_timestamp,
    set_subentry_updated_at_timestamp,
)
from integrates.db_model.mailmap.utils.update import (
    move_entry_item,
    move_subentry_item,
    set_subentry_primary_key,
    update_entry_item,
    update_subentries_primary_key,
    update_subentry_item,
)
from integrates.db_model.mailmap.utils.utils import (
    async_gather,
    map_list,
)


async def update_mailmap_subentry(
    subentry: MailmapSubentry,
    entry_email: str,
    subentry_email: str,
    subentry_name: str,
    organization_id: str,
) -> None:
    _organization_id = await ensure_organization_id(organization_id)
    await check_if_organization_not_found(_organization_id)

    # Set `updated_at` timestamp
    _subentry = await set_subentry_updated_at_timestamp(
        subentry=subentry,
        entry_email=entry_email,
        subentry_email=subentry_email,
        subentry_name=subentry_name,
        organization_id=_organization_id,
    )

    await update_subentry_item(
        subentry=_subentry,
        entry_email=entry_email,
        subentry_email=subentry_email,
        subentry_name=subentry_name,
        organization_id=_organization_id,
    )


async def update_mailmap_entry(
    entry_email: str,
    entry: MailmapEntry,
    organization_id: str,
) -> None:
    _organization_id = await ensure_organization_id(organization_id)
    await check_if_organization_not_found(_organization_id)

    # Set `updated_at` timestamp
    _entry = await set_entry_updated_at_timestamp(
        entry=entry,
        entry_email=entry_email,
        organization_id=_organization_id,
    )

    await update_entry_item(
        entry_email=entry_email,
        entry=_entry,
        organization_id=_organization_id,
    )


async def set_mailmap_subentry_as_mailmap_entry(
    entry_email: str,
    subentry_email: str,
    subentry_name: str,
    organization_id: str,
) -> MailmapEntry:
    _organization_id = await ensure_organization_id(organization_id)
    await check_if_organization_not_found(_organization_id)
    # Get items
    entry_item = await get_entry_item(
        entry_email=entry_email,
        organization_id=_organization_id,
    )
    # Already checked if entry exists
    subentry_item = await get_subentry_item(
        entry_email=entry_email,
        subentry_email=subentry_email,
        subentry_name=subentry_name,
        organization_id=_organization_id,
        check_if_entry_exists=False,
    )
    # Swap items
    new_subentry_item = entry_item_to_subentry_item(
        entry_item=entry_item,
        organization_id=_organization_id,
    )
    new_entry_item = subentry_item_to_entry_item(
        subentry_item=subentry_item,
        organization_id=_organization_id,
    )
    # Delete old items
    await delete_entry_item(
        entry_email=entry_email,
        organization_id=_organization_id,
    )
    # Don't check if entry exists because it's already removed
    await delete_subentry_item(
        entry_email=entry_email,
        subentry_email=subentry_email,
        subentry_name=subentry_name,
        organization_id=_organization_id,
        check_if_entry_exists=False,
    )
    # Put items
    await put_item(facet_name="mailmap_entry", item=new_entry_item)
    await put_item(facet_name="mailmap_subentry", item=new_subentry_item)
    # Update subentries primary key
    await update_subentries_primary_key(
        old_email=entry_email,
        new_email=subentry_email,
        organization_id=_organization_id,
    )

    entry = item_to_entry(item=new_entry_item, organization_id=_organization_id)

    return entry


async def set_mailmap_entry_as_mailmap_subentry(
    entry_email: str,
    target_entry_email: str,
    organization_id: str,
) -> None:
    _organization_id = await ensure_organization_id(organization_id)
    await check_if_organization_not_found(_organization_id)
    # Check if target entry doesn't exist
    await check_if_entry_not_found(
        entry_email=target_entry_email,
        organization_id=_organization_id,
    )
    # Get items
    # Get entry item
    entry_item = await get_entry_item(
        entry_email=entry_email,
        organization_id=_organization_id,
    )
    # Already checked if entry exists
    # Get entry subentry items
    subentry_items = await get_entry_subentries_items(
        entry_email=entry_email,
        organization_id=_organization_id,
    )
    # Generate new items
    # Convert entry to subentry
    new_subentry_item = entry_item_to_subentry_item(
        entry_item=entry_item,
        organization_id=_organization_id,
    )
    # Set subentry primary key
    updated_subentry_item = set_subentry_primary_key(
        subentry_item=new_subentry_item,
        entry_email=target_entry_email,
        organization_id=_organization_id,
    )
    # Set entry subentries primary key
    updated_subentry_items = map_list(
        func=lambda subentry_item: set_subentry_primary_key(
            subentry_item=subentry_item,
            entry_email=target_entry_email,
            organization_id=_organization_id,
        ),
        iterable=subentry_items,
    )
    # Delete old items
    # Delete entry
    await delete_entry_item(
        entry_email=entry_email,
        organization_id=_organization_id,
    )
    # Don't check if entry exists because it's already removed
    # Delete entry subentries
    await async_gather(
        func=lambda subentry_item: delete_subentry_item(
            entry_email=entry_email,
            subentry_email=subentry_item["mailmap_subentry_email"],
            subentry_name=subentry_item["mailmap_subentry_name"],
            organization_id=_organization_id,
            check_if_entry_exists=False,
        ),
        iterable=subentry_items,
    )
    # Put items
    # Put new subentry
    await put_item(facet_name="mailmap_subentry", item=updated_subentry_item)
    # Put new subentries
    await async_gather(
        func=lambda subentry_item: put_item(
            facet_name="mailmap_subentry",
            item=subentry_item,
        ),
        iterable=updated_subentry_items,
    )


async def move_mailmap_entry_with_subentries(
    entry_email: str,
    organization_id: str,
    new_organization_id: str,
) -> None:
    _organization_id = await ensure_organization_id(organization_id)
    await check_if_organization_not_found(_organization_id)
    _new_organization_id = await ensure_organization_id(new_organization_id)
    await check_if_organization_not_found(_new_organization_id)

    entry_item = await get_entry_item(
        entry_email=entry_email,
        organization_id=_organization_id,
    )
    subentries_items = await get_items(
        facet_name="mailmap_subentry",
        email=entry_email,
        organization_id=_organization_id,
    )
    await move_entry_item(
        entry_email=entry_email,
        entry=item_to_entry(entry_item, organization_id=_new_organization_id),
        organization_id=_organization_id,
        new_organization_id=_new_organization_id,
    )
    await async_gather(
        func=lambda subentry_item: move_subentry_item(
            subentry=item_to_subentry(subentry_item),
            search_args={
                "entry_email": entry_email,
                "subentry_email": subentry_item["mailmap_subentry_email"],
                "subentry_name": subentry_item["mailmap_subentry_name"],
                "organization_id": _organization_id,
            },
            new_organization_id=_new_organization_id,
        ),
        iterable=subentries_items,
    )


async def convert_mailmap_subentries_to_new_entry(
    entry_email: str,
    main_subentry: MailmapSubentry,
    other_subentries: list[MailmapSubentry],
    organization_id: str,
) -> None:
    _organization_id = await ensure_organization_id(organization_id)
    await check_if_organization_not_found(_organization_id)

    # Delete subentries from previous entry

    # Delete main subentry
    await delete_subentry_item(
        entry_email=entry_email,
        subentry_email=main_subentry["mailmap_subentry_email"],
        subentry_name=main_subentry["mailmap_subentry_name"],
        organization_id=_organization_id,
    )

    # Delete other subentries
    await async_gather(
        func=lambda subentry: delete_subentry_item(
            entry_email=entry_email,
            subentry_email=subentry["mailmap_subentry_email"],
            subentry_name=subentry["mailmap_subentry_name"],
            organization_id=_organization_id,
        ),
        iterable=other_subentries,
    )

    # Post-processing: set entry as subentry if no remaining subentries
    await post_process_delete_subentry(
        entry_email=entry_email,
        organization_id=_organization_id,
    )

    # Construct objects
    entry = subentry_to_entry(subentry=main_subentry)
    entry_with_subentries = MailmapEntryWithSubentries(
        entry=entry,
        subentries=other_subentries,
    )

    # Create new entry with subentries
    await create_entry_item_with_subentries_items(
        entry_with_subentries=entry_with_subentries,
        organization_id=_organization_id,
    )
