from integrates.db_model.items import MailmapEntryItem, MailmapSubentryItem
from integrates.db_model.mailmap.constants import (
    generate_gsi_2_key,
)
from integrates.db_model.mailmap.types import (
    MailmapEntry,
    MailmapSubentry,
)
from integrates.db_model.mailmap.utils.conversions import (
    entry_item_to_subentry_item,
    subentry_item_to_entry_item,
)
from integrates.db_model.mailmap.utils.create import (
    create_entry_item,
    create_subentry_item,
)
from integrates.db_model.mailmap.utils.delete import (
    delete_entry_item,
    delete_subentry_item,
)
from integrates.db_model.mailmap.utils.get import (
    get_items,
)
from integrates.db_model.mailmap.utils.primary_key import (
    build_subentry_primary_key,
)
from integrates.db_model.mailmap.utils.put import (
    put_item,
)
from integrates.db_model.mailmap.utils.utils import (
    async_gather,
    map_list,
)


def set_subentry_primary_key(
    subentry_item: MailmapSubentryItem,
    entry_email: str,
    organization_id: str,
) -> MailmapSubentryItem:
    primary_key = build_subentry_primary_key(
        entry_email=entry_email,
        subentry_email=subentry_item["mailmap_subentry_email"],
        subentry_name=subentry_item["mailmap_subentry_name"],
        organization_id=organization_id,
    )
    gis_2_key = generate_gsi_2_key(
        facet_name="mailmap_subentry",
        organization_id=organization_id,
    )
    item: MailmapSubentryItem = {
        "pk": primary_key.partition_key,
        "sk": primary_key.sort_key,
        "mailmap_subentry_name": subentry_item["mailmap_subentry_name"],
        "mailmap_subentry_email": subentry_item["mailmap_subentry_email"],
        "pk_2": gis_2_key.partition_key,
        "sk_2": gis_2_key.sort_key,
    }
    return item


async def update_subentries_primary_key(
    old_email: str,
    new_email: str,
    organization_id: str,
) -> None:
    # Get items
    subentry_items = await get_items(
        facet_name="mailmap_subentry",
        email=old_email,
        organization_id=organization_id,
    )
    # Delete old items
    await async_gather(
        # Don't check if entry exists because it's already removed
        func=lambda subentry_item: delete_subentry_item(
            entry_email=old_email,
            subentry_email=subentry_item["mailmap_subentry_email"],
            subentry_name=subentry_item["mailmap_subentry_name"],
            organization_id=organization_id,
            check_if_entry_exists=False,
        ),
        iterable=subentry_items,
    )
    # Generate updated
    updated_subentry_items = map_list(
        func=lambda subentry_item: set_subentry_primary_key(
            subentry_item=subentry_item,
            entry_email=new_email,
            organization_id=organization_id,
        ),
        iterable=subentry_items,
    )
    # Put updated items
    await async_gather(
        func=lambda subentry_item: put_item(facet_name="mailmap_subentry", item=subentry_item),
        iterable=updated_subentry_items,
    )


async def update_subentry_item(
    subentry: MailmapSubentry,
    entry_email: str,
    subentry_email: str,
    subentry_name: str,
    organization_id: str,
) -> None:
    # Delete subentry without post-processing
    await delete_subentry_item(
        entry_email=entry_email,
        subentry_email=subentry_email,
        subentry_name=subentry_name,
        organization_id=organization_id,
    )
    # Create updated subentry
    await create_subentry_item(
        subentry=subentry,
        entry_email=entry_email,
        organization_id=organization_id,
    )


async def move_subentry_item(
    subentry: MailmapSubentry,
    search_args: dict,
    new_organization_id: str,
) -> None:
    """
    Move a subentry item from one organization to another.

    Args:
        subentry (MailmapSubentry): Subentry obj to be created in the new org.
        search_args (dict): Arguments to search for the existing subentry item:
            entry_email (str): The email address of the main entry.
            subentry_email (str): The email address of the subentry.
            subentry_name (str): The name associated with the subentry.
            organization_id (str): The current organization ID of the subentry.
        new_organization_id (str): The ID of the org to move the subentry to.

    """
    # Delete subentry without post-processing
    await delete_subentry_item(
        entry_email=search_args["entry_email"],
        subentry_email=search_args["subentry_email"],
        subentry_name=search_args["subentry_name"],
        organization_id=search_args["organization_id"],
        check_if_entry_exists=False,
    )
    # Create updated subentry
    await create_subentry_item(
        subentry=subentry,
        entry_email=search_args["entry_email"],
        organization_id=new_organization_id,
    )


async def update_entry_item(
    entry_email: str,
    entry: MailmapEntry,
    organization_id: str,
) -> None:
    # Delete entry without post-processing
    await delete_entry_item(
        entry_email=entry_email,
        organization_id=organization_id,
    )
    # Create updated entry
    await create_entry_item(
        entry=entry,
        organization_id=organization_id,
    )
    # Update subentries primary key
    await update_subentries_primary_key(
        old_email=entry_email,
        new_email=entry["mailmap_entry_email"],
        organization_id=organization_id,
    )


async def move_entry_item(
    entry_email: str,
    entry: MailmapEntry,
    organization_id: str,
    new_organization_id: str,
) -> None:
    # Delete entry without post-processing
    await delete_entry_item(
        entry_email=entry_email,
        organization_id=organization_id,
    )
    # Create updated entry
    await create_entry_item(
        entry=entry,
        organization_id=new_organization_id,
    )


async def set_subentry_as_entry(
    subentry_item: MailmapSubentryItem,
    organization_id: str,
) -> None:
    entry_item = subentry_item_to_entry_item(
        subentry_item=subentry_item,
        organization_id=organization_id,
    )
    await put_item(facet_name="mailmap_entry", item=entry_item)


async def set_entry_as_subentry(
    entry_item: MailmapEntryItem,
    organization_id: str,
) -> None:
    subentry_item = entry_item_to_subentry_item(
        entry_item=entry_item,
        organization_id=organization_id,
    )
    await put_item(facet_name="mailmap_subentry", item=subentry_item)
