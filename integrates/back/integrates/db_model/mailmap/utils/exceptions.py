from integrates.custom_exceptions import (
    MailmapEntryAlreadyExists,
    MailmapEntryNotFound,
    MailmapOrganizationNotFound,
    MailmapSubentryAlreadyExists,
    MailmapSubentryAlreadyExistsInOrganization,
    MailmapSubentryNotFound,
    NoMailmapSubentriesFound,
)


def raise_organization_not_found_exception(
    organization_id: str,
) -> None:
    raise MailmapOrganizationNotFound(
        organization_id=organization_id,
    )


def raise_not_found_exception(
    facet_name: str,
    entry_email: str,
    organization_id: str,
    subentry_email: str = "",
    subentry_name: str = "",
) -> None:
    if facet_name == "mailmap_entry":
        raise MailmapEntryNotFound(
            entry_email=entry_email,
            organization_id=organization_id,
        )
    raise MailmapSubentryNotFound(
        entry_email=entry_email,
        subentry_email=subentry_email,
        subentry_name=subentry_name,
        organization_id=organization_id,
    )


def raise_already_exists_exception(
    facet_name: str,
    entry_email: str,
    organization_id: str,
    subentry_email: str = "",
    subentry_name: str = "",
) -> None:
    if facet_name == "mailmap_entry":
        raise MailmapEntryAlreadyExists(
            entry_email=entry_email,
            organization_id=organization_id,
        )
    raise MailmapSubentryAlreadyExists(
        entry_email=entry_email,
        subentry_email=subentry_email,
        subentry_name=subentry_name,
        organization_id=organization_id,
    )


def raise_subentry_already_exists_in_organization_exception(
    organization_id: str,
    subentry_email: str = "",
    subentry_name: str = "",
) -> None:
    raise MailmapSubentryAlreadyExistsInOrganization(
        subentry_email=subentry_email,
        subentry_name=subentry_name,
        organization_id=organization_id,
    )


def raise_no_subentries_found_exception(
    entry_email: str,
    organization_id: str,
) -> None:  # pragma: no cover
    # logic guarantees that there is at least 1 subentry per entry
    raise NoMailmapSubentriesFound(
        entry_email=entry_email,
        organization_id=organization_id,
    )
