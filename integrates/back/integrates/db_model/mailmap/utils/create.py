from integrates.audit import AuditEvent, add_audit_event
from integrates.db_model.mailmap.types import (
    MailmapEntry,
    MailmapEntryWithSubentries,
    MailmapSubentry,
)
from integrates.db_model.mailmap.utils.conversions import (
    entry_to_subentry,
)
from integrates.db_model.mailmap.utils.get import (
    check_if_entry_already_exists,
    check_if_entry_not_found,
    check_if_subentry_already_exists,
    check_if_subentry_already_exists_in_organization,
)
from integrates.db_model.mailmap.utils.put import (
    put_entry_item,
    put_subentry_item,
)
from integrates.db_model.mailmap.utils.utils import (
    async_gather,
)


async def create_entry_item(
    entry: MailmapEntry,
    organization_id: str,
    raise_exception: bool = True,
) -> bool:
    # Check if entry already exists
    _entry_exists = await check_if_entry_already_exists(
        entry_email=entry["mailmap_entry_email"],
        organization_id=organization_id,
        raise_exception=raise_exception,
    )

    # don't raise exception but prevent adding the item
    if not raise_exception and _entry_exists:
        return False

    # Put subentry
    await put_entry_item(entry=entry, organization_id=organization_id)
    add_audit_event(
        AuditEvent(
            action="CREATE",
            author="unknown",
            metadata=entry,
            object="MailmapEntry",
            object_id=entry["mailmap_entry_email"],
        )
    )

    return True


async def create_subentry_item(
    subentry: MailmapSubentry,
    entry_email: str,
    organization_id: str,
    raise_exception: bool = True,
) -> bool:
    # Check if entry doesn't exist
    await check_if_entry_not_found(  # pragma: no cover
        # exception is already raised in `entry_exists, get_entry_name`
        entry_email=entry_email,
        organization_id=organization_id,
    )
    # Check if subentry already exists
    _subentry_exists = await check_if_subentry_already_exists(
        entry_email=entry_email,
        subentry_email=subentry["mailmap_subentry_email"],
        subentry_name=subentry["mailmap_subentry_name"],
        organization_id=organization_id,
        raise_exception=raise_exception,
    )

    # don't raise exception but prevent adding the item
    if not raise_exception and _subentry_exists:
        return False

    # Check if subentry already exists in organization
    _subentry_exists_in_organization = await check_if_subentry_already_exists_in_organization(
        subentry_email=subentry["mailmap_subentry_email"],
        subentry_name=subentry["mailmap_subentry_name"],
        organization_id=organization_id,
        raise_exception=raise_exception,
    )

    # don't raise exception but prevent adding the item
    if not raise_exception and _subentry_exists_in_organization:
        return False

    # Put subentry
    await put_subentry_item(
        subentry=subentry,
        entry_email=entry_email,
        organization_id=organization_id,
    )
    add_audit_event(
        AuditEvent(
            action="CREATE",
            author="unknown",
            metadata=subentry,
            object="MailmapSubentry",
            object_id=subentry["mailmap_subentry_email"],
        )
    )

    return True


async def create_entry_item_with_subentries_items(
    entry_with_subentries: MailmapEntryWithSubentries,
    organization_id: str,
    raise_exception: bool = True,
) -> tuple[bool, list[bool]]:
    # Create entry
    entry = entry_with_subentries["entry"]
    subentries = entry_with_subentries["subentries"]
    entry_result = await create_entry_item(
        entry=entry,
        organization_id=organization_id,
        raise_exception=raise_exception,
    )

    if len(subentries) == 0:
        subentry_result = await create_subentry_item(
            subentry=entry_to_subentry(entry),
            entry_email=entry["mailmap_entry_email"],
            organization_id=organization_id,
            raise_exception=raise_exception,
        )
        return entry_result, [subentry_result]

    # Create subentries
    subentries_results = await async_gather(
        func=lambda subentry: create_subentry_item(
            subentry=subentry,
            entry_email=entry["mailmap_entry_email"],
            organization_id=organization_id,
            raise_exception=raise_exception,
        ),
        iterable=subentries,
    )

    return entry_result, subentries_results
