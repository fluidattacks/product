from datetime import (
    UTC,
    datetime,
)

from integrates.db_model.mailmap.types import (
    MailmapEntry,
    MailmapEntryWithSubentries,
    MailmapSubentry,
)
from integrates.db_model.mailmap.utils.get import (
    get_entry_item,
    get_subentry_item,
)


async def get_entry_created_at_timestamp(
    entry_email: str,
    organization_id: str,
) -> str | None:
    entry_item = await get_entry_item(
        entry_email=entry_email,
        organization_id=organization_id,
    )
    timestamp = entry_item.get("mailmap_entry_created_at")

    return timestamp


async def get_subentry_created_at_timestamp(
    entry_email: str,
    subentry_email: str,
    subentry_name: str,
    organization_id: str,
) -> str | None:
    subentry_item = await get_subentry_item(
        entry_email=entry_email,
        subentry_email=subentry_email,
        subentry_name=subentry_name,
        organization_id=organization_id,
    )
    timestamp = subentry_item.get("mailmap_subentry_created_at")

    return timestamp


def set_entry_created_at_timestamp(entry: MailmapEntry) -> MailmapEntry:
    timestamp = str(datetime.now(UTC))

    return MailmapEntry(
        mailmap_entry_name=entry["mailmap_entry_name"],
        mailmap_entry_email=entry["mailmap_entry_email"],
        mailmap_entry_created_at=timestamp,
        mailmap_entry_updated_at=timestamp,
    )


def set_subentry_created_at_timestamp(
    subentry: MailmapSubentry,
) -> MailmapSubentry:
    timestamp = str(datetime.now(UTC))

    return MailmapSubentry(
        mailmap_subentry_name=subentry["mailmap_subentry_name"],
        mailmap_subentry_email=subentry["mailmap_subentry_email"],
        mailmap_subentry_created_at=timestamp,
        mailmap_subentry_updated_at=timestamp,
    )


def set_entry_with_subentries_created_at_timestamp(
    entry_with_subentries: MailmapEntryWithSubentries,
) -> MailmapEntryWithSubentries:
    timestamp = str(datetime.now(UTC))

    entry = entry_with_subentries["entry"]
    subentries = entry_with_subentries["subentries"]

    _entry_with_subentries = MailmapEntryWithSubentries(
        entry=MailmapEntry(
            mailmap_entry_name=entry["mailmap_entry_name"],
            mailmap_entry_email=entry["mailmap_entry_email"],
            mailmap_entry_created_at=timestamp,
            mailmap_entry_updated_at=timestamp,
        ),
        subentries=[
            MailmapSubentry(
                mailmap_subentry_name=subentry["mailmap_subentry_name"],
                mailmap_subentry_email=subentry["mailmap_subentry_email"],
                mailmap_subentry_created_at=timestamp,
                mailmap_subentry_updated_at=timestamp,
            )
            for subentry in subentries
        ],
    )

    return _entry_with_subentries


async def set_entry_updated_at_timestamp(
    entry: MailmapEntry,
    entry_email: str,
    organization_id: str,
) -> MailmapEntry:
    created_at_timestamp = await get_entry_created_at_timestamp(
        entry_email=entry_email,
        organization_id=organization_id,
    )

    updated_at_timestamp = str(datetime.now(UTC))

    return MailmapEntry(
        mailmap_entry_name=entry["mailmap_entry_name"],
        mailmap_entry_email=entry["mailmap_entry_email"],
        mailmap_entry_created_at=created_at_timestamp,
        mailmap_entry_updated_at=updated_at_timestamp,
    )


async def set_subentry_updated_at_timestamp(
    subentry: MailmapSubentry,
    entry_email: str,
    subentry_email: str,
    subentry_name: str,
    organization_id: str,
) -> MailmapSubentry:
    created_at_timestamp = await get_subentry_created_at_timestamp(
        entry_email=entry_email,
        subentry_email=subentry_email,
        subentry_name=subentry_name,
        organization_id=organization_id,
    )

    updated_at_timestamp = str(datetime.now(UTC))

    return MailmapSubentry(
        mailmap_subentry_name=subentry["mailmap_subentry_name"],
        mailmap_subentry_email=subentry["mailmap_subentry_email"],
        mailmap_subentry_created_at=created_at_timestamp,
        mailmap_subentry_updated_at=updated_at_timestamp,
    )
