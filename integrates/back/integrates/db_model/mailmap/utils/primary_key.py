from integrates.db_model import (
    TABLE,
)
from integrates.db_model.organizations.utils import (
    remove_org_id_prefix,
)
from integrates.dynamodb import (
    keys,
)
from integrates.dynamodb.types import (
    PrimaryKey,
)


def build_entry_primary_key(
    entry_email: str,
    entry_name: str,
    organization_id: str,
) -> PrimaryKey:
    primary_key = keys.build_key(
        facet=TABLE.facets["mailmap_entry"],
        values={
            "entry_email": entry_email,
            "entry_name": entry_name,
            "organization_id": remove_org_id_prefix(organization_id),
        },
    )

    return primary_key


def build_subentry_primary_key(
    entry_email: str,
    subentry_email: str,
    subentry_name: str,
    organization_id: str,
) -> PrimaryKey:
    primary_key = keys.build_key(
        facet=TABLE.facets["mailmap_subentry"],
        values={
            "entry_email": entry_email,
            "subentry_email": subentry_email,
            "subentry_name": subentry_name,
            "organization_id": remove_org_id_prefix(organization_id),
        },
    )
    return primary_key
