from integrates.class_types.types import (
    GenericItem,
)
from integrates.db_model import (
    TABLE,
)
from integrates.db_model.mailmap.types import (
    MailmapEntry,
    MailmapSubentry,
)
from integrates.db_model.mailmap.utils.conversions import (
    entry_to_item,
    subentry_to_item,
)
from integrates.db_model.mailmap.utils.primary_key import (
    build_entry_primary_key,
    build_subentry_primary_key,
)
from integrates.dynamodb import (
    operations,
)


async def put_item(facet_name: str, item: GenericItem) -> None:
    await operations.put_item(facet=TABLE.facets[facet_name], item=item, table=TABLE)


async def put_entry_item(
    entry: MailmapEntry,
    organization_id: str,
) -> None:
    primary_key = build_entry_primary_key(
        entry_email=entry["mailmap_entry_email"],
        entry_name=entry["mailmap_entry_name"],
        organization_id=organization_id,
    )
    item = entry_to_item(
        entry=entry,
        primary_key=primary_key,
        organization_id=organization_id,
    )
    await put_item(facet_name="mailmap_entry", item=item)


async def put_subentry_item(
    subentry: MailmapSubentry,
    entry_email: str,
    organization_id: str,
) -> None:
    primary_key = build_subentry_primary_key(
        entry_email=entry_email,
        subentry_email=subentry["mailmap_subentry_email"],
        subentry_name=subentry["mailmap_subentry_name"],
        organization_id=organization_id,
    )
    item = subentry_to_item(
        subentry=subentry,
        primary_key=primary_key,
        organization_id=organization_id,
    )
    await put_item(facet_name="mailmap_subentry", item=item)
