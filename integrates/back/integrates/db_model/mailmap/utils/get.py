from itertools import (
    groupby,
)
from operator import (
    itemgetter,
)
from typing import (
    Literal,
    cast,
    overload,
)

from boto3.dynamodb.conditions import (
    Key,
)

from integrates.class_types.types import (
    GenericItem,
    Item,
)
from integrates.db_model import (
    TABLE,
)
from integrates.db_model.items import MailmapEntryItem, MailmapSubentryItem
from integrates.db_model.mailmap.constants import (
    GSI_2_FACETS,
    generate_gsi_2_key,
)
from integrates.db_model.mailmap.types import (
    MailmapEntriesConnection,
    MailmapEntryEdge,
)
from integrates.db_model.mailmap.utils.conversions import (
    item_to_entry,
)
from integrates.db_model.mailmap.utils.exceptions import (
    raise_already_exists_exception,
    raise_no_subentries_found_exception,
    raise_not_found_exception,
    raise_organization_not_found_exception,
    raise_subentry_already_exists_in_organization_exception,
)
from integrates.db_model.mailmap.utils.primary_key import (
    build_entry_primary_key,
    build_subentry_primary_key,
)
from integrates.db_model.organizations.get import (
    _get_organization_by_name,
)
from integrates.db_model.organizations.utils import (
    remove_org_id_prefix,
)
from integrates.dynamodb import (
    keys,
    operations,
)
from integrates.dynamodb.types import (
    Index,
    PrimaryKey,
)
from integrates.dynamodb.utils import (
    get_cursor,
)


async def get_item(facet_name: str, primary_key: PrimaryKey) -> Item | None:
    item = await operations.get_item(
        facets=(TABLE.facets[facet_name],),
        key=primary_key,
        table=TABLE,
    )
    return item


async def query_items(facet_name: str, primary_key: PrimaryKey) -> list[Item]:
    key_structure = TABLE.primary_key
    response = await operations.query(
        condition_expression=(
            Key(key_structure.partition_key).eq(primary_key.partition_key)
            & Key(key_structure.sort_key).begins_with(primary_key.sort_key)
        ),
        table=TABLE,
        facets=(TABLE.facets[facet_name],),
    )
    items = list(response.items)
    return items


async def query_items_inverted(facet_name: str, primary_key: PrimaryKey) -> list[Item]:
    key_structure = TABLE.primary_key
    response = await operations.query(
        condition_expression=(
            Key(key_structure.partition_key).begins_with(primary_key.partition_key)
            & Key(key_structure.sort_key).eq(primary_key.sort_key)
        ),
        table=TABLE,
        index=TABLE.indexes["inverted_index"],
        facets=(TABLE.facets[facet_name],),
    )
    items = list(response.items)
    return items


@overload
async def query_all_items(
    facet_name: Literal["mailmap_subentry"], organization_id: str
) -> tuple[MailmapSubentryItem, ...]: ...


@overload
async def query_all_items(
    facet_name: Literal["mailmap_entry"], organization_id: str
) -> tuple[MailmapEntryItem, ...]: ...


async def query_all_items(
    facet_name: Literal["mailmap_subentry", "mailmap_entry"], organization_id: str
) -> tuple[GenericItem, ...]:
    gsi_2_key = generate_gsi_2_key(
        facet_name=facet_name,
        organization_id=organization_id,
    )
    index = TABLE.indexes["gsi_2"]
    key_structure = index.primary_key
    response = await operations.query(
        condition_expression=(Key(key_structure.partition_key).eq(gsi_2_key.partition_key)),
        facets=(GSI_2_FACETS[facet_name],),
        table=TABLE,
        index=index,
    )
    items = response.items
    return cast(tuple[GenericItem], items)


def build_edge(
    index: Index,
    item: MailmapEntryItem,
    organization_id: str,
) -> MailmapEntryEdge:
    return MailmapEntryEdge(
        node=item_to_entry(item, organization_id),
        cursor=get_cursor(index, item, TABLE),
    )


async def query_all_items_with_pagination(
    _after: str,
    _first: int,
    organization_id: str,
) -> MailmapEntriesConnection:
    gsi_2_key = generate_gsi_2_key(
        facet_name="mailmap_entry",
        organization_id=organization_id,
    )
    index = TABLE.indexes["gsi_2"]
    key_structure = index.primary_key
    response = await operations.DynamoClient[MailmapEntryItem].query(
        condition_expression=(Key(key_structure.partition_key).eq(gsi_2_key.partition_key)),
        facets=(GSI_2_FACETS["mailmap_entry"],),
        table=TABLE,
        index=index,
    )
    edges = tuple(build_edge(index, item, organization_id) for item in response.items)
    connection = MailmapEntriesConnection(
        edges=edges,
        page_info=response.page_info,
    )
    return connection


# To check if exactly one time exists
async def item_exists(facet_name: str, primary_key: PrimaryKey) -> bool:
    item = await get_item(facet_name=facet_name, primary_key=primary_key)
    exists = bool(item)
    return exists


# To check if at least one item exists
async def items_exist(facet_name: str, primary_key: PrimaryKey) -> bool:
    items = await query_items(facet_name=facet_name, primary_key=primary_key)
    exist = len(items) > 0
    return exist


# To check if at least one item exists using inverted index
async def items_exist_inverted(
    facet_name: str,
    primary_key: PrimaryKey,
) -> bool:
    items = await query_items_inverted(
        facet_name=facet_name,
        primary_key=primary_key,
    )
    exist = len(items) > 0
    return exist


@overload
async def get_items(
    facet_name: Literal["mailmap_subentry"],
    email: str,
    organization_id: str,
) -> list[MailmapSubentryItem]: ...


@overload
async def get_items(
    facet_name: Literal["mailmap_entry"],
    email: str,
    organization_id: str,
) -> list[MailmapEntryItem]: ...


async def get_items(
    facet_name: Literal["mailmap_subentry", "mailmap_entry"],
    email: str,
    organization_id: str,
) -> list[MailmapEntryItem] | list[MailmapSubentryItem]:
    primary_key = keys.build_key(
        facet=TABLE.facets[facet_name],
        values={
            "organization_id": remove_org_id_prefix(organization_id),
            "entry_email": email,
        },
    )
    items = await query_items(facet_name=facet_name, primary_key=primary_key)

    match facet_name:
        case "mailmap_subentry":
            return cast(list[MailmapSubentryItem], items)
        case "mailmap_entry":
            return cast(list[MailmapEntryItem], items)


async def get_entry_item(entry_email: str, organization_id: str) -> MailmapEntryItem:
    entry_items = await get_items(
        facet_name="mailmap_entry",
        email=entry_email,
        organization_id=organization_id,
    )
    if len(entry_items) == 0:
        raise_not_found_exception(
            facet_name="mailmap_entry",
            entry_email=entry_email,
            organization_id=organization_id,
        )
    entry_item = entry_items[0]
    return entry_item


async def get_subentry_item(
    entry_email: str,
    subentry_email: str,
    subentry_name: str,
    organization_id: str,
    check_if_entry_exists: bool = True,
) -> MailmapSubentryItem:
    if check_if_entry_exists:
        # check if entry doesn't exist
        await check_if_entry_not_found(  # pragma: no cover
            # exception is already raised in `entry_exists, get_entry_name`
            entry_email=entry_email,
            organization_id=organization_id,
        )

    primary_key = build_subentry_primary_key(
        entry_email=entry_email,
        subentry_email=subentry_email,
        subentry_name=subentry_name,
        organization_id=organization_id,
    )
    subentry_items = await query_items(facet_name="mailmap_subentry", primary_key=primary_key)
    if len(subentry_items) == 0:
        raise_not_found_exception(
            facet_name="mailmap_subentry",
            entry_email=entry_email,
            organization_id=organization_id,
            subentry_email=subentry_email,
            subentry_name=subentry_name,
        )
    subentry_item = subentry_items[0]
    return cast(MailmapSubentryItem, subentry_item)


async def get_entry_subentries_items(
    entry_email: str,
    organization_id: str,
) -> list[MailmapSubentryItem]:
    # check if entry doesn't exist
    await check_if_entry_not_found(  # pragma: no cover
        # exception is already raised in `entry_exists, get_entry_name`
        entry_email=entry_email,
        organization_id=organization_id,
    )
    # Get items
    subentry_items = await get_items(
        facet_name="mailmap_subentry",
        email=entry_email,
        organization_id=organization_id,
    )
    return subentry_items


async def get_subentries_count(entry_email: str, organization_id: str) -> int:
    subentry_items = await get_items(
        facet_name="mailmap_subentry",
        email=entry_email,
        organization_id=organization_id,
    )
    subentry_items_count = len(subentry_items)
    return subentry_items_count


async def get_entry_name(entry_email: str, organization_id: str) -> str:
    entry_items = await get_items(
        facet_name="mailmap_entry",
        email=entry_email,
        organization_id=organization_id,
    )
    if len(entry_items) == 0:
        raise_not_found_exception(
            facet_name="mailmap_entry",
            entry_email=entry_email,
            organization_id=organization_id,
        )
    entry_item = entry_items[0]
    entry_name = entry_item["mailmap_entry_name"]
    return entry_name


async def get_entry_first_subentry(
    entry_email: str,
    organization_id: str,
) -> MailmapSubentryItem:
    subentry_items = await get_items(
        facet_name="mailmap_subentry",
        email=entry_email,
        organization_id=organization_id,
    )
    # This shouldn't happen as it's guaranteed to have at least 1 subentry
    # per entry
    if len(subentry_items) == 0:  # pragma: no cover
        # logic guarantees that there is at least 1 subentry per entry
        raise_no_subentries_found_exception(
            entry_email=entry_email,
            organization_id=organization_id,
        )
    first_subentry_item = subentry_items[0]
    return first_subentry_item


def group_subentries_by_pk(
    subentry_items: tuple[MailmapSubentryItem, ...],
) -> dict[str, list[MailmapSubentryItem]]:
    key_func = itemgetter("pk")
    sorted_subentries = sorted(subentry_items, key=key_func)

    subentries_grouped_by_pk = dict(
        map(
            lambda group: (group[0], list(group[1])),
            groupby(sorted_subentries, key=key_func),
        ),
    )

    return subentries_grouped_by_pk


async def organization_exists(organization_id: str) -> bool:
    primary_key = keys.build_key(
        facet=TABLE.facets["organization_metadata"],
        values={
            "id": remove_org_id_prefix(organization_id),
        },
    )
    # Check if item exists
    exists = await items_exist(
        facet_name="organization_metadata",
        primary_key=primary_key,
    )
    return exists


async def ensure_organization_id(organization_id: str) -> str:
    _organization_id = (
        organization_id
        if organization_id.startswith("ORG#")
        else getattr(
            await _get_organization_by_name(organization_name=organization_id),
            "id",
            organization_id,
        )
    )
    return _organization_id


async def check_if_organization_not_found(organization_id: str) -> None:
    _organization_exists = await organization_exists(
        organization_id=organization_id,
    )
    if not _organization_exists:
        raise_organization_not_found_exception(
            organization_id=organization_id,
        )


async def entry_exists(entry_email: str, organization_id: str) -> bool:
    # Build primary key: name not considered
    primary_key = build_entry_primary_key(
        entry_email=entry_email,
        entry_name="",
        organization_id=organization_id,
    )
    # Check if item exists
    exists = await items_exist(
        facet_name="mailmap_entry",
        primary_key=primary_key,
    )
    return exists


async def check_if_entry_already_exists(
    entry_email: str,
    organization_id: str,
    raise_exception: bool = True,
) -> bool:
    # Just the email is considered, not the name
    _entry_exists = await entry_exists(
        entry_email=entry_email,
        organization_id=organization_id,
    )
    if _entry_exists and raise_exception:
        raise_already_exists_exception(
            facet_name="mailmap_entry",
            entry_email=entry_email,
            organization_id=organization_id,
        )
    return _entry_exists


async def check_if_entry_not_found(
    entry_email: str,
    organization_id: str,
) -> None:
    _entry_exists = await entry_exists(
        entry_email=entry_email,
        organization_id=organization_id,
    )
    if not _entry_exists:
        raise_not_found_exception(
            facet_name="mailmap_entry",
            entry_email=entry_email,
            organization_id=organization_id,
        )


async def check_if_subentry_already_exists(
    entry_email: str,
    subentry_email: str,
    subentry_name: str,
    organization_id: str,
    raise_exception: bool = True,
) -> bool:
    _subentry_exists = await subentry_exists(
        entry_email=entry_email,
        subentry_email=subentry_email,
        subentry_name=subentry_name,
        organization_id=organization_id,
    )
    if _subentry_exists and raise_exception:
        raise_already_exists_exception(
            facet_name="mailmap_subentry",
            entry_email=entry_email,
            subentry_email=subentry_email,
            subentry_name=subentry_name,
            organization_id=organization_id,
        )
    return _subentry_exists


async def check_if_subentry_already_exists_in_organization(
    subentry_email: str,
    subentry_name: str,
    organization_id: str,
    raise_exception: bool = True,
) -> bool:
    _subentry_exists_in_organization = await subentry_exists_in_organization(
        subentry_email=subentry_email,
        subentry_name=subentry_name,
        organization_id=organization_id,
    )
    if _subentry_exists_in_organization and raise_exception:
        raise_subentry_already_exists_in_organization_exception(
            subentry_email=subentry_email,
            subentry_name=subentry_name,
            organization_id=organization_id,
        )
    return _subentry_exists_in_organization


async def check_if_subentry_not_found(
    entry_email: str,
    subentry_email: str,
    subentry_name: str,
    organization_id: str,
) -> None:
    _subentry_exists = await subentry_exists(
        entry_email=entry_email,
        subentry_email=subentry_email,
        subentry_name=subentry_name,
        organization_id=organization_id,
    )
    if not _subentry_exists:
        raise_not_found_exception(
            facet_name="mailmap_subentry",
            entry_email=entry_email,
            organization_id=organization_id,
            subentry_email=subentry_email,
            subentry_name=subentry_name,
        )


async def subentry_exists(
    entry_email: str,
    subentry_email: str,
    subentry_name: str,
    organization_id: str,
) -> bool:
    # Build primary key
    primary_key = build_subentry_primary_key(
        entry_email=entry_email,
        subentry_email=subentry_email,
        subentry_name=subentry_name,
        organization_id=organization_id,
    )
    # Check if item exists
    exists = await item_exists(facet_name="mailmap_subentry", primary_key=primary_key)
    return exists


async def subentry_exists_in_organization(
    subentry_email: str,
    subentry_name: str,
    organization_id: str,
) -> bool:
    primary_key = keys.build_key(
        facet=TABLE.facets["mailmap_subentry"],
        values={
            "subentry_email": subentry_email,
            "subentry_name": subentry_name,
            "organization_id": remove_org_id_prefix(organization_id),
        },
    )
    # Check if at least one subentry is found using an inverted index
    exists = await items_exist_inverted(
        facet_name="mailmap_subentry",
        primary_key=primary_key,
    )
    return exists
