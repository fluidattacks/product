from datetime import (
    datetime,
)
from typing import (
    NamedTuple,
)

from integrates.db_model.trials.enums import (
    TrialReason,
)


class Trial(NamedTuple):
    email: str
    completed: bool
    extension_date: datetime | None
    extension_days: int
    start_date: datetime | None
    reason: TrialReason | None


class TrialMetadataToUpdate(NamedTuple):
    completed: bool | None = None
    reason: TrialReason | None = None
