from datetime import (
    datetime,
)

from integrates.db_model.items import TrialItem
from integrates.db_model.trials.enums import TrialReason
from integrates.db_model.trials.types import (
    Trial,
)


def format_trial(item: TrialItem) -> Trial:
    return Trial(
        completed=item["completed"] if item.get("completed") else False,
        email=item["email"],
        extension_date=(
            datetime.fromisoformat(item["extension_date"]) if item.get("extension_date") else None
        ),
        extension_days=item["extension_days"],
        start_date=(datetime.fromisoformat(item["start_date"]) if item.get("start_date") else None),
        reason=TrialReason(item["reason"]) if item.get("reason") else None,
    )
