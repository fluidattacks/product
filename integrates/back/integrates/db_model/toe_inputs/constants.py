from integrates.db_model import (
    HISTORIC_TABLE,
    TABLE,
)
from integrates.dynamodb.types import (
    Facet,
)

GSI_2_FACET = Facet(
    attrs=TABLE.facets["toe_input_metadata"].attrs,
    pk_alias="GROUP#group_name",
    sk_alias="INPUTS#PRESENT#be_present#ROOT#root_id#COMPONENT#component#ENTRYPOINT#entry_point",
)

GSI_2_HISTORIC_STATE_FACET = Facet(
    attrs=HISTORIC_TABLE.facets["toe_input_state"].attrs,
    pk_alias="GROUP#INPUTS#ROOT#COMPONENT#ENTRYPOINT#group_name",
    sk_alias="STATE#state#DATE#iso8601utc",
)
