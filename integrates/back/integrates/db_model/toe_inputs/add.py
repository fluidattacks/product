from boto3.dynamodb.conditions import Attr

from integrates.audit import AuditEvent, add_audit_event
from integrates.custom_exceptions import InvalidParameter, RepeatedToeInput
from integrates.db_model import HISTORIC_TABLE, TABLE
from integrates.db_model.toe_inputs.constants import GSI_2_FACET, GSI_2_HISTORIC_STATE_FACET
from integrates.db_model.toe_inputs.types import ToeInput
from integrates.db_model.toe_inputs.utils import (
    format_toe_input_enriched_state_item,
    format_toe_input_item,
)
from integrates.db_model.utils import get_as_utc_iso_format
from integrates.dynamodb import keys, operations
from integrates.dynamodb.exceptions import ConditionalCheckFailedException


async def add(*, toe_input: ToeInput) -> None:
    if toe_input.state.modified_date is None:
        raise InvalidParameter("modified_date")

    key_structure = TABLE.primary_key
    facet = TABLE.facets["toe_input_metadata"]
    toe_input_key = keys.build_key(
        facet=facet,
        values={
            "component": toe_input.component,
            "entry_point": toe_input.entry_point,
            "environment_id": toe_input.environment_id,
            "group_name": toe_input.group_name,
            "root_id": toe_input.root_id,
        },
    )
    gsi_2_key = keys.build_key(
        facet=GSI_2_FACET,
        values={
            "be_present": str(toe_input.state.be_present).lower(),
            "component": toe_input.component,
            "entry_point": toe_input.entry_point,
            "environment_id": toe_input.environment_id,
            "group_name": toe_input.group_name,
            "root_id": toe_input.root_id,
        },
    )
    toe_input_item = format_toe_input_item(
        toe_input,
        primary_key=toe_input_key,
        gsi_2_key=gsi_2_key,
    )
    condition_expression = Attr(key_structure.partition_key).not_exists()
    try:
        await operations.put_item(
            condition_expression=condition_expression,
            facet=facet,
            item=toe_input_item,
            table=TABLE,
        )
        add_audit_event(
            AuditEvent(
                action="CREATE",
                author=toe_input.state.modified_by or "unknown",
                metadata=toe_input_item,
                object="ToeInput",
                object_id=toe_input_key.sort_key,
            )
        )
    except ConditionalCheckFailedException as ex:
        raise RepeatedToeInput() from ex


async def add_historic(*, toe_input: ToeInput) -> None:
    if toe_input.state.modified_date is None:
        raise InvalidParameter("modified_date")

    key_structure = HISTORIC_TABLE.primary_key

    historic_key = keys.build_key(
        facet=HISTORIC_TABLE.facets["toe_input_state"],
        values={
            "component": toe_input.component,
            "entry_point": toe_input.entry_point,
            "group_name": toe_input.group_name,
            "root_id": toe_input.root_id,
            "iso8601utc": get_as_utc_iso_format(toe_input.state.modified_date),
        },
    )

    gsi_2_historic_key = keys.build_key(
        facet=GSI_2_HISTORIC_STATE_FACET,
        values={
            "group_name": toe_input.group_name,
            "iso8601utc": get_as_utc_iso_format(toe_input.state.modified_date),
            "state": "state",
        },
    )
    historic_item = format_toe_input_enriched_state_item(
        toe_input,
        primary_key=historic_key,
        gsi_2_key=gsi_2_historic_key,
    )
    try:
        await operations.put_item(
            facet=HISTORIC_TABLE.facets["toe_input_state"],
            condition_expression=Attr(key_structure.sort_key).not_exists(),
            item=historic_item,
            table=HISTORIC_TABLE,
        )
    except ConditionalCheckFailedException as ex:
        raise RepeatedToeInput() from ex
