from collections.abc import Iterable

from boto3.dynamodb.conditions import Key
from snowflake.connector.cursor import SnowflakeCursor

from integrates.archive.domain import archive_group_toe_inputs
from integrates.audit import AuditEvent, add_audit_event
from integrates.class_types.types import Item
from integrates.context import FI_ENVIRONMENT
from integrates.db_model import TABLE
from integrates.db_model.toe_inputs.types import ToeInput
from integrates.db_model.utils import archive
from integrates.dynamodb import keys, operations
from integrates.dynamodb.types import PrimaryKey


async def _remove_toe_inputs(items: Iterable[Item]) -> None:
    key_structure = TABLE.primary_key
    await operations.batch_delete_item(
        keys=tuple(
            PrimaryKey(
                partition_key=item[key_structure.partition_key],
                sort_key=item[key_structure.sort_key],
            )
            for item in items
        ),
        table=TABLE,
    )


async def remove_toe_inputs_items(cursor: SnowflakeCursor, items: Iterable[Item]) -> None:
    await archive_group_toe_inputs(cursor, items)
    await _remove_toe_inputs(items)


async def remove_group_toe_inputs(*, group_name: str) -> None:
    facet = TABLE.facets["toe_input_metadata"]
    primary_key = keys.build_key(
        facet=facet,
        values={"group_name": group_name},
    )
    key_structure = TABLE.primary_key
    response = await operations.query(
        condition_expression=(
            Key(key_structure.partition_key).eq(primary_key.partition_key)
            & Key(key_structure.sort_key).begins_with(
                primary_key.sort_key.replace("#ROOT#COMPONENT#ENTRYPOINT", ""),
            )
        ),
        facets=(facet,),
        table=TABLE,
    )
    if not response.items:
        return

    await archive(response.items, remove_toe_inputs_items)

    if FI_ENVIRONMENT != "production":
        await _remove_toe_inputs(response.items)


async def remove_toe_inputs(*, toe_inputs: Iterable[ToeInput]) -> None:
    facet = TABLE.facets["toe_input_metadata"]
    primary_keys = tuple(
        keys.build_key(
            facet=facet,
            values={
                "component": toe_input.component,
                "entry_point": toe_input.entry_point,
                "group_name": toe_input.group_name,
                "root_id": toe_input.root_id,
            },
        )
        for toe_input in toe_inputs
    )
    items = await operations.batch_get_item(keys=primary_keys, table=TABLE)
    if not items:
        return

    await archive(items, archive_group_toe_inputs)
    await operations.batch_delete_item(keys=primary_keys, table=TABLE)
    for key in primary_keys:
        add_audit_event(
            AuditEvent(
                action="DELETE",
                author="unknown",
                metadata={},
                object="ToePort",
                object_id=key.sort_key,
            )
        )
