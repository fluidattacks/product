from collections.abc import (
    Iterable,
)

from aiodataloader import (
    DataLoader,
)
from aioextensions import (
    collect,
)
from boto3.dynamodb.conditions import (
    Key,
)

from integrates.db_model import (
    HISTORIC_TABLE,
    TABLE,
)
from integrates.db_model.historic_items import ToeInputStateItem
from integrates.db_model.items import ToeInputItem
from integrates.db_model.toe_inputs.constants import (
    GSI_2_FACET,
    GSI_2_HISTORIC_STATE_FACET,
)
from integrates.db_model.toe_inputs.types import (
    GroupHistoricToeInputsRequest,
    GroupToeInputsRequest,
    RootToeInputsRequest,
    ToeInput,
    ToeInputEnrichedState,
    ToeInputRequest,
    ToeInputsConnection,
)
from integrates.db_model.toe_inputs.utils import (
    format_toe_input,
    format_toe_input_edge,
    format_toe_input_enriched_state,
)
from integrates.db_model.types import (
    Connection,
)
from integrates.db_model.utils import (
    format_connection,
)
from integrates.dynamodb import (
    conditions,
    keys,
    operations,
)
from integrates.dynamodb.exceptions import (
    ValidationException,
)
from integrates.dynamodb.types import (
    ItemQueryResponse,
    PageInfo,
)


async def _get_toe_inputs(
    requests: Iterable[ToeInputRequest],
) -> list[ToeInput | None]:
    primary_keys = tuple(
        keys.build_key(
            facet=TABLE.facets["toe_input_metadata"],
            values={
                "component": request.component,
                "entry_point": request.entry_point,
                "group_name": request.group_name,
                "root_id": request.root_id,
            },
        )
        for request in requests
    )
    items = await operations.DynamoClient[ToeInputItem].batch_get_item(
        keys=primary_keys, table=TABLE
    )

    response = {
        ToeInputRequest(
            component=toe_input.component,
            entry_point=toe_input.entry_point,
            group_name=toe_input.group_name,
            root_id=toe_input.root_id,
        ): toe_input
        for toe_input in list(
            format_toe_input(
                item["pk"].split("#")[1],
                item,
            )
            for item in items
        )
    }

    return [response.get(request) for request in requests]


class ToeInputLoader(DataLoader[ToeInputRequest, ToeInput | None]):
    async def batch_load_fn(self, requests: Iterable[ToeInputRequest]) -> list[ToeInput | None]:
        return await _get_toe_inputs(requests)


async def _get_toe_inputs_enriched_historic_state_by_group(
    request: GroupHistoricToeInputsRequest,
) -> Connection[ToeInputEnrichedState]:
    primary_key = keys.build_key(
        facet=GSI_2_HISTORIC_STATE_FACET,
        values={"group_name": request.group_name, "state": "state"},
    )
    index = HISTORIC_TABLE.indexes["gsi_2"]
    key_structure = index.primary_key
    try:
        response = await operations.DynamoClient[ToeInputStateItem].query(
            after=request.after,
            condition_expression=(
                Key(key_structure.partition_key).eq(primary_key.partition_key)
                & Key(key_structure.sort_key).begins_with(primary_key.sort_key)
            )
            if not request.start_date or not request.end_date
            else (
                Key(key_structure.partition_key).eq(primary_key.partition_key)
                & Key(key_structure.sort_key).between(
                    primary_key.sort_key + "#" + request.start_date.strftime("%Y-%m-%d"),
                    primary_key.sort_key + "#" + request.end_date.strftime("%Y-%m-%d"),
                )
            ),
            facets=(GSI_2_HISTORIC_STATE_FACET,),
            filter_expression=conditions.get_filter_expression(request.filters),
            index=index,
            limit=request.first,
            paginate=request.paginate,
            table=HISTORIC_TABLE,
        )
    except ValidationException:
        response = ItemQueryResponse(
            items=tuple(),
            page_info=PageInfo(has_next_page=False, end_cursor=""),
        )

    return format_connection(
        index=index,
        formatter=format_toe_input_enriched_state,
        response=response,
        table=HISTORIC_TABLE,
    )


class GroupToeInputsEnrichedHistoricStateLoader(
    DataLoader[GroupHistoricToeInputsRequest, Connection[ToeInputEnrichedState]],
):
    async def batch_load_fn(
        self,
        requests: Iterable[GroupHistoricToeInputsRequest],
    ) -> list[Connection[ToeInputEnrichedState]]:
        return list(
            await collect(
                tuple(
                    map(
                        _get_toe_inputs_enriched_historic_state_by_group,
                        requests,
                    ),
                ),
                workers=32,
            ),
        )

    async def load_nodes(
        self,
        request: GroupHistoricToeInputsRequest,
    ) -> list[ToeInputEnrichedState]:
        connection = await self.load(request)
        return [edge.node for edge in connection.edges]


async def _get_toe_inputs_by_group(
    request: GroupToeInputsRequest,
) -> ToeInputsConnection:
    if request.be_present is None:
        facet = TABLE.facets["toe_input_metadata"]
        primary_key = keys.build_key(
            facet=facet,
            values={"group_name": request.group_name},
        )
        index = None
        key_structure = TABLE.primary_key
    else:
        facet = GSI_2_FACET
        primary_key = keys.build_key(
            facet=facet,
            values={
                "group_name": request.group_name,
                "be_present": str(request.be_present).lower(),
            },
        )
        index = TABLE.indexes["gsi_2"]
        key_structure = index.primary_key

    try:
        response = await operations.DynamoClient[ToeInputItem].query(
            after=request.after,
            condition_expression=(
                Key(key_structure.partition_key).eq(primary_key.partition_key)
                & Key(key_structure.sort_key).begins_with(
                    primary_key.sort_key.replace("#ROOT#COMPONENT#ENTRYPOINT", ""),
                )
            ),
            facets=(TABLE.facets["toe_input_metadata"],),
            index=index,
            limit=request.first,
            paginate=request.paginate,
            table=TABLE,
        )
        connection = ToeInputsConnection(
            edges=tuple(
                format_toe_input_edge(request.group_name, index, item, TABLE)
                for item in response.items
            ),
            page_info=response.page_info,
        )
    except ValidationException:
        connection = ToeInputsConnection(
            edges=tuple(),
            page_info=PageInfo(has_next_page=False, end_cursor=""),
        )

    return connection


class GroupToeInputsLoader(DataLoader[GroupToeInputsRequest, ToeInputsConnection]):
    async def batch_load_fn(
        self,
        requests: Iterable[GroupToeInputsRequest],
    ) -> list[ToeInputsConnection]:
        return list(
            await collect(
                tuple(map(_get_toe_inputs_by_group, requests)),
                workers=32,
            ),
        )

    async def load_nodes(self, request: GroupToeInputsRequest) -> list[ToeInput]:
        connection = await self.load(request)
        return [edge.node for edge in connection.edges]


async def _get_toe_inputs_by_root(
    request: RootToeInputsRequest,
    toe_input_dataloader: ToeInputLoader,
) -> ToeInputsConnection:
    if request.be_present is None:
        facet = TABLE.facets["toe_input_metadata"]
        primary_key = keys.build_key(
            facet=facet,
            values={
                "group_name": request.group_name,
                "root_id": request.root_id,
            },
        )
        index = None
        key_structure = TABLE.primary_key
    else:
        facet = GSI_2_FACET
        primary_key = keys.build_key(
            facet=facet,
            values={
                "group_name": request.group_name,
                "be_present": str(request.be_present).lower(),
                "root_id": request.root_id,
            },
        )
        index = TABLE.indexes["gsi_2"]
        key_structure = index.primary_key
    try:
        response = await operations.DynamoClient[ToeInputItem].query(
            after=request.after,
            condition_expression=(
                Key(key_structure.partition_key).eq(primary_key.partition_key)
                & Key(key_structure.sort_key).begins_with(
                    primary_key.sort_key.replace("#ENTRYPOINT", ""),
                )
            ),
            facets=(TABLE.facets["toe_input_metadata"],),
            index=index,
            limit=request.first,
            paginate=request.paginate,
            table=TABLE,
        )
        connection = ToeInputsConnection(
            edges=tuple(
                format_toe_input_edge(request.group_name, index, item, TABLE)
                for item in response.items
            ),
            page_info=response.page_info,
        )
        for edge in connection.edges:
            toe_input_dataloader.prime(
                ToeInputRequest(
                    component=edge.node.component,
                    entry_point=edge.node.entry_point,
                    group_name=edge.node.group_name,
                    root_id=edge.node.root_id,
                ),
                edge.node,
            )
    except ValidationException:
        connection = ToeInputsConnection(
            edges=tuple(),
            page_info=PageInfo(has_next_page=False, end_cursor=""),
        )

    return connection


class RootToeInputsLoader(DataLoader[RootToeInputsRequest, ToeInputsConnection]):
    def __init__(self, toe_input_dataloader: ToeInputLoader) -> None:
        super().__init__()
        self.toe_input_dataloader = toe_input_dataloader

    async def batch_load_fn(
        self,
        requests: Iterable[RootToeInputsRequest],
    ) -> list[ToeInputsConnection]:
        return list(
            await collect(
                tuple(
                    _get_toe_inputs_by_root(
                        request=request,
                        toe_input_dataloader=self.toe_input_dataloader,
                    )
                    for request in requests
                ),
                workers=32,
            ),
        )

    async def load_nodes(self, request: RootToeInputsRequest) -> list[ToeInput]:
        connection = await self.load(request)
        return [edge.node for edge in connection.edges]
