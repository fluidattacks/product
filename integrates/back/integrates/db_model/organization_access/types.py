from datetime import (
    datetime,
)
from typing import (
    NamedTuple,
)


class OrganizationInvitation(NamedTuple):
    is_used: bool
    role: str
    url_token: str


class OrganizationAccessState(NamedTuple):
    modified_date: datetime
    modified_by: str
    has_access: bool
    invitation: OrganizationInvitation | None = None
    role: str | None = None


class OrganizationAccess(NamedTuple):
    organization_id: str
    email: str
    state: OrganizationAccessState
    expiration_time: int | None = None


class OrganizationAccessMetadataToUpdate(NamedTuple):
    state: OrganizationAccessState
    expiration_time: int | None = None


class OrganizationAccessRequest(NamedTuple):
    email: str
    organization_id: str
