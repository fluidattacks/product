from datetime import (
    datetime,
)

import simplejson as json

from integrates.class_types.types import (
    Item,
)
from integrates.db_model.items import (
    OrganizationAccessItem,
    OrganizationAccessStateItem,
    OrganizationInvitationItem,
)
from integrates.db_model.organization_access.types import (
    OrganizationAccess,
    OrganizationAccessMetadataToUpdate,
    OrganizationAccessState,
    OrganizationInvitation,
)
from integrates.db_model.organizations.utils import (
    add_org_id_prefix,
    remove_org_id_prefix,
)
from integrates.db_model.utils import (
    serialize,
)


def format_organization_access(item: OrganizationAccessItem) -> OrganizationAccess:
    state = item["state"]
    return OrganizationAccess(
        email=str(item["email"]).lower().strip(),
        organization_id=add_org_id_prefix(item["organization_id"]),
        expiration_time=int(item["expiration_time"]) if item.get("expiration_time") else None,
        state=OrganizationAccessState(
            modified_by=state["modified_by"],
            modified_date=datetime.fromisoformat(state["modified_date"]),
            has_access=_format_has_access(state),
            invitation=_format_invitation(state["invitation"]) if state.get("invitation") else None,
            role=_format_role(state),
        ),
    )


def _format_has_access(item: OrganizationAccessStateItem) -> bool:
    return bool(item["has_access"]) if item.get("has_access") is not None else False


def _format_invitation(item: OrganizationInvitationItem) -> OrganizationInvitation:
    return OrganizationInvitation(
        is_used=bool(item["is_used"]),
        role=item["role"],
        url_token=item["url_token"],
    )


def _format_role(item: OrganizationAccessStateItem) -> str | None:
    return item.get("role")


def format_metadata_item(
    email: str,
    metadata: OrganizationAccessMetadataToUpdate,
    organization_id: str,
) -> Item:
    state_item = json.loads(json.dumps(metadata.state, default=serialize))
    item: Item = {
        "email": email.lower().strip(),
        "expiration_time": metadata.expiration_time,
        "organization_id": remove_org_id_prefix(organization_id),
        "state": state_item,
    }
    return {
        key: None if not value and value is not False else value
        for key, value in item.items()
        if value is not None
    }
