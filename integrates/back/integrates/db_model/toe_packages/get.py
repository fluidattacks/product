from collections.abc import (
    Iterable,
)

from aiodataloader import (
    DataLoader,
)
from aioextensions import (
    collect,
)
from boto3.dynamodb.conditions import (
    Key,
)

from integrates.db_model import (
    TABLE,
)
from integrates.db_model.items import (
    ToePackagesImageItem,
    ToePackagesItem,
    ToePackagesVulnerabilityItem,
)
from integrates.db_model.toe_packages.constants import (
    GSI_2_FACET,
)
from integrates.db_model.toe_packages.types import (
    GroupToePackagesRequest,
    RootDockerImagePackagesRequest,
    RootDockerImagePkg,
    RootToePackagesRequest,
    ToePackage,
    ToePackageRequest,
    ToePackagesConnection,
    ToePackageVulnerability,
    ToePackageVulnerabilityRequest,
)
from integrates.db_model.toe_packages.utils import (
    format_root_image_pkg,
    format_toe_package,
    format_toe_packages_edge,
    format_toe_pkg_vuln,
)
from integrates.dynamodb import (
    keys,
    operations,
)


async def _get_packages_group(
    request: GroupToePackagesRequest,
) -> ToePackagesConnection:
    if request.be_present is None and request.vulnerable is None:
        facet = TABLE.facets["toe_packages_metadata"]
        primary_key = keys.build_key(
            facet=facet,
            values={
                "group_name": request.group_name,
            },
        )
        index = None
        key_structure = TABLE.primary_key
        condition_expression = Key(key_structure.partition_key).eq(primary_key.partition_key) & Key(
            key_structure.sort_key,
        ).begins_with(primary_key.sort_key.replace("#ROOT#PACKAGE#VERSION", ""))
    else:
        facet = GSI_2_FACET
        primary_key = keys.build_key(
            facet=facet,
            values={
                "group_name": request.group_name,
                "be_present": "true"
                if request.be_present is None
                else str(request.be_present).lower(),
                "vulnerable": str(request.vulnerable).lower()
                if request.vulnerable is not None
                else "",
            },
        )
        index = TABLE.indexes["gsi_2"]
        key_structure = index.primary_key
        condition_expression = Key(key_structure.partition_key).eq(primary_key.partition_key) & Key(
            key_structure.sort_key,
        ).begins_with(
            primary_key.sort_key.replace(
                "#VULNERABLE" if request.vulnerable is not None else "",
                "",
            ),
        )

    response = await operations.DynamoClient[ToePackagesItem].query(
        after=request.after,
        condition_expression=condition_expression,
        facets=(facet,),
        table=TABLE,
        index=index,
        limit=request.first,
        paginate=request.paginate,
    )

    return ToePackagesConnection(
        edges=tuple(
            format_toe_packages_edge(item=item, index=index, table=TABLE) for item in response.items
        ),
        page_info=response.page_info,
    )


class GroupToePackagesLoader(DataLoader[GroupToePackagesRequest, ToePackagesConnection]):
    async def batch_load_fn(
        self,
        requests: Iterable[GroupToePackagesRequest],
    ) -> list[ToePackagesConnection]:
        return list(await collect(tuple(map(_get_packages_group, requests)), workers=32))

    async def load_nodes(self, request: GroupToePackagesRequest) -> list[ToePackage]:
        connection = await self.load(request)
        return [edge.node for edge in connection.edges]


async def _get_packages_root(
    request: RootToePackagesRequest,
) -> ToePackagesConnection:
    facet = TABLE.facets["toe_packages_metadata"]
    primary_key = keys.build_key(
        facet=facet,
        values={
            "group_name": request.group_name,
            "root_id": request.root_id,
        },
    )
    index = None
    key_structure = TABLE.primary_key
    condition_expression = Key(key_structure.partition_key).eq(primary_key.partition_key) & Key(
        key_structure.sort_key,
    ).begins_with(primary_key.sort_key.replace("#PACKAGE#VERSION", ""))

    if request.be_present is None and request.vulnerable is None:
        filter_expression = None
    elif request.be_present is not None and request.vulnerable is None:
        filter_expression = Key("sk_2").begins_with(
            f"PKGS#PRESENT#{str(request.be_present).lower()}",
        )
    else:
        sk_index = "" if request.vulnerable is not None else "#VULNERABLE"
        facet = GSI_2_FACET
        primary_key = keys.build_key(
            facet=facet,
            values={
                "group_name": request.group_name,
                "be_present": "true"
                if request.be_present is None
                else str(request.be_present).lower(),
                "vulnerable": str(request.vulnerable).lower(),
            },
        )
        index = TABLE.indexes["gsi_2"]
        key_structure = index.primary_key
        condition_expression = Key(key_structure.partition_key).eq(primary_key.partition_key) & Key(
            key_structure.sort_key,
        ).begins_with(primary_key.sort_key.replace(sk_index, ""))
        filter_expression = None

    response = await operations.DynamoClient[ToePackagesItem].query(
        after=request.after,
        condition_expression=condition_expression,
        filter_expression=filter_expression,
        facets=(facet,),
        index=index,
        table=TABLE,
        limit=request.first,
        paginate=request.paginate,
    )
    return ToePackagesConnection(
        edges=tuple(
            format_toe_packages_edge(item=item, index=index, table=TABLE) for item in response.items
        ),
        page_info=response.page_info,
    )


class RootToePackagesLoader(DataLoader[RootToePackagesRequest, ToePackagesConnection]):
    async def batch_load_fn(
        self,
        requests: Iterable[RootToePackagesRequest],
    ) -> list[ToePackagesConnection]:
        return list(await collect(tuple(map(_get_packages_root, requests)), workers=32))


async def _get_package(
    requests: Iterable[ToePackageRequest],
) -> list[ToePackage | None]:
    facet = TABLE.facets["toe_packages_metadata"]
    primary_keys = tuple(
        keys.build_key(
            facet=facet,
            values={
                "group_name": request.group_name,
                "root_id": request.root_id,
                "name": request.name,
                "version": request.version,
            },
        )
        for request in requests
    )
    items = await operations.DynamoClient[ToePackagesItem].batch_get_item(
        keys=primary_keys, table=TABLE
    )
    response = {
        ToePackageRequest(
            group_name=toe_package.group_name,
            root_id=toe_package.root_id,
            name=toe_package.name,
            version=toe_package.version,
        ): toe_package
        for toe_package in [format_toe_package(item) for item in items]
    }

    return [response.get(request) for request in requests]


class ToePackageLoader(DataLoader[ToePackageRequest, ToePackage | None]):
    async def batch_load_fn(self, requests: Iterable[ToePackageRequest]) -> list[ToePackage | None]:
        return await _get_package(requests)


async def _get_root_image_packages(
    request: RootDockerImagePackagesRequest,
) -> list[RootDockerImagePkg]:
    facet = TABLE.facets["toe_packages_image"]
    primary_key = keys.build_key(
        facet=facet,
        values={
            "uri": request.uri,
            "root_id": request.root_id,
        },
    )
    index = TABLE.indexes["inverted_index"]
    key_structure = index.primary_key
    condition_expression = Key(key_structure.partition_key).eq(primary_key.sort_key) & Key(
        key_structure.sort_key,
    ).begins_with(primary_key.partition_key.replace("#PACKAGE#VERSION", ""))

    response = await operations.DynamoClient[ToePackagesImageItem].query(
        condition_expression=condition_expression,
        facets=(facet,),
        index=index,
        table=TABLE,
    )
    return [format_root_image_pkg(item) for item in response.items]


async def _get_toe_package_vulnerabilities(
    request: ToePackageVulnerabilityRequest,
) -> list[ToePackageVulnerability]:
    facet = TABLE.facets["toe_packages_vulnerability"]
    primary_key = keys.build_key(
        facet=facet,
        values={
            "root_id": request.root_id,
            "name": request.name,
            "version": request.version,
        },
    )
    key_structure = TABLE.primary_key
    condition_expression = Key(key_structure.partition_key).eq(primary_key.partition_key) & Key(
        key_structure.sort_key,
    ).begins_with(primary_key.sort_key)

    response = await operations.DynamoClient[ToePackagesVulnerabilityItem].query(
        condition_expression=condition_expression,
        facets=(facet,),
        index=None,
        table=TABLE,
    )
    return [format_toe_pkg_vuln(item) for item in response.items]


class RootDockerImagePackagesLoader(
    DataLoader[RootDockerImagePackagesRequest, list[RootDockerImagePkg]],
):
    async def batch_load_fn(
        self,
        requests: Iterable[RootDockerImagePackagesRequest],
    ) -> list[list[RootDockerImagePkg]]:
        return list(await collect(tuple(map(_get_root_image_packages, requests)), workers=32))


class ToePackageVulnerabilityLoader(
    DataLoader[ToePackageVulnerabilityRequest, list[ToePackageVulnerability]],
):
    async def batch_load_fn(
        self,
        requests: Iterable[ToePackageVulnerabilityRequest],
    ) -> list[list[ToePackageVulnerability]]:
        return list(
            await collect(
                tuple(map(_get_toe_package_vulnerabilities, requests)),
                workers=32,
            ),
        )
