from integrates.audit import AuditEvent, add_audit_event
from integrates.db_model import (
    TABLE,
)
from integrates.db_model.toe_packages.types import (
    ToePackage,
    ToePackageVulnerability,
)
from integrates.dynamodb import (
    keys,
)
from integrates.dynamodb.operations import (
    delete_item,
)


async def remove_docker_image_package(*, uri: str, toe_package: ToePackage) -> None:
    primary_key = keys.build_key(
        facet=TABLE.facets["toe_packages_image"],
        values={
            "uri": uri,
            "root_id": toe_package.root_id,
            "name": toe_package.name,
            "version": toe_package.version,
        },
    )

    await delete_item(key=primary_key, table=TABLE)


async def remove_package_vulnerability(
    toe_package: ToePackageVulnerability,
) -> None:
    facet = TABLE.facets["toe_packages_vulnerability"]
    primary_key = keys.build_key(
        facet=facet,
        values={
            "vuln_id": toe_package.vuln_id,
            "root_id": toe_package.root_id,
            "name": toe_package.name,
            "version": toe_package.version,
        },
    )
    await delete_item(key=primary_key, table=TABLE)


async def remove_package(
    toe_package: ToePackage,
) -> None:
    facet = TABLE.facets["toe_packages_metadata"]
    primary_key = keys.build_key(
        facet=facet,
        values={
            "group_name": toe_package.group_name,
            "root_id": toe_package.root_id,
            "name": toe_package.name,
            "version": toe_package.version,
        },
    )
    await delete_item(key=primary_key, table=TABLE)
    add_audit_event(
        AuditEvent(
            action="DELETE",
            author="unknown",
            metadata={},
            object="ToePackage",
            object_id=primary_key.sort_key,
        )
    )
