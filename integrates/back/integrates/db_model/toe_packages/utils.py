from datetime import (
    UTC,
    datetime,
)
from decimal import (
    Decimal,
)

import simplejson as json

from integrates.class_types.types import (
    Item,
)
from integrates.db_model.items import (
    ArtifactItem,
    ToePackagesImageItem,
    ToePackagesItem,
    ToePackagesVulnerabilityItem,
)
from integrates.db_model.toe_packages.types import (
    Artifact,
    Digest,
    RootDockerImagePkg,
    ToePackage,
    ToePackageAdvisory,
    ToePackageCoordinates,
    ToePackageEdge,
    ToePackageHealthMetadata,
    ToePackageVulnerability,
)
from integrates.db_model.utils import (
    serialize,
)
from integrates.dynamodb.types import (
    Index,
    PrimaryKey,
    Table,
)
from integrates.dynamodb.utils import (
    get_cursor,
)

INTEGRATES_EMAIL = "integrates@fluidattacks.com"


def format_toe_packages_edge(
    index: Index | None,
    item: ToePackagesItem,
    table: Table,
) -> ToePackageEdge:
    return ToePackageEdge(
        node=format_toe_package(item),
        cursor=get_cursor(index, item, table),
    )


def _format_health_metadata_artifact(
    artifact: ArtifactItem | None,
) -> Artifact | None:
    if artifact is None:
        return None
    integrity = artifact.get("integrity")
    return Artifact(
        url=artifact["url"],
        integrity=(
            Digest(
                algorithm=integrity.get("algorithm"),
                value=integrity.get("value"),
            )
            if integrity
            else None
        ),
    )


def format_toe_package(item: ToePackagesItem) -> ToePackage:
    health_metadata = item.get("health_metadata")
    vuln_ids: list[str] | set[str] | None = item.get("vulnerability_ids")
    return ToePackage(
        group_name=item["group_name"],
        root_id=item["root_id"],
        name=item["name"],
        version=item["version"],
        type_=item["type_"],
        language=item["language"],
        outdated=item.get("outdated"),
        platform=item.get("platform"),
        package_advisories=[
            ToePackageAdvisory(
                cpes=advisory["cpes"],
                description=advisory.get("description"),
                epss=advisory.get("epss"),
                id=advisory["id"],
                namespace=advisory["namespace"],
                percentile=advisory.get("percentile"),
                severity=advisory["severity"],
                urls=advisory["urls"],
                version_constraint=advisory.get("version_constraint"),
            )
            for advisory in item.get("package_advisories", [])
        ],
        package_url=item["package_url"],
        found_by=item.get("found_by"),
        modified_date=datetime.fromisoformat(
            item["modified_date"] if "modified_date" in item else datetime.now(tz=UTC).isoformat(),
        ),
        id=item["id"],
        be_present=item["be_present"],
        locations=[
            ToePackageCoordinates(
                path=itm["path"],
                line=itm.get("line", None),
                layer=itm.get("layer", None),
                image_ref=itm.get("image_ref", None),
                dependency_type=itm.get("dependency_type", None),
                scope=itm.get("scope", None),
            )
            for itm in item["locations"]
        ],
        licenses=item.get("licenses"),
        url=item.get("url"),
        vulnerable=item.get("vulnerable"),
        vulnerability_ids=set(vuln_ids) if vuln_ids else None,
        health_metadata=ToePackageHealthMetadata(
            artifact=_format_health_metadata_artifact(health_metadata.get("artifact")),
            authors=health_metadata.get("authors"),
            latest_version_created_at=health_metadata.get("latest_version_created_at"),
            latest_version=health_metadata.get("latest_version"),
        )
        if health_metadata
        else None,
        has_related_vulnerabilities=item.get("has_related_vulnerabilities"),
    )


def format_root_image_pkg(item: ToePackagesImageItem) -> RootDockerImagePkg:
    sk_ = item["sk"].split("#")
    pk_ = item["pk"].split("#")
    return RootDockerImagePkg(uri=sk_[1], root_id=pk_[2], name=pk_[4], version=pk_[6])


def format_root_image_pkg_item(
    primary_key: PrimaryKey,
) -> ToePackagesVulnerabilityItem:
    return {
        "pk": primary_key.partition_key,
        "sk": primary_key.sort_key,
    }


def format_toe_pkg_vuln(
    item: ToePackagesVulnerabilityItem,
) -> ToePackageVulnerability:
    sk_ = item["sk"].split("#")
    pk_ = item["pk"].split("#")
    return ToePackageVulnerability(root_id=pk_[2], name=pk_[4], version=pk_[6], vuln_id=sk_[1])


def format_toe_package_item(
    primary_key: PrimaryKey,
    toe_package: ToePackage,
    gsi_2_key: PrimaryKey,
) -> ToePackagesItem:
    package: Item = json.loads(json.dumps(toe_package, default=serialize), parse_float=Decimal)
    toe_package_item: ToePackagesItem = {
        "pk": primary_key.partition_key,
        "sk": primary_key.sort_key,
        "sk_2": gsi_2_key.sort_key,
        "pk_2": gsi_2_key.partition_key,
        **package,  # type: ignore [typeddict-item]
    }
    return toe_package_item
