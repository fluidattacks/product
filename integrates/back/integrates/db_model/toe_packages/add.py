from aioextensions import (
    collect,
)
from boto3.dynamodb.conditions import (
    Attr,
)

from integrates.audit import AuditEvent, add_audit_event
from integrates.custom_exceptions import (
    RepeatedToePackage,
)
from integrates.db_model import (
    TABLE,
)
from integrates.db_model.toe_packages.constants import (
    GSI_2_FACET,
)
from integrates.db_model.toe_packages.types import (
    ToePackage,
    ToePackageVulnerability,
)
from integrates.db_model.toe_packages.utils import (
    format_root_image_pkg_item,
    format_toe_package_item,
)
from integrates.dynamodb import (
    keys,
    operations,
)
from integrates.dynamodb.exceptions import (
    ConditionalCheckFailedException,
)


async def add_package_vulnerability(
    toe_package: ToePackageVulnerability,
) -> None:
    facet = TABLE.facets["toe_packages_vulnerability"]
    toe_packages_key = keys.build_key(
        facet=facet,
        values={
            "vuln_id": toe_package.vuln_id,
            "root_id": toe_package.root_id,
            "name": toe_package.name,
            "version": toe_package.version,
        },
    )
    docker_image_pkg_item = format_root_image_pkg_item(
        primary_key=toe_packages_key,
    )
    await operations.put_item(
        facet=facet,
        item=docker_image_pkg_item,
        table=TABLE,
    )


async def add_docker_image_package(*, uri: str, toe_package: ToePackage | dict[str, str]) -> None:
    facet = TABLE.facets["toe_packages_image"]
    toe_packages_key = keys.build_key(
        facet=facet,
        values={
            "uri": uri,
            "root_id": toe_package.root_id,
            "name": toe_package.name,
            "version": toe_package.version,
        }
        if isinstance(toe_package, ToePackage)
        else {
            "uri": uri,
            "root_id": toe_package["root_id"],
            "name": toe_package["name"],
            "version": toe_package["version"],
        },
    )
    docker_image_pkg_item = format_root_image_pkg_item(
        primary_key=toe_packages_key,
    )
    await operations.put_item(
        facet=facet,
        item=docker_image_pkg_item,
        table=TABLE,
    )


async def add(*, toe_package: ToePackage) -> None:
    key_structure = TABLE.primary_key
    facet = TABLE.facets["toe_packages_metadata"]
    toe_packages_key = keys.build_key(
        facet=facet,
        values={
            "group_name": toe_package.group_name,
            "root_id": toe_package.root_id,
            "name": toe_package.name,
            "version": toe_package.version,
        },
    )
    gsi_2_key = keys.build_key(
        facet=GSI_2_FACET,
        values={
            "group_name": toe_package.group_name,
            "be_present": str(toe_package.be_present).lower(),
            "vulnerable": str(toe_package.vulnerable).lower(),
        },
    )
    toe_packages_item = format_toe_package_item(
        primary_key=toe_packages_key,
        toe_package=toe_package,
        gsi_2_key=gsi_2_key,
    )
    condition_expression = Attr(key_structure.partition_key).not_exists()
    try:
        await operations.put_item(
            condition_expression=condition_expression,
            facet=facet,
            item=toe_packages_item,
            table=TABLE,
        )
        add_audit_event(
            AuditEvent(
                action="CREATE",
                author="unknown",
                metadata=toe_packages_item,
                object="ToePackage",
                object_id=toe_packages_key.sort_key,
            )
        )
    except ConditionalCheckFailedException as ex:
        raise RepeatedToePackage(toe_package.package_url) from ex
    await collect(
        (
            tuple(
                add_docker_image_package(uri=location.image_ref, toe_package=toe_package)
                for location in toe_package.locations
                if location.image_ref
            )
        ),
    )
