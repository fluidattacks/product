from enum import (
    Enum,
)

import simplejson as json
from boto3.dynamodb.conditions import (
    Attr,
    ConditionBase,
)

from integrates.audit import AuditEvent, add_audit_event
from integrates.class_types.types import (
    Item,
)
from integrates.custom_exceptions import (
    FindingNotFound,
    IndicatorAlreadyUpdated,
)
from integrates.db_model import (
    TABLE,
)
from integrates.db_model import (
    utils as db_model_utils,
)
from integrates.db_model.findings.enums import (
    FindingEvidenceName,
)
from integrates.db_model.findings.types import (
    FindingEvidence,
    FindingEvidences,
    FindingEvidenceToUpdate,
    FindingMetadataToUpdate,
    FindingState,
    FindingUnreliableIndicators,
    FindingUnreliableIndicatorsToUpdate,
    FindingVerification,
)
from integrates.db_model.findings.utils import (
    format_base_verification_item,
    format_evidences_item,
    format_state_item_to_update,
    format_unreliable_indicators_item,
    format_unreliable_indicators_to_update_item,
)
from integrates.db_model.types import (
    SeverityScore,
)
from integrates.db_model.utils import (
    get_as_utc_iso_format,
    serialize,
)
from integrates.dynamodb import (
    keys,
    operations,
)
from integrates.dynamodb.exceptions import (
    ConditionalCheckFailedException,
)


async def update_evidence(
    *,
    current_value: FindingEvidence,
    evidence_name: FindingEvidenceName,
    evidence: FindingEvidenceToUpdate,
    finding_id: str,
    group_name: str,
) -> None:
    metadata_key = keys.build_key(
        facet=TABLE.facets["finding_metadata"],
        values={"group_name": group_name, "id": finding_id},
    )
    attribute = f"evidences.{evidence_name.value}"
    evidence_item: Item = json.loads(json.dumps(evidence, default=serialize))
    condition_expression = Attr(f"{attribute}.modified_date").eq(
        current_value.modified_date.isoformat(),
    )
    metadata = {
        f"{attribute}.{key}": value for key, value in evidence_item.items() if value is not None
    }
    await operations.update_item(
        condition_expression=condition_expression,
        item=metadata,
        key=metadata_key,
        table=TABLE,
    )
    add_audit_event(
        AuditEvent(
            action="UPDATE",
            author=evidence.author_email or current_value.author_email,
            metadata=metadata,
            object="Finding",
            object_id=finding_id,
        )
    )


async def update_metadata(
    *,
    group_name: str,
    finding_id: str,
    metadata: FindingMetadataToUpdate,
) -> None:
    key_structure = TABLE.primary_key
    metadata_key = keys.build_key(
        facet=TABLE.facets["finding_metadata"],
        values={"group_name": group_name, "id": finding_id},
    )
    metadata_item = {
        key: value.value
        if isinstance(value, Enum)
        else value._asdict()
        if isinstance(value, SeverityScore)
        else format_evidences_item(value)
        if isinstance(value, FindingEvidences)
        else value
        for key, value in metadata._asdict().items()
        if value is not None
    }
    if "severity" in metadata_item:
        metadata_item["cvss_version"] = "3.1"
    condition_expression = Attr(key_structure.partition_key).exists()
    await operations.update_item(
        condition_expression=condition_expression,
        item=metadata_item,
        key=metadata_key,
        table=TABLE,
    )
    add_audit_event(
        AuditEvent(
            action="UPDATE",
            author="unknown",
            metadata=metadata_item,
            object="Finding",
            object_id=finding_id,
        )
    )


async def update_state(
    *,
    current_value: FindingState,
    finding_id: str,
    group_name: str,
    state: FindingState,
) -> None:
    key_structure = TABLE.primary_key
    metadata_key = keys.build_key(
        facet=TABLE.facets["finding_metadata"],
        values={"group_name": group_name, "id": finding_id},
    )
    state = state._replace(
        modified_date=db_model_utils.get_datetime_with_offset(
            current_value.modified_date,
            state.modified_date,
        ),
    )
    state_item = format_state_item_to_update(state)
    metadata_item = {"state": state_item}
    try:
        await operations.update_item(
            condition_expression=Attr(key_structure.partition_key).exists()
            & Attr("state.modified_date").eq(get_as_utc_iso_format(current_value.modified_date)),
            item=metadata_item,
            key=metadata_key,
            table=TABLE,
        )
        add_audit_event(
            AuditEvent(
                action="UPDATE",
                author=state.modified_by,
                metadata=metadata_item,
                object="Finding",
                object_id=finding_id,
            )
        )
    except ConditionalCheckFailedException as ex:
        raise FindingNotFound() from ex

    state_key = keys.build_key(
        facet=TABLE.facets["finding_historic_state"],
        values={
            "id": finding_id,
            "iso8601utc": get_as_utc_iso_format(state.modified_date),
        },
    )
    historic_state_item = {
        key_structure.partition_key: state_key.partition_key,
        key_structure.sort_key: state_key.sort_key,
        **state_item,
    }
    await operations.put_item(
        facet=TABLE.facets["finding_historic_state"],
        item=historic_state_item,
        table=TABLE,
    )


async def update_unreliable_indicators(
    *,
    current_value: FindingUnreliableIndicators | None,
    group_name: str,
    finding_id: str,
    indicators: FindingUnreliableIndicatorsToUpdate,
) -> None:
    key_structure = TABLE.primary_key
    metadata_key = keys.build_key(
        facet=TABLE.facets["finding_metadata"],
        values={"group_name": group_name, "id": finding_id},
    )
    unreliable_indicators_item = {
        f"unreliable_indicators.{key}": value
        for key, value in format_unreliable_indicators_to_update_item(indicators).items()
    }
    current_indicators_item = (
        {
            f"unreliable_indicators.{key}": value
            for key, value in format_unreliable_indicators_item(current_value).items()
        }
        if current_value
        else {}
    )

    conditions = (
        Attr(indicator_name).eq(current_indicators_item[indicator_name])
        for indicator_name in unreliable_indicators_item
        if indicator_name in current_indicators_item
    )
    condition_expression: ConditionBase = Attr(key_structure.partition_key).exists()
    for condition in conditions:
        condition_expression &= condition
    try:
        if unreliable_indicators_item:
            await operations.update_item(
                condition_expression=condition_expression,
                item=unreliable_indicators_item,
                key=metadata_key,
                table=TABLE,
            )
    except ConditionalCheckFailedException as ex:
        raise IndicatorAlreadyUpdated() from ex


async def update_verification(
    *,
    current_value: FindingVerification | None,
    group_name: str,
    finding_id: str,
    verification: FindingVerification,
) -> None:
    key_structure = TABLE.primary_key
    metadata_key = keys.build_key(
        facet=TABLE.facets["finding_metadata"],
        values={"group_name": group_name, "id": finding_id},
    )
    verification_item = format_base_verification_item(verification)
    metadata_item = {"verification": verification_item}
    condition_expression: ConditionBase = Attr(key_structure.partition_key).exists()
    if current_value:
        condition_expression &= Attr("verification.modified_date").eq(
            get_as_utc_iso_format(current_value.modified_date),
        )
    try:
        await operations.update_item(
            condition_expression=condition_expression,
            item=metadata_item,
            key=metadata_key,
            table=TABLE,
        )
        add_audit_event(
            AuditEvent(
                action="UPDATE",
                author=verification.modified_by,
                metadata=metadata_item,
                object="Finding",
                object_id=finding_id,
            )
        )
    except ConditionalCheckFailedException as ex:
        raise FindingNotFound() from ex

    verification_key = keys.build_key(
        facet=TABLE.facets["finding_historic_verification"],
        values={
            "id": finding_id,
            "iso8601utc": get_as_utc_iso_format(verification.modified_date),
        },
    )
    historic_verification_item = {
        key_structure.partition_key: verification_key.partition_key,
        key_structure.sort_key: verification_key.sort_key,
        **verification_item,
    }
    await operations.put_item(
        facet=TABLE.facets["finding_historic_verification"],
        item=historic_verification_item,
        table=TABLE,
    )
