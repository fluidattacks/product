from collections.abc import (
    Iterable,
)
from datetime import (
    datetime,
)
from decimal import (
    Decimal,
)
from typing import (
    cast,
)

from integrates.class_types.types import (
    Item,
)
from integrates.db_model.constants import (
    CVSS_V4_DEFAULT,
)
from integrates.db_model.enums import (
    Source,
    StateRemovalJustification,
)
from integrates.db_model.findings.enums import (
    DraftRejectionReason,
    FindingSorts,
    FindingStateStatus,
    FindingStatus,
    FindingVerificationStatus,
)
from integrates.db_model.findings.types import (
    DraftRejection,
    Finding,
    FindingEvidence,
    FindingEvidences,
    FindingState,
    FindingTreatmentSummary,
    FindingUnreliableIndicators,
    FindingUnreliableIndicatorsToUpdate,
    FindingVerification,
    FindingVerificationSummary,
    FindingVulnerabilitiesSummary,
    FindingZeroRiskSummary,
)
from integrates.db_model.items import (
    DraftRejectionItem,
    FindingEvidenceItem,
    FindingEvidencesItem,
    FindingHistoricStateItem,
    FindingHistoricVerificationItem,
    FindingItem,
    FindingStateItem,
    FindingTreatmentSummaryItem,
    FindingUnreliableIndicatorsItem,
    FindingVerificationItem,
    FindingVerificationSummaryItem,
    FindingVulnerabilitiesSummaryItem,
    FindingZeroRiskSummaryItem,
    SeverityScoreItem,
)
from integrates.db_model.roots.enums import (
    RootCriticality,
)
from integrates.db_model.types import (
    SeverityScore,
)
from integrates.db_model.utils import (
    get_as_utc_iso_format,
)
from integrates.dynamodb.types import PrimaryKey


def get_finding_inverted_state_converted(state: str) -> str:
    if state in {"CLOSED", "OPEN"}:
        translation: dict[str, str] = {
            "CLOSED": "SAFE",
            "OPEN": "VULNERABLE",
        }
        return translation[state]
    return state


def filter_non_state_status_findings(
    findings: Iterable[Finding],
    status: set[FindingStateStatus],
) -> tuple[Finding, ...]:
    return tuple(finding for finding in findings if finding.state.status not in status)


def format_evidence(item: FindingEvidenceItem) -> FindingEvidence:
    return FindingEvidence(
        description=item["description"],
        author_email=item.get("author_email", ""),
        is_draft=item.get("is_draft", False),
        modified_date=datetime.fromisoformat(item["modified_date"]),
        url=item["url"],
    )


def format_evidence_item(evidence: FindingEvidence) -> FindingEvidenceItem:
    return {
        "description": evidence.description,
        "author_email": evidence.author_email,
        "is_draft": evidence.is_draft,
        "modified_date": get_as_utc_iso_format(evidence.modified_date),
        "url": evidence.url,
    }


def format_evidences_item(evidences: FindingEvidences) -> FindingEvidencesItem:
    item = {
        field: format_evidence_item(evidence)
        for field, evidence in evidences._asdict().items()
        if evidence is not None
    }
    return cast(FindingEvidencesItem, item)


def format_finding(item: FindingItem) -> Finding:
    state = format_state(item["state"])
    creation = format_state(item["creation"])
    verification = format_optional_verification(item.get("verification"))
    unreliable_indicators = format_unreliable_indicators(item["unreliable_indicators"])
    evidences = FindingEvidences(
        **{
            name: format_evidence(cast(FindingEvidenceItem, evidence))
            for name, evidence in item["evidences"].items()
        },
    )

    min_time_to_remediate: int | None = None
    if "min_time_to_remediate" in item:
        min_time_to_remediate = int(item["min_time_to_remediate"])

    severity_score = SeverityScore(
        base_score=Decimal(item["severity_score"]["base_score"]),
        temporal_score=Decimal(item["severity_score"]["temporal_score"]),
        cvss_v3=item["severity_score"]["cvss_v3"],
        cvssf=Decimal(item["severity_score"]["cvssf"]),
        cvss_v4=item["severity_score"]["cvss_v4"]
        if item["severity_score"].get("cvss_v4")
        else CVSS_V4_DEFAULT,
        threat_score=Decimal(item["severity_score"]["threat_score"])
        if item["severity_score"].get("threat_score")
        else Decimal("0.0"),
        cvssf_v4=Decimal(item["severity_score"]["cvssf_v4"])
        if item["severity_score"].get("cvssf_v4")
        else Decimal("0"),
    )

    return Finding(
        attack_vector_description=item["attack_vector_description"],
        creation=creation,
        description=item["description"],
        evidences=evidences,
        group_name=item["group_name"],
        id=item["id"],
        severity_score=severity_score,
        min_time_to_remediate=min_time_to_remediate,
        sorts=FindingSorts[item["sorts"]],
        recommendation=item["recommendation"],
        requirements=item["requirements"],
        title=item["title"],
        threat=item["threat"],
        state=state,
        unfulfilled_requirements=item["unfulfilled_requirements"],
        unreliable_indicators=unreliable_indicators,
        verification=verification,
    )


def format_state(state_item: FindingStateItem) -> FindingState:
    state = state_item["status"]
    if state in ["APPROVED", "SUBMITTED"]:
        state = "CREATED"

    if state in ["REJECTED"]:
        state = "DELETED"

    return FindingState(
        justification=StateRemovalJustification[state_item["justification"]],
        modified_by=state_item["modified_by"],
        modified_date=datetime.fromisoformat(state_item["modified_date"]),
        rejection=format_rejection(state_item.get("rejection", None)),
        source=Source[state_item["source"]],
        status=FindingStateStatus[state],
    )


def format_state_item(state: FindingState, *, state_key: PrimaryKey) -> FindingHistoricStateItem:
    item: FindingHistoricStateItem = {
        "pk": state_key.partition_key,
        "sk": state_key.sort_key,
        "justification": state.justification.value,
        "modified_by": state.modified_by,
        "modified_date": get_as_utc_iso_format(state.modified_date),
        "source": state.source.value,
        "status": state.status.value,
    }
    if state.rejection is not None:
        item["rejection"] = format_rejection_item(state.rejection)
    return item


def format_state_item_to_update(state: FindingState) -> Item:
    return {
        "justification": state.justification.value,
        "modified_by": state.modified_by,
        "modified_date": get_as_utc_iso_format(state.modified_date),
        "rejection": format_rejection_item(state.rejection)
        if state.rejection is not None
        else None,
        "source": state.source.value,
        "status": state.status.value,
    }


def format_treatment_summary_item(
    treatment_summary: FindingTreatmentSummary,
) -> FindingTreatmentSummaryItem:
    return {
        "accepted": treatment_summary.accepted,
        "accepted_undefined": treatment_summary.accepted_undefined,
        "in_progress": treatment_summary.in_progress,
        "untreated": treatment_summary.untreated,
    }


def format_treatment_summary(
    treatment_summary_item: FindingTreatmentSummaryItem | None,
) -> FindingTreatmentSummary:
    if treatment_summary_item is None:
        return FindingTreatmentSummary()
    return FindingTreatmentSummary(
        accepted=int(treatment_summary_item.get("accepted", 0)),
        accepted_undefined=int(treatment_summary_item.get("accepted_undefined", 0)),
        in_progress=int(treatment_summary_item.get("in_progress", 0)),
        untreated=int(  # type: ignore[call-overload]
            treatment_summary_item.get("new", treatment_summary_item.get("untreated", 0)),
        ),
    )


def format_verification_summary_item(
    verification_summary: FindingVerificationSummary,
) -> FindingVerificationSummaryItem:
    return {
        "requested": verification_summary.requested,
        "on_hold": verification_summary.on_hold,
        "verified": verification_summary.verified,
    }


def format_verification_summary(
    verification_summary_item: FindingVerificationSummaryItem | None,
) -> FindingVerificationSummary:
    if verification_summary_item is None:
        return FindingVerificationSummary(requested=0, on_hold=0, verified=0)

    return FindingVerificationSummary(
        requested=int(verification_summary_item.get("requested", 0)),
        on_hold=int(verification_summary_item.get("on_hold", 0)),
        verified=int(verification_summary_item.get("verified", 0)),
    )


def format_zero_risk_summary_item(
    zero_risk_summary: FindingZeroRiskSummary,
) -> FindingZeroRiskSummaryItem:
    return {
        "confirmed": zero_risk_summary.confirmed,
        "rejected": zero_risk_summary.rejected,
        "requested": zero_risk_summary.requested,
    }


def format_zero_risk_summary(
    zero_risk_summary_item: FindingZeroRiskSummaryItem,
) -> FindingZeroRiskSummary:
    return FindingZeroRiskSummary(
        confirmed=int(zero_risk_summary_item.get("confirmed", 0)),
        rejected=int(zero_risk_summary_item.get("rejected", 0)),
        requested=int(zero_risk_summary_item.get("requested", 0)),
    )


def format_vulnerabilities_summary_item(
    vulnerabilities_summary: FindingVulnerabilitiesSummary,
) -> FindingVulnerabilitiesSummaryItem:
    return {
        "closed": vulnerabilities_summary.closed,
        "open": vulnerabilities_summary.open,
        "submitted": vulnerabilities_summary.submitted,
        "rejected": vulnerabilities_summary.rejected,
        "open_critical": vulnerabilities_summary.open_critical,
        "open_high": vulnerabilities_summary.open_high,
        "open_medium": vulnerabilities_summary.open_medium,
        "open_low": vulnerabilities_summary.open_low,
        "open_critical_v3": vulnerabilities_summary.open_critical_v3,
        "open_high_v3": vulnerabilities_summary.open_high_v3,
        "open_medium_v3": vulnerabilities_summary.open_medium_v3,
        "open_low_v3": vulnerabilities_summary.open_low_v3,
    }


def format_vulnerabilities_summary(
    vulnerabilities_summary: FindingVulnerabilitiesSummaryItem,
) -> FindingVulnerabilitiesSummary:
    return FindingVulnerabilitiesSummary(
        closed=int(vulnerabilities_summary["closed"]),
        open=int(vulnerabilities_summary["open"]),
        submitted=int(vulnerabilities_summary.get("submitted", 0)),
        rejected=int(vulnerabilities_summary.get("rejected", 0)),
        open_critical=int(vulnerabilities_summary["open_critical"]),
        open_high=int(vulnerabilities_summary["open_high"]),
        open_medium=int(vulnerabilities_summary["open_medium"]),
        open_low=int(vulnerabilities_summary["open_low"]),
        open_critical_v3=int(vulnerabilities_summary.get("open_critical_v3", 0)),
        open_high_v3=int(vulnerabilities_summary.get("open_high_v3", 0)),
        open_medium_v3=int(vulnerabilities_summary.get("open_medium_v3", 0)),
        open_low_v3=int(vulnerabilities_summary.get("open_low_v3", 0)),
    )


def format_unreliable_indicators_item(
    indicators: FindingUnreliableIndicators,
) -> FindingUnreliableIndicatorsItem:
    item = {
        "max_open_epss": indicators.max_open_epss,
        "max_open_root_criticality": indicators.max_open_root_criticality,
        "max_open_severity_score": indicators.max_open_severity_score,
        "max_open_severity_score_v4": indicators.max_open_severity_score_v4,
        "rejected_vulnerabilities": indicators.rejected_vulnerabilities,
        "submitted_vulnerabilities": indicators.submitted_vulnerabilities,
        "unreliable_newest_vulnerability_report_date": (
            get_as_utc_iso_format(indicators.unreliable_newest_vulnerability_report_date)
            if indicators.unreliable_newest_vulnerability_report_date
            else None
        ),
        "unreliable_oldest_open_vulnerability_report_date": (
            get_as_utc_iso_format(indicators.unreliable_oldest_open_vulnerability_report_date)
            if indicators.unreliable_oldest_open_vulnerability_report_date
            else None
        ),
        "unreliable_oldest_vulnerability_report_date": (
            get_as_utc_iso_format(indicators.unreliable_oldest_vulnerability_report_date)
            if indicators.unreliable_oldest_vulnerability_report_date
            else None
        ),
        "unreliable_status": indicators.unreliable_status.value,
        "unreliable_total_open_cvssf": indicators.unreliable_total_open_cvssf,
        "total_open_cvssf": indicators.total_open_cvssf,
        "total_open_cvssf_v4": indicators.total_open_cvssf_v4,
        "total_open_priority": indicators.total_open_priority,
        "treatment_summary": format_treatment_summary_item(indicators.treatment_summary),
        "verification_summary": format_verification_summary_item(indicators.verification_summary),
        "unreliable_where": indicators.unreliable_where,
        "unreliable_zero_risk_summary": format_zero_risk_summary_item(
            indicators.unreliable_zero_risk_summary,
        )
        if indicators.unreliable_zero_risk_summary
        else format_zero_risk_summary_item(FindingZeroRiskSummary()),
        "vulnerabilities_summary": format_vulnerabilities_summary_item(
            indicators.vulnerabilities_summary,
        )
        if indicators.vulnerabilities_summary
        else format_vulnerabilities_summary_item(FindingVulnerabilitiesSummary()),
    }

    return cast(
        FindingUnreliableIndicatorsItem,
        {key: value for key, value in item.items() if value is not None},
    )


def format_unreliable_indicators_to_update_item(
    indicators: FindingUnreliableIndicatorsToUpdate,
) -> Item:
    item = {
        "unreliable_newest_vulnerability_report_date": (
            get_as_utc_iso_format(indicators.unreliable_newest_vulnerability_report_date)
            if indicators.unreliable_newest_vulnerability_report_date
            else None
        ),
        "unreliable_oldest_open_vulnerability_report_date": (
            get_as_utc_iso_format(indicators.unreliable_oldest_open_vulnerability_report_date)
            if indicators.unreliable_oldest_open_vulnerability_report_date
            else None
        ),
        "unreliable_oldest_vulnerability_report_date": (
            get_as_utc_iso_format(indicators.unreliable_oldest_vulnerability_report_date)
            if indicators.unreliable_oldest_vulnerability_report_date
            else None
        ),
        "unreliable_status": indicators.unreliable_status,
        "unreliable_total_open_cvssf": indicators.unreliable_total_open_cvssf,
        "unreliable_where": indicators.unreliable_where,
        "unreliable_zero_risk_summary": format_zero_risk_summary_item(
            indicators.unreliable_zero_risk_summary,
        )
        if indicators.unreliable_zero_risk_summary
        else None,
        "submitted_vulnerabilities": indicators.submitted_vulnerabilities,
        "rejected_vulnerabilities": indicators.rejected_vulnerabilities,
        "max_open_epss": indicators.max_open_epss,
        "max_open_root_criticality": indicators.max_open_root_criticality,
        "max_open_severity_score": indicators.max_open_severity_score,
        "max_open_severity_score_v4": indicators.max_open_severity_score_v4,
        "newest_vulnerability_report_date": (
            get_as_utc_iso_format(indicators.newest_vulnerability_report_date)
            if indicators.newest_vulnerability_report_date
            else None
        ),
        "oldest_vulnerability_report_date": (
            get_as_utc_iso_format(indicators.oldest_vulnerability_report_date)
            if indicators.oldest_vulnerability_report_date
            else None
        ),
        "total_open_cvssf": indicators.total_open_cvssf,
        "total_open_cvssf_v4": indicators.total_open_cvssf_v4,
        "total_open_priority": indicators.total_open_priority,
        "treatment_summary": format_treatment_summary_item(indicators.treatment_summary)
        if indicators.treatment_summary
        else None,
        "verification_summary": format_verification_summary_item(indicators.verification_summary)
        if indicators.verification_summary
        else None,
        "vulnerabilities_summary": format_vulnerabilities_summary_item(
            indicators.vulnerabilities_summary,
        )
        if indicators.vulnerabilities_summary
        else None,
    }
    item = {key: value for key, value in item.items() if value is not None}

    if indicators.clean_unreliable_newest_vulnerability_report_date:
        item["unreliable_newest_vulnerability_report_date"] = None
    if indicators.clean_unreliable_oldest_open_vulnerability_report_date:
        item["unreliable_oldest_open_vulnerability_report_date"] = None
    if indicators.clean_unreliable_oldest_vulnerability_report_date:
        item["unreliable_oldest_vulnerability_report_date"] = None

    return item


def format_unreliable_indicators(
    indicators_item: FindingUnreliableIndicatorsItem,
) -> FindingUnreliableIndicators:
    return FindingUnreliableIndicators(
        unreliable_newest_vulnerability_report_date=(
            datetime.fromisoformat(indicators_item["unreliable_newest_vulnerability_report_date"])
            if indicators_item.get("unreliable_newest_vulnerability_report_date")
            else None
        ),
        unreliable_oldest_open_vulnerability_report_date=(
            datetime.fromisoformat(
                indicators_item["unreliable_oldest_open_vulnerability_report_date"],
            )
            if indicators_item.get("unreliable_oldest_open_vulnerability_report_date")
            else None
        ),
        unreliable_oldest_vulnerability_report_date=(
            datetime.fromisoformat(indicators_item["unreliable_oldest_vulnerability_report_date"])
            if indicators_item.get("unreliable_oldest_vulnerability_report_date")
            else None
        ),
        unreliable_status=FindingStatus[
            get_finding_inverted_state_converted(
                str(indicators_item.get("unreliable_status", None)).upper(),
            )
        ],
        unreliable_total_open_cvssf=Decimal(indicators_item.get("unreliable_total_open_cvssf", 0)),
        unreliable_where=indicators_item.get("unreliable_where", ""),
        unreliable_zero_risk_summary=format_zero_risk_summary(
            indicators_item.get(
                "unreliable_zero_risk_summary",
                cast(
                    FindingZeroRiskSummaryItem,
                    FindingZeroRiskSummary()._asdict(),
                ),
            ),
        ),
        submitted_vulnerabilities=int(indicators_item.get("submitted_vulnerabilities", 0)),
        rejected_vulnerabilities=int(indicators_item.get("rejected_vulnerabilities", 0)),
        max_open_epss=int(indicators_item.get("max_open_epss", 0)),
        max_open_root_criticality=RootCriticality(
            indicators_item.get("max_open_root_criticality", RootCriticality.LOW),
        ),
        max_open_severity_score=Decimal(indicators_item.get("max_open_severity_score", 0)),
        max_open_severity_score_v4=Decimal(indicators_item.get("max_open_severity_score_v4", 0)),
        newest_vulnerability_report_date=(
            datetime.fromisoformat(indicators_item["newest_vulnerability_report_date"])
            if indicators_item.get("newest_vulnerability_report_date")
            else None
        ),
        oldest_vulnerability_report_date=(
            datetime.fromisoformat(indicators_item["oldest_vulnerability_report_date"])
            if indicators_item.get("oldest_vulnerability_report_date")
            else None
        ),
        total_open_cvssf=Decimal(indicators_item.get("total_open_cvssf", 0)),
        total_open_cvssf_v4=Decimal(indicators_item.get("total_open_cvssf_v4", 0)),
        total_open_priority=Decimal(indicators_item.get("total_open_priority", 0)),
        treatment_summary=format_treatment_summary(indicators_item.get("treatment_summary", None)),
        verification_summary=format_verification_summary(indicators_item["verification_summary"])
        if indicators_item.get("verification_summary")
        else FindingVerificationSummary(),
        vulnerabilities_summary=format_vulnerabilities_summary(
            indicators_item["vulnerabilities_summary"],
        )
        if indicators_item.get("vulnerabilities_summary")
        else FindingVulnerabilitiesSummary(),
    )


def format_verification(
    verification_item: FindingVerificationItem,
) -> FindingVerification:
    return FindingVerification(
        comment_id=verification_item["comment_id"],
        modified_by=verification_item["modified_by"],
        modified_date=datetime.fromisoformat(verification_item["modified_date"]),
        status=FindingVerificationStatus[verification_item["status"]],
        vulnerability_ids=verification_item["vulnerability_ids"]
        if "vulnerability_ids" in verification_item
        else set(),
    )


def format_base_verification_item(verification: FindingVerification) -> FindingVerificationItem:
    item: FindingVerificationItem = {
        "comment_id": verification.comment_id,
        "modified_by": verification.modified_by,
        "modified_date": get_as_utc_iso_format(verification.modified_date),
        "status": verification.status.value,
    }
    if verification.vulnerability_ids:
        item["vulnerability_ids"] = verification.vulnerability_ids

    return item


def format_verification_item(
    verification: FindingVerification, *, verification_key: PrimaryKey
) -> FindingHistoricVerificationItem:
    return {
        "pk": verification_key.partition_key,
        "sk": verification_key.sort_key,
        **format_base_verification_item(verification),
    }


def format_finding_metadata_item(
    finding: Finding, state_item: FindingHistoricStateItem, metadata_key: PrimaryKey
) -> FindingItem:
    finding_item: FindingItem = {
        "pk": metadata_key.partition_key,
        "sk": metadata_key.sort_key,
        "attack_vector_description": finding.attack_vector_description,
        "creation": state_item,
        "cvss_version": "3.1",
        "description": finding.description,
        "evidences": format_evidences_item(finding.evidences),
        "group_name": finding.group_name,
        "id": finding.id,
        "sorts": finding.sorts.value,
        "state": state_item,
        "recommendation": finding.recommendation,
        "requirements": finding.requirements,
        "title": finding.title,
        "threat": finding.threat,
        "unfulfilled_requirements": finding.unfulfilled_requirements,
        "unreliable_indicators": format_unreliable_indicators_item(
            finding.unreliable_indicators,
        ),
        "severity_score": cast(SeverityScoreItem, finding.severity_score._asdict()),
    }
    if finding.verification:
        finding_item["verification"] = format_base_verification_item(finding.verification)
    if finding.min_time_to_remediate:
        finding_item["min_time_to_remediate"] = finding.min_time_to_remediate

    return finding_item


def format_optional_state(
    state_item: FindingStateItem | None,
) -> FindingState | None:
    state = None
    if state_item is not None:
        state = format_state(state_item)
    return state


def format_optional_verification(
    verification_item: FindingVerificationItem | None,
) -> FindingVerification | None:
    verification = None
    if verification_item is not None:
        verification = format_verification(verification_item)
    return verification


def format_rejection(
    rejection_item: DraftRejectionItem | None,
) -> DraftRejection | None:
    return (
        DraftRejection(
            other=rejection_item["other"],
            reasons={DraftRejectionReason[reason] for reason in rejection_item["reasons"]},
            rejected_by=rejection_item["rejected_by"],
            rejection_date=datetime.fromisoformat(rejection_item["rejection_date"]),
            submitted_by=rejection_item["submitted_by"],
        )
        if rejection_item is not None
        else None
    )


def format_rejection_item(rejection: DraftRejection) -> DraftRejectionItem:
    return {
        "other": rejection.other,
        "reasons": {str(reason.value) for reason in rejection.reasons},
        "rejected_by": rejection.rejected_by,
        "rejection_date": get_as_utc_iso_format(rejection.rejection_date),
        "submitted_by": rejection.submitted_by,
    }
