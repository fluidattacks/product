from boto3.dynamodb.conditions import (
    Attr,
    Key,
)

from integrates.archive.domain import (
    archive_finding,
)
from integrates.audit import AuditEvent, add_audit_event
from integrates.db_model import (
    TABLE,
)
from integrates.db_model.constants import (
    MACHINE_EMAIL,
)
from integrates.db_model.findings.enums import (
    FindingEvidenceName,
)
from integrates.db_model.utils import (
    archive,
)
from integrates.dynamodb import (
    keys,
    operations,
)
from integrates.dynamodb.types import (
    PrimaryKey,
)


async def remove_machine_finding_code(
    *,
    created_by: str,
    finding_code: str,
    group_name: str,
) -> None:
    if created_by == MACHINE_EMAIL:
        primary_key = keys.build_key(
            facet=TABLE.facets["machine_finding_code"],
            values={"group_name": group_name, "finding_code": finding_code},
        )
        await operations.delete_item(key=primary_key, table=TABLE)


async def remove(
    *,
    created_by: str,
    finding_code: str,
    finding_id: str,
    group_name: str,
) -> None:
    primary_key = keys.build_key(
        facet=TABLE.facets["finding_metadata"],
        values={"group_name": group_name, "id": finding_id},
    )
    await remove_machine_finding_code(
        created_by=created_by,
        finding_code=finding_code,
        group_name=group_name,
    )
    response = await operations.query(
        condition_expression=(Key(TABLE.primary_key.partition_key).eq(primary_key.partition_key)),
        facets=(
            TABLE.facets["finding_metadata"],
            TABLE.facets["finding_historic_state"],
            TABLE.facets["finding_historic_verification"],
        ),
        table=TABLE,
    )
    if not response.items:
        return

    await archive(response.items, archive_finding)
    await operations.batch_delete_item(
        keys=tuple(
            PrimaryKey(
                partition_key=item[TABLE.primary_key.partition_key],
                sort_key=item[TABLE.primary_key.sort_key],
            )
            for item in response.items
        ),
        table=TABLE,
    )
    add_audit_event(
        AuditEvent(
            action="DELETE",
            author="unknown",
            metadata={},
            object="Finding",
            object_id=finding_id,
        )
    )


async def remove_evidence(
    *,
    evidence_name: FindingEvidenceName,
    finding_id: str,
    group_name: str,
) -> None:
    metadata_key = keys.build_key(
        facet=TABLE.facets["finding_metadata"],
        values={"group_name": group_name, "id": finding_id},
    )
    attribute = f"evidences.{evidence_name.value}"
    item = {attribute: None}
    await operations.update_item(
        condition_expression=Attr(attribute).exists(),
        item=item,
        key=metadata_key,
        table=TABLE,
    )
    add_audit_event(
        AuditEvent(
            action="UPDATE",
            author="unknown",
            metadata=item,
            object="Finding",
            object_id=finding_id,
        )
    )
