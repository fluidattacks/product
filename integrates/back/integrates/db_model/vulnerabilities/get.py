import datetime
from collections.abc import (
    Iterable,
)
from itertools import (
    chain,
)
from typing import (
    Self,
)

from aiodataloader import (
    DataLoader,
)
from aioextensions import (
    collect,
)
from boto3.dynamodb.conditions import (
    Key,
)
from fluidattacks_core.serializers.snippet import (
    Snippet,
)

from integrates.custom_exceptions import (
    RequiredStateStatus,
)
from integrates.db_model import (
    HISTORIC_TABLE,
    TABLE,
)
from integrates.db_model.historic_items import (
    VulnerabilityStateItem,
    VulnerabilityTreatmentItem,
    VulnerabilityVerificationItem,
    VulnerabilityZeroRiskItem,
)
from integrates.db_model.items import VulnerabilityItem, VulnerabilitySnippetItem
from integrates.db_model.types import (
    Connection,
    Treatment,
)
from integrates.db_model.utils import (
    format_connection,
    format_treatment,
    get_as_utc_iso_format,
)
from integrates.db_model.vulnerabilities.constants import (
    ASSIGNED_INDEX_METADATA,
    EVENT_INDEX_METADATA,
    GROUP_INDEX_METADATA,
    HASH_INDEX_METADATA,
    NEW_ZR_INDEX_METADATA,
    ROOT_INDEX_METADATA,
)
from integrates.db_model.vulnerabilities.enums import (
    VulnerabilityStateStatus,
    VulnerabilityVerificationStatus,
)
from integrates.db_model.vulnerabilities.types import (
    FindingVulnerabilitiesRequest,
    FindingVulnerabilitiesZrRequest,
    GetVulnerabilitySnippetRequest,
    GroupVulnerabilitiesRequest,
    RootVulnerabilitiesRequest,
    VulnerabilitiesConnection,
    Vulnerability,
    VulnerabilityHistoricTreatmentRequest,
    VulnerabilityRequest,
    VulnerabilityState,
    VulnerabilityVerification,
    VulnerabilityZeroRisk,
)
from integrates.db_model.vulnerabilities.utils import (
    filter_non_deleted,
    filter_released_and_non_zero_risk,
    filter_released_and_zero_risk,
    format_snippet,
    format_state,
    format_verification,
    format_vulnerability,
    format_vulnerability_edge,
    format_zero_risk,
)
from integrates.dynamodb import (
    conditions,
    keys,
    operations,
)


async def _get_vulnerability(*, vulnerability_id: str) -> Vulnerability | None:
    primary_key = keys.build_key(
        facet=TABLE.facets["vulnerability_metadata"],
        values={"id": vulnerability_id},
    )

    key_structure = TABLE.primary_key
    response = await operations.DynamoClient[VulnerabilityItem].query(
        condition_expression=(
            Key(key_structure.partition_key).eq(primary_key.partition_key)
            & Key(key_structure.sort_key).begins_with(primary_key.sort_key)
        ),
        facets=(TABLE.facets["vulnerability_metadata"],),
        limit=1,
        table=TABLE,
    )

    if not response.items:
        return None

    return format_vulnerability(response.items[0])


async def _get_historic_state(
    *,
    request: VulnerabilityRequest,
) -> list[VulnerabilityState]:
    primary_key = keys.build_key(
        facet=HISTORIC_TABLE.facets["vulnerability_state"],
        values={
            "id": request.vulnerability_id,
            "finding_id": request.finding_id,
            "state": "state",
        },
    )
    key_structure = HISTORIC_TABLE.primary_key
    response = await operations.DynamoClient[VulnerabilityStateItem].query(
        condition_expression=(
            Key(key_structure.partition_key).eq(primary_key.partition_key)
            & Key(key_structure.sort_key).begins_with(primary_key.sort_key)
        ),
        facets=(HISTORIC_TABLE.facets["vulnerability_state"],),
        table=HISTORIC_TABLE,
    )

    return list(map(format_state, response.items))


async def _get_historic_treatment(
    *,
    request: VulnerabilityRequest,
) -> list[Treatment]:
    primary_key = keys.build_key(
        facet=HISTORIC_TABLE.facets["vulnerability_treatment"],
        values={
            "id": request.vulnerability_id,
            "finding_id": request.finding_id,
            "treatment": "treatment",
        },
    )
    key_structure = HISTORIC_TABLE.primary_key
    response = await operations.DynamoClient[VulnerabilityTreatmentItem].query(
        condition_expression=(
            Key(key_structure.partition_key).eq(primary_key.partition_key)
            & Key(key_structure.sort_key).begins_with(primary_key.sort_key)
        ),
        facets=(HISTORIC_TABLE.facets["vulnerability_treatment"],),
        table=HISTORIC_TABLE,
    )

    return list(map(format_treatment, response.items))


async def _get_historic_treatment_c(
    *,
    request: VulnerabilityHistoricTreatmentRequest,
) -> Connection[Treatment]:
    primary_key = keys.build_key(
        facet=HISTORIC_TABLE.facets["vulnerability_treatment"],
        values={
            "id": request.vulnerability_id,
            "finding_id": request.finding_id,
            "treatment": "treatment",
        },
    )
    key_structure = HISTORIC_TABLE.primary_key
    response = await operations.DynamoClient[VulnerabilityTreatmentItem].query(
        after=request.after,
        condition_expression=(
            Key(key_structure.partition_key).eq(primary_key.partition_key)
            & Key(key_structure.sort_key).begins_with(primary_key.sort_key)
        ),
        facets=(HISTORIC_TABLE.facets["vulnerability_treatment"],),
        limit=request.first,
        paginate=request.paginate,
        table=HISTORIC_TABLE,
    )

    return format_connection(index=None, formatter=format_treatment, response=response, table=TABLE)


async def _get_historic_verification(
    *,
    request: VulnerabilityRequest,
) -> list[VulnerabilityVerification]:
    primary_key = keys.build_key(
        facet=HISTORIC_TABLE.facets["vulnerability_verification"],
        values={
            "id": request.vulnerability_id,
            "finding_id": request.finding_id,
            "verification": "verification",
        },
    )
    key_structure = HISTORIC_TABLE.primary_key
    response = await operations.DynamoClient[VulnerabilityVerificationItem].query(
        condition_expression=(
            Key(key_structure.partition_key).eq(primary_key.partition_key)
            & Key(key_structure.sort_key).begins_with(primary_key.sort_key)
        ),
        facets=(HISTORIC_TABLE.facets["vulnerability_verification"],),
        table=HISTORIC_TABLE,
    )

    return list(map(format_verification, response.items))


async def _get_historic_zero_risk(
    *,
    request: VulnerabilityRequest,
) -> list[VulnerabilityZeroRisk]:
    primary_key = keys.build_key(
        facet=HISTORIC_TABLE.facets["vulnerability_zero_risk"],
        values={
            "id": request.vulnerability_id,
            "finding_id": request.finding_id,
            "zero_risk": "zero_risk",
        },
    )
    key_structure = HISTORIC_TABLE.primary_key
    response = await operations.DynamoClient[VulnerabilityZeroRiskItem].query(
        condition_expression=(
            Key(key_structure.partition_key).eq(primary_key.partition_key)
            & Key(key_structure.sort_key).begins_with(primary_key.sort_key)
        ),
        facets=(HISTORIC_TABLE.facets["vulnerability_zero_risk"],),
        table=HISTORIC_TABLE,
    )

    return list(map(format_zero_risk, response.items))


async def _get_finding_vulnerabilities(*, finding_id: str) -> list[Vulnerability]:
    primary_key = keys.build_key(
        facet=TABLE.facets["vulnerability_metadata"],
        values={"finding_id": finding_id},
    )

    index = TABLE.indexes["inverted_index"]
    key_structure = index.primary_key
    response = await operations.DynamoClient[VulnerabilityItem].query(
        condition_expression=(
            Key(key_structure.partition_key).eq(primary_key.sort_key)
            & Key(key_structure.sort_key).begins_with(primary_key.partition_key)
        ),
        facets=(TABLE.facets["vulnerability_metadata"],),
        table=TABLE,
        index=index,
    )

    return [format_vulnerability(item) for item in response.items]


async def _get_vulnerability_by_hash(
    *,
    vulnerability_hash: str,
    root_id: str,
) -> Vulnerability | None:
    primary_key = keys.build_key(
        facet=HASH_INDEX_METADATA,
        values={"hash": vulnerability_hash, "root_id": root_id},
    )

    index = TABLE.indexes["gsi_hash"]
    key_structure = index.primary_key
    response = await operations.DynamoClient[VulnerabilityItem].query(
        condition_expression=(
            Key(key_structure.partition_key).eq(primary_key.partition_key)
            & Key(key_structure.sort_key).eq(primary_key.sort_key)
        ),
        facets=(HASH_INDEX_METADATA,),
        table=TABLE,
        index=index,
    )

    if not response.items:
        return None

    return format_vulnerability(response.items[0])


async def _get_finding_vulnerabilities_released_zr(
    is_released: bool,
    is_zero_risk: bool,
    request: FindingVulnerabilitiesZrRequest,
) -> VulnerabilitiesConnection:
    gsi_6_index = TABLE.indexes["gsi_6"]
    key_values = {
        "finding_id": request.finding_id,
        "is_deleted": "false",
        "is_released": str(is_released).lower(),
        "is_zero_risk": str(is_zero_risk).lower(),
    }
    if isinstance(request.state_status, VulnerabilityStateStatus):
        key_values["state_status"] = str(request.state_status.value).lower()
    if isinstance(request.verification_status, VulnerabilityVerificationStatus):
        if request.state_status is None:
            raise RequiredStateStatus()
        key_values["verification_status"] = str(request.verification_status.value).lower()
    primary_key = keys.build_key(
        facet=NEW_ZR_INDEX_METADATA,
        values=key_values,
    )

    key_structure = gsi_6_index.primary_key
    sort_key = (
        primary_key.sort_key
        if isinstance(request.verification_status, VulnerabilityVerificationStatus)
        else primary_key.sort_key.replace("#VERIF", "")
    )
    response = await operations.DynamoClient[VulnerabilityItem].query(
        after=request.after,
        condition_expression=(
            Key(key_structure.partition_key).eq(primary_key.partition_key)
            & Key(key_structure.sort_key).begins_with(sort_key)
        ),
        facets=(TABLE.facets["vulnerability_metadata"],),
        filter_expression=conditions.get_filter_expression(request.filters),
        index=gsi_6_index,
        limit=request.first,
        paginate=request.paginate,
        table=TABLE,
    )

    return VulnerabilitiesConnection(
        edges=tuple(format_vulnerability_edge(gsi_6_index, item, TABLE) for item in response.items),
        page_info=response.page_info,
    )


async def _get_root_vulnerabilities(*, root_id: str) -> list[Vulnerability]:
    primary_key = keys.build_key(
        facet=ROOT_INDEX_METADATA,
        values={"root_id": root_id},
    )

    index = TABLE.indexes["gsi_2"]
    key_structure = index.primary_key
    response = await operations.DynamoClient[VulnerabilityItem].query(
        condition_expression=(
            Key(key_structure.partition_key).eq(primary_key.partition_key)
            & Key(key_structure.sort_key).begins_with(primary_key.sort_key)
        ),
        facets=(ROOT_INDEX_METADATA,),
        table=TABLE,
        index=index,
    )

    return [format_vulnerability(item) for item in response.items]


async def _get_root_vulnerabilities_connection(
    *,
    request: RootVulnerabilitiesRequest,
) -> VulnerabilitiesConnection:
    primary_key = keys.build_key(
        facet=ROOT_INDEX_METADATA,
        values={"root_id": request.root_id},
    )

    index = TABLE.indexes["gsi_2"]
    key_structure = index.primary_key
    response = await operations.DynamoClient[VulnerabilityItem].query(
        after=request.after,
        condition_expression=(
            Key(key_structure.partition_key).eq(primary_key.partition_key)
            & Key(key_structure.sort_key).begins_with(primary_key.sort_key)
        ),
        facets=(ROOT_INDEX_METADATA,),
        limit=request.first,
        paginate=request.paginate,
        table=TABLE,
        index=index,
    )

    return VulnerabilitiesConnection(
        edges=tuple(format_vulnerability_edge(index, item, TABLE) for item in response.items),
        page_info=response.page_info,
    )


async def _get_assigned_vulnerabilities(*, email: str) -> list[Vulnerability]:
    primary_key = keys.build_key(
        facet=ASSIGNED_INDEX_METADATA,
        values={"email": email},
    )

    index = TABLE.indexes["gsi_3"]
    key_structure = index.primary_key
    response = await operations.DynamoClient[VulnerabilityItem].query(
        condition_expression=(
            Key(key_structure.partition_key).eq(primary_key.partition_key)
            & Key(key_structure.sort_key).begins_with(primary_key.sort_key)
        ),
        facets=(ASSIGNED_INDEX_METADATA,),
        table=TABLE,
        index=index,
    )

    return [format_vulnerability(item) for item in response.items]


async def _get_affected_reattacks(*, event_id: str) -> list[Vulnerability]:
    primary_key = keys.build_key(
        facet=EVENT_INDEX_METADATA,
        values={"event_id": event_id},
    )

    index = TABLE.indexes["gsi_4"]
    key_structure = index.primary_key
    response = await operations.DynamoClient[VulnerabilityItem].query(
        condition_expression=(
            Key(key_structure.partition_key).eq(primary_key.partition_key)
            & Key(key_structure.sort_key).begins_with(primary_key.sort_key)
        ),
        facets=(EVENT_INDEX_METADATA,),
        table=TABLE,
        index=index,
    )

    return [format_vulnerability(item) for item in response.items]


async def _get_group_vulnerabilities(
    *,
    request: GroupVulnerabilitiesRequest,
) -> VulnerabilitiesConnection:
    key_values = {
        "group_name": request.group_name,
        "is_zero_risk": "false",
    }
    if isinstance(request.state_status, VulnerabilityStateStatus):
        key_values["state_status"] = str(request.state_status.value).lower()
    if request.is_accepted is not None:
        if request.state_status is None:
            raise RequiredStateStatus()
        key_values["is_accepted"] = str(request.is_accepted).lower()
    primary_key = keys.build_key(
        facet=GROUP_INDEX_METADATA,
        values=key_values,
    )
    group_index = TABLE.indexes["gsi_5"]
    key_structure = group_index.primary_key
    sort_key = (
        primary_key.sort_key.replace("#TREAT", "")
        if request.is_accepted is None
        else primary_key.sort_key
    )
    condition_expression = Key(key_structure.partition_key).eq(primary_key.partition_key) & Key(
        key_structure.sort_key,
    ).begins_with(sort_key)
    response = await operations.DynamoClient[VulnerabilityItem].query(
        after=request.after,
        condition_expression=condition_expression,
        facets=(TABLE.facets["vulnerability_metadata"],),
        index=group_index,
        limit=request.first,
        paginate=request.paginate,
        table=TABLE,
    )

    return VulnerabilitiesConnection(
        edges=tuple(format_vulnerability_edge(group_index, item, TABLE) for item in response.items),
        page_info=response.page_info,
    )


async def get_vulnerability_snippet(
    requests: Iterable[GetVulnerabilitySnippetRequest],
) -> tuple[Snippet | None, ...]:
    keys_ = tuple(
        keys.build_key(
            facet=TABLE.facets["vulnerability_snippet"],
            values={
                "id": request.vulnerability_id,
                "iso8601utc": get_as_utc_iso_format(request.modified_date),
            },
        )
        for request in requests
    )
    response: tuple[VulnerabilitySnippetItem, ...] = await operations.DynamoClient[
        VulnerabilitySnippetItem
    ].batch_get_item(
        keys=keys_,
        table=TABLE,
    )
    return tuple(
        next(
            (
                format_snippet(x)
                for x in response
                if req.vulnerability_id in x["pk"]
                and get_as_utc_iso_format(req.modified_date) in x["sk"]
            ),
            None,
        )
        for req in requests
    )


class VulnerabilityLoader(DataLoader[str, Vulnerability | None]):
    async def batch_load_fn(self, vulnerability_ids: Iterable[str]) -> list[Vulnerability | None]:
        return list(
            await collect(
                tuple(
                    _get_vulnerability(vulnerability_id=vulnerability_id)
                    for vulnerability_id in vulnerability_ids
                ),
                workers=32,
            ),
        )


class VulnerabilityHashLoader(DataLoader[tuple[str, str], Vulnerability | None]):
    async def batch_load_fn(
        self,
        searched_vulns: Iterable[tuple[str, str]],
    ) -> list[Vulnerability | None]:
        return list(
            await collect(
                tuple(
                    _get_vulnerability_by_hash(
                        vulnerability_hash=vulnerability_hash,
                        root_id=root_id,
                    )
                    for vulnerability_hash, root_id in searched_vulns
                ),
                workers=32,
            ),
        )


class AssignedVulnerabilitiesLoader(DataLoader[str, list[Vulnerability]]):
    async def batch_load_fn(self, emails: Iterable[str]) -> list[list[Vulnerability]]:
        return list(
            await collect(
                tuple(_get_assigned_vulnerabilities(email=email) for email in emails),
                workers=32,
            ),
        )


class FindingVulnerabilitiesLoader(DataLoader[str, list[Vulnerability]]):
    def __init__(self, dataloader: VulnerabilityLoader) -> None:
        super().__init__()
        self.dataloader = dataloader

    def clear(self, key: str) -> Self:  # type: ignore
        self.dataloader.clear(key)
        return super().clear(key)

    async def load_many_chained(self, finding_ids: Iterable[str]) -> list[Vulnerability]:
        unchained_data = await self.load_many(finding_ids)
        return list(chain.from_iterable(unchained_data))

    async def batch_load_fn(self, finding_ids: Iterable[str]) -> list[list[Vulnerability]]:
        vulns = list(
            await collect(
                tuple(
                    _get_finding_vulnerabilities(finding_id=finding_id)
                    for finding_id in finding_ids
                ),
                workers=32,
            ),
        )
        for finding_vulns in vulns:
            for vuln in finding_vulns:
                self.dataloader.prime(vuln.id, vuln)
        return vulns


class FindingVulnerabilitiesDraftConnectionLoader(
    DataLoader[FindingVulnerabilitiesRequest, VulnerabilitiesConnection],
):
    async def batch_load_fn(
        self,
        requests: Iterable[FindingVulnerabilitiesRequest],
    ) -> list[VulnerabilitiesConnection]:
        return list(
            await collect(
                tuple(
                    _get_finding_vulnerabilities_released_zr(
                        is_released=False,
                        is_zero_risk=False,
                        request=FindingVulnerabilitiesZrRequest(
                            finding_id=request.finding_id,
                            after=request.after,
                            first=request.first,
                            paginate=request.paginate,
                            state_status=request.state_status,
                        ),
                    )
                    for request in requests
                ),
                workers=32,
            ),
        )

    async def load_nodes(self, request: FindingVulnerabilitiesRequest) -> list[Vulnerability]:
        connection = await self.load(request)
        return [edge.node for edge in connection.edges]


class FindingVulnerabilitiesNonDeletedLoader(DataLoader[str, list[Vulnerability]]):
    def __init__(self, dataloader: FindingVulnerabilitiesLoader) -> None:
        super().__init__()
        self.dataloader = dataloader

    def clear(self, key: str) -> Self:  # type: ignore
        self.dataloader.clear(key)
        return super().clear(key)

    async def load_many_chained(self, finding_ids: Iterable[str]) -> list[Vulnerability]:
        unchained_data = await self.load_many(finding_ids)
        return list(chain.from_iterable(unchained_data))

    async def batch_load_fn(self, finding_ids: Iterable[str]) -> list[list[Vulnerability]]:
        findings_vulns = await self.dataloader.load_many(finding_ids)
        return [filter_non_deleted(finding_vulns) for finding_vulns in findings_vulns]


class FindingVulnerabilitiesReleasedNonZeroRiskLoader(DataLoader[str, list[Vulnerability]]):
    def __init__(self, dataloader: FindingVulnerabilitiesNonDeletedLoader) -> None:
        super().__init__()
        self.dataloader = dataloader

    async def load_many_chained(self, finding_ids: Iterable[str]) -> list[Vulnerability]:
        unchained_data = await self.load_many(finding_ids)
        return list(chain.from_iterable(unchained_data))

    async def batch_load_fn(self, finding_ids: Iterable[str]) -> list[list[Vulnerability]]:
        findings_vulns = await self.dataloader.load_many(finding_ids)
        return [
            filter_released_and_non_zero_risk(finding_vulns) for finding_vulns in findings_vulns
        ]


class FindingVulnerabilitiesReleasedNonZeroRiskConnectionLoader(
    DataLoader[FindingVulnerabilitiesZrRequest, VulnerabilitiesConnection],
):
    async def batch_load_fn(
        self,
        requests: Iterable[FindingVulnerabilitiesZrRequest],
    ) -> list[VulnerabilitiesConnection]:
        return list(
            await collect(
                tuple(
                    _get_finding_vulnerabilities_released_zr(
                        is_released=True,
                        is_zero_risk=False,
                        request=request,
                    )
                    for request in requests
                ),
                workers=32,
            ),
        )


class FindingVulnerabilitiesReleasedZeroRiskLoader(DataLoader[str, list[Vulnerability]]):
    def __init__(self, dataloader: FindingVulnerabilitiesNonDeletedLoader) -> None:
        super().__init__()
        self.dataloader = dataloader

    async def batch_load_fn(self, finding_ids: Iterable[str]) -> list[list[Vulnerability]]:
        findings_vulns = await self.dataloader.load_many(finding_ids)
        return [filter_released_and_zero_risk(finding_vulns) for finding_vulns in findings_vulns]


class FindingVulnerabilitiesReleasedZeroRiskConnectionLoader(
    DataLoader[FindingVulnerabilitiesZrRequest, VulnerabilitiesConnection],
):
    async def batch_load_fn(
        self,
        requests: Iterable[FindingVulnerabilitiesZrRequest],
    ) -> list[VulnerabilitiesConnection]:
        return list(
            await collect(
                tuple(
                    _get_finding_vulnerabilities_released_zr(
                        is_released=True,
                        is_zero_risk=True,
                        request=request,
                    )
                    for request in requests
                ),
                workers=32,
            ),
        )


class FindingVulnerabilitiesToReattackConnectionLoader(
    DataLoader[FindingVulnerabilitiesRequest, VulnerabilitiesConnection],
):
    async def batch_load_fn(
        self,
        requests: Iterable[FindingVulnerabilitiesRequest],
    ) -> list[VulnerabilitiesConnection]:
        return list(
            await collect(
                tuple(
                    _get_finding_vulnerabilities_released_zr(
                        is_released=True,
                        is_zero_risk=False,
                        request=FindingVulnerabilitiesZrRequest(
                            finding_id=request.finding_id,
                            after=request.after,
                            first=request.first,
                            paginate=request.paginate,
                            state_status=VulnerabilityStateStatus.VULNERABLE,
                            verification_status=(VulnerabilityVerificationStatus.REQUESTED),
                        ),
                    )
                    for request in requests
                ),
                workers=32,
            ),
        )


class RootVulnerabilitiesLoader(DataLoader[str, list[Vulnerability]]):
    async def load_many_chained(self, root_ids: Iterable[str]) -> list[Vulnerability]:
        unchained_data = await self.load_many(root_ids)
        return list(chain.from_iterable(unchained_data))

    async def batch_load_fn(self, root_ids: Iterable[str]) -> list[list[Vulnerability]]:
        return list(
            await collect(
                tuple(_get_root_vulnerabilities(root_id=root_id) for root_id in root_ids),
                workers=32,
            ),
        )


class RootVulnerabilitiesConnectionLoader(
    DataLoader[RootVulnerabilitiesRequest, VulnerabilitiesConnection],
):
    async def batch_load_fn(
        self,
        requests: Iterable[RootVulnerabilitiesRequest],
    ) -> list[VulnerabilitiesConnection]:
        return list(
            await collect(
                tuple(
                    _get_root_vulnerabilities_connection(request=request) for request in requests
                ),
                workers=32,
            ),
        )


class EventVulnerabilitiesLoader(DataLoader[str, list[Vulnerability]]):
    async def batch_load_fn(self, event_ids: Iterable[str]) -> list[list[Vulnerability]]:
        return list(
            await collect(
                tuple(_get_affected_reattacks(event_id=event_id) for event_id in event_ids),
                workers=32,
            ),
        )


class VulnerabilityHistoricStateLoader(DataLoader[VulnerabilityRequest, list[VulnerabilityState]]):
    async def load_many_chained(
        self,
        requests: Iterable[VulnerabilityRequest],
    ) -> list[VulnerabilityState]:
        unchained_data = await self.load_many(requests)
        return list(chain.from_iterable(unchained_data))

    async def batch_load_fn(
        self,
        requests: Iterable[VulnerabilityRequest],
    ) -> list[list[VulnerabilityState]]:
        return list(
            await collect(
                tuple(_get_historic_state(request=request) for request in requests),
                workers=32,
            ),
        )


class VulnerabilityHistoricTreatmentLoader(
    DataLoader[VulnerabilityRequest, list[Treatment]],
):
    async def batch_load_fn(
        self,
        requests: Iterable[VulnerabilityRequest],
    ) -> list[list[Treatment]]:
        return list(
            await collect(
                tuple(_get_historic_treatment(request=request) for request in requests),
                workers=32,
            ),
        )


class VulnerabilityHistoricTreatmentConnectionLoader(
    DataLoader[
        VulnerabilityHistoricTreatmentRequest,
        Connection[Treatment],
    ],
):
    async def batch_load_fn(
        self,
        requests: Iterable[VulnerabilityHistoricTreatmentRequest],
    ) -> list[Connection[Treatment]]:
        return list(
            await collect(
                tuple(_get_historic_treatment_c(request=request) for request in requests),
                workers=32,
            ),
        )


class VulnerabilityHistoricVerificationLoader(
    DataLoader[VulnerabilityRequest, list[VulnerabilityVerification]],
):
    async def batch_load_fn(
        self,
        requests: Iterable[VulnerabilityRequest],
    ) -> list[list[VulnerabilityVerification]]:
        return list(
            await collect(
                tuple(_get_historic_verification(request=request) for request in requests),
                workers=32,
            ),
        )


class VulnerabilityHistoricZeroRiskLoader(
    DataLoader[VulnerabilityRequest, list[VulnerabilityZeroRisk]],
):
    async def batch_load_fn(
        self,
        requests: Iterable[VulnerabilityRequest],
    ) -> list[list[VulnerabilityZeroRisk]]:
        return list(
            await collect(
                tuple(_get_historic_zero_risk(request=request) for request in requests),
                workers=32,
            ),
        )


class GroupVulnerabilitiesLoader(
    DataLoader[GroupVulnerabilitiesRequest, VulnerabilitiesConnection],
):
    async def batch_load_fn(
        self,
        requests: Iterable[GroupVulnerabilitiesRequest],
    ) -> list[VulnerabilitiesConnection]:
        return list(
            await collect(
                tuple(_get_group_vulnerabilities(request=request) for request in requests),
                workers=32,
            ),
        )


class VulnerabilitySnippetLoader(
    DataLoader[tuple[str, datetime.datetime], Snippet | None],
):
    async def batch_load_fn(
        self,
        requests: Iterable[tuple[str, datetime.datetime]],
    ) -> list[Snippet | None]:
        result = await get_vulnerability_snippet(
            tuple(
                GetVulnerabilitySnippetRequest(vulnerability_id, modified_date)
                for vulnerability_id, modified_date in requests
            )
        )
        return list(result)
