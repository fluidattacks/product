from enum import Enum


class VulnerabilityToolImpact(str, Enum):
    DIRECT: str = "DIRECT"
    INDIRECT: str = "INDIRECT"


class VulnerabilityType(str, Enum):
    INPUTS: str = "INPUTS"
    LINES: str = "LINES"
    PORTS: str = "PORTS"


class VulnerabilityTechnique(str, Enum):
    CLOUD: str = "CLOUD"
    CSPM: str = "CSPM"
    DAST: str = "DAST"
    MPT: str = "MPT"
    PTAAS: str = "PTAAS"
    RE: str = "RE"
    SAST: str = "SAST"
    SCA: str = "SCA"
    SCR: str = "SCR"


class VulnerabilityStateReason(str, Enum):
    ANALYST_REPORT: str = "ANALYST_REPORT"
    CLOSED_BY_MACHINE: str = "CLOSED_BY_MACHINE"
    CONSISTENCY: str = "CONSISTENCY"
    DUPLICATED: str = "DUPLICATED"
    ENVIRONMENT_DELETED: str = "ENVIRONMENT_DELETED"
    EVIDENCE: str = "EVIDENCE"
    EXCLUSION: str = "EXCLUSION"
    FALSE_POSITIVE: str = "FALSE_POSITIVE"
    FUNCTIONALITY_NO_LONGER_EXISTS: str = "FUNCTIONALITY_NO_LONGER_EXISTS"
    MODIFIED_BY_MACHINE: str = "MODIFIED_BY_MACHINE"
    MOVED_TO_ANOTHER_GROUP: str = "MOVED_TO_ANOTHER_GROUP"
    NAMING: str = "NAMING"
    NOT_REQUIRED: str = "NOT_REQUIRED"
    NO_JUSTIFICATION: str = "NO_JUSTIFICATION"
    OMISSION: str = "OMISSION"
    OTHER: str = "OTHER"
    REPORTING_ERROR: str = "REPORTING_ERROR"
    ROOT_MOVED_TO_ANOTHER_GROUP: str = "ROOT_MOVED_TO_ANOTHER_GROUP"
    ROOT_OR_ENVIRONMENT_DEACTIVATED: str = "ROOT_OR_ENVIRONMENT_DEACTIVATED"
    SCORING: str = "SCORING"
    SUBMITTED_FROM_FILE: str = "SUBMITTED_FROM_FILE"
    VERIFIED_AS_SAFE: str = "VERIFIED_AS_SAFE"
    VERIFIED_AS_VULNERABLE: str = "VERIFIED_AS_VULNERABLE"
    WRITING: str = "WRITING"
    ZERO_RISK_REQUESTED: str = "ZERO_RISK_REQUESTED"


class VulnerabilityStateStatus(str, Enum):
    SAFE: str = "SAFE"
    DELETED: str = "DELETED"
    MASKED: str = "MASKED"
    VULNERABLE: str = "VULNERABLE"
    REJECTED: str = "REJECTED"
    SUBMITTED: str = "SUBMITTED"


class VulnerabilityVerificationStatus(str, Enum):
    MASKED: str = "MASKED"
    NOT_REQUESTED: str = "NOT_REQUESTED"
    REQUESTED: str = "REQUESTED"
    ON_HOLD: str = "ON_HOLD"
    VERIFIED: str = "VERIFIED"


class VulnerabilityZeroRiskStatus(str, Enum):
    CONFIRMED: str = "CONFIRMED"
    REJECTED: str = "REJECTED"
    REQUESTED: str = "REQUESTED"


class VulnerabilitySeverityProposalStatus(str, Enum):
    REQUESTED: str = "REQUESTED"
    APPROVED: str = "APPROVED"
    REJECTED: str = "REJECTED"
