from boto3.dynamodb.conditions import (
    Key,
)

from integrates.archive.domain import (
    archive_vulnerability,
)
from integrates.audit import AuditEvent, add_audit_event
from integrates.db_model import (
    TABLE,
)
from integrates.db_model.utils import (
    archive,
)
from integrates.dynamodb import (
    keys,
    operations,
)
from integrates.dynamodb.types import (
    PrimaryKey,
)


async def remove(
    *,
    vulnerability_id: str,
    should_archive: bool = True,
) -> None:
    primary_key = keys.build_key(
        facet=TABLE.facets["vulnerability_metadata"],
        values={"id": vulnerability_id},
    )
    key_structure = TABLE.primary_key
    response = await operations.query(
        condition_expression=(Key(key_structure.partition_key).eq(primary_key.partition_key)),
        facets=(
            TABLE.facets["vulnerability_historic_state"],
            TABLE.facets["vulnerability_historic_treatment"],
            TABLE.facets["vulnerability_historic_verification"],
            TABLE.facets["vulnerability_historic_zero_risk"],
            TABLE.facets["vulnerability_metadata"],
        ),
        table=TABLE,
    )
    if not response.items:
        return

    if should_archive:
        await archive(response.items, archive_vulnerability)
    await operations.batch_delete_item(
        keys=tuple(
            PrimaryKey(
                partition_key=item[key_structure.partition_key],
                sort_key=item[key_structure.sort_key],
            )
            for item in response.items
        ),
        table=TABLE,
    )
    add_audit_event(
        AuditEvent(
            action="DELETE",
            author="unknown",
            metadata={},
            object="Vulnerability",
            object_id=vulnerability_id,
        )
    )
