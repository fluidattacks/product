from datetime import (
    datetime,
)
from decimal import (
    Decimal,
)
from typing import (
    NamedTuple,
    TypeAlias,
    TypeVar,
)

from integrates.db_model.enums import (
    Source,
)
from integrates.db_model.types import (
    SeverityScore,
    Treatment,
)
from integrates.db_model.vulnerabilities.enums import (
    VulnerabilitySeverityProposalStatus,
    VulnerabilityStateReason,
    VulnerabilityStateStatus,
    VulnerabilityTechnique,
    VulnerabilityToolImpact,
    VulnerabilityType,
    VulnerabilityVerificationStatus,
    VulnerabilityZeroRiskStatus,
)
from integrates.dynamodb.types import (
    FilterExpression,
    PageInfo,
)


class VulnerabilityTool(NamedTuple):
    name: str
    impact: VulnerabilityToolImpact


class VulnerabilityAdvisory(NamedTuple):
    cve: list[str]
    package: str
    vulnerable_version: str
    epss: int | None = None


class VulnerabilityAuthor(NamedTuple):
    author_email: str
    commit: str
    commit_date: datetime


class VulnerabilitySeverityProposal(NamedTuple):
    severity_score: SeverityScore
    modified_by: str
    modified_date: datetime
    status: VulnerabilitySeverityProposalStatus
    justification: str | None = None


class VulnerabilityState(NamedTuple):
    modified_by: str
    modified_date: datetime
    source: Source
    specific: str
    status: VulnerabilityStateStatus
    where: str
    commit: str | None = None
    advisories: VulnerabilityAdvisory | None = None
    reasons: list[VulnerabilityStateReason] | None = None
    other_reason: str | None = None
    tool: VulnerabilityTool | None = None
    proposed_severity: VulnerabilitySeverityProposal | None = None


class VulnerabilityUnreliableIndicators(NamedTuple):
    unreliable_closing_date: datetime | None = None
    unreliable_source: Source = Source.ASM
    unreliable_efficacy: Decimal | None = None
    unreliable_last_reattack_date: datetime | None = None
    unreliable_last_reattack_requester: str | None = None
    unreliable_last_requested_reattack_date: datetime | None = None
    unreliable_priority: Decimal | None = None
    unreliable_reattack_cycles: int | None = None
    unreliable_report_date: datetime | None = None
    unreliable_treatment_changes: int | None = None


class VulnerabilityVerification(NamedTuple):
    modified_date: datetime
    status: VulnerabilityVerificationStatus
    event_id: str | None = None
    modified_by: str | None = None
    justification: str | None = None


class VulnerabilityZeroRisk(NamedTuple):
    comment_id: str
    modified_by: str
    modified_date: datetime
    status: VulnerabilityZeroRiskStatus


class Vulnerability(NamedTuple):
    created_by: str
    created_date: datetime
    finding_id: str
    group_name: str
    hacker_email: str
    id: str
    organization_name: str
    root_id: str
    state: VulnerabilityState
    type: VulnerabilityType
    author: VulnerabilityAuthor | None = None
    technique: VulnerabilityTechnique | None = None
    bug_tracking_system_url: str | None = None
    custom_severity: int | None = None
    cwe_ids: list[str] | None = None
    developer: str | None = None
    event_id: str | None = None
    hash: int | None = None
    severity_score: SeverityScore | None = None
    skims_method: str | None = None
    skims_description: str | None = None
    stream: list[str] | None = None
    tags: list[str] | None = None
    treatment: Treatment | None = None
    unreliable_indicators: VulnerabilityUnreliableIndicators = VulnerabilityUnreliableIndicators()
    root_id_mismatch: bool = False
    verification: VulnerabilityVerification | None = None
    webhook_url: str | None = None
    zero_risk: VulnerabilityZeroRisk | None = None

    def get_path(self) -> str:
        if self.type.value == VulnerabilityType.INPUTS.value:
            if len(chunks := self.state.where.rsplit(" (", maxsplit=1)) == 2:
                where, _ = chunks
            else:
                where = chunks[0]
        else:
            where = self.state.where
        return where

    def __hash__(self) -> int:
        if self.root_id:
            return hash(
                (
                    self.state.specific,
                    self.type.value,
                    self.get_path(),
                    self.root_id,
                ),
            )
        return hash((self.state.specific, self.type.value, self.get_path()))


class VulnerabilityEdge(NamedTuple):
    node: Vulnerability
    cursor: str


class VulnerabilitiesConnection(NamedTuple):
    edges: tuple[VulnerabilityEdge, ...]
    page_info: PageInfo
    total: int | None = None


class NoUpdate(NamedTuple):
    pass


T = TypeVar("T")
Updatable: TypeAlias = T | NoUpdate


class VulnerabilityMetadataToUpdate(NamedTuple):
    author: Updatable[VulnerabilityAuthor | None] = NoUpdate()
    bug_tracking_system_url: Updatable[str | None] = NoUpdate()
    created_by: Updatable[str] = NoUpdate()
    created_date: Updatable[datetime] = NoUpdate()
    custom_severity: Updatable[int | None] = NoUpdate()
    cwe_ids: Updatable[list[str] | None] = NoUpdate()
    hacker_email: Updatable[str] = NoUpdate()
    hash: Updatable[int | None] = NoUpdate()
    skims_method: Updatable[str | None] = NoUpdate()
    skims_description: Updatable[str | None] = NoUpdate()
    developer: Updatable[str | None] = NoUpdate()
    severity_score: Updatable[SeverityScore | None] = NoUpdate()
    stream: Updatable[list[str] | None] = NoUpdate()
    tags: Updatable[list[str] | None] = NoUpdate()
    type: Updatable[VulnerabilityType] = NoUpdate()
    technique: Updatable[VulnerabilityTechnique | None] = NoUpdate()
    webhook_url: Updatable[str | None] = NoUpdate()


VulnerabilityHistoric = (
    tuple[VulnerabilityState, ...]
    | tuple[Treatment, ...]
    | tuple[VulnerabilityVerification, ...]
    | tuple[VulnerabilityZeroRisk, ...]
)

VulnerabilityHistoricEntry = (
    VulnerabilityState | Treatment | VulnerabilityVerification | VulnerabilityZeroRisk
)


class VulnerabilityUnreliableIndicatorsToUpdate(NamedTuple):
    unreliable_closing_date: datetime | None = None
    unreliable_efficacy: Decimal | None = None
    unreliable_last_reattack_date: datetime | None = None
    unreliable_last_reattack_requester: str | None = None
    unreliable_last_requested_reattack_date: datetime | None = None
    unreliable_priority: Decimal | None = None
    unreliable_reattack_cycles: int | None = None
    unreliable_report_date: datetime | None = None
    unreliable_treatment_changes: int | None = None
    clean_unreliable_closing_date: bool = False
    clean_unreliable_last_reattack_date: bool = False
    clean_unreliable_last_requested_reattack_date: bool = False
    clean_unreliable_report_date: bool = False


class FindingVulnerabilitiesZrRequest(NamedTuple):
    finding_id: str
    after: str | None = None
    filters: FilterExpression | None = None
    first: int | None = None
    paginate: bool = False
    state_status: VulnerabilityStateStatus | None = None
    verification_status: VulnerabilityVerificationStatus | None = None


class GroupVulnerabilitiesRequest(NamedTuple):
    group_name: str
    state_status: VulnerabilityStateStatus | None = None
    after: str | None = None
    first: int | None = None
    is_accepted: bool | None = None
    paginate: bool = False


class RootVulnerabilitiesRequest(NamedTuple):
    root_id: str
    after: str | None = None
    first: int | None = None
    paginate: bool = False


class FindingVulnerabilitiesRequest(NamedTuple):
    finding_id: str
    after: str | None = None
    first: int | None = None
    paginate: bool = False
    state_status: VulnerabilityStateStatus | None = None


class VulnerabilityHistoricTreatmentRequest(NamedTuple):
    finding_id: str
    vulnerability_id: str
    after: str | None = None
    first: int | None = None
    paginate: bool = False


class VulnerabilityRequest(NamedTuple):
    finding_id: str
    vulnerability_id: str


class GetVulnerabilitySnippetRequest(NamedTuple):
    vulnerability_id: str
    modified_date: datetime
