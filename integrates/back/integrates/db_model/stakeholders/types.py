from datetime import (
    datetime,
)
from decimal import (
    Decimal,
)
from enum import (
    Enum,
)
from typing import (
    NamedTuple,
)


class NotificationsParameters(NamedTuple):
    min_severity: Decimal = Decimal("3.0")


class NotificationsPreferences(NamedTuple):
    available: list[str] = []
    email: list[str] = []
    sms: list[str] = []
    parameters: NotificationsParameters = NotificationsParameters()


class TrustedDevice(NamedTuple):
    ip_address: str = ""
    device: str = ""
    location: str = ""
    browser: str = ""
    otp_token_jti: str | None = None
    last_attempt: datetime | None = None
    first_login_date: datetime | None = None
    last_login_date: datetime | None = None


class StateSessionType(str, Enum):
    IS_VALID: str = "IS_VALID"
    REVOKED: str = "REVOKED"


class StakeholderSessionToken(NamedTuple):
    jti: str
    state: StateSessionType


class StakeholderPhone(NamedTuple):
    country_code: str
    calling_country_code: str
    national_number: str


class StakeholderTours(NamedTuple):
    new_group: bool = False
    new_root: bool = False
    welcome: bool = False


class StakeholderState(NamedTuple):
    modified_by: str | None
    modified_date: datetime | None
    notifications_preferences: NotificationsPreferences = NotificationsPreferences()
    trusted_devices: list[TrustedDevice] = []


class StakeholderLogin(NamedTuple):
    modified_by: str
    modified_date: datetime
    expiration_time: int
    browser: str
    country_code: str
    device: str
    ip_address: str
    provider: str
    subject: str


class StakeholderStateToUpdate(NamedTuple):
    notifications_preferences: NotificationsPreferences | None = None
    trusted_device: TrustedDevice | None = None


class AccessTokens(NamedTuple):
    id: str
    issued_at: int
    jti_hashed: str
    salt: str
    name: str = "Token"
    last_use: datetime | None = None


class Stakeholder(NamedTuple):
    email: str
    access_tokens: list[AccessTokens] = []
    aws_customer_id: str | None = None
    enrolled: bool = False
    first_name: str | None = None
    is_concurrent_session: bool = False
    is_registered: bool = False
    jira_client_key: str | None = None
    last_login_date: datetime | None = None
    last_name: str | None = None
    legal_remember: bool = False
    phone: StakeholderPhone | None = None
    registration_date: datetime | None = None
    role: str | None = None
    session_key: str | None = None
    session_token: StakeholderSessionToken | None = None
    state: StakeholderState = StakeholderState(
        notifications_preferences=NotificationsPreferences(),
        modified_by=None,
        modified_date=None,
        trusted_devices=[],
    )
    login: StakeholderLogin | None = None
    subject: str | None = None
    tours: StakeholderTours = StakeholderTours()


class StakeholderMetadataToUpdate(NamedTuple):
    access_tokens: list[AccessTokens] | None = None
    aws_customer_id: str | None = None
    enrolled: bool | None = None
    first_name: str | None = None
    is_concurrent_session: bool | None = None
    is_registered: bool | None = None
    jira_client_key: str | None = None
    last_login_date: datetime | None = None
    last_name: str | None = None
    legal_remember: bool | None = None
    phone: StakeholderPhone | None = None
    registration_date: datetime | None = None
    role: str | None = None
    session_key: str | None = None
    session_token: StakeholderSessionToken | None = None
    subject: str | None = None
    tours: StakeholderTours | None = None
