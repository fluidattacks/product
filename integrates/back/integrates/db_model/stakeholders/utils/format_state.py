from datetime import (
    datetime,
)

from integrates.db_model.items import StakeholderStateItem
from integrates.db_model.stakeholders.types import (
    StakeholderState,
)
from integrates.db_model.stakeholders.utils.format_notifications_preferences import (
    format_notifications_preferences,
)
from integrates.db_model.stakeholders.utils.format_trusted_devices import (
    format_trusted_devices,
)


def format_state(item: StakeholderStateItem | None) -> StakeholderState:
    if item is not None:
        return StakeholderState(
            modified_by=item["modified_by"],
            modified_date=datetime.fromisoformat(item["modified_date"]),
            notifications_preferences=format_notifications_preferences(
                item.get("notifications_preferences"),
            ),
            trusted_devices=format_trusted_devices(item.get("trusted_devices")),
        )

    return StakeholderState(
        modified_by=None,
        modified_date=None,
        notifications_preferences=format_notifications_preferences(item),
        trusted_devices=format_trusted_devices(item),
    )
