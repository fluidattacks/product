from datetime import (
    datetime,
)

from integrates.db_model.items import StakeholderItem
from integrates.db_model.stakeholders.types import (
    Stakeholder,
    StakeholderTours,
)
from integrates.db_model.stakeholders.utils.format_access_tokens import (
    format_access_tokens,
)
from integrates.db_model.stakeholders.utils.format_login import (
    format_login,
)
from integrates.db_model.stakeholders.utils.format_phone import (
    format_phone,
)
from integrates.db_model.stakeholders.utils.format_session_token import (
    format_session_token,
)
from integrates.db_model.stakeholders.utils.format_state import (
    format_state,
)
from integrates.db_model.stakeholders.utils.format_tours import (
    format_tours,
)


def format_stakeholder(item: StakeholderItem) -> Stakeholder:
    email: str = item.get("email") or str(item["pk"]).split("#")[1]
    return Stakeholder(
        access_tokens=format_access_tokens(item["access_tokens"])
        if item.get("access_tokens")
        else [],
        aws_customer_id=item.get("aws_customer_id"),
        email=email.lower().strip(),
        enrolled=item.get("enrolled", False),
        first_name=item.get("first_name"),
        is_concurrent_session=item.get("is_concurrent_session", False),
        is_registered=item.get("is_registered", False),
        jira_client_key=item.get("jira_client_key") if item.get("jira_client_key") else None,
        last_login_date=datetime.fromisoformat(item["last_login_date"])
        if item.get("last_login_date")
        else None,
        last_name=item.get("last_name"),
        legal_remember=item.get("legal_remember", False),
        login=format_login(item.get("login")),
        phone=format_phone(item["phone"]) if item.get("phone") else None,
        state=format_state(item.get("state")),
        registration_date=datetime.fromisoformat(item["registration_date"])
        if item.get("registration_date")
        else None,
        role=item.get("role"),
        session_key=item.get("session_key"),
        session_token=format_session_token(item["session_token"])
        if item.get("session_token")
        else None,
        subject=item.get("subject"),
        tours=format_tours(item["tours"]) if item.get("tours") else StakeholderTours(),
    )
