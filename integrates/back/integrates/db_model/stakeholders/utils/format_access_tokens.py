from datetime import (
    datetime,
)

from integrates.db_model.items import AccessTokensItem
from integrates.db_model.stakeholders.types import (
    AccessTokens,
)


def format_access_tokens(items: list[AccessTokensItem]) -> list[AccessTokens]:
    return [
        AccessTokens(
            id=item["id"],
            issued_at=int(item["issued_at"]),
            jti_hashed=item["jti_hashed"],
            salt=item["salt"],
            name=item.get("name", "Token"),
            last_use=datetime.fromisoformat(item["last_use"]) if item.get("last_use") else None,
        )
        for item in items
    ]
