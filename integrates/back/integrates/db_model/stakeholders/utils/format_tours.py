from integrates.db_model.items import StakeholderToursItem
from integrates.db_model.stakeholders.types import (
    StakeholderTours,
)


def format_tours(item: StakeholderToursItem) -> StakeholderTours:
    return StakeholderTours(
        new_group=bool(item["new_group"]),
        new_root=bool(item["new_root"]),
        welcome=bool(item["welcome"]),
    )
