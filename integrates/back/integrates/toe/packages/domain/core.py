from integrates.custom_exceptions import ToePackageNotFound
from integrates.dataloaders import Dataloaders
from integrates.db_model.toe_packages.types import (
    ToePackage,
    ToePackageRequest,
)


async def get_package(
    *,
    loaders: Dataloaders,
    group_name: str,
    root_id: str,
    name: str,
    version: str,
) -> ToePackage:
    package = await loaders.toe_package.load(
        ToePackageRequest(
            group_name=group_name,
            root_id=root_id,
            name=name,
            version=version,
        ),
    )

    if not package:
        raise ToePackageNotFound()

    return package
