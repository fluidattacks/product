import re
from datetime import (
    datetime,
)

from integrates.custom_exceptions import (
    InvalidFileName,
    InvalidLinesOfCode,
    InvalidModifiedDate,
    InvalidSortsRiskLevel,
    InvalidSortsRiskLevelDate,
    InvalidSortsSuggestions,
)
from integrates.custom_utils import (
    datetime as datetime_utils,
)
from integrates.custom_utils.findings import (
    is_valid_finding_titles,
)
from integrates.db_model.toe_lines.types import (
    SortsSuggestion,
)


def validate_file_name(filename: str) -> None:
    if not re.match(
        r'^(?:[a-z]:)?[/\\]{0,2}(?:[./\\ ](?![./\\\n])|[^<>:"|?*./\\ \n])+$',
        filename,
    ):
        raise InvalidFileName()


def validate_modified_date(modified_date: datetime) -> None:
    if modified_date > datetime_utils.get_now():
        raise InvalidModifiedDate()


def validate_loc(loc: int) -> None:
    if loc < 0:
        raise InvalidLinesOfCode()


def validate_sort_risk_level(value: int) -> None:
    if not 0 <= value <= 100:
        raise InvalidSortsRiskLevel.new()


def validate_sorts_risk_level_date(sorts_risk_level_date: datetime) -> None:
    if sorts_risk_level_date > datetime.today():
        raise InvalidSortsRiskLevelDate()


def validate_sort_suggestions(suggestions: list[SortsSuggestion]) -> None:
    if len(suggestions) > 5:
        raise InvalidSortsSuggestions.new()
    is_valid_finding_titles([item.finding_title for item in suggestions])
    for item in suggestions:
        if not 0 <= item.probability <= 100:
            raise InvalidSortsSuggestions.new()
