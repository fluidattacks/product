from datetime import datetime

from integrates.custom_exceptions import (
    InvalidCommitHash,
    InvalidField,
    InvalidGitRoot,
    InvalidLinesOfCode,
    InvalidModifiedDate,
    InvalidToeLinesAttackAt,
)
from integrates.custom_utils import datetime as datetime_utils
from integrates.dataloaders import Dataloaders, get_new_context
from integrates.db_model.toe_lines.types import ToeLine, ToeLineRequest, ToeLineState
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    GitRootFaker,
    GitRootStateFaker,
    GroupFaker,
    OrganizationFaker,
    OrganizationStateFaker,
    StakeholderFaker,
    ToeLinesFaker,
    ToeLinesStateFaker,
    UrlRootFaker,
)
from integrates.testing.mocks import mocks
from integrates.testing.utils import freeze_time, parametrize, raises
from integrates.toe.lines.domain import _assign_toe_lines, _get_attacked_lines, add, update
from integrates.toe.lines.types import ToeLinesAttributesToAdd, ToeLinesAttributesToUpdate


@parametrize(
    args=["current_value", "attributes", "loc", "expected_output"],
    cases=[
        [
            ToeLine(
                filename="src/main.py",
                group_name="group1",
                root_id="4039d098-ffc5-4984-8ed3-eb17bca98e19",
                state=ToeLineState(
                    attacked_at=datetime.fromisoformat("2023-10-01T05:00:00"),
                    attacked_by="jdoe@fluidattacks.com",
                    attacked_lines=100,
                    be_present=True,
                    be_present_until=None,
                    comments="",
                    first_attack_at=datetime.fromisoformat("2023-10-01T05:00:00"),
                    has_vulnerabilities=False,
                    last_author="jdoe@company.com",
                    last_commit="c213edf2d0cdf3cce271fda72364877de63ff0ed",
                    last_commit_date=datetime.fromisoformat("2023-10-01T05:00:00"),
                    loc=500,
                    modified_by="jdoe@fluidattacks.com",
                    modified_date=datetime.fromisoformat("2023-10-01T05:00:00"),
                    seen_at=datetime.fromisoformat("2023-10-01T05:00:00"),
                    sorts_risk_level=0,
                ),
                seen_first_time_by=None,
            ),
            ToeLinesAttributesToUpdate(
                attacked_lines=150,
                attacked_at=datetime.fromisoformat("2023-10-01T06:00:00"),
                last_commit_date=datetime.fromisoformat("2023-10-01T05:30:00"),
            ),
            500,
            150,
        ],
        [
            ToeLine(
                filename="src/output.exe",
                group_name="group1",
                root_id="4039d098-ffc5-4984-8ed3-eb17bca98e19",
                state=ToeLineState(
                    attacked_at=datetime.fromisoformat("2023-10-01T05:00:00"),
                    attacked_by="jdoe@fluidattacks.com",
                    attacked_lines=100,
                    be_present=True,
                    be_present_until=None,
                    comments="",
                    first_attack_at=datetime.fromisoformat("2023-10-01T05:00:00"),
                    has_vulnerabilities=False,
                    last_author="jdoe@company.com",
                    last_commit="c213edf2d0cdf3cce271fda72364877de63ff0ed",
                    last_commit_date=datetime.fromisoformat("2023-10-01T05:00:00"),
                    loc=500,
                    modified_by="jdoe@fluidattacks.com",
                    modified_date=datetime.fromisoformat("2023-10-01T05:00:00"),
                    seen_at=datetime.fromisoformat("2023-10-01T05:00:00"),
                    sorts_risk_level=0,
                ),
                seen_first_time_by=None,
            ),
            ToeLinesAttributesToUpdate(
                attacked_lines=150,
                attacked_at=datetime.fromisoformat("2023-10-01T06:00:00"),
                last_commit_date=datetime.fromisoformat("2023-10-01T05:30:00"),
            ),
            500,
            500,
        ],
        [
            ToeLine(
                filename="src/test_main.py",
                group_name="group1",
                root_id="4039d098-ffc5-4984-8ed3-eb17bca98e19",
                state=ToeLineState(
                    attacked_at=datetime.fromisoformat("2023-10-01T05:00:00"),
                    attacked_by="jdoe@fluidattacks.com",
                    attacked_lines=100,
                    be_present=True,
                    be_present_until=None,
                    comments="",
                    first_attack_at=datetime.fromisoformat("2023-10-01T05:00:00"),
                    has_vulnerabilities=False,
                    last_author="jdoe@company.com",
                    last_commit="c213edf2d0cdf3cce271fda72364877de63ff0ed",
                    last_commit_date=datetime.fromisoformat("2023-10-01T05:00:00"),
                    loc=500,
                    modified_by="jdoe@fluidattacks.com",
                    modified_date=datetime.fromisoformat("2023-10-01T05:00:00"),
                    seen_at=datetime.fromisoformat("2023-10-01T05:00:00"),
                    sorts_risk_level=0,
                ),
                seen_first_time_by=None,
            ),
            ToeLinesAttributesToUpdate(),
            500,
            100,
        ],
        [
            ToeLine(
                filename="src/test_main.py",
                group_name="group1",
                root_id="4039d098-ffc5-4984-8ed3-eb17bca98e19",
                state=ToeLineState(
                    attacked_at=datetime.fromisoformat("2023-10-01T05:00:00"),
                    attacked_by="jdoe@fluidattacks.com",
                    attacked_lines=80,
                    be_present=True,
                    be_present_until=None,
                    comments="",
                    first_attack_at=datetime.fromisoformat("2023-10-01T05:00:00"),
                    has_vulnerabilities=False,
                    last_author="jdoe@company.com",
                    last_commit="c213edf2d0cdf3cce271fda72364877de63ff0ed",
                    last_commit_date=datetime.fromisoformat("2023-10-01T05:00:00"),
                    loc=500,
                    modified_by="jdoe@fluidattacks.com",
                    modified_date=datetime.fromisoformat("2023-10-01T05:00:00"),
                    seen_at=datetime.fromisoformat("2023-10-01T05:00:00"),
                    sorts_risk_level=0,
                ),
                seen_first_time_by=None,
            ),
            ToeLinesAttributesToUpdate(
                last_commit_date=datetime.fromisoformat("2023-10-01T05:30:00"),
            ),
            500,
            80,
        ],
        [
            ToeLine(
                filename="src/test_main.py",
                group_name="group1",
                root_id="4039d098-ffc5-4984-8ed3-eb17bca98e19",
                state=ToeLineState(
                    attacked_at=datetime.fromisoformat("2023-10-01T05:00:00"),
                    attacked_by="jdoe@fluidattacks.com",
                    attacked_lines=100,
                    be_present=True,
                    be_present_until=None,
                    comments="",
                    first_attack_at=datetime.fromisoformat("2023-10-01T05:00:00"),
                    has_vulnerabilities=False,
                    last_author="jdoe@company.com",
                    last_commit="c213edf2d0cdf3cce271fda72364877de63ff0ed",
                    last_commit_date=datetime.fromisoformat("2023-10-01T05:00:00"),
                    loc=500,
                    modified_by="jdoe@fluidattacks.com",
                    modified_date=datetime.fromisoformat("2023-10-01T05:00:00"),
                    seen_at=datetime.fromisoformat("2023-10-01T05:00:00"),
                    sorts_risk_level=0,
                ),
                seen_first_time_by=None,
            ),
            ToeLinesAttributesToUpdate(
                last_commit_date=datetime.fromisoformat("2023-10-01T05:30:00"),
            ),
            50,
            0,
        ],
    ],
)
async def test_get_attacked_lines(
    current_value: ToeLine,
    attributes: ToeLinesAttributesToUpdate,
    loc: int,
    expected_output: int,
) -> None:
    result = _get_attacked_lines(
        attributes=attributes,
        current_value=current_value,
        loc=loc,
    )
    assert result == expected_output


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(
                    id="org1",
                    state=OrganizationStateFaker(
                        aws_external_id="be991c12-9b68-4312-85c4-16ce5fa2fca0",
                    ),
                ),
            ],
            groups=[
                GroupFaker(name="group1", organization_id="org1"),
            ],
            roots=[
                GitRootFaker(
                    group_name="group1",
                    id="root1",
                    state=GitRootStateFaker(nickname="testroot"),
                ),
            ],
        ),
    )
)
@parametrize(
    args=["expected", "attributes"],
    cases=[
        [
            ToeLine(
                filename="package-lock.json",
                group_name="group1",
                root_id="root1",
                state=ToeLineState(
                    attacked_at=datetime.fromisoformat("2022-08-01T05:00:00+00:00"),
                    attacked_by="machine@fluidattacks.com",
                    attacked_lines=100,
                    be_present=True,
                    be_present_until=None,
                    comments="",
                    first_attack_at=datetime.fromisoformat("2022-08-01T05:00:00+00:00"),
                    has_vulnerabilities=False,
                    last_author="jdoe@group1.com",
                    last_commit="0000",
                    last_commit_date=datetime.fromisoformat("2022-08-01T05:00:00+00:00"),
                    loc=100,
                    modified_by="machine@fluidattacks.com",
                    modified_date=datetime.fromisoformat("2022-08-01T05:00:00+00:00"),
                    seen_at=datetime.fromisoformat("2022-08-01T05:00:00+00:00"),
                    sorts_risk_level=-1,
                    sorts_priority_factor=-1,
                ),
                seen_first_time_by=None,
            ),
            ToeLinesAttributesToAdd(
                last_author="jdoe@group1.com",
                loc=100,
                last_commit="0000",
                last_commit_date=datetime.fromisoformat("2022-08-01T05:00:00+00:00"),
            ),
        ],
    ],
)
@freeze_time("2022-08-01T05:00:00+00:00")
async def test_assign_toe_lines(
    attributes: ToeLinesAttributesToAdd,
    expected: ToeLine,
) -> None:
    loaders: Dataloaders = get_new_context()
    root = next(root for root in await loaders.group_roots.load("group1") if root.id == "root1")
    result = _assign_toe_lines(
        attributes=attributes, filename=expected.filename, group_name="group1", root=root
    )
    assert result == expected


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(
                    id="org1",
                    state=OrganizationStateFaker(
                        aws_external_id="be991c12-9b68-4312-85c4-16ce5fa2fca0",
                    ),
                ),
            ],
            groups=[
                GroupFaker(name="group1", organization_id="org1"),
            ],
            roots=[
                GitRootFaker(
                    group_name="group1",
                    id="root1",
                    state=GitRootStateFaker(nickname="testroot"),
                ),
            ],
            toe_lines=[
                ToeLinesFaker(
                    filename="src/main.py",
                    group_name="group1",
                    root_id="root1",
                    state=ToeLinesStateFaker(
                        loc=180,
                        seen_at=datetime.fromisoformat("2020-09-01T05:00:00+00:00"),
                        modified_date=datetime.fromisoformat("2020-09-01T05:00:00+00:00"),
                    ),
                ),
            ],
        ),
    )
)
@parametrize(
    args=["expected", "attributes"],
    cases=[
        [
            ToeLine(
                filename="src/pyproject.toml",
                group_name="group1",
                root_id="root1",
                state=ToeLineState(
                    attacked_at=datetime.fromisoformat("2022-08-01T05:00:00+00:00"),
                    attacked_by="machine@fluidattacks.com",
                    attacked_lines=100,
                    be_present=True,
                    be_present_until=None,
                    comments="",
                    first_attack_at=datetime.fromisoformat("2022-08-01T05:00:00+00:00"),
                    has_vulnerabilities=False,
                    last_author="jdoe@group1.com",
                    last_commit="f9e4beba70c4f34d6117c3b0c23ebe6b2bff66c4",
                    last_commit_date=datetime.fromisoformat("2022-08-01T05:00:00+00:00"),
                    loc=100,
                    modified_by="machine@fluidattacks.com",
                    modified_date=datetime.fromisoformat("2022-08-01T05:00:00+00:00"),
                    seen_at=datetime.fromisoformat("2022-08-01T05:00:00+00:00"),
                    sorts_risk_level=-1,
                    sorts_priority_factor=-1,
                ),
                seen_first_time_by=None,
            ),
            ToeLinesAttributesToAdd(
                last_author="jdoe@group1.com",
                loc=100,
                last_commit="f9e4beba70c4f34d6117c3b0c23ebe6b2bff66c4",
                last_commit_date=datetime.fromisoformat("2022-08-01T05:00:00+00:00"),
            ),
        ],
    ],
)
@freeze_time("2022-08-01T05:00:00+00:00")
async def test_add_toe_lines_success(
    attributes: ToeLinesAttributesToAdd,
    expected: ToeLine,
) -> None:
    loaders: Dataloaders = get_new_context()
    await add(
        loaders=loaders,
        filename="src/pyproject.toml",
        group_name="group1",
        root_id="root1",
        attributes=attributes,
    )
    toe_line: ToeLine | None = await loaders.toe_lines.load(
        ToeLineRequest(
            filename="src/pyproject.toml",
            group_name="group1",
            root_id="root1",
        ),
    )

    assert toe_line == expected


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(
                    id="org1",
                    state=OrganizationStateFaker(
                        aws_external_id="be991c12-9b68-4312-85c4-16ce5fa2fca0",
                    ),
                ),
            ],
            groups=[
                GroupFaker(name="group1", organization_id="org1"),
            ],
            roots=[
                GitRootFaker(
                    group_name="group1",
                    id="root1",
                    state=GitRootStateFaker(nickname="testroot"),
                ),
            ],
            toe_lines=[
                ToeLinesFaker(
                    filename="src/main.py",
                    group_name="group1",
                    root_id="root1",
                    state=ToeLinesStateFaker(
                        loc=180,
                        seen_at=datetime.fromisoformat("2020-09-01T05:00:00+00:00"),
                        modified_date=datetime.fromisoformat("2020-09-01T05:00:00+00:00"),
                    ),
                ),
            ],
        ),
    )
)
@parametrize(
    args=["current_value", "expected", "attributes"],
    cases=[
        [
            ToeLine(
                filename="src/main.py",
                group_name="group1",
                root_id="root1",
                state=ToeLineState(
                    attacked_at=datetime.fromisoformat("2021-09-01T05:00:00+00:00"),
                    attacked_by="hacker2@test.com",
                    attacked_lines=434,
                    be_present=True,
                    be_present_until=None,
                    comments="comment test 2",
                    first_attack_at=datetime.fromisoformat("2020-08-01T05:00:00+00:00"),
                    has_vulnerabilities=False,
                    last_author="customer2@gmail.com",
                    last_commit="f9e4beba70c4f34d6117c3b0c23ebe6b2bff66c4",
                    last_commit_date=datetime.fromisoformat("2020-08-01T05:00:00+00:00"),
                    loc=180,
                    modified_by="hacker2@test.com",
                    modified_date=datetime.fromisoformat("2020-09-01T05:00:00+00:00"),
                    seen_at=datetime.fromisoformat("2019-08-01T05:00:00+00:00"),
                    sorts_risk_level=50,
                    sorts_priority_factor=70,
                    sorts_risk_level_date=None,
                    sorts_suggestions=None,
                ),
                seen_first_time_by=None,
            ),
            ToeLine(
                filename="src/main.py",
                group_name="group1",
                root_id="root1",
                state=ToeLineState(
                    attacked_at=datetime.fromisoformat("2021-09-01T05:00:00+00:00"),
                    attacked_by="hacker2@test.com",
                    attacked_lines=0,
                    be_present=True,
                    be_present_until=None,
                    comments="comment test 2",
                    first_attack_at=datetime.fromisoformat("2020-08-01T05:00:00+00:00"),
                    has_vulnerabilities=True,
                    last_author="customer2@gmail.com",
                    last_commit="f9e4beba70c4f34d6117c3b0c23ebe6b2bff66c4",
                    last_commit_date=datetime.fromisoformat("2020-08-01T05:00:00+00:00"),
                    loc=180,
                    modified_by="machine@fluidattacks.com",
                    modified_date=datetime.fromisoformat("2022-08-01T05:00:00+00:00"),
                    seen_at=datetime.fromisoformat("2019-08-01T05:00:00+00:00"),
                    sorts_risk_level=50,
                    sorts_priority_factor=70,
                    sorts_risk_level_date=None,
                    sorts_suggestions=None,
                ),
                seen_first_time_by=None,
            ),
            ToeLinesAttributesToUpdate(
                attacked_lines=0,
                has_vulnerabilities=True,
            ),
        ],
    ],
)
@freeze_time("2022-08-01T05:00:00+00:00")
async def test_update_toe_lines_success(
    current_value: ToeLine,
    expected: ToeLine,
    attributes: ToeLinesAttributesToUpdate,
) -> None:
    loaders: Dataloaders = get_new_context()
    await update(current_value=current_value, attributes=attributes)
    toe_line: ToeLine | None = await loaders.toe_lines.load(
        ToeLineRequest(
            filename=current_value.filename,
            group_name=current_value.group_name,
            root_id=current_value.root_id,
        ),
    )

    assert toe_line == expected


@parametrize(
    args=["current_value", "attributes"],
    cases=[
        [
            ToeLine(
                filename="test/new.new",
                group_name="group1",
                root_id="4039d098-ffc5-4984-8ed3-eb17bca98e19",
                state=ToeLineState(
                    attacked_at=datetime.fromisoformat("2021-09-01T05:00:00+00:00"),
                    attacked_by="hacker2@test.com",
                    attacked_lines=434,
                    be_present=True,
                    be_present_until=None,
                    comments="comment test 2",
                    first_attack_at=datetime.fromisoformat("2020-08-01T05:00:00+00:00"),
                    has_vulnerabilities=False,
                    last_author="customer2@gmail.com",
                    last_commit="f9e4beba70c4f34d6117c3b0c23ebe6b2bff66c4",
                    last_commit_date=datetime.fromisoformat("2020-08-01T05:00:00+00:00"),
                    loc=1111,
                    modified_by="hacker2@test.com",
                    modified_date=datetime.fromisoformat("2022-08-01T05:00:00+00:00"),
                    seen_at=datetime.fromisoformat("2019-08-01T05:00:00+00:00"),
                    sorts_risk_level=50,
                    sorts_priority_factor=70,
                    sorts_risk_level_date=None,
                    sorts_suggestions=None,
                ),
                seen_first_time_by=None,
            ),
            ToeLinesAttributesToUpdate(
                attacked_at=datetime.fromisoformat("2021-09-01T05:00:00+00:00"),
                attacked_by="hacker2@test.com",
                attacked_lines=434,
                comments="comment test 2",
                last_author="customer2@gmail.com",
                has_vulnerabilities=False,
                loc=1111,
                last_commit="f9e4beba70c4f34d6117c3b0c23ebe6b2bff66c4",
                last_commit_date=datetime.fromisoformat("2020-08-01T05:00:00+00:00"),
                seen_at=datetime.fromisoformat("2019-08-01T05:00:00+00:00"),
                sorts_risk_level=50,
                sorts_priority_factor=70,
            ),
        ],
    ],
)
@freeze_time("2022-08-01T05:00:00+00:00")
async def test_update_toe_lines_exception(
    current_value: ToeLine,
    attributes: ToeLinesAttributesToUpdate,
) -> None:
    with raises(InvalidToeLinesAttackAt):
        await update(current_value, attributes)


@parametrize(
    args=[
        "root_id",
        "filename",
        "last_author",
        "last_commit",
        "last_commit_date",
        "loc",
        "expected_exception",
    ],
    cases=[
        [
            "root2",
            "src/main.py",
            "testclient@clientapp.com",
            "0000000",
            datetime_utils.get_utc_now(),
            100,
            InvalidGitRoot(),
        ],
        [
            "root1",
            "src/main.py",
            "invalid_author",
            "0000000",
            datetime_utils.get_utc_now(),
            100,
            InvalidField(),
        ],
        [
            "root1",
            "src/main.py",
            "testclient@clientapp.com",
            "0000000",
            datetime_utils.get_utc_now(),
            100,
            InvalidCommitHash(),
        ],
        [
            "root1",
            "src/main.py",
            "testclient@clientapp.com",
            "a4d56f9b8d7c4c9e9f5b12c3a7f8c9e1e2a4b4c6",
            datetime_utils.get_utc_now(),
            -10,
            InvalidLinesOfCode(),
        ],
        [
            "root1",
            "src/main.py",
            "testclient@clientapp.com",
            "a4d56f9b8d7c4c9e9f5b12c3a7f8c9e1e2a4b4c6",
            datetime_utils.get_plus_delta(date=datetime_utils.get_utc_now(), hours=2),
            100,
            InvalidModifiedDate(),
        ],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(id="org1"),
            ],
            stakeholders=[
                StakeholderFaker(email="admin@fluidattacks.com", role="admin"),
            ],
            groups=[
                GroupFaker(name="group1", organization_id="org1"),
            ],
            roots=[
                GitRootFaker(group_name="group1", id="root1"),
                UrlRootFaker(group_name="group1", root_id="root2"),
            ],
        ),
    )
)
async def test_add_toe_lines_fail(
    root_id: str,
    filename: str,
    last_author: str,
    last_commit: str,
    last_commit_date: datetime,
    loc: int,
    expected_exception: Exception,
) -> None:
    loaders: Dataloaders = get_new_context()
    with raises(expected_exception.__class__):
        await add(
            loaders=loaders,
            group_name="group1",
            root_id=root_id,
            filename=filename,
            attributes=ToeLinesAttributesToAdd(
                attacked_lines=0,
                be_present=False,
                last_author=last_author,
                last_commit=last_commit,
                loc=loc,
                last_commit_date=last_commit_date,
                seen_first_time_by="hacker@fluidattacks.com",
            ),
        )
