import logging
import re
from datetime import (
    datetime,
)

from urllib3.util.url import (
    parse_url,
)

from integrates.custom_exceptions import (
    ExcludedEnvironment,
    ExcludedSubPath,
    InvalidParameter,
    InvalidRootComponent,
    InvalidToeInputAttackedAt,
    InvalidToeInputAttackedBy,
    InvalidUrl,
    RootEnvironmentUrlNotFound,
    ToeInputNotPresent,
)
from integrates.custom_utils import (
    datetime as datetime_utils,
)
from integrates.custom_utils.roots import (
    get_root_base_url,
)
from integrates.custom_utils.validations import (
    is_valid_url,
)
from integrates.custom_utils.validations_deco import (
    validate_active_root_deco,
    validate_root_type_deco,
)
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.roots.enums import (
    RootEnvironmentUrlStateStatus,
)
from integrates.db_model.roots.types import (
    GitRoot,
    Root,
    RootEnvironmentUrl,
    RootEnvironmentUrlRequest,
    RootEnvironmentUrlsRequest,
    RootRequest,
    URLRoot,
)
from integrates.db_model.toe_inputs.types import (
    ToeInput,
    ToeInputState,
)
from integrates.settings import (
    LOGGING,
)
from integrates.toe.inputs.types import (
    ToeInputAttributesToUpdate,
)

ELEVATED_AWS_IAM_POLICIES = {
    "arn:aws:iam::aws:policy/PowerUserAccess",
    "arn:aws:iam::aws:policy/IAMFullAccess",
    "arn:aws:iam::aws:policy/AdministratorAccess",
}

logging.config.dictConfig(LOGGING)

LOGGER = logging.getLogger(__name__)


def _extract_aws_id(url: str) -> str:
    match = re.match(r"arn:aws:iam::(\d+):", url)
    return match.group(1) if match else url


def _validate_arn(arn: str) -> bool:
    # Add new regular expressions to the patterns list, for that ARNs that do
    # not have a client id in their structure.
    patterns = [
        r"^arn:aws:apigateway:([a-z\-0-9]+)::/",
        r"^arn:aws:ec2:([a-z\-0-9]+)::image/([a-zA-Z0-9\-]+)$",
        r"^arn:aws:ec2:([a-z\-0-9]+)::snapshot/([a-zA-Z0-9\-]+)$",
    ]

    for pattern in patterns:
        if re.match(pattern, arn):
            return True

    return False


def _validate_aws_component(component: str, env_url: RootEnvironmentUrl) -> bool:
    return bool(
        env_url.state.cloud_name
        and env_url.state.cloud_name.value == "AWS"
        and (
            component.startswith(("arn:aws:s3:::", "arn:aws:route53:::"))
            or _validate_arn(component)
            or component in ELEVATED_AWS_IAM_POLICIES
            or _extract_aws_id(env_url.url) in component
        ),
    )


def _validate_cspm_component(
    component: str,
    env_urls: list[RootEnvironmentUrl],
    env_id: str,
) -> bool:
    for environment_url in env_urls:
        if env_id and environment_url.id != env_id:
            continue
        if environment_url.url in component or _validate_aws_component(component, environment_url):
            return True
    return False


async def get_env_urls(loaders: Dataloaders, root: Root) -> list[RootEnvironmentUrl]:
    return [
        env_url
        for env_url in await loaders.root_environment_urls.load(
            RootEnvironmentUrlsRequest(root_id=root.id, group_name=root.group_name),
        )
        if env_url.state.include
    ]


def validate_cspm_component(
    component: str,
    env_urls: list[RootEnvironmentUrl],
    env_id: str,
) -> bool:
    cspm_env_urls = [
        url
        for url in env_urls
        if url.state.cloud_name and url.state.cloud_name.value in {"AWS", "GCP", "AZURE"}
    ]
    return bool(cspm_env_urls) and _validate_cspm_component(component, cspm_env_urls, env_id)


async def validate_git_root_component(
    loaders: Dataloaders,
    root: Root,
    component: str,
    env_id: str,
) -> None:
    if not isinstance(root, GitRoot):
        return
    env_urls = await get_env_urls(loaders, root)

    if validate_cspm_component(component, env_urls, env_id):
        return

    if (
        component not in [x.url for x in env_urls]
        and not is_valid_url(component)
        and not any(component.startswith(x.url) for x in env_urls)
    ):
        raise InvalidUrl()
    for environment_url in env_urls:
        if env_id and environment_url.id != env_id:
            continue

        formatted_environment_url = (
            environment_url.url if environment_url.url.endswith("/") else f"{environment_url.url}/"
        )
        formatted_component = component if component.endswith("/") else f"{component}/"
        parsed_environment_url = parse_url(formatted_environment_url)
        parsed_component = parse_url(formatted_component)

        if (
            formatted_component.startswith(formatted_environment_url)
            or parsed_component.host == parsed_environment_url.host
        ):
            return
    raise InvalidRootComponent()


async def is_input_excluded_from_scope(loaders: Dataloaders, toe_input: ToeInput) -> bool:
    root = await loaders.root.load(
        RootRequest(
            group_name=toe_input.group_name,
            root_id=toe_input.root_id,
        ),
    )
    formatted_component = (
        toe_input.component if toe_input.component.endswith("/") else f"{toe_input.component}/"
    )
    match root:
        case GitRoot():
            excluded_env_urls = {
                env_url.url if env_url.url.endswith("/") else f"{env_url.url}/"
                for env_url in await loaders.root_environment_urls.load(
                    RootEnvironmentUrlsRequest(root_id=root.id, group_name=root.group_name),
                )
                if not env_url.state.include
            }
        case URLRoot():
            base_url = get_root_base_url(root)
            excluded_env_urls = {
                f"{base_url}/{sub_path}/" for sub_path in root.state.excluded_sub_paths or []
            }
        case _:
            excluded_env_urls = set()

    if any(formatted_component.startswith(env_url) for env_url in excluded_env_urls):
        return True

    return False


async def validate_input_excluded_from_scope(loaders: Dataloaders, toe_input: ToeInput) -> None:
    if await is_input_excluded_from_scope(loaders, toe_input):
        raise ExcludedEnvironment()


def validate_url_root_component(root: Root, component: str) -> None:
    if not isinstance(root, URLRoot):
        return

    url_with_port = (
        f"{root.state.host}:{root.state.port}"
        if root.state.port and root.state.protocol != "FILE"
        else root.state.host
    )

    if not is_valid_url(component):
        raise InvalidUrl()

    if root.state.excluded_sub_paths and component in {
        f"{root.state.protocol.lower()}://{url_with_port}"
        f"{root.state.path.removesuffix('/')}/{sub_path}"
        for sub_path in root.state.excluded_sub_paths
    }:
        raise ExcludedSubPath()

    if root.state.query is None and f"{component}/".startswith(
        f"{root.state.protocol.lower()}://{url_with_port}{root.state.path.removesuffix('/')}/",
    ):
        return

    if (
        root.state.query is not None
        and component
        == f"{root.state.protocol.lower()}://{url_with_port}{root.state.path}?{root.state.query}"
    ):
        return
    raise InvalidRootComponent()


@validate_active_root_deco("root")
async def validate_component(
    *,
    loaders: Dataloaders,
    root: Root,
    component: str,
    env_id: str,
) -> None:
    await validate_git_root_component(loaders, root, component, env_id)
    validate_url_root_component(root, component)


async def validate_input_state(
    loaders: Dataloaders,
    attributes: ToeInputAttributesToUpdate,
    current_value: ToeInput,
) -> None:
    match attributes, current_value:
        case [
            ToeInputAttributesToUpdate(attacked_at=datetime(), be_present=None),
            ToeInput(state=ToeInputState(be_present=False)),
        ]:
            raise ToeInputNotPresent()
        case [
            ToeInputAttributesToUpdate(attacked_at=datetime(), be_present=False),
            ToeInput(),
        ]:
            raise ToeInputNotPresent()
        case [
            ToeInputAttributesToUpdate(attacked_at=datetime() as new_attacked_at),
            ToeInput(state=ToeInputState(attacked_at=datetime() as current_attacked_at)),
        ] if new_attacked_at <= current_attacked_at:
            raise InvalidToeInputAttackedAt()
        case [
            ToeInputAttributesToUpdate(attacked_at=datetime() as new_attacked_at),
            ToeInput(),
        ] if new_attacked_at > datetime_utils.get_utc_now():
            raise InvalidToeInputAttackedAt()
        case [
            ToeInputAttributesToUpdate(attacked_at=datetime() as new_attacked_at),
            ToeInput(state=ToeInputState(seen_at=datetime() as current_seen_at)),
        ] if new_attacked_at < current_seen_at:
            raise InvalidToeInputAttackedAt()
        case [
            ToeInputAttributesToUpdate(attacked_at=datetime(), attacked_by=None),
            ToeInput(),
        ]:
            raise InvalidToeInputAttackedBy()
        case [
            ToeInputAttributesToUpdate(be_present=True),
            ToeInput(state=ToeInputState(be_present=False)),
        ] if await is_input_excluded_from_scope(loaders, current_value):
            raise ExcludedEnvironment()


async def validate_environment_url(
    loaders: Dataloaders,
    root_id: str,
    group_name: str,
    url_id: str,
) -> None:
    if not url_id.strip():
        raise InvalidParameter("environmentId")
    environment_url = await loaders.environment_url.load(
        RootEnvironmentUrlRequest(root_id=root_id, group_name=group_name, url_id=url_id),
    )
    if not environment_url:
        raise RootEnvironmentUrlNotFound()

    if environment_url.state.status is RootEnvironmentUrlStateStatus.DELETED:
        LOGGER.error(
            "Environment URL not found",
            extra={
                "extra": {
                    "root_id": root_id,
                    "url_id": url_id,
                    "status": environment_url.state.status,
                },
            },
        )
        raise RootEnvironmentUrlNotFound()


@validate_root_type_deco("root", (GitRoot, URLRoot))
async def validate_toe_input(loaders: Dataloaders, root: Root, toe_input: ToeInput) -> None:
    await validate_input_excluded_from_scope(loaders=loaders, toe_input=toe_input)
    await validate_component(
        loaders=loaders,
        root=root,
        component=toe_input.component,
        env_id=toe_input.environment_id,
    )
