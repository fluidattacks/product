from integrates.authz.model.types import (
    RoleLevel,
)

SERVICE_FORCES_ROLE: dict[str, RoleLevel] = {
    "group_level": RoleLevel(
        actions={
            "integrates_api_mutations_add_environment_integrity_event_mutate",
            "integrates_api_mutations_add_forces_execution_mutate",
            "integrates_api_resolvers_query_finding_resolve",
            "integrates_api_resolvers_query_group_resolve",
            "integrates_api_resolvers_query_root_resolve",
            "integrates_api_resolvers_query_vulnerability_resolve",
            "integrates_api_resolvers_vulnerability_historic_zero_risk_resolve",
        },
        tags={"forces"},
    ),
}
