from contextlib import (
    suppress,
)
from typing import (
    NamedTuple,
)

from integrates.custom_exceptions import (
    StakeholderNotFound,
)
from integrates.custom_utils import (
    datetime as datetime_utils,
)
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model import (
    group_access as group_access_model,
)
from integrates.db_model import (
    organization_access as organization_access_model,
)
from integrates.db_model import (
    stakeholders as stakeholders_model,
)
from integrates.db_model.group_access.types import (
    GroupAccessMetadataToUpdate,
    GroupAccessRequest,
    GroupAccessState,
)
from integrates.db_model.group_access.utils import (
    merge_group_access_changes,
)
from integrates.db_model.groups.enums import (
    GroupManaged,
    GroupService,
    GroupStateStatus,
    GroupSubscriptionType,
)
from integrates.db_model.groups.types import (
    Group,
)
from integrates.db_model.organization_access.types import (
    OrganizationAccessMetadataToUpdate,
    OrganizationAccessRequest,
    OrganizationAccessState,
)
from integrates.db_model.stakeholders.types import (
    StakeholderMetadataToUpdate,
)

from .model import (
    FLUID_IDENTIFIER,
    get_group_level_roles_model,
    get_organization_level_roles_model,
    get_user_level_roles_model,
)


class ServicePolicy(NamedTuple):
    group_name: str
    service: str


def get_group_service_policies(group: Group) -> tuple[str, ...]:
    """Gets a group's authorization policies."""
    policies: tuple[str, ...] = tuple(
        policy.service for policy in _get_service_policies(group) if policy.group_name == group.name
    )
    return policies


def _get_service_policies(group: Group) -> list[ServicePolicy]:
    """Return a list of policies for the given group."""
    has_advanced = group.state.has_advanced if group else False
    has_asm = group.state.status == GroupStateStatus.ACTIVE if group else False
    service = group.state.service if group else None
    type_ = group.state.type if group else ""
    has_essential_advanced: bool = has_advanced or group.state.has_essential if group else False

    business_rules = (
        (has_asm, "asm"),
        (
            group.state.managed is GroupManaged.UNDER_REVIEW if group else False,
            "is_under_review",
        ),
        (
            type_ == GroupSubscriptionType.CONTINUOUS and has_asm and has_essential_advanced,
            "report_vulnerabilities",
        ),
        (service == GroupService.BLACK and has_asm, "service_black"),
        (service == GroupService.WHITE and has_asm, "service_white"),
        (
            has_asm and has_essential_advanced,
            "request_zero_risk",
        ),
        (
            type_ == GroupSubscriptionType.CONTINUOUS and has_asm,
            "forces",
        ),
        (
            type_ == GroupSubscriptionType.CONTINUOUS and has_asm and has_advanced,
            "advanced",
        ),
        (type_ == GroupSubscriptionType.CONTINUOUS, "continuous"),
        (
            type_ == GroupSubscriptionType.ONESHOT and has_asm,
            "report_vulnerabilities",
        ),
        (
            type_ == GroupSubscriptionType.ONESHOT and has_asm and has_advanced,
            "advanced",
        ),
    )

    return [
        ServicePolicy(group_name=group.name, service=policy_name)
        for condition, policy_name in business_rules
        if condition
    ]


async def get_group_level_role(
    loaders: Dataloaders,
    email: str,
    group_name: str,
) -> str:
    """
    Returns the user's role in the group.
    Empty string if the user has no role.

    Admin users are granted access to all groups.
    """
    group_access = await loaders.group_access.load(
        GroupAccessRequest(group_name=group_name, email=email),
    )
    if group_access and group_access.state.role:
        return group_access.state.role

    if await get_user_level_role(loaders, email) == "admin":
        return "admin"

    return ""


async def get_group_level_roles(
    loaders: Dataloaders,
    email: str,
    groups: list[str],
) -> dict[str, str]:
    """
    Returns the user's role in each queried group.

    Admin users are `admin` if user does not have another role
    in the group. Otherwise, the role in group is returned.

    Returns:
        dict[str, str]: A dictionary with the group name as key
        and the user's role in the group as value.

    """
    is_admin: bool = await get_user_level_role(loaders, email) == "admin"
    groups_access = await loaders.stakeholder_groups_access.load(email)
    db_roles: dict[str, str] = {
        access.group_name: access.state.role for access in groups_access if access.state.role
    }

    return {
        group: "admin" if is_admin and group not in db_roles else db_roles.get(group, "")
        for group in groups
    }


async def get_organization_level_role(
    loaders: Dataloaders,
    email: str,
    organization_id: str,
) -> str:
    """
    Returns the user's role in the organization.
    Empty string if the user has no role.

    Admin users are granted access to all organizations.
    """
    org_access = await loaders.organization_access.load(
        OrganizationAccessRequest(organization_id=organization_id, email=email),
    )
    if org_access and org_access.state.role:
        return org_access.state.role

    if await get_user_level_role(loaders, email) == "admin":
        return "admin"

    return ""


async def get_user_level_role(
    loaders: Dataloaders,
    email: str,
) -> str:
    """
    Returns the user's role. Empty string if the user
    is not a stakeholder.

    Valid roles are:
    `admin`, `hacker`, `reattacker`, `resourcer`,
    `reviewer`, `architect`, `service_forces`,
    `user`, `group_manager`, `organization_manager`,
    `vulnerability_manager`.
    """
    user_role: str = ""
    with suppress(StakeholderNotFound):
        stakeholder = await loaders.stakeholder.load(email)
        if stakeholder and stakeholder.role:
            user_role = stakeholder.role

    return user_role


async def grant_group_level_role(
    loaders: Dataloaders,
    email: str,
    group_name: str,
    role: str,
    modified_by: str,
) -> None:
    if role not in get_group_level_roles_model():
        raise ValueError(f"Invalid role value: {role}")
    metadata = GroupAccessMetadataToUpdate(
        state=GroupAccessState(
            modified_by=modified_by,
            modified_date=datetime_utils.get_utc_now(),
            has_access=True,
            role=role,
        ),
    )
    group_access = await loaders.group_access.load(
        GroupAccessRequest(group_name=group_name, email=email),
    )
    if group_access:
        metadata = GroupAccessMetadataToUpdate(
            state=group_access.state._replace(
                modified_by=modified_by,
                modified_date=datetime_utils.get_utc_now(),
                has_access=True,
                role=role,
            ),
        )
        metadata = merge_group_access_changes(old_access=group_access, changes=metadata)
    await group_access_model.update_metadata(
        email=email,
        group_name=group_name,
        metadata=metadata,
    )
    # If there is no user-level role for this user add one
    if not await get_user_level_role(loaders, email):
        user_level_role: str = role if role in get_user_level_roles_model() else "user"
        await grant_user_level_role(email, user_level_role)


async def grant_organization_level_role(
    loaders: Dataloaders,
    email: str,
    organization_id: str,
    role: str,
    modified_by: str,
) -> None:
    if role not in get_organization_level_roles_model():
        raise ValueError(f"Invalid role value: {role}")

    loaders.organization_access.clear(
        OrganizationAccessRequest(organization_id=organization_id, email=email),
    )
    org_access = await loaders.organization_access.load(
        OrganizationAccessRequest(organization_id=organization_id, email=email),
    )
    await organization_access_model.update_metadata(
        email=email,
        organization_id=organization_id,
        metadata=OrganizationAccessMetadataToUpdate(
            state=OrganizationAccessState(
                has_access=True,
                modified_date=datetime_utils.get_utc_now(),
                modified_by=modified_by,
                role=role,
            )
            if org_access is None
            else org_access.state._replace(
                has_access=True,
                modified_date=datetime_utils.get_utc_now(),
                modified_by=modified_by,
                role=role,
            ),
        ),
    )
    # If there is no user-level role for this user add one
    if not await get_user_level_role(loaders, email):
        user_level_role: str = role if role in get_user_level_roles_model() else "user"
        await grant_user_level_role(email, user_level_role)


async def grant_user_level_role(email: str, role: str) -> None:
    if role not in get_user_level_roles_model():
        raise ValueError(f"Invalid role value: {role}")

    role = "hacker" if role == "user" and email.endswith(FLUID_IDENTIFIER) else role

    await stakeholders_model.update_metadata(
        email=email,
        metadata=StakeholderMetadataToUpdate(role=role),
    )


async def has_access_to_group(
    loaders: Dataloaders,
    email: str,
    group_name: str,
) -> bool:
    return bool(await get_group_level_role(loaders, email, group_name.lower()))
