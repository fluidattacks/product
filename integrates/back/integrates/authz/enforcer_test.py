from integrates.authz.enforcer import (
    get_group_level_enforcer,
    get_group_service_attributes_enforcer,
    get_organization_level_enforcer,
    get_user_level_enforcer,
)
from integrates.authz.model import (
    GROUP_LEVEL_ACTIONS,
    GROUP_LEVEL_ROLES,
    ORGANIZATION_LEVEL_ACTIONS,
    ORGANIZATION_LEVEL_ROLES,
    USER_LEVEL_ACTIONS,
    USER_LEVEL_ROLES,
)
from integrates.dataloaders import get_new_context
from integrates.db_model.groups.enums import (
    GroupManaged,
    GroupService,
    GroupStateStatus,
    GroupSubscriptionType,
)
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    GroupAccessFaker,
    GroupAccessStateFaker,
    GroupFaker,
    GroupStateFaker,
    OrganizationAccessFaker,
    OrganizationAccessStateFaker,
    OrganizationFaker,
    StakeholderFaker,
)
from integrates.testing.mocks import mocks
from integrates.testing.utils import parametrize

EMAIL = "test@company.com"
GROUP_NAME = "test_group"
ORG_ID = "ORG#3432232323r-32r2re2f322r2r2-3rf3233-e232f232f"


@parametrize(
    args=["role"],
    cases=[[role] for role in GROUP_LEVEL_ROLES],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id=ORG_ID)],
            groups=[
                GroupFaker(
                    name=GROUP_NAME,
                    organization_id=ORG_ID,
                    state=GroupStateFaker(service=GroupService.WHITE),
                ),
            ],
            stakeholders=[
                StakeholderFaker(email=f"{role}@company.com", role=role)
                for role in GROUP_LEVEL_ROLES
            ],
            group_access=[
                GroupAccessFaker(
                    email=f"{role}@company.com",
                    group_name=GROUP_NAME,
                    state=GroupAccessStateFaker(role=role),
                )
                for role in GROUP_LEVEL_ROLES
            ],
        ),
    )
)
async def test_group_level_enforcer(role: str) -> None:
    loaders = get_new_context()
    enforcer = await get_group_level_enforcer(loaders, f"{role}@company.com")
    allowed_actions = GROUP_LEVEL_ROLES[role]["actions"]

    for action in GROUP_LEVEL_ACTIONS:
        assert enforcer(GROUP_NAME, action) == (action in allowed_actions)


@parametrize(
    args=["role"],
    cases=[[role] for role in ORGANIZATION_LEVEL_ROLES],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id=ORG_ID)],
            stakeholders=[
                StakeholderFaker(email=f"{role}@company.com") for role in ORGANIZATION_LEVEL_ROLES
            ],
            organization_access=[
                OrganizationAccessFaker(
                    email=f"{role}@company.com",
                    organization_id=ORG_ID,
                    state=OrganizationAccessStateFaker(role=role),
                )
                for role in ORGANIZATION_LEVEL_ROLES
            ],
        ),
    )
)
async def test_organization_level_enforcer(role: str) -> None:
    loaders = get_new_context()
    enforcer = await get_organization_level_enforcer(loaders, f"{role}@company.com")
    allowed_actions = ORGANIZATION_LEVEL_ROLES[role]["actions"]

    for action in ORGANIZATION_LEVEL_ACTIONS:
        assert enforcer(ORG_ID, action) == (action in allowed_actions)


@parametrize(
    args=["service"],
    cases=[
        ["is_continuous"],
        ["can_request_zero_risk"],
        ["has_service_white"],
        ["can_report_vulnerabilities"],
        ["has_forces"],
        ["has_asm"],
        ["has_advanced"],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            groups=[
                GroupFaker(
                    name=GROUP_NAME,
                    organization_id=ORG_ID,
                    state=GroupStateFaker(
                        service=GroupService.WHITE,
                        has_advanced=True,
                        status=GroupStateStatus.ACTIVE,
                        type=GroupSubscriptionType.CONTINUOUS,
                        managed=GroupManaged.MANAGED,
                    ),
                ),
            ],
        ),
    )
)
async def test_group_service_attributes_enforcer(service: str) -> None:
    loaders = get_new_context()
    group = await loaders.group.load(GROUP_NAME)
    assert group
    enforcer = get_group_service_attributes_enforcer(group)
    assert enforcer(service) is True


@parametrize(
    args=["role"],
    cases=[[role] for role in USER_LEVEL_ROLES],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            stakeholders=[
                StakeholderFaker(
                    email=f"{role}@fluidattacks.com",
                    role=role,
                )
                for role in USER_LEVEL_ROLES
            ],
        ),
    )
)
async def test_user_level_enforcer(role: str) -> None:
    loaders = get_new_context()
    enforcer = await get_user_level_enforcer(loaders, f"{role}@fluidattacks.com")
    allowed_actions = USER_LEVEL_ROLES[role]["actions"]

    for action in USER_LEVEL_ACTIONS:
        assert enforcer(action) == (action in allowed_actions)
