from integrates.authz.policy import (
    get_group_level_role,
    get_user_level_role,
    grant_group_level_role,
    grant_user_level_role,
)
from integrates.dataloaders import get_new_context
from integrates.db_model.groups.enums import GroupService
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    EMAIL_FLUIDATTACKS_GROUP_MANAGER,
    GroupAccessFaker,
    GroupAccessStateFaker,
    GroupFaker,
    GroupStateFaker,
    OrganizationFaker,
    StakeholderFaker,
)
from integrates.testing.mocks import mocks
from integrates.testing.utils import parametrize

EMAIL = "jdoe@company.com"
GROUP_NAME = "group1"


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            groups=[
                GroupFaker(
                    name="group1",
                    organization_id="org1",
                    state=GroupStateFaker(service=GroupService.WHITE),
                ),
            ],
            stakeholders=[
                StakeholderFaker(
                    email=EMAIL,
                ),
            ],
            group_access=[
                GroupAccessFaker(
                    email=EMAIL,
                    group_name=GROUP_NAME,
                    state=GroupAccessStateFaker(role="group_manager"),
                ),
            ],
        ),
    )
)
async def test_get_group_level_role() -> None:
    loaders = get_new_context()

    assert await get_group_level_role(loaders, EMAIL, GROUP_NAME) == "group_manager"


@parametrize(
    args=["role"],
    cases=[
        ["hacker"],
        ["user"],
        ["group_manager"],
        ["vulnerability_manager"],
        ["reattacker"],
        ["resourcer"],
        ["reviewer"],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            groups=[
                GroupFaker(
                    name=GROUP_NAME,
                    organization_id="org1",
                    state=GroupStateFaker(service=GroupService.WHITE),
                ),
            ],
            stakeholders=[
                StakeholderFaker(
                    email=EMAIL_FLUIDATTACKS_GROUP_MANAGER,
                ),
                *[
                    StakeholderFaker(
                        email=f"{role}@company.com",
                    )
                    for role in [
                        "hacker",
                        "user",
                        "group_manager",
                        "vulnerability_manager",
                        "reattacker",
                        "resourcer",
                        "reviewer",
                    ]
                ],
            ],
            group_access=[
                GroupAccessFaker(
                    email=EMAIL_FLUIDATTACKS_GROUP_MANAGER,
                    group_name=GROUP_NAME,
                    state=GroupAccessStateFaker(role="group_manager"),
                ),
            ],
        ),
    )
)
async def test_grant_group_level_role(role: str) -> None:
    loaders = get_new_context()

    email = f"{role}@company.com"

    await grant_group_level_role(loaders, email, GROUP_NAME, role, EMAIL_FLUIDATTACKS_GROUP_MANAGER)

    loaders.group_access.clear_all()
    assert await get_user_level_role(loaders, email) == "user"
    assert await get_group_level_role(loaders, email, GROUP_NAME) == role


@parametrize(
    args=["role"],
    cases=[
        ["hacker"],
        ["user"],
        ["admin"],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            groups=[
                GroupFaker(
                    name="group1",
                    organization_id="org1",
                    state=GroupStateFaker(service=GroupService.WHITE),
                ),
            ],
            group_access=[
                GroupAccessFaker(
                    email=EMAIL,
                    group_name=GROUP_NAME,
                    state=GroupAccessStateFaker(role="group_manager"),
                ),
            ],
        ),
    )
)
async def test_grant_user_level_role(role: str) -> None:
    loaders = get_new_context()
    email = f"{role}@company.com"
    await grant_user_level_role(email, role)

    assert await get_user_level_role(loaders, email) == role
