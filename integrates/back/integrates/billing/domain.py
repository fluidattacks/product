import logging
import logging.config
import uuid
from datetime import (
    datetime,
)

import pytz
import stripe
from aioextensions import (
    collect,
)
from starlette.datastructures import (
    UploadFile,
)

from integrates.billing import (
    epayco_customers,
    stripe_customers,
)
from integrates.billing import (
    utils as billing_utils,
)
from integrates.billing.subscriptions import (
    treli,
)
from integrates.billing.types import (
    PaymentMethod,
    Price,
)
from integrates.billing.utils import (
    format_other_payment_methods,
    process_rut,
    process_tax_id,
)
from integrates.context import (
    FI_STRIPE_API_KEY,
)
from integrates.custom_exceptions import (
    CouldNotRemovePaymentMethod,
    GroupNotFound,
    InvalidBillingCustomer,
    InvalidBillingPaymentMethod,
    InvalidExpiryDateField,
    PaymentMethodAlreadyExists,
)
from integrates.custom_utils import (
    validations,
    validations_deco,
)
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model import (
    organizations as organizations_model,
)
from integrates.db_model.groups.types import (
    Group,
)
from integrates.db_model.organizations.types import (
    Organization,
    OrganizationBilling,
    OrganizationDocuments,
    OrganizationMetadataToUpdate,
    OrganizationPaymentMethods,
)
from integrates.organizations import (
    domain as orgs_domain,
)
from integrates.resources import (
    domain as resources_domain,
)
from integrates.s3 import (
    operations as s3_ops,
)
from integrates.settings import (
    LOGGING,
)
from integrates.tickets import (
    domain as tickets_domain,
)

logging.config.dictConfig(LOGGING)
LOGGER = logging.getLogger(__name__)

API_VERSION = "2024-06-20"

stripe.api_key = FI_STRIPE_API_KEY
stripe.api_version = API_VERSION


@validations_deco.validate_length_deco("business_name", max_length=60)
@validations_deco.validate_fields_deco(["business_name", "email"])
async def update_documents(
    *,
    org: Organization,
    payment_method_id: str,
    business_name: str,
    city: str,
    country: str,
    email: str,
    state: str,
    rut: UploadFile | None,
    tax_id: UploadFile | None,
) -> bool:
    documents = OrganizationDocuments()
    org_name = org.name.lower()
    business_name = business_name.lower()
    validations.validate_country_tax(country, rut, tax_id)

    if org.payment_methods:
        actual_payment_method = next(
            method for method in org.payment_methods if method.id == payment_method_id
        )
        if actual_payment_method.business_name.lower() != business_name:
            document_prefix = (
                f"billing/{org.name.lower()}/" + f"{actual_payment_method.business_name.lower()}"
            )
            file_name: str = ""
            if actual_payment_method.documents.rut:
                file_name = actual_payment_method.documents.rut.file_name
            if actual_payment_method.documents.tax_id:
                file_name = actual_payment_method.documents.tax_id.file_name

            await resources_domain.remove_file(f"{document_prefix}/{file_name}")

    if rut:
        documents = await process_rut(org_name, business_name, rut)
    if tax_id:
        documents = await process_tax_id(org_name, business_name, tax_id)

    return await update_other_payment_method(
        org=org,
        documents=documents,
        payment_method_id=payment_method_id,
        business_name=business_name,
        city=city,
        country=country,
        email=email,
        state=state,
    )


async def get_document_link(org: Organization, payment_id: str, file_name: str) -> str:
    org_name = org.name.lower()
    file_url = ""
    if org.payment_methods:
        payment_method = next(
            (method for method in org.payment_methods if method.id == payment_id),
            None,
        )
        if not payment_method:
            raise InvalidBillingPaymentMethod()

        business_name = payment_method.business_name.lower()
        file_url = f"billing/{org_name}/{business_name}/{file_name}"

    return await s3_ops.sign_url(
        f"resources/{file_url}",
        900,
    )


async def _list_credit_cards_info(
    *,
    billing_customer_id: str,
    organization_country: str,
    organization_name: str,
    limit: int = 100,
) -> list[PaymentMethod]:
    is_stripe_customer = billing_customer_id.startswith("cus_")

    if organization_country == "Colombia" and not is_stripe_customer:
        return await epayco_customers.list_customer_credit_cards(
            customer_id=billing_customer_id,
        )

    customer = await stripe.Customer.retrieve_async(
        billing_customer_id,
    )

    default_payment_method = (
        customer.invoice_settings.default_payment_method if customer.invoice_settings else None
    )

    cards = billing_utils.format_stripe_credit_card_info(
        await stripe_customers.get_customer_payment_methods(
            customer_id=customer.id,
            limit=limit,
        ),
        default_payment_method.id
        if isinstance(default_payment_method, stripe.PaymentMethod)
        else default_payment_method,
    )

    if organization_country == "Colombia":
        LOGGER.error(
            "organization from Colombia has customer registered on stripe",
            extra={
                "extra": {
                    "organization": organization_name,
                    "number_of_payment_methods": len(cards),
                },
            },
        )

    return cards


async def list_organization_payment_methods(
    *,
    org: Organization,
    limit: int = 100,
) -> list[PaymentMethod]:
    payment_methods: list[PaymentMethod] = []

    if org.billing_information is not None:
        _payment_methods = await collect(
            (
                _list_credit_cards_info(
                    billing_customer_id=billing_info.customer_id,
                    organization_country=org.country,
                    organization_name=org.name,
                    limit=limit,
                )
            )
            for billing_info in org.billing_information
        )

        payment_methods = [
            method for payment_method in _payment_methods for method in payment_method
        ]

    if org.payment_methods is not None:
        other_payment_methods = org.payment_methods

        payment_methods += format_other_payment_methods(other_payment_methods)

    return payment_methods


async def _add_billing_information_colombia(
    *,
    organization: Organization,
    payment_method_id: str,
    billed_groups: list[str],
    billing_email: str,
    business_id: str,
    business_name: str,
) -> tuple[int, str, str]:
    customer = await epayco_customers.create_customer(
        payment_method_id=payment_method_id,
        user_email=billing_email,
        business_id=business_id,
        business_name=business_name or organization.name,
    )

    subscription_id = await treli.create_organization_subscription(
        organization_name=organization.name,
        business_id=business_id,
        business_name=business_name or organization.name,
        customer=customer,
        email=billing_email,
        billed_groups=billed_groups,
    )

    card = epayco_customers.find_card_by_id(payment_method_id, customer.cards)

    last4 = card.last_four_digits if card else ""

    return subscription_id, customer.id, last4


async def _add_billing_information(
    *,
    organization: Organization,
    user_email: str,
    payment_method_id: str,
    country: str | None,
    billed_groups: list[Group],
    billing_email: str,
    business_id: str | None,
    business_name: str | None,
) -> bool:
    subscription_id = None

    group_names = [group.name for group in billed_groups]

    if country == "Colombia":
        if not business_id or not business_name:
            (
                _business_id,
                _business_name,
            ) = billing_utils.get_groups_business_id_and_name(billed_groups)

            _business_id = _business_id or "0000000000"
            _business_name = _business_name or organization.name

            business_id = business_id or _business_id
            business_name = business_name or _business_name

        (
            subscription_id,
            customer_id,
            last4,
        ) = await _add_billing_information_colombia(
            organization=organization,
            payment_method_id=payment_method_id,
            billed_groups=group_names,
            billing_email=billing_email,
            business_id=business_id,
            business_name=business_name,
        )

    else:
        customer_id = (
            await stripe.Customer.create_async(
                name=organization.name,
                email=billing_email,
            )
        ).id

        last4 = (
            await stripe_customers.attach_payment_method(
                payment_method_id=payment_method_id,
                org_billing_customer=customer_id,
            )
        ).last4

    await orgs_domain.add_billing_information(
        organization=organization,
        billing_information=OrganizationBilling(
            billing_email=billing_email,
            customer_id=customer_id,
            subscription_id=subscription_id,
            last_modified_by=user_email,
            modified_at=datetime.now(pytz.UTC),
            billed_groups=group_names,
        ),
        country=country if organization.country != country else None,
    )

    await billing_utils.send_payment_method_added_notification(
        org_name=organization.name,
        user_email=user_email,
        last4=last4,
    )

    return True


async def create_credit_card_payment_method(
    *,
    org: Organization,
    user_email: str,
    make_default: bool,
    payment_method_id: str,
    country: str | None,
    billed_groups: list[str] | None,
    billing_email: str | None,
    loaders: Dataloaders,
    business_name: str | None = None,
    business_id: str | None = None,
) -> bool:
    billing_emails = [
        billing_info.billing_email for billing_info in (org.billing_information or [])
    ]

    if not billing_email:
        billing_email = user_email

    org_groups: list[Group] = []

    if billed_groups:
        org_groups = [group for group in (await loaders.group.load_many(billed_groups)) if group]

    for group in org_groups:
        if group.organization_id != org.id:
            raise GroupNotFound()

    if billing_email in billing_emails:
        if country == "Colombia":
            last4 = (
                await epayco_customers.associate_card_with_customer(
                    organization=org,
                    make_default=make_default,
                    payment_method_id=payment_method_id,
                    billing_email=billing_email,
                )
            ).last_four_digits

        else:
            card = await stripe_customers.create_credit_card_payment_method(
                org=org,
                billing_email=billing_email,
                make_default=make_default,
                payment_method_id=payment_method_id,
            )

            last4 = card["last4"]

        await billing_utils.send_payment_method_added_notification(
            org_name=org.name,
            user_email=user_email,
            last4=last4,
        )

        return True

    return await _add_billing_information(
        organization=org,
        user_email=user_email,
        payment_method_id=payment_method_id,
        billing_email=billing_email,
        billed_groups=org_groups,
        country=country,
        business_id=business_id,
        business_name=business_name,
    )


@validations_deco.validate_length_deco("business_name", max_length=60)
@validations_deco.validate_fields_deco(["business_name"])
async def create_other_payment_method(
    *,
    org: Organization,
    user_email: str,
    business_name: str,
    city: str,
    country: str,
    email: str,
    state: str,
    rut: UploadFile | None = None,
    tax_id: UploadFile | None = None,
) -> bool:
    """Create other payment method and associate it to the organization"""
    await billing_utils.validate_legal_document(rut, tax_id)

    other_payment_id = str(uuid.uuid4())
    other_payment = OrganizationPaymentMethods(
        business_name=business_name,
        city=city,
        country=country,
        documents=OrganizationDocuments(),
        email=email,
        id=other_payment_id,
        state=state,
    )

    # Raise exception if payment method already exists for organization
    if org.payment_methods:
        if business_name in (
            payment_method.business_name for payment_method in org.payment_methods
        ):
            raise PaymentMethodAlreadyExists()
        org.payment_methods.append(other_payment)
    else:
        org = org._replace(
            payment_methods=[other_payment],
        )
    await organizations_model.update_metadata(
        metadata=OrganizationMetadataToUpdate(payment_methods=org.payment_methods),
        organization_id=org.id,
        organization_name=org.name,
    )
    await tickets_domain.request_other_payment_methods(
        business_legal_name=business_name,
        city=city,
        country=country,
        efactura_email=email,
        rut=rut,
        tax_id=tax_id,
        user_email=user_email,
    )
    return await update_documents(
        org=org,
        payment_method_id=other_payment_id,
        business_name=business_name,
        city=city,
        country=country,
        email=email,
        state=state,
        rut=rut,
        tax_id=tax_id,
    )


async def update_credit_card_payment_method(
    *,
    org: Organization,
    payment_method_id: str,
    card_expiration_month: int | None,
    card_expiration_year: int | None,
    make_default: bool,
) -> bool:
    if org.billing_information is None:
        raise InvalidBillingCustomer()

    payment_methods = await list_organization_payment_methods(
        org=org,
        limit=1000,
    )

    if payment_method_id not in (payment_method.id for payment_method in list(payment_methods)):
        raise InvalidBillingPaymentMethod()

    if org.country == "Colombia":
        return await epayco_customers.update_default_card(
            card_token_id=payment_method_id,
            customer_id=org.billing_information[0].customer_id,
            make_default=make_default,
            organization=org,
        )

    if not isinstance(card_expiration_month, int) or not isinstance(card_expiration_year, int):
        raise InvalidExpiryDateField()

    result = await stripe_customers.update_credit_card_info(
        payment_method_id=payment_method_id,
        card_expiration_month=card_expiration_month,
        card_expiration_year=card_expiration_year,
    )
    if make_default:
        result = result and await stripe_customers.update_default_payment_method(
            payment_method_id=payment_method_id,
            org_billing_customer=org.billing_information[0].customer_id,
        )

    return result


@validations_deco.validate_length_deco("business_name", max_length=60)
@validations_deco.validate_fields_deco(["business_name"])
async def update_other_payment_method(
    *,
    org: Organization,
    documents: OrganizationDocuments,
    payment_method_id: str,
    business_name: str,
    city: str,
    country: str,
    email: str,
    state: str,
) -> bool:
    # Raise exception if payment method does not belong to organization
    payment_methods = await list_organization_payment_methods(
        org=org,
        limit=1000,
    )
    if payment_method_id not in (payment_method.id for payment_method in list(payment_methods)):
        raise InvalidBillingPaymentMethod()

    # get actual payment methods
    other_payment_methods: list[OrganizationPaymentMethods] = []
    if org.payment_methods:
        other_payment_methods = org.payment_methods

    other_payment_methods = [
        method for method in other_payment_methods if method.id != payment_method_id
    ]
    other_payment_methods.append(
        OrganizationPaymentMethods(
            business_name=business_name,
            city=city,
            country=country,
            documents=documents,
            email=email,
            id=payment_method_id,
            state=state,
        ),
    )
    await organizations_model.update_metadata(
        metadata=OrganizationMetadataToUpdate(payment_methods=other_payment_methods),
        organization_id=org.id,
        organization_name=org.name,
    )
    return True


async def _remove_credit_card_payment_method(
    *,
    org: Organization,
    payment_method_id: str,
    all_organization_payment_methods: list[PaymentMethod],
) -> bool:
    if org.billing_information is None:
        raise InvalidBillingCustomer()

    remaining_cards = [
        method
        for method in all_organization_payment_methods
        if method.last_four_digits != "" and method.id != payment_method_id
    ]

    if not remaining_cards:
        raise CouldNotRemovePaymentMethod()

    if org.country == "Colombia":
        result = await epayco_customers.remove_card_token(
            organization=org,
            card_token_id=payment_method_id,
        )
    else:
        customers_payment_methods = await collect(
            (
                stripe_customers.get_customer_payment_methods(
                    customer_id=billing_info.customer_id,
                )
            )
            for billing_info in org.billing_information
        )

        payment_method_customer_id = next(
            (
                payment_method.customer.id
                if isinstance(payment_method.customer, stripe.Customer)
                else payment_method.customer
                for card_info in customers_payment_methods
                for payment_method in card_info
                if payment_method.id == payment_method_id
            ),
            None,
        )

        if not payment_method_customer_id:
            raise InvalidBillingPaymentMethod()

        await stripe_customers.update_default_payment_method(
            payment_method_id=remaining_cards[0].id,
            org_billing_customer=payment_method_customer_id,
        )
        result = (
            await stripe.PaymentMethod.detach_async(
                payment_method_id,
            )
        ).customer is None

    return result


async def _remove_other_payment_method(
    *,
    org: Organization,
    payment_method_id: str,
) -> bool:
    other_payment_methods: list[OrganizationPaymentMethods] = []
    if org.payment_methods:
        other_payment_methods = org.payment_methods

    payment_method_to_delete = next(
        payment_method
        for payment_method in other_payment_methods
        if payment_method_id == payment_method.id
    )
    business_name = payment_method_to_delete.business_name
    remaining_payment_methods = [
        payment_method
        for payment_method in other_payment_methods
        if payment_method.id != payment_method_id
    ]
    await organizations_model.update_metadata(
        metadata=OrganizationMetadataToUpdate(payment_methods=remaining_payment_methods),
        organization_id=org.id,
        organization_name=org.name,
    )
    document_prefix = f"billing/{org.name.lower()}/{business_name.lower()}"
    file_name: str = ""
    if payment_method_to_delete.documents.rut:
        file_name = payment_method_to_delete.documents.rut.file_name
    if payment_method_to_delete.documents.tax_id:
        file_name = payment_method_to_delete.documents.tax_id.file_name

    await resources_domain.remove_file(f"{document_prefix}/{file_name}")

    return True


async def remove_payment_method(
    *,
    org: Organization,
    payment_method_id: str,
) -> bool:
    payment_methods = await list_organization_payment_methods(
        org=org,
        limit=1000,
    )

    if len(payment_methods) == 1:
        raise CouldNotRemovePaymentMethod()

    payment_method_to_delete = next(
        (method for method in payment_methods if method.id == payment_method_id),
        None,
    )

    # Raise exception if payment method does not belong to organization
    if payment_method_to_delete is None:
        raise InvalidBillingPaymentMethod()

    # if it is not a credit card payment method
    if payment_method_to_delete.last_four_digits == "":
        return await _remove_other_payment_method(
            org=org,
            payment_method_id=payment_method_id,
        )

    if org.billing_information is None:
        raise InvalidBillingCustomer()

    return await _remove_credit_card_payment_method(
        org=org,
        payment_method_id=payment_method_id,
        all_organization_payment_methods=payment_methods,
    )


async def get_prices() -> dict[str, Price]:
    """Get model prices"""
    return await stripe_customers.get_prices()
