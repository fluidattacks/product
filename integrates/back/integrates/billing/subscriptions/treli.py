import base64
import json
import logging
import logging.config
from collections.abc import Awaitable, Callable
from functools import (
    wraps,
)
from typing import (
    ParamSpec,
    TypeVar,
    cast,
)

from aiohttp import (
    ClientSession,
)

from integrates.billing.subscriptions import (
    utils as subscriptions_utils,
)
from integrates.billing.types import (
    CustomerTokens,
    EPaycoCard,
    EPaycoCustomer,
    ProductDict,
    TreliSubscription,
    TreliTokensApiResponse,
)
from integrates.context import (
    FI_ENVIRONMENT,
    FI_TRELI_PASSWORD,
    FI_TRELI_USER_NAME,
)
from integrates.custom_exceptions import (
    CouldNotCreateSubscription,
    InvalidBillingPaymentMethod,
)
from integrates.settings import (
    LOGGING,
)

BASE_URL = "https://treli.co"
SUB_PATH = "/wp-json/api"

logging.config.dictConfig(LOGGING)
LOGGER = logging.getLogger(__name__)

P = ParamSpec("P")
R = TypeVar("R")


GATEWAY = "epaycodirect"


def _get_headers() -> Callable[[], dict[str, str]]:
    headers = None

    def _create_headers() -> dict[str, str]:
        nonlocal headers
        if headers is None:
            authorization = f"{FI_TRELI_USER_NAME}:{FI_TRELI_PASSWORD}"
            encode = base64.b64encode(authorization.encode("utf-8"))
            token = str(encode, "utf-8")
            headers = {
                "accept": "application/json",
                "content-type": "application/json",
                "Authorization": f"Basic {token}",
            }

        return headers

    return _create_headers


_get_credentials = _get_headers()


def use_headers(func: Callable[P, Awaitable[R]]) -> Callable[P, Awaitable[R]]:
    @wraps(func)
    async def decorated(*args: P.args, **kwargs: P.kwargs) -> R:
        if FI_ENVIRONMENT == "production":
            headers = _get_credentials()
            kwargs["headers"] = headers
        return await func(*args, **kwargs)

    return decorated


async def _format_subscription_data_to_post(
    *,
    payment_method_id: int,
    plan_id: str,
    business_id: str,
    business_name: str,
    organization_name: str,
    billed_groups: list[str],
    email: str,
) -> TreliSubscription.CreateTreliParamsDict:
    splitted_business_name = business_name.split(" ")
    name = " ".join(splitted_business_name[:-1])
    last_name = splitted_business_name[-1]

    billing_address = TreliSubscription.BillingAddressDict(
        first_name=name,
        last_name=last_name if last_name != name else "-",
        country="CO",
        company=organization_name,
        cedula=int(business_id),
        address_1="-----",
        city="Medellín",
        state="ANT",
        phone="1234567890",
    )
    payment_object = TreliSubscription.PaymentObjectDict(
        cardtoken=payment_method_id,
        gateway="epaycodirect",
        payment_method="card",
    )
    product = ProductDict(
        id=plan_id,
        quantity=1,
        subscription_period="month",
        subscription_period_interval=1,
        subscription_price=0,
    )

    return TreliSubscription.CreateTreliParamsDict(
        email=email,
        billing_address=billing_address,
        products=[product],
        currency="COP",
        payment=payment_object,
        meta_data=[
            TreliSubscription.MetadataDict(
                key="billed_groups",
                value=", ".join(billed_groups),
            ),
        ],
    )


async def _get_data_to_subscription(
    *,
    organization_name: str,
    business_id: str,
    business_name: str,
    billed_groups: list[str],
    token_id: int,
    email: str,
) -> TreliSubscription.CreateTreliParamsDict:
    plan_id = "761394" if FI_ENVIRONMENT == "production" else "753380"
    return await _format_subscription_data_to_post(
        payment_method_id=token_id,
        plan_id=plan_id,
        organization_name=organization_name,
        business_id=business_id,
        business_name=business_name,
        billed_groups=billed_groups,
        email=email,
    )


@use_headers
async def _create_subscription(
    session: ClientSession,
    *,
    data: TreliSubscription.CreateTreliParamsDict,
    headers: dict[str, str] | None = None,
) -> int:
    async with session.post(
        f"{SUB_PATH}/subscriptions/create",
        headers=headers,
        data=json.dumps(data),
    ) as response:
        if not response.ok:
            LOGGER.error(
                "Error creating subscription: %s",
                await response.text(),
            )
            raise CouldNotCreateSubscription(data["billing_address"]["company"])

        response_data = await cast(Awaitable[TreliSubscription.CreateResponse], response.json())
        if not bool(
            response_data.get("status") == "ok"
            and response_data.get("payment_status", "").lower() == "aprobado",
        ):
            raise InvalidBillingPaymentMethod()

        return response_data["subscription_ids"][0]


async def create_organization_subscription(
    *,
    business_id: str,
    business_name: str,
    organization_name: str,
    billed_groups: list[str],
    customer: EPaycoCustomer,
    email: str,
) -> int:
    card = next(
        (card for card in customer.cards if card.default),
        customer.cards[0] if customer.cards else None,
    )

    if card is None:
        raise InvalidBillingPaymentMethod()

    async with ClientSession(BASE_URL) as session:
        token_id = await _add_card_token(session, customer=customer, card=card)
        data = await _get_data_to_subscription(
            organization_name=organization_name,
            token_id=token_id,
            email=email,
            business_id=business_id,
            business_name=business_name,
            billed_groups=billed_groups,
        )
        response = await _create_subscription(session=session, data=data)
        return response


@use_headers
async def _add_card_token(
    session: ClientSession,
    *,
    customer: EPaycoCustomer,
    card: EPaycoCard,
    headers: dict[str, str] | None = None,
) -> int:
    token_info = {
        "email": customer.email,
        "cardTokenId": card.id,
        "customerId": customer.id,
        "last": card.last_four_digits,
        "ctype": card.franchise,
    }

    data = {
        "gateway": GATEWAY,
        "token_info": token_info,
    }

    async with session.post(
        f"{SUB_PATH}/cards/add-token",
        headers=headers,
        data=json.dumps(data),
    ) as response:
        if not response.ok:
            raise InvalidBillingPaymentMethod()

        post_response = await cast(Awaitable[dict[str, int]], response.json())

        return post_response["token_id"]


async def add_card_token_to_customer(customer: EPaycoCustomer, card: EPaycoCard) -> int:
    async with ClientSession(BASE_URL) as session:
        return await _add_card_token(session, customer=customer, card=card)


@use_headers
async def _update_subscription(
    session: ClientSession,
    *,
    update_data: TreliSubscription.UpdateData,
    headers: dict[str, str] | None = None,
) -> bool:
    if not update_data["subscription_id"]:
        raise ValueError("subscription_id is required")

    async with session.post(
        f"{SUB_PATH}/subscriptions/update",
        headers=headers,
        data=json.dumps({key: value for key, value in update_data.items() if value}),
    ) as response:
        if not response.ok:
            LOGGER.error(
                "Error updating subscription: %s",
                await response.text(),
            )
            return False

        response_data = await cast(Awaitable[dict[str, str]], response.json())
        return response_data.get("code") == "subs_updated"


async def update_subscription_payment_method(
    *,
    subscription_id: int,
    new_field: TreliSubscription.PaymentObjectDict,
) -> bool:
    async with ClientSession(BASE_URL) as session:
        return await _update_subscription(
            session=session,
            update_data=TreliSubscription.UpdateData(
                subscription_id=subscription_id,
                payment=new_field,
            ),
        )


async def update_subscription(
    *,
    data: TreliSubscription.UpdateData,
) -> bool:
    async with ClientSession(BASE_URL) as session:
        return await _update_subscription(
            session=session,
            update_data=data,
        )


async def list_tokens(customer_email: str) -> list[CustomerTokens]:
    async with ClientSession(BASE_URL) as session:
        endpoint_post_data: dict[str, str] = {
            "email": customer_email,
            "gateway": GATEWAY,
        }
        async with session.get(
            f"{SUB_PATH}/cards/get-tokens",
            headers=_get_credentials(),
            params=endpoint_post_data,
        ) as response:
            if response.ok:
                data = await cast(Awaitable[list[TreliTokensApiResponse]], response.json())
                return subscriptions_utils.format_treli_tokens(tokens=data)

            LOGGER.error(
                "Error getting tokens: %s",
                await response.text(),
            )
            raise ValueError(f"Error getting tokens for customer: {customer_email}")


@use_headers
async def update_subscription_status(
    *,
    subscription_id: int,
    action: TreliSubscription.action,
    headers: dict[str, str] | None = None,
) -> bool:
    expected_codes: dict[TreliSubscription.action, str] = {
        "activate": "subs_active",
        "pause": "subs_paused",
        "cancel": "subs_cancelled",
    }

    async with ClientSession(BASE_URL) as session:
        endpoint_post_data = TreliSubscription.ActionData(
            subscription_id=subscription_id,
            action=action,
        )

        async with session.post(
            f"{SUB_PATH}/subscriptions/actions",
            headers=headers,
            data=json.dumps(endpoint_post_data),
        ) as response:
            data = await cast(Awaitable[dict[str, str]], response.json())

            if not response.ok:
                LOGGER.error(
                    "Error updating subscription status: %s",
                    await response.text(),
                )

            if data.get("code") == "error":
                LOGGER.error(
                    "Error updating subscription status: %s",
                    data.get("message"),
                )

            return data.get("code") == expected_codes[action]
