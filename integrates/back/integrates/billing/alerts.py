from pydantic import (
    EmailStr,
)

from integrates.custom_exceptions import (
    OrganizationNotFound,
)
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model.reports.types import (
    Report,
)
from integrates.organization_access import (
    domain as organization_access_domain,
)
from integrates.tickets.domain import (
    create_ticket_for_billing_anomaly_alert,
)


async def notify_billing_alert(
    *,
    report: Report,
    requester_email: EmailStr,
) -> bool:
    loaders: Dataloaders = get_new_context()
    organization = await loaders.organization.load(str(report.entity_id))
    if not organization:
        raise OrganizationNotFound()

    customer_manager_email = await organization_access_domain.get_members_email_by_roles(
        loaders=loaders,
        organization_id=organization.id,
        roles={"customer_manager"},
    )

    await create_ticket_for_billing_anomaly_alert(
        report=report,
        organization_name=organization.name,
        requester_email=requester_email,
        customer_manager=customer_manager_email[0],
    )
    return True
