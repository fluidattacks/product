import base64
import json
import logging
import logging.config
from collections.abc import Awaitable, Callable
from functools import (
    wraps,
)
from typing import (
    Concatenate,
    ParamSpec,
    TypeVar,
    cast,
)

import aiohttp
from aioextensions import (
    collect,
)
from aiohttp import (
    ClientSession,
)

from integrates.billing import (
    utils as billing_utils,
)
from integrates.billing.subscriptions import (
    treli,
)
from integrates.billing.types import (
    EPayco,
    EPaycoCard,
    EPaycoCustomer,
    PaymentMethod,
    TreliSubscription,
)
from integrates.context import (
    FI_ENVIRONMENT,
    FI_EPAYCO_PUBLIC_KEY,
    FI_EPAYCO_SECRET_KEY,
)
from integrates.custom_exceptions import (
    CouldNotCreatePaymentMethod,
    CouldNotRemovePaymentMethod,
    InvalidAuthorization,
    InvalidBillingCustomer,
    InvalidBillingPaymentMethod,
)
from integrates.db_model.organizations.types import (
    Organization,
)
from integrates.settings import (
    LOGGING,
)

T = TypeVar("T", EPaycoCard, PaymentMethod)
R = TypeVar("R")
P = ParamSpec("P")

LOGGER = logging.getLogger(__name__)

logging.config.dictConfig(LOGGING)

COMMON_HEADERS = {
    "Content-Type": "application/json",
    "Accept": "application/json",
}

BASE_URL = "https://apify.epayco.co"


def _get_headers() -> Callable[[ClientSession], Awaitable[dict[str, str]]]:
    current_token: str | None = None

    async def _access_token(session: ClientSession) -> dict[str, str]:
        nonlocal current_token

        if current_token:
            return {
                **COMMON_HEADERS,
                "Authorization": f"Bearer {current_token}",
            }

        text = f"{FI_EPAYCO_PUBLIC_KEY}:{FI_EPAYCO_SECRET_KEY}"
        encode = base64.b64encode(text.encode("utf-8"))
        token = str(encode, "utf-8")
        authorization = f"Basic {token}"
        headers = {**COMMON_HEADERS, "Authorization": authorization}
        async with session.post(
            "/login",
            headers=headers,
        ) as response:
            content = await cast(Awaitable[EPayco.LoginResponse], response.json())
            if content and content.get("token"):
                current_token = content["token"]

        if current_token is None:
            LOGGER.error(
                "Could not get access token",
                extra={"extra": {"error_description": content.get("textResponse")}},
            )
            raise InvalidAuthorization()

        return {
            **COMMON_HEADERS,
            "Authorization": f"Bearer {current_token}",
        }

    return _access_token


_get_access_token = _get_headers()


def use_access_token(
    func: Callable[Concatenate[ClientSession, P], Awaitable[R]],
) -> Callable[Concatenate[ClientSession, P], Awaitable[R]]:
    @wraps(func)
    async def decorated(session: ClientSession, /, *args: P.args, **kwargs: P.kwargs) -> R:
        if FI_ENVIRONMENT == "production":
            headers = await _get_access_token(session)
            kwargs["headers"] = headers

        return await func(session, *args, **kwargs)

    return decorated


async def create_customer(
    *,
    business_id: str,
    business_name: str,
    payment_method_id: str,
    user_email: str,
) -> EPaycoCustomer:
    async with aiohttp.ClientSession(BASE_URL) as session:
        customer_id = await tokenize_customer(
            session,
            card_token_id=payment_method_id,
            business_name=business_name,
            business_id=business_id,
            user_email=user_email,
        )

        customer = await _get_customer_epayco(session, customer_id=customer_id)

    if not customer:
        raise CouldNotCreatePaymentMethod()

    return customer


def find_card_by_id(card_id: str, cards: list[T]) -> T | None:
    return next(
        (card for card in cards if card.id == card_id),
        None,
    )


def _find_customer_and_card(
    card_id: str,
    customers: tuple[EPaycoCustomer, ...],
) -> tuple[EPaycoCustomer, EPaycoCard]:
    for customer in customers:
        card = find_card_by_id(card_id, list(customer.cards))
        if card:
            return customer, card

    raise InvalidBillingPaymentMethod()


@use_access_token
async def _associate_token_with_customer(
    session: ClientSession,
    *,
    card_token_id: str,
    customer_id: str,
    headers: dict[str, str] | None = None,
) -> bool:
    data = {"cardToken": card_token_id, "customerId": customer_id}

    async with session.post(
        "/subscriptions/customer/add/new/token",
        headers=headers,
        data=json.dumps(data),
    ) as response:
        response_data = await cast(Awaitable[EPayco.RequestJsonResponse], response.json())
        if not response.ok or not response_data.get("success"):
            LOGGER.error(
                "Could not associate card with customer",
                extra={"extra": {"error_description": response_data["textResponse"]}},
            )
            raise CouldNotCreatePaymentMethod()

        return True


@use_access_token
async def _remove_card_from_customer(
    session: ClientSession,
    *,
    card_token_id: str,
    customer: EPaycoCustomer,
    headers: dict[str, str] | None = None,
) -> bool:
    card_to_remove = find_card_by_id(card_token_id, list(customer.cards))

    if not card_to_remove:
        raise InvalidBillingPaymentMethod()

    data = {
        "franchise": card_to_remove.franchise,
        "mask": card_to_remove.mask,
        "customerId": customer.id,
    }

    async with session.post(
        "/subscription/token/card/delete",
        headers=headers,
        data=json.dumps(data),
    ) as remove_response:
        content = await cast(Awaitable[EPayco.RequestJsonResponse], remove_response.json())
        if remove_response.ok and content.get("success"):
            return content["success"]

        LOGGER.error(
            "Could not remove card",
            extra={"extra": {"error_description": content["textResponse"]}},
        )

        raise CouldNotRemovePaymentMethod()


async def associate_card_with_customer(
    *,
    organization: Organization,
    make_default: bool,
    payment_method_id: str,
    billing_email: str,
) -> EPaycoCard:
    if organization.billing_information is None:
        raise InvalidBillingCustomer()

    billing_info = {
        info.customer_id: info.subscription_id for info in organization.billing_information
    }

    async with aiohttp.ClientSession(BASE_URL) as session:
        customers: tuple[EPaycoCustomer, ...] = await collect(
            tuple(
                _get_customer_epayco(session, customer_id=customer_id)
                for customer_id in billing_info
            ),
        )

        customer = next(
            (customer for customer in customers if customer.email == billing_email),
            None,
        )

        if not customer:
            raise InvalidBillingCustomer()

        success = await _associate_token_with_customer(
            session,
            card_token_id=payment_method_id,
            customer_id=customer.id,
        )

        if not success:
            raise CouldNotCreatePaymentMethod()

        if make_default:
            await use_credit_card_as_default(
                session,
                card_token_id=payment_method_id,
                customer_id=customer.id,
            )

        _customer = await _get_customer_epayco(session, customer_id=customer.id)

        card = find_card_by_id(payment_method_id, _customer.cards)

    if not card:
        raise CouldNotCreatePaymentMethod()

    token_id_treli = await treli.add_card_token_to_customer(
        customer=customer,
        card=card,
    )
    new_field = TreliSubscription.PaymentObjectDict(
        cardtoken=token_id_treli,
        gateway="epaycodirect",
        payment_method="card",
    )
    if subs_id := billing_info.get(customer.id):
        await treli.update_subscription_payment_method(
            subscription_id=subs_id,
            new_field=new_field,
        )

    return card


@use_access_token
async def tokenize_customer(
    session: ClientSession,
    *,
    business_name: str,
    business_id: str,
    card_token_id: str,
    user_email: str,
    headers: dict[str, str] | None = None,
) -> str:
    client_data: dict[str, str | bool] = {
        "docType": "Nit",
        "docNumber": business_id,
        "cardTokenId": card_token_id,
        "email": user_email,
        "lastName": "-",
        "name": business_name,
        "cellPhone": "0000000000",
        "phone": "0000000000",
        "requireCardToken": True,
    }

    async with session.post(
        "/token/customer",
        headers=headers,
        data=json.dumps(client_data),
    ) as customer_response:
        content = await cast(Awaitable[EPaycoCustomer.CreateResponse], customer_response.json())
        data = content.get("data")
        if data and data.get("data") and data["data"].get("customerId"):
            return content["data"]["data"]["customerId"]

        LOGGER.error(
            "Could not tokenize customer",
            extra={
                "extra": {
                    "error_description": content,
                },
            },
        )

        raise CouldNotCreatePaymentMethod()


def format_cards_info(cards: list[EPaycoCard.CardInfo]) -> list[EPaycoCard]:
    return [
        EPaycoCard(
            last_four_digits=card["mask"][-4:],
            franchise=card["franchise"],
            id=card["token"],
            default=card["default"],
            mask=card["mask"],
        )
        for card in cards
    ]


@use_access_token
async def _get_customer_epayco(
    session: ClientSession,
    *,
    customer_id: str,
    headers: dict[str, str] | None = None,
) -> EPaycoCustomer:
    client_data = {
        "customerId": customer_id,
    }

    async with session.post(
        "/subscriptions/customer",
        headers=headers,
        data=json.dumps(client_data),
    ) as customer_response:
        content = await cast(Awaitable[EPaycoCustomer.GetResponse], customer_response.json())
        if content and content.get("success"):
            customer = content["data"]["data"]
            cards = format_cards_info(customer["cards"])
            return EPaycoCustomer(
                id=customer["id_customer"],
                email=customer["email"],
                cards=cards,
                created=customer.get("created", ""),
            )

        LOGGER.error(
            "Could not get customer",
            extra={"extra": {"error_description": content["textResponse"]}},
        )

        raise InvalidBillingCustomer()


@use_access_token
async def use_credit_card_as_default(
    session: ClientSession,
    *,
    card_token_id: str,
    customer_id: str,
    headers: dict[str, str] | None = None,
) -> bool:
    customer = await _get_customer_epayco(session, customer_id=customer_id)

    card = find_card_by_id(card_token_id, list(customer.cards))

    if not card:
        raise InvalidBillingPaymentMethod()

    data = {
        "customerId": customer_id,
        "cardToken": card_token_id,
        "mask": card.mask,
        "franchise": card.franchise,
    }

    async with session.post(
        "/subscriptions/customer/add/new/token/default",
        headers=headers,
        data=json.dumps(data),
    ) as customer_response:
        content = await cast(
            Awaitable[EPaycoCard.SetAsDefaultResponse],
            customer_response.json(),
        )
        return content["success"]


async def list_customer_credit_cards(customer_id: str) -> list[PaymentMethod]:
    async with aiohttp.ClientSession(BASE_URL) as session:
        customer = await _get_customer_epayco(session, customer_id=customer_id)
        return billing_utils.format_credit_cards_info_epayco(customer.cards)


async def remove_card_token(*, card_token_id: str, organization: Organization) -> bool:
    if not organization.billing_information:
        raise InvalidBillingCustomer()

    billing_info = {
        info.customer_id: info.subscription_id for info in organization.billing_information
    }

    async with aiohttp.ClientSession(BASE_URL) as session:
        customers: tuple[EPaycoCustomer, ...] = await collect(
            tuple(
                _get_customer_epayco(session, customer_id=customer_id)
                for customer_id in billing_info
            ),
        )

        customer, card_to_remove = _find_customer_and_card(card_token_id, customers)

        remaining_cards = [card for card in customer.cards if card.id != card_token_id]

        if card_to_remove.default and remaining_cards:
            await use_credit_card_as_default(
                session,
                card_token_id=remaining_cards[0].id,
                customer_id=customer.id,
            )

        removed = await _remove_card_from_customer(
            session,
            card_token_id=card_token_id,
            customer=customer,
        )

        subscription_id = billing_info[customer.id]

        if removed and not remaining_cards and subscription_id:
            await treli.update_subscription_status(
                subscription_id=subscription_id,
                action="pause",
            )

        return removed


async def update_default_card(
    *,
    card_token_id: str,
    customer_id: str,
    make_default: bool,
    organization: Organization,
) -> bool:
    # default is the only thing that can be updated
    if not make_default or not organization.billing_information:
        return False

    async with aiohttp.ClientSession(BASE_URL) as session:
        customer = await _get_customer_epayco(session, customer_id=customer_id)
        card = find_card_by_id(card_token_id, list(customer.cards))
        if not card:
            raise InvalidBillingPaymentMethod()

        updated = await use_credit_card_as_default(
            session,
            card_token_id=card_token_id,
            customer_id=customer_id,
        )

    # Update default card on treli
    if updated:
        customer_tokens_on_treli = await treli.list_tokens(
            customer_email=customer.email,
        )
        card_token_id_treli = next(
            (
                token.token_id
                for token in customer_tokens_on_treli
                if token.card_token_id == card_token_id
            ),
            None,
        )

        if not card_token_id_treli:
            LOGGER.error(
                "Could not find card token on treli",
                extra={
                    "extra": {
                        "last4": card.last_four_digits,
                        "customer_email": customer.email,
                    },
                },
            )
            return False

        new_field = TreliSubscription.PaymentObjectDict(
            cardtoken=card_token_id_treli,
            gateway="epaycodirect",
            payment_method="card",
        )
        if subs_id := organization.billing_information[0].subscription_id:
            await treli.update_subscription_payment_method(
                subscription_id=subs_id,
                new_field=new_field,
            )

    return updated
