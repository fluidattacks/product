import logging
import logging.config

from starlette.datastructures import (
    UploadFile,
)
from stripe import (
    PaymentMethod as StripePaymentMethod,
)

from integrates.context import (
    FI_MAIL_FINANCE,
)
from integrates.custom_exceptions import (
    InvalidFileSize,
    InvalidFileType,
)
from integrates.custom_utils import (
    datetime as datetime_utils,
)
from integrates.custom_utils import (
    files as files_utils,
)
from integrates.custom_utils import (
    validations_deco,
)
from integrates.dataloaders import (
    get_new_context,
)
from integrates.db_model.groups.types import (
    Group,
)
from integrates.db_model.organizations.types import (
    DocumentFile,
    OrganizationDocuments,
    OrganizationPaymentMethods,
)
from integrates.mailer.common import (
    send_mails_async,
)
from integrates.resources import (
    domain as resources_domain,
)
from integrates.settings import (
    LOGGING,
)

from .types import (
    EPaycoCard,
    PaymentMethod,
)

logging.config.dictConfig(LOGGING)

LOGGER = logging.getLogger(__name__)


@validations_deco.validate_file_name_deco("file.filename")
@validations_deco.validate_fields_deco(["file.content_type"])
@validations_deco.validate_sanitized_csv_input_deco(["file.filename", "file.content_type"])
async def validate_file(file: UploadFile) -> None:
    mib = 1048576
    allowed_mimes = [
        "image/gif",
        "image/jpeg",
        "image/png",
        "application/pdf",
    ]
    if not await files_utils.assert_uploaded_file_mime(file, allowed_mimes):
        raise InvalidFileType("TAX_ID")

    if await files_utils.get_file_size(file) > 10 * mib:
        raise InvalidFileSize()


async def validate_legal_document(rut: UploadFile | None, tax_id: UploadFile | None) -> None:
    if rut:
        await validate_file(file=rut)
    if tax_id:
        await validate_file(file=tax_id)


def document_extension(document: UploadFile) -> str:
    extension = {
        "image/gif": ".gif",
        "image/jpeg": ".jpg",
        "image/png": ".png",
        "application/pdf": ".pdf",
        "application/zip": ".zip",
        "text/csv": ".csv",
        "text/plain": ".txt",
    }.get(document.headers.get("content-type", ""), "")

    return extension


async def save_document_file(org_name: str, business_name: str, document: UploadFile) -> str:
    document_file_name = f"{org_name}-{business_name}{document_extension(document)}"
    document_full_name = f"billing/{org_name}/{business_name}/{document_file_name}"
    await resources_domain.save_file(file_object=document, file_name=document_full_name)
    return document_file_name


async def process_rut(org_name: str, business_name: str, rut: UploadFile) -> OrganizationDocuments:
    rut_file_name = await save_document_file(org_name, business_name, rut)
    return OrganizationDocuments(
        rut=DocumentFile(
            file_name=rut_file_name,
            modified_date=datetime_utils.get_utc_now(),
        ),
    )


async def process_tax_id(
    org_name: str,
    business_name: str,
    tax_id: UploadFile,
) -> OrganizationDocuments:
    tax_id_file_name = await save_document_file(org_name, business_name, tax_id)
    return OrganizationDocuments(
        tax_id=DocumentFile(
            file_name=tax_id_file_name,
            modified_date=datetime_utils.get_utc_now(),
        ),
    )


def format_stripe_credit_card_info(
    stripe_payment_methods: list[StripePaymentMethod],
    default_payment_method: str | None,
) -> list[PaymentMethod]:
    return [
        PaymentMethod(
            id=payment_method.id,
            fingerprint=payment_method.card.fingerprint or "",
            last_four_digits=payment_method.card.last4,
            expiration_month=str(payment_method.card.exp_month),
            expiration_year=str(payment_method.card.exp_year),
            brand=payment_method.card.brand,
            default=payment_method.id == default_payment_method,
            business_name="",
            city="",
            country="",
            email="",
            state="",
            rut=None,
            tax_id=None,
        )
        for payment_method in stripe_payment_methods
        if payment_method.card
    ]


def format_other_payment_methods(
    other_payment_methods: list[OrganizationPaymentMethods],
) -> list[PaymentMethod]:
    return [
        PaymentMethod(
            id=other_method.id,
            fingerprint="",
            last_four_digits="",
            expiration_month="",
            expiration_year="",
            brand="",
            default=False,
            business_name=other_method.business_name,
            city=other_method.city,
            country=other_method.country,
            email=other_method.email,
            state=other_method.state,
            rut=DocumentFile(
                file_name=other_method.documents.rut.file_name,
                modified_date=other_method.documents.rut.modified_date,
            )
            if other_method.documents.rut
            else None,
            tax_id=DocumentFile(
                file_name=other_method.documents.tax_id.file_name,
                modified_date=other_method.documents.tax_id.modified_date,
            )
            if other_method.documents.tax_id
            else None,
        )
        for other_method in other_payment_methods
    ]


def format_credit_cards_info_epayco(
    customer_cards: list[EPaycoCard],
) -> list[PaymentMethod]:
    return [
        PaymentMethod(
            id=card.id,
            fingerprint=card.mask,
            last_four_digits=card.last_four_digits,
            expiration_month="",
            expiration_year="",
            brand=card.franchise,
            default=card.default,
            business_name="",
            city="",
            country="Colombia",
            email="",
            state="",
            rut=None,
            tax_id=None,
        )
        for card in customer_cards
    ]


async def send_payment_method_added_notification(
    *,
    org_name: str,
    user_email: str,
    last4: str,
) -> None:
    context = {
        "date": datetime_utils.get_as_str(datetime_utils.get_now(), "%Y-%m-%d %H:%M:%S %Z"),
        "last_digits": last4,
        "organization": org_name,
        "responsible": user_email,
    }
    await send_mails_async(
        loaders=get_new_context(),
        email_to=[FI_MAIL_FINANCE],
        context=context,
        subject=f"New payment method added in [{org_name}]",
        template_name="credit_card_added",
    )


def get_groups_business_id_and_name(
    groups: list[Group],
) -> tuple[str | None, str | None]:
    business_id: set[str] = set()
    business_name: set[str] = set()

    for group in groups:
        if group.business_id:
            business_id.add(group.business_id.replace(".", "").replace("-", "").strip())
        if group.business_name:
            business_name.add(group.business_name.title().strip())

    if len(business_id) > 1 or len(business_name) > 1:
        LOGGER.error(
            "Different business_id or business_name found in groups %s",
            groups,
            extra={
                "extra": {
                    "business_id": business_id,
                    "business_name": business_name,
                    "organization": groups[0].organization_id if groups else "",
                },
            },
        )

    return (
        business_id.pop() if business_id else None,
        business_name.pop() if business_name else None,
    )
