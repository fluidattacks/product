from integrates import batch, jobs_orchestration
from integrates.batch.dal.get import get_actions_by_name
from integrates.batch.enums import Action, SkimsBatchQueue
from integrates.dataloaders import Dataloaders, get_new_context
from integrates.db_model.credentials.types import HttpsSecret
from integrates.db_model.groups.enums import GroupService
from integrates.db_model.roots.enums import RootCloningStatus, RootDockerImageStateStatus
from integrates.schedulers import sbom_queue_group_images
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    EMAIL_GENERIC,
    CredentialsFaker,
    GitRootCloningFaker,
    GitRootFaker,
    GitRootStateFaker,
    GroupFaker,
    GroupStateFaker,
    OrganizationFaker,
    RootDockerImageFaker,
    RootDockerImageStateFaker,
)
from integrates.testing.mocks import Mock, mocks


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            groups=[
                GroupFaker(
                    name="group1",
                    organization_id="org1",
                    state=GroupStateFaker(service=GroupService.WHITE),
                ),
                GroupFaker(
                    name="group2",
                    organization_id="org1",
                    state=GroupStateFaker(service=GroupService.WHITE),
                ),
                GroupFaker(
                    name="group3",
                    organization_id="org1",
                    state=GroupStateFaker(has_essential=False),
                ),
            ],
            roots=[
                GitRootFaker(
                    group_name="group1",
                    id="root1",
                    cloning=GitRootCloningFaker(status=RootCloningStatus.OK),
                    state=GitRootStateFaker(nickname="root1"),
                ),
            ],
            docker_images=[
                RootDockerImageFaker(
                    uri="image1",
                    group_name="group1",
                    root_id="root1",
                    state=RootDockerImageStateFaker(credential_id="test-credential"),
                ),
                RootDockerImageFaker(
                    uri="image2",
                    group_name="group1",
                    root_id="root1",
                    state=RootDockerImageStateFaker(
                        credential_id="test-credential",
                        digest=(
                            "sha256:7c2c9c72b3c65b9881c2a3ff19c8b3c43b6a0d925602cbf9033467b0b7f6f9b132"
                        ),
                    ),
                ),
                RootDockerImageFaker(
                    uri="image3",
                    group_name="group1",
                    root_id="root1",
                    state=RootDockerImageStateFaker(status=RootDockerImageStateStatus.DELETED),
                ),
            ],
            credentials=[
                CredentialsFaker(
                    credential_id="test-credential",
                    organization_id="ORG#org1",
                    secret=HttpsSecret(user=EMAIL_GENERIC, password="pass"),  # noqa: S106
                ),
            ],
        ),
    ),
    others=[
        Mock(
            sbom_queue_group_images,
            "get_image_manifest",
            "async",
            (
                {
                    "Digest": "sha256:fe6a5f3f5555b3c6bfb5b6e0e"
                    "7639db8b8c2272d0848f214d7340d92521bf42d",
                }
            ),
        ),
        Mock(
            jobs_orchestration.jobs,
            "generate_batch_action_config_for_image",
            "async",
            (
                {
                    "images": [
                        {
                            "image_ref": "image1",
                            "root_nickname": "testroot",
                        },
                        {
                            "image_ref": "image2",
                            "root_nickname": "testroot",
                        },
                    ],
                    "images_config_files": ["image1.yaml", "image2.yaml"],
                    "sbom_format": "fluid-json",
                    "job_type": "scheduler",
                    "notification_id_map": None,
                },
                "123456",
                SkimsBatchQueue.SMALL,
            ),
        ),
        Mock(batch.dal.put, "put_action_to_batch", "async", "123456"),
    ],
)
async def test_sbom_queue_group_images() -> None:
    """Testing auxiliary functions of scheduler"""
    loaders: Dataloaders = get_new_context()

    groups = await sbom_queue_group_images.get_machine_groups(loaders)

    assert groups == ["group1", "group2"]

    images_to_execute = await sbom_queue_group_images.get_images_to_execute(loaders, "group1")
    image_uris = [image.uri for image in images_to_execute]
    assert image_uris == ["image1", "image2"]

    await sbom_queue_group_images.main()

    pending_executions = await get_actions_by_name(
        action_name=Action.GENERATE_IMAGE_SBOM,
        entity="group1",
    )
    assert len(pending_executions) == 1


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(id="org1"),
            ],
            groups=[
                GroupFaker(
                    name="group1",
                    organization_id="org1",
                    state=GroupStateFaker(service=GroupService.WHITE),
                ),
            ],
            roots=[
                GitRootFaker(
                    group_name="group1",
                    id="root1",
                    cloning=GitRootCloningFaker(status=RootCloningStatus.OK),
                    state=GitRootStateFaker(
                        nickname="root1",
                    ),
                ),
            ],
            docker_images=[
                RootDockerImageFaker(
                    uri="image2",
                    group_name="group1",
                    root_id="root1",
                    state=RootDockerImageStateFaker(
                        digest=(
                            "sha256:7c2c9c72b3c65b9881c2a3ff19c8b3c43b6a0d925602cbf9033467b0b7f6f9b132"
                        ),
                    ),
                ),
            ],
        ),
    ),
    others=[
        Mock(
            sbom_queue_group_images,
            "get_image_manifest",
            "async",
            {"Digest": "sha256:7c2c9c72b3c65b9881c2a3ff19c8b3c43b6a0d925602cbf9033467b0b7f6f9b132"},
        )
    ],
)
async def test_sbom_queue_group_images_already_updadated() -> None:
    """Testing not enqueueing when the digest is already updated"""
    await sbom_queue_group_images.main()

    pending_executions = await get_actions_by_name(
        action_name=Action.GENERATE_IMAGE_SBOM,
        entity="group1",
    )
    assert len(pending_executions) == 0
