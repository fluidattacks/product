from integrates import batch
from integrates.batch.dal.get import get_actions_by_name
from integrates.batch.enums import Action
from integrates.dataloaders import Dataloaders, get_new_context
from integrates.db_model.groups.enums import GroupService
from integrates.db_model.roots.enums import RootCloningStatus, RootStatus
from integrates.schedulers import clone_roots_that_have_not_cloned
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    GitRootCloningFaker,
    GitRootFaker,
    GitRootStateFaker,
    GroupFaker,
    GroupStateFaker,
    OrganizationFaker,
)
from integrates.testing.mocks import Mock, mocks

MACHINE_EMAIL = "machine@fluidattacks.com"


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            groups=[
                GroupFaker(
                    name="group1",
                    organization_id="org1",
                    state=GroupStateFaker(service=GroupService.WHITE),
                ),
                GroupFaker(
                    name="group2",
                    organization_id="org1",
                    state=GroupStateFaker(service=GroupService.WHITE),
                ),
                GroupFaker(
                    name="group3",
                    organization_id="org1",
                    state=GroupStateFaker(has_essential=False),
                ),
            ],
            roots=[
                GitRootFaker(
                    group_name="group1",
                    id="root1",
                    cloning=GitRootCloningFaker(status=RootCloningStatus.OK),
                    state=GitRootStateFaker(nickname="root1"),
                ),
                GitRootFaker(
                    group_name="group1",
                    id="root2",
                    cloning=GitRootCloningFaker(status=RootCloningStatus.OK),
                    state=GitRootStateFaker(nickname="root2", branch="test", use_vpn=True),
                ),
                GitRootFaker(
                    group_name="group1",
                    id="root3",
                    cloning=GitRootCloningFaker(status=RootCloningStatus.OK),
                    state=GitRootStateFaker(nickname="root3", branch="qa", use_ztna=True),
                ),
                GitRootFaker(
                    group_name="group1",
                    id="root4",
                    cloning=GitRootCloningFaker(status=RootCloningStatus.OK),
                    state=GitRootStateFaker(
                        nickname="root3", branch="qa_2", status=RootStatus.INACTIVE
                    ),
                ),
                GitRootFaker(
                    group_name="group1",
                    id="root5",
                    state=GitRootStateFaker(nickname="root4", branch="qa_3"),
                ),
            ],
        ),
    ),
    others=[Mock(batch.dal.put, "put_action_to_batch", "async", "123456")],
)
async def test_clone_roots_that_have_not_cloned() -> None:
    """Testing auxiliary functions of scheduler"""
    loaders: Dataloaders = get_new_context()

    groups = await clone_roots_that_have_not_cloned.get_machine_groups(loaders)

    assert groups == ["group1", "group2"]

    roots_to_execute = await clone_roots_that_have_not_cloned.get_roots_to_execute(
        loaders, "group1"
    )
    assert roots_to_execute == (["root1", "root3"], ["root2"])

    await clone_roots_that_have_not_cloned.main()

    pending_executions = await get_actions_by_name(
        action_name=Action.CLONE_ROOTS,
        entity="group1",
    )
    assert len(pending_executions) == 1
    clone_execution = pending_executions[0]
    assert clone_execution.additional_info == {
        "git_root_ids": ["root1", "root3"],
        "should_queue_machine": False,
        "should_queue_sbom": False,
    }
