from aioextensions import (
    collect,
)
from cloudflare import (
    AsyncCloudflare,
)

from integrates.context import (
    BASE_URL,
    FI_CLOUDFLARE_ACCOUNT_ID,
    FI_CLOUDFLARE_API_TOKEN,
)
from integrates.custom_utils.datetime import (
    get_as_str,
    get_now,
)
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model.organizations.get import (
    get_all_organizations,
)
from integrates.db_model.organizations.types import (
    Organization,
)
from integrates.db_model.roots.enums import (
    RootStatus,
)
from integrates.db_model.roots.types import (
    GitRoot,
)
from integrates.mailer.groups import (
    send_mail_ztna_down_alert,
)
from integrates.mailer.types import (
    ZTNAAlertContext,
)
from integrates.mailer.utils import (
    get_organization_emails_by_notification,
)
from integrates.organizations.domain import (
    get_all_active_group_names,
    get_group_names,
)
from integrates.organizations.utils import (
    get_organization,
)
from integrates.schedulers.common import (
    info,
)


async def get_tunnels_with_down_status(
    organization_names: list[str],
    number_of_roots: list[int],
) -> list[tuple[str, int, str]]:
    cloudflare = AsyncCloudflare(api_token=FI_CLOUDFLARE_API_TOKEN)
    down_tunnels = []
    tunnels = await cloudflare.zero_trust.tunnels.list(account_id=FI_CLOUDFLARE_ACCOUNT_ID)
    for tunnel in tunnels.result:
        if tunnel.name is None:
            continue

        index = (
            organization_names.index(tunnel.name.lower())
            if tunnel.name.lower() in organization_names
            else -1
        )
        if index >= 0 and tunnel.status == "down":
            down_tunnels.append(
                (
                    tunnel.name.lower(),
                    number_of_roots[index],
                    tunnel.status,
                ),
            )

    return list({down[0]: down for down in down_tunnels}.values())


async def get_number_of_ztna(
    *,
    organization: Organization,
    loaders: Dataloaders,
    all_group_names: set[str],
) -> int:
    organization_group_names = await get_group_names(loaders, organization.id)
    organization_group_names = list(
        all_group_names.intersection(set(group.lower() for group in organization_group_names)),
    )
    if not organization_group_names:
        return False

    groups_roots = await loaders.group_roots.load_many_chained(organization_group_names)
    return sum(
        root.state.use_ztna
        for root in groups_roots
        if isinstance(root, GitRoot) and root.state.status == RootStatus.ACTIVE
    )


async def _send_notification(
    *,
    organization_name: str,
    status: str,
    number_of_roots: int,
    loaders: Dataloaders,
    date: str,
) -> None:
    organization = await get_organization(loaders, organization_name)
    email_context: ZTNAAlertContext = {
        "organization_name": organization_name,
        "status": status.capitalize(),
        "n_roots": number_of_roots,
        "organization_url": f"{BASE_URL}/orgs/{organization_name}",
        "date": date,
    }
    members_emails = await get_organization_emails_by_notification(
        loaders=loaders,
        organization_id=organization.id,
        notification="ztna_down",
    )

    await send_mail_ztna_down_alert(
        loaders=loaders,
        context=email_context,
        email_to=members_emails,
    )


async def send_notification() -> None:
    loaders = get_new_context()
    organizations = await get_all_organizations()
    all_group_names = set(await get_all_active_group_names(loaders))
    all_group_names = {group.lower() for group in all_group_names}
    number_ztna_roots = await collect(
        tuple(
            get_number_of_ztna(
                organization=organization,
                loaders=loaders,
                all_group_names=all_group_names,
            )
            for organization in organizations
        ),
        workers=8,
    )
    organization_tunnels = [
        (organization.name, number_of_roots)
        for organization, number_of_roots in zip(organizations, number_ztna_roots, strict=False)
        if number_of_roots
    ]
    down_tunnels = await get_tunnels_with_down_status(
        [organization_name for organization_name, _ in organization_tunnels],
        [number_of_roots for _, number_of_roots in organization_tunnels],
    )
    date = get_as_str(get_now())
    await collect(
        [
            _send_notification(
                organization_name=org_name,
                status=status,
                loaders=loaders,
                number_of_roots=n_roots,
                date=date,
            )
            for org_name, n_roots, status in down_tunnels
        ],
    )
    info(
        "organization names and down status",
        extra={"down_tunnels": down_tunnels},
    )


async def main() -> None:
    await send_notification()
