import logging
from datetime import (
    date,
)

from integrates.context import (
    FI_ENVIRONMENT,
    FI_TEST_PROJECTS,
)
from integrates.custom_utils import (
    datetime as datetime_utils,
)
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model.groups.enums import (
    GroupService,
    GroupSubscriptionType,
    GroupTier,
)
from integrates.db_model.roots.enums import (
    RootStatus,
)
from integrates.db_model.roots.types import (
    GitRoot,
    RootEnvironmentUrlsRequest,
)
from integrates.mailer import (
    groups as groups_mail,
)
from integrates.mailer.types import MissingEnvironmentContext
from integrates.mailer.utils import (
    get_group_emails_by_notification,
)
from integrates.organizations import (
    domain as orgs_domain,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)

# Constants
LOGGER = logging.getLogger(__name__)


async def has_environment(
    loaders: Dataloaders,
    group: str,
) -> bool:
    roots = await loaders.group_roots.load(group)
    git_roots = [
        root
        for root in roots
        if isinstance(root, GitRoot) and root.state.status == RootStatus.ACTIVE
    ]

    environments = await loaders.root_environment_urls.load_many_chained(
        [
            RootEnvironmentUrlsRequest(root_id=root.id, group_name=root.group_name)
            for root in git_roots
        ],
    )
    return any(environments)


async def _send_mail_report(
    loaders: Dataloaders,
    group: str,
    group_date_delta: int,
) -> None:
    context: MissingEnvironmentContext = {
        "group": group,
        "group_date": group_date_delta,
    }
    members_emails = await get_group_emails_by_notification(
        loaders=loaders,
        group_name=group,
        notification="missing_environment",
    )
    await groups_mail.send_mail_missing_environment_alert(
        loaders=loaders,
        context=context,
        email_to=members_emails,
    )


async def missing_environment_alert() -> None:
    loaders: Dataloaders = get_new_context()
    active_groups = await orgs_domain.get_all_active_groups(loaders)
    groups = tuple(
        group
        for group in active_groups
        if (
            group.state.has_essential
            and group.state.tier != GroupTier.ONESHOT
            and not (
                group.state.type == GroupSubscriptionType.CONTINUOUS
                and group.state.service == GroupService.BLACK
            )
        )
    )

    if FI_ENVIRONMENT == "production":
        groups = tuple(group for group in groups if group.name not in FI_TEST_PROJECTS.split(","))

    if groups:
        for group in groups:
            creation_date: date = group.created_date.date()
            group_date_delta: int = (datetime_utils.get_utc_now().date() - creation_date).days
            has_env: bool = await has_environment(loaders, group.name)
            if (
                not has_env
                and group_date_delta > 0
                and (group_date_delta % 30 == 0 or group_date_delta == 7)
            ):
                await _send_mail_report(loaders, group.name, (group_date_delta // 7))
    else:
        LOGGER.info("- environment alert NOT sent")


async def main() -> None:
    await missing_environment_alert()
