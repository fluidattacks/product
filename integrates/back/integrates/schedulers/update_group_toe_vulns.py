import html
import logging
import logging.config
from contextlib import (
    suppress,
)
from itertools import (
    chain,
)

from aioextensions import (
    collect,
)

from integrates.custom_exceptions import (
    ToeInputAlreadyUpdated,
    ToeLinesAlreadyUpdated,
    ToePortAlreadyUpdated,
)
from integrates.custom_utils import (
    bugsnag as bugsnag_utils,
)
from integrates.custom_utils import (
    datetime as datetime_utils,
)
from integrates.custom_utils.exceptions import (
    NETWORK_ERRORS,
)
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model.toe_inputs.types import (
    GroupToeInputsRequest,
    ToeInput,
)
from integrates.db_model.toe_lines.types import (
    GroupToeLinesRequest,
    ToeLine,
)
from integrates.db_model.toe_ports.types import (
    GroupToePortsRequest,
    ToePort,
)
from integrates.db_model.vulnerabilities.enums import (
    VulnerabilityType,
)
from integrates.db_model.vulnerabilities.types import (
    Vulnerability,
)
from integrates.decorators import (
    retry_on_exceptions,
)
from integrates.findings import (
    domain as findings_domain,
)
from integrates.organizations import (
    domain as orgs_domain,
)
from integrates.settings import (
    LOGGING,
)
from integrates.toe.inputs import (
    domain as toe_inputs_domain,
)
from integrates.toe.inputs.types import (
    ToeInputAttributesToUpdate,
)
from integrates.toe.lines import (
    domain as toe_lines_domain,
)
from integrates.toe.lines.types import (
    ToeLinesAttributesToUpdate,
)
from integrates.toe.ports import (
    domain as toe_ports_domain,
)
from integrates.toe.ports.types import (
    ToePortAttributesToUpdate,
)

logging.config.dictConfig(LOGGING)
LOGGER = logging.getLogger(__name__)

bugsnag_utils.start_scheduler_session()

EMAIL_INTEGRATES = "integrates@fluidattacks.com"


def _log(msg: str, **extra: object) -> None:
    LOGGER.info(msg, extra={"extra": extra})


@retry_on_exceptions(
    exceptions=NETWORK_ERRORS,
    sleep_seconds=10,
)
async def update_toe_input(
    loaders: Dataloaders,
    current_value: ToeInput,
    attributes: ToeInputAttributesToUpdate,
) -> None:
    with suppress(ToeInputAlreadyUpdated):
        await toe_inputs_domain.update(
            loaders=loaders,
            current_value=current_value,
            attributes=attributes,
            modified_by=EMAIL_INTEGRATES,
            is_moving_toe_input=True,
        )


async def process_toe_inputs(
    group_name: str,
    open_vulnerabilities: tuple[Vulnerability, ...],
) -> None:
    loaders = get_new_context()
    group_toe_inputs = await loaders.group_toe_inputs.load_nodes(
        GroupToeInputsRequest(group_name=group_name),
    )
    toe_inputs_to_update = []

    for toe_input in group_toe_inputs:
        has_vulnerabilities: bool = (
            any(
                # ToeInput is not associated to a root_id
                # and vulnerability.root_id == toe_input.root_id
                html.unescape(vulnerability.state.where).startswith(toe_input.component)
                and html.unescape(vulnerability.state.specific).startswith(toe_input.entry_point)
                for vulnerability in open_vulnerabilities
                if vulnerability.type == VulnerabilityType.INPUTS
            )
            if toe_input.state.be_present
            else False
        )

        if toe_input.state.has_vulnerabilities != has_vulnerabilities:
            toe_inputs_to_update.append(
                update_toe_input(
                    loaders,
                    toe_input,
                    ToeInputAttributesToUpdate(
                        attacked_at=datetime_utils.get_utc_now()
                        if has_vulnerabilities and not toe_input.state.attacked_at
                        else toe_input.state.attacked_at,
                        first_attack_at=datetime_utils.get_utc_now()
                        if has_vulnerabilities and not toe_input.state.first_attack_at
                        else toe_input.state.first_attack_at,
                        has_vulnerabilities=has_vulnerabilities,
                    ),
                ),
            )

    await collect(toe_inputs_to_update, workers=32)


@retry_on_exceptions(
    exceptions=NETWORK_ERRORS,
    sleep_seconds=10,
)
async def update_toe_lines(current_value: ToeLine, attributes: ToeLinesAttributesToUpdate) -> None:
    with suppress(ToeLinesAlreadyUpdated):
        await toe_lines_domain.update(
            current_value,
            attributes,
            rules_excluded=True,
        )


async def process_toe_lines(
    group_name: str,
    open_vulnerabilities: tuple[Vulnerability, ...],
) -> None:
    loaders: Dataloaders = get_new_context()
    group_toe_lines = await loaders.group_toe_lines.load_nodes(
        GroupToeLinesRequest(group_name=group_name),
    )

    toe_lines_to_update = []

    for toe_line in group_toe_lines:
        has_vulnerabilities = (
            any(
                vulnerability.state.where.startswith(toe_line.filename)
                for vulnerability in open_vulnerabilities
                if vulnerability.type == VulnerabilityType.LINES
            )
            if toe_line.state.be_present
            else False
        )

        if toe_line.state.has_vulnerabilities != has_vulnerabilities:
            toe_lines_to_update.append(
                update_toe_lines(
                    toe_line,
                    ToeLinesAttributesToUpdate(
                        has_vulnerabilities=has_vulnerabilities,
                    ),
                ),
            )

    await collect(toe_lines_to_update, workers=32)


@retry_on_exceptions(
    exceptions=NETWORK_ERRORS,
    sleep_seconds=10,
)
async def update_toe_port(current_value: ToePort, attributes: ToePortAttributesToUpdate) -> None:
    with suppress(ToePortAlreadyUpdated):
        await toe_ports_domain.update(
            current_value,
            attributes,
            EMAIL_INTEGRATES,
            is_moving_toe_port=True,
        )


async def process_toe_ports(
    group_name: str,
    open_vulnerabilities: tuple[Vulnerability, ...],
) -> None:
    loaders = get_new_context()
    group_toe_ports = await loaders.group_toe_ports.load_nodes(
        GroupToePortsRequest(group_name=group_name),
    )
    toe_ports_to_update = []

    for toe_port in group_toe_ports:
        has_vulnerabilities: bool = (
            any(
                vulnerability.root_id == toe_port.root_id
                and vulnerability.state.specific == toe_port.port
                for vulnerability in open_vulnerabilities
                if vulnerability.type is VulnerabilityType.PORTS
            )
            if toe_port.state.be_present
            else False
        )

        if toe_port.state.has_vulnerabilities != has_vulnerabilities:
            toe_ports_to_update.append(
                update_toe_port(
                    toe_port,
                    ToePortAttributesToUpdate(
                        has_vulnerabilities=has_vulnerabilities,
                    ),
                ),
            )

    await collect(toe_ports_to_update, workers=32)


async def process_group(group_name: str) -> None:
    loaders: Dataloaders = get_new_context()
    findings = await loaders.group_findings.load(group_name)
    open_vulnerabilities: tuple[Vulnerability, ...] = tuple(
        chain.from_iterable(
            await collect(
                tuple(
                    findings_domain.get_open_vulnerabilities(loaders, finding.id)
                    for finding in findings
                ),
                workers=32,
            ),
        ),
    )

    processing_functions = [
        process_toe_lines,
        process_toe_inputs,
        process_toe_ports,
    ]

    for func in processing_functions:
        try:
            await func(group_name, open_vulnerabilities)
        except Exception as exc:
            _log(
                f"An error occurred in {func.__name__} for group {group_name}",
                extra={
                    "group_name": group_name,
                    "function": func.__name__,
                    "exception": str(exc),
                    "exc_type": type(exc).__name__,
                },
            )


async def main() -> None:
    loaders: Dataloaders = get_new_context()
    group_names = sorted(await orgs_domain.get_all_active_group_names(loaders))
    _log(f"There are {len(group_names)} groups to process")
    for group in group_names:
        _log(f"Processing group: {group}")
        await process_group(group)
