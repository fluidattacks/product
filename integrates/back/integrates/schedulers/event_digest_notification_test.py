from datetime import datetime

from integrates.db_model.enums import Notification
from integrates.db_model.stakeholders.types import NotificationsPreferences
from integrates.schedulers import event_digest_notification
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    EventCommentFaker,
    EventFaker,
    GroupAccessFaker,
    GroupAccessStateFaker,
    GroupFaker,
    OrganizationFaker,
    StakeholderFaker,
    StakeholderStateFaker,
)
from integrates.testing.mocks import Mock, mocks
from integrates.testing.utils import freeze_time


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(id="org1"),
            ],
            groups=[
                GroupFaker(organization_id="org1"),
            ],
            stakeholders=[
                StakeholderFaker(
                    role="user",
                    state=StakeholderStateFaker(
                        notifications_preferences=NotificationsPreferences(
                            email=[Notification.EVENT_DIGEST]
                        )
                    ),
                ),
            ],
            group_access=[
                GroupAccessFaker(state=GroupAccessStateFaker(has_access=True, role="user")),
            ],
            events=[EventFaker(id="event1")],
            event_comments=[
                EventCommentFaker(
                    creation_date=datetime.fromisoformat("2022-11-24T15:09:37+00:00"),
                    event_id="event1",
                )
            ],
        )
    ),
    others=[Mock(event_digest_notification, "mail_events_digest", "async", None)],
)
@freeze_time("2022-11-25T05:00:00.00")
async def test_event_digest_notification() -> None:
    groups_data = await event_digest_notification.main()

    assert len(groups_data.keys()) == 1

    assert "test-group" in groups_data
    group_data = groups_data["test-group"]
    assert len(group_data["event_comments"].keys()) == 1
    assert len(group_data["event_comments"]["event1"]) == 1
    assert group_data["event_comments"]["event1"][0].creation_date == datetime.fromisoformat(
        "2022-11-24T15:09:37+00:00"
    )

    assert group_data["event_comments"]["event1"][0].content == "Test event comment"
