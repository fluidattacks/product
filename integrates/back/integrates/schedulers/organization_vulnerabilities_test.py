from datetime import datetime
from decimal import Decimal

from integrates.dataloaders import get_new_context
from integrates.db_model.enums import Source
from integrates.db_model.findings.types import FindingEvidence, FindingEvidences
from integrates.db_model.vulnerabilities.enums import (
    VulnerabilityStateStatus,
    VulnerabilityTechnique,
    VulnerabilityType,
    VulnerabilityVerificationStatus,
)
from integrates.db_model.vulnerabilities.types import (
    VulnerabilityAdvisory,
    VulnerabilityVerification,
)
from integrates.organizations.domain import get_group_names
from integrates.roots.domain import format_environment_id
from integrates.schedulers.organization_vulnerabilities import get_data
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    DATE_2019,
    EMAIL_FLUIDATTACKS_HACKER,
    FindingFaker,
    GitRootFaker,
    GitRootStateFaker,
    GroupFaker,
    OrganizationFaker,
    RootEnvironmentUrlFaker,
    RootEnvironmentUrlStateFaker,
    SeverityScoreFaker,
    VulnerabilityFaker,
    VulnerabilityStateFaker,
)
from integrates.testing.mocks import mocks


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(id="org1"),
                OrganizationFaker(id="org2", name="test-org2"),
            ],
            groups=[GroupFaker(organization_id="org1")],
            roots=[
                GitRootFaker(
                    id="root1",
                    state=GitRootStateFaker(
                        gitignore=["bower_components/*", "node_modules/*"],
                        includes_health_check=True,
                        nickname="test_root",
                        url="https://gitlab.com/fluidattacks/universe",
                    ),
                ),
            ],
            root_environment_urls=[
                RootEnvironmentUrlFaker(
                    id=format_environment_id("https://test.com/"),
                    url="https://test.com/",
                    group_name="test-group",
                    root_id="root1",
                    state=RootEnvironmentUrlStateFaker(),
                ),
            ],
            findings=[
                FindingFaker(
                    id="3c475384-834c-47b0-ac71-a41a022e401c",
                    evidences=FindingEvidences(
                        evidence1=FindingEvidence(
                            description="evidence1",
                            url=("group1-3c475384-834c-47b0-ac71-a41a022e401c-evidence1"),
                            modified_date=datetime.fromisoformat("2020-11-19T13:37:10+00:00"),
                        ),
                    ),
                ),
            ],
            vulnerabilities=[
                VulnerabilityFaker(
                    id="vuln1",
                    finding_id="3c475384-834c-47b0-ac71-a41a022e401c",
                    root_id="root1",
                    state=VulnerabilityStateFaker(
                        specific="2222",
                        where="back/src/controller/user/package.json",
                        status=VulnerabilityStateStatus.SAFE,
                    ),
                    type=VulnerabilityType.LINES,
                ),
                VulnerabilityFaker(
                    id="vuln2",
                    finding_id="3c475384-834c-47b0-ac71-a41a022e401c",
                    root_id="root1",
                    severity_score=SeverityScoreFaker(threat_score=Decimal("1.1")),
                    state=VulnerabilityStateFaker(
                        specific="9999",
                        where="192.168.1.20",
                    ),
                    type=VulnerabilityType.PORTS,
                    verification=VulnerabilityVerification(
                        modified_date=DATE_2019,
                        status=VulnerabilityVerificationStatus.REQUESTED,
                    ),
                ),
                VulnerabilityFaker(
                    id="vuln3",
                    finding_id="3c475384-834c-47b0-ac71-a41a022e401c",
                    root_id="root1",
                    severity_score=SeverityScoreFaker(threat_score=Decimal("5.1")),
                    state=VulnerabilityStateFaker(
                        specific="4848",
                        where="192.168.1.20",
                        advisories=VulnerabilityAdvisory(
                            package="test-package",
                            vulnerable_version="0.1.1",
                            cve=["CVE-2023-22491"],
                            epss=50,
                        ),
                    ),
                    type=VulnerabilityType.PORTS,
                    verification=VulnerabilityVerification(
                        modified_by=EMAIL_FLUIDATTACKS_HACKER,
                        modified_date=DATE_2019,
                        status=VulnerabilityVerificationStatus.VERIFIED,
                    ),
                ),
                VulnerabilityFaker(
                    id="vuln4",
                    finding_id="3c475384-834c-47b0-ac71-a41a022e401c",
                    root_id="root1",
                    severity_score=SeverityScoreFaker(threat_score=Decimal("1.1")),
                    state=VulnerabilityStateFaker(
                        specific="9999",
                        where="192.168.1.20",
                    ),
                    type=VulnerabilityType.PORTS,
                    verification=VulnerabilityVerification(
                        modified_date=DATE_2019,
                        status=VulnerabilityVerificationStatus.REQUESTED,
                    ),
                ),
                VulnerabilityFaker(
                    id="vuln5",
                    finding_id="3c475384-834c-47b0-ac71-a41a022e401c",
                    root_id="root1",
                    state=VulnerabilityStateFaker(
                        specific="4646", where="192.168.1.46", status=VulnerabilityStateStatus.SAFE
                    ),
                    type=VulnerabilityType.PORTS,
                    verification=VulnerabilityVerification(
                        modified_by=EMAIL_FLUIDATTACKS_HACKER,
                        modified_date=DATE_2019,
                        status=VulnerabilityVerificationStatus.VERIFIED,
                    ),
                ),
                VulnerabilityFaker(
                    id="vuln6",
                    finding_id="3c475384-834c-47b0-ac71-a41a022e401c",
                    root_id="root1",
                    state=VulnerabilityStateFaker(
                        specific="4747", where="192.168.1.47", source=Source.MACHINE
                    ),
                    type=VulnerabilityType.PORTS,
                    technique=VulnerabilityTechnique.DAST,
                ),
            ],
        )
    ),
)
async def test_organization_vulnerabilities() -> None:
    loaders = get_new_context()
    all_groups_names = await get_group_names(loaders, "ORG#org2")
    rows = await get_data(groups=tuple(all_groups_names), organization_name="test-org2")

    assert len(rows) == 1
    assert len(rows[0]) == 73

    all_groups_names = await get_group_names(loaders, "ORG#org1")
    rows = await get_data(groups=tuple(all_groups_names), organization_name="test-org")

    assert len(rows) == 7
    assert rows[0][1] == "Related Finding"
    assert rows[1][1] == "001. SQL injection - C Sharp SQL API"
    assert rows[0][3] == "Vulnerability Id"
    assert rows[1][3] == "vuln3"
    assert rows[0][-6] == "Package"
    assert rows[1][-6] == "test-package"
    assert rows[0][-3] == "EPSS"
    assert rows[1][-3] == "50%"
    assert rows[0][-2] == "Root Branch"
    assert rows[1][-2] == "master"
    assert rows[0][-1] == "Group"
    assert rows[1][-1] == "test-group"
