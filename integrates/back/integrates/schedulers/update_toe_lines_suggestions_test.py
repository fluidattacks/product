from integrates.dataloaders import Dataloaders, get_new_context
from integrates.db_model.groups.enums import GroupService
from integrates.db_model.roots.enums import RootCloningStatus
from integrates.db_model.toe_lines.types import GroupToeLinesRequest, SortsSuggestion
from integrates.db_model.vulnerabilities.enums import VulnerabilityType
from integrates.schedulers import update_toe_lines_suggestions
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    FindingFaker,
    GitRootCloningFaker,
    GitRootFaker,
    GitRootStateFaker,
    GroupFaker,
    GroupStateFaker,
    OrganizationFaker,
    ToeLinesFaker,
    ToeLinesStateFaker,
    VulnerabilityFaker,
    VulnerabilityStateFaker,
)
from integrates.testing.mocks import Mock, mocks


class Cursor:
    def execute(self, _: str) -> None:
        return None

    def fetchall(self) -> list[tuple[str, str, float]]:
        result = [
            (
                "001. SQL injection - C Sharp SQL API",
                "211. Asymmetric denial of service - ReDoS",
                0.07142857143,
            ),
            (
                "007. Cross-site request forgery, 200. Traceability loss",
                "239. Technical information leak - Errors, "
                "039. Improper authorization control for web services",
                0.8571428571,
            ),
            (
                "007. Cross-site request forgery",
                "379. Inappropriate coding practices - Unnecessary imports",
                0.02362204724,
            ),
            (
                "007. Cross-site request forgery",
                "239. Technical information leak - Errors, 200. Traceability loss, "
                "039. Improper authorization control for web services",
                0.02834645669,
            ),
        ]
        return result


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(id="org1"),
            ],
            groups=[
                GroupFaker(
                    name="group1",
                    organization_id="org1",
                    state=GroupStateFaker(service=GroupService.WHITE),
                ),
                GroupFaker(
                    name="group2",
                    organization_id="org1",
                    state=GroupStateFaker(service=GroupService.WHITE),
                ),
            ],
            roots=[
                GitRootFaker(
                    group_name="group1",
                    id="root1",
                    cloning=GitRootCloningFaker(status=RootCloningStatus.OK),
                    state=GitRootStateFaker(nickname="root1", branch="test1"),
                ),
                GitRootFaker(
                    group_name="group1",
                    id="root2",
                    cloning=GitRootCloningFaker(status=RootCloningStatus.OK),
                    state=GitRootStateFaker(nickname="root2", branch="test2"),
                ),
                GitRootFaker(
                    group_name="group2",
                    id="root3",
                    cloning=GitRootCloningFaker(status=RootCloningStatus.OK),
                    state=GitRootStateFaker(nickname="root3", branch="test3"),
                ),
            ],
            findings=[
                FindingFaker(
                    group_name="group1",
                    id="fin_id_1",
                    title="001. SQL injection - C Sharp SQL API",
                ),
                FindingFaker(
                    group_name="group1",
                    id="fin_id_2",
                    title="007. Cross-site request forgery",
                ),
                FindingFaker(
                    group_name="group1",
                    id="fin_id_3",
                    title="200. Traceability loss",
                ),
                FindingFaker(
                    group_name="group2",
                    id="fin_id_4",
                    title="092. Insecure encryption algorithm - Anonymous cipher suites",
                ),
                FindingFaker(
                    group_name="group2",
                    id="fin_id_5",
                    title="007. Cross-site request forgery",
                ),
            ],
            vulnerabilities=[
                VulnerabilityFaker(
                    finding_id="fin_id_1",
                    group_name="group1",
                    id="vuln_id_1",
                    organization_name="org1",
                    root_id="root1",
                    type=VulnerabilityType.LINES,
                    state=VulnerabilityStateFaker(
                        where="path/to/file1.py",
                    ),
                ),
                VulnerabilityFaker(
                    finding_id="fin_id_2",
                    group_name="group1",
                    id="vuln_id_2",
                    organization_name="org1",
                    root_id="root2",
                    type=VulnerabilityType.LINES,
                    state=VulnerabilityStateFaker(
                        where="path/to/file1.py",
                    ),
                ),
                VulnerabilityFaker(
                    finding_id="fin_id_3",
                    group_name="group1",
                    id="vuln_id_3",
                    organization_name="org1",
                    root_id="root2",
                    type=VulnerabilityType.LINES,
                    state=VulnerabilityStateFaker(
                        where="path/to/file1.py",
                    ),
                ),
                VulnerabilityFaker(
                    finding_id="fin_id_5",
                    group_name="group2",
                    id="vuln_id_4",
                    organization_name="org1",
                    root_id="root3",
                    type=VulnerabilityType.LINES,
                    state=VulnerabilityStateFaker(
                        where="path/to/file2.py",
                    ),
                ),
                VulnerabilityFaker(
                    finding_id="fin_id_4",
                    group_name="group2",
                    id="vuln_id_5",
                    organization_name="org1",
                    root_id="root3",
                    type=VulnerabilityType.LINES,
                    state=VulnerabilityStateFaker(
                        where="path/to/file3.py",
                    ),
                ),
            ],
            toe_lines=[
                ToeLinesFaker(
                    filename="path/to/file1.py",
                    group_name="group1",
                    root_id="root1",
                    state=ToeLinesStateFaker(
                        be_present=True,
                        has_vulnerabilities=True,
                        sorts_suggestions=[
                            SortsSuggestion(
                                finding_title="363. Weak credential policy - Password strength",
                                probability=51,
                            )
                        ],
                    ),
                ),
                ToeLinesFaker(
                    filename="path/to/file1.py",
                    group_name="group1",
                    root_id="root2",
                    state=ToeLinesStateFaker(
                        be_present=True,
                        has_vulnerabilities=True,
                    ),
                ),
                ToeLinesFaker(
                    filename="path/to/file2.py",
                    group_name="group2",
                    root_id="root3",
                    state=ToeLinesStateFaker(
                        be_present=True,
                        has_vulnerabilities=True,
                    ),
                ),
                ToeLinesFaker(
                    filename="path/to/file3.py",
                    group_name="group2",
                    root_id="root3",
                    state=ToeLinesStateFaker(
                        be_present=True,
                        has_vulnerabilities=True,
                    ),
                ),
            ],
        ),
    ),
    others=[
        Mock(
            update_toe_lines_suggestions,
            "snowflake_db_cursor",
            "managed",
            Cursor(),
        )
    ],
)
async def test_update_toe_lines_suggestions_full() -> None:
    """Testing main auxiliary function of scheduler"""
    await update_toe_lines_suggestions.main()
    loaders: Dataloaders = get_new_context()
    group1_toe_lines = await loaders.group_toe_lines.load_nodes(
        GroupToeLinesRequest(group_name="group1", be_present=True),
    )
    group2_toe_lines = await loaders.group_toe_lines.load_nodes(
        GroupToeLinesRequest(group_name="group2", be_present=True),
    )
    group1_root1_path1_suggestions = next(
        (
            toe_line.state.sorts_suggestions
            for toe_line in group1_toe_lines
            if toe_line.root_id == "root1" and toe_line.filename == "path/to/file1.py"
        ),
        None,
    )
    group1_root2_path1_suggestions = next(
        (
            toe_line.state.sorts_suggestions
            for toe_line in group1_toe_lines
            if toe_line.root_id == "root2" and toe_line.filename == "path/to/file1.py"
        ),
        None,
    )
    group2_root3_path2_suggestions = next(
        (
            toe_line.state.sorts_suggestions
            for toe_line in group2_toe_lines
            if toe_line.root_id == "root3" and toe_line.filename == "path/to/file2.py"
        ),
        None,
    )
    group2_root3_path3_suggestions = next(
        (
            toe_line.state.sorts_suggestions
            for toe_line in group1_toe_lines
            if toe_line.root_id == "root3" and toe_line.filename == "path/to/file3.py"
        ),
        None,
    )
    assert group1_root1_path1_suggestions == [
        SortsSuggestion(finding_title="211. Asymmetric denial of service - ReDoS", probability=7),
    ]
    assert group1_root2_path1_suggestions == [
        SortsSuggestion(finding_title="239. Technical information leak - Errors", probability=85),
        SortsSuggestion(
            finding_title="039. Improper authorization control for web services", probability=85
        ),
        SortsSuggestion(finding_title="239. Technical information leak - Errors", probability=2),
        SortsSuggestion(finding_title="200. Traceability loss", probability=2),
        SortsSuggestion(
            finding_title="039. Improper authorization control for web services", probability=2
        ),
    ]
    assert group2_root3_path2_suggestions == [
        SortsSuggestion(finding_title="239. Technical information leak - Errors", probability=2),
        SortsSuggestion(finding_title="200. Traceability loss", probability=2),
        SortsSuggestion(
            finding_title="039. Improper authorization control for web services", probability=2
        ),
    ]
    assert group2_root3_path3_suggestions is None
