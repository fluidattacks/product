import csv
import os
import tempfile
from collections.abc import (
    Coroutine,
)
from datetime import (
    datetime,
)

import boto3
from aioextensions import (
    collect,
)
from botocore.exceptions import (
    ClientError,
)
from cryptography.fernet import (
    Fernet,
)
from cryptography.fernet import (
    InvalidToken as InvalidFernetToken,
)

from integrates.context import (
    FI_ENVIRONMENT,
    FI_FERNET_TOKEN,
)
from integrates.custom_exceptions import (
    ToeLinesAlreadyUpdated,
)
from integrates.custom_utils import (
    bugsnag as bugsnag_utils,
)
from integrates.custom_utils import (
    datetime as datetime_utils,
)
from integrates.custom_utils.exceptions import (
    NETWORK_ERRORS,
)
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model import (
    toe_lines as toe_lines_model,
)
from integrates.db_model.roots.types import (
    Root,
)
from integrates.db_model.toe_lines.types import (
    GroupToeLinesRequest,
    ToeLine,
)
from integrates.decorators import (
    retry_on_exceptions,
)
from integrates.organizations import (
    domain as orgs_domain,
)
from integrates.schedulers.common import (
    MAX_DAYS_SINCE_ATTACKED,
    get_active_toe_lines,
    get_sorts_days_since_attacked,
    info,
)

S3_BUCKET_NAME: str = "sorts"
S3_RESOURCE = boto3.resource("s3")
S3_BUCKET = S3_RESOURCE.Bucket(S3_BUCKET_NAME)

bugsnag_utils.start_scheduler_session()


@retry_on_exceptions(
    exceptions=NETWORK_ERRORS,
    sleep_seconds=10,
    max_attempts=10,
)
async def update_toe_lines(
    current_value: ToeLine,
    sorts_risk_level: int,
    sorts_priority_factor: int,
    sorts_risk_level_date: datetime,
) -> None:
    await toe_lines_model.update_state(
        current_value=current_value,
        new_state=current_value.state._replace(
            modified_date=datetime_utils.get_utc_now(),
            sorts_risk_level=sorts_risk_level,
            sorts_priority_factor=sorts_priority_factor,
            sorts_risk_level_date=sorts_risk_level_date,
        ),
    )


def get_toe_lines_dict(
    group_toe_lines: list[ToeLine],
    group_roots: list[Root],
) -> dict[str, ToeLine]:
    toe_lines_dict: dict[str, ToeLine] = {}
    for toe_line in group_toe_lines:
        root_nickname = next(
            (root.state.nickname for root in group_roots if root.id == toe_line.root_id),
            None,
        )
        full_filename = f"{root_nickname}/{toe_line.filename}"
        toe_lines_dict[full_filename] = toe_line
    return toe_lines_dict


def _get_updated_toe_lines(
    predicted_files: csv.DictReader,
    fernet: Fernet,
    current_date: datetime,
    toe_lines_dict: dict[str, ToeLine],
    active_group_toe_lines: list[ToeLine],
) -> tuple[dict[str, ToeLine], list[Coroutine], int]:
    in_scope_toes_dict = {}
    updates = []
    out_scope_count = 0

    for predicted_file in predicted_files:
        decrypted_filepath = fernet.decrypt(predicted_file["file"].encode()).decode()
        predicted_file_prob = int(float(predicted_file["prob_vuln"]))

        if decrypted_filepath in toe_lines_dict:
            days_since_attacked = get_sorts_days_since_attacked(toe_lines_dict[decrypted_filepath])
            sorts_priority_factor = (
                2 * predicted_file_prob + min(days_since_attacked.days, MAX_DAYS_SINCE_ATTACKED)
            ) / (MAX_DAYS_SINCE_ATTACKED + 200)
            updates.append(
                update_toe_lines(
                    toe_lines_dict[decrypted_filepath],
                    predicted_file_prob,
                    int(sorts_priority_factor * 100),
                    current_date,
                ),
            )
            in_scope_toes_dict[toe_lines_dict[decrypted_filepath].filename] = toe_lines_dict[
                decrypted_filepath
            ]

    for toe_line in active_group_toe_lines:
        toe_line_date = (
            toe_line.state.sorts_risk_level_date.replace(tzinfo=None)
            if toe_line.state.sorts_risk_level_date is not None
            else None
        )
        if toe_line.filename not in in_scope_toes_dict and toe_line_date != datetime(1970, 1, 1):
            updates.append(
                update_toe_lines(
                    toe_line,
                    -1,
                    -1,
                    datetime(1970, 1, 1),
                ),
            )
            out_scope_count += 1
    return in_scope_toes_dict, updates, out_scope_count


async def update_sorts_attributes(
    group_name: str,
    current_date: datetime,
    group_toe_lines: list[ToeLine],
    predicted_files: csv.DictReader,
    fernet: Fernet,
) -> None:
    loaders: Dataloaders = get_new_context()
    group_roots: list[Root] = await loaders.group_roots.load(group_name)
    active_group_toe_lines = get_active_toe_lines(group_toe_lines, group_roots)
    toe_lines_dict = get_toe_lines_dict(active_group_toe_lines, group_roots)
    in_scope_toes_dict, updates, out_scope_count = _get_updated_toe_lines(
        predicted_files,
        fernet,
        current_date,
        toe_lines_dict,
        active_group_toe_lines,
    )

    info(
        f"Group {group_name} has {len(active_group_toe_lines)} present "
        f"toe lines with {len(in_scope_toes_dict)} toe lines to be updated "
        f"using Sorts and {out_scope_count} toe lines out of scope "
        "to be updated with the default date",
    )

    await collect(tuple(updates), workers=64)


async def process_group(group_name: str, current_date: datetime) -> None:
    # The Fernet token for testing shouldn't be changed
    # to avoid breaking the test for this job
    fernet = (
        Fernet(FI_FERNET_TOKEN)
        if FI_ENVIRONMENT == "production"
        else Fernet("cudK8BSD-eApPCivGAX05i1NOqjiCwu9xXiRRKhB1CY=")
    )
    loaders: Dataloaders = get_new_context()
    info(f"Processing group {group_name}")

    group_toe_lines = await loaders.group_toe_lines.load_nodes(
        GroupToeLinesRequest(group_name=group_name, be_present=True),
    )

    csv_name: str = f"{group_name}_sorts_results_file.csv"

    with tempfile.TemporaryDirectory(
        prefix="integrates_process_group_",
        ignore_cleanup_errors=True,
    ) as tmp_dir:
        local_file: str = os.path.join(tmp_dir, csv_name)
        try:
            S3_BUCKET.Object(f"sorts-execution-results/{csv_name}").download_file(local_file)
        except ClientError as error:
            if error.response["Error"]["Code"] == "404":
                info(f"There is no {group_name} file in S3")
                return

        with open(local_file, encoding="utf8") as csv_file:
            reader = csv.DictReader(csv_file)
            try:
                await update_sorts_attributes(
                    group_name,
                    current_date,
                    group_toe_lines,
                    reader,
                    fernet,
                )
            except (*NETWORK_ERRORS, ToeLinesAlreadyUpdated) as exc:
                info(
                    f"Group {group_name} could not be updated",
                    extra={"extra": {"error": exc}},
                )
            except InvalidFernetToken as exc:
                info(
                    f"The Fernet token used to encrypt {group_name} is invalid or outdated",
                    extra={"extra": {"error": exc}},
                )
            else:
                info(f"ToeLines's sortsFileRisk for {group_name} updated")


async def main() -> None:
    loaders: Dataloaders = get_new_context()
    group_names = sorted(await orgs_domain.get_all_active_group_names(loaders))
    current_date = datetime.now()
    current_date = current_date.replace(hour=0, minute=0, second=0, microsecond=0)

    await collect(
        tuple(process_group(group_name, current_date) for group_name in group_names),
        workers=1,
    )
