import logging
from datetime import (
    UTC,
    datetime,
    timedelta,
)

from integrates.batch.dal import list_jobs_paginated
from integrates.schedulers.common import (
    info,
)

logging.getLogger("boto3").setLevel(logging.ERROR)
logging.getLogger("botocore").setLevel(logging.ERROR)


def _get_machine_jobs(data: dict, timestamp_tonight: int) -> dict[str, dict[str, str]]:
    return {
        job["jobId"]: {"job_name": job["jobName"], "job_status": job["status"]}
        for job in data.get("jobSummaryList", [])
        if str(job["jobName"]).startswith("machine_")
        and str(job["createdAt"]) < str(timestamp_tonight)
        and job["status"] in {"SUCCEEDED", "FAILED"}
    }


async def _get_searched_jobs(
    timestamp_start_date: int, timestamp_end_date: int
) -> dict[str, dict[str, str]]:
    jobs_list = {}
    next_token = None
    while True:
        data = await list_jobs_paginated(
            job_queue="skims",
            next_token=next_token,
            filters=[
                {
                    "name": "AFTER_CREATED_AT",
                    "values": [
                        str(timestamp_start_date),
                    ],
                },
            ],
        )
        jobs_list.update(_get_machine_jobs(data, timestamp_end_date))
        next_token = data.get("nextToken", None)
        if not next_token or len(jobs_list) > 100000:
            break

    return jobs_list


def get_technique_summary(batch_jobs: dict[str, dict[str, str]]) -> list[dict[str, int]]:
    jobs_techniques_success: dict[str, list] = {
        "all": [],
        "apk": [],
        "cspm": [],
        "dast": [],
        "sast": [],
    }
    jobs_techniques_failed: dict[str, list] = {
        "all": [],
        "apk": [],
        "cspm": [],
        "dast": [],
        "sast": [],
    }
    for job_id, job_data in batch_jobs.items():
        technique = job_data["job_name"].split("_")[3]
        if technique in jobs_techniques_success:
            if job_data["job_status"] == "SUCCEEDED":
                jobs_techniques_success[technique].append(job_id)

            if job_data["job_status"] == "FAILED":
                jobs_techniques_failed[technique].append(job_id)

    return [
        {technique: len(job_ids) for technique, job_ids in jobs_techniques_success.items()},
        {technique: len(job_ids) for technique, job_ids in jobs_techniques_failed.items()},
    ]


async def get_machine_job_statistics(searched_date: datetime | None = None) -> list[dict[str, int]]:
    now = datetime.now(UTC)
    end_date = searched_date or datetime(now.year, now.month, now.day)
    timestamp_end_date = int(end_date.timestamp() * 1000)
    start_date = end_date - timedelta(days=1)
    timestamp_start_date = int(start_date.timestamp() * 1000)

    batch_jobs_names = await _get_searched_jobs(timestamp_start_date, timestamp_end_date)
    technique_statistics = get_technique_summary(batch_jobs_names)

    return technique_statistics


async def main() -> None:
    technique_statistics = await get_machine_job_statistics()
    info(
        "Following are machine job statistics",
        extra={
            "Statistics successful jobs": technique_statistics[0],
            "Statistics failed jobs": technique_statistics[1],
        },
    )
