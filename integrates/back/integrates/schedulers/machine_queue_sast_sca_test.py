from integrates import batch, jobs_orchestration
from integrates.batch.dal.get import get_actions_by_name
from integrates.batch.enums import Action, SkimsBatchQueue
from integrates.dataloaders import Dataloaders, get_new_context
from integrates.db_model.groups.enums import GroupService
from integrates.db_model.roots.enums import RootCloningStatus, RootStatus
from integrates.schedulers import machine_queue_sast_sca
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    GitRootCloningFaker,
    GitRootFaker,
    GitRootStateFaker,
    GroupFaker,
    GroupStateFaker,
    OrganizationFaker,
)
from integrates.testing.mocks import Mock, mocks

MACHINE_EMAIL = "machine@fluidattacks.com"


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            groups=[
                GroupFaker(
                    name="group1",
                    organization_id="org1",
                    state=GroupStateFaker(service=GroupService.WHITE),
                ),
                GroupFaker(
                    name="group2",
                    organization_id="org1",
                    state=GroupStateFaker(service=GroupService.WHITE),
                ),
                GroupFaker(
                    name="group3",
                    organization_id="org1",
                    state=GroupStateFaker(has_essential=False),
                ),
            ],
            roots=[
                GitRootFaker(
                    group_name="group1",
                    id="root1",
                    cloning=GitRootCloningFaker(status=RootCloningStatus.OK),
                    state=GitRootStateFaker(nickname="root1"),
                ),
                GitRootFaker(
                    group_name="group1",
                    id="root2",
                    cloning=GitRootCloningFaker(status=RootCloningStatus.OK),
                    state=GitRootStateFaker(nickname="root2", branch="test"),
                ),
                GitRootFaker(
                    group_name="group1",
                    id="root3",
                    cloning=GitRootCloningFaker(status=RootCloningStatus.OK),
                    state=GitRootStateFaker(
                        nickname="root3", branch="qa", status=RootStatus.INACTIVE
                    ),
                ),
                GitRootFaker(
                    group_name="group1",
                    id="root4",
                    state=GitRootStateFaker(nickname="root4", branch="qa_2"),
                ),
            ],
        ),
    ),
    others=[
        Mock(
            jobs_orchestration.jobs,
            "generate_batch_action_config",
            "async",
            (
                {
                    "roots": ["root1", "root2"],
                    "roots_config_files": ["root1.yaml", "root2.yaml"],
                    "roots_sbom_config_files": ["sbom_root1_config.yaml", "sbom_root2_config.yaml"],
                },
                "123456",
                SkimsBatchQueue.SMALL,
            ),
        ),
        Mock(batch.dal.put, "put_action_to_batch", "async", "123456"),
        Mock(machine_queue_sast_sca, "sleep", "sync", None),
    ],
)
async def test_machine_queue_sast_sca() -> None:
    """Testing auxiliary functions of scheduler"""
    loaders: Dataloaders = get_new_context()

    groups = await machine_queue_sast_sca.get_machine_groups(loaders)

    assert groups == ["group1", "group2"]

    roots_to_execute = await machine_queue_sast_sca.get_roots_to_execute(loaders, "group1")
    assert roots_to_execute == {"root1", "root2"}

    await machine_queue_sast_sca.main()

    pending_executions = await get_actions_by_name(
        action_name=Action.EXECUTE_MACHINE_SAST,
        entity="group1",
    )
    assert len(pending_executions) == 1
