import asyncio
import hashlib
import json
import os
import sys
import tempfile
from collections import defaultdict
from collections.abc import Generator, Iterable

from fluidattacks_core.serializers.syntax import (
    TREE_SITTER_FUNCTION_IDENTIFIER,
    get_language_for_tree,
    get_language_from_path,
    parse_content_tree_sitter,
)
from more_itertools import flatten
from tree_sitter import Node, Tree
from types_aiobotocore_sqs import SQSClient

from integrates.context import FI_LLM_SCAN_DB_MODEL_PATH
from integrates.dataloaders import Dataloaders, get_new_context
from integrates.db_model.roots.types import GitRoot
from integrates.db_model.vulnerabilities.enums import VulnerabilityType
from integrates.db_model.vulnerabilities.types import Vulnerability
from integrates.dynamodb import (
    keys,
)
from integrates.dynamodb.operations import put_item
from integrates.dynamodb.tables import load_tables
from integrates.roots.s3_mirror import download_repo
from integrates.sqs.resource import get_client

with open(FI_LLM_SCAN_DB_MODEL_PATH, encoding="utf-8") as file:
    TABLE = load_tables(json.load(file))[0]


def query_nodes_by_language(
    language_name: str,
    tree: Tree,
    interest_nodes: Iterable[str],
) -> dict[str, list[Node]]:
    language_parser = get_language_for_tree(language_name)
    query = "\n".join(f"({item}) @{item}" for item in interest_nodes)
    return language_parser.query(query).captures(tree.root_node)


def walk_file(base_path: str) -> Generator[str, None, None]:
    excluded_dirs = ["node_modules", "dist", "__pycache__"]

    for dirpath, _, filenames in os.walk(base_path):
        if any(
            (
                dirpath.endswith(excluded_dir)
                or f"{os.path.sep}{excluded_dir}{os.path.sep}" in dirpath
            )
            for excluded_dir in excluded_dirs
        ):
            continue

        for filename in filenames:
            full_path: str = os.path.join(dirpath, filename)

            yield full_path


async def analyze_path(
    sqs_client: SQSClient,
    git_root: GitRoot,
    file_path: str,
    vulnerabilities_by_path: list[Vulnerability],
) -> None:
    language = get_language_from_path(file_path)
    if not language:
        return
    if not git_root.cloning.commit:
        return
    try:
        function_nodes_type = tuple(TREE_SITTER_FUNCTION_IDENTIFIER[language].keys())
    except KeyError:
        return
    with open(file_path, "rb") as file:
        content = file.read()
        tree = parse_content_tree_sitter(content, language)
    nodes = tuple(flatten(query_nodes_by_language(language, tree, function_nodes_type).values()))
    specifics_by_path = tuple(int(x.state.specific) for x in vulnerabilities_by_path)
    nodes = tuple(
        node
        for node in nodes
        if all(
            specific not in range(node.start_point[0], node.end_point[0] + 1)
            for specific in specifics_by_path
        )
    )
    for node in nodes:
        if not node.text or len(node.text.replace(b" ", b"")) < 800:
            continue
        snippet_hash = hashlib.sha3_256(node.text).hexdigest()
        key = keys.build_key(
            facet=TABLE.facets["snippet_metadata"],
            values={
                "where": file_path,
                "hash": snippet_hash,
                "root_id": git_root.id,
            },
        )
        key_structure = TABLE.primary_key
        item = {
            key_structure.partition_key: key.partition_key,
            key_structure.sort_key: key.sort_key,
            "group_name": git_root.group_name,
            "commit": git_root.cloning.commit,
            "root_id": git_root.id,
            "root_nickname": git_root.state.nickname,
            "snippet_content": node.text.decode("utf-8"),
            "snippet_hash": snippet_hash,
            "hash_type": "sha3_256",
            "where": file_path,
            "language": language,
            "start_point": [node.start_point[0], node.start_point[1]],
            "end_point": [node.end_point[0], node.end_point[1]],
        }
        await put_item(
            facet=TABLE.facets["snippet_metadata"],
            item=item,
            table=TABLE,
        )
        await sqs_client.send_message(
            QueueUrl=("https://sqs.us-east-1.amazonaws.com/205810638802/integrates_llm_scan.fifo"),
            MessageBody=json.dumps(
                {
                    "id": f"{git_root.id}_{snippet_hash}",
                    "task": "scan_snippet",
                    "args": [git_root.id, file_path, snippet_hash],
                },
            ),
            MessageGroupId=f"{git_root.id}_{snippet_hash}",
        )


async def process_git_root(loaders: Dataloaders, git_root: GitRoot) -> None:
    root_vulnerabilities = await loaders.root_vulnerabilities.load(git_root.id)
    sqs_client = await get_client()
    vulnerabilities_by_path: dict[str, list[Vulnerability]] = defaultdict(list)
    for vuln in root_vulnerabilities:
        if vuln.type != VulnerabilityType.LINES:
            continue
        vulnerabilities_by_path[vuln.state.where].append(vuln)

    with tempfile.TemporaryDirectory(
        prefix="integrates_llm_", ignore_cleanup_errors=True
    ) as tmpdir:
        repo = await download_repo(
            git_root.group_name,
            git_root.state.nickname,
            tmpdir,
            git_root.state.gitignore,
        )
        if not repo:
            return
        for file_path in walk_file(str(repo.working_dir)):
            if any(
                x in file_path.lower()
                for x in (
                    "test",
                    "plugin",
                    "library",
                    ".min",
                    "libs",
                    "static",
                    "assets",
                    "/js/",
                    "themes",
                    "node_modules",
                    "migrat",
                    "site-packages",
                )
            ):
                continue
            os.chdir(repo.working_dir)
            relative_file_path = file_path.replace(f"{repo.working_dir}{os.path.sep}", "")
            await analyze_path(
                sqs_client,
                git_root,
                relative_file_path,
                vulnerabilities_by_path[file_path.replace(relative_file_path, "")],
            )


async def main() -> None:
    group_name = sys.argv[1]
    loaders: Dataloaders = get_new_context()
    git_roots = [
        root for root in await loaders.group_roots.load(group_name) if isinstance(root, GitRoot)
    ]
    for root in git_roots:
        await process_git_root(loaders, root)


if __name__ == "__main__":
    asyncio.run(main())
