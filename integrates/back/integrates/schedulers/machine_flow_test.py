import time

from integrates.batch.types import PutActionResult
from integrates.dataloaders import Dataloaders, get_new_context
from integrates.db_model.enums import Source
from integrates.db_model.groups.enums import GroupService
from integrates.db_model.roots.enums import RootCloningStatus
from integrates.db_model.vulnerabilities.enums import VulnerabilityType
from integrates.schedulers import machine_flow
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    FindingFaker,
    FindingStateFaker,
    GitRootCloningFaker,
    GitRootFaker,
    GitRootStateFaker,
    GroupFaker,
    GroupStateFaker,
    OrganizationFaker,
    VulnerabilityFaker,
    VulnerabilityStateFaker,
)
from integrates.testing.mocks import Mock, mocks

MACHINE_EMAIL = "machine@fluidattacks.com"


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            groups=[
                GroupFaker(
                    name="group1",
                    organization_id="org1",
                    state=GroupStateFaker(service=GroupService.WHITE),
                ),
            ],
            roots=[
                GitRootFaker(
                    group_name="group1",
                    id="root1",
                    cloning=GitRootCloningFaker(status=RootCloningStatus.OK),
                    state=GitRootStateFaker(nickname="root1"),
                ),
            ],
            findings=[
                FindingFaker(
                    group_name="group1",
                    id="fin_id_1",
                    state=FindingStateFaker(modified_by=MACHINE_EMAIL),
                    title="001. SQL injection - C Sharp SQL API",
                ),
            ],
            vulnerabilities=[
                VulnerabilityFaker(
                    finding_id="fin_id_1",
                    group_name="group1",
                    id="vuln_id_1",
                    organization_name="test_org",
                    root_id="root1",
                    created_by=MACHINE_EMAIL,
                    hacker_email=MACHINE_EMAIL,
                    type=VulnerabilityType.LINES,
                    state=VulnerabilityStateFaker(
                        where="back/src/controller/user/index.js",
                        specific="12",
                        source=Source.MACHINE,
                    ),
                    vuln_machine_hash=8061522565195734354,
                    vuln_skims_description="vuln found on root",
                    vuln_skims_method="c_sharp.sql_injection",
                ),
            ],
        ),
    ),
    others=[
        Mock(time, "sleep", "sync", None),
        Mock(machine_flow, "update_status", "sync", None),
        Mock(machine_flow, "queue_sync_git_roots", "async", PutActionResult(success=True)),
    ],
)
async def test_machine_flow() -> None:
    """Testing main auxiliary function of scheduler"""
    loaders: Dataloaders = get_new_context()

    await machine_flow.execute_machine_flow("group1", "root1")

    vulns = await loaders.root_vulnerabilities.load("root1")
    assert len(vulns) == 0
