from datetime import datetime

from integrates.custom_utils import datetime as datetime_utils
from integrates.dataloaders import Dataloaders, get_new_context
from integrates.db_model.groups.enums import GroupManaged
from integrates.mailer import trial as trial_mail
from integrates.organizations.domain import get_all_trial_groups
from integrates.schedulers import send_trial_engagement_notification
from integrates.schedulers.send_trial_engagement_notification import MAILER_MAP
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import GroupFaker, GroupStateFaker, OrganizationFaker, TrialFaker
from integrates.testing.mocks import Mock, mocks
from integrates.testing.utils import freeze_time


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(
                    created_by="johndoe@johndoe.com",
                    id="org1",
                    name="org1",
                ),
                OrganizationFaker(
                    created_by="janedoe@janedoe.com",
                    id="org2",
                    name="org2",
                ),
            ],
            groups=[
                GroupFaker(
                    name="group1",
                    created_by="johndoe@johndoe.com",
                    organization_id="org1",
                    state=GroupStateFaker(
                        managed=GroupManaged.TRIAL,
                    ),
                ),
                GroupFaker(
                    name="group2",
                    created_by="janedoe@janedoe.com",
                    organization_id="org2",
                    state=GroupStateFaker(
                        managed=GroupManaged.TRIAL,
                    ),
                ),
            ],
            trials=[
                TrialFaker(
                    email="johndoe@johndoe.com",
                    completed=False,
                    extension_date=None,
                    start_date=datetime.fromisoformat("2024-01-27T00:00:00+00:00"),
                ),
                TrialFaker(
                    email="janedoe@janedoe.com",
                    completed=False,
                    extension_date=None,
                    start_date=datetime.fromisoformat("2024-01-16T00:00:00+00:00"),
                ),
            ],
        ),
    ),
    others=[
        Mock(trial_mail, "send_mail_free_trial_2", "async", None),
        Mock(trial_mail, "send_mail_free_trial_4", "async", None),
    ],
)
@freeze_time("2024-01-30T00:00:00+00:00")
async def test_expire_free_trial() -> None:
    loaders: Dataloaders = get_new_context()
    groups = await get_all_trial_groups(loaders)
    emails = [group.created_by for group in groups]
    trials = await loaders.trial.load_many(emails)

    assert len(trials) == 2
    two_week_trial = next(
        trial for trial in trials if trial and trial.email == "janedoe@janedoe.com"
    )
    assert two_week_trial
    assert two_week_trial.start_date
    days_since = datetime_utils.get_days_since(two_week_trial.start_date)
    assert days_since == 14
    assert MAILER_MAP.get(days_since) is not None

    await send_trial_engagement_notification.main()
