import time

import boto3
from aioextensions import (
    collect,
)
from botocore.exceptions import (
    ClientError,
)

from integrates.custom_exceptions import (
    GroupNotFound,
)
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model.toe_lines.types import (
    GroupToeLinesRequest,
    ToeLine,
)
from integrates.dynamodb.exceptions import (
    UnavailabilityError,
)
from integrates.organizations.domain import (
    get_all_active_group_names,
)
from integrates.schedulers.common import (
    error,
    info,
)


def send_metric_to_cloudwatch(value: float) -> None:
    try:
        cloudwatch_client = boto3.client("cloudwatch", "us-east-1")
        cloudwatch_client.put_metric_data(
            Namespace="Integrates",
            MetricData=[
                {
                    "MetricName": "ToeLinesAccuracy",
                    "Value": value,
                    "Unit": "Percent",
                },
            ],
        )
    except ClientError as ex:
        error(
            "Error: An error ocurred sending metric to cloudwatch",
            extra={"ex": ex},
        )


def get_accuracy(amount: int, total: int) -> float:
    return round(amount / total * 100, 4) if total != 0 else 0


async def calculate_accuracy_by_toe_line(
    toe_line: ToeLine,
) -> tuple[int, int]:
    loc = toe_line.state.loc
    attacked_lines = toe_line.state.attacked_lines
    asserts = int(attacked_lines <= loc)

    if asserts == 0:
        info(
            "Unreliable ToE lines",
            extra={
                "loc": loc,
                "attacked_lines": attacked_lines,
                "toe_line": toe_line,
            },
        )

    return (asserts, 1)


async def calculate_accuracy_by_group(group_name: str, progress: float) -> tuple[int, int]:
    try:
        loaders: Dataloaders = get_new_context()
        group_toe_lines = await loaders.group_toe_lines.load_nodes(
            GroupToeLinesRequest(group_name=group_name),
        )
        if not group_toe_lines:
            return (0, 0)

        results = await collect(
            tuple(calculate_accuracy_by_toe_line(toe_line) for toe_line in group_toe_lines),
            workers=1,
        )
        values = (sum(v[0] for v in results), sum(v[1] for v in results))

        info(
            "Group ToE lines processed",
            extra={
                "group_name": group_name,
                "partial_accuracy": get_accuracy(values[0], values[1]),
                "progress": round(progress, 2),
            },
        )

        return values
    except (ClientError, GroupNotFound, TypeError, UnavailabilityError) as ex:
        msg = "Error: An error ocurred querying toe lines"
        error(msg, extra={"group_name": group_name, "ex": ex})

        return (0, 0)


async def add_new_metric() -> None:
    start_time = time.strftime("%Y-%m-%d at %H:%M:%S UTC")
    loaders: Dataloaders = get_new_context()
    groups: list[str] = await get_all_active_group_names(loaders)
    sorted_groups: list[str] = sorted(groups)
    total_groups: int = len(sorted_groups)

    results = await collect(
        tuple(
            calculate_accuracy_by_group(
                group_name=group,
                progress=count / total_groups,
            )
            for count, group in enumerate(sorted_groups)
        ),
        workers=1,
    )
    values = (sum(v[0] for v in results), sum(v[1] for v in results))

    send_metric_to_cloudwatch(value=get_accuracy(values[0], values[1]))

    info(
        "ToE lines accuracy metrics updated",
        extra={
            "total_groups": total_groups,
            "accuracy": get_accuracy(values[0], values[1]),
            "start_time": start_time,
            "end_time": time.strftime("%Y-%m-%d at %H:%M:%S UTC"),
        },
    )


async def main() -> None:
    await add_new_metric()
