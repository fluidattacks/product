from time import (
    sleep,
)

from botocore.exceptions import (
    ClientError,
)

from integrates.custom_utils.constants import (
    SCHEDULE_MACHINE_QUEUE_SAST_SCA_EMAIL,
)
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model.roots.enums import (
    RootCloningStatus,
    RootStatus,
)
from integrates.db_model.roots.types import (
    GitRoot,
)
from integrates.decorators import (
    retry_on_exceptions,
)
from integrates.jobs_orchestration.create_machine_config import (
    MachineJobOrigin,
    MachineJobTechnique,
    MachineModulesToExecute,
)
from integrates.jobs_orchestration.jobs import (
    queue_machine_job,
)
from integrates.organizations import (
    domain as orgs_domain,
)
from integrates.schedulers.common import (
    info,
    is_machine_target,
)

SCHEDULE_MODIFIED_BY = SCHEDULE_MACHINE_QUEUE_SAST_SCA_EMAIL


async def get_machine_groups(loaders: Dataloaders) -> list[str]:
    groups = await orgs_domain.get_all_active_groups(loaders)
    machine_group_names = sorted([group.name for group in groups if is_machine_target(group)])
    info("%s groups to process", len(machine_group_names))
    return machine_group_names


async def get_roots_to_execute(loaders: Dataloaders, group_name: str) -> set[str]:
    return {
        root.state.nickname
        for root in await loaders.group_roots.load(group_name)
        if (
            isinstance(root, GitRoot)
            and root.state.status == RootStatus.ACTIVE
            and root.cloning.status == RootCloningStatus.OK
        )
    }


@retry_on_exceptions(exceptions=(ClientError,), max_attempts=3, sleep_seconds=2.0)
async def queue_job(loaders: Dataloaders, group_name: str, root_nicknames: set[str]) -> None:
    await queue_machine_job(
        loaders=loaders,
        group_name=group_name,
        modified_by=SCHEDULE_MODIFIED_BY,
        root_nicknames=root_nicknames,
        modules_to_execute=MachineModulesToExecute(
            APK=False,
            CSPM=False,
            DAST=False,
            SAST=True,
            SCA=True,
        ),
        job_origin=MachineJobOrigin.SCHEDULER,
        job_technique=MachineJobTechnique.SAST,
    )


async def main() -> None:
    loaders = get_new_context()
    groups_to_execute = await get_machine_groups(loaders)
    total_roots = 0
    for group_name in groups_to_execute:
        root_nicknames = await get_roots_to_execute(loaders, group_name)
        curr_roots = len(root_nicknames)
        total_roots += curr_roots
        if root_nicknames:
            await queue_job(loaders, group_name, root_nicknames)
            info(
                "Queued machine static execution for %s roots in group %s",
                curr_roots,
                group_name,
            )
            sleep(5)
    info(
        "Queued machine static execution for %s roots in total",
        total_roots,
    )
