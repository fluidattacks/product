import os
import tempfile

from integrates.custom_utils.groups import get_group
from integrates.custom_utils.roots import get_active_git_roots
from integrates.dataloaders import Dataloaders, get_new_context
from integrates.db_model.groups.types import GroupUnreliableIndicators
from integrates.schedulers import nofluid_comments
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import GitRootFaker, GitRootStateFaker, GroupFaker, OrganizationFaker
from integrates.testing.mocks import Mock, mocks
from integrates.testing.utils import parametrize


class MockTemporaryDirectory:
    def __init__(self, prefix: str, ignore_cleanup_errors: bool, *args: object):
        pass

    def __enter__(self) -> str:
        return os.path.join(os.path.dirname(__file__), "test_data", "nofluid_comments")

    def __exit__(self, *args: object) -> None:
        pass


@parametrize(
    args=["root_nickname", "expected_output"],
    cases=[
        ["root1", (2, True)],
        ["root2", (4, False)],
    ],
)
def test_count_fluidattacks_file_exclusions(
    root_nickname: str,
    expected_output: tuple[int, bool],
) -> None:
    result = nofluid_comments.count_fluidattacks_file_exclusions(
        clone_path=os.path.join(os.path.dirname(__file__), "test_data", "nofluid_comments"),
        repo=root_nickname,
    )
    assert result == expected_output


@parametrize(
    args=["root_nickname", "expected_output"],
    cases=[
        ["root1", (3, True)],
        ["root2", (4, False)],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            groups=[GroupFaker(name="group1", organization_id="org1")],
            roots=[
                GitRootFaker(
                    group_name="group1",
                    id="root1",
                    state=GitRootStateFaker(nickname="root1"),
                ),
                GitRootFaker(
                    group_name="group1",
                    id="root2",
                    state=GitRootStateFaker(nickname="root2", branch="test"),
                ),
            ],
        ),
    ),
)
async def test_process_single_repository(
    root_nickname: str, expected_output: tuple[int, bool]
) -> None:
    loaders: Dataloaders = get_new_context()
    group_name = "group1"
    group = await get_group(loaders, group_name)
    valid_roots = await get_active_git_roots(loaders, group_name)
    result = await nofluid_comments.process_single_repository(
        repo=root_nickname,
        clone_path=os.path.join(os.path.dirname(__file__), "test_data", "nofluid_comments"),
        valid_roots=valid_roots,
        roots_by_nickname={root.state.nickname: root for root in valid_roots},
        group=group,
    )
    assert result == expected_output


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            groups=[GroupFaker(name="group1", organization_id="org1")],
            roots=[
                GitRootFaker(
                    group_name="group1",
                    id="root1",
                    state=GitRootStateFaker(nickname="root1"),
                ),
                GitRootFaker(
                    group_name="group1",
                    id="root2",
                    state=GitRootStateFaker(nickname="root2", branch="test"),
                ),
            ],
        ),
    ),
    others=[
        Mock(nofluid_comments, "download_repo", "async", "repo"),
        Mock(tempfile, "TemporaryDirectory", "function", MockTemporaryDirectory),
    ],
)
async def test_nofluid_comments() -> None:
    loaders: Dataloaders = get_new_context()
    group_name = "group1"
    group_indicators: GroupUnreliableIndicators = await loaders.group_unreliable_indicators.load(
        group_name,
    )
    roots = await loaders.group_roots.load(group_name)
    assert roots[0].unreliable_indicators.nofluid_quantity is None
    assert group_indicators.nofluid_quantity is None

    loaders.group_unreliable_indicators.clear(group_name)
    loaders.group_roots.clear(group_name)

    await nofluid_comments.main()

    group_new_indicators: GroupUnreliableIndicators = (
        await loaders.group_unreliable_indicators.load(group_name)
    )
    roots = await loaders.group_roots.load(group_name)
    assert len(roots) == 2
    assert {root.state.nickname: root.unreliable_indicators.nofluid_quantity for root in roots} == {
        "root1": 3,
        "root2": 4,
    }
    assert group_new_indicators.nofluid_quantity == 7
