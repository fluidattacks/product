from contextlib import suppress
from operator import attrgetter
from typing import cast

from aioextensions import collect
from botocore.exceptions import ClientError

from integrates.custom_exceptions import GroupNotFound, IndicatorAlreadyUpdated
from integrates.custom_utils.findings import get_group_findings
from integrates.dataloaders import Dataloaders, get_new_context
from integrates.db_model.groups.types import Group
from integrates.dynamodb.exceptions import UnavailabilityError
from integrates.groups import domain as groups_domain
from integrates.organizations import domain as orgs_domain
from integrates.schedulers.common import error, info
from integrates.unreliable_indicators.enums import Entity, EntityAttr
from integrates.unreliable_indicators.model import ENTITIES
from integrates.unreliable_indicators.operations import (
    update_findings_unreliable_indicators,
    update_vulnerabilities_unreliable_indicators,
)
from integrates.unreliable_indicators.utils import get_group_indicators


async def update_vulnerabilities_indicators(loaders: Dataloaders, group: str) -> None:
    findings = await get_group_findings(group_name=group, loaders=loaders)
    vulnerabilities = await loaders.finding_vulnerabilities.load_many_chained(
        [finding.id for finding in findings],
    )
    await update_vulnerabilities_unreliable_indicators(
        [vulnerability.id for vulnerability in vulnerabilities],
        set(cast(dict, ENTITIES[Entity.vulnerability]["attrs"]).keys()),
    )


async def update_findings_indicators(group: str) -> None:
    loaders = get_new_context()
    findings = await get_group_findings(group_name=group, loaders=loaders)

    await update_findings_unreliable_indicators(
        [finding.id for finding in findings],
        {
            EntityAttr.status,
            EntityAttr.treatment_summary,
            EntityAttr.where,
        },
    )

    vulnerabilities = await loaders.finding_vulnerabilities_all.load_many(
        [finding.id for finding in findings]
    )
    await update_findings_unreliable_indicators(
        [
            finding.id
            for finding, _vulnerabilities in zip(findings, vulnerabilities, strict=False)
            if _vulnerabilities
        ],
        {
            EntityAttr.status,
            EntityAttr.max_open_severity_score,
            EntityAttr.max_open_severity_score_v4,
            EntityAttr.total_open_cvssf,
            EntityAttr.total_open_cvssf_v4,
            EntityAttr.oldest_vulnerability_report_date,
            EntityAttr.newest_vulnerability_report_date,
            EntityAttr.treatment_summary,
            EntityAttr.where,
            EntityAttr.vulnerabilities_summary,
            EntityAttr.zero_risk_summary,
        },
    )


async def update_group_indicators(group: Group, progress: float) -> None:
    try:
        loaders: Dataloaders = get_new_context()
        indicators = await get_group_indicators(loaders, group)
        await update_vulnerabilities_indicators(loaders, group.name)
        with suppress(IndicatorAlreadyUpdated):
            await update_findings_indicators(group.name)

        await groups_domain.update_indicators(
            group_name=group.name,
            indicators=indicators,
        )
        info(
            "Group indicators processed",
            extra={"group_name": group.name, "progress": round(progress, 2)},
        )
    except (ClientError, GroupNotFound, TypeError, UnavailabilityError) as ex:
        msg = "Error: An error ocurred updating indicators in the database"
        error(msg, extra={"group_name": group.name, "ex": ex})


async def update_indicators() -> None:
    """Update in dynamo indicators."""
    groups = await orgs_domain.get_all_active_groups(loaders=get_new_context())
    groups_sorted_by_name = sorted(groups, key=attrgetter("name"))
    len_groups_sorted_by_name = len(groups_sorted_by_name)
    await collect(
        tuple(
            update_group_indicators(
                group=group,
                progress=count / len_groups_sorted_by_name,
            )
            for count, group in enumerate(groups_sorted_by_name)
        ),
        workers=1,
    )


async def main() -> None:
    await update_indicators()
