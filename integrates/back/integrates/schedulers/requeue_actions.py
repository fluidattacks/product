import itertools
import logging
import re
from operator import (
    attrgetter,
)

from aioextensions import (
    collect,
)

from integrates.batch import (
    dal as batch_dal,
)
from integrates.batch.enums import (
    Action,
    Product,
)
from integrates.batch.types import (
    BatchProcessing,
    BatchProcessingToUpdate,
)
from integrates.batch.utils import (
    get_product,
)
from integrates.class_types.types import (
    Item,
)
from integrates.jobs_orchestration.create_machine_config import (
    generate_machine_job_name,
)
from integrates.jobs_orchestration.create_sbom_config import (
    generate_sbom_job_name,
)
from integrates.jobs_orchestration.model.types import SbomJobScope, SbomJobType

LOGGER = logging.getLogger(__name__)


def _get_new_job_name_for_machine_actions(
    action: BatchProcessing,
    batch_job_name: str | None,
) -> str | None:
    if (
        get_product(action.action_name) == Product.SKIMS
        and batch_job_name
        and (name_parts := batch_job_name.split("_"))
        and len(name_parts) >= 5
    ):
        return generate_machine_job_name(name_parts[1], "requeue", name_parts[3], name_parts[4])
    if (
        batch_job_name
        and batch_job_name.startswith("sbom_")
        and (name_parts := batch_job_name.split("_"))
        and len(name_parts) >= 5
    ):
        job_scope = (
            SbomJobScope.DOCKER_IMAGE if "docker" in name_parts[3] else SbomJobScope(name_parts[3])
        )
        return generate_sbom_job_name(
            group_name=name_parts[1],
            job_type=SbomJobType.REQUEUE,
            job_scope=job_scope,
            job_uuid=name_parts[-1],
        )
    return None


async def _get_batch_jobs(
    actions_to_requeue: list[BatchProcessing],
) -> dict[str, Item]:
    """Filters actions that should not be sent to Batch"""
    batch_jobs_dict: dict[str, Item] = {
        job["jobId"]: job
        for job in await batch_dal.describe_jobs(
            *[
                action.batch_job_id
                for action in actions_to_requeue
                if action.batch_job_id is not None  # Check to comply with Mypy
                and re.match(r"[a-zA-Z_0-9-]{1,128}", action.batch_job_id)
            ],
        )
    }

    return batch_jobs_dict


async def _filter_non_requeueable_actions(
    actions_to_requeue: list[BatchProcessing],
    batch_jobs_dict: dict[str, Item],
) -> list[BatchProcessing]:
    """Filters actions that should not be sent to Batch"""
    _keys_to_delete = [
        action.key
        for action in actions_to_requeue
        if action.action_name
        in {
            Action.UPDATE_ORGANIZATION_REPOSITORIES,
        }
    ]

    succeeded_keys_to_delete: list[str] = [
        action.key
        for action in actions_to_requeue
        if action.batch_job_id
        if (batch_jobs_dict.get(action.batch_job_id, {"status": None})["status"] == "SUCCEEDED")
        # remove false positives jobs failed
        or (
            batch_jobs_dict.get(action.batch_job_id, {"status": None})["status"] == "FAILED"
            and (
                # canceled jobs
                "stoppedAt" not in batch_jobs_dict[action.batch_job_id]
                # false positive, the job has status FAILED but make ends
                # in success
                or "CannotInspectContainerError"
                in batch_jobs_dict[action.batch_job_id]["container"].get("reason", "")
            )
        )
    ]
    retried_keys_to_delete = []
    for action in actions_to_requeue:
        max_retries = 2
        if action.action_name is not Action.REMOVE_GROUP_RESOURCES and action.retries > max_retries:
            retried_keys_to_delete.append(action.key)
            LOGGER.error(
                "Batch action exceeded number of retries",
                extra={
                    "extra": {
                        "action_name": action.action_name,
                        "entity": action.entity,
                        "last_batch_job": action.batch_job_id,
                        "additional_info": action.additional_info,
                        "max_retries": max_retries,
                    },
                },
            )

    keys_to_delete = [
        *succeeded_keys_to_delete,
        *retried_keys_to_delete,
        *_keys_to_delete,
    ]

    await collect(
        [batch_dal.delete_action(action_key=key) for key in set(keys_to_delete)],
        workers=16,
    )

    active_keys: list[str] = [
        action.key
        for action in actions_to_requeue
        if action.running
        and action.batch_job_id
        and batch_jobs_dict.get(action.batch_job_id, {"status": None})["status"]
        in [
            "RUNNING",
        ]
    ]
    pending_keys: list[str] = [
        action.key
        for action in actions_to_requeue
        if not action.running
        and action.batch_job_id
        and batch_jobs_dict.get(action.batch_job_id, {"status": None})["status"]
        in {
            "SUBMITTED",
            "PENDING",
            "RUNNABLE",
            "STARTING",
            "RUNNING",  # makes setup
        }
    ]

    return [
        action
        for action in actions_to_requeue
        if action.key not in set(active_keys + pending_keys + keys_to_delete)
    ]


async def _consolidate_entity_actions(
    actions: list[BatchProcessing],
) -> BatchProcessing:
    if len(actions) == 1:
        return actions[0]

    root_ids = set(
        itertools.chain.from_iterable(action.additional_info["root_ids"] for action in actions),
    )
    action_to_update = actions[0]._replace(additional_info={"root_ids": list(root_ids)})
    await batch_dal.update_action_to_dynamodb(
        action=action_to_update.action_name,
        action_key=action_to_update.key,
        attributes=BatchProcessingToUpdate(
            additional_info={"root_ids": list(root_ids)},
            attempt_duration_seconds=7200,
            retries=0,
        ),
    )
    await collect(
        [
            batch_dal.delete_action(action_key=action.key)
            for action in actions
            if action.key != action_to_update.key
        ],
        workers=16,
    )

    return action_to_update


async def _filter_duplicated_actions(
    actions_to_requeue: list[BatchProcessing],
    action_to_filter: Action,
) -> list[BatchProcessing]:
    if action_to_filter not in {
        Action.REFRESH_TOE_INPUTS,
        Action.REFRESH_TOE_LINES,
        Action.REFRESH_TOE_PORTS,
    }:
        return actions_to_requeue

    filtered_actions = [
        action
        for action in actions_to_requeue
        if action.action_name == action_to_filter and not action.running
    ]
    actions_to_update = await collect(
        [
            _consolidate_entity_actions(list(group_iter))
            for _, group_iter in itertools.groupby(filtered_actions, key=attrgetter("entity"))
        ],
        workers=16,
    )
    remaining_actions: list[BatchProcessing] = [
        action
        for action in actions_to_requeue
        if action.key not in {action.key for action in filtered_actions}
    ]

    return list(actions_to_update) + remaining_actions


async def requeue_actions() -> None:
    actions_to_requeue: list[BatchProcessing] = await batch_dal.get_actions()
    actions_to_requeue = await _filter_duplicated_actions(
        actions_to_requeue,
        Action.REFRESH_TOE_INPUTS,
    )
    actions_to_requeue = await _filter_duplicated_actions(
        actions_to_requeue,
        Action.REFRESH_TOE_LINES,
    )
    actions_to_requeue = await _filter_duplicated_actions(
        actions_to_requeue,
        Action.REFRESH_TOE_PORTS,
    )
    actions_to_requeue = [
        action._replace(retries=action.retries + 1) for action in actions_to_requeue
    ]
    batch_jobs = await _get_batch_jobs(actions_to_requeue)

    actions_to_requeue = await _filter_non_requeueable_actions(actions_to_requeue, batch_jobs)

    new_batch_jobs_ids = await collect(
        [
            batch_dal.put_action_to_batch(
                action=action.action_name,
                attempt_duration_seconds=action.attempt_duration_seconds,
                action_dynamo_pk=action.key,
                entity=action.entity,
                queue=action.queue,
                job_name=_get_new_job_name_for_machine_actions(
                    action,
                    batch_jobs.get(str(action.batch_job_id), {}).get("jobName"),
                ),
            )
            for action in actions_to_requeue
        ],
        workers=16,
    )
    await collect(
        [
            batch_dal.update_action_to_dynamodb(
                action=action.action_name,
                action_key=action.key,
                attributes=BatchProcessingToUpdate(
                    batch_job_id=job_id,
                    retries=action.retries,
                    running=False,
                ),
            )
            for action, job_id in zip(actions_to_requeue, new_batch_jobs_ids, strict=False)
        ],
        workers=16,
    )


async def main() -> None:
    await requeue_actions()
