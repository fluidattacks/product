from datetime import datetime

from integrates.dataloaders import get_new_context
from integrates.db_model.stakeholders.types import NotificationsPreferences
from integrates.schedulers.abandoned_trial_notification import get_stakeholders_to_notify
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    GitRootFaker,
    GitRootStateFaker,
    GroupFaker,
    OrganizationFaker,
    StakeholderFaker,
    StakeholderStateFaker,
)
from integrates.testing.mocks import mocks
from integrates.testing.utils import freeze_time


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            groups=[GroupFaker(name="group1", organization_id="org1")],
            roots=[
                GitRootFaker(
                    group_name="group1", id="root1", state=GitRootStateFaker(use_ztna=True)
                )
            ],
            stakeholders=[
                StakeholderFaker(
                    email="user1@test.test",
                    is_registered=True,
                    registration_date=datetime.fromisoformat("2022-11-09T15:58:31.280182"),
                ),
                StakeholderFaker(
                    email="user2@test.test",
                    is_registered=True,
                    registration_date=datetime.fromisoformat("2022-10-21T15:50:31.280182"),
                    state=StakeholderStateFaker(
                        modified_by="unknown",
                        modified_date=datetime.fromisoformat("2022-10-24T00:00:00+00:00"),
                        notifications_preferences=NotificationsPreferences(
                            email=["UPDATED_TREATMENT"]
                        ),
                    ),
                ),
                StakeholderFaker(
                    email="user3@test.test",
                    is_registered=True,
                    registration_date=datetime.fromisoformat("2022-11-11T14:58:31.280182"),
                ),
                StakeholderFaker(
                    email="user4@test.test",
                    is_registered=True,
                    registration_date=datetime.fromisoformat("2022-11-10T15:58:31.280182"),
                ),
            ],
        ),
    )
)
@freeze_time("2022-11-11T15:58:31.280182")
async def test_get_stakeholders_to_notify() -> None:
    loaders = get_new_context()
    to_notified = sorted(await get_stakeholders_to_notify(loaders=loaders))

    assert len(to_notified) == 3
    assert to_notified[0][0].email == "user1@test.test"
    assert to_notified[1][0].email == "user3@test.test"
    assert to_notified[2][0].email == "user4@test.test"
    assert to_notified[0][1] == 48
    assert to_notified[1][1] == 1
    assert to_notified[2][1] == 24
