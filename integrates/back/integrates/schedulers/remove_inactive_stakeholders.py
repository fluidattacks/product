import re
from datetime import (
    datetime,
)

from aioextensions import (
    collect,
)

from integrates.custom_exceptions import (
    CredentialInUse,
)
from integrates.custom_utils import (
    datetime as datetime_utils,
)
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model import (
    organizations as orgs_model,
)
from integrates.db_model import (
    stakeholders as stakeholders_model,
)
from integrates.db_model.constants import (
    DEFAULT_INACTIVITY_PERIOD,
)
from integrates.db_model.organizations.types import (
    Organization,
)
from integrates.db_model.stakeholders.types import (
    Stakeholder,
)
from integrates.organizations import (
    domain as orgs_domain,
)
from integrates.schedulers.common import (
    info,
)
from integrates.stakeholders import (
    domain as stakeholders_domain,
)


def _is_forces_stakeholder(stakeholder_email: str) -> bool:
    return bool(re.match(r"^forces\..*@fluidattacks.com$", stakeholder_email))


def get_inactivity_days(stakeholder: Stakeholder) -> int:
    return (datetime_utils.get_utc_now() - _get_last_activity(stakeholder)).days


def _get_last_activity(stakeholder: Stakeholder) -> datetime:
    last_login_date = (
        stakeholder.last_login_date
        if stakeholder.last_login_date is not None
        else datetime.fromisoformat(datetime_utils.DEFAULT_ISO_STR)
    )

    last_api_token_use_dates = [
        token.last_use if token.last_use else datetime.fromisoformat(datetime_utils.DEFAULT_ISO_STR)
        for token in stakeholder.access_tokens
    ]

    return max([last_login_date, *last_api_token_use_dates])


async def process_stakeholder(
    stakeholder: Stakeholder,
) -> None:
    if stakeholder.last_login_date is None:
        return

    loaders: Dataloaders = get_new_context()
    has_orgs = bool(await loaders.stakeholder_organizations_access.load(stakeholder.email))
    if has_orgs:
        return

    inactivity_days = get_inactivity_days(stakeholder)
    if inactivity_days < DEFAULT_INACTIVITY_PERIOD:
        return

    await stakeholders_domain.remove(stakeholder.email)
    info(
        "Inactive stakeholder removed",
        extra={
            "email": stakeholder.email,
            "inactivity_days": inactivity_days,
            "last_login_date": stakeholder.last_login_date,
        },
    )


async def _remove_access(
    organization: Organization,
    stakeholder: Stakeholder,
    errors: dict[str, int],
) -> None:
    try:
        await orgs_domain.remove_access(
            organization_id=organization.id,
            email=stakeholder.email,
            modified_by=orgs_domain.EMAIL_INTEGRATES,
        )
    except CredentialInUse as exc:
        key = str(exc)
        errors[key] = errors.get(key, 0) + 1


async def process_organization(
    organization: Organization,
) -> None:
    loaders: Dataloaders = get_new_context()
    inactivity_period_policy = organization.policies.inactivity_period or DEFAULT_INACTIVITY_PERIOD
    org_stakeholders = await orgs_domain.get_stakeholders(
        loaders=loaders,
        organization_id=organization.id,
    )
    inactive_stakeholders = [
        stakeholder
        for stakeholder in org_stakeholders
        if stakeholder
        and not _is_forces_stakeholder(stakeholder.email)
        and stakeholder.last_login_date
        and get_inactivity_days(stakeholder) > inactivity_period_policy
    ]
    errors: dict[str, int] = {}
    if not inactive_stakeholders:
        return

    await collect(
        tuple(
            _remove_access(
                organization=organization,
                stakeholder=stakeholder,
                errors=errors,
            )
            for stakeholder in inactive_stakeholders
        ),
        workers=1,
    )

    if errors:
        errors["total"] = sum(errors.values())

    info(
        "Organization processed",
        extra={
            "organization_id": organization.id,
            "inactivity_policy": inactivity_period_policy,
            "inactive_stakeholders": len(inactive_stakeholders),
            "errors": errors if errors else None,
        },
    )


async def remove_inactive_stakeholders() -> None:
    """Remove stakeholders for inactivity (no logins) in the defined period."""
    all_organizations = await orgs_model.get_all_organizations()
    info("Organizations to process", extra={"item": len(all_organizations)})
    await collect(
        tuple(
            process_organization(
                organization=organization,
            )
            for organization in all_organizations
        ),
        workers=1,
    )

    all_stakeholders = await stakeholders_model.get_all_stakeholders()
    info("Stakeholders to process", extra={"item": len(all_stakeholders)})
    await collect(
        tuple(
            process_stakeholder(
                stakeholder=stakeholder,
            )
            for stakeholder in all_stakeholders
        ),
        workers=1,
    )


async def main() -> None:
    await remove_inactive_stakeholders()
