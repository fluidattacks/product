from datetime import datetime

from integrates.dataloaders import Dataloaders, get_new_context
from integrates.db_model.groups.enums import GroupManaged, GroupStateStatus
from integrates.schedulers import expire_free_trial
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import GroupFaker, GroupStateFaker, OrganizationFaker, TrialFaker
from integrates.testing.mocks import mocks
from integrates.testing.utils import freeze_time, parametrize


@freeze_time("2022-11-11T15:58:31.280182")
@parametrize(
    args=[
        "email",
        "group_name",
        "completed_before",
        "managed_before",
        "completed_after",
        "managed_after",
    ],
    cases=[
        # Reached trial limit
        [
            "johndoe@johndoe.com",
            "group1",
            False,
            GroupManaged.TRIAL,
            True,
            GroupManaged.UNDER_REVIEW,
        ],
        # Still has remaining days
        ["janedoe@janedoe.com", "group2", False, GroupManaged.TRIAL, False, GroupManaged.TRIAL],
        # Reached trial limit but was granted an extension
        [
            "uiguaran@uiguaran.com",
            "group3",
            False,
            GroupManaged.TRIAL,
            False,
            GroupManaged.TRIAL,
        ],
        # Has already completed the trial
        [
            "abuendia@abuendia.com",
            "group4",
            True,
            GroupManaged.MANAGED,
            True,
            GroupManaged.MANAGED,
        ],
        # With trial expired and group's resources to be removed from db
        [
            "atoriyama@atoriyama.com",
            "group5",
            True,
            GroupManaged.UNDER_REVIEW,
            True,
            None,  # Group not found
        ],
        # With trial expired but group's resources still to be kept in db
        [
            "hanno@hanno.com",
            "group6",
            True,
            GroupManaged.UNDER_REVIEW,
            True,
            GroupManaged.UNDER_REVIEW,
        ],
        # With trial expired but group has already been removed
        [
            "jqpublic@jqpublic.com",
            "group7",
            True,
            GroupManaged.UNDER_REVIEW,
            True,
            GroupManaged.UNDER_REVIEW,
        ],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(
                    created_by="johndoe@johndoe.com",
                    id="e314a87c-223f-44bc-8317-75900f2ffbc7",
                    created_date=datetime.fromisoformat("2022-10-21T15:58:31.280182+00:00"),
                    name="org1",
                ),
                OrganizationFaker(
                    created_by="janedoe@janedoe.com",
                    id="5ee9880b-5e19-44ba-baf1-f2601bdf7d25",
                    created_date=datetime.fromisoformat("2022-10-21T15:58:31.280182+00:00"),
                    name="org2",
                ),
                OrganizationFaker(
                    created_by="uiguaran@uiguaran.com",
                    id="a2204896-fbd0-4c55-8163-4cb3b018551c",
                    created_date=datetime.fromisoformat("2022-10-21T15:58:31.280182+00:00"),
                    name="org3",
                ),
                OrganizationFaker(
                    created_by="abuendia@abuendia.com",
                    id="5399f49f-6e2c-4712-af72-5ea6e34cf15d",
                    created_date=datetime.fromisoformat("2022-10-21T15:58:31.280182+00:00"),
                    name="org4",
                ),
                OrganizationFaker(
                    created_by="atoriyama@atoriyama.com",
                    id="5399f49f-6e2c-4712-af72-5ea6e34cf155",
                    created_date=datetime.fromisoformat("2022-10-21T15:58:31.280182+00:00"),
                    name="org5",
                ),
                OrganizationFaker(
                    created_by="hanno@hanno.com",
                    id="5399f49f-6e2c-4712-af72-5ea6e34cf156",
                    created_date=datetime.fromisoformat("2022-10-21T15:58:31.280182+00:00"),
                    name="org6",
                ),
                OrganizationFaker(
                    created_by="jqpublic@jqpublic.com",
                    id="5399f49f-6e2c-4712-af72-5ea6e34cf100",
                    created_date=datetime.fromisoformat("2022-10-21T15:58:31.280182+00:00"),
                    name="org7",
                ),
            ],
            groups=[
                GroupFaker(
                    name="group1",
                    created_by="johndoe@johndoe.com",
                    organization_id="e314a87c-223f-44bc-8317-75900f2ffbc7",
                    state=GroupStateFaker(managed=GroupManaged.TRIAL),
                ),
                GroupFaker(
                    name="group2",
                    created_by="janedoe@janedoe.com",
                    organization_id="5ee9880b-5e19-44ba-baf1-f2601bdf7d25",
                    state=GroupStateFaker(managed=GroupManaged.TRIAL),
                ),
                GroupFaker(
                    name="group3",
                    created_by="uiguaran@uiguaran.com",
                    organization_id="a2204896-fbd0-4c55-8163-4cb3b018551c",
                    state=GroupStateFaker(managed=GroupManaged.TRIAL),
                ),
                GroupFaker(
                    name="group4",
                    created_by="abuendia@abuendia.com",
                    organization_id="5399f49f-6e2c-4712-af72-5ea6e34cf15d",
                    state=GroupStateFaker(managed=GroupManaged.MANAGED),
                ),
                GroupFaker(
                    name="group5",
                    created_by="atoriyama@atoriyama.com",
                    organization_id="5399f49f-6e2c-4712-af72-5ea6e34cf155",
                    state=GroupStateFaker(managed=GroupManaged.UNDER_REVIEW),
                ),
                GroupFaker(
                    name="group6",
                    created_by="hanno@hanno.com",
                    organization_id="5399f49f-6e2c-4712-af72-5ea6e34cf156",
                    state=GroupStateFaker(managed=GroupManaged.UNDER_REVIEW),
                ),
                GroupFaker(
                    name="group7",
                    created_by="jqpublic@jqpublic.com",
                    organization_id="5399f49f-6e2c-4712-af72-5ea6e34cf100",
                    state=GroupStateFaker(
                        managed=GroupManaged.UNDER_REVIEW, status=GroupStateStatus.DELETED
                    ),
                ),
            ],
            trials=[
                TrialFaker(
                    email="johndoe@johndoe.com",
                    completed=False,
                    extension_date=None,
                    start_date=datetime.fromisoformat("2022-10-21T15:58:31.280182+00:00"),
                ),
                TrialFaker(
                    email="janedoe@janedoe.com",
                    completed=False,
                    extension_date=None,
                    start_date=datetime.fromisoformat("2022-10-22T15:58:31.280182+00:00"),
                ),
                TrialFaker(
                    email="uiguaran@uiguaran.com",
                    completed=False,
                    extension_date=datetime.fromisoformat("2022-11-11T15:58:31.280182+00:00"),
                    extension_days=1,
                    start_date=datetime.fromisoformat("2022-10-21T15:58:31.280182+00:00"),
                ),
                TrialFaker(
                    email="abuendia@abuendia.com",
                    completed=True,
                    extension_date=None,
                    start_date=datetime.fromisoformat("2022-10-21T15:58:31.280182+00:00"),
                ),
                TrialFaker(
                    email="atoriyama@atoriyama.com",
                    completed=True,
                    extension_date=None,
                    start_date=datetime.fromisoformat("2022-09-20T15:58:31.280182+00:00"),
                ),
                TrialFaker(
                    email="hanno@hanno.com",
                    completed=True,
                    extension_date=None,
                    start_date=datetime.fromisoformat("2022-09-21T15:58:31.280182+00:00"),
                ),
                TrialFaker(
                    email="jqpublic@jqpublic.com",
                    completed=True,
                    extension_date=None,
                    start_date=datetime.fromisoformat("2022-09-20T15:58:31.280182+00:00"),
                ),
            ],
        ),
    )
)
async def test_expire_free_trial(
    email: str,
    group_name: str,
    completed_before: bool,
    managed_before: GroupManaged,
    completed_after: bool,
    managed_after: GroupManaged,
) -> None:
    loaders: Dataloaders = get_new_context()

    group_before = await loaders.group.load(group_name)
    trial_before = await loaders.trial.load(email)

    assert group_before
    assert trial_before
    assert trial_before.completed == completed_before
    assert group_before.state.managed == managed_before

    await expire_free_trial.main()

    loaders.group.clear_all()
    loaders.trial.clear_all()

    group_after = await loaders.group.load(group_name)
    trial_after = await loaders.trial.load(email)

    assert trial_after
    assert trial_after.completed == completed_after

    if managed_after is not None:
        assert group_after
        assert group_after.state.managed == managed_after
    else:
        assert group_after is None
