from datetime import UTC, datetime
from decimal import Decimal

from integrates.db_model.enums import Source
from integrates.db_model.findings.enums import FindingVerificationStatus
from integrates.db_model.findings.types import FindingEvidence, FindingEvidences
from integrates.db_model.vulnerabilities.enums import (
    VulnerabilityStateStatus,
    VulnerabilityTechnique,
    VulnerabilityType,
    VulnerabilityVerificationStatus,
)
from integrates.db_model.vulnerabilities.types import VulnerabilityVerification
from integrates.roots.domain import format_environment_id
from integrates.schedulers import numerator_report_digest
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    DATE_2019,
    EMAIL_FLUIDATTACKS_HACKER,
    FindingFaker,
    FindingVerificationFaker,
    GitRootFaker,
    GitRootStateFaker,
    GroupAccessFaker,
    GroupAccessStateFaker,
    GroupFaker,
    IPRootFaker,
    IPRootStateFaker,
    OrganizationFaker,
    RootEnvironmentUrlFaker,
    RootEnvironmentUrlStateFaker,
    SeverityScoreFaker,
    StakeholderFaker,
    ToeInputFaker,
    ToeLinesFaker,
    ToeLinesStateFaker,
    ToePortFaker,
    ToePortStateFaker,
    VulnerabilityFaker,
    VulnerabilityStateFaker,
)
from integrates.testing.mocks import Mock, mocks


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(id="org1"),
            ],
            groups=[
                GroupFaker(organization_id="org1"),
            ],
            roots=[
                GitRootFaker(
                    id="root1",
                    state=GitRootStateFaker(
                        gitignore=["bower_components/*", "node_modules/*"],
                        includes_health_check=True,
                        nickname="universe",
                        url="https://gitlab.com/fluidattacks/universe",
                    ),
                ),
                GitRootFaker(
                    id="root2",
                    state=GitRootStateFaker(
                        gitignore=["node_modules/*"],
                        includes_health_check=True,
                        nickname="universe123",
                        url="https://gitlab.com/fluidattacks/universe2",
                    ),
                ),
                GitRootFaker(
                    id="root4",
                    state=GitRootStateFaker(
                        branch="main",
                        gitignore=["node_modules/*"],
                        includes_health_check=True,
                        nickname="universe99",
                        url="https://gitlab.com/fluidattacks/universetest",
                    ),
                ),
                IPRootFaker(
                    id="root5",
                    state=IPRootStateFaker(
                        address="192.168.1.44",
                        nickname="universe44",
                    ),
                ),
                IPRootFaker(
                    id="root6",
                    state=IPRootStateFaker(
                        address="192.168.1.45",
                        nickname="universe45",
                    ),
                ),
                IPRootFaker(
                    id="root7",
                    state=IPRootStateFaker(
                        address="192.168.1.46",
                        nickname="universe46",
                    ),
                ),
                IPRootFaker(
                    id="root8",
                    state=IPRootStateFaker(
                        address="192.168.1.47",
                        nickname="universe47",
                    ),
                ),
                IPRootFaker(
                    id="root9",
                    state=IPRootStateFaker(
                        address="192.168.1.57",
                        nickname="universe57",
                    ),
                ),
            ],
            root_environment_urls=[
                RootEnvironmentUrlFaker(
                    id=format_environment_id("https://test.com/"),
                    url="https://test.com/",
                    group_name="test-group",
                    root_id="root1",
                    state=RootEnvironmentUrlStateFaker(),
                ),
                RootEnvironmentUrlFaker(
                    id=format_environment_id("https://testtest.com/"),
                    url="https://testtest.com/",
                    group_name="test-group",
                    root_id="root2",
                    state=RootEnvironmentUrlStateFaker(),
                ),
                RootEnvironmentUrlFaker(
                    id=format_environment_id("https://test.test/GetList?=1234&page=1&start=56789"),
                    url="https://test.test/GetList?=1234&page=1&start=56789",
                    group_name="group2",
                    root_id="root3",
                    state=RootEnvironmentUrlStateFaker(),
                ),
                RootEnvironmentUrlFaker(
                    id=format_environment_id("https://test.com/test"),
                    url="https://test.com/test",
                    group_name="test_group",
                    root_id="root4",
                    state=RootEnvironmentUrlStateFaker(),
                ),
            ],
            findings=[
                FindingFaker(
                    id="3c475384-834c-47b0-ac71-a41a022e401c",
                    evidences=FindingEvidences(
                        evidence1=FindingEvidence(
                            description="evidence1",
                            url=("group1-3c475384-834c-47b0-ac71-a41a022e401c-evidence1"),
                            modified_date=datetime.fromisoformat("2020-11-19T13:37:10+00:00"),
                        ),
                    ),
                    verification=FindingVerificationFaker(
                        modified_by=EMAIL_FLUIDATTACKS_HACKER,
                        status=FindingVerificationStatus.VERIFIED,
                        vulnerability_ids={"vuln1", "vuln2"},
                    ),
                ),
            ],
            stakeholders=[
                StakeholderFaker(email=EMAIL_FLUIDATTACKS_HACKER, role="hacker"),
            ],
            group_access=[
                GroupAccessFaker(
                    email=EMAIL_FLUIDATTACKS_HACKER,
                    state=GroupAccessStateFaker(has_access=True, role="hacker"),
                ),
            ],
            toe_inputs=[
                ToeInputFaker(component="https://example.com", entry_point="phone", root_id="root1")
            ],
            toe_lines=[
                ToeLinesFaker(
                    filename="path/to/file1.ext",
                    root_id="root1",
                    state=ToeLinesStateFaker(
                        attacked_at=DATE_2019,
                        attacked_lines=4324,
                        loc=4324,
                        sorts_priority_factor=60,
                    ),
                ),
                ToeLinesFaker(
                    filename="path/to/file22.ext", root_id="root4", state=ToeLinesStateFaker(loc=99)
                ),
                ToeLinesFaker(
                    filename="test/1", root_id="root1", state=ToeLinesStateFaker(loc=100)
                ),
            ],
            toe_ports=[
                ToePortFaker(
                    address="192.168.1.44",
                    port="4444",
                    root_id="root5",
                    state=ToePortStateFaker(
                        seen_at=datetime.fromisoformat("2019-04-09T12:59:52+00:00")
                    ),
                ),
                ToePortFaker(
                    address="192.168.1.45",
                    port="4545",
                    root_id="root6",
                    state=ToePortStateFaker(
                        attacked_at=datetime.fromisoformat("2019-04-09T12:59:52+00:00"),
                        attacked_by=EMAIL_FLUIDATTACKS_HACKER,
                        seen_at=datetime.fromisoformat("2019-04-09T12:59:52+00:00"),
                    ),
                ),
                ToePortFaker(
                    address="192.168.1.45",
                    port="4848",
                    root_id="root6",
                    state=ToePortStateFaker(
                        attacked_at=DATE_2019, attacked_by=EMAIL_FLUIDATTACKS_HACKER
                    ),
                ),
                ToePortFaker(address="192.168.1.46", port="4646", root_id="root7"),
                ToePortFaker(address="192.168.1.47", port="4747", root_id="root8"),
            ],
            vulnerabilities=[
                VulnerabilityFaker(
                    id="vuln1",
                    finding_id="3c475384-834c-47b0-ac71-a41a022e401c",
                    root_id="root5",
                    state=VulnerabilityStateFaker(specific="4444", where="192.168.1.44"),
                    type=VulnerabilityType.PORTS,
                    verification=VulnerabilityVerification(
                        modified_by="unittest@fluidattacks.com",
                        modified_date=DATE_2019,
                        status=VulnerabilityVerificationStatus.REQUESTED,
                    ),
                ),
                VulnerabilityFaker(
                    id="vuln2",
                    finding_id="3c475384-834c-47b0-ac71-a41a022e401c",
                    root_id="root6",
                    severity_score=SeverityScoreFaker(threat_score=Decimal("1.1")),
                    state=VulnerabilityStateFaker(
                        specific="4545",
                        where="192.168.1.45",
                        status=VulnerabilityStateStatus.SUBMITTED,
                    ),
                    type=VulnerabilityType.PORTS,
                    verification=VulnerabilityVerification(
                        modified_date=DATE_2019,
                        status=VulnerabilityVerificationStatus.REQUESTED,
                    ),
                ),
                VulnerabilityFaker(
                    id="vuln3",
                    finding_id="3c475384-834c-47b0-ac71-a41a022e401c",
                    root_id="root6",
                    severity_score=SeverityScoreFaker(threat_score=Decimal("5.1")),
                    state=VulnerabilityStateFaker(specific="4848", where="192.168.1.45"),
                    type=VulnerabilityType.PORTS,
                    verification=VulnerabilityVerification(
                        modified_by=EMAIL_FLUIDATTACKS_HACKER,
                        modified_date=DATE_2019,
                        status=VulnerabilityVerificationStatus.VERIFIED,
                    ),
                ),
                VulnerabilityFaker(
                    id="vuln4",
                    finding_id="3c475384-834c-47b0-ac71-a41a022e401c",
                    root_id="root7",
                    state=VulnerabilityStateFaker(
                        specific="4646", where="192.168.1.46", status=VulnerabilityStateStatus.SAFE
                    ),
                    type=VulnerabilityType.PORTS,
                    verification=VulnerabilityVerification(
                        modified_by=EMAIL_FLUIDATTACKS_HACKER,
                        modified_date=DATE_2019,
                        status=VulnerabilityVerificationStatus.VERIFIED,
                    ),
                ),
                VulnerabilityFaker(
                    id="vuln5",
                    finding_id="3c475384-834c-47b0-ac71-a41a022e401c",
                    root_id="root8",
                    state=VulnerabilityStateFaker(
                        specific="4747", where="192.168.1.47", source=Source.MACHINE
                    ),
                    type=VulnerabilityType.PORTS,
                    technique=VulnerabilityTechnique.DAST,
                ),
            ],
        )
    ),
    others=[Mock(numerator_report_digest, "mail_numerator_report", "async", None)],
)
async def test_numerator_report_digest() -> None:
    report_content = await numerator_report_digest.main(
        datetime(2019, 4, 11, 6, 0, 0, 0, tzinfo=UTC)
    )
    assert EMAIL_FLUIDATTACKS_HACKER in report_content
    hacker_metrics = report_content[EMAIL_FLUIDATTACKS_HACKER]
    assert hacker_metrics["enumerated_inputs"]["counter"]["today"] == 1
    assert hacker_metrics["enumerated_ports"]["counter"]["past_day"] == 1
    assert hacker_metrics["enumerated_ports"]["counter"]["today"] == 2
    assert hacker_metrics["verified_ports"]["counter"]["past_day"] == 1
    assert hacker_metrics["verified_ports"]["counter"]["today"] == 1
    assert hacker_metrics["loc"]["counter"]["today"] == 4324
    assert hacker_metrics["reattacked"]["counter"]["today"] == 2
    assert hacker_metrics["submitted"]["counter"]["today"] == 1
    assert hacker_metrics["vulnerable"]["counter"]["today"] == 3
    assert hacker_metrics["sorts_verified_lines"]["counter"]["today"] == 1
    assert hacker_metrics["sorts_verified_lines_priority"]["counter"]["today"] == 60
    assert hacker_metrics["sorts_verified_lines_priority_avg"]["counter"]["today"] == 60.0
    assert hacker_metrics["max_cvss"]["vulnerable"] == Decimal("5.1")
    assert hacker_metrics["max_cvss"]["submitted"] == Decimal("1.1")
