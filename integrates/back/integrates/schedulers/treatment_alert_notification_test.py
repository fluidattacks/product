from datetime import datetime

from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model.enums import (
    Source,
    TreatmentStatus,
)
from integrates.db_model.stakeholders.types import (
    NotificationsPreferences,
)
from integrates.db_model.types import (
    Treatment,
)
from integrates.schedulers import (
    treatment_alert_notification,
)
from integrates.testing.aws import (
    IntegratesAws,
    IntegratesDynamodb,
)
from integrates.testing.fakers import (
    FindingFaker,
    FindingStateFaker,
    GitRootFaker,
    GitRootStateFaker,
    GroupAccessFaker,
    GroupAccessStateFaker,
    GroupFaker,
    OrganizationAccessFaker,
    OrganizationFaker,
    StakeholderFaker,
    StakeholderStateFaker,
    VulnerabilityFaker,
    VulnerabilityStateFaker,
)
from integrates.testing.mocks import (
    Mock,
    mocks,
)
from integrates.testing.utils import freeze_time

MACHINE_EMAIL = "machine@fluidattacks.com"
USER_EMAIL_1 = "user@clientapp.com"
USER_EMAIL_2 = "another_user@some_bank.com"
ORG_ID = "ORG#org1"
ORG_ID_2 = "ORG#org2"
GROUP_NAME_1 = "group1"
GROUP_NAME_2 = "group_bank"
GROUP_NAME_3 = "group_bank_2"


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(
                    created_by=USER_EMAIL_1,
                    id=ORG_ID,
                    name="org1",
                ),
                OrganizationFaker(
                    created_by=USER_EMAIL_2,
                    id=ORG_ID_2,
                    name="org2",
                ),
            ],
            stakeholders=[
                StakeholderFaker(
                    email=USER_EMAIL_1,
                    enrolled=True,
                    state=StakeholderStateFaker(
                        notifications_preferences=NotificationsPreferences(
                            email=["UPDATED_TREATMENT"]
                        )
                    ),
                ),
                StakeholderFaker(
                    email=USER_EMAIL_2,
                    enrolled=True,
                    state=StakeholderStateFaker(
                        notifications_preferences=NotificationsPreferences(
                            email=["UPDATED_TREATMENT"]
                        )
                    ),
                ),
            ],
            organization_access=[
                OrganizationAccessFaker(organization_id=ORG_ID, email=USER_EMAIL_1),
                OrganizationAccessFaker(organization_id=ORG_ID_2, email=USER_EMAIL_2),
            ],
            groups=[
                GroupFaker(
                    name=GROUP_NAME_1,
                    created_by=USER_EMAIL_1,
                    organization_id=ORG_ID,
                ),
                GroupFaker(
                    name=GROUP_NAME_2,
                    created_by=USER_EMAIL_2,
                    organization_id=ORG_ID_2,
                ),
                GroupFaker(
                    name=GROUP_NAME_3,
                    created_by=USER_EMAIL_2,
                    organization_id=ORG_ID_2,
                ),
            ],
            group_access=[
                GroupAccessFaker(
                    email=USER_EMAIL_1,
                    group_name=GROUP_NAME_1,
                    state=GroupAccessStateFaker(
                        modified_by=USER_EMAIL_1,
                        modified_date=datetime.fromisoformat("2024-07-19T10:00:00+00:00"),
                        has_access=True,
                        role="group_manager",
                    ),
                ),
                GroupAccessFaker(
                    email=USER_EMAIL_2,
                    group_name=GROUP_NAME_2,
                    state=GroupAccessStateFaker(
                        modified_by=USER_EMAIL_2,
                        modified_date=datetime.fromisoformat("2024-07-19T10:00:00+00:00"),
                        has_access=True,
                        role="group_manager",
                    ),
                ),
                GroupAccessFaker(
                    email=USER_EMAIL_2,
                    group_name=GROUP_NAME_3,
                    state=GroupAccessStateFaker(
                        modified_by=USER_EMAIL_2,
                        modified_date=datetime.fromisoformat("2024-07-19T10:00:00+00:00"),
                        has_access=True,
                        role="group_manager",
                    ),
                ),
            ],
            roots=[
                GitRootFaker(
                    group_name=GROUP_NAME_1,
                    id="root1",
                    state=GitRootStateFaker(nickname="root1"),
                ),
                GitRootFaker(
                    group_name=GROUP_NAME_2,
                    id="root2",
                    state=GitRootStateFaker(nickname="root2", branch="test"),
                ),
                GitRootFaker(
                    group_name=GROUP_NAME_3,
                    id="root3",
                    state=GitRootStateFaker(nickname="root3", branch="test"),
                ),
            ],
            findings=[
                FindingFaker(
                    group_name=GROUP_NAME_1,
                    id="fin_id_1",
                    state=FindingStateFaker(modified_by=MACHINE_EMAIL),
                    title="001. SQL injection - C Sharp SQL API",
                ),
                FindingFaker(
                    group_name=GROUP_NAME_1,
                    id="fin_id_2",
                    state=FindingStateFaker(modified_by=MACHINE_EMAIL),
                    title="052. Insecure encryption algorithm",
                ),
                FindingFaker(
                    group_name=GROUP_NAME_2,
                    id="fin_id_3",
                    state=FindingStateFaker(modified_by=MACHINE_EMAIL),
                    title="052. Insecure encryption algorithm",
                ),
                FindingFaker(
                    group_name=GROUP_NAME_3,
                    id="fin_id_4",
                    state=FindingStateFaker(modified_by=MACHINE_EMAIL),
                    title="137. Insecure or unset HTTP headers - X-Permitted-Cross-Domain-Policies",
                ),
            ],
            vulnerabilities=[
                VulnerabilityFaker(
                    finding_id="fin_id_1",
                    group_name=GROUP_NAME_1,
                    id="vuln_id_1",
                    organization_name="org1",
                    root_id="root1",
                    state=VulnerabilityStateFaker(
                        where="https://myapp.com",
                        specific="csp ausente",
                        source=Source.MACHINE,
                    ),
                    treatment=Treatment(
                        modified_date=datetime.fromisoformat("2024-11-01T00:00:00+00:00"),
                        status=TreatmentStatus.ACCEPTED,
                        assigned="developer@entry.com",
                        accepted_until=datetime.fromisoformat("2024-11-20T00:00:00+00:00"),
                    ),
                ),
                VulnerabilityFaker(
                    finding_id="fin_id_3",
                    group_name=GROUP_NAME_2,
                    id="vuln_id_2",
                    organization_name="org2",
                    root_id="root2",
                    state=VulnerabilityStateFaker(
                        where="myapp.py",
                        specific="52",
                        source=Source.MACHINE,
                    ),
                    treatment=Treatment(
                        modified_date=datetime.fromisoformat("2024-11-01T00:00:00+00:00"),
                        status=TreatmentStatus.ACCEPTED,
                        accepted_until=datetime.fromisoformat("2024-11-20T00:00:00+00:00"),
                    ),
                ),
                VulnerabilityFaker(
                    finding_id="fin_id_4",
                    group_name=GROUP_NAME_3,
                    id="vuln_id_3",
                    organization_name="org2",
                    root_id="root3",
                    state=VulnerabilityStateFaker(
                        where="package.json",
                        specific="1110",
                        source=Source.MACHINE,
                    ),
                    treatment=Treatment(
                        modified_date=datetime.fromisoformat("2024-11-01T00:00:00+00:00"),
                        status=TreatmentStatus.ACCEPTED,
                        accepted_until=datetime.fromisoformat("2024-11-18T00:00:00+00:00"),
                    ),
                ),
            ],
        ),
    ),
    others=[
        Mock(treatment_alert_notification, "mail_treatment_alert", "async", None),
    ],
)
@freeze_time("2024-11-15T00:00:00+00:00")
async def test_treatment_alert_notification() -> None:
    loaders: Dataloaders = get_new_context()

    email_data = [item async for item in treatment_alert_notification.get_email_data(loaders)]
    sorted_data = sorted(email_data, key=lambda x: x[0])
    assert sorted_data == [
        (
            "another_user@some_bank.com",
            {
                "groups_data": {
                    "group_bank": {
                        "org_name": "org2",
                        "finding_title": ["052. Insecure encryption algorithm"],
                        "group_expiring_findings": [{"fin_id_3": {"myapp.py (52)": 5}}],
                    },
                    "group_bank_2": {
                        "org_name": "org2",
                        "finding_title": [
                            "137. Insecure or unset HTTP headers - "
                            "X-Permitted-Cross-Domain-Policies"
                        ],
                        "group_expiring_findings": [{"fin_id_4": {"package.json (1110)": 3}}],
                    },
                }
            },
        ),
        (
            "user@clientapp.com",
            {
                "groups_data": {
                    "group1": {
                        "org_name": "org1",
                        "finding_title": ["001. SQL injection - C Sharp SQL API"],
                        "group_expiring_findings": [
                            {"fin_id_1": {"https://myapp.com (csp ausente)": 5}}
                        ],
                    }
                }
            },
        ),
    ]
    # Test full scheduler runs without errors
    await treatment_alert_notification.main()
