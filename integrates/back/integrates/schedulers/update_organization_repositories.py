from operator import (
    attrgetter,
)

from aioextensions import (
    collect,
)

from integrates.dataloaders import (
    get_new_context,
)
from integrates.db_model.organizations.get import (
    get_all_organizations,
)
from integrates.organizations.domain import (
    get_all_active_group_names,
)
from integrates.outside_repositories.utils import (
    update_organization_repositories,
)


async def main() -> None:
    loaders = get_new_context()
    organizations = await get_all_organizations()
    all_group_names = set(await get_all_active_group_names(loaders))
    all_group_names = {group.lower() for group in all_group_names}
    organizations_sorted_by_name = sorted(organizations, key=attrgetter("name"))
    len_organizations_sorted_by_name = len(organizations_sorted_by_name)

    await collect(
        tuple(
            update_organization_repositories(
                organization=organization,
                progress=count / len_organizations_sorted_by_name,
                all_group_names=all_group_names,
            )
            for count, organization in enumerate(organizations_sorted_by_name)
        ),
        workers=1,
    )
