import time

import boto3
from aioextensions import (
    collect,
)
from botocore.exceptions import (
    ClientError,
)

from integrates.custom_exceptions import (
    GroupNotFound,
)
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model.findings.types import (
    Finding,
)
from integrates.dynamodb.exceptions import (
    UnavailabilityError,
)
from integrates.findings import (
    domain as findings_domain,
)
from integrates.organizations.domain import (
    get_all_active_group_names,
)
from integrates.schedulers.common import (
    error,
    info,
)


def send_metric_to_cloudwatch(value: float) -> None:
    try:
        cloudwatch_client = boto3.client("cloudwatch", "us-east-1")
        cloudwatch_client.put_metric_data(
            Namespace="Integrates",
            MetricData=[
                {
                    "MetricName": "StreamsAccuracy",
                    "Value": value,
                    "Unit": "Percent",
                },
            ],
        )
    except ClientError as ex:
        error(
            "Error: An error ocurred sending metric to cloudwatch",
            extra={"ex": ex},
        )


def get_accuracy(amount: int, total: int) -> float:
    return round(amount / total * 100, 4) if total != 0 else 0


async def calculate_accuracy_by_finding(loaders: Dataloaders, finding: Finding) -> tuple[int, int]:
    current_values = finding.unreliable_indicators
    new_values = {
        "unreliable_status": await findings_domain.get_status(loaders, finding.id),
        "submitted_vulnerabilities": (
            await findings_domain.get_submitted_vulnerabilities(loaders, finding.id)
        ),
        "rejected_vulnerabilities": (
            await findings_domain.get_rejected_vulnerabilities(loaders, finding.id)
        ),
        "max_open_epss": (await findings_domain.get_max_open_epss(loaders, finding.id)),
        "max_open_root_criticality": (
            await findings_domain.get_max_open_root_criticality(loaders, finding.id)
        ),
        "max_open_severity_score": (
            await findings_domain.get_max_open_severity_score(loaders, finding.id)
        ),
        "max_open_severity_score_v4": (
            await findings_domain.get_max_open_severity_score_v4(loaders, finding.id)
        ),
        "newest_vulnerability_report_date": (
            await findings_domain.get_newest_vulnerability_report_date(loaders, finding.id)
        ),
        "oldest_vulnerability_report_date": (
            await findings_domain.get_oldest_vulnerability_report_date(loaders, finding.id)
        ),
        "total_open_cvssf": (await findings_domain.get_total_open_cvssf(loaders, finding.id)),
        "total_open_cvssf_v4": (await findings_domain.get_total_open_cvssf_v4(loaders, finding.id)),
    }
    treatment_summary = await findings_domain.get_treatment_summary(loaders, finding.id)
    treatment_keys = [
        "accepted",
        "accepted_undefined",
        "in_progress",
        "untreated",
    ]
    vulnerabilities_summary = await findings_domain.get_vulnerabilities_summary(loaders, finding.id)
    vulnerabilities_summary_keys = [
        "closed",
        "open",
        "submitted",
        "rejected",
        "open_critical",
        "open_high",
        "open_low",
        "open_medium",
        "open_critical_v3",
        "open_high_v3",
        "open_low_v3",
        "open_medium_v3",
    ]

    total_keys = len(new_values.keys()) + len(treatment_keys) + len(vulnerabilities_summary_keys)
    asserts = total_keys
    failed_indicators_keys = []

    for indicator, new_value in new_values.items():
        if getattr(current_values, indicator) != new_value:
            failed_indicators_keys.append(indicator)
            asserts -= 1

    for key in treatment_keys:
        if getattr(current_values.treatment_summary, key) != getattr(treatment_summary, key):
            failed_indicators_keys.append(f"treatment_summary.{key}")
            asserts -= 1

    for key in vulnerabilities_summary_keys:
        if getattr(current_values.vulnerabilities_summary, key) != getattr(
            vulnerabilities_summary, key
        ):
            failed_indicators_keys.append(f"vulnerabilities_summary.{key}")
            asserts -= 1

    if len(failed_indicators_keys) > 0:
        info(
            "Unreliable indicators found",
            extra={"finding_id": finding.id, "keys": failed_indicators_keys},
        )
    return (asserts, total_keys)


async def calculate_accuracy_by_group(group_name: str, progress: float) -> tuple[int, int]:
    try:
        loaders: Dataloaders = get_new_context()
        group_findings = await loaders.group_findings.load(group_name)
        if not group_findings:
            return (0, 0)

        results = await collect(
            tuple(calculate_accuracy_by_finding(loaders, finding) for finding in group_findings),
            workers=1,
        )
        values = (sum(v[0] for v in results), sum(v[1] for v in results))

        info(
            "Group indicators processed",
            extra={
                "group_name": group_name,
                "partial_accuracy": get_accuracy(values[0], values[1]),
                "progress": round(progress, 2),
            },
        )

        return values
    except (ClientError, GroupNotFound, TypeError, UnavailabilityError) as ex:
        msg = "Error: An error ocurred querying indicators"
        error(msg, extra={"group_name": group_name, "ex": ex})

        return (0, 0)


async def add_new_metric() -> None:
    start_time = time.strftime("%Y-%m-%d at %H:%M:%S UTC")
    loaders: Dataloaders = get_new_context()
    groups: list[str] = await get_all_active_group_names(loaders)
    sorted_groups: list[str] = sorted(groups)
    total_groups: int = len(sorted_groups)

    results = await collect(
        tuple(
            calculate_accuracy_by_group(
                group_name=group,
                progress=count / total_groups,
            )
            for count, group in enumerate(sorted_groups)
        ),
        workers=1,
    )
    values = (sum(v[0] for v in results), sum(v[1] for v in results))

    send_metric_to_cloudwatch(value=get_accuracy(values[0], values[1]))

    info(
        "Streams accuracy metrics updated",
        extra={
            "total_groups": total_groups,
            "accuracy": get_accuracy(values[0], values[1]),
            "start_time": start_time,
            "end_time": time.strftime("%Y-%m-%d at %H:%M:%S UTC"),
        },
    )


async def main() -> None:
    await add_new_metric()
