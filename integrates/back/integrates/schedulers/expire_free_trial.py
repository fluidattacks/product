import logging
import logging.config

from aioextensions import (
    collect,
    in_thread,
)

from integrates.batch.dal.get import (
    get_actions_by_name,
)
from integrates.batch.dal.put import (
    put_action,
)
from integrates.batch.enums import (
    Action,
    IntegratesBatchQueue,
)
from integrates.custom_exceptions import (
    InvalidManagedChange,
    MailerClientError,
    UnableToSendMail,
)
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model.groups.enums import (
    GroupManaged,
    GroupStateJustification,
    GroupStateStatus,
)
from integrates.db_model.groups.types import (
    Group,
)
from integrates.db_model.trials.types import (
    Trial,
    TrialMetadataToUpdate,
)
from integrates.decorators import (
    retry_on_exceptions,
)
from integrates.groups import (
    domain as groups_domain,
)
from integrates.mailer import (
    trial as trial_mail,
)
from integrates.mailer.utils import get_group_emails_by_notification
from integrates.organizations import (
    domain as orgs_domain,
)
from integrates.schedulers.common import (
    error,
    info,
)
from integrates.settings import (
    LOGGING,
)
from integrates.trials import (
    domain as trials_domain,
)
from integrates.trials import (
    getters as trials_getters,
)

logging.config.dictConfig(LOGGING)

EMAIL_INTEGRATES = "integrates@fluidattacks.com"
REMOVAL_AFTER_EXPIRATION_DAYS = 30
TRANSACTIONS_LOGGER = logging.getLogger("transactional")


mail_free_trial_over = retry_on_exceptions(
    exceptions=(UnableToSendMail, MailerClientError),
    max_attempts=4,
    sleep_seconds=2,
)(trial_mail.send_mail_free_trial_over)


async def _remove_expired_groups_data(
    loaders: Dataloaders,
    group: Group,
    trial: Trial,
) -> None:
    if (
        days_since_expiration := trials_getters.get_days_since_expiration(trial)
    ) <= REMOVAL_AFTER_EXPIRATION_DAYS:
        return

    if group.state.status == GroupStateStatus.DELETED:
        if not await get_actions_by_name(
            action_name=Action.REMOVE_GROUP_RESOURCES,
            entity=group.name,
        ):
            await put_action(
                action=Action.REMOVE_GROUP_RESOURCES,
                entity=group.name,
                subject=EMAIL_INTEGRATES,
                additional_info={
                    "validate_pending_actions": False,
                    "subscription": group.state.type.value,
                    "has_advanced": group.state.has_advanced,
                    "emails": await get_group_emails_by_notification(
                        loaders=loaders,
                        group_name=group.name,
                        notification="group_alert",
                    ),
                },
                queue=IntegratesBatchQueue.SMALL,
                attempt_duration_seconds=86400,
            )

            await in_thread(
                TRANSACTIONS_LOGGER.info,
                "Re-queue Group Resources action",
                extra={
                    "group_name": group.name,
                },
            )
        return

    info(
        "Removing data for group %s, created_by %s, start_date %s, days since expiration: %d",
        group.name,
        group.created_by,
        trial.start_date,
        days_since_expiration,
    )
    await groups_domain.remove_group(
        loaders=loaders,
        comments="Scheduled removal of the group and its data",
        email=EMAIL_INTEGRATES,
        group_name=group.name,
        justification=GroupStateJustification.TRIAL_FINALIZATION,
        validate_pending_actions=False,
    )


async def _expire(
    loaders: Dataloaders,
    group: Group,
    trial: Trial,
) -> None:
    try:
        info(
            "Will expire group %s, created_by %s, start_date %s",
            group.name,
            group.created_by,
            trial.start_date,
        )
        await trials_domain.update_metadata(
            email=trial.email,
            metadata=TrialMetadataToUpdate(completed=True),
        )
        await groups_domain.update_group_managed(
            loaders=loaders,
            comments="Trial period has expired",
            email=EMAIL_INTEGRATES,
            group_name=group.name,
            justification=GroupStateJustification.TRIAL_FINALIZATION,
            managed=GroupManaged.UNDER_REVIEW,
        )
        await mail_free_trial_over(
            loaders=loaders,
            email_to=[group.created_by],
            group_name=group.name,
        )
    except InvalidManagedChange:
        error(
            "Couldn't expire group %s, managed %s",
            group.name,
            group.state.managed,
        )


async def main() -> None:
    loaders: Dataloaders = get_new_context()
    groups = await orgs_domain.get_all_trial_groups(loaders)
    emails = [group.created_by for group in groups]
    trials = await loaders.trial.load_many(emails)

    await collect(
        tuple(
            _expire(loaders, group, trial)
            for group, trial in zip(groups, trials, strict=False)
            if trial and trials_domain.has_expired(trial)
        ),
    )

    await collect(
        tuple(
            _remove_expired_groups_data(loaders, group, trial)
            for group, trial in zip(groups, trials, strict=False)
            if group.state.managed == GroupManaged.UNDER_REVIEW and trial and trial.completed
        ),
        workers=1,
    )
