from decimal import Decimal

from integrates.dataloaders import get_new_context
from integrates.db_model.compliance.types import ComplianceStandard
from integrates.db_model.groups.types import UnfulfilledStandard
from integrates.db_model.organizations.types import OrganizationStandardCompliance
from integrates.db_model.vulnerabilities.enums import VulnerabilityStateStatus
from integrates.schedulers import update_compliance
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb, IntegratesOpensearch
from integrates.testing.fakers import (
    FindingFaker,
    GroupAccessFaker,
    GroupAccessStateFaker,
    GroupFaker,
    OrganizationFaker,
    StakeholderFaker,
    VulnerabilityFaker,
    VulnerabilityStateFaker,
)
from integrates.testing.mocks import mocks


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            groups=[GroupFaker(name="group1", organization_id="org1")],
            findings=[
                FindingFaker(
                    id="3c475384",
                    group_name="group1",
                    unfulfilled_requirements=["155", "159", "273"],
                )
            ],
            vulnerabilities=[
                VulnerabilityFaker(id="891b1d7f", finding_id="3c475384", group_name="group1"),
                VulnerabilityFaker(
                    id="8e5b7694",
                    finding_id="3c475384",
                    group_name="group1",
                    state=VulnerabilityStateFaker(status=VulnerabilityStateStatus.SAFE),
                ),
            ],
            stakeholders=[StakeholderFaker(email="hacker@fluidattacks.com")],
            group_access=[
                GroupAccessFaker(
                    email="hacker@fluidattacks.com",
                    state=GroupAccessStateFaker(has_access=True, role="hacker"),
                )
            ],
        ),
        opensearch=IntegratesOpensearch(autoload=True),
    ),
)
async def test_update_compliance() -> None:
    await update_compliance.main()

    loaders = get_new_context()
    group_indicators = await loaders.group_unreliable_indicators.load("group1")
    org_indicators = await loaders.organization_unreliable_indicators.load("ORG#org1")
    compliance_indicators = await loaders.compliance_unreliable_indicators.load("")
    assert group_indicators.unfulfilled_standards is not None
    assert group_indicators.unfulfilled_standards[0] == UnfulfilledStandard(
        name="bsimm", unfulfilled_requirements=["155", "159", "273"]
    )
    assert org_indicators.standard_compliances is not None
    assert org_indicators.standard_compliances[0] == OrganizationStandardCompliance(
        standard_name="bsimm", compliance_level=Decimal("0.50")
    )
    assert org_indicators.compliance_level == Decimal("0.95")
    assert org_indicators.compliance_weekly_trend == Decimal("0.00")
    assert org_indicators.estimated_days_to_full_compliance == Decimal("0.01")
    assert compliance_indicators.standards is not None
    assert compliance_indicators.standards[-1] == ComplianceStandard(
        avg_organization_compliance_level=Decimal("1.00"),
        best_organization_compliance_level=Decimal("1.00"),
        standard_name="fisma",
        worst_organization_compliance_level=Decimal("1.00"),
    )
