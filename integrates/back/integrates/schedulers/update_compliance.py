from collections import (
    defaultdict,
)
from decimal import (
    Decimal,
)
from statistics import (
    mean,
)

from aioextensions import (
    collect,
)

from integrates.custom_utils import (
    datetime as datetime_utils,
)
from integrates.custom_utils import (
    organizations as orgs_utils,
)
from integrates.custom_utils.criteria import (
    CRITERIA_COMPLIANCE,
    CRITERIA_REQUIREMENTS,
    CRITERIA_VULNERABILITIES,
)
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model import (
    compliance as compliance_model,
)
from integrates.db_model import (
    organizations as orgs_model,
)
from integrates.db_model.compliance.types import (
    ComplianceStandard,
    ComplianceUnreliableIndicators,
)
from integrates.db_model.findings.types import (
    Finding,
)
from integrates.db_model.groups.types import (
    Group,
    GroupUnreliableIndicators,
    UnfulfilledStandard,
)
from integrates.db_model.organizations.types import (
    Organization,
    OrganizationStandardCompliance,
    OrganizationUnreliableIndicators,
)
from integrates.findings import (
    domain as findings_domain,
)
from integrates.groups import (
    domain as groups_domain,
)
from integrates.organizations import (
    domain as orgs_domain,
)
from integrates.schedulers.common import (
    info,
)
from integrates.search.operations import (
    SearchParams,
    search,
)


def get_definition_from_reference(reference: str) -> str:
    return reference.split(".", 1)[1]


def get_standard_from_reference(reference: str) -> str:
    return reference.split(".", 1)[0]


async def get_closed_old_vulnerabilities_last_week(
    finding: Finding,
) -> int:
    """
    Return the amount of vulnerabilities that were open before the
    last week and were closed the last week
    """
    results = await search(
        SearchParams(
            after=None,
            must_filters=[
                {"sk": f"FIN#{finding.id}"},
                {"state.status": "CLOSED"},
                {"state.status": "SAFE"},
            ],
            range_filters=[
                {"state.modified_date": {"gte": "now-1w"}},
                {"created_date": {"lt": "now-1w"}},
            ],
            index_value="vulns_index",
            limit=0,
            query=None,
        ),
    )
    return results.total


async def get_organization_compliance_level(
    loaders: Dataloaders,
    organization: Organization,
) -> Decimal:
    org_groups: list[Group] = await loaders.organization_groups.load(organization.id)
    findings = await loaders.group_findings.load_many_chained([group.name for group in org_groups])
    findings_open_vulnerabilities = await collect(
        tuple(
            findings_domain.get_open_vulnerabilities(loaders, finding.id) for finding in findings
        ),
        workers=100,
    )
    open_findings: list[Finding] = []
    for finding, open_vulnerabilities in zip(findings, findings_open_vulnerabilities, strict=False):
        if open_vulnerabilities:
            open_findings.append(finding)

    requirements_by_finding = tuple(finding.unfulfilled_requirements for finding in open_findings)
    compliances_by_finding = tuple(
        set(
            reference
            for requirement in requirements
            for reference in CRITERIA_REQUIREMENTS[requirement]["references"]
        )
        for requirements in requirements_by_finding
    )
    org_non_compliance = set.union(*compliances_by_finding) if compliances_by_finding else set()
    all_compliances = set(
        f"{name.lower()}.{definition}"
        for name, standard in CRITERIA_COMPLIANCE.items()
        for definition in standard["definitions"]
    )
    return (
        Decimal(
            (len(all_compliances) - len(org_non_compliance.intersection(all_compliances)))
            / len(all_compliances),
        ).quantize(Decimal("0.01"))
        if all_compliances
        else Decimal("0.0")
    )


async def _get_last_week_open_findings(
    *,
    loaders: Dataloaders,
    organization: Organization,
) -> dict[str, Finding]:
    org_groups: list[Group] = await loaders.organization_groups.load(organization.id)
    findings = await loaders.group_findings.load_many_chained([group.name for group in org_groups])
    findings_open_vulnerabilities = await collect(
        tuple(
            findings_domain.get_open_vulnerabilities(loaders, finding.id) for finding in findings
        ),
        workers=100,
    )
    findings_closed_vulnerabilities_last_week = await collect(
        tuple(get_closed_old_vulnerabilities_last_week(finding) for finding in findings),
        workers=20,
    )
    a_week_ago = datetime_utils.get_now_minus_delta(weeks=1)
    last_week_open_findings: dict[str, Finding] = {}
    for finding, open_vulnerabilities in zip(findings, findings_open_vulnerabilities, strict=False):
        # Do not count vulnerabilities that were released the last week
        if [
            vulnerability
            for vulnerability in open_vulnerabilities
            if vulnerability.unreliable_indicators.unreliable_report_date
            and vulnerability.unreliable_indicators.unreliable_report_date < a_week_ago
        ]:
            last_week_open_findings[finding.id] = finding

    for finding, closed_vulnerabilities_last_week in zip(
        findings,
        findings_closed_vulnerabilities_last_week,
        strict=False,
    ):
        # Do not count vulnerabilities that were closed within the last week
        if closed_vulnerabilities_last_week:
            last_week_open_findings[finding.id] = finding
    return last_week_open_findings


async def get_organization_compliance_weekly_trend(
    loaders: Dataloaders,
    organization: Organization,
    current_compliance_level: Decimal,
) -> Decimal:
    last_week_open_findings = await _get_last_week_open_findings(
        loaders=loaders,
        organization=organization,
    )
    requirements_by_finding = tuple(
        finding.unfulfilled_requirements for finding in last_week_open_findings.values()
    )
    compliances_by_finding = tuple(
        set(
            reference
            for requirement in requirements
            for reference in CRITERIA_REQUIREMENTS[requirement]["references"]
        )
        for requirements in requirements_by_finding
    )
    org_non_compliance = set.union(*compliances_by_finding) if compliances_by_finding else set()
    all_compliances = set(
        f"{name.lower()}.{definition}"
        for name, standard in CRITERIA_COMPLIANCE.items()
        for definition in standard["definitions"]
    )
    return (
        (
            current_compliance_level
            - Decimal(
                (len(all_compliances) - len(org_non_compliance.intersection(all_compliances)))
                / len(all_compliances),
            ).quantize(Decimal("0.01"))
        ).quantize(Decimal("0.01"))
        if all_compliances
        else Decimal("0.0")
    )


async def get_organization_estimated_days_to_full_compliance(
    loaders: Dataloaders,
    organization: Organization,
    default_average_minutes_to_remediate_vulnerability: Decimal,
) -> Decimal:
    org_groups: list[Group] = await loaders.organization_groups.load(organization.id)
    findings = await loaders.group_findings.load_many_chained([group.name for group in org_groups])
    findings_open_vulnerabilities = await collect(
        tuple(
            findings_domain.get_open_vulnerabilities(loaders, finding.id) for finding in findings
        ),
        workers=100,
    )
    min_time_to_remediate_total = Decimal("0.0")
    for finding, open_vulnerabilities in zip(findings, findings_open_vulnerabilities, strict=False):
        remediation_time = CRITERIA_VULNERABILITIES.get(
            finding.title[:3],
            {"remediation_time": (default_average_minutes_to_remediate_vulnerability)},
        )["remediation_time"]
        min_time_to_remediate_total += (
            default_average_minutes_to_remediate_vulnerability
            if remediation_time == "__empty__"
            else Decimal(remediation_time)
        ) * len(open_vulnerabilities)

    minutes_in_a_day = 1440
    return Decimal(min_time_to_remediate_total / minutes_in_a_day).quantize(Decimal("0.01"))


async def get_organization_standard_compliances(
    loaders: Dataloaders,
    organization: Organization,
) -> list[OrganizationStandardCompliance]:
    org_groups: list[Group] = await loaders.organization_groups.load(organization.id)
    findings = await loaders.group_findings.load_many_chained([group.name for group in org_groups])
    findings_open_vulnerabilities = await collect(
        tuple(
            findings_domain.get_open_vulnerabilities(loaders, finding.id) for finding in findings
        ),
        workers=100,
    )
    open_findings: list[Finding] = []
    for finding, open_vulnerabilities in zip(findings, findings_open_vulnerabilities, strict=False):
        if open_vulnerabilities:
            open_findings.append(finding)

    requirements_by_finding = tuple(finding.unfulfilled_requirements for finding in open_findings)
    non_compliance_definitions_by_standard = defaultdict(set)
    for requirements in requirements_by_finding:
        for requirement in requirements:
            for reference in CRITERIA_REQUIREMENTS[requirement]["references"]:
                non_compliance_definitions_by_standard[get_standard_from_reference(reference)].add(
                    get_definition_from_reference(reference),
                )

    return list(
        OrganizationStandardCompliance(
            standard_name=standard_name.lower(),
            compliance_level=Decimal(
                (
                    len(standard["definitions"])
                    - len(
                        non_compliance_definitions_by_standard[standard_name].intersection(
                            set(standard["definitions"]),
                        ),
                    )
                )
                / len(standard["definitions"]),
            ).quantize(Decimal("0.01")),
        )
        for standard_name, standard in CRITERIA_COMPLIANCE.items()
    )


async def update_group_standard_fulfillment(
    loaders: Dataloaders,
    group: Group,
) -> None:
    findings = await loaders.group_findings.load(group.name)
    findings_open_vulnerabilities = await collect(
        tuple(
            findings_domain.get_open_vulnerabilities(loaders, finding.id) for finding in findings
        ),
        workers=100,
    )
    open_findings: list[Finding] = []
    for finding, open_vulnerabilities in zip(findings, findings_open_vulnerabilities, strict=False):
        if open_vulnerabilities:
            open_findings.append(finding)

    requirements_by_finding = tuple(finding.unfulfilled_requirements for finding in open_findings)
    non_compliance_requirements_by_standard: defaultdict[str, set] = defaultdict(set)
    for requirements in requirements_by_finding:
        for requirement in requirements:
            for reference in CRITERIA_REQUIREMENTS[requirement]["references"]:
                non_compliance_requirements_by_standard[get_standard_from_reference(reference)].add(
                    requirement,
                )

    unfulfilled_standards: list[UnfulfilledStandard] = [
        UnfulfilledStandard(
            name=standard_name,
            unfulfilled_requirements=sorted(non_compliance_requirements),
        )
        for standard_name, non_compliance_requirements in (
            non_compliance_requirements_by_standard.items()
        )
        if non_compliance_requirements
    ]
    await groups_domain.update_indicators(
        group_name=group.name,
        indicators=GroupUnreliableIndicators(unfulfilled_standards=unfulfilled_standards),
    )


async def update_groups_standard_fulfillment(
    loaders: Dataloaders,
    organization: Organization,
) -> None:
    org_groups: list[Group] = await loaders.organization_groups.load(organization.id)
    await collect(
        tuple(
            update_group_standard_fulfillment(
                loaders=loaders,
                group=group,
            )
            for group in org_groups
        ),
    )


async def update_organization_compliance(
    *,
    loaders: Dataloaders,
    organization: Organization,
    default_average_minutes_to_remediate_vulnerability: Decimal,
) -> None:
    info(f"Update organization compliance: {organization.name}")
    compliance_level = await get_organization_compliance_level(
        loaders=loaders,
        organization=organization,
    )
    compliance_weekly_trend = await get_organization_compliance_weekly_trend(
        loaders=loaders,
        organization=organization,
        current_compliance_level=compliance_level,
    )
    estimated_days_to_full_compliance = await get_organization_estimated_days_to_full_compliance(
        loaders=loaders,
        organization=organization,
        default_average_minutes_to_remediate_vulnerability=(
            default_average_minutes_to_remediate_vulnerability
        ),
    )
    standard_compliances = await get_organization_standard_compliances(
        loaders=loaders,
        organization=organization,
    )
    await orgs_model.update_unreliable_indicators(
        organization_id=organization.id,
        organization_name=organization.name,
        indicators=OrganizationUnreliableIndicators(
            compliance_level=compliance_level,
            compliance_weekly_trend=compliance_weekly_trend,
            estimated_days_to_full_compliance=(estimated_days_to_full_compliance),
            standard_compliances=standard_compliances,
        ),
    )
    await update_groups_standard_fulfillment(
        loaders=loaders,
        organization=organization,
    )


async def update_compliance_indicators(
    loaders: Dataloaders,
    organizations: list[Organization],
) -> None:
    info("Update compliance indicators")
    organizations_unreliable_indicators = (
        await loaders.organization_unreliable_indicators.load_many(
            [organization.id for organization in organizations],
        )
    )
    compliances_level_by_standard: dict[str, set] = {}
    standard_names = tuple(standard_name.lower() for standard_name in CRITERIA_COMPLIANCE)
    for standard_name in standard_names:
        compliances_level_by_standard[standard_name] = set()
    for standard_name in standard_names:
        for indicators in organizations_unreliable_indicators:
            compliance = next(
                (
                    standard_compliance
                    for standard_compliance in indicators.standard_compliances or []
                    if standard_compliance.standard_name == standard_name
                ),
                None,
            )
            if compliance:
                compliances_level_by_standard[standard_name].add(compliance.compliance_level)

    await compliance_model.update_unreliable_indicators(
        indicators=ComplianceUnreliableIndicators(
            standards=[
                ComplianceStandard(
                    avg_organization_compliance_level=Decimal(
                        mean(compliances_level_by_standard[standard_name]),
                    ).quantize(Decimal("0.01")),
                    best_organization_compliance_level=Decimal(
                        max(compliances_level_by_standard[standard_name]),
                    ).quantize(Decimal("0.01")),
                    standard_name=standard_name,
                    worst_organization_compliance_level=Decimal(
                        min(compliances_level_by_standard[standard_name]),
                    ).quantize(Decimal("0.01")),
                )
                for standard_name in standard_names
            ],
        ),
    )


async def update_compliance() -> None:
    loaders: Dataloaders = get_new_context()
    default_average_minutes_to_remediate_vulnerability = mean(
        [
            Decimal(finding["remediation_time"])
            for finding in CRITERIA_VULNERABILITIES.values()
            if finding.get("remediation_time", "__empty__") != "__empty__"
        ],
    )
    current_orgs: list[Organization] = []
    async for organization in orgs_domain.iterate_organizations():
        if orgs_utils.is_deleted(organization):
            continue

        current_orgs.append(organization)

    await collect(
        tuple(
            update_organization_compliance(
                loaders=loaders,
                organization=organization,
                default_average_minutes_to_remediate_vulnerability=(
                    default_average_minutes_to_remediate_vulnerability
                ),
            )
            for organization in current_orgs
        ),
        workers=5,
    )
    await update_compliance_indicators(
        loaders=loaders,
        organizations=current_orgs,
    )


async def main() -> None:
    await update_compliance()
