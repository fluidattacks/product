import logging
from decimal import Decimal

from integrates.dataloaders import get_new_context
from integrates.db_model.groups.types import GroupUnreliableIndicators
from integrates.db_model.roots.types import GitRoot, RootRequest, RootUnreliableIndicators
from integrates.db_model.types import CodeLanguage
from integrates.schedulers import groups_languages_distribution
from integrates.testing.aws import (
    GroupUnreliableIndicatorsToUpdate,
    IntegratesAws,
    IntegratesDynamodb,
)
from integrates.testing.fakers import GitRootFaker, GroupFaker, OrganizationFaker
from integrates.testing.mocks import Mock, mocks

LOGGER = logging.getLogger(__name__)


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1", name="okada")],
            groups=[GroupFaker(name="unittesting", organization_id="org1")],
            roots=[
                GitRootFaker(
                    organization_name="okada",
                    group_name="unittesting",
                    id="root1",
                    unreliable_indicators=RootUnreliableIndicators(
                        unreliable_code_languages=[
                            CodeLanguage("Python", 100),
                            CodeLanguage("YAML", 10),
                        ],
                    ),
                ),
            ],
            group_unreliable_indicators=[
                GroupUnreliableIndicatorsToUpdate(
                    group_name="unittesting",
                    indicators=GroupUnreliableIndicators(
                        closed_vulnerabilities=10,
                        code_languages=[
                            CodeLanguage("Python", 100),
                            CodeLanguage("YAML", 10),
                        ],
                        max_severity=Decimal("8.0"),
                    ),
                )
            ],
        )
    ),
    others=[
        Mock(
            groups_languages_distribution,
            "get_root_code_languages",
            "async",
            [CodeLanguage("Python", 3), CodeLanguage("YAML", 5)],
        ),
    ],
)
async def test_update_groups_languages() -> None:
    await groups_languages_distribution.main()

    loaders = get_new_context()
    group_new_indicators = await loaders.group_unreliable_indicators.load("unittesting")
    assert group_new_indicators.closed_vulnerabilities == 10
    assert group_new_indicators.code_languages
    assert set(group_new_indicators.code_languages) == {
        CodeLanguage("Python", 3),
        CodeLanguage("YAML", 5),
    }
    assert group_new_indicators.max_severity == Decimal("8.0")

    root = await loaders.root.load(RootRequest("unittesting", "root1"))
    assert isinstance(root, GitRoot)
    assert set(root.unreliable_indicators.unreliable_code_languages) == {
        CodeLanguage("Python", 3),
        CodeLanguage("YAML", 5),
    }
