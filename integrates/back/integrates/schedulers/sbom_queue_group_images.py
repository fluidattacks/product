import integrates.organizations.domain as orgs_domain
from integrates.custom_exceptions import (
    CredentialNotFound,
    InvalidAWSRoleTrustPolicy,
    RootDockerImageNotFound,
)
from integrates.dataloaders import Dataloaders, get_new_context
from integrates.db_model.credentials.types import (
    Credentials,
    CredentialsRequest,
)
from integrates.db_model.groups.types import Group
from integrates.db_model.roots.enums import RootDockerImageStateStatus
from integrates.db_model.roots.types import (
    GroupDockerImagesRequest,
    RootDockerImage,
)
from integrates.db_model.toe_packages.utils import INTEGRATES_EMAIL
from integrates.jobs_orchestration.jobs import queue_sbom_image_job
from integrates.jobs_orchestration.model.types import SbomFormat, SbomJobType
from integrates.organizations import (
    utils as orgs_utils,
)
from integrates.roots.docker_images import get_image_manifest
from integrates.schedulers.common import error, info, is_machine_target


async def get_machine_groups(loaders: Dataloaders) -> list[str]:
    groups = await orgs_domain.get_all_active_groups(loaders)
    machine_group_names = sorted([group.name for group in groups if is_machine_target(group)])
    return machine_group_names


async def get_images_to_execute(loaders: Dataloaders, group_name: str) -> list[RootDockerImage]:
    return [
        image
        for image in await loaders.group_docker_images.load(GroupDockerImagesRequest(group_name))
        if (
            isinstance(image, RootDockerImage)
            and image.state.status != RootDockerImageStateStatus.DELETED
            and image.state.include
        )
    ]


async def process_image(
    image: RootDockerImage, group_item: Group, loaders: Dataloaders
) -> str | None:
    """Process a single image, update its state, and return its URI."""
    organization_credential: Credentials | None = None
    organization = await orgs_utils.get_organization(loaders, group_item.organization_id)
    external_id = organization.state.aws_external_id if organization else None

    if image.state.credential_id:
        organization_credential = await loaders.credentials.load(
            CredentialsRequest(
                id=image.state.credential_id,
                organization_id=group_item.organization_id,
            )
        )

    try:
        current_image_manifest = await get_image_manifest(
            image_ref=image.uri,
            credentials=organization_credential,
            external_id=external_id,
        )
    except (CredentialNotFound, RootDockerImageNotFound, InvalidAWSRoleTrustPolicy) as err:
        error(f"Unable to get current image manifest due to {err!s}")
        return None

    digest = current_image_manifest["Digest"]
    if digest == image.state.digest:
        info(f"The digest for image '{image.uri}' is already up-to-date. Skipping.")
        return None

    return image.uri


async def process_images(
    loaders: Dataloaders, images: list[RootDockerImage], group_name: str
) -> None:
    """Process multiple images in a group."""
    group_item: Group | None = await loaders.group.load(group_name)
    if not group_item:
        error(f"Group '{group_name}' not found. Cannot process images.")
        return

    uris_with_credentials = []

    for image in images:
        uri = await process_image(image, group_item, loaders)
        if uri:
            uris_with_credentials.append((uri, image.state.credential_id))

    if not uris_with_credentials:
        info(f"No updatable images found for group '{group_name}'.")
        return

    await queue_sbom_image_job(
        loaders=loaders,
        group_name=group_item.name,
        modified_by=INTEGRATES_EMAIL,
        uris_with_credentials=uris_with_credentials,
        job_type=SbomJobType.SCHEDULER,
        sbom_format=SbomFormat.FLUID_JSON,
    )


async def process_group(loaders: Dataloaders, group_name: str) -> None:
    """Process all images for a specific group."""
    group_images = await get_images_to_execute(loaders, group_name)

    if not group_images:
        info("No images found for group", extra={"group_name": group_name})
        return

    info(
        "Processing group",
        extra={"group_name": group_name, "images": len(group_images)},
    )
    await process_images(loaders, group_images, group_name)


async def main() -> None:
    """Main function to process all active machine groups."""
    loaders = get_new_context()
    machine_group_names = await get_machine_groups(loaders)

    info(
        "Groups to process",
        extra={
            "names": machine_group_names,
            "len_groups": len(machine_group_names),
        },
    )

    for group_name in machine_group_names:
        await process_group(loaders, group_name)
