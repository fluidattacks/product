import html
from collections.abc import Iterable
from datetime import datetime
from typing import Any, cast

from aioextensions import collect
from pydantic import EmailStr
from starlette.datastructures import UploadFile

from integrates.context import BASE_URL, FI_MAIL_PRODUCTION, FI_MAIL_PROJECTS
from integrates.custom_exceptions import FindingNotFound
from integrates.custom_utils import datetime as datetime_utils
from integrates.custom_utils.groups import get_group
from integrates.dataloaders import Dataloaders
from integrates.db_model.groups.enums import GroupManaged, GroupService, GroupSubscriptionType
from integrates.db_model.groups.enums import GroupStateJustification as GroupReason
from integrates.db_model.groups.types import Group, GroupState
from integrates.db_model.reports.types import Report
from integrates.db_model.vulnerabilities.types import Vulnerability
from integrates.mailer import groups as groups_mail
from integrates.mailer import utils as mailer_utils
from integrates.mailer.common import send_mails_async
from integrates.organizations.utils import get_organization
from integrates.tickets import dal as tickets_dal


async def _get_recipient_first_name_async(loaders: Dataloaders, email: str) -> str:
    stakeholder = await loaders.stakeholder.load(email)
    first_name = stakeholder.first_name if stakeholder else ""
    if not first_name:
        first_name = email.split("@")[0]
    else:
        # First name exists in database
        pass
    return str(first_name)


def translate_group_reason(reason: str) -> str:
    translation = {
        GroupReason.DIFF_SECTST.value: "Different security testing strategy",
        GroupReason.MIGRATION.value: "Information will be moved to a different" + " group",
        GroupReason.MISTAKE.value: "Created by mistake",
        GroupReason.NO_SECTST.value: "No more security testing",
        GroupReason.NO_SYSTEM.value: "System will be deprecated",
        GroupReason.OTHER.value: "Other reason not mentioned",
        GroupReason.RENAME.value: "Group rename",
        GroupReason.POC_OVER.value: "Proof of concept over",
        GroupReason.TR_CANCELLED.value: "Testing request cancelled",
        GroupReason.TRIAL_FINALIZATION.value: "Trial expiration",
    }

    return translation[reason]


def translate_group_subscription(group: Group) -> str:
    subscription = group.state.type
    has_advanced = group.state.has_advanced
    translation = {
        GroupSubscriptionType.CONTINUOUS: "Continuous Hacking - "
        + ("Advanced Plan" if has_advanced else "Essential Plan"),
        GroupSubscriptionType.ONESHOT: "Oneshot Hacking",
    }
    if subscription in translation:
        return translation[subscription]
    return subscription.capitalize().replace("_", " ")


def diff_months(first_date: datetime, second_date: datetime) -> float:
    return round(
        (second_date.year - first_date.year) * 12
        + second_date.month
        - first_date.month
        + (second_date.day - first_date.day) / 30,
        1,
    )


def diff_days(first_date: datetime, second_date: datetime) -> int:
    return (second_date - first_date).days


async def delete_group(
    *,
    loaders: Dataloaders,
    deletion_date: datetime,
    group: Group,
    unique_authors: int,
    requester_email: str,
    reason: str,
    comments: str,
    attempt: bool | None = False,
    emails: list[str] = [],
) -> None:
    org_id = group.organization_id
    organization = await get_organization(loaders, org_id)
    org_name = organization.name
    users_email = await mailer_utils.get_group_emails_by_notification(
        loaders=loaders,
        group_name=group.name,
        notification="group_alert",
    )
    users_email.extend(FI_MAIL_PRODUCTION.split(","))
    users_email = list(set([*users_email, *emails]))
    days = diff_days(group.created_date, deletion_date)
    months = int(diff_months(group.created_date, deletion_date))
    await groups_mail.send_mail_group_alert(
        loaders,
        users_email,
        {
            "attempt": attempt,
            "created_by": group.created_by,
            "created_date": group.created_date.date(),
            "comments": html.escape(comments, quote=False),
            "date": deletion_date.date(),
            "days": days,
            "group": group.name,
            "months": months,
            "organization": org_name,
            "reason": translate_group_reason(reason),
            "responsible": requester_email,
            "state": "deleted",
            "subscription": translate_group_subscription(group),
            "unique_authors": unique_authors,
        },
    )


def _get_group_changes(
    *,
    group_state: GroupState,
    group_name: str,
    subscription: str,
    service: str,
    has_arm: bool,
    had_arm: bool,
    has_essential: bool,
    has_advanced: bool,
    comments: str,
    reason: str,
) -> dict[str, Any]:
    old_subscription: str = str(group_state.type.value).lower()
    old_service = group_state.service
    translations: dict[Any, str] = {
        "continuous": "Continuous Hacking",
        "oneshot": "One-Shot Hacking",
        True: "Active",
        False: "Inactive",
    }

    return {
        "Name": group_name,
        "Type": {
            "from": translations.get(old_subscription, old_subscription),
            "to": translations.get(subscription, subscription),
        },
        "Service": {
            "from": str(old_service.value if old_service else "").capitalize(),
            "to": service.capitalize(),
        },
        "ARM": {
            "from": translations[had_arm],
            "to": translations[has_arm],
        },
        "Essential": {
            "from": translations[group_state.has_essential],
            "to": translations[has_essential],
        },
        "Advanced": {
            "from": translations[group_state.has_advanced],
            "to": translations[has_advanced],
        },
        "Comments": comments,
        "Reason": reason.replace("_", " ").capitalize(),
    }


async def update_group(
    *,
    loaders: Dataloaders,
    comments: str,
    group_name: str,
    group_state: GroupState,
    had_arm: bool,
    has_arm: bool,
    has_essential: bool,
    has_advanced: bool,
    reason: str,
    requester_email: str,
    service: str,
    subscription: str,
) -> None:
    group_changes = _get_group_changes(
        group_state=group_state,
        group_name=group_name,
        subscription=subscription,
        service=service,
        has_arm=has_arm,
        had_arm=had_arm,
        has_essential=has_essential,
        has_advanced=has_advanced,
        comments=comments,
        reason=reason,
    )
    await send_mail_services(
        loaders=loaders,
        group_name=group_name,
        group_changes=group_changes,
        requester_email=requester_email,
    )


async def send_mail_services(
    *,
    loaders: Dataloaders,
    group_name: str,
    group_changes: dict[str, Any],
    requester_email: str,
) -> None:
    users_email = await mailer_utils.get_group_emails_by_notification(
        loaders=loaders,
        group_name=group_name,
        notification="updated_services",
    )
    users_email.extend(FI_MAIL_PROJECTS.split(","))

    await groups_mail.send_mail_updated_services(
        loaders=loaders,
        group_name=group_name,
        responsible=requester_email,
        group_changes=group_changes,
        report_date=datetime_utils.get_utc_now(),
        email_to=users_email,
    )


async def new_group(
    *,
    loaders: Dataloaders,
    description: str,
    group_name: str,
    has_essential: bool,
    has_advanced: bool,
    organization: str,
    requester_email: str,
    service: GroupService,
    subscription: GroupSubscriptionType,
) -> bool | None:
    translations: dict[str | bool, str] = {
        "black": "Black",
        "continuous": "Continuous Hacking",
        "oneshot": "One-Shot Hacking",
        "white": "White",
        True: "Active",
        False: "Inactive",
    }
    group_type = translations.get(subscription.value.lower(), subscription.value.capitalize())
    group_service = translations.get(service.value.lower(), service.value.capitalize())
    if "trial" in description.lower():
        return cast(
            bool,
            await tickets_dal.create_ticket(
                subject=f"Group created: {group_name}",
                description=f"""
                You are receiving this email because you have created a group
                through Fluid Attacks platform.

                Here are the details of the group:
                - Name: {group_name}
                - Description: {description}
                - Type: {group_type}
                - Service: {group_service}
                - Organization: {organization}
                - Advanced: {translations[has_advanced]}
                - Essential: {translations[has_essential]}

                If you require any further information,
                do not hesitate to contact us.
            """,
                requester_email=requester_email,
            ),
        )

    await send_mails_async(
        loaders=loaders,
        email_to=FI_MAIL_PROJECTS.split(","),
        context={
            "group_name": group_name.capitalize(),
            "description": description,
            "type": group_type,
            "service": group_service,
            "organization": organization.capitalize(),
            "advanced": translations[has_advanced],
            "essential": translations[has_essential],
            "responsible": requester_email,
        },
        subject=f"Group created:  {group_name}",
        template_name="group_added",
    )
    return None


async def request_managed(
    *,
    group_name: str,
    managed: GroupManaged,
    organization_name: str,
    requester_email: str,
) -> bool:
    translations: dict[str, str] = {
        "MANAGED": "Managed",
        "NOT_MANAGED": "Not Managed",
        "UNDER_REVIEW": "Under Review",
        "TRIAL": "Trial",
    }

    return cast(
        bool,
        await tickets_dal.create_ticket(
            subject=f"{translations[managed]} managed requested: {group_name}",
            description=f"""
                You are receiving this email because you have requested
                to {translations[managed]} managed a group, through
                Fluid Attacks platform.

                Here are the details of the group:
                - Name: {group_name}
                - Organization: {organization_name}

                If you require any further information,
                do not hesitate to contact us.
            """,
            requester_email=requester_email,
        ),
    )


async def new_password_protected_report(
    *,
    loaders: Dataloaders,
    user_email: str,
    group_name: str,
    file_type: str,
    include_report: bool = True,
) -> None:
    today = datetime_utils.get_now()
    fname = await _get_recipient_first_name_async(loaders, user_email)
    subject = f'{file_type}{" Report for" if include_report else ""} [{group_name}]'
    await groups_mail.send_mail_group_report(
        loaders=loaders,
        email_to=[user_email],
        context={
            "filetype": file_type,
            "fname": fname,
            "date": datetime_utils.get_as_str(today, "%Y-%m-%d"),
            "year": datetime_utils.get_as_str(today, "%Y"),
            "time": datetime_utils.get_as_str(today, "%H:%M"),
            "group_name": group_name,
            "subject": subject,
        },
        report=include_report,
    )


async def request_health_check(
    requester_email: str,
    group_name: str,
    repo_url: str,
    branch: str,
) -> None:
    await tickets_dal.create_ticket(
        subject=f"Health Check requested: {group_name}",
        description=f"""
            You are receiving this email because you have requested a health
            check for a repository in {group_name.capitalize()} group
            through Fluid Attacks platform.

            Here are the details of the repository:
            - URL: {repo_url}
            - branch: {branch}

            If you require any further information,
            do not hesitate to contact us.
        """,
        requester_email=requester_email,
    )


async def request_vulnerability_zero_risk(
    loaders: Dataloaders,
    finding_id: str,
    justification: str,
    requester_email: str,
    vulnerabilities: Iterable[Vulnerability],
) -> bool:
    finding = await loaders.finding.load(finding_id)
    if finding is None:
        raise FindingNotFound()

    finding_title = finding.title
    group_name = finding.group_name

    group = await get_group(loaders, group_name)
    org_id = group.organization_id
    organization = await get_organization(loaders, org_id)
    org_name = organization.name
    finding_url = f"{BASE_URL}/orgs/{org_name}/groups/{group_name}/vulns/{finding_id}/locations"
    new_line = "\n\t"
    description = f"""
        You are receiving this case because a zero risk vulnerability has been
        requested through Fluid Attacks platform.

        Here are the details of the zero risk vulnerability:
        Group: {group_name}

        - Finding: {finding_title}
        - ID: {finding_id}
        - URL: {finding_url}
        - Justification: {justification}

        Locations:
        {new_line+new_line.join(
            [
                f"- {vulnerability.state.where} "
                f"| {vulnerability.state.specific}"
                for vulnerability in vulnerabilities
            ]
        )}

        If you require any further information,
        do not hesitate to contact us.
    """

    return cast(
        bool,
        await tickets_dal.create_ticket(
            subject="Requested zero risk vulnerabilities",
            description=description,
            requester_email=requester_email,
        ),
    )


async def request_groups_upgrade(
    loaders: Dataloaders,
    user_email: str,
    groups: Iterable[Group],
) -> None:
    organization_ids = list(group.organization_id for group in groups)
    organizations = await collect(
        list(get_organization(loaders, organization_id) for organization_id in organization_ids),
    )
    organizations_message = "".join(
        f"""
            - Organization {organization.name}:
                {', '.join(
                    group.name
                    for group in groups
                    if group.organization_id == organization.id)
                }
        """
        for organization in organizations
    )

    await tickets_dal.create_ticket(
        subject="Subscription upgrade requested",
        description=f"""
            You are receiving this email because you have requested an upgrade
            to the Advanced plan for the following groups:
            {organizations_message}
            If you require any further information,
            do not hesitate to contact us.
        """,
        requester_email=user_email,
    )


async def request_other_payment_methods(
    *,
    business_legal_name: str,
    city: str,
    country: str,
    efactura_email: str,
    rut: UploadFile | None,
    tax_id: UploadFile | None,
    user_email: str,
) -> None:
    attachments = tuple(attachment for attachment in (rut, tax_id) if attachment is not None)
    efactura_text = f"- Email (e-factura): {efactura_email}" if efactura_email else ""

    await tickets_dal.create_ticket(
        attachments=attachments,
        subject="Other payment methods requested",
        description=f"""
            You are receiving this email because you have requested other
            payment methods.

            - Business legal name: {business_legal_name}
            - Country: {country}
            - City: {city}
            {efactura_text}

            If you require any further information,
            do not hesitate to contact us.
        """,
        requester_email=user_email,
    )


async def create_ticket_for_billing_anomaly_alert(
    *,
    report: Report,
    organization_name: str,
    requester_email: EmailStr,
    customer_manager: EmailStr,
) -> None:
    await tickets_dal.create_ticket(
        subject=f"A billing alert has been reported on [{organization_name}]",
        description=f"""
            - Take place at: {report.state.modified_at.isoformat()}
            - Details: {report.message}
            - Reported by: {report.state.modified_by}
            - Organization customer manager: {customer_manager}
        """,
        requester_email=requester_email,
    )
