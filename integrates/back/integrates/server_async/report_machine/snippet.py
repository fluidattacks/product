from aioextensions import (
    collect,
)
from fluidattacks_core.serializers.snippet import (
    Snippet,
)

from integrates.class_types.types import (
    Item,
)
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.vulnerabilities.enums import (
    VulnerabilityStateStatus,
    VulnerabilityType,
)
from integrates.db_model.vulnerabilities.types import (
    Vulnerability,
)
from integrates.vulnerabilities.domain import (
    set_snippet,
)

MACHINE_EMAIL = "machine@fluidattacks.com"


async def _set_snippet(vulnerability: Vulnerability, sarif_vulns: list[Item]) -> None:
    sarif_vuln: Item | None = next(
        (vuln for vuln in sarif_vulns if int(vuln["fingerprints"]["guid"]) == vulnerability.hash),
        None,
    )
    if (
        sarif_vuln
        and (location := sarif_vuln["locations"][0]["physicalLocation"])
        and (context_region := location.get("contextRegion"))
        and context_region.get("properties")
    ):
        await set_snippet(
            contents=Snippet(
                content=context_region["snippet"]["text"],
                offset=context_region["properties"]["offset"],
                line=context_region["properties"]["line"],
                column=context_region["properties"]["column"],
                line_context=context_region["properties"]["line_context"],
                wrap=context_region["properties"]["wrap"],
                show_line_numbers=context_region["properties"]["show_line_numbers"],
                highlight_line_number=context_region["properties"]["highlight_line_number"],
            ),
            vulnerability_id=vulnerability.id,
        )


async def update_snippets(
    loaders: Dataloaders,
    persisted_vulns: set[str],
    sarif_vulns: list[Item],
) -> None:
    vulnerabilities: list[Vulnerability] = [
        vuln
        for vuln in (await loaders.vulnerability.load_many(persisted_vulns))
        if vuln
        and vuln.type == VulnerabilityType.LINES
        and vuln.state.status == VulnerabilityStateStatus.VULNERABLE
    ]
    await collect(_set_snippet(vuln, sarif_vulns) for vuln in vulnerabilities)
