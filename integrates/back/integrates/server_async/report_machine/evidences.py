import logging
import os
from io import (
    BytesIO,
)
from tempfile import (
    SpooledTemporaryFile,
)

from PIL import (
    ImageFont,
)
from PIL.Image import (
    Image,
)
from PIL.Image import (
    new as new_image,
)
from PIL.Image import (
    open as open_image,
)
from PIL.ImageDraw import (
    Draw,
    ImageDraw,
)
from starlette.datastructures import (
    Headers,
    UploadFile,
)

from integrates.custom_utils.string import (
    boxify,
)
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.constants import (
    MACHINE_EMAIL,
)
from integrates.db_model.findings.types import (
    Finding,
)
from integrates.dynamodb.exceptions import (
    ConditionalCheckFailedException,
)
from integrates.findings.domain.evidence import (
    update_evidence,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)
LOGGER = logging.getLogger(__name__)

DUMMY_IMG: Image = new_image("RGB", (0, 0))
DUMMY_DRAWING: ImageDraw = Draw(DUMMY_IMG)


def clarify_blocking(image: Image, ratio: float) -> Image:
    image_mask: Image = image.convert("L")
    image_mask_pixels = image_mask.load()
    image_width, image_height = image_mask.size

    for i in range(image_width):
        for j in range(image_height):
            if image_mask_pixels[i, j]:
                image_mask_pixels[i, j] = int(ratio * 0xFF)

    image.putalpha(image_mask)

    return image


async def to_png(*, string: str, margin: int = 25) -> Image:
    font = ImageFont.truetype(
        font=os.environ["SKIMS_ROBOTO_FONT"],
        size=18,
    )
    watermark: Image = clarify_blocking(
        image=open_image(os.environ["SKIMS_FLUID_WATERMARK"]),
        ratio=0.15,
    )
    string = boxify(string=string)
    size = DUMMY_DRAWING.multiline_textbbox((0, 0), string, font=font)[-2:]

    size = (size[0] + 2 * margin, size[1] + 2 * margin)
    watermark_size = (
        size[0] // 2,
        watermark.size[1] * size[0] // watermark.size[0] // 2,
    )
    watermark_position = (
        (size[0] - watermark_size[0]) // 2,
        (size[1] - watermark_size[1]) // 2,
    )

    img: Image = new_image("RGB", size, (0xFF, 0xFF, 0xFF))

    drawing: ImageDraw = Draw(img)
    drawing.multiline_text(
        xy=(margin, margin),
        text=string,
        fill=(0x33, 0x33, 0x33),
        font=font,
    )

    watermark = watermark.resize(watermark_size)
    img.paste(watermark, watermark_position, watermark)
    return img


async def upload_evidence(
    loaders: Dataloaders,
    finding: Finding,
    evidence_description: str,
    evidence_content: str,
    author_email: str | None = None,
) -> bool:
    author_email = author_email or MACHINE_EMAIL
    image = await to_png(string=evidence_content)
    with SpooledTemporaryFile(mode="wb") as file:
        headers = Headers(headers={"content_type": "image/png"})
        file_object = UploadFile(
            filename="evidence",
            headers=headers,
            file=file,  # type: ignore
        )
        image_bytes = BytesIO()
        image.save(image_bytes, format="PNG")
        image_bytes.seek(0)
        await file_object.write(image_bytes.read())
        await file_object.seek(0)

        try:
            await update_evidence(
                loaders=loaders,
                finding_id=finding.id,
                evidence_id="evidence_route_5",
                file_object=file_object,
                author_email=author_email,
                description=evidence_description,
                is_draft=False,
            )
        except ConditionalCheckFailedException as exc:
            LOGGER.exception(exc, extra={"group": finding.group_name})
            return False

    loaders.finding.clear(finding.id)
    return True
