import logging
from datetime import (
    datetime,
)

from aioextensions import (
    collect,
)

from integrates.class_types.types import (
    Item,
)
from integrates.custom_utils import datetime as datetime_utils
from integrates.custom_utils.criteria import (
    CRITERIA_REQUIREMENTS,
    CRITERIA_VULNERABILITIES,
)
from integrates.custom_utils.groups import get_group
from integrates.custom_utils.roots import get_unsolved_events_by_root
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model.events.enums import EventSolutionReason, EventType
from integrates.db_model.findings.types import (
    Finding,
)
from integrates.db_model.groups.enums import (
    GroupManaged,
)
from integrates.db_model.roots.enums import (
    RootMachineStatus,
    RootStatus,
)
from integrates.db_model.roots.types import GitRoot, IPRoot, URLRoot
from integrates.events.domain import add_event, solve_event
from integrates.organizations.utils import (
    get_organization,
)
from integrates.roots.utils import (
    update_root_machine_status,
)
from integrates.server_async.report_machine.finding import (
    filter_same_findings,
)
from integrates.server_async.report_machine.report_types import (
    FindingSarifVulnerabilitiesData,
    ScaNewInfo,
)
from integrates.server_async.report_machine.vulnerability import (
    process_criteria_finding,
)
from integrates.server_async.report_machine.vulns_helpers import (
    get_sca_new_required_info,
    get_vuln_machine_hash,
)
from integrates.server_async.utils import (
    get_config,
    get_results_log,
)

logging.getLogger("boto").setLevel(logging.ERROR)
logging.getLogger("botocore").setLevel(logging.ERROR)
logging.getLogger("boto3").setLevel(logging.ERROR)

LOGGER = logging.getLogger(__name__)
EXECUTION_TECHNIQUES = {"apk", "cspm", "dast", "sast", "sca"}
MACHINE_EMAIL = "machine@fluidattacks.com"


async def _get_rules_finding(
    *,
    group_name: str,
    loaders: Dataloaders,
    results: Item,
) -> list[tuple[str, tuple[Finding, ...]]] | None:
    rules_id: set[str] = {item["id"] for item in results["runs"][0]["tool"]["driver"]["rules"]}
    if not rules_id:
        return None

    group_findings = await loaders.group_findings.load(group_name)
    rules_finding: list[tuple[str, tuple[Finding, ...]]] = []
    for fin_code in rules_id:
        same_type_of_findings = filter_same_findings(fin_code, group_findings)
        rules_finding.append((fin_code, same_type_of_findings))

    return rules_finding


def extract_unique_arns(messages: list[str]) -> list[str]:
    unique_arns = set()
    for message in messages:
        split_message = message.split(" ")
        unique_arns.add(f"- {split_message[0]}")
    return list(unique_arns)


def _get_sarif_error_responses(
    sarif_log: Item,
) -> dict[str, dict[str, str | None | list[str]]]:
    sarif_url_responses = {
        "http_responses": {
            req["properties"]["url"].rstrip("/"): req["statusCode"]
            for req in sarif_log["runs"][0]["webResponses"]
            if req.get("statusCode")
        },
        "ssl_responses": {
            req["properties"]["url"].rstrip("/"): req["properties"]["ssl_status"]
            for req in sarif_log["runs"][0]["webResponses"]
            if req["properties"].get("ssl_status", None) is not None
        },
    }

    if cspm_errors := next(
        (
            req["properties"].get("error_methods")
            for req in sarif_log["runs"][0]["webResponses"]
            if req["properties"]["url"] == "cspm_methods"
        ),
        None,
    ):
        sarif_url_responses["cspm_responses"] = cspm_errors

    return sarif_url_responses


async def manage_events(
    sarif_error_responses: dict,
    loaders: Dataloaders,
    group_name: str,
    git_root: GitRoot | IPRoot | URLRoot,
) -> None:
    cspm_responses = sarif_error_responses.get("cspm_responses")

    cspm_events = cspm_responses.get("aws_permissions_problems") if cspm_responses else None
    affected_environments = (
        extract_unique_arns(cspm_events) if cspm_events and isinstance(cspm_events, list) else None
    )
    list_problems = "\n".join(affected_environments) if affected_environments else None
    detail = (
        "We noticed that the AWS CSPM scanner is only running partially "
        "in some of your environments. Could you take a quick peek at your "
        "existing roles and SCP (Service Control Policy)? "
        "We just want to make sure the scans are covering everything you intend."
        " \n\nAffected environments:\n"
    )

    unresolved_events = await get_unsolved_events_by_root(
        loaders=loaders,
        group_name=group_name,
        specific_type=EventType.ENVIRONMENT_ISSUES,
    )
    role_events = [
        event
        for events in unresolved_events.values()
        for event in events
        if detail in event.description and git_root.id == event.root_id
    ]

    if not role_events and git_root.state.status == RootStatus.ACTIVE and cspm_events:
        await add_event(
            loaders=loaders,
            hacker_email=MACHINE_EMAIL,
            group_name=group_name,
            detail=f"{detail}{list_problems}",
            event_date=datetime_utils.get_utc_now(),
            root_id=git_root.id,
            event_type=EventType.ENVIRONMENT_ISSUES,
            environment_url=None,
        )

    if not cspm_events:
        for event in role_events:
            await solve_event(
                loaders=loaders,
                event_id=event.id,
                group_name=group_name,
                hacker_email=MACHINE_EMAIL,
                reason=EventSolutionReason.ENVIRONMENT_IS_WORKING_NOW,
                other=None,
            )


def get_finding_vulns_to_report(
    results: Item, fin_code: str, new_sca_data: ScaNewInfo | None
) -> list[Item]:
    if new_sca_data:
        return [
            vuln
            for vuln in results["runs"][0]["results"]
            if vuln["ruleId"] == fin_code
            and (
                not vuln["properties"].get("is_sca_new")
                or get_vuln_machine_hash(vuln) in new_sca_data.new_sca_vulns_to_report
            )
        ]
    return [vuln for vuln in results["runs"][0]["results"] if vuln["ruleId"] == fin_code]


async def _process_criteria_vulns(
    *,
    execution_id: str,
    loaders: Dataloaders,
    group_name: str,
    criteria_vulns: Item | None,
    sarif_path: str | None = None,
    execution_config: Item,
    git_root: GitRoot,
    criteria_reqs: Item | None,
) -> tuple[bool, str] | None:
    results = await get_results_log(execution_id, sarif_path)
    if not results:
        LOGGER.warning(
            "Could not find execution result",
            extra={"extra": {"execution_id": execution_id}},
        )
        return False, "Could not find execution result"

    rules_finding = await _get_rules_finding(
        group_name=group_name,
        loaders=loaders,
        results=results,
    )
    if rules_finding is None:
        return True, "Rules id is not found"
    criteria_findings = criteria_vulns or CRITERIA_VULNERABILITIES
    criteria_requirements = criteria_reqs or CRITERIA_REQUIREMENTS
    organization_name = (
        await get_organization(loaders, (await get_group(loaders, group_name)).organization_id)
    ).name
    sarif_error_responses = _get_sarif_error_responses(results)

    new_sca_data = None
    if execution_config.get("sca", {}).get("use_new_sca"):
        new_sca_data = await get_sca_new_required_info(loaders, group_name, git_root.id, results)

    await collect(
        [
            process_criteria_finding(
                loaders=loaders,
                group_name=group_name,
                criteria_vulnerability=criteria_findings[fin_code],
                criteria_requirements=criteria_requirements,
                same_type_of_findings=same_type_of_findings,
                language=str(execution_config["language"]).lower(),
                git_root=git_root,
                finding_sarif_info=FindingSarifVulnerabilitiesData(
                    fin_code=fin_code,
                    commit=results["runs"][0]["versionControlProvenance"][0]["revisionId"],
                    finding_reported_vulnerabilities=get_finding_vulns_to_report(
                        results=results,
                        fin_code=fin_code,
                        new_sca_data=new_sca_data,
                    ),
                    sarif_error_responses=sarif_error_responses,
                    new_sca_data=new_sca_data,
                ),
                execution_config=execution_config,
                organization_name=organization_name,
            )
            for fin_code, same_type_of_findings in rules_finding
        ],
    )

    return None


def _get_executed_techniques(execution_config: Item) -> set[str]:
    executed_techniques = set(execution_config) & EXECUTION_TECHNIQUES

    return executed_techniques or EXECUTION_TECHNIQUES


async def _update_root_machine_status(
    *,
    group_name: str,
    git_root: GitRoot,
    commit: str | None = None,
    commit_date: str | None = None,
    execution_id: str,
    user_email: str,
    executed_techniques: set[str],
) -> tuple[bool, str]:
    _commit_date = datetime.fromisoformat(commit_date) if commit_date else None
    message = "Execution has been processed"
    await update_root_machine_status(
        group_name=group_name,
        root_id=git_root.id,
        status=RootMachineStatus.SUCCESS,
        message=message,
        commit=commit,
        commit_date=_commit_date,
        execution_id=execution_id,
        modified_by=user_email,
        executed_techniques=executed_techniques,
    )

    return True, message


async def process_execution(
    *,
    execution_id: str,
    criteria_vulns: Item | None = None,
    criteria_reqs: Item | None = None,
    config_path: str | None = None,
    sarif_path: str | None = None,
    commit: str | None = None,
    commit_date: str | None = None,
    user_email: str = "integrates@fluidattacks.com",
) -> tuple[bool, str]:
    loaders: Dataloaders = get_new_context()
    group_name = execution_id.split("_", maxsplit=1)[0]
    group = await loaders.group.load(group_name)
    if group and (
        group.state.has_essential is False or group.state.managed == GroupManaged.UNDER_REVIEW
    ):
        LOGGER.warning(
            "Machine services is not included or is under review",
            extra={
                "extra": {
                    "execution_id": execution_id,
                    "group_name": group_name,
                },
            },
        )
        return False, "Machine services is not included or is under review"

    execution_config = await get_config(execution_id, config_path)
    try:
        git_root = next(
            root
            for root in await loaders.group_roots.load(group_name)
            if isinstance(root, GitRoot)
            and root.state.status == RootStatus.ACTIVE
            and root.state.nickname == execution_config["namespace"]
        )
    except StopIteration:
        LOGGER.warning(
            "Could not find root for the execution",
            extra={
                "extra": {
                    "execution_id": execution_id,
                    "nickname": execution_config["namespace"],
                },
            },
        )
        return False, "Could not find root for the execution"

    error_processing = await _process_criteria_vulns(
        execution_id=execution_id,
        loaders=loaders,
        group_name=group_name,
        criteria_vulns=criteria_vulns,
        criteria_reqs=criteria_reqs,
        sarif_path=sarif_path,
        execution_config=execution_config,
        git_root=git_root,
    )
    if error_processing is not None:
        return error_processing

    return await _update_root_machine_status(
        group_name=group_name,
        git_root=git_root,
        commit=commit,
        commit_date=commit_date,
        execution_id=execution_id,
        user_email=user_email,
        executed_techniques=_get_executed_techniques(execution_config),
    )
