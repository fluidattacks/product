import logging
from contextlib import (
    suppress,
)

from integrates.class_types.types import (
    Item,
)
from integrates.custom_exceptions import (
    InvalidRootComponent,
    InvalidUrl,
    RepeatedToeInput,
)
from integrates.custom_utils.datetime import (
    get_utc_now,
)
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.roots.types import (
    RootEnvironmentUrlsRequest,
)
from integrates.toe.inputs import (
    domain as toe_inputs_domain,
)
from integrates.toe.inputs.types import (
    ToeInputAttributesToAdd,
)

# Constants
LOGGER = logging.getLogger(__name__)


async def ensure_toe_inputs(
    loaders: Dataloaders,
    group_name: str,
    root_id: str,
    stream: Item,
) -> None:
    vulns_for_toe: list[Item] = [
        vuln for vuln in stream["inputs"] if vuln["state"] in {"open", "submitted"}
    ]
    envs_urls = await loaders.root_environment_urls.load(
        RootEnvironmentUrlsRequest(root_id=root_id, group_name=group_name),
    )

    for vuln in vulns_for_toe:
        url = str(vuln["url"]).rsplit(" (", maxsplit=1)[0]
        with suppress(RepeatedToeInput):
            try:
                attack_date = get_utc_now()
                await toe_inputs_domain.add(
                    loaders=loaders,
                    group_name=group_name,
                    component=url,
                    entry_point="",
                    environment_id=next(
                        (env_url.id for env_url in envs_urls if env_url.url == url),
                        "",
                    ),
                    root_id=root_id,
                    attributes=ToeInputAttributesToAdd(
                        be_present=True,
                        attacked_at=attack_date,
                        attacked_by="machine@fluidattacks.com",
                        first_attack_at=attack_date,
                        has_vulnerabilities=False,
                        seen_first_time_by="machine@fluidattacks.com",
                    ),
                )
            except (InvalidRootComponent, InvalidUrl) as ex:
                LOGGER.error(
                    "Invalid toe for the root",
                    extra={
                        "extra": {
                            "group_name": group_name,
                            "root_id": root_id,
                            "url": url,
                            "ex": str(ex),
                        },
                    },
                )
