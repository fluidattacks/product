import hashlib
import json
from typing import cast

from aioextensions import collect

from integrates.class_types.types import (
    Item,
)
from integrates.context import FI_LLM_SCAN_DB_MODEL_PATH
from integrates.custom_utils.criteria import (
    CRITERIA_REQUIREMENTS,
    CRITERIA_VULNERABILITIES,
)
from integrates.custom_utils.groups import get_group
from integrates.custom_utils.roots import get_root
from integrates.custom_utils.vulnerabilities import (
    is_machine_vuln,
)
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model.enums import (
    Source,
    StateRemovalJustification,
)
from integrates.db_model.findings.types import (
    Finding,
)
from integrates.db_model.groups.types import (
    Group,
)
from integrates.db_model.roots.enums import (
    RootStatus,
)
from integrates.db_model.roots.types import (
    GitRoot,
)
from integrates.db_model.vulnerabilities.enums import (
    VulnerabilityStateStatus,
)
from integrates.db_model.vulnerabilities.types import (
    Vulnerability,
)
from integrates.dynamodb import keys
from integrates.dynamodb import operations as dynamodb_ops
from integrates.dynamodb.tables import load_tables
from integrates.findings.domain.core import (
    remove_finding,
)
from integrates.server_async.gen_snippets import set_snippet_for_vulnerability
from integrates.server_async.report_machine.evidences import (
    upload_evidence,
)
from integrates.server_async.report_machine.finding import (
    create_finding,
    filter_same_findings,
    get_target_finding,
    update_finding_metadata,
)
from integrates.server_async.report_machine.vulnerability import (
    get_vulns_to_open_or_submit,
    persist_vulnerabilities,
)

with open(FI_LLM_SCAN_DB_MODEL_PATH, encoding="utf-8") as file:
    TABLE = load_tables(json.load(file))[0]


async def _report(
    *,
    loaders: Dataloaders,
    group: Group,
    git_root: GitRoot,
    target_finding: Finding,
    vulnerability_file: Item,
    author_email: str | None = None,
) -> None:
    author_email = author_email or "machine@fluidattacks.com"
    organization = await loaders.organization.load(group.organization_id)
    if not organization:
        return

    existing_reported_machine_vulns: tuple[Vulnerability, ...] = tuple(
        vuln
        for vuln in await loaders.finding_vulnerabilities.load(target_finding.id)
        if is_machine_vuln(vuln, author_email)
        and vuln.root_id == git_root.id
        and vuln.state.status
        in {
            VulnerabilityStateStatus.REJECTED,
            VulnerabilityStateStatus.SUBMITTED,
            VulnerabilityStateStatus.VULNERABLE,
        }
    )

    vulns_stream = get_vulns_to_open_or_submit(
        {
            "inputs": [],
            "lines": [
                vulnerability_file,
            ],
        },
        existing_reported_machine_vulns,
    )

    if vulns_stream["lines"]:
        await persist_vulnerabilities(
            loaders=loaders,
            group_name=group.name,
            git_root=git_root,
            finding=target_finding,
            stream=vulns_stream,
            organization_name=organization.name,
            user_email=author_email,
        )


async def process_vulnerabilities(
    group_name: str,
    git_root_nickname: str,
    finding_code: str,
    vulnerability_file: Item,
) -> tuple[bool, str]:
    loaders: Dataloaders = get_new_context()
    group = await loaders.group.load(group_name)
    if not group:
        return False, "Group is not found"

    git_root = next(
        (
            root
            for root in (await loaders.group_roots.load(group_name))
            if isinstance(root, GitRoot)
            and root.state.status == RootStatus.ACTIVE
            and root.state.nickname == git_root_nickname
        ),
        None,
    )
    if not git_root:
        return False, "Git root is not found"

    if await loaders.vulnerability_by_hash.load((str(vulnerability_file["hash"]), git_root.id)):
        return False, "Vulnerability exists in the root"

    group_findings = await loaders.group_findings.load(group_name)
    same_type_of_findings = filter_same_findings(finding_code, group_findings)
    target_finding = get_target_finding(same_type_of_findings)

    new_finding = False
    if not target_finding:
        target_finding = await create_finding(
            loaders,
            group.name,
            finding_code,
            group.language.value,
            CRITERIA_VULNERABILITIES[finding_code],
        )

        has_evidence = await upload_evidence(
            loaders,
            target_finding,
            vulnerability_file["message"]["description"],
            vulnerability_file["message"]["snippet"],
        )
        new_finding = True
    else:
        await update_finding_metadata(
            (group.name, finding_code, group.language.value),
            target_finding,
            CRITERIA_VULNERABILITIES[finding_code],
            CRITERIA_REQUIREMENTS,
        )
        loaders.group_findings.clear(group.name)
        loaders.finding.clear(target_finding.id)

    if new_finding and not has_evidence:
        return False, "An error ocurred creating the finding evidence"

    await _report(
        loaders=loaders,
        group=group,
        git_root=git_root,
        target_finding=target_finding,
        vulnerability_file=vulnerability_file,
    )

    loaders.finding_vulnerabilities.clear(target_finding.id)
    if new_finding and not await loaders.finding_vulnerabilities.load(target_finding.id):
        await remove_finding(
            loaders,
            email="machine@fluidattacks.com",
            finding_id=target_finding.id,
            justification=StateRemovalJustification.REPORTING_ERROR,
            source=Source.MACHINE,
        )

    return True, "Vulnerabilities have been processed"


async def process_vulnerability_llm(
    *,
    root_id: str,
    where: str,
    snippet_hash: str,
    candidate_id: str,
) -> tuple[bool, str]:
    loaders: Dataloaders = get_new_context()
    vulnerability_raw = await dynamodb_ops.get_item(
        facets=(TABLE.facets["snippet_vulnerability"],),
        key=keys.build_key(
            facet=TABLE.facets["snippet_vulnerability"],
            values={
                "where": where,
                "hash": snippet_hash,
                "root_id": root_id,
                "vulnerability_id": candidate_id,
            },
        ),
        table=TABLE,
    )
    if not vulnerability_raw:
        return False, "Vulnerability is not found"
    group_name = vulnerability_raw["group_name"]
    group = await get_group(loaders, group_name)
    git_root = cast(GitRoot, await get_root(loaders, vulnerability_raw["root_id"], group_name))

    # Valida if the vulnerability is in the git root
    finding_code = vulnerability_raw["suggested_criteria_code"]
    group_findings = await loaders.group_findings.load(group_name)
    same_type_of_findings = filter_same_findings(finding_code, group_findings)
    target_finding = get_target_finding(same_type_of_findings, modified_by="ai@fluidattacks.com")

    snippet_raw = await dynamodb_ops.get_item(
        facets=(TABLE.facets["snippet_metadata"],),
        key=keys.build_key(
            facet=TABLE.facets["snippet_metadata"],
            values={
                "where": vulnerability_raw["where"],
                "hash": vulnerability_raw["snippet_hash"],
                "root_id": git_root.id,
            },
        ),
        table=TABLE,
    )
    if not snippet_raw:
        return False, "Snippet is not found"

    new_finding = False
    if not target_finding:
        target_finding = await create_finding(
            loaders,
            group.name,
            finding_code,
            group.language.value,
            CRITERIA_VULNERABILITIES[finding_code],
            stakeholder_email="ai@fluidattacks.com",
            source=Source.LLM,  # LLM
        )

        has_evidence = await upload_evidence(
            loaders,
            target_finding,
            vulnerability_raw["reason"],
            snippet_raw["snippet_content"],
            author_email="ai@fluidattacks.com",
        )
        new_finding = True
    else:
        await update_finding_metadata(
            (group.name, finding_code, group.language.value),
            target_finding,
            CRITERIA_VULNERABILITIES[finding_code],
            CRITERIA_REQUIREMENTS,
        )
        loaders.group_findings.clear(group.name)
        loaders.finding.clear(target_finding.id)

    if new_finding and not has_evidence:
        return False, "An error ocurred creating the finding evidence"

    await _report(
        loaders=loaders,
        group=group,
        git_root=git_root,
        target_finding=target_finding,
        vulnerability_file={
            "repo_nickname": git_root.state.nickname,
            "source": "LLM",  # LLM
            "cwe_ids": [],
            "hash": int.from_bytes(
                hashlib.sha256(
                    bytes(
                        (
                            vulnerability_raw["where"]
                            + str(vulnerability_raw["specific"])
                            + vulnerability_raw["suggested_criteria_code"]
                        ),
                        "utf-8",
                    ),
                ).digest()[:8],
                "little",
            ),
            "commit_hash": vulnerability_raw["commit"],
            "line": str(vulnerability_raw["specific"]),
            "path": vulnerability_raw["where"],
            "technique": "SAST",
            "message": {
                "description": vulnerability_raw["reason"],
            },
            "state": "SUBMITTED",
        },
        author_email="ai@fluidattacks.com",
    )

    loaders.finding_vulnerabilities.clear(target_finding.id)
    if new_finding and not await loaders.finding_vulnerabilities.load(target_finding.id):
        await remove_finding(
            loaders,
            email="ai@fluidattacks.com",
            finding_id=target_finding.id,
            justification=StateRemovalJustification.REPORTING_ERROR,
            source=Source.LLM,  # LLM
        )
    else:
        await collect(
            set_snippet_for_vulnerability(vulnerability_id=x.id, root_commit=x.state.commit)
            for x in await loaders.finding_vulnerabilities.load(target_finding.id)
        )

    return True, "Vulnerabilities have been processed"
