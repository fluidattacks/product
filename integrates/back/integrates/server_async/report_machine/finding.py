from integrates.class_types.types import (
    Item,
)
from integrates.custom_utils import (
    cvss as cvss_utils,
)
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.enums import (
    Source,
)
from integrates.db_model.findings.types import (
    Finding,
    FindingMetadataToUpdate,
)
from integrates.db_model.findings.update import (
    update_metadata,
)
from integrates.findings import (
    domain as findings_domain,
)
from integrates.findings.types import (
    FindingAttributesToAdd,
)
from integrates.server_async.report_machine.report_types import FindingSarifVulnerabilitiesData


async def update_finding_metadata(
    finding_data: tuple[str, str, str],
    finding: Finding,
    criteria_vulnerability: Item,
    criteria_requirements: Item,
) -> None:
    group_name, vulnerability_id, language = finding_data
    language = language.lower()
    current_attrs = FindingMetadataToUpdate(
        attack_vector_description=finding.attack_vector_description,
        description=finding.description,
        min_time_to_remediate=finding.min_time_to_remediate,
        recommendation=finding.recommendation,
        requirements=finding.requirements,
        severity_score=finding.severity_score,
        threat=finding.threat,
        title=finding.title,
    )
    cvss3_vector = cvss_utils.get_criteria_cvss_vector(criteria_vulnerability)
    cvss4_vector = cvss_utils.get_criteria_cvss4_vector(criteria_vulnerability)
    updated_attrs = FindingMetadataToUpdate(
        attack_vector_description=criteria_vulnerability[language]["impact"],
        description=criteria_vulnerability[language]["description"],
        min_time_to_remediate=int(criteria_vulnerability["remediation_time"]),
        recommendation=criteria_vulnerability[language]["recommendation"],
        requirements="\n".join(
            [
                criteria_requirements[item][language]["title"]
                for item in criteria_vulnerability["requirements"]
            ],
        ),
        severity_score=cvss_utils.get_severity_score_from_cvss_vector(cvss3_vector, cvss4_vector),
        threat=criteria_vulnerability[language]["threat"],
        title=(f"{vulnerability_id}. {criteria_vulnerability[language]['title']}"),
    )

    if current_attrs != updated_attrs:
        await update_metadata(
            group_name=group_name,
            finding_id=finding.id,
            metadata=updated_attrs,
        )


async def create_finding(
    loaders: Dataloaders,
    group_name: str,
    vulnerability_id: str,
    language: str,
    criteria_vulnerability: Item,
    stakeholder_email: str | None = None,
    source: Source | None = None,
) -> Finding:
    source = source or Source.MACHINE
    stakeholder_email = stakeholder_email or "machine@fluidattacks.com"
    language = language.lower()
    cvss3_vector = cvss_utils.get_criteria_cvss_vector(criteria_vulnerability)
    cvss4_vector = cvss_utils.get_criteria_cvss4_vector(criteria_vulnerability)

    return await findings_domain.add_finding(
        loaders=loaders,
        group_name=group_name,
        stakeholder_email=stakeholder_email,
        attributes=FindingAttributesToAdd(
            attack_vector_description=criteria_vulnerability[language]["impact"],
            cvss_vector=cvss3_vector,
            cvss4_vector=cvss4_vector,
            description=criteria_vulnerability[language]["description"],
            min_time_to_remediate=criteria_vulnerability["remediation_time"],
            recommendation=criteria_vulnerability[language]["recommendation"],
            source=source,
            threat=criteria_vulnerability[language]["threat"],
            title=(f"{vulnerability_id}. {criteria_vulnerability[language]['title']}"),
            unfulfilled_requirements=criteria_vulnerability["requirements"],
        ),
    )


def filter_same_findings(criteria_vuln_id: str, findings: list[Finding]) -> tuple[Finding, ...]:
    return tuple(fin for fin in findings if fin.title.startswith(f"{criteria_vuln_id}."))


def get_target_finding(
    same_type_of_findings: tuple[Finding, ...],
    modified_by: str | None = None,
) -> Finding | None:
    machine_findings = [
        finding
        for finding in same_type_of_findings
        if finding.creation
        and finding.creation.modified_by == (modified_by or "machine@fluidattacks.com")
    ]
    if len(machine_findings) > 0:
        return sorted(
            machine_findings,
            key=lambda x: x.creation.modified_date.timestamp() if x.creation else 0,
        )[0]
    return None


async def manage_target_finding(
    *,
    loaders: Dataloaders,
    same_type_of_findings: tuple[Finding, ...],
    group_name: str,
    criteria_vulnerability: Item,
    language: str,
    criteria_requirements: Item,
    finding_sarif_info: FindingSarifVulnerabilitiesData,
) -> Finding | None:
    target_finding = get_target_finding(same_type_of_findings)
    if not target_finding:
        if not finding_sarif_info.finding_reported_vulnerabilities or (
            finding_sarif_info.fin_code in {"011", "393"} and finding_sarif_info.new_sca_data
        ):
            return None
        target_finding = await create_finding(
            loaders,
            group_name,
            finding_sarif_info.fin_code,
            language,
            criteria_vulnerability,
        )
    else:
        await update_finding_metadata(
            (group_name, finding_sarif_info.fin_code, language),
            target_finding,
            criteria_vulnerability,
            criteria_requirements,
        )
        loaders.group_findings.clear(group_name)
        loaders.finding.clear(target_finding.id)
    return target_finding
