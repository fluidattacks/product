from textwrap import dedent

from integrates.dataloaders import get_new_context
from integrates.server_async import gen_snippets
from integrates.server_async.gen_snippets import set_snippet_for_vulnerability
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    FindingFaker,
    GitRootCloningFaker,
    GitRootFaker,
    GitRootStateFaker,
    GroupFaker,
    OrganizationFaker,
    VulnerabilityFaker,
    VulnerabilityStateFaker,
)
from integrates.testing.mocks import Mock, mocks

ORG_ID = "ORG#6b110f25-cd46-e94c-2bad-b9e182cf91a8"
FIN_ID = "3c475384-834c-47b0-ac71-a41a022e401c"
ORG_NAME = "orgtest"
GROUP_NAME = "testgroup"
EMAIL_TEST = "testuser@orgtest.com"


def _dedent(content: str) -> str:
    return dedent(content)[1:-1]


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id=ORG_ID, name=ORG_NAME)],
            groups=[GroupFaker(name=GROUP_NAME, organization_id=ORG_ID)],
            roots=[
                GitRootFaker(
                    group_name=GROUP_NAME,
                    id="root1",
                    state=GitRootStateFaker(nickname="root1"),
                    cloning=GitRootCloningFaker(commit="38d41576b572435bcd5ed30b62d0e15f0cc8a2c9"),
                ),
            ],
            findings=[FindingFaker(id=FIN_ID, group_name=GROUP_NAME)],
            vulnerabilities=[
                VulnerabilityFaker(
                    id="9c476334-834c-47b0-ac42-a41a022e401f",
                    finding_id=FIN_ID,
                    root_id="root1",
                    group_name=GROUP_NAME,
                    organization_name=ORG_NAME,
                    state=VulnerabilityStateFaker(specific="4", where="main.py"),
                ),
                VulnerabilityFaker(
                    id="9c476334-834c-47b0-ac42-a41a088e401j",
                    finding_id=FIN_ID,
                    root_id="root_missing",
                    group_name=GROUP_NAME,
                    organization_name=ORG_NAME,
                    state=VulnerabilityStateFaker(specific="4", where="main.py"),
                ),
            ],
        ),
    ),
    others=[
        Mock(
            gen_snippets,
            "get_object_content",
            "async",
            _dedent("""
            import random

            def main() -> None:
                number = random.randint(0, 100)
                print(number)

                return number
            """).encode("utf-8"),
        ),
    ],
)
async def test_process_set_snippet() -> None:
    loaders = get_new_context()
    vuln_id = "9c476334-834c-47b0-ac42-a41a022e401f"
    result, message = await set_snippet_for_vulnerability(
        vuln_id, "38d41576b572435bcd5ed30b62d0e15f0cc8a2c9"
    )
    assert result is True
    assert message == "Snippet has been set"
    vulnerability = await loaders.vulnerability.load(vuln_id)
    assert vulnerability is not None
    snippet = await loaders.vulnerability_snippet.load(
        (
            vulnerability.id,
            vulnerability.state.modified_date,
        )
    )
    assert snippet is not None
    assert snippet.content == _dedent("""
            def main() -> None:
                number = random.randint(0, 100)
                print(number)

                return number
            """)
    result, message = await set_snippet_for_vulnerability(
        vuln_id, "38d41576b572435bcd5ed30b62d0e15f0cc8a2c9"
    )
    assert result is False
    assert message == "Snippet is already set"

    result, _ = await set_snippet_for_vulnerability(
        vuln_id, "45d41576b572435bcd5ed30b62d0e15f0cc8a2c0"
    )
    result, message = await set_snippet_for_vulnerability("mising_vulnid")
    assert message == "Vulnerability does not exist"
    # bad root
    result, message = await set_snippet_for_vulnerability("9c476334-834c-47b0-ac42-a41a088e401j")
    assert message == "Root does not exist"

    result, message = await set_snippet_for_vulnerability(
        vuln_id, "38d41576b572435bcd5ed23b62d0e15f0cc8a4c3"
    )
    assert message == "Root commit does not match"

    assert result is False
