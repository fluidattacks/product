import logging
from contextlib import suppress

from fluidattacks_core.serializers.snippet import (
    Snippet,
    SnippetViewport,
    make_snippet,
    make_snippet_function,
)

from integrates.context import FI_AWS_S3_CONTINUOUS_REPOSITORIES, FI_AWS_S3_PATH_PREFIX
from integrates.custom_exceptions import FileNotFound, InvalidFileType
from integrates.dataloaders import Dataloaders, get_new_context
from integrates.db_model.roots.types import GitRoot, RootRequest
from integrates.db_model.vulnerabilities.enums import VulnerabilityStateStatus
from integrates.db_model.vulnerabilities.types import Vulnerability
from integrates.s3.operations import get_object_content
from integrates.vulnerabilities.domain import set_snippet
from integrates.vulnerabilities.fixes.utils import get_vulnerability_language

LOGGER = logging.getLogger(__name__)


async def build_snippet(
    git_root: GitRoot,
    vulnerability: Vulnerability,
) -> Snippet | None:
    language = get_vulnerability_language(vulnerability)
    try:
        object_key = (
            f"{FI_AWS_S3_PATH_PREFIX}{vulnerability.group_name}"
            f"/{git_root.state.nickname}/"
            f"{vulnerability.state.where}"
        )
    except AttributeError:
        LOGGER.error(
            "Error getting git root",
            extra={
                "extra": {
                    "vulnerability_id": vulnerability.id,
                    "group_name": vulnerability.group_name,
                    "root_id": vulnerability.root_id,
                }
            },
        )
        return None
    try:
        file_content_bytes = await get_object_content(
            object_key,
            bucket=FI_AWS_S3_CONTINUOUS_REPOSITORIES,
        )
        file_content = file_content_bytes.decode(encoding="utf-8")
    except (UnicodeError, InvalidFileType, OSError, FileNotFound):
        LOGGER.exception(
            "Error getting file content",
            extra={
                "extra": {
                    "vulnerability_id": vulnerability.id,
                    "group_name": vulnerability.group_name,
                    "root_id": vulnerability.root_id,
                }
            },
        )
        return None

    snippet = make_snippet_function(
        file_content=file_content,
        language=language,
        viewport=SnippetViewport(
            int(vulnerability.state.specific),
            show_line_numbers=False,
            highlight_line_number=False,
        ),
    )
    if not snippet:
        snippet = make_snippet(
            content=file_content,
            viewport=SnippetViewport(
                int(vulnerability.state.specific),
                line_context=8,
                show_line_numbers=False,
                highlight_line_number=False,
            ),
        )
    return snippet


async def set_snippet_for_vulnerability(
    vulnerability_id: str, root_commit: str | None = None
) -> tuple[bool, str]:
    snippet: Snippet | None = None
    loaders: Dataloaders = get_new_context()
    vulnerability = await loaders.vulnerability.load(vulnerability_id)
    if not vulnerability or vulnerability.state.status not in (
        VulnerabilityStateStatus.REJECTED,
        VulnerabilityStateStatus.SUBMITTED,
        VulnerabilityStateStatus.VULNERABLE,
    ):
        return False, "Vulnerability does not exist"

    git_root = await loaders.root.load(RootRequest(vulnerability.group_name, vulnerability.root_id))
    if not git_root or not isinstance(git_root, GitRoot):
        return False, "Root does not exist"

    if git_root and root_commit and git_root.cloning.commit != root_commit:
        return False, "Root commit does not match"

    with suppress(Exception):
        snippet = await build_snippet(git_root, vulnerability)
    if not snippet:
        return False, "There was an error while building the snippet"
    snippet_db = await loaders.vulnerability_snippet.load(
        (
            vulnerability.id,
            vulnerability.state.modified_date,
        )
    )
    if snippet_db and snippet_db.content == snippet.content:
        return False, "Snippet is already set"

    await set_snippet(
        contents=snippet,
        vulnerability_id=vulnerability.id,
    )
    return True, "Snippet has been set"
