from integrates.jobs_orchestration.model.types import SbomFormat
from integrates.roots.domain import format_environment_id
from integrates.server_async.utils import map_sbom_format
from integrates.testing.utils import parametrize

GROUP_NAME = "group1"
ORG_ID = "org1"
ROOT_ID = "root_id"
EMAIL_TEST = "hacker@fluidattacks.com"
URL = "https://nice-env.net"
URL_ID = format_environment_id(URL)


@parametrize(
    args=["sbom_format", "expected"],
    cases=[
        [SbomFormat.CYCLONEDX_JSON, "CycloneDX"],
        [SbomFormat.SPDX_JSON, "SPDX"],
        [SbomFormat.FLUID_JSON, "Fluid Attacks JSON"],
    ],
)
def test_map_sbom_format(sbom_format: SbomFormat, expected: str) -> None:
    assert map_sbom_format(sbom_format) == expected
