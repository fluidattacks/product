import json
import logging
from datetime import datetime, timedelta

from types_aiobotocore_s3.type_defs import HeadObjectOutputTypeDef

from integrates.dataloaders import Dataloaders, get_new_context
from integrates.db_model.groups.enums import GroupManaged
from integrates.db_model.notifications.types import ReportStatus
from integrates.db_model.roots.enums import RootDockerImageStateStatus, RootStatus
from integrates.db_model.roots.types import GitRoot, RootDockerImage, RootDockerImagesRequest
from integrates.jobs_orchestration.model.types import SbomFormat, SbomJobType
from integrates.notifications.reports import update_report_notification
from integrates.server_async.report_sbom.process import process_packages, process_sbom_to_mail
from integrates.server_async.utils import get_sbom_result, get_sbom_result_head, try_parse_json
from integrates.settings.logger import LOGGING

logging.config.dictConfig(LOGGING)
PROCESS_SBOM_LOGGER = logging.getLogger("process_sbom")


def log_and_return_error(message: str, log_extra: dict) -> tuple[bool, str]:
    PROCESS_SBOM_LOGGER.error(message, extra=log_extra)
    return False, message


async def validate_group(loaders: Dataloaders, group_name: str) -> bool:
    group = await loaders.group.load(group_name)

    if not group:
        return False

    if group.state.has_essential is False or group.state.managed == GroupManaged.UNDER_REVIEW:
        return False

    return True


async def fetch_sbom_object(
    execution_id: str, file_extension: str, job_type: SbomJobType
) -> bytes | None:
    result = await get_sbom_result_head(execution_id, file_extension)
    if not result:
        return None

    return await get_sbom_object(result, job_type, execution_id, file_extension)


async def get_git_root(loaders: Dataloaders, group_name: str, root_nickname: str) -> GitRoot | None:
    return next(
        (
            root
            for root in await loaders.group_roots.load(group_name)
            if isinstance(root, GitRoot)
            and root.state.status == RootStatus.ACTIVE
            and root.state.nickname == root_nickname
        ),
        None,
    )


async def get_docker_image(
    loaders: Dataloaders,
    git_root: GitRoot,
    group_name: str,
    image_ref: str,
) -> RootDockerImage | None:
    return next(
        (
            image
            for image in await loaders.root_docker_images.load(
                RootDockerImagesRequest(root_id=git_root.id, group_name=group_name),
            )
            if image.uri == image_ref and image.state.status != RootDockerImageStateStatus.DELETED
        ),
        None,
    )


async def get_sbom_object(
    result: HeadObjectOutputTypeDef,
    job_type: SbomJobType,
    execution_id: str,
    file_extension: str,
) -> bytes | None:
    if result["ContentLength"] > 10 * 1024 * 1024 and job_type == SbomJobType.REQUEST:
        return None
    return await get_sbom_result(execution_id, file_extension)


async def load_sbom_results(
    *,
    job_type: SbomJobType,
    execution_id: str,
    file_extension: str,
    json_path: str | None = None,
) -> dict | None:
    # json_path is useful to test this flow locally
    if json_path is not None:
        with open(json_path, "rb") as json_file:
            return json.load(json_file)

    sbom_object = await fetch_sbom_object(execution_id, file_extension, job_type)
    if sbom_object is None:
        PROCESS_SBOM_LOGGER.error(
            "Could not find the SBOM object for the execution",
            extra={
                "extra": {
                    "execution_id": execution_id,
                    "file_extension": file_extension,
                    "job_type": job_type,
                }
            },
        )
        return None

    return try_parse_json(sbom_object)


async def _process_job_type(
    *,
    loaders: Dataloaders,
    group_name: str,
    git_root: GitRoot,
    docker_image: RootDockerImage | None,
    job_type: SbomJobType,
    execution_id: str,
    email_to: str | None,
    file_extension: str,
    sbom_format: SbomFormat,
    notification_id: str | None = None,
    json_path: str | None = None,
) -> tuple[bool, str]:
    match job_type:
        case SbomJobType.SCHEDULER:
            PROCESS_SBOM_LOGGER.info(
                "Processing SBOM as a job of type scheduler",
                extra={"extra": {"execution_id": execution_id}},
            )

            sbom_json = await load_sbom_results(
                job_type=job_type,
                execution_id=execution_id,
                file_extension=file_extension,
                json_path=json_path,
            )
            if not isinstance(sbom_json, dict) or sbom_format != SbomFormat.FLUID_JSON:
                return log_and_return_error(
                    "SBOM object is either None or not in the correct format for processing",
                    {"extra": {"sbom_instance": type(sbom_json), "sbom_format": sbom_format}},
                )
            return await process_packages(
                loaders=loaders,
                sbom_json=sbom_json,
                group_name=group_name,
                git_root=git_root,
                docker_image=docker_image,
            )

        case SbomJobType.REQUEST:
            PROCESS_SBOM_LOGGER.info(
                "Processing SBOM as a job of type request",
                extra={"extra": {"execution_id": execution_id}, "email_to": email_to},
            )

            if not email_to:
                return log_and_return_error(
                    "Could not find email to send the SBOM report",
                    {"extra": {"email_to": email_to}},
                )

            await update_report_notification(
                expiration_time=(datetime.now() + timedelta(days=7)),
                notification_id=notification_id or "",
                s3_file_path=f"sbom_results/{execution_id}.{file_extension}",
                size=None,
                status=ReportStatus.READY,
                user_email=email_to,
            )

            return await process_sbom_to_mail(
                loaders=loaders,
                git_root=git_root,
                image_ref=docker_image.uri if docker_image else None,
                email_to=email_to,
                file_extension=file_extension,
                sbom_format=sbom_format,
            )

        case _:
            return log_and_return_error(
                "SBOM execution has not been processed, job type was not recognized",
                {"extra": {"execution_id": execution_id, "job_type": job_type}},
            )


async def process_sbom_execution(
    *,
    execution_id: str,
    job_type: SbomJobType,
    sbom_format: SbomFormat,
    json_path: str | None = None,
    email_to: str | None = None,
    notification_id: str | None = None,
    image_ref: str | None = None,
) -> tuple[bool, str]:
    loaders: Dataloaders = get_new_context()
    execution_id_parts = execution_id.split("/")
    group_name, root_nickname, *_ = execution_id_parts
    file_extension: str = sbom_format.get_file_extension()
    docker_image = None

    if not await validate_group(loaders, group_name):
        return log_and_return_error(
            "Machine service is not included or is under review",
            {
                "extra": {
                    "execution_id": execution_id,
                    "group_name": group_name,
                },
            },
        )

    git_root = await get_git_root(loaders, group_name, root_nickname)

    if not git_root:
        return log_and_return_error(
            "Could not find root for the execution",
            {
                "extra": {"git_root": root_nickname, "group_name": group_name},
            },
        )

    if image_ref:
        docker_image = await get_docker_image(loaders, git_root, group_name, image_ref)

        if not docker_image:
            return log_and_return_error(
                "Could not find docker image for the execution",
                {
                    "extra": {
                        "git_root": git_root.state.nickname,
                        "group_name": group_name,
                        "uri": image_ref,
                    }
                },
            )

    return await _process_job_type(
        loaders=loaders,
        group_name=group_name,
        git_root=git_root,
        docker_image=docker_image,
        job_type=job_type,
        notification_id=notification_id,
        execution_id=execution_id,
        email_to=email_to,
        sbom_format=sbom_format,
        file_extension=file_extension,
        json_path=json_path,
    )
