import logging
from collections import defaultdict
from contextlib import suppress
from copy import deepcopy
from decimal import Decimal
from typing import Any

from aioextensions import collect
from packageurl import PackageURL
from univers.versions import InvalidVersion

from integrates.class_types.types import Item
from integrates.custom_exceptions import RepeatedToePackage, ToePackageAlreadyUpdated
from integrates.custom_utils import datetime as datetime_utils
from integrates.custom_utils.datetime import get_utc_now
from integrates.dataloaders import Dataloaders
from integrates.db_model.roots.types import GitRoot, RootDockerImage
from integrates.db_model.roots.update import update_root_docker_image_state
from integrates.db_model.toe_packages import add, update_package
from integrates.db_model.toe_packages.types import (
    Artifact,
    Digest,
    RootToePackagesRequest,
    ToePackage,
    ToePackageAdvisory,
    ToePackageCoordinates,
    ToePackageHealthMetadata,
)
from integrates.jobs_orchestration.model.types import SbomFormat
from integrates.mailer.common import send_mails_async
from integrates.server_async.report_sbom.version import (
    get_version_scheme,
    matches_constraint,
    validate_version,
)
from integrates.server_async.utils import map_sbom_format
from integrates.settings.logger import LOGGING

logging.config.dictConfig(LOGGING)
LOGGER = logging.getLogger(__name__)
PROCESS_SBOM_LOGGER = logging.getLogger("process_sbom")


def _format_health_metadata_artifact(artifact: Item | None) -> Artifact | None:
    if artifact is None:
        return None
    integrity = artifact.get("integrity")
    return Artifact(
        url=artifact["url"],
        integrity=(
            Digest(
                algorithm=integrity.get("algorithm"),
                value=integrity.get("value"),
            )
            if integrity
            else None
        ),
    )


def format_sbom_package(
    content: dict[str, Any],
    *,
    group_name: str,
    root_id: str,
    image_ref: str | None = None,
) -> ToePackage:
    advisories = [
        ToePackageAdvisory(
            cpes=advisory["cpes"],
            description=advisory["description"],
            epss=(Decimal(str(advisory["epss"])) if advisory.get("epss") else None),
            id=advisory["id"],
            namespace=advisory["namespace"],
            percentile=(
                Decimal(str(advisory["percentile"])) if advisory.get("percentile") else None
            ),
            severity=advisory["severity"],
            urls=advisory["urls"],
            version_constraint=advisory["version_constraint"],
        )
        for advisory in content.get("advisories", [])
    ]
    health_metadata = content.get("health_metadata")
    return ToePackage(
        group_name=group_name,
        root_id=root_id,
        name=content["name"],
        version=content["version"],
        type_=content["type"],
        language=content["language"],
        found_by=content.get("found_by"),
        modified_date=datetime_utils.get_utc_now(),
        be_present=True,
        id=content["id"],
        locations=[
            ToePackageCoordinates(
                path=x["path"],
                line=str(x.get("line")) if x.get("line") is not None else None,
                layer=x.get("layer"),
                image_ref=image_ref if x.get("layer") else None,
                scope=x.get("scope"),
                dependency_type=x.get("dependency_type"),
            )
            for x in content["locations"]
        ],
        licenses=content.get("licenses", []),
        url=content.get("url"),
        vulnerable=bool(advisories),
        health_metadata=(
            ToePackageHealthMetadata(
                artifact=_format_health_metadata_artifact(health_metadata.get("artifact")),
                authors=health_metadata.get("authors"),
                latest_version_created_at=health_metadata.get("latest_version_created_at"),
                latest_version=health_metadata.get("latest_version"),
            )
            if health_metadata
            else None
        ),
        vulnerability_ids=None,
        outdated=(
            content["version"] != health_metadata.get("latest_version") if health_metadata else None
        ),
        package_advisories=advisories,
        package_url=content["package_url"],
        platform=content.get("platform"),
    )


async def process_sbom_to_mail(
    *,
    loaders: Dataloaders,
    git_root: GitRoot,
    email_to: str,
    image_ref: str | None,
    sbom_format: SbomFormat,
    file_extension: str,
) -> tuple[bool, str]:
    root_nickname = git_root.state.nickname
    group_name = git_root.group_name
    resource = image_ref if image_ref else root_nickname.capitalize()

    await send_mails_async(
        loaders=loaders,
        email_to=[email_to],
        context={
            "resource": resource,
            "group_name": group_name.capitalize(),
            "sbom_format": map_sbom_format(sbom_format),
            "file_extension": file_extension.upper().lstrip("."),
        },
        subject="[SBOM] Report generated",
        template_name="send_sbom_file",
    )
    PROCESS_SBOM_LOGGER.info(
        "SBOM report of the resource sent correctly",
        extra={
            "extra": {
                "resource": resource,
                "email_to": email_to,
            },
        },
    )

    return True, f"SBOM report of the resource {resource} sent correctly to {email_to}"


async def update_docker_image_digest(sbom_json: Item, docker_image: RootDockerImage) -> None:
    version = sbom_json.get("sbom_details", {}).get("version")
    if version:
        await update_root_docker_image_state(
            root_id=docker_image.root_id,
            group_name=docker_image.group_name,
            uri=docker_image.uri,
            current_value=docker_image.state,
            state=docker_image.state._replace(
                digest=version,
                modified_date=get_utc_now(),
                modified_by="integrates@fluidattacks.com",
            ),
        )
    else:
        PROCESS_SBOM_LOGGER.warning(
            "Digest version not found in the report, skipping Docker image update",
            extra={"extra": {"image_uri": docker_image.uri}},
        )


async def process_packages(
    loaders: Dataloaders,
    group_name: str,
    git_root: GitRoot,
    sbom_json: Item,
    docker_image: RootDockerImage | None,
) -> tuple[bool, str]:
    image_ref = docker_image.uri if docker_image else None

    execution_packages = list(
        format_sbom_package(
            package,
            group_name=group_name,
            root_id=git_root.id,
            image_ref=image_ref,
        )
        for package in sbom_json["packages"]
    )
    db_root_packages = [
        x.node
        for x in (
            await loaders.root_toe_packages.load(
                RootToePackagesRequest(
                    group_name=group_name,
                    root_id=git_root.id,
                ),
            )
        ).edges
    ]

    (
        packages_to_add,
        packages_to_update_not_bp,
        packages_to_update_bp,
    ) = process_db_packages(execution_packages, db_root_packages, image_ref)
    with suppress(ToePackageAlreadyUpdated, RepeatedToePackage):
        await collect(
            (
                [add(toe_package=package) for package in packages_to_add]
                + [
                    update_package(
                        current_value=db_package,
                        package=db_package._replace(
                            be_present=False,
                            modified_date=datetime_utils.get_utc_now(),
                        ),
                    )
                    for db_package in packages_to_update_not_bp
                ]
                + [
                    update_package(
                        current_value=next(
                            x for x in db_root_packages if x.package_url == db_package.package_url
                        ),
                        package=db_package._replace(
                            modified_date=datetime_utils.get_utc_now(),
                        ),
                    )
                    # attributes have already been updated in
                    # process_db_packages
                    for db_package in packages_to_update_bp
                ]
            ),
            workers=32,
        )

    if docker_image:
        await update_docker_image_digest(sbom_json, docker_image)

    PROCESS_SBOM_LOGGER.info(
        "Packages successfully processed",
        extra={
            "extra": {
                "git_root": git_root.state.nickname,
                "group_name": group_name,
                "image_ref": image_ref,
            },
        },
    )

    return True, "SBOM execution has been processed"


def calculate_new_attrs(db_package: ToePackage, execution_package: ToePackage) -> dict[str, Any]:
    attrs_to_check = [
        "be_present",
        "health_metadata",
        "id",
        "licenses",
        "locations",
        "outdated",
        "package_advisories",
        "url",
        "vulnerable",
    ]
    return {
        attr: getattr(execution_package, attr)
        for attr in attrs_to_check
        if getattr(db_package, attr) != getattr(execution_package, attr)
    }


def create_package_dict(
    packages: list[ToePackage],
) -> dict[tuple[str, str, str, str], ToePackage]:
    """Creates a dictionary with a unique key for each package."""
    return {(pkg.group_name, pkg.root_id, pkg.name, pkg.version): pkg for pkg in packages}


def create_package_dict_no_version(
    packages: list[ToePackage],
) -> defaultdict[tuple[str, str, str], list[ToePackage]]:
    """Creates a dictionary with a unique key for each package."""
    items1: defaultdict[tuple[str, str, str], list[ToePackage]] = defaultdict(list[ToePackage])
    for pkg in packages:
        items1[(pkg.group_name, pkg.root_id, pkg.name)].append(pkg)
    return items1


def merge_locations(
    execution_pkg: ToePackage,
    db_pkg: ToePackage,
    image_ref: str | None,
) -> list[ToePackageCoordinates]:
    """Merges package locations based on `image_ref`."""
    applicable_locations = {loc for loc in db_pkg.locations if loc.image_ref == image_ref}
    merged_locations = set(execution_pkg.locations).intersection(applicable_locations)
    return list(
        {loc for loc in db_pkg.locations if loc.image_ref != image_ref}.union(merged_locations),
    )


def create_new_package(
    db_pkg: ToePackage,
    execution_pkg: ToePackage,
    merged_locations: list,
) -> ToePackage:
    """Creates a new package with updated attributes and merged locations."""
    return deepcopy(db_pkg)._replace(
        locations=merged_locations,
        licenses=execution_pkg.licenses,
        package_advisories=execution_pkg.package_advisories,
        url=execution_pkg.url,
    )


def _join_dicts(
    dict_a: defaultdict[tuple[str, str, str], list[ToePackage]],
    dict_b: defaultdict[tuple[str, str, str], list[ToePackage]],
) -> defaultdict[tuple[str, str, str], list[ToePackage]]:
    for key, values in dict_b.items():
        if key in dict_a:
            dict_a[key].extend(values)
        else:
            dict_a[key] = values
    return dict_a


def process_db_packages(
    execution_packages: list[ToePackage],
    db_root_packages: list[ToePackage],
    image_ref: str | None = None,
) -> tuple[list[ToePackage], list[ToePackage], list[ToePackage]]:
    execution_packages, db_root_packages = remove_duplicate_packages(
        db_root_packages=db_root_packages,
        execution_packages=execution_packages,
    )
    execution_dict = create_package_dict(execution_packages)
    db_dict = create_package_dict(db_root_packages)

    packages_to_add: list[ToePackage] = []
    packages_to_update_not_bp: list[ToePackage] = []
    packages_to_update_bp: list[ToePackage] = []

    for key, execution_pkg in execution_dict.items():
        db_pkg = db_dict.get(key)
        if not db_pkg:
            packages_to_add.append(execution_pkg)
            continue

        attrs_to_update = calculate_new_attrs(db_pkg, execution_pkg)
        db_pkg = db_pkg._replace(**attrs_to_update)

        merged_locations = merge_locations(execution_pkg, db_pkg, image_ref)
        new_package = create_new_package(db_pkg, execution_pkg, merged_locations)

        if attrs_to_update:
            packages_to_update_bp.append(new_package)
            db_dict.pop(key)

    for key, db_pkg in db_dict.items():
        if (key not in execution_dict and db_pkg.be_present) or not db_pkg.locations:
            merged_locations = [loc for loc in db_pkg.locations if loc.image_ref != image_ref]
            updated_package = db_pkg._replace(locations=merged_locations)

            if merged_locations and set(merged_locations) != set(db_pkg.locations):
                packages_to_update_bp.append(updated_package)
            elif not merged_locations:
                packages_to_update_not_bp.append(updated_package)

    return packages_to_add, packages_to_update_not_bp, packages_to_update_bp


def remove_duplicate_packages(
    *,
    db_root_packages: list[ToePackage],
    execution_packages: list[ToePackage],
) -> tuple[list[ToePackage], list[ToePackage]]:
    all_packages = _join_dicts(
        create_package_dict_no_version(execution_packages),
        create_package_dict_no_version(db_root_packages),
    )
    execution_dict = create_package_dict(execution_packages)
    db_dict = create_package_dict(db_root_packages)

    for key, packages in all_packages.items():
        constraint_packages = [
            item
            for item in packages
            if item.version and not item.version[0].isdigit() and validate_version(item.version)
        ]
        version_scheme = None
        try:
            version_scheme = get_version_scheme(packages[0])
        except ValueError as exc:
            LOGGER.exception(
                "SBOM invalid version scheme",
                extra={
                    "extra": {
                        "ex": exc,
                    },
                },
            )

        if not version_scheme:
            continue
        item: ToePackage
        try:
            sorted_packages = sorted(
                filter(
                    lambda x: x.version and x.version[0].isdigit() and validate_version(x.version),
                    packages,
                ),
                key=lambda x, vs=version_scheme: vs(x.version),  # type: ignore
            )

        except InvalidVersion as exc:
            LOGGER.exception(
                "SBOM invalid version",
                extra={
                    "extra": {
                        "ex": exc,
                    },
                },
            )
            continue

        for constraint_package in constraint_packages:
            if item := next(
                (
                    item
                    for item in sorted_packages
                    if matches_constraint(
                        version_scheme(item.version),
                        constraint_package.version,
                        PackageURL.from_string(constraint_package.package_url or "").type,
                    )
                ),
                None,  # type: ignore
            ):
                constraint_complete_key = (
                    *key,
                    constraint_package.version,
                )
                item_complete_key = (
                    *key,
                    item.version,
                )
                new_package = item._replace(
                    locations=[
                        *(item.locations),
                        *(constraint_package.locations),
                    ],
                )
                with suppress(KeyError):
                    execution_dict.pop(constraint_complete_key)

                execution_dict[item_complete_key] = new_package

    return (list(execution_dict.values()), list(db_dict.values()))
