from univers.versions import AlpineLinuxVersion, PypiVersion, SemverVersion

from integrates.server_async.report_sbom.version import (
    ApkVersionRange,
    PubVersionRange,
    _compare_single_constraint,
    convert_to_maven_range,
    get_version_scheme,
    matches_constraint,
    validate_version,
)
from integrates.testing.fakers import ToePackageCoordinatesFaker, ToePackageFaker


def test_validate_version() -> None:
    assert validate_version("1.0.0") is True
    assert validate_version("v1.0.0") is True
    assert validate_version("1.0.0-beta") is True
    assert validate_version("*") is False
    assert validate_version("abc") is False
    assert validate_version("1.0.0-beta.") is False
    assert validate_version("None") is False
    assert validate_version("50, 0, 0, 1") is False


def test_get_version_scheme() -> None:
    root_id = "123"
    package = ToePackageFaker(
        group_name="group1",
        root_id=root_id,
        name="aiohttp",
        version="2.1",
        outdated=False,
        licenses=["Apache-1.0"],
        locations=[ToePackageCoordinatesFaker(path="requirements.txt")],
        package_url="pkg:pypi/sample@1.0.0",
    )
    assert get_version_scheme(package) == PypiVersion
    package = ToePackageFaker(
        group_name="group1",
        root_id=root_id,
        name="aiohttp",
        version="2.1",
        outdated=False,
        licenses=["Apache-1.0"],
        locations=[ToePackageCoordinatesFaker(path="pom.xml")],
        package_url="pkg:apk/sample@1.0.0",
    )
    assert get_version_scheme(package) == AlpineLinuxVersion
    package = ToePackageFaker(
        group_name="group1",
        root_id=root_id,
        name="aiohttp",
        version="2.1",
        outdated=False,
        licenses=["Apache-1.0"],
        locations=[ToePackageCoordinatesFaker(path="requirements.txt")],
        package_url="pkg:unknown/sample@1.0.0",
    )
    assert get_version_scheme(package) is None


def test_apk_version_range() -> None:
    apk_range = ApkVersionRange.from_native(">=1.0.0")
    assert apk_range.constraints[0].comparator == ">="
    assert apk_range.constraints[0].version == AlpineLinuxVersion("1.0.0")


def test_pub_version_range() -> None:
    pub_range = PubVersionRange.from_native("^1.2.3")
    assert pub_range.constraints[0].comparator == ">="
    assert pub_range.constraints[0].version == SemverVersion("1.2.3")

    assert pub_range.constraints[1].comparator == "<"
    assert pub_range.constraints[1].version == SemverVersion("2.0.0")

    non_stable_pub_range = PubVersionRange.from_native("^1.2.3-beta")
    assert non_stable_pub_range.constraints[0].comparator == ">="
    assert non_stable_pub_range.constraints[0].version == SemverVersion("1.2.3-beta")

    assert non_stable_pub_range.constraints[1].comparator == "<"
    assert non_stable_pub_range.constraints[1].version == SemverVersion("2.0.0")


def test_convert_to_maven_range() -> None:
    constraints = [">=1.0.0", "<2.0.0"]
    result = convert_to_maven_range(constraints)
    assert result == "[1.0.0,),(,2.0.0)"

    constraints = ["<=1.0.0", "=2.0.0"]
    result = convert_to_maven_range(constraints)
    assert result == "(,1.0.0],[2.0.0]"


def test_compare_single_constraint() -> None:
    version = SemverVersion("1.5.0")

    assert _compare_single_constraint(version, ">=1.0.0", "pub") is True
    assert _compare_single_constraint(version, "<2.0.0", "pub") is True
    assert _compare_single_constraint(version, "=1.5.0", "pub") is True
    assert _compare_single_constraint(version, "<1.0.0", "pub") is False


def test_matches_constraint() -> None:
    version = SemverVersion("1.5.0")
    assert matches_constraint(version, ">=1.0.0 || <2.0.0", "pub") is True
    assert matches_constraint(version, "<1.0.0 || >2.0.0", "pub") is False
    assert matches_constraint(version, "", "pub") is True
