from datetime import datetime
from decimal import Decimal

from types_aiobotocore_s3.type_defs import HeadObjectOutputTypeDef

from integrates.dataloaders import Dataloaders, get_new_context
from integrates.db_model.groups.enums import GroupManaged
from integrates.db_model.roots.enums import RootStatus
from integrates.db_model.roots.types import RootDockerImageRequest
from integrates.db_model.toe_packages.types import (
    RootToePackagesRequest,
    ToePackage,
    ToePackageAdvisory,
    ToePackageCoordinates,
    ToePackageHealthMetadata,
)
from integrates.jobs_orchestration.model.types import SbomFormat, SbomJobType
from integrates.server_async import report_sbom
from integrates.server_async.report_sbom import (
    fetch_sbom_object,
    get_docker_image,
    get_git_root,
    get_sbom_object,
    process,
    process_sbom_execution,
    validate_group,
)
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    EMAIL_GENERIC,
    GitRootFaker,
    GitRootStateFaker,
    GroupFaker,
    GroupStateFaker,
    OrganizationFaker,
    RootDockerImageFaker,
    RootDockerImageStateFaker,
)
from integrates.testing.mocks import Mock, mocks
from integrates.testing.utils import freeze_time

SBOM_DUMMY_HEAD: HeadObjectOutputTypeDef = {
    "AcceptRanges": "",
    "ArchiveStatus": "ARCHIVE_ACCESS",
    "BucketKeyEnabled": False,
    "CacheControl": "",
    "ChecksumCRC32": "",
    "ChecksumCRC32C": "",
    "ChecksumSHA1": "",
    "ChecksumSHA256": "",
    "ContentDisposition": "",
    "ContentEncoding": "",
    "ContentLanguage": "",
    "ContentLength": 1234,
    "ContentType": "application/json",
    "DeleteMarker": False,
    "ETag": '"e1b849f963c6d8535a3b8a5288c3b0c1"',
    "Expiration": "",
    "Expires": datetime(2023, 12, 18),
    "LastModified": datetime(2024, 12, 18),
    "Metadata": {"custom-key": "custom-value"},
    "MissingMeta": 0,
    "ObjectLockLegalHoldStatus": "OFF",
    "ObjectLockMode": "COMPLIANCE",
    "ObjectLockRetainUntilDate": datetime(2023, 12, 18),
    "PartsCount": 0,
    "ReplicationStatus": "COMPLETE",
    "RequestCharged": "requester",
    "ResponseMetadata": {
        "RequestId": "",
        "HTTPStatusCode": 200,
        "HTTPHeaders": {},
        "RetryAttempts": 0,
    },
    "Restore": "",
    "ServerSideEncryption": "aws:kms",
    "SSECustomerAlgorithm": "",
    "SSECustomerKeyMD5": "",
    "SSEKMSKeyId": "",
    "StorageClass": "STANDARD",
    "VersionId": "",
    "WebsiteRedirectLocation": "",
}
BAD_SBOM_DUMMY_HEAD: HeadObjectOutputTypeDef = {
    "AcceptRanges": "",
    "ArchiveStatus": "ARCHIVE_ACCESS",
    "BucketKeyEnabled": False,
    "CacheControl": "",
    "ChecksumCRC32": "",
    "ChecksumCRC32C": "",
    "ChecksumSHA1": "",
    "ChecksumSHA256": "",
    "ContentDisposition": "",
    "ContentEncoding": "",
    "ContentLanguage": "",
    "ContentLength": 12582912,
    "ContentType": "application/json",
    "DeleteMarker": False,
    "ETag": '"e1b849f963c6d8535a3b8a5288c3b0c1"',
    "Expiration": "",
    "Expires": datetime(2023, 12, 18),
    "LastModified": datetime(2023, 12, 18),
    "Metadata": {"custom-key": "custom-value"},
    "MissingMeta": 0,
    "ObjectLockLegalHoldStatus": "OFF",
    "ObjectLockMode": "COMPLIANCE",
    "ObjectLockRetainUntilDate": datetime(2023, 12, 18),
    "PartsCount": 0,
    "ReplicationStatus": "COMPLETE",
    "RequestCharged": "requester",
    "ResponseMetadata": {
        "RequestId": "",
        "HTTPStatusCode": 200,
        "HTTPHeaders": {},
        "RetryAttempts": 0,
    },
    "Restore": "",
    "ServerSideEncryption": "aws:kms",
    "SSECustomerAlgorithm": "",
    "SSECustomerKeyMD5": "",
    "SSEKMSKeyId": "",
    "StorageClass": "STANDARD",
    "VersionId": "",
    "WebsiteRedirectLocation": "",
}


EXECUTION_ID_ROOT = "group1/root1/simple/sbom_group1_dummy_root_123"
EXCUTION_ID_IMAGE = "group1/root1/images/sha256123/sbom_group1_dummy_image_123"

FROZEN_TIME = "2024-11-14T00:00:00+00:00"


def load_test_file_as_bytes(file_path: str) -> bytes:
    with open(file_path, "rb") as f:
        return f.read()


TEST_IMAGE_BYTES = load_test_file_as_bytes(
    "integrates/back/integrates/server_async/report_sbom/test_data/image/test_image.json"
)
TEST_ROOT_BYTES = load_test_file_as_bytes(
    "integrates/back/integrates/server_async/report_sbom/test_data/root/test_process.json"
)
TEST_REQUEST_XML_BYTES = load_test_file_as_bytes(
    "integrates/back/integrates/server_async/report_sbom/test_data/root/test_request.xml"
)


@mocks(others=[Mock(report_sbom, "get_sbom_result_head", "async", None)])
async def test_fetch_sbom_object_empty() -> None:
    assert await fetch_sbom_object(EXECUTION_ID_ROOT, "json", SbomJobType.REQUEST) is None


@mocks(
    others=[
        Mock(report_sbom, "get_sbom_result_head", "async", SBOM_DUMMY_HEAD),
        Mock(report_sbom, "get_sbom_object", "async", TEST_IMAGE_BYTES),
    ]
)
async def test_fetch_sbom_object() -> None:
    assert (
        await fetch_sbom_object(EXCUTION_ID_IMAGE, "json", SbomJobType.REQUEST) is TEST_IMAGE_BYTES
    )


@mocks(others=[Mock(report_sbom, "get_sbom_result", "async", TEST_IMAGE_BYTES)])
async def test_get_sbom_object() -> None:
    assert (
        await get_sbom_object(BAD_SBOM_DUMMY_HEAD, SbomJobType.REQUEST, EXCUTION_ID_IMAGE, "json")
        is None
    )

    assert (
        await get_sbom_object(SBOM_DUMMY_HEAD, SbomJobType.REQUEST, EXCUTION_ID_IMAGE, "json")
        is TEST_IMAGE_BYTES
    )


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(id="org1", name="test_org"),
            ],
            groups=[
                GroupFaker(name="group1", organization_id="org1"),
            ],
            roots=[
                GitRootFaker(
                    group_name="group1",
                    id="root1",
                    state=GitRootStateFaker(
                        nickname="back",
                    ),
                ),
                GitRootFaker(
                    group_name="group1",
                    id="root2",
                    state=GitRootStateFaker(
                        nickname="back2",
                        status=RootStatus.INACTIVE,
                        url="https://gitlab.com/fluidattacks/back2",
                    ),
                ),
            ],
        ),
    )
)
async def test_get_git_root() -> None:
    loaders: Dataloaders = get_new_context()
    assert (
        await get_git_root(
            loaders,
            "group1",
            "back",
        )
        is not None
    )
    assert (
        await get_git_root(
            loaders,
            "group1",
            "NotExists",
        )
        is None
    )
    assert (
        await get_git_root(
            loaders,
            "group1",
            "back2",
        )
        is None
    )


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(id="org1", name="test_org"),
            ],
            groups=[
                GroupFaker(name="group1", organization_id="org1"),
            ],
            roots=[
                GitRootFaker(
                    group_name="group1",
                    id="root1",
                    state=GitRootStateFaker(
                        nickname="back",
                    ),
                ),
                GitRootFaker(
                    group_name="group1",
                    id="root2",
                    state=GitRootStateFaker(
                        nickname="back2",
                        status=RootStatus.INACTIVE,
                        url="https://gitlab.com/fluidattacks/back2",
                    ),
                ),
            ],
            docker_images=[
                RootDockerImageFaker(
                    uri="dummy_image",
                    group_name="group1",
                    root_id="root1",
                    state=RootDockerImageStateFaker(
                        digest="sha256123",
                    ),
                ),
            ],
        ),
    )
)
async def test_get_docker_image() -> None:
    loaders: Dataloaders = get_new_context()
    if git_root := await get_git_root(loaders, "group1", "back"):
        assert (
            await get_docker_image(loaders, git_root=git_root, group_name="group1", image_ref="")
            is None
        )
        assert await get_docker_image(
            loaders, git_root=git_root, group_name="group1", image_ref="dummy_image"
        )


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(id="org1", name="test_org"),
            ],
            groups=[
                GroupFaker(
                    name="group1",
                    organization_id="org1",
                    state=GroupStateFaker(has_essential=False),
                ),
                GroupFaker(
                    name="group2",
                    organization_id="org1",
                    state=GroupStateFaker(managed=GroupManaged.UNDER_REVIEW),
                ),
                GroupFaker(name="group3", organization_id="org1"),
            ],
        ),
    )
)
async def test_validate_group() -> None:
    loaders = get_new_context()

    test_cases = [
        ("group1", False),
        ("group2", False),
        ("group3", True),
        ("group4", False),
    ]

    for group_name, expected in test_cases:
        assert await validate_group(loaders, group_name) is expected


@freeze_time(FROZEN_TIME)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(id="org1", name="test_org"),
            ],
            groups=[
                GroupFaker(name="group1", organization_id="org1"),
                GroupFaker(
                    name="group2",
                    organization_id="org1",
                    state=GroupStateFaker(managed=GroupManaged.UNDER_REVIEW),
                ),
            ],
            roots=[
                GitRootFaker(
                    group_name="group1",
                    id="root1",
                    state=GitRootStateFaker(nickname="root1"),
                ),
            ],
            docker_images=[
                RootDockerImageFaker(
                    uri="dummy_image",
                    group_name="group1",
                    root_id="root1",
                    state=RootDockerImageStateFaker(
                        digest="sha256123",
                    ),
                ),
            ],
        ),
    ),
    others=[
        Mock(report_sbom, "get_sbom_result_head", "async", SBOM_DUMMY_HEAD),
        Mock(report_sbom, "fetch_sbom_object", "async", TEST_IMAGE_BYTES),
        Mock(process, "send_mails_async", "async", None),
    ],
)
async def test_process_sbom_execution_image() -> None:
    result, message = await process_sbom_execution(
        execution_id=EXCUTION_ID_IMAGE,
        job_type=SbomJobType.SCHEDULER,
        sbom_format=SbomFormat.FLUID_JSON,
        image_ref="dummy_image",
    )
    assert result is True
    assert message == "SBOM execution has been processed"

    loaders = get_new_context()
    docker_image = await loaders.docker_image.load(
        RootDockerImageRequest(
            root_id="root1",
            group_name="group1",
            uri_id="dummy_image",
        )
    )
    assert docker_image
    assert (
        docker_image.state.digest
        == "sha256:21dc6063fd678b478f57c0e13f47560d0ea4eeba26dfc947b2a4f81f686b9f45"
    )
    all_packages = await loaders.root_toe_packages.load(
        RootToePackagesRequest(
            group_name="group1",
            root_id="root1",
        )
    )

    assert len(all_packages.edges) == 2

    assert all_packages.edges[0].node == ToePackage(
        id="51a16b413b694c5879981235f46226e6b1ecfcafdd829a85e1287a077b28942b",
        be_present=True,
        group_name="group1",
        root_id="root1",
        name="alpine-baselayout",
        version="3.6.8-r0",
        type_="apk",
        language="unknown_language",
        modified_date=datetime.fromisoformat(FROZEN_TIME),
        locations=[
            ToePackageCoordinates(
                scope="PROD",
                path="/lib/apk/db/installed",
                dependency_type="UNKNOWN",
                line=None,
                layer="sha256:3e01818d79cd3467f1d60e54224f3f6ce5170eceb54e265d96bb82344b8c24e7",
                image_ref="dummy_image",
            )
        ],
        found_by="apk-db-selector",
        outdated=True,
        package_advisories=[],
        package_url="pkg:apk/alpine/alpine-baselayout@3.6.8-r0?arch=x86_64&distro=alpine-3"
        ".21.0&distro_id=alpine&distro_version_id=3.21.0",
        platform=None,
        licenses=["GPL-2.0-only"],
        url=None,
        vulnerable=False,
        has_related_vulnerabilities=None,
        vulnerability_ids=None,
        health_metadata=ToePackageHealthMetadata(
            artifact=None,
            authors="Natanael Copa <ncopa@alpinelinux.org>",
            latest_version_created_at=None,
            latest_version=None,
        ),
    )

    assert all_packages.edges[1].node == ToePackage(
        id="c8bb7af7af05d123784c69eb1cc899551110e41c4a876c055deafa2980acc332",
        be_present=True,
        group_name="group1",
        root_id="root1",
        name="alpine-baselayout-data",
        version="3.6.8-r0",
        type_="apk",
        language="unknown_language",
        modified_date=datetime.fromisoformat(FROZEN_TIME),
        locations=[
            ToePackageCoordinates(
                scope="PROD",
                path="/lib/apk/db/installed",
                dependency_type="UNKNOWN",
                line=None,
                layer="sha256:3e01818d79cd3467f1d60e54224f3f6ce5170eceb54e265d96bb82344b8c24e7",
                image_ref="dummy_image",
            )
        ],
        found_by="apk-db-selector",
        outdated=True,
        package_advisories=[],
        package_url="pkg:apk/alpine/alpine-baselayout-data@3.6.8-r0?arch=x86_64&distro=alpine-3.21.0&distro_id=alpine&distro_version_id=3.21.0&upstream=alpine-baselayout",
        platform=None,
        licenses=["GPL-2.0-only"],
        url=None,
        vulnerable=False,
        has_related_vulnerabilities=None,
        vulnerability_ids=None,
        health_metadata=ToePackageHealthMetadata(
            artifact=None,
            authors="Natanael Copa <ncopa@alpinelinux.org>",
            latest_version_created_at=None,
            latest_version=None,
        ),
    )

    result, message = await process_sbom_execution(
        execution_id=EXCUTION_ID_IMAGE,
        image_ref="dummy_image",
        job_type=SbomJobType.REQUEST,
        email_to=EMAIL_GENERIC,
        sbom_format=SbomFormat.CYCLONEDX_JSON,
    )
    assert result is True
    assert message == f"SBOM report of the resource dummy_image sent correctly to {EMAIL_GENERIC}"


@freeze_time(FROZEN_TIME)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(id="org1", name="test_org"),
            ],
            groups=[
                GroupFaker(name="group1", organization_id="org1"),
                GroupFaker(
                    name="group2",
                    organization_id="org1",
                    state=GroupStateFaker(managed=GroupManaged.UNDER_REVIEW),
                ),
            ],
            roots=[
                GitRootFaker(
                    group_name="group1",
                    id="root1",
                    state=GitRootStateFaker(nickname="root1"),
                ),
            ],
        ),
    ),
    others=[
        Mock(report_sbom, "get_sbom_result_head", "async", SBOM_DUMMY_HEAD),
        Mock(report_sbom, "fetch_sbom_object", "async", TEST_ROOT_BYTES),
        Mock(process, "send_mails_async", "async", None),
    ],
)
async def test_process_sbom_execution_root() -> None:
    result, message = await process_sbom_execution(
        execution_id=EXECUTION_ID_ROOT,
        job_type=SbomJobType.SCHEDULER,
        sbom_format=SbomFormat.FLUID_JSON,
    )
    assert result is True
    assert message == "SBOM execution has been processed"

    loaders = get_new_context()
    all_packages = await loaders.root_toe_packages.load(
        RootToePackagesRequest(
            group_name="group1",
            root_id="root1",
        )
    )

    assert len(all_packages.edges) == 2

    assert all_packages.edges[0].node == ToePackage(
        id="93e128331376a9a1385a54107694cafbca4b9a8cdc3425e8486d6b56d1d4c642",
        be_present=True,
        group_name="group1",
        root_id="root1",
        name="aiofiles",
        version=">=23.2.1",
        type_="python",
        language="python",
        modified_date=datetime.fromisoformat(FROZEN_TIME),
        locations=[
            ToePackageCoordinates(
                path="pyproject.toml",
                dependency_type="DIRECT",
                line="19",
                layer=None,
                scope="PROD",
                image_ref=None,
            )
        ],
        found_by="python-pyproject-toml-cataloger",
        outdated=True,
        package_advisories=[],
        package_url="pkg:pypi/aiofiles@%3E%3D23.2.1",
        platform="PIP",
        licenses=["Apache-2.0"],
        url=None,
        vulnerable=False,
        has_related_vulnerabilities=None,
        vulnerability_ids=None,
        health_metadata=ToePackageHealthMetadata(
            artifact=None,
            authors="Tin Tvrtkovic <tinchester@gmail.com>",
            latest_version_created_at="2024-06-24T11:02:01.529179Z",
            latest_version="24.1.0",
        ),
    )

    assert all_packages.edges[1].node == ToePackage(
        id="2c0122e0c9f5ce8a2d8ab4a591da53c890fae94fae374342a1bf4fea651f5ddd",
        be_present=True,
        group_name="group1",
        root_id="root1",
        name="python",
        version="^3.11",
        type_="python",
        language="python",
        modified_date=datetime.fromisoformat(FROZEN_TIME),
        locations=[
            ToePackageCoordinates(
                scope="PROD",
                path="pyproject.toml",
                dependency_type="DIRECT",
                line="18",
                layer=None,
                image_ref=None,
            )
        ],
        found_by="python-pyproject-toml-cataloger",
        outdated=None,
        package_advisories=[
            ToePackageAdvisory(
                cpes=["cpe:2.3:a:python:python:*:*:*:*:*:*:*:*"],
                description="Directory traversal vulnerability in the (1) extract and (2)"
                " extractall functions in the tarfile module in Python allows user-assisted"
                " remote attackers to overwrite arbitrary files via a .. (dot dot) sequence"
                " in filenames in a TAR archive, a related issue to CVE-2001-1267.",
                epss=Decimal("0.06504"),
                id="CVE-2007-4559",
                namespace="nvd:cpe",
                percentile=Decimal("0.93664"),
                severity="Medium",
                urls=[
                    "http://mail.python.org/pipermail/python-dev/2007-August/074290.html",
                    "http://mail.python.org/pipermail/python-dev/2007-August/074292.html",
                ],
                version_constraint="< 3.6.16 || >= 3.7.0, < 3.8.17 || >= 3.9.0, < 3.9.17 ||"
                " >= 3.10.0, < 3.10.12 || >= 3.11.0, < 3.11.4",
            ),
            ToePackageAdvisory(
                cpes=["cpe:2.3:a:python:python:*:*:*:*:*:*:*:*"],
                description="An issue in the urllib.parse component of Python before 3.11.4"
                " allows attackers to bypass blocklisting methods by supplying a URL that "
                "starts with blank characters.",
                epss=Decimal("0.00197"),
                id="CVE-2023-24329",
                namespace="nvd:cpe",
                percentile=Decimal("0.57359"),
                severity="High",
                urls=[
                    "https://github.com/python/cpython/issues/102153",
                    "https://github.com/python/cpython/pull/99421",
                ],
                version_constraint="< 3.7.17 || >= 3.8.0, < 3.8.17 || >= 3.9.0, < 3.9.17 ||"
                " >= 3.10.0, < 3.10.12 || >= 3.11.0, < 3.11.4",
            ),
        ],
        package_url="pkg:pypi/python@%5E3.11",
        platform="PIP",
        licenses=[],
        url=None,
        vulnerable=True,
        has_related_vulnerabilities=None,
        vulnerability_ids=None,
        health_metadata=None,
    )


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(id="org1", name="test_org"),
            ],
            groups=[
                GroupFaker(name="group1", organization_id="org1"),
                GroupFaker(
                    name="group2",
                    organization_id="org1",
                    state=GroupStateFaker(managed=GroupManaged.UNDER_REVIEW),
                ),
            ],
            roots=[
                GitRootFaker(
                    group_name="group1",
                    id="root1",
                    state=GitRootStateFaker(nickname="root1"),
                ),
            ],
        ),
    ),
    others=[
        Mock(report_sbom, "get_sbom_result_head", "async", SBOM_DUMMY_HEAD),
        Mock(report_sbom, "fetch_sbom_object", "async", TEST_ROOT_BYTES),
        Mock(process, "send_mails_async", "async", None),
    ],
)
async def test_edge_cases() -> None:
    result, message = await process_sbom_execution(
        execution_id=EXECUTION_ID_ROOT,
        job_type=SbomJobType.REQUEUE,
        email_to=EMAIL_GENERIC,
        sbom_format=SbomFormat.FLUID_JSON,
    )

    assert result is False
    assert message == "SBOM execution has not been processed, job type was not recognized"

    result, message = await process_sbom_execution(
        execution_id=EXECUTION_ID_ROOT,
        job_type=SbomJobType.REQUEST,
        email_to=None,
        sbom_format=SbomFormat.CYCLONEDX_XML,
    )

    assert result is False
    assert message == "Could not find email to send the SBOM report"

    result, message = await process_sbom_execution(
        execution_id=EXECUTION_ID_ROOT,
        job_type=SbomJobType.SCHEDULER,
        sbom_format=SbomFormat.CYCLONEDX_XML,
    )

    assert result is False
    assert message == "SBOM object is either None or not in the correct format for processing"

    result, message = await process_sbom_execution(
        execution_id="group2/root1/simple/sbom_group1_dummy_root_123",
        job_type=SbomJobType.SCHEDULER,
        sbom_format=SbomFormat.CYCLONEDX_XML,
    )

    assert result is False
    assert message == "Machine service is not included or is under review"

    result, message = await process_sbom_execution(
        execution_id="group1/root3/simple/sbom_group1_dummy_root_123",
        job_type=SbomJobType.SCHEDULER,
        sbom_format=SbomFormat.CYCLONEDX_XML,
    )

    assert result is False
    assert message == "Could not find root for the execution"

    result, message = await process_sbom_execution(
        execution_id="group1/root1/images/sha/sbom_group1_dummy_image_123",
        image_ref="no_image",
        job_type=SbomJobType.REQUEST,
        email_to=EMAIL_GENERIC,
        sbom_format=SbomFormat.CYCLONEDX_XML,
    )

    assert result is False
    assert message == "Could not find docker image for the execution"
