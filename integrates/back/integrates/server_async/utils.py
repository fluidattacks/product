import base64
import json
import tempfile
from contextlib import (
    suppress,
)
from json import (
    JSONDecodeError,
)
from typing import (
    Any,
)

import yaml
from aiohttp.client_exceptions import (
    ClientPayloadError,
)
from types_aiobotocore_s3.type_defs import HeadObjectOutputTypeDef
from yaml.reader import (
    ReaderError,
)

from integrates.class_types.types import (
    Item,
)
from integrates.decorators import (
    retry_on_exceptions,
)
from integrates.jobs_orchestration.model.types import SbomFormat
from integrates.s3.operations import (
    file_head,
)
from integrates.s3.resource import (
    get_client,
)

MACHINE_DATA_BUCKET = "machine.data"


async def get_config(
    execution_id: str,
    config_path: str | None = None,
) -> Item:
    # config_path is useful to test this flow locally
    s3_path = "configs"
    prefix = "integrates_get_config_"

    if config_path is not None:
        with open(config_path, "rb") as config_file:
            return yaml.safe_load(config_file)
    s3_client = await get_client()
    with tempfile.NamedTemporaryFile(prefix=prefix, delete=True) as temp:
        await s3_client.download_fileobj(
            MACHINE_DATA_BUCKET,
            f"{s3_path}/{execution_id}.yaml",
            temp,
        )

        temp.seek(0)
        return yaml.safe_load(temp)


@retry_on_exceptions(
    exceptions=(ClientPayloadError,),
    sleep_seconds=1,
)
async def get_results_log(
    execution_id: str,
    sarif_path: str | None = None,
) -> Item | None:
    s3_path = "results"
    file_extension = "sarif"
    prefix = "integrates_get_sarif_log_"
    # sarif_path is useful to test this flow locally
    if sarif_path is not None:
        with open(sarif_path, "rb") as sarif_file:
            return yaml.safe_load(sarif_file)
    s3_client = await get_client()
    with tempfile.NamedTemporaryFile(prefix=prefix, delete=True) as temp:
        await s3_client.download_fileobj(
            MACHINE_DATA_BUCKET,
            f"{s3_path}/{execution_id}.{file_extension}",
            temp,
        )

        temp.seek(0)
        try:
            return yaml.safe_load(temp.read().decode(encoding="utf-8").replace("x0081", ""))
        except ReaderError:
            return None


def decode_sqs_message(message: Any) -> str:
    with suppress(json.JSONDecodeError):
        return json.loads(message.body)["execution_id"]
    return json.loads(
        base64.b64decode(json.loads(base64.b64decode(message.body).decode())["body"]).decode(),
    )["id"]


def delete_message(queue: Any, message: Any) -> None:
    queue.delete_messages(
        Entries=[
            {
                "Id": message.message_id,
                "ReceiptHandle": message.receipt_handle,
            },
        ],
    )


@retry_on_exceptions(
    exceptions=(ClientPayloadError,),
    sleep_seconds=1,
)
async def get_sbom_result(
    execution_id: str,
    file_extension: str,
) -> bytes:
    s3_path = f"sbom_results/{execution_id}.{file_extension}"
    s3_client = await get_client()
    with tempfile.NamedTemporaryFile(delete=True) as temp:
        await s3_client.download_fileobj(
            MACHINE_DATA_BUCKET,
            s3_path,
            temp,
        )
        temp.seek(0)
        return temp.read()


@retry_on_exceptions(
    exceptions=(ClientPayloadError,),
    sleep_seconds=1,
)
async def get_sbom_result_head(
    execution_id: str,
    file_extension: str,
) -> HeadObjectOutputTypeDef | None:
    s3_path = f"sbom_results/{execution_id}.{file_extension}"
    return await file_head(s3_path, MACHINE_DATA_BUCKET)


def try_parse_json(file_content: bytes) -> Item | None:
    try:
        return json.loads(file_content.decode("utf-8"))
    except (ReaderError, JSONDecodeError):
        return None


def map_sbom_format(sbom_format: SbomFormat) -> str:
    format_mapping = {
        SbomFormat.CYCLONEDX_JSON: "CycloneDX",
        SbomFormat.CYCLONEDX_XML: "CycloneDX",
        SbomFormat.SPDX_JSON: "SPDX",
        SbomFormat.SPDX_XML: "SPDX",
        SbomFormat.FLUID_JSON: "Fluid Attacks JSON",
    }

    try:
        return format_mapping[sbom_format]
    except KeyError as ex:
        raise ValueError(f"Sbom format '{sbom_format}' not recognized") from ex
