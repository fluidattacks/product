from datetime import datetime
from typing import NamedTuple

from integrates.batch.constants import DEFAULT_ACTION_TIMEOUT
from integrates.batch.enums import Action, IntegratesBatchQueue, SkimsBatchQueue
from integrates.class_types.types import Item
from integrates.db_model.enums import TreatmentStatus
from integrates.db_model.groups.enums import GroupSubscriptionType
from integrates.db_model.vulnerabilities.enums import (
    VulnerabilityStateStatus,
    VulnerabilityVerificationStatus,
)


class DependentAction(NamedTuple):
    action_name: Action
    additional_info: Item
    queue: IntegratesBatchQueue | SkimsBatchQueue
    attempt_duration_seconds: int = DEFAULT_ACTION_TIMEOUT
    job_name: str | None = None


class BatchProcessing(NamedTuple):
    key: str
    action_name: Action
    entity: str
    subject: str
    time: datetime
    additional_info: Item
    queue: IntegratesBatchQueue | SkimsBatchQueue
    attempt_duration_seconds: int = DEFAULT_ACTION_TIMEOUT
    batch_job_id: str | None = None
    dependent_actions: list[DependentAction] | None = None
    retries: int = 0
    running: bool = False


class BatchProcessingToUpdate(NamedTuple):
    additional_info: Item | None = None
    attempt_duration_seconds: int | None = None
    batch_job_id: str | None = None
    dependent_actions: list[DependentAction] | None = None
    retries: int | None = None
    running: bool | None = None


class PutActionResult(NamedTuple):
    success: bool
    batch_job_id: str | None = None
    dynamo_pk: str | None = None


# Schemas for the Action's additional info field
CLONE_ROOTS_SCHEMA = {
    "additionalProperties": False,
    "properties": {
        "git_root_ids": {
            "items": {"type": "string"},
            "minItems": 1,
            "type": "array",
        },
        "should_queue_machine": {"type": "boolean"},
        "should_queue_sbom": {"type": "boolean"},
    },
    "required": ["git_root_ids"],
    "type": "object",
}

EXECUTE_MACHINE_SCHEMA = {
    "additionalProperties": False,
    "properties": {
        "network_name": {"type": "string"},
        "roots": {"items": {"type": "string"}, "minItems": 1, "type": "array"},
        "roots_config_files": {
            "items": {"type": "string"},
            "minItems": 1,
            "type": "array",
        },
    },
    "required": ["roots", "roots_config_files"],
    "type": "object",
}

EXECUTE_MACHINE_SAST_SCHEMA = {
    "additionalProperties": False,
    "properties": {
        "network_name": {"type": "string"},
        "roots": {"items": {"type": "string"}, "minItems": 1, "type": "array"},
        "roots_config_files": {
            "items": {"type": "string"},
            "minItems": 1,
            "type": "array",
        },
        "roots_sbom_config_files": {
            "items": {"type": "string"},
            "minItems": 1,
            "type": "array",
        },
    },
    "required": ["roots", "roots_config_files", "roots_sbom_config_files"],
    "type": "object",
}

GENERATE_IMAGE_SBOM_SCHEMA = {
    "additionalProperties": False,
    "properties": {
        "images": {
            "type": "array",
            "items": {
                "type": "object",
                "properties": {
                    "image_ref": {"type": "string"},
                    "root_nickname": {"type": "string"},
                },
                "required": [
                    "image_ref",
                    "root_nickname",
                ],
            },
            "minItems": 1,
        },
        "images_config_files": {
            "items": {"type": "string"},
            "minItems": 1,
            "type": "array",
        },
        "sbom_format": {
            "enum": [
                "fluid-json",
                "cyclonedx-json",
                "cyclonedx-xml",
                "spdx-json",
                "spdx-xml",
            ],
            "type": "string",
        },
        "job_type": {
            "enum": [
                "scheduler",
                "request",
                "requeue",
            ],
            "type": "string",
        },
        "notification_id_map": {
            "type": ["object", "null"],
            "patternProperties": {".*": {"type": "string"}},
            "additionalProperties": False,
        },
    },
    "required": [
        "images",
        "images_config_files",
        "sbom_format",
        "job_type",
    ],
    "type": "object",
}

GENERATE_ROOT_SBOM_SCHEMA = {
    "additionalProperties": False,
    "properties": {
        "root_nicknames": {
            "items": {"type": "string"},
            "minItems": 1,
            "type": "array",
        },
        "roots_config_files": {
            "items": {"type": "string"},
            "minItems": 1,
            "type": "array",
        },
        "sbom_format": {
            "enum": [
                "fluid-json",
                "cyclonedx-json",
                "cyclonedx-xml",
                "spdx-json",
                "spdx-xml",
            ],
            "type": "string",
        },
        "job_type": {
            "enum": [
                "scheduler",
                "request",
                "requeue",
            ],
            "type": "string",
        },
        "notification_id_map": {
            "type": ["object", "null"],
            "patternProperties": {".*": {"type": "string"}},
            "additionalProperties": False,
        },
    },
    "required": [
        "root_nicknames",
        "roots_config_files",
        "sbom_format",
        "job_type",
    ],
    "type": "object",
}

HANDLE_FINDING_POLICY_SCHEMA = {
    "additionalProperties": False,
    "properties": {
        "organization_name": {"type": "string"},
    },
    "required": ["organization_name"],
    "type": "object",
}

BULK_JIRA_VULNERABILITIES_SCHEMA = {
    "additionalProperties": False,
    "properties": {
        "jira_jwt": {"type": "string"},
        "base_url": {"type": "string"},
    },
    "required": [
        "jira_jwt",
        "base_url",
    ],
    "type": "object",
}

MOVE_ROOT_SCHEMA = {
    "additionalProperties": False,
    "properties": {
        "source_group_name": {"type": "string"},
        "source_root_id": {"type": "string"},
        "target_group_name": {"type": "string"},
        "target_root_id": {"type": "string"},
    },
    "required": [
        "source_group_name",
        "source_root_id",
        "target_group_name",
        "target_root_id",
    ],
    "type": "object",
}

MOVE_ENVIRONMENT_SCHEMA = {
    "additionalProperties": False,
    "properties": {
        "source_group_name": {"type": "string"},
        "source_root_id": {"type": "string"},
        "source_env_id": {"type": "string"},
        "target_group_name": {"type": "string"},
        "target_root_id": {"type": "string"},
    },
    "required": [
        "source_group_name",
        "source_root_id",
        "source_env_id",
        "target_group_name",
        "target_root_id",
    ],
    "type": "object",
}

REMOVE_GROUP_RESOURCES_SCHEMA = {
    "additionalProperties": False,
    "properties": {
        "has_advanced": {"type": "boolean"},
        "subscription": {
            "enum": [i.value for i in GroupSubscriptionType],
            "type": "string",
        },
        "unique_authors": {"type": "number"},
        "validate_pending_actions": {"type": "boolean"},
        "emails": {
            "items": {"type": "string"},
            "type": "array",
        },
    },
    "required": ["emails", "has_advanced", "subscription", "validate_pending_actions"],
    "type": "object",
}

ONLY_ROOT_IDS_SCHEMA = {
    "additionalProperties": False,
    "properties": {
        "root_ids": {
            "items": {"type": "string"},
            "minItems": 1,
            "type": "array",
        },
    },
    "required": ["root_ids"],
    "type": "object",
}

REPORT_SCHEMA = {
    "additionalProperties": False,
    "properties": {
        "notification_id": {"type": "string"},
        "report_type": {
            "enum": ["CERT", "DATA", "PDF", "XLS", "TOE_INPUTS", "TOE_LINES"],
            "type": "string",
        },
        "treatments": {
            "items": {
                "enum": [i.value for i in TreatmentStatus],
                "type": "string",
            },
            "minItems": 0,
            "type": "array",
        },
        "states": {
            "items": {
                "enum": [i.value for i in VulnerabilityStateStatus],
                "type": "string",
            },
            "minItems": 0,
            "type": "array",
        },
        "verifications": {
            "items": {
                "enum": [i.value for i in VulnerabilityVerificationStatus],
                "type": "string",
            },
            "minItems": 0,
            "type": "array",
        },
        "closing_date": {
            "format": "date-time",
            "type": ["null", "string"],
        },
        "start_closing_date": {
            "format": "date-time",
            "type": ["null", "string"],
        },
        "finding_title": {"type": "string"},
        "age": {"type": ["null", "number"]},
        "min_severity": {"type": ["null", "number"]},
        "max_severity": {"type": ["null", "number"]},
        "last_report": {"type": ["null", "number"]},
        "min_release_date": {
            "format": "date-time",
            "type": ["null", "string"],
        },
        "max_release_date": {
            "format": "date-time",
            "type": ["null", "string"],
        },
        "location": {"type": "string"},
    },
    "required": ["report_type"],
    "type": "object",
}

SEND_EXPORTED_FILE_SCHEMA = {
    "additionalProperties": False,
    "properties": {"notification_id": {"type": "string"}},
    "required": [],
    "type": "object",
}

UPDATE_ORGANIZATION_PRIORITY_POLICIES_SCHEMA = {
    "additionalProperties": False,
    "properties": {
        "priority_policies": {
            "items": {
                "type": "object",
                "properties": {
                    "policy": {"type": "string"},
                    "value": {"type": "number"},
                },
                "required": ["policy", "value"],
            },
            "minItems": 1,
            "type": "array",
        },
        "policy_to_remove": {"type": "string"},
    },
    "required": [],
    "type": "object",
}

UPDATE_ORGANIZATION_REPOSITORIES_SCHEMA = {
    "additionalProperties": False,
    "properties": {
        "credentials_id": {"type": "string"},
    },
    "required": ["credentials_id"],
    "type": "object",
}

UPDATE_TOE_INPUTS_SCHEMA = {
    "additionalProperties": False,
    "properties": {
        "group_name": {"type": "string"},
        "old_url": {"type": "string"},
        "root_id": {"type": "string"},
    },
    "required": ["group_name", "old_url", "root_id"],
    "type": "object",
}

REFRESH_REPOSITORIES_SCHEMA = {
    "additionalProperties": False,
    "properties": {},
    "required": [],
    "type": "object",
}

ACTIONS_SCHEMA = {
    Action.CLONE_ROOTS: CLONE_ROOTS_SCHEMA,
    Action.DEACTIVATE_ROOT: ONLY_ROOT_IDS_SCHEMA,
    Action.EXECUTE_MACHINE: EXECUTE_MACHINE_SCHEMA,
    Action.EXECUTE_MACHINE_SAST: EXECUTE_MACHINE_SAST_SCHEMA,
    Action.GENERATE_ROOT_SBOM: GENERATE_ROOT_SBOM_SCHEMA,
    Action.GENERATE_IMAGE_SBOM: GENERATE_IMAGE_SBOM_SCHEMA,
    Action.HANDLE_FINDING_POLICY: HANDLE_FINDING_POLICY_SCHEMA,
    Action.BULK_JIRA_VULNERABILITIES: BULK_JIRA_VULNERABILITIES_SCHEMA,
    Action.MOVE_ROOT: MOVE_ROOT_SCHEMA,
    Action.MOVE_ENVIRONMENT: MOVE_ENVIRONMENT_SCHEMA,
    Action.REBASE: ONLY_ROOT_IDS_SCHEMA,
    Action.REFRESH_REPOSITORIES: REFRESH_REPOSITORIES_SCHEMA,
    Action.REFRESH_TOE_INPUTS: ONLY_ROOT_IDS_SCHEMA,
    Action.REFRESH_TOE_LINES: ONLY_ROOT_IDS_SCHEMA,
    Action.REFRESH_TOE_PORTS: ONLY_ROOT_IDS_SCHEMA,
    Action.REMOVE_GROUP_RESOURCES: REMOVE_GROUP_RESOURCES_SCHEMA,
    Action.REPORT: REPORT_SCHEMA,
    Action.SEND_EXPORTED_FILE: SEND_EXPORTED_FILE_SCHEMA,
    Action.UPDATE_GROUP_LANGUAGES: ONLY_ROOT_IDS_SCHEMA,
    Action.UPDATE_ORGANIZATION_PRIORITY_POLICIES: (UPDATE_ORGANIZATION_PRIORITY_POLICIES_SCHEMA),
    Action.UPDATE_ORGANIZATION_REPOSITORIES: (UPDATE_ORGANIZATION_REPOSITORIES_SCHEMA),
    Action.UPDATE_TOE_INPUTS: UPDATE_TOE_INPUTS_SCHEMA,
    Action.UPDATE_VULNS_AUTHOR: ONLY_ROOT_IDS_SCHEMA,
}
