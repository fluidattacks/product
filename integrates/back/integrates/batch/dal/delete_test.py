from integrates.batch.dal.delete import delete_action
from integrates.batch.dal.get import get_action
from integrates.batch.enums import Action
from integrates.dynamodb import (
    operations as dynamodb_ops,
)
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    BatchProcessingFaker,
    GroupFaker,
    OrganizationFaker,
    StakeholderFaker,
)
from integrates.testing.mocks import Mock, mocks

EMAIL_TEST = "admin@fluidattacks.com"
GROUP_NAME = "group1"


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(id="org1"),
            ],
            stakeholders=[
                StakeholderFaker(email=EMAIL_TEST, role="admin"),
            ],
            groups=[
                GroupFaker(name=GROUP_NAME, organization_id="org1"),
            ],
            actions=[
                BatchProcessingFaker(
                    action_name=Action.REPORT,
                    subject=EMAIL_TEST,
                    entity=GROUP_NAME,
                    additional_info={
                        "report_type": "TOE_LINES",
                    },
                    key="key1",
                ),
            ],
        ),
    ),
    others=[Mock(dynamodb_ops, "delete_item", "async", None)],
)
async def test_delete_action() -> None:
    await delete_action(action_key="key1")

    action = await get_action(action_key="key1")
    assert action is None
