import logging
import logging.config
import math

from aioextensions import (
    collect,
)
from botocore.exceptions import (
    ClientError,
)
from more_itertools import (
    divide,
)
from more_itertools.recipes import (
    flatten,
)

from integrates.batch.dal.delete import (
    delete_action,
)
from integrates.batch.dal.get import (
    get_action,
    get_actions,
    get_actions_by_name,
)
from integrates.batch.dal.put import (
    put_action,
    put_action_to_batch,
    put_action_to_dynamodb,
)
from integrates.batch.dal.update import (
    update_action_to_dynamodb,
)
from integrates.batch.resources import (
    get_batch_client,
)
from integrates.class_types.types import (
    Item,
)
from integrates.context import (
    FI_ENVIRONMENT,
)
from integrates.dynamodb import (
    operations as dynamodb_ops,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)

LOGGER = logging.getLogger(__name__)

__all__ = [
    "cancel_batch_job",
    "delete_action",
    "describe_jobs",
    "dynamodb_ops",
    "get_action",
    "get_actions",
    "get_actions_by_name",
    "put_action",
    "put_action_to_batch",
    "put_action_to_dynamodb",
    "terminate_batch_job",
    "update_action_to_dynamodb",
]


async def list_jobs_paginated(
    job_queue: str, next_token: str | None, filters: list[dict] | None
) -> Item:
    batch_client = await get_batch_client()

    return await batch_client.list_jobs(
        jobQueue=job_queue,
        maxResults=100,
        nextToken=next_token,
        filters=filters or [],
    )


async def describe_jobs(*job_ids: str) -> tuple[Item, ...]:
    if not job_ids:
        return ()

    batch_client = await get_batch_client()

    return tuple(
        flatten(
            response["jobs"]
            for response in await collect(
                tuple(
                    batch_client.describe_jobs(jobs=list(_set_jobs))
                    for _set_jobs in divide(math.ceil(len(job_ids) / 100), job_ids)
                ),
            )
        ),
    )


async def cancel_batch_job(*, job_id: str, reason: str = "not required") -> None:
    if FI_ENVIRONMENT != "production":
        return

    batch_client = await get_batch_client()

    try:
        await batch_client.cancel_job(jobId=job_id, reason=reason)
    except ClientError as exc:
        LOGGER.exception(
            exc,
            extra={"extra": None},
        )


async def terminate_batch_job(*, job_id: str, reason: str = "not required") -> None:
    if FI_ENVIRONMENT != "production":
        return

    batch_client = await get_batch_client()

    try:
        await batch_client.terminate_job(jobId=job_id, reason=reason)
    except ClientError as exc:
        LOGGER.exception(
            exc,
            extra={"extra": None},
        )
