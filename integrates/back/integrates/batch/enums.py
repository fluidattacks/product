from enum import (
    Enum,
)


class Product(str, Enum):
    INTEGRATES: str = "integrates"
    SKIMS: str = "skims"


class Action(str, Enum):
    CLONE_ROOTS = "clone_roots"
    DEACTIVATE_ROOT = "deactivate_root"
    EXECUTE_MACHINE = "execute_machine"
    EXECUTE_MACHINE_SAST = "execute_machine_sast"
    GENERATE_ROOT_SBOM = "generate_root_sbom"
    GENERATE_IMAGE_SBOM = "generate_image_sbom"
    HANDLE_FINDING_POLICY = "handle_finding_policy"
    BULK_JIRA_VULNERABILITIES = "bulk_jira_vulnerabilities"
    MOVE_ROOT = "move_root"
    MOVE_ENVIRONMENT = "move_environment"
    REBASE = "rebase"
    REFRESH_REPOSITORIES = "refresh_repositories"
    REFRESH_TOE_INPUTS = "refresh_toe_inputs"
    REFRESH_TOE_LINES = "refresh_toe_lines"
    REFRESH_TOE_PORTS = "refresh_toe_ports"
    REMOVE_GROUP_RESOURCES = "remove_group_resources"
    REPORT = "report"
    SEND_EXPORTED_FILE = "send_exported_file"
    UPDATE_GROUP_LANGUAGES = "update_group_languages"
    UPDATE_ORGANIZATION_PRIORITY_POLICIES = "update_organization_priority_policies"
    UPDATE_ORGANIZATION_REPOSITORIES = "update_organization_repositories"
    UPDATE_TOE_INPUTS = "update_toe_inputs"
    UPDATE_VULNS_AUTHOR = "update_vulns_author"


class SkimsBatchQueue(str, Enum):
    SMALL: str = "skims"
    LARGE: str = "skims_large"


class IntegratesBatchQueue(str, Enum):
    SMALL: str = "integrates"
    LARGE: str = "integrates_large"
