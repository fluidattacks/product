import json
from collections import (
    defaultdict,
)
from collections.abc import (
    Awaitable,
    Callable,
)
from contextlib import (
    AsyncExitStack,
)
from typing import (
    Any,
)

import aioboto3
import yaml

from integrates.class_types.types import (
    Item,
)
from integrates.context import (
    FI_ASYNC_PROCESSING_DB_MODEL_PATH,
    FI_AWS_REGION_NAME,
    FI_INTEGRATES_QUEUE_SIZES,
)
from integrates.dynamodb.tables import (
    load_tables,
)

CPU_SIZES = defaultdict(lambda: 1)
JOB_DEFINITIONS = defaultdict(lambda: "prod_integrates")
MEMORY_SIZES = defaultdict(lambda: 7600)

SESSION = aioboto3.Session()
NoArgsAsyncCallable = Callable[[], Awaitable[None]]
ClientAsyncCallable = Callable[[], Awaitable[Any]]
BatchContextTuple = tuple[NoArgsAsyncCallable, NoArgsAsyncCallable, ClientAsyncCallable]


def create_batch_context() -> BatchContextTuple:
    context_stack = None
    client_batch = None

    async def client_startup() -> None:
        nonlocal context_stack, client_batch

        context_stack = AsyncExitStack()
        client_batch = await context_stack.enter_async_context(
            SESSION.client(
                region_name=FI_AWS_REGION_NAME,
                service_name="batch",
            ),
        )

    async def client_shutdown() -> None:
        if context_stack:
            await context_stack.aclose()

    async def get_client() -> Any:
        if client_batch is None:
            await client_startup()

        return client_batch

    return client_startup, client_shutdown, get_client


(
    batch_client_startup,
    batch_client_shutdown,
    get_batch_client,
) = create_batch_context()

with open(FI_INTEGRATES_QUEUE_SIZES, encoding="utf-8") as handler:
    QUEUE_SIZES: Item = yaml.safe_load(handler.read())
    for key, value in QUEUE_SIZES.items():
        CPU_SIZES[key] = value.get("cpu", 1)
        JOB_DEFINITIONS[key] = value.get("job_definition", "prod_integrates")
        MEMORY_SIZES[key] = value.get("memory", 7600)

with open(FI_ASYNC_PROCESSING_DB_MODEL_PATH, encoding="utf-8") as file:
    TABLE = load_tables(json.load(file))[0]
