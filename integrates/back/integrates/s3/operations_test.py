import base64
import json
import os
from contextlib import suppress
from tempfile import mkdtemp

from starlette.datastructures import UploadFile

import integrates.s3.operations
from integrates.custom_exceptions import ErrorUploadingFileS3
from integrates.s3.operations import (
    download_file,
    file_exists,
    file_head,
    list_files,
    remove_file,
    sign_upload_url,
    upload_file,
    upload_memory_file,
)
from integrates.testing.aws import IntegratesAws, IntegratesS3
from integrates.testing.mocks import Mock, mocks
from integrates.testing.utils import freeze_time, get_file_abs_path, parametrize, raises


@parametrize(
    args=["bucket", "filename"],
    cases=[["integrates.dev", "test.txt"]],
)
@mocks(others=[Mock(integrates.s3.operations, "FI_AWS_S3_PATH_PREFIX", "function", "")])
async def test_upload_file(bucket: str, filename: str) -> None:
    # Act
    await upload_file(bucket=bucket, object_key=filename, file_path=get_file_abs_path(filename))

    # Assert
    result = await file_exists(filename, bucket)
    assert result


@parametrize(
    args=["bucket", "filename"],
    cases=[["integrates.dev", "new.txt"]],
)
@mocks(
    others=[Mock(integrates.s3.operations, "FI_AWS_S3_PATH_PREFIX", "function", "")],
)
async def test_upload_memory_file(bucket: str, filename: str) -> None:
    # Act
    with open(get_file_abs_path(filename), "rb") as file:
        fileobj = UploadFile(file)
        await upload_memory_file(file_object=fileobj, file_name=filename, bucket=bucket)

    # Assert
    assert await file_exists(filename, bucket)


@parametrize(
    args=["bucket", "filename"],
    cases=[["integrates.dev", "new.txt"]],
)
@mocks()
async def test_upload_memory_file_fail(bucket: str, filename: str) -> None:
    # Act
    with raises(ErrorUploadingFileS3):
        with open(get_file_abs_path(filename), "rb") as file:
            await upload_memory_file(file_object=file, file_name=filename, bucket=bucket)


@parametrize(
    args=["bucket", "filename"],
    cases=[["integrates.dev", "readme.md"]],
)
@mocks(
    aws=IntegratesAws(s3=IntegratesS3(autoload=True)),
    others=[Mock(integrates.s3.operations, "FI_AWS_S3_PATH_PREFIX", "function", "")],
)
async def test_download_file(bucket: str, filename: str) -> None:
    tmp = mkdtemp()

    # File exists in S3, but not in local
    assert await file_exists(filename, bucket)
    assert len(os.listdir(tmp)) == 0

    # Act
    # KeyError is raised due to moto is returning an empty head
    with suppress(KeyError):
        await download_file(bucket=bucket, file_name=filename, file_path=f"{tmp}/{filename}")

    # File exists in S3 and locally
    assert await file_exists(filename, bucket)
    assert len(os.listdir(tmp)) == 1


@parametrize(
    args=["bucket", "expected_output"],
    cases=[["integrates.dev", ["file1.txt", "file2.txt", "utils/file3.txt"]]],
)
@mocks(
    aws=IntegratesAws(s3=IntegratesS3(autoload=True)),
    others=[Mock(integrates.s3.operations, "FI_AWS_S3_PATH_PREFIX", "function", "")],
)
async def test_list_files(bucket: str, expected_output: str) -> None:
    # Act
    files = await list_files(bucket=bucket, name="")

    assert len(files) == len(expected_output)
    assert all(file in files for file in expected_output)


@parametrize(
    args=["bucket", "filename"],
    cases=[["integrates.dev", "to_remove.txt"]],
)
@mocks(
    aws=IntegratesAws(s3=IntegratesS3(autoload=True)),
    others=[Mock(integrates.s3.operations, "FI_AWS_S3_PATH_PREFIX", "function", "")],
)
async def test_remove_file(bucket: str, filename: str) -> None:
    assert await file_exists(filename, bucket)

    # Act
    await remove_file(bucket=bucket, name=filename)

    # Assert
    assert not await file_exists(filename, bucket)


@freeze_time("2024-12-01 15:00:00")
@parametrize(
    args=["bucket", "filename"],
    cases=[["integrates.dev", "new.txt"]],
)
@mocks(
    aws=IntegratesAws(s3=IntegratesS3(autoload=True)),
    others=[Mock(integrates.s3.operations, "FI_AWS_S3_PATH_PREFIX", "function", "")],
)
async def test_sign_url(bucket: str, filename: str) -> None:
    assert await file_exists(filename, bucket)

    # Act
    metadata = await sign_upload_url(bucket=bucket, file_name=filename, expire_seconds=60)

    # Assert
    decoded_policy = base64.b64decode(metadata["fields"]["policy"]).decode("utf-8")
    json_policy = json.loads(decoded_policy)
    assert json_policy["expiration"] == "2024-12-01T15:01:00Z"
    assert json_policy["conditions"][0]["acl"] == "private"
    assert json_policy["conditions"][1]["bucket"] == bucket


@parametrize(
    args=["bucket", "filename"],
    cases=[["integrates.dev", "new.txt"]],
)
@mocks(
    aws=IntegratesAws(s3=IntegratesS3(autoload=True)),
    others=[Mock(integrates.s3.operations, "FI_AWS_S3_PATH_PREFIX", "function", "")],
)
async def test_file_head(bucket: str, filename: str) -> None:
    assert await file_exists(filename, bucket)

    # Act
    head = await file_head(bucket=bucket, object_key=filename)

    # Assert
    assert head
    assert "ResponseMetadata" in head
    assert head["ResponseMetadata"]["HTTPStatusCode"] == 200
