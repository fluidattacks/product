from integrates.db_model import (
    mailmap,
)
from integrates.db_model.mailmap.types import (
    MailmapEntriesConnection,
    MailmapEntry,
    MailmapEntryWithSubentries,
    MailmapRecord,
    MailmapSubentry,
)


async def get_mailmap_entries_with_subentries(
    organization_id: str,
) -> list[MailmapEntryWithSubentries]:
    entries_with_subentries = await mailmap.get_mailmap_entries_with_subentries(
        organization_id=organization_id,
    )
    return entries_with_subentries


async def get_mailmap_records(
    organization_id: str,
) -> list[MailmapRecord]:
    records = await mailmap.get_mailmap_records(
        organization_id=organization_id,
    )
    return records


async def get_mailmap_entries(
    after: str,
    first: int,
    organization_id: str,
) -> MailmapEntriesConnection:
    connection = await mailmap.get_mailmap_entries(
        after=after,
        first=first,
        organization_id=organization_id,
    )
    return connection


async def get_mailmap_entry_with_subentries(
    entry_email: str,
    organization_id: str,
) -> MailmapEntryWithSubentries:
    entry_with_subentries = await mailmap.get_mailmap_entry_with_subentries(
        entry_email=entry_email,
        organization_id=organization_id,
    )
    return entry_with_subentries


async def get_mailmap_entry(
    entry_email: str,
    organization_id: str,
) -> MailmapEntry:
    entry = await mailmap.get_mailmap_entry(
        entry_email=entry_email,
        organization_id=organization_id,
    )
    return entry


async def get_mailmap_subentry(
    entry_email: str,
    subentry_email: str,
    subentry_name: str,
    organization_id: str,
) -> MailmapSubentry:
    subentry = await mailmap.get_mailmap_subentry(
        entry_email=entry_email,
        subentry_email=subentry_email,
        subentry_name=subentry_name,
        organization_id=organization_id,
    )
    return subentry


async def get_mailmap_entry_subentries(
    entry_email: str,
    organization_id: str,
) -> list[MailmapSubentry]:
    entry_subentries = await mailmap.get_mailmap_entry_subentries(
        entry_email=entry_email,
        organization_id=organization_id,
    )
    return entry_subentries
