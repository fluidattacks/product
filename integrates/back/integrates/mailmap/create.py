import logging

from pandas import DataFrame, Series
from starlette.datastructures import UploadFile

from integrates.custom_exceptions import ConflictingMailmapMappingError, MailmapOrganizationNotFound
from integrates.custom_utils.validations_deco import (
    validate_fields_deco,
    validate_length_field_dict_deco,
)
from integrates.db_model import mailmap
from integrates.db_model.mailmap.types import (
    MailmapEntry,
    MailmapEntryWithSubentries,
    MailmapSubentry,
)
from integrates.mailmap.validations import validate_mailmap_email_deco
from integrates.settings import LOGGING

logging.config.dictConfig(LOGGING)
LOGGER_CONSOLE = logging.getLogger("console")


@validate_mailmap_email_deco("entry.mailmap_entry_email")  # type: ignore[misc]
@validate_length_field_dict_deco(
    "entry", ("mailmap_entry_name", "mailmap_entry_email"), max_length=200, min_length=1
)
@validate_fields_deco(["entry.mailmap_entry_name"])
async def create_mailmap_entry(
    entry: MailmapEntry, organization_id: str
) -> MailmapEntryWithSubentries:
    entry_with_subentries = await mailmap.create_mailmap_entry(
        entry=entry,
        organization_id=organization_id,
    )
    return entry_with_subentries


@validate_mailmap_email_deco("subentry.mailmap_subentry_email")  # type: ignore[misc]
@validate_length_field_dict_deco(
    "subentry", ("mailmap_subentry_name", "mailmap_subentry_email"), max_length=200, min_length=1
)
@validate_fields_deco(["subentry.mailmap_subentry_name"])
async def create_mailmap_subentry(
    subentry: MailmapSubentry,
    entry_email: str,
    organization_id: str,
) -> None:
    await mailmap.create_mailmap_subentry(
        subentry=subentry,
        entry_email=entry_email,
        organization_id=organization_id,
    )


async def create_mailmap_entry_with_subentries(
    entry_with_subentries: MailmapEntryWithSubentries,
    organization_id: str,
) -> None:
    await mailmap.create_mailmap_entry_with_subentries(
        entry_with_subentries=entry_with_subentries,
        organization_id=organization_id,
    )


async def _get_lines_from_file(file: UploadFile) -> list[str]:
    content = await file.read()
    text = content.decode("utf-8")
    raw_lines = text.splitlines()
    await file.close()
    return raw_lines


def _build_mailmap_dataframe(raw_lines: list[str]) -> DataFrame:
    raw_mailmap_df = DataFrame({"raw_lines": raw_lines})

    pattern1 = r"(?P<entry_name>.*?) <(?P<entry_email>.*?)>"
    pattern2 = r"(?P<subentry_name>.*?) <(?P<subentry_email>.*?)>"
    pattern = f"{pattern1} {pattern2}"

    raw_lines_series = raw_mailmap_df["raw_lines"]  # type: ignore[misc]
    extracted_df = raw_lines_series.str.extract(pattern)  # type: ignore[misc]

    filtered_mailmap_df = extracted_df.dropna(axis=0, how="all")

    mailmap_df = filtered_mailmap_df.sort_values(
        by=["entry_email", "entry_name", "subentry_email", "subentry_name"],
    )

    return mailmap_df


def _find_mailmap_mapping_issues(mailmap_df: DataFrame) -> list[str]:
    # find rows that associate more than one entry_name to one entry_email
    unique_entry_names: Series = (  # type: ignore[type-arg]
        mailmap_df.groupby("entry_email")[  # type: ignore[misc]
            "entry_name"
        ].nunique()
    )

    entries_with_multiple_names = unique_entry_names[  # type: ignore[misc]
        unique_entry_names > 1  # type: ignore[misc]
    ].index.tolist()

    result_df = mailmap_df[
        mailmap_df["entry_email"].isin(  # type: ignore[misc]
            entries_with_multiple_names,  # type: ignore[misc]
        )
    ]

    results = []
    for _, row in result_df.iterrows():  # type: ignore[misc]
        entry_email = str(row["entry_email"])  # type: ignore[misc]
        entry_name = str(row["entry_name"])  # type: ignore[misc]
        subentry_email = str(row["subentry_email"])  # type: ignore[misc]
        subentry_name = str(row["subentry_name"])  # type: ignore[misc]

        record_1 = f"{entry_email}, {entry_name}"
        record_2 = f"{subentry_email}, {subentry_name}"
        record = f"{record_1}, {record_2}"
        results.append(record)

    return results


def _build_entries_with_subentries(
    mailmap_df: DataFrame,
) -> list[MailmapEntryWithSubentries]:
    entries_with_subentries = []

    grouped_df = mailmap_df.groupby("entry_email")

    for entry_email, group_df in grouped_df:
        subentries = []
        for _, row in group_df.iterrows():  # type: ignore[misc]
            subentry = MailmapSubentry(
                mailmap_subentry_email=(
                    row["subentry_email"]  # type: ignore[misc]
                ),
                mailmap_subentry_name=(
                    row["subentry_name"]  # type: ignore[misc]
                ),
            )
            subentries.append(subentry)
        first_row: Series = group_df.iloc[0]  # type: ignore[type-arg]
        entry = MailmapEntry(
            mailmap_entry_email=str(entry_email),
            mailmap_entry_name=first_row["entry_name"],  # type: ignore[misc]
        )

        entry_with_subentries = MailmapEntryWithSubentries(
            entry=entry,
            subentries=subentries,
        )

        entries_with_subentries.append(entry_with_subentries)

    return entries_with_subentries


async def _create_mailmap_entries_with_subentries(
    entries_with_subentries: list[MailmapEntryWithSubentries],
    organization_id: str,
) -> tuple[int, list[str]]:
    success_count = 0
    exceptions_messages = []
    for entry_with_subentries in entries_with_subentries:
        try:
            # don't raise exception to be able to store subentries even if
            # entry exists
            (
                _,
                subentries_results,
            ) = await mailmap.create_mailmap_entry_with_subentries(
                entry_with_subentries=entry_with_subentries,
                organization_id=organization_id,
                raise_exception=False,
            )
            success_count += subentries_results.count(True)
        except MailmapOrganizationNotFound as ex:
            exception_message = str(ex)
            exceptions_messages.append(exception_message)

    return success_count, exceptions_messages


async def import_mailmap(
    file: UploadFile,
    organization_id: str,
) -> tuple[int, list[str]]:
    raw_lines = await _get_lines_from_file(file)
    mailmap_df = _build_mailmap_dataframe(raw_lines)
    entries_with_subentries = _build_entries_with_subentries(mailmap_df)

    mapping_issues = _find_mailmap_mapping_issues(mailmap_df)
    if len(mapping_issues) > 0:
        raise ConflictingMailmapMappingError(mapping_issues)

    (
        success_count,
        exceptions_messages,
    ) = await _create_mailmap_entries_with_subentries(entries_with_subentries, organization_id)

    return success_count, exceptions_messages
