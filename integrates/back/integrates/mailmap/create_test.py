import os

from starlette.datastructures import UploadFile

from integrates.custom_exceptions import ConflictingMailmapMappingError
from integrates.db_model.mailmap.types import MailmapEntry, MailmapSubentry
from integrates.mailmap.create import create_mailmap_entry, create_mailmap_subentry, import_mailmap
from integrates.mailmap.get import get_mailmap_entry_with_subentries, get_mailmap_subentry
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import OrganizationFaker
from integrates.testing.mocks import mocks
from integrates.testing.utils import parametrize, raises


@parametrize(
    args=["filename", "organization_id", "expected_output"],
    cases=[
        ["mailmap1.txt", "40f6da5f-4f66-4bf0-825b-a2d9748ad6db", 2],
        ["mailmap2.txt", "40f6da5f-4f66-4bf0-825b-a2d9748ad6db", 3],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="40f6da5f-4f66-4bf0-825b-a2d9748ad6db")],
        ),
    )
)
async def test_import_mailmap(
    filename: str,
    organization_id: str,
    expected_output: int,
) -> None:
    # Act
    path: str = os.path.join(os.path.dirname(__file__), "test-data", filename)

    with open(path, encoding="utf-8") as file:
        values: tuple[int, list[str]] = await import_mailmap(
            file=UploadFile(file.buffer, size=None, filename=filename),
            organization_id=organization_id,
        )

        # Assert
        assert values[0] == expected_output


@parametrize(
    args=["filename", "organization_id", "expected_exception"],
    cases=[
        [
            "mailmap3.txt",
            "40f6da5f-4f66-4bf0-825b-a2d9748ad6db",
            ConflictingMailmapMappingError([]),
        ],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="40f6da5f-4f66-4bf0-825b-a2d9748ad6db")],
        ),
    )
)
async def test_import_mailmap_fail(
    filename: str,
    organization_id: str,
    expected_exception: Exception,
) -> None:
    # Act
    path: str = os.path.join(os.path.dirname(__file__), "test-data", filename)

    with raises(expected_exception.__class__):
        with open(path, encoding="utf-8") as file:
            await import_mailmap(
                file=UploadFile(file.buffer, size=None, filename=filename),
                organization_id=organization_id,
            )


@parametrize(
    args=["entry", "organization_id", "subentry"],
    cases=[
        [
            MailmapEntry(mailmap_entry_name="John Doe", mailmap_entry_email="john@entry.com"),
            "ORG#40f6da5f-4f66-4bf0-825b-a2d9748ad6db",
            MailmapSubentry(
                mailmap_subentry_name="John Doe", mailmap_subentry_email="john@entry.com"
            ),
        ],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="40f6da5f-4f66-4bf0-825b-a2d9748ad6db")]
        ),
    )
)
async def test_create_mailmap_entry(
    entry: MailmapEntry,
    organization_id: str,
    subentry: MailmapSubentry,
) -> None:
    await create_mailmap_entry(entry=entry, organization_id=organization_id)  # type: ignore[misc]

    _entry = await get_mailmap_entry_with_subentries("john@entry.com", organization_id)
    assert _entry["entry"]["mailmap_entry_name"] == entry["mailmap_entry_name"]  # type: ignore[misc]
    assert _entry["subentries"][0] == subentry


@parametrize(
    args=["subentry", "entry_email", "organization_id"],
    cases=[
        [
            MailmapSubentry(
                mailmap_subentry_name="John Doe", mailmap_subentry_email="john@subentry.com"
            ),
            "john@entry.com",
            "ORG#40f6da5f-4f66-4bf0-825b-a2d9748ad6db",
        ],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="40f6da5f-4f66-4bf0-825b-a2d9748ad6db")],
            mailmap_entries=[
                MailmapEntry(
                    mailmap_entry_name="john entry",
                    mailmap_entry_email="john@entry.com",
                    organization_id="ORG#40f6da5f-4f66-4bf0-825b-a2d9748ad6db",
                )
            ],
        ),
    )
)
async def test_create_mailmap_subentry(
    subentry: MailmapSubentry,
    entry_email: str,
    organization_id: str,
) -> None:
    await create_mailmap_subentry(  # type: ignore[misc]
        subentry=subentry, entry_email=entry_email, organization_id=organization_id
    )

    subentry_email: str = subentry["mailmap_subentry_email"]
    subentry_name: str = subentry["mailmap_subentry_name"]
    _subentry = await get_mailmap_subentry(
        entry_email=entry_email,
        subentry_email=subentry_email,
        subentry_name=subentry_name,
        organization_id=organization_id,
    )
    assert subentry == _subentry
