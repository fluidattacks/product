import importlib
import logging
import logging.config
import sys
import tempfile
import time
import uuid
from collections.abc import Iterable
from datetime import datetime
from decimal import Decimal
from itertools import chain, starmap

import aiofiles
import jinja2
import matplotlib
from aioextensions import collect
from jinja2 import select_autoescape
from matplotlib.pylab import axis, cla, clf, close, figure, pie, savefig
from pypdf import PdfReader
from typing_extensions import TypedDict

from integrates.context import BASE_URL, FI_ENVIRONMENT, STARTDIR
from integrates.custom_utils import datetime as datetime_utils
from integrates.custom_utils.reports import filter_context
from integrates.dataloaders import Dataloaders, get_new_context
from integrates.db_model.findings.types import Finding
from integrates.db_model.roots.enums import RootEnvironmentUrlType
from integrates.db_model.roots.types import (
    GitRoot,
    GitRootState,
    IPRoot,
    IPRootState,
    RootEnvironmentUrl,
    RootEnvironmentUrlsRequest,
    URLRoot,
    URLRootState,
)
from integrates.findings import domain as findings_domain
from integrates.reports.types import PdfFindingInfo, PDFWordlistEn, PDFWordlistEs
from integrates.reports.utils import call
from integrates.s3 import (
    operations as s3_ops,
)
from integrates.settings import LOGGING
from integrates.vulnerabilities import domain as vulns_domain
from integrates.vulnerabilities.types import GroupedVulnerabilitiesInfo, SeverityLevelCount

logging.config.dictConfig(LOGGING)
matplotlib.use("Agg")


# Constants
LOGGER = logging.getLogger(__name__)


class VulnTable(TypedDict):
    resume: list[list[float | int | str]]
    top: list[list[int | str]]


class Context(TypedDict, total=False):
    full_group: str
    team: str
    team_mail: str
    customer: str
    toe: str
    version: str
    revdate: str
    simpledate: str
    fluid_tpl: dict[str, str]
    main_pie_filename: str
    main_tables: VulnTable
    findings: tuple[PdfFindingInfo, ...]
    git_root: tuple[GitRootState, ...]
    environment_urls: tuple[str, ...]
    ip_root: tuple[IPRootState, ...]
    url_root: tuple[URLRootState]
    root_address: str
    root_branch: str
    root_environment_title: str
    root_git_title: str
    root_host: str
    root_ip_title: str
    root_nickname: str
    root_scope_title: str
    root_state: str
    root_url_title: str
    root_url: str
    finding_summary_background: str
    finding_summary_pdf: str
    finding_summary_pdf_n_pages: tuple[int, int] | None
    finding_summary_title_pdf: str
    finding_title: str
    finding_section_title: str
    general_view_background: str
    general_view_pdf: str
    goals_background: str
    where_title: str
    description_title: str
    resume_vuln_title: str
    resume_perc_multiline_title: str
    resume_vnum_title: str
    resume_vname_title: str
    resume_ttab_title: str
    resume_top_title: str
    evidence_title: str
    records_title: str
    threat_title: str
    solution_title: str
    requisite_title: str
    treatment_title: str
    severity_multiline_title: str
    severity_title: str
    severity_title_v3: str
    cardinality_title: str
    attack_vector_title: str
    resume_page_title: str
    resume_table_title: str
    state_title: str
    commit_hash: str
    crit_h: str
    crit_m: str
    crit_l: str
    field: str
    inputs: str
    line: str
    lines: str
    path: str
    port: str
    ports: str
    scope_background: str
    scope_pdf: str
    scope_pdf_n_pages: tuple[int, int] | None
    scope_title_pdf: str
    user: str
    date: str
    link: str
    imagesdir: str
    goals_pdf: str
    finding_table_background: str
    finding_table_pdf: str
    finding_table_pdf_n_pages: tuple[int, int] | None
    finding_table_title_pdf: str


class FooterContext(TypedDict, total=False):
    hs_signature_img: str
    vph_signature_img: str
    year: int


def _format_url(root_url: RootEnvironmentUrl) -> str:
    if root_url.state.url_type == RootEnvironmentUrlType.URL:
        return root_url.url

    url = ""
    if root_url.state.url_type:
        url += f"{root_url.state.url_type.value}: "
    if root_url.state.cloud_name:
        url += f"{root_url.state.cloud_name.value}: "
    url += root_url.url

    return url


async def _get_urls(loaders: Dataloaders, root_id: str, group_name: str) -> tuple[str, ...]:
    urls = await loaders.root_environment_urls.load(
        RootEnvironmentUrlsRequest(root_id=root_id, group_name=group_name),
    )

    return tuple(_format_url(url) for url in urls if url.state.include)


def _escape_for_asciidoctor(text: str) -> str:
    special_chars = ["*", "_", "^", "{", "}", "[", "]", "&", "=", "\\"]

    for char in special_chars:
        text = text.replace(char, f"\\{char}")

    text = text.replace(r"\\{nbsp\\}", "{nbsp}")

    return text


async def format_scope(loaders: Dataloaders, group_name: str) -> dict:
    roots = await loaders.group_roots.load(group_name)
    git_roots: tuple[GitRoot, ...] = tuple(root for root in roots if isinstance(root, GitRoot))
    url_roots: tuple[URLRoot, ...] = tuple(root for root in roots if isinstance(root, URLRoot))
    ip_roots: tuple[IPRoot, ...] = tuple(root for root in roots if isinstance(root, IPRoot))
    urls: tuple[tuple[str, ...], ...] = await collect(
        tuple(_get_urls(loaders, root.id, root.group_name) for root in git_roots),
        workers=2,
    )

    return {
        "git_root": tuple(root.state for root in git_roots),
        "environment_urls": tuple(set(chain.from_iterable(urls))),
        "ip_root": tuple(root.state for root in ip_roots),
        "url_root": tuple(root.state for root in url_roots),
    }


async def _format_finding(
    finding: Finding,
    evidence_set: list[dict[str, str]],
    words: dict[str, str],
) -> PdfFindingInfo:
    """Generate the pdf findings info."""
    loaders = get_new_context()
    grouped_vulnerabilities_info = await vulns_domain.get_grouped_vulnerabilities_info(
        loaders,
        finding,
    )
    closed_vulnerabilities = await findings_domain.get_closed_vulnerabilities_len(
        loaders,
        finding.id,
    )
    open_vulnerabilities = await findings_domain.get_open_vulnerabilities_len(loaders, finding.id)
    severity_score_v3 = await findings_domain.get_max_open_severity_score(loaders, finding.id)
    severity_score = await findings_domain.get_max_open_severity_score_v4(loaders, finding.id)

    finding_vulns_loader = loaders.finding_vulnerabilities_released_nzr
    vulnerabilities = await finding_vulns_loader.load(finding.id)
    treatments = vulns_domain.get_treatments_count(vulnerabilities)
    formated_treatments: list[str] = []
    if treatments.accepted > 0:
        formated_treatments.append(f"{words['treat_status_asu']}: {treatments.accepted}")
    if treatments.accepted_undefined > 0:
        formated_treatments.append(f"{words['treat_per_asu']}: {treatments.accepted_undefined}")
    if treatments.in_progress > 0:
        formated_treatments.append(f"{words['treat_status_rem']}: {treatments.in_progress}")
    if treatments.untreated > 0:
        formated_treatments.append(f"{words['treat_status_wor']}: {treatments.untreated}")

    if open_vulnerabilities > 0:
        state = words["fin_status_open"]
        treatment = "\n".join(sorted(formated_treatments))
    else:
        state = words["fin_status_closed"]
        treatment = "-"

    return PdfFindingInfo(
        attack_vector_description=filter_context(finding.attack_vector_description),
        closed_vulnerabilities=closed_vulnerabilities,
        description=filter_context(finding.description),
        evidence_set=evidence_set,
        grouped_inputs_vulnerabilities=(
            grouped_vulnerabilities_info.grouped_inputs_vulnerabilities
        ),
        grouped_lines_vulnerabilities=(grouped_vulnerabilities_info.grouped_lines_vulnerabilities),
        grouped_ports_vulnerabilities=(grouped_vulnerabilities_info.grouped_ports_vulnerabilities),
        open_vulnerabilities=open_vulnerabilities,
        recommendation=filter_context(finding.recommendation),
        requirements=filter_context(finding.requirements),
        severity_score_v3=severity_score_v3,
        severity_score=severity_score,
        state=state,
        title=filter_context(finding.title),
        threat=filter_context(finding.threat),
        treatment=treatment,
        vulnerabilities_summary=finding.unreliable_indicators.vulnerabilities_summary,
        where=filter_context(grouped_vulnerabilities_info.where),
    )


def get_percentage(number_of_findings: int, total_findings: int) -> float:
    return float(number_of_findings * 100 / total_findings if total_findings > 0 else 0.0)


def get_crit_as_text_v3(finding: PdfFindingInfo, words: dict[str, str]) -> str:
    if Decimal("9.0") <= finding.severity_score_v3 <= Decimal("10.0"):
        return words["crit_c"]
    if Decimal("7.0") <= finding.severity_score_v3 <= Decimal("8.9"):
        return words["crit_h"]
    if Decimal("4.0") <= finding.severity_score_v3 <= Decimal("6.9"):
        return words["crit_m"]
    return words["crit_l"]


def get_vuln_level_count(vulns: tuple[GroupedVulnerabilitiesInfo, ...]) -> SeverityLevelCount:
    critical, high, medium, low, total = (0, 0, 0, 0, 0)
    for vuln in vulns:
        critical += int(vuln.severity_score >= 9)
        high += int(vuln.severity_score >= 7 and vuln.severity_score < 9)
        medium += int(vuln.severity_score >= 4 and vuln.severity_score < 7)
        low += int(vuln.severity_score < 4)
        total += 1
    return SeverityLevelCount(critical, high, medium, low, total)


def make_vuln_table(
    context_findings: tuple[PdfFindingInfo, ...],
    words: dict[str, str],
) -> VulnTable:
    """Label findings percent quantity."""
    number_of_findings: int = len(
        [
            finding
            for finding in context_findings
            if finding.open_vulnerabilities > 0
            and Decimal("0.0") <= finding.severity_score <= Decimal("10.0")
        ],
    )
    vuln_table: list[list[float | int | str]] = [
        [words["vuln_c"], 0, 0],
        [words["vuln_h"], 0, 0],
        [words["vuln_m"], 0, 0],
        [words["vuln_l"], 0, 0],
        ["Total", number_of_findings, 0],
    ]
    top_table: list[list[int | str]] = []
    ttl_vulns, top = 0, 1
    for finding in context_findings:
        crit_as_text = words["crit_l"]
        crit_as_text_v3 = get_crit_as_text_v3(finding, words)
        vuln_amount = finding.open_vulnerabilities

        if vuln_amount <= 0:
            continue

        if Decimal(9) <= finding.severity_score:
            vuln_table[0][1] = int(vuln_table[0][1]) + 1
            crit_as_text = words["crit_c"]
        elif Decimal(7) <= finding.severity_score < Decimal(9):
            vuln_table[1][1] = int(vuln_table[1][1]) + 1
            crit_as_text = words["crit_h"]
        elif Decimal(4) <= finding.severity_score < Decimal(7):
            vuln_table[2][1] = int(vuln_table[2][1]) + 1
            crit_as_text = words["crit_m"]
        elif finding.severity_score < Decimal(4):
            vuln_table[3][1] = int(vuln_table[3][1]) + 1

        if vuln_amount == (
            sum(
                [
                    finding.vulnerabilities_summary.open_critical,
                    finding.vulnerabilities_summary.open_high,
                    finding.vulnerabilities_summary.open_medium,
                    finding.vulnerabilities_summary.open_low,
                ]
            )
        ):
            vuln_table[0][2] = int(vuln_table[0][2]) + finding.vulnerabilities_summary.open_critical
            vuln_table[1][2] = int(vuln_table[1][2]) + finding.vulnerabilities_summary.open_high
            vuln_table[2][2] = int(vuln_table[2][2]) + finding.vulnerabilities_summary.open_medium
            vuln_table[3][2] = int(vuln_table[3][2]) + finding.vulnerabilities_summary.open_low
            ttl_vulns += vuln_amount
        else:
            vuln_level_count = get_vuln_level_count(
                finding.grouped_inputs_vulnerabilities
                + finding.grouped_lines_vulnerabilities
                + finding.grouped_ports_vulnerabilities
            )
            vuln_table[0][2] = int(vuln_table[0][2]) + vuln_level_count.critical
            vuln_table[1][2] = int(vuln_table[1][2]) + vuln_level_count.high
            vuln_table[2][2] = int(vuln_table[2][2]) + vuln_level_count.medium
            vuln_table[3][2] = int(vuln_table[3][2]) + vuln_level_count.low
            ttl_vulns += vuln_level_count.total

        if top <= 5:
            top_table.append(
                [
                    top,
                    f"{finding.severity_score_v3!s} {crit_as_text_v3}",
                    f"{finding.severity_score!s} {crit_as_text}",
                    finding.title,
                ],
            )
            top += 1
    vuln_table[4][2] = ttl_vulns
    return {"resume": vuln_table, "top": top_table}


class CreatorPdf:
    """Class to generate reports in PDF."""

    command: list[str] = []
    context: Context | None = None
    doctype: str = "executive"
    font_dir: str = "/resources/fonts"
    footer_context: FooterContext | None = None
    lang: str = "en"
    out_name: str = ""
    proj_tpl: str = "templates/pdf/executive.adoc"
    result_dir: str = "/tpls/results_pdf/"
    style: str = "fluid"
    style_dir: str = "/resources/themes"
    images_dir: str = "/resources/themes"
    tpl_dir: str = "/tpls/"
    wordlist: dict[str, dict[str, str]] = {}

    def __init__(
        self,
        *,
        lang: str,
        doctype: str,
        tempdir: str,
        group: str,
        user: str,
        style: str = "fluid",
    ) -> None:
        """Class constructor."""
        self.path = f"{STARTDIR}/integrates/back/integrates/reports"
        self.tpl_img_path = tempdir

        self.doctype = doctype
        self.font_dir = self.path + self.font_dir
        self.lang = lang
        self.result_dir = self.path + self.result_dir
        self.tpl_dir = self.path + self.tpl_dir
        self.style = style
        self.style_dir = self.path + self.style_dir
        self.out_name_finding_summary_title = f"{self.result_dir}{uuid.uuid4()!s}.pdf"
        self.out_name_finding_summary = f"{self.result_dir}{uuid.uuid4()!s}.pdf"
        self.out_name_scope_title = f"{self.result_dir}{uuid.uuid4()!s}.pdf"
        self.out_name_scope = f"{self.result_dir}{uuid.uuid4()!s}.pdf"
        self.images_dir = self.path + self.images_dir
        self.out_name_goals = f"{self.result_dir}{uuid.uuid4()!s}.pdf"
        self.out_name_finding_table = f"{self.result_dir}{uuid.uuid4()!s}.pdf"
        self.out_name_finding_table_title = f"{self.result_dir}{uuid.uuid4()!s}.pdf"
        self.group_name = group
        self.user_email = user
        self.out_name_general_view = f"{self.result_dir}{uuid.uuid4()!s}.pdf"
        if self.doctype == "tech":
            self.proj_tpl = "templates/pdf/tech.adoc"
        searchpath = self.path
        template_loader = jinja2.FileSystemLoader(searchpath=searchpath)
        self.template_env = jinja2.Environment(
            loader=template_loader,
            autoescape=select_autoescape(["html", "xml"], default=True),
            enable_async=True,
        )
        self.template_env.filters["asciidoctor"] = _escape_for_asciidoctor

        importlib.reload(sys)
        self.lang_support()

    def create_command(self, tpl_name: str, out_path: str) -> None:
        """Create the SO command to create the PDF with asciidoctor."""
        self.command = [
            "asciidoctor-pdf",
            "-a",
            f"pdf-themesdir={self.style_dir}",
            "-a",
            f"pdf-theme={self.style}",
            "-a",
            f"pdf-fontsdir={self.font_dir}",
            "-a",
            f"{'env-en=True' if self.lang == 'en' else 'env-es=True'}",
            "-a",
            "allow-uri-read=False",
            "--safe-mode",
            "secure",
            "-a",
            "linkcss=False",
            "-D",
            f"{self.result_dir}",
            "-o",
            out_path,
            tpl_name,
        ]

    async def fill_group(
        self,
        *,
        findings: Iterable[Finding],
        finding_evidences_set: dict[str, list[dict[str, str]]],
        group: str,
        description: str,
        user: str,
    ) -> None:
        """Add group information."""
        loaders = get_new_context()
        words = self.wordlist[self.lang]
        doctype = words[self.doctype]
        full_group = f"{description} ({group.capitalize()})"
        fluid_tpl_content = await self.make_content(words)
        context_findings = await collect(
            [
                _format_finding(finding, finding_evidences_set[finding.id], words)
                for finding in findings
            ],
        )
        context_root = filter_context(await format_scope(loaders, group))
        main_tables = make_vuln_table(context_findings, words)
        main_pie_filename = self.make_pie_finding(context_findings, group, words)
        self.context = {
            "full_group": full_group,
            "team": "Engineering Team",
            "team_mail": "engineering@fluidattacks.com",
            "customer": "",
            "toe": description,
            "version": "v1.0",
            "revdate": f"{doctype} {time.strftime('%d/%m/%Y')}",
            "simpledate": time.strftime("%Y.%m.%d"),
            "fluid_tpl": fluid_tpl_content,
            "main_pie_filename": main_pie_filename,
            "main_tables": main_tables,
            "findings": context_findings,
            "general_view_background": (
                f"{self.path}/resources/themes/background-general-view.png"
            ),
            "general_view_pdf": self.out_name_general_view,
            "goals_background": (f"{self.path}/resources/themes/background-goals.png"),
            # Titles according to language
            "finding_title": words["finding_title"],
            "finding_section_title": words["finding_section_title"],
            "finding_summary_title_pdf": self.out_name_finding_summary_title,
            "finding_summary_pdf": self.out_name_finding_summary,
            "finding_summary_pdf_n_pages": None,
            "finding_summary_background": (
                f"{self.path}/resources/themes/background-finding-summary.png[]"
            ),
            "git_root": context_root["git_root"],
            "environment_urls": context_root["environment_urls"],
            "ip_root": context_root["ip_root"],
            "url_root": context_root["url_root"],
            "root_address": words["root_address"],
            "root_branch": words["root_branch"],
            "root_environment_title": words["root_environment_title"],
            "root_git_title": words["root_git_title"],
            "root_host": words["root_host"],
            "root_ip_title": words["root_ip_title"],
            "root_nickname": words["root_nickname"],
            "root_scope_title": words["root_scope_title"],
            "root_state": words["root_state"],
            "root_url_title": words["root_url_title"],
            "root_url": words["root_url"],
            "where_title": words["where_title"],
            "description_title": words["description_title"],
            "resume_vuln_title": words["resume_vuln_title"],
            "resume_perc_multiline_title": words["resume_perc_multiline_title"],
            "resume_vname_title": words["resume_vname_title"],
            "resume_vnum_title": words["resume_vnum_title"],
            "resume_ttab_title": words["resume_ttab_title"],
            "resume_top_title": words["resume_top_title"],
            "evidence_title": words["evidence_title"],
            "records_title": words["records_title"],
            "threat_title": words["threat_title"],
            "scope_title_pdf": self.out_name_scope_title,
            "scope_pdf": self.out_name_scope,
            "scope_pdf_n_pages": None,
            "scope_background": (f"{self.path}/resources/themes/background-scope.png[]"),
            "solution_title": words["solution_title"],
            "requisite_title": words["requisite_title"],
            "treatment_title": words["treatment_title"],
            "severity_multiline_title": words["severity_multiline_title"],
            "severity_title": words["severity_title"],
            "severity_title_v3": words["severity_title_v3"],
            "cardinality_title": words["cardinality_title"],
            "attack_vector_title": words["attack_vector_title"],
            "resume_page_title": words["resume_page_title"],
            "resume_table_title": words["resume_table_title"],
            "state_title": words["state_title"],
            "commit_hash": words["commit_hash"],
            "crit_h": words["crit_h"],
            "crit_m": words["crit_m"],
            "crit_l": words["crit_l"],
            "field": words["field"],
            "inputs": words["inputs"],
            "line": words["line"],
            "lines": words["lines"],
            "path": words["path"],
            "port": words["port"],
            "ports": words["ports"],
            "user": user,
            "date": time.strftime("%Y-%m-%d at %H:%M"),
            "link": f"{BASE_URL}/groups/{group}/vulns",
            "imagesdir": self.images_dir,
            "goals_pdf": self.out_name_goals,
            "finding_table_background": (
                f"{self.path}/resources/themes/background-finding-table.png"
            ),
            "finding_table_pdf": self.out_name_finding_table,
            "finding_table_pdf_n_pages": None,
            "finding_table_title_pdf": self.out_name_finding_table_title,
        }

    def lang_support(self) -> None:
        """Define the dictionaries of accepted languages."""
        self.wordlist = {}
        self.lang_support_en()
        self.lang_support_es()

    def lang_support_en(self) -> None:
        """Adds the English dictionary."""
        self.wordlist["en"] = dict(zip(PDFWordlistEn.keys(), PDFWordlistEn.labels(), strict=False))

    def lang_support_es(self) -> None:
        """Adds the Spanish dictionary."""
        self.wordlist["es"] = dict(zip(PDFWordlistEs.keys(), PDFWordlistEs.labels(), strict=False))

    async def make_content(self, words: dict[str, str]) -> dict[str, str]:
        """Create context with the titles of the document."""
        base_img = f"{self.path}/templates/pdf/" + "{name}_{lang}.png"
        base_adoc = f"templates/pdf/footer_{self.lang}.adoc"
        template = self.template_env.get_template(base_adoc)
        self.footer_context = {
            "hs_signature_img": "HoS signature",
            "vph_signature_img": "VPoH signature",
            "year": datetime.today().year,
        }
        with (
            tempfile.NamedTemporaryFile(mode="w+", delete=False) as hs_file,
            tempfile.NamedTemporaryFile(mode="w+", delete=False) as vph_file,
        ):
            if FI_ENVIRONMENT == "production":
                await s3_ops.download_file(
                    "resources/certificate/signature.png",
                    hs_file.name,
                )
                self.footer_context["hs_signature_img"] = hs_file.name
                await s3_ops.download_file(
                    "resources/certificate/vph_signature.png",
                    vph_file.name,
                )
                self.footer_context["vph_signature_img"] = vph_file.name
            render_text = await template.render_async(filter_context(self.footer_context))

            return {
                "content_title": words["content_title"],
                "content_list": words["content_list"],
                "goals_title": words["goals_title"],
                "goals_img": base_img.format(name="goals", lang=self.lang),
                "severity_img": base_img.format(name="severity", lang=self.lang),
                "footer_adoc": render_text,
            }

    def make_pie_finding(
        self,
        context_findings: tuple[PdfFindingInfo, ...],
        group: str,
        words: dict[str, str],
    ) -> str:
        """Create the findings graph."""
        figure(1, figsize=(6, 6))
        finding_state_pie = [0, 0, 0, 0]  # A, PC, C
        finding_state_pielabels = [
            words["vuln_c_plain"],
            words["vuln_h_plain"],
            words["vuln_m_plain"],
            words["vuln_l_plain"],
        ]
        colors = ["#980000", "red", "orange", "yellow"]
        explode = (0.1, 0, 0, 0)
        for finding in context_findings:
            if (
                Decimal("9.0") <= finding.severity_score <= Decimal("10.0")
                and finding.open_vulnerabilities > 0
            ):
                finding_state_pie[0] += 1
            elif (
                Decimal("7.0") <= finding.severity_score <= Decimal("8.9")
                and finding.open_vulnerabilities > 0
            ):
                finding_state_pie[1] += 1
            elif (
                Decimal("4.0") <= finding.severity_score <= Decimal("6.9")
                and finding.open_vulnerabilities > 0
            ):
                finding_state_pie[2] += 1
            elif (
                Decimal("0.0") <= finding.severity_score <= Decimal("3.9")
                and finding.open_vulnerabilities > 0
            ):
                finding_state_pie[3] += 1
        pie(
            x=finding_state_pie,
            autopct="%1.0f%%",
            colors=colors,
            explode=explode,
            labels=finding_state_pielabels,
            normalize=sum(finding_state_pie) > 0,
            startangle=90,
        )
        axis("equal")
        pie_filename = f"{self.tpl_img_path}/finding_graph_{group}.png"
        savefig(pie_filename, bbox_inches="tight", transparent=True, dpi=100)
        cla()
        clf()
        close("all")
        return pie_filename

    async def get_page(
        self,
        name: str,
        template_path: str,
        out_name: str,
    ) -> None:
        template = self.template_env.get_template(template_path)
        tpl_name = f"{self.tpl_dir}{self.group_name}_{name}_IT.tpl"
        async with aiofiles.open(tpl_name, "wb") as tplfile:
            await tplfile.write((await template.render_async(self.context)).encode("utf-8"))

        self.create_command(tpl_name, out_name)
        await call(
            program=self.command[0],
            command=self.command[1:],
            group_name=self.group_name,
        )

    async def tech(
        self,
        findings: Iterable[Finding],
        finding_evidences_set: dict[str, list[dict[str, str]]],
        description: str,
    ) -> None:
        """Create the template to render and apply the context."""
        await self.fill_group(
            findings=findings,
            finding_evidences_set=finding_evidences_set,
            group=self.group_name,
            description=description,
            user=self.user_email,
        )
        current_date = datetime_utils.get_as_str(datetime_utils.get_now())
        self.out_name = f"technical-{self.group_name}-{current_date}.pdf"

        await collect(
            list(
                starmap(
                    self.get_page,
                    [
                        ("scope_summary", "templates/pdf/scope.adoc", self.out_name_scope_title),
                        (
                            "finding_summary",
                            "templates/pdf/finding_summary.adoc",
                            self.out_name_finding_summary_title,
                        ),
                        ("goals", "templates/pdf/goals.adoc", self.out_name_goals),
                        (
                            "finding_table",
                            "templates/pdf/finding_table.adoc",
                            self.out_name_finding_table_title,
                        ),
                        (
                            "general_view",
                            "templates/pdf/general_view.adoc",
                            self.out_name_general_view,
                        ),
                    ],
                )
            ),
            workers=1,
        )
        if self.context:
            with open(
                self.out_name_scope_title,
                "rb",
            ) as pdf_file:
                output_file = PdfReader(pdf_file)

                self.context["scope_background"] = f"{self.path}/resources/themes/background.png[]"
                self.context["scope_pdf"] = self.out_name_scope
                self.context["scope_pdf_n_pages"] = (
                    2,
                    len(output_file.pages) + 1,
                )
            with open(
                self.out_name_finding_summary_title,
                "rb",
            ) as pdf_file:
                output_file = PdfReader(pdf_file)

                self.context["finding_summary_background"] = (
                    f"{self.path}/resources/themes/background.png[]"
                )
                self.context["finding_summary_pdf"] = self.out_name_finding_summary
                self.context["finding_summary_pdf_n_pages"] = (
                    2,
                    len(output_file.pages) + 1,
                )

            with open(
                self.out_name_finding_table_title,
                "rb",
            ) as pdf_file:
                output_table_file = PdfReader(pdf_file)

                self.context["finding_table_background"] = (
                    f"{self.path}/resources/themes/background.png"
                )
                self.context["finding_table_pdf"] = self.out_name_finding_table
                self.context["finding_table_pdf_n_pages"] = (
                    2,
                    len(output_table_file.pages) + 1,
                )

        await self.get_page(
            "scope",
            "templates/pdf/scope.adoc",
            self.out_name_scope,
        )

        await self.get_page(
            "finding_summary",
            "templates/pdf/finding_summary.adoc",
            self.out_name_finding_summary,
        )

        await self.get_page(
            "finding_table",
            "templates/pdf/finding_table.adoc",
            self.out_name_finding_table,
        )

        template = self.template_env.get_template(self.proj_tpl)
        tpl_name = f"{self.tpl_dir}{self.group_name}_IT.tpl"
        render_text = await template.render_async(self.context)
        async with aiofiles.open(tpl_name, "wb") as tplfile:
            await tplfile.write(render_text.encode("utf-8"))
        self.create_command(tpl_name, self.out_name)
        await call(
            program=self.command[0],
            command=self.command[1:],
            group_name=self.group_name,
        )
