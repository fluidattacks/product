from datetime import datetime

from integrates.batch import dal as batch_dal
from integrates.batch.dal.get import get_actions_by_name
from integrates.batch.enums import Action
from integrates.custom_exceptions import (
    InvalidAcceptanceSeverity,
    InvalidDate,
    InvalidFindingTitle,
    InvalidReportFilter,
    ReportAlreadyRequested,
    RequestedReportError,
    RequiredNewPhoneNumber,
)
from integrates.dataloaders import Dataloaders, get_new_context
from integrates.reports.enums import ReportType
from integrates.reports.request_report import (
    request_report,
)
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    BatchProcessingFaker,
    GitRootFaker,
    GitRootStateFaker,
    GroupAccessFaker,
    GroupAccessStateFaker,
    GroupFaker,
    GroupStateFaker,
    OrganizationFaker,
    StakeholderFaker,
    StakeholderPhoneFaker,
)
from integrates.testing.mocks import Mock, mocks
from integrates.testing.utils import freeze_time, parametrize, raises
from integrates.verify import (
    operations as verify_operations,
)

EMAIL_TEST = "user@clientapp.com"
GROUP_NAME = "group1"
ROOT_ID = "root1"


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(id="org1"),
            ],
            stakeholders=[
                StakeholderFaker(email=EMAIL_TEST, role="admin", phone=StakeholderPhoneFaker()),
            ],
            groups=[
                GroupFaker(name=GROUP_NAME, organization_id="org1"),
            ],
            roots=[
                GitRootFaker(
                    group_name=GROUP_NAME,
                    id=ROOT_ID,
                    state=GitRootStateFaker(
                        nickname="back",
                    ),
                ),
            ],
        ),
    ),
    others=[
        Mock(verify_operations, "check_verification", "async", None),
        Mock(batch_dal.put, "put_action_to_batch", "async", "123456"),
    ],
)
async def test_report() -> None:
    loaders: Dataloaders = get_new_context()

    result = await request_report(
        loaders=loaders,
        user_email=EMAIL_TEST,
        group_name=GROUP_NAME,
        verification_code="00000",
        report_type="XLS",
    )
    assert result is True
    report_action = await get_actions_by_name(action_name=Action.REPORT, entity=GROUP_NAME)

    assert len(report_action) == 1
    assert report_action[0].additional_info["report_type"] == "XLS"

    notifications = await loaders.user_notifications.load(EMAIL_TEST)
    assert len(notifications) == 1


@parametrize(args=["report_type"], cases=[[ReportType.PDF], [ReportType.DATA], [ReportType.CERT]])
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(id="org1"),
            ],
            stakeholders=[
                StakeholderFaker(
                    email=EMAIL_TEST, role="group_manager", phone=StakeholderPhoneFaker()
                ),
            ],
            group_access=[
                GroupAccessFaker(
                    email=EMAIL_TEST,
                    group_name=GROUP_NAME,
                    state=GroupAccessStateFaker(has_access=True, role="group_manager"),
                ),
            ],
            groups=[
                GroupFaker(
                    name=GROUP_NAME,
                    organization_id="org1",
                    business_id="14441323",
                    business_name="Testing Company and Sons",
                ),
            ],
            roots=[
                GitRootFaker(
                    group_name=GROUP_NAME,
                    id=ROOT_ID,
                    state=GitRootStateFaker(
                        nickname="back",
                    ),
                ),
            ],
        ),
    ),
    others=[
        Mock(verify_operations, "check_verification", "async", None),
        Mock(batch_dal.put, "put_action_to_batch", "async", "123456"),
    ],
)
@freeze_time("2024-12-01T05:00:00.00")
async def test_report_with_extra_arguments(report_type: ReportType) -> None:
    loaders: Dataloaders = get_new_context()

    result = await request_report(
        loaders=loaders,
        user_email=EMAIL_TEST,
        group_name=GROUP_NAME,
        verification_code="00000",
        report_type=report_type,
        states=["VULNERABLE"],
        treatments=["ACCEPTED", "IN_PROGRESS"],
        verifications=["REQUESTED"],
        closing_date=datetime.fromisoformat("2024-11-01T05:00:00+00:00"),
        start_closing_date=datetime.fromisoformat("2024-10-01T05:00:00+00:00"),
        finding_title="007. Cross-site request forgery",
        age=10,
        min_severity=float("3.0"),
        max_severity=float("9.0"),
    )
    assert result is True
    report_action = await get_actions_by_name(action_name=Action.REPORT, entity=GROUP_NAME)

    assert len(report_action) == 1
    assert report_action[0].additional_info == {
        "age": 10,
        "closing_date": "2024-11-01T05:00:00+00:00",
        "finding_title": "007",
        "last_report": None,
        "location": "",
        "max_release_date": None,
        "max_severity": 9.0,
        "min_release_date": None,
        "min_severity": 3.0,
        "notification_id": "2024-12-01T05:00:00",
        "report_type": report_type,
        "start_closing_date": "2024-10-01T05:00:00+00:00",
        "states": ["SAFE"],
        "treatments": ["ACCEPTED", "ACCEPTED_UNDEFINED", "IN_PROGRESS", "UNTREATED"],
        "verifications": [],
    }

    notifications = await loaders.user_notifications.load(EMAIL_TEST)
    assert len(notifications) == 1


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(id="org1"),
            ],
            stakeholders=[
                StakeholderFaker(email=EMAIL_TEST, role="admin", phone=StakeholderPhoneFaker()),
                StakeholderFaker(
                    email="stakeholder_without_phone@test.com",
                    role="group_manager",
                    phone=None,
                ),
                StakeholderFaker(email="user@clientapp.com", role="user"),
            ],
            groups=[
                GroupFaker(name=GROUP_NAME, organization_id="org1"),
                GroupFaker(
                    name="group2",
                    organization_id="org1",
                    state=GroupStateFaker(has_essential=False),
                ),
                GroupFaker(
                    name="group3",
                    organization_id="org1",
                    business_id="123456",
                    business_name="client app",
                    description="Group for my new app",
                ),
            ],
            group_access=[
                GroupAccessFaker(
                    email="user@clientapp.com",
                    group_name="group3",
                    state=GroupAccessStateFaker(role="user"),
                )
            ],
            roots=[
                GitRootFaker(
                    group_name=GROUP_NAME,
                    id=ROOT_ID,
                    state=GitRootStateFaker(
                        nickname="back",
                    ),
                ),
            ],
            actions=[
                BatchProcessingFaker(
                    action_name=Action.REPORT,
                    entity=GROUP_NAME,
                    additional_info={
                        "report_type": "XLS",
                        "treatments": [
                            "ACCEPTED",
                            "ACCEPTED_UNDEFINED",
                            "IN_PROGRESS",
                            "UNTREATED",
                        ],
                        "states": ["SAFE", "VULNERABLE"],
                        "verifications": [],
                        "closing_date": None,
                        "start_closing_date": None,
                        "finding_title": "",
                        "age": None,
                        "last_report": None,
                        "min_release_date": None,
                        "max_release_date": None,
                        "location": "",
                        "min_severity": None,
                        "max_severity": None,
                    },
                    subject=EMAIL_TEST,
                )
            ],
        ),
    ),
    others=[
        Mock(verify_operations, "check_verification", "async", None),
    ],
)
@freeze_time("2024-12-01T05:00:00.00")
async def test_request_report_errors() -> None:
    loaders: Dataloaders = get_new_context()
    with raises(ReportAlreadyRequested):
        await request_report(
            loaders=loaders,
            user_email=EMAIL_TEST,
            group_name=GROUP_NAME,
            verification_code="00000",
            report_type="XLS",
        )
    with raises(
        RequestedReportError, match="Group must have Machine enabled to generate Certificates"
    ):
        await request_report(
            loaders=loaders,
            user_email=EMAIL_TEST,
            group_name="group2",
            verification_code="00000",
            report_type="CERT",
        )
    with raises(RequestedReportError, match="Lacking required group information to generate"):
        await request_report(
            loaders=loaders,
            user_email=EMAIL_TEST,
            group_name=GROUP_NAME,
            verification_code="00000",
            report_type="CERT",
        )
    with raises(
        RequestedReportError, match="Only user or customer managers can request certificates"
    ):
        await request_report(
            loaders=loaders,
            user_email="user@clientapp.com",
            group_name="group3",
            verification_code="00000",
            report_type="CERT",
        )
    with raises(InvalidReportFilter):
        await request_report(
            loaders=loaders,
            user_email=EMAIL_TEST,
            group_name=GROUP_NAME,
            verification_code="00000",
            report_type="PDF",
            age=10001,
        )
    with raises(InvalidFindingTitle):
        await request_report(
            loaders=loaders,
            user_email=EMAIL_TEST,
            group_name=GROUP_NAME,
            verification_code="00000",
            report_type="PDF",
            finding_title="0078. Cross-site request forgery -- host",
        )
    with raises(InvalidAcceptanceSeverity):
        await request_report(
            loaders=loaders,
            user_email=EMAIL_TEST,
            group_name=GROUP_NAME,
            verification_code="00000",
            report_type="PDF",
            min_severity=float("-1.0"),
        )
    with raises(RequiredNewPhoneNumber):
        await request_report(
            loaders=loaders,
            user_email="stakeholder_without_phone@test.com",
            group_name=GROUP_NAME,
            verification_code="00000",
            report_type="PDF",
        )
    with raises(InvalidDate):
        await request_report(
            loaders=loaders,
            user_email=EMAIL_TEST,
            group_name=GROUP_NAME,
            verification_code="00000",
            report_type="PDF",
            closing_date=datetime.fromisoformat("2025-01-05T05:00:00+00:00"),
        )
