from datetime import (
    datetime,
)
from decimal import (
    Decimal,
)
from typing import (
    Any,
    TypedDict,
)

import pytz

from integrates import (
    authz,
)
from integrates.batch import (
    dal as batch_dal,
)
from integrates.batch.enums import (
    Action,
    IntegratesBatchQueue,
)
from integrates.class_types.types import (
    Item,
)
from integrates.custom_exceptions import (
    InvalidDate,
    InvalidReportFilter,
    ReportAlreadyRequested,
    RequestedReportError,
    RequiredNewPhoneNumber,
)
from integrates.custom_utils.datetime import (
    get_as_utc_iso_format,
    get_now,
)
from integrates.custom_utils.findings import (
    is_valid_finding_title,
)
from integrates.custom_utils.groups import get_group
from integrates.custom_utils.validations_deco import (
    validate_fields_deco,
    validate_length_deco,
)
from integrates.custom_utils.vulnerabilities import (
    get_inverted_state_converted,
)
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.enums import (
    TreatmentStatus,
)
from integrates.db_model.vulnerabilities.enums import (
    VulnerabilityStateStatus,
    VulnerabilityVerificationStatus,
)
from integrates.notifications.reports import (
    add_report_notification,
)
from integrates.organizations.domain import (
    validate_max_acceptance_severity,
    validate_min_acceptance_severity,
)
from integrates.settings import (
    TIME_ZONE,
)
from integrates.stakeholders.utils import (
    get_international_format_phone_number,
)
from integrates.treatment.utils import (
    get_inverted_treatment_converted,
)
from integrates.verify import (
    operations as verify_operations,
)

MIN_VALUE = 0
MAX_VALUE = 10000


class VulnerabilityInfo(TypedDict):
    states: set[VulnerabilityStateStatus]
    treatments: set[TreatmentStatus]
    verifications: set[VulnerabilityVerificationStatus]


def filter_unique_report(
    *,
    old_additional_info: Item,
    new_type: str,
    new_treatments: set[TreatmentStatus],
    new_states: set[VulnerabilityStateStatus],
    new_verifications: set[VulnerabilityVerificationStatus],
    new_closing_date: datetime | None,
    new_finding_title: str,
    new_start_closing_date: datetime | None,
) -> bool:
    if new_type == "XLS":
        return (
            new_type == old_additional_info.get("report_type")
            and sorted(new_treatments) == sorted(old_additional_info.get("treatments", []))
            and sorted(new_states) == sorted(old_additional_info.get("states", []))
            and sorted(new_verifications) == sorted(old_additional_info.get("verifications", []))
            and (new_closing_date.isoformat() if new_closing_date else None)
            == old_additional_info.get("closing_date", None)
            and new_finding_title == old_additional_info.get("finding_title", "")
            and (new_start_closing_date.isoformat() if new_start_closing_date else None)
            == old_additional_info.get("start_closing_date", None)
        )

    return new_type == old_additional_info.get("report_type")


def _format_group_date(date: datetime | None) -> str | None:
    return get_as_utc_iso_format(date) if date else None


async def _validate_and_verify_report_request(
    *,
    loaders: Dataloaders,
    group_name: str,
    user_email: str,
    report_type: str,
    treatments: set[TreatmentStatus],
    states: set[VulnerabilityStateStatus],
    verifications: set[VulnerabilityVerificationStatus],
    date_info: dict[str, datetime | None],
    finding_title: str,
    verification_code: str,
) -> None:
    existing_actions = await batch_dal.get_actions_by_name(
        action_name=Action.REPORT,
        entity=group_name,
    )

    if list(
        filter(
            lambda x: x.subject.lower() == user_email.lower()
            and filter_unique_report(
                old_additional_info=x.additional_info,
                new_type=report_type,
                new_treatments=treatments,
                new_states=states,
                new_verifications=verifications,
                new_closing_date=date_info["closing_date"],
                new_finding_title=finding_title,
                new_start_closing_date=date_info["start_closing_date"],
            ),
            existing_actions,
        ),
    ):
        raise ReportAlreadyRequested()
    stakeholder = await loaders.stakeholder.load(user_email)
    user_phone = stakeholder.phone if stakeholder else None
    if not user_phone:
        raise RequiredNewPhoneNumber()

    await verify_operations.check_verification(
        recipient=get_international_format_phone_number(user_phone),
        code=verification_code,
    )


def _get_report_name(report_type: str) -> str:
    return {
        "CERT": "Certificate",
        "DATA": "Export",
        "PDF": "Executive report",
        "XLS": "Technical report",
    }.get(report_type, "Report")


@validate_fields_deco(["location"])
@validate_length_deco("location", max_length=100)
async def _get_url_group_report(
    *,
    loaders: Dataloaders,
    report_type: str,
    user_email: str,
    group_name: str,
    vulnerability_info: VulnerabilityInfo,
    finding_title: str,
    age: int | None,
    severity_info: dict[str, Decimal | None],
    last_report: int | None,
    location: str,
    verification_code: str,
    date_info: dict[str, datetime | None],
) -> bool:
    await _validate_and_verify_report_request(
        loaders=loaders,
        group_name=group_name,
        user_email=user_email,
        report_type=report_type,
        date_info=date_info,
        finding_title=finding_title,
        verification_code=verification_code,
        **vulnerability_info,
    )
    notification = await add_report_notification(
        report_format=report_type,
        report_name=f"{_get_report_name(report_type)} ({group_name})",
        user_email=user_email,
    )
    additional_info = {
        "notification_id": notification.id,
        "report_type": report_type,
        "treatments": sorted(vulnerability_info["treatments"]),
        "states": sorted(vulnerability_info["states"]),
        "verifications": sorted(vulnerability_info["verifications"]),
        "closing_date": _format_group_date(date_info["closing_date"]),
        "start_closing_date": _format_group_date(date_info["start_closing_date"]),
        "finding_title": finding_title,
        "age": age,
        "last_report": last_report,
        "min_release_date": _format_group_date(date_info["min_release_date"]),
        "max_release_date": _format_group_date(date_info["max_release_date"]),
        "location": location,
        **severity_info,
    }

    success: bool = (
        await batch_dal.put_action(
            action=Action.REPORT,
            entity=group_name,
            subject=user_email,
            additional_info=additional_info,
            attempt_duration_seconds=7200,
            queue=IntegratesBatchQueue.LARGE
            if report_type in ["DATA", "PDF"]
            else IntegratesBatchQueue.SMALL,
        )
    ).success
    if not success:
        raise RequestedReportError()
    return success


def _validate_closing_date(*, closing_date: datetime | None) -> None:
    if closing_date is None:
        return
    tzn = pytz.timezone(TIME_ZONE)
    today = get_now()
    if closing_date.astimezone(tzn) > today:
        raise InvalidDate()


def _validate_days(field: int | None) -> None:
    if field and (field < MIN_VALUE or field > MAX_VALUE):
        raise InvalidReportFilter(f"Age value must be between {MIN_VALUE} and {MAX_VALUE}")


def _validate_min_severity(**kwargs: Decimal) -> None:
    min_severity: Decimal | None = kwargs.get("min_severity")
    if min_severity is not None:
        validate_min_acceptance_severity(Decimal(min_severity))


def _validate_max_severity(**kwargs: Decimal) -> None:
    max_severity: Decimal | None = kwargs.get("max_severity")
    if max_severity is not None:
        validate_max_acceptance_severity(Decimal(max_severity))


def _get_severity_value(field: float | None) -> Decimal | None:
    if field is not None:
        return Decimal(field).quantize(Decimal("0.1"))
    return None


def _get_finding_title(finding_title: str | None) -> str:
    if finding_title:
        is_valid_finding_title(finding_title)
        return finding_title[:3]
    return ""


async def _check_certificate_params(
    *,
    loaders: Dataloaders,
    report_type: str,
    group_name: str,
    user_email: str,
) -> None:
    if report_type == "CERT":
        group = await get_group(loaders, group_name)
        if not group.state.has_essential:
            raise RequestedReportError(
                expr="Group must have Machine enabled to generate Certificates",
            )
        if not (group.business_id and group.business_name and group.description):
            raise RequestedReportError(
                expr=(
                    "Lacking required group information to generate the"
                    " certificate. Make sure the businessId, businessName "
                    " and description fields of the Group are filled out"
                ),
            )
        user_role = await authz.get_group_level_role(loaders, user_email, group_name)
        if user_role not in ["customer_manager", "group_manager"]:
            raise RequestedReportError(
                expr="Only user or customer managers can request certificates",
            )


async def request_report(
    loaders: Dataloaders,
    user_email: str,
    group_name: str,
    verification_code: str,
    **kwargs: Any,
) -> bool:
    report_type: str = kwargs["report_type"]
    await _check_certificate_params(
        loaders=loaders,
        report_type=report_type,
        group_name=group_name,
        user_email=user_email,
    )
    states: set[VulnerabilityStateStatus] = (
        {
            VulnerabilityStateStatus[get_inverted_state_converted(state)]
            for state in kwargs["states"]
        }
        if kwargs.get("states")
        else set(
            [
                VulnerabilityStateStatus["SAFE"],
                VulnerabilityStateStatus["VULNERABLE"],
            ],
        )
    )
    treatments: set[TreatmentStatus] = (
        {
            TreatmentStatus[get_inverted_treatment_converted(treatment)]
            for treatment in kwargs["treatments"]
        }
        if kwargs.get("treatments")
        else set(TreatmentStatus)
    )
    verifications: set[VulnerabilityVerificationStatus] = (
        {VulnerabilityVerificationStatus[verification] for verification in kwargs["verifications"]}
        if kwargs.get("verifications")
        else set()
    )
    closing_date: datetime | None = kwargs.get("closing_date")
    start_closing_date: datetime | None = kwargs.get("start_closing_date")
    finding_title = _get_finding_title(kwargs.get("finding_title"))
    if closing_date is not None or start_closing_date is not None:
        _validate_closing_date(closing_date=closing_date)
        states = set(
            [
                VulnerabilityStateStatus["SAFE"],
            ],
        )
        treatments = set(TreatmentStatus)
        if verifications != set(
            [
                VulnerabilityVerificationStatus["VERIFIED"],
            ],
        ):
            verifications = set()
    date_info: dict[str, datetime | None] = {
        "closing_date": closing_date,
        "start_closing_date": start_closing_date,
        "min_release_date": kwargs.get("min_release_date"),
        "max_release_date": kwargs.get("max_release_date"),
    }
    severity_info: dict[str, Decimal | None] = {
        "min_severity": _get_severity_value(kwargs.get("min_severity")),
        "max_severity": _get_severity_value(kwargs.get("max_severity")),
    }
    _validate_closing_date(closing_date=kwargs.get("min_release_date"))
    _validate_days(kwargs.get("age"))
    _validate_min_severity(**kwargs)
    _validate_max_severity(**kwargs)
    _validate_days(kwargs.get("last_report"))
    _validate_closing_date(closing_date=kwargs.get("max_release_date"))
    _validate_closing_date(closing_date=kwargs.get("start_closing_date"))

    return await _get_url_group_report(
        loaders=loaders,
        report_type=report_type,
        user_email=user_email,
        group_name=group_name,
        vulnerability_info={
            "states": states,
            "treatments": treatments,
            "verifications": verifications,
        },
        finding_title=finding_title,
        age=kwargs.get("age"),
        severity_info=severity_info,
        last_report=kwargs.get("last_report"),
        location=kwargs.get("location", ""),
        verification_code=verification_code,
        date_info=date_info,
    )
