import csv
import logging
import logging.config
import os
import tempfile
from contextlib import suppress
from typing import NamedTuple

from integrates.authz import get_group_level_enforcer
from integrates.custom_exceptions import UnsanitizedInputFound
from integrates.custom_utils.datetime import get_as_str, get_now
from integrates.custom_utils.validations import validate_sanitized_csv_input
from integrates.dataloaders import Dataloaders
from integrates.db_model.roots.types import GitRoot, Root
from integrates.db_model.toe_inputs.types import RootToeInputsRequest, ToeInput
from integrates.settings.logger import LOGGING

logging.config.dictConfig(LOGGING)

LOGGER = logging.getLogger(__name__)


class SpecialFields(NamedTuple):
    attacked_at: bool
    attacked_by: bool
    be_present_until: bool
    first_attack_at: bool
    seen_first_time_by: bool


def get_valid_field(field: str | None) -> str:
    if field:
        with suppress(UnsanitizedInputFound):
            validate_sanitized_csv_input(field)
            return field
    return ""


def _get_row(
    *,
    toe_input: ToeInput,
    root_nickname_by_id: dict[str, str],
    special_fields: SpecialFields,
) -> list[str]:
    return [
        str(toe_input.state.be_present),
        get_valid_field(toe_input.component),
        get_valid_field(toe_input.entry_point),
        str(toe_input.state.has_vulnerabilities),
        toe_input.root_id,
        get_valid_field(root_nickname_by_id[toe_input.root_id.lower()]),
        *([str(toe_input.state.attacked_at)] if special_fields.attacked_at else []),
        *([get_valid_field(toe_input.state.attacked_by)] if special_fields.attacked_by else []),
        *([str(toe_input.state.be_present_until)] if special_fields.be_present_until else []),
        *([str(toe_input.state.first_attack_at)] if special_fields.first_attack_at else []),
        *([str(toe_input.state.seen_first_time_by)] if special_fields.seen_first_time_by else []),
    ]


async def get_group_toe_inputs_report(
    *,
    loaders: Dataloaders,
    group_name: str,
    email: str,
) -> str:
    enforcer = await get_group_level_enforcer(loaders, email)
    special_fields = SpecialFields(
        attacked_at=enforcer(
            group_name,
            "integrates_api_resolvers_toe_input_attacked_at_resolve",
        ),
        attacked_by=enforcer(
            group_name,
            "integrates_api_resolvers_toe_input_attacked_by_resolve",
        ),
        be_present_until=enforcer(
            group_name,
            "integrates_api_resolvers_toe_input_be_present_until_resolve",
        ),
        first_attack_at=enforcer(
            group_name,
            "integrates_api_resolvers_toe_input_first_attack_at_resolve",
        ),
        seen_first_time_by=enforcer(
            group_name,
            "integrates_api_resolvers_toe_input_seen_first_time_by_resolve",
        ),
    )
    rows: list[list[str]] = [
        [
            "bePresent",
            "component",
            "entryPoint",
            "hasVulnerabilities",
            "rootId",
            "rootNickname",
            *(["attackedAt"] if special_fields.attacked_at else []),
            *(["attackedBy"] if special_fields.attacked_by else []),
            *(["bePresentUntil"] if special_fields.be_present_until else []),
            *(["firstAttackAt"] if special_fields.first_attack_at else []),
            *(["seenFirstTimeBy"] if special_fields.seen_first_time_by else []),
        ],
    ]
    group_roots: list[Root] = await loaders.group_roots.load(group_name)
    root_nickname_by_id: dict[str, str] = {
        root.id.lower(): root.state.nickname for root in group_roots
    }

    roots_toe_inputs = await loaders.root_toe_inputs.load_many(
        [
            RootToeInputsRequest(group_name=group_name, root_id=root.id)
            for root in group_roots
            if isinstance(root, GitRoot)
        ],
    )

    for toe_input in [edge.node for connection in roots_toe_inputs for edge in connection.edges]:
        rows.append(
            _get_row(
                toe_input=toe_input,
                root_nickname_by_id=root_nickname_by_id,
                special_fields=special_fields,
            ),
        )

    with tempfile.NamedTemporaryFile() as temp_file:
        target = (
            temp_file.name
            + f"_toe-inputs-{group_name}-"
            + f'{get_as_str(get_now(), date_format="%Y-%m-%dT%H-%M-%S")}.csv'
        )

    with open(
        os.path.join(target),
        mode="w",
        encoding="utf-8",
    ) as csv_file:
        writer = csv.writer(
            csv_file,
            delimiter=",",
            quotechar='"',
            quoting=csv.QUOTE_MINIMAL,
        )
        writer.writerow(rows[0])
        writer.writerows(rows[1:])

    return csv_file.name
