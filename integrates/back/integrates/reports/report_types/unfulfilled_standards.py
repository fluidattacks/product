import csv
import logging
import logging.config
import os
from tempfile import NamedTemporaryFile, TemporaryDirectory

from integrates.class_types.types import Item
from integrates.context import BASE_URL, FI_ENVIRONMENT
from integrates.custom_exceptions import (
    GroupHasNotUnfulfilledStandards,
    InvalidParameter,
    InvalidStandardId,
)
from integrates.custom_utils.criteria import CRITERIA_COMPLIANCE
from integrates.custom_utils.datetime import get_as_str, get_now
from integrates.custom_utils.groups import get_group
from integrates.dataloaders import Dataloaders
from integrates.db_model.findings.types import Finding
from integrates.db_model.groups.types import GroupUnreliableIndicators, UnfulfilledStandard
from integrates.db_model.vulnerabilities.enums import VulnerabilityStateStatus
from integrates.organizations.utils import get_organization
from integrates.reports.secure_pdf import SecurePDF
from integrates.reports.standard_report import StandardReportCreator
from integrates.reports.types import CsvUnfulfilledStandardsInfo
from integrates.settings import LOGGING

logging.config.dictConfig(LOGGING)

LOGGER = logging.getLogger(__name__)
PREFIX = "integrates_report_"


async def generate_pdf_file(
    *,
    loaders: Dataloaders,
    group_name: str,
    stakeholder_email: str,
    unfulfilled_standards: set[str] | None = None,
) -> str:
    # The standard file is only available in English
    lang = "en"
    secure_pdf = SecurePDF()
    report_filename = ""
    with TemporaryDirectory(prefix=PREFIX, ignore_cleanup_errors=True) as tempdir:
        pdf_maker = StandardReportCreator(
            lang=lang,
            doctype="unfulfilled_standards",
            tempdir=tempdir,
            group=group_name,
            user=stakeholder_email,
        )
        await pdf_maker.unfulfilled_standards(
            loaders,
            group_name,
            lang,
            unfulfilled_standards=unfulfilled_standards,
        )
    report_filename = await secure_pdf.create_full(
        loaders,
        stakeholder_email,
        pdf_maker.out_name,
        group_name,
    )

    return report_filename


def format_report_row(finding: str, location: str, requirements: str, standard: str) -> Item:
    return {
        "finding": finding,
        "location": location,
        "requirements": requirements,
        "standard": standard,
    }


def format_requirements(requirements: list[str]) -> str:
    requirements_list = [f"{requirement_id}" for requirement_id in requirements]

    processed_requirements = ", ".join(requirements_list)

    return processed_requirements


async def get_csv_data_list(
    group_name: str,
    org_name: str,
    unfulfilled_data: CsvUnfulfilledStandardsInfo,
) -> list[Item]:
    base_url = "https://localhost:8001" if FI_ENVIRONMENT == "development" else BASE_URL

    data_list = sorted(
        [
            format_report_row(
                finding=finding.title,
                location=(
                    f"{base_url}/orgs/{org_name}/groups/{group_name}/vulns/"
                    + f"{finding.id}/locations/{vuln.id}"
                ),
                requirements=format_requirements(unfulfilled_standard.unfulfilled_requirements),
                standard=str(CRITERIA_COMPLIANCE[unfulfilled_standard.name]["title"]).upper(),
            )
            for unfulfilled_standard in unfulfilled_data.standards
            for finding in unfulfilled_data.findings
            for vuln in unfulfilled_data.vulns
            if vuln.finding_id == finding.id
        ],
        key=lambda item: item["standard"],
    )

    return data_list


def get_findings_by_requirements(
    findings: list[Finding],
    standards: list[UnfulfilledStandard],
) -> list[Finding]:
    unfulfilled_requirements = {
        requirement for standard in standards for requirement in standard.unfulfilled_requirements
    }

    filtered_findings = [
        finding
        for finding in findings
        if set(finding.unfulfilled_requirements) & unfulfilled_requirements
    ]

    return filtered_findings


async def get_unfulfilled_selected_standards(
    group_unfulfilled_standards: list[UnfulfilledStandard] | None,
    selected_standards: set[str],
) -> list[UnfulfilledStandard]:
    if len(selected_standards & CRITERIA_COMPLIANCE.keys()) != len(selected_standards):
        raise InvalidStandardId()

    if group_unfulfilled_standards is None:
        raise GroupHasNotUnfulfilledStandards()

    filtered_standards = [
        unfulfilled_standard
        for unfulfilled_standard in group_unfulfilled_standards
        if unfulfilled_standard.name in selected_standards
    ]

    return filtered_standards


async def get_unfulfilled_data(
    loaders: Dataloaders,
    group_findings: list[Finding],
    group_indicators: GroupUnreliableIndicators,
    selected_standards: set[str],
) -> CsvUnfulfilledStandardsInfo:
    unfulfilled_selected_standards = await get_unfulfilled_selected_standards(
        group_unfulfilled_standards=group_indicators.unfulfilled_standards,
        selected_standards=selected_standards,
    )
    unfulfilled_findings = get_findings_by_requirements(
        findings=group_findings,
        standards=unfulfilled_selected_standards,
    )
    group_vulns = await loaders.finding_vulnerabilities_released_nzr.load_many_chained(
        [finding.id for finding in unfulfilled_findings],
    )
    unfulfilled_vulns = [
        vuln for vuln in group_vulns if vuln.state.status == VulnerabilityStateStatus.VULNERABLE
    ]

    return CsvUnfulfilledStandardsInfo(
        standards=unfulfilled_selected_standards,
        findings=unfulfilled_findings,
        vulns=unfulfilled_vulns,
    )


async def generate_csv_file(
    *,
    loaders: Dataloaders,
    group_name: str,
    unfulfilled_standards: set[str] | None = None,
) -> str:
    if unfulfilled_standards is None:
        raise InvalidParameter("unfulfilledStandards")

    group = await get_group(loaders, group_name)
    group_org = await get_organization(loaders, group.organization_id)
    group_findings = await loaders.group_findings.load(group_name)
    group_indicators: GroupUnreliableIndicators = await loaders.group_unreliable_indicators.load(
        group_name,
    )
    unfulfilled_data = await get_unfulfilled_data(
        loaders=loaders,
        group_findings=group_findings,
        group_indicators=group_indicators,
        selected_standards=unfulfilled_standards,
    )
    fieldnames = ["standard", "location", "finding", "requirements"]
    current_date = get_as_str(get_now(), date_format="%Y-%m-%dT%H-%M-%S")
    csv_filename = f"unfulfilled-standards-{group_name}-{current_date}.csv"

    data_list = await get_csv_data_list(
        group_name=group_name,
        org_name=group_org.name,
        unfulfilled_data=unfulfilled_data,
    )

    with NamedTemporaryFile() as temp_file:
        target = temp_file.name + PREFIX + csv_filename

    with open(
        os.path.join(target),
        mode="w",
        encoding="utf-8",
    ) as csv_file:
        csv_writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
        csv_writer.writeheader()
        csv_writer.writerows(data_list)

    return csv_file.name
