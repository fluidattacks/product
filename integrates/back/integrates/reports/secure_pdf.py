import os

from aioextensions import in_process
from fpdf import FPDF
from pypdf import PdfReader, PdfWriter

from integrates.context import STARTDIR
from integrates.dataloaders import Dataloaders
from integrates.groups import domain as groups_domain


class PDF(FPDF):
    user = ""

    def set_user(self, user: str) -> None:
        self.user = user

    def footer(self) -> None:
        self.set_y(-15)
        self.set_font("Times", "", 12)
        self.cell(0, 10, f"Only for {self.user}", 0, 0, align="L")
        self.cell(0, 10, f"Page {self.page_no() - 1}", 0, 0, align="R")


class SecurePDF:
    """Add basic security to PDF."""

    footer_tpl = ""
    result_dir = ""
    secure_pdf_usermail = ""
    secure_pdf_username = ""
    watermark_tpl = ""

    def __init__(self) -> None:
        """Class constructor."""
        self.base = f"{STARTDIR}/integrates/back/integrates/reports"
        self.footer_tpl = os.path.join(self.base, "resources/themes/overlay_footer.pdf")
        self.result_dir = os.path.join(self.base, "tpls/results_pdf/")
        self.watermark_tpl = os.path.join(self.base, "resources/themes/watermark_integrates_en.pdf")

    async def create_full(
        self,
        loaders: Dataloaders,
        usermail: str,
        basic_pdf_name: str,
        group_name: str,
    ) -> str:
        """Execute the security process in a PDF."""
        self.secure_pdf_usermail = usermail
        self.secure_pdf_username = usermail.split("@")[0]
        if await groups_domain.exists(loaders, group_name.lower()):
            return os.path.join(self.result_dir, basic_pdf_name)

        water_pdf_name = await in_process(self.overlays, basic_pdf_name)
        return os.path.join(self.result_dir, water_pdf_name)

    def overlays(self, in_filename: str) -> str:
        """Add watermark and footer to all pages of a PDF."""
        pdf_foutname = f"water_{in_filename}"

        footer_pdf = PDF()
        footer_pdf.set_user(self.secure_pdf_usermail)
        footer_pdf.alias_nb_pages()
        output = PdfWriter()

        with (
            open(os.path.join(self.result_dir, in_filename), "rb") as input_file,
            open(self.watermark_tpl, "rb") as watermark_file,
            open(self.footer_tpl, "rb") as footer_file,
            open(os.path.join(self.result_dir, pdf_foutname), "wb") as output_stream,
        ):
            input_pdf = PdfReader(input_file)
            for i in range(1, len(input_pdf.pages)):
                footer_pdf.add_page()
            footer_pdf.add_page()
            footer_pdf.output(self.footer_tpl)

            overlay_watermark = PdfReader(watermark_file)
            overlay_footer = PdfReader(footer_file)

            for i, page in enumerate(input_pdf.pages):
                page.merge_page(overlay_watermark.pages[0])
                if i != 0:
                    page.merge_page(overlay_footer.pages[i])
                output.add_page(page)
            output.write(output_stream)

        return pdf_foutname
