from datetime import datetime
from decimal import Decimal

from integrates.custom_utils.findings import get_group_findings
from integrates.dataloaders import get_new_context
from integrates.db_model.findings.types import FindingEvidence, FindingEvidences
from integrates.db_model.vulnerabilities.enums import (
    VulnerabilityStateStatus,
    VulnerabilityType,
    VulnerabilityVerificationStatus,
)
from integrates.db_model.vulnerabilities.types import (
    VulnerabilityAdvisory,
    VulnerabilityUnreliableIndicators,
)
from integrates.reports import it_report
from integrates.reports.it_report import Filters, ITReport
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    FindingFaker,
    GitRootFaker,
    GitRootStateFaker,
    GroupFaker,
    OrganizationFaker,
    SeverityScoreFaker,
    VulnerabilityFaker,
    VulnerabilityStateFaker,
    VulnerabilityVerificationFaker,
)
from integrates.testing.mocks import Mock, mocks


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            groups=[GroupFaker(organization_id="org1")],
            roots=[
                GitRootFaker(
                    id="root1",
                    state=GitRootStateFaker(
                        gitignore=["bower_components/*", "node_modules/*"],
                        includes_health_check=True,
                        nickname="test_root",
                        url="https://gitlab.com/fluidattacks/universe",
                    ),
                ),
            ],
            findings=[
                FindingFaker(
                    id="3c475384-834c-47b0-ac71-a41a022e401c",
                    evidences=FindingEvidences(
                        evidence1=FindingEvidence(
                            description="evidence1",
                            url=("group1-3c475384-834c-47b0-ac71-a41a022e401c-evidence1"),
                            modified_date=datetime.fromisoformat("2020-11-19T13:37:10+00:00"),
                        ),
                    ),
                ),
            ],
            vulnerabilities=[
                VulnerabilityFaker(
                    id="vuln1",
                    finding_id="3c475384-834c-47b0-ac71-a41a022e401c",
                    root_id="root1",
                    state=VulnerabilityStateFaker(
                        specific="2222",
                        where="back/src/controller/user/package.json",
                        status=VulnerabilityStateStatus.SAFE,
                        advisories=VulnerabilityAdvisory(
                            package="test-package",
                            vulnerable_version="0.1.1",
                            cve=["CVE-2023-22491"],
                            epss=50,
                        ),
                    ),
                    type=VulnerabilityType.LINES,
                    verification=VulnerabilityVerificationFaker(
                        status=VulnerabilityVerificationStatus.VERIFIED
                    ),
                ),
                VulnerabilityFaker(
                    id="vuln2",
                    finding_id="3c475384-834c-47b0-ac71-a41a022e401c",
                    root_id="root1",
                    severity_score=SeverityScoreFaker(threat_score=Decimal("1.1")),
                    state=VulnerabilityStateFaker(
                        specific="9999",
                        where="192.168.1.20",
                        modified_date=datetime.fromisoformat("2020-04-08T00:45:15+00:00"),
                    ),
                    type=VulnerabilityType.PORTS,
                    unreliable_indicators=VulnerabilityUnreliableIndicators(
                        unreliable_report_date=datetime.fromisoformat("2020-04-08T00:45:15+00:00")
                    ),
                ),
                VulnerabilityFaker(
                    id="vuln3",
                    finding_id="3c475384-834c-47b0-ac71-a41a022e401c",
                    root_id="root1",
                    severity_score=SeverityScoreFaker(threat_score=Decimal("1.1")),
                    state=VulnerabilityStateFaker(
                        specific="1111",
                        where="192.168.1.20",
                        modified_date=datetime.fromisoformat("2020-04-08T00:45:15+00:00"),
                    ),
                    type=VulnerabilityType.PORTS,
                    unreliable_indicators=VulnerabilityUnreliableIndicators(
                        unreliable_report_date=datetime.fromisoformat("2020-04-08T00:45:15+00:00")
                    ),
                    verification=VulnerabilityVerificationFaker(
                        status=VulnerabilityVerificationStatus.REQUESTED
                    ),
                ),
            ],
        )
    ),
    others=[
        Mock(
            it_report,
            "get_filtered_findings",
            "async",
            [FindingFaker(id="3c475384-834c-47b0-ac71-a41a022e401c")],
        )  # Remove this mock once OpenSearch support is done
    ],
)
async def test_organization_vulnerabilities_filter() -> None:
    loaders = get_new_context()
    findings = await get_group_findings(group_name="test-group", loaders=loaders)
    report = ITReport(
        data=findings,
        group_name="test-group",
        loaders=loaders,
        filters=Filters(location="test_root"),
        generate_raw_data=True,
    )
    await report.generate_data()

    assert len(report.raw_data) == 2
    assert report.raw_data[0][4] == "Where"
    assert report.raw_data[1][4] == "back/src/controller/user/package.json"
    assert report.raw_data[1][3] == "vuln1"

    report = ITReport(
        data=findings,
        group_name="test-group",
        loaders=loaders,
        filters=Filters(
            min_release_date=datetime.fromisoformat("2020-01-01T00:45:15+00:00"),
            max_release_date=datetime.fromisoformat("2020-12-30T00:45:15+00:00"),
            verifications=set(
                [
                    VulnerabilityVerificationStatus.VERIFIED,
                    VulnerabilityVerificationStatus.NOT_REQUESTED,
                ]
            ),
        ),
        generate_raw_data=True,
    )
    await report.generate_data()

    assert len(report.raw_data) == 2
    assert report.raw_data[0][3] == "Vulnerability Id"
    assert report.raw_data[1][3] == "vuln2"
