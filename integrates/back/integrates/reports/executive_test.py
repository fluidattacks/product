from tempfile import TemporaryDirectory

from integrates.analytics import domain as analytics_domain
from integrates.dataloaders import get_new_context
from integrates.reports.executive import ExecutiveCreator
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    EventFaker,
    GitRootFaker,
    GitRootStateFaker,
    GroupFaker,
    OrganizationFaker,
)
from integrates.testing.mocks import Mock, mocks

GROUP_NAME = "group1"
ORG_ID = "org1"
ROOT_ID = "root_id"


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(
                    id=ORG_ID,
                ),
            ],
            events=[EventFaker(id="event1", group_name=GROUP_NAME, root_id=ROOT_ID)],
            groups=[
                GroupFaker(name=GROUP_NAME, organization_id=ORG_ID),
            ],
            roots=[
                GitRootFaker(
                    group_name=GROUP_NAME,
                    id=ROOT_ID,
                    state=GitRootStateFaker(nickname="testroot"),
                ),
            ],
        ),
    ),
    others=[
        Mock(
            analytics_domain,
            "get_document",
            "async",
            {
                "data": {
                    "x": "date",
                    "columns": [
                        ["date", "2020-09-07"],
                        ["Closed", "0.0"],
                        ["Accepted", "0.0"],
                        ["Reported", "1.0"],
                    ],
                    "colors": {"Closed": "#12B76A", "Accepted": "#FFD562", "Reported": "#BF0B1A"},
                    "types": {"Closed": "line", "Accepted": "line", "Reported": "line"},
                },
                "axis": {
                    "x": {
                        "tick": {
                            "multiline": False,
                            "rotate": -45,
                            "format": "%Y-%m-%d",
                            "count": 13,
                        },
                        "type": "timeseries",
                    },
                    "y": {
                        "min": 0,
                        "padding": {"bottom": 0, "top": 0},
                        "label": {"text": "CVSSF", "position": "inner-top"},
                        "tick": {"count": 5},
                        "max": 5.0,
                    },
                },
                "grid": {"x": {"show": False}, "y": {"show": True}},
                "legend": {"position": "bottom"},
                "point": {"focus": {"expand": {"enabled": True}}, "r": 5},
                "stackedBarChartYTickFormat": True,
                "hideYAxisLine": True,
                "height": 320,
                "width": 580,
            },
        )
    ],
)
async def test_executive_report() -> None:
    loaders = get_new_context()
    group_name = "group1"
    user_email = "user1"

    with TemporaryDirectory() as tempdir:
        executive = ExecutiveCreator(lang="en", tempdir=tempdir, group=group_name, user=user_email)
        await executive.executive(loaders)

        assert executive.executive_context["full_group"] == group_name
        assert (
            executive.executive_context["risk_over_time_graphic"]
            == f"{tempdir}/risk_over_time_{group_name}.png"
        )
        assert executive.executive_context["has_unresolved_events"]
        assert executive.executive_context["events"][0]["root"] == "testroot"
        assert executive.out_name.endswith(".pdf")
