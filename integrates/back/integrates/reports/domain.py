import calendar
import os
import tempfile
from datetime import UTC, datetime, timedelta
from operator import itemgetter

from aioextensions import collect, in_thread

from integrates.custom_utils.datetime import get_as_str
from integrates.custom_utils.findings import get_group_findings
from integrates.custom_utils.reports import sign_url, upload_report
from integrates.dataloaders import Dataloaders, get_new_context
from integrates.db_model.notifications.types import ReportStatus
from integrates.findings import domain as findings_domain
from integrates.notifications.reports import add_report_notification, update_report_notification
from integrates.organizations.logs_ztna import get_all_logs, get_email
from integrates.organizations.utils import get_organization
from integrates.reports.enums import ReportType
from integrates.reports.it_report import Filters
from integrates.reports.report_types.toe_inputs import get_group_toe_inputs_report
from integrates.reports.report_types.toe_lines import get_group_toe_lines_report
from integrates.reports.utils import write_file

from .report_types import certificate as cert_report
from .report_types import data as data_report
from .report_types import technical as technical_report
from .report_types import unfulfilled_standards as unfulfilled_standards_report


async def get_group_report_url(
    *,
    report_type: ReportType,
    group_name: str,
    user_email: str,
    filters: Filters,
) -> str | None:
    loaders: Dataloaders = get_new_context()
    group_findings = await get_group_findings(group_name=group_name, loaders=loaders)
    group_findings_score = await collect(
        findings_domain.get_max_open_severity_score_v4(loaders, finding.id)
        for finding in group_findings
    )
    findings_ord = tuple(
        finding
        for finding, _ in sorted(
            zip(group_findings, group_findings_score, strict=False),
            key=itemgetter(1),
            reverse=True,
        )
    )

    if report_type == ReportType.XLS:
        return await technical_report.generate_xls_file(
            loaders=loaders,
            findings=findings_ord,
            group_name=group_name,
            filters=filters,
        )

    if not (group := await loaders.group.load(group_name)):
        return None

    if report_type == ReportType.PDF:
        return await technical_report.generate_pdf_file(
            loaders=loaders,
            description=group.description,
            findings_ord=findings_ord,
            group_name=group_name,
            lang=str(group.language.value).lower(),
            user_email=user_email,
        )
    if report_type == ReportType.CERT:
        return await cert_report.generate_cert_file(
            loaders=loaders,
            description=group.description,
            findings_ord=findings_ord,
            group_name=group_name,
            lang=str(group.language.value).lower(),
            user_email=user_email,
        )
    if report_type == ReportType.DATA:
        return await data_report.generate(
            loaders=loaders,
            findings_ord=findings_ord,
            group_name=group_name,
            group_description=group.description,
            requester_email=user_email,
        )

    return None


async def generate_report(
    report_type: ReportType,
    loaders: Dataloaders,
    group_name: str,
    stakeholder_email: str,
    unfulfilled_standards: set[str] | None = None,
) -> str:
    if report_type == ReportType.CSV:
        return await unfulfilled_standards_report.generate_csv_file(
            loaders=loaders,
            group_name=group_name,
            unfulfilled_standards=unfulfilled_standards,
        )

    return await unfulfilled_standards_report.generate_pdf_file(
        loaders=loaders,
        group_name=group_name,
        stakeholder_email=stakeholder_email,
        unfulfilled_standards=unfulfilled_standards,
    )


async def get_unfulfilled_standard_report(
    group_name: str,
    stakeholder_email: str,
    report_type: ReportType,
    loaders: Dataloaders,
    unfulfilled_standards: set[str] | None = None,
) -> tuple[str, int]:
    filename = await generate_report(
        loaders=loaders,
        group_name=group_name,
        report_type=report_type,
        stakeholder_email=stakeholder_email,
        unfulfilled_standards=unfulfilled_standards,
    )

    s3_filename = await upload_report(filename)
    return s3_filename, os.path.getsize(filename)


def _validate_date(*, start: datetime, log_type: str, end: datetime, log: dict) -> bool:
    if log_type in {"http", "network"}:
        return start <= datetime.fromisoformat(log["Datetime"]) <= end

    return (
        start <= datetime.fromisoformat(log["SessionStartTime"]) <= end
        and start <= datetime.fromisoformat(log["SessionEndTime"]) <= end
    )


def _sort_date(
    *,
    log_type: str,
    rows: list,
) -> list:
    if log_type in {"http", "network"}:
        return sorted(
            rows,
            key=lambda row: row["Datetime"],
        )

    return sorted(
        rows,
        key=lambda row: row["SessionStartTime"],
    )


def _get_signed_ztna_logs_report_url(
    *,
    all_logs: list,
    email: str,
    log_type: str,
    start_date: datetime,
    end_date: datetime,
) -> list:
    first_row: list[str] = list(all_logs[0].keys())
    all_logs = _sort_date(log_type=log_type, rows=all_logs)
    rows = [
        [
            get_email(log[header], email) if header == "Email" else str(log[header])
            for header in first_row
            if _validate_date(start=start_date, log_type=log_type, end=end_date, log=log)
        ]
        for log in all_logs
    ]

    return [*[first_row], *rows]


async def get_signed_ztna_logs_report_url(
    *,
    loaders: Dataloaders,
    organization_id: str,
    email: str,
    log_type: str,
    date: datetime,
) -> str:
    organization = await get_organization(loaders, organization_id)
    notification = await add_report_notification(
        report_format="CSV",
        report_name=f"ZTNA logs ({organization.name})",
        user_email=email,
    )
    await update_report_notification(
        expiration_time=None,
        notification_id=notification.id,
        s3_file_path=None,
        size=None,
        status=ReportStatus.PROCESSING,
        user_email=email,
    )
    _, num_days = calendar.monthrange(date.year, date.month)
    start_date = datetime.combine(
        date.replace(day=1),
        datetime.min.time(),
    ).astimezone(tz=UTC)
    end_date = datetime.combine(
        date.replace(day=num_days),
        datetime.max.time().replace(microsecond=0),
    ).astimezone(tz=UTC)

    all_logs = await get_all_logs(
        organization_name=organization.name,
        start_date=start_date,
        end_date=end_date,
        log_type=log_type,
    )

    csv_filename = (
        f"ztna-{log_type}-{organization.name}-" f'{get_as_str(date, date_format="%Y-%m")}.csv'
    )
    with tempfile.TemporaryDirectory(
        prefix="integrates_report_",
        ignore_cleanup_errors=True,
    ) as directory:
        await in_thread(
            write_file,
            directory=directory,
            csv_filename=csv_filename,
            rows=[["No logs found"]]
            if len(all_logs) == 0
            else _get_signed_ztna_logs_report_url(
                all_logs=all_logs,
                email=email,
                log_type=log_type,
                start_date=start_date,
                end_date=end_date,
            ),
        )
        file_name = os.path.join(directory, csv_filename)
        stored_file_name = await upload_report(file_name)

        await update_report_notification(
            expiration_time=(datetime.now() + timedelta(days=7)),
            notification_id=notification.id,
            s3_file_path=f"reports/{stored_file_name}",
            size=os.path.getsize(file_name),
            status=ReportStatus.READY,
            user_email=email,
        )
        return await sign_url(stored_file_name, expire_seconds=300)


async def get_toe_lines_report(
    *,
    group_name: str,
    email: str,
) -> str:
    loaders: Dataloaders = get_new_context()

    return await get_group_toe_lines_report(
        loaders=loaders,
        group_name=group_name,
        email=email,
    )


async def get_toe_inputs_report(
    *,
    group_name: str,
    email: str,
) -> str:
    loaders: Dataloaders = get_new_context()

    return await get_group_toe_inputs_report(
        loaders=loaders,
        group_name=group_name,
        email=email,
    )
