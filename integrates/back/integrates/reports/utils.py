import asyncio
import csv
import logging
import logging.config
import os
from collections.abc import Iterable
from datetime import datetime
from decimal import Decimal

from integrates.context import BASE_URL, FI_ENVIRONMENT
from integrates.custom_exceptions import DocumentNotFound
from integrates.custom_utils import datetime as datetime_utils
from integrates.custom_utils import vulnerabilities as vulns_utils
from integrates.custom_utils.criteria import CRITERIA_REQUIREMENTS
from integrates.db_model.enums import TreatmentStatus
from integrates.db_model.findings.enums import FindingVerificationStatus
from integrates.db_model.findings.types import Finding, FindingVerification
from integrates.db_model.types import Treatment
from integrates.db_model.vulnerabilities.enums import VulnerabilityVerificationStatus
from integrates.db_model.vulnerabilities.types import Vulnerability, VulnerabilityVerification
from integrates.findings.domain.utils import get_report_days
from integrates.reports.types import PDFWordlistEn
from integrates.settings import LOGGING

logging.config.dictConfig(LOGGING)

# Constants
LOGGER = logging.getLogger(__name__)


def get_requirements(
    lang: str,
    unfulfilled_requirements: list[str],
) -> str:
    requirements = CRITERIA_REQUIREMENTS
    finding_requirements = [
        f'{key}. {item[lang]["title"]}.'
        for key, item in requirements.items()
        for requirement_key in unfulfilled_requirements
        if key == requirement_key
    ]
    return "\n".join(finding_requirements)


def write_file(*, directory: str, csv_filename: str, rows: list[list[str | None]]) -> None:
    with open(
        os.path.join(directory, csv_filename),
        mode="w",
        encoding="utf-8",
    ) as csv_file:
        writer = csv.writer(
            csv_file,
            delimiter=",",
            quotechar='"',
            quoting=csv.QUOTE_MINIMAL,
        )
        writer.writerow(rows[0])
        writer.writerows(rows[1:])


def get_first_treatment(
    treatments: tuple[Treatment, ...],
) -> Treatment | None:
    return next(
        (treatment for treatment in treatments if treatment.status != TreatmentStatus.UNTREATED),
        None,
    )


def format_treatment(treatment: TreatmentStatus) -> str:
    if treatment == TreatmentStatus.ACCEPTED_UNDEFINED:
        return "Permanently accepted"
    if treatment == TreatmentStatus.ACCEPTED:
        return "Temporarily accepted"
    if treatment == TreatmentStatus.UNTREATED:
        return "Untreated"
    return treatment.value.capitalize().replace("_", " ")


def get_reattack_requester(
    vuln: Vulnerability,
    historic_verification: tuple[FindingVerification, ...],
) -> str | None:
    reversed_historic_verification = tuple(reversed(historic_verification))
    for verification in reversed_historic_verification:
        if (
            verification.status == FindingVerificationStatus.REQUESTED
            and verification.vulnerability_ids is not None
            and vuln.id in verification.vulnerability_ids
        ):
            return verification.modified_by
    return None


def get_filtered_vulnerabilities_max_severity(
    finding_data: dict[str, Finding],
    max_severity: Decimal,
    vulnerabilities: Iterable[Vulnerability],
) -> list[Vulnerability]:
    return [
        vulnerability
        for vulnerability in vulnerabilities
        if vulns_utils.get_severity_threat_score(
            vulnerability,
            finding_data[vulnerability.finding_id],
        )
        <= max_severity
    ]


def get_filtered_vulnerabilities_min_severity(
    finding_data: dict[str, Finding],
    min_severity: Decimal,
    vulnerabilities: Iterable[Vulnerability],
) -> list[Vulnerability]:
    return [
        vulnerability
        for vulnerability in vulnerabilities
        if min_severity
        <= vulns_utils.get_severity_threat_score(
            vulnerability,
            finding_data[vulnerability.finding_id],
        )
    ]


def get_reattack_date(verifications: tuple[VulnerabilityVerification, ...]) -> datetime | None:
    modified_date = next(
        (
            verification.modified_date
            for verification in reversed(verifications)
            if verification.status == VulnerabilityVerificationStatus.REQUESTED
        ),
        None,
    )
    if modified_date:
        return datetime_utils.as_zone(modified_date)
    return None


async def call(*, program: str, command: list[str], group_name: str) -> None:
    process = await asyncio.create_subprocess_exec(
        program,
        *command,
        stderr=asyncio.subprocess.PIPE,
        stdout=asyncio.subprocess.PIPE,
        stdin=asyncio.subprocess.PIPE,
    )
    _, stderr = await process.communicate()
    if process.returncode != 0:
        LOGGER.error(
            "Error running command for report",
            extra={
                "extra": {
                    "error": stderr.decode(),
                    "group_name": group_name,
                    "command": command,
                },
            },
        )
        raise DocumentNotFound()


def get_metric_translation_v4(metric: str, cvss_vector_dict: dict[str, str]) -> str:
    metrics = {
        "AV": {
            "N": "Network",
            "A": "Adjacent",
            "L": "Local",
            "P": "Physical",
        },
        "AC": {
            "L": "Low",
            "H": "High",
        },
        "AT": {
            "N": "None",
            "P": "Present",
        },
        "PR": {
            "N": "None",
            "L": "Low",
            "H": "High",
        },
        "UI": {
            "N": "None",
            "P": "Passive",
            "A": "Active",
        },
        "VC": {
            "H": "High",
            "L": "Low",
            "N": "None",
        },
        "VI": {
            "H": "High",
            "L": "Low",
            "N": "None",
        },
        "VA": {
            "H": "High",
            "L": "Low",
            "N": "None",
        },
        "SC": {
            "H": "High",
            "L": "Low",
            "N": "None",
        },
        "SI": {
            "H": "High",
            "L": "Low",
            "N": "None",
        },
        "SA": {
            "H": "High",
            "L": "Low",
            "N": "None",
        },
        "E": {
            "X": "Not Defined",
            "U": "Unreported",
            "P": "POC",
            "A": "Attacked",
        },
    }
    metric_value = cvss_vector_dict.get(metric, "X")
    metric_descriptions = metrics.get(metric, {})

    return metric_descriptions.get(metric_value, "-")


def get_severity_level(score: Decimal) -> str:
    words = dict(zip(PDFWordlistEn.keys(), PDFWordlistEn.labels(), strict=False))
    if Decimal("9.0") <= score <= Decimal("10.0"):
        return words["crit_c"]
    if Decimal("7.0") <= score <= Decimal("8.9"):
        return words["crit_h"]
    if Decimal("4.0") <= score <= Decimal("6.9"):
        return words["crit_m"]

    return words["crit_l"]


def get_first_report_days(finding: Finding) -> int:
    unreliable_indicators = finding.unreliable_indicators
    return get_report_days(unreliable_indicators.oldest_vulnerability_report_date)


def get_last_report_days(finding: Finding) -> int:
    indicators = finding.unreliable_indicators
    return get_report_days(indicators.newest_vulnerability_report_date)


def get_metric_translation(metric: str, cvss_vector_dict: dict[str, str]) -> str:
    metrics = {
        "AV": {
            "N": "Network",
            "A": "Adjacent",
            "L": "Local",
            "P": "Physical",
        },
        "AC": {
            "L": "Low",
            "H": "High",
        },
        "PR": {
            "N": "None",
            "L": "Low",
            "H": "High",
        },
        "UI": {
            "N": "None",
            "R": "Required",
        },
        "S": {
            "U": "Unchanged",
            "C": "Changed",
        },
        "C": {
            "H": "High",
            "L": "Low",
            "N": "None",
        },
        "I": {
            "H": "High",
            "L": "Low",
            "N": "None",
        },
        "A": {
            "H": "High",
            "L": "Low",
            "N": "None",
        },
        "E": {
            "X": "Not Defined",
            "U": "Unproven",
            "P": "Proof-of-Concept",
            "F": "Functional",
            "H": "High",
        },
        "RL": {
            "X": "Not Defined",
            "O": "Official Fix",
            "T": "Temporary Fix",
            "W": "Workaround",
            "U": "Unavailable",
        },
        "RC": {
            "X": "Not Defined",
            "U": "Unknown",
            "R": "Reasonable",
            "C": "Confirmed",
        },
    }
    metric_value = cvss_vector_dict.get(metric, "X")
    metric_descriptions = metrics.get(metric, {})

    return metric_descriptions.get(metric_value, "-")


def get_report_date(vulnerability: Vulnerability) -> datetime:
    if vulnerability.unreliable_indicators.unreliable_report_date:
        return vulnerability.unreliable_indicators.unreliable_report_date

    return vulnerability.created_date


def get_findings_by_requirement(
    findings: list[Finding], findings_vulns: dict[str, int], requirement_id: str
) -> list[Finding]:
    return [
        finding
        for finding in findings
        if requirement_id in finding.unfulfilled_requirements and findings_vulns.get(finding.id, 0)
    ]


def get_base_url() -> str:
    return "https://localhost:8001" if FI_ENVIRONMENT == "development" else BASE_URL
