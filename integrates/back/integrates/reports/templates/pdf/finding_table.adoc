:page-background-image: image::{{finding_table_background}}[]

==== {{resume_table_title}}
[cols="<26%,^12%,^12%,^14%,^16%,^20%", options="header"]
|===
|{{finding_title}} |{{severity_title_v3}} |{{severity_title}} |{{cardinality_title}} |{{state_title}} |{{treatment_title}}
{% for finding in findings %}
    {{"| "+finding.title}}
    {{"| "+finding.severity_score_v3|string}}
    {{"| "+finding.severity_score|string}}
    {{"| "+finding.open_vulnerabilities|string}}
    {{"| "+finding.state+"\n"}}
    {{"| "+finding.treatment+"\n"}}
{%- endfor %}
|===
