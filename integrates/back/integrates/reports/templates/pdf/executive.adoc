= {{full_group}}
:lang:		{{lang}}
:author:	{{team}}
:email:		{{team_mail}}
:date: 	    {{report_date}}
:language:	python
:revnumber:	{{version}}
:revdate:	{{revdate}}
:revmark:	Versión inicial

//Primera pagina - Contenido
<<<
=== {{fluid_tpl['content_title']}}
{% for li in fluid_tpl['content_list'] %}
{{"==== "+li+"\n"}}
{%- endfor %}

//Segunda pagina - Objetivos
<<<
=== {{fluid_tpl['goals_title']}}
image::{{fluid_tpl['goals_img']}}[align=center]

//Tercera pagina - severity
<<<
=== {{severity_title}}
image::{{fluid_tpl['severity_img']}}[align=center]

//Cuarta pagina - Tabla de hallazgos
<<<
=== {{resume_table_title}}
.{{resume_table_title}}
|===
|{{finding_title}} |{{severity_title}} |{{cardinality_title}} |{{state_title}} |{{treatment_title}}
{% for finding in findings %}
    {{"| "+finding.title}}
    {{"| "+finding.severity_score|string}}
    {{"| "+finding.open_vulnerabilities|string}}
    {{"| "+finding.state+"\n"}}
    {{"| "+finding.treatment+"\n"}}
{%- endfor %}
|===

//Quinta pagina - Vista general
<<<
=== {{resume_page_title}}
image::{{main_pie_filename}}[width=300, align=center]
.{{resume_ttab_title}}
[cols="^,^,^", options="header"]
|===
|{{severity_title}}|{{finding_title}}s|{{resume_vuln_title}}
{% for row in main_tables['resume'] %}
  {% for col in row %}
    {{"| "+col|string}}
  {%- endfor %}

{%- endfor %}
|===
//Sexta pagina - Vista general
<<<
.{{resume_top_title}}
|===
|{{resume_vnum_title}}|{{severity_title}}|{{resume_vname_title}}
{% for row in main_tables['top'] %}
    {% for col in row %}
        {{"| "+col|string }}
    {%-  endfor %}
{%- endfor %}
|===

//7th en adelante - Resumen hallazgos
<<<
=== {{finding_section_title}}
{% for finding in findings %}
==== {{finding.title + "\n"}}
|===
|{{severity_title}}|{{cardinality_title}}|{{state_title}}
{{"|"+finding.severity_score|string+"|"+finding.open_vulnerabilities|string+"|"+finding.state}}
|===
===== {{description_title}}
{{finding.title + "\n"}}
===== {{threat_title}}
{{finding.threat + "\n"}}

===== {{attack_vector_title}}
{{finding.attack_vector_description  + "\n"}}

===== {{solution_title}}
{{finding.recommendation + "\n"}}
===== {{requisite_title}}
    {% for req in finding.requirements.split("\n") %}
        - {{req + "\n"}}
    {% endfor %}
<<<
==== {{evidence_title}}
{% for evidence in finding.evidence_set %}
{{evidence['explanation'] + "\n"}}
image::{{evidence['name']}}[align=center]{{"\n"}}
{% endfor %}
<<<
{%- endfor %}

<<<
{{fluid_tpl['footer_adoc']}}
