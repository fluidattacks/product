from types import SimpleNamespace
from typing import Any

import aiohttp
from opentelemetry import context, trace
from opentelemetry.instrumentation.aiohttp_client import AioHttpClientInstrumentor
from opentelemetry.instrumentation.utils import is_instrumentation_enabled
from opentelemetry.trace import SpanKind, get_tracer


class AioHTTPInstrumentor(AioHttpClientInstrumentor):
    """
    Pending contribution to upstream
    https://github.com/open-telemetry/opentelemetry-python-contrib/issues/3198
    """

    def _instrument(self, **kwargs: Any) -> None:
        tracer = get_tracer(__name__)

        async def on_dns_resolvehost_start(
            _session: aiohttp.ClientSession,
            trace_config_ctx: SimpleNamespace,
            params: aiohttp.TraceDnsResolveHostStartParams,
        ) -> None:
            if not is_instrumentation_enabled():
                trace_config_ctx.span = None
                return
            trace_config_ctx.span = tracer.start_span(
                "dns_resolvehost", kind=SpanKind.CLIENT, attributes={"host": params.host}
            )
            trace_config_ctx.token = context.attach(
                trace.set_span_in_context(trace_config_ctx.span)
            )

        async def on_dns_resolvehost_end(
            _session: aiohttp.ClientSession,
            trace_config_ctx: SimpleNamespace,
            _params: aiohttp.TraceDnsResolveHostEndParams,
        ) -> None:
            if trace_config_ctx.span is None:
                return
            context.detach(trace_config_ctx.token)
            trace_config_ctx.span.end()

        trace_config = aiohttp.TraceConfig()
        trace_config.on_dns_resolvehost_start.append(on_dns_resolvehost_start)
        trace_config.on_dns_resolvehost_end.append(on_dns_resolvehost_end)
        super()._instrument(**kwargs, trace_configs=[trace_config])
