from collections import (
    defaultdict,
)
from typing import (
    NamedTuple,
    TypedDict,
)

from starlette.requests import (
    Request,
)
from starlette.websockets import (
    WebSocket,
)

from integrates.db_model.azure_repositories.get import (
    OrganizationRepositoriesCommitsLoader,
    OrganizationRepositoriesLoader,
)
from integrates.db_model.compliance.get import (
    ComplianceUnreliableIndicatorsLoader,
)
from integrates.db_model.credentials.get import (
    CredentialsLoader,
    OrganizationCredentialsLoader,
    UserCredentialsLoader,
)
from integrates.db_model.event_comments.get import (
    EventCommentsLoader,
)
from integrates.db_model.events.get import (
    EventLoader,
    EventsHistoricStateLoader,
    GroupEventsLoader,
)
from integrates.db_model.finding_comments.get import (
    FindingCommentsLoader,
)
from integrates.db_model.findings.get import (
    FindingHistoricVerificationLoader,
    FindingLoader,
    GroupFindingsLoader,
    GroupFindingsNonDeletedLoader,
)
from integrates.db_model.forces.get import (
    ForcesExecutionLoader,
    GroupForcesExecutionsLoader,
)
from integrates.db_model.group_access.get import (
    DomainGroupsAccessLoader,
    GroupAccessLoader,
    GroupStakeholdersAccessLoader,
    StakeholderGroupsAccessLoader,
)
from integrates.db_model.groups.get import (
    GroupLoader,
    GroupUnreliableIndicatorsLoader,
    OrganizationGroupsLoader,
)
from integrates.db_model.hook.get import (
    GroupHookLoader,
    HookLoader,
)
from integrates.db_model.integration_repositories.get import (
    CredentialUnreliableRepositoriesLoader,
    OrganizationUnreliableRepositoriesConnectionLoader,
    OrganizationUnreliableRepositoriesLoader,
)
from integrates.db_model.jira_installations.get import (
    JiraInstallLoader,
    UserJiraInstallLoader,
)
from integrates.db_model.marketplace.get import (
    AWSMarketplaceSubscriptionLoader,
)
from integrates.db_model.notifications.get import (
    NotificationLoader,
    UserNotificationsLoader,
)
from integrates.db_model.organization_access.get import (
    OrganizationAccessLoader,
    OrganizationStakeholdersAccessLoader,
    StakeholderOrganizationsAccessLoader,
)
from integrates.db_model.organization_finding_policies.get import (
    OrganizationFindingPoliciesLoader,
    OrganizationFindingPolicyLoader,
)
from integrates.db_model.organizations.get import (
    OrganizationLoader,
    OrganizationUnreliableIndicatorsLoader,
)
from integrates.db_model.portfolios.get import (
    OrganizationPortfoliosLoader,
    PortfolioLoader,
)
from integrates.db_model.roots.get import (
    DockerImageLayersLoader,
    GitEnvironmentSecretsLoader,
    GroupDockerImagesLoader,
    GroupEnvironmentUrlsLoader,
    GroupEnvironmentUrlsNonDeletedLoader,
    GroupRootsLoader,
    OrganizationRootsLoader,
    RootDockerImageLoader,
    RootDockerImagesLoader,
    RootEnvironmentUrlLoader,
    RootEnvironmentUrlsLoader,
    RootEnvironmentUrlsNonDeletedLoader,
    RootHistoricStateLoader,
    RootLoader,
    RootSecretsLoader,
)
from integrates.db_model.stakeholders.get import (
    StakeholderLoader,
)
from integrates.db_model.toe_inputs.get import (
    GroupToeInputsEnrichedHistoricStateLoader,
    GroupToeInputsLoader,
    RootToeInputsLoader,
    ToeInputLoader,
)
from integrates.db_model.toe_lines.get import (
    GroupToeLinesEnrichedHistoricStateLoader,
    GroupToeLinesLoader,
    RootToeLinesLoader,
    ToeLinesLoader,
)
from integrates.db_model.toe_packages.get import (
    GroupToePackagesLoader,
    RootDockerImagePackagesLoader,
    RootToePackagesLoader,
    ToePackageLoader,
    ToePackageVulnerabilityLoader,
)
from integrates.db_model.toe_ports.get import (
    GroupToePortsLoader,
    GroupToePortStatesLoader,
    RootToePortsLoader,
    ToePortHistoricStateLoader,
    ToePortLoader,
)
from integrates.db_model.trials.get import (
    TrialLoader,
)
from integrates.db_model.vulnerabilities.get import (
    AssignedVulnerabilitiesLoader,
    EventVulnerabilitiesLoader,
    FindingVulnerabilitiesDraftConnectionLoader,
    FindingVulnerabilitiesLoader,
    FindingVulnerabilitiesNonDeletedLoader,
    FindingVulnerabilitiesReleasedNonZeroRiskConnectionLoader,
    FindingVulnerabilitiesReleasedNonZeroRiskLoader,
    FindingVulnerabilitiesReleasedZeroRiskConnectionLoader,
    FindingVulnerabilitiesReleasedZeroRiskLoader,
    FindingVulnerabilitiesToReattackConnectionLoader,
    GroupVulnerabilitiesLoader,
    RootVulnerabilitiesConnectionLoader,
    RootVulnerabilitiesLoader,
    VulnerabilityHashLoader,
    VulnerabilityHistoricStateLoader,
    VulnerabilityHistoricTreatmentConnectionLoader,
    VulnerabilityHistoricTreatmentLoader,
    VulnerabilityHistoricVerificationLoader,
    VulnerabilityHistoricZeroRiskLoader,
    VulnerabilityLoader,
    VulnerabilitySnippetLoader,
)


class Dataloaders(NamedTuple):
    aws_marketplace_subscriptions: AWSMarketplaceSubscriptionLoader
    compliance_unreliable_indicators: ComplianceUnreliableIndicatorsLoader
    credentials: CredentialsLoader
    credential_unreliable_repositories: CredentialUnreliableRepositoriesLoader
    docker_image: RootDockerImageLoader
    docker_image_layers: DockerImageLayersLoader
    domain_group_access: DomainGroupsAccessLoader
    environment_secrets: GitEnvironmentSecretsLoader
    environment_url: RootEnvironmentUrlLoader
    event_historic_state: EventsHistoricStateLoader
    event: EventLoader
    event_comments: EventCommentsLoader
    event_vulnerabilities_loader: EventVulnerabilitiesLoader
    finding: FindingLoader
    finding_comments: FindingCommentsLoader
    finding_historic_verification: FindingHistoricVerificationLoader
    finding_vulnerabilities: FindingVulnerabilitiesNonDeletedLoader
    finding_vulnerabilities_all: FindingVulnerabilitiesLoader
    finding_vulnerabilities_draft_c: FindingVulnerabilitiesDraftConnectionLoader
    finding_vulnerabilities_released_nzr: FindingVulnerabilitiesReleasedNonZeroRiskLoader
    finding_vulnerabilities_released_nzr_c: (
        FindingVulnerabilitiesReleasedNonZeroRiskConnectionLoader
    )
    finding_vulnerabilities_released_zr: FindingVulnerabilitiesReleasedZeroRiskLoader
    finding_vulnerabilities_released_zr_c: FindingVulnerabilitiesReleasedZeroRiskConnectionLoader
    finding_vulnerabilities_to_reattack_c: FindingVulnerabilitiesToReattackConnectionLoader
    forces_execution: ForcesExecutionLoader
    group: GroupLoader
    group_access: GroupAccessLoader
    group_docker_images: GroupDockerImagesLoader
    group_environment_urls: GroupEnvironmentUrlsNonDeletedLoader
    group_environment_urls_all: GroupEnvironmentUrlsLoader
    group_findings: GroupFindingsNonDeletedLoader
    group_findings_all: GroupFindingsLoader
    group_events: GroupEventsLoader
    group_forces_executions: GroupForcesExecutionsLoader
    group_hook: GroupHookLoader
    group_vulnerabilities: GroupVulnerabilitiesLoader
    group_roots: GroupRootsLoader
    group_toe_inputs: GroupToeInputsLoader
    group_toe_inputs_enriched_historic_state: GroupToeInputsEnrichedHistoricStateLoader
    group_toe_lines: GroupToeLinesLoader
    group_toe_lines_enriched_historic_state: GroupToeLinesEnrichedHistoricStateLoader
    group_toe_packages: GroupToePackagesLoader
    group_toe_ports: GroupToePortsLoader
    group_toe_port_states: GroupToePortStatesLoader
    group_unreliable_indicators: GroupUnreliableIndicatorsLoader
    group_stakeholders_access: GroupStakeholdersAccessLoader
    hook: HookLoader
    jira_install: JiraInstallLoader
    me_vulnerabilities: AssignedVulnerabilitiesLoader
    notification: NotificationLoader
    organization_access: OrganizationAccessLoader
    organization_credentials: OrganizationCredentialsLoader
    organization_groups: OrganizationGroupsLoader
    organization_finding_policy: OrganizationFindingPolicyLoader
    organization_finding_policies: OrganizationFindingPoliciesLoader
    organization_integration_repositories_commits: OrganizationRepositoriesCommitsLoader
    organization_integration_repositories: OrganizationRepositoriesLoader
    organization_unreliable_outside_repositories: OrganizationUnreliableRepositoriesLoader
    organization_unreliable_outside_repositories_c: (
        OrganizationUnreliableRepositoriesConnectionLoader
    )
    organization_portfolios: OrganizationPortfoliosLoader
    organization_roots: OrganizationRootsLoader
    organization_stakeholders_access: OrganizationStakeholdersAccessLoader
    organization: OrganizationLoader
    organization_unreliable_indicators: OrganizationUnreliableIndicatorsLoader
    portfolio: PortfolioLoader
    root: RootLoader
    root_docker_images: RootDockerImagesLoader
    root_environment_urls: RootEnvironmentUrlsNonDeletedLoader
    root_environment_urls_all: RootEnvironmentUrlsLoader
    root_historic_state: RootHistoricStateLoader
    root_toe_packages: RootToePackagesLoader
    toe_package: ToePackageLoader
    toe_package_vulnerabilities: ToePackageVulnerabilityLoader
    root_image_toe_packages: RootDockerImagePackagesLoader
    root_secrets: RootSecretsLoader
    root_toe_inputs: RootToeInputsLoader
    root_toe_lines: RootToeLinesLoader
    root_toe_ports: RootToePortsLoader
    root_vulnerabilities: RootVulnerabilitiesLoader
    root_vulnerabilities_connection: RootVulnerabilitiesConnectionLoader
    toe_input: ToeInputLoader
    toe_lines: ToeLinesLoader
    toe_port: ToePortLoader
    toe_port_historic_state: ToePortHistoricStateLoader
    trial: TrialLoader
    stakeholder: StakeholderLoader
    stakeholder_groups_access: StakeholderGroupsAccessLoader
    stakeholder_organizations_access: StakeholderOrganizationsAccessLoader
    user_credentials: UserCredentialsLoader
    user_jira_install: UserJiraInstallLoader
    user_notifications: UserNotificationsLoader
    vulnerability: VulnerabilityLoader
    vulnerability_by_hash: VulnerabilityHashLoader
    vulnerability_historic_state: VulnerabilityHistoricStateLoader
    vulnerability_historic_treatment: VulnerabilityHistoricTreatmentLoader
    vulnerability_historic_treatment_c: VulnerabilityHistoricTreatmentConnectionLoader
    vulnerability_historic_verification: VulnerabilityHistoricVerificationLoader
    vulnerability_historic_zero_risk: VulnerabilityHistoricZeroRiskLoader
    vulnerability_snippet: VulnerabilitySnippetLoader


class RootLoaders(TypedDict):
    root: RootLoader
    root_environment_urls: RootEnvironmentUrlsNonDeletedLoader
    root_environment_urls_all: RootEnvironmentUrlsLoader
    group_roots: GroupRootsLoader
    organization_roots: OrganizationRootsLoader
    root_docker_images: RootDockerImagesLoader
    root_historic_state: RootHistoricStateLoader
    root_toe_packages: RootToePackagesLoader
    root_secrets: RootSecretsLoader
    root_toe_lines: RootToeLinesLoader
    root_toe_ports: RootToePortsLoader
    root_vulnerabilities: RootVulnerabilitiesLoader
    root_vulnerabilities_connection: RootVulnerabilitiesConnectionLoader


class ToeInputLoaders(TypedDict):
    toe_input: ToeInputLoader
    root_toe_inputs: RootToeInputsLoader
    toe_lines: ToeLinesLoader
    toe_port: ToePortLoader
    toe_port_historic_state: ToePortHistoricStateLoader


class CredentialLoaders(TypedDict):
    credentials: CredentialsLoader
    credential_unreliable_repositories: CredentialUnreliableRepositoriesLoader
    user_credentials: UserCredentialsLoader


class EventLoaders(TypedDict):
    event: EventLoader
    group_events: GroupEventsLoader
    event_historic_state: EventsHistoricStateLoader
    event_comments: EventCommentsLoader
    event_vulnerabilities_loader: EventVulnerabilitiesLoader


class EnvironmentLoaders(TypedDict):
    environment_secrets: GitEnvironmentSecretsLoader
    environment_url: RootEnvironmentUrlLoader


class GroupLoaders(TypedDict):
    domain_group_access: DomainGroupsAccessLoader
    group: GroupLoader
    group_findings: GroupFindingsNonDeletedLoader
    group_findings_all: GroupFindingsLoader
    group_access: GroupAccessLoader
    group_environment_urls_all: GroupEnvironmentUrlsLoader
    group_environment_urls: GroupEnvironmentUrlsNonDeletedLoader
    group_stakeholders_access: GroupStakeholdersAccessLoader
    organization_groups: OrganizationGroupsLoader
    stakeholder_groups_access: StakeholderGroupsAccessLoader
    group_docker_images: GroupDockerImagesLoader
    group_forces_executions: GroupForcesExecutionsLoader
    group_hook: GroupHookLoader
    group_vulnerabilities: GroupVulnerabilitiesLoader
    group_toe_inputs: GroupToeInputsLoader
    group_toe_inputs_enriched_historic_state: GroupToeInputsEnrichedHistoricStateLoader
    group_toe_lines: GroupToeLinesLoader
    group_toe_lines_enriched_historic_state: GroupToeLinesEnrichedHistoricStateLoader
    group_toe_packages: GroupToePackagesLoader
    group_toe_ports: GroupToePortsLoader
    group_toe_port_states: GroupToePortStatesLoader
    group_unreliable_indicators: GroupUnreliableIndicatorsLoader


class PortfolioLoaders(TypedDict):
    portfolio: PortfolioLoader
    organization_portfolios: OrganizationPortfoliosLoader


class OrganizationLoaders(TypedDict):
    organization_access: OrganizationAccessLoader
    organization_stakeholders_access: OrganizationStakeholdersAccessLoader
    stakeholder_organizations_access: StakeholderOrganizationsAccessLoader
    organization_finding_policies: OrganizationFindingPoliciesLoader
    organization_finding_policy: OrganizationFindingPolicyLoader
    organization: OrganizationLoader
    organization_credentials: OrganizationCredentialsLoader
    organization_unreliable_indicators: OrganizationUnreliableIndicatorsLoader
    organization_integration_repositories_commits: OrganizationRepositoriesCommitsLoader
    organization_integration_repositories: OrganizationRepositoriesLoader
    organization_unreliable_outside_repositories: OrganizationUnreliableRepositoriesLoader
    organization_unreliable_outside_repositories_c: (
        OrganizationUnreliableRepositoriesConnectionLoader
    )


class FindingLoaders(TypedDict):
    finding_vulnerabilities: FindingVulnerabilitiesNonDeletedLoader
    finding_vulnerabilities_all: FindingVulnerabilitiesLoader
    finding_vulnerabilities_released_nzr: FindingVulnerabilitiesReleasedNonZeroRiskLoader
    finding_vulnerabilities_released_zr: FindingVulnerabilitiesReleasedZeroRiskLoader
    finding_comments: FindingCommentsLoader
    finding_historic_verification: FindingHistoricVerificationLoader
    finding: FindingLoader
    finding_vulnerabilities_draft_c: FindingVulnerabilitiesDraftConnectionLoader
    finding_vulnerabilities_released_nzr_c: (
        FindingVulnerabilitiesReleasedNonZeroRiskConnectionLoader
    )
    finding_vulnerabilities_released_zr_c: FindingVulnerabilitiesReleasedZeroRiskConnectionLoader
    finding_vulnerabilities_to_reattack_c: FindingVulnerabilitiesToReattackConnectionLoader


class VulnerabilityLoaders(TypedDict):
    vulnerability: VulnerabilityLoader
    me_vulnerabilities: AssignedVulnerabilitiesLoader
    vulnerability_by_hash: VulnerabilityHashLoader
    vulnerability_historic_state: VulnerabilityHistoricStateLoader
    vulnerability_historic_treatment: VulnerabilityHistoricTreatmentLoader
    vulnerability_historic_treatment_c: VulnerabilityHistoricTreatmentConnectionLoader
    vulnerability_historic_verification: VulnerabilityHistoricVerificationLoader
    vulnerability_historic_zero_risk: VulnerabilityHistoricZeroRiskLoader
    vulnerability_snippet: VulnerabilitySnippetLoader


def apply_context_attrs(
    context: Request | WebSocket,
    loaders: Dataloaders | None = None,
) -> Request | WebSocket:
    context.loaders = loaders if loaders else get_new_context()  # type: ignore[union-attr]
    store: defaultdict[str, None] = defaultdict(lambda: None)
    context.store = store  # type: ignore[union-attr]

    return context


def _create_root_loaders() -> RootLoaders:
    root_loader = RootLoader()
    root_environment_urls_loader = RootEnvironmentUrlsLoader()
    group_roots_loader = GroupRootsLoader(root_loader)
    organization_roots_loader = OrganizationRootsLoader(root_loader)
    return {
        "root": root_loader,
        "root_environment_urls_all": root_environment_urls_loader,
        "root_environment_urls": RootEnvironmentUrlsNonDeletedLoader(root_environment_urls_loader),
        "group_roots": group_roots_loader,
        "organization_roots": organization_roots_loader,
        "root_docker_images": RootDockerImagesLoader(),
        "root_historic_state": RootHistoricStateLoader(),
        "root_toe_packages": RootToePackagesLoader(),
        "root_secrets": RootSecretsLoader(),
        "root_toe_lines": RootToeLinesLoader(),
        "root_toe_ports": RootToePortsLoader(),
        "root_vulnerabilities": RootVulnerabilitiesLoader(),
        "root_vulnerabilities_connection": (RootVulnerabilitiesConnectionLoader()),
    }


def _create_toe_input_loaders() -> ToeInputLoaders:
    toe_input_loader = ToeInputLoader()
    return {
        "toe_input": toe_input_loader,
        "root_toe_inputs": RootToeInputsLoader(toe_input_loader),
        "toe_lines": ToeLinesLoader(),
        "toe_port": ToePortLoader(),
        "toe_port_historic_state": ToePortHistoricStateLoader(),
    }


def _create_event_loaders() -> EventLoaders:
    event_loader = EventLoader()
    group_events_loader = GroupEventsLoader(event_loader)
    return {
        "event": event_loader,
        "group_events": group_events_loader,
        "event_historic_state": EventsHistoricStateLoader(),
        "event_comments": EventCommentsLoader(),
        "event_vulnerabilities_loader": EventVulnerabilitiesLoader(),
    }


def _create_group_loaders(
    group_loader: GroupLoader,
) -> GroupLoaders:
    group_findings_all_loader = GroupFindingsLoader()
    group_findings_loader = GroupFindingsNonDeletedLoader(group_findings_all_loader)
    group_environment_urls_loader = GroupEnvironmentUrlsLoader()
    organization_groups_loader = OrganizationGroupsLoader(group_loader)
    group_access_loader = GroupAccessLoader()
    group_stakeholders_access_loader = GroupStakeholdersAccessLoader(group_access_loader)
    stakeholder_groups_access_loader = StakeholderGroupsAccessLoader(group_access_loader)
    return {
        "group": group_loader,
        "group_findings": group_findings_loader,
        "group_findings_all": group_findings_all_loader,
        "group_access": group_access_loader,
        "group_environment_urls_all": group_environment_urls_loader,
        "group_environment_urls": GroupEnvironmentUrlsNonDeletedLoader(
            group_environment_urls_loader,
        ),
        "group_stakeholders_access": group_stakeholders_access_loader,
        "organization_groups": organization_groups_loader,
        "stakeholder_groups_access": stakeholder_groups_access_loader,
        "group_docker_images": GroupDockerImagesLoader(),
        "group_forces_executions": GroupForcesExecutionsLoader(),
        "group_hook": GroupHookLoader(),
        "group_vulnerabilities": GroupVulnerabilitiesLoader(),
        "group_toe_inputs": GroupToeInputsLoader(),
        "group_toe_inputs_enriched_historic_state": (GroupToeInputsEnrichedHistoricStateLoader()),
        "group_toe_lines": GroupToeLinesLoader(),
        "group_toe_lines_enriched_historic_state": (GroupToeLinesEnrichedHistoricStateLoader()),
        "group_toe_packages": GroupToePackagesLoader(),
        "group_toe_ports": GroupToePortsLoader(),
        "group_toe_port_states": GroupToePortStatesLoader(),
        "group_unreliable_indicators": GroupUnreliableIndicatorsLoader(),
        "domain_group_access": DomainGroupsAccessLoader(),
    }


def _create_portfolio_loaders() -> PortfolioLoaders:
    portfolio_loader = PortfolioLoader()
    organization_portfolios_loader = OrganizationPortfoliosLoader(portfolio_loader)
    return {
        "portfolio": portfolio_loader,
        "organization_portfolios": organization_portfolios_loader,
    }


def _create_organization_loaders() -> OrganizationLoaders:
    organization_access_loader = OrganizationAccessLoader()
    organization_stakeholders_access_loader = OrganizationStakeholdersAccessLoader(
        organization_access_loader,
    )
    stakeholder_organizations_access_loader = StakeholderOrganizationsAccessLoader(
        organization_access_loader,
    )

    organization_finding_policy_loader = OrganizationFindingPolicyLoader()
    organization_finding_policies_loader = OrganizationFindingPoliciesLoader(
        organization_finding_policy_loader,
    )
    return {
        "organization_access": organization_access_loader,
        "organization_stakeholders_access": (organization_stakeholders_access_loader),
        "stakeholder_organizations_access": (stakeholder_organizations_access_loader),
        "organization_finding_policies": organization_finding_policies_loader,
        "organization_finding_policy": organization_finding_policy_loader,
        "organization": OrganizationLoader(),
        "organization_credentials": OrganizationCredentialsLoader(),
        "organization_unreliable_indicators": (OrganizationUnreliableIndicatorsLoader()),
        "organization_integration_repositories_commits": (OrganizationRepositoriesCommitsLoader()),
        "organization_integration_repositories": (OrganizationRepositoriesLoader()),
        "organization_unreliable_outside_repositories": (
            OrganizationUnreliableRepositoriesLoader()
        ),
        "organization_unreliable_outside_repositories_c": (
            OrganizationUnreliableRepositoriesConnectionLoader()
        ),
    }


def _create_finding_loaders(
    vulnerability_loader: VulnerabilityLoader,
) -> FindingLoaders:
    finding_vulnerabilities_loader = FindingVulnerabilitiesLoader(vulnerability_loader)
    finding_vulns_non_deleted_loader = FindingVulnerabilitiesNonDeletedLoader(
        finding_vulnerabilities_loader,
    )
    finding_vulnerabilities_released_nzr_loader = FindingVulnerabilitiesReleasedNonZeroRiskLoader(
        finding_vulns_non_deleted_loader,
    )
    finding_vulnerabilities_released_zr_loader = FindingVulnerabilitiesReleasedZeroRiskLoader(
        finding_vulns_non_deleted_loader,
    )
    return {
        "finding_vulnerabilities": finding_vulns_non_deleted_loader,
        "finding_vulnerabilities_all": finding_vulnerabilities_loader,
        "finding_vulnerabilities_released_nzr": (finding_vulnerabilities_released_nzr_loader),
        "finding_vulnerabilities_released_zr": (finding_vulnerabilities_released_zr_loader),
        "finding_comments": FindingCommentsLoader(),
        "finding_historic_verification": FindingHistoricVerificationLoader(),
        "finding": FindingLoader(),
        "finding_vulnerabilities_draft_c": (FindingVulnerabilitiesDraftConnectionLoader()),
        "finding_vulnerabilities_released_nzr_c": (
            FindingVulnerabilitiesReleasedNonZeroRiskConnectionLoader()
        ),
        "finding_vulnerabilities_released_zr_c": (
            FindingVulnerabilitiesReleasedZeroRiskConnectionLoader()
        ),
        "finding_vulnerabilities_to_reattack_c": (
            FindingVulnerabilitiesToReattackConnectionLoader()
        ),
    }


def _create_environment_loaders() -> EnvironmentLoaders:
    return {
        "environment_secrets": GitEnvironmentSecretsLoader(),
        "environment_url": RootEnvironmentUrlLoader(),
    }


def _create_credential_loaders() -> CredentialLoaders:
    return {
        "credentials": CredentialsLoader(),
        "credential_unreliable_repositories": (CredentialUnreliableRepositoriesLoader()),
        "user_credentials": UserCredentialsLoader(),
    }


def _create_vulnerability_loaders(
    vulnerability_loader: VulnerabilityLoader,
) -> VulnerabilityLoaders:
    return {
        "vulnerability": vulnerability_loader,
        "me_vulnerabilities": AssignedVulnerabilitiesLoader(),
        "vulnerability_by_hash": VulnerabilityHashLoader(),
        "vulnerability_historic_state": VulnerabilityHistoricStateLoader(),
        "vulnerability_historic_treatment": (VulnerabilityHistoricTreatmentLoader()),
        "vulnerability_historic_treatment_c": (VulnerabilityHistoricTreatmentConnectionLoader()),
        "vulnerability_historic_verification": (VulnerabilityHistoricVerificationLoader()),
        "vulnerability_historic_zero_risk": (VulnerabilityHistoricZeroRiskLoader()),
        "vulnerability_snippet": VulnerabilitySnippetLoader(),
    }


def get_new_context() -> Dataloaders:
    vulnerability_loader = VulnerabilityLoader()
    group_loader = GroupLoader()
    return Dataloaders(
        aws_marketplace_subscriptions=AWSMarketplaceSubscriptionLoader(),
        compliance_unreliable_indicators=(ComplianceUnreliableIndicatorsLoader()),
        docker_image=RootDockerImageLoader(),
        docker_image_layers=DockerImageLayersLoader(),
        toe_package=ToePackageLoader(),
        toe_package_vulnerabilities=ToePackageVulnerabilityLoader(),
        root_image_toe_packages=RootDockerImagePackagesLoader(),
        forces_execution=ForcesExecutionLoader(),
        hook=HookLoader(),
        jira_install=JiraInstallLoader(),
        notification=NotificationLoader(),
        user_jira_install=UserJiraInstallLoader(),
        user_notifications=UserNotificationsLoader(),
        stakeholder=StakeholderLoader(),
        trial=TrialLoader(),
        **_create_credential_loaders(),
        **_create_environment_loaders(),
        **_create_event_loaders(),
        **_create_finding_loaders(vulnerability_loader),
        **_create_group_loaders(group_loader),
        **_create_organization_loaders(),
        **_create_portfolio_loaders(),
        **_create_root_loaders(),
        **_create_toe_input_loaders(),
        **_create_vulnerability_loaders(vulnerability_loader),
    )
