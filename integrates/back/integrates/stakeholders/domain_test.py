from integrates.custom_exceptions import InvalidMobileNumber
from integrates.dataloaders import get_new_context
from integrates.db_model.stakeholders.types import StakeholderMetadataToUpdate, StakeholderPhone
from integrates.stakeholders.domain import (
    acknowledge_concurrent_session,
    update,
    update_mobile,
)
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import StakeholderFaker, StakeholderPhoneFaker
from integrates.testing.mocks import mocks
from integrates.testing.utils import parametrize, raises

ADMIN_EMAIL = "admin@gmail.com"
ORGANIZATION_MANAGER = "organization_manager@gmail.com"
CUSTOMER_MANAGER = "customer_manager@fluidattacks.com"
GROUP_NAME = "group1"
ORG_ID = "org1"


@parametrize(args=["email"], cases=[[ADMIN_EMAIL]])
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            stakeholders=[StakeholderFaker(email=ADMIN_EMAIL, is_concurrent_session=True)],
        ),
    ),
)
async def test_acknowledge_concurrent_session(email: str) -> None:
    await acknowledge_concurrent_session(email)
    loaders = get_new_context()
    stakeholder = await loaders.stakeholder.load(email)
    assert stakeholder
    assert stakeholder.is_concurrent_session is False


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            stakeholders=[
                StakeholderFaker(
                    email=ADMIN_EMAIL,
                    phone=StakeholderPhoneFaker(
                        country_code="CO",
                        calling_country_code="57",
                        national_number="3007654321",
                    ),
                )
            ],
        ),
    ),
)
async def test_update_stakeholder() -> None:
    loaders = get_new_context()
    new_phone: StakeholderPhone = StakeholderPhone(
        calling_country_code="1",
        national_number="1234323225",
        country_code="US",
    )
    await update(
        email=ADMIN_EMAIL,
        metadata=StakeholderMetadataToUpdate(
            phone=new_phone,
        ),
    )
    stakeholder = await loaders.stakeholder.load(ADMIN_EMAIL)
    assert stakeholder
    assert stakeholder.phone
    assert stakeholder.phone.national_number == new_phone.national_number


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            stakeholders=[
                StakeholderFaker(
                    email=ADMIN_EMAIL,
                    phone=StakeholderPhoneFaker(
                        country_code="US",
                        calling_country_code="1",
                        national_number="1111111111",
                    ),
                )
            ],
        ),
    ),
)
async def test_update_stakeholder_phone() -> None:
    new_phone: StakeholderPhone = StakeholderPhone(
        calling_country_code="1",
        national_number="1234323225",
        country_code="US",
    )
    await update_mobile(
        email=ADMIN_EMAIL,
        new_phone=new_phone,
        verification_code="12345",
    )
    loaders = get_new_context()
    stakeholder = await loaders.stakeholder.load(ADMIN_EMAIL)
    assert stakeholder
    assert stakeholder.phone
    assert stakeholder.phone.national_number == new_phone.national_number


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            stakeholders=[
                StakeholderFaker(
                    email=ADMIN_EMAIL,
                    phone=StakeholderPhoneFaker(
                        country_code="US",
                        calling_country_code="1",
                        national_number="1111111111",
                    ),
                )
            ],
        ),
    ),
)
async def test_update_stakeholder_phone_fail() -> None:
    new_phone: StakeholderPhone = StakeholderPhone(
        calling_country_code="+1",
        national_number="1234323225",
        country_code="US",
    )
    with raises(InvalidMobileNumber):
        await update_mobile(
            email=ADMIN_EMAIL,
            new_phone=new_phone,
            verification_code="1234",
        )


@parametrize(
    args=["email"],
    cases=[[ADMIN_EMAIL], [ORGANIZATION_MANAGER], [CUSTOMER_MANAGER]],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            stakeholders=[
                StakeholderFaker(
                    email=ADMIN_EMAIL,
                    phone=StakeholderPhoneFaker(
                        country_code="CO",
                        calling_country_code="57",
                        national_number="3007654321",
                    ),
                )
            ],
        ),
    ),
)
async def test_update_organization_stakeholder(email: str) -> None:
    loaders = get_new_context()
    new_phone: StakeholderPhone = StakeholderPhone(
        calling_country_code="1",
        national_number="1234323225",
        country_code="US",
    )
    await update(
        email=email,
        metadata=StakeholderMetadataToUpdate(
            phone=new_phone,
        ),
    )
    stakeholder = await loaders.stakeholder.load(email)
    assert stakeholder
    assert stakeholder.phone
    assert stakeholder.phone.national_number == new_phone.national_number
