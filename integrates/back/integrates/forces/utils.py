import re


def format_forces_email(group_name: str) -> str:
    return f"forces.{group_name}@fluidattacks.com"


def is_forces_user(email: str) -> bool:
    """Ensure that is an forces user."""
    pattern = r"forces.(?P<group>\w+)@fluidattacks.com"
    return bool(re.match(pattern, email))
