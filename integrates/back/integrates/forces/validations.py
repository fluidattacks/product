from integrates.custom_exceptions import (
    InvalidForcesStakeholder,
)
from integrates.forces.utils import (
    is_forces_user,
)


def validate_non_forces_user(user_email: str) -> None:
    if is_forces_user(user_email):
        raise InvalidForcesStakeholder()
