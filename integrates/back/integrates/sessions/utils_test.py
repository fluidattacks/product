import uuid
from datetime import datetime, timedelta
from time import time

from integrates.db_model.stakeholders.types import AccessTokens
from integrates.sessions.utils import (
    calculate_hash_token,
    is_valid_expiration_time,
    validate_hash_token,
)
from integrates.settings import SESSION_COOKIE_AGE
from integrates.testing.utils import parametrize


def test_validate_hash_token() -> None:
    # Arrange
    token = calculate_hash_token()
    access_token = AccessTokens(
        id=str(uuid.uuid4()),
        issued_at=int(datetime.utcnow().timestamp()),
        jti_hashed=token["jti_hashed"],
        salt=token["salt"],
    )
    different_token = calculate_hash_token()

    # Act
    validate_current = validate_hash_token([access_token], token["jti"])
    validate_different = validate_hash_token([access_token], different_token["jti"])

    # Assert
    assert validate_current == 0
    assert validate_different is None


@parametrize(
    args=["exp_time"],
    cases=[
        [int(time()) + SESSION_COOKIE_AGE],
    ],
)
def test_is_valid_expiration_time(exp_time: int) -> None:
    # Act
    result = is_valid_expiration_time(int(exp_time))

    # Assert
    assert result is True


@parametrize(
    args=["exp_time"],
    cases=[
        [int(time() + timedelta(weeks=27).total_seconds())],
    ],
)
def test_is_valid_expiration_time_fail(exp_time: int) -> None:
    # Act
    result = is_valid_expiration_time(int(exp_time))

    # Assert
    assert result is False
