import collections
import json
import logging
import logging.config
from contextlib import (
    suppress,
)
from datetime import (
    datetime,
    timedelta,
)
from typing import (
    Any,
)

import pytz
from aioextensions import (
    schedule,
)
from botocore.exceptions import (
    ClientError,
)
from jwcrypto.jwe import (
    JWE,
    InvalidJWEData,
)
from jwcrypto.jwk import (
    JWK,
)
from jwcrypto.jws import (
    InvalidJWSObject,
    InvalidJWSSignature,
)
from jwcrypto.jwt import (
    JWT,
    JWTExpired,
)
from starlette.requests import (
    Request,
)
from starlette.responses import (
    Response,
)

from integrates.audit import AuditContext, add_audit_context
from integrates.context import (
    FI_JWT_ENCRYPTION_KEY,
    FI_JWT_SECRET_API_ES512,
    FI_JWT_SECRET_API_RS512,
    FI_JWT_SECRET_ES512,
    FI_JWT_SECRET_RS512,
)
from integrates.custom_exceptions import (
    ExpiredToken,
    InvalidAlgorithm,
    InvalidAuthorization,
    SecureAccessException,
    StakeholderNotFound,
    UnavailabilityError,
)
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model import (
    stakeholders as stakeholders_model,
)
from integrates.db_model.stakeholders.types import (
    StakeholderMetadataToUpdate,
    StakeholderSessionToken,
    StateSessionType,
    TrustedDevice,
)
from integrates.sessions import (
    function,
)
from integrates.sessions import (
    utils as sessions_utils,
)
from integrates.sessions.types import (
    UserAccessInfo,
)
from integrates.settings import (
    JWT_ALGORITHM,
    JWT_COOKIE_NAME,
    JWT_COOKIE_SAMESITE,
    JWT_OTP_COOKIE_NAME,
    LOGGING,
    OTP_COOKIE_AGE,
    SESSION_COOKIE_AGE,
)

logging.config.dictConfig(LOGGING)

# Constants
LOGGER = logging.getLogger(__name__)


def encode_token(
    expiration_time: int | None,
    payload: dict[str, Any],
    subject: str,
    api: bool = False,
) -> str:
    """Encrypts the payload into a jwe token and returns its encoded version"""
    algorithm_mapping = {
        "RS512": (FI_JWT_SECRET_API_RS512, FI_JWT_SECRET_RS512),
        "ES512": (FI_JWT_SECRET_API_ES512, FI_JWT_SECRET_ES512),
    }
    secrets = algorithm_mapping.get(JWT_ALGORITHM)

    if secrets is not None:
        secret = secrets[0] if api else secrets[1]
    else:
        raise InvalidAlgorithm()

    jws_key = JWK.from_json(secret)
    jwe_key = JWK.from_json(FI_JWT_ENCRYPTION_KEY)

    default_claims = {"sub": subject}
    if expiration_time is not None:
        default_claims["exp"] = expiration_time  # type: ignore

    jwt_object = JWT(
        default_claims=default_claims,
        claims=JWE(
            algs=[
                "A256GCM",
                "A256GCMKW",
            ],
            plaintext=json.dumps(payload).encode("utf-8"),
            protected={
                "alg": "A256GCMKW",
                "enc": "A256GCM",
            },
            recipient=jwe_key,
        ).serialize(),
        header={"alg": JWT_ALGORITHM},
    )
    jwt_object.make_signed_token(jws_key)

    return jwt_object.serialize()


def decode_token(token: str) -> dict[str, Any]:
    """Decodes a jwt token and returns its decrypted payload"""
    try:
        jwt_token = JWT(jwt=token)
        secret = sessions_utils.get_secret(jwt_token)
        jws_key = JWK.from_json(secret)
        jwt_token.validate(jws_key)
    except (ValueError, TypeError, KeyError) as ex:
        raise InvalidAuthorization() from ex
    except (InvalidJWSObject, InvalidJWSSignature) as ex:
        raise InvalidAuthorization() from ex
    except JWTExpired:
        # Session expired
        raise InvalidAuthorization() from JWTExpired
    claims = json.loads(jwt_token.claims)
    decoded_payload = sessions_utils.decode_jwe(jwt_token.token.payload)

    # Old token
    if not claims.get("exp"):
        payload = sessions_utils.validate_expiration_time(decoded_payload)
        return payload

    default_claims = {"exp": claims["exp"], "sub": claims["sub"]}
    return dict(decoded_payload, **default_claims)


async def get_jwt_content(context: Any) -> dict[str, str]:
    context_store_key = function.get_id(get_jwt_content)
    store = get_request_store(context)

    # Within the context of one request we only need to process it once
    if context_store_key in store:
        store[context_store_key]["user_email"] = store[context_store_key]["user_email"].lower()
        return store[context_store_key]

    try:
        cookies = context.cookies
        cookie_token = cookies.get(JWT_COOKIE_NAME)
        header_token = context.headers.get("Authorization")
        token = header_token.split()[1] if header_token else cookie_token

        if not token:
            raise InvalidAuthorization()

        content = decode_token(token)
        email = content["user_email"]
        if content.get("sub") == "starlette_session":
            await verify_session_token(content, email)
        content["user_email"] = content["user_email"].lower()
        add_audit_context(AuditContext(author=content["user_email"]))
        store[context_store_key] = content
        return content
    except (ExpiredToken, InvalidJWEData) as exc:
        raise InvalidAuthorization() from exc
    except AttributeError as ex:
        LOGGER.exception(ex, extra={"extra": context})
        raise InvalidAuthorization() from ex
    except IndexError as exc:
        raise InvalidAuthorization() from exc


def get_jti_otp_token(request: Request) -> str:
    default_jti = sessions_utils.calculate_hash_token()["jti"]
    try:
        cookies = request.cookies
        cookie_token = cookies.get(JWT_OTP_COOKIE_NAME)

        if cookie_token is None:
            return default_jti

        jti = decode_token(cookie_token).get("jti", default_jti)
        return str(jti)
    except InvalidAuthorization:
        return default_jti


def get_request_store(context: Any) -> collections.defaultdict:
    """Returns customized store attribute of a Django/Starlette request"""
    return context.store if hasattr(context, "store") else context.state.store


async def create_session_token(user: UserAccessInfo) -> str:
    jti = sessions_utils.calculate_hash_token()["jti"]
    expiration_time = int((datetime.utcnow() + timedelta(seconds=SESSION_COOKIE_AGE)).timestamp())
    jwt_token: str = encode_token(
        expiration_time=expiration_time,
        payload={
            "user_email": user.user_email,
            "first_name": user.first_name,
            "last_name": user.last_name,
            "jti": jti,
        },
        subject="starlette_session",
    )
    await stakeholders_model.update_metadata(
        email=user.user_email,
        metadata=StakeholderMetadataToUpdate(
            session_token=StakeholderSessionToken(jti=jti, state=StateSessionType.IS_VALID),
        ),
    )

    return jwt_token


async def create_otp_token(trusted_device: TrustedDevice) -> str:
    expiration_time = int((datetime.utcnow() + timedelta(seconds=OTP_COOKIE_AGE)).timestamp())

    jwt_token: str = encode_token(
        expiration_time=expiration_time,
        payload={
            "device": trusted_device.device,
            "location": trusted_device.location,
            "browser": trusted_device.browser,
            "jti": trusted_device.otp_token_jti,
        },
        subject="otp_verification",
    )

    return jwt_token


def set_token_in_response(*, response: Response, token: str, key: str, max_age: int) -> Response:
    response.set_cookie(
        httponly=True,
        key=key,
        max_age=max_age,
        samesite=JWT_COOKIE_SAMESITE,
        secure=True,
        value=token,
    )
    return response


async def remove_session_token(content: dict[str, Any], email: str) -> None:
    """Revoke session token attribute"""
    await stakeholders_model.update_metadata(
        metadata=StakeholderMetadataToUpdate(
            session_token=StakeholderSessionToken(
                jti=content["jti"],
                state=StateSessionType.REVOKED,
            ),
        ),
        email=email,
    )


async def verify_session_token(content: dict[str, Any], email: str) -> None:
    loaders: Dataloaders = get_new_context()
    stakeholder = await loaders.stakeholder.load(email)
    if not stakeholder:
        raise InvalidAuthorization()

    if stakeholder.session_token:
        if stakeholder.session_token.state == StateSessionType.REVOKED:
            raise ExpiredToken()

        if stakeholder.session_token.jti != content["jti"]:
            raise ExpiredToken()
    else:
        raise InvalidAuthorization()


async def _has_valid_access_token(
    loaders: Dataloaders,
    email: str,
    context: dict[str, str],
    jti: str,
) -> bool:
    """Verify if has active access token and match."""
    stakeholder = await loaders.stakeholder.load(email)
    if not stakeholder:
        return False
    place = sessions_utils.validate_hash_token(stakeholder.access_tokens, jti)
    if context["value"] and stakeholder.access_tokens and place is not None:
        if email.endswith("@fluidattacks.com") and email.lower().startswith("forces."):
            return True
        with suppress(UnavailabilityError, ClientError):
            last_use = datetime.now(tz=pytz.timezone("UTC"))
            schedule(
                stakeholders_model.update_metadata(
                    metadata=StakeholderMetadataToUpdate(
                        access_tokens=[
                            token._replace(last_use=last_use) if index == place else token
                            for index, token in enumerate(stakeholder.access_tokens)
                        ],
                    ),
                    email=email,
                ),
            )
        return True
    return False


async def verify_jti(loaders: Dataloaders, email: str, context: dict[str, str], jti: str) -> None:
    result = await _has_valid_access_token(loaders, email, context, jti)
    if not result:
        LOGGER.info(
            "Hash token could not be validated.",
            extra={
                "extra": {
                    "email": email,
                    "action": context["action"],
                    "variables": context["variables"],
                },
            },
        )
        raise InvalidAuthorization()


async def create_session_web(request: Request, email: str) -> None:
    session_key: str = request.session["session_key"]

    # Check if there is a session already
    request.session["is_concurrent"] = bool(await get_session_key(email))

    # Proceeds overwriting the user session
    # This means that if a session did exist before, this one will
    # take place and the other will be removed
    return await stakeholders_model.update_metadata(
        metadata=StakeholderMetadataToUpdate(
            session_key=session_key,
        ),
        email=email,
    )


async def get_session_key(email: str) -> str | None:
    loaders: Dataloaders = get_new_context()
    stakeholder = await loaders.stakeholder.load(email)
    session_key = stakeholder.session_key if stakeholder else None
    return session_key


async def remove_session_key(email: str) -> None:
    await stakeholders_model.update_metadata(
        metadata=StakeholderMetadataToUpdate(
            session_key="",
        ),
        email=email,
    )


async def check_session_web_validity(request: Request, email: str) -> None:
    try:
        session_key: str = request.session["session_key"]

        # Check if the stakeholder has a concurrent session and in case they do
        # raise the concurrent session modal flag
        if request.session.get("is_concurrent"):
            request.session.pop("is_concurrent")
            await stakeholders_model.update_metadata(
                metadata=StakeholderMetadataToUpdate(
                    is_concurrent_session=True,
                ),
                email=email,
            )
        # Check if the stakeholder has an active session but it's different
        # than the one in the cookie
        if await get_session_key(email) == session_key:
            # Session and cookie are ok and up to date
            pass
        else:
            # Session or the cookie are expired, let's logout the stakeholder
            await remove_session_key(email)
            request.session.clear()
            raise SecureAccessException()
    except (KeyError, StakeholderNotFound):
        # Stakeholder do not even has an active session
        raise SecureAccessException() from None
