import logging
import logging.config
import re
import sys
import uuid
from collections.abc import AsyncIterator
from datetime import datetime
from decimal import Decimal
from operator import attrgetter

from aioextensions import collect, schedule
from jwcrypto.jwt import JWTExpired

from integrates import authz
from integrates.authz.model import get_organization_level_roles_model, get_user_level_roles_model
from integrates.authz.policy import get_user_level_role
from integrates.authz.validations import validate_role_fluid_reqs, validate_role_fluid_reqs_deco
from integrates.class_types.types import Item
from integrates.context import BASE_URL
from integrates.custom_exceptions import (
    CredentialInUse,
    InvalidAcceptanceDays,
    InvalidAcceptanceDaysUntilItBreaksRange,
    InvalidAcceptanceSeverity,
    InvalidAcceptanceSeverityRange,
    InvalidAuthorization,
    InvalidAWSRoleTrustPolicy,
    InvalidDaysUntilItBreaks,
    InvalidGitCredentials,
    InvalidInactivityPeriod,
    InvalidMaxAcceptanceDays,
    InvalidNumberAcceptances,
    InvalidOrganization,
    InvalidParameter,
    InvalidSeverity,
    InvalidVulnerabilityGracePeriod,
    OrganizationNotFound,
    StakeholderNotFound,
    StakeholderNotInOrganization,
    TrialRestriction,
)
from integrates.custom_utils import datetime as datetime_utils
from integrates.custom_utils import groups as groups_utils
from integrates.custom_utils import validations as validations_utils
from integrates.custom_utils.access import format_invitation_state
from integrates.custom_utils.stakeholders import get_stakeholder
from integrates.custom_utils.utils import validate_personal_email_invitation_deco
from integrates.custom_utils.validations import validate_aws_role
from integrates.custom_utils.validations_deco import (
    validate_email_address_deco,
    validate_include_lowercase_deco,
    validate_include_number_deco,
    validate_include_uppercase_deco,
    validate_length_deco,
    validate_space_field_deco,
    validate_start_letter_deco,
    validate_symbols_deco,
)
from integrates.dataloaders import Dataloaders, get_new_context
from integrates.db_model import credentials as credentials_model
from integrates.db_model import organization_access as org_access_model
from integrates.db_model import organization_finding_policies as policies_model
from integrates.db_model import organizations as orgs_model
from integrates.db_model import portfolios as portfolios_model
from integrates.db_model.constants import (
    DEFAULT_MAX_ACCEPTANCE_DAYS,
    DEFAULT_MAX_SEVERITY,
    DEFAULT_MIN_SEVERITY,
    DEFAULT_VULNERABILITY_GRACE_PERIOD,
    MIN_INACTIVITY_PERIOD,
    POLICIES_FORMATTED,
)
from integrates.db_model.credentials.types import (
    AWSRoleSecret,
    Credentials,
    CredentialsMetadataToUpdate,
    CredentialsRequest,
    CredentialsState,
    HttpsPatSecret,
    HttpsSecret,
    OauthAzureSecret,
    OauthBitbucketSecret,
    OauthGithubSecret,
    OauthGitlabSecret,
    SshSecret,
)
from integrates.db_model.enums import CredentialType
from integrates.db_model.groups.types import Group
from integrates.db_model.organization_access.enums import OrganizationInvitiationState
from integrates.db_model.organization_access.types import (
    OrganizationAccess,
    OrganizationAccessMetadataToUpdate,
    OrganizationAccessRequest,
    OrganizationAccessState,
    OrganizationInvitation,
)
from integrates.db_model.organizations.enums import OrganizationStateStatus
from integrates.db_model.organizations.types import (
    Organization,
    OrganizationBilling,
    OrganizationMetadataToUpdate,
    OrganizationState,
)
from integrates.db_model.organizations.utils import add_org_id_prefix
from integrates.db_model.roots.types import GitRoot
from integrates.db_model.stakeholders.types import Stakeholder, StakeholderMetadataToUpdate
from integrates.db_model.trials.types import TrialMetadataToUpdate
from integrates.db_model.types import Policies, PoliciesToUpdate
from integrates.group_access import domain as group_access_domain
from integrates.mailer import groups as groups_mail
from integrates.mailer import utils as mailer_utils
from integrates.mailer.types import AccessGrantedContext, UpdatedPoliciesContext
from integrates.organization_access import domain as orgs_access
from integrates.organizations import utils as orgs_utils
from integrates.organizations import validations as orgs_validations
from integrates.organizations.types import CredentialAttributesToAdd, CredentialAttributesToUpdate
from integrates.sessions import domain as sessions_domain
from integrates.settings import LOGGING
from integrates.stakeholders import domain as stakeholders_domain
from integrates.trials import domain as trials_domain

from .utils import get_organization, get_organization_roots

logging.config.dictConfig(LOGGING)

EMAIL_INTEGRATES = "integrates@fluidattacks.com"
LOGGER = logging.getLogger(__name__)
TRANSACTIONS_LOGGER: logging.Logger = logging.getLogger("transactional")


async def get_credentials(
    loaders: Dataloaders,
    credentials_id: str,
    organization_id: str,
) -> Credentials:
    credentials = await loaders.credentials.load(
        CredentialsRequest(
            id=credentials_id,
            organization_id=organization_id,
        ),
    )
    if credentials is None:
        raise InvalidGitCredentials()

    return credentials


async def add_credentials(
    loaders: Dataloaders,
    attributes: CredentialAttributesToAdd,
    organization_id: str,
    modified_by: str,
) -> str:
    if attributes.type is CredentialType.SSH:
        secret: HttpsSecret | HttpsPatSecret | SshSecret | AWSRoleSecret = SshSecret(
            key=orgs_utils.format_credentials_ssh_key(attributes.key or ""),
        )
    elif attributes.token is not None:
        token: str = attributes.token
        secret = create_pat_secret(token=token)
    elif attributes.type is CredentialType.AWSROLE:
        arn: str = attributes.arn or ""
        org = await loaders.organization.load(organization_id)
        secret = await create_aws_role_secret(
            arn=arn,
            external_id=org.state.aws_external_id if org else None,
        )
    else:
        user: str = attributes.user or ""
        password: str = attributes.password or ""
        secret = create_https_secret(user=user, password=password)

    credential = Credentials(
        id=(str(uuid.uuid4())),
        organization_id=organization_id,
        state=CredentialsState(
            modified_by=modified_by,
            modified_date=datetime_utils.get_utc_now(),
            name=attributes.name,
            type=attributes.type,
            is_pat=bool(attributes.is_pat),
            azure_organization=attributes.azure_organization,
            owner=modified_by,
        ),
        secret=secret,
    )
    await orgs_validations.validate_credentials_name_in_organization(
        loaders,
        credential.organization_id,
        credential.state.name,
    )
    await credentials_model.add(credential=credential)

    return credential.id


@validate_role_fluid_reqs_deco("email", "role")
async def add_stakeholder(
    loaders: Dataloaders,
    organization_id: str,
    email: str,
    role: str,
    modified_by: str,
) -> None:
    # Check for customer manager granting requirements
    await orgs_access.add_access(loaders, organization_id, email, role, modified_by)
    await _add_group_access(loaders, organization_id, email, role, modified_by)


@validate_role_fluid_reqs_deco("email", "role")
async def _add_group_access(
    loaders: Dataloaders,
    organization_id: str,
    email: str,
    role: str,
    modified_by: str,
) -> None:
    if role == "customer_manager":
        org_groups = await get_group_names(loaders, organization_id)
        await collect(
            group_access_domain.remove_access(loaders, email, group) for group in org_groups
        )
        await collect(
            group_access_domain.add_access(loaders, email, group, role, modified_by)
            for group in org_groups
        )
        loaders.group_access.clear_all()


@validate_email_address_deco("email")
async def add_without_group(
    *,
    email: str,
    role: str,
    is_register_after_complete: bool = False,
) -> None:
    await stakeholders_domain.update(
        email=email,
        metadata=StakeholderMetadataToUpdate(
            enrolled=False,
            is_registered=is_register_after_complete,
        ),
    )
    await authz.grant_user_level_role(email, role)


async def update_state(
    organization_id: str,
    organization_name: str,
    state: OrganizationState,
) -> None:
    await orgs_model.update_state(
        organization_id=organization_id,
        organization_name=organization_name,
        state=state,
    )


async def complete_register_for_organization_invitation(
    loaders: Dataloaders,
    organization_access: OrganizationAccess,
) -> None:
    invitation = organization_access.state.invitation
    if invitation and invitation.is_used:
        TRANSACTIONS_LOGGER.info(
            "Token already used",
            extra={
                "organization_access": organization_access,
                "task_name": "complete_organization_invitation",
            },
        )

    organization_id = organization_access.organization_id
    email = organization_access.email
    if invitation:
        role = invitation.role
        updated_invitation = invitation._replace(is_used=True)
    if role not in get_organization_level_roles_model():
        raise ValueError(f"Invalid role value: {role}")
    validate_role_fluid_reqs(email, role)

    organization = await loaders.organization.load(organization_id)
    if not organization or organization.state.status is OrganizationStateStatus.DELETED:
        raise OrganizationNotFound()
    loaders.organization_access.clear(
        OrganizationAccessRequest(organization_id=organization_id, email=email),
    )
    _organization_access = await loaders.organization_access.load(
        OrganizationAccessRequest(organization_id=organization_id, email=email),
    )
    if _organization_access is None:
        return

    await org_access_model.update_metadata(
        organization_id=organization_id,
        email=email,
        metadata=OrganizationAccessMetadataToUpdate(
            expiration_time=0,
            state=_organization_access.state._replace(
                modified_date=datetime_utils.get_utc_now(),
                modified_by=email,
                has_access=True,
                role=role,
                invitation=updated_invitation,
            ),
        ),
    )
    TRANSACTIONS_LOGGER.info(
        "User complete organization access",
        extra={"user_email": email, "organization_access": organization_access},
    )

    if not await get_user_level_role(loaders, email):
        await add_without_group(
            email=email,
            role=role if role in get_user_level_roles_model() else "user",
            is_register_after_complete=True,
        )

    loaders.stakeholder.clear(email)
    stakeholder = await loaders.stakeholder.load(email)
    if stakeholder and not stakeholder.enrolled:
        await stakeholders_domain.update(
            email=email, metadata=StakeholderMetadataToUpdate(enrolled=True)
        )

    await _add_group_access(loaders, organization_id, email, role, email)
    await complete_trial_after_confirm_invitation(
        loaders=loaders,
        email=email,
        modified_by=organization_access.state.modified_by,
        organization_id=organization_id,
    )
    loaders.organization_access.clear(
        OrganizationAccessRequest(organization_id=organization_id, email=email),
    )
    loaders.stakeholder.clear(email)


@validate_space_field_deco("user")
@validate_space_field_deco("password")
@validate_start_letter_deco("password")
@validate_include_number_deco("password")
@validate_include_lowercase_deco("password")
@validate_include_uppercase_deco("password")
@validate_length_deco("password", min_length=20, max_length=1800)
@validate_symbols_deco("password")
def create_https_secret(*, user: str, password: str) -> HttpsSecret:
    return HttpsSecret(user=user, password=password)


async def create_aws_role_secret(*, arn: str, external_id: str | None) -> AWSRoleSecret:
    if external_id:
        await validate_aws_role(arn, external_id)
        return AWSRoleSecret(arn=arn)
    raise InvalidAWSRoleTrustPolicy()


@validate_space_field_deco("token")
def create_pat_secret(*, token: str) -> HttpsPatSecret:
    return HttpsPatSecret(token=token)


async def get_access_by_url_token(
    loaders: Dataloaders,
    url_token: str,
) -> OrganizationAccess:
    try:
        token_content = sessions_domain.decode_token(url_token)
        organization_id: str = token_content["organization_id"]
        user_email: str = token_content["user_email"]
    except (KeyError, JWTExpired) as ex:
        raise InvalidAuthorization() from ex

    if (
        access := await loaders.organization_access.load(
            OrganizationAccessRequest(organization_id=organization_id, email=user_email),
        )
    ) is None:
        raise StakeholderNotInOrganization()

    return access


async def get_all_groups(
    loaders: Dataloaders,
) -> list[Group]:
    groups = []
    async for organization in iterate_organizations():
        org_groups = await loaders.organization_groups.load(organization.id)
        groups.extend(org_groups)

    return sorted(groups, key=attrgetter("name"))


async def get_all_group_names(
    loaders: Dataloaders,
) -> list[str]:
    groups = await get_all_groups(loaders)
    group_names = [group.name for group in groups]

    return sorted(group_names)


async def get_all_active_groups(
    loaders: Dataloaders,
    exclude_review_groups: bool = True,
) -> list[Group]:
    active_groups = []
    async for organization in iterate_organizations():
        org_groups = await loaders.organization_groups.load(organization.id)
        org_active_groups = groups_utils.exclude_review_groups(
            groups_utils.filter_active_groups(org_groups),
        )
        active_groups.extend(org_active_groups if exclude_review_groups else org_groups)

    return sorted(active_groups)


async def get_all_trial_groups(
    loaders: Dataloaders,
) -> list[Group]:
    trial_groups = []
    async for organization in iterate_organizations():
        org_groups = await loaders.organization_groups.load(organization.id)
        org_trial_groups = groups_utils.filter_trial_groups(org_groups)
        trial_groups.extend(org_trial_groups)

    return sorted(trial_groups)


async def get_all_active_group_names(
    loaders: Dataloaders,
    exclude_review_groups: bool = True,
) -> list[str]:
    active_groups = await get_all_active_groups(loaders, exclude_review_groups)
    active_group_names = [group.name for group in active_groups]

    return sorted(active_group_names)


async def get_all_deleted_groups(
    loaders: Dataloaders,
) -> list[Group]:
    deleted_groups: list[Group] = []
    async for organization in iterate_organizations():
        org_groups = await loaders.organization_groups.load(organization.id)
        org_deleted_groups = groups_utils.filter_deleted_groups(org_groups)
        deleted_groups.extend(org_deleted_groups)

    return sorted(deleted_groups)


async def get_group_names(loaders: Dataloaders, organization_id: str) -> list[str]:
    org_groups = await loaders.organization_groups.load(organization_id)

    return sorted([group.name for group in org_groups])


async def exists(loaders: Dataloaders, organization_name: str) -> bool:
    try:
        await get_organization(loaders, organization_name.lower().strip())
        return True
    except OrganizationNotFound:
        return False


@validate_length_deco("organization_name", min_length=4, max_length=20)
async def add_organization(
    *,
    loaders: Dataloaders,
    organization_name: str,
    email: str,
    country: str,
) -> Organization:
    if await exists(loaders, organization_name):
        raise InvalidOrganization(organization_name + " organization name is already taken")
    if not re.match(r"^[a-zA-Z]{4,20}$", organization_name):
        raise InvalidOrganization(organization_name + " organization name is invalid")

    in_trial = await trials_domain.in_trial(loaders, email)
    if in_trial and await loaders.stakeholder_organizations_access.load(email):
        raise TrialRestriction()

    modified_date = datetime_utils.get_utc_now()
    organization = Organization(
        created_by=email,
        created_date=modified_date,
        country=country,
        id=add_org_id_prefix(str(uuid.uuid4())),
        name=organization_name.lower().strip(),
        policies=Policies(
            modified_by=email,
            modified_date=modified_date,
        ),
        state=OrganizationState(
            aws_external_id=str(uuid.uuid4()),
            modified_by=email,
            modified_date=modified_date,
            status=OrganizationStateStatus.ACTIVE,
        ),
    )
    await orgs_model.add(organization=organization)
    if email:
        user_role = (
            "customer_manager"
            if validations_utils.is_fluid_staff(email)
            else "organization_manager"
        )
        await add_stakeholder(
            loaders=loaders,
            organization_id=organization.id,
            email=email,
            role=user_role,
            modified_by=email,
        )
    return organization


async def get_stakeholder_role(
    loaders: Dataloaders,
    email: str,
    is_registered: bool,
    organization_id: str,
) -> str:
    if (
        org_access := await loaders.organization_access.load(
            OrganizationAccessRequest(organization_id=organization_id, email=email),
        )
    ) is None:
        raise StakeholderNotInOrganization()

    invitation_state = format_invitation_state(org_access.state.invitation, is_registered)

    return (
        org_access.state.invitation.role
        if org_access.state.invitation and invitation_state == OrganizationInvitiationState.PENDING
        else await authz.get_organization_level_role(loaders, email, organization_id)
    )


async def get_stakeholders_emails(loaders: Dataloaders, organization_id: str) -> list[str]:
    stakeholders_access = await loaders.organization_stakeholders_access.load(organization_id)

    return [access.email for access in stakeholders_access]


async def get_stakeholders(
    loaders: Dataloaders,
    organization_id: str,
    user_email: str | None = None,
) -> list[Stakeholder]:
    emails = await get_stakeholders_emails(loaders, organization_id)
    stakeholders: list[Stakeholder] = []
    for email in emails:
        try:
            stakeholder = await get_stakeholder(loaders, email, user_email)
            stakeholders.append(stakeholder)
        except StakeholderNotFound:
            if not validations_utils.is_fluid_staff(email):
                stakeholders.append(Stakeholder(email=email))
    return stakeholders


async def has_group(loaders: Dataloaders, organization_id: str, group_name: str) -> bool:
    if group := await loaders.group.load(group_name):
        return group.organization_id == organization_id
    return False


@validate_email_address_deco("email")
@validate_role_fluid_reqs_deco("email", "role")
@validate_personal_email_invitation_deco("email", "modified_by")
async def invite_to_organization(
    *,
    loaders: Dataloaders,
    email: str,
    role: str,
    organization_name: str,
    modified_by: str,
) -> None:
    expiration_time = datetime_utils.get_as_epoch(datetime_utils.get_now_plus_delta(weeks=1))
    organization = await get_organization(loaders, organization_name)
    organization_id = organization.id
    url_token = sessions_domain.encode_token(
        expiration_time=expiration_time,
        payload={
            "organization_id": organization_id,
            "user_email": email,
        },
        subject="starlette_session",
    )
    await org_access_model.update_metadata(
        email=email,
        organization_id=organization_id,
        metadata=OrganizationAccessMetadataToUpdate(
            expiration_time=expiration_time,
            state=OrganizationAccessState(
                modified_date=datetime_utils.get_utc_now(),
                modified_by=modified_by,
                has_access=False,
                invitation=OrganizationInvitation(
                    is_used=False,
                    role=role,
                    url_token=url_token,
                ),
            ),
        ),
    )
    confirm_access_url = f"{BASE_URL}/confirm_access_organization/{url_token}"
    reject_access_url = f"{BASE_URL}/reject_access_organization/{url_token}"
    mail_to = [email]
    email_context: AccessGrantedContext = {
        "admin": email,
        "group": organization_name,
        "responsible": modified_by,
        "confirm_access_url": confirm_access_url,
        "reject_access_url": reject_access_url,
        "user_role": role.replace("_", " "),
    }
    schedule(groups_mail.send_mail_access_granted(loaders, mail_to, email_context))


async def iterate_organizations() -> AsyncIterator[Organization]:
    async for organization in orgs_model.iterate_organizations():
        # Exception: WF(AsyncIterator is subtype of iterator)
        yield organization


async def iterate_organizations_and_groups(
    loaders: Dataloaders,
) -> AsyncIterator[tuple[str, str, list[str]]]:
    """Yield (org_id, org_name, org_group_names) non-concurrently generated."""
    async for organization in iterate_organizations():
        # Exception: WF(AsyncIterator is subtype of iterator)
        yield organization.id, organization.name, await get_group_names(loaders, organization.id)


async def remove_credentials(
    loaders: Dataloaders,
    organization_id: str,
    credentials_id: str,
) -> None:
    organization = await get_organization(loaders, organization_id)
    organization_roots = await loaders.organization_roots.load(organization.name)
    if any(
        root
        for root in organization_roots
        if isinstance(root, GitRoot) and root.state.credential_id == credentials_id
    ):
        raise CredentialInUse()

    await credentials_model.remove(
        credential_id=credentials_id,
        organization_id=organization_id,
    )


async def remove_stakeholder_credentials(
    *,
    loaders: Dataloaders,
    email: str,
    organization_id: str,
) -> None:
    user_org_credentials: list[Credentials] = [
        credential
        for credential in await loaders.user_credentials.load(email)
        if credential.organization_id == organization_id
    ]
    roots = await get_organization_roots(loaders, organization_id)
    credentials_in_used: list[str] = [
        root.state.credential_id
        for root in roots
        if isinstance(root, GitRoot) and root.state.credential_id is not None
    ]
    if any(True for x in user_org_credentials if x.id in credentials_in_used):
        raise CredentialInUse()

    for credential in user_org_credentials:
        await credentials_model.remove(
            organization_id=organization_id,
            credential_id=credential.id,
        )


async def remove_access(
    organization_id: str,
    email: str,
    modified_by: str,
) -> None:
    loaders: Dataloaders = get_new_context()
    if not await orgs_access.has_access(loaders, organization_id, email):
        raise StakeholderNotInOrganization()

    await remove_stakeholder_credentials(
        loaders=loaders,
        email=email,
        organization_id=organization_id,
    )
    org_group_names = await get_group_names(loaders, organization_id)
    await collect(
        tuple(
            group_access_domain.remove_access(loaders, email, group) for group in org_group_names
        ),
        workers=1,
    )
    await org_access_model.remove(email=email, organization_id=organization_id)
    loaders.organization_access.clear(
        OrganizationAccessRequest(organization_id=organization_id, email=email),
    )
    stakeholder = await loaders.stakeholder.load(email)
    LOGGER.info(
        "Stakeholder removed from organization",
        extra={
            "extra": {
                "email": email,
                "modified_by": modified_by,
                "organization_id": organization_id,
                "last_login_date": stakeholder.last_login_date if stakeholder else "",
            },
        },
    )

    loaders = get_new_context()
    has_orgs = bool(await loaders.stakeholder_organizations_access.load(email))
    if not has_orgs:
        await stakeholders_domain.remove(email)


async def remove_organization(
    *,
    loaders: Dataloaders,
    organization_id: str,
    organization_name: str,
    modified_by: str,
    aws_external_id: str,
) -> None:
    await collect(
        remove_access(organization_id, email, modified_by)
        for email in await get_stakeholders_emails(loaders, organization_id)
    )
    # The state is updated to DELETED, prior to removal from db, as Streams
    # will archived this data for analytics purposes
    await update_state(
        organization_id=organization_id,
        organization_name=organization_name,
        state=OrganizationState(
            aws_external_id=aws_external_id,
            modified_by=modified_by,
            modified_date=datetime_utils.get_utc_now(),
            status=OrganizationStateStatus.DELETED,
            pending_deletion_date=None,
        ),
    )
    await credentials_model.remove_organization_credentials(organization_id=organization_id)
    await policies_model.remove_org_finding_policies(organization_name=organization_name)
    await portfolios_model.remove_organization_portfolios(organization_name=organization_name)
    await orgs_model.remove(
        organization_id=organization_id,
        organization_name=organization_name,
    )


async def reject_register_for_organization_invitation(
    organization_access: OrganizationAccess,
) -> None:
    invitation = organization_access.state.invitation
    if invitation and invitation.is_used:
        TRANSACTIONS_LOGGER.info(
            "Token already used",
            extra={
                "organization_access": organization_access,
                "task_name": "reject_organization_invitation",
            },
        )

    organization_id = organization_access.organization_id
    user_email = organization_access.email
    await remove_access(organization_id, user_email, user_email)


async def update_credentials(
    loaders: Dataloaders,
    attributes: CredentialAttributesToUpdate,
    credentials_id: str,
    organization_id: str,
    modified_by: str,
) -> None:
    current_credentials = await get_credentials(
        loaders=loaders,
        credentials_id=credentials_id,
        organization_id=organization_id,
    )
    credentials_type = attributes.type or current_credentials.state.type
    credentials_name = attributes.name or current_credentials.state.name
    await _validate_credentials_to_update(
        loaders=loaders,
        attributes=attributes,
        organization_id=organization_id,
        current_credentials=current_credentials,
        credentials_type=credentials_type,
        credentials_name=credentials_name,
    )

    update_owner = False
    secret: (
        HttpsSecret
        | HttpsPatSecret
        | OauthAzureSecret
        | OauthBitbucketSecret
        | OauthGithubSecret
        | OauthGitlabSecret
        | SshSecret
        | AWSRoleSecret
    )
    if credentials_type is CredentialType.HTTPS and attributes.token is not None:
        secret = HttpsPatSecret(token=attributes.token)
        update_owner = True
    elif (
        credentials_type is CredentialType.HTTPS
        and attributes.user is not None
        and attributes.password is not None
    ):
        user: str = attributes.user
        password: str = attributes.password or ""
        secret = create_https_secret(user=user, password=password)
        update_owner = True
    elif credentials_type is CredentialType.SSH and attributes.key is not None:
        secret = SshSecret(key=orgs_utils.format_credentials_ssh_key(attributes.key))
        update_owner = True
    else:
        secret = current_credentials.secret

    new_state = CredentialsState(
        modified_by=modified_by,
        modified_date=datetime_utils.get_utc_now(),
        name=credentials_name,
        is_pat=bool(attributes.is_pat),
        azure_organization=attributes.azure_organization,
        type=credentials_type,
        owner=modified_by if update_owner else current_credentials.state.owner,
    )
    await credentials_model.update_credentials(
        current_value=current_credentials,
        credential_id=credentials_id,
        organization_id=organization_id,
        state=new_state,
        metadata=CredentialsMetadataToUpdate(secret=secret),
    )


async def _validate_credentials_to_update(
    *,
    loaders: Dataloaders,
    attributes: CredentialAttributesToUpdate,
    organization_id: str,
    current_credentials: Credentials,
    credentials_type: CredentialType,
    credentials_name: str,
) -> None:
    if (
        credentials_type is CredentialType.HTTPS
        and attributes.password is not None
        and attributes.user is None
    ):
        raise InvalidParameter("user")
    if (
        credentials_type is CredentialType.HTTPS
        and attributes.user is not None
        and attributes.password is None
    ):
        raise InvalidParameter("password")
    if current_credentials.state.name != credentials_name:
        await orgs_validations.validate_credentials_name_in_organization(
            loaders,
            organization_id,
            credentials_name,
        )


async def update_organization_access(
    organization_id: str,
    email: str,
    metadata: OrganizationAccessMetadataToUpdate,
) -> None:
    return await org_access_model.update_metadata(
        email=email,
        metadata=metadata,
        organization_id=organization_id,
    )


async def update_invited_stakeholder(
    *,
    loaders: Dataloaders,
    email: str,
    invitation: OrganizationInvitation,
    organization_id: str,
    role: str,
    modified_by: str,
) -> None:
    invitation = invitation._replace(role=role)
    loaders.organization_access.clear(
        OrganizationAccessRequest(organization_id=organization_id, email=email),
    )
    org_access = await loaders.organization_access.load(
        OrganizationAccessRequest(organization_id=organization_id, email=email),
    )
    await update_organization_access(
        organization_id,
        email,
        OrganizationAccessMetadataToUpdate(
            state=OrganizationAccessState(
                modified_date=datetime_utils.get_utc_now(),
                modified_by=modified_by,
                invitation=invitation,
                has_access=False,
            )
            if org_access is None
            else org_access.state._replace(
                modified_date=datetime_utils.get_utc_now(),
                modified_by=modified_by,
                invitation=invitation,
            ),
        ),
    )


@validate_role_fluid_reqs_deco("user_email", "new_role")
async def update_stakeholder_role(
    *,
    loaders: Dataloaders,
    user_email: str,
    organization_id: str,
    organization_access: OrganizationAccess,
    new_role: str,
    modified_by: str,
) -> None:
    if organization_access.state.invitation:
        await update_invited_stakeholder(
            loaders=loaders,
            email=user_email,
            invitation=organization_access.state.invitation,
            organization_id=organization_id,
            role=new_role,
            modified_by=modified_by,
        )
        if organization_access.state.invitation.is_used:
            await authz.grant_organization_level_role(
                loaders,
                user_email,
                organization_id,
                new_role,
                modified_by,
            )
    else:
        # For some users without invitation
        await authz.grant_organization_level_role(
            loaders,
            user_email,
            organization_id,
            new_role,
            modified_by,
        )


async def update_pathfile(
    organization_id: str,
    organization_name: str,
    vulnerabilities_pathfile: str,
) -> None:
    await orgs_model.update_metadata(
        metadata=OrganizationMetadataToUpdate(
            vulnerabilities_pathfile=vulnerabilities_pathfile,
        ),
        organization_id=organization_id,
        organization_name=organization_name,
    )


async def add_billing_information(
    organization: Organization,
    billing_information: OrganizationBilling,
    country: str | None = None,
) -> None:
    new_billing_information = (
        organization.billing_information + [billing_information]
        if organization.billing_information
        else [billing_information]
    )
    await orgs_model.update_metadata(
        metadata=OrganizationMetadataToUpdate(
            billing_information=new_billing_information,
            country=country,
        ),
        organization_id=organization.id,
        organization_name=organization.name,
    )


def get_validated_policies_to_update(
    policies_to_update: PoliciesToUpdate,
) -> dict[str, Decimal]:
    validated_policies: dict[str, Decimal] = {}
    for attr, value in policies_to_update._asdict().items():
        if value is not None:
            value = (
                Decimal(value).quantize(Decimal("0.1"))
                if isinstance(value, float)
                else Decimal(value)
            )
            validated_policies[attr] = value
            validator_func = getattr(sys.modules[__name__], f"validate_{attr}")
            validator_func(value)

    return validated_policies


def validate_days_until_it_breaks_range(values: PoliciesToUpdate) -> bool:
    success: bool = True
    min_value = (
        values.vulnerability_grace_period
        if values.vulnerability_grace_period is not None
        else DEFAULT_VULNERABILITY_GRACE_PERIOD
    )
    max_value = values.days_until_it_breaks

    if min_value is not None and max_value is not None and (min_value >= max_value):
        raise InvalidAcceptanceDaysUntilItBreaksRange()
    return success


def validate_acceptance_days(values: PoliciesToUpdate) -> bool:
    max_acceptance_days = values.max_acceptance_days

    if max_acceptance_days and max_acceptance_days > DEFAULT_MAX_ACCEPTANCE_DAYS:
        raise InvalidMaxAcceptanceDays()
    return True


async def update_policies(
    loaders: Dataloaders,
    organization_id: str,
    organization_name: str,
    user_email: str,
    policies_to_update: PoliciesToUpdate,
) -> None:
    validated_policies = get_validated_policies_to_update(policies_to_update)
    validate_acceptance_days(policies_to_update)
    await validate_acceptance_severity_range(loaders, organization_id, policies_to_update)
    validate_days_until_it_breaks_range(policies_to_update)
    if validated_policies:
        today = datetime_utils.get_utc_now()
        await orgs_model.update_policies(
            modified_by=user_email,
            modified_date=today,
            organization_id=organization_id,
            organization_name=organization_name,
            policies=policies_to_update,
        )
        schedule(
            send_mail_policies(
                loaders=loaders,
                new_policies=policies_to_update._asdict(),
                organization_id=organization_id,
                organization_name=organization_name,
                responsible=user_email,
                modified_date=today,
            ),
        )


async def send_mail_policies(
    *,
    loaders: Dataloaders,
    new_policies: dict[str, Decimal],
    organization_id: str,
    organization_name: str,
    responsible: str,
    modified_date: datetime,
) -> None:
    organization_data = await get_organization(loaders, organization_id)
    policies_content: Item = {}
    for key, val in new_policies.items():
        old_value = organization_data.policies._asdict().get(key)
        if val is not None and val != old_value:
            policies_content[POLICIES_FORMATTED[key]] = {
                "from": old_value,
                "to": val,
            }
    if not policies_content:
        return

    email_context: UpdatedPoliciesContext = {
        "entity_name": organization_name,
        "entity_type": "organization",
        "policies_link": f"{BASE_URL}/orgs/{organization_name}/policies",
        "policies_content": policies_content,
        "responsible": responsible,
        "date": datetime_utils.get_as_str(modified_date),
    }
    members_emails = await mailer_utils.get_organization_emails_by_notification(
        loaders=loaders,
        organization_id=organization_id,
        notification="updated_policies",
    )
    await groups_mail.send_mail_updated_policies(
        loaders=loaders,
        email_to=members_emails,
        context=email_context,
    )


async def validate_acceptance_severity_range(
    loaders: Dataloaders,
    organization_id: str,
    values: PoliciesToUpdate,
) -> bool:
    success: bool = True
    organization_data = await get_organization(loaders, organization_id)
    min_acceptance_severity = organization_data.policies.min_acceptance_severity
    max_acceptance_severity = organization_data.policies.max_acceptance_severity
    min_value = (
        values.min_acceptance_severity
        if values.min_acceptance_severity is not None
        else min_acceptance_severity
    )
    max_value = (
        values.max_acceptance_severity
        if values.max_acceptance_severity is not None
        else max_acceptance_severity
    )
    if min_value is not None and max_value is not None and (min_value > max_value):
        raise InvalidAcceptanceSeverityRange()
    return success


def validate_inactivity_period(value: int) -> bool:
    success: bool = True
    if value < MIN_INACTIVITY_PERIOD:
        raise InvalidInactivityPeriod()
    return success


def validate_max_acceptance_days(value: int) -> bool:
    success: bool = True
    if value < 0:
        raise InvalidAcceptanceDays()
    return success


def validate_max_acceptance_severity(value: Decimal) -> bool:
    success: bool = True
    if not DEFAULT_MIN_SEVERITY <= value <= DEFAULT_MAX_SEVERITY:
        raise InvalidAcceptanceSeverity()
    return success


def validate_days_until_it_breaks(value: int | None) -> bool:
    success = True
    if value is not None and value <= 0:
        raise InvalidDaysUntilItBreaks()
    return success


def validate_max_number_acceptances(value: int) -> bool:
    success: bool = True
    if value < 0:
        raise InvalidNumberAcceptances()
    return success


def validate_min_acceptance_severity(value: Decimal) -> bool:
    success: bool = True
    if not DEFAULT_MIN_SEVERITY <= value <= DEFAULT_MAX_SEVERITY:
        raise InvalidAcceptanceSeverity()
    return success


def validate_min_breaking_severity(value: Decimal) -> bool:
    success: bool = True
    try:
        float(value)
    except ValueError as error:
        raise InvalidSeverity([DEFAULT_MIN_SEVERITY, DEFAULT_MAX_SEVERITY]) from error
    if not DEFAULT_MIN_SEVERITY <= value <= DEFAULT_MAX_SEVERITY:
        raise InvalidSeverity([DEFAULT_MIN_SEVERITY, DEFAULT_MAX_SEVERITY])
    return success


def validate_vulnerability_grace_period(value: int) -> bool:
    success: bool = True
    if value < 0:
        raise InvalidVulnerabilityGracePeriod()
    return success


@validate_space_field_deco("azure_organization")
def verify_azure_org(azure_organization: str | None) -> None:
    if not azure_organization:
        raise InvalidParameter("azure_organization")


async def complete_trial_after_confirm_invitation(
    *,
    loaders: Dataloaders,
    email: str,
    modified_by: str,
    organization_id: str,
) -> None:
    loaders.trial.clear(email)
    loaders.stakeholder.clear(modified_by)
    if (
        not await trials_domain.has_active_trial(email, loaders)
        and (trial := await loaders.trial.load(email))
        and not trial.completed
        and not await trials_domain.in_trial(
            loaders,
            modified_by,
            await loaders.organization.load(organization_id),
        )
    ):
        await trials_domain.update_metadata(
            email=email,
            metadata=TrialMetadataToUpdate(completed=True),
        )
