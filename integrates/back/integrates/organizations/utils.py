import base64
import binascii

from integrates.custom_exceptions import (
    InvalidBase64SshKey,
    OrganizationNotFound,
)
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.credentials.types import (
    AWSRoleSecret,
    HttpsPatSecret,
    HttpsSecret,
    SshSecret,
)
from integrates.db_model.enums import (
    CredentialType,
)
from integrates.db_model.organizations.types import (
    Organization,
)
from integrates.db_model.roots.types import (
    Root,
)


def format_credentials_ssh_key(ssh_key: str) -> str:
    try:
        raw_ssh_key: str = base64.b64decode(ssh_key, validate=True).decode()
    except binascii.Error as exc:
        raise InvalidBase64SshKey() from exc

    if not raw_ssh_key.endswith("\n"):
        raw_ssh_key += "\n"
    encoded_ssh_key = base64.b64encode(raw_ssh_key.encode()).decode()

    return encoded_ssh_key


def format_credentials_secret_type(
    item: dict[str, str],
) -> HttpsSecret | HttpsPatSecret | SshSecret | AWSRoleSecret:
    credential_type = CredentialType(item["type"])
    if credential_type is CredentialType.HTTPS:
        if item.get("token"):
            return HttpsPatSecret(token=item["token"])
        return HttpsSecret(user=item["user"], password=item["password"])
    if credential_type is CredentialType.AWSROLE:
        return AWSRoleSecret(arn=item["arn"])
    return SshSecret(key=format_credentials_ssh_key(item["key"]))


async def get_organization(loaders: Dataloaders, organization_key: str) -> Organization:
    """
    Returns the organization with the given id.

    Raises:
        OrganizationNotFound: if the organization does not exist.

    """
    organization = await loaders.organization.load(organization_key)
    if not organization:
        raise OrganizationNotFound()

    return organization


async def get_organization_roots(loaders: Dataloaders, organization_key: str) -> list[Root]:
    organization = await get_organization(loaders, organization_key)
    roots = await loaders.organization_roots.load(organization.name)
    return roots
