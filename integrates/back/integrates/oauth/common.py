import functools
import logging
import logging.config
from asyncio import Lock
from collections.abc import Callable
from typing import Any, TypeVar, cast

from fluidattacks_core.git import InvalidParameter as GitInvalidParameter
from fluidattacks_core.git import ls_remote

from integrates.custom_exceptions import (
    InvalidAuthorization,
    InvalidGitCredentials,
)
from integrates.dataloaders import Dataloaders
from integrates.db_model.credentials.types import (
    Credentials,
    OauthAzureSecret,
    OauthBitbucketSecret,
    OauthGithubSecret,
    OauthGitlabSecret,
)
from integrates.db_model.roots.types import GitRoot
from integrates.oauth.azure import get_azure_token
from integrates.oauth.bitbucket import get_bitbucket_token
from integrates.oauth.gitlab import get_token
from integrates.settings.logger import LOGGING

logging.config.dictConfig(LOGGING)

LOGGER = logging.getLogger(__name__)

TFun = TypeVar("TFun", bound=Callable[..., Any])


def lock_refresh(function: TFun) -> TFun:
    lock = Lock()

    @functools.wraps(function)
    async def wrapper(*args: Any, **kwargs: Any) -> Any:
        async with lock:
            return await function(*args, **kwargs)

    return cast(TFun, wrapper)


async def get_credential_token(
    *,
    credential: Credentials,
    loaders: Dataloaders,
) -> str | None:
    if isinstance(credential.secret, OauthGithubSecret):
        return credential.secret.access_token

    return await get_refreshed_token(credential=credential, loaders=loaders)


@lock_refresh
async def get_refreshed_token(
    *,
    credential: Credentials,
    loaders: Dataloaders,
) -> str | None:
    if isinstance(credential.secret, OauthGitlabSecret):
        return await get_token(credential=credential, loaders=loaders)

    if isinstance(credential.secret, OauthAzureSecret):
        return await get_azure_token(credential=credential, loaders=loaders)

    if isinstance(credential.secret, OauthBitbucketSecret):
        return await get_bitbucket_token(credential=credential, loaders=loaders)

    return None


def get_oauth_type(
    credential: Credentials,
) -> str:
    if isinstance(credential.secret, OauthGithubSecret):
        return "GITHUB"

    if isinstance(credential.secret, OauthGitlabSecret):
        return "GITLAB"

    if isinstance(credential.secret, OauthAzureSecret):
        return "AZURE"

    if isinstance(credential.secret, OauthBitbucketSecret):
        return "BITBUCKET"

    return ""


async def get_last_commit_info(
    *,
    root: GitRoot,
    cred: Credentials,
    loaders: Dataloaders,
    follow_redirects: bool = False,
) -> tuple[str | None, str | None]:
    try:
        token = await get_credential_token(
            loaders=loaders,
            credential=cred,
        )
        last_commit, err_message = await ls_remote(
            repo_url=root.state.url,
            token=token,
            provider=get_oauth_type(cred),
            repo_branch=root.state.branch,
            follow_redirects=follow_redirects,
        )
    except (InvalidAuthorization, InvalidGitCredentials) as exc:
        err_message = "Invalid credentials"
        LOGGER.error(
            err_message,
            extra={
                "exc": exc,
                "root": root,
                "owner": cred.state.owner,
                "org": cred.organization_id,
            },
        )
        return None, err_message

    except GitInvalidParameter:
        err_message = "Failed oauth https ls-remote"
        LOGGER.warning(
            err_message,
            extra={
                "extra": {
                    "group": root.group_name,
                    "root_id": root.id,
                    "root_nickname": root.state.nickname,
                },
            },
        )
        return None, err_message

    return last_commit, None
