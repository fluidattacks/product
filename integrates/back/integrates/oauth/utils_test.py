from datetime import UTC, datetime

from integrates.custom_utils.datetime import get_minus_delta, get_plus_delta, get_utc_now
from integrates.dataloaders import get_new_context
from integrates.db_model.credentials.types import (
    Credentials,
    CredentialsRequest,
    CredentialsState,
    OauthAzureSecret,
    OauthBitbucketSecret,
    OauthGitlabSecret,
)
from integrates.db_model.enums import CredentialType
from integrates.oauth.utils import update_token
from integrates.organizations.domain import get_credentials
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import OrganizationFaker
from integrates.testing.mocks import mocks
from integrates.testing.utils import parametrize

VALID_UNTIL = get_plus_delta(get_minus_delta(get_utc_now(), seconds=60), seconds=3600)


@parametrize(
    args=["secret", "modified_date", "c_request"],
    cases=[
        [
            OauthBitbucketSecret(
                brefresh_token="",
                access_token="",
                valid_until=get_plus_delta(
                    get_minus_delta(get_utc_now(), seconds=60),
                    seconds=36000,
                ),
            ),
            datetime.now(tz=UTC),
            CredentialsRequest(
                id="b124134-22ca-4bca-96a8-448f80a6580f",
                organization_id="ORG#40f6da5f-4f66-4bf0-825b-a2d9748ad6db",
            ),
        ],
        [
            OauthAzureSecret(
                access_token="",
                redirect_uri="https://fluidattacks/home",
                arefresh_token="",
                valid_until=get_plus_delta(
                    get_minus_delta(get_utc_now(), seconds=60), seconds=36000
                ),
            ),
            datetime.now(tz=UTC),
            CredentialsRequest(
                id="b224134-22ca-4bca-96a8-448f80a6580f",
                organization_id="ORG#40f6da5f-4f66-4bf0-825b-a2d9748ad6db",
            ),
        ],
        [
            OauthGitlabSecret(
                access_token="",
                redirect_uri="https://fluidattacks/home",
                refresh_token="",
                valid_until=get_plus_delta(
                    get_minus_delta(get_utc_now(), seconds=60), seconds=36000
                ),
            ),
            datetime.now(tz=UTC),
            CredentialsRequest(
                id="b324134-22ca-4bca-96a8-448f80a6580f",
                organization_id="ORG#40f6da5f-4f66-4bf0-825b-a2d9748ad6db",
            ),
        ],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="40f6da5f-4f66-4bf0-825b-a2d9748ad6db")],
            credentials=[
                Credentials(
                    id="b124134-22ca-4bca-96a8-448f80a6580f",
                    organization_id="ORG#40f6da5f-4f66-4bf0-825b-a2d9748ad6db",
                    state=CredentialsState(
                        modified_by="unitest@fluid.com",
                        modified_date=datetime(2022, 11, 21),
                        name="unittest",
                        type=CredentialType.OAUTH,
                        is_pat=False,
                        owner="admin@gmail.com",
                    ),
                    secret=OauthBitbucketSecret(
                        brefresh_token="", access_token="", valid_until=VALID_UNTIL
                    ),
                ),
                Credentials(
                    id="b224134-22ca-4bca-96a8-448f80a6580f",
                    organization_id="ORG#40f6da5f-4f66-4bf0-825b-a2d9748ad6db",
                    state=CredentialsState(
                        modified_by="unitest@fluid.com",
                        modified_date=datetime(2022, 11, 21),
                        name="unittest",
                        type=CredentialType.OAUTH,
                        is_pat=False,
                        owner="admin@gmail.com",
                    ),
                    secret=OauthAzureSecret(
                        arefresh_token="",
                        access_token="",
                        redirect_uri="https://fluidattacks/home",
                        valid_until=VALID_UNTIL,
                    ),
                ),
                Credentials(
                    id="b324134-22ca-4bca-96a8-448f80a6580f",
                    organization_id="ORG#40f6da5f-4f66-4bf0-825b-a2d9748ad6db",
                    state=CredentialsState(
                        modified_by="unitest@fluid.com",
                        modified_date=datetime(2022, 11, 21),
                        name="unittest",
                        type=CredentialType.OAUTH,
                        is_pat=False,
                        owner="admin@gmail.com",
                    ),
                    secret=OauthGitlabSecret(
                        refresh_token="",
                        access_token="",
                        redirect_uri="https://fluidattacks/home",
                        valid_until=VALID_UNTIL,
                    ),
                ),
            ],
        )
    )
)
async def test_update_token(
    secret: OauthAzureSecret | OauthBitbucketSecret | OauthGitlabSecret,
    modified_date: datetime,
    c_request: CredentialsRequest,
) -> None:
    loaders = get_new_context()
    credential = await get_credentials(loaders, c_request.id, c_request.organization_id)
    new_state = CredentialsState(
        modified_by=credential.state.modified_by,
        modified_date=modified_date,
        name=credential.state.name,
        is_pat=credential.state.is_pat,
        azure_organization=credential.state.azure_organization,
        type=credential.state.type,
        owner=credential.state.owner,
    )

    await update_token(
        credential_id=c_request.id,
        organization_id=c_request.organization_id,
        loaders=loaders,
        secret=secret,
        modified_date=modified_date,
    )
    loaders.credentials.clear_all()

    updated_credential = await get_credentials(loaders, c_request.id, c_request.organization_id)
    assert updated_credential
    assert updated_credential.state == new_state


@parametrize(
    args=["secret", "modified_date", "c_request"],
    cases=[
        [
            OauthAzureSecret(
                access_token="",
                redirect_uri="https://fluidattacks/home",
                arefresh_token="",
                valid_until=get_plus_delta(
                    get_minus_delta(get_utc_now(), seconds=60), seconds=36000
                ),
            ),
            datetime.now(tz=UTC),
            CredentialsRequest(
                id="b224134-22ca-4bca-96a8-448f80a6840d",
                organization_id="ORG#40f6da5f-4f66-4bf0-825b-a2d9748ad6db",
            ),
        ],
    ],
)
@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="40f6da5f-4f66-4bf0-825b-a2d9748ad6db")],
        )
    )
)
async def test_update_token_not_credential(
    secret: OauthAzureSecret | OauthBitbucketSecret | OauthGitlabSecret,
    modified_date: datetime,
    c_request: CredentialsRequest,
) -> None:
    loaders = get_new_context()
    await update_token(
        credential_id=c_request.id,
        organization_id=c_request.organization_id,
        loaders=loaders,
        secret=secret,
        modified_date=modified_date,
    )
    loaders.credentials.clear_all()

    updated_credential = await loaders.credentials.load(c_request)
    assert not updated_credential
