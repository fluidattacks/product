import asyncio
import json
import logging
import logging.config

import aiohttp
from aiohttp import (
    BasicAuth,
)

from integrates.context import (
    FI_GITHUB_OAUTH2_APP_ID,
    FI_GITHUB_OAUTH2_SECRET,
)
from integrates.db_model.credentials.types import (
    Credentials,
    OauthGithubSecret,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)

# Constants
LOGGER = logging.getLogger(__name__)
GITHUB_AUTHZ_URL = "https://github.com/login/oauth/authorize"
GITHUB_REFRESH_URL = "https://github.com/login/oauth/access_token"

GITHUB_ARGS = {
    "name": "github",
    "client_id": FI_GITHUB_OAUTH2_APP_ID,
    "authorize_url": GITHUB_AUTHZ_URL,
    "client_kwargs": {"scope": "read:org repo"},
}


async def get_access_token(*, code: str) -> str | None:
    request_parameters: dict[str, str] = {
        "client_id": FI_GITHUB_OAUTH2_APP_ID,
        "client_secret": FI_GITHUB_OAUTH2_SECRET,
        "code": code,
    }
    headers: dict[str, str] = {
        "Accept": "application/json",
        "content-type": "application/json",
    }
    retries: int = 0
    retry: bool = True
    async with aiohttp.ClientSession(headers=headers) as session:
        while retry and retries < 10:
            async with session.post(
                GITHUB_REFRESH_URL,
                data=json.dumps(request_parameters),
            ) as response:
                try:
                    result = await response.json()
                except (
                    json.decoder.JSONDecodeError,
                    aiohttp.ClientError,
                ) as exc:
                    LOGGER.exception(exc, extra={"extra": locals()})
                    break
                if not response.ok:
                    retry = True
                    retries += 1
                    await asyncio.sleep(0.2)
                    continue

                return result["access_token"]

    return None


async def remove_oauth_access(*, credential: Credentials) -> None:
    if not isinstance(credential.secret, OauthGithubSecret):
        return
    request_parameters = {
        "access_token": credential.secret.access_token,
    }
    headers = {
        "Accept": "application/vnd.github+json",
        "X-GitHub-Api-Version": "2022-11-28",
    }
    retries = 0
    retry = True
    async with aiohttp.ClientSession(headers=headers) as session:
        while retry and retries < 5:
            async with session.delete(
                ("https://api.github.com/applications/{FI_GITHUB_OAUTH2_APP_ID}/grant"),
                auth=BasicAuth(
                    login=FI_GITHUB_OAUTH2_APP_ID,
                    password=FI_GITHUB_OAUTH2_SECRET,
                ),
                data=json.dumps(request_parameters),
            ) as response:
                try:
                    result = await response.text()
                except aiohttp.ClientError as exc:
                    LOGGER.exception(exc, extra={"extra": locals()})
                    break

                if response.status != 204:
                    if retries == 4:
                        LOGGER.error(
                            "Failed to revoke access",
                            extra={
                                "extra": {
                                    "result": result,
                                    "credential_id": credential.id,
                                    "retries": retries,
                                    "status": response.status,
                                },
                            },
                        )

                    retry = True
                    retries += 1
                    await asyncio.sleep(0.2)
                    continue

                return
