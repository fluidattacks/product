import asyncio
import json
import logging
import logging.config
from datetime import (
    datetime,
    timedelta,
)

import aiohttp
import pytz

from integrates.context import (
    FI_GITLAB_ISSUES_OAUTH2_APP_ID,
    FI_GITLAB_ISSUES_OAUTH2_SECRET,
    FI_GITLAB_OAUTH2_APP_ID,
    FI_GITLAB_OAUTH2_SECRET,
)
from integrates.dataloaders import (
    Dataloaders,
)
from integrates.db_model.credentials.types import (
    Credentials,
    CredentialsMetadataToUpdate,
    CredentialsState,
    OauthGitlabSecret,
)
from integrates.db_model.credentials.update import (
    update_credentials,
)
from integrates.db_model.roots.types import (
    GitRoot,
)
from integrates.oauth.utils import (
    get_credential_or_token,
)
from integrates.organizations.utils import (
    get_organization_roots,
)
from integrates.settings import (
    LOGGING,
)

logging.config.dictConfig(LOGGING)

LOGGER = logging.getLogger(__name__)
GITLAB_AUTHZ_URL = "https://gitlab.com/oauth/authorize"
GITLAB_REFRESH_URL = "https://gitlab.com/oauth/token"

GITLAB_ISSUES_ARGS = {
    "access_token_url": GITLAB_REFRESH_URL,
    "authorize_url": GITLAB_AUTHZ_URL,
    "client_id": FI_GITLAB_ISSUES_OAUTH2_APP_ID,
    "client_kwargs": {"scope": "api"},
    "client_secret": FI_GITLAB_ISSUES_OAUTH2_SECRET,
    "code_challenge_method": "S256",
    "name": "gitlab_issues",
}
GITLAB_REPOSITORY_ARGS = {
    "name": "gitlab",
    "client_id": FI_GITLAB_OAUTH2_APP_ID,
    "client_secret": FI_GITLAB_OAUTH2_SECRET,
    "authorize_url": GITLAB_AUTHZ_URL,
    "code_challenge_method": "S256",
    "client_kwargs": {"scope": "read_api read_repository"},
}


async def get_refresh_token(
    *,
    code: str,
    redirect_uri: str,
    code_verifier: str,
) -> dict | None:
    request_parameters: dict[str, str] = {
        "client_id": FI_GITLAB_OAUTH2_APP_ID,
        "client_secret": FI_GITLAB_OAUTH2_SECRET,
        "code": code,
        "grant_type": "authorization_code",
        "redirect_uri": redirect_uri,
        "code_verifier": code_verifier,
    }
    headers: dict[str, str] = {"content-type": "application/json"}
    retries: int = 0
    retry: bool = True
    async with aiohttp.ClientSession(headers=headers) as session:
        while retry and retries < 5:
            retry = False
            async with session.post(
                GITLAB_REFRESH_URL,
                data=json.dumps(request_parameters),
            ) as response:
                try:
                    result = await response.json()
                except json.decoder.JSONDecodeError:
                    break
                if not response.ok:
                    retry = True
                    retries += 1
                    await asyncio.sleep(0.2)
                    continue

                return result

    return None


async def get_token(
    *,
    credential: Credentials,
    loaders: Dataloaders,
) -> str | None:
    if not isinstance(credential.secret, OauthGitlabSecret):
        return None

    credential_or_token = await get_credential_or_token(
        credential=credential,
        loaders=loaders,
    )
    if isinstance(credential_or_token, str):
        return credential_or_token

    credential = credential_or_token
    if not isinstance(credential.secret, OauthGitlabSecret):
        return None

    roots = await get_organization_roots(loaders, credential.organization_id)
    used_by = [
        root.group_name
        for root in roots
        if isinstance(root, GitRoot) and root.state.credential_id == credential.id
    ]

    request_parameters: dict[str, str] = {
        "client_id": FI_GITLAB_OAUTH2_APP_ID,
        "client_secret": FI_GITLAB_OAUTH2_SECRET,
        "refresh_token": credential.secret.refresh_token,
        "grant_type": "refresh_token",
        "redirect_uri": credential.secret.redirect_uri,
    }
    headers: dict[str, str] = {"content-type": "application/json"}
    retries: int = 0
    retry: bool = True
    async with aiohttp.ClientSession(headers=headers) as session:
        while retry and retries < 5:
            retry = False
            async with session.post(
                GITLAB_REFRESH_URL,
                data=json.dumps(request_parameters),
            ) as response:
                try:
                    result = await response.json()
                except json.decoder.JSONDecodeError:
                    break
                if not response.ok:
                    retry = True
                    retries += 1
                    if retries == 5:
                        LOGGER.error(
                            "Failed to refresh gitlab token",
                            extra={
                                "extra": {
                                    **result,
                                    "credential_id": credential.id,
                                    "used_by": used_by,
                                },
                            },
                        )
                    await asyncio.sleep(0.2)
                    continue

                secret = OauthGitlabSecret(
                    redirect_uri=credential.secret.redirect_uri,
                    refresh_token=result["refresh_token"],
                    access_token=result["access_token"],
                    valid_until=(
                        datetime.utcfromtimestamp(result["created_at"])
                        + timedelta(seconds=int(result["expires_in"]) - 60)
                    ),
                )
                new_state = CredentialsState(
                    modified_by=credential.state.modified_by,
                    modified_date=datetime.now(tz=pytz.timezone("UTC")),
                    name=credential.state.name,
                    is_pat=credential.state.is_pat,
                    azure_organization=credential.state.azure_organization,
                    type=credential.state.type,
                    owner=credential.state.owner,
                )
                await update_credentials(
                    current_value=credential,
                    credential_id=credential.id,
                    organization_id=credential.organization_id,
                    state=new_state,
                    metadata=CredentialsMetadataToUpdate(secret=secret),
                )
                loaders.credentials.clear_all()
                loaders.organization_credentials.clear(credential.organization_id)

                return result["access_token"]

    return None
