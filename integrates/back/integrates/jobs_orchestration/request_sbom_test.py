from integrates import batch, jobs_orchestration
from integrates.batch.dal import (
    get_actions,
)
from integrates.batch.enums import (
    Action,
    SkimsBatchQueue,
)
from integrates.batch.types import PutActionResult
from integrates.custom_exceptions import (
    SbomFileNotRequested,
)
from integrates.dataloaders import get_new_context
from integrates.db_model.credentials.types import HttpsSecret
from integrates.jobs_orchestration import request_sbom
from integrates.jobs_orchestration.request_sbom import (
    request_sbom_file,
)
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    CredentialsFaker,
    GitRootFaker,
    GitRootStateFaker,
    GroupFaker,
    OrganizationFaker,
    RootDockerImageFaker,
    RootDockerImageStateFaker,
    StakeholderFaker,
)
from integrates.testing.mocks import Mock, mocks
from integrates.testing.utils import raises

EMAIL_TEST = "user@clientapp.com"
GROUP_NAME = "group1"


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            stakeholders=[StakeholderFaker(email=EMAIL_TEST, role="user")],
            groups=[GroupFaker(name=GROUP_NAME, organization_id="org1")],
            roots=[
                GitRootFaker(
                    group_name=GROUP_NAME, id="root1", state=GitRootStateFaker(nickname="back")
                )
            ],
        ),
    ),
    others=[
        Mock(batch.dal.put, "put_action_to_batch", "async", "123456"),
        Mock(
            jobs_orchestration.jobs,
            "generate_batch_action_config_for_root",
            "async",
            (
                {
                    "root_nicknames": ["back"],
                    "roots_config_files": ["back.yaml"],
                    "sbom_format": "fluid-json",
                    "job_type": "request",
                },
                "123456",
                SkimsBatchQueue.SMALL,
            ),
        ),
    ],
)
async def test_request_sbom_file() -> None:
    loaders = get_new_context()
    await request_sbom_file(
        loaders=loaders,
        user_email="user@clientapp.com",
        group_name=GROUP_NAME,
        root_nicknames=["back"],
        uris_with_credentials=None,
        file_format="xml",
        sbom_format_str="cyclone_dx",
    )

    actions = await get_actions()
    assert len(actions) == 1
    assert actions[0].action_name == Action.GENERATE_ROOT_SBOM
    assert actions[0].additional_info["root_nicknames"] == ["back"]
    assert actions[0].additional_info["job_type"] == "request"


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="ORG#org1")],
            stakeholders=[StakeholderFaker(email=EMAIL_TEST, role="user")],
            groups=[GroupFaker(name=GROUP_NAME, organization_id="ORG#org1")],
            roots=[
                GitRootFaker(
                    group_name=GROUP_NAME, id="root1", state=GitRootStateFaker(nickname="back")
                )
            ],
            docker_images=[
                RootDockerImageFaker(
                    uri="docker.io/test-dummy-image:0.0.1",
                    group_name=GROUP_NAME,
                    root_id="root1",
                    state=RootDockerImageStateFaker(credential_id="test-credential"),
                ),
                RootDockerImageFaker(
                    uri="docker.io/test-dummy-image:0.0.2",
                    group_name=GROUP_NAME,
                    root_id="root1",
                    state=RootDockerImageStateFaker(
                        credential_id="test-credential",
                        digest=(
                            "sha256:7c2c9c72b3c65b9881c2a3ff19c8b3c43b6a0d925602cbf9033467b0b7f6f9b132"
                        ),
                    ),
                ),
            ],
            credentials=[
                CredentialsFaker(
                    credential_id="test-credential",
                    organization_id="ORG#org1",
                    secret=HttpsSecret(user=EMAIL_TEST, password="pass"),  # noqa: S106
                ),
            ],
        ),
    ),
    others=[
        Mock(batch.dal.put, "put_action_to_batch", "async", "123456"),
        Mock(
            jobs_orchestration.jobs,
            "generate_batch_action_config_for_image",
            "async",
            (
                {
                    "images": [
                        {
                            "image_ref": "docker.io/test-dummy-image:0.0.1",
                            "root_nickname": "back",
                        }
                    ],
                    "images_config_files": ["docker.io/test-dummy-image:0.0.1.yaml"],
                    "sbom_format": "fluid-json",
                    "job_type": "request",
                },
                "123456",
                SkimsBatchQueue.SMALL,
            ),
        ),
    ],
)
async def test_request_sbom_file_for_image() -> None:
    loaders = get_new_context()
    await request_sbom_file(
        loaders=loaders,
        user_email=EMAIL_TEST,
        group_name=GROUP_NAME,
        root_nicknames=None,
        uris_with_credentials=[
            {"uri": "docker.io/test-dummy-image:0.0.1", "credential_id": "test-credential"}
        ],
        file_format="xml",
        sbom_format_str="cyclone_dx",
    )

    actions = await get_actions()
    assert len(actions) == 1
    assert actions[0].action_name == Action.GENERATE_IMAGE_SBOM
    assert actions[0].additional_info["job_type"] == "request"

    notifications = await loaders.user_notifications.load(EMAIL_TEST)
    assert len(notifications) == 1


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            stakeholders=[
                StakeholderFaker(email="hacker@fluidattacks.com", role="hacker"),
                StakeholderFaker(email="reattacker@test.test", role="reattacker"),
            ],
            groups=[GroupFaker(name=GROUP_NAME, organization_id="org1")],
            roots=[
                GitRootFaker(
                    group_name=GROUP_NAME, id="root1", state=GitRootStateFaker(nickname="back")
                )
            ],
        ),
    )
)
async def test_request_sbom_file_wrong_format() -> None:
    loaders = get_new_context()
    with raises(ValueError, match="Invalid format cyclone, json"):
        await request_sbom_file(
            loaders=loaders,
            user_email="user@clientapp.com",
            group_name=GROUP_NAME,
            root_nicknames=["back"],
            uris_with_credentials=None,
            file_format="json",
            sbom_format_str="cyclone",
        )


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="org1")],
            stakeholders=[
                StakeholderFaker(email="hacker@fluidattacks.com", role="hacker"),
                StakeholderFaker(email="reattacker@test.test", role="reattacker"),
            ],
            groups=[GroupFaker(name=GROUP_NAME, organization_id="org1")],
            roots=[
                GitRootFaker(
                    group_name=GROUP_NAME, id="root1", state=GitRootStateFaker(nickname="back")
                )
            ],
        ),
    ),
    others=[Mock(request_sbom, "queue_sbom_job", "async", PutActionResult(success=False))],
)
async def test_request_sbom_file_failed_job() -> None:
    loaders = get_new_context()
    with raises(SbomFileNotRequested):
        await request_sbom_file(
            loaders=loaders,
            user_email="user@clientapp.com",
            group_name=GROUP_NAME,
            root_nicknames=["back"],
            uris_with_credentials=None,
            file_format="json",
            sbom_format_str="cyclone_dx",
        )
