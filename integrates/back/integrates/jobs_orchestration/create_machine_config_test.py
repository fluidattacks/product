import uuid
from datetime import (
    datetime,
)

import integrates.jobs_orchestration.common_orchestration_utils
import integrates.jobs_orchestration.create_machine_config
import integrates.s3.operations
from integrates.batch.enums import (
    SkimsBatchQueue,
)
from integrates.custom_utils.datetime import (
    get_now,
)
from integrates.dataloaders import (
    Dataloaders,
    get_new_context,
)
from integrates.db_model.roots.enums import (
    RootEnvironmentCloud,
    RootEnvironmentUrlType,
)
from integrates.db_model.roots.types import (
    Secret,
    SecretState,
)
from integrates.jobs_orchestration import (
    common_orchestration_utils,
    create_machine_config,
)
from integrates.jobs_orchestration.create_machine_config import (
    MachineModulesToExecute,
    generate_batch_action_config,
    generate_job_configs,
    get_warp_vnet_name,
)
from integrates.s3.operations import (
    file_exists,
)
from integrates.testing.aws import (
    IntegratesAws,
    IntegratesDynamodb,
)
from integrates.testing.fakers import (
    GitRootFaker,
    GitRootStateFaker,
    GroupFaker,
    OrganizationFaker,
    OrganizationStateFaker,
    RootEnvironmentUrlFaker,
    RootEnvironmentUrlStateFaker,
)
from integrates.testing.mocks import (
    Mock,
    mocks,
)


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(id="org1"),
            ],
            groups=[
                GroupFaker(name="group1", organization_id="org1"),
            ],
            roots=[
                GitRootFaker(
                    group_name="group1",
                    id="root1",
                    state=GitRootStateFaker(
                        nickname="testroot",
                        gitignore=["**/maven/**", "test/"],
                    ),
                ),
            ],
        ),
    ),
)
async def test_generate_static_config() -> None:
    loaders: Dataloaders = get_new_context()

    root_nicknames_and_configs = await generate_job_configs(
        loaders=loaders,
        batch_job_id="123456",
        group_name="group1",
        root_nicknames=["testroot"],
        modules_to_execute=MachineModulesToExecute(
            APK=False,
            CSPM=False,
            DAST=False,
            SAST=True,
            SCA=True,
        ),
    )

    assert len(root_nicknames_and_configs) == 1
    assert root_nicknames_and_configs[0][0] == "testroot"
    assert root_nicknames_and_configs[0][1] == {
        "language": "EN",
        "namespace": "testroot",
        "output": {
            "file_path": ("execution_results/group1_testroot_123456.sarif"),
            "format": "SARIF",
        },
        "execution_id": "group1_testroot_123456",
        "sast": {
            "include": ["."],
            "exclude": ["**/maven/**", "glob(**/.git)", "test/"],
            "recursion_limit": 1000,
        },
        "sca": {
            "include": ["."],
            "exclude": ["**/maven/**", "glob(**/.git)", "test/"],
            "use_new_sca": True,
        },
    }


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(id="org1"),
            ],
            groups=[
                GroupFaker(name="group1", organization_id="org1"),
            ],
            roots=[
                GitRootFaker(
                    group_name="group1",
                    id="root1",
                    state=GitRootStateFaker(nickname="testroot"),
                ),
            ],
            root_environment_urls=[
                RootEnvironmentUrlFaker(
                    url="http://localhost:48000/",
                    group_name="group1",
                    root_id="root1",
                    state=RootEnvironmentUrlStateFaker(modified_by="hacker@fluidattacks.com"),
                ),
            ],
        ),
    )
)
async def test_generate_dast_config() -> None:
    loaders: Dataloaders = get_new_context()
    root_nicknames_and_configs = await generate_job_configs(
        loaders=loaders,
        batch_job_id="123456",
        group_name="group1",
        root_nicknames=["testroot"],
        modules_to_execute=MachineModulesToExecute(
            APK=False,
            CSPM=False,
            DAST=True,
            SAST=False,
            SCA=False,
        ),
    )

    assert len(root_nicknames_and_configs) == 1
    assert root_nicknames_and_configs[0][0] == "testroot"
    assert root_nicknames_and_configs[0][1] == {
        "language": "EN",
        "namespace": "testroot",
        "output": {
            "file_path": ("execution_results/group1_testroot_123456.sarif"),
            "format": "SARIF",
        },
        "execution_id": "group1_testroot_123456",
        "dast": {
            "urls": ["http://localhost:48000/"],
            "http_checks": True,
            "ssl_checks": True,
        },
    }


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(id="org1"),
            ],
            groups=[
                GroupFaker(name="group1", organization_id="org1"),
            ],
            roots=[
                GitRootFaker(
                    group_name="group1",
                    id="root1",
                    state=GitRootStateFaker(nickname="testroot"),
                ),
            ],
            root_environment_urls=[
                RootEnvironmentUrlFaker(
                    url="certs_pinned.apk",
                    group_name="group1",
                    root_id="root1",
                    state=RootEnvironmentUrlStateFaker(
                        modified_by="hacker@fluidattacks.com",
                        url_type=RootEnvironmentUrlType.APK,
                    ),
                ),
            ],
        ),
    )
)
async def test_generate_apk_config() -> None:
    loaders: Dataloaders = get_new_context()
    root_nicknames_and_configs = await generate_job_configs(
        loaders=loaders,
        batch_job_id="123456",
        group_name="group1",
        root_nicknames=["testroot"],
        modules_to_execute=MachineModulesToExecute(
            APK=True,
            CSPM=False,
            DAST=False,
            SAST=False,
            SCA=False,
        ),
    )

    assert len(root_nicknames_and_configs) == 1
    assert root_nicknames_and_configs[0][0] == "testroot"
    assert root_nicknames_and_configs[0][1] == {
        "language": "EN",
        "namespace": "testroot",
        "output": {
            "file_path": ("execution_results/group1_testroot_123456.sarif"),
            "format": "SARIF",
        },
        "execution_id": "group1_testroot_123456",
        "apk": {
            "exclude": [],
            "include": [
                "fa_apks_to_analyze/certs_pinned.apk",
            ],
        },
    }


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(
                    id="org1",
                    state=OrganizationStateFaker(
                        aws_external_id="be991c12-9b68-4312-85c4-16ce5fa2fca0",
                    ),
                ),
            ],
            groups=[
                GroupFaker(name="group1", organization_id="org1"),
            ],
            roots=[
                GitRootFaker(
                    group_name="group1",
                    id="root1",
                    state=GitRootStateFaker(nickname="testroot"),
                ),
            ],
            root_environment_urls=[
                RootEnvironmentUrlFaker(
                    url="arn:aws:iam::123456789012:role/my-role_name",
                    group_name="group1",
                    root_id="root1",
                    state=RootEnvironmentUrlStateFaker(
                        modified_by="hacker@fluidattacks.com",
                        url_type=RootEnvironmentUrlType.CSPM,
                        cloud_name=RootEnvironmentCloud.AWS,
                    ),
                ),
                RootEnvironmentUrlFaker(
                    url="632589123658",
                    group_name="group1",
                    root_id="root1",
                    state=RootEnvironmentUrlStateFaker(
                        modified_by="hacker@fluidattacks.com",
                        url_type=RootEnvironmentUrlType.CSPM,
                        cloud_name=RootEnvironmentCloud.AZURE,
                    ),
                ),
            ],
        ),
    ),
    others=[
        Mock(
            create_machine_config,
            "get_environment_url_secrets",
            "async",
            [
                Secret(
                    key="AZURE_CLIENT_ID",
                    value="azure_client_id_1",
                    created_at=datetime.now(),
                    state=SecretState(
                        owner="admin@gmail.com",
                        modified_by="admin@gmail.com",
                        modified_date=datetime.now(),
                    ),
                ),
                Secret(
                    key="AZURE_CLIENT_SECRET",
                    value="azure_client_secret_1",
                    created_at=datetime.now(),
                    state=SecretState(
                        owner="admin@gmail.com",
                        modified_by="admin@gmail.com",
                        modified_date=datetime.now(),
                    ),
                ),
                Secret(
                    key="AZURE_TENANT_ID",
                    value="azure_tenant_id_1",
                    created_at=datetime.now(),
                    state=SecretState(
                        owner="admin@gmail.com",
                        modified_by="admin@gmail.com",
                        modified_date=datetime.now(),
                    ),
                ),
                Secret(
                    key="AZURE_SUBSCRIPTION_ID",
                    value="azure_subscription_id_1",
                    created_at=datetime.now(),
                    state=SecretState(
                        owner="admin@gmail.com",
                        modified_by="admin@gmail.com",
                        modified_date=datetime.now(),
                    ),
                ),
            ],
        )
    ],
)
async def test_generate_cspm_config() -> None:
    loaders: Dataloaders = get_new_context()
    root_nicknames_and_configs = await generate_job_configs(
        loaders=loaders,
        batch_job_id="123456",
        group_name="group1",
        root_nicknames=["testroot"],
        modules_to_execute=MachineModulesToExecute(
            APK=False,
            CSPM=True,
            DAST=False,
            SAST=False,
            SCA=False,
        ),
    )

    assert len(root_nicknames_and_configs) == 1
    assert root_nicknames_and_configs[0][0] == "testroot"
    assert root_nicknames_and_configs[0][1] == {
        "language": "EN",
        "namespace": "testroot",
        "output": {
            "file_path": ("execution_results/group1_testroot_123456.sarif"),
            "format": "SARIF",
        },
        "execution_id": "group1_testroot_123456",
        "cspm": {
            "aws_credentials": [
                {
                    "external_id": "be991c12-9b68-4312-85c4-16ce5fa2fca0",
                    "role": "arn:aws:iam::123456789012:role/my-role_name",
                },
            ],
            "azure_credentials": [
                {
                    "client_id": "azure_client_id_1",
                    "client_secret": "azure_client_secret_1",
                    "tenant_id": "azure_tenant_id_1",
                    "subscription_id": "azure_subscription_id_1",
                },
            ],
            "gcp_credentials": [],
        },
    }


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(
                    id="org1",
                    state=OrganizationStateFaker(
                        aws_external_id="be991c12-9b68-4312-85c4-16ce5fa2fca0",
                    ),
                ),
            ],
            groups=[
                GroupFaker(name="group1", organization_id="org1"),
            ],
            roots=[
                GitRootFaker(
                    group_name="group1",
                    id="root1",
                    state=GitRootStateFaker(nickname="testroot"),
                ),
            ],
            root_environment_urls=[
                RootEnvironmentUrlFaker(
                    url="235620189920",
                    group_name="group1",
                    root_id="root1",
                    state=RootEnvironmentUrlStateFaker(
                        modified_by="hacker@fluidattacks.com",
                        url_type=RootEnvironmentUrlType.CSPM,
                        cloud_name=RootEnvironmentCloud.GCP,
                    ),
                ),
            ],
        ),
    ),
    others=[
        Mock(
            create_machine_config,
            "get_environment_url_secrets",
            "async",
            [
                Secret(
                    key="GCP_PRIVATE_KEY",
                    value="gcp_credentials_example",
                    created_at=datetime.now(),
                    state=SecretState(
                        owner="admin@gmail.com",
                        modified_by="admin@gmail.com",
                        modified_date=datetime.now(),
                    ),
                ),
            ],
        )
    ],
)
async def test_generate_cspm_gcp_config() -> None:
    loaders: Dataloaders = get_new_context()
    root_nicknames_and_configs = await generate_job_configs(
        loaders=loaders,
        batch_job_id="123456",
        group_name="group1",
        root_nicknames=["testroot"],
        modules_to_execute=MachineModulesToExecute(
            APK=False,
            CSPM=True,
            DAST=False,
            SAST=False,
            SCA=False,
        ),
    )

    assert len(root_nicknames_and_configs) == 1
    assert root_nicknames_and_configs[0][0] == "testroot"
    assert root_nicknames_and_configs[0][1] == {
        "language": "EN",
        "namespace": "testroot",
        "output": {
            "file_path": ("execution_results/group1_testroot_123456.sarif"),
            "format": "SARIF",
        },
        "execution_id": "group1_testroot_123456",
        "cspm": {
            "aws_credentials": [],
            "azure_credentials": [],
            "gcp_credentials": [
                {
                    "private_key": "gcp_credentials_example",
                },
            ],
        },
    }


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(id="org1"),
            ],
            groups=[
                GroupFaker(name="group1", organization_id="org1"),
            ],
            roots=[
                GitRootFaker(
                    group_name="group1",
                    id="root1",
                    state=GitRootStateFaker(
                        nickname="testroot",
                        gitignore=["**/maven/**", "test/"],
                    ),
                ),
            ],
        ),
    )
)
async def test_generate_reattack_config() -> None:
    loaders: Dataloaders = get_new_context()

    root_nicknames_and_configs = await generate_job_configs(
        loaders=loaders,
        batch_job_id="123456",
        group_name="group1",
        root_nicknames=["testroot"],
        checks=["F001"],
        root_paths={"testroot": ["path1.json", "path2.json"]},
        modules_to_execute=MachineModulesToExecute(
            APK=False,
            CSPM=False,
            DAST=False,
            SAST=True,
            SCA=False,
        ),
    )

    assert len(root_nicknames_and_configs) == 1
    assert root_nicknames_and_configs[0][0] == "testroot"
    assert root_nicknames_and_configs[0][1] == {
        "checks": ["F001"],
        "language": "EN",
        "namespace": "testroot",
        "output": {
            "file_path": ("execution_results/group1_testroot_123456.sarif"),
            "format": "SARIF",
        },
        "execution_id": "group1_testroot_123456",
        "sast": {
            "include": ["path1.json", "path2.json"],
            "exclude": ["**/maven/**", "glob(**/.git)", "test/"],
            "recursion_limit": 1000,
        },
    }


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(id="org1"),
            ],
            groups=[
                GroupFaker(name="group1", organization_id="org1"),
            ],
            roots=[
                GitRootFaker(
                    group_name="group1",
                    id="root1",
                    state=GitRootStateFaker(
                        nickname="testroot",
                    ),
                ),
                GitRootFaker(
                    group_name="group1",
                    id="root2",
                    state=GitRootStateFaker(
                        branch="test",
                        nickname="testroot2",
                    ),
                ),
            ],
            root_environment_urls=[
                RootEnvironmentUrlFaker(
                    url="http://localhost:48000/",
                    group_name="group1",
                    root_id="root1",
                    state=RootEnvironmentUrlStateFaker(modified_by="hacker@fluidattacks.com"),
                ),
            ],
        ),
    ),
    others=[
        Mock(uuid, "uuid4", "sync", "123"),
        Mock(
            integrates.jobs_orchestration.common_orchestration_utils,
            "FI_ENVIRONMENT",
            "function",
            "production",
        ),
        Mock(integrates.s3.operations, "FI_AWS_S3_PATH_PREFIX", "function", ""),
    ],
)
async def test_machine_generate_batch_action_config() -> None:
    loaders = get_new_context()
    result = await generate_batch_action_config(
        loaders=loaders,
        group_name="group1",
        root_nicknames=["testroot", "testroot2"],
        modules_to_execute=MachineModulesToExecute(
            APK=False,
            CSPM=False,
            DAST=True,
            SAST=False,
            SCA=False,
        ),
    )

    assert result == (
        {
            "roots": ["testroot", "testroot2"],
            "roots_config_files": [
                "group1_testroot_123.yaml",
                "group1_testroot2_123.yaml",
            ],
        },
        "machine_group1_scheduler_all_123",
        SkimsBatchQueue.SMALL,
    )
    file_path = "configs/group1_testroot_123.yaml"
    was_file_uploaded = await file_exists(file_path, "machine.data")
    assert was_file_uploaded


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(id="org1"),
            ],
            groups=[
                GroupFaker(name="group1", organization_id="org1"),
            ],
            roots=[
                GitRootFaker(
                    group_name="group1",
                    id="root1",
                    state=GitRootStateFaker(
                        nickname="testroot",
                    ),
                ),
                GitRootFaker(
                    group_name="group1",
                    id="root2",
                    state=GitRootStateFaker(
                        branch="test",
                        nickname="testroot2",
                    ),
                ),
            ],
        ),
    ),
    others=[
        Mock(common_orchestration_utils, "upload_config_to_s3", "async", None),
        Mock(uuid, "uuid4", "sync", "123"),
    ],
)
async def test_machine_generate_batch_action_config_sast() -> None:
    loaders = get_new_context()
    result = await generate_batch_action_config(
        loaders=loaders,
        group_name="group1",
        root_nicknames=["testroot", "testroot2"],
        modules_to_execute=MachineModulesToExecute(
            APK=False,
            CSPM=False,
            DAST=False,
            SAST=True,
            SCA=False,
        ),
        include_sbom=True,
    )

    assert result == (
        {
            "roots": ["testroot", "testroot2"],
            "roots_config_files": [
                "group1_testroot_123.yaml",
                "group1_testroot2_123.yaml",
            ],
            "roots_sbom_config_files": [
                "sbom_group1_testroot_123_config.yaml",
                "sbom_group1_testroot2_123_config.yaml",
            ],
        },
        "machine_group1_scheduler_all_123",
        SkimsBatchQueue.SMALL,
    )


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(id="org1", name="testorganization"),
            ],
            groups=[
                GroupFaker(name="group1", organization_id="org1"),
            ],
            roots=[
                GitRootFaker(
                    group_name="group1",
                    id="root1",
                    state=GitRootStateFaker(
                        nickname="testroot",
                    ),
                ),
            ],
            root_environment_urls=[
                RootEnvironmentUrlFaker(
                    url="http://localhost:48000/",
                    group_name="group1",
                    root_id="root1",
                    state=RootEnvironmentUrlStateFaker(
                        modified_by="hacker@fluidattacks.com",
                        use_ztna=True,
                    ),
                ),
            ],
        ),
    ),
    others=[
        Mock(common_orchestration_utils, "upload_config_to_s3", "async", None),
        Mock(uuid, "uuid4", "sync", "123"),
    ],
)
async def test_machine_generate_batch_action_config_vnet() -> None:
    loaders = get_new_context()
    result = await generate_batch_action_config(
        loaders=loaders,
        group_name="group1",
        root_nicknames=["testroot"],
        modules_to_execute=MachineModulesToExecute(
            APK=False,
            CSPM=False,
            DAST=True,
            SAST=False,
            SCA=False,
        ),
    )

    assert result == (
        {
            "network_name": "testorganization",
            "roots": ["testroot"],
            "roots_config_files": ["group1_testroot_123.yaml"],
        },
        "machine_group1_scheduler_all_123",
        SkimsBatchQueue.SMALL,
    )


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(id="org1", name="testorganization"),
            ],
            groups=[
                GroupFaker(name="group1", organization_id="org1"),
            ],
            roots=[
                GitRootFaker(
                    group_name="group1",
                    id="root1",
                    state=GitRootStateFaker(
                        nickname="testroot",
                    ),
                ),
            ],
            root_environment_urls=[
                RootEnvironmentUrlFaker(
                    url="http://localhost:48000/",
                    group_name="group1",
                    root_id="root1",
                    state=RootEnvironmentUrlStateFaker(
                        modified_by="hacker@fluidattacks.com",
                        use_egress=True,
                    ),
                ),
            ],
        ),
    )
)
async def test_get_warp_vnet_name() -> None:
    loaders = get_new_context()
    result = await get_warp_vnet_name(
        loaders=loaders,
        group_name="group1",
        modules_to_execute=MachineModulesToExecute(
            APK=False,
            CSPM=False,
            DAST=True,
            SAST=False,
            SCA=False,
        ),
    )
    assert result == "egress-ips"


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[
                OrganizationFaker(id="org1"),
            ],
            groups=[
                GroupFaker(name="group1", organization_id="org1", created_date=get_now()),
            ],
            roots=[
                GitRootFaker(
                    group_name="group1",
                    id="root1",
                    created_date=get_now(),
                    state=GitRootStateFaker(
                        nickname="testroot",
                    ),
                ),
            ],
        ),
    )
)
async def test_report_soon_config() -> None:
    loaders: Dataloaders = get_new_context()

    root_nicknames_and_configs = await generate_job_configs(
        loaders=loaders,
        batch_job_id="123456",
        group_name="group1",
        root_nicknames=["testroot"],
        modules_to_execute=MachineModulesToExecute(
            APK=False,
            CSPM=False,
            DAST=False,
            SAST=True,
            SCA=False,
        ),
    )

    assert len(root_nicknames_and_configs) == 1
    assert root_nicknames_and_configs[0][0] == "testroot"
    assert root_nicknames_and_configs[0][1] == {
        "language": "EN",
        "namespace": "testroot",
        "output": {
            "file_path": ("execution_results/group1_testroot_123456.sarif"),
            "format": "SARIF",
        },
        "execution_id": "group1_testroot_123456",
        "use_report_soon": True,
        "sast": {
            "include": ["."],
            "exclude": ["glob(**/.git)"],
            "recursion_limit": 1000,
        },
    }
