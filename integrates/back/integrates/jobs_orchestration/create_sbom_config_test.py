import logging
import uuid

import integrates.jobs_orchestration.common_orchestration_utils
import integrates.jobs_orchestration.create_sbom_config
import integrates.s3.operations
from integrates.batch.enums import SkimsBatchQueue
from integrates.dataloaders import get_new_context
from integrates.db_model.credentials.types import HttpsSecret
from integrates.jobs_orchestration.create_sbom_config import (
    generate_batch_action_config_for_image,
    generate_batch_action_config_for_root,
    generate_job_image_configs,
    generate_job_root_configs,
)
from integrates.jobs_orchestration.model.types import SbomFormat, SbomJobType
from integrates.s3.operations import file_exists
from integrates.testing.aws import IntegratesAws, IntegratesDynamodb
from integrates.testing.fakers import (
    EMAIL_GENERIC,
    CredentialsFaker,
    GitRootFaker,
    GitRootStateFaker,
    GroupFaker,
    OrganizationFaker,
    RootDockerImageFaker,
    RootDockerImageStateFaker,
)
from integrates.testing.mocks import Mock, mocks

logging.basicConfig(
    level=logging.INFO,
    format="[%(levelname)s] %(message)s",
)
LOGGER = logging.getLogger(__name__)


# Constants
UUID = "1704067200"


def test_sbom_format_get_file_extension() -> None:
    assert SbomFormat.FLUID_JSON.get_file_extension() == "json"
    assert SbomFormat.CYCLONEDX_JSON.get_file_extension() == "json"
    assert SbomFormat.CYCLONEDX_XML.get_file_extension() == "xml"
    assert SbomFormat.SPDX_JSON.get_file_extension() == "json"
    assert SbomFormat.SPDX_XML.get_file_extension() == "xml"


def test_sbom_job_type_values() -> None:
    assert SbomJobType.REQUEST.value == "request"
    assert SbomJobType.SCHEDULER.value == "scheduler"


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="ORG#org1")],
            groups=[GroupFaker(name="group1", organization_id="ORG#org1")],
            roots=[
                GitRootFaker(
                    group_name="group1",
                    id="root1",
                    state=GitRootStateFaker(nickname="testroot"),
                ),
            ],
            docker_images=[
                RootDockerImageFaker(
                    uri="docker.io/test-dummy-image:0.0.1",
                    group_name="group1",
                    root_id="root1",
                    state=RootDockerImageStateFaker(credential_id="test-credential"),
                ),
                RootDockerImageFaker(
                    uri="docker.io/test-dummy-image:0.0.2",
                    group_name="group1",
                    root_id="root1",
                    state=RootDockerImageStateFaker(credential_id="test-credential"),
                ),
            ],
            credentials=[
                CredentialsFaker(
                    credential_id="test-credential",
                    organization_id="ORG#org1",
                    secret=HttpsSecret(user=EMAIL_GENERIC, password="pass"),  # noqa: S106
                ),
            ],
        ),
    ),
    others=[Mock(uuid, "uuid4", "sync", UUID)],
)
async def test_sbom_generate_batch_action_config_image() -> None:
    loaders = get_new_context()

    result = await generate_batch_action_config_for_image(
        loaders=loaders,
        group_name="group1",
        uris_with_credentials=[
            ("docker.io/test-dummy-image:0.0.1", "test-credential"),
            ("docker.io/test-dummy-image:0.0.2", None),
        ],
        sbom_format=SbomFormat.FLUID_JSON,
        job_type=SbomJobType.SCHEDULER,
        notification_id_map={
            "docker.io/test-dummy-image:0.0.1": "2024-12-19T14:43:18.412793",
            "docker.io/test-dummy-image:0.0.2": "2024-12-19T14:44:18.412793",
        },
    )
    assert result == (
        {
            "images": [
                {
                    "image_ref": "docker.io/test-dummy-image:0.0.1",
                    "root_nickname": "testroot",
                },
                {
                    "image_ref": "docker.io/test-dummy-image:0.0.2",
                    "root_nickname": "testroot",
                },
            ],
            "images_config_files": [
                "sbom_group1_docker_io_test-dummy-image_0_0_1_1704067200_config.yaml",
                "sbom_group1_docker_io_test-dummy-image_0_0_2_1704067200_config.yaml",
            ],
            "sbom_format": "fluid-json",
            "job_type": "scheduler",
            "notification_id_map": {
                "docker.io/test-dummy-image:0.0.1": "2024-12-19T14:43:18.412793",
                "docker.io/test-dummy-image:0.0.2": "2024-12-19T14:44:18.412793",
            },
        },
        "sbom_group1_scheduler_docker_image_1704067200",
        SkimsBatchQueue.SMALL,
    )


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="ORG#org1")],
            groups=[GroupFaker(name="group1", organization_id="ORG#org1")],
            roots=[
                GitRootFaker(
                    group_name="group1",
                    id="root1",
                    state=GitRootStateFaker(
                        nickname="testroot",
                    ),
                ),
                GitRootFaker(
                    group_name="group1",
                    id="root2",
                    state=GitRootStateFaker(
                        branch="test",
                        nickname="testroot2",
                    ),
                ),
            ],
        ),
    ),
    others=[
        Mock(uuid, "uuid4", "sync", UUID),
        Mock(
            integrates.jobs_orchestration.common_orchestration_utils,
            "FI_ENVIRONMENT",
            "function",
            "production",
        ),
        Mock(integrates.s3.operations, "FI_AWS_S3_PATH_PREFIX", "function", ""),
    ],
)
async def test_sbom_generate_batch_action_config_root() -> None:
    loaders = get_new_context()

    result = await generate_batch_action_config_for_root(
        loaders=loaders,
        group_name="group1",
        root_nicknames=["testroot", "testroot2"],
        sbom_format=SbomFormat.FLUID_JSON,
        job_type=SbomJobType.SCHEDULER,
        notification_id_map={
            "testroot": "2024-12-19T14:43:18.412793",
            "testroot2": "2024-12-19T14:44:18.412793",
        },
    )
    assert result == (
        {
            "notification_id_map": {
                "testroot": "2024-12-19T14:43:18.412793",
                "testroot2": "2024-12-19T14:44:18.412793",
            },
            "root_nicknames": ["testroot", "testroot2"],
            "roots_config_files": [
                "sbom_group1_testroot_1704067200_config.yaml",
                "sbom_group1_testroot2_1704067200_config.yaml",
            ],
            "sbom_format": "fluid-json",
            "job_type": "scheduler",
        },
        "sbom_group1_scheduler_root_1704067200",
        SkimsBatchQueue.SMALL,
    )
    file_path = "sbom_configs/sbom_group1_testroot_1704067200_config.yaml"
    was_file_uploaded = await file_exists(file_path, "machine.data")
    assert was_file_uploaded


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="ORG#org1")],
            groups=[GroupFaker(name="group1", organization_id="ORG#org1")],
            roots=[
                GitRootFaker(
                    group_name="group1",
                    id="root1",
                    state=GitRootStateFaker(
                        nickname="testroot",
                        gitignore=["**/maven/**", "test/", "not_deps/lock.json"],
                    ),
                ),
            ],
        ),
    )
)
async def test_generate_root_config() -> None:
    loaders = get_new_context()
    root_nicknames_and_configs = await generate_job_root_configs(
        loaders=loaders,
        group_name="group1",
        root_nicknames=["testroot"],
        batch_job_id=UUID,
        sbom_format=SbomFormat.FLUID_JSON,
    )

    assert len(root_nicknames_and_configs) == 1
    assert root_nicknames_and_configs[0][0] == "testroot"
    assert root_nicknames_and_configs[0][1] == {
        "source": "testroot",
        "output": {
            "name": "execution_results/sbom_group1_testroot_1704067200",
            "format": "fluid-json",
        },
        "source_type": "dir",
        "exclude": [
            "**/maven/**",
            "glob(**/.git/)",
            "not_deps/lock.json",
            "test/",
        ],
        "execution_id": "sbom_group1_testroot_1704067200",
    }


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="ORG#org1")],
            groups=[GroupFaker(name="group1", organization_id="ORG#org1")],
            roots=[
                GitRootFaker(
                    group_name="group1",
                    id="root1",
                    state=GitRootStateFaker(nickname="testroot"),
                ),
            ],
            docker_images=[
                RootDockerImageFaker(
                    uri="docker.io/test-dummy-image:0.0.1",
                    group_name="group1",
                    root_id="root1",
                    state=RootDockerImageStateFaker(credential_id="test-credential"),
                ),
                RootDockerImageFaker(
                    uri="docker.io/test-dummy-image:0.0.2",
                    group_name="group1",
                    root_id="root1",
                    state=RootDockerImageStateFaker(credential_id="test-credential"),
                ),
                RootDockerImageFaker(
                    uri="docker.io/test-dummy-image:0.0.3",
                    group_name="group1",
                    root_id="root1",
                    state=RootDockerImageStateFaker(credential_id=None),
                ),
            ],
            credentials=[
                CredentialsFaker(
                    credential_id="test-credential",
                    organization_id="ORG#org1",
                    secret=HttpsSecret(user=EMAIL_GENERIC, password="pass"),  # noqa: S106
                ),
                CredentialsFaker(
                    credential_id="test-credential2",
                    organization_id="ORG#org1",
                    secret=HttpsSecret(user=EMAIL_GENERIC, password="pass2"),  # noqa: S106
                ),
            ],
        ),
    )
)
async def test_generate_image_config() -> None:
    loaders = get_new_context()

    group = await loaders.group.load("group1")
    assert group
    images_and_configs = await generate_job_image_configs(
        loaders=loaders,
        group=group,
        uris_with_credentials=[
            ("docker.io/test-dummy-image:0.0.1", "test-credential"),
            ("docker.io/test-dummy-image:0.0.2", "test-credential2"),
            ("docker.io/test-dummy-image:0.0.3", None),
        ],
        batch_job_id=UUID,
        sbom_format=SbomFormat.FLUID_JSON,
    )

    assert len(images_and_configs) == 3
    assert images_and_configs[0][0] == {
        "image_ref": "docker.io/test-dummy-image:0.0.1",
        "root_nickname": "testroot",
    }
    assert images_and_configs[0][1] == {
        "source": "docker.io/test-dummy-image:0.0.1",
        "output": {
            "name": "execution_results/sbom_group1_docker_io_test-dummy-image_0_0_1_1704067200",
            "format": "fluid-json",
        },
        "source_type": "docker",
        "execution_id": "sbom_group1_docker_io_test-dummy-image_0_0_1_1704067200",
        "docker_credentials": {
            "password": "pass",
            "username": "jane@entry.com",
        },
    }
    assert images_and_configs[1][0] == {
        "image_ref": "docker.io/test-dummy-image:0.0.2",
        "root_nickname": "testroot",
    }
    assert images_and_configs[1][1] == {
        "source": "docker.io/test-dummy-image:0.0.2",
        "output": {
            "name": "execution_results/sbom_group1_docker_io_test-dummy-image_0_0_2_1704067200",
            "format": "fluid-json",
        },
        "source_type": "docker",
        "execution_id": "sbom_group1_docker_io_test-dummy-image_0_0_2_1704067200",
        "docker_credentials": {
            "password": "pass2",
            "username": "jane@entry.com",
        },
    }
    assert images_and_configs[2][0] == {
        "image_ref": "docker.io/test-dummy-image:0.0.3",
        "root_nickname": "testroot",
    }
    assert images_and_configs[2][1] == {
        "source": "docker.io/test-dummy-image:0.0.3",
        "output": {
            "name": "execution_results/sbom_group1_docker_io_test-dummy-image_0_0_3_1704067200",
            "format": "fluid-json",
        },
        "source_type": "docker",
        "execution_id": "sbom_group1_docker_io_test-dummy-image_0_0_3_1704067200",
    }


@mocks(
    aws=IntegratesAws(
        dynamodb=IntegratesDynamodb(
            organizations=[OrganizationFaker(id="ORG#org1")],
            groups=[GroupFaker(name="group1", organization_id="ORG#org1")],
            roots=[],
        ),
    )
)
async def test_generate_job_root_configs_empty_roots() -> None:
    loaders = get_new_context()
    result = await generate_job_root_configs(
        loaders=loaders,
        group_name="group1",
        root_nicknames=["testroot1"],
        batch_job_id=UUID,
        sbom_format=SbomFormat.SPDX_XML,
    )
    assert result == []
