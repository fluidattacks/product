from enum import Enum
from typing import NamedTuple


class SbomFormat(str, Enum):
    """
    Enum for SBOM formats.

    Format naming convention:
    Values should follow the pattern `name-extension`, where:
    - `name`: The specific name of the format.
    - `extension`: The format type (e.g., json, xml, etc.).

    Example: "cyclonedx-json"
    """

    FLUID_JSON: str = "fluid-json"
    CYCLONEDX_JSON: str = "cyclonedx-json"
    CYCLONEDX_XML: str = "cyclonedx-xml"
    SPDX_JSON: str = "spdx-json"
    SPDX_XML: str = "spdx-xml"

    def get_file_extension(self) -> str:
        return self.value.split("-")[-1]


class SbomJobType(str, Enum):
    REQUEST: str = "request"
    SCHEDULER: str = "scheduler"
    REQUEUE = "requeue"


class SbomJobScope(str, Enum):
    ROOT = "root"
    DOCKER_IMAGE = "docker_image"


class RootDockerImageInfo(NamedTuple):
    uri: str
    credential_id: str | None
    digest_for_update: str | None
