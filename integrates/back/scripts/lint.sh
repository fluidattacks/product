# shellcheck shell=bash

function main {
  if test -n "${CI:-}"; then
    ruff format --config ruff.toml --diff
    ruff check --config ruff.toml
  else
    ruff format --config ruff.toml
    ruff check --config ruff.toml --fix
  fi

  lint-imports --config import-linter.cfg

  mypy --config-file mypy.ini integrates
}

main "${@}"
