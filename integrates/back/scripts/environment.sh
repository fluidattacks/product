# shellcheck shell=bash

function main {
  local specific_secrets=(
    AWS_AURORA_DSQL_ENDPOINT
    AWS_AURORA_DSQL_ENDPOINT_DEV
    AWS_DYNAMODB_HOST
    AWS_DYNAMODB_HOST_DEV
    AWS_OPENSEARCH_HOST
    AWS_OPENSEARCH_HOST_DEV
    AWS_SQS_AUDIT_QUEUE_URL
    AWS_SQS_AUDIT_QUEUE_URL_DEV
    AZURE_OAUTH2_ISSUES_APP_ID
    AZURE_OAUTH2_ISSUES_APP_ID_DEV
    AZURE_OAUTH2_ISSUES_APP_ID_OLD
    AZURE_OAUTH2_ISSUES_SECRET
    AZURE_OAUTH2_ISSUES_SECRET_DEV
    AZURE_OAUTH2_ISSUES_SECRET_OLD
    BITBUCKET_OAUTH2_KEY
    BITBUCKET_OAUTH2_KEY_DEV
    BITBUCKET_OAUTH2_SECRET
    BITBUCKET_OAUTH2_SECRET_DEV
    CLOUDFLARE_API_TOKEN
    CLOUDFLARE_API_TOKEN_INTEGRATES
    CLOUDFRONT_ACCESS_KEY
    CLOUDFRONT_PRIVATE_KEY
    CLOUDFRONT_REPORTS_DOMAIN
    CLOUDFRONT_RESOURCES_DOMAIN
    DEBUG
    DEBUG_DEV
    BITBUCKET_OAUTH2_REPOSITORY_APP_ID
    BITBUCKET_OAUTH2_REPOSITORY_APP_ID_DEV
    BITBUCKET_OAUTH2_REPOSITORY_SECRET
    BITBUCKET_OAUTH2_REPOSITORY_SECRET_DEV
    EPAYCO_PUBLIC_KEY
    EPAYCO_PUBLIC_KEY_DEV
    EPAYCO_SECRET_KEY
    EPAYCO_SECRET_KEY_DEV
    FERNET_TOKEN
    GITLAB_ISSUES_OAUTH2_APP_ID
    GITLAB_ISSUES_OAUTH2_SECRET
    GITLAB_ISSUES_OAUTH2_APP_ID_PROD
    GITLAB_ISSUES_OAUTH2_SECRET_PROD
    JWT_ENCRYPTION_KEY
    JWT_ENCRYPTION_KEY_DEV
    JWT_SECRET_RS512
    JWT_SECRET_RS512_DEV
    JWT_SECRET_API_RS512
    JWT_SECRET_API_RS512_DEV
    JWT_SECRET_ES512
    JWT_SECRET_ES512_DEV
    JWT_SECRET_API_ES512
    JWT_SECRET_API_ES512_DEV
    MAIL_CONTINUOUS
    MAIL_CONTINUOUS_DEV
    MAIL_COS
    MAIL_COS_DEV
    MAIL_CTO
    MAIL_CTO_DEV
    MAIL_CXO
    MAIL_CXO_DEV
    MAIL_CUSTOMER_EXPERIENCE
    MAIL_CUSTOMER_EXPERIENCE_DEV
    MAIL_CUSTOMER_SUCCESS
    MAIL_CUSTOMER_SUCCESS_DEV
    MAIL_FINANCE
    MAIL_FINANCE_DEV
    MAIL_PRODUCTION
    MAIL_PRODUCTION_DEV
    MAIL_PROFILING
    MAIL_PROFILING_DEV
    MAIL_PROJECTS
    MAIL_PROJECTS_DEV
    MAIL_REVIEWERS
    MAIL_REVIEWERS_DEV
    MAIL_TELEMARKETING
    MAIL_TELEMARKETING_DEV
    MANUAL_CLONING_PROJECTS
    MARKETPLACE_EXTERNAL_ID
    MARKETPLACE_PRODUCT_CODE
    MARKETPLACE_PRODUCT_CODE_DEV
    MARKETPLACE_ROLE_ARN
    MIXPANEL_API_SECRET
    MIXPANEL_API_TOKEN
    MIXPANEL_PROJECT_ID
    SNOWFLAKE_ACCOUNT
    SNOWFLAKE_ACCOUNT_DEV
    SNOWFLAKE_PRIVATE_KEY
    SNOWFLAKE_PRIVATE_KEY_DEV
    SNOWFLAKE_USER
    SNOWFLAKE_USER_DEV
    STARLETTE_SESSION_KEY
    STARLETTE_SESSION_KEY_DEV
    STRIPE_API_KEY
    STRIPE_API_KEY_DEV
    STRIPE_WEBHOOK_KEY
    STRIPE_WEBHOOK_KEY_DEV
    TEST_GITHUB_API_TOKEN
    TEST_GITHUB_SSH_PRIVATE_KEY
    TEST_PROJECTS
    TEST_PROJECTS_DEV
    TEST_SSH_KEY
    TRELI_PASSWORD_DEV
    TRELI_USER_NAME_DEV
    TRELI_PASSWORD_PROD
    TRELI_USER_NAME_PROD
    ZOHO_CLIENT_ID
    ZOHO_CLIENT_SECRET
    ZOHO_DEPARTMENT_ID
    ZOHO_ORG_ID
    ZOHO_REFRESH_TOKEN
  )
  local common_secrets=(
    AZUREAD_OAUTH2_KEY
    AZURE_OAUTH2_REPOSITORY_SECRET
    AZURE_OAUTH2_REPOSITORY_APP_ID
    AZUREAD_OAUTH2_SECRET
    BUGSNAG_API_KEY_BACK
    BUGSNAG_API_KEY_SCHEDULER
    CLOUDFLARE_ACCOUNT_ID
    JWT_SECRET
    JWT_SECRET_API
    GITHUB_OAUTH2_APP_ID
    GITHUB_OAUTH2_SECRET
    GITLAB_OAUTH2_APP_ID
    GITLAB_OAUTH2_SECRET
    GOOGLE_OAUTH2_KEY
    GOOGLE_OAUTH2_SECRET
    SENDGRID_API_KEY
    TEST_ORGS
    TWILIO_ACCOUNT_SID
    TWILIO_AUTH_TOKEN
    TWILIO_VERIFY_SERVICE_SID
  )
  local env="${1}"
  local secrets="${2}"
  local finding_codes="${3}"
  local criteria="${4}"
  local compute_sizes="${5}"
  local db_design="${6}"

  case "${env:-}" in
    dev)
      local aws_role="dev"
      local specific_secrets_file="${secrets}/dev.yaml"
      export AWS_S3_PATH_PREFIX="$CI_COMMIT_REF_NAME/"
      export ENVIRONMENT="development"
      export PYTHONDEVMODE=1
      ;;
    eph)
      local aws_role="dev"
      local specific_secrets_file="${secrets}/dev.yaml"
      export AWS_S3_PATH_PREFIX="$CI_COMMIT_REF_NAME/"
      export ENVIRONMENT="development"
      ;;
    prod)
      local aws_role="prod_integrates"
      local specific_secrets_file="${secrets}/prod.yaml"
      export ENVIRONMENT="production"
      ;;
    prod-local)
      local aws_role="prod_integrates"
      local specific_secrets_file="${secrets}/prod.yaml"
      export ENVIRONMENT="production"
      export DEBUG=True
      ;;
    *)
      echo "[ERROR] First argument must be one of: dev, eph, prod, prod-local"
      return 1
      ;;
  esac

  if test -z "${CI_COMMIT_REF_NAME-}"; then
    export CI_COMMIT_REF_NAME
    CI_COMMIT_REF_NAME="$(git rev-parse --abbrev-ref HEAD)"
  fi
  if test -z "${CI_COMMIT_SHA-}"; then
    export CI_COMMIT_SHA
    CI_COMMIT_SHA="$(git rev-parse HEAD)"
  fi

  . utils-aws aws_login "${aws_role}" "3600"
  . utils-sops sops_export_vars "${secrets}/dev.yaml" "${common_secrets[@]}"
  . utils-sops sops_export_vars "${specific_secrets_file}" "${specific_secrets[@]}"

  export GIT_TERMINAL_PROMPT=0
  export MACHINE_FINDINGS="${finding_codes}"
  export INTEGRATES_DB_MODEL_PATH="${db_design}"
  export ASYNC_PROCESSING_DB_MODEL_PATH="${PWD}/integrates/batch/fi_async_processing-design.json"
  export INTEGRATES_REPORTS_LOGO_PATH="${PWD}/integrates/reports/resources/themes/background.png"
  export INTEGRATES_MAILER_TEMPLATES="${PWD}/integrates/mailer/email_templates"
  export INTEGRATES_CRITERIA_COMPLIANCE="${criteria}/src/compliance/data.yaml"
  export INTEGRATES_CRITERIA_REQUIREMENTS="${criteria}/src/requirements/data.yaml"
  export INTEGRATES_CRITERIA_VULNERABILITIES="${criteria}/src/vulnerabilities/data.yaml"
  export INTEGRATES_QUEUE_SIZES="${compute_sizes}"
  export INTEGRATES_TREE_SITTER_PARSERS="${PWD}/parsers"
  export TREE_SITTER_PARSERS="${PWD}/parsers"
  export SKIMS_FLUID_WATERMARK="${PWD}/assets/logo_img/logo_fluid_attacks_854x329.png"
  export SKIMS_ROBOTO_FONT="${PWD}/assets/__argSrcMachineAssets__/fonts/roboto_mono_from_google/regular.ttf"
  export STARTDIR="${PWD}/../../"
  export TIKTOKEN_CACHE_DIR="${PWD}/integrates/custom_utils/tiktoken_cache"
  export TZ="UTC"
}

main "${@}"
