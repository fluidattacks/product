# shellcheck shell=bash

function main {
  local npm_path="integrates/back/lint/charts/npm"
  local src_path="integrates/back/integrates/app/templates/static/graphics"

  if running_in_ci_cd_provider; then
    npm_path=__argNPMPath__
    src_path=__argSrcPath__
  fi

  : \
    && pushd "${npm_path}" \
    && npm ci \
    && export PATH="${PWD}/node_modules/.bin:${PATH}" \
    && export NODE_PATH="${PWD}/node_modules:${NODE_PATH}" \
    && popd \
    && pushd "${src_path}" \
    && eslint --config .eslintrc . \
    && popd \
    || return 1
}

main "${@}"
