{
  imports = [
    ./coverage/makes.nix
    ./dev/makes.nix
    ./deploy/worker/makes.nix
    ./test/makes.nix
  ];
}
