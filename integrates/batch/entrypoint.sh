# shellcheck shell=bash

function main {
  local login_env
  local env="${1-}"
  login_env=$(case "${env}" in
    test | hacker) echo "dev" ;;
    *) echo "${env}" ;;
  esac)

  : && export NODE_OPTIONS='--max_old_space_size=4096' \
    && if test "${env}" == 'prod'; then
      : && source __argIntegratesBackEnv__/template "${login_env}"
    elif test "${env}" == 'dev'; then
      : && trap "integrates-db stop" EXIT \
        && DAEMON=true integrates-db \
        && export AWS_S3_PATH_PREFIX="${CI_COMMIT_REF_NAME}-batch/" \
        && populate_storage "/${CI_COMMIT_REF_NAME}-batch" \
        && source __argIntegratesBackEnv__/template "${login_env}"
    elif test "${env}" == 'hacker'; then
      : && source __argIntegratesBackEnv__/template "${login_env}"
    fi \
    && pushd integrates \
    && python3 -m integrates.batch_dispatch.dispatch "${@:2}" \
    && popd \
    && if test "${env}" == 'test'; then
      rm -rf integrates
    fi \
    || return 1
}

main "${@}"
