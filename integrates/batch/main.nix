{ __system__, inputs, makeScript, outputs, ... }:
let
  version = "2024.9.346";
  architecture = {
    aarch64-linux = "arm64";
    x86_64-linux = "amd64";
  }."${__system__}";
  cloudflare-warp = inputs.nixpkgs.cloudflare-warp.overrideAttrs (_: {
    inherit version architecture;

    src = inputs.nixpkgs.fetchurl {
      url =
        "https://pkg.cloudflareclient.com/pool/noble/main/c/cloudflare-warp/cloudflare-warp_${version}.0_${architecture}.deb";
      hash = {
        aarch64-linux = "sha256-dgu/OiQPT7bkPnhrDArQg2lDAcOyhzZ5nJrjS2dqpFo=";
        x86_64-linux = "sha256-KwxLF7LWB49M+kZPJ9M4OcDSF1f3MX4S0dTtTkzQVRQ=";
      }."${__system__}";
    };
    buildInputs = [
      inputs.nixpkgs.dbus
      inputs.nixpkgs.gtk3
      inputs.nixpkgs.libpcap
      inputs.nixpkgs.openssl_3_3
      inputs.nixpkgs.nss
      inputs.nixpkgs.stdenv.cc.cc.lib
    ];
  });
in makeScript {
  replace = { __argIntegratesBackEnv__ = outputs."/integrates/back/env"; };
  name = "integrates-batch";
  searchPaths = {
    bin = [
      inputs.nixpkgs.git
      inputs.nixpkgs.noto-fonts
      inputs.nixpkgs.openssh
      inputs.nixpkgs.openssl
      inputs.nixpkgs.roboto
      inputs.nixpkgs.roboto-mono
      inputs.nixpkgs.ruby
      inputs.nixpkgs.tokei
      inputs.nixpkgs.iproute2
      outputs."/integrates/db"
    ] ++ inputs.nixpkgs.lib.optionals inputs.nixpkgs.stdenv.isLinux
      [ cloudflare-warp ];
    source = [
      outputs."/common/utils/aws"
      outputs."/common/utils/sops"
      outputs."/integrates/storage/dev/lib/populate"
    ];
  };
  entrypoint = ./entrypoint.sh;
}
