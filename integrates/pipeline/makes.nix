{ lib, projectPath, ... }:
let
  arch = let
    commit = "3c8778c732597a2fdee42d7c20d1ab5d948e13d4";
    sha256 = "sha256:0ii4qam4ykq9gbi2lmkk9p9gixmrypmz54jgcji9hpypk4azxy3w";
    url =
      "https://gitlab.com/fluidattacks/universe/-/raw/${commit}/common/ci/arch.nix";
    src = builtins.fetchurl { inherit sha256 url; };
  in import src;

  rules = {
    all = {
      default = arch.core.rules.titleRule {
        products = [ "all" "integrates[^-]" ];
        types = [ ];
      };
      noRotate = arch.core.rules.titleRule {
        products = [ "all" "integrates" ];
        types = [ "feat" "fix" "refac" ];
      };
    };
    ecosystem = {
      default = arch.core.rules.titleRule {
        products = [ "all" "integrates" ];
        types = [ ];
      };
    };
  };

  utils = import ./utils.nix { inherit lib projectPath; };

  backModules = lib.subtractLists [ "migrations" "testing" ]
    (utils.listDirectories "/integrates/back/integrates");

in {
  pipelines = {
    integratesDefault = {
      gitlabPath = "/integrates/pipeline/default.yaml";
      jobs = lib.flatten [
        {
          output = "/deployTerraform/integratesDesign";
          gitlabExtra = arch.extras.default // {
            resource_group = "deploy/$CI_JOB_NAME";
            rules = arch.rules.prod ++ [ rules.all.noRotate ];
            stage = arch.stages.deploy;
            tags = [ arch.tags.integrates ];
          };
        }
        {
          output = "/deployTerraform/integratesInfra";
          gitlabExtra = arch.extras.default // {
            resource_group = "deploy/$CI_JOB_NAME";
            rules = arch.rules.prod ++ [ rules.all.default ];
            stage = arch.stages.deploy;
            tags = [ arch.tags.integrates ];
          };
        }
        {
          output = "/integrates/back/deploy/dev";
          gitlabExtra = arch.extras.default // {
            environment = {
              name = "integrates/development/$CI_COMMIT_REF_SLUG";
              url = "https://$CI_COMMIT_REF_SLUG.app.fluidattacks.com";
            };
            rules = arch.rules.dev ++ [ rules.ecosystem.default ];
            stage = arch.stages.deploy;
            tags = [ arch.tags.integrates ];
          };
        }
        {
          output = "/integrates/back/deploy/prod";
          gitlabExtra = arch.extras.default // {
            environment = {
              name = "integrates/production";
              url = "https://app.fluidattacks.com";
            };
            resource_group = "deploy/$CI_JOB_NAME";
            rules = arch.rules.prod ++ [ rules.all.default ];
            stage = arch.stages.deploy;
            tags = [ arch.tags.integrates ];
          };
        }
        {
          output = "/integrates/secrets/test dev";
          gitlabExtra = arch.extras.default // {
            rules = arch.rules.dev ++ [ rules.all.default ];
            stage = arch.stages.test;
            tags = [ arch.tags.integrates ];
          };
        }
        {
          output = "/integrates/secrets/test prod";
          gitlabExtra = arch.extras.default // {
            rules = arch.rules.prod ++ [ rules.all.default ];
            stage = arch.stages.test;
            tags = [ arch.tags.integrates ];
          };
        }
        {
          output = "/integrates/schemas/gen_types";
          gitlabExtra = arch.extras.default // {
            rules = arch.rules.dev ++ [ rules.all.default ];
            stage = arch.stages.test;
            tags = [ arch.tags.integrates ];
          };
        }
        {
          output = "/integrates/schemas/fmt";
          gitlabExtra = arch.extras.default // {
            rules = arch.rules.dev ++ [ rules.all.default ];
            stage = arch.stages.test;
            tags = [ arch.tags.integrates ];
          };
        }
        {
          output = "/integrates/schemas/export";
          gitlabExtra = arch.extras.default // {
            rules = arch.rules.dev ++ [ rules.all.default ];
            stage = arch.stages.test;
            tags = [ arch.tags.integrates ];
          };
        }
        {
          output = "/integrates/back/test/unit changes_db";
          gitlabExtra = arch.extras.default // {
            artifacts = {
              paths = [ "integrates/.coverage*" ];
              expire_in = "1 week";
            };
            rules = arch.rules.dev ++ [ rules.all.noRotate ];
            stage = arch.stages.test;
            tags = [ arch.tags.integrates ];
          };
        }
        {
          output = "/integrates/back/test/unit not_changes_db";
          gitlabExtra = arch.extras.default // {
            artifacts = {
              paths = [ "integrates/.coverage*" ];
              expire_in = "1 week";
            };
            rules = arch.rules.dev ++ [ rules.all.noRotate ];
            stage = arch.stages.test;
            tags = [ arch.tags.integrates ];
          };
        }
        rec {
          output = "/integrates/charts/documents";
          gitlabExtra = arch.extras.default // {
            artifacts = {
              expire_in = "1 week";
              paths = [ "integrates/charts" ];
              when = "on_success";
            };
            parallel = 7;
            rules = arch.rules.dev ++ [ rules.ecosystem.default ];
            script = [
              "m . /integrates/charts/documents dev ${
                toString gitlabExtra.parallel
              } gitlab"
            ];
            stage = arch.stages.test;
            tags = [ arch.tags.integrates ];
          };
        }
        {
          output = "/integrates/back/lint/schema";
          gitlabExtra = arch.extras.default // {
            rules = arch.rules.dev ++ [ rules.all.noRotate ];
            stage = arch.stages.test;
            tags = [ arch.tags.integrates ];
          };
        }
        {
          output = "/integrates/back/lint/schema/deprecations";
          gitlabExtra = arch.extras.default // {
            rules = arch.rules.dev ++ [ rules.all.noRotate ];
            stage = arch.stages.test;
            tags = [ arch.tags.integrates ];
          };
        }
        {
          output = "/integrates/back/lint/charts";
          gitlabExtra = arch.extras.default // {
            rules = arch.rules.dev ++ [ rules.all.noRotate ];
            stage = arch.stages.test;
            tags = [ arch.tags.integrates ];
          };
        }
        {
          output = "/integrates/secrets/lint";
          gitlabExtra = arch.extras.default // {
            rules = arch.rules.dev ++ [ rules.all.default ];
            stage = arch.stages.test;
            tags = [ arch.tags.integrates ];
          };
        }
        {
          output = "/integrates/machine/sca";
          gitlabExtra = arch.extras.default // {
            rules = arch.rules.dev ++ [ rules.ecosystem.default ];
            script = [
              "m . /skims scan $PWD/integrates/machine_configs/sca_config.yaml"
            ];
            stage = arch.stages.test;
            tags = [ arch.tags.integrates ];
          };
        }
        {
          output = "/integrates/back/lint";
          gitlabExtra = arch.extras.default // {
            cache = {
              key = "$CI_COMMIT_REF_NAME-integrates-back-lint";
              paths = [
                "integrates/back/.ruff_cache"
                "integrates/back/.import_linter_cache"
                "integrates/back/.mypy_cache"
              ];
            };
            rules = arch.rules.dev ++ [ rules.all.noRotate ];
            stage = arch.stages.test;
            tags = [ arch.tags.integrates ];
          };
        }
        {
          output = "/integrates/jobs/execute_machine/lint";
          gitlabExtra = arch.extras.default // {
            cache = {
              key = "$CI_COMMIT_REF_NAME-integrates-jobs-execute_machine-lint";
              paths = [
                "integrates/jobs/execute_machine/.ruff_cache"
                "integrates/jobs/execute_machine/.import_linter_cache"
                "integrates/jobs/execute_machine/.mypy_cache"
              ];
            };
            rules = arch.rules.dev ++ [ rules.all.noRotate ];
            stage = arch.stages.test;
            tags = [ arch.tags.integrates ];
          };
        }
        {
          output = "/integrates/jobs/execute_machine_sast/lint";
          gitlabExtra = arch.extras.default // {
            cache = {
              key =
                "$CI_COMMIT_REF_NAME-integrates-jobs-execute_machine_sast-lint";
              paths = [
                "integrates/jobs/execute_machine_sast/.ruff_cache"
                "integrates/jobs/execute_machine_sast/.import_linter_cache"
                "integrates/jobs/execute_machine_sast/.mypy_cache"
              ];
            };
            rules = arch.rules.dev ++ [ rules.all.noRotate ];
            stage = arch.stages.test;
            tags = [ arch.tags.integrates ];
          };
        }
        {
          output = "/integrates/jobs/execute_sbom/lint";
          gitlabExtra = arch.extras.default // {
            cache = {
              key = "$CI_COMMIT_REF_NAME-integrates-jobs-execute_sbom-lint";
              paths = [
                "integrates/jobs/execute_sbom/.ruff_cache"
                "integrates/jobs/execute_sbom/.import_linter_cache"
                "integrates/jobs/execute_sbom/.mypy_cache"
              ];
            };
            rules = arch.rules.dev ++ [ rules.all.noRotate ];
            stage = arch.stages.test;
            tags = [ arch.tags.integrates ];
          };
        }
        {
          output = "/lintTerraform/integratesDesign";
          gitlabExtra = arch.extras.default // {
            rules = arch.rules.dev ++ [ rules.all.noRotate ];
            stage = arch.stages.test;
            tags = [ arch.tags.integrates ];
          };
        }
        {
          output = "/lintTerraform/integratesInfra";
          gitlabExtra = arch.extras.default // {
            rules = arch.rules.dev ++ [ rules.all.noRotate ];
            stage = arch.stages.test;
            tags = [ arch.tags.integrates ];
          };
        }
        {
          output = "/integrates/streams/lint";
          gitlabExtra = arch.extras.default // {
            cache = {
              key = "$CI_COMMIT_REF_NAME-integrates-streams-lint";
              paths = [
                "integrates/streams/.ruff_cache"
                "integrates/streams/.mypy_cache"
              ];
            };
            rules = arch.rules.dev ++ [ rules.all.noRotate ];
            stage = arch.stages.test;
            tags = [ arch.tags.integrates ];
          };
        }
        {
          output = "/lintWithAjv/integratesStreamsTriggers";
          gitlabExtra = arch.extras.default // {
            rules = arch.rules.dev ++ [ rules.all.noRotate ];
            stage = arch.stages.test;
            tags = [ arch.tags.integrates ];
          };
        }
        {
          output = "/pipelineOnGitlab/integratesDefault";
          gitlabExtra = arch.extras.default // {
            rules = arch.rules.dev ++ [ rules.all.noRotate ];
            stage = arch.stages.test;
            tags = [ arch.tags.integrates ];
          };
        }
        {
          output = "/testTerraform/integratesDesign";
          gitlabExtra = arch.extras.default // {
            rules = arch.rules.dev ++ [ rules.all.noRotate ];
            stage = arch.stages.test;
            tags = [ arch.tags.integrates ];
          };
        }
        {
          output = "/testTerraform/integratesInfra";
          gitlabExtra = arch.extras.default // {
            rules = arch.rules.dev ++ [ rules.all.noRotate ];
            stage = arch.stages.test;
            tags = [ arch.tags.integrates ];
          };
        }
        {
          output = "/integrates/back/coverage";
          gitlabExtra = arch.extras.default // {
            dependencies =
              builtins.map (module: "/integrates/back/test ${module}")
              backModules;
            needs = null;
            rules = arch.rules.dev ++ [ rules.all.noRotate ];
            stage = arch.stages.deploy;
            tags = [ arch.tags.integrates ];
            variables.GIT_DEPTH = 1000;
          };
        }
        (builtins.map (module: {
          output = "/integrates/back/test ${module}";
          gitlabExtra = arch.extras.default // {
            artifacts = {
              paths = [ "integrates/back/.coverage.*" ];
              expire_in = "1 week";
              when = "on_success";
            };
            rules = arch.rules.dev ++ [ rules.all.noRotate ];
            stage = arch.stages.test;
            tags = [ arch.tags.integrates ];
          };
        }) backModules)
        (builtins.map (args: {
          inherit args;
          output = "/integrates/streams/test/functional";
          gitlabExtra = arch.extras.default // {
            artifacts = {
              paths = [ "integrates/.coverage*" ];
              expire_in = "1 day";
              when = "on_success";
            };
            rules = arch.rules.dev ++ [ rules.all.noRotate ];
            stage = arch.stages.test;
            tags = [ arch.tags.integrates ];
          };
        }) (utils.functionalTests "${utils.streamsPath}/src"))
        (builtins.map (args: {
          inherit args;
          output = "/integrates/streams/test/unit";
          gitlabExtra = arch.extras.default // {
            artifacts = {
              paths = [ "integrates/.coverage*" ];
              expire_in = "1 week";
            };
            rules = arch.rules.dev ++ [ rules.all.noRotate ];
            stage = arch.stages.test;
            tags = [ arch.tags.integrates ];
          };
        }) utils.streamsUnitTests)
      ];
    };
  };
}
