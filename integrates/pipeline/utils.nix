{ lib, projectPath, ... }: rec {
  listDirectories = path:
    let
      content = builtins.readDir (projectPath path);
      directories = lib.filterAttrs
        (key: value: value == "directory" && !lib.hasPrefix "__" key) content;
    in builtins.attrNames directories;
  min = x: y: if x <= y then x else y;
  max = x: y: if x <= y then y else x;
  slice = offset: len: xs:
    let
      remaining = max 0 (builtins.length xs - offset);
      len' = if len == null then remaining else min (max len 0) remaining;
    in builtins.genList (i: builtins.elemAt xs (offset + i)) len';

  functionalTests = path: builtins.map (test: [ test ]) (listDirectories path);

  backPath = "/integrates/back/test/functional";
  streamsPath = "/integrates/streams/test/functional";
  backFunctionalTestsFirstGroup = slice 0
    (builtins.div (builtins.length (functionalTests "${backPath}/src")) 2)
    (functionalTests "${backPath}/src");
  backFunctionalTestsSecondGroup =
    slice (builtins.div (builtins.length (functionalTests "${backPath}/src")) 2)
    null (functionalTests "${backPath}/src");
  backSrcModules = listDirectories "/integrates/back/integrates";
  chartsSrcModules = listDirectories "/integrates/charts/generators";
  streamsUnitTests = builtins.map (test: [ test ])
    (listDirectories "/integrates/streams/test/unit/src");
  gitlabJobDependencies = 40;
  backFunctionalCoverageCombine =
    builtins.genList (x: [ (builtins.toString (x + 1)) ]) (builtins.ceil
      ((builtins.length (functionalTests "${backPath}/src"))
        / (gitlabJobDependencies * 1.0)));
  streamsFunctionalCoverageCombine = builtins.genList (x:
    [
      (builtins.toString
        (x + 1 + builtins.length backFunctionalCoverageCombine))
    ]) (builtins.ceil ((builtins.length (functionalTests "${streamsPath}/src"))
      / (gitlabJobDependencies * 1.0)));

  createPathString = path: test: "${path}__${builtins.elemAt test 0}";
  calculateSublist = args: offsetFactor: testsList:
    (slice (((lib.strings.toInt (builtins.elemAt args 0)) - 1 - offsetFactor)
      * gitlabJobDependencies) gitlabJobDependencies testsList);
  mapTestsToPaths = args: path: offsetFactor: testsList:
    (builtins.map (createPathString path)
      (calculateSublist args offsetFactor testsList));
}
