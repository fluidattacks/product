{ inputs, makeScript, outputs, ... }:
makeScript {
  name = "integrates-web-wait";
  searchPaths = {
    bin = [ inputs.nixpkgs.kubectl ];
    source = [ outputs."/common/utils/aws" ];
  };
  entrypoint = ./entrypoint.sh;
}
