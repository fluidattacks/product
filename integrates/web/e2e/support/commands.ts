import { IntegratesCookies } from "./cookies";
import { generateJWE } from "./utils";

const CLOSE_TOUR_DIALOG_DEFAULT_WAIT = 5000;

export const overrideCommands = () => {
  Cypress.Commands.overwrite("log", function (log, ...args) {
    if (Cypress.browser.isHeadless) {
      return cy.task("log", args, { log: false }).then(() => {
        return log(...args);
      });
    } else {
      console.log(...args);
      return log(...args);
    }
  });
};

export const configureCustomCommands = () => {
  Cypress.Commands.add("setUserSession", (jwt: string) => {
    cy.setCookie(IntegratesCookies.session, jwt);
  });

  Cypress.Commands.add("acceptCookiesIfAsked", () => {
    const acceptButtonId = "#CybotCookiebotDialogBodyButtonAccept";
    if (Cypress.$(acceptButtonId).length) {
      cy.get(acceptButtonId).click();
      return;
    }
    cy.log("Cookie accept dialog not displayed. Skipping...");
  });

  Cypress.Commands.add("loginByGoogleApi", () => {
    cy.log("Logging in to Google");
    cy.request({
      method: "POST",
      url: "https://www.googleapis.com/oauth2/v4/token",
      body: {
        grant_type: "refresh_token",
        client_id: Cypress.env("GOOGLE_OAUTH2_KEY"),
        client_secret: Cypress.env("GOOGLE_OAUTH2_SECRET"),
        refresh_token: Cypress.env("GOOGLE_REFRESH_TOKEN"),
      },
    }).then(({ body }) => {
      const { access_token, id_token } = body;

      cy.request({
        method: "GET",
        url: "https://www.googleapis.com/oauth2/v3/userinfo",
        headers: { Authorization: `Bearer ${access_token}` },
      }).then(({ body }) => {
        const userItem = {
          token: id_token,
          user: {
            googleId: body.sub,
            email: body.email,
            givenName: body.given_name,
            familyName: body.family_name,
            imageUrl: body.picture,
          },
        };

        window.localStorage.setItem("googleCypress", JSON.stringify(userItem));
        cy.visit("/home", { failOnStatusCode: false });
      });
    });
  });

  Cypress.Commands.add(
    "bypassLogin",
    (user: string, firstName?: string, lastName?: string) => {
      return cy.wrap(null).then(async () => {
        const jwt = await generateJWE(user, firstName, lastName);
        cy.setUserSession(jwt);
        return jwt;
      });
    }
  );

  Cypress.Commands.add(
    "shouldNotBeClickable",
    { prevSubject: "element" },
    (subject, done, { position, timeout = 100, ...clickOptions } = {}) => {
      cy.once("fail", (err) => {
        expect(err.message).to.include(
          "`cy.click()` failed because this element"
        );
        expect(err.message).to.include("is being covered by another element");
        done();
      });
      const chainable = position
        ? cy.wrap(subject).click(position, { timeout, ...clickOptions })
        : cy.wrap(subject).click({ timeout, ...clickOptions });
      chainable.then(() =>
        done(new Error("Expected element not to be clickable"))
      );
    }
  );

  Cypress.Commands.add(
    "closeTourDialogIfShown",
    (
      params: CloseTourParams = {
        wait: CLOSE_TOUR_DIALOG_DEFAULT_WAIT,
        withParent: true,
      }
    ) => {
      const parentId = "#close-tour-container";
      const elementId = "#close-tour";
      cy.wait(params.wait);
      cy.get("div").then(() => {
        if (params.withParent)
          if (Cypress.$(parentId).length) {
            cy.get(parentId).find(elementId).click();
            return;
          } else if (Cypress.$(elementId).length) {
            cy.get(elementId).click();
            return;
          }

        cy.log("Tour dialog not displayed. Skipping...");
      });
    }
  );

  Cypress.Commands.add("vulnPolicyIsAlreadyHandled", (vuln: string) => {
    const len = Cypress.$(`:contains("${vuln}")`).length;
    return cy.wrap(!!len);
  });

  Cypress.Commands.add(
    "prepareSession",
    (user: string, session: string | undefined) => {
      if (!session) {
        return cy.bypassLogin(user);
      } else {
        cy.setUserSession(session);
        return cy.wrap(session);
      }
    }
  );

  Cypress.Commands.add("appVisit", (user, session, url) => {
    cy.prepareSession(user, session).then((jwt) => (session = jwt));
    cy.visit(url);
    cy.acceptCookiesIfAsked();
  });

  Cypress.Commands.add("toggleNewsletterNotifications", () => {
    cy.get("#newsletter_tooltip")
      .find("div:contains('Email')")
      .children()
      .last()
      .click();
  });
};
