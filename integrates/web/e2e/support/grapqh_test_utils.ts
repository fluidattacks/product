const pascalToCamel = (str: string) => {
  return str.charAt(0).toLowerCase() + str.slice(1);
};

export const hasOperationName = (req: any, operationName: string) => {
  const { body } = req;
  return (
    Object.prototype.hasOwnProperty.call(body, "operationName") &&
    body.operationName === operationName
  );
};

export const aliasQuery = (req: any, operationName: string) => {
  if (hasOperationName(req, operationName)) {
    req.alias = `gql${operationName}Query`;
  }
};

export const aliasMutation = (req: any, operationName: string) => {
  if (hasOperationName(req, operationName)) {
    req.alias = `gql${operationName}Mutation`;
  }
};

export const assertMutationSuccess = (
  mutationAlias: string,
  responseField?: string
) => {
  cy.wait(`@gql${mutationAlias}Mutation`)
    .its("response")
    .then((response) => {
      expect(response?.statusCode).to.eq(200);
      expect(
        response?.body.data[responseField ?? pascalToCamel(mutationAlias)]
          .success
      ).to.be.true;
    });
};

export const assertMutationFailure = (mutationAlias: string) => {
  cy.wait(`@gql${mutationAlias}Mutation`)
    .its("response")
    .then((response) => {
      expect(response?.statusCode).to.eq(200);
      expect(response?.body.errors).to.have.length.above(0);
    });
};
