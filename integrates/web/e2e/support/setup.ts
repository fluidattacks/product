import "cypress-localstorage-commands";
import "@cypress/code-coverage/support";
import compareSnapshotCommand from "cypress-image-diff-js/command";
import { configureKeyboard } from "./keyboard";
import { manageExceptions } from "./exceptions";
import { configureCustomCommands, overrideCommands } from "./commands";

configureCustomCommands();
overrideCommands();
manageExceptions();
configureKeyboard();
compareSnapshotCommand();
