export const manageExceptions = () => {
  Cypress.on("uncaught:exception", () => {
    return false;
  });
};
