interface CloseTourParams {
  wait: number;
  withParent: boolean;
}

declare namespace Cypress {
  interface Chainable<Subject> {
    acceptCookiesIfAsked(): Chainable<Subject>;
    appVisit(
      user: string,
      session: string | undefined,
      url: string
    ): Chainable<Subject>;
    bypassLogin(
      user: string,
      firsName?: string,
      lastName?: string
    ): Chainable<Subject>;
    closeTourDialogIfShown(
      closeTourParams?: CloseTourParams
    ): Chainable<Subject>;
    loginByGoogleApi(): Chainable<Subject>;
    setUserSession(jwt: string): Chainable<Subject>;
    shouldNotBeClickable(
      done: Mocha.Done,
      clickOptions?: Partial<Cypress.ClickOptions> & {
        position?: Cypress.PositionType;
      }
    ): Chainable<Element>;
    vulnPolicyIsAlreadyHandled(vuln: string): Chainable<Subject>;
    prepareSession(
      user: string,
      session: string | undefined
    ): Chainable<Subject>;
    toggleNewsletterNotifications(): Chainable<Subject>;
  }
}
