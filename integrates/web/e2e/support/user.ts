enum CiNode {
  CI_NODE_1 = 1,
  CI_NODE_2,
  CI_NODE_3,
  CI_NODE_4,
  CI_NODE_5,
  CI_NODE_6,
  CI_NODE_7,
  CI_NODE_8,
  CI_NODE_9,
  CI_NODE_10,
}

const CI_NODE: CiNode = Number(Cypress.env("CI_NODE_INDEX"));
const CI = Cypress.env("CI");

enum IntegratesUsers { // sub index indicates ci node
  integratesmanager_n1,
  integratesmanager_n3,
  integratesmanager_n4,
  integratesmanager_n5,
  integratesmanager_n7,
  integratesmanager_n8,
  integratesmanager_n10,
  continuoushack2_n2,
  continuoushacking_n3,
  integratesmanager_gmail_n4,
  integrateshacker_n5,
  integratesuser_n6,
  integratesuser_n9,
  integratesuser2_n8,
  integratesadmin_n6,
  org_resourcer_integrates_n8,
}

const USER_MAIL_MAPS = new Map<IntegratesUsers, string>();
USER_MAIL_MAPS.set(
  IntegratesUsers.integratesmanager_n1,
  "integratesmanager@fluidattacks.com"
);
USER_MAIL_MAPS.set(
  IntegratesUsers.integratesmanager_n3,
  "integratesmanager@fluidattacks.com" // same value, different ci node
);
USER_MAIL_MAPS.set(
  IntegratesUsers.integratesmanager_n4,
  "integratesmanager@fluidattacks.com"
);
USER_MAIL_MAPS.set(
  IntegratesUsers.integratesmanager_n5,
  "integratesmanager@fluidattacks.com"
);
USER_MAIL_MAPS.set(
  IntegratesUsers.integratesmanager_n7,
  "integratesmanager@fluidattacks.com"
);
USER_MAIL_MAPS.set(
  IntegratesUsers.integratesmanager_n8,
  "integratesmanager@fluidattacks.com"
);
USER_MAIL_MAPS.set(
  IntegratesUsers.integratesmanager_n10,
  "integratesmanager@fluidattacks.com"
);
USER_MAIL_MAPS.set(
  IntegratesUsers.continuoushack2_n2,
  "continuoushack2@gmail.com"
);
USER_MAIL_MAPS.set(
  IntegratesUsers.continuoushacking_n3,
  "continuoushacking@gmail.com"
);
USER_MAIL_MAPS.set(
  IntegratesUsers.integratesmanager_gmail_n4,
  "integratesmanager@gmail.com"
);
USER_MAIL_MAPS.set(
  IntegratesUsers.integrateshacker_n5,
  "integrateshacker@fluidattacks.com"
);
USER_MAIL_MAPS.set(
  IntegratesUsers.integratesuser_n6,
  "integratesuser@gmail.com"
);
USER_MAIL_MAPS.set(
  IntegratesUsers.integratesuser2_n8,
  "integratesuser2@fluidattacks.com"
);
USER_MAIL_MAPS.set(
  IntegratesUsers.integratesadmin_n6,
  "integratesadmin@fluidattacks.com"
);
USER_MAIL_MAPS.set(
  IntegratesUsers.integratesuser_n9,
  "integratesuser@gmail.com" // same user, different node
);
USER_MAIL_MAPS.set(
  IntegratesUsers.org_resourcer_integrates_n8,
  "org-resourcer@integrates.com"
);

const CI_USER_MAP = new Map<CiNode, IntegratesUsers[]>();
CI_USER_MAP.set(CiNode.CI_NODE_1, [IntegratesUsers.integratesmanager_n1]);
CI_USER_MAP.set(CiNode.CI_NODE_2, [IntegratesUsers.continuoushack2_n2]);
CI_USER_MAP.set(CiNode.CI_NODE_3, [
  IntegratesUsers.continuoushacking_n3,
  IntegratesUsers.integratesmanager_n3,
]);
CI_USER_MAP.set(CiNode.CI_NODE_4, [
  IntegratesUsers.integratesmanager_gmail_n4,
  IntegratesUsers.integratesmanager_n4,
]);
CI_USER_MAP.set(CiNode.CI_NODE_5, [
  IntegratesUsers.integrateshacker_n5,
  IntegratesUsers.integratesmanager_n5,
]);
CI_USER_MAP.set(CiNode.CI_NODE_6, [
  IntegratesUsers.integratesuser_n6,
  IntegratesUsers.integratesuser2_n8,
  IntegratesUsers.integratesadmin_n6,
]);

CI_USER_MAP.set(CiNode.CI_NODE_7, [IntegratesUsers.integratesmanager_n7]);
CI_USER_MAP.set(CiNode.CI_NODE_8, [
  IntegratesUsers.integratesmanager_n8,
  IntegratesUsers.integratesuser2_n8,
  IntegratesUsers.org_resourcer_integrates_n8,
]);
CI_USER_MAP.set(CiNode.CI_NODE_9, [IntegratesUsers.integratesuser_n9]);
CI_USER_MAP.set(CiNode.CI_NODE_10, [IntegratesUsers.integratesmanager_n10]);

const runForUsers = (
  users: IntegratesUsers[],
  test: (user: string) => undefined
) => {
  users.forEach((user) => {
    if (userEnabledForCiNode(user)) test(getUserEmail(user));
  });
};

const userEnabledForCiNode = (user: IntegratesUsers) => {
  if (!CI) return true;
  return CI_USER_MAP.get(CI_NODE)?.includes(user);
};

const getUserEmail = (user: IntegratesUsers): string => {
  return USER_MAIL_MAPS.get(user) as string;
};

export { getUserEmail, IntegratesUsers, runForUsers, userEnabledForCiNode };
