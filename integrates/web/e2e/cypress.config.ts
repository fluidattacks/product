import { defineConfig } from "cypress";

const { CI_COMMIT_REF_NAME, CI, CI_NODE_INDEX } = process.env;
import codeCoverageTask from "@cypress/code-coverage/task";
import vitePreprocessor from "cypress-vite";
import getCompareSnapshotsPlugin from "cypress-image-diff-js/plugin";

export default defineConfig({
  e2e: {
    viewportHeight: 1080,
    viewportWidth: 1920,
    baseUrl: CI
      ? `https://${CI_COMMIT_REF_NAME}.app.fluidattacks.com`
      : "https://localhost:8001",
    retries: {
      openMode: 0,
      runMode: 2,
    },
    setupNodeEvents(on, config) {
      on("task", {
        log(args) {
          console.log(...args);
          return null;
        },
        deleteFile(file) {
          require("fs").unlinkSync(file);
          return null;
        },
      });
      on(
        "file:preprocessor",
        vitePreprocessor({
          mode: "development",
        })
      );
      getCompareSnapshotsPlugin(on, config);
      codeCoverageTask(on, config);
      return config;
    },
    specPattern: "./specs/**",
    supportFile: "support/setup.ts",
    testIsolation: true,
    defaultCommandTimeout: 15000,
    fixturesFolder: "./fixtures/",
  },
  env: {
    CI_NODE_INDEX,
    CI,
  },
});
