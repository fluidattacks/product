const deleteMailmapEntry = (entryEmail: string) => {
  cy.log("Force mailmap entry removal");
  const query = `mutation deleteMailmapEntry($entryEmail: String!, $organizationId: String!) {
    deleteMailmapEntry(
      entryEmail: $entryEmail
      organizationId: $organizationId
    ) {
      success
    }
  }`;
  const variables = {
    entryEmail: entryEmail,
    organizationId: "38eb8f25-7945-4173-ab6e-0af4ad8b7ef3",
  };
  cy.request({
    body: {
      query,
      variables,
    },
    method: "POST",
    url: "/api",
  });
};

const deleteMailmapEntrySubentries = (entryEmail: string) => {
  cy.log("Force mailmap entry removal");
  const query = `mutation deleteMailmapEntrySubentries($entryEmail: String!, $organizationId: String!) {
    deleteMailmapEntry(
      entryEmail: $entryEmail
      organizationId: $organizationId
    ) {
      success
    }
  }`;
  const variables = {
    entryEmail: entryEmail,
    organizationId: "38eb8f25-7945-4173-ab6e-0af4ad8b7ef3",
  };
  cy.request({
    body: {
      query,
      variables,
    },
    method: "POST",
    url: "/api",
  });
};

export { deleteMailmapEntry, deleteMailmapEntrySubentries };
