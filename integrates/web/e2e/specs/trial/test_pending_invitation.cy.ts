import { createStakeholder } from "../../support/stakeholder";
import { IntegratesUsers, runForUsers } from "../../support/user";

describe("Test trial", () => {
  const creator = IntegratesUsers.integratesmanager_n4;
  runForUsers([creator], (user) => {
    it(`Test not autoenrollment (${user})`, (done) => {
      const stakeholder = createStakeholder(creator, true);
      cy.appVisit(stakeholder, undefined, "/home");
      cy.contains("You have pending access invitation(s)", { timeout: 22000 });
      cy.contains("Add your repository manually").shouldNotBeClickable(done, {
        waitForAnimations: false,
      });
      cy.contains("Log out").click({ force: true });
    });
  });
});
