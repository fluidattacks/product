import { IntegratesUsers, runForUsers } from "../../support/user";
const EXPECTED_MANY_GROUPS_CHARTS = [
  "Remediation rate benchmark",
  "Mean time to remediate (MTTR) benchmark",
  "Exposure by type",
  "Exposure by group",
  "Exposure benchmark",
  "Exposure management over time",
  "Exposure management over time (%)",
  "Exposure trends by vulnerability category",
  "Vulnerabilities treatment",
  "Total types",
  "Days until zero exposure",
  "Vulnerabilities being re-attacked",
  "Days since last remediation",
  "Sprint exposure increment",
  "Sprint exposure decrement",
  "Sprint exposure change overall",
  "Total vulnerabilities",
  "Active resources distribution",
  "Vulnerabilities by tag",
  "Vulnerabilities by level",
  "Accepted vulnerabilities by user",
  "Unsolved events by group",
  "Distribution of vulnerabilities by group",
  "Vulnerability types by group",
  "Open vulnerability types by group",
  "Oldest vulnerability types",
  "Vulnerabilities by group",
  "Open vulnerabilities by group",
  "Undefined treatment by group",
  "Report technique",
  "Vulnerabilities by assignment",
  "Status of assigned vulnerabilities",
  "Accepted vulnerabilities by CVSS severity",
  "Exposure by assignee",
  "Files with open vulnerabilities in the last 20 weeks",
  "Mean time to remediate (MTTR) by CVSS severity",
  "Overall availability of groups",
  "Days since groups are failing",
  "Mean time to request reattacks",
  "Tags by groups",
];

describe("Test organization analytics", () => {
  let session: string | undefined;
  runForUsers([IntegratesUsers.integratesmanager_n4], (user) => {
    const downloadedChart =
      "cypress/downloads/charts-organization-ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3.png";
    const downloadedCsv =
      "cypress/downloads/ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3-Remediation rate benchmark.csv";
    const downloadedHtml =
      "cypress/downloads/Remediation rate benchmark-ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3-1000x400.html";
    it(`Test org analytics (${user})`, () => {
      cy.appVisit(user, session, "/orgs/okada/analytics");
      const org1 = "2338eb8f25-7945-4173-ab6e-0af4ad8b7ef3";
      const org2 = "2333c08ebd-2068-47e7-9673-e1aa03dc9448";
      const graphic =
        "/graphic?documentName=mttrBenchmarkingCvssf&documentType=barChart&" +
        "entity=organization&generatorName=generic&generatorType=c3&height=320";

      const graphicForOrg =
        "/graphics-for-organization?reportMode=true&bgChange=true";

      const graphicForPortfolio =
        "/graphics-for-portfolio?reportMode=true&bgChange=true";

      EXPECTED_MANY_GROUPS_CHARTS.forEach((item) => {
        cy.contains(item, { timeout: 20000 });
      });
      // download vuln chart
      cy.contains("Download Analytics").click();
      cy.readFile(downloadedChart).should("exist");

      cy.contains("Remediation rate benchmark").trigger("mouseover");
      cy.get("#information-button").should("be.visible");
      cy.get("#csv-button").click();
      cy.readFile(downloadedCsv).should("exist");
      cy.get("#download-button").click();
      cy.readFile(downloadedHtml).should("exist");
      cy.get("#refresh-button").click();
      cy.get("#expand-button").should("be.visible");

      cy.visit(`${graphic}&subject=ORG%${org1}_30&width=1055`);

      cy.get("#root");
      cy.visit(`${graphic}&subject=ORG%${org1}_90&width=1055`);
      cy.get("#root");
      cy.visit(`${graphic}&subject=ORG%${org1}&width=1055`);
      cy.get("#root");
      cy.visit(`${graphicForOrg}&organization=ORG%${org2}`);
      cy.get("#root");
      EXPECTED_MANY_GROUPS_CHARTS.forEach((item) => {
        cy.contains(item);
      });

      cy.visit(
        `${graphicForPortfolio}&portfolio=ORG%${org1}PORTFOLIO%23test-groups`
      );
      cy.get("#root");
      EXPECTED_MANY_GROUPS_CHARTS.forEach((item) => {
        cy.contains(item);
      });
    });
    afterEach(() => {
      const files = [downloadedChart, downloadedCsv, downloadedHtml];
      files.forEach((file) => {
        cy.task("deleteFile", file);
      });
    });
  });
});
