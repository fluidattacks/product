import { IntegratesUsers, runForUsers } from "../../support/user";

describe("Test org ztna logs", () => {
  runForUsers([IntegratesUsers.integratesmanager_n1], (user) => {
    it(`Test org ztna logs (${user})`, () => {
      cy.appVisit(user, undefined, "/orgs/okada/logs");
      cy.contains("Http");
      cy.contains("Network");
      cy.contains("Session");
      cy.get("#session-tab").click();
      cy.contains("Start Date");
      cy.contains("End Date");
      cy.get("#network-tab").click();
      cy.contains("Date");
      cy.contains("Email");
      cy.get("#http-tab").click();
      cy.contains("Destination IP");
      cy.contains("Source IP");
    });
  });
});
