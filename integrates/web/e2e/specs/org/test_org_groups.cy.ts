import {
  aliasMutation,
  assertMutationSuccess,
} from "../../support/grapqh_test_utils";
import { IntegratesUsers, runForUsers } from "../../support/user";

describe("Test org groups", () => {
  runForUsers([IntegratesUsers.integratesmanager_n4], (user) => {
    beforeEach(() => {
      cy.intercept("POST", "/api", (req) => {
        aliasMutation(req, "UpdateGroupInfo");
        aliasMutation(req, "RemoveGroupMutation");
      });
    });
    it(`Test org groups (${user})`, () => {
      cy.appVisit(user, undefined, "/orgs/okada/groups");
      const groupName = `akame${Math.floor(Math.random() * 9999 + 4999)}`;
      const groupDescription = "Some test group description";
      cy.get("#add-group").click();
      cy.closeTourDialogIfShown();
      cy.get("[name='name']").type(groupName);
      cy.get("input[name='description']").type(groupDescription);
      cy.get("#modal-confirm").click();
      cy.contains("Group created successfully");
      cy.visit(`/orgs/okada/groups/${groupName}/scope`);
      cy.get("[name='businessId']").type("91827364");
      cy.get("[name='businessName']").type("Test Business Name");
      cy.get("input[name='description']")
        .clear()
        .type("Some modified description");
      cy.get("[name='sprintDuration']").clear().type("5");
      cy.contains("Continue").click();
      assertMutationSuccess("UpdateGroupInfo");
      cy.get("#delete-group-button").click();
      cy.get("[name='confirmation']").type(
        groupName[0].toUpperCase() + groupName.slice(1)
      );
      cy.get("[name='comments']").type(
        "This group will be deleted for test retry"
      );
      cy.get("#modal-confirm").click();
      cy.contains("Please select a valid option");
      cy.get("input[name='reason']").click();
      cy.get("[data-key='TR_CANCELLED']").click();
      cy.get("#modal-confirm").click();
      assertMutationSuccess("RemoveGroupMutation", "removeGroup");
      cy.contains("Services changed correctly");
    });
  });
});
