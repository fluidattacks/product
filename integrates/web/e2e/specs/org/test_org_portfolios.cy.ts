import { IntegratesUsers, runForUsers } from "../../support/user";
const EXPECTED_MANY_GROUPS_CHARTS = [
  "Remediation rate benchmark",
  "Mean time to remediate (MTTR) benchmark",
  "Exposure by type",
  "Exposure by group",
  "Exposure benchmark",
  "Exposure management over time",
  "Exposure management over time (%)",
  "Exposure trends by vulnerability category",
  "Vulnerabilities treatment",
  "Total types",
  "Days until zero exposure",
  "Vulnerabilities being re-attacked",
  "Days since last remediation",
  "Sprint exposure increment",
  "Sprint exposure decrement",
  "Sprint exposure change overall",
  "Total vulnerabilities",
  "Active resources distribution",
  "Vulnerabilities by tag",
  "Vulnerabilities by level",
  "Accepted vulnerabilities by user",
  "Unsolved events by group",
  "Distribution of vulnerabilities by group",
  "Vulnerability types by group",
  "Open vulnerability types by group",
  "Oldest vulnerability types",
  "Vulnerabilities by group",
  "Open vulnerabilities by group",
  "Undefined treatment by group",
  "Report technique",
  "Vulnerabilities by assignment",
  "Status of assigned vulnerabilities",
  "Accepted vulnerabilities by CVSS severity",
  "Exposure by assignee",
  "Files with open vulnerabilities in the last 20 weeks",
  "Mean time to remediate (MTTR) by CVSS severity",
  "Overall availability of groups",
  "Days since groups are failing",
  "Mean time to request reattacks",
  "Tags by groups",
];

describe("Test org portfolios", () => {
  runForUsers([IntegratesUsers.integratesmanager_n1], (user) => {
    it(`Test org portfolios (${user})`, () => {
      cy.appVisit(user, undefined, "/orgs/okada/portfolios");
      cy.contains("test-groups").click();
      EXPECTED_MANY_GROUPS_CHARTS.forEach((item) => {
        cy.contains(item);
      });
    });
  });
});
