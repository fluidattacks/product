import {
  aliasMutation,
  assertMutationSuccess,
} from '../../support/grapqh_test_utils';
import { IntegratesUsers, runForUsers } from '../../support/user';

describe('Test move environment', () => {
  runForUsers([IntegratesUsers.integratesmanager_n1], (user) => {
    beforeEach(() => {
      cy.intercept('POST', '/api', (req) => {
        aliasMutation(req, 'MoveEnvironment');
      });
    });
    it(`Test request to move an environment from root (${user})`, () => {
      const targetRoot = 'https://gitlab.com/fluidattacks/universe.git';
      const targetEnvironment = 'https://test.com';
      cy.appVisit(user, undefined, '/orgs/okada/groups/unittesting/scope');

      // Access to environment
      cy.contains(targetRoot, { timeout: 18000 }).click();
      cy.get('[data-testid="edit-root-modal"]').get('#environments').click();
      cy.get('[data-testid="edit-root-modal"]')
        .contains(targetEnvironment)
        .parentsUntil('tbody')
        .find('#git-root-move-environment-url')
        .click();

      // Fill the form
      const formModal = cy.contains('Move environment').parent().parent();
      formModal.get("input[name='targetGroupName']").click();
      formModal.get("[data-key='unittesting']").click();
      formModal.get("[data-testid='search']").clear().type('universe').blur();

      cy.wait(1000);

      formModal.get("input[name='targetRootId']").click();
      formModal
        .get("[data-key='4039d098-ffc5-4984-8ed3-eb17bca98e19']")
        .click();
      formModal.get('#move-environment-confirm').click();

      // Confirm the action
      const confirmModal = cy.get("[data-testid='move-root-conditions']");
      confirmModal.get("input[name='closeVulns']").parent().click();
      confirmModal.get("input[name='moveSecrets']").parent().click();
      confirmModal.get('button#move-root-conditions').click();

      assertMutationSuccess('MoveEnvironment');
    });
  });
});
