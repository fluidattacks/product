import {
  aliasMutation,
  assertMutationFailure,
  assertMutationSuccess,
} from "../../support/grapqh_test_utils";
import { IntegratesUsers, runForUsers } from "../../support/user";

describe("Test ip roots", () => {
  runForUsers([IntegratesUsers.integratesmanager_n1], (user) => {
    beforeEach(() => {
      cy.intercept("POST", "/api", (req) => {
        aliasMutation(req, "AddIpRoot");
        aliasMutation(req, "ActivateRoot");
        aliasMutation(req, "DeactivateRoot");
      });
    });
    it(`Test ip roots (${user})`, () => {
      const ipRootAddress = "192.168.50.111";
      cy.appVisit(user, undefined, "/orgs/okada/groups/oneshottest/scope");
      cy.contains("IP Roots");
      cy.contains("Add new root").click();
      cy.get("input[name='address']").type(ipRootAddress);
      cy.get("input[name='nickname']").type("some_test_ip_root");
      cy.get("#modal-confirm").click();
      assertMutationFailure("AddIpRoot"); // should fail for existiip root
      cy.get("#modal-close").click();
      // deactivate ip root
      cy.contains(ipRootAddress)
        .parentsUntil("tr")
        .siblings()
        .find("#ip-root-stateToggle")
        .click();
      cy.get("input[name='reason']").click();
      cy.get("[data-key='REGISTERED_BY_MISTAKE']").click();
      cy.get("#modal-confirm").click();
      cy.contains("Confirm change");
      cy.contains("Deactivating this root");
      cy.get("#modal-confirm:enabled").last().click();
      assertMutationSuccess("DeactivateRoot");
      cy.contains(ipRootAddress)
        .parentsUntil("tr")
        .siblings()
        .find("input[name='ip-root-state']")
        .should("not.be.checked");
      cy.contains(ipRootAddress)
        .parentsUntil("tr")
        .siblings()
        .find("#ip-root-stateToggle")
        .click();
      assertMutationSuccess("ActivateRoot");
    });
  });
});
