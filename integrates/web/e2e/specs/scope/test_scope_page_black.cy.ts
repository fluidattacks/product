import { IntegratesUsers, runForUsers } from "../../support/user";

describe("Test scope page", () => {
  runForUsers([IntegratesUsers.continuoushacking_n3], (user) => {
    it(`Test scope page for black plan (${user})`, () => {
      cy.appVisit(user, undefined, "/orgs/okada/groups/oneshottest/scope");
      cy.contains("IP Roots", { timeout: 22000 });
      cy.contains("URL Roots");
      cy.contains("Files");
      cy.contains("Portfolio");
      cy.contains("Information");
      cy.contains("Policies");
      cy.contains("Group Settings");
      cy.contains("Group context");
      cy.contains("Disambiguation");
      cy.contains("Unsubscribe");
      cy.contains("Delete this group");
    });
  });
});
