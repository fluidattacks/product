import { IntegratesUsers, runForUsers } from "../../support/user";

const key = "test_env_key_1";

const forceInitSecretState = () => {
  cy.log("Force initial secret state");
  const query = `mutation RemoveSecret($groupName: String!, $key: String!, $resourceId: ID!, $resourceType: ResourceType!) {
  removeSecret(
    groupName: $groupName
    key: $key
    resourceId: $resourceId
    resourceType: $resourceType
  ) {
    success
    __typename
  }
}`;
  const variables = {
    groupName: "unittesting",
    key,
    resourceId: "00fbe3579a253b43239954a545dc0536e6c83094",
    resourceType: "URL",
  };
  cy.request({
    body: {
      query,
      variables,
    },
    method: "POST",
    url: "/api",
  });
};

describe("Test secrets", () => {
  runForUsers([IntegratesUsers.integratesuser_n6], (user) => {
    it(`Test add, update and delete operations for environment url secret (${user})`, () => {
      cy.appVisit(user, undefined, "/orgs/okada/groups/unittesting/scope");
      cy.contains("Git Roots", { timeout: 22000 });
      cy.contains("https://app.fluidattacks.com/test").click();
      cy.get("#add-secret").click();
      cy.get("[name=key]").type(key);
      cy.get("[name=value]").type("1234");
      cy.get("[name=description]")
        .filter(":visible")
        .filter("[aria-label=description]")
        .first()
        .type("Some test key");
      cy.get("#git-root-add-secret").click();
      cy.contains("Added secret");
      cy.contains(key)
        .parentsUntil("tr")
        .last()
        .siblings()
        .find("#edit-secret")
        .click();
      cy.get("[name=value]").clear().type("4321");
      cy.get("[name=description]")
        .filter(":visible")
        .filter("[aria-label=description]")
        .first()
        .clear()
        .type("Some updated test description");
      cy.get("#git-root-add-secret").click();
      cy.contains("Updated secret").wait(1000);
      cy.contains(key)
        .parentsUntil("tr")
        .last()
        .siblings()
        .find("#git-root-remove-secret")
        .click();
      cy.get("#modal-confirm").click();
      cy.contains("Removed secret");
    });
    afterEach(() => {
      forceInitSecretState();
    });
  });
});
