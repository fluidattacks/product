import {
  aliasMutation,
  assertMutationSuccess,
} from "../../support/grapqh_test_utils";
import { IntegratesUsers, runForUsers } from "../../support/user";

const key = "test_key_1";

const forceInitSecretState = () => {
  cy.log("Force initial secret state");
  const query = `mutation RemoveSecret(
      $groupName: String!,
      $key: String!,
      $resourceId: ID!,
      $resourceType: ResourceType!
  ) {
  removeSecret(
    groupName: $groupName
    key: $key
    resourceId: $resourceId
    resourceType: $resourceType
  ) {
    success
    __typename
  }
}`;
  const variables = {
    groupName: "oneshottest",
    key,
    resourceId: "8493c82f-2860-4902-86fa-75b0fef76034",
    resourceType: "ROOT",
  };
  cy.request({
    body: {
      query,
      variables,
    },
    method: "POST",
    url: "/api",
  });
};

describe("Test secrets", () => {
  runForUsers([IntegratesUsers.continuoushacking_n3], (user) => {
    beforeEach(() => {
      cy.intercept("POST", "/api", (req) => {
        aliasMutation(req, "AddSecret");
        aliasMutation(req, "updateSecret");
        aliasMutation(req, "RemoveSecret");
      });
    });

    it(`Test add, update and delete operations for url root secret (${user})`, () => {
      cy.appVisit(user, undefined, "/orgs/okada/groups/oneshottest/scope");
      cy.contains("IP Roots", { timeout: 22000 });
      cy.contains("app.fluidattacks.com").click();
      cy.get("#secrets").click();
      cy.get("#add-secret").click();
      cy.get("[name=key]").type(key);
      cy.get("[name=value]").type("1234");
      cy.get("[name=description]")
        .filter(":visible")
        .filter("[aria-label=description]")
        .first()
        .type("Some test key");
      cy.get("#git-root-add-secret").click();
      assertMutationSuccess("AddSecret");
      cy.contains("Added secret");
      cy.contains(key)
        .parentsUntil("tr")
        .last()
        .siblings()
        .find("#edit-secret")
        .click();
      cy.get("[name=value]").clear().type("4321");
      cy.get("[name=description]")
        .filter(":visible")
        .filter("[aria-label=description]")
        .first()
        .clear()
        .type("Some updated test description");
      cy.get("#git-root-add-secret").click();
      assertMutationSuccess("updateSecret");
      cy.contains("Updated secret").wait(1000);
      cy.contains(key)
        .parentsUntil("tr")
        .last()
        .siblings()
        .find("#git-root-remove-secret")
        .click();
      cy.wait(1000);
      cy.get("#modal-confirm").click();
      assertMutationSuccess("RemoveSecret");
      cy.contains("Removed secret");
    });
    afterEach(() => {
      forceInitSecretState();
    });
  });
});
