import { IntegratesUsers, runForUsers } from "../../support/user";

describe("Test scope page", () => {
  runForUsers([IntegratesUsers.integratesuser_n6], (user) => {
    it(`Test scope page for white plan (${user})`, () => {
      cy.appVisit(user, undefined, "/orgs/okada/groups/unittesting/scope");
      cy.contains("Git Roots", { timeout: 22000 });
      cy.contains("Environments");
      cy.contains("Files");
      cy.contains("Portfolio");
      cy.contains("Information");
      cy.contains("Policies");
      cy.contains("Group Settings");
      cy.contains("Group context");
      cy.contains("Disambiguation");
      cy.contains("DevSecOps agent");
      cy.contains("Unsubscribe");
      cy.contains("Delete this group");
    });
  });
});
