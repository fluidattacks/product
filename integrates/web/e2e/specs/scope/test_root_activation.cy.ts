import {
  aliasMutation,
  assertMutationSuccess,
} from "../../support/grapqh_test_utils";
import { IntegratesUsers, runForUsers } from "../../support/user";

describe("Test root activation", () => {
  runForUsers([IntegratesUsers.integratesmanager_n1], (user) => {
    beforeEach(() => {
      cy.intercept("POST", "/api", (req) => {
        aliasMutation(req, "DeactivateRoot");
        aliasMutation(req, "ActivateRoot");
      });
    });
    it(`Test root deactivate and reactivate (${user})`, () => {
      const targetRoot = "https://gitlab.com/fluidattacks/integrates";
      cy.appVisit(user, undefined, "/orgs/kamiya/groups/barranquilla/scope");
      // Deactivate root
      cy.contains(targetRoot, { timeout: 18000 })
        .parentsUntil("tr")
        .siblings()
        .find("input[name='root-status']")
        .parent()
        .parent()
        .should("include.text", "Active");
      cy.contains(targetRoot)
        .parentsUntil("tr")
        .siblings()
        .find("#root-statusToggle")
        .click();
      cy.get("input[name='reason']").click();
      cy.get("[data-key='OTHER']").click();
      cy.get('[name="other"]').type("A very convincing reason");
      cy.get("#modal-confirm").click();
      cy.contains("Deactivating this root")
        .parent()
        .siblings()
        .find("#modal-confirm")
        .click();
      assertMutationSuccess("DeactivateRoot");
      // Reactivate root
      cy.contains(targetRoot)
        .parentsUntil("tr")
        .siblings()
        .find("input[name='root-status']")
        .parentsUntil("tr")
        .should("include.text", "Inactive");
      cy.contains(targetRoot)
        .parentsUntil("tr")
        .siblings()
        .find("#root-statusToggle")
        .click();
      cy.get("#modal-confirm").click();
      assertMutationSuccess("ActivateRoot");
      cy.contains(targetRoot)
        .parentsUntil("tr")
        .siblings()
        .find("input[name='root-status']")
        .parentsUntil("tr")
        .should("include.text", "Active");
    });
  });
});
