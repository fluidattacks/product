import { IntegratesUsers, runForUsers } from "../../support/user";

describe("Test scope", () => {
  runForUsers([IntegratesUsers.integratesmanager_n4], (user) => {
    it(`Test upload git root file (${user})`, () => {
      cy.appVisit(user, undefined, "/orgs/okada/groups/unittesting/scope");
      cy.contains("Import");
      cy.get("#git-root-upload-file").click();
      cy.fixture("upload_git_root_file.csv", null).as("rootData");
      cy.get("input[type=file]").selectFile("@rootData", {
        action: "select",
        force: true,
      });
      cy.get("#modal-confirm:enabled").click();
      cy.contains("https://gitlab.com/fluidattacks/demo");
    });
  });
});
