import { IntegratesUsers, runForUsers } from "../../../support/user";

describe("Test group events", () => {
  runForUsers([IntegratesUsers.integratesmanager_n10], (user) => {
    it(`Test group events search (${user})`, () => {
      cy.appVisit(user, undefined, "/orgs/okada/groups/unittesting/events");
      cy.get("[name=search]")
        .first()
        .type("This is an eventuality with evidence");
      cy.get("tbody:visible")
        .should("contain", "This is an eventuality with evidence")
        .and("have.length", 1);
      cy.contains("Authorization for a special attack");
      cy.get("[name=search]").first().clear();
      cy.get("#filterBtn").click();
      cy.get("[data-testid='li-filter-option-Type']").click();
      cy.get('#filters-options div[style*="overflow"]').scrollTo('bottom');
      cy.get('li[data-testid="Missing supplies"] input[type="checkbox"]').click();
      cy.get('button').contains('Apply').click();
      cy.get('#modal-close').click();

      cy.get("tbody:visible")
        .should("contain", "test test test")
        .and("have.length", 1);
    });
  });
});
