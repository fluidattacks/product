import { IntegratesUsers, runForUsers } from "../../../support/user";

describe("Test group events", () => {
  runForUsers([IntegratesUsers.integratesmanager_n10], (user) => {
    it(`Test group events export (${user})`, () => {
      cy.appVisit(user, undefined, "/orgs/okada/groups/unittesting/events");
      cy.contains("Export", { timeout: 16000 }).click();
      cy.readFile("cypress/downloads/Report.csv").should("exist");
    });
    afterEach(() => {
      // delete the downloaded file
      cy.task("deleteFile", "cypress/downloads/Report.csv");
    });
  });
});
