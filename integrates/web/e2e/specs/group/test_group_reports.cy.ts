import { IntegratesUsers, runForUsers } from "../../support/user";

describe("Test group reports", () => {
  runForUsers([IntegratesUsers.integratesmanager_n10], (user) => {
    it(`Test group reports (${user})`, () => {
      cy.appVisit(user, undefined, "/orgs/okada/groups/unittesting/vulns");
      cy.get("#reports", { timeout: 22000 }).click();
      cy.get("#report-excel").click();
      cy.contains("SMS").click();
      cy.contains("A verification code has been sent");
      cy.get("[name='verificationCode']").type("0000");
      cy.get("#verify").click();
    });
  });
});
