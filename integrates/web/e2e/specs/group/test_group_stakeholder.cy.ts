import { IntegratesUsers, runForUsers } from "../../support/user";

describe("Test group stakeholder", () => {
  runForUsers([IntegratesUsers.integratesmanager_n3], (user) => {
    it(`Test group stakeholder (${user})`, () => {
      cy.appVisit(user, undefined, "/orgs/okada/groups/unittesting/members");
      cy.contains("User email");
      const email = "continuoushacking@gmail.com";
      cy.screenshot();
      cy.get("[name='search']")
        .last()
        .type(email.substring(0, 8), { animationDistanceThreshold: 20, timeout: 3200 });
      cy.contains(email);
      cy.get("[name='search']").clear();
      cy.contains("Registration status");
    });
  });
});
