import { IntegratesUsers, runForUsers } from "../../support/user";

describe("Test group scope files", () => {
  runForUsers([IntegratesUsers.continuoushack2_n2], (user) => {
    it(`Test group scope files (${user})`, () => {
      cy.appVisit(user, undefined, "/orgs/okada/groups/unittesting/scope");
      cy.contains("test.zip");
    });
  });
});
