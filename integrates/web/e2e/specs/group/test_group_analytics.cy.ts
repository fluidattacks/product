import { IntegratesUsers, runForUsers } from "../../support/user";

describe("Test group analytics", () => {
  runForUsers([IntegratesUsers.integratesmanager_n7], (user) => {
    it(`Test group analytics (${user})`, () => {
      Cypress.config("defaultCommandTimeout", 18000);
      cy.appVisit(user, undefined, "/orgs/okada/groups/unittesting/analytics");
      const expectedCharts = [
        "Mean time to remediate (MTTR) benchmark",
        "Exposure by type",
        "Exposure benchmark",
        "Exposure management over time",
        "Exposure management over time (%)",
        "Exposure trends by vulnerability category",
        "Vulnerabilities treatment",
        "Total types",
        "Days until zero exposure",
        "Vulnerabilities being re-attacked",
        "Days since last remediation",
        "Sprint exposure increment",
        "Sprint exposure decrement",
        "Sprint exposure change overall",
        "Total vulnerabilities",
        "Total exclusions",
        "Exclusions by root",
        "Active resources distribution",
        "Vulnerabilities by tag",
        "Vulnerabilities by level",
        "Accepted vulnerabilities by user",
        "Vulnerabilities by assignment",
        "Status of assigned vulnerabilities",
        "Report technique",
        "Group availability",
        "Accepted vulnerabilities by CVSS severity",
        "Exposure by assignee",
        "Files with open vulnerabilities in the last 20 weeks",
        "Mean time to remediate (MTTR) by CVSS severity",
        "Days since group is failing",
        "Mean time to request reattacks",
        "Findings by tags",
        "Your commitment towards security",
        "Builds risk",
      ];
      expectedCharts.forEach((item) => {
        (() => cy.contains(item))();
      });

      const expectedChartsTools = [
        "#filter-button",
        '[href="https://help.fluidattacks.com/portal/en/kb/articles/common-analytics#Exposure_management_over_time"]',
        '[download="unittesting-Exposure management over time.csv"]',
        "#buttonghostrefresh-button-tooltip",
        "#buttonghostexpand-button-tooltip",
        '[download="Exposure management over time-unittesting-1000x400.html"]',
      ];

      cy.contains("Exposure management over time").trigger("mouseover");

      expectedChartsTools.forEach((item) => {
        (() => cy.get(item).should("be.visible"))();
      });
      cy.get("#filter-button-list-items-wrapper").parent().invoke("show");
      cy.get("[data-testid='analytics.limitData.ninetyDays']").should("be.visible");
      cy.get("[data-testid='analytics.limitData.ninetyDays']").click();

      cy.visit(
        `/graphics-for-group?reportMode=true&bgChange=true&group=unittesting`
      );
      cy.get("#root");
      expectedCharts.forEach((item) => {
        (() => cy.contains(item))();
      });
    });
  });
});
