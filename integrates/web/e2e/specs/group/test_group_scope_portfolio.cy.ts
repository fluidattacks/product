import { IntegratesUsers, runForUsers } from "../../support/user";

describe("Test group scope portfolio", () => {
  let session: string | undefined;
  runForUsers([IntegratesUsers.continuoushack2_n2], (user) => {
    it(`Test group scope portfolio (${user})`, () => {
      cy.appVisit(user, session, "/orgs/okada/groups/unittesting/scope");
      const tagName = `test-portfolio${Math.floor(
        Math.random() * 9999 + 4999
      )}`;
      cy.get("#portfolio-add").click();
      cy.get("input").last().type(tagName)
      cy.get("#portfolio-add-confirm").click();
      cy.contains(tagName);
    });
  });
});
