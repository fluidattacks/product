import { IntegratesUsers, runForUsers } from '../../support/user';

describe('Test finding vulnerabilities', () => {
  let session: string | undefined;
  runForUsers([IntegratesUsers.integratesmanager_n7], (user) => {
    it(`Test finding vulnerabilities (${user})`, () => {
      cy.appVisit(user, session, '/orgs/okada/groups/unittesting/vulns');
      cy.get('#reports');
      cy.contains(
        '060. Insecure service configuration - Host verification'
      ).click();
      cy.url().should('include', '/locations');
      cy.contains('test/data/lib_path/f060/csharp.cs').click();
      cy.url().should(
        'include',
        '/locations/0a848781-b6a4-422e-95fa-692151e6a98f'
      );
      cy.contains('Details');
      cy.contains('Expiration');
      cy.get('#trackingtreatments');
      cy.contains('2020-01-03');
      cy.contains('In progress');
      cy.contains('Assigned');
      cy.contains('integratesuser2');
      cy.contains('How to fix', { timeout: 10000 }).click();
      cy.contains(
        'Custom guide not currently available for this vulnerability.',
        { timeout: 10000 }
      );
      cy.get('#modal-close').click();
      cy.contains('Assigned').should('not.exist');
      cy.url().should('include', '/locations');
      cy.get("#vulns input[type='checkbox']")
        .not(':checked')
        .eq(1)
        .parent()
        .click();
      cy.get('#vulnerabilities-notify').click();
      cy.get('#modal-confirm').click();
      cy.contains('vulnerability notification email sent successfully');
      cy.contains('test/data/lib_path/f060/csharp.cs');
    });
  });
});
