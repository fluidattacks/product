import { IntegratesUsers, runForUsers } from "../../support/user";

describe("Test finding description", () => {
  let session: string | undefined;
  runForUsers(
    [IntegratesUsers.integratesmanager_n5, IntegratesUsers.continuoushack2_n2],
    (user) => {
      it(`Test finding description (${user})`, () => {
        cy.appVisit(user, session, "/orgs/okada/groups/unittesting/vulns");
        cy.contains("Treatment").should("not.exist");
        cy.get("#ColumnsToggleBtn").click();
        cy.contains("Manage columns");
        cy.contains("Risk and severity").click();
        cy.contains("Time and tracking").click();
        cy.contains("Vulnerability management").click();
        cy.get("#columns-buttons input[type='checkbox']")
          .not(":checked")
          .each((checkbox) => checkbox.parent().trigger("click"));
        cy.get("#modal-close").click();
        cy.contains("Manage columns").should("not.exist");
        cy.contains("Treatment");
        cy.contains(
          "060. Insecure service configuration - Host verification"
        ).click();
        cy.closeTourDialogIfShown();
        cy.get("#infoItem").click();
        cy.contains(
          "266. The organization must disable or carefully control the insecure" +
          " functions of a system (system hardening)."
        );
      });
    }
  );
});
