import { IntegratesUsers, runForUsers } from "../../support/user";

describe("Test finding evidence", () => {
  let session: string | undefined;
  runForUsers([IntegratesUsers.integratesmanager_n5], (user) => {
    beforeEach(() => {
      Cypress.config("defaultCommandTimeout", 22000);
    });
    it(`Test finding evidence (${user})`, () => {
      cy.appVisit(user, session, "/orgs/okada/groups/unittesting/vulns");
      cy.get("#reports");
      cy.contains(
        "060. Insecure service configuration - Host verification"
      ).click();
      cy.get("#evidenceItem").click();
      cy.get("#evidenceItem").click();
      cy.get("[data-testid='finding-evidences']")
        .find("[data-testid='evidence-preview']")
        .its("length")
        .should("eq", 7);
      cy.contains("exception");
    });
  });
});
