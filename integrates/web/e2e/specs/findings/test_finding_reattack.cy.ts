import { IntegratesUsers, runForUsers } from '../../support/user';

describe('Test finding reattack', () => {
  let session: string | undefined;
  runForUsers([IntegratesUsers.integratesmanager_n7], (user) => {
    beforeEach(() => {
      Cypress.config('defaultCommandTimeout', 22000);
    });
    it(`Test finding reattack (${user})`, () => {
      cy.appVisit(user, session, '/orgs/okada/groups/unittesting/vulns');
      cy.get('#reports');
      cy.contains('014. Insecure functionality').click();

      cy.get('tbody:visible tr').its('length').should('be.gte', 1);
      cy.get('button#manage-vulns').click();
      cy.get('button#reattack-switch').click();

      cy.get('tbody:visible tr:first-child input[type="checkbox"]')
        .parent()
        .click();

      cy.get('button#confirm-reattack').click();

      const formModal = cy.contains('Justification').parent().parent();

      formModal
        .get("textarea[name='treatmentJustification']")
        .clear()
        .type('Working on it...');

      cy.contains('Which was the applied solution?');
      formModal.get('#modal-confirm-cancel').click();
      cy.contains('Justification').should('not.exist');
      cy.contains('192.168.1.19');
      cy.contains('192.168.1.20');
    });
  });
});
