import { aliasMutation } from "../../support/grapqh_test_utils";
import { IntegratesUsers, runForUsers } from "../../support/user";

describe("Test to do vuln severity", () => {
  runForUsers([IntegratesUsers.integratesmanager_n5], (user) => {
    beforeEach(() => {
      cy.intercept("POST", "/api", (req) => {
        aliasMutation(req, "ApproveVulnerabilitiesSeverity");
        aliasMutation(req, "RejectVulnerabilitiesSeverity");
      });
    });
    it(`Test to do severity requests management (${user})`, () => {
      cy.appVisit(user, undefined, "/todos");
      cy.get("#vulnSeverityRequests").click();
      cy.url().should("include", "/todos/vuln-severity-requests");

      cy.get("#vulnerabilitiesTable-severityRequestsTbl")
        .children()
        .find("tbody")
        .children()
        .should("have.length", 2);
    });
  });
});
