import { IntegratesUsers, runForUsers } from "../../support/user";
describe("Test surface", () => {
  runForUsers([IntegratesUsers.integratesmanager_n1], (user) => {
    it(`Test surface page (${user})`, () => {
      cy.appVisit(
        user,
        undefined,
        "/orgs/okada/groups/unittesting/surface/lines"
      );
      cy.get("#ColumnsToggleBtn").click();
      cy.contains("Manage columns");
      cy.get("#modal-close").click();
      cy.contains("Manage columns").should("not.exist");
      cy.contains("f9e4beb");
      cy.get("#toe-inputs-tab").click();
      cy.contains("idTest (en-us)");
      cy.get("#toe-ports-tab").click();
      cy.get("#toe-languages-tab").click();
      cy.contains("Python");
      cy.contains("Ruby");
    });
  });
});
