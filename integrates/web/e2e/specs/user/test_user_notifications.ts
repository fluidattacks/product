import {
  aliasMutation,
  assertMutationSuccess,
} from "../../support/grapqh_test_utils";
import { IntegratesUsers, runForUsers } from "../../support/user";

describe("Test user notifications", () => {
  runForUsers(
    [
      IntegratesUsers.continuoushacking_n3,
      IntegratesUsers.integratesmanager_n1,
      IntegratesUsers.integratesuser_n6,
    ],
    (user) => {
      beforeEach(() => {
        cy.intercept("POST", "/api", (req) => {
          aliasMutation(req, "UpdateNotificationsPreferences");
        });
      });
      it(`Test user notifications setup (${user})`, () => {
        cy.appVisit(user, undefined, "/orgs/okada");
        cy.get("#navbar-user-profile", { timeout: 32000 }).click();
        cy.contains("Notifications").click({ force: true });
        cy.toggleNewsletterNotifications(); //enable notifications
        assertMutationSuccess("UpdateNotificationsPreferences");
        cy.toggleNewsletterNotifications(); //disable notifications
        assertMutationSuccess("UpdateNotificationsPreferences");
      });
    }
  );
});
