import { IntegratesUsers, runForUsers } from "../../support/user";
import { toTypeableDatePlusDays } from "../../support/utils";

describe("Test user token", () => {
  runForUsers([IntegratesUsers.continuoushacking_n3], (user) => {
    beforeEach(() => Cypress.config("defaultCommandTimeout", 22000));
    it(`Test user token generation (${user})`, () => {
      const expDate = toTypeableDatePlusDays(new Date(), 10);
      const typeDate = [
        expDate.substring(5, 7),
        expDate.substring(8),
        expDate.substring(0, 5)
      ];

      cy.appVisit(user, undefined, "/orgs/okada");
      cy.get("#navbar-user-profile").click();
      cy.contains("API token").click({ force: true });
      cy.contains("Access token");
      cy.get("#add-api-token").click();
      cy.get(`[aria-label="name"]`).type("Test token");
      cy.get(`[aria-label="Expiration date"]`)
        .within(() => {
          cy.get("[role='spinbutton']").eq(0).type(typeDate[0])
          cy.get("[role='spinbutton']").eq(1).type(typeDate[1])
          cy.get("[role='spinbutton']").eq(2).type(typeDate[2])
        });
      cy.get("#modal-confirm").click();
      cy.contains("Please save this access token ");
      cy.get("#reveal-token").click({ force: true });
      cy.get(`[aria-label="sessionJwt"]`).should("contain.text", "eyJ");
      cy.contains("A new access token has been added");
      // revoke oldest token so test is retriable
      cy.get("#modal-close").click({ force: true });
      cy.get("#navbar-user-profile").click();
      cy.contains("API token").click({ force: true });
      cy.contains("Access token");
      cy.contains("Revoke").last().click();
      cy.get("#modal-confirm").click();
      cy.contains("Token invalidated successfully");
    });
  });
});
