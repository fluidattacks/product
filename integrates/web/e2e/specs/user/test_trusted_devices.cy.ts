import { IntegratesUsers, runForUsers } from "../../support/user";

describe("Test trusted devices", () => {
  runForUsers([IntegratesUsers.continuoushack2_n2], (user) => {
    it(`Test existence of trusted devices (${user})`, () => {
      cy.appVisit(user, undefined, "/orgs/okada");
      cy.get("#navbar-user-profile", { timeout: 72000 }).click();
      cy.contains("Trusted devices").click({ force: true });
      cy.url().should("include", "/trusted-devices");
      cy.contains("ubuntu").should("be.visible");
      cy.contains("Toronto").should("be.visible");
      cy.contains("firefox").should("be.visible");
    });
  });
});
