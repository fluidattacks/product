# shellcheck shell=bash

function resolve_endpoint() {
  if ! test -z "${CI-}"; then
    endpoint="https://${CI_COMMIT_REF_NAME}.app.fluidattacks.com/api"
  else
    endpoint="https://localhost:8001/api"
  fi
}

function main {
  local cluster="common-k8s"
  local region="us-east-1"

  : && aws_login "dev" "3600" \
    && if test -n "${CI-}"; then
      aws_eks_update_kubeconfig "${cluster}" "${region}" \
        && kubectl rollout status \
          "deploy/integrates-${CI_COMMIT_REF_NAME}" \
          -n "dev" \
          --timeout=0
    fi \
    && sops_export_vars __argIntegratesSecrets__/secrets/dev.yaml \
      TEST_FORCES_TOKEN \
    && resolve_endpoint \
    && echo "[INFO] Running DevSecOps agent lax empty report check..." \
    && API_ENDPOINT="${endpoint}" forces --token "${TEST_FORCES_TOKEN}" --lax -v --breaking 6.0 --repo-name universe \
    && echo "[INFO] Running DevSecOps agent feature preview check..." \
    && API_ENDPOINT="${endpoint}" forces --token "${TEST_FORCES_TOKEN}" --feature-preview \
    && echo "[INFO] Running DevSecOps repo path check..." \
    && API_ENDPOINT="${endpoint}" forces --token "${TEST_FORCES_TOKEN}" --lax -v --repo-path "universe/path/to/npm/**" \
    && echo "[INFO] Running DevSecOps multiple repo paths check..." \
    && API_ENDPOINT="${endpoint}" forces --token "${TEST_FORCES_TOKEN}" --lax -v --repo-path "universe/path/to/npm/**" --repo-path "universe/test/**" \
    && echo "[INFO] Running DevSecOps agent strict check..." \
    && API_ENDPOINT="${endpoint}" forces --token "${TEST_FORCES_TOKEN}" --strict -vv --breaking 10.0 \
    || return 1
}

main "${@}"
