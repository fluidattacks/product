# shellcheck shell=bash

function execute_analytics_generator {
  local generator="${1}"
  local results_dir="${generator//.py/}"
  local prefix="back/"
  local module

  mkdir -p "${results_dir}" \
    && module=$(echo "${generator//.py/}" | sed -e s#^$prefix## -e 's#/#.#g') \
    && echo "[INFO] Running: ${module} ${generator}" \
    && RESULTS_DIR="${results_dir}" python3 'back/integrates/cli/invoker.py' "${module}.main"
}

function main {
  local env="${1}"
  local parallel="${2}"
  local runtime="${3}"
  local todo

  : && if test "${env}" = "dev"; then
    : && trap "integrates-db stop" EXIT \
      && DAEMON=true integrates-db \
      && export AWS_S3_PATH_PREFIX="${CI_COMMIT_REF_NAME}-charts-documents/" \
      && populate_storage "/${CI_COMMIT_REF_NAME}-charts-documents" \
      && source __argIntegratesBackEnv__/template "${env}" \
      && export AWS_S3_MAIN_BUCKET="integrates.dev"
  elif test "${env}" = "prod"; then
    : && source __argIntegratesBackEnv__/template "${env}" \
      && export AWS_S3_MAIN_BUCKET="integrates"
  else
    error "Only 'dev' and 'prod' allowed for env."
  fi \
    && pushd integrates \
    && todo=$(mktemp) \
    && find "back/integrates/charts/generators" ! -name '__init__.py' ! -name 'util*.py' ! -name 'common.py' ! -name 'colors.py' -wholename "*.py" | sort > "${todo}" \
    && execute_chunk_parallel execute_analytics_generator "${todo}" "${parallel}" "${runtime}" \
    && aws_s3_sync \
      "back/integrates/charts/generators" \
      "s3://${AWS_S3_MAIN_BUCKET}/analytics/${CI_COMMIT_REF_NAME}/documents" \
      --exclude "*" --include "*.json" --include "*.csv" \
    && popd \
    || return 1
}

main "${@}"
