{ inputs, makeScript, outputs, ... }:
makeScript {
  entrypoint = ./entrypoint.sh;
  name = "integrates-charts-documents";
  replace.__argIntegratesBackEnv__ = outputs."/integrates/back/env";
  searchPaths = {
    bin = [
      inputs.nixpkgs.findutils
      inputs.nixpkgs.gnused
      outputs."/integrates/db"
    ];
    source = [
      outputs."/common/utils/aws"
      outputs."/common/utils/common"
      outputs."/integrates/storage/dev/lib/populate"
    ];
  };
}
