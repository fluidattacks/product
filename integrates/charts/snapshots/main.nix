{ inputs, makePythonEnvironment, makeScript, outputs, ... }:
makeScript {
  entrypoint = ./entrypoint.sh;
  name = "integrates-charts-snapshots";
  replace = {
    __argGeckoDriver__ = inputs.nixpkgs.geckodriver;
    __argIntegratesBackEnv__ = outputs."/integrates/back/env";
    __argFirefox__ = inputs.nixpkgs.firefox;
  };
  searchPaths = {
    bin = [
      inputs.nixpkgs.noto-fonts
      inputs.nixpkgs.python311
      inputs.nixpkgs.roboto
      inputs.nixpkgs.roboto-mono
      outputs."/integrates/db"
    ];
    source = [
      outputs."/common/utils/aws"
      outputs."/common/utils/sops"
      outputs."/integrates/storage/dev/lib/populate"
    ];
  };
}
