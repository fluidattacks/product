# shellcheck shell=bash
function main {
  local env="${1}"
  if [ -n "${env}" ]; then
    info "Running for ${env}"
  else
    abort "Please provide a target environment"
  fi
  local sops_vars=(
    BITBUCKET_OAUTH2_REPOSITORY_APP_ID
    BITBUCKET_OAUTH2_REPOSITORY_APP_ID_DEV
    BITBUCKET_OAUTH2_REPOSITORY_SECRET
    BITBUCKET_OAUTH2_REPOSITORY_SECRET_DEV
  )
  : \
    && case $env in
      dev)
        aws_login "dev" "3600"
        sops_export_vars __argSecretsDev__ "${sops_vars[@]}"
        ;;
      prod)
        aws_login "prod" "3600"
        sops_export_vars __argSecretsProd__ "${sops_vars[@]}"
        ;;
      *) abort "First argument must be one of dev/prod" ;;
    esac \
    && client_auth_url="https://bitbucket.org/site/oauth2/authorize?client_id=${BITBUCKET_OAUTH2_REPOSITORY_APP_ID}&response_type=code" \
    && echo "Authenticate in: ${client_auth_url}" \
    && read -p "-> Type your access code: " code \
    && token_url="https://bitbucket.org/site/oauth2/access_token" \
    && token_data="grant_type=authorization_code&client_id=${BITBUCKET_OAUTH2_REPOSITORY_APP_ID}&client_secret=${BITBUCKET_OAUTH2_REPOSITORY_SECRET}&code=${code}" \
    && token_response=$(curl -X POST -d "${token_data}" "${token_url}") \
    && echo "${token_response}" \
    || return 1
}

main "${@}"
