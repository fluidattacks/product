{ inputs, makeScript, outputs, projectPath, ... }:
makeScript {
  replace = {
    __argSecretsDev__ = projectPath "/integrates/secrets/dev.yaml";
    __argSecretsProd__ = projectPath "/integrates/secrets/prod.yaml";
  };
  name = "integrates-test-bitbucket-secrets";
  searchPaths = {
    bin = [ inputs.nixpkgs.curl ];
    source = [ outputs."/common/utils/aws" outputs."/common/utils/sops" ];
  };
  entrypoint = ./entrypoint.sh;
}
