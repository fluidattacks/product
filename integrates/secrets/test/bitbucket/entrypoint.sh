# shellcheck shell=bash

function display_base_credentials_oauth_url {
  local env="${1}"
  local client_auth_url
  if [ "$env" == "prod" ]; then
    client_auth_url="https://bitbucket.org/site/oauth2/authorize?client_id=${BITBUCKET_OAUTH2_KEY}&response_type=code"
  else
    client_auth_url="https://bitbucket.org/site/oauth2/authorize?client_id=${BITBUCKET_OAUTH2_KEY_DEV}&response_type=code"
  fi
  echo "Authenticate in: ${client_auth_url} if you want to verify this auth flow"
}

function test_repository_credentials {
  local env="${1}"
  local client_secret
  local refresh_token

  if [ "$env" == "prod" ]; then
    client_id="$BITBUCKET_OAUTH2_REPOSITORY_APP_ID"
    client_secret="$BITBUCKET_OAUTH2_REPOSITORY_SECRET"
    refresh_token="$BITBUCKET_OAUTH2_REFRESH_TOKEN"
  else
    client_id="$BITBUCKET_OAUTH2_REPOSITORY_APP_ID_DEV"
    client_secret="$BITBUCKET_OAUTH2_REPOSITORY_SECRET_DEV"
    refresh_token="$BITBUCKET_OAUTH2_REFRESH_TOKEN_DEV"
  fi

  response=$(curl -sf -X POST -d "grant_type=refresh_token" \
    -d "client_id=$client_id" \
    -d "client_secret=$client_secret" \
    -d "refresh_token=$refresh_token" \
    "$token_url" > /dev/null 2>&1 && echo "success" || echo "failure")

  if [ "$response" == "success" ]; then
    info "Successfully tested repository credentials"
  else
    error "Failed testing repository credentials"
  fi
}

function main {
  local env="${1}"
  if [ -n "${env}" ]; then
    info "Running for ${env}"
  else
    abort "Please provide a target environment"
  fi
  local token_url="https://bitbucket.org/site/oauth2/access_token"
  local sops_vars_dev=(
    BITBUCKET_OAUTH2_KEY_DEV
    BITBUCKET_OAUTH2_SECRET_DEV
    BITBUCKET_OAUTH2_REPOSITORY_APP_ID_DEV
    BITBUCKET_OAUTH2_REPOSITORY_SECRET_DEV
    BITBUCKET_OAUTH2_REFRESH_TOKEN_DEV

  )
  local sops_vars_prod=(
    BITBUCKET_OAUTH2_KEY
    BITBUCKET_OAUTH2_SECRET
    BITBUCKET_OAUTH2_REPOSITORY_APP_ID
    BITBUCKET_OAUTH2_REPOSITORY_SECRET
    BITBUCKET_OAUTH2_REFRESH_TOKEN
  )
  : \
    && case $env in
      dev)
        aws_login "dev" "3600"
        sops_export_vars __argSecretsDev__ "${sops_vars_dev[@]}"
        ;;
      prod)
        aws_login "prod_integrates" "3600"
        sops_export_vars __argSecretsProd__ "${sops_vars_prod[@]}"
        ;;
      *) abort "First argument must be one of dev/prod" ;;
    esac \
    && display_base_credentials_oauth_url "$1" || return 1 \
    && test_repository_credentials "$1" || return 1
}

main "${@}"
