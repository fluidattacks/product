{ makeScript, outputs, ... }:
makeScript {
  name = "integrates-test-secrets";
  searchPaths = { bin = [ outputs."/integrates/secrets/test/bitbucket" ]; };
  entrypoint = ./entrypoint.sh;
}
