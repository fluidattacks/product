# shellcheck shell=bash

function main {
  : \
    && integrates-test-bitbucket-secrets "${@}" \
    || return 1
}

main "${@}"
