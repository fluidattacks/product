{ makeScript, outputs, ... }:
makeScript {
  name = "integrates-schemas";
  entrypoint = ./entrypoint.sh;
  searchPaths.bin = [
    outputs."/integrates/schemas/fmt"
    outputs."/integrates/schemas/export"
    outputs."/integrates/schemas/gen_types"
  ];
}
