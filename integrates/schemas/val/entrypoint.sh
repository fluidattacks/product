# shellcheck shell=bash

function validate_stage {
  local stage="${1}"
  if [[ -z ${stage} ]]; then
    error "Stage is not provided. Please provide one of [dev,prod]"
    return 1
  fi

  if [[ ! ${stage} =~ ^(dev|prod)$ ]]; then
    error "Invalid stage: ${stage}. Please provide one of [dev,prod]"
    return 1
  fi
}

function validate_table {
  local table="${1}"
  if [[ -z ${table} ]]; then
    error "Table is not provided. Please provide a table name"
    return 1
  fi
}

function get_login_role {
  local stage="${1}"
  if [[ ${stage} == "dev" ]]; then
    echo "dev"
  else
    echo "prod_integrates"
  fi
}

function main {
  local stage="${1:-}"
  local table="${2:-}"
  : \
    && validate_stage "${stage}" \
    && validate_table "${table}" \
    && aws_role=$(get_login_role "${stage}") \
    && source __argIntegratesBackEnv__/template "${stage}" \
    && info "Validating db schema" \
    && aws_login "${aws_role}" "3600" \
    && pushd integrates/back/integrates \
    && python3 schedulers/validate_db_schema.py --table "${table}" --schema-base-path __argCueSchemasBase__ \
    && info "Successfully validated db schema" \
    && popd \
    || return 1
}

main "${@}"
