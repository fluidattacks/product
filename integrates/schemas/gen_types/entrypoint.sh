# shellcheck shell=bash

function check_diff {
  local path_1=$1
  local path_2=$2
  if ! diff -q "$path_1" "$path_2" > /dev/null; then
    diff --color=auto "$path_1" "$path_2"
    error "$(realpath "${path_2}") was not up to date."
    return 1
  fi
  info "$(realpath "${path_2}") has not changed"
}

function main {
  local success=0
  : \
    && tables_base_path=$(realpath "integrates/schemas/tables") \
    && types_base_path=$(realpath "integrates/back/integrates/db_model") \
    && pushd integrates/schemas/gen_types/src \
    && go run . \
      "$tables_base_path" \
      "$types_base_path" \
    && if check_diff __argDbModel__/items.py "${types_base_path}/items.py"; then
      success=$((success + 1))
    fi \
    && if check_diff __argDbModel__/historic_items.py "${types_base_path}/historic_items.py"; then
      success=$((success + 1))
    fi \
    && if test "${success}" -eq 2; then
      info "Items files are up to date"
    else
      error "Items files are not up to date"
    fi \
    && popd \
    || return 1
}

main "$@"
