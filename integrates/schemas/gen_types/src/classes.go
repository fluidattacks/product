package main

import (
	"fmt"
	"slices"
	"strings"
)

func getSortedKeys(m map[string]ClassDetails) []string {
	keys := make([]string, 0, len(m))
	for k := range m {
		keys = append(keys, k)
	}
	slices.Sort(keys)
	return keys
}

func visitClass(className string, classes ClassMap, visited,
	resolved map[string]bool, result *[]string) error {
	if resolved[className] {
		return nil
	}

	if visited[className] {
		return fmt.Errorf("cyclic dependency detected involving class %s",
			className)
	}

	visited[className] = true

	for _, dep := range classes[className].Dependencies {
		if _, exists := classes[dep]; exists {
			err := visitClass(dep, classes, visited, resolved, result)
			if err != nil {
				return err
			}
		}
	}

	resolved[className] = true
	*result = append(*result, className)

	return nil
}

func resolveClassOrder(keys []string, classes ClassMap) ([]string, error) {
	visited := make(map[string]bool)
	resolved := make(map[string]bool)
	var result []string

	for _, className := range keys {
		if !resolved[className] {
			if err := visitClass(className, classes,
				visited, resolved, &result); err != nil {
				return nil, err
			}
		}
	}

	return result, nil
}

func correctOrder(classes ClassMap) ([]string, error) {
	sortedKeys := getSortedKeys(classes)
	return resolveClassOrder(sortedKeys, classes)
}

func appendToClassMapDeps(classes ClassMap, className string, deps []string) {
	if value, exists := classes[className]; exists {
		value.Dependencies = append(value.Dependencies, deps...)
		classes[className] = value
		return
	}
	classes[className] = ClassDetails{nil, deps}
}

func setClassMapContent(classes ClassMap, className string,
	content *strings.Builder) {
	if value, exists := classes[className]; exists {
		value.Content = content
		classes[className] = value
		return
	}
	classes[className] = ClassDetails{content, nil}
}

func updateClassMap(classes ClassMap, className string,
	content *strings.Builder, deps []string) {
	setClassMapContent(classes, className, content)
	appendToClassMapDeps(classes, className, deps)
}
