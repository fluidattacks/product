package main

import (
	"os"

	"cuelang.org/go/cue/cuecontext"
)

func main() {
	ctx := cuecontext.New()

	tablesBasePath := os.Args[1]
	itemsBasePath := os.Args[2]

	handleErr(GenItemsPy(ctx, tablesBasePath, itemsBasePath),
		"error generating db items classes",
		"Items generated successfully!",
	)
}
