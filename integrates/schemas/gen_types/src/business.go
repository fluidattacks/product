package main

import (
	"fmt"
	"os"
	"strings"

	"cuelang.org/go/cue"
	"cuelang.org/go/cue/load"
)

func isDir(path string) bool {
	fileInfo, err := os.Stat(path)
	if err != nil {
		return false
	}
	return fileInfo.IsDir()
}

func GenBusinessTypes(ctx *cue.Context, cueTypesRoot string,
	pyTypesRoot string) error {

	files, err := os.ReadDir(cueTypesRoot)
	if err != nil {
		return fmt.Errorf("error reading %v path: %v", cueTypesRoot, err)
	}

	for _, type_dir := range files {
		packageName := type_dir.Name()
		fmt.Printf("Processing: %v.\n", type_dir.Name())
		// First file from path
		var typePath string

		if isDir(fmt.Sprintf("%v/%v", cueTypesRoot, packageName)) {
			typePath = fmt.Sprintf("%v/%v/%v.cue", cueTypesRoot, packageName,
				packageName)
		} else {
			typePath = fmt.Sprintf("%v/%v", cueTypesRoot, packageName)
		}

		fmt.Printf("Full type path: %v\n", typePath)

		instances := load.Instances([]string{typePath}, nil)
		instance := ctx.BuildInstance(instances[0])
		fields, err := instance.Fields(cue.Definitions(true))

		if err != nil {
			return fmt.Errorf("error processing %v path: %v", type_dir.Name(), err)
		}

		for fields.Next() {
			sel := fields.Selector().String()
			val := fields.Value()
			if val.Kind() == cue.StructKind {
				structName := strings.Split(sel, "#")[1]
				fmt.Printf("Processing struct: %v.\n", structName)
			}
		}
	}

	return nil
}
