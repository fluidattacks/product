package main

import (
	"fluidattacks.com/tables/integrates_vms"
	"fluidattacks.com/tables/integrates_vms_historic"
	"fluidattacks.com/database_schema"
)

{
	ModelName: "database-design"
	ModelMetadata: {
		Author:           "development@fluidattacks.com"
		DateCreated:      "Feb 08, 2021, 10:37 AM"
		DateLastModified: "May 17, 2024, 8:00 AM"
		Description:      ""
		AWSService:       "Amazon DynamoDB"
		Version:          "3.0"
	}

	DataModel: [
		integrates_vms.#IntegratesVmsTable,
		integrates_vms_historic.#IntegratesVmsHistoricTable,
	]
} & database_schema.#DbSchema
