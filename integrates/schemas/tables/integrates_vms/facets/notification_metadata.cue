package notification_metadata

import (
	"fluidattacks.com/types/notifications"
)

#NotificationMetadataKeys: {
	pk: string & =~"^USER#[a-zA-Z0-9._%+-@]+$"
	sk: string & =~"^NOTIFICATION#[0-9TZ:+.-]+$"
}

#NotificationItem: {
	#NotificationMetadataKeys
	notifications.#NotificationMetadata
}

NotificationMetadata:
{
	FacetName: "notification_metadata"
	KeyAttributeAlias: {
		PartitionKeyAlias: "USER#email"
		SortKeyAlias:      "NOTIFICATION#id"
	}
	NonKeyAttributes: [
		"format",
		"id",
		"name",
		"notified_at",
		"status",
		"type",
		"user_email",
		"download_url",
		"expiration_time",
		"s3_file_path",
		"size",
	]
	DataAccess: {
		MySql: {}
	}
}

NotificationMetadata: TableData: [] & [...#NotificationItem]
