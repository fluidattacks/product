package git_root_metadata

import (
	"fluidattacks.com/types/roots"
)

#gitRootMetadataPk:   =~"^ROOT#[a-f0-9-]{36}$"
#gitRootMetadataSk:   =~"^GROUP#[a-z]+$"
#gitRootConstraintPk: =~"^ROOT#URL#"

#GitRootMetadataKeys: {
	pk:   string & #gitRootMetadataPk
	sk:   string & #gitRootMetadataSk
	pk_2: string
	sk_2: string
	pk_3: string
	sk_3: string
}

#GitRootItem: {
	#GitRootMetadataKeys
	roots.#GitRootMetadata
}

#GitRootConstraint: {
	pk: string & #gitRootConstraintPk
	sk: string & #gitRootMetadataSk
}

GitRootMetadata:
{
	FacetName: "git_root_metadata"
	KeyAttributeAlias: {
		PartitionKeyAlias: "ROOT#uuid"
		SortKeyAlias:      "GROUP#name"
	}
	NonKeyAttributes: [
		"type",
		"created_date",
		"pk_2",
		"sk_2",
		"pk_3",
		"sk_3",
		"state",
		"unreliable_indicators",
		"cloning",
		"created_by",
		"toe_lines",
		"rebase",
		"machine",
	]
	DataAccess: {
		MySql: {}
	}
}

GitRootMetadata: TableData: [
	{
		pk:           "ROOT#3a69ee71-1183-4cbe-99d9-27b2617df7d5"
		sk:           "GROUP#asgard"
		pk_2:         "ORG#makimachi"
		sk_2:         "ROOT#3a69ee71-1183-4cbe-99d9-27b2617df7d5"
		pk_3:         "GROUP#asgard"
		sk_3:         "ROOT#STATUS#active#URL#https://gitlab.com/fluidattacks/bwapp.git#BRANCH#master"
		created_date: "2020-11-19T13:37:10+00:00"
		state: {
			credential_id: "0df32c4a-526d-4879-8dc8-9ae191d2721b"
			criticality:   "HIGH"
			environment:   "production"
			gitignore: []
			includes_health_check: true
			modified_by:           "jdoe@fluidattacks.com"
			nickname:              "bwapp"
			environment_urls:
			[
				"https://hub.docker.com/r/raesene/bwapp",
				"https://test.com.co/bwapp",
			]

			modified_date: "2020-11-19T13:37:10+00:00"
			branch:        "master"
			url:           "https://gitlab.com/fluidattacks/bwapp.git"
			status:        "ACTIVE"
		}
		unreliable_indicators: {
			unreliable_last_status_update: "2020-11-19T13:37:10+00:00"
		}
		type:       "Git"
		created_by: "jdoe@fluidattacks.com"
		cloning: {
			commit:        "98dbb971caf519fb6c56c67df7e02b253fd5a97f"
			commit_date:   "2022-02-15T18:45:06.493253+00:00"
			reason:        "root OK"
			modified_by:   "integratesmanager@fluidattacks.com"
			modified_date: "2020-11-19T13:39:10+00:00"
			status:        "OK"
		}
	},
	{
		pk: "ROOT#URL#https://gitlab.com/fluidattacks/bwapp.git#master"
		sk: "GROUP#asgard"
	},
	{
		pk:           "ROOT#7d4765c7-9236-4fc8-9724-000f5339422f"
		sk:           "GROUP#asgard"
		pk_2:         "ORG#makimachi"
		sk_2:         "ROOT#7d4765c7-9236-4fc8-9724-000f5339422f"
		pk_3:         "GROUP#asgard"
		sk_3:         "ROOT#STATUS#active#URL#https://github.com/WebGoat/WebGoat.git#BRANCH#master"
		created_date: "2020-11-19T13:37:10+00:00"
		state: {
			credential_id: "0df32c4a-526d-4879-8dc8-9ae191d2721b"
			criticality:   "MEDIUM"
			environment:   "production"
			gitignore: []
			includes_health_check: true
			modified_by:           "jdoe@fluidattacks.com"
			nickname:              "WebGoat"
			environment_urls:
			[
				"https://test.com",
			]

			modified_date: "2020-11-19T13:37:10+00:00"
			branch:        "master"
			url:           "https://github.com/WebGoat/WebGoat.git"
			status:        "ACTIVE"
		}
		unreliable_indicators: {
			unreliable_last_status_update: "2020-11-19T13:37:10+00:00"
		}
		type:       "Git"
		created_by: "jdoe@fluidattacks.com"
		cloning: {
			commit:        "b67eb441420675e0f107e2c1a3ba04900fc110fb"
			commit_date:   "2022-02-15T18:45:06.493253+00:00"
			reason:        "root OK"
			modified_by:   "integratesmanager@fluidattacks.com"
			modified_date: "2020-11-19T13:39:10+00:00"
			status:        "OK"
		}
	},
	{
		pk: "ROOT#URL#https://github.com/WebGoat/WebGoat.git#master"
		sk: "GROUP#asgard"
	},
	{
		pk:           "ROOT#4039d098-ffc5-4984-8ed3-eb17bca98e19"
		sk:           "GROUP#unittesting"
		pk_2:         "ORG#okada"
		sk_2:         "ROOT#4039d098-ffc5-4984-8ed3-eb17bca98e19"
		pk_3:         "GROUP#unittesting"
		sk_3:         "ROOT#STATUS#active#URL#https://gitlab.com/fluidattacks/universe.git#BRANCH#master"
		created_date: "2020-11-19T13:37:10+00:00"
		state: {
			credential_id: "0df32c4a-526d-4879-8dc8-9ae191d2721b"
			criticality:   "MEDIUM"
			environment:   "production"
			gitignore:
			[
				"bower_components/*",
				"node_modules/*",
			]

			includes_health_check: true
			modified_by:           "jdoe@fluidattacks.com"
			nickname:              "universe"
			environment_urls:
			[
				"https://app.fluidattacks.com",
				"https://test.com",
			]

			modified_date: "2020-11-19T13:37:10+00:00"
			branch:        "master"
			url:           "https://gitlab.com/fluidattacks/universe.git"
			status:        "ACTIVE"
		}
		unreliable_indicators: {
			unreliable_last_status_update: "2020-11-19T13:37:10+00:00"
		}
		type:       "Git"
		created_by: "jdoe@fluidattacks.com"
		cloning: {
			commit:                   "5b5c92105b5c92105b5c92105b5c92105b5c9210"
			commit_date:              "2020-02-15T18:45:06.493253+00:00"
			failed_count:             0
			first_failed_cloning:     "2020-11-10T00:00:00+00:00"
			first_successful_cloning: "2020-11-11T00:00:00+00:00"
			last_successful_cloning:  "2020-11-19T00:00:00+00:00"
			modified_by:              "schedule_clone_groups_roots@fluidattacks.com"
			modified_date:            "2020-11-19T00:00:00+00:00"
			reason:                   "Cloned successfully"
			status:                   "OK"
			successful_count:         23
		}
		machine: {
			commit:        "5b5c92105b5c92105b5c92105b5c92105b5c9210"
			commit_date:   "2020-02-15T18:45:06.493253+00:00"
			execution_id:  "unittesting_7a1a794de41d4e0aa037385f80277ee3_universe_0c0d4656"
			modified_by:   "machine@fluidattacks.com"
			modified_date: "2020-11-20T00:00:00+00:00"
			reason:        "Execution has been processed"
			status:        "SUCCESS"
			last_executions_date: {
				apk:  "2020-11-20T00:00:00+00:00"
				cspm: "2020-11-19T00:00:00+00:00"
				dast: "2020-11-18T00:00:00+00:00"
				sast: "2020-11-17T00:00:00+00:00"
				sca:  "2020-11-16T00:00:00+00:00"
			}
		}
		rebase: {
			commit:                 "5b5c92105b5c92105b5c92105b5c92105b5c9210"
			commit_date:            "2020-02-15T18:45:06.493253+00:00"
			last_successful_rebase: "2020-11-21T00:00:00+00:00"
			modified_by:            "schedule_clone_groups_roots@fluidattacks.com"
			modified_date:          "2020-11-21T00:00:00+00:00"
			reason:                 "Git root has been rebased"
			status:                 "SUCCESS"
		}
		toe_lines: {
			commit:                  "5b5c92105b5c92105b5c92105b5c92105b5c9210"
			commit_date:             "2020-02-15T18:45:06.493253+00:00"
			last_successful_refresh: "2020-11-20T12:00:00+00:00"
			modified_by:             "schedule_clone_groups_roots@fluidattacks.com"
			modified_date:           "2020-11-20T12:00:00+00:00"
			reason:                  "Finished refreshing toe lines on ACTIVE root"
			status:                  "SUCCESS"
		}
	},
	{
		pk: "ROOT#URL#https://gitlab.com/fluidattacks/universe.git#BRANCH#master"
		sk: "GROUP#unittesting"
	},
	{
		pk:           "ROOT#765b1d0f-b6fb-4485-b4e2-2c2cb1555b1a"
		sk:           "GROUP#unittesting"
		pk_2:         "ORG#okada"
		sk_2:         "ROOT#765b1d0f-b6fb-4485-b4e2-2c2cb1555b1a"
		pk_3:         "GROUP#unittesting"
		sk_3:         "ROOT#STATUS#active#URL#https://gitlab.com/fluidattacks/demo.git#BRANCH#main"
		created_date: "2020-11-19T13:39:56+00:00"
		state: {
			environment: "QA"
			gitignore: []
			includes_health_check: false
			modified_by:           "jdoe@fluidattacks.com"
			nickname:              "integrates_1"
			environment_urls: []
			modified_date: "2020-11-19T13:39:56+00:00"
			branch:        "main"
			url:           "https://gitlab.com/fluidattacks/demo.git"
			status:        "ACTIVE"
		}
		unreliable_indicators: {
			unreliable_last_status_update: "2020-11-19T13:39:56+00:00"
		}
		type:       "Git"
		created_by: "jdoe@fluidattacks.com"
		cloning: {
			commit:               "cdd48a681aa96082b3095dc06fb1b15ec4b5ea7b"
			commit_date:          "2020-02-15T18:45:06.493253+00:00"
			failed_count:         99
			first_failed_cloning: "2020-01-01T00:00:00+00:00"
			modified_by:          "schedule_clone_groups_roots@fluidattacks.com"
			modified_date:        "2020-11-19T13:37:10+00:00"
			reason:               "Failed to clone without message"
			status:               "FAILED"
			successful_count:     0
		}
		machine: {
			commit:        "cdd48a681aa96082b3095dc06fb1b15ec4b5ea7b"
			commit_date:   "2020-02-15T18:45:06.493253+00:00"
			modified_by:   "machine@fluidattacks.com"
			modified_date: "2020-11-21T00:00:00+00:00"
			reason:        "Error cloning the repository"
			status:        "FAILED"
		}
		rebase: {
			commit:                 "cdd48a681aa96082b3095dc06fb1b15ec4b5ea7b"
			commit_date:            "2020-02-15T18:45:06.493253+00:00"
			last_successful_rebase: "2020-11-20T00:00:00+00:00"
			modified_by:            "schedule_clone_groups_roots@fluidattacks.com"
			modified_date:          "2020-11-21T00:00:00+00:00"
			reason:                 "Error for git blame command"
			status:                 "FAILED"
		}
		toe_lines: {
			commit:                  "cdd48a681aa96082b3095dc06fb1b15ec4b5ea7b"
			commit_date:             "2020-02-15T18:45:06.493253+00:00"
			last_successful_refresh: "2020-11-19T12:00:00+00:00"
			modified_by:             "schedule_clone_groups_roots@fluidattacks.com"
			modified_date:           "2020-11-20T12:00:00+00:00"
			reason:                  "Unexpected error during ToE Lines refresh"
			status:                  "FAILED"
		}
	},
	{
		pk: "ROOT#URL#https://gitlab.com/fluidattacks/demo.git#BRANCH#main"
		sk: "GROUP#unittesting"
	},
	{
		pk:           "ROOT#9eccb9ed-e835-4dbc-b28e-8cecd0296e27"
		sk:           "GROUP#unittesting"
		pk_2:         "ORG#okada"
		sk_2:         "ROOT#9eccb9ed-e835-4dbc-b28e-8cecd0296e27"
		pk_3:         "GROUP#unittesting"
		sk_3:         "ROOT#STATUS#active#URL#https://gitlab.com/fluidattacks/machine#BRANCH#develop"
		created_date: "2020-11-19T13:39:56+00:00"
		state: {
			environment: "QA"
			gitignore: []
			includes_health_check: false
			modified_by:           "integratesmanager@gmail.com"
			nickname:              "integrates_test_root"
			environment_urls: []
			modified_date: "2024-05-09T13:39:56+00:00"
			branch:        "develop"
			url:           "https://gitlab.com/fluidattacks/machine"
			status:        "ACTIVE"
		}
		unreliable_indicators: {
			unreliable_last_status_update: "2024-05-09T13:39:56+00:00"
		}
		type:       "Git"
		created_by: "integratesmanager@gmail.com"
		cloning: {
			commit:        "f7595918f9ba96ac2d765750445e110baf955503"
			commit_date:   "2024-05-09T18:45:06.493253+00:00"
			reason:        "changes"
			modified_by:   "integratesmanager@fluidattacks.com"
			modified_date: "2024-05-09T13:37:10+00:00"
			status:        "UNKNOWN"
		}
		machine: {
			commit:        "aa3aab1a9d07be5b099b1da44032d7b10fb8d3f7"
			commit_date:   "2020-02-15T18:45:06.493253+00:00"
			modified_by:   "machine@fluidattacks.com"
			modified_date: "2020-11-21T00:00:00+00:00"
			reason:        "Error cloning the repository"
			status:        "FAILED"
			last_executions_date: {
				apk:  "2020-11-20T00:00:00+00:00"
				cspm: "2020-11-19T00:00:00+00:00"
				dast: "2020-11-18T00:00:00+00:00"
				sast: "2020-11-17T00:00:00+00:00"
				sca:  "2020-11-16T00:00:00+00:00"
			}
		}
	},
	{
		pk: "ROOT#URL#https://gitlab.com/fluidattacks/machine#BRANCH#develop"
		sk: "GROUP#unittesting"
	},
	{
		pk:           "ROOT#f8771794-0428-4018-90c3-ce20525cee02"
		sk:           "GROUP#unittesting"
		pk_2:         "ORG#okada"
		sk_2:         "ROOT#f8771794-0428-4018-90c3-ce20525cee02"
		pk_3:         "GROUP#unittesting"
		sk_3:         "ROOT#STATUS#active#URL#https://github.com/fluidattacks/integrates#BRANCH#main"
		created_date: "2020-11-19T13:39:56+00:00"
		state: {
			credential_id: "0df32c4a-526d-4879-8dc8-9ae191d2721b"
			criticality:   "LOW"
			environment:   "QA"
			gitignore: []
			includes_health_check: false
			modified_by:           "integratesreattacker@fluidattacks.com"
			nickname:              "integrates_2"
			environment_urls: []
			modified_date: "2020-11-19T13:39:56+00:00"
			branch:        "main"
			url:           "https://github.com/fluidattacks/integrates"
			status:        "ACTIVE"
		}
		unreliable_indicators: {
			unreliable_last_status_update: "2020-11-19T13:39:56+00:00"
		}
		type:       "Git"
		created_by: "integratesreattacker@fluidattacks.com"
		cloning: {
			commit:        "a1b2c3d4e5f6a1b2c3d4e5f6a1b2c3d4e5f6a1b2"
			commit_date:   "2022-02-15T18:45:06.493253+00:00"
			reason:        "root creation 2"
			modified_by:   "jdoe@fluidattacks.com"
			modified_date: "2020-11-19T13:37:10+00:00"
			status:        "UNKNOWN"
		}
	},
	{
		pk: "ROOT#URL#https://github.com/fluidattacks/integrates#BRANCH#main"
		sk: "GROUP#unittesting"
	},
	{
		pk:           "ROOT#f48afafd-a8f2-4925-84b1-6a6dc031b775"
		sk:           "GROUP#monteria"
		pk_2:         "ORG#kamiya"
		sk_2:         "ROOT#f48afafd-a8f2-4925-84b1-6a6dc031b775"
		pk_3:         "GROUP#monteria"
		sk_3:         "ROOT#STATUS#inactive#URL#https://github.com/fluidattacks/universe2.git#BRANCH#main"
		created_date: "2020-11-19T13:39:56+00:00"
		state: {
			credential_id: "c624dc13-ac87-4cff-b710-767d3c3fdfd4"
			criticality:   "MEDIUM"
			environment:   "QA"
			gitignore: []
			includes_health_check: false
			modified_by:           "integratesreattacker@fluidattacks.com"
			nickname:              "monteria_1"
			environment_urls: []
			modified_date: "2020-11-19T13:39:56+00:00"
			branch:        "main"
			url:           "https://github.com/fluidattacks/universe2.git"
			status:        "INACTIVE"
		}
		unreliable_indicators: {
			unreliable_last_status_update: "2020-11-19T13:39:56+00:00"
		}
		type:       "Git"
		created_by: "integratesreattacker@fluidattacks.com"
		cloning: {
			commit:        "a1b2c3d4e5f6a1b2c3d4e5f6a1b2c3d4e5f6a1b2"
			commit_date:   "2022-02-15T18:45:06.493253+00:00"
			reason:        "root creation 2"
			modified_by:   "jdoe@fluidattacks.com"
			modified_date: "2020-11-19T13:37:10+00:00"
			status:        "UNKNOWN"
		}
	},
	{
		pk: "ROOT#URL#https://github.com/fluidattacks/universe2.git#BRANCH#main"
		sk: "GROUP#monteria"
	},
	{
		pk:           "ROOT#2e349e7f-390e-40e9-a8b6-2983eaee92ff"
		sk:           "GROUP#barranquilla"
		pk_2:         "ORG#kamiya"
		sk_2:         "ROOT#2e349e7f-390e-40e9-a8b6-2983eaee92ff"
		pk_3:         "GROUP#barranquilla"
		sk_3:         "ROOT#STATUS#active#URL#https://gitlab.com/fluidattacks/integrates#BRANCH#develop"
		created_date: "2020-11-19T13:39:56+00:00"
		state: {
			credential_id: "c624dc13-ac87-4cff-b710-767d3c3fdfd4"
			criticality:   "HIGH"
			environment:   "QA"
			gitignore: []
			includes_health_check: false
			modified_by:           "integratesmanager@fluidattacks.com"
			nickname:              "barranquilla_1"
			environment_urls: []
			modified_date: "2020-11-19T13:39:56+00:00"
			branch:        "develop"
			url:           "https://gitlab.com/fluidattacks/integrates"
			status:        "ACTIVE"
		}
		unreliable_indicators: {
			unreliable_last_status_update: "2020-11-19T13:39:56+00:00"
		}
		type:       "Git"
		created_by: "jdoe@fluidattacks.com"
		cloning: {
			commit:        "cdd48a681aa96082b3095dc06fb1b15ec4b5ea7b"
			commit_date:   "2022-02-15T18:45:06.493253+00:00"
			reason:        "root creation"
			modified_by:   "jdoe@fluidattacks.com"
			modified_date: "2020-11-19T13:37:10+00:00"
			status:        "UNKNOWN"
		}
	},
	{
		pk: "ROOT#URL#https://gitlab.com/fluidattacks/integrates#BRANCH#develop"
		sk: "GROUP#barranquilla"
	},
	{
		pk:           "ROOT#f48afafd-a8f2-4925-84b1-6a6dc031b779"
		sk:           "GROUP#unittesting"
		pk_2:         "ORG#okada"
		sk_2:         "ROOT#f48afafd-a8f2-4925-84b1-6a6dc031b775"
		pk_3:         "GROUP#unittesting"
		sk_3:         "ROOT#STATUS#inactive#URL#https://gitlab.com/fluidattacks/universe2.git#BRANCH#master"
		created_date: "2020-11-26T13:39:56+00:00"
		state: {
			credential_id: "c624dc13-ac87-4cff-b710-767d3c3fdfd4"
			criticality:   "MEDIUM"
			environment:   "QA"
			gitignore: []
			includes_health_check: true
			modified_by:           "integratesreattacker@fluidattacks.com"
			nickname:              "universe_3"
			environment_urls: []
			modified_date: "2020-11-19T13:39:56+00:00"
			branch:        "master"
			url:           "https://gitlab.com/fluidattacks/universe2.git"
			status:        "INACTIVE"
		}
		unreliable_indicators: {
			unreliable_last_status_update: "2020-11-19T13:39:56+00:00"
		}
		type:       "Git"
		created_by: "integratesreattacker@fluidattacks.com"
		cloning: {
			commit:        "a1b2c3d4e5f6a1b2c3d4e5f6a1b2c3d4e5f6a1b3"
			commit_date:   "2022-02-15T18:45:06.493253+00:00"
			reason:        "root creation 2"
			modified_by:   "jdoe@fluidattacks.com"
			modified_date: "2020-11-19T13:37:10+00:00"
			status:        "UNKNOWN"
		}
	},
	{
		pk: "ROOT#URL#https://gitlab.com/fluidattacks/universe2.git#BRANCH#master"
		sk: "GROUP#unittesting"
	},
	{
		pk:           "ROOT#68416808-a69b-4585-ae24-ac2ff8b8b007"
		sk:           "GROUP#groudon"
		pk_2:         "ORG#kanto"
		sk_2:         "ROOT#68416808-a69b-4585-ae24-ac2ff8b8b007"
		pk_3:         "GROUP#groudon"
		sk_3:         "ROOT#STATUS#active#URL#https://gitlab.com/fluidattacks/integrates#BRANCH#trunk"
		created_date: "2024-09-19T11:00:00+00:00"
		state: {
			environment: "production"
			gitignore: []
			includes_health_check: false
			modified_by:           "__adminEmail__"
			nickname:              "groudon_gitlab"
			environment_urls: []
			modified_date: "2024-09-19T11:00:00+00:00"
			branch:        "trunk"
			url:           "https://gitlab.com/fluidattacks/integrates"
			status:        "ACTIVE"
		}
		unreliable_indicators: {
			unreliable_last_status_update: "2024-09-19T11:00:00+00:00"
		}
		type:       "Git"
		created_by: "__adminEmail__"
		cloning: {
			commit:           "293abe78145b8f097f1709635527faf9b4ea69b2"
			commit_date:      "2024-09-19T11:00:00+00:00"
			failed_count:     0
			modified_by:      "schedule_clone_groups_roots@fluidattacks.com"
			modified_date:    "2024-09-19T11:00:00+00:00"
			reason:           "Cloned successfully"
			status:           "OK"
			successful_count: 1
		}
	},
	{
		pk: "ROOT#URL#https://gitlab.com/fluidattacks/integrates#BRANCH#trunk"
		sk: "GROUP#groudon"
	},
	{
		pk:           "ROOT#1507276e-de9e-424f-bb76-ec0d2dcd6d13"
		sk:           "GROUP#kyogre"
		pk_2:         "ORG#kanto"
		sk_2:         "ROOT#"
		pk_3:         "GROUP#kyogre"
		sk_3:         "ROOT#STATUS#active#URL#https://github.com/fluidattacks/makes#BRANCH#main"
		created_date: "2024-09-19T11:00:00+00:00"
		state: {
			environment: "production"
			gitignore: []
			includes_health_check: false
			modified_by:           "__adminEmail__"
			nickname:              "kyogre_github"
			environment_urls: []
			modified_date: "2024-09-19T11:00:00+00:00"
			branch:        "main"
			url:           "https://github.com/fluidattacks/makes"
			status:        "ACTIVE"
		}
		unreliable_indicators: {
			unreliable_last_status_update: "2024-09-19T11:00:00+00:00"
		}
		type:       "Git"
		created_by: "__adminEmail__"
		cloning: {
			commit:           "293abe78145b8f097f1709635527faf9b4ea69b2"
			commit_date:      "2024-09-19T11:00:00+00:00"
			failed_count:     0
			modified_by:      "schedule_clone_groups_roots@fluidattacks.com"
			modified_date:    "2024-09-19T11:00:00+00:00"
			reason:           "Cloned successfully"
			status:           "OK"
			successful_count: 1
		}
	},
	{
		pk: "ROOT#URL#https://github.com/fluidattacks/makes#BRANCH#main"
		sk: "GROUP#kyogre"
	},
] & [...(#GitRootItem | #GitRootConstraint)]
