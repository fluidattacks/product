package git_root_historic_state

import (
	"fluidattacks.com/types/roots"
)

#gitRootHistoricStatePk: =~"^ROOT#[a-f0-9-]{36}$"
#gitRootHistoricStateSk: =~"^STATE#(\\d{4})-(\\d{2})-(\\d{2})(T(\\d{2}):(\\d{2}):(\\d{2})(\\.\\d+)?(Z|([+-])(\\d{2}):(\\d{2})))?$"

#GitRootHistoricStateKeys: {
	pk: string & #gitRootHistoricStatePk
	sk: string & #gitRootHistoricStateSk
}

#GitRootHistoricStateItem: {
	#GitRootHistoricStateKeys
	roots.#GitRootState
}

GitRootHistoricState:
{
	FacetName: "git_root_historic_state"
	KeyAttributeAlias: {
		PartitionKeyAlias: "ROOT#uuid"
		SortKeyAlias:      "STATE#iso8601utc"
	}
	NonKeyAttributes: [
		"status",
		"reason",
		"modified_date",
		"branch",
		"url",
		"environment",
		"environment_urls",
		"gitignore",
		"includes_health_check",
		"modified_by",
		"nickname",
		"other",
		"credential_id",
		"criticality",
		"use_egress",
		"use_vpn",
		"use_ztna",
	]
	DataAccess: {
		MySql: {}
	}
}

GitRootHistoricState: TableData: [
	{
		pk:            "ROOT#4039d098-ffc5-4984-8ed3-eb17bca98e19"
		sk:            "STATE#2020-11-19T13:37:10+00:00"
		status:        "ACTIVE"
		modified_date: "2020-11-19T13:37:10+00:00"
		branch:        "master"
		url:           "https://gitlab.com/fluidattacks/universe.git"
		environment:   "production"
		environment_urls:
		[
			"https://app.fluidattacks.com",
			"https://test.com",
		]

		gitignore:
		[
			"bower_components/*",
			"node_modules/*",
		]

		includes_health_check: true
		modified_by:           "jdoe@fluidattacks.com"
		nickname:              "universe"
	},
	{
		pk:            "ROOT#765b1d0f-b6fb-4485-b4e2-2c2cb1555b1a"
		sk:            "STATE#2020-11-19T13:39:56+00:00"
		status:        "ACTIVE"
		modified_date: "2020-11-19T13:39:56+00:00"
		branch:        "main"
		url:           "https://gitlab.com/fluidattacks/demo.git"
		environment:   "QA"
		environment_urls: []
		gitignore:
		[
			"bower_components/*",
			"node_modules/*",
		]

		includes_health_check: false
		modified_by:           "jdoe@fluidattacks.com"
		nickname:              "integrates_1"
	},
	{
		pk:            "ROOT#2e349e7f-390e-40e9-a8b6-2983eaee92ff"
		sk:            "STATE#2020-11-19T13:41:56+00:00"
		status:        "ACTIVE"
		modified_date: "2020-11-19T13:41:56+00:00"
		branch:        "develop"
		url:           "https://gitlab.com/fluidattacks/integrates"
		environment:   "QA"
		environment_urls: []
		gitignore:
		[
			"bower_components/*",
			"node_modules/*",
		]

		includes_health_check: false
		modified_by:           "jdoe@fluidattacks.com"
		nickname:              "barranquilla_1"
	},
] & [...#GitRootHistoricStateItem]
