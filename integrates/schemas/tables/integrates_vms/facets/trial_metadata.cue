package trial_metadata

import (
	"fluidattacks.com/types/trials"
)

#trialMetadataPk: =~"^TRIAL#[a-zA-Z0-9._%+-@]+$"
#trialMetadataSk: =~"^TRIAL#all$"

#TrialMetadataKeys: {
	pk: string & #trialMetadataPk
	sk: string & #trialMetadataSk
}

#TrialItem: {
	#TrialMetadataKeys
	trials.#TrialMetadata
}

TrialMetadata:
{
	FacetName: "trial_metadata"
	KeyAttributeAlias: {
		PartitionKeyAlias: "TRIAL#email"
		SortKeyAlias:      "TRIAL#all"
	}
	NonKeyAttributes: [
		"email",
		"completed",
		"extension_date",
		"extension_days",
		"start_date",
		"reason",
	]
	DataAccess: {
		MySql: {}
	}
}

TrialMetadata: TableData: [
	{
		pk:             "TRIAL#continuoushack2@gmail.com"
		sk:             "TRIAL#all"
		email:          "continuoushack2@gmail.com"
		completed:      true
		extension_days: 0
		start_date:     "2022-08-04T22:21:45.961408+00:00"
	},
	{
		pk:             "TRIAL#continuoushacking@gmail.com"
		sk:             "TRIAL#all"
		email:          "continuoushacking@gmail.com"
		completed:      true
		extension_days: 0
		start_date:     "2022-08-04T22:21:45.969646+00:00"
	},
	{
		pk:             "TRIAL#customer_manager@fluidattacks.com"
		sk:             "TRIAL#all"
		email:          "customer_manager@fluidattacks.com"
		completed:      true
		extension_days: 0
		start_date:     "2022-08-04T22:21:45.942336+00:00"
	},
	{
		pk:             "TRIAL#integrateshacker@fluidattacks.com"
		sk:             "TRIAL#all"
		email:          "integrateshacker@fluidattacks.com"
		completed:      true
		extension_days: 0
		start_date:     "2022-08-04T22:21:45.965690+00:00"
	},
	{
		pk:             "TRIAL#integratesmanager@gmail.com"
		sk:             "TRIAL#all"
		email:          "integratesmanager@gmail.com"
		completed:      true
		extension_date: "2022-06-22T20:07:57+00:00"
		extension_days: 5
		start_date:     "2022-06-01T20:07:57+00:00"
	},
	{
		pk:             "TRIAL#integratesreattacker@fluidattacks.com"
		sk:             "TRIAL#all"
		email:          "integratesreattacker@fluidattacks.com"
		completed:      true
		extension_days: 0
		start_date:     "2022-08-04T22:21:45.955784+00:00"
	},
	{
		pk:             "TRIAL#integratesresourcer@fluidattacks.com"
		sk:             "TRIAL#all"
		email:          "integratesresourcer@fluidattacks.com"
		completed:      true
		extension_days: 0
		start_date:     "2022-08-04T22:21:45.936189+00:00"
	},
	{
		pk:             "TRIAL#integratesreviewer@fluidattacks.com"
		sk:             "TRIAL#all"
		email:          "integratesreviewer@fluidattacks.com"
		completed:      true
		extension_days: 0
		start_date:     "2022-08-04T22:21:45.948504+00:00"
	},
	{
		pk:             "TRIAL#integratesserviceforces@fluidattacks.com"
		sk:             "TRIAL#all"
		email:          "integratesserviceforces@fluidattacks.com"
		completed:      true
		extension_days: 0
		start_date:     "2022-08-04T22:21:45.958726+00:00"
	},
	{
		pk:             "TRIAL#integratesuser2@fluidattacks.com"
		sk:             "TRIAL#all"
		email:          "integratesuser2@fluidattacks.com"
		completed:      true
		extension_days: 0
		start_date:     "2022-08-04T22:21:45.940319+00:00"
	},
	{
		pk:             "TRIAL#integratesuser2@gmail.com"
		sk:             "TRIAL#all"
		email:          "integratesuser2@gmail.com"
		completed:      true
		extension_days: 0
		start_date:     "2022-08-04T22:21:45.938298+00:00"
	},
	{
		pk:             "TRIAL#unittest2@fluidattacks.com"
		sk:             "TRIAL#all"
		email:          "unittest2@fluidattacks.com"
		completed:      true
		extension_days: 0
		start_date:     "2022-08-04T22:21:45.951177+00:00"
	},
	{
		pk:             "TRIAL#unittest@fluidattacks.com"
		sk:             "TRIAL#all"
		email:          "unittest@fluidattacks.com"
		completed:      true
		extension_date: "2022-06-22T20:07:57+00:00"
		extension_days: 5
		start_date:     "2022-06-01T20:07:57+00:00"
	},
	{
		pk:             "TRIAL#vulnmanager@gmail.com"
		sk:             "TRIAL#all"
		email:          "vulnmanager@gmail.com"
		completed:      true
		extension_days: 0
		start_date:     "2022-08-04T22:21:45.946286+00:00"
	},
] & [...#TrialItem]
