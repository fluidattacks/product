package event_metadata

import (
	"fluidattacks.com/types/events"
)

#eventMetadataPk: =~"^EVENT#[0-9]+$"
#eventMetadataSk: =~"^GROUP#[a-z]+$"

#EventMetadataKeys: {
	pk:   string & #eventMetadataPk
	sk:   string & #eventMetadataSk
	pk_2: string
	sk_2: string
}

#EventItem: {
	events.#EventMetadata
	#EventMetadataKeys
}

EventMetadata:
{
	FacetName: "event_metadata"
	KeyAttributeAlias: {
		PartitionKeyAlias: "EVENT#id"
		SortKeyAlias:      "GROUP#name"
	}
	NonKeyAttributes: [
		"description",
		"type",
		"evidences",
		"created_date",
		"id",
		"group_name",
		"root_id",
		"pk_2",
		"sk_2",
		"state",
		"client",
		"hacker",
		"event_date",
		"created_by",
		"environment_url",
		"solving_date",
	]
	DataAccess: {
		MySql: {}
	}
}

EventMetadata: TableData: [
	{
		group_name:   "continuoustesting"
		description:  "Continuoustest event test"
		solving_date: "2018-01-03T20:24:25+00:00"
		type:         "OTHER"
		created_by:   "unittest@fluidattacks.com"
		hacker:       "unittest@fluidattacks.com"
		event_date:   "2018-01-01T20:24:25+00:00"
		sk:           "GROUP#continuoustesting"
		client:       "Fluid"
		pk_2:         "GROUP#continuoustesting"
		created_date: "2018-01-02T20:24:25+00:00"
		id:           "418900923"
		pk:           "EVENT#418900923"
		state: {
			modified_by:   "unittest@fluidattacks.com"
			modified_date: "2018-01-03T20:24:25+00:00"
			status:        "SOLVED"
		}
		evidences: {}
		sk_2: "EVENT#SOLVED#true"
	},
	{
		group_name:   "oneshottest"
		description:  "Oneshot event test"
		type:         "OTHER"
		created_by:   "unittest@fluidattacks.com"
		hacker:       "unittest@fluidattacks.com"
		event_date:   "2020-01-02T12:00:00+00:00"
		sk:           "GROUP#oneshottest"
		client:       "Fluid"
		pk_2:         "GROUP#oneshottest"
		created_date: "2020-01-02T19:40:05+00:00"
		id:           "418900978"
		pk:           "EVENT#418900978"
		state: {
			modified_by:   "unittest@fluidattacks.com"
			modified_date: "2020-01-02T19:40:05+00:00"
			status:        "CREATED"
		}
		evidences: {}
		sk_2: "EVENT#SOLVED#false"
	},
	{
		group_name:   "oneshottest"
		description:  "Oneshot event test 2"
		solving_date: "2020-01-03T20:24:25+00:00"
		type:         "OTHER"
		created_by:   "unittest@fluidattacks.com"
		hacker:       "unittest@fluidattacks.com"
		event_date:   "2020-01-01T20:24:25+00:00"
		sk:           "GROUP#oneshottest"
		client:       "Fluid"
		pk_2:         "GROUP#oneshottest"
		created_date: "2020-01-02T20:24:25+00:00"
		id:           "418900979"
		pk:           "EVENT#418900979"
		state: {
			modified_by:   "unittest@fluidattacks.com"
			modified_date: "2020-01-03T20:24:25+00:00"
			status:        "SOLVED"
		}
		evidences: {}
		sk_2: "EVENT#SOLVED#true"
	},
	{
		group_name:   "unittesting"
		description:  "Integrates unit test"
		type:         "OTHER"
		created_by:   "unittest@fluidattacks.com"
		hacker:       "unittest@fluidattacks.com"
		event_date:   "2018-06-27T12:00:00+00:00"
		sk:           "GROUP#unittesting"
		client:       "Fluid"
		pk_2:         "GROUP#unittesting"
		created_date: "2018-06-27T19:40:05+00:00"
		id:           "418900971"
		pk:           "EVENT#418900971"
		state: {
			modified_by:   "unittest@fluidattacks.com"
			modified_date: "2018-06-27T19:40:05+00:00"
			status:        "CREATED"
		}
		evidences: {}
		sk_2: "EVENT#SOLVED#false"
	},
	{
		group_name:   "unittesting"
		description:  "Test con evidencia."
		solving_date: "2018-12-26T18:37:00+00:00"
		type:         "AUTHORIZATION_SPECIAL_ATTACK"
		created_by:   "unittest@fluidattacks.com"
		hacker:       "unittest@fluidattacks.com"
		event_date:   "2018-12-17T21:20:00+00:00"
		sk:           "GROUP#unittesting"
		client:       "Fluid"
		pk_2:         "GROUP#unittesting"
		created_date: "2018-12-17T21:21:03+00:00"
		id:           "463578352"
		pk:           "EVENT#463578352"
		state: {
			modified_by:   "unittest@fluidattacks.com"
			modified_date: "2018-12-26T18:37:00+00:00"
			status:        "SOLVED"
		}
		evidences: {}
		sk_2: "EVENT#SOLVED#true"
	},
	{
		group_name:   "unittesting"
		description:  "This is an eventuality with evidence"
		solving_date: "2020-04-11T18:37:00+00:00"
		type:         "AUTHORIZATION_SPECIAL_ATTACK"
		created_by:   "unittest@fluidattacks.com"
		hacker:       "unittest@fluidattacks.com"
		event_date:   "2020-03-11T14:00:00+00:00"
		sk:           "GROUP#unittesting"
		client:       "Fluid Attacks"
		pk_2:         "GROUP#unittesting"
		created_date: "2019-03-11T15:57:45+00:00"
		id:           "484763304"
		pk:           "EVENT#484763304"
		state: {
			modified_by:   "unittest@fluidattacks.com"
			modified_date: "2020-04-11T18:37:00+00:00"
			status:        "SOLVED"
		}
		evidences: {
			image_1: {
				modified_date: "2020-03-11T15:57:45+00:00"
				file_name:     "unittesting_484763304_evidence_image_1.webm"
			}
			file_1: {
				modified_date: "2020-03-11T15:57:45+00:00"
				file_name:     "unittesting_484763304_evidence_file_1.csv"
			}
		}
		sk_2: "EVENT#SOLVED#true"
	},
	{
		group_name:   "unittesting"
		description:  "Esta eventualidad fue levantada para poder realizar pruebas de unittesting"
		type:         "AUTHORIZATION_SPECIAL_ATTACK"
		created_by:   "unittest@fluidattacks.com"
		hacker:       "unittest@fluidattacks.com"
		event_date:   "2019-09-19T13:09:00+00:00"
		sk:           "GROUP#unittesting"
		client:       "test"
		pk_2:         "GROUP#unittesting"
		created_date: "2019-09-19T15:43:43+00:00"
		id:           "538745942"
		pk:           "EVENT#538745942"
		state: {
			modified_by:   "unittest@fluidattacks.com"
			modified_date: "2019-09-19T15:43:43+00:00"
			status:        "CREATED"
		}
		evidences: {}
		sk_2: "EVENT#SOLVED#false"
	},
	{
		group_name:   "unittesting"
		description:  "test test test"
		type:         "MISSING_SUPPLIES"
		created_by:   "unittest@fluidattacks.com"
		hacker:       "unittest@fluidattacks.com"
		event_date:   "2019-04-02T08:02:00+00:00"
		sk:           "GROUP#unittesting"
		client:       "Fluid Attacks"
		pk_2:         "GROUP#unittesting"
		created_date: "2019-09-25T14:36:27+00:00"
		id:           "540462628"
		pk:           "EVENT#540462628"
		state: {
			modified_by:   "unittest@fluidattacks.com"
			modified_date: "2019-09-25T14:36:27+00:00"
			status:        "CREATED"
		}
		evidences: {}
		sk_2: "EVENT#SOLVED#false"
	},
	{
		group_name:   "unittesting"
		description:  "Testing a new event type"
		type:         "DATA_UPDATE_REQUIRED"
		created_by:   "unittest@fluidattacks.com"
		hacker:       "unittest@fluidattacks.com"
		event_date:   "2019-04-02T08:02:00+00:00"
		sk:           "GROUP#unittesting"
		client:       "Fluid Attacks"
		pk_2:         "GROUP#unittesting"
		created_date: "2021-05-25T14:36:27+00:00"
		id:           "540462638"
		pk:           "EVENT#540462638"
		state: {
			modified_by:   "unittest@fluidattacks.com"
			modified_date: "2021-05-25T14:36:27+00:00"
			status:        "CREATED"
		}
		evidences: {}
		sk_2: "EVENT#SOLVED#false"
	},
	{
		group_name:   "deletegroup"
		description:  "Testing this event removal"
		type:         "DATA_UPDATE_REQUIRED"
		created_by:   "unittest@fluidattacks.com"
		hacker:       "unittest@fluidattacks.com"
		event_date:   "2022-10-27T00:00:00+00:00"
		sk:           "GROUP#deletegroup"
		client:       "Fluid Attacks"
		pk_2:         "GROUP#asgard"
		created_date: "2022-10-28T00:00:00+00:00"
		id:           "48192579"
		pk:           "EVENT#48192579"
		state: {
			modified_by:   "unittest@fluidattacks.com"
			modified_date: "2022-10-28T00:00:00+00:01"
			status:        "CREATED"
		}
		evidences: {}
		sk_2: "EVENT#SOLVED#false"
	},
] & [...#EventItem]
