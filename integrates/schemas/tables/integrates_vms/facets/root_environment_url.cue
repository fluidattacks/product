package root_environment_url

import (
	"fluidattacks.com/types/roots"
)

#rootEnvironmentUrlPk: =~"^GROUP#[a-z]+#ROOT#[a-f0-9-]{36}$"
#rootEnvironmentUrlSk: =~"^URL#[a-f0-9]+$"

#RootEnvironmentUrlKeys: {
	pk:   string & #rootEnvironmentUrlPk
	sk:   string & #rootEnvironmentUrlSk
	pk_2: string
	sk_2: string
}

#RootEnvironmentUrlItem: {
	#RootEnvironmentUrlKeys
	roots.#RootEnvironmentUrl
}

RootEnvironmentUrl:
{
	FacetName: "root_environment_url"
	KeyAttributeAlias: {
		PartitionKeyAlias: "GROUP#group_name#ROOT#uuid"
		SortKeyAlias:      "URL#hash"
	}
	NonKeyAttributes: [
		"id",
		"group_name",
		"root_id",
		"url",
		"created_at",
		"created_by",
		"state",
		"pk_2",
		"sk_2",
	]
	DataAccess: {
		MySql: {}
	}
}

RootEnvironmentUrl: TableData: [
	{
		group_name: "unittesting"
		sk:         "URL#00fbe3579a253b43239954a545dc0536e6c83094"
		pk_2:       "GROUP#unittesting"
		root_id:    "4039d098-ffc5-4984-8ed3-eb17bca98e19"
		id:         "00fbe3579a253b43239954a545dc0536e6c83094"
		pk:         "GROUP#unittesting#ROOT#4039d098-ffc5-4984-8ed3-eb17bca98e19"
		state: {
			modified_by:   "integrates@fluidattacks.com"
			include:       true
			modified_date: "2024-03-13T17:33:52.932537+00:00"
			url_type:      "URL"
			status:        "CREATED"
		}
		sk_2: "URL#00fbe3579a253b43239954a545dc0536e6c83094"
		url:  "https://app.fluidattacks.com/test"
	},
	{
		group_name: "unittesting"
		sk:         "URL#00fbe3579a253b43239954a545dc0536e6c83098"
		pk_2:       "GROUP#unittesting"
		root_id:    "4039d098-ffc5-4984-8ed3-eb17bca98e19"
		id:         "00fbe3579a253b43239954a545dc0536e6c83098"
		pk:         "GROUP#unittesting#ROOT#4039d098-ffc5-4984-8ed3-eb17bca98e19"
		state: {
			modified_by:   "integrates@fluidattacks.com"
			include:       true
			modified_date: "2024-03-13T17:33:52.935263+00:00"
			url_type:      "URL"
			status:        "CREATED"
		}
		sk_2: "URL#00fbe3579a253b43239954a545dc0536e6c83098"
		url:  "https://gitlab.com/fluidattacks"
	},
	{
		group_name: "unittesting"
		sk:         "URL#ba1598f6cf5e8512cd7d776d5dd8a5f79a4fed39"
		pk_2:       "GROUP#unittesting"
		root_id:    "4039d098-ffc5-4984-8ed3-eb17bca98e19"
		id:         "ba1598f6cf5e8512cd7d776d5dd8a5f79a4fed39"
		pk:         "GROUP#unittesting#ROOT#4039d098-ffc5-4984-8ed3-eb17bca98e19"
		state: {
			modified_by:   "integrates@fluidattacks.com"
			include:       true
			modified_date: "2024-03-13T17:33:52.937480+00:00"
			url_type:      "URL"
			status:        "CREATED"
		}
		sk_2: "URL#ba1598f6cf5e8512cd7d776d5dd8a5f79a4fed39"
		url:  "https://test.com"
	},
	{
		group_name: "unittesting"
		sk:         "URL#9c88cd5a4eda379f6200c3092e6e65b47b18a2a5"
		pk_2:       "GROUP#unittesting"
		root_id:    "9eccb9ed-e835-4dbc-b28e-8cecd0296e27"
		id:         "9c88cd5a4eda379f6200c3092e6e65b47b18a2a5"
		pk:         "GROUP#unittesting#ROOT#9eccb9ed-e835-4dbc-b28e-8cecd0296e27"
		state: {
			modified_by:   "integrates@fluidattacks.com"
			include:       true
			modified_date: "2024-03-13T17:33:52.937480+00:00"
			url_type:      "URL"
			status:        "CREATED"
		}
		sk_2: "URL#9c88cd5a4eda379f6200c3092e6e65b47b18a2a5"
		url:  "https://test.com"
	},
	{
		group_name: "groudon"
		sk:         "URL#9f402f55db4f0bd9f8a2315430b3460e7f1ab8af"
		pk_2:       "GROUP#groudon"
		root_id:    "68416808-a69b-4585-ae24-ac2ff8b8b007"
		id:         "9f402f55db4f0bd9f8a2315430b3460e7f1ab8af"
		pk:         "GROUP#groudon#ROOT#68416808-a69b-4585-ae24-ac2ff8b8b007"
		state: {
			modified_by:   "__adminEmail__"
			include:       true
			modified_date: "2024-03-13T17:33:52.937480+00:00"
			url_type:      "URL"
			status:        "CREATED"
		}
		sk_2: "URL#9f402f55db4f0bd9f8a2315430b3460e7f1ab8af"
		url:  "https://example.com"
	},
	{
		group_name: "makimachi"
		sk:         "URL#9f402f55db4f0bd9f8a2315430b3460e7f1ab801"
		pk_2:       "GROUP#makimachi"
		root_id:    "3a69ee71-1183-4cbe-99d9-27b2617df7d5"
		id:         "9f402f55db4f0bd9f8a2315430b3460e7f1ab801"
		pk:         "GROUP#makimachi#ROOT#3a69ee71-1183-4cbe-99d9-27b2617df7d5"
		state: {
			modified_by:   "__adminEmail__"
			include:       true
			modified_date: "2024-01-01T00:00:00+00:00"
			url_type:      "CSPM"
			cloud_name:    "AWS"
			status:        "CREATED"
		}
		sk_2: "URL#9f402f55db4f0bd9f8a2315430b3460e7f1ab801"
		url:  "arn:aws:iam::999911662222:role/CSPMRole"
	},
] & [...#RootEnvironmentUrlItem]
