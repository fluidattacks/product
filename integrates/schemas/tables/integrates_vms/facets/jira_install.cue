package jira_install

import (
	"fluidattacks.com/types/jira"
)

#jiraInstallPk: =~"^CLIENT_KEY#[a-zA-Z0-9-]+$"
#jiraInstallSk: =~"^SITE#https?:\/\/[^[:space:]\/$.?#][^[:space:]]*$"

#JiraInstallKeys: {
	pk: string & #jiraInstallPk
	sk: string & #jiraInstallSk
}

#JiraInstallItem: {
	#JiraInstallKeys
	jira.#JiraSecurityInstall
}

JiraInstall:
{
	FacetName: "jira_install"
	KeyAttributeAlias: {
		PartitionKeyAlias: "CLIENT_KEY#client_key"
		SortKeyAlias:      "SITE#base_url"
	}
	NonKeyAttributes: [
		"base_url",
		"client_key",
		"cloud_id",
		"description",
		"display_url",
		"event_type",
		"installation_id",
		"key",
		"plugins_version",
		"product_type",
		"public_key",
		"server_version",
		"shared_secret",
		"state",
		"associated_email",
	]
	DataAccess: {
		MySql: {}
	}
}

JiraInstall: TableData: [
	{
		sk:               "SITE#https://fluidattacks-dev.atlassian.net"
		pk:               "CLIENT_KEY#42xjhmdl-2zga-e6e8-ox5m-5v7exn16un1k"
		base_url:         "https://fluidattacks-dev.atlassian.net"
		client_key:       "42xjhmdl-2zga-e6e8-ox5m-5v7exn16un1k"
		cloud_id:         "12utleki-nh81-6ko8-vkkk-e4v3190s5syz"
		description:      "testing description"
		display_url:      "https://fluidattacks-dev.atlassian.net"
		event_type:       "installed"
		installation_id:  "ari:cloud:ecosystem: :installation/aSj1b82x-dWCo-px3c-SxV0-77nAHnFu2c7K"
		key:              "com.testing.testing-jira-app"
		plugins_version:  "1001.0.0-SNAPSHOT"
		product_type:     "jira"
		public_key:       "hlybwXMQIETJzKkS6jbBzvH0LzL4PDxtmIASOA0tMp9EDAE5Q4ukNUkjvGVB7S6AjlG1RrogO6Y3H22oOfrdCgaghXFApMtInSnRo9lNWdpzsY8rqq3fV4fTJwZIRcHhwvx5H7ncjdZxndaq87TOoxOS9GVOsUJqoum0xwMSASvxtckj01cAFj99JRiTj2jako5jUAJjens9870be32mASE"
		server_version:   "100100"
		shared_secret:    "IvRLesJmAybihOsZQJ8tt6c5OSK8bL8mxx8fxVyKVE6GxQyrP42nrqdfnAzUCwkzKofoevreaDsFViY2OEEx8YqSG2NgHzVTpL"
		associated_email: "integratesuser@gmail.com"
		state: {
			modified_by:    "integratesuser@gmail.com"
			modified_date:  "2021-09-01T00:00:00Z"
			used_api_token: "test_api_token"
			used_jira_jwt:  "test_jira_jwt"
		}
	},
] & [...#JiraInstallItem]
