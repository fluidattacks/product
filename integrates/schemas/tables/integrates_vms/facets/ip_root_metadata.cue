package ip_root_metadata

import (
	"fluidattacks.com/types/roots"
)

#ipRootMetadataPk: =~"^ROOT#[a-z0-9-]+"
#ipRootMetadataSk: =~"^GROUP#[a-z]+"

#IpRootMetadataKeys: {
	pk:   string & #ipRootMetadataPk
	sk:   string & #ipRootMetadataSk
	pk_2: string & =~"^ORG#[a-z]+"
	sk_2: string & =~"^ROOT#[a-z0-9-]+"
}

#IpRootItem: {
	#IpRootMetadataKeys
	roots.#IpRootMetadata
}

IpRootMetadata:
{
	FacetName: "ip_root_metadata"
	KeyAttributeAlias: {
		PartitionKeyAlias: "ROOT#uuid"
		SortKeyAlias:      "GROUP#name"
	}
	NonKeyAttributes: [
		"type",
		"created_date",
		"pk_2",
		"sk_2",
		"state",
		"unreliable_indicators",
		"created_by",
	]
	DataAccess: {
		MySql: {}
	}
}

IpRootMetadata: TableData: [
	{
		sk:           "GROUP#asgard"
		pk_2:         "ORG#okada"
		created_date: "2020-12-19T13:44:37+00:00"
		pk:           "ROOT#814addf0-316c-4415-850d-21bd3783b011"
		state: {
			modified_by:   "jdoe@fluidattacks.com"
			nickname:      "ip_root_2"
			address:       "127.0.0.1"
			modified_date: "2020-12-19T13:44:37+00:00"
			status:        "INACTIVE"
		}
		type:       "IP"
		created_by: "jdoe@fluidattacks.com"
		sk_2:       "ROOT#814addf0-316c-4415-850d-21bd3783b011"
	},
	{
		sk:           "GROUP#oneshottest"
		pk_2:         "ORG#okada"
		created_date: "2020-11-19T13:44:37+00:00"
		pk:           "ROOT#d312f0b9-da49-4d2b-a881-bed438875e99"
		state: {
			modified_by:   "jdoe@fluidattacks.com"
			nickname:      "ip_root_1"
			address:       "127.0.0.1"
			modified_date: "2020-11-19T13:44:37+00:00"
			status:        "ACTIVE"
		}
		type:       "IP"
		created_by: "jdoe@fluidattacks.com"
		sk_2:       "ROOT#d312f0b9-da49-4d2b-a881-bed438875e99"
	},
	{
		sk:           "GROUP#oneshottest"
		pk_2:         "ORG#okada"
		created_date: "2024-04-22T13:44:37+00:00"
		pk:           "ROOT#80596eb9-8177-42bb-9f67-bea2101fe7c5"
		state: {
			modified_by:   "integratesmanager@fluidattacks.com"
			nickname:      "testing_ip_root"
			address:       "192.168.50.111"
			modified_date: "2024-04-22T13:44:37+00:00"
			status:        "ACTIVE"
		}
		type:       "IP"
		created_by: "integratesmanager@fluidattacks.com"
		sk_2:       "ROOT#80596eb9-8177-42bb-9f67-bea2101fe7c5"
	},
] & [...#IpRootItem]
