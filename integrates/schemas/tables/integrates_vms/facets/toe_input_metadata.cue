package toe_input_metadata

import (
	"fluidattacks.com/types/toe_inputs"
)

#toeInputMetadataPk: =~"^GROUP#[a-z]+$"
#toeInputMetadataSk: =~"^INPUTS#ROOT#[a-f0-9-]{36}#COMPONENT#(.*?)#ENTRYPOINT#(.*?)$"

#ToeInputMetadataKeys: {
	pk:   string & #toeInputMetadataPk
	sk:   string & #toeInputMetadataSk
	pk_2: string
	sk_2: string
}

#ToeInputItem: {
	#ToeInputMetadataKeys
	toe_inputs.#ToeInputMetadata
}

ToeInputMetadata:
{
	FacetName: "toe_input_metadata"
	KeyAttributeAlias: {
		PartitionKeyAlias: "GROUP#group_name"
		SortKeyAlias:      "INPUTS#ROOT#root_id#COMPONENT#component#ENTRYPOINT#entry_point"
	}
	NonKeyAttributes: [
		"component",
		"entry_point",
		"environment_id",
		"group_name",
		"root_id",
		"pk_2",
		"sk_2",
		"state",
		"attacked_at",
		"attacked_by",
		"be_present",
		"be_present_until",
		"first_attack_at",
		"has_vulnerabilities",
		"modified_by",
		"modified_date",
		"seen_at",
		"seen_first_time_by",
	]
	DataAccess: {
		MySql: {}
	}
}

ToeInputMetadata: TableData: [
	{
		group_name:     "unittesting"
		pk:             "GROUP#unittesting"
		sk:             "INPUTS#ROOT#9eccb9ed-e835-4dbc-b28e-8cecd0296e27#COMPONENT#https://test.com/#ENTRYPOINT#/dashboard"
		pk_2:           "GROUP#unittesting"
		sk_2:           "INPUTS#PRESENT#true#ROOT#9eccb9ed-e835-4dbc-b28e-8cecd0296e27#COMPONENT#https://test.com/#ENTRYPOINT#/dashboard"
		root_id:        "9eccb9ed-e835-4dbc-b28e-8cecd0296e27"
		environment_id: "9c88cd5a4eda379f6200c3092e6e65b47b18a2a5"
		component:      "https://test.com/"
		entry_point:    "/dashboard"
		state: {
			seen_first_time_by:  "unittest@fluidattacks.com"
			has_vulnerabilities: false
			modified_by:         "unittest@fluidattacks.com"
			attacked_at:         "2024-11-14T10:00:00+00:00"
			be_present:          true
			first_attack_at:     "2024-11-14T10:00:00+00:00"
			modified_date:       "2024-11-14T10:00:00+00:00"
			attacked_by:         "unittest@fluidattacks.com"
			seen_at:             "2024-11-14T10:00:00+00:00"
		}
	},
	{
		group_name:     "unittesting"
		pk:             "GROUP#unittesting"
		sk:             "INPUTS#ROOT#4039d098-ffc5-4984-8ed3-eb17bca98e19#COMPONENT#https://test.com/#ENTRYPOINT#/"
		pk_2:           "GROUP#unittesting"
		sk_2:           "INPUTS#PRESENT#true#ROOT#4039d098-ffc5-4984-8ed3-eb17bca98e19#COMPONENT#https://test.com/#ENTRYPOINT#/"
		root_id:        "4039d098-ffc5-4984-8ed3-eb17bca98e19"
		environment_id: "ba1598f6cf5e8512cd7d776d5dd8a5f79a4fed39"
		component:      "https://test.com/"
		entry_point:    "/"
		state: {
			seen_first_time_by:  "unittest@fluidattacks.com"
			has_vulnerabilities: false
			modified_by:         "unittest@fluidattacks.com"
			attacked_at:         "2022-01-01T05:00:00+00:00"
			be_present:          true
			first_attack_at:     "2022-01-01T05:00:00+00:00"
			modified_date:       "2022-01-01T05:00:00+00:00"
			attacked_by:         "unittest@fluidattacks.com"
			seen_at:             "2022-01-01T05:00:00+00:00"
		}
	},
	{
		group_name:     "unittesting"
		pk:             "GROUP#unittesting"
		sk:             "INPUTS#ROOT#4039d098-ffc5-4984-8ed3-eb17bca98e19#COMPONENT#https://test.com/test2/test.aspx#ENTRYPOINT#/"
		pk_2:           "GROUP#unittesting"
		sk_2:           "INPUTS#PRESENT#true#ROOT#4039d098-ffc5-4984-8ed3-eb17bca98e19#COMPONENT#https://test.com/test2/test.aspx#ENTRYPOINT#/"
		root_id:        "4039d098-ffc5-4984-8ed3-eb17bca98e19"
		environment_id: "ba1598f6cf5e8512cd7d776d5dd8a5f79a4fed39"
		component:      "https://test.com/test2/test.aspx"
		entry_point:    "/"
		state: {
			seen_first_time_by:  "test2@test.com"
			has_vulnerabilities: false
			modified_by:         "test2@test.com"
			attacked_at:         "2021-02-11T05:00:00+00:00"
			be_present:          true
			first_attack_at:     "2021-02-11T05:00:00+00:00"
			modified_date:       "2021-02-11T05:00:00+00:00"
			attacked_by:         "test2@test.com"
			seen_at:             "2020-01-11T05:00:00+00:00"
		}
	},
	{
		group_name:     "unittesting"
		pk:             "GROUP#unittesting"
		sk:             "INPUTS#ROOT#4039d098-ffc5-4984-8ed3-eb17bca98e19#COMPONENT#https://test.com/test2/test.aspx#ENTRYPOINT#idTest (en-us)"
		pk_2:           "GROUP#unittesting"
		sk_2:           "INPUTS#PRESENT#true#ROOT#4039d098-ffc5-4984-8ed3-eb17bca98e19#COMPONENT#https://test.com/test2/test.aspx#ENTRYPOINT#idTest (en-us)"
		root_id:        "4039d098-ffc5-4984-8ed3-eb17bca98e19"
		environment_id: "ba1598f6cf5e8512cd7d776d5dd8a5f79a4fed39"
		component:      "https://test.com/test2/test.aspx"
		entry_point:    "idTest (en-us)"
		state: {
			seen_first_time_by:  "test2@test.com"
			has_vulnerabilities: true
			modified_by:         "test2@test.com"
			attacked_at:         "2021-02-11T05:00:00+00:00"
			be_present:          true
			first_attack_at:     "2021-02-11T05:00:00+00:00"
			modified_date:       "2021-02-11T05:00:00+00:00"
			attacked_by:         "test2@test.com"
			seen_at:             "2020-01-11T05:00:00+00:00"
		}
	},
	{
		group_name:     "unittesting"
		pk:             "GROUP#unittesting"
		sk:             "INPUTS#ROOT#4039d098-ffc5-4984-8ed3-eb17bca98e19#COMPONENT#https://gitlab.com/fluidattacks/test2/test.aspx#ENTRYPOINT#/"
		pk_2:           "GROUP#unittesting"
		sk_2:           "INPUTS#PRESENT#true#ROOT#4039d098-ffc5-4984-8ed3-eb17bca98e19#COMPONENT#https://gitlab.com/fluidattacks/test2/test.aspx#ENTRYPOINT#/"
		root_id:        "4039d098-ffc5-4984-8ed3-eb17bca98e19"
		environment_id: "00fbe3579a253b43239954a545dc0536e6c83098"
		component:      "https://gitlab.com/fluidattacks"
		entry_point:    "/"
		state: {
			seen_first_time_by:  "test2@test.com"
			has_vulnerabilities: false
			modified_by:         "test2@test.com"
			attacked_at:         "2021-02-09T05:00:00+00:00"
			be_present:          true
			first_attack_at:     "2021-02-09T05:00:00+00:00"
			modified_date:       "2021-02-09T05:00:00+00:00"
			attacked_by:         "test2@test.com"
			seen_at:             "2020-01-09T05:00:00+00:00"
		}
	},
	{
		group_name:     "groudon"
		pk:             "GROUP#groudon"
		sk:             "INPUTS#ROOT#68416808-a69b-4585-ae24-ac2ff8b8b007#COMPONENT#https://example.com/admin#ENTRYPOINT#user"
		pk_2:           "GROUP#groudon"
		sk_2:           "INPUTS#PRESENT#true#ROOT#68416808-a69b-4585-ae24-ac2ff8b8b007#COMPONENT#https://example.com/admin#ENTRYPOINT#user"
		root_id:        "68416808-a69b-4585-ae24-ac2ff8b8b007"
		environment_id: "9f402f55db4f0bd9f8a2315430b3460e7f1ab8af"
		component:      "https://example.com/admin"
		entry_point:    "user"
		state: {
			seen_first_time_by:  "unittest@fluidattacks.com"
			has_vulnerabilities: true
			modified_by:         "unittest@fluidattacks.com"
			attacked_at:         "2024-09-19T11:00:00+00:00"
			be_present:          true
			first_attack_at:     "2024-09-19T11:00:00+00:00"
			modified_date:       "2024-09-19T11:00:00+00:00"
			attacked_by:         "unittest@fluidattacks.com"
			seen_at:             "2024-09-19T11:00:00+00:00"
		}
	},
	{
		group_name:     "groudon"
		pk:             "GROUP#groudon"
		sk:             "INPUTS#ROOT#68416808-a69b-4585-ae24-ac2ff8b8b007#COMPONENT#https://example.com/admin#ENTRYPOINT#password"
		pk_2:           "GROUP#groudon"
		sk_2:           "INPUTS#PRESENT#true#ROOT#68416808-a69b-4585-ae24-ac2ff8b8b007#COMPONENT#https://example.com/admin#ENTRYPOINT#password"
		root_id:        "68416808-a69b-4585-ae24-ac2ff8b8b007"
		environment_id: "9f402f55db4f0bd9f8a2315430b3460e7f1ab8af"
		component:      "https://example.com/admin"
		entry_point:    "password"
		state: {
			seen_first_time_by:  "unittest@fluidattacks.com"
			has_vulnerabilities: true
			modified_by:         "unittest@fluidattacks.com"
			attacked_at:         "2024-09-19T11:00:00+00:00"
			be_present:          true
			first_attack_at:     "2024-09-19T11:00:00+00:00"
			modified_date:       "2024-09-19T11:00:00+00:00"
			attacked_by:         "unittest@fluidattacks.com"
			seen_at:             "2024-09-19T11:00:00+00:00"
		}
	},
	{
		group_name:     "groudon"
		pk:             "GROUP#groudon"
		sk:             "INPUTS#ROOT#68416808-a69b-4585-ae24-ac2ff8b8b007#COMPONENT#https://example.com/#ENTRYPOINT#/"
		pk_2:           "GROUP#groudon"
		sk_2:           "INPUTS#PRESENT#true#ROOT#68416808-a69b-4585-ae24-ac2ff8b8b007#COMPONENT#https://example.com/#ENTRYPOINT#/"
		root_id:        "68416808-a69b-4585-ae24-ac2ff8b8b007"
		environment_id: "9f402f55db4f0bd9f8a2315430b3460e7f1ab8af"
		component:      "https://example.com/"
		entry_point:    "/"
		state: {
			seen_first_time_by:  "unittest@fluidattacks.com"
			has_vulnerabilities: true
			modified_by:         "unittest@fluidattacks.com"
			attacked_at:         "2024-09-19T11:00:00+00:00"
			be_present:          true
			first_attack_at:     "2024-09-19T11:00:00+00:00"
			modified_date:       "2024-09-19T11:00:00+00:00"
			attacked_by:         "unittest@fluidattacks.com"
			seen_at:             "2024-09-19T11:00:00+00:00"
		}
	},
] & [...#ToeInputItem]
