package stakeholder_metadata

import (
	"fluidattacks.com/types/stakeholders"
)

#stakeholderMetadataPk: =~"^USER#[a-zA-Z0-9._%+-@]+$"
#stakeholderMetadataSk: =~"^USER#[a-zA-Z0-9._%+-@]+$"

#StakeholderMetadataKeys: {
	pk:   string & #stakeholderMetadataPk
	sk:   string & #stakeholderMetadataSk
	pk_2: string
	sk_2: string
}

#StakeholderItem: {
	#StakeholderMetadataKeys
	stakeholders.#StakeholderMetadata
}

StakeholderMetadata:
{
	FacetName: "stakeholder_metadata"
	KeyAttributeAlias: {
		PartitionKeyAlias: "USER#email"
		SortKeyAlias:      "USER#email"
	}
	NonKeyAttributes: [
		"access_tokens",
		"aws_customer_id",
		"email",
		"enrolled",
		"first_name",
		"is_concurrent_session",
		"is_registered",
		"last_login_date",
		"last_name",
		"legal_remember",
		"login",
		"phone",
		"pk_2",
		"registration_date",
		"role",
		"session_token",
		"sk_2",
		"state",
		"tours",
	]
	DataAccess: {
		MySql: {}
	}
}

StakeholderMetadata: TableData: [
	{
		enrolled:              true
		is_concurrent_session: false
		is_registered:         true
		role:                  "user"
		last_name:             "User"
		last_login_date:       "2020-12-31T18:40:37+00:00"
		session_token: {
			jti:   "0f98c8d494be2c9eddd973e4a861483988a1d90bb268be48dfc442d0b4cada72"
			state: "IS_VALID"
		}
		tours: {
			new_group:         false
			new_root:          false
			new_risk_exposure: false
			welcome:           false
		}
		legal_remember:    true
		registration_date: "2018-02-28T16:54:12+00:00"
		sk:                "USER#integratesuser@gmail.com"
		pk_2:              "USER#all"
		pk:                "USER#integratesuser@gmail.com"
		access_tokens:
		[
			{
				id:         "43ab2e54-5f02-09c0-a1bb-b8328251bfe3"
				issued_at:  1740434398
				jti_hashed: "61a12cbc77fdf4fba47e5bdba9a44f14b2bd7d7f4b872d2d7562d1fa89f770f9"
				salt:       "87a7ea662a8327f00497c55c91df7ffbe22b9c992f01270ba08e7d3562f823d1"
			},
		]

		state: {
			modified_date: "2018-02-28T16:54:12+00:00"
			modified_by:   "integratesuser@gmail.com"
			notifications_preferences: {
				sms: []
				email:
				[
					"ACCESS_GRANTED",
					"AGENT_TOKEN",
					"EVENT_REPORT",
					"FILE_UPDATE",
					"GROUP_INFORMATION",
					"GROUP_REPORT",
					"NEW_COMMENT",
					"NEW_DRAFT",
					"PORTFOLIO_UPDATE",
					"REMEDIATE_FINDING",
					"REMINDER_NOTIFICATION",
					"ROOT_UPDATE",
					"SERVICE_UPDATE",
					"UNSUBSCRIPTION_ALERT",
					"UPDATED_TREATMENT",
					"VULNERABILITY_ASSIGNED",
					"VULNERABILITY_REPORT",
				]

			}
		}
		login: {
			country_code:    "CO"
			provider:        "BITBUCKET"
			subject:         "BITBUCKET#2222223333333344445555"
			expiration_time: 2026492340
			browser:         "Chrome 114.0.0.0"
			modified_by:     "integrates@fluidattacks.com"
			ip_address:      "127.0.0.1"
			modified_date:   "2024-03-22T18:32:20.800014+00:00"
			device:          "Linux"
		}
		first_name: "Integrates"
		email:      "integratesuser@gmail.com"
		sk_2:       "USER#integratesuser@gmail.com"
	},
	{
		enrolled:              true
		is_concurrent_session: false
		is_registered:         true
		role:                  "user"
		last_name:             ""
		tours: {
			new_group:         false
			new_root:          false
			new_risk_exposure: false
			welcome:           false
		}
		access_tokens:
		[
			{
				id:         "f5412ea7-c6e9-4506-b2f4-0362edd0b9fd"
				issued_at:  1740434178
				jti_hashed: "1537cb0757c4563276525a2c330bd2740469a8b899d1d3fd36902ecf83d55500"
				salt:       "e8b527649674766dcf1e110593a0fa9a995dcf2b5d18caab6e3032d1d3e66196"
			},
		]

		legal_remember: true
		sk:             "USER#forces.unittesting@fluidattacks.com"
		pk_2:           "USER#all"
		pk:             "USER#forces.unittesting@fluidattacks.com"
		first_name:     ""
		email:          "forces.unittesting@fluidattacks.com"
		sk_2:           "USER#forces.unittesting@fluidattacks.com"
	},
	{
		enrolled:              true
		is_concurrent_session: false
		is_registered:         true
		role:                  "user"
		last_name:             "Customer"
		last_login_date:       "2020-12-31T18:40:37+00:00"
		tours: {
			new_group:         false
			new_root:          false
			new_risk_exposure: false
			welcome:           false
		}
		legal_remember:    true
		registration_date: "2018-02-28T16:54:12+00:00"
		sk:                "USER#integratesuser2@gmail.com"
		pk_2:              "USER#all"
		pk:                "USER#integratesuser2@gmail.com"
		state: {
			modified_date: "2018-02-28T16:54:12+00:00"
			modified_by:   "integratesuser2@gmail.com"
			notifications_preferences: {
				sms: []
				email:
				[
					"ACCESS_GRANTED",
					"EVENT_REPORT",
					"GROUP_REPORT",
					"NEW_COMMENT",
					"NEW_DRAFT",
					"REMEDIATE_FINDING",
					"REMINDER_NOTIFICATION",
					"UPDATED_TREATMENT",
					"VULNERABILITY_ASSIGNED",
					"VULNERABILITY_REPORT",
				]

			}
		}
		login: {
			country_code:    ""
			provider:        "MICROSOFT"
			subject:         "MICROSOFT#2222223333333344445555"
			expiration_time: 2026492340
			browser:         "Chrome 114.0.0.0"
			modified_by:     "integrates@fluidattacks.com"
			ip_address:      "127.0.0.1"
			modified_date:   "2024-03-22T18:32:20.800014+00:00"
			device:          "Linux"
		}
		first_name: "Integrates"
		email:      "integratesuser2@gmail.com"
		sk_2:       "USER#integratesuser2@gmail.com"
	},
	{
		enrolled:              true
		is_concurrent_session: false
		is_registered:         true
		role:                  "admin"
		last_name:             "Manager"
		last_login_date:       "2020-12-31T16:50:17+00:00"
		tours: {
			new_group:         true
			new_root:          true
			new_risk_exposure: true
			welcome:           true
		}
		legal_remember:    true
		registration_date: "2018-02-28T16:54:12+00:00"
		phone: {
			national_number:      "1234567891"
			country_code:         "CO"
			calling_country_code: "57"
		}
		sk:   "USER#integratesmanager@fluidattacks.com"
		pk_2: "USER#all"
		pk:   "USER#integratesmanager@fluidattacks.com"
		state: {
			modified_date: "2018-02-28T16:54:12+00:00"
			modified_by:   "integratesmanager@fluidattacks.com"
			notifications_preferences: {
				sms: []
				email:
				[
					"ACCESS_GRANTED",
					"AGENT_TOKEN",
					"EVENT_REPORT",
					"FILE_UPDATE",
					"GROUP_INFORMATION",
					"GROUP_REPORT",
					"NEW_COMMENT",
					"NEW_DRAFT",
					"PORTFOLIO_UPDATE",
					"REMEDIATE_FINDING",
					"REMINDER_NOTIFICATION",
					"ROOT_UPDATE",
					"SERVICE_UPDATE",
					"UNSUBSCRIPTION_ALERT",
					"UPDATED_TREATMENT",
					"VULNERABILITY_ASSIGNED",
					"VULNERABILITY_REPORT",
				]

			}
		}
		login: {
			country_code:    "CO"
			provider:        "GOOGLE"
			subject:         "GOOGLE#2222223333333344445555"
			expiration_time: 2026492340
			browser:         "Chrome 114.0.0.0"
			modified_by:     "integrates@fluidattacks.com"
			ip_address:      "127.0.0.1"
			modified_date:   "2024-03-22T18:32:20.800014+00:00"
			device:          "Linux"
		}
		first_name: "Integrates"
		email:      "integratesmanager@fluidattacks.com"
		sk_2:       "USER#integratesmanager@fluidattacks.com"
	},
	{
		enrolled:              true
		is_concurrent_session: false
		is_registered:         true
		role:                  "hacker"
		last_name:             "Reattacker"
		last_login_date:       "2020-12-31T18:40:37+00:00"
		tours: {
			new_group:         false
			new_root:          false
			new_risk_exposure: false
			welcome:           true
		}
		legal_remember:    true
		registration_date: "2018-02-28T16:54:12+00:00"
		sk:                "USER#integratesreattacker@fluidattacks.com"
		pk_2:              "USER#all"
		pk:                "USER#integratesreattacker@fluidattacks.com"
		first_name:        "Integrates"
		email:             "integratesreattacker@fluidattacks.com"
		sk_2:              "USER#integratesreattacker@fluidattacks.com"
	},
	{
		enrolled:              true
		is_concurrent_session: false
		is_registered:         false
		role:                  "user"
		last_name:             "Manager"
		last_login_date:       "2020-12-31T18:40:37+00:00"
		tours: {
			new_group:         false
			new_root:          false
			new_risk_exposure: false
			welcome:           false
		}
		legal_remember:    true
		registration_date: "2022-02-16T04:54:55+00:00"
		sk:                "USER#vulnmanager@gmail.com"
		pk_2:              "USER#all"
		pk:                "USER#vulnmanager@gmail.com"
		first_name:        "Vooln"
		email:             "vulnmanager@gmail.com"
		sk_2:              "USER#vulnmanager@gmail.com"
	},
	{
		aws_customer_id:       "4oq3nuzyj98"
		enrolled:              true
		is_concurrent_session: false
		is_registered:         true
		role:                  "admin"
		last_name:             "Manager"
		last_login_date:       "2020-12-31T16:50:17+00:00"
		tours: {
			new_group:         false
			new_root:          false
			new_risk_exposure: false
			welcome:           false
		}
		legal_remember:    true
		registration_date: "2018-02-28T16:54:12+00:00"
		phone: {
			national_number:      "1234567894"
			country_code:         "CO"
			calling_country_code: "57"
		}
		sk:   "USER#integratesmanager@gmail.com"
		pk_2: "USER#all"
		pk:   "USER#integratesmanager@gmail.com"
		state: {
			modified_date: "2018-02-28T16:54:12+00:00"
			modified_by:   "integratesmanager@gmail.com"
			notifications_preferences: {
				sms: []
				email:
				[
					"ACCESS_GRANTED",
					"AGENT_TOKEN",
					"EVENT_REPORT",
					"FILE_UPDATE",
					"GROUP_INFORMATION",
					"GROUP_REPORT",
					"NEW_COMMENT",
					"NEW_DRAFT",
					"PORTFOLIO_UPDATE",
					"REMEDIATE_FINDING",
					"REMINDER_NOTIFICATION",
					"ROOT_UPDATE",
					"SERVICE_UPDATE",
					"UNSUBSCRIPTION_ALERT",
					"UPDATED_TREATMENT",
					"VULNERABILITY_ASSIGNED",
					"VULNERABILITY_REPORT",
				]

			}
		}
		first_name: "Integrates"
		email:      "integratesmanager@gmail.com"
		sk_2:       "USER#integratesmanager@gmail.com"
	},
	{
		enrolled:              true
		is_concurrent_session: false
		is_registered:         true
		role:                  "hacker"
		last_name:             "Hacker2"
		last_login_date:       "2020-12-31T20:22:37+00:00"
		tours: {
			new_group:         false
			new_root:          false
			new_risk_exposure: false
			welcome:           false
		}
		legal_remember:    true
		registration_date: "2012-12-22T04:59:59+00:00"
		sk:                "USER#integratesanalyst2@gmail.com"
		pk_2:              "USER#all"
		pk:                "USER#integratesanalyst2@gmail.com"
		first_name:        "Integrates2"
		email:             "integratesanalyst2@gmail.com"
		sk_2:              "USER#integratesanalyst2@gmail.com"
	},
	{
		enrolled:              true
		is_concurrent_session: false
		is_registered:         true
		role:                  "hacker"
		last_name:             "Reviewer"
		last_login_date:       "2020-12-31T18:40:37+00:00"
		tours: {
			new_group:         false
			new_root:          false
			new_risk_exposure: false
			welcome:           false
		}
		legal_remember:    true
		registration_date: "2018-02-28T16:54:12+00:00"
		sk:                "USER#integratesreviewer@fluidattacks.com"
		pk_2:              "USER#all"
		pk:                "USER#integratesreviewer@fluidattacks.com"
		first_name:        "Integrates"
		email:             "integratesreviewer@fluidattacks.com"
		sk_2:              "USER#integratesreviewer@fluidattacks.com"
		login: {
			country_code:    "US"
			provider:        "GOOGLE"
			subject:         "GOOGLE#2222223333333344445555"
			expiration_time: 2026492340
			browser:         "Chrome 114.0.0.0"
			modified_by:     "integrates@fluidattacks.com"
			ip_address:      "127.0.0.1"
			modified_date:   "2024-03-22T18:32:20.800014+00:00"
			device:          "Linux"
		}
	},
	{
		enrolled:              true
		is_concurrent_session: false
		is_registered:         true
		role:                  "hacker"
		last_name:             "Hacking"
		last_login_date:       "2020-12-31T15:45:37+00:00"
		tours: {
			new_group:         false
			new_root:          false
			new_risk_exposure: false
			welcome:           false
		}
		legal_remember:    true
		registration_date: "2018-02-28T16:54:12+00:00"
		phone: {
			national_number:      "1234567893"
			country_code:         "CO"
			calling_country_code: "57"
		}
		sk:   "USER#continuoushacking@gmail.com"
		pk_2: "USER#all"
		pk:   "USER#continuoushacking@gmail.com"
		state: {
			modified_date: "2018-02-28T16:54:12+00:00"
			modified_by:   "continuoushacking@gmail.com"
			notifications_preferences: {
				sms: []
				email:
				[
					"ACCESS_GRANTED",
					"AGENT_TOKEN",
					"EVENT_REPORT",
					"FILE_UPDATE",
					"GROUP_INFORMATION",
					"GROUP_REPORT",
					"NEW_COMMENT",
					"NEW_DRAFT",
					"PORTFOLIO_UPDATE",
					"REMEDIATE_FINDING",
					"REMINDER_NOTIFICATION",
					"ROOT_UPDATE",
					"SERVICE_UPDATE",
					"UNSUBSCRIPTION_ALERT",
					"UPDATED_TREATMENT",
					"VULNERABILITY_ASSIGNED",
					"VULNERABILITY_REPORT",
				]

			}
		}
		first_name: "Continuous"
		email:      "continuoushacking@gmail.com"
		sk_2:       "USER#continuoushacking@gmail.com"
	},
	{
		enrolled:              true
		is_concurrent_session: false
		is_registered:         false
		last_name:             ""
		last_login_date:       "2020-12-31T18:40:37+00:00"
		tours: {
			new_group:         false
			new_root:          false
			new_risk_exposure: false
			welcome:           false
		}
		legal_remember:    true
		registration_date: "2017-12-29T04:54:55+00:00"
		sk:                "USER#unittest3"
		pk_2:              "USER#all"
		pk:                "USER#unittest3"
		first_name:        ""
		email:             "unittest3"
		sk_2:              "USER#unittest3"
	},
	{
		enrolled:              true
		is_concurrent_session: false
		is_registered:         true
		role:                  "hacker"
		last_name:             "Owner"
		last_login_date:       "2020-12-31T18:40:37+00:00"
		tours: {
			new_group:         false
			new_root:          false
			new_risk_exposure: false
			welcome:           false
		}
		legal_remember:    true
		registration_date: "2021-08-28T16:54:12+00:00"
		sk:                "USER#customer_manager@fluidattacks.com"
		pk_2:              "USER#all"
		pk:                "USER#customer_manager@fluidattacks.com"
		state: {
			modified_date: "2018-08-28T16:54:12+00:00"
			modified_by:   "customer_manager@fluidattacks.com"
			notifications_preferences: {
				sms: []
				email:
				[
					"ACCESS_GRANTED",
					"AGENT_TOKEN",
					"EVENT_REPORT",
					"FILE_UPDATE",
					"GROUP_INFORMATION",
					"GROUP_REPORT",
					"NEW_COMMENT",
					"NEW_DRAFT",
					"PORTFOLIO_UPDATE",
					"REMEDIATE_FINDING",
					"REMINDER_NOTIFICATION",
					"ROOT_UPDATE",
					"SERVICE_UPDATE",
					"UNSUBSCRIPTION_ALERT",
					"UPDATED_TREATMENT",
					"VULNERABILITY_ASSIGNED",
					"VULNERABILITY_REPORT",
				]

			}
		}
		first_name: "System"
		email:      "customer_manager@fluidattacks.com"
		sk_2:       "USER#customer_manager@fluidattacks.com"
	},
	{
		enrolled:              true
		is_concurrent_session: false
		is_registered:         true
		role:                  "admin"
		last_name:             "Manager"
		last_login_date:       "2020-12-31T16:50:17+00:00"
		tours: {
			new_group:         false
			new_root:          false
			new_risk_exposure: false
			welcome:           false
		}
		login: {
			country_code:    "PA"
			provider:        "GOOGLE"
			subject:         "GOOGLE#2222223333333344445555"
			expiration_time: 2026492340
			browser:         "Chrome 114.0.0.0"
			modified_by:     "integrates@fluidattacks.com"
			ip_address:      "127.0.0.1"
			modified_date:   "2024-03-22T18:32:20.800014+00:00"
			device:          "Linux"
		}
		legal_remember:    true
		registration_date: "2018-02-28T16:54:12+00:00"
		sk:                "USER#__adminEmail__"
		pk_2:              "USER#all"
		pk:                "USER#__adminEmail__"
		first_name:        "Integrates"
		email:             "__adminEmail__"
		sk_2:              "USER#__adminEmail__"
	},
	{
		enrolled:              true
		is_concurrent_session: false
		is_registered:         true
		role:                  "user"
		last_name:             "Service Forces"
		last_login_date:       "2020-12-31T18:40:37+00:00"
		tours: {
			new_group:         false
			new_root:          false
			new_risk_exposure: false
			welcome:           false
		}
		legal_remember:    true
		registration_date: "2018-02-28T16:54:12+00:00"
		sk:                "USER#integratesserviceforces@fluidattacks.com"
		pk_2:              "USER#all"
		pk:                "USER#integratesserviceforces@fluidattacks.com"
		first_name:        "Integrates"
		email:             "integratesserviceforces@fluidattacks.com"
		sk_2:              "USER#integratesserviceforces@fluidattacks.com"
	},
	{
		enrolled:              true
		is_concurrent_session: false
		is_registered:         true
		role:                  "hacker"
		last_name:             "Resourcer"
		last_login_date:       "2020-12-31T18:40:37+00:00"
		tours: {
			new_group:         false
			new_root:          false
			new_risk_exposure: false
			welcome:           false
		}
		legal_remember:    true
		registration_date: "2018-02-28T16:54:12+00:00"
		sk:                "USER#integratesresourcer@fluidattacks.com"
		pk_2:              "USER#all"
		pk:                "USER#integratesresourcer@fluidattacks.com"
		state: {
			modified_date: "2018-02-28T16:54:12+00:00"
			modified_by:   "integratesresourcer@fluidattacks.com"
			notifications_preferences: {
				sms: []
				email:
				[
					"ACCESS_GRANTED",
					"EVENT_REPORT",
					"GROUP_REPORT",
					"NEW_COMMENT",
					"NEW_DRAFT",
					"REMEDIATE_FINDING",
					"REMINDER_NOTIFICATION",
					"UPDATED_TREATMENT",
					"VULNERABILITY_ASSIGNED",
					"VULNERABILITY_REPORT",
				]

			}
		}
		first_name: "Integrates"
		email:      "integratesresourcer@fluidattacks.com"
		sk_2:       "USER#integratesresourcer@fluidattacks.com"
	},
	{
		enrolled:              true
		is_concurrent_session: false
		is_registered:         true
		role:                  "hacker"
		last_name:             "Manager"
		last_login_date:       "2020-12-31T18:40:37+00:00"
		tours: {
			new_group:         false
			new_root:          false
			new_risk_exposure: false
			welcome:           false
		}
		legal_remember:    true
		registration_date: "2018-02-28T16:54:12+00:00"
		sk:                "USER#unittest2@fluidattacks.com"
		pk_2:              "USER#all"
		pk:                "USER#unittest2@fluidattacks.com"
		state: {
			modified_date: "2018-02-28T16:54:12+00:00"
			modified_by:   "unittest2@fluidattacks.com"
			notifications_preferences: {
				sms: []
				email:
				[
					"ACCESS_GRANTED",
					"AGENT_TOKEN",
					"EVENT_REPORT",
					"FILE_UPDATE",
					"GROUP_INFORMATION",
					"GROUP_REPORT",
					"NEW_COMMENT",
					"NEW_DRAFT",
					"PORTFOLIO_UPDATE",
					"REMEDIATE_FINDING",
					"REMINDER_NOTIFICATION",
					"ROOT_UPDATE",
					"SERVICE_UPDATE",
					"UNSUBSCRIPTION_ALERT",
					"UPDATED_TREATMENT",
					"VULNERABILITY_ASSIGNED",
					"VULNERABILITY_REPORT",
				]

			}
		}
		first_name: "Group"
		email:      "unittest2@fluidattacks.com"
		sk_2:       "USER#unittest2@fluidattacks.com"
	},
	{
		enrolled:              true
		is_concurrent_session: false
		is_registered:         true
		role:                  "user"
		last_name:             "Internal Manager"
		last_login_date:       "2020-12-31T18:40:37+00:00"
		tours: {
			new_group:         false
			new_root:          false
			new_risk_exposure: false
			welcome:           false
		}
		legal_remember:    true
		registration_date: "2018-02-28T16:54:12+00:00"
		sk:                "USER#integratesuser2@fluidattacks.com"
		pk_2:              "USER#all"
		pk:                "USER#integratesuser2@fluidattacks.com"
		first_name:        "Integrates"
		email:             "integratesuser2@fluidattacks.com"
		sk_2:              "USER#integratesuser2@fluidattacks.com"
	},
	{
		enrolled:              true
		is_concurrent_session: false
		is_registered:         true
		role:                  "hacker"
		last_name:             "Hacker"
		last_login_date:       "2020-12-31T18:40:37+00:00"
		tours: {
			new_group:         false
			new_root:          false
			new_risk_exposure: false
			welcome:           false
		}
		legal_remember:    false
		registration_date: "2018-02-28T16:54:12+00:00"
		phone: {
			national_number:      "1234567895"
			country_code:         "CO"
			calling_country_code: "57"
		}
		sk:         "USER#integrateshacker@fluidattacks.com"
		pk_2:       "USER#all"
		pk:         "USER#integrateshacker@fluidattacks.com"
		first_name: "Integrates"
		email:      "integrateshacker@fluidattacks.com"
		sk_2:       "USER#integrateshacker@fluidattacks.com"
		login: {
			country_code:    "PA"
			provider:        "GOOGLE"
			subject:         "GOOGLE#2222223333333344445555"
			expiration_time: 2026492340
			browser:         "Chrome 114.0.0.0"
			modified_by:     "integrates@fluidattacks.com"
			ip_address:      "127.0.0.1"
			modified_date:   "2024-03-22T18:32:20.800014+00:00"
			device:          "Linux"
		}
	},
	{
		enrolled:              true
		is_concurrent_session: false
		is_registered:         true
		role:                  "user"
		last_name:             "Hacking"
		last_login_date:       "2020-12-31T15:45:37+00:00"
		tours: {
			new_group:         true
			new_root:          true
			new_risk_exposure: true
			welcome:           true
		}
		legal_remember:    true
		registration_date: "2018-02-28T16:54:12+00:00"
		phone: {
			national_number:      "1234567892"
			country_code:         "CO"
			calling_country_code: "57"
		}
		sk:   "USER#continuoushack2@gmail.com"
		pk_2: "USER#all"
		pk:   "USER#continuoushack2@gmail.com"
		state: {
			modified_date: "2018-02-28T16:54:12+00:00"
			modified_by:   "continuoushack2@gmail.com"
			notifications_preferences: {
				sms: []
				email:
				[
					"ACCESS_GRANTED",
					"AGENT_TOKEN",
					"EVENT_REPORT",
					"FILE_UPDATE",
					"GROUP_INFORMATION",
					"GROUP_REPORT",
					"NEW_COMMENT",
					"NEW_DRAFT",
					"PORTFOLIO_UPDATE",
					"REMEDIATE_FINDING",
					"REMINDER_NOTIFICATION",
					"ROOT_UPDATE",
					"SERVICE_UPDATE",
					"UNSUBSCRIPTION_ALERT",
					"UPDATED_TREATMENT",
					"VULNERABILITY_ASSIGNED",
					"VULNERABILITY_REPORT",
				]

			}
			trusted_devices:
			[
				{
					browser:          "firefox"
					device:           "ubuntu"
					first_login_date: "2024-02-14T10:55:12+00:00"
					ip_address:       "192.168.1.1"
					last_attempt:     "2024-02-14T10:55:12+00:00"
					last_login_date:  "2024-02-14T10:55:12+00:00"
					location:         "Toronto"
					otp_token_jti:    "abc961"
				},
			]

		}
		first_name: "Continuous2"
		email:      "continuoushack2@gmail.com"
		sk_2:       "USER#continuoushack2@gmail.com"
	},
	{
		enrolled:              true
		is_concurrent_session: false
		is_registered:         false
		last_name:             ""
		tours: {
			new_group:         false
			new_root:          false
			new_risk_exposure: false
			welcome:           false
		}
		legal_remember:    true
		registration_date: "2018-11-28T23:24:10+00:00"
		sk:                "USER#unittest2"
		pk_2:              "USER#all"
		pk:                "USER#unittest2"
		first_name:        ""
		email:             "unittest2"
		sk_2:              "USER#unittest2"
	},
	{
		enrolled:              true
		is_concurrent_session: false
		is_registered:         true
		role:                  "admin"
		last_name:             ""
		tours: {
			new_group:         false
			new_root:          false
			new_risk_exposure: false
			welcome:           false
		}
		legal_remember:    true
		registration_date: "2018-02-28T16:54:12+00:00"
		sk:                "USER#unittest"
		pk_2:              "USER#all"
		pk:                "USER#unittest"
		first_name:        ""
		email:             "unittest"
		sk_2:              "USER#unittest"
	},
	{
		enrolled:              true
		is_concurrent_session: false
		is_registered:         true
		last_name:             "Internal Manager"
		last_login_date:       "2020-12-31T18:40:37+00:00"
		tours: {
			new_group:         false
			new_root:          false
			new_risk_exposure: false
			welcome:           false
		}
		legal_remember:    true
		registration_date: "2018-02-28T16:54:12+00:00"
		sk:                "USER#org_testuser3@gmail.com"
		pk_2:              "USER#all"
		pk:                "USER#org_testuser3@gmail.com"
		first_name:        "Integrates"
		email:             "org_testuser3@gmail.com"
		sk_2:              "USER#org_testuser3@gmail.com"
	},
	{
		enrolled:              true
		is_concurrent_session: false
		is_registered:         true
		role:                  "admin"
		last_name:             "de Orellana"
		last_login_date:       "2020-12-31T18:40:37+00:00"
		tours: {
			new_group:         false
			new_root:          false
			new_risk_exposure: false
			welcome:           false
		}
		access_tokens:
		[
			{
				id:         "21c58d45-a8f3-4a62-8272-adbc56ef3d8b"
				issued_at:  1695934432
				jti_hashed: "321906c10d3a78d9efecd84542488c16a5e4582b81d2858afb0d0fd4ec8dc188"
				salt:       "676881df877eda4cf4d5c08ec445aa1fee200329071c5a4d81febf173d82ad1e"
			},
		]

		legal_remember:    true
		registration_date: "2019-02-28T16:54:12+00:00"
		sk:                "USER#unittest@fluidattacks.com"
		pk_2:              "USER#all"
		pk:                "USER#unittest@fluidattacks.com"
		first_name:        "Miguel"
		email:             "unittest@fluidattacks.com"
		sk_2:              "USER#unittest@fluidattacks.com"
	},
	{
		enrolled:              true
		is_concurrent_session: false
		is_registered:         true
		last_name:             "To Be Deleted"
		last_login_date:       "2020-10-01T18:40:37+00:00"
		tours: {
			new_group:         false
			new_root:          false
			new_risk_exposure: false
			welcome:           false
		}
		legal_remember:    true
		registration_date: "2018-02-28T16:54:12+00:00"
		sk:                "USER#inactive_imamura1@fluidattacks.com"
		pk_2:              "USER#all"
		pk:                "USER#inactive_imamura1@fluidattacks.com"
		first_name:        "Imamura"
		email:             "inactive_imamura1@fluidattacks.com"
		sk_2:              "USER#inactive_imamura1@fluidattacks.com"
	},
	{
		enrolled:              true
		is_concurrent_session: false
		is_registered:         true
		last_name:             "To Be Deleted"
		last_login_date:       "2020-10-02T18:40:37+00:00"
		tours: {
			new_group:         false
			new_root:          false
			new_risk_exposure: false
			welcome:           false
		}
		legal_remember:    true
		registration_date: "2018-02-28T16:54:12+00:00"
		sk:                "USER#inactive_imamura2@fluidattacks.com"
		pk_2:              "USER#all"
		pk:                "USER#inactive_imamura2@fluidattacks.com"
		first_name:        "Imamura"
		email:             "inactive_imamura2@fluidattacks.com"
		sk_2:              "USER#inactive_imamura2@fluidattacks.com"
	},
	{
		enrolled:              true
		is_concurrent_session: false
		is_registered:         true
		last_name:             "Not To Be Deleted"
		last_login_date:       "2020-10-03T18:40:37+00:00"
		tours: {
			new_group:         false
			new_root:          false
			new_risk_exposure: false
			welcome:           false
		}
		legal_remember:    true
		registration_date: "2018-02-28T16:54:12+00:00"
		sk:                "USER#active_imamura3@fluidattacks.com"
		pk_2:              "USER#all"
		pk:                "USER#active_imamura3@fluidattacks.com"
		first_name:        "Imamura"
		email:             "active_imamura3@fluidattacks.com"
		sk_2:              "USER#active_imamura3@fluidattacks.com"
	},
	{
		pk:                "USER#jdoe@testcompany.com"
		sk:                "USER#jdoe@testcompany.com"
		registration_date: "2022-02-09T16:54:12+00:00"
		last_login_date:   "2022-02-09T16:54:12+00:00"
		role:              "user"
		pk_2:              "USER#all"
		sk_2:              "USER#jdoe@testcompany.com"
		state: {
			modified_date: "2022-02-09T16:54:12+00:00"
			modified_by:   "jdoe@testcompany.com"
			notifications_preferences: {
				sms: []
				email:
				[
					"ACCESS_GRANTED",
					"AGENT_TOKEN",
					"EVENT_REPORT",
					"FILE_UPDATE",
					"GROUP_INFORMATION",
					"GROUP_REPORT",
					"NEW_COMMENT",
					"NEW_DRAFT",
					"PORTFOLIO_UPDATE",
					"REMEDIATE_FINDING",
					"REMINDER_NOTIFICATION",
					"ROOT_UPDATE",
					"SERVICE_UPDATE",
					"UNSUBSCRIPTION_ALERT",
					"UPDATED_TREATMENT",
					"VULNERABILITY_ASSIGNED",
					"VULNERABILITY_REPORT",
				]

			}
		}
		enrolled:      false
		first_name:    "John"
		last_name:     "Doe"
		is_registered: true
	},
	{
		email:             "cypress@cypress.com"
		enrolled:          false
		first_name:        "Cypress"
		is_registered:     true
		last_login_date:   "2023-10-05T00:00:00+00:00"
		last_name:         "Cypress"
		pk:                "USER#cypress@cypress.com"
		pk_2:              "USER#all"
		registration_date: "2023-10-05T00:00:00+00:00"
		role:              "user"
		sk:                "USER#cypress@cypress.com"
		sk_2:              "USER#cypress@cypress.com"
		state: {
			modified_date: "2023-10-05T00:00:00+00:00"
			modified_by:   "cypress@cypress.com"
			notifications_preferences: {
				sms: []
				email:
				[
					"ACCESS_GRANTED",
					"AGENT_TOKEN",
					"EVENT_REPORT",
					"FILE_UPDATE",
					"GROUP_INFORMATION",
					"GROUP_REPORT",
					"NEW_COMMENT",
					"NEW_DRAFT",
					"PORTFOLIO_UPDATE",
					"REMEDIATE_FINDING",
					"REMINDER_NOTIFICATION",
					"ROOT_UPDATE",
					"SERVICE_UPDATE",
					"UNSUBSCRIPTION_ALERT",
					"UPDATED_TREATMENT",
					"VULNERABILITY_ASSIGNED",
					"VULNERABILITY_REPORT",
				]

			}
		}
	},
	{
		enrolled:              true
		is_concurrent_session: false
		is_registered:         true
		role:                  "user"
		last_name:             "Integrates"
		last_login_date:       "2020-12-31T18:40:37+00:00"
		session_token: {
			jti:   "91bedb2d4319ac7fa31b03f9d509e2a77033d35d17e385d0e5ad05aaebc83e90"
			state: "IS_VALID"
		}
		tours: {
			new_group:         false
			new_root:          false
			new_risk_exposure: false
			welcome:           false
		}
		legal_remember:    true
		registration_date: "2018-02-28T16:54:12+00:00"
		sk:                "USER#org-resourcer@integrates.com"
		pk_2:              "USER#all"
		pk:                "USER#org-resourcer@integrates.com"
		access_tokens:
		[
			{
				id:         "275b400a-802b-44e0-9cc7-59c957011cc2"
				issued_at:  1718311748
				jti_hashed: "978dd880e20aeae8e1896141f8504c7a5085128c7199acef860733480f52d313"
				salt:       "946a03acdaf97307877ec84ef9277d50f6d8e39f44db39731462fca3a4b83559"
			},
		]

		state: {
			modified_date: "2018-02-28T16:54:12+00:00"
			modified_by:   "org-resourcer@integrates.com"
			notifications_preferences: {
				sms: []
				email:
				[
					"ACCESS_GRANTED",
					"AGENT_TOKEN",
					"EVENT_REPORT",
					"FILE_UPDATE",
					"GROUP_INFORMATION",
					"GROUP_REPORT",
					"NEW_COMMENT",
					"NEW_DRAFT",
					"PORTFOLIO_UPDATE",
					"REMEDIATE_FINDING",
					"REMINDER_NOTIFICATION",
					"ROOT_UPDATE",
					"SERVICE_UPDATE",
					"UNSUBSCRIPTION_ALERT",
					"UPDATED_TREATMENT",
					"VULNERABILITY_ASSIGNED",
					"VULNERABILITY_REPORT",
				]

			}
		}
		login: {
			country_code:    "CO"
			provider:        "BITBUCKET"
			subject:         "BITBUCKET#2222223333333344445555"
			expiration_time: 2026492340
			browser:         "Chrome 114.0.0.0"
			modified_by:     "integrates@fluidattacks.com"
			ip_address:      "127.0.0.1"
			modified_date:   "2024-03-22T18:32:20.800014+00:00"
			device:          "Linux"
		}
		first_name: "org resourcer"
		email:      "org-resourcer@integrates.com"
		sk_2:       "USER#org-resourcer@integrates.com"
	},
] & [...#StakeholderItem]
