package organization_unreliable_integration_repository

import (
	"fluidattacks.com/types/integration_repositories"
)

#organizationUnreliableIntegrationRepositoryPk: =~"^ORG#[a-f0-9-]{36}$"
#organizationUnreliableIntegrationRepositorySk: =~"^URL#[a-z0-9]+#BRANCH#[a-z0-9]+#CRED#[a-f0-9-]{36}$"

#OrganizationUnreliableIntegrationRepositoryKeys: {
	pk:   string & #organizationUnreliableIntegrationRepositoryPk
	sk:   string & #organizationUnreliableIntegrationRepositorySk
	pk_2: string
	sk_2: string
}

#OrganizationUnreliableIntegrationRepositoryItem: {
	#OrganizationUnreliableIntegrationRepositoryKeys
	integration_repositories.#OrganizationUnreliableIntegrationRepository
}

OrganizationUnreliableIntegrationRepository:
{
	FacetName: "organization_unreliable_integration_repository"
	KeyAttributeAlias: {
		PartitionKeyAlias: "ORG#id"
		SortKeyAlias:      "URL#hash#BRANCH#branch#CRED#credential_id"
	}
	NonKeyAttributes: [
		"branches",
		"name",
		"branch",
		"url",
		"pk_2",
		"sk_2",
		"credential_id",
		"last_commit_date",
		"commit_count",
	]
	DataAccess: {
		MySql: {}
	}
}

OrganizationUnreliableIntegrationRepository: TableData: [
	{
		pk:     "ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3"
		sk:     "URL#00fbe3579a253b43239954a545dc0536e6c83098#BRANCH#main#CRED#dd2f08da-be59-4f97-8117-00f1cbbf6aec"
		pk_2:   "CRED#dd2f08da-be59-4f97-8117-00f1cbbf6aec"
		sk_2:   "URL#00fbe3579a253b43239954a545dc0536e6c83098"
		name:   "Example integration repository"
		branch: "main"
		branches: []
		commit_count:     3
		last_commit_date: "2024-04-17T22:21:45.961408+00:00"
		url:              "https://gitlab.com/fluidattacks/integration-repo"
		credential_id:    "dd2f08da-be59-4f97-8117-00f1cbbf6aec"
	},
] & [...#OrganizationUnreliableIntegrationRepositoryItem]
