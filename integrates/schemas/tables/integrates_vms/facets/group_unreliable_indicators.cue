package group_unreliable_indicators

import (
	"fluidattacks.com/types/groups"
)

#groupUnreliableIndicatorsPk: =~"^GROUP#[a-z]+"
#groupUnreliableIndicatorsSk: =~"^GROUP#[a-z]+#UNRELIABLEINDICATORS"

#GroupUnreliableIndicatorsKeys: {
	pk: string & #groupUnreliableIndicatorsPk
	sk: string & #groupUnreliableIndicatorsSk
}

#GroupUnreliableIndicatorsItem: {
	#GroupUnreliableIndicatorsKeys
	groups.#GroupUnreliableIndicators
}

GroupUnreliableIndicators:
{
	FacetName: "group_unreliable_indicators"
	KeyAttributeAlias: {
		PartitionKeyAlias: "GROUP#name"
		SortKeyAlias:      "GROUP#name#UNRELIABLEINDICATORS"
	}
	NonKeyAttributes: [
		"closed_vulnerabilities",
		"exposed_over_time_cvssf",
		"exposed_over_time_month_cvssf",
		"exposed_over_time_year_cvssf",
		"last_closed_vulnerability_days",
		"last_closed_vulnerability_finding",
		"max_open_severity",
		"max_open_severity_finding",
		"max_severity",
		"mean_remediate",
		"mean_remediate_critical_severity",
		"mean_remediate_high_severity",
		"mean_remediate_low_severity",
		"mean_remediate_medium_severity",
		"open_findings",
		"open_vulnerabilities",
		"remediated_over_time",
		"remediated_over_time_30",
		"remediated_over_time_90",
		"remediated_over_time_cvssf",
		"remediated_over_time_cvssf_30",
		"remediated_over_time_cvssf_90",
		"remediated_over_time_month",
		"remediated_over_time_month_cvssf",
		"remediated_over_time_year",
		"remediated_over_time_year_cvssf",
		"treatment_summary",
		"code_languages",
		"nofluid_quantity",
		"unfulfilled_standards",
	]
	DataAccess: {
		MySql: {}
	}
}

GroupUnreliableIndicators: TableData: [
	{
		open_vulnerabilities:              0
		max_open_severity:                 5.7
		closed_vulnerabilities:            0
		mean_remediate_high_severity:      0
		last_closed_vulnerability_finding: ""
		mean_remediate:                    245
		treatment_summary: {
			accepted:           1
			untreated:          0
			in_progress:        1
			accepted_undefined: 0
		}
		mean_remediate_critical_severity: 0
		last_closed_vulnerability_days:   90
		sk:                               "GROUP#asgard#UNRELIABLEINDICATORS"
		max_open_severity_finding:        ""
		open_findings:                    0
		pk:                               "GROUP#asgard"
		remediated_over_time: []
		mean_remediate_low_severity:    0
		mean_remediate_medium_severity: 0
	},
	{
		open_vulnerabilities:              0
		max_open_severity:                 6.6
		closed_vulnerabilities:            0
		mean_remediate_high_severity:      0
		last_closed_vulnerability_finding: ""
		mean_remediate:                    135
		treatment_summary: {
			accepted:           0
			untreated:          2
			in_progress:        1
			accepted_undefined: 0
		}
		mean_remediate_critical_severity: 0
		last_closed_vulnerability_days:   80
		sk:                               "GROUP#barranquilla#UNRELIABLEINDICATORS"
		max_open_severity_finding:        ""
		open_findings:                    0
		pk:                               "GROUP#barranquilla"
		remediated_over_time: []
		mean_remediate_low_severity:    0
		mean_remediate_medium_severity: 0
	},
	{
		open_vulnerabilities:              0
		max_open_severity:                 4.9
		closed_vulnerabilities:            0
		mean_remediate_high_severity:      0
		last_closed_vulnerability_finding: ""
		mean_remediate:                    245
		treatment_summary: {
			accepted:           5
			untreated:          65
			in_progress:        58
			accepted_undefined: 0
		}
		mean_remediate_critical_severity: 0
		last_closed_vulnerability_days:   23
		sk:                               "GROUP#continuoustesting#UNRELIABLEINDICATORS"
		max_open_severity_finding:        ""
		open_findings:                    0
		pk:                               "GROUP#continuoustesting"
		mean_remediate_low_severity:      0
		mean_remediate_medium_severity:   0
	},
	{
		open_vulnerabilities:              0
		max_open_severity:                 4.9
		closed_vulnerabilities:            0
		mean_remediate_high_severity:      0
		last_closed_vulnerability_finding: ""
		mean_remediate:                    100
		treatment_summary: {
			accepted:           2
			untreated:          0
			in_progress:        1
			accepted_undefined: 0
		}
		mean_remediate_critical_severity: 0
		last_closed_vulnerability_days:   12
		sk:                               "GROUP#deletegroup#UNRELIABLEINDICATORS"
		max_open_severity_finding:        ""
		open_findings:                    0
		pk:                               "GROUP#deletegroup"
		remediated_over_time: []
		mean_remediate_low_severity:    0
		mean_remediate_medium_severity: 0
	},
	{
		open_vulnerabilities:              0
		max_open_severity:                 4.9
		closed_vulnerabilities:            0
		mean_remediate_high_severity:      0
		last_closed_vulnerability_finding: ""
		mean_remediate:                    100
		treatment_summary: {
			accepted:           2
			untreated:          0
			in_progress:        1
			accepted_undefined: 0
		}
		mean_remediate_critical_severity: 0
		last_closed_vulnerability_days:   12
		sk:                               "GROUP#deleteimamura#UNRELIABLEINDICATORS"
		max_open_severity_finding:        ""
		open_findings:                    0
		pk:                               "GROUP#deleteimamura"
		remediated_over_time: []
		mean_remediate_low_severity:    0
		mean_remediate_medium_severity: 0
	},
	{
		open_vulnerabilities:              0
		max_open_severity:                 2.9
		closed_vulnerabilities:            0
		mean_remediate_high_severity:      0
		last_closed_vulnerability_finding: ""
		mean_remediate:                    245
		treatment_summary: {
			accepted:           2
			untreated:          0
			in_progress:        0
			accepted_undefined: 0
		}
		mean_remediate_critical_severity: 0
		last_closed_vulnerability_days:   45
		sk:                               "GROUP#gotham#UNRELIABLEINDICATORS"
		max_open_severity_finding:        ""
		open_findings:                    0
		pk:                               "GROUP#gotham"
		remediated_over_time: []
		mean_remediate_low_severity:    0
		mean_remediate_medium_severity: 0
	},
	{
		open_vulnerabilities:              0
		max_open_severity:                 4.9
		closed_vulnerabilities:            0
		mean_remediate_high_severity:      0
		last_closed_vulnerability_finding: ""
		mean_remediate:                    100
		treatment_summary: {
			accepted:           0
			untreated:          0
			in_progress:        0
			accepted_undefined: 0
		}
		mean_remediate_critical_severity: 0
		last_closed_vulnerability_days:   0
		sk:                               "GROUP#kurome#UNRELIABLEINDICATORS"
		max_open_severity_finding:        ""
		nofluid_quantity:                 0
		open_findings:                    0
		pk:                               "GROUP#kurome"
		remediated_over_time: []
		mean_remediate_low_severity: 0
		code_languages:
		[
			{
				loc:      15
				language: "Python"
			},
		]

		mean_remediate_medium_severity: 0
	},
	{
		open_vulnerabilities:              3
		max_open_severity:                 4.9
		closed_vulnerabilities:            0
		mean_remediate_high_severity:      0
		last_closed_vulnerability_finding: ""
		mean_remediate:                    245
		treatment_summary: {
			accepted:           1
			untreated:          1
			in_progress:        0
			accepted_undefined: 1
		}
		mean_remediate_critical_severity: 0
		last_closed_vulnerability_days:   0
		sk:                               "GROUP#lubbock#UNRELIABLEINDICATORS"
		max_open_severity_finding:        ""
		open_findings:                    1
		pk:                               "GROUP#lubbock"
		remediated_over_time: []
		mean_remediate_low_severity:    0
		mean_remediate_medium_severity: 0
	},
	{
		open_vulnerabilities:              0
		max_open_severity:                 4.9
		closed_vulnerabilities:            0
		mean_remediate_high_severity:      0
		last_closed_vulnerability_finding: ""
		mean_remediate:                    245
		treatment_summary: {
			accepted:           2
			untreated:          1
			in_progress:        1
			accepted_undefined: 0
		}
		mean_remediate_critical_severity: 0
		last_closed_vulnerability_days:   23
		sk:                               "GROUP#metropolis#UNRELIABLEINDICATORS"
		max_open_severity_finding:        ""
		open_findings:                    0
		pk:                               "GROUP#metropolis"
		remediated_over_time: []
		mean_remediate_low_severity:    0
		mean_remediate_medium_severity: 0
	},
	{
		open_vulnerabilities:              0
		max_open_severity:                 4.9
		closed_vulnerabilities:            0
		mean_remediate_high_severity:      0
		last_closed_vulnerability_finding: ""
		mean_remediate:                    100
		treatment_summary: {
			accepted:           2
			untreated:          0
			in_progress:        1
			accepted_undefined: 0
		}
		mean_remediate_critical_severity: 0
		last_closed_vulnerability_days:   12
		sk:                               "GROUP#monteria#UNRELIABLEINDICATORS"
		max_open_severity_finding:        ""
		open_findings:                    0
		pk:                               "GROUP#monteria"
		remediated_over_time: []
		mean_remediate_low_severity:    0
		mean_remediate_medium_severity: 0
	},
	{
		open_vulnerabilities:         1
		max_open_severity:            0
		closed_vulnerabilities:       0
		mean_remediate_high_severity: 0
		unfulfilled_standards:
		[
			{
				name: "capec"
				unfulfilled_requirements:
				[
					"077",
				]

			},
			{
				name: "cwe"
				unfulfilled_requirements:
				[
					"077",
					"176",
				]

			},
			{
				name: "agile"
				unfulfilled_requirements:
				[
					"077",
				]

			},
			{
				name: "padss"
				unfulfilled_requirements:
				[
					"077",
					"176",
				]

			},
			{
				name: "cmmc"
				unfulfilled_requirements:
				[
					"077",
					"176",
				]

			},
			{
				name: "hitrust"
				unfulfilled_requirements:
				[
					"077",
					"176",
				]

			},
			{
				name: "iso27002"
				unfulfilled_requirements:
				[
					"077",
					"176",
				]

			},
			{
				name: "wassec"
				unfulfilled_requirements:
				[
					"077",
					"176",
				]

			},
			{
				name: "issaf"
				unfulfilled_requirements:
				[
					"077",
					"176",
				]

			},
			{
				name: "ptes"
				unfulfilled_requirements:
				[
					"077",
					"176",
				]

			},
			{
				name: "owaspscp"
				unfulfilled_requirements:
				[
					"077",
					"176",
				]

			},
			{
				name: "bsafss"
				unfulfilled_requirements:
				[
					"077",
					"176",
				]

			},
			{
				name: "asvs"
				unfulfilled_requirements:
				[
					"077",
					"176",
				]

			},
			{
				name: "pci"
				unfulfilled_requirements:
				[
					"077",
					"176",
				]

			},
			{
				name: "cis"
				unfulfilled_requirements:
				[
					"176",
				]

			},
			{
				name: "eprivacy"
				unfulfilled_requirements:
				[
					"176",
				]

			},
			{
				name: "gdpr"
				unfulfilled_requirements:
				[
					"176",
				]

			},
			{
				name: "nerccip"
				unfulfilled_requirements:
				[
					"176",
				]

			},
			{
				name: "owasp10"
				unfulfilled_requirements:
				[
					"176",
				]

			},
			{
				name: "nist"
				unfulfilled_requirements:
				[
					"176",
				]

			},
			{
				name: "cpra"
				unfulfilled_requirements:
				[
					"176",
				]

			},
			{
				name: "glba"
				unfulfilled_requirements:
				[
					"176",
				]

			},
			{
				name: "nyshield"
				unfulfilled_requirements:
				[
					"176",
				]

			},
			{
				name: "mitre"
				unfulfilled_requirements:
				[
					"176",
				]

			},
			{
				name: "pdpa"
				unfulfilled_requirements:
				[
					"176",
				]

			},
			{
				name: "popia"
				unfulfilled_requirements:
				[
					"176",
				]

			},
			{
				name: "pdpo"
				unfulfilled_requirements:
				[
					"176",
				]

			},
			{
				name: "fedramp"
				unfulfilled_requirements:
				[
					"176",
				]

			},
			{
				name: "iec62443"
				unfulfilled_requirements:
				[
					"176",
				]

			},
			{
				name: "osstmm3"
				unfulfilled_requirements:
				[
					"176",
				]

			},
			{
				name: "wasc"
				unfulfilled_requirements:
				[
					"176",
				]

			},
			{
				name: "owasprisks"
				unfulfilled_requirements:
				[
					"176",
				]

			},
			{
				name: "c2m2"
				unfulfilled_requirements:
				[
					"176",
				]

			},
		]

		last_closed_vulnerability_finding: "457497318"
		mean_remediate:                    103
		treatment_summary: {
			accepted:           0
			untreated:          0
			in_progress:        0
			accepted_undefined: 0
		}
		mean_remediate_critical_severity: 0
		last_closed_vulnerability_days:   45
		remediated_over_time_cvssf:
		[
			[
				{
					x: "Sep 7 - 13, 2020"
					y: 1
				},
			],

			[
				{
					x: "Sep 7 - 13, 2020"
					y: 0
				},
			],
			[
				{
					x: "Sep 7 - 13, 2020"
					y: 0
				},
			],
			[
				{
					x: "Sep 7 - 13, 2020"
					y: 0
				},
			],
			[
				{
					x: "Sep 7 - 13, 2020"
					y: 1
				},
			],

		]

		sk:                        "GROUP#oneshottest#UNRELIABLEINDICATORS"
		max_open_severity_finding: ""
		open_findings:             0
		pk:                        "GROUP#oneshottest"
		remediated_over_time:
		[

			[
				{
					x: "Sep 7 - 13, 2020"
					y: 1
				},
			],

			[
				{
					x: "Sep 7 - 13, 2020"
					y: 0
				},
			],

			[
				{
					x: "Sep 7 - 13, 2020"
					y: 0
				},
			],

			[
				{
					x: "Sep 7 - 13, 2020"
					y: 0
				},
			],

			[
				{
					x: "Sep 7 - 13, 2020"
					y: 1
				},
			],

		]

		mean_remediate_low_severity:    0
		mean_remediate_medium_severity: 0
	},
	{
		open_vulnerabilities:              0
		max_open_severity:                 4.9
		closed_vulnerabilities:            0
		mean_remediate_high_severity:      0
		last_closed_vulnerability_finding: ""
		mean_remediate:                    100
		treatment_summary: {
			accepted:           2
			untreated:          0
			in_progress:        1
			accepted_undefined: 0
		}
		mean_remediate_critical_severity: 0
		last_closed_vulnerability_days:   12
		sk:                               "GROUP#setpendingdeletion#UNRELIABLEINDICATORS"
		max_open_severity_finding:        ""
		open_findings:                    0
		pk:                               "GROUP#setpendingdeletion"
		remediated_over_time: []
		mean_remediate_low_severity:    0
		mean_remediate_medium_severity: 0
	},
	{
		open_vulnerabilities:              0
		max_open_severity:                 4.9
		closed_vulnerabilities:            0
		mean_remediate_high_severity:      0
		last_closed_vulnerability_finding: ""
		mean_remediate:                    100
		treatment_summary: {
			accepted:           0
			untreated:          0
			in_progress:        0
			accepted_undefined: 0
		}
		mean_remediate_critical_severity: 0
		last_closed_vulnerability_days:   0
		sk:                               "GROUP#sheele#UNRELIABLEINDICATORS"
		max_open_severity_finding:        ""
		open_findings:                    0
		pk:                               "GROUP#sheele"
		remediated_over_time: []
		mean_remediate_low_severity:    0
		mean_remediate_medium_severity: 0
	},
	{
		open_vulnerabilities:         31
		max_open_severity:            6.3
		closed_vulnerabilities:       8
		mean_remediate_high_severity: 0
		unfulfilled_standards:
		[
			{
				name: "capec"
				unfulfilled_requirements:
				[
					"029",
					"173",
					"174",
					"261",
					"266",
				]

			},
			{
				name: "cis"
				unfulfilled_requirements:
				[
					"173",
					"176",
					"266",
				]

			},
			{
				name: "cwe"
				unfulfilled_requirements:
				[
					"029",
					"173",
					"174",
					"176",
					"177",
					"261",
					"266",
					"300",
				]

			},
			{
				name: "owasp10"
				unfulfilled_requirements:
				[
					"029",
					"173",
					"176",
					"177",
					"261",
					"266",
					"300",
				]

			},
			{
				name: "owaspm10"
				unfulfilled_requirements:
				[
					"173",
					"266",
				]

			},
			{
				name: "nist"
				unfulfilled_requirements:
				[
					"176",
					"266",
				]

			},
			{
				name: "agile"
				unfulfilled_requirements:
				[
					"173",
					"266",
				]

			},
			{
				name: "nydfs"
				unfulfilled_requirements:
				[
					"266",
				]

			},
			{
				name: "mitre"
				unfulfilled_requirements:
				[
					"029",
					"173",
					"176",
					"266",
				]

			},
			{
				name: "padss"
				unfulfilled_requirements:
				[
					"029",
					"173",
					"174",
					"176",
					"261",
					"266",
					"300",
				]

			},
			{
				name: "cmmc"
				unfulfilled_requirements:
				[
					"174",
					"176",
					"261",
					"266",
				]

			},
			{
				name: "hitrust"
				unfulfilled_requirements:
				[
					"173",
					"174",
					"176",
					"177",
					"261",
					"266",
					"300",
				]

			},
			{
				name: "iso27002"
				unfulfilled_requirements:
				[
					"029",
					"173",
					"176",
					"261",
					"266",
					"300",
				]

			},
			{
				name: "iec62443"
				unfulfilled_requirements:
				[
					"029",
					"173",
					"176",
					"266",
				]

			},
			{
				name: "wassec"
				unfulfilled_requirements:
				[
					"029",
					"173",
					"174",
					"176",
					"261",
					"266",
					"300",
				]

			},
			{
				name: "osstmm3"
				unfulfilled_requirements:
				[
					"176",
					"177",
					"266",
					"300",
				]

			},
			{
				name: "wasc"
				unfulfilled_requirements:
				[
					"029",
					"173",
					"174",
					"176",
					"177",
					"261",
					"266",
					"300",
				]

			},
			{
				name: "nistssdf"
				unfulfilled_requirements:
				[
					"177",
					"261",
					"266",
					"300",
				]

			},
			{
				name: "issaf"
				unfulfilled_requirements:
				[
					"029",
					"173",
					"176",
					"261",
					"266",
				]

			},
			{
				name: "ptes"
				unfulfilled_requirements:
				[
					"029",
					"173",
					"176",
					"177",
					"266",
					"300",
				]

			},
			{
				name: "owasprisks"
				unfulfilled_requirements:
				[
					"029",
					"173",
					"174",
					"176",
					"177",
					"261",
					"266",
					"300",
				]

			},
			{
				name: "mvsp"
				unfulfilled_requirements:
				[
					"029",
					"173",
					"174",
					"266",
				]

			},
			{
				name: "owaspscp"
				unfulfilled_requirements:
				[
					"029",
					"173",
					"176",
					"177",
					"266",
				]

			},
			{
				name: "bsafss"
				unfulfilled_requirements:
				[
					"029",
					"173",
					"176",
					"266",
				]

			},
			{
				name: "owaspmasvs"
				unfulfilled_requirements:
				[
					"173",
					"266",
				]

			},
			{
				name: "nist800171"
				unfulfilled_requirements:
				[
					"266",
				]

			},
			{
				name: "cwe25"
				unfulfilled_requirements:
				[
					"029",
					"173",
					"174",
					"266",
				]

			},
			{
				name: "nist800115"
				unfulfilled_requirements:
				[
					"266",
				]

			},
			{
				name: "swiftcsc"
				unfulfilled_requirements:
				[
					"174",
					"266",
				]

			},
			{
				name: "osamm"
				unfulfilled_requirements:
				[
					"266",
				]

			},
			{
				name: "asvs"
				unfulfilled_requirements:
				[
					"029",
					"173",
					"176",
					"177",
					"261",
					"266",
				]

			},
			{
				name: "c2m2"
				unfulfilled_requirements:
				[
					"173",
					"176",
					"266",
				]

			},
			{
				name: "pci"
				unfulfilled_requirements:
				[
					"029",
					"173",
					"174",
					"176",
					"261",
					"266",
					"300",
				]

			},
			{
				name: "siglite"
				unfulfilled_requirements:
				[
					"029",
					"173",
					"266",
				]

			},
			{
				name: "sig"
				unfulfilled_requirements:
				[
					"029",
					"173",
					"266",
					"300",
				]

			},
			{
				name: "eprivacy"
				unfulfilled_requirements:
				[
					"176",
					"177",
					"261",
					"300",
				]

			},
			{
				name: "gdpr"
				unfulfilled_requirements:
				[
					"176",
					"177",
					"261",
					"300",
				]

			},
			{
				name: "nerccip"
				unfulfilled_requirements:
				[
					"176",
				]

			},
			{
				name: "cpra"
				unfulfilled_requirements:
				[
					"176",
					"300",
				]

			},
			{
				name: "glba"
				unfulfilled_requirements:
				[
					"176",
				]

			},
			{
				name: "nyshield"
				unfulfilled_requirements:
				[
					"176",
					"177",
					"300",
				]

			},
			{
				name: "pdpa"
				unfulfilled_requirements:
				[
					"176",
					"261",
				]

			},
			{
				name: "popia"
				unfulfilled_requirements:
				[
					"176",
				]

			},
			{
				name: "pdpo"
				unfulfilled_requirements:
				[
					"176",
					"300",
				]

			},
			{
				name: "fedramp"
				unfulfilled_requirements:
				[
					"173",
					"176",
					"261",
				]

			},
			{
				name: "iso27001"
				unfulfilled_requirements:
				[
					"177",
					"261",
					"300",
				]

			},
			{
				name: "certj"
				unfulfilled_requirements:
				[
					"173",
					"177",
				]

			},
			{
				name: "sans25"
				unfulfilled_requirements:
				[
					"029",
					"173",
					"174",
					"261",
				]

			},
			{
				name: "lgpd"
				unfulfilled_requirements:
				[
					"261",
				]

			},
			{
				name: "ferpa"
				unfulfilled_requirements:
				[
					"261",
				]

			},
			{
				name: "soc2"
				unfulfilled_requirements:
				[
					"300",
				]

			},
			{
				name: "bizec"
				unfulfilled_requirements:
				[
					"173",
				]

			},
			{
				name: "certc"
				unfulfilled_requirements:
				[
					"173",
				]

			},
			{
				name: "nist80063"
				unfulfilled_requirements:
				[
					"029",
				]

			},
		]

		last_closed_vulnerability_finding: "457497316"
		mean_remediate:                    245
		treatment_summary: {
			accepted:           1
			untreated:          25
			in_progress:        4
			accepted_undefined: 2
		}
		mean_remediate_critical_severity: 0
		last_closed_vulnerability_days:   23
		sk:                               "GROUP#unittesting#UNRELIABLEINDICATORS"
		max_open_severity_finding:        "988493279"
		open_findings:                    5
		pk:                               "GROUP#unittesting"
		remediated_over_time:
		[

			[
				{
					x: "Nov 26 - Dec 2, 2018"
					y: 1
				},
				{
					x: "Jan 14 - 20, 2019"
					y: 3
				},
				{
					x: "Apr 1 - 7, 2019"
					y: 3
				},
				{
					x: "Apr 8 - 14, 2019"
					y: 5
				},
				{
					x: "Aug 5 - 11, 2019"
					y: 5
				},
				{
					x: "Aug 26 - Sep 1, 2019"
					y: 6
				},
				{
					x: "Sep 9 - 15, 2019"
					y: 33
				},
				{
					x: "Sep 16 - 22, 2019"
					y: 35
				},
				{
					x: "Dec 30, 2019 - Jan 5, 2020"
					y: 36
				},
			],

			[
				{
					x: "Nov 26 - Dec 2, 2018"
					y: 0
				},
				{
					x: "Jan 14 - 20, 2019"
					y: 1
				},
				{
					x: "Apr 1 - 7, 2019"
					y: 1
				},
				{
					x: "Apr 8 - 14, 2019"
					y: 1
				},
				{
					x: "Aug 5 - 11, 2019"
					y: 2
				},
				{
					x: "Aug 26 - Sep 1, 2019"
					y: 2
				},
				{
					x: "Sep 9 - 15, 2019"
					y: 6
				},
				{
					x: "Sep 16 - 22, 2019"
					y: 7
				},
				{
					x: "Dec 30, 2019 - Jan 5, 2020"
					y: 7
				},
			],

			[
				{
					x: "Nov 26 - Dec 2, 2018"
					y: 0
				},
				{
					x: "Jan 14 - 20, 2019"
					y: 1
				},
				{
					x: "Apr 1 - 7, 2019"
					y: 2
				},
				{
					x: "Apr 8 - 14, 2019"
					y: 2
				},
				{
					x: "Aug 5 - 11, 2019"
					y: 2
				},
				{
					x: "Aug 26 - Sep 1, 2019"
					y: 2
				},
				{
					x: "Sep 9 - 15, 2019"
					y: 3
				},
				{
					x: "Sep 16 - 22, 2019"
					y: 3
				},
				{
					x: "Dec 30, 2019 - Jan 5, 2020"
					y: 3
				},
			],

			[
				{
					x: "Nov 26 - Dec 2, 2018"
					y: 0
				},
				{
					x: "Jan 14 - 20, 2019"
					y: 2
				},
				{
					x: "Apr 1 - 7, 2019"
					y: 3
				},
				{
					x: "Apr 8 - 14, 2019"
					y: 3
				},
				{
					x: "Aug 5 - 11, 2019"
					y: 4
				},
				{
					x: "Aug 26 - Sep 1, 2019"
					y: 4
				},
				{
					x: "Sep 9 - 15, 2019"
					y: 9
				},
				{
					x: "Sep 16 - 22, 2019"
					y: 10
				},
				{
					x: "Dec 30, 2019 - Jan 5, 2020"
					y: 10
				},
			],

			[
				{
					x: "Nov 26 - Dec 2, 2018"
					y: 1
				},
				{
					x: "Jan 14 - 20, 2019"
					y: 1
				},
				{
					x: "Apr 1 - 7, 2019"
					y: 2
				},
				{
					x: "Apr 8 - 14, 2019"
					y: 2
				},
				{
					x: "Aug 5 - 11, 2019"
					y: 1
				},
				{
					x: "Aug 26 - Sep 1, 2019"
					y: 2
				},
				{
					x: "Sep 9 - 15, 2019"
					y: 24
				},
				{
					x: "Sep 16 - 22, 2019"
					y: 25
				},
				{
					x: "Dec 30, 2019 - Jan 5, 2020"
					y: 26
				},
			],
		]

		mean_remediate_low_severity: 232
		nofluid_quantity:            0
		code_languages:
		[
			{
				loc:      15
				language: "Python"
			},
			{
				loc:      27
				language: "Ruby"
			},
		]

		mean_remediate_medium_severity: 287
	},
] & [...#GroupUnreliableIndicatorsItem]
