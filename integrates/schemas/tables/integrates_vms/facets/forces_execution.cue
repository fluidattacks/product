package forces_execution

import (
	"fluidattacks.com/types/forces"
)

#forcesExecutionPk: =~"^EXEC#[a-f0-9]{32}$"
#forcesExecutionSk: =~"^GROUP#[a-z]+"

#ForcesExecutionKeys: {
	pk:   string & #forcesExecutionPk
	sk:   string & #forcesExecutionSk
	pk_2: string
	sk_2: string
}

#ForcesExecutionItem: {
	#ForcesExecutionKeys
	forces.#ForcesExecution
}

ForcesExecution:
{
	FacetName: "forces_execution"
	KeyAttributeAlias: {
		PartitionKeyAlias: "EXEC#id"
		SortKeyAlias:      "GROUP#name"
	}
	NonKeyAttributes: [
		"exit_code",
		"branch",
		"commit",
		"origin",
		"repo",
		"strictness",
		"grace_period",
		"severity_threshold",
		"days_until_it_breaks",
		"id",
		"group_name",
		"pk_2",
		"sk_2",
		"execution_date",
		"kind",
		"vulnerabilities",
	]
	DataAccess: {
		MySql: {}
	}
}

ForcesExecution: TableData: [
	{
		group_name:     "unittesting"
		execution_date: "2020-09-01T18:54:36+00:00"
		kind:           "other"
		repo:           "integrates"
		origin:         "git@gitlab.com:fluidattacks/integrates.git"
		commit:         "743c41ec54d616299bebe12d9ad5fec1eca12271"
		branch:         "drestrepoatfluid"
		grace_period:   0
		sk:             "GROUP#unittesting"
		exit_code:      "0"
		vulnerabilities: {
			num_of_accepted_vulnerabilities:       0
			num_of_open_vulnerabilities:           24
			num_of_closed_vulnerabilities:         5
			num_of_open_managed_vulnerabilities:   24
			num_of_open_unmanaged_vulnerabilities: 5
		}
		pk_2:               "GROUP#unittesting"
		strictness:         "lax"
		pk:                 "EXEC#23a7ac72b6b24c9fa4cfcd41c7fdd505"
		id:                 "23a7ac72b6b24c9fa4cfcd41c7fdd505"
		sk_2:               "EXEC#2020-09-01T18:54:36+00:00"
		severity_threshold: 0.0
	},
	{
		group_name:     "unittesting"
		execution_date: "2020-09-02T18:54:36+00:00"
		kind:           "other"
		repo:           "integrates"
		origin:         "git@gitlab.com:fluidattacks/integrates.git"
		commit:         "743c41ec54d616299bebe12d9ad5fec1eca12271"
		branch:         "drestrepoatfluid"
		grace_period:   0
		sk:             "GROUP#unittesting"
		exit_code:      "0"
		vulnerabilities: {
			num_of_accepted_vulnerabilities:       0
			num_of_open_vulnerabilities:           32
			num_of_closed_vulnerabilities:         8
			num_of_open_managed_vulnerabilities:   32
			num_of_open_unmanaged_vulnerabilities: 8
		}
		pk_2:               "GROUP#unittesting"
		strictness:         "lax"
		pk:                 "EXEC#ecf62c1fd6064c1eb773da49750c4011"
		id:                 "ecf62c1fd6064c1eb773da49750c4011"
		sk_2:               "EXEC#2020-09-02T18:54:36+00:00"
		severity_threshold: 0.0
	},
	{
		group_name:     "unittesting"
		execution_date: "2020-09-03T18:54:36+00:00"
		kind:           "other"
		repo:           "integrates"
		origin:         "git@gitlab.com:fluidattacks/integrates.git"
		commit:         "743c41ec54d616299bebe12d9ad5fec1eca12271"
		branch:         "drestrepoatfluid"
		grace_period:   0
		sk:             "GROUP#unittesting"
		exit_code:      "0"
		vulnerabilities: {
			num_of_accepted_vulnerabilities:       0
			num_of_open_vulnerabilities:           0
			num_of_closed_vulnerabilities:         8
			num_of_open_managed_vulnerabilities:   0
			num_of_open_unmanaged_vulnerabilities: 0
		}
		pk_2:               "GROUP#unittesting"
		strictness:         "lax"
		pk:                 "EXEC#b97ab29d82c84b14a189bc045bdfa76f"
		id:                 "b97ab29d82c84b14a189bc045bdfa76f"
		sk_2:               "EXEC#2020-09-03T18:54:36+00:00"
		severity_threshold: 0.0
	},
	{
		group_name:     "unittesting"
		execution_date: "2020-09-04T18:54:36+00:00"
		kind:           "other"
		repo:           "unable to retrieve"
		origin:         "unable to retrieve"
		commit:         "unable to retrieve"
		branch:         "unable to retrieve"
		grace_period:   0
		sk:             "GROUP#unittesting"
		exit_code:      "0"
		vulnerabilities: {
			num_of_accepted_vulnerabilities:       0
			num_of_open_vulnerabilities:           32
			num_of_closed_vulnerabilities:         8
			num_of_open_managed_vulnerabilities:   8
			num_of_open_unmanaged_vulnerabilities: 24
		}
		pk_2:               "GROUP#unittesting"
		strictness:         "lax"
		pk:                 "EXEC#08c1e735a73243f2ab1ee0757041f80e"
		id:                 "08c1e735a73243f2ab1ee0757041f80e"
		sk_2:               "EXEC#2020-09-04T18:54:36+00:00"
		severity_threshold: 0.0
	},
	{
		group_name:     "unittesting"
		execution_date: "2020-09-05T18:54:36+00:00"
		kind:           "other"
		repo:           "integrates"
		origin:         "git@gitlab.com:fluidattacks/integrates.git"
		commit:         "743c41ec54d616299bebe12d9ad5fec1eca12271"
		branch:         "drestrepoatfluid"
		grace_period:   0
		sk:             "GROUP#unittesting"
		exit_code:      "0"
		vulnerabilities: {
			num_of_accepted_vulnerabilities:       0
			num_of_open_vulnerabilities:           29
			num_of_closed_vulnerabilities:         0
			num_of_open_managed_vulnerabilities:   24
			num_of_open_unmanaged_vulnerabilities: 5
		}
		pk_2:               "GROUP#unittesting"
		strictness:         "lax"
		pk:                 "EXEC#92968b3ba84645bf976c12b15f9464bb"
		id:                 "92968b3ba84645bf976c12b15f9464bb"
		sk_2:               "EXEC#2020-09-05T18:54:36+00:00"
		severity_threshold: 0.0
	},
] & [...#ForcesExecutionItem]
