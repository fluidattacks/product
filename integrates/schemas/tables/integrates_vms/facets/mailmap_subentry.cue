package mailmap_subentry

import (
	"fluidattacks.com/types/mailmap"
)

#mailmapSubentryPk: =~"^ORG#[a-f0-9-]{36}#MAILMAP_ENTRY#[a-z0-9.@-]+"
#mailmapSubentrySk: =~"^MAILMAP_SUBENTRY#[a-z0-9.@-]+#[a-zA-Z0-9 ]+"

#MailmapSubentryKeys: {
	pk:   string & #mailmapSubentryPk
	sk:   string & #mailmapSubentrySk
	pk_2: string
	sk_2: string
}

#MailmapSubentryItem: {
	#MailmapSubentryKeys
	mailmap.#MailmapSubentry
}

MailmapSubentry:
{
	FacetName: "mailmap_subentry"
	KeyAttributeAlias: {
		PartitionKeyAlias: "ORG#organization_id#MAILMAP_ENTRY#entry_email"
		SortKeyAlias:      "MAILMAP_SUBENTRY#subentry_email#subentry_name"
	}
	NonKeyAttributes: [
		"mailmap_subentry_name",
		"mailmap_subentry_email",
		"mailmap_subentry_created_at",
		"mailmap_subentry_updated_at",
		"sk_2",
		"pk_2",
	]
	DataAccess: {
		MySql: {}
	}
}

MailmapSubentry: TableData: [
	{
		mailmap_subentry_name:  "Alice Garcia"
		mailmap_subentry_email: "alice@subentry.com"
		pk:                     "ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3#MAILMAP_ENTRY#alice@entry.com"
		sk:                     "MAILMAP_SUBENTRY#alice@subentry.com#Alice Garcia"
		pk_2:                   "ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3#MAILMAP_SUBENTRY#all"
		sk_2:                   "MAILMAP_SUBENTRY#alice@subentry.com"
	},
	{
		mailmap_subentry_name:  "Alice B Garcia"
		mailmap_subentry_email: "alice-b@subentry.com"
		pk:                     "ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3#MAILMAP_ENTRY#alice@entry.com"
		sk:                     "MAILMAP_SUBENTRY#alice-b@subentry.com#Alice B Garcia"
		pk_2:                   "ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3#MAILMAP_SUBENTRY#all"
		sk_2:                   "MAILMAP_SUBENTRY#alice-b@subentry.com"
	},
	{
		mailmap_subentry_name:  "Bob Singh"
		mailmap_subentry_email: "bob@subentry.com"
		pk:                     "ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3#MAILMAP_ENTRY#bob@entry.com"
		sk:                     "MAILMAP_SUBENTRY#bob@subentry.com#Bob Singh"
		pk_2:                   "ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3#MAILMAP_SUBENTRY#all"
		sk_2:                   "MAILMAP_SUBENTRY#bob@subentry.com"
	},
	{
		mailmap_subentry_name:  "Bob C Singh"
		mailmap_subentry_email: "bob-c@subentry.com"
		pk:                     "ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3#MAILMAP_ENTRY#bob@entry.com"
		sk:                     "MAILMAP_SUBENTRY#bob-c@subentry.com#Bob C Singh"
		pk_2:                   "ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3#MAILMAP_SUBENTRY#all"
		sk_2:                   "MAILMAP_SUBENTRY#bob-c@subentry.com"
	},
	{
		mailmap_subentry_name:  "Charlie Nguyen D"
		mailmap_subentry_email: "charlie@entry.com"
		pk:                     "ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3#MAILMAP_ENTRY#charlie@entry.com"
		sk:                     "MAILMAP_SUBENTRY#charlie@entry.com#Charlie Nguyen D"
		pk_2:                   "ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3#MAILMAP_SUBENTRY#all"
		sk_2:                   "MAILMAP_SUBENTRY#charlie@entry.com"
	},
	{
		mailmap_subentry_name:  "Charlie D Nguyen"
		mailmap_subentry_email: "charlie@entry.com"
		pk:                     "ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3#MAILMAP_ENTRY#charlie@entry.com"
		sk:                     "MAILMAP_SUBENTRY#charlie@entry.com#Charlie D Nguyen"
		pk_2:                   "ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3#MAILMAP_SUBENTRY#all"
		sk_2:                   "MAILMAP_SUBENTRY#charlie@entry.com"
	},
	{
		mailmap_subentry_name:  "Eve Anderson"
		mailmap_subentry_email: "eve@subentry.com"
		pk:                     "ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3#MAILMAP_ENTRY#eve@entry.com"
		sk:                     "MAILMAP_SUBENTRY#eve@subentry.com#Eve Anderson"
		pk_2:                   "ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3#MAILMAP_SUBENTRY#all"
		sk_2:                   "MAILMAP_SUBENTRY#eve@subentry.com"
	},
	{
		mailmap_subentry_name:  "Eve F Anderson"
		mailmap_subentry_email: "eve-f@subentry.com"
		pk:                     "ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3#MAILMAP_ENTRY#eve@entry.com"
		sk:                     "MAILMAP_SUBENTRY#eve-f@subentry.com#Eve F Anderson"
		pk_2:                   "ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3#MAILMAP_SUBENTRY#all"
		sk_2:                   "MAILMAP_SUBENTRY#eve-f@subentry.com"
	},
	{
		mailmap_subentry_name:  "Mallory Santos"
		mailmap_subentry_email: "mallory@subentry.com"
		pk:                     "ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3#MAILMAP_ENTRY#mallory@entry.com"
		sk:                     "MAILMAP_SUBENTRY#mallory@subentry.com#Mallory Santos"
		pk_2:                   "ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3#MAILMAP_SUBENTRY#all"
		sk_2:                   "MAILMAP_SUBENTRY#mallory@subentry.com"
	},
	{
		mailmap_subentry_name:  "Mallory N Santos"
		mailmap_subentry_email: "mallory-n@subentry.com"
		pk:                     "ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3#MAILMAP_ENTRY#mallory@entry.com"
		sk:                     "MAILMAP_SUBENTRY#mallory-n@subentry.com#Mallory N Santos"
		pk_2:                   "ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3#MAILMAP_SUBENTRY#all"
		sk_2:                   "MAILMAP_SUBENTRY#mallory-n@subentry.com"
	},
	{
		mailmap_subentry_name:  "Trent Hernandez"
		mailmap_subentry_email: "trent@subentry.com"
		pk:                     "ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3#MAILMAP_ENTRY#trent@entry.com"
		sk:                     "MAILMAP_SUBENTRY#trent@subentry.com#Trent Hernandez"
		pk_2:                   "ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3#MAILMAP_SUBENTRY#all"
		sk_2:                   "MAILMAP_SUBENTRY#trent@subentry.com"
	},
	{
		mailmap_subentry_name:  "Trent S Hernandez"
		mailmap_subentry_email: "trent-s@subentry.com"
		pk:                     "ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3#MAILMAP_ENTRY#trent@entry.com"
		sk:                     "MAILMAP_SUBENTRY#trent-s@subentry.com#Trent S Hernandez"
		pk_2:                   "ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3#MAILMAP_SUBENTRY#all"
		sk_2:                   "MAILMAP_SUBENTRY#trent-s@subentry.com"
	},
	{
		mailmap_subentry_name:  "Grace Musa"
		mailmap_subentry_email: "grace@subentry.com"
		pk:                     "ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3#MAILMAP_ENTRY#grace@entry.com"
		sk:                     "MAILMAP_SUBENTRY#grace@subentry.com#Grace Musa"
		pk_2:                   "ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3#MAILMAP_SUBENTRY#all"
		sk_2:                   "MAILMAP_SUBENTRY#grace@subentry.com"
	},
	{
		mailmap_subentry_name:  "Grace H Musa"
		mailmap_subentry_email: "grace-h@subentry.com"
		pk:                     "ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3#MAILMAP_ENTRY#grace@entry.com"
		sk:                     "MAILMAP_SUBENTRY#grace-h@subentry.com#Grace H Musa"
		pk_2:                   "ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3#MAILMAP_SUBENTRY#all"
		sk_2:                   "MAILMAP_SUBENTRY#grace-h@subentry.com"
	},
	{
		mailmap_subentry_name:  "Oscar Schmidt"
		mailmap_subentry_email: "oscar@subentry.com"
		pk:                     "ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3#MAILMAP_ENTRY#oscar@entry.com"
		sk:                     "MAILMAP_SUBENTRY#oscar@subentry.com#Oscar Schmidt"
		pk_2:                   "ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3#MAILMAP_SUBENTRY#all"
		sk_2:                   "MAILMAP_SUBENTRY#oscar@subentry.com"
	},
	{
		mailmap_subentry_name:  "Oscar P Schmidt"
		mailmap_subentry_email: "oscar-p@subentry.com"
		pk:                     "ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3#MAILMAP_ENTRY#oscar@entry.com"
		sk:                     "MAILMAP_SUBENTRY#oscar-p@subentry.com#Oscar P Schmidt"
		pk_2:                   "ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3#MAILMAP_SUBENTRY#all"
		sk_2:                   "MAILMAP_SUBENTRY#oscar-p@subentry.com"
	},
	{
		mailmap_subentry_name:  "Wendy Ozcan"
		mailmap_subentry_email: "wendy@subentry.com"
		pk:                     "ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3#MAILMAP_ENTRY#wendy@entry.com"
		sk:                     "MAILMAP_SUBENTRY#wendy@subentry.com#Wendy Ozcan"
		pk_2:                   "ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3#MAILMAP_SUBENTRY#all"
		sk_2:                   "MAILMAP_SUBENTRY#wendy@subentry.com"
	},
	{
		mailmap_subentry_name:  "Wendy X Ozcan"
		mailmap_subentry_email: "wendy-x@subentry.com"
		pk:                     "ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3#MAILMAP_ENTRY#wendy@entry.com"
		sk:                     "MAILMAP_SUBENTRY#wendy-x@subentry.com#Wendy X Ozcan"
		pk_2:                   "ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3#MAILMAP_SUBENTRY#all"
		sk_2:                   "MAILMAP_SUBENTRY#wendy-x@subentry.com"
	},
] & [...#MailmapSubentryItem]
