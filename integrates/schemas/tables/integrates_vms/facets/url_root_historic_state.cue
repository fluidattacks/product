package url_root_historic_state

import (
	"fluidattacks.com/types/roots"
)

#urlRootHistoricStatePk: =~"^ROOT#[a-f0-9-]{36}$"
#urlRootHistoricStateSk: =~"^STATE#(\\d{4})-(\\d{2})-(\\d{2})(T(\\d{2}):(\\d{2}):(\\d{2})(\\.\\d+)?(Z|([+-])(\\d{2}):(\\d{2})))?$"

#UrlRootHistoricStateKeys: {
	pk: string & #urlRootHistoricStatePk
	sk: string & #urlRootHistoricStateSk
}

#UrlRootHistoricStateItem: {
	#UrlRootHistoricStateKeys
	roots.#UrlRootState
}

UrlRootHistoricState:
{
	FacetName: "url_root_historic_state"
	KeyAttributeAlias: {
		PartitionKeyAlias: "ROOT#uuid"
		SortKeyAlias:      "STATE#iso8601utc"
	}
	NonKeyAttributes: [
		"status",
		"reason",
		"modified_date",
		"modified_by",
		"nickname",
		"port",
		"host",
		"path",
		"protocol",
		"other",
		"query",
		"excluded_sub_paths",
	]
	DataAccess: {
		MySql: {}
	}
}

UrlRootHistoricState: TableData: [
	{
		pk:            "ROOT#8493c82f-2860-4902-86fa-75b0fef76034"
		sk:            "STATE#2020-11-19T13:45:55+00:00"
		status:        "ACTIVE"
		modified_date: "2020-11-19T13:45:55+00:00"
		modified_by:   "jdoe@fluidattacks.com"
		nickname:      "url_root_1"
		port:          "443"
		host:          "app.fluidattacks.com"
		path:          "/"
		protocol:      "HTTPS"
	},
] & [...#UrlRootHistoricStateItem]
