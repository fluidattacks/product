package organization_unreliable_indicators

import (
	"fluidattacks.com/types/organizations"
)

#organizationUnreliableIndicatorsPk: =~"^ORG#ORG#[0-9a-z-]{36}#UNRELIABLEINDICATOR$"
#organizationUnreliableIndicatorsSk: =~"^ORG#[a-z]+#UNRELIABLEINDICATORS$"

#OrganizationUnreliableIndicatorsKeys: {
	pk: string & #organizationUnreliableIndicatorsPk
	sk: string & #organizationUnreliableIndicatorsSk
}

#OrganizationUnreliableIndicatorsItem: {
	#OrganizationUnreliableIndicatorsKeys
	organizations.#OrganizationUnreliableIndicators
}

OrganizationUnreliableIndicators:
{
	FacetName: "organization_unreliable_indicators"
	KeyAttributeAlias: {
		PartitionKeyAlias: "ORG#id#UNRELIABLEINDICATOR"
		SortKeyAlias:      "ORG#name#UNRELIABLEINDICATORS"
	}
	NonKeyAttributes: [
		"compliance_level",
		"standard_compliances",
		"compliance_weekly_trend",
		"estimated_days_to_full_compliance",
		"missed_repositories",
		"covered_repositories",
		"missed_authors",
		"covered_authors",
		"has_ztna_roots",
	]
	DataAccess: {
		MySql: {}
	}
}

OrganizationUnreliableIndicators: TableData: [
	{
		compliance_weekly_trend:           0.00
		sk:                                "ORG#bulat#UNRELIABLEINDICATORS"
		pk:                                "ORG#ORG#c2ee2d15-04ab-4f39-9795-fbe30cdeee86#UNRELIABLEINDICATOR"
		estimated_days_to_full_compliance: 0.00
		standard_compliances:
		[
			{
				standard_name:    "bsimm"
				compliance_level: 1.00
			},
			{
				standard_name:    "capec"
				compliance_level: 1.00
			},
			{
				standard_name:    "cis"
				compliance_level: 1.00
			},
			{
				standard_name:    "cwe"
				compliance_level: 1.00
			},
			{
				standard_name:    "eprivacy"
				compliance_level: 1.00
			},
			{
				standard_name:    "gdpr"
				compliance_level: 1.00
			},
			{
				standard_name:    "hipaa"
				compliance_level: 1.00
			},
			{
				standard_name:    "iso27001"
				compliance_level: 1.00
			},
			{
				standard_name:    "nerccip"
				compliance_level: 1.00
			},
			{
				standard_name:    "nist80053"
				compliance_level: 1.00
			},
			{
				standard_name:    "nist80063"
				compliance_level: 1.00
			},
			{
				standard_name:    "asvs"
				compliance_level: 1.00
			},
			{
				standard_name:    "owasp10"
				compliance_level: 1.00
			},
			{
				standard_name:    "pci"
				compliance_level: 1.00
			},
			{
				standard_name:    "soc2"
				compliance_level: 1.00
			},
			{
				standard_name:    "cwe25"
				compliance_level: 1.00
			},
			{
				standard_name:    "owaspm10"
				compliance_level: 1.00
			},
			{
				standard_name:    "nist"
				compliance_level: 1.00
			},
			{
				standard_name:    "agile"
				compliance_level: 1.00
			},
			{
				standard_name:    "bizec"
				compliance_level: 1.00
			},
			{
				standard_name:    "ccpa"
				compliance_level: 1.00
			},
			{
				standard_name:    "cpra"
				compliance_level: 1.00
			},
			{
				standard_name:    "certc"
				compliance_level: 1.00
			},
			{
				standard_name:    "certj"
				compliance_level: 1.00
			},
			{
				standard_name:    "fcra"
				compliance_level: 1.00
			},
			{
				standard_name:    "facta"
				compliance_level: 1.00
			},
			{
				standard_name:    "glba"
				compliance_level: 1.00
			},
			{
				standard_name:    "misrac"
				compliance_level: 1.00
			},
			{
				standard_name:    "nydfs"
				compliance_level: 1.00
			},
			{
				standard_name:    "nyshield"
				compliance_level: 1.00
			},
			{
				standard_name:    "mitre"
				compliance_level: 1.00
			},
			{
				standard_name:    "padss"
				compliance_level: 1.00
			},
			{
				standard_name:    "sans25"
				compliance_level: 1.00
			},
			{
				standard_name:    "pdpa"
				compliance_level: 1.00
			},
			{
				standard_name:    "popia"
				compliance_level: 1.00
			},
			{
				standard_name:    "pdpo"
				compliance_level: 1.00
			},
			{
				standard_name:    "cmmc"
				compliance_level: 1.00
			},
			{
				standard_name:    "hitrust"
				compliance_level: 1.00
			},
			{
				standard_name:    "fedramp"
				compliance_level: 1.00
			},
			{
				standard_name:    "iso27002"
				compliance_level: 1.00
			},
			{
				standard_name:    "lgpd"
				compliance_level: 1.00
			},
			{
				standard_name:    "iec62443"
				compliance_level: 1.00
			},
			{
				standard_name:    "wassec"
				compliance_level: 1.00
			},
			{
				standard_name:    "osstmm3"
				compliance_level: 1.00
			},
			{
				standard_name:    "c2m2"
				compliance_level: 1.00
			},
			{
				standard_name:    "wasc"
				compliance_level: 1.00
			},
			{
				standard_name:    "ferpa"
				compliance_level: 1.00
			},
			{
				standard_name:    "nistssdf"
				compliance_level: 1.00
			},
			{
				standard_name:    "issaf"
				compliance_level: 1.00
			},
			{
				standard_name:    "ptes"
				compliance_level: 1.00
			},
			{
				standard_name:    "owasprisks"
				compliance_level: 1.00
			},
			{
				standard_name:    "mvsp"
				compliance_level: 1.00
			},
			{
				standard_name:    "owaspscp"
				compliance_level: 1.00
			},
			{
				standard_name:    "bsafss"
				compliance_level: 1.00
			},
			{
				standard_name:    "owaspmasvs"
				compliance_level: 1.00
			},
			{
				standard_name:    "nist800171"
				compliance_level: 1.00
			},
			{
				standard_name:    "nist800115"
				compliance_level: 1.00
			},
			{
				standard_name:    "swiftcsc"
				compliance_level: 1.00
			},
			{
				standard_name:    "osamm"
				compliance_level: 1.00
			},
			{
				standard_name:    "siglite"
				compliance_level: 1.00
			},
			{
				standard_name:    "sig"
				compliance_level: 1.00
			},
		]

		compliance_level: 1.00
	},
	{
		compliance_weekly_trend:           0.00
		sk:                                "ORG#hajime#UNRELIABLEINDICATORS"
		pk:                                "ORG#ORG#f2e2777d-a168-4bea-93cd-d79142b294d2#UNRELIABLEINDICATOR"
		estimated_days_to_full_compliance: 0.00
		standard_compliances:
		[
			{
				standard_name:    "bsimm"
				compliance_level: 1.00
			},
			{
				standard_name:    "capec"
				compliance_level: 1.00
			},
			{
				standard_name:    "cis"
				compliance_level: 1.00
			},
			{
				standard_name:    "cwe"
				compliance_level: 1.00
			},
			{
				standard_name:    "eprivacy"
				compliance_level: 1.00
			},
			{
				standard_name:    "gdpr"
				compliance_level: 1.00
			},
			{
				standard_name:    "hipaa"
				compliance_level: 1.00
			},
			{
				standard_name:    "iso27001"
				compliance_level: 1.00
			},
			{
				standard_name:    "nerccip"
				compliance_level: 1.00
			},
			{
				standard_name:    "nist80053"
				compliance_level: 1.00
			},
			{
				standard_name:    "nist80063"
				compliance_level: 1.00
			},
			{
				standard_name:    "asvs"
				compliance_level: 1.00
			},
			{
				standard_name:    "owasp10"
				compliance_level: 1.00
			},
			{
				standard_name:    "pci"
				compliance_level: 1.00
			},
			{
				standard_name:    "soc2"
				compliance_level: 1.00
			},
			{
				standard_name:    "cwe25"
				compliance_level: 1.00
			},
			{
				standard_name:    "owaspm10"
				compliance_level: 1.00
			},
			{
				standard_name:    "nist"
				compliance_level: 1.00
			},
			{
				standard_name:    "agile"
				compliance_level: 1.00
			},
			{
				standard_name:    "bizec"
				compliance_level: 1.00
			},
			{
				standard_name:    "ccpa"
				compliance_level: 1.00
			},
			{
				standard_name:    "cpra"
				compliance_level: 1.00
			},
			{
				standard_name:    "certc"
				compliance_level: 1.00
			},
			{
				standard_name:    "certj"
				compliance_level: 1.00
			},
			{
				standard_name:    "fcra"
				compliance_level: 1.00
			},
			{
				standard_name:    "facta"
				compliance_level: 1.00
			},
			{
				standard_name:    "glba"
				compliance_level: 1.00
			},
			{
				standard_name:    "misrac"
				compliance_level: 1.00
			},
			{
				standard_name:    "nydfs"
				compliance_level: 1.00
			},
			{
				standard_name:    "nyshield"
				compliance_level: 1.00
			},
			{
				standard_name:    "mitre"
				compliance_level: 1.00
			},
			{
				standard_name:    "padss"
				compliance_level: 1.00
			},
			{
				standard_name:    "sans25"
				compliance_level: 1.00
			},
			{
				standard_name:    "pdpa"
				compliance_level: 1.00
			},
			{
				standard_name:    "popia"
				compliance_level: 1.00
			},
			{
				standard_name:    "pdpo"
				compliance_level: 1.00
			},
			{
				standard_name:    "cmmc"
				compliance_level: 1.00
			},
			{
				standard_name:    "hitrust"
				compliance_level: 1.00
			},
			{
				standard_name:    "fedramp"
				compliance_level: 1.00
			},
			{
				standard_name:    "iso27002"
				compliance_level: 1.00
			},
			{
				standard_name:    "lgpd"
				compliance_level: 1.00
			},
			{
				standard_name:    "iec62443"
				compliance_level: 1.00
			},
			{
				standard_name:    "wassec"
				compliance_level: 1.00
			},
			{
				standard_name:    "osstmm3"
				compliance_level: 1.00
			},
			{
				standard_name:    "c2m2"
				compliance_level: 1.00
			},
			{
				standard_name:    "wasc"
				compliance_level: 1.00
			},
			{
				standard_name:    "ferpa"
				compliance_level: 1.00
			},
			{
				standard_name:    "nistssdf"
				compliance_level: 1.00
			},
			{
				standard_name:    "issaf"
				compliance_level: 1.00
			},
			{
				standard_name:    "ptes"
				compliance_level: 1.00
			},
			{
				standard_name:    "owasprisks"
				compliance_level: 1.00
			},
			{
				standard_name:    "mvsp"
				compliance_level: 1.00
			},
			{
				standard_name:    "owaspscp"
				compliance_level: 1.00
			},
			{
				standard_name:    "bsafss"
				compliance_level: 1.00
			},
			{
				standard_name:    "owaspmasvs"
				compliance_level: 1.00
			},
			{
				standard_name:    "nist800171"
				compliance_level: 1.00
			},
			{
				standard_name:    "nist800115"
				compliance_level: 1.00
			},
			{
				standard_name:    "swiftcsc"
				compliance_level: 1.00
			},
			{
				standard_name:    "osamm"
				compliance_level: 1.00
			},
			{
				standard_name:    "siglite"
				compliance_level: 1.00
			},
			{
				standard_name:    "sig"
				compliance_level: 1.00
			},
		]

		compliance_level: 1.00
	},
	{
		compliance_weekly_trend:           0.00
		sk:                                "ORG#himura#UNRELIABLEINDICATORS"
		pk:                                "ORG#ORG#ffddc7a3-7f05-4fc7-b65d-7defffa883c2#UNRELIABLEINDICATOR"
		estimated_days_to_full_compliance: 0.00
		standard_compliances:
		[
			{
				standard_name:    "bsimm"
				compliance_level: 1.00
			},
			{
				standard_name:    "capec"
				compliance_level: 1.00
			},
			{
				standard_name:    "cis"
				compliance_level: 1.00
			},
			{
				standard_name:    "cwe"
				compliance_level: 1.00
			},
			{
				standard_name:    "eprivacy"
				compliance_level: 1.00
			},
			{
				standard_name:    "gdpr"
				compliance_level: 1.00
			},
			{
				standard_name:    "hipaa"
				compliance_level: 1.00
			},
			{
				standard_name:    "iso27001"
				compliance_level: 1.00
			},
			{
				standard_name:    "nerccip"
				compliance_level: 1.00
			},
			{
				standard_name:    "nist80053"
				compliance_level: 1.00
			},
			{
				standard_name:    "nist80063"
				compliance_level: 1.00
			},
			{
				standard_name:    "asvs"
				compliance_level: 1.00
			},
			{
				standard_name:    "owasp10"
				compliance_level: 1.00
			},
			{
				standard_name:    "pci"
				compliance_level: 1.00
			},
			{
				standard_name:    "soc2"
				compliance_level: 1.00
			},
			{
				standard_name:    "cwe25"
				compliance_level: 1.00
			},
			{
				standard_name:    "owaspm10"
				compliance_level: 1.00
			},
			{
				standard_name:    "nist"
				compliance_level: 1.00
			},
			{
				standard_name:    "agile"
				compliance_level: 1.00
			},
			{
				standard_name:    "bizec"
				compliance_level: 1.00
			},
			{
				standard_name:    "ccpa"
				compliance_level: 1.00
			},
			{
				standard_name:    "cpra"
				compliance_level: 1.00
			},
			{
				standard_name:    "certc"
				compliance_level: 1.00
			},
			{
				standard_name:    "certj"
				compliance_level: 1.00
			},
			{
				standard_name:    "fcra"
				compliance_level: 1.00
			},
			{
				standard_name:    "facta"
				compliance_level: 1.00
			},
			{
				standard_name:    "glba"
				compliance_level: 1.00
			},
			{
				standard_name:    "misrac"
				compliance_level: 1.00
			},
			{
				standard_name:    "nydfs"
				compliance_level: 1.00
			},
			{
				standard_name:    "nyshield"
				compliance_level: 1.00
			},
			{
				standard_name:    "mitre"
				compliance_level: 1.00
			},
			{
				standard_name:    "padss"
				compliance_level: 1.00
			},
			{
				standard_name:    "sans25"
				compliance_level: 1.00
			},
			{
				standard_name:    "pdpa"
				compliance_level: 1.00
			},
			{
				standard_name:    "popia"
				compliance_level: 1.00
			},
			{
				standard_name:    "pdpo"
				compliance_level: 1.00
			},
			{
				standard_name:    "cmmc"
				compliance_level: 1.00
			},
			{
				standard_name:    "hitrust"
				compliance_level: 1.00
			},
			{
				standard_name:    "fedramp"
				compliance_level: 1.00
			},
			{
				standard_name:    "iso27002"
				compliance_level: 1.00
			},
			{
				standard_name:    "lgpd"
				compliance_level: 1.00
			},
			{
				standard_name:    "iec62443"
				compliance_level: 1.00
			},
			{
				standard_name:    "wassec"
				compliance_level: 1.00
			},
			{
				standard_name:    "osstmm3"
				compliance_level: 1.00
			},
			{
				standard_name:    "c2m2"
				compliance_level: 1.00
			},
			{
				standard_name:    "wasc"
				compliance_level: 1.00
			},
			{
				standard_name:    "ferpa"
				compliance_level: 1.00
			},
			{
				standard_name:    "nistssdf"
				compliance_level: 1.00
			},
			{
				standard_name:    "issaf"
				compliance_level: 1.00
			},
			{
				standard_name:    "ptes"
				compliance_level: 1.00
			},
			{
				standard_name:    "owasprisks"
				compliance_level: 1.00
			},
			{
				standard_name:    "mvsp"
				compliance_level: 1.00
			},
			{
				standard_name:    "owaspscp"
				compliance_level: 1.00
			},
			{
				standard_name:    "bsafss"
				compliance_level: 1.00
			},
			{
				standard_name:    "owaspmasvs"
				compliance_level: 1.00
			},
			{
				standard_name:    "nist800171"
				compliance_level: 1.00
			},
			{
				standard_name:    "nist800115"
				compliance_level: 1.00
			},
			{
				standard_name:    "swiftcsc"
				compliance_level: 1.00
			},
			{
				standard_name:    "osamm"
				compliance_level: 1.00
			},
			{
				standard_name:    "siglite"
				compliance_level: 1.00
			},
			{
				standard_name:    "sig"
				compliance_level: 1.00
			},
		]

		compliance_level: 1.00
	},
	{
		compliance_weekly_trend:           0.00
		sk:                                "ORG#imamura#UNRELIABLEINDICATORS"
		pk:                                "ORG#ORG#7376c5fe-4634-4053-9718-e14ecbda1e6b#UNRELIABLEINDICATOR"
		estimated_days_to_full_compliance: 0.00
		standard_compliances:
		[
			{
				standard_name:    "bsimm"
				compliance_level: 1.00
			},
			{
				standard_name:    "capec"
				compliance_level: 1.00
			},
			{
				standard_name:    "cis"
				compliance_level: 1.00
			},
			{
				standard_name:    "cwe"
				compliance_level: 1.00
			},
			{
				standard_name:    "eprivacy"
				compliance_level: 1.00
			},
			{
				standard_name:    "gdpr"
				compliance_level: 1.00
			},
			{
				standard_name:    "hipaa"
				compliance_level: 1.00
			},
			{
				standard_name:    "iso27001"
				compliance_level: 1.00
			},
			{
				standard_name:    "nerccip"
				compliance_level: 1.00
			},
			{
				standard_name:    "nist80053"
				compliance_level: 1.00
			},
			{
				standard_name:    "nist80063"
				compliance_level: 1.00
			},
			{
				standard_name:    "asvs"
				compliance_level: 1.00
			},
			{
				standard_name:    "owasp10"
				compliance_level: 1.00
			},
			{
				standard_name:    "pci"
				compliance_level: 1.00
			},
			{
				standard_name:    "soc2"
				compliance_level: 1.00
			},
			{
				standard_name:    "cwe25"
				compliance_level: 1.00
			},
			{
				standard_name:    "owaspm10"
				compliance_level: 1.00
			},
			{
				standard_name:    "nist"
				compliance_level: 1.00
			},
			{
				standard_name:    "agile"
				compliance_level: 1.00
			},
			{
				standard_name:    "bizec"
				compliance_level: 1.00
			},
			{
				standard_name:    "ccpa"
				compliance_level: 1.00
			},
			{
				standard_name:    "cpra"
				compliance_level: 1.00
			},
			{
				standard_name:    "certc"
				compliance_level: 1.00
			},
			{
				standard_name:    "certj"
				compliance_level: 1.00
			},
			{
				standard_name:    "fcra"
				compliance_level: 1.00
			},
			{
				standard_name:    "facta"
				compliance_level: 1.00
			},
			{
				standard_name:    "glba"
				compliance_level: 1.00
			},
			{
				standard_name:    "misrac"
				compliance_level: 1.00
			},
			{
				standard_name:    "nydfs"
				compliance_level: 1.00
			},
			{
				standard_name:    "nyshield"
				compliance_level: 1.00
			},
			{
				standard_name:    "mitre"
				compliance_level: 1.00
			},
			{
				standard_name:    "padss"
				compliance_level: 1.00
			},
			{
				standard_name:    "sans25"
				compliance_level: 1.00
			},
			{
				standard_name:    "pdpa"
				compliance_level: 1.00
			},
			{
				standard_name:    "popia"
				compliance_level: 1.00
			},
			{
				standard_name:    "pdpo"
				compliance_level: 1.00
			},
			{
				standard_name:    "cmmc"
				compliance_level: 1.00
			},
			{
				standard_name:    "hitrust"
				compliance_level: 1.00
			},
			{
				standard_name:    "fedramp"
				compliance_level: 1.00
			},
			{
				standard_name:    "iso27002"
				compliance_level: 1.00
			},
			{
				standard_name:    "lgpd"
				compliance_level: 1.00
			},
			{
				standard_name:    "iec62443"
				compliance_level: 1.00
			},
			{
				standard_name:    "wassec"
				compliance_level: 1.00
			},
			{
				standard_name:    "osstmm3"
				compliance_level: 1.00
			},
			{
				standard_name:    "c2m2"
				compliance_level: 1.00
			},
			{
				standard_name:    "wasc"
				compliance_level: 1.00
			},
			{
				standard_name:    "ferpa"
				compliance_level: 1.00
			},
			{
				standard_name:    "nistssdf"
				compliance_level: 1.00
			},
			{
				standard_name:    "issaf"
				compliance_level: 1.00
			},
			{
				standard_name:    "ptes"
				compliance_level: 1.00
			},
			{
				standard_name:    "owasprisks"
				compliance_level: 1.00
			},
			{
				standard_name:    "mvsp"
				compliance_level: 1.00
			},
			{
				standard_name:    "owaspscp"
				compliance_level: 1.00
			},
			{
				standard_name:    "bsafss"
				compliance_level: 1.00
			},
			{
				standard_name:    "owaspmasvs"
				compliance_level: 1.00
			},
			{
				standard_name:    "nist800171"
				compliance_level: 1.00
			},
			{
				standard_name:    "nist800115"
				compliance_level: 1.00
			},
			{
				standard_name:    "swiftcsc"
				compliance_level: 1.00
			},
			{
				standard_name:    "osamm"
				compliance_level: 1.00
			},
			{
				standard_name:    "siglite"
				compliance_level: 1.00
			},
			{
				standard_name:    "sig"
				compliance_level: 1.00
			},
		]

		compliance_level: 1.00
	},
	{
		compliance_weekly_trend:           0.00
		sk:                                "ORG#kamiya#UNRELIABLEINDICATORS"
		pk:                                "ORG#ORG#956e9107-fd8d-49bc-b550-5609a7a1f6ac#UNRELIABLEINDICATOR"
		estimated_days_to_full_compliance: 0.00
		standard_compliances:
		[
			{
				standard_name:    "bsimm"
				compliance_level: 1.00
			},
			{
				standard_name:    "capec"
				compliance_level: 1.00
			},
			{
				standard_name:    "cis"
				compliance_level: 1.00
			},
			{
				standard_name:    "cwe"
				compliance_level: 1.00
			},
			{
				standard_name:    "eprivacy"
				compliance_level: 1.00
			},
			{
				standard_name:    "gdpr"
				compliance_level: 1.00
			},
			{
				standard_name:    "hipaa"
				compliance_level: 1.00
			},
			{
				standard_name:    "iso27001"
				compliance_level: 1.00
			},
			{
				standard_name:    "nerccip"
				compliance_level: 1.00
			},
			{
				standard_name:    "nist80053"
				compliance_level: 1.00
			},
			{
				standard_name:    "nist80063"
				compliance_level: 1.00
			},
			{
				standard_name:    "asvs"
				compliance_level: 1.00
			},
			{
				standard_name:    "owasp10"
				compliance_level: 1.00
			},
			{
				standard_name:    "pci"
				compliance_level: 1.00
			},
			{
				standard_name:    "soc2"
				compliance_level: 1.00
			},
			{
				standard_name:    "cwe25"
				compliance_level: 1.00
			},
			{
				standard_name:    "owaspm10"
				compliance_level: 1.00
			},
			{
				standard_name:    "nist"
				compliance_level: 1.00
			},
			{
				standard_name:    "agile"
				compliance_level: 1.00
			},
			{
				standard_name:    "bizec"
				compliance_level: 1.00
			},
			{
				standard_name:    "ccpa"
				compliance_level: 1.00
			},
			{
				standard_name:    "cpra"
				compliance_level: 1.00
			},
			{
				standard_name:    "certc"
				compliance_level: 1.00
			},
			{
				standard_name:    "certj"
				compliance_level: 1.00
			},
			{
				standard_name:    "fcra"
				compliance_level: 1.00
			},
			{
				standard_name:    "facta"
				compliance_level: 1.00
			},
			{
				standard_name:    "glba"
				compliance_level: 1.00
			},
			{
				standard_name:    "misrac"
				compliance_level: 1.00
			},
			{
				standard_name:    "nydfs"
				compliance_level: 1.00
			},
			{
				standard_name:    "nyshield"
				compliance_level: 1.00
			},
			{
				standard_name:    "mitre"
				compliance_level: 1.00
			},
			{
				standard_name:    "padss"
				compliance_level: 1.00
			},
			{
				standard_name:    "sans25"
				compliance_level: 1.00
			},
			{
				standard_name:    "pdpa"
				compliance_level: 1.00
			},
			{
				standard_name:    "popia"
				compliance_level: 1.00
			},
			{
				standard_name:    "pdpo"
				compliance_level: 1.00
			},
			{
				standard_name:    "cmmc"
				compliance_level: 1.00
			},
			{
				standard_name:    "hitrust"
				compliance_level: 1.00
			},
			{
				standard_name:    "fedramp"
				compliance_level: 1.00
			},
			{
				standard_name:    "iso27002"
				compliance_level: 1.00
			},
			{
				standard_name:    "lgpd"
				compliance_level: 1.00
			},
			{
				standard_name:    "iec62443"
				compliance_level: 1.00
			},
			{
				standard_name:    "wassec"
				compliance_level: 1.00
			},
			{
				standard_name:    "osstmm3"
				compliance_level: 1.00
			},
			{
				standard_name:    "c2m2"
				compliance_level: 1.00
			},
			{
				standard_name:    "wasc"
				compliance_level: 1.00
			},
			{
				standard_name:    "ferpa"
				compliance_level: 1.00
			},
			{
				standard_name:    "nistssdf"
				compliance_level: 1.00
			},
			{
				standard_name:    "issaf"
				compliance_level: 1.00
			},
			{
				standard_name:    "ptes"
				compliance_level: 1.00
			},
			{
				standard_name:    "owasprisks"
				compliance_level: 1.00
			},
			{
				standard_name:    "mvsp"
				compliance_level: 1.00
			},
			{
				standard_name:    "owaspscp"
				compliance_level: 1.00
			},
			{
				standard_name:    "bsafss"
				compliance_level: 1.00
			},
			{
				standard_name:    "owaspmasvs"
				compliance_level: 1.00
			},
			{
				standard_name:    "nist800171"
				compliance_level: 1.00
			},
			{
				standard_name:    "nist800115"
				compliance_level: 1.00
			},
			{
				standard_name:    "swiftcsc"
				compliance_level: 1.00
			},
			{
				standard_name:    "osamm"
				compliance_level: 1.00
			},
			{
				standard_name:    "siglite"
				compliance_level: 1.00
			},
			{
				standard_name:    "sig"
				compliance_level: 1.00
			},
		]

		compliance_level: 1.00
	},
	{
		compliance_weekly_trend:           0.00
		sk:                                "ORG#kiba#UNRELIABLEINDICATORS"
		pk:                                "ORG#ORG#33c08ebd-2068-47e7-9673-e1aa03dc9448#UNRELIABLEINDICATOR"
		estimated_days_to_full_compliance: 0.00
		standard_compliances:
		[
			{
				standard_name:    "bsimm"
				compliance_level: 1.00
			},
			{
				standard_name:    "capec"
				compliance_level: 1.00
			},
			{
				standard_name:    "cis"
				compliance_level: 1.00
			},
			{
				standard_name:    "cwe"
				compliance_level: 1.00
			},
			{
				standard_name:    "eprivacy"
				compliance_level: 1.00
			},
			{
				standard_name:    "gdpr"
				compliance_level: 1.00
			},
			{
				standard_name:    "hipaa"
				compliance_level: 1.00
			},
			{
				standard_name:    "iso27001"
				compliance_level: 1.00
			},
			{
				standard_name:    "nerccip"
				compliance_level: 1.00
			},
			{
				standard_name:    "nist80053"
				compliance_level: 1.00
			},
			{
				standard_name:    "nist80063"
				compliance_level: 1.00
			},
			{
				standard_name:    "asvs"
				compliance_level: 1.00
			},
			{
				standard_name:    "owasp10"
				compliance_level: 1.00
			},
			{
				standard_name:    "pci"
				compliance_level: 1.00
			},
			{
				standard_name:    "soc2"
				compliance_level: 1.00
			},
			{
				standard_name:    "cwe25"
				compliance_level: 1.00
			},
			{
				standard_name:    "owaspm10"
				compliance_level: 1.00
			},
			{
				standard_name:    "nist"
				compliance_level: 1.00
			},
			{
				standard_name:    "agile"
				compliance_level: 1.00
			},
			{
				standard_name:    "bizec"
				compliance_level: 1.00
			},
			{
				standard_name:    "ccpa"
				compliance_level: 1.00
			},
			{
				standard_name:    "cpra"
				compliance_level: 1.00
			},
			{
				standard_name:    "certc"
				compliance_level: 1.00
			},
			{
				standard_name:    "certj"
				compliance_level: 1.00
			},
			{
				standard_name:    "fcra"
				compliance_level: 1.00
			},
			{
				standard_name:    "facta"
				compliance_level: 1.00
			},
			{
				standard_name:    "glba"
				compliance_level: 1.00
			},
			{
				standard_name:    "misrac"
				compliance_level: 1.00
			},
			{
				standard_name:    "nydfs"
				compliance_level: 1.00
			},
			{
				standard_name:    "nyshield"
				compliance_level: 1.00
			},
			{
				standard_name:    "mitre"
				compliance_level: 1.00
			},
			{
				standard_name:    "padss"
				compliance_level: 1.00
			},
			{
				standard_name:    "sans25"
				compliance_level: 1.00
			},
			{
				standard_name:    "pdpa"
				compliance_level: 1.00
			},
			{
				standard_name:    "popia"
				compliance_level: 1.00
			},
			{
				standard_name:    "pdpo"
				compliance_level: 1.00
			},
			{
				standard_name:    "cmmc"
				compliance_level: 1.00
			},
			{
				standard_name:    "hitrust"
				compliance_level: 1.00
			},
			{
				standard_name:    "fedramp"
				compliance_level: 1.00
			},
			{
				standard_name:    "iso27002"
				compliance_level: 1.00
			},
			{
				standard_name:    "lgpd"
				compliance_level: 1.00
			},
			{
				standard_name:    "iec62443"
				compliance_level: 1.00
			},
			{
				standard_name:    "wassec"
				compliance_level: 1.00
			},
			{
				standard_name:    "osstmm3"
				compliance_level: 1.00
			},
			{
				standard_name:    "c2m2"
				compliance_level: 1.00
			},
			{
				standard_name:    "wasc"
				compliance_level: 1.00
			},
			{
				standard_name:    "ferpa"
				compliance_level: 1.00
			},
			{
				standard_name:    "nistssdf"
				compliance_level: 1.00
			},
			{
				standard_name:    "issaf"
				compliance_level: 1.00
			},
			{
				standard_name:    "ptes"
				compliance_level: 1.00
			},
			{
				standard_name:    "owasprisks"
				compliance_level: 1.00
			},
			{
				standard_name:    "mvsp"
				compliance_level: 1.00
			},
			{
				standard_name:    "owaspscp"
				compliance_level: 1.00
			},
			{
				standard_name:    "bsafss"
				compliance_level: 1.00
			},
			{
				standard_name:    "owaspmasvs"
				compliance_level: 1.00
			},
			{
				standard_name:    "nist800171"
				compliance_level: 1.00
			},
			{
				standard_name:    "nist800115"
				compliance_level: 1.00
			},
			{
				standard_name:    "swiftcsc"
				compliance_level: 1.00
			},
			{
				standard_name:    "osamm"
				compliance_level: 1.00
			},
			{
				standard_name:    "siglite"
				compliance_level: 1.00
			},
			{
				standard_name:    "sig"
				compliance_level: 1.00
			},
		]

		compliance_level: 1.00
	},
	{
		compliance_weekly_trend:           0.00
		sk:                                "ORG#makimachi#UNRELIABLEINDICATORS"
		pk:                                "ORG#ORG#c6cecc0e-bb92-4079-8b6d-c4e815c10bb1#UNRELIABLEINDICATOR"
		estimated_days_to_full_compliance: 0.00
		standard_compliances:
		[
			{
				standard_name:    "bsimm"
				compliance_level: 1.00
			},
			{
				standard_name:    "capec"
				compliance_level: 1.00
			},
			{
				standard_name:    "cis"
				compliance_level: 1.00
			},
			{
				standard_name:    "cwe"
				compliance_level: 1.00
			},
			{
				standard_name:    "eprivacy"
				compliance_level: 1.00
			},
			{
				standard_name:    "gdpr"
				compliance_level: 1.00
			},
			{
				standard_name:    "hipaa"
				compliance_level: 1.00
			},
			{
				standard_name:    "iso27001"
				compliance_level: 1.00
			},
			{
				standard_name:    "nerccip"
				compliance_level: 1.00
			},
			{
				standard_name:    "nist80053"
				compliance_level: 1.00
			},
			{
				standard_name:    "nist80063"
				compliance_level: 1.00
			},
			{
				standard_name:    "asvs"
				compliance_level: 1.00
			},
			{
				standard_name:    "owasp10"
				compliance_level: 1.00
			},
			{
				standard_name:    "pci"
				compliance_level: 1.00
			},
			{
				standard_name:    "soc2"
				compliance_level: 1.00
			},
			{
				standard_name:    "cwe25"
				compliance_level: 1.00
			},
			{
				standard_name:    "owaspm10"
				compliance_level: 1.00
			},
			{
				standard_name:    "nist"
				compliance_level: 1.00
			},
			{
				standard_name:    "agile"
				compliance_level: 1.00
			},
			{
				standard_name:    "bizec"
				compliance_level: 1.00
			},
			{
				standard_name:    "ccpa"
				compliance_level: 1.00
			},
			{
				standard_name:    "cpra"
				compliance_level: 1.00
			},
			{
				standard_name:    "certc"
				compliance_level: 1.00
			},
			{
				standard_name:    "certj"
				compliance_level: 1.00
			},
			{
				standard_name:    "fcra"
				compliance_level: 1.00
			},
			{
				standard_name:    "facta"
				compliance_level: 1.00
			},
			{
				standard_name:    "glba"
				compliance_level: 1.00
			},
			{
				standard_name:    "misrac"
				compliance_level: 1.00
			},
			{
				standard_name:    "nydfs"
				compliance_level: 1.00
			},
			{
				standard_name:    "nyshield"
				compliance_level: 1.00
			},
			{
				standard_name:    "mitre"
				compliance_level: 1.00
			},
			{
				standard_name:    "padss"
				compliance_level: 1.00
			},
			{
				standard_name:    "sans25"
				compliance_level: 1.00
			},
			{
				standard_name:    "pdpa"
				compliance_level: 1.00
			},
			{
				standard_name:    "popia"
				compliance_level: 1.00
			},
			{
				standard_name:    "pdpo"
				compliance_level: 1.00
			},
			{
				standard_name:    "cmmc"
				compliance_level: 1.00
			},
			{
				standard_name:    "hitrust"
				compliance_level: 1.00
			},
			{
				standard_name:    "fedramp"
				compliance_level: 1.00
			},
			{
				standard_name:    "iso27002"
				compliance_level: 1.00
			},
			{
				standard_name:    "lgpd"
				compliance_level: 1.00
			},
			{
				standard_name:    "iec62443"
				compliance_level: 1.00
			},
			{
				standard_name:    "wassec"
				compliance_level: 1.00
			},
			{
				standard_name:    "osstmm3"
				compliance_level: 1.00
			},
			{
				standard_name:    "c2m2"
				compliance_level: 1.00
			},
			{
				standard_name:    "wasc"
				compliance_level: 1.00
			},
			{
				standard_name:    "ferpa"
				compliance_level: 1.00
			},
			{
				standard_name:    "nistssdf"
				compliance_level: 1.00
			},
			{
				standard_name:    "issaf"
				compliance_level: 1.00
			},
			{
				standard_name:    "ptes"
				compliance_level: 1.00
			},
			{
				standard_name:    "owasprisks"
				compliance_level: 1.00
			},
			{
				standard_name:    "mvsp"
				compliance_level: 1.00
			},
			{
				standard_name:    "owaspscp"
				compliance_level: 1.00
			},
			{
				standard_name:    "bsafss"
				compliance_level: 1.00
			},
			{
				standard_name:    "owaspmasvs"
				compliance_level: 1.00
			},
			{
				standard_name:    "nist800171"
				compliance_level: 1.00
			},
			{
				standard_name:    "nist800115"
				compliance_level: 1.00
			},
			{
				standard_name:    "swiftcsc"
				compliance_level: 1.00
			},
			{
				standard_name:    "osamm"
				compliance_level: 1.00
			},
			{
				standard_name:    "siglite"
				compliance_level: 1.00
			},
			{
				standard_name:    "sig"
				compliance_level: 1.00
			},
		]

		compliance_level: 1.00
	},
	{
		compliance_weekly_trend:           0.00
		sk:                                "ORG#makoto#UNRELIABLEINDICATORS"
		pk:                                "ORG#ORG#d32674a9-9838-4337-b222-68c88bf54647#UNRELIABLEINDICATOR"
		estimated_days_to_full_compliance: 0.00
		standard_compliances:
		[
			{
				standard_name:    "bsimm"
				compliance_level: 1.00
			},
			{
				standard_name:    "capec"
				compliance_level: 1.00
			},
			{
				standard_name:    "cis"
				compliance_level: 1.00
			},
			{
				standard_name:    "cwe"
				compliance_level: 1.00
			},
			{
				standard_name:    "eprivacy"
				compliance_level: 1.00
			},
			{
				standard_name:    "gdpr"
				compliance_level: 1.00
			},
			{
				standard_name:    "hipaa"
				compliance_level: 1.00
			},
			{
				standard_name:    "iso27001"
				compliance_level: 1.00
			},
			{
				standard_name:    "nerccip"
				compliance_level: 1.00
			},
			{
				standard_name:    "nist80053"
				compliance_level: 1.00
			},
			{
				standard_name:    "nist80063"
				compliance_level: 1.00
			},
			{
				standard_name:    "asvs"
				compliance_level: 1.00
			},
			{
				standard_name:    "owasp10"
				compliance_level: 1.00
			},
			{
				standard_name:    "pci"
				compliance_level: 1.00
			},
			{
				standard_name:    "soc2"
				compliance_level: 1.00
			},
			{
				standard_name:    "cwe25"
				compliance_level: 1.00
			},
			{
				standard_name:    "owaspm10"
				compliance_level: 1.00
			},
			{
				standard_name:    "nist"
				compliance_level: 1.00
			},
			{
				standard_name:    "agile"
				compliance_level: 1.00
			},
			{
				standard_name:    "bizec"
				compliance_level: 1.00
			},
			{
				standard_name:    "ccpa"
				compliance_level: 1.00
			},
			{
				standard_name:    "cpra"
				compliance_level: 1.00
			},
			{
				standard_name:    "certc"
				compliance_level: 1.00
			},
			{
				standard_name:    "certj"
				compliance_level: 1.00
			},
			{
				standard_name:    "fcra"
				compliance_level: 1.00
			},
			{
				standard_name:    "facta"
				compliance_level: 1.00
			},
			{
				standard_name:    "glba"
				compliance_level: 1.00
			},
			{
				standard_name:    "misrac"
				compliance_level: 1.00
			},
			{
				standard_name:    "nydfs"
				compliance_level: 1.00
			},
			{
				standard_name:    "nyshield"
				compliance_level: 1.00
			},
			{
				standard_name:    "mitre"
				compliance_level: 1.00
			},
			{
				standard_name:    "padss"
				compliance_level: 1.00
			},
			{
				standard_name:    "sans25"
				compliance_level: 1.00
			},
			{
				standard_name:    "pdpa"
				compliance_level: 1.00
			},
			{
				standard_name:    "popia"
				compliance_level: 1.00
			},
			{
				standard_name:    "pdpo"
				compliance_level: 1.00
			},
			{
				standard_name:    "cmmc"
				compliance_level: 1.00
			},
			{
				standard_name:    "hitrust"
				compliance_level: 1.00
			},
			{
				standard_name:    "fedramp"
				compliance_level: 1.00
			},
			{
				standard_name:    "iso27002"
				compliance_level: 1.00
			},
			{
				standard_name:    "lgpd"
				compliance_level: 1.00
			},
			{
				standard_name:    "iec62443"
				compliance_level: 1.00
			},
			{
				standard_name:    "wassec"
				compliance_level: 1.00
			},
			{
				standard_name:    "osstmm3"
				compliance_level: 1.00
			},
			{
				standard_name:    "c2m2"
				compliance_level: 1.00
			},
			{
				standard_name:    "wasc"
				compliance_level: 1.00
			},
			{
				standard_name:    "ferpa"
				compliance_level: 1.00
			},
			{
				standard_name:    "nistssdf"
				compliance_level: 1.00
			},
			{
				standard_name:    "issaf"
				compliance_level: 1.00
			},
			{
				standard_name:    "ptes"
				compliance_level: 1.00
			},
			{
				standard_name:    "owasprisks"
				compliance_level: 1.00
			},
			{
				standard_name:    "mvsp"
				compliance_level: 1.00
			},
			{
				standard_name:    "owaspscp"
				compliance_level: 1.00
			},
			{
				standard_name:    "bsafss"
				compliance_level: 1.00
			},
			{
				standard_name:    "owaspmasvs"
				compliance_level: 1.00
			},
			{
				standard_name:    "nist800171"
				compliance_level: 1.00
			},
			{
				standard_name:    "nist800115"
				compliance_level: 1.00
			},
			{
				standard_name:    "swiftcsc"
				compliance_level: 1.00
			},
			{
				standard_name:    "osamm"
				compliance_level: 1.00
			},
			{
				standard_name:    "siglite"
				compliance_level: 1.00
			},
			{
				standard_name:    "sig"
				compliance_level: 1.00
			},
		]

		compliance_level: 1.00
	},
	{
		compliance_weekly_trend:           0.00
		sk:                                "ORG#okada#UNRELIABLEINDICATORS"
		pk:                                "ORG#ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3#UNRELIABLEINDICATOR"
		covered_authors:                   10
		has_ztna_roots:                    true
		estimated_days_to_full_compliance: 0.70
		standard_compliances:
		[
			{
				standard_name:    "bsimm"
				compliance_level: 1.00
			},
			{
				standard_name:    "capec"
				compliance_level: 0.71
			},
			{
				standard_name:    "cis"
				compliance_level: 0.93
			},
			{
				standard_name:    "cwe"
				compliance_level: 0.73
			},
			{
				standard_name:    "eprivacy"
				compliance_level: 0.80
			},
			{
				standard_name:    "gdpr"
				compliance_level: 0.87
			},
			{
				standard_name:    "hipaa"
				compliance_level: 1.00
			},
			{
				standard_name:    "iso27001"
				compliance_level: 0.95
			},
			{
				standard_name:    "nerccip"
				compliance_level: 0.94
			},
			{
				standard_name:    "nist80053"
				compliance_level: 1.00
			},
			{
				standard_name:    "nist80063"
				compliance_level: 0.89
			},
			{
				standard_name:    "asvs"
				compliance_level: 0.85
			},
			{
				standard_name:    "owasp10"
				compliance_level: 0.40
			},
			{
				standard_name:    "pci"
				compliance_level: 0.81
			},
			{
				standard_name:    "soc2"
				compliance_level: 0.92
			},
			{
				standard_name:    "cwe25"
				compliance_level: 0.45
			},
			{
				standard_name:    "owaspm10"
				compliance_level: 0.80
			},
			{
				standard_name:    "nist"
				compliance_level: 0.94
			},
			{
				standard_name:    "agile"
				compliance_level: 0.50
			},
			{
				standard_name:    "bizec"
				compliance_level: 0.38
			},
			{
				standard_name:    "ccpa"
				compliance_level: 1.00
			},
			{
				standard_name:    "cpra"
				compliance_level: 0.75
			},
			{
				standard_name:    "certc"
				compliance_level: 0.86
			},
			{
				standard_name:    "certj"
				compliance_level: 0.91
			},
			{
				standard_name:    "fcra"
				compliance_level: 1.00
			},
			{
				standard_name:    "facta"
				compliance_level: 1.00
			},
			{
				standard_name:    "glba"
				compliance_level: 0.67
			},
			{
				standard_name:    "misrac"
				compliance_level: 1.00
			},
			{
				standard_name:    "nydfs"
				compliance_level: 0.92
			},
			{
				standard_name:    "nyshield"
				compliance_level: 0.33
			},
			{
				standard_name:    "mitre"
				compliance_level: 0.79
			},
			{
				standard_name:    "padss"
				compliance_level: 0.79
			},
			{
				standard_name:    "sans25"
				compliance_level: 0.64
			},
			{
				standard_name:    "pdpa"
				compliance_level: 0.87
			},
			{
				standard_name:    "popia"
				compliance_level: 0.82
			},
			{
				standard_name:    "pdpo"
				compliance_level: 0.93
			},
			{
				standard_name:    "cmmc"
				compliance_level: 0.89
			},
			{
				standard_name:    "hitrust"
				compliance_level: 0.84
			},
			{
				standard_name:    "fedramp"
				compliance_level: 0.89
			},
			{
				standard_name:    "iso27002"
				compliance_level: 0.81
			},
			{
				standard_name:    "lgpd"
				compliance_level: 0.97
			},
			{
				standard_name:    "iec62443"
				compliance_level: 0.88
			},
			{
				standard_name:    "wassec"
				compliance_level: 0.54
			},
			{
				standard_name:    "osstmm3"
				compliance_level: 0.88
			},
			{
				standard_name:    "c2m2"
				compliance_level: 0.91
			},
			{
				standard_name:    "wasc"
				compliance_level: 0.40
			},
			{
				standard_name:    "ferpa"
				compliance_level: 0.50
			},
			{
				standard_name:    "nistssdf"
				compliance_level: 0.67
			},
			{
				standard_name:    "issaf"
				compliance_level: 0.67
			},
			{
				standard_name:    "ptes"
				compliance_level: 0.67
			},
			{
				standard_name:    "owasprisks"
				compliance_level: 0.62
			},
			{
				standard_name:    "mvsp"
				compliance_level: 0.55
			},
			{
				standard_name:    "owaspscp"
				compliance_level: 0.21
			},
			{
				standard_name:    "bsafss"
				compliance_level: 0.81
			},
			{
				standard_name:    "owaspmasvs"
				compliance_level: 0.89
			},
			{
				standard_name:    "nist800171"
				compliance_level: 0.97
			},
			{
				standard_name:    "nist800115"
				compliance_level: 0.92
			},
			{
				standard_name:    "swiftcsc"
				compliance_level: 0.79
			},
			{
				standard_name:    "osamm"
				compliance_level: 0.78
			},
			{
				standard_name:    "siglite"
				compliance_level: 0.90
			},
			{
				standard_name:    "sig"
				compliance_level: 0.93
			},
		]

		compliance_level: 0.80
	},
	{
		compliance_weekly_trend:           0.00
		sk:                                "ORG#tatsumi#UNRELIABLEINDICATORS"
		pk:                                "ORG#ORG#fe80d2d4-ccb7-46d1-8489-67c6360581de#UNRELIABLEINDICATOR"
		estimated_days_to_full_compliance: 0.00
		standard_compliances:
		[
			{
				standard_name:    "bsimm"
				compliance_level: 1.00
			},
			{
				standard_name:    "capec"
				compliance_level: 1.00
			},
			{
				standard_name:    "cis"
				compliance_level: 1.00
			},
			{
				standard_name:    "cwe"
				compliance_level: 1.00
			},
			{
				standard_name:    "eprivacy"
				compliance_level: 1.00
			},
			{
				standard_name:    "gdpr"
				compliance_level: 1.00
			},
			{
				standard_name:    "hipaa"
				compliance_level: 1.00
			},
			{
				standard_name:    "iso27001"
				compliance_level: 1.00
			},
			{
				standard_name:    "nerccip"
				compliance_level: 1.00
			},
			{
				standard_name:    "nist80053"
				compliance_level: 1.00
			},
			{
				standard_name:    "nist80063"
				compliance_level: 1.00
			},
			{
				standard_name:    "asvs"
				compliance_level: 1.00
			},
			{
				standard_name:    "owasp10"
				compliance_level: 1.00
			},
			{
				standard_name:    "pci"
				compliance_level: 1.00
			},
			{
				standard_name:    "soc2"
				compliance_level: 1.00
			},
			{
				standard_name:    "cwe25"
				compliance_level: 1.00
			},
			{
				standard_name:    "owaspm10"
				compliance_level: 1.00
			},
			{
				standard_name:    "nist"
				compliance_level: 1.00
			},
			{
				standard_name:    "agile"
				compliance_level: 1.00
			},
			{
				standard_name:    "bizec"
				compliance_level: 1.00
			},
			{
				standard_name:    "ccpa"
				compliance_level: 1.00
			},
			{
				standard_name:    "cpra"
				compliance_level: 1.00
			},
			{
				standard_name:    "certc"
				compliance_level: 1.00
			},
			{
				standard_name:    "certj"
				compliance_level: 1.00
			},
			{
				standard_name:    "fcra"
				compliance_level: 1.00
			},
			{
				standard_name:    "facta"
				compliance_level: 1.00
			},
			{
				standard_name:    "glba"
				compliance_level: 1.00
			},
			{
				standard_name:    "misrac"
				compliance_level: 1.00
			},
			{
				standard_name:    "nydfs"
				compliance_level: 1.00
			},
			{
				standard_name:    "nyshield"
				compliance_level: 1.00
			},
			{
				standard_name:    "mitre"
				compliance_level: 1.00
			},
			{
				standard_name:    "padss"
				compliance_level: 1.00
			},
			{
				standard_name:    "sans25"
				compliance_level: 1.00
			},
			{
				standard_name:    "pdpa"
				compliance_level: 1.00
			},
			{
				standard_name:    "popia"
				compliance_level: 1.00
			},
			{
				standard_name:    "pdpo"
				compliance_level: 1.00
			},
			{
				standard_name:    "cmmc"
				compliance_level: 1.00
			},
			{
				standard_name:    "hitrust"
				compliance_level: 1.00
			},
			{
				standard_name:    "fedramp"
				compliance_level: 1.00
			},
			{
				standard_name:    "iso27002"
				compliance_level: 1.00
			},
			{
				standard_name:    "lgpd"
				compliance_level: 1.00
			},
			{
				standard_name:    "iec62443"
				compliance_level: 1.00
			},
			{
				standard_name:    "wassec"
				compliance_level: 1.00
			},
			{
				standard_name:    "osstmm3"
				compliance_level: 1.00
			},
			{
				standard_name:    "c2m2"
				compliance_level: 1.00
			},
			{
				standard_name:    "wasc"
				compliance_level: 1.00
			},
			{
				standard_name:    "ferpa"
				compliance_level: 1.00
			},
			{
				standard_name:    "nistssdf"
				compliance_level: 1.00
			},
			{
				standard_name:    "issaf"
				compliance_level: 1.00
			},
			{
				standard_name:    "ptes"
				compliance_level: 1.00
			},
			{
				standard_name:    "owasprisks"
				compliance_level: 1.00
			},
			{
				standard_name:    "mvsp"
				compliance_level: 1.00
			},
			{
				standard_name:    "owaspscp"
				compliance_level: 1.00
			},
			{
				standard_name:    "bsafss"
				compliance_level: 1.00
			},
			{
				standard_name:    "owaspmasvs"
				compliance_level: 1.00
			},
			{
				standard_name:    "nist800171"
				compliance_level: 1.00
			},
			{
				standard_name:    "nist800115"
				compliance_level: 1.00
			},
			{
				standard_name:    "swiftcsc"
				compliance_level: 1.00
			},
			{
				standard_name:    "osamm"
				compliance_level: 1.00
			},
			{
				standard_name:    "siglite"
				compliance_level: 1.00
			},
			{
				standard_name:    "sig"
				compliance_level: 1.00
			},
		]

		compliance_level: 1.00
	},
] & [...#OrganizationUnreliableIndicatorsItem]
