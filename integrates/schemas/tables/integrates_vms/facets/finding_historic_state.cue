package finding_historic_state

import (
	"fluidattacks.com/types/findings"
)

#findingHistoricStatePk: =~"^FIN#[0-9]+$"
#findingHistoricStateSk: =~"^STATE#[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}[+-][0-9]{2}:[0-9]{2}$"

#FindingHistoricStateKeys: {
	pk: string & #findingHistoricStatePk
	sk: string & #findingHistoricStateSk
}

#FindingHistoricStateItem: {
	findings.#FindingState
	#FindingHistoricStateKeys
}

FindingHistoricState:
{
	FacetName: "finding_historic_state"
	KeyAttributeAlias: {
		PartitionKeyAlias: "FIN#id"
		SortKeyAlias:      "STATE#iso8601utc"
	}
	NonKeyAttributes: [
		"status",
		"modified_date",
		"modified_by",
		"source",
		"rejection",
		"justification",
	]
	DataAccess: {
		MySql: {}
	}
}

FindingHistoricState: TableData: [
	{
		sk:            "STATE#2018-04-08T00:43:18+00:00"
		modified_by:   "integratesmanager@gmail.com"
		pk:            "FIN#422286126"
		justification: "NO_JUSTIFICATION"
		source:        "ASM"
		modified_date: "2018-04-08T00:43:18+00:00"
		status:        "CREATED"
	},
	{
		sk:            "STATE#2018-04-08T00:43:18+00:00"
		modified_by:   "integratesmanager@gmail.com"
		pk:            "FIN#457497316"
		justification: "NO_JUSTIFICATION"
		source:        "ASM"
		modified_date: "2018-04-08T00:43:18+00:00"
		status:        "CREATED"
	},
	{
		sk:            "STATE#2018-04-08T00:43:18+00:00"
		modified_by:   "integratesmanager@gmail.com"
		pk:            "FIN#457497318"
		justification: "NO_JUSTIFICATION"
		source:        "ASM"
		modified_date: "2018-04-08T00:43:18+00:00"
		status:        "CREATED"
	},
	{
		sk:            "STATE#2018-04-08T00:43:18+00:00"
		modified_by:   "integratesmanager@gmail.com"
		pk:            "FIN#463461507"
		justification: "NO_JUSTIFICATION"
		source:        "ASM"
		modified_date: "2018-04-08T00:43:18+00:00"
		status:        "CREATED"
	},
	{
		sk:            "STATE#2018-04-08T00:43:18+00:00"
		modified_by:   "integratesmanager@gmail.com"
		pk:            "FIN#463558592"
		justification: "NO_JUSTIFICATION"
		source:        "ASM"
		modified_date: "2018-04-08T00:43:18+00:00"
		status:        "CREATED"
	},
	{
		sk:            "STATE#2023-10-18T14:38:14+00:00"
		modified_by:   "integratesmanager@gmail.com"
		pk:            "FIN#151213607"
		justification: "NO_JUSTIFICATION"
		source:        "ASM"
		modified_date: "2018-04-08T00:43:18+00:00"
		status:        "CREATED"
	},
	{
		sk:            "STATE#2018-04-08T00:43:18+00:00"
		modified_by:   "integratesmanager@gmail.com"
		pk:            "FIN#475041513"
		justification: "NO_JUSTIFICATION"
		source:        "ASM"
		modified_date: "2018-04-08T00:43:18+00:00"
		status:        "CREATED"
	},
	{
		sk:            "STATE#2018-04-08T00:43:18+00:00"
		modified_by:   "integratesmanager@gmail.com"
		pk:            "FIN#683100860"
		justification: "NO_JUSTIFICATION"
		source:        "ASM"
		modified_date: "2018-04-08T00:43:18+00:00"
		status:        "CREATED"
	},
	{
		sk:            "STATE#2018-04-08T00:43:18+00:00"
		modified_by:   "integratesmanager@gmail.com"
		pk:            "FIN#997868246"
		justification: "NO_JUSTIFICATION"
		source:        "ASM"
		modified_date: "2018-04-08T00:43:18+00:00"
		status:        "CREATED"
	},
	{
		sk:            "STATE#2018-04-08T00:45:11+00:00"
		modified_by:   "integratesmanager@gmail.com"
		pk:            "FIN#422286126"
		justification: "NO_JUSTIFICATION"
		source:        "ASM"
		modified_date: "2018-04-08T00:45:11+00:00"
		status:        "CREATED"
	},
	{
		sk:            "STATE#2018-04-08T00:45:11+00:00"
		modified_by:   "integratesmanager@gmail.com"
		pk:            "FIN#457497316"
		justification: "NO_JUSTIFICATION"
		source:        "ASM"
		modified_date: "2018-04-08T00:45:11+00:00"
		status:        "CREATED"
	},
	{
		sk:            "STATE#2018-04-08T00:45:11+00:00"
		modified_by:   "integratesmanager@gmail.com"
		pk:            "FIN#457497318"
		justification: "NO_JUSTIFICATION"
		source:        "ASM"
		modified_date: "2018-04-08T00:45:11+00:00"
		status:        "CREATED"
	},
	{
		sk:            "STATE#2018-04-08T00:45:11+00:00"
		modified_by:   "integratesmanager@gmail.com"
		pk:            "FIN#463461507"
		justification: "NO_JUSTIFICATION"
		source:        "ASM"
		modified_date: "2018-04-08T00:45:11+00:00"
		status:        "CREATED"
	},
	{
		sk:            "STATE#2018-04-08T00:45:11+00:00"
		modified_by:   "integratesmanager@gmail.com"
		pk:            "FIN#463558592"
		justification: "NO_JUSTIFICATION"
		source:        "ASM"
		modified_date: "2018-04-08T00:45:11+00:00"
		status:        "CREATED"
	},
	{
		sk:            "STATE#2018-04-08T00:45:11+00:00"
		modified_by:   "integratesmanager@gmail.com"
		pk:            "FIN#475041513"
		justification: "NO_JUSTIFICATION"
		source:        "ASM"
		modified_date: "2018-04-08T00:45:11+00:00"
		status:        "CREATED"
	},
	{
		sk:            "STATE#2018-05-02T00:45:11+00:00"
		modified_by:   "integratesmanager@gmail.com"
		pk:            "FIN#683100860"
		justification: "NO_JUSTIFICATION"
		source:        "ASM"
		modified_date: "2018-05-02T00:45:11+00:00"
		status:        "CREATED"
	},
	{
		sk:            "STATE#2018-05-02T00:45:11+00:00"
		modified_by:   "integratesmanager@gmail.com"
		pk:            "FIN#997868246"
		justification: "NO_JUSTIFICATION"
		source:        "ASM"
		modified_date: "2018-05-02T00:45:11+00:00"
		status:        "CREATED"
	},
	{
		sk:            "STATE#2018-05-02T01:45:11+00:00"
		modified_by:   "integratesmanager@gmail.com"
		pk:            "FIN#997868246"
		justification: "NO_JUSTIFICATION"
		source:        "ASM"
		modified_date: "2018-05-02T01:45:11+00:00"
		status:        "CREATED"
	},
	{
		sk:            "STATE#2018-07-09T05:00:00+00:00"
		modified_by:   "integratesmanager@gmail.com"
		pk:            "FIN#422286126"
		justification: "NO_JUSTIFICATION"
		source:        "ASM"
		modified_date: "2018-07-09T05:00:00+00:00"
		status:        "CREATED"
	},
	{
		sk:            "STATE#2018-11-27T05:00:00+00:00"
		modified_by:   "integratesmanager@gmail.com"
		pk:            "FIN#457497316"
		justification: "NO_JUSTIFICATION"
		source:        "ASM"
		modified_date: "2018-11-27T05:00:00+00:00"
		status:        "CREATED"
	},
	{
		sk:            "STATE#2018-11-29T05:00:00+00:00"
		modified_by:   "integratesmanager@gmail.com"
		pk:            "FIN#457497318"
		justification: "NO_JUSTIFICATION"
		source:        "ASM"
		modified_date: "2018-11-29T05:00:00+00:00"
		status:        "CREATED"
	},
	{
		sk:            "STATE#2018-12-17T05:00:00+00:00"
		modified_by:   "integratesmanager@gmail.com"
		pk:            "FIN#463558592"
		justification: "NO_JUSTIFICATION"
		source:        "ASM"
		modified_date: "2018-12-17T05:00:00+00:00"
		status:        "CREATED"
	},
	{
		sk:            "STATE#2019-01-10T05:00:00+00:00"
		modified_by:   "integratesmanager@gmail.com"
		pk:            "FIN#463461507"
		justification: "NO_JUSTIFICATION"
		source:        "ASM"
		modified_date: "2019-01-10T05:00:00+00:00"
		status:        "CREATED"
	},
	{
		sk:            "STATE#2019-02-04T17:46:10+00:00"
		modified_by:   "unittest@fluidattacks.com"
		pk:            "FIN#475041524"
		justification: "NO_JUSTIFICATION"
		source:        "ASM"
		modified_date: "2019-02-04T17:46:10+00:00"
		status:        "CREATED"
	},
	{
		sk:            "STATE#2019-02-04T17:46:10+00:00"
		modified_by:   "unittest@fluidattacks.com"
		pk:            "FIN#475041535"
		justification: "NO_JUSTIFICATION"
		source:        "ASM"
		modified_date: "2019-02-04T17:46:10+00:00"
		status:        "CREATED"
	},
	{
		sk:            "STATE#2019-02-04T17:46:10+00:00"
		modified_by:   "unittest@fluidattacks.com"
		pk:            "FIN#011011011"
		justification: "NO_JUSTIFICATION"
		source:        "ASM"
		modified_date: "2019-02-04T17:46:10+00:00"
		status:        "DELETED"
	},
	{
		sk:            "STATE#2019-02-04T17:46:10+00:00"
		modified_by:   "unittest@fluidattacks.com"
		pk:            "FIN#011011012"
		justification: "NO_JUSTIFICATION"
		source:        "MACHINE"
		modified_date: "2019-02-04T17:46:10+00:00"
		status:        "CREATED"
	},
	{
		sk:            "STATE#2019-02-04T17:46:10+00:00"
		modified_by:   "unittest@fluidattacks.com"
		pk:            "FIN#560175507"
		justification: "NO_JUSTIFICATION"
		source:        "ASM"
		modified_date: "2019-02-04T17:46:10+00:00"
		status:        "CREATED"
	},
	{
		sk:            "STATE#2019-02-05T17:46:10+00:00"
		modified_by:   "unittest@fluidattacks.com"
		pk:            "FIN#560175507"
		justification: "NO_JUSTIFICATION"
		source:        "ASM"
		modified_date: "2019-02-05T17:46:10+00:00"
		status:        "CREATED"
	},
	{
		sk:            "STATE#2019-02-05T17:46:10+00:00"
		modified_by:   "unittest@fluidattacks.com"
		pk:            "FIN#011011011"
		justification: "NO_JUSTIFICATION"
		source:        "ASM"
		modified_date: "2019-02-05T17:46:10+00:00"
		status:        "CREATED"
	},
	{
		sk:            "STATE#2019-02-07T17:46:10+00:00"
		modified_by:   "unittest@fluidattacks.com"
		pk:            "FIN#560175507"
		justification: "NO_JUSTIFICATION"
		rejection: {
			other: ""
			reasons: {SS: ["OMISSION"]}
			rejected_by:    "customer_manager@fluidattacks.com"
			rejection_date: "2019-02-07T17:46:10+00:00"
			submitted_by:   "unittest@fluidattacks.com"
		}
		source:        "ASM"
		modified_date: "2019-02-07T17:46:10+00:00"
		status:        "DELETED"
	},
	{
		sk:            "STATE#2019-02-07T17:46:10+00:00"
		modified_by:   "unittest@fluidattacks.com"
		pk:            "FIN#011011"
		justification: "NO_JUSTIFICATION"
		rejection: {
			other: ""
			reasons: {SS: ["OMISSION"]}
			rejected_by:    "customer_manager@fluidattacks.com"
			rejection_date: "2019-02-07T17:46:10+00:00"
			submitted_by:   "unittest@fluidattacks.com"
		}
		source:        "ASM"
		modified_date: "2019-02-07T17:46:10+00:00"
		status:        "ACCEPTED"
	},
	{
		sk:            "STATE#2022-08-22T17:46:10+00:00"
		modified_by:   "unittest@fluidattacks.com"
		pk:            "FIN#563827909"
		justification: "NO_JUSTIFICATION"
		source:        "ASM"
		modified_date: "2022-08-22T17:46:10+00:00"
		status:        "CREATED"
	},
	{
		sk:            "STATE#2019-04-08T00:43:18+00:00"
		modified_by:   "integratesmanager@gmail.com"
		pk:            "FIN#436992569"
		justification: "NO_JUSTIFICATION"
		source:        "ASM"
		modified_date: "2019-04-08T00:43:18+00:00"
		status:        "CREATED"
	},
	{
		sk:            "STATE#2019-04-08T00:43:18+00:00"
		modified_by:   "integratesmanager@gmail.com"
		pk:            "FIN#988493279"
		justification: "NO_JUSTIFICATION"
		source:        "ASM"
		modified_date: "2019-04-08T00:43:18+00:00"
		status:        "CREATED"
	},
	{
		sk:            "STATE#2019-04-08T00:45:11+00:00"
		modified_by:   "integratesmanager@gmail.com"
		pk:            "FIN#436992569"
		justification: "NO_JUSTIFICATION"
		source:        "ASM"
		modified_date: "2019-04-08T00:45:11+00:00"
		status:        "CREATED"
	},
	{
		sk:            "STATE#2019-04-08T00:45:11+00:00"
		modified_by:   "integratesmanager@gmail.com"
		pk:            "FIN#988493279"
		justification: "NO_JUSTIFICATION"
		source:        "ASM"
		modified_date: "2019-04-08T00:45:11+00:00"
		status:        "CREATED"
	},
	{
		sk:            "STATE#2019-04-08T00:45:15+00:00"
		modified_by:   "integratesmanager@gmail.com"
		pk:            "FIN#988493279"
		justification: "NO_JUSTIFICATION"
		source:        "ASM"
		modified_date: "2019-04-08T00:45:15+00:00"
		status:        "CREATED"
	},
	{
		sk:            "STATE#2019-04-08T05:00:00+00:00"
		modified_by:   "integratesmanager@gmail.com"
		pk:            "FIN#436992569"
		justification: "NO_JUSTIFICATION"
		source:        "ASM"
		modified_date: "2019-04-08T05:00:00+00:00"
		status:        "CREATED"
	},
	{
		sk:            "STATE#2019-11-22T20:07:57+00:00"
		modified_by:   "integratesmanager@gmail.com"
		pk:            "FIN#818828206"
		justification: "NO_JUSTIFICATION"
		source:        "ASM"
		modified_date: "2019-11-22T20:07:57+00:00"
		status:        "CREATED"
	},
	{
		sk:            "STATE#2019-11-22T20:08:03+00:00"
		modified_by:   "integratesmanager@gmail.com"
		pk:            "FIN#991607942"
		justification: "NO_JUSTIFICATION"
		source:        "ASM"
		modified_date: "2019-11-22T20:08:03+00:00"
		status:        "CREATED"
	},
	{
		sk:            "STATE#2019-11-22T20:08:11+00:00"
		modified_by:   "integratesmanager@gmail.com"
		pk:            "FIN#836530833"
		justification: "NO_JUSTIFICATION"
		source:        "ASM"
		modified_date: "2019-11-22T20:08:11+00:00"
		status:        "CREATED"
	},
	{
		sk:            "STATE#2019-11-22T20:10:04+00:00"
		modified_by:   "integratesmanager@gmail.com"
		pk:            "FIN#836530833"
		justification: "NO_JUSTIFICATION"
		source:        "ASM"
		modified_date: "2019-11-22T20:10:04+00:00"
		status:        "CREATED"
	},
	{
		sk:            "STATE#2019-11-22T20:10:56+00:00"
		modified_by:   "integratesmanager@gmail.com"
		pk:            "FIN#991607942"
		justification: "DUPLICATED"
		source:        "ASM"
		modified_date: "2019-11-22T20:10:56+00:00"
		status:        "DELETED"
	},
	{
		sk:            "STATE#2019-11-23T00:45:11+00:00"
		modified_by:   "integratesmanager@gmail.com"
		pk:            "FIN#818828206"
		justification: "NO_JUSTIFICATION"
		source:        "ASM"
		modified_date: "2019-11-23T00:45:11+00:00"
		status:        "CREATED"
	},
	{
		sk:            "STATE#2019-11-24T05:00:00+00:00"
		modified_by:   "integratesmanager@gmail.com"
		pk:            "FIN#818828206"
		justification: "NO_JUSTIFICATION"
		source:        "ASM"
		modified_date: "2019-11-24T05:00:00+00:00"
		status:        "CREATED"
	},
	{
		sk:            "STATE#2020-04-03T17:43:18+00:00"
		modified_by:   "integratesmanager@gmail.com"
		pk:            "FIN#436712859"
		justification: "NO_JUSTIFICATION"
		source:        "ASM"
		modified_date: "2020-04-03T17:43:18+00:00"
		status:        "CREATED"
	},
	{
		sk:            "STATE#2020-08-02T00:45:11+00:00"
		modified_by:   "integratesmanager@gmail.com"
		pk:            "FIN#436712859"
		justification: "NO_JUSTIFICATION"
		source:        "ASM"
		modified_date: "2020-08-02T00:45:11+00:00"
		status:        "CREATED"
	},
	{
		sk:            "STATE#2020-08-02T01:45:11+00:00"
		modified_by:   "integratesmanager@gmail.com"
		pk:            "FIN#436712859"
		justification: "NO_JUSTIFICATION"
		source:        "ASM"
		modified_date: "2020-08-02T01:45:11+00:00"
		status:        "CREATED"
	},
] & [...#FindingHistoricStateItem]
