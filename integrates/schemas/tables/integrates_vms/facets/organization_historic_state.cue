package organization_historic_state

import (
	"fluidattacks.com/types/organizations"
)

#organizationHistoricStatePk: =~"^ORG#[a-f0-9-]{36}$"
#organizationHistoricStateSk: =~"^STATE#(\\d{4})-(\\d{2})-(\\d{2})(T(\\d{2}):(\\d{2}):(\\d{2})(\\.\\d+)?(Z|([+-])(\\d{2}):(\\d{2})))?$"

#OrganizationHistoricStateKeys: {
	pk: string & #organizationHistoricStatePk
	sk: string & #organizationHistoricStateSk
}

#OrganizationHistoricStateItem: {
	#OrganizationHistoricStateKeys
	organizations.#OrganizationState
}

OrganizationHistoricState:
{
	FacetName: "organization_historic_state"
	KeyAttributeAlias: {
		PartitionKeyAlias: "ORG#id"
		SortKeyAlias:      "STATE#iso8601utc"
	}
	NonKeyAttributes: [
		"status",
		"modified_date",
		"modified_by",
		"pending_deletion_date",
		"aws_external_id",
	]
	DataAccess: {
		MySql: {}
	}
}

OrganizationHistoricState: TableData: [
	{
		sk:              "STATE#2019-11-22T20:07:57+00:00"
		modified_by:     "unknown"
		aws_external_id: "081637b6-c2c1-4568-a72e-e7d16e131cd6"
		pk:              "ORG#33c08ebd-2068-47e7-9673-e1aa03dc9448"
		modified_date:   "2019-11-22T20:07:57+00:00"
		status:          "ACTIVE"
	},
	{
		sk:                    "STATE#2019-11-22T20:07:57+00:00"
		modified_by:           "unknown"
		pk:                    "ORG#7376c5fe-4634-4053-9718-e14ecbda1e6b"
		aws_external_id:       "881a55a3-59ee-4edc-8f56-33b3958dd110"
		modified_date:         "2019-11-22T20:07:57+00:00"
		status:                "ACTIVE"
		pending_deletion_date: "2019-11-22T20:07:57+00:00"
	},
	{
		sk:              "STATE#2019-11-22T20:07:57+00:00"
		modified_by:     "unknown"
		aws_external_id: "c8098ffe-fb25-440f-aaa4-176522b54c36"
		pk:              "ORG#c6cecc0e-bb92-4079-8b6d-c4e815c10bb1"
		modified_date:   "2019-11-22T20:07:57+00:00"
		status:          "ACTIVE"
	},
	{
		sk:              "STATE#2019-11-22T20:07:57+00:00"
		modified_by:     "unknown"
		aws_external_id: "872a9ed6-23d9-4909-a8d1-bdb0e69c9d49"
		pk:              "ORG#ffddc7a3-7f05-4fc7-b65d-7defffa883c2"
		modified_date:   "2019-11-22T20:07:57+00:00"
		status:          "ACTIVE"
	},
	{
		sk:                    "STATE#2019-11-22T20:07:57+00:00"
		modified_by:           "unknown"
		aws_external_id:       "229b9cc0-6281-4a74-a6f3-556263840dd8"
		pk:                    "ORG#f2e2777d-a168-4bea-93cd-d79142b294d2"
		modified_date:         "2019-11-22T20:07:57+00:00"
		status:                "ACTIVE"
		pending_deletion_date: "2019-11-22T20:07:57+00:00"
	},
	{
		sk:                    "STATE#2019-11-22T20:07:57+00:00"
		modified_by:           "unknown"
		aws_external_id:       "9fc30c82-1fb7-42d5-b9af-2dc7fb385c13"
		pk:                    "ORG#c2ee2d15-04ab-4f39-9795-fbe30cdeee86"
		modified_date:         "2019-11-22T20:07:57+00:00"
		status:                "ACTIVE"
		pending_deletion_date: "2019-11-22T20:07:57+00:00"
	},
	{
		sk:              "STATE#2019-11-30T15:00:00+00:00"
		modified_by:     "integratesmanager@gmail.com"
		aws_external_id: "a6321e94-6f8b-4d0c-88da-569a9ac02201"
		pk:              "ORG#ed6f051c-2572-420f-bc11-476c4e71b4ee"
		modified_date:   "2019-11-30T15:00:00+00:00"
		status:          "DELETED"
	},
	{
		sk:              "STATE#2019-11-22T20:07:57+00:00"
		modified_by:     "unknown"
		aws_external_id: "dbf08773-21b4-4c09-8e86-41ec84b501ef"
		pk:              "ORG#956e9107-fd8d-49bc-b550-5609a7a1f6ac"
		modified_date:   "2019-11-22T20:07:57+00:00"
		status:          "ACTIVE"
	},
	{
		sk:              "STATE#2019-11-22T20:07:57+00:00"
		modified_by:     "unknown"
		aws_external_id: "71791c9e-cfff-471b-a930-23760b72e35d"
		pk:              "ORG#fe80d2d4-ccb7-46d1-8489-67c6360581de"
		modified_date:   "2019-11-22T20:07:57+00:00"
		status:          "ACTIVE"
	},
	{
		sk:                    "STATE#2018-02-08T00:43:18+00:00"
		modified_by:           "unknown"
		aws_external_id:       "ec3d49b3-804d-4402-9771-61079eba47fe"
		pk:                    "ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3"
		modified_date:         "2018-02-08T00:43:18+00:00"
		status:                "ACTIVE"
		pending_deletion_date: "2019-11-22T20:07:57+00:00"
	},
	{
		sk:                    "STATE#2019-11-22T20:07:57+00:00"
		modified_by:           "unknown"
		aws_external_id:       "4a549c5f-1b77-4deb-9525-0fd9615cc509"
		pk:                    "ORG#d32674a9-9838-4337-b222-68c88bf54647"
		modified_date:         "2019-11-22T20:07:57+00:00"
		status:                "ACTIVE"
		pending_deletion_date: "2019-11-22T20:07:57+00:00"
	},
	{
		sk:              "STATE#2024-07-16T20:26:54.966549+00:00"
		modified_by:     "integratesmanager@gmail.com"
		pk:              "ORG#5f727061-f608-4ff3-97c0-df9056011e2c"
		aws_external_id: "392c3cb7-4f43-41fb-ba68-bdfc58ba432f"
		modified_date:   "2024-07-16T20:26:54.966549+00:00"
		status:          "ACTIVE"
	},
] & [...#OrganizationHistoricStateItem]
