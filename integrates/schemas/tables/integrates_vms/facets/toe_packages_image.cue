package toe_packages_image

#ToePackagesImageItem: {
	pk: string & =~"^PKGS#ROOT#[a-f0-9-]{36}#PACKAGE#[a-z0-9-:.]+#VERSION#[a-z0-9.-]+$"
	sk: string & =~"^URI#[a-z0-9:./-]+$"
}

ToePackagesImage:
{
	FacetName: "toe_packages_image"
	KeyAttributeAlias: {
		PartitionKeyAlias: "PKGS#ROOT#root_id#PACKAGE#name#VERSION#version"
		SortKeyAlias:      "URI#uri"
	}
	TableData: [
		{
			sk: "URI#docker.io/mongo:latest"
			pk: "PKGS#ROOT#4039d098-ffc5-4984-8ed3-eb17bca98e19#PACKAGE#com.nimbusds:nimbus-jose-jwt#VERSION#8.3"
		},
	] & [...#ToePackagesImageItem]
	NonKeyAttributes: []
	DataAccess: {
		MySql: {}
	}
}
