package mailmap_entry

import (
	"fluidattacks.com/types/mailmap"
)

#mailmapEntryPk: =~"^ORG#[a-z0-9-]+#MAILMAP_ENTRY#[a-z0-9.@-]+"
#mailmapEntrySk: =~"^MAILMAP_ENTRY#[a-zA-Z0-9 ]+"

#MailmapEntryKeys: {
	pk:   string & #mailmapEntryPk
	sk:   string & #mailmapEntrySk
	pk_2: string & =~"^ORG#[a-z0-9-]+#MAILMAP_ENTRY#all"
	sk_2: string & #mailmapEntryPk
}

#MailmapEntryItem: {
	#MailmapEntryKeys
	mailmap.#MailmapEntry
}

MailmapEntry:
{
	FacetName: "mailmap_entry"
	KeyAttributeAlias: {
		PartitionKeyAlias: "ORG#organization_id#MAILMAP_ENTRY#entry_email"
		SortKeyAlias:      "MAILMAP_ENTRY#entry_name"
	}
	NonKeyAttributes: [
		"mailmap_entry_name",
		"mailmap_entry_email",
		"mailmap_entry_created_at",
		"mailmap_entry_updated_at",
		"sk_2",
		"pk_2",
	]
	DataAccess: {
		MySql: {}
	}
}

MailmapEntry: TableData: [
	{
		mailmap_entry_name:  "Alice Garcia"
		mailmap_entry_email: "alice@entry.com"
		pk:                  "ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3#MAILMAP_ENTRY#alice@entry.com"
		sk:                  "MAILMAP_ENTRY#Alice Garcia"
		pk_2:                "ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3#MAILMAP_ENTRY#all"
		sk_2:                "ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3#MAILMAP_ENTRY#alice@entry.com"
	},
	{
		mailmap_entry_name:  "Bob Singh"
		mailmap_entry_email: "bob@entry.com"
		pk:                  "ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3#MAILMAP_ENTRY#bob@entry.com"
		sk:                  "MAILMAP_ENTRY#Bob Singh"
		pk_2:                "ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3#MAILMAP_ENTRY#all"
		sk_2:                "ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3#MAILMAP_ENTRY#bob@entry.com"
	},
	{
		mailmap_entry_name:  "Charlie Nguyen"
		mailmap_entry_email: "charlie@entry.com"
		pk:                  "ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3#MAILMAP_ENTRY#charlie@entry.com"
		sk:                  "MAILMAP_ENTRY#Charlie Nguyen"
		pk_2:                "ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3#MAILMAP_ENTRY#all"
		sk_2:                "ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3#MAILMAP_ENTRY#charlie@entry.com"
	},
	{
		mailmap_entry_name:  "Eve Anderson"
		mailmap_entry_email: "eve@entry.com"
		pk:                  "ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3#MAILMAP_ENTRY#eve@entry.com"
		sk:                  "MAILMAP_ENTRY#Eve Anderson"
		pk_2:                "ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3#MAILMAP_ENTRY#all"
		sk_2:                "ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3#MAILMAP_ENTRY#eve@entry.com"
	},
	{
		mailmap_entry_name:  "Mallory Santos"
		mailmap_entry_email: "mallory@entry.com"
		pk:                  "ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3#MAILMAP_ENTRY#mallory@entry.com"
		sk:                  "MAILMAP_ENTRY#Mallory Santos"
		pk_2:                "ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3#MAILMAP_ENTRY#all"
		sk_2:                "ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3#MAILMAP_ENTRY#mallory@entry.com"
	},
	{
		mailmap_entry_name:  "Trent Hernandez"
		mailmap_entry_email: "trent@entry.com"
		pk:                  "ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3#MAILMAP_ENTRY#trent@entry.com"
		sk:                  "MAILMAP_ENTRY#Trent Hernandez"
		pk_2:                "ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3#MAILMAP_ENTRY#all"
		sk_2:                "ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3#MAILMAP_ENTRY#trent@entry.com"
	},
	{
		mailmap_entry_name:  "Grace Musa"
		mailmap_entry_email: "grace@entry.com"
		pk:                  "ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3#MAILMAP_ENTRY#grace@entry.com"
		sk:                  "MAILMAP_ENTRY#Grace Musa"
		pk_2:                "ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3#MAILMAP_ENTRY#all"
		sk_2:                "ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3#MAILMAP_ENTRY#grace@entry.com"
	},
	{
		mailmap_entry_name:  "Oscar Schmidt"
		mailmap_entry_email: "oscar@entry.com"
		pk:                  "ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3#MAILMAP_ENTRY#oscar@entry.com"
		sk:                  "MAILMAP_ENTRY#Oscar Schmidt"
		pk_2:                "ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3#MAILMAP_ENTRY#all"
		sk_2:                "ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3#MAILMAP_ENTRY#oscar@entry.com"
	},
	{
		mailmap_entry_name:  "Wendy Ozcan"
		mailmap_entry_email: "wendy@entry.com"
		pk:                  "ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3#MAILMAP_ENTRY#wendy@entry.com"
		sk:                  "MAILMAP_ENTRY#Wendy Ozcan"
		pk_2:                "ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3#MAILMAP_ENTRY#all"
		sk_2:                "ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3#MAILMAP_ENTRY#wendy@entry.com"
	},
] & [...#MailmapEntryItem]
