package url_root_metadata

import (
	"fluidattacks.com/types/roots"
)

#urlRootMetadataPk: =~"^ROOT#[a-f0-9-]{36}$"
#urlRootMetadataSk: =~"^GROUP#[a-z]+$"

#UrlRootMetadataKeys: {
	pk:   string & #urlRootMetadataPk
	sk:   string & #urlRootMetadataSk
	pk_2: string
	sk_2: string
}

#UrlRootItem: {
	#UrlRootMetadataKeys
	roots.#UrlRootMetadata
}

UrlRootMetadata:
{
	FacetName: "url_root_metadata"
	KeyAttributeAlias: {
		PartitionKeyAlias: "ROOT#uuid"
		SortKeyAlias:      "GROUP#name"
	}
	NonKeyAttributes: [
		"type",
		"created_date",
		"pk_2",
		"sk_2",
		"state",
		"unreliable_indicators",
		"created_by",
	]
	DataAccess: {
		MySql: {}
	}
}

UrlRootMetadata: TableData: [
	{
		sk:           "GROUP#oneshottest"
		pk_2:         "ORG#okada"
		created_date: "2020-11-19T13:45:55+00:00"
		pk:           "ROOT#8493c82f-2860-4902-86fa-75b0fef76034"
		state: {
			path:          "/"
			protocol:      "HTTPS"
			port:          "443"
			modified_by:   "jdoe@fluidattacks.com"
			nickname:      "url_root_1"
			host:          "app.fluidattacks.com"
			modified_date: "2020-11-19T13:45:55+00:00"
			status:        "ACTIVE"
		}
		type:       "URL"
		created_by: "jdoe@fluidattacks.com"
		sk_2:       "ROOT#8493c82f-2860-4902-86fa-75b0fef76034"
	},
] & [...#UrlRootItem]
