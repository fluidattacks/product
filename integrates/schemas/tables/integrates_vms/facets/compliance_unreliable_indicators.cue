package compliance_unreliable_indicators

import (
	"fluidattacks.com/types/compliance"
)

#ComplianceUnreliableIndicatorsKeys: {
	sk: "COMPLIANCE#UNRELIABLEINDICATOR"
	pk: "COMPLIANCE#UNRELIABLEINDICATOR"
}

#ComplianceUnreliableIndicatorsItem: {
	compliance.#ComplianceUnreliableIndicators
	#ComplianceUnreliableIndicatorsKeys
}

ComplianceUnreliableIndicators:
{
	FacetName: "compliance_unreliable_indicators"
	KeyAttributeAlias: {
		PartitionKeyAlias: "COMPLIANCE#UNRELIABLEINDICATOR"
		SortKeyAlias:      "COMPLIANCE#UNRELIABLEINDICATOR"
	}
	NonKeyAttributes: [
		"standards",
	]
	DataAccess: {
		MySql: {}
	}
}

ComplianceUnreliableIndicators: TableData: [
	{
		standards:
		[
			{
				worst_organization_compliance_level: 1.00
				standard_name:                       "bsimm"
				avg_organization_compliance_level:   1.00
				best_organization_compliance_level:  1.00
			},
			{
				worst_organization_compliance_level: 0.71
				standard_name:                       "capec"
				avg_organization_compliance_level:   0.86
				best_organization_compliance_level:  1.00
			},
			{
				worst_organization_compliance_level: 0.93
				standard_name:                       "cis"
				avg_organization_compliance_level:   0.96
				best_organization_compliance_level:  1.00
			},
			{
				worst_organization_compliance_level: 0.73
				standard_name:                       "cwe"
				avg_organization_compliance_level:   0.86
				best_organization_compliance_level:  1.00
			},
			{
				worst_organization_compliance_level: 0.80
				standard_name:                       "eprivacy"
				avg_organization_compliance_level:   0.90
				best_organization_compliance_level:  1.00
			},
			{
				worst_organization_compliance_level: 0.87
				standard_name:                       "gdpr"
				avg_organization_compliance_level:   0.94
				best_organization_compliance_level:  1.00
			},
			{
				worst_organization_compliance_level: 1.00
				standard_name:                       "hipaa"
				avg_organization_compliance_level:   1.00
				best_organization_compliance_level:  1.00
			},
			{
				worst_organization_compliance_level: 0.95
				standard_name:                       "iso27001"
				avg_organization_compliance_level:   0.98
				best_organization_compliance_level:  1.00
			},
			{
				worst_organization_compliance_level: 0.94
				standard_name:                       "nerccip"
				avg_organization_compliance_level:   0.97
				best_organization_compliance_level:  1.00
			},
			{
				worst_organization_compliance_level: 1.00
				standard_name:                       "nist80053"
				avg_organization_compliance_level:   1.00
				best_organization_compliance_level:  1.00
			},
			{
				worst_organization_compliance_level: 0.89
				standard_name:                       "nist80063"
				avg_organization_compliance_level:   0.94
				best_organization_compliance_level:  1.00
			},
			{
				worst_organization_compliance_level: 0.85
				standard_name:                       "asvs"
				avg_organization_compliance_level:   0.92
				best_organization_compliance_level:  1.00
			},
			{
				worst_organization_compliance_level: 0.40
				standard_name:                       "owasp10"
				avg_organization_compliance_level:   0.70
				best_organization_compliance_level:  1.00
			},
			{
				worst_organization_compliance_level: 0.81
				standard_name:                       "pci"
				avg_organization_compliance_level:   0.90
				best_organization_compliance_level:  1.00
			},
			{
				worst_organization_compliance_level: 0.92
				standard_name:                       "soc2"
				avg_organization_compliance_level:   0.96
				best_organization_compliance_level:  1.00
			},
			{
				worst_organization_compliance_level: 0.45
				standard_name:                       "cwe25"
				avg_organization_compliance_level:   0.72
				best_organization_compliance_level:  1.00
			},
			{
				worst_organization_compliance_level: 0.80
				standard_name:                       "owaspm10"
				avg_organization_compliance_level:   0.90
				best_organization_compliance_level:  1.00
			},
			{
				worst_organization_compliance_level: 0.94
				standard_name:                       "nist"
				avg_organization_compliance_level:   0.97
				best_organization_compliance_level:  1.00
			},
			{
				worst_organization_compliance_level: 0.50
				standard_name:                       "agile"
				avg_organization_compliance_level:   0.75
				best_organization_compliance_level:  1.00
			},
			{
				worst_organization_compliance_level: 0.38
				standard_name:                       "bizec"
				avg_organization_compliance_level:   0.69
				best_organization_compliance_level:  1.00
			},
			{
				worst_organization_compliance_level: 1.00
				standard_name:                       "ccpa"
				avg_organization_compliance_level:   1.00
				best_organization_compliance_level:  1.00
			},
			{
				worst_organization_compliance_level: 0.75
				standard_name:                       "cpra"
				avg_organization_compliance_level:   0.88
				best_organization_compliance_level:  1.00
			},
			{
				worst_organization_compliance_level: 0.86
				standard_name:                       "certc"
				avg_organization_compliance_level:   0.93
				best_organization_compliance_level:  1.00
			},
			{
				worst_organization_compliance_level: 0.91
				standard_name:                       "certj"
				avg_organization_compliance_level:   0.96
				best_organization_compliance_level:  1.00
			},
			{
				worst_organization_compliance_level: 1.00
				standard_name:                       "fcra"
				avg_organization_compliance_level:   1.00
				best_organization_compliance_level:  1.00
			},
			{
				worst_organization_compliance_level: 1.00
				standard_name:                       "facta"
				avg_organization_compliance_level:   1.00
				best_organization_compliance_level:  1.00
			},
			{
				worst_organization_compliance_level: 0.67
				standard_name:                       "glba"
				avg_organization_compliance_level:   0.84
				best_organization_compliance_level:  1.00
			},
			{
				worst_organization_compliance_level: 1.00
				standard_name:                       "misrac"
				avg_organization_compliance_level:   1.00
				best_organization_compliance_level:  1.00
			},
			{
				worst_organization_compliance_level: 0.92
				standard_name:                       "nydfs"
				avg_organization_compliance_level:   0.96
				best_organization_compliance_level:  1.00
			},
			{
				worst_organization_compliance_level: 0.33
				standard_name:                       "nyshield"
				avg_organization_compliance_level:   0.66
				best_organization_compliance_level:  1.00
			},
			{
				worst_organization_compliance_level: 0.79
				standard_name:                       "mitre"
				avg_organization_compliance_level:   0.90
				best_organization_compliance_level:  1.00
			},
			{
				worst_organization_compliance_level: 0.79
				standard_name:                       "padss"
				avg_organization_compliance_level:   0.90
				best_organization_compliance_level:  1.00
			},
			{
				worst_organization_compliance_level: 0.64
				standard_name:                       "sans25"
				avg_organization_compliance_level:   0.82
				best_organization_compliance_level:  1.00
			},
			{
				worst_organization_compliance_level: 0.87
				standard_name:                       "pdpa"
				avg_organization_compliance_level:   0.94
				best_organization_compliance_level:  1.00
			},
			{
				worst_organization_compliance_level: 0.82
				standard_name:                       "popia"
				avg_organization_compliance_level:   0.91
				best_organization_compliance_level:  1.00
			},
			{
				worst_organization_compliance_level: 0.93
				standard_name:                       "pdpo"
				avg_organization_compliance_level:   0.96
				best_organization_compliance_level:  1.00
			},
			{
				worst_organization_compliance_level: 0.89
				standard_name:                       "cmmc"
				avg_organization_compliance_level:   0.94
				best_organization_compliance_level:  1.00
			},
			{
				worst_organization_compliance_level: 0.84
				standard_name:                       "hitrust"
				avg_organization_compliance_level:   0.92
				best_organization_compliance_level:  1.00
			},
			{
				worst_organization_compliance_level: 0.89
				standard_name:                       "fedramp"
				avg_organization_compliance_level:   0.94
				best_organization_compliance_level:  1.00
			},
			{
				worst_organization_compliance_level: 0.81
				standard_name:                       "iso27002"
				avg_organization_compliance_level:   0.90
				best_organization_compliance_level:  1.00
			},
			{
				worst_organization_compliance_level: 0.97
				standard_name:                       "lgpd"
				avg_organization_compliance_level:   0.98
				best_organization_compliance_level:  1.00
			},
			{
				worst_organization_compliance_level: 0.88
				standard_name:                       "iec62443"
				avg_organization_compliance_level:   0.94
				best_organization_compliance_level:  1.00
			},
			{
				worst_organization_compliance_level: 0.54
				standard_name:                       "wassec"
				avg_organization_compliance_level:   0.77
				best_organization_compliance_level:  1.00
			},
			{
				worst_organization_compliance_level: 0.88
				standard_name:                       "osstmm3"
				avg_organization_compliance_level:   0.94
				best_organization_compliance_level:  1.00
			},
			{
				worst_organization_compliance_level: 0.91
				standard_name:                       "c2m2"
				avg_organization_compliance_level:   0.96
				best_organization_compliance_level:  1.00
			},
			{
				worst_organization_compliance_level: 0.40
				standard_name:                       "wasc"
				avg_organization_compliance_level:   0.70
				best_organization_compliance_level:  1.00
			},
			{
				worst_organization_compliance_level: 0.50
				standard_name:                       "ferpa"
				avg_organization_compliance_level:   0.75
				best_organization_compliance_level:  1.00
			},
			{
				worst_organization_compliance_level: 0.67
				standard_name:                       "nistssdf"
				avg_organization_compliance_level:   0.84
				best_organization_compliance_level:  1.00
			},
			{
				worst_organization_compliance_level: 0.67
				standard_name:                       "issaf"
				avg_organization_compliance_level:   0.84
				best_organization_compliance_level:  1.00
			},
			{
				worst_organization_compliance_level: 0.67
				standard_name:                       "ptes"
				avg_organization_compliance_level:   0.84
				best_organization_compliance_level:  1.00
			},
			{
				worst_organization_compliance_level: 0.62
				standard_name:                       "owasprisks"
				avg_organization_compliance_level:   0.81
				best_organization_compliance_level:  1.00
			},
			{
				worst_organization_compliance_level: 0.55
				standard_name:                       "mvsp"
				avg_organization_compliance_level:   0.78
				best_organization_compliance_level:  1.00
			},
			{
				worst_organization_compliance_level: 0.21
				standard_name:                       "owaspscp"
				avg_organization_compliance_level:   0.60
				best_organization_compliance_level:  1.00
			},
			{
				worst_organization_compliance_level: 0.81
				standard_name:                       "bsafss"
				avg_organization_compliance_level:   0.90
				best_organization_compliance_level:  1.00
			},
			{
				worst_organization_compliance_level: 0.89
				standard_name:                       "owaspmasvs"
				avg_organization_compliance_level:   0.94
				best_organization_compliance_level:  1.00
			},
			{
				worst_organization_compliance_level: 0.97
				standard_name:                       "nist800171"
				avg_organization_compliance_level:   0.98
				best_organization_compliance_level:  1.00
			},
			{
				worst_organization_compliance_level: 0.92
				standard_name:                       "nist800115"
				avg_organization_compliance_level:   0.96
				best_organization_compliance_level:  1.00
			},
			{
				worst_organization_compliance_level: 0.79
				standard_name:                       "swiftcsc"
				avg_organization_compliance_level:   0.90
				best_organization_compliance_level:  1.00
			},
			{
				worst_organization_compliance_level: 0.78
				standard_name:                       "osamm"
				avg_organization_compliance_level:   0.89
				best_organization_compliance_level:  1.00
			},
			{
				worst_organization_compliance_level: 0.90
				standard_name:                       "siglite"
				avg_organization_compliance_level:   0.95
				best_organization_compliance_level:  1.00
			},
			{
				worst_organization_compliance_level: 0.93
				standard_name:                       "sig"
				avg_organization_compliance_level:   0.96
				best_organization_compliance_level:  1.00
			},
		]
		sk: "COMPLIANCE#UNRELIABLEINDICATOR"
		pk: "COMPLIANCE#UNRELIABLEINDICATOR"
	},
] & [...#ComplianceUnreliableIndicatorsItem]
