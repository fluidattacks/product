package finding_historic_verification

import (
	"fluidattacks.com/types/findings"
)

#findingHistoricVerificationPk: =~"^FIN#[0-9a-z-]{9,36}$"
#findingHistoricVerificationSk: =~"^VERIFICATION#(\\d{4})-(\\d{2})-(\\d{2})(T(\\d{2}):(\\d{2}):(\\d{2})(\\.\\d+)?(Z|([+-])(\\d{2}):(\\d{2})))?$"

#FindingHistoricVerificationKeys: {
	pk: string & #findingHistoricVerificationPk
	sk: string & #findingHistoricVerificationSk
}

#FindingHistoricVerificationItem: {
	findings.#FindingVerification
	#FindingHistoricVerificationKeys
}

FindingHistoricVerification:
{
	FacetName: "finding_historic_verification"
	KeyAttributeAlias: {
		PartitionKeyAlias: "FIN#id"
		SortKeyAlias:      "VERIFICATION#iso8601utc"
	}
	NonKeyAttributes: [
		"status",
		"modified_date",
		"modified_by",
		"comment_id",
		"vulnerability_ids",
	]
	DataAccess: {
		MySql: {}
	}
}

FindingHistoricVerification: TableData: [
	{
		sk:          "VERIFICATION#2018-04-08T00:43:18+00:00"
		modified_by: "integratesuser@gmail.com"
		vulnerability_ids: SS:
		[
			"3bcdb384-5547-4170-a0b6-3b397a245465",
			"74632c0c-db08-47c2-b013-c70e5b67c49f",
		]
		pk:            "FIN#463558592"
		comment_id:    "1558048727999"
		modified_date: "2018-04-08T00:43:18+00:00"
		status:        "REQUESTED"
	},
	{
		sk:          "VERIFICATION#2019-04-08T00:43:18+00:00"
		modified_by: "integratesuser@gmail.com"
		vulnerability_ids: {
			SS: [
				"15375781-31f2-4953-ac77-f31134225747",
				"587c40de-09a0-4d85-a9f9-eaa46aa895d7",
			]
		}
		pk:            "FIN#436992569"
		comment_id:    "1558048727000"
		modified_date: "2019-04-08T00:43:18+00:00"
		status:        "REQUESTED"
	},
	{
		sk:          "VERIFICATION#2020-01-19T15:41:04+00:00"
		modified_by: "integratesuser@gmail.com"
		vulnerability_ids: {
			SS: [
				"3bcdb384-5547-4170-a0b6-3b397a245465",
				"74632c0c-db08-47c2-b013-c70e5b67c49f",
			]
		}
		pk:            "FIN#463558592"
		comment_id:    "1558048727999"
		modified_date: "2020-01-19T15:41:04+00:00"
		status:        "REQUESTED"
	},
	{
		sk:          "VERIFICATION#2020-02-19T15:41:04+00:00"
		modified_by: "integratesuser@gmail.com"
		vulnerability_ids: {
			SS: [
				"15375781-31f2-4953-ac77-f31134225747",
				"587c40de-09a0-4d85-a9f9-eaa46aa895d7",
			]
		}
		pk:            "FIN#436992569"
		comment_id:    "1558048727000"
		modified_date: "2020-02-19T15:41:04+00:00"
		status:        "REQUESTED"
	},
	{
		sk:          "VERIFICATION#2020-02-21T15:41:04+00:00"
		modified_by: "integrateshacker@fluidattacks.com"
		vulnerability_ids: {
			SS: [
				"15375781-31f2-4953-ac77-f31134225747",
			]
		}
		pk:            "FIN#436992569"
		comment_id:    "1558048727111"
		modified_date: "2020-02-21T15:41:04+00:00"
		status:        "VERIFIED"
	},
] & [...#FindingHistoricVerificationItem]
