package toe_packages_metadata

import (
	"fluidattacks.com/types/toe_packages"
)

#toePackagesMetadataPk: =~"^GROUP#[a-z]+$"
#toePackagesMetadataSk: =~"^PKGS#ROOT#[a-f0-9-]{36}#PACKAGE#[a-z0-9-:.]+#VERSION#[a-z0-9.-]+$"

#ToePackagesMetadataKeys: {
	pk: string & #toePackagesMetadataPk
	sk: string & #toePackagesMetadataSk

	pk_2: string
	sk_2: string
}

#ToePackagesItem: {
	#ToePackagesMetadataKeys
	toe_packages.#ToePackagesMetadata
}

ToePackagesMetadata:
{
	FacetName: "toe_packages_metadata"
	KeyAttributeAlias: {
		PartitionKeyAlias: "GROUP#group_name"
		SortKeyAlias:      "PKGS#ROOT#root_id#PACKAGE#name#VERSION#version"
	}
	NonKeyAttributes: [
		"be_present",
		"found_by",
		"group_name",
		"organization_name",
		"has_related_vulnerabilities",
		"health_metadata",
		"id",
		"image_ref",
		"dependency_type",
		"scope",
		"language",
		"licenses",
		"locations",
		"modified_date",
		"name",
		"outdated",
		"package_advisories",
		"package_url",
		"pk_2",
		"platform",
		"root_id",
		"sk_2",
		"type",
		"type_",
		"url",
		"version",
		"vulnerable",
	]
	DataAccess: {
		MySql: {}
	}
}

ToePackagesMetadata: TableData: [
	{
		group_name:        "unittesting"
		organization_name: "okada"
		name:              "com.nimbusds:nimbus-jose-jwt"
		sk:                "PKGS#ROOT#4039d098-ffc5-4984-8ed3-eb17bca98e19#PACKAGE#com.nimbusds:nimbus-jose-jwt#VERSION#8.3"
		pk_2:              "GROUP#unittesting"
		root_id:           "4039d098-ffc5-4984-8ed3-eb17bca98e19"
		pk:                "GROUP#unittesting"
		package_url:       "pkg:maven/com.nimbusds/nimbus-jose-jwt@8.3"
		language:          "java"
		outdated:          true
		platform:          "MAVEN"
		vulnerable:        true
		be_present:        true
		id:                "nb931d6e982c13ff"
		modified_date:     "2024-01-05T14:38:51.241508+00:00"
		url:               "https://repo1.maven.org/maven2/com/nimbusds/nimbus-jose-jwt/8.3/"
		locations:
		[
			{
				dependency_type: "DIRECT"
				scope:           "PROD"
				path:            "build.gradle"
				line:            "85"
			},
			{
				path:      "/lib/apk/db/installed"
				layer:     "sha256:d4fc045c9e3a848011de66f34b81f052d4f2c15a17bb196d637e526349601820"
				image_ref: "docker.io/mongo:latest"
			},
		]
		licenses:
		[
			"MIT",
		]
		has_related_vulnerabilities: true
		health_metadata: {
			latest_version_created_at: "2024-06-06T14:55:15Z"
			latest_version:            "9.40"
			authors:                   "Vladimir Dzhuvinov <vladimir@dzhuvinov.com>"
		}
		package_advisories: [
			{
				cpes: []
				description: "Denial of Service in Connect2id Nimbus JOSE+JWT"
				id:          "GHSA-pg98-6v7f-2xfv"
				namespace:   "github:language:java"
				severity:    "Medium"
				urls: ["https://github.com/advisories/GHSA-pg98-6v7f-2xfv"]
				version_constraint: "<9.37.2"
				percentile:         0.54019
				epss:               0.00189
			},
		]
		version:  "8.3"
		type_:    "java-archive"
		found_by: "java-parse-gradle-lock"
		sk_2:     "PKGS#PRESENT#false#VULNERABLE#false#"
	},
	{
		group_name:        "unittesting"
		organization_name: "okada"
		name:              "commons-io:commons-io"
		sk:                "PKGS#ROOT#4039d098-ffc5-4984-8ed3-eb17bca98e19#PACKAGE#commons-io:commons-io#VERSION#2.7"
		pk_2:              "GROUP#unittesting"
		root_id:           "4039d098-ffc5-4984-8ed3-eb17bca98e19"
		pk:                "GROUP#unittesting"
		package_url:       "pkg:maven/commons-io/commons-io@2.7"
		language:          "java"
		outdated:          true
		platform:          "MAVEN"
		vulnerable:        false
		be_present:        true
		id:                "f75fd9178b7945bf"
		modified_date:     "2024-01-05T14:38:51.241508+00:00"
		url:               "https://repo1.maven.org/maven2/commons-io/commons-io/2.7/"
		locations:
		[
			{
				path: "build.gradle"
				line: "88"
			},
		]
		licenses:
		[
			"MIT",
		]
		has_related_vulnerabilities: false
		health_metadata: {
			latest_version_created_at: "2024-04-08T16:54:45Z"
			latest_version:            "2.16.1"
			authors:                   "Scott Sanders <sanders@apache.org>"
		}
		package_advisories: []
		version:  "2.7"
		type_:    "java-archive"
		found_by: "java-parse-gradle-lock"
		sk_2:     "PKGS#PRESENT#true#VULNERABLE#false#"
	},
	{
		group_name:        "unittesting"
		organization_name: "okada"
		name:              "javax.mail:mail"
		sk:                "PKGS#ROOT#4039d098-ffc5-4984-8ed3-eb17bca98e19#PACKAGE#javax.mail:mail#VERSION#1.4"
		pk_2:              "GROUP#unittesting"
		root_id:           "4039d098-ffc5-4984-8ed3-eb17bca98e19"
		pk:                "GROUP#unittesting"
		package_url:       "pkg:maven/javax.mail/mail@1.4"
		language:          "java"
		outdated:          false
		platform:          "MAVEN"
		vulnerable:        false
		be_present:        true
		id:                "aa931d6e982c13ff"
		modified_date:     "2024-01-05T14:38:51.241508+00:00"
		url:               "https://repo1.maven.org/maven2/javax/mail/mail/1.4/"
		locations:
		[
			{
				path: "build.gradle"
				line: "85"
			},
		]
		licenses:
		[
			"MIT",
		]
		has_related_vulnerabilities: false
		health_metadata: {
			latest_version_created_at: "2013-03-09T01:41:31Z"
			latest_version:            "1.4"
			authors:                   "Bill Shannon <bill.shannon@oracle.com>"
		}
		package_advisories: []
		version:  "1.4"
		type_:    "java-archive"
		found_by: "java-parse-gradle-lock"
		sk_2:     "PKGS#PRESENT#true#VULNERABLE#true#"
	},
	{
		group_name:        "unittesting"
		organization_name: "okada"
		name:              "org.json:json"
		sk:                "PKGS#ROOT#4039d098-ffc5-4984-8ed3-eb17bca98e19#PACKAGE#org.json:json#VERSION#20190722"
		pk_2:              "GROUP#unittesting"
		root_id:           "4039d098-ffc5-4984-8ed3-eb17bca98e19"
		pk:                "GROUP#unittesting"
		package_url:       "pkg:maven/org.json/json@20190722"
		language:          "java"
		outdated:          true
		platform:          "MAVEN"
		vulnerable:        true
		be_present:        true
		id:                "8413009465d8b195"
		modified_date:     "2024-01-05T14:38:51.241508+00:00"
		url:               "https://repo1.maven.org/maven2/org/json/json/20190722/"
		locations:
		[
			{
				path: "build.gradle"
				line: "82"
			},
		]
		licenses:
		[
			"MIT",
		]
		has_related_vulnerabilities: true
		health_metadata: {
			latest_version_created_at: "2024-03-03T15:19:10Z"
			latest_version:            "20240303"
			authors:                   "Douglas Crockford <douglas@crockford.com>"
		}
		package_advisories: [
			{
				cpes: []
				description: "json stack overflow vulnerability"
				id:          "GHSA-3vqj-43w4-2q58"
				namespace:   "github:language:java"
				severity:    "High"
				urls: ["https://github.com/advisories/GHSA-3vqj-43w4-2q58"]
				version_constraint: "<20230227"
				percentile:         0.74919
				epss:               0.00237
			},
			{
				cpes: []
				description: "Java: DoS Vulnerability in JSON-JAVA"
				id:          "GHSA-4jq9-2xhw-jpx7"
				namespace:   "github:language:java"
				severity:    "High"
				urls: ["https://github.com/advisories/GHSA-4jq9-2xhw-jpx7"]
				version_constraint: "<=20230618"
				percentile:         0.94519
				epss:               0.00164
			},
		]
		version:  "20190722"
		type_:    "java-archive"
		found_by: "java-parse-gradle-lock"
		sk_2:     "PKGS#PRESENT#true#VULNERABLE#false#"
	},
	{
		group_name:        "unittesting"
		organization_name: "okada"
		name:              "vyper"
		sk:                "PKGS#ROOT#4039d098-ffc5-4984-8ed3-eb17bca98e19#PACKAGE#vyper#VERSION#0.3.7"
		pk_2:              "GROUP#unittesting"
		root_id:           "4039d098-ffc5-4984-8ed3-eb17bca98e19"
		pk:                "GROUP#unittesting"
		package_url:       "pkg:pypi/vyper@0.3.7"
		language:          "python"
		outdated:          true
		platform:          "PIP"
		vulnerable:        true
		be_present:        true
		id:                "84130094455d8b195"
		modified_date:     "2024-01-05T14:38:51.241508+00:00"
		url:               "https://pypi.org/project/vyper/0.3.7/"
		locations: [
			{
				path: "requirements.txt"
				line: "45"
			},
		]
		licenses: [
			"Apache-2.0",
		]
		has_related_vulnerabilities: true
		health_metadata: {
			latest_version_created_at: "2024-03-03T15:19:10Z"
			latest_version:            "0.3.8"
			authors:                   "Vyper Team <vyperlang@ethereum.org>"
		}
		package_advisories: [
			{
				cpes: [
					"cpe:2.3:a:vyperlang:vyper:*:*:*:*:*:*:*:*",
				]
				description: "Vyper is a smart contract language for the Ethereum Virtual Machine. In versions 0.3.1 to 0.3.7, the Vyper compiler generates incorrect bytecode. Any contract using 'raw_call' with 'revert_on_failure=False' and 'max_outsize=0' receives an incorrect response from 'raw_call'. Depending on memory content, the result may be either 'True' or 'False'. A patch is available in version 0.3.8. As a workaround, set 'max_outsize>0'."
				id:          "CVE-2022-24439"
				namespace:   "github:language:python"
				severity:    "High"
				urls: [
					"https://nvd.nist.gov/vuln/detail/CVE-2022-24439",
					"https://github.com/vyperlang/vyper/security/advisories/GHSA-w9g2-3w7p-72g9",
				]
				version_constraint: ">=0.3.1, <0.3.8"
				percentile:         0.92345
				epss:               0.00173
			},
			{
				cpes: [
					"cpe:2.3:a:vyperlang:vyper:*:*:*:*:*:*:*:*",
				]
				description: "In Vyper versions 0.3.10 and earlier, the bounds check for slices does not prevent overflow when 'start + length' is not a literal, allowing potential exploitation."
				id:          "CVE-2024-24561"
				namespace:   "github:language:python"
				severity:    "Medium"
				urls: [
					"https://nvd.nist.gov/vuln/detail/CVE-2024-24561",
				]
				version_constraint: "<=0.3.10"
				percentile:         0.85712
				epss:               0.00158
			},
			{
				cpes: [
					"cpe:2.3:a:vyperlang:vyper:*:*:*:*:*:*:*:*",
				]
				description: "Arrays in Vyper can be indexed by signed integers, though they are defined for unsigned integers only, which may cause unexpected behavior."
				id:          "CVE-2024-24563"
				namespace:   "github:language:python"
				severity:    "Medium"
				urls: [
					"https://nvd.nist.gov/vuln/detail/CVE-2024-24563",
				]
				version_constraint: "<=0.3.10"
				percentile:         0.97689
				epss:               0.00182
			},
			{
				cpes: [
					"cpe:2.3:a:vyperlang:vyper:*:*:*:*:*:*:*:*",
				]
				description: "The Vyper compiler allows passing a value in 'raw_call' with 'delegatecall' or 'staticcall', but silently ignores it, leading to potential vulnerabilities."
				id:          "CVE-2024-24567"
				namespace:   "github:language:python"
				severity:    "Low"
				urls: [
					"https://nvd.nist.gov/vuln/detail/CVE-2024-24567",
				]
				version_constraint: "<0.3.8"
				percentile:         0.89723
				epss:               0.00147
			},
			{
				cpes: []
				description: "Malicious code in vyper (pypi)"
				id:          "MAL-2024-8857"
				namespace:   "github:language:python"
				severity:    "Critical"
				urls: ["https://github.com/advisories/GHSA-2446-89wx-689q"]
				version_constraint: "=0.0.1"
				percentile:         0
				epss:               0
			},
		]
		version: "0.3.7"
		type_:   "python-wheel"
		sk_2:    "PKGS#PRESENT#true#VULNERABLE#true#"
	},
] & [...#ToePackagesItem]
