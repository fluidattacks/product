package group_id

#GroupIdItem: {
	pk: string & =~"^GROUP_ID$"
	sk: string & =~"^GROUP#[a-z]+$"
}

GroupId:
{
	FacetName: "group_id"
	KeyAttributeAlias: {
		PartitionKeyAlias: "GROUP_ID"
		SortKeyAlias:      "GROUP#name"
	}
	DataAccess: {
		MySql: {}
	}
}

GroupId: TableData: [
	{
		sk: "GROUP#asgard"
		pk: "GROUP_ID"
	},
	{
		sk: "GROUP#barranquilla"
		pk: "GROUP_ID"
	},
	{
		sk: "GROUP#continuoustesting"
		pk: "GROUP_ID"
	},
	{
		sk: "GROUP#deletegroup"
		pk: "GROUP_ID"
	},
	{
		sk: "GROUP#deleteimamura"
		pk: "GROUP_ID"
	},
	{
		sk: "GROUP#gotham"
		pk: "GROUP_ID"
	},
	{
		sk: "GROUP#kurome"
		pk: "GROUP_ID"
	},
	{
		sk: "GROUP#lubbock"
		pk: "GROUP_ID"
	},
	{
		sk: "GROUP#metropolis"
		pk: "GROUP_ID"
	},
	{
		sk: "GROUP#monteria"
		pk: "GROUP_ID"
	},
	{
		sk: "GROUP#oneshottest"
		pk: "GROUP_ID"
	},
	{
		sk: "GROUP#setpendingdeletion"
		pk: "GROUP_ID"
	},
	{
		sk: "GROUP#sheele"
		pk: "GROUP_ID"
	},
	{
		sk: "GROUP#unittesting"
		pk: "GROUP_ID"
	},
] & [...#GroupIdItem]
