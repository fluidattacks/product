package root_environment_secret

import (
	"fluidattacks.com/types/roots"
)

#rootEnvironmentSecretPk: =~"^GROUP#[a-z]+#URL#[a-f0-9]+$"
#rootEnvironmentSecretSk: =~"^ENVIRONMENT_SECRET#[\\w ]+$"

#RootEnvironmentSecretKeys: {
	pk: string & #rootEnvironmentSecretPk
	sk: string & #rootEnvironmentSecretSk
}

#RootEnvironmentSecretItem: {
	#RootEnvironmentSecretKeys
	roots.#RootEnvironmentSecret
}

RootEnvironmentSecret:
{
	FacetName: "root_environment_secret"
	KeyAttributeAlias: {
		PartitionKeyAlias: "GROUP#group_name#URL#hash"
		SortKeyAlias:      "ENVIRONMENT_SECRET#key"
	}
	NonKeyAttributes: [
		"description",
		"created_at",
		"key",
		"value",
		"state",
	]
	DataAccess: {
		MySql: {}
	}
}

RootEnvironmentSecret: TableData: [
	{
		pk:         "GROUP#unittesting#URL#00fbe3579a253b43239954a545dc0536e6c83094"
		sk:         "ENVIRONMENT_SECRET#Key value"
		key:        "Key value"
		value:      "Value test"
		created_at: "2023-06-22T17:51:53.312099+00:00"
		state: {
			owner:         "jdoe@testcompany.com"
			description:   "Environment secret description"
			modified_by:   "jdoe@testcompany.com"
			modified_date: "2023-07-22T17:51:53.312099+00:00"
		}
	},
] & [...#RootEnvironmentSecretItem]
