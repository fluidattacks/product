package integrates_vms_historic

import (
	"fluidattacks.com/database_schema"
	"fluidattacks.com/tables/integrates_vms_historic/facets:authentication"
	"fluidattacks.com/tables/integrates_vms_historic/facets:event_state"
	"fluidattacks.com/tables/integrates_vms_historic/facets:finding_state"
	"fluidattacks.com/tables/integrates_vms_historic/facets:finding_verification"
	"fluidattacks.com/tables/integrates_vms_historic/facets:git_root_state"
	"fluidattacks.com/tables/integrates_vms_historic/facets:group_access_state"
	"fluidattacks.com/tables/integrates_vms_historic/facets:group_policies"
	"fluidattacks.com/tables/integrates_vms_historic/facets:group_state"
	"fluidattacks.com/tables/integrates_vms_historic/facets:ip_root_state"
	"fluidattacks.com/tables/integrates_vms_historic/facets:org_finding_policy_state"
	"fluidattacks.com/tables/integrates_vms_historic/facets:organization_policies"
	"fluidattacks.com/tables/integrates_vms_historic/facets:organization_state"
	"fluidattacks.com/tables/integrates_vms_historic/facets:root_environment_url_state"
	"fluidattacks.com/tables/integrates_vms_historic/facets:root_docker_image_state"
	"fluidattacks.com/tables/integrates_vms_historic/facets:stakeholder_login"
	"fluidattacks.com/tables/integrates_vms_historic/facets:stakeholder_state"
	"fluidattacks.com/tables/integrates_vms_historic/facets:toe_input_state"
	"fluidattacks.com/tables/integrates_vms_historic/facets:toe_lines_state"
	"fluidattacks.com/tables/integrates_vms_historic/facets:toe_port_state"
	"fluidattacks.com/tables/integrates_vms_historic/facets:url_root_state"
	"fluidattacks.com/tables/integrates_vms_historic/facets:vulnerability_state"
	"fluidattacks.com/tables/integrates_vms_historic/facets:vulnerability_treatment"
	"fluidattacks.com/tables/integrates_vms_historic/facets:vulnerability_verification"
	"fluidattacks.com/tables/integrates_vms_historic/facets:vulnerability_zero_risk"
	"fluidattacks.com/tables/integrates_vms_historic/keys"
)

#IntegratesVmsHistoricTable: {
	TableName: "integrates_vms_historic"
	KeyAttributes: {
		PartitionKey: {
			AttributeName: "pk"
			AttributeType: "S"
		}
		SortKey: {
			AttributeName: "sk"
			AttributeType: "S"
		}
	}
	NonKeyAttributes: [for entry in keys.NonKeyAttributesData {
		AttributeName: entry.n
		AttributeType: entry.t
	}]
	BillingMode: "PAY_PER_REQUEST"
	DataAccess: {"MySql": {}}
	SampleDataFormats: {}
} & database_schema.#Table

#IntegratesVmsHistoricTable: TableFacets: [
	authentication.Authentication,
	event_state.EventState,
	finding_state.FindingState,
	finding_verification.FindingVerification,
	git_root_state.GitRootState,
	group_access_state.GroupAccessState,
	group_policies.GroupPolicies,
	group_state.GroupState,
	ip_root_state.IpRootState,
	org_finding_policy_state.OrgFindingPolicyState,
	organization_policies.OrganizationPolicies,
	organization_state.OrganizationState,
	root_docker_image_state.RootDockerImageState,
	root_environment_url_state.RootEnvironmentUrlState,
	stakeholder_login.StakeholderLogin,
	stakeholder_state.StakeholderState,
	toe_input_state.ToeInputState,
	toe_lines_state.ToeLinesState,
	toe_port_state.ToePortState,
	url_root_state.UrlRootState,
	vulnerability_state.VulnerabilityState,
	vulnerability_treatment.VulnerabilityTreatment,
	vulnerability_verification.VulnerabilityVerification,
	vulnerability_zero_risk.VulnerabilityZeroRisk,
] & [...database_schema.#Facet]

#IntegratesVmsHistoricTable: GlobalSecondaryIndexes: [
	{
		IndexName: "gsi_2"
		KeyAttributes: {
			PartitionKey: {
				AttributeName: "pk_2"
				AttributeType: "S"
			}
			SortKey: {
				AttributeName: "sk_2"
				AttributeType: "S"
			}
		}
		Projection: {
			ProjectionType: "ALL"
		}
	},
	{
		IndexName: "gsi_3"
		KeyAttributes: {
			PartitionKey: {
				AttributeName: "pk_3"
				AttributeType: "S"
			}
			SortKey: {
				AttributeName: "sk_3"
				AttributeType: "S"
			}
		}
		Projection: {
			ProjectionType: "ALL"
		}
	},
]
