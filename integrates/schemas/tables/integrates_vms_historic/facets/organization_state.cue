package organization_state

import (
	"fluidattacks.com/types/organizations"
)

#organizationStatePk: =~"^ORG#[a-z0-9-]+#ORG#[a-z]+"
#organizationStateSk: =~"^STATE#state#DATE#(\\d{4}-[01]\\d-[0-3]\\d([Tt][0-2]\\d:[0-5]\\d(:[0-5]\\d(\\.\\d{1,9})?)?([Zz]|([+-][0-2]\\d:[0-5]\\d))?)?)$"

#OrganizationStateKeys: {
	pk: string & #organizationStatePk
	sk: string & #organizationStateSk
}

#OrganizationStateItem: {
	#OrganizationStateKeys
	organizations.#OrganizationState
}

OrganizationState:
{
	FacetName: "organization_state"
	KeyAttributeAlias: {
		PartitionKeyAlias: "ORG#id#ORG#name"
		SortKeyAlias:      "STATE#state#DATE#iso8601utc"
	}
	NonKeyAttributes: [
		"modified_by",
		"modified_date",
		"aws_external_id",
		"pending_deletion_date",
		"status",
	]
	DataAccess: {
		MySql: {}
	}
}

OrganizationState: TableData: [
	{
		modified_by:     "unknown"
		sk:              "STATE#state#DATE#2019-11-22T20:07:57+00:00"
		pk:              "ORG#33c08ebd-2068-47e7-9673-e1aa03dc9448#ORG#kiba"
		aws_external_id: "ce5a4871-850f-48ee-bb6e-4bde918fe589"
		modified_date:   "2019-11-22T20:07:57+00:00"
		status:          "ACTIVE"
	},
	{
		modified_by:     "unknown"
		sk:              "STATE#state#DATE#2019-11-22T20:07:57+00:00"
		pk:              "ORG#956e9107-fd8d-49bc-b550-5609a7a1f6ac#ORG#kamiya"
		aws_external_id: "bd46edab-20ff-4045-b656-aa4d4752dc85"
		modified_date:   "2019-11-22T20:07:57+00:00"
		status:          "ACTIVE"
	},
	{
		aws_external_id:       "016a56db-a4aa-4fba-954a-2608fd439699"
		pending_deletion_date: "2019-11-22T20:07:57+00:00"
		modified_by:           "unknown"
		sk:                    "STATE#state#DATE#2019-11-22T20:07:57+00:00"
		pk:                    "ORG#7376c5fe-4634-4053-9718-e14ecbda1e6b#ORG#imamura"
		modified_date:         "2019-11-22T20:07:57+00:00"
		status:                "ACTIVE"
	},
	{
		aws_external_id:       "cda07b37-bf28-4c0c-88a4-ca8f4b6a4251"
		pending_deletion_date: "2019-11-22T20:07:57+00:00"
		modified_by:           "unknown"
		sk:                    "STATE#state#DATE#2019-11-22T20:07:57+00:00"
		pk:                    "ORG#d32674a9-9838-4337-b222-68c88bf54647#ORG#makoto"
		modified_date:         "2019-11-22T20:07:57+00:00"
		status:                "ACTIVE"
	},
	{
		modified_by:     "unknown"
		sk:              "STATE#state#DATE#2019-11-22T20:07:57+00:00"
		pk:              "ORG#fe80d2d4-ccb7-46d1-8489-67c6360581de#ORG#tatsumi"
		aws_external_id: "449886ae-64be-472f-b950-22e04d2c747a"
		modified_date:   "2019-11-22T20:07:57+00:00"
		status:          "ACTIVE"
	},
	{
		aws_external_id:       "44b02eb8-376f-4644-adf1-93d3aee726d7"
		pending_deletion_date: "2019-11-22T20:07:57+00:00"
		modified_by:           "unknown"
		sk:                    "STATE#state#DATE#2018-02-08T00:43:18+00:00"
		pk:                    "ORG#38eb8f25-7945-4173-ab6e-0af4ad8b7ef3#ORG#okada"
		modified_date:         "2018-02-08T00:43:18+00:00"
		status:                "ACTIVE"
	},
	{
		modified_by:     "integratesmanager@gmail.com"
		sk:              "STATE#state#DATE#2019-11-30T15:00:00+00:00"
		pk:              "ORG#ed6f051c-2572-420f-bc11-476c4e71b4ee#ORG#ikari"
		aws_external_id: "5f91c7ac-0da9-4875-a23f-2854a66d7ee8"
		modified_date:   "2019-11-30T15:00:00+00:00"
		status:          "DELETED"
	},
	{
		aws_external_id:       "72bbf517-8611-4ec6-9a5e-00438a103653"
		pending_deletion_date: "2019-11-22T20:07:57+00:00"
		modified_by:           "unknown"
		sk:                    "STATE#state#DATE#2019-11-22T20:07:57+00:00"
		pk:                    "ORG#f2e2777d-a168-4bea-93cd-d79142b294d2#ORG#hajime"
		modified_date:         "2019-11-22T20:07:57+00:00"
		status:                "ACTIVE"
	},
	{
		modified_by:     "unknown"
		sk:              "STATE#state#DATE#2019-11-22T20:07:57+00:00"
		pk:              "ORG#c2ee2d15-04ab-4f39-9795-fbe30cdeee86#ORG#bulat"
		aws_external_id: "0a3fd5c4-9e85-4f39-9215-3993f0ed5c14"
		modified_date:   "2019-11-22T20:07:57+00:00"
		status:          "ACTIVE"
	},
	{
		modified_by:     "unknown"
		sk:              "STATE#state#DATE#2019-11-22T20:07:57+00:00"
		pk:              "ORG#c6cecc0e-bb92-4079-8b6d-c4e815c10bb1#ORG#makimachi"
		aws_external_id: "105c358f-6557-4c36-9b00-d38da19f2458"
		modified_date:   "2019-11-22T20:07:57+00:00"
		status:          "ACTIVE"
	},
	{
		modified_by:     "unknown"
		sk:              "STATE#state#DATE#2019-11-22T20:07:57+00:00"
		pk:              "ORG#ffddc7a3-7f05-4fc7-b65d-7defffa883c2#ORG#himura"
		aws_external_id: "97cf786e-a47e-4f15-b3f0-84eb920bc774"
		modified_date:   "2019-11-22T20:07:57+00:00"
		status:          "ACTIVE"
	},
] & [...#OrganizationStateItem]
