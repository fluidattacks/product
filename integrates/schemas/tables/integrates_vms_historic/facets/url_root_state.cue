package url_root_state

import (
	"fluidattacks.com/types/roots"
)

#urlRootStatePk: =~"^ROOT#[a-f0-9-]{36}#GROUP#[a-z]+$"
#urlRootStateSk: =~"^STATE#state#DATE#(\\d{4})-(\\d{2})-(\\d{2})(T(\\d{2}):(\\d{2}):(\\d{2})(\\.\\d+)?(Z|([+-])(\\d{2}):(\\d{2})))?$"

#UrlRootStateKeys: {
	pk:   string & #urlRootStatePk
	sk:   string & #urlRootStateSk
	pk_2: string
	pk_3: string
	sk_2: string
	sk_3: string
}

#UrlRootStateItem: {
	#UrlRootStateKeys
	roots.#UrlRootState
}

UrlRootState:
{
	FacetName: "url_root_state"
	KeyAttributeAlias: {
		PartitionKeyAlias: "ROOT#uuid#GROUP#group_name"
		SortKeyAlias:      "STATE#state#DATE#iso8601utc"
	}
	NonKeyAttributes: [
		"pk_2",
		"pk_3",
		"sk_2",
		"sk_3",
		"modified_by",
		"modified_date",
		"nickname",
		"port",
		"host",
		"other",
		"path",
		"protocol",
		"query",
		"reason",
		"status",
	]
	DataAccess: {
		MySql: {}
	}
}

UrlRootState: TableData: [
	{
		modified_date: "2020-11-19T13:45:55+00:00"
		path:          "/"
		protocol:      "HTTPS"
		port:          "443"
		modified_by:   "jdoe@fluidattacks.com"
		nickname:      "url_root_1"
		host:          "app.fluidattacks.com"
		sk:            "STATE#state#DATE#2020-11-19T13:45:55+00:00"
		sk_3:          "STATE#state#DATE#2020-11-19T13:45:55+00:00"
		pk_2:          "ROOT#GROUP#oneshottest"
		pk:            "ROOT#8493c82f-2860-4902-86fa-75b0fef76034#GROUP#oneshottest"
		pk_3:          "GROUP#oneshottest"
		sk_2:          "STATE#state#DATE#2020-11-19T13:45:55+00:00"
		status:        "ACTIVE"
	},
] & [...#UrlRootStateItem]
