package finding_verification

import (
	"fluidattacks.com/types/findings"
)

#findingVerificationPk: =~"^FIN#[0-9]+#GROUP#[a-zA-Z0-9]+$"
#findingVerificationSk: =~"^STATE#verification#DATE#(\\d{4}-[01]\\d-[0-3]\\d([Tt][0-2]\\d:[0-5]\\d(:[0-5]\\d(\\.\\d{1,9})?)?([Zz]|([+-][0-2]\\d:[0-5]\\d))?)?)$"

#FindingVerificationKeys: {
	pk:   string & #findingVerificationPk
	sk:   string & #findingVerificationSk
	pk_2: string & =~"^FIN#GROUP#[a-z]+$"
	pk_3: string & =~"^GROUP#[a-zA-Z0-9]+$"
	sk_2: string & #findingVerificationSk
	sk_3: string & #findingVerificationSk
}

#FindingVerificationItem: {
	findings.#FindingVerification
	#FindingVerificationKeys
}

FindingVerification:
{
	FacetName: "finding_verification"
	KeyAttributeAlias: {
		PartitionKeyAlias: "FIN#id#GROUP#group_name"
		SortKeyAlias:      "STATE#verification#DATE#iso8601utc"
	}
	NonKeyAttributes: [
		"pk_2",
		"pk_3",
		"sk_2",
		"sk_3",
		"modified_by",
		"modified_date",
		"status",
		"comment_id",
		"vulnerability_ids",
	]
	DataAccess: {
		MySql: {}
	}
}

FindingVerification: TableData: [
	{
		modified_by: "integratesuser@gmail.com"
		sk:          "STATE#verification#DATE#2019-04-08T00:43:18+00:00"
		vulnerability_ids: {SS: [
			"15375781-31f2-4953-ac77-f31134225747",
			"587c40de-09a0-4d85-a9f9-eaa46aa895d7",
		]}
		sk_3:          "STATE#verification#DATE#2019-04-08T00:43:18+00:00"
		pk_2:          "FIN#GROUP#unittesting"
		pk:            "FIN#436992569#GROUP#unittesting"
		pk_3:          "GROUP#unittesting"
		comment_id:    "1558048727000"
		modified_date: "2019-04-08T00:43:18+00:00"
		sk_2:          "STATE#verification#DATE#2019-04-08T00:43:18+00:00"
		status:        "REQUESTED"
	},
	{
		modified_by: "integratesuser@gmail.com"
		sk:          "STATE#verification#DATE#2020-02-19T15:41:04+00:00"
		vulnerability_ids: {SS: [
			"15375781-31f2-4953-ac77-f31134225747",
			"587c40de-09a0-4d85-a9f9-eaa46aa895d7",
		]}
		sk_3:          "STATE#verification#DATE#2020-02-19T15:41:04+00:00"
		pk_2:          "FIN#GROUP#unittesting"
		pk:            "FIN#436992569#GROUP#unittesting"
		pk_3:          "GROUP#unittesting"
		comment_id:    "1558048727000"
		modified_date: "2020-02-19T15:41:04+00:00"
		sk_2:          "STATE#verification#DATE#2020-02-19T15:41:04+00:00"
		status:        "REQUESTED"
	},
	{
		modified_by: "integrateshacker@fluidattacks.com"
		vulnerability_ids: {SS: [
			"15375781-31f2-4953-ac77-f31134225747",
		]}
		sk:            "STATE#verification#DATE#2020-02-21T15:41:04+00:00"
		sk_3:          "STATE#verification#DATE#2020-02-21T15:41:04+00:00"
		pk_2:          "FIN#GROUP#unittesting"
		pk:            "FIN#436992569#GROUP#unittesting"
		pk_3:          "GROUP#unittesting"
		comment_id:    "1558048727111"
		modified_date: "2020-02-21T15:41:04+00:00"
		sk_2:          "STATE#verification#DATE#2020-02-21T15:41:04+00:00"
		status:        "VERIFIED"
	},
	{
		modified_by: "integrateshacker@fluidattacks.com"
		vulnerability_ids: {SS: [
			"7c9e8d7a-7f35-6f04-5cce-8acb98b31cf8",
		]}
		sk:            "STATE#verification#DATE#2020-02-21T15:41:04+00:00"
		sk_3:          "STATE#verification#DATE#2020-02-21T15:41:04+00:00"
		pk_2:          "FIN#GROUP#unittesting"
		pk:            "FIN#151213607#GROUP#unittesting"
		pk_3:          "GROUP#unittesting"
		comment_id:    "8617231051651"
		modified_date: "2020-02-21T15:41:04+00:00"
		sk_2:          "STATE#verification#DATE#2020-02-21T15:41:04+00:00"
		status:        "VERIFIED"
	},
	{
		modified_by: "integratesuser@gmail.com"
		sk:          "STATE#verification#DATE#2018-04-08T00:43:18+00:00"
		vulnerability_ids: {SS: [
			"3bcdb384-5547-4170-a0b6-3b397a245465",
			"74632c0c-db08-47c2-b013-c70e5b67c49f",
		]}
		sk_3:          "STATE#verification#DATE#2018-04-08T00:43:18+00:00"
		pk_2:          "FIN#GROUP#unittesting"
		pk:            "FIN#463558592#GROUP#unittesting"
		pk_3:          "GROUP#unittesting"
		comment_id:    "1558048727999"
		modified_date: "2018-04-08T00:43:18+00:00"
		sk_2:          "STATE#verification#DATE#2018-04-08T00:43:18+00:00"
		status:        "REQUESTED"
	},
	{
		modified_by: "integratesuser@gmail.com"
		vulnerability_ids: {SS: [
			"3bcdb384-5547-4170-a0b6-3b397a245465",
			"74632c0c-db08-47c2-b013-c70e5b67c49f",
		]}
		sk:            "STATE#verification#DATE#2020-01-19T15:41:04+00:00"
		sk_3:          "STATE#verification#DATE#2020-01-19T15:41:04+00:00"
		pk_2:          "FIN#GROUP#unittesting"
		pk:            "FIN#463558592#GROUP#unittesting"
		pk_3:          "GROUP#unittesting"
		comment_id:    "1558048727999"
		modified_date: "2020-01-19T15:41:04+00:00"
		sk_2:          "STATE#verification#DATE#2020-01-19T15:41:04+00:00"
		status:        "REQUESTED"
	},
] & [...#FindingVerificationItem]
