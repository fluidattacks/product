package jira_install_state

import (
	"fluidattacks.com/types/jira"
)

#jiraInstallStatePk: =~"^CLIENT_KEY#[a-zA-Z0-9-]+#SITE#[a-zA-Z0-9]+$"
#jiraInstallStateSk: =~"^STATE#state#DATE#(\\d{4}-[01]\\d-[0-3]\\d([Tt][0-2]\\d:[0-5]\\d(:[0-5]\\d(\\.\\d{1,9})?)?([Zz]|([+-][0-2]\\d:[0-5]\\d))?)?)$"

#JiraInstallStateKeys: {
	pk:   string & #jiraInstallStatePk
	sk:   string & #jiraInstallStateSk
	pk_2: string
	pk_3: string
	sk_2: string
	sk_3: string
}

#JiraInstallStateItem: {
	jira.#JiraInstallState
	#JiraInstallStateKeys
}

JiraInstallState:
{
	FacetName: "jira_install_state"
	KeyAttributeAlias: {
		PartitionKeyAlias: "CLIENT_KEY#id#SITE#jira_base_url"
		SortKeyAlias:      "STATE#state#DATE#iso8601utc"
	}
	NonKeyAttributes: [
		"pk_2",
		"pk_3",
		"sk_2",
		"sk_3",
		"modified_by",
		"modified_date",
		"used_api_token",
		"used_jira_jwt",
	]
	DataAccess: {
		MySql: {}
	}
}
