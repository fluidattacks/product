package org_finding_policy_state

import (
	"fluidattacks.com/types/organization_finding_policies"
)

#orgFindingPolicyStatePk: =~"^POLICY#[a-z0-9-]+#ORG#[a-z]+"
#orgFindingPolicyStateSk: =~"^STATE#state#DATE#(\\d{4}-[01]\\d-[0-3]\\d([Tt][0-2]\\d:[0-5]\\d(:[0-5]\\d(\\.\\d{1,9})?)?([Zz]|([+-][0-2]\\d:[0-5]\\d))?)?)$"

#OrgFindingPolicyStateKeys: {
	pk: string & #orgFindingPolicyStatePk
	sk: string & #orgFindingPolicyStateSk
}

#OrgFindingPolicyStateItem: {
	#OrgFindingPolicyStateKeys
	organization_finding_policies.#OrgFindingPolicyState
}

OrgFindingPolicyState:
{
	FacetName: "org_finding_policy_state"
	KeyAttributeAlias: {
		PartitionKeyAlias: "POLICY#uuid#ORG#name"
		SortKeyAlias:      "STATE#state#DATE#iso8601utc"
	}
	NonKeyAttributes: [
		"modified_by",
		"modified_date",
		"status",
	]
	DataAccess: {
		MySql: {}
	}
}

OrgFindingPolicyState: TableData: [
	{
		modified_by:   "test2@test.com"
		sk:            "STATE#state#DATE#2021-04-26T13:37:10+00:00"
		pk:            "POLICY#8b35ae2a-56a1-4f64-9da7-6a552683bf46#ORG#okada"
		modified_date: "2021-04-26T13:37:10+00:00"
		status:        "APPROVED"
	},
] & [...#OrgFindingPolicyStateItem]
