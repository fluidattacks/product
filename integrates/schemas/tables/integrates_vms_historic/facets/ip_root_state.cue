package ip_root_state

import (
	"fluidattacks.com/types/roots"
)

#ipRootStatePk: =~"^ROOT#[a-z0-9-]+#GROUP#[a-z]+"
#ipRootStateSk: =~"^STATE#state#DATE#(\\d{4}-[01]\\d-[0-3]\\d([Tt][0-2]\\d:[0-5]\\d(:[0-5]\\d(\\.\\d{1,9})?)?([Zz]|([+-][0-2]\\d:[0-5]\\d))?)?)$"

#IpRootStateKeys: {
	pk:   string & #ipRootStatePk
	sk:   string & #ipRootStateSk
	pk_2: string
	sk_2: string
	pk_3: string
	sk_3: string
}

#IpRootStateItem: {
	#IpRootStateKeys
	roots.#IpRootState
}

IpRootState:
{
	FacetName: "ip_root_state"
	KeyAttributeAlias: {
		PartitionKeyAlias: "ROOT#uuid#GROUP#group_name"
		SortKeyAlias:      "STATE#state#DATE#iso8601utc"
	}
	NonKeyAttributes: [
		"pk_2",
		"pk_3",
		"sk_2",
		"sk_3",
		"modified_by",
		"modified_date",
		"address",
		"nickname",
		"other",
		"reason",
		"status",
	]
	DataAccess: {
		MySql: {}
	}
}

IpRootState: TableData: [
	{
		address:       "127.0.0.1"
		modified_by:   "jdoe@fluidattacks.com"
		nickname:      "ip_root_2"
		sk:            "STATE#state#DATE#2020-12-19T13:44:37+00:00"
		sk_3:          "STATE#state#DATE#2020-12-19T13:44:37+00:00"
		pk_2:          "ROOT#GROUP#asgard"
		pk:            "ROOT#814addf0-316c-4415-850d-21bd3783b011#GROUP#asgard"
		pk_3:          "GROUP#asgard"
		modified_date: "2020-12-19T13:44:37+00:00"
		sk_2:          "STATE#state#DATE#2020-12-19T13:44:37+00:00"
		status:        "INACTIVE"
	},
	{
		address:       "127.0.0.1"
		modified_by:   "jdoe@fluidattacks.com"
		nickname:      "ip_root_1"
		sk:            "STATE#state#DATE#2020-11-19T13:44:37+00:00"
		sk_3:          "STATE#state#DATE#2020-11-19T13:44:37+00:00"
		pk_2:          "ROOT#GROUP#oneshottest"
		pk:            "ROOT#d312f0b9-da49-4d2b-a881-bed438875e99#GROUP#oneshottest"
		pk_3:          "GROUP#oneshottest"
		modified_date: "2020-11-19T13:44:37+00:00"
		sk_2:          "STATE#state#DATE#2020-11-19T13:44:37+00:00"
		status:        "ACTIVE"
	},
] & [...#IpRootStateItem]
