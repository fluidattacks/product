package event_state

import (
	"fluidattacks.com/types/events"
)

#eventStatePk: =~"^EVENT#[0-9]+#GROUP#[a-zA-Z0-9]+$"
#eventStateSk: =~"^STATE#state#DATE#(\\d{4}-[01]\\d-[0-3]\\d([Tt][0-2]\\d:[0-5]\\d(:[0-5]\\d(\\.\\d{1,9})?)?([Zz]|([+-][0-2]\\d:[0-5]\\d))?)?)$"

#EventStateKeys: {
	pk:   string & #eventStatePk
	sk:   string & #eventStateSk
	pk_2: string
	pk_3: string
	sk_2: string
	sk_3: string
}

#EventStateItem: {
	events.#EventState
	#EventStateKeys
}

EventState:
{
	FacetName: "event_state"
	KeyAttributeAlias: {
		PartitionKeyAlias: "EVENT#id#GROUP#group_name"
		SortKeyAlias:      "STATE#state#DATE#iso8601utc"
	}
	NonKeyAttributes: [
		"pk_2",
		"pk_3",
		"sk_2",
		"sk_3",
		"modified_by",
		"modified_date",
		"status",
		"comment_id",
	]
	DataAccess: {
		MySql: {}
	}
}

EventState: TableData: [
	{
		modified_by:   "unittest@fluidattacks.com"
		sk:            "STATE#state#DATE#2019-03-11T14:00:00+00:00"
		sk_3:          "STATE#state#DATE#2019-03-11T14:00:00+00:00"
		pk_2:          "EVENT#GROUP#unittesting"
		pk:            "EVENT#484763304#GROUP#unittesting"
		pk_3:          "GROUP#unittesting"
		modified_date: "2019-03-11T14:00:00+00:00"
		sk_2:          "STATE#state#DATE#2019-03-11T14:00:00+00:00"
		status:        "OPEN"
	},
	{
		modified_by:   "unittest@fluidattacks.com"
		sk:            "STATE#state#DATE#2019-03-11T15:57:45+00:00"
		sk_3:          "STATE#state#DATE#2019-03-11T15:57:45+00:00"
		pk_2:          "EVENT#GROUP#unittesting"
		pk:            "EVENT#484763304#GROUP#unittesting"
		pk_3:          "GROUP#unittesting"
		modified_date: "2019-03-11T15:57:45+00:00"
		sk_2:          "STATE#state#DATE#2019-03-11T15:57:45+00:00"
		status:        "CREATED"
	},
	{
		modified_by:   "unittest@fluidattacks.com"
		sk:            "STATE#state#DATE#2019-04-11T18:37:00+00:00"
		sk_3:          "STATE#state#DATE#2019-04-11T18:37:00+00:00"
		pk_2:          "EVENT#GROUP#unittesting"
		pk:            "EVENT#484763304#GROUP#unittesting"
		pk_3:          "GROUP#unittesting"
		modified_date: "2019-04-11T18:37:00+00:00"
		sk_2:          "STATE#state#DATE#2019-04-11T18:37:00+00:00"
		status:        "SOLVED"
	},
	{
		sk:            "STATE#state#DATE#2020-04-11T18:37:00+00:00"
		modified_by:   "unittest@fluidattacks.com"
		sk_3:          "STATE#state#DATE#2020-04-11T18:37:00+00:00"
		pk_2:          "EVENT#GROUP#unittesting"
		pk:            "EVENT#484763304#GROUP#unittesting"
		pk_3:          "GROUP#unittesting"
		modified_date: "2020-04-11T18:37:00+00:00"
		sk_2:          "STATE#state#DATE#2020-04-11T18:37:00+00:00"
		status:        "SOLVED"
	},
	{
		modified_by:   "unittest@fluidattacks.com"
		sk:            "STATE#state#DATE#2020-01-02T12:00:00+00:00"
		sk_3:          "STATE#state#DATE#2020-01-02T12:00:00+00:00"
		pk_2:          "EVENT#GROUP#oneshottest"
		pk:            "EVENT#418900978#GROUP#oneshottest"
		pk_3:          "GROUP#oneshottest"
		modified_date: "2020-01-02T12:00:00+00:00"
		sk_2:          "STATE#state#DATE#2020-01-02T12:00:00+00:00"
		status:        "OPEN"
	},
	{
		modified_by:   "unittest@fluidattacks.com"
		sk:            "STATE#state#DATE#2020-01-02T19:40:05+00:00"
		sk_3:          "STATE#state#DATE#2020-01-02T19:40:05+00:00"
		pk_2:          "EVENT#GROUP#oneshottest"
		pk:            "EVENT#418900978#GROUP#oneshottest"
		pk_3:          "GROUP#oneshottest"
		modified_date: "2020-01-02T19:40:05+00:00"
		sk_2:          "STATE#state#DATE#2020-01-02T19:40:05+00:00"
		status:        "CREATED"
	},
	{
		modified_by:   "unittest@fluidattacks.com"
		sk:            "STATE#state#DATE#2020-01-01T20:24:25+00:00"
		sk_3:          "STATE#state#DATE#2020-01-01T20:24:25+00:00"
		pk_2:          "EVENT#GROUP#oneshottest"
		pk:            "EVENT#418900979#GROUP#oneshottest"
		pk_3:          "GROUP#oneshottest"
		modified_date: "2020-01-01T20:24:25+00:00"
		sk_2:          "STATE#state#DATE#2020-01-01T20:24:25+00:00"
		status:        "OPEN"
	},
	{
		modified_by:   "unittest@fluidattacks.com"
		sk:            "STATE#state#DATE#2020-01-02T20:24:25+00:00"
		sk_3:          "STATE#state#DATE#2020-01-02T20:24:25+00:00"
		pk_2:          "EVENT#GROUP#oneshottest"
		pk:            "EVENT#418900979#GROUP#oneshottest"
		pk_3:          "GROUP#oneshottest"
		modified_date: "2020-01-02T20:24:25+00:00"
		sk_2:          "STATE#state#DATE#2020-01-02T20:24:25+00:00"
		status:        "CREATED"
	},
	{
		modified_by:   "unittest@fluidattacks.com"
		sk:            "STATE#state#DATE#2020-01-03T20:24:25+00:00"
		sk_3:          "STATE#state#DATE#2020-01-03T20:24:25+00:00"
		pk_2:          "EVENT#GROUP#oneshottest"
		pk:            "EVENT#418900979#GROUP#oneshottest"
		pk_3:          "GROUP#oneshottest"
		modified_date: "2020-01-03T20:24:25+00:00"
		sk_2:          "STATE#state#DATE#2020-01-03T20:24:25+00:00"
		status:        "SOLVED"
	},
	{
		modified_by:   "unittest@fluidattacks.com"
		sk:            "STATE#state#DATE#2019-04-02T08:02:00+00:00"
		sk_3:          "STATE#state#DATE#2019-04-02T08:02:00+00:00"
		pk_2:          "EVENT#GROUP#unittesting"
		pk:            "EVENT#540462638#GROUP#unittesting"
		pk_3:          "GROUP#unittesting"
		modified_date: "2019-04-02T08:02:00+00:00"
		sk_2:          "STATE#state#DATE#2019-04-02T08:02:00+00:00"
		status:        "OPEN"
	},
	{
		modified_by:   "unittest@fluidattacks.com"
		sk:            "STATE#state#DATE#2021-05-25T14:36:27+00:00"
		sk_3:          "STATE#state#DATE#2021-05-25T14:36:27+00:00"
		pk_2:          "EVENT#GROUP#unittesting"
		pk:            "EVENT#540462638#GROUP#unittesting"
		pk_3:          "GROUP#unittesting"
		modified_date: "2021-05-25T14:36:27+00:00"
		sk_2:          "STATE#state#DATE#2021-05-25T14:36:27+00:00"
		status:        "CREATED"
	},
	{
		modified_by:   "unittest@fluidattacks.com"
		sk:            "STATE#state#DATE#2019-04-02T08:02:00+00:00"
		sk_3:          "STATE#state#DATE#2019-04-02T08:02:00+00:00"
		pk_2:          "EVENT#GROUP#unittesting"
		pk:            "EVENT#540462628#GROUP#unittesting"
		pk_3:          "GROUP#unittesting"
		modified_date: "2019-04-02T08:02:00+00:00"
		sk_2:          "STATE#state#DATE#2019-04-02T08:02:00+00:00"
		status:        "OPEN"
	},
	{
		modified_by:   "unittest@fluidattacks.com"
		sk:            "STATE#state#DATE#2019-09-25T14:36:27+00:00"
		sk_3:          "STATE#state#DATE#2019-09-25T14:36:27+00:00"
		pk_2:          "EVENT#GROUP#unittesting"
		pk:            "EVENT#540462628#GROUP#unittesting"
		pk_3:          "GROUP#unittesting"
		modified_date: "2019-09-25T14:36:27+00:00"
		sk_2:          "STATE#state#DATE#2019-09-25T14:36:27+00:00"
		status:        "CREATED"
	},
	{
		modified_by:   "unittest@fluidattacks.com"
		sk:            "STATE#state#DATE#2022-10-28T00:00:00+00:00"
		sk_3:          "STATE#state#DATE#2022-10-28T00:00:00+00:00"
		pk_2:          "EVENT#GROUP#deletegroup"
		pk:            "EVENT#48192579#GROUP#deletegroup"
		pk_3:          "GROUP#deletegroup"
		modified_date: "2022-10-28T00:00:00+00:00"
		sk_2:          "STATE#state#DATE#2022-10-28T00:00:00+00:00"
		status:        "OPEN"
	},
	{
		modified_by:   "unittest@fluidattacks.com"
		sk:            "STATE#state#DATE#2022-10-28T00:00:00+00:01"
		sk_3:          "STATE#state#DATE#2022-10-28T00:00:00+00:01"
		pk_2:          "EVENT#GROUP#deletegroup"
		pk:            "EVENT#48192579#GROUP#deletegroup"
		pk_3:          "GROUP#deletegroup"
		modified_date: "2022-10-28T00:00:00+00:01"
		sk_2:          "STATE#state#DATE#2022-10-28T00:00:00+00:01"
		status:        "CREATED"
	},
	{
		modified_by:   "unittest@fluidattacks.com"
		sk:            "STATE#state#DATE#2018-06-27T12:00:00+00:00"
		sk_3:          "STATE#state#DATE#2018-06-27T12:00:00+00:00"
		pk_2:          "EVENT#GROUP#unittesting"
		pk:            "EVENT#418900971#GROUP#unittesting"
		pk_3:          "GROUP#unittesting"
		modified_date: "2018-06-27T12:00:00+00:00"
		sk_2:          "STATE#state#DATE#2018-06-27T12:00:00+00:00"
		status:        "OPEN"
	},
	{
		modified_by:   "unittest@fluidattacks.com"
		sk:            "STATE#state#DATE#2018-06-27T19:40:05+00:00"
		sk_3:          "STATE#state#DATE#2018-06-27T19:40:05+00:00"
		pk_2:          "EVENT#GROUP#unittesting"
		pk:            "EVENT#418900971#GROUP#unittesting"
		pk_3:          "GROUP#unittesting"
		modified_date: "2018-06-27T19:40:05+00:00"
		sk_2:          "STATE#state#DATE#2018-06-27T19:40:05+00:00"
		status:        "CREATED"
	},
	{
		modified_by:   "unittest@fluidattacks.com"
		sk:            "STATE#state#DATE#2018-01-01T20:24:25+00:00"
		sk_3:          "STATE#state#DATE#2018-01-01T20:24:25+00:00"
		pk_2:          "EVENT#GROUP#continuoustesting"
		pk:            "EVENT#418900923#GROUP#continuoustesting"
		pk_3:          "GROUP#continuoustesting"
		modified_date: "2018-01-01T20:24:25+00:00"
		sk_2:          "STATE#state#DATE#2018-01-01T20:24:25+00:00"
		status:        "OPEN"
	},
	{
		modified_by:   "unittest@fluidattacks.com"
		sk:            "STATE#state#DATE#2018-01-02T20:24:25+00:00"
		sk_3:          "STATE#state#DATE#2018-01-02T20:24:25+00:00"
		pk_2:          "EVENT#GROUP#continuoustesting"
		pk:            "EVENT#418900923#GROUP#continuoustesting"
		pk_3:          "GROUP#continuoustesting"
		modified_date: "2018-01-02T20:24:25+00:00"
		sk_2:          "STATE#state#DATE#2018-01-02T20:24:25+00:00"
		status:        "CREATED"
	},
	{
		modified_by:   "unittest@fluidattacks.com"
		sk:            "STATE#state#DATE#2018-01-03T20:24:25+00:00"
		sk_3:          "STATE#state#DATE#2018-01-03T20:24:25+00:00"
		pk_2:          "EVENT#GROUP#continuoustesting"
		pk:            "EVENT#418900923#GROUP#continuoustesting"
		pk_3:          "GROUP#continuoustesting"
		modified_date: "2018-01-03T20:24:25+00:00"
		sk_2:          "STATE#state#DATE#2018-01-03T20:24:25+00:00"
		status:        "SOLVED"
	},
	{
		modified_by:   "unittest@fluidattacks.com"
		sk:            "STATE#state#DATE#2018-12-17T21:20:00+00:00"
		sk_3:          "STATE#state#DATE#2018-12-17T21:20:00+00:00"
		pk_2:          "EVENT#GROUP#unittesting"
		pk:            "EVENT#463578352#GROUP#unittesting"
		pk_3:          "GROUP#unittesting"
		modified_date: "2018-12-17T21:20:00+00:00"
		sk_2:          "STATE#state#DATE#2018-12-17T21:20:00+00:00"
		status:        "OPEN"
	},
	{
		modified_by:   "unittest@fluidattacks.com"
		sk:            "STATE#state#DATE#2018-12-17T21:21:03+00:00"
		sk_3:          "STATE#state#DATE#2018-12-17T21:21:03+00:00"
		pk_2:          "EVENT#GROUP#unittesting"
		pk:            "EVENT#463578352#GROUP#unittesting"
		pk_3:          "GROUP#unittesting"
		modified_date: "2018-12-17T21:21:03+00:00"
		sk_2:          "STATE#state#DATE#2018-12-17T21:21:03+00:00"
		status:        "CREATED"
	},
	{
		modified_by:   "unittest@fluidattacks.com"
		sk:            "STATE#state#DATE#2018-12-26T18:37:00+00:00"
		sk_3:          "STATE#state#DATE#2018-12-26T18:37:00+00:00"
		pk_2:          "EVENT#GROUP#unittesting"
		pk:            "EVENT#463578352#GROUP#unittesting"
		pk_3:          "GROUP#unittesting"
		modified_date: "2018-12-26T18:37:00+00:00"
		sk_2:          "STATE#state#DATE#2018-12-26T18:37:00+00:00"
		status:        "SOLVED"
	},
	{
		modified_by:   "unittest@fluidattacks.com"
		sk:            "STATE#state#DATE#2019-09-19T13:09:00+00:00"
		sk_3:          "STATE#state#DATE#2019-09-19T13:09:00+00:00"
		pk_2:          "EVENT#GROUP#unittesting"
		pk:            "EVENT#538745942#GROUP#unittesting"
		pk_3:          "GROUP#unittesting"
		modified_date: "2019-09-19T13:09:00+00:00"
		sk_2:          "STATE#state#DATE#2019-09-19T13:09:00+00:00"
		status:        "OPEN"
	},
	{
		modified_by:   "unittest@fluidattacks.com"
		sk:            "STATE#state#DATE#2019-09-19T15:43:43+00:00"
		sk_3:          "STATE#state#DATE#2019-09-19T15:43:43+00:00"
		pk_2:          "EVENT#GROUP#unittesting"
		pk:            "EVENT#538745942#GROUP#unittesting"
		pk_3:          "GROUP#unittesting"
		modified_date: "2019-09-19T15:43:43+00:00"
		sk_2:          "STATE#state#DATE#2019-09-19T15:43:43+00:00"
		status:        "CREATED"
	},
] & [...#EventStateItem]
