package root_environment_url_state

import (
	"fluidattacks.com/types/roots"
)

#rootEnvironmentUrlStatePk: =~"^GROUP#[a-z]+#ROOT#[a-z0-9-]+#URL#[a-z0-9-]+"
#rootEnvironmentUrlStateSk: =~"^STATE#state#DATE#\\d{4}-[01]\\d-[0-3]\\d([Tt][0-2]\\d:[0-5]\\d(:[0-5]\\d(\\.\\d{1,9})?)?([Zz]|([+-][0-2]\\d:[0-5]\\d))?)?$"

#RootEnvironmentUrlStateKeys: {
	pk:   string & #rootEnvironmentUrlStatePk
	sk:   string & #rootEnvironmentUrlStateSk
	pk_2: string
	sk_2: string
	pk_3: string
	sk_3: string
}

#RootEnvironmentUrlStateItem: {
	#RootEnvironmentUrlStateKeys
	roots.#RootEnvironmentUrlState
}

RootEnvironmentUrlState:
{
	FacetName: "root_environment_url_state"
	KeyAttributeAlias: {
		PartitionKeyAlias: "GROUP#group_name#ROOT#uuid#URL#hash"
		SortKeyAlias:      "STATE#state#DATE#iso8601utc"
	}
	NonKeyAttributes: [
		"modified_by",
		"modified_date",
		"cloud_name",
		"include",
		"status",
		"url_type",
		"use_egress",
		"use_vpn",
		"use_ztna",
		"is_production",
		"pk_2",
		"pk_3",
		"sk_2",
		"sk_3",
	]
	DataAccess: {
		MySql: {}
	}
}

RootEnvironmentUrlState: TableData: [
	{
		include:       true
		url_type:      "URL"
		sk:            "STATE#state#DATE#2024-03-13T17:33:52.935263+00:00"
		modified_by:   "integrates@fluidattacks.com"
		sk_3:          "STATE#state#DATE#2024-03-13T17:33:52.935263+00:00"
		pk_2:          "GROUP#ROOT#URL#unittesting"
		pk:            "GROUP#unittesting#ROOT#4039d098-ffc5-4984-8ed3-eb17bca98e19#URL#00fbe3579a253b43239954a545dc0536e6c83098"
		pk_3:          "GROUP#unittesting"
		modified_date: "2024-03-13T17:33:52.935263+00:00"
		sk_2:          "STATE#state#DATE#2024-03-13T17:33:52.935263+00:00"
		status:        "CREATED"
	},
	{
		include:       true
		url_type:      "URL"
		sk:            "STATE#state#DATE#2024-03-13T17:33:52.937480+00:00"
		modified_by:   "integrates@fluidattacks.com"
		sk_3:          "STATE#state#DATE#2024-03-13T17:33:52.937480+00:00"
		pk_2:          "GROUP#ROOT#URL#unittesting"
		pk:            "GROUP#unittesting#ROOT#4039d098-ffc5-4984-8ed3-eb17bca98e19#URL#ba1598f6cf5e8512cd7d776d5dd8a5f79a4fed39"
		pk_3:          "GROUP#unittesting"
		modified_date: "2024-03-13T17:33:52.937480+00:00"
		sk_2:          "STATE#state#DATE#2024-03-13T17:33:52.937480+00:00"
		status:        "CREATED"
	},
	{
		include:       true
		url_type:      "URL"
		sk:            "STATE#state#DATE#2024-03-13T17:33:52.932537+00:00"
		modified_by:   "integrates@fluidattacks.com"
		sk_3:          "STATE#state#DATE#2024-03-13T17:33:52.932537+00:00"
		pk_2:          "GROUP#ROOT#URL#unittesting"
		pk:            "GROUP#unittesting#ROOT#4039d098-ffc5-4984-8ed3-eb17bca98e19#URL#00fbe3579a253b43239954a545dc0536e6c83094"
		pk_3:          "GROUP#unittesting"
		modified_date: "2024-03-13T17:33:52.932537+00:00"
		sk_2:          "STATE#state#DATE#2024-03-13T17:33:52.932537+00:00"
		status:        "CREATED"
	},
] & [...#RootEnvironmentUrlStateItem]
