package toe_input_state

import (
	"fluidattacks.com/types/toe_inputs"
)

#toeInputStatePk: =~"^GROUP#[a-z]+#INPUTS#ROOT#[a-f0-9-]{36}#COMPONENT#https://[^#]+#ENTRYPOINT#[^#]*$"
#toeInputStateSk: =~"^STATE#[a-z]+#DATE#(\\d{4})-(\\d{2})-(\\d{2})(T(\\d{2}):(\\d{2}):(\\d{2})(\\.\\d+)?(Z|([+-])(\\d{2}):(\\d{2})))?$"

#ToeInputStateKeys: {
	pk:   string & #toeInputStatePk
	sk:   string & #toeInputStateSk
	pk_2: string
	pk_3: string
	sk_2: string
	sk_3: string
}

#ToeInputStateItem: {
	#ToeInputStateKeys
	toe_inputs.#ToeInputState
}

ToeInputState:
{
	FacetName: "toe_input_state"
	KeyAttributeAlias: {
		PartitionKeyAlias: "GROUP#group_name#INPUTS#ROOT#root_id#COMPONENT#component#ENTRYPOINT#entry_point"
		SortKeyAlias:      "STATE#state#DATE#iso8601utc"
	}
	NonKeyAttributes: [
		"pk_2",
		"pk_3",
		"sk_2",
		"sk_3",
		"attacked_at",
		"attacked_by",
		"be_present",
		"be_present_until",
		"first_attack_at",
		"has_vulnerabilities",
		"modified_by",
		"modified_date",
		"seen_at",
		"seen_first_time_by",
	]
	DataAccess: {
		MySql: {}
	}
}

ToeInputState: TableData: [
	{
		seen_first_time_by:  "test2@test.com"
		has_vulnerabilities: false
		attacked_at:         "2021-02-11T05:00:00+00:00"
		be_present:          true
		modified_date:       "2021-02-11T05:00:00+00:00"
		attacked_by:         "test2@test.com"
		sk:                  "STATE#state#DATE#2021-02-11T05:00:00+00:00"
		modified_by:         "test2@test.com"
		sk_3:                "STATE#state#DATE#2021-02-11T05:00:00+00:00"
		pk_2:                "GROUP#INPUTS#ROOT#COMPONENT#ENTRYPOINT#unittesting"
		pk:                  "GROUP#unittesting#INPUTS#ROOT#4039d098-ffc5-4984-8ed3-eb17bca98e19#COMPONENT#https://test.com/test2/test.aspx#ENTRYPOINT#/"
		pk_3:                "GROUP#unittesting"
		first_attack_at:     "2021-02-11T05:00:00+00:00"
		sk_2:                "STATE#state#DATE#2021-02-11T05:00:00+00:00"
		seen_at:             "2020-01-11T05:00:00+00:00"
	},
	{
		seen_first_time_by:  "test2@test.com"
		has_vulnerabilities: true
		attacked_at:         "2021-02-11T05:00:00+00:00"
		be_present:          true
		modified_date:       "2021-02-11T05:00:00+00:00"
		attacked_by:         "test2@test.com"
		sk:                  "STATE#state#DATE#2021-02-11T05:00:00+00:00"
		modified_by:         "test2@test.com"
		sk_3:                "STATE#state#DATE#2021-02-11T05:00:00+00:00"
		pk_2:                "GROUP#INPUTS#ROOT#COMPONENT#ENTRYPOINT#unittesting"
		pk:                  "GROUP#unittesting#INPUTS#ROOT#4039d098-ffc5-4984-8ed3-eb17bca98e19#COMPONENT#https://test.com/test2/test.aspx#ENTRYPOINT#idTest (en-us)"
		pk_3:                "GROUP#unittesting"
		first_attack_at:     "2021-02-11T05:00:00+00:00"
		sk_2:                "STATE#state#DATE#2021-02-11T05:00:00+00:00"
		seen_at:             "2020-01-11T05:00:00+00:00"
	},
] & [...#ToeInputStateItem]
