package stakeholder_login

import (
	"fluidattacks.com/types/stakeholders"
)

#stakeholderLoginPk: =~"^USER#[A-z0-9._%+-@]+#USER#[A-z0-9._%+-@]+$"
#stakeholderLoginSk: =~"^STATE#login#DATE#(\\d{4}-[01]\\d-[0-3]\\d([Tt][0-2]\\d:[0-5]\\d(:[0-5]\\d(\\.\\d{1,9})?)?([Zz]|([+-][0-2]\\d:[0-5]\\d))?)?)$"

#StakeholderLoginKeys: {
	pk: string & #stakeholderLoginPk
	sk: string & #stakeholderLoginSk
}

#StakeholderLoginItem: {
	#StakeholderLoginKeys
	stakeholders.#StakeholderLogin
}

StakeholderLogin:
{
	FacetName: "stakeholder_login"
	KeyAttributeAlias: {
		PartitionKeyAlias: "USER#email#USER#email"
		SortKeyAlias:      "STATE#login#DATE#iso8601utc"
	}
	NonKeyAttributes: [
		"modified_by",
		"modified_date",
		"expiration_time",
		"browser",
		"country_code",
		"device",
		"ip_address",
		"provider",
		"subject",
	]
	DataAccess: {
		MySql: {}
	}
}

StakeholderLogin: TableData: [
	{
		country_code:    "PA"
		provider:        "GOOGLE"
		subject:         "GOOGLE#2222223333333344445555"
		expiration_time: 2026492340
		browser:         "Chrome 114.0.0.0"
		modified_by:     "integrates@fluidattacks.com"
		ip_address:      "127.0.0.1"
		modified_date:   "2024-03-22T18:32:20.800014+00:00"
		device:          "Linux"
		pk:              "USER#__adminEmail__#USER#__adminEmail__"
		sk:              "STATE#login#DATE#2024-03-22T18:32:20.800014+00:00"
	},
	{
		country_code:    "CO"
		provider:        "BITBUCKET"
		subject:         "BITBUCKET#2222223333333344445555"
		expiration_time: 2026492340
		browser:         "Chrome 114.0.0.0"
		sk:              "STATE#login#DATE#2024-03-22T18:32:20.800014+00:00"
		modified_by:     "integrates@fluidattacks.com"
		pk:              "USER#integratesuser@gmail.com#USER#integratesuser@gmail.com"
		ip_address:      "127.0.0.1"
		modified_date:   "2024-03-22T18:32:20.800014+00:00"
		device:          "Linux"
	},
	{
		country_code:    "US"
		provider:        "GOOGLE"
		subject:         "GOOGLE#2222223333333344445555"
		expiration_time: 2026492340
		browser:         "Chrome 114.0.0.0"
		sk:              "STATE#login#DATE#2024-03-22T18:32:20.800014+00:00"
		modified_by:     "integrates@fluidattacks.com"
		pk:              "USER#integratesreviewer@fluidattacks.com#USER#integratesreviewer@fluidattacks.com"
		ip_address:      "127.0.0.1"
		modified_date:   "2024-03-22T18:32:20.800014+00:00"
		device:          "Linux"
	},
	{
		country_code:    "CO"
		provider:        "GOOGLE"
		subject:         "GOOGLE#2222223333333344445555"
		expiration_time: 2026492340
		browser:         "Chrome 114.0.0.0"
		sk:              "STATE#login#DATE#2024-03-22T18:32:20.800014+00:00"
		modified_by:     "integrates@fluidattacks.com"
		pk:              "USER#integratesmanager@fluidattacks.com#USER#integratesmanager@fluidattacks.com"
		ip_address:      "127.0.0.1"
		modified_date:   "2024-03-22T18:32:20.800014+00:00"
		device:          "Linux"
	},
	{
		country_code:    ""
		provider:        "MICROSOFT"
		subject:         "MICROSOFT#2222223333333344445555"
		expiration_time: 2026492340
		browser:         "Chrome 114.0.0.0"
		sk:              "STATE#login#DATE#2024-03-22T18:32:20.800014+00:00"
		modified_by:     "integrates@fluidattacks.com"
		pk:              "USER#integratesuser2@gmail.com#USER#integratesuser2@gmail.com"
		ip_address:      "127.0.0.1"
		modified_date:   "2024-03-22T18:32:20.800014+00:00"
		device:          "Linux"
	},
	{
		country_code:    "PA"
		provider:        "GOOGLE"
		subject:         "GOOGLE#2222223333333344445555"
		expiration_time: 2026492340
		browser:         "Chrome 114.0.0.0"
		sk:              "STATE#login#DATE#2024-03-22T18:32:20.800014+00:00"
		modified_by:     "integrates@fluidattacks.com"
		pk:              "USER#integrateshacker@fluidattacks.com#USER#integrateshacker@fluidattacks.com"
		ip_address:      "127.0.0.1"
		modified_date:   "2024-03-22T18:32:20.800014+00:00"
		device:          "Linux"
	},
] & [...#StakeholderLoginItem]
