package group_access

import (
	"fluidattacks.com/bounds"
)

#GroupConfirmDeletion: {
	is_used:   bool
	url_token: string
}

#GroupInvitation: {
	is_used:         bool
	role:            string
	url_token:       string
	responsibility?: string
}

#GroupAccessState: {
	modified_date?:    string & bounds.#isoDate
	modified_by?:      string
	confirm_deletion?: #GroupConfirmDeletion
	has_access:        bool
	invitation?:       #GroupInvitation
	responsibility?:   string
	role?:             string
}

#GroupAccess: {
	domain?:          string
	email:            string
	group_name:       string
	state:            #GroupAccessState
	expiration_time?: int
}
