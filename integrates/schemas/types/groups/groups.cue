package groups

import (
	"fluidattacks.com/bounds"
)

#GroupAzureIssues: {
	redirect_uri:             string
	refresh_token:            string
	assigned_to?:             string
	azure_organization?:      string
	azure_project?:           string
	connection_date?:         string & bounds.#isoDate
	issue_automation_enabled: bool
	tags?: [...string]
}

#GroupInvitation:
{
	is_used:         bool
	role:            string
	url_token:       string
	responsibility?: string
}

#GroupState: {
	has_advanced:  bool
	has_essential: bool
	managed:       string
	modified_by:   string
	modified_date: string & bounds.#isoDate
	status:        string
	tier:          string
	type:          string

	comments?:              string
	justification?:         string
	payment_id?:            string
	pending_deletion_date?: string & bounds.#isoDate
	service?:               string
	tags?: [...string]
}

#GroupFile: {
	description:    string
	file_name:      string
	modified_by:    string
	modified_date?: string & bounds.#isoDate
}

#GroupGitLabIssues: {
	redirect_uri:  string
	refresh_token: string
	assignee_ids?: [...int]
	connection_date?:          string & bounds.#isoDate
	gitlab_project?:           string
	issue_automation_enabled?: bool
	labels?: [...string]
}

#GroupPolicies: {
	modified_date:                        string & bounds.#isoDate
	modified_by:                          string
	days_until_it_breaks?:                int
	inactivity_period?:                   int
	max_acceptance_days?:                 int
	max_acceptance_severity?:             number
	max_number_acceptances?:              int
	min_acceptance_severity?:             number
	min_breaking_severity?:               number
	vulnerability_grace_period?:          int
	max_number_of_expected_contributors?: int
}

#GroupMetadata: {
	created_by:      string
	created_date:    string & bounds.#isoDate
	description:     string
	language:        string
	name:            string
	organization_id: string
	state:           #GroupState

	agent_token?:    string
	azure_issues?:   #GroupAzureIssues
	business_id?:    string
	business_name?:  string
	context?:        string
	disambiguation?: string
	files?: [...#GroupFile]
	gitlab_issues?:     #GroupGitLabIssues
	policies?:          #GroupPolicies
	sprint_duration?:   int
	sprint_start_date?: string & bounds.#isoDate
}

#RegisterByTime: [...[...{
	[string]: string | number
}]]

#GroupTreatmentSummary: {
	accepted:           int
	accepted_undefined: int
	in_progress:        int
	untreated:          int
}

#UnfulfilledStandard: {
	name: string
	unfulfilled_requirements: [...string]
}

#CodeLanguage: {
	language: string
	loc:      int
}

#GroupUnreliableIndicators: {
	closed_vulnerabilities?:            int
	exposed_over_time_cvssf?:           #RegisterByTime
	exposed_over_time_month_cvssf?:     #RegisterByTime
	exposed_over_time_year_cvssf?:      #RegisterByTime
	last_closed_vulnerability_days?:    int
	last_closed_vulnerability_finding?: string
	max_open_severity?:                 number
	max_open_severity_finding?:         string
	max_severity?:                      number
	mean_remediate?:                    number
	mean_remediate_critical_severity?:  number
	mean_remediate_high_severity?:      number
	mean_remediate_low_severity?:       number
	mean_remediate_medium_severity?:    number
	open_findings?:                     int
	open_vulnerabilities?:              int
	remediated_over_time?:              #RegisterByTime
	remediated_over_time_30?:           #RegisterByTime
	remediated_over_time_90?:           #RegisterByTime
	remediated_over_time_cvssf?:        #RegisterByTime
	remediated_over_time_cvssf_30?:     #RegisterByTime
	remediated_over_time_cvssf_90?:     #RegisterByTime
	remediated_over_time_month?:        #RegisterByTime
	remediated_over_time_month_cvssf?:  #RegisterByTime
	remediated_over_time_year?:         #RegisterByTime
	remediated_over_time_year_cvssf?:   #RegisterByTime
	treatment_summary?:                 #GroupTreatmentSummary
	code_languages?: [...#CodeLanguage]
	nofluid_quantity?: int
	unfulfilled_standards?: [...#UnfulfilledStandard]
}
