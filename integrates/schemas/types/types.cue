package types

import (
	"fluidattacks.com/bounds"
)

#Policies: {
	modified_date:                        string & bounds.#isoDate
	modified_by:                          string
	days_until_it_breaks?:                int
	inactivity_period?:                   int
	max_acceptance_days?:                 int
	max_acceptance_severity?:             number
	max_number_acceptances?:              int
	min_acceptance_severity?:             number
	min_breaking_severity?:               number
	vulnerability_grace_period?:          int
	max_number_of_expected_contributors?: int
}
