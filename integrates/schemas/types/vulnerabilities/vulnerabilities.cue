package vulnerabilities

import (
	"fluidattacks.com/bounds"
)

#VulnerabilityAdvisory: {
	package:            string
	vulnerable_version: string
	cve: {SS: [...string]}
	epss?: int
}

#VulnerabilityTool: {
	name:   string
	impact: string
}

#Function: {
	name:                  string
	node_type:             string
	filed_identifier_name: string
}

#Snippet: {
	content:                string
	offset:                 int
	line?:                  int
	column?:                int
	columns_per_line?:      int
	line_context?:          int
	wrap?:                  bool
	show_line_numbers?:     bool
	highlight_line_number?: bool
	is_function?:           bool
	function?:              #Function
}

#VulnerabilityState: {
	modified_by:   string
	modified_date: string & bounds.#isoDate
	source:        string
	specific:      string
	status:        string
	where:         string
	commit?:       string
	advisories?:   #VulnerabilityAdvisory
	reasons?: [...string]
	other_reason?:      string
	tool?:              #VulnerabilityTool
	snippet?:           #Snippet
	proposed_severity?: #VulnerabilitySeverityProposal
}

#VulnerabilityTreatment: {
	modified_date: string & bounds.#isoDate
	status:        string

	modified_by?:       string
	acceptance_status?: string
	accepted_until?:    string & bounds.#isoDate
	assigned?:          string
	justification?:     string
}

#VulnerabilityVerification: {
	modified_date: string & bounds.#isoDate
	status:        string

	modified_by?: string
	event_id?:    string
	other?:       string
}

#VulnerabilityZeroRisk: {
	modified_by:   string
	modified_date: string & bounds.#isoDate
	comment_id:    string
	status:        string
}

#VulnerabilityAuthor: {
	author_email: string
	commit:       string
	commit_date:  string & bounds.#isoDate
}

#SeverityScore: {
	base_score:     number
	temporal_score: number
	cvss_v3:        string
	cvssf:          number
	cvssf_v4:       number
	threat_score:   number
	cvss_v4:        string
}

#VulnerabilityTreatment: {
	modified_date:      string & bounds.#isoDate
	status:             string
	acceptance_status?: string
	accepted_until?:    string & bounds.#isoDate
	justification?:     string
	assigned?:          string
	modified_by?:       string
}

#VulnerabilityVerification: {
	modified_date:  string & bounds.#isoDate
	status:         string
	event_id?:      string
	modified_by?:   string
	justification?: string
}

#VulnerabilityUnreliableIndicators: {
	unreliable_closing_date?:                 string & bounds.#isoDate
	unreliable_source:                        string
	unreliable_efficacy?:                     number
	unreliable_last_reattack_date?:           string & bounds.#isoDate
	unreliable_last_reattack_requester?:      string
	unreliable_last_requested_reattack_date?: string & bounds.#isoDate
	unreliable_priority?:                     int
	unreliable_reattack_cycles?:              int
	unreliable_report_date?:                  string & bounds.#isoDate
	unreliable_treatment_changes?:            int
}

#VulnerabilityZeroRisk: {
	comment_id:    string
	modified_by:   string
	modified_date: string & bounds.#isoDate
	status:        string
}

#VulnerabilitySeverityProposal: {
	severity_score: #SeverityScore
	modified_by:    string
	modified_date:  string & bounds.#isoDate
	status:         string
	justification?: string
}

#VulnerabilityMetadata: {
	created_by:               string
	created_date:             string & bounds.#isoDate
	group_name:               string
	hacker_email:             string
	organization_name:        string
	root_id:                  string
	state:                    #VulnerabilityState
	type:                     string
	author?:                  #VulnerabilityAuthor
	technique?:               string
	bug_tracking_system_url?: string
	custom_severity?:         int
	cwe_ids?: [...string]
	developer?:         string
	event_id?:          string
	hash?:              int
	severity_score?:    #SeverityScore
	skims_method?:      string
	skims_description?: string
	stream?: [...string]
	tags?: [...string]
	treatment?:            #VulnerabilityTreatment
	unreliable_indicators: #VulnerabilityUnreliableIndicators
	root_id_mismatch?:     bool
	verification?:         #VulnerabilityVerification
	webhook_url?:          string
	zero_risk?:            #VulnerabilityZeroRisk
}
