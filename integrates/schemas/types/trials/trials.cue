package trials

import (
	"fluidattacks.com/bounds"
)

#TrialMetadata: {
	email:          string
	completed:      bool
	extension_days: int

	extension_date?: string & bounds.#isoDate
	start_date?:     string & bounds.#isoDate
	reason?:         string
}
