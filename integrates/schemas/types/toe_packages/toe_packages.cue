package toe_packages

import (
	"fluidattacks.com/bounds"
)

#ToePackageCoordinates: {
	path:             string
	dependency_type?: string
	scope?:           string
	line?:            string
	layer?:           string
	image_ref?:       string
}

#ToePackageAdvisory: {
	cpes: [...string]
	description?: string
	id:           string
	namespace:    string
	severity:     string
	urls: [...string]
	version_constraint?: string
	percentile:          number
	epss:                number
}

#Digest: {
	algorithm?: string
	value?:     string
}

#Artifact: {
	url:        string
	integrity?: #Digest
}

#ToePackageHealthMetadata: {
	latest_version_created_at?: string & bounds.#isoDate
	latest_version?:            string
	authors?:                   string
	artifact?:                  #Artifact
}

#ToePackagesMetadata: {
	be_present:                   bool
	found_by?:                    string
	group_name:                   string
	organization_name:            string
	has_related_vulnerabilities?: bool
	health_metadata?:             #ToePackageHealthMetadata
	id:                           string
	language:                     string
	licenses?: [...string]
	locations?: [...#ToePackageCoordinates]
	modified_date: string & bounds.#isoDate
	name:          string
	outdated?:     bool
	package_advisories?: [...#ToePackageAdvisory]
	package_url?: string
	platform?:    string
	root_id:      string
	type_:        string
	url?:         string
	version:      string
	vulnerable?:  bool
	vulnerability_ids?: SS: [...string]
}
