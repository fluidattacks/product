package integration_repositories

import (
	"fluidattacks.com/bounds"
)

#OrganizationUnreliableIntegrationRepository: {
	url:    string
	branch: string

	name?:             string
	credential_id?:    string
	last_commit_date?: string & bounds.#isoDate
	commit_count?:     int
	branches?: [...string]
}
