package events

import (
	"fluidattacks.com/bounds"
)

#EventComment: {
	email:         string
	parent_id:     string
	content:       string
	id:            string
	group_name:    string
	creation_date: string & bounds.#isoDate
	full_name:     string
	event_id:      string
}

#EventState: {
	status:        string
	modified_date: string & bounds.#isoDate
	modified_by:   string
	comment_id?:   string
	other?:        string
	reason?:       string
}

#EventEvidence: {
	file_name:     string
	modified_date: string & bounds.#isoDate
}

#EventEvidences: {
	file_1?:  #EventEvidence
	image_1?: #EventEvidence
	image_2?: #EventEvidence
	image_3?: #EventEvidence
	image_4?: #EventEvidence
	image_5?: #EventEvidence
	image_6?: #EventEvidence
}

#EventMetadata: {
	client:           string
	created_by:       string
	created_date:     string & bounds.#isoDate
	description:      string
	environment_url?: string
	event_date:       string & bounds.#isoDate
	evidences:        #EventEvidences
	group_name:       string
	hacker:           string
	id:               string
	state:            #EventState
	type:             string
	environment_url?: string
	root_id?:         string
	n_holds?:         int
	solving_date?:    string & bounds.#isoDate
}
