package toe_inputs

import (
	"fluidattacks.com/bounds"
)

#ToeInputState: {
	be_present:           bool
	modified_date:        string & bounds.#isoDate
	attacked_at?:         string & bounds.#isoDate
	attacked_by?:         string
	be_present_until?:    string & bounds.#isoDate
	first_attack_at?:     string & bounds.#isoDate
	has_vulnerabilities?: bool
	modified_by?:         string
	seen_at?:             string & bounds.#isoDate
	seen_first_time_by?:  string
	unreliable_root_id?:  string // Likely to be removed
}

#ToeInputMetadata: {
	component:      string
	entry_point:    string
	group_name:     string
	root_id:        string
	environment_id: string
	state:          #ToeInputState
}
