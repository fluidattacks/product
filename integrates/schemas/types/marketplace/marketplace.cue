package marketplace

import (
	"fluidattacks.com/bounds"
)

#AWSMarketplaceSubscriptionEntitlement: {
	dimension:       string
	expiration_date: string & bounds.#isoDate
	value:           int
}

#AWSMarketplaceSubscriptionState: {
	entitlements: [...#AWSMarketplaceSubscriptionEntitlement]
	has_free_trial:    bool
	modified_date:     string & bounds.#isoDate
	status:            string
	private_offer_id?: string
}

#AwsMarketplaceSubscriptionMetadata: {
	created_at:      string & bounds.#isoDate
	state:           #AWSMarketplaceSubscriptionState
	aws_account_id?: string
	user?:           string
}
