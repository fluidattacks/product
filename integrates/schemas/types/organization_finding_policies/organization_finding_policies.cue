package organization_finding_policies

import (
	"fluidattacks.com/bounds"
)

#OrgFindingPolicyState: {
	status:        string
	modified_by:   string
	modified_date: string & bounds.#isoDate
}

#OrgFindingPolicyState: {
	status:        string
	modified_date: string & bounds.#isoDate
	modified_by:   string
}

#OrgFindingPolicyMetadata: {
	name: string
	tags?: [...string]
	treatment_acceptance: string
	state:                #OrgFindingPolicyState
	id?:                  string
	organization_name?:   string
}
