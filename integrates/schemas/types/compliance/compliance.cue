package compliance

#ComplianceStandard: {
	avg_organization_compliance_level:   number
	best_organization_compliance_level:  number
	standard_name:                       string
	worst_organization_compliance_level: number
}

#ComplianceUnreliableIndicators: {
	standards?: [...#ComplianceStandard]
}
