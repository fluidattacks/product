package organization_access

import (
	"fluidattacks.com/bounds"
)

#OrganizationInvitation: {
	is_used:   bool
	role:      string
	url_token: string
}

#OrganizationAccessState: {
	modified_date: string & bounds.#isoDate
	modified_by:   string
	has_access:    bool
	invitation?:   #OrganizationInvitation
	role?:         string
}

#OrganizationAccess: {
	email:            string & =~"[\\w.-@]+|__adminEmail__"
	organization_id:  string
	state:            #OrganizationAccessState
	expiration_time?: int
}
