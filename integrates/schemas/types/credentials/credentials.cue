package credentials

import (
	"fluidattacks.com/bounds"
)

#CredentialsState: {
	modified_by:         string
	modified_date:       string & bounds.#isoDate
	name:                string
	type:                string
	is_pat?:             bool
	owner:               string
	azure_organization?: string
}

#HttpsSecret: {
	user:     string
	password: string
}

#SshSecret: {
	key: string
}

#AWSRoleSecret: {
	arn: string
}

#HttpsPatSecret: {
	token: string
}

#OauthAzureSecret: {
	arefresh_token: string
	redirect_uri:   string
	access_token:   string
	valid_until:    string & bounds.#isoDate
}

#OauthBitbucketSecret: {
	brefresh_token: string
	access_token:   string
	valid_until:    string & bounds.#isoDate
}

#OauthGithubSecret: {
	access_token: string
}

#OauthGitlabSecret: {
	refresh_token: string
	redirect_uri:  string
	access_token:  string
	valid_until:   string & bounds.#isoDate
}

#Secret: #HttpsSecret | #HttpsPatSecret | #SshSecret | #AWSRoleSecret | #OauthAzureSecret | #OauthBitbucketSecret | #OauthGithubSecret | #OauthGitlabSecret

#CredentialsMetadata: {
	id:              string
	organization_id: string
	state:           #CredentialsState
	secret:          #Secret
}
