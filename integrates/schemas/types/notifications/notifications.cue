package notifications

import (
	"fluidattacks.com/bounds"
)

#NotificationMetadata: {
	format:           string
	id:               string
	name:             string
	notified_at:      string & bounds.#isoDate
	status:           string
	type:             string
	user_email:       string
	download_url?:    string
	expiration_time?: int
	s3_file_path?:    string
	size?:            int
}
