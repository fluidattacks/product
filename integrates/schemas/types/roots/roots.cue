package roots

import (
	"fluidattacks.com/bounds"
)

#GitRootState: {
	branch:      string
	environment: string
	gitignore: [...string]
	includes_health_check: bool
	modified_by:           string
	modified_date:         string & bounds.#isoDate
	nickname:              string
	status:                string
	url:                   string

	credential_id?: string
	criticality?:   string
	environment_urls?: [...string]
	other?:      string
	reason?:     string
	use_egress?: bool
	use_vpn?:    bool
	use_ztna?:   bool
}

#GitRootCloning: {
	modified_by:               string
	modified_date:             string & bounds.#isoDate
	reason:                    string
	status:                    string
	commit?:                   string
	commit_date?:              string & bounds.#isoDate
	failed_count?:             int
	first_failed_cloning?:     string & bounds.#isoDate
	first_successful_cloning?: string & bounds.#isoDate
	last_successful_cloning?:  string & bounds.#isoDate
	successful_count?:         int
}

#MachineExecutionsDate: {
	apk:  string & bounds.#isoDate
	cspm: string & bounds.#isoDate
	dast: string & bounds.#isoDate
	sast: string & bounds.#isoDate
	sca:  string & bounds.#isoDate
}

#GitRootMachine: {
	modified_by:           string
	modified_date:         string & bounds.#isoDate
	reason:                string
	status:                string
	commit?:               string
	commit_date?:          string & bounds.#isoDate
	execution_id?:         string
	last_executions_date?: #MachineExecutionsDate
}

#GitRootRebase: {
	modified_by:             string
	modified_date:           string & bounds.#isoDate
	reason:                  string
	status:                  string
	commit?:                 string
	commit_date?:            string & bounds.#isoDate
	last_successful_rebase?: string & bounds.#isoDate
}

#GitRootToeLines: {
	modified_by:              string
	modified_date:            string & bounds.#isoDate
	reason:                   string
	status:                   string
	commit?:                  string
	commit_date?:             string & bounds.#isoDate
	last_successful_refresh?: string & bounds.#isoDate
}

#CodeLanguage: {
	language: string
	loc:      int
}

#RootUnreliableIndicators: {...
	unreliable_code_languages?: [...#CodeLanguage]
	unreliable_last_status_update?: string & bounds.#isoDate
	nofluid_quantity?:              int
}

#GitRootMetadata: {
	cloning:               #GitRootCloning
	created_by:            string
	created_date:          string & bounds.#isoDate
	state:                 #GitRootState
	type:                  string
	machine?:              #GitRootMachine
	rebase?:               #GitRootRebase
	toe_lines?:            #GitRootToeLines
	unreliable_indicators: #RootUnreliableIndicators
}

#IpRootState: {
	modified_by:   string
	modified_date: string & bounds.#isoDate
	status:        string
	nickname:      string
	address:       string

	reason?: string
	other?:  string
}

#IpRootMetadata: {
	type:                   string
	created_date:           string & bounds.#isoDate
	state:                  #IpRootState
	created_by:             string
	unreliable_indicators?: #RootUnreliableIndicators
}

#DockerImageHistory: {
	created:      string
	created_by?:  string
	empty_layer?: bool
	comment?:     string
}

#RootDockerImageState: {
	modified_by:   string
	modified_date: string & bounds.#isoDate
	status:        string
	size:          int
	history: [...#DockerImageHistory]
	layers: {SS: [...string]}

	credential_id?: string
	digest?:        string
	include?:       bool
}

#RootDockerImageLayer: {
	digest:     string
	media_type: string
	size:       int
}

#RootDockerImage: {
	group_name: string
	root_id:    string
	state:      #RootDockerImageState
	created_at: string & bounds.#isoDate
	created_by: string
}

#SecretState: {
	owner:         string
	modified_by:   string
	modified_date: string & bounds.#isoDate
	description?:  string
}

#RootEnvironmentSecret: {
	description?: string // Must evaluate if actually required
	created_at:   string & bounds.#isoDate
	key:          string
	value:        string
	state:        #SecretState
}

#RootEnvironmentUrlState: {
	modified_by:    string
	modified_date:  string & bounds.#isoDate
	status:         string
	include:        bool
	url_type:       string
	cloud_name?:    string
	use_egress?:    bool
	use_vpn?:       bool
	use_ztna?:      bool
	is_production?: bool
}

#RootEnvironmentUrl: {
	id:         string
	group_name: string
	root_id:    string
	url:        string
	state:      #RootEnvironmentUrlState

	created_at?: string & bounds.#isoDate
	created_by?: string
}

#RootSecret: {
	created_at: string & bounds.#isoDate
	key:        string
	value:      string
	state:      #SecretState
}

#RootEnvironmentUrlState: {
	modified_by:   string
	modified_date: string & bounds.#isoDate
	status:        string
	include:       bool
	url_type:      string
	cloud_name?:   string
	use_egress?:   bool
	use_vpn?:      bool
	use_ztna?:     bool
}

#UrlRootState: {
	host:          string
	modified_by:   string
	modified_date: string & bounds.#isoDate
	nickname:      string
	path:          string
	port:          string
	protocol:      string
	status:        string
	excluded_sub_paths?: [...string]
	other?:  string
	query?:  string
	reason?: string
}

#UrlRootMetadata: {
	type:                   string
	created_date:           string & bounds.#isoDate
	unreliable_indicators?: #RootUnreliableIndicators
	state?:                 #UrlRootState
	created_by:             string
}
