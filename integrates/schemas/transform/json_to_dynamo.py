import json


class UnexpectedTypeError(Exception):
    pass


def _process_facet(facet: dict) -> None:
    if len(facet["TableData"]) == 0:
        return
    for j, item in enumerate(facet["TableData"]):
        facet["TableData"][j] = _transform_item(item)


def _process_table(table: dict) -> None:
    for facet in table["TableFacets"]:
        _process_facet(facet)


def _transform_item(item: dict | list) -> dict | list:
    if isinstance(item, dict):
        return _transform_dict(item)
    return _transform_list(item)


def _transform_data_model(json_obj: dict) -> None:
    for table in json_obj["DataModel"]:
        _process_table(table)


def _transform_dict(item: dict) -> dict:
    return {k: _simple_json_to_dynamodb_json(v) for k, v in item.items()}


def _transform_list(item: list) -> list:
    return [_simple_json_to_dynamodb_json(v) for v in item]


def _simple_json_to_dynamodb_json(
    obj: str | dict | list | int | float | bool,
) -> dict:
    type_mapping = {
        str: lambda x: {"S": x},
        bool: lambda x: {"BOOL": x},
        int: lambda x: {"N": str(x)},
        float: lambda x: {"N": str(x)},
        dict: (
            lambda x: {
                "M": {
                    k: _simple_json_to_dynamodb_json(v) for k, v in x.items()
                }
            }
            if not (len(x) == 1 and "SS" in x and isinstance(x["SS"], list))
            else x
        ),
        list: lambda x: {"L": [_simple_json_to_dynamodb_json(v) for v in x]},
    }

    obj_type = type(obj)
    if obj_type in type_mapping:
        return type_mapping[obj_type](obj)
    raise UnexpectedTypeError(f"Unexpected type: {obj_type}")


def handle_json_to_dynamodb(json_obj: dict) -> None:
    _transform_data_model(json_obj)
    print(json.dumps(json_obj, ensure_ascii=False, indent=2))
