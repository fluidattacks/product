# shellcheck shell=bash

function check_diff {
  local file_1=$1
  local file_2=$2
  if ! diff -q "$file_1" "$file_2" > /dev/null; then
    error "$(realpath "${file_2}") was not up to date:"
    jd -color "$file_1" "$file_2"
    return 1
  fi
  info "$(realpath "${file_2}") has not changed"
}

function main {
  local database_file="database-design.json"
  local main_file="main.cue"
  local result_file="result.json"
  : \
    && info "Exporting db schema" \
    && pushd integrates/schemas \
    && cue export $main_file -fo $result_file \
    && cat $result_file | python3 transform/__init__.py --to-dynamodb > $database_file \
    && info "Successfully exported db schema" \
    && rm $result_file \
    && check_diff __argDbDesign__ $database_file \
    && popd \
    || return 1
}

main "${@}"
