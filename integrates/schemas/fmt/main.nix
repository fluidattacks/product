{ inputs, makeScript, outputs, ... }:
makeScript {
  name = "integrates-schemas-fmt";
  entrypoint = ./entrypoint.sh;
  searchPaths.bin = [ outputs."/common/utils/cue" ];
}
