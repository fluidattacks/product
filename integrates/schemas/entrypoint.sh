# shellcheck shell=bash

function main {
  : \
    && integrates-schemas-fmt \
    && integrates-schemas-export \
    && integrates-schemas-gen-types \
    || return 1
}

main "${@}"
