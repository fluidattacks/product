package database_schema

#Type: "S" | "N" | "BOOL" | "M" | "L" | "SS"

#KeyAttributesValues:
	"pk" |
	"sk"

#attrValue: string | bool | number | [...#attrValue] | {[string]?: #attrValue}

#TypeMap: {// As required by dynamodb
	S:    string
	N:    number
	BOOL: bool
	M: {[string]: _}
	L: [...]
	SS: {SS: [...string]}
}

#TableItem: {
	pk:        string
	sk:        string
	[string]?: #attrValue
}

#Facet: {
	FacetName: string
	KeyAttributeAlias: {
		PartitionKeyAlias: string
		SortKeyAlias:      string
	}
	TableData: [...#TableItem]
	NonKeyAttributes?: [...string]
	DataAccess: _
}

#KeyDefinition: {
	AttributeName: string
	AttributeType: #Type
}

#KeyAttribute: {
	PartitionKey: {
		AttributeName: string
		AttributeType: #Type
	}
	SortKey: {
		AttributeName: string
		AttributeType: #Type
	}
}

#GSI: {
	IndexName:     string
	KeyAttributes: #KeyAttribute
	Projection: {
		ProjectionType: "ALL" | "KEYS_ONLY" | "INCLUDE"
	}
}

#Table: {
	TableName:     string
	KeyAttributes: #KeyAttribute
	NonKeyAttributes: [...#KeyDefinition]
	TableFacets: [...#Facet]
	GlobalSecondaryIndexes: [...#GSI]
	DataAccess:        _
	SampleDataFormats: _
	BillingMode:       "PAY_PER_REQUEST" | "PROVISIONED"
}

#DbSchema: {
	ModelName: string
	ModelMetadata: {
		Author:           string
		DateCreated:      string
		DateLastModified: string
		Description:      string
		AWSService:       string
		Version:          string
	}
	DataModel: [...#Table]
}
