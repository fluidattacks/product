{ inputs, makeSearchPaths, outputs, ... }: {
  deployTerraform = {
    modules = {
      integratesInfra = {
        setup = [
          (makeSearchPaths {
            bin =
              [ inputs.nixpkgs.poetry inputs.nixpkgs.python311Packages.pip ];
          })
          outputs."/secretsForAwsFromGitlab/prodIntegrates"
          outputs."/secretsForEnvFromSops/integratesInfra"
          outputs."/secretsForEnvFromSops/integratesInfraProd"
          outputs."/secretsForTerraformFromEnv/integratesInfraProd"
        ];
        src = "/integrates/infra/src";
        version = "1.0";
      };
    };
  };
  lintTerraform = {
    modules = {
      integratesInfra = {
        setup = [
          outputs."/secretsForAwsFromGitlab/dev"
          outputs."/secretsForEnvFromSops/integratesInfra"
          outputs."/secretsForEnvFromSops/integratesInfraDev"
          outputs."/secretsForTerraformFromEnv/integratesInfraDev"
        ];
        src = "/integrates/infra/src";
        version = "1.0";
      };
    };
  };
  secretsForEnvFromSops = {
    integratesInfra = {
      vars = [
        "BUGSNAG_API_KEY_STREAMS"
        "CLOUDFLARE_ACCOUNT_ID"
        "TWILIO_ACCOUNT_SID"
        "TWILIO_AUTH_TOKEN"
      ];
      manifest = "/integrates/secrets/dev.yaml";
    };
    integratesInfraDev = {
      vars = [
        "AWS_DYNAMODB_HOST_DEV"
        "AWS_OPENSEARCH_HOST_DEV"
        "AZURE_OAUTH2_ISSUES_SECRET_DEV"
        "CLOUDFLARE_API_TOKEN"
        "GITLAB_ISSUES_OAUTH2_APP_ID"
        "GITLAB_ISSUES_OAUTH2_SECRET"
        "GOOGLE_CHAT_WEBHOOK_URL_DEV"
        "WEBHOOK_POC_KEY_DEV"
        "WEBHOOK_POC_ORG_DEV"
        "WEBHOOK_POC_URL_DEV"
      ];
      manifest = "/integrates/secrets/dev.yaml";
    };
    integratesInfraProd = {
      vars = [
        "AWS_AURORA_ENDPOINT"
        "AWS_DYNAMODB_HOST"
        "AWS_OPENSEARCH_HOST"
        "AZURE_OAUTH2_ISSUES_SECRET"
        "AZURE_OAUTH2_ISSUES_SECRET_OLD"
        "CLOUDFLARE_API_TOKEN_INTEGRATES"
        "GITLAB_ISSUES_OAUTH2_APP_ID_PROD"
        "GITLAB_ISSUES_OAUTH2_SECRET_PROD"
        "GOOGLE_CHAT_WEBHOOK_URL"
        "WEBHOOK_POC_KEY"
        "WEBHOOK_POC_ORG"
        "WEBHOOK_POC_URL"
      ];
      manifest = "/integrates/secrets/prod.yaml";
    };
  };
  secretsForTerraformFromEnv = {
    integratesInfraDev = {
      aws_dynamodb_host = "AWS_DYNAMODB_HOST_DEV";
      aws_opensearch_host = "AWS_OPENSEARCH_HOST_DEV";
      azure_oauth2_issues_secret = "AZURE_OAUTH2_ISSUES_SECRET_DEV";
      azure_oauth2_issues_secret_old = "AZURE_OAUTH2_ISSUES_SECRET_DEV";
      bugsnag_api_key_streams = "BUGSNAG_API_KEY_STREAMS";
      ci_commit_sha = "CI_COMMIT_SHA";
      cloudflare_api_token = "CLOUDFLARE_API_TOKEN";
      gitlab_issues_oauth2_app_id = "GITLAB_ISSUES_OAUTH2_APP_ID";
      gitlab_issues_oauth2_secret = "GITLAB_ISSUES_OAUTH2_SECRET";
      google_chat_webhook_url = "GOOGLE_CHAT_WEBHOOK_URL_DEV";
      twilio_account_sid = "TWILIO_ACCOUNT_SID";
      twilio_auth_token = "TWILIO_AUTH_TOKEN";
      webhook_poc_key = "WEBHOOK_POC_KEY_DEV";
      webhook_poc_org = "WEBHOOK_POC_ORG_DEV";
      webhook_poc_url = "WEBHOOK_POC_URL_DEV";
    };
    integratesInfraProd = {
      aws_aurora_endpoint = "AWS_AURORA_ENDPOINT";
      aws_dynamodb_host = "AWS_DYNAMODB_HOST";
      aws_opensearch_host = "AWS_OPENSEARCH_HOST";
      azure_oauth2_issues_secret = "AZURE_OAUTH2_ISSUES_SECRET";
      azure_oauth2_issues_secret_old = "AZURE_OAUTH2_ISSUES_SECRET_OLD";
      bugsnag_api_key_streams = "BUGSNAG_API_KEY_STREAMS";
      ci_commit_sha = "CI_COMMIT_SHA";
      cloudflare_api_token = "CLOUDFLARE_API_TOKEN_INTEGRATES";
      gitlab_issues_oauth2_app_id = "GITLAB_ISSUES_OAUTH2_APP_ID_PROD";
      gitlab_issues_oauth2_secret = "GITLAB_ISSUES_OAUTH2_SECRET_PROD";
      google_chat_webhook_url = "GOOGLE_CHAT_WEBHOOK_URL";
      twilio_account_sid = "TWILIO_ACCOUNT_SID";
      twilio_auth_token = "TWILIO_AUTH_TOKEN";
      webhook_poc_key = "WEBHOOK_POC_KEY";
      webhook_poc_org = "WEBHOOK_POC_ORG";
      webhook_poc_url = "WEBHOOK_POC_URL";
    };
  };
  testTerraform = {
    modules = {
      integratesInfra = {
        setup = [
          outputs."/secretsForAwsFromGitlab/dev"
          outputs."/secretsForEnvFromSops/integratesInfra"
          outputs."/secretsForEnvFromSops/integratesInfraDev"
          outputs."/secretsForTerraformFromEnv/integratesInfraDev"
        ];
        src = "/integrates/infra/src";
        version = "1.0";
      };
    };
  };
}
