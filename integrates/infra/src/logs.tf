resource "aws_cloudwatch_log_group" "fluid" {
  name              = "FLUID"
  retention_in_days = 3653

  tags = {
    "Name"              = "FLUID"
    "fluidattacks:line" = "cost"
    "fluidattacks:comp" = "integrates"
  }
}

resource "aws_cloudwatch_log_stream" "streams" {
  name           = "streams_hooks"
  log_group_name = aws_cloudwatch_log_group.fluid.name
}

resource "aws_cloudwatch_log_group" "integrates" {
  name              = "integrates"
  retention_in_days = 3653

  tags = {
    "Name"              = "integrates"
    "fluidattacks:line" = "cost"
    "fluidattacks:comp" = "integrates"
  }
}

resource "aws_cloudwatch_log_data_protection_policy" "integrates" {
  log_group_name  = aws_cloudwatch_log_group.integrates.name
  policy_document = data.aws_cloudwatch_log_data_protection_policy_document.integrates.json
}

data "aws_cloudwatch_log_data_protection_policy_document" "integrates" {
  name = "integrates_policy_document"

  statement {
    sid = "audit"

    data_identifiers = [
      "arn:aws:dataprotection::aws:data-identifier/Address",
      "arn:aws:dataprotection::aws:data-identifier/AwsSecretKey",
      "arn:aws:dataprotection::aws:data-identifier/CreditCardExpiration",
      "arn:aws:dataprotection::aws:data-identifier/CreditCardNumber",
      "arn:aws:dataprotection::aws:data-identifier/CreditCardSecurityCode",
      "arn:aws:dataprotection::aws:data-identifier/IpAddress",
      "arn:aws:dataprotection::aws:data-identifier/LatLong",
      "arn:aws:dataprotection::aws:data-identifier/Name",
      "arn:aws:dataprotection::aws:data-identifier/OpenSshPrivateKey",
      "arn:aws:dataprotection::aws:data-identifier/PgpPrivateKey",
      "arn:aws:dataprotection::aws:data-identifier/PkcsPrivateKey",
      "arn:aws:dataprotection::aws:data-identifier/PuttyPrivateKey",
      "arn:aws:dataprotection::aws:data-identifier/VehicleIdentificationNumber",
    ]

    operation {
      audit {
        findings_destination {

        }
      }
    }
  }

  statement {
    sid = "deidentify"

    data_identifiers = [
      "arn:aws:dataprotection::aws:data-identifier/Address",
      "arn:aws:dataprotection::aws:data-identifier/AwsSecretKey",
      "arn:aws:dataprotection::aws:data-identifier/CreditCardExpiration",
      "arn:aws:dataprotection::aws:data-identifier/CreditCardNumber",
      "arn:aws:dataprotection::aws:data-identifier/CreditCardSecurityCode",
      "arn:aws:dataprotection::aws:data-identifier/IpAddress",
      "arn:aws:dataprotection::aws:data-identifier/LatLong",
      "arn:aws:dataprotection::aws:data-identifier/Name",
      "arn:aws:dataprotection::aws:data-identifier/OpenSshPrivateKey",
      "arn:aws:dataprotection::aws:data-identifier/PgpPrivateKey",
      "arn:aws:dataprotection::aws:data-identifier/PkcsPrivateKey",
      "arn:aws:dataprotection::aws:data-identifier/PuttyPrivateKey",
      "arn:aws:dataprotection::aws:data-identifier/VehicleIdentificationNumber",
    ]

    operation {
      deidentify {
        mask_config {}
      }
    }
  }
}

resource "aws_cloudwatch_log_stream" "default" {
  name           = "default"
  log_group_name = aws_cloudwatch_log_group.integrates.name
}

resource "aws_cloudwatch_log_stream" "cloning" {
  name           = "cloning"
  log_group_name = aws_cloudwatch_log_group.integrates.name
}

resource "aws_cloudwatch_log_stream" "db_streams" {
  name           = "db_streams"
  log_group_name = aws_cloudwatch_log_group.integrates.name
}

resource "aws_cloudwatch_log_stream" "machine" {
  name           = "machine"
  log_group_name = aws_cloudwatch_log_group.integrates.name
}

resource "aws_cloudwatch_log_stream" "rebase" {
  name           = "rebase"
  log_group_name = aws_cloudwatch_log_group.integrates.name
}

resource "aws_cloudwatch_log_stream" "refresh_toe_lines" {
  name           = "refresh_toe_lines"
  log_group_name = aws_cloudwatch_log_group.integrates.name
}

resource "aws_cloudwatch_log_stream" "schedulers" {
  name           = "schedulers"
  log_group_name = aws_cloudwatch_log_group.integrates.name
}

resource "aws_cloudwatch_log_stream" "sqs_tasks" {
  name           = "sqs_tasks"
  log_group_name = aws_cloudwatch_log_group.integrates.name
}
resource "aws_cloudwatch_log_stream" "process_sbom" {
  name           = "process_sbom"
  log_group_name = aws_cloudwatch_log_group.integrates.name
}

resource "aws_cloudwatch_log_group" "opensearch" {
  name              = "opensearch"
  retention_in_days = 3653

  tags = {
    "Name"              = "opensearch"
    "fluidattacks:line" = "cost"
    "fluidattacks:comp" = "integrates"
  }
}

data "aws_iam_policy_document" "opensearch-log-policy" {
  statement {
    actions = [
      "logs:CreateLogStream",
      "logs:PutLogEvents",
      "logs:PutLogEventsBatch",
    ]

    resources = ["arn:aws:logs:us-east-1:${data.aws_caller_identity.current.account_id}:log-group:opensearch*", ]

    principals {
      identifiers = ["es.amazonaws.com"]
      type        = "Service"
    }
  }
}

resource "aws_cloudwatch_log_resource_policy" "opensearch-log-policy" {
  policy_document = data.aws_iam_policy_document.opensearch-log-policy.json
  policy_name     = "opensearch-log-policy"
}

resource "aws_cloudwatch_log_metric_filter" "async_server_cloning_started" {
  name           = "CloningTasksStarted"
  pattern        = "{ ($.task_name = \"clone\") && ($.status = \"STARTED\") }"
  log_group_name = aws_cloudwatch_log_group.integrates.name

  metric_transformation {
    name          = "CloningTasksStarted"
    namespace     = "AsyncServerCloning"
    value         = "1"
    default_value = "0"
  }
}

resource "aws_cloudwatch_log_metric_filter" "async_server_cloning_finished" {
  name           = "CloningTasksFinished"
  pattern        = "{ ($.task_name = \"clone\") && ($.status = \"FINISHED\") }"
  log_group_name = aws_cloudwatch_log_group.integrates.name

  metric_transformation {
    name          = "CloningTasksFinished"
    namespace     = "AsyncServerCloning"
    value         = "1"
    default_value = "0"
  }
}

resource "aws_cloudwatch_log_metric_filter" "async_server_cloning_failed" {
  name           = "CloningTasksFailed"
  pattern        = "{ ($.task_name = \"clone\") && ($.result = \"FAILED\") }"
  log_group_name = aws_cloudwatch_log_group.integrates.name

  metric_transformation {
    name          = "CloningTasksFailed"
    namespace     = "AsyncServerCloning"
    value         = "1"
    default_value = "0"
  }
}

resource "aws_cloudwatch_log_metric_filter" "async_server_refresh_toe_lines_started" {
  name           = "RefreshToELinesTasksStarted"
  pattern        = "{ ($.task_name = \"refresh_toe_lines\") && ($.status = \"STARTED\") }"
  log_group_name = aws_cloudwatch_log_group.integrates.name

  metric_transformation {
    name          = "RefreshToELinesTasksStarted"
    namespace     = "AsyncServerCloning"
    value         = "1"
    default_value = "0"
  }
}

resource "aws_cloudwatch_log_metric_filter" "async_server_refresh_toe_lines_finished" {
  name           = "RefreshToELinesTasksFinished"
  pattern        = "{ ($.task_name = \"refresh_toe_lines\") && ($.status = \"FINISHED\") }"
  log_group_name = aws_cloudwatch_log_group.integrates.name

  metric_transformation {
    name          = "RefreshToELinesTasksFinished"
    namespace     = "AsyncServerCloning"
    value         = "1"
    default_value = "0"
  }
}

resource "aws_cloudwatch_log_metric_filter" "async_server_refresh_toe_lines_failed" {
  name           = "RefreshToELinesTasksFailed"
  pattern        = "{ ($.task_name = \"refresh_toe_lines\") && ($.result = \"FAILED\") }"
  log_group_name = aws_cloudwatch_log_group.integrates.name

  metric_transformation {
    name          = "RefreshToELinesTasksFailed"
    namespace     = "AsyncServerCloning"
    value         = "1"
    default_value = "0"
  }
}

resource "aws_cloudwatch_log_group" "audit_opensearch" {
  name              = "audit_opensearch"
  retention_in_days = 365

  tags = {
    "Name"              = "audit_opensearch"
    "fluidattacks:line" = "cost"
    "fluidattacks:comp" = "integrates"
  }
}
