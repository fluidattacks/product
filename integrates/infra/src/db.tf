resource "aws_dynamodb_table" "integrates_vms" {
  name                        = "integrates_vms"
  billing_mode                = "PAY_PER_REQUEST"
  hash_key                    = "pk"
  range_key                   = "sk"
  stream_enabled              = true
  stream_view_type            = "NEW_AND_OLD_IMAGES"
  deletion_protection_enabled = true

  attribute {
    name = "pk"
    type = "S"
  }

  attribute {
    name = "sk"
    type = "S"
  }

  attribute {
    name = "pk_hash"
    type = "S"
  }

  attribute {
    name = "sk_hash"
    type = "S"
  }

  attribute {
    name = "pk_2"
    type = "S"
  }

  attribute {
    name = "sk_2"
    type = "S"
  }

  attribute {
    name = "pk_3"
    type = "S"
  }

  attribute {
    name = "sk_3"
    type = "S"
  }

  attribute {
    name = "pk_4"
    type = "S"
  }

  attribute {
    name = "sk_4"
    type = "S"
  }

  attribute {
    name = "pk_5"
    type = "S"
  }

  attribute {
    name = "sk_5"
    type = "S"
  }

  attribute {
    name = "pk_6"
    type = "S"
  }

  attribute {
    name = "sk_6"
    type = "S"
  }

  global_secondary_index {
    name            = "inverted_index"
    hash_key        = "sk"
    range_key       = "pk"
    projection_type = "ALL"
  }
  global_secondary_index {
    name            = "gsi_hash"
    hash_key        = "pk_hash"
    range_key       = "sk_hash"
    projection_type = "ALL"
  }

  global_secondary_index {
    name            = "gsi_2"
    hash_key        = "pk_2"
    range_key       = "sk_2"
    projection_type = "ALL"
  }

  global_secondary_index {
    name            = "gsi_3"
    hash_key        = "pk_3"
    range_key       = "sk_3"
    projection_type = "ALL"
  }

  global_secondary_index {
    name            = "gsi_4"
    hash_key        = "pk_4"
    range_key       = "sk_4"
    projection_type = "ALL"
  }

  global_secondary_index {
    name            = "gsi_5"
    hash_key        = "pk_5"
    range_key       = "sk_5"
    projection_type = "ALL"
  }

  global_secondary_index {
    name            = "gsi_6"
    hash_key        = "pk_6"
    range_key       = "sk_6"
    projection_type = "ALL"
  }

  point_in_time_recovery {
    enabled = true
  }

  server_side_encryption {
    enabled = true
  }

  tags = {
    "Name"              = "integrates_vms"
    "fluidattacks:line" = "cost"
    "fluidattacks:comp" = "integrates"
  }

  ttl {
    attribute_name = "expiration_time"
    enabled        = true
  }
}

resource "aws_dynamodb_table" "integrates_vms_historic" {
  name                        = "integrates_vms_historic"
  billing_mode                = "PAY_PER_REQUEST"
  deletion_protection_enabled = true
  hash_key                    = "pk"
  range_key                   = "sk"
  stream_enabled              = true
  stream_view_type            = "NEW_AND_OLD_IMAGES"
  table_class                 = "STANDARD_INFREQUENT_ACCESS"

  attribute {
    name = "pk"
    type = "S"
  }
  attribute {
    name = "sk"
    type = "S"
  }
  attribute {
    name = "pk_2"
    type = "S"
  }
  attribute {
    name = "sk_2"
    type = "S"
  }
  attribute {
    name = "pk_3"
    type = "S"
  }
  attribute {
    name = "sk_3"
    type = "S"
  }

  global_secondary_index {
    name            = "gsi_2"
    hash_key        = "pk_2"
    range_key       = "sk_2"
    projection_type = "ALL"
  }
  global_secondary_index {
    name            = "gsi_3"
    hash_key        = "pk_3"
    range_key       = "sk_3"
    projection_type = "ALL"
  }

  point_in_time_recovery {
    enabled = true
  }

  server_side_encryption {
    enabled = true
  }

  tags = {
    "Name"              = "integrates_vms_historic"
    "fluidattacks:line" = "cost"
    "fluidattacks:comp" = "integrates"
  }

  ttl {
    attribute_name = "expiration_time"
    enabled        = true
  }
}

resource "aws_dynamodb_table" "async_processing" {
  name                        = "fi_async_processing"
  billing_mode                = "PAY_PER_REQUEST"
  hash_key                    = "pk"
  deletion_protection_enabled = true

  attribute {
    name = "action_name"
    type = "S"
  }
  attribute {
    name = "entity"
    type = "S"
  }
  attribute {
    name = "pk"
    type = "S"
  }

  global_secondary_index {
    name            = "gsi-1"
    hash_key        = "action_name"
    range_key       = "entity"
    projection_type = "ALL"
  }


  point_in_time_recovery {
    enabled = true
  }

  server_side_encryption {
    enabled = true
  }
  tags = {
    "Name"              = "fi_async_processing"
    "fluidattacks:line" = "cost"
    "fluidattacks:comp" = "integrates"
  }
}
resource "aws_dynamodb_table" "llm_scan" {
  name                        = "llm_scan"
  billing_mode                = "PAY_PER_REQUEST"
  hash_key                    = "pk"
  range_key                   = "sk"
  deletion_protection_enabled = true

  attribute {
    name = "pk"
    type = "S"
  }
  attribute {
    name = "sk"
    type = "S"
  }

  global_secondary_index {
    name            = "inverted_index"
    hash_key        = "sk"
    range_key       = "pk"
    projection_type = "ALL"
  }

  point_in_time_recovery {
    enabled = true
  }

  server_side_encryption {
    enabled = true
  }
  tags = {
    "Name"              = "llm_scan"
    "fluidattacks:line" = "cost"
    "fluidattacks:comp" = "integrates"
  }
}


resource "aws_security_group" "integrates-aurora" {
  name   = "integrates-aurora"
  vpc_id = data.aws_vpc.main.id

  ingress {
    from_port   = 5432
    to_port     = 5432
    protocol    = "tcp"
    cidr_blocks = [data.aws_vpc.main.cidr_block]
  }

  egress {
    from_port   = 5432
    to_port     = 5432
    protocol    = "tcp"
    cidr_blocks = [data.aws_vpc.main.cidr_block]
  }

  tags = {
    "Name"              = "integrates-aurora"
    "fluidattacks:line" = "research"
    "fluidattacks:comp" = "integrates"
    "NOFLUID"           = "f024_same_CIDR_RFC-1918_VPC"
  }
}

resource "aws_db_subnet_group" "integrates-aurora" {
  name = "integrates-aurora"
  subnet_ids = [
    for subnet in data.aws_subnet.main : subnet.id
  ]

  tags = {
    "Name"              = "integrates-aurora"
    "fluidattacks:line" = "research"
    "fluidattacks:comp" = "integrates"
  }
}

resource "aws_rds_cluster_parameter_group" "integrates-aurora-pg" {
  name        = "integrates-cluster-aurora-pg"
  family      = "aurora-postgresql16"
  description = "Custom parameter group for integrates Aurora"

  parameter {
    name  = "rds.force_ssl"
    value = "1"
  }
  tags = {
    "Name"              = "integrates-aurora-db_parameter_group"
    "fluidattacks:line" = "research"
    "fluidattacks:comp" = "integrates"
  }
}

resource "aws_rds_cluster" "integrates-aurora" {
  apply_immediately                   = true
  backup_retention_period             = 35
  cluster_identifier                  = "integrates"
  database_name                       = "integrates"
  db_cluster_parameter_group_name     = aws_rds_cluster_parameter_group.integrates-aurora-pg.name
  db_subnet_group_name                = aws_db_subnet_group.integrates-aurora.name
  deletion_protection                 = true
  enabled_cloudwatch_logs_exports     = ["postgresql"]
  engine                              = "aurora-postgresql"
  engine_mode                         = "provisioned"
  engine_version                      = "16.6"
  final_snapshot_identifier           = "integrates-final-snapshot"
  iam_database_authentication_enabled = true
  manage_master_user_password         = true
  master_username                     = "integrates"
  performance_insights_enabled        = true
  storage_encrypted                   = true
  vpc_security_group_ids              = [aws_security_group.integrates-aurora.id]

  serverlessv2_scaling_configuration {
    max_capacity             = 256
    min_capacity             = 0
    seconds_until_auto_pause = 300
  }

  tags = {
    "Name"              = "integrates-aurora"
    "fluidattacks:line" = "research"
    "fluidattacks:comp" = "integrates"
  }
}

resource "aws_rds_cluster_instance" "integrates-aurora" {
  count               = 2
  cluster_identifier  = aws_rds_cluster.integrates-aurora.id
  engine              = aws_rds_cluster.integrates-aurora.engine
  engine_version      = aws_rds_cluster.integrates-aurora.engine_version
  instance_class      = "db.serverless"
  monitoring_interval = 60
  monitoring_role_arn = aws_iam_role.rds_enhanced_monitoring.arn

  tags = {
    "Name"              = "integrates-aurora"
    "fluidattacks:line" = "research"
    "fluidattacks:comp" = "integrates"
  }
}

resource "aws_iam_role" "rds_enhanced_monitoring" {
  name               = "rds_enhanced_monitoring"
  assume_role_policy = data.aws_iam_policy_document.rds_enhanced_monitoring.json
  tags = {
    "Name"              = "integrates-aurora_monitoring_role"
    "fluidattacks:line" = "research"
    "fluidattacks:comp" = "integrates"
  }
}


resource "aws_iam_role_policy_attachment" "rds_enhanced_monitoring" {
  role       = aws_iam_role.rds_enhanced_monitoring.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonRDSEnhancedMonitoringRole"
}

data "aws_iam_policy_document" "rds_enhanced_monitoring" {
  statement {
    actions = [
      "sts:AssumeRole",
    ]

    effect = "Allow"

    principals {
      type        = "Service"
      identifiers = ["monitoring.rds.amazonaws.com"]
    }
  }
}

resource "aws_cloudwatch_metric_alarm" "rds_cpu_utilization" {
  count               = 2
  alarm_name          = "RDS_CPU_Utilization_Alarm_${count.index}"
  comparison_operator = "GreaterThanThreshold"
  evaluation_periods  = "1"
  metric_name         = "CPUUtilization"
  namespace           = "AWS/RDS"
  period              = "300"
  statistic           = "Average"
  threshold           = "80"
  alarm_description   = "Alarm when CPU utilization exceeds 80% for RDS instance"
  alarm_actions       = [aws_sns_topic.rds_alarms.arn]

  dimensions = {
    DBInstanceIdentifier = aws_rds_cluster_instance.integrates-aurora[count.index].id
  }
  tags = {
    "Name"              = "integrates-aurora_cpu_metric_alarm"
    "fluidattacks:line" = "research"
    "fluidattacks:comp" = "integrates"
  }
}

resource "aws_sns_topic" "rds_alarms" {
  name              = "rds-cpu-utilization-alarms"
  kms_master_key_id = "alias/aws/sns"
  tags = {
    "Name"              = "integrates-aurora_cpu_sns_topic"
    "fluidattacks:line" = "research"
    "fluidattacks:comp" = "integrates"
  }
}

resource "aws_sns_topic_policy" "rds_alarms_policy" {
  arn = aws_sns_topic.rds_alarms.arn

  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Effect = "Allow"
        Principal = {
          Service = "cloudwatch.amazonaws.com"
        }
        Action   = "sns:Publish"
        Resource = aws_sns_topic.rds_alarms.arn
      }
    ]
  })
}

resource "aws_sns_topic_subscription" "email_alerts" {
  topic_arn = aws_sns_topic.rds_alarms.arn
  protocol  = "email"
  endpoint  = "development@fluidattacks.com"
}

resource "aws_cloudwatch_metric_alarm" "disk_queue_depth_alarm" {
  count               = 2
  alarm_name          = "RDS_DiskQueueDepth_Alarm_${count.index}"
  comparison_operator = "GreaterThanThreshold"
  evaluation_periods  = "1"
  metric_name         = "DiskQueueDepth"
  namespace           = "AWS/RDS"
  period              = "300"
  statistic           = "Average"
  threshold           = "5"
  alarm_description   = "Alarm when DiskQueueDepth exceeds 5"
  alarm_actions       = [aws_sns_topic.rds_alarms.arn]

  dimensions = {
    DBInstanceIdentifier = aws_rds_cluster_instance.integrates-aurora[count.index].id
  }
  tags = {
    "Name"              = "integrates-aurora_disk_depth_metric_alarm"
    "fluidattacks:line" = "research"
    "fluidattacks:comp" = "integrates"
  }
}

resource "aws_cloudwatch_metric_alarm" "read_iops_alarm" {
  count               = 2
  alarm_name          = "RDS_ReadIOPS_Alarm_${count.index}"
  comparison_operator = "GreaterThanThreshold"
  evaluation_periods  = "1"
  metric_name         = "ReadIOPS"
  namespace           = "AWS/RDS"
  period              = "300"
  statistic           = "Average"
  threshold           = "100"
  alarm_description   = "Alarm when ReadIOPS exceeds 100"
  alarm_actions       = [aws_sns_topic.rds_alarms.arn]

  dimensions = {
    DBInstanceIdentifier = aws_rds_cluster_instance.integrates-aurora[count.index].id
  }
  tags = {
    "Name"              = "integrates-aurora_read_iops_metric_alarm"
    "fluidattacks:line" = "research"
    "fluidattacks:comp" = "integrates"
  }
}

resource "aws_cloudwatch_metric_alarm" "write_iops_alarm" {
  count               = 2
  alarm_name          = "RDS_WriteIOPS_Alarm_${count.index}"
  comparison_operator = "GreaterThanThreshold"
  evaluation_periods  = "1"
  metric_name         = "WriteIOPS"
  namespace           = "AWS/RDS"
  period              = "300"
  statistic           = "Average"
  threshold           = "100"
  alarm_description   = "Alarm when WriteIOPS exceeds 100"
  alarm_actions       = [aws_sns_topic.rds_alarms.arn]

  dimensions = {
    DBInstanceIdentifier = aws_rds_cluster_instance.integrates-aurora[count.index].id
  }
  tags = {
    "Name"              = "integrates-aurora_write_iops_metric_alarm"
    "fluidattacks:line" = "research"
    "fluidattacks:comp" = "integrates"
  }
}
resource "aws_cloudwatch_metric_alarm" "freeable_memory_alarm" {
  count               = 2
  alarm_name          = "RDS_FreeableMemoryAlarm_${count.index}"
  comparison_operator = "LessThanThreshold"
  evaluation_periods  = "1"
  metric_name         = "FreeableMemory"
  namespace           = "AWS/RDS"
  period              = "300"
  statistic           = "Average"
  threshold           = "500000000"
  alarm_description   = "Alarm when RDS instance freeable memory is low"
  alarm_actions       = [aws_sns_topic.rds_alarms.arn]
  dimensions = {
    DBInstanceIdentifier = aws_rds_cluster_instance.integrates-aurora[count.index].id
  }
  tags = {
    "Name"              = "integrates-aurora_freeable_metric_alarm"
    "fluidattacks:line" = "research"
    "fluidattacks:comp" = "integrates"
  }
}
resource "aws_cloudwatch_metric_alarm" "rds_free_storage_space" {
  count               = 2
  alarm_name          = "RDS_FreeStorageSpaceAlarm-${count.index}"
  comparison_operator = "LessThanThreshold"
  evaluation_periods  = "1"
  metric_name         = "FreeLocalStorage"
  namespace           = "AWS/RDS"
  period              = "300"
  statistic           = "Average"
  threshold           = "1000000000"
  alarm_description   = "Alarm when RDS instance free storage space is low"
  alarm_actions       = [aws_sns_topic.rds_alarms.arn]
  dimensions = {
    DBInstanceIdentifier = aws_rds_cluster_instance.integrates-aurora[count.index].id
  }
  tags = {
    "Name"              = "integrates-aurora_free_storage_space_alarm"
    "fluidattacks:line" = "research"
    "fluidattacks:comp" = "integrates"
  }
}
