resource "aws_security_group" "integrates-opensearch" {
  name   = "integrates-opensearch"
  vpc_id = data.aws_vpc.main.id

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = [data.aws_vpc.main.cidr_block]
  }

  egress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = [data.aws_vpc.main.cidr_block]
  }

  tags = {
    "Name"              = "integrates-opensearch"
    "fluidattacks:line" = "cost"
    "fluidattacks:comp" = "integrates"
    "NOFLUID"           = "f024_same_CIDR_RFC-1918_VPC"
  }
}

resource "aws_iam_service_linked_role" "integrates-opensearch" {
  aws_service_name = "opensearchservice.amazonaws.com"
  tags = {
    "Name"              = "linked_role_integrates-opensearch"
    "fluidattacks:line" = "cost"
    "fluidattacks:comp" = "integrates"
  }
}

resource "aws_opensearch_domain" "integrates" {
  depends_on     = [aws_iam_service_linked_role.integrates-opensearch]
  domain_name    = "integrates"
  engine_version = "OpenSearch_2.15" # Keep in sync with db/opensearch/main.nix -> version

  cluster_config {
    dedicated_master_count   = 3
    dedicated_master_enabled = true
    dedicated_master_type    = "r6gd.large.search"
    instance_count           = 3
    instance_type            = "r6gd.large.search"
    zone_awareness_enabled   = true

    zone_awareness_config {
      availability_zone_count = length(data.aws_subnet.main)
    }
  }

  domain_endpoint_options {
    enforce_https       = true
    tls_security_policy = "Policy-Min-TLS-1-2-2019-07"
  }

  ebs_options {
    ebs_enabled = false
  }

  encrypt_at_rest {
    enabled = true
  }

  log_publishing_options {
    cloudwatch_log_group_arn = aws_cloudwatch_log_group.opensearch.arn
    enabled                  = false
    log_type                 = "INDEX_SLOW_LOGS"
  }

  log_publishing_options {
    cloudwatch_log_group_arn = aws_cloudwatch_log_group.opensearch.arn
    enabled                  = false
    log_type                 = "SEARCH_SLOW_LOGS"
  }

  log_publishing_options {
    cloudwatch_log_group_arn = aws_cloudwatch_log_group.opensearch.arn
    log_type                 = "ES_APPLICATION_LOGS"
  }

  off_peak_window_options {
    enabled = true

    off_peak_window {
      window_start_time {
        hours   = 0
        minutes = 0
      }
    }
  }

  software_update_options {
    auto_software_update_enabled = true
  }

  vpc_options {
    security_group_ids = [
      aws_security_group.integrates-opensearch.id,
    ]

    subnet_ids = [
      for subnet in data.aws_subnet.main : subnet.id
    ]
  }

  tags = {
    "Name"              = "integrates-opensearch"
    "fluidattacks:line" = "cost"
    "fluidattacks:comp" = "integrates"
    "NOFLUID"           = "f031.f165.f400_non_public_domain_restricted_by_ztna_network"
  }
}
