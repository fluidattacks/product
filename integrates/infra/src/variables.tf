data "aws_caller_identity" "current" {}
data "aws_vpc" "main" {
  filter {
    name   = "tag:Name"
    values = ["fluid-vpc"]
  }
}
data "aws_subnet" "lambda" {
  for_each = toset([
    "lambda_1",
    "lambda_2",
  ])

  vpc_id = data.aws_vpc.main.id
  filter {
    name   = "tag:Name"
    values = [each.key]
  }
}
data "aws_subnet" "main" {
  for_each = toset([
    "k8s_1",
    "k8s_2",
    "k8s_3",
  ])

  vpc_id = data.aws_vpc.main.id
  filter {
    name   = "tag:Name"
    values = [each.key]
  }
}
data "cloudflare_zones" "fluidattacks_com" {
  filter {
    name = "fluidattacks.com"
  }
}
data "cloudflare_ip_ranges" "cloudflare" {
  lifecycle {
    postcondition {
      condition     = length(self.cidr_blocks) > 0
      error_message = "CloudFlare's API returned empty list of IPs"
    }
  }
}

variable "cloudflare_api_token" {
  type = string
}
variable "twilio_account_sid" {
  type = string
}
variable "twilio_auth_token" {
  type = string
}
variable "region" {
  default = "us-east-1"
  type    = string
}

variable "accountId" {
  default = "205810638802"
  type    = string
}
