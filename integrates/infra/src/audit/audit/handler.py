import json
import os
from typing import Any, TypedDict

import boto3
import bugsnag
import psycopg
from psycopg.types.json import Jsonb

FI_AWS_AURORA_ENDPOINT = os.environ["AWS_AURORA_ENDPOINT"]
FI_BUGSNAG_API_KEY_STREAMS = os.environ["BUGSNAG_API_KEY_STREAMS"]

bugsnag.configure(api_key=FI_BUGSNAG_API_KEY_STREAMS)


class Attributes(TypedDict):
    MessageDeduplicationId: str
    SenderId: str


class Record(TypedDict):
    attributes: Attributes
    body: str
    messageId: str


class Event(TypedDict):
    Records: list[Record]


def _sanitize_metadata(metadata: dict[str, Any]) -> dict[str, Any]:
    if "state" in metadata and metadata["state"].get("snippet"):
        # The jsonb type rejects null and surrogate unicode characters
        # https://www.postgresql.org/docs/current/datatype-json.html
        snippet = metadata["state"]["snippet"]
        return {
            **metadata,
            "state": {
                **metadata["state"],
                "snippet": {
                    **snippet,
                    "content": snippet["content"]
                    .replace("\x00", "")
                    .encode("ascii", "ignore")
                    .decode(),
                },
            },
        }
    return metadata


@bugsnag.aws_lambda_handler
def process(event: Event, _context: Any) -> dict[str, list[dict[str, str]]]:
    batch_item_failures = []
    client = boto3.client("rds")
    password_token = client.generate_db_auth_token(
        FI_AWS_AURORA_ENDPOINT,
        5432,
        "integrates_lambda",
        "us-east-1",
    )

    with psycopg.connect(
        autocommit=True,
        dbname="integrates",
        host=FI_AWS_AURORA_ENDPOINT,
        password=password_token,
        user="integrates_lambda",
    ) as connection:
        with connection.cursor() as cursor:
            for record in event["Records"]:
                try:
                    audit_event = json.loads(record["body"])
                    mechanism = audit_event["mechanism"]
                    sender_id = record["attributes"]["SenderId"].split(":")[1]
                    author = (
                        sender_id if mechanism == "MIGRATION" else audit_event["author"]
                    )

                    metadata = _sanitize_metadata(audit_event["metadata"])
                    cursor.execute(
                        """
                        INSERT INTO integrates.audit (
                            deduplication_id,
                            event_action,
                            event_author_ip,
                            event_author_role,
                            event_author_user_agent,
                            event_author,
                            event_date,
                            event_mechanism,
                            event_metadata,
                            event_object,
                            event_object_id
                        )
                        VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)
                        ON CONFLICT (deduplication_id) DO NOTHING
                        """,
                        (
                            record["attributes"]["MessageDeduplicationId"],
                            audit_event["action"],
                            audit_event["author_ip"],
                            audit_event["author_role"],
                            audit_event["author_user_agent"],
                            author,
                            audit_event["date"],
                            mechanism,
                            Jsonb(metadata),
                            audit_event["object"],
                            audit_event["object_id"],
                        ),
                    )
                except Exception as exc:
                    batch_item_failures.append({"itemIdentifier": record["messageId"]})
                    bugsnag.notify(exc)

    return {"batchItemFailures": batch_item_failures}
