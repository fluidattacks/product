resource "aws_s3_bucket" "front" {
  bucket = "integrates.front.fluidattacks.com"

  tags = {
    "Name"              = "integrates.front.fluidattacks.com"
    "fluidattacks:line" = "cost"
    "fluidattacks:comp" = "integrates"
    "Access"            = "private"
    "NOFLUID"           = "f335.f325_due_to_generation_significant_volume_data_public_bucket"
  }
}

resource "aws_s3_bucket_ownership_controls" "front" {
  bucket = aws_s3_bucket.front.id

  rule {
    object_ownership = "BucketOwnerPreferred"
  }
}

resource "aws_s3_bucket_public_access_block" "front" {
  bucket = aws_s3_bucket.front.id

  block_public_acls       = false
  block_public_policy     = false
  ignore_public_acls      = false
  restrict_public_buckets = false
}

resource "aws_s3_bucket_acl" "front" {
  bucket = aws_s3_bucket.front.id

  acl = "public-read"

  depends_on = [
    aws_s3_bucket_ownership_controls.front,
    aws_s3_bucket_public_access_block.front,
  ]
}

data "aws_iam_policy_document" "front" {
  statement {
    sid    = "CloudFlare"
    effect = "Allow"

    principals {
      type        = "AWS"
      identifiers = ["*"]
    }
    actions = [
      "s3:GetObject",
    ]
    resources = [
      "${aws_s3_bucket.front.arn}/*",
    ]
    condition {
      test     = "IpAddress"
      variable = "aws:SourceIp"
      values   = data.cloudflare_ip_ranges.cloudflare.cidr_blocks
    }
  }
}

resource "aws_s3_bucket_policy" "front" {
  bucket = aws_s3_bucket.front.id
  policy = data.aws_iam_policy_document.front.json

  depends_on = [aws_s3_bucket_acl.front]
}

resource "aws_s3_bucket_server_side_encryption_configuration" "front" {
  bucket = aws_s3_bucket.front.id

  rule {
    apply_server_side_encryption_by_default {
      sse_algorithm = "AES256"
    }
  }
}
resource "aws_s3_bucket_logging" "front" {
  bucket = aws_s3_bucket.front.id

  target_bucket = "common.logging"
  target_prefix = "log/${aws_s3_bucket.front.id}"
}

resource "aws_s3_bucket_cors_configuration" "front" {
  bucket = aws_s3_bucket.front.id

  cors_rule {
    allowed_headers = ["*"]
    allowed_methods = ["GET", "HEAD"]
    allowed_origins = [
      "https://app.fluidattacks.com",
      "https://design.fluidattacks.com",
      "https://*.app.fluidattacks.com",
    ]
    expose_headers  = ["GET", "HEAD"]
    max_age_seconds = 3000
  }
}

resource "cloudflare_page_rule" "front" {
  zone_id  = data.cloudflare_zones.fluidattacks_com.zones[0].id
  target   = "integrates.front.${data.cloudflare_zones.fluidattacks_com.zones[0].name}/*"
  status   = "active"
  priority = 1

  actions {
    cache_level            = "bypass"
    browser_cache_ttl      = 1800
    explicit_cache_control = "off"
  }
}

resource "cloudflare_record" "front" {
  zone_id = data.cloudflare_zones.fluidattacks_com.zones[0].id
  name    = "integrates.front.${data.cloudflare_zones.fluidattacks_com.zones[0].name}"
  type    = "CNAME"
  value   = aws_s3_bucket.front.bucket_domain_name
  proxied = true
  ttl     = 1
}
