/// <reference types="vite/client" />
import path from "path";
import { fileURLToPath } from "node:url"
import { glob } from "glob"

import react from "@vitejs/plugin-react-swc";
import { visualizer } from "rollup-plugin-visualizer";
import dts from "vite-plugin-dts";
import { libInjectCss } from 'vite-plugin-lib-inject-css'
import { defineConfig } from "vite";

// https://vitejs.dev/config/
export default defineConfig({
  build: {
    emptyOutDir: true,
    lib: {
      entry: path.resolve(__dirname, "src/index.ts"),
      name: "@fluidattacks/design",
      formats: ["es", "cjs"],
    },
    minify: "esbuild",
    rollupOptions: {
      input: Object.fromEntries(
        glob.sync("src/**/index.{ts,tsx}").map((file) => {
          const entryName = path.relative(
            "src",
            file.slice(0, file.length - path.extname(file).length)
          )
          const entryUrl = fileURLToPath(new URL(file, import.meta.url))
          return [entryName, entryUrl]
        })
      ),
      external: [
        "react",
        "react-dom",
        "react/jsx-runtime",
        "react-router-dom",
      ],
      output: {
        entryFileNames: "[name].js",
        assetFileNames: "assets/[name][extname]",
        manualChunks: (id): string | undefined => {
          if (id.includes("node_modules")) {
            return "vendor";
          }
          if (id.includes("src/components/")) {
            return "components";
          }

          return undefined;
        },
        globals: {
          react: "React",
          "react-dom": "ReactDOM",
          "react/jsx-runtime": "ReactJSXRuntime",
          "react-router-dom": "ReactRouterDOM",
        },
      },
    },
  },
  plugins: [
    dts(),
    libInjectCss(),
    react(),
    visualizer({
      filename: "stats.html",
      open: true,
    }),
  ],
  resolve: {
    alias: {
      components: path.join(__dirname, "src", "components"),
      hooks: path.join(__dirname, "src", "hooks"),
      styles: path.join(__dirname, "src", "styles"),
      utils: path.join(__dirname, "src", "utils"),
    },
  },
  optimizeDeps: {
    include: ["react", "react-dom", "react-router-dom"],
  },
});
