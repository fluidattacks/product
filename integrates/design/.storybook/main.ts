import type { StorybookConfig } from "@storybook/react-vite";
import { mergeConfig } from "vite";
import remarkGfm from "remark-gfm"

const config: StorybookConfig = {
  stories: [
    "./**/*.mdx",
    "../src/**/*.mdx",
    "../src/**/*.stories.@(ts|tsx)",
  ],
  addons: [
    "@storybook/addon-controls",
    "@storybook/addon-a11y",
    "@storybook/addon-actions",
    "@storybook/addon-links",
    "@storybook/addon-viewport",
    "@storybook/addon-interactions",
    {
      name: '@storybook/addon-docs',
      options: {
        mdxPluginOptions: {
          mdxCompileOptions: {
            remarkPlugins: [remarkGfm],
          },
        },
      },
    },
  ],
  features: {
    backgroundsStoryGlobals: true,
    viewportStoryGlobals: false,
  },
  framework: {
    name: "@storybook/react-vite",
    options: {},
  },
  typescript: {
    reactDocgen: "react-docgen-typescript",
  },
  viteFinal: async (config) => {
    return mergeConfig(config, {
      build: {
        minify: false,
        sourcemap: false,
      },
      resolve: {
        alias: {
          components: "/src/components",
        },
      },
    });
  },
};

export default config;
