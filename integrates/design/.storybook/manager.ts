import { addons } from "@storybook/manager-api";
import { create } from "@storybook/theming";

addons.setConfig({
  panelPosition: "right",
  theme: create({
    base: "light",
    brandTitle: "Fluid Attacks UI",
    brandImage:
      "https://res.cloudinary.com/fluid-attacks/image/upload/v1728418266/airs/logo/logo_full.png",
    brandTarget: "_self",

    appBg: "#f9fafb",
    appContentBg: "#ffffff",
    appBorderColor: "#eaecf0",
    appBorderRadius: 0,

    colorPrimary: "#ffffff",
    colorSecondary: "#ac0a17",

    textInverseColor: "#ffffff",
    textMutedColor: "#101828",

    buttonBg: "#ffffff",

    barBg: "#ffffff",
    barTextColor: "#1d2939",
    barSelectedColor: "#bf0b1a",
  }),
});
