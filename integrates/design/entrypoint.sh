# shellcheck shell=bash

function _run {
  local port="${1:-6006}"

  npm run storybook -- -p "${port}"
}

function _lint {
  npx tsc -p tsconfig.json --noEmit
  npx eslint . --fix
  npx stylelint . --fix
}

function _test {
  npm run test-storybook -- --url https://design.fluidattacks.com --failOnConsole --no-cache
}

function deploy {
  local aws_role="prod_common"
  local src="integrates/design"
  local bucket_name="design.fluidattacks.com"

  export DESIGN_ENVIRONMENT="production"
  export DESIGN_DEPLOYMENT_DATE
  export CI_COMMIT_SHA
  export CI_COMMIT_SHORT_SHA
  export NODE_OPTIONS="--max-old-space-size=4096"

  __argDesignBuild__/bin/integrates-design-build
  aws_login "${aws_role}" "3600"
  if test -z "${CI_COMMIT_SHA-}"; then
    CI_COMMIT_SHA="$(get_commit_from_rev . HEAD)"
  fi
  if test -z "${CI_COMMIT_SHORT_SHA-}"; then
    CI_COMMIT_SHORT_SHA="${CI_COMMIT_SHA:0:8}"
  fi

  DESIGN_DEPLOYMENT_DATE="$(date -u '+%FT%H:%M:%SZ')"
  echo __argDesignBuild__
  aws_s3_sync "${src}/out" "s3://${bucket_name}" --delete
}

function main {
  local dir="integrates/design"

  if [[ ${1} == "deploy" ]]; then
    deploy
    return 0
  fi

  pushd "${dir}" || error "${dir} directory not found"
  rm -rf node_modules
  npm clean-install

  case "${1}" in
    run) _run "${@:2}" ;;
    lint) _lint "${@:2}" ;;
    test) _test "${@:2}" ;;
    *) error "Must provide either 'run', 'lint', or 'test', as the first argument" ;;
  esac
}

main "${@}"
