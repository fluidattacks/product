{ inputs, makeScript, outputs, ... }: {
  jobs."/integrates/design/build" = makeScript {
    entrypoint = ./builder.sh;
    name = "integrates-design-build";
    searchPaths = {
      bin = [ inputs.nixpkgs.bash inputs.nixpkgs.nodejs_20 ]
        ++ inputs.nixpkgs.lib.optionals inputs.nixpkgs.stdenv.isDarwin [
          (inputs.makeImpureCmd {
            cmd = "open";
            path = "/usr/bin/open";
          })
        ];
    };
  };
}
