# shellcheck shell=bash

function main {
  export DESIGN_BUILD_DATE

  : \
    && DESIGN_BUILD_DATE="$(date -u '+%FT%H:%M:%SZ')" \
    && pushd integrates/design \
    && rm -rf out \
    && rm -rf node_modules \
    && npm clean-install --ignore-scripts=false \
    && npm run build-storybook -- -o ./out \
    && popd \
    || return 1
}

main "${@}"
