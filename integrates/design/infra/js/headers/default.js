let securityHeaders = {
	"Content-Security-Policy":
		"script-src " +
		"'self' " +
		"'unsafe-inline' " +
		"fluidattacks.matomo.cloud " +
		"*.cloudflareinsights.com " +
		"*.cookiebot.com " +
		"*.google-analytics.com " +
		"*.googletagmanager.com " +
		"*.hotjar.com " +
		"https://fluidattacks.com/ " +
		"https://*.fluidattacks.com/ " +
 		"https://kit.fontawesome.com/ " +
		"https://kit-uploads.fontawesome.com/ " +
		"https://ka-p.fontawesome.com/ " +
		"frame-ancestors " +
		"'self'; " +
		"object-src " +
		"'none'; " +
		"upgrade-insecure-requests;" +
		"worker-src " +
		"'self' " +
		"blob:; " +
		"connect-src " +
		"https://fluidattacks.com/ " +
		"https://*.fluidattacks.com/ " +
		"https://kit.fontawesome.com/ " +
		"https://kit-uploads.fontawesome.com/ " +
		"https://ka-p.fontawesome.com/ " +
		"'self';",
	"Strict-Transport-Security": "max-age=31536000",
	"X-Xss-Protection": "0",
	"X-Content-Type-Options": "nosniff",
	"X-Permitted-Cross-Domain-Policies": "none",
	"Referrer-Policy": "strict-origin-when-cross-origin",
	"Permissions-Policy":
		"geolocation=(self), " +
		"midi=(self), " +
		"push=(self), " +
		"sync-xhr=(self), " +
		"microphone=(self), " +
		"camera=(self), " +
		"magnetometer=(self), " +
		"gyroscope=(self), " +
		"speaker=(self), " +
		"vibrate=(self), " +
		"fullscreen=(self), " +
		"payment=(self)",
};

let sanitizeHeaders = {};

let removeHeaders = ["Public-Key-Pins"];

const homeURL = "https://design.fluidattacks.com/";

addEventListener("fetch", event => {
	event.respondWith(addHeaders(event.request));
});

async function addHeaders(req) {
	const response = await fetch(req);
	const newHdrs = new Headers(response.headers);

	if (newHdrs.has("Content-Type") && !newHdrs.get("Content-Type").includes("text/html")) {
		return new Response(response.body, {
			status: response.status,
			statusText: response.statusText,
			headers: newHdrs,
		});
	}

	Object.keys(securityHeaders).forEach(name => {
		newHdrs.set(name, securityHeaders[name]);
	});

	Object.keys(sanitizeHeaders).forEach(name => {
		newHdrs.set(name, sanitizeHeaders[name]);
	});

	removeHeaders.forEach(function (name) {
		newHdrs.delete(name);
	});

	return new Response(response.body, {
		status: response.status,
		statusText: response.statusText,
		headers: newHdrs,
	});
}
