import { isObject } from "lodash";

export const openUrl = (url: string, openANewTab = true): void => {
  // https://owasp.org/www-community/attacks/Reverse_Tabnabbing
  const newTab: Window | null = window.open(
    url,
    openANewTab ? undefined : "_self",
    "noopener,noreferrer,",
  );
  if (isObject(newTab)) {
    // It is necessary to assign null to opener
    // eslint-disable-next-line functional/immutable-data
    newTab.opener = null;
  }
};
