import type { CloudinaryImage } from "@cloudinary/url-gen";
import { Cloudinary } from "@cloudinary/url-gen";
import { useMemo } from "react";

/**
 * Cloudinary image hook props.
 * @interface IUseCloudinaryImageProps
 * @property {string} publicId The publicId location in cloudinary.
 * @property {string} [format] The image format desired (e.g., png, jpg).
 */
interface IUseCloudinaryImageProps {
  readonly publicId: string;
  readonly format?: string;
}

export const useCloudinaryImage = ({
  publicId,
  format = "webp",
}: IUseCloudinaryImageProps): CloudinaryImage => {
  const cloudinaryInstance = useMemo((): Cloudinary => {
    return new Cloudinary({
      cloud: {
        cloudName: "fluid-attacks",
      },
    });
  }, []);

  const image = useMemo((): CloudinaryImage => {
    return cloudinaryInstance.image(publicId).format(format);
  }, [cloudinaryInstance, publicId, format]);

  return image;
};
