import { useState } from "react";

import { useDebouncedCallback } from "hooks";

interface IUseSearchParams {
  readonly debounceWait: number;
}

const useSearch = (
  params?: IUseSearchParams,
): [string | undefined, (value: string) => void] => {
  const [search, setSearch] = useState<string>();
  const debouncedSetSearch = useDebouncedCallback((value: string): void => {
    setSearch(value);
  }, params?.debounceWait ?? 300);

  return [search, debouncedSetSearch];
};

export { useSearch };
