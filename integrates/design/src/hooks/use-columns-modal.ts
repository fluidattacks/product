import some from "lodash/some";
import { useCallback, useMemo, useState } from "react";

import { type IUseModal, useModal } from "hooks/use-modal";

interface IUseColumnsModal extends IUseModal {
  columns: IColumnModalProps[];
  setColumns: (newOrder: readonly IColumnModalProps[]) => void;
  resetToDefault: VoidFunction;
  hasChanged: boolean;
}

interface IColumnModalProps {
  id: string;
  locked: boolean;
  name: string;
  visible: boolean;
  group?: string;
  toggleVisibility?: (value?: boolean) => void;
}

const useColumnsModal = ({
  name,
  items,
  columnOrder,
  setColumnOrder,
}: Readonly<{
  name: string;
  items: IColumnModalProps[];
  columnOrder?: string[];
  // Custom type coming from tanstack table
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  setColumnOrder?: (updater: any) => void;
}>): IUseColumnsModal => {
  const initialValues = useMemo(
    (): IColumnModalProps[] => [
      // locked -> visible -> hidden
      ...items
        .filter(({ locked }) => locked)
        .map(({ locked, ...props }) => ({
          ...props,
          visible: true,
          locked,
        })),
      ...items.filter(({ locked }) => !locked),
    ],
    [items],
  );

  const [columns, setColumns] = useState<IColumnModalProps[]>(initialValues);

  const onChangeHandler = useCallback(
    (newOrder: readonly IColumnModalProps[]): void => {
      const newOrderFiltered = newOrder.filter(({ locked }) => !locked);
      const nonSortable = newOrder.filter(({ locked }) => locked);
      const visibleColumns = [...nonSortable, ...newOrderFiltered];
      const visibleColumnsNames = visibleColumns.map(({ name }) => name);

      setColumns((prev) => {
        return [
          ...visibleColumns,
          ...prev.filter(({ name }) => !visibleColumnsNames.includes(name)),
        ];
      });
      if (setColumnOrder)
        setColumnOrder(visibleColumns.map(({ id }): string => id));
    },
    [setColumnOrder],
  );

  const resetToDefault = useCallback((): void => {
    const defaultValues = columns
      .map((column): IColumnModalProps => {
        const visibility = columnOrder?.includes(column.id) ?? false;
        column.toggleVisibility?.(visibility);

        return {
          ...column,
          visible: visibility,
        };
      })
      .toSorted((first, last) => {
        const firstIdx = columnOrder?.indexOf(first.id) ?? 0;
        const lastIdx = columnOrder?.indexOf(last.id) ?? 0;

        if (Number(last.visible) !== Number(first.visible))
          return Number(last.visible) - Number(first.visible);
        return firstIdx - lastIdx;
      });

    setColumns(defaultValues);
    setColumnOrder?.(columnOrder);
  }, [columns, columnOrder, setColumnOrder]);

  const hasChanged = useMemo((): boolean => {
    return some(columns, (column): boolean => {
      const visibility = columnOrder?.includes(column.id) ?? false;
      return visibility !== column.visible;
    });
  }, [columns, columnOrder]);

  return {
    setColumns: onChangeHandler,
    resetToDefault,
    columns,
    ...useModal(name),
    hasChanged,
  };
};

export type { IUseColumnsModal, IColumnModalProps };
export { useColumnsModal };
