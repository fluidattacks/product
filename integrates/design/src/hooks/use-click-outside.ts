/* eslint-disable functional/functional-parameters */
import { useEffect } from "react";

const useClickOutside = (
  element: Readonly<HTMLElement | null>,
  onClickOutside: VoidFunction,
  addEventListenerToFullDocument = false,
): void => {
  useEffect((): VoidFunction => {
    const handleClick = (event: Readonly<MouseEvent>): void => {
      if (element && !element.contains(event.target as Node)) {
        onClickOutside();
      }
    };

    if (addEventListenerToFullDocument) {
      document.addEventListener("mousedown", handleClick, true);

      return (): void => {
        document.removeEventListener("mousedown", handleClick, true);
      };
    }
    document
      .getElementById("root")
      ?.addEventListener("mousedown", handleClick, true);

    return (): void => {
      document
        .getElementById("root")
        ?.removeEventListener("mousedown", handleClick, true);
    };
  }, [addEventListenerToFullDocument, element, onClickOutside]);
};

export { useClickOutside };
