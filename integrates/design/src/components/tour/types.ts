import type { Step } from "react-joyride";

/**
 * Tour component props.
 * @interface ITourProps
 * @property { Function } [onFinish] - The callback function to be executed when the tour finishes.
 * @property { boolean } run - A flag indicating whether the tour should be run.
 * @property { Step[] } steps - The steps to be included in the tour.
 */
interface ITourProps {
  onFinish?: () => void;
  run: boolean;
  steps: Step[];
}

export type { ITourProps };
