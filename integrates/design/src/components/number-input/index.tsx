import { isEmpty, toNumber } from "lodash";
import React, { Ref, forwardRef, useCallback, useState } from "react";

import { StyledInput, StyledInputContainer } from "./styles";
import type { INumberInputProps } from "./types";

import { Tooltip } from "components/tooltip";

const NumberInput = forwardRef(function NumberInput(
  {
    autoUpdate = false,
    decimalPlaces = 0,
    defaultValue = 0,
    max,
    min,
    name,
    onEnter,
    tooltipMessage,
    ...props
  }: Readonly<INumberInputProps>,
  ref: Ref<HTMLInputElement>,
): JSX.Element {
  const decPlaces = decimalPlaces < 0 ? 0 : decimalPlaces;
  const [value, setValue] = useState(Number(defaultValue).toFixed(decPlaces));

  const handleOnInputChange = useCallback(
    (event: Readonly<React.ChangeEvent<HTMLInputElement>>): void => {
      const newValue = toNumber(event.target.value);
      if (event.target.value.endsWith(".")) {
        setValue(event.target.value);
      } else if (newValue >= Number(min) && newValue <= Number(max)) {
        setValue(String(newValue));
        if (autoUpdate) {
          onEnter(toNumber(newValue.toFixed(decPlaces)));
        }
      }
      event.stopPropagation();
    },
    [autoUpdate, decPlaces, max, min, onEnter],
  );

  const handleOnInputContainerBlur = useCallback(
    (event: Readonly<React.FocusEvent<HTMLInputElement>>): void => {
      if (!event.currentTarget.contains(event.relatedTarget)) {
        setValue(Number(defaultValue).toFixed(decPlaces));
      }
      event.stopPropagation();
    },
    [decPlaces, defaultValue],
  );

  const handleOnInputKeyUp = useCallback(
    (event: Readonly<React.KeyboardEvent<HTMLInputElement>>): void => {
      event.stopPropagation();
      if (event.key === "Enter" && !isEmpty(event.currentTarget.value)) {
        onEnter(toNumber(event.currentTarget.value));
      }
    },
    [onEnter],
  );

  return (
    <StyledInputContainer onBlur={handleOnInputContainerBlur} tabIndex={-1}>
      <Tooltip id={"numberInputTooltip"} tip={tooltipMessage}>
        <StyledInput
          {...props}
          aria-label={name}
          aria-valuemax={Number(max)}
          aria-valuemin={Number(min)}
          aria-valuenow={Number(value)}
          max={max}
          min={min}
          name={name}
          onChange={handleOnInputChange}
          onKeyUp={handleOnInputKeyUp}
          ref={ref}
          step={10 ** -decPlaces}
          type={"number"}
          value={value}
        />
      </Tooltip>
    </StyledInputContainer>
  );
});

export type { INumberInputProps };
export { NumberInput };
