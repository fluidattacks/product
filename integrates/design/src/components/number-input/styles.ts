import { styled } from "styled-components";

const StyledInputContainer = styled.div`
  width: fit-content;
  border: ${({ theme }): string => `1px solid ${theme.palette.gray[300]}`};
  border-radius: 4px;
  background: none;
  color: ${({ theme }): string => theme.palette.black};
  font-family: ${({ theme }): string => theme.typography.type.primary};
  font-size: ${({ theme }): string => theme.typography.text.md};
  height: 40px;
  display: flex;
  align-items: center;
  justify-content: center;
  padding: 0.5em;
`;

const StyledInput = styled.input`
  width: 4em;
  appearance: textfield;
  border-style: none;
  padding: 0.5rem;
  padding-top: 0.5rem;
  padding-bottom: 0.5rem;
  padding-left: 0.5rem;
  background: none;

  &:focus {
    border-color: unset;
    box-shadow: none;
    outline: none;
  }

  &::-webkit-outer-spin-button,
  &::-webkit-inner-spin-button {
    opacity: 1;
  }
`;

export { StyledInputContainer, StyledInput };
