import type { Meta, StoryObj } from "@storybook/react";
import { useCallback, useState } from "react";

import type { IToggleProps } from "./types";

import { Toggle } from ".";

const config: Meta = {
  component: Toggle,
  parameters: {
    controls: {
      exclude: [
        "onClick",
        "OnBlur",
        "onChange",
        "onFocus",
        "onKeyDown",
        "itemsProps",
      ],
    },
  },
  tags: ["autodocs"],
  title: "Components/Toggle",
};

const CardImgTemplate: StoryObj<{
  itemsProps: IToggleProps[];
}> = {
  render: ({ itemsProps }): JSX.Element => (
    <form style={{ display: "flex", flexDirection: "column", gap: "20px" }}>
      {itemsProps.map((props, index): JSX.Element => {
        const [isChecked, setIsChecked] = useState(props.defaultChecked);
        const key = `$"toggle"-${index}`;

        const handleChange = useCallback((): void => {
          if (isChecked !== undefined) {
            setIsChecked(!isChecked);
          }
        }, [isChecked]);

        return (
          <Toggle
            defaultChecked={isChecked}
            disabled={props.disabled}
            key={key}
            leftDescription={props.leftDescription}
            name={props.name}
            onChange={handleChange}
            rightDescription={props.rightDescription}
            value={props.value}
          />
        );
      })}
    </form>
  ),
};

const Default = {
  ...CardImgTemplate,
  args: {
    itemsProps: [
      {
        defaultChecked: false,
        disabled: false,
        name: "toggleOff",
        value: "toggle1",
      },
      {
        defaultChecked: true,
        disabled: false,
        name: "toggleOn",
        value: "toggle2",
      },
      {
        defaultChecked: false,
        disabled: true,
        name: "toggleDisabled",
        value: "toggle3",
      },
    ],
  },
};

const TogglesWithText = {
  ...CardImgTemplate,
  args: {
    itemsProps: [
      {
        defaultChecked: false,
        disabled: false,
        leftDescription: "Text here",
        name: "toggleOff",
        value: "toggle1",
      },
      {
        defaultChecked: true,
        disabled: false,
        leftDescription: "Text here",
        name: "toggleOn",
        value: "toggle2",
      },
      {
        defaultChecked: false,
        disabled: true,
        leftDescription: "Text here",
        name: "toggleDisabled",
        value: "toggle3",
      },
      {
        defaultChecked: false,
        disabled: false,
        name: "toggleOff",
        rightDescription: "Text here",
        value: "toggle1",
      },
      {
        defaultChecked: true,
        disabled: false,
        name: "toggleOn",
        rightDescription: "Text here",
        value: "toggle2",
      },
      {
        defaultChecked: false,
        disabled: true,
        name: "toggleDisabled",
        rightDescription: "Text here",
        value: "toggle3",
      },
      {
        defaultChecked: false,
        disabled: false,
        leftDescription: "Text here",
        name: "toggleOff",
        rightDescription: "Text here",
        value: "toggle1",
      },
      {
        defaultChecked: true,
        disabled: false,
        leftDescription: "Text here",
        name: "toggleOn",
        rightDescription: "Text here",
        value: "toggle2",
      },
      {
        defaultChecked: false,
        disabled: true,
        leftDescription: "Text here",
        name: "toggleDisabled",
        rightDescription: "Text here",
        value: "toggle3",
      },
    ],
  },
};

export { Default, TogglesWithText };
export default config;
