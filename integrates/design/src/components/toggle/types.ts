import type { Property } from "csstype";
import type { InputHTMLAttributes } from "react";

/**
 * Toggle component props.
 * @interface ILabel
 * @property { string } on - The label for the on state.
 * @property { string } off - The label for the off state.
 */
interface ILabel {
  on: string;
  off: string;
}

/**
 * Toggle component props.
 * @interface IToggleProps
 * @extends InputHTMLAttributes<HTMLInputElement>
 * @property { Property.JustifyContent } [justify] - The justification of the toggle and its labels.
 * @property { ILabel | string } [leftDescription] - The label or description for the left side of the toggle.
 * @property { string } name - The name of the input field.
 * @property { string } [rightDescription] - The label or description for the right side of the toggle.
 */
interface IToggleProps extends InputHTMLAttributes<HTMLInputElement> {
  justify?: Property.JustifyContent;
  leftDescription?: ILabel | string;
  leftDescriptionMinWidth?: string;
  name: string;
  rightDescription?: string;
}

export type { IToggleProps };
