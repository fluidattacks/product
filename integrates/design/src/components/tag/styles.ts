import { DefaultTheme, styled } from "styled-components";

import type { TTagVariant } from "./types";

import { variantBuilder } from "components/@core";

interface IStyledTag {
  $fontSize?: string;
  $variant: TTagVariant;
}

const { getVariant } = variantBuilder(
  (theme: DefaultTheme): Record<TTagVariant, string> => ({
    default: `
      background-color: ${theme.palette.gray[200]};
      color: ${theme.palette.gray[800]};
    `,
    error: `
      &.high {
        background-color: ${theme.palette.error[700]};
        color: ${theme.palette.white};
      }
      &.low {
        background-color: ${theme.palette.error[50]};
        color: ${theme.palette.error[700]};
      }
      &.medium {
        background-color: ${theme.palette.error[500]};
        color: ${theme.palette.error[50]};
      }
    `,
    inactive: `
      background-color: ${theme.palette.gray[200]};
      color: ${theme.palette.gray[600]};
    `,
    info: `
      &.high {
        background-color: ${theme.palette.info[700]};
        color: ${theme.palette.white};
      }
      &.low {
        background-color: ${theme.palette.info[50]};
        color: ${theme.palette.info[700]};
      }
      &.medium {
        background-color: ${theme.palette.info[500]};
        color: ${theme.palette.info[50]};
      }
    `,
    reachable: `
      background-color: ${theme.palette.gray[50]};
      color: ${theme.palette.gray[800]};
    `,
    remediation: `
      background-color: inherit;
      border: 1px solid ${theme.palette.gray[300]};
      color: ${theme.palette.gray[400]};
    `,
    role: `
      background-color: ${theme.palette.gray[800]};
      color: ${theme.palette.white};
    `,
    success: `
      &.high {
        background-color: ${theme.palette.success[700]};
        color: ${theme.palette.white};
      }
      &.low {
        background-color: ${theme.palette.success[50]};
        color: ${theme.palette.success[700]};
      }
      &.medium {
        background-color: ${theme.palette.success[500]};
        color: ${theme.palette.success[50]};
      }
    `,
    technique: `
      --tag-padding-y: ${theme.spacing[0.25]};
      --tag-padding-x: ${theme.spacing[0.75]};

      background-color: inherit;
      border: 1px solid ${theme.palette.success[800]};
      color: ${theme.palette.success[800]};
    `,
    warning: `
      &.high {
        background-color: ${theme.palette.warning[600]};
        color: ${theme.palette.white};
      }
      &.low {
        background-color: ${theme.palette.warning[50]};
        color: ${theme.palette.warning[700]};
      }
      &.medium {
        background-color: ${theme.palette.warning[400]};
        color: ${theme.palette.warning[700]};
      }
    `,
  }),
);

const StyledTag = styled.span<IStyledTag>`
  ${({ theme, $fontSize = theme.typography.text.xs, $variant }): string => `
    align-items: center;
    border-radius: ${theme.spacing[$variant === "technique" ? 1 : 0.25]};
    display: inline-flex;
    gap: ${theme.spacing[0.25]};
    font-family: ${theme.typography.type.primary};
    font-size: ${$fontSize};
    font-style: normal;
    font-weight: ${theme.typography.weight.regular};
    justify-content: center;
    line-height: 18px;
    padding: var(--tag-padding-y) var(--tag-padding-x);
    white-space: nowrap;
    width: max-content;

    ~ a {
      font-size: ${theme.typography.text.xs};
      display: flex;
    }

    ${getVariant(theme, $variant)}
  `}
`;

export { StyledTag };
