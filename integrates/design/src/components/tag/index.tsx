import { isEmpty } from "lodash";

import { StyledTag } from "./styles";
import type { ITagProps, TTagVariant } from "./types";

import { Icon } from "components/icon";
import { IconButton } from "components/icon-button";
import { Link } from "components/link";
import { Span } from "components/typography";

const Tag = ({
  disabled,
  icon,
  iconColor = "inherit",
  iconType = "fa-light",
  id = "close-tag",
  filterValues = "",
  fontSize,
  href,
  linkLabel,
  onClose,
  priority = "default",
  tagTitle = "",
  tagLabel,
  variant = "default",
}: Readonly<ITagProps>): JSX.Element => {
  const shouldSplit = tagLabel.length > 25 && isEmpty(tagTitle + filterValues);
  const splittedText = `${tagLabel.slice(0, 25)}...`;

  return (
    <Span
      className={"gap-0.5"}
      display={href === undefined ? "initial" : "flex"}
      size={"sm"}
    >
      <StyledTag $fontSize={fontSize} $variant={variant} className={priority}>
        {icon && (
          <Icon
            icon={icon}
            iconColor={iconColor}
            iconSize={"xxs"}
            iconType={iconType}
          />
        )}
        <span className={"w-full"}>
          <strong>{tagTitle}</strong>
          <span>{shouldSplit ? splittedText : tagLabel}</span>
          <strong className={"w-full"}>{filterValues}</strong>
        </span>
        {onClose ? (
          <IconButton
            disabled={disabled}
            icon={"xmark"}
            iconSize={"xxs"}
            iconType={"fa-light"}
            id={id}
            onClick={onClose}
            px={0.125}
            py={0.125}
            variant={"ghost"}
          />
        ) : undefined}
      </StyledTag>
      {href === undefined ? undefined : <Link href={href}>{linkLabel}</Link>}
    </Span>
  );
};

export type { ITagProps, TTagVariant };
export { Tag };
