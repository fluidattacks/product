import type { Meta, StoryFn } from "@storybook/react";
import React from "react";

import type { ITagProps } from ".";
import { Tag } from ".";
import { Container } from "../container";

const config: Meta = {
  component: Tag,
  parameters: {
    controls: {
      exclude: ["ref", "theme", "as", "forwardedAs"],
    },
  },
  tags: ["autodocs"],
  title: "Components/Tag",
};

const mockClose = (): void => {
  "testing";
};

const Template: StoryFn<React.PropsWithChildren<ITagProps>> = (
  props,
): JSX.Element => <Tag {...props} />;

const Default: React.FC = (): JSX.Element => (
  <Container display={"flex"} gap={1}>
    <Template onClose={mockClose} tagLabel={"Airs"} />
    <Template tagLabel={"Doc"} />
    <Template tagLabel={"Fluid Attack's"} />
    <Template
      filterValues={"Vulnerable"}
      icon={"circle-info"}
      onClose={mockClose}
      tagLabel={"Status "}
    />
    <Template
      onClose={mockClose}
      tagLabel={
        "This is an example of a text that exceeds the 25-character limit."
      }
    />
  </Container>
);

const Status: React.FC = (): JSX.Element => (
  <Container>
    <Container display={"flex"} gap={1} mb={1}>
      <Template
        href={"https://test.com"}
        icon={"circle-exclamation"}
        priority={"low"}
        tagLabel={"Error"}
        variant={"error"}
      />
      <Template
        href={"https://test.com"}
        icon={"circle-exclamation"}
        priority={"medium"}
        tagLabel={"Error"}
        variant={"error"}
      />
      <Template
        href={"https://test.com"}
        icon={"circle-exclamation"}
        priority={"high"}
        tagLabel={"Error"}
        variant={"error"}
      />
    </Container>
    <Container display={"flex"} gap={1} mb={1}>
      <Template
        href={"https://test.com"}
        icon={"circle-exclamation"}
        priority={"low"}
        tagLabel={"Success"}
        variant={"success"}
      />
      <Template
        href={"https://test.com"}
        icon={"circle-exclamation"}
        priority={"medium"}
        tagLabel={"Success"}
        variant={"success"}
      />
      <Template
        href={"https://test.com"}
        icon={"circle-exclamation"}
        priority={"high"}
        tagLabel={"Success"}
        variant={"success"}
      />
    </Container>
    <Container display={"flex"} gap={1} mb={1}>
      <Template
        href={"https://test.com"}
        icon={"circle-exclamation"}
        priority={"low"}
        tagLabel={"Warning"}
        variant={"warning"}
      />
      <Template
        href={"https://test.com"}
        icon={"circle-exclamation"}
        priority={"medium"}
        tagLabel={"Warning"}
        variant={"warning"}
      />
      <Template
        href={"https://test.com"}
        icon={"circle-exclamation"}
        priority={"high"}
        tagLabel={"Warning"}
        variant={"warning"}
      />
    </Container>
    <Container display={"flex"} gap={1} mb={1}>
      <Template
        href={"https://test.com"}
        icon={"circle-exclamation"}
        priority={"low"}
        tagLabel={"Info"}
        variant={"info"}
      />
      <Template
        href={"https://test.com"}
        icon={"circle-exclamation"}
        priority={"medium"}
        tagLabel={"Info"}
        variant={"info"}
      />
      <Template
        href={"https://test.com"}
        icon={"circle-exclamation"}
        priority={"high"}
        tagLabel={"Info"}
        variant={"info"}
      />
    </Container>
    <Container display={"flex"} gap={1}>
      <Template
        href={"https://test.com"}
        icon={"circle-exclamation"}
        tagLabel={"Inactive"}
        variant={"inactive"}
      />
    </Container>
  </Container>
);

const Role = Template.bind({});
Role.args = {
  tagLabel: "User manager",
  variant: "role",
};

export { Default, Status, Role };
export default config;
