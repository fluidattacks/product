import type { Property } from "csstype";

import {
  IMarginModifiable,
  IPaddingModifiable,
  ISizeModifiable,
  ITextModifiable,
} from "components/@core";

/**
 * Typography component props.
 * @interface ITypographyProps
 * @extends IMarginModifiable
 * @extends IPaddingModifiable
 * @extends ISizeModifiable
 * @extends ITextModifiable
 * @property {Property.Background} [bgGradient] - Background gradient.
 * @property {string} [className] - Additional class names.
 * @property {string} [content] - Text content.
 * @property {Display} [display] - Text display layout.
 * @property {LineHeight<number | string>} - Line height in mobile resolution.
 * @property {Property.WebkitTextFillColor} [textFill] - Text fill color.
 */
interface ITypographyProps
  extends IMarginModifiable,
    ISizeModifiable,
    IPaddingModifiable,
    Omit<ITextModifiable, "fontSize"> {
  bgGradient?: Property.Background;
  className?: string;
  content?: string;
  display?: Property.Display;
  lineSpacingSm?: Property.LineHeight<number | string>;
  textFill?: Property.WebkitTextFillColor;
}

type THeadingProps = ITypographyProps;
type TTextProps = ITypographyProps;
type TSpanProps = Omit<ITypographyProps, "bgGradient">;

export type { THeadingProps, TTextProps, TSpanProps, ITypographyProps };
