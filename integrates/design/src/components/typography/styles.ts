import { styled } from "styled-components";

import type { TTextProps } from "./types";

import { BaseSpanComponent, BaseTextComponent } from "components/@core";

interface IStyledTextProps {
  $bgGradient?: TTextProps["bgGradient"];
  $color?: TTextProps["color"];
  $content?: TTextProps["content"];
  $display?: TTextProps["display"];
  $fontFamily?: TTextProps["fontFamily"];
  $fontWeight?: TTextProps["fontWeight"];
  $letterSpacing?: TTextProps["letterSpacing"];
  $lineSpacing?: TTextProps["lineSpacing"];
  $lineSpacingSm?: TTextProps["lineSpacingSm"];
  $size: TTextProps["size"];
  $sizeMd?: TTextProps["sizeMd"];
  $sizeSm?: TTextProps["sizeSm"];
  $textFill?: TTextProps["textFill"];
  $whiteSpace?: TTextProps["whiteSpace"];
  $wordBreak?: TTextProps["wordBreak"];
}

type TStyledHeadingProps = IStyledTextProps;

const StyledHeading = styled(BaseTextComponent)<TStyledHeadingProps>`
  ${({
    theme,
    $bgGradient = "unset",
    $color = theme.palette.gray[800],
    $display = "block",
    $fontFamily = theme.typography.type.primary,
    $fontWeight = "bold",
    $letterSpacing = 0,
    $lineSpacing = 1.5,
    $lineSpacingSm,
    $size,
    $sizeMd,
    $sizeSm,
    $textFill = "unset",
    $whiteSpace = "pre-line",
    $wordBreak = "normal",
  }): string => `
    background: ${$bgGradient};
    color: ${$color};
    display: ${$display};
    font-family: ${$fontFamily};
    font-size: ${theme.typography.heading[$size]};
    font-weight: ${theme.typography.weight[$fontWeight]};
    letter-spacing: ${$letterSpacing};
    line-height: ${theme.spacing[$lineSpacing]};
    width: ${$display === "block" ? "100%" : "auto"};
    white-space: ${$whiteSpace};
    word-break: ${$wordBreak};
    background-clip: text;
    -webkit-background-clip: text;
    -webkit-text-fill-color: ${$textFill};

    @media screen
      and (min-width: ${theme.breakpoints.mobile})
      and (max-width: ${theme.breakpoints.tablet})
    {
      font-size: ${theme.typography.heading[$sizeMd ?? $size]};
    }

    @media screen and (max-width: ${theme.breakpoints.mobile}) {
      font-size: ${theme.typography.heading[$sizeSm ?? $size]};
      line-height: ${theme.spacing[$lineSpacingSm ?? $lineSpacing]};
    }
  `}
`;

const StyledText = styled(BaseTextComponent)<IStyledTextProps>`
  ${({
    theme,
    $bgGradient = "unset",
    $color = theme.palette.gray[600],
    $display = "block",
    $fontFamily = theme.typography.type.primary,
    $fontWeight = "regular",
    $letterSpacing = 0,
    $lineSpacing = 1.25,
    $lineSpacingSm,
    $size,
    $sizeMd,
    $sizeSm,
    $textFill = "unset",
    $whiteSpace = "pre-line",
    $wordBreak = "normal",
  }): string => `
    background: ${$bgGradient};
    color: ${$color};
    display: ${$display};
    font-family: ${$fontFamily};
    font-size: ${theme.typography.text[$size]};
    font-weight: ${theme.typography.weight[$fontWeight]};
    letter-spacing: ${$letterSpacing};
    line-height: ${theme.spacing[$lineSpacing]};
    width: ${$display === "block" ? "100%" : "auto"};
    white-space: ${$whiteSpace};
    word-break: ${$wordBreak};
    background-clip: text;
    -webkit-background-clip: text;
    -webkit-text-fill-color: ${$textFill};

    @media screen
      and (min-width: ${theme.breakpoints.mobile})
      and (max-width: ${theme.breakpoints.tablet})
    {
      font-size: ${theme.typography.text[$sizeMd ?? $size]};
    }

    @media screen and (max-width: ${theme.breakpoints.mobile}) {
      font-size: ${theme.typography.text[$sizeSm ?? $size]};
      line-height: ${theme.spacing[$lineSpacingSm ?? $lineSpacing]};
    }
  `}
`;

const StyledSpan = styled(BaseSpanComponent)<IStyledTextProps>`
  ${({
    theme,
    $color = theme.palette.gray[600],
    $content = undefined,
    $display = "inline",
    $fontWeight = "regular",
    $size,
    $lineSpacing = 1.25,
  }): string => `
    color: ${$color};
    display: ${$display};
    font-family: ${theme.typography.type.primary};
    font-size: ${theme.typography.text[$size]};
    font-weight: ${theme.typography.weight[$fontWeight]};
    line-height: ${theme.spacing[$lineSpacing]};
    max-width: 100%;
    width: ${$display === "block" ? "100%" : "auto"};

    ${$content === undefined ? "" : `&::after { content: "${$content}"; }`}
  `}
`;

export type { IStyledTextProps };
export { StyledHeading, StyledText, StyledSpan };
