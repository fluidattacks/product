import type { PropsWithChildren, Ref } from "react";
import { forwardRef } from "react";

import { StyledSpan } from "../styles";
import type { TSpanProps } from "../types";

const Span = forwardRef(function Span(
  {
    children,
    color,
    content,
    display,
    fontFamily,
    fontWeight,
    letterSpacing,
    lineSpacing,
    lineSpacingSm,
    size,
    sizeMd,
    sizeSm,
    ...props
  }: Readonly<PropsWithChildren<TSpanProps>>,
  ref: Ref<HTMLSpanElement>,
): JSX.Element {
  return (
    <StyledSpan
      $color={color}
      $content={content}
      $display={display}
      $fontFamily={fontFamily}
      $fontWeight={fontWeight}
      $letterSpacing={letterSpacing}
      $lineSpacing={lineSpacing}
      $lineSpacingSm={lineSpacingSm}
      $size={size}
      $sizeMd={sizeMd}
      $sizeSm={sizeSm}
      ref={ref}
      {...props}
    >
      {children}
    </StyledSpan>
  );
});

export { Span };
