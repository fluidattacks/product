import type { PropsWithChildren, Ref } from "react";
import { forwardRef } from "react";

import { StyledText } from "../styles";
import type { TTextProps } from "../types";

const Text = forwardRef(function Text(
  {
    bgGradient,
    children,
    className,
    color,
    display,
    fontFamily,
    fontWeight,
    letterSpacing,
    lineSpacing,
    lineSpacingSm,
    size,
    sizeMd,
    sizeSm,
    textFill,
    whiteSpace,
    wordBreak,
    ...props
  }: Readonly<PropsWithChildren<TTextProps>>,
  ref: Ref<HTMLParagraphElement>,
): JSX.Element {
  return (
    <StyledText
      $bgGradient={bgGradient}
      $color={color}
      $display={display}
      $fontFamily={fontFamily}
      $fontWeight={fontWeight}
      $letterSpacing={letterSpacing}
      $lineSpacing={lineSpacing}
      $lineSpacingSm={lineSpacingSm}
      $size={size}
      $sizeMd={sizeMd}
      $sizeSm={sizeSm}
      $textFill={textFill}
      $whiteSpace={whiteSpace}
      $wordBreak={wordBreak}
      className={className}
      ref={ref}
      {...props}
    >
      {children}
    </StyledText>
  );
});

export { Text };
