import { styled } from "styled-components";

const OverviewCardContainer = styled.div`
  ${({ className, theme }): string => `
    align-items: flex-start;
    border-radius: ${theme.spacing[1]};
    background: ${theme.palette.white};
    display: flex;
    flex-direction: column;
    gap: ${theme.spacing[1]};
    padding: ${theme.spacing[1.5]};
    position: relative;
    width: 320px;

    button {
      text-align: center;
      width: 100%;
    }

    ${
      className === "essential"
        ? `border: 2px solid ${theme.palette.gray[300]};`
        : `
          box-shadow: 0px 9px 18px 0px rgba(243, 38, 55, 0.06),
            0px 18px 27px 0px rgba(243, 38, 55, 0.10);

          &::before {
            background: ${theme.palette.gradients["01"]} border-box;
            border-radius: ${theme.spacing[1]};
            content: "";
            position: absolute;
            inset: 0;
            border: 2px solid transparent;
            mask:
              linear-gradient(#fff 0 0) padding-box, linear-gradient(#fff 0 0);
            mask-composite: exclude;
          }
        `
    }
  `}
`;

const FullCardContainer = styled.div`
  ${({ className, theme }): string => `
    align-items: flex-start;
    border-radius: ${theme.spacing[1]};
    display: flex;
    flex-direction: column;
    justify-content: center;
    gap: ${theme.spacing[1.5]};
    padding: ${theme.spacing[1.5]};
    width: 466px;

    button {
      text-align: center;
      width: 100%;
    }

    ${
      className === "essential"
        ? `
          background: linear-gradient(
            180deg,
            ${theme.palette.primary[400]} 0%,
            ${theme.palette.black} 100%
          );`
        : `
          background: ${theme.palette.white};
          box-shadow: 0px 9px 18px 0px rgba(243, 38, 55, 0.06),
            0px 18px 27px 0px rgba(243, 38, 55, 0.10);
        `
    }

    @media screen and (max-width: ${theme.breakpoints.mobile}) {
      width: 320px;
    }
  `}
`;

export { OverviewCardContainer, FullCardContainer };
