import { useTheme } from "styled-components";

import { IPriorityScoreProps } from "./types";

import { Container } from "components/container";
import { Icon } from "components/icon";
import { Text } from "components/typography";

const PriorityScore = ({
  score,
}: Readonly<IPriorityScoreProps>): JSX.Element => {
  const theme = useTheme();
  const lowerIcon = score <= 33 ? "priority-bars-low" : "priority-bars-medium";
  const icon = score > 66 || score === 0 ? "priority-bars-high" : lowerIcon;

  return (
    <Container alignItems={"center"} display={"inline-flex"} gap={0.25}>
      <Icon
        icon={icon}
        iconClass={score <= 66 ? "fa-kit-duotone" : ""}
        iconColor={
          score > 0 ? theme.palette.primary[500] : theme.palette.gray[200]
        }
        iconSize={"xs"}
        iconType={"fa-kit"}
        secondaryColor={theme.palette.gray[200]}
      />
      <Text color={theme.palette.gray[800]} size={"sm"} textAlign={"right"}>
        {score + "%"}
      </Text>
    </Container>
  );
};

export { PriorityScore };
