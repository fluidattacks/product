/*
 * Set a key attribute to force DOM updates for ...
 * https://stackoverflow.com/questions/49899245/setstate-not-updating-font-awesome-icon
 */
import type { Ref } from "react";
import { forwardRef } from "react";

import { IconWrap } from "./styles";
import type { IIconProps } from "./types";

const Icon = forwardRef(function Icon(
  {
    clickable,
    disabled,
    hoverColor,
    icon,
    iconClass = "",
    iconColor,
    iconMask,
    iconSize,
    iconTransform,
    iconType = "fa-solid",
    onClick,
    rotation,
    spanClass,
    secondaryColor,
    ...props
  }: Readonly<IIconProps>,
  ref: Ref<HTMLSpanElement>,
): JSX.Element {
  return (
    <IconWrap
      $clickable={clickable}
      $color={iconColor}
      $disabled={disabled}
      $hoverColor={hoverColor}
      $rotation={rotation}
      $secondaryColor={secondaryColor}
      $size={iconSize}
      className={spanClass}
      data-testid={`${icon}-icon`}
      key={`${iconType}-${icon}`}
      onClick={onClick}
      ref={ref}
      {...props}
    >
      <i
        className={`${iconType} fa-${icon} ${iconClass}`}
        {...(iconMask !== undefined ? { "data-fa-mask": iconMask } : {})}
        {...(iconTransform !== undefined
          ? { "data-fa-transform": iconTransform }
          : {})}
      />
    </IconWrap>
  );
});

export { Icon };
