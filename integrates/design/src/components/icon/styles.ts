import { styled } from "styled-components";

import type { IIconWrapProps } from "./types";

import { BaseSpanComponent, type TIconSize } from "components/@core";

interface IStyledIconWrapProps {
  $clickable?: IIconWrapProps["clickable"];
  $color?: IIconWrapProps["iconColor"];
  $hoverColor?: IIconWrapProps["hoverColor"];
  $disabled?: IIconWrapProps["disabled"];
  $rotation?: IIconWrapProps["rotation"];
  $size: IIconWrapProps["iconSize"];
  $secondaryColor: IIconWrapProps["secondaryColor"];
}

const sizes: Record<TIconSize, string> = {
  xl: "96px",
  lg: "64px",
  md: "32px",
  sm: "24px",
  xs: "16px",
  xxs: "12px",
  xxss: "10px",
};

const IconWrap = styled(BaseSpanComponent)<IStyledIconWrapProps>`
  ${({
    $clickable,
    $color = "inherit",
    $disabled = false,
    $hoverColor,
    $rotation,
    $size,
    $secondaryColor,
  }): string => {
    const hoverFilter = $hoverColor ? "" : "brightness(50%)";
    const hoverColor = $hoverColor ?? "";
    return `
      color: ${$color};
      cursor: ${$clickable ? "pointer" : "inherit"};
      font-size: ${sizes[$size]};
      line-height: 0;
      vertical-align: middle;
      pointer-events: inherit;
      opacity: ${$disabled ? "0.5" : "1"};

      > svg {
        --fa-secondary-color: ${$secondaryColor};
        transform: ${$rotation === undefined ? "" : `rotate(${$rotation}deg)`};
      }

      .fill-gradient-01 path {
        fill: url(/gradients.svg#gradient-01);
        fill: url(#gradient-01);
      }
      .fill-gradient-02 path {
        fill: url(/gradients.svg#gradient-02);
        fill: url(#gradient-02);
      }

      ${
        $clickable
          ? `&:hover {
              filter: ${hoverFilter};
              color: ${hoverColor};
            }`
          : ""
      }
    `;
  }}
`;

const IconInGalleryWrap = styled.div`
  cursor: pointer;
  transition: all 0.2s ease-in-out;

  &:hover:not([disabled]) {
    transform: scale(1.2);
  }
`;

export { IconWrap, IconInGalleryWrap };
