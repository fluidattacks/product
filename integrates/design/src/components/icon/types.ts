import type { MouseEventHandler } from "react";

import { IIconModifiable, IMarginModifiable } from "components/@core";

/**
 * Icon Wrapper component props.
 * @interface IIconWrapProps
 * @extends IMarginModifiable
 * @extends IIconModifiable
 * @property {boolean} [clickable] - Defines the cursor to show on icon hover.
 * @property {string} [hoverColor] - The icon color onn hover.
 * @property {Function} [onClick] - Function handler for click event.
 * @property {boolean} [disabled] - Show disabled icon state.
 * @property {number} [rotation] - Rotation degrees to modify icon style.
 */
interface IIconWrapProps extends IIconModifiable, IMarginModifiable {
  clickable?: boolean;
  hoverColor?: string;
  onClick?: MouseEventHandler<HTMLSpanElement>;
  disabled?: boolean;
  rotation?: number;
  secondaryColor?: string;
}

/**
 * Icon component props.
 * @interface IIconProps
 * @extends IIconWrapProps
 * @property {string} [iconClass] - The icon class from fontawesome.
 * @property {string} [iconMask] - The data-fa-mask prop of icon.
 * @property {string} [spanClass] - The icon span class.
 */
interface IIconProps extends IIconWrapProps {
  iconClass?: string;
  iconMask?: string;
  spanClass?: string;
}

export type { IIconProps, IIconWrapProps };
