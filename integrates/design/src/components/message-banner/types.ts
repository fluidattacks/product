import { IconName } from "@fortawesome/free-solid-svg-icons";
import type { Property } from "csstype";
import type { MouseEventHandler } from "react";

type TVariant = "web" | "platform";

/**
 * Message banner props.
 * @interface IMessageBannerProps
 * @extends IMessageBannerProps
 * @property {JSX.Element | string} message - Banner text.
 * @property {string} [buttonText] - Text for action button.
 * @property {FontWeight} [buttonFontWeight] - Weight for button text.
 * @property {IconName} [icon] - Icon for action button.
 * @property {Function} [onClickButton] - Action for action button.
 * @property {Function} [onClose] - Extra action when banner close.
 * @property {TVariant} [variant] - Variant to use: platform or web.
 */
interface IMessageBannerProps {
  message: JSX.Element | string;
  buttonText?: string;
  buttonFontWeight?: Property.FontWeight;
  icon?: IconName;
  onClickButton?: MouseEventHandler<HTMLButtonElement>;
  onClose?: () => void;
  buttonSide?: string;
}

export type { IMessageBannerProps, TVariant };
