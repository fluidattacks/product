import { useCallback, useState } from "react";
import { useTheme } from "styled-components";

import { Button } from "./styles";
import type { IMessageBannerProps } from "./types";

import { Container } from "components/container";
import { Icon } from "components/icon";
import { Text } from "components/typography";

const MessageBanner = ({
  message,
  onClickButton,
  buttonText,
  buttonFontWeight = "regular",
  icon,
  onClose,
  buttonSide = "end",
}: Readonly<IMessageBannerProps>): JSX.Element => {
  const theme = useTheme();
  const [show, setShow] = useState(true);
  const handleClose = useCallback((): void => {
    setShow(false);
    onClose?.();
  }, [onClose]);

  const button = (
    <Button onClick={onClickButton}>
      <Text
        color={theme.palette.white}
        fontWeight={buttonFontWeight}
        size={"sm"}
        textDecoration={"underline"}
      >
        {buttonText}
      </Text>
      <Icon
        icon={icon ?? "arrow-right"}
        iconColor={theme.palette.white}
        iconSize={"xs"}
        iconType={buttonFontWeight === "bold" ? "fa-regular" : "fa-light"}
      />
    </Button>
  );

  return (
    <Container
      alignItems={"center"}
      bgGradient={theme.palette.gradients["01"]}
      display={show ? "flex" : "none"}
      id={"message-banner"}
      justify={"space-between"}
      padding={[0.75, 0.75, 0.75, 0.75]}
    >
      <Container display={"flex"} gap={0.5} justify={"center"} width={"100%"}>
        {buttonSide === "start" && onClickButton ? button : undefined}
        <Container>
          {typeof message === "string" ? (
            <Text color={theme.palette.white} display={"inline"} size={"sm"}>
              {message}
            </Text>
          ) : (
            message
          )}
        </Container>
        {buttonSide === "end" && onClickButton ? button : undefined}
      </Container>
      <Icon
        clickable={true}
        hoverColor={theme.palette.primary[200]}
        icon={"close"}
        iconColor={theme.palette.white}
        iconSize={"xs"}
        iconType={"fa-light"}
        onClick={handleClose}
      />
    </Container>
  );
};
export { MessageBanner };
