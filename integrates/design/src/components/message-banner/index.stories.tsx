import type { Meta, StoryFn } from "@storybook/react";
import { expect, userEvent, within } from "@storybook/test";
import { useCallback } from "react";

import type { IMessageBannerProps } from "./types";

import { MessageBanner } from ".";

const config: Meta = {
  component: MessageBanner,
  parameters: {
    controls: {
      exclude: ["onClickButton"],
    },
  },
  tags: ["autodocs"],
  title: "Components/MessageBanner",
};

const Template: StoryFn<IMessageBannerProps> = ({
  buttonText,
  message,
}: Readonly<IMessageBannerProps>): JSX.Element => {
  const onCLick = useCallback((): void => {
    alert("Hello world :)");
  }, []);

  return (
    <MessageBanner
      buttonText={buttonText}
      message={message}
      onClickButton={onCLick}
    />
  );
};

const Default = Template.bind({});

Default.args = {
  buttonText: "Button Text",
  message: "This is a example of message",
};

Default.play = async ({ canvasElement, step }): Promise<void> => {
  const canvas = within(canvasElement);

  await step("should render a banner", async (): Promise<void> => {
    await expect(
      canvas.getByText("This is a example of message"),
    ).toBeInTheDocument();
    await userEvent.click(canvas.getByTestId("close-icon"));
    await expect(
      canvas.getByText("This is a example of message"),
    ).toBeInTheDocument();
  });
};

const Platform = Template.bind({});

Platform.args = {
  buttonText: "Button Text",
  message: "This is a example of platform banner",
};

Platform.play = async ({ canvasElement, step }): Promise<void> => {
  const canvas = within(canvasElement);

  await step("should render a platform banner", async (): Promise<void> => {
    await expect(
      canvas.getByText("This is a example of platform banner"),
    ).toBeInTheDocument();
    await userEvent.click(canvas.getByTestId("close-icon"));
    await expect(
      canvas.getByText("This is a example of platform banner"),
    ).toBeInTheDocument();
  });
};

export { Default, Platform };
export default config;
