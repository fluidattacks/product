import { TVariant } from "./types";

import { TSize } from "components/@core";

const getFontSize = (
  variant: TVariant,
): { normal: TSize; small: TSize | undefined } => {
  if (variant === "platform") {
    return { normal: "sm", small: undefined };
  }
  return { normal: "md", small: "sm" };
};

export { getFontSize };
