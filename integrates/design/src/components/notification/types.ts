type TNotificationVariant = "error" | "info" | "success" | "warning";

/**
 * Notification component props.
 * @interface INotificationProps
 * @property {string} description - The main content of the notification.
 * @property {string} title - The title of the notification.
 * @property {TNotificationVariant} variant - The notification custom variant.
 * @property { Function } [onClose] - Optional callback function when the notification is closed.
 */
interface INotificationProps {
  description: string;
  title: string;
  variant: TNotificationVariant;
  onClose?: () => void;
}

export type { INotificationProps, TNotificationVariant };
