import { useCallback } from "react";
import { useTheme } from "styled-components";

import { IconsContainer, NotificationContainer } from "./styles";
import type { INotificationProps } from "./types";
import { getIcon } from "./utils";

import { Container } from "components/container";
import { Icon } from "components/icon";
import { IconButton } from "components/icon-button";
import { Heading, Text } from "components/typography";

const Notification = ({
  description,
  onClose,
  title,
  variant,
}: Readonly<INotificationProps>): JSX.Element => {
  const theme = useTheme();
  const handleClose = useCallback((): void => {
    onClose?.();
  }, [onClose]);

  return (
    <NotificationContainer
      $variant={variant}
      className={`notification notification__${variant}`}
      role={"alertdialog"}
    >
      <IconsContainer $variant={variant}>
        <Icon icon={"circle"} iconClass={"ellipse-1"} iconSize={"xs"} />
        <Icon icon={"circle"} iconClass={"ellipse-2"} iconSize={"xs"} />
        <Icon
          icon={getIcon(variant)}
          iconClass={"icon"}
          iconSize={"xs"}
          iconType={"fa-light"}
        />
      </IconsContainer>
      <Container pr={1.5}>
        <Heading fontWeight={"bold"} size={"xs"}>
          {title}
        </Heading>
        <Text size={"sm"} whiteSpace={"break-spaces"} wordWrap={"break-word"}>
          {description}
        </Text>
      </Container>
      <IconButton
        height={"fit-content"}
        icon={"close"}
        iconColor={theme.palette.gray[400]}
        iconSize={"xs"}
        iconType={"fa-light"}
        onClick={handleClose}
        px={0.125}
        py={0.125}
        variant={"ghost"}
      />
    </NotificationContainer>
  );
};

export { Notification };
