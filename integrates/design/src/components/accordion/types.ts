type TAligned = "end" | "start";

/**
 * Accordion item properties.
 * @interface IAccordionItemProps
 * @property {string} title - Accordion title.
 * @property {string} [description] - Accordion description.
 * @property {JSX.Element} [extraElement] - Additional elements for the accordion.
 */
interface IAccordionItemProps {
  title: string;
  description?: string | JSX.Element;
  extraElement?: JSX.Element;
}

/**
 * Accordion Content props
 * @interface IAccordionContentProps
 * @property {IAccordionItemProps} item Accordion item.
 */
interface IAccordionContentProps {
  item: IAccordionItemProps;
}

/**
 * Pass more than 1 item to activate the progress variant.
 *
 * Accordion component properties.
 * @interface IAccordionProps
 * @property {TAligned} [aligned] - Alignment of the accordion title.
 * @property {IAccordionItemProps[]} items - A list of accordion items.
 * @property {string} [bgColor] - Background color of the accordion.
 */
interface IAccordionProps {
  aligned?: TAligned;
  items: IAccordionItemProps[];
  bgColor?: string;
}

export type {
  IAccordionProps,
  IAccordionItemProps,
  IAccordionContentProps,
  TAligned,
};
