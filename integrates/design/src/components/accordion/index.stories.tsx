import type { Meta, StoryFn } from "@storybook/react";
import { expect, userEvent, within } from "@storybook/test";

import type { IAccordionItemProps, IAccordionProps } from "./types";

import { Accordion } from ".";

const config: Meta = {
  component: Accordion,
  tags: ["autodocs"],
  title: "Components/Accordion",
};

const Template: StoryFn<IAccordionProps> = (props): JSX.Element => (
  <Accordion {...props} />
);

const accordionItemsMock: IAccordionItemProps[] = [
  {
    title: "Item 1",
    description: "This is the first item description.",
  },
  {
    title: "Item 2",
    description: "This is the second item description.",
  },
  {
    title: "Item 3",
    description: "This is the third item description.",
  },
  {
    title: "Item 4",
    description: "This is the fourth item description.",
  },
];

const Default = Template.bind({});
Default.args = {
  items: accordionItemsMock,
};

Default.play = async ({ canvasElement, step }): Promise<void> => {
  const canvas = within(canvasElement);

  await step("should render accordion component", async (): Promise<void> => {
    expect(canvas.getAllByText(/^Item/)).toHaveLength(4);
    await userEvent.click(canvas.getByText("Item 2"));
    expect(
      canvas.getByText("This is the second item description."),
    ).toBeVisible();
    await userEvent.click(canvas.getByText("Item 4"));
    expect(
      canvas.getByText("This is the fourth item description."),
    ).toBeVisible();
    await userEvent.click(canvas.getByText("Item 4"));
  });
};

export { Default };
export default config;
