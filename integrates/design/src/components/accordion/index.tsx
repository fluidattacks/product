import type { Ref } from "react";
import { forwardRef, useCallback, useState } from "react";
import { useTheme } from "styled-components";

import { AccordionContent } from "./accordion-content";
import type { IAccordionProps } from "./types";

import { Container } from "components/container";
import { Icon } from "components/icon";
import { Text } from "components/typography";

const Accordion = forwardRef(function Accordion(
  { aligned = "end", items, bgColor }: Readonly<IAccordionProps>,
  ref: Ref<HTMLDivElement>,
): JSX.Element {
  const theme = useTheme();
  const [isOpen, setIsOpen] = useState(false);
  const [currentItem, setCurrentItem] = useState(-1);

  const onItemClick = useCallback(
    (index: number): (() => void) => {
      // eslint-disable-next-line functional/functional-parameters
      return (): void => {
        setCurrentItem(index);
        if (index === currentItem) {
          setIsOpen((isOpen): boolean => !isOpen);
        } else {
          setIsOpen(true);
        }
      };
    },
    [currentItem],
  );

  return (
    <Container ref={ref}>
      {items.map((item, index): JSX.Element => {
        const isSelectedAndOpen = index === currentItem && isOpen;
        return (
          <Container bgColor={bgColor ?? theme.palette.white} key={item.title}>
            <Container
              aria-controls={`panel-${item.title}`}
              aria-expanded={isOpen}
              aria-label={item.title}
              as={"button"}
              bgColor={bgColor ?? theme.palette.white}
              bgColorHover={theme.palette.gray[100]}
              borderBottom={
                isSelectedAndOpen || index === items.length - 1
                  ? "1px solid"
                  : undefined
              }
              borderColor={theme.palette.gray[200]}
              borderTop={"1px solid"}
              cursor={"pointer"}
              display={"flex"}
              onClick={onItemClick(index)}
              padding={[0.75, 0.75, 0.75, 0.75]}
              width={"100%"}
            >
              <Container
                as={"summary"}
                display={"flex"}
                flexDirection={aligned === "end" ? "row" : "row-reverse"}
                gap={0.75}
                justify={"space-between"}
                width={"100%"}
              >
                <Text
                  color={theme.palette.gray[800]}
                  fontWeight={"bold"}
                  size={"sm"}
                  sizeSm={"sm"}
                  textAlign={"start"}
                >
                  {item.title}
                </Text>
                <Icon
                  icon={isSelectedAndOpen ? "chevron-up" : "chevron-down"}
                  iconColor={theme.palette.gray[400]}
                  iconSize={"xs"}
                  iconType={"fa-light"}
                />
              </Container>
            </Container>
            {isSelectedAndOpen && <AccordionContent item={item} />}
          </Container>
        );
      })}
    </Container>
  );
});

export { Accordion };
export type { IAccordionProps };
