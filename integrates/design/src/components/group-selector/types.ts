type TVariant = "organization-selector";

/**
 * Item handlers.
 * @interface IItemHandlers
 * @property { Function } [handleNewOrganization] - Handle new organization.
 * @property { Function } [onSelect] - Handle select item.
 * @property { Function } [onClose] - Handle close.
 */
interface IItemHandlers {
  handleNewOrganization?: () => void;
  onSelect?: (itemName: string) => void;
  onClose?: () => void;
}

/**
 * Group selector component props.
 * @interface IGroupSelectorProps
 * @extends IItemHandlers
 * @property { boolean } [isOpen] - Group selector is open.
 * @property { {name: string}[] } items - Items to display.
 * @property { string } [selectedItem] - Selected item.
 * @property { string } title - Group selector title.
 * @property { TVariant } [variant] - Group selector variant.
 */
interface IGroupSelectorProps extends IItemHandlers {
  isOpen?: boolean;
  items: {
    name: string;
  }[];
  selectedItem: string;
  title: string;
  variant?: TVariant;
}

/**
 * Option component props.
 * @interface IOptionProps
 * @extends IItemHandlers
 * @property { boolean } [isSelected] - Option is selected.
 * @property { string } label - Option label.
 */
interface IOptionProps extends IItemHandlers {
  isSelected?: boolean;
  label: string;
}

export type { IGroupSelectorProps, IOptionProps, TVariant };
