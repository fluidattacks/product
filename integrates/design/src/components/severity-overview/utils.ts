import { capitalize } from "lodash";

import { IVariant, TSeverityOverviewBadgeVariant } from "./types";

import { theme } from "components/colors";

const getVariant = (
  variant: TSeverityOverviewBadgeVariant,
  value: number,
): IVariant => {
  const iconColors: Record<string, string> = {
    critical: theme.palette.primary[700],
    high: theme.palette.error[500],
    low: theme.palette.warning[200],
    medium: theme.palette.warning[500],
    none: theme.palette.gray[200],
  };
  const textColors: Record<string, string> = {
    disabled: theme.palette.gray[400],
    enabled: theme.palette.gray[800],
  };
  const iconTextColors: Record<string, string> = {
    critical: theme.palette.white,
    high: theme.palette.white,
    low: theme.palette.gray[800],
    medium: theme.palette.gray[800],
    none: theme.palette.gray[400],
  };

  return {
    iconColor: value < 1 ? iconColors.none : iconColors[variant],
    iconText: capitalize(variant.charAt(0)),
    iconTextColor: value < 1 ? iconTextColors.none : iconTextColors[variant],
    textColor: value < 1 ? textColors.disabled : textColors.enabled,
  };
};

export { getVariant };
