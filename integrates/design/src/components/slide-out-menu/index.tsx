/* eslint-disable functional/functional-parameters */
import { LazyMotion, domAnimation } from "motion/react";
import { PropsWithChildren, useCallback, useEffect, useRef } from "react";
import { useTheme } from "styled-components";

import { type IMenuItemProps, MenuItem } from "./menu-item";
import { SlideMenuContainer } from "./styles";
import type { ISlideOutMenuProps } from "./types";

import { Button } from "components/button";
import { Container } from "components/container";
import { IconButton } from "components/icon-button";
import { Text } from "components/typography";

const SlideOutMenu = ({
  children,
  closeIconId = "close-menu",
  isOpen,
  items,
  onClose,
  primaryButtonText,
  primaryOnClick,
  secondaryButtonText,
  secondaryOnClick,
  title,
}: Readonly<PropsWithChildren<ISlideOutMenuProps>>): JSX.Element => {
  const theme = useTheme();
  const slideMenuRef = useRef<HTMLDivElement>(null);

  const handleItemClick = useCallback(
    (onClick: VoidFunction): VoidFunction =>
      (): void => {
        onClick();
        onClose();
      },
    [onClose],
  );

  useEffect((): (() => void) => {
    const events = ["mousedown", "touchstart"];
    const onClickOutside = (event: Readonly<Event>): void => {
      const target = event.target as HTMLElement;
      const helpButton = document.getElementById("helpBtn");
      const filterButton = document.getElementById("filterBtn");
      const invalidTarget =
        (target === helpButton ||
          (helpButton?.contains(target) ?? false) ||
          target === filterButton ||
          filterButton?.contains(target)) ??
        false;
      if (
        isOpen &&
        !invalidTarget &&
        !(slideMenuRef.current?.contains(target) ?? false)
      ) {
        onClose();
      }
    };
    events.forEach((event): void => {
      document.addEventListener(event, onClickOutside, { passive: true });
    });

    return (): void => {
      events.forEach((event): void => {
        document.removeEventListener(event, onClickOutside);
      });
    };
  }, [isOpen, onClose]);

  return (
    <LazyMotion features={domAnimation}>
      <SlideMenuContainer
        animate={
          isOpen
            ? {
                display: "block",
                x: 0,
              }
            : {
                transitionEnd: {
                  display: "none",
                },
                x: "100%",
              }
        }
        initial={{
          display: "none",
          x: "100%",
        }}
        ref={slideMenuRef}
        transition={{
          bounce: 0,
          duration: 0.6,
          type: "spring",
        }}
      >
        <Container height={"100%"} position={"relative"}>
          <Container
            alignItems={"center"}
            bgColor={theme.palette.white}
            borderBottom={"1px solid"}
            borderColor={theme.palette.gray[200]}
            display={"flex"}
            gap={1.25}
            justify={"space-between"}
            padding={[1.25, 1.25, 1.25, 1.25]}
            position={"sticky"}
            top={"0"}
            zIndex={40}
          >
            <Text
              color={theme.palette.black}
              display={"inline"}
              fontWeight={"bold"}
              size={"xl"}
            >
              {title}
            </Text>
            <IconButton
              icon={"close"}
              iconColor={theme.palette.gray[400]}
              iconSize={"xs"}
              iconTransform={"grow-4"}
              iconType={"fa-light"}
              id={closeIconId}
              onClick={onClose}
              px={0.25}
              py={0.25}
              variant={"ghost"}
            />
          </Container>
          <Container
            display={"flex"}
            flexDirection={"column"}
            justify={"space-between"}
            minHeight={"100vh"}
          >
            <Container py={1.25}>
              {(items ?? []).map(
                ({
                  customBadge,
                  description,
                  icon,
                  onClick,
                  requiresUpgrade,
                  title: itemTitle,
                }): JSX.Element => {
                  return (
                    <MenuItem
                      customBadge={customBadge}
                      description={description}
                      icon={icon}
                      key={itemTitle}
                      onClick={handleItemClick(onClick)}
                      requiresUpgrade={requiresUpgrade}
                      title={itemTitle}
                    />
                  );
                },
              )}
              {children && <Container px={1.25}>{children}</Container>}
            </Container>
            {primaryButtonText === undefined ? undefined : (
              <Container
                alignItems={"center"}
                bgColor={theme.palette.white}
                borderColor={theme.palette.gray[200]}
                borderTop={"1px solid"}
                bottom={"0"}
                display={"flex"}
                gap={1.25}
                justify={"end"}
                padding={[1.25, 1.25, 1.25, 1.25]}
                position={"sticky"}
                width={"100%"}
                zIndex={40}
              >
                {secondaryButtonText === undefined ? undefined : (
                  <Button
                    color={`${theme.palette.primary[500]} !important`}
                    onClick={secondaryOnClick}
                    pl={0.5}
                    pr={0.5}
                    variant={"ghost"}
                  >
                    {secondaryButtonText}
                  </Button>
                )}
                <Button onClick={primaryOnClick} type={"submit"}>
                  {primaryButtonText}
                </Button>
              </Container>
            )}
          </Container>
        </Container>
      </SlideMenuContainer>
    </LazyMotion>
  );
};

export type { IMenuItemProps };
export { SlideOutMenu };
