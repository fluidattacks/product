import { useTheme } from "styled-components";

import type { IMenuItemProps } from "../types";
import { Container } from "components/container";
import { Icon } from "components/icon";
import { PremiumFeature } from "components/premium-feature";
import { Text } from "components/typography";

const MenuItem = ({
  customBadge = undefined,
  description,
  icon,
  onClick,
  requiresUpgrade = false,
  title,
}: Readonly<IMenuItemProps>): JSX.Element => {
  const theme = useTheme();

  return (
    <Container
      bgColor={theme.palette.white}
      bgColorHover={theme.palette.gray[100]}
      cursor={"pointer"}
      display={"flex"}
      gap={0.5}
      onClick={onClick}
      px={1.25}
      py={1}
      width={"100%"}
    >
      <Container
        alignItems={"center"}
        bgColor={theme.palette.gray[300]}
        borderRadius={theme.spacing[0.25]}
        display={"flex"}
        height={"24px"}
        justify={"center"}
        width={"24px"}
      >
        <Icon
          icon={icon}
          iconColor={theme.palette.gray[800]}
          iconSize={"xxs"}
          ml={0.5}
          mr={0.5}
        />
      </Container>
      <Container>
        <Container alignItems={"center"} display={"inline-flex"} gap={0.5}>
          <Text color={theme.palette.gray[800]} size={"sm"}>
            {title}
          </Text>
          {requiresUpgrade ? <PremiumFeature text={"Upgrade"} /> : undefined}
          {customBadge}
        </Container>
        <Text color={theme.palette.gray[400]} size={"xs"}>
          {description}
        </Text>
      </Container>
    </Container>
  );
};

export type { IMenuItemProps };
export { MenuItem };
