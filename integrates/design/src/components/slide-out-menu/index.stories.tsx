import type { Meta, StoryFn } from "@storybook/react";
import { PropsWithChildren, useCallback, useState } from "react";

import type { IMenuItemProps, ISlideOutMenuProps } from "./types";

import { SlideOutMenu } from ".";
import { Button } from "../button";
import { Input } from "../inputs";

const config: Meta = {
  component: SlideOutMenu,
  tags: ["autodocs"],
  title: "Components/SlideOutMenu",
};

const handleSubmit = (): void => {
  console.warn("On click");
};

const Template: StoryFn<PropsWithChildren<ISlideOutMenuProps>> = ({
  items,
  title,
}: Readonly<PropsWithChildren<ISlideOutMenuProps>>): JSX.Element => {
  const [isOpenMenu, setIsOpenMenu] = useState(false);

  const handleMenu = useCallback((): void => {
    setIsOpenMenu(!isOpenMenu);
  }, [isOpenMenu]);

  return (
    <div className={"h-[600px]"}>
      <Button onClick={handleMenu}>{isOpenMenu ? "Close" : "Open"}</Button>
      <SlideOutMenu
        isOpen={isOpenMenu}
        items={items}
        onClose={handleMenu}
        title={title}
      />
    </div>
  );
};

const items: IMenuItemProps[] = [
  {
    description: "Discuss any issue with reported vulnerabilities",
    icon: "headphones",
    onClick: handleSubmit,
    title: "Talk to a hacker",
  },
  {
    description:
      "Get answers to your questions about reported vulnerabilities or using the platform",
    icon: "message",
    onClick: handleSubmit,
    title: "Live chat",
  },
  {
    description:
      "Learn with our experts how to get the most out of the platform",
    icon: "life-ring",
    onClick: handleSubmit,
    title: "Learn how to use",
  },
  {
    description: "Read our product documentation and related help articles",
    icon: "book",
    onClick: handleSubmit,
    title: "Documentation",
  },
];

const Default = Template.bind({});

Default.args = {
  items,
  title: "Help",
};

const Filters: StoryFn = (): JSX.Element => {
  return (
    <div className={"h-[900px]"}>
      <SlideOutMenu
        isOpen={true}
        onClose={handleSubmit}
        primaryButtonText={"Save filters"}
        primaryOnClick={handleSubmit}
        secondaryButtonText={"Cancel"}
        secondaryOnClick={handleSubmit}
        title={"Filters"}
      >
        <Input
          disabled={false}
          label={"Location"}
          name={"exampleName"}
          placeholder={"Example placeholder"}
          required={false}
        />
      </SlideOutMenu>
    </div>
  );
};

export { Default, Filters };
export default config;
