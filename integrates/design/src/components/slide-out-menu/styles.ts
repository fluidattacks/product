import { m } from "motion/react";
import { styled } from "styled-components";

const SlideMenuContainer = styled(m.div)`
  ${({ theme }): string => `
    background-color: ${theme.palette.white};
    box-shadow: ${theme.shadows.lg};
    max-width: 350px;
    min-width: 350px;
    height: 100%;
    position: absolute;
    right: 0;
    top: 0;
    overflow: hidden auto;
    scrollbar-color: ${theme.palette.gray[600]} ${theme.palette.gray[100]};
    scroll-padding: ${theme.spacing[0.5]};
    scrollbar-width: thin;
    z-index: 99999;

    > div {
      border: 1px solid ${theme.palette.gray[100]};
      width: auto;
      min-height: 100vh;
      height: auto;
    }
  `}
`;

export { SlideMenuContainer };
