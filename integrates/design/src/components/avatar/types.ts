/**
 * Avatar component props.
 * @interface IAvatarProps
 * @property { Function } [onClick] - Called when the avatar is clicked.
 * @property { string } [userName] - The name of the user.
 * @property { boolean } [showIcon] - Whether the avatar should show the icon.
 * @property { boolean } [showUsername] - Whether the avatar should show the username.
 */
interface IAvatarProps {
  onClick?: React.MouseEventHandler;
  userName: string;
  showIcon?: boolean;
  showUsername?: boolean;
}

export type { IAvatarProps };
