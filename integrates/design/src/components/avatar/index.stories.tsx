import type { Meta, StoryFn } from "@storybook/react";
import React from "react";

import type { IAvatarProps } from ".";
import { Avatar } from ".";

const config: Meta = {
  component: Avatar,
  tags: ["autodocs"],
  title: "Components/Avatar",
};

const Template: StoryFn<Readonly<React.PropsWithChildren<IAvatarProps>>> = (
  props,
): JSX.Element => <Avatar {...props} />;

const Default = Template.bind({});

Default.args = {
  onClick: (): void => {},
  userName: "User",
  showIcon: true,
  showUsername: true,
};

const UserName = Template.bind({});
UserName.args = {
  userName: "User",
  showIcon: false,
  showUsername: true,
};

const Icon = Template.bind({});
Icon.args = {
  userName: "User",
  showIcon: false,
  showUsername: false,
};

export { Default, UserName, Icon };
export default config;
