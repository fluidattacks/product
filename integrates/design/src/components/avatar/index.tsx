import type { PropsWithChildren, Ref } from "react";
import { forwardRef } from "react";

import { ChildrenContainer, InitialsCircle } from "./styles";
import type { IAvatarProps } from "./types";

import { Button } from "components/button";
import { Container } from "components/container";
import { Icon } from "components/icon";
import { Text } from "components/typography";

const Avatar = forwardRef(function Avatar(
  {
    children,
    onClick,
    userName,
    showIcon = false,
    showUsername = false,
  }: Readonly<PropsWithChildren<IAvatarProps>>,
  ref: Ref<HTMLDivElement>,
): JSX.Element {
  const isInteractive = Boolean(onClick);

  return (
    <Container id={"navbar-user-profile"} ref={ref} zIndex={9999}>
      <Container
        alignItems={"end"}
        display={"flex"}
        flexDirection={"column"}
        position={"relative"}
        zIndex={30}
      >
        {isInteractive ? (
          <Button
            onClick={onClick}
            pb={0.5}
            pl={0.5}
            pr={0.5}
            pt={0.5}
            variant={"ghost"}
          >
            <InitialsCircle>{userName[0].toUpperCase()}</InitialsCircle>
            {showUsername ? (
              <Text color={"#1d2939"} display={"inline"} ml={0.5} size={"sm"}>
                {userName.split(" ")[0]}
              </Text>
            ) : undefined}
            {showIcon ? (
              <Icon
                clickable={false}
                icon={"ellipsis-v"}
                iconSize={"xxss"}
                ml={0.5}
              />
            ) : undefined}
          </Button>
        ) : (
          <Container alignItems={"center"} display={"flex"} px={0.5} py={0.5}>
            <InitialsCircle>{userName[0].toUpperCase()}</InitialsCircle>
            {showUsername ? (
              <Text color={"#1d2939"} display={"inline"} ml={0.5} size={"sm"}>
                {"Hi "}
                {userName.split(" ")[0]}
                {"!"}
              </Text>
            ) : undefined}
            {showIcon ? (
              <Icon
                clickable={false}
                icon={"ellipsis-v"}
                iconSize={"xxss"}
                ml={0.5}
              />
            ) : undefined}
          </Container>
        )}
        <ChildrenContainer>{children}</ChildrenContainer>
      </Container>
    </Container>
  );
});

export type { IAvatarProps };
export { Avatar };
