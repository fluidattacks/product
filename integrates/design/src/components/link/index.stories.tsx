import type { Meta, StoryFn } from "@storybook/react";
import { PropsWithChildren } from "react";
import { BrowserRouter } from "react-router-dom";

import type { ILinkProps } from ".";
import { Link } from ".";
import { theme } from "components/colors";

const config: Meta = {
  component: Link,
  tags: ["autodocs"],
  title: "Components/Link",
};

const Template: StoryFn<Readonly<PropsWithChildren<ILinkProps>>> = (
  props,
): JSX.Element => (
  <BrowserRouter>
    <Link {...props} />
  </BrowserRouter>
);

const Default = Template.bind({});
Default.args = {
  children: "Example test",
  href: "/groups",
};

const ExternalLink = Template.bind({});
ExternalLink.args = {
  children: "Example test",
  href: "https://test.com",
};

const HighRelevanceLink = Template.bind({});
HighRelevanceLink.args = {
  children: "Example test",
  href: "/groups",
  variant: "highRelevance",
};

const CustomLink = Template.bind({});
CustomLink.args = {
  children: "Example test",
  color: theme.palette.info[700],
  href: "https://test.com",
};

export { Default, ExternalLink, HighRelevanceLink, CustomLink };
export default config;
