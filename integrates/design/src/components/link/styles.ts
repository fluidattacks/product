import { Link } from "react-router-dom";
import { css, styled } from "styled-components";

import type { TVariant } from "./types";

import { variantBuilder } from "components/@core/variants/utils";

interface IStyledLinkProps {
  $color?: string;
  $variant: TVariant;
}

const { getVariant } = variantBuilder(
  (theme): Record<TVariant, string> => ({
    highRelevance: `
      color: ${theme.palette.primary[500]};

      &:hover:not([disabled]) {
        color: ${theme.palette.primary[400]};
      }
    `,
    lowRelevance: `
      color: ${theme.palette.gray[800]};

      &:hover:not([disabled]) {
        color: ${theme.palette.gray[500]};
      }
    `,
  }),
);

const sharedStyles = css<IStyledLinkProps>`
  ${({ theme, $color, $variant }): string => `
    align-items: center;
    cursor: pointer;
    font-family: ${theme.typography.type.primary};
    font-size: ${theme.typography.text.sm};
    font-weight: ${theme.typography.weight.regular};
    gap: ${theme.spacing[0.25]};
    line-height: ${theme.spacing[1.25]};
    text-decoration: underline;
    word-break: break-word;
    word-wrap: break-word;

    &:disabled {
      color: ${theme.palette.gray[300]};
      cursor: not-allowed;
    }

    &:has(button) {
      text-decoration: none;
    }

    ${$color ? `color: ${$color}` : getVariant(theme, $variant)}
  `}
`;

const StyledExternalLink = styled.a<IStyledLinkProps>`
  ${sharedStyles}
`;

const StyledInternalLink = styled(Link)<IStyledLinkProps>`
  ${sharedStyles}
`;

export { StyledExternalLink, StyledInternalLink };
