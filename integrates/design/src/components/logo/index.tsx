import { ILogoProps, TLogoVariant } from "./types";

import { CloudImage } from "components/cloud-image";
import { Container } from "components/container";

const containerProps: Record<TLogoVariant, { height?: string; width: string }> =
  {
    company: { height: "83px", width: "460px" },
    footer: { height: "57px", width: "315px" },
    header: { height: "35px", width: "160px" },
    icon: { height: "40px", width: "40px" },
    inline: { width: "150px" },
  };

const Logo = ({ publicId, variant }: Readonly<ILogoProps>): JSX.Element => {
  return (
    <Container {...containerProps[variant]}>
      <CloudImage alt={"logo"} publicId={publicId} />
    </Container>
  );
};

export { Logo };
