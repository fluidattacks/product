// Fluid logo variants
type TLogoVariant = "company" | "footer" | "header" | "icon" | "inline";

/**
 * Logo component props.
 * @interface ILogoProps
 * @property {string} publicId - The publicId location in cloudinary.
 * @property {TLogoVariant} variant - The different types of logo used.
 */
interface ILogoProps {
  publicId: string;
  variant: TLogoVariant;
}

export type { ILogoProps, TLogoVariant };
