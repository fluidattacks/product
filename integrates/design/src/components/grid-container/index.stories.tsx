import type { Meta, StoryFn } from "@storybook/react";

import type { IGridContainerProps } from "./types";

import { GridContainer } from ".";
import { Container } from "../container";

const config: Meta = {
  component: GridContainer,
  tags: ["autodocs"],
  title: "Components/GridContainer",
};

const Template: StoryFn<IGridContainerProps> = ({
  xl,
  lg,
  md,
  sm,
}: Readonly<IGridContainerProps>): JSX.Element => {
  const range = Array.from(
    { length: 12 },
    (_, index): string => `key-${index}`,
  );

  return (
    <GridContainer lg={lg} md={md} sm={sm} xl={xl}>
      {range.map(
        (key, index): JSX.Element => (
          <Container
            alignItems={"center"}
            display={"flex"}
            justify={"center"}
            key={key}
          >
            {index}
          </Container>
        ),
      )}
    </GridContainer>
  );
};

const Default = Template.bind({});
Default.args = {
  lg: 9,
  md: 6,
  sm: 3,
  xl: 12,
};

export default config;
export { Default };
