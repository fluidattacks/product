import type { Meta, StoryFn } from "@storybook/react";
import React from "react";

import type { IAlertProps } from ".";
import { Alert } from ".";

const config: Meta = {
  argTypes: {
    id: {
      control: {
        disable: true,
      },
    },
  },
  component: Alert,
  parameters: {
    controls: {
      exclude: ["onTimeOut"],
    },
  },
  tags: ["autodocs"],
  title: "Components/Alert",
};

const Template: StoryFn<Readonly<React.PropsWithChildren<IAlertProps>>> = (
  props,
): JSX.Element => <Alert {...props} />;

const ErrorState = Template.bind({});

ErrorState.args = {
  children:
    "Decreases 40% of total risk exposure (CVSS) by remediating this vulnerability",
  variant: "error",
};

const Success = Template.bind({});
Success.args = {
  children:
    "Decreases 40% of total risk exposure (CVSS) by remediating this vulnerability",
  variant: "success",
};

const Warning = Template.bind({});
Warning.args = {
  children:
    "Decreases 40% of total risk exposure (CVSS) by remediating this vulnerability",
  variant: "warning",
};

const Info = Template.bind({});
Info.args = {
  children:
    "Decreases 40% of total risk exposure (CVSS) by remediating this vulnerability",
  variant: "info",
};

export { ErrorState, Success, Warning, Info };
export default config;
