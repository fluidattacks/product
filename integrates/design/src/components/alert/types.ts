import type { Dispatch, HTMLAttributes, SetStateAction } from "react";

import type { IMarginModifiable } from "components/@core/types";

type TAlertAndNotifyVariant = "error" | "info" | "success" | "warning";

/**
 * Alert component props.
 * @interface IAlertProps
 * @extends IMarginModifiable
 * @property {boolean} [autoHide] - Automatically hide the alert after a certain time.
 * @property {boolean} [closable] - Whether the alert should be closable.
 * @property {boolean} [show] - Whether the alert should be visible.
 * @property {number} [time] - The duration (in seconds) to show the alert.
 * @property {Function} [onTimeOut] - Callback function to be executed when the alert timeout.
 * @property {TAlertAndNotifyVariant} [variant] - The variant of the alert (error, info, success, warning).
 */
interface IAlertProps
  extends IMarginModifiable,
    HTMLAttributes<HTMLDivElement> {
  autoHide?: boolean;
  closable?: boolean;
  show?: boolean;
  time?: 4 | 8 | 12;
  onTimeOut?: Dispatch<SetStateAction<boolean>>;
  variant?: TAlertAndNotifyVariant;
}

export type { IAlertProps, TAlertAndNotifyVariant };
