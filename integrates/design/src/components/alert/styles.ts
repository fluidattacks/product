import type { IconName } from "@fortawesome/free-solid-svg-icons";
import { styled } from "styled-components";
import type { DefaultTheme } from "styled-components";

import { TAlertAndNotifyVariant } from "./types";

import { variantBuilder } from "components/@core/variants/utils";

interface IStyledAlertAndNotifyProps {
  $variant?: TAlertAndNotifyVariant;
}

const getIcon = (variant: TAlertAndNotifyVariant): IconName => {
  switch (variant) {
    case "warning":
      return "triangle-exclamation";
    case "info":
      return "circle-info";
    case "success":
      return "circle-check";
    case "error":
    default:
      return "circle-exclamation";
  }
};

const { getVariant } = variantBuilder(
  (theme: DefaultTheme): Record<TAlertAndNotifyVariant, string> => ({
    error: `
      background: ${theme.palette.error["50"]};
      border: 1px solid ${theme.palette.error["500"]};
      color: ${theme.palette.error["700"]};

      button {
        color: ${theme.palette.error["700"]};

        &:hover:not([disabled]) {
          background-color: ${theme.palette.error["200"]};
          color: ${theme.palette.error["700"]};
        }
      }
    `,
    info: `
      background: ${theme.palette.info["50"]};
      border: 1px solid ${theme.palette.info["500"]};
      color: ${theme.palette.info["700"]};

      button {
        color: ${theme.palette.info["700"]};

        &:hover:not([disabled]) {
          background-color: ${theme.palette.info["200"]};
          color: ${theme.palette.info["700"]};
        }
      }
    `,
    success: `
      background: ${theme.palette.success["50"]};
      border: 1px solid ${theme.palette.success["500"]};
      color: ${theme.palette.success["700"]};

      button {
        color: ${theme.palette.success["700"]};

        &:hover:not([disabled]) {
          background-color: ${theme.palette.success["200"]};
          color: ${theme.palette.success["700"]};
        }
      }
    `,
    warning: `
      background: ${theme.palette.warning["50"]};
      border: 1px solid ${theme.palette.warning["500"]};
      color: ${theme.palette.warning["700"]};

      button {
        color: ${theme.palette.warning["700"]};

        &:hover:not([disabled]) {
          background-color: ${theme.palette.warning["200"]};
          color: ${theme.palette.warning["700"]};
        }
      }
    `,
  }),
);

const AlertContainer = styled.div<IStyledAlertAndNotifyProps>`
  ${({ theme, $variant }): string => `
    background: transparent;
    align-items: center;
    border-radius: ${theme.spacing[0.25]};
    display: inline-flex;
    font-family: ${theme.typography.type.primary};
    font-size: ${theme.typography.text.sm};
    font-weight: ${theme.typography.weight.bold};
    line-height: ${theme.spacing[1.25]};
    min-width: 250px;
    width: 100%;
    padding: ${theme.spacing[0.75]};
    position: relative;
    gap: ${theme.spacing[0.75]};

    button {
      background-color: transparent;
    }

    ${getVariant(theme, $variant ?? "error")}
  `}
`;

const MessageContainer = styled.div`
  padding-right: 4px;
  width: 100%;
`;

export { getIcon, AlertContainer, MessageContainer };
