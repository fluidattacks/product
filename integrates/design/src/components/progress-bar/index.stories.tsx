import type { Meta, StoryObj } from "@storybook/react";

import type { IProgressBarProps } from "./types";

import { ProgressBar } from ".";
import { Container } from "../container";

const config: Meta = {
  component: ProgressBar,
  tags: ["autodocs"],
  title: "Components/ProgressBar",
};

const DefaultTemplate: StoryObj<{
  itemsProps: IProgressBarProps[];
}> = {
  render: ({ itemsProps }): JSX.Element => (
    <Container display={"flex"} flexDirection={"column"} gap={2.5}>
      {itemsProps.map((props, index): JSX.Element => {
        const key = `default-bar-${index}`;

        return <ProgressBar {...props} key={key} />;
      })}
    </Container>
  ),
};

const ComplianceTemplate: StoryObj<{
  itemsProps: IProgressBarProps[];
}> = {
  render: ({ itemsProps }): JSX.Element => (
    <Container display={"flex"} flexDirection={"column"} gap={2.5}>
      {itemsProps.map((props, index): JSX.Element => {
        const key = `compliance-bar-${index}`;

        return <ProgressBar {...props} key={key} />;
      })}
    </Container>
  ),
};

const EnrollmentTemplate: StoryObj<{
  itemsProps: IProgressBarProps[];
}> = {
  render: ({ itemsProps }): JSX.Element => (
    <Container display={"flex"} flexDirection={"column"} gap={2.5}>
      {itemsProps.map((props, index): JSX.Element => {
        const key = `enrollment-bar-${index}`;

        return <ProgressBar {...props} key={key} />;
      })}
    </Container>
  ),
};

const Default = {
  ...DefaultTemplate,
  args: {
    itemsProps: [
      {
        minWidth: 300,
        percentage: 1,
        percentageLocation: "right",
        showPercentage: true,
      },
      {
        minWidth: 300,
        percentage: 100,
        percentageLocation: "right",
        showPercentage: true,
      },
      {
        minWidth: 300,
        percentage: 100,
        percentageLocation: "left",
        showPercentage: true,
      },
      {
        minWidth: 300,
        percentage: 1,
        percentageLocation: "left",
        showPercentage: true,
      },
      {
        minWidth: 300,
        percentage: 70,
      },
    ],
  },
};
const Compliance = {
  ...ComplianceTemplate,
  args: {
    itemsProps: [
      {
        minWidth: 300,
        percentage: 70,
        showPercentage: true,
        variant: "compliance",
      },
      {
        minWidth: 300,
        percentage: 80,
        showPercentage: true,
        variant: "compliance",
      },
      {
        minWidth: 300,
        percentage: 49,
        showPercentage: true,
        variant: "compliance",
      },
    ],
  },
};

const Variants = {
  ...EnrollmentTemplate,
  args: {
    itemsProps: [
      {
        minWidth: 300,
        percentage: 70,
        showPercentage: true,
        variant: "small",
      },
      {
        minWidth: 300,
        percentage: 50,
        rounded: false,
        showPercentage: true,
        variant: "small",
      },
      {
        minWidth: 300,
        percentage: 60,
        showPercentage: true,
      },
      {
        minWidth: 300,
        percentage: 90,
        rounded: false,
        showPercentage: true,
      },
    ],
  },
};

export default config;
export { Default, Compliance, Variants };
