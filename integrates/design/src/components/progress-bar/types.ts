type TOrientation = "horizontal" | "vertical";
type TPercentageLocation = "left" | "right";
type TProgressVariant =
  | "compliance"
  | "default"
  | "progressIndicator"
  | "progressIndicatorError"
  | "small";

/**
 * Progress bar component props.
 * @interface IProgressBarProps
 * @property { number } [minWidth] - Minimum width of the progress bar.
 * @property { number } percentage - Percentage of the progress bar.
 * @property { TPercentageLocation } [percentageLocation] - Location of the percentage text.
 * @property { boolean } [rounded] - Whether to round the progress bar corners.
 * @property { boolean } [showPercentage] - Whether to show the percentage text.
 * @property { TOrientation } [orientation] - Orientation of the progress bar.
 * @property { TProgressVariant } [variant] - Progress bar variant.
 */
interface IProgressBarProps {
  minWidth?: number;
  percentage: number;
  percentageLocation?: TPercentageLocation;
  rounded?: boolean;
  showPercentage?: boolean;
  orientation?: TOrientation;
  variant?: TProgressVariant;
}

export type { IProgressBarProps, TProgressVariant, TOrientation };
