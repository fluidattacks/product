import type { DefaultTheme } from "styled-components";
import { styled } from "styled-components";

import type { TOrientation, TProgressVariant } from "./types";

import { variantBuilder } from "components/@core";

const { getVariant } = variantBuilder(
  (theme: DefaultTheme): Record<TProgressVariant, string> => ({
    compliance: `
      display: inline-flex;

      &.horizontal {
        height: ${theme.spacing[0.75]};
      }
      &.vertical {
        width: ${theme.spacing[0.75]};
      }
    `,
    default: `
      display: inline-flex;

      &.horizontal {
        height: ${theme.spacing[0.5]};
      }
      &.vertical {
        width: ${theme.spacing[0.5]};
      }
      .progress-bar {
        background-color: ${theme.palette.primary[500]};
      }
    `,
    progressIndicator: `
      display: flex;

      &.horizontal {
        height: ${theme.spacing[0.125]};
      }
      &.vertical {
        width: ${theme.spacing[0.125]};
      }
      .progress-bar {
        background-color: ${theme.palette.primary[500]};
      }
    `,
    progressIndicatorError: `
      display: flex;

      &.horizontal {
        height: ${theme.spacing[0.125]};
      }
      &.vertical {
        width: ${theme.spacing[0.125]};
      }
      .progress-bar {
        background-color: ${theme.palette.error[200]};
      }
    `,
    small: `
      color: ${theme.palette.primary[500]};
      display: inline-flex;

      &.horizontal {
        height: ${theme.spacing[0.25]};
      }
      &.vertical {
        width: ${theme.spacing[0.25]};
      }
      .progress-bar {
        background-color: ${theme.palette.primary[500]};
      }
    `,
  }),
);

const getComplianceColor = (
  theme: DefaultTheme,
  percentage: number,
): string => {
  if (percentage < 50) return theme.palette.error[500];
  if (percentage < 80) return theme.palette.warning[500];
  return theme.palette.success[500];
};

const Bar = styled.div<{
  $minWidth: number;
  $orientation: TOrientation;
  $rounded: boolean;
  $variant: TProgressVariant;
}>`
  ${({ theme, $minWidth, $orientation, $rounded, $variant }): string => `
    background-color: ${theme.palette.gray[100]};
    border-radius: ${$rounded ? "100px" : "unset"};
    ${$orientation === "horizontal" ? "min-width" : "min-height"}: ${$minWidth}px;
    ${$orientation === "horizontal" ? "" : "height: 100%;"}

    ${getVariant(theme, $variant)}

  `}
`;

const StyledProgressBar = styled.div<{
  $percentage: number;
  $rounded: boolean;
  $variant: TProgressVariant;
}>`
  ${({ theme, $percentage, $rounded, $variant }): string => `
    display: inline-flex;
    width: ${$percentage}%;
    max-width: 100%;
    min-width: 0%;
    height: 100%;
    border-radius: ${$rounded ? "100px" : "unset"};
    border-end-end-radius: ${$rounded && $percentage < 100 ? "0" : "inherit"};
    border-start-end-radius: ${$rounded && $percentage < 100 ? "0" : "inherit"};
    background-color: ${
      $variant === "compliance"
        ? getComplianceColor(theme, $percentage)
        : "inherit"
    };
  `}
`;

export { Bar, StyledProgressBar };
