import { styled } from "styled-components";

const FloatButton = styled.div.attrs({
  className: "comp-scroll-up",
})`
  bottom: 15px;
  position: fixed;
  right: 140px;
  z-index: 100;
`;

export { FloatButton };
