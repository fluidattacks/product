/**
 * Scroll up button component props.
 * @interface IScrollUpButtonProps
 * @property { string } [scrollerId] - The ID of the scrollable element.
 * @property { number } [visibleAt] - The pixel value at which the scroll up button should become visible.
 */
interface IScrollUpButtonProps {
  scrollerId?: string;
  visibleAt?: number;
}

export type { IScrollUpButtonProps };
