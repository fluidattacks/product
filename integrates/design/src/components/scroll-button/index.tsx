import type { FC } from "react";
import { useCallback, useEffect, useState } from "react";

import { FloatButton } from "./styles";
import { IScrollUpButtonProps } from "./types";

import { IconButton } from "components/icon-button";

const ScrollUpButton: FC<IScrollUpButtonProps> = ({
  scrollerId = "dashboard",
  visibleAt = 500,
}: Readonly<IScrollUpButtonProps>): JSX.Element | null => {
  const [visible, setVisible] = useState(false);
  const [scroller, setScroller] = useState<HTMLElement | null>(null);

  useEffect((): void => {
    setScroller(document.getElementById(scrollerId));
    scroller?.addEventListener("scroll", (): void => {
      setVisible(scroller.scrollTop > visibleAt);
    });
  }, [scroller, scrollerId, visibleAt]);

  const goToTop = useCallback((): void => {
    scroller?.scrollTo({
      behavior: "smooth",
      top: 0,
    });
  }, [scroller]);

  return visible ? (
    <FloatButton>
      <IconButton
        height={"40px"}
        icon={"arrow-up"}
        iconSize={"sm"}
        id={"scroll-up"}
        onClick={goToTop}
        width={"40px"}
      />
    </FloatButton>
  ) : null;
};

export { ScrollUpButton };
