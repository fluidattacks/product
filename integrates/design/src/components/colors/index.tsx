import type { IconName, IconPrefix } from "@fortawesome/free-solid-svg-icons";
import React from "react";
import { type DefaultTheme, ThemeProvider } from "styled-components";

const gray = {
  "25": "#fcfcfd",
  "50": "#f9fafb",
  "100": "#f2f4f7",
  "200": "#eaecf0",
  "300": "#d0d5dd",
  "400": "#98a2b3",
  "500": "#667085",
  "600": "#475467",
  "700": "#2f394b",
  "800": "#212a36",
  "900": "#161b25",
};

const primary = {
  "25": "#fef2f3",
  "50": "#fddfe2",
  "100": "#fbbac0",
  "200": "#f9959e",
  "300": "#f65e6a",
  "400": "#f32637",
  "500": "#bf0b1a", // Brand color
  "600": "#ac0a17",
  "700": "#9a0915",
  "800": "#870812",
  "900": "#750710",
};

const complementary = {
  "25": "",
  "50": "",
  "100": "#ffe6ff",
  "200": "",
  "300": "#b8075d",
  "400": "",
  "500": "#7f0540",
  "600": "",
  "700": "#59042d",
  "800": "",
  "900": "#460323",
};

const error = {
  "25": "",
  "50": "#fef3f2",
  "100": "",
  "200": "#fecdca",
  "300": "",
  "400": "",
  "500": "#f04438",
  "600": "",
  "700": "#b42318",
  "800": "",
  "900": "",
};

const warning = {
  "25": "",
  "50": "#fffaeb",
  "100": "",
  "200": "#fef0c7",
  "300": "",
  "400": "#ffd562",
  "500": "#fdb022",
  "600": "#dc6803",
  "700": "#b54708",
  "800": "",
  "900": "",
};

const success = {
  "25": "",
  "50": "#f6fef9",
  "100": "",
  "200": "#a6f4c5",
  "300": "",
  "400": "",
  "500": "#12b76a",
  "600": "",
  "700": "#027a48",
  "800": "",
  "900": "",
};

const info = {
  "25": "",
  "50": "#eff8ff",
  "100": "",
  "200": "#b2ddff",
  "300": "",
  "400": "",
  "500": "#2e90fa",
  "600": "",
  "700": "#175cd3",
  "800": "",
  "900": "",
};

const gradients = {
  "01": "linear-gradient(45deg, #f32637 0%, #b8075d 100%)",
  "02": "linear-gradient(45deg, #fcfcfd 0%, #667085 100%)",
};

const typography: DefaultTheme["typography"] = {
  type: {
    primary: "Roboto, sans-serif",
    poppins: "'Poppins', sans-serif",
    mono: "'Space Mono', monospace",
  },
  heading: {
    xxl: "64px",
    xl: "48px",
    lg: "32px",
    md: "24px",
    sm: "20px",
    xs: "16px",
  },
  text: {
    xl: "20px",
    lg: "18px",
    md: "16px",
    sm: "14px",
    xs: "12px",
  },
  weight: {
    bold: "700",
    semibold: "600",
    regular: "400",
  },
};

const shadows: DefaultTheme["shadows"] = {
  none: "none",
  sm: "0px 1px 2px 0px rgba(16, 24, 40, 0.15)",
  md: "0px 4px 6px 0px rgba(16, 24, 40, 0.15)",
  lg: "0px 8px 24px 0px rgba(16, 24, 40, 0.15)",
};

const breakpoints: DefaultTheme["breakpoints"] = {
  mobile: "480px",
  tablet: "960px",
  sm: "1024px",
  md: "1440px",
  lg: "1920px",
};

const spacing: DefaultTheme["spacing"] = {
  0: "0rem",
  0.125: "0.125rem",
  0.188: "0.188rem",
  0.25: "0.25rem",
  0.5: "0.5rem",
  0.625: "0.625rem",
  0.75: "0.75rem",
  1: "1rem",
  1.25: "1.25rem",
  1.5: "1.5rem",
  1.75: "1.75rem",
  2: "2rem",
  2.25: "2.25rem",
  2.5: "2.5rem",
  3: "3rem",
  3.5: "3.5rem",
  4: "4rem",
  4.5: "4.5rem",
  5: "5rem",
  6: "6rem",
};

const icons: IconName[] = [
  "lock-keyhole",
  "bars-sort",
  "bolt",
  "books",
  "brain-circuit",
  "briefcase",
  "bug",
  "buildings",
  "bullseye-pointer",
  "calendar-clock",
  "calendar-xmark",
  "chart-network",
  "clipboard-check",
  "comment",
  "envelope",
  "eye",
  "file",
  "flag",
  "globe",
  "hand-holding-dollar",
  "hands-clapping",
  "headset",
  "hexagon-exclamation",
  "light-emergency-on",
  "magnifying-glass",
  "pen-line",
  "plane-departure",
  "screwdriver-wrench",
  "sensor-triangle-exclamation",
  "shield-check",
  "sidebar",
  "skull",
  "sliders",
  "star-christmas",
  "telescope",
  "trash",
  "unlock",
  "user-group",
  "user-hoodie",
  "xmark",
];

const uniqueIcons: [IconPrefix, IconName][] = [
  ["fal", "arrow-down"],
  ["fal", "arrow-left"],
  ["fal", "arrow-right"],
  ["fal", "arrow-up"],
  ["fal", "plus"],
  ["fal", "minus"],
  ["fas", "check"],
  ["fas", "circle-question"],
];

const theme: DefaultTheme = {
  typography,
  shadows,
  breakpoints,
  spacing,
  palette: {
    primary,
    complementary,
    error,
    info,
    warning,
    success,
    black: "#0c111d",
    white: "#ffffff",
    gray,
    gradients,
  },
};

const CustomThemeProvider = ({
  children,
}: Readonly<{
  children: React.ReactNode;
}>): JSX.Element => <ThemeProvider theme={theme}>{children}</ThemeProvider>;

export { CustomThemeProvider, theme, icons, uniqueIcons };
