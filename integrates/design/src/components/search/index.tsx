import { useFloating } from "@floating-ui/react-dom";
import { isEmpty } from "lodash";
import { Fragment, Ref, forwardRef, useCallback, useState } from "react";
import { useTheme } from "styled-components";

import { OutlineContainer, OutlineDropdown, StyledInput } from "./styles";
import type { ISearchProps } from "./types";

import { Icon } from "components/icon";
import { IconButton } from "components/icon-button";
import { useClickOutside } from "hooks/use-click-outside";

const Search = forwardRef(function Search(
  {
    name,
    smallSearch = false,
    onClear,
    value,
    ...props
  }: Readonly<ISearchProps>,
  ref: Ref<HTMLInputElement>,
): JSX.Element {
  const theme = useTheme();
  const [isOpen, setIsOpen] = useState(false);
  const { refs, floatingStyles } = useFloating();

  const openSearchBar = useCallback((): void => {
    setIsOpen((previous): boolean => !previous);
  }, []);

  const onKeyDown = useCallback(
    (event: Readonly<React.KeyboardEvent<HTMLInputElement>>): void => {
      if (event.key === "Enter") {
        event.preventDefault();
      }
    },
    [],
  );

  useClickOutside(
    refs.floating.current as HTMLElement,
    (): void => {
      setIsOpen(false);
    },
    true,
  );

  const searchBar = (
    <OutlineContainer>
      <Icon
        icon={"magnifying-glass"}
        iconColor={theme.palette.gray[700]}
        iconSize={"xs"}
        iconType={"fa-light"}
      />
      <StyledInput
        {...props}
        id={name}
        name={name}
        onKeyDown={onKeyDown}
        ref={ref}
        type={"search"}
        value={value}
      />
      {!isEmpty(value) && (
        <IconButton
          icon={"xmark"}
          iconColor={theme.palette.gray[300]}
          iconSize={"xs"}
          iconType={"fa-light"}
          onClick={onClear}
          px={0.25}
          py={0.25}
          type={"reset"}
          variant={"ghost"}
        />
      )}
    </OutlineContainer>
  );

  return smallSearch ? (
    <Fragment>
      <IconButton
        border={`1px solid ${theme.palette.gray[300]} !important`}
        icon={"magnifying-glass"}
        iconSize={"xs"}
        iconType={"fa-light"}
        onClick={openSearchBar}
        ref={refs.setReference}
        variant={"ghost"}
      />
      {isOpen ? (
        <OutlineDropdown ref={refs.setFloating} style={floatingStyles}>
          {searchBar}
        </OutlineDropdown>
      ) : undefined}
    </Fragment>
  ) : (
    searchBar
  );
});

export { Search };
