import { useTheme } from "styled-components";

import { Circle, CircleBg, LoadingContainer } from "./styles";
import type { ILoadingProps } from "./types";

import { Container } from "components/container";
import { Text } from "components/typography";

const Loading = ({
  color = "red",
  label,
  size,
}: Readonly<ILoadingProps>): JSX.Element => {
  const theme = useTheme();

  return (
    <Container alignItems={"center"} display={"flex"}>
      <LoadingContainer $size={size} viewBox={"0 0 100 100"}>
        <CircleBg $color={color} cx={"50"} cy={"50"} r={"45"} />
        <Circle $color={color} cx={"50"} cy={"50"} r={"45"} />
      </LoadingContainer>
      {label && (
        <Text
          color={
            color === "red" ? theme.palette.gray[700] : theme.palette.white
          }
          ml={0.5}
          size={"sm"}
        >
          {label}
        </Text>
      )}
    </Container>
  );
};

export type { ILoadingProps };
export { Loading };
