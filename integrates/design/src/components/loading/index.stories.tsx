import type { Meta, StoryFn } from "@storybook/react";

import type { ILoadingProps } from ".";
import { Loading } from ".";
import { Container } from "../container";

const config: Meta = {
  component: Loading,
  tags: ["autodocs"],
  title: "Components/Loading",
};

const Template: StoryFn<ILoadingProps> = (props): JSX.Element => (
  <Loading {...props} />
);

const Default = Template.bind({});
Default.args = {
  color: "red",
  label: "Loading...",
  size: "lg",
};

const DarkBackground: StoryFn = (): JSX.Element => (
  <Container
    bgColor={"#212a36"}
    borderRadius={"4px"}
    center={true}
    pb={1.5}
    pl={1.5}
    pr={1.5}
    pt={1.5}
    width={"500px"}
  >
    <Loading color={"white"} label={"Loading..."} size={"lg"} />
  </Container>
);

export { Default, DarkBackground };
export default config;
