import type { DefaultTheme, RuleSet } from "styled-components";
import { css, keyframes, styled } from "styled-components";

import type { TColor, TSize } from "./types";

import { variantBuilder } from "components/@core";

interface IStyledLoadingProps {
  $color?: TColor;
  $size?: TSize;
}

const sizes: Record<TSize, string> = {
  lg: "56",
  md: "30",
  sm: "12",
};

const { getVariant } = variantBuilder(
  (theme: DefaultTheme): Record<TColor, string> => ({
    red: `stroke: ${theme.palette.primary[500]};`,
    white: `stroke: ${theme.palette.white};`,
  }),
);

const svgAnim = keyframes`
  0% {
    transform: rotateZ(0deg);
  }
  100% {
    transform: rotateZ(360deg)
  }
`;

const dash = keyframes`
  0%,
  25% {
    stroke-dashoffset: 280;
    transform: rotate(0);
  }

  50%,
  75% {
    stroke-dashoffset: 75;
    transform: rotate(45deg);
  }

  100% {
    stroke-dashoffset: 280;
    transform: rotate(360deg);
  }
`;

const LoadingContainer = styled.svg<IStyledLoadingProps>`
  ${({ $size = "md" }): RuleSet<object> => css`
    animation: ${svgAnim} 2s linear infinite;
    animation-timing-function: linear;
    display: inline-block;
    width: ${sizes[$size]}px;
  `}
`;

const Circle = styled.circle<IStyledLoadingProps>`
  ${({ theme, $color = "red" }): RuleSet<object> => css`
    animation: 1.4s ease-in-out infinite both ${dash};
    display: block;
    fill: transparent;
    stroke-linecap: round;
    stroke-dasharray: 283;
    stroke-dashoffset: 280;
    stroke-width: 10px;
    transform-origin: 50% 50%;

    ${getVariant(theme, $color)}
  `}
`;

const CircleBg = styled.circle<IStyledLoadingProps>`
  display: block;
  fill: transparent;
  stroke: ${({ theme }): string => theme.palette.gray[200]};
  stroke-linecap: round;
  stroke-width: 10px;
  transform-origin: 50% 50%;
  opacity: ${({ $color }): string => ($color === "white" ? "20%" : "unset")};
`;

export { Circle, CircleBg, LoadingContainer };
