import type { Meta, StoryFn } from "@storybook/react";
import { Fragment } from "react";

import {
  DebugContainer,
  Grid,
  GridContent,
  GridSidebar,
  StyledGrid,
} from "./styles";

const config: Meta = {
  component: Grid,
  globals: {
    showToolbar: false,
  },
  title: "Essentials/Layout",
};

const PlatformLayout = (): JSX.Element => (
  <Grid>
    <GridSidebar>
      <DebugContainer>{"Sidebar"}</DebugContainer>
    </GridSidebar>
    <GridContent>
      <DebugContainer>{"1"}</DebugContainer>
      <DebugContainer>{"2"}</DebugContainer>
      <DebugContainer>{"3"}</DebugContainer>
      <DebugContainer>{"4"}</DebugContainer>
      <DebugContainer>{"5"}</DebugContainer>
      <DebugContainer>{"6"}</DebugContainer>
      <DebugContainer>{"7"}</DebugContainer>
      <DebugContainer>{"8"}</DebugContainer>
      <DebugContainer>{"9"}</DebugContainer>
      <DebugContainer>{"10"}</DebugContainer>
      <DebugContainer>{"11"}</DebugContainer>
      <DebugContainer>{"12"}</DebugContainer>
    </GridContent>
  </Grid>
);

const WebsiteLayout: StoryFn<{ columns: number }> = (
  props: Readonly<{
    columns: number;
  }>,
): JSX.Element => (
  <div className={`px-12 w-${props.columns} h-[500px]`}>
    <StyledGrid $columns={props.columns}>
      <DebugContainer>{"1"}</DebugContainer>
      <DebugContainer>{"2"}</DebugContainer>
      <DebugContainer>{"3"}</DebugContainer>
      <DebugContainer>{"4"}</DebugContainer>
      {props.columns > 4 ? (
        <Fragment>
          <DebugContainer>{"5"}</DebugContainer>
          <DebugContainer>{"6"}</DebugContainer>
        </Fragment>
      ) : undefined}
      {props.columns > 6 ? (
        <Fragment>
          <DebugContainer>{"7"}</DebugContainer>
          <DebugContainer>{"8"}</DebugContainer>
        </Fragment>
      ) : undefined}
      {props.columns > 8 ? (
        <Fragment>
          <DebugContainer>{"9"}</DebugContainer>
          <DebugContainer>{"10"}</DebugContainer>
          <DebugContainer>{"11"}</DebugContainer>
          <DebugContainer>{"12"}</DebugContainer>
        </Fragment>
      ) : undefined}
    </StyledGrid>
  </div>
);

const Large = WebsiteLayout.bind({});
Large.parameters = {
  viewport: { defaultViewport: "lg" },
};
Large.args = {
  columns: 12,
};

const Medium = WebsiteLayout.bind({});
Medium.parameters = {
  viewport: { defaultViewport: "md" },
};
Medium.args = {
  columns: 8,
};

const Tablet = WebsiteLayout.bind({});
Tablet.parameters = {
  viewport: { defaultViewport: "tablet" },
};
Tablet.args = {
  columns: 6,
};

export { PlatformLayout, Large, Medium, Tablet };
export default config;
