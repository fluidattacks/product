import { styled } from "styled-components";

const Li = styled.li`
  ${({ theme }): string => `
    align-items: center;
    background-color: ${theme.palette.white};
    color: ${theme.palette.gray[800]};
    cursor: pointer;
    display: flex;
    font-size: ${theme.typography.text["sm"]};
    font-weight: ${theme.typography.weight.regular};
    justify-content: space-between;
    gap: ${theme.spacing[0.625]};
    line-height: ${theme.spacing[1.25]};
    list-style-type: none;
    min-height: 40px;
    min-width: 240px;
    padding: ${theme.spacing[0.625]} ${theme.spacing[1]};

    p,
    a {
      word-break: break-word;
    }

    & > label {
      margin: unset;
    }

    a {
      color: ${theme.palette.gray[500]};
    }

    &[aria-disabled="true"] {
      color: ${theme.palette.gray[300]};
      cursor: not-allowed;

      a {
        color: ${theme.palette.gray[300]};
      }
    }

    &[aria-selected="true"] {
      background-color: ${theme.palette.gray[200]};
    }

    &:hover:not([aria-selected="true"]) {
      background-color: ${theme.palette.gray[100]};
    }
  `}
`;

const ListItemsWrapper = styled.ul`
  ${({ theme }): string => `
    background-color: ${theme.palette.white};
    border: 1px solid ${theme.palette.gray[200]};
    border-radius: ${theme.spacing[0.25]};
    box-shadow: ${theme.shadows.md};
    display: inline-block;
    margin: unset;
    min-width: 240px;
    padding: ${theme.spacing[0.25]} 0 ${theme.spacing[0.25]} 0;
  `}
`;

export { Li, ListItemsWrapper };
