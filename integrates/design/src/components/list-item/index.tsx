import { Children, cloneElement, isValidElement } from "react";
import type { ReactNode } from "react";
import { useTheme } from "styled-components";

import { Li, ListItemsWrapper } from "./styles";
import type { IListItemProps } from "./types";

import { Container } from "components/container";
import { Icon } from "components/icon";
import { Link } from "components/link";

const ListItem = ({
  children,
  disabled = false,
  icon,
  iconType = "fa-light",
  href,
  onClick,
  onClickIcon,
  onKeyDown,
  rightIcon,
  selected = false,
  value,
  ...props
}: Readonly<IListItemProps>): JSX.Element => {
  const theme = useTheme();
  const textColor = theme.palette.gray[disabled ? 300 : 800];
  const childrenWithProps = disabled
    ? Children.map(children, (child): ReactNode => {
        if (isValidElement(child)) {
          return cloneElement(child, {
            disabled,
            ...child.props,
          });
        }

        return child;
      })
    : children;

  return (
    <Li
      aria-disabled={disabled}
      aria-label={value}
      aria-selected={selected}
      data-testid={value}
      onClick={onClick}
      onKeyDown={onKeyDown}
      value={value}
      {...props}
    >
      {icon !== undefined || value !== undefined ? (
        <Container alignItems={"center"} display={"flex"} gap={0.625}>
          {icon ? (
            <Icon
              disabled={disabled}
              icon={icon}
              iconColor={textColor}
              iconSize={"xxs"}
              iconType={iconType}
            />
          ) : null}
          {href === undefined ? (
            childrenWithProps
          ) : (
            <Link href={href}>{childrenWithProps}</Link>
          )}
        </Container>
      ) : null}
      {selected || rightIcon ? (
        <Icon
          disabled={disabled}
          icon={rightIcon ?? "check"}
          iconClass={"selected"}
          iconColor={textColor}
          iconSize={"xs"}
          iconType={iconType}
          onClick={onClickIcon}
        />
      ) : null}
    </Li>
  );
};

export { ListItem, ListItemsWrapper };
