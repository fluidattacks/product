import styled from "styled-components";

import { ListItem } from "components/list-item";

const StyledList = styled(ListItem)`
  padding: 8px;
  min-width: unset;
`;

export { StyledList };
