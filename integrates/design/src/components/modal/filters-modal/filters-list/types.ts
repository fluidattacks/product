import type { Property } from "csstype";

import { IFilterOptions } from "../types";
import type { IListItemProps } from "components/list-item/types";

/**
 * Filters options properties.
 * @interface IOptionsProps
 * @property { IconsProps["icon"] } [icon] - The icon to display.
 * @property { string } label - The label of the option.
 * @property { string } [group] - The group of the option.
 * @property { IFilterOptions[] } filterOptions - The available options to apply filter.
 */
interface IOptionsProps<IData extends object> {
  icon?: IListItemProps["icon"];
  label: string;
  group?: string;
  filterOptions: IFilterOptions<IData>[];
}

/**
 * Options filters modal properties.
 * @interface IFiltersListProps
 * @property { Function } onApplyFilters - Function to apply filters.
 * @property { IOptionsProps[] } options - The options to display.
 * @property { "grouped" | "ungrouped" } variant - The variant of the options.
 */
interface IFiltersListProps<IData extends object> {
  options: IOptionsProps<IData>[];
  setDirection?: React.Dispatch<React.SetStateAction<Property.FlexDirection>>;
  setHandler?: React.Dispatch<React.SetStateAction<boolean>>;
  variant: "grouped" | "ungrouped";
  onApplyFilters: (filters: ReadonlyArray<IFilterOptions<IData>>) => void;
}

export type { IFiltersListProps, IOptionsProps };
