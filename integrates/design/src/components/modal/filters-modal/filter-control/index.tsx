import type { IFilterControlProps } from "./types";

import { StyledList } from "../styles";
import { Checkbox } from "components/checkbox";
import {
  ComboBox,
  Input,
  InputDateRange,
  InputNumberRange,
} from "components/inputs";
import { RadioButton } from "components/radio-button";

const FilterControl = <IData extends object>({
  control,
  option,
  register,
  setValue,
}: Readonly<IFilterControlProps<IData>>): JSX.Element => {
  switch (option.type) {
    case "checkboxes":
      return (
        <div
          style={{ overflowX: "hidden", overflowY: "auto", maxHeight: "280px" }}
        >
          {option.options?.map(
            (option): JSX.Element => (
              <StyledList
                key={`li-checkbox-${option.value}`}
                value={option.value}
                width={"content"}
              >
                <Checkbox
                  {...register("checkbox")}
                  label={option.label}
                  value={option.value}
                />
              </StyledList>
            ),
          )}
        </div>
      );
    case "radioButton":
      return (
        <div
          style={{ overflowX: "hidden", overflowY: "auto", maxHeight: "280px" }}
        >
          {option.options?.map(
            (selectedOption): JSX.Element => (
              <StyledList
                key={`li-radio-${selectedOption.value}`}
                value={selectedOption.value}
                width={"content"}
              >
                <RadioButton
                  {...register("radio")}
                  label={selectedOption.label}
                  value={selectedOption.value}
                />
              </StyledList>
            ),
          )}
        </div>
      );
    case "select":
      return (
        <ComboBox
          control={control}
          items={
            option.options?.map((opt) => ({
              name: opt.label ?? "",
              value: opt.value,
            })) ?? []
          }
          label={option.label}
          name={`text.${String(option.key)}-${option.filterFn ?? "caseInsensitive"}`}
          value={option.value}
        />
      );
    case "text":
      const fieldName = `text.${String(option.key)}-${option.filterFn ?? "caseInsensitive"}`;
      return (
        <Input
          label={option.label}
          name={fieldName}
          register={register}
          value={option.value}
        />
      );
    case "numberRange":
      return (
        <InputNumberRange
          label={option.label}
          name={String(option.key)}
          propsMax={{
            placeholder: "Max",
            register: register,
            name: "maxValue",
            value: option.maxValue,
            setValue: setValue,
          }}
          propsMin={{
            placeholder: "Min",
            register: register,
            name: "minValue",
            value: option.minValue,
            setValue: setValue,
          }}
          variant={"filters"}
        />
      );
    case "dateRange":
      return (
        <InputDateRange
          label={option.label}
          name={String(option.key)}
          propsMax={{
            name: "maxValue",
            value: option.maxValue?.toString(),
          }}
          propsMin={{
            name: "minValue",
            value: option.minValue?.toString(),
          }}
          register={register}
          setValue={setValue}
          variant={"filters"}
        />
      );
    default:
      return <div />;
  }
};

export default FilterControl;
