import { useCallback, useEffect } from "react";

import { FilterFormContent } from "./form-content";
import { useFilterForm } from "./hooks";
import { IFormFieldVales } from "./types";
import { filterCheckboxOptions, updateFormValues } from "./utils";

import { IOptionsSelectorProps } from "../types";
import { Button } from "components/button";
import { Search } from "components/search";

const FilterForm = <IData extends object>({
  options,
  onSubmit,
  id,
}: Readonly<IOptionsSelectorProps<IData>>): JSX.Element => {
  const { control, items, setItems, register, handleSubmit, setValue } =
    useFilterForm(options);

  const hasCheckboxes = options.some((option) => option.type === "checkboxes");

  const onSearchHandler = useCallback(
    (event: Readonly<React.ChangeEvent<HTMLInputElement>>): void => {
      const { value } = event.target;
      const filteredCheckboxOptions = filterCheckboxOptions(options, value);
      setItems(filteredCheckboxOptions);
    },
    [options, setItems],
  );

  const onSubmitHandler = useCallback(
    (data: Readonly<IFormFieldVales>): void => {
      if (onSubmit) {
        onSubmit(data);
      }
    },
    [onSubmit],
  );

  useEffect(() => {
    setItems(options);
    updateFormValues(options, setValue);
  }, [options, setItems, setValue]);

  return (
    <div className={"flex flex-col gap-[8px]"} id={id}>
      {hasCheckboxes && <Search onChange={onSearchHandler} />}
      <form id={id} onSubmit={handleSubmit(onSubmitHandler)}>
        <FilterFormContent
          control={control}
          items={items}
          register={register}
          setValue={setValue}
        />
        <Button mt={1} type={"submit"}>
          {"Apply"}
        </Button>
      </form>
    </div>
  );
};

export { FilterForm };
