import { Control, UseFormRegister, UseFormSetValue } from "react-hook-form";

import { IFilterOptions } from "../../types";
import { IFormFieldVales } from "../types";

/**
 * Interface representing props for the FilterFormContent component.
 * @interface IFilterFormContentProps
 * @template IData - Type extending object for filter data.
 * @property {IFilterOptions<IData>[]} items - Array of filter options.
 * @property {UseFormRegister<IFormFieldVales>} register - React Hook Form register function.
 * @property {UseFormSetValue<IFormFieldVales>} setValue - React Hook Form setValue function.
 */
interface IFilterFormContentProps<IData extends object> {
  control: Control<IFormFieldVales>;
  items: IFilterOptions<IData>[];
  register: UseFormRegister<IFormFieldVales>;
  setValue: UseFormSetValue<IFormFieldVales>;
}

export type { IFilterFormContentProps };
