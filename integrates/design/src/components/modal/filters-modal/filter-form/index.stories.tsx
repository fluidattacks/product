import type { Meta, StoryFn } from "@storybook/react";

import { FilterForm } from ".";
import type { IOptionsSelectorProps } from "../types";

const config: Meta = {
  component: FilterForm,
  tags: ["autodocs"],
  title: "Components/Modal/FilterForm",
};

const Template: StoryFn<Readonly<IOptionsSelectorProps<object>>> = (
  props,
): JSX.Element => {
  return (
    <div id={"portals"} style={{ minHeight: "100vh" }}>
      <FilterForm {...props} onSubmit={console.log} />
    </div>
  );
};
const Default = Template.bind({});
Default.args = {
  options: [
    {
      label: "Type",
      type: "checkboxes",
      value: "Type",
      key: (item: object): boolean =>
        (item as { type?: string }).type === "Type",
      options: [
        {
          checked: false,
          value: "Authorization for a special attack",
        },
        {
          checked: true,
          value: "Cloning issues",
        },
        {
          checked: false,
          value: "Credentials issues",
        },
        {
          checked: false,
          value: "Environment issues",
        },
        {
          checked: false,
          value: "Installer issues",
        },
        {
          checked: false,
          value: "Missing supplies",
        },
        {
          checked: false,
          value: "Network access issues",
        },
      ],
    },
  ],
};
export default config;
export { Default };
