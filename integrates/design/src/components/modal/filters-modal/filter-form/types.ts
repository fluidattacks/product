/**
 * Interface representing form field values.
 * @interface IFormFieldValues
 * @property {string[]} checkbox - Array of selected checkbox values.
 * @property {string} radio - Selected radio button value.
 * @property {string} text - Text input value.
 */
interface IFormFieldVales {
  checkbox: string[];
  radio: string;
  text?: Record<string, string>;
  minValue?: number | string;
  maxValue?: number | string;
}

export type { IFormFieldVales };
