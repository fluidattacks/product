import { UseFormSetValue } from "react-hook-form";

import { IFormFieldVales } from "./types";

import { IFilterOptions } from "../types";

const updateFormValues = <IData extends object>(
  options: ReadonlyArray<IFilterOptions<IData>>,
  setValue: UseFormSetValue<IFormFieldVales>,
): void => {
  setValue(
    "checkbox",
    options
      .filter((option) => option.type === "checkboxes")
      .flatMap(
        (option) =>
          option.options
            ?.filter((opt) => opt.checked)
            .map((opt) => opt.value) ?? [],
      ),
  );

  setValue(
    "radio",
    options
      .find((option) => option.type === "radioButton")
      ?.options?.find(({ checked }) => checked)?.value ?? "",
  );

  setValue(
    "text",
    options
      .filter((option) => ["text", "select"].includes(option.type))
      .reduce(
        (acc, option) => ({
          ...acc,
          [`${String(option.key)}-${option.filterFn ?? "caseInsensitive"}`]:
            option.value ?? "",
        }),
        {},
      ),
  );

  setValue(
    "minValue",
    options.find((option) => ["numberRange", "dateRange"].includes(option.type))
      ?.minValue,
  );

  setValue(
    "maxValue",
    options.find((option) => ["numberRange", "dateRange"].includes(option.type))
      ?.maxValue,
  );
};

const filterCheckboxOptions = <IData extends object>(
  options: ReadonlyArray<IFilterOptions<IData>>,
  value: string,
): IFilterOptions<IData>[] =>
  options.map((option) => {
    if (option.type === "checkboxes") {
      return {
        ...option,
        options: option.options?.filter(
          (opt) =>
            opt.label?.toLowerCase().includes(value.toLowerCase()) ??
            opt.value.toLowerCase().includes(value.toLowerCase()),
        ),
      };
    }
    return option;
  });

export { filterCheckboxOptions, updateFormValues };
