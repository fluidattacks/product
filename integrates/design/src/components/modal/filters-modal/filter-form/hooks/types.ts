import {
  Control,
  UseFormHandleSubmit,
  UseFormRegister,
  UseFormSetValue,
} from "react-hook-form";

import { IFilterOptions } from "../../types";
import { IFormFieldVales } from "../types";

/**
 * Interface representing props for the useFilterForm hook.
 * @interface IUseFilterForm
 * @template IData - Type extending object for filter data.
 * @property {IFilterOptions<IData>[]} items - Array of filter options.
 * @property {React.Dispatch<React.SetStateAction<IFilterOptions<IData>[]>>} setItems - Function to set filter options.
 * @property {UseFormRegister<IFormFieldVales>} register - React Hook Form register function.
 * @property {UseFormHandleSubmit<IFormFieldVales>} handleSubmit - React Hook Form handleSubmit function.
 * @property {UseFormSetValue<IFormFieldVales>} setValue - React Hook Form setValue function.
 */
interface IUseFilterForm<IData extends object> {
  control: Control<IFormFieldVales>;
  items: IFilterOptions<IData>[];
  setItems: React.Dispatch<React.SetStateAction<IFilterOptions<IData>[]>>;
  register: UseFormRegister<IFormFieldVales>;
  handleSubmit: UseFormHandleSubmit<IFormFieldVales>;
  setValue: UseFormSetValue<IFormFieldVales>;
}

export type { IUseFilterForm };
