import dayjs, { extend } from "dayjs";
import localizedFormat from "dayjs/plugin/localizedFormat";
import _ from "lodash";

import type { IOptions } from "../types";

function formatValue(value?: string): string | undefined {
  if (!value) {
    return undefined;
  }

  return value;
}

function formatCheckValues(
  options?: ReadonlyArray<IOptions>,
): string | undefined {
  if (!options || options.length === 0) {
    return undefined;
  }

  const selectedOptions = options
    .filter((option) => option.checked)
    .map((option) => option.label ?? option.value);

  if (selectedOptions.length === 0) return undefined;

  const formattedCheckValues = selectedOptions.join(", ");

  return formattedCheckValues;
}

function formatNumberRange(
  rawMinValue?: number | string,
  rawMaxValue?: number | string,
): string | undefined {
  const minValue =
    _.isEmpty(rawMinValue) && typeof rawMinValue !== "number"
      ? undefined
      : Number(rawMinValue);
  const maxValue =
    _.isEmpty(rawMaxValue) && typeof rawMaxValue !== "number"
      ? undefined
      : Number(String(rawMaxValue));

  if (!_.isUndefined(minValue) && !_.isUndefined(maxValue)) {
    return `${minValue} - ${maxValue}`;
  }
  if (!_.isUndefined(minValue)) {
    return `Min ${minValue}`;
  }
  if (!_.isUndefined(maxValue)) {
    return `Max ${maxValue}`;
  }

  return undefined;
}

function formatDateRange(
  minValue?: string,
  maxValue?: string,
): string | undefined {
  extend(localizedFormat);
  if (_.isUndefined(minValue) || _.isUndefined(maxValue)) {
    return undefined;
  }
  if (minValue !== "" && maxValue !== "") {
    return `${dayjs(minValue).format("LL")} - ${dayjs(maxValue).format("LL")}`;
  }
  if (minValue !== "") {
    return `From ${dayjs(minValue).format("LL")}`;
  }
  if (maxValue !== "") {
    return `To ${dayjs(maxValue).format("LL")}`;
  }

  return undefined;
}

function formatLabel(
  type: string,
  label: string,
  filterFn?: "caseInsensitive" | "includesInsensitive",
): string {
  if (type === "select") return label;

  const filterFnType = filterFn === "includesInsensitive" ? "contains" : "is";
  return `${label} ${filterFnType}`;
}

export {
  formatCheckValues,
  formatDateRange,
  formatLabel,
  formatNumberRange,
  formatValue,
};
