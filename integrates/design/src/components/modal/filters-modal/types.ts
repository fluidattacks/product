import React from "react";

import { IFormFieldVales } from "./filter-form/types";
import type { IFiltersListProps, IOptionsProps } from "./filters-list/types";

type TInputType =
  | "checkboxes"
  | "dateRange"
  | "numberRange"
  | "radioButton"
  | "select"
  | "text";

/**
 * Selector properties.
 * @interface ISelector
 * @property {boolean} [checked] - Whether the selector is checked.
 * @property {string} value - The value of the selector.
 * @property {string} [label] - The label of the selector.
 */
interface IOptions {
  checked?: boolean;
  value: string;
  label?: string;
}

/**
 * Filter options interface.
 * @interface IFilterOptions
 * @property {boolean} [isBackFilter] - Whether the filter is a back filter.
 * @property {TInputType} type - The type of input for the filter.
 * @property {string} value - The value of the filter option.
 * @property {string} [label] - Optional label for the filter option.
 * @property {IOptions[]} [options] - Optional array of IOptions for the filter.
 */
interface IFilterOptions<IData extends object> {
  filterFn?: "caseInsensitive" | "includesInsensitive";
  isBackFilter?: boolean;
  type: TInputType;
  key:
    | keyof IData
    | ((
        arg0: IData,
        value?: string,
        rangeValues?: readonly [string, string],
        numberRangeValues?: readonly [number | undefined, number | undefined],
      ) => boolean);
  value?: string;
  label: string;
  options?: IOptions[];
  minValue?: number | string;
  maxValue?: number | string;
}

/**
 * Modal component props.
 * @interface IOptionsSelectorProps
 * @property {IFilterOptions[]} options - The properties of the options to display.
 * @property {(selectedOptions: Readonly<string[]>) => void} onSubmit - The function to call whit the selected options.
 * @property {string} [id] - The id of the form.
 * @property {boolean} open - Whether the modal is open.
 */
interface IOptionsSelectorProps<IData extends object> {
  options: IFilterOptions<IData>[];
  onSubmit?: (selectedOptions: Readonly<IFormFieldVales>) => void;
  id?: string;
}

/**
 * Interface for filter component with generic data type.
 * @interface IFilterComp
 * @template IData - The type of data in the dataset.
 * @extends {IFilterOptions<IData>}
 * @property {keyof IData} key - The key of the data object to filter on.
 */
interface IFilterComp<IData extends object> extends IFilterOptions<IData> {
  key: keyof IData;
}

/**
 * Props for the Filters component.
 * @interface IFiltersProps
 * @template IData - The type of data in the dataset.
 * @property {IData[]} [dataset] - Optional array of data items.
 * @property {string} key - The key of the data object to filter on.
 * @property {IOptionsProps[]} options - Array of options to filter by.
 * @property {React.Dispatch<React.SetStateAction<IData[]>>} setFilteredDataset - Function to set the filtered dataset.
 */
interface IFiltersProps<IData extends object> {
  dataset?: IData[];
  localStorageKey: string;
  options: IOptionsProps<IData>[];
  setFilteredDataset?: React.Dispatch<React.SetStateAction<IData[]>>;
  onFiltersChange?: (filters: ReadonlyArray<IFilterOptions<IData>>) => void;
  variant?: IFiltersListProps<IData>["variant"];
}

/**
 * Props for useFilter hook.
 */
interface IUseFilterProps<IData extends object> {
  options: IOptionsProps<IData>[];
  removeFilter: (
    filterToRemove: Readonly<IFilterOptions<IData>>,
  ) => VoidFunction;
  Filters: React.FC;
}

export type {
  IFilterComp,
  IFilterOptions,
  IFiltersProps,
  IOptions,
  IOptionsSelectorProps,
  IUseFilterProps,
};
