import styled from "styled-components";

import { ListItem } from "components/list-item";

const ModalWrapper = styled.div.attrs({
  className: "comp-modal absolute top-[55px] overflow-auto",
})<{ $bgColor?: string }>`
  align-items: center;
  background-color: ${({ $bgColor = "rgb(52 64 84 / 70%)" }): string =>
    $bgColor};
  display: flex;
  justify-content: center;
  z-index: 99999;
`;

const StyledList = styled(ListItem)`
  padding: 7px 8px;
  word-wrap: break-word;

  svg.fa-check {
    border-radius: 2px;
  }
`;

export { ModalWrapper, StyledList };
