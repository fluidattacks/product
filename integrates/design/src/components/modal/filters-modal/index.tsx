/* eslint-disable functional/functional-parameters */
import React, { useCallback, useEffect, useState } from "react";

import { AppliedFilters } from "./applied-filters";
import { formatCheckValues } from "./applied-filters/utils";
import { ColumFilters } from "./column-filters";
import {
  IFilterOptions,
  IFiltersProps,
  IOptions,
  IUseFilterProps,
} from "./types";
import { clearFilterValues, getActiveFilters, setFiltersUtil } from "./utils";

import { Button } from "components/button";
import { Container } from "components/container";
import { useModal } from "hooks";

const useFilters = <IData extends object>({
  dataset,
  localStorageKey,
  options,
  setFilteredDataset,
  onFiltersChange,
  variant = "ungrouped",
}: Readonly<IFiltersProps<IData>>): Readonly<IUseFilterProps<IData>> => {
  const modal = useModal("filters-modal");
  const [filters, setFilters] = useState<IFilterOptions<IData>[]>([]);
  const [activeFilters, setActiveFilters] = useState<number>(
    getActiveFilters(filters),
  );

  const mappedOptions = options.map((option) => ({
    ...option,
    filterOptions: option.filterOptions.map((filterOption) => {
      const savedFilter = filters.find(
        (filter) =>
          filter.key === filterOption.key &&
          filter.type === filterOption.type &&
          filter.filterFn === filterOption.filterFn,
      );
      return savedFilter ?? { ...filterOption };
    }),
  }));

  const handleApplyFilters = useCallback(
    (newFilters: ReadonlyArray<IFilterOptions<IData>>): void => {
      setFilters([...newFilters]);
      localStorage.setItem(localStorageKey, JSON.stringify(newFilters));
    },
    [setFilters, localStorageKey],
  );

  const handleRemoveFilter = useCallback(
    (filterToRemove: Readonly<IFilterOptions<IData>>): VoidFunction => {
      return (): void => {
        const updatedFilters = filters.map((filter) =>
          filter.key === filterToRemove.key &&
          filter.type === filterToRemove.type &&
          filter.filterFn === filterToRemove.filterFn
            ? clearFilterValues(filter)
            : filter,
        );
        setFilters(updatedFilters);
        localStorage.setItem(localStorageKey, JSON.stringify(updatedFilters));
      };
    },
    [filters, localStorageKey],
  );

  useEffect(() => {
    const savedFilters = localStorage.getItem(localStorageKey);
    const filterOpts = options.flatMap((option) => option.filterOptions);
    const currentFilters = savedFilters ? JSON.parse(savedFilters) : filterOpts;
    if (
      filterOpts.flatMap((option) => option.value).filter(Boolean).length > 0 ||
      filterOpts
        .flatMap((option) => option.options)
        .filter((option) => option?.checked === undefined).length > 0
    ) {
      setFilters(filterOpts);
      localStorage.setItem(localStorageKey, JSON.stringify(filterOpts));
    } else setFilters(currentFilters);
  }, [localStorageKey, options, setFilters]);

  useEffect((): void => {
    if (setFilteredDataset && dataset !== undefined)
      setFilteredDataset(setFiltersUtil(dataset, filters));
    setActiveFilters(getActiveFilters(filters));
    onFiltersChange?.(filters);
  }, [dataset, filters, setFilteredDataset, setActiveFilters, onFiltersChange]);

  const Filters: React.FC = (): React.ReactElement => {
    return (
      <Container
        alignItems={"center"}
        display={"flex"}
        gap={0.25}
        maxWidth={"100%"}
        position={"relative"}
        wrap={"wrap"}
      >
        <Container pr={0.25}>
          <Button
            icon={"filter"}
            id={"filterBtn"}
            onClick={modal.toggle}
            tag={activeFilters.toString()}
            variant={"ghost"}
          />
        </Container>
        {modal.isOpen && (
          <ColumFilters
            modalRef={modal}
            onApplyFilters={handleApplyFilters}
            options={mappedOptions}
            variant={variant}
          />
        )}
      </Container>
    );
  };

  return { Filters, options: mappedOptions, removeFilter: handleRemoveFilter };
};

export { formatCheckValues, useFilters, AppliedFilters };
export type { IFilterOptions, IUseFilterProps, IOptions };
