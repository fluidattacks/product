import _ from "lodash";

import type { IAppliedFilter } from "./types";

import { Tag } from "components/tag";

const AppliedFilter = <IData extends object>({
  filter,
  icon = "filter-list",
  label,
  onClose,
  filterValue,
}: Readonly<IAppliedFilter<IData>>): JSX.Element | null => {
  if (_.isNil(filterValue) || _.isEmpty(String(filterValue))) {
    return null;
  }

  return (
    <Tag
      filterValues={filterValue}
      icon={icon}
      id={"remove-filter"}
      onClose={onClose(filter)}
      tagLabel={`${label} `}
    />
  );
};
export { AppliedFilter };
