import _ from "lodash";

import { IFilterComp, IFilterOptions } from "./types";

const setFiltersUtil = <IData extends object>(
  data: Readonly<IData[]>,
  filters: Readonly<IFilterOptions<IData>[]>,
): IData[] => {
  function handleTextSelectCases(
    dataPoint: IData,
    filter: Readonly<IFilterComp<IData>>,
  ): boolean {
    if (
      filter.value === "" ||
      filter.value === undefined ||
      _.defaultTo(filter.isBackFilter, false)
    )
      return true;

    switch (filter.filterFn) {
      case "includesInsensitive":
        return String(dataPoint[filter.key])
          .toLowerCase()
          .includes(filter.value.toLowerCase());

      case "caseInsensitive":
      case undefined:
      default:
        return (
          String(dataPoint[filter.key]).toLowerCase() ===
          filter.value.toLowerCase()
        );
    }
  }

  function handleCheckboxSelectCases(
    dataPoint: IData,
    filter: Readonly<IFilterComp<IData>>,
  ): boolean {
    if (!filter.options || filter.options.length === 0) return true;

    const selectedOptions = filter.options
      .filter((option) => option.checked)
      .map((option) => option.value);

    if (selectedOptions.length === 0) return true;

    return selectedOptions.includes(String(dataPoint[filter.key]));
  }

  function handleRangeCases(
    dataPoint: IData,
    filter: Readonly<IFilterComp<IData>>,
    parseValue: (value: string) => number | Date,
  ): boolean {
    const value = parseValue(String(dataPoint[filter.key]));
    const { minValue: rawMinValue, maxValue: rawMaxValue } = filter;
    const minValue =
      _.isEmpty(rawMinValue) && typeof rawMinValue !== "number"
        ? undefined
        : parseValue(String(rawMinValue));
    const maxValue =
      _.isEmpty(rawMaxValue) && typeof rawMaxValue !== "number"
        ? undefined
        : parseValue(String(rawMaxValue));

    if (_.isUndefined(minValue) && _.isUndefined(maxValue)) {
      return true;
    }

    return (minValue ?? -Infinity) <= value && value <= (maxValue ?? Infinity);
  }

  function checkAllFilters(dataPoint: IData): boolean {
    return filters.every((filter): boolean => {
      if (typeof filter.key === "function")
        return filter.key(dataPoint, filter.value);

      switch (filter.type) {
        case "radioButton":
        case "checkboxes":
          return handleCheckboxSelectCases(
            dataPoint,
            filter as IFilterComp<IData>,
          );
        case "numberRange":
          return handleRangeCases(
            dataPoint,
            filter as IFilterComp<IData>,
            Number,
          );
        case "dateRange":
          return handleRangeCases(
            dataPoint,
            filter as IFilterComp<IData>,
            (value) => new Date(value),
          );
        case "select":
        case "text":
        default:
          return handleTextSelectCases(dataPoint, filter as IFilterComp<IData>);
      }
    });
  }

  return data.filter(checkAllFilters);
};

const getActiveFilters = <IData extends object>(
  filters: Readonly<IFilterOptions<IData>[]>,
): number => {
  const activeFilters = [
    ...filters
      .flatMap(({ options }) => options)
      .filter((option) => option?.checked),
    ...filters
      .flatMap((filter) => [filter.value, filter.maxValue, filter.minValue])
      .filter(Boolean),
  ];

  return activeFilters.length;
};

const clearFilterValues = <IData extends object>(
  filter: Readonly<IFilterOptions<IData>>,
): IFilterOptions<IData> => {
  const updatedOptions =
    filter.options?.map((option) => ({ ...option, checked: undefined })) ??
    undefined;

  return {
    ...filter,
    minValue: undefined,
    maxValue: undefined,
    value: undefined,
    options: updatedOptions,
  };
};

export { clearFilterValues, getActiveFilters, setFiltersUtil };
