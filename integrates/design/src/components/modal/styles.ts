import { DefaultTheme, styled } from "styled-components";

import type { TSize } from "./types";

import { variantBuilder } from "components/@core";
import { Container } from "components/container";

const { getVariant } = variantBuilder(
  (theme: DefaultTheme): Record<TSize, string> => ({
    fixed: `
      height: auto;
      min-width: 300px;
      padding: ${theme.spacing[1.25]};
      overflow: hidden auto;
    `,
    lg: `
      width: 80%;

      @media screen and (min-width: ${theme.breakpoints.sm}) {
        width: 72%;
      }
    `,
    md: `
      width: 60%;

      @media screen and (min-width: ${theme.breakpoints.sm}) {
        width: 48%;
      }
    `,
    sm: `
      width: 40%;

      @media screen and (min-width: ${theme.breakpoints.sm}) {
        width: 32%;
      }
    `,
  }),
);

const ModalWrapper = styled.div.attrs({
  className: "comp-modal fixed inset-0 overflow-auto",
})<{ $bgColor?: string }>`
  align-items: center;
  background-color: ${({ $bgColor = "rgb(52 64 84 / 70%)" }): string =>
    $bgColor};
  display: flex;
  justify-content: center;
  z-index: 99999;
`;

const ModalContainer = styled(Container)<{ $size: TSize }>`
  ${({ theme, $size }): string => `
    background-color: ${theme.palette.white};
    border: 1px solid ${theme.palette.gray[200]};
    border-radius: ${theme.spacing[0.25]};
    color: ${theme.palette.gray[800]};
    display: flex;
    font-family: ${theme.typography.type.primary};
    font-size: ${theme.typography.text.sm};
    max-height: 80%;
    min-height: minmax(content-fit, 32%);
    box-shadow: ${theme.shadows.lg};
    margin: auto;
    white-space: pre-line;

    ${getVariant(theme, $size)}
  `}
`;

const ModalContent = styled.div`
  ${({ theme }): string => `
    background-color: ${theme.palette.white};
    border-bottom: 1px solid ${theme.palette.gray[200]};
    border-radius: 0 0 ${theme.spacing[0.25]} ${theme.spacing[0.25]};
    display: flex;
    flex-direction: column;
    overflow: hidden auto;
    padding: 0 ${theme.spacing[1.5]} ${theme.spacing[1.5]} ${theme.spacing[1.5]};
    scrollbar-color: ${theme.palette.gray[600]} ${theme.palette.gray[100]};
    scroll-padding: ${theme.spacing[0.5]};

    &:has(form) {
      flex-direction: unset;
      overflow: hidden;
      padding: 0;
    }
  `}
`;

const Header = styled.div`
  border-radius: ${({ theme }): string =>
    `${theme.spacing[0.25]} ${theme.spacing[0.25]} 0 0`};
  display: block;
  overflow: unset;
  height: max-content;
  max-height: 100%;
  max-width: 100%;
  padding: ${({ theme }): string => `${theme.spacing[1.25]}`};
  padding-bottom: ${({ theme }): string => `${theme.spacing[1.5]}`};
  position: sticky;
`;

const Title = styled.div.attrs({
  className: "flex items-center justify-between",
})``;

const Footer = styled.div`
  ${({ theme }): string => `
    border-radius: 0 0 ${theme.spacing[0.25]} ${theme.spacing[0.25]};
    display: flex;
    flex-direction: row;
    gap: ${theme.spacing[0.75]};
    justify-content: space-between;
    max-width: 392px;

    > label {
      margin: 0;
      flex: 1 1 auto;
    }

    > div {
      display: flex;
      flex-direction: row;
      flex: 1 1 auto;
      gap: ${theme.spacing[0.75]};
      margin-right: ${theme.spacing[0.75]};

      > button {
        justify-content: center;
        max-width: 190px;
      }
    }

    > div:last-child {
      margin-right: 0;
    }
  `}
`;

const ImageContainer = styled.div<{ $framed?: boolean }>`
  img {
    margin: 0 calc(-${({ theme }): string => theme.spacing[1.25]} - 1px)
      ${({ theme }): string => theme.spacing[1.25]};

    min-width: calc(100% + 2px + ${({ theme }): string => theme.spacing[3]});
    height: 185px;
    object-fit: cover;

    ${({ $framed = false }): string =>
      $framed
        ? `
            border-radius: 8px;
            min-width: 100%;
            margin-left: 0;
            margin-right: 0;
          `
        : ``}
  }
`;

export {
  Header,
  ModalContainer,
  ModalContent,
  Footer,
  ModalWrapper,
  ImageContainer,
  Title,
};
