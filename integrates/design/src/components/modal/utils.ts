const groupOptions = <T extends { group?: string }>(
  columns: readonly T[],
): Record<string, T[]> => {
  const initialVisibility: Record<string, T[]> = columns.reduce(
    (acc, option): Record<string, T[]> => {
      if (!option.group) return acc;

      if (acc[option.group]) {
        return {
          ...acc,
          [option.group]: [...acc[option.group], option],
        };
      }

      return {
        ...acc,
        [option.group]: [option],
      };
    },
    {} as Record<string, T[]>,
  );

  return initialVisibility;
};

export { groupOptions };
