import { isEmpty, isString } from "lodash";
import { useCallback, useEffect } from "react";
import { useTheme } from "styled-components";

import { Header as TextHeader, Title } from "../styles";
import type { IModalHeaderProps } from "../types";
import { IconButton } from "components/icon-button";
import { Heading, Span, Text } from "components/typography";

const Header = ({
  title,
  description,
  modalRef,
  otherActions,
  onClose,
}: Readonly<IModalHeaderProps>): JSX.Element => {
  const theme = useTheme();
  const handleClose = useCallback((): void => {
    onClose?.();
    modalRef.close();
  }, [modalRef, onClose]);

  useEffect((): (() => void) => {
    const handleKeydown = (event: Readonly<KeyboardEvent>): void => {
      if (event.key === "Escape") {
        handleClose();
      }
    };
    window.addEventListener("keydown", handleKeydown);

    // eslint-disable-next-line functional/functional-parameters
    return (): void => {
      window.removeEventListener("keydown", handleKeydown);
    };
  }, [handleClose]);

  return (
    <TextHeader>
      <Title>
        {typeof title === "string" ? (
          <Heading
            fontWeight={"bold"}
            lineSpacing={1.75}
            mr={1}
            size={"sm"}
            wordBreak={"break-word"}
          >
            {title}
          </Heading>
        ) : (
          title
        )}
        <div className={"items-center flex gap-3"}>
          {otherActions}
          <IconButton
            icon={"close"}
            iconColor={theme.palette.gray[400]}
            iconSize={"xs"}
            iconTransform={"grow-2"}
            iconType={"fa-light"}
            id={"modal-close"}
            onClick={handleClose}
            px={0.25}
            py={0.25}
            variant={"ghost"}
          />
        </div>
      </Title>
      {description && isString(description) && !isEmpty(description) ? (
        <Text mt={0.5} size={"sm"}>
          {description}
        </Text>
      ) : undefined}
      {description && !isString(description) && (
        <Span display={"block"} mt={0.5} size={"sm"}>
          {description}
        </Span>
      )}
    </TextHeader>
  );
};

export { Header };
