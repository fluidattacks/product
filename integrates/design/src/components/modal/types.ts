import { ITabProps } from "components/tabs/types";
import type { IUseModal } from "hooks";

type TSize = "fixed" | "lg" | "md" | "sm";

/**
 * Button props.
 * @interface IButtonProps
 * @property { string } [id]
 * @property { string } text - The text of the button.
 * @property { Function } [onClick] - The function to call when the button is clicked.
 */
interface IButtonProps {
  id?: string;
  onClick?: () => void;
  text: string;
}

/**
 * Modal content props.
 * @interface IModalContent
 * @property { string } [imageSrc] - The source of the image.
 * @property { boolean } [imageFramed] - Whether the image is framed.
 * @property { string } [imageText] - The text of the image.
 */
interface IModalContent {
  imageSrc?: string;
  imageFramed?: boolean;
  imageText?: string;
}

/**
 * Modal header component props.
 * @interface IModalHeaderProps
 * @property { React.ReactNode | string } [description] - The description of the modal.
 * @property { IUseModal } [modalRef] - The modal reference.
 * @property { React.ReactNode } [otherActions] - The other actions of the modal.
 * @property { Function } [onClose] - The function to call when the modal is closed.
 * @property { React.ReactNode | string } [title] - The title of the modal.
 */
interface IModalHeaderProps {
  description?: React.ReactNode | string;
  modalRef: IModalProps["modalRef"];
  otherActions?: React.ReactNode;
  onClose?: () => void;
  title?: React.ReactNode | string;
}

/**
 * Modal footer component props.
 * @interface IModalFooterProps
 * @property { JSX.Element } [alert] - The alert element to show on footer.
 * @property { IButtonProps } [cancelButton] - The cancel button props.
 * @property { IButtonProps } [confirmButton] - The confirm button props.
 * @property { IUseModal } [modalRef] - The modal reference.
 */
interface IModalFooterProps {
  alert?: JSX.Element;
  cancelButton?: IButtonProps;
  confirmButton?: IButtonProps;
  modalRef: IUseModal;
}

/**
 * Modal component props.
 * @interface IModalProps
 * @extends IModalHeaderProps
 * @extends IModalFooterProps
 * @property { boolean } [portal] - Whether the modal is a portal.
 * @property { IModalContent } [content] - The modal content.
 * @property { ITabProps } [tabItems] - The tab items.
 * @property { string } [id] - The id of the modal.
 * @property { TSize } [size] - The size of the modal.
 */
interface IModalProps
  extends Omit<IModalHeaderProps, "modalRef">,
    IModalFooterProps {
  _portal?: boolean;
  content?: IModalContent;
  tabItems?: ITabProps[];
  id?: string;
  size: TSize;
}

/**
 * Modal confirm props.
 * @interface IModalConfirmProps
 * @property { boolean } [disabled] - Whether the confirm button is disabled.
 * @property { string } [id] - The id of the confirm button.
 * @property { Function } [onCancel] - The function to call when the cancel button is clicked.
 * @property { "submit" | Function } [onConfirm] - The function to call when the confirm button is clicked.
 * @property { string } [txtCancel] - The text of the cancel button.
 * @property { React.ReactNode | string } [txtConfirm] - The text of the confirm button.
 */
interface IModalConfirmProps {
  disabled?: boolean;
  id?: string;
  onCancel?: () => void;
  onConfirm?: "submit" | (() => void);
  txtCancel?: string;
  txtConfirm?: React.ReactNode | string;
}

export type {
  IModalProps,
  IModalContent,
  IModalConfirmProps,
  IModalFooterProps,
  IModalHeaderProps,
  TSize,
};
