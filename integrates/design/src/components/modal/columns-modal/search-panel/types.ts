import { IColumnModalProps } from "hooks/use-columns-modal";

/**
 * Columns selector component props.
 * @interface ISearchPanelProps
 * @property { IColumnModalProps[] } columns - The columns to display.
 * @property { string } [formId] - The form id.
 * @property { Function } onChangeHandler - The handler for change events.
 * @property { "grouped" | "ungrouped" } [variant] - The variant of the columns selector.
 */
interface ISearchPanelProps {
  columns: IColumnModalProps[];
  formId?: string;
  onChangeHandler: (
    event: Readonly<React.ChangeEvent<HTMLInputElement>>,
  ) => void;
  variant?: "grouped" | "ungrouped";
}

export type { ISearchPanelProps };
