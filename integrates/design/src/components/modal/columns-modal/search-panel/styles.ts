import styled from "styled-components";

import { ListItem } from "components/list-item";

const StyledList = styled(ListItem)`
  padding: 7px 8px;

  &[aria-selected="true"] {
    background-color: unset;
  }

  svg {
    &.fa-check {
      border-radius: 2px;
    }

    &.selected {
      display: none;
    }
  }
`;

export { StyledList };
