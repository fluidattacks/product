import { Reorder } from "framer-motion";
import { useCallback } from "react";

import { StyledReorderItem } from "./styles";
import { IPreviewPanelProps } from "./types";

import { Container } from "components/container";
import { Tag } from "components/tag";
import { Heading, Text } from "components/typography";

const PreviewPanel = ({
  items,
  onReorder,
  title,
  description,
  onClickClose,
}: Readonly<IPreviewPanelProps>): JSX.Element => {
  const onClickCloseHandler = useCallback(
    (label: string): VoidFunction => {
      // eslint-disable-next-line functional/functional-parameters
      return (): void => {
        onClickClose(label);
      };
    },
    [onClickClose],
  );

  return (
    <Reorder.Group
      as={"div"}
      axis={"y"}
      layoutScroll={true}
      onReorder={onReorder}
      style={{
        height: "max-content",
        padding: "1.25rem 0",
        width: "45%",
      }}
      values={items}
    >
      <Container display={"flex"} gap={0.25}>
        <Heading display={"flex"} size={"xs"}>
          {title}
        </Heading>
        <Tag
          priority={"high"}
          tagLabel={String(
            items.filter((item): boolean => item.visible).length,
          )}
          variant={"error"}
        />
      </Container>
      <Text size={"sm"}>{description}</Text>
      {items.map((item) =>
        item.visible ? (
          <Reorder.Item
            as={"div"}
            dragListener={!item.locked}
            item={item.name}
            key={item.name}
            value={item}
          >
            <StyledReorderItem
              disabled={item.locked}
              icon={"grid-2"}
              label={item.name}
              onClickIcon={
                item.locked ? undefined : onClickCloseHandler(item.id)
              }
              rightIcon={item.locked ? "lock" : "close"}
              selected={item.visible}
              value={item.name}
            >
              {item.name}
            </StyledReorderItem>
          </Reorder.Item>
        ) : undefined,
      )}
    </Reorder.Group>
  );
};

export { PreviewPanel };
