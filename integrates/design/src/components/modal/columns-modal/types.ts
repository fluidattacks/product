import { ISearchPanelProps } from "./search-panel/types";

import { IUseColumnsModal } from "hooks/use-columns-modal";

/**
 * @interface IColumnsModalProps
 * @property { string } [_containerId] - The portal container id
 * @property { string } title - The title of the modal
 * @property { IUseColumnsModal } modalRef - The modal reference
 * @property { ISearchPanelProps["variant"] } variant - The variant of the columns selector
 */
interface IColumnsModalProps {
  _containerId?: string;
  title: string;
  modalRef: IUseColumnsModal;
  variant: ISearchPanelProps["variant"];
}

export type { IColumnsModalProps };
