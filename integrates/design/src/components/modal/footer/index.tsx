import { isEmpty } from "lodash";
import { useCallback } from "react";

import { Footer } from "../styles";
import type { IModalFooterProps } from "../types";
import { Button } from "components/button";
import { Container } from "components/container";

const ModalFooter = ({
  alert,
  modalRef,
  confirmButton = undefined,
  cancelButton = undefined,
}: Readonly<IModalFooterProps>): JSX.Element | null => {
  const { close } = modalRef;
  const handleCancel = useCallback((): void => {
    if (cancelButton?.onClick) cancelButton.onClick();
    close();
  }, [cancelButton, close]);

  const handleConfirm = useCallback((): void => {
    if (confirmButton?.onClick) confirmButton.onClick();
    close();
  }, [confirmButton, close]);

  const isThereExtraInfo = [confirmButton, cancelButton].some(
    (item): boolean => !isEmpty(item),
  );

  if (isThereExtraInfo) {
    return (
      <Container
        display={"flex"}
        flexDirection={"column"}
        gap={0.625}
        position={"sticky"}
        px={1.25}
        py={1.25}
      >
        {alert}
        <Footer>
          <div>
            {cancelButton && (
              <Button
                id={cancelButton.id}
                onClick={handleCancel}
                variant={"tertiary"}
                width={"100%"}
              >
                {cancelButton.text}
              </Button>
            )}
            {confirmButton && (
              <Button
                id={confirmButton.id}
                onClick={handleConfirm}
                variant={"primary"}
                width={"100%"}
              >
                {confirmButton.text}
              </Button>
            )}
          </div>
        </Footer>
      </Container>
    );
  }

  return null;
};

export { ModalFooter };
