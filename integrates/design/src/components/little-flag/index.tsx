import { LittleFlag as StyledLittleFlag } from "./styles";
import { ILittleFlagProps } from "./types";

const LittleFlag = ({
  bgColor,
  children,
  txtDecoration,
  ml = 1,
}: Readonly<ILittleFlagProps>): JSX.Element => (
  <StyledLittleFlag
    $bgColor={bgColor}
    $txtDecoration={txtDecoration}
    className={`ml-${ml}`}
  >
    {children}
  </StyledLittleFlag>
);

export type { ILittleFlagProps };
export { LittleFlag };
