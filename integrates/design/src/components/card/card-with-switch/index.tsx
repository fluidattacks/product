import { useTheme } from "styled-components";

import { CardHeader } from "../card-header";
import type { ICardWithSwitchProps } from "../types";
import { Container } from "components/container";
import { Toggle } from "components/toggle";

const CardWithSwitch = ({
  id,
  title,
  toggles,
  minWidth = "auto",
  height = "auto",
}: Readonly<ICardWithSwitchProps>): JSX.Element => {
  const theme = useTheme();

  return (
    <Container
      alignItems={"center"}
      bgColor={theme.palette.white}
      border={`1px solid ${theme.palette.gray[200]}`}
      borderRadius={theme.spacing[0.25]}
      display={"flex"}
      flexDirection={"column"}
      gap={0.5}
      height={height}
      justify={"center"}
      minHeight={"100px"}
      minWidth={minWidth}
      padding={[1.25, 1.25, 1.25, 1.25]}
    >
      <CardHeader id={id} title={title} />
      <Container
        display={"flex"}
        flexDirection={"column"}
        gap={0.5}
        minHeight={"40px"}
        width={"100%"}
      >
        {toggles.map(
          (props): JSX.Element => (
            <Toggle key={`$card-toggle-${props.name}`} {...props} />
          ),
        )}
      </Container>
    </Container>
  );
};

export { CardWithSwitch };
