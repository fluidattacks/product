import { useTheme } from "styled-components";

import type { ICardWithSelectorProps } from "../types";
import { CloudImage } from "components/cloud-image";
import { Container } from "components/container";
import { Icon } from "components/icon";
import { RadioButton } from "components/radio-button";
import { Text } from "components/typography";

const CardWithSelector = ({
  alt,
  imageId,
  icon,
  selectorProps,
  onClick,
  register,
  title,
  width = "fit-content",
}: Readonly<ICardWithSelectorProps>): JSX.Element => {
  const theme = useTheme();

  return (
    <Container
      alignItems={"center"}
      border={`1px solid ${theme.palette.gray[200]}`}
      borderColorHover={theme.palette.gray[600]}
      borderRadius={theme.spacing[0.25]}
      display={"inline-flex"}
      padding={[1, 1, 1, 1]}
      shadowHover={"md"}
      width={width}
    >
      <Container alignSelf={"start"} display={"flex"} width={"fit-content"}>
        <RadioButton
          name={selectorProps.name}
          onClick={onClick}
          value={selectorProps.value}
          {...register?.(selectorProps.name)}
        />
      </Container>
      <Container
        alignItems={"center"}
        display={"flex"}
        gap={0.5}
        justify={"space-between"}
        ml={1}
        mr={1}
        width={"100%"}
      >
        {imageId === undefined ? undefined : (
          <CloudImage
            alt={alt ?? "card image"}
            height={"32px"}
            publicId={imageId}
            width={"32px"}
          />
        )}
        {icon === undefined ? undefined : (
          <Icon icon={icon} iconSize={"sm"} iconType={"fa-light"} />
        )}
        <Text color={theme.palette.gray[800]} fontWeight={"bold"} size={"sm"}>
          {title}
        </Text>
      </Container>
    </Container>
  );
};

export { CardWithSelector };
