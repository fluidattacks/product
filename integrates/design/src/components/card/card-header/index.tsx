import { useTheme } from "styled-components";

import type { ICardHeader } from "../types";
import { Container } from "components/container";
import { Tooltip } from "components/tooltip";
import { Text } from "components/typography";

const CardHeader = ({
  authorEmail,
  date,
  description,
  descriptionColor,
  id,
  title,
  titleColor,
  textAlign,
  textSpacing = 0,
  tooltip,
}: Readonly<ICardHeader>): JSX.Element => {
  const theme = useTheme();

  return (
    <Container width={"100%"}>
      {typeof title === "string" ? (
        <Text
          color={titleColor ?? theme.palette.gray[800]}
          fontWeight={"bold"}
          mb={textSpacing}
          size={"sm"}
          textAlign={textAlign}
          wordBreak={"break-word"}
        >
          {title}
        </Text>
      ) : (
        title
      )}
      <Container
        alignItems={"start"}
        display={"flex"}
        flexDirection={"column"}
        gap={0.25}
      >
        {description && (
          <Text
            color={descriptionColor ?? theme.palette.gray[800]}
            fontWeight={description === undefined ? "bold" : "regular"}
            size={"sm"}
            textAlign={textAlign}
            wordBreak={"break-word"}
          >
            {description}
          </Text>
        )}
        {date && (
          <Text
            color={theme.palette.gray[800]}
            fontWeight={"bold"}
            size={"sm"}
            textAlign={textAlign}
            wordBreak={"break-word"}
          >
            {`Date: ${date.split(" ")[0]}`}
          </Text>
        )}
        {authorEmail && (
          <Text
            color={theme.palette.gray[800]}
            fontWeight={"bold"}
            size={"sm"}
            textAlign={textAlign}
            wordBreak={"break-word"}
          >
            {`Author: ${authorEmail}`}
          </Text>
        )}
        {tooltip && <Tooltip icon={"circle-info"} id={id} tip={tooltip} />}
      </Container>
    </Container>
  );
};

export { CardHeader };
