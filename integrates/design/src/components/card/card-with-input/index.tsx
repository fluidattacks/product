import { useMemo } from "react";

import { CardInputContainer } from "./styles";

import { CardHeader } from "../card-header";
import type { ICardWithInputProps } from "../types";
import { Input, InputDate, InputNumber } from "components/inputs";

const CardWithInput = ({
  description,
  disabled,
  id,
  align,
  inputType = "text",
  inputNumberProps,
  minHeight,
  minWidth,
  name,
  placeholder,
  tooltip,
  title,
}: Readonly<ICardWithInputProps>): JSX.Element => {
  const input = useMemo((): JSX.Element => {
    if (inputType === "text")
      return (
        <Input disabled={disabled} name={name} placeholder={placeholder} />
      );

    if (inputType === "date")
      return <InputDate disabled={disabled} name={name} />;

    const { decimalPlaces, max, min } = inputNumberProps ?? {};

    return (
      <InputNumber
        decimalPlaces={decimalPlaces}
        disabled={disabled}
        max={max}
        min={min}
        name={name}
        placeholder={placeholder}
      />
    );
  }, [disabled, inputNumberProps, inputType, name, placeholder]);

  return (
    <CardInputContainer
      $align={align}
      $minHeight={minHeight}
      $minWidth={minWidth}
    >
      <CardHeader
        description={description}
        id={id}
        title={title}
        tooltip={tooltip}
      />
      {input}
    </CardInputContainer>
  );
};

export { CardWithInput };
