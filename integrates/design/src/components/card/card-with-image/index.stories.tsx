import type { Meta, StoryObj } from "@storybook/react";

import { CardWithImage } from ".";
import { GridContainer } from "../../grid-container";
import type { ICardWithImageProps } from "../types";

const config: Meta = {
  component: CardWithImage,
  tags: ["autodocs"],
  title: "Components/Cards/CardWithImage",
};

const CardImgTemplate: StoryObj<{
  itemsProps: ICardWithImageProps[];
}> = {
  render: ({ itemsProps }): JSX.Element => (
    <GridContainer lg={4} md={3} sm={2} xl={5}>
      {itemsProps.map((props, index): JSX.Element => {
        const key = `cardWithImg-${index}`;

        return <CardWithImage {...props} key={key} />;
      })}
    </GridContainer>
  ),
};

const src =
  "https://res.cloudinary.com/fluid-attacks/image/upload/f_auto,q_auto/v1/airs/blogs/blogs-cta?_a=AXCkERR0";

const videoSrc =
  "https://res.cloudinary.com/fluid-attacks/video/upload/v1741122852/samples/animation.webm";

const description =
  "It is possible to induce the application’s server into making requests to an arbitrary domain. in product/skims/test/data/lib_root/f100/test.cs1\nDate: 2022-07-21";

const Default = {
  ...CardImgTemplate,
  args: {
    itemsProps: [
      {
        alt: "Fluid Video",
        description,
        showMaximize: false,
        src: videoSrc,
        title: "Video",
        videoViewStatus: (stage: "start" | "half" | "end"): void => {
          console.log(`Video view status: ${stage}`);
        },
      },
      {
        alt: "Fluid Image",
        description,
        src,
        title: "Image 1",
      },
      {
        alt: "Fluid Image",
        description,
        src,
        title: "Image 2",
      },
      {
        alt: "Fluid Image",
        description,
        src,
        title: "Image 3",
      },
      {
        alt: "Fluid Image",
        description,
        src,
        title: "Image 4",
      },
    ],
  },
};

export { Default };
export default config;
