import React, { PropsWithChildren, useCallback, useState } from "react";
import { useTheme } from "styled-components";

import { CardHeader } from "../card-header";
import type { ICardWithImageProps } from "../types";
import { Container } from "components/container";
import { FilePreview } from "components/file-preview";
import { IconButton } from "components/icon-button";

const CardWithImage = ({
  alt,
  authorEmail,
  date,
  description,
  src,
  children,
  isEditing,
  onClick,
  title,
  hideDescription = false,
  headerType = "image",
  showMaximize = true,
  videoViewStatus,
}: Readonly<PropsWithChildren<ICardWithImageProps>>): JSX.Element => {
  const theme = useTheme();
  const [isHovered, setIsHovered] = useState(false);
  const onHover = useCallback((): void => {
    setIsHovered(true);
  }, []);
  const onLeave = useCallback((): void => {
    setIsHovered(false);
  }, []);

  return (
    <Container
      bgColor={"transparent"}
      border={"1px solid transparent"}
      borderColorHover={"black"}
      borderRadius={theme.spacing[0.25]}
      display={"flex"}
      flexDirection={"column"}
      gap={1}
      onHover={onHover}
      onLeave={onLeave}
      padding={[0.5, 0.5, 0.5, 0.5]}
      width={"100%"}
    >
      <Container
        cursor={isEditing ? "unset" : "pointer"}
        display={"inline-flex"}
        justify={"center"}
        onClick={onClick}
        position={"relative"}
        width={"100%"}
      >
        {children === undefined ? (
          <React.Fragment>
            <FilePreview
              alt={alt}
              fileType={headerType}
              height={"147px"}
              opacity={isHovered ? 0.3 : 0}
              src={src}
              videoViewStatus={videoViewStatus}
              width={"100%"}
            />
            {showMaximize ? (
              <Container bottom={"10px"} position={"absolute"} right={"10px"}>
                <IconButton
                  color={"white"}
                  icon={"arrows-maximize"}
                  iconSize={"xxs"}
                  iconType={"fa-light"}
                  variant={"secondary"}
                />
              </Container>
            ) : undefined}
          </React.Fragment>
        ) : (
          children
        )}
      </Container>
      {hideDescription ? null : (
        <CardHeader
          authorEmail={authorEmail}
          date={date}
          description={description}
          descriptionColor={theme.palette.gray[600]}
          id={`${alt}-card-header`}
          title={title}
        />
      )}
    </Container>
  );
};

export { CardWithImage };
