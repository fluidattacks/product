import { IStyledLottieProps } from "./styles";

/**
 * Lottie props.
 * @interface ILottieProps
 * @extends Partial<IStyledLottieProps>
 * @property {IStyledLottieProps[number]} [size] - Size for the Lottie component.
 */
interface ILottieProps extends Partial<IStyledLottieProps> {
  readonly size?: IStyledLottieProps["$size"];
}

export type { ILottieProps };
