import type { Ref } from "react";
import { forwardRef } from "react";

import type { IDividerProps } from "./types";

import { theme } from "components/colors";
import { Container } from "components/container";

const Divider = forwardRef(function Divider(
  {
    color = theme.palette.gray[100],
    mt = 0.5,
    mb = 0.5,
    width,
    ...props
  }: Readonly<IDividerProps>,
  ref: Ref<HTMLHRElement>,
): JSX.Element {
  return (
    <Container
      as={"hr"}
      borderTop={`1px solid ${color}`}
      mb={mb}
      mt={mt}
      ref={ref}
      width={width ?? "100%"}
      {...props}
    />
  );
});

export { Divider };
