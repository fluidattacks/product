import type {
  IDisplayModifiable,
  IMarginModifiable,
} from "components/@core/types";

/**
 * Divider component props.
 * @interface IDividerProps
 * @extends IMarginModifiable
 * @property {string} [color] - The divider color.
 */
interface IDividerProps
  extends Pick<IDisplayModifiable, "height" | "width">,
    IMarginModifiable {
  color?: string;
}

export type { IDividerProps };
