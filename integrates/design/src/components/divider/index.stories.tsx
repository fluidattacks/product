import type { Meta, StoryFn } from "@storybook/react";
import React from "react";

import type { IDividerProps } from "./types";

import { Divider } from ".";
import { theme } from "../colors";
import { Container } from "../container";
import { Heading, Text } from "../typography";

const config: Meta = {
  component: Divider,
  tags: ["autodocs"],
  title: "Components/Divider",
};

const Template: StoryFn<React.PropsWithChildren<IDividerProps>> = (
  props,
): JSX.Element => (
  <Container>
    <Heading size={"lg"}>{"Items"}</Heading>
    <Divider {...props} />
    {[...Array(3).keys()].map((index): JSX.Element => {
      return <Text key={index} size={"md"}>{`Item ${index + 1}`}</Text>;
    })}
  </Container>
);

const Default = Template.bind({});

Default.args = {
  color: theme.palette.gray[100],
  mb: 1,
  mt: 1,
};

export { Default };
export default config;
