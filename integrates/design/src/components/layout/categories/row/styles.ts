import type { Property } from "csstype";
import { styled } from "styled-components";

interface IStyledRowProps {
  $align?: Property.AlignItems;
  $cols?: number;
  $colsPadding?: number;
  $justify?: Property.JustifyContent;
}

/**
 * @param cols Total of cols for this Row's descendants
 */
const StyledRow = styled.div.attrs({
  className: "comp-row flex flex-row flex-wrap",
})<IStyledRowProps>`
  ${({
    $align = "stretch",
    $cols = 100,
    $colsPadding = 6,
    $justify = "start",
  }): string => `
    align-items: ${$align};
    justify-content: ${$justify};
    margin: -6px;
    text-align: ${$justify};

    --cols: ${$cols};

    > *:not(.comp-col) {
      width: 100%;
      margin: 6px;
    }

    > .comp-col {
      padding: ${$colsPadding}px;
    }
  `}
`;

export { StyledRow };
