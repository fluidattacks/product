import type { Property } from "csstype";
import { styled } from "styled-components";

interface IStyledGapProps {
  $disp?: Property.Display;
  $mh?: number;
  $mv?: number;
}

const StyledGap = styled.div.attrs({
  className: "comp-gap",
})<IStyledGapProps>`
  ${({ $disp = "flex", $mh = 4, $mv = 4 }): string => `
    align-items: center;
    display: ${$disp};
    margin: ${-$mv}px ${-$mh}px;

    > * {
      margin: ${$mv}px ${$mh}px;
    }
  `}
`;

export { StyledGap };
