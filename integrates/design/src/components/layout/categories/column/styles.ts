import isNumber from "lodash/isNumber";
import { styled } from "styled-components";

interface IStyledColProps {
  $xl?: number;
  $lg?: number;
  $md?: number;
  $sm?: number;
  $paddingTop?: number;
  $width?: string;
}

const getAttrs = (cols?: number): string =>
  cols === undefined
    ? "flex-grow: 1;"
    : `width: calc(${cols}00% / var(--cols));`;

/**
 * @param xl Amount of cols taken from nearest Row in extra-large screens
 * @param lg Amount of cols taken from nearest Row in large screens
 * @param md Amount of cols taken from nearest Row in medium screens
 * @param sm Amount of cols taken from nearest Row in small screens
 */
const StyledCol = styled.div.attrs({
  className: "comp-col",
})<IStyledColProps>`
  word-wrap: break-word;

  ${({ $width = null }): string => ($width === null ? "" : `width: ${$width}`)};

  ${({ $paddingTop }): string => `padding-top: ${$paddingTop ?? 0}px;`}

  @media (width <= 768px) {
    ${({ $sm }): string => getAttrs($sm)}
  }

  @media (width >= 768px) and (width <= 992px) {
    ${({ $md }): string => getAttrs($md)}
  }

  @media (width >= 992px) {
    ${({ $lg }): string => getAttrs($lg)}
  }

  ${({ $xl }): string =>
    isNumber($xl) ? `@media (min-width: 1200px) { ${getAttrs($xl)} }` : ""}

  > .comp-card {
    height: 100%;
  }
`;

export { StyledCol };
