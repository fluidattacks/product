import { StyledCol } from "./styles";

import { IColProps } from "../../types";

const Col = ({
  children,
  xl,
  lg,
  md,
  sm,
  paddingTop,
  width,
}: Readonly<IColProps>): JSX.Element => (
  <StyledCol
    $lg={lg}
    $md={md}
    $paddingTop={paddingTop}
    $sm={sm}
    $width={width}
    $xl={xl}
  >
    {children}
  </StyledCol>
);

export { Col };
