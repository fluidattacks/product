import type { Property } from "csstype";
import type { HTMLAttributes } from "react";

/**
 * Column component props.
 * @interface IColProps
 * @extends HTMLAttributes<HTMLDivElement>
 * @property { number } [xl] - The number of columns in xl resolution.
 * @property { number } [lg] - The number of columns in lg resolution.
 * @property { number } [md] - The number of columns in md resolution.
 * @property { number } [sm] - The number of columns in sm resolution.
 * @property { number } [paddingTop] - The padding top of the column.
 * @property { string } [width] - The width of the column.
 */
interface IColProps extends HTMLAttributes<HTMLDivElement> {
  xl?: number;
  lg?: number;
  md?: number;
  sm?: number;
  paddingTop?: number;
  width?: string;
}

/**
 * Gap component props.
 * @interface IGapProps
 * @extends HTMLAttributes<HTMLDivElement>
 * @property { Property.Display } [disp] - The display of the gap.
 * @property { number } [mh] - The horizontal margin.
 * @property { number } [mv] - The vertical margin.
 */
interface IGapProps extends HTMLAttributes<HTMLDivElement> {
  disp?: Property.Display;
  mh?: number;
  mv?: number;
}

/**
 * Row component props.
 * @interface IRowProps
 * @extends HTMLAttributes<HTMLDivElement>
 * @property { Property.AlignItems } [align] - The row alignment.
 * @property { number } [cols] - The number of columns.
 * @property { number } [colsPadding] - The padding for each column.
 * @property { Property.JustifyContent } [justify] - The row justify.
 */
interface IRowProps extends HTMLAttributes<HTMLDivElement> {
  align?: Property.AlignItems;
  cols?: number;
  colsPadding?: number;
  justify?: Property.JustifyContent;
}

export type { IColProps, IGapProps, IRowProps };
