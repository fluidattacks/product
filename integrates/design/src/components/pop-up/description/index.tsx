import { Fragment } from "react";
import { useTheme } from "styled-components";

import { Span, Text } from "components/typography";

const Description = ({
  description = "",
  highlightDescription = "",
}: Readonly<{
  description: string;
  highlightDescription: string[] | string;
}>): JSX.Element => {
  const theme = useTheme();
  const highlightType = typeof highlightDescription === "string";
  const separatorRegex = highlightType
    ? ""
    : new RegExp(`(${highlightDescription.join("|")})`, "u");

  if (highlightDescription.length > 0) {
    return (
      <Text
        display={"inline"}
        lineSpacing={1.5}
        size={"lg"}
        textAlign={"center"}
      >
        {highlightType ? (
          <Fragment>
            <Span
              color={theme.palette.gray["600"]}
              display={"inline"}
              fontWeight={"bold"}
              lineSpacing={1.5}
              size={"lg"}
            >
              {highlightDescription}
            </Span>
            {description}
          </Fragment>
        ) : (
          description
            .split(separatorRegex)
            .map((text, idx): JSX.Element | string =>
              highlightDescription.includes(text) ? (
                <Span
                  color={theme.palette.gray["600"]}
                  display={"inline"}
                  fontWeight={"bold"}
                  key={`${idx + 1}-bold`}
                  lineSpacing={1.5}
                  size={"lg"}
                >
                  {text}
                </Span>
              ) : (
                text
              ),
            )
        )}
      </Text>
    );
  }

  return (
    <Text lineSpacing={1.5} size={"lg"} textAlign={"center"}>
      {description}
    </Text>
  );
};

export { Description };
