import type { Meta, StoryFn } from "@storybook/react";
import { expect, userEvent, within } from "@storybook/test";
import React from "react";
import { MemoryRouter } from "react-router-dom";

import type { ITabsProps } from "./types";

import { Tabs } from ".";

const config: Meta = {
  component: Tabs,
  tags: ["autodocs"],
  title: "Components/Tabs",
};

const Template: StoryFn<React.PropsWithChildren<ITabsProps>> = (
  props,
): JSX.Element => {
  return (
    <MemoryRouter initialEntries={["/tab1"]}>
      <Tabs {...props} />
    </MemoryRouter>
  );
};

const Default = Template.bind({});
Default.args = {
  items: [
    {
      id: "tab1",
      label: "Tab 1",
      link: "/tab1",
    },
    {
      id: "tab2",
      label: "Tab 2",
      link: "/tab2",
    },
    {
      id: "tab3",
      label: "Tab 3",
      link: "/tab3",
    },
    {
      id: "tab4",
      label: "Tab 4",
      link: "/tab4",
    },
    {
      id: "tab5",
      label: "Tab 5",
      link: "/tab5",
    },
  ],
};
Default.play = async ({ canvasElement, step }): Promise<void> => {
  const canvas = within(canvasElement);
  const tab1 = canvas.getByRole("link", { name: "Tab 1" });
  const tab2 = canvas.getByRole("link", { name: "Tab 2" });
  const tab3 = canvas.getByRole("link", { name: "Tab 3" });
  const tab4 = canvas.getByRole("link", { name: "Tab 4" });
  const tab5 = canvas.getByRole("link", { name: "Tab 5" });

  await step("should render all tabs", async (): Promise<void> => {
    await expect(tab1).toBeInTheDocument();
    await expect(tab2).toBeInTheDocument();
    await expect(tab3).toBeInTheDocument();
    await expect(tab4).toBeInTheDocument();
    await expect(tab5).toBeInTheDocument();
  });

  await step(
    "should change active tab and redirect",
    async (): Promise<void> => {
      await expect(tab1).toHaveClass("active");
      await expect(tab2).not.toHaveClass("active");
      await userEvent.click(tab2);
      await expect(tab1).not.toHaveClass("active");
      await expect(tab2).toHaveClass("active");
      await userEvent.click(tab3);
      await expect(tab2).not.toHaveClass("active");
      await expect(tab3).toHaveClass("active");
      await expect(tab3).toHaveAttribute("href", "/tab3");
      await userEvent.click(tab1);
    },
  );
};

const WithTooltips = Template.bind({});
WithTooltips.args = {
  items: [
    {
      id: "tab1",
      label: "Tab 1",
      link: "/tab1",
      tooltip: "Test tooltip tab 1",
    },
    {
      id: "tab2",
      label: "Tab 2",
      link: "/tab2",
      tooltip: "Test tooltip tab 2",
    },
    {
      id: "tab3",
      label: "Tab 3",
      link: "/tab3",
      tooltip: "Test tooltip tab 3",
    },
    {
      id: "tab4",
      label: "Tab 4",
      link: "/tab4",
      tooltip: "Test tooltip tab 4",
    },
    {
      id: "tab5",
      label: "Tab 5",
      link: "/tab5",
      tooltip: "Test tooltip tab 5",
    },
  ],
};

const SecondaryTabs = Template.bind({});
SecondaryTabs.args = {
  items: [
    {
      id: "tab1",
      label: "Tab 1",
      link: "/tab1",
      tooltip: "Test tooltip tab 1",
    },
    {
      id: "tab2",
      label: "Tab 2",
      link: "/tab2",
      tooltip: "Test tooltip tab 2",
    },
    {
      id: "tab3",
      label: "Tab 3",
      link: "/tab3",
      tooltip: "Test tooltip tab 3",
    },
    {
      id: "tab4",
      label: "Tab 4",
      link: "/tab4",
      tooltip: "Test tooltip tab 4",
    },
    {
      id: "tab5",
      label: "Tab 5",
      link: "/tab5",
      tooltip: "Test tooltip tab 5",
    },
  ],
  variant: "secondary",
};

const WithTag = Template.bind({});
WithTag.args = {
  items: [
    {
      id: "tab1",
      label: "Tab 1",
      link: "/tab1",
      tag: "1",
    },
    {
      id: "tab2",
      label: "Tab 2",
      link: "/tab2",
      tag: "2",
    },
    {
      id: "tab3",
      label: "Tab 3",
      link: "/tab3",
      tag: "3",
    },
    {
      id: "tab4",
      label: "Tab 4",
      link: "/tab4",
      tag: "4",
    },
    {
      id: "tab5",
      label: "Tab 5",
      link: "/tab5",
      tag: "5",
    },
  ],
};

export { Default, WithTooltips, SecondaryTabs, WithTag };
export default config;
