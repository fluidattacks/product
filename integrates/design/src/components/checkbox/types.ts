import type { HTMLAttributes } from "react";

/**
 * Checkbox props.
 * @interface ICheckboxProps
 * @extends HTMLAttributes<HTMLInputElement>
 * @property {boolean} [disabled] - Disable checkbox.
 * @property {boolean} [checked] - Checkbox checked state.
 * @property {string} [label] - Checkbox label.
 * @property {string} [tooltip] - Tooltip associated with the checkbox.
 * @property {string} name - Input name.
 * @property {string} value - Checkbox value.
 */
interface ICheckboxProps extends HTMLAttributes<HTMLInputElement> {
  disabled?: boolean;
  checked?: boolean;
  label?: string;
  name: string;
  tooltip?: string;
  value?: string;
}

export type { ICheckboxProps };
