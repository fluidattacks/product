import { Ref, forwardRef } from "react";

import { InputContainer } from "./styles";
import type { ICheckboxProps } from "./types";

import { Icon } from "components/icon";
import { Tooltip } from "components/tooltip";

const Checkbox = forwardRef(function Checkbox(
  {
    checked,
    disabled = false,
    label,
    name,
    onChange,
    onClick,
    onFocus,
    onKeyDown,
    tooltip,
    value,
    ...props
  }: Readonly<ICheckboxProps>,
  ref: Ref<HTMLInputElement>,
): JSX.Element {
  return (
    <div className={"gap-1 inline-flex items-start"}>
      <InputContainer aria-disabled={disabled}>
        <input
          aria-checked={checked}
          aria-disabled={disabled}
          aria-hidden={false}
          aria-label={name}
          checked={checked}
          disabled={disabled}
          id={name}
          name={name}
          onChange={onChange}
          onClick={onClick}
          onFocus={onFocus}
          onKeyDown={onKeyDown}
          ref={ref}
          type={"checkbox"}
          value={value}
          {...props}
        />
        <Icon disabled={disabled} icon={"check"} iconSize={"sm"} />
        {label ?? value}
      </InputContainer>
      {tooltip === undefined ? undefined : (
        <Tooltip
          icon={"circle-info"}
          id={tooltip}
          place={"right"}
          tip={tooltip}
        />
      )}
    </div>
  );
});

export { Checkbox };
