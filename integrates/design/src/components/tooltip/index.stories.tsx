import type { Meta, StoryFn } from "@storybook/react";

import type { ITooltipProps } from "./types";

import { Tooltip } from ".";
import { Button } from "../button";

const config: Meta = {
  component: Tooltip,
  parameters: { a11y: { disable: true } },
  tags: ["autodocs"],
  title: "Components/Tooltip",
};

const Template: StoryFn<ITooltipProps> = (props): JSX.Element => (
  <Tooltip {...props}>
    <Button variant={"ghost"}>{"Button Text"}</Button>
  </Tooltip>
);

const Default = Template.bind({});
Default.args = {
  id: "some1",
  place: "right",
  tip: "This is a tooltip",
};

const TitleTooltip = Template.bind({});
TitleTooltip.args = {
  id: "some2",
  place: "right",
  tip: `
    Tooltips are used to describe or identify an element.
    In most scenarios, tooltips help the user understand the meaning,
    function or alt-text of an element.
  `,
  title: "This is a tooltip",
};

const IconTooltip = Template.bind({});
IconTooltip.args = {
  icon: "circle-info",
  id: "some3",
  place: "right",
  tip: "Tooltips are used to describe or identify an element.",
  title: "This is a tooltip",
};

export { Default, TitleTooltip, IconTooltip };
export default config;
