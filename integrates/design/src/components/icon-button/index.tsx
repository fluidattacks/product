import type { Ref } from "react";
import { forwardRef } from "react";

import { StyledIconButton } from "./styles";
import type { IIconButtonProps } from "./types";

import { Icon } from "components/icon";
import { Tooltip } from "components/tooltip";

const IconButton = forwardRef(function IconButton(
  {
    borderRadius,
    disabled,
    icon,
    iconColor,
    iconSize = "xxs",
    iconTransform,
    iconType,
    id,
    justify = "center",
    onClick,
    px,
    py,
    type = "button",
    tooltip,
    tooltipPlace,
    value,
    variant = "primary",
    ...props
  }: Readonly<IIconButtonProps>,
  ref: Ref<HTMLButtonElement>,
): JSX.Element {
  const btn = (
    <StyledIconButton
      $borderRadius={borderRadius}
      $px={px}
      $py={py}
      $variant={variant}
      aria-disabled={disabled}
      aria-label={id}
      borderRadius={borderRadius}
      disabled={disabled}
      id={id}
      justify={justify}
      onClick={onClick}
      ref={ref}
      type={type}
      value={value}
      {...props}
    >
      <Icon
        clickable={false}
        icon={icon}
        iconColor={iconColor}
        iconSize={iconSize}
        iconTransform={iconTransform}
        iconType={iconType}
      />
    </StyledIconButton>
  );

  const tag = type + (variant ?? "") + (id ?? icon);
  const formattedTag = tag.replace(/ /u, "_");

  if (tooltip === undefined) {
    return btn;
  }

  return (
    <Tooltip id={`${formattedTag}-tooltip`} place={tooltipPlace} tip={tooltip}>
      {btn}
    </Tooltip>
  );
});

export { IconButton };
