import { styled } from "styled-components";

import { TSpacing } from "components/@core";
import { StyledButton } from "components/button/styles";

const StyledIconButton = styled(StyledButton)<{
  $borderRadius?: string;
  $px?: TSpacing;
  $py?: TSpacing;
}>`
  ${({ theme, $borderRadius = "4px", $px = 0.5, $py = 0.5 }): string => `
    --btn-spacing: ${$borderRadius};
    --btn-padding-x: ${theme.spacing[$px]};
    --btn-padding-y: ${theme.spacing[$py]};

    align-items: center;
    line-height: 0;
    text-align: center;

    > span {
      margin: 0 !important;
    }
  `}
`;

export { StyledIconButton };
