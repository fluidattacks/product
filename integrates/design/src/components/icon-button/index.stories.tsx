import type { Meta, StoryFn, StoryObj } from "@storybook/react";
import { expect, within } from "@storybook/test";
import type { PropsWithChildren } from "react";

import type { IIconButtonProps } from "./types";

import { IconButton } from ".";
import { CustomThemeProvider } from "../colors";

const config: Meta = {
  component: IconButton,
  parameters: {
    controls: {
      exclude: ["id", "onClick"],
    },
  },
  tags: ["autodocs"],
  title: "Components/IconButton",
};

const Template: StoryFn<PropsWithChildren<IIconButtonProps>> = (
  props,
): JSX.Element => <IconButton {...props} />;

const Default = Template.bind({});
Default.args = {
  disabled: false,
  icon: "plus",
  variant: "primary",
};
Default.play = async ({ canvasElement, step }): Promise<void> => {
  const canvas = within(canvasElement);

  await step("should render the icon button", async () => {
    await expect(canvas.getByRole("button")).toBeInTheDocument();
    await expect(canvas.getByTestId("plus-icon")).toBeInTheDocument();
  });
};

const ManyTemplate: StoryObj<{
  items: PropsWithChildren<IIconButtonProps>[];
}> = {
  render: ({ items }): JSX.Element => {
    return (
      <CustomThemeProvider>
        <div
          style={{
            display: "flex",
            justifyContent: "space-between",
            maxWidth: "300px",
            width: "200px",
          }}
        >
          {items.map(
            (item): JSX.Element => (
              <IconButton key={item.icon} {...item} />
            ),
          )}
        </div>
      </CustomThemeProvider>
    );
  },
};

const ManyButtons = {
  ...ManyTemplate,
  args: {
    items: [
      { ...Default.args },
      {
        disabled: false,
        icon: "search",
        variant: "secondary",
      },
      {
        disabled: false,
        icon: "bell",
        variant: "tertiary",
      },
      {
        disabled: true,
        icon: "warning",
        variant: "primary",
      },
    ],
  },
};

export { Default, ManyButtons };
export default config;
