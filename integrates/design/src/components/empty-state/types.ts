import type { DefaultTheme } from "styled-components";

type TSize = "md" | "sm";

/**
 * Button empty state props.
 * @interface IButtonProps
 * @property { string } text - Button text.
 * @property { Function } [onClick] - Button click handler.
 */
interface IButtonProps {
  onClick?: () => void;
  text: string;
}

/**
 * Empty state component props.
 * @interface IEmptyProps
 * @property { IButtonProps } [cancelButton] - Cancel button props.
 * @property { IButtonProps } [confirmButton] - Confirm button props.
 * @property { string } description - Empty state description.
 * @property { string } imageSrc - Empty state image source.
 * @property { DefaultTheme["spacing"] } [padding] - Empty state padding.
 * @property { string } [question] - Question text.
 * @property { TSize } [size] - Empty state size.
 * @property { string } title - Empty state title.
 */
interface IEmptyProps {
  cancelButton?: IButtonProps;
  confirmButton?: IButtonProps;
  description: string;
  imageSrc?: string;
  padding?: keyof DefaultTheme["spacing"];
  question?: string;
  size?: TSize;
  title: string;
}

export type { IEmptyProps, IButtonProps };
