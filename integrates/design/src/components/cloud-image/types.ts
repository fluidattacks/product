import type { Plugins } from "@cloudinary/html";

/**
 * Cloud image component props.
 * @interface ICloudImageProps
 * @property {string} [alt] - The alternate text for an image.
 * @property {number | string} [height] - The height of the image.
 * @property {number | string} [width] - The width of the image.
 * @property {Plugins} [plugins] - A list of cloudinary plugins.
 * @property {string} publicId - The publicId location in cloudinary.
 */
interface ICloudImageProps {
  alt?: string;
  height?: number | string;
  width?: number | string;
  plugins?: Plugins;
  publicId: string;
}

export type { ICloudImageProps };
