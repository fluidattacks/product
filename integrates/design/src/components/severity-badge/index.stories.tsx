import type { Meta, StoryFn } from "@storybook/react";
import React from "react";

import type { ISeverityBadgeProps } from "./types";

import { SeverityBadge } from ".";
import { Container } from "../container";

const config: Meta = {
  component: SeverityBadge,
  tags: ["autodocs"],
  title: "Components/SeverityBadge",
};

const Template: StoryFn<ISeverityBadgeProps> = (props): JSX.Element => (
  <SeverityBadge {...props} />
);

const Default = Template.bind({});

Default.args = {
  textL: "9.0",
  textR: "Text",
  variant: "critical",
};

const Priority: React.FC = (): JSX.Element => (
  <Container>
    <Container display={"flex"} gap={1} mb={1}>
      <Template textR={"Critical"} variant={"critical"} />
      <Template textR={"High"} variant={"high"} />
      <Template textR={"Medium"} variant={"medium"} />
      <Template textR={"Low"} variant={"low"} />
      <Template textR={"Disable"} variant={"disable"} />
    </Container>
  </Container>
);

export { Default, Priority };
export default config;
