import { useTheme } from "styled-components";

import { ISeverityBadgeProps } from "./types";
import { variants } from "./utils";

import { Container } from "components/container";
import { Text } from "components/typography";

const SeverityBadge = ({
  textL,
  textR,
  variant,
}: Readonly<ISeverityBadgeProps>): JSX.Element => {
  const theme = useTheme();
  const { bgColor, colorL, colorR } = variants[variant];
  return (
    <Container alignItems={"flex-start"} display={"inline-flex"}>
      {textL ? (
        <Container
          alignItems={"center"}
          bgColor={theme.palette.white}
          border={`1px solid ${bgColor}`}
          borderRadius={"4px 0px 0px 4px"}
          display={"flex"}
          justify={"center"}
          padding={[0, 0.25, 0, 0.25]}
        >
          <Text color={colorL} size={"xs"}>
            {textL}
          </Text>
        </Container>
      ) : undefined}
      <Container
        alignItems={"center"}
        bgColor={bgColor}
        border={`1px solid ${bgColor}`}
        borderRadius={textL ? "0px 4px 4px 0px" : "4px"}
        display={"flex"}
        padding={textL ? [0, 0.25, 0, 0.125] : [0, 0.25, 0, 0.25]}
        pb={0.125}
        pl={0.125}
        pr={0.125}
        pt={0.125}
      >
        <Text color={colorR} size={"xs"}>
          {textR}
        </Text>
      </Container>
    </Container>
  );
};

export { SeverityBadge };
