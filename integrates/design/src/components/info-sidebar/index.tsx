import { PropsWithChildren } from "react";
import { useTheme } from "styled-components";

import { IInfoSidebarProps } from "./types";

import { Button } from "components/button";
import { Container } from "components/container";
import { IconButton } from "components/icon-button";
import { Tabs } from "components/tabs";
import { Text } from "components/typography";

const InfoSidebar = ({
  actions,
  children,
  header,
  title,
  tabs,
  footer,
}: Readonly<PropsWithChildren<IInfoSidebarProps>>): JSX.Element => {
  const theme = useTheme();

  return (
    <Container
      alignItems={"flex-start"}
      bgColor={theme.palette.white}
      borderLeft={`1px solid ${theme.palette.gray[300]}`}
      display={"flex"}
      flexDirection={"column"}
      maxHeight={"750px"}
      width={"480px"}
    >
      <Container
        alignItems={"flex-start"}
        alignSelf={"stretch"}
        borderBottom={`1px solid ${theme.palette.gray[300]}`}
        display={"flex"}
        gap={0.25}
        justify={"space-between"}
        px={1.25}
        py={1}
      >
        <Container display={"flex"} flexDirection={"column"} gap={0.25}>
          <Text
            color={theme.palette.gray[800]}
            fontWeight={"bold"}
            lineSpacing={1.75}
            size={"xl"}
          >
            {title}
          </Text>
          {header}
        </Container>
        {actions && (
          <Container display={"flex"} gap={0.25}>
            {actions.map(
              ({ icon, onClick }, index): JSX.Element => (
                <IconButton
                  icon={icon ?? "paper-plane"}
                  iconColor={theme.palette.gray[400]}
                  iconSize={"xxs"}
                  iconType={"fa-light"}
                  key={`${index}-${icon}`}
                  onClick={onClick}
                  variant={"ghost"}
                />
              ),
            )}
          </Container>
        )}
      </Container>
      <Container display={"flex"} pt={0.25} width={"100%"}>
        <Tabs items={tabs} />
      </Container>
      <Container
        alignSelf={"stretch"}
        display={"flex"}
        flexDirection={"column"}
        px={1.25}
        py={1}
        scroll={"y"}
      >
        {children}
      </Container>
      {footer && (
        <Container
          alignSelf={"stretch"}
          borderTop={`1px solid ${theme.palette.gray[300]}`}
          display={"flex"}
          gap={0.5}
          justify={"end"}
          px={1.25}
          py={1}
        >
          {footer.map(
            ({ icon, text, onClick }, index): JSX.Element => (
              <Button
                icon={icon}
                key={`${index}-${text}`}
                onClick={onClick}
                variant={index === 0 ? "primary" : "tertiary"}
                width={"100%"}
              >
                {text}
              </Button>
            ),
          )}
        </Container>
      )}
    </Container>
  );
};

export { InfoSidebar };
