import type { Meta, StoryFn } from "@storybook/react";
import { PropsWithChildren } from "react";
import { MemoryRouter } from "react-router-dom";

import type { IInfoSidebarProps } from "./types";

import { InfoSidebar } from ".";
import { Container } from "../container";
import { Divider } from "../divider";
import { PriorityScore } from "../priority-score";
import { SeverityBadge } from "../severity-badge";
import { Tag } from "../tag";

const config: Meta = {
  component: InfoSidebar,
  tags: ["autodocs"],
  title: "Components/InfoSidebar",
};

const Template: StoryFn<PropsWithChildren<IInfoSidebarProps>> = (
  props,
): JSX.Element => {
  return (
    <MemoryRouter initialEntries={["/tab-1"]}>
      <InfoSidebar {...props}>
        <Container height={"800px"}></Container>
      </InfoSidebar>
    </MemoryRouter>
  );
};

const Default = Template.bind({});
Default.args = {
  actions: [
    {
      icon: "copy",
      onClick: (): void => {
        "test click";
      },
    },
    {
      icon: "paper-plane",
      onClick: (): void => {
        "test click";
      },
    },
  ],
  footer: [
    {
      icon: "wrench",
      id: "button-1",
      onClick: (): void => {
        "test click";
      },
      text: "Primary",
    },
    {
      id: "button-2",
      onClick: (): void => {
        "test click";
      },
      text: "Tertiary",
    },
  ],
  header: (
    <Container alignItems={"center"} display={"flex"} gap={0.5} height={"21px"}>
      <Tag priority={"low"} tagLabel={"Vulnerable"} variant={"error"} />
      <Divider height={"100%"} width={"1px"} />
      <PriorityScore score={36.33} />
      <Divider height={"100%"} width={"1px"} />
      <SeverityBadge textL={"9.0"} textR={"Critical"} variant={"critical"} />
    </Container>
  ),
  tabs: [
    {
      id: "tab-1",
      label: "Tab 1",
      link: "/tab-1",
    },
    {
      id: "tab-2",
      label: "Tab 2",
      link: "/tab-2",
    },
    {
      id: "tab-3",
      label: "Tab 3",
      link: "/tab-3",
    },
  ],
  title: "Name",
};

export { Default };
export default config;
