import { styled } from "styled-components";

import type { INotificationSignProps } from "./types";

import { BaseComponent } from "components/@core";

interface IStyledNotificationSignProps {
  $variant?: INotificationSignProps["variant"];
}

const NotificationSignButton = styled(
  BaseComponent,
)<IStyledNotificationSignProps>`
  ${({ theme, $variant }): string => `
    align-items: center;
    background: ${
      $variant === "error"
        ? theme.palette.primary[400]
        : theme.palette.warning[500]
    };
    border-radius: 100%;
    font-family: ${theme.typography.type.primary};
    justify-content: center;
    padding: 0;
    position: absolute;
    z-index: 11;
    width: ${theme.spacing[0.5]};
    height: ${theme.spacing[0.5]};
  `}
`;

const NumberIndicator = styled.div`
  align-items: center;
  color: ${({ theme }): string => theme.palette.white};
  display: flex;
  font-size: 6px;
  font-weight: ${({ theme }): string => theme.typography.weight.bold};
  justify-content: center;
`;

export { NotificationSignButton, NumberIndicator };
