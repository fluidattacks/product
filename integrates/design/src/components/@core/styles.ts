import { styled } from "styled-components";

import type { TModifiable } from "./types";
import {
  getStyledConfig,
  setBorder,
  setDisplay,
  setInteraction,
  setMargin,
  setPadding,
  setPosition,
  setText,
} from "./utils";

const BaseComponent = styled.div.withConfig(getStyledConfig)<TModifiable>`
  ${setPadding}
  ${setMargin}
  ${setPosition}
  ${setDisplay}
  ${setText}
  ${setBorder}
  ${setInteraction}
`;

const BaseButtonComponent = styled.button.withConfig(
  getStyledConfig,
)<TModifiable>`
  ${setPadding}
  ${setMargin}
  ${setPosition}
  ${setDisplay}
  ${setText}
  ${setBorder}
  ${setInteraction}
`;

const BaseTextComponent = styled.p.withConfig(getStyledConfig)<TModifiable>`
  ${setPadding}
  ${setMargin}
  ${setPosition}
  ${setDisplay}
  ${setText}
  ${setBorder}
  ${setInteraction}
`;

const BaseSpanComponent = styled.span.withConfig(getStyledConfig)<TModifiable>`
  ${setPadding}
  ${setMargin}
  ${setPosition}
  ${setDisplay}
  ${setText}
  ${setBorder}
  ${setInteraction}
`;

export {
  BaseComponent,
  BaseButtonComponent,
  BaseTextComponent,
  BaseSpanComponent,
};
