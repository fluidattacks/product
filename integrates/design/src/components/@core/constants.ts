const hiddenProps = [
  // IPaddingModifiable
  "padding",
  "px",
  "py",
  "pl",
  "pr",
  "pt",
  "pb",

  // IMarginModifiable
  "margin",
  "mx",
  "my",
  "ml",
  "mr",
  "mt",
  "mb",

  // IPositionModifiable
  "zIndex",
  "position",
  "top",
  "right",
  "bottom",
  "left",

  // IBorderModifiable
  "border",
  "borderTop",
  "borderRight",
  "borderBottom",
  "borderLeft",
  "borderColor",
  "borderRadius",

  // IDisplayModifiable
  "scroll",
  "visibility",
  "display",
  "height",
  "width",
  "maxHeight",
  "maxWidth",
  "minHeight",
  "minWidth",
  "shadow",
  "bgColor",
  "bgGradient",
  "gap",
  "flexDirection",
  "flexGrow",
  "justify",
  "justifySelf",
  "alignItems",
  "alignSelf",
  "wrap",

  // ITextModifiable
  "color",
  "fontFamily",
  "fontSize",
  "fontWeight",
  "textAlign",
  "whiteSpace",
  "letterSpacing",
  "lineSpacing",
  "textDecoration",
  "textOverflow",
  "whiteSpace",
  "wordBreak",
  "wordWrap",

  // IInteractionModifiable
  "cursor",
  "transition",
  "borderColorHover",
  "bgColorHover",
  "shadowHover",
];

export { hiddenProps };
