import { styled } from "styled-components";

const CarouselContainer = styled.div`
  position: relative;
  margin: auto;
  overflow: hidden;
`;

const CarouselWrapper = styled.div<{ $currentIndex: number }>`
  display: flex;
  transition: transform 0.5s ease-in-out;
  transform: translateX(
    ${({ $currentIndex }): number => -$currentIndex * 100}%
  );
`;

const CarouselSlide = styled.div`
  align-items: center;
  display: flex;
  justify-content: center;
  min-width: 100%;
`;

const NavButton = styled.button<{ $isActive: boolean }>`
  ${({ theme, $isActive }): string => `
    background-color: ${
      $isActive ? theme.palette.primary[400] : theme.palette.white
    };
    border: none;
    border-radius: 50%;
    cursor: pointer;
    height: ${theme.spacing[0.75]};
    width: ${theme.spacing[0.75]};
  `}
`;

const NavContainer = styled.div`
  align-items: center;
  display: flex;
  gap: ${({ theme }): string => theme.spacing[0.5]};
  justify-content: center;
  margin-top: ${({ theme }): string => theme.spacing[2]};
`;

export {
  CarouselContainer,
  CarouselWrapper,
  CarouselSlide,
  NavContainer,
  NavButton,
};
