/**
 * Slide component props.
 * @interface ISlide
 * @property {JSX.Element} content - The content to be displayed in the slide.
 */
interface ISlide {
  content: JSX.Element;
}

/**
 * Carousel component props.
 * @interface ICarouselProps
 * @property {ISlide[]} slides - The array of slides to be displayed in the carousel.
 * @property {number} [interval] - The interval (in milliseconds) between slide transitions. Default is 300.
 */
interface ICarouselProps {
  slides: ISlide[];
  interval?: number;
}

export type { ICarouselProps, ISlide };
