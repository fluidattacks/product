import type { Meta, StoryFn } from "@storybook/react";
import React from "react";
import { styled } from "styled-components";

import type { ICarouselProps } from "./types";

import { Carousel } from ".";
import { FilePreview } from "../file-preview";

const SlideContent = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  height: 300px;
  font-size: 2rem;
  color: white;
  background-color: #667085;
`;

const config: Meta = {
  component: Carousel,
  tags: ["autodocs"],
  title: "Components/Carousel",
};

const Template: StoryFn<React.PropsWithChildren<ICarouselProps>> = (
  props,
): JSX.Element => (
  <div className={"flex center h-90 bg-black rounded-xl p-8 w-[400px]"}>
    <Carousel {...props} />
  </div>
);

const src =
  "https://res.cloudinary.com/fluid-attacks/image/upload/f_auto,q_auto/v1/airs/blogs/blogs-cta?_a=AXCkERR0";

const createSlides = (count: number): { content: JSX.Element }[] =>
  Array.from({ length: count }, (_, index): { content: JSX.Element } => ({
    content: (
      <SlideContent key={index}>
        <FilePreview
          alt={"testing"}
          fileType={"image"}
          height={"147px"}
          src={src}
          width={"344px"}
        />
      </SlideContent>
    ),
  }));

const slides = createSlides(3);

const Default = Template.bind({});
Default.args = {
  interval: 50,
  slides,
};

export { Default };
export default config;
