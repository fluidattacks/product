import { styled } from "styled-components";

const ParentContainer = styled.div`
  ${({ theme }): string => `
    border-radius: ${theme.spacing[0.25]};
    cursor: pointer;
    display: flex;
    gap: ${theme.spacing[0.5]};
    justify-content: end;
    position: relative;
    z-index: 20;

    &:hover {
      background-color: ${theme.palette.gray[100]};
    }

    &:hover > div {
      display: block;
    }
  `}
`;

export { ParentContainer };
