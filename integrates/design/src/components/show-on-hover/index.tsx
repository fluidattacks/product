import { ParentContainer } from "./styles";
import type { IShowOnHoverProps } from "./types";

import { Container } from "components/container";

const ShowOnHover = ({
  visibleElement,
  hiddenElement,
}: Readonly<IShowOnHoverProps>): JSX.Element => (
  <ParentContainer>
    {visibleElement}
    <Container display={"none"} position={"absolute"} top={"100%"} zIndex={10}>
      {hiddenElement}
    </Container>
  </ParentContainer>
);

export { ShowOnHover };
