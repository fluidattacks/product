import type { ReactNode } from "react";

/**
 * Show on hover component props.
 * @interface IShowOnHoverProps
 * @property { ReactNode } visibleElement - The element to show by default.
 * @property { ReactNode } hiddenElement - The element to show when hovering.
 */
interface IShowOnHoverProps {
  visibleElement: ReactNode;
  hiddenElement: ReactNode;
}

export type { IShowOnHoverProps };
