import type { DefaultTheme } from "styled-components";
import { styled } from "styled-components";

import type { TVariant } from "./types";

import {
  BaseButtonComponent,
  type ITextModifiable,
  variantBuilder,
} from "components/@core";

interface IStyledButtonProps {
  $size?: ITextModifiable["fontSize"];
  $variant?: TVariant;
}

const { getVariant } = variantBuilder(
  (theme: DefaultTheme): Record<TVariant, string> => ({
    ghost: `
      background: transparent;
      border: none;
      color: ${theme.palette.gray[800]};
      --btn-padding-x: ${theme.spacing[0.5]};
      --btn-padding-y: ${theme.spacing[0.625]};
      --btn-spacing: ${theme.spacing[0.25]};

      &:disabled {
        color: ${theme.palette.gray[400]};
        cursor: not-allowed;
      }

      &:hover:not([disabled]) {
        background-color: ${theme.palette.gray[100]};
        color: ${theme.palette.gray[800]};
      }
    `,
    link: `
      --btn-padding-x: 0;
      --btn-padding-y: 0;
      color: ${theme.palette.gray[800]};

      &:hover {
        color: ${theme.palette.gray[500]};
      }
    `,
    primary: `
      background: ${theme.palette.primary[500]};
      border: none;
      color: ${theme.palette.white};
      --btn-spacing: ${theme.spacing[0.25]};

      &:disabled {
        background: ${theme.palette.gray[200]};
        color: ${theme.palette.gray[400]};
        cursor: not-allowed;
      }

      &:hover:not([disabled]) {
        background-color: ${theme.palette.primary[700]};
        color: ${theme.palette.white};
      }
    `,
    secondary: `
      background: ${theme.palette.gray[800]};
      border: none;
      color: ${theme.palette.white};
      --btn-spacing: ${theme.spacing[0.25]};

      &:disabled {
        background: ${theme.palette.gray[200]};
        color: ${theme.palette.gray[400]};
        cursor: not-allowed;
      }

      &:hover:not([disabled]) {
        background-color: ${theme.palette.gray[600]};
        color: ${theme.palette.white};
      }
    `,
    tertiary: `
      background: transparent;
      border: 1px solid ${theme.palette.primary[500]};
      color: ${theme.palette.primary[500]};
      --btn-spacing: ${theme.spacing[0.25]};

      &:disabled {
        background: transparent;
        border: 1px solid ${theme.palette.gray[200]};
        color: ${theme.palette.gray[300]};
        cursor: not-allowed;
      }

      &:hover:not([disabled]) {
        background-color: ${theme.palette.primary[500]};
        color: ${theme.palette.white};
      }
    `,
  }),
);

const StyledButton = styled(BaseButtonComponent)<IStyledButtonProps>`
  ${({ theme, justify, $size, $variant }): string => `
    align-items: center;
    border-radius: var(--btn-spacing);
    cursor: pointer;
    display: inline-flex;
    font-family: ${theme.typography.type.primary};
    font-size: ${$size ?? theme.typography.text.sm};
    font-weight: ${theme.typography.weight.regular};
    gap: var(--btn-spacing);
    justify-content: ${justify ?? "center"};
    line-height: ${theme.spacing[1.25]};
    padding: var(--btn-padding-y) var(--btn-padding-x);
    position: relative;
    text-align: start;
    transition: all 0.5s ease;
    white-space: nowrap;

    @media screen and (max-width: ${theme.breakpoints.mobile}) {
      ${$variant !== "link" ? `--btn-padding-y: ${theme.spacing[1]}` : ""};
    }

    ${getVariant(theme, $variant ?? "primary")}
  `}
`;

export type { IStyledButtonProps, TVariant };
export { StyledButton };
