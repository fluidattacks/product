import type { Meta, StoryFn } from "@storybook/react";
import {
  expect,
  fn,
  screen,
  userEvent,
  waitFor,
  within,
} from "@storybook/test";
import type { PropsWithChildren } from "react";

import type { IButtonProps } from ".";
import { Button } from ".";

const config: Meta = {
  argTypes: {
    id: {
      control: {
        disable: true,
      },
    },
  },
  component: Button,
  parameters: {
    a11y: { disable: true },
    controls: {
      exclude: ["id", "onClick"],
    },
  },
  tags: ["autodocs"],
  title: "Components/Button",
};

const onClick = fn();
const Template: StoryFn<PropsWithChildren<IButtonProps>> = (
  props,
): JSX.Element => <Button {...props} />;

const GrayTemplate: StoryFn<PropsWithChildren<IButtonProps>> = (
  props,
): JSX.Element => <Button {...props} />;

const Default = Template.bind({});
Default.args = {
  children: "Button text",
  disabled: false,
  variant: "primary",
};

const Secondary = Template.bind({});
Secondary.args = {
  children: "Button text",
  disabled: false,
  icon: "gear",
  onClick: onClick,
  showArrow: true,
  variant: "secondary",
};
Secondary.play = async ({ canvasElement, step }): Promise<void> => {
  const canvas = within(canvasElement);
  const button = canvas.getByRole("button", { name: "Button text" });

  await step("should render a Button", async (): Promise<void> => {
    await expect(button).toBeInTheDocument();
  });

  await step("should be clickable", async (): Promise<void> => {
    await userEvent.click(button);
    await waitFor(async (): Promise<void> => {
      await expect(onClick).toHaveBeenCalledTimes(1);
    });
  });
};

const Tertiary = Template.bind({});
Tertiary.args = {
  children: "Button text",
  disabled: true,
  onClick: onClick,
  variant: "tertiary",
};
Tertiary.play = async ({ canvasElement, step }): Promise<void> => {
  const canvas = within(canvasElement);
  const button = canvas.getByRole("button", { name: "Button text" });

  await step("should render a Button", async (): Promise<void> => {
    await expect(button).toBeInTheDocument();
  });

  await step(
    "should not trigger onClick when button is disabled",
    async (): Promise<void> => {
      await userEvent.click(button);
      await waitFor(async (): Promise<void> => {
        await expect(onClick).not.toHaveBeenCalled();
      });
    },
  );
};

const Ghost = GrayTemplate.bind({});
Ghost.args = {
  children: "Button text",
  disabled: false,
  tooltip: "This is a tooltip",
  variant: "ghost",
  icon: "gear",
  showArrow: true,
  tag: "1",
};
Ghost.play = async ({ canvasElement, step }): Promise<void> => {
  const canvas = within(canvasElement);
  const button = canvas.getByRole("button", { name: "Button text 1" });

  await step("should render a Button", async (): Promise<void> => {
    await expect(button).toBeInTheDocument();
  });

  await step(
    "should render tooltip on Button hover",
    async (): Promise<void> => {
      await userEvent.hover(button);
      await waitFor(async (): Promise<void> => {
        await expect(screen.getByText("This is a tooltip")).toBeInTheDocument();
      });
    },
  );
};

const IconOnly = GrayTemplate.bind({});
IconOnly.args = {
  disabled: false,
  variant: "ghost",
  icon: "filter",
  tag: "1",
};

export { Default, Secondary, Tertiary, Ghost, IconOnly };
export default config;
