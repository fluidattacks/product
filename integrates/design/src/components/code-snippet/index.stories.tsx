import type { Meta, StoryFn } from "@storybook/react";

import type { ICodeSnippetProps } from "./types";

import { CodeSnippet } from ".";

const config: Meta = {
  component: CodeSnippet,
  tags: ["autodocs"],
  title: "Components/CodeSnippet",
};

const Template: StoryFn<ICodeSnippetProps> = ({
  snippet,
  specific,
  variant,
}: Readonly<ICodeSnippetProps>): JSX.Element => (
  <CodeSnippet snippet={snippet} specific={specific} variant={variant} />
);

const Default = Template.bind({});

Default.args = {
  snippet: `function cool(x) {
  /*Use brief comments in English only when necessary.
  We must explain our code in the document.*/
  int y;
  y = x + 1;
  return y;
  //And don't forget this: no more than eight lines.
}
function cool(x) {
  /*Use brief comments in English only when necessary.
  We must explain our code in the document.*/
  int y;
  y = x + 1;
  return y;
  //And don't forget this: no more than eight lines.
}`,
};

const Location = Template.bind({});

Location.args = {
  snippet: {
    content: `      - "5000:5000"
    volumes:\n      - .:/code\n      - logvolume01:/var/log
    links:\n      - redis
    environment:\n      - DEBUG=\${VAR}\n      - API_KEY='123'\n      - API_KEY\n      - API_KEY_CLOUD_CLIENT_SECRET=\${APIKEY_CLIENT_SECRET}
    redis:\n       image: redis\n       environment:\n         - API_NAME=name1
    db:\n      image: "postgres:\${POSTGRES_VERSION}"\n      environment:\n         - API_PASSWORD=pass
    volumes:\n        logvolume01: {}`,
    offset: 5,
  },
  specific: "17",
  variant: "location",
};

const NoIdentation = Template.bind({});

NoIdentation.args = {
  snippet: {
    content: `function cool(x) {
  /*Use brief comments in English only when necessary.
  We must explain our code in the document.*/
  int y;
  y = x + 1;
  return y;
  //And don't forget this: no more than eight lines.
}
function cool(x) {
  /*Use brief comments in English only when necessary.
  We must explain our code in the document.*/
  int y;
  y = x + 1;
  return y;
  //And don't forget this: no more than eight lines.
}`,
    offset: 0,
  },
  specific: "4",
  variant: "location",
};

export default config;
export { Default, Location, NoIdentation };
