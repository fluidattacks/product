import _ from "lodash";
import { useCallback, useMemo, useState } from "react";
import { useTheme } from "styled-components";

import { LocationCode } from "./location-code";
import { ButtonsContainer, CodeContainer } from "./styles";
import type { ICodeSnippetProps } from "./types";

import { Button } from "components/button";
import { Container } from "components/container";
import { Icon } from "components/icon";
import { IconButton } from "components/icon-button";

const CodeSnippet = ({
  noCodeMessage = "Code snippet not available",
  snippet,
  specific,
  variant = "normal",
}: Readonly<ICodeSnippetProps>): JSX.Element => {
  const theme = useTheme();
  const [showAll, setShowAll] = useState(false);
  const [showButton, setShowButton] = useState(true);

  const toggleShowMore = useCallback((): void => {
    setShowAll((previousState): boolean => !previousState);
  }, [setShowAll]);

  const fragment = useCallback((): string => {
    if (_.isNil(snippet) || _.isEmpty(snippet) || !_.isString(snippet)) {
      setShowButton(false);

      return noCodeMessage;
    }

    const contentAsList = snippet.split("\n");
    if (contentAsList.length > 8 && !showAll) {
      return `${[...contentAsList].splice(0, 7).join("\n")}\n${
        contentAsList[8]
      }...`;
    }

    if (contentAsList.length < 8) setShowButton(false);

    return snippet;
  }, [showAll, snippet, noCodeMessage]);

  const codeContent = useMemo(fragment, [fragment]);

  const copy = useCallback(async (): Promise<void> => {
    await navigator.clipboard.writeText(codeContent);
  }, [codeContent]);

  if (variant === "location") {
    return (
      <LocationCode
        noCodeMessage={noCodeMessage}
        snippet={snippet}
        specific={specific}
      />
    );
  }

  return (
    <Container
      bgColor={theme.palette.gray[100]}
      borderRadius={"4px"}
      display={"inline-flex"}
      minWidth={"565px"}
      position={"relative"}
    >
      <CodeContainer data-private={true}>
        <pre>
          <code className={"language-none"}>{codeContent}</code>
        </pre>
      </CodeContainer>
      <ButtonsContainer>
        <IconButton
          height={theme.spacing[2.5]}
          icon={"copy"}
          iconColor={theme.palette.gray[700]}
          iconSize={"xs"}
          iconType={"fa-light"}
          onClick={copy}
          variant={"ghost"}
          width={theme.spacing[2.5]}
        />
        {showButton ? (
          <Button onClick={toggleShowMore} variant={"ghost"}>
            {showAll ? "Show Less" : "Show more"}
            <Icon
              icon={showAll ? "chevron-up" : "chevron-down"}
              iconColor={theme.palette.gray[700]}
              iconSize={"xxs"}
              iconType={"fa-light"}
              ml={0.25}
              onClick={copy}
            />
          </Button>
        ) : (
          false
        )}
      </ButtonsContainer>
    </Container>
  );
};

export { CodeSnippet };
