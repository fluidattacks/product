import { styled } from "styled-components";

import { BaseComponent } from "components/@core";

interface IStyledContainerProps {
  $center?: boolean;
  $styleMd?: string;
  $styleSm?: string;
}

const StyledContainer = styled(BaseComponent)<IStyledContainerProps>`
  ${({ theme, $center = false, $styleMd, $styleSm }): string => `
    ${$center ? "margin: auto;" : ""}
    scrollbar-color: ${theme.palette.gray[600]} ${theme.palette.gray[100]};
    scroll-padding: ${theme.spacing[0.5]};

    @media screen
      and (min-width: ${theme.breakpoints.mobile})
      and (max-width: ${theme.breakpoints.tablet})
    {
      ${$styleMd};
    }

    @media screen and (max-width: ${theme.breakpoints.mobile}) {
      ${$styleSm};
    }
  `}
`;

export { StyledContainer };
