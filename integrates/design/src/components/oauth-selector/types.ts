import type { Property } from "csstype";
import type { MouseEventHandler } from "react";

import { IIconModifiable } from "components/@core";

type TOAuthProvider = "Azure" | "Bitbucket" | "GitHub" | "GitLab";

/**
 * Shared props for OAuth selector.
 * @interface ISharedProps
 * @property {string} [label] - Label to display on the button.
 * @property {Function} onClick - Function to be called when the button is clicked.
 */
interface ISharedProps {
  label?: string;
  onClick: MouseEventHandler<HTMLOrSVGElement>;
}

/**
 * Oauth selector component props.
 * @interface IOAuthSelectorProps
 * @property {string} [align] - Alignment of the button.
 * @property {string} [buttonLabel] - Label to display on the button.
 * @property {string} [id] - Id to be used on the button.
 * @property {ISharedProps} [manualOption] - Object with the label and onClick function for the manual option.
 * @property {Record} [providers] - Object with the providers and their onClick function.
 */
interface IOAuthSelectorProps {
  align?: Property.AlignItems;
  buttonLabel?: string;
  id?: string;
  manualOption?: ISharedProps;
  providers: Partial<
    Record<TOAuthProvider, { onClick: MouseEventHandler<HTMLOrSVGElement> }>
  >;
}

/**
 * Option container component props.
 * @interface IOptionContainerProps
 * @extends IIconModifiable
 * @extends ISharedProps
 * @property {boolean} [onlyLabel] - Whether to only display the label.
 * @property {TOAuthProvider} [provider] - Provider to display.
 */
interface IOptionContainerProps
  extends Partial<Pick<IIconModifiable, "icon">>,
    ISharedProps {
  onlyLabel?: boolean;
  provider?: TOAuthProvider;
}

export type { IOAuthSelectorProps, IOptionContainerProps, TOAuthProvider };
