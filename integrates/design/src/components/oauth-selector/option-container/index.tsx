import { useTheme } from "styled-components";

import type { IOptionContainerProps, TOAuthProvider } from "../types";
import { CloudImage } from "components/cloud-image";
import { Container } from "components/container";
import { Icon } from "components/icon";
import { Text } from "components/typography";

const providerImageSRC: Record<TOAuthProvider, string> = {
  Azure: "integrates/autoenrollment/azureIcon",
  Bitbucket: "integrates/autoenrollment/bitbucketLogo",
  GitHub: "integrates/autoenrollment/githubLogo",
  GitLab: "integrates/autoenrollment/gitlabIcon",
};

const OptionContainer = ({
  icon,
  label,
  onClick,
  onlyLabel = false,
  provider = "GitLab",
}: Readonly<IOptionContainerProps>): JSX.Element => {
  const theme = useTheme();

  return (
    <Container
      alignItems={"center"}
      bgColorHover={theme.palette.gray[100]}
      borderRadius={"4px"}
      cursor={"pointer"}
      display={"flex"}
      flexDirection={"column"}
      gap={0.25}
      height={"62px"}
      justify={"center"}
      minWidth={"64px"}
      onClick={onClick}
      pb={0.5}
      pl={0.25}
      pr={0.25}
      pt={0.5}
    >
      {onlyLabel ? (
        false
      ) : (
        <Container height={"24px"} width={"24px"}>
          {icon === undefined ? (
            <CloudImage
              alt={`${provider} Logo`}
              height={"100%"}
              publicId={providerImageSRC[provider]}
              width={"100%"}
            />
          ) : (
            <Icon
              icon={icon}
              iconColor={"#000"}
              iconSize={"sm"}
              iconType={"fa-light"}
            />
          )}
        </Container>
      )}
      <Container alignItems={"center"} display={"flex"} minWidth={"24px"}>
        <Text
          color={theme.palette.gray[800]}
          display={"inline"}
          size={"xs"}
          textAlign={"center"}
          whiteSpace={onlyLabel ? "break-spaces" : "nowrap"}
          wordWrap={"unset"}
        >
          {label ?? provider}
        </Text>
      </Container>
    </Container>
  );
};

export { OptionContainer };
