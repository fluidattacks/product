import type { Meta, StoryFn } from "@storybook/react";
import { PropsWithChildren } from "react";

import type { IOAuthSelectorProps } from "./types";

import { OAuthSelector, OptionContainer } from ".";

const config: Meta = {
  component: OAuthSelector,
  parameters: {
    controls: {
      exclude: ["onClick"],
    },
  },
  tags: ["autodocs"],
  title: "Components/OAuthSelector",
};

const Template: StoryFn<IOAuthSelectorProps> = ({
  buttonLabel,
  providers,
}: Readonly<PropsWithChildren<IOAuthSelectorProps>>): JSX.Element => (
  <div className={"flex justify-end"}>
    <OAuthSelector buttonLabel={buttonLabel} providers={providers}>
      <OptionContainer
        label={"Add root manually"}
        onClick={handleClick}
        onlyLabel={true}
      />
    </OAuthSelector>
  </div>
);

const handleClick = (): void => {
  "test";
};

const Default = Template.bind({});
Default.args = {
  manualOption: { label: "Add root manually", onClick: handleClick },
  providers: {
    Azure: { onClick: handleClick },
    Bitbucket: { onClick: handleClick },
    GitHub: { onClick: handleClick },
    GitLab: { onClick: handleClick },
  },
};

const AddRootOptions = Template.bind({});
AddRootOptions.args = {
  buttonLabel: "Add Root",
  providers: {
    GitHub: { onClick: handleClick },
    GitLab: { onClick: handleClick },
  },
};

export default config;
export { Default, AddRootOptions };
