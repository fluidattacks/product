import { yupResolver } from "@hookform/resolvers/yup";
import type { PropsWithChildren } from "react";
import {
  type FieldValues,
  FormProvider,
  useFieldArray,
  useForm,
  useFormContext,
  useWatch,
} from "react-hook-form";
import { useTheme } from "styled-components";

import { InnerForm } from "./inner-form";
import { StyledForm } from "./styles";
import type { IFormProps, TFormMethods } from "./types";

import { Button } from "components/button";
import { Container } from "components/container";

const Form = <T extends FieldValues = FieldValues>({
  alert,
  buttonAlignment,
  cancelButton,
  children,
  confirmButton,
  defaultDisabled = true,
  id = "modal-confirm",
  maxButtonWidth = "392px",
  mode,
  onSubmit,
  showButtons = true,
  yupSchema,
  ...props
}: Readonly<PropsWithChildren<IFormProps<T>>>): JSX.Element => {
  const theme = useTheme();
  const methods = useForm<T>({
    ...props,
    mode: mode ?? "all",
    resolver: yupSchema ? yupResolver(yupSchema) : undefined,
  });

  return (
    <FormProvider {...methods}>
      <StyledForm onSubmit={methods.handleSubmit(onSubmit)}>
        <Container scroll={"y"} width={"100%"}>
          <Container
            borderRadius={`0 0 ${theme.spacing[0.25]} ${theme.spacing[0.25]}`}
            display={"flex"}
            flexDirection={"column"}
            gap={0.5}
            height={alert ? "calc(100% - 139px)" : "calc(100% - 83px)"}
            pb={1.5}
            pt={0}
            px={1.5}
            scroll={"y"}
          >
            {children}
          </Container>
          {showButtons && (
            <Container
              borderRadius={`0 0 ${theme.spacing[0.25]} ${theme.spacing[0.25]}`}
              borderTop={`1px solid ${theme.palette.gray[200]}`}
              display={"flex"}
              flexDirection={buttonAlignment ? "unset" : "column"}
              gap={0.625}
              justify={buttonAlignment ?? "stretch"}
              position={"sticky"}
              px={1.25}
              py={1.25}
            >
              {alert}
              <Container display={"flex"} gap={0.75} maxWidth={maxButtonWidth}>
                {cancelButton ? (
                  <Button
                    icon={cancelButton.icon}
                    id={`${id}-cancel`}
                    justify={"center"}
                    onClick={cancelButton.onClick}
                    type={"reset"}
                    variant={"tertiary"}
                    width={"100%"}
                  >
                    {cancelButton.label ?? "Cancel"}
                  </Button>
                ) : undefined}
                <Button
                  disabled={
                    (confirmButton?.disabled ?? false) ||
                    methods.formState.isSubmitting ||
                    (defaultDisabled && !methods.formState.isDirty)
                  }
                  icon={confirmButton?.icon}
                  id={id}
                  justify={"center"}
                  type={"submit"}
                  width={"100%"}
                >
                  {confirmButton?.label ?? "Confirm"}
                </Button>
              </Container>
            </Container>
          )}
        </Container>
      </StyledForm>
    </FormProvider>
  );
};

export type { TFormMethods };
export { InnerForm, Form, useFormContext, useFieldArray, useWatch };
