import { FieldValues, useFormContext } from "react-hook-form";

import type { IInnerFormProps } from "../types";

export const InnerForm = <T extends FieldValues>({
  children,
}: IInnerFormProps<T>): JSX.Element => {
  const methods = useFormContext<T>();

  return children({ ...methods });
};
