import {
  FieldValues,
  SubmitHandler,
  UseFormProps,
  UseFormReturn,
} from "react-hook-form";

import { IIconModifiable } from "components/@core";

type TFormMethods<T extends FieldValues> = UseFormReturn<T>;

/**
 * Inner form props to allow passing methods to children.
 */
interface IInnerFormProps<T extends FieldValues> {
  children: (children: Readonly<TFormMethods<T>>) => React.ReactElement;
}

/**
 * Button component props.
 * @property { boolean } [disabled] - Whether the button is disabled.
 * @property { IconName } [icon] - The name of the icon to show.
 * @property { string } [label] - The label of the button.
 * @property { Function } [onClick] - The function to call when the button is clicked.
 */
interface IButtonProps {
  disabled?: boolean;
  icon?: IIconModifiable["icon"];
  label?: string;
  onClick?: () => void;
}

/**
 * Form component props.
 * @interface IFormProps
 * @property { JSX.Element } [alert] - The alert to display on footer.
 * @property { "end" } [buttonAlignment] - The button alignment.
 * @property { IButtonProps } [cancelButton] - The cancel button props.
 * @property { IButtonProps } [confirmButton] - The confirm button props.
 * @property { boolean } [defaultDisabled] - Whether the submit button is disabled by default.
 * @property { string } [id] - The form id.
 * @property { string } [maxButtonWidth] - The max button width.
 * @property { Function } onSubmit - The callback to be called when the form is submitted.
 * @property { boolean } [showButtons] - Whether to show the action buttons.
 * @property { Schema } [yupSchema] - The yup schema for form validation.
 */
interface IFormProps<T extends FieldValues> extends UseFormProps<T, unknown> {
  alert?: JSX.Element;
  buttonAlignment?: "end";
  cancelButton?: IButtonProps;
  confirmButton?: IButtonProps;
  defaultDisabled?: boolean;
  id?: string;
  maxButtonWidth?: string;
  onSubmit: SubmitHandler<T>;
  showButtons?: boolean;
  yupSchema?: unknown;
}

export type { IFormProps, IInnerFormProps, TFormMethods };
