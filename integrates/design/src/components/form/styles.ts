import { styled } from "styled-components";

const StyledForm = styled.form`
  ${({ theme }): string => `
    align-items: flex-start;
    align-self: stretch;
    display: flex;
    flex-direction: column;
    gap: ${theme.spacing[0.5]};
    overflow: hidden auto;
    width: 100%;
  `}
`;

export { StyledForm };
