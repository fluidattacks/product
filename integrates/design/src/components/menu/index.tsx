import { isUndefined } from "lodash";
import { PropsWithChildren } from "react";
import { useTheme } from "styled-components";

import type { IMenuProps } from "./types";

import { Container } from "components/container";
import { Divider } from "components/divider";
import { Link } from "components/link";
import { ListItemsWrapper } from "components/list-item";
import { Tag } from "components/tag";
import { Text } from "components/typography";
import { useClickOutside } from "hooks/use-click-outside";

const Menu = ({
  children,
  commitSha,
  commitShortSha,
  parentElement,
  userInfo,
  setVisibility,
}: Readonly<PropsWithChildren<IMenuProps>>): JSX.Element => {
  const theme = useTheme();
  const { userName, email, phone, userRole } = userInfo;

  useClickOutside(parentElement, (): void => {
    setVisibility(false);
  });

  return (
    <ListItemsWrapper>
      <li className={"user-info"}>
        <Container display={"inline-flex"} gap={0.25} width={"100%"}>
          <Container
            display={"flex"}
            flexDirection={"column"}
            gap={0.25}
            width={"100%"}
          >
            <Text
              color={theme.palette.gray[800]}
              fontWeight={"bold"}
              size={"sm"}
            >
              {userName}
            </Text>
            <Text color={theme.palette.gray[400]} size={"sm"}>
              {email}
            </Text>
            {isUndefined(phone) ? undefined : (
              <Text color={theme.palette.gray[400]} size={"sm"}>
                {phone}
              </Text>
            )}
          </Container>
          <Container>
            <Tag tagLabel={userRole} variant={"role"} />
          </Container>
        </Container>
      </li>
      <Divider />
      {children}
      <Divider />
      <li className={"commit-info"}>
        <Link
          href={`https://gitlab.com/fluidattacks/universe/-/tree/${commitSha}`}
          iconPosition={"hidden"}
        >
          <Text color={theme.palette.gray[400]} size={"xs"}>
            {`commit : ${commitShortSha}`}
          </Text>
        </Link>
      </li>
    </ListItemsWrapper>
  );
};

export { Menu };
