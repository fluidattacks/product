/**
 * User information interface.
 * @interface IUserInfo
 * @property {string} userName - User's name.
 * @property {string} email - User's email.
 * @property {number | string} [phone] - User's phone number.
 * @property {string} userRole - User's role.
 */
interface IUserInfo {
  userName: string;
  email: string;
  phone?: number | string;
  userRole: string;
}

/**
 * Menu component props.
 * @interface IMenuProps
 * @property {string} commitSha - The commit SHA of the integration.
 * @property { string } commitShortSha - The shortened commit SHA of the integration.
 * @property { HTMLElement | null } parentElement - The parent element of the menu.
 * @property { Function } setVisibility - Function to set the menu visibility.
 * @property { IUserInfo } userInfo - User information.
 */
interface IMenuProps {
  commitSha: string;
  commitShortSha: string;
  parentElement: HTMLElement | null;
  setVisibility: (visible: boolean) => void;
  userInfo: IUserInfo;
}

export type { IUserInfo, IMenuProps };
