import type { IconName } from "@fortawesome/free-solid-svg-icons";
import { useTheme } from "styled-components";

import { StyledTableButton } from "./styles";
import type { ITableButtonProps, TVariant } from "./types";

import { Container } from "components/container";
import { Icon } from "components/icon";
import { Text } from "components/typography";

const types: Record<TVariant, Record<string, IconName>> = {
  add: { icon: "plus" },
  approve: { icon: "check" },
  disabled: { icon: "ban" },
  reject: { icon: "xmark" },
  submit: { icon: "arrow-right" },
  success: { icon: "check" },
};

const TableButton = ({
  disabled = false,
  icon,
  id,
  label,
  name,
  onClick,
  type = "button",
  variant,
}: Readonly<ITableButtonProps>): JSX.Element => {
  const theme = useTheme();

  const btn = (
    <StyledTableButton
      aria-label={name ?? undefined}
      className={variant}
      disabled={disabled}
      id={id}
      onClick={onClick}
      type={type}
    >
      <Icon
        clickable={false}
        icon={icon ?? types[variant].icon}
        iconSize={name === undefined ? "xxs" : "xs"}
        iconType={"fa-light"}
      />
      {name ?? undefined}
    </StyledTableButton>
  );

  return (
    <Container alignItems={"center"} display={"flex"}>
      {btn}
      {label && (
        <Text color={theme.palette.gray[800]} ml={0.5} size={"sm"}>
          {label}
        </Text>
      )}
    </Container>
  );
};

export { TableButton };
