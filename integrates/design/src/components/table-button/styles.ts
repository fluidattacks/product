import { styled } from "styled-components";

const StyledTableButton = styled.button`
  ${({ theme }): string => `
    align-items: center;
    background-color: ${theme.palette.white};
    border: 1px solid ${theme.palette.gray[100]};
    border-radius: ${theme.spacing[0.25]};
    color: ${theme.palette.gray[800]};
    cursor: pointer;
    display: flex;
    font-size: ${theme.typography.text.sm};
    gap: ${theme.spacing[0.25]};
    height: 21px;
    width: 21px;
    justify-content: center;
    padding: 6px;
    transition: all 0.25s ease;

    &[aria-label] {
      height: auto;
      width: auto;
    }

    &.success {
      border-color: ${theme.palette.success[500]};
      color: ${theme.palette.success[500]};

      &:hover {
        background-color: ${theme.palette.success[200]};
      }
    }

    &:disabled {
      cursor: not-allowed;
      background: ${theme.palette.gray[200]};
      border-color: ${theme.palette.gray[200]};
      color: ${theme.palette.gray[300]};
    }

    &:hover:not(.success) {
      background-color: ${theme.palette.gray[100]};
    }
  `}
`;

export { StyledTableButton };
