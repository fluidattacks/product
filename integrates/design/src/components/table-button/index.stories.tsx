import type { Meta, StoryFn, StoryObj } from "@storybook/react";
import type { PropsWithChildren } from "react";

import type { ITableButtonProps } from "./types";

import { TableButton } from ".";
import { Container } from "../container";

const config: Meta = {
  component: TableButton,
  parameters: {
    controls: {
      exclude: ["onClick"],
    },
  },
  tags: ["autodocs"],
  title: "Components/TableButton",
};

const Template: StoryFn<ITableButtonProps> = (props): JSX.Element => (
  <TableButton {...props} />
);

const Default = Template.bind({});

Default.args = {
  type: "button",
  variant: "approve",
};

const ManyTemplate: StoryObj<{
  items: PropsWithChildren<ITableButtonProps>[];
}> = {
  render: ({ items }): JSX.Element => {
    return (
      <Container display={"flex"} justify={"space-between"} maxWidth={"200px"}>
        {items.map((item, index): JSX.Element => {
          const key = index;

          return <TableButton key={key} {...item} />;
        })}
      </Container>
    );
  },
};

const ManyButtons = {
  ...ManyTemplate,
  args: {
    items: [
      { icon: "check" },
      { icon: "xmark" },
      { icon: "arrow-right" },
      { icon: "ban" },
    ],
  },
};

const SpecialButtons = {
  ...ManyTemplate,
  args: {
    items: [
      { name: "Confirm", variant: "success" },
      { name: "Reject", icon: "xmark" },
    ],
  },
};

const WithLabel: StoryFn = (): JSX.Element => (
  <TableButton label={"Add"} type={"button"} variant={"add"} />
);

export { Default, ManyButtons, SpecialButtons, WithLabel };
export default config;
