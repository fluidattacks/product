import { useCallback, useState } from "react";

import {
  StepContainer,
  StepperContainer,
  StyledButtonsContainer,
  StyledStepLabel,
} from "./styles";
import type { IStepLapseProps } from "./types";

import { Button } from "components/button";
import { Container } from "components/container";
import { Icon } from "components/icon";
import { Heading } from "components/typography";

const StepLapse = ({
  button: { disabled = false, text, type = "button", onClick },
  steps,
}: Readonly<IStepLapseProps>): JSX.Element => {
  const [currentStep, setCurrentStep] = useState(1);

  const createStepHandler = useCallback(
    (direction: 1 | -1, action?: () => void): (() => void) =>
      // eslint-disable-next-line functional/functional-parameters
      (): void => {
        setCurrentStep((prevStep) => prevStep + direction);
        action?.();
      },
    [],
  );

  return (
    <StepperContainer>
      {steps.map((step, index): JSX.Element => {
        const { content, title, nextAction, previousAction } = step;
        const stepNumber = index + 1;
        const stepCheck = stepNumber === currentStep ? "current" : "disabled";
        const stepStatus = stepNumber < currentStep ? "completed" : stepCheck;

        return (
          <StepContainer key={step.title}>
            <Container maxWidth={"40px"} position={"relative"}>
              <StyledStepLabel $variant={stepStatus}>
                {stepStatus === "completed" ? (
                  <Icon icon={"check"} iconSize={"xs"} iconType={"fa-solid"} />
                ) : (
                  stepNumber
                )}
              </StyledStepLabel>
            </Container>
            <Container ml={1.5} width={"100%"}>
              <Heading
                fontWeight={"bold"}
                lineSpacing={1.25}
                mb={1.5}
                size={"xs"}
              >
                {title}
              </Heading>
              {stepStatus === "current" && (
                <Container>
                  {content}
                  <StyledButtonsContainer>
                    {currentStep > 1 && (
                      <Button
                        disabled={step.isDisabledPrevious ?? false}
                        onClick={createStepHandler(-1, previousAction)}
                        variant={"tertiary"}
                      >
                        {"Previous"}
                      </Button>
                    )}
                    {currentStep < steps.length ? (
                      <Button
                        disabled={step.isDisabledNext ?? false}
                        onClick={createStepHandler(1, nextAction)}
                        variant={"primary"}
                      >
                        {"Next step"}
                      </Button>
                    ) : (
                      <Button
                        disabled={disabled}
                        onClick={onClick}
                        type={type}
                        variant={"primary"}
                      >
                        {text}
                      </Button>
                    )}
                  </StyledButtonsContainer>
                </Container>
              )}
            </Container>
          </StepContainer>
        );
      })}
    </StepperContainer>
  );
};

export { StepLapse };
