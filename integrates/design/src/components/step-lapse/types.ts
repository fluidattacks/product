import type { ButtonHTMLAttributes, ReactNode } from "react";

type TStepVariant = "completed" | "current" | "disabled";

/**
 * Step component props.
 * @interface IStep
 * @property { ReactNode } content - The content to be displayed in the step.
 * @property { boolean } [isDisabledNext] - A flag indicating whether the next button should be disabled.
 * @property { boolean } [isDisabledPrevious] - A flag indicating whether the previous button should be disabled.
 * @property { Function } [nextAction] - The callback function to be executed when the next button is clicked.
 * @property { Function } [previousAction] - The callback function to be executed when the previous button is clicked.
 * @property { string } title - The title of the step.
 */
interface IStep {
  content: ReactNode;
  isDisabledNext?: boolean;
  isDisabledPrevious?: boolean;
  nextAction?: () => void;
  previousAction?: () => void;
  title: string;
}

/**
 * Step lapse button props.
 * @interface IStepLapseButton
 * @extends ButtonHTMLAttributes<HTMLButtonElement>
 * @property { boolean } disabled - Whether the button should be disabled.
 * @property { string } text - Button text.
 * @property { Function } onClick - Callback function to be executed when the button is clicked.
 */
interface IStepLapseButton
  extends Pick<ButtonHTMLAttributes<HTMLButtonElement>, "type"> {
  disabled?: boolean;
  text: string;
  onClick?: () => void;
}

/**
 * Step lapse component props.
 * @interface IStepLapseProps
 * @property { IStepLapseButton } button - The button to be displayed at the end of the step lapse.
 * @property { IStep[] } steps - The steps to be included in the step lapse.
 */
interface IStepLapseProps {
  button: IStepLapseButton;
  steps: IStep[];
}

export type { IStepLapseProps, TStepVariant };
