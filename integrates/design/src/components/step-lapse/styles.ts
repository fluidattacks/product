import { styled } from "styled-components";
import type { DefaultTheme } from "styled-components";

import { TStepVariant } from "./types";

import { variantBuilder } from "components/@core";

const { getVariant } = variantBuilder(
  (theme: DefaultTheme): Record<TStepVariant, string> => ({
    completed: `
      background-color: ${theme.palette.primary[500]};
      color: ${theme.palette.white};

      &::after {
        background-color: ${theme.palette.primary[500]};
      }
    `,
    current: `
      background-color: ${theme.palette.primary[500]};
      color: ${theme.palette.white};

      &::after {
        background-color: ${theme.palette.gray[200]};
      }
    `,
    disabled: `
      background-color: ${theme.palette.gray[200]};
      outline: 1px solid ${theme.palette.gray[400]};
      outline-offset: -1px;
      color: ${theme.palette.gray[400]};

      &::after {
        background-color: ${theme.palette.gray[200]};
      }
    `,
  }),
);

const StepperContainer = styled.div`
  ${({ theme }): string => `
    align-items: stretch;
    background-color: ${theme.palette.white};
    display: flex;
    flex-direction: column;
    height: 100%;
    padding: ${`${theme.spacing[1]} ${theme.spacing[1]}`};
    scroll-behavior: unset;
  `}
`;

const StepContainer = styled.div`
  display: flex;
  flex-direction: row;
  margin-bottom: ${({ theme }): string => theme.spacing[2]};

  &:last-child > div:first-child > span::after {
    display: none;
  }
`;

const StyledButtonsContainer = styled.div`
  ${({ theme }): string => `
    display: inline-flex;
    font-family: ${theme.typography.type.primary};
    font-size: ${theme.typography.text.sm};
    line-height: ${theme.spacing[1.25]};
    margin-top: ${theme.spacing[1]};

    & > button:first-child {
      margin-right: ${theme.spacing[0.5]};
    }
  `}
`;

const StyledStepLabel = styled.span<{ $variant: TStepVariant }>`
  ${({ theme, $variant }): string => `
    border-radius: 6px;
    display: inline-block;
    text-align: center;
    font-size: ${theme.typography.text.sm};
    font-weight: ${theme.typography.weight.bold};
    font-family: ${theme.typography.type.primary};
    line-height: ${theme.spacing[1]};
    padding: ${`${theme.spacing[0.5]} ${theme.spacing[0.5]}`};
    height: ${theme.spacing[2]};
    width: ${theme.spacing[2]};

    &::after {
      content: "";
      position: absolute;
      top: calc(32px + ${theme.spacing[0.5]});
      left: 50%;
      bottom: 0;
      width: 2px;
      height: calc(100% - ${theme.spacing[1]});
    }

    ${getVariant(theme, $variant)}
  `}
`;

export {
  StyledStepLabel,
  StyledButtonsContainer,
  StepContainer,
  StepperContainer,
};
