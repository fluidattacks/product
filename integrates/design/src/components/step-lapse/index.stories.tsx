import type { Meta, StoryFn } from "@storybook/react";

import type { IStepLapseProps } from "./types";

import { StepLapse } from ".";

const config: Meta = {
  component: StepLapse,
  tags: ["autodocs"],
  title: "Components/StepLapse",
};

const Template: StoryFn<IStepLapseProps> = (props): JSX.Element => (
  <StepLapse {...props} />
);

const Default = Template.bind({});
Default.args = {
  button: {
    disabled: true,
    text: "Send",
  },
  steps: [
    {
      content: (
        <div>
          <p>{"I'am first step"}</p>
        </div>
      ),
      title: "First step",
    },
    {
      content: (
        <div>
          <p>{"I'am"}</p>
          <p>{"second"}</p>
          <p>{"step"}</p>
        </div>
      ),
      title: "Second step",
    },
    {
      content: (
        <div>
          <p>{"I'am third step"}</p>
        </div>
      ),
      title: "Third step",
    },
  ],
};

export { Default };
export default config;
