import { useRef } from "react";
import { useNumberFormatter, useSlider } from "react-aria";
import { useSliderState } from "react-stately";
import { useTheme } from "styled-components";

import { LimitValues, Output, StyledSlider } from "./styles";
import { Thumb } from "./thumb";
import { TSliderProps } from "./types";
import { normalizeValue } from "./utils";

import { Container } from "components/container";
import { OutlineContainer } from "components/inputs";
import { Text } from "components/typography";

const Slider = (props: Readonly<TSliderProps>): JSX.Element => {
  const theme = useTheme();
  const trackRef = useRef(null);
  const { label, minValue = 0, maxValue = 100, name, setValue, watch } = props;
  const numberFormatter = useNumberFormatter();
  const state = useSliderState({
    ...props,
    numberFormatter,
    onChange(value) {
      setValue?.(name, value);
    },
    value: watch?.(name),
  });
  const inputValue = normalizeValue(minValue, maxValue, state.getThumbValue(0));
  const { groupProps, trackProps } = useSlider(
    { ...props, "aria-label": name, id: name },
    state,
    trackRef,
  );

  return (
    <OutlineContainer htmlFor={name} label={label}>
      <Container
        alignItems={"center"}
        display={"inline-flex"}
        gap={1.25}
        pl={0.5}
        width={"100%"}
      >
        <StyledSlider
          {...groupProps}
          $min={minValue ?? 0}
          $value={inputValue}
          className={"slider"}
        >
          <div
            {...trackProps}
            className={`track ${state.isDisabled ? "disabled" : ""}`}
            ref={trackRef}
          >
            <Thumb index={0} name={name} state={state} trackRef={trackRef} />
          </div>
          <LimitValues>
            <p>{`${numberFormatter.format(minValue)} (Low)`}</p>
            {minValue < 0 && <p>{"0"}</p>}
            <p>{`${numberFormatter.format(maxValue)} (High)`}</p>
          </LimitValues>
        </StyledSlider>
        <Output>
          <Text color={theme.palette.gray[800]} size={"sm"} textAlign={"right"}>
            {state.getThumbValueLabel(0)}
          </Text>
        </Output>
      </Container>
    </OutlineContainer>
  );
};

export { Slider };
