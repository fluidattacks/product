/* eslint-disable @typescript-eslint/no-explicit-any */
import type { AriaSliderThumbOptions } from "react-aria";
import { UseFormSetValue, UseFormWatch } from "react-hook-form";
import type { SliderState, SliderStateOptions } from "react-stately";

/**
 * Slider thumb component props.
 * @interface  ISliderThumbProps
 * @extends AriaSliderThumbOptions
 * @property { SliderState } state - The slider state.
 */
interface ISliderThumbProps extends Omit<AriaSliderThumbOptions, "inputRef"> {
  state: SliderState;
}

type TSliderProps = Partial<SliderStateOptions<number>> & {
  label?: string;
  name: string;
  setValue?: UseFormSetValue<any>;
  watch?: UseFormWatch<any>;
};

export type { ISliderThumbProps, TSliderProps };
