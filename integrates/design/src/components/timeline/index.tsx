import { useEffect, useRef, useState } from "react";

import { TimeLineCard } from "./card";
import { TimelineContainer } from "./styles";
import type { ITimelineProps, TTimelineContainerSize } from "./types";

import { Container } from "components/container";

const TimeLine = ({ items }: Readonly<ITimelineProps>): JSX.Element => {
  const containerRef = useRef<HTMLDivElement | null>(null);
  const [size, setSize] = useState<TTimelineContainerSize>("long");

  useEffect(() => {
    if (containerRef.current) {
      setSize(containerRef.current.offsetWidth <= 1100 ? "small" : "long");
    }
  }, []);

  return (
    <Container ref={containerRef} width={"100%"}>
      <TimelineContainer $size={size}>
        {items.map((item, index) => (
          <TimeLineCard
            date={item.date}
            description={item.description}
            key={`${item.title}-${index}`}
            size={size}
            title={item.title}
          />
        ))}
      </TimelineContainer>
    </Container>
  );
};

export { TimeLineCard, TimeLine };
