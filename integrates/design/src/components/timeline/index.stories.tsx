import type { Meta, StoryFn } from "@storybook/react";
import { Fragment } from "react";

import type { ITimelineProps } from "./types";

import { TimeLine } from ".";
import { Text } from "../typography";

const config: Meta = {
  component: TimeLine,
  tags: ["autodocs"],
  title: "Components/Timeline",
};

const Template: StoryFn<ITimelineProps> = (props): JSX.Element => {
  return (
    <Fragment>
      <Text mb={2} size={"md"}>
        {"This component adapts according to the size of its container"}
      </Text>
      <TimeLine {...props} />
    </Fragment>
  );
};

const Default = Template.bind({});
Default.args = {
  items: [
    {
      date: "2023-03-02",
      description: [
        "1 vulnerabilities were permanently accepted",
        "Justification: thats ok",
        "Assigned: jbuendia@fluidattacks.com",
      ],
      title: "Cycle 11",
    },
    {
      date: "2023-03-02",
      description: [
        "1 vulnerabilities were permanently accepted",
        "Justification: thats ok",
        "Assigned: jbuendia@fluidattacks.com",
      ],
      title: "Cycle 12",
    },
    {
      date: "2023-03-02",
      description: [
        "1 vulnerabilities were permanently accepted",
        "Justification: thats ok",
        "Assigned: jbuendia@fluidattacks.com",
      ],
      title: "Cycle 13",
    },
    {
      date: "2023-03-02",
      description: [
        "1 vulnerabilities were permanently accepted",
        "Justification: thats ok",
        "Assigned: jbuendia@fluidattacks.com",
      ],
      title: "Cycle 14",
    },
  ],
};

export default config;
export { Default };
