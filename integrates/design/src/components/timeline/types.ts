type TTimelineContainerSize = "long" | "small";

/**
 * Timeline card component props.
 * @interface ITimelineCardProps
 * @property { string } date - The date of the event.
 * @property { string[] } description - The description of the event.
 * @property { TTimelineContainerSize } [size] - Timeline size.
 * @property { string } [title] - The title of the event.
 */
interface ITimelineCardProps {
  date: string;
  description: string[];
  title?: string;
  size?: TTimelineContainerSize;
}

/**
 * Timeline component props.
 * @interface ITimelineProps
 * @property { ITimelineCardProps[] } cards - The array of timeline card items.
 */
interface ITimelineProps {
  items: ITimelineCardProps[];
}

export type { ITimelineCardProps, ITimelineProps, TTimelineContainerSize };
