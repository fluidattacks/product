import { styled } from "styled-components";

import { IStyledTextProps, StyledText } from "components/typography/styles";

const StyledInputText = styled(StyledText)<IStyledTextProps>`
  white-space: nowrap;
  margin-right: ${({ theme }): string => theme.spacing[0.25]};
  text-overflow: ellipsis;
  overflow: hidden;
  max-width: 80%;
`;

const FileInput = styled.input`
  opacity: 0;
  position: absolute;
  visibility: hidden;
  width: 0;
`;

export { StyledInputText, FileInput };
