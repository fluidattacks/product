import type { Meta, StoryFn } from "@storybook/react";

import { InputFile } from ".";
import { Form, InnerForm } from "../../../form";
import type { IInputProps } from "../../types";

const config: Meta = {
  component: InputFile,
  tags: ["autodocs"],
  title: "Components/Inputs/InputFile",
};

const mockSubmit = (): void => {
  // Mock submit function without any implementation
};

const Template: StoryFn<IInputProps> = (props): JSX.Element => (
  <Form
    defaultValues={{ fileinput: "" }}
    onSubmit={mockSubmit}
    showButtons={false}
  >
    <InnerForm>
      {({ watch, register, setValue }): JSX.Element => {
        return (
          <InputFile
            register={register}
            setValue={setValue}
            watch={watch}
            {...props}
          />
        );
      }}
    </InnerForm>
  </Form>
);

const Default = Template.bind({});
Default.args = {
  accept: "image/*",
  disabled: false,
  label: "File upload label",
  multiple: true,
  name: "fileinput",
  required: false,
  tooltip: "",
};

export { Default };
export default config;
