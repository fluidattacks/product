import { isEmpty } from "lodash";
import type { ChangeEvent, MouseEvent } from "react";
import { useCallback, useRef } from "react";
import { useTheme } from "styled-components";

import { FileInput, StyledInputText } from "./styles";

import { OutlineContainer } from "../../outline-container";
import { StyledTextInputContainer } from "../../styles";
import type { IInputProps } from "../../types";
import { createEvent, getFileName } from "../../utils";
import { Button } from "components/button";
import { Container } from "components/container";
import { Icon } from "components/icon";
import { IconButton } from "components/icon-button";

const InputFile = ({
  accept,
  disabled = false,
  error,
  label,
  helpLink,
  helpText,
  id,
  multiple,
  name,
  onChange: onChangeProp,
  required,
  register,
  setValue,
  tooltip,
  watch,
  weight,
}: Readonly<IInputProps>): JSX.Element => {
  const theme = useTheme();
  const errorMsg = disabled ? undefined : error;
  const inputRef = useRef<HTMLInputElement>(null);
  const formProps = register?.(name, { required });
  const value = watch?.(name);
  const fileName = getFileName(value);

  const handleRemoveFile = useCallback((): void => {
    const changeEvent = createEvent("change", name, undefined);
    if (setValue) setValue(name, undefined);
    if (formProps) formProps.onChange(changeEvent);
  }, [formProps, name, setValue]);

  const handleInputChange = useCallback(
    (event: Readonly<ChangeEvent<HTMLInputElement>>): void => {
      const { files } = event.currentTarget;
      if (files && files.length > 0) {
        onChangeProp?.(event);
        if (setValue) setValue(name, files);
        if (formProps) formProps.onChange(event);
      }
    },
    [formProps, name, onChangeProp, setValue],
  );

  const handleButtonClick = useCallback(
    (event: Readonly<MouseEvent<HTMLButtonElement>>): void => {
      if (disabled) {
        event.preventDefault();
      } else if (inputRef.current) {
        inputRef.current.click();
      }
    },
    [disabled],
  );
  return (
    <OutlineContainer
      error={errorMsg}
      helpLink={helpLink}
      helpText={helpText}
      htmlFor={id ?? name}
      label={label}
      required={required}
      tooltip={tooltip}
      weight={weight}
    >
      <StyledTextInputContainer
        className={`${disabled ? "disabled" : ""} ${errorMsg ? "error" : ""}`}
      >
        <StyledInputText $size={"xs"}>{fileName}</StyledInputText>
        <Container alignItems={"center"} display={"flex"} gap={0.625}>
          {isEmpty(fileName) ? undefined : (
            <IconButton
              color={theme.palette.gray[400]}
              height={"20px"}
              icon={"xmark"}
              iconSize={"lg"}
              iconType={"fa-light"}
              onClick={handleRemoveFile}
              variant={"ghost"}
              width={"20px"}
            />
          )}
          {errorMsg && (
            <Icon
              icon={"exclamation-circle"}
              iconClass={"error-icon"}
              iconColor={theme.palette.error[500]}
              iconSize={"xs"}
            />
          )}
          <Button
            border={
              disabled ? `1px solid ${theme.palette.gray[300]} !important` : ""
            }
            borderRadius={"0px 8px 8px 0px !important"}
            disabled={disabled}
            height={"40px"}
            onClick={handleButtonClick}
            variant={"primary"}
            width={"96px"}
          >
            <Icon icon={"search"} iconSize={"sm"} mr={0.5} />
            {"Add file"}
          </Button>
        </Container>
      </StyledTextInputContainer>
      <FileInput
        {...formProps}
        accept={accept}
        aria-label={name}
        data-testid={name}
        disabled={disabled}
        id={id ?? name}
        multiple={multiple}
        name={name}
        onChange={handleInputChange}
        ref={inputRef}
        type={"file"}
      />
    </OutlineContainer>
  );
};

export { InputFile };
