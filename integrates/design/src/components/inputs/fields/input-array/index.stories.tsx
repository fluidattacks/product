/* eslint-disable @typescript-eslint/no-explicit-any */
import type { Meta, StoryFn } from "@storybook/react";

import { InputArray } from ".";
import {
  Form,
  InnerForm,
  type TFormMethods,
  useFieldArray,
} from "../../../form";
import type { IInputArrayProps } from "../../types";

const config: Meta = {
  component: InputArray,
  tags: ["autodocs"],
  title: "Components/Inputs/InputArray",
};

const mockSubmit = (): void => {
  // Mock submit function without any implementation
};

const InputField = ({
  props,
  methods,
}: Readonly<{
  props: IInputArrayProps;
  methods: TFormMethods<any>;
}>): JSX.Element => {
  const { fields, append, remove } = useFieldArray({
    control: methods.control,
    name: props.name,
  });

  return <InputArray {...props} {...methods} {...{ fields, append, remove }} />;
};

const Template: StoryFn<IInputArrayProps> = (props): JSX.Element => (
  <Form
    defaultValues={{ tags: [{ tag: "" }] }}
    onSubmit={mockSubmit}
    showButtons={false}
  >
    <InnerForm>
      {(methods): JSX.Element => <InputField {...{ props, methods }} />}
    </InnerForm>
  </Form>
);

const Default = Template.bind({});
Default.args = {
  disabled: false,
  name: "tags",
  max: 3,
  placeholder: "Placeholder text",
  required: true,
};

export { Default };
export default config;
