/* eslint-disable functional/functional-parameters */
import { useCallback } from "react";

import type { IInputArrayProps } from "../../../types";
import { Input } from "../../input";
import { Button } from "components/button";
import { Col, Row } from "components/layout";

export const ArrayField = ({
  addText,
  append,
  disabled = false,
  fields,
  fieldState,
  initValue = "",
  index = 0,
  required,
  label,
  max = 100,
  name,
  onBlur,
  onChange,
  placeholder,
  tooltip,
  remove,
  value,
}: Readonly<IInputArrayProps>): JSX.Element => {
  const addItem = useCallback((): void => {
    append?.(initValue);
  }, [append, initValue]);

  const removeItem = useCallback(
    (index: number): (() => void) =>
      (): void => {
        remove?.(index);
      },
    [remove],
  );

  return (
    <Row align={"start"}>
      <Col lg={75} md={75} sm={75}>
        <Input
          disabled={disabled}
          error={fieldState ? fieldState.error?.message : undefined}
          id={"array"}
          indexArray={index}
          isTouched={fieldState ? fieldState.isTouched : undefined}
          isValid={fieldState ? !fieldState.invalid : undefined}
          label={label}
          name={name}
          onBlur={onBlur}
          onChange={onChange}
          placeholder={placeholder}
          removeItemArray={removeItem}
          required={required}
          tooltip={tooltip}
          value={value}
        />
      </Col>
      {index < max - 1 && index + 1 === fields?.length ? (
        <Col lg={25} md={25} sm={25}>
          <Button
            disabled={disabled}
            icon={"plus"}
            onClick={addItem}
            variant={"ghost"}
            width={"100%"}
          >
            {addText ?? "Add"}
          </Button>
        </Col>
      ) : undefined}
    </Row>
  );
};
