import { styled } from "styled-components";

const StyledPhoneInputContainer = styled.div`
  ${({ theme }): string => `
    align-items: flex-start;
    align-self: stretch;
    display: flex;
    gap: ${theme.spacing[0.5]};

    > button, div {
      border: 1px solid ${theme.palette.gray[300]};
      border-radius: ${theme.spacing[0.5]};
    }

    &.disabled {
      > button, div {
        background: ${theme.palette.gray[100]};
        color: ${theme.palette.gray[400]};
        cursor: not-allowed;
      }
    }

    &.error:not(.disabled) {
      > button, div {
        border: 1px solid ${theme.palette.error[500]};
      }
    }

    &:focus-within:not(.disabled, .error) {
      > button, div {
        background: ${theme.palette.white};
        border: 2px solid ${theme.palette.black};
      }
    }

    &:hover:not(:focus-within, .disabled, .error) {
      > button, div {
        background: ${theme.palette.white};
        border-color: ${theme.palette.gray[600]};
      }
    }
  `}
`;

const StyledCountrySelector = styled.button`
  ${({ theme }): string => `
    align-items: center;
    background: inherit;
    border-color: inherit;
    display: flex;
    gap: ${theme.spacing[0.25]};
    height: 40px;
    min-width: 67px;
    padding: ${theme.spacing[0.625]} ${theme.spacing[1]};

    &:disabled {
      background: ${theme.palette.gray[100]};
      cursor: not-allowed;
    }
  `}
`;

const StyledDropdown = styled.ul`
  ${({ theme }): string => `
    align-items: flex-start;
    box-shadow: ${theme.shadows.md};
    display: flex;
    flex-direction: column;
    margin-top: ${theme.spacing[0.25]};
    max-height: 190px;
    overflow: hidden auto;
    scrollbar-color: ${theme.palette.gray[600]} ${theme.palette.gray[100]};
    scroll-padding: ${theme.spacing[0.5]};
    scrollbar-width: thin;
    width: max-content;
  `}
`;

const StyledCountryItem = styled.li`
  ${({ theme }): string => `
    align-items: center;
    background: ${theme.palette.white};
    cursor: pointer;
    display: inline-flex;
    font-size: ${theme.typography.text.sm};
    gap: ${theme.spacing[0.5]};
    height: 38px;
    line-height: ${theme.spacing[1.25]};
    padding: ${theme.spacing[0.5]} ${theme.spacing[1]};
    width: 190px;

    &:hover {
      background: ${theme.palette.gray[100]};
    }
  `}
`;

const StyledCountryName = styled.p`
  color: ${({ theme }): string => theme.palette.gray[800]};
  font-size: inherit;
  min-width: 55%;
  overflow: hidden;
  text-overflow: ellipsis;
  white-space: nowrap;
`;

export {
  StyledCountryName,
  StyledCountrySelector,
  StyledCountryItem,
  StyledDropdown,
  StyledPhoneInputContainer,
};
