import type { Meta, StoryFn } from "@storybook/react";
import { expect, screen, userEvent, within } from "@storybook/test";

import { PhoneInput } from ".";
import { Form, InnerForm } from "../../../form";
import type { IInputProps } from "../../types";

const config: Meta = {
  component: PhoneInput,
  tags: ["autodocs"],
  title: "Components/Inputs/PhoneInput",
};

const mockSubmit = (): void => {
  // Mock submit function without any implementation
};

const Template: StoryFn<IInputProps> = (props): JSX.Element => {
  return (
    <Form
      defaultValues={{ phoneinput: "" }}
      onSubmit={mockSubmit}
      showButtons={false}
    >
      <InnerForm>
        {(methods): JSX.Element => <PhoneInput {...props} {...methods} />}
      </InnerForm>
    </Form>
  );
};

const Default = Template.bind({});
Default.args = {
  disabled: false,
  label: "Phone input label",
  name: "phoneinput",
  placeholder: "Placeholder text",
  required: true,
};
Default.play = async ({ canvasElement, step }): Promise<void> => {
  const canvas = within(canvasElement);
  const countrySelector = canvas.getByRole("button", {
    name: "country-selector",
  });
  const input = canvas.getByRole("textbox", { name: "phoneinput" });

  await step(
    "should render a PhoneInput component",
    async (): Promise<void> => {
      await expect(input).toBeInTheDocument();
      await expect(
        canvas.getByPlaceholderText("Placeholder text"),
      ).toBeInTheDocument();
      await expect(
        canvas.getByLabelText("Phone input label"),
      ).toBeInTheDocument();
      await expect(input).toBeRequired();
      await expect(input).not.toBeDisabled();
      await expect(input).not.toBeInvalid();
    },
  );
  await step(
    "should render country selector dropdown",
    async (): Promise<void> => {
      await expect(input).toHaveValue("+1 ");
      await expect(countrySelector).toBeInTheDocument();
      await expect(
        within(countrySelector).getByRole("presentation"),
      ).toBeInTheDocument();
    },
  );
  await step(
    "should show country options on click",
    async (): Promise<void> => {
      await userEvent.click(countrySelector);
      await expect(screen.getByText("Afghanistan")).toBeInTheDocument();
      await expect(screen.getByText("Colombia")).toBeInTheDocument();
      await expect(screen.getByText("United States")).toBeInTheDocument();
    },
  );
  await step("should allow typing", async (): Promise<void> => {
    await userEvent.type(input, "3129044590");
    await expect(input).toHaveValue("+1 (312) 904-4590");
  });
};

export { Default };
export default config;
