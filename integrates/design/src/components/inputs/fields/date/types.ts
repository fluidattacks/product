/* eslint-disable @typescript-eslint/no-explicit-any */
import { InputHTMLAttributes } from "react";
import type {
  AriaCalendarCellProps,
  AriaCalendarGridProps,
  CalendarAria,
  DateValue,
} from "react-aria";
import {
  UseFormRegister,
  UseFormSetValue,
  UseFormWatch,
} from "react-hook-form";
import type {
  CalendarState,
  DatePickerStateOptions,
  DateRangePickerStateOptions,
  RangeCalendarState,
} from "react-stately";

import type { IInputDateRangeProps, IOutlineContainerProps } from "../../types";
/**
 * Calendar state props.
 * @interface ICalendarState
 * @property {CalendarState | RangeCalendarState} state - Calendar state.
 */
interface ICalendarState {
  state: CalendarState | RangeCalendarState;
}

/**
 * Date component props.
 * @interface IDateProps
 * @extends InputHTMLAttributes<HTMLInputElement>
 * @extends IOutlineContainerProps
 * @property {string} [label] - Label of the date input.
 * @property {string} name - Name of the date input.
 * @property {"default" | "filters"} [variant] - The variant of the input.
 */
interface IDateProps
  extends InputHTMLAttributes<HTMLInputElement>,
    Omit<
      IOutlineContainerProps,
      "value" | "htmlFor" | "maxLenght" | "weight" | "required" | "label"
    > {
  label?: string;
  name: string;
  register?: UseFormRegister<any>;
  setValue?: UseFormSetValue<any>;
  watch?: UseFormWatch<any>;
  propsMin?: IInputDateRangeProps;
  propsMax?: IInputDateRangeProps;
  variant?: "default" | "filters";
}

type TInputDateProps = IDateProps &
  Omit<DatePickerStateOptions<DateValue>, "label">;

type TGridProps = AriaCalendarGridProps & ICalendarState;
type TCellProps = AriaCalendarCellProps & ICalendarState;
type THeaderProps = ICalendarState &
  Pick<CalendarAria, "nextButtonProps" | "prevButtonProps" | "title">;

type TInputDateRangeProps = IDateProps &
  Omit<DateRangePickerStateOptions, "label">;

export type {
  IDateProps,
  TInputDateProps,
  TCellProps,
  TGridProps,
  THeaderProps,
  TInputDateRangeProps,
};
