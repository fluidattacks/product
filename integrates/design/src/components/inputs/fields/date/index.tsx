import { parseDate } from "@internationalized/date";
import _ from "lodash";
import { useCallback, useRef } from "react";
import { DateValue, useDatePicker } from "react-aria";
import { useDatePickerState } from "react-stately";

import { Calendar } from "./calendar";
import type { TInputDateProps } from "./types";

import { OutlineContainer } from "../../outline-container";
import { DateSelector } from "../../utils/date-selector";
import { Dialog } from "../../utils/dialog";
import { Popover } from "../../utils/popover";
import { formatDate } from "utils/date";

const InputDate = ({
  disabled = false,
  error,
  helpLink,
  helpLinkText,
  helpText,
  required,
  label,
  name,
  register,
  setValue,
  tooltip,
  watch,
  ...props
}: Readonly<TInputDateProps>): JSX.Element => {
  const datePickerRef = useRef(null);
  const value = watch?.(name);

  const handleCalendarChange = useCallback(
    (selectedValue: DateValue | null): void => {
      if (setValue && selectedValue)
        setValue(name, formatDate(String(selectedValue)));
    },
    [name, setValue],
  );

  const state = useDatePickerState({
    ...props,
    onChange(value) {
      handleCalendarChange(value);
    },
    value:
      _.isNil(value) || _.isEmpty(value) || value === "-"
        ? null
        : parseDate(value.replace(/\//gu, "-")),
  });

  const { close } = state;
  const { groupProps, fieldProps, buttonProps, dialogProps, calendarProps } =
    useDatePicker(
      { ...props, "aria-label": label ?? name, name },
      state,
      datePickerRef,
    );

  return (
    <OutlineContainer
      error={error}
      helpLink={helpLink}
      helpLinkText={helpLinkText}
      helpText={helpText}
      htmlFor={name}
      label={label}
      required={required}
      tooltip={tooltip}
    >
      <DateSelector
        buttonProps={buttonProps}
        datePickerRef={datePickerRef}
        disabled={disabled}
        error={error}
        fieldProps={fieldProps}
        groupProps={groupProps}
        name={name}
        register={register}
        required={required}
        setValue={setValue}
        value={value}
      />
      {state.isOpen && !disabled ? (
        <Popover
          offset={8}
          placement={"bottom start"}
          state={state}
          triggerRef={datePickerRef}
        >
          <Dialog {...dialogProps}>
            <Calendar onClose={close} props={calendarProps} />
          </Dialog>
        </Popover>
      ) : null}
    </OutlineContainer>
  );
};

export { InputDate };
