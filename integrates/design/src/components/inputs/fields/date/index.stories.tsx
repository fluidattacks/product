import type { Meta, StoryFn } from "@storybook/react";

import type { TInputDateProps } from "./types";

import { InputDate } from ".";
import { Form } from "../../../form";

const config: Meta = {
  component: InputDate,
  tags: ["autodocs"],
  title: "Components/Inputs/InputDate",
};

const mockSubmit = (): void => {
  // Mock submit function without any implementation
};

const Template: StoryFn<TInputDateProps> = (props): JSX.Element => (
  <Form
    defaultValues={{ exampleDateName: "" }}
    onSubmit={mockSubmit}
    showButtons={false}
  >
    <InputDate {...props} />
  </Form>
);

const Default = Template.bind({});
Default.args = {
  disabled: false,
  label: "Label test date",
  name: "exampleDateName",
  required: true,
};

export { Default };
export default config;
