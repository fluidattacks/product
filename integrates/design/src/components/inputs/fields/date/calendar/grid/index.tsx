import { getWeeksInMonth } from "@internationalized/date";
import { useCalendarGrid, useLocale } from "react-aria";

import type { TGridProps } from "../../types";
import { CalendarCell } from "../cell";
import { DayHeaderTh, WeekDaysTr } from "../styles";

const CalendarGrid = ({
  state,
  ...props
}: Readonly<TGridProps>): JSX.Element => {
  const { locale } = useLocale();
  const { weekDays } = useCalendarGrid(props, state);

  const weeksInMonth = getWeeksInMonth(state.visibleRange.start, locale);

  return (
    <table>
      <thead>
        <WeekDaysTr>
          {weekDays.map((day, index): JSX.Element => {
            const keyWeek = `key_${index}`;

            return <DayHeaderTh key={keyWeek}>{day}</DayHeaderTh>;
          })}
        </WeekDaysTr>
      </thead>
      <tbody>
        {[...new Array(weeksInMonth).keys()].map(
          (weekIndex): JSX.Element => (
            <WeekDaysTr key={weekIndex}>
              {state
                .getDatesInWeek(weekIndex)
                .map((date, index): JSX.Element => {
                  const datesIndex = `key_${index}`;

                  return date ? (
                    <CalendarCell date={date} key={datesIndex} state={state} />
                  ) : (
                    <td key={datesIndex} />
                  );
                })}
            </WeekDaysTr>
          ),
        )}
      </tbody>
    </table>
  );
};

export { CalendarGrid };
