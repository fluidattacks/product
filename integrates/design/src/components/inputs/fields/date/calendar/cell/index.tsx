import { getLocalTimeZone, isSameDay, isToday } from "@internationalized/date";
import { isNull } from "lodash";
import { useRef } from "react";
import { useCalendarCell } from "react-aria";

import type { TCellProps } from "../../types";
import { DayItemTd, DotIconContainer } from "../styles";
import { Icon } from "components/icon";

const CalendarCell = ({ state, date }: Readonly<TCellProps>): JSX.Element => {
  const cellRef = useRef(null);
  const today = isToday(date, getLocalTimeZone());
  const { cellProps, buttonProps, isSelected, isDisabled, formattedDate } =
    useCalendarCell({ date }, state, cellRef);

  const isSelectionStart =
    "highlightedRange" in state && !isNull(state.highlightedRange)
      ? isSameDay(date, state.highlightedRange.start)
      : isSelected;
  const isSelectionEnd =
    "highlightedRange" in state && !isNull(state.highlightedRange)
      ? isSameDay(date, state.highlightedRange.end)
      : isSelected;
  const cellInRange =
    isSelected && !isDisabled && !(isSelectionStart || isSelectionEnd)
      ? "range"
      : "";

  return (
    <DayItemTd id={cellInRange} {...cellProps}>
      <div
        {...buttonProps}
        className={`cell ${isSelectionStart || isSelectionEnd ? "selected" : ""}
          ${isDisabled ? "disabled" : ""} ${today ? "today" : ""}
          ${!state.isFocused && !today ? "focused" : ""} ${cellInRange}`}
        ref={cellRef}
      >
        {formattedDate}
        {today ? (
          <DotIconContainer>
            <Icon clickable={false} icon={"circle"} iconSize={"xxs"} />
          </DotIconContainer>
        ) : undefined}
      </div>
    </DayItemTd>
  );
};

export { CalendarCell };
