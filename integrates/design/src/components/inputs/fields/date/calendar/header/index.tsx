import { useCallback } from "react";

import type { THeaderProps } from "../../types";
import { CaretButton, TitleContainer, YearSwitch } from "../styles";
import { Icon } from "components/icon";
import { Button } from "components/inputs/utils/calendar-button";

const Header = ({
  state,
  prevButtonProps,
  nextButtonProps,
  title,
}: Readonly<THeaderProps>): JSX.Element => {
  const date = state.focusedDate;

  const handleFutureClick = useCallback((): void => {
    state.setFocusedDate(date.add({ years: 1 }));
  }, [date, state]);

  const handlePastClick = useCallback((): void => {
    state.setFocusedDate(date.subtract({ years: 1 }));
  }, [date, state]);

  return (
    <div className={"header flex items-start"}>
      <Button icon={"chevron-left"} props={prevButtonProps} />
      <TitleContainer>
        {title}
        <YearSwitch>
          <CaretButton aria-label={"top"} onClick={handleFutureClick}>
            <Icon icon={"caret-up"} iconClass={"fa-sharp"} iconSize={"xxss"} />
          </CaretButton>
          <CaretButton aria-label={"bottom"} onClick={handlePastClick}>
            <Icon
              icon={"caret-down"}
              iconClass={"fa-sharp"}
              iconSize={"xxss"}
            />
          </CaretButton>
        </YearSwitch>
      </TitleContainer>
      <Button icon={"chevron-right"} props={nextButtonProps} />
    </div>
  );
};

export { Header };
