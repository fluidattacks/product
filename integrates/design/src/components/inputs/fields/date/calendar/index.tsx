import { createCalendar } from "@internationalized/date";
import { useCalendar, useLocale } from "react-aria";
import type { CalendarStateOptions } from "react-stately";
import { useCalendarState } from "react-stately";

import { CalendarGrid } from "./grid";
import { Header } from "./header";
import { FooterContainer } from "./styles";

import { Button } from "components/button";

const Calendar = ({
  onClose,
  props,
}: Readonly<{
  onClose: () => void;
  props: Omit<CalendarStateOptions, "createCalendar" | "locale">;
}>): JSX.Element => {
  const { locale } = useLocale();
  const state = useCalendarState({ ...props, createCalendar, locale });
  const { prevButtonProps, nextButtonProps, title } = useCalendar(props, state);

  return (
    <div className={"calendar"}>
      <Header
        nextButtonProps={nextButtonProps}
        prevButtonProps={prevButtonProps}
        state={state}
        title={title}
      />
      <CalendarGrid state={state} />
      <FooterContainer>
        <Button onClick={onClose} variant={"ghost"}>
          {"Cancel"}
        </Button>
      </FooterContainer>
    </div>
  );
};

export { Calendar };
