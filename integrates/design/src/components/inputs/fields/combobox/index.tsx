import { Controller } from "react-hook-form";

import { ComboBoxField } from "./field";
import { IComboBoxProps, IItem } from "./types";

const ComboBox = ({
  control,
  disabled = false,
  error,
  helpLink,
  helpLinkText,
  helpText,
  label,
  loadingItems,
  name,
  items,
  onBlur,
  onChange,
  placeholder,
  required,
  tooltip,
  value,
  weight,
}: Readonly<IComboBoxProps>): JSX.Element => {
  return control ? (
    <Controller
      control={control}
      name={name}
      // eslint-disable-next-line react/jsx-no-bind
      render={({ field, fieldState }) => {
        return (
          <ComboBoxField
            disabled={disabled}
            error={fieldState.error?.message}
            field={field}
            helpLink={helpLink}
            helpLinkText={helpLinkText}
            helpText={helpText}
            items={items}
            label={label}
            loadingItems={loadingItems}
            name={name}
            onBlur={onBlur}
            onChange={onChange}
            placeholder={placeholder}
            required={required}
            tooltip={tooltip}
            value={value}
            weight={weight}
          />
        );
      }}
      rules={{ required }}
    />
  ) : (
    <ComboBoxField
      disabled={disabled}
      error={error}
      helpLink={helpLink}
      helpLinkText={helpLinkText}
      helpText={helpText}
      items={items}
      label={label}
      loadingItems={loadingItems}
      name={name}
      onBlur={onBlur}
      onChange={onChange}
      placeholder={placeholder}
      required={required}
      tooltip={tooltip}
      value={value}
      weight={weight}
    />
  );
};

export { ComboBox };
export type { IComboBoxProps, IItem };
