import { styled } from "styled-components";

const DateSelectorOutline = styled.div<{
  $disabled?: boolean;
  $show?: boolean;
}>`
  ${({ theme, $disabled, $show }): string => `
    border-color: ${$show ? theme.palette.error[500] : theme.palette.gray[300]};
    display: flex;
    gap: ${theme.spacing[0.25]};
    width: 100%;

    ~ div {
      background: ${$disabled ? theme.palette.gray[200] : theme.palette.white};
    }
  `}
`;

export { DateSelectorOutline };
