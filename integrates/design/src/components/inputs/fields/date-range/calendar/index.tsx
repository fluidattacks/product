import { createCalendar } from "@internationalized/date";
import { useRef } from "react";
import { useLocale, useRangeCalendar } from "react-aria";
import type { RangeCalendarStateOptions } from "react-stately";
import { useRangeCalendarState } from "react-stately";

import { CalendarGrid } from "../../date/calendar/grid";
import { Header } from "../../date/calendar/header";
import { FooterContainer } from "../../date/calendar/styles";
import { Button } from "components/button";

const Calendar = ({
  onClose,
  props,
}: Readonly<{
  onClose: () => void;
  props: Omit<RangeCalendarStateOptions, "createCalendar" | "locale">;
}>): JSX.Element => {
  const { locale } = useLocale();
  const calendarRef = useRef(null);
  const state = useRangeCalendarState({ ...props, createCalendar, locale });
  const { prevButtonProps, nextButtonProps, title } = useRangeCalendar(
    props,
    state,
    calendarRef,
  );

  return (
    <div className={"calendar-range"} ref={calendarRef}>
      <Header
        nextButtonProps={nextButtonProps}
        prevButtonProps={prevButtonProps}
        state={state}
        title={title}
      />
      <CalendarGrid state={state} />
      <FooterContainer>
        <Button onClick={onClose} variant={"ghost"}>
          {"Cancel"}
        </Button>
      </FooterContainer>
    </div>
  );
};

export { Calendar };
