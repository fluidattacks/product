import { Controller } from "react-hook-form";

import { TagsField } from "./field";

import type { IInputTagProps } from "../../types";

const InputTags = ({
  disabled = false,
  control,
  error,
  helpLink,
  helpLinkText,
  helpText,
  label,
  name = "input-tag",
  onChange,
  onRemove,
  placeholder,
  required,
  tooltip,
  value,
  weight,
}: Readonly<IInputTagProps>): JSX.Element => {
  return control ? (
    <Controller
      control={control}
      name={name}
      // eslint-disable-next-line react/jsx-no-bind
      render={({ field }) => {
        return (
          <TagsField
            disabled={disabled}
            error={error}
            field={field}
            helpLink={helpLink}
            helpLinkText={helpLinkText}
            helpText={helpText}
            label={label}
            name={name}
            onRemove={onRemove}
            placeholder={placeholder}
            required={required}
            tooltip={tooltip}
            weight={weight}
          />
        );
      }}
      rules={{ required }}
    />
  ) : (
    <TagsField
      disabled={disabled}
      error={error}
      helpLink={helpLink}
      helpLinkText={helpLinkText}
      helpText={helpText}
      label={label}
      name={name}
      onChange={onChange}
      onRemove={onRemove}
      placeholder={placeholder}
      required={required}
      tooltip={tooltip}
      value={value}
      weight={weight}
    />
  );
};

export { InputTags };
