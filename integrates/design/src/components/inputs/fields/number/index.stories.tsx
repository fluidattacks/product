import type { Meta, StoryFn } from "@storybook/react";

import { InputNumber } from ".";
import { Form, InnerForm } from "../../../form";
import type { IInputNumberProps } from "../../types";

const config: Meta = {
  component: InputNumber,
  tags: ["autodocs"],
  title: "Components/Inputs/InputNumber",
};

const mockSubmit = (): void => {
  // Mock submit function without any implementation
};

const Template: StoryFn<IInputNumberProps> = (props): JSX.Element => (
  <Form
    defaultValues={{ textinputnumber: undefined }}
    onSubmit={mockSubmit}
    showButtons={false}
  >
    <InnerForm>
      {(methods): JSX.Element => <InputNumber {...props} {...methods} />}
    </InnerForm>
  </Form>
);

const Default = Template.bind({});
Default.args = {
  disabled: false,
  label: "Number input label",
  name: "textinputnumber",
  required: false,
};

export { Default };
export default config;
