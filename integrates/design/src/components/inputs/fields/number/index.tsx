import { OutlineContainer } from "../../outline-container";
import type { IInputNumberProps } from "../../types";
import { NumberField } from "../../utils/number-field";

const InputNumber = ({
  decimalPlaces,
  disabled = false,
  error,
  helpLink,
  helpText,
  id,
  label,
  linkPosition,
  maxLength,
  max,
  min,
  name,
  onChange,
  placeholder,
  register,
  required,
  setValue,
  tooltip,
  value,
  watch,
  weight,
}: Readonly<IInputNumberProps>): JSX.Element => {
  return (
    <OutlineContainer
      error={disabled ? undefined : error}
      helpLink={helpLink}
      helpText={helpText}
      htmlFor={name}
      label={label}
      linkPosition={linkPosition}
      maxLength={maxLength}
      required={required}
      tooltip={tooltip}
      weight={weight}
    >
      <NumberField
        decimalPlaces={decimalPlaces}
        disabled={disabled}
        error={error}
        id={id}
        max={max}
        maxLength={maxLength}
        min={min}
        name={name}
        onChange={onChange}
        placeholder={placeholder}
        register={register}
        required={required}
        setValue={setValue}
        value={onChange ? value : watch?.(name)}
      />
    </OutlineContainer>
  );
};

export { InputNumber };
