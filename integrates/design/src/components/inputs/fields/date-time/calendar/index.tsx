import { createCalendar } from "@internationalized/date";
import type { TimeValue } from "react-aria";
import { useCalendar, useLocale } from "react-aria";
import { useCalendarState } from "react-stately";
import type { CalendarStateOptions, DatePickerState } from "react-stately";
import { useTheme } from "styled-components";

import { OutlineContainer, TimeOutlineContainer } from "./styles";

import { TimeField } from "../../../utils/date-time-field";
import { CalendarGrid } from "../../date/calendar/grid";
import { Header } from "../../date/calendar/header";
import { FooterContainer } from "../../date/calendar/styles";
import { Button } from "components/button";
import { Text } from "components/typography";

const Calendar = ({
  handleTimeChange,
  onClose,
  props,
  timeState,
}: Readonly<{
  handleTimeChange: (selectedValue: TimeValue | null) => void;
  onClose: () => void;
  props: Omit<CalendarStateOptions, "createCalendar" | "locale">;
  timeState: DatePickerState;
}>): JSX.Element => {
  const theme = useTheme();
  const { locale } = useLocale();
  const state = useCalendarState({ ...props, createCalendar, locale });
  const { prevButtonProps, nextButtonProps, title } = useCalendar(props, state);

  return (
    <div className={"calendar"}>
      <Header
        nextButtonProps={nextButtonProps}
        prevButtonProps={prevButtonProps}
        state={state}
        title={title}
      />
      <CalendarGrid state={state} />
      <TimeOutlineContainer>
        <Text color={theme.palette.gray[800]} fontWeight={"bold"} size={"sm"}>
          {"Time"}
        </Text>
        <OutlineContainer>
          <TimeField
            hourCycle={12}
            label={"time-field"}
            onChange={handleTimeChange}
            value={timeState.timeValue}
          />
        </OutlineContainer>
      </TimeOutlineContainer>
      <FooterContainer>
        <Button onClick={onClose} variant={"ghost"}>
          {"Cancel"}
        </Button>
      </FooterContainer>
    </div>
  );
};

export { Calendar };
