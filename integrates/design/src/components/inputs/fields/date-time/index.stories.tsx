import type { Meta, StoryFn } from "@storybook/react";

import { InputDateTime } from ".";
import type { TInputDateProps } from ".";
import { Form } from "../../../form";

const config: Meta = {
  component: InputDateTime,
  tags: ["autodocs"],
  title: "Components/Inputs/InputDateTime",
};

const mockSubmit = (): void => {
  // Mock submit function without any implementation
};

const Template: StoryFn<TInputDateProps> = (props): JSX.Element => (
  <Form
    defaultValues={{ exampleDateTimeName: "" }}
    onSubmit={mockSubmit}
    showButtons={false}
  >
    <InputDateTime {...props} />
  </Form>
);

const Default = Template.bind({});
Default.args = {
  disabled: false,
  label: "Label test date time",
  name: "exampleDateTimeName",
  required: true,
};

export { Default };
export default config;
