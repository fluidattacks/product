import { type KeyboardEvent, useCallback, useState } from "react";
import { useTheme } from "styled-components";

import { OutlineContainer } from "../../outline-container";
import { StyledTextInputContainer } from "../../styles";
import type { IInputProps } from "../../types";
import { Icon } from "components/icon";
import { IconButton } from "components/icon-button";

const Input = ({
  disabled = false,
  hasIcon = false,
  helpLink,
  helpLinkText,
  helpText,
  error,
  id,
  indexArray = 0,
  isValid,
  isTouched,
  label,
  linkPosition,
  name,
  maskValue = false,
  onPaste,
  placeholder,
  removeItemArray,
  register,
  suggestions,
  tooltip,
  type = "text",
  weight,
  ...rest
}: Readonly<IInputProps>): JSX.Element => {
  const { required, onBlur, onChange } = rest;
  const theme = useTheme();
  const errorMsg = disabled ? undefined : error;
  const [inputValue, setInputValue] = useState("");

  const handleChange = useCallback(
    (event: Readonly<React.ChangeEvent<HTMLInputElement>>): void => {
      setInputValue(event.target.value);

      onChange?.(event);
    },
    [onChange],
  );

  const success = isValid && isTouched && !disabled && inputValue.trim() !== "";

  const onKeyDown = useCallback(
    (event: Readonly<KeyboardEvent<HTMLInputElement>>): void => {
      if (event.key === "Enter") {
        event.preventDefault();
      }
    },
    [],
  );

  return (
    <OutlineContainer
      error={errorMsg}
      helpLink={helpLink}
      helpLinkText={helpLinkText}
      helpText={helpText}
      htmlFor={name}
      label={label}
      linkPosition={linkPosition}
      required={required}
      tooltip={tooltip}
      weight={weight}
    >
      <StyledTextInputContainer
        className={`
          ${disabled ? "disabled" : ""} ${errorMsg ? "error" : ""} ${success ? "success" : ""}
        `}
      >
        {hasIcon ? (
          <Icon icon={"user"} iconSize={"sm"} iconType={"fa-light"} />
        ) : undefined}
        <input
          aria-hidden={false}
          aria-invalid={errorMsg ? "true" : "false"}
          aria-label={name}
          aria-required={required}
          autoComplete={"off"}
          className={maskValue ? "sr-block" : ""}
          data-testid={`${name}-input`}
          disabled={disabled}
          id={name}
          list={suggestions ? `list_${name}` : undefined}
          onKeyDown={onKeyDown}
          onPaste={onPaste}
          placeholder={placeholder}
          type={type}
          {...(register
            ? register(name, { required, onBlur, onChange: handleChange })
            : { ...rest, onChange: handleChange })}
        />
        {errorMsg ? (
          <Icon
            icon={"exclamation-circle"}
            iconClass={"error-icon"}
            iconColor={theme.palette.error[500]}
            iconSize={"xs"}
          />
        ) : undefined}
        {success ? (
          <Icon
            icon={"check-circle"}
            iconClass={"success-icon"}
            iconColor={theme.palette.success[500]}
            iconSize={"xs"}
          />
        ) : undefined}
        {id === "array" && indexArray > 0 ? (
          <IconButton
            color={theme.palette.gray["400"]}
            icon={"trash"}
            iconSize={"xxs"}
            iconType={"fa-light"}
            onClick={removeItemArray ? removeItemArray(indexArray) : undefined}
            variant={"ghost"}
          />
        ) : undefined}
      </StyledTextInputContainer>
      {suggestions ? (
        <datalist id={`list_${name}`}>
          {suggestions.map(
            (suggestion): JSX.Element => (
              <option key={suggestion} value={suggestion} />
            ),
          )}
        </datalist>
      ) : undefined}
    </OutlineContainer>
  );
};

export { Input };
