import type { Meta, StoryFn } from "@storybook/react";
import { expect, userEvent, within } from "@storybook/test";

import { Input } from ".";
import { Form, InnerForm } from "../../../form";
import type { IInputProps } from "../../types";

const config: Meta = {
  component: Input,
  tags: ["autodocs"],
  title: "Components/Inputs/Input",
};

const mockSubmit = (): void => {
  // Mock submit function without any implementation
};

const Template: StoryFn<IInputProps> = (props): JSX.Element => (
  <Form
    defaultValues={{ textinput: "" }}
    onSubmit={mockSubmit}
    showButtons={false}
  >
    <InnerForm>
      {(methods): JSX.Element => <Input {...props} {...methods} />}
    </InnerForm>
  </Form>
);

const Default = Template.bind({});
Default.args = {
  disabled: false,
  label: "Label",
  name: "textinput",
  placeholder: "Placeholder text",
  required: true,
};
Default.play = async ({ canvasElement, step }): Promise<void> => {
  const canvas = within(canvasElement);
  const input = canvas.getByRole("textbox", { name: "textinput" });

  await step("should render an Input component", async (): Promise<void> => {
    await expect(input).toBeInTheDocument();
    await expect(
      canvas.getByPlaceholderText("Placeholder text"),
    ).toBeInTheDocument();
    await expect(canvas.getByLabelText("Label")).toBeInTheDocument();
    await expect(input).toBeRequired();
    await expect(input).not.toBeDisabled();
    await expect(input).not.toBeInvalid();
  });
  await step("should allow typing", async (): Promise<void> => {
    await userEvent.type(input, "This is an example text");
    await expect(input).toHaveValue("This is an example text");
  });
};

const HelpLink = Template.bind({});
HelpLink.args = {
  disabled: false,
  helpText: "Help text here",
  helpLink: "https://test.com",
  label: "Label",
  name: "textinputlink",
  placeholder: "Placeholder text",
  required: true,
};

const HelpLinkUp = Template.bind({});
HelpLinkUp.args = {
  disabled: false,
  label: "Number input label",
  name: "textinputnumber",
  required: true,
  helpLink: "https://www.google.com",
  linkPosition: "up",
};

export { Default, HelpLink, HelpLinkUp };
export default config;
