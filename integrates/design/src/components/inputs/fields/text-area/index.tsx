import { useTheme } from "styled-components";

import { OutlineContainer } from "../../outline-container";
import { StyledTextInputContainer } from "../../styles";
import type { ITextAreaProps } from "../../types";
import { Icon } from "components/icon";

const TextArea = ({
  disabled = false,
  error,
  helpLink,
  helpText,
  isTouched,
  isValid,
  label,
  name = "input-text",
  id,
  maskValue = false,
  maxLength,
  placeholder,
  register,
  required,
  rows,
  tooltip,
  watch,
  weight,
  value: propValue,
}: Readonly<ITextAreaProps>): JSX.Element => {
  const theme = useTheme();
  const value = watch?.(name) ?? propValue;
  const success = isValid && isTouched && !disabled;
  const errorMsg = disabled ? undefined : error;

  return (
    <OutlineContainer
      error={errorMsg}
      helpLink={helpLink}
      helpText={helpText}
      htmlFor={name}
      label={label}
      maxLength={maxLength}
      required={required}
      tooltip={tooltip}
      value={value ? String(value) : undefined}
      weight={weight}
    >
      <StyledTextInputContainer
        className={`
          ${disabled ? "disabled" : ""} ${errorMsg ? "error" : ""} ${success ? "success" : ""}
        `}
      >
        <textarea
          aria-hidden={false}
          aria-invalid={errorMsg ? "true" : "false"}
          aria-label={name}
          aria-required={required}
          className={maskValue ? "sr-block" : ""}
          data-testid={`${name}-text-area`}
          disabled={disabled}
          id={id ?? name}
          maxLength={maxLength}
          placeholder={placeholder}
          rows={rows}
          value={value}
          {...register?.(name, { required })}
        />
        {errorMsg ? (
          <Icon
            icon={"exclamation-circle"}
            iconClass={"error-icon"}
            iconColor={theme.palette.error[500]}
            iconSize={"xs"}
          />
        ) : undefined}
        {success ? (
          <Icon
            icon={"check-circle"}
            iconClass={"success-icon"}
            iconColor={theme.palette.success[500]}
            iconSize={"xs"}
          />
        ) : undefined}
      </StyledTextInputContainer>
    </OutlineContainer>
  );
};

export { TextArea };
