import { PropsWithChildren } from "react";
import { useTheme } from "styled-components";

import { Label } from "../label";
import { StyledOutlineContainer } from "../styles";
import type { IOutlineContainerProps } from "../types";
import { Container } from "components/container";
import { Icon } from "components/icon";
import { Link } from "components/link";
import { Tag } from "components/tag";
import { Text } from "components/typography";

const OutlineContainer = ({
  children,
  error,
  helpLink,
  helpLinkText = "Link here",
  helpText,
  htmlFor,
  label,
  linkPosition = "down",
  maxLength,
  required,
  tooltip,
  value,
  weight,
}: Readonly<PropsWithChildren<IOutlineContainerProps>>): JSX.Element => {
  const theme = useTheme();
  const valueLength = value ? value.length : 0;

  return (
    <StyledOutlineContainer>
      {label === undefined ? undefined : (
        <Label
          htmlFor={htmlFor}
          label={label}
          link={linkPosition === "up" ? helpLink : undefined}
          required={required}
          tooltip={tooltip}
          weight={weight}
        />
      )}
      {children}
      {error || maxLength ? (
        <Container
          display={"flex"}
          justify={error === undefined ? "end" : "space-between"}
          width={"100%"}
        >
          {error && (
            <Text
              className={"error-msg"}
              color={theme.palette.error[500]}
              display={"block"}
              lineSpacing={1}
              size={"sm"}
            >
              {error}
            </Text>
          )}
          {maxLength && (
            <Tag
              priority={"low"}
              tagLabel={`${valueLength}/${maxLength}`}
              variant={"error"}
            />
          )}
        </Container>
      ) : undefined}
      {helpText || helpLink ? (
        <Container
          alignItems={"center"}
          display={"flex"}
          gap={0.25}
          width={"100%"}
        >
          {helpText && (
            <>
              <Icon
                icon={"circle-info"}
                iconColor={theme.palette.gray[400]}
                iconSize={"xxs"}
                iconType={"fa-light"}
              />
              <Text
                color={theme.palette.gray[400]}
                display={"inline-block"}
                size={"sm"}
              >
                {helpText}
              </Text>
            </>
          )}
          {helpLink && linkPosition === "down" ? (
            <Link href={helpLink}>{helpLinkText}</Link>
          ) : undefined}
        </Container>
      ) : undefined}
    </StyledOutlineContainer>
  );
};

export { OutlineContainer };
