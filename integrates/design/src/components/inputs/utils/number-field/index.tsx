import _ from "lodash";
import { type ChangeEvent, useCallback, useState } from "react";
import { useTheme } from "styled-components";

import { StyledTextInputContainer } from "../../styles";
import type { IInputNumberProps } from "../../types";
import { ActionButton } from "../action-button";
import { Icon } from "components/icon";

const NumberField = ({
  error,
  decimalPlaces = 0,
  disabled = false,
  id,
  maxLength,
  max = Infinity,
  min = 0,
  name,
  onChange: onChangeProp,
  placeholder,
  register,
  required,
  setValue,
  value,
}: Readonly<IInputNumberProps>): JSX.Element => {
  const theme = useTheme();
  const errorMsg = disabled ? undefined : error;
  const decimals = decimalPlaces < 0 ? 0 : decimalPlaces;
  const [currentValue, setCurrentValue] = useState<number>(
    value ? _.toNumber(value) : 0,
  );

  const handleClickMinus = useCallback((): void => {
    const newValue = (currentValue - 10 ** -decimals).toFixed(decimals);
    const newValueNumber = Math.max(Number(min), Number(newValue));
    if (setValue) setValue(name, newValueNumber, { shouldDirty: true });
    setCurrentValue(newValueNumber);
  }, [decimals, currentValue, min, name, setValue]);

  const handleClickPlus = useCallback((): void => {
    const newValue = (currentValue + 10 ** -decimals).toFixed(decimals);
    const newValueNumber = Math.min(
      Number(max),
      Number(newValue) > Number(min) ? Number(newValue) : Number(min),
    );
    if (setValue) setValue(name, newValueNumber, { shouldDirty: true });
    setCurrentValue(newValueNumber);
  }, [decimals, currentValue, max, min, name, setValue]);

  const handleChange = useCallback(
    (event: Readonly<ChangeEvent<HTMLInputElement>>): void => {
      const { value: targetValue } = event.target;
      const isNumber = /\d+/u.test(targetValue) && !_.isEmpty(targetValue);
      if (setValue) setValue(name, isNumber ? targetValue : undefined);
      if (onChangeProp) onChangeProp(event);
      setCurrentValue(Number(targetValue));
    },
    [name, onChangeProp, setValue],
  );

  const onKeyDown = useCallback(
    (event: Readonly<React.KeyboardEvent<HTMLInputElement>>): void => {
      if (event.key === "Enter") {
        event.preventDefault();
      }
    },
    [],
  );

  return (
    <StyledTextInputContainer
      className={`${disabled ? "disabled" : ""} ${errorMsg ? "error" : ""}`}
    >
      <input
        aria-hidden={false}
        aria-invalid={errorMsg ? "true" : "false"}
        aria-label={name}
        aria-required={required}
        autoComplete={"off"}
        disabled={disabled}
        id={id ?? name}
        max={max}
        min={min}
        onKeyDown={onKeyDown}
        placeholder={placeholder}
        required={required}
        step={10 ** -decimals}
        type={"number"}
        {...register?.(name, {
          maxLength,
          max,
          min,
          onChange: handleChange,
          value,
          valueAsNumber: true,
        })}
      />
      {errorMsg && (
        <Icon
          icon={"exclamation-circle"}
          iconClass={"error-icon"}
          iconColor={theme.palette.error[500]}
          iconSize={"xs"}
        />
      )}
      <ActionButton
        borderLeft={"inherit"}
        borderRight={"inherit"}
        disabled={disabled}
        icon={"minus"}
        margin={"0 -0.625rem 0 0"}
        onClick={handleClickMinus}
        px={0.75}
        py={0.75}
      />
      <ActionButton
        borderRadius={`0 ${theme.spacing[0.5]} ${theme.spacing[0.5]} 0`}
        disabled={disabled}
        icon={"plus"}
        onClick={handleClickPlus}
        px={0.75}
        py={0.75}
      />
    </StyledTextInputContainer>
  );
};

export { NumberField };
