import type { IconName } from "@fortawesome/free-solid-svg-icons";
import { useRef } from "react";
import type { AriaButtonOptions } from "react-aria";
import { useButton } from "react-aria";

import { CalendarButtonAction } from "../styles";
import { theme } from "components/colors";
import { Icon } from "components/icon";

const Button = ({
  disabled = false,
  icon,
  props,
}: Readonly<{
  disabled?: boolean;
  icon: IconName;
  props: AriaButtonOptions<"button">;
}>): JSX.Element => {
  const ref = useRef(null);
  const { buttonProps } = useButton(props, ref);

  return (
    <CalendarButtonAction
      {...buttonProps}
      $disabled={disabled}
      $focus={Boolean(buttonProps["aria-expanded"])}
      $header={icon === "calendar" || icon === "calendar-clock"}
      disabled={disabled}
      ref={ref}
      type={"button"}
    >
      <Icon
        icon={icon}
        iconColor={theme.palette.gray[disabled ? 400 : 700]}
        iconSize={"xs"}
        iconType={"fa-light"}
      />
    </CalendarButtonAction>
  );
};

export { Button };
