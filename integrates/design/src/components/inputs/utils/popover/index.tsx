import React, { PropsWithChildren, useCallback, useRef } from "react";
import { Overlay, usePopover } from "react-aria";

import type { IPopoverProps } from "../types";

const Popover = ({
  className,
  children,
  isFilter = false,
  popoverRef,
  state,
  triggerRef,
  ...props
}: Readonly<PropsWithChildren<IPopoverProps>>): JSX.Element => {
  const popover = useRef(null);
  const parentRect = triggerRef?.current?.getBoundingClientRect();
  const { popoverProps } = usePopover(
    {
      ...props,
      popoverRef: popoverRef ?? popover,
      triggerRef,
      isNonModal: popoverRef ? true : undefined,
    },
    state,
  );

  const handlePointerEvents = useCallback(
    (event: Readonly<React.PointerEvent<HTMLDivElement>>): void => {
      event.preventDefault();
    },
    [],
  );

  return (
    <Overlay>
      <div
        {...popoverProps}
        className={className}
        id={"popover"}
        onPointerDown={isFilter ? handlePointerEvents : undefined}
        ref={popoverRef ?? popover}
        style={{
          ...popoverProps.style,
          ...(className === "dropdown"
            ? {
                display: "flex",
                justifyContent: "center",
                width: parentRect?.width,
              }
            : {}),
        }}
      >
        {children}
      </div>
    </Overlay>
  );
};

export { Popover };
