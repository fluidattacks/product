import { useCallback } from "react";
import { DateValue } from "react-aria";

import { StyledTextInputContainer } from "../../styles";
import { Button } from "../calendar-button";
import { DateField } from "../date-time-field";
import type { IDateSelectorProps } from "../types";
import { formatDate, formatDateTime } from "utils/date";

const DateSelector = ({
  error,
  buttonProps,
  datePickerRef,
  disabled = false,
  granularity = "day",
  fieldProps,
  groupProps,
  name,
  register,
  required,
  setValue,
  value,
}: Readonly<IDateSelectorProps>): JSX.Element => {
  const handleChange = useCallback(
    (selectedValue: DateValue | null): void => {
      const formattedDate =
        granularity === "day"
          ? formatDate(String(selectedValue))
          : formatDateTime(String(selectedValue));

      if (fieldProps.onChange) {
        fieldProps.onChange(selectedValue ?? null);
      }
      if (setValue) {
        setValue(name, formattedDate);
      }
    },
    [fieldProps, granularity, name, setValue],
  );

  return (
    <StyledTextInputContainer
      {...groupProps}
      className={`${disabled ? "disabled" : ""} ${error ? "error" : ""}`}
      ref={datePickerRef}
    >
      <DateField
        disabled={disabled}
        error={error != undefined}
        props={{
          ...fieldProps,
          autoFocus: false,
          granularity,
          onChange: handleChange,
          shouldForceLeadingZeros: true,
        }}
      />
      <Button
        disabled={disabled}
        icon={granularity === "minute" ? "calendar-clock" : "calendar"}
        props={buttonProps}
      />
      <input
        {...register?.(name, { required })}
        aria-label={name}
        className={"date-input"}
        disabled={disabled}
        id={name}
        name={name}
        type={"text"}
        value={value ?? ""}
      />
    </StyledTextInputContainer>
  );
};

export { DateSelector };
