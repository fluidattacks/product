import type { Ref } from "react";
import { forwardRef } from "react";

import { StyledButtonAction } from "../styles";
import type { IActionButtonProps } from "../types";
import { Icon } from "components/icon";

const ActionButton = forwardRef(function ActionButton(
  { disabled, icon, margin, onClick, ...props }: Readonly<IActionButtonProps>,
  ref: Ref<HTMLButtonElement>,
): JSX.Element {
  return (
    <StyledButtonAction
      $margin={margin}
      aria-label={icon}
      className={"action-btn"}
      disabled={disabled}
      onClick={onClick}
      ref={ref}
      type={"button"}
      {...props}
    >
      <Icon
        icon={icon}
        iconClass={"action"}
        iconSize={"xs"}
        iconType={"fa-light"}
      />
    </StyledButtonAction>
  );
});

export { ActionButton };
