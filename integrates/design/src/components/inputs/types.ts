/* eslint-disable @typescript-eslint/no-explicit-any */
import type { InputHTMLAttributes, TextareaHTMLAttributes } from "react";
import {
  Control,
  ControllerFieldState,
  ControllerRenderProps,
  UseFieldArrayAppend,
  UseFieldArrayRemove,
  UseFormRegister,
  UseFormSetValue,
  UseFormWatch,
} from "react-hook-form";

/**
 * Label component props.
 * @interface ILabelProps
 * @property {string} htmlFor - The htmlFor of the input.
 * @property {string} [link] - Link where more information could be found.
 * @property {string | JSX.Element} [label] - The label of the input.
 * @property {boolean} [required] - Whether the input is required.
 * @property {string} [tooltip] - The tooltip of the input.
 * @property {string} [weight] - The weight of the label.
 */
interface ILabelProps {
  htmlFor: string;
  label?: string | JSX.Element;
  link?: string;
  required?: boolean;
  tooltip?: string;
  weight?: "bold" | "normal";
}

/**
 * Outline container component props.
 * @interface IOutlineContainerProps
 * @extends ILabelProps
 * @property {string} [error] - The error message of the input.
 * @property {string} [helpLink] - The help link of the input.
 * @property {string} [helpLinkText] - The help link text of the input.
 * @property {string} [helpText] - The help text of the input.
 * @property {number} [maxLength] - The max length of the input.
 */
interface IOutlineContainerProps extends ILabelProps {
  error?: string;
  helpLink?: string;
  helpLinkText?: string;
  linkPosition?: "up" | "down";
  helpText?: string;
  maxLength?: number;
  value?: string;
}

/**
 * Input text component props.
 * @interface IInputProps
 * @extends InputHTMLAttributes<HTMLInputElement>
 * @extends IOutlineContainerProps
 * @property {boolean} [hasIcon] - Whether the input has an icon.
 * @property {string} name - The name of the input.
 * @property {boolean} [maskValue] - Whether the input has a mask value.
 * @property {string[]} [suggestions] - The suggestions of the input.
 */
interface IInputProps
  extends InputHTMLAttributes<HTMLInputElement>,
    Partial<Omit<IOutlineContainerProps, "maxLength" | "value">> {
  hasIcon?: boolean;
  indexArray?: number;
  isValid?: boolean;
  isTouched?: boolean;
  name: string;
  maskValue?: boolean;
  removeItemArray?: (index: number) => () => void;
  register?: UseFormRegister<any>;
  setValue?: UseFormSetValue<any>;
  suggestions?: string[];
  watch?: UseFormWatch<any>;
}

/**
 * Text area component props.
 * @interface ITextAreaProps
 * @extends TextareaHTMLAttributes<HTMLTextAreaElement>
 * @extends IOutlineContainerProps
 * @property {string} name - The name of the text area.
 * @property {boolean} [maskValue] - Whether the input has a mask value.
 */
interface ITextAreaProps
  extends TextareaHTMLAttributes<HTMLTextAreaElement>,
    Partial<Omit<IOutlineContainerProps, "value">> {
  isValid?: boolean;
  isTouched?: boolean;
  name: string;
  maskValue?: boolean;
  register?: UseFormRegister<any>;
  watch?: UseFormWatch<any>;
}

/**
 * Input number component props.
 * @interface IInputNumberProps
 * @extends InputHTMLAttributes<HTMLInputElement>
 * @extends IOutlineContainerProps
 * @property {number} [decimalPlaces] - The decimal places of the input.
 * @property {string} name - The name of the input.
 */
interface IInputNumberProps
  extends InputHTMLAttributes<HTMLInputElement>,
    Partial<Omit<IOutlineContainerProps, "value">> {
  decimalPlaces?: number;
  name: string;
  register?: UseFormRegister<any>;
  setValue?: UseFormSetValue<any>;
  watch?: UseFormWatch<any>;
}

/**
 * Input number range component props.
 * @interface IInputNumberRangeProps
 * @property {boolean} [disabled] - Whether the input is disabled.
 * @property {string} name - The name of the input.
 * @property {string} [label] - The label of the input.
 * @property {IInputNumberProps} propsMin - The props of the minimum input.
 * @property {IInputNumberProps} propsMax - The props of the maximum input.
 * @property {boolean} [required] - Whether the input is required.
 * @property {string} [tooltip] - The tooltip of the input.
 * @property {"default" | "filters"} [variant] - The variant of the input.
 */
interface IInputNumberRangeProps {
  disabled?: boolean;
  error?: string;
  name: string;
  label?: string;
  propsMin: IInputNumberProps;
  propsMax: IInputNumberProps;
  required?: boolean;
  tooltip?: string;
  variant?: "default" | "filters";
}

/**
 * Input number component props.
 * @interface IInputNumberProps
 * @extends InputHTMLAttributes<HTMLInputElement>
 * @extends IOutlineContainerProps
 * @property {Function} [onRemove] - The function to remove the tag.
 */
interface IInputTagProps
  extends InputHTMLAttributes<HTMLInputElement>,
    Partial<Omit<IOutlineContainerProps, "value">> {
  control?: Control<any>;
  onRemove?: (tag: string) => void;
}

/**
 * Input date range component props.
 * @interface IInputDateRangeProps
 * @extends InputHTMLAttributes<HTMLInputElement>
 * @extends IOutlineContainerProps
 * @property {string} [label] - The label of the input.
 * @property {string} name - The name of the input.
 */
interface IInputDateRangeProps
  extends InputHTMLAttributes<HTMLInputElement>,
    Partial<
      Omit<
        IOutlineContainerProps,
        "htmlFor" | "maxLenght" | "weight" | "required" | "label"
      >
    > {
  name: string;
  value?: string;
}

/**
 * Input array component props.
 * @interface IInputArrayProps
 * @property { string } [addText] - The text to show in add button.
 * @property { UseFieldArrayAppend<FieldValues, string> } [append] - append a new field.
 * @property { Control<any> } [control] - The form controller.
 * @property { boolean } [disabled] - Whether the input is disabled.
 * @property { Record<"id", string>[] } [fields] - Fields to spawn.
 * @property { number } [index] - The input index.
 * @property { string } [initValue] - The initial value.
 * @property { string } [label] - The input label.
 * @property { number } [max] - The max allowed inputs to spawn.
 * @property { string } name - The input name.
 * @property { Function } [onChange] - The function to handle on change.
 * @property { string } [placeholder] - The input placeholder.
 * @property { UseFieldArrayRemove } [remove] - The function to remove an input.
 * @property { boolean } [required] - Whether the input is required.
 * @property { string } [tooltip] - The input tooltip.
 */
interface IInputArrayProps
  extends Partial<Omit<ControllerRenderProps, "onChange">> {
  addText?: string;
  append?: UseFieldArrayAppend<any, any>;
  control?: Control<any>;
  disabled?: boolean;
  fields?: Record<"id", string>[];
  fieldState?: ControllerFieldState;
  index?: number;
  initValue?: string;
  label?: string;
  max?: number;
  name: string;
  onChange?: (event: Readonly<React.ChangeEvent<HTMLInputElement>>) => void;
  placeholder?: string;
  remove?: UseFieldArrayRemove;
  required?: boolean;
  tooltip?: string;
}

export type {
  ILabelProps,
  IOutlineContainerProps,
  IInputProps,
  ITextAreaProps,
  IInputNumberProps,
  IInputNumberRangeProps,
  IInputTagProps,
  IInputDateRangeProps,
  IInputArrayProps,
};
