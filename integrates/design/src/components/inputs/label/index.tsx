import { isEmpty } from "lodash";
import { useTheme } from "styled-components";

import { ILabelProps } from "../types";
import { Icon } from "components/icon";
import { Link } from "components/link";
import { Tooltip } from "components/tooltip";

const Label = ({
  htmlFor,
  label,
  link,
  required,
  tooltip,
  weight = "normal",
}: Readonly<ILabelProps>): JSX.Element => {
  const theme = useTheme();
  const tooltipId = `${htmlFor}-tooltip`;
  const hasTooltip = tooltip !== undefined && !isEmpty(tooltip);

  return (
    <label className={`flex font-${weight} gap-1 w-full`} htmlFor={htmlFor}>
      {label}
      {required && (
        <Icon
          icon={"asterisk"}
          iconColor={theme.palette.error[500]}
          iconSize={"xxss"}
        />
      )}
      {link ? <Link href={link} target={"_blank"} /> : null}
      {hasTooltip ? (
        <Tooltip
          display={"inline-flex"}
          icon={"circle-info"}
          id={tooltipId}
          place={"top"}
          tip={tooltip}
        />
      ) : null}
    </label>
  );
};

export { Label };
