{ inputs, makeScript, outputs, ... }:
makeScript {
  entrypoint = ./entrypoint.sh;
  name = "integrates-jira-deploy";
  searchPaths = {
    bin = [
      inputs.nixpkgs.bash
      inputs.nixpkgs.git
      inputs.nixpkgs.gnutar
      inputs.nixpkgs.gzip
      inputs.nixpkgs.jq
      inputs.nixpkgs.nodejs_20
      inputs.nixpkgs.kubectl
    ];
    source = [ outputs."/common/utils/aws" outputs."/common/utils/sops" ];
  };
}
