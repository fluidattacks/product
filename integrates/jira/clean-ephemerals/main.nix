{ inputs, makeScript, outputs, ... }:
makeScript {
  entrypoint = ./entrypoint.sh;
  name = "integrates-jira-clean-ephemerals";
  searchPaths = {
    bin = [
      inputs.nixpkgs.bash
      inputs.nixpkgs.git
      inputs.nixpkgs.jq
      inputs.nixpkgs.nodejs_20
    ];
    source = [ outputs."/common/utils/aws" outputs."/common/utils/sops" ];
  };
}
