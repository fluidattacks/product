import type React from "react";
import { createPortal } from "react-dom";

import { useModal } from "./hooks";
import { ModalFooter } from "./modal-footer";
import { ModalHeader } from "./modal-header";
import { ModalContainer, ModalWrapper } from "./styles";
import type { IModalProps } from "./types";

function Modal({
  cancelButton = undefined,
  children = undefined,
  confirmButton = undefined,
  description = "",
  modalRef,
  onCloseModal,
  title,
}: Readonly<IModalProps>): Readonly<React.JSX.Element> | null {
  if (modalRef.isOpen) {
    return createPortal(
      <ModalWrapper>
        <ModalContainer>
          <ModalHeader
            description={description}
            modalRef={modalRef}
            onCloseModal={onCloseModal}
            title={title}
          />
          {children}
          <ModalFooter
            cancelButton={cancelButton}
            confirmButton={confirmButton}
            modalRef={modalRef}
          />
        </ModalContainer>
      </ModalWrapper>,
      document.body,
    );
  }

  return null;
}

export { Modal, useModal };
