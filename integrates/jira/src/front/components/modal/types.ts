import type { HTMLAttributes } from "react";

import type { IUseModal } from "./hooks";

interface IStyledFooterProps extends HTMLAttributes<HTMLDivElement> {
  $fullInfo?: boolean;
  $gap?: string;
  $justifyContent?: "flex-start" | "space-between";
}

interface IButtonProps {
  onClick: () => void;
  text: string;
}

interface IModalProps {
  cancelButton?: IButtonProps;
  children?: React.ReactNode;
  confirmButton?: IButtonProps;
  description?: React.ReactNode | string;
  modalRef: IUseModal;
  onCloseModal?: () => void;
  title: string;
}

export type { IModalProps, IStyledFooterProps };
