import { styled } from "styled-components";

import type { TTagVariant } from "./types";

import { theme } from "../theme";

interface IStyledTag {
  $variant: TTagVariant;
  $fontSize?: string;
}

interface IVariant {
  bgColor: string;
  color: string;
}

const variants: Readonly<Record<TTagVariant, IVariant>> = {
  green: {
    bgColor: theme.palette.success["50"],
    color: theme.palette.success["700"],
  },
  orange: {
    bgColor: theme.palette.warning["50"],
    color: theme.palette.warning["700"],
  },
  red: {
    bgColor: theme.palette.error["50"],
    color: theme.palette.error["700"],
  },
};

const StyledTag = styled.span<IStyledTag>`
  align-items: center;
  justify-content: center;
  gap: 10px;
  border-radius: 4px;
  display: inline-flex;
  padding: 0 4px;
  text-align: center;
  font-family: Roboto, sans-serif;
  ${({ $fontSize = "12px" }): string => `font-size: ${$fontSize};`}
  font-style: normal;
  font-weight: 400;
  line-height: 18px;
  ${({ $variant }): string => {
    const { bgColor, color } = variants[$variant];

    return `
      background-color: ${bgColor};
      color: ${color};
    `;
  }}
`;

export { StyledTag };
