/* eslint-disable @typescript-eslint/no-magic-numbers */
import type { FormikValues } from "formik";
import { Formik, Form as FormikForm } from "formik";
import type { Schema } from "yup";

import type { TFormProps } from "./types";

import { Button } from "../button";
import { Container } from "../container";
import { Heading } from "../typography";

function Form<Values extends FormikValues>({
  cancelButtonLabel = "Cancel",
  children,
  enableReinitialize,
  initialValues,
  onClickCancel,
  onSubmit,
  submitButtonLabel = "Submit",
  title,
  validationSchema,
}: Readonly<TFormProps<Values>>): Readonly<React.JSX.Element> {
  return (
    <Container>
      {title === undefined ? undefined : (
        <Container display={"flex"} flexDirection={"column"} mb={2.25}>
          <Heading size={"sm"} weight={"bold"}>
            {title}
          </Heading>
        </Container>
      )}
      <Formik
        enableReinitialize={enableReinitialize}
        initialValues={initialValues}
        onSubmit={onSubmit}
        validationSchema={validationSchema as Schema}
      >
        {({ isSubmitting }) => {
          return (
            <FormikForm>
              <Container
                display={"flex"}
                flexDirection={"column"}
                gap={0.5}
                mb={2.25}
                mt={title === undefined ? undefined : 2.25}
              >
                {children}
              </Container>
              <Container display={"flex"} gap={1} mt={2.25}>
                {onClickCancel ? (
                  <Button
                    onClick={onClickCancel}
                    type={"reset"}
                    variant={"tertiary"}
                  >
                    {cancelButtonLabel}
                  </Button>
                ) : undefined}
                <Button disabled={isSubmitting} type={"submit"}>
                  {submitButtonLabel}
                </Button>
              </Container>
            </FormikForm>
          );
        }}
      </Formik>
    </Container>
  );
}

export { Form };
