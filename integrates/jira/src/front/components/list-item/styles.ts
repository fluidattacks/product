/* eslint-disable @typescript-eslint/no-magic-numbers */
import { styled } from "styled-components";

const Li = styled.li`
  list-style-type: none;
  height: 40px;
  display: flex;
  padding: 10px 16px;
  cursor: pointer;
  pointer-events: auto;
  background-color: #fff;
  z-index: 10;

  .main-container {
    min-width: 240px;
    display: flex;
    justify-content: space-between;
    align-items: center;
    gap: 8px;

    a,
    p {
      color: ${({ theme }): string => theme.palette.gray[500]};
    }

    [aria-disabled="true"] {
      color: ${({ theme }): string => theme.palette.gray[300]};
      pointer-events: none;
    }
  }

  .main-container label {
    margin: unset;
  }

  div.main-container[aria-disabled="true"] {
    color: ${({ theme }): string => theme.palette.gray[300]};
  }

  &:hover {
    background-color: ${({ theme }): string => theme.palette.gray[100]};
  }
`;

export { Li };
