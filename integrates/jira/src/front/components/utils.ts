/* eslint-disable functional/prefer-immutable-types */
import type { DefaultTheme, Interpolation } from "styled-components";

type TCssString = Interpolation<object>[];

interface IComponentVariant<T extends string> {
  getVariant: (theme: DefaultTheme, variant: T) => TCssString;
}

function variantBuilder<T extends string>(
  variants: (theme: DefaultTheme) => Record<T, TCssString>,
): IComponentVariant<T> {
  return {
    getVariant: (theme: DefaultTheme, variant: T): TCssString =>
      variants(theme)[variant],
  };
}

export type { TCssString, IComponentVariant };
export { variantBuilder };
