/* eslint-disable react/no-multi-comp */
import Bugsnag from "@bugsnag/js";
import React from "react";

import { Alert } from "../alert";

function ErrorMessage(): Readonly<React.JSX.Element> {
  return (
    <Alert variant={"error"}>
      {"Something went wrong on our end."}
      <br />
      {"While we work on a fix, you can help us by reporting"}&nbsp;
      {"additional details at help@fluidattacks.com"}
    </Alert>
  );
}

interface IErrorBoundary {
  readonly children: React.ReactNode;
}

function ErrorBoundary({
  children,
}: IErrorBoundary): Readonly<React.JSX.Element> {
  const bugsnagPlugin = Bugsnag.getPlugin("react");

  if (bugsnagPlugin) {
    // eslint-disable-next-line @typescript-eslint/naming-convention
    const BugsnagBoundary = bugsnagPlugin.createErrorBoundary(React);

    return (
      <BugsnagBoundary FallbackComponent={ErrorMessage}>
        {children}
      </BugsnagBoundary>
    );
  }

  return <div>{children}</div>;
}

export { ErrorBoundary };
