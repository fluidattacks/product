import type { IconName } from "@fortawesome/free-solid-svg-icons";
import type { MouseEventHandler } from "react";
import type { DefaultTheme } from "styled-components";

type TSize = "lg" | "md" | "sm" | "xs";
type TIconType = "fa-light" | "fa-regular" | "fa-solid";
type TSpacing = keyof DefaultTheme["spacing"];

interface IIconWrapProps {
  clickable?: boolean;
  color?: string;
  disabled?: boolean;
  mr?: TSpacing;
  onClick?: MouseEventHandler<HTMLSpanElement>;
  size: TSize;
}

interface IIconProps extends IIconWrapProps {
  icon: IconName;
  iconType?: TIconType;
}

export type { IIconProps, IIconWrapProps, TSize, TIconType };
