import { IconWrap } from "./styles";
import type { IIconProps } from "./types";

function Icon({
  clickable,
  color,
  disabled,
  icon,
  mr,
  onClick,
  size,
  iconType = "fa-solid",
}: Readonly<IIconProps>): Readonly<React.JSX.Element> {
  return (
    <IconWrap
      $clickable={clickable}
      $color={color}
      $disabled={disabled}
      $mr={mr}
      $size={size}
      key={`${iconType}-${icon}`}
      onClick={onClick}
    >
      <i className={`${iconType} fa-${icon}`} />
    </IconWrap>
  );
}

export { Icon };
