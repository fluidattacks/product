/* eslint-disable functional/prefer-immutable-types */
import { styled } from "styled-components";

import type { IIconWrapProps, TSize } from "./types";

import { theme } from "../theme";

interface IStyledIconWrapProps {
  $clickable?: IIconWrapProps["clickable"];
  $color?: IIconWrapProps["color"];
  $disabled?: IIconWrapProps["disabled"];
  $mr?: IIconWrapProps["mr"];
  $size: IIconWrapProps["size"];
}

const sizes: Record<TSize, string> = {
  lg: "16px",
  md: "14px",
  sm: "12px",
  xs: "10px",
};

const IconWrap = styled.span<IStyledIconWrapProps>`
  color: ${({ $color }): string => $color ?? "inherit"};
  cursor: ${({ $clickable = true }): string =>
    $clickable ? "pointer" : "unset"};
  font-size: ${({ $size }): string => sizes[$size]};
  margin: ${({ $mr = 0 }): string =>
    `${theme.spacing[0]} ${theme.spacing[$mr]} ${theme.spacing[0]} ${theme.spacing[0]}`};
  opacity: ${({ $disabled = false }): string => ($disabled ? "0.5" : "1")};
  pointer-events: ${({ $disabled = false }): string =>
    $disabled ? "none" : "auto"};
  vertical-align: middle;
`;

export { IconWrap };
