import { useFormikContext } from "formik";
import { useEffect } from "react";

import { Icon } from "../../icon";
import { Label } from "../label";
import { ErrorMessage, InputBox } from "../styles";
import type { IInputBaseProps } from "../types";

function InputContainer({
  alert,
  boxVariant = "regular",
  children,
  label,
  name,
  inputRequired,
}: Readonly<IInputBaseProps>): Readonly<React.JSX.Element> {
  const { errors, isSubmitting } = useFormikContext();

  useEffect((): void => {
    if (
      isSubmitting &&
      Object.keys(errors).findIndex(
        (errorKey): boolean => errorKey === name,
      ) === 0
    ) {
      const inputElement = document.getElementsByName(name)[0] as
        | Readonly<HTMLElement>
        | undefined;
      inputElement?.focus();
    }
  }, [errors, isSubmitting, name]);

  return (
    <InputBox $variant={boxVariant}>
      {label === undefined ? undefined : (
        <Label htmlFor={name} inputRequired={inputRequired}>
          {label}
        </Label>
      )}
      {children ?? undefined}
      {alert === undefined ? undefined : (
        <ErrorMessage $show>
          <Icon icon={"circle-exclamation"} iconType={"fa-light"} size={"sm"} />
          {alert}
        </ErrorMessage>
      )}
    </InputBox>
  );
}

export { InputContainer };
