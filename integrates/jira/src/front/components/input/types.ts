/* eslint-disable functional/prefer-immutable-types */
import type { FieldHookConfig } from "formik";
import type { ReactNode } from "react";
import type { DefaultTheme } from "styled-components";

import type { IListItemProps } from "../list-item/types";

type TInputBoxVariant = "box-inputs" | "regular";

interface ILabelProps {
  children?: ReactNode;
  htmlFor?: string;
  inputRequired?: boolean;
  tooltip?: string;
  weight?: keyof DefaultTheme["typography"]["weight"];
}

interface IInput extends ILabelProps {
  disabled?: boolean;
  id?: string;
  label?: string;
  placeholder?: string;
  suggestions?: string[];
}

interface IInputBaseProps extends IInput {
  alert?: string;
  children?: React.ReactNode;
  margin?: string;
  maxLength?: number;
  name: string;
  boxVariant?: TInputBoxVariant;
  width?: number | string;
}

interface ITextArea extends IInput {
  rows?: number;
}

interface ISelect extends IInput {
  items: IListItemProps[];
  handleOnChange?: (selection: IListItemProps) => void;
}

type TInputProps = FieldHookConfig<string> & IInput;
type TTextAreaProps = FieldHookConfig<string> & ITextArea;
type TSelectProps = FieldHookConfig<string> & ISelect;

export type {
  ILabelProps,
  TInputProps,
  TSelectProps,
  IInputBaseProps,
  TTextAreaProps,
};
