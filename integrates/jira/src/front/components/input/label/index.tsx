/* eslint-disable @typescript-eslint/no-magic-numbers */
import type React from "react";

import { Icon } from "../../icon";
import { theme } from "../../theme";
import { Text } from "../../typography/text";
import type { ILabelProps } from "../types";

function Label({
  children,
  htmlFor,
  inputRequired = false,
}: Readonly<ILabelProps>): Readonly<React.JSX.Element> {
  return (
    <label className={"flex"} htmlFor={htmlFor}>
      <Text
        color={theme.palette.gray[800]}
        display={"inline"}
        mr={0.25}
        size={"sm"}
      >
        {children}
      </Text>
      {inputRequired ? (
        <Icon
          color={theme.palette.error["500"]}
          icon={"asterisk"}
          mr={0.25}
          size={"xs"}
        />
      ) : undefined}
    </label>
  );
}

export { Label };
