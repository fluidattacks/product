/* eslint-disable functional/prefer-immutable-types */
/* eslint-disable max-statements */
/* eslint-disable react/forbid-component-props, react/jsx-props-no-spreading */
import { useFormikContext } from "formik";
import type React from "react";
import { useCallback, useEffect, useRef, useState } from "react";

import { IconButton } from "../../../icon-button";
import { ListItem } from "../../../list-item";
import type { IListItemProps } from "../../../list-item/types";
import { theme } from "../../../theme";
import { Text } from "../../../typography";
import { InputContainer } from "../../input-container";
import {
  ActionButtons,
  InputWrapper,
  SelectDropdownContainer,
} from "../../styles";
import type { TSelectProps } from "../../types";

function Select(props: TSelectProps): Readonly<React.JSX.Element> {
  const { disabled, placeholder, items, name, handleOnChange } = props;
  const { getFieldMeta, getFieldHelpers } = useFormikContext();
  const { error, value } = getFieldMeta<string | undefined>(name);
  const { setValue } = getFieldHelpers(name);
  const selectContainer = useRef<HTMLDivElement>(null);
  const [isOpen, setIsOpen] = useState(false);
  const [itemSelected, setItemSelected] = useState<string>();

  const openSelectOptions = useCallback((): void => {
    setIsOpen(!isOpen);
  }, [isOpen]);

  const handleClickItem = useCallback(
    (selection: IListItemProps): VoidFunction =>
      (): void => {
        if (handleOnChange) {
          handleOnChange(selection);
        }
        void setValue(selection.header ?? selection.value);
        setIsOpen(!isOpen);
      },
    [handleOnChange, isOpen, setValue],
  );

  useEffect((): VoidFunction => {
    if (value === undefined) {
      setItemSelected(placeholder);
    } else {
      setItemSelected(value);
    }
    function handleClickOutside(event: MouseEvent): void {
      if (
        selectContainer.current &&
        !selectContainer.current.contains(event.target as Node)
      ) {
        setIsOpen(false);
      }
    }

    document.addEventListener("mousedown", handleClickOutside, {
      passive: true,
    });

    return (): void => {
      document.removeEventListener("mousedown", handleClickOutside);
    };
  }, [items, placeholder, value]);

  return (
    <div ref={selectContainer}>
      <InputContainer alert={error} {...props}>
        <InputWrapper
          $show={error !== undefined}
          className={disabled === true ? "disabled" : ""}
          onClick={openSelectOptions}
        >
          <Text
            color={
              theme.palette.gray[itemSelected === placeholder ? "400" : "700"]
            }
            mr={1.25}
            size={"sm"}
          >
            {itemSelected}
          </Text>
          <ActionButtons className={"input-select"}>
            <IconButton
              actionButton
              color={theme.palette.gray[disabled === true ? "300" : "400"]}
              disabled={disabled}
              icon={isOpen ? "chevron-up" : "chevron-down"}
              iconType={"fa-light"}
              onClick={openSelectOptions}
              variant={"ghost"}
            />
          </ActionButtons>
        </InputWrapper>
        <SelectDropdownContainer $isOpen={isOpen}>
          {items.map((itemProps, index): JSX.Element => {
            const key = `${index}`;

            return (
              <ListItem
                disabled={itemProps.disabled}
                header={itemProps.header}
                key={key}
                onClick={handleClickItem(itemProps)}
                value={itemProps.value}
              />
            );
          })}
        </SelectDropdownContainer>
      </InputContainer>
    </div>
  );
}

export { Select };
