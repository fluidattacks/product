/* eslint-disable @typescript-eslint/no-magic-numbers */
/* eslint-disable functional/prefer-immutable-types */
import type { ButtonHTMLAttributes } from "react";
import type { DefaultTheme } from "styled-components";
import { css, styled } from "styled-components";

import type { TCssString } from "../utils";
import { variantBuilder } from "../utils";

type TVariant = "ghost" | "primary" | "tertiary";
type TDisplay = "block" | "inline-block" | "inline";
type TSpacing = keyof DefaultTheme["spacing"];

interface IStyledButtonProps extends ButtonHTMLAttributes<HTMLButtonElement> {
  $display?: TDisplay;
  $pb?: TSpacing;
  $pl?: TSpacing;
  $pr?: TSpacing;
  $pt?: TSpacing;
  $variant: TVariant | undefined;
}

const { getVariant } = variantBuilder(
  (theme: DefaultTheme): Record<TVariant, TCssString> => ({
    ghost: css`
      background: transparent;
      border: none;
      color: ${theme.palette.gray[800]};

      &:disabled {
        background: transparent;
        border: none;
        color: ${theme.palette.gray[300]};
      }

      &:hover:not([disabled]) {
        background-color: ${theme.palette.gray[100]};
        color: ${theme.palette.black};
      }
    `,
    primary: css`
      background: ${theme.palette.primary[500]};
      border: none;
      color: ${theme.palette.white};

      &:hover:not([disabled]) {
        background-color: ${theme.palette.primary[400]};
      }
    `,
    tertiary: css`
      padding: 9px 15px;
      background: transparent;
      border: 1px solid ${theme.palette.primary[500]};
      color: ${theme.palette.primary[500]};

      &:disabled {
        background: transparent;
        border: 1px solid ${theme.palette.gray[200]};
        color: ${theme.palette.gray[300]};
      }

      &:hover:not([disabled]) {
        background-color: ${theme.palette.primary[500]};
        color: ${theme.palette.white};
      }
    `,
  }),
);

const StyledButton = styled.button<IStyledButtonProps>`
  font-family: Roboto, sans-serif;
  font-size: ${({ theme }): string => theme.typography.text.sm};
  border-radius: 4px;
  display: ${({ $display = "inline-block" }): string => $display};
  font-weight: 400;
  line-height: ${({ theme }): string => theme.spacing[1.25]};
  padding: ${({ theme, $pt = 0, $pr = 0, $pb = 0, $pl = 0 }): string => {
    if ($pt !== 0 || $pb !== 0 || $pr !== 0 || $pl !== 0) {
      return `${theme.spacing[$pt]} ${theme.spacing[$pr]} ${theme.spacing[$pb]} ${theme.spacing[$pl]}`;
    }
    return "10px 16px";
  }};
  height: auto;
  text-align: start;
  text-decoration: unset;
  transition: all 0.25s ease;
  width: auto;
  cursor: pointer;

  &:disabled {
    cursor: not-allowed;
    background: ${({ theme }): string => theme.palette.gray[200]};
    border-color: ${({ theme }): string => theme.palette.gray[200]};
    color: ${({ theme }): string => theme.palette.gray[300]};
  }

  ${({ theme, $variant }): TCssString =>
    getVariant(theme, $variant ?? "primary")}
`;

export type { IStyledButtonProps, TDisplay, TVariant };
export { StyledButton };
