import type { DefaultTheme } from "styled-components";

type TDirection =
  | "column-reverse"
  | "column"
  | "reverse"
  | "row-reverse"
  | "row"
  | "unset";
type TDisplay =
  | "block"
  | "flex"
  | "ib"
  | "inline-block"
  | "inline-flex"
  | "inline"
  | "none";
type TSpacing = keyof DefaultTheme["spacing"];
type TAlign =
  | "center"
  | "end"
  | "flex-end"
  | "flex-start"
  | "start"
  | "stretch"
  | "unset";
type TJustify =
  | "center"
  | "end"
  | "flex-end"
  | "space-around"
  | "space-between"
  | "start"
  | "unset";
type TWrap = "nowrap" | "unset" | "wrap";

interface IContainerProps {
  alignItems?: TAlign;
  bgColor?: string;
  border?: string;
  borderColor?: string;
  borderRadius?: string;
  children?: React.ReactNode;
  display?: TDisplay;
  flexDirection?: TDirection;
  gap?: TSpacing;
  height?: string;
  justify?: TJustify;
  mb?: TSpacing;
  mt?: TSpacing;
  pb?: TSpacing;
  pl?: TSpacing;
  pr?: TSpacing;
  pt?: TSpacing;
  whiteSpace?: "normal" | "nowrap" | "pre-line" | "pre-wrap" | "pre";
  width?: string;
  wrap?: TWrap;
}

export type { IContainerProps, TAlign, TDisplay, TJustify };
