import type React from "react";
import type { DefaultTheme } from "styled-components";

type TAling = "center" | "end" | "start" | "unset";
type TDisplay = "block" | "inline-block" | "inline";
type TSize = "lg" | "md" | "sm" | "xs";
type TWeight = keyof DefaultTheme["typography"]["weight"];
type TSpacing = keyof DefaultTheme["spacing"];

interface ITypographyProps {
  children: React.ReactNode;
  color?: string;
  display?: TDisplay;
  ml?: TSpacing;
  mr?: TSpacing;
  mt?: TSpacing;
  size: TSize;
  textAlign?: TAling;
  weight?: TWeight;
}

type THeadingProps = ITypographyProps;

type TTextProps = ITypographyProps;

export type { THeadingProps, TTextProps, TSize };
