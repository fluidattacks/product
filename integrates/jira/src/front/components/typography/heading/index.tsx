import type React from "react";

import { StyledHeading } from "../styles";
import type { THeadingProps } from "../types";

function Heading({
  children,
  size,
  weight,
}: Readonly<THeadingProps>): Readonly<React.JSX.Element> {
  return (
    <StyledHeading $size={size} $weight={weight}>
      {children}
    </StyledHeading>
  );
}

export { Heading };
