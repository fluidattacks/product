import { styled } from "styled-components";

import { theme } from "../theme";

const StyledInput = styled.input<{ $isEmpty: boolean }>`
  background: none;
  border: none !important;
  box-shadow: none;
  box-sizing: border-box;
  color: ${({ $isEmpty }): string =>
    $isEmpty ? theme.palette.gray["400"] : theme.palette.gray["800"]};
  font-family: ${theme.typography.type.primary};
  font-size: ${theme.typography.text.sm};
  outline: none;
  width: 100%;
`;

export { StyledInput };
