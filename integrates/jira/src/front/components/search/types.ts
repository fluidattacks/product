import type { FieldHookConfig } from "formik";

interface ISearchProps {
  enableSearchBar?: boolean;
  handleOnKeyPressed?: (value: string) => void;
  handleOnSubmit?: (value: string) => void;
  placeHolder: string;
}

type TInputProps = FieldHookConfig<string> & ISearchProps;

export type { ISearchProps, TInputProps };
