import { CSVLink } from "react-csv";

import { Button } from "../../button";

interface ITableControlsProps {
  exportCsv: boolean;
  flattenedData: object[];
  csvName: string;
}

function TableControls({
  exportCsv,
  flattenedData,
  csvName,
}: Readonly<ITableControlsProps>): Readonly<React.JSX.Element> {
  return (
    <div>
      {exportCsv ? (
        <CSVLink data={flattenedData} filename={csvName}>
          <Button icon={"file-export"} id={"CSVExportBtn"} variant={"ghost"}>
            {"Export"}
          </Button>
        </CSVLink>
      ) : null}
    </div>
  );
}

export { TableControls };
