/* eslint-disable @typescript-eslint/no-magic-numbers */
/* eslint-disable functional/prefer-immutable-types */
import type { RowData, Table } from "@tanstack/react-table";
import { flexRender } from "@tanstack/react-table";

import { Container } from "../../container";
import { Loading } from "../../loading";
import { theme } from "../../theme";
import { Text } from "../../typography/text";

interface IBodyProps<TData extends RowData> {
  loadingData?: boolean;
  table: Table<TData>;
}

function Body<TData extends RowData>({
  loadingData,
  table,
}: Readonly<IBodyProps<TData>>): JSX.Element {
  if (loadingData === true) {
    return (
      <tbody>
        <tr className={"no-data-row"}>
          <td colSpan={table.getVisibleLeafColumns().length}>
            <Container
              alignItems={"center"}
              display={"flex"}
              justify={"center"}
              width={"100%"}
            >
              <Loading size={"lg"} />
            </Container>
          </td>
        </tr>
      </tbody>
    );
  }

  if (table.getRowModel().rows.length === 0) {
    return (
      <tbody>
        <tr className={"no-data-row"}>
          <td colSpan={table.getVisibleLeafColumns().length}>
            <Text
              color={theme.palette.gray[800]}
              size={"sm"}
              textAlign={"center"}
            >
              {"No data to display"}
            </Text>
          </td>
        </tr>
      </tbody>
    );
  }

  return (
    <tbody>
      {table.getRowModel().rows.map((row): JSX.Element => {
        return (
          <tr key={row.id}>
            {row.getVisibleCells().map(
              (cell): JSX.Element => (
                <td key={cell.id}>
                  <label>
                    <Container border={"none"}>
                      {flexRender(
                        cell.column.columnDef.cell,
                        cell.getContext(),
                      )}
                    </Container>
                  </label>
                </td>
              ),
            )}
          </tr>
        );
      })}
    </tbody>
  );
}

export { Body };
