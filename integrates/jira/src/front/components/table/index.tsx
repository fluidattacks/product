import {
  getCoreRowModel,
  getPaginationRowModel,
  getSortedRowModel,
  useReactTable,
} from "@tanstack/react-table";
import type {
  ColumnDef,
  PaginationState,
  Row,
  RowData,
  SortingState,
} from "@tanstack/react-table";
import { useCallback, useState } from "react";

import { Paginator } from "./paginator";
import { TableContainer } from "./styles";
import { Body } from "./table-body";
import { TableControls } from "./table-controls";
import { Head } from "./table-head";
import type { ITableProps } from "./types";

import { Container } from "../container";
import { Search } from "../search";
import { theme } from "../theme";

const MIN_ROWS_PAGINATOR = 10;
const defaultCsvConfig = {};
const defaultOptions = {};

function Table<TData extends RowData>({
  columns,
  csvConfig = defaultCsvConfig,
  data,
  loadingData,
  onNextPage,
  onSearch,
  options = defaultOptions,
}: Readonly<ITableProps<TData>>): Readonly<React.JSX.Element> {
  const [pagination, setPagination] = useState<PaginationState>({
    pageIndex: 0,
    pageSize: 10,
  });
  const [globalFilter, setGlobalFilter] = useState("");
  const [sorting, setSorting] = useState<SortingState>([]);

  const {
    enableSearchBar = true,
    hasGlobalFilter = true,
    hasNextPage = false,
    searchPlaceholder,
    size,
  } = options;
  const { export: csvExport = false, name: csvName = "Report" } = csvConfig;

  const globalFilterHandler = useCallback(
    (value: string): void => {
      setGlobalFilter(value);

      if (onSearch) {
        onSearch(value);
      }
    },
    [onSearch],
  );

  const filterFun = useCallback(
    (
      row: Readonly<Row<TData>>,
      columnId: string,
      filterValue: string,
    ): boolean => {
      return String(row.getValue(columnId))
        .toLowerCase()
        .includes(filterValue.toLowerCase());
    },
    [],
  );

  const table = useReactTable<TData>({
    autoResetAll: false,
    columns: columns as ColumnDef<TData>[],
    data,
    getCoreRowModel: getCoreRowModel(),
    getPaginationRowModel: getPaginationRowModel(),
    getSortedRowModel: getSortedRowModel(),
    globalFilterFn: filterFun,
    onGlobalFilterChange: setGlobalFilter,
    onPaginationChange: setPagination,
    onSortingChange: setSorting,
    state: {
      globalFilter: hasGlobalFilter ? globalFilter : null,
      pagination,
      sorting,
    },
  });

  return (
    <div style={{ backgroundColor: theme.palette.gray["50"], padding: "20px" }}>
      <Container
        alignItems={"center"}
        display={"flex"}
        justify={"space-between"}
        pb={1.25}
      >
        <Container
          alignItems={"center"}
          display={"flex"}
          gap={0.5}
          justify={"space-between"}
          width={"100%"}
        >
          <TableControls
            csvName={csvName}
            exportCsv={csvExport}
            flattenedData={data as object[]}
          />
          <Container bgColor={"#fff"} width={"250px"}>
            <Search
              enableSearchBar={enableSearchBar}
              handleOnKeyPressed={globalFilterHandler}
              placeHolder={searchPlaceholder ?? "Search"}
            />
          </Container>
        </Container>
      </Container>
      <TableContainer>
        <table>
          <Head table={table} />
          <Body loadingData={loadingData} table={table} />
        </table>
      </TableContainer>
      {table.getFilteredRowModel().rows.length > MIN_ROWS_PAGINATOR ? (
        <Paginator
          hasNextPage={hasNextPage}
          onNextPage={onNextPage}
          size={size ?? table.getFilteredRowModel().rows.length}
          table={table}
        />
      ) : undefined}
    </div>
  );
}

export { Table };
