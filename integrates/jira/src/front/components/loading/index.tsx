/* eslint-disable @typescript-eslint/no-magic-numbers */
/* eslint-disable functional/prefer-immutable-types */
import { Circle, CircleBg, LoadingContainer } from "./styles";
import type { ILoadingProps } from "./types";

import { Container } from "../container";
import { theme } from "../theme";
import { Text } from "../typography";

function Loading({ color = "red", label, size }: ILoadingProps): JSX.Element {
  return (
    <Container alignItems={"center"} display={"flex"}>
      <LoadingContainer $size={size} viewBox={"0 0 100 100"}>
        <CircleBg $color={color} cx={"50"} cy={"50"} r={"45"} />
        <Circle $color={color} cx={"50"} cy={"50"} r={"45"} />
      </LoadingContainer>
      <Text
        color={color === "red" ? theme.palette.gray[700] : theme.palette.white}
        ml={0.5}
        size={"sm"}
      >
        {label}
      </Text>
    </Container>
  );
}

export type { ILoadingProps };
export { Loading };
