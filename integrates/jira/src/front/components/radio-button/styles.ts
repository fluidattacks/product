/* eslint-disable @typescript-eslint/no-magic-numbers */
import { styled } from "styled-components";

import { theme } from "../theme";

const Container = styled.label`
  display: block;
  font-family: ${theme.typography.type.primary};
  font-size: ${theme.typography.text.sm};
  font-weight: 400;
  line-height: ${theme.typography.text.lg};
  text-align: left;
  position: relative;
  padding-left: 24px;
  cursor: pointer;

  input {
    opacity: 0;
    cursor: pointer;
    height: 0;
    width: 0;
  }

  .checkmark {
    position: absolute;
    top: 0;
    left: 0;
    height: 16px;
    width: 16px;
    background-color: #fff;
    border-radius: 50%;
    border: 1px solid ${theme.palette.gray[200]};
  }

  input:disabled ~ .checkmark {
    border: 1px solid ${theme.palette.gray[200]};
    background-color: ${theme.palette.gray[100]};
  }

  input:checked ~ .checkmark {
    background-color: #fff;
    border: 1px solid ${theme.palette.primary[500]};
  }

  input:not(:disabled):hover ~ .checkmark {
    background-color: #fff;
    border: 1px solid ${theme.palette.primary[500]};
  }

  .checkmark::after {
    content: "";
    position: absolute;
    display: none;
    top: 4px;
    left: 4px;
    width: 8px;
    height: 8px;
    border-radius: 50%;
    background: ${theme.palette.primary[500]};
  }

  &:hover input ~ .checkmark::after {
    background-color: ${theme.palette.primary[500]};
  }

  input:checked ~ .checkmark::after {
    display: block;
  }
`;

const CheckMark = styled.span.attrs({ className: "checkmark" })``;

export { CheckMark, Container };
