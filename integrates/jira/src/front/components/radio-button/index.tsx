import { Field } from "formik";
import type React from "react";

import { CheckMark, Container } from "./styles";
import type { IRadioButtonProps } from "./types";

function RadioButton({
  defaultChecked = false,
  disabled = false,
  label,
  name,
  onChange,
  onClick,
  value,
  variant = "input",
}: Readonly<IRadioButtonProps>): Readonly<React.JSX.Element> {
  if (variant === "input") {
    return (
      <Container>
        {label}
        <input
          aria-label={label}
          defaultChecked={defaultChecked}
          disabled={disabled}
          name={name}
          onChange={onChange}
          onClick={onClick}
          type={"radio"}
          value={value}
        />
        <CheckMark />
      </Container>
    );
  }

  return (
    <Container>
      {label}
      <Field name={name} type={"radio"} value={value} />
      <CheckMark />
    </Container>
  );
}

export { RadioButton };
