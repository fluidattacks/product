import type React from "react";

import { AlertContainer, MessageContainer, getIcon } from "./styles";
import type { IAlertAndNotifyBoxProps } from "./styles";

import { Container } from "../container";
import { Icon } from "../icon";

interface IAlertProps {
  variant?: IAlertAndNotifyBoxProps["$variant"];
}

function Alert({
  children,
  variant = "error",
}: Readonly<
  React.PropsWithChildren<IAlertProps>
>): Readonly<React.JSX.Element> {
  return (
    <Container display={"block"}>
      <AlertContainer $variant={variant}>
        <Icon icon={getIcon(variant)} iconType={"fa-light"} size={"lg"} />
        <MessageContainer>{children}</MessageContainer>
      </AlertContainer>
    </Container>
  );
}

export type { IAlertProps };
export { Alert };
