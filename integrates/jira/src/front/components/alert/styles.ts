/* eslint-disable @typescript-eslint/no-magic-numbers */
/* eslint-disable functional/prefer-immutable-types */
import type { IconName } from "@fortawesome/free-solid-svg-icons";
import { css, styled } from "styled-components";
import type { DefaultTheme } from "styled-components";

import type { TCssString } from "../utils";
import { variantBuilder } from "../utils";

type TAlertAndNotifyVariant = "error" | "info" | "success" | "warning";

interface IAlertAndNotifyBoxProps {
  $variant?: TAlertAndNotifyVariant;
}

function getIcon(variant: TAlertAndNotifyVariant): IconName {
  switch (variant) {
    case "warning":
      return "triangle-exclamation";
    case "info":
      return "circle-info";
    case "success":
      return "circle-check";
    default:
      return "circle-exclamation";
  }
}

const { getVariant } = variantBuilder(
  (theme: DefaultTheme): Record<TAlertAndNotifyVariant, TCssString> => ({
    error: css`
      &.alert {
        background: ${theme.palette.error["50"]};
        border: 1px solid ${theme.palette.error["500"]};
        color: ${theme.palette.error["700"]};
      }
    `,
    info: css`
      &.alert {
        background: ${theme.palette.info["50"]};
        border: 1px solid ${theme.palette.info["500"]};
        color: ${theme.palette.info["700"]};
      }
    `,
    success: css`
      &.alert {
        background: ${theme.palette.success["50"]};
        border: 1px solid ${theme.palette.success["500"]};
        color: ${theme.palette.success["700"]};
      }
    `,
    warning: css`
      &.alert {
        background: ${theme.palette.warning["50"]};
        border: 1px solid ${theme.palette.warning["500"]};
        color: ${theme.palette.warning["700"]};
      }
    `,
  }),
);

const AlertContainer = styled.div.attrs({
  className: "alert",
})<IAlertAndNotifyBoxProps>`
  align-items: center;
  border-radius: ${({ theme }): string => theme.spacing[0.25]};
  box-sizing: border-box;
  display: inline-flex;
  font-family: ${({ theme }): string => theme.typography.type.primary};
  font-size: ${({ theme }): string => theme.typography.text.sm};
  font-weight: ${({ theme }): string => theme.typography.weight.bold};
  line-height: ${({ theme }): string => theme.spacing[1.25]};
  min-height: 48px;
  min-width: 250px;
  width: 100%;
  padding: ${({ theme }): string => theme.spacing[0.75]};
  position: relative;
  ${({ theme, $variant }): TCssString => getVariant(theme, $variant ?? "error")}
`;

const MessageContainer = styled.div`
  padding-left: 8px;
  padding-right: 4px;
  width: 100%;
`;

export type { IAlertAndNotifyBoxProps };
export { getIcon, AlertContainer, MessageContainer };
