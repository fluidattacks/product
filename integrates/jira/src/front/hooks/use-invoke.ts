import { invoke } from "@forge/bridge";
import type { InvokePayload } from "@forge/bridge/out/types";
import type { QueryObserverResult } from "@tanstack/react-query";
import { keepPreviousData, useQuery } from "@tanstack/react-query";

interface IInvokeOptions {
  readonly payload?: InvokePayload;
  readonly skip?: boolean;
}

interface IInvokeResult<T> {
  readonly loading: boolean;
  readonly refetch: () => Promise<QueryObserverResult<T>>;
  readonly result: T | undefined;
}

function useInvoke<T = Record<string, unknown>>(
  resolverName: string,
  options?: IInvokeOptions,
): IInvokeResult<T> {
  const { data, isLoading, refetch } = useQuery({
    enabled: !(options?.skip ?? false),
    placeholderData: keepPreviousData,
    queryFn: async () => {
      return invoke<T>(resolverName, options?.payload);
    },
    queryKey: [resolverName, options?.payload],
  });

  return { loading: isLoading, refetch, result: data };
}

export { useInvoke };
