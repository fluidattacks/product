import { useCallback } from "react";

import { useInvoke } from "./use-invoke";
import { useLazyInvoke } from "./use-lazy-invoke";

import {
  GET_SETTINGS_RESOLVER,
  UPDATE_SETTINGS_RESOLVER,
} from "../../back/constants";
import type { ISettings } from "../../back/model/settings";

interface IUseSettings {
  readonly settings: ISettings | undefined;
  readonly updateSettings: (data: Readonly<ISettings>) => Promise<void>;
}

function useSettings(): IUseSettings {
  const { result, refetch } = useInvoke<ISettings>(GET_SETTINGS_RESOLVER);
  const [invokeUpdate] = useLazyInvoke(UPDATE_SETTINGS_RESOLVER);

  const updateSettings = useCallback(
    async (values: Readonly<ISettings>) => {
      await invokeUpdate(values);
      await refetch();
    },
    [invokeUpdate, refetch],
  );

  return { settings: result, updateSettings };
}

export { useSettings };
