import { router } from "@forge/bridge";
import type { GraphQLError } from "graphql";

function hasConfigurationErrors(
  errors: Readonly<GraphQLError[] | undefined>,
): boolean {
  const configurationErrors = ["Login required", "Missing configuration"];

  if (errors) {
    return configurationErrors.includes(errors[0].message);
  }
  return false;
}

function openLink(url: string) {
  return (): void => {
    void router.open(url);
  };
}

export { hasConfigurationErrors, openLink };
