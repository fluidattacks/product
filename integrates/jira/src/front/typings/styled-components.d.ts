/* eslint-disable @typescript-eslint/naming-convention */
/* eslint-disable @typescript-eslint/no-magic-numbers */
import "styled-components";

declare module "styled-components" {
  type ColorPalette =
    | "25"
    | "50"
    | "100"
    | "200"
    | "300"
    | "400"
    | "500"
    | "600"
    | "700"
    | "800"
    | "900";

  export interface DefaultTheme {
    spacing: {
      0: "0rem";
      0.25: "0.25rem";
      0.5: "0.5rem";
      0.75: "0.75rem";
      1: "1rem";
      1.25: "1.25rem";
      1.5: "1.5rem";
      1.75: "1.75rem";
      2: "2rem";
      2.25: "2.25rem";
      2.5: "2.5rem";
      3: "3rem";
      3.5: "3.5rem";
      4: "4rem";
      5: "5rem";
      6: "6rem";
    };
    typography: {
      type: {
        primary: string;
        mono: string;
      };
      heading: {
        lg: string;
        md: string;
        sm: string;
        xs: string;
      };
      text: {
        lg: string;
        md: string;
        sm: string;
        xs: string;
      };
      weight: {
        bold: string;
        regular: string;
      };
    };
    shadows: {
      none: string;
      sm: string;
      md: string;
      lg: string;
    };
    palette: {
      primary: Record<ColorPalette, string>;
      error: Record<ColorPalette, string>;
      info: Record<ColorPalette, string>;
      warning: Record<ColorPalette, string>;
      success: Record<ColorPalette, string>;
      gray: Record<ColorPalette, string>;
      black: string;
      white: string;
    };
  }
}
