import BugsnagPerformance from "@bugsnag/browser-performance";
import Bugsnag from "@bugsnag/js";
import BugsnagPluginReact from "@bugsnag/plugin-react";
import { CoralogixRum } from "@coralogix/browser";
import { QueryClient, QueryClientProvider } from "@tanstack/react-query";
import { StrictMode } from "react";
import { createRoot } from "react-dom/client";

import { App } from "./app";
import { ErrorBoundary } from "./components/error-boundary";
import { CustomThemeProvider } from "./components/theme";
import { AppContextProvider } from "./context/app-context";
import { GlobalStyle } from "./styles";

Bugsnag.start({
  apiKey: "01f58ce301ef01dac3dac3b0947cb9af",
  plugins: [new BugsnagPluginReact()],
});
BugsnagPerformance.start({ apiKey: "01f58ce301ef01dac3dac3b0947cb9af" });
CoralogixRum.init({
  application: "integrates-jira",
  coralogixDomain: "US2",
  // eslint-disable-next-line camelcase
  public_key: "cxtp_NQqL1usqbHZ4q8r3GvP3iE627uL3ph",
  version: "integrates_version",
});

const rootElement = document.getElementById("root");

if (rootElement) {
  const root = createRoot(rootElement);
  const queryClient = new QueryClient();

  root.render(
    <StrictMode>
      <CustomThemeProvider>
        <GlobalStyle />
        <ErrorBoundary>
          <AppContextProvider>
            <QueryClientProvider client={queryClient}>
              <App />
            </QueryClientProvider>
          </AppContextProvider>
        </ErrorBoundary>
      </CustomThemeProvider>
    </StrictMode>,
  );
}
