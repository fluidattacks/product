import type { ApolloQueryResult } from "@apollo/client";
import { invoke } from "@forge/bridge";
import { useInfiniteQuery } from "@tanstack/react-query";
import type { InfiniteData, QueryObserverResult } from "@tanstack/react-query";
import type { GraphQLError } from "graphql";
import { useState } from "react";

import { GET_GROUP_VULNERABILITIES_RESOLVER } from "../../../../back/constants";
import type { GetGroupVulnerabilitiesQuery } from "../../../../back/gql/graphql";
import { useDebouncedCallback } from "../../../hooks/use-debounced-callback";

const DEBOUNCE_WAIT_MS = 300;

type TVulnerability = NonNullable<
  GetGroupVulnerabilitiesQuery["group"]["vulnerabilities"]["edges"][0]
>["node"];

type TVulnerabilitiesResponse = ApolloQueryResult<
  GetGroupVulnerabilitiesQuery | undefined
>;

interface IUseVulnerabilities {
  readonly refetch: () => Promise<
    QueryObserverResult<InfiniteData<TVulnerabilitiesResponse>>
  >;
  readonly errors?: ApolloQueryResult<unknown>["errors"];
  readonly fetchNextPage: () => Promise<void>;
  readonly hasNextPage: boolean;
  readonly isLoading: boolean;
  readonly isRefetching: boolean;
  readonly setSearch: (search: string) => void;
  readonly vulnerabilities: TVulnerability[];
}

function useVulnerabilities(): IUseVulnerabilities {
  const [search, setSearch] = useState("");
  const debouncedSetSearch = useDebouncedCallback((value: string): void => {
    setSearch(value);
  }, DEBOUNCE_WAIT_MS);

  const { data, fetchNextPage, isLoading, isRefetching, refetch, status } =
    useInfiniteQuery<TVulnerabilitiesResponse>({
      getNextPageParam: (lastPage) => {
        return lastPage.data?.group.vulnerabilities.pageInfo.endCursor;
      },
      initialPageParam: undefined,
      queryFn: async ({ pageParam }) => {
        return invoke(GET_GROUP_VULNERABILITIES_RESOLVER, {
          after: pageParam,
          search,
        });
      },
      queryKey: [GET_GROUP_VULNERABILITIES_RESOLVER, search],
      refetchInterval: 600000,
    });

  const pages = status === "success" ? data.pages : [];
  const errors = pages
    .flatMap((page) => page.errors)
    .filter((error): error is GraphQLError => error !== undefined);
  const vulnerabilities = pages
    .flatMap((page) => page.data?.group.vulnerabilities.edges)
    .map((edge) => edge?.node)
    .filter((node): node is TVulnerability => Boolean(node));
  const hasNextPage = pages.map((page, index): boolean => {
    if (page.data && index < pages.length) {
      return page.data.group.vulnerabilities.pageInfo.hasNextPage;
    }
    return true;
  })[pages.length - 1];

  return {
    errors: errors.length ? errors : undefined,
    fetchNextPage: async (): Promise<void> => {
      if (hasNextPage) {
        await fetchNextPage();
      }
    },
    hasNextPage,
    isLoading,
    isRefetching,
    refetch,
    setSearch: debouncedSetSearch,
    vulnerabilities,
  };
}

export type { IUseVulnerabilities, TVulnerability };
export { useVulnerabilities };
