import { doc, link, paragraph } from "@atlaskit/adf-utils/builders";
import { CreateIssueModal } from "@forge/jira-bridge";
import { useCallback } from "react";

import { UPDATE_VULNERABILITY_ISSUE_RESOLVER } from "../../../../back/constants";
import { Button } from "../../../components/button";
import { useAppContext } from "../../../context/app-context";
import { useLazyInvoke } from "../../../hooks/use-lazy-invoke";
import type {
  IUseVulnerabilities,
  TVulnerability,
} from "../hooks/use-vulnerabilities";

interface ICreateIssueProps {
  readonly refetch: IUseVulnerabilities["refetch"];
  readonly vulnerability: TVulnerability;
}

function CreateIssue({
  refetch,
  vulnerability,
}: ICreateIssueProps): Readonly<React.JSX.Element> {
  const appContext = useAppContext();
  const [invokeUpdate, { loading }] = useLazyInvoke(
    UPDATE_VULNERABILITY_ISSUE_RESOLVER,
  );

  const handleClick = useCallback(() => {
    const { finding } = vulnerability;
    const criteriaIdSlice = 3;
    const findingNumber = finding.title.slice(0, criteriaIdSlice);
    const createIssueModal = new CreateIssueModal({
      context: {
        description: doc(
          paragraph(
            finding.description,
            link({
              href: [
                "https://help.fluidattacks.com/portal/en/kb/articles/criteria",
                `vulnerabilities-${findingNumber}`,
              ].join("-"),
            })("Learn more..."),
          ),
          paragraph(`Location: ${vulnerability.where}`),
          paragraph(`Specific: ${vulnerability.specific}`),
          paragraph(
            link({
              href: [
                "https://app.fluidattacks.com",
                `groups/${vulnerability.finding.groupName}`,
                `vulns/${vulnerability.finding.id}`,
              ].join("/"),
            })("View on platform"),
          ),
          paragraph(
            link({
              href: [
                "https://app.fluidattacks.com",
                `groups/${vulnerability.finding.groupName}`,
                `vulns/${vulnerability.finding.id}`,
                "evidence",
              ].join("/"),
            })("View evidence"),
          ),
        ),
        summary: finding.title,
      },
      onClose: ({ payload }): void => {
        async function update(): Promise<void> {
          await invokeUpdate({
            issueURL: `${appContext?.siteUrl}/issues/?jql=id=${payload[0].issueId}`,
            vulnerabilityId: vulnerability.id,
          });
          await refetch();
        }

        if (payload.length) {
          void update();
        }
      },
    });

    void createIssueModal.open();
  }, [appContext, invokeUpdate, refetch, vulnerability]);

  return (
    <Button
      disabled={loading}
      icon={"plus"}
      onClick={handleClick}
      variant={"ghost"}
    >
      {"Create"}
    </Button>
  );
}

export { CreateIssue };
