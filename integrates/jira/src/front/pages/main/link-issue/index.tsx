import { showFlag } from "@forge/bridge";
import type { ChangeEvent } from "react";
import { useCallback } from "react";
import type { Schema } from "yup";
import { object, string } from "yup";

import { UPDATE_VULNERABILITY_ISSUE_RESOLVER } from "../../../../back/constants";
import { Button } from "../../../components/button";
import { Form } from "../../../components/form";
import { Input } from "../../../components/input";
import { Modal, useModal } from "../../../components/modal";
import { useAppContext } from "../../../context/app-context";
import { useLazyInvoke } from "../../../hooks/use-lazy-invoke";
import { useIssueSearch } from "../hooks/use-issue-search";
import type {
  IUseVulnerabilities,
  TVulnerability,
} from "../hooks/use-vulnerabilities";

function getValidationSchema(
  suggestions: Readonly<string[]>,
): Readonly<Schema> {
  return object({
    query: string()
      .required()
      .oneOf(suggestions, "Select an issue from the list"),
  });
}

interface ILinkIssueProps {
  readonly refetch: IUseVulnerabilities["refetch"];
  readonly vulnerability: TVulnerability;
}

function LinkIssue({
  refetch,
  vulnerability,
}: ILinkIssueProps): Readonly<React.JSX.Element> {
  const appContext = useAppContext();

  const modal = useModal();
  const openModal = useCallback(() => {
    modal.open();
  }, [modal]);
  const closeModal = useCallback(() => {
    modal.close();
  }, [modal]);

  const { issues, setQuery } = useIssueSearch();
  const suggestions = issues.map((issue) => {
    return `${issue.key} - ${issue.summaryText}`;
  });
  const handleChange = useCallback(
    (event: Readonly<ChangeEvent<HTMLInputElement>>) => {
      if (!suggestions.includes(event.target.value)) {
        setQuery(event.target.value);
      }
    },
    [setQuery, suggestions],
  );

  const [invokeUpdate, { loading }] = useLazyInvoke(
    UPDATE_VULNERABILITY_ISSUE_RESOLVER,
  );
  const handleSubmit = useCallback(
    async (values: Readonly<{ query: string }>) => {
      const issueToLink = issues.find(
        (issue) => `${issue.key} - ${issue.summaryText}` === values.query,
      );

      if (issueToLink) {
        await invokeUpdate({
          issueURL: `${appContext?.siteUrl}/issues/?jql=id=${issueToLink.id}`,
          vulnerabilityId: vulnerability.id,
        });
        showFlag({
          id: "link-success",
          isAutoDismiss: true,
          title: "Vulnerability linked",
          type: "success",
        });
        closeModal();
        await refetch();
      }
    },
    [appContext, closeModal, invokeUpdate, issues, refetch, vulnerability],
  );

  return (
    <>
      <Button
        disabled={loading}
        icon={"link"}
        onClick={openModal}
        variant={"ghost"}
      >
        {"Link"}
      </Button>
      <Modal modalRef={modal} title={"Link issue"}>
        <Form
          initialValues={{ query: "" }}
          onClickCancel={closeModal}
          onSubmit={handleSubmit}
          title={""}
          validationSchema={getValidationSchema(suggestions)}
        >
          <Input
            name={"query"}
            onChangeCapture={handleChange}
            suggestions={suggestions}
          />
        </Form>
      </Modal>
    </>
  );
}

export { LinkIssue };
