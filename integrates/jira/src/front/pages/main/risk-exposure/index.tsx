import { Text } from "../../../components/typography";

const FRACTION_DIGITS = 3;
const MINIMUM = 0.001;

function formatPercentage(value: number): string {
  const formatter = new Intl.NumberFormat("en-IN", {
    maximumSignificantDigits: 2,
    style: "percent",
  });

  return formatter.format(
    Math.max(MINIMUM, parseFloat(value.toFixed(FRACTION_DIGITS))),
  );
}

interface IRiskExposureProps {
  readonly exposure: number;
}

function RiskExposure({
  exposure,
}: IRiskExposureProps): Readonly<React.JSX.Element> {
  const formattedRiskExposure = formatPercentage(exposure);

  return (
    <Text size={"sm"}>
      {exposure < MINIMUM ? "<" : undefined}&nbsp;
      {formattedRiskExposure}
    </Text>
  );
}

export { RiskExposure };
