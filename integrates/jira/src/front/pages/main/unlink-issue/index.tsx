import { showFlag } from "@forge/bridge";
import { useCallback } from "react";

import { UPDATE_VULNERABILITY_ISSUE_RESOLVER } from "../../../../back/constants";
import { Button } from "../../../components/button";
import { useLazyInvoke } from "../../../hooks/use-lazy-invoke";
import type {
  IUseVulnerabilities,
  TVulnerability,
} from "../hooks/use-vulnerabilities";

interface IUnlinkIssueProps {
  readonly refetch: IUseVulnerabilities["refetch"];
  readonly vulnerability: TVulnerability;
}

function UnlinkIssue({
  refetch,
  vulnerability,
}: IUnlinkIssueProps): Readonly<React.JSX.Element> {
  const [invokeUpdate, { loading }] = useLazyInvoke(
    UPDATE_VULNERABILITY_ISSUE_RESOLVER,
  );

  const handleClick = useCallback(() => {
    async function update(): Promise<void> {
      await invokeUpdate({ issueURL: "", vulnerabilityId: vulnerability.id });
      showFlag({
        id: "unlink-success",
        isAutoDismiss: true,
        title: "Vulnerability unlinked",
        type: "success",
      });
      await refetch();
    }

    void update();
  }, [invokeUpdate, refetch, vulnerability]);

  return (
    <Button
      disabled={loading}
      icon={"link-slash"}
      onClick={handleClick}
      variant={"ghost"}
    >
      {"Unlink"}
    </Button>
  );
}

export { UnlinkIssue };
