import type { ApolloQueryResult } from "@apollo/client";

import type { GetMeQuery } from "../../../../back/gql/graphql";
import { Alert } from "../../../components/alert";

interface IAuthenticationStatusProps {
  readonly loading: boolean;
  readonly result: ApolloQueryResult<GetMeQuery> | undefined;
}

function AuthenticationStatus({
  loading,
  result,
}: IAuthenticationStatusProps): Readonly<React.JSX.Element> {
  if (loading || result === undefined) {
    return <Alert variant={"info"}>{"Checking..."}</Alert>;
  }

  const { data, errors } = result;

  if (errors) {
    if (errors[0].message === "Missing configuration") {
      return (
        <Alert variant={"info"}>
          {"Welcome, provide an API token to get started"}
        </Alert>
      );
    }

    if (errors[0].message === "Login required") {
      return (
        <Alert variant={"error"}>
          {"The provided token is not valid or it could have expired"}
        </Alert>
      );
    }

    return (
      <Alert variant={"error"}>
        {"Error:"}
        {errors.map((error) => {
          return <p key={error.message}>{error.message}</p>;
        })}
      </Alert>
    );
  }

  return (
    <Alert variant={"success"}>
      {`Authenticated as ${data.me.userName}(${data.me.userEmail})`}
    </Alert>
  );
}

export { AuthenticationStatus };
