import { showFlag } from "@forge/bridge";
import { useCallback } from "react";
import { object, string } from "yup";

import type { GetMeQuery } from "../../../../back/gql/graphql";
import type { ISettings } from "../../../../back/model/settings";
import { Form } from "../../../components/form";
import { Select } from "../../../components/input";
import { useSettings } from "../../../hooks/use-settings";

const validationSchema = object({
  groupName: string().required(),
});

interface ISettingsForm {
  readonly data: GetMeQuery;
}

function SettingsForm({ data }: ISettingsForm): Readonly<React.JSX.Element> {
  const { organizations } = data.me;
  const { settings, updateSettings } = useSettings();

  const handleSubmit = useCallback(
    async (values: Readonly<ISettings>) => {
      await updateSettings(values);
      showFlag({
        id: "settings-saved",
        isAutoDismiss: true,
        title: "Settings saved.",
        type: "success",
      });
    },
    [updateSettings],
  );

  return (
    <Form
      enableReinitialize
      initialValues={settings ?? {}}
      onSubmit={handleSubmit}
      submitButtonLabel={"Save"}
      title={"Settings"}
      validationSchema={validationSchema}
    >
      <Select
        inputRequired
        items={organizations.flatMap((organization) => {
          return organization.groups.map((group) => {
            return { value: group.name };
          });
        })}
        label={"Group"}
        name={"groupName"}
        placeholder={"Choose a group"}
      />
    </Form>
  );
}

export { SettingsForm };
