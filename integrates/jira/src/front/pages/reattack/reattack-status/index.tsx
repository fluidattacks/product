import { VulnerabilityState } from "../../../../back/gql/graphql";
import { Tag } from "../../../components/tag";
import type { TVulnerability } from "../hooks/use-linked-vulnerabilities";

interface IReattackStatusProps {
  readonly vulnerability: TVulnerability;
}

function ReattackStatus({
  vulnerability,
}: IReattackStatusProps): Readonly<React.JSX.Element> {
  if (vulnerability.state === VulnerabilityState.Safe) {
    return <Tag variant={"green"}>{"Safe"}</Tag>;
  }

  if (vulnerability.verification === "Requested") {
    return <Tag variant={"orange"}>{"Reattack requested"}</Tag>;
  }

  if (vulnerability.verification === "On_hold") {
    return <Tag variant={"orange"}>{"Reattack on hold"}</Tag>;
  }

  return <Tag variant={"red"}>{"Vulnerable"}</Tag>;
}

export { ReattackStatus };
