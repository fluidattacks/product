import type { FullContext } from "@forge/bridge/out/types";

import { useLinkedVulnerabilities } from "./hooks/use-linked-vulnerabilities";
import { ReattackRequest } from "./reattack-request";
import { ReattackStatus } from "./reattack-status";
import { isReattackable } from "./utils";

import { Button } from "../../components/button";
import { Container } from "../../components/container";
import { Loading } from "../../components/loading";
import { Heading, Text } from "../../components/typography";
import { Welcome } from "../../components/welcome";
import { useAppContext } from "../../context/app-context";
import { hasConfigurationErrors, openLink } from "../../utils";

interface IIssueContext extends FullContext {
  extension: {
    /**
     * https://developer.atlassian.com/platform/forge/manifest-reference/modules/jira-issue-context/#extension-data
     */
    issue: { key: string; id: string; type: string; typeId: string };
  };
}

function Reattack(): Readonly<React.JSX.Element> {
  const appContext = useAppContext() as Readonly<IIssueContext>;
  const { loading, refetch, vulnerabilities, errors } =
    useLinkedVulnerabilities(
      `${appContext.siteUrl}/issues/?jql=id=${appContext.extension.issue.id}`,
    );

  if (loading) {
    return (
      <Container display={"flex"} justify={"center"}>
        <Loading />
      </Container>
    );
  }

  if (hasConfigurationErrors(errors)) {
    return <Welcome />;
  }

  if (vulnerabilities.length === 0) {
    return (
      <Text size={"sm"}>
        {"There are no vulnerabilities linked to this issue"}
      </Text>
    );
  }

  const reattackableVulnerabilities = vulnerabilities.filter(isReattackable);

  return (
    <>
      <Heading size={"xs"}>{"Linked vulnerabilities"}</Heading>
      <br />
      {vulnerabilities.map((vulnerability) => {
        return (
          <Container
            alignItems={"center"}
            display={"flex"}
            justify={"space-between"}
            key={vulnerability.id}
            pr={0.25}
            wrap={"wrap"}
          >
            <Text mt={0.5} size={"sm"}>
              <b>{vulnerability.finding.title}</b>
              <br />
              {"Location:"}&nbsp;{vulnerability.where}
              <br />
              {"Specific:"}&nbsp;{vulnerability.specific}
              <br />
              <ReattackStatus vulnerability={vulnerability} />
              <Button
                onClick={openLink(
                  [
                    "https://app.fluidattacks.com",
                    `groups/${vulnerability.finding.groupName}`,
                    `vulns/${vulnerability.finding.id}`,
                    `locations/${vulnerability.id}`,
                  ].join("/"),
                )}
                variant={"ghost"}
              >
                {"View on platform"}
              </Button>
            </Text>
          </Container>
        );
      })}
      <br />
      {reattackableVulnerabilities.length ? (
        <ReattackRequest
          refetch={refetch}
          vulnerabilities={reattackableVulnerabilities}
        />
      ) : undefined}
      <br />
      <Button
        icon={"headset"}
        onClick={openLink("https://calendly.com/fluidattacks/talk-to-a-hacker")}
        variant={"ghost"}
      >
        {"Talk to a Hacker"}
      </Button>
      <Button
        icon={"circle-question"}
        onClick={openLink("https://help.fluidattacks.com/portal/en/newticket")}
        variant={"ghost"}
      >
        {"Help"}
      </Button>
      <Button
        icon={"code"}
        onClick={openLink(
          "https://marketplace.visualstudio.com/items?itemName=FluidAttacks.fluidattacks",
        )}
        variant={"ghost"}
      >
        {"VS Code extension"}
      </Button>
    </>
  );
}

export { Reattack };
