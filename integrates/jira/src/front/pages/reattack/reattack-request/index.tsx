import type { FetchResult } from "@apollo/client";
import { showFlag } from "@forge/bridge";
import _ from "lodash";
import { useCallback, useState } from "react";
import { object, string } from "yup";

import { REQUEST_REATTACK_RESOLVER } from "../../../../back/constants";
import type { RequestReattackMutation } from "../../../../back/gql/graphql";
import { Button } from "../../../components/button";
import { Form } from "../../../components/form";
import { TextArea } from "../../../components/input";
import { useLazyInvoke } from "../../../hooks/use-lazy-invoke";
import type {
  IUseLinkedVulnerability,
  TVulnerability,
} from "../hooks/use-linked-vulnerabilities";

const MAX_JUSTIFICATION_LENGTH = 1000;
const MIN_JUSTIFICATION_LENGTH = 10;

const validationSchema = object({
  justification: string()
    .required()
    .max(MAX_JUSTIFICATION_LENGTH)
    .min(MIN_JUSTIFICATION_LENGTH),
});

interface IReattackRequestProps {
  readonly refetch: IUseLinkedVulnerability["refetch"];
  readonly vulnerabilities: TVulnerability[];
}

function ReattackRequest({
  refetch,
  vulnerabilities,
}: IReattackRequestProps): Readonly<React.JSX.Element> {
  const [requesting, setRequesting] = useState(false);
  const openRequestForm = useCallback(() => {
    setRequesting(true);
  }, []);
  const closeRequestForm = useCallback(() => {
    setRequesting(false);
  }, []);

  const [invokeUpdate] = useLazyInvoke<FetchResult<RequestReattackMutation>>(
    REQUEST_REATTACK_RESOLVER,
  );
  const handleSubmit = useCallback(
    async (values: Readonly<{ justification: string }>) => {
      const vulnerabilitiesByFinding = Object.entries(
        _.groupBy(vulnerabilities, (vulnerability) => {
          return vulnerability.finding.id;
        }),
      );

      const results = await Promise.all(
        vulnerabilitiesByFinding.map(async ([findingId, vulns]) => {
          const result = await invokeUpdate({
            findingId,
            justification: values.justification,
            vulnerabilities: vulns.map((vulnerability) => {
              return vulnerability.id;
            }),
          });
          return { findingTitle: vulns[0].finding.title, result };
        }),
      );

      results.forEach(({ findingTitle, result }) => {
        if (result.errors) {
          showFlag({
            description: result.errors
              .map((error) => error.message.replace("Exception -", ""))
              .join(", "),
            id: "reattack-error",
            isAutoDismiss: false,
            title: `Could not reattack ${findingTitle}`,
            type: "error",
          });
        } else {
          showFlag({
            id: "reattack-success",
            isAutoDismiss: true,
            title: `Reattack requested for ${findingTitle}`,
            type: "success",
          });
        }
      });
      await refetch();
    },
    [invokeUpdate, refetch, vulnerabilities],
  );

  return (
    <>
      {requesting ? undefined : (
        <Button onClick={openRequestForm}>{"Reattack"}</Button>
      )}
      {requesting ? (
        <Form
          enableReinitialize
          initialValues={{ justification: "" }}
          onClickCancel={closeRequestForm}
          onSubmit={handleSubmit}
          submitButtonLabel={"Reattack"}
          validationSchema={validationSchema}
        >
          <TextArea
            inputRequired
            label={"Justification"}
            name={"justification"}
            rows={3}
          />
        </Form>
      ) : undefined}
    </>
  );
}

export { ReattackRequest };
