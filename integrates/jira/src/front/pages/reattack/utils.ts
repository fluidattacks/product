import type { TVulnerability } from "./hooks/use-linked-vulnerabilities";

import { VulnerabilityState } from "../../../back/gql/graphql";

function isReattackable(vulnerability: Readonly<TVulnerability>): boolean {
  return (
    vulnerability.state === VulnerabilityState.Vulnerable &&
    [null, "Verified"].includes(vulnerability.verification)
  );
}

export { isReattackable };
