import { Agent } from "https";

import type { NormalizedCacheObject } from "@apollo/client";
import {
  ApolloClient,
  ApolloLink,
  InMemoryCache,
  createHttpLink,
} from "@apollo/client";
import { RetryLink } from "@apollo/client/link/retry";
import nodeFetch from "node-fetch";

import packageJson from "../../../package.json";

const { CI_COMMIT_REF_NAME } = process.env;

function getAppUrl(): string {
  if (CI_COMMIT_REF_NAME === "trunk") {
    return "https://app.fluidattacks.com";
  }

  return `https://${CI_COMMIT_REF_NAME}.app.fluidattacks.com`;
}

const httpsAgent = new Agent({ keepAlive: true });

function getClient(
  apiToken: string,
): Readonly<ApolloClient<NormalizedCacheObject>> {
  const retryLink = new RetryLink({
    attempts: {
      max: 5,
      retryIf: (error, operation): boolean => {
        if (error === undefined) {
          return false;
        }
        console.error("Retrying", operation.operationName, "due to", error);
        return true;
      },
    },
    delay: {
      initial: 300,
      jitter: false,
      max: Infinity,
    },
  });
  const httpLink = createHttpLink({
    fetch: nodeFetch as unknown as typeof fetch,
    fetchOptions: { agent: httpsAgent },
    headers: {
      authorization: `Bearer ${apiToken}`,
      "user-agent": `IntegratesJira/${packageJson.version}`,
    },
    uri: `${getAppUrl()}/api`,
  });

  return new ApolloClient({
    cache: new InMemoryCache(),
    defaultOptions: {
      mutate: {
        context: { fetchOptions: { timeout: 10000 } },
        errorPolicy: "all",
      },
      query: {
        context: { fetchOptions: { timeout: 5000 } },
        errorPolicy: "all",
      },
    },
    link: ApolloLink.from([retryLink, httpLink]),
  });
}

export { getClient };
