import { storage } from "@forge/api";

interface ISettings {
  autoClose?: boolean;
  groupName?: string;
}

const defaultSettings: Readonly<ISettings> = {
  autoClose: true,
};

async function getSettings(projectId: string): Promise<ISettings> {
  const settings = (await storage.get(`${projectId}#SETTINGS`)) as
    | ISettings
    | undefined;

  return { ...defaultSettings, ...settings };
}

async function updateSettings(
  projectId: string,
  settings: Readonly<ISettings>,
): Promise<void> {
  await storage.set(`${projectId}#SETTINGS`, settings);
}

export { getSettings, updateSettings };
export type { ISettings };
