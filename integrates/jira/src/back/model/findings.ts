import type { ApolloQueryResult } from "@apollo/client";

import { getClient } from "../api/client";
import { graphql } from "../gql";
import type { GetGroupFindingsQuery } from "../gql/graphql";

async function getFindings(
  apiToken: string,
  groupName: string,
): Promise<ApolloQueryResult<GetGroupFindingsQuery>> {
  const client = getClient(apiToken);
  const query = graphql(`
    query GetGroupFindings($groupName: String!) {
      group(groupName: $groupName) {
        findings {
          description
          id
          lastVulnerability
          maxOpenSeverityScore
          maxOpenSeverityScoreV4
          status
          title
          totalOpenCVSSF
          totalOpenCVSSFV4
        }
      }
    }
  `);

  return client.query({ query, variables: { groupName } });
}

export { getFindings };
