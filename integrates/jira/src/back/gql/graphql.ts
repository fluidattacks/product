/* eslint-disable */
import { TypedDocumentNode as DocumentNode } from '@graphql-typed-document-node/core';
export type Maybe<T> = T | null;
export type InputMaybe<T> = Maybe<T>;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
export type MakeEmpty<T extends { [key: string]: unknown }, K extends keyof T> = { [_ in K]?: never };
export type Incremental<T> = T | { [P in keyof T]?: P extends ' $fragmentName' | '__typename' ? T[P] : never };
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: { input: string; output: string; }
  String: { input: string; output: string; }
  Boolean: { input: boolean; output: boolean; }
  Int: { input: number; output: number; }
  Float: { input: number; output: number; }
  /** DateTime scalar definition */
  DateTime: { input: string; output: string; }
  /** GenericScalar definition */
  GenericScalar: { input: unknown; output: unknown; }
  /** JSONString scalar definition */
  JSONString: { input: string; output: string; }
  /** Upload scalar definition */
  Upload: { input: any; output: any; }
};

/** AWS Marketplace subscription type definition */
export type AwsSubscription = {
  __typename?: 'AWSSubscription';
  /** Number of contracted groups */
  contractedGroups: Maybe<Scalars['Int']['output']>;
  /** Current status of the subscription */
  status: AwsSubscriptionStatus;
  /** Number of groups owned by the user */
  usedGroups: Scalars['Int']['output'];
};

/** AWS Marketplace subscription status enum definition */
export enum AwsSubscriptionStatus {
  /** Subscription is active */
  Active = 'ACTIVE',
  /** Subscription expired */
  Expired = 'EXPIRED',
  /** Subscription payment failed */
  Failed = 'FAILED',
  /** Subscription is waiting for activation notification */
  Pending = 'PENDING'
}

/** Access Token type definition */
export type AccessToken = {
  __typename?: 'AccessToken';
  /** ID of the access token */
  id: Scalars['ID']['output'];
  /** Date of creation */
  issuedAt: Scalars['Int']['output'];
  /** Date of last use */
  lastUse: Maybe<Scalars['DateTime']['output']>;
  /** Name of the access token */
  name: Scalars['String']['output'];
};

/** Add consult Payload type definition */
export type AddConsultPayload = Payload & {
  __typename?: 'AddConsultPayload';
  /** Identifier of the new comment */
  commentId: Maybe<Scalars['String']['output']>;
  /** Did the query succeed? */
  success: Scalars['Boolean']['output'];
};

/** Add environment url Payload type definition */
export type AddEnvironmentUrlPayload = Payload & {
  __typename?: 'AddEnvironmentUrlPayload';
  /** Did the query succeed? */
  success: Scalars['Boolean']['output'];
  /** Newly added id url */
  urlId: Maybe<Scalars['String']['output']>;
};

/** Add Event Payload type definition */
export type AddEventPayload = Payload & {
  __typename?: 'AddEventPayload';
  /** Identifier of the new Event */
  eventId: Scalars['String']['output'];
  /** Did the query succeed? */
  success: Scalars['Boolean']['output'];
};

/** Simple Payload type definition */
export type AddOrganizationPayload = Payload & {
  __typename?: 'AddOrganizationPayload';
  /** Newly added Organization */
  organization: Maybe<Organization>;
  /** Did the query succeed? */
  success: Scalars['Boolean']['output'];
};

/** Add Docker Image Payload type definition */
export type AddRootDockerImagePayload = Payload & {
  __typename?: 'AddRootDockerImagePayload';
  /** Is the job to generate the SBOM queued? */
  sbomJobQueued: Scalars['Boolean']['output'];
  /** Did the query succeed? */
  success: Scalars['Boolean']['output'];
  /** Newly added id url */
  uri: Scalars['String']['output'];
};

/** Add root Payload type definition */
export type AddRootPayload = Payload & {
  __typename?: 'AddRootPayload';
  /** Newly added id root */
  rootId: Maybe<Scalars['String']['output']>;
  /** Did the query succeed? */
  success: Scalars['Boolean']['output'];
};

/** Add stakeholder Payload type definition */
export type AddStakeholderPayload = Payload & {
  __typename?: 'AddStakeholderPayload';
  /** Email address of the potential new Stakeholder */
  email: Maybe<Scalars['String']['output']>;
  /** Did the query succeed? */
  success: Scalars['Boolean']['output'];
};

/** Azure issues integration type definition */
export type AzureIssuesIntegration = {
  __typename?: 'AzureIssuesIntegration';
  /** Unique name of the user to assign the issue to */
  assignedTo: Maybe<Scalars['String']['output']>;
  /** Name of the configured organization */
  azureOrganization: Maybe<Scalars['String']['output']>;
  /** Name of the configured project */
  azureProject: Maybe<Scalars['String']['output']>;
  /** Date when the integration was set up */
  connectionDate: Maybe<Scalars['DateTime']['output']>;
  /** Whether to create issues for new reports */
  issueAutomationEnabled: Maybe<Scalars['Boolean']['output']>;
  /** List of Azure DevOps organization names */
  organizationNames: Array<Scalars['String']['output']>;
  /** List of Azure DevOps project members */
  projectMembers: Array<AzureProjectMember>;
  /** List of Azure DevOps project names */
  projectNames: Array<Scalars['String']['output']>;
  /** Tags to assign to new issues */
  tags: Maybe<Array<Scalars['String']['output']>>;
};


/** Azure issues integration type definition */
export type AzureIssuesIntegrationProjectMembersArgs = {
  azureOrganization: Scalars['String']['input'];
  azureProject: Scalars['String']['input'];
};


/** Azure issues integration type definition */
export type AzureIssuesIntegrationProjectNamesArgs = {
  azureOrganization: Scalars['String']['input'];
};

/** Azure project member type definition */
export type AzureProjectMember = {
  __typename?: 'AzureProjectMember';
  /** Display name of the project member */
  displayName: Scalars['String']['output'];
  /** ID of the project member */
  id: Scalars['String']['output'];
  /** Unique name of the project member */
  uniqueName: Scalars['String']['output'];
};

/** Billing type definition */
export type Billing = {
  __typename?: 'Billing';
  /** Billing prices */
  prices: Prices;
};

/** Git root cloning status */
export enum CloningStatus {
  /** The repository is being cloned */
  Cloning = 'CLONING',
  /** Something went wrong with the cloning */
  Failed = 'FAILED',
  /** The cloning was successful */
  Ok = 'OK',
  /** A queued machine run to check this root */
  Queued = 'QUEUED',
  /**
   * Is the initial state when creating a root meaning
   * it has not yet been cloned or is glued for this action
   */
  Unknown = 'UNKNOWN'
}

/** Definition for language distribution in a repository */
export type CodeLanguages = {
  __typename?: 'CodeLanguages';
  /** Name of the language */
  language: Scalars['String']['output'];
  /** Number of lines of code */
  loc: Scalars['Int']['output'];
};

/** Consult type definition */
export type Consult = {
  __typename?: 'Consult';
  /** Body of the comment/consult */
  content: Scalars['String']['output'];
  /** Datetime of creation in the format `YYYY/MM/DD hh:mm:ss` */
  created: Scalars['String']['output'];
  /** Email address of the user who made the comment */
  email: Scalars['String']['output'];
  /** Full name of the user who made the comment */
  fullName: Scalars['String']['output'];
  /** Identifier of the comment */
  id: Scalars['String']['output'];
  /** Datetime of last modification in the format `YYYY/MM/DD hh:mm:ss` */
  modified: Scalars['String']['output'];
  /** Identifier of the parent comment it replies to, 0 otherwise */
  parentComment: Scalars['String']['output'];
};

/** Types of authentication to a Git repository */
export enum CredentialType {
  /**
   * Allows IAM principals in another AWS account to
   * assume the role
   */
  Awsrole = 'AWSROLE',
  /** Access is granted using a username and password */
  Https = 'HTTPS',
  /**
   * Connection without a username and password, using
   * a private key
   */
  Ssh = 'SSH'
}

/** Credentials type definition */
export type Credentials = {
  __typename?: 'Credentials';
  /** ARN of the AWS role */
  arn: Maybe<Scalars['String']['output']>;
  /** Whether credential is PAT (personal access token) integration */
  azureOrganization: Maybe<Scalars['String']['output']>;
  /** ID of the credential */
  id: Scalars['String']['output'];
  /** List of possible integration repositories */
  integrationRepositories: Array<Maybe<OrganizationIntegrationRepositories>>;
  /** Whether credential is a personal access token (PAT) integration */
  isPat: Scalars['Boolean']['output'];
  /** Whether credential is a token */
  isToken: Scalars['Boolean']['output'];
  /** Returns the key of the credential type for SSH protocol */
  key: Maybe<Scalars['String']['output']>;
  /** Name of the credential */
  name: Scalars['String']['output'];
  /** Type of OAuth authentication of the credential */
  oauthType: Scalars['String']['output'];
  /** The organization associated with the credential */
  organization: Organization;
  /** Owner of the credential */
  owner: Scalars['String']['output'];
  /** Returns the key of the password of the credential type for HTTPS protocol */
  password: Maybe<Scalars['String']['output']>;
  /** Https token which allows identification and login confirmation */
  token: Maybe<Scalars['String']['output']>;
  /** Type of authentication of the credential, which can be HTTPS, SSH, or OAuth */
  type: Scalars['String']['output'];
  /** User id */
  user: Maybe<Scalars['String']['output']>;
};

/** Credentials definition */
export type CredentialsInput = {
  /** ARN of the AWS role */
  arn?: InputMaybe<Scalars['String']['input']>;
  /** Name of the (azure) organization to which a PAT associates the credential */
  azureOrganization?: InputMaybe<Scalars['String']['input']>;
  /** Whether credential is pat (Personal Access Token) integration */
  isPat?: InputMaybe<Scalars['Boolean']['input']>;
  /** Returns the key of the credential type SSH protocol */
  key?: InputMaybe<Scalars['String']['input']>;
  /** Name of the credential */
  name?: InputMaybe<Scalars['String']['input']>;
  /** Returns the key of the password of the credential type HTTPS protocol */
  password?: InputMaybe<Scalars['String']['input']>;
  /**
   * Https token,
   * which allows identification and login confirmation
   */
  token?: InputMaybe<Scalars['String']['input']>;
  /**
   * Type of authentication,
   * which can be HTTPS,
   * SSH,
   * or OAuth
   */
  type?: InputMaybe<CredentialType>;
  /** User id */
  user?: InputMaybe<Scalars['String']['input']>;
};

/** Criteria data */
export type Criteria = {
  __typename?: 'Criteria';
  /** Vulnerability main data in english */
  en: CriteriaMainData;
  /** Vulnerability main data in spanish */
  es: CriteriaMainData;
  /** Finding number */
  findingNumber: Scalars['String']['output'];
  /** Vulnerability expected remediation time */
  remediationTime: Scalars['String']['output'];
  /** Vulnerability requirements */
  requirements: Array<Maybe<Scalars['String']['output']>>;
  /**
   * Vulnerability score
   * @deprecated This value is deprecated and will be removed after 2025/04/26
   */
  score: CriteriaScore;
  /** Vulnerability score cvss4.0 */
  scoreV4: CriteriaScoreV4;
};

/** Vulnerability criteria base */
export type CriteriaBaseScore = {
  __typename?: 'CriteriaBaseScore';
  /** Vulnerability attack complexity */
  attackComplexity: Scalars['String']['output'];
  /** Vulnerability attack vector */
  attackVector: Scalars['String']['output'];
  /** Vulnerability availability */
  availability: Scalars['String']['output'];
  /** Vulnerability confidentiality */
  confidentiality: Scalars['String']['output'];
  /** Vulnerability integrity */
  integrity: Scalars['String']['output'];
  /** Vulnerability privileges required */
  privilegesRequired: Scalars['String']['output'];
  /** Vulnerability scope */
  scope: Scalars['String']['output'];
  /** Vulnerability user interaction */
  userInteraction: Scalars['String']['output'];
};

/** Vulnerability criteria base cvss4.0 */
export type CriteriaBaseScoreV4 = {
  __typename?: 'CriteriaBaseScoreV4';
  /** Vulnerability attack complexity */
  attackComplexity: Scalars['String']['output'];
  /** Vulnerability attack requirements */
  attackRequirements: Scalars['String']['output'];
  /** Vulnerability attack vector */
  attackVector: Scalars['String']['output'];
  /** Vulnerability availability in the subsequent system */
  availabilitySa: Scalars['String']['output'];
  /** Vulnerability availability in the vulnerable system */
  availabilityVa: Scalars['String']['output'];
  /** Vulnerability confidentiality in the vulnerable system */
  confidentialitySc: Scalars['String']['output'];
  /** Vulnerability integrity in the subsequent system */
  confidentialityVc: Scalars['String']['output'];
  /** Vulnerability confidentiality in the subsequent system */
  integritySi: Scalars['String']['output'];
  /** Vulnerability integrity in the vulnerable system */
  integrityVi: Scalars['String']['output'];
  /** Vulnerability privileges required */
  privilegesRequired: Scalars['String']['output'];
  /** Vulnerability user interaction */
  userInteraction: Scalars['String']['output'];
};

/** Vulnerabilities criteria Connection type definition */
export type CriteriaConnection = {
  __typename?: 'CriteriaConnection';
  /** A list of Vulnerabilities criteria edges */
  edges: Array<CriteriaEdge>;
  /** Information to aid in pagination */
  pageInfo: PageInfo;
  /** Total packages found */
  total: Scalars['Int']['output'];
};

/** Criteria Edge type definition */
export type CriteriaEdge = Edge & {
  __typename?: 'CriteriaEdge';
  /** The cursor for the edge */
  cursor: Scalars['String']['output'];
  /** The node with the payload */
  node: Criteria;
};

/** Vulnerability criteria main data */
export type CriteriaMainData = {
  __typename?: 'CriteriaMainData';
  /** Vulnerability description */
  description: Scalars['String']['output'];
  /** Vulnerability impact */
  impact: Scalars['String']['output'];
  /** Recommendation to solve the vulnerability */
  recommendation: Scalars['String']['output'];
  /** Vulnerability threat */
  threat: Scalars['String']['output'];
  /** Vulnerability title */
  title: Scalars['String']['output'];
};

/** Requirement data */
export type CriteriaRequirement = {
  __typename?: 'CriteriaRequirement';
  /** Requirement category */
  category: Scalars['String']['output'];
  /** Requirement main data in english */
  en: CriteriaRequirementMainData;
  /** Requirement main data in spanish */
  es: CriteriaRequirementMainData;
  /** Requirement references */
  references: Array<Maybe<Scalars['String']['output']>>;
  /** Requirement number */
  requirementNumber: Scalars['String']['output'];
  /** Requirement support */
  supportedIn: CriteriaRequirementSupport;
};

/** Criteria Requirement Edge type definition */
export type CriteriaRequirementEdge = Edge & {
  __typename?: 'CriteriaRequirementEdge';
  /** The cursor for the edge */
  cursor: Scalars['String']['output'];
  /** The node with the payload */
  node: CriteriaRequirement;
};

/** Requirement criteria main data */
export type CriteriaRequirementMainData = {
  __typename?: 'CriteriaRequirementMainData';
  /** Requirement description */
  description: Scalars['String']['output'];
  /** Requirement summary */
  summary: Scalars['String']['output'];
  /** Requirement title */
  title: Scalars['String']['output'];
};

/** Requirement criteria main data */
export type CriteriaRequirementSupport = {
  __typename?: 'CriteriaRequirementSupport';
  /** Support advanced */
  advanced: Scalars['Boolean']['output'];
  /** Support essential */
  essential: Scalars['Boolean']['output'];
};

/** Requirements criteria Connection type definition */
export type CriteriaRequirementsConnection = {
  __typename?: 'CriteriaRequirementsConnection';
  /** A list of Requirements criteria edges */
  edges: Array<CriteriaRequirementEdge>;
  /** Information to aid in pagination */
  pageInfo: PageInfo;
  /** Total packages found */
  total: Scalars['Int']['output'];
};

/** Vulnerability criteria score */
export type CriteriaScore = {
  __typename?: 'CriteriaScore';
  /** Vulnerability base score */
  base: CriteriaBaseScore;
  /** Vulnerability temporal score */
  temporal: CriteriaTemporalScore;
};

/** Vulnerability criteria score cvss4.0 */
export type CriteriaScoreV4 = {
  __typename?: 'CriteriaScoreV4';
  /** Vulnerability base score */
  base: CriteriaBaseScoreV4;
  /** Vulnerability threat score */
  threat: CriteriaThreatScore;
};

/** Vulnerability criteria temporal */
export type CriteriaTemporalScore = {
  __typename?: 'CriteriaTemporalScore';
  /** Vulnerability exploit code maturity */
  exploitCodeMaturity: Scalars['String']['output'];
  /** Vulnerability remediation level */
  remediationLevel: Scalars['String']['output'];
  /** Vulnerability report confidence */
  reportConfidence: Scalars['String']['output'];
};

/** Vulnerability criteria threat */
export type CriteriaThreatScore = {
  __typename?: 'CriteriaThreatScore';
  /** Vulnerability exploit maturity */
  exploitMaturity: Scalars['String']['output'];
};

/** Docker image history */
export type DockerImageHistory = {
  __typename?: 'DockerImageHistory';
  /** Comments in the layer */
  comment: Maybe<Scalars['String']['output']>;
  /** Instruction within the Dockerfile that created the layer */
  created: Maybe<Scalars['DateTime']['output']>;
  /** Instruction within the Dockerfile that created the layer */
  createdBy: Maybe<Scalars['String']['output']>;
  /** The layer does not contain files */
  emptyLayer: Scalars['Boolean']['output'];
};

/** Document file metadata definition */
export type DocumentFile = {
  __typename?: 'DocumentFile';
  /** File name of Document */
  fileName: Maybe<Scalars['String']['output']>;
  /** Date when document was created or updated */
  modifiedDate: Maybe<Scalars['String']['output']>;
};

/** Download file Payload type definition */
export type DownloadFilePayload = Payload & {
  __typename?: 'DownloadFilePayload';
  /** Did the query succeed? */
  success: Scalars['Boolean']['output'];
  /** URL to the file */
  url: Scalars['String']['output'];
};

/** Generic Edge interface definition */
export type Edge = {
  /** Reference an item into a list */
  cursor: Scalars['String']['output'];
};

/** Root environment url definition */
export type EnvironmentUrl = {
  /** Date of creation */
  createdAt: Maybe<Scalars['DateTime']['output']>;
  /** GitRoot environment url */
  id: Scalars['String']['output'];
  /** Environment url */
  url: Scalars['String']['output'];
};

/** Event type definition */
export type Event = {
  __typename?: 'Event';
  /**
   * Vulnerabilities requested for a reattack that had to be put on hold
   * because of this Event
   */
  affectedReattacks: Array<Vulnerability>;
  /** ID of the organization where the event was created */
  client: Maybe<Scalars['String']['output']>;
  /** Datetime of solving of the Event, in the format yyyy-MM-ddThh:mm:ssZ */
  closingDate: Maybe<Scalars['String']['output']>;
  /** Comment/consult array regarding the event */
  consulting: Maybe<Array<Consult>>;
  /** Comment laying out or explaining the event */
  detail: Scalars['String']['output'];
  /** Environment URL where the event was reported */
  environment: Maybe<Scalars['String']['output']>;
  /** Date the event was created with the format `yyyy-MM-ddThh:mm:ssZ` */
  eventDate: Scalars['String']['output'];
  /** Current status of the Event which can be: solved, unsolved or pending */
  eventStatus: Scalars['String']['output'];
  /** The kind of event that happened */
  eventType: EventType;
  /**
   * Evidence of the event where shows us how it is no
   * possible to continue with analyse the inputs
   */
  evidences: EventEvidence;
  /** Name of the group linked to the event */
  groupName: Scalars['String']['output'];
  /** Person who reported the event */
  hacker: Scalars['String']['output'];
  /** Identifier of the event */
  id: Scalars['String']['output'];
  /** Name of the organization linked to the event */
  organization: Scalars['String']['output'];
  /** Description for other solving reason */
  otherSolvingReason: Maybe<Scalars['String']['output']>;
  /** Nickname of the root where the event was reported */
  root: Maybe<Root>;
  /** Reason of solving the Event */
  solvingReason: Maybe<SolveEventReason>;
};

/** Event Edge type definition */
export type EventEdge = Edge & {
  __typename?: 'EventEdge';
  /** The cursor for the edge */
  cursor: Scalars['String']['output'];
  /** The node with the payload */
  node: Event;
};

/** Event evidence type definition */
export type EventEvidence = {
  __typename?: 'EventEvidence';
  /** Evidence file 1 */
  file1: Maybe<EventEvidenceItem>;
  /** Evidence image 1 */
  image1: Maybe<EventEvidenceItem>;
  /** Evidence image 2 */
  image2: Maybe<EventEvidenceItem>;
  /** Evidence image 3 */
  image3: Maybe<EventEvidenceItem>;
  /** Evidence image 4 */
  image4: Maybe<EventEvidenceItem>;
  /** Evidence image 5 */
  image5: Maybe<EventEvidenceItem>;
  /** Evidence image 6 */
  image6: Maybe<EventEvidenceItem>;
};

/** Event evidence item type definition */
export type EventEvidenceItem = {
  __typename?: 'EventEvidenceItem';
  /** Upload date in the format yyyy-MM-dd hh:mm:ss */
  date: Maybe<Scalars['String']['output']>;
  /** Filename of the event evidence */
  fileName: Maybe<Scalars['String']['output']>;
};

/** Types of Event evidence */
export enum EventEvidenceType {
  /** File 1 */
  File_1 = 'FILE_1',
  /** Image 1 */
  Image_1 = 'IMAGE_1',
  /** Image 2 */
  Image_2 = 'IMAGE_2',
  /** Image 3 */
  Image_3 = 'IMAGE_3',
  /** Image 4 */
  Image_4 = 'IMAGE_4',
  /** Image 5 */
  Image_5 = 'IMAGE_5',
  /** Image 6 */
  Image_6 = 'IMAGE_6'
}

/** Event type enum definition */
export enum EventType {
  /**
   * A specific test needs to be performed, affecting
   * availability or integrity, requiring customer
   * permission
   */
  AuthorizationSpecialAttack = 'AUTHORIZATION_SPECIAL_ATTACK',
  /** The client cancels a project milestone */
  ClientCancelsProjectMilestone = 'CLIENT_CANCELS_PROJECT_MILESTONE',
  /**
   * The client explicitly suspends the project for a
   * specified time or excludes a particular part of
   * the ToE
   */
  ClientExplicitlySuspendsProject = 'CLIENT_EXPLICITLY_SUSPENDS_PROJECT',
  /** Some repository does not clone */
  CloningIssues = 'CLONING_ISSUES',
  /**
   * Repository credentials issues, no credentials
   * exists, or existing ones are not valid
   */
  CredentialIssues = 'CREDENTIAL_ISSUES',
  /**
   * Data update required, reset user credentials
   * or data changes to consume a service
   */
  DataUpdateRequired = 'DATA_UPDATE_REQUIRED',
  /**
   * Problems in the environment, either because
   * it does not open or because some flow inside
   * it has functional problems
   */
  EnvironmentIssues = 'ENVIRONMENT_ISSUES',
  /**
   * Unable to install or error when installed
   * on mobile or desktop application
   */
  InstallerIssues = 'INSTALLER_ISSUES',
  /** Lack of supplies other than credentials */
  MissingSupplies = 'MISSING_SUPPLIES',
  /** The network port no longer has internet access */
  NetworkAccessIssues = 'NETWORK_ACCESS_ISSUES',
  /** Any other problem not covered by the other categories */
  Other = 'OTHER',
  /** Problems with connection methods */
  RemoteAccessIssues = 'REMOTE_ACCESS_ISSUES',
  /**
   * In Continuous Hacking, there are services different
   * form those agreed upon or non-existent functionalities
   */
  ToeDiffersApproved = 'TOE_DIFFERS_APPROVED',
  /** Fails on the VPN connection */
  VpnIssues = 'VPN_ISSUES'
}

/** Events Connection type definition */
export type EventsConnection = {
  __typename?: 'EventsConnection';
  /** A list of Findings edges */
  edges: Array<EventEdge>;
  /** Information to aid in pagination */
  pageInfo: PageInfo;
  /** Total findings found */
  total: Maybe<Scalars['Int']['output']>;
};

/** Evidence description enum definition */
export enum EvidenceDescriptionType {
  /** Animated exploitation */
  Animation = 'ANIMATION',
  /** Evidence 1 */
  Evidence1 = 'EVIDENCE1',
  /** Evidence 2 */
  Evidence2 = 'EVIDENCE2',
  /** Evidence 3 */
  Evidence3 = 'EVIDENCE3',
  /** Evidence 4 */
  Evidence4 = 'EVIDENCE4',
  /** Evidence 5 */
  Evidence5 = 'EVIDENCE5',
  /** Exploitation */
  Exploitation = 'EXPLOITATION'
}

/** Evidence type enum definition */
export enum EvidenceType {
  /** Animated exploitation */
  Animation = 'ANIMATION',
  /** Evidence 1 */
  Evidence1 = 'EVIDENCE1',
  /** Evidence 2 */
  Evidence2 = 'EVIDENCE2',
  /** Evidence 3 */
  Evidence3 = 'EVIDENCE3',
  /** Evidence 4 */
  Evidence4 = 'EVIDENCE4',
  /** Evidence 5 */
  Evidence5 = 'EVIDENCE5',
  /** Exploitation */
  Exploitation = 'EXPLOITATION',
  /** Records */
  Records = 'RECORDS'
}

/** Execution Edge type definition */
export type ExecutionEdge = Edge & {
  __typename?: 'ExecutionEdge';
  /** The cursor for the edge */
  cursor: Scalars['String']['output'];
  /** The node with the payload */
  node: ForcesExecution;
};

/** Execution Vulnerabilities definition */
export type ExecutionVulnerabilities = {
  __typename?: 'ExecutionVulnerabilities';
  /** Array of accepted vulnerabilities */
  accepted: Maybe<Array<ExploitResult>>;
  /** Array of closed vulnerabilities */
  closed: Maybe<Array<ExploitResult>>;
  /** Number of accepted vulnerabilities */
  numOfAcceptedVulnerabilities: Maybe<Scalars['Int']['output']>;
  /** Number of closed vulnerabilities */
  numOfClosedVulnerabilities: Maybe<Scalars['Int']['output']>;
  /** Number of managed vulnerabilities */
  numOfManagedVulnerabilities: Maybe<Scalars['Int']['output']>;
  /** Number of open vulnerabilities */
  numOfOpenVulnerabilities: Maybe<Scalars['Int']['output']>;
  /** Number of unmanaged vulnerabilities */
  numOfUnManagedVulnerabilities: Maybe<Scalars['Int']['output']>;
  /** Array of open vulnerabilities */
  open: Maybe<Array<ExploitResult>>;
};

/** Execution Vulnerabilities input definition */
export type ExecutionVulnerabilitiesInput = {
  /** Accepted vulnerabilities detected in the execution of Forces */
  accepted?: InputMaybe<Array<ExploitResultInput>>;
  /** Closed vulnerabilities detected in the execution of Forces */
  closed?: InputMaybe<Array<ExploitResultInput>>;
  /** Open vulnerabilities detected in the execution of Forces */
  open?: InputMaybe<Array<ExploitResultInput>>;
};

/** Executions Connection type definition */
export type ExecutionsConnection = {
  __typename?: 'ExecutionsConnection';
  /** A list of executions edges */
  edges: Array<ExecutionEdge>;
  /** Information to aid in pagination */
  pageInfo: PageInfo;
  /** Total executions found */
  total: Maybe<Scalars['Int']['output']>;
};

/** Exploit Result definition */
export type ExploitResult = {
  __typename?: 'ExploitResult';
  /** CVSS Exploitability */
  exploitability: Maybe<Scalars['String']['output']>;
  /** Applied kind of application security testing technique */
  kind: Maybe<Scalars['String']['output']>;
  /** Vulnerability state */
  state: Maybe<VulnerabilityExploitState>;
  /** Vulnerability where */
  where: Maybe<Scalars['String']['output']>;
  /** Execution specific */
  who: Maybe<Scalars['String']['output']>;
};

/** Exploit Result input definition */
export type ExploitResultInput = {
  /** Execution compliance */
  compliance?: InputMaybe<Scalars['Boolean']['input']>;
  /**
   * The CVSS score of the vulnerability returned by the
   * execution of Forces
   */
  exploitability?: InputMaybe<Scalars['Float']['input']>;
  /** Vulnerability kind which can be either DAST or SAST */
  kind?: InputMaybe<Scalars['String']['input']>;
  /**
   * Vulnerability status from the execution can be OPEN,
   * CLOSED,
   * and ACCEPTED
   */
  state?: InputMaybe<VulnerabilityExploitState>;
  /** The path where the vulnerability was found */
  where?: InputMaybe<Scalars['String']['input']>;
  /** Execution specific */
  who?: InputMaybe<Scalars['String']['input']>;
};

/** Files data input definition */
export type FilesDataInput = {
  /** Description of the file content */
  description?: InputMaybe<Scalars['String']['input']>;
  /** File name */
  fileName: Scalars['String']['input'];
  /** Date the file was uploaded */
  uploadDate?: InputMaybe<Scalars['String']['input']>;
};

/** Finding type definition */
export type Finding = {
  __typename?: 'Finding';
  /** Age of the Finding in days */
  age: Scalars['Int']['output'];
  /** List of assignees in the Finding */
  assignees: Array<Scalars['String']['output']>;
  /** Malicious actions that can be performed by exploiting the vulnerability */
  attackVectorDescription: Scalars['String']['output'];
  /** Information about the Finding shared by the stakeholders via comments */
  consulting: Maybe<Array<Maybe<Consult>>>;
  /** Finding transactional status which can be `CREATED`, `DELETED`, `MASKED` */
  currentState: Maybe<FindingStateStatus>;
  /** Brief explanation of the vulnerability and how it works */
  description: Scalars['String']['output'];
  /** List of unreleased vulnerabilities within the Finding */
  draftsConnection: VulnerabilitiesConnection;
  /** Evidences of the Finding where shows us how to exploit this vulnerability */
  evidence: FindingEvidence;
  /** Name of the Group where the Finding is in */
  groupName: Scalars['String']['output'];
  /** Hacker email that created the Finding */
  hacker: Scalars['String']['output'];
  /** Finding Identifier */
  id: Scalars['String']['output'];
  /** True if there is at least one vulnerability with the specified technique */
  includeTechnique: Scalars['Boolean']['output'];
  /** Is the Finding still exploitable? */
  isExploitable: Scalars['Boolean']['output'];
  /** Date from last state status */
  lastStateDate: Maybe<Scalars['String']['output']>;
  /** Time in days since the last closed vulnerability in this Finding */
  lastVulnerability: Maybe<Scalars['Int']['output']>;
  /** List of locations in the Finding */
  locations: Array<Scalars['String']['output']>;
  /**
   * Maximum EPSS score among all open vulnerabilities
   * @deprecated This value is deprecated and will be removed after 2025/03/23
   */
  maxOpenEPSS: Scalars['Int']['output'];
  /**
   * Maximum root criticality among all open vulnerabilities
   * @deprecated This value is deprecated and will be removed after 2025/03/23
   */
  maxOpenRootCriticality: Maybe<RootCriticality>;
  /**
   * Maximum CVSS v3.1 temporal score among all open vulnerabilities
   * @deprecated This value is deprecated and will be removed after 2025/10/04
   * Use maxOpenSeverityScoreV4 instead.
   */
  maxOpenSeverityScore: Scalars['Float']['output'];
  /** Maximum CVSS v4.0 threat score among all open vulnerabilities */
  maxOpenSeverityScoreV4: Scalars['Float']['output'];
  /**
   * Minimum time in minutes to remediate a typical vulnerability in this
   * Finding
   */
  minTimeToRemediate: Maybe<Scalars['Int']['output']>;
  /** Consults/Comments about the Finding. This query is exclusive to hackers */
  observations: Maybe<Array<Maybe<Consult>>>;
  /** Age of the oldest open vulnerability in the Finding in days */
  openAge: Maybe<Scalars['Int']['output']>;
  /** Name of the Organization where the Finding is in */
  organizationName: Scalars['String']['output'];
  /** Recommended course of action to close the vulnerabilities */
  recommendation: Scalars['String']['output'];
  /**
   * Information that was compromised or disclosed by exploiting the
   * vulnerability
   */
  records: Maybe<Scalars['JSONString']['output']>;
  /** Quantity of rejected vulnerabilities within the Finding */
  rejectedVulnerabilities: Scalars['Int']['output'];
  /** The finding release date or `None` if not released */
  releaseDate: Maybe<Scalars['String']['output']>;
  /** All open locations of a finding that have requested a re-attack */
  remediated: Maybe<Scalars['Boolean']['output']>;
  /** The finding release date or creation date if not released */
  reportDate: Maybe<Scalars['String']['output']>;
  /**
   * CVSS temporal score already calculated
   * @deprecated This value is deprecated and will be removed after 2025/10/04
   * Use severityScoreV4 instead.
   */
  severityScore: Scalars['Float']['output'];
  /** CVSS-BTE v4.0 threat score already calculated */
  severityScoreV4: Maybe<Scalars['Float']['output']>;
  /**
   * CVSS vector string
   * @deprecated This value is deprecated and will be removed after 2025/10/04
   * Use severityVectorV4 instead.
   */
  severityVector: Scalars['String']['output'];
  /** CVSS v4.0 vector string */
  severityVectorV4: Scalars['String']['output'];
  /** Was Sorts involved in finding the vulnerability? */
  sorts: Maybe<Sorts>;
  /** Current status of the finding which can be `SAFE`, `VULNERABLE` or `DRAFT` */
  status: Scalars['String']['output'];
  /** Quantity of submitted vulnerabilities within the Finding */
  submittedVulnerabilities: Scalars['Int']['output'];
  /** Actor and scenario where the vulnerability can be exploited */
  threat: Scalars['String']['output'];
  /** Finding number and name */
  title: Scalars['String']['output'];
  /**
   * Sum of all open vulnerabilities' CVSSF score.
   * Used in calculations of Risk Exposure
   * @deprecated This value is deprecated and will be removed after 2025/10/04
   * Use totalOpenCVSSFV4 instead.
   */
  totalOpenCVSSF: Scalars['Float']['output'];
  /** Sum of all open vulnerabilities' CVSSF (v4) score. */
  totalOpenCVSSFV4: Scalars['Float']['output'];
  /** Sum of all open vulnerabilities' priority score. */
  totalOpenPriority: Scalars['Float']['output'];
  /** Timeline of actions in this Finding */
  tracking: Maybe<Array<Tracking>>;
  /** Summary of the number of vulnerabilities in each treatment status */
  treatmentSummary: TreatmentSummary;
  /** Rules that are broken and lead to the existence of the vulnerability */
  unfulfilledRequirements: Array<Requirement>;
  /** Summary of the number of vulnerabilities in each verification status */
  verificationSummary: VerificationSummary;
  /** Is it verified i.e. with open and no remediated vulnerabilities? */
  verified: Scalars['Boolean']['output'];
  /** Vulnerabilities associated with the Finding */
  vulnerabilities: VulnerabilitiesConnection;
  /** Vulnerabilities associated with the Finding */
  vulnerabilitiesConnection: VulnerabilitiesConnection;
  /**
   * Number of vulnerabilities in the finding
   * @deprecated This value is deprecated and will be removed after 2025/10/04
   * Use vulnerabilitiesSummaryV4 instead.
   */
  vulnerabilitiesSummary: VulnerabilitiesSummary;
  /** Vulnerabilities summary in the finding for cvss v4 */
  vulnerabilitiesSummaryV4: VulnerabilitiesSummary;
  /** Vulnerabilities that have been requested for a reattack */
  vulnerabilitiesToReattackConnection: VulnerabilitiesConnection;
  /**
   * General locations of the Vulnerabilities, can be files, URLs or IPs.
   * It is limited to 20 locations.
   */
  where: Maybe<Scalars['String']['output']>;
  /** List of requested zero-risk vulnerabilities within the finding */
  zeroRiskConnection: VulnerabilitiesConnection;
  /** Summary of the number of zero risk vulnerabilities in each status */
  zeroRiskSummary: ZeroRiskSummary;
};


/** Finding type definition */
export type FindingDraftsConnectionArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  state?: InputMaybe<VulnerabilityState>;
  where?: InputMaybe<Scalars['String']['input']>;
};


/** Finding type definition */
export type FindingIncludeTechniqueArgs = {
  technique?: InputMaybe<Scalars['String']['input']>;
};


/** Finding type definition */
export type FindingVulnerabilitiesArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  filters?: InputMaybe<VulnerabilityFiltersInput>;
  first: Scalars['Int']['input'];
  sortBy?: InputMaybe<Array<InputMaybe<VulnerabilitySortInput>>>;
};


/** Finding type definition */
export type FindingVulnerabilitiesConnectionArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  state?: InputMaybe<VulnerabilityState>;
  treatment?: InputMaybe<VulnerabilityTreatment>;
  verification?: InputMaybe<VulnerabilityVerification>;
  where?: InputMaybe<Scalars['String']['input']>;
};


/** Finding type definition */
export type FindingVulnerabilitiesToReattackConnectionArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
};


/** Finding type definition */
export type FindingZeroRiskConnectionArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
};

/** Type of Finding consult enum definition */
export enum FindingConsultType {
  /** To share information about the finding */
  Consult = 'CONSULT',
  /** To review the finding and suggest adjustments */
  Observation = 'OBSERVATION'
}

/** Finding Edge type definition */
export type FindingEdge = Edge & {
  __typename?: 'FindingEdge';
  /** The cursor for the edge */
  cursor: Scalars['String']['output'];
  /** The node with the payload */
  node: Finding;
};

/** Finding evidence type definition */
export type FindingEvidence = {
  __typename?: 'FindingEvidence';
  /** Exploit Animation */
  animation: Maybe<FindingEvidenceItem>;
  /** Evidence image 1 */
  evidence1: Maybe<FindingEvidenceItem>;
  /** Evidence image 2 */
  evidence2: Maybe<FindingEvidenceItem>;
  /** Evidence image 3 */
  evidence3: Maybe<FindingEvidenceItem>;
  /** Evidence image 4 */
  evidence4: Maybe<FindingEvidenceItem>;
  /** Evidence image 5 */
  evidence5: Maybe<FindingEvidenceItem>;
  /** Exploitation evidence */
  exploitation: Maybe<FindingEvidenceItem>;
};

/** Finding evidence item type definition */
export type FindingEvidenceItem = {
  __typename?: 'FindingEvidenceItem';
  /** Evidence author */
  authorEmail: Maybe<Scalars['String']['output']>;
  /** Upload date in the format yyyy-MM-dd hh:mm:ss */
  date: Maybe<Scalars['String']['output']>;
  /** Evidence description */
  description: Maybe<Scalars['String']['output']>;
  /** Whether the evidence is a draft */
  isDraft: Maybe<Scalars['Boolean']['output']>;
  /** Evidence URL */
  url: Maybe<Scalars['String']['output']>;
};

/** Finding policy definition */
export type FindingPolicy = {
  __typename?: 'FindingPolicy';
  /** ID of the policy */
  id: Scalars['ID']['output'];
  /** Last time the finding policy status was modified */
  lastStatusUpdate: Maybe<Scalars['DateTime']['output']>;
  /** Name of the finding the policy is associated with */
  name: Scalars['String']['output'];
  /** Whether the finding policy is approved or not */
  status: OrganizationFindingPolicyStatus;
  /** Tags associated with the organization the finding policy */
  tags: Array<Scalars['String']['output']>;
  /** Vulnerability treatment covered by the policy */
  treatmentAcceptance: TreatmentAcceptancePolicy;
};

/** Finding transactional status */
export enum FindingStateStatus {
  /** Finding normal state */
  Created = 'CREATED',
  /** Finding was deleted */
  Deleted = 'DELETED',
  /** Finding's group was deleted */
  Masked = 'MASKED'
}

/** Findings Connection type definition */
export type FindingsConnection = {
  __typename?: 'FindingsConnection';
  /** A list of Findings edges */
  edges: Array<Maybe<FindingEdge>>;
  /** Information to aid in pagination */
  pageInfo: PageInfo;
  /** Total findings found */
  total: Maybe<Scalars['Int']['output']>;
};

/** Forces Execution type definition */
export type ForcesExecution = {
  __typename?: 'ForcesExecution';
  /** Date forces was executed in the format yyyy-MM-ddThh:mm:ssZ */
  date: Scalars['DateTime']['output'];
  /** Number of days until vulnerabilities are considered technical debt and do not break the build */
  daysUntilItBreaks: Maybe<Scalars['Int']['output']>;
  /** Identifier of the Forces execution */
  executionId: Scalars['String']['output'];
  /** Forces exit code, 0 for success, 1 for anything else */
  exitCode: Maybe<Scalars['String']['output']>;
  /** Tested repo branch */
  gitBranch: Maybe<Scalars['String']['output']>;
  /** Full SHA-1 hash of the tested commit */
  gitCommit: Maybe<Scalars['String']['output']>;
  /** Repository in which the agent was executed */
  gitOrigin: Maybe<Scalars['String']['output']>;
  /** Root nickname */
  gitRepo: Maybe<Scalars['String']['output']>;
  /** Grace period in days for new vulnerabilities at the time of execution */
  gracePeriod: Scalars['Int']['output'];
  /** Name of the Group where the Forces log will be in */
  groupName: Scalars['String']['output'];
  /** Forces execution log formatted as a JSON */
  jsonLog: Maybe<Scalars['JSONString']['output']>;
  /** Kind of application security testing technique applied */
  kind: Maybe<Scalars['String']['output']>;
  /**
   * Forces execution log. It is recommended to check this string with a
   * viewer capable of reading ANSI escape codes
   */
  log: Scalars['String']['output'];
  /**
   * Minimum CVSS score of an `open` vulnerability for DevSecOps to break the
   * build in strict mode
   */
  severityThreshold: Scalars['Float']['output'];
  /** The mode of execution of the DevSecOps agent, can be lax or strict */
  strictness: Maybe<Scalars['String']['output']>;
  /** Object containing arrays of accepted, open and closed vulnerabilities */
  vulnerabilities: Maybe<ExecutionVulnerabilities>;
};

/** Git Environment url types */
export enum GitEnvironmentCloud {
  /**
   * APK files (Android Package) can be added as
   * scan targets
   */
  Apk = 'APK',
  /**
   * Credentials are provided to access AWS, Azure,
   * or GCP platform
   */
  Cspm = 'CSPM',
  /**
   * URl where the application is deployed is provided
   * as the scan target
   */
  Url = 'URL'
}

/** Git Root environment url definition */
export type GitEnvironmentUrl = EnvironmentUrl & {
  __typename?: 'GitEnvironmentUrl';
  /** The cloud name provider if apply */
  cloudName: Maybe<Scalars['String']['output']>;
  /** Number of available secrets */
  countSecrets: Scalars['Int']['output'];
  /** The day when the URL was added */
  createdAt: Maybe<Scalars['DateTime']['output']>;
  /** User who added environment urll */
  createdBy: Maybe<Scalars['String']['output']>;
  /**
   * If environment has secrets or no
   * @deprecated This value is deprecated and will be removed after 2025/04/29.
   * Use countSecrets instead.
   */
  hasSecrets: Scalars['Boolean']['output'];
  /** GitRoot environment url */
  id: Scalars['String']['output'];
  /** If the url must be included in the scope */
  include: Scalars['Boolean']['output'];
  /** Indicates if the environment is a production environment */
  isProduction: Maybe<Scalars['Boolean']['output']>;
  /** Git Root */
  root: GitRoot;
  /** ID of the root */
  rootId: Scalars['ID']['output'];
  /** Environment secrets */
  secrets: Maybe<Array<Maybe<Secret>>>;
  /** The address of your URL where the environment is displayed */
  url: Scalars['String']['output'];
  /** Environment URL type which can be: CSPM, Mobile App or URL */
  urlType: Scalars['String']['output'];
  /** If the environment uses egress */
  useEgress: Scalars['Boolean']['output'];
  /** If the environment uses legacy */
  useVpn: Scalars['Boolean']['output'];
  /** If the environment uses connector */
  useZtna: Scalars['Boolean']['output'];
  /**
   * If urlType equal to CSPM, the role status will be verified
   * @deprecated This value is deprecated and will be removed after 2025/05/25.
   */
  verifyRoleStatus: Scalars['Boolean']['output'];
};

/** GitLab issues integration type definition */
export type GitLabIssuesIntegration = {
  __typename?: 'GitLabIssuesIntegration';
  /** The IDs of the users to assign the issue to */
  assigneeIds: Maybe<Array<Scalars['Int']['output']>>;
  /** Date when the integration was set up */
  connectionDate: Maybe<Scalars['DateTime']['output']>;
  /** Name of the configured project */
  gitlabProject: Maybe<Scalars['String']['output']>;
  /** Whether to create issues for new reports */
  issueAutomationEnabled: Maybe<Scalars['Boolean']['output']>;
  /** Label names to assign to new issues */
  labels: Maybe<Array<Scalars['String']['output']>>;
  /** List of GitLab project members */
  projectMembers: Array<GitLabProjectMember>;
  /** List of GitLab project names */
  projectNames: Array<Scalars['String']['output']>;
};


/** GitLab issues integration type definition */
export type GitLabIssuesIntegrationProjectMembersArgs = {
  gitlabProject: Scalars['String']['input'];
};

/** GitLab project member type definition */
export type GitLabProjectMember = {
  __typename?: 'GitLabProjectMember';
  /** ID of the project member */
  id: Scalars['Int']['output'];
  /** Name of the project member */
  name: Scalars['String']['output'];
  /** User name of the project member */
  username: Scalars['String']['output'];
};

/** Git repositories composed of code */
export type GitRoot = IRoot & {
  __typename?: 'GitRoot';
  /** Repository branch to be tested */
  branch: Scalars['String']['output'];
  /** Cloning status of Git Root */
  cloningStatus: GitRootCloningStatus;
  /** Date of creation */
  createdAt: Maybe<Scalars['DateTime']['output']>;
  /** User who added Git Root */
  createdBy: Maybe<Scalars['String']['output']>;
  /** Credentials used to clone the repository */
  credentials: Maybe<Credentials>;
  /**
   * Indicates how crucial this root is for your organization
   * and helps us calculate the impact of reported vulnerabilities.
   */
  criticality: Maybe<RootCriticality>;
  /** Docker image associated with the root. */
  dockerImage: Maybe<RootDockerImage>;
  /** Docker images associated with the root. */
  dockerImages: Maybe<Array<Maybe<RootDockerImage>>>;
  /** AWS s3 presigned url to download the git root */
  downloadUrl: Maybe<Scalars['String']['output']>;
  /**
   * Kind of environment to test. E.g. production, QA, etc.
   * @deprecated This value is deprecated and will be removed after 2025/05/17
   * The field is redundant with the branch
   */
  environment: Maybe<Scalars['String']['output']>;
  /** URLs to access the environment */
  gitEnvironmentUrls: Array<Maybe<GitEnvironmentUrl>>;
  /** Repository paths to be ignored */
  gitignore: Array<Scalars['String']['output']>;
  /** ID of the root */
  id: Scalars['ID']['output'];
  /** Analyze already existing code */
  includesHealthCheck: Scalars['Boolean']['output'];
  /** Last time the root was modified */
  lastEditedAt: Maybe<Scalars['DateTime']['output']>;
  /** Last user who modified the root */
  lastEditedBy: Maybe<Scalars['String']['output']>;
  /** Last time the root status was modified */
  lastStateStatusUpdate: Scalars['DateTime']['output'];
  /** Machine execution status of the Git Root */
  machineStatus: Maybe<GitRootMachineStatus>;
  /** Nickname for the repository */
  nickname: Scalars['String']['output'];
  /** Secrets for the environment */
  secrets: Array<Secret>;
  /** Whether the root is active or not */
  state: ResourceState;
  /** Dependencies present in the git root */
  toePackages: ToePackagesConnection;
  /** AWS s3 presigned url to upload the git root */
  uploadUrl: Maybe<Scalars['String']['output']>;
  /** Repository URL */
  url: Scalars['String']['output'];
  /** Environment requires Cloudflare Egress Ips to clone */
  useEgress: Scalars['Boolean']['output'];
  /** Repository requires Legacy VPN to clone */
  useVpn: Scalars['Boolean']['output'];
  /** Repository requires Cloudflare ZTNA to clone */
  useZtna: Scalars['Boolean']['output'];
  /** Vulnerabilities associated to the root */
  vulnerabilities: Maybe<Array<Vulnerability>>;
  /** Vulnerabilities associated to the root (paginated) */
  vulnerabilitiesConnection: VulnerabilitiesConnection;
};


/** Git repositories composed of code */
export type GitRootDockerImageArgs = {
  uri?: InputMaybe<Scalars['String']['input']>;
};


/** Git repositories composed of code */
export type GitRootToePackagesArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  outdated?: InputMaybe<Scalars['Boolean']['input']>;
  platform?: InputMaybe<Scalars['String']['input']>;
  platforms?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  search?: InputMaybe<Scalars['String']['input']>;
  vulnerable?: InputMaybe<Scalars['Boolean']['input']>;
};


/** Git repositories composed of code */
export type GitRootVulnerabilitiesConnectionArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
};

/** Git Root cloning status definition */
export type GitRootCloningStatus = {
  __typename?: 'GitRootCloningStatus';
  /** Full SHA-1 hash of the cloned repo */
  commit: Maybe<Scalars['String']['output']>;
  /** Whether the repository mirror is available in S3 */
  hasMirrorInS3: Scalars['Boolean']['output'];
  /** Message after repository cloning */
  message: Scalars['String']['output'];
  /** DateTime of the status update */
  modifiedDate: Scalars['DateTime']['output'];
  /** Cloning status of Git Root */
  status: CloningStatus;
};

/** GitRoot Edge type definition */
export type GitRootEdge = Edge & {
  __typename?: 'GitRootEdge';
  /** The cursor for the edge */
  cursor: Scalars['String']['output'];
  /** The node with the payload */
  node: GitRoot;
};

/** Definition for Machine execution status in a Git Root */
export type GitRootMachineStatus = {
  __typename?: 'GitRootMachineStatus';
  /** Full SHA-1 hash of the HEAD commit */
  commit: Maybe<Scalars['String']['output']>;
  /** Message related to the state */
  message: Scalars['String']['output'];
  /** DateTime of the status update */
  modifiedDate: Scalars['DateTime']['output'];
  /** Execution status of Root */
  status: MachineStatus;
};

/** GitRoots Connection type definition */
export type GitRootsConnection = {
  __typename?: 'GitRootsConnection';
  /** A list of GitRoots edges */
  edges: Array<GitRootEdge>;
  /** Information to aid in pagination */
  pageInfo: PageInfo;
  /** Total GitRoots found */
  total: Scalars['Int']['output'];
};

/** Grant stakeholder access Payload type definition */
export type GrantStakeholderAccessPayload = Payload & {
  __typename?: 'GrantStakeholderAccessPayload';
  /** New Organization/Group Stakeholder */
  grantedStakeholder: Maybe<Stakeholder>;
  /** Did the query succeed? */
  success: Scalars['Boolean']['output'];
};

/** Group type definition */
export type Group = {
  __typename?: 'Group';
  /** Analytics and graph data from the group */
  analytics: Maybe<Scalars['GenericScalar']['output']>;
  /** Azure DevOps integration details */
  azureIssuesIntegration: Maybe<AzureIssuesIntegration>;
  /** Billing information for the group */
  billing: GroupBilling;
  /** Official business registration number */
  businessId: Maybe<Scalars['String']['output']>;
  /** Name of the business related to the Group */
  businessName: Maybe<Scalars['String']['output']>;
  /** Number of currently closed Vulnerabilities in the group */
  closedVulnerabilities: Maybe<Scalars['Int']['output']>;
  /** Language distribution in a group */
  codeLanguages: Maybe<Array<CodeLanguages>>;
  /** Compliance with the standards in the Group */
  compliance: GroupCompliance;
  /** Credentials associated to the group */
  credentials: Maybe<Array<Credentials>>;
  /** Number of days until vulnerabilities are considered technical debt and do not break the build */
  daysUntilItBreaks: Maybe<Scalars['Int']['output']>;
  /** Brief description to identify the group */
  description: Maybe<Scalars['String']['output']>;
  /** Necessary clarifications on what should be tested */
  disambiguation: Maybe<Scalars['String']['output']>;
  /** Docker images associated with the group. */
  dockerImages: Array<RootDockerImage>;
  /** Situations affecting the group */
  events: Maybe<Array<Event>>;
  /** Approved Findings with discovered Vulnerabilities */
  findings: Maybe<Array<Maybe<Finding>>>;
  /** List of Forces executions paginated within the provided time period */
  forcesExecutionsConnection: ExecutionsConnection;
  /** Forces's token expiration date */
  forcesExpDate: Maybe<Scalars['String']['output']>;
  /** Current DevSecOps (Forces) token to allow access to your CI/CD pipelines */
  forcesToken: Maybe<Scalars['String']['output']>;
  /**
   * Vulnerabilities that have been reported to the group. Vulnerabilities
   * reported as Zero risks and Accepted are excluded
   */
  forcesVulnerabilities: VulnerabilitiesConnection;
  /** URLs to access the environment */
  gitEnvironmentUrls: Array<GitEnvironmentUrl>;
  /** Git Roots that have been added to the group */
  gitRoots: GitRootsConnection;
  /** GitLab integration details */
  gitlabIssuesIntegration: Maybe<GitLabIssuesIntegration>;
  /** How to access the group ToE */
  groupContext: Maybe<Scalars['String']['output']>;
  /** State of the Advanced plan within the Group */
  hasAdvanced: Scalars['Boolean']['output'];
  /** State of ARM services. Either true or the Group will be deleted soon */
  hasAsm: Maybe<Scalars['Boolean']['output']>;
  /** State of the Essential plan within the Group */
  hasEssential: Scalars['Boolean']['output'];
  /** State of the CI/CD agent Forces */
  hasForces: Maybe<Scalars['Boolean']['output']>;
  /** Hooks information */
  hook: Maybe<Array<Maybe<Hook>>>;
  /** IP Roots that have been added to the group */
  ipRoots: IpRootsConnection;
  /** Language in which Findings should be reported */
  language: Language;
  /** Time since last closed Vulnerability in days */
  lastClosedVulnerability: Maybe<Scalars['Int']['output']>;
  /** Identifier of the last closed Finding */
  lastClosedVulnerabilityFinding: Maybe<Finding>;
  /** Indicates if the group is managed manually */
  managed: ManagedType;
  /** Maximum number of calendar days a Finding can be temporarily accepted */
  maxAcceptanceDays: Maybe<Scalars['Int']['output']>;
  /** Maximum CVSS score in which a Finding can be temporarily accepted */
  maxAcceptanceSeverity: Scalars['Float']['output'];
  /** Maximum number of times a Finding can be temporarily accepted */
  maxNumberAcceptances: Maybe<Scalars['Int']['output']>;
  /** Current highest severity of an open Vulnerability in the group */
  maxOpenSeverity: Maybe<Scalars['Float']['output']>;
  /** Current open Vulnerability with the highest severity */
  maxOpenSeverityFinding: Maybe<Finding>;
  /** Mean time to remediate Vulnerabilities in days */
  meanRemediate: Maybe<Scalars['Int']['output']>;
  /** Mean time to remediate critical Vulnerabilities in days */
  meanRemediateCriticalSeverity: Maybe<Scalars['Int']['output']>;
  /** Mean time to remediate high-severity Vulnerabilities in days */
  meanRemediateHighSeverity: Maybe<Scalars['Int']['output']>;
  /** Mean time to remediate low-severity Vulnerabilities in days */
  meanRemediateLowSeverity: Maybe<Scalars['Int']['output']>;
  /** Mean time to remediate medium-severity Vulnerabilities in days */
  meanRemediateMediumSeverity: Maybe<Scalars['Int']['output']>;
  /** Minimum CVSS score in which a Finding can be temporarily accepted */
  minAcceptanceSeverity: Scalars['Float']['output'];
  /**
   * Minimum CVSS score of an `open` Vulnerability for DevSecOps to break
   * the build in strict mode
   */
  minBreakingSeverity: Maybe<Scalars['Float']['output']>;
  /** Name of the group */
  name: Scalars['String']['output'];
  /** Number of currently open Findings in the group */
  openFindings: Maybe<Scalars['Int']['output']>;
  /** Number of currently open Vulnerabilities in the group */
  openVulnerabilities: Maybe<Scalars['Int']['output']>;
  /** Organization name to which the group belongs */
  organization: Scalars['String']['output'];
  /** Dependency present in the given root */
  package: Maybe<ToePackage>;
  /**
   * ID of payment method selected for the group
   * @deprecated This field is deprecated and will be removed after 2025/08/14
   */
  paymentId: Maybe<Scalars['String']['output']>;
  /** Permissions of the current stakeholder within the group */
  permissions: Array<Scalars['String']['output']>;
  /** Group roots, can be IP, URL or Git Roots */
  roots: Maybe<Array<Root>>;
  /** Black or White box type of hacking service */
  service: ServiceType;
  /** Currently active service attributes */
  serviceAttributes: Array<Scalars['String']['output']>;
  /** Average Sprint length in weeks */
  sprintDuration: Scalars['Int']['output'];
  /** DateTime of the start date of sprint */
  sprintStartDate: Scalars['DateTime']['output'];
  /** Stakeholders of the group */
  stakeholders: Array<Stakeholder>;
  /** Continuous or one-shot hacking */
  subscription: Scalars['String']['output'];
  /** Group-wide tags */
  tags: Maybe<Array<Scalars['String']['output']>>;
  /** Group tier (Free, One-Shot, Other, Machine or Squad) */
  tier: Maybe<TierType>;
  /** Target of Evaluation inputs information */
  toeInputs: ToeInputsConnection;
  /** Target of Evaluation lines information which is paginated */
  toeLines: ToeLinesConnection;
  /** Target of Evaluation lines information which is paginated */
  toeLinesConnection: ToeLinesConnection;
  /** Dependencies present in the group's roots. */
  toePackages: ToePackagesConnection;
  /** Target of Evaluation ports information */
  toePorts: ToePortsConnection;
  /** Total priority score of vulnerable findings in the group */
  totalOpenPriority: Scalars['Float']['output'];
  /** URL Roots that have been added to the group */
  urlRoots: UrlRootsConnection;
  /** Last removed stakeholder from the group */
  userDeletion: Maybe<Scalars['String']['output']>;
  /** Role of the current stakeholder within the group */
  userRole: Maybe<Scalars['String']['output']>;
  /** Vulnerabilities that have been reported to the group */
  vulnerabilities: VulnerabilitiesConnection;
  /**
   * Vulnerability drafts that have been submitted to the group
   * @deprecated This field is deprecated and will be removed after 2025/07/21
   * Use me.vulnerabilityDrafts instead.
   */
  vulnerabilityDrafts: VulnerabilitiesConnection;
  /**
   * Grace period in days where newly reported Vulnerabilities won't break the
   * build (DevSecOps only)
   */
  vulnerabilityGracePeriod: Scalars['Int']['output'];
};


/** Group type definition */
export type GroupAnalyticsArgs = {
  documentName: Scalars['String']['input'];
  documentType: Scalars['String']['input'];
};


/** Group type definition */
export type GroupBillingArgs = {
  date?: InputMaybe<Scalars['DateTime']['input']>;
};


/** Group type definition */
export type GroupDockerImagesArgs = {
  include?: InputMaybe<Scalars['Boolean']['input']>;
};


/** Group type definition */
export type GroupFindingsArgs = {
  filters?: InputMaybe<Scalars['GenericScalar']['input']>;
  root?: InputMaybe<Scalars['String']['input']>;
  technique?: InputMaybe<Technique>;
  title?: InputMaybe<Scalars['String']['input']>;
};


/** Group type definition */
export type GroupForcesExecutionsConnectionArgs = {
  after?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  exitCode?: InputMaybe<Scalars['String']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  fromDate?: InputMaybe<Scalars['DateTime']['input']>;
  gitRepo?: InputMaybe<Scalars['String']['input']>;
  gitRepoExactFilter?: InputMaybe<Scalars['String']['input']>;
  search?: InputMaybe<Scalars['String']['input']>;
  status?: InputMaybe<Scalars['String']['input']>;
  strictness?: InputMaybe<Scalars['String']['input']>;
  toDate?: InputMaybe<Scalars['DateTime']['input']>;
  type?: InputMaybe<Scalars['String']['input']>;
};


/** Group type definition */
export type GroupForcesVulnerabilitiesArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  state?: InputMaybe<VulnerabilityState>;
};


/** Group type definition */
export type GroupGitRootsArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  branch?: InputMaybe<Scalars['String']['input']>;
  cloningStatus?: InputMaybe<CloningStatus>;
  first?: InputMaybe<Scalars['Int']['input']>;
  includesHealthCheck?: InputMaybe<Scalars['Boolean']['input']>;
  nickname?: InputMaybe<Scalars['String']['input']>;
  search?: InputMaybe<Scalars['String']['input']>;
  state?: InputMaybe<ResourceState>;
};


/** Group type definition */
export type GroupIpRootsArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  search?: InputMaybe<Scalars['String']['input']>;
};


/** Group type definition */
export type GroupPackageArgs = {
  name: Scalars['String']['input'];
  rootId: Scalars['ID']['input'];
  version: Scalars['String']['input'];
};


/** Group type definition */
export type GroupToeInputsArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  attackedBy?: InputMaybe<Scalars['String']['input']>;
  bePresent?: InputMaybe<Scalars['Boolean']['input']>;
  component?: InputMaybe<Scalars['String']['input']>;
  components?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  entryPoint?: InputMaybe<Scalars['String']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  fromAttackedAt?: InputMaybe<Scalars['DateTime']['input']>;
  fromBePresentUntil?: InputMaybe<Scalars['DateTime']['input']>;
  fromFirstAttackAt?: InputMaybe<Scalars['DateTime']['input']>;
  fromSeenAt?: InputMaybe<Scalars['DateTime']['input']>;
  hasVulnerabilities?: InputMaybe<Scalars['Boolean']['input']>;
  rootId?: InputMaybe<Scalars['ID']['input']>;
  rootNickname?: InputMaybe<Scalars['String']['input']>;
  search?: InputMaybe<Scalars['String']['input']>;
  seenFirstTimeBy?: InputMaybe<Scalars['String']['input']>;
  toAttackedAt?: InputMaybe<Scalars['DateTime']['input']>;
  toBePresentUntil?: InputMaybe<Scalars['DateTime']['input']>;
  toFirstAttackAt?: InputMaybe<Scalars['DateTime']['input']>;
  toSeenAt?: InputMaybe<Scalars['DateTime']['input']>;
};


/** Group type definition */
export type GroupToeLinesArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  attackedBy?: InputMaybe<Scalars['String']['input']>;
  bePresent?: InputMaybe<Scalars['Boolean']['input']>;
  comments?: InputMaybe<Scalars['String']['input']>;
  filename?: InputMaybe<Scalars['String']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  fromAttackedAt?: InputMaybe<Scalars['DateTime']['input']>;
  fromBePresentUntil?: InputMaybe<Scalars['DateTime']['input']>;
  fromFirstAttackAt?: InputMaybe<Scalars['DateTime']['input']>;
  fromModifiedDate?: InputMaybe<Scalars['DateTime']['input']>;
  fromSeenAt?: InputMaybe<Scalars['DateTime']['input']>;
  hasVulnerabilities?: InputMaybe<Scalars['Boolean']['input']>;
  lastAuthor?: InputMaybe<Scalars['String']['input']>;
  lastCommit?: InputMaybe<Scalars['String']['input']>;
  maxAttackedLines?: InputMaybe<Scalars['Int']['input']>;
  maxLoc?: InputMaybe<Scalars['Int']['input']>;
  maxSortsPriorityFactor?: InputMaybe<Scalars['Int']['input']>;
  minAttackedLines?: InputMaybe<Scalars['Int']['input']>;
  minLoc?: InputMaybe<Scalars['Int']['input']>;
  minSortsPriorityFactor?: InputMaybe<Scalars['Int']['input']>;
  rootId?: InputMaybe<Scalars['ID']['input']>;
  toAttackedAt?: InputMaybe<Scalars['DateTime']['input']>;
  toBePresentUntil?: InputMaybe<Scalars['DateTime']['input']>;
  toFirstAttackAt?: InputMaybe<Scalars['DateTime']['input']>;
  toModifiedDate?: InputMaybe<Scalars['DateTime']['input']>;
  toSeenAt?: InputMaybe<Scalars['DateTime']['input']>;
};


/** Group type definition */
export type GroupToeLinesConnectionArgs = {
  after?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  attackedBy?: InputMaybe<Scalars['String']['input']>;
  bePresent?: InputMaybe<Scalars['Boolean']['input']>;
  comments?: InputMaybe<Scalars['String']['input']>;
  filename?: InputMaybe<Scalars['String']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  fromAttackedAt?: InputMaybe<Scalars['DateTime']['input']>;
  fromBePresentUntil?: InputMaybe<Scalars['DateTime']['input']>;
  fromFirstAttackAt?: InputMaybe<Scalars['DateTime']['input']>;
  fromModifiedDate?: InputMaybe<Scalars['DateTime']['input']>;
  fromSeenAt?: InputMaybe<Scalars['DateTime']['input']>;
  hasVulnerabilities?: InputMaybe<Scalars['Boolean']['input']>;
  lastAuthor?: InputMaybe<Scalars['String']['input']>;
  lastCommit?: InputMaybe<Scalars['String']['input']>;
  maxAttackedLines?: InputMaybe<Scalars['Int']['input']>;
  maxCoverage?: InputMaybe<Scalars['Int']['input']>;
  maxLoc?: InputMaybe<Scalars['Int']['input']>;
  maxSortsPriorityFactor?: InputMaybe<Scalars['Int']['input']>;
  minAttackedLines?: InputMaybe<Scalars['Int']['input']>;
  minCoverage?: InputMaybe<Scalars['Int']['input']>;
  minLoc?: InputMaybe<Scalars['Int']['input']>;
  minSortsPriorityFactor?: InputMaybe<Scalars['Int']['input']>;
  rootId?: InputMaybe<Scalars['ID']['input']>;
  sort?: InputMaybe<LinesSortInput>;
  toAttackedAt?: InputMaybe<Scalars['DateTime']['input']>;
  toBePresentUntil?: InputMaybe<Scalars['DateTime']['input']>;
  toFirstAttackAt?: InputMaybe<Scalars['DateTime']['input']>;
  toModifiedDate?: InputMaybe<Scalars['DateTime']['input']>;
  toSeenAt?: InputMaybe<Scalars['DateTime']['input']>;
};


/** Group type definition */
export type GroupToePackagesArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  bePresent?: InputMaybe<Scalars['Boolean']['input']>;
  featureFlag?: InputMaybe<Scalars['Boolean']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  name?: InputMaybe<Scalars['String']['input']>;
  outdated?: InputMaybe<Scalars['Boolean']['input']>;
  path?: InputMaybe<Scalars['String']['input']>;
  platform?: InputMaybe<Scalars['String']['input']>;
  platforms?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  rootId?: InputMaybe<Scalars['ID']['input']>;
  search?: InputMaybe<Scalars['String']['input']>;
  vulnerable?: InputMaybe<Scalars['Boolean']['input']>;
};


/** Group type definition */
export type GroupToePortsArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  bePresent?: InputMaybe<Scalars['Boolean']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  rootId?: InputMaybe<Scalars['ID']['input']>;
};


/** Group type definition */
export type GroupUrlRootsArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  search?: InputMaybe<Scalars['String']['input']>;
};


/** Group type definition */
export type GroupVulnerabilitiesArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  closedAfter?: InputMaybe<Scalars['DateTime']['input']>;
  closedBefore?: InputMaybe<Scalars['DateTime']['input']>;
  externalBugTrackingSystem?: InputMaybe<Scalars['String']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  maxSeverity?: InputMaybe<Scalars['String']['input']>;
  minSeverity?: InputMaybe<Scalars['String']['input']>;
  reportedAfter?: InputMaybe<Scalars['DateTime']['input']>;
  reportedBefore?: InputMaybe<Scalars['DateTime']['input']>;
  root?: InputMaybe<Scalars['String']['input']>;
  search?: InputMaybe<Scalars['String']['input']>;
  severityRating?: InputMaybe<SeverityRating>;
  state?: InputMaybe<VulnerabilityState>;
  technique?: InputMaybe<Technique>;
  treatment?: InputMaybe<Scalars['String']['input']>;
  type?: InputMaybe<Scalars['String']['input']>;
  verificationStatus?: InputMaybe<Scalars['String']['input']>;
  zeroRisk?: InputMaybe<VulnerabilityZeroRiskStatus>;
};


/** Group type definition */
export type GroupVulnerabilityDraftsArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  maxSeverity?: InputMaybe<Scalars['String']['input']>;
  minSeverity?: InputMaybe<Scalars['String']['input']>;
  root?: InputMaybe<Scalars['String']['input']>;
  search?: InputMaybe<Scalars['String']['input']>;
  state?: InputMaybe<VulnerabilityState>;
  type?: InputMaybe<Scalars['String']['input']>;
};

/** GroupBilling type definition */
export type GroupBilling = {
  __typename?: 'GroupBilling';
  /** Authors data */
  authors: Array<GroupBillingAuthor>;
  /** Authors costs for group */
  costsAuthors: Scalars['Int']['output'];
  /** Base costs for group */
  costsBase: Scalars['Int']['output'];
  /** Total costs for group */
  costsTotal: Scalars['Int']['output'];
  /** Number of authors for group */
  numberAuthors: Scalars['Int']['output'];
};

/** GroupAuthor type definition */
export type GroupBillingAuthor = {
  __typename?: 'GroupBillingAuthor';
  /** Author who has contributed */
  actor: Scalars['String']['output'];
  /** Example contribution commit */
  commit: Maybe<Scalars['String']['output']>;
  /** Groups the author has contributed to */
  groups: Array<Scalars['String']['output']>;
  /** Organization the author has contributed to */
  organization: Maybe<Scalars['String']['output']>;
  /** Root repository of the contributions */
  repository: Maybe<Scalars['String']['output']>;
};

/** GroupCompliance type definition */
export type GroupCompliance = {
  __typename?: 'GroupCompliance';
  /** Compliance information by standard */
  unfulfilledStandards: Array<Maybe<UnfulfilledStandard>>;
};

/** Group file type definition */
export type GroupFile = {
  __typename?: 'GroupFile';
  /** Meaningful description for the usage or purpose of the added file */
  description: Scalars['String']['output'];
  /** Name of the uploaded file and with which it will be available in Scope */
  fileName: Scalars['String']['output'];
  /** Date when the file was uploaded */
  uploadDate: Maybe<Scalars['String']['output']>;
  /** Stakeholder email who uploaded the file */
  uploader: Scalars['String']['output'];
};

/** Available integrations */
export enum GroupIntegration {
  /** Azure DevOps */
  AzureDevops = 'AZURE_DEVOPS',
  /** GitLab */
  Gitlab = 'GITLAB'
}

/** Hook type definition */
export type Hook = {
  __typename?: 'Hook';
  /** URL to send the event */
  entryPoint: Scalars['String']['output'];
  /** GroupName of a hook */
  groupName: Scalars['String']['output'];
  /** List of events to send requests */
  hookEvents: Maybe<Array<HookEventType>>;
  /** Hook Id */
  id: Scalars['String']['output'];
  /** Hook Name */
  name: Scalars['String']['output'];
  /** Hook state */
  state: HookState;
  /** Token to send in the event request */
  token: Scalars['String']['output'];
  /** Header to send in the event request */
  tokenHeader: Scalars['String']['output'];
};

/** Type of hook event enum definition */
export enum HookEventType {
  /** When the agent token is about to expire */
  AgentTokenExpiration = 'AGENT_TOKEN_EXPIRATION',
  /** When a environment is removed as a resource */
  EnvironmentRemoved = 'ENVIRONMENT_REMOVED',
  /** When an Event is solved in the group */
  EventClosed = 'EVENT_CLOSED',
  /** When an Event is created in the group */
  EventCreated = 'EVENT_CREATED',
  /** When a root is created */
  RootCreated = 'ROOT_CREATED',
  /** When a root is disabled */
  RootDisabled = 'ROOT_DISABLED',
  /**
   * When a Vulnerability is assigned to a specific
   * member of the group
   */
  VulnerabilityAssigned = 'VULNERABILITY_ASSIGNED',
  /** When a Vulnerability changes to safe in the group */
  VulnerabilityClosed = 'VULNERABILITY_CLOSED',
  /** When a Vulnerability is created in the group */
  VulnerabilityCreated = 'VULNERABILITY_CREATED',
  /** When a Vulnerability is deleted */
  VulnerabilityDeleted = 'VULNERABILITY_DELETED',
  /** When the severity score of a vulnerability changes */
  VulnerabilitySeverityChanged = 'VULNERABILITY_SEVERITY_CHANGED',
  /** When the existence of a vulnerability is confirmed */
  VulnerabilityVerified = 'VULNERABILITY_VERIFIED'
}

/** Hook update definition */
export type HookInput = {
  /** Url to send events */
  entryPoint: Scalars['String']['input'];
  /** List of events to send requests */
  hookEvents?: InputMaybe<Array<HookEventType>>;
  /** Name of the hook */
  name: Scalars['String']['input'];
  /** Token to send in the request headers */
  token: Scalars['String']['input'];
  /** Token header to send un the request headers */
  tokenHeader: Scalars['String']['input'];
};

/** Hook state type definition */
export type HookState = {
  __typename?: 'HookState';
  /** Last stakeholder updated */
  modifiedBy: Scalars['String']['output'];
  /** Last date updated */
  modifiedDate: Scalars['String']['output'];
  /** Hook status */
  status: HookStatus;
};

/** Type of hook status enum definition */
export enum HookStatus {
  /** When hook is ok */
  Active = 'ACTIVE',
  /** When hook is inactive */
  Inactive = 'INACTIVE'
}

/** Notification interface definition */
export type INotification = {
  /** ID of the notification */
  id: Scalars['ID']['output'];
  /** Time at which this notification was issued */
  notifiedAt: Scalars['DateTime']['output'];
};

/**
 * The IP address who is the unique identifier
 * on the Internet or a local network
 */
export type IpRoot = IRoot & {
  __typename?: 'IPRoot';
  /** IP address of the environment to test */
  address: Scalars['String']['output'];
  /** ID of the root */
  id: Scalars['ID']['output'];
  /** Nickname for the root */
  nickname: Scalars['String']['output'];
  /** Whether the root is active or not */
  state: ResourceState;
  /** Vulnerabilities associated to the root */
  vulnerabilities: Maybe<Array<Vulnerability>>;
};

/** IPRoot Edge type definition */
export type IpRootEdge = Edge & {
  __typename?: 'IPRootEdge';
  /** The cursor for the edge */
  cursor: Scalars['String']['output'];
  /** The node with the payload */
  node: IpRoot;
};

/** IPRoots Connection type definition */
export type IpRootsConnection = {
  __typename?: 'IPRootsConnection';
  /** A list of IPRoots edges */
  edges: Array<IpRootEdge>;
  /** Information to aid in pagination */
  pageInfo: PageInfo;
  /** Total IPRoots found */
  total: Scalars['Int']['output'];
};

/** Root interface definition */
export type IRoot = {
  /** ID of the root */
  id: Scalars['ID']['output'];
  /** Nickname for the root */
  nickname: Scalars['String']['output'];
  /** Whether the root is active or not */
  state: ResourceState;
};

/** Import mailmap response object */
export type ImportMailmapPayload = {
  __typename?: 'ImportMailmapPayload';
  /** List of exceptions messages obtained after this operation */
  exceptionsMessages: Array<Maybe<Scalars['String']['output']>>;
  /** Request result */
  success: Scalars['Boolean']['output'];
  /** Number of mailmap entries that were successfully processed */
  successCount: Scalars['Int']['output'];
};

/** IntegrationRepositories Connection type definition */
export type IntegrationRepositoriesConnection = {
  __typename?: 'IntegrationRepositoriesConnection';
  /** A list of IntegrationRepositories edges */
  edges: Maybe<Array<Maybe<IntegrationRepositoriesEdge>>>;
  /** Information to aid in pagination */
  pageInfo: PageInfo;
  /** Total IntegrationRepositories found */
  total: Maybe<Scalars['Int']['output']>;
};

/** IntegrationRepositories Edge type definition */
export type IntegrationRepositoriesEdge = Edge & {
  __typename?: 'IntegrationRepositoriesEdge';
  /** The cursor for the edge */
  cursor: Scalars['String']['output'];
  /** The node with the payload */
  node: Maybe<OrganizationIntegrationRepositories>;
};

/** Stakeholder invitation state enum definition */
export enum InvitationState {
  /** Acceptance pending */
  Pending = 'PENDING',
  /** Accepted */
  Registered = 'REGISTERED',
  /** Unregistered invitation */
  Unregistered = 'UNREGISTERED'
}

/** Language for Findings */
export enum Language {
  /** English */
  En = 'EN',
  /** Spanish */
  Es = 'ES'
}

/** Values for sorting drafts */
export enum LinesSort {
  /** Organizes items based on their lines */
  Loc = 'LOC',
  /**
   * Factor assigned by sorts to determine the priority
   * of the items
   */
  SortsPriorityFactor = 'SORTS_PRIORITY_FACTOR',
  /**
   * Assigned by sorts to determine the probability that
   * the item has vulnerabilities
   */
  SortsRiskLevel = 'SORTS_RISK_LEVEL'
}

/** Lines Sort input definition */
export type LinesSortInput = {
  /** Field to sort by */
  field: LinesSort;
  /** Sorting order */
  order: OrderSort;
};

/** Git root Machine Execution status */
export enum MachineStatus {
  /** Something went wrong with the execution */
  Failed = 'FAILED',
  /** The execution is ongoing */
  InProgress = 'IN_PROGRESS',
  /** The execution was successful */
  Success = 'SUCCESS'
}

/** Mailmap entries connection type object */
export type MailmapEntriesConnection = {
  __typename?: 'MailmapEntriesConnection';
  /** List of mailmap entries */
  edges: Array<MailmapEntryEdge>;
  /** Page information */
  pageInfo: PageInfo;
};

/** Mailmap entry type object */
export type MailmapEntry = {
  __typename?: 'MailmapEntry';
  /** Timestamp indicating when the mailmap entry was created */
  mailmapEntryCreatedAt: Maybe<Scalars['String']['output']>;
  /** Mailmap entry email: User primary email */
  mailmapEntryEmail: Scalars['String']['output'];
  /** Mailmap entry name: User primary name or username */
  mailmapEntryName: Scalars['String']['output'];
  /** Timestamp indicating when the mailmap entry was last updated */
  mailmapEntryUpdatedAt: Maybe<Scalars['String']['output']>;
};

/** Mailmap Entry Edge type definition */
export type MailmapEntryEdge = Edge & {
  __typename?: 'MailmapEntryEdge';
  /** The cursor for the edge */
  cursor: Scalars['String']['output'];
  /** The node with the payload */
  node: MailmapEntry;
};

/** Mailmap entry input object */
export type MailmapEntryInput = {
  /** Mailmap entry email: User primary email */
  mailmapEntryEmail: Scalars['String']['input'];
  /** Mailmap entry name: User primary name or username */
  mailmapEntryName: Scalars['String']['input'];
};

/** Mailmap entry response object */
export type MailmapEntryPayload = {
  __typename?: 'MailmapEntryPayload';
  /** Mailmap entry object: User primary account */
  entry: MailmapEntry;
  /** Request result */
  success: Scalars['Boolean']['output'];
};

/** Mailmap entry with subentries type object */
export type MailmapEntryWithSubentries = {
  __typename?: 'MailmapEntryWithSubentries';
  /** Mailmap entry object: User primary account */
  entry: MailmapEntry;
  /** List of mailmap subentries objects: User secondary accounts */
  subentries: Array<Maybe<MailmapSubentry>>;
};

/** Mailmap entry with subentries response object */
export type MailmapEntryWithSubentriesPayload = {
  __typename?: 'MailmapEntryWithSubentriesPayload';
  /** Mailmap entry with subentries object: User accounts */
  entryWithSubentries: MailmapEntryWithSubentries;
  /** Request result */
  success: Scalars['Boolean']['output'];
};

/** Mailmap record: one line of a mailmap file */
export type MailmapRecord = {
  __typename?: 'MailmapRecord';
  /** Timestamp indicating when the mailmap entry was created */
  mailmapEntryCreatedAt: Maybe<Scalars['String']['output']>;
  /** Mailmap entry email: User primary email */
  mailmapEntryEmail: Scalars['String']['output'];
  /** Mailmap entry name: User primary name or username */
  mailmapEntryName: Scalars['String']['output'];
  /** Timestamp indicating when the mailmap entry was last updated */
  mailmapEntryUpdatedAt: Maybe<Scalars['String']['output']>;
  /** Timestamp indicating when the mailmap subentry was created */
  mailmapSubentryCreatedAt: Maybe<Scalars['String']['output']>;
  /** Mailmap subentry email: User secondary email */
  mailmapSubentryEmail: Scalars['String']['output'];
  /** Mailmap subentry name: User secondary name or username */
  mailmapSubentryName: Scalars['String']['output'];
  /** Timestamp indicating when the mailmap subentry was last updated */
  mailmapSubentryUpdatedAt: Maybe<Scalars['String']['output']>;
};

/** Mailmap subentry type object */
export type MailmapSubentry = {
  __typename?: 'MailmapSubentry';
  /** Timestamp indicating when the mailmap subentry was created */
  mailmapSubentryCreatedAt: Maybe<Scalars['String']['output']>;
  /** Mailmap subentry email: User secondary email */
  mailmapSubentryEmail: Scalars['String']['output'];
  /** Mailmap subentry name: User secondary name or username */
  mailmapSubentryName: Scalars['String']['output'];
  /** Timestamp indicating when the mailmap subentry was last updated */
  mailmapSubentryUpdatedAt: Maybe<Scalars['String']['output']>;
};

/** Mailmap subentry input object */
export type MailmapSubentryInput = {
  /** Mailmap subentry email: User secondary email */
  mailmapSubentryEmail: Scalars['String']['input'];
  /** Mailmap subentry name: User secondary name or username */
  mailmapSubentryName: Scalars['String']['input'];
};

/** Group managed state */
export enum ManagedType {
  /**
   * The group uses a validated payment method other
   * than a credit card
   */
  Managed = 'MANAGED',
  /** The group's payment method is a credit card */
  NotManaged = 'NOT_MANAGED',
  /** Groups currently in a trial period on the Platform */
  Trial = 'TRIAL',
  /**
   * No payments made, validation of alternative payment
   * methods, or expired free trials, resulting in blocked
   * group access
   */
  UnderReview = 'UNDER_REVIEW'
}

/** Me type definition */
export type Me = {
  __typename?: 'Me';
  /** Access tokens metadata belonging to the stakeholder */
  accessTokens: Array<AccessToken>;
  /** Information about AWS Marketplace subscription status */
  awsSubscription: Maybe<AwsSubscription>;
  /** Credentials belonging to the stakeholder */
  credentials: Maybe<Array<Credentials>>;
  /**
   * Indicates if the stakeholder has completed the initial enrollment
   * process successfully
   */
  enrolled: Scalars['Boolean']['output'];
  /** Findings with empty evidence in all groups where the user has access */
  findingEmptyEvidence: FindingsConnection;
  /** Evidence drafts in all groups where the stakeholder has access */
  findingEvidenceDrafts: FindingsConnection;
  /** Open Vulnerabilities with a re-attack request status */
  findingReattacksConnection: FindingsConnection;
  /** Indicates if the user email has all group/organization invitations pending */
  hasAllInvitationsPending: Scalars['Boolean']['output'];
  /** Indicates if there is more than one open session currently */
  isConcurrentSession: Scalars['Boolean']['output'];
  /** Indicates if the user email is a personal account */
  isPersonalEmail: Scalars['Boolean']['output'];
  /** Platform notification */
  notification: Maybe<Notification>;
  /** Platform notifications */
  notifications: Array<Notification>;
  /**
   * Query to retrieve the configuration of notification preferences for
   * the stakeholder
   */
  notificationsPreferences: NotificationsPreferences;
  /** List of Organizations the stakeholder is a member of */
  organizations: Array<Organization>;
  /** List of pending events from the groups the stakeholders belongs to */
  pendingEvents: EventsConnection;
  /** Gets the user-level permissions of the current stakeholder */
  permissions: Array<Scalars['String']['output']>;
  /** Stakeholder's phone information */
  phone: Maybe<Phone>;
  /** Indicates if the legal notice modal won't appear after every ARM login */
  remember: Scalars['Boolean']['output'];
  /** Gets the user-level role of the current stakeholder */
  role: Scalars['String']['output'];
  /**
   * Unix timestamp representing the moment where the stakeholders's
   * API access token will expire
   */
  sessionExpiration: Scalars['String']['output'];
  /** Tags associated with the organization/groups */
  tags: Maybe<Array<Tag>>;
  /** New stakeholder workflow skipping information */
  tours: Tours;
  /** Free trial information */
  trial: Maybe<Trial>;
  /** Stakeholder's trusted devices */
  trustedDevices: Maybe<Array<Maybe<TrustedDevice>>>;
  /** Stakeholder's Email address */
  userEmail: Scalars['String']['output'];
  /** Current user name */
  userName: Scalars['String']['output'];
  /** Assigned Vulnerabilities of the current stakeholder */
  vulnerabilitiesAssigned: Array<Vulnerability>;
  /** Priority vulnerabilities to remediate in group or organization */
  vulnerabilitiesPriorityRanking: VulnerabilitiesConnection;
  /** Vulnerabilities with a pending severity update request request */
  vulnerabilitiesSeverityRequests: VulnerabilitiesConnection;
  /** Vulnerability drafts in all groups where the user has access */
  vulnerabilityDrafts: VulnerabilitiesConnection;
};


/** Me type definition */
export type MeFindingEmptyEvidenceArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
};


/** Me type definition */
export type MeFindingEvidenceDraftsArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
};


/** Me type definition */
export type MeFindingReattacksConnectionArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
};


/** Me type definition */
export type MeNotificationArgs = {
  notificationId: Scalars['ID']['input'];
};


/** Me type definition */
export type MePendingEventsArgs = {
  after: Scalars['String']['input'];
  first: Scalars['Int']['input'];
};


/** Me type definition */
export type MeTagsArgs = {
  organizationId?: InputMaybe<Scalars['String']['input']>;
  organizationName?: InputMaybe<Scalars['String']['input']>;
};


/** Me type definition */
export type MeVulnerabilitiesPriorityRankingArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  groupName?: InputMaybe<Scalars['String']['input']>;
  treatment?: InputMaybe<VulnerabilityTreatment>;
};


/** Me type definition */
export type MeVulnerabilitiesSeverityRequestsArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
};


/** Me type definition */
export type MeVulnerabilityDraftsArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  findingTitle?: InputMaybe<Scalars['String']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  fromReportDate?: InputMaybe<Scalars['DateTime']['input']>;
  groupName?: InputMaybe<Scalars['String']['input']>;
  hacker?: InputMaybe<Scalars['String']['input']>;
  maxSeverityTemporalScore?: InputMaybe<Scalars['Float']['input']>;
  minSeverityTemporalScore?: InputMaybe<Scalars['Float']['input']>;
  organizationName?: InputMaybe<Scalars['String']['input']>;
  sourceType?: InputMaybe<VulnerabilitySourceType>;
  state?: InputMaybe<VulnerabilityState>;
  toReportDate?: InputMaybe<Scalars['DateTime']['input']>;
};

/** Main mutation object */
export type Mutation = {
  __typename?: 'Mutation';
  /** Whether to keep showing the legal notice modal or not */
  acceptLegal: SimplePayload;
  /**
   * Acknowledges termination of concurrent session
   * @deprecated This mutation is deprecated and will be removed after 2025/04/30
   */
  acknowledgeConcurrentSession: SimplePayload;
  /** Activates a root and make its content available for security analyses */
  activateRoot: SimplePayload;
  /** Adds an API token */
  addAccessToken: UpdateAccessTokenPayload;
  /** Adds an audit event */
  addAuditEvent: SimplePayload;
  /** Adds new credentials to the organization */
  addCredentials: SimplePayload;
  /** Adds credit card payment method to an organization */
  addCreditCardPaymentMethod: SimplePayload;
  /**
   * Adds information about a user's auto-enrollment to the ARM, allowing
   * to know the status of the free trial, if they are using it, how many
   * days he has been using it, or if has completed it
   */
  addEnrollment: SimplePayload;
  /** Reports an unexpected Event when CSPM environments are broken */
  addEnvironmentIntegrityEvent: SimplePayload;
  /** Reports an unexpected Event when dealing with the ToE */
  addEvent: AddEventPayload;
  /** Adds a consult/comment to an Event */
  addEventConsult: AddConsultPayload;
  /** Adds files to a Group's scope view */
  addFilesToDb: SimplePayload;
  /** Adds a new Finding to the specified Group */
  addFinding: SimplePayload;
  /** Adds a consult/comment in a Finding */
  addFindingConsult: AddConsultPayload;
  /** Adds a new Forces execution results and log to a Group */
  addForcesExecution: SimplePayload;
  /** Adds an environment CSPM to a Git Root, including its secrets */
  addGitEnvironmentCspm: AddEnvironmentUrlPayload;
  /** Adds an environment URL to a Git Root */
  addGitEnvironmentUrl: AddEnvironmentUrlPayload;
  /** Adds a new root to be analyzed and tested */
  addGitRoot: AddRootPayload;
  /** Adds a new group to the specified organization */
  addGroup: SimplePayload;
  /** Adds a consult/comment to the Group's Consulting tab */
  addGroupConsult: AddConsultPayload;
  /** Adds specified tags to the group */
  addGroupTags: SimpleGroupPayload;
  /** Creates a hook */
  addHook: SimplePayload;
  /** Adds an IP to the Group's roots */
  addIpRoot: AddRootPayload;
  /** Adds a new Organization */
  addOrganization: AddOrganizationPayload;
  /**
   * Adds a Finding policy to the Organization. Approval is
   * still required for enactment
   */
  addOrganizationFindingPolicy: SimplePayload;
  /** Adds other payment method to an organization */
  addOtherPaymentMethod: SimplePayload;
  /** Adds an environment Docker Image to a Root */
  addRootDockerImage: AddRootDockerImagePayload;
  /** Adds a machine execution to git root */
  addSecret: SimplePayload;
  /** Adds a new Stakeholder to the default ARM organization */
  addStakeholder: AddStakeholderPayload;
  /** Adds a new Toe input to the ARM group */
  addToeInput: SimplePayload;
  /** Adds a new Toe lines to the ARM group */
  addToeLines: SimplePayload;
  /** Adds a new Toe port to the ARM group */
  addToePort: SimplePayload;
  /** Adds the URL of the environment to be tested */
  addUrlRoot: AddRootPayload;
  /** Approves an evidence draft */
  approveEvidence: SimplePayload;
  /** Approves a proposed change in the severity score for one or more vulnerabilities */
  approveVulnerabilitiesSeverityUpdate: SimpleFindingPayload;
  /** Closes the provided vulnerabilities */
  closeVulnerabilities: SimplePayload;
  /** Confirms the provided vulnerabilities */
  confirmVulnerabilities: SimplePayload;
  /** Confirms the provided vulnerabilities as zero risk */
  confirmVulnerabilitiesZeroRisk: SimplePayload;
  /** Convert mailmap subentries into a new entry with subentries */
  convertMailmapSubentriesToNewEntry: SimplePayload;
  /** Create a new mailmap entry */
  createMailmapEntry: MailmapEntryWithSubentriesPayload;
  /** Create a new mailmap subentry */
  createMailmapSubentry: SimplePayload;
  /** Disables the specified Finding policy */
  deactivateOrganizationFindingPolicy: SimplePayload;
  /** Deactivates a Group root, which stops further testing of it */
  deactivateRoot: SimplePayload;
  /** Delete mailmap entry */
  deleteMailmapEntry: SimplePayload;
  /** Delete subentries of mailmap entry */
  deleteMailmapEntrySubentries: SimplePayload;
  /** Delete mailmap entry and its subentries */
  deleteMailmapEntryWithSubentries: SimplePayload;
  /** Delete mailmap subentry */
  deleteMailmapSubentry: SimplePayload;
  /** Downloads an evidence file associated with an Event */
  downloadBillingFile: DownloadFilePayload;
  /** Downloads an evidence file associated with an Event */
  downloadEventFile: DownloadFilePayload;
  /**
   * Downloads the file that has the group, which was uploaded
   * in the Scope section
   */
  downloadFile: DownloadFilePayload;
  /** Downloads a csv file with the Git roots of this group */
  downloadGitRootsFile: DownloadFilePayload;
  /** Downloads a yaml file with all the vulnerabilities of this finding */
  downloadVulnerabilityFile: DownloadFilePayload;
  /** Grants a stakeholder access to a Group */
  grantStakeholderAccess: GrantStakeholderAccessPayload;
  /** Grants a stakeholder access to an Organization */
  grantStakeholderOrganizationAccess: GrantStakeholderAccessPayload;
  /** Handles submission, acceptance or rejection of a Finding policy */
  handleOrganizationFindingPolicyAcceptance: SimplePayload;
  /** Handles acceptance and rejection of Vulnerabilities */
  handleVulnerabilitiesAcceptance: SimplePayload;
  /** Import mailmap by uploading a `.mailmap` file */
  importMailmap: ImportMailmapPayload;
  /** Revokes a user API access token */
  invalidateAccessToken: SimplePayload;
  /** Move an environment URL to a new root */
  moveEnvironment: SimplePayload;
  /** Move mailmap entry with subentries to another organization */
  moveMailmapEntryWithSubentries: SimplePayload;
  /** Moves a root and its associations to another group */
  moveRoot: SimplePayload;
  /** Refresh the ToE Lines of a concrete group */
  refreshToeLines: SimplePayload;
  /** Reject the event solution */
  rejectEventSolution: SimplePayload;
  /** Rejects an evidence draft */
  rejectEvidence: SimplePayload;
  /** Rejects the provided vulnerabilities from being considered as opened */
  rejectVulnerabilities: SimplePayload;
  /** Rejects the latest severity update request for the provided vulnerabilities */
  rejectVulnerabilitiesSeverityUpdate: SimplePayload;
  /** Rejects the provided vulnerabilities from being considered as zero risk */
  rejectVulnerabilitiesZeroRisk: SimplePayload;
  /** Removes credentials from an Organization */
  removeCredentials: SimplePayload;
  /** Removes root environment url */
  removeEnvironmentUrl: SimplePayload;
  /** Removes evidence from an Event */
  removeEventEvidence: SimplePayload;
  /** Removes evidence from a Finding */
  removeEvidence: SimpleFindingPayload;
  /** Removes Files from a Group */
  removeFiles: SimplePayload;
  /** Removes/deletes a Finding from a Group */
  removeFinding: SimplePayload;
  /**
   * Removes/deletes a group from ARM. Once successful
   * this action cannot be undone
   */
  removeGroup: SimplePayload;
  /** Removes/deletes the specified hook */
  removeGroupHook: SimpleGroupPayload;
  /** Removes a previously set up integration from a group */
  removeGroupIntegration: SimplePayload;
  /** Removes/deletes the specified tag from the group */
  removeGroupTag: SimpleGroupPayload;
  /** Removes an Organization's priority policy */
  removeOrganizationPriorityPolicy: SimplePayload;
  /** Removes a payment method for an organization */
  removePaymentMethod: SimplePayload;
  /** Removes an environment Docker Image to a Root */
  removeRootDockerImage: SimplePayload;
  /** Deletes a group secret */
  removeSecret: SimplePayload;
  /** The user deletes their ARM account */
  removeStakeholder: SimplePayload;
  /** Removes a stakeholder's access from a Group */
  removeStakeholderAccess: RemoveStakeholderAccessPayload;
  /** Removes a stakeholder's access from an Organization */
  removeStakeholderOrganizationAccess: SimplePayload;
  /** Removes tags from vulnerabilities */
  removeTags: SimplePayload;
  /** Removes users trusted devices */
  removeTrustedDevice: SimplePayload;
  /** Removes/deletes a vulnerability within a finding */
  removeVulnerability: SimplePayload;
  /** Requests the verification of an event when it has been solved */
  requestEventVerification: SimplePayload;
  /** Requests an upgrade of your subscriptions */
  requestGroupsUpgrade: SimplePayload;
  /** Requests a sbom for a rot in a specific format */
  requestSbomFile: SimplePayload;
  /**
   * Requests a hold on ongoing vulnerability reattacks.
   * Usually because of an Event blocking access to them
   */
  requestVulnerabilitiesHold: SimplePayload;
  /**
   * Requests a change in the severity score for one or more vulnerabilities.
   * Requires later approval for applying the changes.
   */
  requestVulnerabilitiesSeverityUpdate: SimpleFindingPayload;
  /** Requests a reattack on the provided Vulnerabilities */
  requestVulnerabilitiesVerification: SimplePayload;
  /** Requests a zero risk on the provided Vulnerabilities */
  requestVulnerabilitiesZeroRisk: SimplePayload;
  /** Resubmit the rejected vulnerabilities */
  resubmitVulnerabilities: SimplePayload;
  /** Helper mutation to send a notification to the assigned hacker */
  sendAssignedNotification: SimplePayload;
  /** Helper mutation to send an email when new user has completed registration */
  sendNewEnrolledUser: SimplePayload;
  /** Helper mutation to send a manual vulnerability report notification */
  sendVulnerabilityNotification: SimplePayload;
  /** Set mailmap entry as mailmap subentry */
  setMailmapEntryAsMailmapSubentry: SimplePayload;
  /** Set mailmap subentry as mailmap entry */
  setMailmapSubentryAsMailmapEntry: MailmapEntryPayload;
  /** Signs a POST URL to upload files to a Group's resources bucket */
  signPostUrl: SignPostUrlsPayload;
  /** Marks an ongoing Event within the group as solved */
  solveEvent: SimplePayload;
  /** Queues a Machine execution to check all findings over specified roots */
  submitGroupMachineExecution: SimplePayloadMessage;
  /** Queues a job to execute Machine (Skims) on the provided root */
  submitMachineJob: SimplePayloadMessage;
  /**
   * Submits a Finding policy for the Organization, pending approval of a
   * Stakeholder
   */
  submitOrganizationFindingPolicy: SimplePayload;
  /** Queues an execution to sync the state of the S3 mirror with the repository */
  syncGitRoot: SimplePayload;
  /** Revokes your access permissions to a group */
  unsubscribeFromGroup: SimplePayload;
  /** Updates Azure integration */
  updateAzureIssuesIntegration: SimplePayload;
  /** Updates any credentials at the organization */
  updateCredentials: SimplePayload;
  /** Updates credit card payment method from an organization */
  updateCreditCardPaymentMethod: SimplePayload;
  /** Updates a Finding's description */
  updateDescription: SimpleFindingPayload;
  /** Updates and enrollment reason */
  updateEnrollment: SimplePayload;
  /** Updates an Event's description */
  updateEvent: SimplePayload;
  /** Updates an Event's evidence */
  updateEventEvidence: SimplePayload;
  /** Updates a Finding's evidence */
  updateEvidence: SimplePayload;
  /** Updates the description of an Evidence in a Finding */
  updateEvidenceDescription: SimplePayload;
  /** Updates the Forces token in use for a Group */
  updateForcesAccessToken: UpdateAccessTokenPayload;
  /** Updates GitLab integration */
  updateGitLabIssuesIntegration: SimplePayload;
  /** Updates a Group's Git root and its attributes */
  updateGitRoot: SimplePayload;
  /** Updates the Environment File of a Root */
  updateGitRootEnvironmentFile: SimplePayload;
  /** Updates the Environment URL of a Root */
  updateGitRootEnvironmentUrl: SimplePayload;
  /** Updates a group's services and parameters */
  updateGroup: SimplePayload;
  /** Updates the information on how to access group TOE */
  updateGroupAccessInfo: SimplePayload;
  /** Updates the necessary clarifications on what should be tested */
  updateGroupDisambiguation: SimplePayload;
  /** Updates a group's general information */
  updateGroupInfo: SimplePayload;
  /** Updates group information on whether it's managed manually */
  updateGroupManaged: SimplePayload;
  /** Updates the Group's policies */
  updateGroupPolicies: SimplePayload;
  /** Updates/edits an user belonging to a group */
  updateGroupStakeholder: UpdateStakeholderPayload;
  /** Updates policies of several groups. All groups should belong to the same organization */
  updateGroupsPolicies: SimplePayload;
  /** Updates a hook */
  updateHook: SimplePayload;
  /** Updates a Group's IP root and its attributes */
  updateIpRoot: SimplePayload;
  /** Update mailmap entry */
  updateMailmapEntry: SimplePayload;
  /** Update mailmap subentry */
  updateMailmapSubentry: SimplePayload;
  /** Updates user notifications */
  updateNotifications: SimplePayload;
  /** Updates users notifications preferences */
  updateNotificationsPreferences: SimplePayload;
  /** Updates the Organization's common policies */
  updateOrganizationPolicies: SimplePayload;
  /** Updates the Organization's priority policies */
  updateOrganizationPriorityPolicies: SimplePayload;
  /** Updates an stakeholder's data from an Organization */
  updateOrganizationStakeholder: UpdateStakeholderPayload;
  /** Updates other payment method from an organization */
  updateOtherPaymentMethod: SimplePayload;
  /**
   * Updates the git root cloning status and its related HEAD commit if
   * successful
   */
  updateRootCloningStatus: SimplePayload;
  /** Updates a Docker image in the root */
  updateRootDockerImage: SimplePayload;
  /** Updates a secret in a git environment */
  updateSecret: SimplePayload;
  /** Updates the severity of a Finding */
  updateSeverity: SimpleFindingPayload;
  /** Updates an stakeholder information */
  updateStakeholder: SimplePayload;
  /** Updates the stakeholder's mobile */
  updateStakeholderPhone: SimplePayload;
  /** Updates the input of a concrete root */
  updateToeInput: UpdateToeInputPayload;
  /** Updates the attacked lines for one or more ToE Lines of a concrete root */
  updateToeLinesAttackedLines: UpdateToeLinesPayload;
  /** Updates the loc for a ToE Line of a concrete root */
  updateToeLinesLoc: UpdateToeLinesPayload;
  /**
   * Updates the sorts_risk_level attribute in one or more ToEs of
   * a concrete group
   */
  updateToeLinesSorts: SimplePayload;
  /** Updates the port of a concrete root */
  updateToePort: UpdateToePortPayload;
  /** Updates the new user completed workflow */
  updateTours: SimplePayload;
  /** Updates a Group's URL root and its attributes */
  updateUrlRoot: SimplePayload;
  /** Updates the severity for the given vulnerabilities */
  updateVulnerabilitiesSeverity: SimpleFindingPayload;
  /** Updates the treatment of a single Vulnerability */
  updateVulnerabilitiesTreatment: SimplePayload;
  /** Updates Vulnerability description */
  updateVulnerabilityDescription: SimplePayload;
  /** Updates the associated issue from a third party management system */
  updateVulnerabilityIssue: SimplePayload;
  /** Updates data related to the treatment of a Vulnerability */
  updateVulnerabilityTreatment: SimplePayload;
  /** Uploads a JSON file generated by postman which contains the API endpoints */
  uploadEndpointsFile: SimplePayloadMessage;
  /** Uploads a file describing a Vulnerability to a Finding */
  uploadFile: SimplePayloadMessage;
  /** Uploads several repositories through CSV file */
  uploadGitRootFile: UploadGitRootFilePayload;
  /** Validates if Git root is accessible with the credentials provided */
  validateGitAccess: SimplePayload;
  /** Starts a verification process for a stakeholder by using OTP */
  verifyStakeholder: SimplePayload;
  /** To verify if the vulnerabilities still remain open or were closed */
  verifyVulnerabilitiesRequest: SimplePayload;
};


/** Main mutation object */
export type MutationAcceptLegalArgs = {
  remember?: InputMaybe<Scalars['Boolean']['input']>;
};


/** Main mutation object */
export type MutationActivateRootArgs = {
  groupName: Scalars['String']['input'];
  id: Scalars['ID']['input'];
};


/** Main mutation object */
export type MutationAddAccessTokenArgs = {
  expirationTime: Scalars['Int']['input'];
  name: Scalars['String']['input'];
};


/** Main mutation object */
export type MutationAddAuditEventArgs = {
  object: Scalars['String']['input'];
  objectId: Scalars['ID']['input'];
};


/** Main mutation object */
export type MutationAddCredentialsArgs = {
  credentials: CredentialsInput;
  organizationId: Scalars['ID']['input'];
};


/** Main mutation object */
export type MutationAddCreditCardPaymentMethodArgs = {
  billedGroups: Array<Scalars['String']['input']>;
  billingEmail: Scalars['String']['input'];
  businessId?: InputMaybe<Scalars['String']['input']>;
  businessName?: InputMaybe<Scalars['String']['input']>;
  country?: InputMaybe<Scalars['String']['input']>;
  makeDefault: Scalars['Boolean']['input'];
  organizationId: Scalars['String']['input'];
  paymentMethodId: Scalars['String']['input'];
};


/** Main mutation object */
export type MutationAddEnvironmentIntegrityEventArgs = {
  groupName: Scalars['String']['input'];
  urlId: Scalars['String']['input'];
};


/** Main mutation object */
export type MutationAddEventArgs = {
  detail: Scalars['String']['input'];
  environmentUrl?: InputMaybe<Scalars['String']['input']>;
  eventDate: Scalars['DateTime']['input'];
  eventType: EventType;
  groupName: Scalars['String']['input'];
  rootId?: InputMaybe<Scalars['ID']['input']>;
};


/** Main mutation object */
export type MutationAddEventConsultArgs = {
  content: Scalars['String']['input'];
  eventId: Scalars['String']['input'];
  groupName: Scalars['String']['input'];
  parentComment: Scalars['GenericScalar']['input'];
};


/** Main mutation object */
export type MutationAddFilesToDbArgs = {
  filesDataInput?: InputMaybe<Array<FilesDataInput>>;
  groupName: Scalars['String']['input'];
};


/** Main mutation object */
export type MutationAddFindingArgs = {
  attackVectorDescription: Scalars['String']['input'];
  cvss4Vector: Scalars['String']['input'];
  cvssVector?: InputMaybe<Scalars['String']['input']>;
  description: Scalars['String']['input'];
  groupName: Scalars['String']['input'];
  minTimeToRemediate?: InputMaybe<Scalars['Int']['input']>;
  recommendation: Scalars['String']['input'];
  threat: Scalars['String']['input'];
  title: Scalars['String']['input'];
  unfulfilledRequirements: Array<Scalars['String']['input']>;
};


/** Main mutation object */
export type MutationAddFindingConsultArgs = {
  content: Scalars['String']['input'];
  findingId: Scalars['String']['input'];
  parentComment: Scalars['GenericScalar']['input'];
  type: FindingConsultType;
};


/** Main mutation object */
export type MutationAddForcesExecutionArgs = {
  date: Scalars['DateTime']['input'];
  daysUntilItBreaks?: InputMaybe<Scalars['Int']['input']>;
  executionId?: InputMaybe<Scalars['String']['input']>;
  exitCode: Scalars['String']['input'];
  gitBranch?: InputMaybe<Scalars['String']['input']>;
  gitCommit?: InputMaybe<Scalars['String']['input']>;
  gitOrigin?: InputMaybe<Scalars['String']['input']>;
  gitRepo?: InputMaybe<Scalars['String']['input']>;
  gracePeriod?: InputMaybe<Scalars['Int']['input']>;
  groupName: Scalars['String']['input'];
  jsonLog?: InputMaybe<Scalars['String']['input']>;
  kind?: InputMaybe<Scalars['String']['input']>;
  log?: InputMaybe<Scalars['Upload']['input']>;
  managedVulnerabilities?: InputMaybe<Scalars['Int']['input']>;
  severityThreshold?: InputMaybe<Scalars['Float']['input']>;
  strictness: Scalars['String']['input'];
  unManagedVulnerabilities?: InputMaybe<Scalars['Int']['input']>;
  vulnerabilities: ExecutionVulnerabilitiesInput;
};


/** Main mutation object */
export type MutationAddGitEnvironmentCspmArgs = {
  azureClientId?: InputMaybe<Scalars['String']['input']>;
  azureClientSecret?: InputMaybe<Scalars['String']['input']>;
  azureSubscriptionId?: InputMaybe<Scalars['String']['input']>;
  azureTenantId?: InputMaybe<Scalars['String']['input']>;
  cloudName: Scalars['String']['input'];
  gcpPrivateKey?: InputMaybe<Scalars['String']['input']>;
  groupName: Scalars['String']['input'];
  rootId: Scalars['ID']['input'];
  url: Scalars['String']['input'];
  useEgress?: InputMaybe<Scalars['Boolean']['input']>;
  useVpn?: InputMaybe<Scalars['Boolean']['input']>;
  useZtna?: InputMaybe<Scalars['Boolean']['input']>;
};


/** Main mutation object */
export type MutationAddGitEnvironmentUrlArgs = {
  cloudName?: InputMaybe<Scalars['String']['input']>;
  groupName: Scalars['String']['input'];
  isProduction?: InputMaybe<Scalars['Boolean']['input']>;
  rootId: Scalars['ID']['input'];
  url: Scalars['String']['input'];
  urlType: GitEnvironmentCloud;
  useEgress?: InputMaybe<Scalars['Boolean']['input']>;
  useVpn?: InputMaybe<Scalars['Boolean']['input']>;
  useZtna?: InputMaybe<Scalars['Boolean']['input']>;
};


/** Main mutation object */
export type MutationAddGitRootArgs = {
  branch: Scalars['String']['input'];
  credentials?: InputMaybe<RootCredentialsInput>;
  criticality?: InputMaybe<RootCriticality>;
  environment?: InputMaybe<Scalars['String']['input']>;
  gitignore?: InputMaybe<Array<Scalars['String']['input']>>;
  groupName: Scalars['String']['input'];
  includesHealthCheck: Scalars['Boolean']['input'];
  nickname?: InputMaybe<Scalars['String']['input']>;
  url: Scalars['String']['input'];
  useEgress?: InputMaybe<Scalars['Boolean']['input']>;
  useVpn?: InputMaybe<Scalars['Boolean']['input']>;
  useZtna?: InputMaybe<Scalars['Boolean']['input']>;
};


/** Main mutation object */
export type MutationAddGroupArgs = {
  description: Scalars['String']['input'];
  groupName: Scalars['String']['input'];
  hasAdvanced?: InputMaybe<Scalars['Boolean']['input']>;
  hasEssential?: InputMaybe<Scalars['Boolean']['input']>;
  language?: InputMaybe<Language>;
  organizationName?: InputMaybe<Scalars['String']['input']>;
  service?: InputMaybe<ServiceType>;
  subscription?: InputMaybe<SubscriptionType>;
};


/** Main mutation object */
export type MutationAddGroupConsultArgs = {
  content: Scalars['String']['input'];
  groupName: Scalars['String']['input'];
  parentComment: Scalars['GenericScalar']['input'];
};


/** Main mutation object */
export type MutationAddGroupTagsArgs = {
  groupName: Scalars['String']['input'];
  tagsData?: InputMaybe<Array<Scalars['String']['input']>>;
};


/** Main mutation object */
export type MutationAddHookArgs = {
  groupName: Scalars['String']['input'];
  hook: HookInput;
};


/** Main mutation object */
export type MutationAddIpRootArgs = {
  address: Scalars['String']['input'];
  groupName: Scalars['String']['input'];
  nickname: Scalars['String']['input'];
};


/** Main mutation object */
export type MutationAddOrganizationArgs = {
  country?: InputMaybe<Scalars['String']['input']>;
  name: Scalars['String']['input'];
};


/** Main mutation object */
export type MutationAddOrganizationFindingPolicyArgs = {
  findingName: Scalars['String']['input'];
  organizationName: Scalars['String']['input'];
  tags?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  treatmentAcceptance: TreatmentAcceptancePolicy;
};


/** Main mutation object */
export type MutationAddOtherPaymentMethodArgs = {
  businessName: Scalars['String']['input'];
  city: Scalars['String']['input'];
  country: Scalars['String']['input'];
  email: Scalars['String']['input'];
  organizationId: Scalars['String']['input'];
  rut?: InputMaybe<Scalars['Upload']['input']>;
  state: Scalars['String']['input'];
  taxId?: InputMaybe<Scalars['Upload']['input']>;
};


/** Main mutation object */
export type MutationAddRootDockerImageArgs = {
  credentials?: InputMaybe<RootCredentialsInput>;
  groupName: Scalars['String']['input'];
  rootId: Scalars['ID']['input'];
  uri: Scalars['String']['input'];
};


/** Main mutation object */
export type MutationAddSecretArgs = {
  description?: InputMaybe<Scalars['String']['input']>;
  groupName: Scalars['String']['input'];
  key: Scalars['String']['input'];
  resourceId: Scalars['ID']['input'];
  resourceType: ResourceType;
  value: Scalars['String']['input'];
};


/** Main mutation object */
export type MutationAddStakeholderArgs = {
  email: Scalars['String']['input'];
  role: StakeholderRole;
};


/** Main mutation object */
export type MutationAddToeInputArgs = {
  component: Scalars['String']['input'];
  entryPoint: Scalars['String']['input'];
  environmentId: Scalars['String']['input'];
  groupName: Scalars['String']['input'];
  rootId: Scalars['String']['input'];
};


/** Main mutation object */
export type MutationAddToeLinesArgs = {
  filename: Scalars['String']['input'];
  groupName: Scalars['String']['input'];
  lastAuthor: Scalars['String']['input'];
  lastCommit: Scalars['String']['input'];
  loc: Scalars['Int']['input'];
  modifiedDate: Scalars['DateTime']['input'];
  rootId: Scalars['String']['input'];
};


/** Main mutation object */
export type MutationAddToePortArgs = {
  address: Scalars['String']['input'];
  groupName: Scalars['String']['input'];
  port: Scalars['Int']['input'];
  rootId: Scalars['String']['input'];
};


/** Main mutation object */
export type MutationAddUrlRootArgs = {
  groupName: Scalars['String']['input'];
  nickname: Scalars['String']['input'];
  url: Scalars['String']['input'];
};


/** Main mutation object */
export type MutationApproveEvidenceArgs = {
  evidenceId: EvidenceDescriptionType;
  findingId: Scalars['String']['input'];
};


/** Main mutation object */
export type MutationApproveVulnerabilitiesSeverityUpdateArgs = {
  findingId: Scalars['ID']['input'];
  vulnerabilityIds: Array<Scalars['ID']['input']>;
};


/** Main mutation object */
export type MutationCloseVulnerabilitiesArgs = {
  findingId: Scalars['String']['input'];
  justification?: InputMaybe<VulnerabilityStateReason>;
  vulnerabilities: Array<Scalars['String']['input']>;
};


/** Main mutation object */
export type MutationConfirmVulnerabilitiesArgs = {
  findingId: Scalars['String']['input'];
  vulnerabilities: Array<Scalars['String']['input']>;
};


/** Main mutation object */
export type MutationConfirmVulnerabilitiesZeroRiskArgs = {
  findingId: Scalars['String']['input'];
  justification: Scalars['String']['input'];
  vulnerabilities: Array<InputMaybe<Scalars['String']['input']>>;
};


/** Main mutation object */
export type MutationConvertMailmapSubentriesToNewEntryArgs = {
  entryEmail: Scalars['String']['input'];
  mainSubentry: MailmapSubentryInput;
  organizationId: Scalars['String']['input'];
  otherSubentries: Array<MailmapSubentryInput>;
};


/** Main mutation object */
export type MutationCreateMailmapEntryArgs = {
  entry: MailmapEntryInput;
  organizationId: Scalars['String']['input'];
};


/** Main mutation object */
export type MutationCreateMailmapSubentryArgs = {
  entryEmail: Scalars['String']['input'];
  organizationId: Scalars['String']['input'];
  subentry: MailmapSubentryInput;
};


/** Main mutation object */
export type MutationDeactivateOrganizationFindingPolicyArgs = {
  findingPolicyId: Scalars['ID']['input'];
  organizationName: Scalars['String']['input'];
  treatmentAcceptance: TreatmentAcceptancePolicy;
};


/** Main mutation object */
export type MutationDeactivateRootArgs = {
  groupName: Scalars['String']['input'];
  id: Scalars['ID']['input'];
  other?: InputMaybe<Scalars['String']['input']>;
  reason: RootDeactivationReason;
};


/** Main mutation object */
export type MutationDeleteMailmapEntryArgs = {
  entryEmail: Scalars['String']['input'];
  organizationId: Scalars['String']['input'];
};


/** Main mutation object */
export type MutationDeleteMailmapEntrySubentriesArgs = {
  entryEmail: Scalars['String']['input'];
  organizationId: Scalars['String']['input'];
};


/** Main mutation object */
export type MutationDeleteMailmapEntryWithSubentriesArgs = {
  entryEmail: Scalars['String']['input'];
  organizationId: Scalars['String']['input'];
};


/** Main mutation object */
export type MutationDeleteMailmapSubentryArgs = {
  entryEmail: Scalars['String']['input'];
  organizationId: Scalars['String']['input'];
  subentryEmail: Scalars['String']['input'];
  subentryName: Scalars['String']['input'];
};


/** Main mutation object */
export type MutationDownloadBillingFileArgs = {
  fileName: Scalars['String']['input'];
  organizationId: Scalars['String']['input'];
  paymentMethodId: Scalars['String']['input'];
};


/** Main mutation object */
export type MutationDownloadEventFileArgs = {
  eventId: Scalars['String']['input'];
  fileName: Scalars['String']['input'];
  groupName: Scalars['String']['input'];
};


/** Main mutation object */
export type MutationDownloadFileArgs = {
  filesDataInput: Scalars['String']['input'];
  groupName: Scalars['String']['input'];
};


/** Main mutation object */
export type MutationDownloadGitRootsFileArgs = {
  groupName: Scalars['String']['input'];
};


/** Main mutation object */
export type MutationDownloadVulnerabilityFileArgs = {
  findingId: Scalars['String']['input'];
};


/** Main mutation object */
export type MutationGrantStakeholderAccessArgs = {
  email: Scalars['String']['input'];
  groupName: Scalars['String']['input'];
  responsibility?: InputMaybe<Scalars['String']['input']>;
  role: StakeholderRole;
};


/** Main mutation object */
export type MutationGrantStakeholderOrganizationAccessArgs = {
  organizationId: Scalars['String']['input'];
  role: OrganizationRole;
  userEmail: Scalars['String']['input'];
};


/** Main mutation object */
export type MutationHandleOrganizationFindingPolicyAcceptanceArgs = {
  findingPolicyId: Scalars['ID']['input'];
  organizationName: Scalars['String']['input'];
  status: OrganizationFindingPolicy;
  treatmentAcceptance: TreatmentAcceptancePolicy;
};


/** Main mutation object */
export type MutationHandleVulnerabilitiesAcceptanceArgs = {
  acceptedVulnerabilities: Array<InputMaybe<Scalars['String']['input']>>;
  findingId: Scalars['String']['input'];
  justification: Scalars['String']['input'];
  rejectedVulnerabilities: Array<InputMaybe<Scalars['String']['input']>>;
};


/** Main mutation object */
export type MutationImportMailmapArgs = {
  file: Scalars['Upload']['input'];
  organizationId: Scalars['String']['input'];
};


/** Main mutation object */
export type MutationInvalidateAccessTokenArgs = {
  id?: InputMaybe<Scalars['ID']['input']>;
};


/** Main mutation object */
export type MutationMoveEnvironmentArgs = {
  groupName: Scalars['String']['input'];
  rootId: Scalars['ID']['input'];
  targetGroupName: Scalars['String']['input'];
  targetRootId: Scalars['ID']['input'];
  urlId: Scalars['String']['input'];
};


/** Main mutation object */
export type MutationMoveMailmapEntryWithSubentriesArgs = {
  entryEmail: Scalars['String']['input'];
  newOrganizationId: Scalars['String']['input'];
  organizationId: Scalars['String']['input'];
};


/** Main mutation object */
export type MutationMoveRootArgs = {
  groupName: Scalars['String']['input'];
  id: Scalars['ID']['input'];
  targetGroupName: Scalars['String']['input'];
};


/** Main mutation object */
export type MutationRefreshToeLinesArgs = {
  groupName: Scalars['String']['input'];
};


/** Main mutation object */
export type MutationRejectEventSolutionArgs = {
  comments: Scalars['String']['input'];
  eventId: Scalars['String']['input'];
  groupName: Scalars['String']['input'];
};


/** Main mutation object */
export type MutationRejectEvidenceArgs = {
  evidenceId: EvidenceDescriptionType;
  findingId: Scalars['String']['input'];
  justification: Scalars['String']['input'];
};


/** Main mutation object */
export type MutationRejectVulnerabilitiesArgs = {
  findingId: Scalars['String']['input'];
  otherReason?: InputMaybe<Scalars['String']['input']>;
  reasons: Array<VulnerabilityRejectionReason>;
  vulnerabilities: Array<Scalars['String']['input']>;
};


/** Main mutation object */
export type MutationRejectVulnerabilitiesSeverityUpdateArgs = {
  findingId: Scalars['String']['input'];
  justification: Scalars['String']['input'];
  vulnerabilityIds: Array<Scalars['String']['input']>;
};


/** Main mutation object */
export type MutationRejectVulnerabilitiesZeroRiskArgs = {
  findingId: Scalars['String']['input'];
  justification: Scalars['String']['input'];
  vulnerabilities: Array<InputMaybe<Scalars['String']['input']>>;
};


/** Main mutation object */
export type MutationRemoveCredentialsArgs = {
  credentialsId: Scalars['ID']['input'];
  organizationId: Scalars['ID']['input'];
};


/** Main mutation object */
export type MutationRemoveEnvironmentUrlArgs = {
  groupName: Scalars['String']['input'];
  rootId: Scalars['ID']['input'];
  urlId: Scalars['String']['input'];
};


/** Main mutation object */
export type MutationRemoveEventEvidenceArgs = {
  eventId: Scalars['String']['input'];
  evidenceType: EventEvidenceType;
  groupName: Scalars['String']['input'];
};


/** Main mutation object */
export type MutationRemoveEvidenceArgs = {
  evidenceId: EvidenceType;
  findingId: Scalars['String']['input'];
};


/** Main mutation object */
export type MutationRemoveFilesArgs = {
  filesDataInput: FilesDataInput;
  groupName: Scalars['String']['input'];
};


/** Main mutation object */
export type MutationRemoveFindingArgs = {
  findingId: Scalars['String']['input'];
  justification: RemoveFindingJustification;
};


/** Main mutation object */
export type MutationRemoveGroupArgs = {
  comments?: InputMaybe<Scalars['String']['input']>;
  groupName: Scalars['String']['input'];
  reason: RemoveGroupReason;
};


/** Main mutation object */
export type MutationRemoveGroupHookArgs = {
  groupName: Scalars['String']['input'];
  hookId: Scalars['String']['input'];
};


/** Main mutation object */
export type MutationRemoveGroupIntegrationArgs = {
  groupName: Scalars['String']['input'];
  integration: GroupIntegration;
};


/** Main mutation object */
export type MutationRemoveGroupTagArgs = {
  groupName: Scalars['String']['input'];
  tag: Scalars['String']['input'];
};


/** Main mutation object */
export type MutationRemoveOrganizationPriorityPolicyArgs = {
  organizationId: Scalars['String']['input'];
  priorityPolicy: Scalars['String']['input'];
};


/** Main mutation object */
export type MutationRemovePaymentMethodArgs = {
  organizationId: Scalars['String']['input'];
  paymentMethodId: Scalars['String']['input'];
};


/** Main mutation object */
export type MutationRemoveRootDockerImageArgs = {
  groupName: Scalars['String']['input'];
  rootId: Scalars['ID']['input'];
  uri: Scalars['String']['input'];
};


/** Main mutation object */
export type MutationRemoveSecretArgs = {
  groupName: Scalars['String']['input'];
  key: Scalars['String']['input'];
  resourceId: Scalars['ID']['input'];
  resourceType: ResourceType;
};


/** Main mutation object */
export type MutationRemoveStakeholderAccessArgs = {
  groupName?: InputMaybe<Scalars['String']['input']>;
  userEmail: Scalars['String']['input'];
};


/** Main mutation object */
export type MutationRemoveStakeholderOrganizationAccessArgs = {
  organizationId: Scalars['String']['input'];
  userEmail: Scalars['String']['input'];
};


/** Main mutation object */
export type MutationRemoveTagsArgs = {
  findingId: Scalars['String']['input'];
  tag?: InputMaybe<Scalars['String']['input']>;
  vulnerabilities: Array<InputMaybe<Scalars['String']['input']>>;
};


/** Main mutation object */
export type MutationRemoveTrustedDeviceArgs = {
  trustedDevice: TrustedDeviceInput;
};


/** Main mutation object */
export type MutationRemoveVulnerabilityArgs = {
  findingId: Scalars['String']['input'];
  id: Scalars['String']['input'];
  justification: RemoveVulnerabilityJustification;
};


/** Main mutation object */
export type MutationRequestEventVerificationArgs = {
  comments: Scalars['String']['input'];
  eventId: Scalars['String']['input'];
  groupName: Scalars['String']['input'];
};


/** Main mutation object */
export type MutationRequestGroupsUpgradeArgs = {
  groupNames: Array<Scalars['String']['input']>;
};


/** Main mutation object */
export type MutationRequestSbomFileArgs = {
  fileFormat: Scalars['String']['input'];
  groupName: Scalars['String']['input'];
  rootNicknames?: InputMaybe<Array<Scalars['String']['input']>>;
  sbomFormat: Scalars['String']['input'];
  urisWithCredentials?: InputMaybe<Array<UriWithCredentialsInput>>;
};


/** Main mutation object */
export type MutationRequestVulnerabilitiesHoldArgs = {
  eventId: Scalars['String']['input'];
  findingId: Scalars['String']['input'];
  groupName: Scalars['String']['input'];
  vulnerabilities: Array<Scalars['String']['input']>;
};


/** Main mutation object */
export type MutationRequestVulnerabilitiesSeverityUpdateArgs = {
  cvss4Vector: Scalars['String']['input'];
  cvssVector?: InputMaybe<Scalars['String']['input']>;
  findingId: Scalars['ID']['input'];
  vulnerabilityIds: Array<Scalars['ID']['input']>;
};


/** Main mutation object */
export type MutationRequestVulnerabilitiesVerificationArgs = {
  findingId: Scalars['String']['input'];
  justification: Scalars['String']['input'];
  vulnerabilities: Array<InputMaybe<Scalars['String']['input']>>;
};


/** Main mutation object */
export type MutationRequestVulnerabilitiesZeroRiskArgs = {
  findingId: Scalars['String']['input'];
  justification: Scalars['String']['input'];
  vulnerabilities: Array<InputMaybe<Scalars['String']['input']>>;
};


/** Main mutation object */
export type MutationResubmitVulnerabilitiesArgs = {
  findingId: Scalars['String']['input'];
  vulnerabilities: Array<Scalars['String']['input']>;
};


/** Main mutation object */
export type MutationSendAssignedNotificationArgs = {
  findingId: Scalars['String']['input'];
  vulnerabilities: Array<InputMaybe<Scalars['String']['input']>>;
};


/** Main mutation object */
export type MutationSendNewEnrolledUserArgs = {
  groupName: Scalars['String']['input'];
};


/** Main mutation object */
export type MutationSendVulnerabilityNotificationArgs = {
  findingId: Scalars['String']['input'];
};


/** Main mutation object */
export type MutationSetMailmapEntryAsMailmapSubentryArgs = {
  entryEmail: Scalars['String']['input'];
  organizationId: Scalars['String']['input'];
  targetEntryEmail: Scalars['String']['input'];
};


/** Main mutation object */
export type MutationSetMailmapSubentryAsMailmapEntryArgs = {
  entryEmail: Scalars['String']['input'];
  organizationId: Scalars['String']['input'];
  subentryEmail: Scalars['String']['input'];
  subentryName: Scalars['String']['input'];
};


/** Main mutation object */
export type MutationSignPostUrlArgs = {
  filesDataInput?: InputMaybe<Array<FilesDataInput>>;
  groupName: Scalars['String']['input'];
};


/** Main mutation object */
export type MutationSolveEventArgs = {
  eventId: Scalars['String']['input'];
  groupName: Scalars['String']['input'];
  other?: InputMaybe<Scalars['String']['input']>;
  reason: SolveEventReason;
};


/** Main mutation object */
export type MutationSubmitGroupMachineExecutionArgs = {
  groupName: Scalars['String']['input'];
  rootNicknames?: InputMaybe<Array<Scalars['String']['input']>>;
};


/** Main mutation object */
export type MutationSubmitMachineJobArgs = {
  findingId: Scalars['String']['input'];
  rootNicknames?: InputMaybe<Array<Scalars['String']['input']>>;
};


/** Main mutation object */
export type MutationSubmitOrganizationFindingPolicyArgs = {
  findingPolicyId: Scalars['ID']['input'];
  organizationName: Scalars['String']['input'];
};


/** Main mutation object */
export type MutationSyncGitRootArgs = {
  groupName: Scalars['String']['input'];
  rootId: Scalars['String']['input'];
};


/** Main mutation object */
export type MutationUnsubscribeFromGroupArgs = {
  groupName: Scalars['String']['input'];
};


/** Main mutation object */
export type MutationUpdateAzureIssuesIntegrationArgs = {
  assignedTo?: InputMaybe<Scalars['String']['input']>;
  azureOrganizationName: Scalars['String']['input'];
  azureProjectName: Scalars['String']['input'];
  groupName: Scalars['String']['input'];
  issueAutomationEnabled?: InputMaybe<Scalars['Boolean']['input']>;
  tags?: InputMaybe<Array<Scalars['String']['input']>>;
};


/** Main mutation object */
export type MutationUpdateCredentialsArgs = {
  credentials: CredentialsInput;
  credentialsId: Scalars['ID']['input'];
  organizationId: Scalars['ID']['input'];
};


/** Main mutation object */
export type MutationUpdateCreditCardPaymentMethodArgs = {
  cardExpirationMonth?: InputMaybe<Scalars['Int']['input']>;
  cardExpirationYear?: InputMaybe<Scalars['Int']['input']>;
  makeDefault: Scalars['Boolean']['input'];
  organizationId: Scalars['String']['input'];
  paymentMethodId: Scalars['String']['input'];
};


/** Main mutation object */
export type MutationUpdateDescriptionArgs = {
  attackVectorDescription: Scalars['String']['input'];
  description: Scalars['String']['input'];
  findingId: Scalars['String']['input'];
  recommendation: Scalars['String']['input'];
  records?: InputMaybe<Scalars['String']['input']>;
  sorts?: InputMaybe<Sorts>;
  threat: Scalars['String']['input'];
  title: Scalars['String']['input'];
  unfulfilledRequirements?: InputMaybe<Array<Scalars['String']['input']>>;
};


/** Main mutation object */
export type MutationUpdateEnrollmentArgs = {
  reason: TrialReason;
};


/** Main mutation object */
export type MutationUpdateEventArgs = {
  eventDescription?: InputMaybe<Scalars['String']['input']>;
  eventId: Scalars['String']['input'];
  eventType?: InputMaybe<EventType>;
  groupName: Scalars['String']['input'];
  otherSolvingReason?: InputMaybe<Scalars['String']['input']>;
  solvingReason?: InputMaybe<SolveEventReason>;
};


/** Main mutation object */
export type MutationUpdateEventEvidenceArgs = {
  eventId: Scalars['String']['input'];
  evidenceType: EventEvidenceType;
  file: Scalars['Upload']['input'];
  groupName: Scalars['String']['input'];
};


/** Main mutation object */
export type MutationUpdateEvidenceArgs = {
  evidenceId: EvidenceType;
  file: Scalars['Upload']['input'];
  findingId: Scalars['String']['input'];
};


/** Main mutation object */
export type MutationUpdateEvidenceDescriptionArgs = {
  description: Scalars['String']['input'];
  evidenceId: EvidenceDescriptionType;
  findingId: Scalars['String']['input'];
};


/** Main mutation object */
export type MutationUpdateForcesAccessTokenArgs = {
  groupName?: InputMaybe<Scalars['String']['input']>;
};


/** Main mutation object */
export type MutationUpdateGitLabIssuesIntegrationArgs = {
  assigneeIds?: InputMaybe<Array<Scalars['Int']['input']>>;
  gitlabProjectName: Scalars['String']['input'];
  groupName: Scalars['String']['input'];
  issueAutomationEnabled?: InputMaybe<Scalars['Boolean']['input']>;
  labels?: InputMaybe<Array<Scalars['String']['input']>>;
};


/** Main mutation object */
export type MutationUpdateGitRootArgs = {
  branch: Scalars['String']['input'];
  credentials?: InputMaybe<RootCredentialsInput>;
  criticality?: InputMaybe<RootCriticality>;
  environment?: InputMaybe<Scalars['String']['input']>;
  gitignore?: InputMaybe<Array<Scalars['String']['input']>>;
  groupName: Scalars['String']['input'];
  id: Scalars['ID']['input'];
  includesHealthCheck: Scalars['Boolean']['input'];
  nickname?: InputMaybe<Scalars['String']['input']>;
  url: Scalars['String']['input'];
  useEgress?: InputMaybe<Scalars['Boolean']['input']>;
  useVpn?: InputMaybe<Scalars['Boolean']['input']>;
  useZtna?: InputMaybe<Scalars['Boolean']['input']>;
};


/** Main mutation object */
export type MutationUpdateGitRootEnvironmentFileArgs = {
  fileName: Scalars['String']['input'];
  groupName: Scalars['String']['input'];
  oldFileName: Scalars['String']['input'];
  rootId: Scalars['ID']['input'];
  urlId: Scalars['String']['input'];
};


/** Main mutation object */
export type MutationUpdateGitRootEnvironmentUrlArgs = {
  groupName: Scalars['String']['input'];
  include?: InputMaybe<Scalars['Boolean']['input']>;
  rootId: Scalars['ID']['input'];
  urlId: Scalars['String']['input'];
};


/** Main mutation object */
export type MutationUpdateGroupArgs = {
  comments: Scalars['String']['input'];
  description?: InputMaybe<Scalars['String']['input']>;
  groupName: Scalars['String']['input'];
  hasAdvanced?: InputMaybe<Scalars['Boolean']['input']>;
  hasAsm: Scalars['Boolean']['input'];
  hasEssential?: InputMaybe<Scalars['Boolean']['input']>;
  language?: InputMaybe<Scalars['String']['input']>;
  reason: UpdateGroupReason;
  service?: InputMaybe<ServiceType>;
  subscription: SubscriptionType;
  tier?: InputMaybe<TierType>;
};


/** Main mutation object */
export type MutationUpdateGroupAccessInfoArgs = {
  groupContext?: InputMaybe<Scalars['String']['input']>;
  groupName: Scalars['String']['input'];
};


/** Main mutation object */
export type MutationUpdateGroupDisambiguationArgs = {
  disambiguation?: InputMaybe<Scalars['String']['input']>;
  groupName: Scalars['String']['input'];
};


/** Main mutation object */
export type MutationUpdateGroupInfoArgs = {
  businessId?: InputMaybe<Scalars['String']['input']>;
  businessName?: InputMaybe<Scalars['String']['input']>;
  description: Scalars['String']['input'];
  groupName: Scalars['String']['input'];
  language: Language;
  sprintDuration?: InputMaybe<Scalars['Int']['input']>;
  sprintStartDate?: InputMaybe<Scalars['DateTime']['input']>;
};


/** Main mutation object */
export type MutationUpdateGroupManagedArgs = {
  comments: Scalars['String']['input'];
  groupName: Scalars['String']['input'];
  managed: ManagedType;
};


/** Main mutation object */
export type MutationUpdateGroupPoliciesArgs = {
  daysUntilItBreaks?: InputMaybe<Scalars['Int']['input']>;
  groupName: Scalars['String']['input'];
  maxAcceptanceDays?: InputMaybe<Scalars['Int']['input']>;
  maxAcceptanceSeverity?: InputMaybe<Scalars['Float']['input']>;
  maxNumberAcceptances?: InputMaybe<Scalars['Int']['input']>;
  minAcceptanceSeverity?: InputMaybe<Scalars['Float']['input']>;
  minBreakingSeverity?: InputMaybe<Scalars['Float']['input']>;
  vulnerabilityGracePeriod?: InputMaybe<Scalars['Int']['input']>;
};


/** Main mutation object */
export type MutationUpdateGroupStakeholderArgs = {
  email: Scalars['String']['input'];
  groupName: Scalars['String']['input'];
  responsibility: Scalars['String']['input'];
  role: StakeholderRole;
};


/** Main mutation object */
export type MutationUpdateGroupsPoliciesArgs = {
  daysUntilItBreaks?: InputMaybe<Scalars['Int']['input']>;
  groups: Array<Scalars['String']['input']>;
  maxAcceptanceDays?: InputMaybe<Scalars['Int']['input']>;
  maxAcceptanceSeverity?: InputMaybe<Scalars['Float']['input']>;
  maxNumberAcceptances?: InputMaybe<Scalars['Int']['input']>;
  minAcceptanceSeverity?: InputMaybe<Scalars['Float']['input']>;
  minBreakingSeverity?: InputMaybe<Scalars['Float']['input']>;
  organizationId: Scalars['String']['input'];
  vulnerabilityGracePeriod?: InputMaybe<Scalars['Int']['input']>;
};


/** Main mutation object */
export type MutationUpdateHookArgs = {
  groupName: Scalars['String']['input'];
  hook: HookInput;
  hookId: Scalars['String']['input'];
};


/** Main mutation object */
export type MutationUpdateIpRootArgs = {
  groupName: Scalars['String']['input'];
  nickname: Scalars['String']['input'];
  rootId: Scalars['ID']['input'];
};


/** Main mutation object */
export type MutationUpdateMailmapEntryArgs = {
  entry: MailmapEntryInput;
  entryEmail: Scalars['String']['input'];
  organizationId: Scalars['String']['input'];
};


/** Main mutation object */
export type MutationUpdateMailmapSubentryArgs = {
  entryEmail: Scalars['String']['input'];
  organizationId: Scalars['String']['input'];
  subentry: MailmapSubentryInput;
  subentryEmail: Scalars['String']['input'];
  subentryName: Scalars['String']['input'];
};


/** Main mutation object */
export type MutationUpdateNotificationsArgs = {
  notificationId: Scalars['String']['input'];
  s3FilePath?: InputMaybe<Scalars['String']['input']>;
  size?: InputMaybe<Scalars['Int']['input']>;
};


/** Main mutation object */
export type MutationUpdateNotificationsPreferencesArgs = {
  notificationsPreferences: NotificationPreferences;
};


/** Main mutation object */
export type MutationUpdateOrganizationPoliciesArgs = {
  daysUntilItBreaks?: InputMaybe<Scalars['Int']['input']>;
  inactivityPeriod?: InputMaybe<Scalars['Int']['input']>;
  maxAcceptanceDays?: InputMaybe<Scalars['Int']['input']>;
  maxAcceptanceSeverity?: InputMaybe<Scalars['Float']['input']>;
  maxNumberAcceptances?: InputMaybe<Scalars['Int']['input']>;
  minAcceptanceSeverity?: InputMaybe<Scalars['Float']['input']>;
  minBreakingSeverity?: InputMaybe<Scalars['Float']['input']>;
  organizationId: Scalars['String']['input'];
  organizationName: Scalars['String']['input'];
  vulnerabilityGracePeriod?: InputMaybe<Scalars['Int']['input']>;
};


/** Main mutation object */
export type MutationUpdateOrganizationPriorityPoliciesArgs = {
  organizationId: Scalars['String']['input'];
  priorityPolicies?: InputMaybe<Array<PriorityPolicyInput>>;
};


/** Main mutation object */
export type MutationUpdateOrganizationStakeholderArgs = {
  organizationId: Scalars['String']['input'];
  role: OrganizationRole;
  userEmail: Scalars['String']['input'];
};


/** Main mutation object */
export type MutationUpdateOtherPaymentMethodArgs = {
  businessName: Scalars['String']['input'];
  city: Scalars['String']['input'];
  country: Scalars['String']['input'];
  email: Scalars['String']['input'];
  organizationId: Scalars['String']['input'];
  paymentMethodId: Scalars['String']['input'];
  rut?: InputMaybe<Scalars['Upload']['input']>;
  state: Scalars['String']['input'];
  taxId?: InputMaybe<Scalars['Upload']['input']>;
};


/** Main mutation object */
export type MutationUpdateRootCloningStatusArgs = {
  commit?: InputMaybe<Scalars['String']['input']>;
  commitDate?: InputMaybe<Scalars['DateTime']['input']>;
  groupName: Scalars['String']['input'];
  id: Scalars['ID']['input'];
  message: Scalars['String']['input'];
  queueMachine?: InputMaybe<Scalars['Boolean']['input']>;
  status: CloningStatus;
};


/** Main mutation object */
export type MutationUpdateRootDockerImageArgs = {
  credentials?: InputMaybe<RootCredentialsInput>;
  groupName: Scalars['String']['input'];
  rootId: Scalars['ID']['input'];
  uri: Scalars['String']['input'];
};


/** Main mutation object */
export type MutationUpdateSecretArgs = {
  description?: InputMaybe<Scalars['String']['input']>;
  groupName: Scalars['String']['input'];
  key: Scalars['String']['input'];
  resourceId: Scalars['ID']['input'];
  resourceType: ResourceType;
  value: Scalars['String']['input'];
};


/** Main mutation object */
export type MutationUpdateSeverityArgs = {
  cvss4Vector: Scalars['String']['input'];
  cvssVector?: InputMaybe<Scalars['String']['input']>;
  findingId: Scalars['String']['input'];
};


/** Main mutation object */
export type MutationUpdateStakeholderArgs = {
  phone: PhoneInput;
};


/** Main mutation object */
export type MutationUpdateStakeholderPhoneArgs = {
  phone: PhoneInput;
  verificationCode: Scalars['String']['input'];
};


/** Main mutation object */
export type MutationUpdateToeInputArgs = {
  bePresent: Scalars['Boolean']['input'];
  component: Scalars['String']['input'];
  entryPoint: Scalars['String']['input'];
  groupName: Scalars['String']['input'];
  hasRecentAttack?: InputMaybe<Scalars['Boolean']['input']>;
  rootId: Scalars['String']['input'];
};


/** Main mutation object */
export type MutationUpdateToeLinesAttackedLinesArgs = {
  attackedLines?: InputMaybe<Scalars['Int']['input']>;
  comments?: InputMaybe<Scalars['String']['input']>;
  filename: Scalars['String']['input'];
  groupName: Scalars['String']['input'];
  rootId: Scalars['String']['input'];
};


/** Main mutation object */
export type MutationUpdateToeLinesLocArgs = {
  comments?: InputMaybe<Scalars['String']['input']>;
  filename: Scalars['String']['input'];
  groupName: Scalars['String']['input'];
  loc: Scalars['Int']['input'];
  rootId: Scalars['String']['input'];
};


/** Main mutation object */
export type MutationUpdateToeLinesSortsArgs = {
  filename?: InputMaybe<Scalars['String']['input']>;
  groupName?: InputMaybe<Scalars['String']['input']>;
  rootNickname?: InputMaybe<Scalars['String']['input']>;
  sortsRiskLevel?: InputMaybe<Scalars['Int']['input']>;
  sortsRiskLevelDate?: InputMaybe<Scalars['DateTime']['input']>;
  sortsSuggestions?: InputMaybe<Array<SortsSuggestionInput>>;
};


/** Main mutation object */
export type MutationUpdateToePortArgs = {
  address: Scalars['String']['input'];
  bePresent: Scalars['Boolean']['input'];
  groupName: Scalars['String']['input'];
  hasRecentAttack?: InputMaybe<Scalars['Boolean']['input']>;
  port: Scalars['Int']['input'];
  rootId: Scalars['String']['input'];
};


/** Main mutation object */
export type MutationUpdateToursArgs = {
  tours: ToursInput;
};


/** Main mutation object */
export type MutationUpdateUrlRootArgs = {
  excludedSubPaths?: InputMaybe<Array<Scalars['String']['input']>>;
  groupName: Scalars['String']['input'];
  nickname?: InputMaybe<Scalars['String']['input']>;
  rootId: Scalars['ID']['input'];
};


/** Main mutation object */
export type MutationUpdateVulnerabilitiesSeverityArgs = {
  cvss4Vector: Scalars['String']['input'];
  cvssVector?: InputMaybe<Scalars['String']['input']>;
  findingId: Scalars['ID']['input'];
  vulnerabilityIds: Array<Scalars['ID']['input']>;
};


/** Main mutation object */
export type MutationUpdateVulnerabilitiesTreatmentArgs = {
  acceptanceDate?: InputMaybe<Scalars['String']['input']>;
  assigned?: InputMaybe<Scalars['String']['input']>;
  findingId: Scalars['String']['input'];
  justification: Scalars['String']['input'];
  treatment: UpdateClientDescriptionTreatment;
  vulnerabilityId: Scalars['ID']['input'];
};


/** Main mutation object */
export type MutationUpdateVulnerabilityDescriptionArgs = {
  commit?: InputMaybe<Scalars['String']['input']>;
  source?: InputMaybe<VulnerabilitySource>;
  vulnerabilityId: Scalars['ID']['input'];
};


/** Main mutation object */
export type MutationUpdateVulnerabilityIssueArgs = {
  issueURL: Scalars['String']['input'];
  vulnerabilityId: Scalars['ID']['input'];
  webhookURL?: InputMaybe<Scalars['String']['input']>;
};


/** Main mutation object */
export type MutationUpdateVulnerabilityTreatmentArgs = {
  customSeverity?: InputMaybe<Scalars['Int']['input']>;
  externalBugTrackingSystem?: InputMaybe<Scalars['String']['input']>;
  findingId: Scalars['String']['input'];
  tag?: InputMaybe<Scalars['String']['input']>;
  vulnerabilityId: Scalars['ID']['input'];
};


/** Main mutation object */
export type MutationUploadEndpointsFileArgs = {
  file: Scalars['Upload']['input'];
  groupName: Scalars['String']['input'];
  rootId: Scalars['ID']['input'];
};


/** Main mutation object */
export type MutationUploadFileArgs = {
  file: Scalars['Upload']['input'];
  findingId: Scalars['String']['input'];
};


/** Main mutation object */
export type MutationUploadGitRootFileArgs = {
  file: Scalars['Upload']['input'];
  groupName: Scalars['String']['input'];
};


/** Main mutation object */
export type MutationValidateGitAccessArgs = {
  branch: Scalars['String']['input'];
  credentials: RootCredentialsInput;
  groupName?: InputMaybe<Scalars['String']['input']>;
  organizationId?: InputMaybe<Scalars['String']['input']>;
  url: Scalars['String']['input'];
};


/** Main mutation object */
export type MutationVerifyStakeholderArgs = {
  channel?: InputMaybe<VerificationChannel>;
  newPhone?: InputMaybe<PhoneInput>;
  verificationCode?: InputMaybe<Scalars['String']['input']>;
};


/** Main mutation object */
export type MutationVerifyVulnerabilitiesRequestArgs = {
  closedVulnerabilities: Array<InputMaybe<Scalars['String']['input']>>;
  findingId: Scalars['String']['input'];
  justification: Scalars['String']['input'];
  openVulnerabilities: Array<InputMaybe<Scalars['String']['input']>>;
  verificationReason?: InputMaybe<VulnerabilityStateReason>;
};

/** General Notification type definition */
export type Notification = ReportNotification;

/** Notifications parameters input definition */
export type NotificationParameters = {
  /** Minimum severity to send the vulnerability alert notification */
  minSeverity: Scalars['Float']['input'];
};

/** Notifications preferences input definition */
export type NotificationPreferences = {
  /** Email channel to notifications preferences */
  email: Array<NotificationsName>;
  /** Configuration parameters for sending notifications */
  parameters?: InputMaybe<NotificationParameters>;
  /** Sms channel to notifications preferences */
  sms?: InputMaybe<Array<InputMaybe<NotificationsName>>>;
};

/** notifications name enum definition */
export enum NotificationsName {
  /** When a user is granted access to a group */
  AccessGranted = 'ACCESS_GRANTED',
  /** When a DevSecOps agent token is generated or reset. */
  AgentToken = 'AGENT_TOKEN',
  /**
   * A single notification listing all the updates that occurred
   * the previous day on events reported across your groups
   */
  EventDigest = 'EVENT_DIGEST',
  /** When information about an event is reported in a group */
  EventReport = 'EVENT_REPORT',
  /** When a file is added or removed */
  FileUpdate = 'FILE_UPDATE',
  /** When information of groups changes or a group is deleted */
  GroupInformation = 'GROUP_INFORMATION',
  /** When a group report is ordered, allowing download within an hour */
  GroupReport = 'GROUP_REPORT',
  /** Biweekly notifications about our product updates */
  Newsletter = 'NEWSLETTER',
  /**
   * When a user submits a comment concerning a group,
   * a specific vulnerability or an event
   */
  NewComment = 'NEW_COMMENT',
  /** When a vulnerability draft is submitted or rejected */
  NewDraft = 'NEW_DRAFT',
  /** When a portfolio is created or removed */
  PortfolioUpdate = 'PORTFOLIO_UPDATE',
  /** When a new vulnerability is discovered, fixed, or removed */
  RemediateFinding = 'REMEDIATE_FINDING',
  /** When three weeks have passed since last platform use */
  ReminderNotification = 'REMINDER_NOTIFICATION',
  /**
   * When the status of root cloning, environment adds,
   * or deletes changes, or a root is activated, added,
   * updated or moved
   */
  RootUpdate = 'ROOT_UPDATE',
  /** When services are updated */
  ServiceUpdate = 'SERVICE_UPDATE',
  /** When a user unsubscribes from a group */
  UnsubscriptionAlert = 'UNSUBSCRIPTION_ALERT',
  /**
   * When a treatment is defined, requested, or approved
   * for a vulnerability
   */
  UpdatedTreatment = 'UPDATED_TREATMENT',
  /** When a user is assigned to work on a vulnerability */
  VulnerabilityAssigned = 'VULNERABILITY_ASSIGNED',
  /** When a vulnerability is reported or closed */
  VulnerabilityReport = 'VULNERABILITY_REPORT'
}

/** Parameters of notifications preferences type definition */
export type NotificationsParameters = {
  __typename?: 'NotificationsParameters';
  /** Minimum severity to trigger the Vulnerability alert notification */
  minSeverity: Scalars['Float']['output'];
};

/** Notifications preferences type definition */
export type NotificationsPreferences = {
  __typename?: 'NotificationsPreferences';
  /** Available notifications for the user */
  available: Array<NotificationsName>;
  /** Email channel notifications preferences */
  email: Array<NotificationsName>;
  /** Configuration parameters for sending notifications */
  parameters: Maybe<NotificationsParameters>;
  /** SMS channel notifications preferences */
  sms: Maybe<Array<Maybe<NotificationsName>>>;
};

/** Order values in sort */
export enum OrderSort {
  /** Ascending order for sorting items */
  Asc = 'ASC',
  /** Descending order for sorting items */
  Desc = 'DESC'
}

/** Organization type definition */
export type Organization = {
  __typename?: 'Organization';
  /** Analytics and statistics of the organization */
  analytics: Maybe<Scalars['GenericScalar']['output']>;
  /** External ID to set up cross-account roles in AWS */
  awsExternalId: Scalars['String']['output'];
  /** Billing information for the organization */
  billing: Maybe<OrganizationBilling>;
  /** Compliance with the standards in the organization */
  compliance: OrganizationCompliance;
  /** Country where the organization is based */
  country: Maybe<Scalars['String']['output']>;
  /** Number of authors in the repositories of the scope */
  coveredAuthors: Scalars['Int']['output'];
  /** Number of repositories covered in scope */
  coveredRepositories: Scalars['Int']['output'];
  /** Credential associated with the organization by ID */
  credential: Maybe<Credentials>;
  /** Credentials associated with the organization */
  credentials: Maybe<Array<Credentials>>;
  /** Number of days until vulnerabilities are considered technical debt and do not break the build */
  daysUntilItBreaks: Maybe<Scalars['Int']['output']>;
  /** Enacted and pending Finding policies within the organization */
  findingPolicies: Array<FindingPolicy>;
  /** Groups belonging to the organization */
  groups: Array<Group>;
  /**
   * Indicates wheter at least one of the groups belonging to the
   * organization has a root that uses ZTNA
   */
  hasZtnaRoots: Scalars['Boolean']['output'];
  /** Organization ID */
  id: Scalars['String']['output'];
  /**
   * Number of days to remove a stakeholder from the organization
   * due to inactivity
   */
  inactivityPeriod: Scalars['Int']['output'];
  /** Repositories integration within the organization */
  integrationRepositoriesConnection: Maybe<IntegrationRepositoriesConnection>;
  /** Maximum number of calendar days a Finding can be temporarily accepted */
  maxAcceptanceDays: Maybe<Scalars['Int']['output']>;
  /** Maximum CVSS 4.0 score in which a Finding can be temporarily accepted */
  maxAcceptanceSeverity: Scalars['Float']['output'];
  /** Maximum number of times a Finding can be temporarily accepted */
  maxNumberAcceptances: Maybe<Scalars['Int']['output']>;
  /** Minimum CVSS 4.0 score in which a Finding can be temporarily accepted */
  minAcceptanceSeverity: Scalars['Float']['output'];
  /**
   * Minimum CVSS 4.0 score of an open Vulnerability for DevSecOps to break
   * the build in strict mode
   */
  minBreakingSeverity: Maybe<Scalars['Float']['output']>;
  /** Number of authors of integration repositories outside */
  missedAuthors: Scalars['Int']['output'];
  /** Number of integration repositories outside */
  missedRepositories: Scalars['Int']['output'];
  /** Number of groups belonging to the organization */
  nGroups: Maybe<Scalars['Int']['output']>;
  /** Name of the organization */
  name: Scalars['String']['output'];
  /** Permissions of the current user within the organization */
  permissions: Maybe<Array<Maybe<Scalars['String']['output']>>>;
  /** List of custom priority policies */
  priorityPolicies: Maybe<Array<PriorityPolicy>>;
  /** Users who have access to the organization */
  stakeholders: Array<Stakeholder>;
  /** Free trial information */
  trial: Maybe<Trial>;
  /** Role of the current user within the organization */
  userRole: Scalars['String']['output'];
  /** Signed URL for organization Vulnerabilities CSV file */
  vulnerabilitiesUrl: Maybe<Scalars['String']['output']>;
  /**
   * Grace period in days where newly reported Vulnerabilities
   * won't break the build (DevSecOps only)
   */
  vulnerabilityGracePeriod: Scalars['Int']['output'];
  /** ZTNA HTTP logs for the organization */
  ztnaHttpLogs: Maybe<ZtnaHttpConnection>;
  /** ZTNA gateway network logs for the organization */
  ztnaNetworkLogs: Maybe<ZtnaNetworkConnection>;
  /** ZTNA session logs for the organization */
  ztnaSessionLogs: Maybe<ZtnaSessionConnection>;
};


/** Organization type definition */
export type OrganizationAnalyticsArgs = {
  documentName: Scalars['String']['input'];
  documentType: Scalars['String']['input'];
};


/** Organization type definition */
export type OrganizationBillingArgs = {
  date?: InputMaybe<Scalars['DateTime']['input']>;
};


/** Organization type definition */
export type OrganizationCredentialArgs = {
  id?: InputMaybe<Scalars['String']['input']>;
};


/** Organization type definition */
export type OrganizationIntegrationRepositoriesConnectionArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
};


/** Organization type definition */
export type OrganizationVulnerabilitiesUrlArgs = {
  verificationCode?: InputMaybe<Scalars['String']['input']>;
};


/** Organization type definition */
export type OrganizationZtnaHttpLogsArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  endDate?: InputMaybe<Scalars['DateTime']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  startDate: Scalars['DateTime']['input'];
};


/** Organization type definition */
export type OrganizationZtnaNetworkLogsArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  endDate?: InputMaybe<Scalars['DateTime']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  startDate: Scalars['DateTime']['input'];
};


/** Organization type definition */
export type OrganizationZtnaSessionLogsArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  endDate?: InputMaybe<Scalars['DateTime']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  startDate: Scalars['DateTime']['input'];
};

/** OrganizationBilling type definition */
export type OrganizationBilling = {
  __typename?: 'OrganizationBilling';
  /** Authors data */
  authors: Array<OrganizationBillingAuthor>;
  /** Authors costs for Advanced subscriptions */
  costsAuthors: Scalars['Int']['output'];
  /** Base costs for priced subscriptions */
  costsBase: Scalars['Int']['output'];
  /** Total costs for priced subscriptions */
  costsTotal: Scalars['Int']['output'];
  /** Number of authors for Advanced subscriptions */
  numberAuthorsAdvanced: Scalars['Int']['output'];
  /** Number of authors for Essential subscriptions */
  numberAuthorsEssential: Scalars['Int']['output'];
  /** Number of authors for all subscriptions */
  numberAuthorsTotal: Scalars['Int']['output'];
  /** Number of groups with Advanced subscription */
  numberGroupsAdvanced: Scalars['Int']['output'];
  /** Number of groups with Essential subscription */
  numberGroupsEssential: Scalars['Int']['output'];
  /** Total number of groups */
  numberGroupsTotal: Scalars['Int']['output'];
  /** Payment methods for the organization */
  paymentMethods: Array<Maybe<PaymentMethod>>;
};

/** OrganizationBillingActiveGroup type definition */
export type OrganizationBillingActiveGroup = {
  __typename?: 'OrganizationBillingActiveGroup';
  /** Name of the group */
  name: Scalars['String']['output'];
  /** Tier of the group (Free, One-Shot, Other, Machine, or Squad) */
  tier: TierType;
};

/** OrganizationBillingAuthor type definition */
export type OrganizationBillingAuthor = {
  __typename?: 'OrganizationBillingAuthor';
  /** List of groups the author has contributed to */
  activeGroups: Array<OrganizationBillingActiveGroup>;
  /** Author who has contributed to at least one group in the organization */
  actor: Scalars['String']['output'];
};

/** OrganizationCompliance type definition */
export type OrganizationCompliance = {
  __typename?: 'OrganizationCompliance';
  /** Average level of compliance with standards across the organization */
  complianceLevel: Maybe<Scalars['Float']['output']>;
  /**
   * Weekly trend of compliance for the organization, indicating wheter
   * compliance is increasing, decreasing, or remaining neutral
   */
  complianceWeeklyTrend: Maybe<Scalars['Float']['output']>;
  /**
   * Estimated time (in days) for the organization to achieve full
   * compliance with all standards
   */
  estimatedDaysToFullCompliance: Maybe<Scalars['Float']['output']>;
  /** Compliance information for each standard at Organization level */
  standards: Array<OrganizationComplianceStandard>;
};

/** OrganizationCompliance type definition */
export type OrganizationComplianceStandard = {
  __typename?: 'OrganizationComplianceStandard';
  /** Average level of compliance with the standard in all organizations */
  avgOrganizationComplianceLevel: Scalars['Float']['output'];
  /** Best level of compliance with the standard in all organizations */
  bestOrganizationComplianceLevel: Scalars['Float']['output'];
  /** Level of compliance with the standard */
  complianceLevel: Scalars['Float']['output'];
  /** ID of the standard */
  standardId: Scalars['String']['output'];
  /** Title of the standard */
  standardTitle: Scalars['String']['output'];
  /** Worst level of compliance with the standard in all organizations */
  worstOrganizationComplianceLevel: Scalars['Float']['output'];
};

/** Approval status of an organization finding policy */
export enum OrganizationFindingPolicy {
  /** Policy was approved and enacted */
  Approved = 'APPROVED',
  /** Policy was rejected and discarded */
  Rejected = 'REJECTED'
}

/** Status of an organization finding policy */
export enum OrganizationFindingPolicyStatus {
  /** Approved */
  Approved = 'APPROVED',
  /** Inactive */
  Inactive = 'INACTIVE',
  /** Rejected */
  Rejected = 'REJECTED',
  /** Submitted */
  Submitted = 'SUBMITTED'
}

/** IntegrationRepositories type definition */
export type OrganizationIntegrationRepositories = {
  __typename?: 'OrganizationIntegrationRepositories';
  /** List of repository branches */
  branches: Array<Maybe<Scalars['String']['output']>>;
  /** Name of the default repository branch */
  defaultBranch: Scalars['String']['output'];
  /** Date of the last commit */
  lastCommitDate: Maybe<Scalars['String']['output']>;
  /** Name of the repository */
  name: Scalars['String']['output'];
  /** Repository URL */
  url: Scalars['String']['output'];
};

/** Organization stakeholder role enum definition */
export enum OrganizationRole {
  /** Customer Manager */
  CustomerManager = 'CUSTOMER_MANAGER',
  /** Organization Manager */
  OrganizationManager = 'ORGANIZATION_MANAGER',
  /** Resourcer */
  Resourcer = 'RESOURCER',
  /** User */
  User = 'USER'
}

/** Information about pagination in a connection */
export type PageInfo = {
  __typename?: 'PageInfo';
  /** When paginating forwards, the cursor to continue */
  endCursor: Scalars['String']['output'];
  /** When paginating forwards, are there more items? */
  hasNextPage: Scalars['Boolean']['output'];
};

/** Generic Payload interface definition */
export type Payload = {
  /** Did the query succeed? */
  success: Scalars['Boolean']['output'];
};

/** PaymentMethod type definition */
export type PaymentMethod = {
  __typename?: 'PaymentMethod';
  /** Mastercard, Visa, or any other brand */
  brand: Scalars['String']['output'];
  /** Name of thew business related to this payment method */
  businessName: Scalars['String']['output'];
  /** City where the business is located */
  city: Scalars['String']['output'];
  /** Country where the business is located */
  country: Scalars['String']['output'];
  /** True if the payment method is default for the customer */
  default: Scalars['Boolean']['output'];
  /** Email for e-invoice */
  email: Scalars['String']['output'];
  /** Expiration month */
  expirationMonth: Scalars['String']['output'];
  /** Expiration year */
  expirationYear: Scalars['String']['output'];
  /** Payment method Stripe ID */
  id: Scalars['ID']['output'];
  /** Last four digits */
  lastFourDigits: Scalars['String']['output'];
  /** RUT document file metadata */
  rut: Maybe<DocumentFile>;
  /** State where the business is located */
  state: Scalars['String']['output'];
  /** Tax ID document file metadata */
  taxId: Maybe<DocumentFile>;
};

/** Phone type definition */
export type Phone = {
  __typename?: 'Phone';
  /** International dialing prefix */
  callingCountryCode: Scalars['String']['output'];
  /** International iso country code */
  countryCode: Scalars['String']['output'];
  /** Phone number without the calling country code */
  nationalNumber: Scalars['String']['output'];
};

/** Phone definition */
export type PhoneInput = {
  /** International dialing prefix */
  callingCountryCode: Scalars['String']['input'];
  /** Phone number without the calling country code */
  nationalNumber: Scalars['String']['input'];
};

/** Price type definition */
export type Price = {
  __typename?: 'Price';
  /** Amount of money the price costs */
  amount: Scalars['Int']['output'];
  /** Price currency (COP, EUR, USD, etc.) */
  currency: Scalars['String']['output'];
};

/** Prices type definition */
export type Prices = {
  __typename?: 'Prices';
  /** Advanced price */
  advanced: Maybe<Price>;
  /** Essential price */
  essential: Maybe<Price>;
};

/** Priority policy definition */
export type PriorityPolicy = {
  __typename?: 'PriorityPolicy';
  /** Policy name used to set priority policy */
  policy: Scalars['String']['output'];
  /** Custom priority value for associated policy */
  value: Scalars['Int']['output'];
};

/** Priority policy definition */
export type PriorityPolicyInput = {
  /** Policy name used to set priority policy */
  policy: Scalars['String']['input'];
  /** Custom priority value for associated policy */
  value: Scalars['Int']['input'];
};

/** Main Query object */
export type Query = {
  __typename?: 'Query';
  /** Billing entity */
  billing: Maybe<Billing>;
  /** Vulnerabilities criteria connection */
  criteriaConnection: CriteriaConnection;
  /** Requirements criteria connection */
  criteriaRequirementsConnection: CriteriaRequirementsConnection;
  /** Group CSV report generated in the platform */
  csvReport: Maybe<Report>;
  /** Environments based on the associated Git Roots */
  environmentUrl: Maybe<GitEnvironmentUrl>;
  /** Identifier that helps us to classify an event individually */
  event: Event;
  /** List of events from a specific group */
  events: Maybe<Array<Maybe<Event>>>;
  /** Type of a vulnerability */
  finding: Finding;
  /**
   * Get a specific execution of Forces where you specify
   * that one according to the ID
   */
  forcesExecution: Maybe<ForcesExecution>;
  /** Single project to manage their Vulnerabilities separately */
  group: Group;
  /** List groups a user is subscribed to */
  listUserGroups: Maybe<Array<Maybe<Group>>>;
  /** Get list of mailmap entry objects */
  mailmapEntries: MailmapEntriesConnection;
  /** Get list of mailmap entry with subentries objects */
  mailmapEntriesWithSubentries: Array<MailmapEntryWithSubentries>;
  /** Get mailmap entry object */
  mailmapEntry: Maybe<MailmapEntry>;
  /** Get list of subentries objects of a mailmap entry */
  mailmapEntrySubentries: Maybe<Array<MailmapSubentry>>;
  /** Get mailmap entry with subentries object */
  mailmapEntryWithSubentries: Maybe<MailmapEntryWithSubentries>;
  /** Get list of mailmap records */
  mailmapRecords: Array<MailmapRecord>;
  /** Get mailmap subentry object */
  mailmapSubentry: Maybe<MailmapSubentry>;
  /** General user information */
  me: Me;
  /**
   * Refers to the different macro projects, each
   * containing multiple subprojects (Groups)
   */
  organization: Organization;
  /** Get complete organization data from its name */
  organizationId: Organization;
  /** Reports generated in the ARM (Executive & Technical) */
  report: Maybe<Report>;
  /** Get resource file and the metadata from group */
  resources: Resource;
  /** Get a root, can be Git, IP, or URL */
  root: Root;
  /** Get a value of th specified secret */
  secret: Maybe<Secret>;
  /** Exported csv file with the Forces executions group */
  sendExportedFile: Maybe<SendExportedFile>;
  /** Get user info from group */
  stakeholder: Maybe<Stakeholder>;
  /** Create a suggested fix for a vulnerable function */
  suggestedFix: SimplePayloadMessage;
  /** How to identify the Vulnerability according to the customer */
  tag: Maybe<Tag>;
  /**
   * ToE lines report generated in the ARM
   * @deprecated This query is deprecated and will be removed after 2025/07/08
   * Use verifyEnvironmentIntegrity instead.
   */
  toeLinesReport: Maybe<Report>;
  /** Get the url for the unfulfilled standard report */
  unfulfilledStandardReportUrl: Scalars['String']['output'];
  /** Check if environment is reachable */
  verifyEnvironmentIntegrity: Scalars['Boolean']['output'];
  /**
   * Check if URL gets status 200
   * @deprecated This query is deprecated and will be removed after 2025/05/20
   * Use verifyEnvironmentIntegrity instead.
   */
  verifyUrlStatus: Scalars['Boolean']['output'];
  /** Gets Vulnerabilities with pending severity update requests */
  vulnerabilitiesSeverityRequests: Maybe<Array<Maybe<Vulnerability>>>;
  /** Gets Vulnerabilities pending to reattack */
  vulnerabilitiesToReattack: Maybe<Array<Maybe<Vulnerability>>>;
  /** The specific location where the Vulnerability is found */
  vulnerability: Vulnerability;
  /** Get the URL for the ZTNA logs report */
  ztnaLogsReportUrl: Scalars['String']['output'];
};


/** Main Query object */
export type QueryCriteriaConnectionArgs = {
  after: Scalars['String']['input'];
  first?: InputMaybe<Scalars['Int']['input']>;
};


/** Main Query object */
export type QueryCriteriaRequirementsConnectionArgs = {
  after: Scalars['String']['input'];
  first?: InputMaybe<Scalars['Int']['input']>;
};


/** Main Query object */
export type QueryCsvReportArgs = {
  groupName: Scalars['String']['input'];
  reportType: ReportType;
  verificationCode: Scalars['String']['input'];
};


/** Main Query object */
export type QueryEnvironmentUrlArgs = {
  groupName: Scalars['String']['input'];
  urlId: Scalars['String']['input'];
};


/** Main Query object */
export type QueryEventArgs = {
  groupName: Scalars['String']['input'];
  identifier: Scalars['String']['input'];
};


/** Main Query object */
export type QueryEventsArgs = {
  groupName?: InputMaybe<Scalars['String']['input']>;
};


/** Main Query object */
export type QueryFindingArgs = {
  identifier: Scalars['String']['input'];
};


/** Main Query object */
export type QueryForcesExecutionArgs = {
  executionId: Scalars['String']['input'];
  groupName?: InputMaybe<Scalars['String']['input']>;
};


/** Main Query object */
export type QueryGroupArgs = {
  groupName: Scalars['String']['input'];
};


/** Main Query object */
export type QueryListUserGroupsArgs = {
  userEmail: Scalars['String']['input'];
};


/** Main Query object */
export type QueryMailmapEntriesArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  first: Scalars['Int']['input'];
  organizationId: Scalars['String']['input'];
};


/** Main Query object */
export type QueryMailmapEntriesWithSubentriesArgs = {
  organizationId: Scalars['String']['input'];
};


/** Main Query object */
export type QueryMailmapEntryArgs = {
  entryEmail: Scalars['String']['input'];
  organizationId: Scalars['String']['input'];
};


/** Main Query object */
export type QueryMailmapEntrySubentriesArgs = {
  entryEmail: Scalars['String']['input'];
  organizationId: Scalars['String']['input'];
};


/** Main Query object */
export type QueryMailmapEntryWithSubentriesArgs = {
  entryEmail: Scalars['String']['input'];
  organizationId: Scalars['String']['input'];
};


/** Main Query object */
export type QueryMailmapRecordsArgs = {
  organizationId: Scalars['String']['input'];
};


/** Main Query object */
export type QueryMailmapSubentryArgs = {
  entryEmail: Scalars['String']['input'];
  organizationId: Scalars['String']['input'];
  subentryEmail: Scalars['String']['input'];
  subentryName: Scalars['String']['input'];
};


/** Main Query object */
export type QueryOrganizationArgs = {
  organizationId: Scalars['String']['input'];
};


/** Main Query object */
export type QueryOrganizationIdArgs = {
  organizationName: Scalars['String']['input'];
};


/** Main Query object */
export type QueryReportArgs = {
  age?: InputMaybe<Scalars['Int']['input']>;
  closingDate?: InputMaybe<Scalars['DateTime']['input']>;
  findingTitle?: InputMaybe<Scalars['String']['input']>;
  groupName?: InputMaybe<Scalars['String']['input']>;
  lang?: InputMaybe<ReportLang>;
  lastReport?: InputMaybe<Scalars['Int']['input']>;
  location?: InputMaybe<Scalars['String']['input']>;
  maxReleaseDate?: InputMaybe<Scalars['DateTime']['input']>;
  maxSeverity?: InputMaybe<Scalars['Float']['input']>;
  minReleaseDate?: InputMaybe<Scalars['DateTime']['input']>;
  minSeverity?: InputMaybe<Scalars['Float']['input']>;
  reportType: ReportType;
  startClosingDate?: InputMaybe<Scalars['DateTime']['input']>;
  states?: InputMaybe<Array<VulnerabilityState>>;
  treatments?: InputMaybe<Array<VulnerabilityTreatment>>;
  verificationCode: Scalars['String']['input'];
  verifications?: InputMaybe<Array<VulnerabilityVerification>>;
};


/** Main Query object */
export type QueryResourcesArgs = {
  groupName?: InputMaybe<Scalars['String']['input']>;
};


/** Main Query object */
export type QueryRootArgs = {
  groupName: Scalars['String']['input'];
  rootId: Scalars['ID']['input'];
};


/** Main Query object */
export type QuerySecretArgs = {
  groupName: Scalars['String']['input'];
  resourceId: Scalars['ID']['input'];
  resourceType: ResourceType;
  secretKey: Scalars['String']['input'];
};


/** Main Query object */
export type QuerySendExportedFileArgs = {
  groupName: Scalars['String']['input'];
};


/** Main Query object */
export type QueryStakeholderArgs = {
  entity: StakeholderEntity;
  groupName?: InputMaybe<Scalars['String']['input']>;
  organizationId?: InputMaybe<Scalars['String']['input']>;
  userEmail: Scalars['String']['input'];
};


/** Main Query object */
export type QuerySuggestedFixArgs = {
  vulnerabilityId: Scalars['String']['input'];
  vulnerableCodeImports: Scalars['String']['input'];
  vulnerableFunction: Scalars['String']['input'];
  vulnerableLineContent: Scalars['String']['input'];
};


/** Main Query object */
export type QueryTagArgs = {
  organizationId?: InputMaybe<Scalars['String']['input']>;
  tag: Scalars['String']['input'];
};


/** Main Query object */
export type QueryToeLinesReportArgs = {
  groupName: Scalars['String']['input'];
  verificationCode: Scalars['String']['input'];
};


/** Main Query object */
export type QueryUnfulfilledStandardReportUrlArgs = {
  groupName: Scalars['String']['input'];
  reportType: ReportType;
  unfulfilledStandards?: InputMaybe<Array<Scalars['String']['input']>>;
  verificationCode: Scalars['String']['input'];
};


/** Main Query object */
export type QueryVerifyEnvironmentIntegrityArgs = {
  azureClientId?: InputMaybe<Scalars['String']['input']>;
  azureClientSecret?: InputMaybe<Scalars['String']['input']>;
  azureTenantId?: InputMaybe<Scalars['String']['input']>;
  cloudName?: InputMaybe<Scalars['String']['input']>;
  gcpPrivateKey?: InputMaybe<Scalars['String']['input']>;
  groupName: Scalars['String']['input'];
  url: Scalars['String']['input'];
  urlType: Scalars['String']['input'];
};


/** Main Query object */
export type QueryVerifyUrlStatusArgs = {
  url: Scalars['String']['input'];
};


/** Main Query object */
export type QueryVulnerabilitiesSeverityRequestsArgs = {
  group?: InputMaybe<Scalars['String']['input']>;
};


/** Main Query object */
export type QueryVulnerabilitiesToReattackArgs = {
  group?: InputMaybe<Scalars['String']['input']>;
};


/** Main Query object */
export type QueryVulnerabilityArgs = {
  uuid: Scalars['String']['input'];
};


/** Main Query object */
export type QueryZtnaLogsReportUrlArgs = {
  date?: InputMaybe<Scalars['DateTime']['input']>;
  logType: ZtnaLogType;
  organizationId: Scalars['String']['input'];
  verificationCode: Scalars['String']['input'];
};

/** Reasons to remove a Finding */
export enum RemoveFindingJustification {
  /** Finding is a duplicate of another */
  Duplicated = 'DUPLICATED',
  /** Finding is a false positive */
  FalsePositive = 'FALSE_POSITIVE',
  /** Finding is not required */
  NotRequired = 'NOT_REQUIRED'
}

/** Reasons allowed for removing a group enum definition */
export enum RemoveGroupReason {
  /** Different security testing strategy */
  DiffSectst = 'DIFF_SECTST',
  /** Information will be moved to a different group */
  Migration = 'MIGRATION',
  /** Created by mistake */
  Mistake = 'MISTAKE',
  /** No more security testing */
  NoSectst = 'NO_SECTST',
  /** System will be deprecated */
  NoSystem = 'NO_SYSTEM',
  /** Other reason not mentioned above */
  Other = 'OTHER',
  /** End of Proof of Concept period */
  PocOver = 'POC_OVER',
  /** Group rename */
  Rename = 'RENAME',
  /** Trial expiration */
  TrialFinalization = 'TRIAL_FINALIZATION',
  /** Testing request cancelled */
  TrCancelled = 'TR_CANCELLED'
}

/** Grant stakeholder access Payload type definition */
export type RemoveStakeholderAccessPayload = Payload & {
  __typename?: 'RemoveStakeholderAccessPayload';
  /** Email address of the former Stakeholder */
  removedEmail: Maybe<Scalars['String']['output']>;
  /** Did the query succeed? */
  success: Scalars['Boolean']['output'];
};

/** Reasons for removing a Vulnerability */
export enum RemoveVulnerabilityJustification {
  /** Vulnerability is a duplicate of another */
  Duplicated = 'DUPLICATED',
  /** Vulnerability is a false positive */
  FalsePositive = 'FALSE_POSITIVE',
  /** Vulnerability was mistakenly reported */
  ReportingError = 'REPORTING_ERROR'
}

/** Report type definition */
export type Report = {
  __typename?: 'Report';
  /** Indicates that the report was successfully requested */
  success: Scalars['Boolean']['output'];
};

/** Language for Group Reports enum definition */
export enum ReportLang {
  /** English */
  En = 'EN'
}

/** Report notification type definition */
export type ReportNotification = INotification & {
  __typename?: 'ReportNotification';
  /** URL to download the report when ready */
  downloadURL: Maybe<Scalars['String']['output']>;
  /**
   * Time at which this notification expires.
   * Expired notifications are removed automatically but it may not happen immediately
   */
  expirationTime: Maybe<Scalars['DateTime']['output']>;
  /** Report file format */
  format: ReportType;
  /** ID of the notification */
  id: Scalars['ID']['output'];
  /** Report name */
  name: Scalars['String']['output'];
  /** Time at which this notification was issued */
  notifiedAt: Scalars['DateTime']['output'];
  /** S3 file path for the report file */
  s3FilePath: Maybe<Scalars['String']['output']>;
  /** Size in bytes of the file when ready */
  size: Maybe<Scalars['Int']['output']>;
  /** Report status */
  status: ReportStatus;
};

/** Report Status enum definition */
export enum ReportStatus {
  /** The report was removed by the user */
  Deleted = 'DELETED',
  /** The report is being prepared for processing */
  Preparing = 'PREPARING',
  /** The report is being processed */
  Processing = 'PROCESSING',
  /** The report is ready for download */
  Ready = 'READY'
}

/** Group Reports type enum definition */
export enum ReportType {
  /** PDF (Security Testing Certificate) */
  Cert = 'CERT',
  /** Comma-separated values */
  Csv = 'CSV',
  /** ZIP file (Findings data) */
  Data = 'DATA',
  /** PDF (Executive) */
  Pdf = 'PDF',
  /** ToE inputs */
  ToeInputs = 'TOE_INPUTS',
  /** ToE lines */
  ToeLines = 'TOE_LINES',
  /** Excel Spreadsheet (Technical) */
  Xls = 'XLS'
}

/** Requirement type definition */
export type Requirement = {
  __typename?: 'Requirement';
  /** ID of the requirement */
  id: Scalars['ID']['output'];
  /** Summary of the requirement */
  summary: Scalars['String']['output'];
  /** Title of the requirement */
  title: Scalars['String']['output'];
};

/** Resource type definition */
export type Resource = {
  __typename?: 'Resource';
  /**
   * Files that have essential information in the performing
   * penetration test are useful and necessary
   */
  files: Maybe<Array<GroupFile>>;
  /** Name of the Group containing the files */
  groupName: Scalars['String']['output'];
};

/** Root state enum definition */
export enum ResourceState {
  /** The root is being tested */
  Active = 'ACTIVE',
  /** The root is no longer being tested */
  Inactive = 'INACTIVE'
}

/** Resource where secrets come from */
export enum ResourceType {
  /** Resource type of git root secret */
  Root = 'ROOT',
  /** Resource type of url environment secret */
  Url = 'URL'
}

/** General Root type definition */
export type Root = GitRoot | IpRoot | UrlRoot;

/** Root credentials definition */
export type RootCredentialsInput = {
  /** ARN of the AWS role */
  arn?: InputMaybe<Scalars['String']['input']>;
  /** Name of the (azure) organization to which a PAT associates the credential */
  azureOrganization?: InputMaybe<Scalars['String']['input']>;
  /** ID of an existing credential */
  id?: InputMaybe<Scalars['String']['input']>;
  /** Whether credential is PAT (Personal Access Token) integration */
  isPat?: InputMaybe<Scalars['Boolean']['input']>;
  /** Returns the key of the credential type SSH protocol */
  key?: InputMaybe<Scalars['String']['input']>;
  /** Name of the credential */
  name?: InputMaybe<Scalars['String']['input']>;
  /** Returns the key of the password of the credential type HTTPS protocol */
  password?: InputMaybe<Scalars['String']['input']>;
  /** HTTPS token, which allows identification and login confirmation */
  token?: InputMaybe<Scalars['String']['input']>;
  /** Type of authentication */
  type?: InputMaybe<CredentialType>;
  /** User ID */
  user?: InputMaybe<Scalars['String']['input']>;
};

/**
 * Indicates how crucial this root is for your organization
 * and helps us calculate the impact of reported vulnerabilities.
 */
export enum RootCriticality {
  /** Critical impact to the organization if the root is compromised */
  Critical = 'CRITICAL',
  /** High impact to the organization if the root is compromised */
  High = 'HIGH',
  /** Low impact to the organization if the root is compromised */
  Low = 'LOW',
  /** Medium impact to the organization if the root is compromised */
  Medium = 'MEDIUM'
}

/** Reasons to deactivate a Root enum definition */
export enum RootDeactivationReason {
  /** Other reason not covered */
  Other = 'OTHER',
  /** The Root is out of the scope for this group */
  OutOfScope = 'OUT_OF_SCOPE',
  /** The Root was registered by mistake */
  RegisteredByMistake = 'REGISTERED_BY_MISTAKE'
}

/** Docker image data definition */
export type RootDockerImage = {
  __typename?: 'RootDockerImage';
  /** The day when the Image was added */
  createdAt: Maybe<Scalars['DateTime']['output']>;
  /** User who added Docker Image */
  createdBy: Maybe<Scalars['String']['output']>;
  /** Credential used to Image access */
  credentials: Maybe<Credentials>;
  /** Steps to build the Image */
  history: Array<Maybe<DockerImageHistory>>;
  /** If the Image must be included in the scope */
  include: Scalars['Boolean']['output'];
  /** Layers digests */
  layers: Array<Maybe<Scalars['String']['output']>>;
  /** Git Root */
  root: GitRoot;
  /** ID of the root */
  rootId: Scalars['ID']['output'];
  /** Packages related with the image */
  toePackages: Array<Maybe<ToePackage>>;
  /** Dependencies present in the image */
  toePackagesConnection: ToePackagesConnection;
  /** Docker image URI */
  uri: Scalars['String']['output'];
};


/** Docker image data definition */
export type RootDockerImageToePackagesConnectionArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  outdated?: InputMaybe<Scalars['Boolean']['input']>;
  platform?: InputMaybe<Scalars['String']['input']>;
  platforms?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  search?: InputMaybe<Scalars['String']['input']>;
  vulnerable?: InputMaybe<Scalars['Boolean']['input']>;
};

/** Secret type definition */
export type Secret = {
  __typename?: 'Secret';
  /** Secret creation date */
  createdAt: Scalars['DateTime']['output'];
  /** Description of secret */
  description: Maybe<Scalars['String']['output']>;
  /** Secret key */
  key: Scalars['String']['output'];
  /** Secret owner */
  owner: Scalars['String']['output'];
  /** Secret value */
  value: Scalars['String']['output'];
};

/** SendExportedFile type definition */
export type SendExportedFile = {
  __typename?: 'SendExportedFile';
  /** Indicates that the report was successfully requested */
  success: Scalars['Boolean']['output'];
};

/** Testing service type enum definition */
export enum ServiceType {
  /** Black-box testing */
  Black = 'BLACK',
  /** White-box testing */
  White = 'WHITE'
}

/** Qualitative severity rating */
export enum SeverityRating {
  /** Critical impact */
  Critical = 'CRITICAL',
  /** High impact */
  High = 'HIGH',
  /** Low impact */
  Low = 'LOW',
  /** Medium impact */
  Medium = 'MEDIUM',
  /** No impact */
  None = 'NONE'
}

/** Sign post URLs Payload Payload type definition */
export type SignPostUrlsPayload = Payload & {
  __typename?: 'SignPostUrlsPayload';
  /** Did the query succeed? */
  success: Scalars['Boolean']['output'];
  /** Metadata of the newly uploaded file */
  url: SignedUrlObject;
};

/**
 * Signed fields object Payload type definition, for more information about
 * this type feel free to check the relevant [AWS docs](https://docs.aws.amazon.com/AmazonCloudFront/latest/DeveloperGuide/private-content-signed-urls.html)
 */
export type SignedFieldsObject = {
  __typename?: 'SignedFieldsObject';
  /** Encryption Algorithm */
  algorithm: Scalars['String']['output'];
  /** Assigned Credential */
  credential: Scalars['String']['output'];
  /** Signature creation Date */
  date: Scalars['String']['output'];
  /** Key */
  key: Scalars['String']['output'];
  /** Access control policy */
  policy: Scalars['String']['output'];
  /** Signature Security Token */
  securitytoken: Scalars['String']['output'];
  /** Signature */
  signature: Scalars['String']['output'];
};

/** Signed URL object Payload type definition */
export type SignedUrlObject = {
  __typename?: 'SignedUrlObject';
  /** Signed URL metadata */
  fields: SignedFieldsObject;
  /** Signed URL */
  url: Scalars['String']['output'];
};

/** Simple finding Payload type definition */
export type SimpleFindingPayload = Payload & {
  __typename?: 'SimpleFindingPayload';
  /** Updated Finding */
  finding: Maybe<Finding>;
  /** Did the query succeed? */
  success: Scalars['Boolean']['output'];
};

/** Approve draft Payload type definition */
export type SimpleGroupPayload = Payload & {
  __typename?: 'SimpleGroupPayload';
  /** Updated Group */
  group: Maybe<Group>;
  /** Did the query succeed? */
  success: Scalars['Boolean']['output'];
};

/** Simple Payload type definition */
export type SimplePayload = Payload & {
  __typename?: 'SimplePayload';
  /** Did the query succeed? */
  success: Scalars['Boolean']['output'];
};

/** Simple Payload with message type definition */
export type SimplePayloadMessage = Payload & {
  __typename?: 'SimplePayloadMessage';
  /** Message result */
  message: Maybe<Scalars['String']['output']>;
  /** Did the query succeed? */
  success: Scalars['Boolean']['output'];
};

/** Stakeholder type definition */
export type Snippet = {
  __typename?: 'Snippet';
  /** An integer indicating the column where the vulnerable code line is located */
  column: Maybe<Scalars['Int']['output']>;
  /** Total number of columns occupied by each line of code in the snippet */
  columnsPerLine: Scalars['Int']['output'];
  /** The code content of the snippet */
  content: Scalars['String']['output'];
  /**
   * True if the snippet is formatted for better visibility and clarity,
   * False if it represents the raw code portion
   */
  highlightLineNumber: Scalars['Boolean']['output'];
  /** The line number where the Vulnerability occurs */
  line: Scalars['Int']['output'];
  /**
   * Number of lines before and after the focus line
   * (the line where the Vulnerability is present)
   */
  lineContext: Scalars['Int']['output'];
  /** Total number of lines above the snippet */
  offset: Scalars['Int']['output'];
  /** Indicates wheter line numbers are displayed in the snippet */
  showLineNumbers: Scalars['Boolean']['output'];
};

/** Reasons to close an event enum definition */
export enum SolveEventReason {
  /** When access is authorized to a resource */
  AccessGranted = 'ACCESS_GRANTED',
  /** When a resource is no longer within the scope of assessment */
  AffectedResourceRemovedFromScope = 'AFFECTED_RESOURCE_REMOVED_FROM_SCOPE',
  /** When a successful cloning operation occurs */
  ClonedSuccessfully = 'CLONED_SUCCESSFULLY',
  /** When previously problematic credentials are now functional */
  CredentialsAreWorkingNow = 'CREDENTIALS_ARE_WORKING_NOW',
  /** When data associated with the event is updated */
  DataUpdated = 'DATA_UPDATED',
  /** When the environment related to the event is functional */
  EnvironmentIsWorkingNow = 'ENVIRONMENT_IS_WORKING_NOW',
  /** When the installer related to the event is functional */
  InstallerIsWorkingNow = 'INSTALLER_IS_WORKING_NOW',
  /** When it is safe to resume operations after an event */
  IsOkToResume = 'IS_OK_TO_RESUME',
  /** When the event is transferred to a different group */
  MovedToAnotherGroup = 'MOVED_TO_ANOTHER_GROUP',
  /** When new credentials are supplied */
  NewCredentialsProvided = 'NEW_CREDENTIALS_PROVIDED',
  /** When a new environment is provided */
  NewEnvironmentProvided = 'NEW_ENVIRONMENT_PROVIDED',
  /** For any other reason not specified */
  Other = 'OTHER',
  /** When permission is denied for an action */
  PermissionDenied = 'PERMISSION_DENIED',
  /** When permission is granted for an action */
  PermissionGranted = 'PERMISSION_GRANTED',
  /** When the problem associated with the event is resolved */
  ProblemSolved = 'PROBLEM_SOLVED',
  /** When necessary supplies are provided */
  SuppliesWereGiven = 'SUPPLIES_WERE_GIVEN',
  /** When a change in the Target of Evaluation is approved */
  ToeChangeApproved = 'TOE_CHANGE_APPROVED',
  /** When the Target of Evaluation will remain the same */
  ToeWillRemainUnchanged = 'TOE_WILL_REMAIN_UNCHANGED'
}

/** Use of Sorts enum definition */
export enum Sorts {
  /** Sorts wasn't involved */
  No = 'NO',
  /** Sorts was involved */
  Yes = 'YES'
}

/** Sorts vulnerability suggestion */
export type SortsSuggestionInput = {
  /** Finding title */
  findingTitle: Scalars['String']['input'];
  /** Probability as a percentage */
  probability: Scalars['Int']['input'];
};

/** Stakeholder type definition */
export type Stakeholder = {
  __typename?: 'Stakeholder';
  /** The email address of the user who is part of the organization/group in ARM */
  email: Maybe<Scalars['String']['output']>;
  /** Datetime of the first login to ARM */
  firstLogin: Maybe<Scalars['String']['output']>;
  /** Current invitation state which can be: Pending, Registered or Unregistered */
  invitationState: Maybe<InvitationState>;
  /** Datetime of the latest login to ARM */
  lastLogin: Maybe<Scalars['String']['output']>;
  /**
   * The stakeholder responsibility or position in the organization/group
   * (actions executed in the project)
   */
  responsibility: Maybe<Scalars['String']['output']>;
  /**
   * The stakeholder role in the organization/group could be:
   * User, User Manager or Vulnerability Manager
   */
  role: Maybe<Scalars['String']['output']>;
};

/** Stakeholder entity scope enum definition */
export enum StakeholderEntity {
  /** Group level member */
  Group = 'GROUP',
  /** Organization level member */
  Organization = 'ORGANIZATION'
}

/** Stakeholder role enum definition */
export enum StakeholderRole {
  /**
   * Has all privileges on the platform, except for
   * changing treatments
   */
  Admin = 'ADMIN',
  /**
   * Ensures high-quality ethical hacking and pentesting
   * deliverables
   */
  Architect = 'ARCHITECT',
  /**
   * Provides support and streamlines processes for
   * organizations
   */
  CustomerManager = 'CUSTOMER_MANAGER',
  /**
   * Designed for technical leaders within their group,
   * this role provides access to basic privileges on the
   * platform and enables them to generate reports, receive
   * notifications, define, change, and approve treatments,
   * adding tags and other group related actions
   */
  GroupManager = 'GROUP_MANAGER',
  /**
   * Identifies, exploits, and reports vulnerabilities
   * in organizations
   */
  Hacker = 'HACKER',
  /**
   * Designed for technical leaders within their organization,
   * this role provides access to basic privileges on the
   * platform and enables them to handle credentials, organization
   * analytics, billing, stakeholders and mailmaps
   */
  OrganizationManager = 'ORGANIZATION_MANAGER',
  /**
   * Verifies the effectiveness of solutions implemented
   * for vulnerability remediation
   */
  Reattacker = 'REATTACKER',
  /** Helps keep organization inputs updated */
  Resourcer = 'RESOURCER',
  /**
   * Manages reported vulnerabilities, evaluates drafts,
   * and verifies risk levels
   */
  Reviewer = 'REVIEWER',
  /**
   * Forces manages service forces execution and queries
   * at the group level, including adding executions,
   * querying findings, groups, roots, and vulnerabilities.
   * Handles historical zero risk resolution and manages
   * associated tags
   */
  ServiceForces = 'SERVICE_FORCES',
  /**
   * Default role given to developers or individuals
   * responsible for resolving vulnerabilities
   */
  User = 'USER',
  /**
   * Designed for technical leaders within their organization,
   * this role provides access to basic privileges on the
   * platform and enables them to generate reports, receive
   * notifications, define, change, and approve treatments,
   * request reattacks, and add tags
   */
  VulnerabilityManager = 'VULNERABILITY_MANAGER'
}

/** Main Subscription object */
export type Subscription = {
  __typename?: 'Subscription';
  /**
   * Suggested fix for the vulnerability. This is a fix created with AI
   * from the code of the vulnerability, this fix is only available for
   * vulnerabilities in code.
   */
  getCustomFix: Scalars['String']['output'];
  /** Create a suggested fix for a vulnerable function */
  getSuggestedFix: Scalars['String']['output'];
};


/** Main Subscription object */
export type SubscriptionGetCustomFixArgs = {
  featurePreview?: InputMaybe<Scalars['Boolean']['input']>;
  vulnerabilityId: Scalars['String']['input'];
};


/** Main Subscription object */
export type SubscriptionGetSuggestedFixArgs = {
  featurePreview?: InputMaybe<Scalars['Boolean']['input']>;
  vulnerabilityId: Scalars['String']['input'];
  vulnerableCodeImports: Scalars['String']['input'];
  vulnerableFunction: Scalars['String']['input'];
  vulnerableLineContent: Scalars['String']['input'];
};

/** Group subscription type enum definition */
export enum SubscriptionType {
  /** Continuous Hacking */
  Continuous = 'CONTINUOUS',
  /**
   * One-Shot Hacking
   * @deprecated This value is deprecated and will be removed after 2025/04/30
   */
  Oneshot = 'ONESHOT'
}

/** Tag type definition */
export type Tag = {
  __typename?: 'Tag';
  /** Groups where the tag is present */
  groups: Maybe<Array<Group>>;
  /**
   * Days since the last remediation of a Vulnerability
   * in the groups where the tag is present
   */
  lastClosedVulnerability: Maybe<Scalars['Int']['output']>;
  /**
   * Maximum severity of a currently open Vulnerability
   * in the groups where the tag is present
   */
  maxOpenSeverity: Maybe<Scalars['Float']['output']>;
  /**
   * Mean time in days to remediate a Vulnerability
   * in the groups where the tag is present
   */
  meanRemediate: Maybe<Scalars['Float']['output']>;
  /**
   * Mean time in days to remediate a critical Vulnerability
   * in the groups where the tag is present
   */
  meanRemediateCriticalSeverity: Maybe<Scalars['Float']['output']>;
  /**
   * Mean time in days to remediate a high-severity Vulnerability
   * in the groups where the tag is present
   */
  meanRemediateHighSeverity: Maybe<Scalars['Float']['output']>;
  /**
   * Mean time in days to remediate a low-severity Vulnerability
   * in the groups where the tag is present
   */
  meanRemediateLowSeverity: Maybe<Scalars['Float']['output']>;
  /**
   * Mean time in days to remediate a medium-severity Vulnerability
   * in the groups where the tag is present
   */
  meanRemediateMediumSeverity: Maybe<Scalars['Float']['output']>;
  /** Tag name, also its content */
  name: Scalars['String']['output'];
  /** Organization where the tag is present */
  organization: Maybe<Scalars['String']['output']>;
};

/** Technique enum definition */
export enum Technique {
  /** CLOUD */
  Cloud = 'CLOUD',
  /** Cloud security posture management */
  Cspm = 'CSPM',
  /** Dynamic application security testing */
  Dast = 'DAST',
  /**
   * Manual penetration testing
   * @deprecated This enum is deprecated and will be removed after 2025/04/30.
   * Use PTAAS technique instead.
   */
  Mpt = 'MPT',
  /** Pen testing as a service */
  Ptaas = 'PTAAS',
  /** Reverse engineering */
  Re = 'RE',
  /** Static application security testing */
  Sast = 'SAST',
  /** Software composition analysis */
  Sca = 'SCA',
  /** Secure code review */
  Scr = 'SCR'
}

/** Group tier type enum definition */
export enum TierType {
  /** Continuous Advanced Tier */
  Advanced = 'ADVANCED',
  /** Continuous Essential Tier */
  Essential = 'ESSENTIAL',
  /** Free Tier */
  Free = 'FREE',
  /** One-Shot Tier */
  Oneshot = 'ONESHOT',
  /** Other custom tier (old) */
  Other = 'OTHER'
}

/** ToeInput type definition */
export type ToeInput = {
  __typename?: 'ToeInput';
  /** Attack moment in ISO format */
  attackedAt: Maybe<Scalars['DateTime']['output']>;
  /** Hacker's email */
  attackedBy: Scalars['String']['output'];
  /** Indicates if the input is present in the Root */
  bePresent: Scalars['Boolean']['output'];
  /** Date until the input is present in ToE */
  bePresentUntil: Maybe<Scalars['DateTime']['output']>;
  /** Application/infrastructure input URL */
  component: Scalars['String']['output'];
  /** Entry point of the test */
  entryPoint: Scalars['String']['output'];
  /** The address of the environment the input is associated with */
  environmentId: Scalars['String']['output'];
  /** First attack moment in ISO format */
  firstAttackAt: Maybe<Scalars['DateTime']['output']>;
  /**
   * True if the associated `component` has OPEN vulnerabilities.
   * This field is eventually consistent
   */
  hasVulnerabilities: Maybe<Scalars['Boolean']['output']>;
  /** ToE input Root */
  root: Maybe<Root>;
  /** First time the input was seen */
  seenAt: Maybe<Scalars['DateTime']['output']>;
  /** Hacker who found the application input */
  seenFirstTimeBy: Scalars['String']['output'];
};

/** ToE Input Edge type definition */
export type ToeInputEdge = Edge & {
  __typename?: 'ToeInputEdge';
  /** The cursor for the edge */
  cursor: Scalars['String']['output'];
  /** The node with the payload */
  node: ToeInput;
};

/** Toe Inputs Connection type definition */
export type ToeInputsConnection = {
  __typename?: 'ToeInputsConnection';
  /** List of ToE Inputs edges */
  edges: Array<ToeInputEdge>;
  /** Information to aid in pagination */
  pageInfo: PageInfo;
  /** Total number of inputs found */
  total: Maybe<Scalars['Int']['output']>;
};

/** ToeLines type definition */
export type ToeLines = {
  __typename?: 'ToeLines';
  /** Attack moment in iso format */
  attackedAt: Maybe<Scalars['DateTime']['output']>;
  /** Hacker email */
  attackedBy: Scalars['String']['output'];
  /** Attacked lines */
  attackedLines: Scalars['Int']['output'];
  /** If the file is present in the repository */
  bePresent: Scalars['Boolean']['output'];
  /** Date until lines was present in ToE */
  bePresentUntil: Maybe<Scalars['DateTime']['output']>;
  /** ToE comment */
  comments: Scalars['String']['output'];
  /** ToE Filename */
  filename: Scalars['String']['output'];
  /** First attack moment in iso format */
  firstAttackAt: Maybe<Scalars['DateTime']['output']>;
  /**
   * True if the associated `filename` has OPEN vulnerabilities.
   * This field is eventually consistent.
   */
  hasVulnerabilities: Maybe<Scalars['Boolean']['output']>;
  /** Last author to modify this file */
  lastAuthor: Scalars['String']['output'];
  /** SHA-1 commit hash in short form in which this file was modified */
  lastCommit: Scalars['String']['output'];
  /** Lines of code */
  loc: Scalars['Int']['output'];
  /** Modified date in iso format */
  modifiedDate: Scalars['DateTime']['output'];
  /** Toe lines Git Root */
  root: GitRoot;
  /** First time the file was seen */
  seenAt: Scalars['DateTime']['output'];
  /** Sorts priority factor */
  sortsPriorityFactor: Scalars['Int']['output'];
  /** The last time that Priority was updated */
  sortsRiskLevelDate: Maybe<Scalars['DateTime']['output']>;
  /** Sorts suggestions for vulnerabilities */
  sortsSuggestions: Maybe<Array<Maybe<ToeLinesSortSuggestion>>>;
};

/** ToE lines Connection type definition */
export type ToeLinesConnection = {
  __typename?: 'ToeLinesConnection';
  /** List of ToE Lines edges */
  edges: Maybe<Array<Maybe<ToeLinesEdge>>>;
  /** Information to aid in pagination */
  pageInfo: PageInfo;
  /** Total number of lines found */
  total: Maybe<Scalars['Int']['output']>;
};

/** ToE lines Edge type definition */
export type ToeLinesEdge = Edge & {
  __typename?: 'ToeLinesEdge';
  /** The cursor for the edge */
  cursor: Scalars['String']['output'];
  /** The node with the payload */
  node: Maybe<ToeLines>;
};

/** Sorts suggestion for vulnerabilities in the file, given as a probability */
export type ToeLinesSortSuggestion = {
  __typename?: 'ToeLinesSortSuggestion';
  /** Title of the Finding related to the suggestion */
  findingTitle: Scalars['String']['output'];
  /** Probability presented as a percentage */
  probability: Scalars['Int']['output'];
};

/** ToePackage type definition */
export type ToePackage = {
  __typename?: 'ToePackage';
  /** List of associated advisories */
  advisories: Maybe<Array<ToePackageAdvisory>>;
  /** Indicates if the package is present in the repository */
  bePresent: Scalars['Boolean']['output'];
  /** Health-related metadata */
  healthMetadata: Maybe<ToePackageHealthMetadata>;
  /** Package identifier */
  id: Scalars['ID']['output'];
  /** List of package locations */
  locations: Maybe<Array<ToePackageCoordinates>>;
  /** Package name */
  name: Scalars['String']['output'];
  /** Indicates if the package and its version are outdated */
  outdated: Maybe<Scalars['Boolean']['output']>;
  /**
   * String used to identify and locate a software package in
   * a mostly universal and uniform way across programming
   * languages, package managers, packaging conventions, tools,
   * APIs and databases
   */
  packageUrl: Maybe<Scalars['String']['output']>;
  /** Specific software environment */
  platform: Maybe<Scalars['String']['output']>;
  /** Git Root associated with the package */
  root: GitRoot;
  /** Package web address */
  url: Maybe<Scalars['String']['output']>;
  /** The version currently declared */
  version: Scalars['String']['output'];
  /** IDs of related vulnerabilities */
  vulnerabilityInfo: Maybe<Array<ToePackageVulnerabilityInfo>>;
  /** Indicates if the package and its version are vulnerable */
  vulnerable: Maybe<Scalars['Boolean']['output']>;
};

/** ToePackageAdvisory type definition */
export type ToePackageAdvisory = {
  __typename?: 'ToePackageAdvisory';
  /** List of Common Platform Enumeration (CPE) identifiers associated with this advisory */
  cpes: Array<Scalars['String']['output']>;
  /** Detailed explanation of the advisory */
  description: Maybe<Scalars['String']['output']>;
  /** Exploit Prediction Scoring System (EPSS) score for this advisory */
  epss: Maybe<Scalars['Float']['output']>;
  /** Unique identifier for the advisory */
  id: Scalars['String']['output'];
  /** The ecosystem or package manager namespace for this advisory */
  namespace: Scalars['String']['output'];
  /** The EPSS percentile for this advisory */
  percentile: Maybe<Scalars['Float']['output']>;
  /** The assessed severity level of the advisory */
  severity: Scalars['String']['output'];
  /** List of URLs providing additional information about the advisory */
  urls: Array<Scalars['String']['output']>;
  /** Version constraint indicating which package versions are affected by this advisory */
  versionConstraint: Maybe<Scalars['String']['output']>;
};

/** Indicating the location of a package within a project. */
export type ToePackageCoordinates = {
  __typename?: 'ToePackageCoordinates';
  /** Transitive, direct, or unknown type of dependency */
  dependencyType: Maybe<Scalars['String']['output']>;
  /** The ref of the container image where the package was found */
  imageRef: Maybe<Scalars['String']['output']>;
  /** The ID of the layer where the package was found */
  layer: Maybe<Scalars['String']['output']>;
  /** The line number in the file where the package is located */
  line: Maybe<Scalars['String']['output']>;
  /** The file in which the package is located */
  path: Scalars['String']['output'];
  /** Dependency type dev o prod */
  scope: Maybe<Scalars['String']['output']>;
};

/** Toe Package Edge type definition */
export type ToePackageEdge = Edge & {
  __typename?: 'ToePackageEdge';
  /** The cursor for the edge */
  cursor: Scalars['String']['output'];
  /** The node with the payload */
  node: ToePackage;
};

/** Health metadata type definition */
export type ToePackageHealthMetadata = {
  __typename?: 'ToePackageHealthMetadata';
  /** Authors who contributed to the package */
  authors: Maybe<Scalars['String']['output']>;
  /** Latest version of the package */
  latestVersion: Maybe<Scalars['String']['output']>;
  /** Creation date of the latest version */
  latestVersionCreatedAt: Maybe<Scalars['String']['output']>;
};

/** Sorts suggestion for vulnerabilities in the file, given as a probability */
export type ToePackageVulnerabilityInfo = {
  __typename?: 'ToePackageVulnerabilityInfo';
  /** Common Vulnerabilities and Exposures array */
  cve: Array<Scalars['String']['output']>;
  /** Vulnerability id */
  id: Scalars['String']['output'];
  /** CVSS temporal score, derived from the associated vector string */
  severityScore: Scalars['Float']['output'];
};

/** Toe Packages Connection type definition */
export type ToePackagesConnection = {
  __typename?: 'ToePackagesConnection';
  /** A list of ToE Packages edges */
  edges: Array<ToePackageEdge>;
  /** Information to aid in pagination */
  pageInfo: PageInfo;
  /** Total packages found */
  total: Scalars['Int']['output'];
};

/** ToePort type definition */
export type ToePort = {
  __typename?: 'ToePort';
  /** IP address of the environment */
  address: Scalars['String']['output'];
  /** Attack moment in ISO format */
  attackedAt: Maybe<Scalars['DateTime']['output']>;
  /** Hacker's email */
  attackedBy: Maybe<Scalars['String']['output']>;
  /** Indicates if the port is present in the root */
  bePresent: Scalars['Boolean']['output'];
  /** Date until the port is present in ToE */
  bePresentUntil: Maybe<Scalars['DateTime']['output']>;
  /** First attack moment in ISO format */
  firstAttackAt: Maybe<Scalars['DateTime']['output']>;
  /**
   * True if the associated `address` has OPEN vulnerabilities.
   * This field is eventually consistent
   */
  hasVulnerabilities: Maybe<Scalars['Boolean']['output']>;
  /** Port number */
  port: Scalars['Int']['output'];
  /** Root of the ToE port */
  root: Maybe<IpRoot>;
  /** First time the port was seen */
  seenAt: Maybe<Scalars['DateTime']['output']>;
  /** Hacker who found the application port */
  seenFirstTimeBy: Maybe<Scalars['String']['output']>;
};

/** Toe Port Edge type definition */
export type ToePortEdge = Edge & {
  __typename?: 'ToePortEdge';
  /** The cursor for the edge */
  cursor: Scalars['String']['output'];
  /** The node with the payload */
  node: Maybe<ToePort>;
};

/** Toe Ports Connection type definition */
export type ToePortsConnection = {
  __typename?: 'ToePortsConnection';
  /** A list of Toe Ports edges */
  edges: Maybe<Array<Maybe<ToePortEdge>>>;
  /** Information to aid in pagination */
  pageInfo: PageInfo;
};

/** Tours type definition */
export type Tours = {
  __typename?: 'Tours';
  /** Add group new user workflow */
  newGroup: Scalars['Boolean']['output'];
  /** Add git roots new user workflow */
  newRoot: Scalars['Boolean']['output'];
  /** Manage welcome tour */
  welcome: Scalars['Boolean']['output'];
};

/** Tours input definition */
export type ToursInput = {
  /** Add group new user workflow */
  newGroup: Scalars['Boolean']['input'];
  /** Add git roots new user workflow */
  newRoot: Scalars['Boolean']['input'];
  /** Manage welcome tour */
  welcome: Scalars['Boolean']['input'];
};

/** Finding Tracking type definition */
export type Tracking = {
  __typename?: 'Tracking';
  /** Number of  Vulnerabilities temporarily accepted in this cycle */
  accepted: Maybe<Scalars['Int']['output']>;
  /** Number of Vulnerabilities permanently accepted in this cycle */
  acceptedUndefined: Maybe<Scalars['Int']['output']>;
  /** Email address of the user assigned to this tracking */
  assigned: Maybe<Scalars['String']['output']>;
  /** Total number of re-attacks requested */
  cycle: Maybe<Scalars['Int']['output']>;
  /** Specific date of the tracking cycle */
  date: Maybe<Scalars['String']['output']>;
  /** Specific justification of the tracking cycle */
  justification: Maybe<Scalars['String']['output']>;
  /** Number of Safe locations in this cycle */
  safe: Maybe<Scalars['Int']['output']>;
  /** Number of vulnerable locations in this cycle */
  vulnerable: Maybe<Scalars['Int']['output']>;
};

/** Treatment type definition */
export type Treatment = {
  __typename?: 'Treatment';
  /**
   * Date stipulated in a Temporarily accepted treatment cycle
   * (if the date arrives and the Vulnerability remains Vulnerable,
   * the treatment changes from ACCEPTED to UNTREATED in a new cycle)
   */
  acceptanceDate: Maybe<Scalars['String']['output']>;
  /**
   * Status of the request to apply Permanently accepted treatment
   * to a Vulnerability in all cycles of a Vulnerability
   */
  acceptanceStatus: Maybe<Scalars['String']['output']>;
  /** Email address of the user assigned to the treatment */
  assigned: Maybe<Scalars['String']['output']>;
  /** Date of the treatment cycle */
  date: Maybe<Scalars['String']['output']>;
  /** Justifications registered in the treatment cycle */
  justification: Maybe<Scalars['String']['output']>;
  /** Treatment applied to the Vulnerability in the treatment cycle */
  treatment: Maybe<Scalars['String']['output']>;
  /**
   * Email address of the user assigned to the Vulnerability
   * in a processing cycle
   */
  user: Maybe<Scalars['String']['output']>;
};

/** Vulnerability Treatment acceptance policy type enum definition */
export enum TreatmentAcceptancePolicy {
  /**
   * Temporally accept the vulnerability without immediate
   * remediation
   */
  Accepted = 'ACCEPTED',
  /**
   * Permanently accept the vulnerability without immediate
   * remediation, acknowledging the associated risks permanently
   */
  AcceptedUndefined = 'ACCEPTED_UNDEFINED'
}

/** Treatment Edge type definition */
export type TreatmentEdge = Edge & {
  __typename?: 'TreatmentEdge';
  /** The cursor for the edge */
  cursor: Scalars['String']['output'];
  /** The node with the payload */
  node: Treatment;
};

/** TreatmentSummary type definition */
export type TreatmentSummary = {
  __typename?: 'TreatmentSummary';
  /** Number of vulnerabilities in Temporarily accepted status */
  accepted: Scalars['Int']['output'];
  /** Number of vulnerabilities in Permanently accepted status */
  acceptedUndefined: Scalars['Int']['output'];
  /** Number of vulnerabilities in In progress status */
  inProgress: Scalars['Int']['output'];
  /** Number of untreated vulnerabilities */
  untreated: Scalars['Int']['output'];
};

/** Trial type definition */
export type Trial = {
  __typename?: 'Trial';
  /** If true, the user has finished the ARM trial */
  completed: Scalars['Boolean']['output'];
  /** Date and time of an extension of the ARM trial */
  extensionDate: Scalars['String']['output'];
  /** Duration of the ARM trial extension in days */
  extensionDays: Scalars['Int']['output'];
  /** Trial Group Role */
  groupRole: Maybe<Scalars['String']['output']>;
  /** The reason why accounts are created */
  reason: Maybe<TrialReason>;
  /** Start date of the ARM trial */
  startDate: Scalars['String']['output'];
  /** Trial state based on its dates */
  state: TrialState;
};

/** Trial reason */
export enum TrialReason {
  /** Generate a compliance certificate */
  Compliance = 'COMPLIANCE',
  /** Develop secure software */
  DevelopSecure = 'DEVELOP_SECURE',
  /** Validate CASA framework */
  ValidateFramework = 'VALIDATE_FRAMEWORK'
}

/** Trial state */
export enum TrialState {
  /** On extended trial */
  Extended = 'EXTENDED',
  /** Extended trial ended */
  ExtendedEnded = 'EXTENDED_ENDED',
  /** On trial */
  Trial = 'TRIAL',
  /** Trial ended */
  TrialEnded = 'TRIAL_ENDED'
}

/** Trusted device type definition */
export type TrustedDevice = {
  __typename?: 'TrustedDevice';
  /** User session browser */
  browser: Scalars['String']['output'];
  /** User session device */
  device: Scalars['String']['output'];
  /** First login device */
  firstLoginDate: Maybe<Scalars['String']['output']>;
  /** User session ip address */
  ipAddress: Scalars['String']['output'];
  /** OTP last attempt */
  lastAttempt: Maybe<Scalars['String']['output']>;
  /** Last login device */
  lastLoginDate: Maybe<Scalars['String']['output']>;
  /** User session location */
  location: Scalars['String']['output'];
  /** OTP token identification */
  otpTokenJti: Maybe<Scalars['String']['output']>;
};

/** Trusted device input definition */
export type TrustedDeviceInput = {
  /** User session browser */
  browser: Scalars['String']['input'];
  /** User session device */
  device: Scalars['String']['input'];
  /** OTP token identification */
  otpTokenJti?: InputMaybe<Scalars['String']['input']>;
};

/** Protocol to use in URLRoots enum definition */
export enum UrlProtocol {
  /** Protocol for local file access */
  File = 'FILE',
  /** Protocol for web communication */
  Http = 'HTTP',
  /** Protocol for secure web communication */
  Https = 'HTTPS'
}

/**
 * Roots whose identifier is the URL of an environment,
 * to perform dynamic application security testing (DAST)
 */
export type UrlRoot = IRoot & {
  __typename?: 'URLRoot';
  /** Sub-paths that are excluded from the scope */
  excludedSubPaths: Array<Scalars['String']['output']>;
  /** Domain name or IP of the environment to test */
  host: Scalars['String']['output'];
  /** ID of the root */
  id: Scalars['ID']['output'];
  /** Nickname for the root */
  nickname: Scalars['String']['output'];
  /** URL path */
  path: Scalars['String']['output'];
  /** URL port */
  port: Scalars['Int']['output'];
  /** URL protocol */
  protocol: UrlProtocol;
  /** URL query */
  query: Maybe<Scalars['String']['output']>;
  /** Secrets associated with the environment */
  secrets: Array<Secret>;
  /** Whether the root is active or not */
  state: ResourceState;
  /** Vulnerabilities associated with the root */
  vulnerabilities: Maybe<Array<Vulnerability>>;
};

/** URLRoot Edge type definition */
export type UrlRootEdge = Edge & {
  __typename?: 'URLRootEdge';
  /** The cursor for the edge */
  cursor: Scalars['String']['output'];
  /** The node with the payload */
  node: UrlRoot;
};

/** URLRoots Connection type definition */
export type UrlRootsConnection = {
  __typename?: 'URLRootsConnection';
  /** A list of URLRoots edges */
  edges: Array<UrlRootEdge>;
  /** Information to aid in pagination */
  pageInfo: PageInfo;
  /** Total URLRoots found */
  total: Scalars['Int']['output'];
};

/** UnfulfilledStandard type definition */
export type UnfulfilledStandard = {
  __typename?: 'UnfulfilledStandard';
  /** Id of the standard */
  standardId: Scalars['String']['output'];
  /** Title of the standard */
  title: Maybe<Scalars['String']['output']>;
  /** Unfulfilled requirements of the standard */
  unfulfilledRequirements: Array<Maybe<Requirement>>;
};

/** Update access token Payload type definition */
export type UpdateAccessTokenPayload = Payload & {
  __typename?: 'UpdateAccessTokenPayload';
  /** New API access token */
  sessionJwt: Scalars['String']['output'];
  /** Did the query succeed? */
  success: Scalars['Boolean']['output'];
};

/** Update client description treatment enum definition */
export enum UpdateClientDescriptionTreatment {
  /**
   * Temporally accept the vulnerability without immediate
   * remediation
   */
  Accepted = 'ACCEPTED',
  /**
   * Permanently accept the vulnerability without immediate
   * remediation, acknowledging the associated risks permanently
   */
  AcceptedUndefined = 'ACCEPTED_UNDEFINED',
  /**
   * Acknowledge the vulnerability and assign it to a user
   * for remediation
   */
  InProgress = 'IN_PROGRESS'
}

/** Reasons for downgrading a Group's services/plans */
export enum UpdateGroupReason {
  /** Budget constraints */
  Budget = 'BUDGET',
  /** Client decided to end the security testing */
  GroupFinalization = 'GROUP_FINALIZATION',
  /** Client decided to put security testing on hold */
  GroupSuspension = 'GROUP_SUSPENSION',
  /** No particular reason */
  None = 'NONE',
  /** Another reason not covered */
  Other = 'OTHER'
}

/** Update stakeholder Payload type definition */
export type UpdateStakeholderPayload = Payload & {
  __typename?: 'UpdateStakeholderPayload';
  /** Modified Organization/Group Stakeholder */
  modifiedStakeholder: Maybe<Stakeholder>;
  /** Did the query succeed? */
  success: Scalars['Boolean']['output'];
};

/** Update toe input Payload type definition */
export type UpdateToeInputPayload = Payload & {
  __typename?: 'UpdateToeInputPayload';
  /** Did the query succeed? */
  success: Scalars['Boolean']['output'];
  /** Modified Toe Input */
  toeInput: ToeInput;
};

/** Update toe lines Payload type definition */
export type UpdateToeLinesPayload = Payload & {
  __typename?: 'UpdateToeLinesPayload';
  /** Did the query succeed? */
  success: Scalars['Boolean']['output'];
  /** Modified Toe Lines */
  toeLines: ToeLines;
};

/** Update toe port Payload type definition */
export type UpdateToePortPayload = Payload & {
  __typename?: 'UpdateToePortPayload';
  /** Did the query succeed? */
  success: Scalars['Boolean']['output'];
  /** Modified Toe Port */
  toePort: ToePort;
};

/** Upload git root file Payload type definition */
export type UploadGitRootFilePayload = Payload & {
  __typename?: 'UploadGitRootFilePayload';
  /** List of lines with errors */
  errorLines: Array<Scalars['String']['output']>;
  /** Helper message about status of the request */
  message: Scalars['String']['output'];
  /** Did the query succeed? */
  success: Scalars['Boolean']['output'];
  /** Total error lines */
  totalErrors: Scalars['Int']['output'];
  /** Total success lines */
  totalSuccess: Scalars['Int']['output'];
};

/** URI with credentials input definition */
export type UriWithCredentialsInput = {
  /** The associated credentials ID (optional) */
  credentialId?: InputMaybe<Scalars['String']['input']>;
  /** The URI to be used */
  uri: Scalars['String']['input'];
};

/** Channel to send notifications */
export enum VerificationChannel {
  /** Voice channel */
  Call = 'CALL',
  /** Email channel */
  Email = 'EMAIL',
  /** SMS Channel */
  Sms = 'SMS',
  /** Whatsapp channel */
  Whatsapp = 'WHATSAPP'
}

/** VerificationSummary type definition */
export type VerificationSummary = {
  __typename?: 'VerificationSummary';
  /** Number of vulnerabilities in On hold status */
  onHold: Scalars['Int']['output'];
  /** Number of vulnerabilities in Requested status */
  requested: Scalars['Int']['output'];
  /** Number of vulnerabilities Verified status */
  verified: Scalars['Int']['output'];
};

/** Vulnerabilities Connection type definition */
export type VulnerabilitiesConnection = {
  __typename?: 'VulnerabilitiesConnection';
  /** A list of Vulnerabilities edges */
  edges: Array<Maybe<VulnerabilityEdge>>;
  /** Information to aid in pagination */
  pageInfo: PageInfo;
  /** Total vulnerabilities found */
  total: Maybe<Scalars['Int']['output']>;
};

/** Vulnerabilities summary type definition */
export type VulnerabilitiesSummary = {
  __typename?: 'VulnerabilitiesSummary';
  /** Total number of closed vulnerabilities */
  closed: Scalars['Int']['output'];
  /** Total number of open vulnerabilities */
  open: Scalars['Int']['output'];
  /** Total number of open vulnerabilities with critical severity */
  openCritical: Scalars['Int']['output'];
  /** Total number of open vulnerabilities with high severity */
  openHigh: Scalars['Int']['output'];
  /** Total number of open vulnerabilities with low severity */
  openLow: Scalars['Int']['output'];
  /** Total number of open vulnerabilities with medium severity */
  openMedium: Scalars['Int']['output'];
};

/** Vulnerability type definition */
export type Vulnerability = {
  __typename?: 'Vulnerability';
  /** Advisory information for SCA vulnerabilities */
  advisories: Maybe<VulnerabilityAdvisories>;
  /**
   * Commit information on when the vulnerability
   * was introduced to the codebase.
   * Only applies for LINES type
   */
  author: Maybe<VulnerabilityAuthor>;
  /** Date when the Vulnerability was closed */
  closingDate: Maybe<Scalars['String']['output']>;
  /** Commit hash to get to the Vulnerability */
  commitHash: Maybe<Scalars['String']['output']>;
  /**
   * Suggested fix for the Vulnerability. This is a fix created with
   * AI from the code of the Vulnerability and is only available for
   * vulnerabilities in code
   */
  customFix: Maybe<Scalars['String']['output']>;
  /**
   * Custom severity score, not corresponding to a CVSS score.
   * Must be a positive number and can be greater than 10.0
   */
  customSeverity: Maybe<Scalars['Int']['output']>;
  /** Number of requested re-attacks the vulnerability has */
  cycles: Scalars['Int']['output'];
  /** Remediation effectiveness */
  efficacy: Scalars['Float']['output'];
  /** Identifier of the last Event keeping the Vulnerability's re-attack on hold */
  eventId: Maybe<Scalars['String']['output']>;
  /** The URL to the ticket in the Bug Tracking System (BTS) of preference */
  externalBugTrackingSystem: Maybe<Scalars['String']['output']>;
  /** The Finding the Vulnerability is associated with */
  finding: Finding;
  /** Identifier of the Finding the Vulnerability is associated with */
  findingId: Scalars['String']['output'];
  /** Identifier of the Group the Vulnerability is associated with */
  groupName: Scalars['String']['output'];
  /** Last user who changed the vulnerability's state */
  hacker: Scalars['String']['output'];
  /** Historic configuration of the treatments the Vulnerability has had */
  historicTreatmentConnection: VulnerabilityHistoricTreatmentConnection;
  /** Identifier of the Vulnerability */
  id: Scalars['String']['output'];
  /**
   * Whether the custom fix feature supports generating a remediation guide or
   * an autofix diff for this vulnerability
   */
  isAutoFixable: Scalars['Boolean']['output'];
  /** Last stakeholder who edited the vulnerability's state */
  lastEditedBy: Maybe<Scalars['String']['output']>;
  /**
   * The date of the last VERIFIED status on the Vulnerability (i.e. the date on which
   * a Fluid Attacks Hacker verifies a re-attack and whether a fix was effective)
   */
  lastReattackDate: Maybe<Scalars['String']['output']>;
  /** Last stakeholder who has requested a re-attack */
  lastReattackRequester: Maybe<Scalars['String']['output']>;
  /** Date of last REQUESTED status (i.e. when the last re-attack was requested) */
  lastRequestedReattackDate: Maybe<Scalars['String']['output']>;
  /** Date from last state status */
  lastStateDate: Maybe<Scalars['String']['output']>;
  /** Date from last treatment status */
  lastTreatmentDate: Maybe<Scalars['String']['output']>;
  /** Date from last verification status */
  lastVerificationDate: Maybe<Scalars['String']['output']>;
  /** The description for the method that reports the Vulnerability */
  methodDescription: Maybe<Scalars['String']['output']>;
  /** Priority value assigned by the user */
  priority: Maybe<Scalars['Float']['output']>;
  /** Author from latest severity proposal */
  proposedSeverityAuthor: Maybe<Scalars['String']['output']>;
  /**
   * Proposed CVSS v3.1 temporal score, derived from the associated vector string
   * @deprecated This value is deprecated and will be removed after 2025/10/04
   * Use proposedSeverityThreatScore instead.
   */
  proposedSeverityTemporalScore: Maybe<Scalars['Float']['output']>;
  /** Proposed CVSS-BTE v4 score, derived from the associated vector string */
  proposedSeverityThreatScore: Maybe<Scalars['Float']['output']>;
  /**
   * Proposed CVSS v3.1 vector string associated to the Vulnerability
   * @deprecated This value is deprecated and will be removed after 2025/10/04
   * Use proposedSeverityVectorV4 instead.
   */
  proposedSeverityVector: Maybe<Scalars['String']['output']>;
  /** Proposed CVSS v4 vector string associated to the Vulnerability */
  proposedSeverityVectorV4: Maybe<Scalars['String']['output']>;
  /** Is the Vulnerability currently requested for a re-attack/remediation? */
  remediated: Scalars['Boolean']['output'];
  /** Report date of the Vulnerability */
  reportDate: Maybe<Scalars['String']['output']>;
  /** Root where the Vulnerability was found */
  root: Maybe<Root>;
  /** Nickname of the root where the Vulnerability was found */
  rootNickname: Maybe<Scalars['String']['output']>;
  /** CVSS qualitative severity rating */
  severityRating: Maybe<SeverityRating>;
  /**
   * CVSS v3.1 temporal score, derived from the associated vector string
   * @deprecated This value is deprecated and will be removed after 2025/10/04
   * Use severityThreatScore instead.
   */
  severityTemporalScore: Scalars['Float']['output'];
  /** CVSS-BTE v4 score, derived from the associated vector string */
  severityThreatScore: Scalars['Float']['output'];
  /**
   * CVSS v3.1 vector string associated to the Vulnerability
   * @deprecated This value is deprecated and will be removed after 2025/10/04
   * Use severityVectorV4 instead.
   */
  severityVector: Scalars['String']['output'];
  /** CVSS v4 vector string associated to the Vulnerability */
  severityVectorV4: Scalars['String']['output'];
  /**
   * If the Vulnerability is of type lines, it returns a portion of code where
   * the Vulnerability was found
   */
  snippet: Maybe<Snippet>;
  /** Detection source of the Vulnerability */
  source: Scalars['String']['output'];
  /**
   * Specific location of the Vulnerability, can be a line of code, a field
   * or a port
   */
  specific: Scalars['String']['output'];
  /** Whether the Vulnerability is Safe or Vulnerable */
  state: VulnerabilityState;
  /** Justifications for the vulnerability state */
  stateReasons: Maybe<Array<Maybe<VulnerabilityStateReason>>>;
  /** Stream to get to the Vulnerability */
  stream: Maybe<Scalars['String']['output']>;
  /** All the tags associated with the Vulnerability in concatenated string form */
  tag: Maybe<Scalars['String']['output']>;
  /** List of all the tags associated with the Vulnerability */
  tags: Maybe<Array<Scalars['String']['output']>>;
  /** Technique used to discover the Vulnerability */
  technique: Maybe<Technique>;
  /**
   * Date stipulated in the temporarily accepted treatment (if the date arrives and
   * the Vulnerability has not been closed, the treatment goes from ACCEPTED
   * to UNTREATED)
   */
  treatmentAcceptanceDate: Maybe<Scalars['String']['output']>;
  /**
   * Status of the request of the application of the permanently accepted treatment
   * to the Vulnerability
   */
  treatmentAcceptanceStatus: Maybe<Scalars['String']['output']>;
  /** Email address of the user who is assigned */
  treatmentAssigned: Maybe<Scalars['String']['output']>;
  /** Total number of times a treatment in the Vulnerability has changed */
  treatmentChanges: Scalars['Int']['output'];
  /** The last comment was made in the treatment of the Vulnerability */
  treatmentJustification: Maybe<Scalars['String']['output']>;
  /** The current treatment of the Vulnerability */
  treatmentStatus: Maybe<VulnerabilityTreatment>;
  /** Email address of the user who assigns treatment */
  treatmentUser: Maybe<Scalars['String']['output']>;
  /**
   * Current re-attack status, which can be one of the following:
   * On hold, Verified or Requested
   */
  verification: Maybe<Scalars['String']['output']>;
  /** Comment of the Vulnerability verification */
  verificationJustification: Maybe<Scalars['String']['output']>;
  /** Type of Vulnerability, which can be: Input, lines or ports */
  vulnerabilityType: Scalars['String']['output'];
  /** General location of the Vulnerability, which can be a file, URL or IP */
  where: Scalars['String']['output'];
  /**
   * Status of the Vulnerability that requested the treatment which can be
   * one of the following: Confirmed, Rejected or Requested
   */
  zeroRisk: Maybe<Scalars['String']['output']>;
};


/** Vulnerability type definition */
export type VulnerabilityHistoricTreatmentConnectionArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
};

/** Historic state type definition */
export type VulnerabilityAdvisories = {
  __typename?: 'VulnerabilityAdvisories';
  /** Common Vulnerabilities and Exposures array */
  cve: Maybe<Array<Scalars['String']['output']>>;
  /** Highest EPSS score of the CVE array */
  epss: Maybe<Scalars['Int']['output']>;
  /** Name of affected package */
  package: Maybe<Scalars['String']['output']>;
  /** Version or range of versions of the affected package */
  vulnerableVersion: Maybe<Scalars['String']['output']>;
};

/** Vulnerability Author type definition */
export type VulnerabilityAuthor = {
  __typename?: 'VulnerabilityAuthor';
  /** Author's email for the commit. Could be different from the Comitter email. */
  authorEmail: Scalars['String']['output'];
  /** Commit hash where it was introduced */
  commit: Scalars['String']['output'];
  /** Committer date in the format yyyy-MM-dd hh:mm:ssZ */
  commitDate: Scalars['DateTime']['output'];
};

/** Vulnerability Edge type definition */
export type VulnerabilityEdge = Edge & {
  __typename?: 'VulnerabilityEdge';
  /** The cursor for the edge */
  cursor: Scalars['String']['output'];
  /** The node with the payload */
  node: Vulnerability;
};

/** Vulnerability exploit state */
export enum VulnerabilityExploitState {
  /** Vulnerability has been acknowledge and accepted */
  Accepted = 'ACCEPTED',
  /** Vulnerability has been resolved or closed  */
  Closed = 'CLOSED',
  /** Vulnerability is still active and unresolved */
  Open = 'OPEN',
  /**
   * The current state of the vulnerability is uncertain
   * or not determined
   */
  Unknown = 'UNKNOWN'
}

/** Query filters for vulnerabilities */
export type VulnerabilityFiltersInput = {
  /** Filter by exact assigned users */
  assignees?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** Filter by the reattack status of the vulnerability */
  reattack?: InputMaybe<Scalars['String']['input']>;
  /** Filter vulnerabilities reported since this date */
  reportedAfter?: InputMaybe<Scalars['DateTime']['input']>;
  /** Filter vulnerabilities reported until this date */
  reportedBefore?: InputMaybe<Scalars['DateTime']['input']>;
  /** Search by text */
  search?: InputMaybe<Scalars['String']['input']>;
  /** Filter vulnerabilities by their current state */
  state?: InputMaybe<Array<InputMaybe<VulnerabilityState>>>;
  /** Exclude vulnerabilities by their current state */
  stateNot?: InputMaybe<Array<InputMaybe<VulnerabilityState>>>;
  /** Filter by specific tags */
  tags?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** Exclude by specific tags */
  tagsNot?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  /** Filter by the used technique */
  technique?: InputMaybe<Array<InputMaybe<Technique>>>;
  /** Filter by the used technique */
  techniqueNot?: InputMaybe<Array<InputMaybe<Technique>>>;
  /** Filter by the treatment given to the vulnerability */
  treatment?: InputMaybe<Array<InputMaybe<VulnerabilityTreatment>>>;
  /** Filter by the treatment given to the vulnerability */
  treatmentNot?: InputMaybe<Array<InputMaybe<VulnerabilityTreatment>>>;
  /** Filter by the location where the vulnerability was found */
  where?: InputMaybe<Scalars['String']['input']>;
  /** Filter by zero risk status of the vulnerability */
  zeroRisk?: InputMaybe<Array<InputMaybe<VulnerabilityZeroRiskStatus>>>;
  /** Filter by zero risk status of the vulnerability */
  zeroRiskNot?: InputMaybe<Array<InputMaybe<VulnerabilityZeroRiskStatus>>>;
};

/** Historic Treatment Connection type definition */
export type VulnerabilityHistoricTreatmentConnection = {
  __typename?: 'VulnerabilityHistoricTreatmentConnection';
  /** A list of Treatment edges */
  edges: Array<TreatmentEdge>;
  /** Information to aid in pagination */
  pageInfo: PageInfo;
};

/** Reason to reject a vulnerability, Use vulnerabilityStateReason instead */
export enum VulnerabilityRejectionReason {
  /** Reported by analyst */
  AnalystReport = 'ANALYST_REPORT',
  /** The vulnerability was marked as safe by our automated scanner */
  ClosedByMachine = 'CLOSED_BY_MACHINE',
  /**
   * There are consistency issues with the vulnerabilities, the severity or
   * the evidence
   */
  Consistency = 'CONSISTENCY',
  /** This vulnerability has been previously reported */
  Duplicated = 'DUPLICATED',
  /** The environment related to the vulnerability was removed */
  EnvironmentDeleted = 'ENVIRONMENT_DELETED',
  /** The evidence is insufficient */
  Evidence = 'EVIDENCE',
  /** When the root or environment is deactivated */
  Exclusion = 'EXCLUSION',
  /** The reported vulnerability is not a vulnerability */
  FalsePositive = 'FALSE_POSITIVE',
  /** Functionality is not longer used */
  FunctionalityNoLongerExists = 'FUNCTIONALITY_NO_LONGER_EXISTS',
  /** The vulnerability was updated by our scanner */
  ModifiedByMachine = 'MODIFIED_BY_MACHINE',
  /** The vulnerabilities should be submitted under another Finding type */
  Naming = 'NAMING',
  /** When the vulnerability is not applicable */
  NotRequired = 'NOT_REQUIRED',
  /** When there is no justification */
  NoJustification = 'NO_JUSTIFICATION',
  /** More data should be gathered before submission */
  Omission = 'OMISSION',
  /** Other justification */
  Other = 'OTHER',
  /** Mistake when reporting the vulnerability */
  ReportingError = 'REPORTING_ERROR',
  /** The root associated to the vuln was moved */
  RootMovedToAnotherGroup = 'ROOT_MOVED_TO_ANOTHER_GROUP',
  /** When the root or environment is deactivated */
  RootOrEnvironmentDeactivated = 'ROOT_OR_ENVIRONMENT_DEACTIVATED',
  /** Faulty severity scoring */
  Scoring = 'SCORING',
  /** Vulnerability submitted from yaml */
  SubmittedFromFile = 'SUBMITTED_FROM_FILE',
  /** The fix was effective and it is now safe */
  VerifiedAsSafe = 'VERIFIED_AS_SAFE',
  /** The vulnerability was found to be vulnerable */
  VerifiedAsVulnerable = 'VERIFIED_AS_VULNERABLE',
  /** The writing could be improved */
  Writing = 'WRITING',
  /** Zero risk treatment was requested */
  ZeroRiskRequested = 'ZERO_RISK_REQUESTED'
}

/** Fields to sort vulnerabilities */
export enum VulnerabilitySort {
  /** Sort by severity */
  CvssfScore = 'CVSSF_SCORE',
  /** Sort by severity (v4) */
  CvssfV4Score = 'CVSSF_V4_SCORE',
  /** Sort by id */
  Id = 'ID',
  /** Sort by priority score */
  PriorityScore = 'PRIORITY_SCORE',
  /** Sort by report date */
  ReportDate = 'REPORT_DATE',
  /** Sort by specific */
  Specific = 'SPECIFIC',
  /** Sort by state */
  State = 'STATE',
  /** Sort by technique */
  Technique = 'TECHNIQUE',
  /** Sort by treatment assigned */
  TreatmentAssigned = 'TREATMENT_ASSIGNED',
  /** Sort by treatment status */
  TreatmentStatus = 'TREATMENT_STATUS',
  /** Sort by verification state */
  Verification = 'VERIFICATION',
  /** Sort by location */
  Where = 'WHERE',
  /** Sort by zero risk status */
  ZeroRisk = 'ZERO_RISK'
}

/** Sort fields for vulnerabilities */
export type VulnerabilitySortInput = {
  /** Field to sort by */
  field: VulnerabilitySort;
  /** Sort order */
  order: OrderSort;
};

/** Vulnerability source */
export enum VulnerabilitySource {
  /** Identified by an analyst */
  Analyst = 'ANALYST',
  /** Identified by static code analysis */
  Asm = 'ASM',
  /** Identified by the customer */
  Customer = 'CUSTOMER',
  /** Identified through deterministic means */
  Deterministic = 'DETERMINISTIC',
  /** Identified through an escape mechanism */
  Escape = 'ESCAPE',
  /** Identified automatically by Machine */
  Machine = 'MACHINE'
}

/** Types of vulnerability source */
export enum VulnerabilitySourceType {
  /** Reported automatically by Machine */
  Machine = 'MACHINE',
  /** Reported by Squad */
  Squad = 'SQUAD'
}

/** Vulnerability state */
export enum VulnerabilityState {
  /** Reported vulnerability deemed unnecessary for further action */
  Deleted = 'DELETED',
  /** Vulnerability removed from the group, and masked */
  Masked = 'MASKED',
  /** Vulnerability determined non-vulnerable in the SUBMITTED status */
  Rejected = 'REJECTED',
  /** Vulnerability with applied and validated solution */
  Safe = 'SAFE',
  /** Reported vulnerability awaiting review */
  Submitted = 'SUBMITTED',
  /** Vulnerability without applied fix */
  Vulnerable = 'VULNERABLE'
}

/** Why the vulnerability state */
export enum VulnerabilityStateReason {
  /** Reported by analyst */
  AnalystReport = 'ANALYST_REPORT',
  /** The vulnerability was marked as safe by our automated scanner */
  ClosedByMachine = 'CLOSED_BY_MACHINE',
  /**
   * There are consistency issues with the vulnerabilities, the severity or
   * the evidence
   */
  Consistency = 'CONSISTENCY',
  /** This vulnerability has been previously reported */
  Duplicated = 'DUPLICATED',
  /** The environment related to the vulnerability was removed */
  EnvironmentDeleted = 'ENVIRONMENT_DELETED',
  /** The evidence is insufficient */
  Evidence = 'EVIDENCE',
  /** When the root or environment is deactivated */
  Exclusion = 'EXCLUSION',
  /** The reported vulnerability is not a vulnerability */
  FalsePositive = 'FALSE_POSITIVE',
  /** Functionality is not longer used */
  FunctionalityNoLongerExists = 'FUNCTIONALITY_NO_LONGER_EXISTS',
  /** The vulnerability was updated by our scanner */
  ModifiedByMachine = 'MODIFIED_BY_MACHINE',
  /** The vulnerability was moved to another group due to a migration */
  MovedToAnotherGroup = 'MOVED_TO_ANOTHER_GROUP',
  /** The vulnerabilities should be submitted under another Finding type */
  Naming = 'NAMING',
  /** When the vulnerability is not applicable */
  NotRequired = 'NOT_REQUIRED',
  /** When there is no justification */
  NoJustification = 'NO_JUSTIFICATION',
  /** More data should be gathered before submission */
  Omission = 'OMISSION',
  /** Other justification */
  Other = 'OTHER',
  /** Mistake when reporting the vulnerability */
  ReportingError = 'REPORTING_ERROR',
  /** The root associated to the vuln was moved */
  RootMovedToAnotherGroup = 'ROOT_MOVED_TO_ANOTHER_GROUP',
  /** When the root or environment is deactivated */
  RootOrEnvironmentDeactivated = 'ROOT_OR_ENVIRONMENT_DEACTIVATED',
  /** Faulty severity scoring */
  Scoring = 'SCORING',
  /** Vulnerability submitted from yaml */
  SubmittedFromFile = 'SUBMITTED_FROM_FILE',
  /** The fix was effective and it is now safe */
  VerifiedAsSafe = 'VERIFIED_AS_SAFE',
  /** The vulnerability was found to be vulnerable */
  VerifiedAsVulnerable = 'VERIFIED_AS_VULNERABLE',
  /** The writing could be improved */
  Writing = 'WRITING',
  /** Zero risk treatment was requested */
  ZeroRiskRequested = 'ZERO_RISK_REQUESTED'
}

/** Treatment given to a vulnerability */
export enum VulnerabilityTreatment {
  /** The client opted to temporarily assume the risk */
  Accepted = 'ACCEPTED',
  /** The client opted to indefinitely assume the risk */
  AcceptedUndefined = 'ACCEPTED_UNDEFINED',
  /** The client is working on a solution */
  InProgress = 'IN_PROGRESS',
  /** No treatment has been given yet */
  Untreated = 'UNTREATED'
}

/** Vulnerability verification */
export enum VulnerabilityVerification {
  /** Vulnerabilities without any verification requested */
  NotRequested = 'NOT_REQUESTED',
  /**
   * Verification of solution is pending resolution of an open
   * eventuality
   */
  OnHold = 'ON_HOLD',
  /**
   * Client requests validation of vulnerability solution through
   * initiating reattack
   */
  Requested = 'REQUESTED',
  /**
   * Solution's effectiveness determines state. VULNERABLE if
   * ineffective, SAFE if effective
   */
  Verified = 'VERIFIED'
}

/** Vulnerability zero risk status */
export enum VulnerabilityZeroRiskStatus {
  /**
   * When a Fluid Attacks analyst reviews and approves
   * the Zero Risk classification due to user's mitigation
   * measures
   */
  Confirmed = 'CONFIRMED',
  /**
   * When a Fluid Attacks analyst reviews and rejects
   * the Zero Risk classification
   */
  Rejected = 'REJECTED',
  /**
   * When a user requests to classify the vulnerability
   * as Zero Risk, indicating additional means of mitigation
   */
  Requested = 'REQUESTED'
}

/** ZeroRiskSummary type definition */
export type ZeroRiskSummary = {
  __typename?: 'ZeroRiskSummary';
  /** Number of zero risk vulnerabilities in Confirmed status */
  confirmed: Scalars['Int']['output'];
  /** Number of zero risk vulnerabilities in Rejected status */
  rejected: Scalars['Int']['output'];
  /** Number of zero risk vulnerabilities in Requested status */
  requested: Scalars['Int']['output'];
};

/** ZtnaHttpItems Connection type definition */
export type ZtnaHttpConnection = {
  __typename?: 'ZtnaHttpConnection';
  /** A list of ZtnaHttpItems edges */
  edges: Array<Maybe<ZtnaHttpEdge>>;
  /** Information to aid in pagination */
  pageInfo: PageInfo;
};

/** Ztna http edge type definition */
export type ZtnaHttpEdge = Edge & {
  __typename?: 'ZtnaHttpEdge';
  /** The cursor for the edge */
  cursor: Scalars['String']['output'];
  /** The node with the payload */
  node: ZtnaHttpItem;
};

/** Ztna http item type definition */
export type ZtnaHttpItem = {
  __typename?: 'ZtnaHttpItem';
  /** Date of http request was made in the format yyyy-MM-dd hh:mm:ssZ */
  date: Scalars['DateTime']['output'];
  /** Destination ip of the http request */
  destinationIP: Maybe<Scalars['String']['output']>;
  /** Email where the gateway http request originated from */
  email: Scalars['String']['output'];
  /** Source ip of the http request */
  sourceIP: Maybe<Scalars['String']['output']>;
};

/** Ztna log type enum definition */
export enum ZtnaLogType {
  /**
   * Logs specifically related to HTTP requests, including
   * all events by default, regardless of risk level
   */
  Http = 'HTTP',
  /**
   * Logs related to network traffic, including all events
   * by default, regardless of risk level
   */
  Network = 'NETWORK',
  /**
   * Comprehensive logs detailing network sessions within
   * a Zero Trust environment
   */
  Session = 'SESSION'
}

/** ZtnaNetworkItems Connection type definition */
export type ZtnaNetworkConnection = {
  __typename?: 'ZtnaNetworkConnection';
  /** A list of ZtnaNetworkItems edges */
  edges: Array<Maybe<ZtnaNetworkEdge>>;
  /** Information to aid in pagination */
  pageInfo: PageInfo;
};

/** Gateway network Edge type definition */
export type ZtnaNetworkEdge = Edge & {
  __typename?: 'ZtnaNetworkEdge';
  /** The cursor for the edge */
  cursor: Scalars['String']['output'];
  /** The node with the payload */
  node: ZtnaNetworkItem;
};

/** Gateway network item type definition */
export type ZtnaNetworkItem = {
  __typename?: 'ZtnaNetworkItem';
  /** Date of network session in the format yyyy-MM-dd hh:mm:ssZ */
  date: Scalars['DateTime']['output'];
  /** Email where the network sesion originated from */
  email: Scalars['String']['output'];
};

/** ZtnaSessionItems Connection type definition */
export type ZtnaSessionConnection = {
  __typename?: 'ZtnaSessionConnection';
  /** A list of ZtnaNetworkItems edges */
  edges: Array<Maybe<ZtnaSessionEdge>>;
  /** Information to aid in pagination */
  pageInfo: PageInfo;
};

/** Ztna session edge type definition */
export type ZtnaSessionEdge = Edge & {
  __typename?: 'ZtnaSessionEdge';
  /** The cursor for the edge */
  cursor: Scalars['String']['output'];
  /** The node with the payload */
  node: ZtnaSessionItem;
};

/** Gateway network item type definition */
export type ZtnaSessionItem = {
  __typename?: 'ZtnaSessionItem';
  /** Email where the network sesion originated from */
  email: Scalars['String']['output'];
  /** End date of network session in the format yyyy-MM-dd hh:mm:ssZ */
  endSessionDate: Scalars['DateTime']['output'];
  /** Start date of network session in the format yyyy-MM-dd hh:mm:ssZ */
  startSessionDate: Scalars['DateTime']['output'];
};

export type GetGroupFindingsQueryVariables = Exact<{
  groupName: Scalars['String']['input'];
}>;


export type GetGroupFindingsQuery = { __typename?: 'Query', group: { __typename?: 'Group', findings: Array<{ __typename?: 'Finding', description: string, id: string, lastVulnerability: number | null, maxOpenSeverityScore: number, maxOpenSeverityScoreV4: number, status: string, title: string, totalOpenCVSSF: number, totalOpenCVSSFV4: number } | null> | null } };

export type GetMeQueryVariables = Exact<{ [key: string]: never; }>;


export type GetMeQuery = { __typename?: 'Query', me: { __typename?: 'Me', userEmail: string, userName: string, organizations: Array<{ __typename?: 'Organization', name: string, groups: Array<{ __typename?: 'Group', name: string }> }> } };

export type GetLinkedVulnerabilitiesQueryVariables = Exact<{
  groupName: Scalars['String']['input'];
  issueURL: Scalars['String']['input'];
}>;


export type GetLinkedVulnerabilitiesQuery = { __typename?: 'Query', group: { __typename?: 'Group', vulnerabilities: { __typename?: 'VulnerabilitiesConnection', edges: Array<{ __typename?: 'VulnerabilityEdge', node: { __typename?: 'Vulnerability', id: string, specific: string, state: VulnerabilityState, verification: string | null, where: string, finding: { __typename?: 'Finding', groupName: string, id: string, title: string } } } | null> } } };

export type GetGroupVulnerabilitiesQueryVariables = Exact<{
  after?: InputMaybe<Scalars['String']['input']>;
  groupName: Scalars['String']['input'];
  search?: InputMaybe<Scalars['String']['input']>;
}>;


export type GetGroupVulnerabilitiesQuery = { __typename?: 'Query', group: { __typename?: 'Group', vulnerabilities: { __typename?: 'VulnerabilitiesConnection', edges: Array<{ __typename?: 'VulnerabilityEdge', node: { __typename?: 'Vulnerability', externalBugTrackingSystem: string | null, id: string, reportDate: string | null, severityTemporalScore: number, severityThreatScore: number, specific: string, where: string, finding: { __typename?: 'Finding', description: string, groupName: string, id: string, title: string, totalOpenCVSSF: number, totalOpenCVSSFV4: number } } } | null>, pageInfo: { __typename?: 'PageInfo', endCursor: string, hasNextPage: boolean } } } };

export type RequestReattackMutationVariables = Exact<{
  findingId: Scalars['String']['input'];
  justification: Scalars['String']['input'];
  vulnerabilities: Array<InputMaybe<Scalars['String']['input']>> | InputMaybe<Scalars['String']['input']>;
}>;


export type RequestReattackMutation = { __typename?: 'Mutation', requestVulnerabilitiesVerification: { __typename?: 'SimplePayload', success: boolean } };

export type UpdateVulnerabilityIssueMutationVariables = Exact<{
  issueURL: Scalars['String']['input'];
  vulnerabilityId: Scalars['ID']['input'];
  webhookURL?: InputMaybe<Scalars['String']['input']>;
}>;


export type UpdateVulnerabilityIssueMutation = { __typename?: 'Mutation', updateVulnerabilityIssue: { __typename?: 'SimplePayload', success: boolean } };


export const GetGroupFindingsDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"GetGroupFindings"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"groupName"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"group"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"groupName"},"value":{"kind":"Variable","name":{"kind":"Name","value":"groupName"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"findings"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"description"}},{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"lastVulnerability"}},{"kind":"Field","name":{"kind":"Name","value":"maxOpenSeverityScore"}},{"kind":"Field","name":{"kind":"Name","value":"maxOpenSeverityScoreV4"}},{"kind":"Field","name":{"kind":"Name","value":"status"}},{"kind":"Field","name":{"kind":"Name","value":"title"}},{"kind":"Field","name":{"kind":"Name","value":"totalOpenCVSSF"}},{"kind":"Field","name":{"kind":"Name","value":"totalOpenCVSSFV4"}}]}}]}}]}}]} as unknown as DocumentNode<GetGroupFindingsQuery, GetGroupFindingsQueryVariables>;
export const GetMeDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"GetMe"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"me"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"organizations"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"groups"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"name"}}]}},{"kind":"Field","name":{"kind":"Name","value":"name"}}]}},{"kind":"Field","name":{"kind":"Name","value":"userEmail"}},{"kind":"Field","name":{"kind":"Name","value":"userName"}}]}}]}}]} as unknown as DocumentNode<GetMeQuery, GetMeQueryVariables>;
export const GetLinkedVulnerabilitiesDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"GetLinkedVulnerabilities"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"groupName"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"issueURL"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"group"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"groupName"},"value":{"kind":"Variable","name":{"kind":"Name","value":"groupName"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"vulnerabilities"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"externalBugTrackingSystem"},"value":{"kind":"Variable","name":{"kind":"Name","value":"issueURL"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"edges"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"node"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"finding"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"groupName"}},{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"title"}}]}},{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"specific"}},{"kind":"Field","name":{"kind":"Name","value":"state"}},{"kind":"Field","name":{"kind":"Name","value":"verification"}},{"kind":"Field","name":{"kind":"Name","value":"where"}}]}}]}}]}}]}}]}}]} as unknown as DocumentNode<GetLinkedVulnerabilitiesQuery, GetLinkedVulnerabilitiesQueryVariables>;
export const GetGroupVulnerabilitiesDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"GetGroupVulnerabilities"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"after"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"groupName"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"search"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"group"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"groupName"},"value":{"kind":"Variable","name":{"kind":"Name","value":"groupName"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"vulnerabilities"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"after"},"value":{"kind":"Variable","name":{"kind":"Name","value":"after"}}},{"kind":"Argument","name":{"kind":"Name","value":"first"},"value":{"kind":"IntValue","value":"30"}},{"kind":"Argument","name":{"kind":"Name","value":"search"},"value":{"kind":"Variable","name":{"kind":"Name","value":"search"}}},{"kind":"Argument","name":{"kind":"Name","value":"state"},"value":{"kind":"EnumValue","value":"VULNERABLE"}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"edges"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"node"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"externalBugTrackingSystem"}},{"kind":"Field","name":{"kind":"Name","value":"finding"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"description"}},{"kind":"Field","name":{"kind":"Name","value":"groupName"}},{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"title"}},{"kind":"Field","name":{"kind":"Name","value":"totalOpenCVSSF"}},{"kind":"Field","name":{"kind":"Name","value":"totalOpenCVSSFV4"}}]}},{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"reportDate"}},{"kind":"Field","name":{"kind":"Name","value":"severityTemporalScore"}},{"kind":"Field","name":{"kind":"Name","value":"severityThreatScore"}},{"kind":"Field","name":{"kind":"Name","value":"specific"}},{"kind":"Field","name":{"kind":"Name","value":"where"}}]}}]}},{"kind":"Field","name":{"kind":"Name","value":"pageInfo"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"endCursor"}},{"kind":"Field","name":{"kind":"Name","value":"hasNextPage"}}]}}]}}]}}]}}]} as unknown as DocumentNode<GetGroupVulnerabilitiesQuery, GetGroupVulnerabilitiesQueryVariables>;
export const RequestReattackDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"mutation","name":{"kind":"Name","value":"RequestReattack"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"findingId"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"justification"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"vulnerabilities"}},"type":{"kind":"NonNullType","type":{"kind":"ListType","type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"requestVulnerabilitiesVerification"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"findingId"},"value":{"kind":"Variable","name":{"kind":"Name","value":"findingId"}}},{"kind":"Argument","name":{"kind":"Name","value":"justification"},"value":{"kind":"Variable","name":{"kind":"Name","value":"justification"}}},{"kind":"Argument","name":{"kind":"Name","value":"vulnerabilities"},"value":{"kind":"Variable","name":{"kind":"Name","value":"vulnerabilities"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"success"}}]}}]}}]} as unknown as DocumentNode<RequestReattackMutation, RequestReattackMutationVariables>;
export const UpdateVulnerabilityIssueDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"mutation","name":{"kind":"Name","value":"UpdateVulnerabilityIssue"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"issueURL"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"vulnerabilityId"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"ID"}}}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"webhookURL"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"updateVulnerabilityIssue"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"issueURL"},"value":{"kind":"Variable","name":{"kind":"Name","value":"issueURL"}}},{"kind":"Argument","name":{"kind":"Name","value":"vulnerabilityId"},"value":{"kind":"Variable","name":{"kind":"Name","value":"vulnerabilityId"}}},{"kind":"Argument","name":{"kind":"Name","value":"webhookURL"},"value":{"kind":"Variable","name":{"kind":"Name","value":"webhookURL"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"success"}}]}}]}}]} as unknown as DocumentNode<UpdateVulnerabilityIssueMutation, UpdateVulnerabilityIssueMutationVariables>;