import api, { route } from "@forge/api";

interface IPermissionsResponse {
  projectPermissions: {
    permission: string;
    issues: string[];
    projects: number[];
  }[];
  globalPermissions: string[];
}

async function isAdmin(projectId: string): Promise<boolean> {
  const response = await api
    .asUser()
    .requestJira(route`/rest/api/3/permissions/check`, {
      body: JSON.stringify({
        globalPermissions: ["ADMINISTER"],
        projectPermissions: [
          {
            permissions: ["ADMINISTER_PROJECTS"],
            projects: [Number(projectId)],
          },
        ],
      }),
      headers: { "Content-Type": "application/json" },
      method: "POST",
    });
  const { globalPermissions, projectPermissions } =
    (await response.json()) as IPermissionsResponse;
  const isGlobalAdmin = globalPermissions.includes("ADMINISTER");
  const isProjectAdmin =
    projectPermissions[0].projects[0] === Number(projectId);

  return isGlobalAdmin || isProjectAdmin;
}

export { isAdmin };
