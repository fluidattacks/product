# shellcheck shell=bash

function export_secrets {
  local secrets=(
    FORGE_API_TOKEN
    FORGE_EMAIL
  )

  : && aws_login "dev" "3600" \
    && sops_export_vars integrates/secrets/dev.yaml "${secrets[@]}" \
    || return 1
}

function start_dev_server {
  local vite_pid

  # https://github.com/koalaman/shellcheck/issues/2743
  # shellcheck disable=SC2064
  : && { npx vite & } \
    && vite_pid=$! \
    && trap "kill -- -${vite_pid}" EXIT
}

function main {
  export FORGE_USER_VAR_CI_COMMIT_REF_NAME="${CI_COMMIT_REF_NAME}"
  export FORGE_APP_KEY="com.fluidattacks.integrates-jira-${CI_COMMIT_REF_NAME}"
  export FORGE_FLUID_HOST="https://${CI_COMMIT_REF_NAME}.app.fluidattacks.com"

  : && export_secrets \
    && pushd integrates/jira \
    && npm install \
    && npx forge settings set usage-analytics false \
    && sleep 1 \
    && start_dev_server \
    && npx forge tunnel \
      --environment "${CI_COMMIT_REF_NAME}" \
    && popd \
    || return 1
}

main "$@"
