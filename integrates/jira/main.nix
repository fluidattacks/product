{ inputs, makeScript, outputs, ... }:
makeScript {
  entrypoint = ./entrypoint.sh;
  name = "integrates-jira";
  persistState = true;
  searchPaths = {
    bin = [
      inputs.nixpkgs.bash
      inputs.nixpkgs.git
      inputs.nixpkgs.gnutar
      inputs.nixpkgs.gzip
      inputs.nixpkgs.ngrok
      inputs.nixpkgs.nodejs_20
      inputs.nixpkgs.procps
    ];
    source = [ outputs."/common/utils/aws" outputs."/common/utils/sops" ];
  };
}
