{ inputs, makeScript, ... }:
makeScript {
  entrypoint = ./entrypoint.sh;
  name = "integrates-jira-types";
  searchPaths = {
    bin = [ inputs.nixpkgs.bash inputs.nixpkgs.git inputs.nixpkgs.nodejs_20 ];
  };
}
