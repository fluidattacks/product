{ inputs, makeScript, outputs, ... }:
makeScript {
  entrypoint = ./entrypoint.sh;
  name = "integrates-jira-lint";
  searchPaths.bin =
    [ inputs.nixpkgs.bash inputs.nixpkgs.git inputs.nixpkgs.nodejs_20 ];
}
