import type { CodegenConfig } from "@graphql-codegen/cli";

const config: Readonly<CodegenConfig> = {
  documents: "src/back/model/*.ts",
  generates: {
    "src/back/gql/": {
      config: {
        avoidOptionals: {
          defaultValue: true,
          field: true,
          inputValue: false,
        },
        scalars: {
          DateTime: "string",
          GenericScalar: "unknown",
          JSONString: "string",
        },
      },
      plugins: [],
      preset: "client",
      presetConfig: {
        fragmentMasking: false,
      },
    },
  },
  overwrite: true,
  schema: "./../back/integrates/api",
};

// eslint-disable-next-line import/no-default-export
export default config;
