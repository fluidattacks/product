/* eslint-disable @typescript-eslint/no-magic-numbers */
/* eslint-disable cypress/no-unnecessary-waiting */
const BRANCH = Cypress.env("CI_COMMIT_REF_NAME") as string;
const APP_NAME =
  BRANCH === "trunk" ? "Fluid Attacks" : `Fluid Attacks - ${BRANCH}`;

describe("Main workflow", () => {
  beforeEach(() => {
    cy.loginToJira();
    cy.visit("/browse/TESTING");
  });

  it("should show project page welcome", () => {
    cy.get("div[aria-label='Apps']").within(() => {
      cy.get("a").contains(APP_NAME).first().click({ force: true });
    });
    cy.enter("[data-testid='hosted-resources-iframe']").then((getBody) => {
      getBody().find("div#root").should("be.visible").and("contain", "Welcome");
    });
    cy.screenshot();
  });

  it("should show issue panel welcome", () => {
    cy.visit("/jira/software/projects/TESTING/issues/TESTING-1");
    cy.get(
      "[data-testid='issue-view-layout-templates-views.ui.context.visible-hidden.ui.context-items']",
    ).within(() => {
      cy.get("summary").contains(APP_NAME).click();
    });
    cy.enter("[data-testid='hosted-resources-iframe']").then((getBody) => {
      getBody().find("div#root").should("be.visible").and("contain", "Welcome");
    });
    cy.screenshot();
  });

  it("should show settings page", () => {
    cy.visit("/jira/software/projects/TESTING/settings/apps");
    cy.get("a").contains(APP_NAME).first().click();
    cy.enter("[data-testid='hosted-resources-iframe']").then((getBody) => {
      getBody().find("div#root").should("be.visible").and("contain", "Welcome");
      getBody()
        .find("input[name='token']")
        .type(Cypress.env("TEST_RETRIEVES_TOKEN") as string, { log: false });
      getBody().find("button").contains("Connect").click();
      getBody()
        .find("div[class*='alert']")
        .should("contain", "Authenticated as  (integratesuser@gmail.com)");
      cy.wait(10000);
      getBody().find("p").contains("Choose a group").click();
      getBody().find("ul > li[value='unittesting']").click();
      getBody().find("button").contains("Save").click();
      cy.get("div").should("contain", "Settings saved.");
    });
    cy.screenshot();
  });

  it("should show project page", () => {
    cy.get("div[aria-label='Apps']").within(() => {
      cy.get("a").contains(APP_NAME).first().click({ force: true });
    });
    cy.enter("[data-testid='hosted-resources-iframe']").then((getBody) => {
      getBody().find("p").should("contain", "Vulnerabilities");
      getBody().find("td").should("contain", "038. Business information leak");
      getBody().find("button").contains("Create").first().click();
      cy.get("form[id='issue-create.ui.modal.create-form']").should("exist");
      cy.get("input[id='summary-field']").should(
        "have.value",
        "038. Business information leak",
      );
      cy.get("div[id='description-container']")
        .should("contain", "Se obtiene información de negocio")
        .and("contain", "Learn more")
        .and("contain", "View on platform");
    });
    cy.screenshot();
  });

  it("should show issue panel", () => {
    cy.visit("/jira/software/projects/TESTING/issues/TESTING-1");
    cy.get(
      "[data-testid='issue-view-layout-templates-views.ui.context.visible-hidden.ui.context-items']",
    ).within(() => {
      cy.get("summary").contains(APP_NAME).click();
    });
    cy.enter("[data-testid='hosted-resources-iframe']").then((getBody) => {
      getBody()
        .find("p")
        .should("contain", "There are no vulnerabilities linked to this issue");
    });
    cy.screenshot();
  });
});
