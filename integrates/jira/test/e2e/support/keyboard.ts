function configureKeyboard(): void {
  Cypress.Keyboard.defaults({ keystrokeDelay: 0 });
}

export { configureKeyboard };
