import { configureCustomCommands, overrideCommands } from "./commands";
import { manageExceptions } from "./exceptions";
import { configureKeyboard } from "./keyboard";

configureCustomCommands();
overrideCommands();
manageExceptions();
configureKeyboard();
