# For more information visit:
# https://github.com/fluidattacks/makes
{ fetchNixpkgs, ... }:
let
  repository = builtins.fetchTarball {
    url =
      "https://github.com/zaninime/sbt-derivation/archive/6762cf2c31de50efd9ff905cbcc87239995a4ef9.tar.gz";
    sha256 = "sha256:0g9dzw734k4qhvc4h88zjbrxdiz6g8kgq7qgbac8jgj8cvns6xry";
  };
  sbt-derivation = import "${repository}/overlay.nix";
  nixpkgs = fetchNixpkgs {
    rev = "8d08b3a8432cfe6e5f76a70aac567d900a347ad7";
    sha256 = "sha256:0kvjln265anp6g5nzj4h7shlcpcy8j3xh825668asi698h104b2h";
    overlays = [
      (_: super: {
        # Nginx by default tries to use directories owned by root
        # We have to recompile it pointing to the user-space
        nginxLocal = super.nginx.overrideAttrs (attrs: {
          configureFlags = attrs.configureFlags ++ [
            "--error-log-path=/tmp/error.log"
            "--http-client-body-temp-path=/tmp/nginx_client_body"
            "--http-fastcgi-temp-path=/tmp/nginx_fastcgi"
            "--http-log-path=/tmp/access.log"
            "--http-proxy-temp-path=/tmp/nginx_proxy"
            "--http-scgi-temp-path=/tmp/nginx_scgi"
            "--http-uwsgi-temp-path=/tmp/nginx_uwsgi"
          ];
        });
      })
      sbt-derivation
    ];
  };
in {
  cache = {
    readNixos = true;
    extra = {
      fluidattacks = {
        enable = true;
        pubKey =
          "fluidattacks.cachix.org-1:upiUCP8kWnr7NxVSJtTOM+SBqL0pZhZnUoqPG04sBv0=";
        token = "CACHIX_AUTH_TOKEN";
        type = "cachix";
        url = "https://fluidattacks.cachix.org";
        write = true;
      };
    };
  };
  extendingMakesDirs = [ "/" ];
  formatBash = {
    enable = true;
    targets = [ "/" ];
  };
  formatMarkdown = {
    enable = true;
    doctocArgs = [ "--title" "# Contents" ];
    targets = [ "/skims/LICENSE.md" ];
  };
  formatNix = {
    enable = true;
    targets = [ "/" ];
  };
  formatTerraform = {
    enable = true;
    targets = [ "/" ];
  };
  formatYaml = {
    enable = true;
    targets = [ "/" ];
  };
  lintBash = {
    enable = true;
    targets = [ "/" ];
  };
  lintGitMailMap = {
    enable = true;
    exclude = "^.+<.+@noreply.gitlab.com>$";
  };
  lintNix = {
    enable = true;
    targets = [ "/" ];
  };
  lintTerraform = { config = "/.lint-terraform.hcl"; };
  imports = [
    ./airs/makes.nix
    ./common/makes.nix
    ./forces/makes.nix
    ./integrates/makes.nix
    ./matches/makes.nix
    ./melts/makes.nix
    ./observes/makes.nix
    ./retrieves/makes.nix
    ./sifts/makes.nix
    ./skims/makes.nix
    ./sorts/makes.nix
  ];
  inputs = {
    inherit nixpkgs;
    nix-filter = let
      src = builtins.fetchTarball {
        url =
          "https://github.com/numtide/nix-filter/archive/fc282c5478e4141842f9644c239a41cfe9586732.tar.gz";
        sha256 = "sha256:155cmq1w8s5v2l4d5zlhbph8r4fh0k2cl503z94ma7yizmmx9ll5";
      };
    in import src;
    flakeAdapter = import (builtins.fetchTarball {
      url =
        "https://github.com/edolstra/flake-compat/archive/12c64ca55c1014cdc1b16ed5a804aa8576601ff2.tar.gz";
      sha256 = "0jm6nzb83wa6ai17ly9fzpqc40wg1viib8klq8lby54agpl213w5";
    });
    makeImpureCmd = { cmd, path, }:
      nixpkgs.runCommandLocal "${cmd}-impure" {
        __impureHostDeps = [ path ];
      } ''
        if ! [ -x ${path} ]; then
          echo Cannot find command ${path}
          exit 1
        fi
        mkdir -p $out/bin
        ln -s ${path} $out/bin
        manpage="/usr/share/man/man1/${cmd}.1"
        if [ -f $manpage ]; then
          mkdir -p $out/share/man/man1
          ln -s $manpage $out/share/man/man1
        fi
      '';
  };
  testLicense = { enable = true; };
}
