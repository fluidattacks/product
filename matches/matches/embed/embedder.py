import logging
import threading
from collections.abc import Awaitable, Callable

from voyageai.client_async import AsyncClient as VoyageAsyncClient

from matches.context import VOYAGE_API_KEY

logging.getLogger("voyage").setLevel(logging.WARNING)

GetClientCallable = Callable[[], Awaitable[VoyageAsyncClient]]

EmbeddingContext = tuple[GetClientCallable]


_EMBEDDING_MODEL = "voyage-3"


class Embedder:
    _instance = None
    _lock = threading.Lock()

    def __new__(cls) -> None:  # type: ignore[misc]
        if cls._instance is None:
            with cls._lock:
                if not cls._instance:
                    cls._instance = super().__new__(cls)
        return cls._instance  # type: ignore[return-value]

    def __init__(self) -> None:
        self.client = VoyageAsyncClient(api_key=VOYAGE_API_KEY)

    async def embed(self, data: list[str]) -> list[list[float]] | list[list[int]]:
        res = await self.client.embed(data, model=_EMBEDDING_MODEL)
        return res.embeddings
