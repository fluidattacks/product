import logging
from collections.abc import Awaitable, Callable
from pathlib import Path
from typing import cast

from pymilvus.milvus_client import AsyncMilvusClient

from matches.context import MATCHES_BUCKET
from matches.exceptions import FileDownloadError
from matches.utils.aws import download_from_s3, upload_file_to_s3

LOGGER = logging.getLogger()

_DB_REMOTE_PATH = "matches/criteria_embeddings/"
_DB_NAME = "criteria.db"

StartupCallable = Callable[[], Awaitable[None]]
ShutdownCallable = Callable[[], Awaitable[None]]
PostCallable = Callable[[], Awaitable[None]]
GetClientCallable = Callable[[], Awaitable[AsyncMilvusClient]]

DbContext = tuple[StartupCallable, ShutdownCallable, PostCallable, GetClientCallable]


def create_db_context(db_name: str) -> DbContext:
    client: AsyncMilvusClient | None = None

    async def _fetch_meta() -> None:
        try:
            await download_from_s3(
                bucket=MATCHES_BUCKET,
                object_key=f"{_DB_REMOTE_PATH}{db_name}",
                download_path=Path(db_name),
            )
        except FileDownloadError:
            LOGGER.info(
                "Remote db object not found. A new instance will be created on post action."
            )

    async def _startup() -> None:
        nonlocal client
        await _fetch_meta()

        client = AsyncMilvusClient(db_name)

    async def _shutdown() -> None:
        if client is not None:
            await client.close()

    async def _get_client() -> AsyncMilvusClient:
        if client is None:
            await db_startup()
        return cast(AsyncMilvusClient, client)

    async def _post_checkpoint() -> None:
        await upload_file_to_s3(
            bucket=MATCHES_BUCKET,
            obj_prefix=_DB_REMOTE_PATH,
            object_key=db_name,
            file_path=db_name,
        )

    return _startup, _shutdown, _post_checkpoint, _get_client


db_startup, db_shutdown, post_db_checkpoint, get_db_client = create_db_context(_DB_NAME)
