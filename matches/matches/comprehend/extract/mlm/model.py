import logging
import tarfile
from pathlib import Path
from typing import cast

from transformers.modeling_utils import PreTrainedModel
from transformers.models.auto.modeling_auto import (
    AutoModelForSequenceClassification,
)

from matches.context import MATCHES_BUCKET
from matches.exceptions import FileDownloadError, ModelFetchingError
from matches.utils.aws import download_from_s3

LOGGER = logging.getLogger(__name__)
TARGET_MODEL = "column-classification-28-02-2025-v1"
LOCAL_MODELS_PATH = Path("models")


def _is_member_safe(
    member: tarfile.TarInfo,
) -> bool:
    max_member_size_megs = 512
    one_meg = 1024 * 1024
    return not (
        member.issym()
        or member.islnk()
        or Path(member.name).is_absolute()
        or "../" in member.name
        or member.size > max_member_size_megs * one_meg
    )


def _safe_extract_tar(tar_handler: tarfile.TarFile, file_path: Path) -> None:
    for member in tar_handler.getmembers():
        if not _is_member_safe(member):
            LOGGER.error("Unsafe path detected: %s", member.name)
            continue
        tar_handler.extract(member, path=file_path, numeric_owner=True)
        LOGGER.info("Extracted: %s", member.name)


async def fetch_model() -> None:
    default_extension = "tar.gz"
    LOCAL_MODELS_PATH.mkdir(parents=True, exist_ok=True)
    download_path = LOCAL_MODELS_PATH / f"{TARGET_MODEL}.{default_extension}"
    extract_path = LOCAL_MODELS_PATH / TARGET_MODEL
    obj_full_key = f"matches/{download_path!s}"

    try:
        await download_from_s3(MATCHES_BUCKET, obj_full_key, download_path)

        with tarfile.open(download_path, mode="r:gz") as gz:
            _safe_extract_tar(gz, file_path=extract_path)
    except FileDownloadError as ex:
        LOGGER.exception("Error downloading model: %s", TARGET_MODEL)
        raise ModelFetchingError(TARGET_MODEL) from ex


async def load_model() -> PreTrainedModel:
    model = LOCAL_MODELS_PATH / TARGET_MODEL
    if not model.exists():
        await fetch_model()
        LOGGER.info("Model %s downloaded and extracted.", TARGET_MODEL)

    return cast(
        PreTrainedModel,
        AutoModelForSequenceClassification.from_pretrained(str(model)),
    )
