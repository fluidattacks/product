import asyncio
import os
import tarfile
from pathlib import Path

from datasets import Dataset
from pandas import concat, read_csv
from transformers.models.auto.modeling_auto import AutoModelForSequenceClassification
from transformers.models.auto.tokenization_auto import AutoTokenizer
from transformers.trainer import Trainer
from transformers.training_args import TrainingArguments

from matches.comprehend.constants import LABEL_MAPPINGS, N_CLASSES
from matches.context import MATCHES_BUCKET, MODELS_REMOTE_PATH
from matches.utils.aws.s3 import upload_file_to_s3

PRETRAINED_BASE_MODEL = "distilbert-base-uncased"
TARGET_MODEL = "column-classification-28-02-2025-v1"


async def load_merged_dataset() -> Dataset:
    labeled_dataframe = concat(
        [
            read_csv(
                Path("data/train/labeled") / file, sep=";", header=None, names=["text", "label"]
            )
            for file in os.listdir("data/train/labeled")
        ]
    )
    return Dataset.from_pandas(labeled_dataframe).map(
        lambda x: {"label": LABEL_MAPPINGS[x["label"]]}
    )


async def train(model_path: Path) -> None:
    dataset = await load_merged_dataset()
    tokenizer = AutoTokenizer.from_pretrained(PRETRAINED_BASE_MODEL)
    tokenized_datasets = dataset.map(
        lambda x: tokenizer(x["text"], padding="max_length", truncation=True), batched=True
    )

    model = AutoModelForSequenceClassification.from_pretrained(
        PRETRAINED_BASE_MODEL, num_labels=N_CLASSES
    )

    training_args = TrainingArguments(
        output_dir="./results",
        evaluation_strategy="epoch",
        learning_rate=2e-5,
        per_device_train_batch_size=16,
        per_device_eval_batch_size=16,
        num_train_epochs=10,
        weight_decay=0.01,
    )

    trainer = Trainer(
        model=model,
        args=training_args,
        train_dataset=tokenized_datasets,
        eval_dataset=tokenized_datasets,
    )
    model.save_pretrained(model_path)

    trainer.train()


async def upload_model(model_path: Path) -> None:
    with tarfile.open(model_path.with_suffix(".tar.gz"), "w:gz") as tar_ref:
        tar_ref.add(model_path, arcname=model_path.name)

    await upload_file_to_s3(
        bucket=MATCHES_BUCKET,
        obj_prefix=MODELS_REMOTE_PATH,
        object_key=TARGET_MODEL,
        file_path=str(model_path.with_suffix(".zip")),
    )


async def main() -> None:
    target_model_path = Path(TARGET_MODEL)
    await train(target_model_path)
    await upload_model(target_model_path)


if __name__ == "__main__":
    asyncio.run(main())
