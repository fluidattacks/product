import asyncio
from typing import NamedTuple

import torch
from transformers.models.auto.tokenization_auto import (
    AutoTokenizer,
)

from matches.comprehend.constants import LABEL_MAPPINGS

from .model import load_model


async def _a_predict(model: torch.nn.Module, inputs: torch.Tensor) -> tuple[str, torch.Tensor]:
    return await asyncio.to_thread(model, **inputs)  # type: ignore[arg-type]


class Prediction(NamedTuple):
    label: str
    probabilities: torch.Tensor


async def predict_column(text: str) -> Prediction:
    device = torch.device("mps")
    model = await load_model()
    model.to(device)  # type: ignore[arg-type]
    tokenizer = AutoTokenizer.from_pretrained("distilbert-base-uncased")
    inputs = tokenizer(text, return_tensors="pt", padding="max_length", truncation=True).to(device)
    with torch.no_grad():
        outputs = await _a_predict(model, inputs)
        probs = torch.nn.functional.softmax(outputs.logits, dim=-1)  # type: ignore[attr-defined]
        predicted = list(LABEL_MAPPINGS.keys())[int(probs.argmax().item())]
        return Prediction(label=predicted, probabilities=probs)
