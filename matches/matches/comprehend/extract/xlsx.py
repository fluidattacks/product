import asyncio
import logging
import sys
from pathlib import Path
from typing import NamedTuple

import pandas as pd

from matches.comprehend.extract.mlm.extraction import predict_column

LOGGER = logging.getLogger(__name__)

MAX_COLUMNS_TO_PROCESS = 128
COL_N_SAMPLES = 10


def build_str_from_samples(samples: list[str]) -> str:
    text_samples = " || ".join(samples[1:])
    return f"{samples[0]}: {text_samples}".replace(";", "").replace("\n", ". ")


class ColSample(NamedTuple):
    sheet: int | str
    column: str
    text: str
    raw_cols: list[str]


def retrieve_column_samples(file_path: Path) -> list[ColSample]:
    xls = pd.ExcelFile(file_path)
    column_samples: list[ColSample] = []
    for sheet_name in xls.sheet_names:
        dataframe = pd.read_excel(xls, sheet_name=sheet_name, header=None)
        if len(dataframe.columns) > MAX_COLUMNS_TO_PROCESS:
            LOGGER.warning(
                "Number of columns in %s::%s is greater than %d. "
                "Only first %d columns will be processed.",
                file_path,
                sheet_name,
                MAX_COLUMNS_TO_PROCESS,
                MAX_COLUMNS_TO_PROCESS,
            )
            dataframe = dataframe.iloc[:, :MAX_COLUMNS_TO_PROCESS]
        for col in dataframe.columns:
            dropped = dataframe[col].dropna()
            samples = dropped.astype(str).head(min(COL_N_SAMPLES, len(dropped))).tolist()
            if len(samples) > 1:
                column_samples.append(
                    ColSample(
                        sheet=sheet_name,
                        column=col,
                        text=build_str_from_samples(samples),
                        raw_cols=dropped.astype(str).tolist()[1:],
                    )
                )
    return column_samples


def filter_predictions(
    *,
    samples: list[ColSample],
    predicted_labels: list[str],
    target_labels: set[str],
) -> set[str]:
    filtered_columns = []
    for sample, prediction in zip(samples, predicted_labels, strict=False):
        if prediction in target_labels:
            filtered_columns.extend(sample.raw_cols)
    return set(filtered_columns)


async def extract(file: Path) -> set[str]:
    samples = retrieve_column_samples(file)
    predictions = [await predict_column(sample.text) for sample in samples]
    target_labels = {"TT", "TD"}
    return filter_predictions(
        samples=samples,
        predicted_labels=[pred.label for pred in predictions],
        target_labels=target_labels,
    )


async def main() -> None:
    logging.basicConfig(level=logging.INFO)
    target = sys.argv[1]
    file = Path(target)
    if not file.exists():
        LOGGER.error("File %s does not exist.", file)
        sys.exit(1)
    filtered = await extract(file)
    LOGGER.info("\n\nFiltered columns:\n\n>> %s", "\n\n>> ".join(filtered))


def run() -> None:
    asyncio.run(main())
