from io import StringIO
from typing import Literal

import aiofiles

from matches.context import EXTRACTION_SYSTEM_PROMPT_PATH
from matches.exceptions import InvalidResponseLengthError
from matches.utils.aws import ConverseMessage, converse

MAX_COLUMN_REQUEST_LEN = 5
CONVERSE_MAX_TOKENS = 4096

type ConverseRole = Literal[
    "assistant",
    "user",
    "system",
]


def build_str_from_samples(samples: list[str]) -> str:
    text_samples = " || ".join(samples[1:])
    return f"{samples[0]}: {text_samples}".replace(";", "").replace("\n", ". ")


def parse_content(columns: list[str]) -> str:
    sep_seq = "\n\n\n>>>>>>>"
    return sep_seq + sep_seq.join(columns)


async def load_system_promt() -> str:
    async with aiofiles.open(EXTRACTION_SYSTEM_PROMPT_PATH) as file:
        return await file.read()


async def predict_columns(columns: list[str]) -> list[str]:
    if len(columns) > MAX_COLUMN_REQUEST_LEN:
        mid = len(columns) // 2
        first_half = await predict_columns(columns[:mid])
        second_half = await predict_columns(columns[mid:])
        return first_half + second_half

    string_buffer = StringIO()
    content = parse_content(columns)
    async for line in converse(
        messages=(
            ConverseMessage(
                role="system",
                content=await load_system_promt(),
            ),
            ConverseMessage(role="user", content=content),
        ),
        temperature=0.2,
    ):
        string_buffer.write(line)
    values = string_buffer.getvalue().split(",")
    if len(values) != len(columns):
        msg = (
            f"The number of values in response {len(values)} does not match the "
            f"number of columns {len(columns)}. "
            f"Raw input: {content}. "
            f"Raw response: {string_buffer.getvalue()}"
        )
        raise InvalidResponseLengthError(msg)
    return values
