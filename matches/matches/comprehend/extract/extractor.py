from collections.abc import Callable, Coroutine
from pathlib import Path
from typing import Any, Literal

from .xlsx import extract as xlsx_extract

ExtractionTarget = Literal["multi_sheet_excel", "pdf", "csv", "json"]


def get_extractor(target: ExtractionTarget) -> Callable[[Path], Coroutine[Any, Any, set[str]]]:
    match target:
        case "multi_sheet_excel":
            return xlsx_extract
        case _:  # More extractors will be supported in the future
            error_message = f"Extractor for target {target} not implemented"
            raise NotImplementedError(error_message)
