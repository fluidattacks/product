from collections.abc import (
    AsyncGenerator,
)
from typing import TYPE_CHECKING, Literal, NamedTuple, TypedDict

from matches.context import EXTRACTION_MODEL_ID, REGION

from .session import SESSION

if TYPE_CHECKING:
    from types_aiobotocore_bedrock_runtime.type_defs import (
        InferenceConfigurationTypeDef,
        MessageTypeDef,
        SystemContentBlockTypeDef,
    )

MAX_COLUMN_REQUEST_LEN = 5
CONVERSE_MAX_TOKENS = 4096

type ConverseRole = Literal[
    "assistant",
    "user",
    "system",
]


class ConverseMessage(NamedTuple):
    content: str
    role: ConverseRole


class Tokens(TypedDict):
    inputTokens: int
    outputTokens: int
    totalTokens: int


async def converse(
    messages: tuple[ConverseMessage, ...],
    *,
    temperature: float = 0.8,
) -> AsyncGenerator[str, None]:
    inference_parameters: InferenceConfigurationTypeDef = {
        "temperature": temperature,
        "maxTokens": CONVERSE_MAX_TOKENS,
        "stopSequences": ["\n\nHuman:"],
        "topP": 1.0,
    }
    custom_parameters = {
        "top_k": 250,
    }

    system_messages: list[SystemContentBlockTypeDef] = [
        {"text": message.content} for message in messages if message.role == "system"
    ]
    user_messages: list[MessageTypeDef] = [
        {
            "role": message.role,
            "content": [{"text": message.content}],
        }
        for message in messages
        if message.role != "system"
    ]

    async with SESSION.client(
        service_name="bedrock-runtime",
        region_name=REGION,
    ) as bedrock:
        response = await bedrock.converse_stream(
            modelId=EXTRACTION_MODEL_ID,
            messages=user_messages,
            system=system_messages,
            inferenceConfig=inference_parameters,
            additionalModelRequestFields=custom_parameters,
        )
        async for chunk in response["stream"]:
            if "contentBlockDelta" in chunk:
                yield chunk["contentBlockDelta"]["delta"]["text"]
