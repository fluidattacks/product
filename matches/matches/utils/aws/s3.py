from pathlib import Path

from botocore.exceptions import ClientError

from matches.exceptions import FileDownloadError, FileUploadError

from .session import SESSION


async def download_from_s3(bucket: str, object_key: str, download_path: Path) -> None:
    try:
        async with SESSION.client("s3") as s3_client:
            await s3_client.download_file(bucket, object_key, str(download_path))
    except ClientError as exc:
        msg = f"Error downloading file {object_key} from bucket: {bucket}"
        raise FileDownloadError(msg) from exc


async def upload_file_to_s3(bucket: str, obj_prefix: str, object_key: str, file_path: str) -> None:
    try:
        async with SESSION.client("s3") as client:
            await client.upload_file(file_path, bucket, f"{obj_prefix}{object_key}")
    except ClientError as ex:
        msg = f"Error uploading file to bucket: {bucket}"
        raise FileUploadError(msg) from ex
