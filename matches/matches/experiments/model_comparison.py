import asyncio
import os
import sys
from pathlib import Path

import numpy as np
import pandas as pd
from matplotlib import colormaps
from matplotlib import pyplot as plt
from sklearn.metrics import ConfusionMatrixDisplay, confusion_matrix

from matches.comprehend.constants import LABEL_MAPPINGS
from matches.comprehend.extract.llm.extraction import predict_columns as predict_columns_llm
from matches.comprehend.extract.mlm.extraction import (
    predict_column as predict_column_distilled_mlm,
)


def produce_confusion_matrices(test_dataframe: pd.DataFrame, results_col: str) -> tuple[str, str]:
    cm = confusion_matrix(
        test_dataframe["label"],
        test_dataframe[results_col],
        labels=list(LABEL_MAPPINGS.keys()),
    )

    disp = ConfusionMatrixDisplay(confusion_matrix=cm, display_labels=list(LABEL_MAPPINGS.keys()))
    disp.plot(cmap=colormaps["blues"])
    plt.title(f"Confusion Matrix ({results_col})")
    cm_path = f"{results_col}_confusion_matrix.png"
    plt.savefig(cm_path)

    cm_normalized = cm.astype("float") / cm.sum(axis=1)[:, np.newaxis]

    disp_normalized = ConfusionMatrixDisplay(
        confusion_matrix=cm_normalized,
        display_labels=list(LABEL_MAPPINGS.keys()),
    )
    disp_normalized.plot(cmap=colormaps["blues"])
    plt.title(f"Normalized Confusion Matrix ({results_col})")
    normalized_cm_path = f"normalized_{results_col}_confusion_matrix.png"
    plt.savefig(normalized_cm_path)

    return cm_path, normalized_cm_path


async def run_comparison_experiment() -> None:
    labeled_path = sys.argv[1]
    if not Path(labeled_path).exists():
        msg = f"Path {labeled_path} does not exist."
        raise FileNotFoundError(msg)
    test_dataframe = pd.concat(
        [
            pd.read_csv(
                Path(labeled_path) / file,
                sep=";",
                header=None,
                names=["text", "label"],
            )
            for file in os.listdir(labeled_path)
        ],
    )
    predicted_llm_tag = "predicted_llm"
    predicted_mlm_tag = "predicted_mlm"

    test_dataframe[predicted_llm_tag] = await predict_columns_llm(
        test_dataframe["text"].str.replace('"', "``").tolist(),
    )
    test_dataframe[predicted_mlm_tag] = await asyncio.gather(
        *[
            predict_column_distilled_mlm(text)
            for text in test_dataframe["text"].str.replace('"', "``").tolist()
        ],
    )
    _ = [
        produce_confusion_matrices(test_dataframe, tag)
        for tag in [predicted_llm_tag, predicted_mlm_tag]
    ]
