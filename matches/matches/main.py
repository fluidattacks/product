import asyncio

from matches.experiments.model_comparison import run_comparison_experiment


def run() -> None:
    asyncio.run(run_comparison_experiment())
