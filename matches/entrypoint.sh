# shellcheck shell=bash

function _login_env {
  case "${1:-}" in
    dev) aws_login dev 3600 && sops_export_vars __argSecretsDev__ VOYAGE_API_KEY && export ENVIRONMENT="development" ;;
    prod) aws_login prod 3600 && sops_export_vars __argSecretsProd__ VOYAGE_API_KEY && export ENVIRONMENT="production" ;;
    *) error "Must provide either 'dev' or 'prod' as the second argument" ;;
  esac
}
function _run {
  _login_env "${1:-}"
  poetry run matches "${@:2}"
}

function _embed_criteria {
  _login_env "${1:-}"
  poetry run embed_criteria "${@:2}"
}

function _semantic_compare {
  _login_env "${1:-}"
  poetry run semantic_compare "${@:2}"
}

function _extract {
  _login_env "${1:-}"
  poetry run extract "${@:2}"
}

function _lint {
  if [[ ${CI:-} == "" ]]; then
    poetry run ruff format --config ruff.toml
    poetry run ruff check --config ruff.toml --fix
  else
    poetry run ruff format --config ruff.toml --diff
    poetry run ruff check --config ruff.toml
  fi

  poetry run mypy --config-file mypy.ini matches
}

function _test {
  poetry run pytest -rP matches/test
}

function main {
  local dir="matches"

  pushd "${dir}" || error "${dir} directory not found"
  poetry install
  export EXTRACTION_SYSTEM_PROMPT_PATH='__argExtractionSystemPrompt__'

  case "${1:-}" in
    run) _run "${@:2}" ;;
    embed_criteria) _embed_criteria "${@:2}" ;;
    semantic_compare) _semantic_compare "${@:2}" ;;
    extract) _extract "${@:2}" ;;
    lint) _lint "${@:2}" ;;
    test) _test "${@:2}" ;;
    *) error "Must provide either 'run', 'lint', or 'test' as the second first argument" ;;
  esac
}

main "${@}"
