{ lib, projectPath, ... }:
let
  arch = let
    commit = "eb0bbc4b8548d6d21c4f3be8023bf04e66379037";
    sha256 = "sha256:0m3v4w0nbnfbw5jlj87ab43m38caqzz88nwrmrmyv7wqx4x44xmg";
    url =
      "https://gitlab.com/fluidattacks/universe/-/raw/${commit}/common/ci/arch.nix";
    src = builtins.fetchurl { inherit sha256 url; };
  in import src;

  rules = {
    all = {
      default = arch.core.rules.titleRule {
        products = [ "all" "matches" ];
        types = [ ];
      };
      noRotate = arch.core.rules.titleRule {
        products = [ "all" "matches" ];
        types = [ "feat" "fix" "refac" ];
      };
    };
    ecosystem = {
      default = arch.core.rules.titleRule {
        products = [ "all" "matches" ];
        types = [ ];
      };
    };
  };

in {
  pipelines = {
    matches = {
      gitlabPath = "/matches/pipeline/default.yaml";
      jobs = lib.flatten [
        {
          output = "/pipelineOnGitlab/matches";
          gitlabExtra = arch.extras.default // {
            rules = arch.rules.dev ++ [ rules.all.noRotate ];
            stage = arch.stages.test;
            tags = [ arch.tags.matches ];
          };
        }
        {
          output = "/matches lint";
          gitlabExtra = arch.extras.default // {
            rules = arch.rules.dev ++ [ rules.all.noRotate ];
            stage = arch.stages.test;
            tags = [ arch.tags.matches ];
          };
        }
      ];
    };
  };
}
