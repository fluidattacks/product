{ makeSearchPaths, makeTemplate, outputs, ... }: {
  dev = {
    matches = {
      source = [
        (makeTemplate {
          name = "matches-dev";
          replace = { __argMatchesEnv__ = outputs."/matches/env"; };
          template = ''
            require_env_var AWS_ACCESS_KEY_ID
            require_env_var AWS_SECRET_ACCESS_KEY
            source __argMatchesEnv__/template dev
          '';
        })
        outputs."/common/dev/global_deps"
      ];
    };
  };
}
