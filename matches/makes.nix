{ inputs, makeScript, outputs, projectPath, ... }: {
  dev = { matches = { source = [ outputs."/common/dev/global_deps" ]; }; };
  imports = [ ./pipeline/makes.nix ];
  jobs."/matches" = makeScript {
    name = "matches";
    replace = {
      __argSecretsProd__ = projectPath "/matches/secrets/prod.yaml";
      __argSecretsDev__ = projectPath "/matches/secrets/dev.yaml";
      __argExtractionSystemPrompt__ =
        projectPath "/matches/matches/comprehend/extract/llm/system_prompt.txt";
    };
    searchPaths = {
      bin = [ inputs.nixpkgs.patchelf inputs.nixpkgs.poetry ];
      rpath = [ inputs.nixpkgs.gcc.cc.lib ];
      source = [ outputs."/common/utils/aws" outputs."/common/utils/sops" ];
    };
    entrypoint = ./entrypoint.sh;
  };
}
