config {
  call_module_type = "local"
}
plugin "aws" {
  enabled = true
  deep_check = true
  version = "0.33.0"
  source  = "github.com/terraform-linters/tflint-ruleset-aws"
}
rule "aws_resource_missing_tags" {
  enabled = true
  tags = [
    "Name",
    "fluidattacks:line",
    "fluidattacks:comp",
  ]
  exclude = [
    "aws_elasticache_subnet_group",
    "aws_iam_instance_profile",
    "aws_iam_policy",
  ]
}
