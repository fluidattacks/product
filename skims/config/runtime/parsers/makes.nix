{ inputs, ... }:
let
  tree-sitter = inputs.nixpkgs.python311Packages.tree-sitter.overridePythonAttrs
    (oldAttrs: {
      src = inputs.nixpkgs.fetchFromGitHub {
        owner = "tree-sitter";
        repo = "py-tree-sitter";
        rev = "refs/tags/v0.21.1";
        sha256 = "sha256-U4ZdU0lxjZO/y0q20bG5CLKipnfpaxzV3AFR6fGS7m4=";
        fetchSubmodules = true;
      };

      dependencies = [ inputs.nixpkgs.python311Packages.setuptools ];
    });
in {
  jobs."/skims/config/runtime/parsers" = inputs.nixpkgs.stdenv.mkDerivation {
    buildPhase = ''
      export envTreeSitterCSharp="${inputs.skimsTreeSitterCSharp}"
      export envTreeSitterDart="${inputs.skimsTreeSitterDart}"
      export envTreeSitterGo="${inputs.skimsTreeSitterGo}"
      export envTreeSitterHcl="${inputs.skimsTreeSitterHcl}"
      export envTreeSitterJava="${inputs.skimsTreeSitterJava}"
      export envTreeSitterJavaScript="${inputs.skimsTreeSitterJavaScript}"
      export envTreeSitterJson="${inputs.skimsTreeSitterJson}"
      export envTreeSitterKotlin="${inputs.skimsTreeSitterKotlin}"
      export envTreeSitterPhp="${inputs.skimsTreeSitterPhp}"
      export envTreeSitterPython="${inputs.skimsTreeSitterPython}"
      export envTreeSitterRuby="${inputs.skimsTreeSitterRuby}"
      export envTreeSitterScala="${inputs.skimsTreeSitterScala}"
      export envTreeSitterSwift="${inputs.skimsTreeSitterSwift}"
      export envTreeSitterTypeScript="${inputs.skimsTreeSitterTypeScript}"
      export envTreeSitterYaml="${inputs.skimsTreeSitterYaml}"
      export envTreeSitterGemFileLock="${inputs.sbomTreeSitterGemFileLock}"
      export envTreeSitterMixLock="${inputs.sbomTreeSitterMixLock}"

      python build.py
    '';
    name = "skims-config-runtime-parsers";
    nativeBuildInputs = [ tree-sitter ];
    src = ./.;
  };
}
