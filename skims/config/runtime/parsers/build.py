import json
import os
from pathlib import (
    Path,
)

import tree_sitter

GRAMMARS: dict[str, str] = {
    "c_sharp": os.environ["envTreeSitterCSharp"],
    "go": os.environ["envTreeSitterGo"],
    "java": os.environ["envTreeSitterJava"],
    "javascript": os.environ["envTreeSitterJavaScript"],
    "json": os.environ["envTreeSitterJson"],
    "kotlin": os.environ["envTreeSitterKotlin"],
    "php": os.path.join(os.environ["envTreeSitterPhp"], "php"),
    "python": os.environ["envTreeSitterPython"],
    "ruby": os.environ["envTreeSitterRuby"],
    "tsx": os.path.join(os.environ["envTreeSitterTypeScript"], "tsx"),
    "yaml": os.environ["envTreeSitterYaml"],
    "scala": os.environ["envTreeSitterScala"],
    # Grammars required because not available on pypi as of 12/24
    "dart": os.environ["envTreeSitterDart"],
    "hcl": os.environ["envTreeSitterHcl"],
    "swift": os.environ["envTreeSitterSwift"],
    # Required for SBOM -> Reachability and not available on pypi as of 12/24
    "gemfilelock": os.environ["envTreeSitterGemFileLock"],
    "mix_lock": os.environ["envTreeSitterMixLock"],
}


def get_fields(src: str) -> dict[str, tuple[str, ...]]:
    path: str = os.path.join(src, "src", "node-types.json")
    with Path(path).open(encoding="utf-8") as handle:
        fields: dict[str, tuple[str, ...]] = {
            node["type"]: fields
            for node in json.load(handle)
            for fields in [tuple(node.get("fields", {}))]
            if fields
        }
    return fields


def main() -> None:
    out: str = os.environ["out"]
    os.makedirs(out)

    for grammar, src in GRAMMARS.items():
        if grammar in {"dart", "hcl", "swift", "gemfilelock", "mix_lock"}:
            path = os.path.join(out, f"{grammar}.so")
            tree_sitter.Language.build_library(path, [src])

        if grammar not in {"gemfilelock", "mix_lock"}:
            path = os.path.join(out, f"{grammar}-fields.json")
            with Path(path).open(encoding="utf-8", mode="w") as file:
                json.dump(get_fields(src), file)


if __name__ == "__main__":
    main()
