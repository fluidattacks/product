variable "cache_user_write_password" {
  type = string
}
variable "cache_user_read_password" {
  type = string
}

data "aws_vpc" "main" {
  filter {
    name   = "tag:Name"
    values = ["fluid-vpc"]
  }
}
data "aws_subnet" "main" {
  for_each = toset([
    "batch_main_1",
    "batch_main_2",
    "batch_main_3",
    "batch_main_4",
    "batch_main_5",
  ])

  vpc_id = data.aws_vpc.main.id
  filter {
    name   = "tag:Name"
    values = [each.key]
  }
}
