# shellcheck shell=bash

function score {
  local category="${1-}"
  local extra_flags=("${@:2}")
  local config_file="${PWD}/skims/benchmark/owasp/benchmark_config.yaml"
  local benchmark_local_repo="${PWD}/../owasp_benchmark"
  local benchmark_utils_local_repo="${PWD}/../owasp_benchmark_utils"
  export EXPECTED_RESULTS_CSV="${benchmark_local_repo}/expectedresults-1.2.csv"

  rm -rf "${benchmark_local_repo}"
  echo '[INFO] Downloading OWASP benchmark repo'
  git clone --depth 1 \
    https://github.com/OWASP-Benchmark/BenchmarkJava "${benchmark_local_repo}"

  echo '[INFO] Generating skims results'
  python3.11 '__argSkims__/skims/lib_sast/owasp_benchmark/__init__.py'
  csvstack skims/test/lib_sast/results/benchmark*.csv > "${benchmark_local_repo}/results/Benchmark_1.2-Fluid-Attacks-v2023.csv"

  echo '[INFO] Computing  Benchmark Score Cards with current skims results'
  rm -rf "${benchmark_utils_local_repo}"
  git clone --depth 1 \
    https://github.com/OWASP-Benchmark/BenchmarkUtils "${benchmark_utils_local_repo}"
  pushd "${benchmark_utils_local_repo}" || return 1
  mvn install
  popd || return 1
  pushd "${benchmark_local_repo}" || return 1
  mvn org.owasp:benchmarkutils-maven-plugin:create-scorecard -DconfigFile="${config_file}"
  popd || return 1

}

function main {
  local category="${1-}"
  local extra_flags=("${@:2}")

  score "${category}" "${extra_flags[@]}"
}

main "${@}"
