{ inputs, makeScript, outputs, projectPath, ... }: {
  jobs."/skims/benchmark/owasp" = makeScript {
    replace = { __argSkims__ = projectPath "/skims"; };
    searchPaths = {
      bin = [
        inputs.nixpkgs.python311
        inputs.nixpkgs.csvkit
        inputs.nixpkgs.maven
        outputs."/skims"
      ];
      source = [ outputs."/skims/config/runtime" ];
    };
    name = "skims-benchmark-owasp";
    entrypoint = ./entrypoint.sh;
  };
}
