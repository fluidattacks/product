{ makeScript, outputs, ... }: {
  jobs."/skims/lint" = makeScript {
    name = "skims-lint";
    entrypoint = ''
      pushd skims

      if test -n "''${CI:-}"; then
        ruff format --config ruff.toml --diff
        ruff check --config ruff.toml
      else
        ruff format --config ruff.toml
        ruff check --config ruff.toml --fix
      fi

      lint-imports --config import-linter.cfg
      mypy --config-file mypy.ini skims
      mypy --config-file mypy.ini test
    '';
    searchPaths.source = [ outputs."/skims/config/runtime" ];
  };
}
