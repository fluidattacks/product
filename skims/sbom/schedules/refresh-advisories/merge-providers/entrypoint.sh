# shellcheck shell=bash

function main {
  local providers_with_cache=()

  aws_login "prod_common" "3600" \
    && for provider in $(vunnel list); do
      temp_cache_provider=$(mktemp) \
        && if aws s3 cp --quiet "s3://fluidattacks.public.storage/sbom/grype-bd/${provider}" "${temp_cache_provider}"; then
          grype-db --config __argGrypeConfig__ cache restore --path "${temp_cache_provider}" -p "${provider}" \
            && providers_with_cache+=("-p" "${provider}")
        else
          echo "No data cache found for ${provider}"
        fi
    done \
    && grype-db --config __argGrypeConfig__ cache status \
    && grype-db --config __argGrypeConfig__ build "${providers_with_cache[@]}" \
    && zstd ./build/vulnerability.db \
    && aws s3 cp \
      ./build/vulnerability.db.zst \
      s3://fluidattacks.public.storage/sbom/grype-bd/pure_database.db.zst \
      --acl public-read \
    && rm ./build/vulnerability.db.zst \
    && python3 __argImproveDataQualityScript__ \
    && zstd ./build/vulnerability.db \
    && aws s3 cp \
      ./build/vulnerability.db.zst \
      s3://fluidattacks.public.storage/sbom/ \
      --acl public-read \
    || return 1
}

main "${@}"
