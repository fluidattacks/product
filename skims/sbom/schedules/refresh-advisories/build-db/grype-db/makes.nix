{ __system__, inputs, makeDerivation, ... }:
let
  version = "0.27.0";
  srcs = {
    aarch64-darwin = {
      url =
        "https://github.com/anchore/grype-db/releases/download/v${version}/grype-db_${version}_darwin_arm64.tar.gz";
      sha256 = "sha256-lPm9M8FdVa80l6p0tTH1TaZTZr/3La5GWbWgoRAV5sU=";
    };
    aarch64-linux = {
      url =
        "https://github.com/anchore/grype-db/releases/download/v${version}/grype-db_${version}_linux_arm64.tar.gz";
      sha256 = "sha256-PReHHKV+JNNixjBX357zy/4a1SKU69m1t49PqCuGTc8=";
    };
  };
in {
  jobs."/skims/sbom/schedules/refresh-advisories/build-db/grype-db" =
    makeDerivation {
      builder = ''
        mkdir -p $out/bin
        copy $envSrc/grype-db $out/bin/grype-db
        chmod +x $out/bin/grype-db
      '';
      env.envSrc = inputs.nixpkgs.fetchzip {
        inherit (srcs.${__system__}) url sha256;
        stripRoot = false;
      };
      name = "grype-db";
    };
}
