import csv
import gzip
import json
import logging
import sqlite3
import urllib.request
from typing import (
    Any,
)

LOGGER = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)

CSV_NAME = "epss_scores-current.csv.gz"
CSV_URL = f"https://epss.cyentia.com/{CSV_NAME}"
DB_PATH = "build/vulnerability.db"
NVD_DB_PATH = "data/nvd/results/results.db"
GITHUB_DB_PATH = "data/github/results/results.db"

PATCHES = [
    {"cve": "CVE-2024-21907", "old_pkg_name": "json.net", "new_pkg_name": "newtonsoft.json"},
    {
        "cve": "CVE-2021-43570",
        "old_pkg_name": "ecdsa-java",
        "new_pkg_name": "com.starkbank:starkbank-ecdsa",
    },
    {
        "cve": "CVE-2023-26919",
        "old_pkg_name": "nashorn_sandbox",
        "new_pkg_name": "org.javadelight:delight-nashorn-sandbox",
    },
    {
        "cve": "CVE-2021-37573",
        "old_pkg_name": "tiny_java_web_server",
        "new_pkg_name": "org.javassist:tiny_java_web_server",
    },
    {
        "cve": "CVE-2020-1416",
        "old_pkg_name": "typescript",
        "new_pkg_name": "vscode",
    },
]

urllib.request.urlretrieve(CSV_URL, CSV_NAME)  # noqa: S310


def get_cve_severities_from_db(cve_ids: list[str]) -> dict[str, str]:
    results = {}
    connection = sqlite3.connect("data/nvd/results/results.db")
    cursor = connection.cursor()

    for cve_id in cve_ids:
        cursor.execute("SELECT record FROM results WHERE id = ?", (cve_id,))
        row = cursor.fetchone()

        if not row:
            results[cve_id] = "Unknown"
            continue

        record = json.loads(row[0])
        metrics = record.get("item", {}).get("cve", {}).get("metrics", {})

        severity = "Unknown"
        for version in ["cvssMetricV4", "cvssMetricV31", "cvssMetricV3", "cvssMetricV2"]:
            if version in metrics:
                metric = next(
                    (m for m in metrics[version] if m.get("source") == "nvd@nist.gov"),
                    metrics[version][0],
                )
                severity = metric["cvssData"].get("baseSeverity", "Unknown")
                if severity == "Unknown":
                    severity = metric.get("baseSeverity", "Unknown")
                break
        results[cve_id.split("/")[1].upper()] = severity.capitalize()

    connection.close()

    return results


def get_ghsa_severities_from_db(ghsa_ids: list[str]) -> dict[str, str]:
    results = {}
    connection = sqlite3.connect(GITHUB_DB_PATH)
    cursor = connection.cursor()

    for ghsa_id in ghsa_ids:
        cursor.execute("SELECT record FROM results WHERE id like ?", (f"%{ghsa_id.lower()}",))
        row = cursor.fetchone()

        if not row:
            results[ghsa_id] = "Unknown"
            continue

        record = json.loads(row[0])
        severity = record.get("item", {}).get("Advisory", {}).get("Severity", "Unknown")
        results[ghsa_id] = severity.capitalize()

    connection.close()

    return results


def get_cve_awaiting_analysis(cve_ids: list[str]) -> list[str]:
    results: list[str] = ["CVE-2021-35940.patch"]
    connection = sqlite3.connect(NVD_DB_PATH)
    cursor = connection.cursor()

    for cve_id in cve_ids:
        cursor.execute("SELECT record FROM results WHERE id = ?", (cve_id,))
        row = cursor.fetchone()
        if not row:
            # Reserved cves, not needed
            results.append(cve_id.split("/")[1].upper())
            continue

        record = json.loads(row[0])
        status = record.get("item", {}).get("cve", {}).get("vulnStatus", "None")
        if status.lower() in {
            "awaiting analysis",
            "undergoing analysis",
            "received",
        } or not record.get("item", {}).get("cve", {}).get("metrics", {}):
            results.append(cve_id.split("/")[1].upper())

    connection.close()

    return results


def convert_cve_to_db_id(cve: str) -> str:
    year = cve.split("-")[1]
    return f"{year}/{cve.lower()}"


def convert_cve_list_to_db_id(cve_list: list[str]) -> list[str]:
    return [convert_cve_to_db_id(cve) for cve in cve_list]


def _add_epss(
    cursor: sqlite3.Cursor,
    epss_for_cves: list[dict[str, Any]],
) -> None:
    LOGGER.info("Adding EPSS related columns")
    cursor.execute(
        """
        ALTER TABLE vulnerability_metadata
        ADD COLUMN epss REAL DEFAULT null;
        """,
    )
    cursor.execute(
        """
        ALTER TABLE vulnerability_metadata
        ADD COLUMN percentile REAL DEFAULT null;
        """,
    )
    LOGGER.info("Matching GHSA EPSS scores from related CVEs")
    cursor.execute(
        """
        SELECT
            id,
            json_extract(related_vulnerabilities, '$[0].id') as cve
        FROM vulnerability
        WHERE namespace LIKE 'github:%'
        AND cve IS NOT NULL
        """,
    )
    results = cursor.fetchall()
    epss_by_cve = {item["cve"]: item for item in epss_for_cves}
    epss_for_ghsa = [
        {**epss_by_cve[cve], "cve": ghsa} for ghsa, cve in results if cve in epss_by_cve
    ]
    LOGGER.info("Adding EPSS scores")
    cursor.executemany(
        """
        UPDATE vulnerability_metadata
        SET epss = :epss, percentile = :percentile
        WHERE id = :cve
        """,
        [*epss_for_cves, *epss_for_ghsa],
    )
    connection.commit()
    LOGGER.info("✅ EPSS scores added to database")


def _patch_known_errors(cursor: sqlite3.Cursor) -> None:
    for patch in PATCHES:
        LOGGER.info("Applying patch in %s", patch["cve"])
        cursor.execute(
            """
            UPDATE vulnerability
            SET package_name = ?
            WHERE id = ? AND package_name = ?
            """,
            (patch["new_pkg_name"], patch["cve"], patch["old_pkg_name"]),
        )
        connection.commit()
    LOGGER.info("✅ Known patches applied successfully")


def _patch_severity_incongruences(cursor: sqlite3.Cursor) -> None:
    LOGGER.info("Patching severity incongruences")
    query = """
        SELECT id
        FROM vulnerability_metadata
        WHERE (severity = 'Unknown' OR severity = 'Negligible')
    """
    cursor.execute(query)
    results = cursor.fetchall()
    cve_list = [row[0] for row in results if row[0].startswith("CVE")]
    ghsa_list = [row[0] for row in results if row[0].startswith("GHSA")]
    severities_dict_cve = get_cve_severities_from_db(convert_cve_list_to_db_id(cve_list))
    LOGGER.info("%d CVE's to update", len(severities_dict_cve.items()))
    severities_dict_ghsa = get_ghsa_severities_from_db(ghsa_list)
    LOGGER.info("%d GHSA's to update", len(severities_dict_ghsa.items()))
    severities_dict = severities_dict_ghsa | severities_dict_cve
    query = """
        UPDATE vulnerability_metadata
        SET severity = ?
        WHERE id = ?
    """
    data = [(severity, cve) for cve, severity in severities_dict.items()]
    cursor.executemany(query, data)
    connection.commit()
    LOGGER.info("✅ Severity incongruences patched successfully")


def _delete_rejected_cves(cursor: sqlite3.Cursor) -> None:
    LOGGER.info("Deleting rejected CVEs")
    query_with_cte = """
        WITH cve_ids AS (
            SELECT id
            FROM vulnerability_metadata
            WHERE description LIKE 'Rejected reason:%'
            OR description LIKE '** REJECT **%'
            OR description LIKE '[REJECTED CVE]%'
        )
        DELETE FROM vulnerability
        WHERE id IN (SELECT id FROM cve_ids)
    """
    cursor.execute(query_with_cte)
    query_delete_metadata = """
        WITH cve_ids AS (
            SELECT id
            FROM vulnerability_metadata
            WHERE description LIKE 'Rejected reason:%'
            OR description LIKE '** REJECT **%'
            OR description LIKE '[REJECTED CVE]%'
        )
        DELETE FROM vulnerability_metadata
        WHERE id IN (SELECT id FROM cve_ids)
    """
    cursor.execute(query_delete_metadata)

    connection.commit()

    LOGGER.info("✅ Deleted rejected CVE's from vulnerability_metadata.")


def _delete_awaiting_analysis_cves(cursor: sqlite3.Cursor) -> None:
    LOGGER.info("Deleting awaiting analysis")
    query = """
        SELECT id
        FROM vulnerability_metadata
        WHERE (severity = 'Unknown' OR severity = 'Negligible')
    """
    cursor.execute(query)
    results = cursor.fetchall()
    ghsa_list = [row[0] for row in results if not row[0].startswith("CVE")]
    cve_list = [row[0] for row in results if row[0].startswith("CVE")]
    delete_list = get_cve_awaiting_analysis(convert_cve_list_to_db_id(cve_list)) + ghsa_list
    placeholders = ", ".join(["?"] * len(delete_list))
    query = f"""DELETE FROM vulnerability_metadata WHERE id IN ({placeholders}) """  # noqa: S608
    cursor.execute(query, delete_list)
    query_vuln = f"""DELETE FROM vulnerability WHERE id IN ({placeholders}) """  # noqa: S608
    cursor.execute(query_vuln, delete_list)
    connection.commit()
    LOGGER.info("Deleted %s AW CVE's from vulnerability", len(delete_list))


with gzip.open(CSV_NAME, mode="rt") as csv_file:
    next(csv_file)  # Skip first line as it is metadata text
    csv_reader = csv.DictReader(csv_file)
    epss_for_cves = list(csv_reader)

    with sqlite3.connect(DB_PATH) as connection:
        cursor = connection.cursor()
        _delete_rejected_cves(cursor)
        _add_epss(cursor, epss_for_cves)
        _patch_known_errors(cursor)
        _patch_severity_incongruences(cursor)
        _delete_awaiting_analysis_cves(cursor)
