# shellcheck shell=bash

aws_login "prod_common" "3600" \
  && JSON_ARRAY=$(vunnel list | jq -R -s 'split("\n") | map(select(length > 0))') \
  && TASK_OUTPUT=$(jq -n --argjson providers "$JSON_ARRAY" '{providers: $providers}') \
  && if [[ -v TASK_TOKEN && -n $TASK_TOKEN ]]; then
    echo "TASK_TOKEN is present. Executing AWS Step Functions command..."
    aws stepfunctions send-task-success \
      --task-token "${TASK_TOKEN}" \
      --task-output "${TASK_OUTPUT}"
  else
    echo "TASK_TOKEN is not set. Nothing to execute."
  fi
