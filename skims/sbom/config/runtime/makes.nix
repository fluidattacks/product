{ libGit, makePythonEnvironment, makeTemplate, inputs, projectPath, outputs
, makeSearchPaths, ... }:
let
  requirements = makePythonEnvironment {
    pythonProjectDir = projectPath "/skims/sbom";
    pythonVersion = "3.11";
    overrides = _self: super: {
      grimp = super.grimp.overridePythonAttrs (old: {
        preUnpack = ''
          export HOME=$(mktemp -d)
          rm -rf /homeless-shelter
        '' + (old.preUnpack or "");
        buildInputs = [ super.setuptools ];
      });
      cpe = super.cpe.overridePythonAttrs (old: {
        preUnpack = ''
          export HOME=$(mktemp -d)
          rm -rf /homeless-shelter
        '' + (old.preUnpack or "");
        buildInputs = [ super.setuptools ];
      });
      cyclonedx-python-lib = super.cyclonedx-python-lib.overridePythonAttrs
        (old: {
          preUnpack = ''
            export HOME=$(mktemp -d)
            rm -rf /homeless-shelter
          '' + (old.preUnpack or "");
          # Skip https://github.com/nix-community/poetry2nix/blob/master/overrides/default.nix#L588
          postPatch = "echo Skipping postPatch";
        });
    };
  };
  sbomPythonEnv = makeSearchPaths { source = [ requirements ]; };
in {
  jobs."/skims/sbom/config/runtime" = makeTemplate {
    name = "sbom-pypi-runtime";
    replace = { __argSrcSbom__ = projectPath "/skims/sbom"; };
    searchPaths = {
      pythonPackage = [
        "${inputs.nixpkgs.python311Packages.python_magic}/lib/python3.11/site-packages/"
        (projectPath "/skims/sbom")
      ];
      source = [
        libGit
        sbomPythonEnv
        (makeTemplate {
          replace = {
            __argSrcTreeSitterParsers__ =
              outputs."/skims/sbom/config/runtime/parsers";
          };
          name = "sbom-config-context-file";
          template = ''
            export TREE_SITTER_PARSERS_DIR='__argSrcTreeSitterParsers__'
            export BUGSNAG_API_KEY=82c9f090e2049a63c44a2045b029a7a8
          '';
        })
      ];
      bin = [ inputs.nixpkgs.skopeo inputs.nixpkgs.zstd ];
    };
    template = ./template.sh;
  };
}
