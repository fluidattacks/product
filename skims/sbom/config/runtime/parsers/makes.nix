{ inputs, ... }:
let
  tree-sitter = inputs.nixpkgs.python311Packages.tree-sitter.overridePythonAttrs
    (oldAttrs: {
      src = inputs.nixpkgs.fetchFromGitHub {
        owner = "tree-sitter";
        repo = "py-tree-sitter";
        rev = "refs/tags/v0.21.1";
        sha256 = "sha256-U4ZdU0lxjZO/y0q20bG5CLKipnfpaxzV3AFR6fGS7m4=";
        fetchSubmodules = true;
      };

      dependencies = [ inputs.nixpkgs.python311Packages.setuptools ];
    });
in {
  jobs."/skims/sbom/config/runtime/parsers" =
    inputs.nixpkgs.stdenv.mkDerivation {
      buildPhase = ''
        export envTreeSitterGemFileLock="${inputs.TreeSitterGemFileLockTarball}"
        export envTreeSitterMixLock="${inputs.TreeSitterMixLockTarball}"

        python build.py
      '';
      name = "sbom-config-runtime-parsers";
      nativeBuildInputs = [ tree-sitter ];
      src = ./.;
    };
}
