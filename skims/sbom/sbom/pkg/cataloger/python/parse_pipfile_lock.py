import logging
from typing import TYPE_CHECKING

from sbom.utils.strings import format_exception

if TYPE_CHECKING:
    from collections.abc import ItemsView
from typing import cast

from pydantic import ValidationError

from sbom.artifact.relationship import Relationship
from sbom.file.location import Location
from sbom.file.location_read_closer import LocationReadCloser
from sbom.file.resolver import Resolver
from sbom.internal.collection.json import ParsedJSON, parse_json_with_tree_sitter
from sbom.internal.collection.types import IndexedDict
from sbom.model.core import Language, Package, PackageType
from sbom.pkg.cataloger.generic.parser import Environment
from sbom.pkg.cataloger.python.package import package_url

LOGGER = logging.getLogger(__name__)


def _get_location(location: Location, sourceline: int) -> Location:
    if location.coordinates:
        c_upd = {"line": sourceline}
        l_upd = {"coordinates": location.coordinates.model_copy(update=c_upd)}
        return location.model_copy(update=l_upd)
    return location


def _get_version(value: IndexedDict[str, ParsedJSON]) -> str:
    version: str = value["version"] if isinstance(value["version"], str) else ""
    return version.strip("=<>~^ ")


def _get_packages(
    reader: LocationReadCloser,
    dependencies: ParsedJSON | None,
) -> list[Package]:
    if dependencies is None or not isinstance(dependencies, IndexedDict):
        return []

    packages = []

    items: ItemsView[str, ParsedJSON] = dependencies.items()
    for name, value in items:
        if not isinstance(value, IndexedDict) or not isinstance(name, str):
            continue
        version = _get_version(value)
        if not name or not version:
            continue

        location = _get_location(reader.location, value.position.start.line)

        try:
            packages.append(
                Package(
                    name=name,
                    version=version,
                    locations=[location],
                    language=Language.PYTHON,
                    type=PackageType.PythonPkg,
                    p_url=package_url(
                        name=name,
                        version=version,
                        package=None,
                    ),
                    licenses=[],
                ),
            )
        except ValidationError as ex:
            LOGGER.warning(
                "Malformed package. Required fields are missing or data types are incorrect.",
                extra={
                    "extra": {
                        "exception": format_exception(str(ex)),
                        "location": location.path(),
                    },
                },
            )
            continue

    return packages


def parse_pipfile_lock_deps(
    _resolver: Resolver | None,
    _env: Environment | None,
    reader: LocationReadCloser,
) -> tuple[list[Package], list[Relationship]]:
    content = cast(
        IndexedDict[str, ParsedJSON],
        parse_json_with_tree_sitter(reader.read_closer.read()),
    )
    deps: ParsedJSON | None = content.get("default")
    dev_deps: ParsedJSON | None = content.get("develop")
    packages = [
        *_get_packages(reader, deps),
        *_get_packages(reader, dev_deps),
    ]
    return packages, []
