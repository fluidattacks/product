import logging
import re
from copy import (
    deepcopy,
)

from bs4 import (
    BeautifulSoup,
)
from bs4.element import (
    Tag,
)
from packageurl import (
    PackageURL,
)

from sbom.artifact.relationship import (
    Relationship,
)
from sbom.file.dependency_type import (
    DependencyType,
)
from sbom.file.location_read_closer import (
    LocationReadCloser,
)
from sbom.file.resolver import (
    Resolver,
)
from sbom.model.core import (
    Language,
    Package,
    PackageType,
)
from sbom.pkg.cataloger.generic.parser import (
    Environment,
)

LOGGER = logging.getLogger(__name__)


def parse_dotnet_config_executable(
    _resolver: Resolver | None,
    _env: Environment | None,
    reader: LocationReadCloser,
) -> tuple[list[Package], list[Relationship]]:
    packages = []
    root = BeautifulSoup(reader.read_closer.read(), features="html.parser")
    net_dep = re.compile(r".NETFramework,Version=v(?P<version>[^\s,]*)")
    net_runtime = root.find("supportedruntime")
    if (
        isinstance(net_runtime, Tag)
        and isinstance(net_runtime.parent, Tag)
        and net_runtime.parent.name == "startup"
        and (runtime_info := net_runtime.get("sku", ""))
        and (version_match := net_dep.match(str(runtime_info)))
    ):
        version = version_match.group("version")

        location = deepcopy(reader.location)
        if location.coordinates:
            location.coordinates.line = net_runtime.sourceline
            location.dependency_type = DependencyType.DIRECT

        packages.append(
            Package(
                name=".net_framework",
                version=version,
                locations=[location],
                licenses=[],
                type=PackageType.DotnetPkg,
                language=Language.DOTNET,
                metadata=None,
                p_url=PackageURL(
                    type="nuget",
                    namespace="",
                    name=".net_framework",
                    version=version,
                    qualifiers={},
                    subpath="",
                ).to_string(),
            ),
        )

    return packages, []
