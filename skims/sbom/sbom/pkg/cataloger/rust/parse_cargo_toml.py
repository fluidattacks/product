import logging
from typing import TYPE_CHECKING

from sbom.utils.strings import format_exception

if TYPE_CHECKING:
    from collections.abc import ItemsView

from pydantic import ValidationError

from sbom.artifact.relationship import Relationship
from sbom.file.dependency_type import DependencyType
from sbom.file.location import Location
from sbom.file.location_read_closer import LocationReadCloser
from sbom.file.resolver import Resolver
from sbom.file.scope import Scope
from sbom.internal.collection.toml import parse_toml_with_tree_sitter
from sbom.internal.collection.types import IndexedDict
from sbom.model.core import Language, Package, PackageType
from sbom.pkg.cataloger.generic.parser import Environment
from sbom.pkg.cataloger.rust.package import package_url

LOGGER = logging.getLogger(__name__)


def _get_location(location: Location, sourceline: int) -> Location:
    if location.coordinates:
        c_upd = {"line": sourceline}
        l_upd = {"coordinates": location.coordinates.model_copy(update=c_upd)}
        location.dependency_type = DependencyType.DIRECT
        return location.model_copy(update=l_upd)
    return location


def _get_version(value: IndexedDict[str, str] | str) -> str | None:
    if isinstance(value, str):
        return value
    if "git" in value:
        repo_url: str | None = value.get("git")
        branch: str | None = value.get("branch")
        if repo_url and branch:
            return f"{repo_url}@{branch}"
    version: str | None = value.get("version")
    return version


def _get_packages(
    reader: LocationReadCloser,
    dependencies: IndexedDict[str, IndexedDict[str, str]] | None,
    *,
    is_dev: bool,
) -> list[Package]:
    if dependencies is None:
        return []

    packages = []

    general_location = _get_location(
        reader.location,
        dependencies.position.start.line,
    )
    items: ItemsView[str, IndexedDict[str, str] | str] = dependencies.items()

    for name, value in items:
        version = _get_version(value)
        if not name or not version:
            continue

        location = (
            _get_location(reader.location, value.position.start.line)
            if isinstance(value, IndexedDict)
            else general_location
        )
        location.scope = Scope.DEV if is_dev else Scope.PROD
        try:
            packages.append(
                Package(
                    name=name,
                    version=version,
                    locations=[location],
                    language=Language.RUST,
                    licenses=[],
                    p_url=package_url(name=name, version=version),
                    type=PackageType.RustPkg,
                ),
            )
        except ValidationError as ex:
            LOGGER.warning(
                "Malformed package. Required fields are missing or data types are incorrect.",
                extra={
                    "extra": {
                        "exception": format_exception(str(ex)),
                        "location": location.path(),
                    },
                },
            )
            continue

    return packages


def parse_cargo_toml(
    _: Resolver | None,
    __: Environment | None,
    reader: LocationReadCloser,
) -> tuple[list[Package], list[Relationship]]:
    content: IndexedDict[
        str,
        IndexedDict[str, IndexedDict[str, str]] | None,
    ] = parse_toml_with_tree_sitter(reader.read_closer.read())

    deps: IndexedDict[str, IndexedDict[str, str]] | None = content.get(
        "dependencies",
    )
    dev_deps: IndexedDict[str, IndexedDict[str, str]] | None = content.get(
        "dev-dependencies",
    )
    packages = [
        *_get_packages(reader, deps, is_dev=False),
        *_get_packages(reader, dev_deps, is_dev=True),
    ]
    return packages, []
