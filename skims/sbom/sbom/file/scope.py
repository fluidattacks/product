from enum import (
    Enum,
)


class Scope(Enum):
    DEV = "DEV"
    PROD = "PROD"
