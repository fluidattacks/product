from sbom.internal.collection.json import (
    parse_json_with_tree_sitter,
)
from sbom.internal.collection.types import (
    FileCoordinate,
    IndexedDict,
    IndexedList,
    Position,
)


def test_parse_json() -> None:
    content = """{
    "a": 1,
    "b": 2,
    "numbers": [1,
        2,
            3,
        4.5, -4],
    "booleans": [true, false],
    "strings": ["hello", "world"],
    "null": null
  }
    """
    result = parse_json_with_tree_sitter(content)
    assert isinstance(result, IndexedDict)
    assert result.get_value_position("b").start == FileCoordinate(line=3, column=10)
    assert result.get_key_position("a") == Position(
        start=FileCoordinate(line=2, column=5),
        end=FileCoordinate(line=2, column=8),
    )
    assert result.get_value_position("numbers").start == FileCoordinate(line=4, column=16)
    assert isinstance(result["numbers"], IndexedList)
    assert result["numbers"].get_position(2).start == FileCoordinate(line=6, column=13)
