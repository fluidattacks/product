import os

import pytest

from sbom.internal.file.zip_file_manifest import (
    new_zip_file_manifest,
    zip_glob_match,
)

from .test_helper import (
    ensure_nested_zip_exists,
    setup_zip_file_test,
)


@pytest.mark.usefixtures("_change_test_dir")
def test_new_zip_file_manifest() -> None:
    source_dir = os.path.join("test-fixtures", "zip-source")
    ensure_nested_zip_exists(source_dir)

    archive_file_path = setup_zip_file_test(source_dir)
    actual_manifest = new_zip_file_manifest(archive_file_path)

    ezpected_zip_entries = [
        "b-file.txt",
        "nested.zip",
        f"some-dir{os.path.sep}",
        os.path.join("some-dir", "a-file.txt"),
    ]
    assert sorted([x.filename for x in actual_manifest]) == ezpected_zip_entries


@pytest.mark.parametrize(
    ("glob", "expected"),
    [
        (
            "/b*",
            "b-file.txt",
        ),
        (
            "*/a-file.txt",
            "some-dir/a-file.txt",
        ),
        (
            "*/A-file.txt",
            "some-dir/a-file.txt",
        ),
        (
            "**/*.zip",
            "nested.zip",
        ),
    ],
)
@pytest.mark.usefixtures("_change_test_dir")
def test_zip_file_manifest_glob_match(
    glob: str,
    expected: str,
) -> None:
    source_dir = os.path.join("test-fixtures", "zip-source")
    ensure_nested_zip_exists(source_dir)

    archive_file_path = setup_zip_file_test(source_dir)
    manifest = new_zip_file_manifest(archive_file_path)
    result = zip_glob_match(manifest, case_sensitive=True, patterns=(glob,))
    assert len(result) == 1
    assert result[0] == expected
