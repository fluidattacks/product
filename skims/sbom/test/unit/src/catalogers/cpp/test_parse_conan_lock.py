from pathlib import Path

from sbom.file.coordinates import (
    Coordinates,
)
from sbom.file.location import (
    Location,
    new_location,
)
from sbom.file.location_read_closer import (
    LocationReadCloser,
)
from sbom.model.core import (
    Language,
    Package,
    PackageType,
)
from sbom.pkg.cataloger.cpp.parse_conan_lock import (
    parse_conan_lock,
)


def test_parse_conan_lock() -> None:
    fixture = "test/lib/data/dependencies/cpp/conan.lock"
    expected_packages = [
        Package(
            name="xbyak",
            version="6.73",
            language=Language.CPP,
            licenses=[],
            locations=[
                Location(
                    coordinates=Coordinates(
                        real_path="test/lib/data/dependencies/cpp/conan.lock",
                        file_system_id=None,
                        line=4,
                    ),
                    access_path="test/lib/data/dependencies/cpp/conan.lock",
                    annotations={},
                ),
            ],
            type=PackageType.ConanPkg,
            advisories=None,
            dependencies=None,
            found_by=None,
            health_metadata=None,
            is_dev=False,
            metadata=None,
            p_url="pkg:conan/xbyak@6.73",
        ),
        Package(
            name="snappy",
            version="1.1.10",
            language=Language.CPP,
            licenses=[],
            locations=[
                Location(
                    coordinates=Coordinates(
                        real_path="test/lib/data/dependencies/cpp/conan.lock",
                        file_system_id=None,
                        line=5,
                    ),
                    access_path="test/lib/data/dependencies/cpp/conan.lock",
                    annotations={},
                ),
            ],
            type=PackageType.ConanPkg,
            advisories=None,
            dependencies=None,
            found_by=None,
            health_metadata=None,
            is_dev=False,
            metadata=None,
            p_url="pkg:conan/snappy@1.1.10",
        ),
        Package(
            name="rapidjson",
            version="cci.20220822",
            language=Language.CPP,
            licenses=[],
            locations=[
                Location(
                    coordinates=Coordinates(
                        real_path="test/lib/data/dependencies/cpp/conan.lock",
                        file_system_id=None,
                        line=6,
                    ),
                    access_path="test/lib/data/dependencies/cpp/conan.lock",
                    annotations={},
                ),
            ],
            type=PackageType.ConanPkg,
            advisories=None,
            dependencies=None,
            found_by=None,
            health_metadata=None,
            is_dev=False,
            metadata=None,
            p_url="pkg:conan/rapidjson@cci.20220822",
        ),
        Package(
            name="pybind11",
            version="2.12.0",
            language=Language.CPP,
            licenses=[],
            locations=[
                Location(
                    coordinates=Coordinates(
                        real_path="test/lib/data/dependencies/cpp/conan.lock",
                        file_system_id=None,
                        line=7,
                    ),
                    access_path="test/lib/data/dependencies/cpp/conan.lock",
                    annotations={},
                ),
            ],
            type=PackageType.ConanPkg,
            advisories=None,
            dependencies=None,
            found_by=None,
            health_metadata=None,
            is_dev=False,
            metadata=None,
            p_url="pkg:conan/pybind11@2.12.0",
        ),
        Package(
            name="pugixml",
            version="1.13",
            language=Language.CPP,
            licenses=[],
            locations=[
                Location(
                    coordinates=Coordinates(
                        real_path="test/lib/data/dependencies/cpp/conan.lock",
                        file_system_id=None,
                        line=8,
                    ),
                    access_path="test/lib/data/dependencies/cpp/conan.lock",
                    annotations={},
                ),
            ],
            type=PackageType.ConanPkg,
            advisories=None,
            dependencies=None,
            found_by=None,
            health_metadata=None,
            is_dev=False,
            metadata=None,
            p_url="pkg:conan/pugixml@1.13",
        ),
        Package(
            name="zlib",
            version="1.2.13",
            language=Language.CPP,
            licenses=[],
            locations=[
                Location(
                    coordinates=Coordinates(
                        real_path="test/lib/data/dependencies/cpp/conan.lock",
                        file_system_id=None,
                        line=11,
                    ),
                    access_path="test/lib/data/dependencies/cpp/conan.lock",
                    annotations={},
                ),
            ],
            type=PackageType.ConanPkg,
            advisories=None,
            dependencies=None,
            found_by=None,
            health_metadata=None,
            is_dev=True,
            metadata=None,
            p_url="pkg:conan/zlib@1.2.13",
        ),
        Package(
            name="protobuf",
            version="3.21.12",
            language=Language.CPP,
            licenses=[],
            locations=[
                Location(
                    coordinates=Coordinates(
                        real_path="test/lib/data/dependencies/cpp/conan.lock",
                        file_system_id=None,
                        line=12,
                    ),
                    access_path="test/lib/data/dependencies/cpp/conan.lock",
                    annotations={},
                ),
            ],
            type=PackageType.ConanPkg,
            advisories=None,
            dependencies=None,
            found_by=None,
            health_metadata=None,
            is_dev=True,
            metadata=None,
            p_url="pkg:conan/protobuf@3.21.12",
        ),
        Package(
            name="pkgconf",
            version="1.9.5",
            language=Language.CPP,
            licenses=[],
            locations=[
                Location(
                    coordinates=Coordinates(
                        real_path="test/lib/data/dependencies/cpp/conan.lock",
                        file_system_id=None,
                        line=13,
                    ),
                    access_path="test/lib/data/dependencies/cpp/conan.lock",
                    annotations={},
                ),
            ],
            type=PackageType.ConanPkg,
            advisories=None,
            dependencies=None,
            found_by=None,
            health_metadata=None,
            is_dev=True,
            metadata=None,
            p_url="pkg:conan/pkgconf@1.9.5",
        ),
        Package(
            name="patchelf",
            version="0.13",
            language=Language.CPP,
            licenses=[],
            locations=[
                Location(
                    coordinates=Coordinates(
                        real_path="test/lib/data/dependencies/cpp/conan.lock",
                        file_system_id=None,
                        line=14,
                    ),
                    access_path="test/lib/data/dependencies/cpp/conan.lock",
                    annotations={},
                ),
            ],
            type=PackageType.ConanPkg,
            advisories=None,
            dependencies=None,
            found_by=None,
            health_metadata=None,
            is_dev=True,
            metadata=None,
            p_url="pkg:conan/patchelf@0.13",
        ),
        Package(
            name="ninja",
            version="1.11.1",
            language=Language.CPP,
            licenses=[],
            locations=[
                Location(
                    coordinates=Coordinates(
                        real_path="test/lib/data/dependencies/cpp/conan.lock",
                        file_system_id=None,
                        line=15,
                    ),
                    access_path="test/lib/data/dependencies/cpp/conan.lock",
                    annotations={},
                ),
            ],
            type=PackageType.ConanPkg,
            advisories=None,
            dependencies=None,
            found_by=None,
            health_metadata=None,
            is_dev=True,
            metadata=None,
            p_url="pkg:conan/ninja@1.11.1",
        ),
    ]
    with Path(fixture).open(encoding="utf-8") as reader:
        pkgs, _ = parse_conan_lock(
            None,
            None,
            LocationReadCloser(location=new_location(fixture), read_closer=reader),
        )
        for pkg in pkgs:
            pkg.health_metadata = None
            pkg.licenses = []
        assert pkgs == expected_packages
