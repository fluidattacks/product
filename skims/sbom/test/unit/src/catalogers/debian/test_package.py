import pytest

from sbom.linux.release import (
    Release,
)
from sbom.pkg.cataloger.debian.model import DpkgDBEntry
from sbom.pkg.cataloger.debian.package import (
    package_url,
)


@pytest.mark.parametrize(
    ("distro", "metadata", "expected"),
    [
        (
            Release(id_="debian", version_id="11", id_like=["debian"]),
            DpkgDBEntry(package="p", version="v"),
            ("pkg:deb/debian/p@v?distro=debian-11&distro_id=debian&distro_version_id=11"),
        ),
        (
            Release(id_="debian", version_id="11"),
            DpkgDBEntry(package="p", version="v"),
            ("pkg:deb/debian/p@v?distro=debian-11&distro_id=debian&distro_version_id=11"),
        ),
        (
            Release(id_="ubuntu", version_id="16.04", id_like=["debian"]),
            DpkgDBEntry(package="p", version="v", architecture="a"),
            (
                "pkg:deb/ubuntu/p@v?arch=a&distro=ubuntu-16.04"
                "&distro_id=ubuntu&distro_version_id=16.04"
            ),
        ),
        (
            Release(id_="debian", version_id="11", id_like=["debian"]),
            DpkgDBEntry(package="p", source="s", version="v"),
            (
                "pkg:deb/debian/p@v?distro=debian-11"
                "&distro_id=debian&distro_version_id=11&upstream=s"
            ),
        ),
        (
            Release(id_="debian", version_id="11", id_like=["debian"]),
            DpkgDBEntry(package="p", source="s", version="v", source_version="2.3"),
            (
                "pkg:deb/debian/p@v?distro=debian-11"
                "&distro_id=debian&distro_version_id=11&upstream=s%402.3"
            ),
        ),
    ],
)
def test_package_url(distro: Release, metadata: DpkgDBEntry, expected: str | None) -> None:
    actual = package_url(metadata, distro)
    assert expected == actual
