import os
from pathlib import Path

from sbom.file.coordinates import (
    Coordinates,
)
from sbom.file.location import (
    Location,
    new_location,
)
from sbom.file.location_read_closer import (
    LocationReadCloser,
)
from sbom.model.core import (
    Language,
    Package,
    PackageType,
)
from sbom.pkg.cataloger.javascript.model import (
    NpmPackage,
)
from sbom.pkg.cataloger.javascript.parse_package_json import (
    parse_package_json,
)


def test_parse_package_json() -> None:
    expected_packages = [
        Package(
            name="JSONStream",
            version="^1.3.5",
            locations=[
                Location(
                    coordinates=Coordinates(
                        real_path="test/lib/data/dependencies/javascript/pkg-json/package.json",
                        file_system_id=None,
                        line=139,
                    ),
                    access_path="test/lib/data/dependencies/javascript/pkg-json/package.json",
                    annotations={},
                ),
            ],
            language=Language.JAVASCRIPT,
            licenses=[],
            type=PackageType.NpmPkg,
            metadata=NpmPackage(
                name="JSONStream",
                version="^1.3.5",
                author=None,
                homepage=None,
                description=None,
                url=None,
                private=None,
                is_dev=False,
            ),
            p_url="pkg:npm/JSONStream@%5E1.3.5",
            dependencies=None,
            found_by=None,
            health_metadata=None,
        ),
        Package(
            name="abbrev",
            version="~1.1.1",
            locations=[
                Location(
                    coordinates=Coordinates(
                        real_path="test/lib/data/dependencies/javascript/pkg-json/package.json",
                        file_system_id=None,
                        line=140,
                    ),
                    access_path="test/lib/data/dependencies/javascript/pkg-json/package.json",
                    annotations={},
                ),
            ],
            language=Language.JAVASCRIPT,
            licenses=[],
            type=PackageType.NpmPkg,
            metadata=NpmPackage(
                name="abbrev",
                version="~1.1.1",
                author=None,
                homepage=None,
                description=None,
                url=None,
                private=None,
                is_dev=False,
            ),
            p_url="pkg:npm/abbrev@~1.1.1",
            dependencies=None,
            found_by=None,
            health_metadata=None,
        ),
        Package(
            name="ansicolors",
            version="~0.3.2",
            locations=[
                Location(
                    coordinates=Coordinates(
                        real_path="test/lib/data/dependencies/javascript/pkg-json/package.json",
                        file_system_id=None,
                        line=141,
                    ),
                    access_path="test/lib/data/dependencies/javascript/pkg-json/package.json",
                    annotations={},
                ),
            ],
            language=Language.JAVASCRIPT,
            licenses=[],
            type=PackageType.NpmPkg,
            metadata=NpmPackage(
                name="ansicolors",
                version="~0.3.2",
                author=None,
                homepage=None,
                description=None,
                url=None,
                private=None,
                is_dev=False,
            ),
            p_url="pkg:npm/ansicolors@~0.3.2",
            dependencies=None,
            found_by=None,
            health_metadata=None,
        ),
        Package(
            name="ansistyles",
            version="~0.1.3",
            locations=[
                Location(
                    coordinates=Coordinates(
                        real_path="test/lib/data/dependencies/javascript/pkg-json/package.json",
                        file_system_id=None,
                        line=142,
                    ),
                    access_path="test/lib/data/dependencies/javascript/pkg-json/package.json",
                    annotations={},
                ),
            ],
            language=Language.JAVASCRIPT,
            licenses=[],
            type=PackageType.NpmPkg,
            metadata=NpmPackage(
                name="ansistyles",
                version="~0.1.3",
                author=None,
                homepage=None,
                description=None,
                url=None,
                private=None,
                is_dev=False,
            ),
            p_url="pkg:npm/ansistyles@~0.1.3",
            dependencies=None,
            found_by=None,
            health_metadata=None,
        ),
        Package(
            name="aproba",
            version="^2.0.0",
            locations=[
                Location(
                    coordinates=Coordinates(
                        real_path="test/lib/data/dependencies/javascript/pkg-json/package.json",
                        file_system_id=None,
                        line=143,
                    ),
                    access_path="test/lib/data/dependencies/javascript/pkg-json/package.json",
                    annotations={},
                ),
            ],
            language=Language.JAVASCRIPT,
            licenses=[],
            type=PackageType.NpmPkg,
            metadata=NpmPackage(
                name="aproba",
                version="^2.0.0",
                author=None,
                homepage=None,
                description=None,
                url=None,
                private=None,
                is_dev=False,
            ),
            p_url="pkg:npm/aproba@%5E2.0.0",
            dependencies=None,
            found_by=None,
            health_metadata=None,
        ),
        Package(
            name="archy",
            version="~1.0.0",
            locations=[
                Location(
                    coordinates=Coordinates(
                        real_path="test/lib/data/dependencies/javascript/pkg-json/package.json",
                        file_system_id=None,
                        line=144,
                    ),
                    access_path="test/lib/data/dependencies/javascript/pkg-json/package.json",
                    annotations={},
                ),
            ],
            language=Language.JAVASCRIPT,
            licenses=[],
            type=PackageType.NpmPkg,
            metadata=NpmPackage(
                name="archy",
                version="~1.0.0",
                author=None,
                homepage=None,
                description=None,
                url=None,
                private=None,
                is_dev=False,
            ),
            p_url="pkg:npm/archy@~1.0.0",
            dependencies=None,
            found_by=None,
            health_metadata=None,
        ),
        Package(
            name="bin-links",
            version="^1.1.7",
            locations=[
                Location(
                    coordinates=Coordinates(
                        real_path="test/lib/data/dependencies/javascript/pkg-json/package.json",
                        file_system_id=None,
                        line=145,
                    ),
                    access_path="test/lib/data/dependencies/javascript/pkg-json/package.json",
                    annotations={},
                ),
            ],
            language=Language.JAVASCRIPT,
            licenses=[],
            type=PackageType.NpmPkg,
            metadata=NpmPackage(
                name="bin-links",
                version="^1.1.7",
                author=None,
                homepage=None,
                description=None,
                url=None,
                private=None,
                is_dev=False,
            ),
            p_url="pkg:npm/bin-links@%5E1.1.7",
            dependencies=None,
            found_by=None,
            health_metadata=None,
        ),
        Package(
            name="bluebird",
            version="^3.5.5",
            locations=[
                Location(
                    coordinates=Coordinates(
                        real_path="test/lib/data/dependencies/javascript/pkg-json/package.json",
                        file_system_id=None,
                        line=146,
                    ),
                    access_path="test/lib/data/dependencies/javascript/pkg-json/package.json",
                    annotations={},
                ),
            ],
            language=Language.JAVASCRIPT,
            licenses=[],
            type=PackageType.NpmPkg,
            metadata=NpmPackage(
                name="bluebird",
                version="^3.5.5",
                author=None,
                homepage=None,
                description=None,
                url=None,
                private=None,
                is_dev=False,
            ),
            p_url="pkg:npm/bluebird@%5E3.5.5",
            dependencies=None,
            found_by=None,
            health_metadata=None,
        ),
        Package(
            name="byte-size",
            version="^5.0.1",
            locations=[
                Location(
                    coordinates=Coordinates(
                        real_path="test/lib/data/dependencies/javascript/pkg-json/package.json",
                        file_system_id=None,
                        line=147,
                    ),
                    access_path="test/lib/data/dependencies/javascript/pkg-json/package.json",
                    annotations={},
                ),
            ],
            language=Language.JAVASCRIPT,
            licenses=[],
            type=PackageType.NpmPkg,
            metadata=NpmPackage(
                name="byte-size",
                version="^5.0.1",
                author=None,
                homepage=None,
                description=None,
                url=None,
                private=None,
                is_dev=False,
            ),
            p_url="pkg:npm/byte-size@%5E5.0.1",
            dependencies=None,
            found_by=None,
            health_metadata=None,
        ),
        Package(
            name="cacache",
            version="^12.0.3",
            locations=[
                Location(
                    coordinates=Coordinates(
                        real_path="test/lib/data/dependencies/javascript/pkg-json/package.json",
                        file_system_id=None,
                        line=148,
                    ),
                    access_path="test/lib/data/dependencies/javascript/pkg-json/package.json",
                    annotations={},
                ),
            ],
            language=Language.JAVASCRIPT,
            licenses=[],
            type=PackageType.NpmPkg,
            metadata=NpmPackage(
                name="cacache",
                version="^12.0.3",
                author=None,
                homepage=None,
                description=None,
                url=None,
                private=None,
                is_dev=False,
            ),
            p_url="pkg:npm/cacache@%5E12.0.3",
            dependencies=None,
            found_by=None,
            health_metadata=None,
        ),
        Package(
            name="call-limit",
            version="^1.1.1",
            locations=[
                Location(
                    coordinates=Coordinates(
                        real_path="test/lib/data/dependencies/javascript/pkg-json/package.json",
                        file_system_id=None,
                        line=149,
                    ),
                    access_path="test/lib/data/dependencies/javascript/pkg-json/package.json",
                    annotations={},
                ),
            ],
            language=Language.JAVASCRIPT,
            licenses=[],
            type=PackageType.NpmPkg,
            metadata=NpmPackage(
                name="call-limit",
                version="^1.1.1",
                author=None,
                homepage=None,
                description=None,
                url=None,
                private=None,
                is_dev=False,
            ),
            p_url="pkg:npm/call-limit@%5E1.1.1",
            dependencies=None,
            found_by=None,
            health_metadata=None,
        ),
        Package(
            name="chownr",
            version="^1.1.4",
            locations=[
                Location(
                    coordinates=Coordinates(
                        real_path="test/lib/data/dependencies/javascript/pkg-json/package.json",
                        file_system_id=None,
                        line=150,
                    ),
                    access_path="test/lib/data/dependencies/javascript/pkg-json/package.json",
                    annotations={},
                ),
            ],
            language=Language.JAVASCRIPT,
            licenses=[],
            type=PackageType.NpmPkg,
            metadata=NpmPackage(
                name="chownr",
                version="^1.1.4",
                author=None,
                homepage=None,
                description=None,
                url=None,
                private=None,
                is_dev=False,
            ),
            p_url="pkg:npm/chownr@%5E1.1.4",
            dependencies=None,
            found_by=None,
            health_metadata=None,
        ),
        Package(
            name="ci-info",
            version="^2.0.0",
            locations=[
                Location(
                    coordinates=Coordinates(
                        real_path="test/lib/data/dependencies/javascript/pkg-json/package.json",
                        file_system_id=None,
                        line=151,
                    ),
                    access_path="test/lib/data/dependencies/javascript/pkg-json/package.json",
                    annotations={},
                ),
            ],
            language=Language.JAVASCRIPT,
            licenses=[],
            type=PackageType.NpmPkg,
            metadata=NpmPackage(
                name="ci-info",
                version="^2.0.0",
                author=None,
                homepage=None,
                description=None,
                url=None,
                private=None,
                is_dev=False,
            ),
            p_url="pkg:npm/ci-info@%5E2.0.0",
            dependencies=None,
            found_by=None,
            health_metadata=None,
        ),
        Package(
            name="cli-columns",
            version="^3.1.2",
            locations=[
                Location(
                    coordinates=Coordinates(
                        real_path="test/lib/data/dependencies/javascript/pkg-json/package.json",
                        file_system_id=None,
                        line=152,
                    ),
                    access_path="test/lib/data/dependencies/javascript/pkg-json/package.json",
                    annotations={},
                ),
            ],
            language=Language.JAVASCRIPT,
            licenses=[],
            type=PackageType.NpmPkg,
            metadata=NpmPackage(
                name="cli-columns",
                version="^3.1.2",
                author=None,
                homepage=None,
                description=None,
                url=None,
                private=None,
                is_dev=False,
            ),
            p_url="pkg:npm/cli-columns@%5E3.1.2",
            dependencies=None,
            found_by=None,
            health_metadata=None,
        ),
        Package(
            name="fs-vacuum",
            version="~1.2.10",
            locations=[
                Location(
                    coordinates=Coordinates(
                        real_path="test/lib/data/dependencies/javascript/pkg-json/package.json",
                        file_system_id=None,
                        line=153,
                    ),
                    access_path="test/lib/data/dependencies/javascript/pkg-json/package.json",
                    annotations={},
                ),
            ],
            language=Language.JAVASCRIPT,
            licenses=[],
            type=PackageType.NpmPkg,
            metadata=NpmPackage(
                name="fs-vacuum",
                version="~1.2.10",
                author=None,
                homepage=None,
                description=None,
                url=None,
                private=None,
                is_dev=False,
            ),
            p_url="pkg:npm/fs-vacuum@~1.2.10",
            dependencies=None,
            found_by=None,
            health_metadata=None,
        ),
        Package(
            name="deep-equal",
            version="^1.0.1",
            locations=[
                Location(
                    coordinates=Coordinates(
                        real_path="test/lib/data/dependencies/javascript/pkg-json/package.json",
                        file_system_id=None,
                        line=157,
                    ),
                    access_path="test/lib/data/dependencies/javascript/pkg-json/package.json",
                    annotations={},
                ),
            ],
            language=Language.JAVASCRIPT,
            licenses=[],
            type=PackageType.NpmPkg,
            metadata=NpmPackage(
                name="deep-equal",
                version="^1.0.1",
                author=None,
                homepage=None,
                description=None,
                url=None,
                private=None,
                is_dev=True,
            ),
            p_url="pkg:npm/deep-equal@%5E1.0.1",
            dependencies=None,
            found_by=None,
            health_metadata=None,
            is_dev=True,
        ),
        Package(
            name="get-stream",
            version="^4.1.0",
            locations=[
                Location(
                    coordinates=Coordinates(
                        real_path="test/lib/data/dependencies/javascript/pkg-json/package.json",
                        file_system_id=None,
                        line=158,
                    ),
                    access_path="test/lib/data/dependencies/javascript/pkg-json/package.json",
                    annotations={},
                ),
            ],
            language=Language.JAVASCRIPT,
            licenses=[],
            type=PackageType.NpmPkg,
            metadata=NpmPackage(
                name="get-stream",
                version="^4.1.0",
                author=None,
                homepage=None,
                description=None,
                url=None,
                private=None,
                is_dev=True,
            ),
            p_url="pkg:npm/get-stream@%5E4.1.0",
            dependencies=None,
            found_by=None,
            health_metadata=None,
            is_dev=True,
        ),
        Package(
            name="licensee",
            version="^7.0.3",
            locations=[
                Location(
                    coordinates=Coordinates(
                        real_path="test/lib/data/dependencies/javascript/pkg-json/package.json",
                        file_system_id=None,
                        line=159,
                    ),
                    access_path="test/lib/data/dependencies/javascript/pkg-json/package.json",
                    annotations={},
                ),
            ],
            language=Language.JAVASCRIPT,
            licenses=[],
            type=PackageType.NpmPkg,
            metadata=NpmPackage(
                name="licensee",
                version="^7.0.3",
                author=None,
                homepage=None,
                description=None,
                url=None,
                private=None,
                is_dev=True,
            ),
            p_url="pkg:npm/licensee@%5E7.0.3",
            dependencies=None,
            found_by=None,
            health_metadata=None,
            is_dev=True,
        ),
        Package(
            name="marked",
            version="^0.6.3",
            locations=[
                Location(
                    coordinates=Coordinates(
                        real_path="test/lib/data/dependencies/javascript/pkg-json/package.json",
                        file_system_id=None,
                        line=160,
                    ),
                    access_path="test/lib/data/dependencies/javascript/pkg-json/package.json",
                    annotations={},
                ),
            ],
            language=Language.JAVASCRIPT,
            licenses=[],
            type=PackageType.NpmPkg,
            metadata=NpmPackage(
                name="marked",
                version="^0.6.3",
                author=None,
                homepage=None,
                description=None,
                url=None,
                private=None,
                is_dev=True,
            ),
            p_url="pkg:npm/marked@%5E0.6.3",
            dependencies=None,
            found_by=None,
            health_metadata=None,
            is_dev=True,
        ),
        Package(
            name="marked-man",
            version="^0.6.0",
            locations=[
                Location(
                    coordinates=Coordinates(
                        real_path="test/lib/data/dependencies/javascript/pkg-json/package.json",
                        file_system_id=None,
                        line=161,
                    ),
                    access_path="test/lib/data/dependencies/javascript/pkg-json/package.json",
                    annotations={},
                ),
            ],
            language=Language.JAVASCRIPT,
            licenses=[],
            type=PackageType.NpmPkg,
            metadata=NpmPackage(
                name="marked-man",
                version="^0.6.0",
                author=None,
                homepage=None,
                description=None,
                url=None,
                private=None,
                is_dev=True,
            ),
            p_url="pkg:npm/marked-man@%5E0.6.0",
            dependencies=None,
            found_by=None,
            health_metadata=None,
            is_dev=True,
        ),
    ]
    fixture = os.path.join("test/lib/data/dependencies/javascript", "pkg-json", "package.json")
    with Path(fixture).open(
        encoding="utf-8",
    ) as reader:
        pkgs, _ = parse_package_json(
            None,
            None,
            LocationReadCloser(location=new_location(fixture), read_closer=reader),
        )
    for pkg in pkgs:
        pkg.health_metadata = None
        pkg.licenses = []
    assert pkgs == expected_packages
