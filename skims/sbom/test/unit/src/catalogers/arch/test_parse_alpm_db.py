import gzip
from datetime import (
    datetime,
)
from pathlib import Path
from typing import cast

import pytest

from sbom.file.resolver import FileReader
from sbom.model.core import (
    Digest,
)
from sbom.pkg.cataloger.arch.package import (
    AlpmDBEntry,
    AlpmFileRecord,
)
from sbom.pkg.cataloger.arch.parse_alpm import (
    parse_alpm_db_entry,
    parse_mtree,
)
from sbom.sources.directory_source import (
    Directory,
)


@pytest.mark.parametrize(
    ("fixture", "expected"),
    [
        (
            "test/lib/data/dependencies/arch/files",
            AlpmDBEntry(
                backup=[
                    AlpmFileRecord(
                        path="/etc/pacman.conf",
                        digests=[
                            Digest(
                                algorithm="md5",
                                value="de541390e52468165b96511c4665bff4",
                            ),
                        ],
                    ),
                    AlpmFileRecord(
                        path="/etc/makepkg.conf",
                        digests=[
                            Digest(
                                algorithm="md5",
                                value="79fce043df7dfc676ae5ecb903762d8b",
                            ),
                        ],
                    ),
                ],
                files=[
                    AlpmFileRecord(path="/etc/"),
                    AlpmFileRecord(path="/etc/makepkg.conf"),
                    AlpmFileRecord(path="/etc/pacman.conf"),
                    AlpmFileRecord(path="/usr/"),
                    AlpmFileRecord(path="/usr/bin/"),
                    AlpmFileRecord(path="/usr/bin/makepkg"),
                    AlpmFileRecord(path="/usr/bin/makepkg-template"),
                    AlpmFileRecord(path="/usr/bin/pacman"),
                    AlpmFileRecord(path="/usr/bin/pacman-conf"),
                    AlpmFileRecord(path="/var/"),
                    AlpmFileRecord(path="/var/cache/"),
                    AlpmFileRecord(path="/var/cache/pacman/"),
                    AlpmFileRecord(path="/var/cache/pacman/pkg/"),
                    AlpmFileRecord(path="/var/lib/"),
                    AlpmFileRecord(path="/var/lib/pacman/"),
                ],
            ),
        ),
    ],
)
def test_database_parser(
    fixture: str,
    expected: AlpmDBEntry,
) -> None:
    with Path(fixture).open(encoding="utf-8") as reader:
        entry = parse_alpm_db_entry(
            reader,
        )

    assert entry == expected


def test_mtree_parse() -> None:
    expected = [
        AlpmFileRecord(
            path="/etc",
            type="dir",
            time=datetime.fromisoformat("2022-04-10T12:59:52Z"),
            digests=[],
        ),
        AlpmFileRecord(
            path="/etc/pacman.d",
            type="dir",
            time=datetime.fromisoformat("2022-04-10T12:59:52Z"),
            digests=[],
        ),
        AlpmFileRecord(
            path="/etc/pacman.d/mirrorlist",
            size="44683",
            time=datetime.fromisoformat("2022-04-10T12:59:52Z"),
            digests=[
                Digest(
                    algorithm="md5",
                    value="81c39827e38c759d7e847f05db62c233",
                ),
                Digest(
                    algorithm="sha256",
                    value=("fc135ab26f2a227b9599b66a2f1ba325c445acb914d60e7ecf6e5997a87abe1e"),
                ),
            ],
        ),
    ]
    resolver = Directory(root="test/lib/data/dependencies/arch", exclude=())
    mtree_location = resolver.files_by_path("mtree")[0]
    reader = resolver.file_contents_by_location(
        mtree_location,
        function_reader=cast(FileReader, gzip.open),
        mode="rt",
    )
    if reader:
        entries = parse_mtree(reader)
        assert entries == expected
