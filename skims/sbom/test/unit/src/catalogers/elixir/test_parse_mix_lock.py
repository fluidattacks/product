from pathlib import Path

from sbom.file.coordinates import (
    Coordinates,
)
from sbom.file.location import (
    Location,
    new_location,
)
from sbom.file.location_read_closer import (
    LocationReadCloser,
)
from sbom.model.core import (
    Language,
    Package,
    PackageType,
)
from sbom.pkg.cataloger.elixir.package import (
    ElixirMixLockEntry,
)
from sbom.pkg.cataloger.elixir.parse_mix_lock import (
    parse_mix_lock,
)


def test_parse_mix_lock() -> None:
    fixture = "test/lib/data/dependencies/elixir/mix.lock"
    expected_packages = [
        Package(
            name="castore",
            version="0.1.17",
            locations=[
                Location(
                    coordinates=Coordinates(
                        real_path="test/lib/data/dependencies/elixir/mix.lock",
                        file_system_id=None,
                        line=2,
                    ),
                    access_path="test/lib/data/dependencies/elixir/mix.lock",
                    annotations={},
                ),
            ],
            language=Language.ELIXIR,
            licenses=[],
            type=PackageType.HexPkg,
            metadata=ElixirMixLockEntry(
                name="castore",
                version="0.1.17",
                pkg_hash=("ba672681de4e51ed8ec1f74ed624d104c0db72742ea1a5e74edbc770c815182f"),
                pkg_hash_ext=("d9844227ed52d26e7519224525cb6868650c272d4a3d327ce3ca5570c12163f9"),
            ),
            p_url="pkg:hex/castore@0.1.17",
            dependencies=None,
            found_by=None,
            health_metadata=None,
        ),
        Package(
            name="connection",
            version="1.1.0",
            locations=[
                Location(
                    coordinates=Coordinates(
                        real_path="test/lib/data/dependencies/elixir/mix.lock",
                        file_system_id=None,
                        line=3,
                    ),
                    access_path="test/lib/data/dependencies/elixir/mix.lock",
                    annotations={},
                ),
            ],
            language=Language.ELIXIR,
            licenses=[],
            type=PackageType.HexPkg,
            metadata=ElixirMixLockEntry(
                name="connection",
                version="1.1.0",
                pkg_hash=("ff2a49c4b75b6fb3e674bfc5536451607270aac754ffd1bdfe175abe4a6d7a68"),
                pkg_hash_ext=("722c1eb0a418fbe91ba7bd59a47e28008a189d47e37e0e7bb85585a016b2869c"),
            ),
            p_url="pkg:hex/connection@1.1.0",
            dependencies=None,
            found_by=None,
            health_metadata=None,
        ),
        Package(
            name="cowboy",
            version="2.9.0",
            locations=[
                Location(
                    coordinates=Coordinates(
                        real_path="test/lib/data/dependencies/elixir/mix.lock",
                        file_system_id=None,
                        line=4,
                    ),
                    access_path="test/lib/data/dependencies/elixir/mix.lock",
                    annotations={},
                ),
            ],
            language=Language.ELIXIR,
            licenses=[],
            type=PackageType.HexPkg,
            metadata=ElixirMixLockEntry(
                name="cowboy",
                version="2.9.0",
                pkg_hash=("865dd8b6607e14cf03282e10e934023a1bd8be6f6bacf921a7e2a96d800cd452"),
                pkg_hash_ext=("2c729f934b4e1aa149aff882f57c6372c15399a20d54f65c8d67bef583021bde"),
            ),
            p_url="pkg:hex/cowboy@2.9.0",
            dependencies=None,
            found_by=None,
            health_metadata=None,
        ),
        Package(
            name="cowboy_telemetry",
            version="0.4.0",
            locations=[
                Location(
                    coordinates=Coordinates(
                        real_path="test/lib/data/dependencies/elixir/mix.lock",
                        file_system_id=None,
                        line=5,
                    ),
                    access_path="test/lib/data/dependencies/elixir/mix.lock",
                    annotations={},
                ),
            ],
            language=Language.ELIXIR,
            licenses=[],
            type=PackageType.HexPkg,
            metadata=ElixirMixLockEntry(
                name="cowboy_telemetry",
                version="0.4.0",
                pkg_hash=("f239f68b588efa7707abce16a84d0d2acf3a0f50571f8bb7f56a15865aae820c"),
                pkg_hash_ext=("7d98bac1ee4565d31b62d59f8823dfd8356a169e7fcbb83831b8a5397404c9de"),
            ),
            p_url="pkg:hex/cowboy_telemetry@0.4.0",
            dependencies=None,
            found_by=None,
            health_metadata=None,
        ),
        Package(
            name="cowlib",
            version="2.11.0",
            locations=[
                Location(
                    coordinates=Coordinates(
                        real_path="test/lib/data/dependencies/elixir/mix.lock",
                        file_system_id=None,
                        line=6,
                    ),
                    access_path="test/lib/data/dependencies/elixir/mix.lock",
                    annotations={},
                ),
            ],
            language=Language.ELIXIR,
            licenses=[],
            type=PackageType.HexPkg,
            metadata=ElixirMixLockEntry(
                name="cowlib",
                version="2.11.0",
                pkg_hash=("0b9ff9c346629256c42ebe1eeb769a83c6cb771a6ee5960bd110ab0b9b872063"),
                pkg_hash_ext=("2b3e9da0b21c4565751a6d4901c20d1b4cc25cbb7fd50d91d2ab6dd287bc86a9"),
            ),
            p_url="pkg:hex/cowlib@2.11.0",
            dependencies=None,
            found_by=None,
            health_metadata=None,
        ),
        Package(
            name="db_connection",
            version="2.4.2",
            locations=[
                Location(
                    coordinates=Coordinates(
                        real_path="test/lib/data/dependencies/elixir/mix.lock",
                        file_system_id=None,
                        line=7,
                    ),
                    access_path="test/lib/data/dependencies/elixir/mix.lock",
                    annotations={},
                ),
            ],
            language=Language.ELIXIR,
            licenses=[],
            type=PackageType.HexPkg,
            metadata=ElixirMixLockEntry(
                name="db_connection",
                version="2.4.2",
                pkg_hash=("f92e79aff2375299a16bcb069a14ee8615c3414863a6fef93156aee8e86c2ff3"),
                pkg_hash_ext=("4fe53ca91b99f55ea249693a0229356a08f4d1a7931d8ffa79289b145fe83668"),
            ),
            p_url="pkg:hex/db_connection@2.4.2",
            dependencies=None,
            found_by=None,
            health_metadata=None,
        ),
        Package(
            name="decimal",
            version="2.0.0",
            locations=[
                Location(
                    coordinates=Coordinates(
                        real_path="test/lib/data/dependencies/elixir/mix.lock",
                        file_system_id=None,
                        line=8,
                    ),
                    access_path="test/lib/data/dependencies/elixir/mix.lock",
                    annotations={},
                ),
            ],
            language=Language.ELIXIR,
            licenses=[],
            type=PackageType.HexPkg,
            metadata=ElixirMixLockEntry(
                name="decimal",
                version="2.0.0",
                pkg_hash=("a78296e617b0f5dd4c6caf57c714431347912ffb1d0842e998e9792b5642d697"),
                pkg_hash_ext=("34666e9c55dea81013e77d9d87370fe6cb6291d1ef32f46a1600230b1d44f577"),
            ),
            p_url="pkg:hex/decimal@2.0.0",
            dependencies=None,
            found_by=None,
            health_metadata=None,
        ),
        Package(
            name="earmark_parser",
            version="1.4.25",
            locations=[
                Location(
                    coordinates=Coordinates(
                        real_path="test/lib/data/dependencies/elixir/mix.lock",
                        file_system_id=None,
                        line=9,
                    ),
                    access_path="test/lib/data/dependencies/elixir/mix.lock",
                    annotations={},
                ),
            ],
            language=Language.ELIXIR,
            licenses=[],
            type=PackageType.HexPkg,
            metadata=ElixirMixLockEntry(
                name="earmark_parser",
                version="1.4.25",
                pkg_hash=("2024618731c55ebfcc5439d756852ec4e85978a39d0d58593763924d9a15916f"),
                pkg_hash_ext=("56749c5e1c59447f7b7a23ddb235e4b3defe276afc220a6227237f3efe83f51e"),
            ),
            p_url="pkg:hex/earmark_parser@1.4.25",
            dependencies=None,
            found_by=None,
            health_metadata=None,
        ),
        Package(
            name="ecto",
            version="3.8.1",
            locations=[
                Location(
                    coordinates=Coordinates(
                        real_path="test/lib/data/dependencies/elixir/mix.lock",
                        file_system_id=None,
                        line=10,
                    ),
                    access_path="test/lib/data/dependencies/elixir/mix.lock",
                    annotations={},
                ),
            ],
            language=Language.ELIXIR,
            licenses=[],
            type=PackageType.HexPkg,
            metadata=ElixirMixLockEntry(
                name="ecto",
                version="3.8.1",
                pkg_hash=("35e0bd8c8eb772e14a5191a538cd079706ecb45164ea08a7523b4fc69ab70f56"),
                pkg_hash_ext=("f1b68f8d5fe3ab89e24f57c03db5b5d0aed3602077972098b3a6006a1be4b69b"),
            ),
            p_url="pkg:hex/ecto@3.8.1",
            dependencies=None,
            found_by=None,
            health_metadata=None,
        ),
        Package(
            name="ecto_sql",
            version="3.8.1",
            locations=[
                Location(
                    coordinates=Coordinates(
                        real_path="test/lib/data/dependencies/elixir/mix.lock",
                        file_system_id=None,
                        line=11,
                    ),
                    access_path="test/lib/data/dependencies/elixir/mix.lock",
                    annotations={},
                ),
            ],
            language=Language.ELIXIR,
            licenses=[],
            type=PackageType.HexPkg,
            metadata=ElixirMixLockEntry(
                name="ecto_sql",
                version="3.8.1",
                pkg_hash=("1acaaba32ca0551fd19e492fc7c80414e72fc1a7140fc9395aaa53c2e8629798"),
                pkg_hash_ext=("ba7fc75882edce6f2ceca047315d5db27ead773cafea47f1724e35f1e7964525"),
            ),
            p_url="pkg:hex/ecto_sql@3.8.1",
            dependencies=None,
            found_by=None,
            health_metadata=None,
        ),
        Package(
            name="esbuild",
            version="0.5.0",
            locations=[
                Location(
                    coordinates=Coordinates(
                        real_path="test/lib/data/dependencies/elixir/mix.lock",
                        file_system_id=None,
                        line=12,
                    ),
                    access_path="test/lib/data/dependencies/elixir/mix.lock",
                    annotations={},
                ),
            ],
            language=Language.ELIXIR,
            licenses=[],
            type=PackageType.HexPkg,
            metadata=ElixirMixLockEntry(
                name="esbuild",
                version="0.5.0",
                pkg_hash=("d5bb08ff049d7880ee3609ed5c4b864bd2f46445ea40b16b4acead724fb4c4a3"),
                pkg_hash_ext=("f183a0b332d963c4cfaf585477695ea59eef9a6f2204fdd0efa00e099694ffe5"),
            ),
            p_url="pkg:hex/esbuild@0.5.0",
            dependencies=None,
            found_by=None,
            health_metadata=None,
        ),
        Package(
            name="ex_doc",
            version="0.28.4",
            locations=[
                Location(
                    coordinates=Coordinates(
                        real_path="test/lib/data/dependencies/elixir/mix.lock",
                        file_system_id=None,
                        line=13,
                    ),
                    access_path="test/lib/data/dependencies/elixir/mix.lock",
                    annotations={},
                ),
            ],
            language=Language.ELIXIR,
            licenses=[],
            type=PackageType.HexPkg,
            metadata=ElixirMixLockEntry(
                name="ex_doc",
                version="0.28.4",
                pkg_hash=("001a0ea6beac2f810f1abc3dbf4b123e9593eaa5f00dd13ded024eae7c523298"),
                pkg_hash_ext=("bf85d003dd34911d89c8ddb8bda1a958af3471a274a4c2150a9c01c78ac3f8ed"),
            ),
            p_url="pkg:hex/ex_doc@0.28.4",
            dependencies=None,
            found_by=None,
            health_metadata=None,
        ),
        Package(
            name="gettext",
            version="0.19.1",
            locations=[
                Location(
                    coordinates=Coordinates(
                        real_path="test/lib/data/dependencies/elixir/mix.lock",
                        file_system_id=None,
                        line=14,
                    ),
                    access_path="test/lib/data/dependencies/elixir/mix.lock",
                    annotations={},
                ),
            ],
            language=Language.ELIXIR,
            licenses=[],
            type=PackageType.HexPkg,
            metadata=ElixirMixLockEntry(
                name="gettext",
                version="0.19.1",
                pkg_hash=("564953fd21f29358e68b91634799d9d26989f8d039d7512622efb3c3b1c97892"),
                pkg_hash_ext=("10c656c0912b8299adba9b061c06947511e3f109ab0d18b44a866a4498e77222"),
            ),
            p_url="pkg:hex/gettext@0.19.1",
            dependencies=None,
            found_by=None,
            health_metadata=None,
        ),
        Package(
            name="hpax",
            version="0.1.1",
            locations=[
                Location(
                    coordinates=Coordinates(
                        real_path="test/lib/data/dependencies/elixir/mix.lock",
                        file_system_id=None,
                        line=15,
                    ),
                    access_path="test/lib/data/dependencies/elixir/mix.lock",
                    annotations={},
                ),
            ],
            language=Language.ELIXIR,
            licenses=[],
            type=PackageType.HexPkg,
            metadata=ElixirMixLockEntry(
                name="hpax",
                version="0.1.1",
                pkg_hash=("2396c313683ada39e98c20a75a82911592b47e5c24391363343bde74f82396ca"),
                pkg_hash_ext=("0ae7d5a0b04a8a60caf7a39fcf3ec476f35cc2cc16c05abea730d3ce6ac6c826"),
            ),
            p_url="pkg:hex/hpax@0.1.1",
            dependencies=None,
            found_by=None,
            health_metadata=None,
        ),
        Package(
            name="jason",
            version="1.3.0",
            locations=[
                Location(
                    coordinates=Coordinates(
                        real_path="test/lib/data/dependencies/elixir/mix.lock",
                        file_system_id=None,
                        line=16,
                    ),
                    access_path="test/lib/data/dependencies/elixir/mix.lock",
                    annotations={},
                ),
            ],
            language=Language.ELIXIR,
            licenses=[],
            type=PackageType.HexPkg,
            metadata=ElixirMixLockEntry(
                name="jason",
                version="1.3.0",
                pkg_hash=("fa6b82a934feb176263ad2df0dbd91bf633d4a46ebfdffea0c8ae82953714946"),
                pkg_hash_ext=("53fc1f51255390e0ec7e50f9cb41e751c260d065dcba2bf0d08dc51a4002c2ac"),
            ),
            p_url="pkg:hex/jason@1.3.0",
            dependencies=None,
            found_by=None,
            health_metadata=None,
        ),
    ]
    with Path(fixture).open(encoding="utf-8") as reader:
        pkgs, _ = parse_mix_lock(
            None,
            None,
            LocationReadCloser(location=new_location(fixture), read_closer=reader),
        )
        assert pkgs == expected_packages
