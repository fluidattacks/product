from pathlib import Path

from sbom.file.coordinates import (
    Coordinates,
)
from sbom.file.location import (
    Location,
    new_location,
)
from sbom.file.location_read_closer import (
    LocationReadCloser,
)
from sbom.model.core import (
    Language,
    Package,
    PackageType,
)
from sbom.pkg.cataloger.elixir.parse_mix_exs import (
    parse_mix_exs,
)


def test_parse_mix_lock() -> None:
    fixture = "test/lib/data/dependencies/elixir/mix.exs"
    expected_packages = [
        Package(
            name="poison",
            version="3.1.0",
            language=Language.ELIXIR,
            licenses=[],
            locations=[
                Location(
                    coordinates=Coordinates(
                        real_path="test/lib/data/dependencies/elixir/mix.exs",
                        file_system_id=None,
                        line=21,
                    ),
                    access_path="test/lib/data/dependencies/elixir/mix.exs",
                    annotations={},
                ),
            ],
            type=PackageType.HexPkg,
            advisories=None,
            dependencies=None,
            found_by=None,
            health_metadata=None,
            is_dev=False,
            metadata=None,
            p_url="pkg:hex/poison@3.1.0",
        ),
        Package(
            name="plug",
            version="1.3.0",
            language=Language.ELIXIR,
            licenses=[],
            locations=[
                Location(
                    coordinates=Coordinates(
                        real_path="test/lib/data/dependencies/elixir/mix.exs",
                        file_system_id=None,
                        line=22,
                    ),
                    access_path="test/lib/data/dependencies/elixir/mix.exs",
                    annotations={},
                ),
            ],
            type=PackageType.HexPkg,
            advisories=None,
            dependencies=None,
            found_by=None,
            health_metadata=None,
            is_dev=False,
            metadata=None,
            p_url="pkg:hex/plug@1.3.0",
        ),
        Package(
            name="coherence",
            version="0.5",
            language=Language.ELIXIR,
            licenses=[],
            locations=[
                Location(
                    coordinates=Coordinates(
                        real_path="test/lib/data/dependencies/elixir/mix.exs",
                        file_system_id=None,
                        line=23,
                    ),
                    access_path="test/lib/data/dependencies/elixir/mix.exs",
                    annotations={},
                ),
            ],
            type=PackageType.HexPkg,
            advisories=None,
            dependencies=None,
            found_by=None,
            health_metadata=None,
            is_dev=False,
            metadata=None,
            p_url="pkg:hex/coherence@0.5",
        ),
        Package(
            name="ecto",
            version="2.2.0",
            language=Language.ELIXIR,
            licenses=[],
            locations=[
                Location(
                    coordinates=Coordinates(
                        real_path="test/lib/data/dependencies/elixir/mix.exs",
                        file_system_id=None,
                        line=24,
                    ),
                    access_path="test/lib/data/dependencies/elixir/mix.exs",
                    annotations={},
                ),
            ],
            type=PackageType.HexPkg,
            advisories=None,
            dependencies=None,
            found_by=None,
            health_metadata=None,
            is_dev=True,
            metadata=None,
            p_url="pkg:hex/ecto@2.2.0",
        ),
        Package(
            name="inch_ex",
            version="1.0",
            language=Language.ELIXIR,
            licenses=[],
            locations=[
                Location(
                    coordinates=Coordinates(
                        real_path="test/lib/data/dependencies/elixir/mix.exs",
                        file_system_id=None,
                        line=25,
                    ),
                    access_path="test/lib/data/dependencies/elixir/mix.exs",
                    annotations={},
                ),
            ],
            type=PackageType.HexPkg,
            advisories=None,
            dependencies=None,
            found_by=None,
            health_metadata=None,
            is_dev=False,
            metadata=None,
            p_url="pkg:hex/inch_ex@1.0",
        ),
    ]

    with Path(fixture).open(encoding="utf-8") as reader:
        pkgs, _ = parse_mix_exs(
            None,
            None,
            LocationReadCloser(location=new_location(fixture), read_closer=reader),
        )
        assert pkgs == expected_packages
