import os
from pathlib import Path

from sbom.file.coordinates import (
    Coordinates,
)
from sbom.file.location import (
    Location,
    new_location,
)
from sbom.file.location_read_closer import (
    LocationReadCloser,
)
from sbom.model.core import (
    Language,
    Package,
    PackageType,
)
from sbom.pkg.cataloger.python.model import (
    PythonFileDigest,
    PythonFileRecord,
    PythonPackage,
)
from sbom.pkg.cataloger.python.parse_wheel_egg import (
    parse_wheel_or_egg,
)
from sbom.sources.directory_source import (
    Directory,
)


def test_pyproject_file() -> None:
    fixture = os.path.join("test/lib/data/dependencies/python", "egg-info/PKG-INFO")
    expected = [
        Package(
            name="requests",
            version="2.22.0",
            language=Language.PYTHON,
            licenses=["Apache 2.0"],
            locations=[
                Location(
                    coordinates=Coordinates(
                        real_path="test/lib/data/dependencies/python/egg-info/PKG-INFO",
                        file_system_id=None,
                        line=None,
                    ),
                    access_path="test/lib/data/dependencies/python/egg-info/PKG-INFO",
                    annotations={},
                ),
            ],
            type=PackageType.PythonPkg,
            advisories=None,
            dependencies=None,
            found_by=None,
            health_metadata=None,
            is_dev=False,
            metadata=PythonPackage(
                name="requests",
                version="2.22.0",
                author="Kenneth Reitz",
                author_email="me@kennethreitz.org",
                platform="UNKNOWN",
                files=[
                    PythonFileRecord(
                        path="requests-2.22.0.dist-info/INSTALLER",
                        digest=PythonFileDigest(
                            algorithm="sha256",
                            value=("zuuue4knoyJ-UwPPXg8fezS7VCrXJQrAP7zeNuwvFQg"),
                        ),
                        size="4",
                    ),
                    PythonFileRecord(
                        path="requests/__init__.py",
                        digest=PythonFileDigest(
                            algorithm="sha256",
                            value=("PnKCgjcTq44LaAMzB-7--B2FdewRrE8F_vjZeaG9NhA"),
                        ),
                        size="3921",
                    ),
                    PythonFileRecord(
                        path="requests/__version__.py",
                        digest=PythonFileDigest(
                            algorithm="sha256",
                            value=("Bm-GFstQaFezsFlnmEMrJDe8JNROz9n2XXYtODdvjjc"),
                        ),
                        size="436",
                    ),
                    PythonFileRecord(
                        path="requests/utils.py",
                        digest=PythonFileDigest(
                            algorithm="sha256",
                            value=("LtPJ1db6mJff2TJSJWKi7rBpzjPS3mSOrjC9zRhoD3A"),
                        ),
                        size="30049",
                    ),
                ],
                site_package_root_path="test/lib/data/dependencies/python",
                top_level_packages=["requests"],
                direct_url_origin=None,
                dependencies=None,
            ),
            p_url="pkg:pypi/requests@2.22.0",
        ),
    ]

    with Path(fixture).open(encoding="utf-8") as reader:
        pkgs, _ = parse_wheel_or_egg(
            Directory(root=fixture, exclude=()),
            None,
            LocationReadCloser(location=new_location(fixture), read_closer=reader),
        )
        assert pkgs is not None
        for pkg in pkgs:
            pkg.health_metadata = None
            pkg.licenses = []
    assert pkgs == expected
