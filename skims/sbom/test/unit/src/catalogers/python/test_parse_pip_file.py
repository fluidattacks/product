import os
from pathlib import Path

from sbom.file.coordinates import (
    Coordinates,
)
from sbom.file.location import (
    Location,
    new_location,
)
from sbom.file.location_read_closer import (
    LocationReadCloser,
)
from sbom.model.core import (
    Language,
    Package,
    PackageType,
)
from sbom.pkg.cataloger.python.model import (
    PythonPackage,
    PythonRequirementsEntry,
)
from sbom.pkg.cataloger.python.parse_pipfile_deps import (
    parse_pipfile_deps,
)
from sbom.pkg.cataloger.python.parse_requirements import (
    parse_requirements_txt,
)


def test_parse_pip_file() -> None:
    fixture = os.path.join("test/lib/data/dependencies/python", "Pipfile")
    expected = [
        Package(
            name="records",
            version="0.5.0",
            locations=[
                Location(
                    coordinates=Coordinates(
                        real_path="test/lib/data/dependencies/python/Pipfile",
                        file_system_id=None,
                        line=11,
                    ),
                    access_path="test/lib/data/dependencies/python/Pipfile",
                    annotations={},
                ),
            ],
            language=Language.PYTHON,
            licenses=[],
            type=PackageType.PythonPkg,
            metadata=PythonPackage(
                name="records",
                version="0.5.0",
                author=None,
                author_email=None,
                platform=None,
                files=None,
                site_package_root_path=None,
                top_level_packages=None,
                direct_url_origin=None,
                dependencies=None,
            ),
            p_url="pkg:pypi/records@0.5.0",
            dependencies=None,
            found_by=None,
        ),
    ]

    with Path(fixture).open(encoding="utf-8") as reader:
        pkgs, _ = parse_pipfile_deps(
            None,
            None,
            LocationReadCloser(location=new_location(fixture), read_closer=reader),
        )
        assert pkgs is not None
        for pkg in pkgs:
            pkg.health_metadata = None
            pkg.licenses = []
    assert pkgs == expected


def test_requirements() -> None:
    fixture = os.path.join("test/lib/data/dependencies/python", "requires/requirements.txt")
    expected = [
        Package(
            name="flask",
            version="4.0.0",
            language=Language.PYTHON,
            licenses=[],
            locations=[
                Location(
                    coordinates=Coordinates(
                        real_path="test/lib/data/dependencies/python/requires/requirements.txt",
                        file_system_id=None,
                        line=1,
                    ),
                    access_path="test/lib/data/dependencies/python/requires/requirements.txt",
                    annotations={},
                ),
            ],
            type=PackageType.PythonPkg,
            advisories=None,
            dependencies=None,
            found_by=None,
            health_metadata=None,
            is_dev=False,
            metadata=PythonRequirementsEntry(
                name="flask",
                extras=[],
                markers="pkg:pypi/flask@4.0.0",
                version_constraint="== 4.0.0",
            ),
            p_url="pkg:pypi/flask@4.0.0",
        ),
        Package(
            name="sqlalchemy",
            version="3.0.0",
            language=Language.PYTHON,
            licenses=[],
            locations=[
                Location(
                    coordinates=Coordinates(
                        real_path="test/lib/data/dependencies/python/requires/requirements.txt",
                        file_system_id=None,
                        line=3,
                    ),
                    access_path="test/lib/data/dependencies/python/requires/requirements.txt",
                    annotations={},
                ),
            ],
            type=PackageType.PythonPkg,
            advisories=None,
            dependencies=None,
            found_by=None,
            health_metadata=None,
            is_dev=False,
            metadata=PythonRequirementsEntry(
                name="sqlalchemy",
                extras=[],
                markers="pkg:pypi/sqlalchemy@3.0.0",
                version_constraint=">= 1.0.0,<= 2.0.0,!= 3.0.0,<= 3.0.0",
            ),
            p_url="pkg:pypi/sqlalchemy@3.0.0",
        ),
        Package(
            name="foo",
            version="1.0.0",
            language=Language.PYTHON,
            licenses=[],
            locations=[
                Location(
                    coordinates=Coordinates(
                        real_path="test/lib/data/dependencies/python/requires/requirements.txt",
                        file_system_id=None,
                        line=4,
                    ),
                    access_path="test/lib/data/dependencies/python/requires/requirements.txt",
                    annotations={},
                ),
            ],
            type=PackageType.PythonPkg,
            advisories=None,
            dependencies=None,
            found_by=None,
            health_metadata=None,
            is_dev=False,
            metadata=PythonRequirementsEntry(
                name="foo",
                extras=[],
                markers="pkg:pypi/foo@1.0.0",
                version_constraint="== 1.0.0",
            ),
            p_url="pkg:pypi/foo@1.0.0",
        ),
        Package(
            name="bar",
            version="3.0.0",
            language=Language.PYTHON,
            licenses=[],
            locations=[
                Location(
                    coordinates=Coordinates(
                        real_path="test/lib/data/dependencies/python/requires/requirements.txt",
                        file_system_id=None,
                        line=5,
                    ),
                    access_path="test/lib/data/dependencies/python/requires/requirements.txt",
                    annotations={},
                ),
            ],
            type=PackageType.PythonPkg,
            advisories=None,
            dependencies=None,
            found_by=None,
            health_metadata=None,
            is_dev=False,
            metadata=PythonRequirementsEntry(
                name="bar",
                extras=[],
                markers="pkg:pypi/bar@3.0.0",
                version_constraint=">= 1.0.0,<= 2.0.0,!= 3.0.0,<= 3.0.0",
            ),
            p_url="pkg:pypi/bar@3.0.0",
        ),
        Package(
            name="SomeProject",
            version="5.4",
            language=Language.PYTHON,
            licenses=[],
            locations=[
                Location(
                    coordinates=Coordinates(
                        real_path="test/lib/data/dependencies/python/requires/requirements.txt",
                        file_system_id=None,
                        line=10,
                    ),
                    access_path="test/lib/data/dependencies/python/requires/requirements.txt",
                    annotations={},
                ),
            ],
            type=PackageType.PythonPkg,
            advisories=None,
            dependencies=None,
            found_by=None,
            health_metadata=None,
            is_dev=False,
            metadata=PythonRequirementsEntry(
                name="SomeProject",
                extras=[],
                markers="pkg:pypi/someproject@5.4",
                version_constraint="== 5.4",
            ),
            p_url="pkg:pypi/someproject@5.4",
        ),
        Package(
            name="coverage",
            version="3.5",
            language=Language.PYTHON,
            licenses=[],
            locations=[
                Location(
                    coordinates=Coordinates(
                        real_path="test/lib/data/dependencies/python/requires/requirements.txt",
                        file_system_id=None,
                        line=11,
                    ),
                    access_path="test/lib/data/dependencies/python/requires/requirements.txt",
                    annotations={},
                ),
            ],
            type=PackageType.PythonPkg,
            advisories=None,
            dependencies=None,
            found_by=None,
            health_metadata=None,
            is_dev=False,
            metadata=PythonRequirementsEntry(
                name="coverage",
                extras=[],
                markers="pkg:pypi/coverage@3.5",
                version_constraint="!= 3.5",
            ),
            p_url="pkg:pypi/coverage@3.5",
        ),
        Package(
            name="numpy",
            version="3.4.1",
            language=Language.PYTHON,
            licenses=[],
            locations=[
                Location(
                    coordinates=Coordinates(
                        real_path="test/lib/data/dependencies/python/requires/requirements.txt",
                        file_system_id=None,
                        line=13,
                    ),
                    access_path="test/lib/data/dependencies/python/requires/requirements.txt",
                    annotations={},
                ),
            ],
            type=PackageType.PythonPkg,
            advisories=None,
            dependencies=None,
            found_by=None,
            health_metadata=None,
            is_dev=False,
            metadata=PythonRequirementsEntry(
                name="numpy",
                extras=[],
                markers="pkg:pypi/numpy@3.4.1",
                version_constraint=">= 3.4.1",
            ),
            p_url="pkg:pypi/numpy@3.4.1",
        ),
        Package(
            name="Mopidy-Dirble",
            version="1.1",
            language=Language.PYTHON,
            licenses=[],
            locations=[
                Location(
                    coordinates=Coordinates(
                        real_path="test/lib/data/dependencies/python/requires/requirements.txt",
                        file_system_id=None,
                        line=14,
                    ),
                    access_path="test/lib/data/dependencies/python/requires/requirements.txt",
                    annotations={},
                ),
            ],
            type=PackageType.PythonPkg,
            advisories=None,
            dependencies=None,
            found_by=None,
            health_metadata=None,
            is_dev=False,
            metadata=PythonRequirementsEntry(
                name="Mopidy-Dirble",
                extras=[],
                markers="pkg:pypi/mopidy-dirble@1.1",
                version_constraint="~= 1.1",
            ),
            p_url="pkg:pypi/mopidy-dirble@1.1",
        ),
        Package(
            name="argh",
            version="0.26.2",
            language=Language.PYTHON,
            licenses=[],
            locations=[
                Location(
                    coordinates=Coordinates(
                        real_path="test/lib/data/dependencies/python/requires/requirements.txt",
                        file_system_id=None,
                        line=15,
                    ),
                    access_path="test/lib/data/dependencies/python/requires/requirements.txt",
                    annotations={},
                ),
            ],
            type=PackageType.PythonPkg,
            advisories=None,
            dependencies=None,
            found_by=None,
            health_metadata=None,
            is_dev=False,
            metadata=PythonRequirementsEntry(
                name="argh",
                extras=[],
                markers="pkg:pypi/argh@0.26.2",
                version_constraint="== 0.26.2",
            ),
            p_url="pkg:pypi/argh@0.26.2",
        ),
        Package(
            name="argh",
            version="0.26.3",
            language=Language.PYTHON,
            licenses=[],
            locations=[
                Location(
                    coordinates=Coordinates(
                        real_path="test/lib/data/dependencies/python/requires/requirements.txt",
                        file_system_id=None,
                        line=18,
                    ),
                    access_path="test/lib/data/dependencies/python/requires/requirements.txt",
                    annotations={},
                ),
            ],
            type=PackageType.PythonPkg,
            advisories=None,
            dependencies=None,
            found_by=None,
            health_metadata=None,
            is_dev=False,
            metadata=PythonRequirementsEntry(
                name="argh",
                extras=[],
                markers="pkg:pypi/argh@0.26.3",
                version_constraint="== 0.26.3",
            ),
            p_url="pkg:pypi/argh@0.26.3",
        ),
        Package(
            name="celery",
            version="4.4.7",
            language=Language.PYTHON,
            licenses=[],
            locations=[
                Location(
                    coordinates=Coordinates(
                        real_path="test/lib/data/dependencies/python/requires/requirements.txt",
                        file_system_id=None,
                        line=22,
                    ),
                    access_path="test/lib/data/dependencies/python/requires/requirements.txt",
                    annotations={},
                ),
            ],
            type=PackageType.PythonPkg,
            advisories=None,
            dependencies=None,
            found_by=None,
            health_metadata=None,
            is_dev=False,
            metadata=PythonRequirementsEntry(
                name="celery",
                extras=["pytest", "redis"],
                markers="pkg:pypi/celery@4.4.7",
                version_constraint="== 4.4.7",
            ),
            p_url="pkg:pypi/celery@4.4.7",
        ),
    ]
    with Path(fixture).open(encoding="utf-8") as reader:
        pkgs, _ = parse_requirements_txt(
            None,
            None,
            LocationReadCloser(location=new_location(fixture), read_closer=reader),
        )
        assert pkgs is not None
        for pkg in pkgs:
            pkg.health_metadata = None
            pkg.licenses = []
    assert pkgs == expected
