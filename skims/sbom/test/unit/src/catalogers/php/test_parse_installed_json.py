from pathlib import Path

from sbom.file.coordinates import (
    Coordinates,
)
from sbom.file.location import (
    Location,
    new_location,
)
from sbom.file.location_read_closer import (
    LocationReadCloser,
)
from sbom.model.core import (
    Language,
    Package,
    PackageType,
)
from sbom.pkg.cataloger.php.model import (
    PhpComposerAuthors,
    PhpComposerExternalReference,
    PhpComposerLockEntry,
)
from sbom.pkg.cataloger.php.parse_installed_json import (
    parse_installed_json,
)


def test_parse_composer_lock() -> None:
    fixtures = [
        "test/lib/data/dependencies/php/vendor/composer_1/installed.json",
        "test/lib/data/dependencies/php/vendor/composer_2/installed.json",
    ]
    expected_packages = [
        Package(
            name="asm89/stack-cors",
            version="1.3.0",
            locations=[
                Location(
                    coordinates=Coordinates(
                        real_path=(
                            "test/lib/data/dependencies/php/vendor/composer_1/installed.json"
                        ),
                        file_system_id=None,
                        line=0,
                    ),
                    access_path=("test/lib/data/dependencies/php/vendor/composer_1/installed.json"),
                    annotations={},
                ),
            ],
            language=Language.PHP,
            licenses=[],
            type=PackageType.PhpComposerPkg,
            metadata=PhpComposerLockEntry(
                name="asm89/stack-cors",
                version="1.3.0",
                source=PhpComposerExternalReference(
                    type="git",
                    url="https://github.com/asm89/stack-cors.git",
                    reference="b9c31def6a83f84b4d4a40d35996d375755f0e08",
                    shasum=None,
                ),
                dist=PhpComposerExternalReference(
                    type="zip",
                    url=(
                        "https://api.github.com/repos/asm89/stack-cors"
                        "/zipball/b9c31def6a83f84b4d4a40d35996d375755f0e08"
                    ),
                    reference="b9c31def6a83f84b4d4a40d35996d375755f0e08",
                    shasum=None,
                ),
                require={
                    "php": ">=5.5.9",
                    "symfony/http-foundation": "~2.7|~3.0|~4.0|~5.0",
                    "symfony/http-kernel": "~2.7|~3.0|~4.0|~5.0",
                },
                provide=None,
                require_dev={
                    "phpunit/phpunit": "^5.0 || ^4.8.10",
                    "squizlabs/php_codesniffer": "^2.3",
                },
                suggest=None,
                license=["MIT"],
                type="library",
                notification_url="https://packagist.org/downloads/",
                bin=[],
                authors=[
                    PhpComposerAuthors(
                        name="Alexander",
                        email="iam.asm89@gmail.com",
                        homepage=None,
                    ),
                ],
                description=("Cross-origin resource sharing library and stack middleware"),
                homepage="https://github.com/asm89/stack-cors",
                keywords=["cors", "stack"],
                time="2019-12-24T22:41:47+00:00",
            ),
            p_url="pkg:composer/asm89/stack-cors@1.3.0",
            dependencies=None,
            found_by=None,
            health_metadata=None,
            is_dev=False,
        ),
        Package(
            name="behat/mink",
            version="v1.8.1",
            locations=[
                Location(
                    coordinates=Coordinates(
                        real_path=(
                            "test/lib/data/dependencies/php/vendor/composer_1/installed.json"
                        ),
                        file_system_id=None,
                        line=0,
                    ),
                    access_path=("test/lib/data/dependencies/php/vendor/composer_1/installed.json"),
                    annotations={},
                ),
            ],
            language=Language.PHP,
            licenses=[],
            type=PackageType.PhpComposerPkg,
            metadata=PhpComposerLockEntry(
                name="behat/mink",
                version="v1.8.1",
                source=PhpComposerExternalReference(
                    type="git",
                    url="https://github.com/minkphp/Mink.git",
                    reference="07c6a9fe3fa98c2de074b25d9ed26c22904e3887",
                    shasum=None,
                ),
                dist=PhpComposerExternalReference(
                    type="zip",
                    url=(
                        "https://api.github.com/repos/minkphp/Mink/zipball"
                        "/07c6a9fe3fa98c2de074b25d9ed26c22904e3887"
                    ),
                    reference="07c6a9fe3fa98c2de074b25d9ed26c22904e3887",
                    shasum=None,
                ),
                require={
                    "php": ">=5.3.1",
                    "symfony/css-selector": "^2.7|^3.0|^4.0|^5.0",
                },
                provide=None,
                require_dev={
                    "phpunit/phpunit": ("^4.8.36 || ^5.7.27 || ^6.5.14 || ^7.5.20"),
                    "symfony/debug": "^2.7|^3.0|^4.0",
                    "symfony/phpunit-bridge": "^3.4.38 || ^5.0.5",
                },
                suggest={
                    "behat/mink-browserkit-driver": (
                        "extremely fast headless driver for"
                        " Symfony\\\\Kernel-based apps (Sf2, Silex)"
                    ),
                    "behat/mink-goutte-driver": (
                        "fast headless driver for any app without JS emulation"
                    ),
                    "behat/mink-selenium2-driver": (
                        "slow, but JS-enabled driver for any app (requires Selenium2)"
                    ),
                    "behat/mink-zombie-driver": (
                        "fast and JS-enabled headless driver for any app (requires node.js)"
                    ),
                    "dmore/chrome-mink-driver": (
                        "fast and JS-enabled driver for any app"
                        " (requires chromium or google chrome)"
                    ),
                },
                license=["MIT"],
                type="library",
                notification_url="https://packagist.org/downloads/",
                bin=[],
                authors=[
                    PhpComposerAuthors(
                        name="Konstantin Kudryashov",
                        email="ever.zet@gmail.com",
                        homepage="http://everzet.com",
                    ),
                ],
                description="Browser controller/emulator abstraction for PHP",
                homepage="http://mink.behat.org/",
                keywords=["browser", "testing", "web"],
                time="2020-03-11T15:45:53+00:00",
            ),
            p_url="pkg:composer/behat/mink@v1.8.1",
            dependencies=None,
            found_by=None,
            health_metadata=None,
            is_dev=False,
        ),
    ]
    for fixture in fixtures:
        with Path(fixture).open(encoding="utf-8") as reader:
            pkgs, _ = parse_installed_json(
                None,
                None,
                LocationReadCloser(location=new_location(fixture), read_closer=reader),
            )
            for pkg in pkgs:
                # These values are fixed, because the content of the files and
                # packages should be the same, but the location is different
                pkg.locations[
                    0
                ].access_path = "test/lib/data/dependencies/php/vendor/composer_1/installed.json"
                if pkg.locations[0].coordinates:
                    pkg.locations[0].coordinates.line = 0
                    pkg.locations[
                        0
                    ].coordinates.real_path = (
                        "test/lib/data/dependencies/php/vendor/composer_1/installed.json"
                    )
                pkg.is_dev = False
        for pkg in pkgs:
            pkg.health_metadata = None
            pkg.licenses = []
        assert pkgs == expected_packages
