{ inputs, makeScript, outputs, ... }: {
  jobs."/skims/sbom/test/unit" = makeScript {
    name = "sbom-test-unit";
    entrypoint = ./entrypoint.sh;
    searchPaths = { source = [ outputs."/skims/sbom/config/runtime" ]; };
    replace.__argSbomEnv__ = outputs."/skims/sbom/config/runtime";
  };
}
