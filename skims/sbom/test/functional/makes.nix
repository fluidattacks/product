{ inputs, makeScript, outputs, ... }: {
  jobs."/skims/sbom/test/functional" = makeScript {
    name = "sbom-test-functional";
    entrypoint = ./entrypoint.sh;
    searchPaths = { source = [ outputs."/skims/sbom/config/runtime" ]; };
    replace.__argSbomEnv__ = outputs."/skims/sbom/config/runtime";
  };
}
