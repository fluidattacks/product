{ makeSearchPaths, makeScript, makeTemplate, outputs, projectPath, inputs
, __system__, ... }: {
  dev = {
    skimsSbom = {
      source = [
        outputs."/skims/sbom/config/runtime"
        (makeSearchPaths { pythonPackage = [ "$PWD/skims/sbom" ]; })
      ];
    };
  };
  secretsForEnvFromSops = {
    sbom = {
      vars = [ "CACHE_URL" "CACHE_USER_WRITE_PASSWORD" ];
      manifest = "/skims/secrets/dev.yaml";
    };
  };
  inputs = {
    # Also used by skims
    TreeSitterGemFileLockTarball = builtins.fetchTarball {
      url =
        "https://github.com/fluidattacks/tree-sitter-gemfilelock/archive/a0c57ba1edbe39ec0327838995d104aab2ed16c3.tar.gz";
      sha256 = "sha256-MBMJX9mi2fj6wfZqYw5VwHhbRZakSRBuqQ4wsIhUnlQ=";
    };
    TreeSitterMixLockTarball = builtins.fetchTarball {
      url =
        "https://github.com/fluidattacks/tree-sitter-mix_lock/archive/8ef8b09bf9b11369ce1d83478af3971e6dfe21b9.tar.gz";
      sha256 = "sha256:1fxj1s6p18qqfb876yxzcy0n3rmp6hipxym3hjnacbaa0a42s0p5";
    };
  };
  jobs."/skims/sbom" = makeScript {
    name = "sbom";
    entrypoint = ./entrypoint.sh;
    searchPaths = { source = [ outputs."/skims/sbom/config/runtime" ]; };
  };
  jobs."/skims/sbom/lint" = makeScript {
    name = "sbom-lint";
    entrypoint = ''
      pushd skims/sbom

      if test -n "''${CI:-}"; then
        ruff format --config ruff.toml --diff
        ruff check --config ruff.toml
      else
        ruff format --config ruff.toml
        ruff check --config ruff.toml --fix
      fi

      lint-imports --config import-linter.cfg

      mypy --config-file mypy.ini sbom
      mypy --config-file mypy.ini test
    '';
    searchPaths.source = [ outputs."/skims/sbom/config/runtime" ];
  };
}
