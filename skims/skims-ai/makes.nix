{ inputs, makeScript, projectPath, outputs, ... }: {
  jobs."/skims/skims-ai" = makeScript {
    replace = {
      __argSecrets__ = projectPath "/skims/skims-ai/secrets/dev.yaml";
      __argGoogleSheetsToken__ =
        projectPath "/skims/skims-ai/secrets/google_sheets_token.json";
    };
    name = "skims-ai";
    searchPaths = {
      bin = [
        inputs.nixpkgs.python311
        inputs.nixpkgs.poetry
        inputs.nixpkgs.sops
        inputs.nixpkgs.jq
        inputs.nixpkgs.zstd
      ];
      source = [ outputs."/common/utils/sops" ];
      pythonPackage = [ (projectPath "/skims/skims-ai/") ];
    };
    entrypoint = ./entrypoint.sh;
  };
}
