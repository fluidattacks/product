"""Commands for the skims/skims-ai CLI using the Click library."""

import asyncio

import asyncclick as click

from skims_ai.classifies.classifier import classify_cves_to_findings
from skims_ai.config.logger import (
    configure_logger,
    initialize_bugsnag,
)
from skims_ai.prioritizes import run as run_prioritize


@click.group
async def cli() -> None:
    # skims-ai CLI
    initialize_bugsnag()
    configure_logger()


@click.option(
    "--top",
    default=50,
    help="Specify the number of top packages to analyze.",
)
@click.option(
    "--debug",
    help="When True, the data will not be uploaded or downloaded from Google Sheets",
    type=bool,
    default=True,
)
@cli.command(name="prioritizes")
async def prioritizes(top: int, debug: bool) -> None:
    await run_prioritize(top, debug)


@click.option(
    "--db_path",
    default=None,
    help="Specify the path to the SCA DB, otherwise, it will download it from s3",
)
@click.option(
    "--debug",
    type=bool,
    default=False,
    help="Print the results to a local csv file",
)
@cli.command(name="classify_cve")
async def classify_cve(db_path: str | None, debug: bool) -> None:
    await classify_cves_to_findings(db_path, debug=debug)


if __name__ == "__main__":
    asyncio.run(cli())
