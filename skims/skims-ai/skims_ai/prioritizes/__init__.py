from dataclasses import replace

from skims_ai.prioritizes.classify import (
    classify_cves,
)
from skims_ai.prioritizes.defs import CVEStatus, EnrichedCVE, Sheet
from skims_ai.prioritizes.google_sheets import (
    get_prev_analyzed_advisories,
    get_prev_analyzed_advisories_ids,
    upload_to_google_sheets,
)
from skims_ai.prioritizes.sbom_data import (
    get_advisories_descriptions,
    get_advisories_languages,
    get_advisories_pkgs,
    get_advisories_severity,
    get_all_sbom_advisories,
    get_groups_by_pkg,
)
from skims_ai.prioritizes.skims_sca_db import list_cve_by_alternative_id, list_cves_descriptions
from skims_ai.prioritizes.utils import (
    get_covered_cves,
    get_findings_by_from_classifier,
    get_hyperlink,
    get_rows,
    prioritize_candidates,
)


async def get_new_cves(top: int) -> list[EnrichedCVE]:
    prev_cves = get_prev_analyzed_advisories_ids()
    sbom_cves_ids = await get_all_sbom_advisories()
    new_cves_ids = [_id for _id in sbom_cves_ids if _id not in prev_cves]
    top_new_cves = new_cves_ids if top == -1 else new_cves_ids[:top]
    return [EnrichedCVE(sbom_cve_id=cve_id) for cve_id in top_new_cves]


async def get_new_and_prev_cves(top: int) -> list[EnrichedCVE]:
    prev_cadidates: list[EnrichedCVE] = get_prev_analyzed_advisories(sheet=Sheet.CANDIDATES.value)
    prev_discarded: list[EnrichedCVE] = get_prev_analyzed_advisories(sheet=Sheet.DISCARDED.value)
    new_advisories: list[EnrichedCVE] = await get_new_cves(top=top)
    return prev_cadidates + prev_discarded + new_advisories


async def fill_base_fields(enriched_cves: list[EnrichedCVE]) -> list[EnrichedCVE]:
    affected_pkgs = await get_advisories_pkgs()
    groups_by_pkg = await get_groups_by_pkg()
    languages = await get_advisories_languages()
    sbom_descriptions = await get_advisories_descriptions()
    sca_descriptions = list_cves_descriptions()
    severities = await get_advisories_severity()
    findings_from_methods = get_covered_cves()
    findings_from_classifier = get_findings_by_from_classifier()
    cves_by_alternative_id = list_cve_by_alternative_id()
    filled_cves = []

    for cve in enriched_cves:
        cve_id = cves_by_alternative_id.get(cve.sbom_cve_id) or cve.sbom_cve_id

        description = sca_descriptions.get(
            cve_id, sbom_descriptions.get(cve.sbom_cve_id, cve.description)
        )

        language = languages.get(cve.sbom_cve_id, cve.language)
        pkg = affected_pkgs.get(cve.sbom_cve_id, cve.pkg)
        finding = findings_from_methods.get(
            cve.cve_id, findings_from_classifier.get(cve.cve_id, cve.finding)
        )
        is_candidate = findings_from_methods.get(cve.sbom_cve_id) is not None or cve.is_candidate
        status = (
            CVEStatus.IMPLEMENTED.value
            if findings_from_methods.get(cve.sbom_cve_id)
            else cve.status
        )
        reason = (
            "Skims method found by Prioritizes"
            if status == CVEStatus.IMPLEMENTED.value
            else cve.reason
        )

        if "unknown_language" in [cve.language, language]:
            continue

        updated_enriched_cve = replace(
            cve,
            cve_id=cve_id,
            is_candidate=is_candidate,
            url=get_hyperlink(cve_id=cve.cve_id),
            language=language,
            pkg=pkg,
            affected_groups=groups_by_pkg.get(pkg, cve.affected_groups),
            severity=severities.get(cve.sbom_cve_id, cve.severity),
            description=description,
            finding=finding,
            status=status,
            reason=reason,
        )
        filled_cves.append(updated_enriched_cve)
    return filled_cves


async def run(top: int, debug: bool) -> None:
    enriched_cves_base = await get_new_and_prev_cves(top=top)
    enriched_cves = await fill_base_fields(enriched_cves=enriched_cves_base)
    classified_cves = await classify_cves(cves=enriched_cves)
    candidates = get_rows([cve for cve in classified_cves if cve.is_candidate is True])
    discarded_cves = get_rows([cve for cve in classified_cves if cve.is_candidate is False])
    prioritized_candidates = prioritize_candidates(candidates)
    if debug:
        return
    upload_to_google_sheets(sheet=Sheet.CANDIDATES.value, cves=prioritized_candidates)
    upload_to_google_sheets(sheet=Sheet.DISCARDED.value, cves=discarded_cves)
