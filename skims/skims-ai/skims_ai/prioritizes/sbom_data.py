import os

from boto3 import Session
from opensearchpy import AsyncHttpConnection, AsyncOpenSearch, AWSV4SignerAsyncAuth

from skims_ai.config.logger import (
    LOGGER,
)

SESSION = Session()

HOST = os.getenv("FI_AWS_OPENSEARCH_HOST")
REGION = "us-east-1"
SERVICE = "es"
CREDENTIALS = SESSION.get_credentials()


async def fetch_opensearch(query: dict) -> dict:
    auth = AWSV4SignerAsyncAuth(CREDENTIALS, REGION, SERVICE)

    async with AsyncOpenSearch(
        hosts=[{"host": HOST, "port": 443}],
        http_auth=auth,
        use_ssl=True,
        verify_certs=True,
        connection_class=AsyncHttpConnection,
        timeout=300,
    ) as client:
        return await client.search(index="pkgs_index", body=query)


async def get_all_sbom_advisories() -> list[str]:
    LOGGER.info("Getting all SBOM advisories")
    query = {
        "query": {
            "bool": {
                "must": [
                    {"term": {"locations.dependency_type.keyword": {"value": "DIRECT"}}},
                    {"term": {"be_present": {"value": "true"}}},
                ]
            }
        },
        "size": 0,
        "aggs": {
            "unique_advisory_ids": {
                "terms": {
                    "field": "package_advisories.id.keyword",
                    "size": 10000,
                    "exclude": "MAL-.*",
                }
            }
        },
    }
    advisories = await fetch_opensearch(query)
    advisories_ids = advisories["aggregations"]["unique_advisory_ids"]["buckets"]
    return [advisory["key"] for advisory in advisories_ids]


async def get_advisories_languages() -> dict[str, str]:
    LOGGER.info("Getting advisories languages")
    query = {
        "size": 0,
        "query": {"exists": {"field": "package_advisories.id.keyword"}},
        "aggs": {
            "cve_ids": {
                "terms": {"field": "package_advisories.id.keyword", "size": 10000},
                "aggs": {"language": {"terms": {"field": "language.keyword", "size": 1}}},
            }
        },
    }
    results = await fetch_opensearch(query)
    aggs = results["aggregations"]["cve_ids"]["buckets"]
    return {agg["key"]: agg["language"]["buckets"][0]["key"] for agg in aggs}


async def get_advisories_descriptions() -> dict[str, str]:
    LOGGER.info("Getting advisories languages")
    query = {
        "size": 0,
        "query": {
            "bool": {"must": [{"exists": {"field": "package_advisories.description.keyword"}}]}
        },
        "aggs": {
            "cve_ids": {
                "terms": {"field": "package_advisories.id.keyword", "size": 10000},
                "aggs": {
                    "description": {
                        "terms": {"field": "package_advisories.description.keyword", "size": 1}
                    }
                },
            }
        },
    }
    results = await fetch_opensearch(query)
    aggs = results["aggregations"]["cve_ids"]["buckets"]
    return {agg["key"]: agg["description"]["buckets"][0]["key"] for agg in aggs}


async def get_advisories_pkgs() -> dict[str, str]:
    LOGGER.info("Getting advisories packages")
    query = {
        "size": 0,
        "query": {"exists": {"field": "package_advisories.id.keyword"}},
        "aggs": {
            "cve_ids": {
                "terms": {"field": "package_advisories.id.keyword", "size": 10000},
                "aggs": {"pkg": {"terms": {"field": "name.keyword", "size": 1}}},
            }
        },
    }
    results = await fetch_opensearch(query)
    aggs = results["aggregations"]["cve_ids"]["buckets"]
    return {agg["key"]: agg["pkg"]["buckets"][0]["key"] for agg in aggs}


async def get_groups_by_pkg() -> dict[str, str]:
    LOGGER.info("Getting all SBOM packages")
    query = {
        "size": 0,
        "query": {
            "bool": {
                "must": [
                    {"exists": {"field": "package_advisories.id.keyword"}},
                    {"term": {"be_present": {"value": "true"}}},
                ]
            }
        },
        "aggs": {
            "pkg_groups": {
                "terms": {"field": "name.keyword", "size": 10000},
                "aggs": {
                    "unique_group_count": {"cardinality": {"field": "group_name.keyword"}},
                    "sort_by_unique_group_count": {
                        "bucket_sort": {
                            "sort": [{"unique_group_count": {"order": "desc"}}],
                            "size": 10000,
                        }
                    },
                },
            }
        },
    }
    results = await fetch_opensearch(query)
    aggs = results["aggregations"]["pkg_groups"]["buckets"]
    return {agg["key"]: str(agg["unique_group_count"]["value"]) for agg in aggs}


async def get_advisories_severity() -> dict[str, str]:
    LOGGER.info("Getting advisories severities")
    query = {
        "size": 0,
        "query": {"exists": {"field": "package_advisories.id.keyword"}},
        "aggs": {
            "advisories": {
                "terms": {"field": "package_advisories.id.keyword", "size": 10000},
                "aggs": {
                    "severity": {
                        "terms": {"field": "package_advisories.severity.keyword", "size": 1}
                    }
                },
            }
        },
    }
    results = await fetch_opensearch(query)
    aggs = results["aggregations"]["advisories"]["buckets"]
    severities = {"Negligible": "0", "Low": "1", "Medium": "2", "High": "3", "Critical": "4"}
    return {agg["key"]: severities[agg["severity"]["buckets"][0]["key"]] for agg in aggs}
