import sqlite3

from skims_ai.classifies.sca_advisory_db import DB_LOCAL_PATH, download_skims_sca_db


def list_cve_by_alternative_id() -> dict[str, str]:
    download_skims_sca_db()
    with sqlite3.connect(DB_LOCAL_PATH) as conn:
        cursor = conn.cursor()
        cursor.execute("SELECT alternative_id, adv_id FROM advisories")
        alternative_id_by_cve = cursor.fetchall()
        return {item[0]: item[1] for item in alternative_id_by_cve}


def list_cves_descriptions() -> dict[str, str]:
    with sqlite3.connect(DB_LOCAL_PATH) as conn:
        cursor = conn.cursor()
        cursor.execute("SELECT adv_id, details FROM advisories")
        alternative_id_by_cve = cursor.fetchall()
        return {item[0]: item[1] for item in alternative_id_by_cve}
