import json
import os

from google.auth.exceptions import RefreshError
from google.oauth2.credentials import Credentials
from googleapiclient.discovery import build
from googleapiclient.errors import HttpError

from skims_ai.config.logger import (
    LOGGER,
)
from skims_ai.prioritizes.defs import EnrichedCVE, Sheet, SheetColumn
from skims_ai.prioritizes.utils import (
    get_hyperlink,
)

GOOGLE_SHEETS_ID = os.getenv("GOOGLE_SHEETS_ID")
GOOGLE_SHEETS_TOKEN = json.loads(os.getenv("GOOGLE_SHEETS_TOKEN", "{}"))
DATA_RANGE = "A3:J"
IDS_RANGE = "B3:B"


def get_prev_analyzed_advisories(sheet: str) -> list[EnrichedCVE]:
    credentials = Credentials.from_authorized_user_info(info=GOOGLE_SHEETS_TOKEN)
    service = build("sheets", "v4", credentials=credentials)
    candidates = (
        service.spreadsheets()
        .values()
        .get(
            spreadsheetId=GOOGLE_SHEETS_ID,
            range=f"{sheet}!{DATA_RANGE}",
            valueRenderOption="FORMATTED_VALUE",
        )
        .execute()
    )
    rows = candidates.get("values", [])
    return [
        EnrichedCVE(
            is_candidate=sheet == "candidates",
            affected_groups=row[SheetColumn.AFFECTED_GROUPS.value],
            sbom_cve_id=row[SheetColumn.SBOM_CVE_ID.value],
            cve_id=row[SheetColumn.CVE.value],
            url=get_hyperlink(row[SheetColumn.CVE.value]),
            pkg=row[SheetColumn.PKG.value],
            finding=row[SheetColumn.FINDING.value],
            language=row[SheetColumn.LANGUAGE.value],
            severity=row[SheetColumn.SEVERITY.value],
            under_review_by=row[SheetColumn.UNDER_REVIEW_BY.value],
            status=row[SheetColumn.STATUS.value],
            reason=row[SheetColumn.REASON.value],
        )
        for row in rows
    ]


def get_prev_analyzed_advisories_ids() -> list[str]:
    credentials = Credentials.from_authorized_user_info(info=GOOGLE_SHEETS_TOKEN)
    service = build("sheets", "v4", credentials=credentials)
    candidates = (
        service.spreadsheets()
        .values()
        .get(
            spreadsheetId=GOOGLE_SHEETS_ID,
            range=f"{Sheet.CANDIDATES.value}!{IDS_RANGE}",
            valueRenderOption="FORMATTED_VALUE",
        )
        .execute()
    )
    discarded = (
        service.spreadsheets()
        .values()
        .get(
            spreadsheetId=GOOGLE_SHEETS_ID,
            range=f"{Sheet.DISCARDED.value}!{IDS_RANGE}",
            valueRenderOption="FORMATTED_VALUE",
        )
        .execute()
    )
    return [
        row[0] for row in candidates.get("values", []) + discarded.get("values", []) if len(row) > 0
    ]


def clear_prev_analyzed_advisories(sheet: str) -> None:
    LOGGER.info("Clearing sheet %s", sheet)
    credentials = Credentials.from_authorized_user_info(info=GOOGLE_SHEETS_TOKEN)
    service = build("sheets", "v4", credentials=credentials)
    (
        service.spreadsheets()
        .values()
        .clear(
            spreadsheetId=GOOGLE_SHEETS_ID,
            range=f"{sheet}!{DATA_RANGE}",
        )
        .execute()
    )


def upload_to_google_sheets(
    sheet: str,
    cves: list[list[str]],
) -> None:
    credentials = Credentials.from_authorized_user_info(info=GOOGLE_SHEETS_TOKEN)
    service = build("sheets", "v4", credentials=credentials)
    try:
        clear_prev_analyzed_advisories(sheet=sheet)
        body = {"values": cves}
        (
            service.spreadsheets()
            .values()
            .append(
                spreadsheetId=GOOGLE_SHEETS_ID,
                range=f"{sheet}!{DATA_RANGE}",
                valueInputOption="USER_ENTERED",
                insertDataOption="INSERT_ROWS",
                body=body,
            )
            .execute()
        )
        LOGGER.info("CVEs uploaded to Google Sheets")
    except (RefreshError, HttpError, Exception):
        LOGGER.exception("Error uploading CVE automation candidates to Google Sheets.")
