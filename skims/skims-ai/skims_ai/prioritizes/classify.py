import json
from asyncio import Semaphore, gather
from dataclasses import replace

from openai import BadRequestError

from skims_ai.ai.open_ai import assistant
from skims_ai.config.logger import (
    LOGGER,
)
from skims_ai.prioritizes.defs import EnrichedCVE
from skims_ai.prioritizes.utils import get_context_messages, get_cve_status, get_reason


async def is_automation_candidate(cve: EnrichedCVE) -> EnrichedCVE:
    if cve.is_candidate is not None:
        return cve

    try:
        context_messages = await get_context_messages()
        classification = await assistant(context_messages=context_messages, prompt=cve.description)
        classification_json = json.loads(classification)
    except (json.JSONDecodeError, BadRequestError):
        LOGGER.exception(
            "Error in OpenAI API with CVE_ID: %s prompt: '%s'", cve.cve_id, cve.description
        )
        return cve
    if "automatable" in classification_json:
        automatable = classification_json["automatable"]
        status = get_cve_status(automatable=automatable)

        reason = get_reason(automatable=automatable, status=status)
        LOGGER.info(
            "CVE with ID %s was analyzed. Result: (automatable: %s)", cve.cve_id, automatable
        )
        return replace(cve, is_candidate=automatable, status=status, reason=reason)
    return cve


async def classify_cves(cves: list[EnrichedCVE]) -> list[EnrichedCVE]:
    semaphore = Semaphore(50)

    async def classify_cves_semaphore(cve: EnrichedCVE) -> EnrichedCVE:
        async with semaphore:
            return await is_automation_candidate(cve=cve)

    classification_tasks = [classify_cves_semaphore(cve) for cve in cves]
    return await gather(*classification_tasks)
