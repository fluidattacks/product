import json
import re
import sqlite3
import subprocess
from pathlib import (
    Path,
)
from typing import (
    NamedTuple,
    cast,
)

import boto3
from botocore import (
    UNSIGNED,
)
from botocore.config import (
    Config,
)

from skims_ai.config.logger import (
    LOGGER,
)

DB_NAME = "skims_sca_advisories.db"
DB_COMPRESSED_NAME = f"{DB_NAME}.zst"
DB_LOCAL_PATH = f"./outputs/{DB_NAME}"
DB_COMPRESSED_LOCAL_PATH = f"{DB_LOCAL_PATH}.zst"

S3_CLIENT = boto3.client(
    service_name="s3",
    config=Config(
        region_name="us-east-1",
        signature_version=UNSIGNED,
    ),
)
ERR_MSG = "Unable to find the DB at the provided path. Adjust it or execute in debug mode"


class CveData(NamedTuple):
    cve_id: str
    description: str
    cwe_ids: set[str]


MAX_WORDS = 1000


def clean_text(text: str) -> str:
    text = text.lower()
    text = re.sub(r"https?://\S+|www\.\S+", "", text)
    text = re.sub(r"<.*?>", "", text)
    text = text.replace("#", "").replace("\n", " ")
    text = re.sub(r"\s+", " ", text).strip()
    words = text.split()
    if len(words) > MAX_WORDS:
        return " ".join(words[:1000])
    return text


def download_skims_sca_db() -> None:
    LOGGER.info("Downloading and decompressing SKIMS.SCA advisories database from s3")
    S3_CLIENT.download_file(
        Bucket="skims.sca",
        Key=DB_COMPRESSED_NAME,
        Filename=DB_COMPRESSED_LOCAL_PATH,
    )
    with subprocess.Popen(  # noqa:S603
        ["zstd", "-d", "-f", DB_COMPRESSED_LOCAL_PATH],  # noqa:S607
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
    ) as process:
        _, stderr = process.communicate()
        if cast(int, process.returncode) != 0:
            raise RuntimeError(stderr.decode())


def existing_classified_advisories() -> list[str]:
    cve_data_path = Path("../static/cve_findings/cve_findings.json")
    if cve_data_path.is_file():
        with cve_data_path.open("rb") as f:
            return json.load(f)
    return []


def get_advisory_data(db_path: str | None, *, debug: bool) -> dict[str, CveData]:
    if db_path is None or not Path(db_path).is_file():
        if not debug:
            LOGGER.error(ERR_MSG)
            raise ValueError

        if not Path(DB_LOCAL_PATH).is_file():
            download_skims_sca_db()

        LOGGER.info("Using DB found locally.")

    conn = sqlite3.connect(db_path or DB_LOCAL_PATH)
    cursor = conn.cursor()
    cursor.execute(
        """
        SELECT adv_id, cwe_ids, details
        FROM advisories
        WHERE cve_finding IS NULL and vulnerable_version != "<0" and details IS NOT NULL
    """
    )
    classified_cve = existing_classified_advisories()

    advisory_dict: dict[str, CveData] = {}
    for adv_id, cwe_ids, details in cursor.fetchall():
        if adv_id in classified_cve:
            continue
        clean_details = clean_text(details)

        if adv_id not in advisory_dict or len(clean_details) >= len(
            advisory_dict[adv_id].description
        ):
            advisory_dict[adv_id] = CveData(
                cve_id=adv_id,
                description=clean_details,
                cwe_ids=set(json.loads(cwe_ids)) if cwe_ids else set(),
            )
    conn.close()
    return advisory_dict


def update_advisory_data(new_cve_classification: list[dict], db_path: str | None) -> None:
    conn = sqlite3.connect(db_path or DB_LOCAL_PATH)
    cursor = conn.cursor()
    for cve_results in new_cve_classification:
        cursor.execute(
            "UPDATE advisories SET cve_finding = ? WHERE adv_id = ?",
            (cve_results["fin_code"], cve_results["cve_id"]),
        )
    conn.commit()
    conn.close()
