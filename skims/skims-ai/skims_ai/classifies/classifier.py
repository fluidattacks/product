import csv
import json
from pathlib import Path

import numpy as np

from skims_ai.classifies.ai_queries import (
    get_embeddings,
    obtain_best_cwe_match,
    verify_best_xss_match,
)
from skims_ai.classifies.criteria_data import PRIORITIZED_FINDINGS, get_criteria_data
from skims_ai.classifies.sca_advisory_db import (
    CveData,
    get_advisory_data,
    update_advisory_data,
)
from skims_ai.config.logger import (
    LOGGER,
)

CVE_EMBEDDINGS_LOCAL = "./outputs/cve_embeddings.json"

FORBIDDEN_CWE = {
    "CWE-1",
    "CWE-2",
    "CWE-3",
    "CWE-4",
    "CWE-10",
    "CWE-16",
    "CWE-17",
    "CWE-18",
    "CWE-19",
    "CWE-21",
    "CWE-60",
    "CWE-63",
    "CWE-68",
    "CWE-70",
    "CWE-71",
    "CWE-92",
    "CWE-100",
    "CWE-101",
    "CWE-132",
    "CWE-133",
    "CWE-136",
    "CWE-137",
    "CWE-139",
    "CWE-169",
    "CWE-171",
    "CWE-189",
    "CWE-199",
    "CWE-216",
    "CWE-217",
    "CWE-218",
    "CWE-225",
    "CWE-227",
    "CWE-247",
    "CWE-249",
    "CWE-251",
    "CWE-254",
    "CWE-255",
    "CWE-264",
    "CWE-265",
    "CWE-275",
    "CWE-292",
    "CWE-310",
    "CWE-320",
    "CWE-355",
    "CWE-361",
    "CWE-365",
    "CWE-371",
    "CWE-373",
    "CWE-376",
    "CWE-380",
    "CWE-381",
    "CWE-387",
    "CWE-388",
    "CWE-389",
    "CWE-391",
    "CWE-398",
    "CWE-399",
    "CWE-411",
    "CWE-417",
    "CWE-418",
    "CWE-423",
    "CWE-429",
    "CWE-438",
    "CWE-442",
    "CWE-443",
    "CWE-445",
    "CWE-452",
    "CWE-458",
    "CWE-461",
    "CWE-465",
    "CWE-485",
    "CWE-490",
    "CWE-503",
    "CWE-504",
    "CWE-505",
    "CWE-513",
    "CWE-516",
    "CWE-517",
    "CWE-518",
    "CWE-519",
    "CWE-533",
    "CWE-534",
    "CWE-542",
    "CWE-545",
    "CWE-557",
    "CWE-559",
    "CWE-569",
    "CWE-592",
    "CWE-596",
    "CWE-604",
    "CWE-629",
    "CWE-630",
    "CWE-631",
    "CWE-632",
    "CWE-633",
    "CWE-634",
    "CWE-635",
    "CWE-658",
    "CWE-659",
    "CWE-660",
    "CWE-661",
    "CWE-677",
    "CWE-678",
    "CWE-679",
    "CWE-699",
    "CWE-700",
    "CWE-701",
    "CWE-702",
    "CWE-709",
    "CWE-711",
    "CWE-712",
    "CWE-713",
    "CWE-714",
    "CWE-715",
    "CWE-716",
    "CWE-717",
    "CWE-718",
    "CWE-719",
    "CWE-720",
    "CWE-721",
    "CWE-722",
    "CWE-723",
    "CWE-724",
    "CWE-725",
    "CWE-726",
    "CWE-727",
    "CWE-728",
    "CWE-729",
    "CWE-730",
    "CWE-731",
    "CWE-734",
    "CWE-735",
    "CWE-736",
    "CWE-737",
    "CWE-738",
    "CWE-739",
    "CWE-740",
    "CWE-741",
    "CWE-742",
    "CWE-743",
    "CWE-744",
    "CWE-745",
    "CWE-746",
    "CWE-747",
    "CWE-748",
    "CWE-750",
    "CWE-751",
    "CWE-752",
    "CWE-753",
    "CWE-769",
    "CWE-800",
    "CWE-801",
    "CWE-802",
    "CWE-803",
    "CWE-808",
    "CWE-809",
    "CWE-810",
    "CWE-811",
    "CWE-812",
    "CWE-813",
    "CWE-814",
    "CWE-815",
    "CWE-816",
    "CWE-817",
    "CWE-818",
    "CWE-819",
    "CWE-840",
    "CWE-844",
    "CWE-845",
    "CWE-846",
    "CWE-847",
    "CWE-848",
    "CWE-849",
    "CWE-850",
    "CWE-851",
    "CWE-852",
    "CWE-853",
    "CWE-854",
    "CWE-855",
    "CWE-856",
    "CWE-857",
    "CWE-858",
    "CWE-859",
    "CWE-860",
    "CWE-861",
    "CWE-864",
    "CWE-865",
    "CWE-866",
    "CWE-867",
    "CWE-868",
    "CWE-869",
    "CWE-870",
    "CWE-871",
    "CWE-872",
    "CWE-873",
    "CWE-874",
    "CWE-875",
    "CWE-876",
    "CWE-877",
    "CWE-878",
    "CWE-879",
    "CWE-880",
    "CWE-881",
    "CWE-882",
    "CWE-883",
    "CWE-884",
    "CWE-885",
    "CWE-886",
    "CWE-887",
    "CWE-888",
    "CWE-889",
    "CWE-890",
    "CWE-891",
    "CWE-892",
    "CWE-893",
    "CWE-894",
    "CWE-895",
    "CWE-896",
    "CWE-897",
    "CWE-898",
    "CWE-899",
    "CWE-900",
    "CWE-901",
    "CWE-902",
    "CWE-903",
    "CWE-904",
    "CWE-905",
    "CWE-906",
    "CWE-907",
    "CWE-919",
    "CWE-928",
    "CWE-929",
    "CWE-930",
    "CWE-931",
    "CWE-932",
    "CWE-933",
    "CWE-934",
    "CWE-935",
    "CWE-936",
    "CWE-937",
    "CWE-938",
    "CWE-944",
    "CWE-945",
    "CWE-946",
    "CWE-947",
    "CWE-948",
    "CWE-949",
    "CWE-950",
    "CWE-951",
    "CWE-952",
    "CWE-953",
    "CWE-954",
    "CWE-955",
    "CWE-956",
    "CWE-957",
    "CWE-958",
    "CWE-959",
    "CWE-960",
    "CWE-961",
    "CWE-962",
    "CWE-963",
    "CWE-964",
    "CWE-965",
    "CWE-966",
    "CWE-967",
    "CWE-968",
    "CWE-969",
    "CWE-970",
    "CWE-971",
    "CWE-972",
    "CWE-973",
    "CWE-974",
    "CWE-975",
    "CWE-976",
    "CWE-977",
    "CWE-978",
    "CWE-979",
    "CWE-980",
    "CWE-981",
    "CWE-982",
    "CWE-983",
    "CWE-984",
    "CWE-985",
    "CWE-986",
    "CWE-987",
    "CWE-988",
    "CWE-989",
    "CWE-990",
    "CWE-991",
    "CWE-992",
    "CWE-993",
    "CWE-994",
    "CWE-995",
    "CWE-996",
    "CWE-997",
    "CWE-998",
    "CWE-999",
    "CWE-1000",
    "CWE-1001",
    "CWE-1002",
    "CWE-1003",
    "CWE-1005",
    "CWE-1006",
    "CWE-1008",
    "CWE-1009",
    "CWE-1010",
    "CWE-1011",
    "CWE-1012",
    "CWE-1013",
    "CWE-1014",
    "CWE-1015",
    "CWE-1016",
    "CWE-1017",
    "CWE-1018",
    "CWE-1019",
    "CWE-1020",
    "CWE-1026",
    "CWE-1027",
    "CWE-1028",
    "CWE-1029",
    "CWE-1030",
    "CWE-1031",
    "CWE-1032",
    "CWE-1033",
    "CWE-1034",
    "CWE-1035",
    "CWE-1036",
    "CWE-1040",
    "CWE-1041",
    "CWE-1042",
    "CWE-1043",
    "CWE-1044",
    "CWE-1047",
    "CWE-1048",
    "CWE-1051",
    "CWE-1053",
    "CWE-1054",
    "CWE-1055",
    "CWE-1056",
    "CWE-1057",
    "CWE-1059",
    "CWE-1060",
    "CWE-1062",
    "CWE-1063",
    "CWE-1064",
    "CWE-1065",
    "CWE-1066",
    "CWE-1068",
    "CWE-1069",
    "CWE-1070",
    "CWE-1072",
    "CWE-1073",
    "CWE-1074",
    "CWE-1076",
    "CWE-1078",
    "CWE-1080",
    "CWE-1081",
    "CWE-1082",
    "CWE-1083",
    "CWE-1084",
    "CWE-1085",
    "CWE-1086",
    "CWE-1090",
    "CWE-1092",
    "CWE-1094",
    "CWE-1095",
    "CWE-1097",
    "CWE-1099",
    "CWE-1101",
    "CWE-1103",
    "CWE-1105",
    "CWE-1106",
    "CWE-1107",
    "CWE-1109",
    "CWE-1110",
    "CWE-1111",
    "CWE-1112",
    "CWE-1113",
    "CWE-1114",
    "CWE-1115",
    "CWE-1117",
    "CWE-1118",
    "CWE-1119",
    "CWE-1121",
    "CWE-1122",
    "CWE-1124",
    "CWE-1125",
    "CWE-1128",
    "CWE-1129",
    "CWE-1130",
    "CWE-1131",
    "CWE-1132",
    "CWE-1133",
    "CWE-1134",
    "CWE-1135",
    "CWE-1136",
    "CWE-1137",
    "CWE-1138",
    "CWE-1139",
    "CWE-1140",
    "CWE-1141",
    "CWE-1142",
    "CWE-1143",
    "CWE-1144",
    "CWE-1145",
    "CWE-1146",
    "CWE-1147",
    "CWE-1148",
    "CWE-1149",
    "CWE-1150",
    "CWE-1151",
    "CWE-1152",
    "CWE-1153",
    "CWE-1154",
    "CWE-1155",
    "CWE-1156",
    "CWE-1157",
    "CWE-1158",
    "CWE-1159",
    "CWE-1160",
    "CWE-1161",
    "CWE-1162",
    "CWE-1163",
    "CWE-1165",
    "CWE-1166",
    "CWE-1167",
    "CWE-1168",
    "CWE-1169",
    "CWE-1170",
    "CWE-1171",
    "CWE-1172",
    "CWE-1175",
    "CWE-1178",
    "CWE-1179",
    "CWE-1180",
    "CWE-1181",
    "CWE-1182",
    "CWE-1183",
    "CWE-1184",
    "CWE-1185",
    "CWE-1186",
    "CWE-1187",
    "CWE-1194",
    "CWE-1195",
    "CWE-1196",
    "CWE-1197",
    "CWE-1198",
    "CWE-1199",
    "CWE-1200",
    "CWE-1201",
    "CWE-1202",
    "CWE-1203",
    "CWE-1205",
    "CWE-1206",
    "CWE-1207",
    "CWE-1208",
    "CWE-1210",
    "CWE-1211",
    "CWE-1212",
    "CWE-1213",
    "CWE-1214",
    "CWE-1215",
    "CWE-1216",
    "CWE-1217",
    "CWE-1218",
    "CWE-1219",
    "CWE-1225",
    "CWE-1226",
    "CWE-1227",
    "CWE-1228",
    "CWE-1237",
    "CWE-1238",
    "CWE-1305",
    "CWE-1306",
    "CWE-1307",
    "CWE-1308",
    "CWE-1309",
    "CWE-1324",
    "CWE-1337",
    "CWE-1340",
    "CWE-1343",
    "CWE-1344",
    "CWE-1345",
    "CWE-1346",
    "CWE-1347",
    "CWE-1348",
    "CWE-1349",
    "CWE-1350",
    "CWE-1352",
    "CWE-1353",
    "CWE-1354",
    "CWE-1355",
    "CWE-1356",
    "CWE-1358",
    "CWE-1359",
    "CWE-1360",
    "CWE-1361",
    "CWE-1362",
    "CWE-1363",
    "CWE-1364",
    "CWE-1365",
    "CWE-1366",
    "CWE-1367",
    "CWE-1368",
    "CWE-1369",
    "CWE-1370",
    "CWE-1371",
    "CWE-1372",
    "CWE-1373",
    "CWE-1374",
    "CWE-1375",
    "CWE-1376",
    "CWE-1377",
    "CWE-1378",
    "CWE-1379",
    "CWE-1380",
    "CWE-1381",
    "CWE-1382",
    "CWE-1383",
    "CWE-1387",
    "CWE-1388",
    "CWE-1396",
    "CWE-1397",
    "CWE-1398",
    "CWE-1399",
    "CWE-1400",
    "CWE-1401",
    "CWE-1402",
    "CWE-1403",
    "CWE-1404",
    "CWE-1405",
    "CWE-1406",
    "CWE-1407",
    "CWE-1408",
    "CWE-1409",
    "CWE-1410",
    "CWE-1411",
    "CWE-1412",
    "CWE-1413",
    "CWE-1414",
    "CWE-1415",
    "CWE-1416",
    "CWE-1417",
    "CWE-1418",
    "CWE-1424",
    "CWE-1425",
    "CWE-1430",
    "CWE-2000",
}


def dump_csv_to_json() -> None:
    cve_data_path = Path("./outputs/findings_cwe.csv")
    with cve_data_path.open(newline="", encoding="utf-8") as file:
        reader = csv.reader(file)
        findings_json = {row[0]: list({value.strip() for value in row[1:]}) for row in reader}

    with Path("./outputs/findings_cwe.json").open(mode="w") as handler:
        json.dump(findings_json, handler, indent=2)


async def query_cve_embeddings(cves_data: dict[str, CveData]) -> dict[str, list[float]]:
    cves_queries = {cve_id: cve_data.description for cve_id, cve_data in cves_data.items()}
    cve_embeddings = await get_embeddings(cves_queries)
    with Path("./outputs/cve_embeddings.json").open(  # noqa:ASYNC230
        mode="w"
    ) as handler:
        json.dump(cve_embeddings, handler, indent=2)
    return cve_embeddings


async def get_cve_embeddings(cves_data: dict[str, CveData]) -> dict[str, list[float]]:
    local_path = Path(CVE_EMBEDDINGS_LOCAL)
    if local_path.is_file():
        with local_path.open("rb") as f:  # noqa: ASYNC230
            return json.load(f)
    return await query_cve_embeddings(cves_data)


def cosine_similarity(a: list[float], b: list[float]) -> float:
    return np.dot(a, b) / (np.linalg.norm(a) * np.linalg.norm(b))


def get_finding_best_match(
    filtered_fin_codes: list[str],
    findings_embeddings: dict[str, list[float]],
    cve_embeddings: list[float],
) -> str:
    filtered_findings = {
        fin_title: fin_embeddings
        for fin_title, fin_embeddings in findings_embeddings.items()
        if fin_title in filtered_fin_codes
    }

    criteria_embeddings = list(filtered_findings.values())
    fin_codes = list(filtered_findings.keys())
    embeddings_distances = [
        cosine_similarity(cve_embeddings, criteria_embedding)
        for criteria_embedding in criteria_embeddings
    ]
    max_idx = np.argmax(embeddings_distances)
    return fin_codes[max_idx]


def get_related_cwe_findings(
    cve_cwes: set[str] | None, criteria_cwe: dict[str, set[str]]
) -> list[str]:
    filtered_findings = None
    if cve_cwes and (
        matched_findings := {
            fin_code: len(common_cwe)
            for fin_code, fin_cwe_ids in criteria_cwe.items()
            if (common_cwe := fin_cwe_ids.intersection(set(cve_cwes))) and len(common_cwe) > 0
        }
    ):
        max_value = max(matched_findings.values())
        most_common_findings = [
            fin_code for fin_code, value in matched_findings.items() if value == max_value
        ]
        filtered_findings = (
            [fin_code for fin_code in most_common_findings if fin_code in PRIORITIZED_FINDINGS]
            if len(most_common_findings) > 1
            else most_common_findings
        )

    if filtered_findings:
        return filtered_findings
    return PRIORITIZED_FINDINGS


async def apply_special_cases(fin_best_match: str, cve_details: str) -> str:
    if fin_best_match in ["F008", "F425"]:
        return await verify_best_xss_match(fin_best_match, cve_details)
    return fin_best_match


async def classify_cves_to_findings(db_path: str | None, *, debug: bool = False) -> None:
    cves_to_classify = get_advisory_data(db_path, debug=debug)
    if not cves_to_classify:
        LOGGER.info("All existing advisories have an assigned finding")
        return
    cves_embeddings = await get_cve_embeddings(cves_to_classify)

    criteria_data = await get_criteria_data()
    criteria_cwe = {fin_code: fin_data.fin_cwe for fin_code, fin_data in criteria_data.items()}
    criteria_embeddings = {
        fin_code: fin_data.fin_embeddings for fin_code, fin_data in criteria_data.items()
    }

    unique_criteria_cwe = set.union(*criteria_cwe.values())
    cwes_not_in_criteria = set()
    results = []
    for cve_id, cve_embeddings in cves_embeddings.items():
        LOGGER.info("Processing CVE %s", cve_id)

        cve_cwes = cves_to_classify[cve_id].cwe_ids - FORBIDDEN_CWE
        if not cve_cwes:
            LOGGER.info("Found an advisory without CWE %s", cve_id)
            cve_cwes = await obtain_best_cwe_match(cve_id, cves_to_classify[cve_id].description)
        cwes_not_in_criteria.update(cve_cwes - unique_criteria_cwe - FORBIDDEN_CWE)

        filtered_findings = get_related_cwe_findings(cve_cwes, criteria_cwe)

        criteria_best_match = (
            filtered_findings[0]
            if len(filtered_findings) == 1
            else get_finding_best_match(filtered_findings, criteria_embeddings, cve_embeddings)
        )

        adjusted_best_match = await apply_special_cases(
            criteria_best_match, cves_to_classify[cve_id].description
        )
        results.append(
            {
                "cve_id": cve_id,
                "cve_description": cves_to_classify[cve_id].description,
                "cwe_ids": cve_cwes,
                "fin_code": adjusted_best_match,
            }
        )
    if debug:
        with Path("./outputs/cve_data_with_findings.csv").open(  # noqa:ASYNC230
            "w"
        ) as handler:
            fieldnames = results[0].keys()
            writer = csv.DictWriter(handler, fieldnames=fieldnames)
            writer.writeheader()
            writer.writerows(results)

    if cwes_not_in_criteria:
        LOGGER.error("CWES found not mapped to criteria %s", cwes_not_in_criteria)
    update_advisory_data(results, db_path)
