from openai import OpenAIError

from skims_ai.ai.client import openai_client
from skims_ai.config.logger import (
    LOGGER,
)


async def assistant(  # noqa: PLR0913
    context_messages: list[dict[str, str]],
    prompt: str,
    model: str = "gpt-4o-mini",
    temperature: float = 0.0,
    top_p: float = 0.0,
    max_tokens: int = 200,
) -> str:
    messages = [*context_messages, {"role": "user", "content": prompt}]
    try:
        openai_response = await openai_client.chat.completions.create(
            model=model,
            messages=messages,  # type: ignore[arg-type]
            temperature=temperature,
            top_p=top_p,
            n=1,
            stream=False,
            max_tokens=max_tokens,
        )
        return openai_response.choices[0].message.content  # type: ignore[union-attr,return-value]
    except OpenAIError as e:
        LOGGER.exception("OpenAI API error: %s | Prompt: %s | Model: %s", e, prompt, model)
        return "{}"
