from unittest.mock import patch

import pytest

from skims_ai.prioritizes import fill_base_fields, get_new_and_prev_cves, get_new_cves
from skims_ai.prioritizes.defs import EnrichedCVE, Sheet

MODULE_PATH = "skims_ai.prioritizes"


@pytest.mark.asyncio
async def test_get_new_cves() -> None:
    with (
        patch(
            f"{MODULE_PATH}.get_prev_analyzed_advisories_ids",
            return_value=["cve-a", "cve-b", "cve-c"],
        ),
        patch(
            f"{MODULE_PATH}.get_all_sbom_advisories",
            return_value=["cve-a", "cve-d", "cve-e"],
        ),
    ):
        result = await get_new_cves(top=-1)
        assert result == [EnrichedCVE(sbom_cve_id="cve-d"), EnrichedCVE(sbom_cve_id="cve-e")]


@pytest.mark.asyncio
async def test_get_new_and_prev_cves() -> None:
    prev_candidates_mock = [EnrichedCVE(sbom_cve_id="cve-a"), EnrichedCVE(sbom_cve_id="cve-b")]
    prev_discarded_mock = [EnrichedCVE(sbom_cve_id="cve-c"), EnrichedCVE(sbom_cve_id="cve-e")]
    new_cves_mock = [EnrichedCVE(sbom_cve_id="cve-f"), EnrichedCVE(sbom_cve_id="cve-g")]
    with (
        patch(f"{MODULE_PATH}.get_prev_analyzed_advisories") as get_prev_mock,
        patch(f"{MODULE_PATH}.get_new_cves", return_value=new_cves_mock),
    ):
        get_prev_mock.side_effect = lambda sheet: {
            Sheet.CANDIDATES.value: prev_candidates_mock,
            Sheet.DISCARDED.value: prev_discarded_mock,
        }[sheet]
        result = await get_new_and_prev_cves(top=100)
        assert result == prev_candidates_mock + prev_discarded_mock + new_cves_mock


enriched_cves = [
    EnrichedCVE(sbom_cve_id="GHSA-e"),
    EnrichedCVE(sbom_cve_id="CVE-a"),
    EnrichedCVE(sbom_cve_id="CVE-b"),
    EnrichedCVE(sbom_cve_id="CVE-c"),
]


@pytest.mark.asyncio
async def test_fill_base_fields() -> None:
    with (
        patch(
            f"{MODULE_PATH}.get_advisories_pkgs",
            return_value={"CVE-a": "pkg1", "CVE-b": "pkg2", "CVE-c": "pkg2", "GHSA-e": "pkg2"},
        ),
        patch(
            f"{MODULE_PATH}.get_groups_by_pkg",
            return_value={"pkg1": "10", "pkg2": "5"},
        ),
        patch(
            f"{MODULE_PATH}.get_advisories_languages",
            return_value={"CVE-a": "lang-1", "CVE-b": "lang-2", "CVE-c": "unknown_language"},
        ),
        patch(
            f"{MODULE_PATH}.get_advisories_descriptions",
            return_value={"CVE-a": "des 1", "CVE-b": "des 2", "CVE-c": "des 3", "GHSA-E": "des e"},
        ),
        patch(
            f"{MODULE_PATH}.list_cves_descriptions",
            return_value={"CVE-e": "cve-e des"},
        ),
        patch(
            f"{MODULE_PATH}.get_advisories_severity",
            return_value={"CVE-a": "4", "CVE-b": "3", "CVE-c": "2", "GHSA-e": "3"},
        ),
        patch(
            f"{MODULE_PATH}.get_covered_cves",
            return_value={"CVE-a": "F001"},
        ),
        patch(
            f"{MODULE_PATH}.get_findings_by_from_classifier",
            return_value={"CVE-a": "F001", "CVE-b": "F002", "CVE-c": "F002"},
        ),
        patch(
            f"{MODULE_PATH}.list_cve_by_alternative_id",
            return_value={"GHSA-e": "CVE-e"},
        ),
    ):
        result = await fill_base_fields(enriched_cves=enriched_cves)
        assert result == [
            EnrichedCVE(
                sbom_cve_id="GHSA-e",
                cve_id="CVE-e",
                affected_groups="5",
                url="-",
                pkg="pkg2",
                finding="-",
                language="-",
                severity="3",
                is_candidate=None,
                under_review_by="-",
                status="Not implemented",
                reason="-",
                description="cve-e des",
            ),
            EnrichedCVE(
                sbom_cve_id="CVE-a",
                cve_id="CVE-a",
                affected_groups="10",
                url="-",
                pkg="pkg1",
                finding="-",
                language="lang-1",
                severity="4",
                is_candidate=True,
                under_review_by="-",
                status="Implemented",
                reason="Skims method found by Prioritizes",
                description="des 1",
            ),
            EnrichedCVE(
                sbom_cve_id="CVE-b",
                cve_id="CVE-b",
                affected_groups="5",
                url="-",
                pkg="pkg2",
                finding="-",
                language="lang-2",
                severity="3",
                is_candidate=None,
                under_review_by="-",
                status="Not implemented",
                reason="-",
                description="des 2",
            ),
        ]
