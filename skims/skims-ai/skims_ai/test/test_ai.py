from unittest.mock import AsyncMock, MagicMock, patch

import pytest

from skims_ai.ai.open_ai import assistant
from skims_ai.prioritizes.utils import get_context_messages


@pytest.mark.asyncio
async def test_assistant() -> None:
    context_messages = await get_context_messages()
    messages = [
        *context_messages,
        {"role": "user", "content": "test"},
    ]
    mock_choice = MagicMock()
    mock_choice.message.content = '{"automatable": true}'
    with patch(
        "skims_ai.ai.open_ai.openai_client.chat.completions.create",
        new=AsyncMock(return_value=MagicMock(choices=[mock_choice])),
    ) as mock_openai_client:
        result = await assistant(context_messages=context_messages, prompt="test")
        mock_openai_client.assert_called_once_with(
            model="gpt-4o-mini",
            messages=messages,
            temperature=0,
            top_p=0,
            n=1,
            stream=False,
            max_tokens=200,
        )
        assert result == '{"automatable": true}'

    error_response = await assistant(context_messages=context_messages, prompt="test")
    assert error_response == "{}"
