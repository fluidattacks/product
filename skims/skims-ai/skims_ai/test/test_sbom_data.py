from typing import Any
from unittest.mock import AsyncMock, MagicMock, patch

import pytest
from opensearchpy import AsyncHttpConnection

from skims_ai.prioritizes.sbom_data import (
    HOST,
    fetch_opensearch,
    get_advisories_descriptions,
    get_advisories_languages,
    get_advisories_pkgs,
    get_advisories_severity,
    get_all_sbom_advisories,
    get_groups_by_pkg,
)

MODULE_PATH = "skims_ai.prioritizes.sbom_data"


@pytest.mark.asyncio
async def test_fetch_opensearch() -> None:
    query: dict[str, dict[str, Any]] = {"query": {"match_all": {}}}
    expected_response = {"hits": {"total": {"value": 1}, "hits": []}}

    mock_client = AsyncMock()
    mock_client.search.return_value = expected_response

    async_mock_context_manager = AsyncMock()
    async_mock_context_manager.__aenter__.return_value = mock_client
    async_mock_context_manager.__aexit__.return_value = None

    dummy_auth = MagicMock()

    with (
        patch(f"{MODULE_PATH}.AWSV4SignerAsyncAuth", return_value=dummy_auth),
        patch(
            f"{MODULE_PATH}.AsyncOpenSearch", return_value=async_mock_context_manager
        ) as mocked_async_os,
    ):
        result = await fetch_opensearch(query)

        mocked_async_os.assert_called_once_with(
            hosts=[{"host": HOST, "port": 443}],
            http_auth=dummy_auth,
            use_ssl=True,
            verify_certs=True,
            connection_class=AsyncHttpConnection,
            timeout=300,
        )

    mock_client.search.assert_called_once_with(index="pkgs_index", body=query)
    assert result == expected_response


@pytest.mark.asyncio
async def test_get_all_sbom_advisories() -> None:
    mock_response = {
        "aggregations": {
            "unique_advisory_ids": {
                "doc_count_error_upper_bound": 0,
                "sum_other_doc_count": 0,
                "buckets": [
                    {"key": "CVE-2020-1416", "doc_count": 4640},
                    {"key": "GHSA-3r9r-2c2r-c3h9", "doc_count": 3196},
                    {"key": "CVE-2002-1647", "doc_count": 3196},
                    {"key": "GHSA-2rqc-h9c9-5c52", "doc_count": 3196},
                ],
            }
        },
    }
    with patch(
        f"{MODULE_PATH}.fetch_opensearch",
        new=AsyncMock(return_value=mock_response),
    ):
        result = await get_all_sbom_advisories()
    assert result == [
        "CVE-2020-1416",
        "GHSA-3r9r-2c2r-c3h9",
        "CVE-2002-1647",
        "GHSA-2rqc-h9c9-5c52",
    ]


@pytest.mark.asyncio
async def test_get_advisories_languages() -> None:
    mock_response = {
        "aggregations": {
            "cve_ids": {
                "buckets": [
                    {
                        "key": "CVE-2020-1416",
                        "language": {
                            "buckets": [{"key": "javascript"}],
                        },
                    },
                    {
                        "key": "CVE-2002-1647",
                        "language": {
                            "buckets": [{"key": "javascript"}],
                        },
                    },
                ],
            }
        },
    }
    with patch(f"{MODULE_PATH}.fetch_opensearch", new=AsyncMock(return_value=mock_response)):
        result = await get_advisories_languages()
    assert result == {
        "CVE-2020-1416": "javascript",
        "CVE-2002-1647": "javascript",
    }


@pytest.mark.asyncio
async def test_get_advisories_descriptions() -> None:
    mock_response = {
        "aggregations": {
            "cve_ids": {
                "buckets": [
                    {
                        "key": "CVE-2020-1416",
                        "description": {
                            "buckets": [
                                {"key": "An elevation of privilege vuln exists in Visual..."}
                            ],
                        },
                    },
                    {
                        "key": "GHSA-3xgq-45jj-v275",
                        "description": {
                            "buckets": [
                                {"key": "Regular Expression Denial of Service (ReDoS) in..."}
                            ],
                        },
                    },
                ],
            }
        }
    }
    with patch(f"{MODULE_PATH}.fetch_opensearch", new=AsyncMock(return_value=mock_response)):
        result = await get_advisories_descriptions()
    assert result == {
        "CVE-2020-1416": "An elevation of privilege vuln exists in Visual...",
        "GHSA-3xgq-45jj-v275": "Regular Expression Denial of Service (ReDoS) in...",
    }


@pytest.mark.asyncio
async def test_get_advisories_pkgs() -> None:
    mock_response = {
        "aggregations": {
            "cve_ids": {
                "buckets": [
                    {
                        "key": "CVE-2020-1416",
                        "pkg": {
                            "buckets": [{"key": "typescript"}],
                        },
                    },
                    {
                        "key": "CVE-2002-1647",
                        "pkg": {
                            "buckets": [{"key": "slash"}],
                        },
                    },
                ],
            }
        }
    }
    with patch(f"{MODULE_PATH}.fetch_opensearch", new=AsyncMock(return_value=mock_response)):
        result = await get_advisories_pkgs()
    assert result == {
        "CVE-2002-1647": "slash",
        "CVE-2020-1416": "typescript",
    }


@pytest.mark.asyncio
async def test_get_groups_by_pkg() -> None:
    mock_response = {
        "aggregations": {
            "pkg_groups": {
                "buckets": [
                    {"key": "typescript", "unique_group_count": {"value": 294}},
                    {"key": "slash", "unique_group_count": {"value": 286}},
                ],
            }
        }
    }
    with patch(f"{MODULE_PATH}.fetch_opensearch", new=AsyncMock(return_value=mock_response)):
        result = await get_groups_by_pkg()
    assert result == {
        "slash": "286",
        "typescript": "294",
    }


@pytest.mark.asyncio
async def test_get_advisories_severity() -> None:
    mock_response = {
        "aggregations": {
            "advisories": {
                "buckets": [
                    {
                        "key": "CVE-2020-1416",
                        "severity": {
                            "buckets": [{"key": "High"}],
                        },
                    },
                    {
                        "key": "CVE-2002-1647",
                        "severity": {
                            "buckets": [{"key": "Medium"}],
                        },
                    },
                    {
                        "key": "CVE-XXXX-XXXX",
                        "severity": {
                            "buckets": [{"key": "Low"}],
                        },
                    },
                    {
                        "key": "CVE-ZZZZ-ZZZZ",
                        "severity": {
                            "buckets": [{"key": "Critical"}],
                        },
                    },
                ],
            }
        }
    }
    with patch(f"{MODULE_PATH}.fetch_opensearch", new=AsyncMock(return_value=mock_response)):
        result = await get_advisories_severity()
    assert result == {
        "CVE-2020-1416": "3",
        "CVE-2002-1647": "2",
        "CVE-XXXX-XXXX": "1",
        "CVE-ZZZZ-ZZZZ": "4",
    }
