from unittest.mock import MagicMock, patch

import pytest

from skims_ai.prioritizes.defs import EnrichedCVE, Sheet
from skims_ai.prioritizes.google_sheets import (
    DATA_RANGE,
    clear_prev_analyzed_advisories,
    get_prev_analyzed_advisories,
    get_prev_analyzed_advisories_ids,
    upload_to_google_sheets,
)
from skims_ai.prioritizes.utils import get_rows, prioritize_candidates

MODULE_PATH = "skims_ai.prioritizes.google_sheets"


def test_get_prev_analyzed_advisories_ids() -> None:
    with (
        patch(
            f"{MODULE_PATH}.Credentials.from_authorized_user_info",
            MagicMock(universe_domain="googleapis.com"),
        ),
        patch(f"{MODULE_PATH}.build") as mock_build,
    ):
        mock_service = MagicMock()
        mock_spreadsheets = MagicMock()
        mock_values = MagicMock()
        mock_get_candidates = MagicMock()
        mock_get_discarded = MagicMock()

        mock_service.spreadsheets.return_value = mock_spreadsheets
        mock_spreadsheets.values.return_value = mock_values
        mock_values.get.side_effect = [mock_get_candidates, mock_get_discarded]
        mock_get_candidates.execute.return_value = {"values": [["CVE-A"], ["CVE-B"]]}
        mock_get_discarded.execute.return_value = {"values": [["CVE-C"], ["CVE-D"]]}

        mock_build.return_value = mock_service

        result = get_prev_analyzed_advisories_ids()

        assert result == ["CVE-A", "CVE-B", "CVE-C", "CVE-D"]


def test_get_prev_analyzed_advisories() -> None:
    with (
        patch(
            f"{MODULE_PATH}.Credentials.from_authorized_user_info",
            MagicMock(universe_domain="googleapis.com"),
        ),
        patch(f"{MODULE_PATH}.build") as mock_build,
        patch(f"{MODULE_PATH}.GOOGLE_SHEETS_ID", "fake_id") as mock_google_sheets_id,
    ):
        mock_service = MagicMock()
        mock_spreadsheets = MagicMock()
        mock_values = MagicMock()
        mock_get_candidates = MagicMock()

        mock_service.spreadsheets.return_value = mock_spreadsheets
        mock_spreadsheets.values.return_value = mock_values
        mock_values.get.side_effect = [mock_get_candidates]
        mock_get_candidates.execute.return_value = {
            "values": [
                ["10", "CVE-A", "CVE-A", "-", "-", "-", "-", "-", "Not implemented", "-"],
                ["10", "CVE-B", "CVE-B", "-", "-", "-", "-", "-", "Not implemented", "-"],
            ]
        }

        mock_build.return_value = mock_service

        result = get_prev_analyzed_advisories(sheet=Sheet.CANDIDATES.value)

        mock_values.get.assert_called_once_with(
            spreadsheetId=mock_google_sheets_id,
            range=f"candidates!{DATA_RANGE}",
            valueRenderOption="FORMATTED_VALUE",
        )
        assert result == [
            EnrichedCVE(
                affected_groups="10",
                sbom_cve_id="CVE-A",
                cve_id="CVE-A",
                url='=HYPERLINK("https://nvd.nist.gov/vuln/detail/CVE-A", "CVE-A")',
                pkg="-",
                finding="-",
                language="-",
                severity="-",
                is_candidate=True,
            ),
            EnrichedCVE(
                affected_groups="10",
                sbom_cve_id="CVE-B",
                cve_id="CVE-B",
                url='=HYPERLINK("https://nvd.nist.gov/vuln/detail/CVE-B", "CVE-B")',
                pkg="-",
                finding="-",
                language="-",
                severity="-",
                is_candidate=True,
            ),
        ]


def test_clear_prev_analyzed_advisories() -> None:
    with (
        patch(
            f"{MODULE_PATH}.Credentials.from_authorized_user_info",
            MagicMock(universe_domain="googleapis.com"),
        ),
        patch(f"{MODULE_PATH}.build") as mock_build,
        patch(f"{MODULE_PATH}.GOOGLE_SHEETS_ID", "fake_id") as mock_google_sheets_id,
    ):
        clear_prev_analyzed_advisories(sheet=Sheet.CANDIDATES.value)
        mock_build().spreadsheets().values().clear.assert_called_once_with(
            spreadsheetId=mock_google_sheets_id,
            range=f"candidates!{DATA_RANGE}",
        )


@pytest.mark.parametrize(
    ("new_analyzed_cves", "sheet"),
    [
        (
            [
                EnrichedCVE(
                    sbom_cve_id="CVE-2021-26540",
                    cve_id="CVE-2021-26540",
                    affected_groups="100",
                    url='=HYPERLINK("https://nvd.nist.gov/vuln/detail/CVE-2021-26540", "CVE-2021-26540")',  # noqa: E501
                    pkg="pkg1",
                    finding="F060",
                    language="lang1",
                    severity="3",
                    status="Implemented",
                    reason="Skims method found by Prioritize",
                    under_review_by="-",
                    is_candidate=True,
                )
            ],
            Sheet.CANDIDATES.value,
        ),
    ],
)
def test_upload_to_google_sheets(new_analyzed_cves: list[EnrichedCVE], sheet: str) -> None:
    with (
        patch(
            f"{MODULE_PATH}.Credentials.from_authorized_user_info",
            MagicMock(universe_domain="googleapis.com"),
        ),
        patch(f"{MODULE_PATH}.build") as mock_build,
        patch(f"{MODULE_PATH}.GOOGLE_SHEETS_ID", "fake_id") as mock_google_sheets_id,
    ):
        upload_to_google_sheets(
            sheet=sheet,
            cves=get_rows(new_analyzed_cves),
        )
        prioritized_cves = prioritize_candidates(
            cves_list=get_rows(new_analyzed_cves),
        )
        mock_build().spreadsheets().values().append.assert_called_once_with(
            spreadsheetId=mock_google_sheets_id,
            range=f"{sheet}!{DATA_RANGE}",
            valueInputOption="USER_ENTERED",
            insertDataOption="INSERT_ROWS",
            body={"values": prioritized_cves},
        )
