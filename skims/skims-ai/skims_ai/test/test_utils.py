import json
from pathlib import Path
from unittest.mock import patch

import anyio
import pytest

from skims_ai.prioritizes.defs import EnrichedCVE
from skims_ai.prioritizes.utils import (
    classify_advisory_type,
    get_context_messages,
    get_covered_cves,
    get_cve_status,
    get_hyperlink,
    get_reason,
    get_rows,
    prioritize_candidates,
)


@pytest.mark.parametrize(
    ("cve_id"),
    [("CVE-2023-25157"), ("GHSA-5pgg-2q56-6h6h"), ("CVE-2023-25158"), ("OTHER-CVE-ID")],
)
def test_get_hyperlink(
    cve_id: str,
) -> None:
    url = get_hyperlink(cve_id=cve_id)
    if cve_id.startswith("CVE-"):
        assert url == f'=HYPERLINK("https://nvd.nist.gov/vuln/detail/{cve_id}", "{cve_id}")'
    if cve_id.startswith("GHSA-"):
        assert url == f'=HYPERLINK("https://github.com/advisories/{cve_id}", "{cve_id}")'
    if not cve_id.startswith(("CVE-", "GHSA-")):
        assert url == "-"


def test_get_covered_cves() -> None:
    with Path("../skims/lib_sast/reachability/covered_cves.json").open(
        encoding="utf-8"
    ) as covered_cves_file:
        covered_cves = json.load(covered_cves_file)
    assert get_covered_cves() == covered_cves


@pytest.mark.parametrize(
    ("advisory_id", "expected"),
    [
        (
            "CVE-2023-25157",
            {
                "type": "CVE",
                "url": (
                    "https://www.cvedetails.com/api/v1/vulnerability/cve-json?cveId=CVE-2023-25157"
                ),
                "bucket_prefix": "cves",
            },
        ),
        (
            "GHSA-5pgg-2q56-6h6h",
            {
                "type": "GHSA",
                "url": (
                    "https://www.cvedetails.com/api/v1/osv/data-info?dataGuid=GHSA-5pgg-2q56-6h6h"
                ),
                "bucket_prefix": "ghsa-advisories",
            },
        ),
        ("OTHER-CVE-ID", {"type": "Unknown", "url": "-", "bucket_prefix": "-"}),
    ],
)
def test_classify_advisory_type(advisory_id: str, expected: dict[str, str]) -> None:
    result = classify_advisory_type(advisory_id=advisory_id)
    assert result == expected


@pytest.mark.parametrize(
    ("automatable", "expected"),
    [
        (False, "Discarded"),
        (True, "Not implemented"),
    ],
)
def test_get_cve_status(automatable: bool, expected: str) -> None:
    assert get_cve_status(automatable=automatable) == expected


@pytest.mark.parametrize(
    ("automatable", "status", "expected"),
    [
        (True, "Not implemented", "Lack of implementation"),
        (False, "-", "Discarded by Prioritizes"),
        (True, "Implemented", "Skims method found by Prioritize"),
    ],
)
def test_get_reason(automatable: bool, status: str, expected: str) -> None:
    assert get_reason(automatable=automatable, status=status) == expected


MOCK_URL = '=HYPERLINK("https://nvd.nist.gov/vuln/detail/CVE-...", "CVE-...")'
IMP_MOCK_URL = '=HYPERLINK("https://nvd.nist.gov/vuln/detail/CVE-IMP", "CVE-IMP")'


@pytest.mark.parametrize(
    ("cves", "expected"),
    [
        (
            [
                EnrichedCVE(
                    sbom_cve_id="CVE-...", cve_id="CVE-...", affected_groups="5", severity="3"
                ),
                EnrichedCVE(
                    sbom_cve_id="CVE-...", cve_id="CVE-...", affected_groups="1", severity="3"
                ),
                EnrichedCVE(
                    sbom_cve_id="CVE-IMP",
                    cve_id="CVE-IMP",
                    affected_groups="4",
                    severity="4",
                    status="Implemented",
                ),
                EnrichedCVE(
                    sbom_cve_id="CVE-...", cve_id="CVE-...", affected_groups="1", severity="4"
                ),
                EnrichedCVE(
                    sbom_cve_id="CVE-...", cve_id="CVE-...", affected_groups="3", severity="4"
                ),
                EnrichedCVE(
                    sbom_cve_id="CVE-...", cve_id="CVE-...", affected_groups="1", severity="1"
                ),
            ],
            [
                ["5", MOCK_URL, MOCK_URL, "-", "-", "-", "3", "-", "Not implemented", "-"],
                ["3", MOCK_URL, MOCK_URL, "-", "-", "-", "4", "-", "Not implemented", "-"],
                ["1", MOCK_URL, MOCK_URL, "-", "-", "-", "4", "-", "Not implemented", "-"],
                ["1", MOCK_URL, MOCK_URL, "-", "-", "-", "3", "-", "Not implemented", "-"],
                ["1", MOCK_URL, MOCK_URL, "-", "-", "-", "1", "-", "Not implemented", "-"],
                ["4", IMP_MOCK_URL, IMP_MOCK_URL, "-", "-", "-", "4", "-", "Implemented", "-"],
            ],
        )
    ],
)
def test_prioritize_candidates(cves: list[EnrichedCVE], expected: list[list[str]]) -> None:
    with patch("skims_ai.prioritizes.utils.get_covered_cves", return_value={"CVE-IMP": "F002"}):
        assert prioritize_candidates(cves_list=get_rows(cves)) == expected


@pytest.mark.asyncio
async def test_context_messages() -> None:
    ai_msgs_path = "skims_ai/prioritizes/ai_messages"
    system_msg = await anyio.Path(f"{ai_msgs_path}/system_message.md").read_text(encoding="utf-8")
    context = await anyio.Path(f"{ai_msgs_path}/context_messages.json").read_text(encoding="utf-8")
    context_messages = [{"role": "system", "content": system_msg}, *json.loads(context)]
    assert await get_context_messages() == context_messages
