import csv
import io
import json
import os
from collections.abc import (
    Callable,
    Iterable,
)
from contextlib import (
    redirect_stderr,
    redirect_stdout,
)
from itertools import (
    zip_longest,
)
from pathlib import Path
from unittest.mock import (
    patch,
)

from core.cli import (
    cli,
)
from utils.logs import (
    configure,
)


def load_json_results(filepath: str) -> dict:
    with Path(filepath).open(encoding="utf-8") as file:
        return json.load(file)


def _default_snippet_filter(snippet: str) -> str:
    return snippet


def _format_csv(
    content: Iterable[str],
    *,
    snippet_filter: Callable[[str], str],
) -> list[dict[str, str]]:
    result: list[dict[str, str]] = []
    for row in csv.DictReader(content):
        row["snippet"] = snippet_filter(row["snippet"])
        result.append(row)
    result.sort(key=str)
    return result


def _get_suite_expected_results(suite: str, path: str | None = None) -> str:
    if not path:
        return f"skims/test/data/results/{suite}.csv"
    return f"{path}/{suite}.csv"


def _get_suite_produced_results(suite: str) -> str:
    return f"skims/test/outputs/{suite}.csv"


def get_suite_config(suite: str, path: str | None = None) -> str:
    if not path:
        return f"skims/test/all/test_configs/{suite}.yaml"
    return f"{path}/{suite}.yaml"


def create_config(
    finding: str,
    template: str,
) -> str:
    with Path(template).open(encoding="utf-8") as file:
        content = file.read()
        content = content.replace("{FINDING}", str(finding))
        return content.replace("{FINDING_LOWER}", str(finding.lower()))


def skims(*args: str) -> tuple[int, str, str]:
    out_buffer, err_buffer = io.StringIO(), io.StringIO()

    code: int = 0
    with redirect_stdout(out_buffer), redirect_stderr(err_buffer):
        try:
            configure()
            cli.main(args=list(args), prog_name="skims")
        except SystemExit as exc:
            if isinstance(exc.code, int):
                code = exc.code
    try:
        return code, out_buffer.getvalue(), err_buffer.getvalue()
    finally:
        del out_buffer
        del err_buffer


def check_that_csv_results_match(
    suite: str,
    path: str | None = None,
    *,
    snippet_filter: Callable[[str], str] = _default_snippet_filter,
) -> None:
    with Path(_get_suite_produced_results(suite)).open(encoding="utf-8") as produced:
        expected_path = Path(os.environ["STATE"]) / _get_suite_expected_results(suite, path)
        expected_path.parent.mkdir(parents=True, exist_ok=True)
        with Path(expected_path).open("w", encoding="utf-8") as expected:
            expected.write(produced.read())
            produced.seek(0)

        with Path(_get_suite_expected_results(suite, path)).open(encoding="utf-8") as expected:
            for producted_item, expected_item in zip_longest(
                _format_csv(produced, snippet_filter=snippet_filter),
                _format_csv(expected, snippet_filter=snippet_filter),
                fillvalue=None,
            ):
                assert producted_item == expected_item


def run_skims_group(
    suite: str,
    config_path: str,
    results_path: str,
) -> None:
    with patch("lib_cspm.aws.analyze.run_boto3_fun", return_value={}):
        code, stdout, _ = skims("scan", get_suite_config(suite, config_path))
        assert code == 0, stdout
        assert "[INFO] Startup work dir is:" in stdout
        assert "[INFO] An output file has been written:" in stdout
        check_that_csv_results_match(suite, results_path)
