# noqa: INP001
import json

import pytest
import yaml
from pytest_mock import (
    MockerFixture,
)

from test.utils import (
    load_json_results,
    run_skims_group,
    skims,
)

CONFIG_PATH = "skims/test/lib_apk/test_configs"
RESULTS_PATH = "skims/test/lib_apk/results"


@pytest.mark.skims_test_group("apk_findings")
def test_apk_findings() -> None:
    run_skims_group("lib_apk", CONFIG_PATH, RESULTS_PATH)


def check_sarif_results() -> None:
    sarif_data = load_json_results("skims/test/lib_apk/results/lib_apk_sast.sarif")
    produced_results = yaml.safe_load(json.dumps(sarif_data))["runs"][0]

    assert len(produced_results["results"]) == 7

    reported_sast_method = False
    for vuln in produced_results["results"]:
        assert (
            vuln["locations"][0]["physicalLocation"]["artifactLocation"]["uri"]
            == "task_hijacking.apk"
        )
        assert vuln["properties"]["kind"] == "DAST"
        assert vuln["properties"]["vulnerability_type"] == "INPUTS"

        if vuln["properties"]["source_method"] == "java.java_insecure_hash":
            reported_sast_method = True
            assert (
                vuln["locations"][0]["physicalLocation"]["region"]["message"]["text"]
                == "com/fluidattacks/taskhijacking/MainActivity.java#14"
            )

    # Confirm SAST method reported on APK
    assert reported_sast_method


@pytest.mark.skims_test_group("apk_with_sast")
def test_apk_with_sast(mocker: MockerFixture) -> None:
    path = "skims/test/lib_apk/test_configs/lib_apk_sast.yaml"
    with mocker.patch("core.sarif_result.get_repo_branch", return_value="trunk"):
        code, stdout, _ = skims("scan", path)
    assert code == 0, stdout
    assert "[INFO] Startup work dir is:" in stdout

    check_sarif_results()
