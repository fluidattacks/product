# shellcheck shell=bash

function main {
  local success=true
  local pytest_flags=()
  local benchmark_local_repo="${PWD}/../owasp_benchmark"
  export EXPECTED_RESULTS_CSV="${benchmark_local_repo}/expectedresults-1.2.csv"
  source __argExtraSrcs__/template extra_srcs

  for extra_src in "${!extra_srcs[@]}"; do
    copy "${extra_srcs[$extra_src]}" "../${extra_src}"
  done
  pushd skims || return 1
  aws_login "dev" "3600"
  if ! PYTHONPATH="${PWD}/skims:${PYTHONPATH}" pytest \
    "${pytest_flags[@]}" \
    --cov=skims \
    --cov-branch \
    --cov-config=.coveragerc \
    --cov-report=term \
    --skims-test-group=__argCategory__ \
    --capture=tee-sys \
    --disable-pytest-warnings \
    --durations=10 \
    --exitfirst \
    --showlocals \
    --show-capture=no \
    --ignore=skims/skims-ai \
    --ignore=skims/sbom \
    -vvv; then
    success=false
  fi
  mv .coverage .coverage.__argCategory__
  popd || return 1
  if test "${success}" = false; then
    copy "${STATE}" .
    return 1
  fi
}

main "${@}"
