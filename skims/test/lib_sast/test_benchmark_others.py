# noqa: INP001
import pytest

from test.utils import (
    run_skims_group,
)

CONFIG_PATH = "skims/test/lib_sast/test_configs"
RESULTS_PATH = "skims/test/lib_sast/results"


@pytest.mark.skims_test_group("sast_vulnerable_java_app")
def test_sast_vulnerable_java_app() -> None:
    run_skims_group("vulnerableapp", CONFIG_PATH, RESULTS_PATH)


@pytest.mark.skims_test_group("sast_vulnerable_js_app")
def test_sast_vulnerable_js_app() -> None:
    run_skims_group("vulnerable_js_app", CONFIG_PATH, RESULTS_PATH)
