const express = require('express');
const _ = require('lodash');
const app = express();

app.use(express.urlencoded({ extended: true }));

app.post('/greeting', (req, res) => {
  let userInput = req.body.greeting;
  let compiled = _.template(`<h1>${userInput}</h1>`);
  res.send(compiled());
});

app.listen(3000, () => {
  console.log('Server running on port 3000');
});
