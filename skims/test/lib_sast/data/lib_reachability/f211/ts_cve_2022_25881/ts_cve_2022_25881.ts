import { type Request, type Response, type NextFunction } from 'express'
const CachePolicy = require("http-cache-semantics");


module.exports = function vulnFunc () {
  return (req: Request, res: Response, next: NextFunction) => {
    new CachePolicy({
      headers: {},
      }, {
      headers: {
      "cache-control": req.body.cache,
      }})
  }
}

module.exports = function safeFunc () {
  let variable = "some internal var";
  return (req: Request, res: Response, next: NextFunction) => {
    new CachePolicy({
      headers: {},
      }, {
      headers: {
      "cache-control": variable,
      }})
  }
}
