import { argument } from "cross-spawn/lib/util/escape";
import { Request, Response } from "express";

const handler = (req: Request, res: Response) => {
  const text: string = req.params.body;
  argument(text);

  const text2: string = "Sample text";
  argument(text2);

  res.send("Processed");
};

export default handler;
