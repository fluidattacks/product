const CachePolicy = require("http-cache-semantics");

module.exports = function vulnFunc () {
  return (req, res, next) => {
    new CachePolicy({
      headers: {},
      }, {
      headers: {
      "cache-control": req.body.cache,
      }})
  }
}

module.exports = function safeFunc () {
  let variable = "some internal var";
  return (req, res, next) => {
    new CachePolicy({
      headers: {},
      }, {
      headers: {
      "cache-control": variable,
      }})
  }
}
