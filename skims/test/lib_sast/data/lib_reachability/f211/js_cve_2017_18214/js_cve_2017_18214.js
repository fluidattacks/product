const express = require('express');
const moment = require('moment');
const app = express();

app.use(express.urlencoded({ extended: true }));
app.use(express.json());

app.post('/submit-date', (req, res) => {
  const userInput = req.body.date;

  const parsedDate = moment(userInput);

  if (parsedDate.isValid()) {
    res.send(`Valid Date: ${parsedDate.format()}`);
  } else {
    res.send('Invalid Date');
  }
});

app.listen(3000, () => {
  console.log('Running server on port 3000');
});
