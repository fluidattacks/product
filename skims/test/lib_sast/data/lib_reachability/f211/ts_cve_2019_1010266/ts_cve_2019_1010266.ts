import { type Request, type Response, type NextFunction } from 'express'
var _ = require('lodash');

module.exports = function vulnFunc () {
  return (req: Request, res: Response, next: NextFunction) => {
    res.redirect(_.mergeWith(req.params.body))
  }
}

module.exports = function safeFunc () {
  const variable = "some internal var";
  return (req: Request, res: Response, next: NextFunction) => {
    res.redirect(_.mergeWith(variable))
  }
}
