import { type Request, type Response, type NextFunction } from 'express'
import { Sequelize } from 'sequelize';

const sequelize = new Sequelize('database', 'username', 'password', {
    host: 'localhost',
    dialect: 'mysql',
});

async function vulnerableQuery(req: Request) {
    try {
        const result = await sequelize.query(
            'SELECT * FROM users WHERE id = :id',
            {
                replacements: { id: req.id },
                type: sequelize.QueryTypes.SELECT,
                where: {
                    status: 'active',
                },
            }
        );
        console.log(result);
    } catch (error) {
        console.error('Error ejecutando la consulta', error);
    }
}

async function safeQuery(req: Request) {
    try {
        const result = await sequelize.query(
            'SELECT * FROM users WHERE id = :id',
            {
                type: sequelize.QueryTypes.SELECT,
                where: {
                    status: 'active',
                    id: req.id,
                },
            }
        );
        console.log(result);
    } catch (error) {
        console.error('Error ejecutando la consulta', error);
    }
}
