import express, { Request, Response, NextFunction } from "express";
const app = express();

// Vulnerable function
app.get('/', function (req: Request, res: Response, next: NextFunction) {
  res.links({preload: req.query.resource});
  res.send('ok');
});

// Safe function
app.get('/safe', function (req: Request, res: Response, next: NextFunction) {
  res.links({preload: "https://example.com"});
  res.send('ok');
});
