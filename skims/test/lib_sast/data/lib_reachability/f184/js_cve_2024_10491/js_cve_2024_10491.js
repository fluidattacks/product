const express = require("express");
const app = express();

// Vulnerable
app.get("/", function (request, response) {
  response.links({ preload: request.query.resource });
  response.send("ok");
});

// Safe
app.get("/safe", function (request, response) {
  response.links({ preload: "safe" });
  response.send("ok");
});
