const json = require('json');

function vulnFunc (req, res) {
    const code = req.params.code;
    console.log(json.parseLookup(code));
}
    
function safeFunc (req, res) {
    const code = "some internal code";
    console.log(json.parseLookup(code));
}