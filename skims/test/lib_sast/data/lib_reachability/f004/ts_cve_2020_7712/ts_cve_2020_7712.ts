import { type NextFunction, type Request, type Response } from 'express'
const json = require('json');


function vulnFunc ({ content }: Request, res: Response, next: NextFunction) {
  const code = content.code;
  console.log(json.parseLookup(code));
}


function safeFunc ({ content }: Request, res: Response, next: NextFunction) {
  const code = "some internal code";
  console.log(json.parseLookup(code));
}
