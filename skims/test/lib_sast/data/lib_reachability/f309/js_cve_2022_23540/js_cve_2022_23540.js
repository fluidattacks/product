const jwt = require('jsonwebtoken');

function safejwt() {
    let utoken = jwt.sign(payload, key);

    let safe_algo = "PS384";
    let token_secure = jwt.sign(payload, key, {algorithm: safe_algo, issuer: "none"});

    let allowed_algos = ['PS384'];
    const verify_config = { expiresIn: 10000, algorithms:  allowed_algos};
    jwt.verify(token, key, verify_config);

}

export const unsafeJwtDefaultsNone = () => (req, res, next) => {
    const token = req.cookies.token
    if (token) {
      jwt.verify(token, "placeholder-public-key", (err) => {
        if (err === null) {
            res.cookie('token', token)
        }
      })
    }
    next()
  }
