
import { type Request, type Response, type NextFunction } from 'express'
const jwt = require('jsonwebtoken')

export function safeJwt<T extends Object> (payload: T): string {
    const privateKey: string = "mykey";
    let allowed_algos: Array<string> = ['hs256'];
    const options: jwt.SignOptions = { expiresIn: 10000, issuer: "None", algorithms:  allowed_algos};
    try {
      return jwt.sign(payload, privateKey, options)
    } catch (e) {
      throw new Error(e.message)
    }
}

export const unsafeJwtDefaultsNone = () => (req: Request, res: Response, next: NextFunction) => {
  const token = req.cookies.token
  if (token) {
    jwt.verify(token, "placeholder-public-key", (err: Error | null) => {
      if (err === null) {
          res.cookie('token', token)
      }
    })
  }
  next()
}
