# If ._n_support[0] was not modified it's safe

from sklearn import svm
from sklearn import datasets


if __name__ == '__main__':
    X,y = datasets.load_iris(return_X_y=True)
    clf = svm.SVC()
    clf.fit(X, y)
    y_pred = clf.predict(X)
