import { type Request, type Response, type NextFunction } from 'express'
const b = require('braces');

module.exports = function vulnFunc() {
  return (req: Request, res: Response, next: NextFunction) => {
    res.redirect(b(req.params.body))
  }
}

module.exports = function safeFunc() {
  const variable = "some internal var";
  return (req: Request, res: Response, next: NextFunction) => {
    res.redirect(b(variable))
  }
}

module.exports = function sanitizedFunc() {
  return (req: Request, res: Response, next: NextFunction) => {
    const fileExtension = sanitize(req.params.body);
    if (fileExtension) {
      res.redirect(b(req.params.body))
    }
  }
}
