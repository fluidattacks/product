from flask import (
    request,
)
import os
import shlex
import subprocess
import io
from PyPDF2 import PdfFileReader
from PyPDF2.pdf import ContentStream


def vulnerable_process_pdf() -> None:
    file = request.files['pdf_file']
    pdf = PdfFileReader(io.BytesIO(file.read()))

    for page in pdf.pages:
        content_stream = ContentStream(page.getContents(), pdf)


def safe_process_pdf(file_path: str) -> None:
    with open(file_path, 'rb') as f:
        pdf = PdfFileReader(f)

        for page in pdf.pages:
            content_stream = ContentStream(page.getContents(), pdf)
