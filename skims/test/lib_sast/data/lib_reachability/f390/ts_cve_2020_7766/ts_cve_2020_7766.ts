import { Request, Response, NextFunction } from 'express';
import { JsonPointer } from 'json-ptr';


module.exports = function safeFunc() {
  return (req: Request, res: Response, next: NextFunction) => {
    res.redirect(JsonPointer.set(req.params.body));
  };
}

module.exports = function safeFunc2() {
  const variable = "some internal var";
  return (req: Request, res: Response, next: NextFunction) => {
    res.redirect(JsonPointer.set(variable));
  };
}

module.exports = function sanitizedFunc() {
  return (req: Request, res: Response, next: NextFunction) => {
    const fileExtension = sanitize(req.params.body);
    if (fileExtension) {
      res.redirect(JsonPointer.set(req.params.body));
    }
  };
}

module.exports = function vulnerableFlagFunc() {
  return (req: Request, res: Response, next: NextFunction) => {
    const result = JsonPointer.set(
      {},
      req.params.path,
      req.params.value,
      true
    );
    res.json(result);
  };
}

module.exports = function vulnerableIndirectFlagFunc() {
  return (req: Request, res: Response, next: NextFunction) => {
    const allowPrototypePollution = true;
    const result = JsonPointer.set(
      {},
      req.params.path,
      req.params.value,
      allowPrototypePollution
    );
    res.json(result);
  };
}

module.exports = function safeFlagFunc() {
  return (req: Request, res: Response, next: NextFunction) => {
    const result = JsonPointer.set(
      {},
      req.params.path,
      req.params.value
    );
    res.json(result);
  };
}
