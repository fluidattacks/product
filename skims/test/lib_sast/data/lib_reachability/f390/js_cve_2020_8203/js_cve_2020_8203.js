var _ = require('lodash');

module.exports = function vulnFunc () {
  return (req, res, next) => {
    res.redirect(_.zipObjectDeep(req.params.body))
  }
}

module.exports = function safeFunc () {
  const variable = "some internal var";
  return (req, res, next) => {
    res.redirect(_.zipObjectDeep(variable))
  }
}

module.exports = function sanitizedFunc () {
  return (req, res, next) => {
    const fileExtension = sanitize(req.params.body);
    if (fileExtension) {
      res.redirect(_.zipObjectDeep(req.params.body))
    }
  }
}
