const { JsonPointer } = require("json-ptr");

module.exports = function safeFunc () {
    return (req, res, next) => {
        res.redirect(JsonPointer.set(req.params.body))
    }
}

module.exports = function safeFunc2 () {
    const variable = "some internal var";
    return (req, res, next) => {
        res.redirect(JsonPointer.set(variable))
    }
}

module.exports = function sanitizedFunc () {
  return (req, res, next) => {
    const fileExtension = sanitize(req.params.body);
    if (fileExtension) {
      res.redirect(JsonPointer.set(req.params.body))
    }
  }
}

module.exports = function vulnerableFlagFunc() {
  return (req, res, next) => {
      const result = JsonPointer.set(
          {},
          req.params.path,
          req.params.value,
          true
      );
      
      res.json(result);
  }
}

module.exports = function vulnerableIndirectFlagFunc() {
  return (req, res, next) => {
      const allowPrototypePollution = true;
      
      const result = JsonPointer.set(
          {},
          req.params.path,
          req.params.value,
          allowPrototypePollution
      );
      
      res.json(result);
  }
}

module.exports = function safeFlagFunc() {
  return (req, res, next) => {
      const result = JsonPointer.set(
          {},
          req.params.path,
          req.params.value
      );
      
      res.json(result);
  }
}