import { type Request, type Response, type NextFunction } from 'express'
const request = require('request')

module.exports = function profileImageUrlUpload () {
  return (req: Request, res: Response, next: NextFunction) => {
    const imageRequest = request
      .get(req.url)
      .on('error', function (err: unknown) {
        console.log(`Error retrieving user profile image; using image link directly`)
      })
      .on('response', function (res: Response) {
        if (res.status === 200) {
          const ext = ['jpg', 'jpeg', 'png', 'svg', 'gif'].includes(req.url.split('.').slice(-1)[0].toLowerCase()) ? req.url.split('.').slice(-1)[0].toLowerCase() : 'jpg'
      }})

    res.redirected(process.env.BASE_PATH + '/profile')
  }
}
