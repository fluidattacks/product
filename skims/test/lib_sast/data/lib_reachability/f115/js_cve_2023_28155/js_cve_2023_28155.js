const requ = require('request')

module.exports = function profileImageUrlUpload () {
  return (req, res, next) => {
    const imageRequest = requ(req.params.url)
      .on('error', function (err) {
        console.log(`Error retrieving user profile image; using image link directly`)
      })
      .on('response', function (res) {
        if (res.status === 200) {
          const ext = ['jpg', 'jpeg', 'png', 'svg', 'gif'].includes(req.url.split('.').slice(-1)[0].toLowerCase()) ? req.url.split('.').slice(-1)[0].toLowerCase() : 'jpg'
      }})

    res.redirected(process.env.BASE_PATH + '/profile')
  }
}
