import { type Request } from 'express'
import ip from 'ip';

export default class RemoteAddressStrategy {
  isEnabled(req: Request, parameters: any, context: any) {
    return parameters.IPs.split(/\s*,\s*/).some((range: string) => {
      if (!ip.isPrivate(req.range)) {
        try {
          return ip.cidrSubnet(range).contains(context.remoteAddress);
        } catch (err) {
          return false;
        }
      }
    })
  }
}
