const express = require("express");
const sanitizeHtml = require("sanitize-html");

const app = express();

app.get("/", (req, res) => {
    const clean = sanitizeHtml(req.query.dirty, {
        allowedTags: ['iframe'],
        allowedAttributes: {
          iframe: ['src']
        },
        allowedIframeHostnames: ["www.youtube.com"],
        allowIframeRelativeUrls: true
    });
    res.end(clean);
});

app.listen(8080);
