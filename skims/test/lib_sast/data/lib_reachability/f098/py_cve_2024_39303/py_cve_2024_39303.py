import tempfile
from weblate.trans.backups import (
    ProjectBackup,
)


def restore_backup(zip_path: str, path: str) -> None:
    backup = ProjectBackup()
    with open(zip_path, "rb") as zip_file:
        for _ in zip_file:
            backup.restore(zip_file, path)


if __name__ == "__main__":
    ZIP_PATH = "backup.zip"
    restore_path = tempfile.mkdtemp()

    restore_backup(ZIP_PATH, restore_path)
