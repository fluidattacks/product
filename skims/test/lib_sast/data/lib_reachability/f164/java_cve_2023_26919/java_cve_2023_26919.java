package com.arextest.diff.model.script;

import delight.nashornsandbox.NashornSandbox;
import delight.nashornsandbox.NashornSandboxes;

public class ScriptSandbox {

  public void runUnsafe(HttpServletRequest request) {
    String input = request.getParameter("input");
    NashornSandbox sandbox = NashornSandBoxes.create();
    sandbox.allowLoadFunctions(true);
    sandbox.allowExitFunctions(false);
    sandbox.eval(input); //vuln

  }

  public static void main(String[] args) {
    NashornSandbox sandbox = NashornSandBoxes.create();
    sandbox.allowLoadFunctions(true);
    sandbox.allowExitFunctions(false);
    sandbox.eval("internal value"); // safe
  }

  public void runsafe(HttpServletRequest request) {
    String input = request.getParameter("input");
    NashornSandbox sandbox = NashornSandBoxes.create();
    sandbox.allowLoadFunctions(false);
    sandbox.allowExitFunctions(false);
    sandbox.eval(input); //safe

  }


}
