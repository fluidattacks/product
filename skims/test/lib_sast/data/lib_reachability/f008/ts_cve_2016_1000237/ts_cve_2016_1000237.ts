import { type Request, type Response, type NextFunction } from 'express'
import sanitizeHtml from 'sanitize-html';

module.exports = function profileImageUrlUpload () {
  return (req: Request, res: Response, next: NextFunction) => {
    res.redirect(sanitizeHtml(req.params, {
      allowedTags: [ 'textarea' ]
    }));
  }
}

module.exports = function profileImageUrlUpload () {
  return (req: Request, res: Response, next: NextFunction) => {
    let text = "some internal text";
    res.redirect(sanitizeHtml(text, {
      allowedTags: [ 'textarea' ]
    }));
  }
}
