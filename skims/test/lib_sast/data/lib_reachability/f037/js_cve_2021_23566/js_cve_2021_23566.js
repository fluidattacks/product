const nanoid = require('nanoid')

function vulnFunc (req, res) {
  const code = req.params.code;
  console.log(nanoid.nanoid(code));
}


function safeFunc (req, res) {
  const code = "some internal code";
  console.log(nanoid.nanoid(code));
}
