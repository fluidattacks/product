FROM node:18.20-bullseye as build

ARG NPM_TOKEN

# Create app directory
WORKDIR /app

# Install app dependencies
COPY . .

RUN npm run build

FROM node:18.20-alpine

COPY --from=build /app/dist /dist
COPY package*.json ./

# Add support for https on wget
RUN apk update && apk add --no-cache wget && apk --no-cache add openssl wget && apk add ca-certificates && update-ca-certificates

# Add phantomjs
#RUN wget -qO- "https://github.com/dustinblackman/phantomized/archive/refs/tags/2.1.1a.tar.gz" | tar xz -C / \
#  && npm config set user 0 \
#  && npm install -g phantomjs
##
## Add fonts required by phantomjs to render html correctly
#RUN apk add --update ttf-dejavu ttf-droid ttf-freefont ttf-liberation && rm -rf /var/cache/apk/*

RUN addgroup -S usr-micro-prospects && adduser -S usr-micro-prospects -G usr-micro-prospects
RUN chown -R usr-ts /node_modules && chown -R usr/dist && chown -R usr/package*.json
RUN chmod 777 -R /node_modules && \
    chmod 777 -R /dist && \
    chmod 777 -R /package*.json

USER 1000

EXPOSE 8080

HEALTHCHECK --interval=2m --timeout=3s \
  CMD curl -f http://localhost:8080/ || exit 1

CMD [ "node", "dist/main.js" ]
