FROM ubuntu:22.04 AS ubuntu
ARG DEBIAN_FRONTEND=noninteractive
RUN apt update && apt install -y nginx php-fpm supervisor
WORKDIR /app
RUN touch testfile && chmod 757 testfile
