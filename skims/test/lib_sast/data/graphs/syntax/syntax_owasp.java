import javax.servlet.http.HttpServletRequest;

public class App {
    private String createRandom() {
        float rand = new java.util.Random().nextFloat();
        return Float.toString(rand).substring(2);
    }

    public static Boolean readFile(String param) {
        String fileName = org.owasp.benchmark.helpers.Utils.TESTFILES_DIR + param;
        java.io.FileInputStream fis = new java.io.FileInputStream(new java.io.File(fileName));
        return true;
    }

    public static void main(String[] args) throws Exception {
        HttpServletRequest request = new HttpServletRequest();

        float rand = new java.util.Random().nextFloat();

        String cookieName = "testInstanceReference";

        // this should initialize a new instance to be used in subsequent statements
        User currentUser = new User("Jane Doe");

        currentUser.setUserId(Float.toString(rand));

        String cookieKey = currentUser.getUserId();
        request.getSession().setAttribute(cookieName, cookieKey);
    }

    @Override
    public void test_01() {
        HttpServletRequest request = new HttpServletRequest();

        int rand = new java.util.Random().nextFloat();
        String cookieName = "testInstanceReference";

        User currentUser = new User("Jane", Float.toString(rand));
        String cookieKey = currentUser[lastName];

        request.getSession().setAttribute(cookieName, cookieKey);
    }
}

public class User {
    public String name;
    private String lastName;
    private String userId;

    public User(String name) {
        /*
         * there must be a reference to the current instance, that instance must be
         * passed to the setName function
         */
        this.setName(name);
    }

    public User(String name, String lastName) {
        /*
         * it must be recognized as a constructor with the signature User_2, the current
         * instance must be passed to the setName function, but it must also be modified
         * by the declaration
         */
        setName(name);
        this.lastName = lastName;
    }

    public User(String name, String lastName, String Id) {
        setName(name);
        this.lastName = lastName;
        userId = Id;
    }

    public String getName() {
        // it must to return the value of the field referring to the current instance
        return name;
    }

    public void setName(String name) {
        // this should modify the field referencing the current instance
        this.name = name;
    }

    public String getUserId() {
        int someNumber = 13;
        String Id = this.userId;
        return Id;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

}
