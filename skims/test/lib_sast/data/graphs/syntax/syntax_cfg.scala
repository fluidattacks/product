// First method Hello
/*
  This is a multi-line comment in Scala.
  It can be used to describe large parts of the code.
*/
/**
 * Adds two numbers and returns the result.
 *
 * @param a First number
 * @param b Second number
 * @return The sum of `a` and `b`
 */

def printfForLoop(): Unit = {
  var xyz: String = ""
  for (num <- 1 to 5) {
    xyz = s"Hello, Scala! - Iteration $num"
    println(xyz)
  }
}

def printWhileLoop(): Unit = {
  var xyz = ""
  var num = 1
  while (num <= 5) {
    xyz = s"Hello, Scala! - Iteration $num"
    println(xyz)
    num += 1
  }
}

def printDoWhileLoop(): Unit = {
  var xyz = ""
  var num = 1
  do {
    xyz = s"Hello, Scala! - Iteration $num"
    println(xyz)
    num += 1
  } while (num <= 5)
}

def checkNumber(num: Int): String = {
  num match {
    case 1 => "One"
    case 2 => "Two"
    case 3 => "Three"
    case _ => "Other"
  }
}

def sumar(a: Int, b: Int): Int = {
  return a + b
}

def dividir(a: Int, b: Int): Option[Int] = {
  try {
    Some(a / b)
  } catch {
    case e: ArithmeticException =>
      println("Error: División por cero")
      None
    case e: Exception =>
      println(s"Error inesperado: ${e.getMessage}")
      None
  } finally {
    println("Operación de división finalizada")
  }
}


class MyClass

class HelloScala2 {
  def demonstrateVariables(): Unit = {
    val message: String = "Hello, Scala!"
    val number: Int = 12
    val decimal: Double = 12.5
    val booleanValue: Boolean = true
    val character: Char = 'A'
    val list: List[Int] = List(1, 2, 3)
    val map: Map[String, Int] = Map("one" -> 1, "two" -> 2)

    println(s"Message: $message")
    println(s"Number: $number, Decimal: $decimal, Boolean: $booleanValue, Character: $character")
    println(s"List: $list, Map: $map")
  }

  def checkCondition(message: String): Unit = {
    if (message.contains("Scala")) {
      println("The message contains the word 'Scala'.")
    } else if (message.length > 10) {
      println("The message does not contain 'Scala', but it has more than 10 characters.")
    } else {
      println("The message does not contain 'Scala' and has 10 characters or less.")
    }
  }

  def printMessage(): Unit = {
    val message = "Hello, Scala!"
    println(message)
  }

  def main2(args: Array[String]): String = {
    val message = "Hello, Scala!"
    demonstrateVariables()
    checkCondition(message)
    printMessage()
    message
  }
}
