<?php

$unsafe_gen_0 = uniqid(mt_rand(1, mt_getrandmax()));
$unsafe_gen_1 = array_rand($some_array);
$unsafe_gen_2 = lcg_value();
$unsafe_gen_3 = mt_rand(1, 100);
$unsafe_gen_4 = rand(1, 100);


$secure_rng = new Random\Engine\Secure;

$safe_gen_0 = random_bytes(16);
$safe_gen_1 = $secure_rng->rand(1, 100);
$safe_gen_2 = random_int(1, 100);
$safe_gen_3 = openssl_random_pseudo_bytes(16);


// Unsafe cases, random generated with insecure functions

$bad_random_0 = hash('sha512', uniqid(mt_rand(1, mt_getrandmax())), true);
$bad_random_1 = hash('sha256', array_rand($some_array), true);
$bad_random_2 = hash('sha512', lcg_value(), true);
$bad_random_3 = hash('sha256', mt_rand(1, 100), true);
$bad_random_4 = hash('sha512', rand(1, 100), true);
$bad_random_5 = hash('sha512', $unsafe_gen_0, true);
$bad_random_6 = hash('sha256', $unsafe_gen_1, true);
$bad_random_7 = hash('sha512', $unsafe_gen_2, true);
$bad_random_8 = hash('sha256', $unsafe_gen_3, true);
$bad_random_9 = hash('sha512', $unsafe_gen_4, true);

// Safe cases, random generated with secure functions
$good_random_0 = hash('sha512', $safe_gen_0, true);
$good_random_1 = hash('sha256', $safe_gen_1, true);
$good_random_2 = hash('sha512', $safe_gen_2, true);
$good_random_3 = hash('sha256', $safe_gen_3, true);
$good_random_4 = hash('sha512', random_bytes(16), true);
$good_random_5 = hash('sha256', $secure_rng->rand(1, 100), true);
$good_random_6 = hash('sha512', random_int(1, 100), true);
$good_random_7 = hash('sha256', openssl_random_pseudo_bytes(16), true);
