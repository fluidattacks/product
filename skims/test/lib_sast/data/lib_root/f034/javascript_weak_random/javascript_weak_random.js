import { Router } from "express";

import fs from 'fs'
import expressJwt from 'express-jwt'

export const publicKey = fs ? fs.readFileSync('encryptionkeys/jwt.pub', 'utf8') : 'placeholder-public-key'

export const isAuthorized = () => expressJwt(({ secret: publicKey }))
export const denyAll = () => expressJwt({ secret: '' + Math.random() })

function unsafe_route(req, res) {
  let key = Math.random().toString();
  res.cookie("rememberKey", key);
  res.json({ ok: true });
}

function safe_route(req, res) {
  param = Math.random().toString();
  let map64 = {
    keyA: "a_Value",
    keyB: "b_Value",
    keyC: param,
  };
  const key = map64.keyB;
  res.cookie("rememberKey", key);
  res.json({ ok: true });
}
