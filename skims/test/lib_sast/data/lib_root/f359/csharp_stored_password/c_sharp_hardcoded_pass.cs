namespace Controllers
{
    public class DBaccess
    {
        public void dbauth(String safe_password)
        {
            DbContextOptionsBuilder optionsBuilder = new DbContextOptionsBuilder();
            //insecure
            var con_str = "Server=myServerAddress;Database=myDataBase;User Id=myUsername;Password=5674_H5lloW0rld";
            optionsBuilder.UseSqlServer(con_str);

            //insecure
            DbContextOptionsBuilder optionsBuilder2 = new DbContextOptionsBuilder();
            var password = "5674_H5lloW0rld";
            var con_str1 = "Server=myServerAddress;Database=myDataBase;User Id=myUsername;Password=" + password;
            optionsBuilder2.UseSqlServer(con_str1);

            //secure
            DbContextOptionsBuilder optionsBuilder3 = new DbContextOptionsBuilder();
            var con_str2 = "Server=myServerAddress;Database=myDataBase;User Id=myUsername;Password=" + safe_password;
            optionsBuilder3.UseSqlServer(con_str2);

            //secure
            DbContextOptionsBuilder optionsBuilder4 = new DbContextOptionsBuilder();
            optionsBuilder4.UseSqlServer("Server=myServerAddress;Database=myDataBase;User Id=myUsername;");

        }
    }
}
