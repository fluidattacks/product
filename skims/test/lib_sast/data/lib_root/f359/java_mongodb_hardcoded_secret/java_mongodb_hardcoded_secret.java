// Source: https://semgrep.dev/orgs/janumejia_personal_org/editor/r/java.mongo.java-mongo-hardcoded-secret.java-mongo-hardcoded-secret?editorMode=advanced
package services;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;

public class A {
    protected DB() {

        new MongoClient(new MongoClientURI("mongodb://user:password@a.mongolab.com:37601/goparty")); // -> Vulnerable

        new MongoClient(new MongoClientURI("mongodb://<user>:password@a.mongolab.com:37601/goparty")); // -> Vulnerable

        new MongoClient(new MongoClientURI("mongodb://<user>:<password>@a.a.com:37601/goparty")); // -> Safe

        new MongoClient(new MongoClientURI("mongodb://a.mongolab.com:37601/goparty")); // -> Safe

        new MongoClient(new MongoClientURI("mongodb://admin:admin123@localhost:27017/testdb")); // -> Vulnerable

        new MongoClient(new MongoClientURI("mongodb://<username>:<password>@localhost:27017/testdb")); // -> Safe

        new MongoClient(new MongoClientURI("mongodb://exampleUser:examplePass@mydbserver.com:12345/mydb")); // -> Vulnerable

        new MongoClient(new MongoClientURI("mongodb://<username>:<password>@mydbserver.com:12345/mydb")); // -> Safe

        String mongoUri = "mongodb://<user>:<password>@a.mongolab.com:37601/goparty";
        new MongoClient(new MongoClientURI(mongoUri)); // -> Safe

        String insecureUri = "mongodb://user:password@localhost:27017/testdb";
        new MongoClient(new MongoClientURI(insecureUri)); // -> Vulnerable
    }
}
