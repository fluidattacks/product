import jwt from 'jsonwebtoken';

const payload = {
  userId: 123,
  username: 'user_example'
};

// Vulnerable: The secret key is hardcoded directly in the code
const vulnerable_secret_key = 'hardcoded_secret_key'

const vulnerableToken1 = jwt.sign( // -> Vulnerable
  {
    exp: Math.floor(Date.now() / 1000) + (60 * 60),
    data: 'foobar'
  },
  'secret'
);

const vulnerableToken2 = jwt.sign( // -> Vulnerable
  payload,
  'hardcoded_secret_key',
  { expiresIn: '1h' }
);

const vulnerableToken3 = jwt.sign( // -> Vulnerable
  payload,
  vulnerable_secret_key,
  { expiresIn: '1h' }
);

try {
  const decoded1 = jwt.verify(vulnerableToken1, 'hardcoded_secret_key'); // -> Vulnerable
  console.log('Vulnerable Token is valid:', decoded1);
} catch (err: any) {
  console.log('Vulnerable Token verification failed:', err.message);
}

// Secure: The secret key is loaded from an environment variable
const secure_secret_key = process.env.JWT_SECRET as string

const secureToken1 = jwt.sign( // -> Secure
  {
    exp: Math.floor(Date.now() / 1000) + (60 * 60),
    data: 'foobar'
  },
  process.env.BACKEND_JWT_SECRET as string
);

const secureToken2 = jwt.sign( // -> Secure
  payload,
  process.env.JWT_SECRET as string,
  { expiresIn: '1h' }
);

const secureToken3 = jwt.sign( // -> Secure
  payload,
  secure_secret_key,
  { expiresIn: '1h' }
);

try {
  const decoded2 = jwt.verify(secureToken1, process.env.BACKEND_JWT_SECRET as string); // -> Secure
  console.log('Secure Token 1 is valid:', decoded2);
} catch (err: any) {
  console.log('Secure Token 1 verification failed:', err.message);
}
