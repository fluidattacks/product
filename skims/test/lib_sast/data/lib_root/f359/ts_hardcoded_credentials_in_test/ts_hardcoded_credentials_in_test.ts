import frisby = require('frisby')

const REST_URL = 'http://localhost:3000/rest'

const jsonHeader = { 'content-type': 'application/json' }

beforeAll(() => {
  return frisby.post(REST_URL + '/user/login', {
    headers: jsonHeader,
    body: {
      email: 'jim@juice-sh.op',
      password: 'ncc-1701'
    }
  })
    .expect('status', 200)
})

// user created during test runtime, SAFE

describe('/rest/user/change-password', () => {
  it('GET password change for newly created user with recognized token as Authorization header', () => {
    return frisby.post(API_URL + '/Users', {
      headers: jsonHeader,
      body: {
        email: 'kuni@be.rt',
        password: 'kunigunde'
      }
    })
      .expect('status', 201)
      .then(() => {
        return frisby.post(REST_URL + '/user/login', {
          headers: jsonHeader,
          body: {
            email: 'kuni@be.rt',
            password: 'kunigunde'
          }
        })
          .expect('status', 200)
          .then(({ json }) => {
            return frisby.get(REST_URL + '/user/change-password?current=kunigunde&new=foo&repeat=foo', {
              headers: { Authorization: 'Bearer ' + json.authentication.token }
            })
              .expect('status', 200)
          })
      })
  })

})

// Test to login into a preexistent user and expect it to work, VULN

describe('/rest/products/reviews', () => {
  it('POST non-existing product review cannot be liked', () => {
    return frisby.post(`${REST_URL}/user/login`, {
      headers: jsonHeader,
      body: {
        email: 'bjoern.kimminich@gmail.com',
        password: 'bW9jLmxpYW1nQGhjaW5pbW1pay5ucmVvamI='
      }
    })
      .expect('status', 200)})

  // Test to login into a preexistent user and expect it to fail, SAFE

  it('POST single product review can be liked', () => {
    return frisby.post(`${REST_URL}/user/login`, {
      headers: jsonHeader,
      body: {
        email: 'bjoern.kimminich@gmail.com',
        password: 'notPassword'
      }
    })
      .expect('status', 400)
  })

})

  // Test to login into a preexistent user and expect it to work and
  // doing something afer that, VULN

  describe('/rest/user/whoami', () => {
    it('GET own user id and email on who-am-i request', () => {
      return frisby.post(`${REST_URL}/user/login`, {
        headers: jsonHeader,
        body: {
          email: 'bjoern.kimminich@gmail.com',
          password: 'bW9jLmxpYW1nQGhjaW5pbW1pay5ucmVvamI='
        }
      })
        .expect('status', 200)
        .then(({ json }) => {
          return frisby.get(`${REST_URL}/user/whoami`, { headers: { Cookie: `token=${json.authentication.token}` } })
            .expect('status', 200)
            .expect('header', 'content-type', /application\/json/)
            .expect('jsonTypes', 'user', {
              id: Joi.number(),
              email: Joi.string()
            })
            .expect('json', 'user', {
              email: 'bjoern.kimminich@gmail.com'
            })
        })
    })

    it('GET password change for Bender without current password using GET request', () => {
      return frisby.post(REST_URL + '/user/login', {
        headers: jsonHeader,
        body: {
          email: 'bender@' + config.get('application.domain'),
          password: 'OhG0dPlease1nsertLiquor!'
        }
      })
        .expect('status', 200)
        .then(({ json }) => {
          return frisby.get(REST_URL + '/user/change-password?new=slurmCl4ssic&repeat=slurmCl4ssic', {
            headers: { Authorization: 'Bearer ' + json.authentication.token }
          })
            .expect('status', 200)
        })
    })
  })
