// Source: https://semgrep.dev/r?q=java.jsch.jsch-hardcoded-secret.jsch-hardcoded-secret
import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;


public class Sftp {


	public static void f() {

        String password = "aaaa";
        String configPassword = System.getenv("CONFIG_PASSWORD");

        JSch jsch = new JSch();
        session = jsch.getSession("ubuntu", "192.69.69.69",22);
        session.setPassword("aaaaa"); // -> Vulnerable
        session.setPassword(config); // -> Safe
        session.setPassword(password); // -> Vulnerable
        session.connect();

        Session session2 = jsch.getSession("admin", "192.168.1.10", 22);
        session2.setPassword(configPassword); // -> Safe
        session2.connect();
    }

}
