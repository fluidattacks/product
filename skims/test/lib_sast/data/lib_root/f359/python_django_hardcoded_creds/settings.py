# type: ignore
# settings.py

import os

# --------- VULNERABLE PART WITH HARDCODED VARIABLES ----------

# Reference: https://docs.djangoproject.com/en/5.1/ref/settings/#databases
DATABASES = {  # -> Vulnerable
    "default": {
        "ENGINE": "django.db.backends.sqlite3",
        "NAME": "db.sqlite3",
        "USER": "mydatabaseuser",
        "PASSWORD": "mypassword",
        "HOST": "127.0.0.1",
        "PORT": "5432",
    }
}

# Reference: https://docs.djangoproject.com/en/5.1/ref/settings/#caches
CACHES = {  # -> Vulnerable
    "default": {
        "BACKEND": "django_redis.cache.RedisCache",
        "LOCATION": "redis://localhost:6379/1",
        "OPTIONS": {
            "CLIENT_CLASS": "django_redis.client.DefaultClient",
            "PASSWORD": "your_redis_password",
        },
    }
}

# Reference: https://docs.djangoproject.com/en/5.1/ref/settings/#email-host-password
EMAIL_BACKEND = "django.core.mail.backends.smtp.EmailBackend"
EMAIL_HOST = "smtp.your-email-provider.com"
EMAIL_PORT = 587
EMAIL_USE_TLS = True
EMAIL_HOST_USER = "your_email@example.com"
EMAIL_HOST_PASSWORD = "your_email_password"  # -> Vulnerable

# Reference: https://docs.djangoproject.com/en/5.1/ref/settings
API_KEY = "myapikey"  # -> Vulnerable
SECRET_KEY = "django-insecure-@)9x@9-ve2u&u@@%#16yg+iwp-rbrd2z628+t8upj4yre&+t&@"  # -> Vulnerable
SECRET_KEY_FALLBACKS = [  # -> Vulnerable
    "your_old_secret_key_1",
    "your_old_secret_key_2",
]

# Reference: https://django-storages.readthedocs.io/en/latest/backends/amazon-S3.html
AWS_SECRET_ACCESS_KEY = "your_aws_secret_access_key"  # -> Vulnerable
AWS_S3_SECRET_ACCESS_KEY = "your_aws_secret_access_key"  # -> Vulnerable

# Reference: https://python-social-auth.readthedocs.io/en/latest/backends/google.html
SOCIAL_AUTH_GOOGLE_OAUTH_SECRET = "your_google_client_secret"  # -> Vulnerable
SOCIAL_AUTH_GOOGLE_OAUTH2_SECRET = "your_google_client_secret"  # -> Vulnerable
SOCIAL_AUTH_GOOGLE_PLUS_SECRET = "your_google_client_secret"  # -> Vulnerable

# Reference: https://django-auth-ldap.readthedocs.io/en/latest/authentication.html
AUTH_LDAP_BIND_PASSWORD = "your_ldap_password"  # -> Vulnerable

# --------- SECURE PART USING ENVIRONMENT VARIABLES ----------

DATABASES = {  # -> Safe
    "default": {
        "ENGINE": "django.db.backends.sqlite3",
        "NAME": os.getenv("DB_NAME", "db.sqlite3"),
        "USER": os.getenv("DB_USER", ""),
        "PASSWORD": os.getenv("DB_PASSWORD", ""),
        "HOST": os.getenv("DB_HOST", "127.0.0.1"),
        "PORT": os.getenv("DB_PORT", "5432"),
    }
}

CACHES = {  # -> Safe
    "default": {
        "BACKEND": "django_redis.cache.RedisCache",
        "LOCATION": os.getenv("REDIS_LOCATION", "redis://localhost:6379/1"),
        "OPTIONS": {
            "CLIENT_CLASS": "django_redis.client.DefaultClient",
            "PASSWORD": os.getenv("REDIS_PASSWORD", ""),
        },
    }
}

EMAIL_BACKEND = "django.core.mail.backends.smtp.EmailBackend"
EMAIL_HOST = os.getenv("EMAIL_HOST", "smtp.your-email-provider.com")
EMAIL_PORT = int(os.getenv("EMAIL_PORT", 587))
EMAIL_USE_TLS = bool(os.getenv("EMAIL_USE_TLS", True))
EMAIL_HOST_USER = os.getenv("EMAIL_HOST_USER", "your_email@example.com")
EMAIL_HOST_PASSWORD = os.getenv("EMAIL_HOST_PASSWORD", "")  # -> Safe

API_KEY = os.getenv("API_KEY", "")  # -> Safe
SECRET_KEY = os.getenv("SECRET_KEY", "")  # -> Safe
SECRET_KEY_FALLBACKS = [  # -> Safe
    os.getenv("SECRET_KEY_FALLBACK_1", ""),
    os.getenv("SECRET_KEY_FALLBACK_2", ""),
]

AWS_SECRET_ACCESS_KEY = os.getenv("AWS_SECRET_ACCESS_KEY", "")  # -> Safe
AWS_S3_SECRET_ACCESS_KEY = os.getenv("AWS_S3_SECRET_ACCESS_KEY", "")  # -> Safe

SOCIAL_AUTH_GOOGLE_OAUTH_SECRET = os.getenv(
    "GOOGLE_OAUTH_SECRET", ""
)  # -> Safe
SOCIAL_AUTH_GOOGLE_OAUTH2_SECRET = os.getenv(
    "GOOGLE_OAUTH2_SECRET", ""
)  # -> Safe
SOCIAL_AUTH_GOOGLE_PLUS_SECRET = os.getenv("GOOGLE_PLUS_SECRET", "")  # -> Safe

AUTH_LDAP_BIND_PASSWORD = os.getenv("LDAP_BIND_PASSWORD", "")  # -> Safe
