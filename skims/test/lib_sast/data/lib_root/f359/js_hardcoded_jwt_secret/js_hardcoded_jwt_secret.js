const jwt = require('jsonwebtoken');


const payload = {
  userId: 123,
  username: 'user_example'
};


// Vulnerable: The secret key is hardcoded directly in the code
const vulnerable_token_1 = jwt.sign({ // -> Vulnerable
  exp: Math.floor(Date.now() / 1000) + (60 * 60),
  data: 'foobar'
}, 'secret');


const vulnerable_token = jwt.sign( // -> Vulnerable
  payload,
  'hardcoded_secret_key',
  { expiresIn: '1h' }
);


try {
  const decoded1 = jwt.verify(vulnerable_token_1, 'hardcoded_secret_key'); // -> Vulnerable
  console.log('Vulnerable Token is valid:', decoded1);
} catch (err) {
  console.log('Vulnerable Token verification failed:', err.message);
}


// Secure: The secret key is loaded from an environment variable
const secure_token_1 = jwt.sign({ // -> Secure
  exp: Math.floor(Date.now() / 1000) + (60 * 60),
  data: 'foobar'
}, process.env.BACKEND_JWT_SECRET);


const secure_token = jwt.sign(payload, process.env.JWT_SECRET, { expiresIn: '1h' }); // -> Secure


try {
  const decoded2 = jwt.verify(secure_token_1, process.env.BACKEND_JWT_SECRET); // -> Secure
  console.log('Secure Token 1 is valid:', decoded2);
} catch (err) {
  console.log('Secure Token 1 verification failed:', err.message);
}
