// Source: https://semgrep.dev/r?q=java.okhttp.secrets.hardcoded-secret-in-credentials.hardcoded-secret-in-credentials
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Credentials;

public class OkhttpSecretBasicAuth {
  private String username = "wowee";
  private String password = "hi";
  private String empty = "";

  public void run() {
    String credential = Credentials.basic(username, "asdf"); // -> Vulnerable

    String credential = Credentials.basic(username, password); // -> Vulnerable

    String credential = Credentials.basic(username, ""); // -> Safe

    String credential = Credentials.basic(username, empty); // -> Safe

    String credential = Credentials.basic(username, System.getenv("PASSWORD")); // -> Safe
  }
}
