import java.net.http.HttpRequest;
import java.net.PasswordAuthentication;

public class UhOh {

    public void run(){
        String password = "password";
        char[] asdf = "password".toCharArray();
        var authClient = HttpClient
            .newBuilder()
            .authenticator(new Authenticator() {
                @Override
                protected PasswordAuthentication getPasswordAuthentication() {
                    new PasswordAuthentication("postman", "password".toCharArray()); // -> Vulnerable

                    new PasswordAuthentication("postman", asdf); // -> Vulnerable

                    new PasswordAuthentication("postman", "password"); // -> Vulnerable

                    new PasswordAuthentication("postman", password); // -> Vulnerable
                }
            })
            .build();
    }

    protected PasswordAuthentication getPasswordAuthentication() {
        return new PasswordAuthentication("sender@gmail.com", "appsPassword"); // -> Vulnerable
    }

}

public class Test2 extends Authenticator {
    private static final PasswordAuthentication ZO = new PasswordAuthentication("", new char[0]); // -> Vulnerable

    protected PasswordAuthentication getPasswordAuthentication() {
        return ZO;
    }
}

public class SecurityTestCases {

    public void testHardcodedPasswords() {
        var authClient = HttpClient
            .newBuilder()
            .authenticator(new Authenticator() {
                @Override
                protected PasswordAuthentication getPasswordAuthentication() {
                    // Vulnerable cases
                    new PasswordAuthentication("admin", "12345".toCharArray()); // -> Vulnerable
                    new PasswordAuthentication("user", "password123".toCharArray()); // -> Vulnerable

                    // Safe cases
                    char[] securePassword = System.getenv("PASSWORD").toCharArray(); // Using environment variable
                    new PasswordAuthentication("user", securePassword); // -> Safe

                    char[] userInputPassword = readPasswordFromInput(); // Simulate secure user input
                    new PasswordAuthentication("user", userInputPassword); // -> Safe

                    return null;
                }
            })
            .build();
    }

    public PasswordAuthentication testDirectAssignment() {
        return new PasswordAuthentication("testuser", "hardcoded".toCharArray()); // -> Vulnerable
    }

    public void testCharArrayPassword() {
        char[] hardcodedPassword = {'s', 'e', 'c', 'r', 'e', 't'};
        PasswordAuthentication auth = new PasswordAuthentication("user", hardcodedPassword); // -> Vulnerable

        PasswordAuthentication anotherVulnerableAuth = new PasswordAuthentication("user2", new char[]{'1', '2', '3', '4', '5'}); // -> Vulnerable
    }

    public PasswordAuthentication testEnvironmentVariable() {
        // Safe case
        char[] passwordFromEnv = System.getenv("PASSWORD_ENV").toCharArray();
        return new PasswordAuthentication("user", passwordFromEnv); // -> Safe
    }

    public void testEdgeCases() {
        new PasswordAuthentication("admin", "".toCharArray()); // -> Vulnerable

        new PasswordAuthentication("admin", null); // -> Vulnerable

        char[] dynamicPassword = generateSecurePassword();
        new PasswordAuthentication("dynamicUser", dynamicPassword); // -> Safe
    }
}

public class ExtendedTest extends Authenticator {

    // Vulnerable static password case
    private static final PasswordAuthentication STATIC_AUTH =
        new PasswordAuthentication("staticUser", "staticPassword".toCharArray()); // -> Vulnerable

    @Override
    protected PasswordAuthentication getPasswordAuthentication() {
        return STATIC_AUTH;
    }

    public PasswordAuthentication testNullUser() {
        return new PasswordAuthentication(null, "nullUserPassword".toCharArray()); // -> Vulnerable
    }
}
