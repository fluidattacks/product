// Source: https://semgrep.dev/r?q=java.lang.security.sql.drivermanager-hardcoded-secret.drivermanager-hardcoded-secret
import java.sql.*;

public class a
{
   public static void main(String[] args) throws SQLException
   {
      String password = "a";

      Connection conn =DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:o92", "a", "password"); // -> Vulnerable

      Connection conn =DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:o92", "a", password); // -> Vulnerable

      Connection conn = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:o92", "a", random); // -> Safe

      Connection conn = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:o92", "a", ""); // -> Safe

      Connection conn = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:o92"); // -> Safe

      Connection conn = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:o92","a"); // -> Safe

      DriverManagerDataSource driverManagerDataSource = new DriverManagerDataSource();

      driverManagerDataSource.setPassword("a"); // -> Vulnerable

      driverManagerDataSource.setPassword(config.foo); // -> Safe

      new DriverManagerDataSource("aaaa", "root", "123456"); // -> Vulnerable

      new DriverManagerDataSource("aaaa", "root", config); // -> Safe

      new DriverManagerDataSource("aaaa", "root", ""); // -> Safe

      String envPassword = System.getenv("DB_SECRET");
      Connection conn10 = DriverManager.getConnection("jdbc:mysql://localhost:3306/mydb", "root", envPassword); // -> Safe

      Config config = new Config();
      Connection conn11 = DriverManager.getConnection("jdbc:mysql://localhost:3306/mydb", "root", config.getDatabasePassword()); // -> Safe

   }
}
