<?php

$host = getenv("DB_HOST");
$username = getenv("DB_USER");
$secure_password = getenv("DB_PASS");
$database = getenv("DB_NAME");
$hc_password = "Aix34**78bn-23nhj";


// Vulnerable cases

$conn_vulnerable_1 = mysqli_connect("localhost", "username", "Aix34**78bn-23nhj", "database");
$conn_vulnerable_2 = new mysqli($host, $username, "Aix34**78bn-23nhj", $database);


$conn_vulnerable_3 = mysqli_connect("localhost", "username", $hc_password, "database");
$conn_vulnerable_4 = mysqli_connect($host, $username, $hc_password, $database);

// Safe cases

if ($host && $username && $password && $database) {
    $conn_safe_1 = mysqli_connect($host, $username, $secure_password, $database);
    $conn_safe_2 = new mysqli($host, $username, $secure_password, $database);

}
