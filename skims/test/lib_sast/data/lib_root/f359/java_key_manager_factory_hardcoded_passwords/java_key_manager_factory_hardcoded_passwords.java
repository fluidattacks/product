// Source: https://semgrep.dev/r?q=gitlab.find_sec_bugs.HARD_CODE_PASSWORD-1
import javax.net.ssl.KeyManagerFactory;

// Case 4. KeyManagerFactory.getInstance().init Pattern
KeyManagerFactory vulnerableKmf = KeyManagerFactory.getInstance("SunX509") // -> Vulnerable
    .init(keyStore, "hardcodedPass123".toCharArray());

String kmfPassword = SecureConfigLoader.getKeyManagerPassword();
KeyManagerFactory secureKmf = KeyManagerFactory.getInstance("SunX509") // -> Safe
    .init(keyStore, kmfPassword.toCharArray());

KeyManagerFactory kmf = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm()) // -> Vulnerable
    .init(null, "hardcodedPassword".toCharArray());

String password = System.getenv("KEY_MANAGER_PASSWORD");
KeyManagerFactory kmf = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm()) // -> Safe
    .init(null, password.toCharArray());


// Case 5. KeyManagerFactory variable init Pattern
KeyManagerFactory vulnerableFactory = KeyManagerFactory.getInstance("SunX509");
vulnerableFactory.init(keyStore, "hardcodedPass123".toCharArray()); // -> Vulnerable

KeyManagerFactory secureFactory = KeyManagerFactory.getInstance("SunX509");
String factoryPassword = SecureConfigLoader.getKeyManagerPassword();
secureFactory.init(keyStore, factoryPassword.toCharArray()); // -> Safe

KeyManagerFactory kmf = KeyManagerFactory.getInstance("SunX509");
kmf.init(null, "hardcodedPassword".toCharArray()); // -> Vulnerable

String password = System.getenv("KEY_MANAGER_PASSWORD");
KeyManagerFactory kmf = KeyManagerFactory.getInstance("SunX509");
kmf.init(null, password.toCharArray()); // -> Safe
