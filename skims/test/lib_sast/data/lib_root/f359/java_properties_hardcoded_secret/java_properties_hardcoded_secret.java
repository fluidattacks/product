import java.util.Properties;

public class Client {
	public void connect() {
          String password = "foo";
          Properties jndiProps = new Properties();

          jndiProps.put(Context.SECURITY_CREDENTIALS, "password"); // -> Vulnerable

          jndiProps.put(Context.SECURITY_CREDENTIALS, password); // -> Vulnerable

          jndiProps.put(Context.SECURITY_CREDENTIALS, config); // -> Safe
	}
}

public class Test2 {
	public void connect() {
          String password = "theHardcodedPassword";
          Properties jndiProps2 = new Properties2();

          jndiProps2.put(Context.SECURITY_CREDENTIALS, "password"); // -> Safe

          jndiProps2.put(Context.SECURITY_CREDENTIALS, password); // -> Safe

	}
}

public class Test3 {
	public void connect() {
          String password = "theHardcodedPassword";
          Properties jndiProps2 = new Properties();

          jndiProps2.put(Context.othervalue, "password"); // -> Safe

          jndiProps2.put(Context.othervalue, password); // -> Safe

	}
}
