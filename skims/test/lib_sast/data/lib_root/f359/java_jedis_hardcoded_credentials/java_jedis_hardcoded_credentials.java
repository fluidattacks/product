import redis.clients.jedis.Jedis;


// Source: https://semgrep.dev/r?q=java.jedis.secrets.jedis-hardcoded-uri.jedis-hardcoded-uri
@Service
public class JedisService implements IJedisService {
    @Test
    public void startWithUrl() {
        Jedis jedis = new Jedis( // -> Vulnerable
          "redis://default:yPqm07QgkiXFbZ9gxR9ejjpmuhO3j9sG@redis-18384.c16.us-east-1-2.ec2.cloud.redislabs.com:18384"
        );

        try (Jedis j = new Jedis("localhost", 6379)) { // -> Safe
            j.set("foo", "bar");
        }

        try (Jedis j2 = new Jedis("redis://foobared:fizzbuzz@localhost:6379/2")) { // -> Vulnerable
            j2.set("foo", "bar");
        }

        Jedis jedis = new Jedis("redis://aasdf:foobared@localhost:6380/2"); // -> Vulnerable

        String host = "fizzbuzz";
        String port = "3456";
        Jedis jedis = new Jedis("redis://:foobared@" + host + ":" + port + "/3"); // -> Vulnerable

        String host2 = "fizzbuzz";
        String port2 = "3456";
        String password2 = "superpassword";
        Jedis jedis = new Jedis("redis://user:" + password2 + "@" + host + ":" + port + "/3"); // -> Vulnerable
    }
}
