# type: ignore

from flask import (
    Flask,
)

app = Flask(__name__)


# Vulnerable cases
SEC_KEY_VULNERABLE = "your_secret_key"
app.secret_key = "my_super_secret_key_12345"  # -> Vulnerable
app.secret_key = SEC_KEY_VULNERABLE  # -> Vulnerable
app.config["SECRET_KEY"] = "my_super_secret_key_12345"  # -> Vulnerable
app.config["SECRET_KEY"] = SEC_KEY_VULNERABLE  # -> Vulnerable


# Safe cases, using environment variables
SEC_KEY_SAFE = os.environ.get("FLASK_SECRET_KEY")
app.secret_key = os.environ.get("FLASK_SECRET_KEY")  # -> Safe
app.secret_key = SEC_KEY_SAFE  # -> Safe
app.config["SECRET_KEY"] = os.environ.get("FLASK_SECRET_KEY")  # -> Safe
app.config["SECRET_KEY"] = SEC_KEY_SAFE  # -> Safe


@app.route("/example")
def example():
    return "Hello world!"


if __name__ == "__main__":
    app.run()
