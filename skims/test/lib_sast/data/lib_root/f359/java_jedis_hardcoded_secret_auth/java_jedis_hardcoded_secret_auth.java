// Source: https://semgrep.dev/r?q=java.jedis.secrets.jedis-auth-hardcoded-password.jedis-auth-hardcoded-secret
import redis.clients.jedis.Jedis;

public class JedisTest {
    void run() {
        Jedis jedis = new Jedis();

        String password = "hi";
        jedis.auth("asdf"); // -> Vulnerable
        jedis.auth(password); // -> Vulnerable
        jedis.auth("superuser", password); // -> Vulnerable
        jedis.auth("superuser2", "superpassword86"); // -> Vulnerable

        int noAPassword = 10;
        jedis.auth(1); // -> Safe
        jedis.auth(noAPassword); // -> Safe
        jedis.auth(""); // -> Safe
        jedis.auth(config.getAuthPassword()); // -> Safe
        jedis.auth(config.getAuthPassword("john")); // -> Safe
        jedis.auth(config.getAuthPassword["john"]); // -> Safe
        jedis.auth("superuser", config.getAuthPassword()); // -> Safe
        jedis.auth("superuser2", 1); // -> Safe
        jedis.auth("superuser3", noAPassword); // -> Safe
    }
}
