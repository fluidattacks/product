resource "kubernetes_pod" "rss_site" {
  metadata {
    name = "rss-site"
    labels = {
      app = "web"
    }
  }
  spec {
    automount_service_account_token = false
    security_context {
      run_as_non_root = true
      seccomp_profile {
        type = "Localhost"
      }
    }
    container {
      name  = "unsafe_runasUser"
      image = "nginx"
      port {
        container_port = 80
      }
      security_context {
        run_as_non_root            = true
        allow_privilege_escalation = false
        read_only_root_filesystem  = true
        run_as_user                = 0

        capabilities {
          drop = ["ALL"]
        }
      }
    }
    container {
      name  = "safe_runasUser"
      image = "nginx"
      port {
        container_port = 80
      }
      security_context {
        run_as_non_root            = true
        allow_privilege_escalation = false
        read_only_root_filesystem  = true
        run_as_user                = 10000
        capabilities {
          drop = ["ALL"]
        }
      }
    }
    container {
      name  = "nondeterministic_runasUser"
      image = "nginx"
      port {
        container_port = 80
      }
      security_context {
        run_as_non_root            = true
        allow_privilege_escalation = false
        read_only_root_filesystem  = true
        # run_as_user set dynamically
        run_as_user = "{ { runasUser } }"
        capabilities {
          drop = ["ALL"]
        }
      }
    }
  }
}
