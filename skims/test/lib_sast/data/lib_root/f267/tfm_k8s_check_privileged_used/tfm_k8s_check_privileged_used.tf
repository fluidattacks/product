resource "kubernetes_pod" "rss_site" {
  metadata {
    name = "rss-site"
    labels = {
      app = "web"
    }
  }
  spec {
    automount_service_account_token = false
    security_context {
      run_as_non_root = false
      seccomp_profile {
        type = "Localhost"
      }
    }
    container {
      name  = "unsafe_privileged"
      image = "nginx"
      port {
        container_port = 80
      }
      security_context {
        privileged                 = true
        run_as_non_root            = true
        allow_privilege_escalation = false
        read_only_root_filesystem  = true
        capabilities {
          drop = ["all"]
        }
        seccomp_profile {
          type = "Localhost"
        }
      }
    }
    container {
      name  = "safe_privileged"
      image = "nginx"
      port {
        container_port = 80
      }
      security_context {
        run_as_non_root            = true
        allow_privilege_escalation = false
        read_only_root_filesystem  = true
        capabilities {
          drop = ["all"]
        }
        seccomp_profile {
          type = "Localhost"
        }
      }
    }
  }
}
