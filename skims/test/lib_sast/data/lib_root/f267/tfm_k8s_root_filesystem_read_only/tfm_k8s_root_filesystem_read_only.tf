resource "kubernetes_pod" "rss_site" {
  metadata {
    name = "rss-site"
    labels = {
      app = "web"
    }
  }
  spec {
    automount_service_account_token = false
    security_context {
      seccomp_profile {
        type = "Localhost"
      }
    }
    container {
      name  = "unsafe-readOnly"
      image = "nginx"
      port {
        container_port = 80
      }
      security_context {
        run_as_non_root            = true
        allow_privilege_escalation = false
        read_only_root_filesystem  = false
        capabilities {
          drop = ["ALL"]
        }
      }
    }
    container {
      name  = "must-NOT-fail"
      image = "nginx"
      port {
        container_port = 80
      }
      security_context {
        run_as_non_root            = true
        allow_privilege_escalation = false
        read_only_root_filesystem  = true
        capabilities {
          drop = ["ALL"]
        }
      }
    }
  }
}
