resource "kubernetes_pod" "hostpath_example_linux" {
  metadata {
    name = "hostpath-example-linux"
  }
  spec {
    automount_service_account_token = false
    os {
      name = "linux"
    }
    node_selector = {
      "kubernetes.io/os" = "linux"
    }
    container {
      name  = "example-container"
      image = "registry.k8s.io/test-webserver"
      volume_mount {
        mount_path = "/foo"
        name       = "example-volume"
        read_only  = true
      }
      security_context {
        run_as_non_root            = true
        allow_privilege_escalation = false
        read_only_root_filesystem  = true
      }
    }
    volume {
      name = "example-volume"
      host_path {
        path = "/data/foo"
        type = "Directory"
      }
    }
  }
}
