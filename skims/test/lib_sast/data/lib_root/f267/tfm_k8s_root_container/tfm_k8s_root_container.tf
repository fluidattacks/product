resource "kubernetes_pod_v1" "rss_site" {
  metadata {
    name = "rss-site"
    labels = {
      app = "web"
    }
  }
  spec {
    automount_service_account_token = false
    security_context {
      run_as_non_root = false
      seccomp_profile {
        type = "Localhost"
      }
    }
    container {
      name  = "unsafe_root"
      image = "nginx"
      port {
        container_port = 80
      }
      security_context {
        run_as_non_root            = false
        allow_privilege_escalation = false
        read_only_root_filesystem  = true
        capabilities {
          drop = ["all"]
        }
      }
    }
    container {
      name  = "safe_root"
      image = "nginx"
      port {
        container_port = 80
      }
      security_context {
        run_as_non_root            = true
        allow_privilege_escalation = false
        read_only_root_filesystem  = true
        capabilities {
          drop = ["all"]
        }
      }
    }
    container {
      name  = "unsafe_root_unset"
      image = "nginx"
      port {
        container_port = 80
      }
      security_context {
        allow_privilege_escalation = false
        read_only_root_filesystem  = true
        capabilities {
          drop = ["all"]
        }
      }
    }
  }
}

resource "kubernetes_pod_v1" "rss_site_run_as_non_root" {
  metadata {
    name = "rss-site"
    labels = {
      app = "web"
    }
  }
  spec {
    automount_service_account_token = false
    security_context {
      seccomp_profile {
        type = "Localhost"
      }
    }
    container {
      name  = "unsafe_root"
      image = "nginx"
      port {
        container_port = 80
      }
      security_context {
        run_as_non_root            = false
        allow_privilege_escalation = false
        read_only_root_filesystem  = true
        capabilities {
          drop = ["all"]
        }
      }
    }
    container {
      name  = "unsafe_root_unset"
      image = "nginx"
      port {
        container_port = 80
      }
      security_context {
        allow_privilege_escalation = false
        read_only_root_filesystem  = true
        capabilities {
          drop = ["all"]
        }
      }
    }
  }
}

resource "kubernetes_pod_v1" "rss_site_no_security_context" {
  metadata {
    name = "rss-site"
    labels = {
      app = "web"
    }
  }
  spec {
    automount_service_account_token = false
    container {
      name  = "unsafe_root"
      image = "nginx"
      port {
        container_port = 80
      }
      security_context {
        run_as_non_root            = false
        allow_privilege_escalation = false
        read_only_root_filesystem  = true
        capabilities {
          drop = ["all"]
        }
      }
    }
    container {
      name  = "unsafe_security_unset"
      image = "nginx"
      port {
        container_port = 80
      }
    }
  }
}

resource "kubernetes_deployment_v1" "rss_site" {
  metadata {
    name = "rss-site"
    labels = {
      app = "web"
    }
  }
  spec {
    replicas = 1
    selector {
      match_labels = {
        app = "web"
      }
    }
    template {
      metadata {
        labels = {
          app = "web"
        }
      }
      spec {
        automount_service_account_token = false
        security_context {
          run_as_non_root = false
          seccomp_profile {
            type = "Localhost"
          }
        }
        container {
          name  = "unsafe_root"
          image = "nginx"
          port {
            container_port = 80
          }
          security_context {
            run_as_non_root            = false
            allow_privilege_escalation = false
            read_only_root_filesystem  = true
            capabilities {
              drop = ["all"]
            }
          }
        }
      }
    }
  }
}

resource "kubernetes_cron_job_v1" "rss_site" {
  metadata {
    name = "rss-site-cron"
    labels = {
      app = "web"
    }
  }
  spec {
    schedule           = "0 * * * *"
    concurrency_policy = "Forbid"
    job_template {
      metadata {
        labels = {
          app = "web"
        }
      }
      spec {
        template {
          metadata {
            labels = {
              app = "web"
            }
          }
          spec {
            automount_service_account_token = false
            security_context {
              run_as_non_root = false
              seccomp_profile {
                type = "Localhost"
              }
            }
            container {
              name  = "unsafe_root"
              image = "nginx"
              port {
                container_port = 80
              }
              security_context {
                run_as_non_root            = false
                allow_privilege_escalation = false
                read_only_root_filesystem  = true
                capabilities {
                  drop = ["all"]
                }
              }
            }
            restart_policy = "OnFailure"
          }
        }
      }
    }
  }
}
