import axios from "axios";

axios.defaults.baseURL = "https://api.example.com";
axios.defaults.headers.post["Content-Type"] = "application/json";
axios.defaults.headers.post["Accept"] = "*/*";

const instance = axios.create({ baseURL: "https://api.example.com" });
let dang_value: string = "*/*";
instance.defaults.headers.common["Accept"] = dang_value;

const dangHeaders = {
  Authorization: "Bearer <token>",
  Accept: "*/*",
};

fetch("https://example.com", { headers: new Headers(dangHeaders) });

axios.get("https://example.com", { headers: dangHeaders });
