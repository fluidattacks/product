using MBrace.FsPickler.Json;

namespace InsecureDeserialization
{
    public class InsecureFsPicklerDeserialization
    {
        public void FsPicklerDeserialization(HttpRequest request)
        {

            var fsPickler = FsPickler.CreateJsonSerializer();

            // Read the request body as a Base64-encoded string
            using var reader = new StreamReader(request.Body);
            string jsonBase64 = reader.ReadToEndAsync();

            // Convert from Base64 to a memory stream and deserialize
            using var memoryStream = new MemoryStream(Convert.FromBase64String(jsonBase64));
            var deserializedObject = fsPickler.Deserialize<object>(memoryStream);

        }
    }

    public class InsecureFsPicklerDeserializationII
    {
        public async Task FsPicklerDeserialization(HttpRequest request)
        {
            var fsPickler = FsPickler.CreateJsonSerializer();

            // Read the request body directly into a byte array (assuming it is Base64-encoded)
            using var memoryStream = new MemoryStream(request.Body);

            // Reset stream position and decode from Base64
            memoryStream.Position = 0;
            var decodedStream = new MemoryStream(Convert.FromBase64String(new StreamReader(memoryStream).ReadToEnd()));

            // Deserialize from decoded stream
            var deserializedObject = fsPickler.Deserialize<object>(decodedStream);
        }
    }

     public class SafeCaseNotUserInput
    {
        public async Task FsPicklerDeserialization(string request)
        {
            var fsPickler = FsPickler.CreateJsonSerializer();

            // Read the request body directly into a byte array (assuming it is Base64-encoded)
            using var memoryStream = new MemoryStream(request.Body);

            // Reset stream position and decode from Base64
            memoryStream.Position = 0;
            var decodedStream = new MemoryStream(Convert.FromBase64String(new StreamReader(memoryStream).ReadToEnd()));

            // Deserialize from decoded stream
            var deserializedObject = fsPickler.Deserialize<object>(decodedStream);
        }
    }
}
