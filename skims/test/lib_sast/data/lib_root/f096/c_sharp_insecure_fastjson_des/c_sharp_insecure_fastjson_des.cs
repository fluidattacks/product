using fastJSON;

namespace InsecureDeserialization
{
    public class InsecureFastJSONDeserialization
    {
        public void FastJSONDeserialization(string json)
        {
            try
            {
                var vuln_obj1 = JSON.ToObject(json, new JSONParameters { BadListTypeChecking = false }); // -> Vulnerable

                var vuln_obj3 = JSON.ToObject(json, new JSONParameters { UseExtensions = true, BadListTypeChecking = false }); // -> Vulnerable

                var jsonParams = new JSONParameters { BadListTypeChecking = false };
                var vuln_obj4 = JSON.ToObject(json, jsonParams); // -> Vulnerable

                var safe_obj1 = JSON.ToObject(json, new JSONParameters { BadListTypeChecking = true }); // -> Safe

                var safe_obj3 = JSON.ToObject(json, new JSONParameters { UseExtensions = true, BadListTypeChecking = true }); // -> Safe

                var jsonParams = new JSONParameters { BadListTypeChecking = true };
                var safe_obj4 = JSON.ToObject(json, jsonParams); // -> Safe

                var jsonParams = new JSONParameters { UseExtensions = true };
                var safe_obj5 = JSON.ToObject(json, jsonParams); // -> Safe
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }
    }
}
