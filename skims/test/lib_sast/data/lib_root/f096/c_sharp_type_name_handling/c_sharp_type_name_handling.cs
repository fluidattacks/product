using Newtonsoft.Json;

public class ExampleClass
{

    public void ExampleClass()
    {
        var Settings = new JsonSerializerSettings();
        Settings.TypeNameHandling = TypeNameHandling.All; // -> Vulnerable
    }
}

namespace InsecureDeserialization
{
    public class InsecureNewtonsoftDeserialization
    {
        public void ExampleClass2()
        {
            var Settings = new JsonSerializerSettings();
            var unsafeValue = TypeNameHandling.All;
            Settings.TypeNameHandling = unsafeValue; // -> Vulnerable
        }

        public void NewtonsoftDeserialization(string json)
        {
            try
            {
                JsonConvert.DeserializeObject<object>(json, new JsonSerializerSettings
                {
                    TypeNameHandling = TypeNameHandling.All // -> Vulnerable
                });
            } catch(Exception e)
            {
                Console.WriteLine(e);
            }
        }

        public void ConverterOverrideSettings(){
            JsonConvert.DefaultSettings = () =>
                new JsonSerializerSettings{TypeNameHandling = TypeNameHandling.Auto}; // -> Vulnerable
            Bar newBar = JsonConvert.DeserializeObject<Bar>(someJson);
        }

        public void ConverterOverrideSettingsStaggeredInitialize(){
            var settings = new JsonSerializerSettings();
            settings.TypeNameHandling = TypeNameHandling.Auto; // -> Vulnerable
            Bar newBar = JsonConvert.DeserializeObject<Bar>(someJson,settings);
        }

        public void ConverterOverrideSettingsMultipleSettingArgs(){
            JsonConvert.DefaultSettings = () =>
                new JsonSerializerSettings{
                    Culture = InvariantCulture,
                    TypeNameHandling = TypeNameHandling.Auto, // -> Vulnerable
                    TraceWriter = traceWriter
                    };
            Bar newBar = JsonConvert.DeserializeObject<Bar>(someJson);
        }

      public void SafeDeserialize(){
        Bar newBar = JsonConvert.DeserializeObject<Bar>(someJson, new JsonSerializerSettings
        {
            TypeNameHandling = TypeNameHandling.None // -> Safe
        });
      }

      public void SafeDefaults(){
        Bar newBar = JsonConvert.DeserializeObject<Bar>(someJson); // -> Safe
      }
    }
}
