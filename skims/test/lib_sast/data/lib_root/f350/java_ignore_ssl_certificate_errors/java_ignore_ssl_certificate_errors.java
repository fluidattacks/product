import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.net.http.SslError;
import android.webkit.SslErrorHandler;

// Source: https://semgrep.dev/r?q=gitlab.mobsf.java-webview-rule-ignore_ssl_certificate_errors
public class MyWebViewClient extends WebViewClient {
    @Override
    public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
        handler.proceed(); // -> Vulnerable
    }
}

public class MyWebViewClient extends WebViewClient {
    @Override
    public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
        System.out.println("SSL Error: " + error.toString());
        handler.proceed(); // -> Vulnerable
    }
}

public class MyWebViewClient extends WebViewClient {
    @Override
    public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
        switch (error.getPrimaryError()) {
            case SslError.SSL_UNTRUSTED:
                System.out.println("SSL Error: Certificate is untrusted.");
                break;
            case SslError.SSL_EXPIRED:
                System.out.println("SSL Error: Certificate has expired.");
                break;
            case SslError.SSL_IDMISMATCH:
                System.out.println("SSL Error: Hostname mismatch.");
                break;
            case SslError.SSL_NOTYETVALID:
                System.out.println("SSL Error: Certificate not yet valid.");
                break;
            default:
                System.out.println("SSL Error: Unknown error.");
                break;
        }
        handler.cancel();  // -> Safe
    }
}

public class MyWebViewClient extends WebViewClient {
    @Override
    public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
        switch (error.getPrimaryError()) {
            case SslError.SSL_UNTRUSTED:
                System.out.println("SSL Error: Certificate is untrusted.");
                handler.cancel();
                break;
            case SslError.SSL_EXPIRED:
                System.out.println("SSL Error: Certificate has expired.");
                handler.cancel();
                break;
            case SslError.SSL_IDMISMATCH:
                System.out.println("SSL Error: Hostname mismatch.");
                handler.cancel();
                break;
            case SslError.SSL_NOTYETVALID:
                System.out.println("SSL Error: Certificate is not yet valid.");
                handler.cancel();
                break;
            default:
                System.out.println("SSL Error: No critical issue detected.");
                handler.proceed(); // -> Safe
                break;
        }
    }
}


public class SecureWebViewClient extends WebViewClient {
    @Override
    public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
        Log.e("SecureWebView", "SSL Error occurred: " + error.toString());
        handler.cancel(); // -> Safe
    }
}

public class ValidatedWebViewClient extends WebViewClient {
    @Override
    public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
        if (isTrustedCertificate(error)) {
            handler.proceed(); // -> Safe
        } else {
            handler.cancel(); // -> Safe
        }
    }

    private boolean isTrustedCertificate(SslError error) {
        return false;
    }
}

public class VulnerableWebViewClient extends WebViewClient {
    @Override
    public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
        handler.proceed(); // -> Vulnerable: Always ignores SSL errors
    }
}

public class InsecureWebViewHandler extends WebViewClient {
    @Override
    public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
        Log.w("SSL_ERROR", "Ignored SSL error: " + error.toString());
        handler.proceed(); // -> Vulnerable
    }
}

public class SecureWebViewClient extends WebViewClient {
    @Override
    public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
        switch (error.getPrimaryError()) {
            case SslError.SSL_UNTRUSTED:
            case SslError.SSL_EXPIRED:
            case SslError.SSL_IDMISMATCH:
            case SslError.SSL_NOTYETVALID:
                handler.cancel(); // -> Safe: Reject invalid certificates
                showSSLErrorDialog(error);
                break;
            default:
                handler.cancel(); // -> Safe
        }
    }

    private void showSSLErrorDialog(SslError error) {
        // Notify user about security risks
        new AlertDialog.Builder(context)
            .setTitle("SSL Certificate Error")
            .setMessage("Secure connection cannot be established.")
            .setPositiveButton("OK", null)
            .show();
    }
}

public class StrictWebViewClient extends WebViewClient {
    @Override
    public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
        handler.cancel(); // -> Safe
        logSSLError(error);
    }

    private void logSSLError(SslError error) {
        // Implement secure logging of SSL validation failures
        SecurityLogger.log("SSL Certificate Validation Failed: " + error.toString());
    }
}
