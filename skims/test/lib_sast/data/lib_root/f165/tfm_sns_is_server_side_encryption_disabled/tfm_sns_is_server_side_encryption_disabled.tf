resource "aws_sns_topic" "vuln" {
  name = "my-topic"
}

resource "aws_sns_topic" "safe" {
  name              = "my-topic"
  kms_master_key_id = "arn:aws:kms:region:account-id:key/key-id"

}