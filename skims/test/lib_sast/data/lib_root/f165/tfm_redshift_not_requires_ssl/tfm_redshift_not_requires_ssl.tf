resource "aws_redshift_parameter_group" "my_redshift_parameter_group" {
  name        = "my-redshift-parameter-group"
  family      = "redshift-1.0"
  description = "My Redshift parameter group"

  parameter {
    name  = "require_ssl"
    value = "false"
  }

  parameter {
    name  = "enable_user_activity_logging"
    value = "false"
  }

  parameter {
    name  = "query_group"
    value = "my_query_group"
  }
}


resource "aws_redshift_parameter_group" "my_redshift_parameter" {
  name        = "my-redshift-parameter-group"
  family      = "redshift-1.0"
  description = "My Redshift parameter group"

  parameter {
    name  = "enable_user_activity_logging"
    value = "false"
  }

  parameter {
    name  = "query_group"
    value = "my_query_group"
  }
}

resource "aws_redshift_cluster" "my_redshift_cluster" {
  parameter_group_name = my_redshift_parameter_group.name
}
