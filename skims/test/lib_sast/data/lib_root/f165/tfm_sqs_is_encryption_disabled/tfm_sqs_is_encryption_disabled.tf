resource "aws_sqs_queue" "vuln" {
  name                    = "my-queue"
  sqs_managed_sse_enabled = false
}

resource "aws_sqs_queue" "safeKMS" {
  name              = "my-queue"
  kms_master_key_id = "arn:aws:kms:region:account-id:key/key-id"
}

resource "aws_sqs_queue" "safesqsbydefault" {
  name = "my-queue"
}

resource "aws_sqs_queue" "safeSSE" {
  name                    = "my-queue"
  sqs_managed_sse_enabled = true
}
