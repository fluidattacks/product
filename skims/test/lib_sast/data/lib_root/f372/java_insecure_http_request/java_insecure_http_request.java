class Bad {
    public void sendbad1() {
        HttpClient client = HttpClient.newHttpClient();
        HttpRequest request = HttpRequest.newBuilder()
            .uri(URI.create("http://openjdk.java.net/")) // -> Vulnerable
            .build();

        client.sendAsync(request, BodyHandlers.ofString())
            .thenApply(HttpResponse::body)
            .thenAccept(System.out::println)
            .join();
    }

    public void sendbad2() {
        HttpClient client = HttpClient.newHttpClient();
        HttpRequest request = HttpRequest.newBuilder()
            .uri(URI.create("http://openjdk.java.net/")) // -> Vulnerable
            .timeout(Duration.ofMinutes(1))
            .header("Content-Type", "application/json")
            .POST(BodyPublishers.ofFile(Paths.get("file.json")))
            .build();

        client.sendAsync(request, BodyHandlers.ofString())
            .thenApply(HttpResponse::body)
            .thenAccept(System.out::println)
            .join();
    }

    public void sendbad3() {
        HttpClient client = HttpClient.newHttpClient();
        HttpRequest request = HttpRequest.newBuilder()
            .uri(URI.create("http://openjdk.java.net/")) // -> Vulnerable
            .timeout(Duration.ofMinutes(1))
            .header("Content-Type", "application/json")
            .POST(BodyPublishers.ofFile(Paths.get("file.json")))
            .build();

        HttpResponse<String> response =
            client.send(request, BodyHandlers.ofString());
    }

    public void sendbad4() {
        HttpClient client = HttpClient.newBuilder()
            .version(Version.HTTP_2)
            .followRedirects(Redirect.SAME_PROTOCOL)
            .proxy(ProxySelector.of(new InetSocketAddress("www-proxy.com", 8080)))
            .authenticator(Authenticator.getDefault())
            .build();
        HttpRequest request = HttpRequest.newBuilder()
            .uri(URI.create("http://openjdk.java.net/")) // -> Vulnerable
            .timeout(Duration.ofMinutes(1))
            .header("Content-Type", "application/json")
            .POST(BodyPublishers.ofFile(Paths.get("file.json")))
            .build();

        HttpResponse<String> response =
            client.send(request, BodyHandlers.ofString());
    }

    public void sendbad5() {
        String uri = "http://openjdk.java.net/";
        HttpClient client = HttpClient.newBuilder().build();
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create(uri)) // -> Vulnerable
                .POST(BodyPublishers.ofString(data))
                .build();

        HttpResponse<?> response = client.send(request, BodyHandlers.discarding());
        System.out.println(response.statusCode());
    }


    public void sendbad6() {
        String uri = "http://openjdk.java.net/";
        HttpClient client = HttpClient.newBuilder().build();
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create(uri)) // -> Vulnerable
                .POST(BodyPublishers.ofString(data))
                .build();

        client.sendAsync(request, BodyHandlers.ofString())
            .thenApply(HttpResponse::body)
            .thenAccept(System.out::println)
            .join();
    }

    public void sendbad7() {
        HttpClient client = HttpClient.newHttpClient();
        HttpRequest request = HttpRequest.newBuilder("http://example.com") // -> Vulnerable
            .build();

        client.sendAsync(request, BodyHandlers.ofString())
            .thenApply(HttpResponse::body)
            .thenAccept(System.out::println)
            .join();
    }

    public void sendbad8() {
        HttpClient client = HttpClient.newHttpClient();
        String url = "http://example.com/api/resource";
        HttpRequest request = HttpRequest.newBuilder(url) // -> Vulnerable
            .timeout(Duration.ofSeconds(30))
            .header("Authorization", "Bearer token")
            .GET()
            .build();

        HttpResponse<String> response = client.send(request, BodyHandlers.ofString());
    }

    public void sendbad9() {
        HttpClient client = HttpClient.newHttpClient();
        HttpRequest request = HttpRequest.newBuilder("http://insecure-site.com/data") // -> Vulnerable
            .POST(BodyPublishers.ofString("payload"))
            .header("Content-Type", "application/json")
            .build();

        client.sendAsync(request, BodyHandlers.ofString())
            .thenAccept(response -> System.out.println(response));
    }

    public void sendbad10() {
        URI uri = URI.create("http://example.com/vulnerable-endpoint");
        HttpClient client = HttpClient.newHttpClient();
        HttpRequest request = HttpRequest.newBuilder(uri) // -> Vulnerable
            .GET()
            .build();

        HttpResponse<String> response = client.send(request, BodyHandlers.ofString());
        System.out.println(response.body());
    }
}

class Ok {
    public void sendok1() {
        HttpClient client = HttpClient.newHttpClient();
        HttpRequest request = HttpRequest.newBuilder()
            .uri(URI.create("https://openjdk.java.net/")) // -> Safe
            .build();

        client.sendAsync(request, BodyHandlers.ofString())
            .thenApply(HttpResponse::body)
            .thenAccept(System.out::println)
            .join();
    }

    public void sendok2() {
        HttpClient client = HttpClient.newHttpClient();
        HttpRequest request = HttpRequest.newBuilder()
            .uri(URI.create("https://openjdk.java.net/")) // -> Safe
            .timeout(Duration.ofMinutes(1))
            .header("Content-Type", "application/json")
            .POST(BodyPublishers.ofFile(Paths.get("file.json")))
            .build();

        client.sendAsync(request, BodyHandlers.ofString())
            .thenApply(HttpResponse::body)
            .thenAccept(System.out::println)
            .join();
    }

    public void sendok3() {
        HttpClient client = HttpClient.newHttpClient();
        HttpRequest request = HttpRequest.newBuilder()
            .uri(URI.create("https://openjdk.java.net/")) // -> Safe
            .timeout(Duration.ofMinutes(1))
            .header("Content-Type", "application/json")
            .POST(BodyPublishers.ofFile(Paths.get("file.json")))
            .build();

        HttpResponse<String> response =
            client.send(request, BodyHandlers.ofString());
    }

    public void sendok4() {
        HttpClient client = HttpClient.newBuilder()
            .version(Version.HTTP_2)
            .followRedirects(Redirect.SAME_PROTOCOL)
            .proxy(ProxySelector.of(new InetSocketAddress("www-proxy.com", 8080)))
            .authenticator(Authenticator.getDefault())
            .build();
        HttpRequest request = HttpRequest.newBuilder()
            .uri(URI.create("https://openjdk.java.net/")) // -> Safe
            .timeout(Duration.ofMinutes(1))
            .header("Content-Type", "application/json")
            .POST(BodyPublishers.ofFile(Paths.get("file.json")))
            .build();

        HttpResponse<String> response =
            client.send(request, BodyHandlers.ofString());
    }

    public void sendok5() {
        String uri = "https://openjdk.java.net/";
        HttpClient client = HttpClient.newBuilder().build();
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create(uri)) // -> Safe
                .POST(BodyPublishers.ofString(data))
                .build();

        HttpResponse<?> response = client.send(request, BodyHandlers.discarding());
        System.out.println(response.statusCode());
    }


    public void sendok6() {
        String uri = "https://openjdk.java.net/";
        HttpClient client = HttpClient.newBuilder().build();
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create(uri)) // -> Safe
                .POST(BodyPublishers.ofString(data))
                .build();

        client.sendAsync(request, BodyHandlers.ofString())
            .thenApply(HttpResponse::body)
            .thenAccept(System.out::println)
            .join();
    }

    public void sendok7() {
        HttpClient client = HttpClient.newHttpClient();
        HttpRequest request = HttpRequest.newBuilder("https://example.com") // -> Safe
            .build();

        client.sendAsync(request, BodyHandlers.ofString())
            .thenApply(HttpResponse::body)
            .thenAccept(System.out::println)
            .join();
    }

    public void sendok8() {
        HttpClient client = HttpClient.newHttpClient();
        String url = "https://example.com/api/resource";
        HttpRequest request = HttpRequest.newBuilder(url) // -> Safe
            .timeout(Duration.ofSeconds(30))
            .header("Authorization", "Bearer token")
            .GET()
            .build();

        HttpResponse<String> response = client.send(request, BodyHandlers.ofString());
    }
}
