import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

class Bad {
    public void bad1() {
        CloseableHttpClient httpclient = HttpClients.createDefault();
        HttpGet httpGet = new HttpGet("http://targethost/homepage"); // -> Vulnerable
        CloseableHttpResponse response1 = httpclient.execute(httpGet);
    }

    public void bad2() {
        CloseableHttpClient httpclient = HttpClients.createDefault();
        System.out.println("hello");
        CloseableHttpResponse response1 = httpclient.execute(new HttpPost("http://example.com")); // -> Vulnerable
    }

    public void bad3() {
        CloseableHttpClient httpclient = HttpClients.createDefault();
        CloseableHttpResponse response1 = httpclient.execute(new HttpPost("http://example.com")); // -> Vulnerable
    }

    public void bad4() {
        String url = "http://testserver.com/resource";
        CloseableHttpClient httpclient = HttpClients.createDefault();
        HttpGet httpGet = new HttpGet(url); // -> Vulnerable
        CloseableHttpResponse response1 = httpclient.execute(httpGet);
    }

    public void bad5() {
        try {
            URI uri = new URI("http://testserver.com/resource");
            CloseableHttpClient httpclient = HttpClients.createDefault();
            HttpGet httpGet = new HttpGet(uri); // -> Vulnerable
            CloseableHttpResponse response1 = httpclient.execute(httpGet);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

class Ok {
    public void ok1() {
        CloseableHttpClient httpclient = HttpClients.createDefault();
        HttpGet httpGet = new HttpGet("https://targethost/homepage"); // -> Safe
        CloseableHttpResponse response1 = httpclient.execute(httpGet);
    }

    public void ok2() {
        CloseableHttpClient httpclient = HttpClients.createDefault();
        System.out.println("hello");
        CloseableHttpResponse response1 = httpclient.execute(new HttpPost("https://example.com")); // -> Safe
    }

    public void ok3() {
        CloseableHttpClient httpclient = HttpClients.createDefault();
        CloseableHttpResponse response1 = httpclient.execute(new HttpPost("https://example.com")); // -> Safe
    }

    public void ok4() {
        String url = "https://testserver.com/resource";
        CloseableHttpClient httpclient = HttpClients.createDefault();
        HttpGet httpGet = new HttpGet(url); // -> Safe
        CloseableHttpResponse response1 = httpclient.execute(httpGet);
    }

    public void ok5() {
        try {
            URI uri = new URI("https://testserver.com/resource");
            CloseableHttpClient httpclient = HttpClients.createDefault();
            HttpGet httpGet = new HttpGet(uri); // -> Safe
            CloseableHttpResponse response1 = httpclient.execute(httpGet);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
