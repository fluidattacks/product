import org.springframework.web.client.RestTemplate;
import org.springframework.web.client.*;

public class Bad {
    public void bad1() {
        RestTemplate restTemplate = new RestTemplate();
        return restTemplate.delete("http://example.com"); // -> Vulnerable
    }

    public void bad2() {
        RestTemplate restTemplate = new RestTemplate();
        String url = "http://example.com";
        void hello = restTemplate.delete(url, object); // -> Vulnerable
    }

    public void bad3() {
        RestTemplate restTemplate = new RestTemplate();
        URI url = new URI("http://example.com");
        void hello = restTemplate.delete(url, object); // -> Vulnerable
    }

    public void bad4() {
        RestTemplate restTemplate = new RestTemplate();
        return restTemplate.doExecute("http://example.com"); // -> Vulnerable
    }

    public void bad5() {
        RestTemplate restTemplate = new RestTemplate();
        String url = "http://example.com";
        result = restTemplate.doExecute(url, object); // -> Vulnerable
    }

    public void bad6() {
        RestTemplate restTemplate = new RestTemplate();
        URI url = new URI("http://example.com");
        result = restTemplate.doExecute(url, object); // -> Vulnerable
    }

    public void bad7() {
        RestTemplate restTemplate = new RestTemplate();
        String fooResourceUrl
        = "http://localhost:8080/spring-rest/foos";
        ResponseEntity<String> response
        = restTemplate.getForEntity(fooResourceUrl, String.class); // -> Vulnerable
        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
    }

    public void bad8() {
        RestTemplate restTemplate = new RestTemplate();
        HttpEntity<Foo> request = new HttpEntity<>(new Foo("bar"));
        String fooResourceUrl = "http://example.com";
        Foo foo = restTemplate.postForObject(fooResourceUrl, request, Foo.class); // -> Vulnerable
        assertThat(foo, notNullValue());
        assertThat(foo.getName(), is("bar"));
    }

    public void bad9() {
        restTemplate template = new RestTemplate();
        Foo updatedInstance = new Foo("newName");
        updatedInstance.setId(createResponse.getBody().getId());
        String resourceUrl = "http://example.com";
        HttpEntity<Foo> requestUpdate = new HttpEntity<>(updatedInstance, headers);
        template.exchange(resourceUrl, HttpMethod.PUT, requestUpdate, Void.class); // -> Vulnerable
    }

    public void bad10() {
        RestTemplate restTemplate = new RestTemplate();
        URI uri = URI.create("http://example.com");
        restTemplate.delete(uri); // -> Vulnerable
    }
}

public class Ok {
    public void ok1() {
        RestTemplate restTemplate = new RestTemplate();
        return restTemplate.delete("https://example.com"); // -> Safe
    }

    public void ok2() {
        RestTemplate restTemplate = new RestTemplate();
        String url = "https://example.com";
        void hello = restTemplate.delete(url, object); // -> Safe
    }

    public void ok3() {
        RestTemplate restTemplate = new RestTemplate();
        URI url = new URI("https://example.com");
        void hello = restTemplate.delete(url, object); // -> Safe
    }

    public void ok4() {
        RestTemplate restTemplate = new RestTemplate();
        return restTemplate.doExecute("https://example.com"); // -> Safe
    }

    public void ok5() {
        RestTemplate restTemplate = new RestTemplate();
        String url = "https://example.com";
        result = restTemplate.doExecute(url, object); // -> Safe
    }

    public void ok6() {
        RestTemplate restTemplate = new RestTemplate();
        URI url = new URI("https://example.com");
        result = restTemplate.doExecute(url, object); // -> Safe
    }

    public void ok7() {
        RestTemplate restTemplate = new RestTemplate();
        String fooResourceUrl
        = "https://localhost:8080/spring-rest/foos";
        ResponseEntity<String> response
        = restTemplate.getForEntity(fooResourceUrl, String.class); // -> Safe
        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
    }

    public void ok8() {
        RestTemplate restTemplate = new RestTemplate();
        HttpEntity<Foo> request = new HttpEntity<>(new Foo("bar"));
        String fooResourceUrl = "https://example.com";
        Foo foo = restTemplate.postForObject(fooResourceUrl, request, Foo.class); // -> Safe
        assertThat(foo, notNullValue());
        assertThat(foo.getName(), is("bar"));
    }

    public void ok9() {
        restTemplate template = new RestTemplate();
        Foo updatedInstance = new Foo("newName");
        updatedInstance.setId(createResponse.getBody().getId());
        String resourceUrl = "https://example.com";
        HttpEntity<Foo> requestUpdate = new HttpEntity<>(updatedInstance, headers);
        template.exchange(resourceUrl, HttpMethod.PUT, requestUpdate, Void.class); // -> Safe
    }
}
