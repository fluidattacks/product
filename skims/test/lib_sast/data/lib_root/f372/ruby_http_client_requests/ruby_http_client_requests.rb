# Source: https://semgrep.dev/r?q=problem-based-packs.insecure-transport.ruby-stdlib.http-client-requests.http-client-requests
require 'httparty'
require 'rest-client'
require 'json'

def bad1
  hash = { foo: }
  a = 1
  b = 2
  c = {a:, b:}
  response = HTTParty.get('http://example.com', format: :plain) # -> Vulnerable
  JSON.parse response, symbolize_names: true
end

def bad2
  str = 'http://example.com'
  response = HTTParty.get(str, format: :plain) # -> Vulnerable
  JSON.parse response, symbolize_names: true
end

def bad3
  RestClient.get('http://example.com/resource') # -> Vulnerable
end

def bad4
  RestClient.post "http://example.com/resource", {'x' => 1}.to_json, {content_type: :json, accept: :json} # -> Vulnerable
end


def ok1
  response = HTTParty.get('https://example.com', format: :plain) # -> Safe
  JSON.parse response, symbolize_names: true
end

def ok2
  str = 'https://example.com'
  response = HTTParty.get(str, format: :plain) # -> Safe
  JSON.parse response, symbolize_names: true
end

def ok3
  RestClient.get('https://example.com/resource') # -> Safe
end

def ok4
  RestClient.post "https://example.com/resource", {'x' => 1}.to_json, {content_type: :json, accept: :json} # -> Safe
end
