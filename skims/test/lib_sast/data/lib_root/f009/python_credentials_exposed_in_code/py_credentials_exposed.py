import jwt

token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyLCJleHAiOjE1MTYyMzkwMjJ9.VFro30FzEcbgAtT6uhLWoLl4vuG6-T7amT_C-8hJhyc"

try:
    public_key = "test_key"
    decoded_token = jwt.decode(token, public_key, algorithms=["RS256"])
    print("Token verified successfully:")
except jwt.ExpiredSignatureError:
    print("Token has expired.")
except jwt.InvalidTokenError:
    print("Token verification failed.")
