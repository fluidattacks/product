import com.auth0.jwt.JWT
import com.auth0.jwt.algorithms.Algorithm
import java.security.interfaces.RSAPublicKey

fun main() {
    // Example JWT token to verify
    val token = "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyLCJleHAiOjE1MTYyMzkwMjJ9.VFro30FzEcbgAtT6uhLWoLl4vuG6-T7amT_C-8hJhyc"

    val publicKey: RSAPublicKey = GET_KEY()

    try {
        val algorithm = Algorithm.RSA256(publicKey, null)
        val verifier = JWT.require(algorithm).build()
        val jwt = verifier.verify(token)
        println("Token verified successfully:")
        println(jwt)
    } catch (e: Exception) {
        println("Token verification failed: ${e.message}")
    }
}
