// Source: https://semgrep.dev/r?q=gitlab.find_sec_bugs.HARD_CODE_PASSWORD-1

// Case 11. CSRFHandler Pattern
CSRFHandler vulnerableHandler = CSRFHandler.create(vertx, "hardcodedSecret123"); // -> Vulnerable

String pbePasswordStr = "hardcodedPass123";
CSRFHandler vulnerableHandler = CSRFHandler.create(vertx, pbePasswordStr); // -> Vulnerable

String csrfSecret = SecureConfigLoader.getCsrfSecret();
CSRFHandler secureHandler = CSRFHandler.create(vertx, csrfSecret); // -> Safe

CSRFHandler csrfHandler = CSRFHandler.create("hardcodedSecret"); // -> Vulnerable

String pbePasswordStr = "hardcodedPass123";
CSRFHandler csrfHandler = CSRFHandler.create(pbePasswordStr); // -> Vulnerable

String secret = System.getenv("CSRF_SECRET");
CSRFHandler csrfHandler = CSRFHandler.create(secret); // -> Safe
