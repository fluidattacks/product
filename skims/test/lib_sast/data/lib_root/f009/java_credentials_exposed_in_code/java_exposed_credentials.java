import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.auth.EnvironmentVariableCredentialsProvider;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;

public class S3Example {
    public static void main(String[] args) {
        AWSStaticCredentialsProvider credentialsProvider = new AWSStaticCredentialsProvider(
            new BasicAWSCredentials("AKIAIOCMG56TISNLV69H", System.getenv("AWS_SECRET_ACCESS_KEY")));

        AmazonS3 s3Client = AmazonS3ClientBuilder.standard()
            .withCredentials(credentialsProvider)
            .withRegion(Regions.US_WEST_2)
            .build();

        s3Client.listBuckets().stream().map(bucket -> bucket.getName()).forEach(System.out::println);
    }
}


public class JWTExample {
    public static void main(String[] args) {
        String fakeJwtToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c";

        Claims claims = Jwts.parser()
            .setSigningKey("fakeSigningKey")
            .parseClaimsJws(fakeJwtToken)
            .getBody();

        String fullName = (String) claims.get("name");
    }
}
