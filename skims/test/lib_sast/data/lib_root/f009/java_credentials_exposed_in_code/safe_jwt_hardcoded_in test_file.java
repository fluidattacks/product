package co.com.bancolombia.api.utils;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.lang.reflect.Field;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(SpringExtension.class)
class ValidateJWTTest {


    @InjectMocks
    private ValidateJWT validateJWT;

    @BeforeEach
    void setUp() throws Exception {
        MockitoAnnotations.openMocks(this);
        setField(validateJWT, "secretTokenJwt", "B8LXMbE1pb4xtqrnqlC3dTjjtv6ZfWwdxBZtQHsyV6c5OQ2FqR5q6nit34kfA3H6gDcVTPjlde8IILeoSSzHYCzvDAbx047vmyWl");
    }

    private void setField(Object target, String fieldName, Object value) throws Exception {
        Field field = target.getClass().getDeclaredField(fieldName);
        field.setAccessible(true);
        field.set(target, value);
    }

    @Test
    void testValidateToken() {
        String token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJCQUFTS09HIiwiaWF0IjoxNzIwNzMzODk3LCJleHAiOjE3NTA3MzQ0ODR9.0lCrkIC5r2hoiLWKTqNr51uo_FhUvQAI1q_XX5M24YFQ_ympAfF1NH83tMppni5WOYcBsDOTMIdt_CDa4lRC6w";
        String seedName = "BAASKOG";

        assertTrue(validateJWT.validateToken(token, seedName));
    }

    @Test
    void testValidateTokenInvalid() {
        String token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJCQUFTS09HIiwiaWF0IjoxNzIwNzMzODk3LCJleHAiOjE3NTA3MzQ0ODR9.0lCrkIC5r2hoiLWKTqNr51uo_FhUvQAI1q_XX5M24YFQ_ympAfF1NH83tMppni5WOYcBsDOTMIdt_CDa4lRC6w";
        String seedName = "BAASPRUEBA";


        assertFalse(validateJWT.validateToken(token, seedName));

    }

    //Se pasa token invalido para que entre por JwtException
    @Test
    public void testValidateTokenThrowsJwtException() {
        String invalidToken = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJCQUFTS09HIiwiaWF0IjoxNzIwNzMzODk3LCJleHAiOjE3NTA3MzQ0ODR9.0lCrkIC5r2hoiLWKTqNr51uo_FhUvQAI1q_XX5M24YFQ_ympAfF1NH83tMppni5WOYcBsDOTMIdt_CDa4lWAAAS";
        String seedName = "anySeedName";

        assertFalse(validateJWT.validateToken(invalidToken, seedName));
    }

    @Test
    void validateTokenQueryLogsTest() {

        var token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJpc3MiOiJCYWNrZW5kUHJpbmNpcGFsIiwic3ViIjoiMTIzNDY1Nzk4IiwiZXhwIjoyNzMxNDQyOTA1LCJpYXQiOjE3MzE0NDI5MTUsImp0aSI6IjEwYTQwYzg4LTQ3NzUtNDEyNy05MWU5LTViYTY4YWZkN2MwNSIsIkRPQ1VNRU5UX05VTUJFUiI6IjEyMzQ2NTc5OCIsIlNDT1BFIjpbIlJPTEVfVVNFUiJdLCJET0NVTUVOVF9UWVBFIjoiQ0MifQ.MPcU7UI7IhTOxVy2xyx49TMjALWGh0ACU_vEFKkCrOxNqQj4vJYPO5TVDGoEZompjtfiwPHoER1xxePvVM4rNw";

        var result = validateJWT.validateTokenQueryLogs(token);

        Assertions.assertTrue(result);
    }

    @Test
    void validateTokenQueryLogsInvalidTokenTest() {

        var result = validateJWT.validateTokenQueryLogs("Bearer 123465798");

        Assertions.assertFalse(result);
    }

}
