const express_alias = require('express');
const cors = require('cors')
const app = express_alias()

// unsafe
app.options('*', cors())

// safe
const corsOptions = {
    origin: 'https://example.com',
    optionsSuccessStatus: 200
};
app.use(cors(corsOptions));
app.options('*', cors(corsOptions));


// Vulnerable cases
const unsafeCorsOptions = {
    origin: '*',
    optionsSuccessStatus: 200
};

const defaultVulnCorsOptions = {
    optionsSuccessStatus: 200
};

app.use(cors()); // -> Vulnerable: Enable All CORS Requests
app.use(cors(unsafeCorsOptions)); // -> Vulnerable
app.use(cors({ origin: '*' })); // -> Vulnerable
app.use(cors(defaultVulnCorsOptions)); // -> Vulnerable no origin in options


app.get('/path', cors(), (req, res) => res.send('GET request to /path')); // -> Vulnerable
app.post('/path', cors({ origin: '*' }), (req, res) => res.send('POST request to /path')); // -> Vulnerable
app.put('/path', cors(unsafeCorsOptions), (req, res) => res.send('PUT request to /path')); // -> Vulnerable
app.patch('/path', cors(), (req, res) => res.send('PATCH request to /path')); // -> Vulnerable
app.delete('/path', cors(), (req, res) => res.send('DELETE request to /path')); // -> Vulnerable
app.options('/path', cors(), (req, res) => res.send('OPTIONS request to /path')); // -> Vulnerable
app.head('/path', cors(), (req, res) => res.send('HEAD request to /path')); // -> Vulnerable
app.all('/path', cors(), (req, res) => res.send('Request to /path with any method')); // -> Vulnerable

// Safe cases
app.use(cors({ origin: 'https://example.com' })); // -> Safe

app.get('/path', cors(corsOptions), (req, res) => res.send('GET request to /path')); // -> Safe
app.post('/path', cors({ origin: 'https://example.com' }), (req, res) => res.send('POST request to /path')); // -> Safe
app.put('/path', cors(corsOptions), (req, res) => res.send('PUT request to /path')); // -> Safe
app.patch('/path', cors(corsOptions), (req, res) => res.send('PATCH request to /path')); // -> Safe
app.delete('/path', cors(corsOptions), (req, res) => res.send('DELETE request to /path')); // -> Safe
app.options('/path', cors(corsOptions), (req, res) => res.send('OPTIONS request to /path')); // -> Safe
app.head('/path', cors(corsOptions), (req, res) => res.send('HEAD request to /path')); // -> Safe
app.all('/path', (req, res) => res.send('Request to /path with any method')); // -> Safe
