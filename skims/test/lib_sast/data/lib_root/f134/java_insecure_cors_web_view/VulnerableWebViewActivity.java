package test.data.lib_root.f134.java_insecure_cors_web_view;

import android.os.Bundle;
import android.webkit.WebSettings;
import android.webkit.WebView;
import androidx.appcompat.app.AppCompatActivity;

public class VulnerableWebViewActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vulnerable_webview);

        WebView webView = findViewById(R.id.vulnerableWebView);

        // Insecure WebView settings
        WebSettings settings = webView.getSettings();
        settings.setJavaScriptEnabled(true);  // Enable JavaScript
        settings.setAllowUniversalAccessFromFileURLs(true);  // Insecure: Allows CORS from file URLs
        settings.setAllowFileAccessFromFileURLs(true);  // Insecure: Allows file URL access

        // Load a URL that could be exploited
        webView.loadUrl("file:///android_asset/vulnerable_page.html");
    }
}
