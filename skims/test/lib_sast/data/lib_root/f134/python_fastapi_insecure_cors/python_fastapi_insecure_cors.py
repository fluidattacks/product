# type: ignore

from fastapi import (
    FastAPI,
)
from fastapi.middleware.cors import (
    CORSMiddleware,
)

app = FastAPI()
app_fake = FastAPIFake()  # For test

# ------ Start vulnerable part: --------

origins = [
    "http://localhost.tiangolo.com",
    "https://localhost.tiangolo.com",
    "http://localhost",
    "http://localhost:8080",
    "*",
]

# Insecure CORS configuration allowing all origins
app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],  # -> Vulnerable
)

# Insecure CORS configuration allowing all origins, methods, and headers
app.add_middleware(
    CORSMiddleware,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
    allow_origins=["*"],  # -> Vulnerable
)

# Insecure CORS configuration allowing all origins, methods, and headers
app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,  # -> Vulnerable
)

# ------ End vulnerable part: --------


# -------- Start safe part: ----------

origins2 = [
    "http://localhost.tiangolo.com",
    "https://localhost.tiangolo.com",
    "http://localhost",
    "http://localhost:8080",
]

# CORS configuration allowing some origins
app.add_middleware(
    CORSMiddleware,
    allow_origins=origins2,  # -> Safe
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


# It is not an instance of FastAPI, then is not vulnerable
app_fake.add_middleware(
    CORSMiddleware,
    allow_origins=origins,  # -> Safe, because is not a FastAPI instance
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

# -------- End safe part: ----------


@app.get("/")
def read_root():
    return {"message": "Hello, world!"}
