# type: ignore

from flask import (
    Flask,
)
from flask_cors import (
    CORS,
)

app_flask = Flask(__name__)
app = "test"

# Allow all origins for all routes
CORS(app_flask)  # Non compliant

# Allow all origins for all routes using a resources dictionary
CORS(app_flask, resources={r"/*": {"origins": "*"}})  # Non compliant

CORS(
    app_flask,
    resources={
        r"/*": {"origins": "https://trustedorigin.com"}
    },  # Allows only a specific origin
)  # Safe

CORS(
    app_flask,
    resources={
        r"/*": {
            "origins": "*",  # Allow all origins
            "methods": ["GET", "POST"],  # Allows only necessary methods
            "allow_headers": [
                "Authorization",
                "Content-Type",
            ],  # Allows only necessary headers
        }
    },
)  # Non compliant

CORS(
    app_flask,
    resources={
        r"/*": {
            "origins": [
                "https://trustedorigin.com"
            ],  # Allows only a specific origin
            "methods": ["GET", "POST"],  # Allows only necessary methods
            "allow_headers": [
                "Authorization",
                "Content-Type",
            ],  # Allows only necessary headers
        }
    },
)  # Safe

# Allow all origins, methods, and headers for all routes
CORS(app_flask, supports_credentials=True)  # Non compliant

# Allow all origins
CORS(app_flask, origins="*")  # Non compliant

allowed_origins = "*"

# Allow all origins using the variable
CORS(app_flask, app_flask, origins=allowed_origins)  # Non compliant

CORS(app_flask, origins="https://trustedorigin.com")  # Safe

CORS()  # Safe

CORS(app)  # Safe


@app_flask.route("/example")
def example():
    return "Hello world!"


if __name__ == "__main__":
    app_flask.run()
