<?php

$dang_complete = "Access-Control-Allow-Origin: *";
$dang_complete_access = "Access-Control-Allow-Origin: " . $_SERVER['HTTP_ORIGIN'];

$dang_header = "Access-Control-Allow-Origin";
$dang_header_colon = "Access-Control-Allow-Origin: ";

$dang_val = "*";
$dang_val_access = $_SERVER['HTTP_ORIGIN'];

$safe_header = "Content-Type";
$safe_val = "application/json";


// Danger invocations, following lines must be marked:

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Origin: ' . '*');
header('Access-Control-Allow-Origin: ' . $dang_val);
header('Access-Control-Allow-Origin: ' . $dang_val_access);
header('Access-Control-Allow-Origin: ' . $_SERVER['HTTP_ORIGIN']);
header($dang_complete);
header($dang_complete_access);
header($dang_header . ':' . '*');
header($dang_header . ':' . $dang_val);
header($dang_header . ':' . $dang_val_access);
header($dang_header . ':' . '*');
header($dang_header . '*');
header($dang_header_colon . $dang_val);
header($dang_header_colon . $dang_val_access);

// Safe invocations, following lines are safe:

header("Content-Type: application/json");
header("Content-Type: *");
header($dang_header . ':' . 'OtherVal');
header('Content-Type: ' . $dang_val);
header('Content-Type: ' . $dang_val_access);
