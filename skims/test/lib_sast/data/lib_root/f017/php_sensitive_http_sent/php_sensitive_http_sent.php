<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use GuzzleHttp\Client;

// Non compliant code
function insecure_info_1(Request $request)
{
    $sensitive_info = $request->input('sensitive_info');

    $client = new Client();
    $res = $client->request('GET', 'http://example.com/api/send_info', [
        'query' => ['info' => $sensitive_info]
    ]);

    return response()->json(['message' => 'Information sent']);
};

function insecure_info_2(Request $request)
{
    $email = $request->input('email');
    $password = $request->input('password');

    $client = new Client();
    $res = $client->request('POST', 'http://example.com/api/login', [
        'form_params' => [
            'email' => $email,
            'password' => $password
        ]
    ]);

    return response()->json(['message' => 'Login successful']);
};

function insecure_info_3(Request $request)
{
    $credit_card = $request->input('credit_card');
    $cvv = $request->input('cvv');

    $client = new Client();
    $res = $client->request('GET', 'https://example.com/api/pay', [
        'form_params' => [
            'credit_card' => $credit_card,
            'cvv' => $cvv
        ]
    ]);

    return response()->json(['message' => 'Payment successful']);
};

// Compliant code
function secure_info_1(Request $request)
{
    $sensitive_info = $request->input('sensitive_info');

    // Encrypt the sensitive information before sending
    $encrypted_info = encrypt($sensitive_info);

    $client = new Client(['base_uri' => 'https://example.com']);
    $res = $client->request('POST', '/api/send_info', [
        'form_params' => ['info' => $encrypted_info]
    ]);

    return response()->json(['message' => 'Information sent']);
};

function secure_info_2(Request $request)
{
    $email = $request->input('email');
    $password = $request->input('password');

    // Encrypt the sensitive information before sending
    $encrypted_email = encrypt($email);
    $encrypted_password = encrypt($password);

    $client = new Client(['base_uri' => 'https://example.com']);
    $res = $client->request('POST', '/api/login', [
        'form_params' => [
            'email' => $encrypted_email,
            'password' => $encrypted_password
        ]
    ]);

    return response()->json(['message' => 'Login successful']);
};

function secure_info_3(Request $request)
{
    $credit_card = $request->input('credit_card');
    $cvv = $request->input('cvv');

    // Encrypt the sensitive information before sending
    $encrypted_credit_card = encrypt($credit_card);
    $encrypted_cvv = encrypt($cvv);

    $client = new Client(['base_uri' => 'https://example.com']);
    $res = $client->request('POST', '/api/pay', [
        'form_params' => [
            'credit_card' => $encrypted_credit_card,
            'cvv' => $encrypted_cvv
        ]
    ]);

    return response()->json(['message' => 'Payment successful']);
};

?>
