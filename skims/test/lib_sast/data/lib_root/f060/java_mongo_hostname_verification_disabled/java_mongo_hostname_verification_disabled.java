// Source: https://semgrep.dev/r?q=trailofbits.jvm.mongo-hostname-verification-disabled.mongo-hostname-verification-disabled
package test;

import com.mongodb.MongoClientSettings;
import com.mongodb.MongoClients;
import org.mongodb.client.*;
import org.springframework.data.mongodb.*;

class BadConfiguration {
    public static void main(String[] args) {
        MongoClientSettings settings = MongoClientSettings.builder()
            .applyToSslSettings(builder -> {
                builder.enabled(true);
                builder.invalidHostNameAllowed(true); // -> Vulnerable
            })
            .build();

        Boolean invalidHostNameAllowed = true;

        MongoClientSettings settings = MongoClientSettings.builder()
            .applyToSslSettings(builder -> {
                builder.enabled(true);
                builder.invalidHostNameAllowed(invalidHostNameAllowed); // -> Vulnerable
            })
            .build();

    }
}

class OKConfiguration {
    public static void main(String[] args) {
        MongoClientSettings settings = MongoClientSettings.builder()
            .applyToSslSettings(builder -> { // -> Safe
                builder.enabled(true);
            })
            .build();

        Boolean invalidHostNameAllowed = false;

        MongoClientSettings settings = MongoClientSettings.builder()
            .applyToSslSettings(builder -> {
                builder.enabled(true);
                builder.invalidHostNameAllowed(invalidHostNameAllowed); // -> Safe
            })
            .build();
    }
}
