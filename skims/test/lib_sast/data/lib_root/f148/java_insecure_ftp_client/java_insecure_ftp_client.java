class Bad {
    public static void badftp1() {
        String server = "www.yourserver.net";
        int port = 21;
        FTPClient ftpClient = new FTPClient();
        ftpClient.connect(server, port); // -> Vulnerable
    }

    public static void badftp4() {
        String insecureHost = System.getenv("FTP_HOST");
        FTPClient ftpClient = new FTPClient();
        ftpClient.connect(insecureHost, 21); // -> Vulnerable
    }

    public static void badftp5() {
        FTPClient ftpClient = new FTPClient();
        ftpClient.connect("ftp.example.com", 21); // -> Vulnerable
        ftpClient.setProxy("proxy.example.com");
    }

}

class Ok {

    public static void goodftp2() {
        String secureHost = System.getenv("SFTP_HOST");
        FTPSClient ftpsClient = new FTPSClient();
        ftpsClient.connect(secureHost, 21); // -> Safe
    }
}
