class Bad {
    public static void badftp2() {
        URL url = new URL("ftp://user01:pass1234@ftp.foo.com/README.txt;type=i");
        URLConnection urlc = url.openConnection(); // -> Vulnerable
        InputStream is = urlc.getInputStream(); // To download
        OutputStream os = urlc.getOutputStream(); // To upload
    }
    public static void badftp6() {
        String ftpUrl = "ftp://user:pass@ftp.example.com/resource";
        URL url = new URL(ftpUrl);
        URLConnection conn = url.openConnection(); // -> Vulnerable
    }
}

class Ok {
    public static void badftp1() {
        URL url = new URL("sftp://user01:pass1234@ftp.foo.com/README.txt;type=i");
        URLConnection urlc = url.openConnection(); // -> Safe
        InputStream is = urlc.getInputStream(); // To download
        OutputStream os = urlc.getOutputStream(); // To upload
    }

    public static void goodftp3() {
        String fullUrl = "sftp://user01:pass1234@ftp.foo.com/README.txt;type=i";
        URL url = new URL(fullUrl);
        URLConnection conn = url.openConnection(); // -> Safe
    }
}
