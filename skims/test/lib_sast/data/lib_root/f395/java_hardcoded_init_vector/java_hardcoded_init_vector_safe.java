// Safe because javax.crypto.spec was not imported
public class StaticIV {

    public StaticIV() {
        byte[] iv = {
            (byte) 0, (byte) 0, (byte) 0, (byte) 0,
            (byte) 0, (byte) 0, (byte) 0, (byte) 0,
            (byte) 0, (byte) 0, (byte) 0, (byte) 0,
            (byte) 0, (byte) 0, (byte) 0, (byte) 0
        };

        IvParameterSpec staticIvSpec = new IvParameterSpec(iv); // -> Safe

        c.init(Cipher.ENCRYPT_MODE, skeySpec, staticIvSpec, new SecureRandom());
    }
}
