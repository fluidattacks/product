import javax.crypto.spec.IvParameterSpec;

public class StaticIV {

    public StaticIV() {
        byte[] iv = {
            (byte) 0, (byte) 0, (byte) 0, (byte) 0,
            (byte) 0, (byte) 0, (byte) 0, (byte) 0,
            (byte) 0, (byte) 0, (byte) 0, (byte) 0,
            (byte) 0, (byte) 0, (byte) 0, (byte) 0
        };

        IvParameterSpec staticIvSpec = new IvParameterSpec(iv); // -> Vulnerable

        c.init(Cipher.ENCRYPT_MODE, skeySpec, staticIvSpec, new SecureRandom());
    }
}

public class StaticIV2 {
    byte[] iv = {
        (byte) 0, (byte) 0, (byte) 0, (byte) 0,
        (byte) 0, (byte) 0, (byte) 0, (byte) 0,
        (byte) 0, (byte) 0, (byte) 0, (byte) 0,
        (byte) 0, (byte) 0, (byte) 0, (byte) 0
    };


    public StaticIV2() {
        IvParameterSpec staticIvSpec = new IvParameterSpec(iv); // -> Vulnerable

        c.init(Cipher.ENCRYPT_MODE, skeySpec, staticIvSpec, new SecureRandom());
    }
}

public class RandomIV {

    public RandomIV() {

        byte[] iv = new byte[16];
        new SecureRandom().nextBytes(iv);

        IvParameterSpec staticIvSpec = new IvParameterSpec(iv); // -> Safe

        c.init(Cipher.ENCRYPT_MODE, skeySpec, staticIvSpec, new SecureRandom());
    }

    public NotRandomIV() {

        byte[] iv = new byte[16];

        IvParameterSpec staticIvSpec = new IvParameterSpec(iv); // -> Vulnerable

        c.init(Cipher.ENCRYPT_MODE, skeySpec, staticIvSpec, new SecureRandom());
    }
}

public class EncryptionUtil {

    private static final String ALGORITHM = "AES";
    private static final String MODE = "CBC";
    private static final String PADDING = "PKCS5Padding";
    private static final String IV = "0123456789abcdef"; // static and hardcoded initialization vector

    private static SecretKeySpec getKey(String password) {
        return new SecretKeySpec(password.getBytes(), ALGORITHM);
    }

    public static byte[] encrypt(String data, String password) throws Exception {
        cipher.init(Cipher.ENCRYPT_MODE, getKey(password), new IvParameterSpec(IV.getBytes())); // -> Vulnerable
        return cipher.doFinal(data.getBytes());
    }

    public static byte[] encrypt2(String data, String password) throws Exception {
        String initVector = "0123456789abcdefg"; // static and hardcoded initialization vector
        cipher.init(Cipher.ENCRYPT_MODE, getKey(password), new IvParameterSpec(initVector.getBytes())); // -> Vulnerable
        return cipher.doFinal(data.getBytes());
    }

    public static byte[] encryptError(String data, String password) throws Exception {
        cipher.init(Cipher.ENCRYPT_MODE, getKey(password), new IvParameterSpec()); // -> Safe. NullPointerException because no IV is set
        return cipher.doFinal(data.getBytes());
    }
}
