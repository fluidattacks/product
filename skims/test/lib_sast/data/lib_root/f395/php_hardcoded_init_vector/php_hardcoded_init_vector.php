<?php
    $cipherMethod = 'AES-256-CBC';
    $key = 'ThisIsASecretKey';
    $static_iv = '1234567890123456'; // This is a static, hardcoded initialization vector

    function vulnCaseEncrypt($data) {
        global $cipherMethod, $key, $iv;
        $encryptedData = openssl_encrypt($data, $cipherMethod, $key, OPENSSL_RAW_DATA, $static_iv);
        return $encryptedData;
    }

    function vulnCaseDecrypt($encryptedData) {
        global $cipherMethod, $key, $iv;
        $decryptedData = openssl_decrypt($encryptedData, $cipherMethod, $key, OPENSSL_RAW_DATA, $static_iv);
        return $decryptedData;
    }


    function safeCaseEncrypt($data) {
        global $cipherMethod, $key;
        $iv = openssl_random_pseudo_bytes(openssl_cipher_iv_length($cipherMethod)); // Generate a random IV
        $encryptedData = openssl_encrypt($data, $cipherMethod, $key, OPENSSL_RAW_DATA, $iv);
        return base64_encode($encryptedData . '::' . $iv); // Store the IV along with the encrypted data
    }

    function safeCaseDecrypt($encryptedData) {
        global $cipherMethod, $key;
        list($encryptedData, $iv) = explode('::', base64_decode($encryptedData), 2); // Retrieve the IV from the stored data
        $decryptedData = openssl_decrypt($encryptedData, $cipherMethod, $key, OPENSSL_RAW_DATA, $iv);
        return $decryptedData;
    }
?>
