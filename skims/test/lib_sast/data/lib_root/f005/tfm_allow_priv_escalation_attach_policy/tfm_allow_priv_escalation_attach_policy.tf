locals {
  prod_common = {
    policies = {
      aws = {
        AdminPolicy = [
          {
            Sid    = "allWrite"
            Effect = "Allow"
            Action = [
              "iam:AttachUserPolicy",
              "iam:CreatePolicyVersion",
              "iam:SetDefaultPolicyVersion",
            ]
            Resource = ["*"]
          }
        ]
      }
    }
  }
}

module "test" {
  source   = "./modules/aws"
  name     = "test"
  policies = local.prod_common.policies.aws
}
