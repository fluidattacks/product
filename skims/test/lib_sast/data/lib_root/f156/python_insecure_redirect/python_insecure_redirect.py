# type: ignore
# settings.py
from flask import (
    abort,
    Flask,
    redirect,
    request,
    url_for,
)
import logging
import re
from urllib.parse import (
    urljoin,
    urlparse,
)

app = Flask(__name__)


logging.basicConfig(level=logging.INFO)


ALLOWED_DOMAINS = {"example.com", "trusted.com"}


def is_authenticated_user():
    # This function checks if the user is authenticated and is omitted for brevity
    pass


@app.route("/")
def home():
    if not is_authenticated_user():
        logging.info("Unauthorized access attempt.")
        return redirect(url_for("login"))

    redirect_url = request.args.get("redirect_url")
    if redirect_url:
        logging.info(f"Redirecting to: {redirect_url}")
        return redirect(redirect_url)  # -> Vulnerable

    return "Welcome to the home page!"


def is_safe_redirect_url(target):
    host_url = request.host_url
    redirect_url = urljoin(host_url, target)
    parsed_url = urlparse(redirect_url)

    if redirect_url.startswith(host_url):
        return True

    return parsed_url.hostname in ALLOWED_DOMAINS


@app.route("/")
def home():
    if not is_authenticated_user():
        logging.info("Unauthorized access attempt.")
        return redirect(url_for("login"))

    redirect_url = request.args.get("redirect_url", "")
    if redirect_url and is_safe_redirect_url(redirect_url):
        logging.info(f"Redirecting to: {redirect_url}")
        return redirect(redirect_url)  # -> Safe

    if redirect_url:
        logging.warning(f"Unsafe redirect attempt to: {redirect_url}")
        return abort(400, "Invalid redirect URL")

    return "Welcome to the home page!"


def is_safe_url(target_url):
    if target_url is None:
        return False

    if not bool(urlparse(target_url).netloc):
        return True

    ref_url = urljoin(request.host_url, target_url)
    test_url = urlparse(ref_url)

    return test_url.netloc in ALLOWED_DOMAINS


def validate_redirect_pattern(url):
    dangerous_patterns = [
        r"javascript:",
        r"data:",
        r"vbscript:",
        r"\\\\",  # Doble backslash
        r"%2f",  # URL encoded slash
        r"%5c",  # URL encoded backslash
    ]

    url_lower = url.lower()
    return not any(
        re.search(pattern, url_lower) for pattern in dangerous_patterns
    )


@app.route("/")
def home():
    if not is_authenticated_user():
        logging.info("Unauthorized access attempt.")
        return redirect(url_for("login"))

    redirect_url = request.args.get("redirect_url")
    if redirect_url:
        if not is_safe_url(redirect_url):
            logging.warning(f"Unsafe redirect attempt to: {redirect_url}")
            return "Invalid redirect URL", 400

        if not validate_redirect_pattern(redirect_url):
            logging.warning(
                f"Potentially malicious redirect pattern detected: {redirect_url}"
            )
            return "Invalid URL pattern", 400

        logging.info(f"Safe redirect to: {redirect_url}")
        return redirect(redirect_url)  # -> Safe

    return "Welcome to the home page!"


def undeterministic_redirect(url):
    redirect(url)  # -> Safe


@app.route("/login")
def login():
    # Simulated login page
    return "Login Page - User authentication goes here."


if __name__ == "__main__":
    app.run(debug=False)
