# type: ignore
# settings.py
from flask import (
    abort,
    Flask,
    redirect,
    request,
    url_for,
)
import logging
from markupsafe import (
    escape,
)
import re
from urllib.parse import (
    urljoin,
    urlparse,
)

# Whitelists
ALLOWED_REDIRECTS = ["/", "/home", "/profile", "/dashboard"]
ALLOWED_DOMAINS = {"example.com", "api.example.com", "subdomain.example.com"}
ALLOWED_PATHS = {"/home", "/dashboard", "/profile", "/login", "/logout"}


app = Flask(__name__)


logging.basicConfig(level=logging.INFO)


def is_authenticated_user():
    # This function checks if the user is authenticated and is omitted for brevity
    pass


@app.route("/")
def home():
    if not is_authenticated_user():
        logging.info("Unauthorized access attempt.")
        return redirect(url_for("login"))

    redirect_url = request.args.get("redirect_url")
    if redirect_url:
        logging.info(f"Redirecting to: {redirect_url}")  # -> Vulnerable
        return redirect(redirect_url)

    return "Welcome to the home page!"


@app.route("/redirect")
def home():
    if not is_authenticated_user():
        logging.info("Unauthorized access attempt.")
        return redirect(url_for("login"))

    logging.info(  # -> Vulnerable
        f'Redirecting to: {request.args.get("redirect_url")}'
    )
    return redirect(request.args.get("redirect_url"))


def is_safe_url(target):
    ref_url = urlparse(request.host_url)
    test_url = urlparse(urljoin(request.host_url, target))
    return (
        test_url.scheme in ["http", "https"]
        and ref_url.netloc == test_url.netloc
        and test_url.path in ALLOWED_REDIRECTS
    )


@app.route("/redirect2")
def validate_with_method():
    if not is_authenticated_user():
        logging.info("Unauthorized access attempt.")
        return redirect(url_for("login"))

    redirect_url = request.args.get("redirect_url", "/")

    if redirect_url and is_safe_url(redirect_url):
        logging.info(f"Redirecting to: {escape(redirect_url)}")  # -> Safe
        return redirect(redirect_url)
    else:
        logging.warning(  # -> Safe (to avoid FP)
            f"Unsafe redirecting to: {escape(redirect_url)}"
        )
        return "Redirect URL not allowed or unsafe.", 400


def is_relative_url(url):
    return url.startswith("/") and not url.startswith("//")


@app.route("/redirect3")
def validate_with_method2():
    if not is_authenticated_user():
        logging.info("Intento de acceso no autorizado.")
        return redirect(url_for("login"))

    redirect_url = request.args.get("redirect_url", "/")

    if is_relative_url(redirect_url):
        logging.info(f"Redirecting to: {escape(redirect_url)}")  # -> Safe
        return redirect(redirect_url)
    else:
        logging.warning(  # -> Safe (to avoid FP)
            f"Unsafe redirecting to: {escape(redirect_url)}"
        )
        return "Redirect URL not allowed or unsafe.", 400


@app.route("/redirect3")
def validate_with_whitelist():
    if not is_authenticated_user():
        logging.info("Intento de acceso no autorizado.")
        return redirect(url_for("login"))

    redirect_url = request.args.get("redirect_url", "/")

    if redirect_url in ALLOWED_REDIRECTS:
        logging.info(f"Redirecting to: {escape(redirect_url)}")  # -> Safe
        return redirect(redirect_url)
    else:
        logging.warning(  # -> Safe (to avoid FP)
            f"Unsafe redirecting to: {escape(redirect_url)}"
        )
        return "Redirect URL not allowed or unsafe.", 400


@app.route("/redirect4")
def validate_with_conditional():
    redirect_url = request.args.get("redirect_url", "/")
    if redirect_url in {"/", "/home"}:
        logging.info(f"Redirecting to: {escape(redirect_url)}")  # -> Safe
        return redirect(redirect_url)

    return "Redirect URL not allowed or unsafe.", 400


@app.route("/redirect5")
def validate_with_pattern():
    redirect_url = request.args.get("redirect_url", "/")
    safe_pattern = r"^/(?:user/\d+|products/[a-zA-Z0-9-]+)(?:\?.*)?$"

    if re.match(safe_pattern, redirect_url):
        logging.info(f"Redirecting to: {escape(redirect_url)}")  # -> Safe
        return redirect(redirect_url)

    return "Redirect URL not allowed or unsafe.", 400


@app.route("/redirect6")
def validate_with_domain():
    redirect_url = request.args.get("redirect_url", "/")
    try:
        parsed_url = urlparse(redirect_url)
        if parsed_url.netloc in ALLOWED_REDIRECTS:
            logging.info(  # -> Safe
                f"Redirecting to allowed domain: {escape(redirect_url)}"
            )
            return redirect(redirect_url)

    except Exception as e:
        logging.error(f"URL parsing error: {str(e)}")

    return abort(400, "Invalid redirect URL")


@app.route("/redirect7")
def validate_with_path_prefix():
    redirect_url = request.args.get("redirect_url", "/")
    SAFE_PREFIXES = {"/api/v1/", "/static/", "/public/"}

    if any(redirect_url.startswith(prefix) for prefix in SAFE_PREFIXES):
        logging.info(  # -> Safe
            f"Redirecting to safe prefix URL: {escape(redirect_url)}"
        )
        return redirect(redirect_url)
    return abort(400, "Invalid redirect URL")


@app.route("/redirect8")
def validate_with_multiple_checks():
    redirect_url = request.args.get("redirect_url", "/")

    def is_valid_url() -> bool:
        if not redirect_url:
            return False

        if redirect_url.startswith("/"):
            return redirect_url in ALLOWED_PATHS

        try:
            parsed = urlparse(redirect_url)
            return (
                parsed.scheme in {"http", "https"}
                and parsed.netloc in ALLOWED_DOMAINS
                and parsed.path in ALLOWED_PATHS
            )
        except Exception:
            return False

    if is_valid_url():
        logging.info(  # -> Safe
            f"Redirecting to validated URL: {escape(redirect_url)}"
        )
        return redirect(redirect_url)

    return abort(400, "Invalid redirect URL")


@app.route("/redirect9")
def validate_with_custom_rules():
    redirect_url = request.args.get("redirect_url", "/")

    class URLValidator:
        def __init__(self, url: str):
            self.url = url
            self.parsed = urlparse(url)

        def is_safe(self) -> bool:
            return all(
                [
                    self._has_safe_scheme(),
                    self._has_safe_domain(),
                    self._has_safe_path(),
                    self._has_safe_query(),
                ]
            )

        def _has_safe_scheme(self) -> bool:
            return self.parsed.scheme in {"http", "https", ""}

        def _has_safe_domain(self) -> bool:
            return (
                not self.parsed.netloc or self.parsed.netloc in ALLOWED_DOMAINS
            )

        def _has_safe_path(self) -> bool:
            return self.parsed.path in ALLOWED_PATHS

        def _has_safe_query(self) -> bool:
            return "javascript:" not in self.url.lower()

    validator = URLValidator(redirect_url)
    if validator.is_safe():
        logging.info(  # -> Safe
            f"Redirecting to custom validated URL: {escape(redirect_url)}"
        )
        return redirect(redirect_url)

    return abort(400, "Invalid redirect URL")


@app.route("/notice", methods=["GET", "POST"])
def notice():
    try:
        if request.method == "GET":
            from_uid = request.args.get("from_uid", None)
            notice_list = nu.get(from_uid)

            logging.info(  # -> Vulnerable
                "<notice GET> success. from_uid = %s" % from_uid
            )
            return response_success({"notices": notice_list})

        elif request.method == "POST":
            name = request.form["admin_name"]
            password = request.form["password"]

            if au.admin.isAdmin(name, password):
                notice = Notice().from_dict(dict(request.form))
                nu.add(notice)

                logging.info(  # -> Safe
                    "<notice POST> success. name = %s" % name
                )
                return response_success(get_simple_success_content("notice"))
            else:
                logging.warning(  # -> Safe (to avoid FP)
                    "<notice POST> not_permitted. name = %s, password = %s"
                    % (name, password)
                )
                return response_error(
                    get_simple_error_content(ResponseError.not_permitted)
                )

    except Exception as e:
        logging.error(  # -> Vulnerable
            "<{name}>: unexpected. request = {request}, request.args = {r_args}, request.form = {form}".format(
                name="notice",
                request=request,
                r_args=request.args,
                form=request.form,
            )
        )
        return response_unexpected(e)


@app.route("/redirect10")
def validate_with_multiple_checks():
    redirect_url = request.args.get("redirect_url", "/")
    redirect_url_other = "test"

    def is_valid_url() -> bool:
        if not redirect_url_other:
            return False

        if redirect_url_other.startswith("/"):
            return redirect_url_other in ALLOWED_PATHS

        try:
            parsed = urlparse(redirect_url_other)
            return (
                parsed.scheme in {"http", "https"}
                and parsed.netloc in ALLOWED_DOMAINS
                and parsed.path in ALLOWED_PATHS
            )
        except Exception:
            return False

    if is_valid_url():
        logging.info(  # -> Vulnerable
            f"Redirecting to validated URL: {escape(redirect_url)}"
        )
        return redirect(redirect_url)

    return abort(400, "Invalid redirect URL")


@app.route("/login")
def login():
    # Simulated login page
    return "Login Page - User authentication goes here."


if __name__ == "__main__":
    app.run(debug=False)
