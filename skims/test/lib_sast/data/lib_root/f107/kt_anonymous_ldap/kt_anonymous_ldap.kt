import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.*;

public class Cls {

    public fun ldapBind(): Void {
        val env: MutableMap<String, Any> = HashMap();
        env.put(Context.PROVIDER_URL, "ldap://your-ldap-server:389");
        env.put(Context.SECURITY_AUTHENTICATION, "none")
        var ctx: DirContext = InitialDirContext(env)
    }

    public fun ldapBindSafe(): Void {
        val env: MutableMap<String, Any> = HashMap();
        env.put(Context.PROVIDER_URL, "ldap://your-ldap-server:389");
        env.put(Context.SECURITY_AUTHENTICATION, "simple")
        var ctx = InitialDirContext(env)
    }
}
