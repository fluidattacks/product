
resource "aws_instance" "unsafe" {
  ami           = "ami-005e54dee72cc1d00"
  instance_type = "t2.micro"
}

resource "aws_instance" "safe0" {
  # This one is safe because as it is associated with a network interface,
  # it can have a seecurity group defined at network interface definition level.

  ami           = "ami-005e54dee72cc1d00"
  instance_type = "t2.micro"

  network_interface {
    network_interface_id = aws_network_interface.foo.id
    device_index         = 0
  }

}

resource "aws_instance" "safe1" {
  ami             = "ami-12345678"
  instance_type   = "t2.micro"
  subnet_id       = "subnet-12345678"
  key_name        = "example_key"
  security_groups = ["my_security_group"]
}

resource "aws_instance" "safe2" {
  ami                    = "ami-12345678"
  instance_type          = "t2.micro"
  subnet_id              = "subnet-12345678"
  key_name               = "example_key"
  vpc_security_group_ids = ["sg-12345678", "sg-87654321"]
}
