resource "aws_launch_template" "unsafe" {
  name = "foo"

  cpu_options {
    core_count       = 4
    threads_per_core = 2
  }

  network_interfaces {
    associate_public_ip_address = true
  }

}

resource "aws_launch_template" "safe" {
  name = "foo"
  network_interfaces {
    associate_public_ip_address = true
  }

  vpc_security_group_ids = ["sg-12345678"]
}


resource "aws_launch_template" "safe2" {
  name = "foo"
  network_interfaces {
    associate_public_ip_address = false
    security_groups             = [aws_security_group.EC2_SecurityGroup.id]
  }

}
