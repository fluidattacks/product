import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Value("${maxSessions}")
    private int maxSessions;

    private int insecureMaxSessions = 2;

    protected void configureVulnerable1(HttpSecurity http) throws Exception {
        http
            .authorizeRequests()
                .antMatchers("/admin/**").hasRole("ADMIN")
                .anyRequest().authenticated()
                .and()
            .formLogin()
                .permitAll()
                .and()
            .sessionManagement()
                .maximumSessions(-1); // -> Vulnerable: No limit on concurrent sessions
    }

    protected void configureVulnerable2(HttpSecurity http) throws Exception {
        http
            .authorizeRequests()
                .antMatchers("/admin/**").hasRole("ADMIN")
                .anyRequest().authenticated()
                .and()
            .formLogin()
                .permitAll()
                .and()
            .sessionManagement()
                .maximumSessions(insecureMaxSessions); // -> Vulnerable
    }

    protected void configureSecure1(HttpSecurity http) throws Exception {
        http
            .authorizeRequests()
                .antMatchers("/admin/**").hasRole("ADMIN")
                .anyRequest().authenticated()
                .and()
            .formLogin()
                .permitAll()
                .and()
            .sessionManagement()
                .maximumSessions(1) // -> Secure: Limit to 1 simultaneous session per user
                .maxSessionsPreventsLogin(true)
                .sessionRegistry(sessionRegistry());
    }

    protected void configureSecure2(HttpSecurity http) throws Exception {
        http
            .authorizeRequests()
                .antMatchers("/admin/**").hasRole("ADMIN")
                .anyRequest().authenticated()
                .and()
            .formLogin()
                .permitAll()
                .and()
            .sessionManagement()
                .maximumSessions(maxSessions); // -> Secure: Unknown values
    }

}
