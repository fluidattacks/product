using System;
using Microsoft.IdentityModel.Tokens;

class TestClass1
{
    // Non compliant code
    public void InsecureTestMethod()
    {
        TokenValidationParameters parameters = new TokenValidationParameters();
        parameters.RequireExpirationTime = false;
        parameters.RequireSignedTokens = false; // -> Vulnerable
        parameters.ValidateAudience = false;
        parameters.ValidateIssuer = false;
        parameters.ValidateLifetime = false;
    }
}

class TestClass2
{
    // Compliant code
    public void SecureTestMethod()
    {
        TokenValidationParameters parameters = new TokenValidationParameters();
        parameters.RequireExpirationTime = true;
        parameters.RequireSignedTokens = true; // -> Safe
        parameters.ValidateAudience = true;
        parameters.ValidateIssuer = true;
        parameters.ValidateLifetime = true;
    }
}

public class JwtTestService
{
    private readonly IServiceCollection _services;

    public JwtTestService(IServiceCollection services)
    {
        _services = services;
    }

    public void ConfigureJwtAuthentication()
    {
        // First JWT configuration (Vulnerable)
        _services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme).AddJwtBearer(options =>
        {
            options.TokenValidationParameters = new TokenValidationParameters
            {
                ValidateLifetime = false, // -> Vulnerable
                RequireSignedTokens = false, // -> Vulnerable
                ValidateIssuer = false, // -> Vulnerable
                ValidateAudience = false, // -> Vulnerable
                RequireExpirationTime = false // -> Vulnerable
            };
        });

        // Second JWT configuration (Safe)
        _services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme).AddJwtBearer(options =>
        {
            options.TokenValidationParameters = new TokenValidationParameters
            {
                ValidateLifetime = true, // -> Safe
                RequireSignedTokens = true, // -> Safe
                ValidateIssuer = true, // -> Safe
                ValidateAudience = true, // -> Safe
                RequireExpirationTime = true // -> Safe
            };
        });
    }
}
