// Source: https://semgrep.dev/r?q=java.spring.security.audit.cookie-serializer-secure-false.cookie-serializer-secure-false
package sample;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory;
import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession;
import org.springframework.session.web.http.CookieSerializer;
import org.springframework.session.web.http.DefaultCookieSerializer;

@Import(EmbeddedRedisConfig.class)
@EnableRedisHttpSession
public class Config {

	@Bean
	public LettuceConnectionFactory connectionFactory() {
		return new LettuceConnectionFactory();
	}

	@Bean
	public CookieSerializer cookieSerializer1() {
		DefaultCookieSerializer serializer = new DefaultCookieSerializer(); // -> Vulnerable
		serializer.setCookieName("JSESSIONID");
		serializer.setCookiePath("/");
		serializer.setDomainNamePattern("^.+?\\.(\\w+\\.[a-z]+)$");
		return serializer;
	}

	@Bean
	public CookieSerializer cookieSerializer2() {
    Boolean isSecure = false;
		DefaultCookieSerializer serializer = new DefaultCookieSerializer(); // -> Vulnerable
		serializer.setCookieName("JSESSIONID");
		serializer.setCookiePath("/");
		serializer.setDomainNamePattern("^.+?\\.(\\w+\\.[a-z]+)$");
    serializer.setUseSecureCookie(isSecure);
		return serializer;
	}

	@Bean
	public CookieSerializer cookieSerializer3() {
    Boolean isSecure = true;
		DefaultCookieSerializer serializer = new DefaultCookieSerializer(); // -> Safe
		serializer.setCookieName("JSESSIONID");
		serializer.setCookiePath("/");
		serializer.setDomainNamePattern("^.+?\\.(\\w+\\.[a-z]+)$");
    serializer.setUseSecureCookie(isSecure);
		return serializer;
	}

	@Bean
	public CookieSerializer cookieSerializer4() {
		DefaultCookieSerializer serializer = new DefaultCookieSerializer(); // -> Safe
		serializer.setCookieName("JSESSIONID");
		serializer.setCookiePath("/");
		serializer.setDomainNamePattern("^.+?\\.(\\w+\\.[a-z]+)$");
    serializer.setUseSecureCookie(true);
		return serializer;
	}

}
