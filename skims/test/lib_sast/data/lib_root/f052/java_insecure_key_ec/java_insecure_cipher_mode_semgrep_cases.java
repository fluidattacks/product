// Source: https://semgrep.dev/r?q=java.lang.security.audit.weak-ssl-context.weak-ssl-context
import java.lang.Runtime;

class Cls {

    public Cls() {
        System.out.println("Hello");
    }

    public void test1() {
        SSLContext ctx = SSLContext.getInstance("SSL"); // -> Vulnerable
    }

    public void test2() {
        SSLContext ctx = SSLContext.getInstance("TLS"); // -> Safe. Depends on the Java version
    }

    public void test3() {
        SSLContext ctx = SSLContext.getInstance("TLSv1"); // -> Vulnerable
    }

    public void test4() {
        SSLContext ctx = SSLContext.getInstance("SSLv3"); // -> Vulnerable
    }

    public void test5() {
        SSLContext ctx = SSLContext.getInstance("TLSv1.1"); // -> Vulnerable
    }

    public void test6() {
        SSLContext ctx = SSLContext.getInstance("TLSv1.2"); // -> Safe
    }

    public void test7() {
        SSLContext ctx = SSLContext.getInstance("TLSv1.3"); // -> Safe
    }

    public String getSslContext() {
        return "Anything";
    }

    public void test8() {
        SSLContext ctx = SSLContext.getInstance(getSslContext()); // -> Safe
    }
}
