package main

import (
    "fmt"
    "os"
    "github.com/dgrijalva/jwt-go"
    "time"
)

func unsafe() {
    secretKey := []byte("your-secret-key")

    claims := jwt.MapClaims{
        "username": "exampleUser",
        "exp":      time.Now().Add(time.Hour * 1).Unix(),
    }

    token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)

    signedToken, err := token.SignedString(secretKey)
    if err != nil {
        return
    }

    fmt.Println("JWT Token:", signedToken)
}

func safe() {
    secretKey := os.Getenv("SECRET_KEY")
    if secretKey == "" {
        fmt.Println("SECRET_KEY environment variable is not set.")
        return
    }

    claims := jwt.MapClaims{
        "username": "exampleUser",
    }

    token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)

    signedToken, err := token.SignedString([]byte(secretKey))
    if err != nil {
        fmt.Println("Error signing token:", err)
        return
    }

}
