package f052

import java.security.MessageDigest
import java.security.NoSuchAlgorithmException
import java.security.spec.ECGenParameterSpec
import java.security.spec.RSAKeyGenParameterSpec
import javax.crypto.Cipher
import javax.crypto.NoSuchPaddingException

class test {
    fun main(args: Array<String>) {

        val c1 = Cipher.getInstance("AES")
        val cipher_ins = "DES"
        val c2 = Cipher.getInstance(cipher_ins)
        val c18 = Cipher.getInstance("RSA/ECB/OAEPWithSHA-1AndMGF1Padding")
        val c19 = Cipher.getInstance("RSA/ECB/OAEPWithSHA-256AndMGF1Padding")

        val k1 = RSAKeyGenParameterSpec(2048, RSAKeyGenParameterSpec.F4)
        val key = 2047
        val k2 = RSAKeyGenParameterSpec(key, RSAKeyGenParameterSpec.F4)
        val k3 = ECGenParameterSpec("secp521r1")
        val k4 = ECGenParameterSpec("c2pnb208w1")

        val spec1: ConnectionSpec = (
            ConnectionSpec.Builder(ConnectionSpec.MODERN_TLS)
                .tlsVersions(TlsVersion.TLS_1_1)
                .build()
        )
        val spec2: ConnectionSpec = (
            ConnectionSpec.Builder(ConnectionSpec.MODERN_TLS)
                .tlsVersions(TlsVersion.TLS_1_2)
                .build()
        )

        val md2: MessageDigest = MessageDigest.getInstance("SHA1")
        val md3: MessageDigest = MessageDigest.getInstance("SHA-512")

        val ssl_version: String = "SSLv3"
	    val unsafecontext: SSLContext = SSLContext.getInstance(ssl_version)
    }
}
