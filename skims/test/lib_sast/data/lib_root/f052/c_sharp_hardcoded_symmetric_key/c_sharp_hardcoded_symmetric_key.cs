using Microsoft.IdentityModel.Tokens;

namespace jwtauth.server.web
{
    public class SimetricKey
    {
        public void MustFail()
        {
            var secretKey = "mysupersecretkey";
            var signingKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(secretKey));
            SigningCredentials = new SigningCredentials(signingKey, SecurityAlgorithms.HmacSha256);
        }

        public void MustFailII()
        {
            var secretKey = "mysupersecretkey";
            var byteKey = Encoding.ASCII.GetBytes(secretKey);
            var signingKey = new SymmetricSecurityKey(byteKey);
            SigningCredentials = new SigningCredentials(signingKey, SecurityAlgorithms.HmacSha256);
        }

        public void MustPass()
        {
            byte[] keyForHmacSha256 = new byte[64];

            var randomGen = RandomNumberGenerator.Create();
            randomGen.GetBytes(keyForHmacSha256);

            var securityKey = new SymmetricSecurityKey(keyForHmacSha256);
            var signingCredentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256Signature);


        }
    }
}
