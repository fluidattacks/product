import io.fusionauth.jwt.Signer
import io.fusionauth.jwt.domain.JWT
import io.fusionauth.jwt.hmac.HMACSigner
import org.junit.Assert.assertTrue
import org.junit.Test
import java.time.ZoneOffset
import java.time.ZonedDateTime

class JwtTest {

    fun mustFail() {
        val accessKey = "auzNN7V0aB30poSilNi15HCiE"

        val signer: Signer = HMACSigner.newSHA256Signer(accessKey)

        val jwt = JWT()
            .addClaim("data", "flow")
            .setIssuedAt(ZonedDateTime.now(ZoneOffset.UTC))
            .setExpiration(ZonedDateTime.now(ZoneOffset.UTC).plusHours(2))

        val token = JWT.getEncoder().encode(jwt, signer)
    }

    fun mustFailByte() {
        val accessKey = "auzNN7V0aB30poSilNi15HCiE"

        val castKey = stringToByteArray(accessKey)

        val signer: Signer = HMACSigner.newSHA384Signer(castKey)

        val jwt = JWT()
            .addClaim("data", "flow")
            .setIssuedAt(ZonedDateTime.now(ZoneOffset.UTC))
            .setExpiration(ZonedDateTime.now(ZoneOffset.UTC).plusHours(2))

        val token = JWT.getEncoder().encode(jwt, signer)
    }

    fun mustPass() {

        val accessKey = System.getenv("JWT_SIGN_SECRET")

        val signer: Signer = HMACSigner.newSHA256Signer(accessKey)

        val jwt = JWT()
            .addClaim("data", "flow")
            .setIssuedAt(ZonedDateTime.now(ZoneOffset.UTC))
            .setExpiration(ZonedDateTime.now(ZoneOffset.UTC).plusHours(2))

        val token = JWT.getEncoder().encode(jwt, signer)
    }
}
