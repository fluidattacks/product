import com.auth0.jwt.JWT
import com.auth0.jwt.algorithms.Algorithm

val secret = "hardcoded_str"
val secretByte = stringToByteArray(secret)
val secretByteII = secret.toByteArray(Charsets.UTF_8)


val algorithm = Algorithm.HMAC256("secret")
val algorithmII = Algorithm.HMAC384(secretByte)
val algorithmIII = Algorithm.HMAC512(secretByteII)


val unsafeToken = JWT.create()
    .withIssuer("my-app")
    .withSubject("user123")
    .sign(algorithm)

val safeSecret = System.getenv("JWT_SIGN_SECRET")
val algorithmSafe = Algorithm.HMAC256(safeSecret)

val safeToken = JWT.create()
    .withIssuer("my-app")
    .withSubject("user123")
    .sign(algorithmSafe)
