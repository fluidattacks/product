<?php

$data = "Sensitive Information!";
$key = "supersecretkey";
$iv = openssl_random_pseudo_bytes(16);

// Unsafe cases
$encrypted_1 = base64_encode(mcrypt_encrypt(MCRYPT_DES, $key, $data, MCRYPT_MODE_CBC, $iv));
$encrypted_2 = mcrypt_encrypt(MCRYPT_DES, $key, $data, MCRYPT_MODE_CBC, $iv);
$encrypted_3 = mcrypt_decrypt(MCRYPT_TRIPLEDES, $key, $data, MCRYPT_MODE_CBC, $iv);


?>
