import SwiftJWT

func generateJWTVuln(withClaim claim: JWTClaimsSet) throws -> String {
    let signer = JWTSigner.hs256(key: Data("your-secret-key"))
    let jwt = JWT(claims: claim)
    let jwtData = try jwt.sign(using: signer)
    let jwtString = jwtData.base64URLEncodedString()

    return jwtString
}

func generateJWTSafe(withClaim claim: JWTClaimsSet, secretKey: String) throws -> String {
    let signer = JWTSigner.hs256(key: Data(secretKey))
    let jwt = JWT(claims: claim)
    let jwtData = try jwt.sign(using: signer)
    let jwtString = jwtData.base64URLEncodedString()

    return jwtString
}
