import type { PixelMessage } from './shared'
import React from "react";

declare let window: any


// Vulnerable report
export const CustomFixForVulnerability = (): React.JSX.Element => {
  let customFix;
  window.addEventListener("message", (event): void => {
    const message: { command: string; payload: string } = event.data;


    switch (message.command) {
      case "nextLine":
        customFix == "Delete this line"
        break;
      case "failedToFetchData":
        customFix = undefined
        break;
      default:
        break;
    }
  });

  if (customFix === undefined) {
    return <div>"Could not generate a custom fix for the vulnerability"</div>;
  }

  return (
    <div>
      {customFix}
    </div>
  );
};


function handleEvents(e: PixelMessage) {
  if (e.origin === "somesafesite.com") {
    return
  }
}

//Safe Report
window.addEventListener('message', handleEvents)

// Safe report
window.addEventListener('message', function handleEvents(e: PixelMessage) {
    if (e.origin === window.location.origin || e.origin === "safesite.com"){
      // Do something
    }
  }
)

// Safe report
window.addEventListener(
  'message',
  (event) => {
    if (event.origin !== ('https://sandbox.esignlive.com' || 'https://apps.e-signlive.com')){
      return
    } else {
      return
    }
  },
)
