const axiosAlias = require("axios");
import kyAlias from "ky";

const dangerousCases = async () => {
  const axiosResponse = await axiosAlias.get(`https://example.com`);
  localStorage.setItem("axiosKey", axiosResponse);

  const fetchResponse = await fetch("/movies");
  localStorage.setItem("fetchKey", fetchResponse);

  const kyResponse = await kyAlias("/movies");
  localStorage.setItem("fetchKey", kyResponse);

};


// async/await implementation
async function unsafegetUser() {
  try {
    const responseI = await axiosAlias.get("/user?ID=12345");
    localStorage.setItem("sensDataIII", responseI);
  } catch (error) {
    console.error(error);
  }
}


const safeCases = async () => {
  const safeVar = await otherFunction("/example");
  localStorage.setItem("safe", safeVar);
};
