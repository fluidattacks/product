# type: ignore
# Example source: https://semgrep.dev/r?q=python.boto3.security.hardcoded-token.hardcoded-token
# AWS Credentials Pattern: https://awsteele.com/blog/2020/09/26/aws-access-key-format.html
import boto3
from boto3 import (
    client,
)
import os

client(
    "s3",
    aws_secret_access_key="jWnyxxxxxxxxxxxxxxxxX7ZQxxxxxxxxxxxxxxxx",  # -> Vulnerable
)

boto3.sessions.Session(
    aws_secret_access_key="jWnyxxxxxxxxxxxxxxxxX7ZQxxxxxxxxxxxxxxxx"  # -> Vulnerable
)

s = boto3.sessions
s.Session(aws_access_key_id="AKIAJLVYNHUWCPKOPSYQ")  # -> Vulnerable

uhoh_key = "ASIAJLVYNHUWCPKOPSYQ"
ok_secret = os.environ.get("SECRET_ACCESS_KEY")
s3 = boto3.resource(
    "s3",
    aws_access_key_id=uhoh_key,  # -> Vulnerable
    aws_secret_access_key=ok_secret,  # -> Safe
    region_name="sfo2",
    endpoint_url="https://sfo2.digitaloceanspaces.com",
)

ok_key = os.environ.get("ACCESS_KEY_ID")

uhoh_secret = "jWnyxxxxxxxxxxxxxxxxX7ZQxxxxxxxxxxxxxxxx"
s3 = boto3.resource(
    "s3",
    aws_access_key_id=ok_key,  # -> Safe
    aws_secret_access_key=uhoh_secret,  # -> Vulnerable
    region_name="sfo2",
    endpoint_url="https://sfo2.digitaloceanspaces.com",
)

vul_session_token = "FQoDYXdzEPP//////////wEaDPv5GPAhRW8pw6/nsiKsAZu7sZDCXPtEBEurxmvyV1r+nWy1I4VPbdIJV+iDnotwS3PKIyj+yDnOeigMf2yp9y2Dg9D7r51vWUyUQQfceZi9/8Ghy38RcOnWImhNdVP5zl1zh85FHz6ytePo+puHZwfTkuAQHj38gy6VF/14GU17qDcPTfjhbETGqEmh8QX6xfmWlO0ZrTmsAo4ZHav8yzbbl3oYdCLICOjMhOO1oY+B/DiURk3ZLPjaXyoo2Iql2QU="
s3 = boto3.resource(
    "s3",
    aws_access_key_id=ok_key,  # -> Safe
    aws_secret_access_key=ok_secret,  # -> Safe
    aws_session_token=vul_session_token,  # -> Vulnerable
    region_name="sfo2",
    endpoint_url="https://sfo2.digitaloceanspaces.com",
)

ok_secret = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
s3 = boto3.resource(
    "s3",
    aws_access_key_id=ok_key,  # -> Safe
    aws_secret_access_key=ok_secret,  # -> Safe
    region_name="sfo2",
    endpoint_url="https://sfo2.digitaloceanspaces.com",
)

ok_token = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
s3 = boto3.resource(
    "s3",
    aws_access_key_id=ok_key,  # -> Safe
    aws_secret_access_key=ok_secret,  # -> Safe
    aws_session_token=ok_token,  # -> Safe
    region_name="sfo2",
    endpoint_url="https://sfo2.digitaloceanspaces.com",
)

s3 = client("s3", aws_access_key_id="this-is-not-a-key")  # -> Safe

s3 = boto3.resource(
    "s3",
    aws_access_key_id="XXXXXXXX",  # -> Safe
    aws_secret_access_key="----------------",  # -> Safe
    region_name="us-east-1",
)

s3 = boto3.resource(
    "s3",
    aws_access_key_id="<your token here>",  # -> Safe
    aws_secret_access_key="<your secret here>",  # -> Safe
    region_name="us-east-1",
)

key = os.environ.get("ACCESS_KEY_ID")
secret = os.environ.get("SECRET_ACCESS_KEY")
s3 = boto3.resource(
    "s3",
    aws_access_key_id=key,  # -> Safe
    aws_secret_access_key=secret,  # -> Safe
    region_name="sfo2",
    endpoint_url="https://sfo2.digitaloceanspaces.com",
)

# ruff: noqa: S105
