import mysql from 'mysql';
import crypto from 'crypto';

const connection = mysql.createConnection({
  host: 'localhost',
  user: 'root',
  password: 'password',
  database: 'test',
});

connection.connect();

function vuln (req, res) {
  const name = req.query.name; // user-controlled input
  const password = crypto.createHash('sha256').update(req.query.password).digest('base64');

  const sql = "select * from user where name = '" + name + "' and password = '" + password + "'";

  connection.query(sql, function(err, result) { // Noncompliant
    if (err) {
      return res.status(500).send('Internal Server Error');
    }

    res.send(result);
  })
}

function safe (req, res) {
  const name = req.query.name;
  const password = crypto.createHash('sha256').update(req.query.password).digest('base64');

  const sql = "select * from user where name = ? and password = ?";

  connection.query(sql, [name, password], function(err, result) {
    if (err) {
      return res.status(500).send('Internal Server Error');
    }

    res.send(result);
  })
}


const express = require('express')

module.exports = function login () {
  return (req, res, next) => {
    sequelize.query(
      `SELECT * FROM Users WHERE email = '${req.body.email || ''}' AND password = '${req.body.password || ''}' AND deletedAt IS NULL`,
      { plain: true }
    )
      .then(() => {
        res.send("a")
      }).catch((error) => {
        next(error)
      })
  }
}
