using System;
using System.Web.Configuration;

class TestClass{
    public void VulnCase(){

        HttpRuntimeSection httpObj = new HttpRuntimeSection();
        httpObj.EnableHeaderChecking = false;

        bool isEnabledCheck = false;
        HttpRuntimeSection httpObj2 = new HttpRuntimeSection();
        httpObj2.EnableHeaderChecking = isEnabledCheck;

    }

    public void SafeCase(){

        bool isEnabledCheck = true;
        HttpRuntimeSection httpObj = new HttpRuntimeSection();
        httpObj.EnableHeaderChecking = isEnabledCheck;

        httpObj = new CleanHttpImplementation();
        httpObj.EnableHeaderChecking = false;

    }
}
