<?php

  $data = "Sensitive Information";
  $key = "random_key";
  $method_128 = "AES-128-CBC";  // Insecure encryption algorithm
  $method_256 = "AES-256-CBC";  // Secure encryption algorithm

  // vuln cases AES-128

  $iv = openssl_random_pseudo_bytes(openssl_cipher_iv_length($method_128));
  $encrypted = openssl_encrypt($data, $method_128, $key, OPENSSL_RAW_DATA, $iv);

  $decrypted = openssl_decrypt($encrypted, $method_128, $key, OPENSSL_RAW_DATA, $iv);

  // vuln cases lack of Initialization Vector

  $iv = openssl_random_pseudo_bytes(openssl_cipher_iv_length($method_256));
  $encrypted = openssl_encrypt($data, $method_256, $key);

  $decrypted = openssl_decrypt($encrypted, $method_256, $key);

  // Safe case AES-256 with IV

  $iv = openssl_random_pseudo_bytes(openssl_cipher_iv_length($method_256));
  $encrypted = openssl_encrypt($data, $method_256, $key, OPENSSL_RAW_DATA, $iv);

  $decrypted = openssl_decrypt($encrypted, $method_256, $key, OPENSSL_RAW_DATA, $iv);
