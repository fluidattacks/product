resource "aws_sqs_queue" "example" {
  name = "example-queue"

  policy = jsonencode({
    Version = "2012-10-17",
    Id      = "ExamplePolicy",
    Statement = [
      {
        Sid       = "AllowPublicAccess",
        Effect    = "Allow",
        Principal = "*",
        Action    = "sqs:SendMessage",
        Resource  = self.arn
      }
    ]
  })
}
