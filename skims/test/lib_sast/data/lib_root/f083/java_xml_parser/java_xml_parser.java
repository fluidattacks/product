import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.parsers.SAXParser;
import javax.xml.XMLConstants;

public class Test {

    private final String URL_DISALLOW = "http://apache.org/xml/features/disallow-doctype-decl";

    public String sAXParserVuln(HttpServletRequest request) {
        String body = WebUtils.getRequestBody(request);
        SAXParserFactory spf = SAXParserFactory.newInstance();
        SAXParser parser = spf.newSAXParser();
        parser.parse(body);  // parse xml
    }

    public String xmlReaderVuln(HttpServletRequest request) {
        String body = WebUtils.getRequestBody(request);
        XMLReader xmlReader = XMLReaderFactory.createXMLReader();
        xmlReader.parse(body);  // Dangerous parse xml
    }

    public String documentBuilderVuln(HttpServletRequest request) {
        String body = WebUtils.getRequestBody(request);
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = dbf.newDocumentBuilder();
        db.parse(body);  // Dangerous parse xml
    }

    public String sAXParserSec(HttpServletRequest request) {
        // Safe option: https://cheatsheetseries.owasp.org/cheatsheets/XML_External_Entity_Prevention_Cheat_Sheet.html
        String body = WebUtils.getRequestBody(request);
        SAXParserFactory spf = SAXParserFactory.newInstance();
        spf.setFeature(URL_DISALLOW, true);
        SAXParser parser = spf.newSAXParser();
        parser.parse(body);  // Safe parse xml
    }

    public String sAXParserSec(HttpServletRequest request) {
        String body = WebUtils.getRequestBody(request);
        SAXParserFactory spf = SAXParserFactory.newInstance();

        // Disallowed URL comes from another file or module
        spf.setFeature(Constants.URL_DISALLOW, true);
        SAXParser parser = spf.newSAXParser();
        parser.parse(body);  // Non deterministic option
    }

    public String documentBuilderSec(HttpServletRequest request) {
        String body = WebUtils.getRequestBody(request);
        DocumentBuilderFactory df = DocumentBuilderFactory.newInstance();
        df.setAttribute(XMLConstants.ACCESS_EXTERNAL_DTD, "");
        df.setAttribute(XMLConstants.ACCESS_EXTERNAL_SCHEMA, "");
        DocumentBuilder db = df.newDocumentBuilder();
        db.parse(body);  // Safe parse xml
    }


    // New test case created following the false positive reported in the following case: https://cheatsheetseries.owasp.org/cheatsheets/XML_External_Entity_Prevention_Cheat_Sheet.html#jaxp-documentbuilderfactory-saxparserfactory-and-dom4j
    @RequestMapping(method=RequestMethod.POST, value="/safeProcess")
    public String SafeProcess(String inputXml) {
            if (inputXml == null) {
                return "Provide an inputXml variable";
            }
            DocumentBuilderFactory dbfS = DocumentBuilderFactory.newInstance();
            String FEATURE = null;
            try {
                // This is the PRIMARY defense. If DTDs (doctypes) are disallowed, almost all
                // XML entity attacks are prevented
                // Xerces 2 only - http://xerces.apache.org/xerces2-j/features.html#disallow-doctype-decl
                FEATURE = "http://apache.org/xml/features/disallow-doctype-decl";
                dbfS.setFeature(FEATURE, true);

                // and these as well, per Timothy Morgan's 2014 paper: "XML Schema, DTD, and Entity Attacks"
                dbfS.setXIncludeAware(false);

                // remaining parser logic
            } catch (ParserConfigurationException e) {
                // This should catch a failed setFeature feature
                // NOTE: Each call to setFeature() should be in its own try/catch otherwise subsequent calls will be skipped.
                // This is only important if you're ignoring errors for multi-provider support.
                logger.info("ParserConfigurationException was thrown. The feature '" + FEATURE
                + "' is not supported by your XML processor.");
            } catch (SAXException e) {
                // On Apache, this should be thrown when disallowing DOCTYPE
                logger.warning("A DOCTYPE was passed into the XML document");
            } catch (IOException e) {
                // XXE that points to a file that doesn't exist
                logger.error("IOException occurred, XXE may still possible: " + e.getMessage());
            }

            // Load XML file or stream using a XXE agnostic configured parser...
            DocumentBuilder safebuilder = dbfS.newDocumentBuilder();
            Document doc = safebuilder.parse(new InputSource(new StringReader(inputXml))); // -> Safe. Previous version of this method flagged this line as vulnerable
            return xmlToString(doc);
    }

    // Another test cases:
	@RequestMapping(method=RequestMethod.POST, value="/vulnProcess")
	public String vulnProcess(String inputXml) {
		if (inputXml == null) {
			return "Provide an inputXml variable";
		}

		try {
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = dbf.newDocumentBuilder();
			Document doc = builder.parse(new InputSource(new StringReader(inputXml))); // -> Vulnerable

			return xmlToString(doc);
		} catch (Exception e) {
			e.printStackTrace();
			return e.getMessage();
		}
	}

    @RequestMapping(method=RequestMethod.POST, value="/vulnProcess")
        public String vulnProcess(String inputXml) {
            if (inputXml == null) {
                return "Provide an inputXml variable";
            }

            try {
                DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
                dbf.setXIncludeAware(false);
                DocumentBuilder builder = dbf.newDocumentBuilder();
                Document doc = builder.parse(new InputSource(new StringReader(inputXml))); // -> Vulnerable

                return xmlToString(doc);
            } catch (Exception e) {
                e.printStackTrace();
                return e.getMessage();
            }
        }

    @RequestMapping(method=RequestMethod.POST, value="/safeProcess")
        public String safeProcess(String inputXml) {
            if (inputXml == null) {
                return "Provide an inputXml variable";
            }

            try {
                DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
                dbf.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
                DocumentBuilder builder = dbf.newDocumentBuilder();
                Document doc = builder.parse(new InputSource(new StringReader(inputXml))); // -> Safe

                return xmlToString(doc);
            } catch (Exception e) {
                e.printStackTrace();
                return e.getMessage();
            }
        }

        public static String xmlToString(Document doc) {
            try {
                StringWriter sw = new StringWriter();
                TransformerFactory tf = TransformerFactory.newInstance();

                Transformer transformer = tf.newTransformer();
                transformer.transform(new DOMSource(doc), new StreamResult(sw));

                return sw.toString();
            } catch (Exception ex) {
                throw new RuntimeException("Error converting to String", ex);
            }
        }

    @RequestMapping(method=RequestMethod.POST, value="/alsoSafeProcess")
    public String alsoSafeProcess(String inputXml) {
        if (inputXml == null) {
            return "Provide an inputXml variable";
        }
        try {
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();

            // Configuraciones de seguridad
            dbf.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
            dbf.setFeature("http://xml.org/sax/features/external-general-entities", false);
            dbf.setFeature("http://xml.org/sax/features/external-parameter-entities", false);
            dbf.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);
            dbf.setXIncludeAware(false);
            dbf.setExpandEntityReferences(false);

            DocumentBuilder builder = dbf.newDocumentBuilder();
            Document doc = builder.parse(new InputSource(new StringReader(inputXml))); // -> Safe
            return xmlToString(doc);
        } catch (Exception e) {
            // Mejor manejo de errores: no exponer stacktrace
            logger.error("Error processing XML", e);
            return "Error processing XML";
        }
    }

    @RequestMapping(method=RequestMethod.POST, value="/alsoSafeProcess2")
    public String alsoSafeProcess2(String inputXml) {
        if (inputXml == null) {
            return "Provide an inputXml variable";
        }
        try {
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();

            // Configuraciones de seguridad
            dbf.setFeature( // -> Safe
                "http://apache.org/xml/features/disallow-doctype-decl",
                true
            );

            DocumentBuilder builder = dbf.newDocumentBuilder();
            Document doc = builder.parse(new InputSource(new StringReader(inputXml))); // -> Safe
            return xmlToString(doc);
        } catch (Exception e) {
            // Mejor manejo de errores: no exponer stacktrace
            logger.error("Error processing XML", e);
            return "Error processing XML";
        }
    }

}
