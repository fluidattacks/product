const safeWindowFeatures: string = "noopener,noreferrer";
const unsafeWindowFeatures: string = "noreferrer";

function unsafePopup(url: string): void{
  // unsafe cases
  window.open("https://External.com");
  window.open("https://External.com", "_blank");
  window.open("https://External.com", "_blank", "Any");
  window.open(url, "_blank", unsafeWindowFeatures);
}

// Unsafe cases

function safePopup(url: string, name: string, windowFeatures: any): void {
  // Safe cases
  window.open(url, name, 'noopener,noreferrer,' + windowFeatures);
  window.open("https://External.com", "_blank", "noreferrer,noopener");
  window.open("https://www.external.com/", "_blank", safeWindowFeatures);
}
