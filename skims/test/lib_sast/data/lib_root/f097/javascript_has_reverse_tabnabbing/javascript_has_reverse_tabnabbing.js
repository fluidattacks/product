const safeWindowFeatures = "noopener,noreferrer";
const unsafeWindowFeatures = "noreferrer";

function unsafePopup(url){
  // unsafe cases
  window.open("https://External.com");
  window.open("https://External.com", "_blank");
  window.open("https://External.com", "_blank", "Any");
  window.open(url, "_blank", unsafeWindowFeatures);
}

// Unsafe cases

function safePopup(url, name, windowFeatures){
  // Safe cases
  window.open(url, name, 'noopener,noreferrer,' + windowFeatures);
  window.open("https://External.com", "_blank", "noreferrer,noopener");
  window.open("https://www.external.com/", "_blank", safeWindowFeatures);
}
