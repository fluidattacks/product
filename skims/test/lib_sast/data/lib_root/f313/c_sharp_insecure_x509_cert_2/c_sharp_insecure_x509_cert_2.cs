using System.Security.Cryptography;

class CertSelect
{
    static void Main()
    {
        X509Certificate2 cert = new X509Certificate2();
        var privkey = cert.PrivateKey; // -> Vulnerable

        X509Certificate2 certSafe = new X509Certificate2("certificate.pfx", "password", X509KeyStorageFlags.MachineKeySet);
        RSA privateKeySafe = certSafe.GetRSAPrivateKey(); // Safe
    }
}
