resource "aws_s3_bucket_policy" "vuln_bucket" {
  bucket = aws_s3_bucket.b.id

  policy = <<POLICY
  {
    "Version": "2012-10-17",
    "Id": "MYBUCKETPOLICY",
    "Statement": [
      {
        "Sid": "IPAllow",
        "Effect": "Allow",
        "Principal": "*",
        "Action": ["s3:*"]
      }
    ]
  }
  POLICY
}


resource "aws_s3_bucket_policy" "vuln_bucket_1" {
  bucket = "vuln_bucket_1"

  policy = jsonencode({
    Version = "2012-10-17"
    Id      = "MYBUCKETPOLICY"
    Statement = [
      {
        Sid       = "IPAllow"
        Action    = ["s3:*"]
        Effect    = "Allow"
        Principal = "*"
      },
    ]
  })
}

resource "aws_s3_bucket_policy" "safe_bucket" {
  bucket = "safe_bucket_1"

  policy = jsonencode({
    Version = "2012-10-17"
    Id      = "MYBUCKETPOLICY"
    Statement = [
      {
        Sid       = "IPAllow"
        Action    = ["s3:GetObject"]
        Effect    = "Allow"
        Principal = "*"
      },
    ]
  })
}
