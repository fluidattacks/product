[AllowAnonymous]
public class VulnCase : Controller
{
    [AllowAnonymous]
    public ActionResult About()
    {
        ViewBag.Message = "Your application description page.";

        return View();
    }

    [Authorize]
    public string Method1()
    {
        return "The secure method";
    }
}

[Authorize]
public class SafeCase : Controller
{
    [AllowAnonymous]
    public ActionResult About()
    {
        ViewBag.Message = "Your application description page.";

        return View();
    }

    [Authorize]
    public string Method1()
    {
        return "The secure method";
    }
}
