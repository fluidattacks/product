resource "kubernetes_pod" "rss_site" {
  metadata {
    name = "rss-site"
    labels = {
      app = "web"
    }
  }
  spec {
    container {
      name  = "unsafe_image"
      image = "nginx"
      port {
        container_port = 80
      }
      security_context {
        allow_privilege_escalation = false
        read_only_root_filesystem  = true
        run_as_non_root            = true
        seccomp_profile {
          type = "RuntimeDefault"
        }
      }
    }
    container {
      name  = "safe_image"
      image = "nginx@${var.could_be_a_digest}"
      port {
        container_port = 80
      }
      security_context {
        allow_privilege_escalation = false
        read_only_root_filesystem  = true
        run_as_non_root            = true
        seccomp_profile {
          type = "RuntimeDefault"
        }
      }
    }
    container {
      name  = "safe_image_2"
      image = "${var.image}@sha256:HASH"
      port {
        container_port = 80
      }
      security_context {
        allow_privilege_escalation = false
        read_only_root_filesystem  = true
        run_as_non_root            = true
        seccomp_profile {
          type = "RuntimeDefault"
        }
      }
    }
  }
}

resource "kubernetes_cron_job_v1" "rss_site" {
  metadata {
    name = "rss-site-cron"
    labels = {
      app = "web"
    }
  }
  spec {
    schedule           = "0 * * * *"
    concurrency_policy = "Forbid"
    job_template {
      metadata {
        labels = {
          app = "web"
        }
      }
      spec {
        template {
          metadata {
            labels = {
              app = "web"
            }
          }
          spec {
            host_ipc     = false
            host_network = false
            host_pid     = true
            security_context {
              run_as_non_root = false
              seccomp_profile {
                type = "Localhost"
              }
            }

            container {
              name  = "unsafe_image_2"
              image = "nginx@sha256:notavalidshahash098882"
              port {
                container_port = 80
              }
              security_context {
                allow_privilege_escalation = false
                read_only_root_filesystem  = true
                run_as_non_root            = true
                seccomp_profile {
                  type = "RuntimeDefault"
                }
              }
            }

            container {
              name  = "safe_image_3"
              image = "nginx@sha256:043a718774c572bd8a25adbeb1bfcd5c0256ae11cecf9f9c3f925d0e52beaf89"
              port {
                container_port = 80
              }
              security_context {
                allow_privilege_escalation = false
                read_only_root_filesystem  = true
                run_as_non_root            = true
                seccomp_profile {
                  type = "RuntimeDefault"
                }
              }
            }
            restart_policy = "OnFailure"
          }
        }
      }
    }
  }
}
