import path = require('path')
import { type Request, type Response, type NextFunction } from 'express'


const whitelist = [
  'file1.txt',
  'file2.json',
  'config.json'
];

module.exports = function serveKeyFiles () {
  return ({ params }: Request, res: Response, next: NextFunction) => {
    const file = params.file

    if (whitelist.includes(file)) {
      res.sendFile(path.resolve('encryptionkeys/', file))
    } else {
      res.status(403)
      next(new Error('File names cannot contain forward slashes!'))
    }
  }
}
