resource "aws_redshift_cluster" "safe" {
  cluster_identifier = "my-redshift-cluster"
  encrypted          = true
  kms_key_id         = "arn:aws:kms:us-west-2:"
}

resource "aws_redshift_cluster" "vuln" {
  cluster_identifier = "my-redshift-cluster"
  encrypted          = false
  kms_key_id         = "alias/aws/redshift"
}

resource "aws_redshift_cluster" "vuln1" {
  cluster_identifier = "my-redshift-cluster"
  kms_key_id         = "alias/aws/redshift"
}
