package servlets;

import java.io.File;
import java.io.FileInputStream;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import com.biz.org.Status;

import org.apache.commons.io.FilenameUtils;

@Path("/")
public class Cls
{
    @GET
    @Path("/images/{image}")
    @Produces("images/*")
    public Response getImage(@PathParam("image") String image) {
        File file = new File("resources/images/", image); // -> Vulnerable

        if (!file.exists()) {
            return Response.status(Status.NOT_FOUND).build();
        }

        return Response.ok().entity(new FileInputStream(file)).build();
    }

    @GET
    @Path("/user/{username}/profile")
    @Produces("application/json")
    public Response getUserProfile(@PathParam("username") String username) {
        File file = new File("users/" + username + "/profile.json"); // -> Vulnerable

        // Checking if file exists
        if (!file.exists()) {
            return Response.status(Status.NOT_FOUND).build();
        }

        return Response.ok().entity(new FileInputStream(file)).build();
    }

    @GET
    @Path("/config/{configFile}")
    @Produces("application/json")
    public Response getConfig(@PathParam("configFile") String configFile) {
        File file = new File("config/" + configFile); // -> Vulnerable

        // Checking if file exists
        if (!file.exists()) {
            return Response.status(Status.NOT_FOUND).build();
        }

        return Response.ok().entity(new FileInputStream(file)).build();
    }

    @GET
    @Path("/images/{image}")
    @Produces("images/*")
    public Response ok(@PathParam("image") String image) {

        File file = new File("resources/images/", FilenameUtils.getName(image)); // -> Safe

        if (!file.exists()) {
            return Response.status(Status.NOT_FOUND).build();
        }

        return Response.ok().entity(new FileInputStream(file)).build();
    }

    @GET
    @Path("/files/{filename}")
    @Produces("application/octet-stream")
    public Response safeGetFile(@PathParam("filename") String filename) {
        String safeFilename = FilenameUtils.getName(filename);
        File file = new File("resources/files/", safeFilename); // -> Safe

        // Validating file existence
        if (!file.exists()) {
            return Response.status(Status.NOT_FOUND).build();
        }

        return Response.ok().entity(new FileInputStream(file)).build();
    }

    @GET
    @Path("/user/{username}/profile")
    @Produces("application/json")
    public Response safeGetUserProfile(@PathParam("username") String username) {
        String safeUsername = FilenameUtils.getName(username);
        File file = new File("users/" + safeUsername + "/profile.json"); // -> Safe

        // Checking if file exists
        if (!file.exists()) {
            return Response.status(Status.NOT_FOUND).build();
        }

        return Response.ok().entity(new FileInputStream(file)).build();
    }

    @GET
    @Path("/config/{configFile}")
    @Produces("application/json")
    public Response safeGetConfig(@PathParam("configFile") String configFile) {
        String safeConfigFile = FilenameUtils.getName(configFile);

        if (!safeConfigFile.matches("^[a-zA-Z0-9_-]+\\.json$")) {
            return Response.status(Status.BAD_REQUEST).build();
        }

        File file = new File("config/" + safeConfigFile); // -> Safe
        if (!file.getCanonicalPath().startsWith(new File("config").getCanonicalPath())) {
            return Response.status(Status.FORBIDDEN).build();
        }

        if (!file.exists()) {
            return Response.status(Status.NOT_FOUND).build();
        }

        return Response.ok().entity(new FileInputStream(file)).build();
    }

    @GET
    @Path("/documents/{docId}")
    @Produces("application/pdf")
    public Response safeGetDocument(@PathParam("docId") String docId) {
        if (!docId.matches("^[0-9]+\\.pdf$")) {
            return Response.status(Status.BAD_REQUEST).build();
        }

        File file = new File("documents/" + docId); // -> Safe
        if (!file.getCanonicalPath().startsWith(new File("documents").getCanonicalPath())) {
            return Response.status(Status.FORBIDDEN).build();
        }

        if (!file.exists()) {
            return Response.status(Status.NOT_FOUND).build();
        }

        return Response.ok().entity(new FileInputStream(file)).build();
    }


}
