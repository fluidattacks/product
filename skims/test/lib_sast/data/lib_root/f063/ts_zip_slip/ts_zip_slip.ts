import { type Request, type Response, type NextFunction } from 'express'
import path from 'path'
import os from 'os'
const unzipper = require('unzipper')


const AdmZip = require('adm-zip');
const fs = require('fs');

const zip = new AdmZip("zip-slip.zip");
const zipEntries = zip.getEntries();
zipEntries.forEach(function (zipEntry) {
  fs.createWriteStream(zipEntry.entryName); // Noncompliant
});

const pathmodule = require('path');
zipEntries.forEach(function (zipEntry) {
  let resolvedPath = pathmodule.join(__dirname + '/archive_tmp', zipEntry.entryName); // join will resolve "../"

  if (resolvedPath.startsWith(__dirname + '/archive_tmp')) {
    // the file cannot be extracted outside of the "archive_tmp" folder
    fs.createWriteStream(resolvedPath); // Compliant
  }
});

function safeFunc () {
  return (req: Request, res: Response, next: NextFunction) => {
    const loggedInUser = req.data
    if (loggedInUser) {
      const imageRequest = req
        .get(req.url)
        .on('response', function (res: Response) {
          const ext = 'jpg'
          const var1 = `order_${req.data.body}.pdf`
          // If we force an extension someway it should not be flagged as a vuln
          imageRequest.pipe(fs.createWriteStream(`uploads/${loggedInUser.data.id}.${ext}`))
          imageRequest.pipe(fs.createWriteStream(path.join('ftp/', var1)))

        })
      }
  }
}

function insecureFunc ({ file }: Request, res: Response, next: NextFunction) {
  const buffer = file.buffer
  const filename = file.originalname.toLowerCase()
  const tempFile = path.join(os.tmpdir(), filename)
  fs.open(tempFile, 'w', function (err, fd) {
    if (err != null) { next(err) }
    fs.write(fd, buffer, 0, buffer.length, null, function (err) {
      if (err != null) { next(err) }
      fs.close(fd, function () {
        fs.createReadStream(tempFile)
          .pipe(unzipper.Parse())
          .on('entry', function (entry: any) {
            const fileName = entry.path
            // Vuln because does not validates the entries
            entry.pipe(fs.createWriteStream('uploads/complaints/' + fileName))
          })
      })
    })
  })

}
