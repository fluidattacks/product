<?php
  /* --- START OF VULNERABLE CASES --- */
  // Vulnerable case #1
  $safe_command = 'ls -l';
  $user_input =  $_REQUEST['input'];

  // Executing command from $_GET
  require_once $user_input;
  move_uploaded_file($_GET['filename'], $_POST['path']);
  mkdir("../upload/{$_FILES['tag']}", 0777, true);

  // Executing command from other user inputs
  move_uploaded_file($user_input, $_REQUEST['path']);

  // Vulnerable case #2
  include $_GET["filename1"];

  // Vulnerable case #3
  include $_GET["filename2"] . ".php";

  // Vulnerable case #4
  $filename3 = $_GET["filename3"];
  include $filename3; // Noncompliant
  /* --- END OF VULNERABLE CASES --- */

  /* --- START OF SAFE CASES  --- */
  // Safe case #1
  require_once "file.php";
  move_uploaded_file($safe_command, "/tmp");
  mkdir($safe_command);
  $file = tempnam("../upload", $_GET['filename4']);
  move_uploaded_file("/tmp", $file );

  // Safe case #2
  $INCLUDE_ALLOW_LIST = [
      "home.php",
      "dashboard.php",
      "profile.php",
      "settings.php"
  ];
  $filename5 = $_GET["filename5"];
  if (in_array($filename5, $INCLUDE_ALLOW_LIST)) {
    include $filename5;
  }

  // Safe case #3
  $filename6 = $_GET["filename6"];
  $sanitized_filename6 = basename($filename6);
  $base_dir = '/path/to/your/includes/';
  $file_path = $base_dir . $sanitized_filename6;
  if (file_exists($file_path) && is_file($file_path)) {
      include $file_path;
  } else {
      echo "Invalid or not found file.";
  }

  // Safe case #4
  function sanitizeAndValidateFilename($filename7) {
    $INCLUDE_ALLOW_LIST = [
        "home.php",
        "dashboard.php",
        "profile.php",
        "settings.php"
    ];
    $filename7 = basename($filename7);
    if (in_array($filename7, $INCLUDE_ALLOW_LIST)) {
        return $filename7;
    } else {
        return false;
    }
  }
  $filename7 = $_GET["filename7"];
  $validFilename = sanitizeAndValidateFilename($filename7);
  if ($validFilename) {
    include $validFilename; // Compliant solution
  } else {
    echo "Invalid or not found file.";
  }

  // Safe case #5
  $contentfile = "content.php";
  include $contentfile; // Compliant solution
  /* --- END OF SAFE CASES  --- */
?>
