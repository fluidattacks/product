resource "azurerm_redis_cache" "insecure" {
  name                 = "example570a-cache-insecure"
  location             = azurerm_resource_group.example570a.location
  resource_group_name  = azurerm_resource_group.example570a.name
  capacity             = 2
  family               = "P"
  sku_name             = "Premium"
  non_ssl_port_enabled = true
  minimum_tls_version  = "1.2"

  redis_configuration {
  }
}

resource "azurerm_redis_cache" "secure" {
  name                 = "example570a-cache-secure"
  location             = azurerm_resource_group.example570a.location
  resource_group_name  = azurerm_resource_group.example570a.name
  capacity             = 2
  family               = "P"
  sku_name             = "Premium"
  non_ssl_port_enabled = false
  minimum_tls_version  = "1.2"

  redis_configuration {
  }
}

resource "azurerm_redis_cache" "secure2" {
  name                = "example570a-cache-secure"
  location            = azurerm_resource_group.example570a.location
  resource_group_name = azurerm_resource_group.example570a.name
  capacity            = 2
  family              = "P"
  sku_name            = "Premium"
  minimum_tls_version = "1.2"

  redis_configuration {
  }
}
