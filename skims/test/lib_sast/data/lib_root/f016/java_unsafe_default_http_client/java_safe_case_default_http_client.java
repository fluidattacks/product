package com.example;

import com.custom.http.DefaultHttpClient;
import com.custom.http.HttpClient;
import com.custom.http.HttpRequest;
import com.custom.http.HttpResponse;

public class CustomHttpClientExample {
    public static void main(String[] args) {
        HttpClient client = new DefaultHttpClient(); // -> Safe. Doesn't come from the standard library
        HttpRequest request = new HttpRequest("https://example.com/api", "GET");

        try {
            HttpResponse response = client.execute(request);
            System.out.println("Response Code: " + response.getStatusCode());
            System.out.println("Response Body: " + response.getBody());
        } catch (Exception e) {
            System.err.println("Error during HTTP request: " + e.getMessage());
        }
    }
}
