// Source: https://semgrep.dev/r?q=problem-based-packs.insecure-transport.java-stdlib.disallow-old-tls-versions2.disallow-old-tls-versions2
public class Bad {
    public void bad1() {
        java.lang.System.setProperty("jdk.tls.client.protocols", "TLSv1.2,TLSv1.3,TLSv1"); // -> Vulnerable
    }
    public void bad2() {
        java.lang.System.setProperty("jdk.tls.client.protocols", "TLSv1.2,TLSv1.3,SSLv3"); // -> Vulnerable
    }
    public void bad1() {
        java.lang.System.setProperty("jdk.tls.client.protocols", "TLSv1.2,TLSv1.3,TLS1"); // -> Vulnerable
    }
}

public class Ok {
    public void ok1() {
        java.lang.System.setProperty("jdk.tls.client.protocols", "TLSv1.2,TLSv1.3"); // -> Safe
    }
    public void ok2() {
        java.lang.System.setProperty("jdk.tls.client.protocols", "TLSv1.3"); // -> Safe
    }
}
