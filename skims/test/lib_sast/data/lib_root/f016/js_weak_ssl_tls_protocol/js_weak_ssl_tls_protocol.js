const https = require('node:https');
const https2 = require('https'); // Same as above
const tls = require('node:tls');
const tls2 = require('tls'); // Same as above
const constants = require('node:crypto');

// Example source: https://rules.sonarsource.com/javascript/RSPEC-4423/
/* *********** VULNERABLE CASES *********** */

let unsafeOptions1 = {
  secureProtocol: 'TLSv1_method' // Noncompliant
};

let unsafeOptions2 = {
  minVersion: 'TLSv1.1',  // Noncompliant
  maxVersion: 'TLSv1.2'
};

let unsafeOptions3 = {
  secureOptions:
    constants.SSL_OP_NO_SSLv2
    | constants.SSL_OP_NO_SSLv3
    | constants.SSL_OP_NO_TLSv1
}; // Noncompliant

let unsaferReq1 = https.request(unsafeOptions1, (res) => { }); // -> Vulnerable
let unsafeSocket1 = tls.connect(443, "www.example.com", unsafeOptions1, () => { }); // -> Vulnerable

let unsaferReq2 = https.request(unsafeOptions2, (res) => { }); // -> Vulnerable
let unsafeSocket2 = tls.connect(443, "www.example.com", unsafeOptions2, () => { }); // -> Vulnerable

let unsaferReq3 = https.request(unsafeOptions3, (res) => { }); // -> Vulnerable
let unsafeSocket3 = tls.connect(443, "www.example.com", unsafeOptions3, () => { }); // -> Vulnerable

let unsaferReq4 = https.request( // -> Vulnerable
  { secureProtocol: 'TLSv1_method' }, // Noncompliant
  (res) => { }
);

let unsafeSocket4 = tls.connect( // -> Vulnerable
  443,
  "www.example.com",
  {
    minVersion: 'TLSv1.1',  // Noncompliant
    maxVersion: 'TLSv1.2'
  },
  () => { }
);


/* *************** SAFE CASES ************** */

let safeOptions1 = {
  secureProtocol: 'TLSv1_2_method'
};

let safeOptions2 = {
  minVersion: 'TLSv1.2',
  maxVersion: 'TLSv1.2'
};

let safeOptions3 = {
  secureOptions:
    constants.SSL_OP_NO_SSLv2
    | constants.SSL_OP_NO_SSLv3
    | constants.SSL_OP_NO_TLSv1
    | constants.SSL_OP_NO_TLSv1_1
};

let safeReq1 = https.request(safeOptions1, (res) => { }); // -> Safe
let safeSocket1 = tls.connect(443, "www.example.com", safeOptions1, () => { }); // -> Safe

let safeReq2 = https.request(safeOptions2, (res) => { }); // -> Safe
let safeSocket2 = tls.connect(443, "www.example.com", safeOptions2, () => { }); // -> Safe

let safeReq3 = https.request(safeOptions3, (res) => { }); // -> Safe
let safeSocket3 = tls.connect(443, "www.example.com", safeOptions3, () => { }); // -> Safe
