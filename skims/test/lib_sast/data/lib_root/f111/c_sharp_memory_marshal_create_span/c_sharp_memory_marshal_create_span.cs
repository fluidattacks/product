namespace MemMarshalCreateSpan {
  public class MemMarshalCreateSpan {
    public void MarshalTest() {
      Span<T> ToSpan() => MemoryMarshal.CreateSpan(ref _e0, 1); // -> Vulnerable

      Span<T> ToSpan() => MemoryMarshal.CreateReadOnlySpan(ref _e0, 2); // -> Vulnerable

      Span<T> ToReadOnlySpanLarge() => MemoryMarshal.CreateReadOnlySpan(  // -> Vulnerable
        ref _e0, int.MaxValue);

      Span<byte> span = MemoryMarshal.CreateSpan( // -> Vulnerable
        ref Unsafe.AsRef(writer.Span.GetPinnableReference()), 4);

      Span<byte> span = MemoryMarshal.CreateReadOnlySpan( // -> Vulnerable
        ref Unsafe.AsRef(writer.Span.GetPinnableReference()), 8);
    }
  }
}
