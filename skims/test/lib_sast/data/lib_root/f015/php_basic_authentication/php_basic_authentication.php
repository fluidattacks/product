<?php

class DangerCases {
  public function firstCase () {

    $header = array(
      "Content-Type: application/json",
      "Authorization: Basic " . base64_encode($this->username.":".$this->password)
    );

    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
    $result = (curl_exec($ch));
    curl_close($ch);
  }

  public function secondCase () {
    $username = 'username';
    $password = 'password';

    $auth_string = $username . ':' . $password;

    $base64_auth_string = base64_encode($auth_string);

    $headers = [
        'Authorization: Basic ' . $base64_auth_string,
        'Content-Type: application/json',
    ];

    $url = 'https://api.example.com/resource';

    $ch = curl_init($url);

    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    $response = curl_exec($ch);
    curl_close($ch);
  }
  public function safeCase () {
    $payload = [
        'user_id' => 123,
    ];

    $jwt_token = get_jwt_token($payload);

    $headers = [
        'Authorization: Bearer ' . $jwt_token,
        'Content-Type: application/json',
    ];

    $url = 'https://api.example.com/resource';

    $ch = curl_init($url);

    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    $response = curl_exec($ch);

    if (curl_errno($ch)) {
        echo 'Error: ' . curl_error($ch);
    }

    curl_close($ch);

  }
}
