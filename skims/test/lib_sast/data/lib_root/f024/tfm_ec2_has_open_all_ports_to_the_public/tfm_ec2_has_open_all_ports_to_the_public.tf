resource "aws_security_group_rule" "vulnerable" {
  security_group_id = "sg-123456"
  type              = "ingress"
  from_port         = 0
  to_port           = 65535
  protocol          = "tcp"
  cidr_blocks       = "0.0.0.0/0"
}


resource "aws_security_group_rule" "not_vulnerable1" {
  security_group_id = "sg-123456"
  type              = "ingress"
  from_port         = 443
  to_port           = 443
  protocol          = "tcp"
  cidr_blocks       = "127.0.0.1/32"
}
