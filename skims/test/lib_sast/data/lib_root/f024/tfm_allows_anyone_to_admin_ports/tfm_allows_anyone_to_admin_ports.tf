resource "aws_security_group" "vulnerable" {
  name        = "allow_tls"
  description = "Allow TLS inbound traffic"
  vpc_id      = "someid"

  ingress {
    from_port        = 445
    to_port          = 445
    protocol         = "tcp"
    ipv6_cidr_blocks = ["::/0"]
  }

  egress {
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = "127.0.0.1/32"
  }
}


resource "aws_security_group_rule" "vulnerable" {
  security_group_id = "sg-123456"
  type              = "ingress"
  from_port         = 445
  to_port           = 445
  protocol          = "tcp"
  cidr_blocks       = "0.0.0.0/0"
}


resource "aws_security_group_rule" "not_vulnerable" {
  security_group_id = "sg-123456"
  type              = "ingress"
  from_port         = 443
  to_port           = 443
  protocol          = "tcp"
  cidr_blocks       = "127.0.0.1/32"
}

resource "aws_security_group" "not_vulnerable" {
  name        = "allow_tls"
  description = "Allow TLS inbound traffic"
  vpc_id      = "someid"

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = "127.0.0.1/32"
  }

  egress {
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = ["127.0.0.1/32"]
  }
}
