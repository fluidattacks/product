# type: ignore
from Crypto.Ciphers import (
    AES,
)


def bad1():
    cipher = AES.new("", AES.MODE_CFB, iv)  # -> Vulnerable
    msg = iv + cipher.encrypt(b"Attack at dawn")


def bad2():
    the_key = ""
    cipher = AES.new(the_key, AES.MODE_CFB, iv)  # -> Vulnerable
    msg = iv + cipher.encrypt(b"Attack at dawn")


def bad3():
    cipher = AES.new("hc_val", AES.MODE_CFB, iv)  # -> Vulnerable
    msg = iv + cipher.encrypt(b"Attack at dawn")


def bad4():
    the_key = "SecretPass"
    cipher = AES.new(the_key, AES.MODE_CFB, iv)  # -> Vulnerable
    msg = iv + cipher.encrypt(b"Attack at dawn")


def ok1(key):
    cipher = AES.new(key, AES.MODE_EAX, nonce=nonce)  # -> Safe
    plaintext = cipher.decrypt(ciphertext)
