using System;
using System.Security.Cryptography;


// Non Compliant
public class InsecureEllipticCurve
{
    public static void Main()
    {
        ECDiffieHellmanCng ecd = new ECDiffieHellmanCng(128);
    }
}

// Compliant
public class SecureEllipticCurve
{
    public static void Main()
    {
        ECDiffieHellmanCng ecd = new ECDiffieHellmanCng(512);
        // Use a secure elliptic curve, such as secp384r1
        ECDiffieHellman ecd = ECDiffieHellman.Create(ECCurve.NamedCurves.nistP384);
    }
}
