import {multerConfig, storageConfig} from "../config.js"
const multer = require('multer')

const unsafeUpload1 = multer({
  storage: multer.diskStorage(storageConfig),
});

const safeUpload = multer({
  storage: multer.memoryStorage(),
  limits: { fileSize: 200000 }
})
const notDeterministicUpload = multer(multerConfig);
