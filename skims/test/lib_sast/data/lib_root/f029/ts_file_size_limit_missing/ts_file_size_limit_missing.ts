import {storageConfig} from "../config.js"
const multer = require('multer')

const unsafeUpload = multer({
  storage: storageConfig,
})

const safeUpload = multer({ storage: multer.memoryStorage(), limits: { fileSize: 200000 } })
