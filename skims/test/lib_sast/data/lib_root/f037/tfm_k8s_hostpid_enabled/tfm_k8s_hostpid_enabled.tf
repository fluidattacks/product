resource "kubernetes_pod" "rss_site" {
  metadata {
    name = "rss-site"
    labels = {
      app = "web"
    }
  }
  spec {
    host_ipc     = false
    host_network = false
    host_pid     = false
    container {
      name  = "safe-host-pid"
      image = "nginx"
      port {
        container_port = 80
      }
      security_context {
        run_as_non_root            = true
        allow_privilege_escalation = false
        read_only_root_filesystem  = true
        capabilities {
          drop = ["all"]
        }
        seccomp_profile {
          type = "Localhost"
        }
      }
    }
  }
}
resource "kubernetes_cron_job_v1" "rss_site" {
  metadata {
    name = "rss-site-cron"
    labels = {
      app = "web"
    }
  }
  spec {
    schedule           = "0 * * * *"
    concurrency_policy = "Forbid"
    job_template {
      metadata {
        labels = {
          app = "web"
        }
      }
      spec {
        template {
          metadata {
            labels = {
              app = "web"
            }
          }
          spec {
            host_ipc     = false
            host_network = false
            host_pid     = true
            security_context {
              run_as_non_root = false
              seccomp_profile {
                type = "Localhost"
              }
            }
            container {
              name  = "unsafe_root"
              image = "nginx"
              port {
                container_port = 80
              }
              security_context {
                run_as_non_root            = false
                allow_privilege_escalation = false
                read_only_root_filesystem  = true
                capabilities {
                  drop = ["all"]
                }
              }
            }
            restart_policy = "OnFailure"
          }
        }
      }
    }
  }
}
