resource "azurerm_postgresql_configuration" "unsafe" {
  resource_group_name = azurerm_resource_group.example542v.name
  server_name         = azurerm_postgresql_server.example542v.name

  name  = "log_retention_days"
  value = 1
}

resource "azurerm_postgresql_configuration" "safe" {
  resource_group_name = azurerm_resource_group.example542v.name
  server_name         = azurerm_postgresql_server.example542v.name

  name  = "log_retention_days"
  value = 4
}
