const crypto = require('crypto');
require('dotenv').config();

const hcKey = "SAFE_PRIVATE_KEY";
const safeKey = process.env.HMAC_SECRET_KEY;

// Following lines must fail
const hmac = (data) => crypto.createHmac('sha256', 'pa4qacea4VK9t9nGv7yZtwmj').update(data).digest('hex');
let hmac2 = crypto.createHmac('sha256', hcKey);

// Safe case:
const safeHmac = (data) => crypto.createHmac('sha256', safeKey).update(data).digest('hex');
let safeHmac2 = crypto.createHmac('sha256', safeKey);
