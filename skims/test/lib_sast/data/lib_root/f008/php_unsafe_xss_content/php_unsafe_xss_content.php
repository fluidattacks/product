<?php

$user_input = $_GET['userInput'];
$not_certain_if_its_user_input = Pictures::get_var($_GET['userInput']);


// Vulnerable code
echo $user_input;
echo $_POST['userInput'];
echo $user_input, $_POST['userInput'];
echo $_GET['userInput'], $user_input;
echo $_GET['userInput'], $_POST['userInput'], $user_input;

// Secure code (sanitizing user input)
$sanitizedUserInput = htmlspecialchars($user_input, ENT_QUOTES, 'UTF-8');
echo $sanitizedUserInput;
echo htmlspecialchars($user_input, ENT_QUOTES, 'UTF-8');
echo htmlspecialchars($_POST['userInput'], ENT_QUOTES, 'UTF-8');
echo htmlspecialchars($user_input, ENT_QUOTES, 'UTF-8'), htmlspecialchars($_POST['userInput'], ENT_QUOTES, 'UTF-8');
echo htmlspecialchars($_GET['userInput'], ENT_QUOTES, 'UTF-8'), htmlspecialchars($user_input, ENT_QUOTES, 'UTF-8');

// Special insecure case (False positive)
echo htmlspecialchars($user_input, ENT_QUOTES, 'UTF-8'), $_POST['userInput'];

?>

<div class="column prepend-1 span-24 first last">
  <h2>Checking your file <?= $user_input ?></h2>
  <p>
    File is O.K. to upload!
  </p>
</div>

<div class="column prepend-1 span-24 first last">
  <h2>Checking your file <?= $sanitizedUserInput ?></h2>
  <p>
    File is O.K. to upload!
  </p>
</div>

<div class="column prepend-1 span-24 first last">
  <h2>Checking your file <?= $not_certain_if_its_user_input ?></h2>
  <p>
    File is O.K. to upload!
  </p>
</div>
