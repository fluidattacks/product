using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace SendMailTest
{
    class Program
    {
        static void Main(string[] args)
        {
            // Vulnerable:
            new SqlConnection(@"Data Source=208.91.198.196;Initial Catalog=BookMe;User Id=upo133;Password=test1234");

            var vulConnectionString = @"Data Source=208.91.198.196;Initial Catalog=BookMe;User Id=upo133;Password=test1234";
            // Vulnerable:
            new SqlConnection(vulConnectionString);

            // Vulnerable:
            SqlConnection con = new SqlConnection( @"Data Source=208.91.198.196;Initial Catalog=BookMe;User Id=upo133;pwd=test1234");

            // Vulnerable:
            new SqlConnection(@"Data Source=208.91.198.196;Initial Catalog=BookMe;User Id=upo133;password=test1234");

            // Safe:
            new SqlConnection(@"Data Source=208.91.198.196;Initial Catalog=BookMe;User Id=upo133;password=");

            // Safe:
            SqlConnection con = new SqlConnection(@"Data Source=208.91.198.196;Initial Catalog=BookMe;User Id=upo133;Password=<password>");

            // Safe:
            new SqlConnection(@"Data Source=208.91.198.196;Initial Catalog=BookMe;Password=;User Id=upo133");

            // Safe:
            new SqlConnection(@"Data Source=208.91.198.196;Initial Catalog=BookMe;User Id=upo133");

            var safeConnectionString = @"Data Source=208.91.198.196;Initial Catalog=BookMe;User Id=upo133";
            // Safe:
            new SqlConnection(safeConnectionString);

        }
    }
}
