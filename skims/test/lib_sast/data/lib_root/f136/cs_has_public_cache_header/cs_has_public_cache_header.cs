
namespace cachecontrol
{
    public class CacheHeaderSet
    {

        public CacheControlHeaderValue VulnCache()
        {
            var cachecontrol = new CacheControlHeaderValue
            {
                MaxAge = TimeSpan.FromSeconds(Duration),
                Public = true
            };
            return cachecontrol;
        }

        public CacheControlHeaderValue VulnCacheII()
        {
            var cachecontrol = new CacheControlHeaderValue
            {
                MaxAge = TimeSpan.FromSeconds(Duration),

            };
            cachecontrol.Public = true;
            return cachecontrol;
        }

        public CacheControlHeaderValue SafeCache()
        {
            var cachecontrol = new CacheControlHeaderValue
            {
                MaxAge = TimeSpan.FromSeconds(Duration),
            };
            return cachecontrol;
        }

        public CacheControlHeaderValue SafeCacheII()
        {
            var cachecontrol = new CacheControlHeaderValue
            {
                MaxAge = TimeSpan.FromSeconds(Duration),
                MustRevalidate = true

            };
            cachecontrol.Public = false;
            return cachecontrol;
        }
        public CacheControlHeaderValue VulnCacheParam(CacheControlHeaderValue cache)
        {
            cache.Public = true;
            return cache;
        }
    }
}
