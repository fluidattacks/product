// Rule source: https://semgrep.dev/r?q=gitlab.find_sec_bugs.HARD_CODE_PASSWORD-1

import java.security.KeyStore;

// Case 1. KeyStore.PasswordProtection Pattern
KeyStore.PasswordProtection vulnerableProtection =
    new KeyStore.PasswordProtection("hardcodedPass123".toCharArray()); // -> Vulnerable

KeyStore.PasswordProtection vulnerableProtection =
    new KeyStore.PasswordProtection("hardcodedPass123"); // -> Vulnerable

String securePassword = SecureConfigLoader.getKeyStorePassword(); // Load from KMS/secure config
KeyStore.PasswordProtection secureProtection =
    new KeyStore.PasswordProtection(securePassword.toCharArray()); // -> Safe

KeyStore.PasswordProtection protection = new KeyStore.PasswordProtection("hardcodedPassword".toCharArray()); // -> Vulnerable

String password = System.getenv("KEYSTORE_PASSWORD");
KeyStore.PasswordProtection protection = new KeyStore.PasswordProtection(password.toCharArray()); // -> Safe


// Case 2. KeyStore.getInstance().load Pattern
KeyStore vulnerableKs = KeyStore.getInstance("PKCS12");
vulnerableKs.load(new FileInputStream("keystore.p12"), "hardcodedPass123".toCharArray()); // -> Vulnerable

KeyStore secureKs = KeyStore.getInstance("PKCS12");
String keystorePassword = SecureConfigLoader.getKeyStorePassword();
secureKs.load(new FileInputStream("keystore.p12"), keystorePassword.toCharArray()); // -> Safe

KeyStore keyStore = KeyStore.getInstance("PKCS12");
keyStore.load(null, "hardcodedPassword".toCharArray()); // -> Vulnerable

KeyStore keyStore = KeyStore.getInstance("PKCS12").load(null, "hardcodedPassword".toCharArray()); // -> Vulnerable

String password = System.getenv("KEYSTORE_PASSWORD");
KeyStore keyStore = KeyStore.getInstance("PKCS12");
keyStore.load(null, password.toCharArray()); // -> Safe

String password = System.getenv("KEYSTORE_PASSWORD");
KeyStore keyStore = KeyStore.getInstance("PKCS12").load(null, password.toCharArray()); // -> Safe


// Case 3. KeyStore variable load Pattern
KeyStore vulnerableStore = KeyStore.getInstance("PKCS12");
vulnerableStore.load(new FileInputStream("keystore.p12"), "hardcodedPass123".toCharArray()); // -> Vulnerable

KeyStore secureStore = KeyStore.getInstance("PKCS12");
String storePassword = SecureConfigLoader.getKeyStorePassword();
secureStore.load(new FileInputStream("keystore.p12"), storePassword.toCharArray()); // -> Safe

KeyStore ks = KeyStore.getInstance("JKS");
ks.load(null, "hardcodedPassword".toCharArray()); // -> Vulnerable

String password = System.getenv("KEYSTORE_PASSWORD");
KeyStore ks = KeyStore.getInstance("JKS");
ks.load(null, password.toCharArray()); // -> Safe
