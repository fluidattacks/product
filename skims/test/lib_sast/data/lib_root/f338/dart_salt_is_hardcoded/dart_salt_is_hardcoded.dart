import 'dart:typed_data';
import 'dart:math';
import 'dart:convert';

const _iterations = 1000;
const _keyLength = 32;
const _saltLength = 50;
const _blockLength = 64;

var _secureRandom = Random.secure();

const String unsafe_salt = 'HARDCODED_SALT';


String unsafeSalts(String password) {

  KeyDerivator keyDerivator = PBKDF2KeyDerivator(HMac(SHA1Digest(), _blockLength));
  Pbkdf2Parameters parameters = Pbkdf2Parameters(unsafe_salt, _iterations, _keyLength);
  keyDerivator.init(parameters);

  return keyDerivator.process(utf8.encode(password));
}

String secureSalts(int length) {
  Uint8List salt = Uint8List(_saltLength);

  for (var i = 0; i < _saltLength; i++) {
    salt[i] = _secureRandom.nextInt(256);
  }

  KeyDerivator keyDerivator = PBKDF2KeyDerivator(HMac(SHA1Digest(), _blockLength));

  keyDerivator.init(Pbkdf2Parameters(salt, _iterations, _keyLength));

  return keyDerivator.process(utf8.encode(password));

}
