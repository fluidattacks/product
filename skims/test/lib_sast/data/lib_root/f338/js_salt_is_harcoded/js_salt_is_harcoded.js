import { createHash } from "crypto";

const cryptoHash = createHash('sha512');

function unsafeHashPassword(password) {
  cryptoHash.update("HARDCODED_SALT");
}

function safeHashPassword1(password) {
  const salt = crypto.randomBytes(16).toString("hex");
  cryptoHash.update(password + salt);
}

function safeHashPassword2() {
  return cryptoHash.update(`009${process.env.var}`);
}

function safeHashPassword3(body) {
  cryptoHash.update(body.document.type.toUpperCase() + "-" + body.document.number);
}

export {unsafeHashPassword, safeHashPassword1, safeHashPassword2, safeHashPassword3}
