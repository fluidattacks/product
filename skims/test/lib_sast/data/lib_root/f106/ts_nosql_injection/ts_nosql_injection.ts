import { type Request, type Response } from 'express'
import MarsDB = require('marsdb')

const reviews = new MarsDB.Collection('posts')
const orders = new MarsDB.Collection('orders')

const db = {
  reviews,
  orders
}

module.exports = function vuln() {
  return (req: Request, res: Response) => {
    const id = req.params.id
    db.orders.find({ $where: `this.orderId === '${id}'` }).then((order: any) => {
      res.json(order)
    }, () => {
      res.status(400).json({ error: 'Wrong Param' })
    })
  }
}

module.exports = function safe() {
  return (req: Request, res: Response) => {
    const id = req.params.id
    db.orders.find({ _id: id }).then((order: any) => {
      res.json(order)
    }, () => {
      res.status(400).json({ error: 'Wrong Param' })
    })
  }
}

module.exports = function vuln1() {
  return (req: Request, res: Response) => {
    const id = req.params.id
    db.orders.update(
      { _id: id },
      { $set: { message: req.body.message } },
      { multi: true }
    ).then((order: any) => {
      res.json(order)
    }, () => {
      res.status(400).json({ error: 'Wrong Param' })
    })
  }
}

module.exports = function vuln2() {
  return (req: Request, res: Response) => {
    const id = req.params.id
    db.orders.update(
      { _id: id },
      { $set: { message: "non-vuln-message" } },
    ).then((order: any) => {
      res.json(order)
    }, () => {
      res.status(400).json({ error: 'Wrong Param' })
    })
  }
}

module.exports = function vuln2() {
  return (req: Request, res: Response) => {
    const id = req.params.id
    db.orders.findOne(
      { _id: id },
    ).then((order: any) => {
      res.json(order)
    }, () => {
      res.status(400).json({ error: 'Wrong Param' })
    })
  }
}

module.exports = function vuln2() {
  return (req: Request, res: Response) => {
    const id = req.params.id
    db.orders.insert(
      { _id: id },
    ).then((order: any) => {
      res.json(order)
    }, () => {
      res.status(400).json({ error: 'Wrong Param' })
    })
  }
}

module.exports = function safe1() {
  return (req: Request, res: Response) => {
    db.reviews.update(
      { _id: "1" },
      { $set: { message: "static message" } },
      { multi: true }
    ).then((order: any) => {
      res.json(order)
    }, () => {
      res.status(400).json({ error: 'Wrong Param' })
    })
  }
}

module.exports = function safe2 () {
  return (req: Request, res: Response) => {
    const id = String(req.params.id).replace(/[^\w-]+/g, '');

    db.orders.find({ $where: `this.orderId === '${id}'` }).then((order: any) => {
      res.json(order)
    }, () => {
      res.status(400).json({ error: 'Wrong Param' })
    })
  }
}
