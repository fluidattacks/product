import { type Request, type Response, type NextFunction } from 'express'
const db = require('../data/mongodb')
const customSanitizer = require('../utils/customSanitizer')
const database = require('../data/mariadb')


module.exports = function unsafeCase () {
  return (req: Request, res: Response, next: NextFunction) => {
    const id = req.params.id
    BasketModel.findOne({ where: { id }, include: [{ model: ProductModel, paranoid: false, as: 'Products' }] })

      const wallet = await WalletModel.findOne({ where: { UserId: req.body.UserId } })

      db.orders.insert({
        promotionalAmount: discountAmount,
        paymentId: req.body.orderDetails ? req.body.orderDetails.paymentId : null,
        addressId: req.body.orderDetails ? req.body.orderDetails.addressId : null,
        eta: deliveryMethod.eta.toString()
      })


  }
}

module.exports = function safeCase () {
  return (req: Request, res: Response, next: NextFunction) => {
    const id = req.params.id
    BasketModel.findOne({ where: { id }, include: [{ model: ProductModel, paranoid: false, as: 'Products' }] })

      const wallet = await WalletModel.findOne({ where: { UserId: req.body.UserId } })

      db.orders.insert({
        promotionalAmount: discountAmount,
        paymentId: req.body.orderDetails ? customSanitizer(req.body.orderDetails.paymentId) : null,
        addressId: req.body.orderDetails ? customSanitizer(req.body.orderDetails.addressId) : null,
        eta: deliveryMethod.eta.toString()
      })


  }
}

module.exports = function safeCaseII () {
  return (req: Request, res: Response, next: NextFunction) => {
    const id = req.params.id
    BasketModel.findOne({ where: { id }, include: [{ model: ProductModel, paranoid: false, as: 'Products' }] })

      const wallet = await WalletModel.findOne({ where: { UserId: req.body.UserId } })

      db.orders.order({
        promotionalAmount: discountAmount,
        paymentId: req.body.orderDetails ? req.body.orderDetails.paymentId : null,
        addressId: req.body.orderDetails ? req.body.orderDetails.addressId : null,
        eta: deliveryMethod.eta.toString()
      })


  }
}

module.exports = function safeCaseIII () {
  return (req: Request, res: Response, next: NextFunction) => {
    const id = req.params.id
    BasketModel.findOne({ where: { id }, include: [{ model: ProductModel, paranoid: false, as: 'Products' }] })

      const wallet = await WalletModel.findOne({ where: { UserId: req.body.UserId } })

      database.orders.insert({
        promotionalAmount: discountAmount,
        paymentId: req.body.orderDetails ? req.body.orderDetails.paymentId : null,
        addressId: req.body.orderDetails ? req.body.orderDetails.addressId : null,
        eta: deliveryMethod.eta.toString()
      })


  }
}
