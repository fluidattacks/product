const express = require('express')
const MarsDB = require('marsdb')

const reviews = new MarsDB.Collection('posts')
const orders = new MarsDB.Collection('orders')

const db = {
  reviews,
  orders
}

module.exports = function trackOrder() {
  return (req, res) => {
    const id = req.params.id
    db.orders.find({ $where: `this.orderId === '${id}'` }).then((order) => {
      res.json(order)
    }, () => {
      res.status(400).json({ error: 'Wrong Param' })
    })
  }
}

module.exports = function safe() {
  return (req, res) => {
    const id = req.params.id
    db.orders.find({ _id: id }).then((order) => {
      res.json(order)
    }, () => {
      res.status(400).json({ error: 'Wrong Param' })
    })
  }
}

module.exports = function vuln1() {
  return (req, res) => {
    const id = req.params.id
    db.orders.update(
      { _id: id },
      { $set: { message: req.body.message } },
      { multi: true }
    ).then((order) => {
      res.json(order)
    }, () => {
      res.status(400).json({ error: 'Wrong Param' })
    })
  }
}

module.exports = function vuln2() {
  return (req, res) => {
    const id = req.params.id
    db.orders.update(
      { _id: id },
      { $set: { message: "non-vuln-message" } },
    ).then((order) => {
      res.json(order)
    }, () => {
      res.status(400).json({ error: 'Wrong Param' })
    })
  }
}

module.exports = function vuln3() {
  return (req, res) => {
    const id = req.params.id
    db.orders.findOne(
      { _id: id },
    ).then((order) => {
      res.json(order)
    }, () => {
      res.status(400).json({ error: 'Wrong Param' })
    })
  }
}

module.exports = function vuln4() {
  return (req, res) => {
    const id = req.params.id
    db.orders.insert(
      { _id: id },
    ).then((order) => {
      res.json(order)
    }, () => {
      res.status(400).json({ error: 'Wrong Param' })
    })
  }
}

module.exports = function safe1() {
  return (req, res) => {
    db.reviews.update(
      { _id: "1" },
      { $set: { message: "static message" } },
      { multi: true }
    ).then((order) => {
      res.json(order)
    }, () => {
      res.status(400).json({ error: 'Wrong Param' })
    })
  }
}

module.exports = function safe2 () {
  return (req, res) => {
    const id = String(req.params.id).replace(/[^\w-]+/g, '');

    db.orders.find({ $where: `this.orderId === '${id}'` }).then((order) => {
      res.json(order)
    }, () => {
      res.status(400).json({ error: 'Wrong Param' })
    })
  }
}
