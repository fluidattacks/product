const main: any = document.getElementById('main');

let express: any = require("express");
let router: any = express.Router();

// Unsafe value

router.get("/test080/:user", function (req, res) {
  let user = "";
  user = `<p> ${req.params["user"]} </p>`;
  main.innerHTML = user;
});

// safe value
main.innerHTML = "<p> Hello New User </p>"

export default router;
