import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from '@angular/router'

@Component({
  selector: "my-app",
  template: `
    <input (keyup)="onKey($event)" />
    <h4>An untrusted URL:</h4>
    <p><a class="e2e-dangerous-url" [href]="resource">Click me</a></p>
    <h4>A trusted URL:</h4>
    <p><a class="e2e-trusted-url" [href]="trustedUrl">Click me</a></p>
  `,
})
export class Aplication {
  constructor(sanitizer: DomSanitizer, private readonly route: ActivatedRoute) {
    resource = "../myownlogo.svg";
    this.trustedUrl = sanitizer.bypassSecurityTrustScript(resource);
    this.trustedHtml = sanitizer.bypassSecurityTrustResourceUrl(resource);
  }

  onKey(event) {
    // Dangerously set values
    let resource = event.target.value;
    this.trustedUrl = sanitizer.bypassSecurityTrustUrl(resource);
    this.trustedHtml = sanitizer.bypassSecurityTrustHtml(resource);
  }

  trustProductDescription (tableData: any[]) {
    for (let i = 0; i < tableData.length; i++) {
      tableData[i].description = this.sanitizer.bypassSecurityTrustHtml(tableData[i].description)
    }
  }

  trustProductDescriptionSafe (tableData: any) {
    for (let i = 0; i < tableData.length; i++) {
      tableData.description = this.sanitizer.bypassSecurityTrustHtml(tableData.image)
    }
  }

  findAllFeedbacks () {
    this.feedbackService.find().subscribe((feedbacks) => {
      this.feedbackDataSource = feedbacks
      for (const feedback of this.feedbackDataSource) {
        feedback.comment = this.sanitizer.bypassSecurityTrustHtml(feedback.comment)
      }
    })
  }

  filterTable() {
    let queryParam: string = this.route.snapshot.queryParams.q
    if (queryParam) {
      this.searchValue = this.sanitizer.bypassSecurityTrustHtml(queryParam)
    }
  }
}
