import { Component } from "@angular/core";
import { ActivatedRoute, Router } from '@angular/router'

@Component({
  selector: "my-app",
  template: `
    <input (keyup)="onKey($event)">
    <h4>An untrusted URL:</h4>
    <p><a class="e2e-dangerous-url" [href]="resource">Click me</a></p>
    <h4>A trusted URL:</h4>
    <p><a class="e2e-trusted-url" [href]="trustedUrl">Click me</a></p>
  `,
})
export class Aplication {
  constructor(sanitizer, route) {
    let resource = "../myownlogo.svg";
    this.trustedUrl = sanitizer.bypassSecurityTrustUrl(resource);
    this.trustedHtml = sanitizer.bypassSecurityTrustHtml(resource);
  }

  onKey(event) {
    // Dangerously set values
    let resource = event.target.value;
    this.trustedUrl = sanitizer.bypassSecurityTrustUrl(resource);
    this.trustedHtml = sanitizer.bypassSecurityTrustHtml(resource);
  }

  trustProductDescription (tableData) {
    for (let i = 0; i < tableData.length; i++) {
      tableData[i].description = this.sanitizer.bypassSecurityTrustHtml(tableData[i].description)
    }
  }

  trustProductDescription (tableData) {
    for (let i = 0; i < tableData.length; i++) {
      tableData[i].description = this.sanitizer.bypassSecurityTrustHtml(tableData[i].image)
    }
  }

  filterTable() {
    let queryParam = this.route.snapshot.queryParams.q
    if (queryParam) {
      this.searchValue = this.sanitizer.bypassSecurityTrustHtml(queryParam)
    }
  }
}
