// Source: https://semgrep.dev/r?q=gitlab.mobsf.java-webview-rule-webview_debugging
import android.webkit.WebView;

public class VulnerableExample1 {
    public void enableDebugging() {
        WebView.setWebContentsDebuggingEnabled(true); // -> Vulnerable: WebView debugging is enabled.
    }
}

public class VulnerableExample2 {
    public void enableDebuggingWithVariable() {
        boolean debug = true;
        WebView.setWebContentsDebuggingEnabled(debug); // -> Vulnerable: Debugging enabled indirectly.
    }
}

public class VulnerableExample3 {
    public void enableDebuggingConditionally() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            WebView.setWebContentsDebuggingEnabled(true); // -> Vulnerable: Debugging enabled for newer devices.
        }
    }
}

public class SecureExample1 {
    public void disableDebugging() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            WebView.setWebContentsDebuggingEnabled(false); // -> Safe: Debugging disabled.
        }
    }
}

public class SecureExample2 {
    public void configureSecureWebView() {
        WebView webView = new WebView(context);

        // Disable remote debugging
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            WebView.setWebContentsDebuggingEnabled(false); // -> Safe
        }

        // Restrict file access
        webView.getSettings().setAllowFileAccessFromFileURLs(false); // -> Safe

        // Load a secure web page
        webView.loadUrl("https://example.com");
    }
}
