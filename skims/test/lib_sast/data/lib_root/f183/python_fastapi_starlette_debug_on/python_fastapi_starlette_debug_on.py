# type: ignore
from fastapi import (
    FastAPI,
)

debug_mode = True

app_vulnerable_1 = FastAPI(debug=True)  # -> Vulnerable
app_vulnerable_2 = FastAPI(debug=debug_mode)  # -> Vulnerable
app_safe_1 = FastAPI()  # -> Safe


@app_vulnerable_1.get("/")
def read_root():
    return {"Hello": "World"}


# ------------------

from starlette.applications import (
    Starlette,
)
from starlette.responses import (
    JSONResponse,
)
from starlette.routing import (
    Route,
)


async def homepage(request):
    return JSONResponse({"hello": "world"})


app_vulnerable_1 = Starlette(
    debug=debug_mode,
    routes=[
        Route("/", homepage),
    ],
)  # -> Vulnerable

app_vulnerable_2 = Starlette(
    debug=True,
    routes=[
        Route("/", homepage),
    ],
)  # -> Vulnerable

app_safe_1 = Starlette(
    debug=False,
    routes=[
        Route("/", homepage),
    ],
)  # -> Safe

app_safe_2 = Starlette(
    routes=[
        Route("/", homepage),
    ]
)  # -> Safe
