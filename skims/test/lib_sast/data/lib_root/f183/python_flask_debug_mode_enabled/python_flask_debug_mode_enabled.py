from flask import (
    Flask,
)

app: Flask = Flask(__name__)

DEBUG_MODE: bool = True


@app.route("/")
def home() -> str:
    return "Hello, Flask!"


if __name__ == "__main__":
    app.run(
        host="127.0.0.1", port=8080, debug=DEBUG_MODE
    )  # Non compliant: Debugging enabled in production
