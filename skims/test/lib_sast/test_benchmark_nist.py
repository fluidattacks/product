# noqa: INP001
import shutil

import pytest

from test.utils import (
    run_skims_group,
)

CONFIG_PATH = "skims/test/lib_sast/test_configs"
RESULTS_PATH = "skims/test/lib_sast/results"
SBOM_INPUT = "skims/test/lib_sast/test_configs/NIST-SARD-SBOM.json"
DESTINATION_FOLDER = "../NIST-SARD-Test-Suites/Juliet_Test_Suite_v1.3_for_C#/"


@pytest.mark.skims_test_group("sast_nist_c_sharp_f001")
def test_sast_nist_c_sharp_f001() -> None:
    shutil.copy(SBOM_INPUT, DESTINATION_FOLDER)
    run_skims_group("nist_c_sharp_f001", CONFIG_PATH, RESULTS_PATH)


@pytest.mark.skims_test_group("sast_nist_c_sharp_f004")
def test_sast_nist_c_sharp_f004() -> None:
    shutil.copy(SBOM_INPUT, DESTINATION_FOLDER)
    run_skims_group("nist_c_sharp_f004", CONFIG_PATH, RESULTS_PATH)


@pytest.mark.skims_test_group("sast_nist_c_sharp_f008")
def test_sast_nist_c_sharp_f008() -> None:
    shutil.copy(SBOM_INPUT, DESTINATION_FOLDER)
    run_skims_group("nist_c_sharp_f008", CONFIG_PATH, RESULTS_PATH)


@pytest.mark.skims_test_group("sast_nist_c_sharp_f021")
def test_sast_nist_c_sharp_f021() -> None:
    shutil.copy(SBOM_INPUT, DESTINATION_FOLDER)
    run_skims_group("nist_c_sharp_f021", CONFIG_PATH, RESULTS_PATH)


@pytest.mark.skims_test_group("sast_nist_c_sharp_f052")
def test_sast_nist_c_sharp_f052() -> None:
    shutil.copy(SBOM_INPUT, DESTINATION_FOLDER)
    run_skims_group("nist_c_sharp_f052", CONFIG_PATH, RESULTS_PATH)


@pytest.mark.skims_test_group("sast_nist_c_sharp_f063")
def test_sast_nist_c_sharp_f063() -> None:
    shutil.copy(SBOM_INPUT, DESTINATION_FOLDER)
    run_skims_group("nist_c_sharp_f063", CONFIG_PATH, RESULTS_PATH)


@pytest.mark.skims_test_group("sast_nist_c_sharp_f107")
def test_sast_nist_c_sharp_f107() -> None:
    shutil.copy(SBOM_INPUT, DESTINATION_FOLDER)
    run_skims_group("nist_c_sharp_f107", CONFIG_PATH, RESULTS_PATH)
