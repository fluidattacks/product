# noqa: INP001
import json
import os
from contextlib import (
    redirect_stderr,
    redirect_stdout,
)
from itertools import (
    zip_longest,
)
from pathlib import Path
from unittest import (
    mock,
)

import pytest
from fluid_sbom.advisories.database import (
    DATABASE,
)
from fluid_sbom.cli import (
    scan as sbom_scan,
)
from fluid_sbom.pkg.operations.package_operation import (
    package_operations_factory,
)
from fluid_sbom.sources.directory_source import (
    Directory,
)
from lib_sast.reachability.utils import (
    create_dependencies_dict,
)


def _load_json(filepath: str) -> dict:
    with Path(filepath).open(encoding="utf-8") as file:
        return json.load(file)


def run_sbom(*args: str) -> int:
    code: int = 0
    with (
        redirect_stdout(Path(os.devnull).open("w", encoding="utf-8")),
        redirect_stderr(Path(os.devnull).open("w", encoding="utf-8")),
    ):
        try:
            sbom_scan(args=list(args))
        except SystemExit as exc:
            if isinstance(exc.code, int):
                code = exc.code
    return code


def modify_existing_results(expected_path_str: str, produced_results_path: str) -> None:
    with Path(produced_results_path).open(encoding="utf-8") as produced:
        expected_path = Path(expected_path_str)
        expected_path.parent.mkdir(parents=True, exist_ok=True)
        with expected_path.open("w", encoding="utf-8") as expected:
            expected.write(produced.read())


def clean_package_data(package: dict) -> dict:
    return {
        "name": package["name"],
        "version": package["version"],
        "locations": sorted(package["locations"], key=lambda x: (x["path"], x["line"])),
        "type": package["type"],
        "language": package["language"],
        "platform": package["platform"],
        "advisories": sorted(
            [
                {
                    "id": adv["id"],
                    "version_constraint": adv["version_constraint"],
                }
                for adv in package["advisories"]
            ],
            key=lambda x: x["id"],
        ),
    }


def assert_packages_equal(expected_path: str, produced_path: str) -> bool:
    expected_sbom = _load_json(expected_path)
    produced_sbom = _load_json(produced_path)

    expected_packages = expected_sbom.get("packages", [])
    produced_packages = produced_sbom.get("packages", [])

    expected_packages_cleaned = [clean_package_data(pkg) for pkg in expected_packages]
    produced_packages_cleaned = [clean_package_data(pkg) for pkg in produced_packages]

    expected_packages_sorted = sorted(
        expected_packages_cleaned,
        key=lambda x: (x.get("name"), x.get("version")),
    )
    produced_packages_sorted = sorted(
        produced_packages_cleaned,
        key=lambda x: (x.get("name"), x.get("version")),
    )

    for produced_item, expected_item in zip_longest(
        produced_packages_sorted,
        expected_packages_sorted,
        fillvalue=None,
    ):
        if produced_item != expected_item:
            modify_existing_results(expected_path, produced_path)

        assert produced_item == expected_item

    return True


def run_reachability_inputs() -> None:
    expected_path = "skims/test/lib_sast/results/reachability_sbom.json"
    produced_path = "skims/test/outputs/sbom_results.json"

    code = run_sbom("skims/test/lib_sast/test_configs/reachability_sbom.yaml", "--config")
    assert code == 0
    assert_packages_equal(expected_path, produced_path)


@pytest.mark.skims_test_group("sast_reachability_inputs")
def test_sast_reachability_inputs() -> None:
    run_reachability_inputs()


def run_reachability_factory() -> None:
    root_directory_test = "skims/test/lib_sast/data/lib_reachability/f002/"
    DATABASE.initialize()
    packages, _ = package_operations_factory(Directory(root=root_directory_test, exclude=()))
    deps_dict, vuln_ranges = create_dependencies_dict(packages)
    vuln_ranges = dict(vuln_ranges)

    assert deps_dict == {
        "items": {
            "c_sharp_cve_2024_21907/project.csproj": {
                "Newtonsoft.Json": {"id": mock.ANY, "version": "13.0.0"},
            },
            "js_cve_2019_10775/package.json": {
                "ecstatic": {"id": mock.ANY, "version": "4.1.2"},
                "express": {"id": mock.ANY, "version": "^4.17.1"},
                "express-jwt": {"id": mock.ANY, "version": "0.1.3"},
            },
            "js_cve_2018_1109/package.json": {
                "braces": {"id": mock.ANY, "version": "2.3.0"},
            },
            "py_cve_2020_28975/requirements.txt": {
                "scikit-learn": {"id": mock.ANY, "version": "0.23.2"},
            },
            "py_cve_2022_24859/requirements.txt": {
                "PyPDF2": {"id": mock.ANY, "version": "1.27.4"},
            },
            "ts_cve_2018_1109/package.json": {
                "braces": {"id": mock.ANY, "version": "2.3.0"},
            },
            "ts_cve_2019_10775/package.json": {
                "ecstatic": {"id": mock.ANY, "version": "4.1.2"},
                "express": {"id": mock.ANY, "version": "^4.17.1"},
                "express-jwt": {"id": mock.ANY, "version": "0.1.3"},
            },
        },
    }, "Dependencies doesn't match for F002 directory"

    assert vuln_ranges == {
        "Newtonsoft.Json": {
            "CVE-2024-21907": "< 13.0.1",
            "GHSA-5crp-9r3c-p9vr": "<13.0.1",
        },
        "scikit-learn": {
            "CVE-2020-28975": ">= 0.23.2, < 1.0.1",
            "CVE-2024-5206": "< 1.5.0",
            "GHSA-jw8x-6495-233v": "<1.5.0",
            "GHSA-jxfp-4rvq-9h9m": ">=0.23.2,<1.0.1",
        },
        "ecstatic": {
            "CVE-2019-10775": "<4.1.3",
            "GHSA-jc84-3g44-wf2q": "<4.1.3",
        },
        "PyPDF2": {
            "CVE-2022-24859": "< 1.27.5",
            "CVE-2023-36810": "<=1.27.8",
            "GHSA-jrm6-h9cq-8gqw": "<=1.27.8",
            "GHSA-xcjx-m2pj-8g79": "<1.27.5",
        },
        "express": mock.ANY,
        "express-jwt": {"CVE-2020-15084": "<= 5.3.3", "GHSA-6g6m-m6h5-w9gf": "<=5.3.3"},
        "braces": {
            "CVE-2018-1109": "< 2.3.1",
            "CVE-2024-4068": "<3.0.3",
            "GHSA-cwfw-4gq5-mrqx": "<2.3.1",
            "GHSA-g95f-p29q-9xw4": "<2.3.1",
            "GHSA-grv7-fg5c-xmjg": "<3.0.3",
        },
    }, "Vulnerability ranges doesn't match for F002 directory"


@pytest.mark.skims_test_group("sast_reachability_inputs")
def test_sast_reachability_pkg_factory() -> None:
    run_reachability_factory()
