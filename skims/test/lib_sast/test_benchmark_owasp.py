# noqa: INP001
import pytest
from lib_sast.owasp_benchmark import (
    load_skims_results,
)

from test.utils import (
    run_skims_group,
)

CONFIG_PATH = "skims/test/lib_sast/test_configs"
RESULTS_PATH = "skims/test/lib_sast/results"


@pytest.mark.skims_test_group("sast_benchmark_cmdi")
def test_sast_benchmark_cmdi() -> None:
    run_skims_group("benchmark_owasp_cmdi", CONFIG_PATH, RESULTS_PATH)


@pytest.mark.skims_test_group("sast_benchmark_crypto")
def test_sast_benchmark_crypto() -> None:
    run_skims_group("benchmark_owasp_crypto", CONFIG_PATH, RESULTS_PATH)


@pytest.mark.skims_test_group("sast_benchmark_hash")
def test_sast_benchmark_hash() -> None:
    run_skims_group("benchmark_owasp_hash", CONFIG_PATH, RESULTS_PATH)


@pytest.mark.skims_test_group("sast_benchmark_ldapi")
def test_sast_benchmark_ldapi() -> None:
    run_skims_group("benchmark_owasp_ldapi", CONFIG_PATH, RESULTS_PATH)


@pytest.mark.skims_test_group("sast_benchmark_pathtraver")
def test_sast_benchmark_pathtraver() -> None:
    run_skims_group("benchmark_owasp_pathtraver", CONFIG_PATH, RESULTS_PATH)


@pytest.mark.skims_test_group("sast_benchmark_securecookie")
def test_sast_benchmark_securecookie() -> None:
    run_skims_group("benchmark_owasp_securecookie", CONFIG_PATH, RESULTS_PATH)


@pytest.mark.skims_test_group("sast_benchmark_sqli")
def test_sast_benchmark_sqli() -> None:
    run_skims_group("benchmark_owasp_sqli", CONFIG_PATH, RESULTS_PATH)


@pytest.mark.skims_test_group("sast_benchmark_trustbound")
def test_sast_benchmark_trustbound() -> None:
    run_skims_group("benchmark_owasp_trustbound", CONFIG_PATH, RESULTS_PATH)


@pytest.mark.skims_test_group("sast_benchmark_weakrand")
def test_sast_benchmark_weakrand() -> None:
    run_skims_group("benchmark_owasp_weakrand", CONFIG_PATH, RESULTS_PATH)


@pytest.mark.skims_test_group("sast_benchmark_xpathi")
def test_sast_benchmark_xpathi() -> None:
    run_skims_group("benchmark_owasp_xpathi", CONFIG_PATH, RESULTS_PATH)


@pytest.mark.skims_test_group("sast_benchmark_xss")
def test_sast_benchmark_xss() -> None:
    run_skims_group("benchmark_owasp_xss", CONFIG_PATH, RESULTS_PATH)


@pytest.mark.skims_test_group("sast_benchmark_score")
def test_sast_benchmark_score() -> None:
    score = load_skims_results()
    err_message = "This change alters benchmark results, this is not allowed"
    assert score.false_positives == 0, err_message
    assert score.score > 0.99, err_message
