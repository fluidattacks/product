# noqa: INP001
import json
import os
from pathlib import Path
from typing import (
    Any,
)

import pytest
from lib_sast import (
    sast_model,
)
from lib_sast.sast_model import (
    decide_language,
)
from lib_sast.syntax_graph.parse_graph.parse import (
    _get_content,
    parse_one,
)
from lib_sast.utils.graph.styles import (
    GRAPH_STYLE_ATTRS,
)


def export_graph_as_json(graph: sast_model.Graph) -> dict:
    data: dict = {}
    data["nodes"] = {}
    data["edges"] = {}
    ignored_attrs = GRAPH_STYLE_ATTRS

    for n_id, n_attrs in graph.nodes.items():
        data["nodes"][n_id] = n_attrs.copy()
        for attr in ignored_attrs:
            data["nodes"][n_id].pop(attr, None)

    for n_id_from, n_id_to in graph.edges:
        data["edges"].setdefault(n_id_from, {})
        data["edges"][n_id_from][n_id_to] = graph[n_id_from][n_id_to].copy()
        for attr in ignored_attrs:
            data["edges"][n_id_from][n_id_to].pop(attr, None)

    return data


def simplify(obj: Any) -> Any:  # noqa: ANN401
    simplified_obj: Any
    if hasattr(obj, "_fields"):
        # NamedTuple
        simplified_obj = dict(zip(simplify(obj._fields), simplify(tuple(obj)), strict=False))
    elif isinstance(obj, dict):
        simplified_obj = dict(
            zip(simplify(tuple(obj.keys())), simplify(tuple(obj.values())), strict=False),
        )
    elif isinstance(obj, (list | tuple | set)):
        simplified_obj = tuple(map(simplify, obj))
    elif isinstance(obj, sast_model.Graph):
        simplified_obj = export_graph_as_json(obj)
    else:
        simplified_obj = obj

    return simplified_obj


def dump_graph_to_json(element: object, *args: Any, **kwargs: Any) -> str:  # noqa: ANN401
    return json.dumps(simplify(element), *args, **kwargs)


@pytest.mark.skims_test_group("sast_graph_generation")
@pytest.mark.parametrize(
    ("files_to_test", "suffix_out"),
    [
        (
            (
                "skims/test/lib_sast/data/graphs/syntax/syntax_cfg.json",
                "skims/test/lib_sast/data/graphs/syntax/syntax_cfg.yaml",
            ),
            "syntax_cfn",
        ),
        (
            ("skims/test/lib_sast/data/graphs/syntax/syntax_cfg.tf",),
            "syntax_hcl",
        ),
        (
            (
                "skims/test/lib_sast/data/graphs/syntax/syntax_cfg.cs",
                "skims/test/lib_sast/data/graphs/syntax/syntax_nist.cs",
            ),
            "syntax_csharp",
        ),
        (
            ("skims/test/lib_sast/data/graphs/syntax/syntax_cfg.dart",),
            "syntax_dart",
        ),
        (
            ("skims/test/lib_sast/data/graphs/syntax/syntax_cfg.go",),
            "syntax_go",
        ),
        (
            (
                "skims/test/lib_sast/data/graphs/syntax/syntax_cfg.java",
                "skims/test/lib_sast/data/graphs/syntax/syntax_owasp.java",
            ),
            "syntax_java",
        ),
        (
            ("skims/test/lib_sast/data/graphs/syntax/syntax_cfg.js",),
            "syntax_javascript",
        ),
        (
            ("skims/test/lib_sast/data/graphs/syntax/syntax_cfg.kt",),
            "syntax_kotlin",
        ),
        (
            ("skims/test/lib_sast/data/graphs/syntax/syntax_cfg.py",),
            "syntax_python",
        ),
        (
            ("skims/test/lib_sast/data/graphs/syntax/syntax_cfg.rb",),
            "syntax_ruby",
        ),
        (
            ("skims/test/lib_sast/data/graphs/syntax/syntax_cfg.scala",),
            "syntax_scala",
        ),
        (
            ("skims/test/lib_sast/data/graphs/syntax/syntax_cfg.swift",),
            "syntax_swift",
        ),
        (
            ("skims/test/lib_sast/data/graphs/syntax/syntax_cfg.ts",),
            "syntax_typescript",
        ),
        (
            ("skims/test/lib_sast/data/graphs/syntax/syntax_cfg.php",),
            "syntax_php",
        ),
    ],
)
def test_sast_graph_generation(
    files_to_test: tuple[str, ...],
    suffix_out: str,
) -> None:
    working_dir = os.getcwd()  # noqa: PTH109
    # Test the GraphDB
    paths = tuple(sorted(files_to_test))

    graph_db: dict[str, dict] = {
        "graphs": {},
    }

    for path in paths:
        if (
            (language := decide_language(path))
            and (content := _get_content(path, working_dir))
            and (parsed_shard := parse_one(path, language, content))
        ):
            graph_db["graphs"].update(
                {
                    parsed_shard.path: {
                        "graph": parsed_shard.graph,
                        "syntax_graph": parsed_shard.syntax_graph,
                    },
                },
            )

    graph_db_as_json_str = dump_graph_to_json(graph_db, indent=2, sort_keys=True)

    expected_path = os.path.join(  # noqa: PTH118
        os.environ["STATE"],
        f"skims/test/lib_sast/data/graphs/json_results/root-graph_{suffix_out}.json",
    )
    os.makedirs(  # noqa: PTH103
        os.path.dirname(expected_path),  # noqa: PTH120
        exist_ok=True,
    )
    with Path(expected_path).open("w", encoding="utf-8") as handle:
        handle.write(graph_db_as_json_str)

    results_path = "skims/test/lib_sast/data/graphs/json_results/root-graph"
    with Path(f"{results_path}_{suffix_out}.json").open(encoding="utf-8") as handle:
        assert graph_db_as_json_str == handle.read()
