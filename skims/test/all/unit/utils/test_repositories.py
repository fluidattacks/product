# noqa: INP001
from pathlib import Path

import pytest
from pytest_mock import (
    MockerFixture,
)
from utils.repositories import (
    DEFAULT_COMMIT,
    get_repo_head_hash,
)


@pytest.mark.skims_test_group("all_unittesting")
def test_get_repo_head_hash(mocker: MockerFixture) -> None:
    base = Path("../universe")
    head = get_repo_head_hash(str(base))
    assert head != DEFAULT_COMMIT
    mocker.patch("utils.repositories.log_blocking")

    for path, commit_hash in (
        # Not a repository
        ("/", DEFAULT_COMMIT),
        # Not exist
        ("/path-not-exists", DEFAULT_COMMIT),
        # Inside a repository, file
        ("skims/test/conftest.py", head),
        # Inside a repository, directory
        ("skims/test", head),
        # Inside a repsitory, not exists
        ("skims/test/path-not-exists", DEFAULT_COMMIT),
    ):
        repo_path = base / path
        assert get_repo_head_hash(str(repo_path)) == commit_hash, path
