# noqa: INP001

import json
import os
from pathlib import Path

import pytest
from lib_sca.advisories_database import DATABASE
from model.core import (
    FindingEnum,
)


@pytest.mark.skims_test_group("all_unittesting")
@pytest.mark.skip(reason="While SCA CVE finding mapping is stable")
def test_update_cve_findings() -> None:
    path = "skims/static/cve_findings/cve_findings.json"
    with Path(path).open(encoding="utf-8") as handle_r:
        cve_findings_current: dict[str, str] = json.load(handle_r)

    DATABASE.initialize()
    connection = DATABASE.get_connection()
    cursor = connection.cursor()
    cursor.execute(
        """
        SELECT adv_id, cve_finding
        FROM advisories
        WHERE cve_finding IS NOT NULL and adv_id NOT LIKE "MAL-%"
    """,
    )
    cve_findings_new = {
        adv_id: assigned_finding
        for adv_id, assigned_finding in cursor.fetchall()
        if adv_id not in cve_findings_current
    }
    connection.close()

    cve_findings_current.update(cve_findings_new)
    expected = json.dumps(cve_findings_current, indent=2, sort_keys=True) + "\n"
    unmapped_findings = set(cve_findings_current.values()) - {
        finding.name for finding in FindingEnum
    }

    expected_path = Path(os.environ["STATE"]) / path
    expected_path.parent.mkdir(parents=True, exist_ok=True)
    with expected_path.open("w", encoding="utf-8") as handle_w:
        handle_w.write(expected)

    with Path(path).open(encoding="utf-8") as handle_r:
        assert handle_r.read() == expected

    assert len(unmapped_findings) == 0, (
        f"Please add the following findings to skims' model {unmapped_findings}"
    )
