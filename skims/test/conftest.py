import contextlib
import json
import os
import subprocess
from collections.abc import (
    Generator,
    Iterator,
)
from pathlib import Path

import pytest
from lib_sca.packages_parsers.cfn_custom_parser import (
    load_as_yaml,
)
from utils.create_context import (
    create_test_context,
)

# Side effects
os.chdir("..")
create_test_context()


def load_test_groups() -> dict[str, list[str]]:
    with Path("skims/test/test_groups.json").open(encoding="utf-8") as file:
        data: dict[str, list[str]] = json.load(file)
        return data


# Constants
TEST_GROUPS_CATEGORIES = load_test_groups()
TEST_GROUPS = set(TEST_GROUPS_CATEGORIES.keys())


def pytest_addoption(parser: pytest.Parser) -> None:
    parser.addoption(
        "--skims-test-group",
        action="store",
        metavar="SKIMS_TEST_GROUP",
    )


def pytest_runtest_setup(item: pytest.Module) -> None:
    # We pass this command line option to tell what group do we want to run
    # This split the big test suite in components
    skims_test_group = item.config.getoption("--skims-test-group")

    # Validate that the dev specified a group that is run on the CI

    if not skims_test_group:
        try:
            marker = next(x for x in item.iter_markers(name="skims_test_group"))
        except StopIteration as exc:
            exc_log = "skims-test-group not specified"
            raise ValueError(exc_log) from exc
        skims_test_group = marker.args[0]
    if skims_test_group != "all" and skims_test_group not in TEST_GROUPS:
        exc_log = f"skims-test-group must be one of: {TEST_GROUPS}, or all"
        raise ValueError(exc_log)

    # The test expects to run on one of these groups
    # If the dev forgets to specify it while writing the test it'll run
    runnable_groups = {mark.args[0] for mark in item.iter_markers(name="skims_test_group")}

    # Validate that the dev specified a group that is run on the CI
    if not runnable_groups or runnable_groups - TEST_GROUPS:
        exc_log = f"skims-test-group must be one of: {TEST_GROUPS}"
        raise ValueError(exc_log)

    # Small logic to skip tests that should not run
    if skims_test_group == "all" or skims_test_group in runnable_groups:
        # Test should execute
        pass
    else:
        pytest.skip(f"Requires skims test group in: {runnable_groups}")


@pytest.fixture(scope="session")
def convert_yaml_test_data_to_json() -> Generator:
    test_data_path = "skims/test/lib_sast/data"
    for path in Path(test_data_path).glob("lib_root/**/*.yaml"):
        # Take the yaml and dump it as json as is
        with (
            Path(path).open(encoding="utf-8") as source,
            Path(f"{path}.json").open("w", encoding="utf-8") as target,
        ):
            source_data = load_as_yaml(source.read())
            target.write(json.dumps(source_data, indent=2))

    yield

    for path in Path(test_data_path).glob("lib_root/**/*.yaml.json"):
        Path(path).unlink()


def _exec_and_wait_command(cmd: list[str]) -> int:
    exit_code: int = -1
    with subprocess.Popen(cmd) as process:  # noqa: S603
        exit_code = process.wait()
    return exit_code  # noqa: RET504


@contextlib.contextmanager
def _exec_command(cmd: list[str], signal: str = "15") -> Iterator[None]:
    with subprocess.Popen(cmd, start_new_session=True) as sproc:  # noqa: S603
        try:
            yield
        finally:
            _exec_and_wait_command(["common-kill-tree", signal, f"{sproc.pid}"])


def _exec_mock_server(
    cmd: list[str],
    port: str,
    signal: str = "15",
    wait_time: str = "5",
) -> Iterator[None]:
    _exec_and_wait_command(["common-kill-port", port])
    with _exec_command(cmd, signal):
        _exec_and_wait_command(["common-wait", wait_time, f"localhost:{port}"])
        yield


@pytest.fixture(autouse=False, scope="session")
def test_mocks_http() -> Iterator[None]:
    yield from _exec_mock_server(["skims-test-mocks-http", "localhost", "48000"], "48000")


@pytest.fixture(autouse=False, scope="session")
def test_mocks_ssl_safe() -> Iterator[None]:
    yield from _exec_mock_server(["skims-test-mocks-ssl-safe"], "4445")


@pytest.fixture(autouse=False, scope="session")
def test_mocks_ssl_unsafe() -> Iterator[None]:
    yield from _exec_mock_server(["skims-test-mocks-ssl-unsafe"], "4446")


@pytest.fixture(autouse=False, scope="session")
def test_clean_cspm_cache() -> None:
    os.environ["AIOCACHE_DISABLE"] = "1"
