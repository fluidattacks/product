from typing import (
    Any,
)


def mock_data() -> dict[str, list[dict[str, Any]]]:
    return {
        "redis_cache_insecure_port": [
            {
                "id": "cspm",
                "name": "test-cspm",
                "type": "Microsoft.Cache/Redis",
                "tags": {},
                "location": "East US",
                "redis_configuration": {
                    "maxfragmentationmemory_reserved": "30",
                    "maxmemory_reserved": "30",
                    "maxmemory_delta": "30",
                    "maxclients": "256",
                },
                "redis_version": "6.0",
                "enable_non_ssl_port": True,
                "public_network_access": "Enabled",
                "update_channel": "Stable",
                "sku": {"name": "Basic", "family": "C", "capacity": 0},
                "provisioning_state": "Succeeded",
                "port": 6379,
                "ssl_port": 6380,
                "linked_servers": [],
            },
        ],
    }
