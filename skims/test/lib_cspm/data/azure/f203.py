from typing import (
    Any,
)


def mock_data() -> dict[str, list[dict[str, Any]]]:
    return {
        "storage_account_allows_public_blobs": [
            {
                "id": "vulntest1",
                "name": "fluid-cspm",
                "kind": "StorageV2",
                "provisioning_state": "Succeeded",
                "allow_blob_public_access": True,
            },
            {
                "id": "vulntest1",
                "name": "fluid-cspm",
                "kind": "StorageV2",
                "provisioning_state": "Succeeded",
                "allow_blob_public_access": False,
            },
        ],
        "blob_containers_are_public": [
            {
                "id": "vulntest",
                "name": "fluid-cspm-blob",
                "kind": "StorageV2",
                "public_access": "Allow",
            },
            {
                "id": "safetest",
                "name": "fluid-cspm-blob",
                "kind": "StorageV2",
                "public_access": "None",
            },
        ],
    }
