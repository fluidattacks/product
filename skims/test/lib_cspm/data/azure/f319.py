from typing import (
    Any,
)


def mock_data() -> dict[str, list[dict[str, Any]]]:
    return {
        "azure_role_based_access_control_on_key_vault_is_not_enabled": [
            {
                "id": "vulnerable",
                "properties": {"enable_rbac_authorization": False},
            },
            {"id": "safe", "properties": {"enable_rbac_authorization": True}},
            {"id": "safe", "properties": {}},
        ],
        "azure_function_app_with_admin_privileges": [
            {
                "id": "vulnerable1",
                "roles": ["Somerole", "Other role", "Owner"],
            },
            {
                "id": "vulnerable2",
                "roles": ["Somerole", "Other role", "Contributor"],
            },
            {
                "id": "vulnerable3",
                "roles": [
                    "Some role1",
                    "Other role1",
                    "Role Based Access Control Administrator",
                ],
            },
            {
                "id": "vulnerable4",
                "roles": [
                    "Some role2",
                    "Other role2",
                    "User Access Administrator",
                ],
            },
            {
                "id": "vulnerable5",
                "roles": [
                    "Some role3",
                    "Other role3",
                    "Access Review Operator Service Role",
                ],
            },
            {"id": "safe", "roles": ["Some role4", "Other role4"]},
        ],
    }
