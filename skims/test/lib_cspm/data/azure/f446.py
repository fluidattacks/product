from typing import (
    Any,
)

KEY_VAULT__KEY_ACCESS_UNORDERED_PERMISSIONS = [
    "recover",
    "getrotationpolicy",
    "restore",
    "backup",
    "purge",
    "release",
    "import",
    "verify",
    "decrypt",
    "delete",
    "create",
    "wrapkey",
    "list",
    "update",
    "setrotationpolicy",
    "sign",
    "encrypt",
    "unwrapkey",
    "get",
    "rotate",
]

KEY_VAULT__KEY_ACCESS_SOME_PERMISSIONS = [
    "backup",
    "create",
    "encrypt",
    "get",
    "wrapkey",
]


def mock_data() -> dict[str, list[dict[str, Any]]]:
    return {
        "check_ssh_authentication": [
            {
                "id": "test",
                "name": "test",
                "type": "Microsoft.Compute/virtualMachines",
                "location": "eastus",
                "os_profile": {
                    "computer_name": "testing",
                    "linux_configuration": {
                        "disable_password_authentication": False,
                    },
                },
            },
            {
                "id": "testSafe",
                "name": "test",
                "type": "Microsoft.Compute/virtualMachines",
                "location": "eastus",
                "os_profile": {
                    "computer_name": "testing",
                    "linux_configuration": {
                        "disable_password_authentication": True,
                    },
                },
            },
        ],
        "redis_cache_public_network_access_enabled": [
            {
                "id": "test",
                "name": "test-cspm-vulnerable",
                "type": "Microsoft.Cache/Redis",
                "location": "East US",
                "public_network_access": "Enabled",
                "provisioning_state": "Succeeded",
                "host_name": "test-cspm.redis.cache.windows.net",
            },
            {
                "id": "test2",
                "name": "test-cspm-safe",
                "type": "Microsoft.Cache/Redis",
                "location": "East US",
                "public_network_access": "Disabled",
                "provisioning_state": "Succeeded",
                "host_name": "test-cspm.redis.cache.windows.net",
            },
        ],
        "redis_cache_firewall_allows_public_access": [
            {
                "id": "vulnerable 1/1",
                "public_network_access": "Enabled",
                "start_ip": "0.0.0.0",  # noqa: S104
                "end_ip": "255.255.255.255",
            },
            {
                "id": "safe 1/3",
                "public_network_access": "Enabled",
                "start_ip": "156.12.14.1",
                "end_ip": "192.168.0.1",
            },
            {
                "id": "safe 2/3",
                "public_network_access": "Enabled",
                "start_ip": None,
                "end_ip": None,
            },
            {
                "id": "safe 3/3",
                "public_network_access": "Disabled",
                "start_ip": "0.0.0.0",  # noqa: S104
                "end_ip": "255.255.255.255",
            },
        ],
        "azure_app_service_http2_is_disabled": [
            {
                "id": "vulnerable",
                "site_config": {"http20_enabled": False},
            },
            {
                "id": "safe",
                "site_config": {"http20_enabled": True},
            },
        ],
        "azure_app_service_does_not_use_a_managed_identity": [
            {
                "id": "vulnerable",
            },
            {
                "id": "safe",
                "identity": "some managed identity",
            },
        ],
        "azure_function_app_logging_is_disabled": [
            {
                "id": "vulnerable1",
                "detailed_error_logging_enabled": False,
                "request_tracing_enabled": False,
            },
            {
                "id": "vulnerable2",
                "detailed_error_logging_enabled": False,
                "request_tracing_enabled": True,
            },
            {
                "id": "vulnerable3",
                "detailed_error_logging_enabled": True,
                "request_tracing_enabled": False,
            },
            {
                "id": "safe",
                "detailed_error_logging_enabled": True,
                "request_tracing_enabled": True,
            },
        ],
        "azure_keys_expiration_date_is_not_enabled": [
            {
                "id": "vulnerable",
                "attributes": {
                    "enabled": True,
                },
            },
            {
                "id": "safe1",
                "attributes": {"enabled": True, "expires": "1710260210"},
            },
            {
                "id": "safe2",
                "attributes": {"enabled": False, "expires": "1710260210"},
            },
            {
                "id": "safe3",
                "attributes": {
                    "enabled": False,
                },
            },
        ],
        "azure_secret_expiration_date_is_not_enabled": [
            {
                "id": "vulnerable",
                "properties": {
                    "attributes": {
                        "enabled": True,
                    },
                },
            },
            {
                "id": "safe1",
                "properties": {"attributes": {"enabled": True, "expires": "1710260210"}},
            },
            {
                "id": "safe2",
                "properties": {"attributes": {"enabled": False, "expires": "1710260210"}},
            },
            {
                "id": "safe3",
                "properties": {
                    "attributes": {
                        "enabled": False,
                    },
                },
            },
        ],
        "azure_app_service_always_on_is_not_enabled": [
            {"id": "vulnerable", "site_config": {"always_on": False}},
            {"id": "safe", "site_config": {"always_on": True}},
        ],
        "network_out_of_date_owasp_rules": [
            {
                "id": "test",
                "name": "test",
                "type": "//ApplicationGatewayWebApplicationFirewallPolicies",
                "location": "eastus",
                "tags": {},
                "policy_settings": {
                    "state": "Enabled",
                    "mode": "Detection",
                    "request_body_check": True,
                    "request_body_inspect_limit_in_kb": 128,
                    "request_body_enforcement": True,
                    "max_request_body_size_in_kb": 128,
                    "file_upload_enforcement": True,
                    "file_upload_limit_in_mb": 100,
                },
                "custom_rules": [],
                "application_gateways": [
                    {
                        "id": "test",
                    },
                ],
                "provisioning_state": "Succeeded",
                "managed_rules": {
                    "exclusions": [],
                    "managed_rule_sets": [
                        {
                            "rule_set_type": "OWASP",
                            "rule_set_version": "3.1",
                            "rule_group_overrides": [],
                        },
                    ],
                },
            },
        ],
        "container_registry_is_not_using_replication": [
            {
                "id": "test",
                "replications": [],
            },
        ],
        "aks_is_not_using_latest_version": [
            {
                "id": "test",
                "upgrade_profile": {
                    "id": "test",
                    "name": "default",
                    "control_plane_profile": {
                        "kubernetes_version": "1.28.3",
                        "os_type": "Linux",
                        "upgrades": [
                            {
                                "kubernetes_version": "1.29.0",
                                "is_preview": True,
                            },
                            {
                                "kubernetes_version": "1.28.5",
                            },
                        ],
                    },
                },
            },
        ],
        "aks_has_enable_local_accounts": [
            {
                "id": "test",
                "name": "test",
                "type": "Microsoft.ContainerService/ManagedClusters",
                "location": "eastus",
                "provisioning_state": "Succeeded",
                "power_state": {"code": "Running"},
                "max_agent_pools": 100,
                "kubernetes_version": "1.27.9",
                "current_kubernetes_version": "1.27.9",
                "service_principal_profile": {"client_id": "msi"},
                "addon_profiles": {
                    "azureKeyvaultSecretsProvider": {"enabled": False},
                    "azurepolicy": {"enabled": False},
                },
                "oidc_issuer_profile": {"enabled": False},
                "node_resource_group": "MC_asserts_ludsrill_eastus",
                "enable_rbac": True,
                "support_plan": "KubernetesOfficial",
                "auto_upgrade_profile": {
                    "upgrade_channel": "patch",
                    "node_os_upgrade_channel": "NodeImage",
                },
                "disable_local_accounts": False,
            },
        ],
        "aks_has_kubenet_network_plugin": [
            {
                "id": "test",
                "name": "test",
                "type": "Microsoft.ContainerService/ManagedClusters",
                "location": "eastus",
                "sku": {"name": "Base", "tier": "Free"},
                "provisioning_state": "Succeeded",
                "power_state": {"code": "Running"},
                "max_agent_pools": 100,
                "kubernetes_version": "1.27.9",
                "current_kubernetes_version": "1.27.9",
                "network_profile": {
                    "network_plugin": "kubenet",
                    "network_policy": "calico",
                    "pod_cidr": "10.244.0.0/16",
                    "service_cidr": "10.0.0.0/16",
                    "dns_service_ip": "10.0.0.10",
                    "outbound_type": "loadBalancer",
                    "load_balancer_sku": "Standard",
                    "pod_cidrs": ["10.244.0.0/16"],
                    "service_cidrs": ["10.0.0.0/16"],
                    "ip_families": ["IPv4"],
                },
            },
        ],
        "aks_api_server_allows_public_access": [
            {
                "id": "test",
                "name": "test",
                "type": "Microsoft.ContainerService/ManagedClusters",
                "location": "eastus",
                "sku": {"name": "Base", "tier": "Free"},
                "provisioning_state": "Succeeded",
                "power_state": {"code": "Running"},
                "max_agent_pools": 100,
                "kubernetes_version": "1.27.9",
                "current_kubernetes_version": "1.27.9",
                "network_profile": {
                    "network_plugin": "kubenet",
                    "network_policy": "calico",
                    "pod_cidr": "10.244.0.0/16",
                    "service_cidr": "10.0.0.0/16",
                    "dns_service_ip": "10.0.0.10",
                    "outbound_type": "loadBalancer",
                    "load_balancer_sku": "Standard",
                    "pod_cidrs": ["10.244.0.0/16"],
                    "service_cidrs": ["10.0.0.0/16"],
                    "ip_families": ["IPv4"],
                },
            },
        ],
        "storage_not_enabled_infrastructure_encryption": [
            {
                "id": "test",
                "name": "test",
                "type": "Microsoft.Storage/storageAccounts",
                "tags": {},
                "location": "eastus",
                "sku": {"name": "Standard_RAGRS", "tier": "Standard"},
                "kind": "StorageV2",
                "provisioning_state": "Succeeded",
                "primary_location": "eastus",
                "status_of_primary": "available",
                "secondary_location": "westus",
                "status_of_secondary": "available",
                "encryption": {
                    "services": {
                        "blob": {
                            "enabled": True,
                            "last_enabled_time": "2024-03-27T21:19:46.724398Z",
                            "key_type": "Account",
                        },
                        "file": {
                            "enabled": True,
                            "last_enabled_time": "2024-03-27T21:19:46.724398Z",
                            "key_type": "Account",
                        },
                    },
                    "key_source": "Microsoft.Storage",
                    "require_infrastructure_encryption": False,
                },
            },
        ],
        "aks_has_rbac_disabled": [
            {
                "id": "test",
                "name": "test",
                "type": "Microsoft.ContainerService/ManagedClusters",
                "location": "eastus",
                "sku": {"name": "Base", "tier": "Free"},
                "provisioning_state": "Succeeded",
                "enable_rbac": False,
                "support_plan": "KubernetesOfficial",
                "auto_upgrade_profile": {
                    "upgrade_channel": "patch",
                    "node_os_upgrade_channel": "NodeImage",
                },
                "disable_local_accounts": False,
                "resource_uid": "660aff3b1cefe40001eab665",
            },
        ],
        "azure_vm_encryption_at_host_disabled": [
            {
                "id": "test",
                "name": "test",
                "type": "Microsoft.Compute/virtualMachines",
                "location": "eastus",
                "zones": ["1"],
                "hardware_profile": {"vm_size": "Standard_B1ls"},
                "storage_profile": {
                    "image_reference": {
                        "publisher": "canonical",
                        "offer": "0001-com-ubuntu-server-focal",
                        "sku": "20_04-lts-gen2",
                        "version": "latest",
                        "exact_version": "20.04.202403190",
                    },
                    "data_disks": [],
                    "disk_controller_type": "SCSI",
                },
                "additional_capabilities": {"hibernation_enabled": False},
                "security_profile": {
                    "uefi_settings": {
                        "secure_boot_enabled": True,
                        "v_tpm_enabled": True,
                    },
                    "security_type": "TrustedLaunch",
                    "encryption_at_host": False,
                },
                "diagnostics_profile": {"boot_diagnostics": {"enabled": True}},
                "provisioning_state": "Succeeded",
                "vm_id": "0174837e-99b6-45c0-9d19-24251135ebb1",
                "time_created": "2024-04-03T14:26:24.921569Z",
            },
        ],
        "azure_vm_scale_set_encryption_at_host_disabled": [
            {
                "id": "test",
                "name": "test",
                "type": "Microsoft.Compute/virtualMachineScaleSets",
                "location": "eastus",
                "sku": {
                    "name": "Standard_DC1s_v3",
                    "tier": "Standard",
                    "capacity": 3,
                },
                "upgrade_policy": {"mode": "Manual"},
                "automatic_repairs_policy": {
                    "enabled": False,
                    "grace_period": "PT10M",
                },
                "virtual_machine_profile": {
                    "security_profile": {
                        "uefi_settings": {
                            "secure_boot_enabled": True,
                            "v_tpm_enabled": True,
                        },
                        "security_type": "TrustedLaunch",
                    },
                    "diagnostics_profile": {"boot_diagnostics": {"enabled": True}},
                },
            },
        ],
        "azure_vm_scale_set_check_ssh_authentication": [
            {
                "id": "test",
                "name": "test",
                "type": "Microsoft.Compute/virtualMachineScaleSets",
                "location": "eastus",
                "sku": {
                    "name": "Standard_DC1s_v3",
                    "tier": "Standard",
                    "capacity": 3,
                },
                "upgrade_policy": {"mode": "Manual"},
                "automatic_repairs_policy": {
                    "enabled": False,
                    "grace_period": "PT10M",
                },
                "virtual_machine_profile": {
                    "os_profile": {
                        "computer_name_prefix": "ludsrilll",
                        "admin_username": "azureuser",
                        "linux_configuration": {
                            "disable_password_authentication": False,
                            "provision_vm_agent": True,
                            "enable_vm_agent_platform_updates": False,
                        },
                        "secrets": [],
                        "allow_extension_operations": True,
                        "require_guest_provision_signal": True,
                    },
                    "security_profile": {
                        "uefi_settings": {
                            "secure_boot_enabled": True,
                            "v_tpm_enabled": True,
                        },
                        "security_type": "TrustedLaunch",
                    },
                    "diagnostics_profile": {"boot_diagnostics": {"enabled": True}},
                    "priority": "Spot",
                    "eviction_policy": "Delete",
                    "billing_profile": {"max_price": -1.0},
                },
            },
        ],
        "azure_batch_jobs_runs_in_admin_mode": [
            {
                "id": "vulnerable",
                "start_task": {"user_identity": {"auto_user": {"elevation_level": "Admin"}}},
            },
            {
                "id": "safe",
                "start_task": {"user_identity": {"auto_user": {"elevation_level": "NonAdmin"}}},
            },
        ],
        "azure_db_postgresql_connection_throttling_disabled": [
            {
                "id": "test",
                "name": "connection_throttling",
                "type": "Microsoft.DBforPostgreSQL/servers/configurations",
                "value": "OFF",
                "description": "",
                "default_value": "on",
                "data_type": "Boolean",
                "allowed_values": "on,off",
                "source": "user-override",
            },
            {
                "id": "test_2",
                "name": "connection_throttling",
                "type": "Microsoft.DBforPostgreSQL/servers/configurations",
                "value": "on",
                "description": "",
                "default_value": "on",
                "data_type": "Boolean",
                "allowed_values": "on,off",
                "source": "system-default",
            },
        ],
        "azure_function_app_use_not_host_keys": [
            {
                "function_id": "vulnerable",
                "function_keys": {},
                "master_key": "somemasterkeyvalue",
            },
            {
                "function_id": "safe",
                "function_keys": {"custom_key": "somecustomkeyvalue"},
                "master_key": "somemasterkeyvalue",
            },
        ],
        "azure_db_postgresql_firewall_allows_public_access": [
            {
                "id": "vulnerable 1/1",
                "public_network_access": "Enabled",
                "start_ip_address": "0.0.0.0",  # noqa: S104
                "end_ip_address": "255.255.255.255",
            },
            {
                "id": "safe 1/3",
                "public_network_access": "Enabled",
                "start_ip_address": "156.12.14.1",
                "end_ip_address": "192.168.0.1",
            },
            {
                "id": "safe 2/3",
                "public_network_access": "Enabled",
                "start_ip_address": None,
                "end_ip_address": None,
            },
            {
                "id": "safe 3/3",
                "public_network_access": "Disabled",
                "start_ip_address": "0.0.0.0",  # noqa: S104
                "end_ip_address": "255.255.255.255",
            },
        ],
        "azure_db_sql_firewall_allows_public_access": [
            {
                "id": "vulnerable 1/1",
                "public_network_access": "Enabled",
                "start_ip_address": "0.0.0.0",  # noqa: S104
                "end_ip_address": "255.255.255.255",
            },
            {
                "id": "safe 1/3",
                "public_network_access": "Enabled",
                "start_ip_address": "156.12.14.1",
                "end_ip_address": "192.168.0.1",
            },
            {
                "id": "safe 2/3",
                "public_network_access": "Enabled",
                "start_ip_address": None,
                "end_ip_address": None,
            },
            {
                "id": "safe 3/3",
                "public_network_access": "Disabled",
                "start_ip_address": "0.0.0.0",  # noqa: S104
                "end_ip_address": "255.255.255.255",
            },
        ],
        "azure_db_sql_extended_audit_disabled": [
            {
                "id": "test",
                "name": "Default",
                "type": "Microsoft.Sql/servers/extendedAuditingSettings",
                "predicate_expression": "",
                "retention_days": 5,
                "audit_actions_and_groups": [
                    "SUCCESSFUL_DATABASE_AUTHENTICATION_GROUP",
                    "FAILED_DATABASE_AUTHENTICATION_GROUP",
                    "BATCH_COMPLETED_GROUP",
                ],
                "is_storage_secondary_key_in_use": False,
                "is_azure_monitor_target_enabled": False,
                "state": "Disabled",
                "storage_endpoint": "https://test.blob.core.windows.net/",
                "storage_account_subscription_id": "ffffff",
            },
        ],
        "azure_db_sql_insecure_audit_retention_period": [
            {
                "id": "test",
                "name": "Default",
                "type": "Microsoft.Sql/servers/extendedAuditingSettings",
                "predicate_expression": "",
                "retention_days": 5,
                "audit_actions_and_groups": [
                    "SUCCESSFUL_DATABASE_AUTHENTICATION_GROUP",
                    "FAILED_DATABASE_AUTHENTICATION_GROUP",
                    "BATCH_COMPLETED_GROUP",
                ],
                "is_storage_secondary_key_in_use": False,
                "is_azure_monitor_target_enabled": False,
                "state": "Disabled",
                "storage_endpoint": "https://test.blob.core.windows.net/",
                "storage_account_subscription_id": "ffffff",
            },
        ],
        "azure_db_mysql_firewall_allows_public_access": [
            {
                "id": "vulnerable 1/1",
                "network": {
                    "public_network_access": "Enabled",
                },
                "start_ip_address": "0.0.0.0",  # noqa: S104
                "end_ip_address": "255.255.255.255",
            },
            {
                "id": "safe 1/3",
                "network": {
                    "public_network_access": "Enabled",
                },
                "start_ip_address": "156.12.14.1",
                "end_ip_address": "192.168.0.1",
            },
            {
                "id": "safe 2/3",
                "network": {
                    "public_network_access": "Enabled",
                },
                "start_ip_address": None,
                "end_ip_address": None,
            },
            {
                "id": "safe 3/3",
                "network": {"public_network_access": "Disabled"},
                "start_ip_address": "0.0.0.0",  # noqa: S104
                "end_ip_address": "255.255.255.255",
            },
        ],
        "azure_db_postgresql_log_checkpoints_disabled": [
            {
                "id": "test_vulnerable",
                "name": "log_checkpoints",
                "type": "Microsoft.DBforPostgreSQL/servers/configurations",
                "value": "on",
                "description": "Logs each checkpoint.",
                "default_value": "on",
                "data_type": "Boolean",
                "allowed_values": "on,off",
                "source": "system-default",
            },
            {
                "id": "test",
                "name": "log_checkpoints",
                "type": "Microsoft.DBforPostgreSQL/servers/configurations",
                "value": "off",
                "description": "Logs each checkpoint.",
                "default_value": "on",
                "data_type": "Boolean",
                "allowed_values": "on,off",
                "source": "system-default",
            },
        ],
        "azure_db_postgresql_log_disconnections_disabled": [
            {
                "id": "test",
                "name": "log_disconnections",
                "type": "Microsoft.DBforPostgreSQL/servers/configurations",
                "value": "off",
                "description": "Logs end of a session, including duration.",
                "default_value": "off",
                "data_type": "Boolean",
                "allowed_values": "on,off",
                "source": "system-default",
            },
            {
                "id": "test",
                "name": "log_disconnections",
                "type": "Microsoft.DBforPostgreSQL/servers/configurations",
                "value": "on",
                "description": "Logs end of a session, including duration.",
                "default_value": "off",
                "data_type": "Boolean",
                "allowed_values": "on,off",
                "source": "system-default",
            },
        ],
        "azure_db_postgresql_log_connections_disabled": [
            {
                "id": "test",
                "name": "log_connections",
                "type": "Microsoft.DBforPostgreSQL/servers/configurations",
                "value": "off",
                "description": "Logs each successful connection.",
                "default_value": "off",
                "data_type": "Boolean",
                "allowed_values": "on,off",
                "source": "system-default",
            },
            {
                "id": "test",
                "name": "log_connections",
                "type": "Microsoft.DBforPostgreSQL/servers/configurations",
                "value": "on",
                "description": "Logs each successful connection.",
                "default_value": "on",
                "data_type": "Boolean",
                "allowed_values": "on,off",
                "source": "system-default",
            },
        ],
        "azure_publicly_exposed_funct_app": [
            {
                "id": "vulnerable1",
                "public_network_access": "Enabled",
                "ip_security_restrictions": [{"action": "Allow"}],
            },
            {
                "id": "vulnerable2",
                "public_network_access": "Enabled",
                "ip_security_restrictions": [],
            },
            {
                "id": "safe1",
                "public_network_access": "Enabled",
                "ip_security_restrictions": [
                    {"action": "Deny"},
                    {"action": "Allow"},
                ],
            },
            {
                "id": "safe2",
                "public_network_access": "Disabled",
                "ip_security_restrictions": [{"action": "Allow"}],
            },
            {
                "id": "safe3",
                "public_network_access": "Disabled",
                "ip_security_restrictions": [{"action": "Deny"}],
            },
            {
                "id": "safe4",
                "public_network_access": "Disabled",
                "ip_security_restrictions": [],
            },
        ],
        "azure_db_postgresql_log_duration_disabled": [
            {
                "id": "test",
                "name": "log_duration",
                "type": "Microsoft.DBforPostgreSQL/servers/configurations",
                "value": "off",
                "description": "Logs the duration of each completed SQL.",
                "default_value": "off",
                "data_type": "Boolean",
                "allowed_values": "on,off",
                "source": "system-default",
            },
            {
                "id": "test",
                "name": "log_duration",
                "type": "Microsoft.DBforPostgreSQL/servers/configurations",
                "value": "on",
                "description": "Logs the duration of each completed SQL.",
                "default_value": "off",
                "data_type": "Boolean",
                "allowed_values": "on,off",
                "source": "system-default",
            },
        ],
        "az_subscription_not_allowed_resource_types_policy": [
            {
                "policy_definition_name": (
                    "Audit virtual machines without disasterrecovery configured"
                ),
            },
        ],
        "container_registry_admin_user_enabled": [
            {
                "id": "test",
                "name": "test",
                "type": "Microsoft.ContainerRegistry/registries",
                "location": "eastus",
                "sku": {"name": "Premium", "tier": "Premium"},
                "login_server": "test.azurecr.io",
                "provisioning_state": "Succeeded",
                "admin_user_enabled": True,
            },
        ],
        "network_application_gateway_waf_is_disabled": [
            {
                "id": "test",
                "name": "test",
                "type": "Microsoft.Network/applicationGateways",
                "location": "eastus",
                "tags": {},
                "zones": ["1", "2", "3"],
                "sku": {"name": "WAF_v2", "tier": "WAF_v2"},
                "operational_state": "Stopped",
                "trusted_root_certificates": [],
                "trusted_client_certificates": [],
                "ssl_certificates": [],
                "probes": [],
                "backend_settings_collection": [],
            },
        ],
        "azure_subscription_has_at_least_two_owners": [
            {
                "rol1": ["Owner", "Administrator"],
                "rol2": ["User"],
                "rol3": ["User", "SQL Administrator"],
            },
        ],
        "azure_search_service_does_not_use_a_managed_identity": [
            {"id": "vulnerable", "identity": "None"},
            {
                "id": "safe",
                "identity": {
                    "some_hash": [
                        "Some Permission",
                        "Other Permission",
                    ],
                },
            },
        ],
        "azure_search_service_has_insufficient_replicas_configured": [
            {"id": "vulnerable1", "replica_count": 1},
            {"id": "vulnerable2", "replica_count": 2},
            {"id": "safe1", "replica_count": 3},
            {"id": "safe2", "replica_count": 12},
        ],
        "azure_app_service_mutual_tls_is_disabled": [
            {"id": "vulnerable", "client_cert_enabled": False},
            {"id": "safe", "client_cert_enabled": True},
        ],
        "azure_api_mgmt_svc_does_not_use_a_managed_identity": [
            {
                "id": "vulnerable",
            },
            {"id": "safe", "identity": {"type": "SystemAssigned"}},
        ],
        "azure_key_vault__admin_permissions_on_keys": [
            {
                "id": "vulnerable",
                "properties": {
                    "enable_rbac_authorization": False,
                    "access_policies": [
                        {"permissions": {"keys": (KEY_VAULT__KEY_ACCESS_UNORDERED_PERMISSIONS)}},
                    ],
                },
            },
            {
                "id": "safe1",
                "properties": {
                    "enable_rbac_authorization": True,
                    "access_policies": [
                        {"permissions": {"keys": (KEY_VAULT__KEY_ACCESS_UNORDERED_PERMISSIONS)}},
                    ],
                },
            },
            {
                "id": "safe2",
                "properties": {
                    "enable_rbac_authorization": False,
                    "access_policies": [
                        {"permissions": {"keys": KEY_VAULT__KEY_ACCESS_SOME_PERMISSIONS}},
                    ],
                },
            },
        ],
        "azure_search_service__public_network_access_is_enabled": [
            {
                "id": "vulnerable",
                "public_network_access": "Enabled",
                "network_rule_set": {"ip_rules": []},
            },
            {
                "id": "safe1",
                "public_network_access": "Enabled",
                "network_rule_set": {"ip_rules": [[{"value": "201.01.001.01"}]]},
            },
            {
                "id": "safe2",
                "public_network_access": "Disabled",
                "network_rule_set": {"ip_rules": []},
            },
        ],
        "azure_cosmos_db__public_network_access_is_enabled": [
            {
                "id": "vulnerable1 - Public all",
                "public_network_access": "Enabled",
                "ip_rules": [],
                "is_virtual_network_filter_enabled": False,
            },
            {
                "id": "vulnerable2 - Public and restricted but allows all IPs",
                "public_network_access": "Enabled",
                "ip_rules": [
                    {"ip_address_or_range": "0.0.0.0"},  # noqa: S104
                    {"ip_address_or_range": "201.49.130.76"},
                ],
                "is_virtual_network_filter_enabled": False,
            },
            {
                "id": "safe1 - Public but restricted by IP",
                "public_network_access": "Enabled",
                "ip_rules": [{"ip_address_or_range": "201.49.130.76"}],
                "is_virtual_network_filter_enabled": False,
            },
            {
                "id": "safe2 - Private",
                "public_network_access": "Disabled",
                "ip_rules": [],
                "is_virtual_network_filter_enabled": False,
            },
            {
                "id": "vulnerable2 - Public but restricted due to no IP rules",
                "public_network_access": "Enabled",
                "ip_rules": [],
                "is_virtual_network_filter_enabled": True,
            },
        ],
        "azure_datafactory__public_network_access_is_enabled": [
            {
                "id": "vulnerable",
                "public_network_access": "Enabled",
            },
            {
                "id": "safe",
                "public_network_access": "Disabled",
            },
        ],
        "azure_api_mgmt_svc__public_network_access_is_enabled": [
            {
                "id": "vulnerable",
                "public_network_access": "Enabled",
            },
            {
                "id": "safe",
                "public_network_access": "Disabled",
            },
        ],
        "azure_key_vault__public_network_access_is_enabled": [
            {
                "properties": {
                    "id": "vulnerable",
                    "public_network_access": "Enabled",
                },
            },
            {
                "id": "safe1",
                "properties": {
                    "public_network_access": "Disabled",
                },
            },
            {
                "id": "safe2",
                "properties": {
                    "public_network_access": "Enabled",
                    "network_acls": {
                        "bypass": "AzureServices",
                        "default_action": "Deny",
                        "ip_rules": [],
                        "virtual_network_rules": [],
                    },
                },
            },
        ],
        "azure_api_mgmt_uses_the_triple_des_cipher_algorithm": [
            {
                "id": "vulnerable",
                "custom_properties": {
                    "Microsoft.WindowsAzure.ApiManagement"
                    ".Gateway.Security.Ciphers.TripleDes168": "True",
                },
            },
            {
                "id": "safe",
                "custom_properties": {
                    "Microsoft.WindowsAzure.ApiManagement"
                    ".Gateway.Security.Ciphers.TripleDes168": "False",
                },
            },
        ],
        "az_db_psql_flex_server_connection_throttling_disabled": [
            {
                "id": "vulnerable 1/1",
                "value": "off",
            },
            {
                "id": "safe 1/1",
                "value": "on",
            },
        ],
        "azure_sql_db_transparent_encryption_is_disabled": [
            {
                "id": "vulnerable 1/1",
                "status": "Disabled",
            },
            {
                "id": "safe 1/1",
                "status": "Enabled",
            },
        ],
        "azure_vm_scale_set_does_not_have_zonal_redundancy": [
            {
                "id": "vulnerable 1/1",
                "zones": ["2"],
            },
            {
                "id": "safe 1/1",
                "zones": ["1", "2"],
            },
        ],
    }
