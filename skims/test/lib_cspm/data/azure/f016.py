from typing import (
    Any,
)


def mock_data() -> dict[str, list[dict[str, Any]]]:
    sku_name: str = "Standard_GRS"
    tls_version: str = "TLS1_2"
    default_action: str = "None"
    public_access: str = "None"
    https_only: bool = True
    tls_10__backend_key = (
        "Microsoft.WindowsAzure.ApiManagement.Gateway.Security.Backend.Protocols.Tls10"
    )
    tls_11__backend_key = (
        "Microsoft.WindowsAzure.ApiManagement.Gateway.Security.Backend.Protocols.Tls11"
    )
    tls_10__frontend_key = "Microsoft.WindowsAzure.ApiManagement.Gateway.Security.Protocols.Tls10"
    tls_11__frontend_key = "Microsoft.WindowsAzure.ApiManagement.Gateway.Security.Protocols.Tls11"
    return {
        "storage_account_not_enforce_latest_tls": [
            {
                "sku": {"name": sku_name},
                "id": "safetest",
                "minimum_tls_version": tls_version,
                "network_rule_set": {"default_action": default_action},
                "public_access": public_access,
                "enable_https_traffic_only": https_only,
            },
            {
                "sku": {"name": sku_name},
                "id": "vulntest1",
                "minimum_tls_version": "TLS1_0",
                "network_rule_set": {"default_action": default_action},
                "public_access": public_access,
                "enable_https_traffic_only": https_only,
            },
        ],
        "redis_cache_insecure_tls_version": [
            {
                "id": "test",
                "name": "test-cspm",
                "type": "Microsoft.Cache/Redis",
                "tags": {},
                "location": "East US",
                "redis_configuration": {
                    "maxfragmentationmemory_reserved": "30",
                    "maxmemory_policy": "volatile-lru",
                    "maxmemory_reserved": "30",
                    "maxmemory_delta": "30",
                    "maxclients": "256",
                },
                "redis_version": "6.0",
                "enable_non_ssl_port": False,
                "minimum_tls_version": "1.0",
                "public_network_access": "Enabled",
                "update_channel": "Stable",
                "sku": {"name": "Basic", "family": "C", "capacity": 0},
                "provisioning_state": "Succeeded",
                "host_name": "test-cspm.redis.cache.windows.net",
                "port": 6379,
                "ssl_port": 6380,
                "linked_servers": [],
                "instances": [{"ssl_port": 15000, "is_master": True, "is_primary": True}],
            },
        ],
        "azure_web_app_insecure_tls_version": [
            {"id": "vulnerable", "min_tls_version": "1.1"},
            {"id": "safe", "min_tls_version": "1.2"},
        ],
        "azure_db_for_mysql_flex_servers_insecure_tls_version": [
            {"id": "vulnerable", "name": "tls_version", "value": "TLSv1.1"},
            {"id": "safe", "name": "tls_version", "value": "TLSv1.2"},
        ],
        "azure_db_for_postgresql_insecure_tls_version": [
            {
                "id": "test",
                "name": "test",
                "type": "Microsoft.DBforPostgreSQL/servers",
                "location": "eastus",
                "sku": {
                    "name": "GP_Gen5_2",
                    "tier": "GeneralPurpose",
                    "capacity": 2,
                    "family": "Gen5",
                },
                "version": "11",
                "ssl_enforcement": "Enabled",
                "minimal_tls_version": "TLSEnforcementDisabled",
                "storage_profile": {
                    "backup_retention_days": 7,
                    "geo_redundant_backup": "Disabled",
                    "storage_mb": 5120,
                    "storage_autogrow": "Enabled",
                },
                "replication_role": "",
                "master_server_id": "",
                "public_network_access": "Enabled",
                "private_endpoint_connections": [],
            },
            {
                "id": "test",
                "name": "test",
                "type": "Microsoft.DBforPostgreSQL/servers",
                "location": "eastus",
                "sku": {
                    "name": "GP_Gen5_2",
                    "tier": "GeneralPurpose",
                    "capacity": 2,
                    "family": "Gen5",
                },
                "version": "11",
                "ssl_enforcement": "Enabled",
                "minimal_tls_version": "TLS1_2",
                "storage_profile": {
                    "backup_retention_days": 7,
                    "geo_redundant_backup": "Disabled",
                    "storage_mb": 5120,
                    "storage_autogrow": "Enabled",
                },
                "replication_role": "",
                "master_server_id": "",
                "public_network_access": "Enabled",
                "private_endpoint_connections": [],
            },
        ],
        "azure_db_postgresql_ssl_disabled": [
            {
                "id": "test",
                "name": "test",
                "type": "Microsoft.DBforPostgreSQL/servers",
                "location": "eastus",
                "sku": {
                    "name": "GP_Gen5_2",
                    "tier": "GeneralPurpose",
                    "capacity": 2,
                    "family": "Gen5",
                },
                "version": "11",
                "ssl_enforcement": "Disabled",
                "minimal_tls_version": "TLSEnforcementDisabled",
                "storage_profile": {
                    "backup_retention_days": 7,
                    "geo_redundant_backup": "Disabled",
                    "storage_mb": 5120,
                    "storage_autogrow": "Enabled",
                },
                "replication_role": "",
                "master_server_id": "",
                "public_network_access": "Enabled",
                "private_endpoint_connections": [],
            },
            {
                "id": "test",
                "name": "test",
                "type": "Microsoft.DBforPostgreSQL/servers",
                "location": "eastus",
                "sku": {
                    "name": "GP_Gen5_2",
                    "tier": "GeneralPurpose",
                    "capacity": 2,
                    "family": "Gen5",
                },
                "version": "11",
                "ssl_enforcement": "Enabled",
                "minimal_tls_version": "TLS1_2",
                "storage_profile": {
                    "backup_retention_days": 7,
                    "geo_redundant_backup": "Disabled",
                    "storage_mb": 5120,
                    "storage_autogrow": "Enabled",
                },
                "replication_role": "",
                "master_server_id": "",
                "public_network_access": "Enabled",
                "private_endpoint_connections": [],
            },
        ],
        "azure_db_mysql_ssl_disabled": [
            {
                "id": "test",
                "name": "require_secure_transport",
                "type": "Microsoft.DBforMySQL/flexibleServers/configurations",
                "value": "OFF",
                "default_value": "ON",
                "data_type": "Enumeration",
                "allowed_values": "ON,OFF",
                "source": "system-default",
                "is_read_only": "False",
                "is_config_pending_restart": "False",
                "is_dynamic_config": "True",
            },
            {
                "id": "test",
                "name": "require_secure_transport",
                "type": "Microsoft.DBforMySQL/flexibleServers/configurations",
                "value": "ON",
                "default_value": "ON",
                "data_type": "Enumeration",
                "allowed_values": "ON,OFF",
                "source": "system-default",
                "is_read_only": "False",
                "is_config_pending_restart": "False",
                "is_dynamic_config": "True",
            },
        ],
        "azure_api_mgmt_back__insecure_tls_version": [
            {
                "id": "vulnerable1",
                "custom_properties": {
                    tls_10__backend_key: "True",
                    tls_11__backend_key: "True",
                },
            },
            {
                "id": "vulnerable2",
                "custom_properties": {
                    tls_10__backend_key: "True",
                    tls_11__backend_key: "False",
                },
            },
            {
                "id": "vulnerable3",
                "custom_properties": {
                    tls_10__backend_key: "False",
                    tls_11__backend_key: "True",
                },
            },
            {
                "id": "safe",
                "custom_properties": {
                    tls_10__backend_key: "False",
                    tls_11__backend_key: "False",
                },
            },
        ],
        "azure_api_mgmt_front__insecure_tls_version": [
            {
                "id": "vulnerable1",
                "custom_properties": {
                    tls_10__frontend_key: "True",
                    tls_11__frontend_key: "True",
                },
            },
            {
                "id": "vulnerable2",
                "custom_properties": {
                    tls_10__frontend_key: "True",
                    tls_11__frontend_key: "False",
                },
            },
            {
                "id": "vulnerable3",
                "custom_properties": {
                    tls_10__frontend_key: "False",
                    tls_11__frontend_key: "True",
                },
            },
            {
                "id": "safe",
                "custom_properties": {
                    tls_10__frontend_key: "False",
                    tls_11__frontend_key: "False",
                },
            },
        ],
        "azure_db_psql_flex_server_insecure_tls_version": [
            {
                "id": "vulnerable 1/2",
                "value": "TLSv1.0",
            },
            {
                "id": "vulnerable 2/2",
                "value": "TLSv1.1",
            },
            {"id": "safe 1/1", "value": "TLSv1.2"},
        ],
        "azure_redis_cache_allows_connections_without_ssl": [
            {"id": "vulnerable 1/1", "enable_non_ssl_port": True},
            {"id": "safe 1/1", "enable_non_ssl_port": False},
        ],
        "azure_db_psql_flexible_server__ssl_disabled": [
            {"id": "vulnerable1/1", "value": "off"},
            {
                "id": "safe1/1",
                "ftps_state": "on",
            },
        ],
    }
