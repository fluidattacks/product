import inspect
from collections.abc import (
    Callable,
)
from typing import (
    Any,
)

from test.lib_cspm.data.azure import (
    f016,
    f101,
    f148,
    f157,
    f158,
    f183,
    f203,
    f281,
    f300,
    f319,
    f325,
    f372,
    f392,
    f402,
    f446,
)

MOCKERS = {
    "F016": f016.mock_data(),
    "F101": f101.mock_data(),
    "F148": f148.mock_data(),
    "F157": f157.mock_data(),
    "F158": f158.mock_data(),
    "F183": f183.mock_data(),
    "F203": f203.mock_data(),
    "F281": f281.mock_data(),
    "F300": f300.mock_data(),
    "F319": f319.mock_data(),
    "F325": f325.mock_data(),
    "F372": f372.mock_data(),
    "F392": f392.mock_data(),
    "F402": f402.mock_data(),
    "F446": f446.mock_data(),
}


def get_azure_mock(finding: str) -> dict[str, list[dict[str, Any]]]:
    mock = MOCKERS.get(finding)
    if mock:
        return mock
    return {}


def make_side_effect(
    mock_data: dict[str, list[dict[str, Any]]],
) -> Callable[..., list[dict[str, Any]]]:
    def side_effect(
        *args: Any,  # noqa: ARG001 ANN401
        **kwargs: Any,  # noqa: ARG001 ANN401
    ) -> list[dict[str, Any]]:
        stack = inspect.stack()
        # We need to know the invoker function of run_boto3_fun
        # so we can use it as key to retrieve the specific
        # mock_data. Sometimes the invoker of run_boto3_fun
        # is an util function inside a vulnerability check method
        # thats why we check indexes 2 and 3 of the stack.
        invoker = stack[2].function
        upper_invoker = stack[3].function

        if mock_data.get(invoker):
            return mock_data[invoker]
        if mock_data.get(upper_invoker):
            return mock_data[upper_invoker]
        return mock_data.get("default", [])

    return side_effect


class AzureMockCredentials:
    subscription_id = "000f"
    tenant_id: str
    client_id: str
    client_secret: str

    def __init__(self, tenant_id: str, client_id: str, client_secret: str) -> None:
        self.tenant_id = tenant_id
        self.client_id = client_id
        self.client_secret = client_secret

    async def __aenter__(self) -> "AzureMockCredentials":
        return self

    async def __aexit__(
        self,
        exc_type: type[BaseException] | None,
        exc_value: BaseException | None,
        traceback: object,
    ) -> None:
        # __aexit__ should exist for context manager handling
        pass
