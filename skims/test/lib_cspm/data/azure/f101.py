from typing import (
    Any,
)


def mock_data() -> dict[str, list[dict[str, Any]]]:
    sku_name: str = "Standard_GRS"
    tls_version: str = "TLS1_2"
    default_action: str = "None"
    public_access: str = "None"
    https_only: bool = True
    return {
        "storage_account_geo_replication_disabled": [
            {
                "sku": {"name": sku_name},
                "id": "safetest",
                "minimum_tls_version": tls_version,
                "network_rule_set": {"default_action": default_action},
                "public_access": public_access,
                "enable_https_traffic_only": https_only,
            },
            {
                "sku": {"name": "Standard_LRS"},
                "id": "vulntest1",
                "minimum_tls_version": tls_version,
                "network_rule_set": {"default_action": default_action},
                "public_access": public_access,
                "enable_https_traffic_only": https_only,
            },
        ],
        "blob_service_soft_deleted_disabled": [
            {
                "id": "test",
                "name": "default",
                "delete_retention_policy": {
                    "enabled": True,
                    "days": 3,
                    "allow_permanent_delete": False,
                },
                "container_delete_retention_policy": {
                    "enabled": True,
                    "days": 5,
                },
            },
        ],
        "blob_containers_soft_deleted_disabled": [
            {
                "id": "test",
                "name": "default",
                "delete_retention_policy": {
                    "enabled": True,
                    "days": 3,
                    "allow_permanent_delete": False,
                },
                "container_delete_retention_policy": {
                    "enabled": True,
                    "days": 5,
                },
            },
        ],
        "azure_key_vault_accidental_purge_prevention_is_disabled": [
            {
                "id": "vulnerable",
                "properties": {},
            },
            {"id": "safe", "properties": {"enable_purge_protection": "True"}},
        ],
        "azure_db_postgresql_insecure_log_retention_days": [
            {
                "id": "test",
                "name": "log_retention_days",
                "type": "Microsoft.DBforPostgreSQL/servers/configurations",
                "value": "3",
                "description": "Sets how many days a log file is saved for.",
                "default_value": "3",
                "data_type": "Integer",
                "allowed_values": "1-7",
                "source": "system-default",
            },
            {
                "id": "test2",
                "name": "log_retention_days",
                "type": "Microsoft.DBforPostgreSQL/servers/configurations",
                "value": "7",
                "description": "Sets how many days a log file is saved for.",
                "default_value": "3",
                "data_type": "Integer",
                "allowed_values": "1-7",
                "source": "system-default",
            },
        ],
    }
