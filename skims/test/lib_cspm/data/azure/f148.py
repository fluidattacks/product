from typing import (
    Any,
)


def mock_data() -> dict[str, list[dict[str, Any]]]:
    return {
        "azure_app_service_allows_ftp_deployments": [
            {
                "id": "001",
                "ftps_state": "AllAllowed",
            },
            {
                "id": "002",
                "ftps_state": "FtpsOnly",
            },
            {
                "id": "003",
                "ftps_state": "Disabled",
            },
        ],
    }
