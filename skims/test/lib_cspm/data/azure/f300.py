from typing import (
    Any,
)


def mock_data() -> dict[str, list[dict[str, Any]]]:
    return {
        "redis_cache_authnotrequired_enabled": [
            {
                "id": "test",
                "name": "test-cspm",
                "type": "Microsoft.Cache/Redis",
                "tags": {},
                "location": "East US",
                "redis_configuration": {
                    "maxfragmentationmemory_reserved": "30",
                    "maxmemory_policy": "volatile-lru",
                    "maxmemory_reserved": "30",
                    "maxmemory_delta": "30",
                    "maxclients": "256",
                    "authnotrequired": "Enable",
                },
                "redis_version": "6.0",
                "enable_non_ssl_port": False,
                "minimum_tls_version": "1.0",
                "public_network_access": "Enabled",
                "update_channel": "Stable",
                "sku": {"name": "Basic", "family": "C", "capacity": 0},
                "provisioning_state": "Succeeded",
                "host_name": "test-cspm.redis.cache.windows.net",
                "port": 6379,
                "ssl_port": 6380,
                "linked_servers": [],
                "instances": [{"ssl_port": 15000, "is_master": True, "is_primary": True}],
            },
        ],
        "azure_app_service_authentication_is_not_enabled": [
            {"id": "safe", "platform": {"enabled": True}},
            {"id": "vulnerable", "platform": {"enabled": False}},
        ],
        "azure_dev_portal_has_auth_methods_inactive": [
            {"value": [{"id": "vulnerable", "signin": {"require": False}}]},
            {"value": [{"id": "safe", "signin": {"require": True}}]},
        ],
    }
