from typing import (
    Any,
)


def mock_data() -> dict[str, list[dict[str, Any]]]:
    return {
        "azure_app_service_remote_debugging_enabled": [
            {
                "id": "001",
                "remote_debugging_enabled": True,
            },
            {
                "id": "002",
                "remote_debugging_enabled": False,
            },
        ],
    }
