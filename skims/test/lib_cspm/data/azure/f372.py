from typing import (
    Any,
)


def mock_data() -> dict[str, list[dict[str, Any]]]:
    sku_name: str = "Standard_GRS"
    tls_version: str = "TLS1_2"
    default_action: str = "None"
    public_access: str = "None"
    https_only: bool = True
    return {
        "storage_account_not_enforce_https": [
            {
                "sku": {"name": sku_name},
                "id": "safetest",
                "minimum_tls_version": tls_version,
                "network_rule_set": {"default_action": default_action},
                "public_access": public_access,
                "enable_https_traffic_only": https_only,
            },
            {
                "sku": {"name": sku_name},
                "id": "vulntest1",
                "minimum_tls_version": "TLS1_2",
                "network_rule_set": {"default_action": default_action},
                "public_access": public_access,
                "enable_https_traffic_only": False,
            },
        ],
        "azure_app_service_allows_http_trafic": [
            {
                "id": "safetest",
                "https_only": True,
            },
            {
                "id": "vulntest1",
                "https_only": False,
            },
        ],
        "azure_api_not_enforce_https": [
            {
                "id": "safe",
                "protocols": ["https"],
            },
            {
                "id": "vulnerable1",
                "protocols": ["http", "https"],
            },
            {
                "id": "vulnerable2",
                "protocols": ["http"],
            },
        ],
    }
