import boto3
from mypy_boto3_s3 import (
    S3Client,
)


def s3_bucket_versioning_disabled(
    s3_service: S3Client,
) -> None:
    bucket_name = "my-vulnerable-bucket-123456"
    s3_service.create_bucket(
        Bucket=bucket_name,
    )


def main() -> None:
    s3_service = boto3.client("s3")
    s3_bucket_versioning_disabled(s3_service)
