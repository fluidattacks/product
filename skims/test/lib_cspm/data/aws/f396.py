from collections.abc import (
    Collection,
)


def mock_data() -> dict[str, dict[str, Collection | bool]]:
    return {
        "secrets_manager_has_automatic_rotation_disabled": {
            "SecretList": [
                {
                    "Name": "fluidsecret",
                },
            ],
            "RotationEnabled": False,
            "ARN": "arn:aws:iam::123456789012:secret/mysecret",
        },
        "default": {},
    }
