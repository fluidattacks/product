from collections.abc import (
    Collection,
)
from datetime import (
    datetime,
)


def mock_data() -> dict[str, dict[str, Collection | bool]]:
    date = datetime.fromisoformat("2022-11-01T04:16:13-04:00")
    content = (
        "user,arn,password_enabled,access_key_1_active,access_key_2_active,"
        "access_key_1_last_rotated,access_key_2_last_rotated\n"
        f"fluid,arn:aws:iam::myUser,true,true,true,{date},{date}"
    )
    return {
        "has_old_ssh_public_keys": {
            "Account": "234234234234",
            "Users": [
                {
                    "UserName": "fluidattacks",
                    "Arn": "arn:aws:iam::123456789012:user/fluid",
                },
            ],
            "SSHPublicKeys": [
                {
                    "UserName": "fluidattacks",
                    "SSHPublicKeyId": "ssh:42673",
                    "Status": "Active",
                    "UploadDate": date,
                },
            ],
        },
        "have_old_creds_enabled": {
            "Account": "234234234234",
            "Content": bytes(content, "utf-8"),
            "User": {
                "PasswordLastUsed": date,
            },
        },
        "have_old_access_keys": {
            "Account": "234234234234",
            "Content": bytes(content, "utf-8"),
        },
        "default": {},
    }
