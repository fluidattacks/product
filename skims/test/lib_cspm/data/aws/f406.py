from collections.abc import (
    Collection,
)


def mock_data() -> dict[str, dict[str, Collection | bool]]:
    return {
        "efs_is_encryption_disabled": {
            "FileSystems": [
                {
                    "FileSystemArn": ("arn:aws:iam::123456789012:fs/fluidunsafe"),
                    "LifeCycleState": "available",
                    "PerformanceMode": "generalPurpose",
                    "Encrypted": False,
                },
                {
                    "FileSystemArn": "arn:aws:iam::123456789012:fs/fluidsafe",
                    "LifeCycleState": "available",
                    "PerformanceMode": "generalPurpose",
                    "Encrypted": True,
                },
            ],
        },
    }
