import json

import boto3
import requests
from freezegun import (
    freeze_time,
)
from mypy_boto3_ec2 import (
    EC2Client,
)
from mypy_boto3_ecr import (
    ECRClient,
)
from mypy_boto3_iam import (
    IAMClient,
)
from mypy_boto3_opensearch import (
    OpenSearchServiceClient,
)
from mypy_boto3_s3 import (
    S3Client,
)

PERMISSIVE_POLICY = json.dumps(
    {
        "Version": "2012-10-17",
        "Statement": [
            {
                "Sid": "new statement",
                "Effect": "Allow",
                "Principal": "*",
                "Action": "*",
            },
        ],
    },
)


def aws_s3_log_delivery_write_access(
    s3_service: S3Client,
) -> None:
    bucket_name_target = "bucket-target-log-delivery"
    s3_service.create_bucket(Bucket=bucket_name_target)

    bucket_name_vulnerable = "bucket-vulnerable-log-delivery"
    s3_service.create_bucket(Bucket=bucket_name_vulnerable)

    s3_service.put_bucket_acl(
        Bucket=bucket_name_vulnerable,
        GrantFullControl=("uri=http://acs.amazonaws.com/groups/s3/LogDelivery"),
    )

    bucket_policy = {
        "Version": "2012-10-17",
        "Statement": [
            {
                "Sid": "AllowLogDelivery",
                "Effect": "Allow",
                "Principal": {"Service": "logging.s3.amazonaws.com"},
                "Action": ["s3:PutObject", "s3:PutObjectAcl"],
                "Resource": f"arn:aws:s3:::{bucket_name_target}/*",
            },
        ],
    }

    s3_service.put_bucket_policy(Bucket=bucket_name_target, Policy=json.dumps(bucket_policy))

    s3_service.put_bucket_logging(
        Bucket=bucket_name_vulnerable,
        BucketLoggingStatus={
            "LoggingEnabled": {
                "TargetBucket": bucket_name_target,
                "TargetPrefix": "log/",
            },
        },
    )


def admin_policy_attached(
    iam_service: IAMClient,
) -> None:
    policy_arn = "arn:aws:iam::aws:policy/AdministratorAccess"

    user_name = "vulnerable-user"
    iam_service.create_user(UserName=user_name)

    iam_service.attach_user_policy(UserName=user_name, PolicyArn=policy_arn)


def open_passrole(
    iam_service: IAMClient,
) -> None:
    policy_document = {
        "Version": "2012-10-17",
        "Statement": [{"Effect": "Allow", "Action": "iam:PassRole", "Resource": "*"}],
    }

    policy = iam_service.create_policy(
        PolicyName="open-passrole-policy",
        PolicyDocument=json.dumps(policy_document),
    )

    user_name = "vulnerable-user-passrole"
    iam_service.create_user(UserName=user_name)
    iam_service.attach_user_policy(UserName=user_name, PolicyArn=policy["Policy"]["Arn"])


def negative_statement(
    iam_service: IAMClient,
) -> None:
    policy_document = {
        "Version": "2012-10-17",
        "Statement": [
            {
                "Effect": "Allow",
                "NotAction": "s3:DeleteBucket",
                "NotResource": "*",
            },
        ],
    }

    policy_name = "VulnerableNegativeStatementPolicy"
    response = iam_service.create_policy(
        PolicyName=policy_name,
        PolicyDocument=json.dumps(policy_document),
        Description="Declaración negativa mal configurada.",
    )
    policy_arn = response["Policy"]["Arn"]
    user_name = "vulnerable-user-negative-statement"
    iam_service.create_user(UserName=user_name)
    iam_service.attach_user_policy(UserName=user_name, PolicyArn=policy_arn)


def full_access_to_ssm(
    iam_service: IAMClient,
) -> None:
    policy_document = {
        "Version": "2012-10-17",
        "Statement": [
            {
                "Effect": "Allow",
                "Action": [
                    "ssm:*",
                ],
                "Resource": "*",
            },
        ],
    }

    policy_name = "VulnerableSSMAcess"
    response = iam_service.create_policy(
        PolicyName=policy_name,
        PolicyDocument=json.dumps(policy_document),
    )
    policy_arn = response["Policy"]["Arn"]
    user_name = "vulnerable-user-ssm-access"
    iam_service.create_user(UserName=user_name)
    iam_service.attach_user_policy(UserName=user_name, PolicyArn=policy_arn)


def has_permissive_role_policies(
    iam_service: IAMClient,
) -> None:
    full_access_policy_document = {
        "Version": "2012-10-17",
        "Statement": [{"Effect": "Allow", "Action": "*", "Resource": "*"}],
    }

    role_name = "vulnerable-full-access-role"
    assume_role_policy_document = {
        "Version": "2012-10-17",
        "Statement": [
            {
                "Effect": "Allow",
                "Principal": {"Service": "ec2.amazonaws.com"},
                "Action": "sts:AssumeRole",
            },
        ],
    }

    iam_service.create_role(
        RoleName=role_name,
        AssumeRolePolicyDocument=json.dumps(assume_role_policy_document),
    )

    iam_service.put_role_policy(
        RoleName=role_name,
        PolicyName="full-access-policy",
        PolicyDocument=json.dumps(full_access_policy_document),
    )


def vpc_endpoints_exposed(ec2_service: EC2Client) -> None:
    vpc = ec2_service.create_vpc(CidrBlock="10.0.0.0/16")
    vpc_id = vpc["Vpc"]["VpcId"]

    subnet = ec2_service.create_subnet(CidrBlock="10.0.1.0/24", VpcId=vpc_id)
    subnet_id = subnet["Subnet"]["SubnetId"]

    ec2_service.create_vpc_endpoint(
        VpcId=vpc_id,
        ServiceName="com.amazonaws.us-east-1.s3",
        SubnetIds=[subnet_id],
        VpcEndpointType="Gateway",
        PolicyDocument=json.dumps(
            {
                "Version": "2012-10-17",
                "Statement": [{"Principal": {"AWS": "*"}, "Resource": "*"}],
            },
        ),
    )


def users_with_password_and_access_keys(iam_service: IAMClient) -> None:
    username = "vulnerable-iam-user"

    iam_service.create_user(UserName=username)

    iam_service.create_access_key(UserName=username)

    iam_service.create_login_profile(
        UserName=username,
        Password="SecurePassword123!",  # noqa: S106
        PasswordResetRequired=False,
    )


@freeze_time("2024-10-01 12:00:00")
def aws_ecr_repository_exposed(ecr_service: ECRClient) -> None:
    ecr_service.create_repository(
        repositoryName="vulnerable_repository",
    )
    ecr_service.create_repository(repositoryName="safe_repository")
    ecr_service.set_repository_policy(
        repositoryName="vulnerable_repository",
        policyText=PERMISSIVE_POLICY,
    )


def aws_aws_opensearch_domain_exposed(
    opensearch_service: OpenSearchServiceClient,
) -> None:
    opensearch_service.create_domain(
        DomainName="vulnerable_domain",
        AccessPolicies=PERMISSIVE_POLICY,
    )


def main() -> None:
    requests.post(  # noqa: S113
        "http://motoapi.amazonaws.com/moto-api/seed?a=12345",
    )
    s3_service = boto3.client("s3")
    iam_service = boto3.client("iam")
    ec2_service = boto3.client("ec2")
    ecr_service = boto3.client("ecr")
    opensearch_service = boto3.client("opensearch")
    aws_s3_log_delivery_write_access(s3_service)
    admin_policy_attached(iam_service)
    open_passrole(iam_service)
    negative_statement(iam_service)
    full_access_to_ssm(iam_service)
    has_permissive_role_policies(iam_service)
    vpc_endpoints_exposed(ec2_service)
    users_with_password_and_access_keys(iam_service)
    aws_ecr_repository_exposed(ecr_service=ecr_service)
    aws_aws_opensearch_domain_exposed(opensearch_service)
