import boto3
import requests
from mypy_boto3_apigateway import (
    APIGatewayClient,
)
from mypy_boto3_ec2 import (
    EC2Client,
)
from mypy_boto3_eks import (
    EKSClient,
)


def allows_anyone_to_admin_ports(
    ec2_service: EC2Client,
) -> None:
    """Report vulnerabilities for methods.

    > allows_anyone_to_admin_ports,
    > network_acls_allow_all_ingress_traffic,
    > network_acls_allow_egress_traffic,
    > unrestricted_ip_protocols
    > unrestricted_cidrs
    > unrestricted_dns_access
    > unrestricted_ftp_access
    > open_all_ports_to_the_public
    > insecure_port_range_in_security_group
    """
    vpc_response = ec2_service.create_vpc(
        CidrBlock="10.0.0.0/16",
        TagSpecifications=[
            {
                "ResourceType": "vpc",
                "Tags": [{"Key": "Name", "Value": "my-vpc"}],
            },
        ],
    )
    vpc_id = vpc_response["Vpc"]["VpcId"]

    ec2_service.create_subnet(
        VpcId=vpc_id,
        CidrBlock="10.0.1.0/24",
        AvailabilityZone="us-east-1a",
        TagSpecifications=[
            {
                "ResourceType": "subnet",
                "Tags": [{"Key": "Name", "Value": "my-subnet"}],
            },
        ],
    )
    sg_response = ec2_service.create_security_group(
        GroupName="my-insecure-sg",
        Description="Security Group with open admin ports",
        VpcId=vpc_id,
        TagSpecifications=[
            {
                "ResourceType": "security-group",
                "Tags": [{"Key": "Name", "Value": "my-insecure-sg"}],
            },
        ],
    )
    sg_id = sg_response["GroupId"]

    ec2_service.authorize_security_group_ingress(
        GroupId=sg_id,
        IpPermissions=[
            {
                "IpProtocol": "tcp",
                "FromPort": 22,
                "ToPort": 22,
                "IpRanges": [{"CidrIp": "0.0.0.0/0"}],
            },
            {
                "IpProtocol": "tcp",
                "FromPort": 53,
                "ToPort": 53,
                "IpRanges": [{"CidrIp": "0.0.0.0/0"}],
            },
            {
                "IpProtocol": "tcp",
                "FromPort": 20,
                "ToPort": 21,
                "IpRanges": [{"CidrIp": "0.0.0.0/0"}],
            },
            {
                "IpProtocol": "udp",
                "FromPort": 0,
                "ToPort": 65535,
                "IpRanges": [{"CidrIp": "0.0.0.0/0"}],
            },
        ],
    )


def default_seggroup_allows_all_traffic(
    ec2_service: EC2Client,
) -> None:
    """Report vulnerabilities for methods.

    > default_seggroup_allows_all_traffic
    > insecure_port_range_in_security_group
    """
    group_name = "default"

    response = ec2_service.describe_security_groups(
        Filters=[
            {"Name": "group-name", "Values": [group_name]},
        ],
    )
    sg_id = response["SecurityGroups"][0]["GroupId"]

    ec2_service.authorize_security_group_ingress(
        GroupId=sg_id,
        IpPermissions=[
            {
                "IpProtocol": "tcp",
                "FromPort": 0,
                "ToPort": 1023,
                "IpRanges": [{"CidrIp": "0.0.0.0/0"}],
            },
        ],
    )


def security_groups_ip_ranges_in_rfc1918(
    ec2_service: EC2Client,
) -> None:
    sg_response = ec2_service.create_security_group(
        GroupName="sg-with-rfc1918-ip-ranges",
        Description="Security Group with IP ranges in RFC 1918",
        VpcId="vpc-0123456789abcdef0",
    )
    sg_id = sg_response["GroupId"]

    rfc1918_ranges = ["10.0.0.0/8", "172.16.0.0/12", "192.168.0.0/16"]

    for cidr in rfc1918_ranges:
        ec2_service.authorize_security_group_ingress(
            GroupId=sg_id,
            IpPermissions=[
                {
                    "IpProtocol": "tcp",
                    "FromPort": 80,
                    "ToPort": 80,
                    "IpRanges": [{"CidrIp": cidr}],
                },
            ],
        )


def apigateway_allows_anonymous_access(
    apigateway_service: APIGatewayClient,
) -> None:
    api_response = apigateway_service.create_rest_api(
        name="VulnerableAPI",
        description="API Gateway vulnerable to anonymous access",
        endpointConfiguration={"types": ["REGIONAL"]},
    )
    api_id = api_response["id"]

    resources_response = apigateway_service.get_resources(restApiId=api_id)
    root_id = next(
        (resource["id"] for resource in resources_response["items"] if resource["path"] == "/"),
    )

    resource_response = apigateway_service.create_resource(
        restApiId=api_id,
        parentId=root_id,
        pathPart="vulnerable",
    )
    resource_id = resource_response["id"]

    apigateway_service.put_method(
        restApiId=api_id,
        resourceId=resource_id,
        httpMethod="GET",
        authorizationType="NONE",
    )

    apigateway_service.put_integration(
        restApiId=api_id,
        resourceId=resource_id,
        httpMethod="GET",
        type="MOCK",
        requestTemplates={"application/json": '{"statusCode": 200}'},
    )

    apigateway_service.put_method_response(
        restApiId=api_id,
        resourceId=resource_id,
        httpMethod="GET",
        statusCode="200",
        responseModels={"application/json": "Empty"},
    )

    apigateway_service.put_integration_response(
        restApiId=api_id,
        resourceId=resource_id,
        httpMethod="GET",
        statusCode="200",
        selectionPattern="",
    )

    apigateway_service.create_deployment(restApiId=api_id, stageName="prod")


def aws_eks_unrestricted_cidr(
    eks_service: EKSClient,
) -> None:
    eks_service.create_cluster(
        name="vulnerable-eks-cluster",
        version="1.21",
        roleArn="arn:aws:iam::123456789012:role/eks-service-role",
        resourcesVpcConfig={
            "subnetIds": ["subnet-12345678", "subnet-87654321"],
            "endpointPublicAccess": True,
            "endpointPrivateAccess": True,
            "publicAccessCidrs": ["0.0.0.0/0"],
        },
    )
    eks_service.create_cluster(
        name="safe-eks-cluster-1-of-2",
        version="1.21",
        roleArn="arn:aws:iam::123456789012:role/eks-service-role",
        resourcesVpcConfig={
            "subnetIds": ["subnet-12345678", "subnet-87654321"],
            "endpointPublicAccess": True,
            "endpointPrivateAccess": True,
            "publicAccessCidrs": ["10.0.0.0/24"],
        },
    )
    eks_service.create_cluster(
        name="safe-eks-cluster-2-of-2",
        version="1.21",
        roleArn="arn:aws:iam::123456789012:role/eks-service-role",
        resourcesVpcConfig={
            "subnetIds": ["subnet-12345678", "subnet-87654321"],
            "endpointPublicAccess": False,
            "endpointPrivateAccess": True,
            "publicAccessCidrs": ["0.0.0.0/0"],
        },
    )


def main() -> None:
    requests.post(  # noqa: S113
        "http://motoapi.amazonaws.com/moto-api/seed?a=12345",
    )
    ec2_service = boto3.client("ec2", region_name="us-east-1")
    apigateway_service = boto3.client("apigateway", region_name="us-east-1")
    eks_service = boto3.client("eks")
    allows_anyone_to_admin_ports(ec2_service)
    security_groups_ip_ranges_in_rfc1918(ec2_service)
    default_seggroup_allows_all_traffic(ec2_service)
    apigateway_allows_anonymous_access(apigateway_service)
    aws_eks_unrestricted_cidr(eks_service)
