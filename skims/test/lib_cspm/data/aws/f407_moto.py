import boto3
import requests
from mypy_boto3_ds import (
    DirectoryServiceClient,
)
from mypy_boto3_ec2 import (
    EC2Client,
)
from mypy_boto3_workspaces import (
    WorkSpacesClient,
)


def ebs_has_encryption_disabled(
    ec2_service: EC2Client,
) -> None:
    ec2_service.create_volume(
        AvailabilityZone="us-east-1",
        Size=8,
        VolumeType="gp2",
        Encrypted=False,
    )


def aws_workspaces_has_volume_encryption_disabled(
    ec2_service: EC2Client,
    ds_service: DirectoryServiceClient,
    workspaces_service: WorkSpacesClient,
) -> None:
    vpc_id = ec2_service.create_vpc(CidrBlock="10.0.0.0/16")["Vpc"]["VpcId"]

    subnet_1 = ec2_service.create_subnet(
        VpcId=vpc_id,
        CidrBlock="10.0.1.0/24",
        AvailabilityZone="us-east-1a",
    )["Subnet"]["SubnetId"]

    subnet_2 = ec2_service.create_subnet(
        VpcId=vpc_id,
        CidrBlock="10.0.2.0/24",
        AvailabilityZone="us-east-1b",
    )["Subnet"]["SubnetId"]

    ds_id = ds_service.create_directory(
        Name="some-directory",
        Password="adfljF1p023&23*",  # noqa: S106
        ShortName="some-directory",
        Size="Small",
        VpcSettings={
            "SubnetIds": [subnet_1, subnet_2],
            "VpcId": vpc_id,
        },
    )["DirectoryId"]

    workspaces_service.register_workspace_directory(DirectoryId=ds_id, EnableWorkDocs=False)
    # Vulnerable 1/3
    workspaces_service.create_workspaces(
        Workspaces=[
            {
                "BundleId": "wsb-55rrhyyg1",
                "DirectoryId": ds_id,
                "UserName": "username",
                "UserVolumeEncryptionEnabled": False,
                "RootVolumeEncryptionEnabled": False,
            },
        ],
    )
    # Vulnerable 2/3
    workspaces_service.create_workspaces(
        Workspaces=[
            {
                "BundleId": "wsb-55rrhyyg1",
                "DirectoryId": ds_id,
                "UserName": "username",
                "UserVolumeEncryptionEnabled": True,
                "RootVolumeEncryptionEnabled": False,
            },
        ],
    )
    # Vulnerable 3/3
    workspaces_service.create_workspaces(
        Workspaces=[
            {
                "BundleId": "wsb-55rrhyyg1",
                "DirectoryId": ds_id,
                "UserName": "username",
                "UserVolumeEncryptionEnabled": False,
                "RootVolumeEncryptionEnabled": True,
            },
        ],
    )
    # Safe 1/1
    workspaces_service.create_workspaces(
        Workspaces=[
            {
                "BundleId": "wsb-55rrhyyg1",
                "DirectoryId": ds_id,
                "UserName": "username",
                "UserVolumeEncryptionEnabled": True,
                "RootVolumeEncryptionEnabled": True,
            },
        ],
    )


def main() -> None:
    requests.post(  # noqa: S113
        "http://motoapi.amazonaws.com/moto-api/seed?a=12345",
    )
    ec2_service = boto3.client("ec2", region_name="us-east-1")
    workspaces_service = boto3.client("workspaces")
    ds_service = boto3.client("ds")
    ebs_has_encryption_disabled(ec2_service)
    aws_workspaces_has_volume_encryption_disabled(ec2_service, ds_service, workspaces_service)
