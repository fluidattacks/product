import boto3
from mypy_boto3_dynamodb import (
    DynamoDBClient,
)


def dynamodb_has_not_deletion_protection(
    dynamodb_service: DynamoDBClient,
) -> None:
    """Report vulnerabilities for methods.

    > dynamodb_has_not_deletion_protection,
    > dynamodb_has_not_point_in_time_recovery
    """
    table_name = "vulnerable-dynamodb-table"

    dynamodb_service.create_table(
        TableName=table_name,
        KeySchema=[{"AttributeName": "id", "KeyType": "HASH"}],
        AttributeDefinitions=[{"AttributeName": "id", "AttributeType": "S"}],
        ProvisionedThroughput={
            "ReadCapacityUnits": 5,
            "WriteCapacityUnits": 5,
        },
        SSESpecification={"Enabled": False},
    )


def main() -> None:
    dynamodb_service = boto3.client("dynamodb", region_name="us-east-1")
    dynamodb_has_not_deletion_protection(dynamodb_service)
