from collections.abc import (
    Collection,
)


def mock_data() -> dict[str, dict[str, Collection | bool]]:
    return {
        "use_default_security_group": {
            "Account": "234234234234",
            "LaunchTemplateVersions": [
                {
                    "LaunchTemplateId": "ltid1-018de572ae43404d8",
                    "LaunchTemplateName": "fluidtemplateunsafe",
                    "LaunchTemplateData": {
                        "EbsOptimized": True,
                    },
                },
                {
                    "LaunchTemplateId": "ltid2-018de572ae43404d8",
                    "LaunchTemplateName": "fluidtemplatesafe",
                    "LaunchTemplateData": {
                        "SecurityGroupIds": [
                            "sg-018de572ae43404d8",
                        ],
                        "SecurityGroups": [
                            "fluidsecurity",
                        ],
                    },
                },
            ],
        },
    }
