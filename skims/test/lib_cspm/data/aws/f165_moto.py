import json
from typing import TYPE_CHECKING

import boto3
import requests
from freezegun import (
    freeze_time,
)
from mypy_boto3_dax import (
    DAXClient,
)
from mypy_boto3_ec2 import (
    EC2Client,
)
from mypy_boto3_eks import (
    EKSClient,
)
from mypy_boto3_elasticache import (
    ElastiCacheClient,
)
from mypy_boto3_elbv2 import (
    ElasticLoadBalancingv2Client,
)
from mypy_boto3_emr import (
    EMRClient,
)
from mypy_boto3_iam import (
    IAMClient,
)
from mypy_boto3_kinesis import (
    KinesisClient,
)
from mypy_boto3_kms import (
    KMSClient,
)
from mypy_boto3_mq import (
    MQClient,
)
from mypy_boto3_opensearch import (
    OpenSearchServiceClient,
)
from mypy_boto3_rds import (
    RDSClient,
)
from mypy_boto3_sagemaker import (
    SageMakerClient,
)
from mypy_boto3_sns import (
    SNSClient,
)
from mypy_boto3_sqs import (
    SQSClient,
)

if TYPE_CHECKING:
    from mypy_boto3_sagemaker.type_defs import (
        ResourceConfigTypeDef,
    )


def policies_attached_to_users(
    iam_service: IAMClient,
) -> None:
    user_name = "vulnerable-user"
    iam_service.create_user(UserName=user_name)

    policy_document = {
        "Version": "2012-10-17",
        "Statement": [{"Effect": "Allow", "Action": "s3:*", "Resource": "*"}],
    }

    policy_response = iam_service.create_policy(
        PolicyName="vulnerable-policy",
        PolicyDocument=json.dumps(policy_document),
    )
    policy_arn = policy_response["Policy"]["Arn"]

    iam_service.attach_user_policy(
        UserName=user_name,
        PolicyArn=policy_arn,
    )


def users_with_multiple_access_keys(
    iam_service: IAMClient,
) -> None:
    user_name = "vulnerable-user-multiple-keys"
    iam_service.create_user(UserName=user_name)
    iam_service.create_access_key(UserName=user_name)
    iam_service.create_access_key(UserName=user_name)


def root_has_access_keys(
    iam_service: IAMClient,
) -> None:
    user_name = "root"
    iam_service.create_access_key(UserName=user_name)


def eks_mock(
    eks_service: EKSClient,
    kms_sevice: KMSClient,
) -> None:
    """Report vulnerabilities for methods.

    > eks_has_endpoints_publicly_accessible
    > aws_eks_unencrypted_secrets
    """
    kms_key = kms_sevice.create_key()
    eks_service.create_cluster(
        name="safe-cluster",
        version="1.21",
        roleArn="arn:aws:iam::123456789012:role/eks-service-role",
        resourcesVpcConfig={
            "subnetIds": ["subnet-12345678", "subnet-87654321"],
            "endpointPublicAccess": False,
            "endpointPrivateAccess": False,
        },
        encryptionConfig=[
            {
                "resources": [
                    "secrets",
                ],
                "provider": {"keyArn": kms_key["KeyMetadata"]["Arn"]},
            },
        ],
    )
    eks_service.create_cluster(
        name="vulnerable-cluster",
        version="1.21",
        roleArn="arn:aws:iam::123456789012:role/eks-service-role",
        resourcesVpcConfig={
            "subnetIds": ["subnet-12345678", "subnet-87654321"],
            "endpointPublicAccess": True,
            "endpointPrivateAccess": False,
        },
    )


def rds_mocks(
    rds_service: RDSClient,
) -> None:
    """Report vulnerabilities for methods.

    > rds_not_uses_iam_authentication,
    > rds_has_public_snapshots
    > aws_rds_unencrypted_db_snapshot
    """
    rds_service.create_db_instance(
        DBInstanceIdentifier="my-db-instance",
        AllocatedStorage=20,
        DBName="mydatabase",
        Engine="mysql",
        EngineVersion="8.0.23",
        MasterUsername="admin",
        MasterUserPassword="password",
        DBInstanceClass="db.t2.micro",
        PubliclyAccessible=True,
    )

    rds_service.create_db_snapshot(
        DBSnapshotIdentifier="my-db-snapshot",
        DBInstanceIdentifier="my-db-instance",
    )
    rds_service.modify_db_snapshot_attribute(
        DBSnapshotIdentifier="my-db-snapshot",
        AttributeName="restore",
        ValuesToAdd=["all"],
    )


def elasticache_is_at_rest_encryption_disabled(
    elasticache_service: ElastiCacheClient,
) -> None:
    """Report vulnerabilities for methods.

    > elasticache_is_at_rest_encryption_disabled
    > elasticache_is_transit_encryption_disabled
    """
    elasticache_service.create_cache_cluster(
        CacheClusterId="vulnerable-redis-cluster",
        NumCacheNodes=1,
        CacheNodeType="cache.t2.micro",
        Engine="redis",
        EngineVersion="6.2",
        Port=6379,
        CacheSubnetGroupName="default",
    )


def sns_is_server_side_encryption_disabled(
    sns_service: SNSClient,
) -> None:
    sns_service.create_topic(Name="vulnerable-sns-topic")


def sns_can_anyone_publish(
    sns_service: SNSClient,
) -> None:
    """Report vulnerabilities for methods.

    > sns_can_anyone_publish
    > sns_can_anyone_subscribe
    """
    response = sns_service.create_topic(Name="vulnerable-sns-topic-2")
    topic_arn = response["TopicArn"]

    policy = {
        "Version": "2012-10-17",
        "Statement": [
            {"Principal": "*"},
            {"Principal": "*", "Condition": "Anything"},
            {
                "Effect": "Allow",
                "Principal": {"AWS": "*"},
                "Action": "SNS:Publish",
                "Resource": topic_arn,
            },
            {
                "Effect": "Allow",
                "Principal": {"AWS": "*"},
                "Action": ["SNS:Subscribe", "SNS:Receive"],
                "Resource": topic_arn,
            },
        ],
    }

    sns_service.set_topic_attributes(
        TopicArn=topic_arn,
        AttributeName="Policy",
        AttributeValue=json.dumps(policy),
    )


def sqs_is_encryption_disabled(
    sqs_service: SQSClient,
) -> None:
    sqs_service.create_queue(
        QueueName="vulnerable-queue",
        Attributes={
            "SqsManagedSseEnabled": "false",
        },
    )


def aws_ebs_public_snapshots(ec2_service: EC2Client) -> None:
    ec2_create_volume_response = ec2_service.create_volume(
        AvailabilityZone="us-east-1a",
        Size=8,
        VolumeType="gp2",
    )
    volume_id = ec2_create_volume_response["VolumeId"]
    ec2_service.create_snapshot(
        VolumeId=volume_id,
        Description="Safe snapshot",
    )
    snapshot_create_response = ec2_service.create_snapshot(
        VolumeId=volume_id,
        Description="Vulnerable snapshot",
    )
    snapshot_id = snapshot_create_response["SnapshotId"]
    ec2_service.modify_snapshot_attribute(
        SnapshotId=snapshot_id,
        Attribute="createVolumePermission",
        OperationType="add",
        GroupNames=["all"],
    )


@freeze_time("2024-10-03 05:00:00.000000+00:00")
def emr_mock(emr_service: EMRClient) -> None:
    emr_service.run_job_flow(Name="vulnerable_job_flow", Instances={})
    emr_service.run_job_flow(
        Name="safe_job_flow",
        Instances={},
        SecurityConfiguration="emr_configuration",
    )


def mock_opensearch(opensearch_service: OpenSearchServiceClient) -> None:
    """Report vulnerabilities for methods.

    > aws_opensearch_without_encryption_at_rest
    > aws_opensearch_domain_node_to_node_encryption
    """
    opensearch_service.create_domain(
        DomainName="vulnerable_domain",
        NodeToNodeEncryptionOptions={"Enabled": False},
    )
    opensearch_service.create_domain(
        DomainName="safe_domain",
        EncryptionAtRestOptions={"Enabled": True},
        NodeToNodeEncryptionOptions={"Enabled": True},
    )


@freeze_time("2024-10-11 05:00:00.000000+00:00")
def aws_kinesis_stream_without_encryption_at_rest(
    kinesis_service: KinesisClient,
) -> None:
    # Vulnerable stream
    kinesis_service.create_stream(StreamName="vulnerable-stream", ShardCount=1)

    # Safe stream
    kinesis_service.create_stream(StreamName="safe-stream", ShardCount=1)
    kinesis_service.start_stream_encryption(
        StreamName="safe-stream",
        EncryptionType="KMS",
        KeyId="alias/aws/kinesis",
    )


@freeze_time("2024-10-11 05:00:00.000000+00:00")
def aws_mq_broker_publicly_accessible(
    mq_service: MQClient,
) -> None:
    def create_broker(*, broker_name: str, public_access: bool) -> None:
        mq_service.create_broker(
            BrokerName=broker_name,
            EngineType="ACTIVEMQ",
            EngineVersion="5.18.4",
            HostInstanceType="mq.t3.micro",
            AutoMinorVersionUpgrade=True,
            DeploymentMode="ACTIVE_STANDBY_MULTI_AZ",
            PubliclyAccessible=public_access,
            Users=[
                {
                    "ConsoleAccess": True,
                    "Username": "user",
                    "Password": "password",
                },
            ],
        )

    create_broker(
        broker_name="vulnerable_broker",
        public_access=True,
    )
    create_broker(
        broker_name="safe_broker",
        public_access=False,
    )


@freeze_time("2024-10-24 05:00:00.000000+00:00")
def aws_sagemaker_training_job_intercontainer_encryption(
    sagemaker_client: SageMakerClient,
) -> None:
    resource_config: ResourceConfigTypeDef = {
        "InstanceCount": 1,
        "InstanceType": "ml.c4.4xlarge",
        "VolumeSizeInGB": 30,
    }
    sagemaker_client.create_training_job(
        TrainingJobName="vulnerable-training-job",
        AlgorithmSpecification={
            "TrainingImage": "13.dkr.ecr.us-east-1.amazonaws.com/sgm:0.23-1-c",
            "TrainingInputMode": "File",
        },
        RoleArn="arn:aws:iam::123456789012:role/role",
        OutputDataConfig={"S3OutputPath": "s3://bucket/prefix"},
        ResourceConfig=resource_config,
        StoppingCondition={"MaxRuntimeInSeconds": 86400},
        EnableInterContainerTrafficEncryption=False,
    )
    sagemaker_client.create_training_job(
        TrainingJobName="safe-training-job",
        AlgorithmSpecification={
            "TrainingImage": "13.dkr.ecr.us-east-1.amazonaws.com/sgm:0.23-1-c",
            "TrainingInputMode": "File",
        },
        RoleArn="arn:aws:iam::123456789012:role/role",
        OutputDataConfig={"S3OutputPath": "s3://bucket/prefix"},
        ResourceConfig=resource_config,
        StoppingCondition={"MaxRuntimeInSeconds": 86400},
        EnableInterContainerTrafficEncryption=True,
    )


@freeze_time("2024-10-30 05:00:00.000000+00:00")
def aws_sagemaker_notebook_instance_encryption(
    sagemaker_client: SageMakerClient,
) -> None:
    sagemaker_client.create_notebook_instance(
        NotebookInstanceName="vulnerable-notebook-instance",
        RoleArn="arn:aws:iam::123456789012:role/role",
        InstanceType="ml.c4.4xlarge",
        VolumeSizeInGB=30,
        DirectInternetAccess="Enabled",
    )
    sagemaker_client.create_notebook_instance(
        NotebookInstanceName="safe-notebook-instance",
        RoleArn="arn:aws:iam::123456789012:role/role",
        InstanceType="ml.c4.4xlarge",
        VolumeSizeInGB=30,
        DirectInternetAccess="Enabled",
        KmsKeyId="arn:aws:kms:ca-central-1:000:key/15460369-0619-44ad-818b-",
    )


def aws_dax_cluster_without_encryption_at_rest(
    dax_service: DAXClient,
) -> None:
    dax_service.create_cluster(
        ClusterName="vulnerable-dax-cluster",
        NodeType="dax.t2.small",
        ReplicationFactor=1,
        IamRoleArn="arn:aws:iam::123456789012:role/role",
        SSESpecification={
            "Enabled": False,
        },
    )
    dax_service.create_cluster(
        ClusterName="safe-dax-cluster",
        NodeType="dax.t2.small",
        ReplicationFactor=1,
        IamRoleArn="arn:aws:iam::123456789012:role/role",
        SSESpecification={
            "Enabled": True,
        },
    )


def aws_alb_does_not_drop_invalid_header_fields(
    elbv2_service: ElasticLoadBalancingv2Client,
    ec2_service: EC2Client,
) -> None:
    vpc_response = ec2_service.create_vpc(
        CidrBlock="10.0.0.0/16",
        TagSpecifications=[
            {
                "ResourceType": "vpc",
                "Tags": [{"Key": "Name", "Value": "my-vpc"}],
            },
        ],
    )
    vpc_id = vpc_response["Vpc"]["VpcId"]
    subnet_response = ec2_service.create_subnet(
        VpcId=vpc_id,
        CidrBlock="10.0.1.0/24",
        AvailabilityZone="us-east-1a",
        TagSpecifications=[
            {
                "ResourceType": "subnet",
                "Tags": [{"Key": "Name", "Value": "my-subnet"}],
            },
        ],
    )
    subnet_id = subnet_response["Subnet"]["SubnetId"]
    vulnerable_alb = elbv2_service.create_load_balancer(
        Name="vulnerable-alb",
        Type="application",
        Subnets=[subnet_id],
    )
    elbv2_service.modify_load_balancer_attributes(
        LoadBalancerArn=vulnerable_alb["LoadBalancers"][0]["LoadBalancerArn"],
        Attributes=[
            {
                "Key": "routing.http.drop_invalid_header_fields.enabled",
                "Value": "false",
            },
        ],
    )
    safe_alb = elbv2_service.create_load_balancer(
        Name="safe-alb",
        Type="application",
        Subnets=[subnet_id],
    )
    elbv2_service.modify_load_balancer_attributes(
        LoadBalancerArn=safe_alb["LoadBalancers"][0]["LoadBalancerArn"],
        Attributes=[
            {
                "Key": "routing.http.drop_invalid_header_fields.enabled",
                "Value": "true",
            },
        ],
    )


def main() -> None:
    requests.post(  # noqa: S113
        "http://motoapi.amazonaws.com/moto-api/seed?a=12345",
    )
    iam_service = boto3.client("iam")
    eks_service = boto3.client("eks")
    rds_service = boto3.client("rds")
    sns_service = boto3.client("sns", region_name="us-east-1")
    sqs_service = boto3.client("sqs", region_name="us-east-1")
    elasticache_service = boto3.client("elasticache", region_name="us-east-1")
    ec2_service = boto3.client("ec2", region_name="us-east-1")
    iam_service.create_user(UserName="root")
    kms_service = boto3.client("kms")
    emr_service = boto3.client("emr")
    opensearch_service = boto3.client("opensearch")
    kinesis_service = boto3.client("kinesis")
    mq_service = boto3.client("mq", region_name="us-east-1")
    sagemaker_service = boto3.client("sagemaker")
    dax_service = boto3.client("dax")
    elbv2_service = boto3.client("elbv2")
    policies_attached_to_users(iam_service)
    users_with_multiple_access_keys(iam_service)
    root_has_access_keys(iam_service)
    eks_mock(eks_service, kms_service)
    rds_mocks(rds_service)
    elasticache_is_at_rest_encryption_disabled(elasticache_service)
    sns_is_server_side_encryption_disabled(sns_service)
    sns_can_anyone_publish(sns_service)
    sqs_is_encryption_disabled(sqs_service)
    aws_ebs_public_snapshots(ec2_service=ec2_service)
    emr_mock(emr_service)
    mock_opensearch(opensearch_service)
    aws_kinesis_stream_without_encryption_at_rest(kinesis_service)
    aws_mq_broker_publicly_accessible(mq_service)
    aws_sagemaker_training_job_intercontainer_encryption(sagemaker_service)
    aws_sagemaker_notebook_instance_encryption(sagemaker_service)
    aws_dax_cluster_without_encryption_at_rest(dax_service)
    aws_alb_does_not_drop_invalid_header_fields(elbv2_service, ec2_service)
