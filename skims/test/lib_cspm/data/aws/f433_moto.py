import boto3
from mypy_boto3_redshift import (
    RedshiftClient,
)


def redshift_has_encryption_disabled(
    redshift_service: RedshiftClient,
) -> None:
    redshift_service.create_cluster(
        ClusterIdentifier="vulnerable-redshift-cluster",
        NodeType="dc2.large",
        MasterUsername="masteruser",
        MasterUserPassword="MasterUserPassword123!",
        DBName="mydatabase",
        Port=5439,
        ClusterType="single-node",
        Encrypted=False,
        Tags=[
            {"Key": "Name", "Value": "vulnerable-redshift-cluster"},
            {"Key": "Environment", "Value": "Production"},
        ],
    )


def main() -> None:
    redshift_service = boto3.client("redshift", region_name="us-east-1")
    redshift_has_encryption_disabled(redshift_service)
