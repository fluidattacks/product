from collections.abc import (
    Collection,
)


def mock_data() -> dict[str, dict[str, Collection | bool]]:
    return {
        "aws_unrestricted_access_to_msk_brokers": {
            "ClusterInfoList": [
                {
                    "ClusterType": "provisioned",
                    "ClusterArn": "arn:aws:kafka:us-east-1:123:cluster/abc",
                },
            ],
            "ClusterInfo": {"ClientAuthentication": {"Unauthenticated": {"Enabled": True}}},
        },
        "default": {},
    }
