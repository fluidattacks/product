import boto3
from mypy_boto3_efs import (
    EFSClient,
)


def efs_is_encryption_disabled(
    efs_service: EFSClient,
) -> None:
    efs_service.create_file_system(
        CreationToken="vulnerable-efs",
        PerformanceMode="generalPurpose",
        Encrypted=False,
        Tags=[
            {"Key": "Name", "Value": "vulnerable-efs"},
            {"Key": "Environment", "Value": "Production"},
        ],
    )


def main() -> None:
    efs_service = boto3.client("efs", region_name="us-east-1")
    efs_is_encryption_disabled(efs_service)
