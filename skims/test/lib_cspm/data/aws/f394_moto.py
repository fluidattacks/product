import boto3
from mypy_boto3_cloudtrail import (
    CloudTrailClient,
)
from mypy_boto3_s3 import (
    S3Client,
)


def cloudtrail_files_not_validated(
    cloudtrail_service: CloudTrailClient,
    s3_service: S3Client,
) -> None:
    bucket_name = "my-cloudtrail-logs-bucket"
    s3_service.create_bucket(Bucket=bucket_name)
    cloudtrail_service.create_trail(
        Name="vulnerable-cloudtrail",
        S3BucketName=bucket_name,
        IncludeGlobalServiceEvents=True,
        IsMultiRegionTrail=True,
        EnableLogFileValidation=False,
        IsOrganizationTrail=False,
    )

    cloudtrail_service.start_logging(Name="vulnerable-cloudtrail")


def main() -> None:
    cloudtrail_service = boto3.client("cloudtrail", region_name="us-east-1")
    s3_service = boto3.client("s3")
    cloudtrail_files_not_validated(cloudtrail_service, s3_service)
