from typing import (
    Any,
)

from test.lib_cspm.data.gcp import (
    f200,
    f325,
    f405,
)

MOCKERS = {
    "F200": f200.mock_data(),
    "F325": f325.mock_data(),
    "F405": f405.mock_data(),
}


def get_mock(finding: str) -> Any | None:  # noqa: ANN401
    mock = MOCKERS.get(finding)
    if mock:
        return mock
    return None
