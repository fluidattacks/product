# noqa: INP001
import tempfile
from functools import (
    partial,
)
from pathlib import Path
from typing import (
    Any,
)
from unittest.mock import (
    patch,
)

import pytest
from lib_cspm.aws.utils import (
    run_boto3_fun,
)

from test.lib_cspm.data.aws.fixtures import (
    get_moto_mock,
)
from test.lib_cspm.data.aws.moto_patch import (
    mock_aio_aws,
)
from test.lib_cspm.data.aws.side_effects import (
    get_mock_info as get_aws_mock_info,
)
from test.lib_cspm.data.aws.side_effects import (
    make_side_effect,
)
from test.lib_cspm.data.azure.side_effects import (
    AzureMockCredentials,
    get_azure_mock,
)
from test.lib_cspm.data.azure.side_effects import (
    make_side_effect as azure_make_side_effect,
)
from test.lib_cspm.data.gcp.side_effects import (
    get_mock as get_gcp_mock_info,
)
from test.utils import (
    check_that_csv_results_match,
    create_config,
    skims,
)


def run_finding(finding: str) -> None:
    async def handle_side_effect(
        mock_data_aws: Any,  # noqa: ANN401
        *args: Any,  # noqa: ARG001 ANN401
        **kwargs: Any,  # noqa: ANN401
    ) -> Any:  # noqa: ANN401
        available = make_side_effect(mock_data_aws) if mock_data_aws else None
        if not available:
            return await run_boto3_fun(**kwargs)
        return available

    with tempfile.TemporaryDirectory() as tmp_dir:
        path = Path(tmp_dir) / f"{finding}.yaml"
        with path.open("w", encoding="utf-8") as tmpfile:
            template = "skims/test/lib_cspm/test_configs/template.yaml"
            tmpfile.write(create_config(finding, template))

        if mock_data_aws := get_aws_mock_info(finding):
            aws_patch = f"lib_cspm.aws.{finding.lower()}.run_boto3_fun"
        else:
            aws_patch = "lib_cspm.aws.utils.run_boto3_fun"
            mock_data_aws = {}

        if mock_data_gcp := get_gcp_mock_info(finding):
            gcp_patch = f"lib_cspm.gcp.{finding.lower()}.run_gcp_client"
        else:
            gcp_patch = "lib_cspm.gcp.utils.run_gcp_client"
            mock_data_gcp = {}

        if mock_data_azure := get_azure_mock(finding):
            mock_azure_path = f"lib_cspm.azure.{finding.lower()}.run_azure_client"
        else:
            mock_azure_path = "lib_cspm.azure.azure_clients.common.run_azure_client"
            mock_data_azure = {}

        handle_side_effect_with_partial = partial(handle_side_effect, mock_data_aws=mock_data_aws)

        with (
            patch(aws_patch, new=handle_side_effect_with_partial),
            patch("lib_cspm.gcp.analyze.get_regions", return_value=["asia-east1"]),
            patch(gcp_patch, return_value=mock_data_gcp),
            patch(
                "lib_cspm.azure.analyze.ClientSecretCredential",
                AzureMockCredentials,
            ),
            patch(
                mock_azure_path,
                side_effect=azure_make_side_effect(mock_data_azure),
            ),
        ):
            run_moto = get_moto_mock(finding)
            if run_moto:
                run_moto()
            code, stdout, _ = skims("scan", str(path))

        assert code == 0, stdout
        assert "[INFO] Startup work dir is:" in stdout
        assert "[INFO] An output file has been written:" in stdout
        check_that_csv_results_match(finding, "skims/test/lib_cspm/results")


def run_multiple_findings_test(findings: list[str]) -> None:
    for finding_code in findings:
        with mock_aio_aws():
            run_finding(finding_code)


@pytest.mark.usefixtures("test_clean_cspm_cache")
@pytest.mark.skims_test_group("cspm_findings_001_099")
def test_cspm_findings_001_099() -> None:
    findings = [
        "F005",
        "F016",
        "F024",
        "F031",
        "F073",
        "F081",
        "F099",
    ]
    run_multiple_findings_test(findings)


@pytest.mark.usefixtures("test_clean_cspm_cache")
@pytest.mark.skims_test_group("cspm_findings_100_199")
def test_cspm_findings_100_199() -> None:
    findings = [
        "F101",
        "F109",
        "F148",
        "F157",
        "F158",
        "F165",
        "F177",
        "F183",
    ]
    run_multiple_findings_test(findings)


@pytest.mark.usefixtures("test_clean_cspm_cache")
@pytest.mark.skims_test_group("cspm_findings_200_299")
def test_cspm_findings_200_299() -> None:
    findings = [
        "F200",
        "F203",
        "F246",
        "F256",
        "F257",
        "F258",
        "F259",
        "F277",
        "F281",
    ]
    run_multiple_findings_test(findings)


@pytest.mark.usefixtures("test_clean_cspm_cache")
@pytest.mark.skims_test_group("cspm_findings_300_399")
def test_cspm_findings_300_399() -> None:
    findings = [
        "F300",
        "F319",
        "F325",
        "F333",
        "F394",
        "F335",
        "F363",
        "F372",
        "F392",
        "F396",
    ]
    run_multiple_findings_test(findings)


@pytest.mark.usefixtures("test_clean_cspm_cache")
@pytest.mark.skims_test_group("cspm_findings_400_499")
def test_cspm_findings_400_499() -> None:
    findings = [
        "F400",
        "F402",
        "F405",
        "F406",
        "F407",
        "F433",
        "F446",
    ]
    run_multiple_findings_test(findings)
