{ inputs, makeScript, outputs, ... }: {
  jobs."/skims/test/hooks/pre-push" = makeScript {
    entrypoint = ./entrypoint.sh;
    name = "skims-test-migration-needed";
    searchPaths.bin = [ inputs.nixpkgs.git inputs.nixpkgs.gnugrep ];
  };
}
