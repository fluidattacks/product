# noqa: INP001
import json

import pytest
from lib_api.api_model import (
    APIContext,
    Auth,
    AuthEnum,
    Body,
    DefaultAttribute,
    HttpMethodEnum,
    Query,
    Request,
    RequestBodyModeEnum,
    Response,
)
from lib_api.parse import (
    parse_json,
    parse_response,
)
from lib_api.utils import (
    prepare_request_headers,
    prepare_request_params,
)


@pytest.mark.skims_test_group("all_unittesting")
def test_parse_response() -> None:
    response_data = {
        "code": 200,
        "header": [
            {
                "key": "Content-Type",
                "value": "application/json",
                "type": "text",
            },
        ],
        "body": '{"success": true}',
        "responseTime": 150,
    }
    expected = Response(
        status_code=200,
        headers=[DefaultAttribute(key="Content-Type", value="application/json", type="text")],
        response_body='{"success": true}',
        response_time=150,
    )
    assert parse_response(response_data) == expected


@pytest.mark.skims_test_group("all_unittesting")
def test_parse_json() -> None:
    json_data = {
        "item": [
            {
                "name": "Test Query",
                "request": {
                    "method": "POST",
                    "header": [
                        {
                            "key": "Content-Type",
                            "value": "application/json",
                            "type": "text",
                        },
                    ],
                    "body": {"mode": "raw", "raw": '{"key": "value"}'},
                    "auth": {
                        "type": "bearer",
                        "bearer": [
                            {
                                "key": "token",
                                "value": "abc123",
                                "type": "string",
                            },
                        ],
                    },
                    "url": {
                        "raw": "https://api.example.com/resource",
                        "variable": [],
                    },
                },
                "response": [
                    {
                        "code": 200,
                        "header": [
                            {
                                "key": "Content-Type",
                                "value": "application/json",
                                "type": "text",
                            },
                        ],
                        "body": '{"success": true}',
                        "responseTime": 150,
                    },
                ],
            },
            {
                "name": "Test Query 2",
                "request": {
                    "method": "POST",
                    "header": [
                        {
                            "key": "Content-Type",
                            "value": "application/json",
                            "type": "text",
                        },
                    ],
                    "body": {"mode": "raw", "raw": '{"key": "value"}'},
                    "auth": {
                        "type": "bearer",
                        "bearer": [
                            {
                                "key": "token",
                                "value": "abc123",
                                "type": "string",
                            },
                        ],
                    },
                    "url": {
                        "raw": "https://api.example.com/resource",
                        "variable": [],
                    },
                },
                "response": [
                    {
                        "header": None,
                        "body": None,
                        "responseTime": 150,
                    },
                ],
            },
        ],
    }
    expected = APIContext(
        queries=(
            Query(
                name="Test Query",
                request=Request(
                    url="https://api.example.com/resource",
                    http_method=HttpMethodEnum.POST,
                    headers=[
                        DefaultAttribute(
                            key="Content-Type",
                            value="application/json",
                            type="text",
                        ),
                    ],
                    variable={},
                    body=Body(mode=RequestBodyModeEnum.RAW, data='{"key": "value"}'),
                    auth=Auth(
                        type=AuthEnum.BEARER,
                        data=[DefaultAttribute(key="token", value="abc123", type="string")],
                    ),
                ),
                response=Response(
                    status_code=200,
                    headers=[
                        DefaultAttribute(
                            key="Content-Type",
                            value="application/json",
                            type="text",
                        ),
                    ],
                    response_body='{"success": true}',
                    response_time=150,
                ),
            ),
            Query(
                name="Test Query 2",
                request=Request(
                    url="https://api.example.com/resource",
                    http_method=HttpMethodEnum.POST,
                    headers=[
                        DefaultAttribute(
                            key="Content-Type",
                            value="application/json",
                            type="text",
                        ),
                    ],
                    variable={},
                    body=Body(mode=RequestBodyModeEnum.RAW, data='{"key": "value"}'),
                    auth=Auth(
                        type=AuthEnum.BEARER,
                        data=[DefaultAttribute(key="token", value="abc123", type="string")],
                    ),
                ),
                response=Response(
                    status_code=0,
                    headers=[],
                    response_body=None,
                    response_time=150,
                ),
            ),
        ),
    )
    assert parse_json(json_data) == expected


@pytest.mark.skims_test_group("all_unittesting")
@pytest.mark.parametrize(
    ("body", "expected"),
    [
        (
            Body(mode=RequestBodyModeEnum.RAW, data=json.dumps({"key": "value"})),
            {"key": "value"},
        ),
        (None, {}),
        (Body(mode=RequestBodyModeEnum.URLENCODED, data="key=value"), {}),
    ],
)
def test_prepare_request_params(body: Body | None, expected: dict) -> None:
    assert prepare_request_params(body) == expected


@pytest.mark.skims_test_group("all_unittesting")
@pytest.mark.parametrize(
    ("headers", "expected"),
    [
        (
            [
                DefaultAttribute(key="Content-Type", value="application/json", type="string"),
                DefaultAttribute(key="Authorization", value="Bearer token", type="string"),
            ],
            {
                "Content-Type": "application/json",
                "Authorization": "Bearer token",
            },
        ),
        (
            [
                DefaultAttribute(
                    key="Content-Type",
                    value="application/json",
                    type="string",
                    disabled=True,
                ),
                DefaultAttribute(
                    key="Authorization",
                    value="Bearer token",
                    type="string",
                    disabled=False,
                ),
            ],
            {"Authorization": "Bearer token"},
        ),
        ([], {}),
    ],
)
def test_prepare_request_headers(headers: list[DefaultAttribute], expected: dict) -> None:
    assert prepare_request_headers(headers) == expected
