# noqa: INP001
import pytest

from test.utils import (
    run_skims_group,
)

CONFIG_PATH = "skims/test/lib_api/test_configs"
RESULTS_PATH = "skims/test/lib_api/results"


@pytest.mark.skip(reason="Lib API currently has no methods")
def test_lib_api() -> None:
    run_skims_group("lib_api", CONFIG_PATH, RESULTS_PATH)
