# noqa: INP001
import pytest
from lib_sca.model import (
    Advisory,
    Platform,
)
from lib_sca.vulnerabilities import (
    get_vulnerabilities,
)
from pytest_mock import (
    MockerFixture,
)


@pytest.mark.skims_test_group("sca_unittesting")
@pytest.mark.asyncio
@pytest.mark.parametrize(
    ("advisories", "expected_output"),
    [
        (
            [
                Advisory(
                    id="CVE-2018-5689",
                    severity=None,
                    cwe_ids=None,
                    package_manager="maven",
                    package_name="package",
                    vulnerable_version="=1.0.0",
                    source="MANUAL",
                ),
                Advisory(
                    id="CVE-2021-3456",
                    severity=None,
                    cwe_ids=None,
                    package_manager="maven",
                    package_name="package",
                    vulnerable_version="^1.2.0",
                    source="MANUAL",
                ),
            ],
            [
                Advisory(
                    id="CVE-2021-3456",
                    severity=None,
                    cwe_ids=None,
                    package_manager="maven",
                    package_name="package",
                    vulnerable_version="^1.2.0",
                    source="MANUAL",
                ),
            ],
        ),
    ],
)
async def test_get_vulnerabilities(
    mocker: MockerFixture,
    advisories: list[Advisory],
    expected_output: list[Advisory],
) -> None:
    platform = Platform.NPM
    product = "package"
    version = "1.2.3"
    mocker.patch(
        "lib_sca.vulnerabilities.get_package_advisories",
        return_value=advisories,
    )
    result = get_vulnerabilities(platform, product, version)
    assert result == expected_output
