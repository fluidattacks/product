# noqa: INP001
from pathlib import Path

import pytest
import yaml
from cvss import (
    CVSS3,
    CVSS3Error,
)
from lib_sca.model import (
    SUPPORTED_PLATFORMS,
)

MANDATORY_KEYS = [
    "type",
    "id",
    "package_name",
    "package_manager",
    "vulnerable_version",
    "sources",
]


def is_cvssv3(cvss: str) -> bool:
    try:
        CVSS3(cvss)
    except CVSS3Error:
        return False
    else:
        return True


@pytest.mark.skims_test_group("sca_unittesting")
def test_manual_advisories_format() -> None:
    for path in Path("skims/static/advisories").glob("**/*.json"):
        with Path(path).open(encoding="utf-8") as file:
            adv: dict[str, str | list[str]] = yaml.safe_load(file)
            assert adv["type"] in {"PATCH", "NEW"}
            assert adv["package_manager"] in SUPPORTED_PLATFORMS
            assert all(key in adv for key in MANDATORY_KEYS)
            assert isinstance(adv["sources"], list)
            assert all(source.startswith("https://") for source in adv["sources"])

            if adv["type"] == "NEW":
                assert is_cvssv3(str(adv["severity"]))
                assert isinstance(adv["cwe_ids"], list)
                assert all(cwe.startswith("CWE-") for cwe in adv["cwe_ids"])
                assert len(adv["details"]) > 20
