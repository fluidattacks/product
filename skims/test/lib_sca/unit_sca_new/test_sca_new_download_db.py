# noqa: INP001
import os
from datetime import (
    timedelta,
)

import pytest
from lib_sca.sca_new.db.db_entry import (
    DB_CACHE_DIR,
    is_update_available,
    new_db_entry,
    update_db,
)
from lib_sca.sca_new.db.listing import (
    ListingEntry,
)
from lib_sca.sca_new.db.metadata import (
    is_superseded_by,
    metadata_from_dir,
)


@pytest.mark.asyncio
@pytest.mark.order(1)
@pytest.mark.skims_test_group("sca_unittesting")
async def test_download_db() -> None:
    db_entry = new_db_entry()
    await update_db(db_entry)
    assert os.path.exists(os.path.join(DB_CACHE_DIR, "metadata.json"))  # noqa: PTH118,PTH110
    assert os.path.exists(os.path.join(DB_CACHE_DIR, "vulnerability.db"))  # noqa: PTH118,PTH110


@pytest.mark.asyncio
@pytest.mark.order(2)
@pytest.mark.skims_test_group("sca_unittesting")
async def test_download_db_alredy_exists() -> None:
    db_entry = new_db_entry()
    update_available, _, __ = await is_update_available(db_entry)
    assert not update_available
    assert os.path.exists(os.path.join(DB_CACHE_DIR, "metadata.json"))  # noqa: PTH118,PTH110
    assert os.path.exists(os.path.join(DB_CACHE_DIR, "vulnerability.db"))  # noqa: PTH118,PTH110


@pytest.mark.order(3)
@pytest.mark.skims_test_group("sca_unittesting")
def test_get_metadata() -> None:
    metadata = metadata_from_dir(DB_CACHE_DIR)
    assert metadata is not None


@pytest.mark.order(3)
@pytest.mark.skims_test_group("sca_unittesting")
def test_get_metadata_fail() -> None:
    metadata = metadata_from_dir(os.path.join(DB_CACHE_DIR, "metadata_fail.json"))  # noqa: PTH118
    assert metadata is None


@pytest.mark.order(4)
@pytest.mark.skims_test_group("sca_unittesting")
def test_is_superseded_by() -> None:
    metadata = metadata_from_dir(DB_CACHE_DIR)
    assert metadata is not None
    assert is_superseded_by(
        metadata,
        ListingEntry(
            built=metadata.built + timedelta(days=-4),
            version=5,
            url="",
            checksum="",
        ),
    )
    assert not is_superseded_by(
        metadata,
        ListingEntry(
            built=metadata.built + timedelta(days=5),
            version=5,
            url="",
            checksum="",
        ),
    )
    assert is_superseded_by(
        metadata,
        ListingEntry(
            built=metadata.built + timedelta(days=5),
            version=4,
            url="",
            checksum="",
        ),
    )
