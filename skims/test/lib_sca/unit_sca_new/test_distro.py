# noqa: INP001
from typing import (
    cast,
)

import pytest
from lib_sca.sca_new.distro import (
    Distro,
    new_distro_from_release,
)
from lib_sca.sca_new.distro.type import (
    LinuxDistributionType,
)
from semantic_version import (
    Version,
)


@pytest.mark.skims_test_group("sca_unittesting")
def test_is_rolling() -> None:
    rolling_distros = [
        LinuxDistributionType.WOLFI,
        LinuxDistributionType.CHAINGUARD,
        LinuxDistributionType.ARCHLINUX,
        LinuxDistributionType.GENTOO,
    ]

    for distro_type in rolling_distros:
        distro = Distro(type=distro_type, version=None, raw_version="", id_like=[])
        assert distro.is_rolling() is True

    non_rolling_distro = Distro(
        type=LinuxDistributionType.UBUNTU,
        version=None,
        raw_version="",
        id_like=[],
    )
    assert non_rolling_distro.is_rolling() is False


@pytest.mark.skims_test_group("sca_unittesting")
def test_full_version() -> None:
    raw_version = "1.2.3"
    distro = Distro(
        type=LinuxDistributionType.UBUNTU,
        version=Version.coerce("1.2.3"),
        raw_version=raw_version,
        id_like=[],
    )
    assert distro.full_version() == raw_version


@pytest.mark.parametrize(
    ("distro_type", "version", "raw_version", "id_like"),
    [
        (
            LinuxDistributionType.UBUNTU,
            Version(major=20, minor=4, patch=0),
            "20.04",
            ["debian"],
        ),
        (
            LinuxDistributionType.CENTOS,
            Version(major=7, minor=0, patch=0),
            "7",
            ["rhel", "fedora"],
        ),
        (
            LinuxDistributionType.ALPINE,
            Version(major=3, minor=14, patch=0),
            "3.14.0",
            [],
        ),
    ],
)
@pytest.mark.skims_test_group("sca_unittesting")
def test_distro_attributes(
    distro_type: LinuxDistributionType,
    version: Version,
    raw_version: str,
    id_like: list[str],
) -> None:
    distro = Distro(
        type=distro_type,
        version=version,
        raw_version=raw_version,
        id_like=id_like,
    )
    assert distro.type == distro_type
    assert distro.version is not None
    assert distro.version == version
    assert distro.raw_version == raw_version
    assert distro.id_like == id_like


@pytest.mark.skims_test_group("sca_unittesting")
def test_distro_without_version() -> None:
    distro = Distro(
        type=LinuxDistributionType.WOLFI,
        version=None,
        raw_version="",
        id_like=[],
    )
    assert distro.version is None
    assert distro.full_version() == ""


@pytest.mark.skims_test_group("sca_unittesting")
@pytest.mark.parametrize(
    ("release_data", "expected"),
    [
        (
            {
                "id_": "ubuntu",
                "version_id": "20.04",
                "version": "20.04 LTS",
                "id_like": ["debian"],
            },
            Distro(
                type=LinuxDistributionType.UBUNTU,
                raw_version="20.04",
                id_like=["debian"],
                version=Version.coerce("20.04"),
            ),
        ),
        (
            {
                "id_": "debian",
                "version_id": "",
                "version": "",
                "pretty_name": "Debian GNU/Linux sid",
                "id_like": ["debian"],
            },
            Distro(
                type=LinuxDistributionType.DEBIAN,
                raw_version="unstable",
                id_like=["debian"],
                version=None,
            ),
        ),
        (
            {
                "id_": "unknown",
                "version_id": "1.0",
                "version": "1.0",
                "id_like": [],
            },
            None,
        ),
        (
            {
                "id_": "centos",
                "version_id": "",
                "version": "7",
                "id_like": ["rhel", "fedora"],
            },
            Distro(
                type=LinuxDistributionType.CENTOS,
                raw_version="7",
                id_like=["rhel", "fedora"],
                version=Version.coerce("7"),
            ),
        ),
    ],
)
def test_new_distro_from_release(release_data: dict[str, str], expected: Distro) -> None:
    result = new_distro_from_release(release_data)
    if expected is None:
        assert result is None
    else:
        assert result == expected
        assert result.type == expected.type
        assert result.raw_version == expected.raw_version
        assert result.id_like == expected.id_like
        assert result.version == expected.version


@pytest.mark.skims_test_group("sca_unittesting")
def test_new_distro_from_release_invalid_version() -> None:
    release_data = {
        "id_": "ubuntu",
        "version_id": "invalid",
        "version": "also invalid",
        "id_like": ["debian"],
    }
    result = new_distro_from_release(cast(dict[str, str], release_data))
    assert result is None


@pytest.mark.skims_test_group("sca_unittesting")
def test_new_distro_from_release_empty_data() -> None:
    release_data = {
        "id_": "",
        "version_id": "",
        "version": "",
        "id_like": [],
    }
    result = new_distro_from_release(cast(dict[str, str], release_data))
    assert result is None
