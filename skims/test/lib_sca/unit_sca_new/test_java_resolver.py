# noqa: INP001
import pytest
from cpe import (
    CPE,
)
from lib_sca.sca_new.db.sqlite_vunnel.pkg.resolver.from_language import (
    from_language,
)
from lib_sca.sca_new.db.sqlite_vunnel.pkg.resolver.java.resolver import (
    Resolver as JavaResolver,
)
from lib_sca.sca_new.db.sqlite_vunnel.pkg.resolver.python.resolver import (
    Resolver as PythonResolver,
)
from lib_sca.sca_new.db.sqlite_vunnel.pkg.resolver.stock.resolver import (
    Resolver as DefaultResolver,
)
from lib_sca.sca_new.pkg.package import (
    Language,
    Location,
    Package,
    PackageType,
)


@pytest.mark.skims_test_group("sca_unittesting")
def test_resolver_for_java() -> None:
    java_package = Package(
        id="1",
        name="sample-java-package",
        version="1.0.0",
        locations=[Location(path="/path/to/java/package")],
        language=Language.JAVA,
        licenses=["MIT"],
        type=PackageType.JAVA_PKG,
        purl="pkg:maven/sample-java-package@1.0.0",
        cpes=[CPE("cpe:/a:sample:java-package:1.0.0")],
        metadata={
            "virtual_path": "/path/to/sample-java-package-1.0.0.jar",
            "pom_artifact_id": "sample-java-package",
            "pom_group_id": "com.example",
            "manifest_name": "Sample-Java-Package",
        },
    )
    resolver = from_language(java_package.language)
    assert isinstance(resolver, JavaResolver)
    resolved = resolver.resolve(java_package)
    assert resolved[0] == "com.example:sample-java-package"


@pytest.mark.skims_test_group("sca_unittesting")
def test_resolver_for_java_invalid_purl() -> None:
    java_package = Package(
        id="1",
        name="sample-java-package",
        version="1.0.0",
        locations=[Location(path="/path/to/java/package")],
        language=Language.JAVA,
        licenses=["MIT"],
        type=PackageType.JAVA_PKG,
        purl="invalid",
        cpes=[CPE("cpe:/a:sample:java-package:1.0.0")],
        metadata={
            "virtual_path": "/path/to/sample-java-package-1.0.0.jar",
            "pom_artifact_id": "sample-java-package",
            "pom_group_id": "com.example",
            "manifest_name": "Sample-Java-Package",
        },
    )
    resolver = from_language(java_package.language)
    assert isinstance(resolver, JavaResolver)
    resolver.resolve(java_package)


@pytest.mark.skims_test_group("sca_unittesting")
def test_resolver_for_python() -> None:
    python_package = Package(
        id="2",
        name="sample-python-package",
        version="2.0.0",
        locations=[Location(path="/path/to/python/package")],
        language=Language.PYTHON,
        licenses=["Apache-2.0"],
        type=PackageType.PYTHON_PKG,
        purl="pkg:pypi/sample-python-package@2.0.0",
        cpes=[CPE("cpe:/a:sample:python-package:2.0.0")],
        metadata={"key": "value"},
    )
    resolver = from_language(python_package.language)
    assert isinstance(resolver, PythonResolver)


@pytest.mark.skims_test_group("sca_unittesting")
def test_resolver_for_unknown_language() -> None:
    unknown_package = Package(
        id="3",
        name="sample-unknown-package",
        version="3.0.0",
        locations=[Location(path="/path/to/unknown/package")],
        language=Language.UNKNOWN_LANGUAGE,
        licenses=["GPL-3.0"],
        type=PackageType.UNKNOWN_PKG,
        purl="pkg:generic/sample-unknown-package@3.0.0",
        cpes=[CPE("cpe:/a:sample:unknown-package:3.0.0")],
        metadata={"key": "value"},
    )
    resolver = from_language(unknown_package.language)
    assert isinstance(resolver, DefaultResolver)
