# noqa: INP001
import pytest
from lib_sca.sca_new.db.sqlite_vunnel.namespace.cpe.namespace import (
    Namespace,
)
from lib_sca.sca_new.db.sqlite_vunnel.namespace.cpe.namespace import (
    from_str as from_str_namespace,
)
from lib_sca.sca_new.db.sqlite_vunnel.namespace.distro.namespace import (
    from_str as from_str_distro,
)
from lib_sca.sca_new.db.sqlite_vunnel.namespace.language.namespace import (
    from_str as from_str_language,
)


@pytest.mark.skims_test_group("sca_unittesting")
def test_from_str_valid() -> None:
    result = from_str_namespace("provider:cpe")
    assert isinstance(result, Namespace)
    assert result.provider() == "provider"
    assert result.resolver() is not None
    assert result.string() == "provider:cpe"


@pytest.mark.skims_test_group("sca_unittesting")
def test_from_str_empty() -> None:
    with pytest.raises(ValueError, match="Empty namespace"):
        from_str_namespace("")


@pytest.mark.skims_test_group("sca_unittesting")
def test_from_str_invalid_components() -> None:
    with pytest.raises(ValueError, match="incorrect number of components"):
        from_str_namespace("provider:cpe:extra")


@pytest.mark.skims_test_group("sca_unittesting")
def test_from_str_unknown_provider() -> None:
    with pytest.raises(ValueError, match="unknown provider provider"):
        from_str_namespace("provider:unknown")


@pytest.mark.skims_test_group("sca_unittesting")
def test_from_str_valid_distro() -> None:
    result = from_str_distro("provider:distro:ubuntu:22.04")
    assert isinstance(result, Namespace)
    assert result.provider() == "provider"
    assert result.resolver() is not None
    assert result.string() == "provider:distro:ubuntu:22.04"


@pytest.mark.skims_test_group("sca_unittesting")
def test_from_str_invalid_distro_type() -> None:
    with pytest.raises(ValueError, match="is not a valid LinuxDistributionType"):
        from_str_distro("provider:distro:invalid:22.04")


@pytest.mark.skims_test_group("sca_unittesting")
def test_from_str_invalid_distro_empty() -> None:
    with pytest.raises(ValueError):  # noqa: PT011
        from_str_distro("")


@pytest.mark.skims_test_group("sca_unittesting")
def test_from_str_incorrect_id() -> None:
    with pytest.raises(ValueError, match="type cpe is incorrect"):
        from_str_distro("provider:cpe:ubuntu:22.04")


@pytest.mark.skims_test_group("sca_unittesting")
def test_from_str_missing_version() -> None:
    with pytest.raises(ValueError, match="incorrect number of components"):
        from_str_distro("provider:distro:ubuntu")


@pytest.mark.skims_test_group("sca_unittesting")
def test_from_str_extra_components() -> None:
    with pytest.raises(ValueError, match="incorrect number of components"):
        from_str_distro("provider:distro:ubuntu:22.04:extra")


@pytest.mark.skims_test_group("sca_unittesting")
def test_from_str_valid_language() -> None:
    result = from_str_language("provider:language:python")
    assert isinstance(result, Namespace)
    assert result.provider() == "provider"
    assert result.resolver() is not None
    assert result.string() == "provider:language:python"


@pytest.mark.skims_test_group("sca_unittesting")
def test_from_str_valid_language_with_package_type() -> None:
    result = from_str_language("provider:language:javascript:npm")
    assert isinstance(result, Namespace)
    assert result.provider() == "provider"
    assert result.resolver() is not None
    assert result.string() == "provider:language:javascript:npm"


@pytest.mark.skims_test_group("sca_unittesting")
def test_from_str_invalid_language() -> None:
    with pytest.raises(ValueError, match="is not a valid Language"):
        from_str_language("provider:language:invalid_language")


@pytest.mark.skims_test_group("sca_unittesting")
def test_from_str_invalid_package_type() -> None:
    with pytest.raises(ValueError, match="is not a valid PackageType"):
        from_str_language("provider:language:python:invalid_package_type")


@pytest.mark.skims_test_group("sca_unittesting")
def test_from_str_too_few_components() -> None:
    with pytest.raises(ValueError, match="incorrect number of components"):
        from_str_language("provider:language")


@pytest.mark.skims_test_group("sca_unittesting")
def test_from_str_too_many_components() -> None:
    with pytest.raises(ValueError, match="incorrect number of components"):
        from_str_language("provider:language:python:npm:extra")


@pytest.mark.skims_test_group("sca_unittesting")
def test_from_str_incorrect_id_language() -> None:
    with pytest.raises(ValueError, match="type cpe is incorrect"):
        from_str_language("provider:cpe:python")


@pytest.mark.skims_test_group("sca_unittesting")
def test_from_str_default_package_type() -> None:
    result = from_str_language("provider:language:ruby")
    assert isinstance(result, Namespace)
    assert result.provider() == "provider"
    assert result.resolver() is not None
    assert result.string() == "provider:language:ruby"
