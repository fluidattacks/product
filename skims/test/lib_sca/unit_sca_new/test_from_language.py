# noqa: INP001
import pytest
from lib_sca.sca_new.db.sqlite_vunnel.pkg.resolver.from_language import (
    from_language,
)
from lib_sca.sca_new.db.sqlite_vunnel.pkg.resolver.java.resolver import (
    Resolver as JavaResolver,
)
from lib_sca.sca_new.db.sqlite_vunnel.pkg.resolver.python.resolver import (
    Resolver as PythonResolver,
)
from lib_sca.sca_new.db.sqlite_vunnel.pkg.resolver.stock.resolver import (
    Resolver as DefaultResolver,
)
from lib_sca.sca_new.pkg.package import (
    Language,
)


@pytest.mark.skims_test_group("sca_unittesting")
def test_from_language_java() -> None:
    resolver = from_language(Language.JAVA)
    assert isinstance(resolver, JavaResolver)


@pytest.mark.skims_test_group("sca_unittesting")
def test_from_language_python() -> None:
    resolver = from_language(Language.PYTHON)
    assert isinstance(resolver, PythonResolver)


@pytest.mark.skims_test_group("sca_unittesting")
def test_from_language_default() -> None:
    resolver = from_language(Language.JAVASCRIPT)
    assert isinstance(resolver, DefaultResolver)
