# noqa: INP001
import pytest
from lib_sca.sca_new.version.version_operator import (
    Operator,
    parse_operator,
)


@pytest.mark.skims_test_group("sca_unittesting")
@pytest.mark.parametrize(
    ("input_operator", "expected_output"),
    [
        ("=", Operator.EQ),
        ("", Operator.EQ),
        (">", Operator.GT),
        (">=", Operator.GTE),
        ("<", Operator.LT),
        ("<=", Operator.LTE),
        ("||", Operator.OR),
        (",", Operator.AND),
    ],
)
def test_parse_operator_valid(input_operator: str, expected_output: Operator) -> None:
    assert parse_operator(input_operator) == expected_output


@pytest.mark.skims_test_group("sca_unittesting")
@pytest.mark.parametrize(
    "invalid_operator",
    [
        "!=",
        "~",
        "==",
        "&&",
    ],
)
def test_parse_operator_invalid(invalid_operator: str) -> None:
    with pytest.raises(ValueError, match=f"unknown operator: '{invalid_operator}'"):
        parse_operator(invalid_operator)


@pytest.mark.skims_test_group("sca_unittesting")
def test_operator_enum_values() -> None:
    assert Operator.EQ.value == "="
    assert Operator.GT.value == ">"
    assert Operator.LT.value == "<"
    assert Operator.GTE.value == ">="
    assert Operator.LTE.value == "<="
    assert Operator.OR.value == "||"
    assert Operator.AND.value == ","


@pytest.mark.skims_test_group("sca_unittesting")
def test_operator_enum_type() -> None:
    assert isinstance(Operator.EQ, Operator)
