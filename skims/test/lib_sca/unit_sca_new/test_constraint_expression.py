# noqa: INP001
import pytest
from lib_sca.sca_new.version.constraint_expression import (
    scan_expression,
)


@pytest.mark.skims_test_group("sca_unittesting")
@pytest.mark.parametrize(
    ("phrase", "expected"),
    [
        ("x,y||z", [["x", "y"], ["z"]]),
        ("1.0,2.0,3.0", [["1.0", "2.0", "3.0"]]),
        ("<1.0, >=2.0|| 3.0 || =4.0", [["<1.0", ">=2.0"], ["3.0"], ["=4.0"]]),
        (
            "(<1.0, >=2.0|| 3.0) || =4.0",
            None,
        ),
        (
            " > 1.0,  <=   2.0,,,    || = 3.0 ",
            [[">1.0", "<=2.0"], ["=3.0"]],
        ),
    ],
)
def test_single_version(phrase: str, expected: list[list[str]] | None) -> None:
    if expected is None:
        with pytest.raises(ValueError):  # noqa: PT011
            scan_expression(phrase)
        return
    result = scan_expression(phrase)
    assert result == expected


@pytest.mark.skims_test_group("sca_unittesting")
def test_multiple_versions_and() -> None:
    result = scan_expression("1.0,2.0,3.0")
    assert result == [["1.0", "2.0", "3.0"]]


@pytest.mark.skims_test_group("sca_unittesting")
def test_multiple_versions_or() -> None:
    result = scan_expression("1.0||2.0||3.0")
    assert result == [["1.0"], ["2.0"], ["3.0"]]


@pytest.mark.skims_test_group("sca_unittesting")
def test_complex_expression() -> None:
    result = scan_expression("1.0,>=2.0||<3.0,=4.0||5.0")
    assert result == [["1.0", ">=2.0"], ["<3.0", "=4.0"], ["5.0"]]


@pytest.mark.skims_test_group("sca_unittesting")
def test_whitespace_handling() -> None:
    result = scan_expression(" 1.0 , >=2.0 || <3.0 , =4.0 || 5.0 ")
    assert result == [["1.0", ">=2.0"], ["<3.0", "=4.0"], ["5.0"]]


@pytest.mark.skims_test_group("sca_unittesting")
def test_empty_input() -> None:
    result = scan_expression("")
    assert not result


@pytest.mark.skims_test_group("sca_unittesting")
def test_single_operator() -> None:
    result = scan_expression(">1.0")
    assert result == [[">1.0"]]


@pytest.mark.skims_test_group("sca_unittesting")
def test_multiple_operators() -> None:
    result = scan_expression(">1.0,<2.0,=3.0")
    assert result == [[">1.0", "<2.0", "=3.0"]]


@pytest.mark.skims_test_group("sca_unittesting")
def test_parentheses_error() -> None:
    with pytest.raises(ValueError, match="parenthetical expressions are not supported yet"):
        scan_expression("(1.0)")


@pytest.mark.skims_test_group("sca_unittesting")
def test_trailing_operator() -> None:
    result = scan_expression("1.0,>=")
    assert result == [["1.0", ">="]]


@pytest.mark.skims_test_group("sca_unittesting")
def test_leading_operator() -> None:
    result = scan_expression(">=,1.0")
    assert result == [[">=", "1.0"]]
