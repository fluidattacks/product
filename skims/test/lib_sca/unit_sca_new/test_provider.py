# noqa: INP001
import json
import tempfile

import pytest
from lib_sca.sca_new.distro.type import (
    LinuxDistributionType,
)
from lib_sca.sca_new.pkg.package import (
    Language,
    PackageType,
)
from lib_sca.sca_new.pkg.provider import (
    provide,
)


@pytest.mark.skims_test_group("sca_unittesting")
def test_provide_with_multiple_packages() -> None:
    sbom_data = {
        "packages": [
            {
                "id": "5755515152704602328",
                "name": "",
                "version": "",
                "locations": [{"path": "test/e2e/package-lock.json", "line": 6}],
                "licenses": [],
                "type": "npm",
                "language": "javascript",
                "package_url": None,
                "metadata": {"is_dev": False},
            },
            {
                "id": "5834740171416009162",
                "name": "@colors/colors",
                "version": "1.5.0",
                "locations": [
                    {"path": "test/e2e/package-lock.json", "line": 12},
                    {"path": "web/package-lock.json", "line": 24},
                    {"path": "front/package-lock.json", "line": 2574},
                ],
                "licenses": ["MIT"],
                "type": "npm",
                "language": "javascript",
                "package_url": "pkg:npm/%40colors/colors@1.5.0",
                "metadata": {
                    "resolved": ("https://registry.npmjs.org/@colors/colors/-/colors-1.5.0.tgz"),
                    "integrity": (
                        "sha512-ooWCrlZP11i8GImSjTHYHLkvFDP48nS4"
                        "+204nGb1RiX/WXYHmJA2III9/e2DWVabCESdW"
                        "7hBAEzHRqUn9OUVvQ=="
                    ),
                    "is_dev": True,
                },
            },
        ],
        "sbom_details": {
            "environment": {
                "linux_release": {
                    "id_": "ubuntu",
                    "version_id": "20.04",
                    "version": "20.04 LTS",
                    "id_like": "debian",
                },
            },
        },
    }

    with tempfile.NamedTemporaryFile(delete=False, mode="w", encoding="utf-8") as temp_file:
        json.dump(sbom_data, temp_file)
        temp_file_path = temp_file.name

    packages, _ = provide(temp_file_path)

    assert len(packages) == 2
    assert packages[0].id == "5755515152704602328"
    assert packages[1].id == "5834740171416009162"


@pytest.mark.skims_test_group("sca_unittesting")
def test_provide_with_no_packages() -> None:
    sbom_data = {
        "packages": [],
        "sbom_details": {
            "environment": {
                "linux_release": {
                    "id_": "ubuntu",
                    "version_id": "20.04",
                    "version": "20.04 LTS",
                    "id_like": "debian",
                },
            },
        },
    }

    with tempfile.NamedTemporaryFile(delete=False, mode="w", encoding="utf-8") as temp_file:
        json.dump(sbom_data, temp_file)
        temp_file_path = temp_file.name

    packages, context = provide(temp_file_path)

    assert len(packages) == 0
    assert context is not None
    assert context.distro.type == LinuxDistributionType.UBUNTU


@pytest.mark.skims_test_group("sca_unittesting")
def test_provide_with_no_context() -> None:
    sbom_data = {
        "packages": [
            {
                "id": "5755515152704602328",
                "name": "",
                "version": "",
                "locations": [{"path": "test/e2e/package-lock.json", "line": 6}],
                "licenses": [],
                "type": "npm",
                "language": "javascript",
                "package_url": None,
                "metadata": {"is_dev": False},
            },
        ],
        "sbom_details": {},
    }

    with tempfile.NamedTemporaryFile(delete=False, mode="w", encoding="utf-8") as temp_file:
        json.dump(sbom_data, temp_file)
        temp_file_path = temp_file.name

    packages, context = provide(temp_file_path)

    assert len(packages) == 1
    assert packages[0].id == "5755515152704602328"
    assert context is None


@pytest.mark.skims_test_group("sca_unittesting")
def test_provide_with_unknown_language_and_type() -> None:
    sbom_data = {
        "packages": [
            {
                "id": "5755515152704602328",
                "name": "",
                "version": "",
                "locations": [{"path": "test/e2e/package-lock.json", "line": 6}],
                "licenses": [],
                "type": "UnknownPackage",
                "language": "",
                "package_url": None,
                "metadata": {"is_dev": False},
            },
        ],
        "sbom_details": {
            "environment": {
                "linux_release": {
                    "id_": "ubuntu",
                    "version_id": "20.04",
                    "version": "20.04 LTS",
                    "id_like": "debian",
                },
            },
        },
    }

    with tempfile.NamedTemporaryFile(delete=False, mode="w", encoding="utf-8") as temp_file:
        json.dump(sbom_data, temp_file)
        temp_file_path = temp_file.name

    packages, _ = provide(temp_file_path)

    assert len(packages) == 1
    assert packages[0].id == "5755515152704602328"
    assert packages[0].language.value == Language.UNKNOWN_LANGUAGE.value
    assert packages[0].type.value == PackageType.UNKNOWN_PKG.value
