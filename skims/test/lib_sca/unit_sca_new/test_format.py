# noqa: INP001
from enum import (
    Enum,
)

import pytest
from lib_sca.sca_new.pkg.package import (
    PackageType,
)
from lib_sca.sca_new.version.format import (
    VersionFormat,
    format_to_string,
    parse_format,
    version_format_from_pkg_type,
)


@pytest.mark.skims_test_group("sca_unittesting")
@pytest.mark.parametrize(
    ("input_str", "expected_format"),
    [
        ("semantic", VersionFormat.SEMANTIC),
        ("semver", VersionFormat.SEMANTIC),
        ("apk", VersionFormat.APK),
        ("dpkg", VersionFormat.DEB),
        ("go", VersionFormat.GOLANG),
        ("maven", VersionFormat.MAVEN),
        ("rpm", VersionFormat.RPM),
        ("python", VersionFormat.PYTHON),
        ("kb", VersionFormat.KB),
        ("gem", VersionFormat.GEM),
        ("portage", VersionFormat.PORTAGE),
        ("unknown", VersionFormat.UNKNOWN),
    ],
)
def test_parse_format(input_str: str, expected_format: VersionFormat) -> None:
    assert parse_format(input_str) == expected_format


@pytest.mark.skims_test_group("sca_unittesting")
@pytest.mark.parametrize(
    ("pkg_type", "expected_format"),
    [
        (PackageType.APK_PKG, VersionFormat.APK),
        (PackageType.DEB_PKG, VersionFormat.DEB),
        (PackageType.JAVA_PKG, VersionFormat.MAVEN),
        (PackageType.RPM_PKG, VersionFormat.RPM),
        (PackageType.GEM_PKG, VersionFormat.GEM),
        (PackageType.PYTHON_PKG, VersionFormat.PYTHON),
        (PackageType.KB_PKG, VersionFormat.KB),
        (PackageType.PORTAGE_PKG, VersionFormat.PORTAGE),
        (PackageType.GO_MODULE_PKG, VersionFormat.GOLANG),
        (PackageType.UNKNOWN_PKG, VersionFormat.UNKNOWN),
    ],
)
def test_version_format_from_pkg_type(
    pkg_type: PackageType,
    expected_format: VersionFormat,
) -> None:
    assert version_format_from_pkg_type(pkg_type) == expected_format


@pytest.mark.skims_test_group("sca_unittesting")
@pytest.mark.parametrize(
    ("version_format", "expected_string"),
    [
        (VersionFormat.SEMANTIC, "semantic"),
        (VersionFormat.APK, "apk"),
        (VersionFormat.DEB, "deb"),
        (VersionFormat.MAVEN, "maven"),
        (VersionFormat.RPM, "rpm"),
        (VersionFormat.PYTHON, "python"),
        (VersionFormat.KB, "kb"),
        (VersionFormat.GEM, "gem"),
        (VersionFormat.PORTAGE, "portage"),
        (VersionFormat.GOLANG, "go"),
        (VersionFormat.UNKNOWN, "unknownformat"),
    ],
)
def test_format_to_string(version_format: VersionFormat, expected_string: str) -> None:
    assert format_to_string(version_format) == expected_string


@pytest.mark.skims_test_group("sca_unittesting")
def test_format_to_string_invalid_input() -> None:
    class InvalidEnum:
        pass

    assert format_to_string(InvalidEnum()) == "UnknownFormat"  # type: ignore[arg-type]


@pytest.mark.skims_test_group("sca_unittesting")
@pytest.mark.parametrize(
    ("v_format", "expected_string"),
    [
        (VersionFormat.SEMANTIC, "semantic"),
        (VersionFormat.APK, "apk"),
        (VersionFormat.DEB, "deb"),
        (VersionFormat.MAVEN, "maven"),
        (VersionFormat.RPM, "rpm"),
        (VersionFormat.PYTHON, "python"),
        (VersionFormat.KB, "kb"),
        (VersionFormat.GEM, "gem"),
        (VersionFormat.PORTAGE, "portage"),
        (VersionFormat.GOLANG, "go"),
        (VersionFormat.UNKNOWN, "unknownformat"),
        (None, "UnknownFormat"),
        ("InvalidString", "UnknownFormat"),
        (123, "UnknownFormat"),
        ([], "UnknownFormat"),
        ({}, "UnknownFormat"),
    ],
)
def test_format_to_string_extended(v_format: VersionFormat, expected_string: str) -> None:
    assert format_to_string(v_format) == expected_string


@pytest.mark.skims_test_group("sca_unittesting")
def test_format_to_string_custom_enum() -> None:
    class CustomEnum(Enum):
        CUSTOM = "custom"

    assert format_to_string(CustomEnum.CUSTOM) == "UnknownFormat"  # type: ignore[arg-type]


@pytest.mark.skims_test_group("sca_unittesting")
def test_format_to_string_non_enum_class() -> None:
    class NonEnum:
        def __init__(self, value: str):  # noqa: ANN204
            self.value = value

    assert format_to_string(NonEnum("test")) == "UnknownFormat"  # type: ignore[arg-type]


@pytest.mark.skims_test_group("sca_unittesting")
def test_format_to_string_none() -> None:
    assert format_to_string(None) == "UnknownFormat"  # type: ignore[arg-type]
