# noqa: INP001
import pytest
from lib_sca.model import (
    Advisory,
)
from lib_sca.schedulers.repositories.advisory_database import (
    get_advisory_database,
    get_final_range,
    get_limit,
    spread_affected_packages,
)


@pytest.mark.skims_test_group("sca_unittesting")
@pytest.mark.parametrize(
    ("events", "db_specific", "expected"),
    [
        (
            [{"introduced": "1.0.2"}, {"last_affected": "1.2.4"}],
            None,
            " <=1.2.4",
        ),
        (
            [{"introduced": "1.3.5"}, {"fixed": "2.3.6"}],
            None,
            " <2.3.6",
        ),
        ([{"introduced": "1.0.2"}], None, ""),
        (
            [{"introduced": "1.0.2"}],
            {"last_known_affected_version_range": "< 2.3.6"},
            " <2.3.6",
        ),
    ],
)
def test_get_limit(events: list[dict[str, str]], db_specific: dict | None, expected: str) -> None:
    dict_test: dict = {
        "package": {"ecosystem": "npm", "name": "vega"},
        "ranges": [
            {
                "type": "ECOSYSTEM",
                "events": events,
            },
        ],
    }
    if db_specific:
        dict_test["database_specific"] = db_specific
    formated_range: str = get_limit(events, dict_test)
    assert formated_range == expected


@pytest.mark.skims_test_group("sca_unittesting")
@pytest.mark.parametrize(
    ("obj_pkg", "expected"),
    [
        (
            {
                "package": {"ecosystem": "crates.io", "name": "gfx-auxil"},
                "ranges": [
                    {
                        "type": "ECOSYSTEM",
                        "events": [
                            {"introduced": "0"},
                            {"last_affected": "0.10.0"},
                        ],
                    },
                ],
            },
            ">=0 <=0.10.0",
        ),
        (
            {
                "package": {"ecosystem": "crates.io", "name": "simple_asn1"},
                "ranges": [
                    {
                        "type": "ECOSYSTEM",
                        "events": [
                            {"introduced": "0.6.0"},
                            {"fixed": "0.6.1"},
                        ],
                    },
                ],
                "versions": ["0.6.0"],
            },
            "=0.6.0 || >=0.6.0 <0.6.1",
        ),
        (
            {
                "package": {"ecosystem": "RubyGems", "name": "activerecord"},
                "ecosystem_specific": {"affected_functions": [""]},
                "ranges": [
                    {
                        "type": "ECOSYSTEM",
                        "events": [
                            {"introduced": "3.1.0"},
                        ],
                    },
                ],
                "database_specific": {"last_known_affected_version_range": "<= 3.2.22.0"},
            },
            ">=3.1.0 <=3.2.22.0",
        ),
        (
            {
                "package": {"ecosystem": "crates.io", "name": "simple_asn1"},
                "ranges": [
                    {
                        "type": "ECOSYSTEM",
                        "events": [
                            {"introduced": "0.6.0"},
                            {"fixed": "0.6.1"},
                        ],
                    },
                    {
                        "type": "ECOSYSTEM",
                        "events": [
                            {"introduced": "1.6.0"},
                            {"fixed": "2.6.1"},
                        ],
                    },
                ],
            },
            ">=0.6.0 <0.6.1 || >=1.6.0 <2.6.1",
        ),
    ],
)
def test_get_final_range(obj_pkg: dict, expected: str) -> None:
    formated_range: str = get_final_range(obj_pkg)
    assert formated_range == expected


@pytest.mark.skims_test_group("sca_unittesting")
@pytest.mark.parametrize(
    ("affected", "expected"),
    [
        (
            [
                {
                    "package": {"ecosystem": "npm", "name": "vega"},
                    "ranges": [
                        {
                            "type": "ECOSYSTEM",
                            "events": [
                                {"introduced": "0"},
                                {"fixed": "5.23.0"},
                            ],
                        },
                    ],
                },
            ],
            {"vega": {"platform": "npm", "version": ">=0 <5.23.0"}},
        ),
        (
            [
                {
                    "package": {"ecosystem": "Packagist", "name": "php_pkg"},
                    "ranges": [
                        {
                            "type": "ECOSYSTEM",
                            "events": [
                                {"introduced": "1.0.1"},
                                {"last_affected": "2.24.5"},
                            ],
                        },
                    ],
                },
                {
                    "package": {"ecosystem": "Packagist", "name": "php_pkg"},
                    "ranges": [
                        {
                            "type": "ECOSYSTEM",
                            "events": [
                                {"introduced": "3.0.5"},
                                {"fixed": "4.0.8"},
                            ],
                        },
                    ],
                },
            ],
            {
                "php_pkg": {
                    "platform": "packagist",
                    "version": ">=1.0.1 <=2.24.5 || >=3.0.5 <4.0.8",
                },
            },
        ),
    ],
)
def test_spread_affected_packages(affected: list[dict], expected: dict) -> None:
    affected_packages = spread_affected_packages(affected)
    assert affected_packages == expected


@pytest.mark.skims_test_group("sca_unittesting")
def test_get_advisory_database() -> None:
    expected: list[Advisory] = [
        Advisory(
            id="CVE-2022-36901",
            package_manager="maven",
            package_name="org.jenkins-ci.plugins:http_request",
            severity="CVSS:3.1/AV:L/AC:L/PR:L/UI:N/S:U/C:L/I:N/A:N",
            severity_v4=None,
            source=(
                "https://github.com/github/advisory-database/blob/main/"
                "advisories/github-reviewed/GHSA-2qh6-hhvv-m2ww.json"
            ),
            vulnerable_version=">=0 <1.16",
            cwe_ids=["CWE-256", "CWE-668"],
            created_at="2022-07-28T00:00:42Z",
            modified_at="2022-12-09T17:01:35Z",
            alternative_id="GHSA-2qh6-hhvv-m2ww",
            details="Jenkins HTTP Request stores HTTP Request passwords unencrypted\n",
        ),
        Advisory(
            id="CVE-2020-11973",
            package_manager="maven",
            package_name="org.apache.camel:camel-netty",
            severity="CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:H/A:H",
            severity_v4=None,
            source=(
                "https://github.com/github/advisory-database/blob/main/"
                "advisories/github-reviewed/GHSA-h79p-32mx-fjj9.json"
            ),
            vulnerable_version=">=3.0.0 <3.2.0",
            cwe_ids=["CWE-502"],
            created_at="2020-05-21T21:09:04Z",
            modified_at="2022-10-06T18:15:00Z",
            alternative_id="GHSA-h79p-32mx-fjj9",
            details=(
                "Apache Camel Netty enables Java deserialization by default\n"
                "Apache Camel Netty enables Java deserialization by default."
            ),
        ),
    ]
    advisories = get_advisory_database(set(), "skims/test/lib_sca/data/scheduler_mocks")
    assert advisories == expected
