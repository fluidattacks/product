# noqa: INP001
from pathlib import Path
from typing import (
    NamedTuple,
)

import pytest
from lib_sca.model import (
    Advisory,
)
from lib_sca.schedulers.repositories.open_osv import (
    PLATFORMS,
    download_platform_advisories_from_ovs,
    get_osv_advisories,
)
from pytest_mock import (
    MockerFixture,
)

OSV_MODULE = "lib_sca.schedulers.repositories.open_osv"


class MockResponse(NamedTuple):
    status_code: int
    content: bytes


@pytest.mark.skims_test_group("sca_unittesting")
def test_get_osv_advisories(mocker: MockerFixture) -> None:
    zip_file_path = "skims/test/lib_sca/data/scheduler_mocks/test_osv/osv_mock_advisories.zip"

    with Path(zip_file_path).open("rb") as file:
        zip_file = file.read()

    response_instance = MockResponse(
        status_code=200,
        content=zip_file,
    )
    mocker.patch(
        f"{OSV_MODULE}.requests.get",
        return_value=response_instance,
    )
    advisories = download_platform_advisories_from_ovs("pypi", set())
    assert advisories == [
        Advisory(
            id="CVE-2013-7323",
            package_name="python-gnupg",
            package_manager="pip",
            vulnerable_version=">=0 <0.3.5",
            source="https://osv-vulnerabilities",
            created_at="2014-06-09T19:55:00Z",
            modified_at="2023-11-08T03:57:28.630422Z",
            details=(
                "python-gnupg before 0.3.5 allows context-dependent attackers to "
                "execute arbitrary commands via shell metacharacters in unspecified vectors."
            ),
        ),
        Advisory(
            id="CVE-2012-5578",
            package_name="keyring",
            package_manager="pip",
            vulnerable_version=">=0 <0.10.1",
            source="https://osv-vulnerabilities",
            created_at="2019-11-25T13:15:00Z",
            modified_at="2023-11-08T03:57:10.093723Z",
            details=(
                "Python keyring has insecure permissions on new databases allowing "
                "world-readable files to be created"
            ),
        ),
    ]


@pytest.mark.skims_test_group("sca_unittesting")
def test_get_osv_advisories_full(mocker: MockerFixture) -> None:
    zip_file_path = "skims/test/lib_sca/data/scheduler_mocks/test_osv/osv_mock_advisories.zip"

    with Path(zip_file_path).open("rb") as file:
        zip_file = file.read()

    response_instance = MockResponse(
        status_code=200,
        content=zip_file,
    )
    mocker.patch(
        f"{OSV_MODULE}.requests.get",
        return_value=response_instance,
    )
    advisories = get_osv_advisories(set())
    assert len(advisories) == len(PLATFORMS) * 2
