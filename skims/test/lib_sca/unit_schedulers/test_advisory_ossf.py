# noqa: INP001
import pytest
from lib_sca.model import (
    Advisory,
)
from lib_sca.schedulers.repositories.open_ssf import (
    get_advisory_ossf,
)


@pytest.mark.skims_test_group("sca_unittesting")
def test_get_advisory_ossf() -> None:
    expected: list[Advisory] = [
        Advisory(
            id="MAL-2022-595",
            package_manager="npm",
            package_name="@snapp/framework",
            source="https://github.com/ossf/malicious-packages",
            vulnerable_version=">=0",
            cwe_ids=["CWE-506"],
            created_at="2022-06-20T20:25:19Z",
            modified_at="2022-06-20T20:25:19Z",
            details="Malicious code in @snapp/framework (npm)",
            cve_finding="F448",
            auto_approve=False,
        ),
        Advisory(
            id="MAL-2023-1579",
            package_manager="pip",
            package_name="aiohttpp",
            source="https://github.com/ossf/malicious-packages",
            vulnerable_version=">=0",
            cwe_ids=["CWE-506"],
            created_at="2023-02-10T20:48:06Z",
            modified_at="2023-08-21T20:12:58Z",
            details="Malicious code in aiohttpp (pypi)",
            cve_finding="F448",
            auto_approve=False,
        ),
    ]
    advisories = get_advisory_ossf(set(), "skims/test/lib_sca/data/scheduler_mocks")
    assert advisories == expected
