# noqa: INP001
from collections.abc import Callable
from pathlib import Path
from typing import (
    NamedTuple,
)

import pytest
from lib_sca.model import (
    Advisory,
)
from lib_sca.schedulers.repositories.epss_scores import (
    add_epss_scores,
)
from pytest_mock import (
    MockerFixture,
)

EPSS_MODULE = "lib_sca.schedulers.repositories.epss_scores"


class MockResponse(NamedTuple):
    status_code: int
    content: bytes
    raise_for_status: Callable


TEST_ADVISORIES: list[Advisory] = [
    Advisory(
        id="CVE-2023-26346",
        package_name="python-gnupg",
        package_manager="pip",
        vulnerable_version=">=0 <0.3.5",
        source="fluidattacks.com",
    ),
    Advisory(
        id="CVE-2023-2635",
        package_name="keyring",
        package_manager="pip",
        vulnerable_version=">=0 <0.10.1",
        source="fluidattacks.com",
    ),
    Advisory(
        id="GHSA-28p5-7rg4-8v99",
        package_name="gfx-auxil",
        package_manager="cargo",
        vulnerable_version=">=0 <0.10.1",
        source="fluidattacks.com",
        alternative_id="CVE-2023-26353",
    ),
]


def raise_mock() -> bool:
    return True


@pytest.mark.skims_test_group("sca_unittesting")
def test_add_epss_scores(mocker: MockerFixture) -> None:
    with Path("skims/test/lib_sca/data/scheduler_mocks/epss/test_epss.csv.gz").open("rb") as file:
        gzip_csv = file.read()

    response_instance = MockResponse(status_code=200, content=gzip_csv, raise_for_status=raise_mock)
    mocker.patch(f"{EPSS_MODULE}.requests.get", return_value=response_instance)

    advisories_with_epss = add_epss_scores(TEST_ADVISORIES)
    epss_list = [adv.epss for adv in advisories_with_epss]
    assert epss_list == ["0.00086", "0.00045", "0.00086"]
