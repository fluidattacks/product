# noqa: INP001
import sqlite3
from pathlib import (
    Path,
)

import pytest
from lib_sca.model import (
    Advisory,
)
from lib_sca.schedulers.create_adv_db import (
    create_advisory_db,
)


@pytest.mark.skims_test_group("sca_unittesting")
def test_create_advisory_db() -> None:
    advisories = {
        "npm": {
            "test_pkg_1": {
                "GHSA-4gj": Advisory(
                    id="GHSA-4gj",
                    package_name="test_pkg_1",
                    severity="CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:C/C:H/I:H/A:H",
                    severity_v4="CVSS:4.0/AV:N/AC:L/PR:N/AT:N/UI:N/VC:H/VI:H/VA:H/SC:L/SI:L/SA:H",
                    package_manager="npm",
                    vulnerable_version="=2.0.0",
                    source="test_source",
                    created_at="test_date",
                    modified_at="test_date",
                    cwe_ids=["CWE-22", "CWE-345"],
                    alternative_id="CVE-002",
                ),
                "CVE-2023": Advisory(
                    id="CVE-2023",
                    package_name="test_pkg_1",
                    severity="CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:C/C:H/I:H/A:H",
                    severity_v4="CVSS:4.0/AV:N/AC:L/PR:N/AT:N/UI:N/VC:H/VI:H/VA:H/SC:L/SI:L/SA:H",
                    package_manager="npm",
                    vulnerable_version="<1.0.0",
                    source="test_source",
                    created_at="test_date",
                    modified_at="test_date",
                    cwe_ids=["CWE-22", "CWE-345"],
                    alternative_id="CVE-002",
                ),
            },
            "test_pkg_2": {
                "CVE-2024": Advisory(
                    id="CVE-2024",
                    package_name="test_pkg_1",
                    severity="CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:C/C:H/I:H/A:H",
                    severity_v4="CVSS:4.0/AV:N/AC:L/PR:N/AT:N/UI:N/VC:H/VI:H/VA:H/SC:L/SI:L/SA:H",
                    package_manager="npm",
                    vulnerable_version=">0",
                    source="test_source",
                    created_at="test_date",
                    modified_at="test_date",
                    cwe_ids=["CWE-22", "CWE-345"],
                    alternative_id=None,
                ),
            },
        },
        "pypi": {
            "test_pkg_3_malware": {
                "MAL-2022-01": Advisory(
                    id="MAL-2022-01",
                    package_name="test_pkg_3_malware",
                    severity="CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:C/C:H/I:H/A:H",
                    severity_v4="CVSS:4.0/AV:N/AC:L/PR:N/AT:N/UI:N/VC:H/VI:H/VA:H/SC:L/SI:L/SA:H",
                    package_manager="pypi",
                    vulnerable_version=">0",
                    source="test_source",
                    created_at="test_date",
                    modified_at="test_date",
                    cwe_ids=["CWE-22"],
                    alternative_id=None,
                ),
            },
        },
    }
    db_path = "./skims/test/outputs/sca_vulnerabilities.db"
    if Path(db_path).is_file():
        Path(db_path).unlink()

    create_advisory_db(
        advisories=advisories,
        cve_findings={"GHSA-4gj": "F211"},
        db_path=db_path,
    )
    conn = sqlite3.connect(db_path)
    cursor = conn.cursor()
    cursor.execute(
        """
        SELECT
            adv_id,
            vulnerable_version,
            cve_finding
        FROM advisories
        WHERE package_manager = ? AND package_name = ?;
        """,
        ("npm", "test_pkg_1"),
    )
    assert cursor.fetchall() == [("GHSA-4gj", "=2.0.0", "F211"), ("CVE-2023", "<1.0.0", None)]

    cursor.execute(
        """
        SELECT
            adv_id,
            vulnerable_version,
            cve_finding
        FROM advisories
        WHERE package_manager = ? AND package_name = ?;
        """,
        ("pypi", "test_pkg_3_malware"),
    )
    assert cursor.fetchall() == [("MAL-2022-01", ">0", "F448")]

    conn.close()
