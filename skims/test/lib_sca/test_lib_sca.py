# noqa: INP001
import pytest

from test.utils import (
    run_skims_group,
)

CONFIG_PATH = "skims/test/lib_sca/test_configs"
RESULTS_PATH = "skims/test/lib_sca/results"


@pytest.mark.skims_test_group("sca_findings")
def test_sca_findings() -> None:
    run_skims_group("lib_sca", CONFIG_PATH, RESULTS_PATH)


@pytest.mark.skims_test_group("sca_findings_new")
def test_sca_findings_new() -> None:
    run_skims_group("lib_sca_new", CONFIG_PATH, RESULTS_PATH)
