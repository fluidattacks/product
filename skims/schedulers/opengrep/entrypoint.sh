# shellcheck shell=bash

function main {
  sops_export_vars "skims/schedulers/opengrep/secrets/dev.yaml" GOOGLE_SHEETS_ID

  GOOGLE_SHEETS_TOKEN="$(sops --decrypt __argGoogleSheetsToken__)"
  export GOOGLE_SHEETS_TOKEN

  python3 "skims/skims/lib_sast/open_grep_security_rules/open_grep_security_rules.py"
}

main "${@}"
