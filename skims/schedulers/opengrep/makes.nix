{ inputs, makeScript, outputs, projectPath, ... }: {
  jobs."/skims/sast/opengrep" = makeScript {
    replace = {
      __argGoogleSheetsToken__ = projectPath
        "/skims/schedulers/opengrep/secrets/gs_secret_encrypted.json";
    };
    name = "skims-opengrep";
    searchPaths = {
      bin = [ inputs.nixpkgs.python311 inputs.nixpkgs.sops outputs."/skims" ];
      source = [ outputs."/common/utils/sops" outputs."/skims/config/runtime" ];
    };
    entrypoint = ./entrypoint.sh;
  };
}
