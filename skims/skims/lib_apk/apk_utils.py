import os
import shutil
import subprocess
import time
import zipfile
from collections.abc import (
    Callable,
)
from functools import (
    wraps,
)
from pathlib import (
    Path,
)
from typing import (
    Any,
    NamedTuple,
)

import ctx
import lxml.etree  # nosec
from androguard.core.analysis.analysis import (
    Analysis,
)
from androguard.core.apk import (
    APK,
)
from androguard.core.dex import (
    DEX,
)
from androguard.decompiler.decompiler import (
    DecompilerDAD,
)
from bs4 import (
    BeautifulSoup,
)
from lib_apk.methods_enum import (
    MethodsEnumAPK as MethodsEnum,
)
from model.core import (
    LocalesEnum,
    MethodExecutionResult,
    Vulnerabilities,
)
from utils.build_vulns import (
    build_inputs_vuln,
    build_metadata,
)
from utils.function import (
    shield_blocking,
)
from utils.logs import (
    log_blocking,
)
from utils.statics import (
    calculate_time_coefficient,
)
from utils.translations import (
    t,
)


class APKContext(NamedTuple):
    path: str
    apk_obj: APK
    apk_manifest: BeautifulSoup | None
    analysis: Analysis | None
    java_source_path: str | None


class APKBytecodesCtx(NamedTuple):
    apk_path: str
    apk_obj: APK
    analysis: Analysis | None


class APKManifestCtx(NamedTuple):
    apk_path: str
    apk_manifest: BeautifulSoup
    target_sdk: int


class Location(NamedTuple):
    description: str
    snippet: str
    vuln_line: str | None = None
    details: str | None = None


class Locations(NamedTuple):
    locations: list[Location]

    def append(
        self,
        method: MethodsEnum,
        snippet: str,
        **desc_kwargs: LocalesEnum | str,
    ) -> None:
        self.locations.append(
            Location(
                description=t(
                    method.name,
                    **desc_kwargs,
                ),
                snippet=snippet,
            ),
        )


def create_vulns(
    apk_path: str,
    locations: Locations,
    method: MethodsEnum,
    execution_time: float,
) -> MethodExecutionResult:
    vuln_what = apk_path.rsplit("/", 1)[-1]

    results: Vulnerabilities = tuple(
        build_inputs_vuln(
            method=method.value,
            stream="skims",
            what=vuln_what,
            where=location.description,
            metadata=build_metadata(
                method=method.value,
                description=(f"{location.description} {t(key='words.in')} {vuln_what}"),
                snippet=location.snippet,
            ),
        )
        for location in locations.locations
    )

    return MethodExecutionResult(
        method_name=method.value.name,
        time_coefficient=calculate_time_coefficient(execution_time, apk_path, 1),
        vulnerabilities=results,
    )


def _remove_dirs_except_excluded(root: str, dirs: list, dir_exclude: str) -> None:
    for name in dirs:
        dir_path = os.path.join(root, name)  # noqa: PTH118
        if dir_path.startswith(dir_exclude):
            continue
        if not dir_exclude.startswith(dir_path):
            shutil.rmtree(dir_path, ignore_errors=True)


def _remove_files_except_excluded(root: str, files: list, dir_exclude: str) -> None:
    for name in files:
        file_path = os.path.join(root, name)  # noqa: PTH118
        if dir_exclude not in file_path:
            Path(file_path).unlink()


def _clean_apk_out_path_specific(dir_base: str, dir_exclude: str) -> None:
    try:
        for root, dirs, files in os.walk(dir_base, topdown=False):
            root_full_path = os.path.join(dir_base, "resources")  # noqa: PTH118
            if root_full_path in root and "AndroidManifest.xml" not in files:
                continue
            if "resources" in dirs:
                dirs.remove("resources")
            _remove_files_except_excluded(root, files, dir_exclude)
            _remove_dirs_except_excluded(root, dirs, dir_exclude)
    except FileNotFoundError:
        msg = "Race conditions caused an excluded file to not exist"
        log_blocking("debug", msg)


def apk_2_java(apk_path: str, package_name: str) -> str | None:
    try:
        apk_name = Path(apk_path).stem
        output = os.path.join(  # noqa: PTH118
            Path.cwd(),
            "apk_files",
            apk_name,
        )
        os.makedirs(apk_name, exist_ok=True)  # noqa: PTH103
        output = os.path.relpath(output, Path.cwd())
        args = [
            "jadx",
            "-ds",
            output,
            "--quiet",
            "--show-bad-code",
            apk_path,
        ]

        with subprocess.Popen(  # noqa: S603
            args,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
        ) as proc:
            _, stderr = proc.communicate(None, 600)

            if proc.returncode != 0:
                log_blocking("error", "Error in jadx execution: %s", stderr.decode())
                return None

        specific_output = os.path.join(  # noqa: PTH118
            output,
            os.path.join(*(package_name.split("."))),  # noqa: PTH118
        )
        if os.path.isdir(specific_output):  # noqa: PTH112
            _clean_apk_out_path_specific(output, specific_output)
            output = specific_output

    except subprocess.TimeoutExpired:
        log_blocking("exception", "Decompiling with jadx timed out")

    return output


def get_apk_context(path: str) -> APKContext | None:
    apk_obj: APK | None = None
    apk_manifest: BeautifulSoup | None = None
    analysis: Analysis | None = None
    try:
        apk_obj = APK(path)

        apk_manifest_data = apk_obj.get_android_manifest_xml()
        if apk_manifest_data:
            mark_up = lxml.etree.tostring(apk_manifest_data)
            apk_manifest = BeautifulSoup(
                BeautifulSoup(mark_up, features="html.parser").prettify(),
                features="html.parser",
            )
        dalviks = []
        analysis = Analysis()
        api_target = apk_obj.get_target_sdk_version()
        for dex in apk_obj.get_all_dex():
            dalvik = DEX(dex, using_api=api_target)
            analysis.add(dalvik)
            dalviks.append(dalvik)
            dalvik.set_decompiler(DecompilerDAD(dalviks, analysis))

        analysis.create_xref()
        if ctx.SKIMS_CONFIG.apk.use_sast_analysis:
            apk_source_path = apk_2_java(path, apk_obj.get_package())
        else:
            apk_source_path = None
        return APKContext(
            analysis=analysis,
            apk_manifest=apk_manifest,
            apk_obj=apk_obj,
            path=path,
            java_source_path=apk_source_path,
        )
    except (zipfile.BadZipFile, KeyError, ValueError) as exc:
        log_blocking("error", "Unable to decompile APK due to: %s", exc)
    return None


def get_bytecodes_ctx(apk_ctx: APKContext) -> APKBytecodesCtx:
    return APKBytecodesCtx(
        apk_path=apk_ctx.path,
        apk_obj=apk_ctx.apk_obj,
        analysis=apk_ctx.analysis,
    )


def get_manifest_ctx(apk_ctx: APKContext) -> APKManifestCtx | None:
    if not apk_ctx.apk_manifest:
        return None

    return APKManifestCtx(
        apk_path=apk_ctx.path,
        apk_manifest=apk_ctx.apk_manifest,
        target_sdk=get_target_sdk(apk_ctx.apk_obj),
    )


def get_target_sdk(apk_obj: APK) -> int:
    target_sdk_version: str = apk_obj.get_target_sdk_version() or apk_obj.get_min_sdk_version()
    try:
        target_sdk = int(target_sdk_version)
    except ValueError:
        target_sdk = 26

    return target_sdk


def calculate_execution_time(
    func: Callable,
) -> Callable[..., tuple[Locations, float]]:
    @wraps(func)
    def wrapper(*args: Any, **kwargs: Any) -> tuple[Locations, float]:  # noqa: ANN401
        start_time = time.time()
        result: Locations = func(*args, **kwargs)
        execution_time: float = time.time() - start_time
        return result, execution_time

    return wrapper


SHIELD_BLOCKING: Callable[[Callable], Callable] = shield_blocking(
    on_error_return=MethodExecutionResult(vulnerabilities=()),
)
