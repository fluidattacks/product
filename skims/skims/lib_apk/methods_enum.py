from enum import (
    Enum,
)

from model.core import (
    DeveloperEnum,
    FindingEnum,
    MethodOriginEnum,
    VulnerabilityTechnique,
)
from utils.method_enums import get_method_info_interface

dast_apk_method_info = get_method_info_interface(
    module="lib_apk",
    technique=VulnerabilityTechnique.DAST,
)


class MethodsEnumAPK(Enum):
    APK_BACKUPS_ENABLED = dast_apk_method_info(
        file_name="analyze_bytecodes",
        name="apk_backups_enabled",
        finding=FindingEnum.F055,
        developer=DeveloperEnum.BRIAM_AGUDELO,
        origin=MethodOriginEnum.ASSERTS,
        auto_approve=True,
    )
    APK_DEBUGGING_ENABLED = dast_apk_method_info(
        file_name="analyze_bytecodes",
        name="apk_debugging_enabled",
        finding=FindingEnum.F058,
        developer=DeveloperEnum.BRIAM_AGUDELO,
        origin=MethodOriginEnum.ASSERTS,
        auto_approve=True,
    )
    NOT_VERIFIES_SSL_HOSTNAME = dast_apk_method_info(
        file_name="analyze_bytecodes",
        name="not_verifies_ssl_hostname",
        finding=FindingEnum.F060,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        origin=MethodOriginEnum.ASSERTS,
        cvss="CVSS:3.1/AV:N/AC:L/PR:L/UI:N/S:U/C:L/I:L/A:N/E:U/RL:O/RC:U",
        cvss_v4=("CVSS:4.0/AV:N/AC:L/AT:N/PR:L/UI:N/VC:L/VI:L/VA:N/SC:N/SI:N/SA:N/E:U"),
        cwe_ids=["CWE-396"],
        auto_approve=True,
    )
    APK_EXPORTED_CP = dast_apk_method_info(
        file_name="analyze_bytecodes",
        name="apk_exported_cp",
        finding=FindingEnum.F075,
        developer=DeveloperEnum.BRIAM_AGUDELO,
        origin=MethodOriginEnum.ASSERTS,
        auto_approve=True,
    )
    SOCKET_USES_GET_INSECURE = dast_apk_method_info(
        file_name="analyze_bytecodes",
        name="socket_uses_get_insecure",
        finding=FindingEnum.F082,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        origin=MethodOriginEnum.ASSERTS,
        auto_approve=True,
    )
    APK_UNSIGNED = dast_apk_method_info(
        file_name="analyze_bytecodes",
        name="apk_unsigned",
        finding=FindingEnum.F103,
        developer=DeveloperEnum.DEFAULT,
        origin=MethodOriginEnum.ASSERTS,
        auto_approve=True,
    )
    WEBVIEW_VULNERABILITIES = dast_apk_method_info(
        file_name="analyze_bytecodes",
        name="webview_vulnerabilities",
        finding=FindingEnum.F268,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        origin=MethodOriginEnum.ASSERTS,
        auto_approve=True,
    )
    IMPROPER_CERTIFICATE_VALIDATION = dast_apk_method_info(
        file_name="analyze_bytecodes",
        name="improper_certificate_validation",
        finding=FindingEnum.F313,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        origin=MethodOriginEnum.ASSERTS,
        auto_approve=True,
    )
    IMPROPER_CERTIFICATE_VALIDATION_DEFAULT = dast_apk_method_info(
        file_name="analyze_bytecodes",
        name="improper_certificate_validation_default",
        finding=FindingEnum.F313,
        developer=DeveloperEnum.LUIS_PATINO,
        origin=MethodOriginEnum.HACKER,
        auto_approve=True,
    )
    APK_UNPROTECTED_EXPORTED_RECEIVERS = dast_apk_method_info(
        file_name="analyze_bytecodes",
        name="apk_unprotected_exported_receivers",
        finding=FindingEnum.F346,
        developer=DeveloperEnum.DIEGO_RESTREPO,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    APK_UNPROTECTED_EXPORTED_SERVICES = dast_apk_method_info(
        file_name="analyze_bytecodes",
        name="apk_unprotected_exported_services",
        finding=FindingEnum.F346,
        developer=DeveloperEnum.DIEGO_RESTREPO,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    APK_TASK_HIJACKING = dast_apk_method_info(
        file_name="analyze_bytecodes",
        name="apk_task_hijacking",
        finding=FindingEnum.F347,
        developer=DeveloperEnum.DIEGO_RESTREPO,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    HAS_FRAGMENT_INJECTION = dast_apk_method_info(
        file_name="analyze_bytecodes",
        name="has_fragment_injection",
        finding=FindingEnum.F398,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        origin=MethodOriginEnum.ASSERTS,
        auto_approve=True,
    )
    APK_CLEAR_TEXT_TRAFFIC = dast_apk_method_info(
        file_name="analyze_bytecodes",
        name="apk_clear_text_traffic",
        finding=FindingEnum.F403,
        developer=DeveloperEnum.DIEGO_RESTREPO,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
