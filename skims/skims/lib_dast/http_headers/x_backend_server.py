from operator import (
    methodcaller,
)

from lib_dast.http_headers.model import (
    XBackendServerHeader,
)


def _is_backend_server(name: str) -> bool:
    return name.lower() == "x-backend-server"


def parse(line: str) -> XBackendServerHeader | None:
    portions: list[str] = line.split(":", maxsplit=1)
    portions = list(map(methodcaller("strip"), portions))

    name, value = portions

    if not _is_backend_server(name):
        return None

    return XBackendServerHeader(name=name, value=value)
