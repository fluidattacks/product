from operator import (
    methodcaller,
)

from lib_dast.http_headers.model import (
    ServerHeader,
)


def _is_server(name: str) -> bool:
    return name.lower() == "server"


def parse(line: str) -> ServerHeader | None:
    portions: list[str] = line.split(":", maxsplit=1)
    portions = list(map(methodcaller("strip"), portions))

    name, value = portions

    if not _is_server(name):
        return None

    return ServerHeader(
        name=name,
        value=value,
    )
