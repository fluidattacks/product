from operator import (
    methodcaller,
)

from lib_dast.http_headers.model import (
    XPermittedCrossDomainPolicies,
)


def _is_x_permitted_cross_domain_policies(name: str) -> bool:
    return name.lower() == "x-permitted-cross-domain-policies"


def parse(line: str) -> XPermittedCrossDomainPolicies | None:
    portions: list[str] = line.split(":", maxsplit=1)
    portions = list(map(methodcaller("strip"), portions))

    name, value = portions

    if not _is_x_permitted_cross_domain_policies(name):
        return None

    return XPermittedCrossDomainPolicies(name=name, value=value)
