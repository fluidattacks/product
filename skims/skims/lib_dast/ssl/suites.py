from collections.abc import (
    Iterator,
)

from utils.cipher_suites import (
    SSLAuthentication,
    SSLCipherSuite,
    SSLEncryption,
    SSLHash,
    SSLKeyExchange,
    SSLSpecialSuite,
    SSLSuiteInfo,
    SSLSuiteVuln,
)


def get_suite_by_openssl_name(name: str) -> SSLSuiteInfo:
    for normal_suite in SSLCipherSuite:
        if name == normal_suite.value.openssl_name:
            return normal_suite.value

    return SSLSuiteInfo(
        rfc=0,
        iana_name="UNKNOWN",
        openssl_name=name,
        gnutls_name=None,
        code=None,
        key_exchange=SSLKeyExchange.UNKNOWN,
        authentication=SSLAuthentication.UNKNOWN,
        encryption=SSLEncryption.UNKNOWN,
        ssl_hash=SSLHash.UNKNOWN,
        tls_versions=(),
        vulnerabilities=(),
    )


def get_suite_by_code(code: tuple[int, int]) -> SSLSuiteInfo:
    for normal_suite in SSLCipherSuite:
        if code == normal_suite.value.code:
            return normal_suite.value

    for special_suite in SSLSpecialSuite:
        if code == special_suite.value.code:
            return special_suite.value

    return SSLSuiteInfo(
        rfc=0,
        iana_name="UNKNOWN",
        openssl_name=None,
        gnutls_name=None,
        code=code,
        key_exchange=SSLKeyExchange.UNKNOWN,
        authentication=SSLAuthentication.UNKNOWN,
        encryption=SSLEncryption.UNKNOWN,
        ssl_hash=SSLHash.UNKNOWN,
        tls_versions=(),
        vulnerabilities=(),
    )


def get_suites_with_pfs() -> Iterator[SSLSuiteInfo]:
    for suite in SSLCipherSuite:
        if SSLSuiteVuln.NO_PFS not in suite.value.vulnerabilities:
            yield suite.value


def get_suites_with_cbc() -> Iterator[SSLSuiteInfo]:
    for suite in SSLCipherSuite:
        if SSLSuiteVuln.CBC in suite.value.vulnerabilities:
            yield suite.value


def get_weak_suites() -> Iterator[SSLSuiteInfo]:
    ignored_vulns = {SSLSuiteVuln.NO_PFS, SSLSuiteVuln.CBC}
    for suite in SSLCipherSuite:
        if len(suite.value.vulnerabilities) > 1 and not any(
            vuln in ignored_vulns for vuln in suite.value.vulnerabilities
        ):
            yield suite.value
