from collections.abc import (
    Callable,
)
from concurrent.futures.process import (
    ProcessPoolExecutor,
)
from contextlib import (
    suppress,
)

import ctx
from aioextensions import (
    collect,
)
from lib_dast.common import (
    apply_exclusions,
)
from lib_dast.ssl import (
    analyze_certificate,
    analyze_protocol,
)
from lib_dast.ssl.model import (
    SSLCtxt,
    SSLServerResponse,
    get_path_from_attrs,
)
from model.core import (
    FindingEnum,
    Vulnerabilities,
    Vulnerability,
)
from urllib3 import (
    exceptions,
)
from urllib3.util.url import (
    parse_url,
)
from utils.cipher_suites import (
    SSLVersionId,
)
from utils.function import (
    shield,
)
from utils.logs import (
    log_blocking,
)
from utils.state import (
    VulnerabilitiesEphemeralStore,
)

CHECKS: tuple[
    dict[
        FindingEnum,
        list[Callable[[SSLCtxt], Vulnerabilities]],
    ],
    ...,
] = (analyze_protocol.CHECKS, analyze_certificate.CHECKS)

BLACKLISTED_DOMAINS = {
    "amazon-dss.com",
    "amazonaws.com",
    "s3.amazonaws.com",
    "amazonaws.com.cn",
    "amazonaws.org",
    "amazonses.com",
    "amazonwebservices.com",
    "aws.a2z.com",
    "aws.amazon.com",
    "aws.dev",
    "awsstatic.com",
    "elasticbeanstalk.com",
    "azure-api.net",
    "cloudapp.net",
    "cloudapp.azure.com",
    "azurewebsites.net",
    "googleapis.com",
}


def handle_vulnerabilities(
    vulnerabilities: tuple[Vulnerability, ...],
    stores: VulnerabilitiesEphemeralStore,
    exclusions: dict[str, list[str]] | None,
) -> None:
    if exclusions:
        vulnerabilities = apply_exclusions(vulnerabilities, exclusions)
    stores.store_vulns(vulnerabilities)


@shield(on_error_return=[], get_path_from_attrs=get_path_from_attrs)
async def analyze_one(
    *,
    ssl_ctx: SSLCtxt,
    stores: VulnerabilitiesEphemeralStore,
    exclusions: dict[str, list[str]] | None,
) -> None:
    for checks in CHECKS:
        for finding, check_list in checks.items():
            if finding in ctx.SKIMS_CONFIG.checks:
                for check in check_list:
                    vulns = check(ssl_ctx)
                    handle_vulnerabilities(vulns, stores, exclusions)


def _get_ssl_targets(urls: set[str]) -> set[tuple[str, int]]:
    targets: set[tuple[str, int]] = set()
    default_port = 443
    for url in urls:
        with suppress(ValueError, exceptions.HTTPError):
            parsed_url = parse_url(url)
            if not parsed_url.host or str(parsed_url.host).endswith(tuple(BLACKLISTED_DOMAINS)):
                continue
            port = parsed_url.port or default_port
            targets.add((parsed_url.host, port))
    return targets


def _get_ssl_context(ssl_target: tuple[str, int]) -> SSLCtxt:
    host, port = ssl_target
    responses: list[SSLServerResponse] = []
    for v_id in SSLVersionId:
        with suppress(Exception):
            if v_id != SSLVersionId.sslv3_0 and (
                tls_response := analyze_protocol.tls_connect(
                    host=host,
                    port=port,
                    v_id=v_id,
                )
            ):
                responses = [*responses, tls_response]

    return SSLCtxt(
        host=host,
        port=port,
        tls_responses=tuple(responses),
    )


async def analyze(
    *,
    vuln_stores: VulnerabilitiesEphemeralStore,
    urls: set[str],
    exclusions: dict[str, list[str]] | None,
) -> dict[str, bool]:
    if not any(finding in ctx.SKIMS_CONFIG.checks for checks in CHECKS for finding in checks):
        return {}

    ssl_targets = _get_ssl_targets(urls)
    log_blocking("info", "Requesting ssl connections from %s targets", len(ssl_targets))

    with ProcessPoolExecutor() as executor:
        ssl_contexts = tuple(executor.map(_get_ssl_context, ssl_targets))

    ssl_connections = {str(ssl_ctx): len(ssl_ctx.tls_responses) > 0 for ssl_ctx in ssl_contexts}

    count = len(ssl_contexts)
    log_blocking("info", "Performing ssl checks on %s available targets", count)

    await collect(
        (
            analyze_one(ssl_ctx=ssl_ctx, stores=vuln_stores, exclusions=exclusions)
            for ssl_ctx in ssl_contexts
        ),
        workers=ctx.CPU_CORES,
    )
    log_blocking("info", "SSL analysis completed!")
    return ssl_connections
