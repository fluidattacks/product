import datetime
from collections.abc import (
    Callable,
    Iterable,
)
from contextlib import (
    suppress,
)
from socket import (
    AF_INET,
    SOCK_STREAM,
    socket,
)
from ssl import (
    CERT_NONE,
    create_default_context,
)

import ctx as skims_ctx
from cryptography import (
    x509,
)
from cryptography.hazmat.backends import default_backend
from cryptography.x509.oid import ExtensionOID, NameOID
from fluidattacks_core.serializers.snippet import (
    SnippetViewport,
    make_snippet,
)
from lib_dast.methods_enum_ssl import (
    MethodsEnumSSL as MethodsEnum,
)
from lib_dast.ssl.as_string import (
    construct_certificate_snippet,
)
from lib_dast.ssl.model import (
    SSLCertificateVulnerability,
    SSLCtxt,
)
from model.core import (
    FindingEnum,
    Vulnerabilities,
)
from utils.build_vulns import (
    build_inputs_vuln,
    build_metadata,
)
from utils.translations import (
    t,
)


def _create_core_vulns(
    *,
    ssl_cert_vulns: Iterable[SSLCertificateVulnerability],
) -> Vulnerabilities:
    return tuple(
        build_inputs_vuln(
            method=ssl_cert_vuln.method.value,
            stream="home,socket-send,socket-response",
            what=str(ssl_cert_vuln),
            where=ssl_cert_vuln.description,
            metadata=build_metadata(
                method=ssl_cert_vuln.method.value,
                description=(f"{ssl_cert_vuln.description} {t(key='words.in')} {ssl_cert_vuln!s}"),
                snippet=make_snippet(
                    content=construct_certificate_snippet(
                        ssl_cert_vuln,
                        skims_ctx.SKIMS_CONFIG.language,
                    ),
                    viewport=SnippetViewport(line=0, column=0, wrap=True),
                ).content,
            ),
        )
        for ssl_cert_vuln in ssl_cert_vulns
    )


def _create_ssl_vuln(
    method: MethodsEnum,
    host: str,
    check_kwargs: dict[str, str] | None = None,
) -> SSLCertificateVulnerability:
    return SSLCertificateVulnerability(
        method=method,
        host=host,
        description=t(
            method.name,
            **(check_kwargs or {}),
        ),
    )


def _is_wildcard_certificate(host_domain: str, certificate_name: str) -> bool:
    if len(certificate_name.split("*.")) != 2:
        return False

    cn_prefix, cn_suffix = certificate_name.split("*.")

    if len(cn_prefix) > 0:
        return False
    if host_domain.endswith(cn_suffix):
        start_idx = len(host_domain) - len(cn_suffix)
        if (
            cn_suffix == host_domain[start_idx:]
            and host_domain[start_idx - 1] == "."
            and host_domain[: start_idx - 1].count(".") == 0
        ):
            return True
    return False


def _get_certificate_supported_names(cert: x509.Certificate) -> list[str]:
    cert_common_names = cert.subject.get_attributes_for_oid(NameOID.COMMON_NAME)
    cert_common_name = str(cert_common_names[0].value) if len(cert_common_names) > 0 else ""
    alt_names = []
    with suppress(Exception):
        ext = cert.extensions.get_extension_for_oid(ExtensionOID.SUBJECT_ALTERNATIVE_NAME)
        alt_names = ext.value.get_values_for_type(x509.DNSName)  # type:ignore[attr-defined]
    return [cert_common_name, *alt_names]


def _get_server_certificate(ssl_ctx: SSLCtxt) -> x509.Certificate | None:
    sock = socket(AF_INET, SOCK_STREAM)
    cert_bin = None

    try:
        sock.connect((ssl_ctx.host, ssl_ctx.port))
        context = create_default_context()
        context.check_hostname = False
        context.verify_mode = CERT_NONE
        sock = context.wrap_socket(sock, server_hostname=ssl_ctx.host)
        sock.settimeout(10)
        cert_bin = sock.getpeercert(binary_form=True)
        sock.close()
    except OSError:
        return None

    if cert_bin is None:
        return None

    return x509.load_der_x509_certificate(cert_bin, default_backend())


def ssl_expired_certificate(
    server_x509_cert: x509.Certificate,
    host_domain: str,
) -> SSLCertificateVulnerability | None:
    if datetime.datetime.now(tz=datetime.UTC) > server_x509_cert.not_valid_after_utc:
        return _create_ssl_vuln(
            method=MethodsEnum.SSL_CERTIFICATE_EXPIRED,
            host=host_domain,
        )

    return None


def ssl_self_signed_certificate(
    server_x509_cert: x509.Certificate,
    host_domain: str,
) -> SSLCertificateVulnerability | None:
    if server_x509_cert.issuer == server_x509_cert.subject:
        return _create_ssl_vuln(
            method=MethodsEnum.SSL_SELF_SIGNED_CERTIFICATE,
            host=host_domain,
        )
    return None


def ssl_wrong_cert_name(
    server_x509_cert: x509.Certificate,
    host_domain: str,
) -> SSLCertificateVulnerability | None:
    certificate_supported_names = _get_certificate_supported_names(server_x509_cert)

    if host_domain in certificate_supported_names:
        return None

    wildcard_names = [
        name
        for name in certificate_supported_names
        if "*." in name and _is_wildcard_certificate(host_domain, name)
    ]

    if len(wildcard_names) == 0:
        return _create_ssl_vuln(
            method=MethodsEnum.SSL_WRONG_CN,
            host=host_domain,
            check_kwargs={"cert_name": certificate_supported_names[0]},
        )

    if len(wildcard_names) > 0:
        return _create_ssl_vuln(
            method=MethodsEnum.SSL_WILDCARD_CERTIFICATE,
            host=host_domain,
            check_kwargs={"cert_name": max(wildcard_names, key=len)},
        )

    return None


def ssl_certificate_vulns(ssl_ctx: SSLCtxt) -> Vulnerabilities:
    server_x509_cert = _get_server_certificate(ssl_ctx)
    if not server_x509_cert:
        return ()

    ssl_vulnerabilities: list[SSLCertificateVulnerability] = []

    if expired_vuln := ssl_expired_certificate(server_x509_cert, ssl_ctx.host):
        ssl_vulnerabilities.append(expired_vuln)

    if signed_vuln := ssl_self_signed_certificate(server_x509_cert, ssl_ctx.host):
        ssl_vulnerabilities.append(signed_vuln)

    if wrong_name_vuln := ssl_wrong_cert_name(server_x509_cert, ssl_ctx.host):
        ssl_vulnerabilities.append(wrong_name_vuln)

    return _create_core_vulns(ssl_cert_vulns=ssl_vulnerabilities)


CHECKS: dict[
    FindingEnum,
    list[Callable[[SSLCtxt], Vulnerabilities]],
] = {
    FindingEnum.F313: [
        ssl_certificate_vulns,
    ],
}
