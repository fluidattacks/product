from enum import (
    Enum,
)
from ssl import (
    TLSVersion,
)
from typing import (
    NamedTuple,
)

from lib_dast.methods_enum_ssl import (
    MethodsEnumSSL as MethodsEnum,
)
from model.core import (
    LocalesEnum,
)
from utils.cipher_suites import (
    SSLSuiteInfo,
    SSLVersionId,
)


class SSLSnippetLine(Enum):
    SERVER = 1
    INTENTION = 2
    VERSIONS = 3
    REQUEST = 5
    RESPONSE = 10


class TLSVersionId(Enum):
    tlsv1_0 = TLSVersion.TLSv1
    tlsv1_1 = TLSVersion.TLSv1_1
    tlsv1_2 = TLSVersion.TLSv1_2
    tlsv1_3 = TLSVersion.TLSv1_3


class SSLHandshakeRecord(Enum):
    CLIENT_HELLO = 1
    SERVER_HELLO = 2
    CERTIFICATE = 11
    SERVER_KEY_EXCHANGE = 12
    CERTIFICATE_REQUEST = 13
    SERVER_HELLO_DONE = 14
    CERTIFICATE_VERIFY = 15
    CLIENT_KEY_EXCHANGE = 16
    FINISHED = 20


class SSLRecord(Enum):
    CHANGE_CIPHER_SPEC = 20
    ALERT = 21
    HANDSHAKE = 22
    APPLICATION_DATA = 23


class SSLAlertLevel(Enum):
    WARNING = 1
    FATAL = 2
    unknown = 255


class SSLAlertDescription(Enum):
    close_notify = 0
    unexpected_message = 10
    bad_record_mac = 20
    decryption_failed_reserved = 21
    record_overflow = 22
    decompression_failure_reserved = 30
    handshake_failure = 40
    no_certificate_reserved = 41
    bad_certificate = 42
    unsupported_certificate = 43
    certificate_revoked = 44
    certificate_expired = 45
    certificate_unknown = 46
    illegal_parameter = 47
    unknown_ca = 48
    access_denied = 49
    decode_error = 50
    decrypt_error = 51
    export_restriction_reserved = 60
    protocol_version = 70
    insufficient_security = 71
    internal_error = 80
    inappropriate_fallback = 86
    user_canceled = 90
    no_renegotiation_reserved = 100
    missing_extension = 109
    unsupported_extension = 110
    certificate_unobtainable_reserved = 111
    unrecognized_name = 112
    bad_certificate_status_response = 113
    bad_certificate_hash_value_reserved = 114
    unknown_psk_identity = 115
    certificate_required = 116
    no_application_protocol = 120
    unknown = 255


class SSLAlert(NamedTuple):
    level: SSLAlertLevel
    description: SSLAlertDescription


class SSLServerHandshake(NamedTuple):
    record: SSLHandshakeRecord
    version_id: SSLVersionId
    cipher_suite: SSLSuiteInfo | None = None


class SSLServerResponse(NamedTuple):
    record: SSLRecord
    version_id: SSLVersionId
    alert: SSLAlert | None = None
    handshake: SSLServerHandshake | None = None


class SSLCtxt(NamedTuple):
    host: str = "localhost"
    port: int = 443
    tls_responses: tuple[SSLServerResponse, ...] = ()

    def get_tls_response(self, v_id: SSLVersionId) -> SSLServerResponse | None:
        for tls_response in self.tls_responses:
            if tls_response.handshake is not None and tls_response.handshake.version_id == v_id:
                return tls_response
        return None

    def get_supported_tls_versions(self) -> tuple[SSLVersionId, ...]:
        return tuple(
            tls_response.handshake.version_id
            for tls_response in self.tls_responses
            if tls_response.handshake is not None
        )

    def __str__(self) -> str:
        return f"https://{self.host}:{self.port}"


class SSLSettings(NamedTuple):
    context: SSLCtxt
    scsv: bool = False
    tls_version: SSLVersionId = SSLVersionId.sslv3_0
    key_exchange_names: list[str] = ["ANY"]  # noqa: RUF012
    authentication_names: list[str] = ["ANY"]  # noqa: RUF012
    cipher_names: list[str] = ["ANY"]  # noqa: RUF012
    hash_names: list[str] = ["ANY"]  # noqa: RUF012
    intention: dict[LocalesEnum, str] = {  # noqa: RUF012
        LocalesEnum.EN: "establish SSL/TLS connection",
        LocalesEnum.ES: "establecer conexión SSL/TLS",
    }

    def __str__(self) -> str:
        return str(self.context)


class SSLVulnerability(NamedTuple):
    description: str
    ssl_settings: SSLSettings
    server_response: SSLServerResponse | None
    method: MethodsEnum

    def get_context(self) -> SSLCtxt:
        return self.ssl_settings.context

    def get_intention(self, locale: LocalesEnum) -> str:
        return self.ssl_settings.intention[locale]

    def __str__(self) -> str:
        return str(self.ssl_settings)


class SSLCertificateVulnerability(NamedTuple):
    method: MethodsEnum
    host: str
    description: str

    def __str__(self) -> str:
        return f"https://{self.host}"


def get_path_from_attrs(*args: tuple | dict) -> str:
    path = ""
    if len(args) > 1 and isinstance(args[1], dict) and "ssl_ctx" in args[1]:
        path = args[1]["ssl_ctx"]
    return path
