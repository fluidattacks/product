from model.core import (
    Vulnerabilities,
)


def apply_exclusions(reports: Vulnerabilities, exclusions: dict[str, list[str]]) -> Vulnerabilities:
    modified_reports = []
    for vuln in reports:
        fin_code = f"f{vuln.finding.value.title.split('.')[2]}"
        if any(
            end_point in vuln.what and fin_code in findings
            for end_point, findings in exclusions.items()
        ):
            modified_reports.append(vuln.set_exclusion_value(is_exclusion=True))
        else:
            modified_reports.append(vuln)

    return tuple(modified_reports)
