from enum import (
    Enum,
)

from model.core import (
    DeveloperEnum,
    FindingEnum,
    MethodInfo,
    MethodOriginEnum,
    VulnerabilityTechnique,
)


class MethodsEnumHTTP(Enum):
    WWW_AUTHENTICATE = MethodInfo(
        file_name="analyze_headers",
        name="www_authenticate",
        module="lib_dast/http",
        finding=FindingEnum.F015,
        developer=DeveloperEnum.JUAN_ECHEVERRI,
        technique=VulnerabilityTechnique.DAST,
        origin=MethodOriginEnum.ASSERTS,
        auto_approve=True,
    )
    LOCATION = MethodInfo(
        file_name="analyze_headers",
        name="location",
        module="lib_dast/http",
        finding=FindingEnum.F023,
        developer=DeveloperEnum.JUAN_ECHEVERRI,
        technique=VulnerabilityTechnique.DAST,
        origin=MethodOriginEnum.ASSERTS,
        auto_approve=True,
    )
    VIEW_STATE = MethodInfo(
        file_name="analyze_content",
        name="view_state",
        module="lib_dast/http",
        finding=FindingEnum.F036,
        developer=DeveloperEnum.DEFAULT,
        technique=VulnerabilityTechnique.DAST,
        origin=MethodOriginEnum.ASSERTS,
        auto_approve=True,
    )
    CONTENT_SECURITY_POLICY = MethodInfo(
        file_name="analyze_headers",
        name="content_security_policy",
        module="lib_dast/http",
        finding=FindingEnum.F043,
        developer=DeveloperEnum.DEFAULT,
        technique=VulnerabilityTechnique.DAST,
        origin=MethodOriginEnum.ASSERTS,
        auto_approve=True,
    )
    CONT_SEC_POL_MIXED_CONTENT = MethodInfo(
        file_name="analyze_headers",
        name="cont_sec_pol_mixed_content",
        module="lib_dast/http",
        finding=FindingEnum.F043,
        developer=DeveloperEnum.DEFAULT,
        technique=VulnerabilityTechnique.DAST,
        origin=MethodOriginEnum.ASSERTS,
        auto_approve=True,
    )
    CONT_SEC_POL_FRAME_ANCESTORS = MethodInfo(
        file_name="analyze_headers",
        name="cont_sec_pol_frame_ancestors",
        module="lib_dast/http",
        finding=FindingEnum.F043,
        developer=DeveloperEnum.DEFAULT,
        technique=VulnerabilityTechnique.DAST,
        origin=MethodOriginEnum.ASSERTS,
        auto_approve=True,
    )
    CONT_SEC_POL_WILD_URI = MethodInfo(
        file_name="analyze_headers",
        name="cont_sec_pol_wild_uri",
        module="lib_dast/http",
        finding=FindingEnum.F043,
        developer=DeveloperEnum.DEFAULT,
        technique=VulnerabilityTechnique.DAST,
        origin=MethodOriginEnum.ASSERTS,
        auto_approve=True,
    )
    CONT_SEC_POL_MISSING_OBJ = MethodInfo(
        file_name="analyze_headers",
        name="cont_sec_pol_missing_obj",
        module="lib_dast/http",
        finding=FindingEnum.F043,
        developer=DeveloperEnum.DEFAULT,
        technique=VulnerabilityTechnique.DAST,
        origin=MethodOriginEnum.ASSERTS,
        auto_approve=True,
    )
    CONT_SEC_POL_MISSING_SCRIPT = MethodInfo(
        file_name="analyze_headers",
        name="cont_sec_pol_missing_script",
        module="lib_dast/http",
        finding=FindingEnum.F043,
        developer=DeveloperEnum.DEFAULT,
        technique=VulnerabilityTechnique.DAST,
        origin=MethodOriginEnum.ASSERTS,
        auto_approve=True,
    )
    CONT_SEC_POL_UNSAFE_LINE = MethodInfo(
        file_name="analyze_headers",
        name="cont_sec_pol_unsafe_line",
        module="lib_dast/http",
        finding=FindingEnum.F043,
        developer=DeveloperEnum.DEFAULT,
        technique=VulnerabilityTechnique.DAST,
        origin=MethodOriginEnum.ASSERTS,
        auto_approve=True,
    )
    CONT_SEC_POL_HOSTS_JSONP = MethodInfo(
        file_name="analyze_headers",
        name="cont_sec_pol_hosts_jsonp",
        module="lib_dast/http",
        finding=FindingEnum.F043,
        developer=DeveloperEnum.DEFAULT,
        technique=VulnerabilityTechnique.DAST,
        origin=MethodOriginEnum.ASSERTS,
        auto_approve=True,
    )
    UPGRADE_INSECURE_REQUESTS = MethodInfo(
        file_name="analyze_headers",
        name="upgrade_insecure_requests",
        module="lib_dast/http",
        finding=FindingEnum.F043,
        developer=DeveloperEnum.ALEJANDRO_SALGADO,
        technique=VulnerabilityTechnique.DAST,
        origin=MethodOriginEnum.ASSERTS,
        auto_approve=True,
    )
    DATE = MethodInfo(
        file_name="analyze_headers",
        name="date",
        module="lib_dast/http",
        finding=FindingEnum.F064,
        developer=DeveloperEnum.ANDRES_CUBEROS,
        technique=VulnerabilityTechnique.DAST,
        origin=MethodOriginEnum.ASSERTS,
        auto_approve=True,
    )
    REFERRER_POLICY = MethodInfo(
        file_name="analyze_headers",
        name="referrer_policy",
        module="lib_dast/http",
        finding=FindingEnum.F071,
        developer=DeveloperEnum.DEFAULT,
        technique=VulnerabilityTechnique.DAST,
        origin=MethodOriginEnum.ASSERTS,
        auto_approve=True,
    )
    MISSING_REFERRER_POLICY = MethodInfo(
        file_name="analyze_headers",
        name="missing_referrer_policy",
        module="lib_dast/http",
        finding=FindingEnum.F071,
        developer=DeveloperEnum.DEFAULT,
        technique=VulnerabilityTechnique.DAST,
        origin=MethodOriginEnum.ASSERTS,
        auto_approve=True,
    )
    SUB_RESOURCE_INTEGRITY = MethodInfo(
        file_name="analyze_content",
        name="sub_resource_integrity",
        module="lib_dast/http",
        finding=FindingEnum.F086,
        developer=DeveloperEnum.DEFAULT,
        technique=VulnerabilityTechnique.DAST,
        origin=MethodOriginEnum.ASSERTS,
        auto_approve=True,
    )
    SET_COOKIE_HTTPONLY = MethodInfo(
        file_name="analyze_headers",
        name="set_cookie_httponly",
        module="lib_dast/http",
        finding=FindingEnum.F128,
        developer=DeveloperEnum.ALEJANDRO_SALGADO,
        technique=VulnerabilityTechnique.DAST,
        origin=MethodOriginEnum.ASSERTS,
        auto_approve=True,
    )
    SET_COOKIE_SAMESITE = MethodInfo(
        file_name="analyze_headers",
        name="set_cookie_samesite",
        module="lib_dast/http",
        finding=FindingEnum.F129,
        developer=DeveloperEnum.ALEJANDRO_SALGADO,
        technique=VulnerabilityTechnique.DAST,
        origin=MethodOriginEnum.ASSERTS,
        auto_approve=True,
    )
    SET_COOKIE_SECURE = MethodInfo(
        file_name="analyze_headers",
        name="set_cookie_secure",
        module="lib_dast/http",
        finding=FindingEnum.F130,
        developer=DeveloperEnum.ALEJANDRO_SALGADO,
        technique=VulnerabilityTechnique.DAST,
        origin=MethodOriginEnum.ASSERTS,
        auto_approve=True,
    )
    STRICT_TRANSPORT_SECURITY = MethodInfo(
        file_name="analyze_headers",
        name="strict_transport_security",
        module="lib_dast/http",
        finding=FindingEnum.F131,
        developer=DeveloperEnum.DEFAULT,
        technique=VulnerabilityTechnique.DAST,
        origin=MethodOriginEnum.ASSERTS,
        auto_approve=True,
    )
    STRICT_TRANSPORT_LOW_MAX_AGE = MethodInfo(
        file_name="analyze_headers",
        name="strict_transport_low_max_age",
        module="lib_dast/http",
        finding=FindingEnum.F131,
        developer=DeveloperEnum.DEFAULT,
        technique=VulnerabilityTechnique.DAST,
        origin=MethodOriginEnum.ASSERTS,
        auto_approve=True,
    )
    STRICT_TRANSPORT_INCLUDE_SUBDOMAINS = MethodInfo(
        file_name="analyze_headers",
        name="strict_transport_include_subdomains",
        module="lib_dast/http",
        finding=FindingEnum.F131,
        developer=DeveloperEnum.DEFAULT,
        technique=VulnerabilityTechnique.DAST,
        origin=MethodOriginEnum.ASSERTS,
        auto_approve=True,
    )
    X_CONTENT_TYPE_OPTIONS = MethodInfo(
        file_name="analyze_headers",
        name="x_content_type_options",
        module="lib_dast/http",
        finding=FindingEnum.F132,
        developer=DeveloperEnum.DEFAULT,
        technique=VulnerabilityTechnique.DAST,
        origin=MethodOriginEnum.ASSERTS,
        auto_approve=True,
    )
    X_CONTENT_TYPE_OPTIONS_NOSNIFF = MethodInfo(
        file_name="analyze_headers",
        name="x_content_type_options_nosniff",
        module="lib_dast/http",
        finding=FindingEnum.F132,
        developer=DeveloperEnum.DEFAULT,
        technique=VulnerabilityTechnique.DAST,
        origin=MethodOriginEnum.ASSERTS,
        auto_approve=True,
    )
    X_XSS_PROTECTION_ENABLED = MethodInfo(
        file_name="analyze_headers",
        name="x_xss_protection_enabled",
        module="lib_dast/http",
        finding=FindingEnum.F135,
        developer=DeveloperEnum.JUAN_ECHEVERRI,
        technique=VulnerabilityTechnique.DAST,
        origin=MethodOriginEnum.HACKER,
        auto_approve=True,
    )
    X_PERMITTED_CROSS_DOMAIN_POLICIES = MethodInfo(
        file_name="analyze_headers",
        name="x_permitted_cross_domain_policies",
        module="lib_dast/http",
        finding=FindingEnum.F137,
        developer=DeveloperEnum.JULIAN_GOMEZ,
        technique=VulnerabilityTechnique.DAST,
        origin=MethodOriginEnum.BENCHMARK,
        auto_approve=True,
    )
    UNSAFE_HTTP_XFRAME_OPTIONS = MethodInfo(
        file_name="analyze_headers",
        name="unsafe_http_xframe_options",
        module="lib_dast/http",
        finding=FindingEnum.F152,
        developer=DeveloperEnum.LUIS_PATINO,
        technique=VulnerabilityTechnique.DAST,
        origin=MethodOriginEnum.BENCHMARK,
        auto_approve=True,
    )
    CHECK_DNS_RECORDS = MethodInfo(
        file_name="analyze_dns",
        name="check_dns_records",
        module="lib_dast/http",
        finding=FindingEnum.F182,
        developer=DeveloperEnum.JULIAN_GOMEZ,
        technique=VulnerabilityTechnique.DAST,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    HTTP_ERROR_IN_RESPONSE = MethodInfo(
        file_name="analyze_content",
        name="http_error_in_response",
        module="lib_dast/http",
        finding=FindingEnum.F234,
        developer=DeveloperEnum.JULIAN_GOMEZ,
        technique=VulnerabilityTechnique.DAST,
        origin=MethodOriginEnum.BENCHMARK,
        auto_approve=True,
    )
    HTTP_SERVER_HEADER_LEAKED = MethodInfo(
        file_name="analyze_headers",
        name="http_server_header_leaked",
        module="lib_dast/http",
        finding=FindingEnum.F235,
        developer=DeveloperEnum.JUAN_ECHEVERRI,
        technique=VulnerabilityTechnique.DAST,
        origin=MethodOriginEnum.HACKER,
        cvss="CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:U/C:L/I:N/A:N/E:P/RL:U/RC:C",
        cvss_v4=("CVSS:4.0/AV:N/AC:L/AT:N/PR:N/UI:N/SC:N/SI:N/SA:N/VC:L/VI:N/VA:N/E:P"),
        cwe_ids=["CWE-497"],
        auto_approve=True,
    )
    HTTP_X_ASPNET_VERSION_BY_HEADER_LEAKED = MethodInfo(
        file_name="analyze_headers",
        name="http_x_aspnet_version_by_header_leaked",
        module="lib_dast/http",
        finding=FindingEnum.F235,
        developer=DeveloperEnum.ROBIN_QUINTERO,
        technique=VulnerabilityTechnique.DAST,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    HTTP_X_ASPNET_MVC_VERSION_HEADER_LEAKED = MethodInfo(
        file_name="analyze_headers",
        name="http_x_aspnet_mvc_version_header_leaked",
        module="lib_dast/http",
        finding=FindingEnum.F235,
        developer=DeveloperEnum.ROBIN_QUINTERO,
        technique=VulnerabilityTechnique.DAST,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    HTTP_X_BACKEND_SERVER_HEADER_LEAKED = MethodInfo(
        file_name="analyze_headers",
        name="http_x_backend_server_header_leaked",
        module="lib_dast/http",
        finding=FindingEnum.F235,
        developer=DeveloperEnum.ROBIN_QUINTERO,
        technique=VulnerabilityTechnique.DAST,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    HTTP_X_POWERED_BY_HEADER_LEAKED = MethodInfo(
        file_name="analyze_headers",
        name="http_x_powered_by_header_leaked",
        module="lib_dast/http",
        finding=FindingEnum.F235,
        developer=DeveloperEnum.JUAN_ECHEVERRI,
        technique=VulnerabilityTechnique.DAST,
        origin=MethodOriginEnum.EXTERNAL,
        cvss="CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:U/C:L/I:N/A:N/E:P/RL:U/RC:C",
        cvss_v4=("CVSS:4.0/AV:N/AC:L/AT:N/PR:N/UI:N/SC:N/SI:N/SA:N/VC:L/VI:N/VA:N/E:P"),
        cwe_ids=["CWE-497"],
        auto_approve=True,
    )
    CDN_VULNERABLE_DEP = MethodInfo(
        file_name="analyze_content",
        name="cdn_vulnerable_dep",
        module="lib_dast/http",
        finding=FindingEnum.F435,
        developer=DeveloperEnum.LUIS_PATINO,
        technique=VulnerabilityTechnique.DAST,
        origin=MethodOriginEnum.BENCHMARK,
        auto_approve=True,
    )
    HTTP_PERMISSIONS_POLICY_HEADER_NOT_PRESENT = MethodInfo(
        file_name="analyze_headers",
        name="http_permissions_policy_header_not_present",
        module="lib_dast/http",
        finding=FindingEnum.F440,
        developer=DeveloperEnum.JUAN_ECHEVERRI,
        technique=VulnerabilityTechnique.DAST,
        origin=MethodOriginEnum.HACKER,
        auto_approve=True,
    )
