import contextlib
from collections.abc import (
    Callable,
    Iterable,
)
from typing import (
    NamedTuple,
)
from urllib.parse import (
    urlparse,
)

import requests
import viewstate
from bs4 import (
    BeautifulSoup,
)
from bs4.element import (
    Tag,
)
from fluidattacks_core.serializers.snippet import (
    SnippetViewport,
    make_snippet,
)
from lib_dast.http.model import (
    URLContext,
)
from lib_dast.methods_enum_http import (
    MethodsEnumHTTP as MethodsEnum,
)
from model.core import (
    FindingEnum,
    HTTPProperties,
    LocalesEnum,
    Vulnerabilities,
)
from utils.build_vulns import (
    build_inputs_vuln,
    build_metadata,
)
from utils.logs import (
    log_blocking,
)
from utils.string_handlers import (
    get_numbers,
)
from utils.translations import (
    t,
)


class ContentCheckCtx(NamedTuple):
    url_ctx: URLContext


class Location(NamedTuple):
    description: str
    column: int
    line: int

    @classmethod
    def from_soup_tag(
        cls: object,
        tag: Tag,
        method: MethodsEnum,
        desc_kwargs: dict[str, LocalesEnum] | None = None,
    ) -> "Location":
        line, column = get_numbers(tag)
        return Location(
            description=t(
                method.name,
                **(desc_kwargs or {}),
            ),
            column=column,
            line=line,
        )


def _build_content_vulnerabilities(
    locations: Iterable[Location],
    ctx: ContentCheckCtx,
    method: MethodsEnum,
    content_alt: str | None = None,
) -> Vulnerabilities:
    return tuple(
        build_inputs_vuln(
            method=method.value,
            stream="home,response,content",
            what=ctx.url_ctx.original_url,
            where=str(location.line),
            metadata=build_metadata(
                method=method.value,
                description=(
                    f"{location.description} {t(key='words.in')} {ctx.url_ctx.original_url}"
                ),
                snippet=make_snippet(
                    content=content_alt if content_alt else ctx.url_ctx.content,
                    viewport=SnippetViewport(
                        column=location.column,
                        line=location.line,
                    ),
                ).content,
                http_properties=HTTPProperties(
                    has_redirect=ctx.url_ctx.has_redirect,
                    original_url=ctx.url_ctx.original_url,
                ),
            ),
        )
        for location in locations
    )


def _sub_resource_integrity(
    ctx: ContentCheckCtx,
) -> Vulnerabilities:
    method = MethodsEnum.SUB_RESOURCE_INTEGRITY
    locations: list[Location] = []

    for script in ctx.url_ctx.soup.find_all("script"):
        if (not script.get("integrity") or not script.get("crossorigin")) and (
            src := script.get("src")
        ):
            netloc = urlparse(src).netloc

            for domain in (
                "cdnjs.cloudflare.com",
                "cloudflareinsights.com",
                "cookiebot.com",
                "newrelic.com",
                "nr-data.net",
            ):
                if netloc.endswith(domain):
                    locations.append(  # noqa: PERF401
                        Location.from_soup_tag(
                            method=method,
                            desc_kwargs={"netloc": netloc},
                            tag=script,
                        ),
                    )

    return _build_content_vulnerabilities(
        ctx=ctx,
        locations=locations,
        method=method,
    )


def _view_state(ctx: ContentCheckCtx) -> Vulnerabilities:
    method = MethodsEnum.VIEW_STATE
    locations: list[Location] = []

    for tag in ctx.url_ctx.soup.find_all("input"):
        if tag.get("name") == "__VIEWSTATE" and (value := tag.get("value")):
            with contextlib.suppress(viewstate.ViewStateException):
                view_state = viewstate.ViewState(base64=value)
                view_state.decode()

                locations.append(
                    Location.from_soup_tag(
                        method=method,
                        tag=tag,
                    ),
                )

    return _build_content_vulnerabilities(
        ctx=ctx,
        locations=locations,
        method=method,
    )


def _cdn_vulnerable_dep(
    ctx: ContentCheckCtx,
) -> Vulnerabilities:
    method = MethodsEnum.CDN_VULNERABLE_DEP
    locations: list[Location] = []

    for tag in ctx.url_ctx.soup.find_all("script"):
        if (source := tag.attrs.get("src")) and "/jquery/2.2.4/" in source.lower():
            locations.append(  # noqa: PERF401
                Location.from_soup_tag(
                    method=method,
                    tag=tag,
                ),
            )
    return _build_content_vulnerabilities(
        ctx=ctx,
        locations=locations,
        method=method,
    )


def http_error_in_response(ctx: ContentCheckCtx) -> Vulnerabilities:
    locations: list[Location] = []
    method = MethodsEnum.HTTP_ERROR_IN_RESPONSE
    url = ctx.url_ctx.url.lower()
    malformed_jwt = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.malformed-payload.signature"
    content: str | None = None
    post_response = None
    try:
        post_response = requests.post(
            url,
            headers={"Authorization": f"Bearer {malformed_jwt}"},
            timeout=4,
        )
    except requests.RequestException as exc:
        log_blocking("warning", "Unable to get response due to: %s", exc)

    if post_response is not None and post_response.status_code >= 500:
        soup = BeautifulSoup(post_response.content, "html.parser")
        uls = soup.find_all("ul")
        for ul_tag in uls:
            if ul_tag.get("id") == "stacktrace":
                content = post_response.content.decode("latin-1")
                locations.append(
                    Location.from_soup_tag(
                        method=method,
                        tag=ul_tag,
                    ),
                )

    return _build_content_vulnerabilities(
        ctx=ctx,
        locations=locations,
        method=method,
        content_alt=content,
    )


def get_check_ctx(url: URLContext) -> ContentCheckCtx:
    return ContentCheckCtx(
        url_ctx=url,
    )


CHECKS: dict[
    FindingEnum,
    list[Callable[[ContentCheckCtx], Vulnerabilities]],
] = {
    FindingEnum.F036: [_view_state],
    FindingEnum.F086: [_sub_resource_integrity],
    FindingEnum.F234: [http_error_in_response],
    FindingEnum.F435: [_cdn_vulnerable_dep],
}
