import asyncio
import re
from collections.abc import (
    Callable,
)
from datetime import (
    datetime,
)
from typing import (
    Any,
)
from urllib.parse import (
    ParseResult,
    urlparse,
)

import aiohttp
import bs4
import ctx
from lib_dast.common import (
    apply_exclusions,
)
from lib_dast.http import (
    analyze_content,
    analyze_dns,
    analyze_headers,
)
from lib_dast.http.model import (
    URLContext,
    get_path_from_attrs,
)
from lib_dast.utils import (
    get_offset,
    is_html,
)
from model.core import (
    FindingEnum,
    UrlStatus,
    Vulnerabilities,
    Vulnerability,
)
from utils.function import (
    shield,
    shield_blocking,
)
from utils.http_requests import (
    create_session,
    request,
)
from utils.logs import (
    log_blocking,
)
from utils.state import (
    VulnerabilitiesEphemeralStore,
)

CHECKS: tuple[
    tuple[
        Callable[[URLContext], Any],
        dict[
            FindingEnum,
            list[Callable[[Any], Vulnerabilities]],
        ],
    ],
    ...,
] = (
    (analyze_content.get_check_ctx, analyze_content.CHECKS),
    (analyze_dns.get_check_ctx, analyze_dns.CHECKS),
    (analyze_headers.get_check_ctx, analyze_headers.CHECKS),
)


def handle_vulnerabilities(
    vulnerabilities: tuple[Vulnerability, ...],
    stores: VulnerabilitiesEphemeralStore,
    exclusions: dict[str, list[str]] | None,
) -> None:
    if exclusions:
        vulnerabilities = apply_exclusions(vulnerabilities, exclusions)

    stores.store_vulns(vulnerabilities)


@shield_blocking(on_error_return=[], get_path_from_attrs=get_path_from_attrs)
def analyze_one(
    url_ctx: URLContext,
    stores: VulnerabilitiesEphemeralStore,
    exclusions: dict[str, list[str]] | None,
) -> None:
    for get_check_ctx, checks in CHECKS:
        check_ctx = get_check_ctx(url_ctx)
        for finding, fin_methods in checks.items():
            if (
                finding not in ctx.SKIMS_CONFIG.checks
                or (url_ctx.response_status >= 400 and finding is not FindingEnum.F015)
                or (url_ctx.response_status != 401 and finding is FindingEnum.F015)
            ):
                continue

            for method in fin_methods:
                vulns = method(check_ctx)
                handle_vulnerabilities(vulns, stores, exclusions)


def _get_urls_to_request(urls: set[str]) -> list[str]:
    http = "http://"
    https = "https://"
    ignored_extensions = {"css", "js", "jpg", "jpeg", "png", "svg"}
    protocol_pattern = re.compile(r"^[a-zA-Z]+://")
    urls_pending = set()
    for url in urls:
        if url.endswith(tuple(ignored_extensions)) or (
            protocol_pattern.match(url) is not None and not url.startswith((http, https))
        ):
            continue

        if not url.startswith((http, https)):
            urls_pending.add(http + url)
            urls_pending.add(https + url)
        else:
            urls_pending.add(url)
            alternate_protocol_url = (
                (https + url[len(http) :]) if url.startswith(http) else (http + url[len(https) :])
            )
            urls_pending.add(alternate_protocol_url)

    return list(urls_pending)


def _check_url_components(
    url: str,
    redirect_url: str,
    original_urls: set[str],
) -> ParseResult | None:
    components = urlparse(redirect_url)
    # If the redirected vuln domain is different than the original url
    # Or if the alternate protocol is redirected to the starting url
    # example: http:localhost is redirected to https:localhost
    # it should not be analyzed
    if (original_components := urlparse(url)) and (
        components[1] != original_components[1]
        or (redirect_url.rstrip("/") in original_urls and components[0] != original_components[0])
    ):
        return None
    return components


def _remove_duplicate_redirected_urls(
    url_contexts: set[URLContext],
    original_urls: set[str],
) -> set[URLContext]:
    redirected_urls = {}
    for url_ctx in url_contexts:
        clean_url = url_ctx.url.rstrip("/")
        if clean_url not in redirected_urls:
            redirected_urls[clean_url] = 1
        else:
            redirected_urls[clean_url] += 1

    return {
        url_ctx
        for url_ctx in url_contexts
        if redirected_urls[url_ctx.url.rstrip("/")] == 1
        or url_ctx.original_url.rstrip("/") in original_urls
    }


async def get_response(url: str, session: aiohttp.ClientSession) -> aiohttp.ClientResponse:
    if (response := await session.request("GET", url)) and (
        (response.status < 400) or (response.status == 401)
    ):
        return response

    return await session.request(
        "GET",
        url,
        headers={
            "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; "
            "rv:91.0) Gecko/20100101 Firefox/91.0",
        },
    )


@shield(on_error_return=None, attempts=3, sleep_between_retries=3)
async def get_url_ctx(
    *,
    url: str,
    session: aiohttp.ClientSession,
    ntp_offset: float | None,
    clean_original_urls: set[str],
) -> URLContext | UrlStatus | None:
    try:
        if response := await get_response(url, session):
            redirect_url = str(response.url)
            components = _check_url_components(url, redirect_url, clean_original_urls)

            if not components:
                return None

            content_raw = await response.content.read(1048576)
            content = content_raw.decode("latin-1")
            soup = bs4.BeautifulSoup(content, features="html.parser")

            headers_location = await request(
                session,
                "GET",
                url,
                headers={
                    "Host": "fluidattacks.com",
                },
            )

            return URLContext(
                components=components,
                content=content,
                custom_f023=headers_location.headers.get("location", "")
                if headers_location
                else None,
                has_redirect=redirect_url.rstrip("/") != url.rstrip("/"),
                headers_raw=response.headers,  # type: ignore[arg-type]
                is_html=is_html(content, soup),
                original_url=url,
                soup=soup,
                timestamp_ntp=(
                    datetime.now().timestamp() + ntp_offset  # noqa: DTZ005
                    if ntp_offset
                    else None
                ),
                url=redirect_url,
                response_status=response.status,
            )
    except (
        TimeoutError,
        aiohttp.client_exceptions.ClientConnectorError,
        aiohttp.client_exceptions.ServerDisconnectedError,
        aiohttp.client_exceptions.ClientOSError,
        aiohttp.client_exceptions.TooManyRedirects,
    ):
        msg = f"Unable to connect to endpoint {url} when attempting an HTTP GET request"
        log_blocking("warning", msg)
        return UrlStatus(url=url, status_code=404)
    return None


async def get_contexts_to_analyze(
    *,
    urls: set[str],
) -> tuple[set[URLContext], list[UrlStatus]]:
    urls_pending = _get_urls_to_request(urls)
    ntp_offset: float | None = get_offset()
    clean_original_urls = {url.rstrip("/") for url in urls}
    web_responses: list[UrlStatus] = []

    initial_url_ctx: set[URLContext] = set()
    async with create_session() as session:  # type: ignore[var-annotated]
        url_contexts = await asyncio.gather(
            *[
                get_url_ctx(
                    url=url,
                    session=session,
                    ntp_offset=ntp_offset,
                    clean_original_urls=clean_original_urls,
                )
                for url in urls_pending
            ],
        )
        for url_ctx in url_contexts:
            if not url_ctx:
                continue
            if isinstance(url_ctx, UrlStatus):
                web_responses.append(url_ctx)
                continue
            initial_url_ctx.add(url_ctx)

    url_ctx_to_analyze = _remove_duplicate_redirected_urls(initial_url_ctx, clean_original_urls)

    web_responses.extend(
        [
            UrlStatus(
                url=url_ctx.original_url,
                status_code=url_ctx.response_status,
            )
            for url_ctx in url_ctx_to_analyze
        ],
    )

    return url_ctx_to_analyze, web_responses


async def analyze(
    *,
    vuln_stores: VulnerabilitiesEphemeralStore,
    urls: set[str],
    exclusions: dict[str, list[str]] | None,
) -> list[UrlStatus]:
    if not any(finding in ctx.SKIMS_CONFIG.checks for _, checks in CHECKS for finding in checks):
        return []

    log_blocking("info", "Performing http requests on %s urls", len(urls))
    urls_ctx_to_analyze, web_responses = await get_contexts_to_analyze(urls=urls)

    unique_count = len(urls_ctx_to_analyze)
    log_blocking("info", "Running analysis over %s available urls", unique_count)

    for url_ctx in urls_ctx_to_analyze:
        analyze_one(url_ctx, vuln_stores, exclusions)

    log_blocking("info", "HTTP analysis completed!")
    return web_responses
