import re
from collections.abc import (
    Callable,
)
from typing import (
    NamedTuple,
)

from lib_dast.http.model import (
    URLContext,
)
from lib_dast.http_headers import (
    as_string,
    content_encoding,
    content_security_policy,
    date,
    permissions_policy,
    referrer_policy,
    server,
    set_cookie,
    strict_transport_security,
    upgrade_insecure_requests,
    www_authenticate,
    x_aspnet_mvc_version,
    x_aspnet_version,
    x_backend_server,
    x_cache,
    x_content_type_options,
    x_frame_options,
    x_permitted_cross_domain_policies,
    x_powered_by,
    x_xss_protection,
)
from lib_dast.http_headers.model import (
    ContentSecurityPolicyHeader,
    Header,
    SetCookieHeader,
)
from lib_dast.methods_enum_http import (
    MethodsEnumHTTP as MethodsEnum,
)
from model.core import (
    FindingEnum,
    HTTPProperties,
    Vulnerabilities,
)
from multidict import (
    MultiDict,
)
from utils.build_vulns import (
    build_inputs_vuln,
    build_metadata,
)
from utils.translations import (
    t,
)

VERSION_IN_LEAK = r"[^\s]+/[\d]"


class HeaderCheckCtx(NamedTuple):
    url_ctx: URLContext
    headers_parsed: MultiDict[str, Header]  # type: ignore[type-arg]


class Location(NamedTuple):
    description: str
    identifier: str


class Locations(NamedTuple):
    locations: list[Location]

    def append(
        self,
        method: MethodsEnum,
        desc_kwargs: dict[str, str] | None = None,
        identifier: str = "",
    ) -> None:
        self.locations.append(
            Location(
                description=t(
                    method.name,
                    **(desc_kwargs or {}),
                ),
                identifier=identifier,
            ),
        )


def _create_vulns(
    locations: Locations,
    header: Header | None,
    ctx: HeaderCheckCtx,
    method: MethodsEnum,
) -> Vulnerabilities:
    return tuple(
        build_inputs_vuln(
            method=method.value,
            stream="home,response,headers",
            what=ctx.url_ctx.original_url,
            where=location.description,
            metadata=build_metadata(
                method=method.value,
                description=(
                    f"{location.description} {t(key='words.in')} {ctx.url_ctx.original_url}"
                ),
                snippet=as_string.snippet(
                    url=ctx.url_ctx.original_url,
                    header=header.name if header else None,
                    value=location.identifier,
                    headers=ctx.url_ctx.headers_raw,
                ),
                http_properties=HTTPProperties(
                    has_redirect=ctx.url_ctx.has_redirect,
                    original_url=ctx.url_ctx.original_url,
                ),
            ),
        )
        for location in locations.locations
    )


def join_header_directives(
    csp_headers: list[ContentSecurityPolicyHeader],
) -> ContentSecurityPolicyHeader:
    joined_directives: dict[str, list[str]] = {}
    for header in csp_headers:
        for key, value in header.directives.items():
            if key not in joined_directives:
                joined_directives[key] = value
            else:
                joined_directives[key] += value

    return ContentSecurityPolicyHeader(
        name="content-security-policy",
        directives=joined_directives,
    )


def _content_security_policy_wild_uri(
    ctx: HeaderCheckCtx,
    method: MethodsEnum,
    value: str,
    directive: str = "default-src",
) -> Vulnerabilities:
    locations = Locations(locations=[])

    for uri in ("data:", "http:", "https:", "://*"):
        if uri == value:
            locations.append(
                method,
                desc_kwargs={
                    "directive": directive,
                    "uri": uri,
                },
            )
    return _create_vulns(
        locations=locations,
        header=None,
        ctx=ctx,
        method=method,
    )


def _content_security_policy_block_all_mixed_content(
    ctx: HeaderCheckCtx,
    header: Header,
    method: MethodsEnum,
) -> Vulnerabilities:
    vulns: Vulnerabilities = ()
    if (
        isinstance(header, ContentSecurityPolicyHeader)
        and "block-all-mixed-content" in header.directives
    ):
        vulns += _generate_vulns_by_method(ctx, header, method)

    return vulns


def _content_security_policy_frame_ancestors(
    ctx: HeaderCheckCtx,
    header: Header,
    method: MethodsEnum,
) -> Vulnerabilities:
    vulns: Vulnerabilities = ()
    if isinstance(header, ContentSecurityPolicyHeader):
        if "frame-ancestors" in header.directives:
            values = header.directives.get("frame-ancestors", [])
            for value in values:
                vulns += _content_security_policy_wild_uri(
                    ctx,
                    MethodsEnum.CONT_SEC_POL_WILD_URI,
                    value,
                    "frame-ancestors",
                )
        else:
            vulns += _generate_vulns_by_method(ctx, header, method)
    return vulns


def _content_security_policy_object_src(
    ctx: HeaderCheckCtx,
    header: Header,
    method: MethodsEnum,
) -> Vulnerabilities:
    vulns: Vulnerabilities = ()

    if isinstance(header, ContentSecurityPolicyHeader) and (
        "object-src" not in header.directives and "default-src" not in header.directives
    ):
        vulns += _generate_vulns_by_method(ctx, header, method)

    return vulns


def _content_security_policy_missing_script(
    ctx: HeaderCheckCtx,
    header: Header,
    method: MethodsEnum,
) -> Vulnerabilities:
    vulns: Vulnerabilities = ()

    if not isinstance(header, ContentSecurityPolicyHeader):
        return vulns

    if not any(directive in header.directives for directive in ["default-src", "script-src"]):
        vulns += _generate_vulns_by_method(ctx, header, method)

    return vulns


def _content_security_policy_unsafe_line(
    ctx: HeaderCheckCtx,
    header: Header,
    method: MethodsEnum,
) -> Vulnerabilities:
    locations = Locations(locations=[])

    if not isinstance(header, ContentSecurityPolicyHeader):
        return ()

    directive = "script-src" if "script-src" in header.directives else "default-src"
    values = header.directives.get(directive, [])

    for value in values:
        if value == "'unsafe-inline'":
            locations.append(method=method)

    return _create_vulns(
        locations=locations,
        header=header,
        ctx=ctx,
        method=method,
    )


def _content_security_policy_hosts_jsonp(
    ctx: HeaderCheckCtx,
    header: Header,
    method: MethodsEnum,
) -> Vulnerabilities:
    locations = Locations(locations=[])

    if not isinstance(header, ContentSecurityPolicyHeader):
        return ()

    directive = "script-src" if "script-src" in header.directives else "default-src"
    whitelisted_endpoints = ("https://www.google.com/recaptcha/",)
    for value in header.directives.get(directive, []):
        for arg in (
            "*.amazonaws.com",
            "*.cloudflare.com",
            "*.cloudfront.net",
            "*.doubleclick.net",
            "*.google.com",
            "*.googleapis.com",
            "*.googlesyndication.com",
            "*.newrelic.com",
            "*.s3.amazonaws.com",
            "*.yandex.ru",
            "ajax.googleapis.com",
            "mc.yandex.ru",
            "vk.com",
            "www.google.com",
        ):
            if arg in value and not any(
                value in safe_endpoint for safe_endpoint in whitelisted_endpoints
            ):
                locations.append(
                    method,
                    desc_kwargs={"host": arg},
                )

    return _create_vulns(
        locations=locations,
        header=header,
        ctx=ctx,
        method=method,
    )


def _content_security_policy_src_wild_uri(
    ctx: HeaderCheckCtx,
    header: Header,
    method: MethodsEnum,
) -> Vulnerabilities:
    if not isinstance(header, ContentSecurityPolicyHeader):
        return ()
    vulns: Vulnerabilities = ()
    directive = "script-src" if "script-src" in header.directives else "default-src"

    for value in header.directives.get(directive, []):
        vulns += _content_security_policy_wild_uri(ctx, method, value, directive)
    return vulns


def _content_security_policy(
    ctx: HeaderCheckCtx,
) -> Vulnerabilities:
    header: Header | None = None
    vulns: Vulnerabilities = ()
    if (csp_headers := ctx.headers_parsed.getall("ContentSecurityPolicyHeader", None)) and (
        header := join_header_directives(csp_headers)
    ):
        vulns += _content_security_policy_block_all_mixed_content(
            ctx,
            header,
            MethodsEnum.CONT_SEC_POL_MIXED_CONTENT,
        )
        vulns += _content_security_policy_frame_ancestors(
            ctx,
            header,
            MethodsEnum.CONT_SEC_POL_FRAME_ANCESTORS,
        )
        vulns += _content_security_policy_object_src(
            ctx,
            header,
            MethodsEnum.CONT_SEC_POL_MISSING_OBJ,
        )
        vulns += _content_security_policy_missing_script(
            ctx,
            header,
            MethodsEnum.CONT_SEC_POL_MISSING_SCRIPT,
        )
        vulns += _content_security_policy_unsafe_line(
            ctx,
            header,
            MethodsEnum.CONT_SEC_POL_UNSAFE_LINE,
        )
        vulns += _content_security_policy_src_wild_uri(
            ctx,
            header,
            MethodsEnum.CONT_SEC_POL_WILD_URI,
        )
        vulns += _content_security_policy_hosts_jsonp(
            ctx,
            header,
            MethodsEnum.CONT_SEC_POL_HOSTS_JSONP,
        )
    else:
        vulns += _generate_vulns_by_method(ctx, header, MethodsEnum.CONTENT_SECURITY_POLICY)

    return vulns


def _upgrade_insecure_requests(
    ctx: HeaderCheckCtx,
) -> Vulnerabilities:
    method = MethodsEnum.UPGRADE_INSECURE_REQUESTS
    locations = Locations(locations=[])
    head: Header | None = None
    if not ctx.headers_parsed.get("UpgradeInsecureRequestsHeader") and (
        not (head := ctx.headers_parsed.get("ContentSecurityPolicyHeader"))
        or "upgrade-insecure-requests" not in head.directives
    ):
        locations.append(method=method)

    return _create_vulns(
        locations=locations,
        header=head,
        ctx=ctx,
        method=method,
    )


def _date(ctx: HeaderCheckCtx) -> Vulnerabilities:
    method = MethodsEnum.DATE
    locations = Locations(locations=[])
    header: Header | None = None

    if (
        (header := ctx.headers_parsed.get("DateHeader"))
        # X-Cache means content is served by a CDN, which may cache
        # a previous server response time
        and ctx.headers_parsed.get("XCacheHeader") is None
        and ctx.url_ctx.timestamp_ntp
    ):
        minutes: float = abs(ctx.url_ctx.timestamp_ntp - header.date.timestamp()) / 60.0

        if minutes > 1:
            locations.append(
                method=method,
                desc_kwargs={
                    "minutes": str(int(minutes)),
                    "minutes_plural": "" if minutes == 1 else "s",
                },
            )

    return _create_vulns(
        locations=locations,
        header=header,
        ctx=ctx,
        method=method,
    )


def _location(ctx: HeaderCheckCtx) -> Vulnerabilities:
    method = MethodsEnum.LOCATION
    locations = Locations(locations=[])
    header: Header | None = None

    if ctx.url_ctx.custom_f023 and "fluidattacks.com" in ctx.url_ctx.custom_f023:
        # Exception: WF(Cannot factorize function)
        locations.append(method=method)

    return _create_vulns(
        locations=locations,
        header=header,
        ctx=ctx,
        method=method,
    )


def _referrer_policy(
    ctx: HeaderCheckCtx,
) -> Vulnerabilities:
    if not ctx.url_ctx.is_html:
        return ()

    header: Header | None = None
    vulns: Vulnerabilities = ()

    if header := ctx.headers_parsed.get("ReferrerPolicyHeader"):
        # The spec says that browsers should read the first supported value
        # starting from right to left.
        safe_values = {
            "no-referrer",
            "same-origin",
            "strict-origin",
            "strict-origin-when-cross-origin",
        }
        danger_values = {
            "origin",
            "unsafe-url",
            "origin-when-cross-origin",
            "no-referrer-when-downgrade",
        }
        for value in reversed(header.values):
            if value in safe_values:
                break

            if value in danger_values:
                desc_params = {"header_value": value}
                vulns += _generate_vulns_by_method(
                    ctx,
                    header,
                    MethodsEnum.REFERRER_POLICY,
                    desc_params,
                )
                break
        else:
            # Experimental or out-of-spec values are also reported as weak
            desc_params = {"header_value": ",".join(header.values)}
            vulns += _generate_vulns_by_method(
                ctx,
                header,
                MethodsEnum.REFERRER_POLICY,
                desc_params,
            )
    else:
        vulns += _generate_vulns_by_method(ctx, header, MethodsEnum.MISSING_REFERRER_POLICY)

    return vulns


def _is_sensitive_cookie(cookie_name: str) -> bool:
    sensitive_names = (
        "session",
        "authorization",
        "phpsessid",
        "id_token",
        "PHPSESSID",
    )
    return any(smell in cookie_name for smell in sensitive_names)


def _set_cookie_httponly(
    ctx: HeaderCheckCtx,
) -> Vulnerabilities:
    method = MethodsEnum.SET_COOKIE_HTTPONLY
    locations = Locations(locations=[])

    headers: list[Header] = ctx.headers_parsed.getall(key="SetCookieHeader", default=[])

    for header in headers:
        if (
            isinstance(header, SetCookieHeader)
            and _is_sensitive_cookie(header.cookie_name)
            and not header.httponly
        ):
            locations.append(
                method=method,
                desc_kwargs={"cookie_name": header.cookie_name},
                identifier=header.raw_content,
            )

    return _create_vulns(
        locations=locations,
        header=None if not headers else headers[0],
        ctx=ctx,
        method=method,
    )


def _set_cookie_samesite(
    ctx: HeaderCheckCtx,
) -> Vulnerabilities:
    method = MethodsEnum.SET_COOKIE_SAMESITE
    locations = Locations(locations=[])

    headers: list[Header] = ctx.headers_parsed.getall(key="SetCookieHeader", default=[])

    for header in headers:
        if (
            isinstance(header, SetCookieHeader)
            and _is_sensitive_cookie(header.cookie_name)
            and header.samesite.lower() == "none"
        ):
            locations.append(
                method=method,
                desc_kwargs={"cookie_name": header.cookie_name},
                identifier=header.raw_content,
            )

    return _create_vulns(
        locations=locations,
        header=None if not headers else headers[0],
        ctx=ctx,
        method=method,
    )


def _set_cookie_secure(
    ctx: HeaderCheckCtx,
) -> Vulnerabilities:
    method = MethodsEnum.SET_COOKIE_SECURE
    locations = Locations(locations=[])

    headers: list[Header] = ctx.headers_parsed.getall(key="SetCookieHeader", default=[])

    for header in headers:
        if (
            isinstance(header, SetCookieHeader)
            and _is_sensitive_cookie(header.cookie_name)
            and not header.secure
        ):
            locations.append(
                method=method,
                desc_kwargs={"cookie_name": header.cookie_name},
                identifier=header.raw_content,
            )

    return _create_vulns(
        locations=locations,
        header=None if not headers else headers[0],
        ctx=ctx,
        method=method,
    )


def _generate_vulns_by_method(
    ctx: HeaderCheckCtx,
    header: Header | None,
    method: MethodsEnum,
    desc_params: dict[str, str] | None = None,
) -> Vulnerabilities:
    locations = Locations(locations=[])
    locations.append(method=method, desc_kwargs=desc_params)
    return _create_vulns(
        locations=locations,
        header=header,
        ctx=ctx,
        method=method,
    )


def _strict_transport_security(
    ctx: HeaderCheckCtx,
) -> Vulnerabilities:
    vulns: Vulnerabilities = ()
    header: Header | None = None

    if val := ctx.headers_parsed.get("StrictTransportSecurityHeader"):
        if val.max_age < 31536000:
            vulns += _generate_vulns_by_method(
                ctx,
                header,
                MethodsEnum.STRICT_TRANSPORT_LOW_MAX_AGE,
            )
        if val.include_sub_domains is not True:
            vulns += _generate_vulns_by_method(
                ctx,
                header,
                MethodsEnum.STRICT_TRANSPORT_INCLUDE_SUBDOMAINS,
            )
    else:
        vulns += _generate_vulns_by_method(ctx, header, MethodsEnum.STRICT_TRANSPORT_SECURITY)

    return vulns


def _www_authenticate(ctx: HeaderCheckCtx) -> Vulnerabilities:
    # You can only see plain-text credentials over http. Avoid FP.
    if not ctx.url_ctx.url.startswith("http://"):
        return ()

    method = MethodsEnum.WWW_AUTHENTICATE
    locations = Locations(locations=[])
    header: Header | None = None

    if (val := ctx.headers_parsed.get("WWWAuthenticate")) and val.type == "basic":
        locations.append(method=method)

    return _create_vulns(
        locations=locations,
        header=header,
        ctx=ctx,
        method=method,
    )


def _x_content_type_options_nosniff(ctx: HeaderCheckCtx) -> Vulnerabilities:
    method = MethodsEnum.X_CONTENT_TYPE_OPTIONS_NOSNIFF
    locations = Locations(locations=[])
    header: Header | None = None

    if (val := ctx.headers_parsed.get("XContentTypeOptionsHeader")) and val.value != "nosniff":
        locations.append(method=method)
    return _create_vulns(
        locations=locations,
        header=header,
        ctx=ctx,
        method=method,
    )


def _x_content_type_options(ctx: HeaderCheckCtx) -> Vulnerabilities:
    method = MethodsEnum.X_CONTENT_TYPE_OPTIONS
    locations = Locations(locations=[])
    header: Header | None = None

    if not ctx.headers_parsed.get("XContentTypeOptionsHeader"):
        locations.append(method=method)

    return _create_vulns(
        locations=locations,
        header=header,
        ctx=ctx,
        method=method,
    )


def _server_header_leaked(ctx: HeaderCheckCtx) -> Vulnerabilities:
    method = MethodsEnum.HTTP_SERVER_HEADER_LEAKED
    locations = Locations(locations=[])
    header: Header | None = None

    if (
        (header := ctx.headers_parsed.get("ServerHeader"))
        and (val := header.value)
        and re.match(VERSION_IN_LEAK, val)
    ):
        locations.append(method=method)

    return _create_vulns(
        locations=locations,
        header=header,
        ctx=ctx,
        method=method,
    )


def _x_aspnet_version_leaked(ctx: HeaderCheckCtx) -> Vulnerabilities:
    method = MethodsEnum.HTTP_X_ASPNET_VERSION_BY_HEADER_LEAKED
    locations = Locations(locations=[])
    header: Header | None = None

    if ctx.headers_parsed.get("XAspNetVersionHeader"):
        locations.append(method=method)

    return _create_vulns(
        locations=locations,
        header=header,
        ctx=ctx,
        method=method,
    )


def _x_aspnet_mvc_version_leaked(ctx: HeaderCheckCtx) -> Vulnerabilities:
    method = MethodsEnum.HTTP_X_ASPNET_MVC_VERSION_HEADER_LEAKED
    locations = Locations(locations=[])
    header: Header | None = None

    if ctx.headers_parsed.get("XAspNetMvcVersionHeader"):
        locations.append(method=method)

    return _create_vulns(
        locations=locations,
        header=header,
        ctx=ctx,
        method=method,
    )


def _x_backend_server_leaked(ctx: HeaderCheckCtx) -> Vulnerabilities:
    method = MethodsEnum.HTTP_X_BACKEND_SERVER_HEADER_LEAKED
    locations = Locations(locations=[])
    header: Header | None = None

    if ctx.headers_parsed.get("XBackendServerHeader"):
        locations.append(method=method)

    return _create_vulns(
        locations=locations,
        header=header,
        ctx=ctx,
        method=method,
    )


def _x_powered_by_header_leaked(ctx: HeaderCheckCtx) -> Vulnerabilities:
    method = MethodsEnum.HTTP_X_POWERED_BY_HEADER_LEAKED
    locations = Locations(locations=[])
    header: Header | None = None

    if (val := ctx.headers_parsed.get("XPoweredByHeader")) and re.match(VERSION_IN_LEAK, val.value):
        locations.append(method=method)

    return _create_vulns(
        locations=locations,
        header=header,
        ctx=ctx,
        method=method,
    )


def _x_xss_protection_enabled(ctx: HeaderCheckCtx) -> Vulnerabilities:
    method = MethodsEnum.X_XSS_PROTECTION_ENABLED
    locations = Locations(locations=[])
    header: Header | None = None

    if (val := ctx.headers_parsed.get("XXSSProtectionHeader")) and val.value == "1":
        locations.append(method=method)

    return _create_vulns(
        locations=locations,
        header=header,
        ctx=ctx,
        method=method,
    )


def _x_permitted_cross_domain_policies(ctx: HeaderCheckCtx) -> Vulnerabilities:
    method = MethodsEnum.X_PERMITTED_CROSS_DOMAIN_POLICIES
    locations = Locations(locations=[])
    header = ctx.headers_parsed.get("XPermittedCrossDomainPolicies")
    if header and str(header.value).lower() != "none":
        locations.append(method=method)

    return _create_vulns(
        locations=locations,
        header=header,
        ctx=ctx,
        method=method,
    )


def _unsafe_xframe_options(ctx: HeaderCheckCtx) -> Vulnerabilities:
    method = MethodsEnum.UNSAFE_HTTP_XFRAME_OPTIONS
    locations = Locations(locations=[])
    header: Header | None = None

    if ctx.headers_parsed.get("XFrameOptionsHeader") and (
        (not (csp_header := ctx.headers_parsed.get("ContentSecurityPolicyHeader")))
        or ("frame-ancestors" not in csp_header.directives)
    ):
        locations.append(method=method)

    return _create_vulns(
        locations=locations,
        header=header,
        ctx=ctx,
        method=method,
    )


def _permissions_policy_not_present(ctx: HeaderCheckCtx) -> Vulnerabilities:
    method = MethodsEnum.HTTP_PERMISSIONS_POLICY_HEADER_NOT_PRESENT
    locations = Locations(locations=[])
    header: Header | None = None

    if not ctx.headers_parsed.get("PermissionsPolicyHeader"):
        locations.append(method=method)

    return _create_vulns(
        locations=locations,
        header=header,
        ctx=ctx,
        method=method,
    )


def get_check_ctx(url: URLContext) -> HeaderCheckCtx:
    headers_parsed: MultiDict[str, Header] = MultiDict(  # type: ignore[type-arg]
        [
            (type(header_parsed).__name__, header_parsed)
            for header_raw_name, header_raw_value in reversed(tuple(url.headers_raw.items()))
            for line in [f"{header_raw_name}: {header_raw_value}"]
            for header_parsed in [
                content_encoding.parse(line),
                content_security_policy.parse(line),
                date.parse(line),
                permissions_policy.parse(line),
                referrer_policy.parse(line),
                server.parse(line),
                set_cookie.parse(line),
                strict_transport_security.parse(line),
                upgrade_insecure_requests.parse(line),
                www_authenticate.parse(line),
                x_aspnet_mvc_version.parse(line),
                x_aspnet_version.parse(line),
                x_backend_server.parse(line),
                x_cache.parse(line),
                x_content_type_options.parse(line),
                x_frame_options.parse(line),
                x_permitted_cross_domain_policies.parse(line),
                x_powered_by.parse(line),
                x_xss_protection.parse(line),
            ]
            if header_parsed is not None
        ],
    )

    return HeaderCheckCtx(
        url_ctx=url,
        headers_parsed=headers_parsed,
    )


CHECKS: dict[
    FindingEnum,
    list[Callable[[HeaderCheckCtx], Vulnerabilities]],
] = {
    FindingEnum.F015: [_www_authenticate],
    FindingEnum.F023: [_location],
    FindingEnum.F043: [
        _content_security_policy,
        _upgrade_insecure_requests,
    ],
    FindingEnum.F064: [_date],
    FindingEnum.F071: [_referrer_policy],
    FindingEnum.F128: [_set_cookie_httponly],
    FindingEnum.F129: [_set_cookie_samesite],
    FindingEnum.F130: [_set_cookie_secure],
    FindingEnum.F131: [_strict_transport_security],
    FindingEnum.F132: [
        _x_content_type_options,
        _x_content_type_options_nosniff,
    ],
    FindingEnum.F135: [_x_xss_protection_enabled],
    FindingEnum.F137: [_x_permitted_cross_domain_policies],
    FindingEnum.F152: [_unsafe_xframe_options],
    FindingEnum.F235: [
        _server_header_leaked,
        _x_powered_by_header_leaked,
        _x_aspnet_version_leaked,
        _x_aspnet_mvc_version_leaked,
        _x_backend_server_leaked,
    ],
    FindingEnum.F440: [_permissions_policy_not_present],
}
