from collections.abc import (
    Callable,
    Iterator,
)
from functools import (
    wraps,
)
from itertools import (
    chain,
)
from os.path import (
    dirname,
    isfile,
    join,
    split,
    splitext,
)

import ctx
from cvss import (
    CVSS3,
    CVSS4,
    CVSS3Error,
    CVSS4Error,
)
from fluidattacks_core.serializers.snippet import (
    SnippetViewport,
    make_snippet,
)
from lib_sca.methods_enum import (
    MethodsEnumSCA as MethodsEnum,
)
from lib_sca.model import (
    Advisory,
    Dependency,
    identify_platform,
)
from lib_sca.vulnerabilities import (
    get_vulnerabilities,
)
from model.core import (
    ScaAdvisoriesMetadata,
    Vulnerabilities,
    Vulnerability,
)
from utils.build_vulns import (
    build_lines_vuln,
    build_metadata,
)
from utils.cvss import (
    set_default_temporal_scores,
    set_default_threat_scores,
)
from utils.function import (
    shield_blocking,
)
from utils.logs import (
    log_blocking,
)
from utils.string_handlers import (
    is_exclusion,
)
from utils.translations import (
    t,
)

NON_REQUIREMENT_CHARS = ("-", "#", ".", "/")


def get_path_from_attrs(*args: tuple[str, str] | dict) -> str:
    path = ""
    if len(args) > 1:
        if isinstance(args[1], dict) and "path" in args[1]:
            path = args[1]["path"]
    else:
        path = args[0][1]
    return path


SHIELD_BLOCKING: Callable = shield_blocking(
    on_error_return=(),
    get_path_from_attrs=get_path_from_attrs,
)

POSSIBLE_LOCK_FILES = {
    "Pipfile": ["Pipfile.lock"],
    "Cargo.toml": ["Cargo.lock"],
    "conanfile.txt": ["conan.lock"],
    "conanfile.py": ["conan.lock"],
    "composer.json": ["composer.lock"],
    "mix.exs": ["mix.lock"],
    "packages.config": ["packages.lock.json"],
    "package.json": [
        "package-lock.json",
        "yarn.lock",
        "pnpm-lock.yaml",
        # For angular libraries
        "ng-package.json",
    ],
    "Gemfile": ["Gemfile.lock", "gems.locked"],
    "pyproject.toml": ["poetry.lock"],
}

ROOT_LOCK_FILE = {
    "mix.exs": "mix.lock",
    "package.json": "yarn.lock",
}


def extract_file_info(path: str) -> tuple[str, str, str]:
    _, file_info = split(path)
    file_name, file_extension = splitext(file_info)  # noqa: PTH122
    file_extension = file_extension.lstrip(".")
    return file_info, file_name, file_extension


def extract_sca_paths(paths: tuple[str, ...]) -> tuple[str, ...]:
    sca_paths = []
    for path in paths:
        file_info, file_name, file_extension = extract_file_info(path)
        platform = identify_platform(file_name, file_extension, path)

        if not platform:
            continue

        lock_file_found = False
        pkg_dir_path = dirname(path)  # noqa: PTH120
        lock_files = POSSIBLE_LOCK_FILES.get(file_info, [])
        for lock_file in lock_files:
            lock_path = join(pkg_dir_path, lock_file)  # noqa: PTH118
            if isfile(lock_path):  # noqa: PTH113
                lock_file_found = True
                break

        root_lock_file = ROOT_LOCK_FILE.get(file_info)
        if root_lock_file and isfile(root_lock_file):  # noqa: PTH113
            lock_file_found = True

        if not lock_file_found:
            sca_paths.append(path)

    return tuple(sca_paths)


def get_vulnerabilities_for_incomplete_deps(
    content: str,
    iterator: Iterator[str],
    path: str,
    method: MethodsEnum,
) -> Vulnerabilities:
    snippet = make_snippet(
        content=content,
        viewport=SnippetViewport(column=0, line=0),
    )

    results: Vulnerabilities = tuple(
        build_lines_vuln(
            method=method.value,
            what=f"{path} (missing dependency: {dep})",
            where="0",
            metadata=build_metadata(
                method=method.value,
                description=(
                    f"{t(key=method.name, name=dep)} "
                    f"{t(key='words.in')} "
                    f"{ctx.SKIMS_CONFIG.namespace}/{path}"
                ),
                snippet=snippet,
                sca_metadata=ScaAdvisoriesMetadata(
                    package=dep,
                    vulnerable_version="",
                    cve=[],
                ),
            ),
        )
        for dep in iterator
    )

    return results


def get_sca_metadata(
    dependency: Dependency,
    advisories: list[Advisory],
    *,
    is_new_sca_report: bool = False,
) -> ScaAdvisoriesMetadata:
    cve_list = [adv.id for adv in advisories] + [
        adv.alternative_id for adv in advisories if adv.alternative_id
    ]
    epss = [
        float(adv.epss) for adv in advisories if adv.epss and adv.epss.replace(".", "").isdigit()
    ] or [float(0)]

    return ScaAdvisoriesMetadata(
        cve=cve_list,
        vulnerable_version=dependency.version,
        package=dependency.name,
        epss=int(max(epss) * 100),
        is_new_sca_report=is_new_sca_report,
    )


def max_cvss_list(target: list) -> str | None:
    if target:
        try:
            scores = [CVSS3(elem).temporal_score for elem in target]
            return target[max(range(len(scores)), key=scores.__getitem__)]
        except CVSS3Error:
            log_blocking("error", "Could not generate the CVSS3 score")
    return None


def max_cvss_v4_list(target: list) -> str | None:
    if target:
        try:
            scores = [CVSS4(elem).base_score for elem in target]
            return target[max(range(len(scores)), key=scores.__getitem__)]
        except CVSS4Error:
            log_blocking("error", "Could not generate the CVSS4 score")
    return None


def get_vulnerability_for_dependency(
    content: str,
    path: str,
    dependency: Dependency,
) -> Vulnerability | None:
    advisories = get_vulnerabilities(dependency.platform, dependency.name, dependency.version)
    if not advisories:
        return None
    cwe_ids = list(set(chain(*[adv.cwe_ids for adv in advisories if adv.cwe_ids])))
    max_cvss = set_default_temporal_scores(
        max_cvss_list([adv.severity for adv in advisories if adv.severity]),
    )
    max_cvss_v4 = set_default_threat_scores(
        max_cvss_v4_list([adv.severity_v4 for adv in advisories if adv.severity_v4]),
    )
    sca_metadata = get_sca_metadata(dependency, advisories)
    return build_lines_vuln(
        method=dependency.found_by.value,
        what=path,
        where=str(dependency.locations.line),
        metadata=build_metadata(
            method=dependency.found_by.value,
            description=(
                t(
                    key=dependency.found_by.name,
                    product=dependency.name,
                    version=dependency.version,
                    cve=sorted(sca_metadata.cve),
                )
                + f" {t(key='words.in')} "
                f"{ctx.SKIMS_CONFIG.namespace}/{path}"
            ),
            snippet=make_snippet(
                content=content,
                viewport=SnippetViewport(line=int(dependency.locations.line), column=0),
            ),
            cvss=max_cvss,
            cvss_v4=max_cvss_v4,
            cwe_ids=cwe_ids,
            sca_metadata=sca_metadata,
            skip=is_exclusion(content, int(dependency.locations.line)),
        ),
    )


def translate_dependencies_to_vulnerabilities(
    *,
    content: str,
    dependencies: Iterator[Dependency],
    path: str,
) -> Vulnerabilities:
    results: Vulnerabilities = tuple(
        filter(
            None,
            (get_vulnerability_for_dependency(content, path, dep) for dep in dependencies),
        ),
    )
    return results


def locations_iterator_to_vulns(
    method: MethodsEnum,
) -> Callable[[Callable], Callable[[str, str], Vulnerabilities]]:
    def resolve_locations(
        vulnerabilities_finder_function: Callable,
    ) -> Callable[[str, str], Vulnerabilities]:
        @wraps(vulnerabilities_finder_function)
        def resolve_vulns(content: str, path: str) -> Vulnerabilities:
            return tuple(
                build_lines_vuln(
                    method=method.value,
                    what=path,
                    where=str(line_no),
                    metadata=build_metadata(
                        method=method.value,
                        description=(
                            f"{t(key=method.name)} {t(key='words.in')} "
                            f"{ctx.SKIMS_CONFIG.namespace}/{path}"
                        ),
                        snippet=make_snippet(
                            content=content,
                            viewport=SnippetViewport(column=column_no, line=line_no),
                        ),
                    ),
                )
                for line_no, column_no in vulnerabilities_finder_function(content, path)
            )

        return resolve_vulns

    return resolve_locations
