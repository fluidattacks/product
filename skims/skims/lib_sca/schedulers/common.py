import re

from cvss import (
    CVSS3,
    CVSS4,
    CVSS3Error,
    CVSS4Error,
)
from lib_sca.model import (
    Advisory,
)
from semantic_version import (
    NpmSpec,
)
from utils.logs import (
    log_blocking,
)

VALID_RANGES = ("=", "<", ">", ">=", "<=")

VERSION_SEPARATORS: re.Pattern[str] = re.compile("([-+])")
SPACE_PATTERN: re.Pattern[str] = re.compile(r"\s+")
RANGES_SEPARATOR: re.Pattern[str] = re.compile(r"\s*\|\|\s*")
TAGS: re.Pattern[str] = re.compile(r"(?P<num>\d+)(?P<tag>[a-zA-Z].+)")
SEMVER_PLATFORMS = {"conan", "go", "npm", "nuget"}


def coerce(constraint: str) -> str:
    constraint_tokens = VERSION_SEPARATORS.split(constraint)
    version_coerced = constraint_tokens[0].split(".")
    last_item = version_coerced[-1]
    tags = ""
    if len(constraint_tokens) < 2 and (match_tag := TAGS.search(last_item)):
        version_coerced[-1] = match_tag.group("num")
        tags = f"-{match_tag.group('tag')}"
    else:
        tags = "".join(constraint_tokens[1:])
    first_item = version_coerced[0]
    desired_length = 3
    coerced_length = len(version_coerced)
    if "v" in first_item:
        version_coerced[0] = first_item.replace("v", "")
    if coerced_length < desired_length:
        filler_item = "*" if first_item.startswith("~") else "0"
        version_coerced += [filler_item] * (desired_length - coerced_length)
    for index in range(3):
        part = version_coerced[index]
        if part.startswith("0") and len(part) > 1:
            version_coerced[index] = part.lstrip("0")

    return f"{'.'.join(version_coerced[:3])}{tags if tags else '-'}"


def coerce_range(range_str: str) -> str:
    range_str = SPACE_PATTERN.sub(" ", range_str)
    range_tokens = RANGES_SEPARATOR.split(range_str)
    arr_range_tokens = []
    for tokens in range_tokens:
        if tokens.startswith("<"):
            tokens = f">=0.0.0 {tokens}"  # noqa: PLW2901
        coerced_tokens = " ".join(coerce(token) for token in tokens.split())
        arr_range_tokens.append(coerced_tokens)
    return "||".join(arr_range_tokens)


def get_clean_severity(severity: str | None) -> str | None:
    if not severity or not severity.startswith("CVSS:3"):
        return None
    try:
        CVSS3(severity)
    except CVSS3Error:
        return None
    else:
        return severity


def get_clean_severity_v4(severity: str | None) -> str | None:
    if not severity or not severity.startswith("CVSS:4"):
        return None
    try:
        CVSS4(severity)
    except CVSS4Error:
        return None
    else:
        return severity


def _check_vulnerable_version(versions: str, package_manager: str) -> bool:
    if not versions:
        return False
    version_list = versions.split(" || ")
    for version_range in version_list:
        range_ver = version_range.split()
        if not all(ver.startswith(VALID_RANGES) for ver in range_ver):
            return False

    if package_manager in SEMVER_PLATFORMS:
        try:
            coerced_versions = coerce_range(versions)
            NpmSpec(coerced_versions)
        except ValueError:
            return False
    return True


def get_clean_vulnerable_version(advisory: Advisory) -> str | None:
    vulnerable_version = advisory.vulnerable_version
    versions = advisory.vulnerable_version.split(" || ")
    if ">=0" in versions and len(versions) > 1:
        fixed_vers = [ver for ver in versions if ver != ">=0"]
        vulnerable_version = " || ".join(fixed_vers)

    if any(symbol in vulnerable_version for symbol in ["=v", ">v", "<v"]):
        vulnerable_version = (
            vulnerable_version.replace("=v", "=").replace(">v", ">").replace("<v", "<")
        )

    if not _check_vulnerable_version(vulnerable_version, advisory.package_manager):
        log_blocking(
            "error",
            "Bad vulnerable version: %s of advisory %s from %s:%s",
            vulnerable_version,
            advisory.id,
            advisory.package_manager,
            advisory.package_name,
        )
        return None

    return vulnerable_version
