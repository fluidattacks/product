import glob
import json
from contextlib import suppress
from pathlib import (
    Path,
)

from lib_sca.model import (
    Advisory,
)
from lib_sca.schedulers.repositories.advisory_database import (
    get_final_range,
)
from lib_sca.schedulers.repositories.common import (
    get_severity_cvss_v4,
)
from utils.logs import (
    log_blocking,
)

URL_OSSF_ADVISORY_DATABASE = "https://github.com/ossf/malicious-packages.git"
PLATFORMS = {
    "crates.io": "cargo",
    "npm": "npm",
    "pypi": "pip",
    "rubygems": "gem",
}


def get_pkg_info(affected: list[dict]) -> tuple[str, str, str] | None:
    pkg_obj = affected[0]
    package: dict = pkg_obj.get("package") or {}
    ecosystem = str(package.get("ecosystem"))
    if (platform := ecosystem.lower()) not in PLATFORMS:
        return None

    pkg_name = str(package.get("name")).lower()
    final_range = get_final_range(pkg_obj)
    return (PLATFORMS[platform], pkg_name, final_range)


def get_advisory_ossf(
    existing_advisories: set[tuple[str, str, str]],
    tmp_dirname: str,
) -> list[Advisory]:
    filenames = sorted(
        glob.glob(  # noqa: PTH207
            f"{tmp_dirname}/osv/malicious/**/*.json",
            recursive=True,
        ),
    )
    log_blocking("info", "Processing OpenSSF Malicious Packages")
    raw_ossf_advisories = []

    for filename in filenames:
        with (
            suppress(IsADirectoryError, json.JSONDecodeError),
            Path(filename).open(encoding="utf-8") as stream,
        ):
            from_json: dict = json.load(stream)
            adv_info = get_pkg_info(from_json.get("affected", []))
            adv_malware_id = str(from_json.get("id", ""))

            if not adv_info or not adv_malware_id.startswith("MAL-"):
                continue

            pkg_manager, pkg_name, vulnerable_version = adv_info
            alias_id = from_json["aliases"][0] if from_json.get("aliases") else None

            if (pkg_manager, pkg_name, alias_id) in existing_advisories:
                continue
            cwe_ids = from_json.get("database_specific", {}).get("cwe_ids", None)
            severity = from_json.get("severity", [])

            raw_ossf_advisories.append(
                Advisory(
                    id=adv_malware_id,
                    package_manager=pkg_manager,
                    package_name=pkg_name,
                    severity=severity[0].get("score") if len(severity) > 0 else None,
                    severity_v4=get_severity_cvss_v4(severity),
                    source="https://github.com/ossf/malicious-packages",
                    vulnerable_version=vulnerable_version,
                    cwe_ids=cwe_ids or ["CWE-506"],
                    created_at=from_json.get("published"),
                    modified_at=from_json.get("modified"),
                    details=from_json.get("summary"),
                    cve_finding="F448",
                ),
            )

    return raw_ossf_advisories
