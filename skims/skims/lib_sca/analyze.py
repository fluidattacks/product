import time
from concurrent.futures.process import (
    ProcessPoolExecutor,
)
from typing import (
    Any,
)

import ctx
from lib_sca import (
    f011,
    f120,
    f393,
    f431,
    new_cve_vulns,
)
from lib_sca.advisories_database import DATABASE
from lib_sca.common import (
    extract_file_info,
    extract_sca_paths,
)
from model.core import (
    FindingEnum,
    SkimsConfig,
    Vulnerabilities,
    Vulnerability,
)
from utils.fs import (
    get_file_content_block,
    resolve_paths,
)
from utils.logs import (
    log_blocking,
    log_to_remote_handled,
)
from utils.state import (
    ExecutionStores,
)

CHECKS: tuple[tuple[FindingEnum, Any], ...] = (
    (FindingEnum.F011, f011.analyze),
    (FindingEnum.F120, f120.analyze),
    (FindingEnum.F393, f393.analyze),
    (FindingEnum.F431, f431.analyze),
)


def exclude_reports(
    reports: tuple[Vulnerabilities, ...],
    exclusions: list[str] | None,
) -> list[Vulnerability]:
    modified_vulns = []
    for group_vuln in reports:
        for vuln in group_vuln:
            if (
                exclusions
                and (sca_metadata := vuln.skims_metadata.sca_metadata)
                and sca_metadata.package in exclusions
            ):
                modified_vulns.append(vuln.set_exclusion_value(is_exclusion=True))
            else:
                modified_vulns.append(vuln)
    return modified_vulns


def analyze_one_path(
    *,
    path: str,
    finding_checks: set[FindingEnum],
    file_content: str,
    exclusions: list[str] | None,
    use_new_sca: bool = False,
) -> list[Vulnerability]:
    _, file_name, file_extension = extract_file_info(path)
    vulnerabilities: list[Vulnerability] = []

    for finding, method in CHECKS:
        if finding not in finding_checks:
            continue

        method_vulns = method(
            file_content=file_content,
            file_extension=file_extension,
            file_name=file_name,
            path=path,
        )
        vulnerabilities.extend(exclude_reports(method_vulns, exclusions))

    if use_new_sca:
        cve_vulns = new_cve_vulns.analyze(
            file_content=file_content,
            file_extension=file_extension,
            file_name=file_name,
            path=path,
        )
        vulnerabilities.extend(exclude_reports(cve_vulns, exclusions))

    return vulnerabilities


def _analyze_one_path(
    *,
    config: SkimsConfig,
    path: str,
    checks: set[FindingEnum],
    exclusions: list[str] | None,
) -> list[Vulnerability]:
    # Re-export config to gain visibility in child subprocesses
    ctx.SKIMS_CONFIG = config
    content = get_file_content_block(path)

    return analyze_one_path(
        path=path,
        finding_checks=checks,
        file_content=content,
        exclusions=exclusions,
        use_new_sca=config.sca.use_new_sca,
    )


async def analyze(
    *,
    stores: ExecutionStores,
    sca_exclusions: list[str] | None,
) -> None:
    init_time = time.time()

    if (
        not any(finding in ctx.SKIMS_CONFIG.checks for finding, _ in CHECKS)
        and not ctx.SKIMS_CONFIG.sca.use_new_sca
    ):
        # No findings will be executed, early abort
        return

    paths = resolve_paths(
        working_dir=ctx.SKIMS_CONFIG.working_dir,
        include=ctx.SKIMS_CONFIG.sca.include,
        exclude=ctx.SKIMS_CONFIG.sca.exclude,
    )

    all_paths = extract_sca_paths(paths.ok_paths)

    if not all_paths:
        return

    log_blocking("info", "Running SCA analysis on %s paths", len(all_paths))

    if (
        set(ctx.SKIMS_CONFIG.checks).intersection({FindingEnum.F011, FindingEnum.F393})
        or ctx.SKIMS_CONFIG.sca.use_new_sca
    ):
        log_blocking("info", "Downloading advisory database")
        DATABASE.initialize()

    with ProcessPoolExecutor(max_workers=ctx.CPU_CORES) as executor:
        futures_to_execute = {
            executor.submit(
                _analyze_one_path,
                config=ctx.SKIMS_CONFIG,
                path=path,
                checks=ctx.SKIMS_CONFIG.checks,
                exclusions=sca_exclusions,
            ): path
            for path in all_paths
        }

        for future, path in futures_to_execute.items():
            try:
                results = future.result(timeout=240)
                stores.vuln_stores.store_vulns(results)
            except TimeoutError:
                msg = f"Future timed out in SCA execution on {path}"
                log_to_remote_handled(msg=msg, severity="warning")
                future.cancel()

    log_blocking("info", "SCA analysis completed!")

    sca_time = ("sca", time.time() - init_time)
    stores.technique_stores["technique_times"].store(sca_time)
