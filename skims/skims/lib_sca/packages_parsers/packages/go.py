import re
from collections.abc import (
    Iterable,
    Iterator,
)

from lib_sca.methods_enum import (
    MethodsEnumSCA as MethodsEnum,
)
from lib_sca.model import (
    Dependency,
    DependencyLocation,
    Platform,
)

GO_DIRECTIVE: re.Pattern[str] = re.compile(r"(?P<directive>require|replace) \(")
GO_MOD_DEP: re.Pattern[str] = re.compile(r"^\s+(?P<product>.+?/[\w\-\.~]+?)\sv(?P<version>\S+)")
GO_REPLACE: re.Pattern[str] = re.compile(
    r"^\s+(?P<old_prod>.+?/[\w\-\.~]+?)(\sv(?P<old_ver>\S+))?\s=>"
    r"\s(?P<new_prod>.+?/[\w\-\.~]+?)(\sv(?P<new_ver>\S+))?$",
)
GO_REP_DEP: re.Pattern[str] = re.compile(
    r"replace\s(?P<old_prod>.+?/[\w\-\.~]+?)(\sv(?P<old_ver>\S+))?\s=>"
    r"\s(?P<new_prod>.+?/[\w\-\.~]+?)(\sv(?P<new_ver>\S+))?$",
)
GO_REQ_MOD_DEP: re.Pattern[str] = re.compile(
    r"require\s(?P<product>.+?/[\w\-\.~]+?)\sv(?P<version>\S+)",
)
GO_VERSION: re.Pattern[str] = re.compile(r"\ngo (?P<major>\d)\.(?P<minor>\d+)(\.\d+)?\n")


def add_require(
    matched: re.Match[str],
    req_dict: dict[str, Dependency],
    line_number: int,
    path: str,
) -> None:
    product = matched.group("product")
    version = matched.group("version")
    req_dict[product] = Dependency(
        name=product,
        locations=DependencyLocation(path=path, line=str(line_number)),
        version=version,
        platform=Platform.GO,
        found_by=MethodsEnum.GO_MOD,
    )


def replace_req(
    req_dict: dict[str, Dependency],
    replace_list: Iterable[tuple[re.Match[str], int]],
    path: str,
) -> Iterator[Dependency]:
    for matched, line_number in replace_list:
        old_pkg = matched.group("old_prod")
        old_version = matched.group("old_ver")
        repl_pkg = matched.group("new_prod")
        version = matched.group("new_ver")
        if old_pkg in req_dict:
            if old_version is None and version is None:
                version = req_dict[old_pkg].version
            if not version or (old_version and req_dict[old_pkg].version != old_version):
                continue
            req_dict[old_pkg] = Dependency(
                name=repl_pkg,
                locations=DependencyLocation(path=path, line=str(line_number)),
                version=version,
                platform=Platform.GO,
                found_by=MethodsEnum.GO_MOD,
            )
    return iter(req_dict.values())


def resolve_go_deps(content: str, path: str) -> Iterator[Dependency]:
    go_req_directive: str = ""
    replace_list: list[tuple[re.Match[str], int]] = []
    req_dict: dict[str, Dependency] = {}

    for line_number, line in enumerate(content.splitlines(), 1):
        if matched := GO_REQ_MOD_DEP.search(line):
            add_require(matched, req_dict, line_number, path)
        elif replace := GO_REP_DEP.search(line):
            replace_list.append((replace, line_number))
        elif not go_req_directive:
            if directive := GO_DIRECTIVE.match(line):
                go_req_directive = directive.group("directive")
        elif go_req_directive == "replace":
            if replace := GO_REPLACE.search(line):
                replace_list.append((replace, line_number))
                continue
            go_req_directive = ""
        elif matched := GO_MOD_DEP.search(line):
            add_require(matched, req_dict, line_number, path)
        else:
            go_req_directive = ""
    return replace_req(req_dict, replace_list, path)


def go_mod(content: str, path: str) -> Iterator[Dependency]:
    go_version = GO_VERSION.search(content)
    if not go_version:
        return iter([])
    major = int(go_version.group("major"))
    minor = int(go_version.group("minor"))
    if major >= 2 or (major == 1 and minor >= 17):
        return resolve_go_deps(content, path)
    return iter([])
