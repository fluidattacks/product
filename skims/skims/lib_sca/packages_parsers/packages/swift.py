from collections.abc import (
    Iterator,
)

from lib_sca.methods_enum import (
    MethodsEnumSCA as MethodsEnum,
)
from lib_sca.model import (
    Dependency,
    DependencyLocation,
    Platform,
)
from lib_sca.packages_parsers.json_custom_parser import (
    loads_blocking as json_loads_blocking,
)


def swift_package_deps(content: str, path: str) -> Iterator[Dependency]:
    content_json = json_loads_blocking(content, default={})
    packages = [
        package
        for key, value in content_json.items()
        if key["item"] == "pins"
        for package in value["item"]
    ]

    pkgs_names = [
        pkg_name
        for package in packages
        for key, pkg_name in package.items()
        if key["item"] == "identity"
    ]

    pkgs_vers = [
        pkg_ver[1]
        for package in packages
        for key, pkg_state in package.items()
        if key["item"] == "state"
        for pkg_ver in pkg_state.items()
        if pkg_ver[0]["item"] == "version"
    ]
    for pkg_name, version in zip(pkgs_names, pkgs_vers, strict=False):
        yield Dependency(
            name=pkg_name["item"],
            locations=DependencyLocation(
                path=path,
                line=str(
                    pkg_name["line"],
                ),
            ),
            version=version["item"],
            platform=Platform.SWIFT,
            found_by=MethodsEnum.SWIFT_PACKAGE_DEPS,
        )
