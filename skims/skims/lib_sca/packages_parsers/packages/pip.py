from collections.abc import (
    Iterator,
)
from contextlib import (
    suppress,
)
from pathlib import (
    Path,
)

import requirements
from lib_sca.methods_enum import (
    MethodsEnumSCA as MethodsEnum,
)
from lib_sca.model import (
    Dependency,
    DependencyLocation,
    Platform,
)

OPERATOR_ORDER = {"==": 1, "===": 1, "~=": 1, ">=": 2, ">": 2, "<": 3, "<=": 3}


def get_dep_version_range(dep_specs: list[tuple[str, str]]) -> str | None:
    version_range = ""
    ordered_specs = sorted(dep_specs, key=lambda x: OPERATOR_ORDER.get(x[0], 1))
    for operator, version in ordered_specs:
        if operator not in OPERATOR_ORDER:
            return None

        if operator in {"==", "~="}:
            version_range = version
            break
        version_range += f"{operator}{version} "
    return version_range.rstrip()


def get_parsed_dependency(line: str) -> tuple[str, str] | None:
    with suppress(Exception):
        parsed_dep = list(requirements.parse(line))[0]  # noqa: RUF015

        if not parsed_dep.specs or not (version := get_dep_version_range(parsed_dep.specs)):
            return None

        return str(parsed_dep.name), version
    return None


def pip_requirements_txt(content: str, path: str) -> Iterator[Dependency]:
    try:
        with Path(path).open("rb") as file:
            file.read().decode("utf-8")
    except UnicodeDecodeError:
        return
    deps_found = False
    for line_number, line in enumerate(content.splitlines(), 1):
        parsed_dep = get_parsed_dependency(line)

        # Avoid parsing big txt files that have nothing to do with pip
        if not parsed_dep and not deps_found and line_number > 3:
            return

        if not parsed_dep:
            continue

        deps_found = True
        product, version = parsed_dep
        yield Dependency(
            name=product,
            locations=DependencyLocation(
                path=path,
                line=str(
                    line_number,
                ),
            ),
            version=version,
            platform=Platform.PIP,
            found_by=MethodsEnum.PIP_REQUIREMENTS_TXT,
        )
