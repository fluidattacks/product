import json
from collections.abc import (
    Iterator,
)
from contextlib import (
    suppress,
)
from urllib.parse import (
    urlparse,
)

from frozendict import (
    frozendict,
)
from lib_sca.methods_enum import (
    MethodsEnumSCA as MethodsEnum,
)
from lib_sca.model import (
    Dependency,
    DependencyLocation,
    Platform,
)
from lib_sca.packages_parsers.json_custom_parser import (
    loads_blocking as json_loads_blocking,
)


def sbom_deps(content: str, path: str) -> Iterator[Dependency]:
    with suppress(ValueError):
        data = json.loads(content, strict=False)
        if not isinstance(data, dict):
            return
        if "SPDXID" in data:
            yield from spdx_deps(content, path)
        if "bomFormat" in data and data["bomFormat"] == "CycloneDX":
            yield from cyclonedx_deps(content, path)


def infer_platform(value: frozendict) -> Platform:
    purl: str = ""
    if isinstance(value["item"], str):
        purl = value["item"]
    else:
        for ref in value["item"]:
            for ref_key, ref_value in ref.items():
                if ref_key["item"] == "referenceLocator":
                    purl = ref_value["item"]
    try:
        return Platform(urlparse(purl).path.split("/")[0].upper())
    except ValueError:
        return Platform.SBOM


def process_component(
    component: frozendict,
    path: str,
    method: MethodsEnum,
) -> Iterator[Dependency]:
    line: str = ""
    name: str = ""
    version: str = ""
    platform: Platform | None = None

    for dep_info, value in component.items():
        if dep_info["item"] == "name":
            line = dep_info["line"]
            name = value["item"]
        elif dep_info["item"] == "purl" or dep_info["item"] == "externalRefs":
            platform = infer_platform(value)
        elif dep_info["item"] == "version" or dep_info["item"] == "versionInfo":
            version = value["item"]

    if not platform:
        return

    yield Dependency(
        name=name,
        locations=DependencyLocation(
            path=path,
            line=str(line),
        ),
        version=version,
        platform=platform,
        found_by=method,
    )


def spdx_deps(content: str, path: str) -> Iterator[Dependency]:
    content_json = json_loads_blocking(content, default={})
    for key in content_json:
        if key["item"] == "packages":
            for component in content_json[key]["item"]:
                yield from process_component(component, path, MethodsEnum.SPDX_DEPS)


def cyclonedx_deps(content: str, path: str) -> Iterator[Dependency]:
    content_json = json_loads_blocking(content, default={})
    for key in content_json:
        if key["item"] == "components":
            for component in content_json[key]["item"]:
                yield from process_component(component, path, MethodsEnum.CYCLONEDX_DEPS)
