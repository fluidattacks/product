import json
import os
import re
from collections.abc import (
    Iterator,
)
from contextlib import (
    suppress,
)
from enum import (
    Enum,
)
from typing import (
    NamedTuple,
)

from frozendict import (
    frozendict,
)
from lib_sca.methods_enum import (
    MethodsEnumSCA as MethodsEnum,
)
from lib_sca.model import (
    Dependency,
    DependencyLocation,
    Platform,
)
from lib_sca.packages_parsers.cfn_custom_parser import (
    Loader,
    load_as_yaml,
)
from lib_sca.packages_parsers.json_custom_parser import (
    loads_blocking as json_loads_blocking,
)
from more_itertools import (
    windowed,
)
from pyarn import (
    lockfile,
)
from utils.fs import (
    get_file_content_block,
)
from utils.logs import (
    log_blocking,
)

VERSION_PATTERN = re.compile(r"(\d+\.\d+\.\d+(-[0-9A-Za-z\.]+)?)")


class DependenciesTypeEnum(Enum):
    DEV = "devDependencies"
    PROD = "dependencies"


class NpmDepInfo(NamedTuple):
    version: str
    product_line: int
    version_line: int


def npm_package_json(content: str, path: str, *, is_dev: bool = False) -> Iterator[Dependency]:
    item_key = (
        ("devDependencies", MethodsEnum.NPM_PKG_JSON)
        if is_dev
        else ("dependencies", MethodsEnum.NPM_PACKAGE_JSON)
    )
    content_json = json_loads_blocking(content, default={})

    ane_keys = {"id", "docUrl", "type"}
    is_ane = all(any(key["item"] == ane_key for key in content_json) for ane_key in ane_keys)
    if is_ane:
        return iter([])

    dependencies: Iterator[Dependency] = (
        Dependency(
            name=product["item"],
            locations=DependencyLocation(path=path, line=str(product["line"])),
            version=version["item"],
            platform=Platform.NPM,
            found_by=item_key[1],
        )
        for key in content_json
        if not isinstance(key, str) and key["item"] == item_key[0]
        for product, version in content_json[key].items()
    )

    return dependencies


def should_include(
    direct_deps: bool,  # noqa: FBT001
    spec: frozendict,
    dev_dep: bool = False,  # noqa: FBT001,FBT002
) -> bool:
    is_dev: bool = False

    for spec_key, spec_val in spec.items():
        if isinstance(spec_key, dict) and spec_key.get("item") == "dev":
            is_dev = spec_val.get("item")
            break
    if not dev_dep:
        return any(
            [
                direct_deps and not is_dev,
                not direct_deps and not is_dev,
            ],
        )
    return any(
        [
            direct_deps and is_dev,
            not direct_deps and not is_dev,
        ],
    )


def process_dependency(
    product: frozendict,
    spec: frozendict,
    name: str | None,
    path: str,
    *,
    is_dev: bool = False,
) -> Iterator[Dependency]:
    method = MethodsEnum.NPM_PKG_LOCK_JSON if is_dev else MethodsEnum.NPM_PACKAGE_LOCK_JSON
    dependencies = [
        dep[0]["item"]
        for spec_key, spec_val in spec.items()
        for dep in spec_val.items()
        if isinstance(spec_key, dict) and spec_key.get("item") == "dependencies" and dep
    ]
    for spec_key, spec_val in spec.items():
        if isinstance(spec_key, dict) and spec_key.get("item") == "version":
            if name:
                yield Dependency(
                    name=name,
                    locations=DependencyLocation(path=path, line=str(product["line"])),
                    version=spec_val["item"],
                    platform=Platform.NPM,
                    found_by=method,
                    depends_on=dependencies,
                )
            else:
                yield Dependency(
                    name=product["item"],
                    locations=DependencyLocation(path=path, line=str(product["line"])),
                    version=spec_val["item"],
                    platform=Platform.NPM,
                    found_by=method,
                    depends_on=dependencies,
                )


def resolve_dependencies(
    obj: frozendict,
    path: str,
    *,
    direct_deps: bool = True,
    is_dev: bool = False,
) -> Iterator[Dependency]:
    for key in obj:
        if key.get("item") == "dependencies":
            for product, spec in obj[key].items():
                if not should_include(direct_deps, spec, dev_dep=is_dev):
                    continue

                yield from process_dependency(product, spec, name=None, path=path, is_dev=is_dev)
                # From this point on, we check the deps of my deps
                yield from resolve_dependencies(spec, path, direct_deps=False, is_dev=is_dev)


def resolve_packages(
    obj: frozendict,
    path: str,
    *,
    direct_deps: bool = True,
    is_dev: bool = False,
) -> Iterator[Dependency]:
    packages = filter(lambda key: key.get("item") == "packages", obj)

    for key in packages:
        for product, spec in obj[key].items():
            if not product["item"].startswith("node_modules/"):
                continue
            name = product["item"].rsplit("node_modules/")
            if len(name) <= 1:
                continue
            name = name[-1]

            if not should_include(direct_deps, spec, dev_dep=is_dev):
                continue

            yield from process_dependency(product, spec, name, path, is_dev=is_dev)


def npm_package_lock_json(content: str, path: str, *, is_dev: bool = False) -> Iterator[Dependency]:
    obj = json_loads_blocking(content, default={})
    dependencies: list[Dependency] = []
    dev = "dev-" if is_dev else ""

    for key in obj:
        if key.get("item") == "packages":
            dependencies.extend(resolve_packages(obj=obj, path=path, is_dev=is_dev))
            break
        if key.get("item") == "dependencies":
            dependencies.extend(resolve_dependencies(obj=obj, path=path, is_dev=is_dev))
            break
    if len(dependencies) == 0:
        log_blocking("info", f"No {dev}dependencies found in the {path} file")
    return iter(dependencies)


def process_yarn_only(content: str, path: str) -> Iterator[Dependency]:
    windower: Iterator[tuple[tuple[int, str], tuple[int, str]]]
    windower = windowed(  # type: ignore[assignment]
        fillvalue="",
        n=2,
        seq=tuple(enumerate(content.splitlines(), start=1)),
        step=1,
    )

    for (product_line, raw_product), (_, raw_version) in windower:
        product, version = raw_product.strip(), raw_version.strip()

        if (
            product.endswith(":")
            and not product.startswith((" ", "__metadata"))
            and version.startswith("version")
        ):
            product = product.rstrip(":").split(",", maxsplit=1)[0].strip('"')
            product = product.rsplit("@", maxsplit=1)[0]

            version = version.split(" ", maxsplit=1)[1]
            version = version.strip('"')

            yield Dependency(
                name=product,
                locations=DependencyLocation(path=path, line=str(product_line)),
                version=version,
                platform=Platform.NPM,
                found_by=MethodsEnum.NPM_YARN_LOCK,
            )


def add_lines_enumeration(
    windower: Iterator[tuple[tuple[int, str], tuple[int, str]]],
    tree: dict[str, str],
) -> dict[str, NpmDepInfo]:
    enumerated_tree: dict[str, NpmDepInfo] = {}
    for (product_line, raw_product), (version_line, raw_version) in windower:
        product, version = raw_product.strip(), raw_version.strip()
        if product.endswith(":") and not product.startswith(" ") and version.startswith("version"):
            product = product.rstrip(":")
            products = [product.strip('"') for product in product.split(", ")]

            version = version.split(" ", maxsplit=1)[1]
            version = version.strip('"')

            if any(tree.get(product) == version for product in products):
                enumerated_tree[products[0]] = NpmDepInfo(version, product_line, version_line)
    return enumerated_tree


def build_subdep_name(dependencies: dict[str, str]) -> list[str]:
    dependencies_list: list[str] = []
    for key, value in dependencies.items():
        dependencies_list.append(key + "@" + value)
    return dependencies_list


def get_subdependencies(package: str, yarn_dict: lockfile) -> tuple:
    subdependencies: list[str] = []
    version: str | None = None
    for pkg_key, pkg_info in yarn_dict.items():
        # There may be keys in the yarn.lock file like this:
        # pkg@v1, pkg@v2, pkg@v3
        pkg_list = pkg_key.split(", ")
        if package in pkg_list:
            version = pkg_info.get("version")
            if "dependencies" in pkg_info:
                subdependencies = build_subdep_name(pkg_info["dependencies"])
    return subdependencies, version


def run_over_subdeps(
    subdeps: list[str],
    tree: dict[str, str],
    yarn_dict: lockfile,
) -> dict[str, str]:
    while subdeps:
        current_subdep = subdeps[0]
        new_subdeps, version = get_subdependencies(current_subdep, yarn_dict)
        if version:
            tree[current_subdep] = version
        subdeps.remove(current_subdep)
        subdeps = [
            subdep
            for subdep in subdeps + new_subdeps
            if subdep not in tree  # Avoid infinite loop with cyclic dependencies
        ]
    return tree


def get_parsed_files(path_yarn: str, path_json: str) -> tuple[Iterator[tuple], dict, dict] | None:
    yarn_content = get_file_content_block(path_yarn)
    lines = tuple(enumerate(yarn_content.splitlines(), start=1))
    windower = windowed(seq=lines, n=2, fillvalue="", step=1)
    yarn_dict = lockfile.Lockfile.from_file(path_yarn).data
    json_content = get_file_content_block(path_json)
    package_json_parser = json.loads(json_content)
    return (windower, yarn_dict, package_json_parser)


def build_dependencies_tree(
    path_yarn: str,
    path_json: str,
    dependencies_type: DependenciesTypeEnum,
) -> dict[str, NpmDepInfo]:
    if not (parsed_files := get_parsed_files(path_yarn, path_json)):
        return {}
    windower, yarn_dict, package_json_parser = parsed_files

    # Dependencies type could be "devDependencies" for dev dependencies
    # or "dependencies" for prod dependencies
    if dependencies_type.value not in package_json_parser:
        return {}

    tree: dict[str, str] = {}
    package_json_dict = package_json_parser[dependencies_type.value]
    for json_pkg_name, json_pkg_version in package_json_dict.items():
        for yarn_pkg_key, yarn_pkg_info in yarn_dict.items():
            # There may be keys in the yarn.lock file like this:
            # pkg@v1, pkg@v2, pkg@v3
            yarn_pkg_list = yarn_pkg_key.split(", ")
            json_pkg = json_pkg_name + "@" + json_pkg_version
            if json_pkg in yarn_pkg_list:
                tree[json_pkg] = yarn_pkg_info["version"]
                if "dependencies" in yarn_pkg_info:
                    subdeps = build_subdep_name(yarn_pkg_info["dependencies"])
                    tree = run_over_subdeps(subdeps, tree, yarn_dict)
    return add_lines_enumeration(windower, tree)


def npm_yarn_lock(content: str, path: str, *, is_dev: bool = False) -> Iterator[Dependency]:
    method = MethodsEnum.NPM_YARN_LOCK_DEV if is_dev else MethodsEnum.NPM_YARN_LOCK
    try:
        yarn_folder = os.path.dirname(path)  # noqa: PTH120
        json_path = "package.json" if yarn_folder == "" else yarn_folder + "/package.json"

        dependencies_tree = build_dependencies_tree(
            path_yarn=path,
            path_json=json_path,
            dependencies_type=DependenciesTypeEnum.DEV if is_dev else DependenciesTypeEnum.PROD,
        )
        if dependencies_tree:
            for key, value in dependencies_tree.items():
                product = key.rsplit("@", 1)[0]
                product_line = value.product_line
                version = value.version
                yield Dependency(
                    name=product,
                    locations=DependencyLocation(path=path, line=str(product_line)),
                    version=version,
                    platform=Platform.NPM,
                    found_by=method,
                )
    except (FileNotFoundError, ValueError):
        if is_dev:
            msg = f"Unable to find development dependencies for {path}"
            log_blocking("info", msg)
        else:
            with suppress(ValueError):
                yield from process_yarn_only(content, path)


def process_package_string(package: str, spec: dict) -> tuple[str, str] | None:
    if package.startswith("github"):
        pkg_name = spec.get("name", "")
        pkg_version = spec.get("version", "")
    else:
        pkg_info = VERSION_PATTERN.split(package.strip("\"'"))
        if len(pkg_info) < 2:
            return None

        pkg_name = pkg_info[0].lstrip("/")[0:-1]
        pkg_version = pkg_info[1]

    return pkg_name, pkg_version


def extract_package_info(
    package: str,
    spec: dict,
    path: str,
    *,
    is_dev: bool = False,
) -> Dependency | None:
    is_package_dev = spec.get("dev", False)
    if (is_dev and is_package_dev) or (not is_dev and not is_package_dev):
        pkg_info = process_package_string(package, spec)
        if pkg_info:
            line = spec["__line__"] - 1
            return Dependency(
                name=pkg_info[0],
                locations=DependencyLocation(path=path, line=str(line)),
                version=pkg_info[1],
                platform=Platform.NPM,
                found_by=MethodsEnum.PNPM_LOCK_DEV if is_dev else MethodsEnum.PNPM_PACKAGE_LOCK,
            )
    return None


def pnpm_package_lock(content: str, path: str, *, is_dev: bool = False) -> Iterator[Dependency]:
    parsed_content: dict = load_as_yaml(content, loader_cls=Loader)

    if isinstance(parsed_content, dict) and (packages := parsed_content.get("packages", {})):
        for package, spec in packages.items():
            if isinstance(spec, dict) and (
                dependency := extract_package_info(package, spec, path, is_dev=is_dev)
            ):
                yield dependency
