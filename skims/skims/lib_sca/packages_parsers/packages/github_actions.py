from collections.abc import (
    Iterator,
)

from lib_sca.methods_enum import (
    MethodsEnumSCA as MethodsEnum,
)
from lib_sca.model import (
    Dependency,
    DependencyLocation,
    Platform,
)
from lib_sca.packages_parsers.cfn_custom_parser import (
    Loader,
    load_as_yaml,
)


def github_actions_deps(content: str, path: str) -> Iterator[Dependency]:
    parsed_content: dict = load_as_yaml(content, loader_cls=Loader)
    if jobs := parsed_content.get("jobs", {}):
        deps = [
            (step.get("uses"), step["__line__"])
            for job in jobs.values()
            if isinstance(job, dict)
            for step in job.get("steps", [])
            if step.get("uses")
        ]
        for dep, line in deps:
            dep_info = dep.rsplit("@", 1)
            if len(dep_info) == 2:
                yield Dependency(
                    name=dep_info[0],
                    locations=DependencyLocation(path=path, line=str(line)),
                    version=dep_info[1],
                    platform=Platform.GITHUBACTIONS,
                    found_by=MethodsEnum.GITHUB_ACTIONS_DEPS,
                )
