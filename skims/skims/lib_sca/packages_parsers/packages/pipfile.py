from collections.abc import (
    Iterator,
)

import tomlkit
from lib_sca.methods_enum import (
    MethodsEnumSCA as MethodsEnum,
)
from lib_sca.model import (
    Dependency,
    DependencyLocation,
    Platform,
)
from lib_sca.packages_parsers.json_custom_parser import (
    loads_blocking as json_loads_blocking,
)
from tomlkit.exceptions import ParseError


def pipfile_lock(content: str, path: str) -> Iterator[Dependency]:
    content_json = json_loads_blocking(content, default={})
    version: str = ""
    product: str = ""
    dep_line: str = ""

    for key in content_json:
        if key["item"] == "default":
            for dep in content_json[key]:
                product = dep["item"]
                dep_line = dep["line"]
                for dep_info in content_json[key][dep]:
                    if dep_info["item"] == "version":
                        version = content_json[key][dep][dep_info]["item"]
                        yield Dependency(
                            name=product,
                            locations=DependencyLocation(path=path, line=str(dep_line)),
                            version=version.strip("=<>~^ "),
                            platform=Platform.PIP,
                            found_by=MethodsEnum.PIPFILE_LOCK,
                        )


def find_dependency_line_numbers(_content: str, dependency_name: str) -> list[str]:
    return [
        str(line_number)
        for line_number, line in enumerate(_content.splitlines(), start=1)
        if dependency_name in line
    ]


def pipfile_deps(content: str, path: str) -> Iterator[Dependency]:
    try:
        toml_content = tomlkit.parse(content)
    except ParseError:
        return

    packages = toml_content.get("packages", {})
    for package in packages:
        version = packages[package]
        if not isinstance(version, str):
            version = version.get("version", "*")
        if "*" in version:
            continue
        line = find_dependency_line_numbers(content, package)
        yield Dependency(
            name=package,
            locations=DependencyLocation(path=path, line=line[0]),
            version=version.strip("=<>~^ "),
            platform=Platform.PIP,
            found_by=MethodsEnum.PIPFILE_DEPS,
        )
