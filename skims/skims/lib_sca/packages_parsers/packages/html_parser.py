import re
from collections.abc import (
    Iterator,
)

from bs4 import (
    BeautifulSoup,
)
from lib_sca.methods_enum import (
    MethodsEnumSCA as MethodsEnum,
)
from lib_sca.model import (
    Dependency,
    DependencyLocation,
    Platform,
)

SCRIPT_DEP: re.Pattern[str] = re.compile(
    r"(?P<dep>[^\s\/]*)(?P<separator>[-@\/])"
    r"(?P<version>(0|[1-9]\d*)\.(0|[1-9]\d*)\.(0|[1-9]\d*))",
)


def html_script_deps(content: str, path: str) -> Iterator[Dependency]:
    try:
        html_obj = BeautifulSoup(content, features="html.parser")
    except AssertionError:
        return

    for obj in html_obj("script"):
        if (
            (src_cdn := obj.attrs.get("src", None))
            and src_cdn.endswith(".js")
            and (matched := SCRIPT_DEP.search(src_cdn))
        ):
            pkg_name = matched.group("dep")
            pkg_version = matched.group("version")
            yield Dependency(
                name=pkg_name,
                locations=DependencyLocation(
                    path=path,
                    line=str(
                        obj.sourceline,
                    ),
                ),
                version=pkg_version,
                platform=Platform.NPM,
                found_by=MethodsEnum.SCRIPT_VULNERABLE_DEPENDENCIES,
            )
