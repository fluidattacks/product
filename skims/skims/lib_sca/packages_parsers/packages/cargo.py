from collections.abc import (
    Iterator,
)

import tomlkit
from lib_sca.methods_enum import (
    MethodsEnumSCA as MethodsEnum,
)
from lib_sca.model import (
    Dependency,
    DependencyLocation,
    Platform,
)

CARGO_VERSION = "{version="


def parse_dep_line(line: str) -> tuple[str, str]:
    pkg_name, pkg_version = line.replace(" ", "").split("=", 1)

    if pkg_version.startswith(CARGO_VERSION):
        pkg_version = pkg_version.strip(CARGO_VERSION).split(",")[0]

    return pkg_name, pkg_version


def _get_parsed_deps(content: str, searched_type: str) -> dict[str, str]:
    toml_content = tomlkit.parse(content)
    deps = toml_content.get(searched_type, {})
    parsed_deps = {}
    for dep_name, dep_info in deps.items():
        version = None
        if isinstance(dep_info, str):
            version = dep_info
        elif isinstance(dep_info, dict):
            version = dep_info.get("version", None)

        if version:
            parsed_deps[str(dep_name)] = str(version)

    return parsed_deps


def cargo_toml_deps(content: str, path: str, *, is_dev: bool = False) -> Iterator[Dependency]:
    look_up = (
        ("dev-dependencies", MethodsEnum.CARGO_TOML_DEPS_DEV)
        if is_dev
        else ("dependencies", MethodsEnum.CARGO_TOML_DEPS)
    )

    parsed_deps = _get_parsed_deps(content, look_up[0])

    reached_searched_deps = False
    for line_number, line in enumerate(content.splitlines(), start=1):
        if line.startswith(f"[{look_up[0]}]"):
            reached_searched_deps = True
        elif reached_searched_deps:
            if not line or line.lstrip().startswith("["):
                break
            if line.lstrip().startswith("#"):
                continue

            dep_name = line.split("=")[0].strip()

            if version := parsed_deps.get(dep_name):
                yield Dependency(
                    name=dep_name,
                    version=version,
                    locations=DependencyLocation(
                        path=path,
                        line=str(line_number),
                        lines=[str(line_number)],
                    ),
                    platform=Platform.CARGO,
                    found_by=look_up[1],
                )


def cargo_lock_deps(content: str, path: str) -> Iterator[Dependency]:
    is_package = False
    pkg_name = None
    pkg_version = None
    for line_number, line in enumerate(content.splitlines(), 1):
        if line.startswith("[[package]]"):
            is_package = True
        elif is_package:
            if line.startswith("name"):
                pkg_name = line.replace('"', "").split("=")[-1]
            elif line.startswith("version"):
                pkg_version = line.replace('"', "").split("=")[-1]

            if pkg_version and pkg_name:
                format_dep = Dependency(
                    name=pkg_name.strip(),
                    version=pkg_version.strip(),
                    locations=DependencyLocation(
                        path=path,
                        line=str(line_number),
                        lines=[str(line_number)],
                    ),
                    platform=Platform.CARGO,
                    found_by=MethodsEnum.CARGO_LOCK_DEPS,
                )
                is_package = False
                pkg_name = None
                pkg_version = None
                yield format_dep
