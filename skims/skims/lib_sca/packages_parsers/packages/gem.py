import re
from collections.abc import (
    Iterator,
)

from gemfileparser import (
    GemfileParser,
)
from lib_sca.methods_enum import (
    MethodsEnumSCA as MethodsEnum,
)
from lib_sca.model import (
    Dependency,
    DependencyLocation,
    Platform,
)
from lib_sca.packages_parsers.gemfile_custom_parser import (
    parse_line,
)

GEMFILE_DEP: re.Pattern[str] = re.compile(
    r'^\s*(?P<gem>gem ".*?",?( "[><~=]{0,2}\s?[\d\.]+",?){0,2})',
)
NOT_PROD_DEP: re.Pattern[str] = re.compile(r":group => \[?[:\w\-, ]*(:development|:test)")
NOT_PROD_GROUP: re.Pattern[str] = re.compile(r"(\s*)group :(test|development)")
GEM_LOCK_DEP: re.Pattern[str] = re.compile(r"^\s{4}(?P<gem>[^\s]*)\s\([^\d]*(?P<version>.*)\)$")


def create_dependency(
    line_number: int,
    path: str,
    method: MethodsEnum,
    matched: re.Match[str],
) -> Dependency:
    gem_info = GemfileParser.preprocess(matched.group("gem"))[3:]
    product, version = parse_line(gem_info, gem_file=True)
    return Dependency(
        name=product,
        locations=DependencyLocation(path=path, line=str(line_number)),
        version=version,
        platform=Platform.GEM,
        found_by=method,
    )


def _is_dependency_valid(line: str, *, is_dev: bool) -> bool:
    return bool(
        (is_dev and NOT_PROD_DEP.search(line)) or (not is_dev and not NOT_PROD_DEP.search(line)),
    )


def gem_gemfile(
    content: str,
    path: str,
    *,
    is_dev: bool = False,
) -> Iterator[Dependency]:
    method = MethodsEnum.GEM_GEMFILE_DEV if is_dev else MethodsEnum.GEM_GEMFILE
    in_group_block = False
    end_line = ""

    for line_number, line in enumerate(content.splitlines(), 1):
        if in_group_block:
            if line == end_line:
                in_group_block = False
            elif (matched := GEMFILE_DEP.search(line)) and _is_dependency_valid(
                line,
                is_dev=is_dev,
            ):
                yield create_dependency(line_number, path, method, matched)
            continue

        if match_group := NOT_PROD_GROUP.search(line):
            in_group_block = True
            end_line = f"{match_group.group(1)}end"
            continue

        if (matched := GEMFILE_DEP.search(line)) and _is_dependency_valid(line, is_dev=is_dev):
            yield create_dependency(line_number, path, method, matched)


def gem_gemfile_lock(content: str, path: str) -> Iterator[Dependency]:
    line_gem: bool = False
    for line_number, line in enumerate(content.splitlines(), 1):
        if line.startswith("GEM"):
            line_gem = True
        elif line_gem:
            if matched := GEM_LOCK_DEP.match(line):
                pkg_name = matched.group("gem")
                pkg_version = matched.group("version")
                yield Dependency(
                    name=pkg_name,
                    locations=DependencyLocation(path=path, line=str(line_number)),
                    version=pkg_version,
                    platform=Platform.GEM,
                    found_by=MethodsEnum.GEM_GEMFILE_LOCK,
                )

            elif not line:
                break
