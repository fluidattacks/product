import ast
from typing import (
    cast,
)

import lark
from frozendict import (
    frozendict,
)

# Constants
GRAMMAR = r"""
    ?start: value

    ?value: object
            | array
            | string
            | SIGNED_NUMBER      -> number
            | single

    array  : "[" [value ("," value)*] "]"
    object : "{" [pair ("," pair)*] "}"
    pair   : string ":" value

    false : "false"
    true : "true"
    null : "null"
    single : false | true | null
    string : ESCAPED_STRING

    %import common.ESCAPED_STRING
    %import common.SIGNED_NUMBER
    %import common.WS

    %ignore WS
"""


class JSONBuilder(lark.Transformer):
    pair = tuple
    object = frozendict
    single_map = {  # noqa: RUF012
        "false": False,
        "null": None,
        "true": True,
    }

    @staticmethod
    @lark.v_args(tree=True)
    def single(tree: lark.Tree) -> frozendict:
        children: lark.Tree | lark.Token = tree.children[0]
        if isinstance(children, lark.Tree):
            column = children.meta.column
            line = children.meta.line
            item = JSONBuilder.single_map[children.data]
        else:
            column = children.column or 0
            line = children.line or 0
            item = JSONBuilder.single_map[children.type]

        return frozendict(
            {
                "column": column,
                "item": item,
                "line": line,
            },
        )

    @staticmethod
    @lark.v_args(inline=True)
    def string(token: lark.Token) -> frozendict:
        return frozendict(
            {
                "column": token.column,
                "item": ast.literal_eval(token),
                "line": token.line,
            },
        )

    @staticmethod
    @lark.v_args(inline=True)
    # Exception: WF(Cannot factorize function)
    def number(token: lark.Token) -> frozendict:
        return frozendict(
            {
                "column": token.column,
                "item": ast.literal_eval(token),
                "line": token.line,
            },
        )

    @staticmethod
    @lark.v_args(tree=True)
    def array(tree: lark.Tree) -> frozendict:
        return frozendict(
            {
                "column": 0,
                "item": tuple(tree.children),
                "line": 0,
            },
        )


def loads_blocking(
    stream: str,
    *,
    default: dict | None = None,
) -> frozendict:
    json_parser = lark.Lark(
        grammar=GRAMMAR,
        parser="lalr",
        lexer="basic",
        propagate_positions=True,
        maybe_placeholders=False,
        transformer=JSONBuilder(),
    )

    try:
        return cast(frozendict, json_parser.parse(stream))
    except lark.exceptions.LarkError:
        if default is None:
            raise
        return frozendict(default)
