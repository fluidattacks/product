from collections.abc import (
    Iterator,
)
from pathlib import Path

from lib_sca.common import (
    locations_iterator_to_vulns,
)
from lib_sca.methods_enum import (
    MethodsEnumSCA as MethodsEnum,
)


@locations_iterator_to_vulns(
    MethodsEnum.PIPFILE_MISSING_PACKAGE_LOCK,
)
def pipfile_sca_attack(content: str, path: str) -> Iterator[tuple[int, int]]:
    if "[packages]" in content:
        pkg_path = Path(path).parent
        lock_path = pkg_path / "Pipfile.lock"
        if not lock_path.is_file():
            yield 0, 0
