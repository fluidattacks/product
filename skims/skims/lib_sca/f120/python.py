import os
import subprocess
import tempfile
import venv
from contextlib import (
    suppress,
)
from pathlib import (
    Path,
)

import requirements
from lib_sca.common import (
    NON_REQUIREMENT_CHARS,
    get_vulnerabilities_for_incomplete_deps,
)
from lib_sca.methods_enum import (
    MethodsEnumSCA as MethodsEnum,
)
from model.core import (
    Vulnerabilities,
)
from utils.fs import (
    get_file_content_block,
)
from utils.logs import (
    log_blocking,
)


def get_dependencies(file_content: str) -> list[str] | None:
    with suppress(Exception):
        return [str(dep.name) for dep in requirements.parse(file_content)]
    return None


def get_dependencies_from_venv(content: str) -> list[str]:
    current_dir = os.getcwd()  # noqa: PTH109
    try:
        reqs = [
            line
            for line in content.splitlines()
            if line and not line.startswith(NON_REQUIREMENT_CHARS)
        ]
        dependencies_names = []
        with tempfile.TemporaryDirectory() as tmp_dir:
            path = os.path.join(tmp_dir)  # noqa: PTH118
            os.chdir(path)
            venv.create("venv", with_pip=True)

            for item in reqs:
                subprocess.call(  # noqa: S603
                    ["venv/bin/pip", "install", f"{item}"],  # noqa: S607
                    shell=False,
                    stdout=subprocess.DEVNULL,
                    stderr=subprocess.DEVNULL,
                    env={"PYTHONPATH": ""},
                )

            with Path("requirements_2.txt").open("w", encoding="utf-8") as outfile:
                subprocess.call(  # noqa: S603
                    ["venv/bin/pip", "freeze", "--local"],  # noqa: S607
                    stdout=outfile,
                    shell=False,
                    env={"PYTHONPATH": ""},
                )

            req_path = os.getcwd() + "/requirements_2.txt"  # noqa: PTH109
            get_requirements = get_file_content_block(req_path)
            dependencies_names = [str(dep.name) for dep in requirements.parse(get_requirements)]
    except FileNotFoundError:
        log_blocking("error", "File not found for dependencies analysis")
        return []
    else:
        return dependencies_names
    finally:
        os.chdir(current_dir)


def pip_incomplete_dependencies_list(content: str, path: str) -> Vulnerabilities:
    method = MethodsEnum.PIP_INCOMPLETE_DEPENDENCIES_LIST
    declared_deps = get_dependencies(content)
    if not declared_deps:
        return ()

    required_deps = get_dependencies_from_venv(content)
    # Standardize equivalent dependencies to different hyphenated names.
    # e.g. "typing-extensions" vs "typing_extensions"
    dep_standard_names = [dep.replace("-", "_").lower() for dep in required_deps]
    client_standard_names = [dep.replace("-", "_").lower() for dep in declared_deps if dep]

    missing_deps = []
    for name, std_name in zip(required_deps, dep_standard_names, strict=False):
        if std_name not in client_standard_names:
            missing_deps.append(name)

    return get_vulnerabilities_for_incomplete_deps(
        content=content,
        iterator=iter(missing_deps),
        path=path,
        method=method,
    )
