from lib_sca.common import (
    SHIELD_BLOCKING,
)
from lib_sca.f120.python import (
    pip_incomplete_dependencies_list,
)
from model.core import (
    Vulnerabilities,
)


@SHIELD_BLOCKING
def run_pip_incomplete_dependencies_list(content: str, path: str) -> Vulnerabilities:
    return pip_incomplete_dependencies_list(content=content, path=path)


@SHIELD_BLOCKING
def analyze(
    *,
    file_content: str,
    file_name: str,
    file_extension: str,
    path: str,
) -> tuple[Vulnerabilities, ...]:
    results: tuple[Vulnerabilities, ...] = ()

    if (file_name, file_extension) == ("requirements", "txt"):
        results = (
            *results,
            run_pip_incomplete_dependencies_list(file_content, path),
        )

    return results
