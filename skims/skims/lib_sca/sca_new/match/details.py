import hashlib
import json
from dataclasses import (
    dataclass,
)
from typing import (
    Any,
)

from lib_sca.sca_new.match.matcher_type import (
    MatcherType,
)
from lib_sca.sca_new.match.type import (
    MatchType,
)


@dataclass
class Detail:
    type: MatchType
    searched_by: Any
    found: Any
    matcher: MatcherType
    confidence: float | None = None

    def id_(self) -> str:
        try:
            # Convert the object to a dictionary
            obj_dict = {
                "type": self.type,
                "searched_by": self.searched_by,
                "found": self.found,
                "matcher": self.matcher,
                "confidence": self.confidence,
            }

            # Sort the dictionary to ensure consistent ordering
            sorted_dict = json.dumps(obj_dict, sort_keys=True)

            # Create a hash of the sorted dictionary
            hash_object = hashlib.sha256(sorted_dict.encode())
            hash_hex = hash_object.hexdigest()
        except ValueError:
            return ""
        else:
            return hash_hex


class Details(list[Detail]):
    pass
