from lib_sca.sca_new.match import (
    fingerprint,
)
from lib_sca.sca_new.match.details import (
    Detail,
)
from lib_sca.sca_new.match.type import (
    MatchType,
)

__all__ = [
    "MatchType",
]
from dataclasses import (
    dataclass,
)

from lib_sca.sca_new import (
    vulnerability,
)
from lib_sca.sca_new.pkg.package import (
    Package,
)


@dataclass
class Match:
    vulnerability: vulnerability.ScaVulnerability
    package: Package
    details: list[Detail]

    def __str__(self) -> str:
        return f"Match(pkg={self.package} vuln={self.vulnerability!r})"

    def fingerprint(self) -> fingerprint.Fingerprint:
        return fingerprint.Fingerprint(
            vulnerability_id=self.vulnerability.id_,
            vulnerability_namespace=self.vulnerability.namespace,
            vulnerability_fixes=",".join(self.vulnerability.fix.versions),
            package_id=self.package.id,
        )

    def merge(self, other: "Match") -> None:
        if self.fingerprint() != other.fingerprint():
            exc_log = "Cannot merge matches with different fingerprints"
            raise ValueError(exc_log)

        related: set[str] = {reference_id(r) for r in self.vulnerability.related_vulnerabilities}

        for rel in other.vulnerability.related_vulnerabilities:
            if reference_id(rel) not in related:
                self.vulnerability.related_vulnerabilities.append(rel)

        self.vulnerability.related_vulnerabilities.sort(key=reference_id)

        detail_ids: set[str] = {d.id_() for d in self.details}

        for detail in other.details:
            if detail.id_() not in detail_ids:
                self.details.append(detail)

        self.details.sort(key=lambda x: x.id_())

        self.vulnerability.cpes = list(set(self.vulnerability.cpes + other.vulnerability.cpes))
        if not self.vulnerability.cpes:
            self.vulnerability.cpes = []

    @staticmethod
    def merge_cpes(cpes1: list[str], cpes2: list[str]) -> list[str]:
        return list(set(cpes1 + cpes2))


def reference_id(refrence: vulnerability.Reference) -> str:
    return f"{refrence.namespace}:{refrence.id}"
