from enum import (
    Enum,
)


class MatcherType(Enum):
    UNKNOWN_MATCHER_TYPE = "UnknownMatcherType"
    STOCK_MATCHER = "stock-matcher"
    APK_MATCHER = "apk-matcher"
    RUBY_GEM_MATCHER = "ruby-gem-matcher"
    DPKG_MATCHER = "dpkg-matcher"
    RPM_MATCHER = "rpm-matcher"
    JAVA_MATCHER = "java-matcher"
    PYTHON_MATCHER = "python-matcher"
    DOTNET_MATCHER = "dotnet-matcher"
    JAVASCRIPT_MATCHER = "javascript-matcher"
    MSRC_MATCHER = "msrc-matcher"
    PORTAGE_MATCHER = "portage-matcher"
    GO_MODULE_MATCHER = "go-module-matcher"
    OPEN_VEX_MATCHER = "openvex-matcher"
    RUST_MATCHER = "rust-matcher"
