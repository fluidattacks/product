import re
from dataclasses import (
    dataclass,
)

from lib_sca.sca_new.version.version_operator import (
    Operator,
    parse_operator,
)

constraint_part_pattern = re.compile(r"\s*(?P<operator>[><=]+)\s*(?P<version>.+)")


@dataclass
class ConstraintUnit:
    range_operator: Operator
    version: str

    def satisfied(self, comparison: int) -> bool:
        if self.range_operator == Operator.EQ:
            return not comparison
        if self.range_operator == Operator.GT:
            return comparison > 0
        if self.range_operator == Operator.GTE:
            return comparison >= 0
        if self.range_operator == Operator.LT:
            return comparison < 0
        if self.range_operator == Operator.LTE:
            return comparison <= 0

        exc_log = f"unknown operator: {self.range_operator}"
        raise ValueError(exc_log)


def trim_quotes(item: str) -> str:
    if item.startswith('"') and item.endswith('"'):
        return item[1:-1]
    if item.startswith("'") and item.endswith("'"):
        return item[1:-1]

    return item


def parse_unit(phrase: str) -> ConstraintUnit | None:
    match = constraint_part_pattern.match(phrase)
    if not match:
        return None

    version = match.group("version").strip()

    version = trim_quotes(version)

    try:
        operator = parse_operator(match.group("operator"))
    except ValueError as exc:  # pragma: no cover
        exc_log = f"unable to parse constraint operator={match.group('operator')}: {exc}"
        raise ValueError(exc_log) from exc  # pragma: no cover

    return ConstraintUnit(range_operator=operator, version=version)
