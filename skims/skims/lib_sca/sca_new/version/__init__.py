import abc
from dataclasses import (
    dataclass,
)
from typing import (
    Any,
)

from cpe import (
    CPE,
)
from lib_sca.sca_new.pkg.package import (
    Package,
)
from lib_sca.sca_new.version.deb_version.version import (
    DebVersion,
    new_deb_version,
)
from lib_sca.sca_new.version.format import (
    VersionFormat,
    version_format_from_pkg_type,
)
from lib_sca.sca_new.version.semantic_version import (
    SemanticVersion,
)


@dataclass
class MultipleVersionType:
    cpe_vers: list[CPE] | None = None
    deb_ver: DebVersion | None = None
    semver_obj: SemanticVersion | None = None


@dataclass
class Version:
    raw: str
    format: VersionFormat
    version_comparator: MultipleVersionType | None = None

    def populate(self) -> None:
        if not self.version_comparator:
            self.version_comparator = MultipleVersionType()

        if self.format == VersionFormat.DEB:
            self.version_comparator.deb_ver = new_deb_version(self.raw)


class IConstraint(metaclass=abc.ABCMeta):
    @classmethod
    def __subclasshook__(cls, subclass: object) -> bool:
        return (  # pragma: no cover
            (
                hasattr(subclass, "satisfied")
                and callable(subclass.satisfied)
                and hasattr(subclass, "string")
                and callable(subclass.string)
            )
            or NotImplemented
        )

    @abc.abstractmethod
    def satisfied(self, version: Any) -> bool:  # noqa: ANN401
        raise NotImplementedError  # pragma: no cover

    @abc.abstractmethod
    def string(self) -> str:
        raise NotImplementedError  # pragma: no cover


def new_version(raw: str, ver_format: VersionFormat) -> Version:
    version = Version(raw=raw, format=ver_format, version_comparator=MultipleVersionType())
    version.populate()
    return version


def new_version_from_package(package: Package) -> Version | None:
    version = new_version(package.version, version_format_from_pkg_type(package.type))

    if version.version_comparator and version.version_comparator.cpe_vers:  # pragma: no cover
        version.version_comparator.cpe_vers = package.cpes

    return version
