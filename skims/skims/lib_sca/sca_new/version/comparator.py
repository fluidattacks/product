import abc
from collections.abc import Callable
from typing import (
    Any,
)

from lib_sca.sca_new.version.constraint_unit import (
    ConstraintUnit,
)


class IComparator(
    metaclass=abc.ABCMeta,
):
    @abc.abstractmethod
    def compare(self, other: Any) -> int:  # noqa: ANN401
        raise NotImplementedError  # pragma: no cover


ComparatorGenerator = Callable[[ConstraintUnit], IComparator]
