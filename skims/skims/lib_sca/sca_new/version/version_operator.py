from enum import (
    Enum,
)


class Operator(Enum):
    EQ = "="
    GT = ">"
    LT = "<"
    GTE = ">="
    LTE = "<="
    OR = "||"
    AND = ","


def parse_operator(operator: str) -> Operator:
    operators = {
        Operator.EQ.value: Operator.EQ,
        "": Operator.EQ,
        Operator.GT.value: Operator.GT,
        Operator.GTE.value: Operator.GTE,
        Operator.LT.value: Operator.LT,
        Operator.LTE.value: Operator.LTE,
        Operator.OR.value: Operator.OR,
        Operator.AND.value: Operator.AND,
    }

    if operator in operators:
        return operators[operator]

    exc_log = f"unknown operator: '{operator}'"
    raise ValueError(exc_log)
