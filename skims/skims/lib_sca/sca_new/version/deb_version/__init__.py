from __future__ import (
    annotations,
)

from dataclasses import (
    dataclass,
)

from lib_sca.sca_new.version.comparator import (
    IComparator,
)
from lib_sca.sca_new.version.deb_version.version import (
    DebVersion,
    new_deb_version,
)


@dataclass
class DebVersionComparator(IComparator):
    obj: DebVersion

    def compare(self, other: DebVersion) -> int:
        return other.compare(self.obj)


def new_deb_version_comparator(
    version_value: str,
) -> DebVersionComparator | None:
    try:
        ver = new_deb_version(version_value)
    except ValueError:
        return None

    return DebVersionComparator(obj=ver)
