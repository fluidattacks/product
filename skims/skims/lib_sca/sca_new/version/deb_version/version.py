from __future__ import (
    annotations,
)

import re

digit_regexp = re.compile(r"\d+")
non_digit_regexp = re.compile(r"\D+")


class DefaultNumList(list):
    def get(self, idx: int) -> int:
        return self[idx] if idx < len(self) else 0


class DefaultStringList(list):
    def get(self, idx: int) -> str:
        return self[idx] if idx < len(self) else ""


def valid(ver: str) -> bool:
    try:
        new_deb_version(ver)
    except ValueError:
        return False
    else:
        return True


class DebVersion:
    def __init__(
        self,
        epoch: int | None = None,
        upstream_version: str | None = None,
        debian_revision: str | None = None,
    ) -> None:
        self.epoch = epoch or 0
        self.upstream_version = upstream_version or ""
        self.debian_revision = debian_revision or ""

    def equal(self, other: DebVersion) -> bool:
        return self.compare(other) == 0

    def greater_than(self, other: DebVersion) -> bool:
        return self.compare(other) > 0

    def less_than(self, other: DebVersion) -> bool:
        return self.compare(other) < 0

    def compare(self, other: DebVersion) -> int:
        # Equal
        if self == other:
            return 0

        # Compare epochs
        if self.epoch > other.epoch:
            return 1
        if self.epoch < other.epoch:
            return -1

        # Compare versions
        result = compare(self.upstream_version, other.upstream_version)
        if result != 0:
            return result

        # Compare debian_revision
        return compare(self.debian_revision, other.debian_revision)

    def __eq__(self, other: object) -> bool:
        if not isinstance(other, DebVersion):
            return False

        return (
            self.epoch == other.epoch
            and self.upstream_version == other.upstream_version
            and self.debian_revision == other.debian_revision
        )


def verify_upstream_version(version_value: str) -> None:
    if len(version_value) == 0:
        exc_log = "upstream_version is empty"
        raise ValueError(exc_log)

    # The upstream-version should start with a digit
    if not version_value[0].isdigit():
        exc_log = "upstream_version must start with digit"
        raise ValueError(exc_log)

    # The upstream-version may contain only alphanumerics("A-Za-z0-9") and
    # the characters .+-:~
    allowed_symbols = ".-+~:_"
    for char in version_value:
        if not char.isdigit() and not char.isalpha() and char not in allowed_symbols:
            exc_log = "upstream_version includes invalid character"
            raise ValueError(exc_log)


def verify_debian_revision(version_value: str) -> None:
    # The debian-revision may contain only alphanumerics and the characters +.~
    allowed_symbols = "+.~_"
    for char in version_value:
        if not char.isdigit() and not char.isalpha() and char not in allowed_symbols:
            exc_log = "debian_revision includes invalid character"
            raise ValueError(exc_log)


def new_deb_version(version_value: str) -> DebVersion:
    # Trim space
    version_value = version_value.strip()

    # Parse epoch
    splitted = version_value.split(":", 1)
    version = DebVersion()
    if len(splitted) == 1:
        version.epoch = 0
        version_value = splitted[0]
    else:
        try:
            version.epoch = int(splitted[0])
        except ValueError as exc:
            exc_log = f"epoch parse error: {exc}"
            raise ValueError(exc_log) from exc

        if version.epoch < 0:
            exc_log = "epoch is negative"
            raise ValueError(exc_log)
        version_value = splitted[1]

    # Parse upstream_version and debian_revision
    index = version_value.rfind("-")
    if index >= 0:
        version.upstream_version = version_value[:index]
        version.debian_revision = version_value[index + 1 :]
    else:
        version.upstream_version = version_value

    # Verify upstream_version is valid
    verify_upstream_version(version.upstream_version)

    # Verify debian_revision is valid
    verify_debian_revision(version.debian_revision)

    return version


def extract(version: str) -> tuple[DefaultNumList, DefaultStringList]:
    numbers = digit_regexp.findall(version)
    default_num = DefaultNumList(int(number) for number in numbers)

    string = non_digit_regexp.findall(version)

    return default_num, DefaultStringList(string)


def compare_string(string1: str, string2: str) -> int:
    if string1 == string2:
        return 0

    index = 0
    while True:
        char1 = order(string1[index]) if index < len(string1) else 0
        char2 = order(string2[index]) if index < len(string2) else 0

        if char1 != char2:
            return char1 - char2

        index += 1


def order(char: str) -> int:
    # all the letters sort earlier than all the non-letters
    if char.isalpha():
        return ord(char)

    # a tilde sorts before anything
    if char == "~":
        return -1

    return ord(char) + 256


def compare(version1: str, version2: str) -> int:
    # Equal
    if version1 == version2:
        return 0

    # Extract digit strings and non-digit strings
    numbers1, strings1 = extract(version1)
    numbers2, strings2 = extract(version2)

    if len(version1) > 0 and version1[0].isdigit():
        strings1.insert(0, "")
    if len(version2) > 0 and version2[0].isdigit():
        strings2.insert(0, "")

    index = 0
    while True:
        # Compare non-digit strings
        diff = compare_string(strings1.get(index), strings2.get(index))
        if diff != 0:
            return diff

        # Compare digit strings
        diff = numbers1.get(index) - numbers2.get(index)
        if diff != 0:
            return diff

        index += 1
