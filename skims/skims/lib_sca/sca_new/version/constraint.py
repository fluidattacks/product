from typing import (
    cast,
)

from lib_sca.sca_new.version import (
    IConstraint,
)
from lib_sca.sca_new.version.deb_constraint import (
    new_deb_constraint,
)
from lib_sca.sca_new.version.format import (
    VersionFormat,
)
from lib_sca.sca_new.version.semantic_constraint import (
    new_semantic_constraint,
)


def get_constraint(constraint: str, vuln_format: VersionFormat) -> IConstraint | None:
    if vuln_format == VersionFormat.SEMANTIC:
        return cast(IConstraint, new_semantic_constraint(constraint))
    if vuln_format == VersionFormat.DEB:
        return cast(IConstraint, new_deb_constraint(constraint))
    return None
