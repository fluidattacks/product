from dataclasses import (
    dataclass,
)

from lib_sca.sca_new.vulnerability.provider import (
    IMetadataProvider,
    IProvider,
)


@dataclass
class Store:
    provider: IProvider
    metadata_provider: IMetadataProvider
