from contextlib import (
    suppress,
)
from dataclasses import (
    dataclass,
)
from typing import (
    cast,
)

from lib_sca.sca_new.distro.type import (
    LinuxDistributionType,
)
from semantic_version.base import (
    Version,
)
from utils.logs import (
    log_blocking,
)


@dataclass
class Distro:
    type: LinuxDistributionType
    version: Version | None
    raw_version: str
    id_like: list[str]

    def is_rolling(self) -> bool:
        return any(
            [
                self.type == LinuxDistributionType.WOLFI,
                self.type == LinuxDistributionType.CHAINGUARD,
                self.type == LinuxDistributionType.ARCHLINUX,
                self.type == LinuxDistributionType.GENTOO,
            ],
        )

    def full_version(self) -> str:
        return self.raw_version


def new_distro_from_release(release_data: dict[str, str]) -> Distro | None:
    selected_version: str | None = None
    try:
        linux_type = LinuxDistributionType.get_instance(release_data["id_"])
    except ValueError:
        return None

    for version in [release_data["version_id"], release_data["version"]]:
        if version:
            with suppress(ValueError):
                parsed_version = Version.coerce(version)
                return Distro(
                    type=linux_type,
                    raw_version=version,
                    id_like=cast(list[str], release_data["id_like"]),
                    version=parsed_version,
                )

    if (
        linux_type == LinuxDistributionType.DEBIAN
        and not selected_version
        and release_data["version_id"] == ""
        and release_data["version"] == ""
        and "sid" in release_data["pretty_name"]
    ):
        return Distro(
            type=linux_type,
            raw_version="unstable",
            id_like=cast(list[str], release_data["id_like"]),
            version=None,
        )

    log_blocking("warning", "Unable to parse distro version")

    return None
