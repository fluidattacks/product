from __future__ import (
    annotations,
)

from enum import (
    Enum,
)


class LinuxDistributionType(Enum):
    DEBIAN = "debian"
    UBUNTU = "ubuntu"
    REDHAT = "redhat"
    CENTOS = "centos"
    FEDORA = "fedora"
    ALPINE = "alpine"
    BUSYBOX = "busybox"
    AMAZONLINUX = "amazonlinux"
    ORACLELINUX = "oraclelinux"
    ARCHLINUX = "archlinux"
    OPENSUSELEAP = "opensuseleap"
    SLES = "sles"
    PHOTON = "photon"
    WINDOWS = "windows"
    MARINER = "mariner"
    ROCKYLINUX = "rockylinux"
    ALMALINUX = "almalinux"
    GENTOO = "gentoo"
    WOLFI = "wolfi"
    CHAINGUARD = "chainguard"

    @classmethod
    def get_instance(cls, key: str) -> LinuxDistributionType:
        return cls(key.strip().lower())
