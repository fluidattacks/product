from dataclasses import (
    dataclass,
)

from lib_sca.sca_new.distro import (
    Distro,
)
from lib_sca.sca_new.match import (
    Match,
)
from lib_sca.sca_new.match.matcher_type import (
    MatcherType,
)
from lib_sca.sca_new.matcher import (
    IMatcher,
)
from lib_sca.sca_new.pkg.package import (
    Package,
    PackageType,
)
from lib_sca.sca_new.search.distro import (
    by_package_distro,
)
from lib_sca.sca_new.vulnerability.provider import (
    IProvider,
)


@dataclass
class Matcher(IMatcher):
    def package_types(self) -> list[PackageType]:
        return [PackageType.DEB_PKG]

    def type(self) -> MatcherType:
        return MatcherType.DPKG_MATCHER

    def match(self, store: IProvider, distro: Distro | None, package: Package) -> list[Match]:
        matches: list[Match] = by_package_distro(store, distro, package, self.type())
        return matches
