from dataclasses import (
    dataclass,
)

from lib_sca.sca_new.distro import (
    Distro,
)
from lib_sca.sca_new.match import (
    Match,
)
from lib_sca.sca_new.match.matcher_type import (
    MatcherType,
)
from lib_sca.sca_new.matcher import (
    IMatcher,
)
from lib_sca.sca_new.pkg.package import (
    Package,
    PackageType,
)
from lib_sca.sca_new.search.criteria import (
    CommonCriteria,
    Criteria,
    by_criteria,
)
from lib_sca.sca_new.vulnerability.provider import (
    IProvider,
)


@dataclass
class MatcherConfig:
    use_cpes: bool


@dataclass
class Matcher(IMatcher):
    cfg: MatcherConfig

    @staticmethod
    def new_stock_matcher(cfg: MatcherConfig) -> "Matcher":
        return Matcher(cfg=cfg)

    def package_types(self) -> list[PackageType]:
        return []

    def type(self) -> MatcherType:
        return MatcherType.STOCK_MATCHER

    def match(self, store: IProvider, distro: Distro | None, package: Package) -> list[Match]:
        criteria = CommonCriteria

        if self.cfg.use_cpes:
            criteria.append(Criteria.BY_CPE)

        return by_criteria(store, distro, package, self.type(), *criteria)


def new_stock_matcher(cfg: MatcherConfig) -> IMatcher:
    return Matcher(cfg=cfg)
