import abc

from lib_sca.sca_new.distro import (
    Distro,
)
from lib_sca.sca_new.match import (
    Match,
)
from lib_sca.sca_new.match.matcher_type import (
    MatcherType,
)
from lib_sca.sca_new.pkg.package import (
    Package,
    PackageType,
)
from lib_sca.sca_new.vulnerability.provider import (
    IProvider,
)


class IMatcher(metaclass=abc.ABCMeta):
    @abc.abstractmethod
    def package_types(self) -> list[PackageType]:  # pragma: no cover
        raise NotImplementedError  # pragma: no cover

    @abc.abstractmethod
    def type(self) -> MatcherType:
        raise NotImplementedError  # pragma: no cover

    @abc.abstractmethod
    def match(self, store: IProvider, distro: Distro | None, package: Package) -> list[Match]:
        raise NotImplementedError  # pragma: no cover
