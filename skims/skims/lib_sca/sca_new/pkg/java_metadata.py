from typing import (
    NamedTuple,
)


class Digest(NamedTuple):
    algorithm: str
    value: str


class JavaMetadata(NamedTuple):
    virtual_path: str
    pom_artifact_id: str
    pom_group_id: str
    manifest_name: str
    archive_digests: list[Digest]
