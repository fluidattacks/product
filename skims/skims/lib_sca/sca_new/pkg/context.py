from attr import (
    dataclass,
)
from lib_sca.sca_new.distro import (
    Distro,
)


@dataclass
class Context:
    distro: Distro
