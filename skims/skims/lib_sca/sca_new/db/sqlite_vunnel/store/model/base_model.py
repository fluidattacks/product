from peewee import (
    Model,
    Proxy,
    SqliteDatabase,
)

# Create a proxy for our database
db_proxy = Proxy()


class BaseModel(Model):
    class Meta:
        database = db_proxy


def init_database(database: SqliteDatabase) -> None:
    db_proxy.initialize(database)
