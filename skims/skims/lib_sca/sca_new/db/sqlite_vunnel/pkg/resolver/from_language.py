from typing import (
    cast,
)

from lib_sca.sca_new.db.sqlite_vunnel.pkg.resolver import (
    IResolver,
)
from lib_sca.sca_new.db.sqlite_vunnel.pkg.resolver.java.resolver import (
    Resolver as JavaResolver,
)
from lib_sca.sca_new.db.sqlite_vunnel.pkg.resolver.python.resolver import (
    Resolver as PythonResolver,
)
from lib_sca.sca_new.db.sqlite_vunnel.pkg.resolver.stock.resolver import (
    Resolver as DefaultResolver,
)
from lib_sca.sca_new.pkg.package import (
    Language,
)


def from_language(language: Language) -> IResolver:
    match language:
        case Language.JAVA:
            return cast(IResolver, JavaResolver())
        case Language.PYTHON:
            return cast(IResolver, PythonResolver())
        case _:
            return cast(IResolver, DefaultResolver())
