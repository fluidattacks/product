from typing import (
    cast,
)

from lib_sca.sca_new.db.sqlite_vunnel.namespace import (
    INamespace,
)
from lib_sca.sca_new.db.sqlite_vunnel.namespace.cpe.namespace import (
    from_str as cpe_namespace_from_str,
)
from lib_sca.sca_new.db.sqlite_vunnel.namespace.distro.namespace import (
    from_str as distro_namespace_from_str,
)
from lib_sca.sca_new.db.sqlite_vunnel.namespace.language.namespace import (
    from_str as language_namespace_from_str,
)


def from_str(namespace: str) -> INamespace:
    if not namespace:
        exc_log = "Empty namespace"
        raise ValueError(exc_log)

    components = namespace.split(":")

    if len(components) < 2:
        exc_log = f"unable to create namespace from {namespace}: incorrect number of components"
        raise ValueError(exc_log)

    match components[1]:
        case "cpe":
            return cast(INamespace, cpe_namespace_from_str(namespace))
        case "distro":
            return cast(INamespace, distro_namespace_from_str(namespace))
        case "language":
            return cast(INamespace, language_namespace_from_str(namespace))

    exc_log = f"unable to create namespace from {namespace}: type {components[1]} is incorrect"
    raise ValueError(exc_log)
