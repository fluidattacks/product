from attr import (
    dataclass,
)
from lib_sca.sca_new.db.sqlite_vunnel.namespace import (
    INamespace,
)
from lib_sca.sca_new.db.sqlite_vunnel.pkg.resolver import (
    IResolver,
)
from lib_sca.sca_new.db.sqlite_vunnel.pkg.resolver.stock.resolver import (
    Resolver,
)

ID = "cpe"


@dataclass
class Namespace(INamespace):
    provider_: str
    resolver_: IResolver

    def provider(self) -> str:
        return self.provider_

    def resolver(self) -> IResolver:
        return self.resolver_

    def string(self) -> str:
        return f"{self.provider_}:{ID}"


def new_namespace(provider: str) -> Namespace:
    return Namespace(
        provider_=provider,
        resolver_=Resolver(),
    )


def from_str(namespace: str) -> Namespace:
    if not namespace:
        exc_log = "Empty namespace"
        raise ValueError(exc_log)

    components = namespace.split(":")
    if len(components) != 2:
        exc_log = f"unable to create CPE namespace from {namespace} incorrect number of components"
        raise ValueError(exc_log)
    if components[1] != ID:
        exc_log = f"unknown provider {components[0]}"
        raise ValueError(exc_log)

    return new_namespace(components[0])
