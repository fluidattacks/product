from dataclasses import (
    dataclass,
)

from lib_sca.sca_new.db.sqlite_vunnel.namespace import (
    INamespace,
)
from lib_sca.sca_new.db.sqlite_vunnel.pkg.resolver import (
    IResolver,
)
from lib_sca.sca_new.db.sqlite_vunnel.pkg.resolver.stock.resolver import (
    Resolver,
)
from lib_sca.sca_new.distro.type import (
    LinuxDistributionType,
)

ID = "distro"


@dataclass
class Namespace(INamespace):
    provider_: str
    distro_type_: LinuxDistributionType
    version_: str
    resolver_: IResolver

    def provider(self) -> str:
        return self.provider_

    def resolver(self) -> IResolver:
        return self.resolver_

    def string(self) -> str:
        return f"{self.provider_}:{ID}:{self.distro_type_.value}:{self.version_}"


def new_namespace(provider: str, distro_type: LinuxDistributionType, version: str) -> Namespace:
    return Namespace(
        provider_=provider,
        distro_type_=distro_type,
        version_=version,
        resolver_=Resolver(),
    )


def from_str(namespace_str: str) -> Namespace:
    if not namespace_str:
        exc_log = "unable to create distro namespace from empty string"
        raise ValueError(exc_log)

    components = namespace_str.split(":")

    if len(components) != 4:
        exc_log = (
            "unable to create distro namespace from"
            f" {namespace_str}: incorrect number of components"
        )
        raise ValueError(exc_log)

    if components[1] != ID:
        exc_log = (
            "unable to create distro namespace from "
            f"{namespace_str}: type {components[1]} is incorrect"
        )
        raise ValueError(exc_log)

    return new_namespace(components[0], LinuxDistributionType(components[2]), components[3])
