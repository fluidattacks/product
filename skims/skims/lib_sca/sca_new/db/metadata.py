import json
import os
from datetime import (
    datetime,
)
from pathlib import (
    Path,
)
from typing import (
    NamedTuple,
)

from lib_sca.sca_new.db.listing import (
    ListingEntry,
)


class Metadata(NamedTuple):
    built: datetime
    version: int
    checksum: str


def metadata_path(dir_name: str) -> str:
    return os.path.join(dir_name, "metadata.json")  # noqa: PTH118


def is_superseded_by(metadata: Metadata, entry: ListingEntry) -> bool:
    if metadata.version > entry.version:
        return True

    return metadata.built > entry.built


def metadata_from_dir(dir_name: str) -> Metadata | None:
    metadata_file_path = metadata_path(dir_name)
    if os.path.exists(metadata_file_path):  # noqa: PTH110
        with Path(metadata_file_path).open(encoding="utf-8") as file:
            data = json.load(file)
            return Metadata(
                built=datetime.fromisoformat(data["built"]),
                version=data["version"],
                checksum=data["checksum"],
            )

    return None
