from lib_sca.sca_new.distro import (
    Distro,
)
from lib_sca.sca_new.pkg.package import (
    Language,
    Package,
)
from lib_sca.sca_new.version import (
    Version,
)
from lib_sca.sca_new.vulnerability import (
    ScaVulnerability,
)
from utils.logs import (
    log_blocking,
)


def only_qualified_packages(
    distro: Distro | None,
    pkg: Package,
    all_vulns: list[ScaVulnerability],
) -> list[ScaVulnerability]:
    vulns = []
    for vuln in all_vulns:
        is_vulnerable = True
        for qualifier in vuln.package_qualifiers:
            satisfied = qualifier.satisfied(distro, pkg)
            is_vulnerable = satisfied
            if not is_vulnerable:
                break
        if not is_vulnerable:
            continue
        vulns.append(vuln)
    return vulns


def only_vulnerable_versions(
    version_object: Version | None,
    all_vulns: list[ScaVulnerability],
) -> list[ScaVulnerability]:
    vulns = []
    for vuln in all_vulns:
        try:
            is_vulnerable = vuln.constraint.satisfied(version_object)
        except ValueError:
            log_blocking(
                "warning",
                "failde to check contraint=%s version=%s",
                vuln.constraint.string(),
                version_object,
            )
            continue

        if not is_vulnerable:
            continue
        vulns.append(vuln)
    return vulns


# Define the package types
OS_PACKAGE_TYPES = {
    "alpm",
    "apk",
    "deb",
    "kb",
    "portage",
    "rpm",
}

# Define known target software
KNOWN_TARGET_SOFTWARE = {
    "wordpress",
    "wordpress_",
    "joomla",
    "joomla!",
    "drupal",
}


def is_os_package(pkg: Package) -> bool:
    return pkg.type.value in list(OS_PACKAGE_TYPES)


def is_unknown_target(target_sw: str) -> bool:
    if target_sw in KNOWN_TARGET_SOFTWARE:
        return False
    # Assuming LanguageByName and UnknownLanguage are defined elsewhere
    return Language(target_sw) == Language.UNKNOWN_LANGUAGE


def only_vulnerable_targets(
    pkg: Package,
    all_vulns: list[ScaVulnerability],
) -> list[ScaVulnerability]:
    if is_os_package(pkg):
        return all_vulns

    if (
        pkg.language == Language.JAVA
    ):  # Assuming 'java' is the string representation of the Java language
        return all_vulns

    vulns = []
    for vuln in all_vulns:
        is_package_vulnerable = len(vuln.cpes) == 0
        for cpe in vuln.cpes:
            target_sw = cpe.get_target_software()
            mismatch_with_unknown_language = target_sw != pkg.language and is_unknown_target(
                target_sw,
            )
            if (
                target_sw in {"ANY", "NA"}
                or target_sw == pkg.language
                or mismatch_with_unknown_language
            ):
                is_package_vulnerable = True

        if is_package_vulnerable:
            vulns.append(vuln)

    return vulns
