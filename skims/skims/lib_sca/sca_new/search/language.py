import logging

from lib_sca.sca_new.distro import (
    Distro,
)
from lib_sca.sca_new.match import (
    Match,
    MatchType,
)
from lib_sca.sca_new.match.details import (
    Detail,
)
from lib_sca.sca_new.match.matcher_type import (
    MatcherType,
)
from lib_sca.sca_new.pkg.package import (
    Package,
)
from lib_sca.sca_new.search import (
    only_qualified_packages,
    only_vulnerable_versions,
)
from lib_sca.sca_new.version import (
    Version,
    new_version_from_package,
)
from lib_sca.sca_new.vulnerability import (
    ScaVulnerability,
)
from lib_sca.sca_new.vulnerability.provider import (
    IProviderByLanguage,
)
from utils.logs import (
    log_blocking,
)

logger = logging.getLogger(__name__)


def by_package_language(
    store: IProviderByLanguage,
    distro: Distro | None,
    package: Package,
    upstream_matcher: MatcherType,
) -> list[Match] | None:
    ver_obj = _get_version_object(package)
    if ver_obj is None:
        return None

    all_pkg_vulns = _fetch_vulnerabilities(store, package)
    if all_pkg_vulns is None:
        return None

    applicable_vulns = _filter_vulnerabilities(distro, package, all_pkg_vulns, ver_obj)
    if applicable_vulns is None:
        return None

    return _create_matches(applicable_vulns, package, upstream_matcher)


def _get_version_object(package: Package) -> Version | None:
    try:
        return new_version_from_package(package)
    except ValueError:
        log_blocking(
            "debug",
            f"skipping package '{package.name}@{package.version}'",
        )
        return None


def _fetch_vulnerabilities(
    store: IProviderByLanguage,
    package: Package,
) -> list[ScaVulnerability] | None:
    try:
        return store.get_by_language(package.language, package)
    except ValueError as err:
        log_blocking(
            "error",
            f"failed to fetch language='{package.language}' pkg='{package.name}': {err}",
        )
        return None


def _filter_vulnerabilities(
    distro: Distro | None,
    package: Package,
    all_pkg_vulns: list[ScaVulnerability],
    ver_obj: Version | None,
) -> list[ScaVulnerability] | None:
    try:
        applicable_vulns = only_qualified_packages(distro, package, all_pkg_vulns)
        applicable_vulns = only_vulnerable_versions(ver_obj, applicable_vulns)
    except ValueError as err:
        log_blocking(
            "error",
            f"unable to filter language-related vulnerabilities: {err}",
            exc_info=True,
        )
        return None
    else:
        return applicable_vulns


def _create_matches(
    applicable_vulns: list[ScaVulnerability],
    package: Package,
    upstream_matcher: MatcherType,
) -> list[Match]:
    matches = []
    for vuln in applicable_vulns:
        match_details = Detail(
            type=MatchType.EXACT_DIRECT_MATCH,
            matcher=upstream_matcher,
            searched_by={
                "language": str(package.language),
                "namespace": vuln.namespace,
                "package": {
                    "name": package.name,
                    "version": package.version,
                },
            },
            found={
                "vulnerabilityID": vuln.id_,
                "versionConstraint": vuln.constraint.string(),
            },
        )
        match = Match(
            vulnerability=vuln,
            package=package,
            details=[match_details],
        )
        matches.append(match)
    return matches
