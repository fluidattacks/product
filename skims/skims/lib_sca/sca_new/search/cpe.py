from dataclasses import (
    dataclass,
    field,
)

from cpe.cpe import (
    CPE,
)
from lib_sca.sca_new.distro import (
    Distro,
)
from lib_sca.sca_new.match import (
    Match,
)
from lib_sca.sca_new.match.details import (
    Detail,
)
from lib_sca.sca_new.match.fingerprint import (
    Fingerprint,
)
from lib_sca.sca_new.match.matcher_type import (
    MatcherType,
)
from lib_sca.sca_new.match.type import (
    MatchType,
)
from lib_sca.sca_new.pkg.package import (
    Package,
    PackageType,
)
from lib_sca.sca_new.search import (
    only_qualified_packages,
    only_vulnerable_targets,
    only_vulnerable_versions,
)
from lib_sca.sca_new.version import (
    Version,
    new_version,
)
from lib_sca.sca_new.version.constraint import (
    get_constraint,
)
from lib_sca.sca_new.version.format import (
    VersionFormat,
    version_format_from_pkg_type,
)
from lib_sca.sca_new.vulnerability import (
    ScaVulnerability,
)
from lib_sca.sca_new.vulnerability.provider import (
    IProviderByCPE,
)
from utils.logs import (
    log_blocking,
)


@dataclass
class CPEPackageParameter:
    name: str
    version: str


@dataclass
class CPEParameters:
    namespace: str
    package: CPEPackageParameter
    cpes: list[str] = field(default_factory=list)

    def merge(self, other: "CPEParameters") -> None:
        if self.namespace != other.namespace:
            exc_log = "namespaces do not match"
            raise ValueError(exc_log)

        existing_cpes = set(self.cpes)
        new_cpes = set(other.cpes)
        merged_cpes = sorted(existing_cpes.union(new_cpes))

        self.cpes = merged_cpes


@dataclass
class CPEResult:
    vulnerability_id: str
    version_constraint: str
    cpes: list[str] = field(default_factory=list)

    def equals(self, other: "CPEResult") -> bool:
        if self.version_constraint != other.version_constraint:
            return False

        if len(self.cpes) != len(other.cpes):
            return False

        return all(cpe == other.cpes[index] for index, cpe in enumerate(self.cpes))


def alpine_cpe_comparable_version(version: str) -> str:
    """Clean the alpine package version.

    So that it compares correctly with the
    CPE version comparison logic. Alpine versions are suffixed with
    -r{buildindex}; however, if left intact CPE comparison logic will
    incorrectly treat these as a pre-release. In actuality, we just want to
    treat 1.2.3-r21 as equivalent to 1.2.3 for purposes of CPE-based matching
    since the alpine fix should filter out any cases where a later
    build fixes something that was vulnerable in 1.2.3.
    """
    components = version.split("-r")
    cpe_comparable_version = version

    if len(components) == 2:
        cpe_comparable_version = components[0]

    return cpe_comparable_version


class CPEError(Exception):
    pass


def by_package_cpe(
    store: IProviderByCPE,
    distro: Distro | None,
    package: Package,
    upstream_matcher: MatcherType,
) -> list[Match]:
    if not package.cpes:
        log_blocking("warning", "attempted CPE match against package with no CPEs")
        return []

    matches_by_fingerprint: dict[Fingerprint, Match] = {}

    for cpe in package.cpes:
        try:
            ver_obj = get_search_version(package, cpe)
            all_pkg_vulns = store.get_by_cpe(cpe)
            applicable_vulns = filter_vulnerabilities(distro, package, all_pkg_vulns, ver_obj)

            for vuln in applicable_vulns:
                add_new_match(
                    matches_by_fingerprint,
                    vuln,
                    package,
                    ver_obj,
                    upstream_matcher,
                    cpe,
                )
        except ValueError as err:
            log_blocking("error", f"Error processing CPE {cpe}: {err}")
            continue

    return to_matches(matches_by_fingerprint)


def get_search_version(package: Package, cpe: CPE) -> Version:
    search_version: str = cpe.get_version()[0]

    if package.type == PackageType.APK_PKG:
        search_version = alpine_cpe_comparable_version(search_version)

    if search_version in ["NA", "Any"]:
        search_version = package.version

    try:
        return new_version(search_version, version_format_from_pkg_type(package.type))
    except ValueError as err:
        exc_log = f"Failed to parse version for package {package.name}: {err}"
        raise ValueError(exc_log) from err


def filter_vulnerabilities(
    distro: Distro | None,
    package: Package,
    all_pkg_vulns: list[ScaVulnerability],
    ver_obj: Version,
) -> list[ScaVulnerability]:
    try:
        applicable_vulns = only_qualified_packages(distro, package, all_pkg_vulns)
        applicable_vulns = only_vulnerable_versions(ver_obj, applicable_vulns)
        return only_vulnerable_targets(package, applicable_vulns)
    except ValueError as err:
        exc_log = f"Failed to filter vulnerabilities: {err}"
        raise ValueError(exc_log) from err


def add_new_match(  # noqa: PLR0913
    matches_by_fingerprint: dict[Fingerprint, Match],
    vuln: ScaVulnerability,
    package: Package,
    search_version: Version | None,
    upstream_matcher: MatcherType,
    searched_by_cpe: CPE,
) -> None:
    candidate_match = Match(
        vulnerability=vuln,
        package=package,
        details=[],
    )

    if candidate_match.fingerprint() in matches_by_fingerprint:
        candidate_match = matches_by_fingerprint[candidate_match.fingerprint()]

    candidate_match.details = add_match_details(
        candidate_match.details,
        Detail(
            type=MatchType.CPE_MATCH,
            confidence=0.9,
            matcher=upstream_matcher,
            searched_by=CPEParameters(
                namespace=vuln.namespace,
                cpes=[searched_by_cpe.cpe_str],
                package=CPEPackageParameter(
                    name=package.name,
                    version=package.version,
                ),
            ),
            found=CPEResult(
                vulnerability_id=vuln.id_,
                version_constraint=vuln.constraint.string(),
                cpes=cpes_to_string(filter_cpes_by_version(search_version, vuln.cpes)),
            ),
        ),
    )

    matches_by_fingerprint[candidate_match.fingerprint()] = candidate_match


def add_match_details(existing_details: list[Detail], new_details: Detail) -> list[Detail]:
    new_found: CPEResult = new_details.found
    new_searched_by: CPEParameters = new_details.searched_by

    for idx, detail in enumerate(existing_details):
        found: CPEResult = detail.found
        searched_by: CPEParameters = detail.searched_by

        if not found.equals(new_found):
            continue

        try:
            searched_by.merge(new_searched_by)
        except ValueError:
            continue

        existing_details[idx].searched_by = searched_by
        return existing_details

    existing_details.append(new_details)
    return existing_details


def filter_cpes_by_version(pkg_version: Version | None, all_cpes: list[CPE]) -> list[CPE]:
    matched_cpes = []

    for cpe in all_cpes:
        if "" in cpe.get_version() or "-" in cpe.get_version():
            matched_cpes.append(cpe)
            continue

        try:
            constraint = get_constraint(cpe.get_version()[0], VersionFormat.UNKNOWN)
        except ValueError:
            matched_cpes.append(cpe)
            continue
        if constraint and constraint.satisfied(pkg_version):
            matched_cpes.append(cpe)

    return matched_cpes


def to_matches(matches_by_fingerprint: dict[Fingerprint, Match]) -> list[Match]:
    return list(matches_by_fingerprint.values())


def cpes_to_string(cpes: list[CPE]) -> list[str]:
    strs = [c.cpe_str for c in cpes]
    strs.sort()
    return strs
