import ast
from contextlib import (
    suppress,
)
from typing import (
    Any,
)

from lib_cspm.aws.model import (
    AwsMethodsTuple,
    Location,
)
from lib_cspm.aws.shield import (
    SHIELD,
)
from lib_cspm.aws.utils import (
    build_vulnerabilities,
    run_boto3_fun,
)
from lib_cspm.methods_enum import (
    MethodsEnumCSPM as MethodsEnum,
)
from model.core import (
    AwsCredentials,
    Vulnerabilities,
)


async def get_paginated_items(
    credentials: AwsCredentials,
) -> list:
    """Get all items in paginated API calls."""
    pools: list[dict] = []
    args: dict[str, Any] = {
        "credentials": credentials,
        "service": "iam",
        "function": "list_policies",
        "parameters": {"MaxItems": 50, "Scope": "Local", "OnlyAttached": True},
    }
    data = await run_boto3_fun(**args)
    object_name = "Policies"
    pools += data.get(object_name, [])

    next_token = data.get("Marker", None)
    while next_token:
        args["parameters"]["Marker"] = next_token
        data = await run_boto3_fun(**args)
        pools += data.get(object_name, [])
        next_token = data.get("Marker", None)

    return pools


def get_vuln_index(stm: dict[str, Any]) -> int:
    return (
        stm["Action"].index("iam:*")
        if "iam:*" in stm["Action"]
        else stm["Action"].index("iam:SetDefaultPolicyVersion")
    )


@SHIELD
async def allows_priv_escalation_by_policies_versions(
    credentials: AwsCredentials,
) -> tuple[Vulnerabilities, bool]:
    policies = await get_paginated_items(credentials)
    method = MethodsEnum.ALLOWS_PRIV_ESCALATION_BY_POLICIES_VERSIONS
    vulns: Vulnerabilities = ()

    for policy in policies:
        locations: list[Location] = []
        pol_ver: dict[str, Any] = await run_boto3_fun(
            credentials=credentials,
            service="iam",
            function="get_policy_version",
            parameters={
                "PolicyArn": str(policy["Arn"]),
                "VersionId": str(policy["DefaultVersionId"]),
            },
        )
        policy_names = pol_ver.get("PolicyVersion", {})
        pol_access = ast.literal_eval(str(policy_names.get("Document", {})))
        policy_statements = ast.literal_eval(str(pol_access.get("Statement", [])))

        if not isinstance(policy_statements, list):
            policy_statements = [policy_statements]

        for index, stm in enumerate(policy_statements):
            with suppress(KeyError):
                vulnerable = (stm["Effect"] == "Allow" and "Resource" in stm) and (
                    (
                        "iam:CreatePolicyVersion" in stm["Action"]
                        and "iam:SetDefaultPolicyVersion" in stm["Action"]
                    )
                    or "iam:*" in stm["Action"]
                )

                if vulnerable:
                    locations.append(
                        Location(
                            access_patterns=(
                                f"/Document/Statement/{index}/Action/{get_vuln_index(stm)}",
                            ),
                            arn=(f"{policy['Arn']}"),
                            values=(stm["Action"][get_vuln_index(stm)],),
                            method=method,
                        ),
                    )

        vulns += build_vulnerabilities(
            locations=locations,
            method=method,
            aws_response=policy_names,
        )

    return (vulns, True)


@SHIELD
async def allows_priv_escalation_by_attach_policy(
    credentials: AwsCredentials,
) -> tuple[Vulnerabilities, bool]:
    policies = await get_paginated_items(credentials)
    method = MethodsEnum.ALLOWS_PRIV_ESCALATION_BY_ATTACH_POLICY
    vulns: Vulnerabilities = ()

    for policy in policies:
        locations: list[Location] = []
        pol_ver: dict[str, Any] = await run_boto3_fun(
            credentials=credentials,
            service="iam",
            function="get_policy_version",
            parameters={
                "PolicyArn": str(policy["Arn"]),
                "VersionId": str(policy["DefaultVersionId"]),
            },
        )
        policy_names = pol_ver.get("PolicyVersion", {})
        pol_access = ast.literal_eval(str(policy_names.get("Document", {})))
        policy_statements = ast.literal_eval(str(pol_access.get("Statement", [])))

        if not isinstance(policy_statements, list):
            policy_statements = [policy_statements]

        for index, stm in enumerate(policy_statements):
            with suppress(KeyError):
                vulnerable = (stm["Effect"] == "Allow" and "Resource" in stm) and (
                    "iam:AttachUserPolicy" in stm["Action"] or "iam:*" in stm["Action"]
                )

                if vulnerable:
                    vuln_index = (
                        stm["Action"].index("iam:*")
                        if "iam:*" in stm["Action"]
                        else stm["Action"].index("iam:AttachUserPolicy")
                    )
                    locations.append(
                        Location(
                            access_patterns=(f"/Document/Statement/{index}/Action/{vuln_index}",),
                            arn=(f"{policy['Arn']}"),
                            values=(stm["Action"][vuln_index],),
                            method=method,
                        ),
                    )

        vulns += build_vulnerabilities(
            locations=locations,
            method=method,
            aws_response=policy_names,
        )

    return (vulns, True)


CHECKS: AwsMethodsTuple = (
    allows_priv_escalation_by_policies_versions,
    allows_priv_escalation_by_attach_policy,
)
