from typing import (
    Any,
)

from lib_cspm.aws.model import (
    AwsMethodsTuple,
    Location,
)
from lib_cspm.aws.shield import (
    SHIELD,
)
from lib_cspm.aws.utils import (
    build_vulnerabilities,
    run_boto3_fun,
)
from lib_cspm.methods_enum import (
    MethodsEnumCSPM as MethodsEnum,
)
from model.core import (
    AwsCredentials,
    Vulnerabilities,
)


async def get_paginated_items(
    credentials: AwsCredentials,
    region: str,
) -> list:
    """Get all items in paginated API calls."""
    pools: list[dict] = []
    args: dict[str, Any] = {
        "credentials": credentials,
        "region": region,
        "service": "efs",
        "function": "describe_file_systems",
        "parameters": {"MaxItems": 50},
    }
    data = await run_boto3_fun(**args)
    object_name = "FileSystems"
    pools += data.get(object_name, [])

    next_token = data.get("NextMarker", None)
    while next_token:
        args["parameters"]["Marker"] = next_token
        data = await run_boto3_fun(**args)
        pools += data.get(object_name, [])
        next_token = data.get("NextMarker", None)

    return pools


@SHIELD
async def efs_is_encryption_disabled(
    credentials: AwsCredentials,
    region: str,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.EFS_IS_ENCRYPTION_DISABLED
    vulns: Vulnerabilities = ()
    filesystems = await get_paginated_items(credentials, region)
    for filesystem in filesystems:
        if not filesystem.get("Encrypted"):
            locations = [
                Location(
                    arn=(filesystem["FileSystemArn"]),
                    method=method,
                    values=(filesystem["Encrypted"],),
                    access_patterns=("/Encrypted",),
                ),
            ]
            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                aws_response=filesystem,
            )

    return (vulns, True)


CHECKS: AwsMethodsTuple = (efs_is_encryption_disabled,)
