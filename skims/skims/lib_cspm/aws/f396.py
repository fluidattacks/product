from typing import (
    Any,
)

from lib_cspm.aws.model import (
    AwsMethodsTuple,
    Location,
)
from lib_cspm.aws.shield import (
    SHIELD,
)
from lib_cspm.aws.utils import (
    build_vulnerabilities,
    run_boto3_fun,
)
from lib_cspm.methods_enum import (
    MethodsEnumCSPM as MethodsEnum,
)
from model.core import (
    AwsCredentials,
    Vulnerabilities,
)


@SHIELD
async def kms_key_is_key_rotation_absent_or_disabled(
    credentials: AwsCredentials,
    region: str,
) -> tuple[Vulnerabilities, bool]:
    paginated_results_key = "Keys"
    response: dict[str, Any] = await run_boto3_fun(
        credentials=credentials,
        service="kms",
        function="list_keys",
        region=region,
        paginated_results_key=paginated_results_key,
    )
    method = MethodsEnum.KMS_KEY_IS_KEY_ROTATION_ABSENT_OR_DISABLED
    keys = response.get(paginated_results_key, []) if response else []
    vulns: Vulnerabilities = ()
    for key in keys:
        key_rotation: dict[str, Any] = await run_boto3_fun(
            region=region,
            credentials=credentials,
            service="kms",
            function="get_key_rotation_status",
            parameters={"KeyId": str(key["KeyId"])},
        )
        key_status: dict[str, Any] = await run_boto3_fun(
            region=region,
            credentials=credentials,
            service="kms",
            function="describe_key",
            parameters={"KeyId": str(key["KeyId"])},
        )
        key_rotation_status = key_rotation.get("KeyRotationEnabled", True)
        key_state = key_status.get("KeyMetadata", {}).get("KeyState", "")

        if not key_rotation_status and key_state not in {"PendingDeletion", "Disabled"}:
            locations = [
                Location(
                    arn=(key["KeyArn"]),
                    method=method,
                    values=(),
                    access_patterns=(),
                ),
            ]

            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                aws_response=key_rotation,
            )

    return (vulns, True)


@SHIELD
async def secrets_manager_has_automatic_rotation_disabled(
    credentials: AwsCredentials,
    region: str,
) -> tuple[Vulnerabilities, bool]:
    paginated_results_key = "SecretList"
    describe_file_systems: dict[str, Any] = await run_boto3_fun(
        region=region,
        credentials=credentials,
        service="secretsmanager",
        function="list_secrets",
        paginated_results_key=paginated_results_key,
    )
    secrets = describe_file_systems.get(paginated_results_key, [])

    vulns: Vulnerabilities = ()
    method = MethodsEnum.SECRETS_MANAGER_HAS_AUTOMATIC_ROTATION_DISABLED
    for secret_ in secrets:
        description: dict[str, Any] = await run_boto3_fun(
            region=region,
            credentials=credentials,
            service="secretsmanager",
            function="describe_secret",
            parameters={"SecretId": secret_["Name"]},
        )
        rot_enabled = description.get("RotationEnabled", "")
        if not rot_enabled:
            locations = [
                Location(
                    arn=(description["ARN"]),
                    method=method,
                    values=(),
                    access_patterns=(),
                ),
            ]

            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                aws_response=description,
            )

    return (vulns, True)


CHECKS: AwsMethodsTuple = (
    kms_key_is_key_rotation_absent_or_disabled,
    secrets_manager_has_automatic_rotation_disabled,
)
