from typing import (
    Any,
)

from lib_cspm.aws.model import (
    AwsMethodsTuple,
    Location,
)
from lib_cspm.aws.shield import (
    SHIELD,
)
from lib_cspm.aws.utils import (
    build_arn,
    build_vulnerabilities,
    get_owner_id,
    run_boto3_fun,
)
from lib_cspm.methods_enum import (
    MethodsEnumCSPM as MethodsEnum,
)
from model.core import (
    AwsCredentials,
    Vulnerabilities,
)


@SHIELD
async def has_default_security_groups_in_use(
    credentials: AwsCredentials,
    region: str,
) -> tuple[Vulnerabilities, bool]:
    paginated_results_key = "Reservations"
    response: dict[str, Any] = await run_boto3_fun(
        credentials=credentials,
        service="ec2",
        function="describe_instances",
        region=region,
        paginated_results_key=paginated_results_key,
    )
    owner_id = await get_owner_id(credentials)
    reservations = response.get(paginated_results_key, []) if response else []
    vulns: Vulnerabilities = ()
    method = MethodsEnum.HAS_DEFAULT_SECURITY_GROUPS_IN_USE
    for res in reservations:
        for instance in res["Instances"]:
            locations: list[Location] = []
            for index, security_group in enumerate(instance["SecurityGroups"]):
                group_name = security_group["GroupName"]
                if "default" in group_name:
                    parameters = {
                        "Region": region,
                        "Account": owner_id,
                        "InstanceId": instance["InstanceId"],
                    }
                    locations.append(
                        Location(
                            access_patterns=(f"/SecurityGroups/{index}/GroupName",),
                            arn=build_arn(
                                service="ec2",
                                resource="instance",
                                parameters=parameters,
                            ),
                            values=(instance["SecurityGroups"],),
                            method=method,
                        ),
                    )

            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                aws_response=instance,
            )
    return (vulns, True)


def get_template_security_groups(
    launch_template_data: dict[str, Any],
) -> bool:
    template_network = launch_template_data.get("NetworkInterfaces", [])

    for interface in template_network:
        template_security_groups = interface.get("Groups", [])
        if template_security_groups:
            return True

    return False


@SHIELD
async def use_default_security_group(
    credentials: AwsCredentials,
    region: str,
) -> tuple[Vulnerabilities, bool]:
    security_group_attributes = {"SecurityGroups", "SecurityGroupIds"}
    method = MethodsEnum.USE_DEFAULT_SECURITY_GROUP
    paginated_results_key = "LaunchTemplateVersions"
    response: dict[str, Any] = await run_boto3_fun(
        region=region,
        credentials=credentials,
        service="ec2",
        function="describe_launch_template_versions",
        parameters={
            "Versions": ["$Latest"],
        },
        paginated_results_key=paginated_results_key,
    )
    vulns: Vulnerabilities = ()

    launch_template_versions = response.get(paginated_results_key, {})
    if launch_template_versions:
        owner_id = await get_owner_id(credentials)
        for template_version in launch_template_versions:
            locations: list[Location] = []
            template_data = template_version.get("LaunchTemplateData", {})
            if not any(
                template_data.get(attr, False) for attr in security_group_attributes
            ) and not get_template_security_groups(template_data):
                parameters = {
                    "Region": region,
                    "Account": owner_id,
                    "LaunchTemplateId": template_version["LaunchTemplateId"],
                }

                locations = [
                    Location(
                        access_patterns=(),
                        arn=(
                            build_arn(
                                service="ec2",
                                resource="launch-template",
                                parameters=parameters,
                            )
                        ),
                        values=(),
                        method=method,
                    ),
                ]
            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                aws_response=template_version,
            )

    return (vulns, True)


CHECKS: AwsMethodsTuple = (
    use_default_security_group,
    has_default_security_groups_in_use,
)
