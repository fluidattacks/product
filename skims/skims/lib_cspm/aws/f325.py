import ast
import json
import re
from collections.abc import (
    Iterable,
)
from contextlib import (
    suppress,
)
from typing import (
    Any,
)

from lib_cspm.aws.model import (
    AwsMethodsTuple,
    Location,
)
from lib_cspm.aws.shield import (
    SHIELD,
)
from lib_cspm.aws.utils import (
    _get_action,
    _is_action_permissive,
    build_arn,
    build_vulnerabilities,
    run_boto3_fun,
)
from lib_cspm.methods_enum import (
    MethodsEnumCSPM as MethodsEnum,
)
from model.core import (
    AwsCredentials,
    Vulnerabilities,
)
from utils.aws.iam import (
    ALL_RESOURCES,
    RESOURCES_EXCEPTIONS,
)


def expand_wildcard_to_actions(wildcard_action: str, all_resources: set) -> set:
    service, pattern = wildcard_action.split(":", 1)
    regex_pattern = f"^{service}:{pattern.replace('*', '.*')}$"
    return {action for action in all_resources if re.match(regex_pattern, action)}


def should_include_action(
    serv: str,
    act: str,
    resource_exceptions: set,
    all_resources: set,
) -> bool:
    action_full = f"{serv}:{act}"

    if action_full in resource_exceptions:
        return False

    if "*" in act:
        expanded_actions = expand_wildcard_to_actions(
            f"{serv}:{act}",
            all_resources,
        )
        if all(action in resource_exceptions for action in expanded_actions):
            return False

    return True


async def get_paginated_items(
    credentials: AwsCredentials,
) -> list:
    """Get all items in paginated API calls."""
    pools: list[dict] = []
    args: dict[str, Any] = {
        "credentials": credentials,
        "service": "iam",
        "function": "list_policies",
        "parameters": {"MaxItems": 50, "Scope": "Local", "OnlyAttached": True},
    }
    data = await run_boto3_fun(**args)
    object_name = "Policies"
    pools += data.get(object_name, [])

    next_token = data.get("Marker", None)
    while next_token:
        args["parameters"]["Marker"] = next_token
        data = await run_boto3_fun(**args)
        pools += data.get(object_name, [])
        next_token = data.get("Marker", None)

    return pools


async def iterate_kms_has_master_keys_exposed_to_everyone(
    key_policy: dict[str, Any],
    alias: dict[str, Any],
    method: MethodsEnum,
) -> list[Location]:
    locations: list[Location] = []
    for index, item in enumerate(key_policy["Statement"]):
        if item["Principal"]["AWS"] == "*" and "Condition" not in item:
            locations.append(
                Location(
                    access_patterns=(f"/Statement/{index}/Principal/AWS",),
                    arn=(f"{alias['AliasArn']}"),
                    values=(item["Principal"]["AWS"],),
                    method=method,
                ),
            )
    return locations


@SHIELD
async def kms_has_master_keys_exposed_to_everyone(
    credentials: AwsCredentials,
    region: str,
) -> tuple[Vulnerabilities, bool]:
    paginated_results_key = "Aliases"
    response: dict[str, Any] = await run_boto3_fun(
        region=region,
        credentials=credentials,
        service="kms",
        function="list_aliases",
        paginated_results_key=paginated_results_key,
    )

    aliases = response.get(paginated_results_key, []) if response else []
    method = MethodsEnum.KMS_HAS_MASTER_KEYS_EXPOSED_TO_EVERYONE
    vulns: Vulnerabilities = ()
    if aliases:
        for alias in aliases:
            with suppress(KeyError):
                paginated_results_key = "PolicyNames"
                list_key_policies: dict[str, Any] = await run_boto3_fun(
                    region=region,
                    credentials=credentials,
                    service="kms",
                    function="list_key_policies",
                    parameters={
                        "KeyId": alias["TargetKeyId"],
                    },
                    paginated_results_key=paginated_results_key,
                )
                policy_names = list_key_policies.get(paginated_results_key, {})
                for policy in policy_names:
                    get_key_policy: dict[str, Any] = await run_boto3_fun(
                        region=region,
                        credentials=credentials,
                        service="kms",
                        function="get_key_policy",
                        parameters={
                            "KeyId": alias["TargetKeyId"],
                            "PolicyName": policy,
                        },
                    )
                    key_string = get_key_policy["Policy"]
                    key_policy = ast.literal_eval(key_string)

                    locations = await iterate_kms_has_master_keys_exposed_to_everyone(
                        key_policy,
                        alias,
                        method,
                    )

                    vulns += build_vulnerabilities(
                        locations=locations,
                        method=method,
                        aws_response=key_policy,
                    )

    return (vulns, True)


def resource_all(resource: Any) -> bool:  # noqa: ANN401
    """Check if an action is permitted for any resource."""
    if isinstance(resource, list):
        success = any(map(resource_all, resource))
    elif isinstance(resource, str):
        success = resource == "*"
    else:
        success = any(resource_all(i) for i in dict(resource).values())

    return success


def force_list(obj: Any) -> list[Any]:  # noqa: ANN401
    """Wrap the element in a list, or if list, leave it intact."""
    if not obj:
        ret = []
    elif isinstance(obj, list):
        ret = obj
    else:
        ret = [obj]
    return ret


def iterate_actions(
    item: dict[str, Any],
    action: list[str],
    arn: str,
    index: int,
    method: MethodsEnum,
) -> list[Location]:
    locations: list[Location] = []
    for idx, raw_act in enumerate(action):
        serv, act = raw_act.split(":")
        if act == "*":
            locations.append(
                Location(
                    access_patterns=(f"/{index}/Action",),
                    arn=arn,
                    values=(
                        item["Action"][idx] if isinstance(item["Action"], list) else item["Action"],
                    ),
                    method=method,
                ),
            )
        if should_include_action(serv, act, RESOURCES_EXCEPTIONS, ALL_RESOURCES):
            locations.append(
                Location(
                    access_patterns=(f"/{index}/Resource",),
                    arn=arn,
                    values=(item["Resource"],),
                    method=method,
                ),
            )

    return locations


def policy_actions_has_privilege(
    *,
    action: list[str],
    item: dict[str, Any],
    arn: str,
    index: int,
    method: MethodsEnum,
) -> list[Location]:
    """Check if an action have a privilege."""
    locations: list[Location] = []

    with suppress(KeyError):
        if action == ["*"]:
            locations = [
                Location(
                    access_patterns=(f"/{index}/Action",),
                    arn=(arn),
                    values=(item["Action"],),
                    method=method,
                ),
            ]
        else:
            locations = iterate_actions(item, action, arn, index, method)

    return locations


def get_locations(
    policy_statements: Iterable,
    policy: dict[str, Any],
    method: MethodsEnum,
) -> list[Location]:
    locations: list[Location] = []
    arn = policy["Arn"]
    for index, raw_item in enumerate(policy_statements):
        item = ast.literal_eval(str(raw_item))
        if (
            item["Effect"] == "Allow"
            and "Resource" in item
            and resource_all(item["Resource"])
            and (actions := force_list(item.get("Action", [])))
        ):
            locations.extend(
                policy_actions_has_privilege(
                    action=actions,
                    item=item,
                    arn=arn,
                    index=index,
                    method=method,
                ),
            )

    return locations


@SHIELD
async def iam_has_wildcard_resource_on_write_action(
    credentials: AwsCredentials,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.IAM_HAS_WILDCARD_RESOURCE_ON_WRITE_ACTION

    policies = await get_paginated_items(credentials)
    vulns: Vulnerabilities = ()
    if policies:
        for policy in policies:
            pol_ver: dict[str, Any] = await run_boto3_fun(
                credentials=credentials,
                service="iam",
                function="get_policy_version",
                parameters={
                    "PolicyArn": str(policy["Arn"]),
                    "VersionId": str(policy["DefaultVersionId"]),
                },
            )
            policy_names = pol_ver.get("PolicyVersion", {})
            pol_access = ast.literal_eval(str(policy_names.get("Document", {})))
            policy_statements = ast.literal_eval(str(pol_access.get("Statement", [])))
            if not isinstance(policy_statements, list):
                policy_statements = [policy_statements]

            locations = get_locations(policy_statements, policy, method)

            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                aws_response=policy_statements,
            )

    return (vulns, True)


def _is_statement_miss_configured(
    stmt: dict[str, Any],
    index: int,
    policy: dict[str, Any],
    method: MethodsEnum,
) -> list[Location]:
    effect = stmt.get("Effect")
    if effect != "Allow":
        return []
    no_action = stmt.get("NotAction")
    no_resource = stmt.get("NotResource")
    wildcard_action = re.compile(r"^((\*)|(\w+:\*))$")
    locations: list[Location] = []
    if no_action and isinstance(no_action, list):
        locations.append(
            Location(
                access_patterns=(f"/Document/Statement/{index}/NotAction",),
                arn=policy["Arn"],
                values=(no_action,),
                method=method,
            ),
        )
    elif no_resource and isinstance(no_resource, list):
        locations.append(
            Location(
                access_patterns=(f"/Document/Statement/{index}/NotResource",),
                arn=policy["Arn"],
                values=(no_resource,),
                method=method,
            ),
        )
    elif (
        (action := stmt.get("Action"))
        and (action_list := action if isinstance(action, list) else [action])
        and any(wildcard_action.match(act) for act in action_list)
    ):
        locations.append(
            Location(
                access_patterns=(f"/Document/Statement/{index}/Action",),
                arn=policy["Arn"],
                values=(action,),
                method=method,
            ),
        )
    return locations


@SHIELD
async def iam_is_policy_miss_configured(
    credentials: AwsCredentials,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.IAM_IS_POLICY_MISS_CONFIGURED
    policies = await get_paginated_items(credentials)
    vulns: Vulnerabilities = ()
    if policies:
        for policy in policies:
            locations: list[Location] = []
            pol_ver: dict[str, Any] = await run_boto3_fun(
                credentials=credentials,
                service="iam",
                function="get_policy_version",
                parameters={
                    "PolicyArn": str(policy["Arn"]),
                    "VersionId": str(policy["DefaultVersionId"]),
                },
            )
            policy_names = pol_ver.get("PolicyVersion", {})
            pol_access = ast.literal_eval(str(policy_names.get("Document", {})))
            policy_statements = ast.literal_eval(str(pol_access.get("Statement", [])))
            if not isinstance(policy_statements, list):
                policy_statements = [policy_statements]

            for index, policy_statement in enumerate(policy_statements):
                locations.extend(
                    _is_statement_miss_configured(policy_statement, index, policy, method),
                )
            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                aws_response=policy_names,
            )

    return (vulns, True)


async def _is_public_grant(grant: dict[str, Any]) -> bool:
    public_acl = [
        "http://acs.amazonaws.com/groups/global/AllUsers",
        "http://acs.amazonaws.com/groups/global/AuthenticatedUsers",
    ]
    perms = ["READ", "WRITE", "FULL_CONTROL", "READ_ACP", "WRITE_ACP"]

    public_grants = [
        grant
        for (key, val) in grant.items()
        if key == "Permission" and any(perm in val for perm in perms)
        for (grantee_k, _) in grant["Grantee"].items()
        if ("URI" in grantee_k and grant["Grantee"]["URI"] in public_acl)
    ]

    return len(public_grants) > 0


@SHIELD
async def public_buckets(
    credentials: AwsCredentials,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.AWS_S3_PUBLIC_BUCKETS
    response: dict[str, Any] = await run_boto3_fun(
        credentials=credentials,
        service="s3",
        function="list_buckets",
    )
    buckets = response.get("Buckets", []) if response else []

    vulns: Vulnerabilities = ()
    if buckets:
        for bucket in buckets:
            locations: list[Location] = []
            bucket_name = bucket["Name"]
            bucket_grants: dict[str, Any] = await run_boto3_fun(
                credentials=credentials,
                service="s3",
                function="get_bucket_acl",
                parameters={"Bucket": str(bucket_name)},
            )
            grants = bucket_grants.get("Grants", [])
            for index, grant in enumerate(grants):
                is_public = await _is_public_grant(grant)
                if is_public:
                    parameters = {
                        "BucketName": bucket_name,
                    }
                    arn = build_arn(service="s3", resource="bucket", parameters=parameters)
                    locations = [
                        *[
                            Location(
                                access_patterns=(f"/Grants/{index}/Permission",),
                                arn=(arn),
                                values=(grant["Permission"],),
                                method=method,
                            ),
                        ],
                    ]

            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                aws_response=bucket_grants,
            )

    return (vulns, True)


async def _is_public_bucket(credentials: AwsCredentials, bucket_name: str) -> bool:
    response: dict[str, Any] = await run_boto3_fun(
        credentials=credentials,
        service="s3",
        function="get_bucket_acl",
        parameters={"Bucket": bucket_name},
    )
    if response:
        grants = response.get("Grants", [])
        for grant in grants:
            if await _is_public_grant(grant):
                return True
    return False


@SHIELD
async def private_buckets_not_blocking_public_acls(
    credentials: AwsCredentials,
) -> tuple[Vulnerabilities, bool]:
    response: dict[str, Any] = await run_boto3_fun(
        credentials=credentials,
        service="s3",
        function="list_buckets",
    )
    buckets = response.get("Buckets", []) if response else []
    method = MethodsEnum.PRIVATE_BUCKETS_NOT_BLOCKING_PUBLIC_ACLS
    vulns: Vulnerabilities = ()
    for bucket in buckets:
        bucket_name = bucket["Name"]
        if await _is_public_bucket(credentials, str(bucket_name)):
            continue

        bucket_config: dict[str, Any] = await run_boto3_fun(
            region="us-west-2",
            credentials=credentials,
            service="s3",
            function="get_public_access_block",
            parameters={"Bucket": str(bucket_name)},
        )

        public_access_config = bucket_config.get("PublicAccessBlockConfiguration")
        parameters = {"BucketName": bucket_name}
        arn = build_arn(service="s3", resource="bucket", parameters=parameters)
        if not public_access_config:
            locations = [
                Location(
                    access_patterns=(),
                    arn=arn,
                    values=(),
                    method=method,
                ),
            ]
        else:
            locations = [
                Location(
                    access_patterns=(f"/PublicAccessBlockConfiguration/{config_item}",),
                    arn=arn,
                    values=(public_access_config,),
                    method=method,
                )
                for config_item, value in public_access_config.items()
                if value is False
            ]

        vulns += build_vulnerabilities(
            locations=locations,
            method=method,
            aws_response=bucket_config,
        )
    return (vulns, True)


@SHIELD
async def sqs_is_public(
    credentials: AwsCredentials,
    region: str,
) -> tuple[Vulnerabilities, bool]:
    response: dict[str, Any] = await run_boto3_fun(
        credentials=credentials,
        service="sqs",
        function="list_queues",
        region=region,
    )
    queues = response.get("QueueUrls", []) if response else []
    method = MethodsEnum.SQS_IS_PUBLIC
    vulns: Vulnerabilities = ()
    if queues:
        for queue_url in queues:
            get_queue_attributes: dict[str, Any] = await run_boto3_fun(
                region=region,
                credentials=credentials,
                service="sqs",
                function="get_queue_attributes",
                parameters={
                    "QueueUrl": queue_url,
                    "AttributeNames": ["QueueArn", "All"],
                },
            )
            attrs = get_queue_attributes.get("Attributes", {})
            policy = json.loads(attrs.get("Policy", "{}"))

            for statement in policy.get("Statement", []):
                if (
                    statement.get("Effect", "") == "Allow"
                    and statement.get("Principal", "")
                    in [
                        "*",
                        {"AWS": "*"},
                    ]
                    and not statement.get("Condition", {})
                ):
                    locations = [
                        Location(
                            access_patterns=("/Principal",),
                            arn=(attrs["QueueArn"]),
                            values=(statement["Principal"],),
                            method=method,
                        ),
                    ]
                    vulns += build_vulnerabilities(
                        locations=locations,
                        method=method,
                        aws_response=statement,
                    )

    return (vulns, True)


@SHIELD
async def permissive_policy(
    credentials: AwsCredentials,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.PERMISSIVE_POLICY
    policies = await get_paginated_items(credentials)
    vulns: Vulnerabilities = ()
    if policies:
        for policy in policies:
            locations: list[Location] = []
            pol_ver: dict[str, Any] = await run_boto3_fun(
                credentials=credentials,
                service="iam",
                function="get_policy_version",
                parameters={
                    "PolicyArn": str(policy["Arn"]),
                    "VersionId": str(policy["DefaultVersionId"]),
                },
            )
            policy_names = pol_ver.get("PolicyVersion", {})
            pol_access = ast.literal_eval(str(policy_names.get("Document", {})))
            policy_statements = ast.literal_eval(str(pol_access.get("Statement", [])))

            if not isinstance(policy_statements, list):
                policy_statements = [policy_statements]

            for index, raw_item in enumerate(policy_statements):
                item = ast.literal_eval(str(raw_item))
                with suppress(KeyError):
                    action = _get_action(item, "Action")

                    if (
                        item["Effect"] == "Allow"
                        and any(map(_is_action_permissive, action))
                        and item["Resource"] == "*"
                    ):
                        locations.append(
                            Location(
                                access_patterns=(
                                    f"/Statement/{index}/Action",
                                    f"/Statement/{index}/Resource",
                                ),
                                arn=(f"{policy['Arn']}"),
                                values=(
                                    raw_item["Action"],
                                    raw_item["Resource"],
                                ),
                                method=method,
                            ),
                        )

            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                aws_response=pol_access,
            )

    return (vulns, True)


async def check_group_permissive_policy(
    method: MethodsEnum,
    group_arn: str,
    policy: dict[str, Any],
) -> Vulnerabilities:
    vulns: Vulnerabilities = ()
    raw_pol_doc = policy.get("PolicyDocument", "{}")
    policy_document = raw_pol_doc if isinstance(raw_pol_doc, dict) else json.loads(raw_pol_doc)

    if not policy_document:
        return vulns

    policy_statements = policy_document["Statement"]
    if not isinstance(policy_statements, list):
        policy_statements = [policy_statements]

    for index, sts in enumerate(policy_statements):
        with suppress(KeyError):
            action = _get_action(sts, "Action")
            if (
                sts["Effect"] == "Allow"
                and any(map(_is_action_permissive, action))
                and sts["Resource"] == "*"
            ):
                locations = [
                    Location(
                        access_patterns=(f"/Statement/{index}/Action",),
                        arn=group_arn,
                        values=(sts["Action"],),
                        method=method,
                    ),
                ]

                vulns += build_vulnerabilities(
                    locations=locations,
                    method=method,
                    aws_response=policy_document,
                )

    return vulns


@SHIELD
async def group_with_permissive_inline_policies(
    credentials: AwsCredentials,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.GROUP_WITH_PERMISSIVE_INLINE_POLICIES
    paginated_results_key = "Groups"
    response: dict[str, Any] = await run_boto3_fun(
        credentials=credentials,
        service="iam",
        function="list_groups",
        paginated_results_key=paginated_results_key,
    )
    groups = response.get(paginated_results_key, []) if response else []
    vulns: Vulnerabilities = ()
    paginated_results_key = "PolicyNames"
    for group in groups:
        group_policies: dict[str, Any] = await run_boto3_fun(
            credentials=credentials,
            service="iam",
            function="list_group_policies",
            parameters={"GroupName": str(group["GroupName"])},
            paginated_results_key="PolicyNames",
        )
        policy_names = group_policies.get(paginated_results_key, [])
        for policy_name in policy_names:
            policy = await run_boto3_fun(
                credentials=credentials,
                service="iam",
                function="get_group_policy",
                parameters={
                    "GroupName": group["GroupName"],
                    "PolicyName": policy_name,
                },
            )
            policy_vulns = await check_group_permissive_policy(
                method,
                group["Arn"],
                policy,
            )
            vulns += policy_vulns

    return (vulns, True)


CHECKS: AwsMethodsTuple = (
    kms_has_master_keys_exposed_to_everyone,
    iam_has_wildcard_resource_on_write_action,
    iam_is_policy_miss_configured,
    permissive_policy,
    private_buckets_not_blocking_public_acls,
    public_buckets,
    sqs_is_public,
    group_with_permissive_inline_policies,
)
