from typing import (
    Any,
)

from lib_cspm.aws.model import (
    AwsMethodsTuple,
    Location,
)
from lib_cspm.aws.shield import (
    SHIELD,
)
from lib_cspm.aws.utils import (
    build_vulnerabilities,
    run_boto3_fun,
)
from lib_cspm.methods_enum import (
    MethodsEnumCSPM as MethodsEnum,
)
from model.core import (
    AwsCredentials,
    Vulnerabilities,
)


@SHIELD
async def elbv2_listeners_not_using_https(
    credentials: AwsCredentials,
    region: str,
) -> tuple[Vulnerabilities, bool]:
    paginated_results_key = "LoadBalancers"
    response: dict[str, Any] = await run_boto3_fun(
        region=region,
        credentials=credentials,
        service="elbv2",
        function="describe_load_balancers",
        paginated_results_key=paginated_results_key,
    )
    balancers = response.get(paginated_results_key, []) if response else []
    method = MethodsEnum.ELBV2_LISTENERS_NOT_USING_HTTPS
    vulns: Vulnerabilities = ()
    for balancer in balancers:
        locations: list[Location] = []
        load_balancer_arn = balancer["LoadBalancerArn"]

        attributes: dict[str, Any] = await run_boto3_fun(
            region=region,
            credentials=credentials,
            service="elbv2",
            function="describe_listeners",
            parameters={
                "LoadBalancerArn": load_balancer_arn,
            },
        )

        for attrs in attributes.get("Listeners", []):
            if attrs.get("Protocol", "") == "HTTP":
                describe_tags: dict[str, Any] = await run_boto3_fun(
                    region=region,
                    credentials=credentials,
                    service="elbv2",
                    function="describe_tags",
                    parameters={
                        "ResourceArns": [load_balancer_arn],
                    },
                )
                tags = describe_tags.get("TagDescriptions", [])[0].get("Tags", [])
                is_k8s = any(tag["Key"] == "elbv2.k8s.aws/cluster" for tag in tags)
                if not is_k8s:
                    locations = [
                        Location(
                            arn=(attrs["ListenerArn"]),
                            method=method,
                            values=(attrs["Protocol"],),
                            access_patterns=("/Protocol",),
                        ),
                    ]
                    vulns += build_vulnerabilities(
                        locations=locations,
                        method=method,
                        aws_response=attrs,
                    )
    return (vulns, True)


def _iterate_locations(
    distribution: dict[str, Any],
    dist_arn: str,
    method: MethodsEnum,
) -> list[Location]:
    locations: list[Location] = []
    distribution_config = distribution["DistributionConfig"]
    if (
        "DefaultCacheBehavior" in distribution_config
        and (def_cache_beh := distribution_config["DefaultCacheBehavior"])
        and "ViewerProtocolPolicy" in def_cache_beh
        and def_cache_beh["ViewerProtocolPolicy"] == "allow-all"
    ):
        locations = [
            *locations,
            Location(
                arn=(dist_arn),
                method=method,
                values=(def_cache_beh["ViewerProtocolPolicy"],),
                access_patterns=(
                    ("/DistributionConfig/DefaultCacheBehavior/ViewerProtocolPolicy"),
                ),
            ),
        ]

    if (
        "CacheBehaviors" in distribution_config
        and (cache_behaviors := distribution_config["CacheBehaviors"])
        and cache_behaviors["Quantity"] > 0
    ):
        for index, cache_b in enumerate(cache_behaviors["Items"]):
            if "ViewerProtocolPolicy" in cache_b and cache_b["ViewerProtocolPolicy"] == "allow-all":
                locations = [
                    *locations,
                    Location(
                        arn=(dist_arn),
                        method=method,
                        values=(def_cache_beh["ViewerProtocolPolicy"],),
                        access_patterns=(
                            (
                                "/DistributionConfig/CacheBehaviors/"
                                f"Items/{index}/ViewerProtocolPolicy"
                            ),
                        ),
                    ),
                ]

    return locations


@SHIELD
async def cft_serves_content_over_http(
    credentials: AwsCredentials,
) -> tuple[Vulnerabilities, bool]:
    response: dict[str, Any] = await run_boto3_fun(
        credentials=credentials,
        service="cloudfront",
        function="list_distributions",
    )
    distribution_list = response.get("DistributionList", {}) if response else {}
    method = MethodsEnum.CFT_SERVES_CONTENT_OVER_HTTP
    vulns: Vulnerabilities = ()
    if "Items" in distribution_list:
        for dist in distribution_list["Items"]:
            dist_id = dist["Id"]
            dist_arn = dist["ARN"]

            config: dict[str, Any] = await run_boto3_fun(
                credentials=credentials,
                service="cloudfront",
                function="get_distribution",
                parameters={
                    "Id": str(dist_id),
                },
            )
            distribution = config.get("Distribution", {})
            locations = _iterate_locations(distribution, dist_arn, method)

            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                aws_response=distribution,
            )
    return (vulns, True)


@SHIELD
async def aws_cloudfront_traffic_allows_http(
    credentials: AwsCredentials,
) -> tuple[Vulnerabilities, bool]:
    vulns: Vulnerabilities = ()
    method = MethodsEnum.AWS_CLOUDFRONT_TRAFFIC_ALLOWS_HTTP
    distributions: dict[str, Any] = await run_boto3_fun(
        credentials=credentials,
        service="cloudfront",
        function="list_distributions",
    )
    for distribution in distributions.get("DistributionList", {}).get("Items", []):
        arn = distribution["ARN"]
        origins = distribution.get("Origins", {}).get("Items", [])
        for origin in origins:
            origin_protocol_policy = origin.get("CustomOriginConfig", {}).get(
                "OriginProtocolPolicy",
                "",
            )
            if origin_protocol_policy in {"match-viewer", "http-only"}:
                locations = [
                    Location(
                        arn=arn,
                        access_patterns=("/CustomOriginConfig/OriginProtocolPolicy",),
                        values=(origin_protocol_policy,),
                        method=method,
                    ),
                ]
                vulns += build_vulnerabilities(
                    locations=locations,
                    method=method,
                    aws_response=origin,
                )

    return (vulns, True)


@SHIELD
async def aws_opensearch_domain_allows_http(
    credentials: AwsCredentials,
    region: str,
) -> tuple[Vulnerabilities, bool]:
    vulns: Vulnerabilities = ()
    method = MethodsEnum.AWS_OPENSEARCH_DOMAIN_ALLOWS_HTTP
    environments: dict[str, Any] = await run_boto3_fun(
        credentials=credentials,
        region=region,
        service="opensearch",
        function="list_domain_names",
    )
    for domain in environments.get("DomainNames", []):
        domain_config = await run_boto3_fun(
            credentials=credentials,
            region=region,
            service="opensearch",
            function="describe_domain",
            parameters={
                "DomainName": domain["DomainName"],
            },
        )
        domain_status = domain_config.get("DomainStatus", {})
        enforce_https = domain_status.get("DomainEndpointOptions", {}).get("EnforceHTTPS", True)
        if not enforce_https:
            locations = [
                Location(
                    arn=domain_status["ARN"],
                    access_patterns=("/DomainStatus/DomainEndpointOptions/EnforceHTTPS",),
                    values=(enforce_https,),
                    method=method,
                ),
            ]
            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                aws_response=domain_config,
            )

    return (vulns, True)


@SHIELD
async def aws_cloudfront_distribution_viewer_policy_allows_http(
    credentials: AwsCredentials,
) -> tuple[Vulnerabilities, bool]:
    vulns: Vulnerabilities = ()
    method = MethodsEnum.AWS_CLOUDFRONT_DISTRIBUTION_VIEWER_POLICY_ALLOWS_HTTP
    paginated_results_key = "DistributionList"
    distributions: dict[str, Any] = await run_boto3_fun(
        credentials=credentials,
        service="cloudfront",
        function="list_distributions",
    )
    for distribution_item in distributions.get(paginated_results_key, {}).get("Items", []):
        default_cache_behavior = distribution_item.get("DefaultCacheBehavior", {})
        viewer_protocol_policy = default_cache_behavior.get("ViewerProtocolPolicy", "")
        if viewer_protocol_policy in ["allow-all"]:
            locations = [
                Location(
                    arn=distribution_item["ARN"],
                    access_patterns=("/DefaultCacheBehavior/ViewerProtocolPolicy",),
                    values=(viewer_protocol_policy,),
                    method=method,
                ),
            ]
            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                aws_response=distribution_item,
            )

    return (vulns, True)


@SHIELD
async def aws_alb_http_not_redirected_to_https(
    credentials: AwsCredentials,
    region: str,
) -> tuple[Vulnerabilities, bool]:
    vulns: Vulnerabilities = ()
    method = MethodsEnum.AWS_ALB_HTTP_NOT_REDIRECTED_TO_HTTPS
    paginated_results_key = "LoadBalancers"
    load_balancers: dict[str, Any] = await run_boto3_fun(
        region=region,
        credentials=credentials,
        service="elbv2",
        function="describe_load_balancers",
        paginated_results_key=paginated_results_key,
    )
    for load_balancer in load_balancers.get(paginated_results_key, []):
        if load_balancer.get("Type", "") != "application":
            continue
        arn = load_balancer["LoadBalancerArn"]
        listeners: dict[str, Any] = await run_boto3_fun(
            region=region,
            credentials=credentials,
            service="elbv2",
            function="describe_listeners",
            parameters={"LoadBalancerArn": arn},
        )
        for listener in listeners.get("Listeners", []):
            arn = listener["ListenerArn"]
            listener_protocol = listener.get("Protocol", "")
            redirect_config = listener.get("DefaultActions", [{}])[0].get("RedirectConfig", {})
            redir_protocol = redirect_config.get("Protocol", "")
            if listener_protocol != "HTTP":
                continue
            locations = []
            if not redirect_config:
                listener["DefaultActions"][0]["RedirectConfig"] = None
                locations = [
                    Location(
                        arn=arn,
                        access_patterns=("/DefaultActions/0/RedirectConfig",),
                        values=(None,),
                        method=method,
                    ),
                ]
            elif redir_protocol == "HTTP":
                locations = [
                    Location(
                        arn=arn,
                        access_patterns=("/DefaultActions/0/RedirectConfig/Protocol",),
                        values=(redir_protocol,),
                        method=method,
                    ),
                ]
            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                aws_response=listener,
            )
    return (vulns, True)


CHECKS: AwsMethodsTuple = (
    cft_serves_content_over_http,
    elbv2_listeners_not_using_https,
    aws_cloudfront_traffic_allows_http,
    aws_opensearch_domain_allows_http,
    aws_cloudfront_distribution_viewer_policy_allows_http,
    aws_alb_http_not_redirected_to_https,
)
