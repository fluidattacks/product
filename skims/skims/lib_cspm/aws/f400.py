import asyncio
import traceback
from typing import (
    Any,
)

from lib_cspm.aws.model import (
    AwsMethodsTuple,
    BotoClientError,
    Location,
)
from lib_cspm.aws.shield import (
    SHIELD,
)
from lib_cspm.aws.utils import (
    aws_virtual_gateways_for_app_meshes,
    build_arn,
    build_mesh_virtual_gateway_arn,
    build_vulnerabilities,
    get_owner_id,
    run_boto3_fun,
)
from lib_cspm.methods_enum import (
    MethodsEnumCSPM as MethodsEnum,
)
from model.core import (
    AwsCredentials,
    Vulnerabilities,
)


@SHIELD
async def elbv2_has_access_logging_disabled(
    credentials: AwsCredentials,
    region: str,
) -> tuple[Vulnerabilities, bool]:
    paginated_results_key = "LoadBalancers"
    response: dict[str, Any] = await run_boto3_fun(
        region=region,
        credentials=credentials,
        service="elbv2",
        function="describe_load_balancers",
        paginated_results_key=paginated_results_key,
    )
    method = MethodsEnum.ELBV2_HAS_ACCESS_LOGGING_DISABLED
    balancers = response.get(paginated_results_key, []) if response else []
    vulns: Vulnerabilities = ()
    for balancer in balancers:
        load_balancer_arn = balancer["LoadBalancerArn"]
        locations: list[Location] = []
        key_rotation: dict[str, Any] = await run_boto3_fun(
            region=region,
            credentials=credentials,
            service="elbv2",
            function="describe_load_balancer_attributes",
            parameters={"LoadBalancerArn": str(load_balancer_arn)},
        )
        attributes = key_rotation.get("Attributes", "")
        is_access_logs_enable = any(
            attrs["Key"] == "access_logs.s3.enabled" for attrs in attributes
        )

        for index, attrs in enumerate(attributes):
            if (
                is_access_logs_enable
                and attrs["Key"] == "access_logs.s3.enabled"
                and attrs["Value"] != "true"
            ):
                locations = [
                    Location(
                        arn=(balancer["LoadBalancerArn"]),
                        method=method,
                        values=(attrs["Key"],),
                        access_patterns=(f"/{index}/Key",),
                    ),
                ]
                break
            if not is_access_logs_enable:
                locations = [
                    Location(
                        arn=(balancer["LoadBalancerArn"]),
                        method=method,
                        values=(),
                        access_patterns=(),
                    ),
                ]
                break

        vulns += build_vulnerabilities(
            locations=locations,
            method=method,
            aws_response=attributes,
        )

    return (vulns, True)


@SHIELD
async def cloudfront_has_logging_disabled(
    credentials: AwsCredentials,
) -> tuple[Vulnerabilities, bool]:
    response: dict[str, Any] = await run_boto3_fun(
        credentials=credentials,
        service="cloudfront",
        function="list_distributions",
    )
    method = MethodsEnum.CLOUDFRONT_HAS_LOGGING_DISABLED
    distributions = response.get("DistributionList", {}) if response else {}
    vulns: Vulnerabilities = ()
    if "Items" in distributions:
        for dist in distributions["Items"]:
            dist_id = dist["Id"]
            dist_arn = dist["ARN"]
            config: dict[str, Any] = await run_boto3_fun(
                credentials=credentials,
                service="cloudfront",
                function="get_distribution",
                parameters={"Id": str(dist_id)},
            )
            distribution = config.get("Distribution", "")
            distribution_config = distribution.get("DistributionConfig", {})
            logging = distribution_config.get("Logging", "")
            if not logging["Enabled"]:
                locations = [
                    Location(
                        arn=(dist_arn),
                        method=method,
                        values=(logging["Enabled"],),
                        access_patterns=("/DistributionConfig/Logging/Enabled",),
                    ),
                ]

                vulns += build_vulnerabilities(
                    locations=locations,
                    method=method,
                    aws_response=distribution,
                )

    return (vulns, True)


@SHIELD
async def cloudtrail_trails_not_multiregion(
    credentials: AwsCredentials,
    region: str,
) -> tuple[Vulnerabilities, bool]:
    response: dict[str, Any] = await run_boto3_fun(
        region=region,
        credentials=credentials,
        service="cloudtrail",
        function="describe_trails",
        parameters={"includeShadowTrails": False},
    )

    method = MethodsEnum.CLOUDTRAIL_TRAILS_NOT_MULTIREGION
    trails = response.get("trailList", []) if response else []
    vulns: Vulnerabilities = ()
    for trail in trails:
        if not trail["IsMultiRegionTrail"]:
            locations = [
                Location(
                    arn=trail["TrailARN"],
                    method=method,
                    values=(trail["IsMultiRegionTrail"],),
                    access_patterns=("/IsMultiRegionTrail",),
                ),
            ]
            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                aws_response=trail,
            )

    return (vulns, True)


@SHIELD
async def is_trail_bucket_logging_disabled(
    credentials: AwsCredentials,
    region: str,
) -> tuple[Vulnerabilities, bool]:
    response: dict[str, Any] = await run_boto3_fun(
        region=region,
        credentials=credentials,
        service="cloudtrail",
        function="describe_trails",
        parameters={"includeShadowTrails": False},
    )
    method = MethodsEnum.IS_TRAIL_BUCKET_LOGGING_DISABLED
    trails = response.get("trailList", []) if response else []
    vulns: Vulnerabilities = ()
    for trail in trails:
        logging: dict[str, Any] = await run_boto3_fun(
            credentials=credentials,
            service="s3",
            function="get_bucket_logging",
            parameters={"Bucket": trail["S3BucketName"]},
        )
        if not logging.get("LoggingEnabled"):
            locations = [
                Location(
                    arn=trail["TrailARN"],
                    method=method,
                    values=(),
                    access_patterns=(),
                ),
            ]

            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                aws_response=trail,
            )

    return (vulns, True)


@SHIELD
async def s3_has_server_access_logging_disabled(
    credentials: AwsCredentials,
) -> tuple[Vulnerabilities, bool]:
    response: dict[str, Any] = await run_boto3_fun(
        credentials=credentials,
        service="s3",
        function="list_buckets",
    )
    method = MethodsEnum.S3_HAS_SERVER_ACCESS_LOGGING_DISABLED
    buckets = response.get("Buckets", []) if response else []
    vulns: Vulnerabilities = ()
    for bucket in buckets:
        bucket_name = bucket["Name"]
        bucket_logging: dict[str, Any] = await run_boto3_fun(
            credentials=credentials,
            service="s3",
            function="get_bucket_logging",
            parameters={"Bucket": str(bucket_name)},
        )

        bucket_logging_enabled = bool(bucket_logging.get("LoggingEnabled"))
        if not bucket_logging_enabled:
            parameters = {
                "BucketName": bucket["Name"],
            }
            arn = build_arn(service="s3", resource="bucket", parameters=parameters)
            locations = [
                Location(
                    arn=arn,
                    method=method,
                    values=(),
                    access_patterns=(),
                ),
            ]

            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                aws_response=bucket_logging,
            )

    return (vulns, True)


async def redshift_get_paginated_items(
    credentials: AwsCredentials,
    region: str,
) -> list:
    """Get all items in paginated API calls."""
    pools: list[dict] = []
    args: dict[str, Any] = {
        "credentials": credentials,
        "region": region,
        "service": "redshift",
        "function": "describe_clusters",
        "parameters": {"MaxRecords": 50},
    }
    data = await run_boto3_fun(**args)
    object_name = "Clusters"
    pools += data.get(object_name, [])

    next_token = data.get("Marker", None)
    while next_token:
        args["parameters"]["Marker"] = next_token
        data = await run_boto3_fun(**args)
        pools += data.get(object_name, [])
        next_token = data.get("Marker", None)

    return pools


@SHIELD
async def redshift_has_audit_logging_disabled(
    credentials: AwsCredentials,
    region: str,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.REDSHIFT_HAS_AUDIT_LOGGING_DISABLED
    vulns: Vulnerabilities = ()
    clusters = await redshift_get_paginated_items(credentials, region)
    for cluster in clusters:
        cluster_id = cluster["ClusterIdentifier"]
        logging: dict[str, Any] = await run_boto3_fun(
            region=region,
            credentials=credentials,
            service="redshift",
            function="describe_logging_status",
            parameters={
                "ClusterIdentifier": cluster_id,
            },
        )
        logging_enabled = logging.get("LoggingEnabled")
        if not logging_enabled:
            parameters = {
                "Region": region,
                "Account": await get_owner_id(credentials),
                "ClusterName": cluster["ClusterIdentifier"],
            }
            arn = build_arn(
                service="redshift",
                resource="cluster",
                parameters=parameters,
            )

            locations = [
                Location(
                    arn=(arn),
                    method=method,
                    values=(logging_enabled,),
                    access_patterns=("/LoggingEnabled",),
                ),
            ]

            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                aws_response=logging,
            )

    return (vulns, True)


@SHIELD
async def redshift_is_user_activity_logging_disabled(
    credentials: AwsCredentials,
    region: str,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.REDSHIFT_IS_USER_ACTIVITY_LOGGING_DISABLED
    vulns: Vulnerabilities = ()
    clusters = await redshift_get_paginated_items(credentials, region)
    for cluster in clusters:
        param_groups = cluster.get("ClusterParameterGroups", [])

        for group in param_groups:
            describe_cluster_parameters: dict[str, Any] = await run_boto3_fun(
                region=region,
                credentials=credentials,
                service="redshift",
                function="describe_cluster_parameters",
                parameters={
                    "ParameterGroupName": group["ParameterGroupName"],
                },
            )
            params = describe_cluster_parameters.get("Parameters", [])

            for param in params:
                if (
                    param["ParameterName"] == "enable_user_activity_logging"
                    and param["ParameterValue"] == "false"
                ):
                    parameters = {
                        "Region": region,
                        "Account": await get_owner_id(credentials),
                        "ClusterName": cluster["ClusterIdentifier"],
                    }
                    arn = build_arn(
                        service="redshift",
                        resource="cluster",
                        parameters=parameters,
                    )
                    locations = [
                        Location(
                            arn=(arn),
                            method=method,
                            values=(
                                param["ParameterName"],
                                param["ParameterValue"],
                            ),
                            access_patterns=(
                                "/ParameterName",
                                "/ParameterValue",
                            ),
                        ),
                    ]

                    vulns += build_vulnerabilities(
                        locations=locations,
                        method=method,
                        aws_response=param,
                    )
    return (vulns, True)


@SHIELD
async def vpcs_without_flowlog(
    credentials: AwsCredentials,
    region: str,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.VPCS_WITHOUT_FLOWLOG
    vulns: Vulnerabilities = ()
    paginated_results_key = "Vpcs"
    response: dict[str, Any] = await run_boto3_fun(
        region=region,
        credentials=credentials,
        service="ec2",
        function="describe_vpcs",
        parameters={"Filters": [{"Name": "state", "Values": ["available"]}]},
        paginated_results_key=paginated_results_key,
    )
    vpcs = response.get(paginated_results_key, []) if response else []
    for vpc in vpcs:
        cloud_id = vpc["VpcId"]
        paginated_results_key = "FlowLogs"
        vpc_response: dict[str, Any] = await run_boto3_fun(
            region=region,
            credentials=credentials,
            service="ec2",
            function="describe_flow_logs",
            parameters={"Filters": [{"Name": "resource-id", "Values": [cloud_id]}]},
            paginated_results_key=paginated_results_key,
        )
        if paginated_results_key in vpc_response and len(vpc_response[paginated_results_key]) == 0:
            owner_id = await get_owner_id(credentials)
            parameters = {
                "Region": region,
                "Account": owner_id,
                "VpcId": cloud_id,
            }
            arn = build_arn(service="ec2", resource="vpc", parameters=parameters)
            locations = [
                Location(
                    access_patterns=(),
                    arn=(arn),
                    values=(),
                    method=method,
                ),
            ]
            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                aws_response=vpc,
            )

    return (vulns, True)


@SHIELD
async def cloudtrail_logging_disabled(
    credentials: AwsCredentials,
    region: str,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.CLOUDTRAIL_LOGGING_DISABLED
    paginated_results_key = "Trails"
    response: dict[str, Any] = await run_boto3_fun(
        region=region,
        credentials=credentials,
        service="cloudtrail",
        function="list_trails",
        paginated_results_key=paginated_results_key,
    )

    trails = response.get(paginated_results_key, []) if response else []
    vulns: Vulnerabilities = ()
    for trail in trails:
        status_response = await run_boto3_fun(
            region=trail["HomeRegion"],
            credentials=credentials,
            service="cloudtrail",
            function="get_trail_status",
            parameters={"Name": str(trail["Name"])},
        )
        if not status_response.get("IsLogging", True):
            arn = trail["TrailARN"]
            locations = [
                Location(
                    access_patterns=(),
                    arn=(arn),
                    values=(),
                    method=method,
                ),
            ]
            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                aws_response=trail,
            )

    return (vulns, True)


def get_vulns_for_mesh_virtual_gateway_access_logging_disabled(
    region: str,
    virtual_gw_data: dict[str, Any],
    method: MethodsEnum,
) -> Vulnerabilities:
    vulns: Vulnerabilities = ()
    logging_conf = virtual_gw_data["spec"]["logging"]
    arn = build_mesh_virtual_gateway_arn(region=region, virtual_gateway_data=virtual_gw_data)
    if logging_conf == {}:
        locations = [
            Location(
                access_patterns=("/spec/logging",),
                values=(logging_conf,),
                arn=arn,
                method=method,
            ),
        ]
        vulns = build_vulnerabilities(
            locations=locations,
            method=method,
            aws_response=virtual_gw_data,
        )
    return vulns


@SHIELD
async def aws_app_mesh_virtual_gateway_access_logging_disabled(
    credentials: AwsCredentials,
    region: str,
) -> tuple[Vulnerabilities, bool]:
    vulns: Vulnerabilities = ()
    method = MethodsEnum.AWS_APP_MESH_VIRTUAL_GATEWAY_ACCESS_LOGGING_DISABLED
    virtual_gateways_data = await aws_virtual_gateways_for_app_meshes(
        credentials=credentials,
        region=region,
    )
    for virtual_gateway in virtual_gateways_data:
        virtual_gateway_data = virtual_gateway["virtualGateway"]
        vulns += get_vulns_for_mesh_virtual_gateway_access_logging_disabled(
            region=region,
            virtual_gw_data=virtual_gateway_data,
            method=method,
        )
    return (vulns, True)


@SHIELD
async def aws_cloud_trail_delivery_failing(
    credentials: AwsCredentials,
    region: str,
) -> tuple[Vulnerabilities, bool]:
    vulns: Vulnerabilities = ()
    method = MethodsEnum.AWS_CLOUD_TRAIL_DELIVERY_FAILING
    paginated_results_key = "Trails"
    trails: dict[str, Any] = await run_boto3_fun(
        region=region,
        credentials=credentials,
        service="cloudtrail",
        function="list_trails",
        paginated_results_key=paginated_results_key,
    )
    for trail in trails.get(paginated_results_key, []):
        arn = trail["TrailARN"]
        trail_status: dict[str, Any] = await run_boto3_fun(
            region=region,
            credentials=credentials,
            service="cloudtrail",
            function="get_trail_status",
            parameters={"Name": trail.get("Name", "")},
        )
        latest_delivery_error = trail_status.get("LatestDeliveryError", "")
        if latest_delivery_error != "":
            trail["Status"] = {"LatestDeliveryError": latest_delivery_error}
            locations = [
                Location(
                    arn=arn,
                    access_patterns=("/Status/LatestDeliveryError",),
                    values=(latest_delivery_error,),
                    method=method,
                    desc_params={
                        "trail_error": latest_delivery_error,
                    },
                ),
            ]
            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                aws_response=trail,
            )

    return (vulns, True)


@SHIELD
async def aws_config_referencing_missing_s3_bucket(
    credentials: AwsCredentials,
    region: str,
) -> tuple[Vulnerabilities, bool]:
    vulns: Vulnerabilities = ()
    trace_chunk = "An error occurred (404) when calling the HeadBucket operation: Not Found"
    method = MethodsEnum.AWS_CONFIG_REFERENCING_MISSING_S3_BUCKET
    delivery_channels: dict[str, Any] = await run_boto3_fun(
        region=region,
        credentials=credentials,
        service="config",
        function="describe_delivery_channels",
    )
    for channel in delivery_channels.get("DeliveryChannels", []):
        bucket_name = channel.get("s3BucketName", "")
        try:
            await run_boto3_fun(
                region=region,
                credentials=credentials,
                service="s3",
                function="head_bucket",
                parameters={"Bucket": bucket_name},
            )
        except BotoClientError:
            if trace_chunk in str(traceback.format_exc()):
                channel_name = channel.get("name")
                owner_id = await get_owner_id(credentials)
                arn = f"arn:aws:config:${region}:${owner_id}:config-channel/${channel_name}"
                locations = [
                    Location(
                        arn=arn,
                        access_patterns=("/s3BucketName",),
                        values=(bucket_name,),
                        method=method,
                    ),
                ]
                vulns += build_vulnerabilities(
                    locations=locations,
                    method=method,
                    aws_response=channel,
                )
            else:
                raise

    return (vulns, True)


@SHIELD
async def aws_eks_cluster_logging_disabled(
    credentials: AwsCredentials,
    region: str,
) -> tuple[Vulnerabilities, bool]:
    vulns: Vulnerabilities = ()
    method = MethodsEnum.AWS_EKS_CLUSTER_LOGGING_DISABLED
    paginated_results_key = "clusters"
    eks_clusters: dict[str, Any] = await run_boto3_fun(
        credentials=credentials,
        region=region,
        service="eks",
        function="list_clusters",
        paginated_results_key=paginated_results_key,
    )
    for cluster_name in eks_clusters.get(paginated_results_key, []):
        _cluster = await run_boto3_fun(
            credentials=credentials,
            region=region,
            service="eks",
            function="describe_cluster",
            parameters={"name": cluster_name},
        )
        cluster = _cluster["cluster"]
        arn = cluster["arn"]
        cluster_logging = cluster.get("logging", {}).get("clusterLogging", {})
        for index, config in enumerate(cluster_logging):
            enabled_cluster_logging = config.get("enabled")
            if not enabled_cluster_logging:
                locations = [
                    Location(
                        arn=arn,
                        access_patterns=(f"/logging/clusterLogging/{index}",),
                        values=(config,),
                        method=method,
                    ),
                ]
                vulns += build_vulnerabilities(
                    locations=locations,
                    method=method,
                    aws_response=cluster,
                )

    return (vulns, True)


@SHIELD
async def aws_beanstalk_persistent_logs(
    credentials: AwsCredentials,
    region: str,
) -> tuple[Vulnerabilities, bool]:
    vulns: Vulnerabilities = ()
    method = MethodsEnum.AWS_BEANSTALK_PERSISTENT_LOGS
    paginated_results_key = "Environments"
    environments: dict[str, Any] = await run_boto3_fun(
        credentials=credentials,
        region=region,
        service="elasticbeanstalk",
        function="describe_environments",
        paginated_results_key=paginated_results_key,
    )
    for environment in environments.get(paginated_results_key, []):
        arn = environment["EnvironmentArn"]
        configurations = await run_boto3_fun(
            credentials=credentials,
            region=region,
            service="elasticbeanstalk",
            function="describe_configuration_settings",
            parameters={
                "ApplicationName": environment["ApplicationName"],
                "EnvironmentName": environment["EnvironmentName"],
            },
        )
        for config in configurations["ConfigurationSettings"]:
            environment["ConfigurationSettings"] = [config]
            vulnerable_indexes = []
            for option_index, option in enumerate(config["OptionSettings"]):
                if (
                    option["OptionName"] in {"LogPublicationControl", "StreamLogs"}
                    and option.get("Value") == "false"
                ):
                    vulnerable_indexes.append(option_index)
            if len(vulnerable_indexes) == 2:
                locations = [
                    Location(
                        arn=arn,
                        access_patterns=(
                            f"/ConfigurationSettings/0/OptionSettings/{vulnerable_indexes[0]}",
                            f"/ConfigurationSettings/0/OptionSettings/{vulnerable_indexes[1]}",
                        ),
                        values=(
                            "false",
                            "false",
                        ),
                        method=method,
                    ),
                ]
                vulns += build_vulnerabilities(
                    locations=locations,
                    method=method,
                    aws_response=environment,
                )

    return (vulns, True)


@SHIELD
async def aws_opensearch_without_audit_logs(
    credentials: AwsCredentials,
    region: str,
) -> tuple[Vulnerabilities, bool]:
    vulns: Vulnerabilities = ()
    method = MethodsEnum.AWS_OPENSEARCH_WITHOUT_AUDIT_LOGS
    environments: dict[str, Any] = await run_boto3_fun(
        credentials=credentials,
        region=region,
        service="opensearch",
        function="list_domain_names",
    )
    for domain in environments.get("DomainNames", []):
        domain_name = domain["DomainName"]
        domain_config = await run_boto3_fun(
            credentials=credentials,
            region=region,
            service="opensearch",
            function="describe_domain",
            parameters={
                "DomainName": domain_name,
            },
        )
        domain_status = domain_config.get("DomainStatus", {})
        log_publishing_options = domain_status.setdefault("LogPublishingOptions", {})
        audit_logs = log_publishing_options.setdefault("AUDIT_LOGS", {"Enabled": False})
        audit_logs_bool = audit_logs["Enabled"]
        if not audit_logs_bool:
            locations = [
                Location(
                    arn=domain_status["ARN"],
                    access_patterns=("/DomainStatus/LogPublishingOptions/AUDIT_LOGS/Enabled",),
                    values=(audit_logs_bool,),
                    method=method,
                ),
            ]
            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                aws_response=domain_config,
            )

    return (vulns, True)


@SHIELD
async def aws_mq_broker_logs_disabled(
    credentials: AwsCredentials,
    region: str,
) -> tuple[Vulnerabilities, bool]:
    vulns: Vulnerabilities = ()
    method = MethodsEnum.AWS_MQ_BROKER_LOGS_DISABLED
    paginated_results_key = "BrokerSummaries"
    brokers: dict[str, Any] = await run_boto3_fun(
        credentials=credentials,
        region=region,
        service="mq",
        function="list_brokers",
        paginated_results_key=paginated_results_key,
    )
    for summary in brokers.get(paginated_results_key, []):
        broker_description: dict[str, Any] = await run_boto3_fun(
            credentials=credentials,
            region=region,
            service="mq",
            function="describe_broker",
            parameters={"BrokerId": summary["BrokerId"]},
        )
        logs = broker_description.get("Logs", {})
        if "Audit" in logs and "General" in logs and (not logs["Audit"] or not logs["General"]):
            locations = [
                Location(
                    arn=broker_description["BrokerArn"],
                    access_patterns=("/Logs",),
                    values=(logs,),
                    method=method,
                ),
            ]
            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                aws_response=broker_description,
            )

    return (vulns, True)


@SHIELD
async def aws_route53_dns_query_logging_disabled(
    credentials: AwsCredentials,
    region: str,
) -> tuple[Vulnerabilities, bool]:
    async def check_logging_config(hosted_zone: dict[str, Any]) -> Vulnerabilities:
        hosted_zone_id = hosted_zone["Id"]
        logging_configs: dict[str, Any] = await run_boto3_fun(
            credentials=credentials,
            region=region,
            service="route53",
            function="list_query_logging_configs",
            parameters={"HostedZoneId": hosted_zone_id},
        )
        query_logging_configs = logging_configs.get("QueryLoggingConfigs", [])
        if query_logging_configs:
            return ()
        hosted_zone["QueryLoggingConfigs"] = []
        arn = build_arn(
            service="route53",
            resource="hostedzone",
            parameters={"Id": hosted_zone_id},
        )
        locations = [
            Location(
                arn=arn,
                access_patterns=("/QueryLoggingConfigs",),
                values=(query_logging_configs,),
                method=method,
            ),
        ]
        return build_vulnerabilities(
            locations=locations,
            method=method,
            aws_response=hosted_zone,
        )

    method = MethodsEnum.AWS_ROUTE53_DNS_QUERY_LOGGING_DISABLED
    paginated_results_key = "HostedZones"
    hosted_zones: dict[str, Any] = await run_boto3_fun(
        credentials=credentials,
        region=region,
        service="route53",
        function="list_hosted_zones",
        paginated_results_key=paginated_results_key,
    )
    public_zones = [
        hz
        for hz in hosted_zones.get(paginated_results_key, [])
        if not hz.get("Config", {}).get("PrivateZone", False)
    ]
    vulns: Vulnerabilities = sum(
        await asyncio.gather(*(check_logging_config(hz) for hz in public_zones)),
        (),
    )

    return (vulns, True)


@SHIELD
async def aws_document_db_without_audit_logs(
    credentials: AwsCredentials,
    region: str,
) -> tuple[Vulnerabilities, bool]:
    vulns: Vulnerabilities = ()
    method = MethodsEnum.AWS_DOCUMENT_DB_WITHOUT_AUDIT_LOGS
    paginated_results_key = "DBClusters"
    db_clusters: dict[str, Any] = await run_boto3_fun(
        credentials=credentials,
        region=region,
        service="docdb",
        function="describe_db_clusters",
        paginated_results_key=paginated_results_key,
    )
    for db_cluster in db_clusters.get(paginated_results_key, []):
        arn = db_cluster["DBClusterArn"]
        parameter_group = db_cluster.get("DBClusterParameterGroup", "")
        param_groups: dict[str, Any] = await run_boto3_fun(
            credentials=credentials,
            region=region,
            service="docdb",
            function="describe_db_cluster_parameters",
            parameters={
                "DBClusterParameterGroupName": parameter_group,
                "Filters": [
                    {
                        "Name": "parameter-name",
                        "Values": [
                            "audit_logs",
                        ],
                    },
                ],
            },
        )
        audit_logs_param = param_groups.get("Parameters", [])
        if not audit_logs_param:
            continue
        audit_logs_param_value = audit_logs_param[0].get("ParameterValue")
        if audit_logs_param_value == "disabled":
            db_cluster["ClusterParameters"] = audit_logs_param
            locations = [
                Location(
                    arn=arn,
                    access_patterns=("/ClusterParameters/0/ParameterValue",),
                    values=(audit_logs_param_value,),
                    method=method,
                    desc_params={
                        "param_name": "audit_logs",
                        "param_value": audit_logs_param_value,
                        "param_group": parameter_group,
                    },
                ),
            ]
            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                aws_response=db_cluster,
            )

    return (vulns, True)


@SHIELD
async def aws_rds_db_cluster_logs_disabled(
    credentials: AwsCredentials,
    region: str,
) -> tuple[Vulnerabilities, bool]:
    vulns: Vulnerabilities = ()
    method = MethodsEnum.AWS_RDS_DB_CLUSTER_LOGS_DISABLED
    paginated_results_key = "DBClusters"
    db_clusters: dict[str, Any] = await run_boto3_fun(
        credentials=credentials,
        region=region,
        service="rds",
        function="describe_db_clusters",
        paginated_results_key=paginated_results_key,
    )
    for db_cluster in db_clusters.get(paginated_results_key, []):
        arn = db_cluster["DBClusterArn"]
        cloudwatch_logs_enabled = db_cluster.get("EnabledCloudwatchLogsExports")
        if not cloudwatch_logs_enabled:
            db_cluster["EnabledCloudwatchLogsExports"] = db_cluster.get(
                "EnabledCloudwatchLogsExports",
                [],
            )
            locations = [
                Location(
                    arn=arn,
                    access_patterns=("/EnabledCloudwatchLogsExports",),
                    values=([],),
                    method=method,
                ),
            ]
            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                aws_response=db_cluster,
            )
    return (vulns, True)


@SHIELD
async def aws_rds_db_instance_logs_disabled(
    credentials: AwsCredentials,
    region: str,
) -> tuple[Vulnerabilities, bool]:
    vulns: Vulnerabilities = ()
    method = MethodsEnum.AWS_RDS_DB_INSTANCE_LOGS_DISABLED
    paginated_results_key = "DBInstances"
    db_instances: dict[str, Any] = await run_boto3_fun(
        credentials=credentials,
        region=region,
        service="rds",
        function="describe_db_instances",
        paginated_results_key=paginated_results_key,
    )
    for db_instance in db_instances.get(paginated_results_key, []):
        arn = db_instance["DBInstanceArn"]
        cloudwatch_logs_enabled = db_instance.get("EnabledCloudwatchLogsExports")
        if not cloudwatch_logs_enabled:
            db_instance["EnabledCloudwatchLogsExports"] = db_instance.get(
                "EnabledCloudwatchLogsExports",
                [],
            )
            locations = [
                Location(
                    arn=arn,
                    access_patterns=("/EnabledCloudwatchLogsExports",),
                    values=([],),
                    method=method,
                ),
            ]
            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                aws_response=db_instance,
            )
    return (vulns, True)


@SHIELD
async def aws_global_accelerator_flow_logs_disabled(
    credentials: AwsCredentials,
    region: str,
) -> tuple[Vulnerabilities, bool]:
    vulns: Vulnerabilities = ()
    method = MethodsEnum.AWS_GLOBAL_ACCELERATOR_FLOW_LOGS_DISABLED
    paginated_results_key = "Accelerators"
    accelerators: dict[str, Any] = await run_boto3_fun(
        credentials=credentials,
        region=region,
        service="globalaccelerator",
        function="list_accelerators",
        paginated_results_key=paginated_results_key,
    )
    for accelerator in accelerators.get(paginated_results_key, []):
        arn = accelerator["AcceleratorArn"]
        accelerator_attrs: dict[str, Any] = await run_boto3_fun(
            credentials=credentials,
            region=region,
            service="globalaccelerator",
            function="describe_accelerator_attributes",
            parameters={"AcceleratorArn": arn},
        )
        flow_logs_enabled = accelerator_attrs.get("AcceleratorAttributes", {}).get(
            "FlowLogsEnabled",
            True,
        )
        if not flow_logs_enabled:
            locations = [
                Location(
                    arn=arn,
                    access_patterns=("/AcceleratorAttributes/FlowLogsEnabled",),
                    values=(flow_logs_enabled,),
                    method=method,
                ),
            ]
            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                aws_response=accelerator_attrs,
            )

    return (vulns, True)


@SHIELD
async def aws_neptune_db_instance_logs_disabled(
    credentials: AwsCredentials,
    region: str,
) -> tuple[Vulnerabilities, bool]:
    vulns: Vulnerabilities = ()
    method = MethodsEnum.AWS_NEPTUNE_DB_INSTANCE_LOGS_DISABLED
    paginated_results_key = "DBInstances"
    db_instances: dict[str, Any] = await run_boto3_fun(
        credentials=credentials,
        region=region,
        service="neptune",
        function="describe_db_instances",
        paginated_results_key=paginated_results_key,
    )
    for db_instance in db_instances.get(paginated_results_key, []):
        arn = db_instance["DBInstanceArn"]
        cloudwatch_logs_enabled = db_instance.get("EnabledCloudwatchLogsExports")
        if not cloudwatch_logs_enabled:
            db_instance["EnabledCloudwatchLogsExports"] = db_instance.get(
                "EnabledCloudwatchLogsExports",
                [],
            )
            locations = [
                Location(
                    arn=arn,
                    access_patterns=("/EnabledCloudwatchLogsExports",),
                    values=([],),
                    method=method,
                ),
            ]
            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                aws_response=db_instance,
            )

    return (vulns, True)


@SHIELD
async def aws_msk_cluster_logging_disabled(
    credentials: AwsCredentials,
    region: str,
) -> tuple[Vulnerabilities, bool]:
    vulns: Vulnerabilities = ()
    method = MethodsEnum.AWS_MSK_CLUSTER_LOGGING_DISABLED
    paginated_results_key = "ClusterInfoList"
    clusters: dict[str, Any] = await run_boto3_fun(
        credentials=credentials,
        region=region,
        service="kafka",
        function="list_clusters_v2",
        paginated_results_key=paginated_results_key,
    )
    for cluster in clusters.get(paginated_results_key, []):
        arn = cluster.get("ClusterArn")
        if provisioned := cluster.get("Provisioned"):
            logging_info = provisioned.get("LoggingInfo")
            if not logging_info:
                cluster["Provisioned"]["LoggingInfo"] = None
                locations = [
                    Location(
                        arn=arn,
                        access_patterns=("/Provisioned/LoggingInfo",),
                        values=(None,),
                        method=method,
                    ),
                ]
                vulns += build_vulnerabilities(
                    locations=locations,
                    method=method,
                    aws_response=cluster,
                )

    return (vulns, True)


CHECKS: AwsMethodsTuple = (
    is_trail_bucket_logging_disabled,
    elbv2_has_access_logging_disabled,
    cloudfront_has_logging_disabled,
    cloudtrail_trails_not_multiregion,
    s3_has_server_access_logging_disabled,
    redshift_has_audit_logging_disabled,
    vpcs_without_flowlog,
    redshift_is_user_activity_logging_disabled,
    cloudtrail_logging_disabled,
    aws_app_mesh_virtual_gateway_access_logging_disabled,
    aws_cloud_trail_delivery_failing,
    aws_config_referencing_missing_s3_bucket,
    aws_eks_cluster_logging_disabled,
    aws_beanstalk_persistent_logs,
    aws_opensearch_without_audit_logs,
    aws_mq_broker_logs_disabled,
    aws_route53_dns_query_logging_disabled,
    aws_document_db_without_audit_logs,
    aws_rds_db_cluster_logs_disabled,
    aws_rds_db_instance_logs_disabled,
    aws_global_accelerator_flow_logs_disabled,
    aws_neptune_db_instance_logs_disabled,
    aws_msk_cluster_logging_disabled,
)
