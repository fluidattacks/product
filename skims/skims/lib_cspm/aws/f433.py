from typing import (
    Any,
)

from lib_cspm.aws.model import (
    AwsMethodsTuple,
    Location,
)
from lib_cspm.aws.shield import (
    SHIELD,
)
from lib_cspm.aws.utils import (
    build_arn,
    build_vulnerabilities,
    get_owner_id,
    run_boto3_fun,
)
from lib_cspm.methods_enum import (
    MethodsEnumCSPM as MethodsEnum,
)
from model.core import (
    AwsCredentials,
    Vulnerabilities,
)


async def redshift_get_paginated_items(
    credentials: AwsCredentials,
    region: str,
) -> list:
    """Get all items in paginated API calls."""
    pools: list[dict] = []
    args: dict[str, Any] = {
        "credentials": credentials,
        "service": "redshift",
        "region": region,
        "function": "describe_clusters",
        "parameters": {"MaxRecords": 25},
    }
    data = await run_boto3_fun(**args)
    object_name = "Clusters"
    pools += data.get(object_name, [])

    next_token = data.get("Marker", None)
    while next_token:
        args["parameters"]["Marker"] = next_token
        data = await run_boto3_fun(**args)
        pools += data.get(object_name, [])
        next_token = data.get("Marker", None)

    return pools


@SHIELD
async def redshift_has_encryption_disabled(
    credentials: AwsCredentials,
    region: str,
) -> tuple[Vulnerabilities, bool]:
    clusters = await redshift_get_paginated_items(credentials, region)
    method = MethodsEnum.REDSHIFT_HAS_ENCRYPTION_DISABLED
    vulns: Vulnerabilities = ()
    for cluster in clusters:
        locations: list[Location] = []
        if not cluster["Encrypted"]:
            parameters = {
                "Region": region,
                "Account": await get_owner_id(credentials),
                "ClusterName": cluster["ClusterIdentifier"],
            }
            arn = build_arn(
                service="redshift",
                resource="cluster",
                parameters=parameters,
            )

            locations = [
                Location(
                    access_patterns=("/Encrypted",),
                    arn=(arn),
                    values=(cluster["Encrypted"],),
                    method=method,
                ),
            ]
            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                aws_response=cluster,
            )

    return (vulns, True)


CHECKS: AwsMethodsTuple = (redshift_has_encryption_disabled,)
