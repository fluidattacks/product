import ast
from collections.abc import (
    Iterable,
)
from contextlib import (
    suppress,
)
from typing import (
    Any,
)

from lib_cspm.aws.model import (
    AwsMethodsTuple,
    Location,
)
from lib_cspm.aws.shield import (
    SHIELD,
)
from lib_cspm.aws.utils import (
    build_arn,
    build_vulnerabilities,
    run_boto3_fun,
)
from lib_cspm.methods_enum import (
    MethodsEnumCSPM as MethodsEnum,
)
from model.core import (
    AwsCredentials,
    Vulnerabilities,
)


def iterate_s3_has_insecure_transport(
    bucket_statements: Iterable,
    arn: str,
    method: MethodsEnum,
) -> list[Location]:
    locations: list[Location] = []
    for index, stm in enumerate(bucket_statements):
        with suppress(KeyError):
            if (
                stm["Condition"]["Bool"]["aws:SecureTransport"] != "false"
                and stm["Effect"] == "Deny"
            ):
                locations.append(
                    Location(
                        access_patterns=((f"/{index}/Condition/Bool/aws:SecureTransport"),),
                        arn=(arn),
                        values=(
                            stm["Condition"]["Bool"]["aws:SecureTransport"],
                            stm["Effect"],
                        ),
                        method=method,
                    ),
                )
            elif (
                stm["Condition"]["Bool"]["aws:SecureTransport"] != "true"
                and stm["Effect"] == "Allow"
            ):
                locations.append(
                    Location(
                        access_patterns=(f"/{index}/Condition/Bool/aws:SecureTransport",),
                        arn=(arn),
                        values=(stm["Condition"]["Bool"]["aws:SecureTransport"],),
                        method=method,
                    ),
                )

    return locations


@SHIELD
async def s3_has_insecure_transport(
    credentials: AwsCredentials,
) -> tuple[Vulnerabilities, bool]:
    response: dict[str, Any] = await run_boto3_fun(
        credentials=credentials,
        service="s3",
        function="list_buckets",
    )
    buckets = response.get("Buckets", []) if response else []
    method = MethodsEnum.S3_HAS_INSECURE_TRANSPORT
    vulns: Vulnerabilities = ()

    for bucket in buckets:
        bucket_name = bucket["Name"]
        parameters = {
            "BucketName": bucket_name,
        }
        arn = build_arn(service="s3", resource="bucket", parameters=parameters)
        bucket_policy_string: dict[str, Any] = await run_boto3_fun(
            credentials=credentials,
            service="s3",
            function="get_bucket_policy",
            parameters={"Bucket": bucket_name},
        )
        policy = ast.literal_eval(str(bucket_policy_string.get("Policy", {})))
        bucket_statements = ast.literal_eval(str(policy.get("Statement", [])))
        if not isinstance(bucket_statements, list):
            bucket_statements = [bucket_statements]

        locations = iterate_s3_has_insecure_transport(bucket_statements, arn, method)

        vulns += build_vulnerabilities(
            locations=locations,
            method=method,
            aws_response=bucket_statements,
        )

    return (vulns, True)


CHECKS: AwsMethodsTuple = (s3_has_insecure_transport,)
