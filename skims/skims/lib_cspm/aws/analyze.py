from typing import TYPE_CHECKING

import ctx
from aioextensions import (
    resolve,
)
from lib_cspm.aws import (
    f005,
    f016,
    f024,
    f031,
    f073,
    f081,
    f099,
    f101,
    f109,
    f165,
    f177,
    f203,
    f246,
    f256,
    f257,
    f258,
    f259,
    f277,
    f281,
    f325,
    f333,
    f335,
    f363,
    f372,
    f392,
    f394,
    f396,
    f400,
    f406,
    f407,
    f433,
)
from lib_cspm.aws.model import (
    AwsMethodsTuple,
    BotoClientError,
)
from lib_cspm.aws.utils import (
    exclude_reports,
    get_arns_that_are_excluded,
    run_boto3_fun,
)
from model.core import (
    AwsCredentials,
    FindingEnum,
    Vulnerabilities,
    Vulnerability,
)
from utils.logs import (
    log_blocking,
    log_to_remote_blocking,
)
from utils.state import (
    VulnerabilitiesEphemeralStore,
)

if TYPE_CHECKING:
    from collections.abc import (
        Awaitable,
    )

AWS_CHECKS: dict[FindingEnum, AwsMethodsTuple] = {
    FindingEnum.F005: f005.CHECKS,
    FindingEnum.F016: f016.CHECKS,
    FindingEnum.F024: f024.CHECKS,
    FindingEnum.F031: f031.CHECKS,
    FindingEnum.F073: f073.CHECKS,
    FindingEnum.F081: f081.CHECKS,
    FindingEnum.F099: f099.CHECKS,
    FindingEnum.F101: f101.CHECKS,
    FindingEnum.F109: f109.CHECKS,
    FindingEnum.F165: f165.CHECKS,
    FindingEnum.F177: f177.CHECKS,
    FindingEnum.F203: f203.CHECKS,
    FindingEnum.F246: f246.CHECKS,
    FindingEnum.F256: f256.CHECKS,
    FindingEnum.F257: f257.CHECKS,
    FindingEnum.F258: f258.CHECKS,
    FindingEnum.F259: f259.CHECKS,
    FindingEnum.F277: f277.CHECKS,
    FindingEnum.F281: f281.CHECKS,
    FindingEnum.F325: f325.CHECKS,
    FindingEnum.F333: f333.CHECKS,
    FindingEnum.F335: f335.CHECKS,
    FindingEnum.F372: f372.CHECKS,
    FindingEnum.F363: f363.CHECKS,
    FindingEnum.F394: f394.CHECKS,
    FindingEnum.F392: f392.CHECKS,
    FindingEnum.F396: f396.CHECKS,
    FindingEnum.F406: f406.CHECKS,
    FindingEnum.F400: f400.CHECKS,
    FindingEnum.F407: f407.CHECKS,
    FindingEnum.F433: f433.CHECKS,
}

CHECKS_NO_REGIONS = (
    f005.allows_priv_escalation_by_policies_versions,
    f005.allows_priv_escalation_by_attach_policy,
    f016.serves_content_over_insecure_protocols,
    f031.users_with_password_and_access_keys,
    f031.admin_policy_attached,
    f031.aws_s3_log_delivery_write_access,
    f031.full_access_to_ssm,
    f031.negative_statement,
    f031.has_permissive_role_policies,
    f031.open_passrole,
    f081.root_without_mfa,
    f081.mfa_disabled_for_users_with_console_password,
    f099.bucket_policy_has_server_side_encryption_disable,
    f165.users_with_multiple_access_keys,
    f165.root_has_access_keys,
    f165.has_root_active_signing_certificates,
    f165.policies_attached_to_users,
    f203.acl_public_buckets,
    f203.s3_buckets_allow_unauthorized_public_access,
    f277.has_old_ssh_public_keys,
    f277.have_old_creds_enabled,
    f277.have_old_access_keys,
    f281.s3_has_insecure_transport,
    f325.iam_has_wildcard_resource_on_write_action,
    f325.iam_is_policy_miss_configured,
    f325.permissive_policy,
    f325.private_buckets_not_blocking_public_acls,
    f325.public_buckets,
    f325.group_with_permissive_inline_policies,
    f335.s3_bucket_versioning_disabled,
    f363.not_requires_uppercase,
    f363.not_requires_lowercase,
    f363.not_requires_symbols,
    f363.not_requires_numbers,
    f363.min_password_len_unsafe,
    f363.password_reuse_unsafe,
    f363.password_expiration_unsafe,
    f392.aws_cloudfront_is_not_protected_with_waf,
    f372.cft_serves_content_over_http,
    f372.aws_cloudfront_traffic_allows_http,
    f372.aws_cloudfront_distribution_viewer_policy_allows_http,
    f400.cloudfront_has_logging_disabled,
    f400.s3_has_server_access_logging_disabled,
)


async def get_active_aws_regions(credentials: AwsCredentials) -> list[str]:
    active_regions = ["us-east-1"]
    try:
        response = await run_boto3_fun(
            credentials=credentials,
            service="ec2",
            function="describe_regions",
        )
        if regions := response.get("Regions"):
            active_regions = [region["RegionName"] for region in regions]
    except BotoClientError:
        msg = "Unable to get active regions. CSPM analysis will be done on us-east-1"
        log_blocking("warning", msg)

    return active_regions


async def get_exclusions(
    credentials: AwsCredentials,
    active_regions: list[str],
) -> dict[str, list[str]]:
    excluded_arns: dict[str, list[str]] = {}
    try:
        log_blocking("info", "Getting exclusions from the environment")
        excluded_arns = await get_arns_that_are_excluded(
            credentials,
            active_regions,
        )
        log_blocking("info", "%s exclusions found", len(excluded_arns.keys()))
    except BotoClientError:
        msg = "Unable to get ARN exclusions due to boto client error"
        log_blocking("error", msg)
        log_to_remote_blocking(msg=msg, severity="error")
    return excluded_arns


def handle_vulnerabilities(
    vulnerabilities: tuple[Vulnerability, ...],
    stores: VulnerabilitiesEphemeralStore,
) -> None:
    stores.store_vulns(vulnerabilities)


async def analyze_cloud(
    credentials: AwsCredentials,
    stores: VulnerabilitiesEphemeralStore,
    finding_checks: set[FindingEnum],
) -> set[str]:
    active_regions = await get_active_aws_regions(credentials)

    methods_with_errors: set[str] = set()
    all_methods_vulns: list[Vulnerabilities] = []
    for finding, finding_methods in AWS_CHECKS.items():
        if finding not in finding_checks:
            continue

        log_blocking("info", "Running finding %s", finding.name)
        futures: list[Awaitable[tuple[Vulnerabilities, bool | str]]] = []
        for method in finding_methods:
            if method not in CHECKS_NO_REGIONS:
                futures += [
                    method(credentials, region)  # type: ignore[call-arg]
                    for region in active_regions
                ]
            else:
                futures.append(method(credentials))

        for aws_method in resolve(futures, workers=ctx.CPU_CORES):
            method_vulns, method_succeeded = await aws_method
            all_methods_vulns.append(method_vulns)
            if isinstance(method_succeeded, str):
                methods_with_errors.add(method_succeeded)

    excluded_arns = await get_exclusions(credentials, active_regions)
    for method_vulns in all_methods_vulns:
        filtered_reports = exclude_reports(method_vulns, excluded_arns)
        handle_vulnerabilities(filtered_reports, stores)

    return methods_with_errors


async def get_role(credentials: AwsCredentials) -> str:
    get_caller_identity = await run_boto3_fun(
        credentials=credentials,
        service="sts",
        function="get_caller_identity",
    )
    arn = get_caller_identity["Arn"]

    account_id = get_caller_identity["Account"]

    if "assumed-role" in arn:
        role_name = arn.split("/")[-2]
        role_arn = f"arn:aws:iam::{account_id}:role/{role_name}"
    else:
        role_arn = arn

    return role_arn


async def analyze(
    *,
    credentials: AwsCredentials,
    stores: VulnerabilitiesEphemeralStore,
) -> dict[str, list[str]]:
    if not any(finding in ctx.SKIMS_CONFIG.checks for finding in AWS_CHECKS):
        return {}

    log_blocking("info", "Starting CSPM/AWS analysis on credentials")

    role = await get_role(credentials)

    error_methods = await analyze_cloud(
        credentials,
        stores,
        ctx.SKIMS_CONFIG.checks,
    )
    auth_problems = []
    other_problems = []

    for error in error_methods:
        if error.startswith("AccessDenied"):
            auth_problems.append(f"{role} {error}")
        else:
            other_problems.append(error)

    aws_errors = {
        "aws_errors": other_problems,
        "aws_permissions_problems": auth_problems,
    }

    log_blocking("info", "CSPM/AWS analysis completed!")

    return aws_errors
