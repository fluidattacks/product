from typing import (
    Any,
)

from lib_cspm.aws.model import (
    AwsMethodsTuple,
    Location,
)
from lib_cspm.aws.shield import (
    SHIELD,
)
from lib_cspm.aws.utils import (
    build_vulnerabilities,
    run_boto3_fun,
)
from lib_cspm.methods_enum import (
    MethodsEnumCSPM as MethodsEnum,
)
from model.core import (
    AwsCredentials,
    Vulnerabilities,
)


@SHIELD
async def rds_has_unencrypted_storage(
    credentials: AwsCredentials,
    region: str,
) -> tuple[Vulnerabilities, bool]:
    paginated_results_key = "DBInstances"
    response: dict[str, Any] = await run_boto3_fun(
        region=region,
        credentials=credentials,
        service="rds",
        function="describe_db_instances",
        paginated_results_key=paginated_results_key,
    )
    db_instances = response.get(paginated_results_key, []) if response else []
    method = MethodsEnum.RDS_HAS_UNENCRYPTED_STORAGE
    vulns: Vulnerabilities = ()

    for instance in db_instances:
        if not instance.get("StorageEncrypted", False):
            instance_arn = instance["DBInstanceArn"]
            locations = [
                Location(
                    access_patterns=("/StorageEncrypted",),
                    arn=(f"{instance_arn}"),
                    values=(instance.get("StorageEncrypted", False),),
                    method=method,
                ),
            ]
            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                aws_response=instance,
            )

    return (vulns, True)


CHECKS: AwsMethodsTuple = (rds_has_unencrypted_storage,)
