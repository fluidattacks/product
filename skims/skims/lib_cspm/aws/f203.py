import ast
from typing import (
    Any,
)

from lib_cspm.aws.model import (
    AwsMethodsTuple,
    Location,
)
from lib_cspm.aws.shield import (
    SHIELD,
)
from lib_cspm.aws.utils import (
    build_arn,
    build_vulnerabilities,
    run_boto3_fun,
)
from lib_cspm.methods_enum import (
    MethodsEnumCSPM as MethodsEnum,
)
from model.core import (
    AwsCredentials,
    Vulnerabilities,
)
from utils.aws.iam import (
    ACTIONS,
)


@SHIELD
async def acl_public_buckets(
    credentials: AwsCredentials,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.ACL_PUBLIC_BUCKETS
    vulns: Vulnerabilities = ()
    response: dict[str, Any] = await run_boto3_fun(
        credentials=credentials,
        service="s3",
        function="list_buckets",
    )
    buckets = response.get("Buckets", []) if response else []
    if buckets:
        public_acl = "http://acs.amazonaws.com/groups/global/AllUsers"
        danger_perms = ["WRITE", "FULL_CONTROL", "WRITE_ACP"]
        for bucket in buckets:
            locations: list[Location] = []
            bucket_name = bucket["Name"]

            bucket_grants: dict[str, Any] = await run_boto3_fun(
                credentials=credentials,
                service="s3",
                function="get_bucket_acl",
                parameters={"Bucket": str(bucket_name)},
            )

            grants = bucket_grants.get("Grants", [])
            for index, grant in enumerate(grants):
                if (
                    grant["Permission"] in danger_perms
                    and grant["Grantee"].get("URI", "") == public_acl
                ):
                    parameters = {
                        "BucketName": bucket["Name"],
                    }
                    locations.append(
                        Location(
                            access_patterns=(f"/Grants/{index}/Permission",),
                            arn=(
                                build_arn(
                                    service="s3",
                                    resource="bucket",
                                    parameters=parameters,
                                )
                            ),
                            values=(grant["Permission"],),
                            method=method,
                        ),
                    )

            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                aws_response=bucket_grants,
            )

    return (vulns, True)


async def iterate_s3_buckets_allow_unauthorized_public_access(
    policies_statements: list,
    arn: str,
) -> Vulnerabilities:
    vulns: Vulnerabilities = ()
    method = MethodsEnum.S3_BUCKETS_ALLOW_UNAUTHORIZED_PUBLIC_ACCESS
    for policy in policies_statements:
        locations: list[Location] = []
        if not (
            (effect := policy.get("Effect", {}))
            and (principal := policy.get("Principal", {}))
            and (actions := policy.get("Action", {}))
        ):
            continue
        actions = (
            actions
            if isinstance(actions, list)
            else [
                actions,
            ]
        )
        if (
            effect == "Allow"
            and ("*" in principal.values() if isinstance(principal, dict) else principal == "*")
            and (
                any(action.split(":")[-1] not in ACTIONS["s3"]["read"] for action in actions)
                or "s3:*" in actions
            )
        ):
            locations.append(
                Location(
                    access_patterns=(
                        "/Effect",
                        "/Principal",
                    ),
                    arn=(arn),
                    values=(
                        effect,
                        principal,
                    ),
                    method=method,
                ),
            )

        vulns += build_vulnerabilities(
            locations=locations,
            method=method,
            aws_response=policy,
        )

    return vulns


@SHIELD
async def s3_buckets_allow_unauthorized_public_access(
    credentials: AwsCredentials,
) -> tuple[Vulnerabilities, bool]:
    response: dict[str, Any] = await run_boto3_fun(
        credentials=credentials,
        service="s3",
        function="list_buckets",
    )
    buckets = response.get("Buckets", []) if response else []
    vulns: Vulnerabilities = ()
    if buckets:
        for bucket in buckets:
            bucket_name = bucket["Name"]
            parameters = {
                "BucketName": bucket["Name"],
            }
            arn = build_arn(service="s3", resource="bucket", parameters=parameters)
            bucket_policy: dict[str, Any] = await run_boto3_fun(
                credentials=credentials,
                service="s3",
                function="get_bucket_policy",
                parameters={"Bucket": str(bucket_name)},
            )

            if bucket_policy:
                bucket_policies = ast.literal_eval(str(bucket_policy.get("Policy", [])))

                vulns += await iterate_s3_buckets_allow_unauthorized_public_access(
                    bucket_policies["Statement"],
                    arn,
                )

    return (vulns, True)


CHECKS: AwsMethodsTuple = (
    acl_public_buckets,
    s3_buckets_allow_unauthorized_public_access,
)
