from typing import (
    Any,
)

from lib_cspm.aws.model import (
    AwsMethodsTuple,
    Location,
)
from lib_cspm.aws.shield import (
    SHIELD,
)
from lib_cspm.aws.utils import (
    build_arn,
    build_vulnerabilities,
    get_owner_id,
    run_boto3_fun,
)
from lib_cspm.methods_enum import (
    MethodsEnumCSPM as MethodsEnum,
)
from model.core import (
    AwsCredentials,
    Vulnerabilities,
)


def _get_vuln_db_instances(
    response: dict[str, Any],
) -> Vulnerabilities:
    db_instances = response.get("DBInstances", []) if response else []
    vulns: Vulnerabilities = ()
    method = MethodsEnum.INSTANCES_IS_NOT_INSIDE_A_DB_SUBNET_GROUP
    for instance in db_instances:
        instance_arn = instance["DBInstanceArn"]
        if not instance.get("DBSubnetGroup", {}):
            locations = [
                Location(
                    access_patterns=(),
                    arn=(f"{instance_arn}"),
                    values=(),
                    method=method,
                ),
            ]
            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                aws_response=instance,
            )
    return vulns


def _get_vulns_db_clusters(
    response: dict[str, Any],
) -> Vulnerabilities:
    db_clusters = response.get("DBClusters", []) if response else []
    vulns: Vulnerabilities = ()
    method = MethodsEnum.CLUSTERS_IS_NOT_INSIDE_A_DB_SUBNET_GROUP
    for clusters in db_clusters:
        cluster_arn = clusters["DBClusterArn"]
        if not clusters.get("DBSubnetGroup", {}):
            locations = [
                Location(
                    access_patterns=(),
                    arn=(f"{cluster_arn}"),
                    values=(),
                    method=method,
                ),
            ]
            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                aws_response=clusters,
            )
    return vulns


@SHIELD
async def instances_is_not_inside_a_db_subnet_group(
    credentials: AwsCredentials,
    region: str,
) -> tuple[Vulnerabilities, bool]:
    vulns: Vulnerabilities = ()
    describe_db_instances: dict[str, Any] = await run_boto3_fun(
        region=region,
        credentials=credentials,
        service="rds",
        function="describe_db_instances",
        paginated_results_key="DBInstances",
    )

    vulns = _get_vuln_db_instances(describe_db_instances)

    return (vulns, True)


@SHIELD
async def clusters_is_not_inside_a_db_subnet_group(
    credentials: AwsCredentials,
    region: str,
) -> tuple[Vulnerabilities, bool]:
    vulns: Vulnerabilities = ()

    describe_db_clusters: dict[str, Any] = await run_boto3_fun(
        credentials=credentials,
        service="rds",
        function="describe_db_clusters",
        region=region,
        paginated_results_key="DBClusters",
    )
    vulns = _get_vulns_db_clusters(describe_db_clusters)

    return (vulns, True)


@SHIELD
async def rds_unrestricted_db_security_groups(
    credentials: AwsCredentials,
    region: str,
) -> tuple[Vulnerabilities, bool]:
    vulns: Vulnerabilities = ()
    method = MethodsEnum.RDS_UNRESTRICTED_DB_SECURITY_GROUPS
    describe_db_instances: dict[str, Any] = await run_boto3_fun(
        region=region,
        credentials=credentials,
        service="rds",
        function="describe_db_instances",
        paginated_results_key="DBInstances",
    )
    db_instances = describe_db_instances.get("DBInstances", []) if describe_db_instances else []
    for instance in db_instances:
        security_groups_ids = [
            sec_group["VpcSecurityGroupId"] for sec_group in instance["VpcSecurityGroups"]
        ]
        describe_security_groups: dict[str, Any] = await run_boto3_fun(
            region=region,
            credentials=credentials,
            service="ec2",
            function="describe_security_groups",
            parameters={"GroupIds": security_groups_ids},
        )
        security_groups = describe_security_groups.get("SecurityGroups", [])
        for group in security_groups:
            owner_id = await get_owner_id(credentials=credentials)
            parameters = {
                "Region": region,
                "Account": owner_id,
                "SecurityGroupId": group["GroupId"],
            }

            for ip_permission in group["IpPermissions"]:
                locations = [
                    Location(
                        access_patterns=(f"/IpRanges/{index}/CidrIp",),
                        arn=(
                            build_arn(
                                service="ec2",
                                resource="security-group",
                                parameters=parameters,
                            )
                        ),
                        values=(ip_range["CidrIp"],),
                        method=method,
                    )
                    for index, ip_range in enumerate(ip_permission["IpRanges"])
                    if ip_range["CidrIp"] == "0.0.0.0/0"
                ]

                vulns += build_vulnerabilities(
                    locations=locations,
                    method=method,
                    aws_response=ip_permission,
                )

    return (vulns, True)


@SHIELD
async def rds_unrestricted_cluster_security_groups(
    credentials: AwsCredentials,
    region: str,
) -> tuple[Vulnerabilities, bool]:
    vulns: Vulnerabilities = ()
    method = MethodsEnum.RDS_UNRESTRICTED_CLUSTER_SECURITY_GROUPS
    describe_db_clusters: dict[str, Any] = await run_boto3_fun(
        region=region,
        credentials=credentials,
        service="rds",
        function="describe_db_clusters",
        paginated_results_key="DBClusters",
    )
    db_clusters = describe_db_clusters.get("DBClusters", [])
    for cluster in db_clusters:
        security_groups_ids = [
            sec_group["VpcSecurityGroupId"] for sec_group in cluster["VpcSecurityGroups"]
        ]
        describe_security_groups: dict[str, Any] = await run_boto3_fun(
            region=region,
            credentials=credentials,
            service="ec2",
            function="describe_security_groups",
            parameters={"GroupIds": security_groups_ids},
        )
        security_groups = describe_security_groups.get("SecurityGroups", [])
        for group in security_groups:
            owner_id = await get_owner_id(credentials=credentials)
            parameters = {
                "Region": region,
                "Account": owner_id,
                "SecurityGroupId": group["GroupId"],
            }
            for ip_permission in group["IpPermissions"]:
                locations = [
                    Location(
                        access_patterns=(f"/IpRanges/{index}/CidrIp",),
                        arn=(
                            build_arn(
                                service="ec2",
                                resource="security-group",
                                parameters=parameters,
                            )
                        ),
                        values=(ip_range["CidrIp"],),
                        method=method,
                        desc_params={"cluster": cluster["DBClusterIdentifier"]},
                    )
                    for index, ip_range in enumerate(ip_permission["IpRanges"])
                    if ip_range["CidrIp"] == "0.0.0.0/0"
                ]

                vulns += build_vulnerabilities(
                    locations=locations,
                    method=method,
                    aws_response=ip_permission,
                )

    return (vulns, True)


CHECKS: AwsMethodsTuple = (
    instances_is_not_inside_a_db_subnet_group,
    clusters_is_not_inside_a_db_subnet_group,
    rds_unrestricted_db_security_groups,
    rds_unrestricted_cluster_security_groups,
)
