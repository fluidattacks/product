from typing import (
    Any,
)

from lib_cspm.aws.model import (
    AwsMethodsTuple,
    Location,
)
from lib_cspm.aws.shield import (
    SHIELD,
)
from lib_cspm.aws.utils import (
    build_vulnerabilities,
    run_boto3_fun,
)
from lib_cspm.methods_enum import (
    MethodsEnumCSPM as MethodsEnum,
)
from model.core import (
    AwsCredentials,
    Vulnerabilities,
)


@SHIELD
async def cloudtrail_files_not_validated(
    credentials: AwsCredentials,
    region: str,
) -> tuple[Vulnerabilities, bool]:
    response: dict[str, Any] = await run_boto3_fun(
        region=region,
        credentials=credentials,
        service="cloudtrail",
        function="describe_trails",
        parameters={"includeShadowTrails": False},
    )
    method = MethodsEnum.CLOUDTRAIL_FILES_NOT_VALIDATED
    trails = response.get("trailList", []) if response else []
    vulns: Vulnerabilities = ()
    for trail in trails:
        if not trail["LogFileValidationEnabled"]:
            locations = [
                Location(
                    arn=trail["TrailARN"],
                    method=method,
                    values=(trail["LogFileValidationEnabled"],),
                    access_patterns=("/LogFileValidationEnabled",),
                ),
            ]
            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                aws_response=trail,
            )

    return (vulns, True)


CHECKS: AwsMethodsTuple = (cloudtrail_files_not_validated,)
