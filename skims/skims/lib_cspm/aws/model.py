from collections.abc import (
    Callable,
    Coroutine,
)
from typing import (
    Any,
    NamedTuple,
)

from lib_cspm.methods_enum import (
    MethodsEnumCSPM as MethodsEnum,
)
from model.core import (
    AwsCredentials,
    Vulnerabilities,
)

AwsMethod = Callable[[AwsCredentials], Coroutine[Any, Any, tuple[Vulnerabilities, bool | str]]]

AwsMethodsTuple = tuple[AwsMethod, ...]


class Location(NamedTuple):
    arn: str
    access_patterns: tuple[str, ...]
    method: MethodsEnum
    values: tuple[Any, ...]
    desc_params: dict[str, str] = {}  # noqa: RUF012
    skip: bool = False


class BotoClientError(Exception):
    pass
