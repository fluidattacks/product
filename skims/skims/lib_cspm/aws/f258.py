from typing import (
    Any,
)

from lib_cspm.aws.model import (
    AwsMethodsTuple,
    Location,
)
from lib_cspm.aws.shield import (
    SHIELD,
)
from lib_cspm.aws.utils import (
    build_vulnerabilities,
    run_boto3_fun,
)
from lib_cspm.methods_enum import (
    MethodsEnumCSPM as MethodsEnum,
)
from model.core import (
    AwsCredentials,
    Vulnerabilities,
)


def check_is_eks(load_balancer_tags: dict[str, Any], eks_cluster_names: list) -> bool:
    elb_tags = load_balancer_tags.get("TagDescriptions", [])[0]
    tags = elb_tags.get("Tags", {})
    for tag in tags:
        if (
            tag.get("Key", "") == "elbv2.k8s.aws/cluster"
            and tag.get("Value", "") in eks_cluster_names
        ):
            return True
    return False


async def list_eks_cluster_names(credentials: AwsCredentials, region: str) -> list[str]:
    paginated_results_key = "clusters"
    clusters_names: dict[str, Any] = await run_boto3_fun(
        region=region,
        credentials=credentials,
        service="eks",
        function="list_clusters",
        paginated_results_key=paginated_results_key,
    )
    return clusters_names.get(paginated_results_key, [])


@SHIELD
async def elb2_has_not_deletion_protection(
    credentials: AwsCredentials,
    region: str,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.ELB2_HAS_NOT_DELETION_PROTECTION
    vulns: Vulnerabilities = ()
    paginated_results_key = "LoadBalancers"
    response: dict[str, Any] = await run_boto3_fun(
        region=region,
        credentials=credentials,
        service="elbv2",
        function="describe_load_balancers",
        paginated_results_key=paginated_results_key,
    )
    for balancer in response.get(paginated_results_key, []):
        load_balancer_arn = balancer["LoadBalancerArn"]
        load_balancer_tags: dict[str, Any] = await run_boto3_fun(
            region=region,
            credentials=credentials,
            service="elbv2",
            function="describe_tags",
            parameters={
                "ResourceArns": [load_balancer_arn],
            },
        )
        eks_cluster_names = await list_eks_cluster_names(credentials, region)
        is_eks = check_is_eks(load_balancer_tags, eks_cluster_names)
        if not is_eks:
            attributes: dict[str, Any] = await run_boto3_fun(
                region=region,
                credentials=credentials,
                service="elbv2",
                function="describe_load_balancer_attributes",
                parameters={
                    "LoadBalancerArn": load_balancer_arn,
                },
            )
            for index, attr in enumerate(attributes.get("Attributes", [])):
                if attr["Key"] == "deletion_protection.enabled" and attr["Value"] != "true":
                    locations = [
                        Location(
                            arn=(load_balancer_arn),
                            access_patterns=(f"/Attributes/{index}/Value",),
                            values=(attr["Value"],),
                            method=method,
                        ),
                    ]
                    vulns += build_vulnerabilities(
                        locations=locations,
                        method=method,
                        aws_response=attributes,
                    )
    return (vulns, True)


CHECKS: AwsMethodsTuple = (elb2_has_not_deletion_protection,)
