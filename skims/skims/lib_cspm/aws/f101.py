from typing import (
    Any,
)

from lib_cspm.aws.model import (
    Location,
)
from lib_cspm.aws.shield import (
    SHIELD,
)
from lib_cspm.aws.utils import (
    build_vulnerabilities,
    run_boto3_fun,
)
from lib_cspm.methods_enum import (
    MethodsEnumCSPM as MethodsEnum,
)
from model.core import (
    AwsCredentials,
    Vulnerabilities,
)


@SHIELD
async def aws_rds_instance_backup_retention_period(
    credentials: AwsCredentials,
    region: str,
) -> tuple[Vulnerabilities, bool]:
    vulns: Vulnerabilities = ()
    method = MethodsEnum.AWS_RDS_INSTANCE_BACKUP_RETENTION_PERIOD
    paginated_results_key = "DBInstances"
    instances: dict[str, Any] = await run_boto3_fun(
        credentials=credentials,
        region=region,
        service="rds",
        function="describe_db_instances",
        paginated_results_key=paginated_results_key,
    )
    for instance in instances.get(paginated_results_key, []):
        backup_retention_period = instance["BackupRetentionPeriod"]
        arn = instance["DBInstanceArn"]
        if backup_retention_period < 7:
            locations = [
                Location(
                    arn=arn,
                    access_patterns=("/BackupRetentionPeriod",),
                    values=(backup_retention_period,),
                    method=method,
                ),
            ]
            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                aws_response=instance,
            )

    return (vulns, True)


@SHIELD
async def aws_rds_cluster_backup_retention_period(
    credentials: AwsCredentials,
    region: str,
) -> tuple[Vulnerabilities, bool]:
    vulns: Vulnerabilities = ()
    method = MethodsEnum.AWS_RDS_CLUSTER_BACKUP_RETENTION_PERIOD
    paginated_results_key = "DBClusters"
    clusters: dict[str, Any] = await run_boto3_fun(
        credentials=credentials,
        region=region,
        service="rds",
        function="describe_db_clusters",
        paginated_results_key=paginated_results_key,
    )
    for cluster in clusters.get(paginated_results_key, []):
        backup_retention_period = cluster["BackupRetentionPeriod"]
        arn = cluster["DBClusterArn"]
        if backup_retention_period < 7:
            # Remove random values to avoid breaking Moto test
            cluster.pop("Endpoint", None)
            cluster.pop("ReaderEndpoint", None)
            locations = [
                Location(
                    arn=arn,
                    access_patterns=("/BackupRetentionPeriod",),
                    values=(backup_retention_period,),
                    method=method,
                ),
            ]
            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                aws_response=cluster,
            )

    return (vulns, True)


@SHIELD
async def aws_elasticache_replication_group_wo_auto_backups(
    credentials: AwsCredentials,
    region: str,
) -> tuple[Vulnerabilities, bool]:
    vulns: Vulnerabilities = ()
    method = MethodsEnum.AWS_ELASTICACHE_REPLICATION_GROUP_WO_AUTO_BACKUPS
    paginated_results_key = "ReplicationGroups"
    replication_groups: dict[str, Any] = await run_boto3_fun(
        credentials=credentials,
        region=region,
        service="elasticache",
        function="describe_replication_groups",
        paginated_results_key=paginated_results_key,
    )
    for replication_group in replication_groups.get(paginated_results_key, []):
        arn = replication_group["ARN"]
        snapshot_retention_limit = replication_group["SnapshotRetentionLimit"]
        if snapshot_retention_limit == 0:
            locations = [
                Location(
                    arn=arn,
                    access_patterns=("/SnapshotRetentionLimit",),
                    values=(snapshot_retention_limit,),
                    method=method,
                ),
            ]
            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                aws_response=replication_group,
            )
    return (vulns, True)


@SHIELD
async def aws_elasticache_replication_backup_retention_period(
    credentials: AwsCredentials,
    region: str,
) -> tuple[Vulnerabilities, bool]:
    vulns: Vulnerabilities = ()
    method = MethodsEnum.AWS_ELASTICACHE_REPLICATION_BACKUP_RETENTION_PERIOD
    paginated_results_key = "ReplicationGroups"
    replication_groups: dict[str, Any] = await run_boto3_fun(
        credentials=credentials,
        region=region,
        service="elasticache",
        function="describe_replication_groups",
        paginated_results_key=paginated_results_key,
    )
    for replication_group in replication_groups.get(paginated_results_key, []):
        arn = replication_group["ARN"]
        snapshot_retention_limit = replication_group.get("SnapshotRetentionLimit")
        if snapshot_retention_limit and snapshot_retention_limit < 7:
            locations = [
                Location(
                    arn=arn,
                    access_patterns=("/SnapshotRetentionLimit",),
                    values=(snapshot_retention_limit,),
                    method=method,
                ),
            ]
            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                aws_response=replication_group,
            )
    return (vulns, True)


CHECKS = (
    aws_rds_instance_backup_retention_period,
    aws_rds_cluster_backup_retention_period,
    aws_elasticache_replication_group_wo_auto_backups,
    aws_elasticache_replication_backup_retention_period,
)
