from contextlib import (
    suppress,
)
from typing import (
    Any,
)

from lib_cspm.aws.model import (
    AwsMethodsTuple,
    Location,
)
from lib_cspm.aws.shield import (
    SHIELD,
)
from lib_cspm.aws.utils import (
    build_arn,
    build_vulnerabilities,
    run_boto3_fun,
)
from lib_cspm.methods_enum import (
    MethodsEnumCSPM as MethodsEnum,
)
from model.core import (
    AwsCredentials,
    Vulnerabilities,
)

ADMIN_PORTS = {
    22,  # SSH
    1521,  # Oracle
    2438,  # Oracle
    2484,  # Oracle
    3306,  # MySQL
    3389,  # RDP
    5432,  # Postgres
    6379,  # Redis
    7199,  # Cassandra
    7001,  # Cassandra
    8111,  # DAX
    8888,  # Cassandra
    9160,  # Cassandra
    61621,  # Cassandra
    11211,  # Memcached
    27017,  # MongoDB
    27018,  # MongoDB
    27019,  # MongoDB
    445,  # CIFS
    3020,  # CIFS/SMB
    389,  # LDAP
    636,  # LDAP SSL
    11214,  # Memcached SSL
    11215,  # Memcached SSL
    135,  # MSSQL
    137,  # NetBIOS
    138,  # NetBIOS
    139,  # NetBIOS
    1433,  # MSSQL
    1434,  # MSSQL
    23,  # Telnet
    2379,  # etcd
    2382,  # SQL Server
    2383,  # SQL Server
    3000,  # Ruby on Rails
    4505,  # SaltStack
    4506,  # SaltStack
    5500,  # Virtual Network Computing
    5800,  # Virtual Network Computing
    5900,  # Virtual Network Computing
    8000,  # HTTP
    9200,  # ElasticSearch
    9300,  # ElasticSearch
}


async def get_security_groups(credentials: AwsCredentials, region: str) -> list[dict[str, Any]]:
    paginated_results_key = "SecurityGroups"
    response: dict[str, Any] = await run_boto3_fun(
        region=region,
        credentials=credentials,
        service="ec2",
        function="describe_security_groups",
        paginated_results_key=paginated_results_key,
    )
    return response.get(paginated_results_key, []) if response else []


def _get_insecure_locations_for_permission(
    perm: dict[str, Any],
    index: int,
    region: str,
    group: dict[str, Any],
    method: MethodsEnum,
) -> list[Location]:
    locations = []
    for index_ip_range, ip_range in enumerate(perm["IpRanges"]):
        if ip_range["CidrIp"] == "0.0.0.0/0":
            parameters = {
                "Region": region,
                "Account": group["OwnerId"],
                "SecurityGroupId": group["GroupId"],
            }
            locations.extend(
                [
                    Location(
                        arn=build_arn(
                            service="ec2",
                            resource="security-group",
                            parameters=parameters,
                        ),
                        access_patterns=(
                            f"/IpPermissions/{index}/FromPort",
                            f"/IpPermissions/{index}/ToPort",
                            (f"/IpPermissions/{index}/IpRanges/{index_ip_range}/CidrIp"),
                        ),
                        values=(
                            perm["FromPort"],
                            perm["ToPort"],
                            ip_range["CidrIp"],
                        ),
                        method=method,
                        desc_params={"port": str(port)},
                    )
                    for port in ADMIN_PORTS
                    if perm["FromPort"] <= port <= perm["ToPort"]
                ],
            )

    for index_ip_range, ip_range in enumerate(perm["Ipv6Ranges"]):
        if ip_range["CidrIpv6"] == "::/0":
            parameters = {
                "Region": region,
                "Account": group["OwnerId"],
                "SecurityGroupId": group["GroupId"],
            }
            locations.extend(
                [
                    Location(
                        arn=build_arn(
                            service="ec2",
                            resource="security-group",
                            parameters=parameters,
                        ),
                        access_patterns=(
                            f"/IpPermissions/{index}/FromPort",
                            f"/IpPermissions/{index}/ToPort",
                            (f"/IpPermissions/{index}/Ipv6Ranges/{index_ip_range}/CidrIpv6"),
                        ),
                        values=(
                            perm["FromPort"],
                            perm["ToPort"],
                            ip_range["CidrIpv6"],
                        ),
                        method=method,
                        desc_params={"port": str(port)},
                    )
                    for port in ADMIN_PORTS
                    if perm["FromPort"] <= port <= perm["ToPort"]
                ],
            )
    return locations


@SHIELD
async def allows_anyone_to_admin_ports(
    credentials: AwsCredentials,
    region: str,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.ALLOWS_ANYONE_TO_ADMIN_PORTS
    security_groups = await get_security_groups(credentials, region)
    vulns: Vulnerabilities = ()
    for group in security_groups:
        locations: list[Location] = []
        for index, perm in enumerate(group["IpPermissions"]):
            if "FromPort" not in perm and "ToPort" not in perm:
                continue

            locations.extend(
                _get_insecure_locations_for_permission(
                    perm,
                    index,
                    region,
                    group,
                    method,
                ),
            )

        vulns += build_vulnerabilities(
            locations=locations,
            method=method,
            aws_response=group,
        )

    return (vulns, True)


@SHIELD
async def unrestricted_cidrs(
    credentials: AwsCredentials,
    region: str,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.UNRESTRICTED_CIDRS
    security_groups = await get_security_groups(credentials, region)
    vulns: Vulnerabilities = ()
    for group in security_groups:
        parameters = {
            "Region": region,
            "Account": group["OwnerId"],
            "SecurityGroupId": group["GroupId"],
        }
        group_arn = build_arn(service="ec2", resource="security-group", parameters=parameters)
        locations: list[Location] = []
        for index_ip, ip_permission in enumerate(group["IpPermissions"]):
            locations += [
                Location(
                    access_patterns=((f"/IpPermissions/{index_ip}/IpRanges/{index_range}/CidrIp"),),
                    arn=group_arn,
                    values=(ip_range["CidrIp"],),
                    method=method,
                )
                for index_range, ip_range in enumerate(ip_permission["IpRanges"])
                if (ip_range.get("CidrIp") == "0.0.0.0/0")
            ] + [
                Location(
                    access_patterns=(
                        (f"/IpPermissions/{index_ip}/Ipv6Ranges/{index_range}/CidrIpv6"),
                    ),
                    arn=group_arn,
                    values=(ip_range["CidrIpv6"],),
                    method=method,
                )
                for index_range, ip_range in enumerate(ip_permission["Ipv6Ranges"])
                if (ip_range.get("CidrIpv6") == "::/0")
            ]

        vulns += build_vulnerabilities(
            locations=locations,
            method=method,
            aws_response=group,
        )

    return (vulns, True)


@SHIELD
async def unrestricted_ip_protocols(
    credentials: AwsCredentials,
    region: str,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.UNRESTRICTED_IP_PROTOCOLS
    security_groups = await get_security_groups(credentials, region)
    vulns: Vulnerabilities = ()
    if security_groups:
        for group in security_groups:
            parameters = {
                "Region": region,
                "Account": group["OwnerId"],
                "SecurityGroupId": group["GroupId"],
            }
            locations = [
                Location(
                    access_patterns=(
                        f"/IpPermissions/{index_ip}/FromPort",
                        f"/IpPermissions/{index_ip}/ToPort",
                        f"/IpPermissions/{index_ip}/IpProtocol",
                    ),
                    arn=build_arn(
                        service="ec2",
                        resource="security-group",
                        parameters=parameters,
                    ),
                    values=(
                        ip_permission.get("FromPort"),
                        ip_permission.get("ToPort"),
                        ip_permission.get("IpProtocol"),
                    ),
                    method=method,
                )
                for index_ip, ip_permission in enumerate(group["IpPermissions"])
                if ip_permission["IpProtocol"] in ("-1", -1)
            ]
            locations.extend(
                [
                    Location(
                        access_patterns=(f"/IpPermissionsEgress/{index_ip}/IpProtocol",),
                        arn=build_arn(
                            service="ec2",
                            resource="security-group",
                            parameters=parameters,
                        ),
                        values=(ip_permission.get("IpProtocol"),),
                        method=method,
                    )
                    for index_ip, ip_permission in enumerate(group["IpPermissionsEgress"])
                    if (ip_permission["IpProtocol"] in ("-1", -1))
                ],
            )
            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                aws_response=group,
            )
    return (vulns, True)


@SHIELD
async def security_groups_ip_ranges_in_rfc1918(
    credentials: AwsCredentials,
    region: str,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.SECURITY_GROUPS_IP_RANGES_IN_RFC1918
    rfc1918 = {"10.0.0.0/8", "172.16.0.0/12", "192.168.0.0/16"}
    security_groups = await get_security_groups(credentials, region)
    vulns: Vulnerabilities = ()
    if security_groups:
        for group in security_groups:
            parameters = {
                "Region": region,
                "Account": group["OwnerId"],
                "SecurityGroupId": group["GroupId"],
            }
            locations = [
                Location(
                    access_patterns=((f"/IpPermissions/{index}/IpRanges/{index_ip_range}/CidrIp"),),
                    arn=build_arn(
                        service="ec2",
                        resource="security-group",
                        parameters=parameters,
                    ),
                    values=(ip_range["CidrIp"],),
                    method=method,
                )
                for index, ip_permission in enumerate(group["IpPermissions"])
                for index_ip_range, ip_range in enumerate(ip_permission["IpRanges"])
                if ip_range["CidrIp"] in rfc1918
            ]
            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                aws_response=group,
            )
    return (vulns, True)


@SHIELD
async def unrestricted_dns_access(
    credentials: AwsCredentials,
    region: str,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.UNRESTRICTED_DNS_ACCESS
    security_groups = await get_security_groups(credentials, region)
    vulns: Vulnerabilities = ()
    if security_groups:
        for group in security_groups:
            locations: list[Location] = []
            for index, ip_permission in enumerate(group["IpPermissions"]):
                with suppress(KeyError):
                    if ip_permission["FromPort"] <= 53 <= ip_permission["ToPort"] and ip_permission[
                        "IpProtocol"
                    ] in {"tcp", "udp"}:
                        parameters = {
                            "Region": region,
                            "Account": group["OwnerId"],
                            "SecurityGroupId": group["GroupId"],
                        }
                        locations = [
                            Location(
                                access_patterns=(
                                    f"/IpPermissions/{index}/FromPort",
                                    f"/IpPermissions/{index}/ToPort",
                                    (f"/IpPermissions/{index}/IpRanges/{index_ip_range}/CidrIp"),
                                ),
                                arn=build_arn(
                                    service="ec2",
                                    resource="security-group",
                                    parameters=parameters,
                                ),
                                values=(
                                    ip_permission["FromPort"],
                                    ip_permission["ToPort"],
                                    ip_range["CidrIp"],
                                ),
                                method=method,
                            )
                            for index_ip_range, ip_range in enumerate(ip_permission["IpRanges"])
                            if ip_range["CidrIp"] == "0.0.0.0/0"
                        ]
                        locations.extend(
                            [
                                Location(
                                    access_patterns=(
                                        f"/IpPermissions/{index}/FromPort",
                                        f"/IpPermissions/{index}/ToPort",
                                        (
                                            f"/IpPermissions/{index}/"
                                            "Ipv6Ranges"
                                            f"/{index_ip_range}/CidrIpv6"
                                        ),
                                    ),
                                    arn=build_arn(
                                        service="ec2",
                                        resource="security-group",
                                        parameters=parameters,
                                    ),
                                    values=(
                                        ip_permission["FromPort"],
                                        ip_permission["ToPort"],
                                        ip_range["CidrIpv6"],
                                    ),
                                    method=method,
                                )
                                for index_ip_range, ip_range in enumerate(
                                    ip_permission["Ipv6Ranges"],
                                )
                                if ip_range["CidrIpv6"] == "::/0"
                            ],
                        )
            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                aws_response=group,
            )
    return (vulns, True)


@SHIELD
async def unrestricted_ftp_access(
    credentials: AwsCredentials,
    region: str,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.UNRESTRICTED_FTP_ACCESS
    security_groups = await get_security_groups(credentials, region)
    vulns: Vulnerabilities = ()
    if security_groups:
        for group in security_groups:
            locations: list[Location] = []
            for index, ip_permission in enumerate(group["IpPermissions"]):
                with suppress(KeyError):
                    if (
                        ip_permission["FromPort"] <= 20 <= ip_permission["ToPort"]
                        and ip_permission["FromPort"] <= 21 <= ip_permission["ToPort"]
                        and ip_permission["IpProtocol"] in {"tcp"}
                    ):
                        parameters = {
                            "Region": region,
                            "Account": group["OwnerId"],
                            "SecurityGroupId": group["GroupId"],
                        }
                        locations = [
                            Location(
                                access_patterns=(
                                    f"/IpPermissions/{index}/FromPort",
                                    f"/IpPermissions/{index}/ToPort",
                                    (f"/IpPermissions/{index}/IpRanges/{index_ip_range}/CidrIp"),
                                ),
                                arn=build_arn(
                                    service="ec2",
                                    resource="security-group",
                                    parameters=parameters,
                                ),
                                values=(
                                    ip_permission["FromPort"],
                                    ip_permission["ToPort"],
                                    ip_range["CidrIp"],
                                ),
                                method=method,
                            )
                            for index_ip_range, ip_range in enumerate(ip_permission["IpRanges"])
                            if ip_range["CidrIp"] == "0.0.0.0/0"
                        ]
                        locations.extend(
                            [
                                Location(
                                    access_patterns=(
                                        f"/IpPermissions/{index}/FromPort",
                                        f"/IpPermissions/{index}/ToPort",
                                        (
                                            f"/IpPermissions/{index}/"
                                            "Ipv6Ranges"
                                            f"/{index_ip_range}/CidrIpv6"
                                        ),
                                    ),
                                    arn=build_arn(
                                        service="ec2",
                                        resource="security-group",
                                        parameters=parameters,
                                    ),
                                    values=(
                                        ip_permission["FromPort"],
                                        ip_permission["ToPort"],
                                        ip_range["CidrIpv6"],
                                    ),
                                    method=method,
                                )
                                for index_ip_range, ip_range in enumerate(
                                    ip_permission["Ipv6Ranges"],
                                )
                                if ip_range["CidrIpv6"] == "::/0"
                            ],
                        )
            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                aws_response=group,
            )
    return (vulns, True)


def _get_locations_permissions(
    group: dict,
    locations: list[Location],
    permission_data: dict,
    method: MethodsEnum,
) -> list[Location]:
    index = permission_data["index"]
    region = permission_data["region"]
    if (
        (ip_permission := permission_data["ip_permission"])
        and ip_permission["FromPort"] == 0
        and ip_permission["ToPort"] == 65535
    ):
        parameters = {
            "Region": region,
            "Account": group["OwnerId"],
            "SecurityGroupId": group["GroupId"],
        }
        locations = [
            Location(
                access_patterns=(
                    f"/IpPermissions/{index}/FromPort",
                    f"/IpPermissions/{index}/ToPort",
                    (f"/IpPermissions/{index}/IpRanges/{index_ip_range}/CidrIp"),
                ),
                arn=build_arn(
                    service="ec2",
                    resource="security-group",
                    parameters=parameters,
                ),
                values=(
                    ip_permission["FromPort"],
                    ip_permission["ToPort"],
                    ip_range["CidrIp"],
                ),
                method=method,
            )
            for index_ip_range, ip_range in enumerate(ip_permission["IpRanges"])
            if ip_range["CidrIp"] == "0.0.0.0/0"
        ]
    return locations


@SHIELD
async def open_all_ports_to_the_public(
    credentials: AwsCredentials,
    region: str,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.OPEN_ALL_PORTS_TO_THE_PUBLIC
    security_groups = await get_security_groups(credentials, region)
    vulns: Vulnerabilities = ()
    if security_groups:
        for group in security_groups:
            locations: list[Location] = []
            for index, ip_permission in enumerate(group["IpPermissions"]):
                permission_data = {
                    "index": index,
                    "ip_permission": ip_permission,
                    "region": region,
                }
                with suppress(KeyError):
                    locations = _get_locations_permissions(
                        group,
                        locations,
                        permission_data,
                        method,
                    )
            for index, ip_permission in enumerate(group["IpPermissionsEgress"]):
                with suppress(KeyError):
                    if ip_permission["FromPort"] == 0 and ip_permission["ToPort"] == 65535:
                        parameters = {
                            "Region": region,
                            "Account": group["OwnerId"],
                            "SecurityGroupId": group["GroupId"],
                        }
                        locations.append(
                            Location(
                                access_patterns=(
                                    (f"/IpPermissionsEgress/{index}/FromPort"),
                                    f"/IpPermissionsEgress/{index}/ToPort",
                                ),
                                arn=build_arn(
                                    service="ec2",
                                    resource="security-group",
                                    parameters=parameters,
                                ),
                                values=(
                                    ip_permission["FromPort"],
                                    ip_permission["ToPort"],
                                ),
                                method=method,
                            ),
                        )
            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                aws_response=group,
            )
    return (vulns, True)


@SHIELD
async def default_seggroup_allows_all_traffic(
    credentials: AwsCredentials,
    region: str,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.DEFAULT_SEGGROUP_ALLOWS_ALL_TRAFFIC
    security_groups = await get_security_groups(credentials, region)
    vulns: Vulnerabilities = ()
    if security_groups:
        for group in security_groups:
            if group["GroupName"] != "default":
                continue
            locations: list[Location] = []
            for index, ip_permission in enumerate(group["IpPermissions"]):
                parameters = {
                    "Region": region,
                    "Account": group["OwnerId"],
                    "SecurityGroupId": group["GroupId"],
                }
                locations = [
                    Location(
                        access_patterns=(
                            (f"/IpPermissions/{index}/IpRanges/{index_ip_range}/CidrIp"),
                        ),
                        arn=build_arn(
                            service="ec2",
                            resource="security-group",
                            parameters=parameters,
                        ),
                        values=(ip_range["CidrIp"],),
                        method=method,
                    )
                    for index_ip_range, ip_range in enumerate(ip_permission["IpRanges"])
                    if ip_range["CidrIp"] == "0.0.0.0/0"
                ]

            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                aws_response=group,
            )
    return (vulns, True)


@SHIELD
async def insecure_port_range_in_security_group(
    credentials: AwsCredentials,
    region: str,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.INSECURE_PORT_RANGE_IN_SECURITY_GROUP
    security_groups = await get_security_groups(credentials, region)
    vulns: Vulnerabilities = ()
    if security_groups:
        for group in security_groups:
            locations: list[Location] = []
            for index, rule in enumerate(group["IpPermissions"]):
                with suppress(KeyError):
                    if rule["FromPort"] != rule["ToPort"]:
                        parameters = {
                            "Region": region,
                            "Account": group["OwnerId"],
                            "SecurityGroupId": group["GroupId"],
                        }
                        locations.append(
                            Location(
                                access_patterns=(
                                    (f"/IpPermissions/{index}/FromPort"),
                                    f"/IpPermissions/{index}/ToPort",
                                ),
                                arn=build_arn(
                                    service="ec2",
                                    resource="security-group",
                                    parameters=parameters,
                                ),
                                values=(
                                    rule["FromPort"],
                                    rule["ToPort"],
                                ),
                                method=method,
                            ),
                        )

            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                aws_response=group,
            )

    return (vulns, True)


def _acl_rule_is_public(acl_rule: dict, *, egress: bool, action: str) -> bool:
    """Check if an ACL rule allow all ingress traffic."""
    is_public = False
    if acl_rule["Egress"] == egress and acl_rule["RuleAction"] == action:
        if "CidrBlock" in acl_rule:
            is_public = acl_rule["CidrBlock"] == "0.0.0.0/0"
        if "Ipv6CidrBlock" in acl_rule:
            is_public = acl_rule["Ipv6CidrBlock"] == "::/0"
    return is_public and "PortRange" not in acl_rule and acl_rule["Protocol"] == "-1"


@SHIELD
async def network_acls_allow_all_ingress_traffic(
    credentials: AwsCredentials,
    region: str,
) -> tuple[Vulnerabilities, bool]:
    paginated_results_key = "NetworkAcls"
    response: dict[str, Any] = await run_boto3_fun(
        credentials=credentials,
        service="ec2",
        function="describe_network_acls",
        region=region,
        paginated_results_key=paginated_results_key,
    )
    method = MethodsEnum.NETWORK_ACLS_ALLOW_ALL_INGRESS_TRAFFIC
    network_acls = response.get(paginated_results_key, []) if response else []
    vulns: Vulnerabilities = ()
    for rule in network_acls:
        owner_id = rule["OwnerId"]
        for entry in rule["Entries"]:
            parameters = {
                "Region": region,
                "Account": owner_id,
                "NaclId": rule["NetworkAclId"],
            }
            locations: list[Location] = []
            if (
                not entry["Egress"]
                and _acl_rule_is_public(entry, egress=False, action="allow")
                and entry.get("CidrBlock", "")
            ):
                locations = [
                    Location(
                        access_patterns=("/Egress", "/CidrBlock", "/Protocol"),
                        arn=build_arn(service="ec2", resource="network-acl", parameters=parameters),
                        values=(entry["Egress"], entry["CidrBlock"], entry["Protocol"]),
                        method=method,
                    ),
                ]
            elif (
                not entry["Egress"]
                and _acl_rule_is_public(entry, egress=False, action="allow")
                and entry.get("Ipv6CidrBlock", "")
            ):
                locations = [
                    Location(
                        access_patterns=("/Egress", "/Ipv6CidrBlock", "/Protocol"),
                        arn=build_arn(service="ec2", resource="network-acl", parameters=parameters),
                        values=(entry["Egress"], entry["Ipv6CidrBlock"], entry["Protocol"]),
                        method=method,
                    ),
                ]

            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                aws_response=entry,
            )
    return (vulns, True)


def _acl_check_ports(rule: dict, resource_arn: str, method: MethodsEnum) -> Vulnerabilities:
    vulns: Vulnerabilities = ()
    for acl_rule in rule["Entries"]:
        if acl_rule["RuleAction"] != "allow" or not acl_rule["Egress"]:
            continue

        if (
            acl_rule["Protocol"] == "-1"
            or "PortRange" not in acl_rule
            or (int(acl_rule["PortRange"]["To"]) - int(acl_rule["PortRange"]["From"]) == 0)
        ):
            use_protocol = acl_rule["Protocol"] == "-1" or "PortRange" not in acl_rule
            locations = [
                Location(
                    access_patterns=("/Egress", "/Protocol")
                    if use_protocol
                    else ("/Egress", "/PortRange"),
                    arn=resource_arn,
                    values=(acl_rule["Egress"], acl_rule["Protocol"])
                    if use_protocol
                    else (acl_rule["Egress"], acl_rule["PortRange"]),
                    method=method,
                ),
            ]
            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                aws_response=acl_rule,
            )
    return vulns


@SHIELD
async def network_acls_allow_egress_traffic(
    credentials: AwsCredentials,
    region: str,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.NETWORK_ACLS_ALLOW_EGRESS_TRAFFIC
    paginated_results_key = "NetworkAcls"
    response: dict[str, Any] = await run_boto3_fun(
        credentials=credentials,
        region=region,
        service="ec2",
        function="describe_network_acls",
        paginated_results_key=paginated_results_key,
    )
    network_acls = response.get(paginated_results_key, []) if response else []
    vulns: Vulnerabilities = ()
    for rule in network_acls:
        parameters = {
            "Region": region,
            "Account": rule["OwnerId"],
            "NaclId": rule["NetworkAclId"],
        }
        resource_arn = build_arn(service="ec2", resource="network-acl", parameters=parameters)
        vulns += _acl_check_ports(rule, resource_arn, method)

    return (vulns, True)


async def iterate_api_methods_all_resources(
    *,
    region: str,
    credentials: AwsCredentials,
    api_id: str,
    resource_id: str,
    resource_methods_names: list[str],
) -> Vulnerabilities:
    method = MethodsEnum.APIGATEWAY_ALLOWS_ANONYMOUS_ACCESS
    vulns: Vulnerabilities = ()
    for resource_method in resource_methods_names:
        api_method = await run_boto3_fun(
            credentials=credentials,
            region=region,
            service="apigateway",
            function="get_method",
            parameters={
                "restApiId": api_id,
                "resourceId": resource_id,
                "httpMethod": resource_method,
            },
        )

        if not api_method or api_method["authorizationType"] != "NONE":
            continue
        parameters = {
            "Region": region,
            "RestApiId": api_id,
        }
        arn = build_arn(service="apigateway", resource="RestApi", parameters=parameters)
        locations = [
            Location(
                access_patterns=("/authorizationType",),
                arn=arn,
                values=(api_method["authorizationType"],),
                method=method,
            ),
        ]

        vulns += build_vulnerabilities(
            locations=locations,
            method=method,
            aws_response=api_method,
        )

    return vulns


@SHIELD
async def apigateway_allows_anonymous_access(
    credentials: AwsCredentials,
    region: str,
) -> tuple[Vulnerabilities, bool]:
    paginated_results_key = "items"
    response: dict[str, Any] = await run_boto3_fun(
        credentials=credentials,
        service="apigateway",
        function="get_rest_apis",
        region=region,
        paginated_results_key=paginated_results_key,
    )
    rest_apis = response.get(paginated_results_key, []) if response else []
    vulns: Vulnerabilities = ()
    for rest_api in rest_apis:
        api_id = rest_api["id"]
        resources_response = await run_boto3_fun(
            region=region,
            credentials=credentials,
            service="apigateway",
            function="get_resources",
            parameters={"restApiId": str(api_id)},
        )
        api_resources = resources_response.get("items", []) if resources_response else []
        for api_resource in api_resources:
            resource_id = api_resource["id"]
            resource_methods: dict[str, Any] = api_resource.get("resourceMethods", {})
            resource_methods_names = list(resource_methods.keys())
            vulns += await iterate_api_methods_all_resources(
                region=region,
                credentials=credentials,
                api_id=api_id,
                resource_id=resource_id,
                resource_methods_names=resource_methods_names,
            )

    return (vulns, True)


@SHIELD
async def aws_unrestricted_access_to_msk_brokers(
    credentials: AwsCredentials,
    region: str,
) -> tuple[Vulnerabilities, bool]:
    vulns: Vulnerabilities = ()
    method = MethodsEnum.AWS_UNRESTRICTED_ACCESS_TO_MSK_BROKERS
    paginated_results_key = "ClusterInfoList"
    clusters: dict[str, Any] = await run_boto3_fun(
        credentials=credentials,
        region=region,
        service="kafka",
        function="list_clusters_v2",
        paginated_results_key=paginated_results_key,
    )
    for cluster in clusters.get(paginated_results_key, []):
        if cluster.get("ClusterType", "") == "SERVERLESS":
            continue
        cluster_arn = cluster["ClusterArn"]
        cluster_config = await run_boto3_fun(
            credentials=credentials,
            region=region,
            service="kafka",
            function="describe_cluster",
            parameters={
                "ClusterArn": cluster_arn,
            },
        )
        unauthenticated_access = (
            cluster_config.get("ClusterInfo", {})
            .get("ClientAuthentication", {})
            .get("Unauthenticated", {})
            .get("Enabled")
        )
        if unauthenticated_access:
            locations = [
                Location(
                    arn=cluster_arn,
                    access_patterns=("/ClusterInfo/ClientAuthentication/Unauthenticated/Enabled",),
                    values=(unauthenticated_access,),
                    method=method,
                ),
            ]
            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                aws_response=cluster_config,
            )

    return (vulns, True)


@SHIELD
async def aws_eks_unrestricted_cidr(
    credentials: AwsCredentials,
    region: str,
) -> tuple[Vulnerabilities, bool]:
    vulns: Vulnerabilities = ()
    method = MethodsEnum.AWS_EKS_UNRESTRICTED_CIDR
    paginated_results_key = "clusters"
    eks_clusters: dict[str, Any] = await run_boto3_fun(
        region=region,
        credentials=credentials,
        service="eks",
        function="list_clusters",
        paginated_results_key=paginated_results_key,
    )
    for cluster in eks_clusters.get(paginated_results_key, []):
        cluster_description: dict[str, Any] = await run_boto3_fun(
            region=region,
            credentials=credentials,
            service="eks",
            function="describe_cluster",
            parameters={"name": cluster},
        )
        vpc_config = cluster_description["cluster"].get("resourcesVpcConfig", {})
        public_access = vpc_config.get("endpointPublicAccess")
        public_access_cidrs = vpc_config.get("publicAccessCidrs", [])
        if public_access and public_access_cidrs == ["0.0.0.0/0"]:
            locations = [
                Location(
                    arn=cluster_description["cluster"]["arn"],
                    access_patterns=("/cluster/resourcesVpcConfig/publicAccessCidrs",),
                    values=(public_access_cidrs,),
                    method=method,
                ),
            ]
            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                aws_response=cluster_description,
            )

    return (vulns, True)


CHECKS: AwsMethodsTuple = (
    allows_anyone_to_admin_ports,
    unrestricted_cidrs,
    unrestricted_ip_protocols,
    security_groups_ip_ranges_in_rfc1918,
    unrestricted_dns_access,
    unrestricted_ftp_access,
    open_all_ports_to_the_public,
    default_seggroup_allows_all_traffic,
    insecure_port_range_in_security_group,
    network_acls_allow_all_ingress_traffic,
    network_acls_allow_egress_traffic,
    apigateway_allows_anonymous_access,
    aws_unrestricted_access_to_msk_brokers,
    aws_eks_unrestricted_cidr,
)
