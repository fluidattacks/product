import json

import ctx
from aioextensions import (
    resolve,
)
from google.api_core.exceptions import (
    Forbidden,
)
from google.cloud import (
    compute_v1,
)
from google.oauth2.service_account import (
    Credentials,
)
from lib_cspm.gcp import (
    f200,
    f325,
    f405,
)
from lib_cspm.gcp.model import (
    GcpMethodsTuple,
)
from model.core import (
    FindingEnum,
    GcpCredentials,
    Vulnerability,
)
from utils.logs import (
    log_blocking,
)
from utils.state import VulnerabilitiesEphemeralStore

GCP_CHECKS: dict[FindingEnum, GcpMethodsTuple] = {
    FindingEnum.F200: f200.CHECKS,
    FindingEnum.F325: f325.CHECKS,
    FindingEnum.F405: f405.CHECKS,
}

CHECKS_NO_REGIONS = (
    f200.object_versioning_is_not_enabled,
    f200.retention_policy_is_not_configured,
    f200.logging_is_not_enabled_on_storage_bucket,
    f200.lifecycle_is_not_defined,
    f325.public_buckets,
    f405.uniform_bucket_level_access_is_disabled,
)


def get_regions(credentials: Credentials) -> list[str]:
    active_regions = ["us-east1-b"]
    try:
        compute_client = compute_v1.RegionsClient(credentials=credentials)
        regions = compute_client.list(project=credentials.project_id).items
        active_regions = [region.name for region in regions]
    except Forbidden:
        msg = "Unable to get active regions. CSPM analysis will be done on us-east1-b"
        log_blocking("warning", msg)

    return active_regions


def handle_vulnerabilities(
    vulnerabilities: tuple[Vulnerability, ...],
    stores: VulnerabilitiesEphemeralStore,
) -> None:
    stores.store_vulns(vulnerabilities)


async def analyze_cloud(
    credentials: GcpCredentials,
    stores: VulnerabilitiesEphemeralStore,
    finding_checks: set[FindingEnum],
) -> set[str]:
    active_regions = get_regions(credentials)  # type: ignore[arg-type]
    methods_with_errors: set[str] = set()
    for finding, finding_methods in GCP_CHECKS.items():
        if finding not in finding_checks:
            continue

        log_blocking("info", "Running finding %s", finding.name)
        futures = []
        for method in finding_methods:
            if method not in CHECKS_NO_REGIONS:
                futures += [
                    method(credentials, region)  # type: ignore[call-arg]
                    for region in active_regions
                ]
            else:
                futures.append(method(credentials))

        for gcp_method in resolve(futures, workers=ctx.CPU_CORES):
            method_vulns, method_succeeded = await gcp_method

            handle_vulnerabilities(method_vulns, stores)

            if isinstance(method_succeeded, str):
                methods_with_errors.add(method_succeeded)

    return methods_with_errors


async def analyze(
    *,
    credentials: GcpCredentials,
    stores: VulnerabilitiesEphemeralStore,
) -> dict[str, list[str]]:
    if not any(finding in ctx.SKIMS_CONFIG.checks for finding in GCP_CHECKS):
        return {}

    try:
        decoded_credentials = json.loads(credentials.private_key)
    except json.JSONDecodeError:
        log_blocking("error", "Unable to json decode GCP credentials")
        return {}

    credentials = Credentials.from_service_account_info(decoded_credentials)

    log_blocking("info", "Starting CSPM/GCP analysis on credentials")

    error_methods = await analyze_cloud(credentials, stores, ctx.SKIMS_CONFIG.checks)

    gcp_errors = {"gcp_errors": list(error_methods)}

    log_blocking("info", "CSPM/GCP analysis on credentials completed!")
    return gcp_errors
