import functools
import sys
from typing import (
    Any,
    cast,
)

from lib_cspm.gcp.model import (
    GcpClientError,
    GcpMethod,
)
from model.core import (
    Vulnerabilities,
)
from utils.logs import (
    log,
    log_to_remote,
)


def shield_cspm_gcp_decorator(function: GcpMethod) -> GcpMethod:
    @functools.wraps(function)
    async def wrapper(
        *args: Any,  # noqa: ANN401
        **kwargs: Any,  # noqa: ANN401
    ) -> tuple[Vulnerabilities, bool | str]:
        try:
            return await function(*args, **kwargs)
        except GcpClientError:
            await log(
                "warning",
                "Unable to obtain GCP service information in %s",
                function.__name__,
            )
            return ((), f"gcp.{function.__name__}")
        except Exception:  # noqa: BLE001
            exc_type, _, _ = sys.exc_info()
            msg = f"Function {function.__name__} failed with exception: {exc_type}"
            await log("error", msg)

            await log_to_remote(msg=msg, severity="error")
            return ((), f"gcp.{function.__name__}")

    return cast(GcpMethod, wrapper)
