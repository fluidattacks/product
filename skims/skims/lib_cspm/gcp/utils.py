import json
from collections.abc import (
    Callable,
    Iterable,
)

from fluidattacks_core.serializers.snippet import (
    SnippetViewport,
    make_snippet,
)
from google.api_core.exceptions import (
    Forbidden,
)
from google.cloud.storage.bucket import (
    Bucket,
)
from google.cloud.storage.client import (
    Client,
)
from json_source_map import (
    calculate,
)
from lib_cspm.gcp.model import (
    GcpClientError,
    Location,
    SnippetObject,
)
from lib_cspm.methods_enum import (
    MethodsEnumCSPM as MethodsEnum,
)
from model.core import (
    GcpCredentials,
    Vulnerabilities,
)
from utils.build_vulns import (
    build_inputs_vuln,
    build_metadata,
)
from utils.logs import (
    log_blocking,
)
from utils.translations import (
    t,
)


def _build_where(gcp_response: SnippetObject, location: str) -> str:
    return f"{gcp_response['path']}/{gcp_response['vulnerable_property']}: {location}"


def build_vulnerabilities(
    locations: Iterable[Location],
    method: MethodsEnum,
    gcp_response: SnippetObject,
) -> Vulnerabilities:
    str_content = json.dumps(gcp_response, indent=4, default=str)
    json_paths = calculate(str_content)

    vulns: Vulnerabilities = ()

    for location in locations:
        description = (
            f"{t(location.method.name, **location.desc_params)} {t(key='words.in')} {location.uri}"
        )

        vulns += (
            build_inputs_vuln(
                method=method.value,
                what=location.uri,
                where=_build_where(gcp_response, location.values[0])
                if location.access_patterns
                else description,
                stream="skims",
                metadata=build_metadata(
                    method=method.value,
                    description=description,
                    snippet=make_snippet(
                        content=str_content,
                        viewport=SnippetViewport(
                            column=json_paths[location.access_patterns[-1]].key_start.column
                            if location.access_patterns
                            else 0,
                            line=json_paths[location.access_patterns[-1]].key_start.line + 1
                            if location.access_patterns
                            else 0,
                            wrap=True,
                        ),
                    ).content,
                ),
            ),
        )
    return vulns


def get_storage_client_buckets(
    credentials: GcpCredentials,
) -> tuple[list[Bucket], str]:
    storage_client = Client(credentials=credentials)
    project_id = str(storage_client.project)
    buckets: list[Bucket] = list(storage_client.list_buckets())
    return buckets, project_id


def run_gcp_client(
    gcp_client_to_run: Callable[[GcpCredentials], tuple[list, str]],
    credentials: GcpCredentials,
) -> tuple[list, str]:
    try:
        return gcp_client_to_run(credentials)
    except Forbidden:
        msg = "Credentials do not have permissions to access resource or resource does not exist"
        log_blocking("warning", msg)
        return ([], "")
    except Exception as exc:
        raise GcpClientError from exc
