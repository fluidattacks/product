from collections.abc import (
    Callable,
    Coroutine,
)
from enum import (
    Enum,
)
from typing import (
    Any,
    NamedTuple,
    TypeAlias,
    TypedDict,
)

from lib_cspm.methods_enum import (
    MethodsEnumCSPM as MethodsEnum,
)
from model.core import (
    GcpCredentials,
    Vulnerabilities,
)

GcpMethod: TypeAlias = Callable[
    [GcpCredentials],
    Coroutine[Any, Any, tuple[Vulnerabilities, bool | str]],
]

GcpMethodsTuple = tuple[GcpMethod, ...]


class GcpClientError(Exception):
    pass


class Location(NamedTuple):
    uri: str
    access_patterns: tuple[str, ...]
    method: MethodsEnum
    values: tuple[Any, ...]
    desc_params: dict[str, str] = {}  # noqa: RUF012


class GcpServices(Enum):
    CLOUD_STORAGE = "CLOUD_STORAGE"


class SnippetObject(TypedDict):
    project_id: str
    service: GcpServices
    path: str
    vulnerable_property: str
    vulnerability: str
