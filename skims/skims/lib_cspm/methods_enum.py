from enum import (
    Enum,
)

from model.core import (
    DeveloperEnum,
    FindingEnum,
    MethodOriginEnum,
    VulnerabilityTechnique,
)
from utils.method_enums import get_method_info_interface

cspm_method_info = get_method_info_interface(
    module="lib_cspm",
    technique=VulnerabilityTechnique.CSPM,
)


class MethodsEnumCSPM(Enum):
    ALLOWS_PRIV_ESCALATION_BY_POLICIES_VERSIONS = cspm_method_info(
        file_name="aws",
        name="allows_priv_escalation_by_policies_versions",
        finding=FindingEnum.F005,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        origin=MethodOriginEnum.ASSERTS,
        auto_approve=True,
    )
    ALLOWS_PRIV_ESCALATION_BY_ATTACH_POLICY = cspm_method_info(
        file_name="aws",
        name="allows_priv_escalation_by_attach_policy",
        finding=FindingEnum.F005,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        origin=MethodOriginEnum.ASSERTS,
        auto_approve=True,
    )
    USES_INSECURE_SECURITY_POLICY = cspm_method_info(
        file_name="aws",
        name="uses_insecure_security_policy",
        finding=FindingEnum.F016,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        origin=MethodOriginEnum.ASSERTS,
        auto_approve=True,
    )
    REDIS_CACHE_INSECURE_TLS_VERSION = cspm_method_info(
        file_name="azure",
        name="redis_cache_insecure_tls_version",
        finding=FindingEnum.F016,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    AZURE_WEB_APP_INSECURE_TLS_VERSION = cspm_method_info(
        file_name="azure",
        name="azure_web_app_insecure_tls_version",
        finding=FindingEnum.F016,
        developer=DeveloperEnum.ELVER_TOBO,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    AZURE_DB_FOR_MYSQL_FLEX_SERVERS_INSECURE_TLS_VERSION = cspm_method_info(
        file_name="azure",
        name="azure_db_for_mysql_flex_servers_insecure_tls_version",
        finding=FindingEnum.F016,
        developer=DeveloperEnum.ELVER_TOBO,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    AZURE_DB_POSTGRESQL_INSECURE_TLS_VERSION = cspm_method_info(
        file_name="azure",
        name="azure_db_postgresql_insecure_tls_version",
        finding=FindingEnum.F016,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    AZURE_DB_PSQL_FLEX_SERVER_INSECURE_TLS_VERSION = cspm_method_info(
        file_name="azure",
        name="azure_db_psql_flex_server_insecure_tls_version",
        finding=FindingEnum.F016,
        developer=DeveloperEnum.ELVER_TOBO,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    AZURE_REDIS_CACHE_ALLOWS_CONNECTIONS_WITHOUT_SSL = cspm_method_info(
        file_name="azure",
        name="azure_redis_cache_allows_connections_without_ssl",
        finding=FindingEnum.F016,
        developer=DeveloperEnum.ELVER_TOBO,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    AZURE_DB_FOR_POSTGRESQL_SSL_DISABLED = cspm_method_info(
        file_name="azure",
        name="azure_db_for_postgresql_ssl_disabled",
        finding=FindingEnum.F016,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    AZURE_DB_MYSQL_SSL_DISABLED = cspm_method_info(
        file_name="azure",
        name="azure_db_mysql_ssl_disabled",
        finding=FindingEnum.F016,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    SERVES_CONTENT_OVER_INSECURE_PROTOCOLS = cspm_method_info(
        file_name="aws",
        name="serves_content_over_insecure_protocols",
        finding=FindingEnum.F016,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        origin=MethodOriginEnum.ASSERTS,
        auto_approve=True,
    )
    ELBV2_USES_INSECURE_SSL_PROTOCOL = cspm_method_info(
        file_name="aws",
        name="elbv2_uses_insecure_ssl_protocol",
        finding=FindingEnum.F016,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        origin=MethodOriginEnum.ASSERTS,
        auto_approve=True,
    )
    ELBV2_USES_INSECURE_SSL_CIPHER = cspm_method_info(
        file_name="aws",
        name="elbv2_uses_insecure_ssl_cipher",
        finding=FindingEnum.F016,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        origin=MethodOriginEnum.ASSERTS,
        auto_approve=True,
    )
    STORAGE_ACCOUNT_NOT_ENFORCE_LATEST_TLS = cspm_method_info(
        file_name="azure",
        name="storage_account_not_enforce_latest_tls",
        finding=FindingEnum.F016,
        developer=DeveloperEnum.JOHN_PEREZ,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    AZURE_API_MGMT_BACK_INSECURE_TLS_VERSION = cspm_method_info(
        file_name="azure",
        name="azure_api_mgmt_back_insecure_tls_version",
        finding=FindingEnum.F016,
        developer=DeveloperEnum.ELVER_TOBO,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    AZURE_API_MGMT_FRONT_INSECURE_TLS_VERSION = cspm_method_info(
        file_name="azure",
        name="azure_api_mgmt_front_insecure_tls_version",
        finding=FindingEnum.F016,
        developer=DeveloperEnum.ELVER_TOBO,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    AZURE_DB_PSQL_FLEXIBLE_SERVER__SSL_DISABLED = cspm_method_info(
        file_name="azure",
        name="azure_db_psql_flexible_server__ssl_disabled",
        finding=FindingEnum.F016,
        developer=DeveloperEnum.ELVER_TOBO,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    API_GATEWAY_INSECURE_TLS_VERSION = cspm_method_info(
        file_name="aws",
        name="api_gateway_insecure_tls_version",
        finding=FindingEnum.F016,
        developer=DeveloperEnum.ELVER_TOBO,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    AWS_APP_MESH_VIRTUAL_GATEWAY_TLS_DISABLED = cspm_method_info(
        file_name="aws",
        name="aws_app_mesh_virtual_gateway_tls_disabled",
        finding=FindingEnum.F016,
        developer=DeveloperEnum.ELVER_TOBO,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    AWS_RDS_INSTANCE_TLS_DISABLED = cspm_method_info(
        file_name="aws",
        name="aws_rds_instance_tls_disabled",
        finding=FindingEnum.F016,
        developer=DeveloperEnum.ELVER_TOBO,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    AWS_RDS_CLUSTER_TLS_DISABLED = cspm_method_info(
        file_name="aws",
        name="aws_rds_cluster_tls_disabled",
        finding=FindingEnum.F016,
        developer=DeveloperEnum.ELVER_TOBO,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    AWS_OPENSEARCH_DOMAIN_INSECURE_TLS_VERSION = cspm_method_info(
        file_name="aws",
        name="aws_opensearch_domain_insecure_tls_version",
        finding=FindingEnum.F016,
        developer=DeveloperEnum.ELVER_TOBO,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    AWS_MSK_CLIENT_BROKER_TLS_DISABLED = cspm_method_info(
        file_name="aws",
        name="aws_msk_client_broker_tls_disabled",
        finding=FindingEnum.F016,
        developer=DeveloperEnum.ELVER_TOBO,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    AWS_MSK_BROKER_BROKER_TLS_DISABLED = cspm_method_info(
        file_name="aws",
        name="aws_msk_broker_broker_tls_disabled",
        finding=FindingEnum.F016,
        developer=DeveloperEnum.ELVER_TOBO,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    AWS_DOCUMENT_DB_CLUSTER_TLS_DISABLED = cspm_method_info(
        file_name="aws",
        name="aws_document_db_cluster_tls_disabled",
        finding=FindingEnum.F016,
        developer=DeveloperEnum.ELVER_TOBO,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    ALLOWS_ANYONE_TO_ADMIN_PORTS = cspm_method_info(
        file_name="aws",
        name="allows_anyone_to_admin_ports",
        finding=FindingEnum.F024,
        developer=DeveloperEnum.DIEGO_RESTREPO,
        origin=MethodOriginEnum.ASSERTS,
        auto_approve=True,
    )
    UNRESTRICTED_CIDRS = cspm_method_info(
        file_name="aws",
        name="unrestricted_cidrs",
        finding=FindingEnum.F024,
        developer=DeveloperEnum.DIEGO_RESTREPO,
        origin=MethodOriginEnum.ASSERTS,
        auto_approve=True,
    )
    UNRESTRICTED_IP_PROTOCOLS = cspm_method_info(
        file_name="aws",
        name="unrestricted_ip_protocols",
        finding=FindingEnum.F024,
        developer=DeveloperEnum.DIEGO_RESTREPO,
        origin=MethodOriginEnum.ASSERTS,
        auto_approve=True,
    )
    SECURITY_GROUPS_IP_RANGES_IN_RFC1918 = cspm_method_info(
        file_name="aws",
        name="security_groups_ip_ranges_in_rfc1918",
        finding=FindingEnum.F024,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        origin=MethodOriginEnum.ASSERTS,
        auto_approve=True,
    )
    UNRESTRICTED_DNS_ACCESS = cspm_method_info(
        file_name="aws",
        name="unrestricted_dns_access",
        finding=FindingEnum.F024,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        origin=MethodOriginEnum.ASSERTS,
        auto_approve=True,
    )
    UNRESTRICTED_FTP_ACCESS = cspm_method_info(
        file_name="aws",
        name="unrestricted_ftp_access",
        finding=FindingEnum.F024,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        origin=MethodOriginEnum.ASSERTS,
        auto_approve=True,
    )
    OPEN_ALL_PORTS_TO_THE_PUBLIC = cspm_method_info(
        file_name="aws",
        name="open_all_ports_to_the_public",
        finding=FindingEnum.F024,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        origin=MethodOriginEnum.ASSERTS,
        auto_approve=True,
    )
    DEFAULT_SEGGROUP_ALLOWS_ALL_TRAFFIC = cspm_method_info(
        file_name="aws",
        name="default_seggroup_allows_all_traffic",
        finding=FindingEnum.F024,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        origin=MethodOriginEnum.ASSERTS,
        auto_approve=True,
    )
    INSECURE_PORT_RANGE_IN_SECURITY_GROUP = cspm_method_info(
        file_name="aws",
        name="insecure_port_range_in_security_group",
        finding=FindingEnum.F024,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        origin=MethodOriginEnum.ASSERTS,
        auto_approve=True,
    )
    NETWORK_ACLS_ALLOW_EGRESS_TRAFFIC = cspm_method_info(
        file_name="aws",
        name="network_acls_allow_egress_traffic",
        finding=FindingEnum.F024,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        origin=MethodOriginEnum.ASSERTS,
        auto_approve=True,
    )
    NETWORK_ACLS_ALLOW_ALL_INGRESS_TRAFFIC = cspm_method_info(
        file_name="aws",
        name="network_acls_allow_all_ingress_traffic",
        finding=FindingEnum.F024,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        origin=MethodOriginEnum.ASSERTS,
        cvss="CVSS:3.1/AV:A/AC:H/PR:L/UI:N/S:U/C:L/I:N/A:N/E:U/RL:U/RC:U",
        cvss_v4=("CVSS:4.0/AV:A/AC:H/PR:L/UI:N/AT:N/VC:L/VI:N/VA:N/SC:N/SI:N/SA:N/E:U"),
        cwe_ids=["CWE-1327"],
        auto_approve=True,
    )
    APIGATEWAY_ALLOWS_ANONYMOUS_ACCESS = cspm_method_info(
        file_name="aws",
        name="apigateway_allows_anonymous_access",
        finding=FindingEnum.F024,
        developer=DeveloperEnum.JOHN_PEREZ,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    AWS_UNRESTRICTED_ACCESS_TO_MSK_BROKERS = cspm_method_info(
        file_name="aws",
        name="aws_unrestricted_access_to_msk_brokers",
        finding=FindingEnum.F024,
        developer=DeveloperEnum.ELVER_TOBO,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    AWS_EKS_UNRESTRICTED_CIDR = cspm_method_info(
        file_name="aws",
        name="aws_eks_unrestricted_cidr",
        finding=FindingEnum.F024,
        developer=DeveloperEnum.ELVER_TOBO,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    ADMIN_POLICY_ATTACHED = cspm_method_info(
        file_name="aws",
        name="admin_policy_attached",
        finding=FindingEnum.F031,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        origin=MethodOriginEnum.ASSERTS,
        auto_approve=True,
    )
    VPC_ENDPOINTS_EXPOSED = cspm_method_info(
        file_name="aws",
        name="vpc_endpoints_exposed",
        finding=FindingEnum.F031,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        origin=MethodOriginEnum.ASSERTS,
        auto_approve=True,
    )
    AWS_S3_LOG_DELIVERY_WRITE_ACCESS = cspm_method_info(
        file_name="aws",
        name="aws_s3_log_delivery_write_access",
        finding=FindingEnum.F031,
        developer=DeveloperEnum.ELVER_TOBO,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    USERS_WITH_PASSWORD_AND_ACCESS_KEYS = cspm_method_info(
        file_name="aws",
        name="users_with_password_and_access_keys",
        finding=FindingEnum.F031,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        origin=MethodOriginEnum.ASSERTS,
        auto_approve=True,
    )
    OPEN_PASSROLE = cspm_method_info(
        file_name="aws",
        name="open_passrole",
        finding=FindingEnum.F031,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        origin=MethodOriginEnum.ASSERTS,
        auto_approve=True,
    )
    HAS_PERMISSIVE_ROLE_POLICIES = cspm_method_info(
        file_name="aws",
        name="has_permissive_role_policies",
        finding=FindingEnum.F031,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        origin=MethodOriginEnum.ASSERTS,
        auto_approve=True,
    )
    FULL_ACCESS_TO_SSM = cspm_method_info(
        file_name="aws",
        name="full_access_to_ssm",
        finding=FindingEnum.F031,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        origin=MethodOriginEnum.ASSERTS,
        auto_approve=True,
    )
    NEGATIVE_STATEMENT = cspm_method_info(
        file_name="aws",
        name="negative_statement",
        finding=FindingEnum.F031,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        origin=MethodOriginEnum.ASSERTS,
        auto_approve=True,
    )
    AWS_ECR_REPOSITORY_EXPOSED = cspm_method_info(
        file_name="aws",
        name="aws_ecr_repository_exposed",
        finding=FindingEnum.F031,
        developer=DeveloperEnum.ELVER_TOBO,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    AWS_OPENSEARCH_DOMAIN_EXPOSED = cspm_method_info(
        file_name="aws",
        name="aws_opensearch_domain_exposed",
        finding=FindingEnum.F031,
        developer=DeveloperEnum.ELVER_TOBO,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    HAS_PUBLIC_INSTANCES = cspm_method_info(
        file_name="aws",
        name="has_public_instances",
        finding=FindingEnum.F073,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        origin=MethodOriginEnum.ASSERTS,
        auto_approve=True,
    )
    HAS_PUBLIC_CLUSTER = cspm_method_info(
        file_name="aws",
        name="has_public_cluster",
        finding=FindingEnum.F073,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        origin=MethodOriginEnum.ASSERTS,
        auto_approve=True,
    )
    MFA_DISABLED_FOR_USERS_WITH_CONSOLE_PASSWORD = cspm_method_info(
        file_name="aws",
        name="mfa_disabled_for_users_with_console_password",
        finding=FindingEnum.F081,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        origin=MethodOriginEnum.ASSERTS,
        auto_approve=True,
    )
    ROOT_WITHOUT_MFA = cspm_method_info(
        file_name="aws",
        name="root_without_mfa",
        finding=FindingEnum.F081,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        origin=MethodOriginEnum.ASSERTS,
        auto_approve=True,
    )
    COGNITO_HAS_MFA_DISABLED = cspm_method_info(
        file_name="aws",
        name="cognito_has_mfa_disabled",
        finding=FindingEnum.F081,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        origin=MethodOriginEnum.ASSERTS,
        cvss="CVSS:3.1/AV:N/AC:L/PR:L/UI:N/S:U/C:N/I:L/A:N/E:U/RL:O/RC:C",
        cvss_v4=("CVSS:4.0/AV:N/AC:L/AT:N/PR:L/UI:N/VC:L/VI:L/VA:N/SC:N/SI:N/SA:N/E:U"),
        auto_approve=True,
    )
    BUCKET_POLICY_HAS_SERVER_SIDE_ENCRYPTION_DISABLE = cspm_method_info(
        file_name="aws",
        name="bucket_policy_has_server_side_encryption_disable",
        finding=FindingEnum.F099,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        origin=MethodOriginEnum.ASSERTS,
        auto_approve=True,
    )
    BLOB_SERVICE_SOFT_DELETED_DISABLED = cspm_method_info(
        file_name="azure",
        name="blob_service_soft_deleted_disabled",
        finding=FindingEnum.F101,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    BLOB_CONTAINERS_SOFT_DELETED_DISABLED = cspm_method_info(
        file_name="azure",
        name="blob_containers_soft_deleted_disabled",
        finding=FindingEnum.F101,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    STORAGE_ACCOUNT_GEO_REPLICATION_DISABLED = cspm_method_info(
        file_name="azure",
        name="storage_account_geo_replication_disabled",
        finding=FindingEnum.F101,
        developer=DeveloperEnum.JOHN_PEREZ,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    AZURE_KEY_VAULT_ACCIDENTAL_PURGE_PREVENTION_IS_DISABLED = cspm_method_info(
        file_name="azure",
        name="azure_key_vault_accidental_purge_prevention_is_disabled",
        finding=FindingEnum.F101,
        developer=DeveloperEnum.ELVER_TOBO,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    AZURE_DB_POSTGRESQL_INSECURE_LOG_RETENTION_DAYS = cspm_method_info(
        file_name="azure",
        name="azure_db_postgresql_insecure_log_retention_days",
        finding=FindingEnum.F101,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    AWS_RDS_INSTANCE_BACKUP_RETENTION_PERIOD = cspm_method_info(
        file_name="aws",
        name="aws_rds_instance_backup_retention_period",
        finding=FindingEnum.F101,
        developer=DeveloperEnum.ELVER_TOBO,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    AWS_RDS_CLUSTER_BACKUP_RETENTION_PERIOD = cspm_method_info(
        file_name="aws",
        name="aws_rds_cluster_backup_retention_period",
        finding=FindingEnum.F101,
        developer=DeveloperEnum.ELVER_TOBO,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    AWS_ELASTICACHE_REPLICATION_GROUP_WO_AUTO_BACKUPS = cspm_method_info(
        file_name="aws",
        name="aws_elasticache_replication_group_wo_auto_backups",
        finding=FindingEnum.F101,
        developer=DeveloperEnum.ELVER_TOBO,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    AWS_ELASTICACHE_REPLICATION_BACKUP_RETENTION_PERIOD = cspm_method_info(
        file_name="aws",
        name="aws_elasticache_replication_backup_retention_period",
        finding=FindingEnum.F101,
        developer=DeveloperEnum.ELVER_TOBO,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    INSTANCES_IS_NOT_INSIDE_A_DB_SUBNET_GROUP = cspm_method_info(
        file_name="aws",
        name="instances_is_not_inside_a_db_subnet_group",
        finding=FindingEnum.F109,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        origin=MethodOriginEnum.ASSERTS,
        auto_approve=True,
    )
    CLUSTERS_IS_NOT_INSIDE_A_DB_SUBNET_GROUP = cspm_method_info(
        file_name="aws",
        name="clusters_is_not_inside_a_db_subnet_group",
        finding=FindingEnum.F109,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        origin=MethodOriginEnum.ASSERTS,
        auto_approve=True,
    )
    RDS_UNRESTRICTED_DB_SECURITY_GROUPS = cspm_method_info(
        file_name="aws",
        name="rds_unrestricted_db_security_groups",
        finding=FindingEnum.F109,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        origin=MethodOriginEnum.ASSERTS,
        auto_approve=True,
    )
    RDS_UNRESTRICTED_CLUSTER_SECURITY_GROUPS = cspm_method_info(
        file_name="aws",
        name="rds_unrestricted_cluster_security_groups",
        finding=FindingEnum.F109,
        developer=DeveloperEnum.ELVER_TOBO,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    AZURE_APP_SERVICE_ALLOWS_FTP_DEPLOYMENTS = cspm_method_info(
        file_name="azure",
        name="azure_app_service_allows_ftp_deployments",
        finding=FindingEnum.F148,
        developer=DeveloperEnum.ELVER_TOBO,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    STORAGE_ACCOUNT_ALLOW_PUBLIC_TRAFFIC = cspm_method_info(
        file_name="azure",
        name="storage_account_allow_public_traffic",
        finding=FindingEnum.F157,
        developer=DeveloperEnum.JOHN_PEREZ,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    AZURE_MONGODB_NSG_ALLOWS_UNRESTRICTED_ACCESS = cspm_method_info(
        file_name="azure",
        name="azure_mongodb_nsg_allows_unrestricted_access",
        finding=FindingEnum.F157,
        developer=DeveloperEnum.ELVER_TOBO,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    AZURE_MS_SQL_SERVER_NSG_ALLOWS_UNRESTRICTED_ACCESS = cspm_method_info(
        file_name="azure",
        name="azure_ms_sql_server_nsg_allows_unrestricted_access",
        finding=FindingEnum.F157,
        developer=DeveloperEnum.ELVER_TOBO,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    AZURE_MYSQL_NSG_ALLOWS_UNRESTRICTED_ACCESS = cspm_method_info(
        file_name="azure",
        name="azure_mysql_nsg_allows_unrestricted_access",
        finding=FindingEnum.F157,
        developer=DeveloperEnum.ELVER_TOBO,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    AZURE_NETBIOS_NSG_ALLOWS_UNRESTRICTED_ACCESS = cspm_method_info(
        file_name="azure",
        name="azure_netbios_nsg_allows_unrestricted_access",
        finding=FindingEnum.F157,
        developer=DeveloperEnum.ELVER_TOBO,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    AZURE_ORACLE_DATABASE_NSG_ALLOWS_UNRESTRICTED_ACCESS = cspm_method_info(
        file_name="azure",
        name="azure_oracle_database_nsg_allows_unrestricted_access",
        finding=FindingEnum.F157,
        developer=DeveloperEnum.ELVER_TOBO,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    AZURE_POSTGRESQL_DB_NSG_ALLOWS_UNRESTRICTED_ACCESS = cspm_method_info(
        file_name="azure",
        name="azure_postgresql_db_nsg_allows_unrestricted_access",
        finding=FindingEnum.F157,
        developer=DeveloperEnum.ELVER_TOBO,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    AZURE_VMS_NSG_ALLOWS_UNRESTRICTED_ACCESS = cspm_method_info(
        file_name="azure",
        name="azure_vms_nsg_allows_unrestricted_access",
        finding=FindingEnum.F157,
        developer=DeveloperEnum.ELVER_TOBO,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    AZURE_RPC_NSG_ALLOWS_UNRESTRICTED_ACCESS = cspm_method_info(
        file_name="azure",
        name="azure_rpc_nsg_allows_unrestricted_access",
        finding=FindingEnum.F157,
        developer=DeveloperEnum.ELVER_TOBO,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    AZURE_SMTP_NSG_ALLOWS_UNRESTRICTED_ACCESS = cspm_method_info(
        file_name="azure",
        name="azure_smtp_nsg_allows_unrestricted_access",
        finding=FindingEnum.F157,
        developer=DeveloperEnum.ELVER_TOBO,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    AZURE_SSH_NSG_ALLOWS_UNRESTRICTED_ACCESS = cspm_method_info(
        file_name="azure",
        name="azure_ssh_nsg_allows_unrestricted_access",
        finding=FindingEnum.F157,
        developer=DeveloperEnum.ELVER_TOBO,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    AZURE_UDP_PORTS_NSG_ALLOWS_UNRESTRICTED_ACCESS = cspm_method_info(
        file_name="azure",
        name="azure_udp_ports_nsg_allows_unrestricted_access",
        finding=FindingEnum.F157,
        developer=DeveloperEnum.ELVER_TOBO,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    NETWORK_SECURITY_GROUP_ALLOWS_PUBLIC_ACCESS = cspm_method_info(
        file_name="azure",
        name="network_security_group_allows_public_access",
        finding=FindingEnum.F158,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    NETWORK_SECURITY_GROUP_USING_PORT_RANGES = cspm_method_info(
        file_name="azure",
        name="network_security_group_using_port_ranges",
        finding=FindingEnum.F158,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    NETWORK_SECURITY_GROUP_ACCESS_ON_PORTS = cspm_method_info(
        file_name="azure",
        name="network_security_group_access_on_ports",
        finding=FindingEnum.F158,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    NETWORK_ICMP_INGRESS_NOT_RESTRICTED = cspm_method_info(
        file_name="azure",
        name="network_icmp_ingress_not_restricted",
        finding=FindingEnum.F158,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    NETWORK_FIREWALL_APP_RULE_UNRESTRICTED = cspm_method_info(
        file_name="azure",
        name="network_firewall_app_rule_unrestricted",
        finding=FindingEnum.F158,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    NETWORK_FIREWALL_NETWORK_RULE_UNRESTRICTED = cspm_method_info(
        file_name="azure",
        name="network_firewall_network_rule_unrestricted",
        finding=FindingEnum.F158,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    USERS_WITH_MULTIPLE_ACCESS_KEYS = cspm_method_info(
        file_name="aws",
        name="users_with_multiple_access_keys",
        finding=FindingEnum.F165,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        origin=MethodOriginEnum.ASSERTS,
        auto_approve=True,
    )
    POLICIES_ATTACHED_TO_USERS = cspm_method_info(
        file_name="aws",
        name="policies_attached_to_users",
        finding=FindingEnum.F165,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        origin=MethodOriginEnum.ASSERTS,
        auto_approve=True,
    )
    EKS_HAS_ENDPOINTS_PUBLICLY_ACCESSIBLE = cspm_method_info(
        file_name="aws",
        name="eks_has_endpoints_publicly_accessible",
        finding=FindingEnum.F165,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        origin=MethodOriginEnum.ASSERTS,
        cvss="CVSS:3.1/AV:N/AC:H/PR:L/UI:N/S:U/C:L/I:N/A:N/E:U/RL:O/RC:C",
        cvss_v4=("CVSS:4.0/AV:N/AT:N/AC:H/PR:L/UI:N/VC:L/VI:N/VA:N/SC:N/SI:N/SA:N/E:U"),
        auto_approve=True,
    )
    HAS_ROOT_ACTIVE_SIGNING_CERTIFICATES = cspm_method_info(
        file_name="aws",
        name="has_root_active_signing_certificates",
        finding=FindingEnum.F165,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        origin=MethodOriginEnum.ASSERTS,
        auto_approve=True,
    )
    ROOT_HAS_ACCESS_KEYS = cspm_method_info(
        file_name="aws",
        name="root_has_access_keys",
        finding=FindingEnum.F165,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        origin=MethodOriginEnum.ASSERTS,
        auto_approve=True,
    )
    RDS_HAS_PUBLIC_SNAPSHOTS = cspm_method_info(
        file_name="aws",
        name="rds_has_public_snapshots",
        finding=FindingEnum.F165,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        origin=MethodOriginEnum.ASSERTS,
        auto_approve=True,
    )
    RDS_NOT_USES_IAM_AUTHENTICATION = cspm_method_info(
        file_name="aws",
        name="rds_not_uses_iam_authentication",
        finding=FindingEnum.F165,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        origin=MethodOriginEnum.ASSERTS,
        auto_approve=True,
    )
    ELASTICACHE_USES_DEFAULT_PORT = cspm_method_info(
        file_name="aws",
        name="elasticache_uses_default_port",
        finding=FindingEnum.F165,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        origin=MethodOriginEnum.ASSERTS,
        auto_approve=True,
    )
    ELASTICACHE_IS_TRANSIT_ENCRYPTION_DISABLED = cspm_method_info(
        file_name="aws",
        name="elasticache_is_transit_encryption_disabled",
        finding=FindingEnum.F165,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        origin=MethodOriginEnum.ASSERTS,
        auto_approve=True,
    )
    SNS_IS_SERVER_SIDE_ENCRYPTION_DISABLED = cspm_method_info(
        file_name="aws",
        name="sns_is_server_side_encryption_disabled",
        finding=FindingEnum.F165,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        origin=MethodOriginEnum.ASSERTS,
        auto_approve=True,
    )
    SNS_CAN_ANYONE_PUBLISH = cspm_method_info(
        file_name="aws",
        name="sns_can_anyone_publish",
        finding=FindingEnum.F165,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        origin=MethodOriginEnum.ASSERTS,
        auto_approve=True,
    )
    SNS_CAN_ANYONE_SUBSCRIBE = cspm_method_info(
        file_name="aws",
        name="sns_can_anyone_subscribe",
        finding=FindingEnum.F165,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        origin=MethodOriginEnum.ASSERTS,
        auto_approve=True,
    )
    ELASTICACHE_IS_AT_REST_ENCRYPTION_DISABLED = cspm_method_info(
        file_name="aws",
        name="elasticache_is_at_rest_encryption_disabled",
        finding=FindingEnum.F165,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        origin=MethodOriginEnum.ASSERTS,
        auto_approve=True,
    )
    REDSHIFT_HAS_PUBLIC_CLUSTERS = cspm_method_info(
        file_name="aws",
        name="redshift_has_public_clusters",
        finding=FindingEnum.F165,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        origin=MethodOriginEnum.ASSERTS,
        auto_approve=True,
    )
    SQS_IS_ENCRYPTION_DISABLED = cspm_method_info(
        file_name="aws",
        name="sqs_is_encryption_disabled",
        finding=FindingEnum.F165,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        origin=MethodOriginEnum.ASSERTS,
        auto_approve=True,
    )
    REDSHIFT_NOT_REQUIRES_SSL = cspm_method_info(
        file_name="aws",
        name="redshift_not_requires_ssl",
        finding=FindingEnum.F165,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        origin=MethodOriginEnum.ASSERTS,
        auto_approve=True,
    )
    ACM_CERTIFICATE_EXPIRED = cspm_method_info(
        file_name="aws",
        name="acm_certificate_expired",
        finding=FindingEnum.F165,
        developer=DeveloperEnum.ELVER_TOBO,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    API_GATEWAY_CACHE_ENCRYPTION_DISABLED = cspm_method_info(
        file_name="aws",
        name="api_gateway_cache_encryption_disabled",
        finding=FindingEnum.F165,
        developer=DeveloperEnum.ELVER_TOBO,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    BACKUP_VAULT_POLICY_ALLOW_DELETE_RECOVERY_POINTS = cspm_method_info(
        file_name="aws",
        name="backup_vault_policy_allow_delete_recovery_points",
        finding=FindingEnum.F165,
        developer=DeveloperEnum.ELVER_TOBO,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    AWS_BEDROCK_GUARDRAILS_NO_SENSITIVE_INFO_FILTER = cspm_method_info(
        file_name="aws",
        name="aws_bedrock_guardrails_no_sensitive_info_filter",
        finding=FindingEnum.F165,
        developer=DeveloperEnum.ELVER_TOBO,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    AWS_EVENT_BRIDGE_DEFAULT_EVENT_BUS_EXPOSED = cspm_method_info(
        file_name="aws",
        name="aws_event_bridge_default_event_bus_exposed",
        finding=FindingEnum.F165,
        developer=DeveloperEnum.ELVER_TOBO,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    AWS_LAMBDA_FUNCTION_EXPOSED = cspm_method_info(
        file_name="aws",
        name="aws_lambda_function_exposed",
        finding=FindingEnum.F165,
        developer=DeveloperEnum.ELVER_TOBO,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    AWS_LAMBDA_URL_WITHOUT_AUTHENTICATION = cspm_method_info(
        file_name="aws",
        name="aws_lambda_url_without_authentication",
        finding=FindingEnum.F165,
        developer=DeveloperEnum.ELVER_TOBO,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    AWS_COMPREHEND_ANALYSIS_WITHOUT_ENCRYPTION = cspm_method_info(
        file_name="aws",
        name="aws_comprehend_analysis_without_encryption",
        finding=FindingEnum.F165,
        developer=DeveloperEnum.ELVER_TOBO,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    AWS_EBS_PUBLIC_SNAPSHOT = cspm_method_info(
        file_name="aws",
        name="aws_ebs_public_snapshot",
        finding=FindingEnum.F165,
        developer=DeveloperEnum.ELVER_TOBO,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    AWS_EKS_UNENCRYPTED_SECRETS = cspm_method_info(
        file_name="aws",
        name="aws_eks_unencrypted_secrets",
        finding=FindingEnum.F165,
        developer=DeveloperEnum.ELVER_TOBO,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    AWS_EMR_HAS_NOT_SECURITY_CONFIG = cspm_method_info(
        file_name="aws",
        name="aws_emr_has_not_security_config",
        finding=FindingEnum.F165,
        developer=DeveloperEnum.ELVER_TOBO,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    AWS_OPENSEARCH_WITHOUT_ENCRYPTION_AT_REST = cspm_method_info(
        file_name="aws",
        name="aws_opensearch_without_encryption_at_rest",
        finding=FindingEnum.F165,
        developer=DeveloperEnum.ELVER_TOBO,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    AWS_OPENSEARCH_DOMAIN_NODE_TO_NODE_ENCRYPTION = cspm_method_info(
        file_name="aws",
        name="aws_opensearch_domain_node_to_node_encryption",
        finding=FindingEnum.F165,
        developer=DeveloperEnum.ELVER_TOBO,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    AWS_GLUE_CATALOG_WITHOUT_ENCRYPTION_AT_REST = cspm_method_info(
        file_name="aws",
        name="aws_glue_catalog_without_encryption_at_rest",
        finding=FindingEnum.F165,
        developer=DeveloperEnum.ELVER_TOBO,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    AWS_KINESIS_STREAM_WITHOUT_ENCRYPTION_AT_REST = cspm_method_info(
        file_name="aws",
        name="aws_kinesis_stream_without_encryption_at_rest",
        finding=FindingEnum.F165,
        developer=DeveloperEnum.ELVER_TOBO,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    AWS_MQ_BROKER_PUBLICLY_ACCESSIBLE = cspm_method_info(
        file_name="aws",
        name="aws_mq_broker_publicly_accessible",
        finding=FindingEnum.F165,
        developer=DeveloperEnum.ELVER_TOBO,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    AWS_MSK_CLUSTER_IS_PUBLICLY_ACCESSIBLE = cspm_method_info(
        file_name="aws",
        name="aws_msk_cluster_is_publicly_accessible",
        finding=FindingEnum.F165,
        developer=DeveloperEnum.ELVER_TOBO,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    AWS_NEPTUNE_DB_INSTANCE_WITHOUT_ENCRYPTION_AT_REST = cspm_method_info(
        file_name="aws",
        name="aws_neptune_db_instance_without_encryption_at_rest",
        finding=FindingEnum.F165,
        developer=DeveloperEnum.ELVER_TOBO,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    AWS_ROUTE53_TRANSFER_LOCK_DISABLED = cspm_method_info(
        file_name="aws",
        name="aws_route53_transfer_lock_disabled",
        finding=FindingEnum.F165,
        developer=DeveloperEnum.ELVER_TOBO,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    AWS_SAGEMAKER_TRAINING_JOB_INTERCONTAINER_ENCRYPTION = cspm_method_info(
        file_name="aws",
        name="aws_sagemaker_training_job_intercontainer_encryption",
        finding=FindingEnum.F165,
        developer=DeveloperEnum.ELVER_TOBO,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    AWS_SAGEMAKER_NOTEBOOK_INSTANCE_ENCRYPTION = cspm_method_info(
        file_name="aws",
        name="aws_sagemaker_notebook_instance_encryption",
        finding=FindingEnum.F165,
        developer=DeveloperEnum.ELVER_TOBO,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    AWS_ATHENA_WORKGROUP_QUERY_RESULTS_NOT_ENCRYPTED = cspm_method_info(
        file_name="aws",
        name="aws_athena_workgroup_query_results_not_encrypted",
        finding=FindingEnum.F165,
        developer=DeveloperEnum.ELVER_TOBO,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    AWS_DAX_CLUSTER_WITHOUT_ENCRYPTION_AT_REST = cspm_method_info(
        file_name="aws",
        name="aws_dax_cluster_without_encryption_at_rest",
        finding=FindingEnum.F165,
        developer=DeveloperEnum.ELVER_TOBO,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    AWS_UNENCRYPTED_ECR_REPOSITORY = cspm_method_info(
        file_name="aws",
        name="aws_unencrypted_ecr_repository",
        finding=FindingEnum.F165,
        developer=DeveloperEnum.ELVER_TOBO,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    AWS_RDS_UNENCRYPTED_DB_CLUSTER_SNAPSHOT = cspm_method_info(
        file_name="aws",
        name="aws_rds_unencrypted_db_cluster_snapshot",
        finding=FindingEnum.F165,
        developer=DeveloperEnum.ELVER_TOBO,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    AWS_RDS_UNENCRYPTED_DB_SNAPSHOT = cspm_method_info(
        file_name="aws",
        name="aws_rds_unencrypted_db_snapshot",
        finding=FindingEnum.F165,
        developer=DeveloperEnum.ELVER_TOBO,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    AWS_ALB_DOES_NOT_DROP_INVALID_HEADER_FIELDS = cspm_method_info(
        file_name="aws",
        name="aws_alb_does_not_drop_invalid_header_fields",
        finding=FindingEnum.F165,
        developer=DeveloperEnum.ELVER_TOBO,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    AWS_PUBLIC_ACCESSIBLE_DMS_REPLICATION = cspm_method_info(
        file_name="aws",
        name="aws_public_accessible_dms_replication",
        finding=FindingEnum.F165,
        developer=DeveloperEnum.ELVER_TOBO,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    HAS_DEFAULT_SECURITY_GROUPS_IN_USE = cspm_method_info(
        file_name="aws",
        name="has_default_security_groups_in_use",
        finding=FindingEnum.F177,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        origin=MethodOriginEnum.ASSERTS,
        auto_approve=True,
    )
    USE_DEFAULT_SECURITY_GROUP = cspm_method_info(
        file_name="aws",
        name="use_default_security_group",
        finding=FindingEnum.F177,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        origin=MethodOriginEnum.ASSERTS,
        auto_approve=True,
    )
    AZURE_APP_SERVICE_REMOTE_DEBUGGING_ENABLED = cspm_method_info(
        file_name="azure",
        name="azure_app_service_remote_debugging_enabled",
        finding=FindingEnum.F183,
        developer=DeveloperEnum.ELVER_TOBO,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    OBJECT_VERSIONING_IS_NOT_ENABLED = cspm_method_info(
        file_name="gcp",
        name="object_versioning_is_not_enabled",
        finding=FindingEnum.F200,
        developer=DeveloperEnum.ROBIN_QUINTERO,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    LIFECYCLE_IS_NOT_DEFINED = cspm_method_info(
        file_name="gcp",
        name="lifecycle_is_not_defined",
        finding=FindingEnum.F200,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    RETENTION_POLICY_IS_NOT_CONFIGURED = cspm_method_info(
        file_name="gcp",
        name="retention_policy_is_not_configured",
        finding=FindingEnum.F200,
        developer=DeveloperEnum.ROBIN_QUINTERO,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    LOGGING_IS_NOT_ENABLED_ON_STORAGE_BUCKET = cspm_method_info(
        file_name="gcp",
        name="logging_is_not_enabled_on_storage_bucket",
        finding=FindingEnum.F200,
        developer=DeveloperEnum.ROBIN_QUINTERO,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    BLOB_CONTAINERS_ARE_PUBLIC = cspm_method_info(
        file_name="azure",
        name="blob_containers_are_public",
        finding=FindingEnum.F203,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    STORAGE_ACCOUNT_ALLOWS_PUBLIC_BLOBS = cspm_method_info(
        file_name="azure",
        name="storage_account_allows_public_blobs",
        finding=FindingEnum.F203,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    ACL_PUBLIC_BUCKETS = cspm_method_info(
        file_name="aws",
        name="acl_public_buckets",
        finding=FindingEnum.F203,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        origin=MethodOriginEnum.ASSERTS,
        auto_approve=True,
    )
    S3_BUCKETS_ALLOW_UNAUTHORIZED_PUBLIC_ACCESS = cspm_method_info(
        file_name="aws",
        name="s3_buckets_allow_unauthorized_public_access",
        finding=FindingEnum.F203,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        origin=MethodOriginEnum.ASSERTS,
        auto_approve=True,
    )
    RDS_HAS_UNENCRYPTED_STORAGE = cspm_method_info(
        file_name="aws",
        name="rds_has_unencrypted_storage",
        finding=FindingEnum.F246,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        origin=MethodOriginEnum.ASSERTS,
        auto_approve=True,
    )
    RDS_HAS_NOT_AUTOMATED_BACKUPS = cspm_method_info(
        file_name="aws",
        name="rds_has_not_automated_backups",
        finding=FindingEnum.F256,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        origin=MethodOriginEnum.ASSERTS,
        auto_approve=True,
    )
    RDS_HAS_NOT_DELETION_PROTECTION = cspm_method_info(
        file_name="aws",
        name="rds_has_not_deletion_protection",
        finding=FindingEnum.F256,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        origin=MethodOriginEnum.ASSERTS,
        auto_approve=True,
    )
    EC2_HAS_NOT_TERMINATION_PROTECTION = cspm_method_info(
        file_name="aws",
        name="ec2_has_not_termination_protection",
        finding=FindingEnum.F257,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        origin=MethodOriginEnum.ASSERTS,
        auto_approve=True,
    )
    ELB2_HAS_NOT_DELETION_PROTECTION = cspm_method_info(
        file_name="aws",
        name="elb2_has_not_deletion_protection",
        finding=FindingEnum.F258,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        origin=MethodOriginEnum.ASSERTS,
        auto_approve=True,
    )
    DYNAMODB_HAS_NOT_DELETION_PROTECTION = cspm_method_info(
        file_name="aws",
        name="dynamodb_has_not_deletion_protection",
        finding=FindingEnum.F259,
        developer=DeveloperEnum.JULIAN_GOMEZ,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    DYNAMODB_HAS_NOT_POINT_IN_TIME_RECOVERY = cspm_method_info(
        file_name="aws",
        name="dynamodb_has_not_point_in_time_recovery",
        finding=FindingEnum.F259,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        origin=MethodOriginEnum.ASSERTS,
        auto_approve=True,
    )
    HAVE_OLD_CREDS_ENABLED = cspm_method_info(
        file_name="aws",
        name="have_old_creds_enabled",
        finding=FindingEnum.F277,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        origin=MethodOriginEnum.ASSERTS,
        auto_approve=True,
    )
    HAS_OLD_SSH_PUBLIC_KEYS = cspm_method_info(
        file_name="aws",
        name="has_old_ssh_public_keys",
        finding=FindingEnum.F277,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        origin=MethodOriginEnum.ASSERTS,
        auto_approve=True,
    )
    HAVE_OLD_ACCESS_KEYS = cspm_method_info(
        file_name="aws",
        name="have_old_access_keys",
        finding=FindingEnum.F277,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        origin=MethodOriginEnum.ASSERTS,
        auto_approve=True,
    )
    S3_HAS_INSECURE_TRANSPORT = cspm_method_info(
        file_name="aws",
        name="s3_has_insecure_transport",
        finding=FindingEnum.F281,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        origin=MethodOriginEnum.ASSERTS,
        auto_approve=True,
    )
    REDIS_CACHE_INSECURE_PORT = cspm_method_info(
        file_name="azure",
        name="redis_cache_insecure_port",
        finding=FindingEnum.F281,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    REDIS_CACHE_AUTHNOTREQUIRED_ENABLED = cspm_method_info(
        file_name="azure",
        name="redis_cache_authnotrequired_enabled",
        finding=FindingEnum.F300,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    AZURE_APP_SERVICE_AUTHENTICATION_IS_NOT_ENABLED = cspm_method_info(
        file_name="azure",
        name="azure_app_service_authentication_is_not_enabled",
        finding=FindingEnum.F300,
        developer=DeveloperEnum.ELVER_TOBO,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    AZURE_DEV_PORTAL_HAS_AUTH_METHODS_INACTIVE = cspm_method_info(
        file_name="azure",
        name="azure_dev_portal_has_auth_methods_inactive",
        finding=FindingEnum.F300,
        developer=DeveloperEnum.ELVER_TOBO,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    AZURE_ROLE_BASED_ACCESS_CONTROL_ON_KEY_VAULT_IS_NOT_ENABLED = cspm_method_info(
        file_name="azure",
        name="azure_role_based_access_control_on_key_vault_is_not_enabled",
        finding=FindingEnum.F319,
        developer=DeveloperEnum.ELVER_TOBO,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    AZURE_SUBSCRIPTION_DOES_NOT_HAVE_A_LOCKING_RESOURCE_MANAGER = cspm_method_info(
        file_name="azure",
        name="azure_subscription_does_not_have_a_locking_resource_manager",
        finding=FindingEnum.F319,
        developer=DeveloperEnum.ELVER_TOBO,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    AZURE_FUNCTION_APP_WITH_ADMIN_PRIVILEGES = cspm_method_info(
        file_name="azure",
        name="azure_function_app_with_admin_privileges",
        finding=FindingEnum.F319,
        developer=DeveloperEnum.ELVER_TOBO,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    GCP_STORAGE_PUBLIC_BUCKETS = cspm_method_info(
        file_name="gcp",
        name="gcp_storage_public_buckets",
        finding=FindingEnum.F325,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    AWS_S3_PUBLIC_BUCKETS = cspm_method_info(
        file_name="aws",
        name="aws_s3_public_buckets",
        finding=FindingEnum.F325,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        origin=MethodOriginEnum.ASSERTS,
        auto_approve=True,
    )
    PRIVATE_BUCKETS_NOT_BLOCKING_PUBLIC_ACLS = cspm_method_info(
        file_name="aws",
        name="private_buckets_not_blocking_public_acls",
        finding=FindingEnum.F325,
        developer=DeveloperEnum.JOHN_PEREZ,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    KMS_HAS_MASTER_KEYS_EXPOSED_TO_EVERYONE = cspm_method_info(
        file_name="aws",
        name="kms_has_master_keys_exposed_to_everyone",
        finding=FindingEnum.F325,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        origin=MethodOriginEnum.ASSERTS,
        auto_approve=True,
    )
    IAM_HAS_WILDCARD_RESOURCE_ON_WRITE_ACTION = cspm_method_info(
        file_name="aws",
        name="iam_has_wildcard_resource_on_write_action",
        finding=FindingEnum.F325,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        origin=MethodOriginEnum.ASSERTS,
        auto_approve=True,
    )
    IAM_IS_POLICY_MISS_CONFIGURED = cspm_method_info(
        file_name="aws",
        name="iam_is_policy_miss_configured",
        finding=FindingEnum.F325,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    SQS_IS_PUBLIC = cspm_method_info(
        file_name="aws",
        name="sqs_is_public",
        finding=FindingEnum.F325,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        origin=MethodOriginEnum.ASSERTS,
        auto_approve=True,
    )
    PERMISSIVE_POLICY = cspm_method_info(
        file_name="aws",
        name="permissive_policy",
        finding=FindingEnum.F325,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        origin=MethodOriginEnum.ASSERTS,
        auto_approve=True,
    )
    GROUP_WITH_PERMISSIVE_INLINE_POLICIES = cspm_method_info(
        file_name="aws",
        name="group_with_permissive_inline_policies",
        finding=FindingEnum.F325,
        developer=DeveloperEnum.FABIO_LAGOS,
        origin=MethodOriginEnum.EXTERNAL,
        auto_approve=True,
    )
    AZURE_ROLE_ACTIONS_IS_A_WILDCARD = cspm_method_info(
        file_name="azure",
        name="azure_role_actions_is_a_wildcard",
        finding=FindingEnum.F325,
        developer=DeveloperEnum.ELVER_TOBO,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    EC2_HAS_TERMINATE_SHUTDOWN_BEHAVIOR = cspm_method_info(
        file_name="aws",
        name="ec2_has_terminate_shutdown_behavior",
        finding=FindingEnum.F333,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        origin=MethodOriginEnum.ASSERTS,
        auto_approve=True,
    )
    EC2_HAS_ASSOCIATE_PUBLIC_IP_ADDRESS = cspm_method_info(
        file_name="aws",
        name="ec2_has_associate_public_ip_address",
        finding=FindingEnum.F333,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        origin=MethodOriginEnum.ASSERTS,
        auto_approve=True,
    )
    EC2_IAM_INSTANCES_WITHOUT_PROFILE = cspm_method_info(
        file_name="aws",
        name="ec2_iam_instances_without_profile",
        finding=FindingEnum.F333,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        origin=MethodOriginEnum.ASSERTS,
        auto_approve=True,
    )
    HAS_INSTANCES_USING_UNAPPROVED_AMIS = cspm_method_info(
        file_name="aws",
        name="has_instances_using_unapproved_amis",
        finding=FindingEnum.F333,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        origin=MethodOriginEnum.ASSERTS,
        auto_approve=True,
    )
    EC2_INSTANCE_HAS_MULTIPLE_NETWORK_INTERFACES = cspm_method_info(
        file_name="aws",
        name="ec2_instance_has_multiple_network_interfaces",
        finding=FindingEnum.F333,
        developer=DeveloperEnum.ELVER_TOBO,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    AWS_EC2_INSTANCE_USING_IMDS_V1 = cspm_method_info(
        file_name="aws",
        name="aws_ec2_instance_using_imds_v1",
        finding=FindingEnum.F333,
        developer=DeveloperEnum.ELVER_TOBO,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    HAS_MODIFY_INSTANCE_ATTRIBUTE = cspm_method_info(
        file_name="aws",
        name="has_modify_instance_attribute",
        finding=FindingEnum.F333,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        origin=MethodOriginEnum.ASSERTS,
        auto_approve=True,
    )
    HAS_UNUSED_EC2_KEY_PAIRS = cspm_method_info(
        file_name="aws",
        name="has_unused_ec2_key_pairs",
        finding=FindingEnum.F333,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        origin=MethodOriginEnum.ASSERTS,
        auto_approve=True,
    )
    HAS_PUBLICLY_SHARED_AMIS = cspm_method_info(
        file_name="aws",
        name="has_publicly_shared_amis",
        finding=FindingEnum.F333,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        origin=MethodOriginEnum.ASSERTS,
        auto_approve=True,
    )
    HAS_UNENCRYPTED_SNAPSHOTS = cspm_method_info(
        file_name="aws",
        name="has_unencrypted_snapshots",
        finding=FindingEnum.F333,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        origin=MethodOriginEnum.ASSERTS,
        auto_approve=True,
    )
    HAS_UNENCRYPTED_AMIS = cspm_method_info(
        file_name="aws",
        name="has_unencrypted_amis",
        finding=FindingEnum.F333,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        origin=MethodOriginEnum.ASSERTS,
        auto_approve=True,
    )
    S3_BUCKET_VERSIONING_DISABLED = cspm_method_info(
        file_name="aws",
        name="s3_bucket_versioning_disabled",
        finding=FindingEnum.F335,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    NOT_REQUIRES_UPPERCASE = cspm_method_info(
        file_name="aws",
        name="not_requires_uppercase",
        finding=FindingEnum.F363,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        origin=MethodOriginEnum.ASSERTS,
        auto_approve=True,
    )
    NOT_REQUIRES_LOWERCASE = cspm_method_info(
        file_name="aws",
        name="not_requires_lowercase",
        finding=FindingEnum.F363,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        origin=MethodOriginEnum.ASSERTS,
        auto_approve=True,
    )
    NOT_REQUIRES_SYMBOLS = cspm_method_info(
        file_name="aws",
        name="not_requires_symbols",
        finding=FindingEnum.F363,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        origin=MethodOriginEnum.ASSERTS,
        auto_approve=True,
    )
    NOT_REQUIRES_NUMBERS = cspm_method_info(
        file_name="aws",
        name="not_requires_numbers",
        finding=FindingEnum.F363,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        origin=MethodOriginEnum.ASSERTS,
        auto_approve=True,
    )
    MIN_PASSWORD_LEN_UNSAFE = cspm_method_info(
        file_name="aws",
        name="min_password_len_unsafe",
        finding=FindingEnum.F363,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        origin=MethodOriginEnum.ASSERTS,
        auto_approve=True,
    )
    PASSWORD_REUSE_UNSAFE = cspm_method_info(
        file_name="aws",
        name="password_reuse_unsafe",
        finding=FindingEnum.F363,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        origin=MethodOriginEnum.ASSERTS,
        auto_approve=True,
    )
    PASSWORD_EXPIRATION_UNSAFE = cspm_method_info(
        file_name="aws",
        name="password_expiration_unsafe",
        finding=FindingEnum.F363,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        origin=MethodOriginEnum.ASSERTS,
        auto_approve=True,
    )
    STORAGE_ACCOUNT_NOT_ENFORCE_HTTPS = cspm_method_info(
        file_name="azure",
        name="storage_account_not_enforce_https",
        finding=FindingEnum.F372,
        developer=DeveloperEnum.JOHN_PEREZ,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    AZURE_APP_SERVICE_ALLOWS_HTTP_TRAFIC = cspm_method_info(
        file_name="azure",
        name="azure_app_service_allows_http_trafic",
        finding=FindingEnum.F372,
        developer=DeveloperEnum.ELVER_TOBO,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    ELBV2_LISTENERS_NOT_USING_HTTPS = cspm_method_info(
        file_name="aws",
        name="elbv2_listeners_not_using_https",
        finding=FindingEnum.F372,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    CFT_SERVES_CONTENT_OVER_HTTP = cspm_method_info(
        file_name="aws",
        name="cft_serves_content_over_http",
        finding=FindingEnum.F372,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    AZURE_API_NOT_ENFORCE_HTTPS = cspm_method_info(
        file_name="azure",
        name="azure_api_not_enforce_https",
        finding=FindingEnum.F372,
        developer=DeveloperEnum.ELVER_TOBO,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    AWS_CLOUDFRONT_TRAFFIC_ALLOWS_HTTP = cspm_method_info(
        file_name="aws",
        name="aws_cloudfront_traffic_allows_http",
        finding=FindingEnum.F372,
        developer=DeveloperEnum.ELVER_TOBO,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    AWS_OPENSEARCH_DOMAIN_ALLOWS_HTTP = cspm_method_info(
        file_name="aws",
        name="aws_opensearch_domain_allows_http",
        finding=FindingEnum.F372,
        developer=DeveloperEnum.ELVER_TOBO,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    AWS_CLOUDFRONT_DISTRIBUTION_VIEWER_POLICY_ALLOWS_HTTP = cspm_method_info(
        file_name="aws",
        name="aws_cloudfront_distribution_viewer_policy_allows_http",
        finding=FindingEnum.F372,
        developer=DeveloperEnum.ELVER_TOBO,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    AWS_ALB_HTTP_NOT_REDIRECTED_TO_HTTPS = cspm_method_info(
        file_name="aws",
        name="aws_alb_http_not_redirected_to_https",
        finding=FindingEnum.F372,
        developer=DeveloperEnum.ELVER_TOBO,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    AZURE_DATA_LAKE_ALLOWS_ACCESS_FROM_ANY_SOURCE = cspm_method_info(
        file_name="azure",
        name="azure_data_lake_allows_access_from_any_source",
        finding=FindingEnum.F392,
        developer=DeveloperEnum.ELVER_TOBO,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    AZURE_SYNAPSE_FIREWALL_ALLOWS_PUBLIC_ACCESS = cspm_method_info(
        file_name="azure",
        name="azure_synapse_firewall_allows_public_access",
        finding=FindingEnum.F392,
        developer=DeveloperEnum.ELVER_TOBO,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    AZ_DB_PSQL_FLEX_SERVER_FIREWALL_ALLOWS_PUBLIC_ACCESS = cspm_method_info(
        file_name="azure",
        name="az_db_psql_flex_server_firewall_allows_public_access",
        finding=FindingEnum.F392,
        developer=DeveloperEnum.ELVER_TOBO,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    AWS_CLOUDFRONT_IS_NOT_PROTECTED_WITH_WAF = cspm_method_info(
        file_name="aws",
        name="aws_cloudfront_is_not_protected_with_waf",
        finding=FindingEnum.F392,
        developer=DeveloperEnum.ELVER_TOBO,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    CLOUDTRAIL_FILES_NOT_VALIDATED = cspm_method_info(
        file_name="aws",
        name="cloudtrail_files_not_validated",
        finding=FindingEnum.F394,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    KMS_KEY_IS_KEY_ROTATION_ABSENT_OR_DISABLED = cspm_method_info(
        file_name="aws",
        name="kms_key_is_key_rotation_absent_or_disabled",
        finding=FindingEnum.F396,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    SECRETS_MANAGER_HAS_AUTOMATIC_ROTATION_DISABLED = cspm_method_info(
        file_name="aws",
        name="secrets_manager_has_automatic_rotation_disabled",
        finding=FindingEnum.F396,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        origin=MethodOriginEnum.ASSERTS,
        auto_approve=True,
    )
    CLOUDTRAIL_LOGGING_DISABLED = cspm_method_info(
        file_name="aws",
        name="cloudtrail_logging_disabled",
        finding=FindingEnum.F400,
        developer=DeveloperEnum.JOHN_PEREZ,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    VPCS_WITHOUT_FLOWLOG = cspm_method_info(
        file_name="aws",
        name="vpcs_without_flowlog",
        finding=FindingEnum.F400,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        origin=MethodOriginEnum.ASSERTS,
        auto_approve=True,
    )
    REDSHIFT_HAS_AUDIT_LOGGING_DISABLED = cspm_method_info(
        file_name="aws",
        name="redshift_has_audit_logging_disabled",
        finding=FindingEnum.F400,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        origin=MethodOriginEnum.ASSERTS,
        auto_approve=True,
    )
    REDSHIFT_IS_USER_ACTIVITY_LOGGING_DISABLED = cspm_method_info(
        file_name="aws",
        name="redshift_is_user_activity_logging_disabled",
        finding=FindingEnum.F400,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        origin=MethodOriginEnum.ASSERTS,
        auto_approve=True,
    )
    ELBV2_HAS_ACCESS_LOGGING_DISABLED = cspm_method_info(
        file_name="aws",
        name="elbv2_has_access_logging_disabled",
        finding=FindingEnum.F400,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    CLOUDFRONT_HAS_LOGGING_DISABLED = cspm_method_info(
        file_name="aws",
        name="cloudfront_has_logging_disabled",
        finding=FindingEnum.F400,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    CLOUDTRAIL_TRAILS_NOT_MULTIREGION = cspm_method_info(
        file_name="aws",
        name="cloudtrail_trails_not_multiregion",
        finding=FindingEnum.F400,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    IS_TRAIL_BUCKET_LOGGING_DISABLED = cspm_method_info(
        file_name="aws",
        name="is_trail_bucket_logging_disabled",
        finding=FindingEnum.F400,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        origin=MethodOriginEnum.ASSERTS,
        auto_approve=True,
    )
    S3_HAS_SERVER_ACCESS_LOGGING_DISABLED = cspm_method_info(
        file_name="aws",
        name="s3_has_server_access_logging_disabled",
        finding=FindingEnum.F400,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    AWS_APP_MESH_VIRTUAL_GATEWAY_ACCESS_LOGGING_DISABLED = cspm_method_info(
        file_name="aws",
        name="aws_app_mesh_virtual_gateway_access_logging_disabled",
        finding=FindingEnum.F400,
        developer=DeveloperEnum.ELVER_TOBO,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    AWS_CLOUD_TRAIL_DELIVERY_FAILING = cspm_method_info(
        file_name="aws",
        name="aws_cloud_trail_delivery_failing",
        finding=FindingEnum.F400,
        developer=DeveloperEnum.ELVER_TOBO,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    AWS_CONFIG_REFERENCING_MISSING_S3_BUCKET = cspm_method_info(
        file_name="aws",
        name="aws_config_referencing_missing_s3_bucket",
        finding=FindingEnum.F400,
        developer=DeveloperEnum.ELVER_TOBO,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    AWS_EKS_CLUSTER_LOGGING_DISABLED = cspm_method_info(
        file_name="aws",
        name="aws_eks_cluster_logging_disabled",
        finding=FindingEnum.F400,
        developer=DeveloperEnum.ELVER_TOBO,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    AWS_BEANSTALK_PERSISTENT_LOGS = cspm_method_info(
        file_name="aws",
        name="aws_beanstalk_persistent_logs",
        finding=FindingEnum.F400,
        developer=DeveloperEnum.ELVER_TOBO,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    AWS_OPENSEARCH_WITHOUT_AUDIT_LOGS = cspm_method_info(
        file_name="aws",
        name="aws_opensearch_without_audit_logs",
        finding=FindingEnum.F400,
        developer=DeveloperEnum.ELVER_TOBO,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    AWS_MQ_BROKER_LOGS_DISABLED = cspm_method_info(
        file_name="aws",
        name="aws_mq_broker_logs_disabled",
        finding=FindingEnum.F400,
        developer=DeveloperEnum.ELVER_TOBO,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    AWS_ROUTE53_DNS_QUERY_LOGGING_DISABLED = cspm_method_info(
        file_name="aws",
        name="aws_route53_dns_query_logging_disabled",
        finding=FindingEnum.F400,
        developer=DeveloperEnum.ELVER_TOBO,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    AWS_DOCUMENT_DB_WITHOUT_AUDIT_LOGS = cspm_method_info(
        file_name="aws",
        name="aws_document_db_without_audit_logs",
        finding=FindingEnum.F400,
        developer=DeveloperEnum.ELVER_TOBO,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    AWS_RDS_DB_CLUSTER_LOGS_DISABLED = cspm_method_info(
        file_name="aws",
        name="aws_rds_db_cluster_logs_disabled",
        finding=FindingEnum.F400,
        developer=DeveloperEnum.ELVER_TOBO,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    AWS_RDS_DB_INSTANCE_LOGS_DISABLED = cspm_method_info(
        file_name="aws",
        name="aws_rds_db_instance_logs_disabled",
        finding=FindingEnum.F400,
        developer=DeveloperEnum.ELVER_TOBO,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    AWS_GLOBAL_ACCELERATOR_FLOW_LOGS_DISABLED = cspm_method_info(
        file_name="aws",
        name="aws_global_accelerator_flow_logs_disabled",
        finding=FindingEnum.F400,
        developer=DeveloperEnum.ELVER_TOBO,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    AWS_NEPTUNE_DB_INSTANCE_LOGS_DISABLED = cspm_method_info(
        file_name="aws",
        name="aws_neptune_db_instance_logs_disabled",
        finding=FindingEnum.F400,
        developer=DeveloperEnum.ELVER_TOBO,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    AWS_MSK_CLUSTER_LOGGING_DISABLED = cspm_method_info(
        file_name="aws",
        name="aws_msk_cluster_logging_disabled",
        finding=FindingEnum.F400,
        developer=DeveloperEnum.ELVER_TOBO,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    NETWORK_FLOW_LOG_INSECURE_RETENTION_PERIOD = cspm_method_info(
        file_name="azure",
        name="network_flow_log_insecure_retention_period",
        finding=FindingEnum.F402,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    NETWORK_WATCHER_NOT_ENABLED = cspm_method_info(
        file_name="azure",
        name="network_watcher_not_enabled",
        finding=FindingEnum.F402,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    AZURE_KEY_VAULT_SOFT_DELETE_RETENTION = cspm_method_info(
        file_name="azure",
        name="azure_key_vault_soft_delete_retention",
        finding=FindingEnum.F402,
        developer=DeveloperEnum.ELVER_TOBO,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    UNIFORM_BUCKET_LEVEL_ACCESS_IS_DISABLED = cspm_method_info(
        file_name="gcp",
        name="uniform_bucket_level_access_is_disabled",
        finding=FindingEnum.F405,
        developer=DeveloperEnum.ROBIN_QUINTERO,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    EFS_IS_ENCRYPTION_DISABLED = cspm_method_info(
        file_name="aws",
        name="efs_is_encryption_disabled",
        finding=FindingEnum.F406,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    EBS_HAS_ENCRYPTION_DISABLED = cspm_method_info(
        file_name="aws",
        name="ebs_has_encryption_disabled",
        finding=FindingEnum.F407,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    AWS_WORKSPACES_HAS_VOLUME_ENCRYPTION_DISABLED = cspm_method_info(
        file_name="aws",
        name="aws_workspaces_has_volume_encryption_disabled",
        finding=FindingEnum.F407,
        developer=DeveloperEnum.ELVER_TOBO,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    REDSHIFT_HAS_ENCRYPTION_DISABLED = cspm_method_info(
        file_name="aws",
        name="redshift_has_encryption_disabled",
        finding=FindingEnum.F433,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        origin=MethodOriginEnum.ASSERTS,
        auto_approve=True,
    )
    REPORT_INSPECTOR2_EC2_VULNERABILITIES = cspm_method_info(
        file_name="sbom/aws",
        name="report_inspector2_ec2_vulnerabilities",
        finding=FindingEnum.F435,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    REPORT_INSPECTOR2_ECR_VULNERABILITIES = cspm_method_info(
        file_name="sbom/aws",
        name="report_inspector2_ecr_vulnerabilities",
        finding=FindingEnum.F435,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    REPORT_INSPECTOR2_LAMBDA_VULNERABILITIES = cspm_method_info(
        file_name="sbom/aws",
        name="report_inspector2_lambda_vulnerabilities",
        finding=FindingEnum.F435,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    NETWORK_OUT_OF_DATE_OWASP_RULES = cspm_method_info(
        file_name="azure",
        name="network_out_of_date_owasp_rules",
        finding=FindingEnum.F446,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    CONTAINER_REGISTRY_IS_NOT_USING_REPLICATION = cspm_method_info(
        file_name="azure",
        name="container_registry_is_not_using_replication",
        finding=FindingEnum.F446,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    AKS_IS_NOT_USING_LATEST_VERSION = cspm_method_info(
        file_name="azure",
        name="aks_is_not_using_latest_version",
        finding=FindingEnum.F446,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    AKS_HAS_ENABLE_LOCAL_ACCOUNTS = cspm_method_info(
        file_name="azure",
        name="aks_has_enable_local_accounts",
        finding=FindingEnum.F446,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    AKS_HAS_KUBENET_NETWORK_PLUGIN = cspm_method_info(
        file_name="azure",
        name="aks_has_kubenet_network_plugin",
        finding=FindingEnum.F446,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    AKS_HAS_RBAC_DISABLED = cspm_method_info(
        file_name="azure",
        name="aks_has_rbac_disabled",
        finding=FindingEnum.F446,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    AKS_API_SERVER_ALLOWS_PUBLIC_ACCESS = cspm_method_info(
        file_name="azure",
        name="aks_api_server_allows_public_access",
        finding=FindingEnum.F446,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    STORAGE_NOT_ENABLED_INFRASTRUCTURE_ENCRYPTION = cspm_method_info(
        file_name="azure",
        name="storage_not_enabled_infrastructure_encryption",
        finding=FindingEnum.F446,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    CHECK_SSH_AUTHENTICATION = cspm_method_info(
        file_name="azure",
        name="check_ssh_authentication",
        finding=FindingEnum.F446,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    AZURE_VM_ENCRYPTION_AT_HOST_DISABLED = cspm_method_info(
        file_name="azure",
        name="azure_vm_encryption_at_host_disabled",
        finding=FindingEnum.F446,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    REDIS_CACHE_PUBLIC_NETWORK_ACCESS_ENABLED = cspm_method_info(
        file_name="azure",
        name="redis_cache_public_network_access_enabled",
        finding=FindingEnum.F446,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    REDIS_CACHE_FIREWALL_ALLOWS_PUBLIC_ACCESS = cspm_method_info(
        file_name="azure",
        name="redis_cache_firewall_allows_public_access",
        finding=FindingEnum.F446,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    AZ_SUBSCRIPTION_NOT_ALLOWED_RESOURCE_TYPES_POLICY = cspm_method_info(
        file_name="azure",
        name="az_subscription_not_allowed_resource_types_policy",
        finding=FindingEnum.F446,
        developer=DeveloperEnum.ELVER_TOBO,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    CONTAINER_REGISTRY_ADMIN_USER_ENABLED = cspm_method_info(
        file_name="azure",
        name="container_registry_admin_user_enabled",
        finding=FindingEnum.F446,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    NETWORK_APPLICATION_GATEWAY_WAF_IS_DISABLED = cspm_method_info(
        file_name="azure",
        name="network_application_gateway_waf_is_disabled",
        finding=FindingEnum.F446,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    AZURE_APP_SERVICE_HTTP2_IS_DISABLED = cspm_method_info(
        file_name="azure",
        name="azure_app_service_http2_is_disabled",
        finding=FindingEnum.F446,
        developer=DeveloperEnum.ELVER_TOBO,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    AZURE_APP_SERVICE_DOES_NOT_USE_A_MANAGED_IDENTITY = cspm_method_info(
        file_name="azure",
        name="azure_app_service_does_not_use_a_managed_identity",
        finding=FindingEnum.F446,
        developer=DeveloperEnum.ELVER_TOBO,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    AZURE_FUNCTION_APP_LOGGING_IS_DISABLED = cspm_method_info(
        file_name="azure",
        name="azure_function_app_logging_is_disabled",
        finding=FindingEnum.F446,
        developer=DeveloperEnum.ELVER_TOBO,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    AZURE_KEYS_EXPIRATION_DATE_IS_NOT_ENABLED = cspm_method_info(
        file_name="azure",
        name="azure_keys_expiration_date_is_not_enabled",
        finding=FindingEnum.F446,
        developer=DeveloperEnum.ELVER_TOBO,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    AZURE_SECRET_EXPIRATION_DATE_IS_NOT_ENABLED = cspm_method_info(
        file_name="azure",
        name="azure_secret_expiration_date_is_not_enabled",
        finding=FindingEnum.F446,
        developer=DeveloperEnum.ELVER_TOBO,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    AZURE_APP_SERVICE_ALWAYS_ON_IS_NOT_ENABLED = cspm_method_info(
        file_name="azure",
        name="azure_app_service_always_on_is_not_enabled",
        finding=FindingEnum.F446,
        developer=DeveloperEnum.ELVER_TOBO,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    AZURE_BATCH_JOBS_RUNS_IN_ADMIN_MODE = cspm_method_info(
        file_name="azure",
        name="azure_batch_jobs_runs_in_admin_mode",
        finding=FindingEnum.F446,
        developer=DeveloperEnum.ELVER_TOBO,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    AZURE_DB_POSTGRESQL_CONNECTION_THROTTLING_DISABLED = cspm_method_info(
        file_name="azure",
        name="azure_db_postgresql_connection_throttling_disabled",
        finding=FindingEnum.F446,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    AZURE_DB_POSTGRESQL_LOG_SETTINGS_DISABLED = cspm_method_info(
        file_name="azure",
        name="azure_db_postgresql_log_settings_disabled",
        finding=FindingEnum.F446,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    AZURE_DB_POSTGRESQL_FIREWALL_ALLOWS_PUBLIC_ACCESS = cspm_method_info(
        file_name="azure",
        name="azure_db_postgresql_firewall_allows_public_access",
        finding=FindingEnum.F446,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    AZURE_DB_MYSQL_FIREWALL_ALLOWS_PUBLIC_ACCESS = cspm_method_info(
        file_name="azure",
        name="azure_db_mysql_firewall_allows_public_access",
        finding=FindingEnum.F446,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    AZURE_DB_SQL_FIREWALL_ALLOWS_PUBLIC_ACCESS = cspm_method_info(
        file_name="azure",
        name="azure_db_sql_firewall_allows_public_access",
        finding=FindingEnum.F446,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    AZURE_DB_SQL_EXTENDED_AUDIT_DISABLED = cspm_method_info(
        file_name="azure",
        name="azure_db_sql_extended_audit_disabled",
        finding=FindingEnum.F446,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    AZURE_DB_SQL_INSECURE_AUDIT_RETENTION_PERIOD = cspm_method_info(
        file_name="azure",
        name="azure_db_sql_insecure_audit_retention_period",
        finding=FindingEnum.F446,
        developer=DeveloperEnum.LUIS_SAAVEDRA,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    AZURE_FUNCTION_APP_USE_NOT_HOST_KEYS = cspm_method_info(
        file_name="azure",
        name="azure_function_app_use_not_host_keys",
        finding=FindingEnum.F446,
        developer=DeveloperEnum.ELVER_TOBO,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    AZURE_PUBLICLY_EXPOSED_FUNCT_APP = cspm_method_info(
        file_name="azure",
        name="azure_publicly_exposed_funct_app",
        finding=FindingEnum.F446,
        developer=DeveloperEnum.ELVER_TOBO,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    AZURE_APP_SERVICE_MUTUAL_TLS_IS_DISABLED = cspm_method_info(
        file_name="azure",
        name="azure_app_service_mutual_tls_is_disabled",
        finding=FindingEnum.F446,
        developer=DeveloperEnum.ELVER_TOBO,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    AZURE_SUBSCRIPTION_HAS_AT_LEAST_TWO_OWNERS = cspm_method_info(
        file_name="azure",
        name="azure_subscription_has_at_least_two_owners",
        finding=FindingEnum.F446,
        developer=DeveloperEnum.ELVER_TOBO,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    AZURE_SEARCH_SERVICE_DOES_NOT_USE_A_MANAGED_IDENTITY = cspm_method_info(
        file_name="azure",
        name="azure_search_service_does_not_use_a_managed_identity",
        finding=FindingEnum.F446,
        developer=DeveloperEnum.ELVER_TOBO,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    AZURE_SEARCH_SERVICE_HAS_INSUFFICIENT_REPLICAS_CONFIGURED = cspm_method_info(
        file_name="azure",
        name="azure_search_service_has_insufficient_replicas_configured",
        finding=FindingEnum.F446,
        developer=DeveloperEnum.ELVER_TOBO,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    AZURE_API_MGMT_SVC_DOES_NOT_USE_A_MANAGED_IDENTITY = cspm_method_info(
        file_name="azure",
        name="azure_api_mgmt_svc_does_not_use_a_managed_identity",
        finding=FindingEnum.F446,
        developer=DeveloperEnum.ELVER_TOBO,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    AZURE_KEY_VAULT__ADMIN_PERMISSIONS_ON_KEYS = cspm_method_info(
        file_name="azure",
        name="azure_key_vault__admin_permissions_on_keys",
        finding=FindingEnum.F446,
        developer=DeveloperEnum.ELVER_TOBO,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    AZURE_SEARCH_SERVICE__PUBLIC_NETWORK_ACCESS_IS_ENABLED = cspm_method_info(
        file_name="azure",
        name="azure_search_service__public_network_access_is_enabled",
        finding=FindingEnum.F446,
        developer=DeveloperEnum.ELVER_TOBO,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    AZURE_COSMOS_DB__PUBLIC_NETWORK_ACCESS_IS_ENABLED = cspm_method_info(
        file_name="azure",
        name="azure_cosmos_db__public_network_access_is_enabled",
        finding=FindingEnum.F446,
        developer=DeveloperEnum.ELVER_TOBO,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    AZURE_DATAFACTORY__PUBLIC_NETWORK_ACCESS_IS_ENABLED = cspm_method_info(
        file_name="azure",
        name="azure_datafactory__public_network_access_is_enabled",
        finding=FindingEnum.F446,
        developer=DeveloperEnum.ELVER_TOBO,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    AZURE_API_MGMT_SVC__PUBLIC_NETWORK_ACCESS_IS_ENABLED = cspm_method_info(
        file_name="azure",
        name="azure_api_mgmt_svc__public_network_access_is_enabled",
        finding=FindingEnum.F446,
        developer=DeveloperEnum.ELVER_TOBO,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    AZURE_KEY_VAULT__PUBLIC_NETWORK_ACCESS_IS_ENABLED = cspm_method_info(
        file_name="azure",
        name="azure_key_vault__public_network_access_is_enabled",
        finding=FindingEnum.F446,
        developer=DeveloperEnum.ELVER_TOBO,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    AZURE_API_MGMT_USES_THE_TRIPLE_DES_CIPHER_ALGORITHM = cspm_method_info(
        file_name="azure",
        name="azure_api_mgmt_uses_the_triple_des_cipher_algorithm",
        finding=FindingEnum.F446,
        developer=DeveloperEnum.ELVER_TOBO,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    AZ_DB_PSQL_FLEX_SERVER_CONNECTION_THROTTLING_DISABLED = cspm_method_info(
        file_name="azure",
        name="az_db_psql_flex_server_connection_throttling_disabled",
        finding=FindingEnum.F446,
        developer=DeveloperEnum.ELVER_TOBO,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    AZURE_SQL_DB_TRANSPARENT_ENCRYPTION_IS_DISABLED = cspm_method_info(
        file_name="azure",
        name="azure_sql_db_transparent_encryption_is_disabled",
        finding=FindingEnum.F446,
        developer=DeveloperEnum.ELVER_TOBO,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
    AZURE_VM_SCALE_SET_DOES_NOT_HAVE_ZONAL_REDUNDANCY = cspm_method_info(
        file_name="azure",
        name="azure_vm_scale_set_does_not_have_zonal_redundancy",
        finding=FindingEnum.F446,
        developer=DeveloperEnum.ELVER_TOBO,
        origin=MethodOriginEnum.DEVELOPER,
        auto_approve=True,
    )
