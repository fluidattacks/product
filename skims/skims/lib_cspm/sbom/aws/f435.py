from contextlib import (
    suppress,
)

from lib_cspm.aws.model import (
    AwsMethodsTuple,
    Location,
)
from lib_cspm.aws.shield import (
    SHIELD,
)
from lib_cspm.aws.utils import (
    build_vulnerabilities,
    run_boto3_fun,
)
from lib_cspm.methods_enum import (
    MethodsEnumCSPM as MethodsEnum,
)
from model.core import (
    AwsCredentials,
    Vulnerabilities,
)


async def get_paginated_items_for_resource(
    credentials: AwsCredentials,
    region: str,
    resource_type_filter: list[dict[str, str]],
) -> list:
    """Get all items in paginated API calls."""
    pools: list[dict] = []
    args: dict = {
        "credentials": credentials,
        "region": region,
        "service": "inspector2",
        "function": "list_findings",
        "parameters": {
            "maxResults": 50,
            "filterCriteria": {
                "findingStatus": [{"comparison": "EQUALS", "value": "ACTIVE"}],
                "findingType": [{"comparison": "EQUALS", "value": "PACKAGE_VULNERABILITY"}],
                "resourceType": resource_type_filter,
            },
        },
    }
    data = await run_boto3_fun(**args)
    object_name = "findings"
    pools += data.get(object_name, [])
    next_token = data.get("nextToken", None)
    pagination_number = 0
    while next_token and pagination_number <= 100:
        args["parameters"]["nextToken"] = next_token
        data = await run_boto3_fun(**args)
        pools += data.get(object_name, [])
        next_token = data.get("nextToken", None)
        pagination_number += 1
    return pools


@SHIELD
async def report_inspector2_ec2_vulnerabilities(
    credentials: AwsCredentials,
    region: str,
) -> tuple[Vulnerabilities, bool]:
    response = await get_paginated_items_for_resource(
        credentials,
        region,
        [{"comparison": "EQUALS", "value": "AWS_EC2_INSTANCE"}],
    )
    method = MethodsEnum.REPORT_INSPECTOR2_EC2_VULNERABILITIES
    vulns: Vulnerabilities = ()
    with suppress(KeyError):
        for finding in response:
            image_id = finding["resources"][0]["details"]["awsEc2Instance"]["imageId"]
            arn = f"arn:aws:ec2:{region}:{finding['awsAccountId']}:ami-id/{image_id}"

            for package in finding["packageVulnerabilityDetails"]["vulnerablePackages"]:
                locations: list[Location] = []
                vulnerable_package = package["name"] + ":" + package["version"]
                vulnerability_id = finding["packageVulnerabilityDetails"]["vulnerabilityId"]
                if finding["status"] == "ACTIVE":
                    locations = [
                        Location(
                            access_patterns=("/name",),
                            arn=arn,
                            values=(vulnerable_package,),
                            method=method,
                            desc_params={
                                "product": package["name"],
                                "version": package["version"],
                                "cve": vulnerability_id,
                            },
                        ),
                    ]
                    vulns += build_vulnerabilities(
                        locations=locations,
                        method=method,
                        aws_response=package,
                    )

    return (vulns, True)


@SHIELD
async def report_inspector2_ecr_vulnerabilities(
    credentials: AwsCredentials,
    region: str,
) -> tuple[Vulnerabilities, bool]:
    response = await get_paginated_items_for_resource(
        credentials,
        region,
        [
            {"comparison": "EQUALS", "value": "AWS_ECR_REPOSITORY"},
            {
                "comparison": "EQUALS",
                "value": "AWS_ECR_CONTAINER_IMAGE",
            },
        ],
    )
    method = MethodsEnum.REPORT_INSPECTOR2_ECR_VULNERABILITIES
    vulns: Vulnerabilities = ()
    with suppress(KeyError):
        for finding in response:
            ecr = finding["resources"][0]["details"]["awsEcrContainerImage"]
            arn = (
                f"arn:aws:ecr:{region}:{finding['awsAccountId']}:repository/"
                f"{ecr['repositoryName']}/{ecr['imageHash']}"
            )

            for package in finding["packageVulnerabilityDetails"]["vulnerablePackages"]:
                locations: list[Location] = []
                vulnerable_package = package["name"] + ":" + package["version"]
                vulnerability_id = finding["packageVulnerabilityDetails"]["vulnerabilityId"]
                if finding["status"] == "ACTIVE":
                    locations = [
                        Location(
                            access_patterns=("/name",),
                            arn=arn,
                            values=(vulnerable_package,),
                            method=method,
                            desc_params={
                                "product": package["name"],
                                "version": package["version"],
                                "cve": vulnerability_id,
                            },
                        ),
                    ]
                    vulns += build_vulnerabilities(
                        locations=locations,
                        method=method,
                        aws_response=package,
                    )

    return (vulns, True)


@SHIELD
async def report_inspector2_lambda_vulnerabilities(
    credentials: AwsCredentials,
    region: str,
) -> tuple[Vulnerabilities, bool]:
    response = await get_paginated_items_for_resource(
        credentials,
        region,
        [{"comparison": "EQUALS", "value": "AWS_LAMBDA_FUNCTION"}],
    )
    method = MethodsEnum.REPORT_INSPECTOR2_LAMBDA_VULNERABILITIES
    vulns: Vulnerabilities = ()
    with suppress(KeyError):
        for finding in response:
            lambda_fun = finding["resources"][0]["details"]["awsLambdaFunction"]["functionName"]
            arn = f"arn:aws:lambda:{region}:{finding['awsAccountId']}:lambda/{lambda_fun}"

            for package in finding["packageVulnerabilityDetails"]["vulnerablePackages"]:
                locations: list[Location] = []
                vulnerable_package = package["name"] + ":" + package["version"]
                vulnerability_id = finding["packageVulnerabilityDetails"]["vulnerabilityId"]
                if finding["status"] == "ACTIVE":
                    locations = [
                        Location(
                            access_patterns=("/name",),
                            arn=arn,
                            values=(vulnerable_package,),
                            method=method,
                            desc_params={
                                "product": package["name"],
                                "version": package["version"],
                                "cve": vulnerability_id,
                            },
                        ),
                    ]
                    vulns += build_vulnerabilities(
                        locations=locations,
                        method=method,
                        aws_response=package,
                    )

    return (vulns, True)


CHECKS: AwsMethodsTuple = (
    report_inspector2_ec2_vulnerabilities,
    report_inspector2_ecr_vulnerabilities,
    report_inspector2_lambda_vulnerabilities,
)
