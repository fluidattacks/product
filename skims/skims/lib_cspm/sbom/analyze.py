from typing import TYPE_CHECKING

import ctx
from aioextensions import (
    resolve,
)
from lib_cspm.aws.analyze import (
    get_active_aws_regions,
)
from lib_cspm.aws.model import (
    AwsMethodsTuple,
)
from lib_cspm.aws.utils import (
    exclude_reports,
    get_arns_that_are_excluded,
)
from lib_cspm.sbom.aws import (
    f435,
)
from model.core import (
    AwsCredentials,
    FindingEnum,
    Vulnerabilities,
    Vulnerability,
)
from utils.logs import (
    log_blocking,
)
from utils.state import (
    VulnerabilitiesEphemeralStore,
)

if TYPE_CHECKING:
    from collections.abc import (
        Awaitable,
    )

AWS_SBOM_CHECKS: dict[FindingEnum, AwsMethodsTuple] = {
    FindingEnum.F435: f435.CHECKS,
}


def handle_vulnerabilities(
    vulnerabilities: tuple[Vulnerability, ...],
    stores: VulnerabilitiesEphemeralStore,
) -> None:
    stores.store_vulns(vulnerabilities)


async def analyze_cloud(
    credentials: AwsCredentials,
    stores: VulnerabilitiesEphemeralStore,
    finding_checks: set[FindingEnum],
) -> set[str]:
    active_regions = await get_active_aws_regions(credentials)
    excluded_arns = await get_arns_that_are_excluded(credentials, active_regions)
    methods_with_errors: set[str] = set()
    for finding, finding_methods in AWS_SBOM_CHECKS.items():
        if finding not in finding_checks:
            continue

        futures: list[Awaitable[tuple[Vulnerabilities, bool | str]]] = []
        for method in finding_methods:
            futures += [
                method(credentials, region)  # type: ignore[call-arg]
                for region in active_regions
            ]

        for aws_method in resolve(futures, workers=ctx.CPU_CORES):
            method_vulns, method_succeeded = await aws_method
            filtered_reports = exclude_reports(method_vulns, excluded_arns)
            handle_vulnerabilities(filtered_reports, stores)

            if isinstance(method_succeeded, str):
                methods_with_errors.add(method_succeeded)

    return methods_with_errors


async def analyze(
    *,
    credentials: AwsCredentials,
    stores: VulnerabilitiesEphemeralStore,
) -> None | dict[str, list[str]]:
    if not any(finding in ctx.SKIMS_CONFIG.checks for finding in AWS_SBOM_CHECKS):
        return None

    log_blocking("info", "Starting AWS SBOM analysis on credentials")

    error_methods = await analyze_cloud(credentials, stores, ctx.SKIMS_CONFIG.checks)

    log_blocking("info", "AWS SBOM analysis completed!")
    return {"aws_errors": list(error_methods)}
