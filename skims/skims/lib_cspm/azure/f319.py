from typing import (
    Any,
)

from lib_cspm.azure.azure_clients.app_service import (
    run_function_app_with_roles,
)
from lib_cspm.azure.azure_clients.authorization import (
    run_azure_subscription_role_assignments,
    run_azure_subscription_roles,
)
from lib_cspm.azure.azure_clients.common import (
    AzureClientSecretCredential as ClientSecretCredential,
)
from lib_cspm.azure.azure_clients.common import (
    AzureMethodsTuple,
    run_azure_client,
)
from lib_cspm.azure.azure_clients.key_vault import (
    run_azure_key_vault,
)
from lib_cspm.azure.shield import (
    shield_cspm_azure_decorator,
)
from lib_cspm.azure.utils import (
    Location,
    build_vulnerabilities,
)
from lib_cspm.methods_enum import (
    MethodsEnumCSPM as MethodsEnum,
)
from model.core import (
    AzureCredentials,
    Vulnerabilities,
)


@shield_cspm_azure_decorator
async def azure_role_based_access_control_on_key_vault_is_not_enabled(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.AZURE_ROLE_BASED_ACCESS_CONTROL_ON_KEY_VAULT_IS_NOT_ENABLED
    vulns: Vulnerabilities = ()
    vaults = await run_azure_client(run_azure_key_vault, credentials, client_credential)
    rbac_key = "enable_rbac_authorization"
    for vault in vaults:
        vault_properties = vault.get("properties", {})
        enable_rbac_authorization = vault_properties.get(rbac_key)
        if rbac_key in vault_properties and not enable_rbac_authorization:
            locations = [
                Location(
                    id=vault["id"],
                    access_patterns=(f"/properties/{rbac_key}",),
                    values=(enable_rbac_authorization,),
                    method=method,
                ),
            ]
            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                azure_response=vault,
            )

    return (vulns, True)


def _filter_roles_with_lock_actions(roles: list[dict[str, Any]]) -> tuple[list[Any], ...]:
    locks_all, locks_read, locks_write, locks_delete = [], [], [], []
    for role in roles:
        permissions = role.get("permissions", [])
        role_id = role.get("id", "")
        for permission in permissions:
            actions = permission.get("actions", [])
            if "Microsoft.Authorization/locks/*" in actions:
                locks_all.append(role_id)
                continue
            if "Microsoft.Authorization/locks/read" in actions:
                locks_read.append(role_id)
            if "Microsoft.Authorization/locks/write" in actions:
                locks_write.append(role_id)
            if "Microsoft.Authorization/locks/delete" in actions:
                locks_delete.append(role_id)
    return (
        locks_all,
        locks_read,
        locks_write,
        locks_delete,
    )


def _verify_role_assignations(
    role_assignments: list[dict[str, Any]],
    roles_with_lock: tuple[list[str], ...],
) -> tuple[bool, ...]:
    (locks_all, locks_read, locks_write, locks_delete) = roles_with_lock
    locks_read_vuln, locks_write_vuln, locks_delete_vuln = False, False, False
    for role_assignment in role_assignments:
        role_definition_id = role_assignment.get("role_definition_id", "")
        if role_definition_id in locks_all:
            return (True, True, True)
        if role_definition_id in locks_read:
            locks_read_vuln = True
        if role_definition_id in locks_write:
            locks_write_vuln = True
        if role_definition_id in locks_delete:
            locks_delete_vuln = True
    return (
        locks_read_vuln,
        locks_write_vuln,
        locks_delete_vuln,
    )


@shield_cspm_azure_decorator
async def azure_subscription_does_not_have_a_locking_resource_manager(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.AZURE_SUBSCRIPTION_DOES_NOT_HAVE_A_LOCKING_RESOURCE_MANAGER
    roles = await run_azure_client(
        run_azure_subscription_roles,
        credentials,
        client_credential,
    )
    roles_with_lock = _filter_roles_with_lock_actions(roles)
    role_assignments = await run_azure_client(
        run_azure_subscription_role_assignments,
        credentials,
        client_credential,
    )
    (
        locks_read_vuln,
        locks_write_vuln,
        locks_delete_vuln,
    ) = _verify_role_assignations(role_assignments, roles_with_lock)
    locations = []
    subscription_id = credentials.subscription_id
    access_pattern = "/subscriptions/subscription"
    if not locks_read_vuln:
        locations.append(
            Location(
                values=("/Microsoft.Authorization/locks/read",),
                id=f"subscriptions/{subscription_id}/Microsoft.Authorization/locks/read",
                access_patterns=(access_pattern,),
                method=method,
                desc_params={"action": "read"},
            ),
        )
    if not locks_write_vuln:
        locations.append(
            Location(
                values=("/Microsoft.Authorization/locks/write",),
                id=f"subscriptions/{subscription_id}/Microsoft.Authorization/locks/write",
                access_patterns=(access_pattern,),
                method=method,
                desc_params={"action": "write"},
            ),
        )
    if not locks_delete_vuln:
        locations.append(
            Location(
                values=("/Microsoft.Authorization/locks/delete",),
                id=f"subscriptions/{subscription_id}/Microsoft.Authorization/locks/delete",
                access_patterns=(access_pattern,),
                method=method,
                desc_params={"action": "delete"},
            ),
        )

    vulns = build_vulnerabilities(
        locations=locations,
        method=method,
        azure_response={"subscriptions": {"subscription": subscription_id}},
    )
    return (vulns, True)


@shield_cspm_azure_decorator
async def azure_function_app_with_admin_privileges(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.AZURE_FUNCTION_APP_WITH_ADMIN_PRIVILEGES
    vulns: Vulnerabilities = ()
    azure_admin_roles = [
        "Owner",
        "Contributor",
        "Role Based Access Control Administrator",
        "User Access Administrator",
        "Access Review Operator Service Role",
    ]
    function_apps = await run_azure_client(
        run_function_app_with_roles,
        credentials,
        client_credential,
    )
    for function_app in function_apps:
        roles = function_app.get("roles", [])
        function_id = function_app.get("id", "")
        for role in roles:
            if role in azure_admin_roles:
                function_app.pop("roles")
                locations = [
                    Location(
                        id=f"{function_id}-{role}",
                        access_patterns=("/id",),
                        values=(f"Admin role: {role}",),
                        method=method,
                    ),
                ]
                vulns += build_vulnerabilities(
                    locations=locations,
                    method=method,
                    azure_response=function_app,
                )

    return (vulns, True)


CHECKS: AzureMethodsTuple = (
    azure_function_app_with_admin_privileges,
    azure_role_based_access_control_on_key_vault_is_not_enabled,
    azure_subscription_does_not_have_a_locking_resource_manager,
)
