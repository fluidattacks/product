import aiocache
from aiocache.serializers import (
    PickleSerializer,
)
from azure.identity.aio._credentials.client_secret import (
    ClientSecretCredential,
)
from azure.mgmt.resource.policy.aio import (
    PolicyClient,
)
from azure.mgmt.resource.resources.v2022_09_01.aio import (
    ResourceManagementClient,
)
from lib_cspm.azure.azure_clients.common import (
    custom_key_builder,
)
from model.core import (
    AzureCredentials,
)


@aiocache.cached(
    serializer=PickleSerializer(),
    key_builder=custom_key_builder,
)
async def list_resource_groups_names(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> list[str]:
    async with ResourceManagementClient(
        client_credential,
        credentials.subscription_id,
    ) as resource_client:
        return [
            str(resource_group.name)
            async for resource_group in resource_client.resource_groups.list()
        ]


@aiocache.cached(
    serializer=PickleSerializer(),
    key_builder=custom_key_builder,
)
async def list_subscription_policy_definitions(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> list[str]:
    policy_definitions = []
    async with PolicyClient(client_credential, credentials.subscription_id) as policy_client:
        async for policy_definition in policy_client.policy_definitions.list():
            policy_definitions.append(policy_definition.as_dict())  # noqa: PERF401
    return policy_definitions


@aiocache.cached(
    serializer=PickleSerializer(),
    key_builder=custom_key_builder,
)
async def run_subscription_assigned_policies(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> list[str]:
    policy_definitions = await list_subscription_policy_definitions(
        credentials,
        client_credential,
    )
    policies_name_by_id = {
        policy.get("id"): policy.get("display_name") for policy in policy_definitions
    }
    policy_assignments = []
    async with PolicyClient(client_credential, credentials.subscription_id) as policy_client:
        async for policy_assignment in policy_client.policy_assignments.list():
            policy_definition_id = policy_assignment.policy_definition_id
            display_name = policies_name_by_id.get(policy_definition_id)
            policy_assignment_dict = policy_assignment.as_dict()
            policy_assignment_dict["policy_definition_name"] = display_name
            policy_assignments.append(policy_assignment_dict)
    return policy_assignments
