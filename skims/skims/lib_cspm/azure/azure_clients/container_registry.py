from typing import (
    Any,
)

import aiocache
from aiocache.serializers import (
    PickleSerializer,
)
from azure.identity.aio._credentials.client_secret import (
    ClientSecretCredential,
)
from azure.mgmt.containerregistry.aio import (
    _container_registry_management_client as _crc_client,
)
from lib_cspm.azure.azure_clients.common import (
    custom_key_builder,
)
from model.core import (
    AzureCredentials,
)


@aiocache.cached(
    serializer=PickleSerializer(),
    key_builder=custom_key_builder,
)
async def run_azure_container_registries(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> list[dict[str, Any]]:
    container_registries = []
    async with _crc_client.ContainerRegistryManagementClient(
        client_credential,
        credentials.subscription_id,
    ) as container_client:
        async for container in container_client.registries.list():
            container_registries.append(container.as_dict())  # noqa: PERF401
    return container_registries


@aiocache.cached(
    serializer=PickleSerializer(),
    key_builder=custom_key_builder,
)
async def run_azure_container_registries_replication(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> list[dict[str, Any]]:
    container_registries = await run_azure_container_registries(credentials, client_credential)
    replications_by_container = []
    async with _crc_client.ContainerRegistryManagementClient(
        client_credential,
        credentials.subscription_id,
    ) as container_client:
        for container in container_registries:
            group_name = container["id"].split("/")[4]
            replications = []
            async for replication in container_client.replications.list(
                registry_name=container["name"],
                resource_group_name=group_name,
            ):
                replications.append(replication.as_dict())  # noqa: PERF401
            replications_by_container.append({"id": container["id"], "replications": replications})
    return replications_by_container
