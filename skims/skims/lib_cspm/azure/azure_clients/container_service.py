from typing import (
    Any,
)

import aiocache
from aiocache.serializers import (
    PickleSerializer,
)
from azure.identity.aio._credentials.client_secret import (
    ClientSecretCredential,
)
from azure.mgmt.containerservice.aio._container_service_client import (
    ContainerServiceClient,
)
from lib_cspm.azure.azure_clients.common import (
    custom_key_builder,
)
from model.core import (
    AzureCredentials,
)


@aiocache.cached(
    serializer=PickleSerializer(),
    key_builder=custom_key_builder,
)
async def run_azure_container_service(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> list[dict[str, Any]]:
    clusters = []
    async with ContainerServiceClient(
        client_credential,
        credentials.subscription_id,
    ) as container_client:
        async for cluster in container_client.managed_clusters.list():
            clusters.append(cluster.as_dict())  # noqa: PERF401
    return clusters


@aiocache.cached(
    serializer=PickleSerializer(),
    key_builder=custom_key_builder,
)
async def run_azure_container_service_upgrade_profile(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> list[dict[str, Any]]:
    clusters = await run_azure_container_service(credentials, client_credential)
    upgrade_profiles = []
    async with ContainerServiceClient(
        client_credential,
        credentials.subscription_id,
    ) as container_client:
        for cluster in clusters:
            group_name = cluster["id"].split("/")[4]
            upgrade_profile = await container_client.managed_clusters.get_upgrade_profile(
                resource_group_name=group_name,
                resource_name=cluster["name"],
            )
            upgrade_profiles.append(
                {
                    "id": cluster["id"],
                    "upgrade_profile": upgrade_profile.as_dict(),
                },
            )

    return upgrade_profiles
