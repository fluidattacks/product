from typing import (
    Any,
)

import aiocache
from aiocache.serializers import (
    PickleSerializer,
)
from azure.identity.aio._credentials.client_secret import (
    ClientSecretCredential,
)
from azure.mgmt.storage.v2023_01_01.aio._storage_management_client import (
    StorageManagementClient,
)
from lib_cspm.azure.azure_clients.common import (
    custom_key_builder,
)
from model.core import (
    AzureCredentials,
)


@aiocache.cached(
    serializer=PickleSerializer(),
    key_builder=custom_key_builder,
)
async def run_azure_storage_client(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> list[dict[str, Any]]:
    async with StorageManagementClient(
        client_credential,
        credentials.subscription_id,
    ) as client_storage:
        all_storages = []
        async for storage_account in client_storage.storage_accounts.list():
            all_storages.append(dict(storage_account.as_dict()))  # noqa: PERF401
    return all_storages


@aiocache.cached(
    serializer=PickleSerializer(),
    key_builder=custom_key_builder,
)
async def run_azure_storage_blob_containers(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> list[dict[str, Any]]:
    async with StorageManagementClient(
        client_credential,
        credentials.subscription_id,
    ) as client_storage:
        all_containers = []
        all_storages = await run_azure_storage_client(credentials, client_credential)
        for account in all_storages:
            group_name = account["id"].split("/")[4]
            async for container in client_storage.blob_containers.list(group_name, account["name"]):
                all_containers.append(dict(container.as_dict()))  # noqa: PERF401
    return all_containers


@aiocache.cached(
    serializer=PickleSerializer(),
    key_builder=custom_key_builder,
)
async def run_azure_blob_service_properties(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> list[dict[str, Any]]:
    storage_accounts = await run_azure_storage_client(credentials, client_credential)
    service_blob_config = []
    for storage in storage_accounts:
        group_name = storage["id"].split("/")[4]
        async with StorageManagementClient(
            client_credential,
            credentials.subscription_id,
        ) as client_storage:
            blob_service = await client_storage.blob_services.get_service_properties(
                resource_group_name=group_name,
                account_name=storage["name"],
            )
            service_blob_config.append(dict(blob_service.as_dict()))

    return service_blob_config
