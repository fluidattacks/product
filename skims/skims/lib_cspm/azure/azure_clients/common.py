import hashlib
import json
import re
from collections.abc import (
    Awaitable,
    Callable,
    Coroutine,
)
from typing import (
    Any,
)

from azure.core.exceptions import (
    AzureError,
    ClientAuthenticationError,
    HttpResponseError,
)
from azure.identity.aio._credentials.client_secret import (
    ClientSecretCredential,
)
from model.core import (
    AzureCredentials,
    Vulnerabilities,
)
from utils.logs import (
    log,
)


class AzureClientSecretCredential(
    ClientSecretCredential,
):
    pass


class AzureClientError(Exception):
    pass


AzureMethod = Callable[
    [AzureCredentials, AzureClientSecretCredential],
    Coroutine[Any, Any, tuple[Vulnerabilities, bool | str]],
]

AzureMethodsTuple = tuple[AzureMethod, ...]


def custom_key_builder(func: Any, *args: Any, **kwargs: Any) -> str:  # noqa: ANN401
    args = tuple(arg if not isinstance(arg, set) else list(arg) for arg in args)
    kwargs = {
        key: value if not isinstance(value, set) else list(value) for key, value in kwargs.items()
    }
    input_data = (args[0], kwargs, func.__name__)
    input_json = json.dumps(input_data, sort_keys=True)
    hash_generator = hashlib.sha256()
    hash_generator.update(input_json.encode("utf-8"))
    return hash_generator.hexdigest()


def get_resource_group_from_resource_id(resource_id: str) -> str:
    pattern = r"/resourceGroups/([^/]+)"
    resource_group = re.search(string=resource_id, pattern=pattern)
    if resource_group:
        return resource_group.group(1)
    return ""


async def run_azure_client(
    azure_client: Callable[
        [AzureCredentials, ClientSecretCredential],
        Awaitable[list[dict[str, Any]]],
    ],
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
    **kwargs: Any,  # noqa: ANN401
) -> list[dict[str, Any]]:
    try:
        return await azure_client(credentials, client_credential, **kwargs)
    except HttpResponseError as exc:
        if "Code: AuthorizationFailed" in str(exc):
            msg = "Credentials do not have permissions to access resource"
            await log("warning", msg)
            return []
        raise AzureClientError from exc
    except (AzureError, ClientAuthenticationError) as exc:
        raise AzureClientError from exc
