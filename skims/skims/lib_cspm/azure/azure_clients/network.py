from typing import (
    Any,
)

import aiocache
from aiocache.serializers import (
    PickleSerializer,
)
from azure.identity.aio._credentials.client_secret import (
    ClientSecretCredential,
)
from azure.mgmt.network.aio._client import (
    NetworkManagementClient,
)
from lib_cspm.azure.azure_clients.common import (
    custom_key_builder,
)
from model.core import (
    AzureCredentials,
)


@aiocache.cached(
    serializer=PickleSerializer(),
    key_builder=custom_key_builder,
)
async def run_azure_network_security_groups(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> list[dict[str, Any]]:
    network_security_groups = []
    async with NetworkManagementClient(
        client_credential,
        credentials.subscription_id,
    ) as network_client:
        async for nsg in network_client.network_security_groups.list_all():
            network_security_groups.append(nsg.as_dict())  # noqa: PERF401
    return network_security_groups


@aiocache.cached(
    serializer=PickleSerializer(),
    key_builder=custom_key_builder,
)
async def run_azure_network_watchers(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> list[dict[str, Any]]:
    network_watchers = []
    async with NetworkManagementClient(
        client_credential,
        credentials.subscription_id,
    ) as network_client:
        async for watcher in network_client.network_watchers.list_all():
            network_watchers.append(watcher.as_dict())  # noqa: PERF401
    return network_watchers


@aiocache.cached(
    serializer=PickleSerializer(),
    key_builder=custom_key_builder,
)
async def run_azure_network_application_gateway(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> list[dict[str, Any]]:
    application_gateways = []
    async with NetworkManagementClient(
        client_credential,
        credentials.subscription_id,
    ) as network_client:
        async for gateway in network_client.application_gateways.list_all():
            application_gateways.append(gateway.as_dict())  # noqa: PERF401
    return application_gateways


@aiocache.cached(
    serializer=PickleSerializer(),
    key_builder=custom_key_builder,
)
async def run_azure_network_waf_policies(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> list[dict[str, Any]]:
    waf_policies = []
    async with NetworkManagementClient(
        client_credential,
        credentials.subscription_id,
    ) as network_client:
        async for policies in network_client.web_application_firewall_policies.list_all():
            waf_policies.append(policies.as_dict())  # noqa: PERF401
    return waf_policies


@aiocache.cached(
    serializer=PickleSerializer(),
    key_builder=custom_key_builder,
)
async def run_azure_network_flow_logs(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> list[dict[str, Any]]:
    network_watchers = await run_azure_network_watchers(credentials, client_credential)
    flow_logs = []
    async with NetworkManagementClient(
        client_credential,
        credentials.subscription_id,
    ) as network_client:
        for watcher in network_watchers:
            group_name = watcher["id"].split("/")[4]
            async for log in network_client.flow_logs.list(
                resource_group_name=group_name,
                network_watcher_name=watcher["name"],
            ):
                flow_logs.append(log.as_dict())  # noqa: PERF401
    return flow_logs


@aiocache.cached(
    serializer=PickleSerializer(),
    key_builder=custom_key_builder,
)
async def run_azure_firewall_network_rules(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> list[dict[str, Any]]:
    firewall_network_rules = []
    async with NetworkManagementClient(
        client_credential,
        credentials.subscription_id,
    ) as network_client:
        async for firewall in network_client.azure_firewalls.list_all():
            firewall_network_rules.append(firewall.as_dict())  # noqa: PERF401
    return firewall_network_rules
