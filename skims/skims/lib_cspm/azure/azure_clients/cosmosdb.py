from typing import (
    Any,
)

import aiocache
from aiocache.serializers import (
    PickleSerializer,
)
from azure.identity.aio._credentials.client_secret import (
    ClientSecretCredential,
)
from azure.mgmt.cosmosdb.aio import (
    CosmosDBManagementClient,
)
from lib_cspm.azure.azure_clients.common import (
    custom_key_builder,
)
from model.core import (
    AzureCredentials,
)


@aiocache.cached(
    serializer=PickleSerializer(),
    key_builder=custom_key_builder,
)
async def run_azure_cosmos_db(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> list[dict[str, Any]]:
    accounts = []
    async with CosmosDBManagementClient(
        client_credential,
        credentials.subscription_id,
    ) as cosmos_db_client:
        async for account in cosmos_db_client.database_accounts.list():
            accounts.append(dict(account.as_dict()))  # noqa: PERF401
    return accounts
