from typing import (
    Any,
)

import aiocache
from aiocache.serializers import (
    PickleSerializer,
)
from azure.identity.aio._credentials.client_secret import (
    ClientSecretCredential,
)
from azure.mgmt.redis.aio._redis_management_client import (
    RedisManagementClient,
)
from lib_cspm.azure.azure_clients.common import (
    custom_key_builder,
    get_resource_group_from_resource_id,
)
from model.core import (
    AzureCredentials,
)


@aiocache.cached(
    serializer=PickleSerializer(),
    key_builder=custom_key_builder,
)
async def run_azure_redis_client(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> list[dict[str, Any]]:
    async with RedisManagementClient(
        client_credential,
        credentials.subscription_id,
    ) as redis_client:
        all_redis = []
        async for redis in redis_client.redis.list_by_subscription():
            all_redis.append(dict(redis.as_dict()))  # noqa: PERF401
    return all_redis


@aiocache.cached(
    serializer=PickleSerializer(),
    key_builder=custom_key_builder,
)
async def run_azure_redis_firewall_rules(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> list[dict[str, Any]]:
    all_redis = await run_azure_redis_client(credentials, client_credential)
    async with RedisManagementClient(
        client_credential,
        credentials.subscription_id,
    ) as redis_client:
        firewall_rules = []
        for redis in all_redis:
            cache_id = redis.get("id", "")
            cache_name = redis.get("name", "")
            public_network_access = redis.get("public_network_access", "")
            resource_group = get_resource_group_from_resource_id(resource_id=cache_id)
            rules = []
            async for firewall in redis_client.firewall_rules.list(
                resource_group_name=resource_group,
                cache_name=cache_name,
            ):
                firewall_dict = firewall.as_dict()
                firewall_dict["public_network_access"] = public_network_access
                rules.append(firewall_dict)
            firewall_rules += rules
    return firewall_rules
