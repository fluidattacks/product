from typing import (
    Any,
)

import aiocache
from aiocache.serializers import (
    PickleSerializer,
)
from azure.identity.aio._credentials.client_secret import (
    ClientSecretCredential,
)
from azure.mgmt.synapse.aio._synapse_management_client import (
    SynapseManagementClient,
)
from lib_cspm.azure.azure_clients.common import (
    custom_key_builder,
    get_resource_group_from_resource_id,
)
from model.core import (
    AzureCredentials,
)


@aiocache.cached(
    serializer=PickleSerializer(),
    key_builder=custom_key_builder,
)
async def get_azure_synapse_workspaces(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> list[dict[str, Any]]:
    async with SynapseManagementClient(
        client_credential,
        credentials.subscription_id,
    ) as synapse_client:
        workspaces = []
        async for workspace in synapse_client.workspaces.list():
            workspaces.append(dict(workspace.as_dict()))  # noqa: PERF401
    return workspaces


@aiocache.cached(
    serializer=PickleSerializer(),
    key_builder=custom_key_builder,
)
async def run_azure_synapse_workspaces_ip_firewall_rules(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> list[dict[str, Any]]:
    async with SynapseManagementClient(
        client_credential,
        credentials.subscription_id,
    ) as synapse_client:
        workspaces = await get_azure_synapse_workspaces(
            credentials,
            client_credential,
        )
        all_ip_firewall_rules = []
        for workspace in workspaces:
            resource_group = get_resource_group_from_resource_id(workspace.get("id", ""))
            public_network_access = workspace.get("public_network_access", "")
            async for rules in synapse_client.ip_firewall_rules.list_by_workspace(
                resource_group_name=resource_group,
                workspace_name=workspace.get("name", ""),
            ):
                rules_dict = rules.as_dict()
                rules_dict["public_network_access"] = public_network_access
                all_ip_firewall_rules.append(rules_dict)
    return all_ip_firewall_rules
