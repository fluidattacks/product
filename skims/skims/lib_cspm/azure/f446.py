from typing import (
    Any,
)

from lib_cspm.azure.azure_clients.apimanagement import (
    run_api_management_services,
)
from lib_cspm.azure.azure_clients.app_service import (
    run_azure_function_app_host_keys,
    run_azure_web_apps,
    run_azure_web_apps_configuration,
)
from lib_cspm.azure.azure_clients.authorization import (
    run_roles_by_principal_list,
)
from lib_cspm.azure.azure_clients.azure_resource import (
    run_subscription_assigned_policies,
)
from lib_cspm.azure.azure_clients.batch import (
    run_batch_pools,
)
from lib_cspm.azure.azure_clients.common import (
    AzureClientSecretCredential as ClientSecretCredential,
)
from lib_cspm.azure.azure_clients.common import (
    AzureMethodsTuple,
    run_azure_client,
)
from lib_cspm.azure.azure_clients.compute import (
    run_azure_compute_client,
    run_azure_compute_scale_sets_client,
)
from lib_cspm.azure.azure_clients.container_registry import (
    run_azure_container_registries,
    run_azure_container_registries_replication,
)
from lib_cspm.azure.azure_clients.container_service import (
    run_azure_container_service,
    run_azure_container_service_upgrade_profile,
)
from lib_cspm.azure.azure_clients.cosmosdb import (
    run_azure_cosmos_db,
)
from lib_cspm.azure.azure_clients.datafactory import (
    run_azure_datafactory,
)
from lib_cspm.azure.azure_clients.key_vault import (
    run_azure_key_vault,
    run_azure_key_vault_keys,
    run_azure_key_vault_secrets,
)
from lib_cspm.azure.azure_clients.network import (
    run_azure_network_application_gateway,
    run_azure_network_waf_policies,
)
from lib_cspm.azure.azure_clients.rdbms import (
    run_azure_db_mysql_flex_server_firewall_client,
    run_azure_db_postgresql_configs_client,
    run_azure_db_postgresql_firewall_client,
    run_azure_db_psql_flexible_server_configs_client,
)
from lib_cspm.azure.azure_clients.redis import (
    run_azure_redis_client,
    run_azure_redis_firewall_rules,
)
from lib_cspm.azure.azure_clients.search import (
    run_search_services,
)
from lib_cspm.azure.azure_clients.sql import (
    run_azure_db_sql_extended_audit_client,
    run_azure_db_sql_firewall_client,
    run_azure_dbs_transparent_data_encryption,
)
from lib_cspm.azure.azure_clients.storage import (
    run_azure_storage_client,
)
from lib_cspm.azure.shield import (
    shield_cspm_azure_decorator,
)
from lib_cspm.azure.utils import (
    Location,
    build_vulnerabilities,
    is_valid_ip_address_or_range,
)
from lib_cspm.methods_enum import (
    MethodsEnumCSPM as MethodsEnum,
)
from model.core import (
    AzureCredentials,
    Vulnerabilities,
)

KEY_VAULT__KEY_ACCESS_PERMISSIONS = [
    "backup",
    "create",
    "decrypt",
    "delete",
    "encrypt",
    "get",
    "getrotationpolicy",
    "import",
    "list",
    "purge",
    "recover",
    "release",
    "restore",
    "rotate",
    "setrotationpolicy",
    "sign",
    "unwrapkey",
    "update",
    "verify",
    "wrapkey",
]


@shield_cspm_azure_decorator
async def check_ssh_authentication(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.CHECK_SSH_AUTHENTICATION
    vulns: Vulnerabilities = ()
    vm_machines = await run_azure_client(run_azure_compute_client, credentials, client_credential)
    for vm_machine in vm_machines:
        os_profile = vm_machine.get("os_profile", {})
        linux_configuration = os_profile.get("linux_configuration", {})
        disable_password_authentication = linux_configuration.get(
            "disable_password_authentication",
            True,
        )
        if not disable_password_authentication and not vm_machine.get(
            "virtual_machine_scale_set",
            {},
        ):
            locations = [
                Location(
                    values=(disable_password_authentication,),
                    id=vm_machine["id"],
                    access_patterns=(
                        "/os_profile/linux_configuration/disable_password_authentication",
                    ),
                    method=method,
                ),
            ]
            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                azure_response=vm_machine,
            )

    return (vulns, True)


@shield_cspm_azure_decorator
async def azure_vm_scale_set_check_ssh_authentication(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.CHECK_SSH_AUTHENTICATION
    vulns: Vulnerabilities = ()
    vm_machines = await run_azure_client(
        run_azure_compute_scale_sets_client,
        credentials,
        client_credential,
    )
    for vm_machine in vm_machines:
        os_profile = vm_machine["virtual_machine_profile"]["os_profile"]
        linux_configuration = os_profile.get("linux_configuration", {})
        disable_password_authentication = linux_configuration.get(
            "disable_password_authentication",
            True,
        )
        if not disable_password_authentication:
            locations = [
                Location(
                    values=(disable_password_authentication,),
                    id=vm_machine["id"],
                    access_patterns=(
                        "/virtual_machine_profile/os_profile/"
                        "linux_configuration/disable_password_authentication",
                    ),
                    method=method,
                ),
            ]
            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                azure_response=vm_machine,
            )

    return (vulns, True)


@shield_cspm_azure_decorator
async def azure_vm_encryption_at_host_disabled(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.AZURE_VM_ENCRYPTION_AT_HOST_DISABLED
    vulns: Vulnerabilities = ()
    vm_machines = await run_azure_client(run_azure_compute_client, credentials, client_credential)
    for vm_machine in vm_machines:
        security_profile = vm_machine.get("security_profile", {})
        encryption_at_host = security_profile.get("encryption_at_host", False)
        if not encryption_at_host and not vm_machine.get("virtual_machine_scale_set", {}):
            locations = [
                Location(
                    values=(encryption_at_host,)
                    if security_profile.get("encryption_at_host") is not None
                    else (),
                    id=vm_machine["id"],
                    access_patterns=("/security_profile/encryption_at_host",)
                    if security_profile.get("encryption_at_host") is not None
                    else (),
                    method=method,
                ),
            ]
            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                azure_response=vm_machine,
            )

    return (vulns, True)


@shield_cspm_azure_decorator
async def azure_vm_scale_set_encryption_at_host_disabled(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.AZURE_VM_ENCRYPTION_AT_HOST_DISABLED
    vulns: Vulnerabilities = ()
    vm_machines = await run_azure_client(
        run_azure_compute_scale_sets_client,
        credentials,
        client_credential,
    )
    for vm_machine in vm_machines:
        security_profile = vm_machine["virtual_machine_profile"].get("security_profile", {})
        encryption_at_host = security_profile.get("encryption_at_host", False)
        if not encryption_at_host:
            locations = [
                Location(
                    values=(encryption_at_host,)
                    if security_profile.get("encryption_at_host") is not None
                    else (),
                    id=vm_machine["id"],
                    access_patterns=(
                        "/virtual_machine_profile/security_profile/encryption_at_host",
                    )
                    if security_profile.get("encryption_at_host") is not None
                    else (),
                    method=method,
                ),
            ]
            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                azure_response=vm_machine,
            )

    return (vulns, True)


@shield_cspm_azure_decorator
async def redis_cache_public_network_access_enabled(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.REDIS_CACHE_PUBLIC_NETWORK_ACCESS_ENABLED
    vulns: Vulnerabilities = ()
    redis_caches = await run_azure_client(run_azure_redis_client, credentials, client_credential)
    for redis in redis_caches:
        public_network_access = redis["public_network_access"]
        if public_network_access == "Enabled":
            locations = [
                Location(
                    values=(public_network_access,),
                    id=redis["id"],
                    access_patterns=("/public_network_access",),
                    method=method,
                ),
            ]
            vulns += build_vulnerabilities(locations=locations, method=method, azure_response=redis)

    return (vulns, True)


@shield_cspm_azure_decorator
async def redis_cache_firewall_allows_public_access(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.REDIS_CACHE_FIREWALL_ALLOWS_PUBLIC_ACCESS
    vulns: Vulnerabilities = ()
    redis_firewalls = await run_azure_client(
        run_azure_redis_firewall_rules,
        credentials,
        client_credential,
    )
    for firewall in redis_firewalls:
        start_ip = firewall.get("start_ip", "")
        end_ip = firewall.get("end_ip", "")
        public_network_access = firewall.get("public_network_access", "Disabled")
        locations = []
        if (
            public_network_access.lower() == "enabled"
            and start_ip
            and end_ip
            and not is_valid_ip_address_or_range(start_ip)
            and not is_valid_ip_address_or_range(end_ip)
        ):
            firewall.pop("public_network_access")
            locations.append(
                Location(
                    id=firewall["id"],
                    access_patterns=("/start_ip", "/end_ip"),
                    values=(start_ip, end_ip),
                    method=method,
                ),
            )

        vulns += build_vulnerabilities(locations=locations, method=method, azure_response=firewall)

    return (vulns, True)


@shield_cspm_azure_decorator
async def azure_app_service_http2_is_disabled(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.AZURE_APP_SERVICE_HTTP2_IS_DISABLED
    vulns: Vulnerabilities = ()
    web_apps = await run_azure_client(run_azure_web_apps, credentials, client_credential)
    for web_app in web_apps:
        site_config = web_app.get("site_config", {})
        http2_is_enabled_bool = site_config.get("http20_enabled")
        if not http2_is_enabled_bool:
            locations = [
                Location(
                    id=web_app["id"],
                    access_patterns=("/site_config/http20_enabled",),
                    values=(http2_is_enabled_bool,),
                    method=method,
                ),
            ]
            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                azure_response=web_app,
            )

    return (vulns, True)


@shield_cspm_azure_decorator
async def azure_app_service_does_not_use_a_managed_identity(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.AZURE_APP_SERVICE_DOES_NOT_USE_A_MANAGED_IDENTITY
    vulns: Vulnerabilities = ()
    web_apps = await run_azure_client(run_azure_web_apps, credentials, client_credential)
    for web_app in web_apps:
        has_managed_identity = web_app.get("identity")
        if not has_managed_identity:
            locations = [
                Location(
                    id=web_app["id"],
                    access_patterns=(),
                    values=(),
                    method=method,
                ),
            ]
            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                azure_response=web_app,
            )

    return (vulns, True)


@shield_cspm_azure_decorator
async def azure_function_app_logging_is_disabled(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.AZURE_FUNCTION_APP_LOGGING_IS_DISABLED
    vulns: Vulnerabilities = ()
    app_service_configuration = await run_azure_client(
        run_azure_web_apps_configuration,
        credentials,
        client_credential,
        filter_type="functionapp",
    )
    for app_service_config in app_service_configuration:
        locations: list[Location] = []
        detailed_error_logging_enabled = app_service_config.get("detailed_error_logging_enabled")
        if not detailed_error_logging_enabled:
            locations.append(
                Location(
                    id=app_service_config["id"],
                    access_patterns=("/detailed_error_logging_enabled",),
                    values=(detailed_error_logging_enabled,),
                    method=method,
                ),
            )

        request_tracing_enabled = app_service_config.get("request_tracing_enabled")
        if not request_tracing_enabled:
            locations.append(
                Location(
                    id=app_service_config["id"],
                    access_patterns=("/request_tracing_enabled",),
                    values=(request_tracing_enabled,),
                    method=method,
                ),
            )
        vulns += build_vulnerabilities(
            locations=locations,
            method=method,
            azure_response=app_service_config,
        )

    return (vulns, True)


@shield_cspm_azure_decorator
async def azure_keys_expiration_date_is_not_enabled(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.AZURE_KEYS_EXPIRATION_DATE_IS_NOT_ENABLED
    vulns: Vulnerabilities = ()
    keys = await run_azure_client(run_azure_key_vault_keys, credentials, client_credential)
    for key in keys:
        attributes: dict = key.get("attributes", {})
        if not attributes.get("expires") and attributes.get("enabled"):
            locations = [
                Location(
                    id=key["id"],
                    access_patterns=(),
                    values=(),
                    method=method,
                ),
            ]
            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                azure_response=key,
            )

    return (vulns, True)


@shield_cspm_azure_decorator
async def azure_secret_expiration_date_is_not_enabled(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.AZURE_SECRET_EXPIRATION_DATE_IS_NOT_ENABLED
    vulns: Vulnerabilities = ()
    secrets = await run_azure_client(run_azure_key_vault_secrets, credentials, client_credential)
    for secret in secrets:
        secret_attrs: dict = secret.get("properties", {}).get("attributes", {})
        if not secret_attrs.get("expires") and secret_attrs.get("enabled"):
            locations = [
                Location(
                    id=secret["id"],
                    access_patterns=(),
                    values=(),
                    method=method,
                ),
            ]
            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                azure_response=secret,
            )

    return (vulns, True)


@shield_cspm_azure_decorator
async def azure_app_service_always_on_is_not_enabled(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.AZURE_APP_SERVICE_ALWAYS_ON_IS_NOT_ENABLED
    vulns: Vulnerabilities = ()
    web_apps = await run_azure_client(run_azure_web_apps, credentials, client_credential)
    for web_app in web_apps:
        site_config = web_app.get("site_config", {})
        always_on = site_config.get("always_on")
        if not always_on:
            locations = [
                Location(
                    id=web_app["id"],
                    access_patterns=("/site_config/always_on",),
                    values=(always_on,),
                    method=method,
                ),
            ]
            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                azure_response=web_app,
            )

    return (vulns, True)


@shield_cspm_azure_decorator
async def network_out_of_date_owasp_rules(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.NETWORK_OUT_OF_DATE_OWASP_RULES
    vulns: Vulnerabilities = ()
    waf_policies = await run_azure_client(
        run_azure_network_waf_policies,
        credentials,
        client_credential,
    )
    for policy in waf_policies:
        managed_rule_sets = policy.get("managed_rules", {}).get("managed_rule_sets", [])
        for rules in managed_rule_sets:
            rule_set_type = rules.get("rule_set_type", "")
            rule_set_version = rules.get("rule_set_version", "")
            if (
                rule_set_type == "OWASP"
                and rule_set_version < "3.2"
                and policy.get("application_gateways", [])
            ):
                locations = [
                    Location(
                        id=policy["id"],
                        access_patterns=("/rule_set_version",),
                        values=(rule_set_version,),
                        method=method,
                    ),
                ]
                vulns += build_vulnerabilities(
                    locations=locations,
                    method=method,
                    azure_response=rules,
                )

    return (vulns, True)


@shield_cspm_azure_decorator
async def container_registry_is_not_using_replication(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.CONTAINER_REGISTRY_IS_NOT_USING_REPLICATION
    vulns: Vulnerabilities = ()
    replications_by_container = await run_azure_client(
        run_azure_container_registries_replication,
        credentials,
        client_credential,
    )
    for replications in replications_by_container:
        if not replications.get("replications", []):
            locations = [
                Location(
                    id=replications["id"],
                    access_patterns=("/replications",),
                    values=(replications["replications"],),
                    method=method,
                ),
            ]
            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                azure_response=replications,
            )

    return (vulns, True)


def check_if_preview(is_upgradable: list) -> bool:
    return any(
        version.get("is_preview", False) and len(is_upgradable) == 1 for version in is_upgradable
    )


@shield_cspm_azure_decorator
async def aks_is_not_using_latest_version(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.AKS_IS_NOT_USING_LATEST_VERSION
    vulns: Vulnerabilities = ()
    upgrade_profiles = await run_azure_client(
        run_azure_container_service_upgrade_profile,
        credentials,
        client_credential,
    )
    for upgrade_profile in upgrade_profiles:
        is_upgradable = upgrade_profile["upgrade_profile"]["control_plane_profile"].get(
            "upgrades",
            [],
        )
        if is_upgradable and not check_if_preview(is_upgradable):
            locations = [
                Location(
                    id=upgrade_profile["id"],
                    access_patterns=("/upgrade_profile/control_plane_profile/upgrades",),
                    values=(
                        upgrade_profile["upgrade_profile"]["control_plane_profile"]["upgrades"],
                    ),
                    method=method,
                ),
            ]
            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                azure_response=upgrade_profile,
            )

    return (vulns, True)


@shield_cspm_azure_decorator
async def aks_has_enable_local_accounts(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.AKS_HAS_ENABLE_LOCAL_ACCOUNTS
    vulns: Vulnerabilities = ()
    container_services = await run_azure_client(
        run_azure_container_service,
        credentials,
        client_credential,
    )
    for container in container_services:
        if not container.get("disable_local_accounts", True):
            locations = [
                Location(
                    id=container["id"],
                    access_patterns=("/disable_local_accounts",),
                    values=(container["disable_local_accounts"],),
                    method=method,
                ),
            ]
            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                azure_response=container,
            )

    return (vulns, True)


@shield_cspm_azure_decorator
async def aks_has_kubenet_network_plugin(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.AKS_HAS_KUBENET_NETWORK_PLUGIN
    vulns: Vulnerabilities = ()
    container_services = await run_azure_client(
        run_azure_container_service,
        credentials,
        client_credential,
    )
    for container in container_services:
        network_profile = container["network_profile"]
        if network_profile["network_plugin"] == "kubenet":
            locations = [
                Location(
                    id=container["id"],
                    access_patterns=("/network_profile/network_plugin",),
                    values=(container["network_profile"]["network_plugin"],),
                    method=method,
                ),
            ]
            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                azure_response=container,
            )

    return (vulns, True)


@shield_cspm_azure_decorator
async def aks_has_rbac_disabled(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.AKS_HAS_RBAC_DISABLED
    vulns: Vulnerabilities = ()
    container_services = await run_azure_client(
        run_azure_container_service,
        credentials,
        client_credential,
    )
    for container in container_services:
        is_rbac_enabled = container["enable_rbac"]
        if not is_rbac_enabled:
            locations = [
                Location(
                    id=container["id"],
                    access_patterns=("/enable_rbac",),
                    values=(container["enable_rbac"],),
                    method=method,
                ),
            ]
            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                azure_response=container,
            )

    return (vulns, True)


@shield_cspm_azure_decorator
async def aks_api_server_allows_public_access(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.AKS_API_SERVER_ALLOWS_PUBLIC_ACCESS
    vulns: Vulnerabilities = ()
    container_services = await run_azure_client(
        run_azure_container_service,
        credentials,
        client_credential,
    )
    for container in container_services:
        access_profile = container.get("api_server_access_profile", [])
        if not access_profile:
            locations = [
                Location(
                    id=container["id"],
                    access_patterns=(),
                    values=(),
                    method=method,
                ),
            ]
            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                azure_response=container,
            )

    return (vulns, True)


@shield_cspm_azure_decorator
async def storage_not_enabled_infrastructure_encryption(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.STORAGE_NOT_ENABLED_INFRASTRUCTURE_ENCRYPTION
    vulns: Vulnerabilities = ()
    storages = await run_azure_client(
        run_azure_storage_client,
        credentials,
        client_credential,
    )
    for storage in storages:
        require_infrastructure_encryption = storage.get("encryption", {}).get(
            "require_infrastructure_encryption",
            True,
        )
        if not require_infrastructure_encryption:
            locations = [
                Location(
                    id=storage["id"],
                    access_patterns=("/encryption/require_infrastructure_encryption",),
                    values=(require_infrastructure_encryption,),
                    method=method,
                ),
            ]
            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                azure_response=storage,
            )

    return (vulns, True)


@shield_cspm_azure_decorator
async def azure_batch_jobs_runs_in_admin_mode(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> tuple[Vulnerabilities, bool]:
    pools = await run_azure_client(run_batch_pools, credentials, client_credential)
    method = MethodsEnum.AZURE_BATCH_JOBS_RUNS_IN_ADMIN_MODE
    vulns: Vulnerabilities = ()
    for pool in pools:
        if not (start_task := pool.get("start_task")):
            continue
        auto_user = start_task.get("user_identity", {}).get("auto_user", {})
        elevation_level = str(auto_user.get("elevation_level", ""))
        if elevation_level.lower() == "admin":
            locations = [
                Location(
                    id=pool["id"],
                    access_patterns=("/start_task/user_identity/auto_user/elevation_level",),
                    values=(elevation_level,),
                    method=method,
                ),
            ]
            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                azure_response=pool,
            )
    return (vulns, True)


@shield_cspm_azure_decorator
async def azure_db_postgresql_connection_throttling_disabled(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.AZURE_DB_POSTGRESQL_CONNECTION_THROTTLING_DISABLED
    vulns: Vulnerabilities = ()
    dbs_configurations = await run_azure_client(
        run_azure_db_postgresql_configs_client,
        credentials,
        client_credential,
        config_name="connection_throttling",
    )
    for configuration in dbs_configurations:
        value = configuration.get("value", "on")
        if value.lower() == "off":
            locations = [
                Location(
                    values=(value,),
                    id=configuration["id"],
                    access_patterns=("/value",),
                    method=method,
                ),
            ]
            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                azure_response=configuration,
            )
    return (vulns, True)


@shield_cspm_azure_decorator
async def azure_function_app_use_not_host_keys(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.AZURE_FUNCTION_APP_USE_NOT_HOST_KEYS
    vulns: Vulnerabilities = ()
    functions_host_keys = await run_azure_client(
        run_azure_function_app_host_keys,
        credentials,
        client_credential,
    )
    for function_host_keys in functions_host_keys:
        function_keys = function_host_keys.get("function_keys")
        if function_keys == {}:
            function_id = function_host_keys.get("id", "")
            master_key_len = len(function_host_keys["master_key"])
            function_host_keys["master_key"] = "*" * master_key_len
            function_host_keys.pop("function_id")
            locations = [
                Location(
                    values=(function_keys,),
                    id=function_id,
                    access_patterns=("/function_keys",),
                    method=method,
                ),
            ]
            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                azure_response=function_host_keys,
            )
    return (vulns, True)


@shield_cspm_azure_decorator
async def azure_db_postgresql_firewall_allows_public_access(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.AZURE_DB_POSTGRESQL_FIREWALL_ALLOWS_PUBLIC_ACCESS
    vulns: Vulnerabilities = ()
    firewall_config = await run_azure_client(
        run_azure_db_postgresql_firewall_client,
        credentials,
        client_credential,
    )
    for firewall in firewall_config:
        firewall_id = firewall.get("id", "")
        public_network_access = firewall.get("public_network_access", "Disabled")
        start_ip = firewall.get("start_ip_address", "")
        end_ip = firewall.get("end_ip_address", "")
        if (
            public_network_access.lower() == "enabled"
            and start_ip
            and end_ip
            and not is_valid_ip_address_or_range(start_ip)
            and not is_valid_ip_address_or_range(end_ip)
        ):
            firewall.pop("public_network_access")
            locations = [
                (
                    Location(
                        id=firewall_id,
                        access_patterns=(
                            "/start_ip_address",
                            "/end_ip_address",
                        ),
                        values=(
                            start_ip,
                            end_ip,
                        ),
                        method=method,
                    )
                ),
            ]
            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                azure_response=firewall,
            )
    return (vulns, True)


@shield_cspm_azure_decorator
async def azure_db_mysql_firewall_allows_public_access(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.AZURE_DB_MYSQL_FIREWALL_ALLOWS_PUBLIC_ACCESS
    vulns: Vulnerabilities = ()
    firewall_config = await run_azure_client(
        run_azure_db_mysql_flex_server_firewall_client,
        credentials,
        client_credential,
    )
    for firewall in firewall_config:
        firewall_id = firewall.get("id", "")
        network = firewall.get("network", {})
        public_network_access = network.get("public_network_access", "Disabled")
        start_ip = firewall.get("start_ip_address", "")
        end_ip = firewall.get("end_ip_address", "")
        if (
            public_network_access.lower() == "enabled"
            and start_ip
            and end_ip
            and not is_valid_ip_address_or_range(start_ip)
            and not is_valid_ip_address_or_range(end_ip)
        ):
            firewall.pop("network")
            locations = [
                (
                    Location(
                        id=firewall_id,
                        access_patterns=(
                            "/start_ip_address",
                            "/end_ip_address",
                        ),
                        values=(
                            start_ip,
                            end_ip,
                        ),
                        method=method,
                    )
                ),
            ]
            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                azure_response=firewall,
            )
    return (vulns, True)


@shield_cspm_azure_decorator
async def azure_db_sql_extended_audit_disabled(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.AZURE_DB_SQL_EXTENDED_AUDIT_DISABLED
    vulns: Vulnerabilities = ()
    sql_audit_config = await run_azure_client(
        run_azure_db_sql_extended_audit_client,
        credentials,
        client_credential,
    )
    for config in sql_audit_config:
        state = config.get("state", "")
        if state == "Disabled":
            locations = [
                Location(
                    values=(state,),
                    id=config["id"],
                    access_patterns=("/state",),
                    method=method,
                ),
            ]

            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                azure_response=config,
            )
    return (vulns, True)


@shield_cspm_azure_decorator
async def azure_db_sql_insecure_audit_retention_period(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.AZURE_DB_SQL_INSECURE_AUDIT_RETENTION_PERIOD
    vulns: Vulnerabilities = ()
    sql_audit_config = await run_azure_client(
        run_azure_db_sql_extended_audit_client,
        credentials,
        client_credential,
    )
    minimum_retention_days = 90
    for config in sql_audit_config:
        retention_days = config.get("retention_days", 0)
        if retention_days < minimum_retention_days:
            locations = [
                Location(
                    values=(retention_days,),
                    id=config["id"],
                    access_patterns=("/retention_days",),
                    method=method,
                ),
            ]

            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                azure_response=config,
            )
    return (vulns, True)


@shield_cspm_azure_decorator
async def azure_db_sql_firewall_allows_public_access(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.AZURE_DB_SQL_FIREWALL_ALLOWS_PUBLIC_ACCESS
    vulns: Vulnerabilities = ()
    firewall_config = await run_azure_client(
        run_azure_db_sql_firewall_client,
        credentials,
        client_credential,
    )
    for firewall in firewall_config:
        firewall_id = firewall.get("id", "")
        public_network_access = firewall.get("public_network_access", "Disabled")
        start_ip = firewall.get("start_ip_address", "")
        end_ip = firewall.get("end_ip_address", "")
        if (
            public_network_access.lower() == "enabled"
            and start_ip
            and end_ip
            and not is_valid_ip_address_or_range(start_ip)
            and not is_valid_ip_address_or_range(end_ip)
        ):
            firewall.pop("public_network_access")
            locations = [
                (
                    Location(
                        id=firewall_id,
                        access_patterns=(
                            "/start_ip_address",
                            "/end_ip_address",
                        ),
                        values=(
                            start_ip,
                            end_ip,
                        ),
                        method=method,
                    )
                ),
            ]
            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                azure_response=firewall,
            )
    return (vulns, True)


async def _check_if_vulnerable_log_settings(
    dbs_configurations: list[dict[str, Any]],
    method: MethodsEnum,
    setting: str,
    default_value: str = "on",
) -> Vulnerabilities:
    vulns: Vulnerabilities = ()
    for configuration in dbs_configurations:
        value = configuration.get("value", default_value)
        if value.lower() == "off":
            locations = [
                Location(
                    values=(value,),
                    id=configuration["id"],
                    access_patterns=("/value",),
                    method=method,
                    desc_params={"setting": setting},
                ),
            ]
            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                azure_response=configuration,
            )
    return vulns


async def azure_db_postgresql_log_checkpoints_disabled(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
    method: MethodsEnum,
) -> Vulnerabilities:
    dbs_configurations = await run_azure_client(
        run_azure_db_postgresql_configs_client,
        credentials,
        client_credential,
        config_name="log_checkpoints",
    )
    setting = "log_checkpoints"
    return await _check_if_vulnerable_log_settings(
        dbs_configurations,
        method,
        setting,
    )


async def azure_db_postgresql_log_connections_disabled(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
    method: MethodsEnum,
) -> Vulnerabilities:
    dbs_configurations = await run_azure_client(
        run_azure_db_postgresql_configs_client,
        credentials,
        client_credential,
        config_name="log_connections",
    )
    setting = "log_connections"
    return await _check_if_vulnerable_log_settings(dbs_configurations, method, setting)


async def azure_db_postgresql_log_disconnections_disabled(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
    method: MethodsEnum,
) -> Vulnerabilities:
    dbs_configurations = await run_azure_client(
        run_azure_db_postgresql_configs_client,
        credentials,
        client_credential,
        config_name="log_disconnections",
    )
    setting = "log_disconnections"

    return await _check_if_vulnerable_log_settings(
        dbs_configurations,
        method,
        setting,
        default_value="off",
    )


async def azure_db_postgresql_log_duration_disabled(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
    method: MethodsEnum,
) -> Vulnerabilities:
    dbs_configurations = await run_azure_client(
        run_azure_db_postgresql_configs_client,
        credentials,
        client_credential,
        config_name="log_duration",
    )
    setting = "log_duration"
    return await _check_if_vulnerable_log_settings(
        dbs_configurations,
        method,
        setting,
        default_value="off",
    )


@shield_cspm_azure_decorator
async def azure_db_postgresql_log_settings_disabled(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.AZURE_DB_POSTGRESQL_LOG_SETTINGS_DISABLED
    vulns = (
        *await azure_db_postgresql_log_checkpoints_disabled(credentials, client_credential, method),
        *await azure_db_postgresql_log_connections_disabled(credentials, client_credential, method),
        *await azure_db_postgresql_log_disconnections_disabled(
            credentials,
            client_credential,
            method,
        ),
        *await azure_db_postgresql_log_duration_disabled(credentials, client_credential, method),
    )
    return (vulns, True)


@shield_cspm_azure_decorator
async def azure_publicly_exposed_funct_app(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.AZURE_PUBLICLY_EXPOSED_FUNCT_APP
    vulns: Vulnerabilities = ()
    function_apps = await run_azure_client(
        run_azure_web_apps_configuration,
        credentials,
        client_credential,
        filter_type="functionapp",
    )
    for function_app in function_apps:
        public_access = function_app.get("public_network_access", "")
        ip_restrictions = function_app.get("ip_security_restrictions", {})
        deny_restrictions = [
            restriction for restriction in ip_restrictions if restriction.get("action") == "Deny"
        ]
        if public_access == "Enabled" and not deny_restrictions:
            locations = [
                Location(
                    id=function_app["id"],
                    access_patterns=(
                        "/public_network_access",
                        "/ip_security_restrictions",
                    ),
                    values=(
                        public_access,
                        "No deny restrictions",
                    ),
                    method=method,
                ),
            ]
            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                azure_response=function_app,
            )

    return (vulns, True)


@shield_cspm_azure_decorator
async def azure_subscription_has_at_least_two_owners(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.AZURE_SUBSCRIPTION_HAS_AT_LEAST_TWO_OWNERS
    vulns: Vulnerabilities = ()
    roles_by_principal_list = await run_azure_client(
        run_roles_by_principal_list,
        credentials,
        client_credential,
        filter_principal_type="User",
    )

    if (
        len(roles_by_principal_list) == 1
        and (roles_by_principal := roles_by_principal_list[0])
        and (
            users_with_owner_role := [rol for rol in roles_by_principal.values() if "Owner" in rol]
        )
        and len(users_with_owner_role) < 2
    ):
        locations = [
            Location(
                id=credentials.subscription_id,
                access_patterns=(),
                values=(),
                method=method,
            ),
        ]
        vulns += build_vulnerabilities(
            locations=locations,
            method=method,
            azure_response=roles_by_principal,
        )
    return (vulns, True)


@shield_cspm_azure_decorator
async def az_subscription_not_allowed_resource_types_policy(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.AZ_SUBSCRIPTION_NOT_ALLOWED_RESOURCE_TYPES_POLICY
    vulns: Vulnerabilities = ()
    assigned_policies = await run_azure_client(
        run_subscription_assigned_policies,
        credentials,
        client_credential,
    )
    subscription_id = credentials.subscription_id
    needed_policy = "Not allowed resource types"
    defined_policies = []
    for assigned_policy in assigned_policies:
        policy_definition_name = assigned_policy.get("policy_definition_name")
        defined_policies.append(policy_definition_name)
    if needed_policy not in defined_policies:
        locations = [
            Location(
                id=subscription_id,
                access_patterns=("/subscriptions/subscription",),
                values=(f"'{needed_policy}' policy is not assigned",),
                method=method,
            ),
        ]
        vulns += build_vulnerabilities(
            locations=locations,
            method=method,
            azure_response={"subscriptions": {"subscription": subscription_id}},
        )

    return (vulns, True)


@shield_cspm_azure_decorator
async def container_registry_admin_user_enabled(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.CONTAINER_REGISTRY_ADMIN_USER_ENABLED
    vulns: Vulnerabilities = ()
    containers = await run_azure_client(
        run_azure_container_registries,
        credentials,
        client_credential,
    )
    for container in containers:
        if container.get("admin_user_enabled", False):
            locations = [
                Location(
                    id=container["id"],
                    access_patterns=("/admin_user_enabled",),
                    values=(container["admin_user_enabled"],),
                    method=method,
                ),
            ]
            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                azure_response=container,
            )

    return (vulns, True)


@shield_cspm_azure_decorator
async def network_application_gateway_waf_is_disabled(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.NETWORK_APPLICATION_GATEWAY_WAF_IS_DISABLED
    vulns: Vulnerabilities = ()
    application_gateways = await run_azure_client(
        run_azure_network_application_gateway,
        credentials,
        client_credential,
    )
    for gateway in application_gateways:
        firewall_policy = gateway.get("firewall_policy", {})
        if not firewall_policy:
            locations = [
                Location(
                    id=gateway["id"],
                    access_patterns=(),
                    values=(),
                    method=method,
                ),
            ]
            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                azure_response=gateway,
            )

    return (vulns, True)


@shield_cspm_azure_decorator
async def azure_search_service_does_not_use_a_managed_identity(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.AZURE_SEARCH_SERVICE_DOES_NOT_USE_A_MANAGED_IDENTITY
    vulns: Vulnerabilities = ()
    search_services = await run_azure_client(run_search_services, credentials, client_credential)
    for search_service in search_services:
        has_managed_identity = search_service.get("identity")
        if has_managed_identity == "None":
            locations = [
                Location(
                    id=search_service.get("id", ""),
                    access_patterns=("/identity",),
                    values=(has_managed_identity,),
                    method=method,
                ),
            ]
            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                azure_response=search_service,
            )

    return (vulns, True)


@shield_cspm_azure_decorator
async def azure_search_service_has_insufficient_replicas_configured(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.AZURE_SEARCH_SERVICE_HAS_INSUFFICIENT_REPLICAS_CONFIGURED
    vulns: Vulnerabilities = ()
    search_services = await run_azure_client(run_search_services, credentials, client_credential)
    for search_service in search_services:
        replicas = search_service.get("replica_count", 3)
        if replicas < 3:
            locations = [
                Location(
                    id=search_service.get("id", ""),
                    access_patterns=("/replica_count",),
                    values=(replicas,),
                    method=method,
                ),
            ]
            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                azure_response=search_service,
            )

    return (vulns, True)


@shield_cspm_azure_decorator
async def azure_app_service_mutual_tls_is_disabled(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.AZURE_APP_SERVICE_MUTUAL_TLS_IS_DISABLED
    vulns: Vulnerabilities = ()

    web_apps = await run_azure_client(
        run_azure_web_apps,
        credentials,
        client_credential,
        filter_type="app",
    )
    for web_app in web_apps:
        client_cert_enabled = web_app.get("client_cert_enabled")
        if not client_cert_enabled:
            locations = [
                Location(
                    id=web_app["id"],
                    access_patterns=("/client_cert_enabled",),
                    values=(client_cert_enabled,),
                    method=method,
                ),
            ]
            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                azure_response=web_app,
            )

    return (vulns, True)


@shield_cspm_azure_decorator
async def azure_api_mgmt_svc_does_not_use_a_managed_identity(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.AZURE_API_MGMT_SVC_DOES_NOT_USE_A_MANAGED_IDENTITY
    vulns: Vulnerabilities = ()
    services = await run_azure_client(run_api_management_services, credentials, client_credential)
    for service in services:
        if not service.get("identity"):
            service["identity"] = "None"
            locations = [
                Location(
                    id=service.get("id", ""),
                    access_patterns=("/identity",),
                    values=("None",),
                    method=method,
                ),
            ]
            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                azure_response=service,
            )

    return (vulns, True)


@shield_cspm_azure_decorator
async def azure_key_vault__admin_permissions_on_keys(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.AZURE_KEY_VAULT__ADMIN_PERMISSIONS_ON_KEYS
    vulns: Vulnerabilities = ()
    vaults = await run_azure_client(run_azure_key_vault, credentials, client_credential)
    for vault in vaults:
        properties = vault.get("properties", {})
        enabled_rbac = properties.get("enable_rbac_authorization")
        if enabled_rbac:
            continue
        access_policies = properties.get("access_policies", [])
        for index, access_policy in enumerate(access_policies):
            permissions = access_policy.get("permissions", {})
            keys_permissions = permissions.get("keys", [])
            keys_permissions.sort()
            if keys_permissions == KEY_VAULT__KEY_ACCESS_PERMISSIONS:
                locations = [
                    Location(
                        id=vault.get("id", ""),
                        access_patterns=(f"/properties/access_policies/{index}/permissions/keys",),
                        values=("All",),
                        method=method,
                    ),
                ]
                vulns += build_vulnerabilities(
                    locations=locations,
                    method=method,
                    azure_response=vault,
                )

    return (vulns, True)


@shield_cspm_azure_decorator
async def azure_search_service__public_network_access_is_enabled(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.AZURE_SEARCH_SERVICE__PUBLIC_NETWORK_ACCESS_IS_ENABLED
    vulns: Vulnerabilities = ()
    search_services = await run_azure_client(run_search_services, credentials, client_credential)
    for search_service in search_services:
        public_network_access = search_service.get("public_network_access", "Disabled")
        network_rule_set = search_service.get("network_rule_set", {})
        ip_rules = network_rule_set.get("ip_rules", [])
        if public_network_access.lower() == "enabled" and not ip_rules:
            locations = [
                Location(
                    id=search_service.get("id", ""),
                    access_patterns=("/public_network_access",),
                    values=(public_network_access,),
                    method=method,
                ),
            ]
            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                azure_response=search_service,
            )

    return (vulns, True)


@shield_cspm_azure_decorator
async def azure_cosmos_db__public_network_access_is_enabled(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.AZURE_COSMOS_DB__PUBLIC_NETWORK_ACCESS_IS_ENABLED
    vulns: Vulnerabilities = ()
    cosmos_db_accounts = await run_azure_client(run_azure_cosmos_db, credentials, client_credential)
    for account in cosmos_db_accounts:
        public_network_access = account.get("public_network_access", "Disabled")
        is_virtual_network_filter_enabled = account.get("is_virtual_network_filter_enabled", "")
        invalid_ip_rules = set()
        ip_rules = account.get("ip_rules", [])
        for rule in ip_rules:
            ip_address_or_range = rule.get("ip_address_or_range", "-")
            if not is_valid_ip_address_or_range(ip_address_or_range):
                invalid_ip_rules.add(ip_address_or_range)
        if public_network_access.lower() == "enabled":
            if not is_virtual_network_filter_enabled and not ip_rules:
                locations = [
                    Location(
                        id=account.get("id", ""),
                        access_patterns=("/public_network_access",),
                        values=("All networks",),
                        method=method,
                    ),
                ]
                vulns += build_vulnerabilities(
                    locations=locations,
                    method=method,
                    azure_response=account,
                )
            elif invalid_ip_rules:
                locations = [
                    Location(
                        id=account.get("id", ""),
                        access_patterns=(
                            "/public_network_access",
                            "/ip_rules",
                        ),
                        values=(public_network_access, invalid_ip_rules),
                        method=method,
                    ),
                ]
                vulns += build_vulnerabilities(
                    locations=locations,
                    method=method,
                    azure_response=account,
                )

    return (vulns, True)


@shield_cspm_azure_decorator
async def azure_datafactory__public_network_access_is_enabled(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.AZURE_DATAFACTORY__PUBLIC_NETWORK_ACCESS_IS_ENABLED
    vulns: Vulnerabilities = ()
    data_factories = await run_azure_client(run_azure_datafactory, credentials, client_credential)
    for factory in data_factories:
        public_network_access = factory.get("public_network_access", "Disabled")
        if public_network_access.lower() == "enabled":
            locations = [
                Location(
                    id=factory.get("id", ""),
                    access_patterns=("/public_network_access",),
                    values=(public_network_access,),
                    method=method,
                ),
            ]
            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                azure_response=factory,
            )

    return (vulns, True)


@shield_cspm_azure_decorator
async def azure_api_mgmt_svc__public_network_access_is_enabled(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.AZURE_API_MGMT_SVC__PUBLIC_NETWORK_ACCESS_IS_ENABLED
    vulns: Vulnerabilities = ()
    api_mgmt_services = await run_azure_client(
        run_api_management_services,
        credentials,
        client_credential,
    )
    for service in api_mgmt_services:
        public_network_access = service.get("public_network_access", "Disabled")
        if public_network_access.lower() == "enabled":
            locations = [
                Location(
                    id=service.get("id", ""),
                    access_patterns=("/public_network_access",),
                    values=(public_network_access,),
                    method=method,
                ),
            ]
            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                azure_response=service,
            )

    return (vulns, True)


@shield_cspm_azure_decorator
async def azure_key_vault__public_network_access_is_enabled(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.AZURE_KEY_VAULT__PUBLIC_NETWORK_ACCESS_IS_ENABLED
    vulns: Vulnerabilities = ()
    key_vaults = await run_azure_client(run_azure_key_vault, credentials, client_credential)
    for vault in key_vaults:
        properties = vault.get("properties", {})
        public_network_access = properties.get("public_network_access", "Disabled")
        network_acls = properties.get("network_acls", {})
        if public_network_access.lower() == "enabled" and not network_acls:
            locations = [
                Location(
                    id=vault.get("id", ""),
                    access_patterns=("/properties/public_network_access",),
                    values=(public_network_access,),
                    method=method,
                ),
            ]
            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                azure_response=vault,
            )

    return (vulns, True)


@shield_cspm_azure_decorator
async def azure_api_mgmt_uses_the_triple_des_cipher_algorithm(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.AZURE_API_MGMT_USES_THE_TRIPLE_DES_CIPHER_ALGORITHM
    api_mgmt_services = await run_azure_client(
        run_api_management_services,
        credentials,
        client_credential,
    )
    cipher__key = "Microsoft.WindowsAzure.ApiManagement.Gateway.Security.Ciphers.TripleDes168"
    vulns: Vulnerabilities = ()
    for api_mgmt_service in api_mgmt_services:
        api_mgmt_service_id = api_mgmt_service.get("id", "")
        custom_properties = api_mgmt_service.get("custom_properties", {})
        triple_des_cipher = custom_properties.get(cipher__key, "false")
        if triple_des_cipher.lower() == "true":
            locations = [
                Location(
                    id=api_mgmt_service_id,
                    access_patterns=(f"/custom_properties/{cipher__key}",),
                    values=(triple_des_cipher,),
                    method=method,
                ),
            ]
            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                azure_response=api_mgmt_service,
            )
    return (vulns, True)


@shield_cspm_azure_decorator
async def az_db_psql_flex_server_connection_throttling_disabled(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.AZ_DB_PSQL_FLEX_SERVER_CONNECTION_THROTTLING_DISABLED
    vulns: Vulnerabilities = ()
    dbs_configurations = await run_azure_client(
        run_azure_db_psql_flexible_server_configs_client,
        credentials,
        client_credential,
        config_name="connection_throttle.enable",
    )
    for configuration in dbs_configurations:
        value = configuration.get("value", "on")
        if value.lower() == "off":
            locations = [
                Location(
                    values=(value,),
                    id=configuration["id"],
                    access_patterns=("/value",),
                    method=method,
                ),
            ]
            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                azure_response=configuration,
            )
    return (vulns, True)


@shield_cspm_azure_decorator
async def azure_sql_db_transparent_encryption_is_disabled(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.AZURE_SQL_DB_TRANSPARENT_ENCRYPTION_IS_DISABLED
    vulns: Vulnerabilities = ()
    databases_tde = await run_azure_client(
        run_azure_dbs_transparent_data_encryption,
        client_credential=client_credential,
        credentials=credentials,
    )
    for tde in databases_tde:
        tde_status = tde.get("status", "Enabled")
        if tde_status.lower() == "disabled":
            locations = [
                Location(
                    access_patterns=("/status",),
                    values=(tde_status,),
                    id=tde.get("id", ""),
                    method=method,
                ),
            ]
            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                azure_response=tde,
            )
    return (vulns, True)


@shield_cspm_azure_decorator
async def azure_vm_scale_set_does_not_have_zonal_redundancy(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.AZURE_VM_SCALE_SET_DOES_NOT_HAVE_ZONAL_REDUNDANCY
    vulns: Vulnerabilities = ()
    scale_sets = await run_azure_client(
        run_azure_compute_scale_sets_client,
        credentials,
        client_credential,
    )
    for scale_set in scale_sets:
        zones = scale_set.get("zones", [])
        if len(zones) == 1:
            locations = [
                Location(
                    id=scale_set["id"],
                    access_patterns=("/zones",),
                    values=(zones,),
                    method=method,
                ),
            ]
            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                azure_response=scale_set,
            )

    return (vulns, True)


CHECKS: AzureMethodsTuple = (
    aks_api_server_allows_public_access,
    aks_has_enable_local_accounts,
    aks_has_kubenet_network_plugin,
    aks_has_rbac_disabled,
    aks_is_not_using_latest_version,
    az_db_psql_flex_server_connection_throttling_disabled,
    az_subscription_not_allowed_resource_types_policy,
    azure_api_mgmt_svc__public_network_access_is_enabled,
    azure_api_mgmt_svc_does_not_use_a_managed_identity,
    azure_app_service_always_on_is_not_enabled,
    azure_app_service_does_not_use_a_managed_identity,
    azure_app_service_http2_is_disabled,
    azure_app_service_mutual_tls_is_disabled,
    azure_batch_jobs_runs_in_admin_mode,
    azure_cosmos_db__public_network_access_is_enabled,
    azure_datafactory__public_network_access_is_enabled,
    azure_db_mysql_firewall_allows_public_access,
    azure_db_postgresql_connection_throttling_disabled,
    azure_db_postgresql_firewall_allows_public_access,
    azure_db_postgresql_log_settings_disabled,
    azure_db_sql_extended_audit_disabled,
    azure_db_sql_firewall_allows_public_access,
    azure_db_sql_insecure_audit_retention_period,
    azure_function_app_logging_is_disabled,
    azure_function_app_use_not_host_keys,
    azure_key_vault__admin_permissions_on_keys,
    azure_key_vault__public_network_access_is_enabled,
    azure_keys_expiration_date_is_not_enabled,
    azure_publicly_exposed_funct_app,
    azure_vm_scale_set_does_not_have_zonal_redundancy,
    azure_search_service__public_network_access_is_enabled,
    azure_search_service_does_not_use_a_managed_identity,
    azure_search_service_has_insufficient_replicas_configured,
    azure_secret_expiration_date_is_not_enabled,
    azure_sql_db_transparent_encryption_is_disabled,
    azure_subscription_has_at_least_two_owners,
    azure_vm_encryption_at_host_disabled,
    azure_vm_scale_set_check_ssh_authentication,
    azure_vm_scale_set_encryption_at_host_disabled,
    check_ssh_authentication,
    container_registry_admin_user_enabled,
    container_registry_is_not_using_replication,
    network_application_gateway_waf_is_disabled,
    network_out_of_date_owasp_rules,
    redis_cache_firewall_allows_public_access,
    redis_cache_public_network_access_enabled,
    storage_not_enabled_infrastructure_encryption,
    azure_api_mgmt_uses_the_triple_des_cipher_algorithm,
)
