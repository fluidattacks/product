import json
from collections.abc import (
    Iterable,
)
from typing import (
    Any,
    NamedTuple,
)

from fluidattacks_core.serializers.snippet import (
    SnippetViewport,
    make_snippet,
)
from json_source_map import (
    calculate,
)
from lib_cspm.methods_enum import (
    MethodsEnumCSPM as MethodsEnum,
)
from model.core import (
    Vulnerabilities,
)
from utils.build_vulns import (
    build_inputs_vuln,
    build_metadata,
)
from utils.translations import (
    t,
)


class Location(NamedTuple):
    id: str
    access_patterns: tuple[str, ...]
    method: MethodsEnum
    values: tuple[Any, ...]
    desc_params: dict[str, str] = {}  # noqa: RUF012


def sanitize(specific: str) -> str:
    if "-1" in specific or "-" in specific:
        sanitized = specific.replace("-", "_")
        return sanitized.replace(" _1", " (-1)")
    return specific


def _build_where(location: Location) -> str:
    if len(location.access_patterns) == 1:
        return sanitize(f"{location.access_patterns[0]}: {location.values[0]}")
    return "; ".join(
        [
            sanitize(f"{path.split('/')[-1]}: {location.values[index_path]}")
            for index_path, path in enumerate(location.access_patterns)
        ],
    )


def build_vulnerabilities(
    locations: Iterable[Location],
    method: MethodsEnum,
    azure_response: dict[str, dict | list],
) -> Vulnerabilities:
    str_content = json.dumps(azure_response, indent=4, default=str)
    json_paths = calculate(str_content)

    vulns: Vulnerabilities = ()
    for location in locations:
        description = (
            f"{t(location.method.name, **location.desc_params)} {t(key='words.in')} {location.id}"
        )

        vulns += (
            build_inputs_vuln(
                method=method.value,
                what=location.id,
                where=_build_where(location) if location.access_patterns else description,
                stream="skims",
                metadata=build_metadata(
                    method=method.value,
                    description=description,
                    snippet=make_snippet(
                        content=str_content,
                        viewport=SnippetViewport(
                            column=json_paths[location.access_patterns[-1]].key_start.column
                            if location.access_patterns
                            else 0,
                            line=json_paths[location.access_patterns[-1]].key_start.line + 1
                            if location.access_patterns
                            else 0,
                            wrap=True,
                        ),
                    ).content,
                ),
            ),
        )
    return vulns


FORBIDDEN_PREFIXES = {
    "*",
    "0.0.0.0",  # noqa: S104
    "0.0.0.0/0",
    "/0",
    "255.255.255.255",
    "internet",
    "any",
}


def is_valid_ip_address_or_range(_ip: str) -> bool:
    if _ip:
        return _ip not in FORBIDDEN_PREFIXES
    return False


def check_source_prefixes(rules: dict[str, Any]) -> bool:
    all_rules = {
        rules.get("source_address_prefix", ""),
        *rules.get("source_address_prefixes", []),
    }
    return len(all_rules.intersection(FORBIDDEN_PREFIXES)) > 0


def has_vulnerable_ip_rules(ip_rules: list[dict[str, str]]) -> bool:
    for rule in ip_rules:
        ip_address_or_range = rule.get("ip_address_or_range", "")
        action = rule.get("action", "Deny")
        if not is_valid_ip_address_or_range(ip_address_or_range) and action == "Allow":
            return True
    return False
