from lib_cspm.azure.azure_clients.common import (
    AzureClientSecretCredential as ClientSecretCredential,
)
from lib_cspm.azure.azure_clients.common import (
    AzureMethodsTuple,
    run_azure_client,
)
from lib_cspm.azure.azure_clients.key_vault import (
    run_azure_key_vault,
)
from lib_cspm.azure.azure_clients.rdbms import (
    run_azure_db_postgresql_configs_client,
)
from lib_cspm.azure.azure_clients.storage import (
    run_azure_blob_service_properties,
    run_azure_storage_client,
)
from lib_cspm.azure.shield import (
    shield_cspm_azure_decorator,
)
from lib_cspm.azure.utils import (
    Location,
    build_vulnerabilities,
)
from lib_cspm.methods_enum import (
    MethodsEnumCSPM as MethodsEnum,
)
from model.core import (
    AzureCredentials,
    Vulnerabilities,
)

STORAGE_MINIMUM_RETENTION_DAYS = 7


@shield_cspm_azure_decorator
async def storage_account_geo_replication_disabled(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> tuple:
    method = MethodsEnum.STORAGE_ACCOUNT_GEO_REPLICATION_DISABLED
    vulns: Vulnerabilities = ()
    suggested_sku = [
        "Standard_GRS",
        "Standard_GZRS",
        "Standard_RAGRS",
        "Standard_RAGZRS",
    ]
    storage_accounts = await run_azure_client(
        run_azure_storage_client,
        credentials,
        client_credential,
    )
    for storage_account in storage_accounts:
        current_sku = storage_account["sku"]
        if current_sku["name"] not in suggested_sku:
            locations = [
                Location(
                    id=storage_account["id"],
                    access_patterns=("/sku/name",),
                    values=(current_sku,),
                    method=method,
                ),
            ]
            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                azure_response=storage_account,
            )

    return (vulns, True)


@shield_cspm_azure_decorator
async def blob_service_soft_deleted_disabled(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.BLOB_SERVICE_SOFT_DELETED_DISABLED
    blob_services = await run_azure_client(
        run_azure_blob_service_properties,
        credentials,
        client_credential,
    )
    vulns: Vulnerabilities = ()
    for container in blob_services:
        if (
            not container["delete_retention_policy"]["enabled"]
            or container["delete_retention_policy"]["days"] < STORAGE_MINIMUM_RETENTION_DAYS
        ):
            locations = [
                Location(
                    id=container["id"],
                    access_patterns=("/delete_retention_policy/enabled",)
                    if not container["delete_retention_policy"]["enabled"]
                    else ("/delete_retention_policy/days",),
                    values=(container["delete_retention_policy"]["enabled"],)
                    if not container["delete_retention_policy"]["enabled"]
                    else (container["delete_retention_policy"]["days"],),
                    method=method,
                ),
            ]
            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                azure_response=container,
            )

    return (vulns, True)


@shield_cspm_azure_decorator
async def blob_containers_soft_deleted_disabled(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.BLOB_CONTAINERS_SOFT_DELETED_DISABLED
    blob_services = await run_azure_client(
        run_azure_blob_service_properties,
        credentials,
        client_credential,
    )
    vulns: Vulnerabilities = ()
    for container in blob_services:
        container_delete_retention_policy = container.get("container_delete_retention_policy")
        if container_delete_retention_policy is not None and (
            not container_delete_retention_policy["enabled"]
            or container_delete_retention_policy["days"] < STORAGE_MINIMUM_RETENTION_DAYS
        ):
            locations = [
                Location(
                    id=container["id"],
                    access_patterns=("/container_delete_retention_policy/enabled",)
                    if not container["container_delete_retention_policy"]["enabled"]
                    else ("/container_delete_retention_policy/days",),
                    values=(container["container_delete_retention_policy"]["enabled"],)
                    if not container["container_delete_retention_policy"]["enabled"]
                    else (container["container_delete_retention_policy"]["days"],),
                    method=method,
                ),
            ]
            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                azure_response=container,
            )
        if container_delete_retention_policy is None:
            locations = [
                Location(
                    id=container["id"],
                    access_patterns=(),
                    values=(),
                    method=method,
                ),
            ]
            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                azure_response=container,
            )

    return (vulns, True)


@shield_cspm_azure_decorator
async def azure_key_vault_accidental_purge_prevention_is_disabled(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.AZURE_KEY_VAULT_ACCIDENTAL_PURGE_PREVENTION_IS_DISABLED
    vulns: Vulnerabilities = ()
    vaults = await run_azure_client(run_azure_key_vault, credentials, client_credential)
    for vault in vaults:
        vault_properties = vault.get("properties", {})
        enable_purge_protection = vault_properties.get(
            "enable_purge_protection",
        )
        if not enable_purge_protection:
            vault["properties"]["enable_purge_protection"] = "None"
            locations = [
                Location(
                    id=vault["id"],
                    access_patterns=("/properties/enable_purge_protection",),
                    values=("None",),
                    method=method,
                ),
            ]
            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                azure_response=vault,
            )

    return (vulns, True)


@shield_cspm_azure_decorator
async def azure_db_postgresql_insecure_log_retention_days(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.AZURE_DB_POSTGRESQL_INSECURE_LOG_RETENTION_DAYS
    vulns: Vulnerabilities = ()
    dbs_configurations = await run_azure_client(
        run_azure_db_postgresql_configs_client,
        credentials,
        client_credential,
        config_name="log_retention_days",
    )
    minimum_log_retention_days = "4"
    for configuration in dbs_configurations:
        value = configuration.get("value", "3")
        if value < minimum_log_retention_days:
            locations = [
                Location(
                    values=(value,),
                    id=configuration["id"],
                    access_patterns=("/value",),
                    method=method,
                ),
            ]
            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                azure_response=configuration,
            )
    return (vulns, True)


CHECKS: AzureMethodsTuple = (
    azure_db_postgresql_insecure_log_retention_days,
    storage_account_geo_replication_disabled,
    blob_service_soft_deleted_disabled,
    blob_containers_soft_deleted_disabled,
    azure_key_vault_accidental_purge_prevention_is_disabled,
)
