import functools
import sys
from typing import (
    Any,
    cast,
)

from lib_cspm.azure.azure_clients.common import (
    AzureClientError,
    AzureMethod,
)
from model.core import (
    Vulnerabilities,
)
from utils.logs import (
    log,
    log_to_remote,
)


def shield_cspm_azure_decorator(function: AzureMethod) -> AzureMethod:
    @functools.wraps(function)
    async def wrapper(
        *args: Any,  # noqa: ANN401
        **kwargs: Any,  # noqa: ANN401
    ) -> tuple[Vulnerabilities, bool | str]:
        try:
            return await function(*args, **kwargs)
        except AzureClientError:
            await log(
                "warning",
                "Unable to obtain Azure service information in %s",
                function.__name__,
            )
            return ((), f"azure.{function.__name__}")
        except Exception:  # noqa: BLE001
            exc_type, _, _ = sys.exc_info()
            msg = f"Function {function.__name__} failed with exception: {exc_type}"
            await log("error", msg)

            await log_to_remote(msg=msg, severity="error")
            return ((), f"azure.{function.__name__}")

    return cast(AzureMethod, wrapper)
