from lib_cspm.azure.azure_clients.app_service import (
    run_azure_web_apps_configuration,
)
from lib_cspm.azure.azure_clients.common import (
    AzureClientSecretCredential as ClientSecretCredential,
)
from lib_cspm.azure.azure_clients.common import (
    AzureMethodsTuple,
    run_azure_client,
)
from lib_cspm.azure.shield import (
    shield_cspm_azure_decorator,
)
from lib_cspm.azure.utils import (
    Location,
    build_vulnerabilities,
)
from lib_cspm.methods_enum import (
    MethodsEnumCSPM as MethodsEnum,
)
from model.core import (
    AzureCredentials,
    Vulnerabilities,
)


@shield_cspm_azure_decorator
async def azure_app_service_allows_ftp_deployments(
    credentials: AzureCredentials,
    client_credential: ClientSecretCredential,
) -> tuple[Vulnerabilities, bool]:
    method = MethodsEnum.AZURE_APP_SERVICE_ALLOWS_FTP_DEPLOYMENTS
    vulns: Vulnerabilities = ()
    web_apps_configuration = await run_azure_client(
        run_azure_web_apps_configuration,
        credentials,
        client_credential,
    )
    for app_configuration in web_apps_configuration:
        ftps_state = app_configuration.get("ftps_state")
        if ftps_state == "AllAllowed":
            locations = [
                Location(
                    id=app_configuration["id"],
                    access_patterns=("/ftps_state",),
                    values=(ftps_state,),
                    method=method,
                ),
            ]
            vulns += build_vulnerabilities(
                locations=locations,
                method=method,
                azure_response=app_configuration,
            )

    return (vulns, True)


CHECKS: AzureMethodsTuple = (azure_app_service_allows_ftp_deployments,)
