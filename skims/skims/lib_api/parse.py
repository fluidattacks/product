from lib_api.api_model import (
    APIContext,
    Auth,
    AuthEnum,
    Body,
    DefaultAttribute,
    HttpMethodEnum,
    Query,
    Request,
    RequestBodyModeEnum,
    Response,
)


def parse_default_attribute(attr: dict) -> DefaultAttribute:
    return DefaultAttribute(
        key=attr["key"],
        value=attr.get("value"),
        type=attr.get("type"),
        disabled=attr.get("disabled", False),
    )


def parse_body(body_data: dict) -> Body:
    return Body(
        mode=RequestBodyModeEnum[body_data["mode"].upper()],
        data=body_data.get("raw", ""),
    )


def parse_auth(auth_data: dict) -> Auth:
    auth_type = AuthEnum[auth_data["type"].upper()]
    auth_attributes = [parse_default_attribute(attr) for attr in auth_data.get(auth_type.value, [])]
    return Auth(type=auth_type, data=auth_attributes)


def parse_variables(variables: list) -> dict[str, str]:
    result: dict[str, str] = {}
    for var in variables:
        result[var["key"]] = var["value"]
    return result


def parse_request(request_data: dict) -> Request:
    http_method = HttpMethodEnum[request_data["method"].upper()]
    headers = [parse_default_attribute(header) for header in request_data["header"]]
    body = parse_body(request_data["body"]) if "body" in request_data else None
    auth = parse_auth(request_data["auth"]) if "auth" in request_data else None
    url = request_data["url"]["raw"]
    variables = parse_variables(request_data["url"].get("variable", []))
    return Request(
        url=url,
        http_method=http_method,
        body=body,
        auth=auth,
        headers=headers,
        variable=variables,
    )


def parse_response(response_data: dict) -> Response:
    headers = [parse_default_attribute(header) for header in response_data.get("header", []) or []]
    return Response(
        status_code=response_data.get("code", 0),
        headers=headers,
        response_body=response_data.get("body"),
        response_time=response_data.get("responseTime"),
    )


def parse_queries(item_data: list[dict]) -> tuple[Query, ...]:
    queries = []
    for item in item_data:
        request = parse_request(item["request"])
        responses = [parse_response(response) for response in item.get("response", [])]
        query = Query(
            name=item["name"],
            request=request,
            response=responses[0] if responses else None,
        )
        queries.append(query)
    return tuple(queries)


def parse_json(json_data: dict) -> APIContext:
    return APIContext(
        queries=parse_queries(json_data["item"]),
    )
