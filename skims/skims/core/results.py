import csv
import json
import textwrap
from contextlib import (
    suppress,
)
from pathlib import (
    Path,
)

import ctx
from core.sarif_result import (
    get_sarif,
)
from model.core import (
    DastResponses,
    FindingEnum,
    OutputFormat,
    Vulnerability,
    VulnerabilityTechnique,
)
from tabulate import (
    tabulate,
)
from utils.logs import (
    log_blocking,
)
from utils.state import (
    VulnerabilitiesEphemeralStore,
)
from utils.translations import (
    t,
)


def is_sca_dependency_report(result: Vulnerability) -> bool:
    return result.kind == VulnerabilityTechnique.SCA and result.finding not in {
        FindingEnum.F120,
        FindingEnum.F431,
    }


def get_vuln_snippet(vulnerability: Vulnerability) -> str:
    raw_snippet = (
        vulnerability.skims_metadata.snippet
        if isinstance(vulnerability.skims_metadata.snippet, str)
        else vulnerability.skims_metadata.snippet.content
    )
    snippet = raw_snippet.replace("\x00", "")
    return f"\n{snippet}\n"


def notify_findings_as_sarif(
    stores: VulnerabilitiesEphemeralStore,
    output_path: str,
    requests_stores: DastResponses | None = None,
    cspm_error_stores: dict[str, list[str]] | None = None,
) -> None:
    result = get_sarif(stores, requests_stores, cspm_error_stores)
    with Path(output_path).open("w", encoding="utf-8") as writer:
        json.dump(result, writer, indent=2)

    log_blocking("info", "An output file has been written at %s", output_path)


def notify_findings_as_csv(
    stores: VulnerabilitiesEphemeralStore,
    output: str,
) -> int:
    headers = (
        "title",
        "cwe",
        "description",
        "cvss",
        "cvss_v4",
        "finding",
        "stream",
        "kind",
        "where",
        "snippet",
        "method",
    )

    rows = [
        {
            "cwe": " + ".join(map(str, sorted(result.skims_metadata.cwe_ids))),
            "description": result.skims_metadata.description,
            "kind": result.kind.value,
            "cvss": result.skims_metadata.cvss,
            "cvss_v4": result.skims_metadata.cvss_v4,
            "method": result.skims_metadata.source_method,
            "snippet": get_vuln_snippet(result),
            "stream": result.stream,
            "title": t(result.finding.value.title),
            "where": result.where,
            "finding": (
                "https://help.fluidattacks.com/portal/en/kb/articles/"
                "criteria-vulnerabilities-"
                f"{result.finding.name.replace('F', '')}"
            ),
        }
        for result in stores.read_vulns()
        if result.skims_metadata and not result.skims_metadata.skip
    ]

    summary = (
        f"Summary: {len(rows)} vulnerabilities were found in your targets."
        if len(rows) != 0
        else "Summary: No vulnerabilities were found in your targets."
    )
    with Path(output).open("w", encoding="utf-8") as file:
        writer = csv.DictWriter(file, headers, quoting=csv.QUOTE_MINIMAL)
        writer.writeheader()
        for row in sorted(rows, key=str):
            with suppress(UnicodeEncodeError):
                writer.writerow(row)
        file.write(summary)

    log_blocking("info", "An output file has been written: %s", output)
    return len(rows)


def notify_findings_as_snippets(
    stores: VulnerabilitiesEphemeralStore,
) -> None:
    """Print user-friendly messages about the results found."""
    sca_vulns = []
    for result in stores.read_vulns():
        if result.skims_metadata and not result.skims_metadata.skip:
            if is_sca_dependency_report(result):
                sca_vulns.append(result)
                continue
            title = t(result.finding.value.title)
            kind = result.kind.value
            snippet = (
                result.skims_metadata.snippet
                if isinstance(result.skims_metadata.snippet, str)
                else result.skims_metadata.snippet.content
            )
            cwe = " + ".join(map(str, sorted(result.skims_metadata.cwe_ids)))
            cvss = result.skims_metadata.cvss
            cvss_v4 = result.skims_metadata.cvss_v4
            vuln_url = (
                "https://help.fluidattacks.com/portal/en/kb/articles/"
                "criteria-vulnerabilities-"
                f"{result.finding.name.replace('F', '')}"
            )
            log_blocking(
                "info",
                f"{title}\n"
                f"{result.skims_metadata.description}\n"
                f"\n{snippet}\n"
                f"{cwe} - {cvss} - {cvss_v4}\n"
                f"Report found by {kind} module.\n"
                f"More information in: {vuln_url}\n",
            )
    notify_sca_table(sca_vulns)


def notify_sca_table(vulns: list[Vulnerability]) -> None:
    if len(vulns) > 0:
        rows = [
            {
                "package": sca_metadata.package,
                "version": sca_metadata.vulnerable_version,
                "cve": "\n".join(map(str, sorted(sca_metadata.cve))),
                "path": f"{result.what}:{result.where}",
                "cvss": result.skims_metadata.cvss,
                "cvss4": result.skims_metadata.cvss_v4,
            }
            for result in vulns
            if (sca_metadata := result.skims_metadata.sca_metadata)
        ]
        max_width = 8

        wrapped_data = [
            {key: textwrap.fill(str(value), width=max_width) for key, value in row.items()}
            for row in rows
        ]
        table = tabulate(
            wrapped_data,
            headers="keys",
            tablefmt="simple_grid",
        )

        log_blocking("info", "SCA vulnerabilities found: \n%s", table)


def report_results(
    stores: VulnerabilitiesEphemeralStore,
    requests_stores: DastResponses | None,
    cspm_error_stores: dict[str, list[str]] | None,
) -> None:
    if ctx.SKIMS_CONFIG.output:
        if ctx.SKIMS_CONFIG.output.format == OutputFormat.ALL:
            notify_findings_as_csv(
                stores=stores,
                output=ctx.SKIMS_CONFIG.output.file_path + ".csv",
            )
            notify_findings_as_sarif(
                stores=stores,
                output_path=ctx.SKIMS_CONFIG.output.file_path + ".sarif",
                requests_stores=requests_stores,
                cspm_error_stores=cspm_error_stores,
            )
        elif ctx.SKIMS_CONFIG.output.format == OutputFormat.CSV:
            notify_findings_as_csv(stores=stores, output=ctx.SKIMS_CONFIG.output.file_path)
        elif ctx.SKIMS_CONFIG.output.format == OutputFormat.SARIF:
            notify_findings_as_sarif(
                stores=stores,
                output_path=ctx.SKIMS_CONFIG.output.file_path,
                requests_stores=requests_stores,
                cspm_error_stores=cspm_error_stores,
            )
    else:
        notify_findings_as_snippets(stores)
