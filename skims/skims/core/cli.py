import sys
from functools import (
    partial,
)
from pathlib import Path
from time import (
    time,
)

import click
import ctx
from aioextensions import (
    run,
)
from core.config import (
    set_execution_config,
)
from core.scan import (
    main as skims_main,
)
from model.core import (
    OutputFormat,
    SkimsModules,
    SkimsOutputConfig,
)
from utils.bugs import (
    add_bugsnag_data,
    initialize_bugsnag,
)
from utils.env import (
    is_fluid_batch_env,
)
from utils.function import (
    shield_blocking,
)
from utils.logs import (
    configure as configure_logs,
)
from utils.logs import (
    log_blocking,
    log_to_remote_blocking,
    set_tracing_handler,
)

# Reusable components
CONFIG_PATH = partial(
    click.argument,
    "config_path",
    type=click.Path(
        allow_dash=False,
        dir_okay=False,
        exists=True,
        file_okay=True,
        readable=True,
        resolve_path=True,
    ),
)


class Config:
    strict_run: bool = False


@click.group(
    help=(
        "Deterministic vulnerability analysis and reporting tool. \n"
        "Find our documentation at "
        "https://help.fluidattacks.com/portal/en/kb/find-security-vulnerabilities/use-the-cli"
    ),
    epilog=("For legal information read https://dev.fluidattacks.com/components/skims/legal"),
)
@click.option(
    "--strict",
    help="Enable strict mode.",
    is_flag=True,
)
def cli(
    strict: bool,  # noqa: FBT001
) -> None:
    Config.strict_run = strict


@cli.command(
    help=(
        "Perform vulnerability detection.\n"
        "Receives an argument which can be the "
        "path to a configuration file or directory"
    ),
    name="scan",
)
@click.argument("arg")
@click.option(
    "--csv",
    help=(
        "The output of the scan will be in csv format, creating a "
        "skims_results.csv file in the path that was executed."
    ),
    is_flag=True,
)
@click.option(
    "--sarif",
    help=(
        "The output of the scan will be in sarif format, creating a "
        "skims_results.sarif file in the path that was executed."
    ),
    is_flag=True,
)
@click.option(
    "--execution-module",
    help=(
        "Specify the module to exclusively perform checks for (e.g. apk, dast, cspm, sast, sca)."
    ),
    metavar="<module>",
)
@click.option(
    "--reachability_input",
    help=(
        "Specify the path to the sbom results file to be used for reachability analysis. "
        "This is applicable only when the SAST module is enabled."
    ),
    metavar="<path>",
)
def cli_scan(
    arg: str,
    csv: bool,  # noqa: FBT001
    sarif: bool,  # noqa: FBT001
    execution_module: str,
    reachability_input: str | None,
) -> None:
    output_config = None
    cwd = arg if Path(arg).is_dir() else Path.cwd()
    if csv and sarif:
        output_config = SkimsOutputConfig(file_path=f"{cwd}/skims_results", format=OutputFormat.ALL)
    elif csv:
        output_config = SkimsOutputConfig(
            file_path=f"{cwd}/skims_results.csv",
            format=OutputFormat.CSV,
        )
    elif sarif:
        output_config = SkimsOutputConfig(
            file_path=f"{cwd}/skims_results.sarif",
            format=OutputFormat.SARIF,
        )

    scan_modules = validate_execution_modules(execution_module)

    start_time = time()
    success = cli_scan_wrapped(arg, output_config, scan_modules, reachability_input)

    if is_fluid_batch_env() and not success[0]:
        msg = f"Skims Analysis failed on {time() - start_time}"
        log_to_remote_blocking(msg=msg, severity="error")

    handle_scan_result(success)


def validate_execution_modules(execution_module: str | None) -> SkimsModules:
    if execution_module:
        modules_to_run = execution_module.split(",")
        return SkimsModules(
            api="api" in modules_to_run,
            apk="apk" in modules_to_run,
            cspm="cspm" in modules_to_run,
            dast="dast" in modules_to_run,
            sast="sast" in modules_to_run,
            sca="sca" in modules_to_run,
        )
    return SkimsModules(api=True, apk=True, cspm=True, dast=True, sast=True, sca=True)


def handle_scan_result(success: tuple[bool, int]) -> None:
    if not success[0]:
        if success[1] == 2:
            sys.exit(2)
        log_blocking(
            "info",
            "Summary: An error occurred while analyzing your targets.",
        )
        sys.exit(1)
    elif success[1] > 0:
        log_blocking(
            "info",
            "Summary: %s vulnerabilities were found in your targets.",
            success[1],
        )
        if ctx.SKIMS_CONFIG.strict or Config.strict_run:
            sys.exit(1)
        else:
            sys.exit(0)
    else:
        log_blocking(
            "info",
            "Summary: No vulnerabilities were found in your targets.",
        )
        sys.exit(0)


@shield_blocking(on_error_return=(False, 0))
def cli_scan_wrapped(
    arg: str,
    output_config: SkimsOutputConfig | None,
    scan_modules: SkimsModules,
    sbom_path: str | None = None,
) -> tuple[bool, int]:
    configure_logs()

    loaded_config = set_execution_config(arg, output_config, scan_modules, sbom_path)
    if not loaded_config:
        log_blocking(
            "error",
            (
                "Wrong scan argument. Please see: "
                "https://help.fluidattacks.com/portal/en/kb/articles/"
                "configure-the-tests-by-the-standalone-scanner#scan_arguments"
            ),
        )
        return False, 2

    ctx.SKIMS_CONFIG = loaded_config
    if not ctx.SKIMS_CONFIG.tracing_opt_out:
        log_blocking(
            "warning",
            (
                "Although skims does not collect any sensitive information, "
                "if you do not want to send any data to our servers, set the "
                "`tracing_opt_out` key to `True` in your configuration file"
            ),
        )

        initialize_bugsnag()
        add_bugsnag_data(config=arg)
        add_bugsnag_data(namespace=ctx.SKIMS_CONFIG.namespace)

        set_tracing_handler()

    return run(skims_main())


if __name__ == "__main__":
    cli(
        prog_name="skims",
    )
