import hashlib
import re
from enum import (
    Enum,
)
from typing import (
    NamedTuple,
)

from fluidattacks_core.serializers.snippet import (
    Snippet,
)


class FindingMetadata(NamedTuple):
    cwe: str  # must have the format CWE-{number}, ex CWE-139
    description: str
    impact: str
    recommendation: str
    requirements: list[int]
    threat: str
    title: str
    sca_exclusive: bool

    @classmethod
    def new(
        cls,
        *,
        code: str,
        cwe: str,
        requirements: list[int],
        sca_exclusive: bool = False,
    ) -> "FindingMetadata":
        return FindingMetadata(
            cwe=cwe,
            description=f"criteria.vulns.{code[1:]}.description",
            impact=f"criteria.vulns.{code[1:]}.impact",
            recommendation=f"criteria.vulns.{code[1:]}.recommendation",
            requirements=requirements,
            threat=f"criteria.vulns.{code[1:]}.threat",
            title=f"criteria.vulns.{code[1:]}.title",
            sca_exclusive=sca_exclusive,
        )


class FindingEnum(Enum):
    F001 = FindingMetadata.new(
        code="F001",
        cwe="CWE-89",
        requirements=[169, 173],
    )
    F002 = FindingMetadata.new(
        code="F002",
        cwe="CWE-770",
        requirements=[72, 327],
    )
    F004 = FindingMetadata.new(
        code="F004",
        cwe="CWE-78",
        requirements=[173, 265, 266],
    )
    F005 = FindingMetadata.new(
        code="F005",
        cwe="CWE-78",
        requirements=[35],
    )
    F006 = FindingMetadata.new(
        code="F006",
        cwe="CWE-1390",
        requirements=[227, 228, 229, 231, 235, 264, 319],
    )
    F007 = FindingMetadata.new(
        code="F007",
        cwe="CWE-352",
        requirements=[29, 174],
    )
    F008 = FindingMetadata.new(
        code="F008",
        cwe="CWE-79",
        requirements=[29, 173],
    )
    F009 = FindingMetadata.new(
        code="F009",
        cwe="CWE-798",
        requirements=[145, 156, 266],
    )
    F010 = FindingMetadata.new(
        code="F010",
        cwe="CWE-79",
        requirements=[29, 173],
    )
    F011 = FindingMetadata.new(
        code="F011",
        cwe="CWE-1395",
        requirements=[262],
    )
    F012 = FindingMetadata.new(
        code="F012",
        cwe="CWE-89",
        requirements=[169, 173],
    )
    F013 = FindingMetadata.new(
        code="F013",
        cwe="CWE-706",
        requirements=[176],
        sca_exclusive=True,
    )
    F014 = FindingMetadata.new(
        code="F014",
        cwe="CWE-440",
        requirements=[266],
        sca_exclusive=True,
    )
    F015 = FindingMetadata.new(
        code="F015",
        cwe="CWE-319",
        requirements=[30, 228, 319],
    )
    F016 = FindingMetadata.new(
        code="F016",
        cwe="CWE-327",
        requirements=[148, 149, 150, 181, 336],
    )
    F017 = FindingMetadata.new(
        code="F017",
        cwe="CWE-319",
        requirements=[32, 181],
    )
    F020 = FindingMetadata.new(
        code="F020",
        cwe="CWE-312",
        requirements=[134, 135, 185, 229, 264, 300],
    )
    F021 = FindingMetadata.new(
        code="F021",
        cwe="CWE-643",
        requirements=[173],
    )
    F023 = FindingMetadata.new(
        code="F023",
        cwe="CWE-601",
        requirements=[173, 324],
    )
    F024 = FindingMetadata.new(
        code="F024",
        cwe="CWE-1327",
        requirements=[255],
    )
    F026 = FindingMetadata.new(
        code="F026",
        cwe="CWE-203",
        requirements=[225],
        sca_exclusive=True,
    )
    F027 = FindingMetadata.new(
        code="F027",
        cwe="CWE-434",
        requirements=[40, 41],
    )
    F028 = FindingMetadata.new(
        code="F028",
        cwe="CWE-377",
        requirements=[36, 177],
        sca_exclusive=True,
    )
    F029 = FindingMetadata.new(
        code="F029",
        cwe="CWE-770",
        requirements=[39],
    )
    F031 = FindingMetadata.new(
        code="F031",
        cwe="CWE-250",
        requirements=[95, 95, 186],
    )
    F032 = FindingMetadata.new(
        code="F032",
        cwe="CWE-290",
        requirements=[35, 96, 173, 176, 265, 320],
        sca_exclusive=True,
    )
    F033 = FindingMetadata.new(
        code="F033",
        cwe="CWE-640",
        requirements=[223, 224],
        sca_exclusive=True,
    )
    F034 = FindingMetadata.new(
        code="F034",
        cwe="CWE-338",
        requirements=[223, 224],
    )
    F035 = FindingMetadata.new(
        code="F035",
        cwe="CWE-521",
        requirements=[130, 132, 133, 139, 332],
    )
    F036 = FindingMetadata.new(
        code="F036",
        cwe="CWE-319",
        requirements=[26],
    )
    F037 = FindingMetadata.new(
        code="F037",
        cwe="CWE-497",
        requirements=[77, 176],
    )
    F038 = FindingMetadata.new(
        code="F038",
        cwe="CWE-359",
        requirements=[176, 177, 261, 300],
        sca_exclusive=True,
    )
    F039 = FindingMetadata.new(
        code="F039",
        cwe="CWE-639",
        requirements=[96, 176, 265, 320],
        sca_exclusive=True,
    )
    F042 = FindingMetadata.new(
        code="F042",
        cwe="CWE-614",
        requirements=[29],
    )
    F043 = FindingMetadata.new(
        code="F043",
        cwe="CWE-079",
        requirements=[62, 117, 175, 349],
    )
    F044 = FindingMetadata.new(
        code="F044",
        cwe="CWE-650",
        requirements=[266],
    )
    F047 = FindingMetadata.new(
        code="F047",
        cwe="CWE-203",
        requirements=[237, 266, 327],
        sca_exclusive=True,
    )
    F052 = FindingMetadata.new(
        code="F052",
        cwe="CWE-327",
        requirements=[148, 149, 150, 181, 336],
    )
    F053 = FindingMetadata.new(
        code="F053",
        cwe="CWE-307",
        requirements=[237, 327],
        sca_exclusive=True,
    )
    F055 = FindingMetadata.new(
        code="F055",
        cwe="CWE-530",
        requirements=[185, 266],
    )
    F056 = FindingMetadata.new(
        code="F056",
        cwe="CWE-306",
        requirements=[142, 264, 265, 266, 319],
    )
    F058 = FindingMetadata.new(
        code="F058",
        cwe="CWE-489",
        requirements=[77, 78],
    )
    F059 = FindingMetadata.new(
        code="F059",
        cwe="CWE-532",
        requirements=[83],
        sca_exclusive=True,
    )
    F060 = FindingMetadata.new(
        code="F060",
        cwe="CWE-940",
        requirements=[266],
    )
    F061 = FindingMetadata.new(
        code="F061",
        cwe="CWE-98",
        requirements=[173, 265, 266],
        sca_exclusive=True,
    )
    F062 = FindingMetadata.new(
        code="F060",
        cwe="CWE-384",
        requirements=[25],
    )
    F063 = FindingMetadata.new(
        code="F063",
        cwe="CWE-22",
        requirements=[173, 320, 342],
    )
    F064 = FindingMetadata.new(
        code="F064",
        cwe="CWE-778",
        requirements=[75, 320],
    )
    F065 = FindingMetadata.new(
        code="F065",
        cwe="CWE-525",
        requirements=[177],
    )
    F067 = FindingMetadata.new(
        code="F067",
        cwe="CWE-770",
        requirements=[72, 158, 164, 173],
    )
    F068 = FindingMetadata.new(
        code="F068",
        cwe="CWE-613",
        requirements=[23],
    )
    F069 = FindingMetadata.new(
        code="F069",
        cwe="CWE-804",
        requirements=[237],
        sca_exclusive=True,
    )
    F071 = FindingMetadata.new(
        code="F071",
        cwe="CWE-523",
        requirements=[62, 349],
    )
    F073 = FindingMetadata.new(
        code="F073",
        cwe="CWE-863",
        requirements=[96, 176, 265],
    )
    F075 = FindingMetadata.new(
        code="F075",
        cwe="CWE-552",
        requirements=[96, 176, 264, 320],
    )
    F076 = FindingMetadata.new(
        code="F076",
        cwe="CWE-613",
        requirements=[30, 31, 141],
        sca_exclusive=True,
    )
    F078 = FindingMetadata.new(
        code="F078",
        cwe="CWE-1270",
        requirements=[224],
    )
    F079 = FindingMetadata.new(
        code="F079",
        cwe="CWE-1395",
        requirements=[302],
    )
    F081 = FindingMetadata.new(
        code="F081",
        cwe="CWE-308",
        requirements=[229, 231, 264, 319, 328],
    )
    F082 = FindingMetadata.new(
        code="F082",
        cwe="CWE-459",
        requirements=[183],
    )
    F083 = FindingMetadata.new(
        code="F083",
        cwe="CWE-611",
        requirements=[173],
    )
    F085 = FindingMetadata.new(
        code="F085",
        cwe="CWE-922",
        requirements=[177, 329],
    )
    F086 = FindingMetadata.new(
        code="F086",
        cwe="CWE-354",
        requirements=[178, 262, 330],
    )
    F087 = FindingMetadata.new(
        code="F087",
        cwe="CWE-645",
        requirements=[226],
        sca_exclusive=True,
    )
    F089 = FindingMetadata.new(
        code="F089",
        cwe="CWE-501",
        requirements=[173, 320, 342],
    )
    F090 = FindingMetadata.new(
        code="F090",
        cwe="CWE-1236",
        requirements=[173],
        sca_exclusive=True,
    )
    F091 = FindingMetadata.new(
        code="F091",
        cwe="CWE-117",
        requirements=[80, 173],
    )
    F094 = FindingMetadata.new(
        code="F094",
        cwe="CWE-327",
        requirements=[148, 149, 150, 181, 336],
    )
    F096 = FindingMetadata.new(
        code="F096",
        cwe="CWE-502",
        requirements=[173, 321],
    )
    F097 = FindingMetadata.new(
        code="F097",
        cwe="CWE-601",
        requirements=[173, 324],
    )
    F098 = FindingMetadata.new(
        code="F098",
        cwe="CWE-22",
        requirements=[37, 320],
    )
    F099 = FindingMetadata.new(
        code="F099",
        cwe="CWE-312",
        requirements=[134, 135, 185, 227, 229, 264, 300],
    )
    F100 = FindingMetadata.new(
        code="F100",
        cwe="CWE-918",
        requirements=[173, 324],
    )
    F101 = FindingMetadata.new(
        code="F101",
        cwe="CWE-732",
        requirements=[186, 265],
    )
    F103 = FindingMetadata.new(
        code="F103",
        cwe="CWE-347",
        requirements=[122, 173, 178, 320],
    )
    F106 = FindingMetadata.new(
        code="F106",
        cwe="CWE-943",
        requirements=[109, 173],
    )
    F107 = FindingMetadata.new(
        code="F107",
        cwe="CWE-90",
        requirements=[173],
    )
    F108 = FindingMetadata.new(
        code="F108",
        cwe="CWE-799",
        requirements=[327],
    )
    F109 = FindingMetadata.new(
        code="F109",
        cwe="CWE-1327",
        requirements=[255],
    )
    F110 = FindingMetadata.new(
        code="F110",
        cwe="CWE-444",
        requirements=[62, 173, 266],
        sca_exclusive=True,
    )
    F111 = FindingMetadata.new(
        code="F111",
        cwe="CWE-125",
        requirements=[111],
    )
    F112 = FindingMetadata.new(
        code="F112",
        cwe="CWE-89",
        requirements=[169, 173],
    )
    F113 = FindingMetadata.new(
        code="F113",
        cwe="CWE-704",
        requirements=[164],
        sca_exclusive=True,
    )
    F115 = FindingMetadata.new(
        code="F115",
        cwe="CWE-288",
        requirements=[62, 266, 273],
    )
    F117 = FindingMetadata.new(
        code="F117",
        cwe="CWE-912",
        requirements=[323],
    )
    F119 = FindingMetadata.new(
        code="F119",
        cwe="CWE-1230",
        requirements=[45],
        sca_exclusive=True,
    )
    F120 = FindingMetadata.new(
        code="F120",
        cwe="CWE-1395",
        requirements=[302],
    )
    F123 = FindingMetadata.new(
        code="F123",
        cwe="CWE-548",
        requirements=[173, 176],
    )
    F124 = FindingMetadata.new(
        code="F124",
        cwe="CWE-362",
        requirements=[337],
        sca_exclusive=True,
    )
    F128 = FindingMetadata.new(
        code="F128",
        cwe="CWE-1004",
        requirements=[29],
    )
    F129 = FindingMetadata.new(
        code="F129",
        cwe="CWE-1275",
        requirements=[29],
    )
    F130 = FindingMetadata.new(
        code="F130",
        cwe="CWE-614",
        requirements=[29],
    )
    F131 = FindingMetadata.new(
        code="F131",
        cwe="CWE-319",
        requirements=[62, 181, 349],
    )
    F132 = FindingMetadata.new(
        code="F132",
        cwe="CWE-173",
        requirements=[62, 349],
    )
    F133 = FindingMetadata.new(
        code="F133",
        cwe="CWE-326",
        requirements=[148, 149, 150, 181, 336],
    )
    F134 = FindingMetadata.new(
        code="F134",
        cwe="CWE-942",
        requirements=[62, 266, 349],
    )
    F135 = FindingMetadata.new(
        code="F135",
        cwe="CWE-79",
        requirements=[62, 175, 266, 349],
    )
    F136 = FindingMetadata.new(
        code="F136",
        cwe="CWE-525",
        requirements=[62, 177, 266, 349],
    )
    F137 = FindingMetadata.new(
        code="F137",
        cwe="CWE-942",
        requirements=[62, 266, 349],
    )
    F138 = FindingMetadata.new(
        code="F138",
        cwe="CWE-391",
        requirements=[156, 168, 266, 302, 342],
        sca_exclusive=True,
    )
    F140 = FindingMetadata.new(
        code="F140",
        cwe="CWE-248",
        requirements=[161, 266, 359],
        sca_exclusive=True,
    )
    F143 = FindingMetadata.new(
        code="F143",
        cwe="CWE-95",
        requirements=[266],
    )
    F146 = FindingMetadata.new(
        code="F146",
        cwe="CWE-89",
        requirements=[169, 173],
    )
    F148 = FindingMetadata.new(
        code="F148",
        cwe="CWE-319",
        requirements=[181],
    )
    F149 = FindingMetadata.new(
        code="F149",
        cwe="CWE-319",
        requirements=[181],
    )
    F151 = FindingMetadata.new(
        code="F151",
        cwe="CWE-319",
        requirements=[181],
    )
    F152 = FindingMetadata.new(
        code="F152",
        cwe="CWE-693",
        requirements=[62, 175, 266, 349],
    )
    F153 = FindingMetadata.new(
        code="F153",
        cwe="CWE-173",
        requirements=[62, 266, 349],
    )
    F156 = FindingMetadata.new(
        code="F156",
        cwe="CWE-601",
        requirements=[173, 324],
    )
    F157 = FindingMetadata.new(
        code="F157",
        cwe="CWE-1327",
        requirements=[255],
    )
    F158 = FindingMetadata.new(
        code="F158",
        cwe="CWE-1327",
        requirements=[255],
    )
    F159 = FindingMetadata.new(
        code="F159",
        cwe="CWE-266",
        requirements=[95, 96, 186],
    )
    F160 = FindingMetadata.new(
        code="F160",
        cwe="CWE-378",
        requirements=[95, 96, 186],
    )
    F163 = FindingMetadata.new(
        code="F163",
        cwe="CWE-295",
        requirements=[88, 89, 9, 91, 92, 93],
        sca_exclusive=True,
    )
    F164 = FindingMetadata.new(
        code="F164",
        cwe="CWE-1188",
        requirements=[185, 266],
    )
    F165 = FindingMetadata.new(
        code="F165",
        cwe="CWE-306",
        requirements=[185, 265, 266],
    )
    F168 = FindingMetadata.new(
        code="F168",
        cwe="CWE-798",
        requirements=[266],
    )
    F169 = FindingMetadata.new(
        code="F169",
        cwe="CWE-798",
        requirements=[145],
    )
    F177 = FindingMetadata.new(
        code="F177",
        cwe="CWE-842",
        requirements=[266],
    )
    F182 = FindingMetadata.new(
        code="F182",
        cwe="CWE-290",
        requirements=[62, 273],
    )
    F183 = FindingMetadata.new(
        code="F183",
        cwe="CWE-489",
        requirements=[77, 78],
    )
    F184 = FindingMetadata.new(
        code="F184",
        cwe="CWE-436",
        requirements=[173, 320, 342],
        sca_exclusive=True,
    )
    F188 = FindingMetadata.new(
        code="F188",
        cwe="CWE-79",
        requirements=[173, 320, 342],
    )
    F200 = FindingMetadata.new(
        code="F200",
        cwe="CWE-778",
        requirements=[75, 320],
    )
    F201 = FindingMetadata.new(
        code="F201",
        cwe="CWE-552",
        requirements=[96, 176, 264, 320],
        sca_exclusive=True,
    )
    F203 = FindingMetadata.new(
        code="F203",
        cwe="CWE-552",
        requirements=[96, 176, 264, 320],
    )
    F204 = FindingMetadata.new(
        code="F204",
        cwe="CWE-347",
        requirements=[96, 176, 264, 320],
        sca_exclusive=True,
    )
    F211 = FindingMetadata.new(
        code="F211",
        cwe="CWE-770",
        requirements=[72, 327],
    )
    F234 = FindingMetadata.new(
        code="F234",
        cwe="CWE-209",
        requirements=[77, 176],
    )
    F235 = FindingMetadata.new(
        code="F235",
        cwe="CWE-497",
        requirements=[77, 176],
    )
    F236 = FindingMetadata.new(
        code="F236",
        cwe="CWE-497",
        requirements=[77, 176],
    )
    F239 = FindingMetadata.new(
        code="F239",
        cwe="CWE-209",
        requirements=[77, 176],
    )
    F246 = FindingMetadata.new(
        code="F246",
        cwe="CWE-312",
        requirements=[134, 135, 185, 229, 264, 300],
    )
    F247 = FindingMetadata.new(
        code="F247",
        cwe="CWE-312",
        requirements=[134, 135, 185, 229, 264, 300],
    )
    F249 = FindingMetadata.new(
        code="F249",
        cwe="CWE-798",
        requirements=[134, 135, 185, 229, 264, 300],
    )
    F250 = FindingMetadata.new(
        code="F250",
        cwe="CWE-313",
        requirements=[266],
    )
    F256 = FindingMetadata.new(
        code="F256",
        cwe="CWE-732",
        requirements=[186, 265],
    )
    F257 = FindingMetadata.new(
        code="F257",
        cwe="CWE-732",
        requirements=[186, 265],
    )
    F258 = FindingMetadata.new(
        code="F258",
        cwe="CWE-732",
        requirements=[186, 265],
    )
    F259 = FindingMetadata.new(
        code="F259",
        cwe="CWE-732",
        requirements=[186, 265],
    )
    F262 = FindingMetadata.new(
        code="F262",
        cwe="CWE-328",
        requirements=[148, 150],
    )
    F265 = FindingMetadata.new(
        code="F265",
        cwe="CWE-327",
        requirements=[148, 150],
    )
    F266 = FindingMetadata.new(
        code="F266",
        cwe="CWE-250",
        requirements=[95, 96, 186],
    )
    F267 = FindingMetadata.new(
        code="F267",
        cwe="CWE-250",
        requirements=[95, 96, 186],
    )
    F268 = FindingMetadata.new(
        code="F268",
        cwe="CWE-749",
        requirements=[266],
    )
    F274 = FindingMetadata.new(
        code="F274",
        cwe="CWE-639",
        requirements=[173, 320],
        sca_exclusive=True,
    )
    F275 = FindingMetadata.new(
        code="F275",
        cwe="CWE-922",
        requirements=[185],
    )
    F277 = FindingMetadata.new(
        code="F277",
        cwe="CWE-521",
        requirements=[130, 132, 133, 139, 332],
    )
    F280 = FindingMetadata.new(
        code="F280",
        cwe="CWE-384",
        requirements=[30],
    )
    F281 = FindingMetadata.new(
        code="F281",
        cwe="CWE-319",
        requirements=[181],
    )
    F282 = FindingMetadata.new(
        code="F282",
        cwe="CWE-327",
        requirements=[148, 150],
    )
    F289 = FindingMetadata.new(
        code="F289",
        cwe="CWE-1295",
        requirements=[77, 176],
        sca_exclusive=True,
    )
    F297 = FindingMetadata.new(
        code="F297",
        cwe="CWE-89",
        requirements=[169, 173],
    )
    F300 = FindingMetadata.new(
        code="F300",
        cwe="CWE-306",
        requirements=[227, 228, 229, 231, 235, 264, 323],
    )
    F308 = FindingMetadata.new(
        code="F308",
        cwe="CWE-200",
        requirements=[266],
        sca_exclusive=True,
    )
    F309 = FindingMetadata.new(
        code="F309",
        cwe="CWE-347",
        requirements=[228],
    )
    F310 = FindingMetadata.new(
        code="F310",
        cwe="CWE-200",
        requirements=[264],
        sca_exclusive=True,
    )
    F313 = FindingMetadata.new(
        code="F313",
        cwe="CWE-295",
        requirements=[266],
    )
    F316 = FindingMetadata.new(
        code="F316",
        cwe="CWE-787",
        requirements=[72, 158, 164, 173],
        sca_exclusive=True,
    )
    F319 = FindingMetadata.new(
        code="F319",
        cwe="CWE-266",
        requirements=[266],
    )
    F320 = FindingMetadata.new(
        code="F320",
        cwe="CWE-90",
        requirements=[266],
    )
    F324 = FindingMetadata.new(
        code="F324",
        cwe="CWE-639",
        requirements=[266],
        sca_exclusive=True,
    )
    F325 = FindingMetadata.new(
        code="F325",
        cwe="CWE-250",
        requirements=[95, 96, 186],
    )
    F332 = FindingMetadata.new(
        code="F332",
        cwe="CWE-319",
        requirements=[181],
    )
    F333 = FindingMetadata.new(
        code="F333",
        cwe="CWE-497",
        requirements=[266],
    )
    F335 = FindingMetadata.new(
        code="F335",
        cwe="CWE-922",
        requirements=[266],
    )
    F338 = FindingMetadata.new(
        code="F338",
        cwe="CWE-760",
        requirements=[266],
    )
    F343 = FindingMetadata.new(
        code="F343",
        cwe="CWE-327",
        requirements=[266],
    )
    F344 = FindingMetadata.new(
        code="F344",
        cwe="CWE-78",
        requirements=[173, 320, 357],
    )
    F346 = FindingMetadata.new(
        code="F346",
        cwe="CWE-272",
        requirements=[95, 96, 186],
    )
    F347 = FindingMetadata.new(
        code="F347",
        cwe="CWE-250",
        requirements=[266],
    )
    F350 = FindingMetadata.new(
        code="F350",
        cwe="CWE-295",
        requirements=[88, 89, 90, 91, 92, 93],
    )
    F353 = FindingMetadata.new(
        code="F353",
        cwe="CWE-347",
        requirements=[173, 320, 357],
    )
    F354 = FindingMetadata.new(
        code="F354",
        cwe="CWE-770",
        requirements=[40, 41],
    )
    F358 = FindingMetadata.new(
        code="F358",
        cwe="CWE-611",
        requirements=[158],
    )
    F359 = FindingMetadata.new(
        code="F359",
        cwe="CWE-798",
        requirements=[145, 156, 266],
    )
    F360 = FindingMetadata.new(
        code="F360",
        cwe="CWE-1021",
        requirements=[175],
        sca_exclusive=True,
    )
    F362 = FindingMetadata.new(
        code="F362",
        cwe="CWE-497",
        requirements=[77, 176],
    )
    F363 = FindingMetadata.new(
        code="F363",
        cwe="CWE-521",
        requirements=[130, 132, 133, 139, 332],
    )
    F368 = FindingMetadata.new(
        code="F368",
        cwe="CWE-295",
        requirements=[255],
    )
    F371 = FindingMetadata.new(
        code="F371",
        cwe="CWE-79",
        requirements=[173],
    )
    F372 = FindingMetadata.new(
        code="F372",
        cwe="CWE-319",
        requirements=[181],
    )
    F380 = FindingMetadata.new(
        code="F380",
        cwe="CWE-494",
        requirements=[266],
    )
    F385 = FindingMetadata.new(
        code="F385",
        cwe="CWE-312",
        requirements=[134, 135, 185],
    )
    F390 = FindingMetadata.new(
        code="F390",
        cwe="CWE-1321",
        requirements=[173],
    )
    F392 = FindingMetadata.new(
        code="F392",
        cwe="CWE-602",
        requirements=[62, 266, 273],
    )
    F393 = FindingMetadata.new(
        code="F393",
        cwe="CWE-1395",
        requirements=[48, 262],
    )
    F394 = FindingMetadata.new(
        code="F394",
        cwe="CWE-117",
        requirements=[80],
    )
    F395 = FindingMetadata.new(
        code="F395",
        cwe="CWE-329",
        requirements=[223, 224],
    )
    F396 = FindingMetadata.new(
        code="F396",
        cwe="CWE-262",
        requirements=[266],
    )
    F398 = FindingMetadata.new(
        code="F398",
        cwe="CWE-470",
        requirements=[266, 173],
    )
    F400 = FindingMetadata.new(
        code="F400",
        cwe="CWE-778",
        requirements=[75, 376, 377, 378],
    )
    F401 = FindingMetadata.new(
        code="F401",
        cwe="CWE-521",
        requirements=[130, 138, 140],
    )
    F402 = FindingMetadata.new(
        code="F402",
        cwe="CWE-778",
        requirements=[75, 376, 377, 378],
    )
    F403 = FindingMetadata.new(
        code="F403",
        cwe="CWE-319",
        requirements=[130, 138, 140],
    )
    F404 = FindingMetadata.new(
        code="F404",
        cwe="CWE-78",
        requirements=[173, 265, 266],
    )
    F405 = FindingMetadata.new(
        code="F405",
        cwe="CWE-732",
        requirements=[33, 176, 265, 280],
    )
    F406 = FindingMetadata.new(
        code="F406",
        cwe="CWE-312",
        requirements=[185, 300],
    )
    F407 = FindingMetadata.new(
        code="F407",
        cwe="CWE-312",
        requirements=[185, 300],
    )
    F408 = FindingMetadata.new(
        code="F408",
        cwe="CWE-778",
        requirements=[75, 79, 376],
    )
    F410 = FindingMetadata.new(
        code="F410",
        cwe="CWE-110",
        requirements=[48, 262, 302],
        sca_exclusive=True,
    )
    F412 = FindingMetadata.new(
        code="F412",
        cwe="CWE-732",
        requirements=[186, 265],
    )
    F413 = FindingMetadata.new(
        code="F413",
        cwe="CWE-434",
        requirements=[40, 41],
    )
    F414 = FindingMetadata.new(
        code="F414",
        cwe="CWE-644",
        requirements=[266],
    )
    F416 = FindingMetadata.new(
        code="F416",
        cwe="CWE-94",
        requirements=[173],
    )
    F417 = FindingMetadata.new(
        code="F417",
        cwe="CWE-640",
        requirements=[266, 238],
        sca_exclusive=True,
    )
    F418 = FindingMetadata.new(
        code="F418",
        cwe="CWE-494",
        requirements=[266],
    )
    F421 = FindingMetadata.new(
        code="F421",
        cwe="CWE-327",
        requirements=[148, 150],
    )
    F422 = FindingMetadata.new(
        code="F422",
        cwe="CWE-1336",
        requirements=[173, 176, 265, 266],
        sca_exclusive=True,
    )
    F423 = FindingMetadata.new(
        code="F423",
        cwe="CWE-382",
        requirements=[164, 167, 72, 327],
    )
    F425 = FindingMetadata.new(
        code="F425",
        cwe="CWE-79",
        requirements=[266],
        sca_exclusive=True,
    )
    F426 = FindingMetadata.new(
        code="F426",
        cwe="CWE-749",
        requirements=[266],
    )
    F431 = FindingMetadata.new(
        code="F431",
        cwe="CWE-1357",
        requirements=[266],
    )
    F433 = FindingMetadata.new(
        code="F433",
        cwe="CWE-312",
        requirements=[185, 300],
    )
    F435 = FindingMetadata.new(
        code="F435",
        cwe="CWE-1395",
        requirements=[262],
    )
    F437 = FindingMetadata.new(
        code="F437",
        cwe="CWE-494",
        requirements=[266],
    )
    F440 = FindingMetadata.new(
        code="F440",
        cwe="CWE-276",
        requirements=[62, 175, 266, 349],
    )
    F446 = FindingMetadata.new(
        code="F446",
        cwe="CWE-1188",
        requirements=[266],
    )
    F448 = FindingMetadata.new(
        code="F448",
        cwe="CWE-1395",
        requirements=[262, 330],
    )


# This is a subset of the same Enum that exists on Integrates side
class VulnerabilityTechnique(Enum):
    CSPM = "CSPM"
    DAST = "DAST"
    SAST = "SAST"
    SCA = "SCA"


class VulnerabilityType(str, Enum):
    INPUTS = "INPUTS"
    LINES = "LINES"
    PORTS = "PORTS"


class SkimsAPKConfig(NamedTuple):
    exclude: tuple[str, ...]
    include: tuple[str, ...]
    use_sast_analysis: bool = True


class SkimsSastConfig(NamedTuple):
    exclude: tuple[str, ...]
    include: tuple[str, ...]
    recursion_limit: int | None = 100
    reachability_sbom_input: str | None = None


class SkimsScaConfig(NamedTuple):
    exclude: tuple[str, ...]
    include: tuple[str, ...]
    use_new_sca: bool = False


class SkimsDastConfig(NamedTuple):
    urls: tuple[str, ...]
    http_checks: bool
    ssl_checks: bool


class SkimsApiConfig(NamedTuple):
    include: tuple[str, ...]


class AwsCredentials(NamedTuple):
    access_key_id: str
    secret_access_key: str
    session_token: str | None


class AwsRole(NamedTuple):
    external_id: str | None
    role: str


class GcpCredentials(NamedTuple):
    private_key: str


class AzureCredentials(NamedTuple):
    client_id: str
    client_secret: str
    tenant_id: str
    subscription_id: str


class SkimsCspmConfig(NamedTuple):
    aws_credentials: list[AwsCredentials | AwsRole]
    gcp_credentials: list[GcpCredentials]
    azure_credentials: list[AzureCredentials]


class OutputFormat(Enum):
    CSV = "CSV"
    SARIF = "SARIF"
    ALL = "ALL"


class SkimsOutputConfig(NamedTuple):
    file_path: str
    format: OutputFormat


class LocalesEnum(Enum):
    EN = "EN"
    ES = "ES"


class SkimsConfig(NamedTuple):
    # lib module keys
    api: SkimsApiConfig | None
    apk: SkimsAPKConfig
    cspm: SkimsCspmConfig | None
    dast: SkimsDastConfig | None
    sast: SkimsSastConfig
    sca: SkimsScaConfig
    # set up keys
    start_dir: str
    working_dir: str
    namespace: str
    commit: str | None
    checks: set[FindingEnum]
    language: LocalesEnum
    output: SkimsOutputConfig | None
    # specific features keys
    strict: bool
    execution_id: str | None
    debug: bool
    multifile: bool
    file_size_limit: bool | None
    tracing_opt_out: bool
    use_report_soon: bool = False


class SkimsModules(NamedTuple):
    api: bool
    apk: bool
    cspm: bool
    dast: bool
    sast: bool
    sca: bool


class UrlStatus(NamedTuple):
    url: str
    status_code: int


class DastResponses(NamedTuple):
    ssl_responses: dict[str, bool]
    http_responses: list[UrlStatus]


class HTTPProperties(NamedTuple):
    has_redirect: bool
    original_url: str


class DeveloperEnum(Enum):
    ALEJANDRO_SALGADO = "asalgado@fluidattacks.com"
    ALEJANDRO_TRUJILLO = "atrujillo@fluidattacks.com"
    ANDRES_CUBEROS = "acuberos@fluidattacks.com"
    ANDRES_URIBE = "auribe@fluidattacks.com"
    BRIAM_AGUDELO = "bagudelo@fluidattacks.com"
    DEFAULT = "machine@fluidattacks.com"
    DIEGO_RESTREPO = "drestrepo@fluidattacks.com"
    DEIBYT_PAEZ = "dpaez@fluidattacks.com"
    ELVER_TOBO = "etobo@fluidattacks.com"
    FABIO_LAGOS = "flagos@fluidattacks.com"
    FLOR_CALDERON = "fcalderon@fluidattacks.com"
    JOHN_PEREZ = "jperez@fluidattacks.com"
    JHON_ROMERO = "jromero@fluidattacks.com"
    JULIAN_GOMEZ = "ugomez@fluidattacks.com"
    JULIAN_NUNEZ = "jnunez@fluidattacks.com"
    JUAN_ECHEVERRI = "jecheverri@fluidattacks.com"
    LEWIS_CONTRERAS = "lcontreras@fluidattacks.com"
    LUIS_PATINO = "lpatino@fluidattacks.com"
    LUIS_SAAVEDRA = "lsaavedra@fluidattacks.com"
    MIGUEL_CARDENAS = "acardenas@fluidattacks.com"
    ROBIN_QUINTERO = "rquintero@fluidattacks.com"
    YEISON_LISCANO = "yliscano@fluidattacks.com"


class ScaAdvisoriesMetadata(NamedTuple):
    cve: list[str]
    vulnerable_version: str
    package: str
    epss: int = 0
    pkg_id: str | None = None
    is_new_sca_report: bool = False


class SkimsVulnerabilityMetadata(NamedTuple):
    source_method: str
    developer: DeveloperEnum
    description: str
    snippet: str | Snippet
    cvss: str | None
    cvss_v4: str | None
    cwe_ids: list[str]
    http_properties: HTTPProperties | None = None
    sca_metadata: ScaAdvisoriesMetadata | None = None
    skip: bool = False
    auto_approve: bool = False


class Vulnerability(NamedTuple):
    finding: FindingEnum
    kind: VulnerabilityTechnique
    vulnerability_type: VulnerabilityType
    namespace: str
    skims_metadata: SkimsVulnerabilityMetadata
    what: str
    where: str
    stream: str | None = "skims"

    @property
    def extract_path(self) -> str:
        what = re.sub(r"(\s+\(.*\))?(\s+\[.*\])?", "", self.what)
        while what.endswith("/"):
            what = what.rstrip("/")
        return what

    @property
    def digest(self) -> int:
        """Hash a Vulnerability according to Integrates rules."""
        # if you want to add a field to the hash you must
        # also do it in the integrates function

        location = self.extract_path
        specific = self.where
        method = self.skims_metadata.source_method

        if self.kind == VulnerabilityTechnique.SCA and (
            sca_metadata := self.skims_metadata.sca_metadata
        ):
            sca_info = sca_metadata.package + sca_metadata.vulnerable_version
        else:
            sca_info = None

        return int.from_bytes(
            hashlib.sha256(
                bytes(
                    (
                        location
                        + specific
                        + self.finding.value.title.split(".")[2]
                        + method
                        + (sca_info or "")
                    ),
                    "utf-8",
                ),
            ).digest()[:8],
            "little",
        )

    def set_exclusion_value(self, *, is_exclusion: bool) -> "Vulnerability":
        updated_skims_metadata = self.skims_metadata._replace(skip=is_exclusion)
        return self._replace(skims_metadata=updated_skims_metadata)


Vulnerabilities = tuple[Vulnerability, ...]


def get_advisory_info(vulnerability: Vulnerability) -> dict | None:
    sca_metadata = vulnerability.skims_metadata.sca_metadata
    if not sca_metadata:
        return None

    return {
        "package": sca_metadata.package,
        "vulnerable_version": sca_metadata.vulnerable_version,
        "cve": sca_metadata.cve,
        "epss": str(sca_metadata.epss),
    }


class MethodExecutionResult(NamedTuple):
    vulnerabilities: tuple[Vulnerability, ...]
    method_name: str | None = None
    time_coefficient: float | None = None


class MethodOriginEnum(Enum):
    ARM = "ARM"
    ASSERTS = "ASSERTS"
    BENCHMARK = "BENCHMARK"
    DEVELOPER = "DEVELOPER"
    EXTERNAL = "EXTERNAL"
    HACKER = "HACKER"
    PRIORITIZE = "PRIORITIZE"


class MethodInfo(NamedTuple):
    file_name: str
    name: str
    module: str
    finding: FindingEnum
    developer: DeveloperEnum
    technique: VulnerabilityTechnique
    origin: MethodOriginEnum
    cvss: str | None = None
    cvss_v4: str | None = None
    cwe_ids: list[str] | None = None
    auto_approve: bool = False

    def get_name(self) -> str:
        return f"{self.file_name}.{self.name}"

    def get_cwe(self) -> list[str]:
        return self.cwe_ids if self.cwe_ids else [self.finding.value.cwe]

    def get_finding(self) -> str:
        return self.finding.name.replace("F", "")

    def __hash__(self) -> int:
        # Identical MethodInfo instances are treated as different objects,
        # preventing issues related to method name uniqueness in MethodEnums
        return hash((self.file_name, self.name, id(self)))
