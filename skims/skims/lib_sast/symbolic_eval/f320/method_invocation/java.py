from lib_sast.symbolic_eval.types import (
    SymbolicEvalArgs,
    SymbolicEvaluation,
)
from lib_sast.utils.graph import (
    adj_ast,
)


def java_anonymous_ldap_bind(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    if args.graph.nodes[args.n_id].get("expression", "") == "put" and (
        arguments_id := args.graph.nodes[args.n_id].get("arguments_id")
    ):
        has_security_authentication = False
        for argument in adj_ast(args.graph, arguments_id):
            if args.graph.nodes[argument]["label_type"] == "Comment":
                continue

            if has_security_authentication:
                if args.graph.nodes[argument].get("value", "") == "none":
                    args.triggers.add("java_anonymous_ldap_bind")

            elif "SECURITY_AUTHENTICATION" in args.graph.nodes[argument].get("symbol", ""):
                has_security_authentication = True

    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)
