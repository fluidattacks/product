from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.symbolic_eval.f320.member_access.c_sharp import (
    cs_insec_auth,
)
from lib_sast.symbolic_eval.f320.member_access.python import (
    python_unsafe_ldap,
)
from lib_sast.symbolic_eval.types import (
    Evaluator,
    SymbolicEvalArgs,
    SymbolicEvaluation,
)

METHOD_EVALUATORS: dict[MethodsEnum, Evaluator] = {
    MethodsEnum.CSHARP_LDAP_CONNECTIONS_AUTHENTICATED: cs_insec_auth,
    MethodsEnum.PYTHON_UNSAFE_LDAP_CONNECTIONS: python_unsafe_ldap,
}


def evaluate(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    if language_evaluator := METHOD_EVALUATORS.get(args.method):
        return language_evaluator(args)
    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)
