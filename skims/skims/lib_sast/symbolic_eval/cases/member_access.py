from lib_sast.symbolic_eval.context.member import (
    solve as solve_member_access,
)
from lib_sast.symbolic_eval.f052.member_access import (
    evaluate as evaluate_member_access_f052,
)
from lib_sast.symbolic_eval.f060.member_access import (
    evaluate as evaluate_member_access_f060,
)
from lib_sast.symbolic_eval.f063.member_access import (
    evaluate as evaluate_member_access_f063,
)
from lib_sast.symbolic_eval.f091.member_access import (
    evaluate as evaluate_member_access_f091,
)
from lib_sast.symbolic_eval.f098.member_access import (
    evaluate as evaluate_member_access_f098,
)
from lib_sast.symbolic_eval.f106.member_access import (
    evaluate as evaluate_member_access_f106,
)
from lib_sast.symbolic_eval.f107.member_access import (
    evaluate as evaluate_member_access_f107,
)
from lib_sast.symbolic_eval.f112.member_access import (
    evaluate as evaluate_member_access_f112,
)
from lib_sast.symbolic_eval.f143.member_access import (
    evaluate as evaluate_member_access_f143,
)
from lib_sast.symbolic_eval.f211.member_access import (
    evaluate as evaluate_member_access_f211,
)
from lib_sast.symbolic_eval.f239.member_access import (
    evaluate as evaluate_member_access_f239,
)
from lib_sast.symbolic_eval.f280.member_access import (
    evaluate as evaluate_member_access_f280,
)
from lib_sast.symbolic_eval.f297.member_access import (
    evaluate as evaluate_member_access_f297,
)
from lib_sast.symbolic_eval.f313.member_access import (
    evaluate as evaluate_member_access_f313,
)
from lib_sast.symbolic_eval.f320.member_access import (
    evaluate as evaluate_member_access_f320,
)
from lib_sast.symbolic_eval.f371.member_access import (
    evaluate as evaluate_member_access_f371,
)
from lib_sast.symbolic_eval.f413.member_access import (
    evaluate as evaluate_member_access_f413,
)
from lib_sast.symbolic_eval.f416.member_access import (
    evaluate as evaluate_member_access_f416,
)
from lib_sast.symbolic_eval.types import (
    Evaluator,
    SymbolicEvalArgs,
    SymbolicEvaluation,
)
from model.core import (
    FindingEnum,
)

FINDING_EVALUATORS: dict[FindingEnum, Evaluator] = {
    FindingEnum.F052: evaluate_member_access_f052,
    FindingEnum.F060: evaluate_member_access_f060,
    FindingEnum.F063: evaluate_member_access_f063,
    FindingEnum.F091: evaluate_member_access_f091,
    FindingEnum.F098: evaluate_member_access_f098,
    FindingEnum.F106: evaluate_member_access_f106,
    FindingEnum.F107: evaluate_member_access_f107,
    FindingEnum.F112: evaluate_member_access_f112,
    FindingEnum.F143: evaluate_member_access_f143,
    FindingEnum.F211: evaluate_member_access_f211,
    FindingEnum.F239: evaluate_member_access_f239,
    FindingEnum.F280: evaluate_member_access_f280,
    FindingEnum.F297: evaluate_member_access_f297,
    FindingEnum.F313: evaluate_member_access_f313,
    FindingEnum.F320: evaluate_member_access_f320,
    FindingEnum.F371: evaluate_member_access_f371,
    FindingEnum.F413: evaluate_member_access_f413,
    FindingEnum.F416: evaluate_member_access_f416,
}


def evaluate(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    expr_id = solve_member_access(args) or args.graph.nodes[args.n_id]["expression_id"]
    args.evaluation[args.n_id] = args.generic(args.fork_n_id(expr_id)).danger

    if args.method_evaluators and (method_evaluator := args.method_evaluators.get("member_access")):
        args.evaluation[args.n_id] = method_evaluator(args).danger
    elif finding_evaluator := FINDING_EVALUATORS.get(args.method.value.finding):
        args.evaluation[args.n_id] = finding_evaluator(args).danger

    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)
