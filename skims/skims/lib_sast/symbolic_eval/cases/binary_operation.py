from lib_sast.symbolic_eval.f106.binary_operation import (
    evaluate as evaluate_binary_operation_f106,
)
from lib_sast.symbolic_eval.f188.binary_operation import (
    evaluate as evaluate_binary_operation_f188,
)
from lib_sast.symbolic_eval.types import (
    Evaluator,
    SymbolicEvalArgs,
    SymbolicEvaluation,
)
from model.core import (
    FindingEnum,
)

FINDING_EVALUATORS: dict[FindingEnum, Evaluator] = {
    FindingEnum.F106: evaluate_binary_operation_f106,
    FindingEnum.F188: evaluate_binary_operation_f188,
}


def evaluate(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    op_attr = args.graph.nodes[args.n_id]
    d_l_expr = args.generic(args.fork_n_id(op_attr["left_id"])).danger
    d_r_expr = args.generic(args.fork_n_id(op_attr["right_id"])).danger

    args.evaluation[args.n_id] = d_l_expr or d_r_expr

    if args.method_evaluators and (
        method_evaluator := args.method_evaluators.get("binary_operation")
    ):
        args.evaluation[args.n_id] = method_evaluator(args).danger
    elif finding_evaluator := FINDING_EVALUATORS.get(args.method.value.finding):
        args.evaluation[args.n_id] = finding_evaluator(args).danger

    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)
