from lib_sast.symbolic_eval.types import (
    SymbolicEvalArgs,
    SymbolicEvaluation,
)
from lib_sast.utils.graph import (
    adj_ast,
)


def evaluate(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    args.evaluation[args.n_id] = False
    c_id = adj_ast(args.graph, args.n_id)[0]
    args.evaluation[args.n_id] = args.generic(args.fork_n_id(c_id)).danger

    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)
