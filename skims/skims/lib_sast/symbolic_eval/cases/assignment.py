from lib_sast.symbolic_eval.types import (
    SymbolicEvalArgs,
    SymbolicEvaluation,
)


def evaluate(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    # Danger should NOT propagate to the variable_id node because of FP risks
    val_id = args.graph.nodes[args.n_id]["value_id"]
    args.evaluation[args.n_id] = args.generic(args.fork_n_id(val_id)).danger

    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)
