from lib_sast.symbolic_eval.f063.element_access import (
    evaluate as evaluate_element_access_f063,
)
from lib_sast.symbolic_eval.f083.element_access import (
    evaluate as evaluate_element_access_f083,
)
from lib_sast.symbolic_eval.f134.element_access import (
    evaluate as evaluate_element_access_f134,
)
from lib_sast.symbolic_eval.f143.element_access import (
    evaluate as evaluate_element_access_f143,
)
from lib_sast.symbolic_eval.types import (
    Evaluator,
    SymbolicEvalArgs,
    SymbolicEvaluation,
)
from model.core import (
    FindingEnum,
)

FINDING_EVALUATORS: dict[FindingEnum, Evaluator] = {
    FindingEnum.F063: evaluate_element_access_f063,
    FindingEnum.F083: evaluate_element_access_f083,
    FindingEnum.F134: evaluate_element_access_f134,
    FindingEnum.F143: evaluate_element_access_f143,
}


def evaluate(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    n_attr = args.graph.nodes[args.n_id]
    args.evaluation[args.n_id] = args.generic(args.fork_n_id(n_attr["expression_id"])).danger

    if args.method_evaluators and (
        method_evaluator := args.method_evaluators.get("element_access")
    ):
        args.evaluation[args.n_id] = method_evaluator(args).danger
    elif finding_evaluator := FINDING_EVALUATORS.get(args.method.value.finding):
        args.evaluation[args.n_id] = finding_evaluator(args).danger

    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)
