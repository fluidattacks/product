from lib_sast.sast_model import (
    SUPPORTED_MULTIFILE,
    NId,
    decide_language,
)
from lib_sast.symbolic_eval.context.method import (
    solve_invocation,
)
from lib_sast.symbolic_eval.f052.method_invocation import (
    evaluate as evaluate_method_f052,
)
from lib_sast.symbolic_eval.f063.method_invocation import (
    evaluate as evaluate_method_f063,
)
from lib_sast.symbolic_eval.f083.method_invocation import (
    evaluate as evaluate_method_f083,
)
from lib_sast.symbolic_eval.f089.method_invocation import (
    evaluate as evaluate_method_f089,
)
from lib_sast.symbolic_eval.f091.method_invocation import (
    evaluate as evaluate_method_f091,
)
from lib_sast.symbolic_eval.f107.method_invocation import (
    evaluate as evaluate_method_f107,
)
from lib_sast.symbolic_eval.f112.method_invocation import (
    evaluate as evaluate_method_f112,
)
from lib_sast.symbolic_eval.f153.method_invocation import (
    evaluate as evaluate_method_f153,
)
from lib_sast.symbolic_eval.f188.method_invocation import (
    evaluate as evaluate_method_f188,
)
from lib_sast.symbolic_eval.f211.method_invocation import (
    evaluate as evaluate_method_f211,
)
from lib_sast.symbolic_eval.f280.method_invocation import (
    evaluate as evaluate_method_f280,
)
from lib_sast.symbolic_eval.f309.method_invocation import (
    evaluate as evaluate_method_f309,
)
from lib_sast.symbolic_eval.f320.method_invocation import (
    evaluate as evaluate_method_f320,
)
from lib_sast.symbolic_eval.f338.method_invocation import (
    evaluate as evaluate_method_f338,
)
from lib_sast.symbolic_eval.f344.method_invocation import (
    evaluate as evaluate_method_f344,
)
from lib_sast.symbolic_eval.f359.method_invocation import (
    evaluate as evaluate_method_f359,
)
from lib_sast.symbolic_eval.f368.method_invocation import (
    evaluate as evaluate_method_f368,
)
from lib_sast.symbolic_eval.f371.method_invocation import (
    evaluate as evaluate_method_f371,
)
from lib_sast.symbolic_eval.f395.method_invocation import (
    evaluate as evaluate_method_f395,
)
from lib_sast.symbolic_eval.multifile.method_invocation import (
    evaluate_method_invocation,
)
from lib_sast.symbolic_eval.types import (
    Evaluator,
    SymbolicEvalArgs,
    SymbolicEvaluation,
)
from lib_sast.utils.graph import (
    adj_ast,
)
from model.core import (
    FindingEnum,
)

FINDING_EVALUATORS: dict[FindingEnum, Evaluator] = {
    FindingEnum.F052: evaluate_method_f052,
    FindingEnum.F063: evaluate_method_f063,
    FindingEnum.F083: evaluate_method_f083,
    FindingEnum.F089: evaluate_method_f089,
    FindingEnum.F091: evaluate_method_f091,
    FindingEnum.F107: evaluate_method_f107,
    FindingEnum.F112: evaluate_method_f112,
    FindingEnum.F153: evaluate_method_f153,
    FindingEnum.F188: evaluate_method_f188,
    FindingEnum.F211: evaluate_method_f211,
    FindingEnum.F280: evaluate_method_f280,
    FindingEnum.F309: evaluate_method_f309,
    FindingEnum.F320: evaluate_method_f320,
    FindingEnum.F338: evaluate_method_f338,
    FindingEnum.F344: evaluate_method_f344,
    FindingEnum.F359: evaluate_method_f359,
    FindingEnum.F395: evaluate_method_f395,
    FindingEnum.F368: evaluate_method_f368,
    FindingEnum.F371: evaluate_method_f371,
}

MODIFYING_METHODS: set[str] = {"add", "push", "put", "get"}


def evaluate_method_expression(
    args: SymbolicEvalArgs,
    expr_id: NId,
) -> bool:
    if args.graph.nodes[expr_id].get("symbol") in MODIFYING_METHODS:
        return False

    if md_id := solve_invocation(args.graph, args.path, expr_id):
        # Avoid infinite propagation in recursive method invocations
        if expr_id in adj_ast(args.graph, md_id, depth=-1):
            return False
        fork_id = args.graph.nodes[md_id].get("block_id") or md_id
        d_expression = args.generic(args.fork_n_id(fork_id)).danger
    else:
        d_expression = args.generic(args.fork_n_id(expr_id)).danger

    return d_expression


def evaluate(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    args.evaluation[args.n_id] = False
    n_attrs = args.graph.nodes[args.n_id]

    d_method = False
    propagate_danger = True
    if (
        (metadata_node := args.graph.nodes.get("0"))
        and (lang := decide_language(path=metadata_node["path"]))
        and lang in SUPPORTED_MULTIFILE
    ):
        d_method, propagate_danger = evaluate_method_invocation(args, lang)

    propagation_result = False
    if propagate_danger:
        d_expression = False
        if expr_id := n_attrs.get("expression_id"):
            d_expression = evaluate_method_expression(args, expr_id)

        d_arguments = False
        if al_id := args.graph.nodes[args.n_id].get("arguments_id"):
            d_arguments = args.generic(args.fork_n_id(al_id)).danger

        d_object = False
        if obj_id := args.graph.nodes[args.n_id].get("object_id"):
            d_object = args.generic(args.fork_n_id(obj_id)).danger

        propagation_result = d_expression or d_arguments or d_object

    args.evaluation[args.n_id] = propagation_result or d_method
    if args.method_evaluators and (
        method_evaluator := args.method_evaluators.get("method_invocation")
    ):
        args.evaluation[args.n_id] = method_evaluator(args).danger
    elif finding_evaluator := FINDING_EVALUATORS.get(args.method.value.finding):
        args.evaluation[args.n_id] = finding_evaluator(args).danger
    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)
