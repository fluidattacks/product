from lib_sast.symbolic_eval.types import (
    SymbolicEvalArgs,
    SymbolicEvaluation,
)


def evaluate(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    n_attr = args.graph.nodes[args.n_id]
    alt_danger = args.generic(args.fork_n_id(n_attr["true_id"])).danger
    cons_danger = args.generic(args.fork_n_id(n_attr["false_id"])).danger

    args.evaluation[args.n_id] = alt_danger or cons_danger

    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)
