from lib_sast.symbolic_eval.f042.pair import (
    evaluate as evaluate_pair_f042,
)
from lib_sast.symbolic_eval.f052.pair import (
    evaluate as evaluate_pair_f052,
)
from lib_sast.symbolic_eval.f083.pair import (
    evaluate as evaluate_pair_f083,
)
from lib_sast.symbolic_eval.f153.pair import (
    evaluate as evaluate_pair_f153,
)
from lib_sast.symbolic_eval.f309.pair import (
    evaluate as evaluate_pair_f309,
)
from lib_sast.symbolic_eval.f371.pair import (
    evaluate as evaluate_pair_f371,
)
from lib_sast.symbolic_eval.types import (
    Evaluator,
    SymbolicEvalArgs,
    SymbolicEvaluation,
)
from model.core import (
    FindingEnum,
)

FINDING_EVALUATORS: dict[FindingEnum, Evaluator] = {
    FindingEnum.F042: evaluate_pair_f042,
    FindingEnum.F052: evaluate_pair_f052,
    FindingEnum.F083: evaluate_pair_f083,
    FindingEnum.F153: evaluate_pair_f153,
    FindingEnum.F309: evaluate_pair_f309,
    FindingEnum.F371: evaluate_pair_f371,
}


def evaluate(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    n_attr = args.graph.nodes[args.n_id]
    key_danger = args.generic(args.fork_n_id(n_attr["key_id"])).danger
    val_danger = args.generic(args.fork_n_id(n_attr["value_id"])).danger

    args.evaluation[args.n_id] = key_danger or val_danger

    if args.method_evaluators and (method_evaluator := args.method_evaluators.get("pair")):
        args.evaluation[args.n_id] = method_evaluator(args).danger
    elif finding_evaluator := FINDING_EVALUATORS.get(args.method.value.finding):
        args.evaluation[args.n_id] = finding_evaluator(args).danger

    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)
