from lib_sast.symbolic_eval.types import (
    SymbolicEvalArgs,
    SymbolicEvaluation,
)


def evaluate(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    iterable_id = args.graph.nodes[args.n_id]["iterable_item_id"]
    args.evaluation[args.n_id] = args.generic(args.fork_n_id(iterable_id)).danger

    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)
