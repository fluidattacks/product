from lib_sast.symbolic_eval.types import (
    SymbolicEvalArgs,
    SymbolicEvaluation,
)
from lib_sast.utils.graph import (
    adj_ast,
)


def evaluate(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    args.evaluation[args.n_id] = False
    if args.n_id in args.path:
        deps_ids = tuple(args.path[: args.path.index(args.n_id)])
    else:
        deps_ids = adj_ast(args.graph, args.n_id)

    danger = [args.generic(args.fork_n_id(_id)).danger for _id in deps_ids]
    args.evaluation[args.n_id] = any(danger)

    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)
