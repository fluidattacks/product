from lib_sast.symbolic_eval.types import (
    SymbolicEvalArgs,
    SymbolicEvaluation,
)
from lib_sast.utils.graph import (
    adj_ast,
)


def evaluate(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    arg_ids = adj_ast(args.graph, args.n_id)
    danger = [args.generic(args.fork_n_id(arg_id)).danger for arg_id in arg_ids]
    args.evaluation[args.n_id] = any(danger)

    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)
