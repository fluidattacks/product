from lib_sast.symbolic_eval.f153.import_node import (
    evaluate as evaluate_import_f153,
)
from lib_sast.symbolic_eval.f371.import_node import (
    evaluate as evaluate_import_f371,
)
from lib_sast.symbolic_eval.types import (
    Evaluator,
    SymbolicEvalArgs,
    SymbolicEvaluation,
)
from lib_sast.utils.graph import (
    adj_ast,
)
from model.core import (
    FindingEnum,
)

FINDING_EVALUATORS: dict[FindingEnum, Evaluator] = {
    FindingEnum.F153: evaluate_import_f153,
    FindingEnum.F371: evaluate_import_f371,
}


def evaluate(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    args.evaluation[args.n_id] = False
    s_ids = adj_ast(args.graph, args.n_id)
    if len(s_ids) > 0:
        danger = [args.generic(args.fork_n_id(_id)).danger for _id in s_ids]
        args.evaluation[args.n_id] = any(danger)

    if args.method_evaluators and (
        method_evaluator := args.method_evaluators.get("import_statement")
    ):
        args.evaluation[args.n_id] = method_evaluator(args).danger
    elif finding_evaluator := FINDING_EVALUATORS.get(args.method.value.finding):
        args.evaluation[args.n_id] = finding_evaluator(args).danger

    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)
