from lib_sast.symbolic_eval.f063.object_creation import (
    evaluate as evaluate_parameter_f063,
)
from lib_sast.symbolic_eval.f107.object_creation import (
    evaluate as evaluate_parameter_f107,
)
from lib_sast.symbolic_eval.f128.object_creation import (
    evaluate as evaluate_parameter_f128,
)
from lib_sast.symbolic_eval.f130.object_creation import (
    evaluate as evaluate_parameter_f130,
)
from lib_sast.symbolic_eval.f134.object_creation import (
    evaluate as evaluate_parameter_f134,
)
from lib_sast.symbolic_eval.f136.object_creation import (
    evaluate as evaluate_parameter_f136,
)
from lib_sast.symbolic_eval.f153.object_creation import (
    evaluate as evaluate_parameter_f153,
)
from lib_sast.symbolic_eval.f169.object_creation import (
    evaluate as evaluate_parameter_f169,
)
from lib_sast.symbolic_eval.f320.object_creation import (
    evaluate as evaluate_parameter_f320,
)
from lib_sast.symbolic_eval.f359.object_creation import (
    evaluate as evaluate_parameter_f359,
)
from lib_sast.symbolic_eval.f368.object_creation import (
    evaluate as evaluate_parameter_f368,
)
from lib_sast.symbolic_eval.f414.object_creation import (
    evaluate as evaluate_parameter_f414,
)
from lib_sast.symbolic_eval.types import (
    Evaluator,
    SymbolicEvalArgs,
    SymbolicEvaluation,
)
from model.core import (
    FindingEnum,
)

FINDING_EVALUATORS: dict[FindingEnum, Evaluator] = {
    FindingEnum.F063: evaluate_parameter_f063,
    FindingEnum.F107: evaluate_parameter_f107,
    FindingEnum.F128: evaluate_parameter_f128,
    FindingEnum.F130: evaluate_parameter_f130,
    FindingEnum.F134: evaluate_parameter_f134,
    FindingEnum.F136: evaluate_parameter_f136,
    FindingEnum.F153: evaluate_parameter_f153,
    FindingEnum.F169: evaluate_parameter_f169,
    FindingEnum.F320: evaluate_parameter_f320,
    FindingEnum.F359: evaluate_parameter_f359,
    FindingEnum.F368: evaluate_parameter_f368,
    FindingEnum.F414: evaluate_parameter_f414,
}


def evaluate(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    args.evaluation[args.n_id] = False
    if al_id := args.graph.nodes[args.n_id].get("arguments_id"):
        args.evaluation[args.n_id] = args.generic(args.fork_n_id(al_id)).danger

    if args.method_evaluators and (
        method_evaluator := args.method_evaluators.get("object_creation")
    ):
        args.evaluation[args.n_id] = method_evaluator(args).danger
    elif finding_evaluator := FINDING_EVALUATORS.get(args.method.value.finding):
        args.evaluation[args.n_id] = finding_evaluator(args).danger

    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)
