from lib_sast.symbolic_eval.f052.object import (
    evaluate as evaluate_object_f052,
)
from lib_sast.symbolic_eval.f153.object import (
    evaluate as evaluate_object_f153,
)
from lib_sast.symbolic_eval.f359.object import (
    evaluate as evaluate_object_f359,
)
from lib_sast.symbolic_eval.types import (
    Evaluator,
    SymbolicEvalArgs,
    SymbolicEvaluation,
)
from lib_sast.utils.graph import (
    adj_ast,
)
from model.core import (
    FindingEnum,
)

FINDING_EVALUATORS: dict[FindingEnum, Evaluator] = {
    FindingEnum.F052: evaluate_object_f052,
    FindingEnum.F153: evaluate_object_f153,
    FindingEnum.F359: evaluate_object_f359,
}


def evaluate(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    param_ids = adj_ast(args.graph, args.n_id)
    danger = [args.generic(args.fork_n_id(p_id)).danger for p_id in param_ids]
    args.evaluation[args.n_id] = any(danger)

    if args.method_evaluators and (method_evaluator := args.method_evaluators.get("object_node")):
        args.evaluation[args.n_id] = method_evaluator(args).danger
    elif finding_evaluator := FINDING_EVALUATORS.get(args.method.value.finding):
        args.evaluation[args.n_id] = finding_evaluator(args).danger

    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)
