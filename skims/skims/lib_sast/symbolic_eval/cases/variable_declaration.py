from lib_sast.symbolic_eval.f052.variable_declaration import (
    evaluate as evaluate_variable_declaration_f052,
)
from lib_sast.symbolic_eval.f128.variable_declaration import (
    evaluate as evaluate_variable_declaration_f128,
)
from lib_sast.symbolic_eval.f130.variable_declaration import (
    evaluate as evaluate_variable_declaration_f130,
)
from lib_sast.symbolic_eval.f134.variable_declaration import (
    evaluate as evaluate_variable_declaration_f134,
)
from lib_sast.symbolic_eval.f153.variable_declaration import (
    evaluate as evaluate_variable_declaration_f153,
)
from lib_sast.symbolic_eval.f297.variable_declaration import (
    evaluate as evaluate_variable_declaration_f297,
)
from lib_sast.symbolic_eval.f353.variable_declaration import (
    evaluate as evaluate_variable_declaration_f353,
)
from lib_sast.symbolic_eval.f395.variable_declaration import (
    evaluate as evaluate_variable_declaration_f395,
)
from lib_sast.symbolic_eval.types import (
    Evaluator,
    SymbolicEvalArgs,
    SymbolicEvaluation,
)
from model.core import (
    FindingEnum,
)

FINDING_EVALUATORS: dict[FindingEnum, Evaluator] = {
    FindingEnum.F052: evaluate_variable_declaration_f052,
    FindingEnum.F153: evaluate_variable_declaration_f153,
    FindingEnum.F128: evaluate_variable_declaration_f128,
    FindingEnum.F130: evaluate_variable_declaration_f130,
    FindingEnum.F134: evaluate_variable_declaration_f134,
    FindingEnum.F297: evaluate_variable_declaration_f297,
    FindingEnum.F353: evaluate_variable_declaration_f353,
    FindingEnum.F395: evaluate_variable_declaration_f395,
}


def evaluate(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    args.evaluation[args.n_id] = False
    if val_id := args.graph.nodes[args.n_id].get("value_id"):
        args.evaluation[args.n_id] = args.generic(args.fork_n_id(val_id)).danger

    if args.method_evaluators and (
        method_evaluator := args.method_evaluators.get("variable_declaration")
    ):
        args.evaluation[args.n_id] = method_evaluator(args).danger
    elif finding_evaluator := FINDING_EVALUATORS.get(args.method.value.finding):
        args.evaluation[args.n_id] = finding_evaluator(args).danger

    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)
