from lib_sast.symbolic_eval.types import (
    SymbolicEvalArgs,
    SymbolicEvaluation,
)


def php_insecure_elliptic_curve(
    args: SymbolicEvalArgs,
) -> SymbolicEvaluation:
    nodes = args.graph.nodes
    if (value := nodes[args.n_id].get("value")) and value == "md5":
        args.triggers.add("hash")

    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)
