from lib_sast.symbolic_eval.common import (
    NET_CONNECTION_OBJECTS,
)
from lib_sast.symbolic_eval.types import (
    SymbolicEvalArgs,
    SymbolicEvaluation,
)


def cs_ldap_injection(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    if args.graph.nodes[args.n_id]["name"] in NET_CONNECTION_OBJECTS:
        args.triggers.add("user_connection")
    elif args.graph.nodes[args.n_id]["name"] == "DirectorySearcher":
        args.triggers.add("dir_searcher")
        args.evaluation[args.n_id] = True

    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)
