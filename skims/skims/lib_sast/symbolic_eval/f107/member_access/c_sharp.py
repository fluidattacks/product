from lib_sast.symbolic_eval.common import (
    NET_SYSTEM_HTTP_METHODS,
)
from lib_sast.symbolic_eval.types import (
    SymbolicEvalArgs,
    SymbolicEvaluation,
)


def cs_ldap_injection(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    ma_attr = args.graph.nodes[args.n_id]
    if ma_attr["member"] in NET_SYSTEM_HTTP_METHODS:
        args.triggers.add("user_parameters")

    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)
