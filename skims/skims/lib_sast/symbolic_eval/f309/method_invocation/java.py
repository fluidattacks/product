from lib_sast.symbolic_eval.types import (
    SymbolicEvalArgs,
    SymbolicEvaluation,
)


def java_none_alg(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    nodes = args.graph.nodes

    symbol: str | None = None

    expression = nodes[args.n_id].get("expression", "")

    if object_n_id := nodes[args.n_id].get("object_id", ""):
        symbol = nodes[object_n_id].get("symbol", "")

    if expression == "create" and symbol == "JWT":
        args.triggers.add("JWT")

    if expression == "none" and symbol == "Algorithm":
        args.evaluation[args.n_id] = True

    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)
