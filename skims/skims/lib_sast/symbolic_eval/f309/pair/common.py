from lib_sast.symbolic_eval.types import (
    SymbolicEvalArgs,
    SymbolicEvaluation,
)


def insecure_jwt_token(
    args: SymbolicEvalArgs,
) -> SymbolicEvaluation:
    args.evaluation[args.n_id] = False
    n_attrs = args.graph.nodes[args.n_id]
    key = n_attrs["key_id"]
    value = n_attrs["value_id"]
    if (
        (key_label := args.graph.nodes[key].get("symbol"))
        and key_label.lower() in {"algorithm", "algorithms"}
        and args.generic(args.fork(n_id=value)).danger
    ):
        args.evaluation[args.n_id] = True
        args.triggers.add("unsafealgorithm")

    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)
