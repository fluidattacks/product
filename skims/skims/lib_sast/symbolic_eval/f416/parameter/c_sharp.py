from lib_sast.symbolic_eval.types import (
    SymbolicEvalArgs,
    SymbolicEvaluation,
)


def cs_xaml_injection(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    if args.graph.nodes[args.n_id].get("variable_type") == "HttpRequest":
        args.triggers.add("user_connection")
    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)
