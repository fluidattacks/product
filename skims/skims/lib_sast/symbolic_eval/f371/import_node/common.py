from lib_sast.symbolic_eval.types import (
    SymbolicEvalArgs,
    SymbolicEvaluation,
)


def is_use_translation(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    nodes = args.graph.nodes
    if "react-i18next" in nodes[args.n_id].get("expression", ""):
        args.triggers.add("Safe")

    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)
