from lib_sast.symbolic_eval.common import (
    check_js_ts_http_inputs,
)
from lib_sast.symbolic_eval.types import (
    SymbolicEvalArgs,
    SymbolicEvaluation,
)

KNOWN_AS_NON_VULN = {"image"}


def insecure_inner_html(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    if check_js_ts_http_inputs(args):
        args.evaluation[args.n_id] = True

    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)


def comes_from_local_storage(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    n_exp = args.graph.nodes[args.n_id].get("expression")

    if n_exp and n_exp.split(".")[-1] == "sanitize":
        args.triggers.add("sanitized")
    if n_exp == "localStorage.getItem":
        args.evaluation[args.n_id] = True

    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)


def is_sanitized(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    if "sanitize" in args.graph.nodes[args.n_id].get("expression", ""):
        args.triggers.add("sanitized")
    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)
