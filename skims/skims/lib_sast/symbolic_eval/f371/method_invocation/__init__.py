from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.symbolic_eval.f371.method_invocation.common import (
    comes_from_local_storage,
    is_sanitized,
)
from lib_sast.symbolic_eval.types import (
    Evaluator,
    SymbolicEvalArgs,
    SymbolicEvaluation,
)

METHOD_EVALUATORS: dict[MethodsEnum, Evaluator] = {
    MethodsEnum.JS_USE_OF_BYPASS_SECURITY_TRUST_URL: comes_from_local_storage,
    MethodsEnum.TS_USE_OF_BYPASS_SECURITY_TRUST_URL: comes_from_local_storage,
    MethodsEnum.JS_USES_DANGEROUSLY_SET_HTML: is_sanitized,
    MethodsEnum.TS_USES_DANGEROUSLY_SET_HTML: is_sanitized,
}


def evaluate(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    if language_evaluator := METHOD_EVALUATORS.get(args.method):
        return language_evaluator(args)
    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)
