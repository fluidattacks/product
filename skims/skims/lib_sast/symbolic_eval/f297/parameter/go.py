from lib_sast.symbolic_eval.types import (
    SymbolicEvalArgs,
    SymbolicEvaluation,
)


def go_insecure_sql(
    args: SymbolicEvalArgs,
) -> SymbolicEvaluation:
    if args.graph.nodes[args.n_id].get("variable_type") == "*http.Request":
        args.evaluation[args.n_id] = True
        args.triggers.add("user_connection")

    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)
