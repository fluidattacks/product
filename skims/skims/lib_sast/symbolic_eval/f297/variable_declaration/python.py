from lib_sast.symbolic_eval.common import (
    PYTHON_INPUTS,
)
from lib_sast.symbolic_eval.types import (
    SymbolicEvalArgs,
    SymbolicEvaluation,
)


def python_django_sql_injection(
    args: SymbolicEvalArgs,
) -> SymbolicEvaluation:
    nodes = args.graph.nodes
    if (
        (val_id := nodes[args.n_id]["value_id"])
        and nodes[val_id]["label_type"] == "MethodInvocation"
        and (exp := nodes[val_id]["expression"])
    ):
        if exp in PYTHON_INPUTS:
            args.evaluation[args.n_id] = True

        else:
            args.triggers.add("PossiblySanitized")

    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)
