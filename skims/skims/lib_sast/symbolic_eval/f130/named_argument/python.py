from lib_sast.symbolic_eval.types import (
    SymbolicEvalArgs,
    SymbolicEvaluation,
)

COOKIE_SET = {"secure"}


def python_insecure_cookie(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    if args.graph.nodes[args.n_id]["argument_name"] in COOKIE_SET:
        value_id = args.graph.nodes[args.n_id]["value_id"]
        value = args.graph.nodes[value_id].get("value")
        if value and value == "False":
            args.evaluation[args.n_id] = True
    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)
