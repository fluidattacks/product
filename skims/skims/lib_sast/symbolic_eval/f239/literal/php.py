from lib_sast.symbolic_eval.types import (
    SymbolicEvalArgs,
    SymbolicEvaluation,
)


def php_info_leak_errors(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    danger_vals = {"on", "1"}

    if (value := args.graph.nodes[args.n_id].get("value")) and value.lower() == "display_errors":
        args.triggers.add("dang_key")

    if value.lower() in danger_vals:
        args.triggers.add("dang_val")

    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)
