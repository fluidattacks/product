from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.symbolic_eval.f211.member_access.c_sharp import (
    cs_regex_injection,
    cs_vuln_regex,
)
from lib_sast.symbolic_eval.f211.member_access.common import (
    common_regex_injection,
)
from lib_sast.symbolic_eval.f211.member_access.python import (
    python_regex_dos,
    python_regex_injection,
)
from lib_sast.symbolic_eval.types import (
    Evaluator,
    SymbolicEvalArgs,
    SymbolicEvaluation,
)

METHOD_EVALUATORS: dict[MethodsEnum, Evaluator] = {
    MethodsEnum.CSHARP_REGEX_INJECTION: cs_regex_injection,
    MethodsEnum.CSHARP_VULN_REGULAR_EXPRESSION: cs_vuln_regex,
    MethodsEnum.JS_REGEX_INJECTION: common_regex_injection,
    MethodsEnum.PYTHON_REGEX_DOS: python_regex_dos,
    MethodsEnum.PYTHON_REGEX_INJECTION: python_regex_injection,
    MethodsEnum.TS_REGEX_INJECTION: common_regex_injection,
}


def evaluate(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    if language_evaluator := METHOD_EVALUATORS.get(args.method):
        return language_evaluator(args)
    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)
