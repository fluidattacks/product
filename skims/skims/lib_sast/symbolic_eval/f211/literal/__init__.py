from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.symbolic_eval.f211.literal.c_sharp import (
    cs_vuln_regex,
)
from lib_sast.symbolic_eval.f211.literal.java import (
    java_vuln_regex,
)
from lib_sast.symbolic_eval.f211.literal.kotlin import (
    kt_vuln_regex,
)
from lib_sast.symbolic_eval.f211.literal.python import (
    python_regex_injection,
)
from lib_sast.symbolic_eval.types import (
    Evaluator,
    SymbolicEvalArgs,
    SymbolicEvaluation,
)

METHOD_EVALUATORS: dict[MethodsEnum, Evaluator] = {
    MethodsEnum.CSHARP_VULN_REGULAR_EXPRESSION: cs_vuln_regex,
    MethodsEnum.JAVA_VULN_REGEX: java_vuln_regex,
    MethodsEnum.KOTLIN_VULN_REGEX: kt_vuln_regex,
    MethodsEnum.PYTHON_REGEX_INJECTION: python_regex_injection,
}


def evaluate(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    if language_evaluator := METHOD_EVALUATORS.get(args.method):
        return language_evaluator(args)
    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)
