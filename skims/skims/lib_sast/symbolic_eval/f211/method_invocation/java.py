from lib_sast.symbolic_eval.common import (
    owasp_user_connection,
)
from lib_sast.symbolic_eval.types import (
    SymbolicEvalArgs,
    SymbolicEvaluation,
)


def java_vuln_regex(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    if owasp_user_connection(args):
        args.triggers.add("userparameters")

    n_attrs = args.graph.nodes[args.n_id]
    if (
        n_attrs["expression"] == "quote"
        and args.graph.nodes[n_attrs["object_id"]].get("symbol") == "Pattern"
    ):
        args.triggers.add("saferegex")

    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)
