from lib_sast.symbolic_eval.types import (
    SymbolicEvalArgs,
    SymbolicEvaluation,
)
from lib_sast.utils.graph import (
    get_nodes_by_path,
)


def js_ls_sens_data_this(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    nodes = args.graph.nodes
    if (
        nodes[args.n_id].get("expression") == "localStorage.setItem"
        and (
            suspicious_n_ids := get_nodes_by_path(
                args.graph,
                args.n_id,
                (),
                "ArgumentList",
                "MemberAccess",
            )
        )
        and (suspicious_n_id := next(iter(suspicious_n_ids), None))
        and (nodes[suspicious_n_id].get("expression") == "this")
    ):
        args.triggers.add(f"this_{suspicious_n_id}")
        args.evaluation[args.n_id] = True

    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)
