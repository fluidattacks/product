from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.symbolic_eval.f344.method_invocation.common import (
    js_ls_sens_data_this as sens_this,
)
from lib_sast.symbolic_eval.types import (
    Evaluator,
    SymbolicEvalArgs,
    SymbolicEvaluation,
)

METHOD_EVALUATORS: dict[MethodsEnum, Evaluator] = {
    MethodsEnum.JAVASCRIPT_LOCAL_STORAGE_SENSITIVE_DATA_ASYNC: sens_this,
    MethodsEnum.TYPESCRIPT_LOCAL_STORAGE_SENSITIVE_DATA_ASSIGNMENT: sens_this,
}


def evaluate(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    if language_evaluator := METHOD_EVALUATORS.get(args.method):
        return language_evaluator(args)
    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)
