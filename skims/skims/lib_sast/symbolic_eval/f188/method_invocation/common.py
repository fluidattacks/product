from lib_sast.symbolic_eval.types import (
    SymbolicEvalArgs,
    SymbolicEvaluation,
)


def has_origin_check(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    if ".origin" in args.graph.nodes[args.n_id].get("expression"):
        args.triggers.add("origin_comparison")

    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)
