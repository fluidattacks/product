from lib_sast.symbolic_eval.types import (
    SymbolicEvalArgs,
    SymbolicEvaluation,
)


def c_sharp_code_injection(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    http_methods = {"HttpResponse", "Response"}
    if args.graph.nodes[args.n_id].get("variable_type") in http_methods:
        args.triggers.add("source")
    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)
