from lib_sast.symbolic_eval.types import (
    SymbolicEvalArgs,
    SymbolicEvaluation,
)


def c_sharp_log_injection(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    expr = str(args.graph.nodes[args.n_id]["expression"]).lower()
    if expr.endswith((".replace", ".encode")):
        args.triggers.add("sanitized")
    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)
