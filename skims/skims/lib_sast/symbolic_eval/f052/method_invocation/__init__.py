from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.symbolic_eval.f052.method_invocation.java import (
    java_check_jwt_decode,
    java_check_jwt_invocations,
    java_engine_cipher_ssl,
    java_weak_rsa_key,
)
from lib_sast.symbolic_eval.f052.method_invocation.kotlin import (
    kt_insec_key_gen,
    kt_insec_key_pair_gen,
    kt_insecure_cert,
)
from lib_sast.symbolic_eval.f052.method_invocation.swift import (
    swift_arg_from_literal,
)
from lib_sast.symbolic_eval.types import (
    Evaluator,
    SymbolicEvalArgs,
    SymbolicEvaluation,
)

METHOD_EVALUATORS: dict[MethodsEnum, Evaluator] = {
    MethodsEnum.JAVA_JWT_WITHOUT_PROPER_SIGN: java_check_jwt_invocations,
    MethodsEnum.JAVA_JWT_UNSAFE_DECODE: java_check_jwt_decode,
    MethodsEnum.JAVA_INSECURE_ENGINE_CIPHER_SSL: java_engine_cipher_ssl,
    MethodsEnum.KT_INSECURE_CERTIFICATE_VALIDATION: kt_insecure_cert,
    MethodsEnum.KT_INSECURE_KEY_GEN: kt_insec_key_gen,
    MethodsEnum.KT_INSECURE_KEY_PAIR_GEN: kt_insec_key_pair_gen,
    MethodsEnum.SWIFT_HC_SECRET_JWT: swift_arg_from_literal,
    MethodsEnum.JAVA_WEAK_RSA_KEY: java_weak_rsa_key,
}


def evaluate(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    if language_evaluator := METHOD_EVALUATORS.get(args.method):
        return language_evaluator(args)
    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)
