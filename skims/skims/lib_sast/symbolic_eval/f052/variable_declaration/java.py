from lib_sast.sast_model import (
    Graph,
    NId,
)
from lib_sast.symbolic_eval.common import (
    INSECURE_HASHES,
)
from lib_sast.symbolic_eval.types import (
    SymbolicEvalArgs,
    SymbolicEvaluation,
)
from lib_sast.utils.graph import (
    adj_ast,
    match_ast_d,
)
from utils.crypto import (
    is_vulnerable_cipher,
)


def get_first_string(graph: Graph, n_id: NId) -> str | None:
    if first_str := match_ast_d(graph, n_id, "Literal", -1):
        return str(graph.nodes[first_str].get("value"))
    return None


def get_first_arg_value(graph: Graph, n_id: NId) -> str | None:
    if (first_arg := adj_ast(graph, n_id)[0]) and (value := graph.nodes[first_arg].get("value")):
        return value
    return None


def get_file_name(args: SymbolicEvalArgs) -> str | None:
    loader_var = next(
        (
            trigger.split("__")[1]
            for trigger in args.triggers
            if trigger.startswith("loader_name__")
        ),
        None,
    )

    if not loader_var:
        return None

    graph = args.graph
    for n_id in args.path:
        n_attrs = graph.nodes[n_id]
        if (
            n_attrs["label_type"] == "MethodInvocation"
            and n_attrs["expression"] == "load"
            and (obj_id := graph.nodes[n_id].get("object_id"))
            and graph.nodes[obj_id].get("symbol") == loader_var
        ):
            file_name = get_first_string(graph, n_id)
            if str(file_name).endswith(".properties"):
                return file_name
    return None


def java_insecure_hash(
    args: SymbolicEvalArgs,
) -> SymbolicEvaluation:
    graph = args.graph
    nodes = graph.nodes
    node = nodes[args.n_id]
    if not (args.graph_db and args.graph_db.properties):
        return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)

    if (node.get("variable_type") == "java.util.Properties") and (var_name := node.get("variable")):
        args.triggers.add(f"loader_name__{var_name}")

    if (
        (val_id := node.get("value_id"))
        and (nodes[val_id].get("expression") == "getProperty")
        and (args_id := nodes[val_id].get("arguments_id"))
        and (load_key := get_first_arg_value(graph, args_id))
        and (file_name := get_file_name(args))
    ):
        p_value = args.graph_db.properties.get(file_name, {}).get(load_key)
        if str(p_value).lower() in INSECURE_HASHES:
            args.evaluation[args.n_id] = True

    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)


def java_insecure_cipher(
    args: SymbolicEvalArgs,
) -> SymbolicEvaluation:
    if not (args.graph_db and args.graph_db.properties):
        return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)
    graph = args.graph

    n_attrs = graph.nodes[args.n_id]
    if (n_attrs.get("variable_type") == "java.util.Properties") and (
        var_name := n_attrs.get("variable")
    ):
        args.triggers.add(f"loader_name__{var_name}")

    if (
        (val_id := n_attrs.get("value_id"))
        and (graph.nodes[val_id].get("expression") == "getProperty")
        and (args_id := graph.nodes[val_id].get("arguments_id"))
        and (load_key := get_first_arg_value(graph, args_id))
        and (file_name := get_file_name(args))
    ):
        p_value = args.graph_db.properties.get(file_name, {}).get(load_key)
        if p_value:
            alg, mode, pad, *_ = (p_value + "///").split("/", 3)
            if is_vulnerable_cipher(alg, mode, pad):
                args.evaluation[args.n_id] = True
            else:
                args.evaluation[args.n_id] = False

    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)


def java_check_jwt_declaration(
    args: SymbolicEvalArgs,
) -> SymbolicEvaluation:
    graph = args.graph

    if graph.nodes[args.n_id].get("variable_type") == "JwtBuilder":
        args.triggers.add("builder_instance")
    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)
