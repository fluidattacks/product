from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.symbolic_eval.f052.pair.common import (
    insecure_key_pair,
    insecure_mode,
)
from lib_sast.symbolic_eval.types import (
    Evaluator,
    SymbolicEvalArgs,
    SymbolicEvaluation,
)

METHOD_EVALUATORS: dict[MethodsEnum, Evaluator] = {
    MethodsEnum.JAVASCRIPT_INSECURE_ENCRYPT: insecure_mode,
    MethodsEnum.TYPESCRIPT_INSECURE_ENCRYPT: insecure_mode,
    MethodsEnum.JAVASCRIPT_INSECURE_EC_KEYPAIR: insecure_key_pair,
    MethodsEnum.TYPESCRIPT_INSECURE_EC_KEYPAIR: insecure_key_pair,
    MethodsEnum.JAVASCRIPT_INSECURE_RSA_KEYPAIR: insecure_key_pair,
    MethodsEnum.TS_INSECURE_RSA_KEYPAIR: insecure_key_pair,
}


def evaluate(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    if language_evaluator := METHOD_EVALUATORS.get(args.method):
        return language_evaluator(args)
    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)
