from lib_sast.symbolic_eval.common import (
    INSECURE_MODES,
)
from lib_sast.symbolic_eval.types import (
    SymbolicEvalArgs,
    SymbolicEvaluation,
)

INSECURE_ALGOS = {
    "TripleDES",
    "Blowfish",
    "ARC4",
}


def python_unsafe_ciphers(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    n_attrs = args.graph.nodes[args.n_id]
    member = n_attrs["member"]
    expr = n_attrs["expression"]

    if member in INSECURE_ALGOS and expr == "algorithms":
        args.evaluation[args.n_id] = True
        # Any mode is dangerous with these algorithms
        args.triggers.add("unsafemode")
    if member == "AES" and expr == "algorithms":
        args.evaluation[args.n_id] = True
    if member.lower() in INSECURE_MODES and expr == "modes":
        args.triggers.add("unsafemode")

    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)


def py_insecure_cipher_mode(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    insecure_modes = {"MODE_ECB", "MODE_CBC"}
    ma_attr = args.graph.nodes[args.n_id]

    if ma_attr["expression"] == "AES" and ma_attr["member"] in insecure_modes:
        args.evaluation[args.n_id] = True

    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)
