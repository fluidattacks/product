from lib_sast.symbolic_eval.types import (
    SymbolicEvalArgs,
    SymbolicEvaluation,
)


def go_comes_from_jwt(
    args: SymbolicEvalArgs,
) -> SymbolicEvaluation:
    n_attrs = args.graph.nodes[args.n_id]

    if n_attrs.get("expression") == "jwt":
        args.triggers.add("jwt")

    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)
