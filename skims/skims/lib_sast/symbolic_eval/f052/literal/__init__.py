from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.symbolic_eval.f052.literal.c_sharp import (
    cs_disabled_strong_crypto,
    cs_insecure_keys,
    cs_rsa_secure_mode,
)
from lib_sast.symbolic_eval.f052.literal.common import (
    insecure_create_cipher,
)
from lib_sast.symbolic_eval.f052.literal.java import (
    java_engine_cipher_ssl,
    java_evaluate_cipher,
    java_evaluate_cipher_mode,
    java_evaluate_key_spec,
    java_insecure_cipher_jmqi,
    java_insecure_cipher_ssl,
    java_insecure_hash,
    java_insecure_key_ec,
    java_insecure_key_rsa,
    java_uses_unsafe_alg,
)
from lib_sast.symbolic_eval.f052.literal.javascript import (
    insecure_ecdh_key,
    insecure_hash,
    insecure_key_pair,
)
from lib_sast.symbolic_eval.f052.literal.kotlin import (
    kt_insecure_cipher,
    kt_insecure_cipher_mode,
    kt_insecure_cipher_ssl,
    kt_insecure_hash,
    kt_insecure_key,
    kt_insecure_key_ec,
    kt_insecure_parm_espec,
)
from lib_sast.symbolic_eval.f052.literal.swift import (
    swift_insecure_crypto,
)
from lib_sast.symbolic_eval.types import (
    Evaluator,
    SymbolicEvalArgs,
    SymbolicEvaluation,
)

METHOD_EVALUATORS: dict[MethodsEnum, Evaluator] = {
    MethodsEnum.C_SHARP_DISABLED_STRONG_CRYPTO: cs_disabled_strong_crypto,
    MethodsEnum.C_SHARP_INSECURE_KEYS: cs_insecure_keys,
    MethodsEnum.C_SHARP_RSA_SECURE_MODE: cs_rsa_secure_mode,
    MethodsEnum.JAVA_INSECURE_CIPHER: java_evaluate_cipher,
    MethodsEnum.JAVA_INSECURE_CIPHER_MODE: java_evaluate_cipher_mode,
    MethodsEnum.JAVA_INSECURE_CIPHER_SSL: java_insecure_cipher_ssl,
    MethodsEnum.JAVA_INSECURE_ENGINE_CIPHER_SSL: java_engine_cipher_ssl,
    MethodsEnum.JAVA_INSECURE_CIPHER_JMQI: java_insecure_cipher_jmqi,
    MethodsEnum.JAVA_INSECURE_HASH: java_insecure_hash,
    MethodsEnum.JAVA_INSECURE_KEY_EC: java_insecure_key_ec,
    MethodsEnum.JAVA_INSECURE_KEY_RSA: java_insecure_key_rsa,
    MethodsEnum.JAVA_INSECURE_KEY_SECRET: java_evaluate_key_spec,
    MethodsEnum.JAVA_INSEC_SIGN_ALGORITHM: java_uses_unsafe_alg,
    MethodsEnum.JAVASCRIPT_INSECURE_CREATE_CIPHER: insecure_create_cipher,
    MethodsEnum.JAVASCRIPT_INSECURE_ECDH_KEY: insecure_ecdh_key,
    MethodsEnum.TS_INSECURE_ECDH_KEY: insecure_ecdh_key,
    MethodsEnum.JAVASCRIPT_INSECURE_EC_KEYPAIR: insecure_key_pair,
    MethodsEnum.JAVASCRIPT_INSECURE_HASH: insecure_hash,
    MethodsEnum.KOTLIN_INSECURE_CIPHER: kt_insecure_cipher,
    MethodsEnum.KOTLIN_INSECURE_CIPHER_MODE: kt_insecure_cipher_mode,
    MethodsEnum.KOTLIN_INSECURE_CIPHER_SSL: kt_insecure_cipher_ssl,
    MethodsEnum.KOTLIN_INSECURE_HASH: kt_insecure_hash,
    MethodsEnum.KT_INSECURE_KEY_PAIR_GEN: kt_insecure_key,
    MethodsEnum.KOTLIN_INSECURE_KEY: kt_insecure_key,
    MethodsEnum.KOTLIN_INSECURE_KEY_EC: kt_insecure_key_ec,
    MethodsEnum.KT_INSECURE_PARAMETER_SPEC: kt_insecure_parm_espec,
    MethodsEnum.KT_INSECURE_ENCRYPTION_KEY: kt_insecure_parm_espec,
    MethodsEnum.TS_INSECURE_CREATE_CIPHER: insecure_create_cipher,
    MethodsEnum.TYPESCRIPT_INSECURE_EC_KEYPAIR: insecure_key_pair,
    MethodsEnum.TYPESCRIPT_INSECURE_HASH: insecure_hash,
    MethodsEnum.JAVASCRIPT_INSECURE_RSA_KEYPAIR: insecure_key_pair,
    MethodsEnum.TS_INSECURE_RSA_KEYPAIR: insecure_key_pair,
    MethodsEnum.SWIFT_INSECURE_CRYPTOR: swift_insecure_crypto,
}


def evaluate(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    if language_evaluator := METHOD_EVALUATORS.get(args.method):
        return language_evaluator(args)
    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)
