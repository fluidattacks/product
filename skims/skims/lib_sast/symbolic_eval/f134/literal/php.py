from lib_sast.symbolic_eval.types import (
    SymbolicEvalArgs,
    SymbolicEvaluation,
)


def php_insecure_cors(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    val = args.graph.nodes[args.n_id]["value"].strip(" ").lower()
    if "access-control-allow-origin" in val:
        args.triggers.add("dang_header")
    if val in {"*", "access-control-allow-origin: *"}:
        args.triggers.add("dang_val")

    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)
