from lib_sast.symbolic_eval.types import (
    SymbolicEvalArgs,
    SymbolicEvaluation,
)


def python_fastapi_insecure_cors(
    args: SymbolicEvalArgs,
) -> SymbolicEvaluation:
    if (
        (val_id := args.graph.nodes[args.n_id].get("value_id"))
        and (args.graph.nodes[val_id].get("label_type") == "MethodInvocation")
        and (exp_id := args.graph.nodes[val_id].get("expression_id"))
        and (args.graph.nodes[exp_id].get("symbol") == "FastAPI")
    ):
        args.triggers.add("python_fastapi")

    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)


def python_flask_insecure_cors(
    args: SymbolicEvalArgs,
) -> SymbolicEvaluation:
    if (
        (val_id := args.graph.nodes[args.n_id].get("value_id"))
        and (args.graph.nodes[val_id].get("label_type") == "MethodInvocation")
        and (exp_id := args.graph.nodes[val_id].get("expression_id"))
        and (args.graph.nodes[exp_id].get("symbol") == "Flask")
    ):
        args.triggers.add("python_flask")

    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)
