from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.symbolic_eval.f063.parameter.c_sharp import (
    cs_unsafe_path_traversal,
)
from lib_sast.symbolic_eval.f063.parameter.java import (
    java_unsafe_path_traversal,
    java_zip_slip_injection,
)
from lib_sast.symbolic_eval.f063.parameter.typescript import (
    user_input,
)
from lib_sast.symbolic_eval.types import (
    Evaluator,
    SymbolicEvalArgs,
    SymbolicEvaluation,
)

METHOD_EVALUATORS: dict[MethodsEnum, Evaluator] = {
    MethodsEnum.C_SHARP_OPEN_REDIRECT: cs_unsafe_path_traversal,
    MethodsEnum.C_SHARP_UNSAFE_PATH_TRAVERSAL: cs_unsafe_path_traversal,
    MethodsEnum.JAVA_ZIP_SLIP_INJECTION: java_zip_slip_injection,
    MethodsEnum.JAVA_UNSAFE_PATH_TRAVERSAL: java_unsafe_path_traversal,
    MethodsEnum.TS_ZIP_SLIP: user_input,
}


def evaluate(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    if language_evaluator := METHOD_EVALUATORS.get(args.method):
        return language_evaluator(args)
    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)
