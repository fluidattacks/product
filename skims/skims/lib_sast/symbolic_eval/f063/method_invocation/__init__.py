from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.symbolic_eval.f063.method_invocation.java import (
    java_unsafe_path_traversal,
)
from lib_sast.symbolic_eval.f063.method_invocation.php import (
    php_unsafe_path_traversal,
)
from lib_sast.symbolic_eval.types import (
    Evaluator,
    SymbolicEvalArgs,
    SymbolicEvaluation,
)

METHOD_EVALUATORS: dict[MethodsEnum, Evaluator] = {
    MethodsEnum.JAVA_UNSAFE_PATH_TRAVERSAL: java_unsafe_path_traversal,
    MethodsEnum.PHP_UNSAFE_PATH_TRAVERSAL: php_unsafe_path_traversal,
}


def evaluate(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    if language_evaluator := METHOD_EVALUATORS.get(args.method):
        return language_evaluator(args)
    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)
