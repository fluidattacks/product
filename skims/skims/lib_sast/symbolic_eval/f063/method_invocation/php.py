from lib_sast.symbolic_eval.types import (
    SymbolicEvalArgs,
    SymbolicEvaluation,
)


def php_unsafe_path_traversal(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    ma_attr = args.graph.nodes[args.n_id]
    if ma_attr["expression"] == "tempnam":
        args.triggers.add("Sanitized")

    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)
