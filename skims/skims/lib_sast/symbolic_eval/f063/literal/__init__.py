from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.symbolic_eval.f063.literal.common import (
    has_extension,
)
from lib_sast.symbolic_eval.f063.literal.python import (
    path_traversal,
)
from lib_sast.symbolic_eval.types import (
    Evaluator,
    SymbolicEvalArgs,
    SymbolicEvaluation,
)

METHOD_EVALUATORS: dict[MethodsEnum, Evaluator] = {
    MethodsEnum.JS_ZIP_SLIP: has_extension,
    MethodsEnum.PYTHON_IO_PATH_TRAVERSAL: path_traversal,
    MethodsEnum.TS_ZIP_SLIP: has_extension,
}


def evaluate(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    if language_evaluator := METHOD_EVALUATORS.get(args.method):
        return language_evaluator(args)
    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)
