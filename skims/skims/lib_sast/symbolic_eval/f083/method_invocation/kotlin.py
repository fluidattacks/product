from lib_sast.symbolic_eval.types import (
    SymbolicEvalArgs,
    SymbolicEvaluation,
)
from lib_sast.utils.graph import (
    adj_ast,
    match_ast_d,
)

features_general_entities = (
    "http://xml.org/sax/features/external-general-entities",
    "false",
)

feature_doctype_decl = (
    "http://apache.org/xml/features/disallow-doctype-decl",
    "true",
)

feature_parameter_entities = (
    "http://xml.org/sax/features/external-parameter-entities",
    "false",
)


methods_inits = {
    "createXMLReader",
    "newInstance",
    "newDocumentBuilder",
    "newSAXParser",
    "DocumentBuilderFactory",
}


def kt_xml_parser(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    graph = args.graph
    expression = graph.nodes[args.n_id]["expression"].split(".")
    if expression[-1] in methods_inits:
        args.evaluation[args.n_id] = True
    if (
        expression[-1] == "setFeature"
        and (arg_list := match_ast_d(graph, args.n_id, "ArgumentList"))
        and (arg_features := adj_ast(graph, arg_list))
    ):
        arg_features = (
            graph.nodes[arg_features[0]].get("value", ""),
            graph.nodes[arg_features[1]].get("value", ""),
        )
        if arg_features == features_general_entities:
            args.triggers.add("featureEntitiesSetted")
        if arg_features == feature_doctype_decl:
            args.triggers.add("featureDoctypeSetted")
        if arg_features == feature_parameter_entities:
            args.triggers.add("featureParameterSetted")
    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)
