from lib_sast.symbolic_eval.types import (
    SymbolicEvalArgs,
    SymbolicEvaluation,
)


def cs_stored_pass(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    args.evaluation[args.n_id] = False

    if str_value := str(args.graph.nodes[args.n_id]["value"]):
        if len(args.triggers) == 0:
            args.triggers.add(str_value)
        else:
            curr_value = next(iter(args.triggers))
            args.triggers.clear()
            args.triggers.add(curr_value + str_value)

    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)
