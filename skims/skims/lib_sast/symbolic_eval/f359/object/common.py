from lib_sast.sast_model import (
    Graph,
    NId,
)
from lib_sast.symbolic_eval.types import (
    SymbolicEvalArgs,
    SymbolicEvaluation,
)
from lib_sast.utils.graph import (
    adj_ast,
)


def get_key_value(graph: Graph, nid: NId) -> tuple[str, str]:
    key_id = graph.nodes[nid]["key_id"]
    key = graph.nodes[key_id].get("symbol", graph.nodes[key_id].get("value", ""))
    value_id = graph.nodes[nid]["value_id"]
    value = ""
    if graph.nodes[value_id]["label_type"] == "ArrayInitializer":
        child_id = adj_ast(graph, value_id, label_type="Literal")
        if len(child_id) > 0:
            value = graph.nodes[child_id[0]].get("value", "")
    else:
        value = graph.nodes[value_id].get("value", "")
    return key, value


def credentials_inside_test(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    graph = args.graph
    childs = adj_ast(graph, args.n_id, label_type="Pair")
    if len(childs) == 2:
        email_key, _ = get_key_value(graph, childs[0])
        pass_key, _ = get_key_value(graph, childs[1])
        if (
            email_key == "email"
            and pass_key in {"password", "pass"}
            and graph.nodes[graph.nodes[childs[1]]["value_id"]]["label_type"] == "Literal"
            and graph.nodes[graph.nodes[childs[1]]["value_id"]].get("value_type", "") != "null"
        ):
            args.evaluation[args.n_id] = True
            args.triggers.update({childs[0], childs[1]})
    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)
