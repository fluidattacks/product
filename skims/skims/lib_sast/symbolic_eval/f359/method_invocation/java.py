from lib_sast.root.utilities.common import (
    get_n_arg,
    has_string_definition,
)
from lib_sast.symbolic_eval.types import (
    SymbolicEvalArgs,
    SymbolicEvaluation,
)


def comes_from_jwt(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    symmetric_algorithms = {
        "HMAC256",
        "HMAC384",
        "HMAC512",
    }

    nodes = args.graph.nodes

    symbol: str | None = None

    expression = nodes[args.n_id].get("expression", "")

    if object_n_id := nodes[args.n_id].get("object_id", ""):
        symbol = nodes[object_n_id].get("symbol", "")

    if expression == "create" and symbol == "JWT":
        args.triggers.add("JWT")

    if (
        expression in symmetric_algorithms
        and symbol == "Algorithm"
        and (f_arg := get_n_arg(args.graph, args.n_id, 0))
        and has_string_definition(args.graph, f_arg)
    ):
        args.evaluation[args.n_id] = True

    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)
