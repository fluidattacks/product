from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.symbolic_eval.f395.method_invocation.c_sharp import (
    c_sharp_hardcoded_init_vector,
)
from lib_sast.symbolic_eval.f395.method_invocation.java import (
    get_bytes_from_string,
)
from lib_sast.symbolic_eval.types import (
    Evaluator,
    SymbolicEvalArgs,
    SymbolicEvaluation,
)

METHOD_EVALUATORS: dict[MethodsEnum, Evaluator] = {
    MethodsEnum.C_SHARP_HARDCODED_INIT_VECTOR: c_sharp_hardcoded_init_vector,
    MethodsEnum.JAVA_HARDCODED_INIT_VECTOR: get_bytes_from_string,
}


def evaluate(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    if language_evaluator := METHOD_EVALUATORS.get(args.method):
        return language_evaluator(args)
    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)
