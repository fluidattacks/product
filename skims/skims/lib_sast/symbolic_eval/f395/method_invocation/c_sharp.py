from lib_sast.symbolic_eval.types import (
    SymbolicEvalArgs,
    SymbolicEvaluation,
)


def c_sharp_hardcoded_init_vector(
    args: SymbolicEvalArgs,
) -> SymbolicEvaluation:
    if (expr := args.graph.nodes[args.n_id].get("expression")) and expr == "Aes.Create":
        args.triggers.add("createAes")
        args.evaluation[args.n_id] = True

    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)
