from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.symbolic_eval.f395.variable_declaration.java import var_is_literal_array
from lib_sast.symbolic_eval.types import (
    Evaluator,
    SymbolicEvalArgs,
    SymbolicEvaluation,
)

METHOD_EVALUATORS: dict[MethodsEnum, Evaluator] = {
    MethodsEnum.JAVA_HARDCODED_INIT_VECTOR: var_is_literal_array,
}


def evaluate(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    if language_evaluator := METHOD_EVALUATORS.get(args.method):
        return language_evaluator(args)
    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)
