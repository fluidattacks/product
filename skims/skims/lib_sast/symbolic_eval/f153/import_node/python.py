from lib_sast.symbolic_eval.common import (
    PYTHON_REQUESTS_LIBRARIES,
)
from lib_sast.symbolic_eval.types import (
    SymbolicEvalArgs,
    SymbolicEvaluation,
)


def danger_mime_type(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    n_attrs = args.graph.nodes[args.n_id]
    if (expression := n_attrs.get("expression")) and (
        expression.split(".")[0] in PYTHON_REQUESTS_LIBRARIES
    ):
        args.triggers.add("client_connection")

    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)
