from lib_sast.symbolic_eval.types import (
    SymbolicEvalArgs,
    SymbolicEvaluation,
)
from lib_sast.utils.graph import (
    match_ast_group_d,
)


def insecure_type(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    for pair_id in match_ast_group_d(args.graph, args.n_id, "Pair"):
        pair_node = args.graph.nodes[pair_id]
        key = args.graph.nodes[pair_node["key_id"]].get("symbol")
        value = args.graph.nodes[pair_node["value_id"]].get("value", "")
        if key == "type" and value == "*/*":
            args.evaluation[args.n_id] = True

    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)
