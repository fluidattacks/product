from lib_sast.reachability.f004.javascript.js_cve_2020_7712 import (
    js_cve_2020_7712 as _js_cve_2020_7712,
)
from lib_sast.reachability.f004.javascript.js_cve_2023_32314 import (
    js_cve_2023_32314 as _js_cve_2023_32314,
)
from lib_sast.reachability.f004.javascript.js_cve_2023_37466 import (
    js_cve_2023_37466 as _js_cve_2023_37466,
)
from lib_sast.reachability.f004.javascript.js_cve_2023_37903 import (
    js_cve_2023_37903 as _js_cve_2023_37903,
)
from lib_sast.reachability.f004.python.py_cve_2022_22817 import (
    py_cve_2022_22817 as _py_cve_2022_22817,
)
from lib_sast.reachability.f004.typescript.ts_cve_2020_7712 import (
    ts_cve_2020_7712 as _ts_cve_2020_7712,
)
from lib_sast.reachability.f004.typescript.ts_cve_2023_32314 import (
    ts_cve_2023_32314 as _ts_cve_2023_32314,
)
from lib_sast.reachability.f004.typescript.ts_cve_2023_37466 import (
    ts_cve_2023_37466 as _ts_cve_2023_37466,
)
from lib_sast.reachability.f004.typescript.ts_cve_2023_37903 import (
    ts_cve_2023_37903 as _ts_cve_2023_37903,
)
from lib_sast.reachability.types import (
    MethodsArgs,
)
from lib_sast.root.utilities.common import (
    SHIELD_BLOCKING,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def js_cve_2023_32314(
    methods_args: MethodsArgs,
) -> MethodExecutionResult:
    return _js_cve_2023_32314(methods_args=methods_args)


@SHIELD_BLOCKING
def js_cve_2020_7712(
    methods_args: MethodsArgs,
) -> MethodExecutionResult:
    return _js_cve_2020_7712(methods_args=methods_args)


@SHIELD_BLOCKING
def js_cve_2023_37466(
    methods_args: MethodsArgs,
) -> MethodExecutionResult:
    return _js_cve_2023_37466(methods_args=methods_args)


@SHIELD_BLOCKING
def js_cve_2023_37903(
    methods_args: MethodsArgs,
) -> MethodExecutionResult:
    return _js_cve_2023_37903(methods_args=methods_args)


@SHIELD_BLOCKING
def ts_cve_2020_7712(
    methods_args: MethodsArgs,
) -> MethodExecutionResult:
    return _ts_cve_2020_7712(methods_args=methods_args)


@SHIELD_BLOCKING
def ts_cve_2023_37903(
    methods_args: MethodsArgs,
) -> MethodExecutionResult:
    return _ts_cve_2023_37903(methods_args=methods_args)


@SHIELD_BLOCKING
def ts_cve_2023_37466(
    methods_args: MethodsArgs,
) -> MethodExecutionResult:
    return _ts_cve_2023_37466(methods_args=methods_args)


@SHIELD_BLOCKING
def ts_cve_2023_32314(
    methods_args: MethodsArgs,
) -> MethodExecutionResult:
    return _ts_cve_2023_32314(methods_args=methods_args)


@SHIELD_BLOCKING
def py_cve_2022_22817(
    methods_args: MethodsArgs,
) -> MethodExecutionResult:
    return _py_cve_2022_22817(methods_args=methods_args)
