from collections.abc import Callable, Iterator

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.reachability.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.reachability.types import (
    MethodsArgs,
)
from lib_sast.sast_model import Graph, NId
from lib_sast.symbolic_eval.evaluate import (
    get_node_evaluation_results,
)
from lib_sast.symbolic_eval.types import (
    SymbolicEvalArgs,
    SymbolicEvaluation,
)
from lib_sast.utils.graph import (
    adj_ast,
)
from model.core import (
    MethodExecutionResult,
)


def aux_evaluator(graph: Graph, n_id: NId) -> bool:
    for assign_id in adj_ast(graph, n_id, label_type="Assignment"):
        if (var_id := graph.nodes[assign_id].get("variable_id")) and graph.nodes[var_id].get(
            "symbol",
            "",
        ) == "MaxDepth":
            return True

    return False


def object_creation_evaluator(
    args: SymbolicEvalArgs,
) -> SymbolicEvaluation:
    if (
        args.graph.nodes[args.n_id].get("name", "") == "JsonSerializerOptions"
        and (init_id := args.graph.nodes[args.n_id].get("initializer_id"))
        and aux_evaluator(args.graph, init_id)
    ):
        args.triggers.add("secure")

    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)


def parameter_evaluator(args: SymbolicEvalArgs) -> SymbolicEvaluation:
    if args.graph.nodes[args.n_id].get("variable_type") == "HttpRequest":
        args.evaluation[args.n_id] = True

    return SymbolicEvaluation(args.evaluation[args.n_id], args.triggers)


METHOD_EVALUATORS: dict[str, Callable[[SymbolicEvalArgs], SymbolicEvaluation]] = {
    "object_creation": object_creation_evaluator,
    "parameter": parameter_evaluator,
}


def c_sharp_cve_2024_21907(
    methods_args: MethodsArgs,
) -> MethodExecutionResult:
    method = MethodsEnum.C_SHARP_CVE_2024_21907

    def n_ids() -> Iterator[NId]:
        graph = methods_args.shard.syntax_graph
        for n_id in methods_args.nodes:
            if (
                graph.nodes[n_id]["expression"].split(".")[-1].lower()
                in {"writeto", "tostring", "serializeobject", "deserializeobject"}
                and "newtonsoft.json" in methods_args.var
                and get_node_evaluation_results(
                    method,
                    graph,
                    n_id,
                    set(),
                    method_evaluators=METHOD_EVALUATORS,
                )
            ):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        method=method,
        shard=methods_args.shard,
        method_calls=len(methods_args.nodes),
        dep=methods_args.dep,
        version=methods_args.current_version,
        cve=methods_args.cve,
        pkg_id=methods_args.pkg_id,
    )
