from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.reachability.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.reachability.types import (
    MethodsArgs,
)
from lib_sast.root.utilities.common import (
    get_parent_scope_n_id,
)
from lib_sast.sast_model import (
    Graph,
    NId,
)
from lib_sast.utils import (
    graph as g,
)
from model.core import (
    MethodExecutionResult,
)


def has_modified_n_support(graph: Graph, n_id: NId) -> bool:
    nodes = graph.nodes
    p_n_id_0, p_n_id_1 = g.pred(graph, n_id, 2)

    return bool(
        nodes[p_n_id_0].get("label_type") == "ElementAccess"
        and (args_n_id := nodes[p_n_id_0].get("arguments_id"))
        and (nodes[args_n_id].get("value") == "0")
        and (nodes[p_n_id_1].get("label_type") == "Assignment")  # noqa: COM812
    )


def n_support_modified(graph: Graph, n_id: str, obj: str) -> bool:
    return bool(
        (p_n_id := get_parent_scope_n_id(graph, n_id))
        and any(
            has_modified_n_support(graph, node_id)
            for node_id in g.adj_ast(
                graph,
                p_n_id,
                -1,
                label_type="MemberAccess",
                member="_n_support",
                expression=obj,
            )
        ),
    )


def py_cve_2020_28975(
    methods_args: MethodsArgs,
) -> MethodExecutionResult:
    method = MethodsEnum.PY_CVE_2020_28975

    def n_ids() -> Iterator[NId]:
        graph = methods_args.shard.syntax_graph
        nodes = graph.nodes

        for n_id in methods_args.nodes:
            if (
                (exp := nodes[n_id].get("expression", "")).endswith("predict")
                and (obj := ".".join(exp.split(".")[:-1]))
                and n_support_modified(graph, n_id, obj)
            ):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        method=method,
        shard=methods_args.shard,
        method_calls=len(methods_args.nodes),
        dep=methods_args.dep,
        version=methods_args.current_version,
        cve=methods_args.cve,
        pkg_id=methods_args.pkg_id,
    )
