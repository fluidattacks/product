from lib_sast.reachability.f098.python.py_cve_2024_39303 import (
    py_cve_2024_39303 as _py_cve_2024_39303,
)
from lib_sast.reachability.types import (
    MethodsArgs,
)
from lib_sast.root.utilities.common import (
    SHIELD_BLOCKING,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def py_cve_2024_39303(
    methods_args: MethodsArgs,
) -> MethodExecutionResult:
    return _py_cve_2024_39303(methods_args=methods_args)
