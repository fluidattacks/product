import os
import time
from collections.abc import (
    Iterator,
)

import ctx
from fluidattacks_core.serializers.snippet import (
    SnippetViewport,
    make_snippet,
)
from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.reachability.types import (
    Query,
)
from lib_sast.reachability.utils import (
    find_lock_file,
)
from lib_sast.sast_model import (
    DEPENDENCY_FILES,
    POSSIBLE_LOCK_FILES,
    SUPPORTED_LANGUAGES,
    GraphShard,
    GraphShardMetadataLanguage,
    NId,
)
from model.core import (
    MethodExecutionResult,
    ScaAdvisoriesMetadata,
    Vulnerability,
)
from utils.build_vulns import (
    build_lines_vuln,
    build_metadata,
)
from utils.match_versions import (
    match_vulnerable_versions,
)
from utils.statics import (
    calculate_time_coefficient,
)
from utils.string_handlers import (
    is_exclusion,
)
from utils.translations import (
    t,
)


def find_dependency_file_by_extension(
    current_path: str,
    dependency_file: str,
    possible_locks: list[str],
) -> str:
    if os.path.isfile(current_path):  # noqa: PTH113
        current_path = (
            os.path.dirname(current_path)  # noqa: PTH120
            if os.path.dirname(current_path)  # noqa: PTH120
            else "."
        )
    while current_path:
        for file in os.listdir(current_path):
            if file.endswith(dependency_file):
                csproj_path = os.path.join(current_path, file)  # noqa: PTH118
                if lock_candidate := find_lock_file(current_path, possible_locks):
                    return lock_candidate.strip("./")
                return csproj_path.strip("./")

        current_path = os.path.dirname(current_path)  # noqa: PTH120

    return ""


def aux_find_dependency_file(
    current_path: str,
    dependency_file: str,
    possible_locks: list[str],
) -> str:
    while current_path is not None:
        candidate = os.path.join(current_path, dependency_file)  # noqa: PTH118
        if os.path.isfile(candidate):  # noqa: PTH113
            if lock_candidate := find_lock_file(current_path, possible_locks):
                return lock_candidate
            return candidate
        if current_path == "":
            break
        current_path = os.path.dirname(current_path)  # noqa: PTH120
    return ""


def find_dependency_file(start_path: str, lang: GraphShardMetadataLanguage) -> str:
    """Find the dependency file corresponding to the given path.

    Args:
        start_path (str): The path from which to start the search.
        lang: The language of the file.

    Returns:
        str : The path to the found dependency file,
        or "" if no dependency file is found.

    """
    current_path = start_path
    dependency_files = DEPENDENCY_FILES.get(lang, [])
    if not dependency_files:
        return ""
    if lang == GraphShardMetadataLanguage.CSHARP:
        possible_locks = POSSIBLE_LOCK_FILES.get(dependency_files[0], [])
        candidate = find_dependency_file_by_extension(
            current_path,
            dependency_files[0],
            possible_locks,
        )
        if candidate:
            return candidate
    for dependency_file in dependency_files:
        possible_locks = POSSIBLE_LOCK_FILES.get(dependency_file, [])
        candidate = aux_find_dependency_file(
            current_path,
            dependency_file,
            possible_locks,
        )
        if candidate:
            return candidate
    return ""


def get_vulnerability_from_n_id(  # noqa: PLR0913
    *,
    n_id: str,
    graph_shard: GraphShard,
    method: MethodsEnum,
    pkg_id: str,
    desc_params: dict[str, str],
    default_desc: bool,
) -> Vulnerability:
    node_attrs = graph_shard.graph.nodes[n_id]
    node_attrs_column = node_attrs["label_c"]
    node_attrs_line = node_attrs["label_l"]
    content = graph_shard.content_as_str
    vuln_what = graph_shard.path
    return build_lines_vuln(
        method=method.value,
        what=vuln_what,
        where=str(node_attrs_line),
        metadata=build_metadata(
            method=method.value,
            description=(
                f"{t(key=method.name, **desc_params)} {t(key='words.in')} "
                f"{ctx.SKIMS_CONFIG.namespace}/{vuln_what}"
            )
            if default_desc
            else (
                f"{t(key='REACHABILITY_METHOD_DESC', **desc_params)} "
                f"{t(key='words.in')} "
                f"{ctx.SKIMS_CONFIG.namespace}/{vuln_what}"
            ),
            snippet=make_snippet(
                content=content,
                viewport=SnippetViewport(
                    column=int(node_attrs_column),
                    line=int(node_attrs_line),
                ),
            ),
            skip=is_exclusion(content, int(node_attrs_line)),
            sca_metadata=ScaAdvisoriesMetadata(
                package=desc_params["dep"],
                vulnerable_version=desc_params["version"],
                cve=[desc_params["cve"]],
                pkg_id=pkg_id,
            ),
        ),
    )


def get_vulnerabilities_from_n_ids(  # noqa: PLR0913
    *,
    n_ids: Iterator[NId],
    method: MethodsEnum,
    shard: GraphShard,
    method_calls: int,
    dep: str,
    version: str,
    cve: str,
    pkg_id: str,
    default_desc: bool = False,
) -> MethodExecutionResult:
    init_time = time.time()
    vulns = (
        get_vulnerability_from_n_id(
            n_id=n_id,
            graph_shard=shard,
            method=method,
            desc_params={"dep": dep, "version": version, "cve": cve},
            pkg_id=pkg_id,
            default_desc=default_desc,
        )
        for n_id in n_ids
    )

    elapsed_time = time.time() - init_time

    return MethodExecutionResult(
        method_name=method.value.name,
        time_coefficient=calculate_time_coefficient(
            elapsed_time,
            shard.path,
            method_calls,
        ),
        vulnerabilities=tuple(vulns),
    )


def get_filtered_queries(
    queries: tuple[Query, ...],
    packages: dict[str, dict[str, str]],
    adv_db: dict[str, dict[str, str]],
) -> list[Query]:
    result = []
    for query in queries:
        for dep in query.dependency:
            if dep in packages:
                vuln_range = adv_db.get(dep, {}).get(query.cve)
                if vuln_range and match_vulnerable_versions(packages[dep]["version"], vuln_range):
                    query = query._replace(pkg_id=packages[dep]["id"])  # noqa: PLW2901
                    result.append(query)
    return result


def determine_queries_to_execute(
    queries: dict[GraphShardMetadataLanguage, tuple[Query, ...]],
    deps: dict[str, dict[str, dict[str, dict[str, str]]]],
    vuln_ranges: dict[str, dict[str, str]],
) -> dict[str, dict[GraphShardMetadataLanguage, tuple[Query, ...]]]:
    global_result: dict[str, dict[GraphShardMetadataLanguage, tuple[Query, ...]]] = {}
    for file_path, packages in deps["items"].items():
        result = {}
        for lang in SUPPORTED_LANGUAGES:
            filtered_queries = get_filtered_queries(queries.get(lang, ()), packages, vuln_ranges)
            result[lang] = tuple(filtered_queries)
        global_result[file_path] = result
    return global_result
