from lib_sast.reachability.f390.javascript.js_cve_2018_3721 import (
    js_cve_2018_3721 as _js_cve_2018_3721,
)
from lib_sast.reachability.f390.javascript.js_cve_2018_16487 import (
    js_cve_2018_16487 as _js_cve_2018_16487,
)
from lib_sast.reachability.f390.javascript.js_cve_2019_10744 import (
    js_cve_2019_10744 as _js_cve_2019_10744,
)
from lib_sast.reachability.f390.javascript.js_cve_2020_7766 import (
    js_cve_2020_7766 as _js_cve_2020_7766,
)
from lib_sast.reachability.f390.javascript.js_cve_2020_8203 import (
    js_cve_2020_8203 as _js_cve_2020_8203,
)
from lib_sast.reachability.f390.javascript.js_cve_2021_3918 import (
    js_cve_2021_3918 as _js_cve_2021_3918,
)
from lib_sast.reachability.f390.javascript.js_cve_2021_23771 import (
    js_cve_2021_23771 as _js_cve_2021_23771,
)
from lib_sast.reachability.f390.typescript.ts_cve_2018_3721 import (
    ts_cve_2018_3721 as _ts_cve_2018_3721,
)
from lib_sast.reachability.f390.typescript.ts_cve_2018_16487 import (
    ts_cve_2018_16487 as _ts_cve_2018_16487,
)
from lib_sast.reachability.f390.typescript.ts_cve_2019_10744 import (
    ts_cve_2019_10744 as _ts_cve_2019_10744,
)
from lib_sast.reachability.f390.typescript.ts_cve_2020_7766 import (
    ts_cve_2020_7766 as _ts_cve_2020_7766,
)
from lib_sast.reachability.f390.typescript.ts_cve_2020_8203 import (
    ts_cve_2020_8203 as _ts_cve_2020_8203,
)
from lib_sast.reachability.f390.typescript.ts_cve_2021_3918 import (
    ts_cve_2021_3918 as _ts_cve_2021_3918,
)
from lib_sast.reachability.f390.typescript.ts_cve_2021_23771 import (
    ts_cve_2021_23771 as _ts_cve_2021_23771,
)
from lib_sast.reachability.types import (
    MethodsArgs,
)
from lib_sast.root.utilities.common import (
    SHIELD_BLOCKING,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def js_cve_2018_3721(
    methods_args: MethodsArgs,
) -> MethodExecutionResult:
    return _js_cve_2018_3721(methods_args=methods_args)


@SHIELD_BLOCKING
def js_cve_2018_16487(
    methods_args: MethodsArgs,
) -> MethodExecutionResult:
    return _js_cve_2018_16487(methods_args=methods_args)


@SHIELD_BLOCKING
def js_cve_2019_10744(
    methods_args: MethodsArgs,
) -> MethodExecutionResult:
    return _js_cve_2019_10744(methods_args=methods_args)


@SHIELD_BLOCKING
def js_cve_2020_7766(
    methods_args: MethodsArgs,
) -> MethodExecutionResult:
    return _js_cve_2020_7766(methods_args=methods_args)


@SHIELD_BLOCKING
def js_cve_2020_8203(
    methods_args: MethodsArgs,
) -> MethodExecutionResult:
    return _js_cve_2020_8203(methods_args=methods_args)


@SHIELD_BLOCKING
def js_cve_2021_3918(
    methods_args: MethodsArgs,
) -> MethodExecutionResult:
    return _js_cve_2021_3918(methods_args=methods_args)


@SHIELD_BLOCKING
def js_cve_2021_23771(
    methods_args: MethodsArgs,
) -> MethodExecutionResult:
    return _js_cve_2021_23771(methods_args=methods_args)


@SHIELD_BLOCKING
def ts_cve_2021_23771(
    methods_args: MethodsArgs,
) -> MethodExecutionResult:
    return _ts_cve_2021_23771(methods_args=methods_args)


@SHIELD_BLOCKING
def ts_cve_2018_3721(
    methods_args: MethodsArgs,
) -> MethodExecutionResult:
    return _ts_cve_2018_3721(methods_args=methods_args)


@SHIELD_BLOCKING
def ts_cve_2018_16487(
    methods_args: MethodsArgs,
) -> MethodExecutionResult:
    return _ts_cve_2018_16487(methods_args=methods_args)


@SHIELD_BLOCKING
def ts_cve_2019_10744(
    methods_args: MethodsArgs,
) -> MethodExecutionResult:
    return _ts_cve_2019_10744(methods_args=methods_args)


@SHIELD_BLOCKING
def ts_cve_2020_7766(
    methods_args: MethodsArgs,
) -> MethodExecutionResult:
    return _ts_cve_2020_7766(methods_args=methods_args)


@SHIELD_BLOCKING
def ts_cve_2020_8203(
    methods_args: MethodsArgs,
) -> MethodExecutionResult:
    return _ts_cve_2020_8203(methods_args=methods_args)


@SHIELD_BLOCKING
def ts_cve_2021_3918(
    methods_args: MethodsArgs,
) -> MethodExecutionResult:
    return _ts_cve_2021_3918(methods_args=methods_args)
