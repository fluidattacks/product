from lib_sast.reachability.f008.java.java_cve_2021_37573 import (
    java_cve_2021_37573 as _java_cve_2021_37573,
)
from lib_sast.reachability.f008.javascript.js_cve_2016_1000237 import (
    js_cve_2016_1000237 as _js_cve_2016_1000237,
)
from lib_sast.reachability.f008.javascript.js_cve_2017_16016 import (
    js_cve_2017_16016 as _js_cve_2017_16016,
)
from lib_sast.reachability.f008.javascript.js_cve_2021_26539 import (
    js_cve_2021_26539 as _js_cve_2021_26539,
)
from lib_sast.reachability.f008.typescript.ts_cve_2016_1000237 import (
    ts_cve_2016_1000237 as _ts_cve_2016_1000237,
)
from lib_sast.reachability.f008.typescript.ts_cve_2017_16016 import (
    ts_cve_2017_16016 as _ts_cve_2017_16016,
)
from lib_sast.reachability.f008.typescript.ts_cve_2021_26539 import (
    ts_cve_2021_26539 as _ts_cve_2021_26539,
)
from lib_sast.reachability.types import (
    MethodsArgs,
)
from lib_sast.root.utilities.common import (
    SHIELD_BLOCKING,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def java_cve_2021_37573(
    methods_args: MethodsArgs,
) -> MethodExecutionResult:
    return _java_cve_2021_37573(methods_args=methods_args)


@SHIELD_BLOCKING
def js_cve_2017_16016(
    methods_args: MethodsArgs,
) -> MethodExecutionResult:
    return _js_cve_2017_16016(methods_args=methods_args)


@SHIELD_BLOCKING
def js_cve_2016_1000237(
    methods_args: MethodsArgs,
) -> MethodExecutionResult:
    return _js_cve_2016_1000237(methods_args=methods_args)


@SHIELD_BLOCKING
def js_cve_2021_26539(
    methods_args: MethodsArgs,
) -> MethodExecutionResult:
    return _js_cve_2021_26539(methods_args=methods_args)


@SHIELD_BLOCKING
def ts_cve_2021_26539(
    methods_args: MethodsArgs,
) -> MethodExecutionResult:
    return _ts_cve_2021_26539(methods_args=methods_args)


@SHIELD_BLOCKING
def ts_cve_2016_1000237(
    methods_args: MethodsArgs,
) -> MethodExecutionResult:
    return _ts_cve_2016_1000237(methods_args=methods_args)


@SHIELD_BLOCKING
def ts_cve_2017_16016(
    methods_args: MethodsArgs,
) -> MethodExecutionResult:
    return _ts_cve_2017_16016(methods_args=methods_args)
