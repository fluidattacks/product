from collections.abc import (
    Iterator,
)

from lib_sast.methods_enum import (
    MethodsEnumSAST as MethodsEnum,
)
from lib_sast.reachability.common import (
    get_vulnerabilities_from_n_ids,
)
from lib_sast.reachability.types import (
    MethodsArgs,
)
from lib_sast.sast_model import (
    NId,
)
from model.core import (
    MethodExecutionResult,
)


def java_cve_2021_37573(
    methods_args: MethodsArgs,
) -> MethodExecutionResult:
    method = MethodsEnum.JAVA_CVE_2021_37573

    def n_ids() -> Iterator[NId]:
        graph = methods_args.shard.syntax_graph
        dangerous_expressions: list[str] = [var.split(".")[-1] for var in methods_args.var]
        for n_id in methods_args.nodes:
            if graph.nodes[n_id].get("name").lower() in dangerous_expressions or (
                graph.nodes[n_id].get("name").lower() == "serve" and "*" in dangerous_expressions
            ):
                yield n_id

    return get_vulnerabilities_from_n_ids(
        n_ids=n_ids(),
        method=method,
        shard=methods_args.shard,
        method_calls=len(methods_args.nodes),
        dep=methods_args.dep,
        version=methods_args.current_version,
        cve=methods_args.cve,
        pkg_id=methods_args.pkg_id,
    )
