from lib_sast.root.utilities.common import (
    get_n_arg,
)
from lib_sast.sast_model import (
    Graph,
    NId,
)
from lib_sast.utils.graph import (
    adj_ast,
)


def get_list_from_node(graph: Graph, nid: NId | None) -> list:
    if nid:
        value_id = graph.nodes[nid]["value_id"]
        if graph.nodes[value_id]["label_type"] == "ArrayInitializer":
            child_ids = adj_ast(graph, value_id)
            return [graph.nodes[c_id].get("value", "") for c_id in child_ids]
        return [graph.nodes[value_id].get("value", "")]
    return []


def has_non_text_tags(graph: Graph, n_id: NId) -> bool:
    non_text_tags: list[str] = ["script", "style", "textarea"]
    second_arg = get_n_arg(graph, n_id, 1)
    if not second_arg:
        return False
    if graph.nodes[second_arg].get("label_type") == "Object":
        for pair_id in adj_ast(graph, second_arg, label_type="Pair"):
            key_n_id = graph.nodes[pair_id].get("key_id")
            if graph.nodes[key_n_id].get("symbol") == "allowedTags":
                allowed_tags = get_list_from_node(graph, pair_id)
                return any(tag in non_text_tags for tag in allowed_tags)
    return False
