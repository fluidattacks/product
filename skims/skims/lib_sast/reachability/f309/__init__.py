from lib_sast.reachability.f309.javascript.js_cve_2020_15084 import (
    js_cve_2020_15084 as _js_cve_2020_15084,
)
from lib_sast.reachability.f309.javascript.js_cve_2022_23540 import (
    js_cve_2022_23540 as _js_cve_2022_23540,
)
from lib_sast.reachability.f309.typescript.ts_cve_2020_15084 import (
    ts_cve_2020_15084 as _ts_cve_2020_15084,
)
from lib_sast.reachability.f309.typescript.ts_cve_2022_23540 import (
    ts_cve_2022_23540 as _ts_cve_2022_23540,
)
from lib_sast.reachability.types import (
    MethodsArgs,
)
from lib_sast.root.utilities.common import (
    SHIELD_BLOCKING,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def js_cve_2022_23540(
    methods_args: MethodsArgs,
) -> MethodExecutionResult:
    return _js_cve_2022_23540(methods_args=methods_args)


@SHIELD_BLOCKING
def js_cve_2020_15084(
    methods_args: MethodsArgs,
) -> MethodExecutionResult:
    return _js_cve_2020_15084(methods_args=methods_args)


@SHIELD_BLOCKING
def ts_cve_2022_23540(
    methods_args: MethodsArgs,
) -> MethodExecutionResult:
    return _ts_cve_2022_23540(methods_args=methods_args)


@SHIELD_BLOCKING
def ts_cve_2020_15084(
    methods_args: MethodsArgs,
) -> MethodExecutionResult:
    return _ts_cve_2020_15084(methods_args=methods_args)
