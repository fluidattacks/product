from lib_sast.sast_model import (
    GraphShard,
    NId,
)


def get_variables_from_import(
    shard: GraphShard,
    node: NId,
    dependencies: set[str],
) -> dict[str, list[str]]:
    variables_dict: dict[str, list[str]] = {}
    graph = shard.syntax_graph
    dep = graph.nodes[node].get("expression").split(".")[0]
    if dep and dep in dependencies:
        var = graph.nodes[node].get("label_alias") if graph.nodes[node].get("label_alias") else dep
        variables_dict[dep] = [var]
    return variables_dict
