from lib_sast.sast_model import (
    Graph,
    GraphShard,
    NId,
)
from lib_sast.utils.graph import (
    adj_ast,
    pred_ast,
)


def is_require_method(node: dict[str, str]) -> bool:
    return bool(
        (node.get("label_type") == "MethodInvocation") and (node.get("expression") == "require"),
    )


def match_dependency(args_nid: NId, graph: Graph, dependencies: set[str]) -> str | None:
    if (
        (c_ids := adj_ast(graph, args_nid))
        and len(c_ids) == 1
        and graph.nodes[c_ids[0]].get("label_type") == "Literal"
        and graph.nodes[c_ids[0]].get("value") in dependencies
    ):
        return graph.nodes[c_ids[0]].get("value")
    return None


def get_variables_from_require(
    shard: GraphShard,
    node: NId,
    dependencies: set[str],
) -> dict[str, list[str]]:
    variables_dict: dict[str, list[str]] = {}
    graph = shard.syntax_graph
    if (
        (args := graph.nodes[node].get("arguments_id"))
        and (dependency := match_dependency(args, graph, dependencies))
        and graph.nodes[pred_ast(graph, node)[0]].get("label_type") == "VariableDeclaration"
    ):
        variables_dict[dependency] = [graph.nodes[pred_ast(graph, node)[0]].get("variable")]
    return variables_dict
