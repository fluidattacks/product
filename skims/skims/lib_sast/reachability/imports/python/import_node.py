from lib_sast.sast_model import (
    GraphShard,
    NId,
)


def get_variables_from_import(
    shard: GraphShard,
    node: NId,
    dependencies: set[str],
) -> dict[str, list[str]]:
    variables_dict: dict[str, list[str]] = {}
    graph = shard.syntax_graph
    dep = graph.nodes[node].get("expression")
    if dep:
        parts = dep.split(".")
        dep, var_import = parts[0].lower(), parts[-1].lower()
        dependencies_lower = [d.lower() for d in dependencies]
        if dep in dependencies_lower:
            var = (
                graph.nodes[node].get("label_alias")
                if graph.nodes[node].get("label_alias")
                else var_import
            )
            variables_dict[dep] = [dep, var]
    return variables_dict
