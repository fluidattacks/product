from lib_sast.sast_model import (
    GraphShard,
    NId,
)


def get_variables_from_import(
    shard: GraphShard,
    node: NId,
    dependencies: set[str],
) -> dict[str, list[str]]:
    variables_dict: dict[str, list[str]] = {}
    graph = shard.syntax_graph
    dep = graph.nodes[node].get("expression").lower()
    if dep:
        dep_specific = dep.split(".")[-1].lower()
        if dep in dependencies:
            var = (
                graph.nodes[node].get("label_alias")
                if graph.nodes[node].get("label_alias")
                else dep_specific
            )
            variables_dict[dep] = [dep, var]
    return variables_dict
