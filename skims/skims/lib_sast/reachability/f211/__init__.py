from lib_sast.reachability.f211.javascript.js_cve_2017_18214 import (
    js_cve_2017_18214 as _js_cve_2017_18214,
)
from lib_sast.reachability.f211.javascript.js_cve_2019_1010266 import (
    js_cve_2019_1010266 as _js_cve_2019_1010266,
)
from lib_sast.reachability.f211.javascript.js_cve_2020_28500 import (
    js_cve_2020_28500 as _js_cve_2020_28500,
)
from lib_sast.reachability.f211.javascript.js_cve_2021_3749 import (
    js_cve_2021_3749 as _js_cve_2021_3749,
)
from lib_sast.reachability.f211.javascript.js_cve_2022_25881 import (
    js_cve_2022_25881 as _js_cve_2022_25881,
)
from lib_sast.reachability.f211.javascript.js_cve_2022_25887 import (
    js_cve_2022_25887 as _js_cve_2022_25887,
)
from lib_sast.reachability.f211.javascript.js_cve_2022_31129 import (
    js_cve_2022_31129 as _js_cve_2022_31129,
)
from lib_sast.reachability.f211.javascript.js_cve_2024_21538 import (
    js_cve_2024_21538 as _js_cve_2024_21538,
)
from lib_sast.reachability.f211.typescript.ts_cve_2017_18214 import (
    ts_cve_2017_18214 as _ts_cve_2017_18214,
)
from lib_sast.reachability.f211.typescript.ts_cve_2019_1010266 import (
    ts_cve_2019_1010266 as _ts_cve_2019_1010266,
)
from lib_sast.reachability.f211.typescript.ts_cve_2020_28500 import (
    ts_cve_2020_28500 as _ts_cve_2020_28500,
)
from lib_sast.reachability.f211.typescript.ts_cve_2021_3749 import (
    ts_cve_2021_3749 as _ts_cve_2021_3749,
)
from lib_sast.reachability.f211.typescript.ts_cve_2022_25881 import (
    ts_cve_2022_25881 as _ts_cve_2022_25881,
)
from lib_sast.reachability.f211.typescript.ts_cve_2022_25887 import (
    ts_cve_2022_25887 as _ts_cve_2022_25887,
)
from lib_sast.reachability.f211.typescript.ts_cve_2022_31129 import (
    ts_cve_2022_31129 as _ts_cve_2022_31129,
)
from lib_sast.reachability.f211.typescript.ts_cve_2024_21538 import (
    ts_cve_2024_21538 as _ts_cve_2024_21538,
)
from lib_sast.reachability.types import (
    MethodsArgs,
)
from lib_sast.root.utilities.common import (
    SHIELD_BLOCKING,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def js_cve_2017_18214(
    methods_args: MethodsArgs,
) -> MethodExecutionResult:
    return _js_cve_2017_18214(methods_args=methods_args)


@SHIELD_BLOCKING
def js_cve_2022_31129(
    methods_args: MethodsArgs,
) -> MethodExecutionResult:
    return _js_cve_2022_31129(methods_args=methods_args)


@SHIELD_BLOCKING
def js_cve_2022_25881(
    methods_args: MethodsArgs,
) -> MethodExecutionResult:
    return _js_cve_2022_25881(methods_args=methods_args)


@SHIELD_BLOCKING
def js_cve_2022_25887(
    methods_args: MethodsArgs,
) -> MethodExecutionResult:
    return _js_cve_2022_25887(methods_args=methods_args)


@SHIELD_BLOCKING
def js_cve_2020_28500(
    methods_args: MethodsArgs,
) -> MethodExecutionResult:
    return _js_cve_2020_28500(methods_args=methods_args)


@SHIELD_BLOCKING
def js_cve_2019_1010266(
    methods_args: MethodsArgs,
) -> MethodExecutionResult:
    return _js_cve_2019_1010266(methods_args=methods_args)


@SHIELD_BLOCKING
def js_cve_2021_3749(
    methods_args: MethodsArgs,
) -> MethodExecutionResult:
    return _js_cve_2021_3749(methods_args=methods_args)


@SHIELD_BLOCKING
def js_cve_2024_21538(
    methods_args: MethodsArgs,
) -> MethodExecutionResult:
    return _js_cve_2024_21538(methods_args=methods_args)


@SHIELD_BLOCKING
def ts_cve_2017_18214(
    methods_args: MethodsArgs,
) -> MethodExecutionResult:
    return _ts_cve_2017_18214(methods_args=methods_args)


@SHIELD_BLOCKING
def ts_cve_2019_1010266(
    methods_args: MethodsArgs,
) -> MethodExecutionResult:
    return _ts_cve_2019_1010266(methods_args=methods_args)


@SHIELD_BLOCKING
def ts_cve_2022_25881(
    methods_args: MethodsArgs,
) -> MethodExecutionResult:
    return _ts_cve_2022_25881(methods_args=methods_args)


@SHIELD_BLOCKING
def ts_cve_2022_25887(
    methods_args: MethodsArgs,
) -> MethodExecutionResult:
    return _ts_cve_2022_25887(methods_args=methods_args)


@SHIELD_BLOCKING
def ts_cve_2022_31129(
    methods_args: MethodsArgs,
) -> MethodExecutionResult:
    return _ts_cve_2022_31129(methods_args=methods_args)


@SHIELD_BLOCKING
def ts_cve_2020_28500(
    methods_args: MethodsArgs,
) -> MethodExecutionResult:
    return _ts_cve_2020_28500(methods_args=methods_args)


@SHIELD_BLOCKING
def ts_cve_2024_28500(
    methods_args: MethodsArgs,
) -> MethodExecutionResult:
    return _ts_cve_2024_21538(methods_args=methods_args)


@SHIELD_BLOCKING
def ts_cve_2021_3749(
    methods_args: MethodsArgs,
) -> MethodExecutionResult:
    return _ts_cve_2021_3749(methods_args=methods_args)
