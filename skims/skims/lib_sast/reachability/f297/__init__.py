from lib_sast.reachability.f297.javascript.js_cve_2023_25813 import (
    js_cve_2023_25813 as _js_cve_2023_25813,
)
from lib_sast.reachability.f297.typescript.ts_cve_2023_25813 import (
    ts_cve_2023_25813 as _ts_cve_2023_25813,
)
from lib_sast.reachability.types import (
    MethodsArgs,
)
from lib_sast.root.utilities.common import (
    SHIELD_BLOCKING,
)
from model.core import (
    MethodExecutionResult,
)


@SHIELD_BLOCKING
def js_cve_2023_25813(
    methods_args: MethodsArgs,
) -> MethodExecutionResult:
    return _js_cve_2023_25813(methods_args=methods_args)


@SHIELD_BLOCKING
def ts_cve_2023_25813(
    methods_args: MethodsArgs,
) -> MethodExecutionResult:
    return _ts_cve_2023_25813(methods_args=methods_args)
