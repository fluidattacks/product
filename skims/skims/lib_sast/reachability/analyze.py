from lib_sast.reachability.imports import (
    VARS_BY_LANG,
)
from lib_sast.reachability.types import (
    MethodsArgs,
    Query,
)
from lib_sast.sast_model import (
    GraphShard,
    GraphShardMetadataLanguage,
)
from lib_sast.utils.graph import (
    nodes_by_type as nodes_type,
)
from model.core import (
    MethodExecutionResult,
    SkimsConfig,
)


def analyze_reachability(  # noqa: PLR0913
    *,
    config: SkimsConfig,
    lang: GraphShardMetadataLanguage,
    queries_by_dep_file: dict[str, dict[GraphShardMetadataLanguage, tuple[Query, ...]]],
    shard: GraphShard,
    package_path: str,
    deps: dict[str, dict],
) -> list[MethodExecutionResult]:
    vulnerabilities: list[MethodExecutionResult] = []
    queries = queries_by_dep_file.get(package_path, {})
    if not queries:
        return vulnerabilities

    vuln_deps: set[str] = {dep for query in queries[lang] for dep in query.dependency}
    deps_vars = VARS_BY_LANG[lang]

    for query in queries[lang]:
        for dep in query.dependency:
            if (
                query.pkg_id
                and query.finding in config.checks
                and (var := deps_vars(shard, set(vuln_deps)).get(dep.lower()))
                and (nodes := nodes_type(shard.syntax_graph).get(query.node_type))
            ):
                vulnerabilities.append(
                    query.method(
                        MethodsArgs(
                            shard,
                            nodes,
                            var,
                            deps.get(dep, {}).get("version", "0"),
                            dep,
                            query.cve,
                            query.pkg_id,
                        ),
                    ),
                )
                break
    return vulnerabilities
