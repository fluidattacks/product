from lib_sast.sast_model import (
    NId,
)
from lib_sast.syntax_graph.types import (
    SyntaxGraphArgs,
)


def build_import_global_node(
    args: SyntaxGraphArgs,
    expression: str,
    module_nodes: set[NId],
    alias: str | None = None,
) -> NId:
    args.syntax_graph.add_node(
        args.n_id,
        expression=expression,
        label_type="Import",
    )

    if alias:
        args.syntax_graph.nodes[args.n_id]["label_alias"] = alias

    for node in module_nodes:
        args.syntax_graph.add_edge(
            args.n_id,
            args.generic(args.fork_n_id(node)),
            label_ast="AST",
        )

    return args.n_id
