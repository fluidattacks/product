from collections.abc import (
    Iterator,
)

from lib_sast.sast_model import (
    NId,
)
from lib_sast.syntax_graph.types import (
    SyntaxGraphArgs,
)


def build_parameter_node(  # noqa: PLR0913
    *,
    args: SyntaxGraphArgs,
    variable: str | None,
    variable_type: str | None,
    value_id: NId | None,
    c_ids: Iterator[NId] | list[str] | None = None,
    variable_id: NId | None = None,
    modifier: NId | None = None,
) -> NId:
    _id = variable_id if variable_id else args.n_id
    args.syntax_graph.add_node(
        _id,
        label_type="Parameter",
    )

    if variable:
        args.syntax_graph.nodes[_id]["variable"] = variable

    if variable_type:
        args.syntax_graph.nodes[_id]["variable_type"] = variable_type

    if value_id:
        args.syntax_graph.nodes[_id]["value_id"] = value_id
        args.syntax_graph.add_edge(
            _id,
            args.generic(args.fork_n_id(value_id)),
            label_ast="AST",
        )

    if c_ids:
        for c_id in c_ids:
            args.syntax_graph.add_edge(
                _id,
                args.generic(args.fork_n_id(c_id)),
                label_ast="AST",
            )

    if modifier:
        args.syntax_graph.nodes[_id]["parameter_mode"] = modifier

    return _id
