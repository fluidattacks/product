from lib_sast.sast_model import (
    NId,
)
from lib_sast.syntax_graph.types import (
    SyntaxGraphArgs,
)


def build_variable_declaration_node(
    args: SyntaxGraphArgs,
    variable_name: str,
    variable_type: str | None,
    value_id: NId | None,
    var_id: NId | None = None,
) -> NId:
    args.syntax_graph.add_node(
        args.n_id,
        label_type="VariableDeclaration",
        variable=variable_name,
    )

    if variable_type:
        args.syntax_graph.nodes[args.n_id]["variable_type"] = variable_type

    if value_id:
        args.syntax_graph.nodes[args.n_id]["value_id"] = value_id

        args.syntax_graph.add_edge(
            args.n_id,
            args.generic(args.fork_n_id(value_id)),
            label_ast="AST",
        )
    if var_id:
        args.syntax_graph.nodes[args.n_id]["variable_id"] = var_id

        args.syntax_graph.add_edge(
            args.n_id,
            args.generic(args.fork_n_id(var_id)),
            label_ast="AST",
        )

    return args.n_id
