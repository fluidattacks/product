from collections.abc import (
    Iterable,
)

from lib_sast.sast_model import (
    NId,
)
from lib_sast.syntax_graph.types import (
    SyntaxGraphArgs,
)


def build_class_node(  # noqa: PLR0913
    args: SyntaxGraphArgs,
    name: str,
    block_id: NId | None,
    attrl_ids: Iterable[NId] | None,
    inherited_class: str | None = None,
    modifiers_id: NId | None = None,
) -> NId:
    args.syntax_graph.add_node(
        args.n_id,
        name=name,
        block_id=block_id,
        label_type="Class",
    )

    if block_id:
        args.syntax_graph.add_edge(
            args.n_id,
            args.generic(args.fork_n_id(block_id)),
            label_ast="AST",
        )

    if attrl_ids:
        for n_id in attrl_ids:
            args.syntax_graph.add_edge(
                args.n_id,
                args.generic(args.fork_n_id(n_id)),
                label_ast="AST",
            )

    if inherited_class:
        args.syntax_graph.nodes[args.n_id]["inherited_class"] = inherited_class

    if modifiers_id:
        args.syntax_graph.nodes[args.n_id]["modifiers_id"] = modifiers_id
        args.syntax_graph.add_edge(
            args.n_id,
            args.generic(args.fork_n_id(modifiers_id)),
            label_ast="AST",
        )

    if args.metadata["class_path"]:
        args.metadata["class_path"].pop()
    return args.n_id
