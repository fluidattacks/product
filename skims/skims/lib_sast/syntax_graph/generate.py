from lib_sast.sast_model import (
    Graph,
    NId,
)
from lib_sast.sast_model import (
    GraphShardMetadataLanguage as GraphLanguage,
)
from lib_sast.syntax_graph.dispatchers import (
    DISPATCHERS_BY_LANG,
)
from lib_sast.syntax_graph.syntax_readers.common.missing_node import (
    reader as missing_node_reader,
)
from lib_sast.syntax_graph.types import (
    SyntaxGraphArgs,
    SyntaxMetadata,
)
from utils.logs import (
    log_blocking,
)


def generic(args: SyntaxGraphArgs) -> NId:
    node_type = args.ast_graph.nodes[args.n_id]["label_type"]
    lang_dispatchers = DISPATCHERS_BY_LANG[args.language]
    if dispatcher := lang_dispatchers.get(node_type):
        return dispatcher(args)

    msg = f"Missing syntax reader for {node_type} in {args.language.name}"
    log_blocking("debug", msg)

    return missing_node_reader(args, node_type)


def build_syntax_graph(
    path: str,
    language: GraphLanguage,
    ast_graph: Graph,
    *,
    with_metadata: bool,
) -> Graph:
    syntax_graph = Graph()

    syntax_metadata = SyntaxMetadata(class_path=[])

    if with_metadata:
        syntax_graph.add_node(
            "0",
            label_type="Metadata",
            structure={},
            instances={},
            imports=[],
            path=path,
        )

    generic(
        SyntaxGraphArgs(
            generic,
            path,
            language,
            ast_graph,
            syntax_graph,
            "1",
            syntax_metadata,
        ),
    )

    return syntax_graph
