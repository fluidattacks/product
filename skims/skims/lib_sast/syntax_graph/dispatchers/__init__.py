from lib_sast.sast_model import (
    GraphShardMetadataLanguage as GraphLanguage,
)
from lib_sast.syntax_graph.dispatchers.c_sharp import (
    C_SHARP_DISPATCHERS,
)
from lib_sast.syntax_graph.dispatchers.dart import (
    DART_DISPATCHERS,
)
from lib_sast.syntax_graph.dispatchers.go import (
    GO_DISPATCHERS,
)
from lib_sast.syntax_graph.dispatchers.hcl import (
    HCL_DISPATCHERS,
)
from lib_sast.syntax_graph.dispatchers.java import (
    JAVA_DISPATCHERS,
)
from lib_sast.syntax_graph.dispatchers.javascript import (
    JAVASCRIPT_DISPATCHERS,
)
from lib_sast.syntax_graph.dispatchers.json_dispatchers import (
    JSON_DISPATCHERS,
)
from lib_sast.syntax_graph.dispatchers.kotlin import (
    KOTLIN_DISPATCHERS,
)
from lib_sast.syntax_graph.dispatchers.php import (
    PHP_DISPATCHERS,
)
from lib_sast.syntax_graph.dispatchers.python import (
    PYTHON_DISPATCHERS,
)
from lib_sast.syntax_graph.dispatchers.ruby import (
    RUBY_DISPATCHERS,
)
from lib_sast.syntax_graph.dispatchers.scala import (
    SCALA_DISPATCHERS,
)
from lib_sast.syntax_graph.dispatchers.swift import (
    SWIFT_DISPATCHERS,
)
from lib_sast.syntax_graph.dispatchers.typescript import (
    TYPESCRIPT_DISPATCHERS,
)
from lib_sast.syntax_graph.dispatchers.yaml import (
    YAML_DISPATCHERS,
)
from lib_sast.syntax_graph.types import (
    Dispatcher,
)

DISPATCHERS_BY_LANG: dict[GraphLanguage, Dispatcher] = {
    GraphLanguage.CSHARP: C_SHARP_DISPATCHERS,
    GraphLanguage.DART: DART_DISPATCHERS,
    GraphLanguage.GO: GO_DISPATCHERS,
    GraphLanguage.HCL: HCL_DISPATCHERS,
    GraphLanguage.JAVA: JAVA_DISPATCHERS,
    GraphLanguage.JAVASCRIPT: JAVASCRIPT_DISPATCHERS,
    GraphLanguage.JSON: JSON_DISPATCHERS,
    GraphLanguage.KOTLIN: KOTLIN_DISPATCHERS,
    GraphLanguage.PYTHON: PYTHON_DISPATCHERS,
    GraphLanguage.PHP: PHP_DISPATCHERS,
    GraphLanguage.RUBY: RUBY_DISPATCHERS,
    GraphLanguage.SCALA: SCALA_DISPATCHERS,
    GraphLanguage.SWIFT: SWIFT_DISPATCHERS,
    GraphLanguage.TYPESCRIPT: TYPESCRIPT_DISPATCHERS,
    GraphLanguage.YAML: YAML_DISPATCHERS,
}
