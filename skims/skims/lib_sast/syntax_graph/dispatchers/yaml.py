from lib_sast.syntax_graph.syntax_readers.yaml import (
    block_mapping as yaml_block_mapping,
)
from lib_sast.syntax_graph.syntax_readers.yaml import (
    block_mapping_pair as yaml_block_mapping_pair,
)
from lib_sast.syntax_graph.syntax_readers.yaml import (
    block_sequence as yaml_block_sequence,
)
from lib_sast.syntax_graph.syntax_readers.yaml import (
    flow_node as yaml_flow_node,
)
from lib_sast.syntax_graph.syntax_readers.yaml import (
    stream as yaml_stream,
)
from lib_sast.syntax_graph.types import (
    Dispatcher,
)

YAML_DISPATCHERS: Dispatcher = {
    "block_mapping": yaml_block_mapping.reader,
    "flow_mapping": yaml_block_mapping.reader,
    "block_mapping_pair": yaml_block_mapping_pair.reader,
    "flow_pair": yaml_block_mapping_pair.reader,
    "block_sequence": yaml_block_sequence.reader,
    "flow_node": yaml_flow_node.reader,
    "stream": yaml_stream.reader,
}
